package ph.com.sunlife.wms.ingenium.data.dbutils.mapper;

import com.sunlife.ascp.configuration.data.dbutils.handler.EntityBeanListHandler;
import org.apache.commons.dbutils.BasicRowProcessor;
import org.apache.commons.dbutils.BeanProcessor;
import ph.com.sunlife.wms.data.transfer.IngeniumSunLifePlan;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class IngeniumSunLifePlanMapper extends EntityBeanListHandler<IngeniumSunLifePlan> {

  public IngeniumSunLifePlanMapper() {
    super(IngeniumSunLifePlan.class, new BasicRowProcessor(new BeanProcessor(columnsToFields())));
  }

  @Override
  public List<IngeniumSunLifePlan> handle(ResultSet rs) throws SQLException {
    List<IngeniumSunLifePlan> plans = super.handle(rs);
    return plans;
  }

  public static final Map<String, String> columnsToFields() {
    Map<String, String> columnsToFieldsMap;
    columnsToFieldsMap = new HashMap<String, String>();
    columnsToFieldsMap.put("Id", "id");
    columnsToFieldsMap.put("Description", "description");
    return columnsToFieldsMap;
  }
}
