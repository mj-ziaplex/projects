package ph.com.sunlife;

import com.sunlife.ascp.cryptography.CryptoHashAlgorithm;
import com.sunlife.ascp.cryptography.CryptographicHash;
import com.sunlife.ascp.cryptography.CryptographicHashEngine;
import com.sunlife.ascp.cryptography.MessageDigest;
import org.junit.Assert;
import org.junit.Test;
import ph.com.sunlife.pg.passwordgenerator.Hash_MD5;
import ph.com.sunlife.pg.passwordgenerator.PasswordGenerator;
import ph.com.sunlife.wms.ingenium.authentication.IngeniumPasswordBuilder;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;

public class GeneralTests {

  private static final String TEST_STRING = "Password";
  private static final String TEST_HASH = "dc647eb65e6711e155375218212b3964";

  @Test
  public void testMD5() {
    Hash_MD5 md5 = new Hash_MD5();
    String pgMd5 = md5.calcMD5(TEST_STRING);

    CryptographicHashEngine engine = new CryptographicHashEngine();
    CryptographicHash hash = engine.create(CryptoHashAlgorithm.MD5);
    hash.update(TEST_STRING);
    MessageDigest hashCode = hash.digest();
    String ascpMd5 = hashCode.getAsString();

    System.out.println(pgMd5 + " VS " + ascpMd5);
    System.out.println(TEST_HASH + " VS " + ascpMd5);

    Assert.assertEquals(pgMd5, ascpMd5);
  }

  @Test
  public void testTheKey() throws IOException, ClassNotFoundException {
    FileInputStream in = new FileInputStream("D:/WMS/WMS_Files/theKey");
    ObjectInputStream s = new ObjectInputStream(in);
    String sKey = (String) s.readObject();
    int iDisplacement = s.readInt();
    char cFirstChar = s.readChar();
    String username = (String) s.readObject();
    String password = (String) s.readObject();

    System.out.println("sKey " + sKey);
    System.out.println("iDisplacement " + iDisplacement);
    System.out.println("cFirstChar " + cFirstChar);
    System.out.println("username " + username);
    System.out.println("password " + password);
  }

  @Test
  public void testPasswordGeneration() {
//    PasswordGenerator pg = new PasswordGenerator();
//    String pgPassword = pg.generatePassword("SHDH51W");
//    System.out.println("pg >>> " + pgPassword);

    IngeniumPasswordBuilder ipb = new IngeniumPasswordBuilder("SHDH51W ");
    ipb.setSalt("R6CWAHGSUI8VQEDFBVA1C66R3QEPFBGSUNGH3EHJA4WBH2ACUCZ6Y5HZHMO44XSC");
    ipb.setDisplacement(15);
    ipb.setPasswordPrefix("v");
    String ipbPassword = ipb.build();
    System.out.println("ipb >>> " + ipbPassword);
//    Assert.assertEquals(pgPassword, ipbPassword);
  }
}
