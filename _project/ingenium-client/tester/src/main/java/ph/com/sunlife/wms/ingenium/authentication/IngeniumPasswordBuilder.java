package ph.com.sunlife.wms.ingenium.authentication;

import com.sunlife.ascp.cryptography.CryptoHashAlgorithm;
import com.sunlife.ascp.cryptography.CryptographicHash;
import com.sunlife.ascp.cryptography.CryptographicHashEngine;
import com.sunlife.ascp.cryptography.MessageDigest;

public class IngeniumPasswordBuilder {

  private String username;
  private String pseudoSalt;
  private String passwordPrefix;
  private int truncationOffset;

  public IngeniumPasswordBuilder(final String username) {
    this(username, "");
  }

  public IngeniumPasswordBuilder(final String username, final String salt) {
    if (username == null && username.length() <= 0) {
      throw new IllegalArgumentException("");
    }
    this.username = username.toLowerCase();
    this.pseudoSalt = salt == null ? "" : salt;
    this.passwordPrefix = "";
    this.truncationOffset = 1;
  }

  public String getUsername() {
    return username;
  }

  public String getSalt() {
    return pseudoSalt;
  }

  public IngeniumPasswordBuilder setSalt(final String salt) {
    this.pseudoSalt = salt == null ? "" : salt;
    return this;
  }

  public String getPasswordPrefix() {
    return passwordPrefix;
  }

  public IngeniumPasswordBuilder setPasswordPrefix(final String prefix) {
    this.passwordPrefix = prefix == null ? "" : prefix;
    return this;
  }

  public int getDisplacement() {
    return truncationOffset;
  }

  public IngeniumPasswordBuilder setDisplacement(final int displacement) {
    if (displacement > 0) {
      this.truncationOffset = displacement;
    }
    return this;
  }

  public String build() {
    CryptographicHashEngine engine = new CryptographicHashEngine();
    CryptographicHash hash = engine.create(CryptoHashAlgorithm.MD5);
    hash.update(username + pseudoSalt);
    MessageDigest hashCode = hash.digest();
    return passwordPrefix + hashCode.getAsString().substring(truncationOffset - 1, truncationOffset + 6);
  }
}
