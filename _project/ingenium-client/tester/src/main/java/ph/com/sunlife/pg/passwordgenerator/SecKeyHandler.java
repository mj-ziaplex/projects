package ph.com.sunlife.pg.passwordgenerator;

import java.io.*;

/**
 * @author PM13
 */
public class SecKeyHandler {
  private static SecKeyHandler keyretrieverInstance = null;
  private String sKey;
  private int iDisplacement;
  private char cFirstChar;
  private String username;
  private String password;

  private String temp_sKey;
  private int temp_iDisplacement;
  private char temp_cFirstChar;
  private String temp_username;
  private String temp_password;
  private boolean isFileFound = true;


  /**
   * Creates a new instance of KeyRetriever
   */
  public SecKeyHandler() {
    init_values();
  }

  //this is to ensure singelton property
  public static synchronized SecKeyHandler getInstance() {
    //check if authenticatorInstance has already been initialized
    if (keyretrieverInstance == null)
      keyretrieverInstance = new SecKeyHandler();
    return keyretrieverInstance;
  }

  private void init_values() {
    File f = new File("D:/WMS/WMS_Files/theKey");

    sKey = "";
    iDisplacement = 0;
    cFirstChar = 'a';
    //not yet sure what the default values are, just in case i left it blank
    username = "";
    password = "";
    // if the "theKey" file does not exists, initialize the variable, else retrieve the contents of the serialized files
    if (f.exists()) {
      retrieveSerialized();
    } else {
      sKey = "";
      iDisplacement = 0;
      cFirstChar = 'a';
      username = "";
      password = "";
    }
  }

  //read the serialized file and loads the contents to the specific variables involved.
  public void retrieveSerialized() {
    try {
      FileInputStream in = new FileInputStream("D:/WMS/WMS_Files/theKey");
      ObjectInputStream s = new ObjectInputStream(in);
      sKey = (String) s.readObject();
      iDisplacement = s.readInt();
      cFirstChar = s.readChar();
      username = (String) s.readObject();
      password = (String) s.readObject();

      temp_sKey = sKey;
      temp_iDisplacement = iDisplacement;
      temp_cFirstChar = cFirstChar;
      temp_username = username;
      temp_password = password;
    } catch (Exception c) {
      System.out.println(c);
      isFileFound = false;
    }
  }

  public boolean isFileFound() {
    return isFileFound;
  }

  //writes the specified variables into the file (Serialization)
  public void serialize() {
    try {
      sKey = temp_sKey;
      iDisplacement = temp_iDisplacement;
      cFirstChar = temp_cFirstChar;
      username = temp_username;
      password = temp_password;
      FileOutputStream out = new FileOutputStream("D:/WMS/WMS_Files/theKey");
      ObjectOutputStream s = new ObjectOutputStream(out);
      s.writeObject(sKey);
      s.writeInt(iDisplacement);
      s.writeChar(cFirstChar);
      s.writeObject(username);
      s.writeObject(password);
      s.flush();
    } catch (Exception e) {
    }
  }

  public void updateKey(String p_sKey) {
    temp_sKey = p_sKey;
  }

  public void updateUserName(String p_username) {
    temp_username = p_username;
  }

  public void updatePassword(String p_password) {
    temp_password = p_password;
  }

  public void updateDisplacement(int p_iDisplacement) {
    temp_iDisplacement = p_iDisplacement;
  }

  public void updateFirstChar(char p_cFirstChar) {
    temp_cFirstChar = p_cFirstChar;
  }

  public String getUserName() {
    return username;
  }

  public String getPassword() {
    return password;
  }

  public String getKey() {
    return sKey;
  }

  public int getDisplacement() {
    return iDisplacement;
  }

  public char getFirstChar() {
    return cFirstChar;
  }
}
