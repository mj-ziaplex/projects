/*
 * Copyright (C) 2019 Sun Life Financial. All Rights Reserved.
 *
 * This software is confidential and proprietary information of Sun Life Financial
 * ("Confidential Information"). You shall not disclose such Confidential Information
 * and shall use it only in accordance with the terms and conditions you entered
 * with Sun Life Financial.
 *
 *
 * Written by Marlon Janssen Arao <marlon.arao@sunlife.com>, 2019 November 26
 */

package com.sunlife.ascp.configuration.data.dbutils;

import com.sunlife.ascp.connectivity.database.DatabaseConnector;
import com.sunlife.ascp.configuration.data.DataRepository;
import com.sunlife.ascp.configuration.data.DatabaseConnectorDataSource;
import com.sunlife.ascp.configuration.data.PropertiesDataSource;
import java.sql.SQLException;
import java.util.Properties;
import javax.sql.DataSource;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;

/**
 *
 * @author Marlon Janssen Arao <mj.arao@techie.com>
 */
public class OpenQueryRepository extends DataRepository {

  private QueryRunner runner;

  public OpenQueryRepository(final DataSource dataSource) {
    super(dataSource);
    runner = new QueryRunner(DATA_SOURCE);
  }

  public OpenQueryRepository(final Properties properties) {
    this(new PropertiesDataSource(properties).getDataSource());
  }

  public OpenQueryRepository(final DatabaseConnector connector) {
    this(new DatabaseConnectorDataSource(connector).getDataSource());
  }

  protected final int insert(final String sql,
                             final String linkedServer,
                             final Object... parameters) throws SQLException {
    int result = 0;
    try {
      result = runner.execute(INSERT_QUERY, linkedServer, sql, parameters);
    } catch (SQLException sqle) {
      LOGGER.debug(sqle.getMessage(), sqle.getCause());
      throw sqle;
    }
    return result;
  }

  protected <T> T query(final String sql,
                        final String linkedServer,
                        final ResultSetHandler<T> handler,
                        final Object... parameters) throws SQLException {
    T result = null;
    try {
      result = runner.query(SELECT_QUERY, handler, linkedServer, sql, parameters);
    } catch (SQLException sqle) {
      LOGGER.debug(sqle.getMessage(), sqle.getCause());
      throw sqle;
    }
    return result;
  }

  protected final int update(final String sql,
                             final String linkedServer,
                             final Object... parameters) throws SQLException {
    int result = 0;
    try {
      result = runner.update(sql, linkedServer, sql, parameters);
    } catch (SQLException sqle) {
      LOGGER.debug(sqle.getMessage(), sqle.getCause());
      throw sqle;
    }
    return result;
  }

  private static final String UPDATE_QUERY = "UPDATE * OPENQUERY(?, ?)";
  private static final String INSERT_QUERY = "INSERT * OPENQUERY(?, ?)";
  private static final String DELETE_QUERY = "DELETE * OPENQUERY(?, ?)";
  private static final String SELECT_QUERY = "SELECT * OPENQUERY(?, ?)";
}
