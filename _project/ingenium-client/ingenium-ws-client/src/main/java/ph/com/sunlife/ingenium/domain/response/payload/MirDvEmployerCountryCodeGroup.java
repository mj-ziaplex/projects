package ph.com.sunlife.ingenium.domain.response.payload;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

@XStreamAlias("MirDvEmplrCtryCdG")
public class MirDvEmployerCountryCodeGroup {

    @XStreamImplicit(itemFieldName = "MirDvEmplrCtryCdT")
    private List<String> mirDvEmployerCountryCodeTexts;

    public List<String> getMirDvEmployerCountryCodeTexts() {
        return mirDvEmployerCountryCodeTexts;
    }

    public void setMirDvEmployerCountryCodeTexts(List<String> countryCodes) {
        mirDvEmployerCountryCodeTexts = countryCodes;
    }
}
