package ph.com.sunlife.ingenium.domain.response.payload;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

@XStreamAlias("MirCliOtherLastNmG")
public class MirClientOtherLastNameGroup {

    @XStreamImplicit(itemFieldName = "MirCliOtherLastNmT")
    private List<String> mirClientOtherLastnameTypes;

    public List<String> getMirClientOtherLastNameTypes() {
        return mirClientOtherLastnameTypes;
    }

    public void setMirClientOtherLastnameTypes(List<String> mirClientOtherLastnameTypes) {
        this.mirClientOtherLastnameTypes = mirClientOtherLastnameTypes;
    }
}
