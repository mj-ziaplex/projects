package ph.com.sunlife.ingenium.domain.response.payload;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

@XStreamAlias("MirCliIndvMidNmG")
public class MirClientIndividualMiddlenameGroup {

    @XStreamImplicit(itemFieldName = "MirCliIndvMidNmT")
    private List<String> mirClientIndividualMiddlenameTexts;

    public List<String> getMirClientIndividualMiddlenameTexts() {
        return mirClientIndividualMiddlenameTexts;
    }

    public void setMirClientIndividualMiddlenameTexts(final List<String> middlenames) {
        mirClientIndividualMiddlenameTexts = middlenames;
    }
}
