package ph.com.sunlife.ingenium.ws.domain;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import ph.com.sunlife.ingenium.domain.MirUserMessageGroup;

@XStreamAlias("OLifE")
public class ListRequirementsContainer extends Container {

    @XStreamAlias("RequirementData")
    private RequirementData requirementData;
    @XStreamAlias("MirUserMsgG")
    private MirUserMessageGroup mirUSerMessageGroup;

    public RequirementData getRequirementData() {
        return requirementData;
    }

    public ListRequirementsContainer setRequirementData(RequirementData requirementData) {
        this.requirementData = requirementData;
        return this;
    }

    public MirUserMessageGroup getMirUSerMessageGroup() {
        return mirUSerMessageGroup;
    }

    public void setMirUSerMessageGroup(MirUserMessageGroup mirUSerMessageGroup) {
        this.mirUSerMessageGroup = mirUSerMessageGroup;
    }
}
