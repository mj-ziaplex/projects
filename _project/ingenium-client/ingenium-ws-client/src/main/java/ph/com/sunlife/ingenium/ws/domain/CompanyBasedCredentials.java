package ph.com.sunlife.ingenium.ws.domain;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("UserAuthRequest")
public class CompanyBasedCredentials extends Credentials {

  @XStreamAlias("UserCoId")
  private String companyId;

  public CompanyBasedCredentials() {
    super("", new PlainPassword());
    this.companyId = "";
  }

  public CompanyBasedCredentials(final String username, final String companyId) {
    super(username, "");
    this.companyId = companyId;
  }

  public CompanyBasedCredentials(final String username, final String password, final String companyId) {
    super(username, password);
    this.companyId = companyId;
  }

  public CompanyBasedCredentials(final String username, final String password, final String algorithm, final String companyId) {
    super(username, password, algorithm);
    this.companyId = companyId;
  }

  public CompanyBasedCredentials(final String username, final Password password) {
    super(username, password);
    this.companyId = "";
  }

  public CompanyBasedCredentials(final String username, final Password password, final String companyId) {
    super(username, password);
    this.companyId = companyId;
  }

  public String getCompanyId() {
    return companyId;
  }

  public CompanyBasedCredentials setCompanyId(final String companyId) {
    this.companyId = companyId;
    return this;
  }
}
