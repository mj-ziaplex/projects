package ph.com.sunlife.ingenium.domain;

import java.util.ArrayList;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("MirCdCvgParCdG")
public class MirCdCvgParCdG {

	@XStreamImplicit(itemFieldName = "MirCdCvgParCdT")
	protected List<String> mirCdCvgParCdT;

	public List<String> getMirCdCvgParCdT() {
		if (mirCdCvgParCdT == null) {
			mirCdCvgParCdT = new ArrayList<String>();
		}
		return this.mirCdCvgParCdT;
	}

}
