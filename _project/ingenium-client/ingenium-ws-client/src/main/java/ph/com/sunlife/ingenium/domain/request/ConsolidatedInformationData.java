package ph.com.sunlife.ingenium.domain.request;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import ph.com.sunlife.ingenium.domain.MirPolicyID;

@XStreamAlias("InquiryConsolidatedInformationData")
public class ConsolidatedInformationData {

    @XStreamAlias("SrcSystem")
    private String sourceSystem;
    @XStreamAlias("MirPolId")
    private MirPolicyID mirPolicyId;

    public String getSourceSystem() {
        return sourceSystem;
    }

    public void setSourceSystem(String sourceSystem) {
        this.sourceSystem = sourceSystem;
    }

    public MirPolicyID getMirPolicyId() {
        return mirPolicyId;
    }

    public void setMirPolicyId(MirPolicyID mirPolicyId) {
        this.mirPolicyId = mirPolicyId;
    }
}
