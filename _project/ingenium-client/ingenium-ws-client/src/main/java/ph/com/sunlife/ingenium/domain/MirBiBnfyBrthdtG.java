package ph.com.sunlife.ingenium.domain;

import java.util.ArrayList;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("MirBiBnfyBrthdtG")
public class MirBiBnfyBrthdtG {

	@XStreamImplicit(itemFieldName = "MirBiBnfyBrthdtT")
	protected List<String> mirBiBnfyBrthdtT;

	public List<String> getMirBiBnfyBrthdtT() {
		if (mirBiBnfyBrthdtT == null) {
			mirBiBnfyBrthdtT = new ArrayList<String>();
		}
		return this.mirBiBnfyBrthdtT;
	}

}
