package ph.com.sunlife.ingenium.domain.response.payload;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

@XStreamAlias("MirCliAddrEffDtG")
public class MirClientAddressEffectivityDateGroup {

    @XStreamImplicit(itemFieldName = "MirCliAddrEffDtT")
    private List<String> mirClientAddressEffectivityDateTexts;

    public List<String> getMirClientAddressEffectivityDateTexts() {
        return mirClientAddressEffectivityDateTexts;
    }

    public void setMirClientAddressEffectivityDateTexts(final List<String> dates) {
        mirClientAddressEffectivityDateTexts = dates;
    }
}
