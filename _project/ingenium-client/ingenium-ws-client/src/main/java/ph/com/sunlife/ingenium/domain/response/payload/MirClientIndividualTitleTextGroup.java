package ph.com.sunlife.ingenium.domain.response.payload;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

@XStreamAlias("MirCliIndvTitlTxtG")
public class MirClientIndividualTitleTextGroup {

    @XStreamImplicit(itemFieldName = "MirCliIndvTitlTxtT")
    private List<String> mirClientIndividualTitleTextTypes;

    public List<String> getMirClientIndividualTitleTextTypes() {
        return mirClientIndividualTitleTextTypes;
    }

    public void setMirClientIndividualTitleTextTypes(final List<String> types) {
        mirClientIndividualTitleTextTypes = types;
    }
}
