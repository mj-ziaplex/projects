package ph.com.sunlife.ingenium.ws.domain;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import ph.com.sunlife.ingenium.domain.MirUserMessageGroup;

@XStreamAlias("OLifE")
public class ClientInquiryContainer extends Container {

    @XStreamAlias("ClientData")
    private ClientData clientData;
    @XStreamAlias("ClientInquiryData")
    private ClientInquiryData clientInquiryData;
    @XStreamAlias("MirUserMsgG")
    private MirUserMessageGroup mirUserMessageGroup;

    public ClientData getClientData() {
        return clientData;
    }

    public void setClientData(ClientData clientData) {
        this.clientData = clientData;
    }

    public ClientInquiryData getClientInquiryData() {
        return clientInquiryData;
    }

    public void setClientInquiryData(ClientInquiryData clientInquiryData) {
        this.clientInquiryData = clientInquiryData;
    }

    public MirUserMessageGroup getMirUserMessageGroup() {
        return mirUserMessageGroup;
    }

    public void setMirUserMessageGroup(MirUserMessageGroup mirUserMessageGroup) {
        this.mirUserMessageGroup = mirUserMessageGroup;
    }
}
