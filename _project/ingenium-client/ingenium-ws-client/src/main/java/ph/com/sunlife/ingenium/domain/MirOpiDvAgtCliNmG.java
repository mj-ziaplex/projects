package ph.com.sunlife.ingenium.domain;

import java.util.ArrayList;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("MirOpiDvAgtCliNmG")
public class MirOpiDvAgtCliNmG {

	@XStreamImplicit(itemFieldName = "MirOpiDvAgtCliNmT")
	protected List<String> mirOpiDvAgtCliNmT;

	public List<String> getMirOpiDvAgtCliNmT() {
		if (mirOpiDvAgtCliNmT == null) {
			mirOpiDvAgtCliNmT = new ArrayList<String>();
		}
		return this.mirOpiDvAgtCliNmT;
	}

}
