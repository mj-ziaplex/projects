package ph.com.sunlife.zold.ws.ingenium.soap;

import com.sun.xml.rpc.client.SenderException;
import com.sun.xml.rpc.client.StreamingSenderState;
import com.sun.xml.rpc.client.StubBase;
import com.sun.xml.rpc.client.http.HttpClientTransport;
import com.sun.xml.rpc.encoding.CombinedSerializer;
import com.sun.xml.rpc.encoding.InternalTypeMappingRegistry;
import com.sun.xml.rpc.encoding.SOAPDeserializationContext;
import com.sun.xml.rpc.encoding.SOAPDeserializationState;
import com.sun.xml.rpc.soap.message.InternalSOAPMessage;
import com.sun.xml.rpc.soap.message.SOAPBlockInfo;
import com.sun.xml.rpc.soap.streaming.SOAPNamespaceConstants;
import com.sun.xml.rpc.streaming.XMLReader;
import com.sun.xml.rpc.wsdl.document.schema.SchemaConstants;

import javax.xml.namespace.QName;
import javax.xml.rpc.JAXRPCException;
import javax.xml.rpc.handler.HandlerChain;
import java.rmi.RemoteException;

public class TXLifeServicePort_Stub extends StubBase implements TXLifeServicePort {

    private static final int callTXLife_OPCODE = 0;
    private static final QName ns1_callTXLife_callTXLifeRequest_QNAME = new QName("http://schema/webservices.elink.solcorp.com",
            "callTXLifeRequest");
    private static final QName ns2_string_TYPE_QNAME = SchemaConstants.QNAME_TYPE_STRING;
    private CombinedSerializer ns2_myns2_string__java_lang_String_String_Serializer;
    private static final QName ns1_callTXLife_callTXLifeResponse_QNAME = new QName("http://schema/webservices.elink.solcorp.com",
            "callTXLifeResponse");
    private static final String[] myNamespace_declarations = new String[] {};

    private static final QName[] understoodHeaderNames = new QName[] {};
	
    /*
     * public constructor
    public TXLifeServicePort_Stub(HandlerChain handlerChain){
        super(handlerChain);
        StringBuffer stb = new StringBuffer();
        stb.append("http://");
        stb.append(Configuration.getInstanceOf().getHost());
        stb.append(":");
        stb.append(Configuration.getInstanceOf().getPort());
        stb.append("/PFTXLifeWebservices/services/TXLifeService");
        System.out.println(stb.toString());
        _setProperty(ENDPOINT_ADDRESS_PROPERTY, stb.toString());
    }
    */
    
    /**
     * @author i694 - Olund
     * MR-WF-15-00177 - Config change and testing of Ingenium HTTPS protocol (Mandiant remediation)
     */
    public TXLifeServicePort_Stub(HandlerChain handlerChain){
        super(handlerChain);
        StringBuffer stb = new StringBuffer();
        stb.append(Configuration.getInstanceOf().getHost());//This will get the URL string in configuration property for WMS/INGENIUM web service connection
        //String ingeniumWebserviceTarget = Configuration.getInstanceOf().getHost(); 
        System.out.println("WMS REQUESTING to : "+  stb.toString());
        _setProperty(ENDPOINT_ADDRESS_PROPERTY, stb.toString());
    }

    /*
     * implementation of callTXLife
     */
    public java.lang.String callTXLife(java.lang.String request) throws java.rmi.RemoteException{

        try{

            StreamingSenderState _state = _start(_handlerChain);

            InternalSOAPMessage _request = _state.getRequest();
            _request.setOperationCode(callTXLife_OPCODE);

            SOAPBlockInfo _bodyBlock = new SOAPBlockInfo(ns1_callTXLife_callTXLifeRequest_QNAME);
            _bodyBlock.setValue(request);
            _bodyBlock.setSerializer(ns2_myns2_string__java_lang_String_String_Serializer);
            _request.setBody(_bodyBlock);

            _state.getMessageContext().setProperty(HttpClientTransport.HTTP_SOAPACTION_PROPERTY, "");

            _send((String)_getProperty(ENDPOINT_ADDRESS_PROPERTY), _state);

            java.lang.String _result = null;
            Object _responseObj = _state.getResponse().getBody().getValue();
            if (_responseObj instanceof SOAPDeserializationState){
                _result = (java.lang.String)((SOAPDeserializationState)_responseObj).getInstance();
            }else{
                _result = (java.lang.String)_responseObj;
            }

            return _result;

        }catch (RemoteException e){
            // let this one through unchanged
            throw e;
        }
        catch (JAXRPCException e){
            throw new RemoteException(e.getMessage(), e);
        }
        catch (Exception e)
        {
            if (e instanceof RuntimeException)
            {
                throw (RuntimeException)e;
            }
            else
            {
                throw new RemoteException(e.getMessage(), e);
            }
        }
    }

    /*
     * this method deserializes the request/response structure in the body
     */
    protected void _readFirstBodyElement(XMLReader bodyReader, SOAPDeserializationContext deserializationContext,
            StreamingSenderState state) throws Exception
    {
        int opcode = state.getRequest().getOperationCode();
        switch (opcode)
        {
        case callTXLife_OPCODE:
            _deserialize_callTXLife(bodyReader, deserializationContext, state);
            break;
        default:
            throw new SenderException("sender.response.unrecognizedOperation", Integer.toString(opcode));
        }
    }

    /*
     * This method deserializes the body of the callTXLife operation.
     */
    private void _deserialize_callTXLife(XMLReader bodyReader, SOAPDeserializationContext deserializationContext,
            StreamingSenderState state) throws Exception
    {
        Object myStringObj = ns2_myns2_string__java_lang_String_String_Serializer.deserialize(
                ns1_callTXLife_callTXLifeResponse_QNAME, bodyReader, deserializationContext);

        SOAPBlockInfo bodyBlock = new SOAPBlockInfo(ns1_callTXLife_callTXLifeResponse_QNAME);
        bodyBlock.setValue(myStringObj);
        state.getResponse().setBody(bodyBlock);
    }

    protected String _getDefaultEnvelopeEncodingStyle()
    {
        return null;
    }

    public String _getImplicitEnvelopeEncodingStyle()
    {
        return "";
    }

    public String _getEncodingStyle()
    {
        return SOAPNamespaceConstants.ENCODING;
    }

    public void _setEncodingStyle(String encodingStyle){
        throw new UnsupportedOperationException("cannot set encoding style");
    }

    /*
     * This method returns an array containing (prefix, nsURI) pairs.
     */
    protected String[] _getNamespaceDeclarations(){
        return myNamespace_declarations;
    }

    /*
     * This method returns an array containing the names of the headers we understand.
     */
    public QName[] _getUnderstoodHeaders() {
        return understoodHeaderNames;
    }

    public void _initialize(InternalTypeMappingRegistry registry) throws Exception {
        super._initialize(registry);
        ns2_myns2_string__java_lang_String_String_Serializer = (CombinedSerializer)registry.getSerializer("",
                java.lang.String.class, ns2_string_TYPE_QNAME);
    }
}