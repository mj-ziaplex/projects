package ph.com.sunlife.ingenium.domain;

import java.util.ArrayList;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("MirRtCvgMeFctG")
public class MirRtCvgMeFctG {

	@XStreamImplicit(itemFieldName = "MirRtCvgMeFctT")
	protected List<String> mirRtCvgMeFctT;

	public List<String> getMirRtCvgMeFctT() {
		if (mirRtCvgMeFctT == null) {
			mirRtCvgMeFctT = new ArrayList<String>();
		}
		return this.mirRtCvgMeFctT;
	}

}
