//package ph.com.sunlife.ingenium.domain.request;
//
//import com.thoughtworks.xstream.annotations.XStreamAlias;
//import ph.com.sunlife.ingenium.domain.Container;
//
//@XStreamAlias("OLifE")
//public class PolicyDataContainer implements Container {
//
//  @XStreamAlias("PolicyData")
//  private PolicyData data;
//
//  public PolicyDataContainer() { }
//
//  public PolicyDataContainer(final PolicyData data) {
//    this.data = data;
//  }
//
//  @Override
//  public PolicyData getPayload() {
//    return data;
//  }
//
//  public PolicyDataContainer setPayload(PolicyData policyData) {
//    this.data = policyData;
//    return this;
//  }
//}
