package ph.com.sunlife.zold.ws.ingenium.soap;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class WebRequest implements Cloneable {

  private String webRequest;

  public Object copy() {
    try {
      return super.clone();
    } catch (Exception x) {
      return null;
    }
  }

  public WebRequest() {
    StringBuffer xmlData = new StringBuffer();
    try {
      BufferedReader reader = new BufferedReader(new FileReader("D:/java/TSS/Input1/BeneficiaryList.xml"));
      String line = reader.readLine();
      while (line != null) {
        xmlData.append(line.trim());
        line = reader.readLine();
        //System.out.println(line);
      }
    } catch (IOException ioe) {
      ioe.printStackTrace();
    }
    webRequest = xmlData.toString();
  }


  public String getRequestString() {
    return webRequest;
  }

}
