package ph.com.sunlife.ingenium.ws.domain;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("TXLife")
public class RequirementsListResponse extends RequestResponse {

    @XStreamAlias("UserAuthResponse")
    private ResponseMetadata metadata;
    @XStreamAlias("TXLifeResponse")
    private RequirementsListTransactionResponse transaction;

    public ResponseMetadata getMetadata() {
        return metadata;
    }

    public void setMetadata(ResponseMetadata metadata) {
        this.metadata = metadata;
    }

    public RequirementsListTransactionResponse getTransaction() {
        return transaction;
    }

    public void setTransaction(RequirementsListTransactionResponse transaction) {
        this.transaction = transaction;
    }
}
