package ph.com.sunlife.zold.ws.ingenium;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ph.com.sunlife.wms.ingenium.configuration.IngeniumClientConfigurator;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;


/**
 * @author PM13
 */
public class IngBusinessProcesses {

  //ROBIN Mods -mcasio 4/5/2011 (start)
  private String m_sIngeniumServer = "";
  //ROBIN Mods -mcasio 4/5/2011 (end)

  //ROBIN Mods -albaj 4/5/2011 (start)
  private String m_sIngeniumWSTarget = "";
  //ROBIN Mods -albaj 4/5/2011 (end)


  WMSLogger TheLogger = new WMSLogger("IngBusinessProcesses");
  boolean printParsedXML = true;
  private InvokeIngenium invokeIngenium;

  private Date date = new Date();
  private SimpleDateFormat exeDateF = new SimpleDateFormat("yyyy-MM-dd");
  private SimpleDateFormat exeTimeF = new SimpleDateFormat("HH:mm:ss");
  private String exeDate = exeDateF.format(date);
  private String exeTime = exeTimeF.format(date);

  private ApplicationContext context =
      new AnnotationConfigApplicationContext(IngeniumClientConfigurator.class);
  private String effectivityDate =
      (String) context.getBean("oldDefaultEffectivityDate");

  /**
   * Creates a new instance of IngBusinessProcesses
   */
  public IngBusinessProcesses() {
    invokeIngenium = new InvokeIngenium();
  }

  public IngBusinessProcesses(String sIngeniumServer, String sWSTarget) {
    invokeIngenium = new InvokeIngenium(sIngeniumServer, sWSTarget);
  }


  public String createLogin(String Authenticate[]) {
    StringBuffer strBuffer = new StringBuffer();
    strBuffer.append("<?xml version=\"1.0\" encoding=\"UTF-8\" ?> ");
    strBuffer.append("<TXLife>");
    strBuffer.append("<UserAuthRequest>");
    strBuffer.append("<UserLoginName>");
    strBuffer.append(Authenticate[0]);
    strBuffer.append("</UserLoginName>");
    strBuffer.append("<UserPswd>");
    strBuffer.append("<CryptType>");
    strBuffer.append("NONE");
    strBuffer.append("</CryptType>");
    strBuffer.append("<Pswd>");
    strBuffer.append(Authenticate[1]);
    strBuffer.append("</Pswd>");
    strBuffer.append("</UserPswd>");
    strBuffer.append("</UserAuthRequest>");
    return strBuffer.toString();
  }


  public Object InquiryULDetails_Append(String Authenticate[], String strMirPolIdBase, String Coverage_Number) {

//    ResourceBundle rb = ResourceBundle.getBundle("com.ph.sunlife.component.properties.Ingenium");
    DateFormat dft = new SimpleDateFormat("yyyy-MM-dd");

    String effective_Date = "";
    if (!effectivityDate.trim().equals(""))
      effective_Date = effectivityDate;
    else {
      effective_Date = dft.format(new Date());
    }

    HashMap hs_result = (HashMap) InquiryULDetails_Append(Authenticate, strMirPolIdBase, Coverage_Number, effective_Date);
    return hs_result;
  }


  public Object InquiryULDetails_Append(String Authenticate[], String strMirPolIdBase, String Coverage_Number, String Effective_Date) {
    HashMap hs_result = null;
    String PolicyNo = "";
    String suffix = "";
    StringBuffer strBuffer = new StringBuffer();
    if (strMirPolIdBase.length() > 9) {
      PolicyNo = strMirPolIdBase.substring(0, 9);
      suffix = "" + strMirPolIdBase.charAt(9);
    } else {
      PolicyNo = strMirPolIdBase;
    }
    strBuffer.append(createLogin(Authenticate))
        .append("<TXLifeRequest>")
        .append("<TransRefGUID>2996b11f-e5b2-d00f-281b-03582683cd49</TransRefGUID> ")
        .append("<TransType tc=\"InquiryULDetails\">InquiryULDetails</TransType>")
        .append("<TransExeDate>")
        .append(exeDate)
        .append("</TransExeDate>")
        .append("<TransExeTime>")
        .append(exeTime)
        .append("</TransExeTime>")
        .append("<OLifE>")
        .append("<InquiryULDetailsData>")
        .append("<MirCommonFields>")
        .append(" <MirCvgNum>")
        .append(Coverage_Number)
        .append("</MirCvgNum>")
        .append("<MirPolId>")
        .append("<MirPolIdBase>")
        .append(PolicyNo)
        .append("</MirPolIdBase> ")
        .append("<MirPolIdSfx>")
        .append(suffix)
        .append("</MirPolIdSfx> ")
        .append("</MirPolId>")
        .append("</MirCommonFields>")
        .append("<MirDvEffDt>")
        .append(Effective_Date)
        .append("</MirDvEffDt>")
        .append("<SrcSystem>WMS</SrcSystem>")
        .append("</InquiryULDetailsData>")
        .append("</OLifE>")
        .append("</TXLifeRequest>")
        .append("</TXLife>");
    try {
      StringBuffer sb = new StringBuffer();
      sb.append("InquiryULDetailsData_PolNo-");
      sb.append(strMirPolIdBase);
      hs_result = invokeIngenium.submit(strBuffer.toString(), sb.toString());

      if (printParsedXML) {
        Object strkeys[] = hs_result.keySet().toArray();
        for (int i = 0; i < strkeys.length; i++) {
          ArrayList al_result = (ArrayList) hs_result.get(strkeys[i]);
          TheLogger.info("TheLogger strkeys: " + strkeys[i] + ": ");
          for (int j = 0; j < al_result.size(); j++)
            TheLogger.info("\t" + al_result.get(j));
        }
      }
      TheLogger.debug("InquiryULDetailsData was called using client number");
    } catch (Exception ex) {
      TheLogger.error(ex.getMessage());
    }
    return hs_result;
  }


  public Object InquiryPolicyValues_Append(String Authenticate[], String strMirPolIdBase) {

//    ResourceBundle rb = ResourceBundle.getBundle("com.ph.sunlife.component.properties.Ingenium");
    DateFormat dft = new SimpleDateFormat("yyyy-MM-dd");

    String effective_Date = "";
    if (effectivityDate.trim().equals(""))
      effective_Date = effectivityDate;
    else {
      effective_Date = dft.format(new Date());
    }
    HashMap hs_result = (HashMap) InquiryPolicyValues_Append(Authenticate, strMirPolIdBase, effective_Date);
    return hs_result;
  }


  public Object InquiryPolicyValues_Append(String Authenticate[], String strMirPolIdBase, String Effective_Date) {
    HashMap hs_result = null;
    String PolicyNo = "";
    String suffix = "";
    StringBuffer strBuffer = new StringBuffer();
    if (strMirPolIdBase.length() > 9) {
      PolicyNo = strMirPolIdBase.substring(0, 9);
      suffix = "" + strMirPolIdBase.charAt(9);
    } else {
      PolicyNo = strMirPolIdBase;
    }
    strBuffer.append(createLogin(Authenticate))
        .append("<TXLifeRequest>")
        .append("<TransRefGUID>2996b11f-e5b2-d00f-281b-03582683cd49</TransRefGUID> ")
        .append("<TransType tc=\"InquiryPolicyValues\">InquiryPolicyValues</TransType> ")
        .append("<TransExeDate>2006-05-31</TransExeDate>")
        .append("<TransExeTime>09:00:00-05:00</TransExeTime>")
        .append("<OLifE>")
        .append("<InquiryPolicyValuesData>")
        .append("<MirCommonFields>")
//   		.append(" <MirCvgNum>")
//   		.append("01")
//   		.append("</MirCvgNum>")
        .append("<MirPolId>")
        .append("<MirPolIdBase>")
        .append(PolicyNo)
        .append("</MirPolIdBase> ")
        .append("<MirPolIdSfx>")
        .append(suffix)
        .append("</MirPolIdSfx> ")
        .append("</MirPolId>")
        .append("<MirDvEffDt>")
        .append(Effective_Date)
        .append("</MirDvEffDt>")
        .append("</MirCommonFields>")
        .append("<SrcSystem>WMS</SrcSystem>")
        .append("</InquiryPolicyValuesData>")
        .append("</OLifE>")
        .append("</TXLifeRequest>")
        .append("</TXLife>");
    try {
      StringBuffer sb = new StringBuffer();
      sb.append("InquiryPolicyValuesData_PolNo-");
      sb.append(strMirPolIdBase);
      hs_result = invokeIngenium.submit(strBuffer.toString(), sb.toString());

      if (printParsedXML) {
        Object strkeys[] = hs_result.keySet().toArray();
        for (int i = 0; i < strkeys.length; i++) {
          ArrayList al_result = (ArrayList) hs_result.get(strkeys[i]);
          TheLogger.info("TheLogger strkeys: " + strkeys[i] + ": ");
          for (int j = 0; j < al_result.size(); j++)
            TheLogger.info("\t" + al_result.get(j));
        }
      }
      TheLogger.debug("InquiryPolicyValuesData was called using client number");
    } catch (Exception ex) {
      TheLogger.error(ex.getMessage());
    }
    return hs_result;
  }


  public Object InquiryCoveragePremiums_Append(String Authenticate[], String strMirPolIdBase, String Coverage_Number) {
    HashMap hs_result = null;
    String PolicyNo = "";
    String suffix = "";
    StringBuffer strBuffer = new StringBuffer();
    if (strMirPolIdBase.length() > 9) {
      PolicyNo = strMirPolIdBase.substring(0, 9);
      suffix = "" + strMirPolIdBase.charAt(9);
    } else {
      PolicyNo = strMirPolIdBase;
    }
    strBuffer.append(createLogin(Authenticate))
        .append("<TXLifeRequest>")
        .append("<TransRefGUID>2996b11f-e5b2-d00f-281b-03582683cd49</TransRefGUID> ")
        .append(" <TransType tc=\"InquiryCoveragePremiums\">InquiryCoveragePremiums</TransType>")
        .append("<TransExeDate>")
        .append(exeDate)
        .append("</TransExeDate>")
        .append("<TransExeTime>")
        .append(exeTime)
        .append("</TransExeTime>")
        .append("<OLifE>")
        .append("<InquiryCoveragePremiumsData>")
        .append("<MirCommonFields>")
        .append(" <MirCvgNum>")
        .append(Coverage_Number)
        .append("</MirCvgNum>")
        .append("<MirPolId>")
        .append("<MirPolIdBase>")
        .append(PolicyNo)
        .append("</MirPolIdBase> ")
        .append("<MirPolIdSfx>")
        .append(suffix)
        .append("</MirPolIdSfx> ")
        .append("</MirPolId>")
        .append(" <MirCvgNum>")
        .append(Coverage_Number)
        .append("</MirCvgNum>")
        .append("</MirCommonFields>")
        .append("<SrcSystem>WMS</SrcSystem>")
        .append("</InquiryCoveragePremiumsData>")
        .append("</OLifE>")
        .append("</TXLifeRequest>")
        .append("</TXLife>");
    try {
      StringBuffer sb = new StringBuffer();
      sb.append("InquiryCoveragePremiums_PolNo-");
      sb.append(strMirPolIdBase);
      hs_result = invokeIngenium.submit(strBuffer.toString(), sb.toString());

      if (printParsedXML) {
        Object strkeys[] = hs_result.keySet().toArray();
        for (int i = 0; i < strkeys.length; i++) {
          ArrayList al_result = (ArrayList) hs_result.get(strkeys[i]);
          TheLogger.info("TheLogger strkeys: " + strkeys[i] + ": ");
          for (int j = 0; j < al_result.size(); j++)
            TheLogger.info("\t" + al_result.get(j));
        }
      }
      TheLogger.debug("InquiryCoveragePremiums was called using client number");
    } catch (Exception ex) {
      TheLogger.error(ex.getMessage());
    }
    return hs_result;
  }


  public Object DeferredActivityList_Append(String Authenticate[], String strMirPolIdBase, String Effective_Date) {
    HashMap hs_result = null;
    String PolicyNo = "";
    String suffix = "";
    StringBuffer strBuffer = new StringBuffer();
    if (strMirPolIdBase.length() > 9) {
      PolicyNo = strMirPolIdBase.substring(0, 9);
      suffix = "" + strMirPolIdBase.charAt(9);
    } else {
      PolicyNo = strMirPolIdBase;
    }
    strBuffer.append(createLogin(Authenticate))
        .append("<TXLifeRequest>")
        .append("<TransRefGUID>2996b11f-e5b2-d00f-281b-03582683cd49</TransRefGUID> ")
        .append("<TransType tc=\"DeferredActivityList\">DeferredActivityList</TransType> ")
        .append("<TransExeDate/>")
        .append("<TransExeTime/>")

        .append("<OLifE>")
        .append("<DeferredActivityListData>")
        .append("<MirPolId>")
        .append("<MirDvEffDt>")
        //. 2009-02-19
        .append(Effective_Date)
        .append("</MirDvEffDt>")
        .append("<MirPolIdBase>")
        .append(PolicyNo)
        .append("</MirPolIdBase> ")
        .append("<MirPolIdSfx>")
        .append(suffix)
        .append("</MirPolIdSfx> ")
        .append("</MirPolId>")
        .append("</DeferredActivityListData>")
        .append("</OLifE>")
        .append("</TXLifeRequest>")
        .append("</TXLife>");
    try {
      StringBuffer sb = new StringBuffer();
      sb.append("DeferredActivityList_PolNo-");
      sb.append(strMirPolIdBase);
      hs_result = invokeIngenium.submit(strBuffer.toString(), sb.toString());

      if (printParsedXML) {
        Object strkeys[] = hs_result.keySet().toArray();
        for (int i = 0; i < strkeys.length; i++) {
          ArrayList al_result = (ArrayList) hs_result.get(strkeys[i]);
          TheLogger.info("TheLogger strkeys: " + strkeys[i] + ": ");
          for (int j = 0; j < al_result.size(); j++)
            TheLogger.info("\t" + al_result.get(j));
        }
      }
      TheLogger.debug("DeferredActivityList was called using client number");
    } catch (Exception ex) {
      TheLogger.error(ex.getMessage());
    }
    return hs_result;
  }


  public Object DeferredActivityList_Append(String Authenticate[], String strMirPolIdBase) {


//    ResourceBundle rb = ResourceBundle.getBundle("com.ph.sunlife.component.properties.Ingenium");
    DateFormat dft = new SimpleDateFormat("yyyy-MM-dd");

    String effective_Date = "";
    if (effectivityDate.trim().equals(""))
      effective_Date = effectivityDate;
    else {
      effective_Date = dft.format(new Date());
    }


    HashMap hs_result = (HashMap) DeferredActivityList_Append(Authenticate, strMirPolIdBase, effective_Date);
    return hs_result;
  }


  public Object FundTransferPercentToPercent_Append(String Authenticate[], String strMirPolIdBase) {

//    ResourceBundle rb = ResourceBundle.getBundle("com.ph.sunlife.component.properties.Ingenium");
    DateFormat dft = new SimpleDateFormat("yyyy-MM-dd");

    String effective_Date = "";
    if (effectivityDate.trim().equals(""))
      effective_Date = effectivityDate;
    else {
      effective_Date = dft.format(new Date());
    }
    HashMap hs_result = (HashMap) FundTransferPercentToPercent_Append(Authenticate, strMirPolIdBase, effective_Date);
    return hs_result;
  }


  public Object FundTransferPercentToPercent_Append(String Authenticate[], String strMirPolIdBase, String Effective_Date) {
    HashMap hs_result = null;
    String PolicyNo = "";
    String suffix = "";
    StringBuffer strBuffer = new StringBuffer();
    if (strMirPolIdBase.length() > 9) {
      PolicyNo = strMirPolIdBase.substring(0, 9);
      suffix = "" + strMirPolIdBase.charAt(9);
    } else {
      PolicyNo = strMirPolIdBase;
    }
    strBuffer.append(createLogin(Authenticate))
        .append("<TXLifeRequest>")
        .append("<TransRefGUID>2996b11f-e5b2-d00f-281b-03582683cd49</TransRefGUID> ")
        .append("<TransType tc=\"FundTransferPercentToPercent\">FundTransferPercentToPercent</TransType> ")
        .append("<TransExeDate/>")
        .append("<TransExeTime/>")

        .append("<OLifE>")
        .append("<FundTransferPercentToPercentData>")
        .append("<MirPolId>")
        .append("<MirPolIdBase>")
        .append(PolicyNo)
        .append("</MirPolIdBase> ")
        .append("<MirPolIdSfx>")
        .append(suffix)
        .append("</MirPolIdSfx> ")
        .append("</MirPolId>")
        .append("<MirCvgNum>01</MirCvgNum> ")
        .append("<MirCiaEffDt>")
        //2007-05-12
        .append(Effective_Date)
        .append("</MirCiaEffDt>")
        .append("<MirCiaLoadAmt>0</MirCiaLoadAmt>")
        .append("<MirCiaLoadForceInd>Y</MirCiaLoadForceInd>")
        .append("<MirSupresCnfrmInd>Y</MirSupresCnfrmInd>")
        .append("<MirDvPrcesStateCd>1</MirDvPrcesStateCd>")
        .append("</FundTransferPercentToPercentData>")
        .append("</OLifE>")
        .append("</TXLifeRequest>")
        .append("</TXLife>");
    try {
      StringBuffer sb = new StringBuffer();
      sb.append("FundTransferPercentToPercent_PolNo-");
      sb.append(strMirPolIdBase);
      hs_result = invokeIngenium.submit(strBuffer.toString(), sb.toString());

      if (printParsedXML) {
        Object strkeys[] = hs_result.keySet().toArray();
        for (int i = 0; i < strkeys.length; i++) {
          ArrayList al_result = (ArrayList) hs_result.get(strkeys[i]);
          TheLogger.info("TheLogger strkeys: " + strkeys[i] + ": ");
          for (int j = 0; j < al_result.size(); j++)
            TheLogger.info("\t" + al_result.get(j));
        }
      }
      TheLogger.debug("FundTransferPercentToPercent was called using client number");
    } catch (Exception ex) {
      TheLogger.error(ex.getMessage());
    }
    return hs_result;
  }


  public Object PolicyAllocationList_Append(String Authenticate[], String strMirPolIdBase, String Effective_Date) {
    HashMap hs_result = null;
    String PolicyNo = "";
    String suffix = "";
    StringBuffer strBuffer = new StringBuffer();
    if (strMirPolIdBase.length() > 9) {
      PolicyNo = strMirPolIdBase.substring(0, 9);
      suffix = "" + strMirPolIdBase.charAt(9);
    } else {
      PolicyNo = strMirPolIdBase;
    }
    strBuffer.append(createLogin(Authenticate))
        .append("<TXLifeRequest>")
        .append("<TransRefGUID>2996b11f-e5b2-d00f-281b-03582683cd49</TransRefGUID> ")
        .append("<TransType tc=\"PolicyAllocationList\">PolicyAllocationList</TransType> ")
        .append("<TransExeDate>2006-05-31</TransExeDate> ")
        .append("<TransExeTime>09:00:00-05:00</TransExeTime>")

        .append("<OLifE>")
        .append("<PolicyAllocationListData>")
        .append("<MirPolId>")
        .append("<MirPolIdBase>")
        .append(PolicyNo)
        .append("</MirPolIdBase> ")
        .append("<MirPolIdSfx>")
        .append(suffix)
        .append("</MirPolIdSfx> ")
        .append("</MirPolId>")
        .append("<MirCdiTypCd/>")
        .append("<MirCdiEffDt>")
        //.append("2007-09-02")
        .append(Effective_Date)
        .append("</MirCdiEffDt>")
        .append("</PolicyAllocationListData>")
        .append("</OLifE>")
        .append("</TXLifeRequest>")
        .append("</TXLife>");
    try {
      StringBuffer sb = new StringBuffer();
      sb.append("PolicyAllocationList_PolNo-");
      sb.append(strMirPolIdBase);
      hs_result = invokeIngenium.submit(strBuffer.toString(), sb.toString());

      if (printParsedXML) {
        Object strkeys[] = hs_result.keySet().toArray();
        for (int i = 0; i < strkeys.length; i++) {
          ArrayList al_result = (ArrayList) hs_result.get(strkeys[i]);
          TheLogger.info("TheLogger strkeys: " + strkeys[i] + ": ");
          for (int j = 0; j < al_result.size(); j++)
            TheLogger.info("\t" + al_result.get(j));
        }
      }
      TheLogger.debug("PolicyAllocationList was called using client number");
    } catch (Exception ex) {
      TheLogger.error(ex.getMessage());
    }
    return hs_result;
  }


  public Object PolicyAllocationList_Append(String Authenticate[], String strMirPolIdBase) {


//    ResourceBundle rb = ResourceBundle.getBundle("com.ph.sunlife.component.properties.Ingenium");
    DateFormat dft = new SimpleDateFormat("yyyy-MM-dd");

    String effective_Date = "";
    if (effectivityDate.trim().equals(""))
      effective_Date = effectivityDate;
    else {
      effective_Date = dft.format(new Date());
    }


    HashMap hs_result = (HashMap) PolicyAllocationList_Append(Authenticate, strMirPolIdBase, effective_Date);
    return hs_result;
  }

  public Object InquiryConsolidatedInformation_Append(String Authenticate[], String strMirPolIdBase, String effectiveDate) {
    HashMap hs_result = null;
    String PolicyNo = "";
    String suffix = "";
    StringBuffer strBuffer = new StringBuffer();
    if (strMirPolIdBase.length() > 9) {
      PolicyNo = strMirPolIdBase.substring(0, 9);
      suffix = "" + strMirPolIdBase.charAt(9);
    } else {
      PolicyNo = strMirPolIdBase;
    }
    strBuffer.append(createLogin(Authenticate))
        .append("<TXLifeRequest>")
        .append("<TransRefGUID>2996b11f-e5b2-d00f-281b-03582683cd49</TransRefGUID> ")
        .append("<TransType tc=\"InquiryConsolidatedInformation\">InquiryConsolidatedInformation</TransType> ")
        .append("<TransExeDate>2008-07-04</TransExeDate> ")
        .append("<TransExeTime>18:32:55</TransExeTime>")

        .append("<OLifE>")
        .append("<InquiryConsolidatedInformationData>")
        .append("<SrcSystem>WMS</SrcSystem>")
        .append("<MirPolId>")
        .append("<MirDvEffDt>")
        .append(effectiveDate)
        .append("</MirDvEffDt>")

        .append("<MirPolIdBase>")
        .append(PolicyNo)
        .append("</MirPolIdBase> ")
        .append("<MirPolIdSfx>")
        .append(suffix)
        .append("</MirPolIdSfx> ")
        .append("</MirPolId>")
        .append("</InquiryConsolidatedInformationData>")
        .append("</OLifE>")
        .append("</TXLifeRequest>")
        .append("</TXLife>");
    try {
      StringBuffer sb = new StringBuffer();
      sb.append("InquiryConsolidatedInformation_PolNo-");
      sb.append(strMirPolIdBase);
      hs_result = invokeIngenium.submit(strBuffer.toString(), sb.toString());

      if (printParsedXML) {
        Object strkeys[] = hs_result.keySet().toArray();
        for (int i = 0; i < strkeys.length; i++) {
          ArrayList al_result = (ArrayList) hs_result.get(strkeys[i]);
          TheLogger.info("TheLogger strkeys: " + strkeys[i] + ": ");
          for (int j = 0; j < al_result.size(); j++)
            TheLogger.info("\t" + al_result.get(j));
        }
      }
      TheLogger.debug("InquiryConsolidatedInformation was called using client number");
    } catch (Exception ex) {
      TheLogger.error(ex.getMessage());
    }
    return hs_result;
  }


  public Object InquiryConsolidatedInformation_Append(String Authenticate[], String strMirPolIdBase) {
//    ResourceBundle rb = ResourceBundle.getBundle("com.ph.sunlife.component.properties.Ingenium");
    DateFormat dft = new SimpleDateFormat("yyyy-MM-dd");

    String effective_Date = "";
    if (effectivityDate.trim().equals(""))
      effective_Date = effectivityDate;
    else {
      effective_Date = dft.format(new Date());
    }
    HashMap hs_result = (HashMap) InquiryConsolidatedInformation_Append(Authenticate, strMirPolIdBase, effective_Date);
    return hs_result;

  }


  public Object PolicyAllocationInquiry_Append(String Authenticate[], String strMirPolIdBase) {
    HashMap hs_result = null;
    String PolicyNo = "";
    String suffix = "";
    StringBuffer strBuffer = new StringBuffer();
    if (strMirPolIdBase.length() > 9) {
      PolicyNo = strMirPolIdBase.substring(0, 9);
      suffix = "" + strMirPolIdBase.charAt(9);
    } else {
      PolicyNo = strMirPolIdBase;
    }
    strBuffer.append(createLogin(Authenticate))
        .append("<TXLifeRequest>")
        .append("<TransRefGUID>2996b11f-e5b2-d00f-281b-03582683cd49</TransRefGUID> ")
        .append("<TransType tc=\"PolicyAllocationInquiry\">PolicyAllocationInquiry</TransType> ")
        .append("<TransExeDate>2006-04-18</TransExeDate> ")
        .append("<TransExeTime>16:55:46</TransExeTime>")
        .append("<OLifE>")
        .append("<PolicyAllocationData>")

        .append("<MirPolId>")
        .append("<MirPolIdBase>")
        .append(PolicyNo)
        .append("</MirPolIdBase> ")
        .append("<MirPolIdSfx>")
        .append(suffix)
        .append("</MirPolIdSfx> ")
        .append("</MirPolId>")
        .append("<MirCommonFields>")
        .append("<MirCdiStatCd />")
        .append("<MirCdiTypCd>")
        .append("S")
        .append("</MirCdiTypCd>")
        .append("<MirCdiEffDt>2007-09-02</MirCdiEffDt>")
        .append("</MirCommonFields>")

        .append("</PolicyAllocationData>")
        .append("</OLifE>")
        .append("</TXLifeRequest>")
        .append("</TXLife>");
    try {
      StringBuffer sb = new StringBuffer();
      sb.append("PolicyAllocationInquiry_PolNo-");
      sb.append(strMirPolIdBase);
      hs_result = invokeIngenium.submit(strBuffer.toString(), sb.toString());

      if (printParsedXML) {
        Object strkeys[] = hs_result.keySet().toArray();
        for (int i = 0; i < strkeys.length; i++) {
          ArrayList al_result = (ArrayList) hs_result.get(strkeys[i]);
          TheLogger.info(strkeys[i] + ": ");
          for (int j = 0; j < al_result.size(); j++)
            TheLogger.info("\t" + al_result.get(j));
        }
      }
      TheLogger.debug("PolicyAllocationInquiry was called using client number");
    } catch (Exception ex) {
      TheLogger.error(ex.getMessage());
    }
    return hs_result;
  }

  public Object InquiryBilling_Append(String Authenticate[], String strMirPolIdBase) {
    HashMap hs_result = null;
    String PolicyNo = "";
    String suffix = "";
    StringBuffer strBuffer = new StringBuffer();
    if (strMirPolIdBase.length() > 9) {
      PolicyNo = strMirPolIdBase.substring(0, 9);
      suffix = "" + strMirPolIdBase.charAt(9);
    } else {
      PolicyNo = strMirPolIdBase;
    }
    strBuffer.append(createLogin(Authenticate))
        .append("<TXLifeRequest>")
        .append("<TransRefGUID>2996b11f-e5b2-d00f-281b-03582683cd49</TransRefGUID> ")
        .append("<TransType tc=\"InquiryBilling\">InquiryBilling</TransType> ")
        .append("<TransExeDate>")
        .append(exeDate)
        .append("</TransExeDate> ")
        .append("<TransExeTime>")
        .append(exeTime)
        .append("</TransExeTime>")
        .append("<OLifE>")
        .append("<InquiryBillingData>")
        .append("<MirCommonFields>")
        .append("<MirPolId>")
        .append("<MirPolIdBase>")
        .append(PolicyNo)
        .append("</MirPolIdBase> ")
        .append("<MirPolIdSfx>")
        .append(suffix)
        .append("</MirPolIdSfx> ")
        .append("</MirPolId>")
        .append("</MirCommonFields>")
        .append("<SrcSystem>WMS</SrcSystem> ")
        .append("</InquiryBillingData>")
        .append("</OLifE>")
        .append("</TXLifeRequest>")
        .append("</TXLife>");
    try {
      StringBuffer sb = new StringBuffer();
      sb.append("InquiryBilling_PolNo-");
      sb.append(strMirPolIdBase);
      hs_result = invokeIngenium.submit(strBuffer.toString(), sb.toString());

      if (printParsedXML) {
        Object strkeys[] = hs_result.keySet().toArray();
        for (int i = 0; i < strkeys.length; i++) {
          ArrayList al_result = (ArrayList) hs_result.get(strkeys[i]);
          TheLogger.info(strkeys[i] + ": ");
          for (int j = 0; j < al_result.size(); j++)
            TheLogger.info("\t" + al_result.get(j));
        }
      }
      TheLogger.debug("BF1191 was called using client number");
    } catch (Exception ex) {
      TheLogger.error(ex.getMessage());
    }
    return hs_result;
  }

  public Object BF1191C_Append(String Authenticate[], String strClientId, String ReqtId, String reqStat, String strNewIndexNo) {
    HashMap hs_result = null;
    String PolicyNo = "";
    String suffix = "";
    StringBuffer strBuffer = new StringBuffer();
    strBuffer.append(createLogin(Authenticate));
    strBuffer.append("<TXLifeRequest>");
    strBuffer.append("<TransRefGUID>2996b11f-e5b2-d00f-281b-03582683cd49</TransRefGUID> ");
    strBuffer.append("<TransType tc=\"RequirementsCreate\">RequirementsCreate</TransType>");
    strBuffer.append("<TransExeDate>2006-05-31</TransExeDate> ");
    strBuffer.append("<TransExeTime>09:00:00-05:00</TransExeTime> ");
    strBuffer.append("<OLifE> ");
    strBuffer.append("<RequirementData> ");
    strBuffer.append("<MirCommonFields> ");
    strBuffer.append("<MirCmpltnessInd>C</MirCmpltnessInd> ");
    strBuffer.append("<MirCliId>");
    strBuffer.append(strClientId);
    strBuffer.append("</MirCliId>");
    strBuffer.append("<MirReqirId>");
    strBuffer.append(ReqtId);
    strBuffer.append("</MirReqirId>");
    strBuffer.append("<MirCpreqStatCd>");
    strBuffer.append(reqStat);
    strBuffer.append("</MirCpreqStatCd>");
    strBuffer.append("<MirIndexNum>");
    strBuffer.append(strNewIndexNo);
    strBuffer.append("</MirIndexNum>");
    strBuffer.append("</MirCommonFields>");
    strBuffer.append("</RequirementData>");
    strBuffer.append("</OLifE>");
    strBuffer.append("</TXLifeRequest> ");
    strBuffer.append("</TXLife> ");

    try {
      StringBuffer sb = new StringBuffer();
      sb.append("BF1191 - RequirementsCreate_CliId-");
      sb.append(strClientId);
      hs_result = invokeIngenium.submit(strBuffer.toString(), sb.toString());

      if (printParsedXML) {
        Object strkeys[] = hs_result.keySet().toArray();
        for (int i = 0; i < strkeys.length; i++) {
          ArrayList al_result = (ArrayList) hs_result.get(strkeys[i]);
          TheLogger.info(strkeys[i] + ": ");
          for (int j = 0; j < al_result.size(); j++)
            TheLogger.info("\t" + al_result.get(j));
        }
      }
      TheLogger.debug("BF1191 was called using client number");
    } catch (Exception ex) {
      TheLogger.error(ex.getMessage());
    }
    return hs_result;
  }

  public Object BF1191C_Append(String Authenticate[], String strClientId, String ReqtId, String reqStat) {
    HashMap hs_result = null;
    String PolicyNo = "";
    String suffix = "";
    StringBuffer strBuffer = new StringBuffer();
    strBuffer.append(createLogin(Authenticate));
    strBuffer.append("<TXLifeRequest>");
    strBuffer.append("<TransRefGUID>2996b11f-e5b2-d00f-281b-03582683cd49</TransRefGUID> ");
    strBuffer.append("<TransType tc=\"RequirementsCreate\">RequirementsCreate</TransType>");
    strBuffer.append("<TransExeDate>2006-05-31</TransExeDate> ");
    strBuffer.append("<TransExeTime>09:00:00-05:00</TransExeTime> ");
    strBuffer.append("<OLifE> ");
    strBuffer.append("<RequirementData> ");
    strBuffer.append("<MirCommonFields> ");
    strBuffer.append("<MirCmpltnessInd>C</MirCmpltnessInd> ");
    strBuffer.append("<MirCliId>");
    strBuffer.append(strClientId);
    strBuffer.append("</MirCliId>");
    strBuffer.append("<MirReqirId>");
    strBuffer.append(ReqtId);
    strBuffer.append("</MirReqirId>");
    strBuffer.append("<MirCpreqStatCd>");
    strBuffer.append(reqStat);
    strBuffer.append("</MirCpreqStatCd>");
    strBuffer.append("</MirCommonFields>");
    strBuffer.append("</RequirementData>");
    strBuffer.append("</OLifE>");
    strBuffer.append("</TXLifeRequest> ");
    strBuffer.append("</TXLife> ");

    try {
      StringBuffer sb = new StringBuffer();
      sb.append("BF1191 - RequirementsCreate_CliId-");
      sb.append(strClientId);
      hs_result = invokeIngenium.submit(strBuffer.toString(), sb.toString());

      if (printParsedXML) {
        Object strkeys[] = hs_result.keySet().toArray();
        for (int i = 0; i < strkeys.length; i++) {
          ArrayList al_result = (ArrayList) hs_result.get(strkeys[i]);
          TheLogger.info(strkeys[i] + ": ");
          for (int j = 0; j < al_result.size(); j++)
            TheLogger.info("\t" + al_result.get(j));
        }
      }
      TheLogger.debug("BF1191 was called using client number");
    } catch (Exception ex) {
      TheLogger.error(ex.getMessage());
    }
    return hs_result;
  }

  public Object BF9892Retrieve_Append(String Authenticate[], String strMirPolIdBase) {

    HashMap hs_result = null;
    String PolicyNo = "";
    String suffix = "";
    if (strMirPolIdBase.length() > 9) {
      PolicyNo = strMirPolIdBase.substring(0, 9);
      suffix = "" + strMirPolIdBase.charAt(9);
    } else {
      PolicyNo = strMirPolIdBase;
    }
    StringBuffer strBuffer = new StringBuffer();
    strBuffer.append(createLogin(Authenticate));
    strBuffer.append("<TXLifeRequest>");
    strBuffer.append("<TransRefGUID>2996b11f-e5b2-d00f-281b-03582683cd49</TransRefGUID> ");
    strBuffer.append("<TransType tc=\"BF9892Retrieve\">BF9892Retrieve</TransType>");
    strBuffer.append("<TransExeDate>2006-05-31</TransExeDate> ");
    strBuffer.append("<TransExeTime>09:00:00-05:00</TransExeTime> ");
    strBuffer.append("<OLifE> ");
    strBuffer.append("<MirCommonFields> ");
    strBuffer.append("<MirPolId>");
    strBuffer.append("<MirPolIdBase>");
    strBuffer.append(PolicyNo);
    strBuffer.append("</MirPolIdBase>");
    strBuffer.append("<MirPolIdSfx>");
    strBuffer.append(suffix);
    strBuffer.append("</MirPolIdSfx>");
    strBuffer.append("</MirPolId>");
    strBuffer.append("</MirCommonFields>");
    strBuffer.append("</OLifE>");
    strBuffer.append("</TXLifeRequest> ");
    strBuffer.append("</TXLife> ");

    try {
      StringBuffer sb = new StringBuffer();
      sb.append("BF9892Retrieve - Requirements and KO_PolNo-");
      sb.append(strMirPolIdBase);
      hs_result = invokeIngenium.submit(strBuffer.toString(), sb.toString());
      if (printParsedXML) {
        Object strkeys[] = hs_result.keySet().toArray();

        for (int i = 0; i < strkeys.length; i++) {
          ArrayList al_result = (ArrayList) hs_result.get(strkeys[i]);
          TheLogger.info(strkeys[i] + ": ");
          for (int j = 0; j < al_result.size(); j++)
            TheLogger.info("\t" + al_result.get(j));
        }
      }
      TheLogger.debug("BF1191 was called using policy no");
    } catch (Exception ex) {
      TheLogger.error(ex.getMessage());
    }
    return hs_result;
  }

  public Object BF1191_Append(String Authenticate[], String strMirPolIdBase, String ReqtId, String reqStat, String strNewIndexNo) {
    HashMap hs_result = null;
    String PolicyNo = "";
    String suffix = "";
    if (strMirPolIdBase.length() > 9) {
      PolicyNo = strMirPolIdBase.substring(0, 9);
      suffix = "" + strMirPolIdBase.charAt(9);
    } else {
      PolicyNo = strMirPolIdBase;
    }
    StringBuffer strBuffer = new StringBuffer();
    strBuffer.append(createLogin(Authenticate));
    strBuffer.append("<TXLifeRequest>");
    strBuffer.append("<TransRefGUID>2996b11f-e5b2-d00f-281b-03582683cd49</TransRefGUID> ");
    strBuffer.append("<TransType tc=\"RequirementsCreate\">RequirementsCreate</TransType>");
    strBuffer.append("<TransExeDate>2006-05-31</TransExeDate> ");
    strBuffer.append("<TransExeTime>09:00:00-05:00</TransExeTime> ");
    strBuffer.append("<OLifE> ");
    strBuffer.append("<RequirementData> ");
    strBuffer.append("<MirCommonFields> ");
    strBuffer.append("<MirCmpltnessInd>C</MirCmpltnessInd> ");
    strBuffer.append("<MirPolId>");
    strBuffer.append("<MirPolIdBase>");
    strBuffer.append(PolicyNo);
    strBuffer.append("</MirPolIdBase>");
    strBuffer.append("<MirPolIdSfx>");
    strBuffer.append(suffix);
    strBuffer.append("</MirPolIdSfx>");
    strBuffer.append("</MirPolId>");
    strBuffer.append("<MirReqirId>");
    strBuffer.append(ReqtId);
    strBuffer.append("</MirReqirId>");
    strBuffer.append("<MirCpreqStatCd>");
    strBuffer.append(reqStat);
    strBuffer.append("</MirCpreqStatCd>");
    strBuffer.append("<MirIndexNum>");
    strBuffer.append(strNewIndexNo);
    strBuffer.append("</MirIndexNum>");
    strBuffer.append("</MirCommonFields>");
    strBuffer.append("</RequirementData>");
    strBuffer.append("</OLifE>");
    strBuffer.append("</TXLifeRequest> ");
    strBuffer.append("</TXLife> ");

    try {
      StringBuffer sb = new StringBuffer();
      sb.append("BF1191 - RequirementsCreate_PolNo-");
      sb.append(strMirPolIdBase);
      hs_result = invokeIngenium.submit(strBuffer.toString(), sb.toString());
      if (printParsedXML) {
        Object strkeys[] = hs_result.keySet().toArray();

        for (int i = 0; i < strkeys.length; i++) {
          ArrayList al_result = (ArrayList) hs_result.get(strkeys[i]);
          TheLogger.info(strkeys[i] + ": ");
          for (int j = 0; j < al_result.size(); j++)
            TheLogger.info("\t" + al_result.get(j));
        }
      }
      TheLogger.debug("BF1191 was called using policy no");
    } catch (Exception ex) {
      TheLogger.error(ex.getMessage());
    }
    return hs_result;
  }

  public Object BF1191_Append(String Authenticate[], String strMirPolIdBase, String ReqtId, String reqStat) {
    HashMap hs_result = null;
    String PolicyNo = "";
    String suffix = "";
    if (strMirPolIdBase.length() > 9) {
      PolicyNo = strMirPolIdBase.substring(0, 9);
      suffix = "" + strMirPolIdBase.charAt(9);
    } else {
      PolicyNo = strMirPolIdBase;
    }
    StringBuffer strBuffer = new StringBuffer();
    strBuffer.append(createLogin(Authenticate));
    strBuffer.append("<TXLifeRequest>");
    strBuffer.append("<TransRefGUID>2996b11f-e5b2-d00f-281b-03582683cd49</TransRefGUID> ");
    strBuffer.append("<TransType tc=\"RequirementsCreate\">RequirementsCreate</TransType>");
    strBuffer.append("<TransExeDate>2006-05-31</TransExeDate> ");
    strBuffer.append("<TransExeTime>09:00:00-05:00</TransExeTime> ");
    strBuffer.append("<OLifE> ");
    strBuffer.append("<RequirementData> ");
    strBuffer.append("<MirCommonFields> ");
    strBuffer.append("<MirCmpltnessInd>C</MirCmpltnessInd> ");
    strBuffer.append("<MirPolId>");
    strBuffer.append("<MirPolIdBase>");
    strBuffer.append(PolicyNo);
    strBuffer.append("</MirPolIdBase>");
    strBuffer.append("<MirPolIdSfx>");
    strBuffer.append(suffix);
    strBuffer.append("</MirPolIdSfx>");
    strBuffer.append("</MirPolId>");
    strBuffer.append("<MirReqirId>");
    strBuffer.append(ReqtId);
    strBuffer.append("</MirReqirId>");
    strBuffer.append("<MirCpreqStatCd>");
    strBuffer.append(reqStat);
    strBuffer.append("</MirCpreqStatCd>");
    strBuffer.append("</MirCommonFields>");
    strBuffer.append("</RequirementData>");
    strBuffer.append("</OLifE>");
    strBuffer.append("</TXLifeRequest> ");
    strBuffer.append("</TXLife> ");

    try {
      StringBuffer sb = new StringBuffer();
      sb.append("BF1191 - RequirementsCreate_PolNo-");
      sb.append(strMirPolIdBase);
      hs_result = invokeIngenium.submit(strBuffer.toString(), sb.toString());
      if (printParsedXML) {
        Object strkeys[] = hs_result.keySet().toArray();

        for (int i = 0; i < strkeys.length; i++) {
          ArrayList al_result = (ArrayList) hs_result.get(strkeys[i]);
          TheLogger.info(strkeys[i] + ": ");
          for (int j = 0; j < al_result.size(); j++)
            TheLogger.info("\t" + al_result.get(j));
        }
      }
      TheLogger.debug("BF1191 was called using policy no");
    } catch (Exception ex) {
      TheLogger.error(ex.getMessage());
    }
    return hs_result;
  }

  /**
   * Handles the BF8000 business requests
   * retrieves the main Policy Details
   * <p>
   * P-6007-11
   **/
  public Object BF8000_Append(String Authenticate[], String strPolicyId) {
    HashMap hs_result = null;
    String PolicyNo = "";
    String suffix = "";
    if (strPolicyId.length() > 9) {
      PolicyNo = strPolicyId.substring(0, 9);
      suffix = "" + strPolicyId.charAt(9);
    } else
      PolicyNo = strPolicyId;
    StringBuffer strBuffer = new StringBuffer();
    strBuffer.append(createLogin(Authenticate));
    strBuffer.append("<TXLifeRequest>");
    strBuffer.append("<TransRefGUID>2996b11f-e5b2-d00f-281b-03582683cd49</TransRefGUID> ");
    strBuffer.append("<TransType tc=\"PolicyInquiry\">PolicyInquiry</TransType>");
    strBuffer.append("<TransExeDate>");
    strBuffer.append(exeDate);
    strBuffer.append("</TransExeDate> ");
    strBuffer.append("<TransExeTime>");
    strBuffer.append(exeTime);
    strBuffer.append("</TransExeTime> ");
    strBuffer.append("<OLifE> ");
    strBuffer.append("<PolicyData> ");
    strBuffer.append("<MirPolId> ");
    strBuffer.append("<MirPolIdBase>");
    strBuffer.append(PolicyNo);
    strBuffer.append("</MirPolIdBase>");
    strBuffer.append("<MirPolIdSfx>");
    strBuffer.append(suffix);
    strBuffer.append("</MirPolIdSfx>");
    strBuffer.append("</MirPolId> ");
    strBuffer.append("<SrcSystem>WMS</SrcSystem>");
    strBuffer.append("</PolicyData> ");
    strBuffer.append("</OLifE> ");
    strBuffer.append("</TXLifeRequest> ");
    strBuffer.append("</TXLife> ");
    try {
      StringBuffer sb = new StringBuffer();
      sb.append("BF8000 - PolicyInquiry_PolNo-");
      sb.append(strPolicyId);
      hs_result = invokeIngenium.submit(strBuffer.toString(), sb.toString());
//      if (printParsedXML) {
//        Object strkeys[] = hs_result.keySet().toArray();
//        for (int i = 0; i < strkeys.length; i++) {
//          ArrayList al_result = (ArrayList) hs_result.get(strkeys[i]);
////          TheLogger.info(strkeys[i] + ": ");
////          for (int j = 0; j < al_result.size(); j++)
////            TheLogger.info("\t" + al_result.get(j));
//        }
//      }
//      TheLogger.debug("BF8000 was called");
    } catch (Exception ex) {
//      TheLogger.error(ex.getMessage());
    }
    return hs_result;
  }

  /**
   * Handles the BF6983 business requests
   * list the coverage of a specific policy
   **/

  public Object BF6983_Append(String Authenticate[], String strPolicyId) {
    System.out.println(strPolicyId);
    HashMap hs_result = null;
    String PolicyNo = "";
    String suffix = "";
    if (strPolicyId.length() > 9) {
      PolicyNo = strPolicyId.substring(0, 9);
      suffix = "" + strPolicyId.charAt(9);
    } else
      PolicyNo = strPolicyId;
    StringBuffer strBuffer = new StringBuffer();
    strBuffer.append(createLogin(Authenticate));
    strBuffer.append("<TXLifeRequest>");
    strBuffer.append("<TransRefGUID>2996b11f-e5b2-d00f-281b-03582683cd49</TransRefGUID> ");
    strBuffer.append("<TransType tc=\"InquiryOtherValues\">InquiryOtherValues</TransType>");
    strBuffer.append("<TransExeDate>2006-05-31</TransExeDate> ");
    strBuffer.append("<TransExeTime>09:00:00-05:00</TransExeTime> ");
    strBuffer.append("<OLifE>");
    strBuffer.append("<PolicyData>");
    strBuffer.append("<MirCommonFields>");
    strBuffer.append("<MirPolId>");
    strBuffer.append("<MirPolIdBase>");
    strBuffer.append(PolicyNo);
    strBuffer.append("</MirPolIdBase>");
    strBuffer.append("<MirPolIdSfx>");
    strBuffer.append(suffix);
    strBuffer.append("</MirPolIdSfx>");
    strBuffer.append("</MirPolId>");
    strBuffer.append("</MirCommonFields>");
    strBuffer.append("</PolicyData>");
    strBuffer.append("</OLifE>");
    strBuffer.append("</TXLifeRequest>");
    strBuffer.append("</TXLife>");
    try {
      StringBuffer sb = new StringBuffer();
      sb.append("BF6983 - InquiryOtherValues_PolNo-");
      sb.append(strPolicyId);
      hs_result = invokeIngenium.submit(strBuffer.toString(), sb.toString());
      if (printParsedXML) {
        Object strkeys[] = hs_result.keySet().toArray();
        for (int i = 0; i < strkeys.length; i++) {
          ArrayList al_result = (ArrayList) hs_result.get(strkeys[i]);
          TheLogger.info(strkeys[i] + ": ");
          for (int j = 0; j < al_result.size(); j++)
            TheLogger.info("\t" + al_result.get(j));
        }
      }
      TheLogger.debug("BF6983 was called");
    } catch (Exception ex) {
      TheLogger.error(ex.getMessage());
    }
    return hs_result;
  }

  /**
   * Handles the BF1220 business requests
   * Retrieves the Client Information
   **/
  //ClientInquiry
  public Object BF1220C_Append(String Authenticate[], String strClientID) {
    HashMap hs_result = null;
    String PolicyNo = "";
    String suffix = "";

    StringBuffer strBuffer = new StringBuffer();
    strBuffer.append(createLogin(Authenticate));
    strBuffer.append("<TXLifeRequest>");
    strBuffer.append("<TransRefGUID>2996b11f-e5b2-d00f-281b-03582683cd49</TransRefGUID> ");
    strBuffer.append("<TransType tc=\"ClientInquiry\">ClientInquiry</TransType>");
    strBuffer.append("<TransExeDate>");
    strBuffer.append(exeDate);
    strBuffer.append("</TransExeDate> ");
    strBuffer.append("<TransExeTime>");
    strBuffer.append(exeTime);
    strBuffer.append("</TransExeTime> ");
    strBuffer.append("<OLifE>");
    strBuffer.append("<ReturnCds/>");
    strBuffer.append("<ClientData>");
    strBuffer.append("<SrcSystem>WMS</SrcSystem>");
    strBuffer.append("<MirCliInfo>");
    strBuffer.append("<MirCliId>");
    strBuffer.append(strClientID);
    strBuffer.append("</MirCliId>");
    strBuffer.append("</MirCliInfo>");
    strBuffer.append("</ClientData>");
    strBuffer.append("</OLifE>");
    strBuffer.append("</TXLifeRequest>");
    strBuffer.append("</TXLife> ");

//        strBuffer.append("<MirCliInfo>");
//        strBuffer.append("<MirCliId>");
//        strBuffer.append(strClientID);
//        strBuffer.append("</MirCliId>");
//        strBuffer.append("</MirCliInfo>");
//        strBuffer.append("<SrcSystem>WMS</SrcSystem>");
//        strBuffer.append("</ClientData>");
//        strBuffer.append("</OLifE>");
//        strBuffer.append("</TXLifeRequest>");
//        strBuffer.append("</TXLife> ");
    try {
      StringBuffer sb = new StringBuffer();
      sb.append("BF1220 - ClientInquiry_CliId-");
      sb.append(strClientID);
      hs_result = invokeIngenium.submit(strBuffer.toString(), sb.toString());
      if (printParsedXML) {
        Object strkeys[] = hs_result.keySet().toArray();
        for (int i = 0; i < strkeys.length; i++) {
          ArrayList al_result = (ArrayList) hs_result.get(strkeys[i]);
          TheLogger.info(strkeys[i] + ": ");
          for (int j = 0; j < al_result.size(); j++)
            TheLogger.info("\t" + al_result.get(j));
        }
      }
      TheLogger.debug("BF1220 was called using client no");
    } catch (Exception ex) {
      TheLogger.error(ex.getMessage());
    }
    return hs_result;
  }

  /**
   * Handles the BF6925 business requests
   * list the coverage of a specific policy
   **/
  public Object BF6925_Append(String Authenticate[], String strPolicyId, String CoverageNum) {
    HashMap hs_result = null;
    String PolicyNo = "";
    String suffix = "";
    if (strPolicyId.length() > 9) {
      PolicyNo = strPolicyId.substring(0, 9);
      suffix = "" + strPolicyId.charAt(9);
    } else
      PolicyNo = strPolicyId;
    StringBuffer strBuffer = new StringBuffer();
    strBuffer.append(createLogin(Authenticate));
    strBuffer.append("<TXLifeRequest>");
    strBuffer.append("<TransRefGUID>2996b11f-e5b2-d00f-281b-03582683cd49</TransRefGUID> ");
    strBuffer.append("<TransType tc=\"CoverageList\">CoverageList</TransType>");
    strBuffer.append("<TransExeDate>2006-05-31</TransExeDate> ");
    strBuffer.append("<TransExeTime>09:00:00-05:00</TransExeTime> ");
    strBuffer.append("<OLifE>");
    strBuffer.append("<CoverageData>");
    strBuffer.append("<MirCommonFields>");
    strBuffer.append("<MirCvgNum>");
    strBuffer.append(CoverageNum);
    strBuffer.append("</MirCvgNum>");
    strBuffer.append("<MirPolId> ");
    strBuffer.append("<MirPolIdBase>");
    strBuffer.append(PolicyNo);
    strBuffer.append("</MirPolIdBase>");
    strBuffer.append("<MirPolIdSfx>");
    strBuffer.append(suffix);
    strBuffer.append("</MirPolIdSfx>");
    strBuffer.append("</MirPolId>");
    strBuffer.append("</MirCommonFields>");
    strBuffer.append("</CoverageData>");
    strBuffer.append("</OLifE>");
    strBuffer.append("</TXLifeRequest>");
    strBuffer.append("</TXLife>");
    try {
      StringBuffer sb = new StringBuffer();
      sb.append("BF6925 - CoverageList_PolNo-");
      sb.append(strPolicyId);
      hs_result = invokeIngenium.submit(strBuffer.toString(), sb.toString());
      if (printParsedXML) {
        Object strkeys[] = hs_result.keySet().toArray();
        for (int i = 0; i < strkeys.length; i++) {
          ArrayList al_result = (ArrayList) hs_result.get(strkeys[i]);
          TheLogger.info(strkeys[i] + ": ");
          for (int j = 0; j < al_result.size(); j++)
            TheLogger.info("\t" + al_result.get(j));
        }
      }
      TheLogger.debug("BF6925 was called");
    } catch (Exception ex) {
      TheLogger.error(ex.getMessage());
    }
    return hs_result;
  }


  /**
   * Handles the BF1054 business requests
   * retrieves list of Policy Beneficiaries
   **/
  public Object BF8020_Append(String Authenticate[], String strPolicyId, String CoverageNo) {
    HashMap hs_result = null;
    String PolicyNo = "";
    String suffix = "";
    if (strPolicyId.length() > 9) {
      PolicyNo = strPolicyId.substring(0, 9);
      suffix = "" + strPolicyId.charAt(9);
    } else {
      PolicyNo = strPolicyId;
    }
    StringBuffer strBuffer = new StringBuffer();
    strBuffer.append(createLogin(Authenticate));
    strBuffer.append("<TXLifeRequest>");
    strBuffer.append("<TransRefGUID>2996b11f-e5b2-d00f-281b-03582683cd49</TransRefGUID> ");
    strBuffer.append("<TransType tc=\"CoverageInquiry\">CoverageInquiry</TransType>");
    strBuffer.append("<TransExeDate>2006-05-31</TransExeDate> ");
    strBuffer.append("<TransExeTime>09:00:00-05:00</TransExeTime> ");
    strBuffer.append("<OLifE>");
    strBuffer.append("<CoverageData>");
    strBuffer.append("<MirPolId>");
    strBuffer.append("<MirPolIdBase>");
    strBuffer.append(PolicyNo);
    strBuffer.append("</MirPolIdBase>");
    strBuffer.append("<MirPolIdSfx>");
    strBuffer.append(suffix);
    strBuffer.append("</MirPolIdSfx>");
    strBuffer.append("</MirPolId>");
    strBuffer.append("<MirCvgNum>");
    strBuffer.append(CoverageNo);
    strBuffer.append("</MirCvgNum>");
    strBuffer.append("</CoverageData>");
    strBuffer.append("</OLifE>");
    strBuffer.append("</TXLifeRequest>");
    strBuffer.append("</TXLife>");
    try {
      StringBuffer sb = new StringBuffer();
      sb.append("BF8020 - CoverageInquiry_PolNo-");
      sb.append(strPolicyId);
      hs_result = invokeIngenium.submit(strBuffer.toString(), sb.toString());
      if (printParsedXML) {
        Object strkeys[] = hs_result.keySet().toArray();
        for (int i = 0; i < strkeys.length; i++) {
          ArrayList al_result = (ArrayList) hs_result.get(strkeys[i]);
          TheLogger.info(strkeys[i] + ": ");
          for (int j = 0; j < al_result.size(); j++)
            TheLogger.info("\t" + al_result.get(j));
        }
      }
      TheLogger.debug("BF1054 was called");
    } catch (Exception ex) {
      TheLogger.error(ex.getMessage());
    }
    return hs_result;
  }

  /**
   * Handles the BF6945 business requests
   * retrieves Beneficiary Inquiry Details
   **/
  public Object BF6945_Append(String Authenticate[], String strPolicyId, String strCoverageNo) {
    HashMap hs_result = null;
    String PolicyNo = "";
    String suffix = "";
    if (strPolicyId.length() > 9) {
      PolicyNo = strPolicyId.substring(0, 9);
      suffix = "" + strPolicyId.charAt(9);
    } else
      PolicyNo = strPolicyId;
    StringBuffer strBuffer = new StringBuffer();
    strBuffer.append(createLogin(Authenticate));
    strBuffer.append("<TXLifeRequest>");
    strBuffer.append("<TransRefGUID>2996b11f-e5b2-d00f-281b-03582683cd49</TransRefGUID> ");
    strBuffer.append("<TransType tc=\"InquiryBeneficiary\">InquiryBeneficiary</TransType>");
    strBuffer.append("<TransExeDate>2006-05-31</TransExeDate> ");
    strBuffer.append("<TransExeTime>09:00:00-05:00</TransExeTime> ");
    strBuffer.append("<OLifE>");
    strBuffer.append("<BeneficiaryData>");
    strBuffer.append("<MirCvgNum>");
    strBuffer.append(strCoverageNo);
    strBuffer.append("</MirCvgNum>");
    strBuffer.append("<MirPolId>");
    strBuffer.append("<MirPolIdBase>");
    strBuffer.append(PolicyNo);
    strBuffer.append("</MirPolIdBase>");
    strBuffer.append("<MirPolIdSfx>");
    strBuffer.append(suffix);
    strBuffer.append("</MirPolIdSfx>");
    strBuffer.append("</MirPolId>");
    strBuffer.append("</BeneficiaryData>");
    strBuffer.append("</OLifE>");
    strBuffer.append("</TXLifeRequest>");
    strBuffer.append("</TXLife>");
    try {
      StringBuffer sb = new StringBuffer();
      sb.append("BF6945 - InquiryBeneficiary_PolNo-");
      sb.append(strPolicyId);
      hs_result = invokeIngenium.submit(strBuffer.toString(), sb.toString());
      if (printParsedXML) {
        Object strkeys[] = hs_result.keySet().toArray();
        for (int i = 0; i < strkeys.length; i++) {
          ArrayList al_result = (ArrayList) hs_result.get(strkeys[i]);
          TheLogger.info(strkeys[i] + ": ");
          for (int j = 0; j < al_result.size(); j++)
            TheLogger.info("\t" + al_result.get(j));
        }
      }
      TheLogger.debug("BF6945 was called");
    } catch (Exception ex) {
      TheLogger.error(ex.getMessage());
    }
    return hs_result;
  }

  /**
   * Handles the BF0592 business requests
   * performs Auto-App/Clear Case Processing
   **/
  //CLEAR CASE SUBMIT
  public Object BF0592_Append(String Authenticate[], String strPolicyId, String ClientID) {
    HashMap hs_result = null;
    String PolicyNo = "";
    String suffix = "";
    if (strPolicyId.length() > 9) {
      PolicyNo = strPolicyId.substring(0, 9);
      suffix = "" + strPolicyId.charAt(9);
    } else
      PolicyNo = strPolicyId;
    StringBuffer strBuffer = new StringBuffer();
    strBuffer.append(createLogin(Authenticate));
    strBuffer.append("<TXLifeRequest>");
    strBuffer.append("<TransRefGUID>2996b11f-e5b2-d00f-281b-03582683cd49</TransRefGUID> ");
    strBuffer.append("<TransType tc=\"ClearCaseSubmit\">ClearCaseSubmit</TransType>");
    strBuffer.append("<TransExeDate>2006-05-31</TransExeDate> ");
    strBuffer.append("<TransExeTime>09:00:00-05:00</TransExeTime> ");
    strBuffer.append("<OLifE>");
    strBuffer.append("<MirCommonFields>");

    strBuffer.append("<MirCliIdG>");
    strBuffer.append("<MirCliIdT>");
    strBuffer.append(ClientID);
    strBuffer.append("</MirCliIdT>");
    strBuffer.append("</MirCliIdG>");

    strBuffer.append("<MirPolId>");
    strBuffer.append("<MirPolIdBase>");
    strBuffer.append(PolicyNo);
    strBuffer.append("</MirPolIdBase>");
    strBuffer.append("<MirPolIdSfx>");
    strBuffer.append(suffix);
    strBuffer.append("</MirPolIdSfx>");
    strBuffer.append("</MirPolId>");
    strBuffer.append("<MirDvUwPrcesCd>D</MirDvUwPrcesCd>");
    strBuffer.append("</MirCommonFields>");
    strBuffer.append("</OLifE>");
    strBuffer.append("</TXLifeRequest> ");
    strBuffer.append("</TXLife>");
    System.out.println(strBuffer.toString());
    try {
      StringBuffer sb = new StringBuffer();
      sb.append("BF0592 - ClearCaseSubmit_PolNo-");
      sb.append(strPolicyId);
      hs_result = invokeIngenium.submit(strBuffer.toString(), sb.toString());
      if (printParsedXML) {
        Object strkeys[] = hs_result.keySet().toArray();
        for (int i = 0; i < strkeys.length; i++) {
          ArrayList al_result = (ArrayList) hs_result.get(strkeys[i]);
          TheLogger.info(strkeys[i] + ": ");
          for (int j = 0; j < al_result.size(); j++)
            TheLogger.info("\t" + al_result.get(j));
        }
      }
      TheLogger.debug("BF0592 was called");
    } catch (Exception ex) {
      TheLogger.error(ex.getMessage());
    }
    return hs_result;
  }

  /**
   * Handles the BF1194 business requests
   * list the requirements for a given policy
   **/
  public Object CLEARCASE(String Authenticate[], String strPolicyId) {

    HashMap hs_result = null;
    String PolicyNo = "";
    String suffix = "";
    if (strPolicyId.length() > 9) {
      PolicyNo = strPolicyId.substring(0, 9);
      suffix = "" + strPolicyId.charAt(9);
    } else
      PolicyNo = strPolicyId;

    StringBuffer strBuffer = new StringBuffer();
    strBuffer.append(createLogin(Authenticate));
    strBuffer.append("<TXLifeRequest>");
    strBuffer.append("<TransRefGUID>2996b11f-e5b2-d00f-281b-03582683cd49</TransRefGUID> ");
    strBuffer.append("<TransType tc=\"ClearCaseMessages\">ClearCaseMessages</TransType>");
    strBuffer.append("<TransExeDate>2006-05-31</TransExeDate> ");
    strBuffer.append("<TransExeTime>09:00:00-05:00</TransExeTime> ");
    strBuffer.append("<OLifE>");
    strBuffer.append("<MirPolId>");
    strBuffer.append("<MirPolIdBase>");
    strBuffer.append(PolicyNo);
    strBuffer.append("</MirPolIdBase>");
    strBuffer.append("<MirPolIdSfx>");
    strBuffer.append(suffix);
    strBuffer.append("</MirPolIdSfx>");
    strBuffer.append("</MirPolId>");
    strBuffer.append("</OLifE>");
    strBuffer.append("</TXLifeRequest>");
    strBuffer.append("</TXLife>");
    try {
      StringBuffer sb = new StringBuffer();
      sb.append("CLEARCASE - ClearCaseMessages_PolNo-");
      sb.append(strPolicyId);
      hs_result = invokeIngenium.submit(strBuffer.toString(), sb.toString());
      if (printParsedXML) {
        Object strkeys[] = hs_result.keySet().toArray();
        for (int i = 0; i < strkeys.length; i++) {
          ArrayList al_result = (ArrayList) hs_result.get(strkeys[i]);
          TheLogger.info(strkeys[i] + ": ");
          for (int j = 0; j < al_result.size(); j++)
            TheLogger.info("\t" + al_result.get(j));
        }
      }
      TheLogger.debug("ClearCase was called");
    } catch (Exception ex) {
      ex.printStackTrace();
      //TheLogger.error(ex.getMessage());
    }
    return hs_result;
  }

  /**
   * Handles the BF1194 business requests
   * list the requirements for a given policy
   **/

  public Object BF1194C_Append(String Authenticate[], String strClientId) {
    HashMap hs_result = null;
    StringBuffer strBuffer = new StringBuffer();
    strBuffer.append(createLogin(Authenticate));
    strBuffer.append("<TXLifeRequest>");
    strBuffer.append("<TransRefGUID>2996b11f-e5b2-d00f-281b-03582683cd49</TransRefGUID> ");
    strBuffer.append("<TransType tc=\"RequirementsList\">RequirementsList</TransType>");
    strBuffer.append("<TransExeDate>2006-05-31</TransExeDate> ");
    strBuffer.append("<TransExeTime>09:00:00-05:00</TransExeTime> ");
    strBuffer.append("<OLifE>");
    strBuffer.append("<RequirementData>");
    strBuffer.append("<MirCommonFields>");
    strBuffer.append("<MirCliId>");
    strBuffer.append(strClientId);
    strBuffer.append("</MirCliId>");
    strBuffer.append("<MirPolId />");
    strBuffer.append("</MirCommonFields>");
    strBuffer.append("</RequirementData>");
    strBuffer.append("</OLifE>");
    strBuffer.append("</TXLifeRequest>");
    strBuffer.append("</TXLife>");
    try {
      StringBuffer sb = new StringBuffer();
      sb.append("BF1194 - RequirementsList_CliId-");
      sb.append(strClientId);
      hs_result = invokeIngenium.submit(strBuffer.toString(), sb.toString());
      if (printParsedXML) {
        Object strkeys[] = hs_result.keySet().toArray();
        for (int i = 0; i < strkeys.length; i++) {
          ArrayList al_result = (ArrayList) hs_result.get(strkeys[i]);
          TheLogger.info(strkeys[i] + ": ");
          for (int j = 0; j < al_result.size(); j++)
            TheLogger.info("\t" + al_result.get(j));
        }
      }
      TheLogger.debug("BF1194 was called using client no");
    } catch (Exception ex) {
      TheLogger.error(ex.getMessage());
    }
    return hs_result;
  }


  public Object BF1194_Append(String Authenticate[], String strPolicyId) {
    HashMap hs_result = null;
    String PolicyNo = "";
    String suffix = "";
    if (strPolicyId.length() > 9) {
      PolicyNo = strPolicyId.substring(0, 9);
      suffix = "" + strPolicyId.charAt(9);
    } else
      PolicyNo = strPolicyId;

    StringBuffer strBuffer = new StringBuffer();
    strBuffer.append(createLogin(Authenticate));
    strBuffer.append("<TXLifeRequest>");
    strBuffer.append("<TransRefGUID>2996b11f-e5b2-d00f-281b-03582683cd49</TransRefGUID> ");
    strBuffer.append("<TransType tc=\"RequirementsList\">RequirementsList</TransType>");
    strBuffer.append("<TransExeDate>2006-05-31</TransExeDate> ");
    strBuffer.append("<TransExeTime>09:00:00-05:00</TransExeTime> ");
    strBuffer.append("<OLifE>");
    strBuffer.append("<RequirementData>");
    strBuffer.append("<MirCommonFields>");
    strBuffer.append("<MirPolId>");
    strBuffer.append("<MirPolIdBase>");
    strBuffer.append(PolicyNo);
    strBuffer.append("</MirPolIdBase>");
    strBuffer.append("<MirPolIdSfx>");
    strBuffer.append(suffix);
    strBuffer.append("</MirPolIdSfx>");
    strBuffer.append("</MirPolId>");
    strBuffer.append("</MirCommonFields>");
    strBuffer.append("</RequirementData>");
    strBuffer.append("</OLifE>");
    strBuffer.append("</TXLifeRequest>");
    strBuffer.append("</TXLife>");
    try {
      StringBuffer sb = new StringBuffer();
      sb.append("BF1194 - RequirementsList_PolNo-");
      sb.append(strPolicyId);
      hs_result = invokeIngenium.submit(strBuffer.toString(), sb.toString());
      if (printParsedXML) {
        Object strkeys[] = hs_result.keySet().toArray();
        for (int i = 0; i < strkeys.length; i++) {
          ArrayList al_result = (ArrayList) hs_result.get(strkeys[i]);
          TheLogger.info(strkeys[i] + ": ");
          for (int j = 0; j < al_result.size(); j++)
            TheLogger.info("\t" + al_result.get(j));

        }
      }
      TheLogger.debug("BF1194 was called using policy no");
    } catch (Exception ex) {
      TheLogger.error(ex.getMessage());
    }
    return hs_result;
  }


  /**
   * Handles the BF1192 business requests
   * Updates the requirement records (status, image reference #) using PolicyID
   * paramters
   * Authenticate[] = username and password screen
   * strPolicyID = policy ID
   * strSeqNum = sequence number of the requirement to be updated
   * strNewReqStatus = new status of the requirement
   * String strReqId = new Requirements ID
   * strNewIndexNo = new IndexNo
   **/
  public Object BF1192C_Append(String Authenticate[], String strClientId, String strSeqNum, String strNewReqStatus, String strReqId, String strNewIndexNo) {
    HashMap hs_result = null;
    String PolicyNo = "";
    String suffix = "";
    StringBuffer strBuffer = new StringBuffer();
    strBuffer.append(createLogin(Authenticate));
    strBuffer.append("<TXLifeRequest>");
    strBuffer.append("<TransRefGUID>2996b11f-e5b2-d00f-281b-03582683cd49</TransRefGUID> ");
    strBuffer.append("<TransType tc=\"RequirementsUpdate\">RequirementsUpdate</TransType>");
    strBuffer.append("<TransExeDate>2006-05-31</TransExeDate> ");
    strBuffer.append("<TransExeTime>09:00:00-05:00</TransExeTime> ");
    strBuffer.append("<OLifE> ");
    strBuffer.append("<RequirementData> ");
    strBuffer.append("<MirCommonFields> ");
    strBuffer.append("<MirCmpltnessInd>C</MirCmpltnessInd> ");
    strBuffer.append("<MirCliId>");
    strBuffer.append(strClientId);
    strBuffer.append("</MirCliId>");
    strBuffer.append("<MirReqirId>");
    strBuffer.append(strReqId);
    strBuffer.append("</MirReqirId>");
    strBuffer.append("<MirCpreqStatCd>");
    strBuffer.append(strNewReqStatus);
    strBuffer.append("</MirCpreqStatCd>");
    strBuffer.append("<MirCpreqSeqNum>");
    strBuffer.append(strSeqNum);
    strBuffer.append("</MirCpreqSeqNum>");
    strBuffer.append("<MirIndexNum>");
    strBuffer.append(strNewIndexNo);
    strBuffer.append("</MirIndexNum>");

    strBuffer.append("</MirCommonFields>");
    strBuffer.append("</RequirementData>");
    strBuffer.append("</OLifE>");
    strBuffer.append("</TXLifeRequest> ");
    strBuffer.append("</TXLife> ");

    try {
      StringBuffer sb = new StringBuffer();
      sb.append("BF1192 - RequirementsUpdate_CliId-");
      sb.append(strClientId);
      hs_result = invokeIngenium.submit(strBuffer.toString(), sb.toString());
      if (printParsedXML) {
        Object strkeys[] = hs_result.keySet().toArray();
        for (int i = 0; i < strkeys.length; i++) {
          ArrayList al_result = (ArrayList) hs_result.get(strkeys[i]);
          TheLogger.info(strkeys[i] + ": ");
          for (int j = 0; j < al_result.size(); j++)
            TheLogger.info("\t" + al_result.get(j));
        }
      }
      TheLogger.debug("BF1192 was called using client no");
    } catch (Exception ex) {
      TheLogger.error(ex.getMessage());
    }
    return hs_result;
  }


  public Object BF1192_Append(String Authenticate[], String strMirPolIdBase, String strSeqNum, String strNewReqStatus, String strReqId, String strNewIndexNo) {
    HashMap hs_result = null;
    String PolicyNo = "";
    String suffix = "";
    if (strMirPolIdBase.length() > 9) {
      PolicyNo = strMirPolIdBase.substring(0, 9);
      suffix = "" + strMirPolIdBase.charAt(9);
    } else
      PolicyNo = strMirPolIdBase;
    StringBuffer strBuffer = new StringBuffer();
    strBuffer.append(createLogin(Authenticate));
    strBuffer.append("<TXLifeRequest>");
    strBuffer.append("<TransRefGUID>2996b11f-e5b2-d00f-281b-03582683cd49</TransRefGUID> ");
    strBuffer.append("<TransType tc=\"RequirementsUpdate\">RequirementsUpdate</TransType>");
    strBuffer.append("<TransExeDate>2006-05-31</TransExeDate> ");
    strBuffer.append("<TransExeTime>09:00:00-05:00</TransExeTime> ");
    strBuffer.append("<OLifE>");
    strBuffer.append("<RequirementData> ");
    strBuffer.append("<MirCommonFields>");
    strBuffer.append("<MirCmpltnessInd>C</MirCmpltnessInd> ");
    strBuffer.append("<MirPolId>");
    strBuffer.append("<MirPolIdBase>");
    strBuffer.append(PolicyNo);
    strBuffer.append("</MirPolIdBase>");
    strBuffer.append("<MirPolIdSfx>");
    strBuffer.append(suffix);
    strBuffer.append("</MirPolIdSfx>");
    strBuffer.append("</MirPolId>");
    strBuffer.append("<MirReqirId>");
    strBuffer.append(strReqId);
    strBuffer.append("</MirReqirId>");
    strBuffer.append("<MirCpreqStatCd>");
    strBuffer.append(strNewReqStatus);
    strBuffer.append("</MirCpreqStatCd>");
    strBuffer.append("<MirCpreqSeqNum>");
    strBuffer.append(strSeqNum);
    strBuffer.append("</MirCpreqSeqNum>");
    strBuffer.append("<MirIndexNum>");
    strBuffer.append(strNewIndexNo);
    strBuffer.append("</MirIndexNum>");
    strBuffer.append("</MirCommonFields>");
    strBuffer.append("</RequirementData>");
    strBuffer.append("</OLifE>");
    strBuffer.append("</TXLifeRequest> ");
    strBuffer.append("</TXLife> ");
    System.out.println(strBuffer.toString());
    try {
      StringBuffer sb = new StringBuffer();
      sb.append("BF1192 - RequirementsUpdate_PolNo-");
      sb.append(strMirPolIdBase);
      hs_result = invokeIngenium.submit(strBuffer.toString(), sb.toString());
      if (printParsedXML) {
        Object strkeys[] = hs_result.keySet().toArray();
        for (int i = 0; i < strkeys.length; i++) {
          ArrayList al_result = (ArrayList) hs_result.get(strkeys[i]);
          TheLogger.info(strkeys[i] + ": ");
          for (int j = 0; j < al_result.size(); j++)
            TheLogger.info("\t" + al_result.get(j));
        }
      }
      TheLogger.debug("BF1192 was called using policy no");
    } catch (Exception ex) {
      TheLogger.error(ex.getMessage());
    }
    return hs_result;
  }


  public Object getPolicyUnderwritingInquiry(String Authenticate[], String strMirPolIdBase) {
    HashMap hs_result = null;
    String PolicyNo = "";
    String suffix = "";
    StringBuffer strBuffer = new StringBuffer();
    if (strMirPolIdBase.length() > 9) {
      PolicyNo = strMirPolIdBase.substring(0, 9);
      suffix = "" + strMirPolIdBase.charAt(9);
    } else {
      PolicyNo = strMirPolIdBase;
    }
    strBuffer.append(createLogin(Authenticate))
        .append("<TXLifeRequest>")
        .append("<TransRefGUID></TransRefGUID> ")
        .append(" <TransType tc=\"PolicyUnderwritingInquiry\">PolicyUnderwritingInquiry</TransType>")
        .append("<TransExeDate/>")
        .append("<TransExeTime/>")

        .append("<OLifE>")
        .append("<PolicyUnderwritingInquiryData>")
        .append("<MirPolId>")
        .append("<MirPolIdBase>")
        .append(PolicyNo)
        .append("</MirPolIdBase>")
        .append("<MirPolIdSfx>")
        .append(suffix)
        .append("</MirPolIdSfx>")
        .append("</MirPolId>")
        .append("</PolicyUnderwritingInquiryData>")

        .append("</OLifE>")
        .append("</TXLifeRequest>")
        .append("</TXLife>");
    try {
      StringBuffer sb = new StringBuffer();
      sb.append("PolicyUnderwritingInquiry_PolNo-");
      sb.append(strMirPolIdBase);
      hs_result = invokeIngenium.submit(strBuffer.toString(), sb.toString());

      if (printParsedXML) {
        Object strkeys[] = hs_result.keySet().toArray();
        for (int i = 0; i < strkeys.length; i++) {
          ArrayList al_result = (ArrayList) hs_result.get(strkeys[i]);
          TheLogger.info("TheLogger strkeys: " + strkeys[i] + ": ");
					/*for(int j=0;j<al_result.size();j++)
	          			TheLogger.info("\t" + al_result.get(j));*/
        }
      }
      TheLogger.debug("InquiryCoveragePremiums was called using client number");
    } catch (Exception ex) {
      TheLogger.error(ex.getMessage());
    }
    return hs_result;
  }

  public Object getPolicyUnderwritingUpdate(String Authenticate[], String strMirPolIdBase, String treatyTypeCode) {
    HashMap hs_result = null;
    String PolicyNo = "";
    String suffix = "";
    StringBuffer strBuffer = new StringBuffer();
    if (strMirPolIdBase.length() > 9) {
      PolicyNo = strMirPolIdBase.substring(0, 9);
      suffix = "" + strMirPolIdBase.charAt(9);
    } else {
      PolicyNo = strMirPolIdBase;
    }
    strBuffer.append(createLogin(Authenticate))
        .append("<TXLifeRequest>")
        .append("<TransRefGUID></TransRefGUID> ")
        .append("<TransType tc=\"PolicyUnderwritingUpdate\">PolicyUnderwritingUpdate</TransType> ")
        .append("<TransExeDate/>")
        .append("<TransExeTime/>")

        .append("<OLifE>")
        .append("<PolicyUnderwritingUpdateData>")
        .append("<MirPolId>")
        .append("<MirPolIdBase>")
        .append(PolicyNo)
        .append("</MirPolIdBase>")
        .append("<MirPolIdSfx>")
        .append(suffix)
        .append("</MirPolIdSfx>")
        .append("</MirPolId>")
        .append("<MirDvOwnCliNm />")
        .append("<MirSbsdryCoId />")
        .append("<MirPolCstatCd />")
        .append("<MirEvdnEvalTypCd>N</MirEvdnEvalTypCd>")
        .append("<MirPolReinsCd />")
        .append("<MirPolReinsCo>PHILAM</MirPolReinsCo>")
        .append("<MirPolReinsTrtyTypCd>")
        .append(treatyTypeCode)
        .append("</MirPolReinsTrtyTypCd>")
        .append("<MirPolReinsAmt>")
        .append("<MirPolReinsFaceAmt />")
        .append("<MirPolReinsAdbFaceAmt />")
        .append("<MirPolReinsTdbFaceAmt />")
        .append("<MirPolReinsSndryAmt />")
        .append("</MirPolReinsAmt>")
        .append("<MirSpclMedicTstInd>")
        .append("<MirSpclTstUriInd>N</MirSpclTstUriInd>")
        .append("<MirSpclTstCotInd>N</MirSpclTstCotInd>")
        .append("<MirSpclTstBldInd>N</MirSpclTstBldInd>")
        .append("<MirSpclTstXryInd>N</MirSpclTstXryInd>")
        .append("<MirSpclTstEcgInd>N</MirSpclTstEcgInd>")
        .append("<MirSpclTstTrdInd>N</MirSpclTstTrdInd>")
        .append("</MirSpclMedicTstInd>")
        .append("</PolicyUnderwritingUpdateData>")
        .append("</OLifE>")

        .append("</TXLifeRequest>")
        .append("</TXLife>");
    try {
      StringBuffer sb = new StringBuffer();
      sb.append("PolicyUnderwritingUpdate_PolNo-");
      sb.append(strMirPolIdBase);
      hs_result = invokeIngenium.submit(strBuffer.toString(), sb.toString());

      if (printParsedXML) {
        Object strkeys[] = hs_result.keySet().toArray();
        for (int i = 0; i < strkeys.length; i++) {
          ArrayList al_result = (ArrayList) hs_result.get(strkeys[i]);
          TheLogger.info("TheLogger strkeys: " + strkeys[i] + ": ");
					/*for(int j=0;j<al_result.size();j++)
	          			TheLogger.info("\t" + al_result.get(j));*/
        }
      }
      TheLogger.debug("InquiryCoveragePremiums was called using client number");
    } catch (Exception ex) {
      TheLogger.error(ex.getMessage());
    }
    return hs_result;
  }

  public Object BF0592Select(String Authenticate[], String strPolicyId, String ClientID) {
    HashMap hs_result = null;
    String PolicyNo = "";
    String suffix = "";
    if (strPolicyId.length() > 9) {
      PolicyNo = strPolicyId.substring(0, 9);
      suffix = "" + strPolicyId.charAt(9);
    } else
      PolicyNo = strPolicyId;
    StringBuffer strBuffer = new StringBuffer();
    strBuffer.append(createLogin(Authenticate));
    strBuffer.append("<TXLifeRequest>");
    strBuffer.append("<TransRefGUID>2996b11f-e5b2-d00f-281b-03582683cd49</TransRefGUID> ");
    strBuffer.append("<TransType tc=\"BF0592Select\">BF0592Select</TransType>");
    strBuffer.append("<TransExeDate/> ");
    strBuffer.append("<TransExeTime/> ");
    strBuffer.append("<OLifE>");

    strBuffer.append("<MirCommonFields>");
    strBuffer.append("<MirPolMibSignCd />");
    strBuffer.append("<MirCliCmpltCcasIndG>");
    strBuffer.append("<MirCliCmpltCcasIndT />");
    strBuffer.append("<MirCliCmpltCcasIndT />");
    strBuffer.append("<MirCliCmpltCcasIndT />");
    strBuffer.append("<MirCliCmpltCcasIndT />");
    strBuffer.append("<MirCliCmpltCcasIndT />");
    strBuffer.append("<MirCliCmpltCcasIndT />");
    strBuffer.append("<MirCliCmpltCcasIndT />");
    strBuffer.append("<MirCliCmpltCcasIndT />");
    strBuffer.append("<MirCliCmpltCcasIndT />");
    strBuffer.append("<MirCliCmpltCcasIndT />");
    strBuffer.append("<MirCliCmpltCcasIndT />");
    strBuffer.append("</MirCliCmpltCcasIndG>");
    strBuffer.append("<MirCliIdG>");
    strBuffer.append("<MirCliIdT>");
    strBuffer.append(ClientID);
    strBuffer.append("</MirCliIdT>");
    strBuffer.append("<MirCliIdT />");
    strBuffer.append("<MirCliIdT />");
    strBuffer.append("<MirCliIdT />");
    strBuffer.append("<MirCliIdT />");
    strBuffer.append("<MirCliIdT />");
    strBuffer.append("<MirCliIdT />");
    strBuffer.append("<MirCliIdT />");
    strBuffer.append("<MirCliIdT />");
    strBuffer.append("<MirCliIdT />");
    strBuffer.append("<MirCliIdT />");
    strBuffer.append("</MirCliIdG>");
    strBuffer.append("<MirCliBthDtG>");
    strBuffer.append("<MirCliBthDtT />");
    strBuffer.append("<MirCliBthDtT />");
    strBuffer.append("<MirCliBthDtT />");
    strBuffer.append("<MirCliBthDtT />");
    strBuffer.append("<MirCliBthDtT />");
    strBuffer.append("<MirCliBthDtT />");
    strBuffer.append("<MirCliBthDtT />");
    strBuffer.append("<MirCliBthDtT />");
    strBuffer.append("<MirCliBthDtT />");
    strBuffer.append("<MirCliBthDtT />");
    strBuffer.append("<MirCliBthDtT />");
    strBuffer.append("</MirCliBthDtG>");
    strBuffer.append("<MirDvInsrdCliNmG>");
    strBuffer.append("<MirDvInsrdCliNmT />");
    strBuffer.append("<MirDvInsrdCliNmT />");
    strBuffer.append("<MirDvInsrdCliNmT />");
    strBuffer.append("<MirDvInsrdCliNmT />");
    strBuffer.append("<MirDvInsrdCliNmT />");
    strBuffer.append("<MirDvInsrdCliNmT />");
    strBuffer.append("<MirDvInsrdCliNmT />");
    strBuffer.append("<MirDvInsrdCliNmT />");
    strBuffer.append("<MirDvInsrdCliNmT />");
    strBuffer.append("<MirDvInsrdCliNmT />");
    strBuffer.append("<MirDvInsrdCliNmT />");
    strBuffer.append("</MirDvInsrdCliNmG>");
    strBuffer.append("<MirCliCrntLocCdG>");
    strBuffer.append("<MirCliCrntLocCdT />");
    strBuffer.append("<MirCliCrntLocCdT />");
    strBuffer.append("<MirCliCrntLocCdT />");
    strBuffer.append("<MirCliCrntLocCdT />");
    strBuffer.append("<MirCliCrntLocCdT />");
    strBuffer.append("<MirCliCrntLocCdT />");
    strBuffer.append("<MirCliCrntLocCdT />");
    strBuffer.append("<MirCliCrntLocCdT />");
    strBuffer.append("<MirCliCrntLocCdT />");
    strBuffer.append("<MirCliCrntLocCdT />");
    strBuffer.append("<MirCliCrntLocCdT />");
    strBuffer.append("</MirCliCrntLocCdG>");
    strBuffer.append("<MirCliBthLocCdG>");
    strBuffer.append("<MirCliBthLocCdT />");
    strBuffer.append("<MirCliBthLocCdT />");
    strBuffer.append("<MirCliBthLocCdT />");
    strBuffer.append("<MirCliBthLocCdT />");
    strBuffer.append("<MirCliBthLocCdT />");
    strBuffer.append("<MirCliBthLocCdT />");
    strBuffer.append("<MirCliBthLocCdT />");
    strBuffer.append("<MirCliBthLocCdT />");
    strBuffer.append("<MirCliBthLocCdT />");
    strBuffer.append("<MirCliBthLocCdT />");
    strBuffer.append("<MirCliBthLocCdT />");
    strBuffer.append("</MirCliBthLocCdG>");
    strBuffer.append("<MirPolId>");
    strBuffer.append("<MirPolIdBase>");
    strBuffer.append(PolicyNo);
    strBuffer.append("</MirPolIdBase>");
    strBuffer.append("<MirPolIdSfx>");
    strBuffer.append(suffix);
    strBuffer.append("</MirPolIdSfx>");
    strBuffer.append("</MirPolId>");
    strBuffer.append("<MirPolCstatCd />");
    strBuffer.append("<MirCliSexCdG>");
    strBuffer.append("<MirCliSexCdT />");
    strBuffer.append("<MirCliSexCdT />");
    strBuffer.append("<MirCliSexCdT />");
    strBuffer.append("<MirCliSexCdT />");
    strBuffer.append("<MirCliSexCdT />");
    strBuffer.append("<MirCliSexCdT />");
    strBuffer.append("<MirCliSexCdT />");
    strBuffer.append("<MirCliSexCdT />");
    strBuffer.append("<MirCliSexCdT />");
    strBuffer.append("<MirCliSexCdT />");
    strBuffer.append("<MirCliSexCdT />");
    strBuffer.append("</MirCliSexCdG>");
    strBuffer.append("<MirCliUwgdecnCdG>");
    strBuffer.append("<MirCliUwgdecnCdT />");
    strBuffer.append("<MirCliUwgdecnCdT />");
    strBuffer.append("<MirCliUwgdecnCdT />");
    strBuffer.append("<MirCliUwgdecnCdT />");
    strBuffer.append("<MirCliUwgdecnCdT />");
    strBuffer.append("<MirCliUwgdecnCdT />");
    strBuffer.append("<MirCliUwgdecnCdT />");
    strBuffer.append("<MirCliUwgdecnCdT />");
    strBuffer.append("<MirCliUwgdecnCdT />");
    strBuffer.append("<MirCliUwgdecnCdT />");
    strBuffer.append("<MirCliUwgdecnCdT />");
    strBuffer.append("</MirCliUwgdecnCdG>");
    strBuffer.append("<MirCliUwgdecnDtG>");
    strBuffer.append("<MirCliUwgdecnDtT />");
    strBuffer.append("<MirCliUwgdecnDtT />");
    strBuffer.append("<MirCliUwgdecnDtT />");
    strBuffer.append("<MirCliUwgdecnDtT />");
    strBuffer.append("<MirCliUwgdecnDtT />");
    strBuffer.append("<MirCliUwgdecnDtT />");
    strBuffer.append("<MirCliUwgdecnDtT />");
    strBuffer.append("<MirCliUwgdecnDtT />");
    strBuffer.append("<MirCliUwgdecnDtT />");
    strBuffer.append("<MirCliUwgdecnDtT />");
    strBuffer.append("<MirCliUwgdecnDtT />");
    strBuffer.append("</MirCliUwgdecnDtG>");
    strBuffer.append("<MirDvUwPrcesCd>D</MirDvUwPrcesCd>");
    strBuffer.append("</MirCommonFields>");


    strBuffer.append("</OLifE>");
    strBuffer.append("</TXLifeRequest> ");
    strBuffer.append("</TXLife>");
    System.out.println(strBuffer.toString());
    try {
      StringBuffer sb = new StringBuffer();
      sb.append("BF0592Select_PolNo-");
      sb.append(strPolicyId);
      hs_result = invokeIngenium.submit(strBuffer.toString(), sb.toString());
      if (printParsedXML) {
        Object strkeys[] = hs_result.keySet().toArray();
        for (int i = 0; i < strkeys.length; i++) {
          ArrayList al_result = (ArrayList) hs_result.get(strkeys[i]);
          TheLogger.info(strkeys[i] + ": ");
          for (int j = 0; j < al_result.size(); j++)
            TheLogger.info("\t" + al_result.get(j));
        }
      }
      TheLogger.debug("BF0592 was called");
    } catch (Exception ex) {
      TheLogger.error(ex.getMessage());
    }
    return hs_result;
  }

  public HashMap ServiceBeneficiaryList_Append(String[] Authenticate, String strPolicyNo) {
    String PolicyNo = "";
    String suffix = "";
    StringBuffer strBuffer = new StringBuffer();
    if (strPolicyNo.length() > 9) {
      PolicyNo = strPolicyNo.substring(0, 9);
      suffix = "" + strPolicyNo.charAt(9);
    } else {
      PolicyNo = strPolicyNo;
    }
    HashMap hs_result = null;
    strBuffer.append(createLogin(Authenticate));
    strBuffer.append("<TXLifeRequest>");
    strBuffer.append("<TransRefGUID>2996b11f-e5b2-d00f-281b-03582683cd49</TransRefGUID> ");
    strBuffer.append("<TransType tc=\"BeneficiaryList\">BeneficiaryList</TransType>");
    strBuffer.append("<TransExeDate>");
    strBuffer.append(exeDate);
    strBuffer.append("</TransExeDate> ");
    strBuffer.append("<TransExeTime>");
    strBuffer.append(exeTime);
    strBuffer.append("</TransExeTime> ");
    strBuffer.append("<OLifE>");
    strBuffer.append("<ReturnCds/>");
    strBuffer.append("<BeneficiaryData>");
    strBuffer.append("<MirPolId>");
    strBuffer.append("<MirPolIdBase>");
    strBuffer.append(PolicyNo);
    strBuffer.append("</MirPolIdBase> ");
    strBuffer.append("<MirPolIdSfx>");
    strBuffer.append(suffix);
    strBuffer.append("</MirPolIdSfx> ");
    strBuffer.append("</MirPolId> ");
    strBuffer.append("<SrcSystem>WMS</SrcSystem> ");
    strBuffer.append("</BeneficiaryData>");
    strBuffer.append("</OLifE> ");
    strBuffer.append("</TXLifeRequest> ");
    strBuffer.append("</TXLife> ");

    try {
      StringBuffer sb = new StringBuffer();
      sb.append("ServiceBeneficiaryList_PolNo-");
      sb.append(strPolicyNo);
      hs_result = invokeIngenium.submit(strBuffer.toString(), sb.toString());
      if (printParsedXML) {
        Object strkeys[] = hs_result.keySet().toArray();
        for (int i = 0; i < strkeys.length; i++) {
          ArrayList al_result = (ArrayList) hs_result.get(strkeys[i]);
          TheLogger.info(strkeys[i] + ": ");
          for (int j = 0; j < al_result.size(); j++)
            TheLogger.info("\t" + al_result.get(j));
        }
      }
      TheLogger.debug("ServiceBeneficiaryList was called using client no");
    } catch (Exception ex) {
      TheLogger.error(ex.getMessage());
    }
    return hs_result;
  }

  public HashMap InquiryFundActivity_Append(String[] authenticate, String strMirPolIdBase, String effectiveDate, String strCoverageNo, String strSequenceNo) {
    HashMap hs_result = null;
    String PolicyNo = "";
    String suffix = "";
    StringBuffer strBuffer = new StringBuffer();
    if (strMirPolIdBase.length() > 9) {
      PolicyNo = strMirPolIdBase.substring(0, 9);
      suffix = "" + strMirPolIdBase.charAt(9);
    } else {
      PolicyNo = strMirPolIdBase;
    }
    strBuffer.append(createLogin(authenticate))
        .append("<TXLifeRequest>")
        .append("<TransRefGUID>2996b11f-e5b2-d00f-281b-03582683cd49</TransRefGUID> ")
        .append("<TransType tc=\"FundInquiryActivity\">FundInquiryActivity</TransType> ")
        .append("<TransExeDate>")
        .append(exeDate)
        .append("</TransExeDate>")
        .append("<TransExeTime>")
        .append(exeTime)
        .append("</TransExeTime>")
        .append("<OLifE>")
        .append("<ReturnCds/>")
        .append("<FundInquiryActivityData>")
        .append("<MirPolId>")
        .append("<MirPolIdBase>")
        .append(PolicyNo)
        .append("</MirPolIdBase>")
        .append("<MirPolIdSfx>")
        .append(suffix)
        .append("</MirPolIdSfx>")
        .append("<MirCvgNum>")
        .append(strCoverageNo)
        .append("</MirCvgNum>")
        .append("</MirPolId>")
        .append("<MirCiaEffDt>")
        .append(effectiveDate)
        .append("</MirCiaEffDt>")
        .append("<MirCiaTypCd />")
        .append("<MirCiaSeqNum>")
        .append(strSequenceNo)
        .append("</MirCiaSeqNum>")
        .append("<SrcSystem>WMS</SrcSystem>")
        .append("</FundInquiryActivityData>")
        .append("</OLifE>")
        .append("</TXLifeRequest>")
        .append("</TXLife>");
    try {
      StringBuffer sb = new StringBuffer();
      sb.append("InquiryFundActivity_PolNo-");
      sb.append(strMirPolIdBase);
      hs_result = invokeIngenium.submit(strBuffer.toString(), sb.toString());
      if (printParsedXML) {
        Object strkeys[] = hs_result.keySet().toArray();
        for (int i = 0; i < strkeys.length; i++) {
          ArrayList al_result = (ArrayList) hs_result.get(strkeys[i]);
          TheLogger.info("TheLogger strkeys: " + strkeys[i] + ": ");
					/*for(int j=0;j<al_result.size();j++)
	          			TheLogger.info("\t" + al_result.get(j));*/
        }
      }
      TheLogger.debug("InquiryFundActivity was called using client number");
    } catch (Exception ex) {
      TheLogger.error(ex.getMessage());
    }
    return hs_result;
  }

  public HashMap PolicyInquiryAgent_Append(String[] authenticate, String strMirPolIdBase, String effectiveDate, String strCoverageNo) {
    HashMap hs_result = null;
    String PolicyNo = "";
    String suffix = "";
    StringBuffer strBuffer = new StringBuffer();
    if (strMirPolIdBase.length() > 9) {
      PolicyNo = strMirPolIdBase.substring(0, 9);
      suffix = "" + strMirPolIdBase.charAt(9);
    } else {
      PolicyNo = strMirPolIdBase;
    }
    strBuffer.append(createLogin(authenticate))
        .append("<TXLifeRequest>")
        .append("<TransRefGUID>2996b11f-e5b2-d00f-281b-03582683cd49</TransRefGUID> ")
        .append("<TransType tc=\"InquiryAgent\">InquiryAgent</TransType> ")
        .append("<TransExeDate>")
        .append(exeDate)
        .append("</TransExeDate>")
        .append("<TransExeTime>")
        .append(exeTime)
        .append("</TransExeTime>")
        .append("<OLifE>")
        .append("<ReturnCds/>")
        .append("<InquiryAgentData>")
        .append("<MirCommonFields>")
        .append("<MirPolId>")
        .append("<MirPolIdBase>")
        .append(PolicyNo)
        .append("</MirPolIdBase>")
        .append("<MirPolIdSfx>")
        .append(suffix)
        .append("</MirPolIdSfx>")
        .append("</MirPolId>")
        .append("<MirCvgNum>")
        .append(strCoverageNo)
        .append("</MirCvgNum>")
        .append("<MirDvEffDt>")
        .append(effectiveDate)
        .append("</MirDvEffDt>")
        .append("</MirCommonFields>")
        .append("<SrcSystem>WMS</SrcSystem>")
        .append("</InquiryAgentData>")
        .append("</OLifE>")
        .append("</TXLifeRequest>")
        .append("</TXLife>");
    try {
      StringBuffer sb = new StringBuffer();
      sb.append("InquiryAgent_PolNo-");
      sb.append(strMirPolIdBase);
      hs_result = invokeIngenium.submit(strBuffer.toString(), sb.toString());
      if (printParsedXML) {
        Object strkeys[] = hs_result.keySet().toArray();
        for (int i = 0; i < strkeys.length; i++) {
          ArrayList al_result = (ArrayList) hs_result.get(strkeys[i]);
          TheLogger.info("TheLogger strkeys: " + strkeys[i] + ": ");
					/*for(int j=0;j<al_result.size();j++)
	          			TheLogger.info("\t" + al_result.get(j));*/
        }
      }
      TheLogger.debug("PolicyInquiryAgent was called using client number");
    } catch (Exception ex) {
      TheLogger.error(ex.getMessage());
    }
    return hs_result;
  }

  public HashMap PolicyInquirySummary_Append(String[] authenticate, String strMirPolIdBase, String effectiveDate) {
    HashMap hs_result = null;
    String PolicyNo = "";
    String suffix = "";
    StringBuffer strBuffer = new StringBuffer();
    if (strMirPolIdBase.length() > 9) {
      PolicyNo = strMirPolIdBase.substring(0, 9);
      suffix = "" + strMirPolIdBase.charAt(9);
    } else {
      PolicyNo = strMirPolIdBase;
    }
    strBuffer.append(createLogin(authenticate))
        .append("<TXLifeRequest>")
        .append("<TransRefGUID>2996b11f-e5b2-d00f-281b-03582683cd49</TransRefGUID> ")
        .append("<TransType tc=\"InquirySummary\">InquirySummary</TransType> ")
        .append("<TransExeDate>")
        .append(exeDate)
        .append("</TransExeDate>")
        .append("<TransExeTime>")
        .append(exeTime)
        .append("</TransExeTime>")
        .append("<OLifE>")
        .append("<InquirySummaryData>")
        .append("<MirCommonFields>")
        .append("<MirPolId>")
        .append("<MirPolIdBase>")
        .append(PolicyNo)
        .append("</MirPolIdBase>")
        .append("<MirPolIdSfx>")
        .append(suffix)
        .append("</MirPolIdSfx>")
        .append("</MirPolId>")
        .append("<MirDvEffDt>")
        .append(effectiveDate)
        .append("</MirDvEffDt>")
        .append("<MirDvPrcesStateCd>1</MirDvPrcesStateCd>")
        .append("</MirCommonFields>")
        .append("<SrcSystem>WMS</SrcSystem>")
        .append("</InquirySummaryData>")
        .append("</OLifE>")
        .append("</TXLifeRequest>")
        .append("</TXLife>");
    try {
      StringBuffer sb = new StringBuffer();
      sb.append("InquirySummary_PolNo-");
      sb.append(strMirPolIdBase);
      hs_result = invokeIngenium.submit(strBuffer.toString(), sb.toString());
      if (printParsedXML) {
        Object strkeys[] = hs_result.keySet().toArray();
        for (int i = 0; i < strkeys.length; i++) {
          ArrayList al_result = (ArrayList) hs_result.get(strkeys[i]);
          TheLogger.info("TheLogger strkeys: " + strkeys[i] + ": ");
					/*for(int j=0;j<al_result.size();j++)
	          			TheLogger.info("\t" + al_result.get(j));*/
        }
      }
      TheLogger.debug("InquirySummary was called using client number");
    } catch (Exception ex) {
      TheLogger.error(ex.getMessage());
    }
    return hs_result;
  }

  public HashMap InquiryCoverageDetails_Append(String[] authenticate, String strMirPolIdBase, String strCoverageNo) {
    HashMap hs_result = null;
    String PolicyNo = "";
    String suffix = "";
    StringBuffer strBuffer = new StringBuffer();
    if (strMirPolIdBase.length() > 9) {
      PolicyNo = strMirPolIdBase.substring(0, 9);
      suffix = "" + strMirPolIdBase.charAt(9);
    } else {
      PolicyNo = strMirPolIdBase;
    }
    strBuffer.append(createLogin(authenticate))
        .append("<TXLifeRequest>")
        .append("<TransRefGUID />")
        .append("<TransType tc=\"InquiryCoverageDetails\">InquiryCoverageDetails</TransType> ")
        .append("<TransExeDate>")
        .append(exeDate)
        .append("</TransExeDate>")
        .append("<TransExeTime>")
        .append(exeTime)
        .append("</TransExeTime>")
        .append("<OLifE>")
        .append("<InquiryCoverageDetailsData>")
        .append("<MirCommonFields>")
        .append("<MirCvgNum>")
        .append(strCoverageNo)
        .append("</MirCvgNum>")
        .append("<MirDvOwnCliNm />")
        .append("<MirPolCvgRecCtr />")
        .append("<MirPolId>")
        .append("<MirPolIdBase>")
        .append(PolicyNo)
        .append("</MirPolIdBase>")
        .append("<MirPolIdSfx>")
        .append(suffix)
        .append("</MirPolIdSfx>")
        .append("</MirPolId>")
        .append("</MirCommonFields>")
        .append("<SrcSystem>WMS</SrcSystem>")
        .append("</InquiryCoverageDetailsData>")
        .append("</OLifE>")
        .append("</TXLifeRequest>")
        .append("</TXLife>");
    try {
      StringBuffer sb = new StringBuffer();
      sb.append("InquiryCoverageDetailsData_PolNo-");
      sb.append(strMirPolIdBase);
      hs_result = invokeIngenium.submit(strBuffer.toString(), sb.toString());
      if (printParsedXML) {
        Object strkeys[] = hs_result.keySet().toArray();
        for (int i = 0; i < strkeys.length; i++) {
          ArrayList al_result = (ArrayList) hs_result.get(strkeys[i]);
          TheLogger.info("TheLogger strkeys: " + strkeys[i] + ": ");
					/*for(int j=0;j<al_result.size();j++)
	          			TheLogger.info("\t" + al_result.get(j));*/
        }
      }
      TheLogger.debug("InquiryCoverageDetailsData was called using client number");
    } catch (Exception ex) {
      TheLogger.error(ex.getMessage());
    }
    return hs_result;
  }

  public HashMap BillingMethodandModeInquiry_Append(String[] authenticate, String strMirPolIdBase, String effectiveDate) {
    HashMap hs_result = null;
    String PolicyNo = "";
    String suffix = "";
    StringBuffer strBuffer = new StringBuffer();
    if (strMirPolIdBase.length() > 9) {
      PolicyNo = strMirPolIdBase.substring(0, 9);
      suffix = "" + strMirPolIdBase.charAt(9);
    } else {
      PolicyNo = strMirPolIdBase;
    }
    strBuffer.append(createLogin(authenticate))
        .append("<TXLifeRequest>")
        .append("<TransRefGUID>2996b11f-e5b2-d00f-281b-03582683cd49</TransRefGUID> ")
        .append("<TransType tc=\"BillingMethodandModeInquiry\">BillingMethodandModeInquiry</TransType> ")
        .append("<TransExeDate>")
        .append(exeDate)
        .append("</TransExeDate>")
        .append("<TransExeTime>")
        .append(exeTime)
        .append("</TransExeTime>")
        .append("<OLifE>")
        .append("<BillingMethodandModeInquiryData>")
        .append("<MirPolId>")
        .append("<MirPolIdBase>")
        .append(PolicyNo)
        .append("</MirPolIdBase>")
        .append("<MirPolIdSfx>")
        .append(suffix)
        .append("</MirPolIdSfx>")
        .append("</MirPolId>")
        .append("<MirCommonFields>")
        .append("<MirPchstEffIdtNum>")
        .append(effectiveDate)
        .append("</MirPchstEffIdtNum>")
        .append("</MirCommonFields>")
        .append("<SrcSystem>WMS</SrcSystem>")
        .append("</BillingMethodandModeInquiryData>")
        .append("</OLifE>")
        .append("</TXLifeRequest>")
        .append("</TXLife>");
    try {
      StringBuffer sb = new StringBuffer();
      sb.append("BillingMethodandModeInquiry_PolNo-");
      sb.append(strMirPolIdBase);
      hs_result = invokeIngenium.submit(strBuffer.toString(), sb.toString());
      if (printParsedXML) {
        Object strkeys[] = hs_result.keySet().toArray();
        for (int i = 0; i < strkeys.length; i++) {
          ArrayList al_result = (ArrayList) hs_result.get(strkeys[i]);
          TheLogger.info("TheLogger strkeys: " + strkeys[i] + ": ");
					/*for(int j=0;j<al_result.size();j++)
	          			TheLogger.info("\t" + al_result.get(j));*/
        }
      }
      TheLogger.debug("BillingMethodandModeInquiry was called using client number");
    } catch (Exception ex) {
      TheLogger.error(ex.getMessage());
    }
    return hs_result;
  }

  public HashMap CoverageRiskInquiry_Append(String[] authenticate, String strMirPolIdBase, String effectiveDate, String strCoverageNo) {
    HashMap hs_result = null;
    String PolicyNo = "";
    String suffix = "";
    StringBuffer strBuffer = new StringBuffer();
    if (strMirPolIdBase.length() > 9) {
      PolicyNo = strMirPolIdBase.substring(0, 9);
      suffix = "" + strMirPolIdBase.charAt(9);
    } else {
      PolicyNo = strMirPolIdBase;
    }
    strBuffer.append(createLogin(authenticate))
        .append("<TXLifeRequest>")
        .append("<TransRefGUID>2996b11f-e5b2-d00f-281b-03582683cd49</TransRefGUID> ")
        .append("<TransType tc=\"CoverageRiskInquiry\">CoverageRiskInquiry</TransType> ")
        .append("<TransExeDate>")
        .append(exeDate)
        .append("</TransExeDate>")
        .append("<TransExeTime>")
        .append(exeTime)
        .append("</TransExeTime>")
        .append("<OLifE>")
        .append("<ReturnCds/>")
        .append("<CoverageRiskInquiryData>")
        .append("<MirCommonFields>")
        .append("<MirPolId>")
        .append("<MirPolIdBase>")
        .append(PolicyNo)
        .append("</MirPolIdBase>")
        .append("<MirPolIdSfx>")
        .append(suffix)
        .append("</MirPolIdSfx>")
        .append("</MirPolId>")
        .append("<MirCvgNum>")
        .append(strCoverageNo)
        .append("</MirCvgNum>")
        .append("<MirPchstEffIdtNum>")
        .append(effectiveDate)
        .append("</MirPchstEffIdtNum>")
        .append("</MirCommonFields>")
        .append("<SrcSystem>WMS</SrcSystem>")
        .append("</CoverageRiskInquiryData>")
        .append("</OLifE>")
        .append("</TXLifeRequest>")
        .append("</TXLife>");
    try {
      StringBuffer sb = new StringBuffer();
      sb.append("CoverageRiskInquiry_PolNo-");
      sb.append(strMirPolIdBase);
      hs_result = invokeIngenium.submit(strBuffer.toString(), sb.toString());
      if (printParsedXML) {
        Object strkeys[] = hs_result.keySet().toArray();
        for (int i = 0; i < strkeys.length; i++) {
          ArrayList al_result = (ArrayList) hs_result.get(strkeys[i]);
          TheLogger.info("TheLogger strkeys: " + strkeys[i] + ": ");
					/*for(int j=0;j<al_result.size();j++)
	          			TheLogger.info("\t" + al_result.get(j));*/
        }
      }
      TheLogger.debug("PolicyInquiryAgent was called using client number");
    } catch (Exception ex) {
      TheLogger.error(ex.getMessage());
    }
    return hs_result;
  }

  public HashMap AccountingSummaryHistory_Append(String[] authenticate, String strMirPolIdBase, String procDate, String sysDate, int[] time, String strRecordType) {
    HashMap hs_result = null;
    String PolicyNo = "";
    String suffix = "";
    StringBuffer strBuffer = new StringBuffer();
    if (strMirPolIdBase.length() > 9) {
      PolicyNo = strMirPolIdBase.substring(0, 9);
      suffix = "" + strMirPolIdBase.charAt(9);
    } else {
      PolicyNo = strMirPolIdBase;
    }
    strBuffer.append(createLogin(authenticate))
        .append("<TXLifeRequest>")
        .append("<TransRefGUID>2996b11f-e5b2-d00f-281b-03582683cd49</TransRefGUID> ")
        .append("<TransType tc=\"AccountingSummaryHistory\">AccountingSummaryHistory</TransType> ")
        .append("<TransExeDate>")
        .append(exeDate)
        .append("</TransExeDate>")
        .append("<TransExeTime>")
        .append(exeTime)
        .append("</TransExeTime>")
        .append("<OLifE>")
        .append("<AccountingSummaryHistoryData>")
        .append("<MirPolId>")
        .append("<MirPolIdBase>")
        .append(PolicyNo)
        .append("</MirPolIdBase>")
        .append("<MirPolIdSfx>")
        .append(suffix)
        .append("</MirPolIdSfx>")
        .append("</MirPolId>")
        .append("<MirDvAcctTrxnPrcesDt>")
        .append(procDate)
        .append("</MirDvAcctTrxnPrcesDt>")
        .append("<MirDvAcctTrxnDt>")
        .append(sysDate)
        .append("</MirDvAcctTrxnDt>")
        .append("<MirDvAcctTrxnTime>")
        .append("<MirDvAcctTrxnHr>")
        .append(time[0])
        .append("</MirDvAcctTrxnHr>")
        .append("<MirDvAcctTrxnMi>")
        .append(time[1])
        .append("</MirDvAcctTrxnMi>")
        .append("<MirDvAcctTrxnSec>")
        .append(time[2])
        .append("</MirDvAcctTrxnSec>")
        .append("</MirDvAcctTrxnTime>")
        .append("<SrcSystem>WMS</SrcSystem>")
        .append("</AccountingSummaryHistoryData>")
        .append("</OLifE>")
        .append("</TXLifeRequest>")
        .append("</TXLife>");
    try {
      StringBuffer sb = new StringBuffer();
      sb.append("AccountingSummaryHistory_PolNo-");
      sb.append(strMirPolIdBase);
      hs_result = invokeIngenium.submit(strBuffer.toString(), sb.toString());
      if (printParsedXML) {
        Object strkeys[] = hs_result.keySet().toArray();
        for (int i = 0; i < strkeys.length; i++) {
          ArrayList al_result = (ArrayList) hs_result.get(strkeys[i]);
          TheLogger.info("TheLogger strkeys: " + strkeys[i] + ": ");
					/*for(int j=0;j<al_result.size();j++)
	          			TheLogger.info("\t" + al_result.get(j));*/
        }
      }
      TheLogger.debug("AccountingSummaryHistory was called using client number");
    } catch (Exception ex) {
      TheLogger.error(ex.getMessage());
    }
    return hs_result;
  }

  public HashMap LoanDetailList_Append(String[] authenticate, String strMirPolIdBase, String strLoanType, String effectiveDate, String strSequenceNo) {
    HashMap hs_result = null;
    String PolicyNo = "";
    String suffix = "";
    StringBuffer strBuffer = new StringBuffer();
    if (strMirPolIdBase.length() > 9) {
      PolicyNo = strMirPolIdBase.substring(0, 9);
      suffix = "" + strMirPolIdBase.charAt(9);
    } else {
      PolicyNo = strMirPolIdBase;
    }
    strBuffer.append(createLogin(authenticate))
        .append("<TXLifeRequest>")
        .append("<TransRefGUID>2996b11f-e5b2-d00f-281b-03582683cd49</TransRefGUID> ")
        .append("<TransType tc=\"LoanDetailList\">LoanDetailList</TransType> ")
        .append("<TransExeDate>")
        .append(exeDate)
        .append("</TransExeDate>")
        .append("<TransExeTime>")
        .append(exeTime)
        .append("</TransExeTime>")
        .append("<OLifE>")
        .append("<ReturnCds />")
        .append("<LoanDetailListData>")
        .append("<MirCommonFields>")
        .append("<MirPolId>")
        .append("<MirPolIdBase>")
        .append(PolicyNo)
        .append("</MirPolIdBase>")
        .append("<MirPolIdSfx>")
        .append(suffix)
        .append("</MirPolIdSfx>")
        .append("</MirPolId>")
        .append("<MirPolLoanEffDt>")
        .append(effectiveDate)
        .append("</MirPolLoanEffDt>")
        .append("<MirPolLoanSeqNum>")
        .append(strSequenceNo)
        .append("</MirPolLoanSeqNum>")
        .append("</MirCommonFields>")
        .append("<SrcSystem>WMS</SrcSystem>")
        .append("</LoanDetailListData>")
        .append("</OLifE>")
        .append("</TXLifeRequest>")
        .append("</TXLife>");
    try {
      StringBuffer sb = new StringBuffer();
      sb.append("LoanDetailListData_PolNo-");
      sb.append(strMirPolIdBase);
      hs_result = invokeIngenium.submit(strBuffer.toString(), sb.toString());
      if (printParsedXML) {
        Object strkeys[] = hs_result.keySet().toArray();
        for (int i = 0; i < strkeys.length; i++) {
          ArrayList al_result = (ArrayList) hs_result.get(strkeys[i]);
          TheLogger.info("TheLogger strkeys: " + strkeys[i] + ": ");
					/*for(int j=0;j<al_result.size();j++)
	          			TheLogger.info("\t" + al_result.get(j));*/
        }
      }
      TheLogger.debug("LoanDetailListData was called using client number");
    } catch (Exception ex) {
      TheLogger.error(ex.getMessage());
    }
    return hs_result;
  }

  public HashMap ChangeHistoryList_Append(String[] authenticate, String strMirPolIdBase, String strCoverageNo, String effectiveDate, String strSequenceNo, String strCurrentStatus) {
    HashMap hs_result = null;
    String PolicyNo = "";
    String suffix = "";
    StringBuffer strBuffer = new StringBuffer();
    if (strMirPolIdBase.length() > 9) {
      PolicyNo = strMirPolIdBase.substring(0, 9);
      suffix = "" + strMirPolIdBase.charAt(9);
    } else {
      PolicyNo = strMirPolIdBase;
    }
    strBuffer.append(createLogin(authenticate))
        .append("<TXLifeRequest>")
        .append("<TransRefGUID>2996b11f-e5b2-d00f-281b-03582683cd49</TransRefGUID> ")
        .append("<TransType tc=\"ChangeHistoryList\">ChangeHistoryList</TransType> ")
        .append("<TransExeDate>")
        .append(exeDate)
        .append("</TransExeDate>")
        .append("<TransExeTime>")
        .append(exeTime)
        .append("</TransExeTime>")
        .append("<OLifE>")
        .append("<ReturnCds />")
        .append("<ChangeHistoryListData>")
        .append("<MirCommonFields>")
        .append("<MirPolId>")
        .append("<MirPolIdBase>")
        .append(PolicyNo)
        .append("</MirPolIdBase>")
        .append("<MirPolIdSfx>")
        .append(suffix)
        .append("</MirPolIdSfx>")
        .append("</MirPolId>")
        .append("<MirPchstEffDt>")
        .append(effectiveDate)
        .append("</MirPchstEffDt>")
        .append("<MirPchstSeqNum>")
        .append(strSequenceNo)
        .append("</MirPchstSeqNum>")
        .append("</MirCommonFields>")
        .append("<SrcSystem>WMS</SrcSystem>")
        .append("</ChangeHistoryListData>")
        .append("</OLifE>")
        .append("</TXLifeRequest>")
        .append("</TXLife>");
    try {
      StringBuffer sb = new StringBuffer();
      sb.append("ChangeHistoryListData_PolNo-");
      sb.append(strMirPolIdBase);
      hs_result = invokeIngenium.submit(strBuffer.toString(), sb.toString());
      if (printParsedXML) {
        Object strkeys[] = hs_result.keySet().toArray();
        for (int i = 0; i < strkeys.length; i++) {
          ArrayList al_result = (ArrayList) hs_result.get(strkeys[i]);
          TheLogger.info("TheLogger strkeys: " + strkeys[i] + ": ");
					/*for(int j=0;j<al_result.size();j++)
	          			TheLogger.info("\t" + al_result.get(j));*/
        }
      }
      TheLogger.debug("ChangeHistoryListData was called using client number");
    } catch (Exception ex) {
      TheLogger.error(ex.getMessage());
    }
    return hs_result;
  }

  public HashMap ClientAddressList_Append(String[] authenticate, String strClientId, String addressEffDate, String strAddressType) {
    HashMap hs_result = null;
    StringBuffer strBuffer = new StringBuffer();
    strBuffer.append(createLogin(authenticate))
        .append("<TXLifeRequest>")
        .append("<TransRefGUID>2996b11f-e5b2-d00f-281b-03582683cd49</TransRefGUID> ")
        .append("<TransType tc=\"AddressList\">AddressList</TransType> ")
        .append("<TransExeDate>")
        .append(exeDate)
        .append("</TransExeDate>")
        .append("<TransExeTime>")
        .append(exeTime)
        .append("</TransExeTime>")
        .append("<OLifE>")
        .append("<AddressData>")
        .append("<MirKeyFields>")
        .append("<MirCliId>")
        .append(strClientId)
        .append("</MirCliId>")
        .append("</MirKeyFields>")
        .append("<SrcSystem>WMS</SrcSystem>")
        .append("</AddressData>")
        .append("</OLifE>")
        .append("</TXLifeRequest>")
        .append("</TXLife>");
    try {
      StringBuffer sb = new StringBuffer();
      sb.append("AddressList_CliId-");
      sb.append(strClientId);
      hs_result = invokeIngenium.submit(strBuffer.toString(), sb.toString());
      if (printParsedXML) {
        Object strkeys[] = hs_result.keySet().toArray();
        for (int i = 0; i < strkeys.length; i++) {
          ArrayList al_result = (ArrayList) hs_result.get(strkeys[i]);
          TheLogger.info("TheLogger strkeys: " + strkeys[i] + ": ");
					/*for(int j=0;j<al_result.size();j++)
	          			TheLogger.info("\t" + al_result.get(j));*/
        }
      }
      TheLogger.debug("AddressList was called using client number");
    } catch (Exception ex) {
      TheLogger.error(ex.getMessage());
    }
    return hs_result;
  }

  public HashMap AgentInquiry_Append(String[] authenticate, String strAgentId) {
    HashMap hs_result = null;
    StringBuffer strBuffer = new StringBuffer();
    strBuffer.append(createLogin(authenticate))
        .append("<TXLifeRequest>")
        .append("<TransRefGUID>2996b11f-e5b2-d00f-281b-03582683cd49</TransRefGUID> ")
        .append("<TransType tc=\"AgentInquiry\">AgentInquiry</TransType> ")
        .append("<TransExeDate>")
        .append(exeDate)
        .append("</TransExeDate>")
        .append("<TransExeTime>")
        .append(exeTime)
        .append("</TransExeTime>")
        .append("<OLifE>")
        .append("<AgentData>")
        .append("<MirCommonFields>")
        .append("<MirAgtId>")
        .append(strAgentId)
        .append("</MirAgtId>")
        .append("</MirCommonFields>")
        .append("<SrcSystem>WMS</SrcSystem>")
        .append("</AgentData>")
        .append("</OLifE>")
        .append("</TXLifeRequest>")
        .append("</TXLife>");
    try {
      StringBuffer sb = new StringBuffer();
      sb.append("AgentInquiry_AgentId-");
      sb.append(strAgentId);
      hs_result = invokeIngenium.submit(strBuffer.toString(), sb.toString());
      if (printParsedXML) {
        Object strkeys[] = hs_result.keySet().toArray();
        for (int i = 0; i < strkeys.length; i++) {
          ArrayList al_result = (ArrayList) hs_result.get(strkeys[i]);
          TheLogger.info("TheLogger strkeys: " + strkeys[i] + ": ");
					/*for(int j=0;j<al_result.size();j++)
	          			TheLogger.info("\t" + al_result.get(j));*/
        }
      }
      TheLogger.debug("AddressList was called using client number");
    } catch (Exception ex) {
      TheLogger.error(ex.getMessage());
    }
    return hs_result;
  }

  public HashMap FutureDatedPolicyActivityList_Append(String[] authenticate, String strMirPolIdBase, String activityEffDate, String strActivityStatus) {
    HashMap hs_result = null;
    String PolicyNo = "";
    String suffix = "";
    StringBuffer strBuffer = new StringBuffer();
    if (strMirPolIdBase.length() > 9) {
      PolicyNo = strMirPolIdBase.substring(0, 9);
      suffix = "" + strMirPolIdBase.charAt(9);
    } else {
      PolicyNo = strMirPolIdBase;
    }
    strBuffer.append(createLogin(authenticate))
        .append("<TXLifeRequest>")
        .append("<TransRefGUID>2996b11f-e5b2-d00f-281b-03582683cd49</TransRefGUID> ")
        .append("<TransType tc=\"FutureDatedPolicyActivityList\">FutureDatedPolicyActivityList</TransType> ")
        .append("<TransExeDate>")
        .append(exeDate)
        .append("</TransExeDate>")
        .append("<TransExeTime>")
        .append(exeTime)
        .append("</TransExeTime>")
        .append("<OLifE>")
        .append("<ReturnCds />")
        .append("<FutureDatedPolicyActivityListData>")
        .append("<MirPolId>")
        .append("<MirPolIdBase>")
        .append(PolicyNo)
        .append("</MirPolIdBase>")
        .append("<MirPolIdSfx>")
        .append(suffix)
        .append("</MirPolIdSfx>")
        .append("</MirPolId>")
//		.append("<MirCvgNum>")
//		.append()
//		.append("</MirCvgNum>")
        .append("<MirPchstEffIdtNum>")
        .append(activityEffDate)
        .append("</MirPchstEffIdtNum>")
        .append("<SrcSystem>WMS</SrcSystem>")
        .append("</FutureDatedPolicyActivityListData>")
        .append("</OLifE>")
        .append("</TXLifeRequest>")
        .append("</TXLife>");
    try {
      StringBuffer sb = new StringBuffer();
      sb.append("FutureDatedPolicyActivityList_PolNo-");
      sb.append(strMirPolIdBase);
      hs_result = invokeIngenium.submit(strBuffer.toString(), sb.toString());
      if (printParsedXML) {
        Object strkeys[] = hs_result.keySet().toArray();
        for (int i = 0; i < strkeys.length; i++) {
          ArrayList al_result = (ArrayList) hs_result.get(strkeys[i]);
          TheLogger.info("TheLogger strkeys: " + strkeys[i] + ": ");
					/*for(int j=0;j<al_result.size();j++)
	          			TheLogger.info("\t" + al_result.get(j));*/
        }
      }
      TheLogger.debug("FutureDatedPolicyActivityList was called using client number");
    } catch (Exception ex) {
      TheLogger.error(ex.getMessage());
    }
    return hs_result;
  }

  public HashMap GeneralAccountingEntries_Append(String[] authenticate, String strMirPolIdBase, String acctDate, String strAcctEntryDesc) {
    HashMap hs_result = null;
    String PolicyNo = "";
    String suffix = "";
    StringBuffer strBuffer = new StringBuffer();
    if (strMirPolIdBase.length() > 9) {
      PolicyNo = strMirPolIdBase.substring(0, 9);
      suffix = "" + strMirPolIdBase.charAt(9);
    } else {
      PolicyNo = strMirPolIdBase;
    }
    strBuffer.append(createLogin(authenticate))
        .append("<TXLifeRequest>")
        .append("<TransRefGUID>2996b11f-e5b2-d00f-281b-03582683cd49</TransRefGUID> ")
        .append("<TransType tc=\"GeneralAccountingEntries\">GeneralAccountingEntries</TransType> ")
        .append("<TransExeDate>")
        .append(exeDate)
        .append("</TransExeDate>")
        .append("<TransExeTime>")
        .append(exeTime)
        .append("</TransExeTime>")
        .append("<OLifE>")
        .append("<GeneralAccountingEntriesData>")
        .append("<MirCommonFields>")
        .append("<MirPolId>")
        .append("<MirPolIdBase>")
        .append(PolicyNo)
        .append("</MirPolIdBase>")
        .append("<MirPolIdSfx>")
        .append(suffix)
        .append("</MirPolIdSfx>")
        .append("</MirPolId>")
        .append("<MirAcctTrxnPrcesDt>")
        .append(acctDate)
        .append("</MirAcctTrxnPrcesDt>")
        .append("<MirAcctDescTxtG>")
        .append("<MirAcctDescTxtT>")
        .append(strAcctEntryDesc)
        .append("</MirAcctDescTxtT>")
        .append("</MirAcctDescTxtG>")
        .append("</MirCommonFields>")
        .append("<SrcSystem>WMS</SrcSystem>")
        .append("</GeneralAccountingEntriesData>")
        .append("</OLifE>")
        .append("</TXLifeRequest>")
        .append("</TXLife>");
    try {
      StringBuffer sb = new StringBuffer();
      sb.append("GeneralAccountingEntriesData_PolNo-");
      sb.append(strMirPolIdBase);
      hs_result = invokeIngenium.submit(strBuffer.toString(), sb.toString());
      if (printParsedXML) {
        Object strkeys[] = hs_result.keySet().toArray();
        for (int i = 0; i < strkeys.length; i++) {
          ArrayList al_result = (ArrayList) hs_result.get(strkeys[i]);
          TheLogger.info("TheLogger strkeys: " + strkeys[i] + ": ");
					/*for(int j=0;j<al_result.size();j++)
	          			TheLogger.info("\t" + al_result.get(j));*/
        }
      }
      TheLogger.debug("GeneralAccountingEntriesData was called using client number");
    } catch (Exception ex) {
      TheLogger.error(ex.getMessage());
    }
    return hs_result;
  }

  public HashMap getClientDataSheet(String[] authenticate, String policyNumber) {
    System.out.println("getClientDataSheet(String[] authenticate, String policyNumber) start");
    HashMap hs_result = null;
    String PolicyNo = "";
    String suffix = "";
    StringBuffer strBuffer = new StringBuffer();
    if (policyNumber.length() > 9) {
      PolicyNo = policyNumber.substring(0, 9);
      suffix = "" + policyNumber.charAt(9);
    } else {
      PolicyNo = policyNumber;
    }
    strBuffer.append(createLogin(authenticate))
        .append("<TXLifeRequest>")
        .append("<TransRefGUID>2996b11f-e5b2-d00f-281b-03582683cd49</TransRefGUID> ")
        .append("<TransType tc=\"BF9891Retrieve\">BF9891Retrieve</TransType>  ")
        .append("<TransExeDate>")
        .append("<TransExeDate>2006-05-31</TransExeDate> ")
        .append("</TransExeDate>")
        .append("<TransExeTime>")
        .append("<TransExeTime>09:00:00-05:00</TransExeTime>")
        .append("</TransExeTime>")
        .append("<OLifE>")
        .append("<MirCommonFields>")
        .append("<MirPolId>")
        .append("<MirPolIdBase>")
        .append(PolicyNo)
        .append("</MirPolIdBase>")
        .append("<MirPolIdSfx>")
        .append(suffix)
        .append("</MirPolIdSfx>")
        .append("</MirPolId>")
        .append("</MirCommonFields>")
        .append("</OLifE>")
        .append("</TXLifeRequest>")
        .append("</TXLife>");

    try {
      StringBuffer sb = new StringBuffer();
      sb.append("ClientDataSheet_PolNo-");
      sb.append(policyNumber);
      hs_result = invokeIngenium.submit(strBuffer.toString(), sb.toString());
      if (printParsedXML) {
        Object strkeys[] = hs_result.keySet().toArray();
        for (int i = 0; i < strkeys.length; i++) {
          ArrayList al_result = (ArrayList) hs_result.get(strkeys[i]);
          TheLogger.info("TheLogger strkeys: " + strkeys[i] + ": ");
					/*for(int j=0;j<al_result.size();j++)
	          			TheLogger.info("\t" + al_result.get(j));*/
        }
      }
      TheLogger.debug("getClientDataSheet was called using policy number");
    } catch (Exception ex) {
      ex.printStackTrace();
      TheLogger.error(ex.getMessage());
    }
    System.out.println("getClientDataSheet(String[] authenticate, String policyNumber) end");
    return hs_result;
  }

  public String getCDSInquiry(String[] authenticate, String policyNumber) {
    System.out.println("getCDSInquiry(String[] authenticate, String policyNumber) start");
    HashMap hs_result = null;
    String PolicyNo = "";
    String suffix = "";
    StringBuffer strBuffer = new StringBuffer();
    if (policyNumber.length() > 9) {
      PolicyNo = policyNumber.substring(0, 9);
      suffix = "" + policyNumber.charAt(9);
    } else {
      PolicyNo = policyNumber;
    }
    strBuffer.append(createLogin(authenticate))
        .append("<TXLifeRequest>")
        .append("<TransRefGUID></TransRefGUID> ")
        .append("<TransType tc=\"CDSInquiry\">CDSInquiry</TransType>  ")
        .append("<TransExeDate>2006-11-8</TransExeDate> ")
        .append("<TransExeTime>14:50:49</TransExeTime>")
        .append("<OLifE>")
        .append("<CDSInquiryData>")
        .append("<MirPolId>")
        .append("<MirPolIdBase>")
        .append(PolicyNo)
        .append("</MirPolIdBase>")
        .append("<MirPolIdSfx>")
        .append(suffix)
        .append("</MirPolIdSfx>")
        .append("</MirPolId>")
        .append("</CDSInquiryData>")
        .append("</OLifE>")
        .append("</TXLifeRequest>")
        .append("</TXLife>");

    //System.out.println("Request.xml CDSInquiry_PolNo-" + policyNumber + " : "+strBuffer.toString());
    try {
      StringBuffer sb = new StringBuffer();
      sb.append("CDSInquiry_PolNo-");
      sb.append(policyNumber);
      hs_result = invokeIngenium.submit(strBuffer.toString(), sb.toString());
      if (printParsedXML) {
        Object strkeys[] = hs_result.keySet().toArray();
        for (int i = 0; i < strkeys.length; i++) {
          ArrayList al_result = (ArrayList) hs_result.get(strkeys[i]);
          TheLogger.info("TheLogger strkeys: " + strkeys[i] + ": ");
					/*for(int j=0;j<al_result.size();j++)
	          			TheLogger.info("\t" + al_result.get(j));*/
        }
      }
      TheLogger.debug("getCDSInquiry was called using policy number");
    } catch (Exception ex) {
      ex.printStackTrace();
      TheLogger.error(ex.getMessage());
    }
    System.out.println("getCDSInquiry(String[] authenticate, String policyNumber) end");
    return invokeIngenium.getResponse();
  }

  public String getCDSRPCListInquiry(String[] authenticate, String policyNumber, String lastRelPolicyNumber, String lastRelPolicyNumberCovNum) {
    System.out.println("getCDSRPCListInquiry(String[] authenticate, String policyNumber, String lastRelPolicyNumber, String lastRelPolicyNumberCovNum) start");
    HashMap hs_result = null;
    String PolicyNo = "";
    String suffix = "";
    StringBuffer strBuffer = new StringBuffer();
    if (policyNumber.length() > 9) {
      PolicyNo = policyNumber.substring(0, 9);
      suffix = "" + policyNumber.charAt(9);
    } else {
      PolicyNo = policyNumber;
    }
    strBuffer.append(createLogin(authenticate))
        .append("<TXLifeRequest>")
        .append("<TransRefGUID /> ")
        .append("<TransType tc=\"CDSRPCList\" /> ")
        .append("<TransExeDate /> ")
        .append("<TransExeTime /> ")
        .append("<OLifE>")
        .append("<CDSRPCListData> ")
        .append("<MirPolId>")
        .append("<MirPolIdBase>")
        .append(PolicyNo)
        .append("</MirPolIdBase>")
        .append("<MirPolIdSfx>")
        .append(suffix)
        .append("</MirPolIdSfx>")
        .append("</MirPolId>")
        .append("<MirRelSysRefId>")
        .append("<MirRelSysRefPolId>")
        .append(lastRelPolicyNumber)
        .append("</MirRelSysRefPolId>")
        .append("<MirRelSysRefCvgNum>")
        .append(lastRelPolicyNumberCovNum)
        .append("</MirRelSysRefCvgNum>")
        .append("</MirRelSysRefId>")
        .append("<MirMoreCtr />")
        .append("<MirMoreInd />")
        .append("</CDSRPCListData>")
        .append("</OLifE>")
        .append("</TXLifeRequest>")
        .append("</TXLife>");


    //System.out.println("Request.xml CDSRPCListInquiry_PolNo-" + policyNumber + " : "+strBuffer.toString());
    try {
      StringBuffer sb = new StringBuffer();
      sb.append("CDSRPCListInquiry_PolNo-");
      sb.append(policyNumber);
      sb.append("LastPolNo-");
      sb.append(lastRelPolicyNumber);
      sb.append("LastCvgNum-");
      sb.append(lastRelPolicyNumberCovNum);
      hs_result = invokeIngenium.submit(strBuffer.toString(), sb.toString());
      if (printParsedXML) {
        Object strkeys[] = hs_result.keySet().toArray();
        for (int i = 0; i < strkeys.length; i++) {
          ArrayList al_result = (ArrayList) hs_result.get(strkeys[i]);
          TheLogger.info("TheLogger strkeys: " + strkeys[i] + ": ");
					/*for(int j=0;j<al_result.size();j++)
	          			TheLogger.info("\t" + al_result.get(j));*/
        }
      }
      TheLogger.debug("getCDSRPCListInquiry was called using policy number, last related policy number, last related coverage number");
    } catch (Exception ex) {
      ex.printStackTrace();
      TheLogger.error(ex.getMessage());
    }
    System.out.println("getCDSRPCListInquiry(String[] authenticate, String policyNumber, String lastRelPolicyNumber, String lastRelPolicyNumberCovNum) start");
    return invokeIngenium.getResponse();
  }

  public HashMap updateProtectedPolicy(String[] authenticate,
                                       String policyNo,
                                       String reinsInd,
                                       String reinsCompany,
                                       String treatyType,
                                       String reinsFaceAmount,
                                       String tdbFaceAmount,
                                       String adbFaceAmount,
                                       String sundyPaymentAmount) {
    System.out.println("updateProtectedPolicy(String[] authenticate, String policyNo, String reinsInd, String reinsCompany, String treatyType, String reinsFaceAmount, String tdbFaceAmount, String adbFaceAmount, String sundyPaymentAmount) start");
    HashMap hs_result = null;
    String PolicyNo = "";
    String suffix = "";
    StringBuffer strBuffer = new StringBuffer();
    if (policyNo.length() > 9) {
      PolicyNo = policyNo.substring(0, policyNo.length() - 1);
      suffix = "" + policyNo.substring(policyNo.length() - 1, policyNo.length());
    } else {
      PolicyNo = policyNo;
    }
    strBuffer.append(createLogin(authenticate))
        .append("<TXLifeRequest>")
        .append("<TransRefGUID>2996b11f-e5b2-d00f-281b-03582683cd49</TransRefGUID> ")
        .append("<TransType tc=\"ProtectedPolicyUpdate\">ProtectedPolicyUpdate</TransType>  ")
        .append("<TransExeDate>2006-05-31</TransExeDate> ")
        .append("<TransExeTime>09:00:00-05:00</TransExeTime>")
        .append("<OLifE>")
        .append("<ProtectedPolicyUpdateData>")
        .append("<MirPolId>")
        .append("<MirPolIdBase>")
        .append(PolicyNo)
        .append("</MirPolIdBase>")
        .append("<MirPolIdSfx>")
        .append(suffix)
        .append("</MirPolIdSfx>")
        .append("</MirPolId>")
        .append("<MirPolReinsCd>")
        .append(reinsInd)
        .append("</MirPolReinsCd>")
        .append("<MirPolReinsCo>")
        .append(reinsCompany)
        .append("</MirPolReinsCo>")
        .append("<MirPolReinsTrtyTypCd>")
        .append(treatyType)
        .append("</MirPolReinsTrtyTypCd>")
        .append("<MirPolReinsFaceAmt>")
        .append(reinsFaceAmount)
        .append("</MirPolReinsFaceAmt>")
        .append("<MirPolReinsTdbFaceAmt>")
        .append(tdbFaceAmount)
        .append("</MirPolReinsTdbFaceAmt>")
        .append("<MirPolReinsAdbFaceAmt>")
        .append(adbFaceAmount)
        .append("</MirPolReinsAdbFaceAmt>")
        .append("<MirPolReinsSndryAmt>")
        .append(sundyPaymentAmount)
        .append("</MirPolReinsSndryAmt>")
        .append("<SrcSystem>WMS</SrcSystem>")
        .append("</ProtectedPolicyUpdateData>")
        .append("</OLifE>")
        .append("</TXLifeRequest>")
        .append("</TXLife>");

    try {
      StringBuffer sb = new StringBuffer();
      sb.append("UpdateProtectedPolicy_PolNo-");
      sb.append(policyNo);
      hs_result = invokeIngenium.submit(strBuffer.toString(), sb.toString());
      if (printParsedXML) {
        Object strkeys[] = hs_result.keySet().toArray();
        for (int i = 0; i < strkeys.length; i++) {
          ArrayList al_result = (ArrayList) hs_result.get(strkeys[i]);
          TheLogger.info("TheLogger strkeys: " + strkeys[i] + ": ");
					/*for(int j=0;j<al_result.size();j++)
	          			TheLogger.info("\t" + al_result.get(j));*/
        }
      }
      TheLogger.debug("updateProtectedPolicy was called using policy number");
    } catch (Exception ex) {
      ex.printStackTrace();
      TheLogger.error(ex.getMessage());
    }
    System.out.println("updateProtectedPolicy(String[] authenticate, String policyNo, String reinsInd, String reinsCompany, String treatyType, String reinsFaceAmount, String tdbFaceAmount, String adbFaceAmount, String sundyPaymentAmount) end");
    return hs_result;
  }

  public Object getPlanInquiryDetails(String Authenticate[], String planCode) {
    HashMap hs_result = null;
    StringBuffer strBuffer = new StringBuffer();
    strBuffer.append(createLogin(Authenticate))
        .append("<TXLifeRequest>")
        .append("<TransRefGUID>2996b11f-e5b2-d00f-281b-03582683cd49</TransRefGUID>")
        .append("<TransType tc=\"PlanInquiry\">PlanInquiry</TransType>")
        .append("<TransExeDate>2008-07-04</TransExeDate>")
        .append("<TransExeTime>18:32:55</TransExeTime>")
        .append("<OLifE>")
        .append("<PlanInquiryData>")
        .append("<MirCommonFields>")
        .append("<MirPlanId>")
        .append(planCode)
        .append("</MirPlanId>")
        .append("</MirCommonFields>")
        .append("</PlanInquiryData>")
        .append("</OLifE>")
        .append("</TXLifeRequest>")
        .append("</TXLife>");

    try {
      StringBuffer sb = new StringBuffer();
      sb.append("PlanInquiryDetails_PlanID-");
      sb.append(planCode);

      hs_result = invokeIngenium.submit(strBuffer.toString(), sb.toString());

      if (printParsedXML) {
        Object strkeys[] = hs_result.keySet().toArray();
        for (int i = 0; i < strkeys.length; i++) {
          ArrayList al_result = (ArrayList) hs_result.get(strkeys[i]);
          TheLogger.info("TheLogger strkeys: " + strkeys[i] + ": ");
          for (int j = 0; j < al_result.size(); j++) {
            TheLogger.info("\t" + al_result.get(j));
          }
        }
      }
      TheLogger.debug("InquiryConsolidatedInformation was called using client number");
    } catch (Exception ex) {
      TheLogger.error(ex.getMessage());
    }
    return hs_result;
  }

  /*
   * Handles the AddressList business requests
   * retrieves list of Address List
   * GFrag - 10/13/2014 for FATCA phase 2
   */
  public Object getAddressList_request(String Authenticate[], String strClientId) {
    HashMap hs_result = null;
    StringBuffer strBuffer = new StringBuffer();
    strBuffer.append(createLogin(Authenticate));

    strBuffer.append("<TXLifeRequest>");
    strBuffer.append("<TransRefGUID>2996b11f-e5b2-d00f-281b-03582683cd49</TransRefGUID> ");
    strBuffer.append("<TransType tc=\"AddressList\">AddressList</TransType>");
    strBuffer.append("<TransExeDate>");
    strBuffer.append(exeDate);
    strBuffer.append("</TransExeDate> ");
    strBuffer.append("<TransExeTime>");
    strBuffer.append(exeTime);
    strBuffer.append("</TransExeTime> ");
    strBuffer.append("<OLifE>");
    strBuffer.append("<ReturnCds/>");
    strBuffer.append("<AddressData>");
    strBuffer.append("<SrcSystem>WMS</SrcSystem>");
    strBuffer.append("<MirKeyFields>");
    strBuffer.append("<MirCliId>");
    strBuffer.append(strClientId);
    strBuffer.append("</MirCliId>");
    strBuffer.append("</MirKeyFields>");
    strBuffer.append("</AddressData>");
    strBuffer.append("<MirUserMsgG/>");
    strBuffer.append("</OLifE>");
    strBuffer.append("</TXLifeRequest>");
    strBuffer.append("</TXLife> ");

    try {
      StringBuffer sb = new StringBuffer();
      sb.append("AddressList-");
      sb.append(strClientId);
      hs_result = invokeIngenium.submit(strBuffer.toString(), sb.toString());
      if (printParsedXML) {
        Object strkeys[] = hs_result.keySet().toArray();
        for (int i = 0; i < strkeys.length; i++) {
          ArrayList al_result = (ArrayList) hs_result.get(strkeys[i]);
          TheLogger.info(strkeys[i] + ": ");
          for (int j = 0; j < al_result.size(); j++)
            TheLogger.info("\t" + al_result.get(j));
        }
      }
      TheLogger.debug("Addresslist was called using client no");
    } catch (Exception ex) {
      TheLogger.error(ex.getMessage());
    }
    return hs_result;
  }

  /*
   * Handles the Client Telephone List business requests
   * retrieves list of Contact info
   * GFrag - 10/13/2014 for FATCA phase 2
   */
  public Object getClientTelephoneList_request(String Authenticate[], String strClientId) {
    HashMap hs_result = null;
    StringBuffer strBuffer = new StringBuffer();

    strBuffer.append(createLogin(Authenticate));
    strBuffer.append("<TXLifeRequest>");
    strBuffer.append("<TransRefGUID>2996b11f-e5b2-d00f-281b-03582683cd49</TransRefGUID> ");
    strBuffer.append("<TransType tc=\"ClientTelephoneList\">ClientTelephoneList</TransType>");
    strBuffer.append("<TransExeDate>");
    strBuffer.append(exeDate);
    strBuffer.append("</TransExeDate> ");
    strBuffer.append("<TransExeTime>");
    strBuffer.append(exeTime);
    strBuffer.append("</TransExeTime> ");
    strBuffer.append("<OLifE>");
    strBuffer.append("<ReturnCds/>");
    strBuffer.append("<ClientTelephoneData>");
    strBuffer.append("<SrcSystem>WMS</SrcSystem>");
    strBuffer.append("<MirCommonFields>");
    strBuffer.append("<MirCliId>");
    strBuffer.append(strClientId);
    strBuffer.append("</MirCliId>");
    strBuffer.append("</MirCommonFields>");
    strBuffer.append("</ClientTelephoneData>");
    strBuffer.append("</OLifE>");
    strBuffer.append("</TXLifeRequest>");
    strBuffer.append("</TXLife> ");

    try {
      StringBuffer sb = new StringBuffer();
      sb.append("ClientTelephoneList-");
      sb.append(strClientId);
      hs_result = invokeIngenium.submit(strBuffer.toString(), sb.toString());
      if (printParsedXML) {
        Object strkeys[] = hs_result.keySet().toArray();
        for (int i = 0; i < strkeys.length; i++) {
          ArrayList al_result = (ArrayList) hs_result.get(strkeys[i]);
          TheLogger.info(strkeys[i] + ": ");
          for (int j = 0; j < al_result.size(); j++)
            TheLogger.info("\t" + al_result.get(j));
        }
      }
      TheLogger.debug("Client Telephone List was called using client no");
    } catch (Exception ex) {
      TheLogger.error(ex.getMessage());
    }
    return hs_result;
  }

  /*
   * Handles the Client ID Forms List business requests
   * retrieves list of ID Forms info
   * GFrag - 10/13/2014 for FATCA phase 2
   */
  public Object getClientIDFormsList_request(String Authenticate[], String strClientId) {
    HashMap hs_result = null;
    StringBuffer strBuffer = new StringBuffer();

    strBuffer.append(createLogin(Authenticate));
    strBuffer.append("<TXLifeRequest>");
    strBuffer.append("<TransRefGUID>2996b11f-e5b2-d00f-281b-03582683cd49</TransRefGUID> ");
    strBuffer.append("<TransType tc=\"ClientIDFormsList\">ClientIDFormsList</TransType>");
    strBuffer.append("<TransExeDate>");
    strBuffer.append(exeDate);
    strBuffer.append("</TransExeDate> ");
    strBuffer.append("<TransExeTime>");
    strBuffer.append(exeTime);
    strBuffer.append("</TransExeTime> ");
    strBuffer.append("<OLifE>");
    strBuffer.append("<ReturnCds/>");
    strBuffer.append("<ClientIdFormsData>");
    strBuffer.append("<SrcSystem>WMS</SrcSystem>");
    strBuffer.append("<MirCliId>");
    strBuffer.append(strClientId);
    strBuffer.append("</MirCliId>");
    strBuffer.append("</ClientIdFormsData>");
    strBuffer.append("</OLifE>");
    strBuffer.append("</TXLifeRequest>");
    strBuffer.append("</TXLife> ");

    try {
      StringBuffer sb = new StringBuffer();
      sb.append("ClientIDFormsList-");
      sb.append(strClientId);
      hs_result = invokeIngenium.submit(strBuffer.toString(), sb.toString());
      if (printParsedXML) {
        Object strkeys[] = hs_result.keySet().toArray();
        for (int i = 0; i < strkeys.length; i++) {
          ArrayList al_result = (ArrayList) hs_result.get(strkeys[i]);
          TheLogger.info(strkeys[i] + ": ");
          for (int j = 0; j < al_result.size(); j++)
            TheLogger.info("\t" + al_result.get(j));
        }
      }
      TheLogger.debug("Client Client ID Forms List was called using client no");
    } catch (Exception ex) {
      TheLogger.error(ex.getMessage());
    }
    return hs_result;
  }


  /*
   * Handles the Entity Substantial US Owner List business requests
   * Playug - 10/20/2015 for FATCA phase 3B
   */
  public Object getEntitySubstantialUSOwnerList_request(String Authenticate[], String strClientId) {
    HashMap hs_result = null;
    StringBuffer strBuffer = new StringBuffer();
    String seqNumber = "000";

    strBuffer.append(createLogin(Authenticate));
    strBuffer.append("<TXLifeRequest>");
    strBuffer.append("<TransRefGUID/> ");
    strBuffer.append("<TransType tc=\"EntitySubstantialUSOwnerList\">EntitySubstantialUSOwnerList</TransType>");
    strBuffer.append("<OLifE>");
    strBuffer.append("<SubstantialUSOwnerData>");
    strBuffer.append("<MirCliId>");
    strBuffer.append(strClientId);
    strBuffer.append("</MirCliId>");
    strBuffer.append("<MirSownSeqNum>");
    strBuffer.append(seqNumber);
    strBuffer.append("</MirSownSeqNum>");
    strBuffer.append("</SubstantialUSOwnerData>");
    strBuffer.append("</OLifE>");
    strBuffer.append("</TXLifeRequest>");
    strBuffer.append("</TXLife> ");

    try {
      StringBuffer sb = new StringBuffer();
      sb.append("EntitySubstantialUSOwnerList-");
      sb.append(strClientId);
      hs_result = invokeIngenium.submit(strBuffer.toString(), sb.toString());
      if (printParsedXML) {
        Object strkeys[] = hs_result.keySet().toArray();
        for (int i = 0; i < strkeys.length; i++) {
          ArrayList al_result = (ArrayList) hs_result.get(strkeys[i]);
          TheLogger.info(strkeys[i] + ": ");
          for (int j = 0; j < al_result.size(); j++)
            TheLogger.info("\t" + al_result.get(j));
        }
      }
      TheLogger.debug("Entity Substantial US Owner List was called using client no");
    } catch (Exception ex) {
      TheLogger.error(ex.getMessage());
    }
    return hs_result;
  }
}
