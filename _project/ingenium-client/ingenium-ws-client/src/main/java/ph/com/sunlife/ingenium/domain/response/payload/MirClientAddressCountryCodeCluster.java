package ph.com.sunlife.ingenium.domain.response.payload;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

@XStreamAlias("MirCliAddrCntyCdG")
public class MirClientAddressCountryCodeCluster {

    @XStreamImplicit(itemFieldName = "MirCliAddrCntyCdT")
    private List<String> clientAddressCountryCodes;

    public List<String> getClientAddressCountryCodes() {
        return clientAddressCountryCodes;
    }

    public void setClientAddressCountryCodes(final List<String> countryCodes) {
        clientAddressCountryCodes = countryCodes;
    }
}
