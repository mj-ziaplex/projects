package ph.com.sunlife.ingenium.domain;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

@XStreamAlias("MirDvOwnSubCdG")
public class MirDvOwnSubCdG {

    @XStreamImplicit(itemFieldName = "MirDvOwnSubCdT")
    private List<String> mirDvOwnSubCdT;

    public List<String> getMirDvOwnSubCdT() {
        return mirDvOwnSubCdT;
    }

    public void setMirDvOwnSubCdT(List<String> mirDvOwnSubCdT) {
        this.mirDvOwnSubCdT = mirDvOwnSubCdT;
    }
}
