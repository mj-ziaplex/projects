package ph.com.sunlife.ingenium.domain;

import java.util.ArrayList;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("MirOpiBrIdG")
public class MirOpiBrIdG {

	@XStreamImplicit(itemFieldName = "MirOpiBrIdT")
	protected List<String> mirOpiBrIdT;
	@XStreamImplicit(itemFieldName = "MirOpiBrIdCdOnlyT")
	protected List<String> mirOpiBrIdCdOnlyT;

	public List<String> getMirOpiBrIdT() {
		if (mirOpiBrIdT == null) {
			mirOpiBrIdT = new ArrayList<String>();
		}
		return this.mirOpiBrIdT;
	}

	public List<String> getMirOpiBrIdCdOnlyT() {
		if (mirOpiBrIdCdOnlyT == null) {
			mirOpiBrIdCdOnlyT = new ArrayList<String>();
		}
		return this.mirOpiBrIdCdOnlyT;
	}

}
