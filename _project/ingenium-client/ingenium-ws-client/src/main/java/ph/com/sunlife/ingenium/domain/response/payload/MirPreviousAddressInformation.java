package ph.com.sunlife.ingenium.domain.response.payload;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("MirPrevAddrInfo")
public class MirPreviousAddressInformation {

    @XStreamAlias("MirDvPrevAddrIndG")
    public MirDvPreviousAddressIndGroup mirDvPreviousAddressIndGroup;
    @XStreamAlias("MirDvCliAddrCntyCdG")
    public MirDvClientAddressCountryCodeGroup mirDvClientAddressCountryCodeGroup;
    @XStreamAlias("MirDvCliAddrLn1TxtG")
    public MirDvClientAddressLine1TextGroup mirDvClientAddressLine1TextGroup;
    @XStreamAlias("MirDvCliAddrLn2TxtG")
    public MirDvClientAddressLine2TextGroup mirDvClientAddressLine2TextGroup;
    @XStreamAlias("MirDvCliAddrLn3TxtG")
    public MirDvClientAddressLine3TextGroup mirDvClientAddressLine3TextGroup;
    @XStreamAlias("MirDvCliAddrAddlTxtG")
    public MirDvClientAddressAdditionalTextGroup mirDvClientAddressAdditionalTextGroup;
    @XStreamAlias("MirDvCliAddrMunCdG")
    public MirDvClientAddressMuninicipalityCodeGroup mirDvClientAddressMuninicipalityCodeGroup;
    @XStreamAlias("MirDvCliAddrYrDurG")
    public MirDvClientAddressYearDurationGroup mirDvClientAddressYearDurationGroup;
    @XStreamAlias("MirDvCliCityNmTxtG")
    public MirDvClientCityNameTextGroup mirDvClientCityNameTextGroup;
    @XStreamAlias("MirDvCliCrntLocCdG")
    public MirDvClientCrntLocationCodeGroup mirDvClientCrntLocationCodeGroup;
    @XStreamAlias("MirDvCliCtryCdG")
    public MirDvClientCountryCodeGroup mirDvClientCountryCodeGroup;
    @XStreamAlias("MirDvCliPstlCdG")
    public MirDvClientPostalCodeGroup mirDvClientPostalCodeGroup;
    @XStreamAlias("MirDvCliAltAddrCdG")
    public MirDvClientAlternateAddressCodeGroup mirDvClientAlternateAddressCodeGroup;
    @XStreamAlias("MirDvCliResNumG")
    public MirDvClientResidenceNumberGroup mirDvClientResidenceNumberGRoup;
    @XStreamAlias("MirDvCliResTypCdG")
    public MirDvClientResidenceTypeCodeGroup mirDvClientResidenceTypeCodeGroup;
    @XStreamAlias("MirDvCliAddrCntctTxtG")
    public MirDvClientAddressContactTextGroup mirDvClientAddressContactTextGroup;
    @XStreamAlias("MirDvCliAddrEffDtG")
    public MirDvClientAddressEffectivityDateGroup mirDvClientAddressEffectivityDateGroup;
    @XStreamAlias("MirDvCliAddrUpdByLnG")
    public MirDvClientAddressUpdatedByLineGroup mirDvClientAddressUpdatedByLineGroup;
    @XStreamAlias("MirDvCliAddrLstUpdLnG")
    public MirDvClientAddressLastUpdatedLineGroup mirDvClientAddressLastUpdatedLineGroup;
}
