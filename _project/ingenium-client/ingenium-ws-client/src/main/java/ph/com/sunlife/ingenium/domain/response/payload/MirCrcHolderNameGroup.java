package ph.com.sunlife.ingenium.domain.response.payload;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

@XStreamAlias("MirCrcHldrNmG")
public class MirCrcHolderNameGroup {

    @XStreamImplicit(itemFieldName = "MirCrcHldrNmT")
    protected List<String> mirCrcHloderNameTexts;
}
