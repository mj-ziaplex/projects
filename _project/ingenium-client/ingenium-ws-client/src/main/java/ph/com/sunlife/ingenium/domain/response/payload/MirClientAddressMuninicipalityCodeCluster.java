package ph.com.sunlife.ingenium.domain.response.payload;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

@XStreamAlias("MirCliAddrMunCdG")
public class MirClientAddressMuninicipalityCodeCluster {

    @XStreamImplicit(itemFieldName = "MirCliAddrMunCdT")
    private List<String> clientAddressMunicipalityCodes;

    public List<String> getClientAddressMunicipalityCodes() {
        return clientAddressMunicipalityCodes;
    }

    public void setClientAddressMunicipalityCodes(final List<String> municipalityCodes) {
        clientAddressMunicipalityCodes = municipalityCodes;
    }
}
