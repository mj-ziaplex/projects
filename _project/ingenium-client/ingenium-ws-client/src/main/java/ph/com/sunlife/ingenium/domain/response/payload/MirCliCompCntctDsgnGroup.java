package ph.com.sunlife.ingenium.domain.response.payload;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

@XStreamAlias("MirCliCompCntctDsgnG")
public class MirCliCompCntctDsgnGroup {

    @XStreamImplicit(itemFieldName = "MirCliCompCntctDsgnT")
    private List<String> mirCliCompCntctDsgnTexts;

    public List<String> getMirCliCompCntctDsgnTexts() {
        return mirCliCompCntctDsgnTexts;
    }

    public void setMirCliCompCntctDsgnTexts(List<String> mirCliCompCntctDsgnTexts) {
        this.mirCliCompCntctDsgnTexts = mirCliCompCntctDsgnTexts;
    }
}
