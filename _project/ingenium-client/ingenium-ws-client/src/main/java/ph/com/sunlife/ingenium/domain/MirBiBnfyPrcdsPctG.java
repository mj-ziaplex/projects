package ph.com.sunlife.ingenium.domain;

import java.util.ArrayList;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("MirBiBnfyPrcdsPctG")
public class MirBiBnfyPrcdsPctG {

	@XStreamImplicit(itemFieldName = "MirBiBnfyPrcdsPctT")
	protected List<String> mirBiBnfyPrcdsPctT;

	public List<String> getMirBiBnfyPrcdsPctT() {
		if (mirBiBnfyPrcdsPctT == null) {
			mirBiBnfyPrcdsPctT = new ArrayList<String>();
		}
		return this.mirBiBnfyPrcdsPctT;
	}

}
