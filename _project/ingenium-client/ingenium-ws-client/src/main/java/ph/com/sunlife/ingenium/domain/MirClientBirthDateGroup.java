package ph.com.sunlife.ingenium.domain;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

@XStreamAlias("MirCliBthDtG")
public class MirClientBirthDateGroup {

    @XStreamImplicit(itemFieldName = "MirCliBthDtT")
    private List<String> mirCliBthDtT;

    public List<String> getMirCliBthDtT() {
        return mirCliBthDtT;
    }

    public void setMirCliBthDtT(List<String> mirCliBthDtT) {
        this.mirCliBthDtT = mirCliBthDtT;
    }
}
