package ph.com.sunlife.ingenium.ws.domain;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("TXLifeResponse")
public class PlanInquiryTransactionResponse {

    @XStreamAlias("TransType")
    private TransactionType transactionType;
    @XStreamAlias("TransResult")
    private TransactionResult transactionResult;
    @XStreamAlias("OLifE")
    private PlanInquiryContainer container;

    public TransactionType getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(TransactionType transactionType) {
        this.transactionType = transactionType;
    }

    public TransactionResult getTransactionResult() {
        return transactionResult;
    }

    public void setTransactionResult(TransactionResult transactionResult) {
        this.transactionResult = transactionResult;
    }

    public PlanInquiryContainer getContainer() {
        return container;
    }

    public void setContainer(PlanInquiryContainer container) {
        this.container = container;
    }
}
