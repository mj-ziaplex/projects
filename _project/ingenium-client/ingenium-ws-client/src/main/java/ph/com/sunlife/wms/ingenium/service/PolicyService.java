package ph.com.sunlife.wms.ingenium.service;

import ph.com.sunlife.wms.ingenium.domain.ConsolidatedInformation;
import ph.com.sunlife.wms.ingenium.domain.Coverage;
import ph.com.sunlife.wms.ingenium.domain.Requirement;

import java.util.List;

public interface PolicyService {

  Coverage getCoverageInformation(final String policyId) throws Exception;

  byte getStructedNoteStatus(String policyId);

  ConsolidatedInformation getPolicyClientDetails(final String policyId) throws Exception;

  List<Requirement> getRequirement(final String policyId);

  String getPlanId(final String policyId);

  List<String> getPolicyAssignees(String policyId);

  boolean createRequirement(final String mirPolicyIdBase,
                            final String requirementId,
                            final String status);
}
