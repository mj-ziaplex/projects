package ph.com.sunlife.ingenium.domain.response.payload;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("MirPrevNmInfo")
public class MirPreviousNameInformation {

    @XStreamAlias("MirDvCliPrevNmInd")
    private String mirDvCliPrevNmInd;
    @XStreamAlias("MirDvCliIndvMidNmG")
    private MirDvClientIndvMiddlenameGroup mirDvClientIndvMiddlenames;
    @XStreamAlias("MirDvCliIndvSfxNmG")
    private MirDvClientIndividualSuffixNameGroup mirDvClientIndividualSuffixNameGroup;
    @XStreamAlias("MirDvCliIndvTitlTxtG")
    private MirDvClientIndividualTitleTextGroup mirDvClientIndividualTitleTextGroup;
    @XStreamAlias("MirDvEntrSurNmG")
    private MirDvEntrySurnameGroup mirDvEntrySurnameGroup;
    @XStreamAlias("MirDvEntrGivNmG")
    private MirDvEntryGivenNameGroup mirDvEntryGivenNameGroup;
    @XStreamAlias("MirDvCliIndvEffDtG")
    private MirDvClientIndividualEffectivityDateGroup mirDvClientIndividualEffectivityDateGroup;
    @XStreamAlias("MirDvCliUpdByLn1G")
    private MirDvClientUpdateddByLn1Group mirDvClientUpdateddByLn1Group;
    @XStreamAlias("MirDvCliUpdDtLn1G")
    private MirDvClientUpdatedDateLn1Group mirDvClientUpdatedDateLn1Group;

    public String getMirDvCliPrevNmInd() {
        return mirDvCliPrevNmInd;
    }

    public void setMirDvCliPrevNmInd(String mirDvCliPrevNmInd) {
        this.mirDvCliPrevNmInd = mirDvCliPrevNmInd;
    }

    public MirDvClientIndvMiddlenameGroup getMirDvClientIndvMiddlenames() {
        return mirDvClientIndvMiddlenames;
    }

    public void setMirDvClientIndvMiddlenames(MirDvClientIndvMiddlenameGroup mirDvClientIndvMiddlenames) {
        this.mirDvClientIndvMiddlenames = mirDvClientIndvMiddlenames;
    }

    public MirDvClientIndividualSuffixNameGroup getMirDvClientIndividualSuffixNameGroup() {
        return mirDvClientIndividualSuffixNameGroup;
    }

    public void setMirDvClientIndividualSuffixNameGroup(MirDvClientIndividualSuffixNameGroup mirDvClientIndividualSuffixNameGroup) {
        this.mirDvClientIndividualSuffixNameGroup = mirDvClientIndividualSuffixNameGroup;
    }

    public MirDvClientIndividualTitleTextGroup getMirDvClientIndividualTitleTextGroup() {
        return mirDvClientIndividualTitleTextGroup;
    }

    public void setMirDvClientIndividualTitleTextGroup(MirDvClientIndividualTitleTextGroup mirDvClientIndividualTitleTextGroup) {
        this.mirDvClientIndividualTitleTextGroup = mirDvClientIndividualTitleTextGroup;
    }

    public MirDvEntrySurnameGroup getMirDvEntrySurnameGroup() {
        return mirDvEntrySurnameGroup;
    }

    public void setMirDvEntrySurnameGroup(MirDvEntrySurnameGroup mirDvEntrySurnameGroup) {
        this.mirDvEntrySurnameGroup = mirDvEntrySurnameGroup;
    }

    public MirDvEntryGivenNameGroup getMirDvEntryGivenNameGroup() {
        return mirDvEntryGivenNameGroup;
    }

    public void setMirDvEntryGivenNameGroup(MirDvEntryGivenNameGroup mirDvEntryGivenNameGroup) {
        this.mirDvEntryGivenNameGroup = mirDvEntryGivenNameGroup;
    }

    public MirDvClientIndividualEffectivityDateGroup getMirDvClientIndividualEffectivityDateGroup() {
        return mirDvClientIndividualEffectivityDateGroup;
    }

    public void setMirDvClientIndividualEffectivityDateGroup(MirDvClientIndividualEffectivityDateGroup mirDvClientIndividualEffectivityDateGroup) {
        this.mirDvClientIndividualEffectivityDateGroup = mirDvClientIndividualEffectivityDateGroup;
    }

    public MirDvClientUpdateddByLn1Group getMirDvClientUpdateddByLn1Group() {
        return mirDvClientUpdateddByLn1Group;
    }

    public void setMirDvClientUpdateddByLn1Group(MirDvClientUpdateddByLn1Group mirDvClientUpdateddByLn1Group) {
        this.mirDvClientUpdateddByLn1Group = mirDvClientUpdateddByLn1Group;
    }

    public MirDvClientUpdatedDateLn1Group getMirDvClientUpdatedDateLn1Group() {
        return mirDvClientUpdatedDateLn1Group;
    }

    public void setMirDvClientUpdatedDateLn1Group(MirDvClientUpdatedDateLn1Group mirDvClientUpdatedDateLn1Group) {
        this.mirDvClientUpdatedDateLn1Group = mirDvClientUpdatedDateLn1Group;
    }
}
