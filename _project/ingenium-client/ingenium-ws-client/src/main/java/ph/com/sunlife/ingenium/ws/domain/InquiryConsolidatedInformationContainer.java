package ph.com.sunlife.ingenium.ws.domain;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import ph.com.sunlife.ingenium.domain.MirUserMsgSevrtyG;
import ph.com.sunlife.ingenium.domain.MirUserMsgTxtG;

public class InquiryConsolidatedInformationContainer extends Container  {

    @XStreamAlias("InquiryConsolidatedInformationData")
    private InquiryConsolidatedInformationData inquiryConsolidatedInformationData;
    @XStreamAlias("MirUserMsgTxtG")
    private MirUserMsgTxtG mirUserMsgTxtG;
    @XStreamAlias("MirUserMsgSevrtyG")
    private MirUserMsgSevrtyG mirUserMsgSevrtyG;

    public InquiryConsolidatedInformationData getInquiryConsolidatedInformationData() {
        return inquiryConsolidatedInformationData;
    }

    public InquiryConsolidatedInformationContainer setInquiryConsolidatedInformationData(InquiryConsolidatedInformationData inquiryConsolidatedInformationData) {
        this.inquiryConsolidatedInformationData = inquiryConsolidatedInformationData;
        return this;
    }

    public MirUserMsgTxtG getMirUserMsgTxtG() {
        return mirUserMsgTxtG;
    }

    public void setMirUserMsgTxtG(MirUserMsgTxtG mirUserMsgTxtG) {
        this.mirUserMsgTxtG = mirUserMsgTxtG;
    }

    public MirUserMsgSevrtyG getMirUserMsgSevrtyG() {
        return mirUserMsgSevrtyG;
    }

    public void setMirUserMsgSevrtyG(MirUserMsgSevrtyG mirUserMsgSevrtyG) {
        this.mirUserMsgSevrtyG = mirUserMsgSevrtyG;
    }
}
