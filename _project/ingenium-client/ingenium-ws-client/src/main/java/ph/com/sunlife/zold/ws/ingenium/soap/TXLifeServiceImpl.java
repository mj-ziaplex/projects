package ph.com.sunlife.zold.ws.ingenium.soap;

import com.sun.xml.rpc.client.BasicService;
import com.sun.xml.rpc.client.HandlerChainImpl;
import com.sun.xml.rpc.client.ServiceExceptionImpl;
import com.sun.xml.rpc.util.exception.LocalizableExceptionAdapter;

import javax.xml.namespace.QName;
import javax.xml.rpc.JAXRPCException;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class TXLifeServiceImpl extends BasicService implements TXLifeService {
  private static final QName serviceName = new QName("http://webservices.elink.solcorp.com", "TXLifeService");
  private static final QName ns1_TXLifeService_QNAME = new QName("http://webservices.elink.solcorp.com", "TXLifeService");
  private static final Class TXLifeServicePort_PortClass = TXLifeServicePort.class;
  private BufferedWriter out = null;

  public TXLifeServiceImpl() {
    super(serviceName, new QName[]{ns1_TXLifeService_QNAME},
        new TXLifeServiceSerializerRegistry().getRegistry());
  }

  public java.rmi.Remote getPort(QName portName, Class serviceDefInterface) throws javax.xml.rpc.ServiceException {
    try {
      if (portName.equals(ns1_TXLifeService_QNAME) && serviceDefInterface.equals(TXLifeServicePort_PortClass)) {
        return getTXLifeService();
      }
    } catch (Exception e) {
      throw new ServiceExceptionImpl(new LocalizableExceptionAdapter(e));
    }
    return super.getPort(portName, serviceDefInterface);
  }

  public java.rmi.Remote getPort(Class serviceDefInterface) throws javax.xml.rpc.ServiceException {
    try {
      if (serviceDefInterface.equals(TXLifeServicePort_PortClass)) {
        return getTXLifeService();
      }
    } catch (Exception e) {
      throw new ServiceExceptionImpl(new LocalizableExceptionAdapter(e));
    }
    return super.getPort(serviceDefInterface);
  }

  public void initializeLog() {
    try {
      File logFile = new File("D:\\WMS_Files\\Logdir\\InvokeIngenium.log");
      if (logFile.exists()) {
        logFile.createNewFile();
      }

      out = new BufferedWriter(new FileWriter(logFile, true));
    } catch (IOException e) {
    }
  }

  public TXLifeServicePort getTXLifeService() {
    String[] roles = new String[]{};
    HandlerChainImpl handlerChain = new HandlerChainImpl(getHandlerRegistry().getHandlerChain(ns1_TXLifeService_QNAME));
    handlerChain.setRoles(roles);
    TXLifeServicePort_Stub stub = new TXLifeServicePort_Stub(
        handlerChain);
    try {
      stub._initialize(super.internalTypeRegistry);
    } catch (JAXRPCException e) {
      throw e;
    } catch (Exception e) {
      throw new JAXRPCException(e.getMessage(), e);
    }
    return stub;
  }
}