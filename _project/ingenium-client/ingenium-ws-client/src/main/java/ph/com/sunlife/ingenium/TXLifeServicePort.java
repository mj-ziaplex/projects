package ph.com.sunlife.ingenium;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.xml.bind.annotation.XmlSeeAlso;

@WebService(name = "TXLifeServicePort",
            targetNamespace = "http://webservices.elink.solcorp.com")
@SOAPBinding(parameterStyle = SOAPBinding.ParameterStyle.BARE)
@XmlSeeAlso({TXLifeServiceFactory.class})
interface TXLifeServicePort {

  @WebMethod
  @WebResult(name = "callTXLifeResponse",
      targetNamespace = "http://schema/webservices.elink.solcorp.com",
      partName = "response")
  String callTXLife(@WebParam(name = "callTXLifeRequest",
          targetNamespace = "http://schema/webservices.elink.solcorp.com",
          partName = "request") String request);
}
