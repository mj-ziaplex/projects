package ph.com.sunlife.wms.ingenium.domain;

import java.util.List;

public class Policy {

    private String planId;
    private List<String> assignees;
    private String policyContractOutputType;

    public String getPlanId() {
        return planId;
    }

    public Policy setPlanId(String planId) {
        this.planId = planId;
        return this;
    }

    public String getPolicyContractOutputType() {
        return policyContractOutputType;
    }

    public Policy setPolicyContractOutputType(String type) {
        this.policyContractOutputType = type;
        return this;
    }

    public List<String> getAssignees() {
        return assignees;
    }

    public Policy setAssignees(List<String> assignees) {
        this.assignees = assignees;
        return this;
    }
}
