package ph.com.sunlife.ingenium.ws.domain;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("UserPswd")
public class UserPassword {

    @XStreamAlias("CryptType")
    private String cryptographicType;
    @XStreamAlias("Pswd")
    private String password;

    public String getCryptographicType() {
        return cryptographicType;
    }

    public void setCryptographicType(final String type) {
        cryptographicType = type;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(final String value) {
        this.password = value;
    }
}
