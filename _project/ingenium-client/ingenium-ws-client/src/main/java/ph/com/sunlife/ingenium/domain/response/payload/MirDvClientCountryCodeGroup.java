package ph.com.sunlife.ingenium.domain.response.payload;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

@XStreamAlias("MirDvCliCtryCdG")
public class MirDvClientCountryCodeGroup {

    @XStreamImplicit(itemFieldName = "MirDvCliCtryCdT")
    private List<String> dvClientCountryCodeTexts;

    public List<String> getDvClientCountryCodeTexts() {
        return dvClientCountryCodeTexts;
    }

    public void setDvClientCountryCodeTexts(final List<String> postalCodes) {
        dvClientCountryCodeTexts = postalCodes;
    }
}
