package ph.com.sunlife.ingenium.domain.response.payload;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

@XStreamAlias("MirEntrSurNmG")
public class MirEntrySurnameGroup {

    @XStreamImplicit(itemFieldName = "MirEntrSurNmT")
    private List<String> mirEntrySurnameTexts;

    public List<String> getMirEntrySurnameTexts() {
        return mirEntrySurnameTexts;
    }

    public void setMirEntrySurnameTexts(final List<String> surnameTexts) {
        mirEntrySurnameTexts = surnameTexts;
    }
}
