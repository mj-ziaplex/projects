package ph.com.sunlife.ingenium;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ph.com.sunlife.wms.ingenium.configuration.IngeniumClientConfigurator;

import java.net.URL;

public class IngeniumWebServiceClient {

  private final TXLifeServicePort port;

  public IngeniumWebServiceClient() {
    ApplicationContext context =
            new AnnotationConfigApplicationContext(IngeniumClientConfigurator.class);
    URL wsdlUrl = (URL) context.getBean("wsdlURL");
    TXLifeService service = new TXLifeService(wsdlUrl);
    port = service.getTXLifeService();
  }

  public String getResponse(final String request) {
    return port.callTXLife(request);
  }
}
