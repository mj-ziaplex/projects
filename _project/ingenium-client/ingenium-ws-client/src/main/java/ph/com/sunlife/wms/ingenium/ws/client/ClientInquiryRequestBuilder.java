package ph.com.sunlife.wms.ingenium.ws.client;

import ph.com.sunlife.ingenium.domain.MirClientInformation;
import ph.com.sunlife.ingenium.ws.domain.ClientData;
import ph.com.sunlife.ingenium.ws.domain.ClientInquiryContainer;
import ph.com.sunlife.ingenium.ws.domain.Transaction;

public class ClientInquiryRequestBuilder extends IngeniumRequestBuilder {

    private final String clientId;

    public ClientInquiryRequestBuilder(final String clientId) {
        this(clientId, "");
    }

    public ClientInquiryRequestBuilder(final String clientId,
                                       final String transactionReferenceGUID) {
        super();
        this.clientId = clientId;
        this.transactionReferenceGUID = transactionReferenceGUID;
    }

    @Override
    protected Transaction getTransaction() {
        MirClientInformation clientInformation = new MirClientInformation();
        clientInformation.setId(clientId);
        ClientData clientData = new ClientData();
        clientData.setMirClientInformation(clientInformation);
        ClientInquiryContainer container = new ClientInquiryContainer();
        container.setClientData(clientData);
        Transaction transaction = new Transaction();
        transaction.setReferenceGUID(transactionReferenceGUID);
        transaction.setType("ClientInquiry");
        transaction.setExecutionDate(getDateFormatter().format(executionTimestamp));
        transaction.setExecutionTime(getTimeFormatter().format(executionTimestamp));
        transaction.setContainer(container);
        return transaction;
    }
}
