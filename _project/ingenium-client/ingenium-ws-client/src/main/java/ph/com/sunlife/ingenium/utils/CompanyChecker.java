package ph.com.sunlife.ingenium.utils;

public class CompanyChecker {

  public static final boolean isSLOCPI(final String policyId) {
    return "05".equals(policyId.substring(0, 2)) ||
           "08".equals(policyId.substring(0, 2));
  }

  public static final boolean isSLGFI(final String policyId) {
    return "25".equals(policyId.substring(0, 2)) ||
           "28".equals(policyId.substring(0, 2));
  }

  private static CompanyChecker instance = new CompanyChecker();
  private CompanyChecker() {}
  public static CompanyChecker getInstance() { return instance; }
}
