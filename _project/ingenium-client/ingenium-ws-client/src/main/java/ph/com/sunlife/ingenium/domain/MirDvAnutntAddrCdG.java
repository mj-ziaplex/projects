package ph.com.sunlife.ingenium.domain;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

@XStreamAlias("MirDvAnutntAddrCdG")
public class MirDvAnutntAddrCdG {

    @XStreamImplicit(itemFieldName = "MirDvAnutntAddrCdT")
    private List<String> mirDvAnutntAddrCdT;

    public List<String> getMirDvAnutntAddrCdT() {
        return mirDvAnutntAddrCdT;
    }

    public void setMirDvAnutntAddrCdT(List<String> mirDvAnutntAddrCdT) {
        this.mirDvAnutntAddrCdT = mirDvAnutntAddrCdT;
    }
}
