package ph.com.sunlife.ingenium.domain.response.payload;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

@XStreamAlias("MirBnkAcctMicrIndG")
public class MirBankAccountMicrIndGroup {

    @XStreamImplicit(itemFieldName = "MirBnkAcctMicrIndT")
    private List<String> mirBankAccountMicrIndTypes;

    public List<String> getMirBankAccountMicrIndTypes() {
        return mirBankAccountMicrIndTypes;
    }

    public MirBankAccountMicrIndGroup setMirBankAccountMicrIndTypes(final List<String> types) {
        this.mirBankAccountMicrIndTypes = types;
        return this;
    }
}
