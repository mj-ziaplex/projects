package ph.com.sunlife.ingenium.domain;

import java.util.ArrayList;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("MirCpreqValidDtG")
public class MirCpreqValidDtG {

	@XStreamImplicit(itemFieldName = "MirCpreqValidDtT")
	protected List<String> mirCpreqValidDtT;

	public List<String> getMirCpreqValidDtT() {
		if (mirCpreqValidDtT == null) {
			mirCpreqValidDtT = new ArrayList<String>();
		}
		return this.mirCpreqValidDtT;
	}

}
