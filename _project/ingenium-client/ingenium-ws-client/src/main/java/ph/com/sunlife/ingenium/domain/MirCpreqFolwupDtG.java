package ph.com.sunlife.ingenium.domain;

import java.util.ArrayList;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("MirCpreqFolwupDtG")
public class MirCpreqFolwupDtG {

	@XStreamImplicit(itemFieldName = "MirCpreqFolwupDtT")
	protected List<String> mirCpreqFolwupDtT;

	public List<String> getMirCpreqFolwupDtT() {
		if (mirCpreqFolwupDtT == null) {
			mirCpreqFolwupDtT = new ArrayList<String>();
		}
		return this.mirCpreqFolwupDtT;
	}

}
