package ph.com.sunlife.ingenium.domain.response.payload;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

@XStreamAlias("MirCliIndvGivNmG")
public class MirClientIndividualGivenNameGroup {

    @XStreamImplicit(itemFieldName = "MirCliIndvGivNmT")
    private List<String> mirClientIndividualGivenNameTexts;

    public List<String> getMirClientIndividualGivenNameTexts() {
        return mirClientIndividualGivenNameTexts;
    }

    public MirClientIndividualGivenNameGroup setMirClientIndividualGivenNameTexts(List<String> types) {
        mirClientIndividualGivenNameTexts = types;
        return this;
    }
}
