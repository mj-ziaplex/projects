package ph.com.sunlife.ingenium.ws.domain;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import ph.com.sunlife.ingenium.domain.*;

@XStreamAlias("InquiryConsolidatedInformationData")
public class InquiryConsolidatedInformationData {

	@XStreamAlias("MirPolId")
	private MirPolicyID mirPolicyId;
	@XStreamAlias("SrcSystem")
	private String sourceSystem;
	@XStreamAlias("MirPrevLtdDodAmt")
	private String mirPrevLtdDodAmt;
	@XStreamAlias("MirHiSystemDt")
	private String mirHiSystemDt;
	@XStreamAlias("MirHiDvOwnCliNm")
	private String mirHiDvOwnCliNm;
	@XStreamAlias("MirHiDvOwnCliBthDt")
	private String mirHiDvOwnCliBthDt;
	@XStreamAlias("MirHiDvInsrdCliNm")
	private String mirHiDvInsrdCliNm;
	@XStreamAlias("MirHiDvInsrdCliBthDt")
	private String mirHiDvInsrdCliBthDt;
	@XStreamAlias("MirHiPolCstatCd")
	private String mirHiPolCstatCd;
	@XStreamAlias("MirHiPlanId")
	private String mirHiPlanId;
	@XStreamAlias("MirHiPolBillTypCd")
	private String mirHiPolBillTypCd;
	@XStreamAlias("MirHiPolBillModeCd")
	private String mirHiPolBillModeCd;
	@XStreamAlias("MirHiPolTpremAmt")
	private String mirHiPolTpremAmt;
	@XStreamAlias("MirHiPolSndryAmt")
	private String mirHiPolSndryAmt;
	@XStreamAlias("MirHiPolIssEffDt")
	private String mirHiPolIssEffDt;
	@XStreamAlias("MirHiPolPrevMpremAmt")
	private String mirHiPolPrevMpremAmt;
	@XStreamAlias("MirHiPolPdToDtNum")
	private String mirHiPolPdToDtNum;
	@XStreamAlias("MirHiPolCrcyCd")
	private String mirHiPolCrcyCd;
	@XStreamAlias("MirHiOwnCliId")
	private String mirHiOwnCliId;
	@XStreamAlias("MirHiInsrdCliId")
	private String mirHiInsrdCliId;
	@XStreamAlias("MirHiPolTotBillAmt")
	private String mirHiPolTotBillAmt;
	@XStreamAlias("MirAsaDvLbillCliNm")
	private String mirAsaDvLbillCliNm;
	@XStreamAlias("MirAsaCliAddrLn1Txt")
	private String mirAsaCliAddrLn1Txt;
	@XStreamAlias("MirAsaCliAddrLn2Txt")
	private String mirAsaCliAddrLn2Txt;
	@XStreamAlias("MirAsaCliAddrLn3Txt")
	private String mirAsaCliAddrLn3Txt;
	@XStreamAlias("MirAsaCliAddrLn4Txt")
	private String mirAsaCliAddrLn4Txt;
	@XStreamAlias("MirAsaCliAddrLn5Txt")
	private String mirAsaCliAddrLn5Txt;
	@XStreamAlias("MirAsaCliAddrChngDt")
	private String mirAsaCliAddrChngDt;
	@XStreamAlias("MirAsaServAgtId")
	private String mirAsaServAgtId;
	@XStreamAlias("MirAsaDvServAgtCliNm")
	private String mirAsaDvServAgtCliNm;
	@XStreamAlias("MirAsaServBrId")
	private String mirAsaServBrId;
	@XStreamAlias("MirAsaServFeeOpt")
	private String mirAsaServFeeOpt;
	@XStreamAlias("MirAsaAgtStatCd")
	private String mirAsaAgtStatCd;
	@XStreamAlias("MirAsaPrevServAgtId")
	private String mirAsaPrevServAgtId;
	@XStreamAlias("MirPolPdfAmt")
	private String mirPolPdfAmt;
	@XStreamAlias("MirPolPdfAvbAmt")
	private String mirPolPdfAvbAmt;
	@XStreamAlias("MirPolPdfStringAmt")
	private String mirPolPdfStringAmt;
	@XStreamAlias("MirPolPdfStringRt")
	private String mirPolPdfStringRt;
	@XStreamAlias("MirCdCvgAcctTypCdG")
	private MirCdCvgAcctTypCdG mirCdCvgAcctTypCdG;
	@XStreamAlias("MirCdPlanIdG")
	private MirCdPlanIdG mirCdPlanIdG;
	@XStreamAlias("MirCdCvgFaceAmtG")
	private MirCdCvgFaceAmtG mirCdCvgFaceAmtG;
	@XStreamAlias("MirCdCvgMpremAmtG")
	private MirCdCvgMpremAmtG mirCdCvgMpremAmtG;
	@XStreamAlias("MirCdCvgIssEffDtG")
	private MirCdCvgIssEffDtG mirCdCvgIssEffDtG;
	@XStreamAlias("MirCdCvgSmkrCdG")
	private MirCdCvgSmkrCdG mirCdCvgSmkrCdG;
	@XStreamAlias("MirCdCvgSexCdG")
	private MirCdCvgSexCdG mirCdCvgSexCdG;
	@XStreamAlias("MirCdCvgRtAgeG")
	private MirCdCvgRtAgeG mirCdCvgRtAgeG;
	@XStreamAlias("MirCdCvgParCdG")
	private MirCdCvgParCdG mirCdCvgParCdG;
	@XStreamAlias("MirCdCvgCstatCdG")
	private MirCdCvgCstatCdG mirCdCvgCstatCdG;
	@XStreamAlias("MirCdCvgMatXpryDtG")
	private MirCdCvgMatXpryDtG mirCdCvgMatXpryDtG;
	@XStreamAlias("MirRtCvgNumG")
	private MirRtCvgNumG mirRtCvgNumG;
	@XStreamAlias("MirRtCvgFePremAmtG")
	private MirRtCvgFePremAmtG mirRtCvgFePremAmtG;
	@XStreamAlias("MirRtCvgMePremAmtG")
	private MirRtCvgMePremAmtG mirRtCvgMePremAmtG;
	@XStreamAlias("MirRtCvgFeUpremG")
	private MirRtCvgFeUpremG mirRtCvgFeUpremG;
	@XStreamAlias("MirRtCvgFeDurG")
	private MirRtCvgFeDurG mirRtCvgFeDurG;
	@XStreamAlias("MirRtCvgFePermUpremAmtG")
	private MirRtCvgFePermUpremAmtG mirRtCvgFePermUpremAmtG;
	@XStreamAlias("MirRtCvgMeFctG")
	private MirRtCvgMeFctG mirRtCvgMeFctG;
	@XStreamAlias("MirRtCvgMeDurG")
	private MirRtCvgMeDurG mirRtCvgMeDurG;
	@XStreamAlias("MirRtCvgFeReasnCdG")
	private MirRtCvgFeReasnCdG mirRtCvgFeReasnCdG;
	@XStreamAlias("MirRtCvgMeReasnCdG")
	private MirRtCvgMeReasnCdG mirRtCvgMeReasnCdG;
	@XStreamAlias("MirRtCvgRtAgeG")
	private MirRtCvgRtAgeG mirRtCvgRtAgeG;
	@XStreamAlias("MirRtCvgMeRatCdG")
	private MirRtCvgMeRatCdG mirRtCvgMeRatCdG;
	@XStreamAlias("MirPvDvMaxLoanAmt")
	private String mirPvDvMaxLoanAmt;
	@XStreamAlias("MirPvDvPolCsvAmt")
	private String mirPvDvPolCsvAmt;
	@XStreamAlias("MirPvDvLoanAmt")
	private String mirPvDvLoanAmt;
	@XStreamAlias("MirPvDvMcvCsvAmt")
	private String mirPvDvMcvCsvAmt;
	@XStreamAlias("MirPvPolDivOptCd")
	private String mirPvPolDivOptCd;
	@XStreamAlias("MirPvDvPrevDivYrQty")
	private String mirPvDvPrevDivYrQty;
	@XStreamAlias("MirPvDivDclrDurAmt")
	private String mirPvDivDclrDurAmt;
	@XStreamAlias("MirPvPolDodAcumAmt")
	private String mirPvPolDodAcumAmt;
	@XStreamAlias("MirPvPolDodStringRt")
	private String mirPvPolDodStringRt;
	@XStreamAlias("MirPvDvAnnvPyrQty")
	private String mirPvDvAnnvPyrQty;
	@XStreamAlias("MirPvPuaLtdFaceAmt")
	private String mirPvPuaLtdFaceAmt;
	@XStreamAlias("MirPvPuaYtdFaceAmt")
	private String mirPvPuaYtdFaceAmt;
	@XStreamAlias("MirAfdPolAeFndAmt")
	private String mirAfdPolAeFndAmt;
	@XStreamAlias("MirAfdPolAeFndStringAmt")
	private String mirAfdPolAeFndStringAmt;
	@XStreamAlias("MirAfdCvgOrigPlanId")
	private String mirAfdCvgOrigPlanId;
	@XStreamAlias("MirAfdPolAeLstWthdrAmt")
	private String mirAfdPolAeLstWthdrAmt;
	@XStreamAlias("MirAfdDvLoanBal")
	private String mirAfdDvLoanBal;
	@XStreamAlias("MirOpiPolNfoCd")
	private String mirOpiPolNfoCd;
	@XStreamAlias("MirOpiPolReinsCd")
	private String mirOpiPolReinsCd;
	@XStreamAlias("MirOpiPolReinsFaceAmt")
	private String mirOpiPolReinsFaceAmt;
	@XStreamAlias("MirOpiPolReinsAdbFaceAmt")
	private String mirOpiPolReinsAdbFaceAmt;
	@XStreamAlias("MirOpiPolReinsSndryAmt")
	private String mirOpiPolReinsSndryAmt;
	@XStreamAlias("MirOpiPolUlShrtAmt")
	private String mirOpiPolUlShrtAmt;
	@XStreamAlias("MirOpiUlLapsNotiAmt")
	private String mirOpiUlLapsNotiAmt;
	@XStreamAlias("MirOpiAgtIdG")
	private MirOpiAgtIdG mirOpiAgtIdG;
	@XStreamAlias("MirOpiDvAgtCliNmG")
	private MirOpiDvAgtCliNmG mirOpiDvAgtCliNmG;
	@XStreamAlias("MirOpiBrIdG")
	private MirOpiBrIdG mirOpiBrIdG;
	@XStreamAlias("MirOpiAgtStatCdG")
	private MirOpiAgtStatCdG mirOpiAgtStatCdG;
	@XStreamAlias("MirOpiPolAgtShrPctG")
	private MirOpiPolAgtShrPctG mirOpiPolAgtShrPctG;
	@XStreamAlias("MirOpiPolAppRecvDt")
	private String mirOpiPolAppRecvDt;
	@XStreamAlias("MirOpiPolAppSignDt")
	private String mirOpiPolAppSignDt;
	@XStreamAlias("MirPdPolPremSuspAmt")
	private String mirPdPolPremSuspAmt;
	@XStreamAlias("MirPdPolMiscSuspAmt")
	private String mirPdPolMiscSuspAmt;
	@XStreamAlias("MirPdPolOsDisbAmt")
	private String mirPdPolOsDisbAmt;
	@XStreamAlias("MirPdPolMiscSuspDt")
	private String mirPdPolMiscSuspDt;
	@XStreamAlias("MirPdPolPdfSuspAmt")
	private String mirPdPolPdfSuspAmt;
	@XStreamAlias("MirPdPolPremSuspDt")
	private String mirPdPolPremSuspDt;
	@XStreamAlias("MirBiCvgNumG")
	private MirBiCvgNumG mirBiCvgNumG;
	@XStreamAlias("MirBiSeqNumG")
	private MirBiSeqNumG mirBiSeqNumG;
	@XStreamAlias("MirBiBnfyPrcedsG")
	private MirBiBnfyPrcedsG mirBiBnfyPrcedsG;
	@XStreamAlias("MirBiBnfyDesgntCdG")
	private MirBiBnfyDesgntCdG mirBiBnfyDesgntCdG;
	@XStreamAlias("MirBiBnfyNmeG")
	private MirBiBnfyNmeG mirBiBnfyNmeG;
	@XStreamAlias("MirBiBnfyBrthdtG")
	private MirBiBnfyBrthdtG mirBiBnfyBrthdtG;
	@XStreamAlias("MirBiBnfyRelinsG")
	private MirBiBnfyRelinsG mirBiBnfyRelinsG;
	@XStreamAlias("MirBiBnfyTypG")
	private MirBiBnfyTypG mirBiBnfyTypG;
	@XStreamAlias("MirBiBnfyPrcdsPctG")
	private MirBiBnfyPrcdsPctG mirBiBnfyPrcdsPctG;
	@XStreamAlias("MirBiLstUpdtDt")
	private String mirBiLstUpdtDt;
	@XStreamAlias("MirBiLstUpdtUserid")
	private String mirBiLstUpdtUserid;
	@XStreamAlias("MirPvDvValuPuaAmt")
	private String mirPvDvValuPuaAmt;
	@XStreamAlias("MirPvDvLoanRepayAmt")
	private String mirPvDvLoanRepayAmt;
	@XStreamAlias("MirPvDvLoanDscntFct")
	private String mirPvDvLoanDscntFct;
	@XStreamAlias("MirPvDvPolBaseCvAmt")
	private String mirPvDvPolBaseCvAmt;
	@XStreamAlias("MirHiPolMpremAmt")
	private String mirHiPolMpremAmt;
	@XStreamAlias("MirCdPolCvgRecCtr")
	private String mirCdPolCvgRecCtr;
	@XStreamAlias("MirCdCvgXhbtIssDt")
	private String mirCdCvgXhbtIssDt;
	@XStreamAlias("MirCdCvgMatXpryDt")
	private String mirCdCvgMatXpryDt;
	@XStreamAlias("MirCdCvgSumInsAmt")
	private String mirCdCvgSumInsAmt;
	@XStreamAlias("MirAsaAgtCntrctStrtDt")
	private String mirAsaAgtCntrctStrtDt;
	@XStreamAlias("MirCdAplClrAmt")
	private String mirCdAplClrAmt;
	@XStreamAlias("MirCdLoanClr1Amt")
	private String mirCdLoanClr1Amt;
	@XStreamAlias("MirDv1PolCsvAmt")
	private String mirDv1PolCsvAmt;
	@XStreamAlias("MirPolPdfSuspAmt")
	private String mirPolPdfSuspAmt;
	@XStreamAlias("MirPolInsTypCd")
	private String mirPolInsTypCd;
	@XStreamAlias("MirOwnCliGvNm")
	private String mirOwnCliGvNm;
	@XStreamAlias("MirOwnCliSurNm")
	private String mirOwnCliSurNm;
	@XStreamAlias("MirOwnCliMidInitNm")
	private String mirOwnCliMidInitNm;
	@XStreamAlias("MirInsCliGvNm")
	private String mirInsCliGvNm;
	@XStreamAlias("MirInsCliSurNm")
	private String mirInsCliSurNm;
	@XStreamAlias("MirInsCliMidInitNm")
	private String mirInsCliMidInitNm;
	@XStreamAlias("MirCliCityNmTxt")
	private String mirCliCityNmTxt;
	@XStreamAlias("MirCliCrntLocCd")
	private String mirCliCrntLocCd;
	@XStreamAlias("MirCliCtryCd")
	private String mirCliCtryCd;
	@XStreamAlias("MirCliPstlCd")
	private String mirCliPstlCd;
	@XStreamAlias("MirPoOptCd")
	private String mirPoOptCd;
	@XStreamAlias("MirPoStatCd")
	private String mirPoStatCd;
	@XStreamAlias("MirDvMaxWthdrwDodAmt")
	private String mirDvMaxWthdrwDodAmt;
	@XStreamAlias("MirDvMaxWthdrwPuaFaAmt")
	private String mirDvMaxWthdrwPuaFaAmt;
	@XStreamAlias("MirDvMaxWthdrwPuaCvAmt")
	private String mirDvMaxWthdrwPuaCvAmt;
	@XStreamAlias("MirPrevLtdPuaAmt")
	private String mirPrevLtdPuaAmt;
	@XStreamAlias("MirPdAutoWthdrInd")
	private String mirPdAutoWthdrInd;
	@XStreamAlias("MirAutoWthdrInd")
	private String mirAutoWthdrInd;
	@XStreamAlias("MirAutoWthdrAmt")
	private String mirAutoWthdrAmt;
	@XStreamAlias("MirAutoWthdrPct")
	private String mirAutoWthdrPct;
	@XStreamAlias("MirAutoWthdrDur")
	private String mirAutoWthdrDur;
	@XStreamAlias("MirRemnWthdrDur")
	private String mirRemnWthdrDur;
	@XStreamAlias("MirFaAutoWthdrAmt")
	private String mirFaAutoWthdrAmt;

	public MirPolicyID getPolicyID() {
		return mirPolicyId;
	}

	public InquiryConsolidatedInformationData setPolicyID(final MirPolicyID mirPolicyId) {
		this.mirPolicyId = mirPolicyId;
		return this;
	}

	public String getSourceSystem() {
		return sourceSystem;
	}

	public InquiryConsolidatedInformationData setSourceSystem(final String sourceSystem) {
		this.sourceSystem = sourceSystem;
		return this;
	}

	public String getMirPrevLtdDodAmt() {
		return mirPrevLtdDodAmt;
	}

	public void setMirPrevLtdDodAmt(String mirPrevLtdDodAmt) {
		this.mirPrevLtdDodAmt = mirPrevLtdDodAmt;
	}

	public String getMirHiSystemDt() {
		return mirHiSystemDt;
	}

	public void setMirHiSystemDt(String mirHiSystemDt) {
		this.mirHiSystemDt = mirHiSystemDt;
	}

	public String getMirHiDvOwnCliNm() {
		return mirHiDvOwnCliNm;
	}

	public void setMirHiDvOwnCliNm(String mirHiDvOwnCliNm) {
		this.mirHiDvOwnCliNm = mirHiDvOwnCliNm;
	}

	public String getMirHiDvOwnCliBthDt() {
		return mirHiDvOwnCliBthDt;
	}

	public void setMirHiDvOwnCliBthDt(String mirHiDvOwnCliBthDt) {
		this.mirHiDvOwnCliBthDt = mirHiDvOwnCliBthDt;
	}

	public String getMirHiDvInsrdCliNm() {
		return mirHiDvInsrdCliNm;
	}

	public void setMirHiDvInsrdCliNm(String mirHiDvInsrdCliNm) {
		this.mirHiDvInsrdCliNm = mirHiDvInsrdCliNm;
	}

	public String getMirHiDvInsrdCliBthDt() {
		return mirHiDvInsrdCliBthDt;
	}

	public void setMirHiDvInsrdCliBthDt(String mirHiDvInsrdCliBthDt) {
		this.mirHiDvInsrdCliBthDt = mirHiDvInsrdCliBthDt;
	}

	public String getMirHiPolCstatCd() {
		return mirHiPolCstatCd;
	}

	public void setMirHiPolCstatCd(String mirHiPolCstatCd) {
		this.mirHiPolCstatCd = mirHiPolCstatCd;
	}

	public String getMirHiPlanId() {
		return mirHiPlanId;
	}

	public void setMirHiPlanId(String mirHiPlanId) {
		this.mirHiPlanId = mirHiPlanId;
	}

	public String getMirHiPolBillTypCd() {
		return mirHiPolBillTypCd;
	}

	public void setMirHiPolBillTypCd(String mirHiPolBillTypCd) {
		this.mirHiPolBillTypCd = mirHiPolBillTypCd;
	}

	public String getMirHiPolBillModeCd() {
		return mirHiPolBillModeCd;
	}

	public void setMirHiPolBillModeCd(String mirHiPolBillModeCd) {
		this.mirHiPolBillModeCd = mirHiPolBillModeCd;
	}

	public String getMirHiPolTpremAmt() {
		return mirHiPolTpremAmt;
	}

	public void setMirHiPolTpremAmt(String mirHiPolTpremAmt) {
		this.mirHiPolTpremAmt = mirHiPolTpremAmt;
	}

	public String getMirHiPolSndryAmt() {
		return mirHiPolSndryAmt;
	}

	public void setMirHiPolSndryAmt(String mirHiPolSndryAmt) {
		this.mirHiPolSndryAmt = mirHiPolSndryAmt;
	}

	public String getMirHiPolIssEffDt() {
		return mirHiPolIssEffDt;
	}

	public void setMirHiPolIssEffDt(String mirHiPolIssEffDt) {
		this.mirHiPolIssEffDt = mirHiPolIssEffDt;
	}

	public String getMirHiPolPrevMpremAmt() {
		return mirHiPolPrevMpremAmt;
	}

	public void setMirHiPolPrevMpremAmt(String mirHiPolPrevMpremAmt) {
		this.mirHiPolPrevMpremAmt = mirHiPolPrevMpremAmt;
	}

	public String getMirHiPolPdToDtNum() {
		return mirHiPolPdToDtNum;
	}

	public void setMirHiPolPdToDtNum(String mirHiPolPdToDtNum) {
		this.mirHiPolPdToDtNum = mirHiPolPdToDtNum;
	}

	public String getMirHiPolCrcyCd() {
		return mirHiPolCrcyCd;
	}

	public void setMirHiPolCrcyCd(String mirHiPolCrcyCd) {
		this.mirHiPolCrcyCd = mirHiPolCrcyCd;
	}

	public String getMirHiOwnCliId() {
		return mirHiOwnCliId;
	}

	public void setMirHiOwnCliId(String mirHiOwnCliId) {
		this.mirHiOwnCliId = mirHiOwnCliId;
	}

	public String getMirHiInsrdCliId() {
		return mirHiInsrdCliId;
	}

	public void setMirHiInsrdCliId(String mirHiInsrdCliId) {
		this.mirHiInsrdCliId = mirHiInsrdCliId;
	}

	public String getMirHiPolTotBillAmt() {
		return mirHiPolTotBillAmt;
	}

	public void setMirHiPolTotBillAmt(String mirHiPolTotBillAmt) {
		this.mirHiPolTotBillAmt = mirHiPolTotBillAmt;
	}

	public String getMirAsaDvLbillCliNm() {
		return mirAsaDvLbillCliNm;
	}

	public void setMirAsaDvLbillCliNm(String mirAsaDvLbillCliNm) {
		this.mirAsaDvLbillCliNm = mirAsaDvLbillCliNm;
	}

	public String getMirAsaCliAddrLn1Txt() {
		return mirAsaCliAddrLn1Txt;
	}

	public void setMirAsaCliAddrLn1Txt(String mirAsaCliAddrLn1Txt) {
		this.mirAsaCliAddrLn1Txt = mirAsaCliAddrLn1Txt;
	}

	public String getMirAsaCliAddrLn2Txt() {
		return mirAsaCliAddrLn2Txt;
	}

	public void setMirAsaCliAddrLn2Txt(String mirAsaCliAddrLn2Txt) {
		this.mirAsaCliAddrLn2Txt = mirAsaCliAddrLn2Txt;
	}

	public String getMirAsaCliAddrLn3Txt() {
		return mirAsaCliAddrLn3Txt;
	}

	public void setMirAsaCliAddrLn3Txt(String mirAsaCliAddrLn3Txt) {
		this.mirAsaCliAddrLn3Txt = mirAsaCliAddrLn3Txt;
	}

	public String getMirAsaCliAddrLn4Txt() {
		return mirAsaCliAddrLn4Txt;
	}

	public void setMirAsaCliAddrLn4Txt(String mirAsaCliAddrLn4Txt) {
		this.mirAsaCliAddrLn4Txt = mirAsaCliAddrLn4Txt;
	}

	public String getMirAsaCliAddrLn5Txt() {
		return mirAsaCliAddrLn5Txt;
	}

	public void setMirAsaCliAddrLn5Txt(String mirAsaCliAddrLn5Txt) {
		this.mirAsaCliAddrLn5Txt = mirAsaCliAddrLn5Txt;
	}

	public String getMirAsaCliAddrChngDt() {
		return mirAsaCliAddrChngDt;
	}

	public void setMirAsaCliAddrChngDt(String mirAsaCliAddrChngDt) {
		this.mirAsaCliAddrChngDt = mirAsaCliAddrChngDt;
	}

	public String getMirAsaServAgtId() {
		return mirAsaServAgtId;
	}

	public void setMirAsaServAgtId(String mirAsaServAgtId) {
		this.mirAsaServAgtId = mirAsaServAgtId;
	}

	public String getMirAsaDvServAgtCliNm() {
		return mirAsaDvServAgtCliNm;
	}

	public void setMirAsaDvServAgtCliNm(String mirAsaDvServAgtCliNm) {
		this.mirAsaDvServAgtCliNm = mirAsaDvServAgtCliNm;
	}

	public String getMirAsaServBrId() {
		return mirAsaServBrId;
	}

	public void setMirAsaServBrId(String mirAsaServBrId) {
		this.mirAsaServBrId = mirAsaServBrId;
	}

	public String getMirAsaServFeeOpt() {
		return mirAsaServFeeOpt;
	}

	public void setMirAsaServFeeOpt(String mirAsaServFeeOpt) {
		this.mirAsaServFeeOpt = mirAsaServFeeOpt;
	}

	public String getMirAsaAgtStatCd() {
		return mirAsaAgtStatCd;
	}

	public void setMirAsaAgtStatCd(String mirAsaAgtStatCd) {
		this.mirAsaAgtStatCd = mirAsaAgtStatCd;
	}

	public String getMirAsaPrevServAgtId() {
		return mirAsaPrevServAgtId;
	}

	public void setMirAsaPrevServAgtId(String mirAsaPrevServAgtId) {
		this.mirAsaPrevServAgtId = mirAsaPrevServAgtId;
	}

	public String getMirPolPdfAmt() {
		return mirPolPdfAmt;
	}

	public void setMirPolPdfAmt(String mirPolPdfAmt) {
		this.mirPolPdfAmt = mirPolPdfAmt;
	}

	public String getMirPolPdfAvbAmt() {
		return mirPolPdfAvbAmt;
	}

	public void setMirPolPdfAvbAmt(String mirPolPdfAvbAmt) {
		this.mirPolPdfAvbAmt = mirPolPdfAvbAmt;
	}

	public String getMirPolPdfStringAmt() {
		return mirPolPdfStringAmt;
	}

	public void setMirPolPdfStringAmt(String mirPolPdfStringAmt) {
		this.mirPolPdfStringAmt = mirPolPdfStringAmt;
	}

	public String getMirPolPdfStringRt() {
		return mirPolPdfStringRt;
	}

	public void setMirPolPdfStringRt(String mirPolPdfStringRt) {
		this.mirPolPdfStringRt = mirPolPdfStringRt;
	}

	public MirCdCvgAcctTypCdG getMirCdCvgAcctTypCdG() {
		return mirCdCvgAcctTypCdG;
	}

	public void setMirCdCvgAcctTypCdG(MirCdCvgAcctTypCdG mirCdCvgAcctTypCdG) {
		this.mirCdCvgAcctTypCdG = mirCdCvgAcctTypCdG;
	}

	public MirCdPlanIdG getMirCdPlanIdG() {
		return mirCdPlanIdG;
	}

	public void setMirCdPlanIdG(MirCdPlanIdG mirCdPlanIdG) {
		this.mirCdPlanIdG = mirCdPlanIdG;
	}

	public MirCdCvgFaceAmtG getMirCdCvgFaceAmtG() {
		return mirCdCvgFaceAmtG;
	}

	public void setMirCdCvgFaceAmtG(MirCdCvgFaceAmtG mirCdCvgFaceAmtG) {
		this.mirCdCvgFaceAmtG = mirCdCvgFaceAmtG;
	}

	public MirCdCvgMpremAmtG getMirCdCvgMpremAmtG() {
		return mirCdCvgMpremAmtG;
	}

	public void setMirCdCvgMpremAmtG(MirCdCvgMpremAmtG mirCdCvgMpremAmtG) {
		this.mirCdCvgMpremAmtG = mirCdCvgMpremAmtG;
	}

	public MirCdCvgIssEffDtG getMirCdCvgIssEffDtG() {
		return mirCdCvgIssEffDtG;
	}

	public void setMirCdCvgIssEffDtG(MirCdCvgIssEffDtG mirCdCvgIssEffDtG) {
		this.mirCdCvgIssEffDtG = mirCdCvgIssEffDtG;
	}

	public MirCdCvgSmkrCdG getMirCdCvgSmkrCdG() {
		return mirCdCvgSmkrCdG;
	}

	public void setMirCdCvgSmkrCdG(MirCdCvgSmkrCdG mirCdCvgSmkrCdG) {
		this.mirCdCvgSmkrCdG = mirCdCvgSmkrCdG;
	}

	public MirCdCvgSexCdG getMirCdCvgSexCdG() {
		return mirCdCvgSexCdG;
	}

	public void setMirCdCvgSexCdG(MirCdCvgSexCdG mirCdCvgSexCdG) {
		this.mirCdCvgSexCdG = mirCdCvgSexCdG;
	}

	public MirCdCvgRtAgeG getMirCdCvgRtAgeG() {
		return mirCdCvgRtAgeG;
	}

	public void setMirCdCvgRtAgeG(MirCdCvgRtAgeG mirCdCvgRtAgeG) {
		this.mirCdCvgRtAgeG = mirCdCvgRtAgeG;
	}

	public MirCdCvgParCdG getMirCdCvgParCdG() {
		return mirCdCvgParCdG;
	}

	public void setMirCdCvgParCdG(MirCdCvgParCdG mirCdCvgParCdG) {
		this.mirCdCvgParCdG = mirCdCvgParCdG;
	}

	public MirCdCvgCstatCdG getMirCdCvgCstatCdG() {
		return mirCdCvgCstatCdG;
	}

	public void setMirCdCvgCstatCdG(MirCdCvgCstatCdG mirCdCvgCstatCdG) {
		this.mirCdCvgCstatCdG = mirCdCvgCstatCdG;
	}

	public MirCdCvgMatXpryDtG getMirCdCvgMatXpryDtG() {
		return mirCdCvgMatXpryDtG;
	}

	public void setMirCdCvgMatXpryDtG(MirCdCvgMatXpryDtG mirCdCvgMatXpryDtG) {
		this.mirCdCvgMatXpryDtG = mirCdCvgMatXpryDtG;
	}

	public MirRtCvgNumG getMirRtCvgNumG() {
		return mirRtCvgNumG;
	}

	public void setMirRtCvgNumG(MirRtCvgNumG mirRtCvgNumG) {
		this.mirRtCvgNumG = mirRtCvgNumG;
	}

	public MirRtCvgFePremAmtG getMirRtCvgFePremAmtG() {
		return mirRtCvgFePremAmtG;
	}

	public void setMirRtCvgFePremAmtG(MirRtCvgFePremAmtG mirRtCvgFePremAmtG) {
		this.mirRtCvgFePremAmtG = mirRtCvgFePremAmtG;
	}

	public MirRtCvgMePremAmtG getMirRtCvgMePremAmtG() {
		return mirRtCvgMePremAmtG;
	}

	public void setMirRtCvgMePremAmtG(MirRtCvgMePremAmtG mirRtCvgMePremAmtG) {
		this.mirRtCvgMePremAmtG = mirRtCvgMePremAmtG;
	}

	public MirRtCvgFeUpremG getMirRtCvgFeUpremG() {
		return mirRtCvgFeUpremG;
	}

	public void setMirRtCvgFeUpremG(MirRtCvgFeUpremG mirRtCvgFeUpremG) {
		this.mirRtCvgFeUpremG = mirRtCvgFeUpremG;
	}

	public MirRtCvgFeDurG getMirRtCvgFeDurG() {
		return mirRtCvgFeDurG;
	}

	public void setMirRtCvgFeDurG(MirRtCvgFeDurG mirRtCvgFeDurG) {
		this.mirRtCvgFeDurG = mirRtCvgFeDurG;
	}

	public MirRtCvgFePermUpremAmtG getMirRtCvgFePermUpremAmtG() {
		return mirRtCvgFePermUpremAmtG;
	}

	public void setMirRtCvgFePermUpremAmtG(MirRtCvgFePermUpremAmtG mirRtCvgFePermUpremAmtG) {
		this.mirRtCvgFePermUpremAmtG = mirRtCvgFePermUpremAmtG;
	}

	public MirRtCvgMeFctG getMirRtCvgMeFctG() {
		return mirRtCvgMeFctG;
	}

	public void setMirRtCvgMeFctG(MirRtCvgMeFctG mirRtCvgMeFctG) {
		this.mirRtCvgMeFctG = mirRtCvgMeFctG;
	}

	public MirRtCvgMeDurG getMirRtCvgMeDurG() {
		return mirRtCvgMeDurG;
	}

	public void setMirRtCvgMeDurG(MirRtCvgMeDurG mirRtCvgMeDurG) {
		this.mirRtCvgMeDurG = mirRtCvgMeDurG;
	}

	public MirRtCvgFeReasnCdG getMirRtCvgFeReasnCdG() {
		return mirRtCvgFeReasnCdG;
	}

	public void setMirRtCvgFeReasnCdG(MirRtCvgFeReasnCdG mirRtCvgFeReasnCdG) {
		this.mirRtCvgFeReasnCdG = mirRtCvgFeReasnCdG;
	}

	public MirRtCvgMeReasnCdG getMirRtCvgMeReasnCdG() {
		return mirRtCvgMeReasnCdG;
	}

	public void setMirRtCvgMeReasnCdG(MirRtCvgMeReasnCdG mirRtCvgMeReasnCdG) {
		this.mirRtCvgMeReasnCdG = mirRtCvgMeReasnCdG;
	}

	public MirRtCvgRtAgeG getMirRtCvgRtAgeG() {
		return mirRtCvgRtAgeG;
	}

	public void setMirRtCvgRtAgeG(MirRtCvgRtAgeG mirRtCvgRtAgeG) {
		this.mirRtCvgRtAgeG = mirRtCvgRtAgeG;
	}

	public MirRtCvgMeRatCdG getMirRtCvgMeRatCdG() {
		return mirRtCvgMeRatCdG;
	}

	public void setMirRtCvgMeRatCdG(MirRtCvgMeRatCdG mirRtCvgMeRatCdG) {
		this.mirRtCvgMeRatCdG = mirRtCvgMeRatCdG;
	}

	public String getMirPvDvMaxLoanAmt() {
		return mirPvDvMaxLoanAmt;
	}

	public void setMirPvDvMaxLoanAmt(String mirPvDvMaxLoanAmt) {
		this.mirPvDvMaxLoanAmt = mirPvDvMaxLoanAmt;
	}

	public String getMirPvDvPolCsvAmt() {
		return mirPvDvPolCsvAmt;
	}

	public void setMirPvDvPolCsvAmt(String mirPvDvPolCsvAmt) {
		this.mirPvDvPolCsvAmt = mirPvDvPolCsvAmt;
	}

	public String getMirPvDvLoanAmt() {
		return mirPvDvLoanAmt;
	}

	public void setMirPvDvLoanAmt(String mirPvDvLoanAmt) {
		this.mirPvDvLoanAmt = mirPvDvLoanAmt;
	}

	public String getMirPvDvMcvCsvAmt() {
		return mirPvDvMcvCsvAmt;
	}

	public void setMirPvDvMcvCsvAmt(String mirPvDvMcvCsvAmt) {
		this.mirPvDvMcvCsvAmt = mirPvDvMcvCsvAmt;
	}

	public String getMirPvPolDivOptCd() {
		return mirPvPolDivOptCd;
	}

	public void setMirPvPolDivOptCd(String mirPvPolDivOptCd) {
		this.mirPvPolDivOptCd = mirPvPolDivOptCd;
	}

	public String getMirPvDvPrevDivYrQty() {
		return mirPvDvPrevDivYrQty;
	}

	public void setMirPvDvPrevDivYrQty(String mirPvDvPrevDivYrQty) {
		this.mirPvDvPrevDivYrQty = mirPvDvPrevDivYrQty;
	}

	public String getMirPvDivDclrDurAmt() {
		return mirPvDivDclrDurAmt;
	}

	public void setMirPvDivDclrDurAmt(String mirPvDivDclrDurAmt) {
		this.mirPvDivDclrDurAmt = mirPvDivDclrDurAmt;
	}

	public String getMirPvPolDodAcumAmt() {
		return mirPvPolDodAcumAmt;
	}

	public void setMirPvPolDodAcumAmt(String mirPvPolDodAcumAmt) {
		this.mirPvPolDodAcumAmt = mirPvPolDodAcumAmt;
	}

	public String getMirPvPolDodStringRt() {
		return mirPvPolDodStringRt;
	}

	public void setMirPvPolDodStringRt(String mirPvPolDodStringRt) {
		this.mirPvPolDodStringRt = mirPvPolDodStringRt;
	}

	public String getMirPvDvAnnvPyrQty() {
		return mirPvDvAnnvPyrQty;
	}

	public void setMirPvDvAnnvPyrQty(String mirPvDvAnnvPyrQty) {
		this.mirPvDvAnnvPyrQty = mirPvDvAnnvPyrQty;
	}

	public String getMirPvPuaLtdFaceAmt() {
		return mirPvPuaLtdFaceAmt;
	}

	public void setMirPvPuaLtdFaceAmt(String mirPvPuaLtdFaceAmt) {
		this.mirPvPuaLtdFaceAmt = mirPvPuaLtdFaceAmt;
	}

	public String getMirPvPuaYtdFaceAmt() {
		return mirPvPuaYtdFaceAmt;
	}

	public void setMirPvPuaYtdFaceAmt(String mirPvPuaYtdFaceAmt) {
		this.mirPvPuaYtdFaceAmt = mirPvPuaYtdFaceAmt;
	}

	public String getMirAfdPolAeFndAmt() {
		return mirAfdPolAeFndAmt;
	}

	public void setMirAfdPolAeFndAmt(String mirAfdPolAeFndAmt) {
		this.mirAfdPolAeFndAmt = mirAfdPolAeFndAmt;
	}

	public String getMirAfdPolAeFndStringAmt() {
		return mirAfdPolAeFndStringAmt;
	}

	public void setMirAfdPolAeFndStringAmt(String mirAfdPolAeFndStringAmt) {
		this.mirAfdPolAeFndStringAmt = mirAfdPolAeFndStringAmt;
	}

	public String getMirAfdCvgOrigPlanId() {
		return mirAfdCvgOrigPlanId;
	}

	public void setMirAfdCvgOrigPlanId(String mirAfdCvgOrigPlanId) {
		this.mirAfdCvgOrigPlanId = mirAfdCvgOrigPlanId;
	}

	public String getMirAfdPolAeLstWthdrAmt() {
		return mirAfdPolAeLstWthdrAmt;
	}

	public void setMirAfdPolAeLstWthdrAmt(String mirAfdPolAeLstWthdrAmt) {
		this.mirAfdPolAeLstWthdrAmt = mirAfdPolAeLstWthdrAmt;
	}

	public String getMirAfdDvLoanBal() {
		return mirAfdDvLoanBal;
	}

	public void setMirAfdDvLoanBal(String mirAfdDvLoanBal) {
		this.mirAfdDvLoanBal = mirAfdDvLoanBal;
	}

	public String getMirOpiPolNfoCd() {
		return mirOpiPolNfoCd;
	}

	public void setMirOpiPolNfoCd(String mirOpiPolNfoCd) {
		this.mirOpiPolNfoCd = mirOpiPolNfoCd;
	}

	public String getMirOpiPolReinsCd() {
		return mirOpiPolReinsCd;
	}

	public void setMirOpiPolReinsCd(String mirOpiPolReinsCd) {
		this.mirOpiPolReinsCd = mirOpiPolReinsCd;
	}

	public String getMirOpiPolReinsFaceAmt() {
		return mirOpiPolReinsFaceAmt;
	}

	public void setMirOpiPolReinsFaceAmt(String mirOpiPolReinsFaceAmt) {
		this.mirOpiPolReinsFaceAmt = mirOpiPolReinsFaceAmt;
	}

	public String getMirOpiPolReinsAdbFaceAmt() {
		return mirOpiPolReinsAdbFaceAmt;
	}

	public void setMirOpiPolReinsAdbFaceAmt(String mirOpiPolReinsAdbFaceAmt) {
		this.mirOpiPolReinsAdbFaceAmt = mirOpiPolReinsAdbFaceAmt;
	}

	public String getMirOpiPolReinsSndryAmt() {
		return mirOpiPolReinsSndryAmt;
	}

	public void setMirOpiPolReinsSndryAmt(String mirOpiPolReinsSndryAmt) {
		this.mirOpiPolReinsSndryAmt = mirOpiPolReinsSndryAmt;
	}

	public String getMirOpiPolUlShrtAmt() {
		return mirOpiPolUlShrtAmt;
	}

	public void setMirOpiPolUlShrtAmt(String mirOpiPolUlShrtAmt) {
		this.mirOpiPolUlShrtAmt = mirOpiPolUlShrtAmt;
	}

	public String getMirOpiUlLapsNotiAmt() {
		return mirOpiUlLapsNotiAmt;
	}

	public void setMirOpiUlLapsNotiAmt(String mirOpiUlLapsNotiAmt) {
		this.mirOpiUlLapsNotiAmt = mirOpiUlLapsNotiAmt;
	}

	public MirOpiAgtIdG getMirOpiAgtIdG() {
		return mirOpiAgtIdG;
	}

	public void setMirOpiAgtIdG(MirOpiAgtIdG mirOpiAgtIdG) {
		this.mirOpiAgtIdG = mirOpiAgtIdG;
	}

	public MirOpiDvAgtCliNmG getMirOpiDvAgtCliNmG() {
		return mirOpiDvAgtCliNmG;
	}

	public void setMirOpiDvAgtCliNmG(MirOpiDvAgtCliNmG mirOpiDvAgtCliNmG) {
		this.mirOpiDvAgtCliNmG = mirOpiDvAgtCliNmG;
	}

	public MirOpiBrIdG getMirOpiBrIdG() {
		return mirOpiBrIdG;
	}

	public void setMirOpiBrIdG(MirOpiBrIdG mirOpiBrIdG) {
		this.mirOpiBrIdG = mirOpiBrIdG;
	}

	public MirOpiAgtStatCdG getMirOpiAgtStatCdG() {
		return mirOpiAgtStatCdG;
	}

	public void setMirOpiAgtStatCdG(MirOpiAgtStatCdG mirOpiAgtStatCdG) {
		this.mirOpiAgtStatCdG = mirOpiAgtStatCdG;
	}

	public MirOpiPolAgtShrPctG getMirOpiPolAgtShrPctG() {
		return mirOpiPolAgtShrPctG;
	}

	public void setMirOpiPolAgtShrPctG(MirOpiPolAgtShrPctG mirOpiPolAgtShrPctG) {
		this.mirOpiPolAgtShrPctG = mirOpiPolAgtShrPctG;
	}

	public String getMirOpiPolAppRecvDt() {
		return mirOpiPolAppRecvDt;
	}

	public void setMirOpiPolAppRecvDt(String mirOpiPolAppRecvDt) {
		this.mirOpiPolAppRecvDt = mirOpiPolAppRecvDt;
	}

	public String getMirOpiPolAppSignDt() {
		return mirOpiPolAppSignDt;
	}

	public void setMirOpiPolAppSignDt(String mirOpiPolAppSignDt) {
		this.mirOpiPolAppSignDt = mirOpiPolAppSignDt;
	}

	public String getMirPdPolPremSuspAmt() {
		return mirPdPolPremSuspAmt;
	}

	public void setMirPdPolPremSuspAmt(String mirPdPolPremSuspAmt) {
		this.mirPdPolPremSuspAmt = mirPdPolPremSuspAmt;
	}

	public String getMirPdPolMiscSuspAmt() {
		return mirPdPolMiscSuspAmt;
	}

	public void setMirPdPolMiscSuspAmt(String mirPdPolMiscSuspAmt) {
		this.mirPdPolMiscSuspAmt = mirPdPolMiscSuspAmt;
	}

	public String getMirPdPolOsDisbAmt() {
		return mirPdPolOsDisbAmt;
	}

	public void setMirPdPolOsDisbAmt(String mirPdPolOsDisbAmt) {
		this.mirPdPolOsDisbAmt = mirPdPolOsDisbAmt;
	}

	public String getMirPdPolMiscSuspDt() {
		return mirPdPolMiscSuspDt;
	}

	public void setMirPdPolMiscSuspDt(String mirPdPolMiscSuspDt) {
		this.mirPdPolMiscSuspDt = mirPdPolMiscSuspDt;
	}

	public String getMirPdPolPdfSuspAmt() {
		return mirPdPolPdfSuspAmt;
	}

	public void setMirPdPolPdfSuspAmt(String mirPdPolPdfSuspAmt) {
		this.mirPdPolPdfSuspAmt = mirPdPolPdfSuspAmt;
	}

	public String getMirPdPolPremSuspDt() {
		return mirPdPolPremSuspDt;
	}

	public void setMirPdPolPremSuspDt(String mirPdPolPremSuspDt) {
		this.mirPdPolPremSuspDt = mirPdPolPremSuspDt;
	}

	public MirBiCvgNumG getMirBiCvgNumG() {
		return mirBiCvgNumG;
	}

	public void setMirBiCvgNumG(MirBiCvgNumG mirBiCvgNumG) {
		this.mirBiCvgNumG = mirBiCvgNumG;
	}

	public MirBiSeqNumG getMirBiSeqNumG() {
		return mirBiSeqNumG;
	}

	public void setMirBiSeqNumG(MirBiSeqNumG mirBiSeqNumG) {
		this.mirBiSeqNumG = mirBiSeqNumG;
	}

	public MirBiBnfyPrcedsG getMirBiBnfyPrcedsG() {
		return mirBiBnfyPrcedsG;
	}

	public void setMirBiBnfyPrcedsG(MirBiBnfyPrcedsG mirBiBnfyPrcedsG) {
		this.mirBiBnfyPrcedsG = mirBiBnfyPrcedsG;
	}

	public MirBiBnfyDesgntCdG getMirBiBnfyDesgntCdG() {
		return mirBiBnfyDesgntCdG;
	}

	public void setMirBiBnfyDesgntCdG(MirBiBnfyDesgntCdG mirBiBnfyDesgntCdG) {
		this.mirBiBnfyDesgntCdG = mirBiBnfyDesgntCdG;
	}

	public MirBiBnfyNmeG getMirBiBnfyNmeG() {
		return mirBiBnfyNmeG;
	}

	public void setMirBiBnfyNmeG(MirBiBnfyNmeG mirBiBnfyNmeG) {
		this.mirBiBnfyNmeG = mirBiBnfyNmeG;
	}

	public MirBiBnfyBrthdtG getMirBiBnfyBrthdtG() {
		return mirBiBnfyBrthdtG;
	}

	public void setMirBiBnfyBrthdtG(MirBiBnfyBrthdtG mirBiBnfyBrthdtG) {
		this.mirBiBnfyBrthdtG = mirBiBnfyBrthdtG;
	}

	public MirBiBnfyRelinsG getMirBiBnfyRelinsG() {
		return mirBiBnfyRelinsG;
	}

	public void setMirBiBnfyRelinsG(MirBiBnfyRelinsG mirBiBnfyRelinsG) {
		this.mirBiBnfyRelinsG = mirBiBnfyRelinsG;
	}

	public MirBiBnfyTypG getMirBiBnfyTypG() {
		return mirBiBnfyTypG;
	}

	public void setMirBiBnfyTypG(MirBiBnfyTypG mirBiBnfyTypG) {
		this.mirBiBnfyTypG = mirBiBnfyTypG;
	}

	public MirBiBnfyPrcdsPctG getMirBiBnfyPrcdsPctG() {
		return mirBiBnfyPrcdsPctG;
	}

	public void setMirBiBnfyPrcdsPctG(MirBiBnfyPrcdsPctG mirBiBnfyPrcdsPctG) {
		this.mirBiBnfyPrcdsPctG = mirBiBnfyPrcdsPctG;
	}

	public String getMirBiLstUpdtDt() {
		return mirBiLstUpdtDt;
	}

	public void setMirBiLstUpdtDt(String mirBiLstUpdtDt) {
		this.mirBiLstUpdtDt = mirBiLstUpdtDt;
	}

	public String getMirBiLstUpdtUserid() {
		return mirBiLstUpdtUserid;
	}

	public void setMirBiLstUpdtUserid(String mirBiLstUpdtUserid) {
		this.mirBiLstUpdtUserid = mirBiLstUpdtUserid;
	}

	public String getMirPvDvValuPuaAmt() {
		return mirPvDvValuPuaAmt;
	}

	public void setMirPvDvValuPuaAmt(String mirPvDvValuPuaAmt) {
		this.mirPvDvValuPuaAmt = mirPvDvValuPuaAmt;
	}

	public String getMirPvDvLoanRepayAmt() {
		return mirPvDvLoanRepayAmt;
	}

	public void setMirPvDvLoanRepayAmt(String mirPvDvLoanRepayAmt) {
		this.mirPvDvLoanRepayAmt = mirPvDvLoanRepayAmt;
	}

	public String getMirPvDvLoanDscntFct() {
		return mirPvDvLoanDscntFct;
	}

	public void setMirPvDvLoanDscntFct(String mirPvDvLoanDscntFct) {
		this.mirPvDvLoanDscntFct = mirPvDvLoanDscntFct;
	}

	public String getMirPvDvPolBaseCvAmt() {
		return mirPvDvPolBaseCvAmt;
	}

	public void setMirPvDvPolBaseCvAmt(String mirPvDvPolBaseCvAmt) {
		this.mirPvDvPolBaseCvAmt = mirPvDvPolBaseCvAmt;
	}

	public String getMirHiPolMpremAmt() {
		return mirHiPolMpremAmt;
	}

	public void setMirHiPolMpremAmt(String mirHiPolMpremAmt) {
		this.mirHiPolMpremAmt = mirHiPolMpremAmt;
	}

	public String getMirCdPolCvgRecCtr() {
		return mirCdPolCvgRecCtr;
	}

	public void setMirCdPolCvgRecCtr(String mirCdPolCvgRecCtr) {
		this.mirCdPolCvgRecCtr = mirCdPolCvgRecCtr;
	}

	public String getMirCdCvgXhbtIssDt() {
		return mirCdCvgXhbtIssDt;
	}

	public void setMirCdCvgXhbtIssDt(String mirCdCvgXhbtIssDt) {
		this.mirCdCvgXhbtIssDt = mirCdCvgXhbtIssDt;
	}

	public String getMirCdCvgMatXpryDt() {
		return mirCdCvgMatXpryDt;
	}

	public void setMirCdCvgMatXpryDt(String mirCdCvgMatXpryDt) {
		this.mirCdCvgMatXpryDt = mirCdCvgMatXpryDt;
	}

	public String getMirCdCvgSumInsAmt() {
		return mirCdCvgSumInsAmt;
	}

	public void setMirCdCvgSumInsAmt(String mirCdCvgSumInsAmt) {
		this.mirCdCvgSumInsAmt = mirCdCvgSumInsAmt;
	}

	public String getMirAsaAgtCntrctStrtDt() {
		return mirAsaAgtCntrctStrtDt;
	}

	public void setMirAsaAgtCntrctStrtDt(String mirAsaAgtCntrctStrtDt) {
		this.mirAsaAgtCntrctStrtDt = mirAsaAgtCntrctStrtDt;
	}

	public String getMirCdAplClrAmt() {
		return mirCdAplClrAmt;
	}

	public void setMirCdAplClrAmt(String mirCdAplClrAmt) {
		this.mirCdAplClrAmt = mirCdAplClrAmt;
	}

	public String getMirCdLoanClr1Amt() {
		return mirCdLoanClr1Amt;
	}

	public void setMirCdLoanClr1Amt(String mirCdLoanClr1Amt) {
		this.mirCdLoanClr1Amt = mirCdLoanClr1Amt;
	}

	public String getMirDv1PolCsvAmt() {
		return mirDv1PolCsvAmt;
	}

	public void setMirDv1PolCsvAmt(String mirDv1PolCsvAmt) {
		this.mirDv1PolCsvAmt = mirDv1PolCsvAmt;
	}

	public String getMirPolPdfSuspAmt() {
		return mirPolPdfSuspAmt;
	}

	public void setMirPolPdfSuspAmt(String mirPolPdfSuspAmt) {
		this.mirPolPdfSuspAmt = mirPolPdfSuspAmt;
	}

	public String getMirPolInsTypCd() {
		return mirPolInsTypCd;
	}

	public void setMirPolInsTypCd(String mirPolInsTypCd) {
		this.mirPolInsTypCd = mirPolInsTypCd;
	}

	public String getMirOwnCliGvNm() {
		return mirOwnCliGvNm;
	}

	public void setMirOwnCliGvNm(String mirOwnCliGvNm) {
		this.mirOwnCliGvNm = mirOwnCliGvNm;
	}

	public String getMirOwnCliSurNm() {
		return mirOwnCliSurNm;
	}

	public void setMirOwnCliSurNm(String mirOwnCliSurNm) {
		this.mirOwnCliSurNm = mirOwnCliSurNm;
	}

	public String getMirOwnCliMidInitNm() {
		return mirOwnCliMidInitNm;
	}

	public void setMirOwnCliMidInitNm(String mirOwnCliMidInitNm) {
		this.mirOwnCliMidInitNm = mirOwnCliMidInitNm;
	}

	public String getMirInsCliGvNm() {
		return mirInsCliGvNm;
	}

	public void setMirInsCliGvNm(String mirInsCliGvNm) {
		this.mirInsCliGvNm = mirInsCliGvNm;
	}

	public String getMirInsCliSurNm() {
		return mirInsCliSurNm;
	}

	public void setMirInsCliSurNm(String mirInsCliSurNm) {
		this.mirInsCliSurNm = mirInsCliSurNm;
	}

	public String getMirInsCliMidInitNm() {
		return mirInsCliMidInitNm;
	}

	public void setMirInsCliMidInitNm(String mirInsCliMidInitNm) {
		this.mirInsCliMidInitNm = mirInsCliMidInitNm;
	}

	public String getMirCliCityNmTxt() {
		return mirCliCityNmTxt;
	}

	public void setMirCliCityNmTxt(String mirCliCityNmTxt) {
		this.mirCliCityNmTxt = mirCliCityNmTxt;
	}

	public String getMirCliCrntLocCd() {
		return mirCliCrntLocCd;
	}

	public void setMirCliCrntLocCd(String mirCliCrntLocCd) {
		this.mirCliCrntLocCd = mirCliCrntLocCd;
	}

	public String getMirCliCtryCd() {
		return mirCliCtryCd;
	}

	public void setMirCliCtryCd(String mirCliCtryCd) {
		this.mirCliCtryCd = mirCliCtryCd;
	}

	public String getMirCliPstlCd() {
		return mirCliPstlCd;
	}

	public void setMirCliPstlCd(String mirCliPstlCd) {
		this.mirCliPstlCd = mirCliPstlCd;
	}

	public String getMirPoOptCd() {
		return mirPoOptCd;
	}

	public void setMirPoOptCd(String mirPoOptCd) {
		this.mirPoOptCd = mirPoOptCd;
	}

	public String getMirPoStatCd() {
		return mirPoStatCd;
	}

	public void setMirPoStatCd(String mirPoStatCd) {
		this.mirPoStatCd = mirPoStatCd;
	}

	public String getMirDvMaxWthdrwDodAmt() {
		return mirDvMaxWthdrwDodAmt;
	}

	public void setMirDvMaxWthdrwDodAmt(String mirDvMaxWthdrwDodAmt) {
		this.mirDvMaxWthdrwDodAmt = mirDvMaxWthdrwDodAmt;
	}

	public String getMirDvMaxWthdrwPuaFaAmt() {
		return mirDvMaxWthdrwPuaFaAmt;
	}

	public void setMirDvMaxWthdrwPuaFaAmt(String mirDvMaxWthdrwPuaFaAmt) {
		this.mirDvMaxWthdrwPuaFaAmt = mirDvMaxWthdrwPuaFaAmt;
	}

	public String getMirDvMaxWthdrwPuaCvAmt() {
		return mirDvMaxWthdrwPuaCvAmt;
	}

	public void setMirDvMaxWthdrwPuaCvAmt(String mirDvMaxWthdrwPuaCvAmt) {
		this.mirDvMaxWthdrwPuaCvAmt = mirDvMaxWthdrwPuaCvAmt;
	}

	public String getMirPrevLtdPuaAmt() {
		return mirPrevLtdPuaAmt;
	}

	public void setMirPrevLtdPuaAmt(String mirPrevLtdPuaAmt) {
		this.mirPrevLtdPuaAmt = mirPrevLtdPuaAmt;
	}

	public String getMirPdAutoWthdrInd() {
		return mirPdAutoWthdrInd;
	}

	public void setMirPdAutoWthdrInd(String mirPdAutoWthdrInd) {
		this.mirPdAutoWthdrInd = mirPdAutoWthdrInd;
	}

	public String getMirAutoWthdrInd() {
		return mirAutoWthdrInd;
	}

	public void setMirAutoWthdrInd(String mirAutoWthdrInd) {
		this.mirAutoWthdrInd = mirAutoWthdrInd;
	}

	public String getMirAutoWthdrAmt() {
		return mirAutoWthdrAmt;
	}

	public void setMirAutoWthdrAmt(String mirAutoWthdrAmt) {
		this.mirAutoWthdrAmt = mirAutoWthdrAmt;
	}

	public String getMirAutoWthdrPct() {
		return mirAutoWthdrPct;
	}

	public void setMirAutoWthdrPct(String mirAutoWthdrPct) {
		this.mirAutoWthdrPct = mirAutoWthdrPct;
	}

	public String getMirAutoWthdrDur() {
		return mirAutoWthdrDur;
	}

	public void setMirAutoWthdrDur(String mirAutoWthdrDur) {
		this.mirAutoWthdrDur = mirAutoWthdrDur;
	}

	public String getMirRemnWthdrDur() {
		return mirRemnWthdrDur;
	}

	public void setMirRemnWthdrDur(String mirRemnWthdrDur) {
		this.mirRemnWthdrDur = mirRemnWthdrDur;
	}

	public String getMirFaAutoWthdrAmt() {
		return mirFaAutoWthdrAmt;
	}

	public void setMirFaAutoWthdrAmt(String mirFaAutoWthdrAmt) {
		this.mirFaAutoWthdrAmt = mirFaAutoWthdrAmt;
	}

}
