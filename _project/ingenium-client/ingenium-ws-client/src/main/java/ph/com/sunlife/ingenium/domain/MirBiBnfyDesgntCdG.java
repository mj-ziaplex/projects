package ph.com.sunlife.ingenium.domain;

import java.util.ArrayList;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("MirBiBnfyDesgntCdG")
public class MirBiBnfyDesgntCdG {

	@XStreamImplicit(itemFieldName = "MirBiBnfyDesgntT")
	protected List<String> mirBiBnfyDesgntT;

	public List<String> getMirBiBnfyDesgntT() {
		if (mirBiBnfyDesgntT == null) {
			mirBiBnfyDesgntT = new ArrayList<String>();
		}
		return this.mirBiBnfyDesgntT;
	}

}
