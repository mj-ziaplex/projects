package ph.com.sunlife.wms.ingenium.domain;

import java.util.Date;

public class ConsolidatedInformation {

  private Client owner;
  private Client insured;
  private String agentCode;
  private String branchCode;
  private String companyName;
  private Date applicationSignDate;
  private String servicingAgentName;
  private String originalFaceAmount;
  private String currency;
  private String policyContractOutputType;

  public Client getOwner() {
    return owner;
  }

  public void setOwner(Client owner) {
    this.owner = owner;
  }

  public Client getInsured() {
    return insured;
  }

  public void setInsured(Client insured) {
    this.insured = insured;
  }

  public String getAgentCode() {
    return agentCode;
  }

  public void setAgentCode(String agentCode) {
    this.agentCode = agentCode;
  }

  public String getBranchCode() {
    return branchCode;
  }

  public void setBranchCode(String branchCode) {
    this.branchCode = branchCode;
  }

  public String getCompanyName() {
    return companyName;
  }

  public void setCompanyName(String companyName) {
    this.companyName = companyName;
  }

  public Date getApplicationSignDate() {
    return applicationSignDate;
  }

  public void setApplicationSignDate(Date applicationSignDate) {
    this.applicationSignDate = applicationSignDate;
  }

  public String getServicingAgentName() {
    return servicingAgentName;
  }

  public void setServicingAgentName(String servicingAgentName) {
    this.servicingAgentName = servicingAgentName;
  }

  public String getOriginalFaceAmount() {
    return originalFaceAmount;
  }

  public void setOriginalFaceAmount(String originalFaceAmount) {
    this.originalFaceAmount = originalFaceAmount;
  }

  public String getCurrency() {
    return currency;
  }

  public void setCurrency(String currency) {
    this.currency = currency;
  }

  public String getPolicyContractOutputType() {
    return policyContractOutputType;
  }

  public void setPolicyContractOutputType(String policyContractOutputType) {
    this.policyContractOutputType = policyContractOutputType;
  }
}
