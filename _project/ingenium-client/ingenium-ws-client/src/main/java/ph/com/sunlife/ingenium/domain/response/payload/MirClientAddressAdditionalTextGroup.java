package ph.com.sunlife.ingenium.domain.response.payload;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import javax.xml.bind.annotation.XmlElement;
import java.util.List;

@XStreamAlias("MirCliAddrAddlTxtG")
public class MirClientAddressAdditionalTextGroup {

    @XStreamImplicit(itemFieldName = "MirCliAddrAddlTxtT")
    private List<String> clientAddressAdditionalTexts;

    public List<String> getClientAddressAdditionalTexts() {
        return clientAddressAdditionalTexts;
    }

    public void setClientAddressAdditionalTexts(final List<String> addressls) {
        clientAddressAdditionalTexts = addressls;
    }
}
