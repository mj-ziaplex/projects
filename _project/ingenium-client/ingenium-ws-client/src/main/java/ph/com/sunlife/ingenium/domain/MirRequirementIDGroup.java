package ph.com.sunlife.ingenium.domain;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

@XStreamAlias("MirReqirIdG")
public class MirRequirementIDGroup {

	@XStreamImplicit(itemFieldName = "MirReqirIdT")
	private List<String> mirRequirementIDTexts;

	public List<String> getMirRequirementIDTexts() {
		return mirRequirementIDTexts;
	}

	public void setMirRequirementIDTexts(List<String> mirRequirementIDTexts) {
		this.mirRequirementIDTexts = mirRequirementIDTexts;
	}
}
