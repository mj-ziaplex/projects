package ph.com.sunlife.ingenium.ws.domain;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import ph.com.sunlife.ingenium.domain.MirUserMsgSevrtyG;
import ph.com.sunlife.ingenium.domain.MirUserMsgTxtG;

@XStreamAlias("OLifE")
public class PlanInquiryContainer extends Container {

    @XStreamAlias("PlanInquiryData")
    private PlanInquiryData planInquiryData;
    @XStreamAlias("MirUserMsgTxtG")
    private MirUserMsgTxtG mirUserMsgTxtG;
    @XStreamAlias("MirUserMsgSevrtyG")
    private MirUserMsgSevrtyG mirUserMsgSevrtyG;

    public PlanInquiryData getPlanInquiryData() {
        return planInquiryData;
    }

    public void setPlanInquiryData(PlanInquiryData planInquiryData) {
        this.planInquiryData = planInquiryData;
    }

    public MirUserMsgTxtG getMirUserMsgTxtG() {
        return mirUserMsgTxtG;
    }

    public void setMirUserMsgTxtG(MirUserMsgTxtG mirUserMsgTxtG) {
        this.mirUserMsgTxtG = mirUserMsgTxtG;
    }

    public MirUserMsgSevrtyG getMirUserMsgSevrtyG() {
        return mirUserMsgSevrtyG;
    }

    public void setMirUserMsgSevrtyG(MirUserMsgSevrtyG mirUserMsgSevrtyG) {
        this.mirUserMsgSevrtyG = mirUserMsgSevrtyG;
    }
}
