package ph.com.sunlife.wms.ingenium.handler;

import org.apache.commons.lang3.StringUtils;
import ph.com.sunlife.ingenium.ws.domain.PlanInquiryData;
import ph.com.sunlife.ingenium.ws.domain.PlanInquiryResponse;
import ph.com.sunlife.ingenium.ws.domain.ReturnCodes;
import ph.com.sunlife.ingenium.ws.domain.converter.SuccessResultCodeAttribute;

public class PlanInquiryResponseHandler extends ResponseHandler {

    private PlanInquiryResponse responseObject;

    public PlanInquiryResponseHandler() {
        xstream.processAnnotations(PlanInquiryResponse.class);
        xstream.registerConverter(new SuccessResultCodeAttribute());
    }

    @Override
    public Object getResponse() {
        return null;
    }

    private PlanInquiryResponse getResponseObject() {
        if (responseObject == null)
            responseObject =
                    (PlanInquiryResponse) xstream.fromXML(getResponseString());
        return responseObject;
    }

    private PlanInquiryData getPlanInquiryData() {
        PlanInquiryResponse response = getResponseObject();
        return response.getTransaction().getContainer().getPlanInquiryData();
    }

    private ReturnCodes getReturnCodes() {
        PlanInquiryResponse response = getResponseObject();
        return response.getTransaction().getContainer().getReturnCodes();
    }

    public String getMirPlanBatchSetlIndicator() {
        PlanInquiryData responseData = getPlanInquiryData();
        if (responseData.getMirCommonFields() != null) {
            return StringUtils
                       .defaultIfEmpty(responseData.getMirCommonFields()
                                                   .getMirPlanBtchSetlInd(),
                                       "");
        }
        return "";
    }
}
