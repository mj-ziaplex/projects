package ph.com.sunlife.ingenium.domain;

import java.util.ArrayList;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("MirBiBnfyPrcedsG")
public class MirBiBnfyPrcedsG {

	@XStreamImplicit(itemFieldName = "MirBiBnfyPrcedsT")
	protected List<String> mirBiBnfyPrcedsT;

	public List<String> getMirBiBnfyPrcedsT() {
		if (mirBiBnfyPrcedsT == null) {
			mirBiBnfyPrcedsT = new ArrayList<String>();
		}
		return this.mirBiBnfyPrcedsT;
	}

}
