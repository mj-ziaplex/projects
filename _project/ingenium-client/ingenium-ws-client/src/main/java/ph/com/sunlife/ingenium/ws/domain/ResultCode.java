package ph.com.sunlife.ingenium.ws.domain;

public class ResultCode {

    private byte Code;
    private String Value;

    public ResultCode() { }

    public ResultCode(final byte code, final String value) {
        this.Code = code;
        this.Value = value;
    }

    public byte getCode() {
        return Code;
    }

    public void setCode(byte code) {
        this.Code = code;
    }

    public String getValue() {
        return Value;
    }

    public void setValue(String value) {
        this.Value = value;
    }
}
