package ph.com.sunlife.ingenium.domain.response.payload;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

@XStreamAlias("MirDvCliAddrYrDurG")
public class MirDvClientAddressYearDurationGroup {

    @XStreamImplicit(itemFieldName = "MirDvCliAddrYrDurT")
    private List<String> mirDvClientAddressYearDurationTexts;

    public List<String> getMirDvClientAddressYearDurationTexts() {
        return mirDvClientAddressYearDurationTexts;
    }

    public void setMirDvClientAddressYearDurationTexts(final List<String> durations) {
        mirDvClientAddressYearDurationTexts = durations;
    }
}
