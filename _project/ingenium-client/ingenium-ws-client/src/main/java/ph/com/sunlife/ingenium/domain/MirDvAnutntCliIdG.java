package ph.com.sunlife.ingenium.domain;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

@XStreamAlias("MirDvAnutntCliIdG")
public class MirDvAnutntCliIdG {

    @XStreamImplicit(itemFieldName = "MirDvAnutntCliIdT")
    private List<String> mirDvAnutntCliIdT;

    public List<String> getMirDvAnutntCliIdT() {
        return mirDvAnutntCliIdT;
    }

    public void setMirDvAnutntCliIdT(List<String> mirDvAnutntCliIdT) {
        this.mirDvAnutntCliIdT = mirDvAnutntCliIdT;
    }
}
