package ph.com.sunlife.ingenium.ws.domain;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("OLifE")
public class RequirementsCreateContainer extends Container  {

    @XStreamAlias("RequirementData")
    private RequirementData requirementData;

    public RequirementData getRequirementData() {
        return requirementData;
    }

    public RequirementsCreateContainer setRequirementData(RequirementData requirementData) {
        this.requirementData = requirementData;
        return this;
    }
}
