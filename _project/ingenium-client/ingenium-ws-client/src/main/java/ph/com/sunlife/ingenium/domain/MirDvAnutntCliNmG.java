package ph.com.sunlife.ingenium.domain;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

@XStreamAlias("MirDvAnutntCliNmG")
public class MirDvAnutntCliNmG {

    @XStreamImplicit(itemFieldName = "MirDvAnutntCliNmT")
    private List<String> mirDvAnutntCliNmT;

    public List<String> getMirDvAnutntCliNmT() {
        return mirDvAnutntCliNmT;
    }

    public void setMirDvAnutntCliNmT(List<String> mirDvAnutntCliNmT) {
        this.mirDvAnutntCliNmT = mirDvAnutntCliNmT;
    }
}
