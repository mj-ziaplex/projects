package ph.com.sunlife.ingenium.domain;

import java.util.ArrayList;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("MirCdCvgRtAgeG")
public class MirCdCvgRtAgeG {

	@XStreamImplicit(itemFieldName = "MirCdCvgRtAgeT")
	protected List<String> mirCdCvgRtAgeT;

	public List<String> getMirCdCvgRtAgeT() {
		if (mirCdCvgRtAgeT == null) {
			mirCdCvgRtAgeT = new ArrayList<String>();
		}
		return this.mirCdCvgRtAgeT;
	}

}
