package ph.com.sunlife.wms.ingenium.handler;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.Predicate;
import org.apache.commons.lang3.StringUtils;
import ph.com.sunlife.ingenium.ws.domain.PolicyData;
import ph.com.sunlife.ingenium.ws.domain.PolicyInquiryResponse;
import ph.com.sunlife.ingenium.ws.domain.ReturnCodes;
import ph.com.sunlife.ingenium.ws.domain.converter.SuccessResultCodeAttribute;
import ph.com.sunlife.wms.ingenium.domain.Policy;

import java.util.ArrayList;
import java.util.List;

public class PolicyInquiryResponseHandler extends ResponseHandler {

  private PolicyInquiryResponse responseObject;

  public PolicyInquiryResponseHandler() {
    xstream.processAnnotations(PolicyInquiryResponse.class);
    xstream.registerConverter(new SuccessResultCodeAttribute());
  }

  @Override
  public Policy getResponse() {
    Policy policy = new Policy();
    policy.setPlanId(getPlanID())
          .setPolicyContractOutputType(getPolicyContractType())
          .setAssignees(getPolicyAssignees());
    return policy;
  }

  private PolicyInquiryResponse getResponseObject() {
    if (responseObject == null)
      responseObject =
              (PolicyInquiryResponse) xstream.fromXML(getResponseString());
    return responseObject;
  }

  private PolicyData getPolicyData() {
    PolicyInquiryResponse response = getResponseObject();
    return response.getTransaction().getContainer().getPolicyData();
  }

  private ReturnCodes getReturnCodes() {
    PolicyInquiryResponse response = getResponseObject();
    return response.getTransaction().getContainer().getReturnCodes();
  }

  public String getPlanID() {
    PolicyData responseData = getPolicyData();
    return StringUtils.isNotEmpty(responseData.getMirPlanID()) ?
           responseData.getMirPlanID() : "";
  }

  public String getPolicyContractType() {
    PolicyData responseData = getPolicyData();
    return StringUtils.isNotEmpty(responseData.getMirPolCntrctTypCd()) ?
           responseData.getMirPolCntrctTypCd() : "";
  }

  public List<String> getPolicyAssignees() {
    PolicyData responseData = getPolicyData();
    List<String> clientNames =
            responseData.getMirDvAsignCliNmG() != null &&
              CollectionUtils.isNotEmpty(responseData.getMirDvAsignCliNmG()
                                                     .getMirDvAsignCliNmT()) ?
            responseData.getMirDvAsignCliNmG().getMirDvAsignCliNmT() :
            new ArrayList<String>(0);
    if (CollectionUtils.isNotEmpty(clientNames)) {
      CollectionUtils.filter(clientNames, new Predicate<String>() {
        @Override
        public boolean evaluate(String s) {
          return !"*".equals(s);
        }
      });
    }
    return clientNames;
  }

  public boolean isExisting() {
    return "00".equals(getReturnCodes().getLsirReturnCode());
  }
}
