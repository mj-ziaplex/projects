package ph.com.sunlife.ingenium.domain;

import java.util.ArrayList;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("MirRtCvgFePremAmtG")
public class MirRtCvgFePremAmtG {

	@XStreamImplicit(itemFieldName = "MirRtCvgFePremAmtT")
	protected List<String> mirRtCvgFePremAmtT;

	public List<String> getMirRtCvgFePremAmtT() {
		if (mirRtCvgFePremAmtT == null) {
			mirRtCvgFePremAmtT = new ArrayList<String>();
		}
		return this.mirRtCvgFePremAmtT;
	}

}
