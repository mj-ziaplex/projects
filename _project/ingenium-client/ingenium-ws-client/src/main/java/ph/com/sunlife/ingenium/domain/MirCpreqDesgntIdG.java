package ph.com.sunlife.ingenium.domain;

import java.util.ArrayList;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("MirCpreqDesgntIdG")
public class MirCpreqDesgntIdG {

	@XStreamImplicit(itemFieldName = "MirCpreqDesgntIdT")
	protected List<String> mirCpreqDesgntIdT;

	public List<String> getMirCpreqDesgntIdT() {
		if (mirCpreqDesgntIdT == null) {
			mirCpreqDesgntIdT = new ArrayList<String>();
		}
		return this.mirCpreqDesgntIdT;
	}

}
