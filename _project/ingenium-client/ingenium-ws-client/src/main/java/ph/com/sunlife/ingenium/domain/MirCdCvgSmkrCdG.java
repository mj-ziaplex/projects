package ph.com.sunlife.ingenium.domain;

import java.util.ArrayList;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("MirCdCvgSmkrCdG")
public class MirCdCvgSmkrCdG {

	@XStreamImplicit(itemFieldName = "MirCdCvgSmkrCdT")
	protected List<String> mirCdCvgSmkrCdT;

	public List<String> getMirCdCvgSmkrCdT() {
		if (mirCdCvgSmkrCdT == null) {
			mirCdCvgSmkrCdT = new ArrayList<String>();
		}
		return this.mirCdCvgSmkrCdT;
	}

}
