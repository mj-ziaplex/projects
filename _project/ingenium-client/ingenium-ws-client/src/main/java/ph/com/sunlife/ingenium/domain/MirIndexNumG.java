package ph.com.sunlife.ingenium.domain;

import java.util.ArrayList;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("MirIndexNumG")
public class MirIndexNumG {

	@XStreamImplicit(itemFieldName = "MirIndexNumT")
	protected List<String> mirIndexNumT;

	public List<String> getMirIndexNumT() {
		if (mirIndexNumT == null) {
			mirIndexNumT = new ArrayList<String>();
		}
		return this.mirIndexNumT;
	}

}
