package ph.com.sunlife.ingenium.domain.response.payload;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

@XStreamAlias("MirUserMsgSevrtyG")
public class MirUserMessageSeverityGroup {

    @XStreamImplicit(itemFieldName = "MirUserMsgSevrtyT")
    private List<String> mirUserMessageSeverityTexts;

    public List<String> getMirUserMessageSeverityTexts() {
        return mirUserMessageSeverityTexts;
    }

    public void setMirUserMessageSeverityTexts(final List<String> mirUserMessageSeverityTexts) {
        this.mirUserMessageSeverityTexts = mirUserMessageSeverityTexts;
    }
}
