package ph.com.sunlife.ingenium.domain;

import java.util.ArrayList;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("MirRtCvgFeReasnCdG")
public class MirRtCvgFeReasnCdG {

	@XStreamImplicit(itemFieldName = "MirRtCvgFeReasnCdT")
	protected List<String> mirRtCvgFeReasnCdT;

	public List<String> getMirRtCvgFeReasnCdT() {
		if (mirRtCvgFeReasnCdT == null) {
			mirRtCvgFeReasnCdT = new ArrayList<String>();
		}
		return this.mirRtCvgFeReasnCdT;
	}

}
