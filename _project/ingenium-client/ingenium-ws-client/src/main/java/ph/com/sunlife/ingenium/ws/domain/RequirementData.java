package ph.com.sunlife.ingenium.ws.domain;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import ph.com.sunlife.ingenium.domain.MirCommonFields;
import ph.com.sunlife.ingenium.domain.MirListFields;

@XStreamAlias("RequirementData")
public class RequirementData {

    @XStreamAlias("MirCommonFields")
     private MirCommonFields mirCommonFields;
    @XStreamAlias("MirListFields")
    private MirListFields mirListFields;

    public MirCommonFields getMirCommonFields() {
        return mirCommonFields;
    }

    public RequirementData setMirCommonFields(final MirCommonFields mirCommonFields) {
        this.mirCommonFields = mirCommonFields;
        return this;
    }

    public MirListFields getMirListFields() {
        return mirListFields;
    }

    public void setMirListFields(MirListFields mirListFields) {
        this.mirListFields = mirListFields;
    }
}
