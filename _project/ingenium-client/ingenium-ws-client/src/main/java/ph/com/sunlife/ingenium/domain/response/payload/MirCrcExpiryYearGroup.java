package ph.com.sunlife.ingenium.domain.response.payload;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

@XStreamAlias("MirCrcXpryYrG")
public class MirCrcExpiryYearGroup {

    @XStreamImplicit(itemFieldName = "MirCrcXpryYrT")
    private List<Byte> mirCrcExpiryYearTexts;

    public List<Byte> getMirCrcExpiryYearTexts() {
        return mirCrcExpiryYearTexts;
    }

    public void setMirCrcExpiryYearTexts(List<Byte> mirCrcExpiryYearTexts) {
        this.mirCrcExpiryYearTexts = mirCrcExpiryYearTexts;
    }
}
