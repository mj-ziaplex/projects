package ph.com.sunlife.zold.ws.ingenium;

import javax.swing.JOptionPane;
import java.util.Properties;
import java.util.Enumeration;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;


public class Profiles {
  private String pathName;
  private Properties prop = new Properties();
  private File file;


  /**
   * Constructor
   */
  public Profiles() {
  }

  /**
   * Constructor with string and boolean parameters
   * @param  pathName, isAbsolute
   *
   */
  public Profiles(String pathName, boolean isAbsolute) {
    if (isAbsolute)
      setAbsolutePath(pathName);
    else
      setRelativePath(pathName);
    loadFile();
  }

  /**
   * Set relative path
   *
   * @param  relativePath
   *
   */
  private void setRelativePath(String relativePath) {
    String path = this.getClass().getResource(relativePath).getPath();
    file = new File(path);
  }

  /**
   * Set absolute path
   *
   * @param  absolutePath
   *
   */
  private void setAbsolutePath(String absolutePath) {
    file = new File(absolutePath);
  }

  /**
   * Load a file specified by its path
   *
   * @param  path
   *
   */
  public void loadFile() {
    try {
      FileInputStream fi = new FileInputStream(file);
      prop.load(fi);
      fi.close();
    } catch (FileNotFoundException ex) {
      JOptionPane.showMessageDialog(null, "FileNotFoundException: " + ex.getMessage());
    } catch (IOException ex) {
      JOptionPane.showMessageDialog(null, "IOException: " + ex.getMessage());
    }
  }

  /**
   * get the value of the corresponding key; returns null if key not found
   *
   * @param  key
   *
   * @return  <CODE>String</CODE>
   *
   */
  public String getValue(String key) {
    return prop.getProperty(key);
  }

  /**
   * get the values of all the keys
   *
   *
   * @return  <CODE>Enumeration</CODE>
   */
  public Enumeration getKeys() {
    return prop.propertyNames();
  }

  /**
   * Add a new key-pair value to the file
   *
   * @param  key
   * @param  value
   *
   */
  public void putValue(String key, String value) {
    if (file.exists()) {
      prop.setProperty(key, value);
      try {
        FileOutputStream fo = new FileOutputStream(pathName);
        prop.store(fo, file.getName());
        fo.close();
      } catch (FileNotFoundException ex) {
        JOptionPane.showMessageDialog(null, "FileNotFoundException: " + ex.getMessage());
      } catch (IOException ex) {
        JOptionPane.showMessageDialog(null, "IOException: " + ex.getMessage());
      }
    } else {
      JOptionPane.showMessageDialog(null, file.getAbsolutePath() + " does not exist");
    }
  }

}
