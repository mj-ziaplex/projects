package ph.com.sunlife.ingenium.domain;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

@XStreamAlias("MirPolCliAsignDtG")
public class MirPolCliAsignDtG {

    @XStreamImplicit(itemFieldName = "MirPolCliAsignDtT")
    private List<String> mirPolCliAsignDtT;

    public List<String> getMirPolCliAsignDtT() {
        return mirPolCliAsignDtT;
    }

    public void setMirPolCliAsignDtT(List<String> mirPolCliAsignDtT) {
        this.mirPolCliAsignDtT = mirPolCliAsignDtT;
    }
}
