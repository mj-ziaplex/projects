package ph.com.sunlife.ingenium.domain.response.payload;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

@XStreamAlias("MirBnkAcctTypCdG")
public class MirBankAccountTypeCodeGroup {

    @XStreamImplicit(itemFieldName = "MirBnkAcctTypCdT")
    private List<String> mirBankAccountTypeCodeTypes;

    public List<String> getMirBankAccountTypeCodeTypes() {
        return mirBankAccountTypeCodeTypes;
    }

    public MirBankAccountTypeCodeGroup setMirBankAccountTypeCodeTypes(final List<String> types) {
        this.mirBankAccountTypeCodeTypes = types;
        return this;
    }
}
