package ph.com.sunlife.ingenium.domain;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

@XStreamAlias("MirDvAsignCliIdG")
public class MirDvAsignCliIdG {

    @XStreamImplicit(itemFieldName = "MirDvAsignCliIdT")
    private List<String> mirDvAsignCliIdT;

    public List<String> getMirDvAsignCliIdT() {
        return mirDvAsignCliIdT;
    }

    public void setMirDvAsignCliIdT(List<String> mirDvAsignCliIdT) {
        this.mirDvAsignCliIdT = mirDvAsignCliIdT;
    }
}
