package ph.com.sunlife.ingenium.ws.domain;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("TXLife")
public class Request extends RequestResponse {

  @XStreamAlias("UserAuthRequest")
  private Credentials credentials;
  @XStreamAlias("TXLifeRequest")
  private Transaction transaction;

  public Request() {
    credentials = new BasicCredentials();
  }

  public Request(final Credentials credentials) {
    this.credentials = credentials;
  }

  public Request(final Credentials credentials, final Transaction transaction) {
    this.credentials = credentials;
    this.transaction = transaction;
  }

  public Credentials getCredentials() {
    return credentials;
  }

  public Request setCredentials(final Credentials credentials) {
    this.credentials = credentials;
    return this;
  }

  public Transaction getTransaction() {
    return transaction;
  }

  public void setTransaction(final Transaction transaction) {
    this.transaction = transaction;
  }
}
