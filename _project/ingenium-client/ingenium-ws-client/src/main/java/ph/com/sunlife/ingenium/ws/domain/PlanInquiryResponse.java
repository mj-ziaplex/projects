package ph.com.sunlife.ingenium.ws.domain;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("TXLife")
public class PlanInquiryResponse {

    @XStreamAlias("UserAuthResponse")
    private ResponseMetadata metadata;
    @XStreamAlias("TXLifeResponse")
    private PlanInquiryTransactionResponse transaction;

    public ResponseMetadata getMetadata() {
        return metadata;
    }

    public void setMetadata(ResponseMetadata metadata) {
        this.metadata = metadata;
    }

    public PlanInquiryTransactionResponse getTransaction() {
        return transaction;
    }

    public void setTransaction(PlanInquiryTransactionResponse transaction) {
        this.transaction = transaction;
    }
}
