package ph.com.sunlife.ingenium.domain;

import java.util.ArrayList;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("MirRtCvgMeRatCdG")
public class MirRtCvgMeRatCdG {

	@XStreamImplicit(itemFieldName = "MirRtCvgMeRatCdT")
	protected List<String> mirRtCvgMeRatCdT;

	public List<String> getMirRtCvgMeRatCdT() {
		if (mirRtCvgMeRatCdT == null) {
			mirRtCvgMeRatCdT = new ArrayList<String>();
		}
		return this.mirRtCvgMeRatCdT;
	}

}
