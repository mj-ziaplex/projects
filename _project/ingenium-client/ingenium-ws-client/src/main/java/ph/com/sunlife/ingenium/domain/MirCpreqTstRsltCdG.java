package ph.com.sunlife.ingenium.domain;

import java.util.ArrayList;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("MirCpreqTstRsltCdG")
public class MirCpreqTstRsltCdG {

	@XStreamImplicit(itemFieldName = "MirCpreqTstRsltCdT")
	protected List<String> mirCpreqTstRsltCdT;

	public List<String> getMirCpreqTstRsltCdT() {
		if (mirCpreqTstRsltCdT == null) {
			mirCpreqTstRsltCdT = new ArrayList<String>();
		}
		return this.mirCpreqTstRsltCdT;
	}

}
