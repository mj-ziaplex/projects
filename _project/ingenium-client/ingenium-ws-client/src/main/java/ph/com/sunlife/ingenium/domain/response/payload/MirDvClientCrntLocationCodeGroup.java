package ph.com.sunlife.ingenium.domain.response.payload;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

@XStreamAlias("MirDvCliCrntLocCdG")
public class MirDvClientCrntLocationCodeGroup {

    @XStreamImplicit(itemFieldName = "MirDvCliCrntLocCdT")
    private List<String> dvClientCrntLocationCodeTexts;

    public List<String> getDvClientCrntLocationCodeTexts() {
        return dvClientCrntLocationCodeTexts;
    }

    public void setDvClientCrntLocationCodeTexts(final List<String> locationCodes) {
        dvClientCrntLocationCodeTexts = locationCodes;
    }
}
