package ph.com.sunlife.ingenium.domain.response.payload;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("MirBnkInfo")
public class MirBankInformation {

    @XStreamAlias("MirBnkIdG")
    private MirBankIDGroup idGroup;
    @XStreamAlias("MirBnkAcctIdG")
    private MirBankAccountIDGroup accountIDGroup;
    @XStreamAlias("MirBnkAcctHldrNmG")
    private MirBankAccountHolderNameGroup accountHolderNameGroup;
    @XStreamAlias("MirBnkAcctTypCdG")
    private MirBankAccountTypeCodeGroup accountTypeCodeGroup;
    @XStreamAlias("MirCliBnkAcctNumG")
    private MirBankAccountNumberGroup clientBankAccountNumberGroup;
    @XStreamAlias("MirBnkBrIdG")
    private MirBankBrIDGroup brIDGroup;
    @XStreamAlias("MirBnkNmG")
    private MirBankNameGroup nameGroup;
    @XStreamAlias("MirBnkAcctMicrIndG")
    private MirBankAccountMicrIndGroup accountMicrIndGroup;
    @XStreamAlias("MirCliBnkAcctDtG")
    private MirBankAccountDateGroup accountDateGroup;
    @XStreamAlias("MirDvCliBnkAcctIndG")
    private MirDvClientBankAccountIndGroup clientBankAccountIndGroup;

    public MirBankIDGroup getIdGroup() {
        return idGroup;
    }

    public MirBankInformation setIdGroup(MirBankIDGroup idGroup) {
        this.idGroup = idGroup;
        return this;
    }

    public MirBankAccountIDGroup getAccountIDGroup() {
        return accountIDGroup;
    }

    public MirBankInformation setAccountIDGroup(MirBankAccountIDGroup accountIDGroup) {
        this.accountIDGroup = accountIDGroup;
        return this;
    }

    public MirBankAccountHolderNameGroup getAccountHolderNameGroup() {
        return accountHolderNameGroup;
    }

    public MirBankInformation setAccountHolderNameGroup(MirBankAccountHolderNameGroup accountHolderNameGroup) {
        this.accountHolderNameGroup = accountHolderNameGroup;
        return this;
    }

    public MirBankAccountTypeCodeGroup getAccountTypeCodeGroup() {
        return accountTypeCodeGroup;
    }

    public MirBankInformation setAccountTypeCodeGroup(MirBankAccountTypeCodeGroup accountTypeCodeGroup) {
        this.accountTypeCodeGroup = accountTypeCodeGroup;
        return this;
    }

    public MirBankAccountNumberGroup getClientBankAccountNumberGroup() {
        return clientBankAccountNumberGroup;
    }

    public MirBankInformation setClientBankAccountNumberGroup(MirBankAccountNumberGroup clientBankAccountNumberGroup) {
        this.clientBankAccountNumberGroup = clientBankAccountNumberGroup;
        return this;
    }

    public MirBankBrIDGroup getBrIDGroup() {
        return brIDGroup;
    }

    public MirBankInformation setBrIDGroup(MirBankBrIDGroup brIDGroup) {
        this.brIDGroup = brIDGroup;
        return this;
    }

    public MirBankNameGroup getNameGroup() {
        return nameGroup;
    }

    public MirBankInformation setNameGroup(MirBankNameGroup nameGroup) {
        this.nameGroup = nameGroup;
        return this;
    }

    public MirBankAccountMicrIndGroup getAccountMicrIndGroup() {
        return accountMicrIndGroup;
    }

    public MirBankInformation setAccountMicrIndGroup(MirBankAccountMicrIndGroup accountMicrIndGroup) {
        this.accountMicrIndGroup = accountMicrIndGroup;
        return this;
    }

    public MirBankAccountDateGroup getAccountDateGroup() {
        return accountDateGroup;
    }

    public MirBankInformation setAccountDateGroup(MirBankAccountDateGroup accountDateGroup) {
        this.accountDateGroup = accountDateGroup;
        return this;
    }

    public MirDvClientBankAccountIndGroup getClientBankAccountIndGroup() {
        return clientBankAccountIndGroup;
    }

    public MirBankInformation setClientBankAccountIndGroup(MirDvClientBankAccountIndGroup clientBankAccountIndGroup) {
        this.clientBankAccountIndGroup = clientBankAccountIndGroup;
        return this;
    }
}
