package ph.com.sunlife.ingenium.domain;

import java.util.ArrayList;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("MirCdCvgMatXpryDtG")
public class MirCdCvgMatXpryDtG {

	@XStreamImplicit(itemFieldName = "MirCdCvgMatXpryDtT")
	protected List<String> mirCdCvgMatXpryDtT;

	public List<String> getMirCdCvgMatXpryDtT() {
		if (mirCdCvgMatXpryDtT == null) {
			mirCdCvgMatXpryDtT = new ArrayList<String>();
		}
		return this.mirCdCvgMatXpryDtT;
	}

}
