package ph.com.sunlife.zold.passwordgenerator;

/**
 * @author PM13
 */
public class PasswordGenerator {

  private SecKeyHandler keyhandler = SecKeyHandler.getInstance(); //singelton keyhandler
  private Hash_MD5 md5 = new Hash_MD5();  // hash_md5 object.

  /**
   * Creates a new instance of PasswordGenerator
   */
  public PasswordGenerator() {
  }

  public void updateKey(String p_sKey) {
    keyhandler.updateKey(p_sKey);
  }

  public void updateDisplacement(int p_iDisplacement) {
    keyhandler.updateDisplacement(p_iDisplacement);
  }

  public void updateFirstChar(char p_cFirstChar) {
    keyhandler.updateFirstChar(p_cFirstChar);
  }

  public void updatePassword(String p_sPassword) {
    keyhandler.updatePassword(p_sPassword);
  }

  public void updateUsername(String p_sUsername) {
    keyhandler.updateUserName(p_sUsername);
  }

  public char getFirstChar() {
    return keyhandler.getFirstChar();
  }

  public String getKey() {
    return keyhandler.getKey();
  }

  public int getDisplacement() {
    return keyhandler.getDisplacement();
  }

  public String getUserName() {
    return keyhandler.getUserName();
  }

  public String getPassword() {
    return keyhandler.getPassword();
  }

  public void saveSettings() {
    keyhandler.serialize();
  }

  //generates the password by calling the createPassword method while retrieving the keyhandler
  public String generatePassword(String p_username) {
    return createPassword(p_username, keyhandler.getKey(), keyhandler.getDisplacement(), keyhandler.getFirstChar());
  }

  //does the necessary steps before generating the password
  private String createPassword(String p_username, String skey, int iDisplacement, char FirstChar) {
    String TempPassword = "";
    //hash the appended value of the username and key using MD5 algorithm
    TempPassword = md5.calcMD5(p_username.toLowerCase() + skey);
    //validates that the diplacement is less than 26 since 7 characters are needed to be retrieved out of the 32 characters
    //System.out.println("TempPassword");
    //System.out.println(iDisplacement);
    //System.out.println(TempPassword);

    if (iDisplacement <= 26)
      TempPassword = TempPassword.substring(iDisplacement - 1, iDisplacement + 6);
    //append the first character into the start of the generated password.
    TempPassword = FirstChar + TempPassword;
    return TempPassword;
  }


}
