package ph.com.sunlife.ingenium.domain.response.payload;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

@XStreamAlias("MirCrcBnkIdG")
public class MirCrcBankIdGroup {

    @XStreamImplicit(itemFieldName = "MirCrcBnkIdT")
    private List<String> mirCrcBankIdTexts;

    public List<String> getMirCrcBankIdTexts() {
        return mirCrcBankIdTexts;
    }

    public void setMirCrcBankIdTexts(List<String> mirCrcBankIdTexts) {
        this.mirCrcBankIdTexts = mirCrcBankIdTexts;
    }
}
