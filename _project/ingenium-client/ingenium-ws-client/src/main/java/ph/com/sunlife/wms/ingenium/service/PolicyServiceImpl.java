package ph.com.sunlife.wms.ingenium.service;

import org.apache.commons.lang3.StringUtils;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ph.com.sunlife.ingenium.IngeniumWebServiceClient;
import ph.com.sunlife.ingenium.utils.PolicyTypeChecker;
import ph.com.sunlife.wms.ingenium.configuration.IngeniumClientConfigurator;
import ph.com.sunlife.wms.ingenium.domain.*;
import ph.com.sunlife.wms.ingenium.handler.*;
import ph.com.sunlife.wms.ingenium.ws.client.*;
import ph.com.sunlife.zold.ws.ingenium.IngeniumDispatcher;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public class PolicyServiceImpl implements PolicyService {

  private ApplicationContext context;
  private IngeniumDispatcher ingeniumDispatcher;

  public PolicyServiceImpl() {
    context = new AnnotationConfigApplicationContext(IngeniumClientConfigurator.class);
    ingeniumDispatcher = new IngeniumDispatcher();
    ingeniumDispatcher.IngeniumInit((String) context.getBean("oldIngeniumUsername"),
        (String) context.getBean("oldIngeniumServer"),
        (String) context.getBean("oldIngeniumTarget"));
  }

  @Override
  public Coverage getCoverageInformation(final String policyId) throws Exception {
    String response = new IngeniumWebServiceClient()
        .getResponse(new InquiryConsolidatedInformationRequestBuilder(policyId)
            .create());
    InquiryConsolidatedInformationResponseHandler handler =
        new InquiryConsolidatedInformationResponseHandler();
    handler.setResponseString(response);
    return handler.getCoverage(PolicyTypeChecker.isVUL(policyId));
  }

  @Override
  public byte getStructedNoteStatus(final String policyId) {
    String response = new IngeniumWebServiceClient()
        .getResponse(new PolicyInquiryRequestBuilder(policyId)
            .create());
    PolicyInquiryResponseHandler policyHandler = new PolicyInquiryResponseHandler();
    policyHandler.setResponseString(response);
    String planId = policyHandler.getPlanID();
    if (StringUtils.isNotEmpty(planId)) {
      response = new IngeniumWebServiceClient()
          .getResponse(new PlanInquiryRequestBuilder(planId)
              .create());
      PlanInquiryResponseHandler planHandler = new PlanInquiryResponseHandler();
      planHandler.setResponseString(response);
      String planBatchSetIndicator = planHandler.getMirPlanBatchSetlIndicator();
      if (StringUtils.isNotEmpty(planBatchSetIndicator)) {
        if ("Y".equals(planBatchSetIndicator)) {
          return 1;
        } else if ("N".equals(planBatchSetIndicator)) {
          return 0;
        } else {
          return -1;
        }
      } else {
        return -1;
      }
    }
    return -1;
  }

  @Override
  public ConsolidatedInformation getPolicyClientDetails(final String policyId) throws Exception {

//    String response = new IngeniumWebServiceClient()
//            .getResponse(new PolicyInquiryRequestBuilder(policyId)
//                    .create());
//    PolicyInquiryResponseHandler handler = new PolicyInquiryResponseHandler();
//    handler.setResponseString(response);


    ConsolidatedInformation consolidatedInformation = new ConsolidatedInformation();
    Client owner = new Client();
    Client insured = new Client();

    Map<String, List<String>> details = ingeniumDispatcher.getPolicyClientDetails(policyId);

    if (details.get("INSURED_CLIENT_NO") == null || details.get("OWNER_ID") == null) {
      throw new Exception("Insured or client details from ingenium is invalid");
    }

    DateFormat formatter = (DateFormat) context.getBean("dateFormatter");

    consolidatedInformation.setAgentCode(details.get("AGENT_CODE").get(0));
    consolidatedInformation.setServicingAgentName(details.get("SERVICING_AGENT_NAME").get(0));
    consolidatedInformation.setBranchCode(details.get("BRANCH_CODE").get(0));
    consolidatedInformation.setApplicationSignDate(formatter.parse(details.get("APP_SIGN_DATE").get(0)));
    consolidatedInformation.setOriginalFaceAmount(details.get("ORIGINAL_FACE_AMOUNT").get(0));
    consolidatedInformation.setCurrency(details.get("CURRENCY_CODE" ).get(0));
    owner.setId(details.get("OWNER_ID").get(0));
    insured.setId(details.get("INSURED_CLIENT_NO").get(0));

    String ownerSex = details.get("SEX").get(0);

    if (!"C".equalsIgnoreCase(ownerSex)) {
      owner.setFamilyName(details.get("CLIENT_LASTNAME").get(0));
      owner.setGivenName(details.get("CLIENT_FIRSTNAME").get(0));
      owner.setMiddlename(details.get("CLIENT_MIDDLENAME").get(0));
      owner.setBirthdate(formatter.parse(details.get("CLIENT_BDAY").get(0)));
    } else {
      owner.setFamilyName(details.get("COMPANY_NAME").get(0));
      owner.setGivenName(details.get("COMPANY_NAME").get(0));
      owner.setMiddlename(details.get("").get(0));
    }

    if (!owner.getId().equalsIgnoreCase(insured.getId())) {
      Map<String, List<String>> insuredInfo = ingeniumDispatcher.getClientDetails(insured.getId());
      insured.setFamilyName(insuredInfo.get("CLIENT_LASTNAME").get(0));
      insured.setGivenName(insuredInfo.get("CLIENT_FIRSTNAME").get(0));
      insured.setMiddlename(insuredInfo.get("CLIENT_MIDDLENAME").get(0));
    } else {
      insured.setFamilyName(owner.getFamilyName());
      insured.setGivenName(owner.getGivenName());
      insured.setMiddlename(owner.getMiddlename());
      if (!"C".equalsIgnoreCase(ownerSex)) {
        insured.setBirthdate(owner.getBirthdate());
      }
    }

    consolidatedInformation.setOwner(owner);
    consolidatedInformation.setInsured(insured);

    PolicyInquiryRequestBuilder builder = new PolicyInquiryRequestBuilder("0880059770");
    IngeniumWebServiceClient client = new IngeniumWebServiceClient();
    PolicyInquiryResponseHandler handler = new PolicyInquiryResponseHandler();
    String policyContractOutputType;

    handler.setResponseString(client.getResponse(builder.create()));
    policyContractOutputType = handler.getPolicyContractType();

    consolidatedInformation.setPolicyContractOutputType(policyContractOutputType);

    return consolidatedInformation;
  }

  @Override
  public List<Requirement> getRequirement(final String policyId) {
    return (List<Requirement>) new ListRequirementsResponseHandler()
        .setResponseString(new IngeniumWebServiceClient()
            .getResponse(new ListRequirementsRequestBuilder(policyId)
                .create()))
        .getResponse();
  }

  @Override
  public String getPlanId(final String policyId) {
    String response = new IngeniumWebServiceClient()
        .getResponse(new PolicyInquiryRequestBuilder(policyId)
            .create());
    PolicyInquiryResponseHandler handler = new PolicyInquiryResponseHandler();
    handler.setResponseString(response);
    return handler.getPlanID();
  }

  @Override
  public List<String> getPolicyAssignees(final String policyId) {
    String response = new IngeniumWebServiceClient()
        .getResponse(new PolicyInquiryRequestBuilder(policyId)
            .create());
    PolicyInquiryResponseHandler handler = new PolicyInquiryResponseHandler();
    handler.setResponseString(response);
    return handler.getPolicyAssignees();
  }

  @Override
  public boolean createRequirement(String policyId, String requirementId, String status) {
    Map<String, List<Object>> returnCodes =
        ingeniumDispatcher.createClientRequirement(policyId.substring(0, 9),
            requirementId, status);
    if ("[00]".equals(returnCodes.toString()) ||
        "[00, 00]".equals(returnCodes.toString())) {
      return true;
    }
    return false;

//    return (Client) new ClientInquiryResponseHandler()
//            .setResponseString(new IngeniumWebServiceClient()
//                    .getResponse(new ClientInquiryRequestBuilder(clientId)
//                            .setTransactionReferenceGUID(guid)
//                            .create())).getResponse();

  }
}
