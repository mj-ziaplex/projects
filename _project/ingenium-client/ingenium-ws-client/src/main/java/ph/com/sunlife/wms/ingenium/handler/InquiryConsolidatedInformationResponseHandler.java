package ph.com.sunlife.wms.ingenium.handler;

import org.apache.commons.collections4.CollectionUtils;
import ph.com.sunlife.ingenium.ws.domain.InquiryConsolidatedInformationData;
import ph.com.sunlife.ingenium.ws.domain.InquiryConsolidatedInformationResponse;
import ph.com.sunlife.ingenium.ws.domain.ReturnCodes;
import ph.com.sunlife.ingenium.ws.domain.converter.SuccessResultCodeAttribute;
import ph.com.sunlife.wms.ingenium.domain.ConsolidatedInformation;
import ph.com.sunlife.wms.ingenium.domain.Coverage;

import java.math.BigDecimal;

public class InquiryConsolidatedInformationResponseHandler extends ResponseHandler {

  private InquiryConsolidatedInformationResponse responseObject;

  public InquiryConsolidatedInformationResponseHandler() {
    xstream.processAnnotations(InquiryConsolidatedInformationResponse.class);
    xstream.registerConverter(new SuccessResultCodeAttribute());
  }

  @Override
  public ConsolidatedInformation getResponse() {
    ConsolidatedInformation consolidatedInformation =
        new ConsolidatedInformation();
    return consolidatedInformation;
  }

  private InquiryConsolidatedInformationResponse getResponseObject() {
    if (responseObject == null)
      responseObject =
          (InquiryConsolidatedInformationResponse) xstream.fromXML(getResponseString());
    return responseObject;
  }

  private InquiryConsolidatedInformationData getConsolidatedInformationData() {
    return getResponseObject().getTransaction().getContainer()
                                               .getInquiryConsolidatedInformationData();
  }

  private ReturnCodes getReturnCodes() {
    return getResponseObject().getTransaction().getContainer().getReturnCodes();
  }

  public Coverage getCoverage(final boolean isVul) {
    InquiryConsolidatedInformationData responseData = getConsolidatedInformationData();
    Coverage coverage = new Coverage();
    coverage.setCurrencyCode(responseData.getMirHiPolCrcyCd());
    if (responseData.getMirCdCvgFaceAmtG() != null &&
          CollectionUtils.isNotEmpty(responseData.getMirCdCvgFaceAmtG()
                                                 .getMirCdCvgFaceAmtT())) {
      if (isVul && responseData.getMirCdCvgFaceAmtG()
                               .getMirCdCvgFaceAmtT().size() > 1) {
        coverage.setPolicyType("VUL");
        coverage.setFaceAmount(new BigDecimal(responseData.getMirCdCvgFaceAmtG()
                                                          .getMirCdCvgFaceAmtT().get(1)));
      } else {
        coverage.setPolicyType("TRAD");
        coverage.setFaceAmount(new BigDecimal(responseData.getMirCdCvgFaceAmtG()
                                                          .getMirCdCvgFaceAmtT().get(0)));
      }
      return coverage;
    }
    return null;
  }


}
