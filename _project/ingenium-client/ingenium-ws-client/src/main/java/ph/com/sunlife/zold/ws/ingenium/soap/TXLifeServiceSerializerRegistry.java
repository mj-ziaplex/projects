package ph.com.sunlife.zold.ws.ingenium.soap;

import com.sun.xml.rpc.client.BasicService;
import com.sun.xml.rpc.encoding.SerializerConstants;

import javax.xml.rpc.encoding.TypeMappingRegistry;

public class TXLifeServiceSerializerRegistry implements SerializerConstants {

  public TXLifeServiceSerializerRegistry() { }

  public TypeMappingRegistry getRegistry() {
    return BasicService.createStandardTypeMappingRegistry();
  }
}