package ph.com.sunlife.wms.ingenium.domain;

import java.math.BigDecimal;

public class Coverage {

  private String policyType;
  private BigDecimal faceAmount;
  private String currencyCode;

  public String getPolicyType() {
    return policyType;
  }

  public void setPolicyType(String policyType) {
    this.policyType = policyType;
  }

  public BigDecimal getFaceAmount() {
    return faceAmount;
  }

  public void setFaceAmount(BigDecimal faceaAmount) {
    this.faceAmount = faceaAmount;
  }

  public String getCurrencyCode() {
    return currencyCode;
  }

  public void setCurrencyCode(String currencyCode) {
    this.currencyCode = currencyCode;
  }
}
