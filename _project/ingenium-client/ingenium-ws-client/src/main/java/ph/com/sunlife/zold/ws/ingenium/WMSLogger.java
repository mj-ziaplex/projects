package ph.com.sunlife.zold.ws.ingenium;

import java.io.File;
import java.io.RandomAccessFile;

public class WMSLogger {
  String ObjectError = "";
  RandomAccessFile writer = null;
  File f = null;

//  ResourceBundle rb = ResourceBundle
//      .getBundle("com.ph.sunlife.component.properties.Ingenium");

  public WMSLogger(String Object) {
    ObjectError = Object;
    try {
      //rb = ResourceBundle.getBundle("c:\\WMS_SharedFolder\\SMS");
//				f = new File(rb.getString("LogTo"));
    } catch (Exception e1) {
      // TODO Auto-generated catch block
      e1.printStackTrace();
    }
  }

  public void write(String strToWrite, String strType) {

    try {
//	    		writer = new RandomAccessFile(f,"rw");  
//	    		writer.seek(writer.length());
//	    		StringBuffer sb = new StringBuffer();
//	    		Date d;
//	    		GregorianCalendar c = new GregorianCalendar();
//	    		d = c.getTime();
//	    		sb.append((d.getYear()-100+2000));
//				sb.append("-");
//				sb.append(d.getMonth());
//				sb.append("-");
//				sb.append(d.getMonth());
//				sb.append("-");
//				sb.append(d.getDay());
//				sb.append(" ");
//				sb.append(d.getHours());
//				sb.append(":");
//				sb.append(d.getMinutes());
//				sb.append(":");
//				sb.append(d.getSeconds());
//				sb.append(" | ");
//				sb.append(strType);
//				sb.append(" | ");
//				sb.append(ObjectError);
//				sb.append(" : ");
//				sb.append(strToWrite);		
//				sb.append("\n");
//				System.out.println(sb.toString());
//				writer.writeBytes(sb.toString());
//				writer.close();
    } catch (Exception e) {

    }
  }

  public void debug(String strErrorCode) {
    write(strErrorCode, "DEBUG");
  }

  /**
   * debug() - logs a debug message given an errorcode, and appends the stack trace.
   *
   * @strErrorCode
   * @'exception - exception object
   */
  public void info(String strErrorCode) {
    write(strErrorCode, "INFO");
  }


  /**
   * warn() - logs an warning message given an errorcode
   *
   * @strErrorCode
   */
  public void warn(String strErrorCode) {
    write(strErrorCode, "WARN");
  }

  /**
   * error() - logs an error message given an errorcode
   *
   * @strErrorCode
   */
  public void error(String strErrorCode) {
    write(strErrorCode, "ERROR");
  }


  /**
   * fatal() - logs a fatal exception message given an errorcode
   *
   * @strErrorCode
   */
  public void fatal(String strErrorCode) {
    write(strErrorCode, "FATAL");
  }


}
