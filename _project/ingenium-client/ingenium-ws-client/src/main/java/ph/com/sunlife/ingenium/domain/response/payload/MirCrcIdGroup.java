package ph.com.sunlife.ingenium.domain.response.payload;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

@XStreamAlias("MirCrcIdG")
public class MirCrcIdGroup {

    @XStreamImplicit(itemFieldName = "MirCrcIdT")
    private List<String> mirCrcIdTexts;

    public List<String> getMirCrcIdTexts() {
        return mirCrcIdTexts;
    }

    public void setMirCrcIdTexts(List<String> mirCrcIdTexts) {
        this.mirCrcIdTexts = mirCrcIdTexts;
    }
}
