package ph.com.sunlife.ingenium.domain;

import java.util.ArrayList;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("MirBiCvgNumG")
public class MirBiCvgNumG {

	@XStreamImplicit(itemFieldName = "MirBiCvgNumT")
	protected List<String> mirBiCvgNumT;

	public List<String> getMirBiCvgNumT() {
		if (mirBiCvgNumT == null) {
			mirBiCvgNumT = new ArrayList<String>();
		}
		return this.mirBiCvgNumT;
	}

}
