package ph.com.sunlife.ingenium.domain;

import java.util.ArrayList;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("MirOpiAgtStatCdG")
public class MirOpiAgtStatCdG {

	@XStreamImplicit(itemFieldName = "MirOpiAgtStatCdT")
	protected List<String> mirOpiAgtStatCdT;

	public List<String> getMirOpiAgtStatCdT() {
		if (mirOpiAgtStatCdT == null) {
			mirOpiAgtStatCdT = new ArrayList<String>();
		}
		return this.mirOpiAgtStatCdT;
	}

}
