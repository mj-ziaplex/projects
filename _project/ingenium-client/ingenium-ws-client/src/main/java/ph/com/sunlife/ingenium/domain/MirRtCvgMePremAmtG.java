package ph.com.sunlife.ingenium.domain;

import java.util.ArrayList;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("MirRtCvgMePremAmtG")
public class MirRtCvgMePremAmtG {

	@XStreamImplicit(itemFieldName = "MirRtCvgMePremAmtT")
	protected List<String> mirRtCvgMePremAmtT;

	public List<String> getMirRtCvgMePremAmtT() {
		if (mirRtCvgMePremAmtT == null) {
			mirRtCvgMePremAmtT = new ArrayList<String>();
		}
		return this.mirRtCvgMePremAmtT;
	}

}
