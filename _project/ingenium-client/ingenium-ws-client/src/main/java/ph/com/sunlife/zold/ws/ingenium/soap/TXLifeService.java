package ph.com.sunlife.zold.ws.ingenium.soap;

import javax.xml.rpc.Service;

public interface TXLifeService extends Service {

    TXLifeServicePort getTXLifeService();
}