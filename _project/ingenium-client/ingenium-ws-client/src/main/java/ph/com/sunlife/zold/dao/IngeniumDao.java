package ph.com.sunlife.zold.dao;

import org.w3c.dom.*;

import javax.sql.rowset.CachedRowSet;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMResult;
import javax.xml.transform.stream.StreamSource;
import java.sql.SQLException;
import java.util.HashMap;

public class IngeniumDao {
	
	//ROBIN Mods -mcasio 4/5/2011 (start)
	private String m_sIngeniumServer = "";
	//ROBIN Mods -mcasio 4/5/2011 (end)
	
//	private WmsDbManager wmsDbManager = null;
	
	/**
	 * Default constructor.  Initializes connection to the DB Manager.
	 */
//	public IngeniumDao() {
//		wmsDbManager = new WmsDbManager();
//	}
	
	//ROBIN Mods -mcasio 4/5/2011 (start)
	public IngeniumDao(String sIngeniumServer) {
		m_sIngeniumServer = sIngeniumServer;
//		wmsDbManager = new WmsDbManager();
	}
	//ROBIN Mods -mcasio 4/5/2011 (end)

	/**
	 * Gets the plan description by plan id.
	 * @param planId
	 * @return
	 */
	public String getPlanDescriptionById(String planId) {
//		ResourceBundle rb = ResourceBundle.getBundle("com.ph.sunlife.component.properties.ComponentProperties");
		
		//String svr = rb.getString("INGENIUM_SERVER");
		String svr = "";
		//ROBIN Mods -mcasio 4/5/2011 (start)
		if (m_sIngeniumServer.trim().length() > 0 ){
			svr = m_sIngeniumServer;
		} else {
//			svr = rb.getString("INGENIUM_SERVER");
			svr = "UDIN6631"; //----------------------------------------------------------------------------
		}
		//ROBIN Mods -mcasio 4/5/2011 (end)
		
		String sql = "SELECT * FROM OPENQUERY ("
				+ svr
				+ ", 'SELECT * FROM APEN91.VEDIT WHERE ETBL_TYP_ID = ''PLAN''  and ETBL_VALU_ID=''"
				+ planId.trim() + "'' ')";
		String plandesc = "";
		try {
//			CachedRowSet rs = wmsDbManager.doSelect(sql);
			CachedRowSet rs = null;

			if (rs.next()) {
				plandesc = rs.getString("ETBL_DESC_TXT");

			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return plandesc;
	}
	
	
	/**
	 * Gets the NBO name by id.
	 * @param nbo
	 * @return
	 */
	public String getNBONameById(String nbo) {
		String sql = "SELECT NBO_NAME FROM NBOs where NBO_ID='" + nbo + "'";
		String nboName = "";
		try {
//			CachedRowSet rs = wmsDbManager.doSelect(sql);
			  CachedRowSet rs = null;

			if (rs.next()) {
				nboName = rs.getString("NBO_NAME");

			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return nboName;
	}
	
//	public String getPlanDescriptionByIdFromXML_old(String planId) {
////		ResourceBundle rb = ResourceBundle.getBundle("com.ph.sunlife.component.properties.Ingenium");
//		DocumentBuilder docBuilder = null;
//		Document doc = null;
//		DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory
//				.newInstance();
//		docBuilderFactory.setIgnoringElementContentWhitespace(true);
//		Text text=null;
//		File sourceFile = new File(rb.getString("XMLPlan"));
//		try {
//			doc = docBuilder.parse(sourceFile);
//
//
//			Element name = doc.getElementById(planId);
//	        if(name == null) {
//	            System.out.println("There is no element with the ID "
//	                    + planId);
//	        } else {
//	            text = (Text)name.getFirstChild();
//
//	        }
//
//		} catch (SAXException e) {
//			System.out.println("Wrong XML file structure: " + e.getMessage());
//			return null;
//		} catch (IOException e) {
//			System.out.println("Could not read source file: " + e.getMessage());
//		}
//
//		return text.getData();
//
//	}
	
	
	public String getPlanDescriptionByIdFromXML(String planId) throws Exception {
//		ResourceBundle rb = ResourceBundle.getBundle("com.ph.sunlife.component.properties.Ingenium");
		StreamSource xmlSource = new StreamSource("\"D:\\\\WMS\\\\WMS_Files\\\\PLAN_XML\\\\PLAN.xml\"");
		
        DOMResult domResult = new DOMResult();
 
        TransformerFactory.newInstance()
                          .newTransformer()
                          .transform(xmlSource, domResult);
        Document doc = (Document) domResult.getNode();
        doc.normalize();

        HashMap hm = processElementPlan(doc.getDocumentElement());

        return (String)hm.get(planId);
	}
	

    private static HashMap processElementPlan(Element elem)
        throws Exception
    {
    	HashMap hm = new HashMap();
    	String id = "";
    	String value = "";
        NodeList children = elem.getChildNodes();
        for (int ii = 0 ; ii < children.getLength() ; ii++)
        {
            Node child = children.item(ii);
            if (child instanceof Element){
            	//System.out.println(child.getAttributes().item(0));
            	if(child.hasAttributes()){
            		id = child.getAttributes().item(0).getNodeValue();
            		//System.out.println("Id: "+child.getAttributes().item(0).getNodeValue());
            		value = processElementPlanText((Element)child);
            		hm.put(id, value);
            	}
            }
        }
        return hm;
    }
    
    private static String processElementPlanText(Element elem)
    throws Exception
    {
    	String value = "";
    	NodeList children = elem.getChildNodes();
        for (int ii = 0 ; ii < children.getLength() ; ii++)
        {
            Node child = children.item(ii);
            if (child instanceof Text)
            {
            	if(!child.getNodeValue().trim().equals("")){
            		value = child.getNodeValue();
            	}
            }
        }
        return value;
	}
}
