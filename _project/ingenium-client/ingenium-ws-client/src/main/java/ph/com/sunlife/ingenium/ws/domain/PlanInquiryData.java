package ph.com.sunlife.ingenium.ws.domain;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import org.apache.commons.lang3.StringUtils;
import ph.com.sunlife.ingenium.domain.MirCommonFields;
import ph.com.sunlife.ingenium.domain.MirOtherFields;

@XStreamAlias("PlanInquiryData")
public class PlanInquiryData {

    @XStreamAlias("MirCommonFields")
    private MirCommonFields mirCommonFields;
    @XStreamAlias("MirOtherFields")
    private MirOtherFields mirOtherFields;

    public PlanInquiryData() {
        mirCommonFields = new MirCommonFields();
    }

    public PlanInquiryData(final MirCommonFields commonFields) {
        mirCommonFields = commonFields;
    }

    public MirCommonFields getMirCommonFields() {
        return mirCommonFields;
    }

    public PlanInquiryData setMirCommonFields(MirCommonFields commonFields) {
        if (commonFields != null) {
            mirCommonFields = commonFields;
        }
        return this;
    }

    public String getPlanID() {
        return mirCommonFields.getMirPlanId();
    }

    public PlanInquiryData setPlanID(final String planId) {
        if (StringUtils.isNotBlank(planId)) {
            mirCommonFields.setMirPlanId(planId);
        }
        return this;
    }

    public MirOtherFields getMirOtherFields() {
        return mirOtherFields;
    }

    public void setMirOtherFields(MirOtherFields mirOtherFields) {
        this.mirOtherFields = mirOtherFields;
    }
}
