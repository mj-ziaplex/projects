package ph.com.sunlife.ingenium.domain.response.payload;

import com.thoughtworks.xstream.annotations.XStreamImplicit;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

public class MirClientEmployerLocationCodeGroup {

    @XStreamImplicit(itemFieldName = "MirCliEmplrLocCdT")
    private List<String> mirClientEmployerLocationCodeTexts;

    public List<String> getMirClientEmployerLocationCodeTexts() {
        return mirClientEmployerLocationCodeTexts;
    }

    public void setMirClientEmployerLocationCodeTexts(List<String> locationCodes) {
        mirClientEmployerLocationCodeTexts = locationCodes;
    }
}
