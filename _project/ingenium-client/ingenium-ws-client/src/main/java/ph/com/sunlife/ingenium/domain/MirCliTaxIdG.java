package ph.com.sunlife.ingenium.domain;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

@XStreamAlias("MirCliTaxIdG")
public class MirCliTaxIdG {

    @XStreamImplicit(itemFieldName = "MirCliTaxIdT")
    private List<String> mirCliTaxIdT;

    public List<String> getMirCliTaxIdT() {
        return mirCliTaxIdT;
    }

    public void setMirCliTaxIdT(List<String> mirCliTaxIdT) {
        this.mirCliTaxIdT = mirCliTaxIdT;
    }
}
