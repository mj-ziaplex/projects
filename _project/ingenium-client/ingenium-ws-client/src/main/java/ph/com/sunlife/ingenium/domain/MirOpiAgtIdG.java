package ph.com.sunlife.ingenium.domain;

import java.util.ArrayList;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("MirOpiAgtIdG")
public class MirOpiAgtIdG {

	@XStreamImplicit(itemFieldName = "MirOpiAgtIdT")
	protected List<String> mirOpiAgtIdT;

	public List<String> getMirOpiAgtIdT() {
		if (mirOpiAgtIdT == null) {
			mirOpiAgtIdT = new ArrayList<String>();
		}
		return this.mirOpiAgtIdT;
	}

}
