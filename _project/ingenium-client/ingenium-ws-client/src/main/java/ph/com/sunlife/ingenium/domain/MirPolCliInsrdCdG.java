package ph.com.sunlife.ingenium.domain;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

@XStreamAlias("MirPolCliInsrdCdG")
public class MirPolCliInsrdCdG {

    @XStreamImplicit(itemFieldName = "MirPolCliInsrdCdT")
    private List<String> mirPolCliInsrdCdT;

    public List<String> getMirPolCliInsrdCdT() {
        return mirPolCliInsrdCdT;
    }

    public void setMirPolCliInsrdCdT(List<String> mirPolCliInsrdCdT) {
        this.mirPolCliInsrdCdT = mirPolCliInsrdCdT;
    }
}
