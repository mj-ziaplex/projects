package ph.com.sunlife.ingenium.ws.domain;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("TXLife")
public class PolicyInquiryResponse {

    @XStreamAlias("UserAuthResponse")
    private ResponseMetadata metadata;
    @XStreamAlias("TXLifeResponse")
    private PolicyInquiryTransactionResponse transaction;

    public ResponseMetadata getMetadata() {
        return metadata;
    }

    public void setMetadata(ResponseMetadata metadata) {
        this.metadata = metadata;
    }

    public PolicyInquiryTransactionResponse getTransaction() {
        return transaction;
    }

    public void setTransaction(PolicyInquiryTransactionResponse transaction) {
        this.transaction = transaction;
    }
}
