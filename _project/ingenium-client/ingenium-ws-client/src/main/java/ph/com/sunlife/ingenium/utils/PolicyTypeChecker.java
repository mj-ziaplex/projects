package ph.com.sunlife.ingenium.utils;

public class PolicyTypeChecker {

  public static final boolean isTraditional(final String policyId) {
    return "05".equals(policyId.substring(0, 2)) ||
           "25".equals(policyId.substring(0, 2));
  }

  public static final boolean isVUL(final String policyId) {
    return "08".equals(policyId.substring(0, 2)) ||
           "28".equals(policyId.substring(0, 2));
  }

  private static PolicyTypeChecker instance = new PolicyTypeChecker();
  private PolicyTypeChecker() {}
  public static PolicyTypeChecker getInstance() { return instance; }
}
