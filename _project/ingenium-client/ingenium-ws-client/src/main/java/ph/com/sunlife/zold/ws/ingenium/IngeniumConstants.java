package ph.com.sunlife.zold.ws.ingenium;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

/**
 * @author pm13
 */
public class IngeniumConstants {
  /**
   * List of all the constants that will be used Sorted alphabetically
   */
  public static final String ADDRESS_1 = "MirCliAddrLn1TxtT";

  public static final String ADDRESS_2 = "MirCliAddrLn2TxtT";

  public static final String ADDRESS_3 = "MirCliAddrLn3TxtT";

  public static final String ADDRESS_4 = "MirDvCliAddrLn4TxtT";

  public static final String ADDRESS_5 = "MirDvCliAddrLn5TxtT";

  public static final String ADDRESS_6 = "MirCliAddrLn6TxtT";

  public static final String AGENT_CODE = "MirServAgtId";

  public static final String SERVC_AGENT_CLIENT_CODE = "MirServAgtCliId";

  public static final String BRANCH = "MirServAgtBrNm";

  public static final String ADDRESS_3_PREVIOUS = "MirDvCliAddrLn3TxtT";

  public static final String AGENT_NAME = "MirDvAgtCliNmT";

  public static final String SERVICING_AGENT_NAME = "MirDvServAgtCliNm";

  public static final String CLIENT_BDAY = "MirCliBthDt";

  public static final String BENEFICIARY_NAME = "MirBnfyNmT";

  public static final String CITY = "MirCliCityNmTxtT";

  public static final String CITY_PREVIOUS = "MirDvCliCityNmTxtT";

  public static final String CLIENT_FIRSTNAME = "MirEntrGivNmT";

  public static final String CLIENT_LASTNAME = "MirEntrSurNmT";

  public static final String CLIENT_MIDDLENAME = "MirCliIndvMidNmT";

  public static final String CLIENT_NO = "MirCliIdT";

  public static final String CLIENT_FLAG_INDICIA = "MirCliFlgTypCd";

  public static final String CLIENT_FLAG_PERSON = "MirCliPersnInd";

  public static final String CLIENT_NUMBER = "MirCliId";

  public static final String COUNTRY_CODE = "MirCliCtryCdT";

  public static final String COUNTRY_CODE_PREVIOUS = "MirDvCliCtryCdT";

  public static final String COVERAGE_NUMBER = "MirCvgNum";

  public static final String COVERAGE_NUMBER_T = "MirCvgNumT";

  public static final String FACE_AMOUNT = "MirCvgFaceAmt";

  public static final String INDEX_NUMBER = "MirIndexNum";

  public static final String INDEX_NUMBERS = "MirIndexNumT";

  public static final String INSURED_CLIENT_NO = "MirInsrdCliIdT";

  public static final String INSURED_NAME = "MirDvInsrdCliNmT";

  public static final String MINOR_CI_CLAIM_STATUS = "MirCiClmStatCd";

  public static final String ORIGINAL_FACE_AMOUNT = "MirCvgOrigFaceAmtT";

  public static final String CVG_FACE_AMOUNT = "MirCvgFaceAmtT";

  public static final String OWNER_TITLE = "MirCliIndvTitlTxtT";

  public static final String PLAN_ID = "MirPlanId";

  public static final String PLAN_ID_T = "MirPlanIdT";

  public static final String PROVINCE_CODE = "MirCliCrntLocCdT";

  public static final String PROVINCE_PREVIOUS = "MirDvCliCrntLocCdT";

  public static final String REFUND_AMOUNT = "MirPolOsDisbAmt";

  public static final String REQUIREMENT_CODE = "MirReqirIdT";

  public static final String REQUIREMENT_ID = "MirReqirId";

  public static final String RETURN_CODE = "MirRetrnCd";

  public static final String LSIR_RETURN_CODE = "LsirReturnCd";

  public static final String SEQUENCE_NUMBER = "MirCpreqSeqNum";

  public static final String SEQUENCE_NUMBERS = "MirCpreqSeqNumT";

  public static final String STAT_CODE = "MirCpreqStatCd";

  public static final String STAT_CODES = "MirCpreqStatCdT";

  public static final String USER_MESSAGE = "MirUserMsgTxtT";

  public static final String ZIPCODE = "MirCliPstlCdT";

  public static final String ZIPCODE_PREVIOUS = "MirDvCliPstlCdT";

  public static final String SEX = "MirCliSexCd";

  public static final String SEX_ = "SEX_";

  public static final String RECEIVE_DATE = "MirCvgAppRecvDtT";

  public static final String APPLICATION_STATUS = "MirAppStatCd";

  public static final String COVERAGE_COUNT = "MirPolCvgRecCtr";

  public static final String BENEFIT_CODE = "MirBnfyTypCdT";

  public static final String POLICY_STATUS_DESC = "MirPolCstatDesc";

  public static final String COVERAGE_PLAN_DESC = "MirCvgPlanDescT";

  public static final String CURRENCY_CODE = "MirPolCrcyCd";

  public static final String PREMIUM_SUSPENSE = "MirPolPremSuspAmt";

  public static final String MODE_PREMIUM = "MirPolMpremAmt";

  public static final String MISC_SUSPENSE_AMOUNT = "MirPolMiscSuspAmt";

  public static final String COMPANY_NAME = "MirCliCoEntrNmT";

  public static final String OWNER_ID = "MirCliId";

  public static final String INSURED_ID = "MirInsrdCliIdT";

  public static final String INS_TYPE_CODE = "MirPolInsTypCd";

  public static final String CLEAR_CASE_RESP = "MirCcasMsgRespTxtT";

  public static final String CLIENT_INDV_SURNAME = "MirCliIndvSurNm";

  public static final String MESSAGE_TEXT = "MirMsgTxtT";

  public static final String POLICY_BASE = "MirPolIdBase";

  public static final String PREVIOUS_UPDATE_DATE = "MirDvPrevUpdtDt";

  public static final String MISS_INFO_IND = "MirMissInfoIndT";

  public static final String SEVERITY = "MirUserMsgSevrtyT";

  public static final String CLRCASE_SEQUENCE_NUMBER = "MirCcasMsgSeqNumT";

  public static final String RESULT_CODE = "ResultCode";

  public static final String TC = "tc";

  public static final String COVERAGE_DECISION = "MirCvgUwgdecnCd";

  public static final String APP_SIGN_DATE = "MirPolAppSignDt";

  public static final String BRANCH_CODE = "MirServBrId";

  public static final String POLICY_STATUS = "MirPolCstatCd";

  /*
   * Start Added by Rommel 'Hackmel' Suarez December 23 2008
   *
   */


  public static final String HI_OWNER_ID = "MirHiOwnCliId";

  public static final String OWNER_NAME = "MirHiDvOwnCliNm";

  public static final String OWNER_BIRTH_DATE = "MirHiDvOwnCliBthDt";

  public static final String HI_INSURED_ID = "MirHiInsrdCliId";

  public static final String HI_INSURED_NAME = "MirHiDvInsrdCliNm";

  public static final String INSURED_BIRTH_DATE = "MirHiDvInsrdCliBthDt";

  public static final String CURRENT_POLICY_STATUS = "MirHiPolCstatCd";

  public static final String PRODUCT_CODE = "MirHiPlanId";

  public static final String BILLING_TYPE_CODE = "MirHiPolBillTypCd";

  public static final String POLICY_ISSUE_DATE = "MirHiPolIssEffDt";

  public static final String POLICY_PAID_TO_DATE = "MirHiPolPdToDtNum";

  public static final String AMOUNT_BILLED = "MirHiPolTotBillAmt";

  public static final String PREMIUM_MODE = "MirHiPolBillModeCd";

  public static final String SUNDRY_AMOUNT = "MirHiPolSndryAmt";

  public static final String TRUE_PREMIUM = "MirHiPolTpremAmt";

  public static final String LAST_MODE_PREMIUM = "MirHiPolPrevMpremAmt";

  public static final String CURRENCY = "MirHiPolCrcyCd";

  public static final String SUSPENSE_DETAILS_OUTSTANDING_DISBURSEMENTS = "MirPdPolOsDisbAmt";
  public static final String SUSPENSE_DETAILS_MISCELLANEOUS_SUSPENSE = "MirPdPolMiscSuspAmt";


  //MirBiBnfyNmeT
  public static final String BENEFICIARY_INFORMATION_BENEFICIARY_NAME = "MirBiBnfyNmeT";

  public static final String BENEFICIARY_INFORMATION_BENEFICIARY_BIRTHDATE = "MirBiBnfyBrthdtT";

  public static final String BENEFICIARY_INFORMATION_BENEFICIARY_PROCEEDS = "MirBiBnfyPrcedsT";

  public static final String BENEFICIARY_INFORMATION_BENEFICIARY_DESIGNATION = "MirBiBnfyDesgntT";

  public static final String BENEFICIARY_INFORMATION_RELATION_TO_INSURED = "MirBiBnfyRelinsT";

  public static final String BENEFICIARY_INFORMATION_BENEFICIARY_TYPE = "MirBiBnfyTypT";

  public static final String BENEFICIARY_INFORMATION_SEQUENCE_NUMBER = "MirBiSeqNumT";

  public static final String BENEFICIARY_INFORMATION_COVERAGE_NUMBER = "MirBiCvgNumT";


  /**/
  public static final String POLICY_FINANCIAL_ACTIVITY_DEFERRED_ACTIVITY_LIST_FINANCIAL_TRANSACTION_EFFECTIVE_DATE = "MirPolPayoEffDtT";

  public static final String POLICY_FINANCIAL_ACTIVITY_DEFERRED_ACTIVITY_LIST_FINANCIAL_TRANSACTION_TYPE = "MirPolPayoTypCdT";

  public static final String POLICY_FINANCIAL_ACTIVITY_DEFERRED_ACTIVITY_LIST_FINANCIAL_TRANSACTION_GROSS_AMOUNT = "MirDvPolPayoTotAmtT";

  public static final String POLICY_FINANCIAL_ACTIVITY_DEFERRED_ACTIVITY_LIST_FINANCIAL_TRANSACTION_STATUS = "MirPolPayoStatCdT";

  public static final String FUND_TRANSFER_PERCENT_TO_PERCENT_CONFIRMATION_FUND_ID = "MirFndIdT";

  public static final String FUND_TRANSFER_PERCENT_TO_PERCENT_CONFIRMATION_APPROXIMATE_VALUE_IN_FUND_CURRENCY = "MirDvCfnAproxAmtT";

  public static final String FUND_TRANSFER_PERCENT_TO_PERCENT_CONFIRMATION_PERCENTAGE_FROM = "MirFiaOutAllocPctT";

  public static final String FUND_TRANSFER_PERCENT_TO_PERCENT_CONFIRMATION_PERCENTAGE_TO = "MirFiaInAllocPctT";

  public static final String POLICY_ALLOCATION_LIST_ALLOCATION_TYPE_2ND_ROW = "MirCdiTypCdT";

  public static final String POLICY_ALLOCATION_LIST_EFFECTIVE_DATE_2ND_ROW = "MirCdiEffDtT";

  public static final String POLICY_ALLOCATION_LIST_APPLICATION_STATUS_2ND_ROW = "MirCdiStatCdT";

  public static final String POLICY_ALLOCATION_INQUIRY_APPLICATION_STATUS_OF_ALLOCATION_TYPE_SUBSEQUENT_PREMIUM = "MirCdiStatCd";

  public static final String POLICY_ALLOCATION_INQUIRY_ALLOCATION_PERCENTAGE_OF_ALLOCATION_TYPE_SUBSEQUENT_PREMIUM = "MirCdiAllocPctT";

  public static final String POLICY_ALLOCATION_INQUIRY_FUND_DESCRIPTION_OF_ALLOCATION_TYPE_SUBSEQUENT_PREMIUM = "MirDvFndDescTxtT";

  public static final String POLICY_ALLOCATION_INQUIRY_COVERAGE_STATUS_OF_ALLOCATION_TYPE_SUBSEQUENT_PREMIUM = "MirCvgCstatCdT";

  public static final String ASSIGNEE_NAME = "MirDvAsignCliNmT";

  public static final String EFFECTIVE_DATE_OF_ASSIGNMENT = "MirPolCliAsignDtT";
  public static final String COMMENTS_REMARKS = "MirOpiPolComntTxt";


  //public static final String HI_AGENT_CODE="MirAsaServAgtId";
  //public static final String HI_AGENT_NAME="MirAsaDvServAgtCliNm";
  //public static final String AGENT_STATUS="MirAsaAgtStatCd";
  //public static final String NBO="MirAsaServBrId";


  /**/

  public static final String LOAN_INFORMATION_LOAN_AMOUNT = "MirPvDvLoanAmt";


  public static final String LOAN_INFORMATION_BASE_CASH_VALUE = "MirPvDvMcvCsvAmt";

  public static final String LOAN_INFORMATION_MAXIMUM_ADVANCE_AMOUNT_AVAILABLE = "MirPvDvMaxLoanAmt";

  public static final String OTHER_OPTIONS_NON_FORFEITURE_OPTION = "MirOpiPolNfoCd";

  public static final String OTHER_OPTIONS_PREMIUM_OFFSET_OPTION_STATUS_CODE = "MirPoStatCd";

  public static final String BENEFICIARY_INFORMATION_PERCENTAGE = "MirBiBnfyPrcdsPctT";

  public static final String BENEFICIARY_INSTRUCTIONS = "MirBiBnfyInstr";

  public static final String LOAN_INFORMATION_CASH_SURRENDER_VALUE = "MirPvDvPolCsvAmt";

  public static final String DIVIDEND_INFORMATION_POLICY_DIVIDEND_OPTION = "MirPvPolDivOptCd";

  public static final String DIVIDEND_INFORMATION_DIVIDENDS_ON_DEPOSIT = "MirPvPolDodAcumAmt";

  public static final String PUA_DETAILS_MAXIMUM_PUA_CV_WITHDRAWABLE_AMOUNT = "MirDvMaxWthdrwPuaCvAmt";

  public static final String AE_FUND_DETAILS_AE_FUND = "MirAfdPolAeFndAmt";

  public static final String AE_FUND_DETAILS_AE_OPTION = "MirAfdCdiPayoOptCd";

  public static final String DIVIDEND_INFORMATION_YEAR_LAST_DECLARED = "MirPvDvPrevDivYrQty";

  public static final String DIVIDEND_INFORMATION_DIVIDEND_DECLARED = "MirPvDivDclrDurAmt"; // Andre Ceasar Dacanay - Datasnapshot Values

  public static final String DIVIDEND_INFORMATION_MAXIMUM_DOD_WITHDRAWABLE_AMOUNT = "MirDvMaxWthdrwDodAmt";

  public static final String DIVIDEND_INFORMATION_ANNIVERSARY_PROCESSING_YEAR = "MirPvDvAnnvPyrQty";

  public static final String OTHER_OPTIONS_PREMIUM_OFFSET_OPTION = "MirPoOptCd";

  public static final String AGENT_INFORMATION_SERVICE_AGENT_CODE = "MirAsaServAgtId";

  public static final String AGENT_INFORMATION_SERVICE_AGENT_NAME = "MirAsaDvServAgtCliNm";

  public static final String AGENT_INFORMATION_SERVICE_AGENT_STATUS_CODE = "MirAsaAgtStatCd";

  public static final String AGENT_INFORMATION_SERVICE_NBO_CODE = "MirAsaServBrId";

  public static final String AGENT_INFORMATION_COVERAGE_CODE = "MirCdCvgCstatCdT";

  public static final String AGENT_INFORMATION_SERVICE_AGENT_CLIENT_ADDRESS_1 = "MirAsaCliAddrLn1Txt";

  public static final String AGENT_INFORMATION_SERVICE_AGENT_CLIENT_ADDRESS_2 = "MirAsaCliAddrLn2Txt";

  public static final String AGENT_INFORMATION_SERVICE_AGENT_CLIENT_ADDRESS_3 = "MirAsaCliAddrLn3Txt";

  public static final String AGENT_INFORMATION_SERVICE_AGENT_CLIENT_ADDRESS_4 = "MirAsaCliAddrLn4Txt";

  public static final String AGENT_INFORMATION_SERVICE_AGENT_CLIENT_ADDRESS_5 = "MirAsaCliAddrLn5Txt";

  public static final String AGENT_INFORMATION_SERVICE_AGENT_CLIENT_ADDRESS_CHANGED_DATE = "MirAsaCliAddrChngDt";

  public static final String AGENT_INFORMATION_SERVICE_AGENT_BILLING_CLIENT_ADDRESS_NAME = "MirAsaDvLbillCliNm";

  public static final String AGENT_INFORMATION_SERVICE_AGENT_PREVIOUS_AGENT_ID = "MirAsaPrevServAgtId";

  public static final String AGENT_INFORMATION_SERVICE_AGENT_SERVICE_FEE_OPTION = "MirAsaServFeeOpt";

  public static final String AGENT_INFORMATION_SERVICE_AGENT_ASSIGNMENT_DATE = "MirAsaServAgtAsignDt";

  public static final String AGENT_INFORMATION_SERVICE_AGENT_ENCODE_DATE = "MirAsaServAgtEncodeDt";


  public static final String LAPSE_INFORMATION_SHORTAGE_AMOUNT = "MirOpiPolUlShrtAmt";

  public static final String LAPSE_INFORMATION_REACTIVE_POLICY_AMOUNT = "MirOpiUlLapsNotiAmt";

  public static final String LAPSE_INFORMATION_START_DATE = "MirOpiUlLapsStrtDt";

  public static final String LAPSE_INFORMATION_COI_REINSTATEMENT_DATE = "MirOpiCoiReinAmt";

  public static final String LAPSE_INFORMATION_APP_REINSTATEMENT_DATE = "MirOpiPolAppReinstDt";


  public static final String COVERAGE_PREMIUM_AMOUNTS = "MirCdCvgMpremAmtT";

  public static final String COVERAGE_ISSUE_EFFECTIVE_DATES = "MirCdCvgIssEffDtT";

  public static final String COVERAGE_SMOKER_CODES = "MirCdCvgSmkrCdT";

  public static final String COVERAGE_SEX_CODES = "MirCdCvgSexCdT";

  public static final String COVERAGE_CD_RT_AGES = "MirCdCvgRtAgeT";

  public static final String COVERAGE_PAR_CODES = "MirCdCvgParCdT";

  public static final String COVERAGE_MAT_EXPIRY_DATES = "MirCdCvgMatXpryDtT";

  public static final String COVERAGE_NUMBERS = "MirCdCvgNumT";

  public static final String COVERAGE_FLAT_EXTRA_PREMIUM_AMOUNTS = "MirRtCvgFePremAmtT";

  public static final String COVERAGE_MORTALITY_EXTRA_PREMIUM_AMOUNTS = "MirRtCvgMePremAmtT";

  public static final String COVERAGE_FLAT_EXTRA_U_PREMIUM_AMOUNTS = "MirRtCvgFeUpremAmtT";

  public static final String COVERAGE_FLAT_EXTRA_DURS = "MirRtCvgFeDurT";

  public static final String COVERAGE_FLAT_EXTRA_PERMANENT_U_PREMIUM_AMOUNTS = "MirRtCvgFePermUpremAmtT";

  public static final String COVERAGE_MORTALITY_EXTRA_DURS = "MirRtCvgMeDurT";

  public static final String COVERAGE_MORTALITY_EXTRA_FCTS = "MirRtCvgMeFctT";

  public static final String COVERAGE_RT_RT_AGES = "MirRtCvgRtAgeT";

  public static final String COVERAGE_FLAT_EXTRA_REASON_CODES = "MirRtCvgFeReasnCdT";

  public static final String COVERAGE_MORTALITY_EXTRA_REASON_CODES = "MirRtCvgMeReasnCdT";

  public static final String COVERAGE_MORTALITY_EXTRA_RATING_CODES = "MirRtCvgMeRatCdT";

  public static final String COVERAGE_RESTRICTED_AUTOMATIC_PREMIUM = "MirCvgRestrPremInd";

  public static final String COVERAGE_BASIC_PREMIUM_AMOUNT = "MirCvgBasicPremAmt";

  public static final String COVERAGE_PARTICIPATION_CODE = "MirCvgParCd";

  public static final String COVERAGE_POLICY_FEE = "MirCvgPfeeAmt";


  public static final String COVERAGE_RT_NUMBERS = "MirRtCvgNumT";

  public static final String COVERAGE_PLANS = "MirCdPlanIdT";

  public static final String COVERAGE_FACE_AMOUNTS = "MirCdCvgFaceAmtT";

  public static final String COVERAGE_MODE_PREMIUM_AMOUNTS = "MirCdCvgMpremAmtT";

  public static final String COVERAGE_ISSUE_DATES = "MirCdCvgIssEffDtT";

  public static final String COVERAGE_PARTICIPATION_CODES = "MirCdCvgParCdT";

  public static final String COVERAGE_STATUS_CODES = "MirCdCvgCstatCdT";

  public static final String COVERAGE_MATURITY_EXPIRY_DATES = "MirCdCvgMatXpryDtT";


  public static final String POLICY_VALUES_APP_FUND_AVERAGE_BALANCE = "MirPolPdfAvbAmt";

  public static final String POLICY_VALUES_INTEREST_RATE = "MirPvLoanIntPct";

  public static final String POLICY_VALUES_APP_FUND = "MirDirBillPdfAmt";

  public static final String POLICY_VALUES_APP_FUND_ACCUMULATED_INTEREST = "MirPolPdfIntAmt";

  public static final String POLICY_VALUES_PO_EFFECTIVE_DATE = "MirPoEffDt";

  public static final String POLICY_VALUES_APP_FUND_INTEREST_RATE = "MirPolPdfIntRt";

  public static final String POLICY_VALUES_DIVIDEND_INTEREST_RATE = "MirPvPolDodIntRt";

  public static final String POLICY_VALUES_PREVIOUS_DIVIDEND_ACCUMULATION = "MirPrevLtdDodAmt";

  public static final String POLICY_VALUES_AE_FUND_INTEREST = "MirAfdPolAeFndIntAmt";

  public static final String POLICY_VALUES_LAST_WITHDRAWAL_DATE = "MirAfdPolAeLstWthdrDt";

  public static final String POLICY_VALUES_LAST_WITHDRAWAL_AMOUNT = "MirAfdPolAeLstWthdrAmt";

  public static final String POLICY_VALUES_LOAN_BALANCE = "MirAfdDvLoanBal";


  public static final String POLICY_VALUES_PREVIOUS_TOTAL_PAID_UP_ADDITIONS = "MirPrevLtdPuaAmt";

  public static final String POLICY_VALUES_MAXIMUM_PUA_FA_WITHDRAWALBLE_AMOUNT = "MirDvMaxWthdrwPuaFaAmt";

  //ListBill Relationship

  public static final String LIST_BILL_CLIENT_ID = "MirObiDvLBillCliId"; //MIR-OBI-DV-LBILL-CLI-ID

  public static final String LIST_BILL_CLIENT_NAME = "MirObiDvLBillCliNm"; //MIR-OBI-DV-LBILL-CLI-NM

  public static final String LIST_BILL_ADDRESS_CODE = "MirObiDvLBillAddrCd"; //MIR-OBI-DV-LBILL-ADDR-CD

  public static final String SMOKER_CODE = "MirCdCvgSmkrCdT";

  public static final String POLICY_INFORMATION_PAID_UP_ADD_CASH = "MirPvPuaLtdFaceAmt";

  public static final String POLICY_VALUES_INFORMATION_PAID_UP_ADD_CASH = "MirDvValuPuaAmt";

  public static final String POLICY_INFORMATION_ASSIGNEE = "MirDvAsignCliNmT";

  /*
   * Modified by VSantos
   * Additional list bill info
   * 01/29/2014
   */

  public static final String LIST_BILL_TYPE_CODE = "MirObiDvLbillTypCd";
  public static final String LIST_BILL_FORM_CODE = "MirObiDvLbillFrmCd";
	
	
	/*	Employee Information - WorkSite Additional Section
	 *  ClientInquiry.xml
		Vsantos
		12/12/2013
	*/

  //start -vsantos
  public static final String LIST_BILL_EMPLOYEE_ACCOUNT_NUMBER = "MirLbillEmplAcctNum";//Employee No/Account No: MIR-LBILL-EMPL-ACCT-NUM

  public static final String LIST_BILL_EMPLOYEE_RANK = "MirLbillEmplRankCd";//Rank/Position: MIR-LBILL-EMPL-RANK-CD

  public static final String LIST_BILL_EMPLOYEE_BRANCH = "MirLbillEmplBrchCd"; //Branch/Dep: MIR-LBILL-EMPL-BRCH-CD

  public static final String LIST_BILL_REGION = "MirLbillRgnCd"; //Region: MIR-LBILL-RGN-CD

  public static final String LIST_BILL_DIVISION = "MirLbillDivnCd";//Division: MIR-LBILL-DIVN-CD

  public static final String LIST_BILL_STATION = "MirLbillStatnCd";//Station: MIR-LBILL-STATN-CD

  public static final String LIST_BILL_UNIT = "MirLbillUnitCd";//Unit: MIR-LBILL-UNIT-CD
  //end - vsantos


  /* Gross Premium Amount
   * Policy Inquiry - all details
   * GFrag
   * 8/12/2014
   */

  //start -GFrag
  public static final String GROSS_PREMIUM_AMOUNT = "MirPolGrsApremAmt";
  //end -GFraga


  public static final String NBO_NAME = "NBO_NAME";

  public static final String PRODUCT_NAME = "PRODUCT_NAME";

  public static final String BILLING_TYPE = "BILLING_TYPE";

  public static final String PREMIUM_MODE_DESC = "PREMIUM_MODE_DESC";

  public static final String MINOR_CI_CLAIM_STATUS_DESC = "MINOR_CI_CLAIM_STATUS_DESC";

  public static final String AGENT_INFORMATION_SERVICE_AGENT_STATUS_DESC = "AGENT_INFORMATION_SERVICE_AGENT_STATUS_DESC";

  public static final String CURRENT_POLICY_STATUS_DESC = "CURRENT_POLICY_STATUS_DESC";

  public static final String PREMIUM_OFFSET_OPTION_STATUS_DESC = "PREMIUM_OFFSET_OPTION_STATUS_DESC";


  public static final String BENEFICIARY_TYPE_DESC = "BENEFICIARY_TYPE_DESC";
  public static final String BENEFICIARY_DESIGNATION_DESC = "BENEFICIARY_DESIGNATION_DESC";
  public static final String BENEFICIARY_PROCEEDS_DESC = "BENEFICIARY_PROCEEDS_DESC";

  public static final String REINSURANCE_INDICATOR = "MirOpiPolReinsCd";
  public static final String REINSURANCE_FACE_AMOUNT = "MirOpiPolReinsFaceAmt";
  public static final String REINSURANCE_ADB_FACE_AMOUNT = "MirOpiPolReinsAdbFaceAmt";

  public static final String POLICY_FINANCIAL_ACTIVITY_DEFERRED_ACTIVITY_LIST_FINANCIAL_TRANSACTION_TYPE_ = "POLICY_FINANCIAL_ACTIVITY_DEFERRED_ACTIVITY_LIST_FINANCIAL_TRANSACTION_TYPE_";
  public static final String POLICY_FINANCIAL_ACTIVITY_DEFERRED_ACTIVITY_LIST_FINANCIAL_TRANSACTION_STATUS_ = "POLICY_FINANCIAL_ACTIVITY_DEFERRED_ACTIVITY_LIST_FINANCIAL_TRANSACTION_STATUS_";

  public static final String POLICY_STATUS_ = "POLICY_STATUS_";
  public static final String PREMIUM_OFFSET_OPTION_STATUS_ = "PREMIUM_OFFSET_OPTION_STATUS_";

  public static final String PREMIUM_OFFSET_OPTION_ = "PREMIUM_OFFSET_OPTION_"; // WMSStepProcessor_Module - Andre Ceasar Dacanay
  public static final String NON_FORFEITURE_OPTION_ = "NON.FORFEITURE.OPTION_"; // WMSStepProcessor_Module - Andre Ceasar Dacanay
  public static final String POLICY_DIVIDEND_OPTION_ = "POLICY.DIVIDEND.OPTION_"; // WMSStepProcessor_Module - Andre Ceasar Dacanay
  public static final String AE_OPTION_ = "AE_OPTION_"; // WMSStepProcessor_Module - Andre Ceasar Dacanay


  public static final String SYSTEM_DATE = "MirHiSystemDt";


  public static final String AGENT_STATUS_ = "AGENT_STATUS_";
  public static final String PREMIUM_MODE_ = "PREMIUM_MODE_";
  public static final String BILLING_TYPE_ = "BILLING_TYPE_";
  public static final String MINOR_CI_CLAIM_STATUS_ = "MINOR_CI_CLAIM_STATUS_";
  public static final String BENEFICIARY_TYPE_ = "BENEFICIARY_TYPE_";
  public static final String BENEFICIARY_DESIGNATION_ = "BENEFICIARY_DESIGNATION_";
  public static final String BENEFICIARY_PROCEEDS_ = "BENEFICIARY_PROCEEDS_";

  public static final String SMOKER_CODE_ = "SMOKER_CODE_";
  public static final String COVERAGE_PAR_CODES_ = "COVERAGE_PAR_CODES_";
  public static final String AGENT_INFORMATION_COVERAGE_CODE_ = "AGENT_INFORMATION_COVERAGE_CODE_";
  public static final String COVERAGE_DETAILS_SEX_CODE_ = "COVERAGE_DETAILS_SEX_CODE_";

  public static final String OTHER_OPTIONS_PREMIUM_OFFSET_OPTION_DESC = "OTHER_OPTIONS_PREMIUM_OFFSET_OPTION_DESC";


  public static final String DIVIDEND_INFORMATION_POLICY_DIVIDEND_OPTION_DESC = "DIVIDEND_INFORMATION_POLICY_DIVIDEND_OPTION_DESC";
  public static final String OTHER_OPTIONS_NON_FORFEITURE_OPTION_DESC = "OTHER_OPTIONS_NON_FORFEITURE_OPTION_DESC";
  public static final String AE_FUND_DETAILS_AE_OPTION_DESC = "AE_FUND_DETAILS_AE_OPTION_DESC";

  public static final String OTHER_LEGAL_FIRST_NAME = "MirCliOtherFirstNmT";
  public static final String OTHER_LEGAL_MID_NAME = "MirCliOtherMidNmT";
  public static final String OTHER_LEGAL_LAST_NAME = "MirCliOtherLastNmT";

  //MirCliOtherFirstNmT
  //MirCliOtherMidNmT
  //MirCliOtherLastNmT
  //MirPdPolMiscSuspAmt

  /* this section is for Elements retrieved from the Ingenium Ref Table */

  //public static final String PRODUCT_NAME = "ING_Product_Name";

  /*
   * End Added by Rommel 'Hackmel' Suarez December 23 2008
   *
   */


  public static final String POLICY_INFORMATION_PREMIUM_REFUND = "MirDvPremRfndAmt";

  public static final String POLICY_INFORMATION_PREMIUM_SUSPENSE = "MirPdPolPremSuspAmt";

  public static final String POLICY_INFORMATION_1 = "MirPvPuaYtdFaceAmt";

  public static final String POLICY_INFORMATION_2 = "MirPdPolPdfSuspAmt";

  public static final String APP_FUND_AMT = "MirPolPdfAmt";
  public static final String APP_FUND_SUSP_AMT = "MirPolPdfSuspAmt";

  // Added for policy values by hackmel
  public static final String POLICY_VALUES_CASH_SURRENDER = "MirDvPolCsvAmt";


  // start added by Joseph Neil G Lanzaderas
  public static final String COVERAGE_DETAILS_AGE = "MirCdCvgRtAgeT";
  public static final String COVERAGE_DETAILS_SEX_CODE = "MirCdCvgSexCdT";
  public static final String COVERAGE_DETAILS_SMOKER_CODE = "MirCdCvgSmkrCdT";
  public static final String CUMULATIVE_EXCESS_PREMIUM_LIFE_TO_DATE = "MirExcsPremLifeToDtT";
  public static final String CUMULATIVE_SURRENDER_LIFE_TO_DATE = "MirCvgSurrLtdAmtT";
  public static final String ANNUAL_PREMIUM_FOR_COVERAGE = "MirCvgBasicPremAmtT";
  public static final String AMOUNT_OF_ADVANCE = "MirDvLoanAmt";
  public static final String APA_AMOUNT = "MirDvAplLoanAmt";
  public static final String ACCRUED_INTEREST_ON_ADVANCE = "MirDvLoanIntYtdAmt";
  public static final String ACCRUED_INTEREST_ON_APA = "MirDvAplIntYtdAmt";
  // end added by Joseph Neil G Lanzaderas

  //added by Arcee dela Cruz
  //Policy Inquiry>UL Details
  public static final String MONTHIVERSARY_PAID_TO_DATE = "MIRPOLULPRCESD";
  public static final String SHORTAGE_AMOUNT = "MIRPOLULSHRTAMT";
  public static final String LAPSE_START_DATE = "MIRULLAPSSTRTDT";
  public static final String COI_ON_REINSTATEMENT_DATE = "MIRCOIREINAMT";
  public static final String DEATH_BENEFIT_OPTION = "MIRPOLDBOPTCD";
  public static final String REACTIVATE_POLICY_AMOUNT = "MIRULLAPSNOTIAMT";
  public static final String NO_LAPSE_GUARANTEED_END_DATE = "MIRDVULNLAPSENDDT";
  public static final String SHORTAGE_CALCULATION_TYPE = "MIRSHRTCALCCD";
  public static final String SURRENDER_CHARGES_LIFE_TO_DATE = "MIRSURRLOADLTDAMTT";

  //Policy Inquiry>Inquiry - Billing
  public static final String BILLING_TYPE_BIL = "MIRPREVBILLTYPCD";
  public static final String PREMIUM_MODE_BIL = "MIRPOLPREVMODECD";
  public static final String PAID_TO_DATE = "MIRPREVPDTODTNUM";

  //Policy Inquiry > All Details
  public static final String COMMENTS = "MIRPOLCOMNTTXT";
  public static final String NONFORFEITURE_OPTION = "MirPolNfoCd";
  public static final String CLIENT_ID_AR = "MirDvAsignCliIdT";
  public static final String ADDRESS_TYPE_ = "MirDvAsignAddrCdT";
  public static final String RELATIONSHIP_TYPE = "MirDvAsignSubCdT";
  public static final String CLIENT_ID_OR = "MirCliIdT";
  public static final String OWNER_NAME_OR = "MirDvOwnCliNmT";
  public static final String CLIENT_TAX_ID = "MirCliTaxIdT";
  public static final String OWNER_RELATIONSHIP_TO_INSURED = "MirPolCliInsrdCdT";
  public static final String OWNER_CLIENT_RELATIONSHIP_TYPE = "MirDvOwnSubCdT";
  public static final String CLIENT_ADDRESS_TYPE = "MirCliAddrTypCdT";
  public static final String CLIENT_ID_PR = "MIRDVPAYRCLIID";
  public static final String PAYOR_NAME = "MIRDVPAYRCLINM";
  public static final String RELATIONSHIP_TYPE_PR = "MIRDVPAYRSUBCD";
  public static final String ADDRESS_TYPE = "MIRDVPAYRADDRCD";
  public static final String SERVICING_AGENT_ID = "MIRSERVAGTID";
  public static final String SERVICING_AGENT_ASSIGN_DATE = "MirServAgtAsignDt";
  public static final String SERVICING_AGENT_ENCODE_DATE = "MirServAgtEncodeDt";
  public static final String SERVICING_AGENT_BRANCH = "MIRSERVBRID";
  public static final String SERVICING_FEE_OPTION = "MirServFeeOpt";
  public static final String PREVIOUS_SERVICING_AGENT = "MirPrevServAgtId";

  //Policy Service>Beneficiary List
  public static final String SEQUENCE_NUMBER_BL = "MIRBNFYSEQNUM";
  public static final String BENEFICIARY_TYPE = "MIRBNFYTYPCDT";
  public static final String BENEFICIARY_DESIGNATION = "MIRBNFYDESGNTCDT";
  public static final String BENEFICIARY_PERCENTAGE = "MIRBNFYPRCDSPCTT";
  public static final String RELATION_TO_INSURED_BL = "MIRBNFYRELINSRDCDT";
  public static final String ON_DEATH_OR_ON_ENDOWMENT = "MIRBNFYDTHENDOWCDT";

  //Client>Client Inquiry
  public static final String NAME_EFFECTIVE_DATE = "MirCliIndvEffDtT";
  public static final String TITLE = "MIRCLIINDVTITLTXTT";
  public static final String FIRST_NAME = "MIRENTRGIVNMT";
  public static final String MIDDLE_NAME = "MIRCLIINDVMIDNMT";
  public static final String LAST_NAME = "MIRENTRSURNMT";
  public static final String SUFFIX = "MIRCLIINDVSFXNMT";
  public static final String BIRTH_DATE = "MIRCLIBTHDT";
  public static final String BIRTH_LOCATION = "MirCliBthLocCd";
  public static final String SMOKER = "MirCliSmkrCd";
  public static final String SMOKER_ = "SMOKER_";
  public static final String MARITAL_STATUS = "MirCliMaritStatCd";
  public static final String MARITAL_STATUS_ = "MARITAL_STATUS_";

  public static final String FEDERAL_TAX_CERT = "MIRCLITXEMPCD";
  public static final String RESET_IDENTIFICATION = "MIRCLIPINRESETTXT";
  public static final String SSS_GSIS = "MIRCLISSSGSIS";
  public static final String RELIGION = "MIRCLIRELIGION";
  public static final String COUNTRY_OF_CITIZENSHIP = "MirCliCtznCtryCd";
  public static final String CITIZENSHIP_ = "CITIZENSHIP_";


  public static final String COUNTRY_OF_CITIZENSHIP_ = "COUNTRY_OF_CITIZENSHIP_";

  public static final String GSIS_ID = "MIRCLIGSISID";
  public static final String CITIZENSHIP = "MirCliCtznshipCd";
  public static final String ACR_NUMBER = "MIRCLIACRNUM";
  public static final String UNMATCHED_MAIL = "MIRUNMTCHMAILIND";
  public static final String PROOF_OF_AGE_SUBMITTED = "MIRCLIAGEPROOFIND";
  public static final String CLIENT_AS_CHARITY = "MIRCLICHRTYIND";
  public static final String CONFIDENTIAL_ACCESS = "MIRCLICNFDIND";
  public static final String LEGIT_TO_DUP = "MIRCLILEGITDUPIND";
  public static final String DATE_OF_DEATH = "MIRCLIDTHDT";
  public static final String CAUSE_OF_DEATH = "MIRCLIDTHREASNCD";
  public static final String PROOF_OF_DEATH = "MIRCLIDTHPROOFDT";
  public static final String ADDRESS_EFF_DATE = "MIRCLIADDREFFDTT";
  public static final String RESIDENCE_TYPE = "MIRCLIRESTYPCDT";
  public static final String APT_UNIT = "MIRCLIRESNUMT";
  public static final String PROVINCE_STATE = "MIRCLICRNTLOCCD1";
  public static final String COUNTRY = "MIRCLICTRYCD1";
  public static final String ZIP_CODE = "MIRCLIPSTLCD1";
  public static final String MUNICIPALITY = "MIRCLIADDRMUNCDT";
  public static final String COUNTY_PARISH = "MIRCLIADDRCNTYCDT";
  public static final String PREFECTURE = "MIRCLIALTADDRCDT";
  public static final String ADDITIONAL_INFO = "MIRCLIADDRADDLTXTT";
  public static final String CONTACT_INFO = "MIRCLIADDRCNTCTTXTT";
  public static final String YEARS_AT_ADDRESS = "MIRCLIADDRYRDURT";
  public static final String ADDRESS_CHANGED_DATE = "MIRCLIADDRCHNGDTT";
  public static final String ADDRESS_STATUS = "MIRCLIADDRSTATCDT";
  public static final String ADDRESS_STATUS_CHANGED_DATE = "MIRADDRSTATCHNGDTT";
  public static final String PREVIOUS_ADDRESS = "MIRDVPREVADDRINDT";
  public static final String OCCUPATION = "MIROCCPID";
  public static final String OCCUPATION_CLASS = "MIRCLIOCCPCLASCD";
  public static final String INCOME_CLASS = "MIRCLIINCMEARNCD";
  public static final String HOURS_WORKED = "MIRCLIWRKQTY";
  public static final String NATURE_OF_BUSINESS = "MIRCLINATUREOFBUS";
  public static final String PROPOSED_RETIREMENT_DATE = "MIRCLIPRPSRETIRDT";
  public static final String UIC_QUALIFIED = "MIRCLIUICQUALFIND";
  public static final String WORK_AT_HOME = "MIRCLIWRKRESIND";
  public static final String PART_TIME = "MIRCLIWRKPTIND";
  public static final String SELF_EMPLOYED = "MIRCLISELFEMPLIND";
  public static final String EMPLOYER_NAME = "MIRDVEMPLRCLICONMT";
  public static final String YEARS_WITH_EMPLOYER = "MIRCLIEMPLYRDURT";
  public static final String EMPLOYER_ADDRESS_EFF_DATE = "MIRDVEMPLRADDREFFDTT";
  public static final String EMPLOYER_ADDRESS = "MIRDVEMPLRADDR1TXTT";
  public static final String EMPLOYER_PROVINCE_STATE = "MIRCLIEMPLRLOCCD1";
  public static final String EMPLOYER_CITY = "MIRDVEMPLRCITYCDT";
  public static final String EMPLOYER_COUNTRY = "MIRDVEMPLRCTRYCD1";
  public static final String EMPLOYER_POSTAL_CODE = "MIRDVEMPLRPSTLCDT";
  public static final String FIRST_NAME_OLN = "MirCliOtherFirstNmT";
  public static final String MIDDLE_NAME_OLN = "MirCliOtherMidNmT";
  public static final String LAST_NAME_OLN = "MirCliOtherLastNmT";
  public static final String BANK_ACCT_NO = "MIRCLIBNKACCTNUMT";
  public static final String CLIENT_BANK_ACCT_EFF_DATE = "MIRCLIBNKACCTDTT";
  public static final String BANK_NUMBER = "MIRBNKIDT";
  public static final String BRANCH_NUMBER = "MIRBNKBRIDT";
  public static final String BANK_ACCT_ID = "MIRBNKACCTIDT";
  public static final String ACCOUNT_TYPE = "MIRBNKACCTTYPCDT";
  public static final String MICR_ENCODED = "MIRBNKACCTMICRINDT";
  public static final String ACCOUNTHOLDER = "MIRBNKACCTHLDRNMT";
  public static final String CREDIT_CARD = "MIRCLICRCNUMT";
  public static final String CREDIT_CARD_TYPE = "MIRCRCTYPCDT";
  public static final String CC_BANK_NUMBER = "MIRCRCBNKIDT";
  public static final String CVV2 = "MIRCRCCVV2NOT";
  public static final String CC_ACCOUNT_ID = "MIRCRCIDT";
  public static final String CC_ACCOUNT_HOLDER_NAME = "MIRCRCHLDRNMT";
  public static final String CC_EXP_MONTH = "MIRCRCXPRYMOT";
  public static final String CC_EXP_YEAR = "MIRCRCXPRYYRT";
  public static final String MIB_INDICATOR = "MIRCLIMIBINDCD";
  public static final String PERSISTENCY_RATE = "MIRCLIPRSTRT";
  public static final String PREVIOUSLY_DECLINED = "MIRCLIPREVDCLNIND";
  public static final String DECLINED_DATE = "MIRCLIPREVDCLNDT";
  public static final String UW_DECISION = "MIRCLIUWGDECNCD";
  public static final String DECISION_TYPE = "MIRCLIUWGDECNTYPCD";
  public static final String UW_DECISION_CHANGED_DATE = "MIRCLIUWGDECNDT";
  public static final String WORKSHEET_NUMBER = "MIRUWGWRKSHTNUM";
  public static final String FIRST_UW_INITIALS = "MIRUWUSER1ID";
  public static final String SECOND_UW_INITIALS = "MIRUWUSER2ID";
  public static final String CLEAR_CASE_PROCESSING_ATTEMPTED = "MIRCLICMPLTCCASIN";
  public static final String SERVICING_AGENT_ID_CI = "MIRAGTID";
  public static final String SERVICING_AGENT_STATUS = "MIRAGTSTATCD";
  public static final String SERVICING_AGENT_NAME_CI = "MIRDVAGTCLINM";
  public static final String CORRESPONDENCE_LANG = "MIRCLILANGCD";
  public static final String STATEMENT_DATE = "MIRCLISTMTDT";
  public static final String CLASSIFICATION = "MIRCLICLASCD";
  public static final String CLASSIFICATION_CHANGE_DATE = "MIRCLICLASCHNGDT";
  public static final String BEST_TIME_TO_CALL = "MIRBESTTIMECALLTXT";
  public static final String ADDITIONAL_COMMENTS = "MIRCLICOMNTTXT";
  public static final String AMOUNT = "MIRCLISUSPAMT";
  public static final String CURRENCY_SA = "MIRCLISUSPCRCYCD";
  public static final String SUB_COMPANY = "MIRSUSPSBSDRYCOID";
  public static final String CHANGE_DATE = "MIRCLISUSPCHNGDT";
  public static final String PREVIOUS_TITLE = "MIRDVCLIINDVTITLTXTT";
  public static final String PREVIOUS_FIRST_NAME = "MIRDVENTRGIVNMT";
  public static final String PREVIOUS_MIDDLE_NAME = "MIRDVCLIINDVMIDNMT";
  public static final String PREVIOUS_LAST_NAME = "MIRDVENTRSURNMT";
  public static final String PREVIOUS_SUFFIX = "MIRDVCLIINDVSFXNMT";
  public static final String PREVIOUS_NAME_EFF_DATE = "MIRDVCLIINDVEFFDTT";
  public static final String UPDATED_BY = "MIRDVCLIUPDBYLNT";
  public static final String DATE_LAST_UPDATED = "MIRDVCLIUPDDTLN1T";
  public static final String PREVIOUS_APT_UNIT = "MIRDVCLIRESNUMT";
  public static final String PREVIOUS_ADDRESS_1 = "MIRDVCLIADDRLN1TXTT";
  public static final String PREVIOUS_ADDRESS_2 = "MIRDVCLIADDRLN2TXTT";
  public static final String PREVIOUS_ADDRESS_3 = "MIRDVCLIADDRLN3TXTT";
  public static final String PREVIOUS_CITY = "MIRDVCLICITYNMTXTT";
  public static final String PREVIOUS_PROVINCE_STATE = "MIRDVCLICRNTLOCCDT";
  public static final String PREVIOUS_COUNTRY = "MIRDVCLICTRYCDT";
  public static final String PREVIOUS_ZIP_CODE = "MIRDVCLIPSTLCDT";
  public static final String PREVIOUS_ADDRESS_EFF_DATE = "MIRDVCLIADDREFFDTT";
  public static final String PREVIOUS_YEARS_AT_ADDRESS = "MIRDVCLIADDRYRDURT";
  public static final String PREVIOUS_MUNICIPALITY = "MIRDVCLIADDRMUNCDT";
  public static final String PREVIOUS_COUNTY_PARISH = "MIRDVCLIADDRCNTYCDT";
  public static final String PREVIOUS_PREFECTURE = "MIRDVCLIALTADDRCDT";
  public static final String PREVIOUS_ADDITIONAL_INFO = "MIRDVCLIADDRADDLTXTT";
  public static final String PREVIOUS_CONTACT_INFO = "MIRDVCLIADDRCNTCTTXTT";
  public static final String PREVIOUS_UPDATED_BY = "MIRDVCLIADDRUPDBYLNT";
  public static final String PREVIOUS_DATE_LAST_UPDATED = "MIRDVCLIADDRLSTUPDLNT";
  public static final String PREVIOUS_EMPLOYER_NAME = "MIRDVPREVEMPLRNMT";
  public static final String PREVIOUS_YEARS_WITH_EMPLOYER = "MIRPREVEMPLYRDURT";

  public static final String BENEFICIARY_INFORMATION_BENEFICIARY_UPDATED_DATE = "MirBiLstUpdtDt";
  public static final String BENEFICIARY_INFORMATION_BENEFICIARY_DESIGNATION_DESC = "BENEFICIARY_INFORMATION_BENEFICIARY_DESIGNATION_DESC";
  public static final String RELATION_TO_INSURED_BL_DESC = "RELATION_TO_INSURED_BL_DESC";
  //start gfrag - fatca
  public static final String COUNTRY_OF_CITIZENSHIP_1 = "MirCliCtznCtry1Cd";
  public static final String COUNTRY_OF_CITIZENSHIP_2 = "MirCliCtznCtry2Cd";
  public static final String COUNTRY_OF_LEGAL_RESIDENCE_1 = "MirCliResCtry1Cd";
  public static final String COUNTRY_OF_LEGAL_RESIDENCE_2 = "MirCliResCtry2Cd";
  public static final String COUNTRY_OF_LEGAL_RESIDENCE_3 = "MirCliResCtry3Cd";
  public static final String ADDRESS_START_DATE = "MirAddrEffDtNumT";
  public static final String ADDRESS_END_DATE = "MirAddrEndDtNumT";
  public static final String CLIENT_ADDRESS_22 = "MirCliAddrLn22TxtT";
  public static final String CLIENT_ADDRESS_23 = "MirCliAddrLn23TxtT";
  public static final String COUNTRY_OF_INCORPORATION = "MirBusRegCtryCd";
  public static final String US_INDICIA_DATE = "MirCliFlgUpdtDt";
  public static final String US_PERSON_DATE = "MirCliPersnUpdtDt";
  public static final String ID_PRESENTED_OR_IRS_FORM = "MirCliIdFormCdT";
  public static final String REFERENCE_NO = "MirCliIdRefNumT";
  public static final String EXPIRY_DATE = "MirCliIdXpryDtT";
  public static final String VALIDITY_DATE = "MirCliIdEffDtT";

  public static final String CONTACT_TYPE = "MirCliCntctIdCdT";
  public static final String COUNTRY_CALLING_CODE = "MirCliCntctCtryCdT";
  public static final String AREA_CODE = "MirCliCntctAreaCdT";
  public static final String CONTACT_DETAIL = "MirCliCntctIdTxtT";
  public static final String POSTAL_CODE = "MirCliPstl2CdT";

  public static final String FATCA_DATE = "MirFatcaFormDt";
  public static final String FATCA_STATUS = "MirFatcaFormStatCd";
  public static final String FATCA_REMARKS = "MirFatcaFormRsnTxt";
  public static final String FATCA_REASON = "MirFatcaFormRsnCd";
  public static final String FATCA_COUNTRY = "MirCliCtry2CdT";


  //end gfrag - fatca
  //end

  //Cession_Module - Andre Ceasar Dacanay - start
  public static final String POLICY_CONSOLDIDATED_PLAN_ID = "MirCdPlanIdT";
  public static final String POLICY_CONSOLDIDATED_COVERAGE_FACE_AMT = "MirCdCvgFaceAmtT";
  //Cession_Module - Andre Ceasar Dacanay - end

  // Inquiry - Consolidated Information
  public static final String ICI_ADDRESS_CHANGE_DATE = "MirAsaCliAddrChngDt";

  //fields that have codes instead of values
  public static final String OWNER_RELATIONSHIP_TO_INSURED_ = "OWNER_RELATIONSHIP_TO_INSURED_";
  public static final String OWNER_CLIENT_RELATIONSHIP_TYPE_ = "OWNER_CLIENT_RELATIONSHIP_TYPE_";
  public static final String CLIENT_ADDRESS_TYPE_ = "CLIENT_ADDRESS_TYPE_";
  public static final String SERVICING_FEE_OPTION_ = "SERVICING_FEE_OPTION_";
  public static final String RELATION_TO_INSURED_BL_ = "RELATION_TO_INSURED_BL_";
  public static final String ADDRESS_TYPE__ = "ADDRESS_TYPE__";
  public static final String RELATIONSHIP_TYPE_ = "RELATIONSHIP_TYPE_";

  //field description

  public static final String OWNER_RELATIONSHIP_TO_INSURED_DESC = "OWNER_RELATIONSHIP_TO_INSURED_DESC";
  public static final String OWNER_CLIENT_RELATIONSHIP_TYPE_DESC = "OWNER_CLIENT_RELATIONSHIP_TYPE_DESC";
  public static final String CLIENT_ADDRESS_TYPE_DESC = "CLIENT_ADDRESS_TYPE_DESC";
  public static final String SERVICING_FEE_OPTION_DESC = "SERVICING_FEE_OPTION_DESC";
  public static final String ADDRESS_TYPE__DESC = "ADDRESS_TYPE__DESC";
  public static final String RELATIONSHIP_TYPE_DESC = "RELATIONSHIP_TYPE_DESC";
  //cds
  public static final String APPLIED_FOR_BR_TAG = "MirEpiDvAppBr";
  public static final String PENDING_BR_AMOUNT_TAG = "MirEpiDvPendBr";
  public static final String EXISTING_BR_TAG = "MirEpiDvInfcBr";
  public static final String OTHER_INSURER_BR_TAG = "MirEpiDvOinsBr";

  public static final String APPLIED_FOR_BR_KEY = "APPLIED_FOR_BR";
  public static final String PENDING_BR_AMOUNT_KEY = "PENDING_BR_AMOUNT";
  public static final String EXISTING_BR_KEY = "EXISTING_BR";
  public static final String OTHER_INSURER_BR_KEY = "OTHER_INSURER_BR";

  public static final String AGENT_TERMINATION_DATE = "MirAgtCntrctTrmnDt";
  public static final String AGENT_START_DATE = "MirAgtCntrctStrtDt";
  public static final String MIR_PLAN_BTCH_SETL_IND = "MirPlanBtchSetlInd";


  /*
   * zlacsamana 10282014
   * added for FATCA 2B
   */
  //US PERSON
  public static final String NON_US_PERSON = "NON_US_PERSON";//0
  public static final String RECALCITRANT = "RECALCITRANT";//2
  public static final String IN_PROCESS = "IN_PROCESS";//3
  public static final String US_PERSON = "US_PERSON";//4

  //CITY
  public static final String CITY_NY = "CITY_NEW_YORK";

  //FATCA REASON
  public static final String FATCA_REASON_1 = "FATCA_REASON_1";
  public static final String FATCA_REASON_2 = "FATCA_REASON_2";
  public static final String FATCA_REASON_3 = "FATCA_REASON_3";
  public static final String FATCA_REASON_4 = "FATCA_REASON_4";


  //FATCA STATUS

  public static final String FATCA_STATUS_0 = "FATCA_STATUS_0";
  public static final String FATCA_STATUS_2 = "FATCA_STATUS_2";
  public static final String FATCA_STATUS_3 = "FATCA_STATUS_3";
  public static final String FATCA_STATUS_4 = "FATCA_STATUS_4";

  /*
   * playug Oct 20, 2015
   * added for FATCA 3B
   */
  public static final String FATCA_SIGNING_DATE = "MirCliIdSignDtT";
  public static final String FATCA_SUBMISSION_DATE = "MirCliIdSbmsnDtT";
  public static final String FATCA_FORM_STATUS = "MirCliIdStatCdT";

  public static final String FATCA_ENTITY_USTIN = "MirUsTaxIdTxt";
  public static final String FATCA_ENTITY_GIIN = "MirGiinIdTxt";
  public static final String FATCA_ENTITY_CLASSIFICATION = "MirFatcaEntityClassCd";
  public static final String FATCA_ENTITY_STATUS = "MirFatcaStatCd";
  public static final String FATCA_ENTITY_FOREIGN_TIN = "MirFrgnTaxIdTxt";

  public static final String FATCA_RECALCITRANT_DATE = "MirRecalctrntDt";
  public static final String FATCA_RECALCITRANT_STATUS = "MirRecalctrntInd";
  public static final String FATCA_RECALCITRANT_USERID = "MirRecalctrntUserId";
  public static final String FATCA_RECALCITRANT_REASON = "MirFatcaRsnCd";
  public static final String FATCA_RECALCITRANT_REMARKS = "MirRecalctrntRsnTxt";

  public static final String FATCA_RED_FLAG_DATE = "MirRedFlagDt";
  public static final String FATCA_RED_FLAG_USERID = "MirRedflagUserId";
  public static final String FATCA_RED_FLAG_INDICATOR = "MirRedflagInd";
  public static final String FATCA_RED_FLAG_REASON = "MirRedflagRsnCd";
  public static final String FATCA_RED_FLAG_REMARKS = "MirRedflagRsnTxt";

  public static final String FATCA_ENTITY_SUBTANTIAL_US_OWNER_SEQUENCE_NO = "MirSownSeqNumT";
  public static final String FATCA_ENTITY_SUBTANTIAL_US_OWNER_NAME = "MirSownNmT";
  public static final String FATCA_ENTITY_SUBTANTIAL_US_OWNER_ADDRESS = "MirSownAddrTxtT";
  public static final String FATCA_ENTITY_SUBTANTIAL_US_OWNER_COUNTRY = "MirSownCtryCdT";
  public static final String FATCA_ENTITY_SUBTANTIAL_US_OWNER_TIN = "MirSownTaxIdTxtT";

  public static final String FATCA_FORM_STATUS_0 = "FATCA_FORM_STATUS_00";
  public static final String FATCA_FORM_STATUS_1 = "FATCA_FORM_STATUS_01";

  /*
   * Added by IY20 2017/12/19 for Auto Partial Withdrawal Project
   */

  public static final String MIR_PD_AUTO_WTHDR_IND = "MirPdAutoWthdrInd";
  public static final String MIR_AUTO_WTHDR_IND = "MirAutoWthdrInd";
  public static final String MIR_AUTO_WTHDR_TERM_RSN = "MirAutoWthdrTermRsn";
  public static final String MIR_AUTO_WTHDR_EFF_DT = "MirAutoWthdrEffDt";
  public static final String MIR_NEXT_WTHDR_DT = "MirNextWthdrDt";
  public static final String MIR_AUTO_WTHDR_AMT = "MirAutoWthdrAmt";
  public static final String MIR_AUTO_WTHDR_PCT = "MirAutoWthdrPct";
  public static final String MIR_AUTO_WTHDR_DUR = "MirAutoWthdrDur";
  public static final String MIR_REMN_WTHDR_DUR = "MirRemnWthdrDur";

  // prefix
//	public static final String MIR_PD_AUTO_WTHDR_IND_ = "MIR_PD_AUTO_WTHDR_IND_";
  public static final String MIR_AUTO_WTHDR_IND_ = "MIR_AUTO_WTHDR_IND_";
  public static final String MIR_AUTO_WTHDR_TERM_RSN_ = "MIR_AUTO_WTHDR_TERM_RSN_";
//	public static final String MIR_AUTO_WTHDR_EFF_DT_ = "MIR_AUTO_WTHDR_EFF_DT_";
//	public static final String MIR_NEXT_WTHDR_DT_ = "MIR_NEXT_WTHDR_DT_";
//	public static final String MIR_AUTO_WTHDR_AMT_ = "MIR_AUTO_WTHDR_AMT_";
//	public static final String MIR_AUTO_WTHDR_PCT_ = "MIR_AUTO_WTHDR_PCT_";
//	public static final String MIR_AUTO_WTHDR_DUR_ = "MIR_AUTO_WTHDR_DUR_";
//	public static final String MIR_REMN_WTHDR_DUR_ = "MIR_REMN_WTHDR_DUR_";

  public static final String MIR_AUTO_WTHDR_TERM_RSN_DESC = "MIR_AUTO_WTHDR_TERM_RSN_DESC";
  public static final String MIR_AUTO_WTHDR_IND_DESC = "MIR_AUTO_WTHDR_IND_DESC";

  /*
   * Added CHMEN 2015/12/23 for transmittal form
   */
  public static final String SERVICING_BANK_BRANCH = "MirServBnkBrNm";


  public static final String POLCON_OUTPUT_TYPE = "MirPolCntrctTypCd";


  /**
   * MirPolPremSuspAmt Creates a new instance of IngeniumConstants
   */
  public IngeniumConstants() {
  }

  public static HashMap getHashMapValue(String key, HashMap hs) {
    if (hs.containsKey(key))
      return (HashMap) hs.get(key);
    return new HashMap();
  }

  public static List getHashMapListValue(String key, HashMap hs) {
    if (hs.containsKey(key))
      return (List) hs.get(key);
    return new ArrayList();
  }

  public static String getListValue(List list) {
    if (list.size() > 0) {
      return (String) list.get(0);
    } else {
      return "&nbsp;";
    }
  }


  public static String getStatusDesc(String code) {
    ResourceBundle rb = ResourceBundle.getBundle("ph.com.sunlife.wms.ws.ingenium.IngeniumDesc");
    try {
      return rb.getString(code);
    } catch (MissingResourceException e) {
      System.out.println("ERROR. getStatusDesc: Property name[" + code + "] not available from "
          + "IngeniumDesc.properties. Returning empty description instead.");
      return "";
    }
  }

  /*
   * added by vsantos
   * date: 01/15/2014
   * overload getStatusDesc
   */
  public static String getStatusDesc(String propertyName, String originalDesc) {
    ResourceBundle rb = ResourceBundle.getBundle("ph.com.sunlife.wms.ws.ingenium.IngeniumDesc");
    try {
      System.out.println("Getting property value of: " + propertyName);
      return rb.getString(propertyName);
    } catch (MissingResourceException e) {
      System.out.println("ERROR. getStatusDesc: Property name[" + propertyName + "] not available from "
          + "IngeniumDesc.properties. Returning orig description - " + originalDesc);
      return originalDesc;
    }
  }
}
