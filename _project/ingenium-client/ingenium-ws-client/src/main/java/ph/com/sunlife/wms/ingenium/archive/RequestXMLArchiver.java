package ph.com.sunlife.wms.ingenium.archive;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RequestXMLArchiver extends XMLArchiver {

    protected static final Logger LOGGER = LoggerFactory.getLogger(RequestXMLArchiver.class);

    public RequestXMLArchiver(final String name) {
        super(name + "_REQUEST");

        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace("Request XML Archiver initiated for {}", name);
        }
    }
}
