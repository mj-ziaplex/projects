package ph.com.sunlife.ingenium.domain.response.payload;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

@XStreamAlias("MirCrcXpryMoG")
public class MirCrcExpiryMoGroup {

    @XStreamImplicit(itemFieldName = "MirCrcXpryMoT")
    private List<Byte> mirCrcExpiryMoTexts;

    public List<Byte> getMirCrcExpiryMoTexts() {
        return mirCrcExpiryMoTexts;
    }

    public void setMirCrcExpiryMoTexts(List<Byte> mirCrcExpiryMoTexts) {
        this.mirCrcExpiryMoTexts = mirCrcExpiryMoTexts;
    }
}
