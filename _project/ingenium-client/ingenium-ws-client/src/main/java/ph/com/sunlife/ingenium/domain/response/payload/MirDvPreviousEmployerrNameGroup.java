package ph.com.sunlife.ingenium.domain.response.payload;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

@XStreamAlias("MirDvPrevEmplrNmG")
public class MirDvPreviousEmployerrNameGroup {

    @XStreamImplicit(itemFieldName = "MirDvPrevEmplrNmT")
    private List<String> mirDvPreviousEmployerrNameTexts;

    public List<String> getMirDvPreviousEmployerrNameTexts() {
        return mirDvPreviousEmployerrNameTexts;
    }

    public void setMirDvPreviousEmployerrNameTexts(final List<String> mirDvPreviousEmployerrNameTexts) {
        this.mirDvPreviousEmployerrNameTexts = mirDvPreviousEmployerrNameTexts;
    }
}
