package ph.com.sunlife.ingenium.domain;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("MirCommonFields")
public class MirCommonFields {

    @XStreamAlias("MirCmpltnessInd")
    private String mirCmpltnessInd;
    @XStreamAlias("MirCliId")
    private String mirCliId;
    @XStreamAlias("MirReqirId")
    private String mirReqirId;
    @XStreamAlias("MirCpreqStatCd")
    private String mirCpreqStatCd;
    @XStreamAlias("MirCpreqCreatDt")
    private String mirCpreqCreatDt;
    @XStreamAlias("MirDvInsrdCliNm")
    private String mirDvInsuredClientNumber;
    @XStreamAlias("MirPolId")
    private MirPolicyID mirPolicyId;
    // -- Plan Inquiry
    @XStreamAlias("MirCommRtTbacCd")
    private String mirCommRtTbacCd;
    @XStreamAlias("MirPmtLoadCalcCd")
    private String mirPmtLoadCalcCd;
    @XStreamAlias("MirPlanId")
    protected String mirPlanId;
    @XStreamAlias("MirPlanSurrChrgCd")
    private String mirPlanSurrChrgCd;
    @XStreamAlias("MirActnColctId")
    private String mirActnColctId;
    @XStreamAlias("MirPlanAdbXpryAge")
    private Byte mirPlanAdbXpryAge;
    @XStreamAlias("MirAdbPlanId")
    private String mirAdbPlanId;
    @XStreamAlias("MirPlanSysId")
    private String mirPlanSysId;
    @XStreamAlias("MirAgeCalcMthdCd")
    private Byte mirAgeCalcMthdCd;
    @XStreamAlias("MirEnhcEndAge")
    private Byte mirEnhcEndAge;
    @XStreamAlias("MirTrailCommBegDur")
    private Byte mirTrailCommBegDur;
    @XStreamAlias("MirEnhcEndDur")
    private Byte mirEnhcEndDur;
    @XStreamAlias("MirEnhcStrtDur")
    private Byte mirEnhcStrtDur;
    @XStreamAlias("MirCasmId")
    private String mirCasmId;
    @XStreamAlias("MirOvridFrstDgtCd")
    private Byte mirOvridFrstDgtCd;
    @XStreamAlias("MirAppFormTypId")
    private String mirAppFormTypId;
    @XStreamAlias("MirDiaSwpThrshdCd")
    private String mirDiaSwpThrshdCd;
    @XStreamAlias("MirPlanBonEffDy")
    private Byte mirPlanBonEffDy;
    @XStreamAlias("MirPlanBonEffMo")
    private Byte mirPlanBonEffMo;
    @XStreamAlias("MirBasicPlanInd")
    private String mirBasicPlanInd;
    @XStreamAlias("MirPlanCpremCalcCd")
    private Byte mirPlanCpremCalcCd;
    @XStreamAlias("MirPremCalcTypCd")
    private String mirPremCalcTypCd;
    @XStreamAlias("MirAllowCcasInd")
    private String mirAllowCcasInd;
    @XStreamAlias("MirPlanCfTypCd")
    private String mirPlanCfTypCd;
    @XStreamAlias("MirPremChngAgeDur")
    private Byte mirPremChngAgeDur;
    @XStreamAlias("MirInitPremChngAge")
    private Byte mirInitPremChngAge;
    @XStreamAlias("MirUltPremChngAge")
    private Byte mirUltPremChngAge;
    @XStreamAlias("MirPlanBusClasCd")
    private String mirPlanBusClasCd;
    @XStreamAlias("MirPlanCvgLoanInd")
    private String mirPlanCvgLoanInd;
    @XStreamAlias("MirIaCommBand1Amt")
    private Float mirIaCommBand1Amt;
    @XStreamAlias("MirIaCommBand2Amt")
    private Float mirIaCommBand2Amt;
    @XStreamAlias("MirIaCommBand3Amt")
    private Float mirIaCommBand3Amt;
    @XStreamAlias("MirIaCommBand1Pct")
    private Float mirIaCommBand1Pct;
    @XStreamAlias("MirIaCommBand2Pct")
    private Float mirIaCommBand2Pct;
    @XStreamAlias("MirIaCommBand3Pct")
    private Float mirIaCommBand3Pct;
    @XStreamAlias("MirCommTrgSstdInd")
    private String mirCommTrgSstdInd;
    @XStreamAlias("MirCvgCvAllocInd")
    private String mirCvgCvAllocInd;
    @XStreamAlias("MirCola1PlanId")
    private String mirCola1PlanId;
    @XStreamAlias("MirCola2PlanId")
    private String mirCola2PlanId;
    @XStreamAlias("MirCola3PlanId")
    private String mirCola3PlanId;
    @XStreamAlias("MirCola4PlanId")
    private String mirCola4PlanId;
    @XStreamAlias("MirPlanColaXpryAge")
    private Byte mirPlanColaXpryAge;
    @XStreamAlias("MirCnvrXpryAgeDur")
    private Byte mirCnvrXpryAgeDur;
    @XStreamAlias("MirCnvrXpryDtCd")
    private String mirCnvrXpryDtCd;
    @XStreamAlias("MirPlanPayoMthdCd")
    private String mirPlanPayoMthdCd;
    @XStreamAlias("MirPlanRpuDivRt")
    private Float mirPlanRpuDivRt;
    @XStreamAlias("MirCntrctPremInd")
    private String mirCntrctPremInd;
    @XStreamAlias("MirCsvCalcMthdCd")
    private String mirCsvCalcMthdCd;
    @XStreamAlias("MirPlanCsvDtTypCd")
    private String mirPlanCsvDtTypCd;
    @XStreamAlias("MirDbCalcTypCd")
    private String mirDbCalcTypCd;
    @XStreamAlias("MirPmtAllocMthdCd")
    private String mirPmtAllocMthdCd;
    @XStreamAlias("MirPlanAcctTypCd")
    private Byte mirPlanAcctTypCd;
    @XStreamAlias("MirPlanParCd")
    private Byte mirPlanParCd;
    @XStreamAlias("MirPlanIssDtTypCd")
    private String mirPlanIssDtTypCd;
    @XStreamAlias("MirPlanEffDt")
    private String mirPlanEffDt;
    @XStreamAlias("MirRedcEpXpryAge")
    private Byte mirRedcEpXpryAge;
    @XStreamAlias("MirPlanEicFreqCd")
    private String mirPlanEicFreqCd;
    @XStreamAlias("MirEicInitYrDur")
    private Byte mirEicInitYrDur;
    @XStreamAlias("MirEicSubseqYrDur")
    private Byte mirEicSubseqYrDur;
    @XStreamAlias("MirCvgEnhcTypCd")
    private String mirCvgEnhcTypCd;
    @XStreamAlias("MirEnhcFreqDur")
    private Byte mirEnhcFreqDur;
    @XStreamAlias("MirEnhcHiatusDur")
    private Byte mirEnhcHiatusDur;
    @XStreamAlias("MirRedcEpPlanId")
    private String mirRedcEpPlanId;
    @XStreamAlias("MirEtiFaceCalcCd")
    private Byte mirEtiFaceCalcCd;
    @XStreamAlias("MirEtiTbacId")
    private String mirEtiTbacId;
    @XStreamAlias("MirPlanBaseXpryAge")
    private Byte mirPlanBaseXpryAge;
    @XStreamAlias("MirXpryDtCalcCd")
    private Byte mirXpryDtCalcCd;
    @XStreamAlias("MirPolXpryAgeDur")
    private Byte mirPolXpryAgeDur;
    @XStreamAlias("MirAfrThrshdCd")
    private String mirAfrThrshdCd;
    @XStreamAlias("MirFaceAmtTypCd")
    private String mirFaceAmtTypCd;
    @XStreamAlias("MirEnhcUwgFreqDur")
    private Byte mirEnhcUwgFreqDur;
    @XStreamAlias("MirPlanLoanIntRt")
    private Float mirPlanLoanIntRt;
    @XStreamAlias("MirPlanModeFeeAmt")
    private Float mirPlanModeFeeAmt;
    @XStreamAlias("MirFreeLkIncrCd")
    private String mirFreeLkIncrCd;
    @XStreamAlias("MirFreeLkPrcesInd")
    private String mirFreeLkPrcesInd;
    @XStreamAlias("MirLbillModeFeeAmt")
    private Float mirLbillModeFeeAmt;
    @XStreamAlias("MirPacModeFeeAmt")
    private Float mirPacModeFeeAmt;
    @XStreamAlias("MirCrcModeFeeAmt")
    private Float mirCrcModeFeeAmt;
    @XStreamAlias("MirDd2ModeFeeAmt")
    private Float mirDd2ModeFeeAmt;
    @XStreamAlias("MirAutoPremPmtInd")
    private String mirAutoPremPmtInd;
    @XStreamAlias("MirFpulLoadTypCd")
    private String mirFpulLoadTypCd;
    @XStreamAlias("MirFpulAddlPremInd")
    private String mirFpulAddlPremInd;
    @XStreamAlias("MirPlanAgeStbckCd")
    private String mirPlanAgeStbckCd;
    @XStreamAlias("MirSurrChrgTypCd")
    private String mirSurrChrgTypCd;
    @XStreamAlias("MirSurrChrgFlatAmt")
    private Float mirSurrChrgFlatAmt;
    @XStreamAlias("MirSurrChrgRtbl2Id")
    private String mirSurrChrgRtbl2Id;
    @XStreamAlias("MirFyrCommXtraRt")
    private Float mirFyrCommXtraRt;
    @XStreamAlias("MirAcumBrkptAmt")
    private Byte mirAcumBrkptAmt;
    @XStreamAlias("MirGirPremInclInd")
    private String mirGirPremInclInd;
    @XStreamAlias("MirPlanGuarIntRt")
    private Float mirPlanGuarIntRt;
    @XStreamAlias("MirPlanRolovrTypCd")
    private String mirPlanRolovrTypCd;
    @XStreamAlias("MirMinRolovrDyDur")
    private Byte mirMinRolovrDyDur;
    @XStreamAlias("MirMaxRolovrDyDur")
    private Byte mirMaxRolovrDyDur;
    @XStreamAlias("MirIndxIncrMthdCd")
    private String mirIndxIncrMthdCd;
    @XStreamAlias("MirIndxIncrDyDur")
    private Byte mirIndxIncrDyDur;
    @XStreamAlias("MirIndxIncrMoDur")
    private Byte mirIndxIncrMoDur;
    @XStreamAlias("MirIntAdjMaxDur")
    private Byte mirIntAdjMaxDur;
    @XStreamAlias("MirCfIntCalcTypCd")
    private String mirCfIntCalcTypCd;
    @XStreamAlias("MirCfIntModeCd")
    private String mirCfIntModeCd;
    @XStreamAlias("MirPlanIntPayoCd")
    private String mirPlanIntPayoCd;
    @XStreamAlias("MirIswlCalcFreqDur")
    private Byte mirIswlCalcFreqDur;
    @XStreamAlias("MirIswlCalcAdvDur")
    private Byte mirIswlCalcAdvDur;
    @XStreamAlias("MirPremRecalcOptCd")
    private String mirPremRecalcOptCd;
    @XStreamAlias("MirIswlPremVarRt")
    private Float mirIswlPremVarRt;
    @XStreamAlias("MirIswlFrstCalcDur")
    private Byte mirIswlFrstCalcDur;
    @XStreamAlias("MirPlanInsTypCd")
    private String mirPlanInsTypCd;
    @XStreamAlias("MirPlanColfndInd")
    private String mirPlanColfndInd;
    @XStreamAlias("MirColfndReqirInd")
    private String mirColfndReqirInd;
    @XStreamAlias("MirLocGrColctId")
    private String mirLocGrColctId;
    @XStreamAlias("MirLoanIntCalcCd")
    private Byte mirLoanIntCalcCd;
    @XStreamAlias("MirPlanImprdRtCd")
    private String mirPlanImprdRtCd;
    @XStreamAlias("MirPlanImprdIntRt")
    private Float mirPlanImprdIntRt;
    @XStreamAlias("MirLoanIntRtTypCd")
    private String mirLoanIntRtTypCd;
    @XStreamAlias("MirPlanLockFndInd")
    private String mirPlanLockFndInd;
    @XStreamAlias("MirPlanLtaXpryAge")
    private Byte mirPlanLtaXpryAge;
    @XStreamAlias("MirLtaPlanId")
    private String mirLtaPlanId;
    @XStreamAlias("MirPlanLtbXpryAge")
    private Byte mirPlanLtbXpryAge;
    @XStreamAlias("MirLtbPlanId")
    private String mirLtbPlanId;
    @XStreamAlias("MirPlanMatAge")
    private Byte mirPlanMatAge;
    @XStreamAlias("MirMatDtCalcCd")
    private String mirMatDtCalcCd;
    @XStreamAlias("MirMatChrgRtbl2Id")
    private String mirMatChrgRtbl2Id;
    @XStreamAlias("MirPlanMatAgeDur")
    private Byte mirPlanMatAgeDur;
    @XStreamAlias("MirIaCommMaxAmt")
    private Float mirIaCommMaxAmt;
    @XStreamAlias("MirEnhcNelctDur")
    private Byte mirEnhcNelctDur;
    @XStreamAlias("MirMcvCalcReqirInd")
    private String mirMcvCalcReqirInd;
    @XStreamAlias("MirMcvIntMthdCd")
    private String mirMcvIntMthdCd;
    @XStreamAlias("MirMcvPmtLoadCd")
    private String mirMcvPmtLoadCd;
    @XStreamAlias("MirDirDposMinAmt")
    private Float mirDirDposMinAmt;
    @XStreamAlias("MirPacDposMinAmt")
    private Float mirPacDposMinAmt;
    @XStreamAlias("MirCrcDposMinAmt")
    private Float mirCrcDposMinAmt;
    @XStreamAlias("MirDd2DposMinAmt")
    private Float mirDd2DposMinAmt;
    @XStreamAlias("MirPlanMinMpremQty")
    private Byte mirPlanMinMpremQty;
    @XStreamAlias("MirPlanMdrtClasCd")
    private String mirPlanMdrtClasCd;
    @XStreamAlias("MirMortXpnsChrgAge")
    private Byte mirMortXpnsChrgAge;
    @XStreamAlias("MirMortXpnsCalcCd")
    private String mirMortXpnsCalcCd;
    @XStreamAlias("MirMortXpnsChrgDur")
    private Byte mirMortXpnsChrgDur;
    @XStreamAlias("MirPlanRolovrDtInd")
    private String mirPlanRolovrDtInd;
    @XStreamAlias("MirMinCaslPayoAmt")
    private Float mirMinCaslPayoAmt;
    @XStreamAlias("MirMnpmtTrgSstdInd")
    private String mirMnpmtTrgSstdInd;
    @XStreamAlias("MirSurrAdjCalcCd")
    private String mirSurrAdjCalcCd;
    @XStreamAlias("MirSurrAdjMoDur")
    private Byte mirSurrAdjMoDur;
    @XStreamAlias("MirSurrAdjTypCd")
    private String mirSurrAdjTypCd;
    @XStreamAlias("MirPlanMktvalAdjCd")
    private String mirPlanMktvalAdjCd;
    @XStreamAlias("MirRpuTbacId")
    private String mirRpuTbacId;
    @XStreamAlias("MirNfPrcesMoDur")
    private Byte mirNfPrcesMoDur;
    @XStreamAlias("MirNotiLowAgeDur")
    private Byte mirNotiLowAgeDur;
    @XStreamAlias("MirNotiDeptId")
    private String mirNotiDeptId;
    @XStreamAlias("MirNotiDtCalcCd")
    private Byte mirNotiDtCalcCd;
    @XStreamAlias("MirNotiHighAgeDur")
    private Byte mirNotiHighAgeDur;
    @XStreamAlias("MirNumGirOptQty")
    private Byte mirNumGirOptQty;
    @XStreamAlias("MirOwnOccpXpryAge")
    private Byte mirOwnOccpXpryAge;
    @XStreamAlias("MirOwnOccpPlanId")
    private String mirOwnOccpPlanId;
    @XStreamAlias("MirDivOytTbacId")
    private String mirDivOytTbacId;
    @XStreamAlias("MirPmtLoadFlatAmt")
    private Float mirPmtLoadFlatAmt;
    @XStreamAlias("MirPmtLoadOrdrCd")
    private String mirPmtLoadOrdrCd;
    @XStreamAlias("MirPmtLoadRtbl2Id")
    private String mirPmtLoadRtbl2Id;
    @XStreamAlias("MirPlanMinPuAmt")
    private Float mirPlanMinPuAmt;
    @XStreamAlias("MirMthvPrcesInd")
    private String mirMthvPrcesInd;
    @XStreamAlias("MirCommPlanTypCd")
    private String mirCommPlanTypCd;
    @XStreamAlias("MirPdisabXpryAge")
    private Byte mirPdisabXpryAge;
    @XStreamAlias("MirPdisabPlanId")
    private String mirPdisabPlanId;
    @XStreamAlias("MirCvAllocMthdCd")
    private String mirCvAllocMthdCd;
    @XStreamAlias("MirPlanPfeeAmt")
    private Float mirPlanPfeeAmt;
    @XStreamAlias("MirPlanIaPfeeAmt")
    private Float mirPlanIaPfeeAmt;
    @XStreamAlias("MirPlanCvgPfeeAmt")
    private Float mirPlanCvgPfeeAmt;
    @XStreamAlias("MirPnsnQualfInd")
    private String mirPnsnQualfInd;
    @XStreamAlias("MirPremChngDtCd")
    private String mirPremChngDtCd;
    @XStreamAlias("MirPlanLocLicCd")
    private String mirPlanLocLicCd;
    @XStreamAlias("MirTrmPremRfndRt")
    private Float mirTrmPremRfndRt;
    @XStreamAlias("MirPeriStmtDyDur")
    private Byte mirPeriStmtDyDur;
    @XStreamAlias("MirPeriStmtMoDur")
    private Byte mirPeriStmtMoDur;
    @XStreamAlias("MirPeriStmtTypCd")
    private String mirPeriStmtTypCd;
    @XStreamAlias("MirPsurChrgFlatAmt")
    private Float mirPsurChrgFlatAmt;
    @XStreamAlias("MirPsurChrgRtbl2Id")
    private String mirPsurChrgRtbl2Id;
    @XStreamAlias("MirPsurChrgTypCd")
    private String mirPsurChrgTypCd;
    @XStreamAlias("MirPuaDivCalcCd")
    private String mirPuaDivCalcCd;
    @XStreamAlias("MirDivPuaTbacId")
    private String mirDivPuaTbacId;
    @XStreamAlias("MirPmtLoadSstdInd")
    private String mirPmtLoadSstdInd;
    @XStreamAlias("MirPlanRegInd")
    private String mirPlanRegInd;
    @XStreamAlias("MirCommRtLoopInd")
    private String mirCommRtLoopInd;
    @XStreamAlias("MirPremRenwRtCd")
    private String mirPremRenwRtCd;
    @XStreamAlias("MirPlanCvgXpryAge")
    private Byte mirPlanCvgXpryAge;
    @XStreamAlias("MirCvgXpryAgeDur")
    private Byte mirCvgXpryAgeDur;
    @XStreamAlias("MirCommRhSubCd")
    private String mirCommRhSubCd;
    @XStreamAlias("MirPlanNarDscntRt")
    private Float mirPlanNarDscntRt;
    @XStreamAlias("MirPlanRsrvCalcCd")
    private Byte mirPlanRsrvCalcCd;
    @XStreamAlias("MirSeGdlnApremInd")
    private String mirSeGdlnApremInd;
    @XStreamAlias("MirAgeStbckStrtAge")
    private Byte mirAgeStbckStrtAge;
    @XStreamAlias("MirPlanAgeStbckQty")
    private Byte mirPlanAgeStbckQty;
    @XStreamAlias("MirSfbNegSuspInd")
    private String mirSfbNegSuspInd;
    @XStreamAlias("MirSfbNegSuspQty")
    private Byte mirSfbNegSuspQty;
    @XStreamAlias("MirSumInsCalcCd")
    private Byte mirSumInsCalcCd;
    @XStreamAlias("MirMajLnSortCd")
    private Byte mirMajLnSortCd;
    @XStreamAlias("MirPlanSuppBnftCd")
    private String mirPlanSuppBnftCd;
    @XStreamAlias("MirSurrChrgCalcCd")
    private String mirSurrChrgCalcCd;
    @XStreamAlias("MirDposWthdrOrdrCd")
    private String mirDposWthdrOrdrCd;
    @XStreamAlias("MirSurrTrgSstdInd")
    private String mirSurrTrgSstdInd;
    @XStreamAlias("MirT5RegTitlTxt")
    private String mirT5RegTitlTxt;
    @XStreamAlias("MirPlanTamraCd")
    private String mirPlanTamraCd;
    @XStreamAlias("MirDiaSwpThrshdAmt")
    private Float mirDiaSwpThrshdAmt;
    @XStreamAlias("MirAfrThrshdPeriCd")
    private String mirAfrThrshdPeriCd;
    @XStreamAlias("MirTrailCommFreqCd")
    private String mirTrailCommFreqCd;
    @XStreamAlias("MirTrailCommLagDur")
    private Byte mirTrailCommLagDur;
    @XStreamAlias("MirPlanTxempCd")
    private String mirPlanTxempCd;
    @XStreamAlias("MirGainRptTimeCd")
    private String mirGainRptTimeCd;
    @XStreamAlias("MirPlanTaxwhInd")
    private String mirPlanTaxwhInd;
    @XStreamAlias("MirPlanUnitValuAmt")
    private Short mirPlanUnitValuAmt;
    @XStreamAlias("MirUwgFaceAmtFct")
    private Float mirUwgFaceAmtFct;
    @XStreamAlias("MirPlanUwgInd")
    private String mirPlanUwgInd;
    @XStreamAlias("MirPlanVpoInd")
    private String mirPlanVpoInd;
    @XStreamAlias("MirPlanWpXpryAge")
    private Byte mirPlanWpXpryAge;
    @XStreamAlias("MirWpPlanId")
    private String mirWpPlanId;
    @XStreamAlias("MirPlanWthdrOrdrCd")
    private String mirPlanWthdrOrdrCd;
    @XStreamAlias("MirPlanWpXpryCd")
    private String mirPlanWpXpryCd;
    @XStreamAlias("MirPlanBonCalcCd")
    private String mirPlanBonCalcCd;
    @XStreamAlias("MirPlanInitPmtInd")
    private String mirPlanInitPmtInd;
    @XStreamAlias("MirPlanFndTypCd")
    private String mirPlanFndTypCd;
    @XStreamAlias("MirPlanMultCrcyInd")
    private String mirPlanMultCrcyInd;
    @XStreamAlias("MirPlanCrcyCd")
    private String mirPlanCrcyCd;
    @XStreamAlias("MirPlanUnitTypCd")
    private Byte mirPlanUnitTypCd;
    @XStreamAlias("MirPlanInclDbgInd")
    private String mirPlanInclDbgInd;
    @XStreamAlias("MirPlanOffannvInd")
    private String mirPlanOffannvInd;
    @XStreamAlias("MirPlanDbOptCd")
    private String mirPlanDbOptCd;
    @XStreamAlias("MirRpuFaceCalcCd")
    private String mirRpuFaceCalcCd;
    @XStreamAlias("MirBnftCatTyp")
    private Byte mirBnftCatTyp;
    @XStreamAlias("MirMaxLmtChkInd")
    private Byte mirMaxLmtChkInd;
    @XStreamAlias("MirPoOptInd")
    private String mirPoOptInd;
    @XStreamAlias("MirPlanBtchSetlInd")
    private String mirPlanBtchSetlInd;
    @XStreamAlias("MirExcessPymtInd")
    private String mirExcessPymtInd;

    public MirCommonFields() { }

    public String getMirCliId() {
        return mirCliId;
    }

    public MirCommonFields setMirCliId(String mirCliId) {
        this.mirCliId = mirCliId;
        return this;
    }

    public String getMirCmpltnessInd() {
        return mirCmpltnessInd;
    }

    public MirCommonFields setMirCmpltnessInd(String mirCmpltnessInd) {
        this.mirCmpltnessInd = mirCmpltnessInd;
        return this;
    }

    public String getMirCpreqCreatDt() {
        return mirCpreqCreatDt;
    }

    public MirCommonFields setMirCpreqCreatDt(String mirCpreqCreatDt) {
        this.mirCpreqCreatDt = mirCpreqCreatDt;
        return this;
    }

    public String getMirCpreqStatCd() {
        return mirCpreqStatCd;
    }

    public MirCommonFields setMirCpreqStatCd(String mirCpreqStatCd) {
        this.mirCpreqStatCd = mirCpreqStatCd;
        return this;
    }

    public String getMirDvInsuredClientNumber() {
        return mirDvInsuredClientNumber;
    }

    public MirCommonFields setMirDvInsuredClientNumber(String mirDvInsuredClientNumber) {
        this.mirDvInsuredClientNumber = mirDvInsuredClientNumber;
        return this;
    }

    public MirPolicyID getMirPolicyId() {
        return mirPolicyId;
    }

    public MirCommonFields setMirPolicyId(MirPolicyID mirPolicyId) {
        this.mirPolicyId = mirPolicyId;
        return this;
    }

    public String getMirReqirId() {
        return mirReqirId;
    }

    public MirCommonFields setMirReqirId(String mirReqirId) {
        this.mirReqirId = mirReqirId;
        return this;
    }

    public String getMirPlanId() {
        return mirPlanId;
    }

    public void setMirPlanId(String mirPlanId) {
        this.mirPlanId = mirPlanId;
    }

    public String getMirCommRtTbacCd() {
        return mirCommRtTbacCd;
    }

    public void setMirCommRtTbacCd(String mirCommRtTbacCd) {
        this.mirCommRtTbacCd = mirCommRtTbacCd;
    }

    public String getMirPmtLoadCalcCd() {
        return mirPmtLoadCalcCd;
    }

    public void setMirPmtLoadCalcCd(String mirPmtLoadCalcCd) {
        this.mirPmtLoadCalcCd = mirPmtLoadCalcCd;
    }

    public String getMirPlanSurrChrgCd() {
        return mirPlanSurrChrgCd;
    }

    public void setMirPlanSurrChrgCd(String mirPlanSurrChrgCd) {
        this.mirPlanSurrChrgCd = mirPlanSurrChrgCd;
    }

    public String getMirActnColctId() {
        return mirActnColctId;
    }

    public void setMirActnColctId(String mirActnColctId) {
        this.mirActnColctId = mirActnColctId;
    }

    public Byte getMirPlanAdbXpryAge() {
        return mirPlanAdbXpryAge;
    }

    public void setMirPlanAdbXpryAge(Byte mirPlanAdbXpryAge) {
        this.mirPlanAdbXpryAge = mirPlanAdbXpryAge;
    }

    public String getMirAdbPlanId() {
        return mirAdbPlanId;
    }

    public void setMirAdbPlanId(String mirAdbPlanId) {
        this.mirAdbPlanId = mirAdbPlanId;
    }

    public String getMirPlanSysId() {
        return mirPlanSysId;
    }

    public void setMirPlanSysId(String mirPlanSysId) {
        this.mirPlanSysId = mirPlanSysId;
    }

    public Byte getMirAgeCalcMthdCd() {
        return mirAgeCalcMthdCd;
    }

    public void setMirAgeCalcMthdCd(Byte mirAgeCalcMthdCd) {
        this.mirAgeCalcMthdCd = mirAgeCalcMthdCd;
    }

    public Byte getMirEnhcEndAge() {
        return mirEnhcEndAge;
    }

    public void setMirEnhcEndAge(Byte mirEnhcEndAge) {
        this.mirEnhcEndAge = mirEnhcEndAge;
    }

    public Byte getMirTrailCommBegDur() {
        return mirTrailCommBegDur;
    }

    public void setMirTrailCommBegDur(Byte mirTrailCommBegDur) {
        this.mirTrailCommBegDur = mirTrailCommBegDur;
    }

    public Byte getMirEnhcEndDur() {
        return mirEnhcEndDur;
    }

    public void setMirEnhcEndDur(Byte mirEnhcEndDur) {
        this.mirEnhcEndDur = mirEnhcEndDur;
    }

    public Byte getMirEnhcStrtDur() {
        return mirEnhcStrtDur;
    }

    public void setMirEnhcStrtDur(Byte mirEnhcStrtDur) {
        this.mirEnhcStrtDur = mirEnhcStrtDur;
    }

    public String getMirCasmId() {
        return mirCasmId;
    }

    public void setMirCasmId(String mirCasmId) {
        this.mirCasmId = mirCasmId;
    }

    public Byte getMirOvridFrstDgtCd() {
        return mirOvridFrstDgtCd;
    }

    public void setMirOvridFrstDgtCd(Byte mirOvridFrstDgtCd) {
        this.mirOvridFrstDgtCd = mirOvridFrstDgtCd;
    }

    public String getMirAppFormTypId() {
        return mirAppFormTypId;
    }

    public void setMirAppFormTypId(String mirAppFormTypId) {
        this.mirAppFormTypId = mirAppFormTypId;
    }

    public String getMirDiaSwpThrshdCd() {
        return mirDiaSwpThrshdCd;
    }

    public void setMirDiaSwpThrshdCd(String mirDiaSwpThrshdCd) {
        this.mirDiaSwpThrshdCd = mirDiaSwpThrshdCd;
    }

    public Byte getMirPlanBonEffDy() {
        return mirPlanBonEffDy;
    }

    public void setMirPlanBonEffDy(Byte mirPlanBonEffDy) {
        this.mirPlanBonEffDy = mirPlanBonEffDy;
    }

    public Byte getMirPlanBonEffMo() {
        return mirPlanBonEffMo;
    }

    public void setMirPlanBonEffMo(Byte mirPlanBonEffMo) {
        this.mirPlanBonEffMo = mirPlanBonEffMo;
    }

    public String getMirBasicPlanInd() {
        return mirBasicPlanInd;
    }

    public void setMirBasicPlanInd(String mirBasicPlanInd) {
        this.mirBasicPlanInd = mirBasicPlanInd;
    }

    public Byte getMirPlanCpremCalcCd() {
        return mirPlanCpremCalcCd;
    }

    public void setMirPlanCpremCalcCd(Byte mirPlanCpremCalcCd) {
        this.mirPlanCpremCalcCd = mirPlanCpremCalcCd;
    }

    public String getMirPremCalcTypCd() {
        return mirPremCalcTypCd;
    }

    public void setMirPremCalcTypCd(String mirPremCalcTypCd) {
        this.mirPremCalcTypCd = mirPremCalcTypCd;
    }

    public String getMirAllowCcasInd() {
        return mirAllowCcasInd;
    }

    public void setMirAllowCcasInd(String mirAllowCcasInd) {
        this.mirAllowCcasInd = mirAllowCcasInd;
    }

    public String getMirPlanCfTypCd() {
        return mirPlanCfTypCd;
    }

    public void setMirPlanCfTypCd(String mirPlanCfTypCd) {
        this.mirPlanCfTypCd = mirPlanCfTypCd;
    }

    public Byte getMirPremChngAgeDur() {
        return mirPremChngAgeDur;
    }

    public void setMirPremChngAgeDur(Byte mirPremChngAgeDur) {
        this.mirPremChngAgeDur = mirPremChngAgeDur;
    }

    public Byte getMirInitPremChngAge() {
        return mirInitPremChngAge;
    }

    public void setMirInitPremChngAge(Byte mirInitPremChngAge) {
        this.mirInitPremChngAge = mirInitPremChngAge;
    }

    public Byte getMirUltPremChngAge() {
        return mirUltPremChngAge;
    }

    public void setMirUltPremChngAge(Byte mirUltPremChngAge) {
        this.mirUltPremChngAge = mirUltPremChngAge;
    }

    public String getMirPlanBusClasCd() {
        return mirPlanBusClasCd;
    }

    public void setMirPlanBusClasCd(String mirPlanBusClasCd) {
        this.mirPlanBusClasCd = mirPlanBusClasCd;
    }

    public String getMirPlanCvgLoanInd() {
        return mirPlanCvgLoanInd;
    }

    public void setMirPlanCvgLoanInd(String mirPlanCvgLoanInd) {
        this.mirPlanCvgLoanInd = mirPlanCvgLoanInd;
    }

    public Float getMirIaCommBand1Amt() {
        return mirIaCommBand1Amt;
    }

    public void setMirIaCommBand1Amt(Float mirIaCommBand1Amt) {
        this.mirIaCommBand1Amt = mirIaCommBand1Amt;
    }

    public Float getMirIaCommBand2Amt() {
        return mirIaCommBand2Amt;
    }

    public void setMirIaCommBand2Amt(Float mirIaCommBand2Amt) {
        this.mirIaCommBand2Amt = mirIaCommBand2Amt;
    }

    public Float getMirIaCommBand3Amt() {
        return mirIaCommBand3Amt;
    }

    public void setMirIaCommBand3Amt(Float mirIaCommBand3Amt) {
        this.mirIaCommBand3Amt = mirIaCommBand3Amt;
    }

    public Float getMirIaCommBand1Pct() {
        return mirIaCommBand1Pct;
    }

    public void setMirIaCommBand1Pct(Float mirIaCommBand1Pct) {
        this.mirIaCommBand1Pct = mirIaCommBand1Pct;
    }

    public Float getMirIaCommBand2Pct() {
        return mirIaCommBand2Pct;
    }

    public void setMirIaCommBand2Pct(Float mirIaCommBand2Pct) {
        this.mirIaCommBand2Pct = mirIaCommBand2Pct;
    }

    public Float getMirIaCommBand3Pct() {
        return mirIaCommBand3Pct;
    }

    public void setMirIaCommBand3Pct(Float mirIaCommBand3Pct) {
        this.mirIaCommBand3Pct = mirIaCommBand3Pct;
    }

    public String getMirCommTrgSstdInd() {
        return mirCommTrgSstdInd;
    }

    public void setMirCommTrgSstdInd(String mirCommTrgSstdInd) {
        this.mirCommTrgSstdInd = mirCommTrgSstdInd;
    }

    public String getMirCvgCvAllocInd() {
        return mirCvgCvAllocInd;
    }

    public void setMirCvgCvAllocInd(String mirCvgCvAllocInd) {
        this.mirCvgCvAllocInd = mirCvgCvAllocInd;
    }

    public String getMirCola1PlanId() {
        return mirCola1PlanId;
    }

    public void setMirCola1PlanId(String mirCola1PlanId) {
        this.mirCola1PlanId = mirCola1PlanId;
    }

    public String getMirCola2PlanId() {
        return mirCola2PlanId;
    }

    public void setMirCola2PlanId(String mirCola2PlanId) {
        this.mirCola2PlanId = mirCola2PlanId;
    }

    public String getMirCola3PlanId() {
        return mirCola3PlanId;
    }

    public void setMirCola3PlanId(String mirCola3PlanId) {
        this.mirCola3PlanId = mirCola3PlanId;
    }

    public String getMirCola4PlanId() {
        return mirCola4PlanId;
    }

    public void setMirCola4PlanId(String mirCola4PlanId) {
        this.mirCola4PlanId = mirCola4PlanId;
    }

    public Byte getMirPlanColaXpryAge() {
        return mirPlanColaXpryAge;
    }

    public void setMirPlanColaXpryAge(Byte mirPlanColaXpryAge) {
        this.mirPlanColaXpryAge = mirPlanColaXpryAge;
    }

    public Byte getMirCnvrXpryAgeDur() {
        return mirCnvrXpryAgeDur;
    }

    public void setMirCnvrXpryAgeDur(Byte mirCnvrXpryAgeDur) {
        this.mirCnvrXpryAgeDur = mirCnvrXpryAgeDur;
    }

    public String getMirCnvrXpryDtCd() {
        return mirCnvrXpryDtCd;
    }

    public void setMirCnvrXpryDtCd(String mirCnvrXpryDtCd) {
        this.mirCnvrXpryDtCd = mirCnvrXpryDtCd;
    }

    public String getMirPlanPayoMthdCd() {
        return mirPlanPayoMthdCd;
    }

    public void setMirPlanPayoMthdCd(String mirPlanPayoMthdCd) {
        this.mirPlanPayoMthdCd = mirPlanPayoMthdCd;
    }

    public Float getMirPlanRpuDivRt() {
        return mirPlanRpuDivRt;
    }

    public void setMirPlanRpuDivRt(Float mirPlanRpuDivRt) {
        this.mirPlanRpuDivRt = mirPlanRpuDivRt;
    }

    public String getMirCntrctPremInd() {
        return mirCntrctPremInd;
    }

    public void setMirCntrctPremInd(String mirCntrctPremInd) {
        this.mirCntrctPremInd = mirCntrctPremInd;
    }

    public String getMirCsvCalcMthdCd() {
        return mirCsvCalcMthdCd;
    }

    public void setMirCsvCalcMthdCd(String mirCsvCalcMthdCd) {
        this.mirCsvCalcMthdCd = mirCsvCalcMthdCd;
    }

    public String getMirPlanCsvDtTypCd() {
        return mirPlanCsvDtTypCd;
    }

    public void setMirPlanCsvDtTypCd(String mirPlanCsvDtTypCd) {
        this.mirPlanCsvDtTypCd = mirPlanCsvDtTypCd;
    }

    public String getMirDbCalcTypCd() {
        return mirDbCalcTypCd;
    }

    public void setMirDbCalcTypCd(String mirDbCalcTypCd) {
        this.mirDbCalcTypCd = mirDbCalcTypCd;
    }

    public String getMirPmtAllocMthdCd() {
        return mirPmtAllocMthdCd;
    }

    public void setMirPmtAllocMthdCd(String mirPmtAllocMthdCd) {
        this.mirPmtAllocMthdCd = mirPmtAllocMthdCd;
    }

    public Byte getMirPlanAcctTypCd() {
        return mirPlanAcctTypCd;
    }

    public void setMirPlanAcctTypCd(Byte mirPlanAcctTypCd) {
        this.mirPlanAcctTypCd = mirPlanAcctTypCd;
    }

    public Byte getMirPlanParCd() {
        return mirPlanParCd;
    }

    public void setMirPlanParCd(Byte mirPlanParCd) {
        this.mirPlanParCd = mirPlanParCd;
    }

    public String getMirPlanIssDtTypCd() {
        return mirPlanIssDtTypCd;
    }

    public void setMirPlanIssDtTypCd(String mirPlanIssDtTypCd) {
        this.mirPlanIssDtTypCd = mirPlanIssDtTypCd;
    }

    public String getMirPlanEffDt() {
        return mirPlanEffDt;
    }

    public void setMirPlanEffDt(String mirPlanEffDt) {
        this.mirPlanEffDt = mirPlanEffDt;
    }

    public Byte getMirRedcEpXpryAge() {
        return mirRedcEpXpryAge;
    }

    public void setMirRedcEpXpryAge(Byte mirRedcEpXpryAge) {
        this.mirRedcEpXpryAge = mirRedcEpXpryAge;
    }

    public String getMirPlanEicFreqCd() {
        return mirPlanEicFreqCd;
    }

    public void setMirPlanEicFreqCd(String mirPlanEicFreqCd) {
        this.mirPlanEicFreqCd = mirPlanEicFreqCd;
    }

    public Byte getMirEicInitYrDur() {
        return mirEicInitYrDur;
    }

    public void setMirEicInitYrDur(Byte mirEicInitYrDur) {
        this.mirEicInitYrDur = mirEicInitYrDur;
    }

    public Byte getMirEicSubseqYrDur() {
        return mirEicSubseqYrDur;
    }

    public void setMirEicSubseqYrDur(Byte mirEicSubseqYrDur) {
        this.mirEicSubseqYrDur = mirEicSubseqYrDur;
    }

    public String getMirCvgEnhcTypCd() {
        return mirCvgEnhcTypCd;
    }

    public void setMirCvgEnhcTypCd(String mirCvgEnhcTypCd) {
        this.mirCvgEnhcTypCd = mirCvgEnhcTypCd;
    }

    public Byte getMirEnhcFreqDur() {
        return mirEnhcFreqDur;
    }

    public void setMirEnhcFreqDur(Byte mirEnhcFreqDur) {
        this.mirEnhcFreqDur = mirEnhcFreqDur;
    }

    public Byte getMirEnhcHiatusDur() {
        return mirEnhcHiatusDur;
    }

    public void setMirEnhcHiatusDur(Byte mirEnhcHiatusDur) {
        this.mirEnhcHiatusDur = mirEnhcHiatusDur;
    }

    public String getMirRedcEpPlanId() {
        return mirRedcEpPlanId;
    }

    public void setMirRedcEpPlanId(String mirRedcEpPlanId) {
        this.mirRedcEpPlanId = mirRedcEpPlanId;
    }

    public Byte getMirEtiFaceCalcCd() {
        return mirEtiFaceCalcCd;
    }

    public void setMirEtiFaceCalcCd(Byte mirEtiFaceCalcCd) {
        this.mirEtiFaceCalcCd = mirEtiFaceCalcCd;
    }

    public String getMirEtiTbacId() {
        return mirEtiTbacId;
    }

    public void setMirEtiTbacId(String mirEtiTbacId) {
        this.mirEtiTbacId = mirEtiTbacId;
    }

    public Byte getMirPlanBaseXpryAge() {
        return mirPlanBaseXpryAge;
    }

    public void setMirPlanBaseXpryAge(Byte mirPlanBaseXpryAge) {
        this.mirPlanBaseXpryAge = mirPlanBaseXpryAge;
    }

    public Byte getMirXpryDtCalcCd() {
        return mirXpryDtCalcCd;
    }

    public void setMirXpryDtCalcCd(Byte mirXpryDtCalcCd) {
        this.mirXpryDtCalcCd = mirXpryDtCalcCd;
    }

    public Byte getMirPolXpryAgeDur() {
        return mirPolXpryAgeDur;
    }

    public void setMirPolXpryAgeDur(Byte mirPolXpryAgeDur) {
        this.mirPolXpryAgeDur = mirPolXpryAgeDur;
    }

    public String getMirAfrThrshdCd() {
        return mirAfrThrshdCd;
    }

    public void setMirAfrThrshdCd(String mirAfrThrshdCd) {
        this.mirAfrThrshdCd = mirAfrThrshdCd;
    }

    public String getMirFaceAmtTypCd() {
        return mirFaceAmtTypCd;
    }

    public void setMirFaceAmtTypCd(String mirFaceAmtTypCd) {
        this.mirFaceAmtTypCd = mirFaceAmtTypCd;
    }

    public Byte getMirEnhcUwgFreqDur() {
        return mirEnhcUwgFreqDur;
    }

    public void setMirEnhcUwgFreqDur(Byte mirEnhcUwgFreqDur) {
        this.mirEnhcUwgFreqDur = mirEnhcUwgFreqDur;
    }

    public Float getMirPlanLoanIntRt() {
        return mirPlanLoanIntRt;
    }

    public void setMirPlanLoanIntRt(Float mirPlanLoanIntRt) {
        this.mirPlanLoanIntRt = mirPlanLoanIntRt;
    }

    public Float getMirPlanModeFeeAmt() {
        return mirPlanModeFeeAmt;
    }

    public void setMirPlanModeFeeAmt(Float mirPlanModeFeeAmt) {
        this.mirPlanModeFeeAmt = mirPlanModeFeeAmt;
    }

    public String getMirFreeLkIncrCd() {
        return mirFreeLkIncrCd;
    }

    public void setMirFreeLkIncrCd(String mirFreeLkIncrCd) {
        this.mirFreeLkIncrCd = mirFreeLkIncrCd;
    }

    public String getMirFreeLkPrcesInd() {
        return mirFreeLkPrcesInd;
    }

    public void setMirFreeLkPrcesInd(String mirFreeLkPrcesInd) {
        this.mirFreeLkPrcesInd = mirFreeLkPrcesInd;
    }

    public Float getMirLbillModeFeeAmt() {
        return mirLbillModeFeeAmt;
    }

    public void setMirLbillModeFeeAmt(Float mirLbillModeFeeAmt) {
        this.mirLbillModeFeeAmt = mirLbillModeFeeAmt;
    }

    public Float getMirPacModeFeeAmt() {
        return mirPacModeFeeAmt;
    }

    public void setMirPacModeFeeAmt(Float mirPacModeFeeAmt) {
        this.mirPacModeFeeAmt = mirPacModeFeeAmt;
    }

    public Float getMirCrcModeFeeAmt() {
        return mirCrcModeFeeAmt;
    }

    public void setMirCrcModeFeeAmt(Float mirCrcModeFeeAmt) {
        this.mirCrcModeFeeAmt = mirCrcModeFeeAmt;
    }

    public Float getMirDd2ModeFeeAmt() {
        return mirDd2ModeFeeAmt;
    }

    public void setMirDd2ModeFeeAmt(Float mirDd2ModeFeeAmt) {
        this.mirDd2ModeFeeAmt = mirDd2ModeFeeAmt;
    }

    public String getMirAutoPremPmtInd() {
        return mirAutoPremPmtInd;
    }

    public void setMirAutoPremPmtInd(String mirAutoPremPmtInd) {
        this.mirAutoPremPmtInd = mirAutoPremPmtInd;
    }

    public String getMirFpulLoadTypCd() {
        return mirFpulLoadTypCd;
    }

    public void setMirFpulLoadTypCd(String mirFpulLoadTypCd) {
        this.mirFpulLoadTypCd = mirFpulLoadTypCd;
    }

    public String getMirFpulAddlPremInd() {
        return mirFpulAddlPremInd;
    }

    public void setMirFpulAddlPremInd(String mirFpulAddlPremInd) {
        this.mirFpulAddlPremInd = mirFpulAddlPremInd;
    }

    public String getMirPlanAgeStbckCd() {
        return mirPlanAgeStbckCd;
    }

    public void setMirPlanAgeStbckCd(String mirPlanAgeStbckCd) {
        this.mirPlanAgeStbckCd = mirPlanAgeStbckCd;
    }

    public String getMirSurrChrgTypCd() {
        return mirSurrChrgTypCd;
    }

    public void setMirSurrChrgTypCd(String mirSurrChrgTypCd) {
        this.mirSurrChrgTypCd = mirSurrChrgTypCd;
    }

    public Float getMirSurrChrgFlatAmt() {
        return mirSurrChrgFlatAmt;
    }

    public void setMirSurrChrgFlatAmt(Float mirSurrChrgFlatAmt) {
        this.mirSurrChrgFlatAmt = mirSurrChrgFlatAmt;
    }

    public String getMirSurrChrgRtbl2Id() {
        return mirSurrChrgRtbl2Id;
    }

    public void setMirSurrChrgRtbl2Id(String mirSurrChrgRtbl2Id) {
        this.mirSurrChrgRtbl2Id = mirSurrChrgRtbl2Id;
    }

    public Float getMirFyrCommXtraRt() {
        return mirFyrCommXtraRt;
    }

    public void setMirFyrCommXtraRt(Float mirFyrCommXtraRt) {
        this.mirFyrCommXtraRt = mirFyrCommXtraRt;
    }

    public Byte getMirAcumBrkptAmt() {
        return mirAcumBrkptAmt;
    }

    public void setMirAcumBrkptAmt(Byte mirAcumBrkptAmt) {
        this.mirAcumBrkptAmt = mirAcumBrkptAmt;
    }

    public String getMirGirPremInclInd() {
        return mirGirPremInclInd;
    }

    public void setMirGirPremInclInd(String mirGirPremInclInd) {
        this.mirGirPremInclInd = mirGirPremInclInd;
    }

    public Float getMirPlanGuarIntRt() {
        return mirPlanGuarIntRt;
    }

    public void setMirPlanGuarIntRt(Float mirPlanGuarIntRt) {
        this.mirPlanGuarIntRt = mirPlanGuarIntRt;
    }

    public String getMirPlanRolovrTypCd() {
        return mirPlanRolovrTypCd;
    }

    public void setMirPlanRolovrTypCd(String mirPlanRolovrTypCd) {
        this.mirPlanRolovrTypCd = mirPlanRolovrTypCd;
    }

    public Byte getMirMinRolovrDyDur() {
        return mirMinRolovrDyDur;
    }

    public void setMirMinRolovrDyDur(Byte mirMinRolovrDyDur) {
        this.mirMinRolovrDyDur = mirMinRolovrDyDur;
    }

    public Byte getMirMaxRolovrDyDur() {
        return mirMaxRolovrDyDur;
    }

    public void setMirMaxRolovrDyDur(Byte mirMaxRolovrDyDur) {
        this.mirMaxRolovrDyDur = mirMaxRolovrDyDur;
    }

    public String getMirIndxIncrMthdCd() {
        return mirIndxIncrMthdCd;
    }

    public void setMirIndxIncrMthdCd(String mirIndxIncrMthdCd) {
        this.mirIndxIncrMthdCd = mirIndxIncrMthdCd;
    }

    public Byte getMirIndxIncrDyDur() {
        return mirIndxIncrDyDur;
    }

    public void setMirIndxIncrDyDur(Byte mirIndxIncrDyDur) {
        this.mirIndxIncrDyDur = mirIndxIncrDyDur;
    }

    public Byte getMirIndxIncrMoDur() {
        return mirIndxIncrMoDur;
    }

    public void setMirIndxIncrMoDur(Byte mirIndxIncrMoDur) {
        this.mirIndxIncrMoDur = mirIndxIncrMoDur;
    }

    public Byte getMirIntAdjMaxDur() {
        return mirIntAdjMaxDur;
    }

    public void setMirIntAdjMaxDur(Byte mirIntAdjMaxDur) {
        this.mirIntAdjMaxDur = mirIntAdjMaxDur;
    }

    public String getMirCfIntCalcTypCd() {
        return mirCfIntCalcTypCd;
    }

    public void setMirCfIntCalcTypCd(String mirCfIntCalcTypCd) {
        this.mirCfIntCalcTypCd = mirCfIntCalcTypCd;
    }

    public String getMirCfIntModeCd() {
        return mirCfIntModeCd;
    }

    public void setMirCfIntModeCd(String mirCfIntModeCd) {
        this.mirCfIntModeCd = mirCfIntModeCd;
    }

    public String getMirPlanIntPayoCd() {
        return mirPlanIntPayoCd;
    }

    public void setMirPlanIntPayoCd(String mirPlanIntPayoCd) {
        this.mirPlanIntPayoCd = mirPlanIntPayoCd;
    }

    public Byte getMirIswlCalcFreqDur() {
        return mirIswlCalcFreqDur;
    }

    public void setMirIswlCalcFreqDur(Byte mirIswlCalcFreqDur) {
        this.mirIswlCalcFreqDur = mirIswlCalcFreqDur;
    }

    public Byte getMirIswlCalcAdvDur() {
        return mirIswlCalcAdvDur;
    }

    public void setMirIswlCalcAdvDur(Byte mirIswlCalcAdvDur) {
        this.mirIswlCalcAdvDur = mirIswlCalcAdvDur;
    }

    public String getMirPremRecalcOptCd() {
        return mirPremRecalcOptCd;
    }

    public void setMirPremRecalcOptCd(String mirPremRecalcOptCd) {
        this.mirPremRecalcOptCd = mirPremRecalcOptCd;
    }

    public Float getMirIswlPremVarRt() {
        return mirIswlPremVarRt;
    }

    public void setMirIswlPremVarRt(Float mirIswlPremVarRt) {
        this.mirIswlPremVarRt = mirIswlPremVarRt;
    }

    public Byte getMirIswlFrstCalcDur() {
        return mirIswlFrstCalcDur;
    }

    public void setMirIswlFrstCalcDur(Byte mirIswlFrstCalcDur) {
        this.mirIswlFrstCalcDur = mirIswlFrstCalcDur;
    }

    public String getMirPlanInsTypCd() {
        return mirPlanInsTypCd;
    }

    public void setMirPlanInsTypCd(String mirPlanInsTypCd) {
        this.mirPlanInsTypCd = mirPlanInsTypCd;
    }

    public String getMirPlanColfndInd() {
        return mirPlanColfndInd;
    }

    public void setMirPlanColfndInd(String mirPlanColfndInd) {
        this.mirPlanColfndInd = mirPlanColfndInd;
    }

    public String getMirColfndReqirInd() {
        return mirColfndReqirInd;
    }

    public void setMirColfndReqirInd(String mirColfndReqirInd) {
        this.mirColfndReqirInd = mirColfndReqirInd;
    }

    public String getMirLocGrColctId() {
        return mirLocGrColctId;
    }

    public void setMirLocGrColctId(String mirLocGrColctId) {
        this.mirLocGrColctId = mirLocGrColctId;
    }

    public Byte getMirLoanIntCalcCd() {
        return mirLoanIntCalcCd;
    }

    public void setMirLoanIntCalcCd(Byte mirLoanIntCalcCd) {
        this.mirLoanIntCalcCd = mirLoanIntCalcCd;
    }

    public String getMirPlanImprdRtCd() {
        return mirPlanImprdRtCd;
    }

    public void setMirPlanImprdRtCd(String mirPlanImprdRtCd) {
        this.mirPlanImprdRtCd = mirPlanImprdRtCd;
    }

    public Float getMirPlanImprdIntRt() {
        return mirPlanImprdIntRt;
    }

    public void setMirPlanImprdIntRt(Float mirPlanImprdIntRt) {
        this.mirPlanImprdIntRt = mirPlanImprdIntRt;
    }

    public String getMirLoanIntRtTypCd() {
        return mirLoanIntRtTypCd;
    }

    public void setMirLoanIntRtTypCd(String mirLoanIntRtTypCd) {
        this.mirLoanIntRtTypCd = mirLoanIntRtTypCd;
    }

    public String getMirPlanLockFndInd() {
        return mirPlanLockFndInd;
    }

    public void setMirPlanLockFndInd(String mirPlanLockFndInd) {
        this.mirPlanLockFndInd = mirPlanLockFndInd;
    }

    public Byte getMirPlanLtaXpryAge() {
        return mirPlanLtaXpryAge;
    }

    public void setMirPlanLtaXpryAge(Byte mirPlanLtaXpryAge) {
        this.mirPlanLtaXpryAge = mirPlanLtaXpryAge;
    }

    public String getMirLtaPlanId() {
        return mirLtaPlanId;
    }

    public void setMirLtaPlanId(String mirLtaPlanId) {
        this.mirLtaPlanId = mirLtaPlanId;
    }

    public Byte getMirPlanLtbXpryAge() {
        return mirPlanLtbXpryAge;
    }

    public void setMirPlanLtbXpryAge(Byte mirPlanLtbXpryAge) {
        this.mirPlanLtbXpryAge = mirPlanLtbXpryAge;
    }

    public String getMirLtbPlanId() {
        return mirLtbPlanId;
    }

    public void setMirLtbPlanId(String mirLtbPlanId) {
        this.mirLtbPlanId = mirLtbPlanId;
    }

    public Byte getMirPlanMatAge() {
        return mirPlanMatAge;
    }

    public void setMirPlanMatAge(Byte mirPlanMatAge) {
        this.mirPlanMatAge = mirPlanMatAge;
    }

    public String getMirMatDtCalcCd() {
        return mirMatDtCalcCd;
    }

    public void setMirMatDtCalcCd(String mirMatDtCalcCd) {
        this.mirMatDtCalcCd = mirMatDtCalcCd;
    }

    public String getMirMatChrgRtbl2Id() {
        return mirMatChrgRtbl2Id;
    }

    public void setMirMatChrgRtbl2Id(String mirMatChrgRtbl2Id) {
        this.mirMatChrgRtbl2Id = mirMatChrgRtbl2Id;
    }

    public Byte getMirPlanMatAgeDur() {
        return mirPlanMatAgeDur;
    }

    public void setMirPlanMatAgeDur(Byte mirPlanMatAgeDur) {
        this.mirPlanMatAgeDur = mirPlanMatAgeDur;
    }

    public Float getMirIaCommMaxAmt() {
        return mirIaCommMaxAmt;
    }

    public void setMirIaCommMaxAmt(Float mirIaCommMaxAmt) {
        this.mirIaCommMaxAmt = mirIaCommMaxAmt;
    }

    public Byte getMirEnhcNelctDur() {
        return mirEnhcNelctDur;
    }

    public void setMirEnhcNelctDur(Byte mirEnhcNelctDur) {
        this.mirEnhcNelctDur = mirEnhcNelctDur;
    }

    public String getMirMcvCalcReqirInd() {
        return mirMcvCalcReqirInd;
    }

    public void setMirMcvCalcReqirInd(String mirMcvCalcReqirInd) {
        this.mirMcvCalcReqirInd = mirMcvCalcReqirInd;
    }

    public String getMirMcvIntMthdCd() {
        return mirMcvIntMthdCd;
    }

    public void setMirMcvIntMthdCd(String mirMcvIntMthdCd) {
        this.mirMcvIntMthdCd = mirMcvIntMthdCd;
    }

    public String getMirMcvPmtLoadCd() {
        return mirMcvPmtLoadCd;
    }

    public void setMirMcvPmtLoadCd(String mirMcvPmtLoadCd) {
        this.mirMcvPmtLoadCd = mirMcvPmtLoadCd;
    }

    public Float getMirDirDposMinAmt() {
        return mirDirDposMinAmt;
    }

    public void setMirDirDposMinAmt(Float mirDirDposMinAmt) {
        this.mirDirDposMinAmt = mirDirDposMinAmt;
    }

    public Float getMirPacDposMinAmt() {
        return mirPacDposMinAmt;
    }

    public void setMirPacDposMinAmt(Float mirPacDposMinAmt) {
        this.mirPacDposMinAmt = mirPacDposMinAmt;
    }

    public Float getMirCrcDposMinAmt() {
        return mirCrcDposMinAmt;
    }

    public void setMirCrcDposMinAmt(Float mirCrcDposMinAmt) {
        this.mirCrcDposMinAmt = mirCrcDposMinAmt;
    }

    public Float getMirDd2DposMinAmt() {
        return mirDd2DposMinAmt;
    }

    public void setMirDd2DposMinAmt(Float mirDd2DposMinAmt) {
        this.mirDd2DposMinAmt = mirDd2DposMinAmt;
    }

    public Byte getMirPlanMinMpremQty() {
        return mirPlanMinMpremQty;
    }

    public void setMirPlanMinMpremQty(Byte mirPlanMinMpremQty) {
        this.mirPlanMinMpremQty = mirPlanMinMpremQty;
    }

    public String getMirPlanMdrtClasCd() {
        return mirPlanMdrtClasCd;
    }

    public void setMirPlanMdrtClasCd(String mirPlanMdrtClasCd) {
        this.mirPlanMdrtClasCd = mirPlanMdrtClasCd;
    }

    public Byte getMirMortXpnsChrgAge() {
        return mirMortXpnsChrgAge;
    }

    public void setMirMortXpnsChrgAge(Byte mirMortXpnsChrgAge) {
        this.mirMortXpnsChrgAge = mirMortXpnsChrgAge;
    }

    public String getMirMortXpnsCalcCd() {
        return mirMortXpnsCalcCd;
    }

    public void setMirMortXpnsCalcCd(String mirMortXpnsCalcCd) {
        this.mirMortXpnsCalcCd = mirMortXpnsCalcCd;
    }

    public Byte getMirMortXpnsChrgDur() {
        return mirMortXpnsChrgDur;
    }

    public void setMirMortXpnsChrgDur(Byte mirMortXpnsChrgDur) {
        this.mirMortXpnsChrgDur = mirMortXpnsChrgDur;
    }

    public String getMirPlanRolovrDtInd() {
        return mirPlanRolovrDtInd;
    }

    public void setMirPlanRolovrDtInd(String mirPlanRolovrDtInd) {
        this.mirPlanRolovrDtInd = mirPlanRolovrDtInd;
    }

    public Float getMirMinCaslPayoAmt() {
        return mirMinCaslPayoAmt;
    }

    public void setMirMinCaslPayoAmt(Float mirMinCaslPayoAmt) {
        this.mirMinCaslPayoAmt = mirMinCaslPayoAmt;
    }

    public String getMirMnpmtTrgSstdInd() {
        return mirMnpmtTrgSstdInd;
    }

    public void setMirMnpmtTrgSstdInd(String mirMnpmtTrgSstdInd) {
        this.mirMnpmtTrgSstdInd = mirMnpmtTrgSstdInd;
    }

    public String getMirSurrAdjCalcCd() {
        return mirSurrAdjCalcCd;
    }

    public void setMirSurrAdjCalcCd(String mirSurrAdjCalcCd) {
        this.mirSurrAdjCalcCd = mirSurrAdjCalcCd;
    }

    public Byte getMirSurrAdjMoDur() {
        return mirSurrAdjMoDur;
    }

    public void setMirSurrAdjMoDur(Byte mirSurrAdjMoDur) {
        this.mirSurrAdjMoDur = mirSurrAdjMoDur;
    }

    public String getMirSurrAdjTypCd() {
        return mirSurrAdjTypCd;
    }

    public void setMirSurrAdjTypCd(String mirSurrAdjTypCd) {
        this.mirSurrAdjTypCd = mirSurrAdjTypCd;
    }

    public String getMirPlanMktvalAdjCd() {
        return mirPlanMktvalAdjCd;
    }

    public void setMirPlanMktvalAdjCd(String mirPlanMktvalAdjCd) {
        this.mirPlanMktvalAdjCd = mirPlanMktvalAdjCd;
    }

    public String getMirRpuTbacId() {
        return mirRpuTbacId;
    }

    public void setMirRpuTbacId(String mirRpuTbacId) {
        this.mirRpuTbacId = mirRpuTbacId;
    }

    public Byte getMirNfPrcesMoDur() {
        return mirNfPrcesMoDur;
    }

    public void setMirNfPrcesMoDur(Byte mirNfPrcesMoDur) {
        this.mirNfPrcesMoDur = mirNfPrcesMoDur;
    }

    public Byte getMirNotiLowAgeDur() {
        return mirNotiLowAgeDur;
    }

    public void setMirNotiLowAgeDur(Byte mirNotiLowAgeDur) {
        this.mirNotiLowAgeDur = mirNotiLowAgeDur;
    }

    public String getMirNotiDeptId() {
        return mirNotiDeptId;
    }

    public void setMirNotiDeptId(String mirNotiDeptId) {
        this.mirNotiDeptId = mirNotiDeptId;
    }

    public Byte getMirNotiDtCalcCd() {
        return mirNotiDtCalcCd;
    }

    public void setMirNotiDtCalcCd(Byte mirNotiDtCalcCd) {
        this.mirNotiDtCalcCd = mirNotiDtCalcCd;
    }

    public Byte getMirNotiHighAgeDur() {
        return mirNotiHighAgeDur;
    }

    public void setMirNotiHighAgeDur(Byte mirNotiHighAgeDur) {
        this.mirNotiHighAgeDur = mirNotiHighAgeDur;
    }

    public Byte getMirNumGirOptQty() {
        return mirNumGirOptQty;
    }

    public void setMirNumGirOptQty(Byte mirNumGirOptQty) {
        this.mirNumGirOptQty = mirNumGirOptQty;
    }

    public Byte getMirOwnOccpXpryAge() {
        return mirOwnOccpXpryAge;
    }

    public void setMirOwnOccpXpryAge(Byte mirOwnOccpXpryAge) {
        this.mirOwnOccpXpryAge = mirOwnOccpXpryAge;
    }

    public String getMirOwnOccpPlanId() {
        return mirOwnOccpPlanId;
    }

    public void setMirOwnOccpPlanId(String mirOwnOccpPlanId) {
        this.mirOwnOccpPlanId = mirOwnOccpPlanId;
    }

    public String getMirDivOytTbacId() {
        return mirDivOytTbacId;
    }

    public void setMirDivOytTbacId(String mirDivOytTbacId) {
        this.mirDivOytTbacId = mirDivOytTbacId;
    }

    public Float getMirPmtLoadFlatAmt() {
        return mirPmtLoadFlatAmt;
    }

    public void setMirPmtLoadFlatAmt(Float mirPmtLoadFlatAmt) {
        this.mirPmtLoadFlatAmt = mirPmtLoadFlatAmt;
    }

    public String getMirPmtLoadOrdrCd() {
        return mirPmtLoadOrdrCd;
    }

    public void setMirPmtLoadOrdrCd(String mirPmtLoadOrdrCd) {
        this.mirPmtLoadOrdrCd = mirPmtLoadOrdrCd;
    }

    public String getMirPmtLoadRtbl2Id() {
        return mirPmtLoadRtbl2Id;
    }

    public void setMirPmtLoadRtbl2Id(String mirPmtLoadRtbl2Id) {
        this.mirPmtLoadRtbl2Id = mirPmtLoadRtbl2Id;
    }

    public Float getMirPlanMinPuAmt() {
        return mirPlanMinPuAmt;
    }

    public void setMirPlanMinPuAmt(Float mirPlanMinPuAmt) {
        this.mirPlanMinPuAmt = mirPlanMinPuAmt;
    }

    public String getMirMthvPrcesInd() {
        return mirMthvPrcesInd;
    }

    public void setMirMthvPrcesInd(String mirMthvPrcesInd) {
        this.mirMthvPrcesInd = mirMthvPrcesInd;
    }

    public String getMirCommPlanTypCd() {
        return mirCommPlanTypCd;
    }

    public void setMirCommPlanTypCd(String mirCommPlanTypCd) {
        this.mirCommPlanTypCd = mirCommPlanTypCd;
    }

    public Byte getMirPdisabXpryAge() {
        return mirPdisabXpryAge;
    }

    public void setMirPdisabXpryAge(Byte mirPdisabXpryAge) {
        this.mirPdisabXpryAge = mirPdisabXpryAge;
    }

    public String getMirPdisabPlanId() {
        return mirPdisabPlanId;
    }

    public void setMirPdisabPlanId(String mirPdisabPlanId) {
        this.mirPdisabPlanId = mirPdisabPlanId;
    }

    public String getMirCvAllocMthdCd() {
        return mirCvAllocMthdCd;
    }

    public void setMirCvAllocMthdCd(String mirCvAllocMthdCd) {
        this.mirCvAllocMthdCd = mirCvAllocMthdCd;
    }

    public Float getMirPlanPfeeAmt() {
        return mirPlanPfeeAmt;
    }

    public void setMirPlanPfeeAmt(Float mirPlanPfeeAmt) {
        this.mirPlanPfeeAmt = mirPlanPfeeAmt;
    }

    public Float getMirPlanIaPfeeAmt() {
        return mirPlanIaPfeeAmt;
    }

    public void setMirPlanIaPfeeAmt(Float mirPlanIaPfeeAmt) {
        this.mirPlanIaPfeeAmt = mirPlanIaPfeeAmt;
    }

    public Float getMirPlanCvgPfeeAmt() {
        return mirPlanCvgPfeeAmt;
    }

    public void setMirPlanCvgPfeeAmt(Float mirPlanCvgPfeeAmt) {
        this.mirPlanCvgPfeeAmt = mirPlanCvgPfeeAmt;
    }

    public String getMirPnsnQualfInd() {
        return mirPnsnQualfInd;
    }

    public void setMirPnsnQualfInd(String mirPnsnQualfInd) {
        this.mirPnsnQualfInd = mirPnsnQualfInd;
    }

    public String getMirPremChngDtCd() {
        return mirPremChngDtCd;
    }

    public void setMirPremChngDtCd(String mirPremChngDtCd) {
        this.mirPremChngDtCd = mirPremChngDtCd;
    }

    public String getMirPlanLocLicCd() {
        return mirPlanLocLicCd;
    }

    public void setMirPlanLocLicCd(String mirPlanLocLicCd) {
        this.mirPlanLocLicCd = mirPlanLocLicCd;
    }

    public Float getMirTrmPremRfndRt() {
        return mirTrmPremRfndRt;
    }

    public void setMirTrmPremRfndRt(Float mirTrmPremRfndRt) {
        this.mirTrmPremRfndRt = mirTrmPremRfndRt;
    }

    public Byte getMirPeriStmtDyDur() {
        return mirPeriStmtDyDur;
    }

    public void setMirPeriStmtDyDur(Byte mirPeriStmtDyDur) {
        this.mirPeriStmtDyDur = mirPeriStmtDyDur;
    }

    public Byte getMirPeriStmtMoDur() {
        return mirPeriStmtMoDur;
    }

    public void setMirPeriStmtMoDur(Byte mirPeriStmtMoDur) {
        this.mirPeriStmtMoDur = mirPeriStmtMoDur;
    }

    public String getMirPeriStmtTypCd() {
        return mirPeriStmtTypCd;
    }

    public void setMirPeriStmtTypCd(String mirPeriStmtTypCd) {
        this.mirPeriStmtTypCd = mirPeriStmtTypCd;
    }

    public Float getMirPsurChrgFlatAmt() {
        return mirPsurChrgFlatAmt;
    }

    public void setMirPsurChrgFlatAmt(Float mirPsurChrgFlatAmt) {
        this.mirPsurChrgFlatAmt = mirPsurChrgFlatAmt;
    }

    public String getMirPsurChrgRtbl2Id() {
        return mirPsurChrgRtbl2Id;
    }

    public void setMirPsurChrgRtbl2Id(String mirPsurChrgRtbl2Id) {
        this.mirPsurChrgRtbl2Id = mirPsurChrgRtbl2Id;
    }

    public String getMirPsurChrgTypCd() {
        return mirPsurChrgTypCd;
    }

    public void setMirPsurChrgTypCd(String mirPsurChrgTypCd) {
        this.mirPsurChrgTypCd = mirPsurChrgTypCd;
    }

    public String getMirPuaDivCalcCd() {
        return mirPuaDivCalcCd;
    }

    public void setMirPuaDivCalcCd(String mirPuaDivCalcCd) {
        this.mirPuaDivCalcCd = mirPuaDivCalcCd;
    }

    public String getMirDivPuaTbacId() {
        return mirDivPuaTbacId;
    }

    public void setMirDivPuaTbacId(String mirDivPuaTbacId) {
        this.mirDivPuaTbacId = mirDivPuaTbacId;
    }

    public String getMirPmtLoadSstdInd() {
        return mirPmtLoadSstdInd;
    }

    public void setMirPmtLoadSstdInd(String mirPmtLoadSstdInd) {
        this.mirPmtLoadSstdInd = mirPmtLoadSstdInd;
    }

    public String getMirPlanRegInd() {
        return mirPlanRegInd;
    }

    public void setMirPlanRegInd(String mirPlanRegInd) {
        this.mirPlanRegInd = mirPlanRegInd;
    }

    public String getMirCommRtLoopInd() {
        return mirCommRtLoopInd;
    }

    public void setMirCommRtLoopInd(String mirCommRtLoopInd) {
        this.mirCommRtLoopInd = mirCommRtLoopInd;
    }

    public String getMirPremRenwRtCd() {
        return mirPremRenwRtCd;
    }

    public void setMirPremRenwRtCd(String mirPremRenwRtCd) {
        this.mirPremRenwRtCd = mirPremRenwRtCd;
    }

    public Byte getMirPlanCvgXpryAge() {
        return mirPlanCvgXpryAge;
    }

    public void setMirPlanCvgXpryAge(Byte mirPlanCvgXpryAge) {
        this.mirPlanCvgXpryAge = mirPlanCvgXpryAge;
    }

    public Byte getMirCvgXpryAgeDur() {
        return mirCvgXpryAgeDur;
    }

    public void setMirCvgXpryAgeDur(Byte mirCvgXpryAgeDur) {
        this.mirCvgXpryAgeDur = mirCvgXpryAgeDur;
    }

    public String getMirCommRhSubCd() {
        return mirCommRhSubCd;
    }

    public void setMirCommRhSubCd(String mirCommRhSubCd) {
        this.mirCommRhSubCd = mirCommRhSubCd;
    }

    public Float getMirPlanNarDscntRt() {
        return mirPlanNarDscntRt;
    }

    public void setMirPlanNarDscntRt(Float mirPlanNarDscntRt) {
        this.mirPlanNarDscntRt = mirPlanNarDscntRt;
    }

    public Byte getMirPlanRsrvCalcCd() {
        return mirPlanRsrvCalcCd;
    }

    public void setMirPlanRsrvCalcCd(Byte mirPlanRsrvCalcCd) {
        this.mirPlanRsrvCalcCd = mirPlanRsrvCalcCd;
    }

    public String getMirSeGdlnApremInd() {
        return mirSeGdlnApremInd;
    }

    public void setMirSeGdlnApremInd(String mirSeGdlnApremInd) {
        this.mirSeGdlnApremInd = mirSeGdlnApremInd;
    }

    public Byte getMirAgeStbckStrtAge() {
        return mirAgeStbckStrtAge;
    }

    public void setMirAgeStbckStrtAge(Byte mirAgeStbckStrtAge) {
        this.mirAgeStbckStrtAge = mirAgeStbckStrtAge;
    }

    public Byte getMirPlanAgeStbckQty() {
        return mirPlanAgeStbckQty;
    }

    public void setMirPlanAgeStbckQty(Byte mirPlanAgeStbckQty) {
        this.mirPlanAgeStbckQty = mirPlanAgeStbckQty;
    }

    public String getMirSfbNegSuspInd() {
        return mirSfbNegSuspInd;
    }

    public void setMirSfbNegSuspInd(String mirSfbNegSuspInd) {
        this.mirSfbNegSuspInd = mirSfbNegSuspInd;
    }

    public Byte getMirSfbNegSuspQty() {
        return mirSfbNegSuspQty;
    }

    public void setMirSfbNegSuspQty(Byte mirSfbNegSuspQty) {
        this.mirSfbNegSuspQty = mirSfbNegSuspQty;
    }

    public Byte getMirSumInsCalcCd() {
        return mirSumInsCalcCd;
    }

    public void setMirSumInsCalcCd(Byte mirSumInsCalcCd) {
        this.mirSumInsCalcCd = mirSumInsCalcCd;
    }

    public Byte getMirMajLnSortCd() {
        return mirMajLnSortCd;
    }

    public void setMirMajLnSortCd(Byte mirMajLnSortCd) {
        this.mirMajLnSortCd = mirMajLnSortCd;
    }

    public String getMirPlanSuppBnftCd() {
        return mirPlanSuppBnftCd;
    }

    public void setMirPlanSuppBnftCd(String mirPlanSuppBnftCd) {
        this.mirPlanSuppBnftCd = mirPlanSuppBnftCd;
    }

    public String getMirSurrChrgCalcCd() {
        return mirSurrChrgCalcCd;
    }

    public void setMirSurrChrgCalcCd(String mirSurrChrgCalcCd) {
        this.mirSurrChrgCalcCd = mirSurrChrgCalcCd;
    }

    public String getMirDposWthdrOrdrCd() {
        return mirDposWthdrOrdrCd;
    }

    public void setMirDposWthdrOrdrCd(String mirDposWthdrOrdrCd) {
        this.mirDposWthdrOrdrCd = mirDposWthdrOrdrCd;
    }

    public String getMirSurrTrgSstdInd() {
        return mirSurrTrgSstdInd;
    }

    public void setMirSurrTrgSstdInd(String mirSurrTrgSstdInd) {
        this.mirSurrTrgSstdInd = mirSurrTrgSstdInd;
    }

    public String getMirT5RegTitlTxt() {
        return mirT5RegTitlTxt;
    }

    public void setMirT5RegTitlTxt(String mirT5RegTitlTxt) {
        this.mirT5RegTitlTxt = mirT5RegTitlTxt;
    }

    public String getMirPlanTamraCd() {
        return mirPlanTamraCd;
    }

    public void setMirPlanTamraCd(String mirPlanTamraCd) {
        this.mirPlanTamraCd = mirPlanTamraCd;
    }

    public Float getMirDiaSwpThrshdAmt() {
        return mirDiaSwpThrshdAmt;
    }

    public void setMirDiaSwpThrshdAmt(Float mirDiaSwpThrshdAmt) {
        this.mirDiaSwpThrshdAmt = mirDiaSwpThrshdAmt;
    }

    public String getMirAfrThrshdPeriCd() {
        return mirAfrThrshdPeriCd;
    }

    public void setMirAfrThrshdPeriCd(String mirAfrThrshdPeriCd) {
        this.mirAfrThrshdPeriCd = mirAfrThrshdPeriCd;
    }

    public String getMirTrailCommFreqCd() {
        return mirTrailCommFreqCd;
    }

    public void setMirTrailCommFreqCd(String mirTrailCommFreqCd) {
        this.mirTrailCommFreqCd = mirTrailCommFreqCd;
    }

    public Byte getMirTrailCommLagDur() {
        return mirTrailCommLagDur;
    }

    public void setMirTrailCommLagDur(Byte mirTrailCommLagDur) {
        this.mirTrailCommLagDur = mirTrailCommLagDur;
    }

    public String getMirPlanTxempCd() {
        return mirPlanTxempCd;
    }

    public void setMirPlanTxempCd(String mirPlanTxempCd) {
        this.mirPlanTxempCd = mirPlanTxempCd;
    }

    public String getMirGainRptTimeCd() {
        return mirGainRptTimeCd;
    }

    public void setMirGainRptTimeCd(String mirGainRptTimeCd) {
        this.mirGainRptTimeCd = mirGainRptTimeCd;
    }

    public String getMirPlanTaxwhInd() {
        return mirPlanTaxwhInd;
    }

    public void setMirPlanTaxwhInd(String mirPlanTaxwhInd) {
        this.mirPlanTaxwhInd = mirPlanTaxwhInd;
    }

    public Short getMirPlanUnitValuAmt() {
        return mirPlanUnitValuAmt;
    }

    public void setMirPlanUnitValuAmt(Short mirPlanUnitValuAmt) {
        this.mirPlanUnitValuAmt = mirPlanUnitValuAmt;
    }

    public Float getMirUwgFaceAmtFct() {
        return mirUwgFaceAmtFct;
    }

    public void setMirUwgFaceAmtFct(Float mirUwgFaceAmtFct) {
        this.mirUwgFaceAmtFct = mirUwgFaceAmtFct;
    }

    public String getMirPlanUwgInd() {
        return mirPlanUwgInd;
    }

    public void setMirPlanUwgInd(String mirPlanUwgInd) {
        this.mirPlanUwgInd = mirPlanUwgInd;
    }

    public String getMirPlanVpoInd() {
        return mirPlanVpoInd;
    }

    public void setMirPlanVpoInd(String mirPlanVpoInd) {
        this.mirPlanVpoInd = mirPlanVpoInd;
    }

    public Byte getMirPlanWpXpryAge() {
        return mirPlanWpXpryAge;
    }

    public void setMirPlanWpXpryAge(Byte mirPlanWpXpryAge) {
        this.mirPlanWpXpryAge = mirPlanWpXpryAge;
    }

    public String getMirWpPlanId() {
        return mirWpPlanId;
    }

    public void setMirWpPlanId(String mirWpPlanId) {
        this.mirWpPlanId = mirWpPlanId;
    }

    public String getMirPlanWthdrOrdrCd() {
        return mirPlanWthdrOrdrCd;
    }

    public void setMirPlanWthdrOrdrCd(String mirPlanWthdrOrdrCd) {
        this.mirPlanWthdrOrdrCd = mirPlanWthdrOrdrCd;
    }

    public String getMirPlanWpXpryCd() {
        return mirPlanWpXpryCd;
    }

    public void setMirPlanWpXpryCd(String mirPlanWpXpryCd) {
        this.mirPlanWpXpryCd = mirPlanWpXpryCd;
    }

    public String getMirPlanBonCalcCd() {
        return mirPlanBonCalcCd;
    }

    public void setMirPlanBonCalcCd(String mirPlanBonCalcCd) {
        this.mirPlanBonCalcCd = mirPlanBonCalcCd;
    }

    public String getMirPlanInitPmtInd() {
        return mirPlanInitPmtInd;
    }

    public void setMirPlanInitPmtInd(String mirPlanInitPmtInd) {
        this.mirPlanInitPmtInd = mirPlanInitPmtInd;
    }

    public String getMirPlanFndTypCd() {
        return mirPlanFndTypCd;
    }

    public void setMirPlanFndTypCd(String mirPlanFndTypCd) {
        this.mirPlanFndTypCd = mirPlanFndTypCd;
    }

    public String getMirPlanMultCrcyInd() {
        return mirPlanMultCrcyInd;
    }

    public void setMirPlanMultCrcyInd(String mirPlanMultCrcyInd) {
        this.mirPlanMultCrcyInd = mirPlanMultCrcyInd;
    }

    public String getMirPlanCrcyCd() {
        return mirPlanCrcyCd;
    }

    public void setMirPlanCrcyCd(String mirPlanCrcyCd) {
        this.mirPlanCrcyCd = mirPlanCrcyCd;
    }

    public Byte getMirPlanUnitTypCd() {
        return mirPlanUnitTypCd;
    }

    public void setMirPlanUnitTypCd(Byte mirPlanUnitTypCd) {
        this.mirPlanUnitTypCd = mirPlanUnitTypCd;
    }

    public String getMirPlanInclDbgInd() {
        return mirPlanInclDbgInd;
    }

    public void setMirPlanInclDbgInd(String mirPlanInclDbgInd) {
        this.mirPlanInclDbgInd = mirPlanInclDbgInd;
    }

    public String getMirPlanOffannvInd() {
        return mirPlanOffannvInd;
    }

    public void setMirPlanOffannvInd(String mirPlanOffannvInd) {
        this.mirPlanOffannvInd = mirPlanOffannvInd;
    }

    public String getMirPlanDbOptCd() {
        return mirPlanDbOptCd;
    }

    public void setMirPlanDbOptCd(String mirPlanDbOptCd) {
        this.mirPlanDbOptCd = mirPlanDbOptCd;
    }

    public String getMirRpuFaceCalcCd() {
        return mirRpuFaceCalcCd;
    }

    public void setMirRpuFaceCalcCd(String mirRpuFaceCalcCd) {
        this.mirRpuFaceCalcCd = mirRpuFaceCalcCd;
    }

    public Byte getMirBnftCatTyp() {
        return mirBnftCatTyp;
    }

    public void setMirBnftCatTyp(Byte mirBnftCatTyp) {
        this.mirBnftCatTyp = mirBnftCatTyp;
    }

    public Byte getMirMaxLmtChkInd() {
        return mirMaxLmtChkInd;
    }

    public void setMirMaxLmtChkInd(Byte mirMaxLmtChkInd) {
        this.mirMaxLmtChkInd = mirMaxLmtChkInd;
    }

    public String getMirPoOptInd() {
        return mirPoOptInd;
    }

    public void setMirPoOptInd(String mirPoOptInd) {
        this.mirPoOptInd = mirPoOptInd;
    }

    public String getMirPlanBtchSetlInd() {
        return mirPlanBtchSetlInd;
    }

    public void setMirPlanBtchSetlInd(String mirPlanBtchSetlInd) {
        this.mirPlanBtchSetlInd = mirPlanBtchSetlInd;
    }

    public String getMirExcessPymtInd() {
        return mirExcessPymtInd;
    }

    public void setMirExcessPymtInd(String mirExcessPymtInd) {
        this.mirExcessPymtInd = mirExcessPymtInd;
    }
}
