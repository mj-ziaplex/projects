package ph.com.sunlife.ingenium.domain;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

@XStreamAlias("MirDvIpartyCliIdG")
public class MirDvIpartyCliIdG {

    @XStreamImplicit(itemFieldName = "MirDvIpartyCliIdT")
    private List<String> mirDvIpartyCliIdT;

    public List<String> getMirDvIpartyCliIdT() {
        return mirDvIpartyCliIdT;
    }

    public void setMirDvIpartyCliIdT(List<String> mirDvIpartyCliIdT) {
        this.mirDvIpartyCliIdT = mirDvIpartyCliIdT;
    }
}
