package ph.com.sunlife.zold.ws.ingenium.soap;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface TXLifeServicePort extends Remote {

    String callTXLife(String request) throws RemoteException;
}