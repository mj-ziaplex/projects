package ph.com.sunlife.ingenium.domain;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

@XStreamAlias("MirDvAgtCliNmG")
public class MirDvAgtCliNmG {

    @XStreamImplicit(itemFieldName = "MirDvAgtCliNmT")
    private List<String> mirDvAgtCliNmT;

    public List<String> getMirDvAgtCliNmT() {
        return mirDvAgtCliNmT;
    }

    public void setMirDvAgtCliNmT(List<String> mirDvAgtCliNmT) {
        this.mirDvAgtCliNmT = mirDvAgtCliNmT;
    }
}
