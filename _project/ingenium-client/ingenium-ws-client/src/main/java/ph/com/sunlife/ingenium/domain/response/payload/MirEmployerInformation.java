package ph.com.sunlife.ingenium.domain.response.payload;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("MirEmplrInfo")
public class MirEmployerInformation {

    @XStreamAlias("MirDvEmplrCliCoNmG")
    private MirDvEmployerClientCompanyNameGroup mirDvEmployerClientCompanyNameGroup;
    @XStreamAlias("MirDvEmplrAddr1TxtG")
    private MirDvEmployerAddress1TextGroup mirDvEmployerAddress1TextGroup;
    @XStreamAlias("MirDvEmplrAddr2TxtG")
    private MirDvEmployerAddress2TextGroup mirDvEmployerAddress2TextGroup;
    @XStreamAlias("MirDvEmplrAddr3TxtG")
    private MirDvEmployerAddress3TextGroup mirDvEmployerAddress3TextGroup;
    @XStreamAlias("MirDvEmplrCtryCdG")
    private MirDvEmployerCountryCodeGroup mirDvEmployerCountryCodeGroup;
    @XStreamAlias("MirCliEmplrLocCdG")
    private MirClientEmployerLocationCodeGroup mirClientEmployerLocationCodeGroup;
    @XStreamAlias("MirCliEmplYrDurG")
    private MirClientEmployeeYearDurationGroup mirClientEmployeeYearDurationGroup;
    @XStreamAlias("MirDvEmplrAddrGrCd")
    private String mirDvEmployerAddressGrCode;
    @XStreamAlias("MirCliCoGrCd")
    private String mirClientCompanyGrCode;
    @XStreamAlias("MirCliCompCntctNmG")
    private MirClientCompanyContactNameGroup mirClientCompanyContactNameGroup;
    @XStreamAlias("MirCliCompCntctDsgnG")
    private MirCliCompCntctDsgnGroup mirCliCompCntctDsgnGroup;
    @XStreamAlias("MirDvEmplrCityCdG")
    private MirDvEmployerCityCodeGroup mirDvEmployerCityCodeGroup;
    @XStreamAlias("MirDvEmplrPstlCdG")
    private MirDvEmployerPostalCodeGroup mirDvEmployerPostalCodeGroup;
    @XStreamAlias("MirDvEmplrAddrEffDtG")
    private MirDvEmployerAddressEffectivityDateGroup mirDvEmployerAddressEffectivityDateGroup;

    public MirDvEmployerClientCompanyNameGroup getMirDvEmployerClientCompanyNameGroup() {
        return mirDvEmployerClientCompanyNameGroup;
    }

    public void setMirDvEmployerClientCompanyNameGroup(MirDvEmployerClientCompanyNameGroup mirDvEmployerClientCompanyNameGroup) {
        this.mirDvEmployerClientCompanyNameGroup = mirDvEmployerClientCompanyNameGroup;
    }

    public MirDvEmployerAddress1TextGroup getMirDvEmployerAddress1TextGroup() {
        return mirDvEmployerAddress1TextGroup;
    }

    public void setMirDvEmployerAddress1TextGroup(MirDvEmployerAddress1TextGroup mirDvEmployerAddress1TextGroup) {
        this.mirDvEmployerAddress1TextGroup = mirDvEmployerAddress1TextGroup;
    }

    public MirDvEmployerAddress2TextGroup getMirDvEmployerAddress2TextGroup() {
        return mirDvEmployerAddress2TextGroup;
    }

    public void setMirDvEmployerAddress2TextGroup(MirDvEmployerAddress2TextGroup mirDvEmployerAddress2TextGroup) {
        this.mirDvEmployerAddress2TextGroup = mirDvEmployerAddress2TextGroup;
    }

    public MirDvEmployerAddress3TextGroup getMirDvEmployerAddress3TextGroup() {
        return mirDvEmployerAddress3TextGroup;
    }

    public void setMirDvEmployerAddress3TextGroup(MirDvEmployerAddress3TextGroup mirDvEmployerAddress3TextGroup) {
        this.mirDvEmployerAddress3TextGroup = mirDvEmployerAddress3TextGroup;
    }

    public MirDvEmployerCountryCodeGroup getMirDvEmployerCountryCodeGroup() {
        return mirDvEmployerCountryCodeGroup;
    }

    public void setMirDvEmployerCountryCodeGroup(MirDvEmployerCountryCodeGroup mirDvEmployerCountryCodeGroup) {
        this.mirDvEmployerCountryCodeGroup = mirDvEmployerCountryCodeGroup;
    }

    public MirClientEmployerLocationCodeGroup getMirClientEmployerLocationCodeGroup() {
        return mirClientEmployerLocationCodeGroup;
    }

    public void setMirClientEmployerLocationCodeGroup(MirClientEmployerLocationCodeGroup mirClientEmployerLocationCodeGroup) {
        this.mirClientEmployerLocationCodeGroup = mirClientEmployerLocationCodeGroup;
    }

    public MirClientEmployeeYearDurationGroup getMirClientEmployeeYearDurationGroup() {
        return mirClientEmployeeYearDurationGroup;
    }

    public void setMirClientEmployeeYearDurationGroup(MirClientEmployeeYearDurationGroup mirClientEmployeeYearDurationGroup) {
        this.mirClientEmployeeYearDurationGroup = mirClientEmployeeYearDurationGroup;
    }

    public String getMirDvEmployerAddressGrCode() {
        return mirDvEmployerAddressGrCode;
    }

    public void setMirDvEmployerAddressGrCode(String mirDvEmployerAddressGrCode) {
        this.mirDvEmployerAddressGrCode = mirDvEmployerAddressGrCode;
    }

    public String getMirClientCompanyGrCode() {
        return mirClientCompanyGrCode;
    }

    public void setMirClientCompanyGrCode(String mirClientCompanyGrCode) {
        this.mirClientCompanyGrCode = mirClientCompanyGrCode;
    }

    public MirClientCompanyContactNameGroup getMirClientCompanyContactNameGroup() {
        return mirClientCompanyContactNameGroup;
    }

    public void setMirClientCompanyContactNameGroup(MirClientCompanyContactNameGroup mirClientCompanyContactNameGroup) {
        this.mirClientCompanyContactNameGroup = mirClientCompanyContactNameGroup;
    }

    public MirCliCompCntctDsgnGroup getMirCliCompCntctDsgnGroup() {
        return mirCliCompCntctDsgnGroup;
    }

    public void setMirCliCompCntctDsgnGroup(MirCliCompCntctDsgnGroup mirCliCompCntctDsgnGroup) {
        this.mirCliCompCntctDsgnGroup = mirCliCompCntctDsgnGroup;
    }

    public MirDvEmployerCityCodeGroup getMirDvEmployerCityCodeGroup() {
        return mirDvEmployerCityCodeGroup;
    }

    public void setMirDvEmployerCityCodeGroup(MirDvEmployerCityCodeGroup mirDvEmployerCityCodeGroup) {
        this.mirDvEmployerCityCodeGroup = mirDvEmployerCityCodeGroup;
    }

    public MirDvEmployerPostalCodeGroup getMirDvEmployerPostalCodeGroup() {
        return mirDvEmployerPostalCodeGroup;
    }

    public void setMirDvEmployerPostalCodeGroup(MirDvEmployerPostalCodeGroup mirDvEmployerPostalCodeGroup) {
        this.mirDvEmployerPostalCodeGroup = mirDvEmployerPostalCodeGroup;
    }

    public MirDvEmployerAddressEffectivityDateGroup getMirDvEmployerAddressEffectivityDateGroup() {
        return mirDvEmployerAddressEffectivityDateGroup;
    }

    public void setMirDvEmployerAddressEffectivityDateGroup(MirDvEmployerAddressEffectivityDateGroup mirDvEmployerAddressEffectivityDateGroup) {
        this.mirDvEmployerAddressEffectivityDateGroup = mirDvEmployerAddressEffectivityDateGroup;
    }
}
