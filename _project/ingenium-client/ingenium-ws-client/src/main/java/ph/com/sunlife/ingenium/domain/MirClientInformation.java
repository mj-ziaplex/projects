package ph.com.sunlife.ingenium.domain;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("MirCliInfo")
public class MirClientInformation {

    @XStreamAlias("MirCliId")
    private String id;
    @XStreamAlias("MirDvCliNm")
    private String mirDvCliNm;
    @XStreamAlias("MirCliLangCd")
    private String languageCode;
    @XStreamAlias("MirCliCnfdInd")
    private String mirCliCnfdInd;
    @XStreamAlias("MirCliChrtyInd")
    private String mirCliChrtyInd;
    @XStreamAlias("MirCliBthDt")
    private String birthdate;
    @XStreamAlias("MirCliBthLocCd")
    private String mirCliBthLocCd;
    @XStreamAlias("MirCliAgeProofInd")
    private String mirCliAgeProofInd;
    @XStreamAlias("MirUnmtchMailInd")
    private String mirUnmtchMailInd;
    @XStreamAlias("MirCliUwgdecnTypCd")
    private String mirCliUwgdecnTypCd;
    @XStreamAlias("MirCliUwgdecnCd")
    private String mirCliUwgdecnCd;
    @XStreamAlias("MirCliUwgdecnDt")
    private String mirCliUwgdecnDt;
    @XStreamAlias("MirUwgWrkshtNum")
    private Byte mirUwgWrkshtNum;
    @XStreamAlias("MirCliTxempCd")
    private String mirCliTxempCd;
    @XStreamAlias("MirCliCmpltCcasInd")
    private String mirCliCmpltCcasInd;
    @XStreamAlias("MirCliSuspAmt")
    private Float mirCliSuspAmt;
    @XStreamAlias("MirCliPrevDclnInd")
    private String mirCliPrevDclnInd;
    @XStreamAlias("MirCliLegitDupInd")
    private String mirCliLegitDupInd;
    @XStreamAlias("MirCliWrkQty")
    private Float mirCliWrkQty;
    @XStreamAlias("MirCliMaritStatCd")
    private String mirCliMaritStatCd;
    @XStreamAlias("MirCliMibIndCd")
    private String mirCliMibIndCd;
    @XStreamAlias("MirDvPrevUpdtDt")
    private String  mirDvPrevUpdtDt;
    @XStreamAlias("MirOccpId")
    private String mirOccpId;
    @XStreamAlias("MirCliOccpClasCd")
    private String mirCliOccpClasCd;
    @XStreamAlias("MirCliPrstRt")
    private Byte mirCliPrstRt;
    @XStreamAlias("MirCliSexCd")
    private String sexCode;
    @XStreamAlias("MirCliTaxId")
    private String mirCliTaxId;
    @XStreamAlias("MirCliSmkrCd")
    private String mirCliSmkrCd;
    @XStreamAlias("MirCliIndvGrCd")
    private String mirCliIndvGrCd;
    @XStreamAlias("MirCliSssGsis")
    private String mirCliSssGsis;
    @XStreamAlias("MirCliNatureOfBus")
    private String mirCliNatureOfBus;
    @XStreamAlias("MirCliCtznshipCd")
    private String mirCliCtznshipCd;
    @XStreamAlias("MirGovtPosInd")
    private String mirGovtPosInd;
    @XStreamAlias("MirUSCtznInd")
    private String mirUSCtznInd;
    @XStreamAlias("MirUSTaxResPRInd")
    private String mirUSTaxResPRInd;
    @XStreamAlias("MirUSTaxResSPInd")
    private String mirUSTaxResSPInd;
    @XStreamAlias("MirCliDutyDetlTxt")
    private String mirCliDutyDetlTxt;
    @XStreamAlias("MirRcvMktngInfoInd")
    private String mirRcvMktngInfoInd;

    public MirClientInformation() { }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMirDvCliNm() {
        return mirDvCliNm;
    }

    public void setMirDvCliNm(String mirDvCliNm) {
        this.mirDvCliNm = mirDvCliNm;
    }

    public String getLanguageCode() {
        return languageCode;
    }

    public void setLanguageCode(String languageCode) {
        this.languageCode = languageCode;
    }

    public String getMirCliCnfdInd() {
        return mirCliCnfdInd;
    }

    public void setMirCliCnfdInd(String mirCliCnfdInd) {
        this.mirCliCnfdInd = mirCliCnfdInd;
    }

    public String getMirCliChrtyInd() {
        return mirCliChrtyInd;
    }

    public void setMirCliChrtyInd(String mirCliChrtyInd) {
        this.mirCliChrtyInd = mirCliChrtyInd;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(final String birthdate) {
        this.birthdate = birthdate;
    }

    public String getMirCliBthLocCd() {
        return mirCliBthLocCd;
    }

    public void setMirCliBthLocCd(String mirCliBthLocCd) {
        this.mirCliBthLocCd = mirCliBthLocCd;
    }

    public String getMirCliAgeProofInd() {
        return mirCliAgeProofInd;
    }

    public void setMirCliAgeProofInd(String mirCliAgeProofInd) {
        this.mirCliAgeProofInd = mirCliAgeProofInd;
    }

    public String getMirUnmtchMailInd() {
        return mirUnmtchMailInd;
    }

    public void setMirUnmtchMailInd(String mirUnmtchMailInd) {
        this.mirUnmtchMailInd = mirUnmtchMailInd;
    }

    public byte getMirUwgWrkshtNum() {
        return mirUwgWrkshtNum;
    }

    public void setMirUwgWrkshtNum(byte mirUwgWrkshtNum) {
        this.mirUwgWrkshtNum = mirUwgWrkshtNum;
    }

    public String getMirCliTxempCd() {
        return mirCliTxempCd;
    }

    public void setMirCliTxempCd(String mirCliTxempCd) {
        this.mirCliTxempCd = mirCliTxempCd;
    }

    public String getMirCliUwgdecnTypCd() {
        return mirCliUwgdecnTypCd;
    }

    public void setMirCliUwgdecnTypCd(String mirCliUwgdecnTypCd) {
        this.mirCliUwgdecnTypCd = mirCliUwgdecnTypCd;
    }

    public String getMirCliUwgdecnCd() {
        return mirCliUwgdecnCd;
    }

    public void setMirCliUwgdecnCd(String mirCliUwgdecnCd) {
        this.mirCliUwgdecnCd = mirCliUwgdecnCd;
    }

    public String getMirCliUwgdecnDt() {
        return mirCliUwgdecnDt;
    }

    public void setMirCliUwgdecnDt(String mirCliUwgdecnDt) {
        this.mirCliUwgdecnDt = mirCliUwgdecnDt;
    }

    public String getMirCliCmpltCcasInd() {
        return mirCliCmpltCcasInd;
    }

    public void setMirCliCmpltCcasInd(String mirCliCmpltCcasInd) {
        this.mirCliCmpltCcasInd = mirCliCmpltCcasInd;
    }

    public Float getMirCliSuspAmt() {
        return mirCliSuspAmt;
    }

    public void setMirCliSuspAmt(Float mirCliSuspAmt) {
        this.mirCliSuspAmt = mirCliSuspAmt;
    }

    public String getMirCliPrevDclnInd() {
        return mirCliPrevDclnInd;
    }

    public void setMirCliPrevDclnInd(String mirCliPrevDclnInd) {
        this.mirCliPrevDclnInd = mirCliPrevDclnInd;
    }

    public String getMirCliLegitDupInd() {
        return mirCliLegitDupInd;
    }

    public void setMirCliLegitDupInd(String mirCliLegitDupInd) {
        this.mirCliLegitDupInd = mirCliLegitDupInd;
    }

    public Float getMirCliWrkQty() {
        return mirCliWrkQty;
    }

    public void setMirCliWrkQty(Float mirCliWrkQty) {
        this.mirCliWrkQty = mirCliWrkQty;
    }

    public String getMirCliMaritStatCd() {
        return mirCliMaritStatCd;
    }

    public void setMirCliMaritStatCd(String mirCliMaritStatCd) {
        this.mirCliMaritStatCd = mirCliMaritStatCd;
    }

    public String getMirCliMibIndCd() {
        return mirCliMibIndCd;
    }

    public void setMirCliMibIndCd(String mirCliMibIndCd) {
        this.mirCliMibIndCd = mirCliMibIndCd;
    }

    public String getMirDvPrevUpdtDt() {
        return mirDvPrevUpdtDt;
    }

    public void setMirDvPrevUpdtDt(String mirDvPrevUpdtDt) {
        this.mirDvPrevUpdtDt = mirDvPrevUpdtDt;
    }

    public String getMirOccpId() {
        return mirOccpId;
    }

    public void setMirOccpId(String mirOccpId) {
        this.mirOccpId = mirOccpId;
    }

    public String getMirCliOccpClasCd() {
        return mirCliOccpClasCd;
    }

    public void setMirCliOccpClasCd(String mirCliOccpClasCd) {
        this.mirCliOccpClasCd = mirCliOccpClasCd;
    }

    public Byte getMirCliPrstRt() {
        return mirCliPrstRt;
    }

    public void setMirCliPrstRt(Byte mirCliPrstRt) {
        this.mirCliPrstRt = mirCliPrstRt;
    }

    public String getSexCode() {
        return sexCode;
    }

    public void setSexCode(String sexCode) {
        this.sexCode = sexCode;
    }

    public String getMirCliTaxId() {
        return mirCliTaxId;
    }

    public void setMirCliTaxId(String mirCliTaxId) {
        this.mirCliTaxId = mirCliTaxId;
    }

    public String getMirCliSmkrCd() {
        return mirCliSmkrCd;
    }

    public void setMirCliSmkrCd(String mirCliSmkrCd) {
        this.mirCliSmkrCd = mirCliSmkrCd;
    }

    public String getMirCliIndvGrCd() {
        return mirCliIndvGrCd;
    }

    public void setMirCliIndvGrCd(String mirCliIndvGrCd) {
        this.mirCliIndvGrCd = mirCliIndvGrCd;
    }

    public String getMirCliNatureOfBus() {
        return mirCliNatureOfBus;
    }

    public void setMirCliNatureOfBus(String mirCliNatureOfBus) {
        this.mirCliNatureOfBus = mirCliNatureOfBus;
    }

    public String getMirCliCtznshipCd() {
        return mirCliCtznshipCd;
    }

    public void setMirCliCtznshipCd(String mirCliCtznshipCd) {
        this.mirCliCtznshipCd = mirCliCtznshipCd;
    }

    public String getMirGovtPosInd() {
        return mirGovtPosInd;
    }

    public void setMirGovtPosInd(String mirGovtPosInd) {
        this.mirGovtPosInd = mirGovtPosInd;
    }

    public String getMirUSCtznInd() {
        return mirUSCtznInd;
    }

    public void setMirUSCtznInd(String mirUSCtznInd) {
        this.mirUSCtznInd = mirUSCtznInd;
    }

    public String getMirUSTaxResPRInd() {
        return mirUSTaxResPRInd;
    }

    public void setMirUSTaxResPRInd(String mirUSTaxResPRInd) {
        this.mirUSTaxResPRInd = mirUSTaxResPRInd;
    }

    public String getMirUSTaxResSPInd() {
        return mirUSTaxResSPInd;
    }

    public void setMirUSTaxResSPInd(String mirUSTaxResSPInd) {
        this.mirUSTaxResSPInd = mirUSTaxResSPInd;
    }

    public String getMirCliDutyDetlTxt() {
        return mirCliDutyDetlTxt;
    }

    public void setMirCliDutyDetlTxt(String mirCliDutyDetlTxt) {
        this.mirCliDutyDetlTxt = mirCliDutyDetlTxt;
    }

    public String getMirRcvMktngInfoInd() {
        return mirRcvMktngInfoInd;
    }

    public void setMirRcvMktngInfoInd(String mirRcvMktngInfoInd) {
        this.mirRcvMktngInfoInd = mirRcvMktngInfoInd;
    }

    public String getMirCliSssGsis() {
        return mirCliSssGsis;
    }

    public void setMirCliSssGsis(String mirCliSssGsis) {
        this.mirCliSssGsis = mirCliSssGsis;
    }
}
