package ph.com.sunlife.wms.ingenium.ws.client;

import ph.com.sunlife.ingenium.domain.MirCommonFields;
import ph.com.sunlife.ingenium.ws.domain.RequirementData;
import ph.com.sunlife.ingenium.ws.domain.Transaction;
import ph.com.sunlife.ingenium.ws.domain.RequirementsCreateContainer;

public class RequirementsCreateRequestBuilder extends IngeniumRequestBuilder {

    private String clientId;
    private String requirementId;
    private String requirementStatusCode;

    public RequirementsCreateRequestBuilder(final String clientId) {
        this.setClientId(clientId);
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getRequirementId() {
        return requirementId;
    }

    public RequirementsCreateRequestBuilder setRequirementId(String requirementId) {
        this.requirementId = requirementId;
        return this;
    }

    public String getRequirementStatusCode() {
        return requirementStatusCode;
    }

    public RequirementsCreateRequestBuilder setRequirementStatusCode(String requirementStatusCode) {
        this.requirementStatusCode = requirementStatusCode;
        return this;
    }

    @Override
    protected Transaction getTransaction() {
        RequirementsCreateContainer box = new RequirementsCreateContainer()
                .setRequirementData(new RequirementData()
                        .setMirCommonFields(new MirCommonFields()
                                .setMirCmpltnessInd("C")
                                .setMirCliId(clientId)
                                .setMirReqirId(requirementId)
                                .setMirCpreqStatCd(requirementStatusCode)));
        Transaction transaction = new Transaction();
        transaction.setReferenceGUID(transactionReferenceGUID);
        transaction.setType("RequirementsCreate");
        transaction.setExecutionDate(getDateFormatter().format(executionTimestamp));
        transaction.setExecutionTime(getTimeFormatter().format(executionTimestamp));
        transaction.setContainer(box);
        return transaction;
    }
}
