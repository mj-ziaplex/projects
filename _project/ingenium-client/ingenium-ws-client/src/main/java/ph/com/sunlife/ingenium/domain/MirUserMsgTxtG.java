package ph.com.sunlife.ingenium.domain;

import java.util.ArrayList;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("MirUserMsgTxtG")
public class MirUserMsgTxtG {

	@XStreamImplicit(itemFieldName = "MirUserMsgTxtT")
	protected List<String> mirUserMsgTxtT;

	public List<String> getMirUserMsgTxtT() {
		if (mirUserMsgTxtT == null) {
			mirUserMsgTxtT = new ArrayList<String>();
		}
		return this.mirUserMsgTxtT;
	}

}
