package ph.com.sunlife.ingenium.domain.response.payload;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

@XStreamAlias("MirCliResTypCdG")
public class MirClientResidenceTypeCodeCluster {

    @XStreamImplicit(itemFieldName = "MirCliResTypCdT")
    private List<String> clientResidenceTypeCodes;

    public List<String> getClientResidenceTypeCodes() {
        return clientResidenceTypeCodes;
    }

    public void setClientResidenceTypeCodes(final List<String> typeCodes) {
        clientResidenceTypeCodes = typeCodes;
    }
}
