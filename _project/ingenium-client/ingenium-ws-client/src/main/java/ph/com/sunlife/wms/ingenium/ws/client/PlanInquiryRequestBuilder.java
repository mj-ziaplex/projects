package ph.com.sunlife.wms.ingenium.ws.client;

import ph.com.sunlife.ingenium.ws.domain.PlanInquiryContainer;
import ph.com.sunlife.ingenium.ws.domain.PlanInquiryData;
import ph.com.sunlife.ingenium.ws.domain.Transaction;

public class PlanInquiryRequestBuilder extends IngeniumRequestBuilder {

    private final String planId;

    public PlanInquiryRequestBuilder(final String planId) {
        this.planId = planId;
    }

    @Override
    protected Transaction getTransaction() {
        PlanInquiryData data = new PlanInquiryData();
        data.setPlanID(planId);
        PlanInquiryContainer container = new PlanInquiryContainer();
        container.setPlanInquiryData(data);
        Transaction transaction = new Transaction();
        transaction.setReferenceGUID(transactionReferenceGUID);
        transaction.setType("PlanInquiry");
        transaction.setExecutionDate(getDateFormatter().format(executionTimestamp));
        transaction.setExecutionTime(getTimeFormatter().format(executionTimestamp));
        transaction.setContainer(container);
        return transaction;
    }
}
