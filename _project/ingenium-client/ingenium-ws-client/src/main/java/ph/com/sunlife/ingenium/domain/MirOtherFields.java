package ph.com.sunlife.ingenium.domain;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("MirOtherFields")
public class MirOtherFields {

    @XStreamAlias("MirDodIntCalcCd")
    private String mirDodIntCalcCd;
    @XStreamAlias("MirRsrvIntRt")
    private Float mirRsrvIntRt;
    @XStreamAlias("MirDirLoanRecogInd")
    private String mirDirLoanRecogInd;

    public String getMirDodIntCalcCd() {
        return mirDodIntCalcCd;
    }

    public void setMirDodIntCalcCd(String mirDodIntCalcCd) {
        this.mirDodIntCalcCd = mirDodIntCalcCd;
    }

    public Float getMirRsrvIntRt() {
        return mirRsrvIntRt;
    }

    public void setMirRsrvIntRt(Float mirRsrvIntRt) {
        this.mirRsrvIntRt = mirRsrvIntRt;
    }

    public String getMirDirLoanRecogInd() {
        return mirDirLoanRecogInd;
    }

    public void setMirDirLoanRecogInd(String mirDirLoanRecogInd) {
        this.mirDirLoanRecogInd = mirDirLoanRecogInd;
    }
}
