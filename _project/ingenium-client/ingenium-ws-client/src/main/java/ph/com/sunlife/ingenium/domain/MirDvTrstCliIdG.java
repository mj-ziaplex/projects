package ph.com.sunlife.ingenium.domain;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

@XStreamAlias("MirDvTrstCliIdG")
public class MirDvTrstCliIdG {

    @XStreamImplicit(itemFieldName = "MirDvTrstCliIdT")
    private List<String> mirDvTrstCliIdT;

    public List<String> getMirDvTrstCliIdT() {
        return mirDvTrstCliIdT;
    }

    public void setMirDvTrstCliIdT(List<String> mirDvTrstCliIdT) {
        this.mirDvTrstCliIdT = mirDvTrstCliIdT;
    }
}
