package ph.com.sunlife.ingenium.domain;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

@XStreamAlias("MirDvOwnCliNmG")
public class MirDvOwnCliNmG {

    @XStreamImplicit(itemFieldName = "MirDvOwnCliNmT")
    private List<String> mirDvOwnCliNmT;

    public List<String> getMirDvOwnCliNmT() {
        return mirDvOwnCliNmT;
    }

    public void setMirDvOwnCliNmT(List<String> mirDvOwnCliNmT) {
        this.mirDvOwnCliNmT = mirDvOwnCliNmT;
    }
}
