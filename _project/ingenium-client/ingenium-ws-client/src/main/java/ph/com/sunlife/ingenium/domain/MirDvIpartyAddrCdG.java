package ph.com.sunlife.ingenium.domain;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

@XStreamAlias("MirDvIpartyAddrCdG")
public class MirDvIpartyAddrCdG {

    @XStreamImplicit(itemFieldName = "MirDvIpartyAddrCdT")
    private List<String> mirDvIpartyAddrCdT;

    public List<String> getMirDvIpartyAddrCdT() {
        return mirDvIpartyAddrCdT;
    }

    public void setMirDvIpartyAddrCdT(List<String> mirDvIpartyAddrCdT) {
        this.mirDvIpartyAddrCdT = mirDvIpartyAddrCdT;
    }
}
