package ph.com.sunlife.zold.ws.ingenium;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ph.com.sunlife.wms.ingenium.configuration.IngeniumClientConfigurator;
import ph.com.sunlife.zold.dao.IngeniumDao;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @author pm13
 * <p>
 * *********************************************
 * Version 1.4
 * Cshells - Jan162015
 * -made changes on method getClientDetails(String strClientId)
 * -remove old codes that was commented
 * -add checking of null with regards to varaible CLIENT_FLAG_INDICIA
 * -enclosed variable in if conditions to String.valueOf to avoid nullPointer
 * -added sysout in catch block for logging.
 * Lacsz - Jan202015
 * - modified try and catch on getClientDetails(String strClientId)
 * *********************************************
 * Version 1.5
 * Cshells - Jan212015
 * - made changes on method getClientDetails(String strClientId)
 * - changed the part of getting the string values from ".iterator.next()" to ".get(0)"
 * - added checking of List size to avoid indexOutOfBounce in if conditions
 * *********************************************
 * Version 1.6
 * Cshells - Jan222015
 * - made changes on method getClientDetails(String strClientId)
 * - added an else where in if indicia is not null & size==0, add a empty string in List
 */
public class IngeniumDispatcher {

  // ROBIN Mods -mcasio 4/5/2011 (start)
  private String m_sIngeniumServer = "";
  // ROBIN Mods -mcasio 4/5/2011 (end)

  // ROBIN Mods -albaj 4/5/2011 (start)
  private String m_sIngeniumWSTarget = "";
  // ROBIN Mods -albaj 4/5/2011 (end)

//  private PasswordGenerator pass;
  private IngBusinessProcesses ingBusProcess;
  private String Authenticate[] = new String[2];
  private WMSLogger TheLogger;
  String effectiveDate = "";

  /**
   * Creates a new instance of IngeniumDispatcher
   */
  public IngeniumDispatcher() {
    TheLogger = new WMSLogger("IngeniumDispatcher");
  }

  // ROBIN Mods -mcasio 4/5/2011 (start)
  public void IngeniumInit(String p_username, String sServer, String wsTarget) {
    m_sIngeniumServer = sServer;
    m_sIngeniumWSTarget = wsTarget;
    IngeniumInit(p_username);
  }

  // ROBIN Mods -mcasio 4/5/2011 (end)

  /**
   * Initialize user information detail for the Ingenium connection.
   *
   * @param p_username
   */
  public void IngeniumInit(String p_username) {
    TheLogger.info("IngeniumInit");

    if (m_sIngeniumWSTarget.trim().length() > 0) {
      ingBusProcess = new IngBusinessProcesses(m_sIngeniumServer,
          m_sIngeniumWSTarget);
    } else {
      ingBusProcess = new IngBusinessProcesses();
    }
//    pass = new PasswordGenerator();

    StringBuffer sb = new StringBuffer();
    sb.append(p_username);
//    sb.append("W");
    System.out.println("sb.toString(): " + sb.toString());
    Authenticate[0] = sb.toString();
    Authenticate[1] = "";

    try {
      /*
       * pass.updateKey(
       * "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
       * ); pass.updateFirstChar('a'); pass.updateDisplacement(1);
       */
      /*
       * pass.saveSettimport java.util.*; import passwordgenerator.*;
       *
       * ings();
       */

      ApplicationContext context =
          new AnnotationConfigApplicationContext(IngeniumClientConfigurator.class);

      Authenticate[0] = sb.toString();
      Authenticate[1] = (String) context.getBean("password");
      // Authenticate[1] = "v26c6042";

      // Authenticate[0] = "PM31W";
      // Authenticate[0]="wc05";
      // Authenticate[1] = "v3c83b3e";

      //Authenticate[0] = "wu11W";
      //Authenticate[1] = "aed849cc";

      System.out.println("username is " + sb.toString() + " password is "
          + (String) context.getBean("password"));


    } catch (Exception ex) {
      TheLogger.error(ex.getMessage());
      ex.printStackTrace();
    }
  }

  /**
   * Check and convert to ingenium date format (ddMMMyy)
   *
   * @param strEffDate
   * @return effDate(ddMMMyy)
   */
  private String convertToIngDateFormat(String strEffDate) {
    Date date = null;
    String effDate = strEffDate;
    SimpleDateFormat ingeniumDateDisplay = new SimpleDateFormat("ddMMMyyyy");
    SimpleDateFormat ingeniumDateFormat = new SimpleDateFormat("yyyy-MM-dd");
    try {
      date = ingeniumDateDisplay.parse(strEffDate);
    } catch (ParseException e) {
      e.getMessage();
    }
    if (date != null) {
      effDate = ingeniumDateFormat.format(date);
    }
    return effDate;
  }

  private int[] convertToIngTimeFormat(String strTime) {
    Date date = null;
    int[] time = new int[10];
    SimpleDateFormat ingeniumTimeDisplay = new SimpleDateFormat("HHmmss");
    int hour;
    int min;
    int sec;
    try {
      date = ingeniumTimeDisplay.parse(strTime);
    } catch (ParseException e) {
      e.getMessage();
    }
    if (date != null) {
      hour = date.getHours();
      min = date.getMinutes();
      sec = date.getSeconds();
    } else {
      date = new Date();
      hour = date.getHours();
      min = date.getMinutes();
      sec = date.getSeconds();
    }
    time[0] = hour;
    time[1] = min;
    time[2] = sec;

    System.out.println("HOUR: " + time[0]);
    System.out.println("MINUTES: " + time[1]);
    System.out.println("SECONDS: " + time[2]);

    return time;
  }

  /**
   * Coverage Premiums Inquiry
   *
   * @param strMirPolIdBase Policy base
   * @param Coverage_Number Coverage number
   * @return
   */
  public HashMap getInquiryCoveragePremiums(String strMirPolIdBase,
                                            String Coverage_Number) {
    TheLogger.info("getInquiryCoveragePremiums");
    HashMap hashMap = null;

    try {
      hashMap = (HashMap) ingBusProcess.InquiryCoveragePremiums_Append(
          Authenticate, strMirPolIdBase, Coverage_Number);
      // printHashMap(hashMap);
    } catch (Exception ex) {
      TheLogger.error(ex.getMessage());
    }
    return hashMap;
  }

  /**
   * Policy Values Inquiry
   *
   * @param strPolicyNo Policy Number
   * @param EffectiveDt Effective Date
   * @return
   */
  public HashMap getInquiryPolicyValues(String strPolicyNo, String EffectiveDt) {
    TheLogger.info("getInquiryPolicyValues");
    HashMap hashMap = null;
    String effDate = convertToIngDateFormat(EffectiveDt);
    try {
      hashMap = (HashMap) ingBusProcess.InquiryPolicyValues_Append(
          Authenticate, strPolicyNo, effDate);
      // printHashMap(hashMap);
    } catch (Exception ex) {
      TheLogger.error(ex.getMessage());
    }
    return hashMap;
  }

  /**
   * Policy Value Inquiry
   *
   * @param strPolicyNo Policy number
   * @return
   */
  public HashMap getInquiryPolicyValues(String strPolicyNo) {
    TheLogger.info("getInquiryPolicyValues");
    HashMap hashMap = null;

    try {
      hashMap = (HashMap) ingBusProcess.InquiryPolicyValues_Append(
          Authenticate, strPolicyNo);
      // printHashMap(hashMap);
    } catch (Exception ex) {
      TheLogger.error(ex.getMessage());
    }
    return hashMap;
  }

  /**
   * Universal Life Details Inquiry
   *
   * @param strPolicyNo     Policy number
   * @param Coverage_Number Coverage number
   * @return
   */
  public HashMap getInquiryULDetails(String strPolicyNo,
                                     String Coverage_Number) {
    TheLogger.info("getInquiryULDetails");
    HashMap hashMap = null;

    try {
      hashMap = (HashMap) ingBusProcess.InquiryULDetails_Append(
          Authenticate, strPolicyNo, Coverage_Number);
      // printHashMap(hashMap);
    } catch (Exception ex) {
      TheLogger.error(ex.getMessage());
    }
    return hashMap;
  }

  /**
   * Universal Life Details Inquiry
   *
   * @param strPolicyNo     Policy number
   * @param Coverage_Number Coverage number
   * @param EffectiveDt     Effective date
   * @return
   */
  public HashMap getInquiryULDetails(String strPolicyNo,
                                     String Coverage_Number, String EffectiveDt) {
    TheLogger.info("getInquiryULDetails");
    HashMap hashMap = null;
    String effDate = convertToIngDateFormat(EffectiveDt);
    try {
      hashMap = (HashMap) ingBusProcess.InquiryULDetails_Append(
          Authenticate, strPolicyNo, Coverage_Number, effDate);
      // printHashMap(hashMap);
    } catch (Exception ex) {
      TheLogger.error(ex.getMessage());
    }
    return hashMap;
  }

  /**
   * Deferred Activity List
   *
   * @param strPolicyNo    Policy number
   * @param Effective_Date Effective date
   * @return
   */
  public HashMap getDeferredActivityList(String strPolicyNo,
                                         String Effective_Date) {
    TheLogger.info("getDeferredActivityList");
    HashMap hashMap = null;
    String effDate = convertToIngDateFormat(Effective_Date);
    try {
      hashMap = (HashMap) ingBusProcess.DeferredActivityList_Append(
          Authenticate, strPolicyNo, effDate);
      // printHashMap(hashMap);
    } catch (Exception ex) {
      TheLogger.error(ex.getMessage());
    }
    return hashMap;

  }

  public HashMap getDeferredActivityList(String policyNo, ArrayList keys) {
    TheLogger.info("getDeferredActivityList");
    HashMap hashMap = null;
    keys = new ArrayList();

    // Referential keys - start
    keys.add("POLICY_FINANCIAL_ACTIVITY_DEFERRED_ACTIVITY_LIST_FINANCIAL_TRANSACTION_TYPE");
    keys.add("POLICY_FINANCIAL_ACTIVITY_DEFERRED_ACTIVITY_LIST_FINANCIAL_TRANSACTION_STATUS");

    // String effDate = convertToIngDateFormat(Effective_Date);

    TheLogger
        .info("######################## P A R A M E T E R S ########################");

    TheLogger.info("Policy No : " + policyNo);

    TheLogger
        .info("######################## / P A R A M E T E R S / ########################");

    TheLogger.info("List of referential keys : " + keys);

    HashMap hmRet = null;

    try {
      TheLogger.info("----------- Processing - start -----------");

      TheLogger.info("Initial Effective Date : " + effectiveDate);

      String effDate = (effectiveDate != null && effectiveDate.equals("")) ? initEffectiveDate()
          : effectiveDate;

      TheLogger.info("New Effective Date : " + effDate);

      hmRet = (HashMap) ingBusProcess.DeferredActivityList_Append(
          Authenticate, policyNo, effDate);

      List tmpList = null;

      List tmpValList = null;

      String prefix = null;

      String suffix = null;

      if ((keys != null)) {

        suffix = "_DESC";

        Iterator it = keys.iterator();

        while (it.hasNext()) {

          String key = (String) it.next();

          tmpList = new ArrayList();

          tmpValList = new ArrayList();

          if (key.equals("POLICY_FINANCIAL_ACTIVITY_DEFERRED_ACTIVITY_LIST_FINANCIAL_TRANSACTION_TYPE")
              || key.equals("POLICY_FINANCIAL_ACTIVITY_DEFERRED_ACTIVITY_LIST_FINANCIAL_TRANSACTION_STATUS")) { // Status
            // Description
            // from
            // Ingenium

            if (key.equals("POLICY_FINANCIAL_ACTIVITY_DEFERRED_ACTIVITY_LIST_FINANCIAL_TRANSACTION_TYPE")) {
              prefix = IngeniumConstants.POLICY_FINANCIAL_ACTIVITY_DEFERRED_ACTIVITY_LIST_FINANCIAL_TRANSACTION_TYPE_;
            } else if (key
                .equals("POLICY_FINANCIAL_ACTIVITY_DEFERRED_ACTIVITY_LIST_FINANCIAL_TRANSACTION_STATUS")) {
              prefix = IngeniumConstants.POLICY_FINANCIAL_ACTIVITY_DEFERRED_ACTIVITY_LIST_FINANCIAL_TRANSACTION_STATUS_;
            } else {
              TheLogger.info("Key " + key
                  + " prefix is not exposed.");
              prefix = null;
            }

            if (prefix != null) {

              tmpList = (hmRet.get(key) != null) ? (ArrayList) hmRet
                  .get(key) : new ArrayList();

              for (Iterator iter = tmpList.iterator(); iter
                  .hasNext(); ) {

                String val = (String) iter.next();

                TheLogger.info("Key is " + key + ".");
                TheLogger.info("Prefix is " + prefix + ".");
                TheLogger.info("Value is " + val + ".");

                if (!val.equals("*")) {
                  tmpValList.add(IngeniumConstants
                      .getStatusDesc(prefix + val));
                }
              }

              hmRet.put(key, tmpList);
              hmRet.put(key + suffix, tmpValList);
            }
          }
        }
      }
    } catch (Exception ex) {
      TheLogger.error(ex.getMessage());
    }
    return hmRet;

  }

  /**
   * Deferred Activity List
   *
   * @param strPolicyNo Policy number
   * @return
   */
  public HashMap getDeferredActivityList(String strPolicyNo) {
    TheLogger.info("getDeferredActivityList");
    HashMap hashMap = null;

    try {
      hashMap = (HashMap) ingBusProcess.DeferredActivityList_Append(
          Authenticate, strPolicyNo);
      // printHashMap(hashMap);
    } catch (Exception ex) {
      TheLogger.error(ex.getMessage());
    }
    return hashMap;

  }

  /**
   * Fund Transfer Percent to Percent
   *
   * @param strPolicyNo    Policy number
   * @param Effective_Date Effective date
   * @return
   */
  public HashMap getFundTransferPercentToPercent(String strPolicyNo,
                                                 String Effective_Date) {
    TheLogger.info("getFundTransferPercentToPercent");
    HashMap hashMap = null;
    String effDate = convertToIngDateFormat(Effective_Date);
    try {
      hashMap = (HashMap) ingBusProcess
          .FundTransferPercentToPercent_Append(Authenticate,
              strPolicyNo, Effective_Date);
      // printHashMap(hashMap);
    } catch (Exception ex) {
      TheLogger.error(ex.getMessage());
    }
    return hashMap;

  }

  /**
   * Fund Transfer Percent to Percent
   *
   * @param strPolicyNo Policy number
   * @return
   */
  public HashMap getFundTransferPercentToPercent(String strPolicyNo) {
    TheLogger.info("getFundTransferPercentToPercent");
    HashMap hashMap = null;

    try {
      hashMap = (HashMap) ingBusProcess
          .FundTransferPercentToPercent_Append(Authenticate,
              strPolicyNo);
      // printHashMap(hashMap);
    } catch (Exception ex) {
      TheLogger.error(ex.getMessage());
    }
    return hashMap;

  }

  /**
   * Policy Allocation List
   *
   * @param strPolicyNo    Policy number
   * @param Effective_Date Effective date
   * @return
   */
  public HashMap getPolicyAllocationList(String strPolicyNo,
                                         String Effective_Date) {
    TheLogger.info("getPolicyAllocationList");
    HashMap hashMap = null;
    String effDate = convertToIngDateFormat(Effective_Date);
    try {
      hashMap = (HashMap) ingBusProcess.PolicyAllocationList_Append(
          Authenticate, strPolicyNo, effDate);
      // hashMap.put(key, value)
    } catch (Exception ex) {
      TheLogger.error(ex.getMessage());
    }
    return hashMap;
  }

  /**
   * Policy Allocation List
   *
   * @param strPolicyNo Policy number
   * @return
   */
  public HashMap getPolicyAllocationList(String strPolicyNo) {
    HashMap hashMap = null;

    try {
      hashMap = (HashMap) ingBusProcess.PolicyAllocationList_Append(
          Authenticate, strPolicyNo);
      // hashMap.put(key, value)
    } catch (Exception ex) {
      TheLogger.error(ex.getMessage());
    }
    return hashMap;

  }

  /**
   * Initializes the effective date.
   *
   * @return
   */
  private String initEffectiveDate() {
    // 20090312 MMANG Use the effective date key in the property file
    // but use current date if the key value is blank
//    ResourceBundle rbIngCode = ResourceBundle
//        .getBundle("com.ph.sunlife.component.properties.Ingenium");
    String effectiveDate =
          (String) new AnnotationConfigApplicationContext(IngeniumClientConfigurator.class)
              .getBean("oldDefaultEffectivityDate");

    Date currentDate = new Date();
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

    return (effectiveDate.equals("")) ? sdf.format(currentDate)
        : effectiveDate;
  }

  /**
   * @param effectDate
   */
  public void setEffectiveDate(String effectDate) {
    effectiveDate = effectDate;
  }

  /**
   * New requirement for getting InquiryConsolidated Information Added by
   * Rommel Suarez Date December 19, 2008
   *
   * @param strPolicyNo
   * @return
   */
  public HashMap getInquiryConsolidatedInformation(String strPolicyNo) throws Exception {
    System.out.println("[IngeniumDispatcher] - getInquiryConsolidatedInformation(String policyNo) - start");
    HashMap hashMap = null;

    // ROBIN Mods -mcasio 4/5/2011 (start)
    IngeniumDao ingeniumDao = new IngeniumDao(m_sIngeniumServer);
    // ROBIN Mods -mcasio 4/5/2011 (end)
    System.out.println("[IngeniumDispatcher] - New Ingenium Session...");
    try {
      String effectDate = (effectiveDate.equals("")) ? initEffectiveDate()
          : effectiveDate;

      hashMap = (HashMap) ingBusProcess
          .InquiryConsolidatedInformation_Append(Authenticate,
              strPolicyNo, effectDate);

      List productCode = IngeniumConstants.getHashMapListValue(
          "PRODUCT_CODE", hashMap);
      List billingTypecode = IngeniumConstants.getHashMapListValue(
          "BILLING_TYPE_CODE", hashMap);
      List premiumMode = IngeniumConstants.getHashMapListValue(
          "PREMIUM_MODE", hashMap);
      List minorCiClaimStatus = IngeniumConstants.getHashMapListValue(
          "MINOR_CI_CLAIM_STATUS", hashMap);
      List AGENT_INFORMATION_SERVICE_AGENT_STATUS_CODE = IngeniumConstants
          .getHashMapListValue(
              "AGENT_INFORMATION_SERVICE_AGENT_STATUS_CODE",
              hashMap);
      List currentPolicyStatus = IngeniumConstants.getHashMapListValue(
          "CURRENT_POLICY_STATUS", hashMap);
      List OTHER_OPTIONS_PREMIUM_OFFSET_OPTION_STATUS_CODE = IngeniumConstants
          .getHashMapListValue(
              "OTHER_OPTIONS_PREMIUM_OFFSET_OPTION_STATUS_CODE",
              hashMap);
      List OTHER_OPTIONS_PREMIUM_OFFSET_OPTION = IngeniumConstants
          .getHashMapListValue("OTHER_OPTIONS_PREMIUM_OFFSET_OPTION",
              hashMap);
      List OTHER_OPTIONS_NON_FORFEITURE_OPTION = IngeniumConstants
          .getHashMapListValue("OTHER_OPTIONS_NON_FORFEITURE_OPTION",
              hashMap);
      List DIVIDEND_INFORMATION_POLICY_DIVIDEND_OPTION = IngeniumConstants
          .getHashMapListValue(
              "DIVIDEND_INFORMATION_POLICY_DIVIDEND_OPTION",
              hashMap);
      List AE_FUND_DETAILS_AE_OPTION = IngeniumConstants
          .getHashMapListValue("AE_FUND_DETAILS_AE_OPTION", hashMap);
      List NBO_CODE = IngeniumConstants.getHashMapListValue(
          "AGENT_INFORMATION_SERVICE_NBO_CODE", hashMap);
      List BENEFICIARY_INFORMATION_BENEFICIARY_TYPE = IngeniumConstants
          .getHashMapListValue(
              "BENEFICIARY_INFORMATION_BENEFICIARY_TYPE", hashMap);
      List BENEFICIARY_INFORMATION_BENEFICIARY_PROCEEDS = IngeniumConstants
          .getHashMapListValue(
              "BENEFICIARY_INFORMATION_BENEFICIARY_PROCEEDS",
              hashMap);
      List BENEFICIARY_INFORMATION_BENEFICIARY_DESIGNATION = IngeniumConstants
          .getHashMapListValue(
              "BENEFICIARY_INFORMATION_BENEFICIARY_DESIGNATION",
              hashMap);

      // Auto Partial Withdrawal Values
      List pdAutoWithdrawalIndicator = IngeniumConstants
          .getHashMapListValue(
              "MIR_PD_AUTO_WTHDR_IND",
              hashMap);
      List autoWithdrawalIndicator = IngeniumConstants
          .getHashMapListValue(
              "MIR_AUTO_WTHDR_IND",
              hashMap);
      List autoWithdrawalReason = IngeniumConstants
          .getHashMapListValue(
              "MIR_AUTO_WTHDR_TERM_RSN",
              hashMap);
      List autoWithdrawalEffectiveDt = IngeniumConstants
          .getHashMapListValue(
              "MIR_AUTO_WTHDR_EFF_DT",
              hashMap);
      List nextWithdrawalDt = IngeniumConstants
          .getHashMapListValue(
              "MIR_NEXT_WTHDR_DT",
              hashMap);
      List autoWithdrawalAmount = IngeniumConstants
          .getHashMapListValue(
              "MIR_AUTO_WTHDR_AMT",
              hashMap);
      List autoWithdrawalPercentage = IngeniumConstants
          .getHashMapListValue(
              "MIR_AUTO_WTHDR_PCT",
              hashMap);
      List autoWithdrawalDuration = IngeniumConstants
          .getHashMapListValue(
              "MIR_AUTO_WTHDR_DUR",
              hashMap);
      List remainingAutoWithdrawalReason = IngeniumConstants
          .getHashMapListValue(
              "MIR_REMN_WTHDR_DUR",
              hashMap);

      // vsantos
      // get LIST_BILL_ADDRESS_CODE description
      // CLIENT_ADDRESS_TYPE_VALUE
      List listBillAddressCode = IngeniumConstants.getHashMapListValue(
          "LIST_BILL_ADDRESS_CODE", hashMap);
      if (listBillAddressCode != null) {
        List addressCode = new ArrayList();
        addressCode.add(IngeniumConstants.getStatusDesc(
            "CLIENT_ADDRESS_TYPE_"
                + (IngeniumConstants
                .getListValue(listBillAddressCode)),
            IngeniumConstants.getListValue(listBillAddressCode)));
        hashMap.put("LIST_BILL_ADDRESS_CODE", addressCode);
      }

      /*
       * vsantos 1/29/2014 add list bill form and list bill type
       */
      List listBillTypeCode = IngeniumConstants.getHashMapListValue(
          "LIST_BILL_TYPE_CODE", hashMap);
      if (listBillTypeCode != null) {
        List listBillType = new ArrayList();
        listBillType.add(IngeniumConstants.getStatusDesc(
            "LIST_BILL_TYPE_CODE_"
                + (IngeniumConstants
                .getListValue(listBillTypeCode)),
            IngeniumConstants.getListValue(listBillTypeCode)));
        hashMap.put("LIST_BILL_TYPE_CD", listBillType);
      }

      List listBillFormCode = IngeniumConstants.getHashMapListValue(
          "LIST_BILL_FORM_CODE", hashMap);
      if (listBillFormCode != null) {
        List listBillForm = new ArrayList();
        listBillForm.add(IngeniumConstants.getStatusDesc(
            "LIST_BILL_FORM_CODE_"
                + (IngeniumConstants
                .getListValue(listBillFormCode)),
            IngeniumConstants.getListValue(listBillFormCode)));
        hashMap.put("LIST_BILL_FORM_CD", listBillForm);
      }

      System.out.println("new map added");
      System.out.println(hashMap.get("LIST_BILL_TYPE_CD"));
      System.out.println(hashMap.get("LIST_BILL_FORM_CD"));

      List productName = new ArrayList();
      try {
        productName.add(ingeniumDao
            .getPlanDescriptionByIdFromXML(IngeniumConstants
                .getListValue(productCode)));
      } catch (Exception e) {
        e.printStackTrace();
      }

      List billingType = new ArrayList();
      billingType.add(IngeniumConstants
          .getStatusDesc(IngeniumConstants.BILLING_TYPE_
              + IngeniumConstants.getListValue(billingTypecode)));

      List premiumModeDesc = new ArrayList();
      premiumModeDesc.add(IngeniumConstants
          .getStatusDesc(IngeniumConstants.PREMIUM_MODE_
              + IngeniumConstants.getListValue(premiumMode)));

      List minorCiClaimStatusDesc = new ArrayList();
      minorCiClaimStatusDesc.add((IngeniumConstants
          .getStatusDesc(IngeniumConstants.MINOR_CI_CLAIM_STATUS_
              + IngeniumConstants.getListValue(minorCiClaimStatus))));

      List autoWthRsnDesc = new ArrayList();
      autoWthRsnDesc.add((IngeniumConstants
          .getStatusDesc(IngeniumConstants.MIR_AUTO_WTHDR_TERM_RSN_
              + IngeniumConstants.getListValue(autoWithdrawalReason))));

      List autoWthIndDesc = new ArrayList();
      autoWthIndDesc.add((IngeniumConstants
          .getStatusDesc(IngeniumConstants.MIR_AUTO_WTHDR_IND_
              + IngeniumConstants.getListValue(autoWithdrawalIndicator))));

      List agentstatusDesc = new ArrayList();
      agentstatusDesc
          .add(IngeniumConstants.getStatusDesc(IngeniumConstants.AGENT_STATUS_
              + IngeniumConstants
              .getListValue(AGENT_INFORMATION_SERVICE_AGENT_STATUS_CODE)));

      List currentPolicyStatusDesc = new ArrayList();
      currentPolicyStatusDesc.add(IngeniumConstants
          .getStatusDesc(IngeniumConstants.POLICY_STATUS_
              + IngeniumConstants
              .getListValue(currentPolicyStatus)));

      List otherOptionsPremiumOffsetOptionStatusCodeDesc = new ArrayList();
      otherOptionsPremiumOffsetOptionStatusCodeDesc
          .add(IngeniumConstants.getStatusDesc(IngeniumConstants.PREMIUM_OFFSET_OPTION_STATUS_
              + IngeniumConstants
              .getListValue(OTHER_OPTIONS_PREMIUM_OFFSET_OPTION_STATUS_CODE)));

      List otherOptionsPremiumOffsetOptionDesc = new ArrayList();
      otherOptionsPremiumOffsetOptionDesc
          .add(IngeniumConstants.getStatusDesc("PREMIUM_OFFSET_OPTION_"
              + IngeniumConstants
              .getListValue(OTHER_OPTIONS_PREMIUM_OFFSET_OPTION)));

      List OTHER_OPTIONS_NON_FORFEITURE_OPTION_DESC = new ArrayList();
      OTHER_OPTIONS_NON_FORFEITURE_OPTION_DESC
          .add(IngeniumConstants.getStatusDesc("NON.FORFEITURE.OPTION_"
              + IngeniumConstants
              .getListValue(OTHER_OPTIONS_NON_FORFEITURE_OPTION)));

      List NBO_NAME = new ArrayList();
      NBO_NAME.add(ingeniumDao.getNBONameById(IngeniumConstants
          .getListValue(NBO_CODE)));

      List DIVIDEND_INFORMATION_POLICY_DIVIDEND_OPTION_DESC = new ArrayList();
      DIVIDEND_INFORMATION_POLICY_DIVIDEND_OPTION_DESC
          .add(IngeniumConstants.getStatusDesc("POLICY.DIVIDEND.OPTION_"
              + IngeniumConstants
              .getListValue(DIVIDEND_INFORMATION_POLICY_DIVIDEND_OPTION)));

      List AE_FUND_DETAILS_AE_OPTION_DESC = new ArrayList();
      AE_FUND_DETAILS_AE_OPTION_DESC.add(IngeniumConstants
          .getStatusDesc("AE_OPTION_"
              + IngeniumConstants
              .getListValue(AE_FUND_DETAILS_AE_OPTION)));

      List beneficiaryInformationProceeds = new ArrayList();
      for (int i = 0; i < BENEFICIARY_INFORMATION_BENEFICIARY_PROCEEDS
          .size(); i++) {
        beneficiaryInformationProceeds.add(IngeniumConstants
            .getStatusDesc(IngeniumConstants.BENEFICIARY_PROCEEDS_
                + BENEFICIARY_INFORMATION_BENEFICIARY_PROCEEDS
                .get(i).toString()));
      }

      List beneficiaryInformationDesignation = new ArrayList();
      for (int i = 0; i < BENEFICIARY_INFORMATION_BENEFICIARY_DESIGNATION
          .size(); i++) {
        beneficiaryInformationDesignation
            .add(IngeniumConstants
                .getStatusDesc(IngeniumConstants.BENEFICIARY_DESIGNATION_
                    + BENEFICIARY_INFORMATION_BENEFICIARY_DESIGNATION
                    .get(i).toString()));
      }

      List beneficiaryInformationBenificiaryType = new ArrayList();
      for (int i = 0; i < BENEFICIARY_INFORMATION_BENEFICIARY_TYPE.size(); i++) {
        beneficiaryInformationBenificiaryType.add(IngeniumConstants
            .getStatusDesc(IngeniumConstants.BENEFICIARY_TYPE_
                + BENEFICIARY_INFORMATION_BENEFICIARY_TYPE.get(
                i).toString()));
      }

      hashMap.put(IngeniumConstants.PRODUCT_NAME, productName);
      hashMap.put(IngeniumConstants.BILLING_TYPE, billingType);
      hashMap.put(IngeniumConstants.PREMIUM_MODE_DESC, premiumModeDesc);
      hashMap.put(IngeniumConstants.MINOR_CI_CLAIM_STATUS_DESC, minorCiClaimStatusDesc);
      hashMap.put(
          IngeniumConstants.AGENT_INFORMATION_SERVICE_AGENT_STATUS_DESC,
          agentstatusDesc);
      hashMap.put(IngeniumConstants.CURRENT_POLICY_STATUS_DESC,
          currentPolicyStatusDesc);
      hashMap.put(IngeniumConstants.PREMIUM_OFFSET_OPTION_STATUS_DESC,
          otherOptionsPremiumOffsetOptionStatusCodeDesc);
      hashMap.put(
          IngeniumConstants.OTHER_OPTIONS_PREMIUM_OFFSET_OPTION_DESC,
          otherOptionsPremiumOffsetOptionDesc);
      hashMap.put(IngeniumConstants.BENEFICIARY_DESIGNATION_DESC,
          beneficiaryInformationDesignation);
      hashMap.put(IngeniumConstants.BENEFICIARY_PROCEEDS_DESC,
          beneficiaryInformationProceeds);
      hashMap.put(IngeniumConstants.BENEFICIARY_TYPE_DESC,
          beneficiaryInformationBenificiaryType);
      hashMap.put(
          IngeniumConstants.OTHER_OPTIONS_NON_FORFEITURE_OPTION_DESC,
          OTHER_OPTIONS_NON_FORFEITURE_OPTION_DESC);
      hashMap.put(
          IngeniumConstants.DIVIDEND_INFORMATION_POLICY_DIVIDEND_OPTION_DESC,
          DIVIDEND_INFORMATION_POLICY_DIVIDEND_OPTION_DESC);
      hashMap.put(IngeniumConstants.AE_FUND_DETAILS_AE_OPTION_DESC,
          AE_FUND_DETAILS_AE_OPTION_DESC);
      hashMap.put(IngeniumConstants.NBO_NAME, NBO_NAME);

      //put related data for Auto Partial Withdrawal - IY20
      hashMap.put(IngeniumConstants.MIR_AUTO_WTHDR_IND_DESC, autoWthIndDesc);
      hashMap.put(IngeniumConstants.MIR_AUTO_WTHDR_TERM_RSN_DESC, autoWthRsnDesc);
      hashMap.put(IngeniumConstants.MIR_AUTO_WTHDR_EFF_DT, autoWithdrawalEffectiveDt);
      hashMap.put(IngeniumConstants.MIR_NEXT_WTHDR_DT, nextWithdrawalDt);
      hashMap.put(IngeniumConstants.MIR_AUTO_WTHDR_AMT, autoWithdrawalAmount);
      hashMap.put(IngeniumConstants.MIR_AUTO_WTHDR_PCT, autoWithdrawalPercentage);
      hashMap.put(IngeniumConstants.MIR_AUTO_WTHDR_DUR, autoWithdrawalDuration);
      hashMap.put(IngeniumConstants.MIR_REMN_WTHDR_DUR, remainingAutoWithdrawalReason);

    } catch (Exception ex) {
      System.out.println("[IngeniumDispatcher] -  getInquiryConsolidatedInformation(String policyNo) EXCEPTION: " + ex.getMessage());
      ex.printStackTrace();
    }
    System.out.println("[IngeniumDispatcher] -  getInquiryConsolidatedInformation(String policyNo) - end");
    return hashMap;
  }

  /*
   * Andre Ceasar Dacanay New requirement for getting Policy Inquiry : Inquiry
   * - Consolidated Information WMSStepProcessor_Module
   */

  public HashMap getInquiryConsolidatedInformation(String policyNo,
                                                   ArrayList keys) throws Exception {
    TheLogger = new WMSLogger("IngeniumDispatcher");

    System.out.println("[IngeniumDispatcher] -  getInquiryConsolidatedInformation(String policyNo, ArrayList keys) - start");

    // ArrayList keys = new ArrayList();
    /*
     * //Referential keys - start keys.add("PRODUCT_CODE");
     * keys.add("AGENT_INFORMATION_SERVICE_NBO_CODE");
     * keys.add("COVERAGE_PLANS"); keys.add("BILLING_TYPE_CODE");
     * keys.add("PREMIUM_MODE");
     * keys.add("AGENT_INFORMATION_SERVICE_AGENT_STATUS_CODE");
     * keys.add("CURRENT_POLICY_STATUS");
     * keys.add("OTHER_OPTIONS_PREMIUM_OFFSET_OPTION_STATUS_CODE");
     * keys.add("OTHER_OPTIONS_PREMIUM_OFFSET_OPTION");
     * keys.add("OTHER_OPTIONS_NON_FORFEITURE_OPTION");
     * keys.add("DIVIDEND_INFORMATION_POLICY_DIVIDEND_OPTION");
     * keys.add("AE_FUND_DETAILS_AE_OPTION");
     * keys.add("BENEFICIARY_INFORMATION_BENEFICIARY_PROCEEDS");
     * keys.add("BENEFICIARY_INFORMATION_BENEFICIARY_DESIGNATION");
     * keys.add("BENEFICIARY_INFORMATION_BENEFICIARY_TYPE");
     * keys.add("SMOKER_CODE"); keys.add("COVERAGE_PAR_CODES");
     * keys.add("AGENT_INFORMATION_COVERAGE_CODE");
     * keys.add("COVERAGE_DETAILS_SEX_CODE");
     *
     * //Referential keys - end
     */
    System.out.println("######################## P A R A M E T E R S ########################");

    System.out.println("[IngeniumDispatcher] - Policy No : " + policyNo);

//		System.out.println("######################## / P A R A M E T E R S / ########################");

    System.out.println("[IngeniumDispatcher] - List of referential keys : " + keys);

    HashMap hmRet = null;

    IngeniumDao ingDao = null;

    try {
      System.out.println("----------- [IngeniumDispatcher] - Processing - start -----------");

      System.out.println("[IngeniumDispatcher] - Initial Effective Date : " + effectiveDate);

      String effDate = (effectiveDate != null && effectiveDate.equals("")) ? initEffectiveDate()
          : effectiveDate;

      System.out.println("[IngeniumDispatcher] - New Effective Date : " + effDate);

      hmRet = (HashMap) ingBusProcess
          .InquiryConsolidatedInformation_Append(Authenticate,
              policyNo, effDate);

      List tmpList = null;

      List tmpValList = null;

      String prefix = null;

      String suffix = null;


      if ((keys != null)) {

        suffix = "_DESC";

        Iterator it = keys.iterator();
        while (it.hasNext()) {
          String key = (String) it.next();
          tmpList = new ArrayList();
          tmpValList = new ArrayList();
          if (key.equals("PRODUCT_CODE")
              || key.equals("AGENT_INFORMATION_SERVICE_NBO_CODE")
              || key.equals("COVERAGE_PLANS")) {
            tmpList = IngeniumConstants.getHashMapListValue(key,
                hmRet);
            // ROBIN Mods -mcasio 4/5/2011 (start)
            // ingDao = new IngeniumDao();
            ingDao = new IngeniumDao(m_sIngeniumServer);
            // ROBIN Mods -mcasio 4/5/2011 (end)
            if (key.equals("PRODUCT_CODE")) { // Plan Description
              // from PLAN.XML
              try {
                tmpValList.add(ingDao.getPlanDescriptionByIdFromXML(IngeniumConstants.getListValue(tmpList)));
              } catch (Exception ex) {
                System.out.println("[IngeniumDispatcher] - getInquiryConsolidatedInformation(String policyNo, ArrayList keys) EXCEPTION: " + ex.getMessage());
                ex.printStackTrace();
              }

            } else if (key.equals("AGENT_INFORMATION_SERVICE_NBO_CODE")) { // NBO
              // Name
              try {
                tmpValList.add(ingDao.getNBONameById(IngeniumConstants.getListValue(tmpList)));
              } catch (Exception ex) {
                System.out.println("[IngeniumDispatcher] - getInquiryConsolidatedInformation(String policyNo, ArrayList keys) EXCEPTION: " + ex.getMessage());
                ex.printStackTrace();
              }

            } else if (key.equals("COVERAGE_PLANS")) { // Plan
              // Description
              // from
              // PLAN.XML
              for (Iterator iter = tmpList.iterator(); iter
                  .hasNext(); ) {
                String listKey = (String) iter.next();
                try {
                  tmpValList.add(ingDao.getPlanDescriptionByIdFromXML(listKey));
                } catch (Exception ex) {
                  System.out.println("[IngeniumDispatcher] - getInquiryConsolidatedInformation(String policyNo, ArrayList keys) EXCEPTION: " + ex.getMessage());
                  ex.printStackTrace();
                }
              }
            }

            hmRet.put(key, tmpList);
            hmRet.put(key + suffix, tmpValList);

          } else if (key.equals("BILLING_TYPE_CODE")
              || key.equals("PREMIUM_MODE")
              || key.equals("AGENT_INFORMATION_SERVICE_AGENT_STATUS_CODE")
              || key.equals("CURRENT_POLICY_STATUS")
              || key.equals("OTHER_OPTIONS_PREMIUM_OFFSET_OPTION_STATUS_CODE")
              || key.equals("OTHER_OPTIONS_PREMIUM_OFFSET_OPTION")
              || key.equals("OTHER_OPTIONS_NON_FORFEITURE_OPTION")
              || key.equals("DIVIDEND_INFORMATION_POLICY_DIVIDEND_OPTION")
              || key.equals("AE_FUND_DETAILS_AE_OPTION")
              || key.equals("MINOR_CI_CLAIM_STATUS")) { // Status
            // Description
            // from
            // IngeniumCode.properties
            if (key.equals("BILLING_TYPE_CODE")) {
              prefix = IngeniumConstants.BILLING_TYPE_;
            } else if (key.equals("PREMIUM_MODE")) {
              prefix = IngeniumConstants.PREMIUM_MODE_;
            } else if (key
                .equals("AGENT_INFORMATION_SERVICE_AGENT_STATUS_CODE")) {
              prefix = IngeniumConstants.AGENT_STATUS_;
            } else if (key.equals("CURRENT_POLICY_STATUS")) {
              prefix = IngeniumConstants.POLICY_STATUS_;
            } else if (key
                .equals("OTHER_OPTIONS_PREMIUM_OFFSET_OPTION_STATUS_CODE")) {
              prefix = IngeniumConstants.PREMIUM_OFFSET_OPTION_STATUS_;
            } else if (key
                .equals("OTHER_OPTIONS_PREMIUM_OFFSET_OPTION")) {
              prefix = IngeniumConstants.PREMIUM_OFFSET_OPTION_;
            } else if (key
                .equals("OTHER_OPTIONS_NON_FORFEITURE_OPTION")) {
              prefix = IngeniumConstants.NON_FORFEITURE_OPTION_;
            } else if (key
                .equals("DIVIDEND_INFORMATION_POLICY_DIVIDEND_OPTION")) {
              prefix = IngeniumConstants.POLICY_DIVIDEND_OPTION_;
            } else if (key.equals("AE_FUND_DETAILS_AE_OPTION")) {
              prefix = IngeniumConstants.AE_OPTION_;
            } else if (key.equals("MINOR_CI_CLAIM_STATUS")) {
              prefix = IngeniumConstants.MINOR_CI_CLAIM_STATUS_;
            } else {
              System.out.println("Key " + key
                  + " prefix is not exposed.");
              prefix = null;
            }

            if (prefix != null) {
              // tmpList =
              // IngeniumConstants.getHashMapListValue(key,hmRet);

              tmpList = (hmRet.get(key) != null) ? (List) hmRet
                  .get(key) : new ArrayList();

              tmpValList.add(IngeniumConstants
                  .getStatusDesc(prefix
                      + IngeniumConstants
                      .getListValue(tmpList)));

              hmRet.put(key, tmpList);
              hmRet.put(key + suffix, tmpValList);
            }

          } else if (key
              .equals("BENEFICIARY_INFORMATION_BENEFICIARY_PROCEEDS")
              || key.equals("BENEFICIARY_INFORMATION_BENEFICIARY_DESIGNATION")
              || key.equals("BENEFICIARY_INFORMATION_BENEFICIARY_TYPE")
              || key.equals("SMOKER_CODE")
              || key.equals("COVERAGE_PAR_CODES")
              || key.equals("AGENT_INFORMATION_COVERAGE_CODE")
              || key.equals("COVERAGE_DETAILS_SEX_CODE")) { // Status
            // Description
            // from
            // Ingenium
            if (key.equals("BENEFICIARY_INFORMATION_BENEFICIARY_PROCEEDS")) {
              prefix = IngeniumConstants.BENEFICIARY_PROCEEDS_;
            } else if (key
                .equals("BENEFICIARY_INFORMATION_BENEFICIARY_DESIGNATION")) {
              prefix = IngeniumConstants.BENEFICIARY_DESIGNATION_;
            } else if (key
                .equals("BENEFICIARY_INFORMATION_BENEFICIARY_TYPE")) {
              prefix = IngeniumConstants.BENEFICIARY_TYPE_;
            } else if (key.equals("SMOKER_CODE")) {
              prefix = IngeniumConstants.SMOKER_CODE_;
            } else if (key.equals("COVERAGE_PAR_CODES")) {
              prefix = IngeniumConstants.COVERAGE_PAR_CODES_;
            } else if (key
                .equals("AGENT_INFORMATION_COVERAGE_CODE")) {
              prefix = IngeniumConstants.AGENT_INFORMATION_COVERAGE_CODE_;
            } else if (key.equals("COVERAGE_DETAILS_SEX_CODE")) {
              prefix = IngeniumConstants.COVERAGE_DETAILS_SEX_CODE_;
            } else {
              System.out.println("Key " + key
                  + " prefix is not exposed.");
              prefix = null;
            }

            if (prefix != null) {

              tmpList = (hmRet.get(key) != null) ? (ArrayList) hmRet
                  .get(key) : new ArrayList();

              for (Iterator iter = tmpList.iterator(); iter
                  .hasNext(); ) {

                String val = (String) iter.next();

                System.out.println("Key is " + key + ".");
                System.out.println("Prefix is " + prefix + ".");
                System.out.println("Value is " + val + ".");

                if (!val.equals("*")) {
                  tmpValList.add(IngeniumConstants
                      .getStatusDesc(prefix + val));
                }
              }

              hmRet.put(key, tmpList);
              hmRet.put(key + suffix, tmpValList);
            }

          } else if (key.equals("MIR_AUTO_WTHDR_TERM_RSN")
              || key.equals("MIR_AUTO_WTHDR_IND")) {
            if (key.equals("MIR_AUTO_WTHDR_TERM_RSN")) {
              prefix = IngeniumConstants.MIR_AUTO_WTHDR_TERM_RSN_;
            } else if (key.equals("MIR_AUTO_WTHDR_IND")) {
              prefix = IngeniumConstants.MIR_AUTO_WTHDR_IND_;
            } else {
              System.out.println("Key for autowithdrawal " + key
                  + " prefix is not exposed.");
              prefix = null;
            }

            if (prefix != null) {

              tmpList = (hmRet.get(key) != null) ? (ArrayList) hmRet
                  .get(key) : new ArrayList();

              for (Iterator iter = tmpList.iterator(); iter
                  .hasNext(); ) {

                String val = (String) iter.next();

                System.out.println("Key is " + key + ".");
                System.out.println("Prefix is " + prefix + ".");
                System.out.println("Value is " + val + ".");

                if (!val.equals("*")) {
                  tmpValList.add(IngeniumConstants
                      .getStatusDesc(prefix + val));
                }
              }

              hmRet.put(key, tmpList);
              hmRet.put(key + suffix, tmpValList);
            }
          } else {
            System.out.println("Key " + key + " is not exposed.");
            tmpList.add("not exposed");
            tmpValList.add("not exposed");
            hmRet.put(key, tmpList);
            hmRet.put(key + suffix, tmpValList);
          }

        } // while iterate keys

      } else {
        System.out.println("Keys are null or empty.");
      }

      /*
       * vsantos 1/29/2014 add list bill form and list bill type
       */
      List listBillTypeCode = IngeniumConstants.getHashMapListValue(
          "LIST_BILL_TYPE_CODE", hmRet);
      if (listBillTypeCode != null) {
        List listBillType = new ArrayList();
        listBillType.add(IngeniumConstants.getStatusDesc(
            "LIST_BILL_TYPE_CODE_"
                + (IngeniumConstants
                .getListValue(listBillTypeCode)),
            IngeniumConstants.getListValue(listBillTypeCode)));
        hmRet.put("LIST_BILL_TYPE_CD", listBillType);
      }

      List listBillFormCode = IngeniumConstants.getHashMapListValue(
          "LIST_BILL_FORM_CODE", hmRet);
      if (listBillFormCode != null) {
        List listBillForm = new ArrayList();
        listBillForm.add(IngeniumConstants.getStatusDesc(
            "LIST_BILL_FORM_CODE_"
                + (IngeniumConstants
                .getListValue(listBillFormCode)),
            IngeniumConstants.getListValue(listBillFormCode)));
        hmRet.put("LIST_BILL_FORM_CD", listBillForm);
      }

      System.out.println("----------- [IngeniumDispatcher] - Processing - end -----------");

    } catch (Exception ex) {
      System.out.println("[IngeniumDispatcher] - getInquiryConsolidatedInformation(String policyNo, ArrayList keys) EXCEPTION: " + ex.getMessage());
      ex.printStackTrace();
    }

    // Testing Purposes only - start
    // check the value of hashmap
    /*
     * Iterator it = hmRet.keySet().iterator(); while(it.hasNext()){ String
     * key = (String)it.next(); TheLogger.info("key : "+key+" - "+
     * hmRet.get(key)); }
     */
    // Testing Purposes only - end
    System.out.println("[IngeniumDispatcher] -  getInquiryConsolidatedInformation(String policyNo, ArrayList keys) - end");
    return hmRet;
  }

  public HashMap getClientDetails(String clientId, ArrayList keys) {

    TheLogger = new WMSLogger("IngeniumDispatcher");

    TheLogger.info("getClientDetails - start");

    // ArrayList keys = new ArrayList();

    // Referential keys - start
    keys.add("COUNTRY_OF_CITIZENSHIP");
    keys.add("SEX");
    keys.add("SMOKER");
    keys.add("MARITAL_STATUS");
    keys.add("CITIZENSHIP");

    // Referential keys - end

    TheLogger
        .info("######################## P A R A M E T E R S ########################");

    TheLogger.info("Client ID : " + clientId);

    TheLogger
        .info("######################## / P A R A M E T E R S / ########################");

    TheLogger.info("List of referential keys : " + keys);

    HashMap hmRet = null;

    // IngeniumDao ingDao = null;

    try {
      TheLogger.info("----------- Processing - start -----------");

      hmRet = (HashMap) ingBusProcess.BF1220C_Append(Authenticate,
          clientId);

      List tmpList = null;

      List tmpValList = null;

      String prefix = null;

      String suffix = null;

      if ((keys != null)) {

        suffix = "_DESC";

        Iterator it = keys.iterator();

        while (it.hasNext()) {

          String key = (String) it.next();

          tmpList = new ArrayList();

          tmpValList = new ArrayList();

          if (key.equals("COUNTRY_OF_CITIZENSHIP")
              || key.equals("SEX") || key.equals("SMOKER")
              || key.equals("MARITAL_STATUS")
              || key.equals("CITIZENSHIP")
              || key.equals("CLIENT_FLAG_INDICIA")) { // Status
            // Description
            // from
            // IngeniumCode.properties

            if (key.equals("COUNTRY_OF_CITIZENSHIP")) {
              prefix = IngeniumConstants.COUNTRY_OF_CITIZENSHIP_;
            } else if (key.equals("SEX")) {
              prefix = IngeniumConstants.SEX_;
            } else if (key.equals("SMOKER")) {
              prefix = IngeniumConstants.SMOKER_;
            } else if (key.equals("MARITAL_STATUS")) {
              prefix = IngeniumConstants.MARITAL_STATUS_;
            } else if (key.equals("CITIZENSHIP")) {
              prefix = IngeniumConstants.CITIZENSHIP_;
            } else {
              TheLogger.info("Key " + key
                  + " prefix is not exposed.");
              prefix = null;
            }

            if (prefix != null) {
              // //CLIENT_FLAG_INDICIA - GFraga 10/14/2014 for
              // FATCA
              // List CLIENT_FLAG_INDICIA =
              // IngeniumConstants.getHashMapListValue("CLIENT_FLAG_INDICIA",
              // hmRet);
              // List cltFlgIndicia = new ArrayList();
              // for(int i=0;i<CLIENT_FLAG_INDICIA.size();i++){
              // cltFlgIndicia.add(CLIENT_FLAG_INDICIA.get(i).toString());
              // }
              // String inicia = (String)
              // cltFlgIndicia.iterator().next();
              // // System.out.println("CLIENT_FLAG_INDICIA " +
              // cltFlgIndicia.iterator().next());
              // // System.out.println(inicia);
              //
              // hmRet.put("CLIENT_FLAG_INDICIA", cltFlgIndicia);
              // //tmpList =
              // IngeniumConstants.getHashMapListValue(key,hmRet);

              tmpList = (hmRet.get(key) != null) ? (List) hmRet
                  .get(key) : new ArrayList();

              tmpValList.add(IngeniumConstants
                  .getStatusDesc(prefix
                      + IngeniumConstants
                      .getListValue(tmpList)));

              hmRet.put(key, tmpList);
              hmRet.put(key + suffix, tmpValList);
              System.out.println("hmRet " + hmRet.entrySet());
            }

          } else {
            TheLogger.info("Key " + key + " is not exposed.");
            tmpList.add("not exposed");
            tmpValList.add("not exposed");
            hmRet.put(key, tmpList);
            hmRet.put(key + suffix, tmpValList);
          }

        } // while iterate keys

      } else {
        TheLogger.info("Keys is null or empty.");
      }

      TheLogger.info("----------- Processing - end -----------");

    } catch (Exception ex) {
      TheLogger.error(ex.getMessage());
    }

    TheLogger.info("getClientDetails - end");

    return hmRet;
  }

  /**
   * New requirement for getting PolicyAllocation Inquiry Added by Rommel
   * Suarez Date December 19, 2008
   *
   * @param strPolicyNo
   * @return
   */
  public HashMap getPolicyAllocationInquiry(String strPolicyNo) {
    TheLogger.info("getPolicyAllocationInquiry");
    HashMap hashMap = null;

    try {
      hashMap = (HashMap) ingBusProcess.PolicyAllocationInquiry_Append(
          Authenticate, strPolicyNo);
      // printHashMap(hashMap);
    } catch (Exception ex) {
      TheLogger.error(ex.getMessage());
    }
    return hashMap;
  }

  /**
   * Billing Inquiry
   *
   * @param strPolicyNo
   * @return
   */
  public HashMap getInquiryBilling(String strPolicyNo) {
    TheLogger.info("getInquiryBilling");
    HashMap hashMap = null;

    try {
      hashMap = (HashMap) ingBusProcess.InquiryBilling_Append(
          Authenticate, strPolicyNo);
      // printHashMap(hashMap);
    } catch (Exception ex) {
      TheLogger.error(ex.getMessage());
    }
    return hashMap;
  }

  /**
   * Knock Off Requirements
   *
   * @param strPolicyNo
   * @return
   */
  public HashMap createKoReqts(String strPolicyNo) {
    TheLogger.info("createKoReqts");
    HashMap hashMap = null;

    try {
      hashMap = (HashMap) ingBusProcess.CLEARCASE(Authenticate,
          strPolicyNo);
      // printHashMap(hashMap);
    } catch (Exception ex) {
      TheLogger.error(ex.getMessage());
    }
    return hashMap;
  }

  /**
   * Client Requirements
   *
   * @param strClientId
   * @param ReqtId
   * @param reqStat
   * @return
   */
  public HashMap createClientRequirement(String strClientId, String ReqtId,
                                         String reqStat) {
    TheLogger.info("createClientRequirement");
    HashMap hashMap = null;

    try {
      hashMap = (HashMap) ingBusProcess.BF1191C_Append(Authenticate,
          strClientId, ReqtId, reqStat);
      // printHashMap(hashMap);
    } catch (Exception ex) {
      TheLogger.error(ex.getMessage());
    }
    return hashMap;
  }

  /**
   * Create Policy Requirement
   *
   * @param strMirPolIdBase
   * @param ReqtId
   * @param reqStat
   * @return
   */
  public HashMap createPolicyRequirement(String strMirPolIdBase,
                                         String ReqtId, String reqStat) {
    TheLogger.info("createPolicyRequirement");
    HashMap hashMap = null;
    try {
      hashMap = (HashMap) ingBusProcess.BF1191_Append(Authenticate,
          strMirPolIdBase, ReqtId, reqStat);
      // printHashMap(hashMap);
    } catch (Exception ex) {
      TheLogger.error(ex.getMessage());
    }
    return hashMap;
  }

  /**
   * Create Insured/Owner Requirement
   *
   * @param strClientId
   * @param ReqtId
   * @param reqStat
   * @param strIndexNo
   * @return
   */
  public HashMap createClientRequirement(String strClientId, String ReqtId,
                                         String reqStat, String strIndexNo) {
    TheLogger.info("createClientRequirement");
    HashMap hashMap = null;

    try {
      hashMap = (HashMap) ingBusProcess.BF1191C_Append(Authenticate,
          strClientId, ReqtId, reqStat, strIndexNo);
      // printHashMap(hashMap);
    } catch (Exception ex) {
      TheLogger.error(ex.getMessage());
    }
    return hashMap;
  }

  /**
   * Create Policy Requirement
   *
   * @param strMirPolIdBase
   * @param ReqtId
   * @param reqStat
   * @param strIndexNo
   * @return
   */
  public HashMap createPolicyRequirement(String strMirPolIdBase,
                                         String ReqtId, String reqStat, String strIndexNo) {
    TheLogger.info("Creating Policy Requirement...");
    HashMap hashMap = null;
    try {
      hashMap = (HashMap) ingBusProcess.BF1191_Append(Authenticate,
          strMirPolIdBase, ReqtId, reqStat, strIndexNo);
      // printHashMap(hashMap);
    } catch (Exception ex) {
      TheLogger.error(ex.getMessage());
    }
    return hashMap;
  }

  /**
   * All Coverage Details
   *
   * @param strPolicyId
   * @return
   */
  public HashMap CoverageAllDetails(String strPolicyId) {
    return (HashMap) ingBusProcess.BF8020_Append(Authenticate, strPolicyId,
        "01");
  }

  /**
   * This function triggers auto-appraisal for a given policy. It then returns
   * to the caller whether the application was approved or rejected
   *
   * @param strPolicyId
   * @param clientID
   * @return
   */
  public HashMap triggerAutoApp(String strPolicyId, String clientID) {
    try {

      HashMap hs = (HashMap) ingBusProcess.BF8020_Append(Authenticate,
          strPolicyId, "01");
      HashMap hs2 = (HashMap) ingBusProcess.BF0592_Append(Authenticate,
          strPolicyId, clientID);
      hs2.put("COVERAGE_DECISION", hs.get("COVERAGE_DECISION"));
      return hs2;
      // CLEARCASE(String Authenticate[],String strPolicyId);
    } catch (Exception ex) {
      TheLogger.error(ex.getMessage());
    }
    return null;
  }

  /**
   * This function updates a requirement record (status and/or Filenet Image
   * Reference #)
   *
   * @param strClientId
   * @param strSeqNum
   * @param strNewReqStatus
   * @param strReqId
   * @param strNewRefNo
   */
  public void updateClientRequirement_void(String strClientId,
                                           String strSeqNum, String strNewReqStatus, String strReqId,
                                           String strNewRefNo) {
    TheLogger.info("Update Client Requirement...");
    try {
      HashMap hashMap = (HashMap) ingBusProcess.BF1192C_Append(
          Authenticate, strClientId, strSeqNum, strNewReqStatus,
          strReqId, strNewRefNo);
      // printHashMap(hashMap);
    } catch (Exception ex) {
      TheLogger.error(ex.getMessage());
    }
  }

  /**
   * This function updates a requirement record (status and/or Filenet Image
   * Reference #)
   *
   * @param strClientId
   * @param strSeqNum
   * @param strNewReqStatus
   * @param strReqId
   */
  public void updateClientRequirement_void(String strClientId,
                                           String strSeqNum, String strNewReqStatus, String strReqId) {
    TheLogger.info("updateClientRequirement_void");
    try {
      HashMap hashMap = (HashMap) ingBusProcess.BF1192C_Append(
          Authenticate, strClientId, strSeqNum, strNewReqStatus,
          strReqId, "");
      // printHashMap(hashMap);
    } catch (Exception ex) {
      TheLogger.error(ex.getMessage());
    }
  }

  /**
   * This function updates a requirement record (status and/or Filenet Image
   * Reference #)
   *
   * @param strClientId
   * @param strSeqNum
   * @param strReqId
   */
  public void updateClientRequirement_void(String strClientId,
                                           String strSeqNum, String strReqId) {
    TheLogger.info("updateClientRequirement_void");
    try {
      HashMap hashMap = (HashMap) ingBusProcess.BF1192C_Append(
          Authenticate, strClientId, strSeqNum, "", strReqId, "");
      // printHashMap(hashMap);
    } catch (Exception ex) {
      TheLogger.error(ex.getMessage());
    }
  }

  /**
   * This function updates a requirement record (status and/or Filenet Image
   * Reference #)
   *
   * @param strPolicyId
   * @param strSeqNum
   * @param strNewReqStatus
   * @param strReqId
   * @param strNewRefNo
   */
  public void updatePolicyRequirement_void(String strPolicyId,
                                           String strSeqNum, String strNewReqStatus, String strReqId,
                                           String strNewRefNo) {
    TheLogger.info("updatePolicyRequirement_void");
    try {
      HashMap hashMap = (HashMap) ingBusProcess.BF1192_Append(
          Authenticate, strPolicyId, strSeqNum, strNewReqStatus,
          strReqId, strNewRefNo);
      // printHashMap(hashMap);
    } catch (Exception ex) {
      TheLogger.error(ex.getMessage());
    }
  }

  /**
   * This function updates a requirement record (status and/or Filenet Image
   * Reference #)
   *
   * @param strPolicyId
   * @param strSeqNum
   * @param strNewReqStatus
   * @param strReqId
   */
  public void updatePolicyRequirement_void(String strPolicyId,
                                           String strSeqNum, String strNewReqStatus, String strReqId) {
    TheLogger.info("updatePolicyRequirement_void");
    try {
      HashMap hashMap = (HashMap) ingBusProcess.BF1192_Append(
          Authenticate, strPolicyId, strSeqNum, strNewReqStatus,
          strReqId, "");
      // printHashMap(hashMap);
    } catch (Exception ex) {
      TheLogger.error(ex.getMessage());
    }
  }

  /**
   * This function updates a requirement record (status and/or Filenet Image
   * Reference #)
   *
   * @param strPolicyId
   * @param strSeqNum
   * @param strReqId
   */
  public void updatePolicyRequirement_void(String strPolicyId,
                                           String strSeqNum, String strReqId) {
    TheLogger.info("updatePolicyRequirement_void");
    try {
      HashMap hashMap = (HashMap) ingBusProcess.BF1192_Append(
          Authenticate, strPolicyId, strSeqNum, "", strReqId, "");
      // printHashMap(hashMap);
    } catch (Exception ex) {
      TheLogger.error(ex.getMessage());
    }
  }

  /**
   * This function updates a requirement record (status and/or Filenet Image
   * Reference #)
   *
   * @param strClientId
   * @param strSeqNum
   * @param strNewReqStatus
   * @param strReqId
   * @param strNewRefNo
   * @return
   */
  public HashMap updateClientRequirement(String strClientId,
                                         String strSeqNum, String strNewReqStatus, String strReqId,
                                         String strNewRefNo) {
    TheLogger.info("updateClientRequirement");
    HashMap hashMap = null;
    try {
      hashMap = (HashMap) ingBusProcess.BF1192C_Append(Authenticate,
          strClientId, strSeqNum, strNewReqStatus, strReqId,
          strNewRefNo);
      // printHashMap(hashMap);
    } catch (Exception ex) {
      TheLogger.error(ex.getMessage());
    }
    return hashMap;
  }

  /**
   * This function updates a requirement record (status and/or Filenet Image
   * Reference #)
   *
   * @param strClientId
   * @param strSeqNum
   * @param strNewReqStatus
   * @param strReqId
   * @return
   */
  public HashMap updateClientRequirement(String strClientId,
                                         String strSeqNum, String strNewReqStatus, String strReqId) {
    TheLogger.info("updateClientRequirement");
    HashMap hashMap = null;
    try {

      hashMap = (HashMap) ingBusProcess.BF1192C_Append(Authenticate,
          strClientId, strSeqNum, strNewReqStatus, strReqId, "");
      // printHashMap(hashMap);
    } catch (Exception ex) {
      TheLogger.error(ex.getMessage());
    }
    return hashMap;
  }

  /**
   * This function updates a requirement record (status and/or Filenet Image
   * Reference #)
   *
   * @param strClientId
   * @param strSeqNum
   * @param strReqId
   * @return
   */
  public HashMap updateClientRequirement(String strClientId,
                                         String strSeqNum, String strReqId) {
    TheLogger.info("updateClientRequirement");
    HashMap hashMap = null;
    try {
      hashMap = (HashMap) ingBusProcess.BF1192C_Append(Authenticate,
          strClientId, strSeqNum, "", strReqId, "");
      // printHashMap(hashMap);
    } catch (Exception ex) {
      TheLogger.error(ex.getMessage());
    }
    return hashMap;
  }

  /**
   * This function updates a requirement record (status and/or Filenet Image
   * Reference #)
   *
   * @param strPolicyId
   * @param strSeqNum
   * @param strNewReqStatus
   * @param strReqId
   * @param strNewRefNo
   * @return
   */
  public HashMap updatePolicyRequirement(String strPolicyId,
                                         String strSeqNum, String strNewReqStatus, String strReqId,
                                         String strNewRefNo) {
    TheLogger.info("updatePolicyRequirement");
    HashMap hashMap = null;
    try {
      hashMap = (HashMap) ingBusProcess.BF1192_Append(Authenticate,
          strPolicyId, strSeqNum, strNewReqStatus, strReqId,
          strNewRefNo);
    } catch (Exception ex) {
      TheLogger.error(ex.getMessage());
    }
    return hashMap;
  }

  /**
   * This function updates a requirement record (status and/or Filenet Image
   * Reference #)
   *
   * @param strPolicyId
   * @param strSeqNum
   * @param strNewReqStatus
   * @param strReqId
   * @return
   */
  public HashMap updatePolicyRequirement(String strPolicyId,
                                         String strSeqNum, String strNewReqStatus, String strReqId) {
    TheLogger.info("updatePolicyRequirement");
    HashMap hashMap = null;
    try {
      hashMap = (HashMap) ingBusProcess.BF1192_Append(Authenticate,
          strPolicyId, strSeqNum, strNewReqStatus, strReqId, "");
    } catch (Exception ex) {
      TheLogger.error(ex.getMessage());
    }
    return hashMap;
  }

  /**
   * This function updates a requirement record (status and/or Filenet Image
   * Reference #)
   *
   * @param strPolicyId
   * @param strSeqNum
   * @param strReqId
   * @return
   */
  public HashMap updatePolicyRequirement(String strPolicyId,
                                         String strSeqNum, String strReqId) {
    TheLogger.info("updatePolicyRequirement");
    HashMap hashMap = null;
    try {
      hashMap = (HashMap) ingBusProcess.BF1192_Append(Authenticate,
          strPolicyId, strSeqNum, "", strReqId, "");
    } catch (Exception ex) {
      TheLogger.error(ex.getMessage());
    }
    return hashMap;
  }

  /**
   * This function retrieves all policy-related requirements for a given
   * policy
   *
   * @param strPolicyId
   * @return
   */
  public HashMap getPolicyRequirements(String strPolicyId) {
    TheLogger.info("updatePolicyRequirement");
    HashMap hashMap = null;
    try {
      hashMap = (HashMap) ingBusProcess.BF1194_Append(Authenticate,
          strPolicyId);
      printHashMap(hashMap);
    } catch (Exception ex) {
      TheLogger.error(ex.getMessage());
    }
    return hashMap;
  }

  /**
   * This function retrieves all client-related requirements for a given
   * client
   *
   * @param strClientId
   * @return
   */
  public HashMap getClientRequirements(String strClientId) {
    TheLogger.info("updatePolicyRequirement");
    HashMap hashMap = null;
    try {
      hashMap = (HashMap) ingBusProcess.BF1194C_Append(Authenticate,
          strClientId);
    } catch (Exception ex) {
      TheLogger.error(ex.getMessage());
    }
    return hashMap;
  }

  /**
   * This function retrieves all requirements (policy and client related) for
   * a given policy
   *
   * @param strPolicyId
   * @param insuredId
   * @param OwnerId
   * @return
   */
  public HashMap getRequirements(String strPolicyId, String insuredId,
                                 String OwnerId) {
    TheLogger.info("getRequirements");
    HashMap hashMap;
    String strOwnerId;
    String strInsuredId;

    HashMap PolicyHashMap = (HashMap) ingBusProcess.BF1194_Append(
        Authenticate, strPolicyId);
    strOwnerId = OwnerId;

    strInsuredId = insuredId;

    hashMap = new HashMap();
    hashMap.put("POLICY", PolicyHashMap);
    if (strOwnerId.equals(strInsuredId)) {
      hashMap.put("OWNER", (HashMap) ingBusProcess.BF1194C_Append(
          Authenticate, strInsuredId));
    } else {
      hashMap.put("OWNER", (HashMap) ingBusProcess.BF1194C_Append(
          Authenticate, strOwnerId));
      hashMap.put("INSURED", (HashMap) ingBusProcess.BF1194C_Append(
          Authenticate, strInsuredId));
    }

    return hashMap;
  }

  /**
   * This function retrieves all requirements (policy and client related) for
   * a given policy
   *
   * @param reqts
   * @param SEQUENCE_NUMBERS
   * @param REQUIREMENT_CODE
   * @param INDEX_NUMBERS
   * @param STAT_CODES
   * @param TYPE
   */
  private void consoliteRequirements(HashMap reqts,
                                     ArrayList SEQUENCE_NUMBERS, ArrayList REQUIREMENT_CODE,
                                     ArrayList INDEX_NUMBERS, ArrayList STAT_CODES, String TYPE) {
    TheLogger.info("consoliteRequirements");
    Iterator seq = SEQUENCE_NUMBERS.iterator();
    Iterator reqt = REQUIREMENT_CODE.iterator();
    Iterator index = INDEX_NUMBERS.iterator();
    Iterator stat = STAT_CODES.iterator();

    ArrayList seqA = (ArrayList) reqts.get("SEQUENCE_NUMBERS");
    ArrayList reqtA = (ArrayList) reqts.get("REQUIREMENT_CODE");
    ArrayList indexA = (ArrayList) reqts.get("INDEX_NUMBERS");
    ArrayList statA = (ArrayList) reqts.get("STAT_CODES");
    ArrayList typeA = (ArrayList) reqts.get("TYPE");

    while (reqt.hasNext()) {
      String sequence = "";
      String requirements = (String) reqt.next();
      String indexNumber = "";
      String statsCodes = "";

      if (requirements.equalsIgnoreCase("*")) {
        seq.next();
        index.next();
        stat.next();
        continue;
      }

      try {
        sequence = (String) seq.next();
      } catch (Exception e) {
        e.printStackTrace();
      }

      try {
        indexNumber = (String) index.next();
      } catch (Exception e) {
        e.printStackTrace();
      }

      try {
        statsCodes = (String) stat.next();
      } catch (Exception e) {
        e.printStackTrace();
      }
      seqA.add(sequence);
      reqtA.add(requirements);
      indexA.add(indexNumber);
      statA.add(statsCodes);
      typeA.add(TYPE);
    }
  }

  /**
   * Policy Inquiry
   *
   * @param strPolicyId
   * @return
   */
  public HashMap getPolicyInquiry(String strPolicyId) {
    TheLogger.info("getPolicyInquiry");
    HashMap hashMap = null;
    HashMap hashRet = new HashMap();
    try {
      hashMap = (HashMap) ingBusProcess.BF8000_Append(Authenticate,
          strPolicyId);
      List OWNER_RELATIONSHIP_TO_INSURED = IngeniumConstants
          .getHashMapListValue("OWNER_RELATIONSHIP_TO_INSURED",
              hashMap);
      List OWNER_CLIENT_RELATIONSHIP_TYPE = IngeniumConstants
          .getHashMapListValue("OWNER_CLIENT_RELATIONSHIP_TYPE",
              hashMap);
      List CLIENT_ADDRESS_TYPE = IngeniumConstants.getHashMapListValue(
          "CLIENT_ADDRESS_TYPE", hashMap);
      List SERVICING_FEE_OPTION = IngeniumConstants.getHashMapListValue(
          "SERVICING_FEE_OPTION", hashMap);

      List ADDRESS_TYPE_ = IngeniumConstants.getHashMapListValue(
          "ADDRESS_TYPE_", hashMap);
      List CLIENT_ID_AR = IngeniumConstants.getHashMapListValue(
          "CLIENT_ID_AR", hashMap);
      List POLICY_INFORMATION_ASSIGNEE = IngeniumConstants
          .getHashMapListValue("POLICY_INFORMATION_ASSIGNEE", hashMap);
      List EFFECTIVE_DATE_OF_ASSIGNMENT = IngeniumConstants
          .getHashMapListValue("EFFECTIVE_DATE_OF_ASSIGNMENT",
              hashMap);
      List RELATIONSHIP_TYPE = IngeniumConstants.getHashMapListValue(
          "RELATIONSHIP_TYPE", hashMap);

      /*
       * Gross Premium Amount Policy Inquiry - all details GFrag 8/12/2014
       */
      List GROSS_PREMIUM_AMOUNT = IngeniumConstants.getHashMapListValue(
          "GROSS_PREMIUM_AMOUNT", hashMap);

      // ADDRESS_TYPE_
      // CLIENT_ID_AR
      // POLICY_INFORMATION_ASSIGNEE
      // EFFECTIVE_DATE_OF_ASSIGNMENT
      // RELATIONSHIP_TYPE
      System.out.println("ADDRESS_TYPE_" + ADDRESS_TYPE_.size());
      System.out.println("CLIENT_ID_AR" + CLIENT_ID_AR.size());
      System.out.println("POLICY_INFORMATION_ASSIGNEE"
          + POLICY_INFORMATION_ASSIGNEE.size());
      System.out.println("EFFECTIVE_DATE_OF_ASSIGNMENT"
          + EFFECTIVE_DATE_OF_ASSIGNMENT.size());
      System.out.println("RELATIONSHIP_TYPE" + RELATIONSHIP_TYPE.size());

      /*
       * Gross Premium Amount Policy Inquiry - all details GFrag 8/12/2014
       */
      List grsPremiumAmt = new ArrayList();
      for (int i = 0; i < GROSS_PREMIUM_AMOUNT.size(); i++) {
        grsPremiumAmt.add(GROSS_PREMIUM_AMOUNT.get(i).toString());
      }
      // END -GFraga

      List addressType_ = new ArrayList();
      for (int i = 0; i < ADDRESS_TYPE_.size() - 1; i++) {
        System.out.println("ADDRESS_TYPE_.get(i).toString() "
            + ADDRESS_TYPE_.get(i).toString());
        addressType_.add(ADDRESS_TYPE_.get(i).toString());
      }

      hashMap.put("ADDRESS_TYPE_", addressType_);

      List clientIdAr = new ArrayList();
      for (int i = 0; i < CLIENT_ID_AR.size() - 1; i++) {
        clientIdAr.add(CLIENT_ID_AR.get(i).toString());
      }

      hashMap.put("CLIENT_ID_AR", clientIdAr);

      List policyInformationAssignee = new ArrayList();
      for (int i = 0; i < POLICY_INFORMATION_ASSIGNEE.size() - 1; i++) {
        policyInformationAssignee.add(POLICY_INFORMATION_ASSIGNEE
            .get(i).toString());
      }

      hashMap.put("POLICY_INFORMATION_ASSIGNEE",
          policyInformationAssignee);

      List effectiveDateOfAssignment = new ArrayList();
      for (int i = 0; i < EFFECTIVE_DATE_OF_ASSIGNMENT.size() - 1; i++) {
        effectiveDateOfAssignment.add(EFFECTIVE_DATE_OF_ASSIGNMENT.get(
            i).toString());
      }

      hashMap.put("EFFECTIVE_DATE_OF_ASSIGNMENT",
          effectiveDateOfAssignment);

      List relationshipType = new ArrayList();
      for (int i = 0; i < RELATIONSHIP_TYPE.size() - 1; i++) {
        relationshipType.add(RELATIONSHIP_TYPE.get(i).toString());
      }

      hashMap.put("RELATIONSHIP_TYPE", relationshipType);

      List ownerRelToInsDesc = new ArrayList();
      for (int i = 0; i < OWNER_RELATIONSHIP_TO_INSURED.size(); i++) {
        ownerRelToInsDesc
            .add(IngeniumConstants
                .getStatusDesc(IngeniumConstants.OWNER_RELATIONSHIP_TO_INSURED_
                    + OWNER_RELATIONSHIP_TO_INSURED.get(i)
                    .toString()));
        System.out.println("OWNER_RELATIONSHIP_TO_INSURED: "
            + OWNER_RELATIONSHIP_TO_INSURED.get(i).toString());
      }

      List ownerCliRelTypeDesc = new ArrayList();
      for (int i = 0; i < OWNER_CLIENT_RELATIONSHIP_TYPE.size(); i++) {
        ownerCliRelTypeDesc
            .add(IngeniumConstants
                .getStatusDesc(IngeniumConstants.OWNER_CLIENT_RELATIONSHIP_TYPE_
                    + OWNER_CLIENT_RELATIONSHIP_TYPE.get(i)
                    .toString()));
      }

      List cliAddTypeDesc = new ArrayList();
      for (int i = 0; i < CLIENT_ADDRESS_TYPE.size(); i++) {
        cliAddTypeDesc.add(IngeniumConstants
            .getStatusDesc(IngeniumConstants.CLIENT_ADDRESS_TYPE_
                + CLIENT_ADDRESS_TYPE.get(i).toString()));
      }

      List servicingFeeOptDesc = new ArrayList();
      for (int i = 0; i < SERVICING_FEE_OPTION.size(); i++) {
        servicingFeeOptDesc.add(IngeniumConstants
            .getStatusDesc(IngeniumConstants.SERVICING_FEE_OPTION_
                + SERVICING_FEE_OPTION.get(i).toString()));
      }

      List addressTypeDesc = new ArrayList();
      for (int i = 0; i < ADDRESS_TYPE_.size() - 1; i++) {
        addressTypeDesc.add(IngeniumConstants
            .getStatusDesc(IngeniumConstants.ADDRESS_TYPE__
                + ADDRESS_TYPE_.get(i).toString()));
      }

      List relTypeDesc = new ArrayList();
      for (int i = 0; i < RELATIONSHIP_TYPE.size() - 1; i++) {
        relTypeDesc.add(IngeniumConstants
            .getStatusDesc(IngeniumConstants.RELATIONSHIP_TYPE_
                + RELATIONSHIP_TYPE.get(i).toString()));
      }

      hashMap.put(IngeniumConstants.OWNER_RELATIONSHIP_TO_INSURED_DESC,
          ownerRelToInsDesc);
      hashMap.put(IngeniumConstants.OWNER_CLIENT_RELATIONSHIP_TYPE_DESC,
          ownerCliRelTypeDesc);
      hashMap.put(IngeniumConstants.CLIENT_ADDRESS_TYPE_DESC,
          cliAddTypeDesc);
      hashMap.put(IngeniumConstants.SERVICING_FEE_OPTION_DESC,
          servicingFeeOptDesc);
      hashMap.put(IngeniumConstants.ADDRESS_TYPE__DESC, addressTypeDesc);
      hashMap.put(IngeniumConstants.RELATIONSHIP_TYPE_DESC, relTypeDesc);

    } catch (Exception ex) {
      TheLogger.error(ex.getMessage());
    }
    return hashMap;
  }

  /**
   * Get requirements
   *
   * @param strPolicyId
   * @return
   */
  public HashMap getRequirements(String strPolicyId) {
    TheLogger.info("getRequirements");
    HashMap hashMap;
    String strOwnerId;
    String strInsuredId;
    ArrayList al_Temp;

    HashMap PolicyHashMap = (HashMap) ingBusProcess.BF1194_Append(
        Authenticate, strPolicyId);
    HashMap OwnerHashMap = (HashMap) ingBusProcess.BF8000_Append(
        Authenticate, strPolicyId);
    HashMap InsuredHashMap = (HashMap) ingBusProcess.BF6925_Append(
        Authenticate, strPolicyId, "00");

    al_Temp = (ArrayList) OwnerHashMap.get("CLIENT_NO");
    strOwnerId = (String) al_Temp.get(0);

    al_Temp = (ArrayList) InsuredHashMap.get("INSURED_CLIENT_NO");
    strInsuredId = (String) al_Temp.get(0);

    hashMap = new HashMap();
    hashMap.put("SEQUENCE_NUMBERS", new ArrayList());
    hashMap.put("REQUIREMENT_CODE", new ArrayList());
    hashMap.put("INDEX_NUMBERS", new ArrayList());
    hashMap.put("STAT_CODES", new ArrayList());
    hashMap.put("TYPE", new ArrayList());

    try {
      consoliteRequirements(hashMap,
          (ArrayList) PolicyHashMap.get("SEQUENCE_NUMBERS"),
          (ArrayList) PolicyHashMap.get("REQUIREMENT_CODE"),
          (ArrayList) PolicyHashMap.get("INDEX_NUMBERS"),
          (ArrayList) PolicyHashMap.get("STAT_CODES"), "Policy");
    } catch (Exception e) {
      e.printStackTrace();
    }

    if (strOwnerId.equals(strInsuredId)) {
      HashMap hs = (HashMap) ingBusProcess.BF1194C_Append(Authenticate,
          strInsuredId);
      System.out.println("SAME");
      try {
        consoliteRequirements(hashMap,
            (ArrayList) hs.get("SEQUENCE_NUMBERS"),
            (ArrayList) hs.get("REQUIREMENT_CODE"),
            (ArrayList) hs.get("INDEX_NUMBERS"),
            (ArrayList) hs.get("STAT_CODES"), "Owner");
      } catch (Exception e) {
        e.printStackTrace();
      }
    } else {
      // System.out.println("NOT SAME");
      try {
        HashMap hs = (HashMap) ingBusProcess.BF1194C_Append(
            Authenticate, strOwnerId);
        consoliteRequirements(hashMap,
            (ArrayList) hs.get("SEQUENCE_NUMBERS"),
            (ArrayList) hs.get("REQUIREMENT_CODE"),
            (ArrayList) hs.get("INDEX_NUMBERS"),
            (ArrayList) hs.get("STAT_CODES"), "Owner");
      } catch (Exception e) {
        e.printStackTrace();
      }

      try {
        HashMap hs = (HashMap) ingBusProcess.BF1194C_Append(
            Authenticate, strInsuredId);
        consoliteRequirements(hashMap,
            (ArrayList) hs.get("SEQUENCE_NUMBERS"),
            (ArrayList) hs.get("REQUIREMENT_CODE"),
            (ArrayList) hs.get("INDEX_NUMBERS"),
            (ArrayList) hs.get("STAT_CODES"), "Insured");
      } catch (Exception e) {
        e.printStackTrace();
      }
    }

    al_Temp = null;
    return hashMap;
  }

  /**
   * This function retrieves payment details about a given policy (Amount
   * Paid, Refund Amount, etc.)
   *
   * @param strPolicyId
   * @return
   */
  public HashMap getPolicyPaymentDetails(String strPolicyId) {
    TheLogger.info("getPolicyPaymentDetails");
    HashMap hashMap = null;
    try {
      hashMap = (HashMap) ingBusProcess.BF6983_Append(Authenticate,
          strPolicyId);
    } catch (Exception ex) {
      TheLogger.error(ex.getMessage());
    }
    return hashMap;
  }

  /**
   * This function retrieves a list of beneficiaries for all coverages in a
   * given Policy
   *
   * @param strPolicyId
   * @return
   */
  public HashMap getPolicyBeneficiaries(String strPolicyId) {
    // Commented. Unless BF8000_Append updates some data on ING.
    // HashMap hashMap;
    // hashMap = (HashMap) ingBusProcess.BF8000_Append(Authenticate,
    // strPolicyId);
    //
    // if (hashMap != null) {
    // ArrayList tempArray = (ArrayList) hashMap.get("COVERAGE_COUNT");
    //
    // if (tempArray != null) {
    // String strTemp = (String) tempArray.get(0);
    // int nCounter = 0;
    //
    // while (nCounter < Integer.parseInt(strTemp)) {
    // // getPolicyCoverageDetails
    // // getCoverageBeneficiaries(strPolicyId, "0"+(nCounter+1));
    // System.out.println("Coverage Counter is " + "0"
    // + (nCounter + 1));
    // nCounter++;
    // // BENEFICIARY_NAME
    // }
    // }
    // }
    HashMap hashMap;
    hashMap = getCoverageBeneficiaries(strPolicyId, "01");

    return hashMap;
  }

  /**
   * This function retrieves a list of beneficiaries for a specific coverage
   *
   * @param strPolicyId
   * @param strCoverageNo
   * @return
   */
  public HashMap getCoverageBeneficiaries(String strPolicyId,
                                          String strCoverageNo) {
    HashMap hashMap;
    hashMap = (HashMap) ingBusProcess.BF6945_Append(Authenticate,
        strPolicyId, strCoverageNo);
    printHashMap(hashMap);

    return hashMap;
  }

  /**
   * This function retrieves details for all coverages of a given policy (Name
   * of insured, policy number, plan, Face Amount, etc.)
   *
   * @param strPolicyId
   * @param CvgNum
   * @return
   */
  public HashMap getPolicyCoverageDetails(String strPolicyId, String CvgNum) {
    TheLogger.info("getPolicyCoverageDetails");
    HashMap hashMap;
    hashMap = (HashMap) ingBusProcess.BF6925_Append(Authenticate,
        strPolicyId, CvgNum);
    return hashMap;
  }

  /**
   * This function retrieves the client information for a given policy (both
   * for the Policy Owner and the Insured) A new parameter is added which is
   * to include the tradIndicator, VUL or TRAD are accepted as inputs
   *
   *
   * @param strPolicyId
   * @return
   */
  public HashMap getPolicyClientDetails(String strPolicyId) {
    HashMap hashMap;
    HashMap HashPolOwner = this.getPolicyOwnerDetails(strPolicyId);
    HashMap HasInsDetails = this.getPolicyInsuredDetails(strPolicyId, "01");
    hashMap = new HashMap();
    if (HashPolOwner != null) {
      if (HasInsDetails != null) {
        if (HashPolOwner.get("BRANCH_CODE") != null)
          hashMap.put("BRANCH_CODE", HashPolOwner.get("BRANCH_CODE"));
        if (HashPolOwner.get("SERVC_AGENT_CLIENT_CODE") != null)
          hashMap.put("SERVC_AGENT_CLIENT_CODE",
              HashPolOwner.get("SERVC_AGENT_CLIENT_CODE"));
        if (HashPolOwner.get("APP_SIGN_DATE") != null)
          hashMap.put("APP_SIGN_DATE",
              HashPolOwner.get("APP_SIGN_DATE"));
        if (HashPolOwner.get("OWNER_TITLE") != null)
          hashMap.put("OWNER_TITLE", HashPolOwner.get("OWNER_TITLE"));
        if (HashPolOwner.get("CLIENT_FIRSTNAME") != null)
          hashMap.put("CLIENT_FIRSTNAME",
              HashPolOwner.get("CLIENT_FIRSTNAME"));
        if (HashPolOwner.get("CLIENT_LASTNAME") != null)
          hashMap.put("CLIENT_LASTNAME",
              HashPolOwner.get("CLIENT_LASTNAME"));
        if (HashPolOwner.get("CLIENT_MIDDLENAME") != null)
          hashMap.put("CLIENT_MIDDLENAME",
              HashPolOwner.get("CLIENT_MIDDLENAME"));
        if (HashPolOwner.get("ADDRESS_1") != null)
          hashMap.put("ADDRESS_1", HashPolOwner.get("ADDRESS_1"));
        if (HashPolOwner.get("ADDRESS_2") != null)
          hashMap.put("ADDRESS_2", HashPolOwner.get("ADDRESS_2"));
        if (HashPolOwner.get("ADDRESS_3") != null)
          hashMap.put("ADDRESS_3", HashPolOwner.get("ADDRESS_3"));
        if (HashPolOwner.get("ADDRESS_4") != null)
          hashMap.put("ADDRESS_4", HashPolOwner.get("ADDRESS_4"));
        if (HashPolOwner.get("ADDRESS_5") != null)
          hashMap.put("ADDRESS_5", HashPolOwner.get("ADDRESS_5"));
        if (HashPolOwner.get("CITY") != null)
          hashMap.put("CITY", HashPolOwner.get("CITY"));
        if (HashPolOwner.get("PROVINCE_CODE") != null)
          hashMap.put("PROVINCE_CODE",
              HashPolOwner.get("PROVINCE_CODE"));
        if (HashPolOwner.get("CLIENT_BDAY") != null)
          hashMap.put("CLIENT_BDAY", HashPolOwner.get("CLIENT_BDAY"));
        if (HashPolOwner.get("PLAN_ID") != null)
          hashMap.put("PLAN_ID", HashPolOwner.get("PLAN_ID"));
        if (HashPolOwner.get("INS_TYPE_CODE") != null)
          hashMap.put("INS_TYPE_CODE",
              HashPolOwner.get("INS_TYPE_CODE"));
        if (HashPolOwner.get("OWNER_ID") != null)
          hashMap.put("OWNER_ID", HashPolOwner.get("OWNER_ID"));
        if (HashPolOwner.get("BRANCH") != null)
          hashMap.put("BRANCH", HashPolOwner.get("BRANCH"));
        if (HashPolOwner.get("COMPANY_NAME") != null)
          hashMap.put("COMPANY_NAME",
              HashPolOwner.get("COMPANY_NAME"));
        if (HashPolOwner.get("AGENT_CODE") != null)
          hashMap.put("AGENT_CODE", HashPolOwner.get("AGENT_CODE"));
        if (HashPolOwner.get("AGENT_NAME") != null)
          hashMap.put("AGENT_NAME", HashPolOwner.get("AGENT_NAME"));
        if (HashPolOwner.get("SERVICING_AGENT_NAME") != null)
          hashMap.put("SERVICING_AGENT_NAME",
              HashPolOwner.get("SERVICING_AGENT_NAME"));
        if (HashPolOwner.get("ZIPCODE") != null)
          hashMap.put("ZIPCODE", HashPolOwner.get("ZIPCODE"));
        else
          hashMap.put("ZIPCODE", HashPolOwner.get("ZIPCODE_PREVIOUS"));
        if (HashPolOwner.get("COUNTRY_CODE") != null)
          hashMap.put("COUNTRY_CODE",
              HashPolOwner.get("COUNTRY_CODE"));
        if (HashPolOwner.get("POLICY_STATUS_DESC") != null)
          hashMap.put("POLICY_STATUS_DESC",
              HashPolOwner.get("POLICY_STATUS_DESC"));
        if (HashPolOwner.get("CURRENCY_CODE") != null)
          hashMap.put("CURRENCY_CODE",
              HashPolOwner.get("CURRENCY_CODE"));
        if (HashPolOwner.get("SEX") != null)
          hashMap.put("SEX", HashPolOwner.get("SEX"));
        if (HasInsDetails.get("INSURED_NAME") != null)
          hashMap.put("INSURED_NAME",
              HasInsDetails.get("INSURED_NAME"));
        if (HasInsDetails.get("INSURED_CLIENT_NO") != null)
          hashMap.put("INSURED_CLIENT_NO",
              HasInsDetails.get("INSURED_CLIENT_NO"));
        if (HasInsDetails.get("ORIGINAL_FACE_AMOUNT") != null)
          hashMap.put("ORIGINAL_FACE_AMOUNT",
              HasInsDetails.get("ORIGINAL_FACE_AMOUNT"));
        if (HasInsDetails.get("COVERAGE_PLAN_DESC") != null)
          hashMap.put("COVERAGE_PLAN_DESC",
              HasInsDetails.get("COVERAGE_PLAN_DESC"));
      }
    }

    System.out
        .println("//.............getPolicyClientDetails...........//");
    printHashMap(hashMap);
    System.out
        .println("//.............getPolicyClientDetails...........//");
    return hashMap;
  }

  /**
   * This function retrieves information about the Client of a given policy
   *
   * @param strPolicyId
   * @param CvgNum
   * @return
   */
  public HashMap getPolicyInsuredDetails(String strPolicyId, String CvgNum) {
    TheLogger.info("getPolicyInsuredDetails");
    HashMap hashMap = (HashMap) ingBusProcess.BF6925_Append(Authenticate,
        strPolicyId, CvgNum);
    return hashMap;
  }

  /**
   * This function retrieves the client information given client ID
   *
   * @param strClientId
   * @return
   */
  public HashMap getClientDetails(String strClientId) {

    TheLogger.info("getClientDetails");
    HashMap hashMap = (HashMap) ingBusProcess.BF1220C_Append(Authenticate, strClientId);

		/*
		 *  modified: vsantos
		 *  date: 1/15/2014
		 *  add list bill codes
		 *
			LIST_BILL_EMPLOYEE_BRANCH - will not include, code is same as description
			LIST_BILL_STATION - will not include, code is same as description
			LIST_BILL_UNIT - not included in list
		 */

    System.out.println("testing new ingenium in getclientdetails");

    //LIST_BILL_REGION
    List regionCode = IngeniumConstants.getHashMapListValue("LIST_BILL_REGION", hashMap);
    List region = new ArrayList();
    //replace space with empty string, region codes from ingenium contain spaces
    region.add(IngeniumConstants.getStatusDesc("LIST_BILL_REGION_" + (IngeniumConstants.getListValue(regionCode)).replace(" ", ""), IngeniumConstants.getListValue(regionCode)));
    hashMap.put("LIST_BILL_REGION", region);


    //LIST_BILL_EMPLOYEE_RANK
    List rankCode = IngeniumConstants.getHashMapListValue("LIST_BILL_EMPLOYEE_RANK", hashMap);
    List rank = new ArrayList();
    rank.add(IngeniumConstants.getStatusDesc("LIST_BILL_EMPLOYEE_RANK_" + (IngeniumConstants.getListValue(rankCode)).replace(" ", ""), IngeniumConstants.getListValue(rankCode)));
    hashMap.put("LIST_BILL_EMPLOYEE_RANK", rank);


    //LIST_BILL_DIVISION
    List divisionCode = IngeniumConstants.getHashMapListValue("C", hashMap);
    List division = new ArrayList();
    division.add(IngeniumConstants.getStatusDesc("LIST_BILL_DIVISION_" + (IngeniumConstants.getListValue(divisionCode)).replace(" ", ""), IngeniumConstants.getListValue(divisionCode)));
    hashMap.put("LIST_BILL_DIVISION", division);


    //CLIENT_FLAG_INDICIA - GFraga 10/14/2014 for FATCA
    List CLIENT_FLAG_INDICIA = IngeniumConstants.getHashMapListValue("CLIENT_FLAG_INDICIA", hashMap);

    //Change 1.4 (Cshells Jan162015) **START**
    String indicia = "";
    List cltFlgIndicia = new ArrayList();
    List tmpclntFlgIndicia = new ArrayList();

    try {
      if (CLIENT_FLAG_INDICIA != null && CLIENT_FLAG_INDICIA.size() > 0) {
        System.out.println("BEFORE INDICIA: " + CLIENT_FLAG_INDICIA.get(0) + ":" + CLIENT_FLAG_INDICIA.size());

        indicia = (String) CLIENT_FLAG_INDICIA.get(0);
        if ("1".equals(indicia)) {
          tmpclntFlgIndicia.add("Non-US Person");
        } else if ("2".equals(indicia)) {
          tmpclntFlgIndicia.add("US Indicia Found");
        } else {
          tmpclntFlgIndicia.add(" ");
        }
      } else {
        tmpclntFlgIndicia.add(" ");
      }
      hashMap.put("CLIENT_FLAG_INDICIA", tmpclntFlgIndicia);
    } catch (Exception e) {
      hashMap.put("CLIENT_FLAG_PERSON", tmpclntFlgIndicia);
      System.out.println("[IngeniumDispatcher] Error in getting indicia: " + exceptionStacktraceToString(e));
    }
    //Change 1.4 (Cshells Jan162015) **END**

    //Dessa - US PERSON
    //Change 1.4 (Cshells, Lacsz)
    //Change 1.5 (Cshells)
    System.out.println("START: CONVERT CODES TO DESCRIPTION: US PERSON");
    List listUsPerson = IngeniumConstants.getHashMapListValue("CLIENT_FLAG_PERSON", hashMap);
    try {
      if (listUsPerson != null && listUsPerson.size() > 0) {

        String usPerson = (String) listUsPerson.get(0);

        List tempUsPerson = new ArrayList();
        if ("0".equals(usPerson)) {
          tempUsPerson.add(IngeniumConstants.getStatusDesc(IngeniumConstants.NON_US_PERSON));
        } else if ("2".equals(usPerson)) {
          tempUsPerson.add(IngeniumConstants.getStatusDesc(IngeniumConstants.RECALCITRANT));
        } else if ("3".equals(usPerson)) {
          tempUsPerson.add(IngeniumConstants.getStatusDesc(IngeniumConstants.IN_PROCESS));
        } else if ("4".equals(usPerson)) {
          tempUsPerson.add(IngeniumConstants.getStatusDesc(IngeniumConstants.US_PERSON));
        }

        System.out.println("US PERSON = " + tempUsPerson.get(0).toString());
        hashMap.put("CLIENT_FLAG_PERSON", tempUsPerson);
        System.out.println("END: CONVERT CODES TO DESCRIPTION: US PERSON");
      }
    } catch (Exception e) {
      hashMap.put("CLIENT_FLAG_PERSON", listUsPerson);
      System.out.println("[IngeniumDispatcher] Error in getting listUsPerson: " + exceptionStacktraceToString(e));
    }

    //Dessa - FATCA REASON
    //Change 1.4(Cshells, Lacsz)
    //Change 1.5 (Cshells)
    System.out.println("START: CONVERT CODES TO DESCRIPTION: FATCA REASON");
    List listFatcaReason = IngeniumConstants.getHashMapListValue("FATCA_REASON", hashMap);
    try {

      if (listFatcaReason != null && listFatcaReason.size() > 0) {
        String fatcaReason = (String) listFatcaReason.get(0);

        List tempFatcaReason = new ArrayList();
        if (fatcaReason.equals("01")) {
          fatcaReason = "Awaiting W8/W9";
        } else if (fatcaReason.equals("03")) {
          fatcaReason = "W8 Submitted";
        } else if (fatcaReason.equals("04")) {
          fatcaReason = "W9 Submitted";
        }
        System.out.println("fatca reason: " + fatcaReason);
        tempFatcaReason.add(fatcaReason);
        hashMap.put("FATCA_REASON", tempFatcaReason);
        System.out.println("END: CONVERT CODES TO DESCRIPTION: FATCA REASON");
      }
    } catch (Exception e) {
      hashMap.put("FATCA_REASON", listFatcaReason);
      System.out.println("[IngeniumDispatcher] Error in getting listFatcaReason: " + exceptionStacktraceToString(e));
    }

    //Dessa - FATCA STATUS
    //Change 1.4 (Cshells, Lacsz)
    //Change 1.5 (Cshells)
    System.out.println("START: CONVERT CODES TO DESCRIPTION: FATCA STATUS");
    List listFatcaStat = IngeniumConstants.getHashMapListValue("FATCA_STATUS", hashMap);

    try {

      if (listFatcaStat != null && listFatcaStat.size() > 0) {
        String fatcaStat = (String) listFatcaStat.get(0);

        List tempFatcaStat = new ArrayList();
        if ("2".equals(fatcaStat)) {
          tempFatcaStat.add("Recalcitrant");
        } else if ("3".equals(fatcaStat)) {
          tempFatcaStat.add("In Process");
        } else if ("5".equals(fatcaStat)) {
          tempFatcaStat.add("Complete");
        }

        System.out.println("FATCA STATUS : " + listFatcaStat.get(0).toString());
        hashMap.put("FATCA_STATUS", tempFatcaStat);
        System.out.println("END: CONVERT CODES TO DESCRIPTION: FATCA STATUS");
      }
    } catch (Exception e) {
      hashMap.put("FATCA_STATUS", listFatcaStat);
      System.out.println("[IngeniumDispatcher] Error in getting listFatcaStat: " + exceptionStacktraceToString(e));
    }

    //CITIZENSHIP
    //Change 1.4 (Cshells, Lacsz)
    //Change 1.5 (Cshells)
    System.out.println("START: CONVERT CODES TO DESCRIPTION: CITIZENSHIP");
    List citizenship = IngeniumConstants.getHashMapListValue("CITIZENSHIP", hashMap);
    List citizenship1 = IngeniumConstants.getHashMapListValue("COUNTRY_OF_CITIZENSHIP_1", hashMap);
    List citizenship2 = IngeniumConstants.getHashMapListValue("COUNTRY_OF_CITIZENSHIP_2", hashMap);

    try {
      if (citizenship != null && citizenship.size() > 0) {
        List tempCitizen1 = new ArrayList();
        String strCitizenship = (String) citizenship.get(0);
        tempCitizen1.add(IngeniumConstants.getStatusDesc("CITIZENSHIP_".concat(strCitizenship)));
        System.out.println("sysout : CITIZENSHIP_".concat(strCitizenship));
        System.out.println("citizen1 : " + tempCitizen1.get(0).toString());
        hashMap.put("CITIZENSHIP", tempCitizen1);
      }
    } catch (Exception e) {
      hashMap.put("CITIZENSHIP", citizenship);
      System.out.println("[IngeniumDispatcher] Error in getting citizenship: " + e);
    }

    try {
      if (citizenship1 != null && citizenship1.size() > 0) {
        List tempCitizen2 = new ArrayList();
        String strCitizenship = (String) citizenship1.get(0);
        tempCitizen2.add(IngeniumConstants.getStatusDesc("CITIZENSHIP_".concat(strCitizenship)));
        System.out.println("sysout : CITIZENSHIP_".concat(strCitizenship));
        System.out.println("citi2: " + tempCitizen2.get(0).toString());
        hashMap.put("COUNTRY_OF_CITIZENSHIP_1", tempCitizen2);
      }
    } catch (Exception e) {
      hashMap.put("COUNTRY_OF_CITIZENSHIP_1", citizenship1);
      System.out.println("[IngeniumDispatcher] Error in getting citizenship1: " + exceptionStacktraceToString(e));
    }

    try {
      if (citizenship2 != null && citizenship2.size() > 0) {
        List tempCitizen3 = new ArrayList();
        String strCitizenship = (String) citizenship2.get(0);
        tempCitizen3.add(IngeniumConstants.getStatusDesc("CITIZENSHIP_".concat(strCitizenship)));
        System.out.println("sysout : CITIZENSHIP_".concat(strCitizenship));
        System.out.println("citi3 : " + tempCitizen3.get(0).toString());
        hashMap.put("COUNTRY_OF_CITIZENSHIP_2", tempCitizen3);
      }
    } catch (Exception e) {
      hashMap.put("COUNTRY_OF_CITIZENSHIP_2", citizenship2);
      System.out.println("[IngeniumDispatcher] Error in getting citizenship2: " + e);
    }
    System.out.println("END: CONVERT CODES TO DESCRIPTION: CITIZENSHIP");

    //COUNTRIES OF LEGAL RESIDENCE
    //Change 1.4 (Cshells, Lacsz)
    //Change 1.5 (Cshells)
    System.out.println("START: CONVERT CODES TO DESCRIPTION: COUNTRY_OF_LEGAL_RESIDENCE_1");
    List ctryOfLegalRes1 = IngeniumConstants.getHashMapListValue("COUNTRY_OF_LEGAL_RESIDENCE_1", hashMap);
    List ctryOfLegalRes2 = IngeniumConstants.getHashMapListValue("COUNTRY_OF_LEGAL_RESIDENCE_2", hashMap);
    List ctryOfLegalRes3 = IngeniumConstants.getHashMapListValue("COUNTRY_OF_LEGAL_RESIDENCE_3", hashMap);

    try {
      if (ctryOfLegalRes1 != null && ctryOfLegalRes1.size() > 0) {
        List tempCtryOfLegalRes1 = new ArrayList();
        String strCtryOfLegalRes1 = (String) ctryOfLegalRes1.get(0);
        tempCtryOfLegalRes1.add(IngeniumConstants.getStatusDesc("COUNTRY_".concat(strCtryOfLegalRes1)));
        System.out.println("COUNTRY_OF_LEGAL_RESIDENCE_1 : " + tempCtryOfLegalRes1.get(0).toString());
        hashMap.put("COUNTRY_OF_LEGAL_RESIDENCE_1", tempCtryOfLegalRes1);
      }
    } catch (Exception e) {
      hashMap.put("COUNTRY_OF_LEGAL_RESIDENCE_1", ctryOfLegalRes1);
      System.out.println("[IngeniumDispatcher] Error in getting ctryOfLegalRes1: " + exceptionStacktraceToString(e));
    }

    try {
      if (ctryOfLegalRes2 != null && ctryOfLegalRes2.size() > 0) {
        List tempCtryOfLegalRes2 = new ArrayList();
        String strCtryOfLegalRes2 = (String) ctryOfLegalRes2.get(0);
        tempCtryOfLegalRes2.add(IngeniumConstants.getStatusDesc("COUNTRY_".concat(strCtryOfLegalRes2)));
        System.out.println("COUNTRY_OF_LEGAL_RESIDENCE_2 : " + tempCtryOfLegalRes2.get(0).toString());
        hashMap.put("COUNTRY_OF_LEGAL_RESIDENCE_2", tempCtryOfLegalRes2);
      }
    } catch (Exception e) {
      hashMap.put("COUNTRY_OF_LEGAL_RESIDENCE_2", ctryOfLegalRes2);
      System.out.println("[IngeniumDispatcher] Error in getting ctryOfLegalRes2: " + e);
    }

    try {
      if (ctryOfLegalRes3 != null && ctryOfLegalRes3.size() > 0) {
        List tempCtryOfLegalRes3 = new ArrayList();
        String strCtryOfLegalRes3 = (String) ctryOfLegalRes3.get(0);
        tempCtryOfLegalRes3.add(IngeniumConstants.getStatusDesc("COUNTRY_".concat(strCtryOfLegalRes3)));
        System.out.println("COUNTRY_OF_LEGAL_RESIDENCE_3 : " + tempCtryOfLegalRes3.get(0).toString());
        hashMap.put("COUNTRY_OF_LEGAL_RESIDENCE_3", tempCtryOfLegalRes3);
      }
    } catch (Exception e) {
      hashMap.put("COUNTRY_OF_LEGAL_RESIDENCE_3", ctryOfLegalRes3);
      System.out.println("[IngeniumDispatcher] Error in getting ctryOfLegalRes3: " + exceptionStacktraceToString(e));
    }
    System.out.println("END: CONVERT CODES TO DESCRIPTION: COUNTRY_OF_LEGAL_RESIDENCE_1");

    //COUNTRY OF INCORPORATION
    //Change 1.4 (Cshells, Lacsz)
    //Change 1.5 (Cshells)
    List ctryOfInco = IngeniumConstants.getHashMapListValue("COUNTRY_OF_INCORPORATION", hashMap);
    try {
      if (ctryOfInco != null && ctryOfInco.size() > 0) {
        List tempCtryOfInco = new ArrayList();
        String strCtryOfInco = (String) ctryOfInco.get(0);
        tempCtryOfInco.add(IngeniumConstants.getStatusDesc("COUNTRY_".concat(strCtryOfInco)));
        System.out.println("COUNTRY_OF_INCORPORATION : " + tempCtryOfInco.get(0).toString());
        hashMap.put("COUNTRY_OF_INCORPORATION", tempCtryOfInco);
      }
    } catch (Exception e) {
      System.out.println("COUNTRY_OF_INCORPORATION has no value");
      hashMap.put("COUNTRY_OF_INCORPORATION", ctryOfInco);
      System.out.println("[IngeniumDispatcher] Error in getting ctryOfInco: " + exceptionStacktraceToString(e));
    }


    List entityUsTinList = IngeniumConstants.getHashMapListValue("FATCA_ENTITY_USTIN", hashMap);

    try {
      if (entityUsTinList != null && entityUsTinList.size() > 0) {
        List tempEntityUsTinList = new ArrayList();
        String entityUsTin = (String) entityUsTinList.get(0);
        tempEntityUsTinList.add(entityUsTin);
        hashMap.put("FATCA_ENTITY_USTIN", tempEntityUsTinList);
      }
    } catch (Exception e) {
      hashMap.put("FATCA_ENTITY_USTIN", entityUsTinList);
      System.out.println("[IngeniumDispatcher] Error in getting entityUsTinList: " + exceptionStacktraceToString(e));
    }


    List entityGiinList = IngeniumConstants.getHashMapListValue("FATCA_ENTITY_GIIN", hashMap);

    try {
      if (entityGiinList != null && entityGiinList.size() > 0) {
        List tempEntityGiinList = new ArrayList();
        String entityGiin = (String) entityGiinList.get(0);
        tempEntityGiinList.add(entityGiin);
        hashMap.put("FATCA_ENTITY_GIIN", tempEntityGiinList);
      }
    } catch (Exception e) {
      hashMap.put("FATCA_ENTITY_GIIN", entityGiinList);
      System.out.println("[IngeniumDispatcher] Error in getting entityGiinList: " + exceptionStacktraceToString(e));
    }

    List entityClassificationList = IngeniumConstants.getHashMapListValue("FATCA_ENTITY_CLASSIFICATION", hashMap);

    try {
      if (entityClassificationList != null && entityClassificationList.size() > 0) {
        List tempEntityClassificationList = new ArrayList();
        String entityClassification = (String) entityClassificationList.get(0);
        if (entityClassification.equals("01")) {
          entityClassification = "Active NFFE";
        } else if (entityClassification.equals("02")) {
          entityClassification = "Deemed-compliant FFI (\"DCFFI\")";
        } else if (entityClassification.equals("03")) {
          entityClassification = "Exempt Beneficial Owner";
        } else if (entityClassification.equals("04")) {
          entityClassification = "Non-participating FFI (\"NPFFI\")";
        } else if (entityClassification.equals("05")) {
          entityClassification = "Owner-documented FFI";
        } else if (entityClassification.equals("06")) {
          entityClassification = "Participating FFI (\"PFFI\")";
        } else if (entityClassification.equals("07")) {
          entityClassification = "Passive NFFE";
        } else if (entityClassification.equals("08")) {
          entityClassification = "Reporting IGA FFI";
        } else if (entityClassification.equals("09")) {
          entityClassification = "Restricted distributor";
        } else if (entityClassification.equals("10")) {
          entityClassification = "Territory financial institution";
        } else if (entityClassification.equals("11")) {
          entityClassification = "Specified U.S. Person";
        }
        tempEntityClassificationList.add(entityClassification);
        hashMap.put("FATCA_ENTITY_CLASSIFICATION", tempEntityClassificationList);
      }
    } catch (Exception e) {
      hashMap.put("FATCA_ENTITY_CLASSIFICATION", entityClassificationList);
      System.out.println("[IngeniumDispatcher] Error in getting entityClassificationList: " + exceptionStacktraceToString(e));
    }

    List entityStatusList = IngeniumConstants.getHashMapListValue("FATCA_ENTITY_STATUS", hashMap);

    try {
      if (entityStatusList != null && entityStatusList.size() > 0) {
        List tempEntityStatusList = new ArrayList();
        String entityStatus = (String) entityStatusList.get(0);
        if (entityStatus.equals("AA")) {
          entityStatus = "Nonparticipating FFI.";
        } else if (entityStatus.equals("AB")) {
          entityStatus = "Participating FFI.";
        } else if (entityStatus.equals("AC")) {
          entityStatus = "Reporting Model 1 FFI";
        } else if (entityStatus.equals("AD")) {
          entityStatus = "Reporting Model 2 FFI.";
        } else if (entityStatus.equals("AE")) {
          entityStatus = "Registered deemed-compliant FFI.";
        } else if (entityStatus.equals("AF")) {
          entityStatus = "Sponsored FFI that has not obtained a GIIN. Complete Part IV.";
        } else if (entityStatus.equals("AG")) {
          entityStatus = "Certified deemed-compliant nonregistering local bank. Complete Part VI.";
        } else if (entityStatus.equals("AH")) {
          entityStatus = "Certified deemed-compliant FFI with only low-value accounts.";
        } else if (entityStatus.equals("AI")) {
          entityStatus = "Certified deemed-compliant sponsored, closed held investment vehicle.";
        } else if (entityStatus.equals("AJ")) {
          entityStatus = "Certified deemed-compliant limited life debt investment entity.";
        } else if (entityStatus.equals("AK")) {
          entityStatus = "Certified deemed-compliant investment advisors and investment managers.";
        } else if (entityStatus.equals("AL")) {
          entityStatus = "Owner-documented FFI. Complete Part X.";
        } else if (entityStatus.equals("AM")) {
          entityStatus = "Restricted distributor. Complete Part XI.";
        } else if (entityStatus.equals("AN")) {
          entityStatus = "Nonreporting IGA FFI.";
        } else if (entityStatus.equals("AO")) {
          entityStatus = "Foreign government,govt of a US possession,or foreign central bank of issue.";
        } else if (entityStatus.equals("AP")) {
          entityStatus = "International organization. Complete Part XIV.";
        } else if (entityStatus.equals("AQ")) {
          entityStatus = "Exempt retirement plans. Complete Part XV.";
        } else if (entityStatus.equals("AR")) {
          entityStatus = "Entity wholly owned by exempt beneficial owners. Complete Part XVI.";
        } else if (entityStatus.equals("AS")) {
          entityStatus = "Territory financial institution. Complete Part XVII.";
        } else if (entityStatus.equals("AT")) {
          entityStatus = "Nonfinancial group entity. Complete Part XVIII.";
        } else if (entityStatus.equals("AU")) {
          entityStatus = "Excepted nonfinancial start-up company. Complete Part XIX.";
        } else if (entityStatus.equals("AV")) {
          entityStatus = "Excepted nonfinancial entity in liquidation or bankruptcy. Complete Part XX.";
        } else if (entityStatus.equals("AW")) {
          entityStatus = "501 (c) organization. Complete Part XXI.";
        } else if (entityStatus.equals("AX")) {
          entityStatus = "Nonprofit organization. Complete Part XXII.";
        } else if (entityStatus.equals("AY")) {
          entityStatus = "Publicly traded NFFE or NFFE affiliate of a publicly traded corporation.";
        } else if (entityStatus.equals("AZ")) {
          entityStatus = "Excepted territory NFFE. Complete Part XXIV.";
        } else if (entityStatus.equals("BA")) {
          entityStatus = "Active NFFE. Complete Part XXV.";
        } else if (entityStatus.equals("BB")) {
          entityStatus = "Passive NFFE. Complete Part XXVI.";
        } else if (entityStatus.equals("BC")) {
          entityStatus = "Excepted inter-affiliate FFI. Complete Part XXVII.";
        } else if (entityStatus.equals("BD")) {
          entityStatus = "Direct reporting NFFE.";
        } else if (entityStatus.equals("BE")) {
          entityStatus = "Sponsored direct reporting NFFE. Complete Part XXVIII.";
        }
        tempEntityStatusList.add(entityStatus);
        hashMap.put("FATCA_ENTITY_STATUS", tempEntityStatusList);
      }
    } catch (Exception e) {
      hashMap.put("FATCA_ENTITY_STATUS", entityStatusList);
      System.out.println("[IngeniumDispatcher] Error in getting entityStatusList: " + exceptionStacktraceToString(e));
    }

    List entityForeignTinList = IngeniumConstants.getHashMapListValue("FATCA_ENTITY_FOREIGN_TIN", hashMap);

    try {
      if (entityForeignTinList != null && entityForeignTinList.size() > 0) {
        List tempEntityForeignTinList = new ArrayList();
        String entityForeignTin = (String) entityForeignTinList.get(0);
        tempEntityForeignTinList.add(entityForeignTin);
        hashMap.put("FATCA_ENTITY_FOREIGN_TIN", tempEntityForeignTinList);
      }
    } catch (Exception e) {
      hashMap.put("FATCA_ENTITY_FOREIGN_TIN", entityForeignTinList);
      System.out.println("[IngeniumDispatcher] Error in getting entityForeignTinList: " + exceptionStacktraceToString(e));
    }

    List recalcitrantDateList = IngeniumConstants.getHashMapListValue("FATCA_RECALCITRANT_DATE", hashMap);
    try {
      if (recalcitrantDateList != null && recalcitrantDateList.size() > 0) {
        List tempRecalcitrantDateList = new ArrayList();
        String recalcitrantDate = (String) recalcitrantDateList.get(0);
        tempRecalcitrantDateList.add(recalcitrantDate);
        hashMap.put("FATCA_RECALCITRANT_DATE", tempRecalcitrantDateList);
      }
    } catch (Exception e) {
      hashMap.put("FATCA_RECALCITRANT_DATE", recalcitrantDateList);
      System.out.println("[IngeniumDispatcher] Error in getting recalcitrantDateList: " + exceptionStacktraceToString(e));
    }

    List recalcitrantStatusList = IngeniumConstants.getHashMapListValue("FATCA_RECALCITRANT_STATUS", hashMap);

    try {
      if (recalcitrantStatusList != null && recalcitrantStatusList.size() > 0) {
        List tempRecalcitrantStatusList = new ArrayList();
        String recalcitrantStatus = (String) recalcitrantStatusList.get(0);
        if ("1".equals(recalcitrantStatus)) {
          recalcitrantStatus = "Not Recalcitrant";
        } else if ("2".equals(recalcitrantStatus)) {
          recalcitrantStatus = "Recalcitrant";
        }
        tempRecalcitrantStatusList.add(recalcitrantStatus);
        hashMap.put("FATCA_RECALCITRANT_STATUS", tempRecalcitrantStatusList);
      }
    } catch (Exception e) {
      hashMap.put("FATCA_RECALCITRANT_STATUS", recalcitrantStatusList);
      System.out.println("[IngeniumDispatcher] Error in getting recalcitrantStatusList: " + exceptionStacktraceToString(e));
    }


    List recalcitrantReasonList = IngeniumConstants.getHashMapListValue("FATCA_RECALCITRANT_REASON", hashMap);

    try {
      if (recalcitrantReasonList != null && recalcitrantReasonList.size() > 0) {
        List tempRecalcitrantReasonList = new ArrayList();
        String recalcitrantReason = (String) recalcitrantReasonList.get(0);
        if (recalcitrantReason.equals("01")) {
          recalcitrantReason = "Incomplete W8F";
        } else if (recalcitrantReason.equals("02")) {
          recalcitrantReason = "Nonsubmission of rqt";
        } else if (recalcitrantReason.equals("03")) {
          recalcitrantReason = "Incomplete W8E";
        } else if (recalcitrantReason.equals("04")) {
          recalcitrantReason = "Incomplete W9F";
        } else if (recalcitrantReason.equals("05")) {
          recalcitrantReason = "Others";
        } else if (recalcitrantReason.equals("06")) {
          recalcitrantReason = "W8F Submitted";
        } else if (recalcitrantReason.equals("07")) {
          recalcitrantReason = "W8E Submitted";
        } else if (recalcitrantReason.equals("08")) {
          recalcitrantReason = "W9F Submitted";
        }
        tempRecalcitrantReasonList.add(recalcitrantReason);
        hashMap.put("FATCA_RECALCITRANT_REASON", tempRecalcitrantReasonList);
      }
    } catch (Exception e) {
      hashMap.put("FATCA_RECALCITRANT_REASON", recalcitrantReasonList);
      System.out.println("[IngeniumDispatcher] Error in getting recalcitrantReasonList: " + exceptionStacktraceToString(e));
    }

    List fatcaDateList = IngeniumConstants.getHashMapListValue("FATCA_DATE", hashMap);

    try {
      if (fatcaDateList != null && fatcaDateList.size() > 0) {
        List tempFatcaDateList = new ArrayList();
        String fatcaDate = (String) fatcaDateList.get(0);
        tempFatcaDateList.add(fatcaDate);
        hashMap.put("FATCA_DATE", tempFatcaDateList);
      }
    } catch (Exception e) {
      hashMap.put("FATCA_DATE", fatcaDateList);
      System.out.println("[IngeniumDispatcher] Error in getting fastcaDateList: " + exceptionStacktraceToString(e));
    }

    List recalcitrantUserIdList = IngeniumConstants.getHashMapListValue("FATCA_RECALCITRANT_USERID", hashMap);

    try {
      if (recalcitrantUserIdList != null && recalcitrantUserIdList.size() > 0) {
        List tempRecalcitrantList = new ArrayList();
        String recalcitrantUserId = (String) recalcitrantUserIdList.get(0);
        tempRecalcitrantList.add(recalcitrantUserId);
        hashMap.put("FATCA_RECALCITRANT_USERID", tempRecalcitrantList);
      }
    } catch (Exception e) {
      hashMap.put("FATCA_RECALCITRANT_USERID", recalcitrantUserIdList);
      System.out.println("[IngeniumDispatcher] Error in getting recalcitrantUserIdList: " + exceptionStacktraceToString(e));
    }

    List recalcitrantRemarksList = IngeniumConstants.getHashMapListValue("FATCA_RECALCITRANT_REMARKS", hashMap);

    try {
      if (recalcitrantRemarksList != null && recalcitrantRemarksList.size() > 0) {
        List tempRecalcitrantRemarksList = new ArrayList();
        String recalcitrantRemarks = (String) recalcitrantRemarksList.get(0);
        tempRecalcitrantRemarksList.add(recalcitrantRemarks);
        hashMap.put("FATCA_RECALCITRANT_REMARKS", tempRecalcitrantRemarksList);
      }
    } catch (Exception e) {
      hashMap.put("FATCA_RECALCITRANT_REMARKS", recalcitrantRemarksList);
      System.out.println("[IngeniumDispatcher] Error in getting recalcitrantRemarksList: " + exceptionStacktraceToString(e));
    }

    List redFlagDateList = IngeniumConstants.getHashMapListValue("FATCA_RED_FLAG_DATE", hashMap);

    try {
      if (redFlagDateList != null && redFlagDateList.size() > 0) {
        List tempRedFlagDateList = new ArrayList();
        String redFladDate = (String) redFlagDateList.get(0);
        tempRedFlagDateList.add(redFladDate);
        hashMap.put("FATCA_RED_FLAG_DATE", tempRedFlagDateList);
      }
    } catch (Exception e) {
      hashMap.put("FATCA_RED_FLAG_DATE", redFlagDateList);
      System.out.println("[IngeniumDispatcher] Error in getting redFlagDateList: " + exceptionStacktraceToString(e));
    }


    List redFlagUserIdList = IngeniumConstants.getHashMapListValue("FATCA_RED_FLAG_USERID", hashMap);

    try {
      if (redFlagUserIdList != null && redFlagUserIdList.size() > 0) {
        List tempRedFlagUserIdList = new ArrayList();
        String redFladUserId = (String) redFlagUserIdList.get(0);
        tempRedFlagUserIdList.add(redFladUserId);
        hashMap.put("FATCA_RED_FLAG_USERID", tempRedFlagUserIdList);
      }
    } catch (Exception e) {
      hashMap.put("FATCA_RED_FLAG_USERID", redFlagUserIdList);
      System.out.println("[IngeniumDispatcher] Error in getting redFlagUserIdList: " + exceptionStacktraceToString(e));
    }

    List redFlagIndicatorList = IngeniumConstants.getHashMapListValue("FATCA_RED_FLAG_INDICATOR", hashMap);

    try {
      if (redFlagIndicatorList != null && redFlagIndicatorList.size() > 0) {
        List tempRedFlagIndicatorList = new ArrayList();
        String redFladIndicator = (String) redFlagIndicatorList.get(0);
        tempRedFlagIndicatorList.add(redFladIndicator);
        hashMap.put("FATCA_RED_FLAG_INDICATOR", tempRedFlagIndicatorList);
      }
    } catch (Exception e) {
      hashMap.put("FATCA_RED_FLAG_INDICATOR", redFlagIndicatorList);
      System.out.println("[IngeniumDispatcher] Error in getting redFlagIndicatorList: " + exceptionStacktraceToString(e));
    }


    List redFlagReasonList = IngeniumConstants.getHashMapListValue("FATCA_RED_FLAG_REASON", hashMap);

    try {
      if (redFlagReasonList != null && redFlagReasonList.size() > 0) {
        List tempRedFlagReasonList = new ArrayList();
        String redFlagReason = (String) redFlagReasonList.get(0);
        if (redFlagReason.equals("01")) {
          redFlagReason = "Credit to US account ";
        } else if (redFlagReason.equals("03")) {
          redFlagReason = "Others";
        }
        tempRedFlagReasonList.add(redFlagReason);
        hashMap.put("FATCA_RED_FLAG_REASON", tempRedFlagReasonList);
      }
    } catch (Exception e) {
      hashMap.put("FATCA_RED_FLAG_REASON", redFlagReasonList);
      System.out.println("[IngeniumDispatcher] Error in getting redFlagReasonList: " + exceptionStacktraceToString(e));
    }


    List redFlagRemarksList = IngeniumConstants.getHashMapListValue("FATCA_RED_FLAG_REMARKS", hashMap);

    try {
      if (redFlagRemarksList != null && redFlagRemarksList.size() > 0) {
        List tempRedFlagRemarksList = new ArrayList();
        String redFladRemarks = (String) redFlagRemarksList.get(0);
        tempRedFlagRemarksList.add(redFladRemarks);
        hashMap.put("FATCA_RED_FLAG_REMARKS", tempRedFlagRemarksList);
      }
    } catch (Exception e) {
      hashMap.put("FATCA_RED_FLAG_REMARKS", redFlagRemarksList);
      System.out.println("[IngeniumDispatcher] Error in getting redFlagRemarksList: " + exceptionStacktraceToString(e));
    }


    return hashMap;
  }

  public HashMap getClientDetails2(String strClientId) {

    TheLogger.info("getClientDetails");
    HashMap hashMap = (HashMap) ingBusProcess.BF1220C_Append(Authenticate,
        strClientId);

    return hashMap;
  }

  public String getIngeniumHMListFirstObj(String key, HashMap map) {

    Object obj = null;
    String strVal = "-";

    try {
      List tempList = IngeniumConstants.getHashMapListValue(key, map);

      if (tempList != null && tempList.size() > 0) {
        obj = tempList.get(0);
      }

      if (obj != null) {
        strVal = (String) obj;
      }
    } catch (Exception e) {

    }

    return strVal;

  }

  /**
   * This function retrieves information about the Owner of given policy
   *
   * @param strPolicyId
   * @return
   */
  public HashMap getPolicyOwnerDetails(String strPolicyId) {
    HashMap hashMap;
    String strClientNo;
    hashMap = (HashMap) ingBusProcess.BF8000_Append(Authenticate,
        strPolicyId);
    ArrayList planId = null;
    ArrayList ServicingAgent = null;
    ArrayList Agent = null;
    ArrayList Status = null;
    ArrayList Count = null;
    ArrayList Currency = null;
    ArrayList AgentCode = null;
    ArrayList Branch = null;
    ArrayList InsType = null;
    ArrayList BranchCode = null;
    ArrayList AppSignDate = null;
    ArrayList SrvcAgent = null;

    ArrayList Address1 = null;
    ArrayList Address2 = null;
    ArrayList Address3 = null;
    ArrayList Address4 = null;
    ArrayList Address5 = null;

    try {
      if (hashMap.get("PLAN_ID") != null)
        planId = (ArrayList) hashMap.get("PLAN_ID");
      if (hashMap.get("SERVC_AGENT_CLIENT_CODE") != null)
        SrvcAgent = (ArrayList) hashMap.get("SERVC_AGENT_CLIENT_CODE");
      if (hashMap.get("SERVICING_AGENT_NAME") != null)
        ServicingAgent = (ArrayList) hashMap
            .get("SERVICING_AGENT_NAME");
      if (hashMap.get("AGENT_NAME") != null)
        Agent = (ArrayList) hashMap.get("AGENT_NAME");
      if (hashMap.get("POLICY_STATUS_DESC") != null)
        Status = (ArrayList) hashMap.get("POLICY_STATUS_DESC");
      if (hashMap.get("COVERAGE_COUNT") != null)
        Count = (ArrayList) hashMap.get("COVERAGE_COUNT");
      if (hashMap.get("CURRENCY_CODE") != null)
        Currency = (ArrayList) hashMap.get("CURRENCY_CODE");
      if (hashMap.get("AGENT_CODE") != null)
        AgentCode = (ArrayList) hashMap.get("AGENT_CODE");
      if (hashMap.get("BRANCH") != null)
        Branch = (ArrayList) hashMap.get("BRANCH");
      if (hashMap.get("INS_TYPE_CODE") != null)
        InsType = (ArrayList) hashMap.get("INS_TYPE_CODE");
      if (hashMap.get("BRANCH_CODE") != null)
        BranchCode = (ArrayList) hashMap.get("BRANCH_CODE");
      if (hashMap.get("APP_SIGN_DATE") != null)
        AppSignDate = (ArrayList) hashMap.get("APP_SIGN_DATE");

    } catch (Exception e) {
      e.printStackTrace();
    }

    try {
      ArrayList al_Client_no = (ArrayList) hashMap.get("CLIENT_NO");
      strClientNo = (String) al_Client_no.get(0);
      hashMap = (HashMap) ingBusProcess.BF1220C_Append(Authenticate,
          strClientNo);
      if (planId != null)
        hashMap.put("PLAN_ID", planId);
      if (ServicingAgent != null)
        hashMap.put("SERVICING_AGENT_NAME", ServicingAgent);
      if (Agent != null)
        hashMap.put("AGENT_NAME", Agent);
      if (Status != null)
        hashMap.put("POLICY_STATUS_DESC", Status);
      if (Count != null)
        hashMap.put("COVERAGE_COUNT", Count);
      if (Currency != null)
        hashMap.put("CURRENCY_CODE", Currency);
      if (AgentCode != null)
        hashMap.put("AGENT_CODE", AgentCode);
      if (Branch != null)
        hashMap.put("BRANCH", Branch);
      if (InsType != null)
        hashMap.put("INS_TYPE_CODE", InsType);
      if (BranchCode != null)
        hashMap.put("BRANCH_CODE", BranchCode);
      if (AppSignDate != null)
        hashMap.put("APP_SIGN_DATE", AppSignDate);
      if (SrvcAgent != null)
        hashMap.put("SERVC_AGENT_CLIENT_CODE", SrvcAgent);

      // Ingenium Mailing Address fix - Andre Ceasar Dacanay - start
      HashMap hs2 = (HashMap) ingBusProcess.InquiryBilling_Append(
          Authenticate, strPolicyId);

      if (hs2.get("ADDRESS_1") != null) {
        Address1 = (ArrayList) hs2.get("ADDRESS_1");
      }
      if (hs2.get("ADDRESS_2") != null) {
        Address2 = (ArrayList) hs2.get("ADDRESS_2");
      }
      if (hs2.get("ADDRESS_3") != null) {
        Address3 = (ArrayList) hs2.get("ADDRESS_3");
      }
      if (hs2.get("ADDRESS_4") != null) {
        Address4 = (ArrayList) hs2.get("ADDRESS_4");
      }
      if (hs2.get("ADDRESS_5") != null) {
        Address5 = (ArrayList) hs2.get("ADDRESS_5");
      }

      if (Address1 != null) {
        hashMap.put("ADDRESS_1", Address1);
      }
      if (Address2 != null) {
        hashMap.put("ADDRESS_2", Address2);
      }
      if (Address3 != null) {
        hashMap.put("ADDRESS_3", Address3);
      }
      if (Address4 != null) {
        hashMap.put("ADDRESS_4", Address4);
      }
      if (Address5 != null) {
        hashMap.put("ADDRESS_5", Address5);
      }

      printHashMap(hashMap);
    } catch (Exception e) {
      return null;
    }
    return hashMap;
  }

  /**
   * Policy Underwriting Inquiry
   *
   * @param strPolicyNo
   * @return
   */
  public HashMap getPolicyUnderwritingInquiry(String strPolicyNo) {
    TheLogger.info("getPolicyUnderwritingInquiry");
    HashMap hashMap = null;

    try {
      hashMap = (HashMap) ingBusProcess.getPolicyUnderwritingInquiry(
          Authenticate, strPolicyNo);
    } catch (Exception ex) {
      TheLogger.error(ex.getMessage());
    }
    return hashMap;
  }

  /**
   * Update policy in unerwriting
   *
   * @param strPolicyNo
   * @param treatyTypeCode
   * @return
   */
  public HashMap doPolicyUnderwritingUpdate(String strPolicyNo,
                                            String treatyTypeCode) {
    TheLogger.info("getPolicyUnderwritingInquiry");
    HashMap hashMap = null;

    try {
      hashMap = (HashMap) ingBusProcess.getPolicyUnderwritingUpdate(
          Authenticate, strPolicyNo, treatyTypeCode);
    } catch (Exception ex) {
      TheLogger.error(ex.getMessage());
    }
    return hashMap;
  }

  public HashMap doBF0592Select(String strPolicyNo, String clientId) {
    TheLogger.info("doBF0592Select");
    HashMap hashMap = null;

    try {
      hashMap = (HashMap) ingBusProcess.BF0592Select(Authenticate,
          strPolicyNo, clientId);
      // hashMap.put(key, value)
    } catch (Exception ex) {
      TheLogger.error(ex.getMessage());
    }
    return hashMap;
  }

  private void printHashMap(HashMap hashMap) {
    try {
      if (hashMap == null)
        return;
      Object strkeys[] = hashMap.keySet().toArray();
      for (int i = 0; i < strkeys.length; i++) {
        ArrayList al_result = (ArrayList) hashMap.get(strkeys[i]);
        System.out.println(strkeys[i] + ": ");
        for (int j = 0; j < al_result.size(); j++)
          System.out.println("\t" + al_result.get(j));
      }
    } catch (Exception ex) {
      TheLogger.error(ex.getMessage());
    }
  }

  /*
   * This method gets Service>Beneficiary List
   */
  public HashMap getServiceBeneficiaryList(String strPolicyNo) {
    TheLogger = new WMSLogger("IngeniumDispatcher");
    TheLogger.info("creating Ingenium client requirement.");
    HashMap hashmap = null;

    try {
      hashmap = (HashMap) ingBusProcess.ServiceBeneficiaryList_Append(
          Authenticate, strPolicyNo);
      List RELATION_TO_INSURED_BL = IngeniumConstants
          .getHashMapListValue("RELATION_TO_INSURED_BL", hashmap);
      List BENEFICIARY_INFORMATION_BENEFICIARY_DESIGNATION = IngeniumConstants
          .getHashMapListValue(
              "BENEFICIARY_INFORMATION_BENEFICIARY_DESIGNATION",
              hashmap);

      List beneficiaryList = new ArrayList();
      for (int i = 0; i < RELATION_TO_INSURED_BL.size(); i++) {
        beneficiaryList
            .add(IngeniumConstants
                .getStatusDesc(IngeniumConstants.RELATION_TO_INSURED_BL_
                    + RELATION_TO_INSURED_BL.get(i)
                    .toString()));
      }

      List beneficiaryInformationDesignation = new ArrayList();
      for (int i = 0; i < BENEFICIARY_INFORMATION_BENEFICIARY_DESIGNATION
          .size(); i++) {
        beneficiaryInformationDesignation
            .add(IngeniumConstants
                .getStatusDesc(IngeniumConstants.BENEFICIARY_DESIGNATION_
                    + BENEFICIARY_INFORMATION_BENEFICIARY_DESIGNATION
                    .get(i).toString()));
      }

      hashmap.put(IngeniumConstants.RELATION_TO_INSURED_BL,
          beneficiaryList);
      hashmap.put(
          IngeniumConstants.BENEFICIARY_INFORMATION_BENEFICIARY_DESIGNATION,
          beneficiaryInformationDesignation);

      // hashmap.put(key, value)
    } catch (Exception ex) {
      TheLogger.error(ex.getMessage());
    }
    return hashmap;
  }

  public HashMap getInquiryFundActivity(String strPolicyNo,
                                        String strEffDate, String strCoverageNo, String strSequenceNo) {
    TheLogger = new WMSLogger("IngeniumDispatcher");
    TheLogger.info("creating Ingenium client requirement.");
    HashMap hashmap = null;
    String effDate = convertToIngDateFormat(strEffDate);
    try {
      hashmap = (HashMap) ingBusProcess.InquiryFundActivity_Append(
          Authenticate, strPolicyNo, effDate, strCoverageNo,
          strSequenceNo);
    } catch (Exception ex) {
      TheLogger.error(ex.getMessage());
    }
    return hashmap;
  }

  public HashMap getPolicyInquiryAgent(String strPolicyNo, String strEffDate,
                                       String strCoverageNo) {
    TheLogger = new WMSLogger("IngeniumDispatcher");
    TheLogger.info("creating Ingenium client requirement.");
    HashMap hashmap = null;
    String effDate = convertToIngDateFormat(strEffDate);
    try {
      hashmap = (HashMap) ingBusProcess.PolicyInquiryAgent_Append(
          Authenticate, strPolicyNo, effDate, strCoverageNo);
    } catch (Exception ex) {
      TheLogger.error(ex.getMessage());
    }
    return hashmap;
  }

  public HashMap getPolicyInquirySummary(String strPolicyNo, String strEffDate) {
    TheLogger = new WMSLogger("IngeniumDispatcher");
    TheLogger.info("creating Ingenium client requirement.");
    HashMap hashmap = null;
    String effDate = convertToIngDateFormat(strEffDate);
    try {
      hashmap = (HashMap) ingBusProcess.PolicyInquirySummary_Append(
          Authenticate, strPolicyNo, effDate);
    } catch (Exception ex) {
      TheLogger.error(ex.getMessage());
    }
    return hashmap;
  }

  public HashMap getInquiryCoverageDetails(String strPolicyNo,
                                           String strCoverageNo) {
    TheLogger = new WMSLogger("IngeniumDispatcher");
    TheLogger.info("creating Ingenium client requirement.");
    HashMap hashmap = null;
    try {
      hashmap = (HashMap) ingBusProcess.InquiryCoverageDetails_Append(
          Authenticate, strPolicyNo, strCoverageNo);
    } catch (Exception ex) {
      TheLogger.error(ex.getMessage());
    }
    return hashmap;
  }

  public HashMap getBillingMethodandModeInquiry(String strPolicyNo,
                                                String strEffDate) {
    TheLogger = new WMSLogger("IngeniumDispatcher");
    TheLogger.info("creating Ingenium client requirement.");
    HashMap hashmap = null;
    String effDate = convertToIngDateFormat(strEffDate);
    try {
      hashmap = (HashMap) ingBusProcess
          .BillingMethodandModeInquiry_Append(Authenticate,
              strPolicyNo, effDate);
    } catch (Exception ex) {
      TheLogger.error(ex.getMessage());
    }
    return hashmap;
  }

  public HashMap getCoverageRiskInquiry(String strPolicyNo,
                                        String strEffDate, String strCoverageNo) {
    TheLogger = new WMSLogger("IngeniumDispatcher");
    TheLogger.info("creating Ingenium client requirement.");
    HashMap hashmap = null;
    String effDate = convertToIngDateFormat(strEffDate);
    try {
      hashmap = (HashMap) ingBusProcess.CoverageRiskInquiry_Append(
          Authenticate, strPolicyNo, effDate, strCoverageNo);
    } catch (Exception ex) {
      TheLogger.error(ex.getMessage());
    }
    return hashmap;
  }

  public HashMap getAccountingSummaryHistory(String strPolicyNo,
                                             String strProcessDate, String strSystemDate, String strTime,
                                             String strRecordType) {
    TheLogger = new WMSLogger("IngeniumDispatcher");
    TheLogger.info("creating Ingenium client requirement.");
    HashMap hashmap = null;
    String procDate = convertToIngDateFormat(strProcessDate);
    String sysDate = convertToIngDateFormat(strSystemDate);
    int[] time = convertToIngTimeFormat(strTime);
    try {
      hashmap = (HashMap) ingBusProcess.AccountingSummaryHistory_Append(
          Authenticate, strPolicyNo, procDate, sysDate, time,
          strRecordType);
    } catch (Exception ex) {
      TheLogger.error(ex.getMessage());
    }
    return hashmap;
  }

  public HashMap getLoanDetailList(String strPolicyNo, String strLoanType,
                                   String strEffDate, String strSequenceNo) {
    TheLogger = new WMSLogger("IngeniumDispatcher");
    TheLogger.info("creating Ingenium client requirement.");
    HashMap hashmap = null;
    String effDate = convertToIngDateFormat(strEffDate);
    try {
      hashmap = (HashMap) ingBusProcess.LoanDetailList_Append(
          Authenticate, strPolicyNo, strLoanType, effDate,
          strSequenceNo);
    } catch (Exception ex) {
      TheLogger.error(ex.getMessage());
    }
    return hashmap;
  }

  public HashMap getChangeHistoryList(String strPolicyNo,
                                      String strCoverageNo, String strEffDate, String strSequenceNo,
                                      String strCurrentStatus) {
    TheLogger = new WMSLogger("IngeniumDispatcher");
    TheLogger.info("creating Ingenium client requirement.");
    HashMap hashmap = null;
    String effDate = convertToIngDateFormat(strEffDate);
    try {
      hashmap = (HashMap) ingBusProcess.ChangeHistoryList_Append(
          Authenticate, strPolicyNo, strCoverageNo, effDate,
          strSequenceNo, strCurrentStatus);
    } catch (Exception ex) {
      TheLogger.error(ex.getMessage());
    }
    return hashmap;
  }

  public HashMap getClientAddressList(String strClientId,
                                      String strAddressEffDate, String strAddressType) {
    TheLogger = new WMSLogger("IngeniumDispatcher");
    TheLogger.info("creating Ingenium client requirement.");
    HashMap hashmap = null;
    String addressEffDate = convertToIngDateFormat(strAddressEffDate);
    try {
      hashmap = (HashMap) ingBusProcess.ClientAddressList_Append(
          Authenticate, strClientId, addressEffDate, strAddressType);
    } catch (Exception ex) {
      TheLogger.error(ex.getMessage());
    }
    return hashmap;
  }

  public HashMap getAgentInquiry(String strAgentId) {
    TheLogger = new WMSLogger("IngeniumDispatcher");
    TheLogger.info("creating Ingenium client requirement.");
    HashMap hashmap = null;
    try {
      hashmap = (HashMap) ingBusProcess.AgentInquiry_Append(Authenticate,
          strAgentId);
    } catch (Exception ex) {
      TheLogger.error(ex.getMessage());
    }
    return hashmap;
  }

  public HashMap getFutureDatedPolicyActivityList(String strPolicyNo,
                                                  String strActivityEffDate, String strActivityStatus) {
    TheLogger = new WMSLogger("IngeniumDispatcher");
    TheLogger.info("creating Ingenium client requirement.");
    HashMap hashmap = null;
    String activityEffDate = convertToIngDateFormat(strActivityEffDate);
    try {
      hashmap = (HashMap) ingBusProcess
          .FutureDatedPolicyActivityList_Append(Authenticate,
              strPolicyNo, activityEffDate, strActivityStatus);
    } catch (Exception ex) {
      TheLogger.error(ex.getMessage());
    }
    return hashmap;
  }

  public HashMap getGeneralAccountingEntries(String strPolicyNo,
                                             String strAcctDate, String strAcctEntryDesc) {
    TheLogger = new WMSLogger("IngeniumDispatcher");
    TheLogger.info("creating Ingenium client requirement.");
    HashMap hashmap = null;
    String acctDate = convertToIngDateFormat(strAcctDate);
    try {
      hashmap = (HashMap) ingBusProcess.GeneralAccountingEntries_Append(
          Authenticate, strPolicyNo, acctDate, strAcctEntryDesc);
    } catch (Exception ex) {
      TheLogger.error(ex.getMessage());
    }
    return hashmap;
  }

  public HashMap getClientDataSheet(String policyNumber) {
    System.out.println("getClientDataSheet start");
    TheLogger.info("getClientDataSheet");
    HashMap hashMap = null;

    try {
      hashMap = (HashMap) ingBusProcess.getClientDataSheet(Authenticate,
          policyNumber);
      // printHashMap(hashMap);
    } catch (Exception ex) {
      TheLogger.error(ex.getMessage());
    }
    System.out.println("getClientDataSheet end");
    return hashMap;
  }

  public String getCDSInquiry(String policyNumber) {
    System.out.println("getCDSInquiry start");
    TheLogger.info("getCDSInquiry start");
    String response = "";

    try {
      response = ingBusProcess.getCDSInquiry(Authenticate, policyNumber);
      // printHashMap(hashMap);
    } catch (Exception ex) {
      TheLogger.error(ex.getMessage());
    }
    System.out.println("getCDSInquiry end");
    return response;
  }

  public String getCDSRPCListInquiry(String policyNumber,
                                     String lastRelPolicyNumber, String lastRelPolicyNumberCovNum) {
    System.out.println("getCDSRPCListInquiry start");
    TheLogger.info("getCDSRPCListInquiry start");
    String response = "";

    try {
      response = ingBusProcess.getCDSRPCListInquiry(Authenticate,
          policyNumber, lastRelPolicyNumber,
          lastRelPolicyNumberCovNum);
      // printHashMap(hashMap);
    } catch (Exception ex) {
      TheLogger.error(ex.getMessage());
    }
    System.out.println("getCDSRPCListInquiry end");
    return response;
  }

  public HashMap updateProtectedPolicy(String policyNo, String reinsInd,
                                       String reinsCompany, String treatyType, String reinsFaceAmount,
                                       String tdbFaceAmount, String adbFaceAmount,
                                       String sundyPaymentAmount) {
    System.out.println("updateProtectedPolicy start");
    TheLogger.info("updateProtectedPolicy start");
    HashMap hashMap = null;

    try {
      hashMap = (HashMap) ingBusProcess.updateProtectedPolicy(
          Authenticate, policyNo, reinsInd, reinsCompany, treatyType,
          reinsFaceAmount, tdbFaceAmount, adbFaceAmount,
          sundyPaymentAmount);
      // printHashMap(hashMap);
    } catch (Exception ex) {
      TheLogger.error(ex.getMessage());
    }
    System.out.println("updateProtectedPolicy end");
    return hashMap;
  }

  public HashMap getPlanInquiryDetails(String planCode) {
    HashMap hm = (HashMap) ingBusProcess.getPlanInquiryDetails(
        Authenticate, planCode);

    if (hm == null)
      System.out.println("Error getPlanInquiryDetails...");

    return hm;
  }

  /*
   * GFraga- 10/14/2014 for FATCA phase 2
   */
  public HashMap getAddressList(String strClientId) {

    TheLogger.info("getAddressList");
    HashMap hashMap = (HashMap) ingBusProcess.getAddressList_request(
        Authenticate, strClientId);
    /*
     * zlacsamana 10/29/2014
     */

    //COUNTRY
    List listCtry = IngeniumConstants.getHashMapListValue("FATCA_COUNTRY", hashMap);
    if (listCtry != null) {
      List ctry = new ArrayList();
      String strCtry = listCtry.get(0).toString();
      ctry.add(IngeniumConstants.getStatusDesc("COUNTRY_".concat(strCtry)));
      System.out.println("FATCA_COUNTRY: " + ctry.get(0).toString());
      hashMap.put("FATCA_COUNTRY", ctry);

    }

    //PROVINCE/STATE

    List listProvState = IngeniumConstants.getHashMapListValue("PROVINCE_CODE", hashMap);
    if (listProvState != null) {
      List tempProvState = new ArrayList();
      String strProvState = (String) listProvState.get(0);
      tempProvState.add(IngeniumConstants.getStatusDesc("PROVINCE_STATE_".concat(strProvState)));
      System.out.println("PROVINCE_CODE: " + tempProvState.get(0).toString());
      hashMap.put("PROVINCE_CODE", tempProvState);
    }

    //ADDRESS STATUS

    List listAddressStat = IngeniumConstants.getHashMapListValue("ADDRESS_STATUS", hashMap);
    if (listAddressStat != null) {
      List tempAddressStat = new ArrayList();
      String strAddressStat = (String) listAddressStat.iterator().next();
      tempAddressStat.add(IngeniumConstants.getStatusDesc("ADDRESS_STATUS_".concat(strAddressStat)));
      System.out.println("ADDRESS_STATUS: " + tempAddressStat.get(0).toString());
      hashMap.put("ADDRESS_STATUS", tempAddressStat);

    }


    List listAddressType = IngeniumConstants.getHashMapListValue("CLIENT_ADDRESS_TYPE", hashMap);
    if (listAddressType != null) {
      List tempAddressType = new ArrayList();
      String strAddressType = (String) listAddressType.get(0);
      tempAddressType.add(IngeniumConstants.getStatusDesc("ADDRESS_TYPE_".concat(strAddressType)));
      System.out.println("ADDRESS_TYPE: " + tempAddressType.get(0).toString());
      hashMap.put("CLIENT_ADDRESS_TYPE", tempAddressType);
    }


    return hashMap;
  }

  public HashMap getClientTelephoneList(String strClientId) {

    TheLogger.info("getClientTelephoneList");
    HashMap hashMap = (HashMap) ingBusProcess
        .getClientTelephoneList_request(Authenticate, strClientId);

    List listContactType = IngeniumConstants.getHashMapListValue("CONTACT_TYPE", hashMap);
    if (listContactType != null) {
      List tempContactType = new ArrayList();

      for (int i = 0; i < listContactType.size(); i++) {
        String strContactType = listContactType.get(i).toString();
        System.out.println(i + " = " + listContactType.size());
        System.out.println(i + " = " + IngeniumConstants.getStatusDesc("CONTACT_TYPE_".concat(strContactType)));
        if (IngeniumConstants.getStatusDesc("CONTACT_TYPE_".concat(strContactType)) != null || IngeniumConstants.getStatusDesc("CONTACT_TYPE_".concat(strContactType)) != "") {
          tempContactType.add(IngeniumConstants.getStatusDesc("CONTACT_TYPE_".concat(strContactType)));
        }
      }
      hashMap.put("CONTACT_TYPE", tempContactType);
    }
    return hashMap;


  }


  public HashMap getClientIDFormsList(String strClientId) {

    TheLogger.info("getClientIDFormsList");
    HashMap hashMap = (HashMap) ingBusProcess.getClientIDFormsList_request(
        Authenticate, strClientId);

    List listIdPresented = IngeniumConstants.getHashMapListValue("ID_PRESENTED_OR_IRS_FORM", hashMap);
    if (listIdPresented != null) {
      List tempIdPresented = new ArrayList();

      for (int i = 0; i < listIdPresented.size(); i++) {
        String strIdPresented = listIdPresented.get(i).toString();
        tempIdPresented.add(strIdPresented);
      }

      hashMap.put("ID_PRESENTED_OR_IRS_FORM", tempIdPresented);
    }

    List listFatcaSigningDate = IngeniumConstants.getHashMapListValue("FATCA_SIGNING_DATE", hashMap);

    if (listFatcaSigningDate != null) {
      List tempFatcaSigningDateList = new ArrayList();

      for (int i = 0; i < listFatcaSigningDate.size(); i++) {
        String strFatcaSigningDate = listFatcaSigningDate.get(i).toString();
        tempFatcaSigningDateList.add(strFatcaSigningDate);
      }

      hashMap.put("FATCA_SIGNING_DATE", tempFatcaSigningDateList);
    }

    List listFatcaSubmissionDate = IngeniumConstants.getHashMapListValue("FATCA_SUBMISSION_DATE", hashMap);

    if (listFatcaSubmissionDate != null) {
      List tempFatcaSubmissionDateList = new ArrayList();

      for (int i = 0; i < listFatcaSubmissionDate.size(); i++) {
        String strFatcaSubmissionDate = listFatcaSubmissionDate.get(i).toString();
        tempFatcaSubmissionDateList.add(strFatcaSubmissionDate);
      }

      hashMap.put("FATCA_SUBMISSION_DATE", tempFatcaSubmissionDateList);
    }

    List listFatcaFormStatus = IngeniumConstants.getHashMapListValue("FATCA_FORM_STATUS", hashMap);

    if (listFatcaFormStatus != null) {
      List tempFatcaFormStatus = new ArrayList();

      for (int i = 0; i < listFatcaFormStatus.size(); i++) {
        String strFatcaFormStatus = listFatcaFormStatus.get(i).toString();
        if (strFatcaFormStatus.equals("01")) {
          strFatcaFormStatus = "Complete";
        } else if (strFatcaFormStatus.equals("00")) {
          strFatcaFormStatus = "InComplete";
        }

        tempFatcaFormStatus.add(strFatcaFormStatus);
      }

      hashMap.put("FATCA_FORM_STATUS", tempFatcaFormStatus);
    }

    List listIdReferenceNo = IngeniumConstants.getHashMapListValue("REFERENCE_NO", hashMap);

    if (listIdReferenceNo != null) {
      List tempIdReferenceNo = new ArrayList();

      for (int i = 0; i < listIdReferenceNo.size(); i++) {
        String strIdReferenceNo = listIdReferenceNo.get(i).toString();
        tempIdReferenceNo.add(strIdReferenceNo);
      }

      hashMap.put("REFERENCE_NO", tempIdReferenceNo);
    }

    List listIdExpiryDate = IngeniumConstants.getHashMapListValue("EXPIRY_DATE", hashMap);

    if (listIdExpiryDate != null) {
      List tempIdExpiryDate = new ArrayList();

      for (int i = 0; i < listIdExpiryDate.size(); i++) {
        String strIdExpiryDate = listIdExpiryDate.get(i).toString();
        tempIdExpiryDate.add(strIdExpiryDate);
      }

      hashMap.put("EXPIRY_DATE", tempIdExpiryDate);
    }

    List listIdValidityDate = IngeniumConstants.getHashMapListValue("VALIDITY_DATE", hashMap);

    if (listIdValidityDate != null) {
      List tempIdValidityDate = new ArrayList();

      for (int i = 0; i < listIdValidityDate.size(); i++) {
        String strIdValidityDate = listIdValidityDate.get(i).toString();
        tempIdValidityDate.add(strIdValidityDate);
      }

      hashMap.put("VALIDITY_DATE", tempIdValidityDate);
    }


    return hashMap;
  }

  public HashMap getEntitySubstantialUSOwnerList(String strClientId) {

    TheLogger.info("getClientIDFormsList");
    HashMap hashMap = (HashMap) ingBusProcess.getEntitySubstantialUSOwnerList_request(
        Authenticate, strClientId);

    List listSequenceNo = IngeniumConstants.getHashMapListValue("FATCA_ENTITY_SUBTANTIAL_US_OWNER_SEQUENCE_NO", hashMap);
    if (listSequenceNo != null) {
      List tempSequenceNo = new ArrayList();

      for (int i = 0; i < listSequenceNo.size(); i++) {
        String strSequenceNo = listSequenceNo.get(i).toString();
        tempSequenceNo.add(strSequenceNo);
      }

      hashMap.put("FATCA_ENTITY_SUBTANTIAL_US_OWNER_SEQUENCE_NO", tempSequenceNo);
    }

    List listName = IngeniumConstants.getHashMapListValue("FATCA_ENTITY_SUBTANTIAL_US_OWNER_NAME", hashMap);
    if (listName != null) {
      List tempName = new ArrayList();

      for (int i = 0; i < listName.size(); i++) {
        String strName = listName.get(i).toString();
        tempName.add(strName);
      }

      hashMap.put("FATCA_ENTITY_SUBTANTIAL_US_OWNER_NAME", tempName);
    }


    List listAddress = IngeniumConstants.getHashMapListValue("FATCA_ENTITY_SUBTANTIAL_US_OWNER_ADDRESS", hashMap);
    if (listAddress != null) {
      List tempAddress = new ArrayList();

      for (int i = 0; i < listAddress.size(); i++) {
        String strAddress = listAddress.get(i).toString();
        tempAddress.add(strAddress);
      }

      hashMap.put("FATCA_ENTITY_SUBTANTIAL_US_OWNER_ADDRESS", tempAddress);
    }

    List listCountry = IngeniumConstants.getHashMapListValue("FATCA_ENTITY_SUBTANTIAL_US_OWNER_COUNTRY", hashMap);
    if (listCountry != null) {
      List tempCountry = new ArrayList();

      for (int i = 0; i < listCountry.size(); i++) {
        String strCountry = listCountry.get(i).toString();
        tempCountry.add(IngeniumConstants.getStatusDesc("COUNTRY_".concat(strCountry)));
      }

      hashMap.put("FATCA_ENTITY_SUBTANTIAL_US_OWNER_COUNTRY", tempCountry);
    }

    List listTin = IngeniumConstants.getHashMapListValue("FATCA_ENTITY_SUBTANTIAL_US_OWNER_TIN", hashMap);
    if (listTin != null) {
      List tempTin = new ArrayList();

      for (int i = 0; i < listTin.size(); i++) {
        String strTin = listTin.get(i).toString();
        tempTin.add(strTin);
      }

      hashMap.put("FATCA_ENTITY_SUBTANTIAL_US_OWNER_TIN", tempTin);
    }


    return hashMap;
  }


  private String exceptionStacktraceToString(Exception e) {
    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    PrintStream ps = new PrintStream(baos);
    e.printStackTrace(ps);
    ps.close();
    return baos.toString();
  }
}