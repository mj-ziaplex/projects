package ph.com.sunlife.ingenium.domain;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

@XStreamAlias("MirDvTrstAddrCdG")
public class MirDvTrstAddrCdG {

    @XStreamImplicit(itemFieldName = "MirDvTrstAddrCdT")
    private List<String> mirDvTrstAddrCdT;

    public List<String> getMirDvTrstAddrCdT() {
        return mirDvTrstAddrCdT;
    }

    public void setMirDvTrstAddrCdT(List<String> mirDvTrstAddrCdT) {
        this.mirDvTrstAddrCdT = mirDvTrstAddrCdT;
    }
}
