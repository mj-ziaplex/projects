package ph.com.sunlife.wms.ingenium.configuration;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;

@Configuration
@ComponentScan
@Lazy
public class RequestCredentialConfiguration {


}

