package ph.com.sunlife.wms.ingenium.configuration;

class IngeniumPropertiesConfigurationKeys {

    public static final String SERVER_HOST = "ServerHost";
    public static final String AUTHENTICATION_USERNAME = "AuthenticationUsername";
    public static final String AUTHENTICATION_PASSWORD_SALT = "AuthenticationPasswordSalt";
    public static final String AUTHENTICATION_PASSWORD_PREFIX = "AuthenticationPasswordPrefix";
    public static final String AUTHENTICATION_PASSWORD_OFFSET = "AuthenticationPasswordOffset";
    public static final String DATABASE_SERVER_HOST = "DatabaseServerHost";
    public static final String DATABASE_LINKED_SERVER = "DatabaseLinkedServer";
    public static final String DATABASE_NAME = "DatabaseName";
    public static final String DATABASE_PORT = "DatabasePort";
    public static final String DATABASE_USERNAME = "DatabaseUsername";
    public static final String DATABASE_PASSWORD = "DatabasePassword";
    public static final String DATE_FORMAT = "DateFormat";
    public static final String TIME_FORMAT = "TimeFormat";
    public static final String ENABLE_REQUEST_RESPONSE_LOGGING = "EnableRequestResponseLogging";
    public static final String REQUEST_RESPONSE_LOG_LOCATION = "RequestResponseLogLocation";
    public static final String APPLICATION_LOG_LOCATION = "ApplicationLogLocation";
    public static final String DEFAULT_TXREFGUID = "DefaultTransactionReferenceGUID";
    public static final String DEFAULT_EFFECTIVITY_DATE = "DefaultEffectivityDate";

    private static IngeniumPropertiesConfigurationKeys instance = new IngeniumPropertiesConfigurationKeys();
    public static IngeniumPropertiesConfigurationKeys getInstance() { return instance; }
    private IngeniumPropertiesConfigurationKeys() { }
}
