package ph.com.sunlife.zold.ws.ingenium.soap;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class TXLifeServiceClient {

  public static void main(String[] args) {
    try {

      TXLifeService TXLifeService = new TXLifeServiceImpl();
      TXLifeServicePort myPort = TXLifeService.getTXLifeService();

      StringBuffer xmlData = new StringBuffer();

      String inputFileName = null;

      if (args.length == 1) {
        inputFileName = args[0];
      }

      if (inputFileName == null) {
        System.out.println("Please specify input file name!");
        System.exit(0);
      }

      try {
        // get new file reader

        BufferedReader reader = new BufferedReader(new FileReader(inputFileName));

        String line = reader.readLine();
        while (line != null) {
          xmlData.append(line.trim());
          line = reader.readLine();
        }
      } catch (IOException ioe) {
        System.out.println("Error occured while reading the XML file: " + inputFileName + "!");
        System.exit(1);
      }

      System.out.println("XML String (" + inputFileName + "):" + xmlData.toString());

      String inputDoc = xmlData.toString();

      String request = inputDoc;

      String response = myPort.callTXLife(request);

      System.out.println(response);
    } catch (Exception ex) {
      ex.printStackTrace();
    }
  }
}