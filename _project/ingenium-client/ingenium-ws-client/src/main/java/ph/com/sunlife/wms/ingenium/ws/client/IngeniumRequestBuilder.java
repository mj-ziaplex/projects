package ph.com.sunlife.wms.ingenium.ws.client;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.CompactWriter;
import org.apache.commons.lang3.StringUtils;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ph.com.sunlife.ingenium.ws.domain.*;
import ph.com.sunlife.ingenium.ws.domain.converter.TransactionTypeAttribute;
import ph.com.sunlife.wms.ingenium.configuration.IngeniumClientConfigurator;

import java.io.*;
import java.text.DateFormat;
import java.util.Date;

public abstract class IngeniumRequestBuilder<RequestBuilder extends IngeniumRequestBuilder> {

    protected String transactionReferenceGUID;
    protected Date executionTimestamp;
    private ApplicationContext context;
    private Request request;

    public IngeniumRequestBuilder() {
        executionTimestamp = new Date();

        context =
                new AnnotationConfigApplicationContext(IngeniumClientConfigurator.class);

        Credentials credentials = new BasicCredentials();
        Password password = new PlainPassword();
        password.setCryptographicAlgorithm("NONE");
        password.setPasswordText((String) context.getBean("password"));
        credentials.setUsername((String) context.getBean("username"));
        credentials.setPassword(password);

        request = new Request();
        request.setCredentials(credentials);
    }

    public String getTransactionReferenceGUID() {
        return transactionReferenceGUID;
    }

    public RequestBuilder setTransactionReferenceGUID(final String transactionReferenceGUID) {
        if (transactionReferenceGUID == null ||
                transactionReferenceGUID.length() <= 0) {
            this.transactionReferenceGUID = (String) context.getBean("defaulTransactionReferenceGUID");
        }
        this.transactionReferenceGUID = transactionReferenceGUID;
        return (RequestBuilder) this;
    }

    public String create() {
        request.setTransaction(getTransaction());

        try {
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            Writer writer = new OutputStreamWriter(outputStream, "UTF-8");
            writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\" ?>");
            writer.write(StringUtils.LF);

            XStream xstream = new XStream();
            xstream.allowTypesByWildcard(new String[] {
                    "ph.com.sunlife.ingenium.domain.**",
                    "ph.com.sunlife.ingenium.ws.domain.**"
            });
            xstream.autodetectAnnotations(true);
            xstream.registerConverter(new TransactionTypeAttribute());
            xstream.marshal(request, new CompactWriter(writer));
//            xstream.toXML(request, writer);

            return outputStream.toString("UTF-8");
        } catch (UnsupportedEncodingException uee) { // ignore
        } catch (IOException ioe) { // ignore
        }

        return "";
    }

    protected DateFormat getDateFormatter() {
        return (DateFormat) context.getBean("dateFormatter");
    }

    protected DateFormat getTimeFormatter() {
        return (DateFormat) context.getBean("timeFormatter");
    }

    protected abstract Transaction getTransaction();
}
