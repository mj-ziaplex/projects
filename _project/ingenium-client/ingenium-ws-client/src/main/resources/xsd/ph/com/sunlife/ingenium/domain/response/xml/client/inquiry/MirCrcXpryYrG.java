//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2020.03.09 at 01:32:19 AM AWST 
//


package ph.com.sunlife.ingenium.domain.response.xml.client.inquiry;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://ACORD.org/Standards/Life/2}MirCrcXpryYrT" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "mirCrcXpryYrT"
})
@XmlRootElement(name = "MirCrcXpryYrG")
public class MirCrcXpryYrG {

    @XmlElement(name = "MirCrcXpryYrT", type = Byte.class)
    protected List<Byte> mirCrcXpryYrT;

    /**
     * Gets the value of the mirCrcXpryYrT property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the mirCrcXpryYrT property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMirCrcXpryYrT().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Byte }
     * 
     * 
     */
    public List<Byte> getMirCrcXpryYrT() {
        if (mirCrcXpryYrT == null) {
            mirCrcXpryYrT = new ArrayList<Byte>();
        }
        return this.mirCrcXpryYrT;
    }

}
