package ph.com.sunlife.ingenium;

import org.junit.Test;
import ph.com.sunlife.wms.ingenium.domain.ConsolidatedInformation;
import ph.com.sunlife.wms.ingenium.handler.PolicyInquiryResponseHandler;
import ph.com.sunlife.wms.ingenium.service.PolicyService;
import ph.com.sunlife.wms.ingenium.service.PolicyServiceImpl;
import ph.com.sunlife.wms.ingenium.ws.client.PolicyInquiryRequestBuilder;
import ph.com.sunlife.wms.ingenium.ws.client.RequirementsCreateRequestBuilder;

import java.util.List;

public class WebServiceClientTest {

  @Test
  public void testStructedNoteStatus() throws Exception {
    PolicyService service = new PolicyServiceImpl();
    byte status = service.getStructedNoteStatus("0880059770" );

    System.out.println("STATUS: " + status);
  }

  @Test
  public void testPolicyAssignees() throws Exception {
    PolicyService service = new PolicyServiceImpl();
    List<String> policyAssignees = service.getPolicyAssignees("0880059770");

    if (policyAssignees != null && !policyAssignees.isEmpty()) {
      System.out.println(policyAssignees.get(0));
    } else {
      System.out.println("policy assignees is empty");
    }
  }

  @Test
  public void testPolicyClientDetails() throws Exception {
    PolicyService service = new PolicyServiceImpl();
    ConsolidatedInformation consoInfo = service.getPolicyClientDetails("0880059770");

    System.out.println(consoInfo.getAgentCode());
  }

  @Test
  public void testPlanID() throws Exception {
    PolicyService service = new PolicyServiceImpl();
    System.out.println(service.getPlanId("0880059770"));
  }

  @Test
  public void test() throws Exception {
//    PolicyInquiryRequestBuilder builder = new PolicyInquiryRequestBuilder("0880059770");
//    IngeniumWebServiceClient client = new IngeniumWebServiceClient();
//    PolicyInquiryResponseHandler handler = new PolicyInquiryResponseHandler();
//
//    String request;
//    String response;
//
//    request = builder.create();
//    System.out.println(request);
//    response = client.getResponse(builder.create());
//    handler.setResponseString(response);
//
//    System.out.println(handler.getPolicyContractType());


//    RequirementsCreateRequestBuilder builder = new RequirementsCreateRequestBuilder("3080008662");
//    IngeniumWebServiceClient client = new IngeniumWebServiceClient();
//
//    String request;
//    String response;
//
//    request = builder.create();
//    System.out.println(request);
//    response = client.getResponse(builder.create());
//
//    System.out.println(response);

    System.out.println("05".equals("0523456789"));
  }
}

