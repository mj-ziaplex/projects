package ph.com.sunlife.ingenium;

import org.junit.Before;
import org.junit.Test;
import ph.com.sunlife.wms.ingenium.ws.client.*;

import java.io.IOException;

public class RequestTests {
  @Before
  public void setUp() {

  }

  @Test
  public void testClientInquiryRequest() {
    System.out.println(new ClientInquiryRequestBuilder("3080008662").create());
  }

  @Test
  public void testPolicyInquiryRequest() {
    System.out.println(new PolicyInquiryRequestBuilder("0880059770").create());
  }

  @Test
  public void testListRequirementRequest() {
    System.out.println(new ListRequirementsRequestBuilder("0880059770").create());
  }

  @Test
  public void testInquiryConsolidatedInformationRequest() {
    System.out.println(new InquiryConsolidatedInformationRequestBuilder("0880059770")
            .setEffectivityDate("2019-01-15")
            .create());
  }

  @Test
  public void testRequirementCreateRequest() {
    System.out.println(new RequirementsCreateRequestBuilder("3080008662")
            .setRequirementId("2019-01-15")
            .setRequirementStatusCode("dsaf")
            .create());
  }

  @Test
  public void testPlanInquiryRequest() {
    System.out.println(new PlanInquiryRequestBuilder("3080008662").create());
  }
}
