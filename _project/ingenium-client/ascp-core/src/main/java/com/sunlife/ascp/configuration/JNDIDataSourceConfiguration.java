/*
 * Copyright (C) 2019 Sun Life Financial. All Rights Reserved.
 * 
 * This software is confidential and proprietary information of Sun Life Financial
 * ("Confidential Information"). You shall not disclose such Confidential Information
 * and shall use it only in accordance with the terms and conditions you entered
 * with Sun Life Financial.
 * 
 * 
 * Written by Marlon Janssen Arao <marlon.arao@sunlife.com>, 2019 December 04
 */

package com.sunlife.ascp.configuration;

import javax.naming.NamingException;
import javax.sql.DataSource;

/**
 *
 * @author Marlon Janssen Arao <mj.arao@techie.com>
 */
public class JNDIDataSourceConfiguration extends JNDIConfiguration {

  public JNDIDataSourceConfiguration() throws NamingException {
    super();
  }

  public JNDIDataSourceConfiguration(final String name) throws NamingException {
    super(name);
  }

  @Override
  public DataSource get() {
    return (DataSource) this.resource;
  }
}
