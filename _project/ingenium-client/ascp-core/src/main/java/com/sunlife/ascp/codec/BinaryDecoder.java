package com.sunlife.ascp.codec;

public interface BinaryDecoder extends Decoder<byte[]> {

  @Override
  byte[] decode(byte[] source);
}
