package com.sunlife.ascp.cryptography;

public class CryptographicHashEngine {

  public CryptographicHash create(final CryptoHashAlgorithm algorithmName) {
    if (algorithmName == null) {
      throw new IllegalArgumentException("The cryptographic hash algorithm must not be null;");
    }
    CryptographicHash hashFunction;
    switch (algorithmName) {
      case MD5:
        hashFunction = new MD5Hash();
        break;
      default:
        hashFunction = null;
    }
    return hashFunction;
  }

  public MD5Hash createMD5() {
    return new MD5Hash();
  }
}
