/*
 * Copyright 2010 Marlon Janssen Arao
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * 
 * 
 * Written by Marlon Janssen Arao <mj.arao@techie.com>, 2010 November 22
 */
package com.sunlife.ascp.connectivity.database;

/**
 *
 * @author Marlon Janssen Arao <mj.arao@techie.com>
 */
public class JdbcDriver {

    public static final String DB2_EXPRESS_C = "com.ibm.db2.jcc.DB2Driver";
    public static final String DERBY = "org.apache.derby.jdbc.EmbeddedDriver";
    public static final String H2 = "org.h2.Driver";
    public static final String HSQLDB = "org.hsqldb.jdbc.JDBCDriver";
    public static final String INFORMIX = "com.informix.jdbc.IfxDriver";
    public static final String MARIADB = "org.mariadb.jdbc.Driver";
    public static final String MICROSOFT_SQL_SERVER = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
    public static final String MYSQL = "com.mysql.jdbc.Driver";
    public static final String ORACLE = "oracle.jdbc.OracleDriver";
    public static final String POSTGRESQL = "org.postgresql.Driver";
    public static final String SAP_HANA = "com.sap.db.jdbc.Driver";



    private static JdbcDriver instance = new JdbcDriver();
    private JdbcDriver() { }
    public static JdbcDriver getInstance() { return instance; }
 }
