package com.sunlife.ascp.exception;

public class MissingConfigurationException extends Exception {

  private static final long serialVersionUID = 1852467398764321301L;
  private static final String DEFAULT_MESSAGE =
      "No Properties, YAML, nor JNDI configuration is found " +
      "in the application server, default directory, or classpath.";

  public MissingConfigurationException() {
    super(DEFAULT_MESSAGE);
  }

  public MissingConfigurationException(String message) {
    super(message);
  }

  public MissingConfigurationException(String message, Throwable cause) {
    super(message, cause);
  }

  public MissingConfigurationException(Throwable cause) {
    super(cause);
  }
}
