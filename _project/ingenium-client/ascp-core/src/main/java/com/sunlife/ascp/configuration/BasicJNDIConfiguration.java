package com.sunlife.ascp.configuration;

import javax.naming.NamingException;

public class BasicJNDIConfiguration extends JNDIConfiguration {

  public BasicJNDIConfiguration() throws NamingException {
    super();
  }

  public BasicJNDIConfiguration(final String name) throws NamingException {
    super(name);
  }

  @Override
  public <T> T get() {
    return null;
  }
}
