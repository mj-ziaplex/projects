/*
 * Copyright 2010 Marlon Janssen Arao
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * 
 * 
 * Written by Marlon Janssen Arao <mj.arao@techie.com>, 2010 November 22
 */
package com.sunlife.ascp.connectivity.database;

/**
 *
 * @author Marlon Janssen Arao <mj.arao@techie.com>
 */
public final class DatabaseConnectorBuilder {

    private String host;
    private int port;
    private String username;
    private String password;
    public String schema;



    public DatabaseConnectorBuilder() {
        host = "localhost";
    }



    public DatabaseConnectorBuilder connectTo(final String host) {
        this.host = host;
        return this;
    }

    public DatabaseConnectorBuilder atPort(final int port) {
        this.port = port;
        return this;
    }

    public DatabaseConnectorBuilder loginAs(final String user) {
        this.username = user;
        return this;
    }

    public DatabaseConnectorBuilder verifyWith(final String password) {
        this.password = password;
        return this;
    }

    public DatabaseConnectorBuilder makeUseOf(final String schema) {
        this.schema = schema;
        return this;
    }
    
    public DatabaseConnector constructMsSql() {
        return construct(new MsSqlServerConnector());
    }
    
    private DatabaseConnector construct(DatabaseConnector connector) {
        DatabaseConnector kuhnektr = connector;
        kuhnektr.setHost(host);
        kuhnektr.setPort(port);
        kuhnektr.setSchema(schema);
        kuhnektr.setUsername(username);
        kuhnektr.setPassword(password);
        return kuhnektr;
    }
}
