/*
 * Copyright (C) 2019 Sun Life Financial. All Rights Reserved.
 * 
 * This software is confidential and proprietary information of Sun Life Financial
 * ("Confidential Information"). You shall not disclose such Confidential Information
 * and shall use it only in accordance with the terms and conditions you entered
 * with Sun Life Financial.
 * 
 * 
 * Written by Marlon Janssen Arao <marlon.arao@sunlife.com>, 2019 November 20
 */

package com.sunlife.ascp.connectivity.database;

/**
 *
 * @author Marlon Janssen Arao <mj.arao@techie.com>
 */
public class JdbcUrl {

    public static final String forDb2ExpressC(final String host, final String schema) {
        return "jdbc:db2://" + host + "/" + schema;
    }
    
    public static final String forDb2ExpressC(final String host, final int port, final String schema) {
        return "jdbc:db2://" + host + ":" + port + "/" + schema;
    }
    
    public static final String forDerby(final String path, final String schema) {
        return "jdbc:derby:" + path + ";databaseName=" + schema + ";create=true";
    }
    
    public static final String forH2(final String schema) {
        return "jdbc:h2:mem:" + schema;
    }
    
    public static final String forHSqldb(final String schema) {
        return "jdbc:hsqldb:mem:" + schema;
    }
    
    public static final String forInformix(final String host, final int port, final String schema, final String serverName) {
        return "jdbc:informix-sqli://" + host + ":" + port + "/" + schema + ":INFORMIXSERVER=" + serverName;
    }
    
    public static final String forMariaDb(final String host, final String schema) {
        return "jdbc:mariadb://" + host + "/" + schema;
    }
    
    public static final String forMariaDb(final String host, final int port, final String schema) {
        return "jdbc:mariadb://" + host + ":" + port + "/" + schema;
    }
    
    public static final String forMicrosoftSqlServer(final String host, final String instance, final String schema) {
        return "jdbc:sqlserver://" + host + ";instance=" + instance + ";databaseName=" + schema;
    }
    
    public static final String forMySql(final String host, final String schema) {
        return "jdbc:mysql://" + host + "/" + schema;
    }
    
    public static final String forMySql(final String host, final int port, final String schema) {
        return "jdbc:mysql://" + host + ":" + port + "/" + schema;
    }
    
    public static final String forOracle(final String host, final int port, final String schema) {
        return "jdbc:oracle:thin:@" + host + ":" + port + ":" + schema;
    }
    
    public static final String forPostgreSql(final String host, final String schema) {
        return "jdbc:postgresql://" + host + "/" + schema;
    }
    
    public static final String forPostgreSql(final String host, final int port, final String schema) {
        return "jdbc:postgresql://" + host + ":" + port + "/" + schema;
    }
    
    public static final String forSapHana(final String host, final String schema) {
        return "jdbc:sap://" + host + "/" + schema;
    }



    private static JdbcUrl instance = new JdbcUrl();
    private JdbcUrl() { }
    public static JdbcUrl getInstance() { return instance; }
 }
