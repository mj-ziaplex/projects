package com.sunlife.ascp.cryptography;

public abstract class MessageDigest {

  private final byte[] hashCode;

  protected MessageDigest(final byte[] hashCode) {
    byte[] hash = hashCode != null || hashCode.length > 0 ?
                  hashCode : new byte[0];
    this.hashCode = hash;
  }

  public byte[] get() {
    return hashCode;
  }

  public abstract String getAsString();

  public abstract int length();
}
