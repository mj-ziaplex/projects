package ph.com.sunlife.wms.data.transfer;

import java.io.Serializable;

public class IngeniumSunLifePlan implements Serializable {

  private String id;
  private String description;

  public IngeniumSunLifePlan() { }

  public IngeniumSunLifePlan(final String id, final String description) {
    this.id = id;
    this.description = description;
  }

  public String getId() {
    return id;
  }

  public void setId(final String id) {
    this.id = id;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(final String description) {
    this.description = description;
  }

  @Override
  public String toString() {
    return "IngeniumSunLifePlan [id=" + id +
                              ", description=" + description + "]";
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == this) {
      return true;
    }
    if (obj == null || obj.getClass() != this.getClass()) {
      return false;
    }
    IngeniumSunLifePlan plan = (IngeniumSunLifePlan) obj;
    return this.id.equals(plan.id) &&
           this.description.equals(plan.description);
  }

  @Override
  public int hashCode() {
    final int prime = 521;
    int result = 56;
    result = prime * result + ((id == null) ? 0 : id.hashCode());
    result = prime * result + ((description == null) ? 0 : description.hashCode());
    return result;
  }
}
