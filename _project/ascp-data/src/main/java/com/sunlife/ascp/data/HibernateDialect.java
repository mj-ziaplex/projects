package com.sunlife.ascp.data;

public class HibernateDialect {

  public static final String DB2_EXPRESS_C = "org.hibernate.dialect.DB2Dialect";
  public static final String DERBY
      = "org.hibernate.dialect.DerbyTenSevenDialect";
  public static final String H2 = "org.hibernate.dialect.H2Dialect";
  public static final String HSQLDB = "org.hibernate.dialect.HSQLDialect";
  public static final String INFORMIX = "org.hibernate.dialect.InformixDialect";
  public static final String MARIADB = "org.hibernate.dialect.MariaDB53Dialect";
  public static final String MYSQL = "org.hibernate.dialect.MySQL8Dialect";
  public static final String ORACLE = "org.hibernate.dialect.Oracle12cDialect";
  public static final String POSTGRESQL
      = "org.hibernate.dialect.PostgreSQL95Dialect";
  public static final String SAP_HANA
      = "org.hibernate.dialect.HANAColumnStoreDialect";
  public static final String SQL_SERVER
      = "org.hibernate.dialect.SQLServer2012Dialect";



  private static HibernateDialect instance = new HibernateDialect();
  public static HibernateDialect getInstance() { return instance; }
  private HibernateDialect() { }
}
