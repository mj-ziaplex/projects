/*
 * Copyright 2010-2020 Marlon Janssen Arao
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.sunlife.ascp.connectivity;

import com.sunlife.ascp.data.RelationalDatabase;

public class DatabaseConnectorFactory {

  public static DatabaseConnector getConnector(final RelationalDatabase rdbms) {
    if (rdbms != null) {
      switch (rdbms) {
        case MICROSOFT_SQL_SERVER:
        default:
          return new MSSQLServerConnector();
      }
    }
    return null;
  }


  private static DatabaseConnectorFactory instance =
      new DatabaseConnectorFactory();
  public static DatabaseConnectorFactory getInstance() { return instance; }
  private DatabaseConnectorFactory() { }
}
