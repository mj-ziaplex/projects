/*
 * Copyright 2010-2020 Marlon Janssen Arao
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.sunlife.ascp.data.access;

import java.sql.SQLException;
import java.util.List;

public interface DataAccessObject<T> {

  T get(long id) throws SQLException;

  T get(String id) throws SQLException;

  List<T> getAll() throws SQLException;

  void save(T t) throws SQLException;

  void update(T t, String[] params) throws SQLException;

  void delete(T t) throws SQLException;

  List<T> find(long id) throws SQLException;

  List<T> find(String id) throws SQLException;
}
