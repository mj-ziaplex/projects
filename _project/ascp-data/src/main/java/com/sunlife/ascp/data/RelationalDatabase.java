/*
 * Copyright 2010-2020 Marlon Janssen Arao
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.sunlife.ascp.data;

public enum RelationalDatabase {

  APACHE_DERBY("Derby"),
  H2("H2"),
  HSQLDB("HSQLDB"),
  IBM_DB2_EXPRESS_C("DB2 Express-C"),
  IBM_INFORMIX("Informix"),
  MARIADB("MariaDB"),
  MICROSOFT_SQL_SERVER("SQL Server"),
  MYSQL("MySQL"),
  ORACLE_DATABASE("Oracle"),
  POSTGRESQL("PostgreSQL"),
  SAP_HANA("SAP HANA");


  public static RelationalDatabase fromString(String name) {
    for (RelationalDatabase rdbms : RelationalDatabase.values()) {
      if (rdbms.name.equalsIgnoreCase(name)) {
        return rdbms;
      }
    }
    return null;
  }


  private String name;
  RelationalDatabase(String name) { this.name = name; }
  public String getName() { return this.name; }
}
