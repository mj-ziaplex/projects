/*
 * Copyright 2010-2020 Marlon Janssen Arao
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.sunlife.ascp.connectivity;

import com.sunlife.ascp.data.DataSourceClassName;
import com.sunlife.ascp.data.JDBCDriver;

import java.util.ArrayList;
import java.util.List;

public final class MSSQLServerConnector extends DatabaseConnector {

  public MSSQLServerConnector() {
    setPort(getDefaultPort());
  }

  public MSSQLServerConnector(final String server,
                              final String databaseName) {
    this();
    setHost(server);
    setDatabaseName(databaseName);
  }

  public MSSQLServerConnector(final String server,
                              final String databaseName,
                              final String username,
                              final String password) {
    this();
    setHost(server);
    setDatabaseName(databaseName);
    setUser(username);
    setPassword(password);
  }

  public MSSQLServerConnector(final String server,
                              final int port,
                              final String databaseName,
                              final String username,
                              final String password) {
    setHost(server);
    setPort(port);
    setDatabaseName(databaseName);
    setUser(username);
    setPassword(password);
  }

  @Override
  public int getDefaultPort() {
    return 1433;
  }

  @Override
  public String getDefaultUser() {
    return "sa";
  }

  @Override
  public String getDefaultDatabase() {
    return "Pubs";
  }

  @Override
  public String getDriver() {
    return JDBCDriver.SQL_SERVER;
  }

  @Override
  public String getUrl() {
    return String.format("jdbc:sqlserver://%s:%d;DatabaseName=%s",
                         getHost(), getPort(), getDatabaseName());
  }

  @Override
  public String getDataSourceName() {
    return DataSourceClassName.MICROSOFT_SQL_SERVER;
  }

  @Override
  public String getServerName() {
    return null;
  }

  public List<String> getSampleDatabases() {
    List<String> sampleDatabases = new ArrayList<String>();
    sampleDatabases.add("Northwind");
    sampleDatabases.add("Pubs");
    return sampleDatabases;
  }

  public String getServer() {
    return getHost();
  }

  public void setServer(String server) {
    setHost(server);
  }

  public String getDatabaseName() {
    return getSchema();
  }

  public void setDatabaseName(String databaseName) {
    setSchema(databaseName);
  }

}
