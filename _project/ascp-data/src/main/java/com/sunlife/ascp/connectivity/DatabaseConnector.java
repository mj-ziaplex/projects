/*
 * Copyright 2010-2020 Marlon Janssen Arao
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.sunlife.ascp.connectivity;

public abstract class DatabaseConnector<T extends DatabaseConnector<T>>
                                          implements Connector<T> {

  private String host;
  private int port;
  private String schema;
  private String database;
  private String user;
  private String password;


  @Override
  public final T setHost(final String host) {
    this.host = host;
    return (T) this;
  }

  @Override
  public final String getHost() {
    return host;
  }

  public final String getSchema() {
    return schema;
  }

  public final T setSchema(final String schema) {
    this.schema = schema;
    return (T) this;
  }

  public String getDatabase() {
    return database;
  }

  public T setDatabase(final String database) {
    this.database = database;
    return (T) this;
  }

  @Override
  public final T setPort(final int port) {
    this.port = port;
    return (T) this;
  }

  @Override
  public final int getPort() {
    return port;
  }

  @Override
  public final T setUser(final String user) {
    this.user = user;
    return (T) this;
  }

  @Override
  public final String getUser() {
    return user;
  }

  @Override
  public final T setPassword(final String password) {
    this.password = password;
    return (T) this;
  }

  @Override
  public final String getPassword() {
    return password;
  }


  @Override
  public final String getDefaultHost() {
    return "localhost";
  }


  @Override
  public abstract int getDefaultPort();

  @Override
  public abstract String getDefaultUser();

  public abstract String getDefaultDatabase();


  public abstract String getDriver();
  public abstract String getUrl();
  public abstract String getDataSourceName();
  public abstract String getServerName();
}
