/*
 * Copyright 2010-2020 Marlon Janssen Arao
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.sunlife.ascp.data;

import com.sunlife.ascp.connectivity.DatabaseConnector;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

import javax.sql.DataSource;

public final class DatabaseConnectorDataSource extends DataSourceRadix {

  public DatabaseConnectorDataSource(final DatabaseConnector connector) {
    super(create(connector));
  }

  private static DataSource create(final DatabaseConnector connector) {
    HikariConfig config = new HikariConfig();
    config.setDriverClassName(connector.getDriver());
    config.setJdbcUrl(connector.getUrl());
    config.setUsername(connector.getUser());
    config.setPassword(connector.getPassword());
    config.addDataSourceProperty(CACHE_PREPARED_STATEMENTS_CONFIG,
        CACHE_PREPARED_STATEMENTS_DEFAULT);
    config.addDataSourceProperty(PREPARED_STATEMENTS_CACHE_SIZE_CONFIG,
        PREPARED_STATEMENTS_CACHE_SIZE_DEFAULT);
    config.addDataSourceProperty(PREPARED_STATEMENTS_CACHE_SQL_LIMIT_CONFIG,
        PREPARED_STATEMENTS_CACHE_SQL_LIMIT_DEFAULT);
    config.setAutoCommit(true);
    return new HikariDataSource(config);
  }

  private static final String CACHE_PREPARED_STATEMENTS_CONFIG
      = "cachePrepStmts";
  private static final String PREPARED_STATEMENTS_CACHE_SIZE_CONFIG
      = "prepStmtCacheSize";
  private static final String PREPARED_STATEMENTS_CACHE_SQL_LIMIT_CONFIG
      = "prepStmtCacheSqlLimit";

  private static final String CACHE_PREPARED_STATEMENTS_DEFAULT = "true";
  private static final String PREPARED_STATEMENTS_CACHE_SIZE_DEFAULT = "250";
  private static final String PREPARED_STATEMENTS_CACHE_SQL_LIMIT_DEFAULT
      = "2048";
}
