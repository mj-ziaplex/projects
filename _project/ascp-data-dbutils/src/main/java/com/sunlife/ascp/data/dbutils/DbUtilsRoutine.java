/*
 * Copyright 2010-2020 Marlon Janssen Arao
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.sunlife.ascp.data.dbutils;

import com.sunlife.ascp.connectivity.DatabaseConnector;
import com.sunlife.ascp.data.DataRountine;
import org.apache.commons.dbutils.QueryRunner;

import java.sql.Connection;
import java.sql.SQLException;

public class DbUtilsRoutine extends DataRountine {

  private QueryRunner runner;

  public DbUtilsRoutine(Connection connection) {
    super(connection);
    runner = new QueryRunner();
  }

  public DbUtilsRoutine(DatabaseConnector connector) throws SQLException {
    super(connector);
    runner = new QueryRunner();
  }

  public int execute(String storedProcedure, Object... params)
      throws SQLException {
    return runner.execute(CONNECTION,
                          String.format(STORED_PROCEDURE_SQL_CALL,
                                        storedProcedure),
                          params);
  }

  private static final String STORED_PROCEDURE_SQL_CALL = "{call %s}";
}
