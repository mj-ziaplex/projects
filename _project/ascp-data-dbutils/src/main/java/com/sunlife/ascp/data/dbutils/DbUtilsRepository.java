/*
 * Copyright 2010-2020 Marlon Janssen Arao
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.sunlife.ascp.data.dbutils;

import com.sunlife.ascp.connectivity.DatabaseConnector;
import com.sunlife.ascp.data.DataRepository;
import com.sunlife.ascp.data.DatabaseConnectorDataSource;
import com.sunlife.ascp.data.PropertiesDataSource;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;

import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.Properties;

public abstract class DbUtilsRepository extends DataRepository {

  private QueryRunner runner;

  public DbUtilsRepository(DataSource dataSource) {
    super(dataSource);
    runner = new QueryRunner(DATA_SOURCE);
  }

  public DbUtilsRepository(Properties properties) {
    this(new PropertiesDataSource(properties).getDataSource());
  }

  public DbUtilsRepository(DatabaseConnector connector) {
    this(new DatabaseConnectorDataSource(connector).getDataSource());
  }

  protected final int insert(String sql,
                             Object... parameters) throws SQLException {
    int result = 0;
    result = runner.execute(sql, parameters);
    return result;
  }

  protected final <T> T query(String query,
                              ResultSetHandler<T> handler) throws SQLException {
    return query(query, handler, null);
  }

  protected final <T> T query(String query,
                              ResultSetHandler<T> handler,
                              Object... parameters) throws SQLException {
    T result = null;
    result = runner.query(query, handler, parameters);
    return result;
  }

  protected final int update(String sql,
                             Object... parameters) throws SQLException {
    int result = 0;
    result = runner.update(sql, parameters);
    return result;
  }
}
