/*
 * Copyright 2010-2020 Marlon Janssen Arao
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sunlife.ascp.mail;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.validator.routines.EmailValidator;
import org.joda.time.DateTime;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static javax.mail.Message.RecipientType.*;

public abstract class Email<T extends Email<T>> {

  private String sender;
  private List<String> recipients;
  private List<String> carbonCopies;
  private List<String> blindCarbonCopies;
  private String characterSet = "UTF-8";
  private String subject;
  private String message;
  private List<File> attachments;
  private DateTime dateSent;


  public final T from(final String sender) throws MessagingException {
    return setSender(sender);
  }

  public final T to(final String... recipients) throws MessagingException {
    return setRecipients(recipients);
  }

  public final T to(final List<String> recipients) throws MessagingException {
    return setRecipients(recipients);
  }

  public final T cc(final String... recipients) throws MessagingException {
    return setRecipients(recipients);
  }

  public final T cc(final List<String> recipients) throws MessagingException {
    return setRecipients(recipients);
  }

  public final T bcc(final String... recipients) throws MessagingException {
    return setRecipients(recipients);
  }

  public final T bcc(final List<String> recipients) throws MessagingException {
    return setRecipients(recipients);
  }


  public final T setSender(final String sender) throws MessagingException {
    if (StringUtils.isNotBlank(sender) &&
        EmailValidator.getInstance().isValid(sender)) {
      this.sender = sender;
    }
    return (T) this;
  }

  public final String getSender() throws MessagingException {
    return sender;
  }

  public final T setRecipients(final String... recipients)
      throws MessagingException {
    if (ArrayUtils.isNotEmpty(recipients)) {
      for (String recipient : recipients) {
        if (EmailValidator.getInstance().isValid(recipient)) {
          if (this.recipients == null) {
            this.recipients = new ArrayList<String>(recipients.length);
          }
          this.recipients.add(recipient);
        }
      }
    }
    return (T) this;
  }

  public final T setRecipients(final List<String> recipients)
      throws MessagingException {
    if (CollectionUtils.isNotEmpty(recipients)) {
      for (String recipient : recipients) {
        if (EmailValidator.getInstance().isValid(recipient)) {
          if (this.recipients == null) {
            this.recipients = new ArrayList<String>(recipients.size());
          }
          this.recipients.add(recipient);
        }
      }
    }
    return (T) this;
  }

  public final List<String> getRecipients() throws MessagingException {
    return recipients;
  }

  public final T setCarbonCopies(final String... recipients)
      throws MessagingException {
    if (ArrayUtils.isNotEmpty(recipients)) {
      for (String recipient : recipients) {
        if (EmailValidator.getInstance().isValid(recipient)) {
          if (this.carbonCopies == null) {
            this.carbonCopies = new ArrayList<String>(recipients.length);
          }
          this.carbonCopies.add(recipient);
        }
      }
    }
    return (T) this;
  }

  public final T setCarbonCopies(final List<String> recipients)
      throws MessagingException {
    if (CollectionUtils.isNotEmpty(recipients)) {
      for (String recipient : recipients) {
        if (EmailValidator.getInstance().isValid(recipient)) {
          if (this.carbonCopies == null) {
            this.carbonCopies = new ArrayList<String>(recipients.size());
          }
          this.carbonCopies.add(recipient);
        }
      }
    }
    return (T) this;
  }

  public final List<String> getCarbonCopies() throws MessagingException {
    return carbonCopies;
  }

  public final T setBlindCarbonCopies(final String... recipients)
      throws MessagingException {
    if (ArrayUtils.isNotEmpty(recipients)) {
      for (String recipient : recipients) {
        if (EmailValidator.getInstance().isValid(recipient)) {
          if (this.blindCarbonCopies == null) {
            this.blindCarbonCopies = new ArrayList<String>(recipients.length);
          }
          this.blindCarbonCopies.add(recipient);
        }
      }
    }
    return (T) this;
  }

  public final T setBlindCarbonCopies(final List<String> recipients)
      throws MessagingException {
    if (CollectionUtils.isNotEmpty(recipients)) {
      for (String recipient : recipients) {
        if (EmailValidator.getInstance().isValid(recipient)) {
          if (this.blindCarbonCopies == null) {
            this.blindCarbonCopies = new ArrayList<String>(recipients.size());
          }
          this.blindCarbonCopies.add(recipient);
        }
      }
    }
    return (T) this;
  }

  public final List<String> getBlindCarbonCopies() throws MessagingException {
    return blindCarbonCopies;
  }

  public final T setSubject(final String subject) throws MessagingException {
    if (StringUtils.isNotBlank(subject)) {
      this.subject = subject;
    }
    return (T) this;
  }

  public final String getSubject() throws MessagingException {
    return subject;
  }

  public final T addAttachment(final File file) {
    if (file != null &&
        (file.isFile() && file.canRead())) {
      if (attachments == null) {
        attachments = new ArrayList<File>();
      }
      attachments.add(file);
    }
    return (T) this;
  }

  public final T addAttachments(final File... files) throws MessagingException {
    if (ArrayUtils.isNotEmpty(files)) {
      for (File file : files) {
        addAttachment(file);
      }
    }
    return (T) this;
  }

  public final T addAttachments(final List<File> files)
      throws MessagingException {
    if (CollectionUtils.isNotEmpty(files)) {
      for (File file : files) {
        addAttachment(file);
      }
    }
    return (T) this;
  }

  public final boolean hasAttachments() {
    return CollectionUtils.isNotEmpty(attachments);
  }

  public final String getCharacterSet() {
    return characterSet;
  }

  public final T setCharacterSet(final String characterSet) {
    if (StringUtils.isNotBlank(characterSet)) {
      this.characterSet =  characterSet;
    }
    return (T) this;
  }

  public final String getMessage() {
    return message;
  }

  public final T setMessage(String message) {
    this.message = message != null ? message : "";
    return (T) this;
  }

  public DateTime getDateSent() {
    return dateSent;
  }

  public void setDateSent(DateTime dateSent) {
    this.dateSent = dateSent;
  }


  protected final void setBasicHeader(MimeMessage message)
      throws MessagingException {
    message.setFrom(new InternetAddress(sender));
    setRecipientsByType(message, recipients, TO);
    setRecipientsByType(message, carbonCopies, CC);
    setRecipientsByType(message, blindCarbonCopies, BCC);
    message.setSubject(subject, characterSet);
  }

  protected final void processAttachments(Multipart multipart)
      throws MessagingException {
    if (CollectionUtils.isNotEmpty(attachments)) {
      for (File file : attachments) {
        MimeBodyPart attachment = new MimeBodyPart();
        attachment.setDataHandler(new DataHandler(new FileDataSource(file)));
        attachment.setFileName(file.getName());
        multipart.addBodyPart(attachment);
      }
    }
  }


  private final void setRecipientsByType(final MimeMessage message,
                                         final List<String> recipients,
                                         final Message.RecipientType type)
      throws MessagingException {
    if (CollectionUtils.isNotEmpty(recipients)) {
      Address[] emails = new Address[recipients.size()];
      int index = 0;
      for(String recipient : recipients)
        if (EmailValidator.getInstance().isValid(recipient))
          emails[index++] = new InternetAddress(recipient);
      message.setRecipients(type, emails);
    }
  }


  public abstract MimeMessage getMimeMessage(Session session)
      throws MessagingException;
}
