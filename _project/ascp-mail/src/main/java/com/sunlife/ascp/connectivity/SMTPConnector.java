/*
 * Copyright 2010-2020 Marlon Janssen Arao
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sunlife.ascp.connectivity;

import com.sunlife.ascp.mail.MailAuthentication;

public class SMTPConnector extends EmailConnector {

  private int timeout = 6000;
  private boolean enableSSL = false;
  private String sslTrust = "*";


  public SMTPConnector(final String hostname,
                       final int port) {
    super(hostname, port);
  }

  public SMTPConnector(final String hostname,
                       final MailAuthentication type) {
    super(hostname, getDefaultPort(type));
    setAuthenticationType(type);
    sslTrust = hostname;
  }

  @Override
  public int getDefaultPort() {
    return getDefaultPort(getAuthenticationType());
  }

  @Override
  public String getDefaultUser() {
    return "";
  }

  public int getTimeout() {
    return timeout;
  }

  public SMTPConnector setTimeout(int milliseconds) {
    if (milliseconds >= 0) {
      timeout = milliseconds;
    }
    return this;
  }

  public boolean isSSLEnabled() {
    return enableSSL;
  }

  public SMTPConnector enableSSL(boolean enable) {
    enableSSL = enable;
    return this;
  }


  protected static final int getDefaultPort(MailAuthentication type) {
    switch(type) {
      case SSL:
        return 465;
      case STARTTLS_STLS:
        return 587;
      default:
        return 25;
    }
  }
}
