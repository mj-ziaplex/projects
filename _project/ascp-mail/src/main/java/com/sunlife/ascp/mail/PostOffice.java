/*
 * Copyright 2010-2020 Marlon Janssen Arao
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sunlife.ascp.mail;

import com.sunlife.ascp.connectivity.EmailConnector;
import stormpot.BlazePool;
import stormpot.Config;
import stormpot.LifecycledResizablePool;
import stormpot.Timeout;

import javax.mail.MessagingException;
import java.util.concurrent.TimeUnit;

public class PostOffice {

  private final LifecycledResizablePool<Courier> pool;


  public PostOffice(EmailConnector connector) {
    Postmaster postmaster = new Postmaster(connector);
    Config<Courier> configuration =
        new Config<Courier>().setAllocator(postmaster);
    pool = new BlazePool<Courier>(configuration);
  }

  public PostOffice(EmailConnector connector,
                    boolean verifyConnectivity) {
    Postmaster postmaster = new Postmaster(connector);
    Config<Courier> configuration =
        new Config<Courier>().setAllocator(postmaster);

    if (verifyConnectivity) {
      configuration.setExpiration(new ConnectivityExpiration());
    }

    pool = new BlazePool<Courier>(configuration);
  }


  public void close() throws InterruptedException {
    pool.shutdown().await(new Timeout(1, TimeUnit.MINUTES));
  }


  public void serve(SendMinistration action) throws InterruptedException,
                                                     MessagingException {
    Courier courier = pool.claim(new Timeout(5, TimeUnit.SECONDS));
    try {
      action.post(courier);
    } finally {
      courier.release();
    }
  }
}
