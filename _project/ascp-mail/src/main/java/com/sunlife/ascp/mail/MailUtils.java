/*
 * Copyright 2010-2020 Marlon Janssen Arao
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sunlife.ascp.mail;

import com.sunlife.ascp.connectivity.EmailConnector;
import com.sunlife.ascp.connectivity.SMTPConnector;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.validator.routines.EmailValidator;

import javax.mail.internet.AddressException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Properties;

public class MailUtils {

  public final List<String> acquireEmailsSafely(final String emails,
                                                final char separator)
      throws AddressException {
    if (StringUtils.isNotBlank(emails)) {
      String[] potentialEmails = emails.split(String.valueOf(separator));
      if (ArrayUtils.isNotEmpty(potentialEmails)) {
        List<String> emailAddresses =
            new ArrayList<String>(potentialEmails.length);
        for (String potentialEmail : potentialEmails) {
          if (EmailValidator.getInstance().isValid(potentialEmail.trim())) {
            emailAddresses.add(potentialEmail.trim());
          }
        }
        return Collections.unmodifiableList(emailAddresses);
      }
    }
    return Collections.unmodifiableList(new ArrayList<String>());
  }

  public final MailProtocol reckonProtocol(final EmailConnector connector) {
    if (SMTPConnector.class.isAssignableFrom(connector.getClass())) {
      return MailProtocol.SMTP;
    }
    return MailProtocol.EXCHANGE;
  }

  public final boolean isSMTP(final EmailConnector connector) {
    if (MailProtocol.SMTP.equals(reckonProtocol(connector))) {
      return true;
    }
    return false;
  }

  public final boolean isNotSMTP(final EmailConnector connector) {
    return !isSMTP(connector);
  }

  public final Properties toProperties(final SMTPConnector connector) {
    Properties properties = new Properties();
    properties.put("mail.smtp.host", connector.getHost());
    properties.put("mail.smtp.port", connector.getPort());
    properties.put("mail.smtp.timeout", connector.getTimeout());
    properties.put("mail.smtp.auth", true);
    properties.put("mail.smtp.starttls.enable", true);
    properties.put("mail.smtp.ssl.trust", connector.getHost());
    return properties;
  }


  private MailUtils() {}
  private static class LazyInnerClassSingleton {
    private static final MailUtils INSTANCE = new MailUtils();
  }
  public static MailUtils get() {
    return MailUtils.LazyInnerClassSingleton.INSTANCE;
  }
}
