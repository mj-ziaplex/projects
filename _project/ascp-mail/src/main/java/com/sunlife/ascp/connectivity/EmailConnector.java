/*
 * Copyright 2010-2020 Marlon Janssen Arao
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sunlife.ascp.connectivity;

import com.sunlife.ascp.mail.MailAuthentication;

public abstract class EmailConnector<T extends EmailConnector<T>>
                                       implements Connector<T> {

  private String host;
  private int port;
  private String user;
  private String password;

  private boolean useAUTH = true;
  private boolean enableStartTLS = true;

  private MailAuthentication authenticationType;


  public EmailConnector(final String host, final int port) {
    this.host = host;
    this.port = port >= 0 && port <= 65535 ? port : 0;
  }


  @Override
  public final T setHost(final String host) {
    this.host = host;
    return (T) this;
  }

  @Override
  public final String getHost() {
    return host;
  }

  @Override
  public final T setPort(final int port) {
    this.port = port;
    return (T) this;
  }

  @Override
  public final int getPort() {
    return port;
  }

  @Override
  public final T setUser(final String user) {
    this.user = user;
    return (T) this;
  }

  @Override
  public final String getUser() {
    return user;
  }

  @Override
  public final T setPassword(final String password) {
    this.password = password;
    return (T) this;
  }

  @Override
  public final String getPassword() {
    return password;
  }

  public boolean isUseAUTH() {
    return useAUTH;
  }

  public T useAUTH(boolean use) {
    useAUTH = use;
    return (T) this;
  }

  public boolean isStartTLSEnabled() {
    return enableStartTLS;
  }

  public T enableStartTLS(boolean enable) {
    enableStartTLS = enable;
    return (T) this;
  }


  @Override
  public final String getDefaultHost() {
    return "localhost";
  }


  @Override
  public abstract int getDefaultPort();

  @Override
  public abstract String getDefaultUser();

  public final MailAuthentication getAuthenticationType() {
    return authenticationType;
  }

  public final T setAuthenticationType(MailAuthentication authenticationType) {
    this.authenticationType = authenticationType;
    return (T) this;
  }
}
