/*
 * Copyright 2010-2020 Marlon Janssen Arao
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sunlife.ascp.mail;

import com.sunlife.ascp.connectivity.EmailConnector;
import com.sunlife.ascp.connectivity.SMTPConnector;
import org.joda.time.DateTime;
import stormpot.Poolable;
import stormpot.Slot;

import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.MimeMessage;

public class Courier implements Poolable {

  private final Slot SLOT;
  private final EmailConnector CONNECTOR;


  public Courier(EmailConnector connection, Slot slot) {
    CONNECTOR = connection;
    SLOT = slot;
  }


  public EmailConnector getConnector() {
    return CONNECTOR;
  }


  public Session getSession() {
    if (!CONNECTOR.getClass().isAssignableFrom(SMTPConnector.class) &&
        !SMTPConnector.class.isAssignableFrom(CONNECTOR.getClass())) {
      return null; // return pop3 Session
    }
    return MailSession.build().smtpWith((SMTPConnector) CONNECTOR);
  }

  public void send(Email email) throws MessagingException {
    if (MailUtils.get().isNotSMTP(CONNECTOR)) {
      throw new IllegalStateException("The Courier was not configured to send e-mails.");
    }

    MimeMessage message = email.getMimeMessage(getSession());
    email.setDateSent(DateTime.now());
    message.setSentDate(email.getDateSent().toDate());
    Transport.send(message);
  }

  @Override
  public void release() {
    SLOT.release(this);
  }
}
