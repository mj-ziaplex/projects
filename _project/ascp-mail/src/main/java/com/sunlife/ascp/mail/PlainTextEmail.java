/*
 * Copyright 2010-2020 Marlon Janssen Arao
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sunlife.ascp.mail;

import javax.mail.BodyPart;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

public final class PlainTextEmail extends Email {

  @Override
  public MimeMessage getMimeMessage(Session session) throws MessagingException {
    if (getMessage() == null) {
      throw new IllegalArgumentException("The email body's message must never be null.");
    }
    MimeMessage emailMessage = new MimeMessage(session);
    setBasicHeader(emailMessage);
    if (hasAttachments()) {
      BodyPart messageBodyPart = new MimeBodyPart();
      messageBodyPart.setText(getMessage());
      Multipart multipart = new MimeMultipart();
      multipart.addBodyPart(messageBodyPart);
      processAttachments(multipart);
      emailMessage.setContent(multipart);
    } else {
      emailMessage.setText(getMessage(), getCharacterSet());
    }
    return emailMessage;
  }
}
