package com.sunlife.ascp.connectivity;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;

public class SMTPServiceScan {

  private final String HOST;
  private final int PORT;


  public SMTPServiceScan(final String host, final int port) {
    HOST = host;
    PORT = port;
  }

  public SMTPServiceScan(final InetAddress host, final int port) {
    this(host.getHostName(), port);
  }


  public boolean isReady() {
    Socket socket = null;
    PrintWriter input = null;
    BufferedReader output = null;
    try {
      socket = new Socket(HOST, PORT);
      input = new PrintWriter(socket.getOutputStream(), true);
      output =
          new BufferedReader(new InputStreamReader(socket.getInputStream()));
      input.println("HELO " + HOST);
      input.println("MAIL FROM: #ASCP-WORKFLOW-DEV@sunlife.com");
      input.println("RCPT TO: #ascp-workflow-aos@sunlife.com");
      return true;
    } catch (IOException e) {
      return false;
    } finally {
      input.close();
      try {
        output.close();
        socket.close();
      } catch (IOException e) { /* ignore */ }
    }
  }
}
