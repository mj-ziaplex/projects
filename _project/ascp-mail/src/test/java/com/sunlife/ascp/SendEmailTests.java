package com.sunlife.ascp;

import com.sunlife.ascp.connectivity.SMTPConnector;
import com.sunlife.ascp.mail.*;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.mail.MessagingException;
import java.io.File;

public class SendEmailTests {

  private static final String SENDER = "marlon.arao@sunlife.com";
  private static final String RECIPIENT = "marlon.arao@rcggs.com";
  private static final String PLAIN_TEXT_MESSAGE = "Testing plain text e-mail message.";
  private static final String HTML_MESSAGE = "<b>Testing HTML e-mail message.</b>";

  private static String hostname;
  private static int port;
  private static String username;
  private static String password;

  private static PostOffice postOffice;
  private static Email email;

  private static File attachment;


  @BeforeClass
  public static void onceExecutedBeforeAll() {
    hostname = "smtp.mailtrap.io";
    port = 2525;
    username = "c594650cfbb69f";
    password = "0ecb0927544b60";

    attachment = new File(SendEmailTests.class
                                        .getClassLoader()
                                        .getResource("PLAN.xml")
                                        .getFile());

    SMTPConnector configuration =
        new SMTPConnector(hostname, MailAuthentication.STARTTLS_STLS);
    configuration.setPort(port);
    configuration.setUser(username);
    configuration.setPassword(password);
    postOffice = new PostOffice(configuration);
  }

  @Test
  public void testPlainTextWithoutAttachment() throws MessagingException, InterruptedException {
    email = new PlainTextEmail().from(SENDER)
                                .to(RECIPIENT)
                                .setSubject("Plain text without attachment")
                                .setMessage(PLAIN_TEXT_MESSAGE);
    postOffice.serve(new SendMinistration() {
      @Override
      public void post(Courier mailman) throws MessagingException {
        mailman.send(email);
      }
    });
  }

  @Test
  public void testPlainTextWithAttachment() throws MessagingException, InterruptedException {
    email = new PlainTextEmail().from(SENDER)
                                .to(RECIPIENT)
                                .setSubject("Plain text with attachment")
                                .addAttachment(attachment)
                                .setMessage(PLAIN_TEXT_MESSAGE);
    postOffice.serve(new SendMinistration() {
      @Override
      public void post(Courier mailman) throws MessagingException {
        mailman.send(email);
      }
    });
  }

  @Test
  public void testHTMLWithoutAttachment() throws MessagingException, InterruptedException {
    email = new HTMLEmail().from(SENDER)
                           .to(RECIPIENT)
                           .setSubject("HTML without attachment")
                           .setMessage(HTML_MESSAGE);
    postOffice.serve(new SendMinistration() {
      @Override
      public void post(Courier mailman) throws MessagingException {
        mailman.send(email);
      }
    });
  }

  @Test
  public void testHTMLWithAttachment() throws MessagingException, InterruptedException {
    email = new HTMLEmail().from(SENDER)
                           .to(RECIPIENT)
                           .setSubject("HTML with attachment")
                           .addAttachment(attachment)
                           .setMessage(HTML_MESSAGE);
    postOffice.serve(new SendMinistration() {
      @Override
      public void post(Courier mailman) throws MessagingException {
        mailman.send(email);
      }
    });
  }
}
