package com.sunlife.ascp;

import com.sunlife.ascp.connectivity.GmailSMTPConnector;
import com.sunlife.ascp.mail.*;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.mail.MessagingException;
import java.io.File;
import java.net.InetAddress;
import java.net.UnknownHostException;

public class SendGmailTests {

  private static final String SENDER = "marlon.arao@sunlife.com";
  private static final String RECIPIENT = "marlon.arao@rcggs.com";
  private static final String PLAIN_TEXT_MESSAGE = "Testing plain text e-mail message.";
  private static final String HTML_MESSAGE = "<b>Testing HTML e-mail message.</b>";

  private static PostOffice postOffice;
  private static Email email;

  private static File attachment;


  @BeforeClass
  public static void onceExecutedBeforeAll() {
    String username = "ziaplex.mjarao@gmail.com";
    String password = "/D3v1c3/Null";

    attachment = new File(SendEmailTests.class
                                        .getClassLoader()
                                        .getResource("PLAN.xml")
                                        .getFile());

    GmailSMTPConnector configuration = new GmailSMTPConnector();
    configuration.setUser(username);
    configuration.setPassword(password);
    postOffice = new PostOffice(configuration);
  }

  @Test
  public void testMisc() throws UnknownHostException {
    InetAddress ip = InetAddress.getByName("smtp.gmail.com");
    System.out.println(ip.getHostName());
  }

  @Test
  public void testPlainTextWithoutAttachment() throws MessagingException, InterruptedException {
    email = new PlainTextEmail().from(SENDER)
                                .to(RECIPIENT)
                                .setSubject("Plain text without attachment")
                                .setMessage(PLAIN_TEXT_MESSAGE);
    postOffice.serve(new SendMinistration() {
      @Override
      public void post(Courier mailman) throws MessagingException {
        mailman.send(email);
      }
    });
  }

  @Test
  public void testPlainTextWithAttachment() throws MessagingException, InterruptedException {
    email = new PlainTextEmail().from(SENDER)
                                .to(RECIPIENT)
                                .setSubject("Plain text with attachment")
                                .addAttachment(attachment)
                                .setMessage(PLAIN_TEXT_MESSAGE);
    postOffice.serve(new SendMinistration() {
      @Override
      public void post(Courier mailman) throws MessagingException {
        mailman.send(email);
      }
    });
  }

  @Test
  public void testHTMLWithoutAttachment() throws MessagingException, InterruptedException {
    email = new HTMLEmail().from(SENDER)
                           .to(RECIPIENT)
                           .setSubject("HTML without attachment")
                           .setMessage(HTML_MESSAGE);
    postOffice.serve(new SendMinistration() {
      @Override
      public void post(Courier mailman) throws MessagingException {
        mailman.send(email);
      }
    });
  }

  @Test
  public void testHTMLWithAttachment() throws MessagingException, InterruptedException {
    email = new HTMLEmail().from(SENDER)
                           .to(RECIPIENT)
                           .setSubject("HTML with attachment")
                           .addAttachment(attachment)
                           .setMessage(HTML_MESSAGE);
    postOffice.serve(new SendMinistration() {
      @Override
      public void post(Courier mailman) throws MessagingException {
        mailman.send(email);
      }
    });
  }
}
