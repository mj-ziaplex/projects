package com.slocpi.ium.workflow;

import java.sql.Connection;
import java.util.ResourceBundle;

import com.slocpi.ium.data.AssessmentRequestData;
import com.slocpi.ium.data.ClientData;
import com.slocpi.ium.data.NotificationTemplateData;
import com.slocpi.ium.data.PolicyRequirementsData;
import com.slocpi.ium.data.StatusData;
import com.slocpi.ium.data.dao.WorkflowDAO;
import com.slocpi.ium.data.util.DataSourceProxy;
import com.slocpi.ium.util.IUMConstants;

import junit.framework.TestCase;

public class ContentAssemblerTest extends TestCase {
	private final String NOTIFICATION_ID = "7";
	private final String LOB = IUMConstants.LOB_INDIVIDUAL_LIFE;
	private final String REF_TEST = "080001233";
	private final String CLIENT_ID = "3001006259";
	private final String REQ_CODE = IUMConstants.REQUIREMENT_CODE_RECEIVED_APPLICATION_BY_USD;
	
	private DataSourceProxy dsProxy;
	private Connection con;	
	private WorkflowDAO wfDao;
	private ResourceBundle rb ;
	
	protected void setUp() throws Exception {
		super.setUp();
		dsProxy = new DataSourceProxy();
		con = dsProxy.getConnection();
		wfDao = new WorkflowDAO(con);
		rb = ResourceBundle.getBundle(IUMConstants.IUM_CONFIG);
	}
	
	protected void tearDown() throws Exception {
		dsProxy.closeConnection(con);
		super.tearDown();
	}

	public void testConnection() throws Exception {
		assertTrue(dsProxy != null);
		assertTrue(con != null);
		assertTrue(!con.isClosed());
		assertTrue(rb != null);
	}
	
	public ContentAssemblerTest(String name) {
		super(name);
	}

	/**
	 * @throws Exception
	 */
	public void testAgentNameReplacement() throws Exception {
		PolicyRequirementsData prd = buildPolicyRequirementsData();
		WorkflowItem wfi = buildWorkflowItem();
		NotificationTemplateData ntd = wfDao.getNotificationTemplate(NOTIFICATION_ID);
		String subjectTemplate = ntd.getSubject();
		String expectedSubjectTemplate = "<Reference No> - <Client Name> <Agent Id>:<Agent Name>"; 
		assertEquals(expectedSubjectTemplate, subjectTemplate);
		
		ContentAssembler assembler = new ContentAssembler();
		String content = assembler.assemblePRContent(subjectTemplate, prd, wfi);
		String expectedSubject = "0800012330 - F000000001VV L000000001VV 050232:ROSARIO LIM";
		assertEquals(expectedSubject, content);
	}

	private WorkflowItem buildWorkflowItem() {
		WorkflowItem wfi = new WorkflowItem();
		wfi.setLOB(LOB);
		return wfi;
	}

	private PolicyRequirementsData buildPolicyRequirementsData() {
		PolicyRequirementsData prd = new PolicyRequirementsData(); 
		prd.setClientId(CLIENT_ID);
		prd.setReferenceNumber(REF_TEST);
		prd.setStatus(buildStatusData());
		prd.setRequirementCode(REQ_CODE);
		return prd;
	}

	private StatusData buildStatusData() {
		StatusData data = new StatusData();
		data.setStatusId(0);
		return data;
	}
	
}
