select LB.LOB_DESC
     , AR.REFERENCE_NUM
     , to_char(SA.ACT_DATETIME, 'DDMONYYYY HH24:MI:SS') as START_DATE
     , to_char(EA.ACT_DATETIME, 'DDMONYYYY HH24:MI:SS') as END_DATE
     , EA.ACT_DATETIME - SA.ACT_DATETIME as ELAPSED_TIME
  from ASSESSMENT_REQUESTS AR 
     , LINES_OF_BUSINESS LB
     , (select UAR_REFERENCE_NUM
             , ACT_DATETIME
          from ACTIVITY_LOGS
         where STAT_ID = 1
           and ACT_DATETIME >= to_date('16JAN2004', 'DDMONYYYY')
        ) SA
     , (select UAR_REFERENCE_NUM
             , ACT_DATETIME
          from ACTIVITY_LOGS
         where STAT_ID = 2
           and ACT_DATETIME <= to_date('18JAN2004', 'DDMONYYYY')
        ) EA
 where LB.LOB_CODE = AR.LOB
   and AR.REFERENCE_NUM = SA.UAR_REFERENCE_NUM
   and AR.REFERENCE_NUM = EA.UAR_REFERENCE_NUM
-- compute count(ar.reference_num)
 order by LB.LOB_DESC
        , AR.REFERENCE_NUM
        , START_DATE
        , END_DATE
