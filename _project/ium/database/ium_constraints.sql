-- Primary Key

ALTER TABLE accesses
 ADD CONSTRAINT pk_accesses
 PRIMARY KEY (acc_id)
 USING INDEX;

-- Primary Key

ALTER TABLE access_templates
 ADD CONSTRAINT pk_access_template 
 PRIMARY KEY (templt_id)
 USING INDEX;
 
-- Primary Key 

ALTER TABLE access_template_details
 ADD CONSTRAINT pk_access_template_details 
 PRIMARY KEY (atd_id)
 USING INDEX;

-- Primary Key 

ALTER TABLE activity_logs
 ADD CONSTRAINT pk_activity_logs
 PRIMARY KEY (user_id, evnt_id, uar_reference_num)
 USING INDEX;

-- Primary Key 

ALTER TABLE assessment_requests
 ADD CONSTRAINT pk_assessment_requests
 PRIMARY KEY (reference_num)
 USING INDEX;

 
-- Primary Key 

ALTER TABLE audit_trails
 ADD CONSTRAINT pk_audit_trails
 PRIMARY KEY (tran_num)
 USING INDEX;

-- Primary Key 

ALTER TABLE auto_assignments
 ADD CONSTRAINT pk_auto_assignments
 PRIMARY KEY (user_id, aac_id)
 USING INDEX;
-- Primary Key 

ALTER TABLE auto_assignment_criteria
 ADD CONSTRAINT pk_auto_assignment_criteria
 PRIMARY KEY (aac_id)
 USING INDEX;

-- Primary Key 

ALTER TABLE clients
 ADD CONSTRAINT pk_clients
 PRIMARY KEY (client_id)
 USING INDEX;

-- Primary Key 

ALTER TABLE client_data_sheets
 ADD CONSTRAINT pk_client_data_sheets
 PRIMARY KEY (cds_reference_num, cds_client_num)
 USING INDEX;

-- Primary Key 

ALTER TABLE client_types
 ADD CONSTRAINT pk_client_types
 PRIMARY KEY (clt_code)
 USING INDEX;
 
-- Primary Key 

ALTER TABLE departments
 ADD CONSTRAINT pk_departments
 PRIMARY KEY (dept_code)
 USING INDEX;

-- Primary Key 

ALTER TABLE document_types
 ADD PRIMARY KEY (doc_code)
 USING INDEX;

-- Primary Key 

ALTER TABLE examination_places
 ADD CONSTRAINT pk_examination_places
 PRIMARY KEY (ep_id)
 USING INDEX;

-- Primary Key 

ALTER TABLE examiners
 ADD CONSTRAINT pk_examiners
 PRIMARY KEY (exmnr_id)
 USING INDEX;

-- Primary Key 

ALTER TABLE examiner_specializations
 ADD CONSTRAINT pk_examiner_specializations
 PRIMARY KEY (exmnr_id, spl_id)
 USING INDEX;

-- Primary Key 

ALTER TABLE facilities
 ADD CONSTRAINT pk_facilities
 PRIMARY KEY (fac_id)
 USING INDEX;
 
-- Primary Key 

ALTER TABLE folder_documents
 ADD CONSTRAINT pk_folder_documents
 PRIMARY KEY (doc_id)
 USING INDEX;

-- Primary Key 

ALTER TABLE holidays
 ADD CONSTRAINT pk_holidays
 PRIMARY KEY (hol_id)
 USING INDEX;
 
-- Primary Key 

ALTER TABLE impairments
 ADD CONSTRAINT pk_impairments
 PRIMARY KEY (imp_impairement_id)
 USING INDEX;

-- Primary Key 

ALTER TABLE kickout_messages
 ADD CONSTRAINT pk_kickout_messages
 PRIMARY KEY (ko_sequence_num)
 USING INDEX;
 
-- Primary Key 

ALTER TABLE laboratories
 ADD CONSTRAINT pk_laboratories
 PRIMARY KEY (lab_id)
 USING INDEX;

-- Primary Key 

ALTER TABLE laboratory_tests
 ADD CONSTRAINT pk_laboratory_tests
 PRIMARY KEY (lab_id, test_id)
 USING INDEX;

-- Primary Key 

ALTER TABLE lines_of_business
 ADD CONSTRAINT pk_lines_of_business
 PRIMARY KEY (lob_code)
 USING INDEX;
 
-- Primary Key 

ALTER TABLE lob_documents
 ADD CONSTRAINT pk_lob_documents
 PRIMARY KEY (lob_code, doc_code)
 USING INDEX;

-- Primary Key 

ALTER TABLE lob_requirements
 ADD CONSTRAINT pk_lob_requirements
 PRIMARY KEY (lob_code, reqt_code)
 USING INDEX;

-- Primary Key 

ALTER TABLE lob_status
 ADD CONSTRAINT pk_lob_status
 PRIMARY KEY (lob_code, stat_id)
 USING INDEX;
 
-- Primary Key 

ALTER TABLE medical_bills
 ADD CONSTRAINT pk_medical_bills
 PRIMARY KEY (mb_bill_number)
 USING INDEX;

-- Primary Key

ALTER TABLE medical_notes
 ADD CONSTRAINT pk_medical_notes
 PRIMARY KEY (mn_notes_id)
 USING INDEX;

-- Primary Key 

ALTER TABLE medical_records
 ADD CONSTRAINT pk_medical_records
 PRIMARY KEY (med_record_id)
 USING INDEX;

-- Primary Key 

ALTER TABLE mib_actions
 ADD CONSTRAINT pk_mib_actions
 PRIMARY KEY (mib_act_code)
 USING INDEX;

-- Primary Key 

ALTER TABLE mib_letters
 ADD CONSTRAINT pk_mib_letters
 PRIMARY KEY (mib_lttr_code)
 USING INDEX;

-- Primary Key

ALTER TABLE mib_numbers
 ADD CONSTRAINT pk_mib_numbers
 PRIMARY KEY (mib_num_code)
 USING INDEX;

-- Primary Key 

ALTER TABLE mib_schedules
 ADD CONSTRAINT pk_mib_schedules
 PRIMARY KEY (mib_sked_id)
 USING INDEX;

-- Primary Key 

ALTER TABLE notification_recepients
 ADD CONSTRAINT pk_notification_recepients
 PRIMARY KEY (not_id, role_id, lob_code)
 USING INDEX;

-- Primary Key 

ALTER TABLE notification_templates
 ADD CONSTRAINT pk_notification_templates
 PRIMARY KEY (not_id)
 USING INDEX;
 
-- Primary Key 

ALTER TABLE pages
 ADD CONSTRAINT pk_pages
 PRIMARY KEY (page_id)
 USING INDEX;
 
-- Primary Key 

ALTER TABLE policy_coverage_details
 ADD CONSTRAINT pk_policy_coverage_details
 PRIMARY KEY (pcd_policy_num)
 USING INDEX;

-- Primary Key 

ALTER TABLE policy_medical_records
 ADD CONSTRAINT pk_policy_medical_records
 PRIMARY KEY (med_record_id, uar_reference_num)
 USING INDEX;

-- Primary Key 

ALTER TABLE policy_requirements
 ADD CONSTRAINT pk_policy_requirements
 PRIMARY KEY (pr_requirement_id)
 USING INDEX;


-- Primary Key

ALTER TABLE process_configurations
 ADD CONSTRAINT pk_process_configurations
 PRIMARY KEY (evnt_id)
 USING INDEX;


-- Primary Key 

ALTER TABLE ranks
 ADD CONSTRAINT pk_ranks
 PRIMARY KEY (rank_id)
 USING INDEX;

-- Primary Key 

ALTER TABLE requirements
 ADD CONSTRAINT pk_requirements
 PRIMARY KEY (reqt_code)
 USING INDEX;


-- Primary Key 

ALTER TABLE requirement_forms
 ADD CONSTRAINT pk_requirement_forms
 PRIMARY KEY (rf_id)
 USING INDEX;

-- Primary Key 

ALTER TABLE roles
 ADD CONSTRAINT pk_roles
 PRIMARY KEY (role_code)
 USING INDEX;
 
-- Primary Key 

ALTER TABLE sections
 ADD CONSTRAINT pk_sections
 PRIMARY KEY (sec_code)
 USING INDEX;

-- Primary Key 

ALTER TABLE specializations
 ADD CONSTRAINT pk_specializations
 PRIMARY KEY (spl_id)
 USING INDEX;

-- Primary Key 

ALTER TABLE status
 ADD CONSTRAINT pk_status
 PRIMARY KEY (stat_id)
 USING INDEX;


-- Primary Key 

ALTER TABLE sunlife_offices
 ADD CONSTRAINT pk_sunlife_offices
 PRIMARY KEY (slo_office_code)
 USING INDEX;
 
-- Primary Key 

ALTER TABLE test_profiles
 ADD CONSTRAINT pk_test_profiles
 PRIMARY KEY (test_id)
 USING INDEX;
 
-- Primary Key 

ALTER TABLE users
 ADD CONSTRAINT pk_users
 PRIMARY KEY (user_id)
 USING INDEX;

-- Primary Key 

ALTER TABLE user_accesses
 ADD CONSTRAINT pk_user_accesses
 PRIMARY KEY (user_id, page_id)
 USING INDEX;
 
-- Primary Key 

ALTER TABLE user_process_preferences
 ADD CONSTRAINT pk_user_process_preferences
 PRIMARY KEY (user_id, evnt_id)
 USING INDEX;

-- Foreign Key

ALTER TABLE access_templates
 ADD CONSTRAINT fk_atp_roles 
 FOREIGN KEY (role_id)
 REFERENCES roles (role_code)
 ON DELETE SET NULL; 

-- Foreign Key

ALTER TABLE access_template_details
 ADD CONSTRAINT fk_atd_access_templates 
 FOREIGN KEY (templt_id)
 REFERENCES access_templates (templt_id)
 ON DELETE SET NULL;

ALTER TABLE access_template_details
 ADD CONSTRAINT fk_atd_pages 
 FOREIGN KEY (page_id)
 REFERENCES pages (page_id)
 ON DELETE SET NULL;

ALTER TABLE access_template_details
 ADD CONSTRAINT fk_atd_access_template_details
 FOREIGN KEY  (acc_id)
 REFERENCES accesses (acc_id)
 ON DELETE SET NULL;

-- Foreign Key

ALTER TABLE activity_logs
 ADD CONSTRAINT fk_act_users
 FOREIGN KEY (user_id)
 REFERENCES users (user_id)
 ON DELETE SET NULL;


ALTER TABLE activity_logs
 ADD CONSTRAINT fk_act_process_configurations
 FOREIGN KEY (evnt_id)
 REFERENCES process_configurations (evnt_id)
 ON DELETE SET NULL;

ALTER TABLE activity_logs
 ADD CONSTRAINT fk_act_assessment_requests
 FOREIGN KEY (uar_reference_num)
 REFERENCES assessment_requests (reference_num)
 ON DELETE SET NULL;
 
-- Foreign Key

ALTER TABLE assessment_requests
 ADD CONSTRAINT fk_ar_lines_of_business
 FOREIGN KEY (lob)
 REFERENCES lines_of_business (lob_code)
 ON DELETE SET NULL;

ALTER TABLE assessment_requests
 ADD CONSTRAINT fk_ar_clients_insured
 FOREIGN KEY (insured_client_id)
 REFERENCES clients (client_id)
 ON DELETE SET NULL;

ALTER TABLE assessment_requests
 ADD CONSTRAINT fk_ar_clients_owner
 FOREIGN KEY (owner_client_id)
 REFERENCES clients (client_id)
 ON DELETE SET NULL;

ALTER TABLE assessment_requests
 ADD CONSTRAINT fk_ar_branches
 FOREIGN KEY (branch_id)
 REFERENCES sunlife_offices (slo_office_code)
 ON DELETE SET NULL;

ALTER TABLE assessment_requests
 ADD CONSTRAINT fk_ar_users_agent
 FOREIGN KEY (agent_id)
 REFERENCES users (user_id)
 ON DELETE SET NULL;

ALTER TABLE assessment_requests
 ADD CONSTRAINT fk_ar_status
 FOREIGN KEY (status_id)
 REFERENCES  status (stat_id)
 ON DELETE SET NULL;

ALTER TABLE assessment_requests
 ADD CONSTRAINT fk_ar_users_assigned_to
 FOREIGN KEY (assigned_to)
 REFERENCES users (user_id)
 ON DELETE SET NULL;

ALTER TABLE assessment_requests
 ADD CONSTRAINT fk_ar_users_underwriter
 FOREIGN KEY (underwriter)
 REFERENCES users (user_id)
 ON DELETE SET NULL;

ALTER TABLE assessment_requests
 ADD CONSTRAINT fk_ar_folder_location
 FOREIGN KEY (folder_location)
 REFERENCES users (user_id)
 ON DELETE SET NULL;

ALTER TABLE assessment_requests
 ADD CONSTRAINT fk_ar_users_created_by
 FOREIGN KEY (created_by)
 REFERENCES users (user_id)
 ON DELETE SET NULL;

ALTER TABLE assessment_requests
 ADD CONSTRAINT fk_ar_users_updated_by
 FOREIGN KEY (updated_by)
 REFERENCES users (user_id)
 ON DELETE SET NULL;


-- Foreign Key

ALTER TABLE auto_assignments
 ADD CONSTRAINT fk_aac_users
 FOREIGN KEY (user_id)
 REFERENCES users (user_id)
 ON DELETE SET NULL;
 
ALTER TABLE auto_assignments
 ADD CONSTRAINT fk_aac_auto_assignment_cri
 FOREIGN KEY (aac_id)
 REFERENCES auto_assignment_criteria (aac_id)
 ON DELETE SET NULL;
 

-- Check Constraint

ALTER TABLE auto_assignment_criteria /*nag-error SKIPPED*/
 ADD CONSTRAINT chk_acc_status
 CHECK (aac_status IN ('0','1'));

-- Check Constraints

ALTER TABLE clients
  ADD CONSTRAINT chk_smoker
  CHECK (cl_smoker IN ('0','1'));
  
-- Foreign Key

ALTER TABLE client_data_sheets
 ADD CONSTRAINT fk_cds_clients
 FOREIGN KEY (cds_client_num)
 REFERENCES clients (client_id)
 ON DELETE SET NULL;

-- Foreign Keys

ALTER TABLE examiners
 ADD CONSTRAINT fk_exm_ranks
 FOREIGN KEY (exmnr_rank_id)
 REFERENCES ranks (rank_id)
 ON DELETE SET NULL;

ALTER TABLE examiners
 ADD CONSTRAINT fk_exm_examination_places
 FOREIGN KEY (exmnr_place)
 REFERENCES examination_places (ep_id)
 ON DELETE SET NULL;

ALTER TABLE examiners
 ADD CONSTRAINT fk_exm_users_created_by
 FOREIGN KEY (created_by)
 REFERENCES users (user_id)
 ON DELETE SET NULL;

ALTER TABLE examiners
 ADD CONSTRAINT fk_exm_users_updated_by
 FOREIGN KEY (updated_by)
 REFERENCES users (user_id)
 ON DELETE SET NULL;

-- Foreign Key

ALTER TABLE examiner_specializations
 ADD CONSTRAINT fk_esp_examiners
 FOREIGN KEY (exmnr_id)
 REFERENCES examiners (exmnr_id)
 ON DELETE SET NULL;

ALTER TABLE examiner_specializations
 ADD CONSTRAINT fk_esp_specializations
 FOREIGN KEY (spl_id)
 REFERENCES specializations (spl_id)
 ON DELETE SET NULL;

-- Foreign Key

ALTER TABLE folder_documents
 ADD CONSTRAINT fk_doc_assessment_requests
 FOREIGN KEY (uar_reference_num)
 REFERENCES assessment_requests (reference_num)
 ON DELETE SET NULL;

ALTER TABLE folder_documents
 ADD CONSTRAINT fk_doc_document_types
 FOREIGN KEY (doc_code)
 REFERENCES document_types (doc_code)
 ON DELETE SET NULL;

ALTER TABLE folder_documents
 ADD CONSTRAINT fk_doc_users
 FOREIGN KEY (doc_received_by)
 REFERENCES users (user_id)
 ON DELETE SET NULL;

-- Foreign Key

ALTER TABLE impairments
 ADD CONSTRAINT fk_imp_clients
 FOREIGN KEY (cl_client_num)
 REFERENCES clients (client_id)
 ON DELETE SET NULL;

ALTER TABLE impairments
 ADD CONSTRAINT fk_imp_assessment_requests
 FOREIGN KEY (uar_reference_num)
 REFERENCES assessment_requests (reference_num)
 ON DELETE SET NULL;

ALTER TABLE impairments
 ADD CONSTRAINT fk_imp_mib_numbers
 FOREIGN KEY (imp_relationship)
 REFERENCES mib_numbers (mib_num_code)
 ON DELETE SET NULL;

ALTER TABLE impairments
 ADD CONSTRAINT fk_imp_mib_letters
 FOREIGN KEY (imp_origin)
 REFERENCES mib_letters (mib_lttr_code)
 ON DELETE SET NULL;

ALTER TABLE impairments
 ADD CONSTRAINT fk_imp_users_created_by
 FOREIGN KEY (created_by)
 REFERENCES users (user_id)
 ON DELETE SET NULL;

ALTER TABLE impairments
 ADD CONSTRAINT fk_imp_users_updated_by
 FOREIGN KEY (updated_by)
 REFERENCES users (user_id)
 ON DELETE SET NULL;
 
-- Foreign Key

ALTER TABLE kickout_messages
 ADD CONSTRAINT fk_ko_assessment_requests
 FOREIGN KEY (uar_reference_num)
 REFERENCES assessment_requests (reference_num)
 ON DELETE SET NULL;

ALTER TABLE kickout_messages
 ADD CONSTRAINT fk_ko_clients
 FOREIGN KEY (client_id)
 REFERENCES clients (client_id)
 ON DELETE SET NULL;

-- Foreign Key

ALTER TABLE laboratories
 ADD CONSTRAINT fk_users_lab_created_by
 FOREIGN KEY (created_by)
 REFERENCES users (user_id)
 ON DELETE SET NULL;

ALTER TABLE laboratories
 ADD CONSTRAINT fk_users_lab_updated_by
 FOREIGN KEY (updated_by)
 REFERENCES users (user_id)
 ON DELETE SET NULL;
 
-- Foreign Key

ALTER TABLE laboratory_tests
 ADD CONSTRAINT pk_test_laboratories
 FOREIGN KEY (lab_id)
 REFERENCES laboratories (lab_id)
 ON DELETE SET NULL;

ALTER TABLE laboratory_tests
 ADD CONSTRAINT fk_test_test_profiles
 FOREIGN KEY (test_id)
 REFERENCES test_profiles (test_id)
 ON DELETE SET NULL;

-- Foreign Key

ALTER TABLE lob_documents 
 ADD CONSTRAINT fk_ldc_lines_of_business
 FOREIGN KEY (lob_code)
 REFERENCES lines_of_business (lob_code)
 ON DELETE SET NULL;

ALTER TABLE lob_documents 
 ADD CONSTRAINT fk_ldc_document_types
 FOREIGN KEY (doc_code)
 REFERENCES document_types (doc_code)
 ON DELETE SET NULL;

-- Foreign Key

ALTER TABLE lob_requirements
 ADD CONSTRAINT fk_lrq_lines_of_business
 FOREIGN KEY (lob_code)
 REFERENCES lines_of_business (lob_code)
 ON DELETE SET NULL;

ALTER TABLE lob_requirements
 ADD CONSTRAINT fk_lrq_requirements
 FOREIGN KEY (reqt_code)
 REFERENCES requirements (reqt_code)
 ON DELETE SET NULL;
 
-- Foreign Key

ALTER TABLE lob_status
 ADD CONSTRAINT fk_lbs_lines_of_business
 FOREIGN KEY (lob_code)
 REFERENCES lines_of_business (lob_code)
 ON DELETE SET NULL;

ALTER TABLE lob_status
 ADD CONSTRAINT fk_lbs_status
 FOREIGN KEY (stat_id)
 REFERENCES status (stat_id)
 ON DELETE SET NULL;

-- Foreign Key

ALTER TABLE medical_bills
 ADD CONSTRAINT fk_mb_assessment_requests
 FOREIGN KEY (uar_reference_num)
 REFERENCES assessment_requests (reference_num)
 ON DELETE SET NULL;

ALTER TABLE medical_bills
 ADD CONSTRAINT fk_mb_examiners
 FOREIGN KEY (exmnr_id)
 REFERENCES examiners (exmnr_id)
 ON DELETE SET NULL;

ALTER TABLE medical_bills
 ADD CONSTRAINT fk_mb_laboratories
 FOREIGN KEY (lab_id)
 REFERENCES laboratories (lab_id)
 ON DELETE SET NULL;
 
-- Foreign Key

ALTER TABLE medical_notes
 ADD CONSTRAINT fk_mn_assessment_requests
 FOREIGN KEY (uar_reference_num)
 REFERENCES assessment_requests (reference_num)
 ON DELETE SET NULL;

ALTER TABLE medical_notes
 ADD CONSTRAINT fk_mn_users
 FOREIGN KEY (mn_posted_by)
 REFERENCES users (user_id)
 ON DELETE SET NULL;
 
-- Foreign Key

ALTER TABLE medical_records
 ADD CONSTRAINT fk_med_clients
 FOREIGN KEY (cl_client_num)
 REFERENCES clients (client_id)
 ON DELETE SET NULL;

ALTER TABLE medical_records
 ADD CONSTRAINT fk_med_users_agent
 FOREIGN KEY (agent_id)
 REFERENCES users (user_id)
 ON DELETE SET NULL;

ALTER TABLE medical_records
 ADD CONSTRAINT fk_med_branches
 FOREIGN KEY (branch_id)
 REFERENCES sunlife_offices (slo_office_code)
 ON DELETE SET NULL;

ALTER TABLE medical_records
 ADD CONSTRAINT fk_med_status
 FOREIGN KEY (med_status)
 REFERENCES status (stat_id)
 ON DELETE SET NULL;

ALTER TABLE medical_records
 ADD CONSTRAINT fk_med_examination_places
 FOREIGN KEY (med_examination_place)
 REFERENCES examination_places (ep_id)
 ON DELETE SET NULL;

ALTER TABLE medical_records
 ADD CONSTRAINT fk_med_laboratories
 FOREIGN KEY (med_lab_id)
 REFERENCES laboratories (lab_id)
 ON DELETE SET NULL;

ALTER TABLE medical_records
 ADD CONSTRAINT fk_med_examiners
 FOREIGN KEY (exmnr_id)
 REFERENCES examiners (exmnr_id)
 ON DELETE SET NULL;

ALTER TABLE medical_records
 ADD CONSTRAINT fk_med_test_profiles
 FOREIGN KEY (med_test_id)
 REFERENCES test_profiles (test_id)
 ON DELETE SET NULL;

ALTER TABLE medical_records
 ADD CONSTRAINT fk_med_sections
 FOREIGN KEY (med_section_id)
 REFERENCES sections (sec_code)
 ON DELETE SET NULL;

ALTER TABLE medical_records
 ADD CONSTRAINT fk_med_departments
 FOREIGN KEY (med_department_id)
 REFERENCES departments (dept_code)
 ON DELETE SET NULL;

ALTER TABLE medical_records
 ADD CONSTRAINT fk_med_users_created_by
 FOREIGN KEY (created_by)
 REFERENCES users (user_id)
 ON DELETE SET NULL;

ALTER TABLE medical_records
 ADD CONSTRAINT fk_med_users_updated_by
 FOREIGN KEY (updated_by)
 REFERENCES users (user_id)
 ON DELETE SET NULL;

-- Foreign Key

ALTER TABLE notification_recepients
 ADD CONSTRAINT fk_not_notification_templates
 FOREIGN KEY (not_id)
 REFERENCES notification_templates (not_id)
 ON DELETE SET NULL;

ALTER TABLE notification_recepients
 ADD CONSTRAINT fk_not_role
 FOREIGN KEY (role_id)
 REFERENCES roles (role_code)
 ON DELETE SET NULL;
 
 ALTER TABLE notification_recepients
 ADD CONSTRAINT fk_not_lines_of_business
 FOREIGN KEY (lob_code)
 REFERENCES lines_of_business (lob_code)
 ON DELETE SET NULL;


-- Foreign Key

ALTER TABLE notification_templates
 ADD CONSTRAINT fk_ntt_users_created_by
 FOREIGN KEY (created_by)
 REFERENCES users (user_id)
 ON DELETE SET NULL;

ALTER TABLE notification_templates
 ADD CONSTRAINT fk_ntt_users_updated_by
 FOREIGN KEY (updated_by)
 REFERENCES users (user_id)
 ON DELETE SET NULL;
 
-- Foreign Key

ALTER TABLE policy_coverage_details
 ADD CONSTRAINT fk_pcd_assessment_requests
 FOREIGN KEY (uar_reference_num)
 REFERENCES assessment_requests (reference_num)
 ON DELETE SET NULL;

ALTER TABLE policy_coverage_details
 ADD CONSTRAINT fk_pcd_clients
 FOREIGN KEY (pcd_client_num)
 REFERENCES clients (client_id)
 ON DELETE SET NULL;
 
-- Foreign Key

ALTER TABLE policy_medical_records
 ADD CONSTRAINT fk_pmr_assessment_request
 FOREIGN KEY (uar_reference_num)
 REFERENCES assessment_requests (reference_num)
 ON DELETE SET NULL;

-- Foreign Key

ALTER TABLE policy_requirements
 ADD CONSTRAINT fk_pr_assessment_requests
 FOREIGN KEY (uar_reference_num)
 REFERENCES assessment_requests (reference_num)
 ON DELETE SET NULL;

ALTER TABLE policy_requirements
 ADD CONSTRAINT fk_pr_requirements
 FOREIGN KEY (pr_reqt_code)
 REFERENCES requirements (reqt_code)
 ON DELETE SET NULL;

ALTER TABLE policy_requirements
 ADD CONSTRAINT fk_pr_status
 FOREIGN KEY (pr_status_id)
 REFERENCES status (stat_id)
 ON DELETE SET NULL;

ALTER TABLE policy_requirements
 ADD CONSTRAINT fk_pr_users_created_by
 FOREIGN KEY (created_by)
 REFERENCES users (user_id)
 ON DELETE SET NULL;

ALTER TABLE policy_requirements
 ADD CONSTRAINT fk_pr_users_updated_by
 FOREIGN KEY (updated_by)
 REFERENCES users (user_id)
 ON DELETE SET NULL;

-- Foreign Keys

ALTER TABLE process_configurations
 ADD CONSTRAINT fk_pc_lines_of_business
 FOREIGN KEY (lob_code)
 REFERENCES lines_of_business (lob_code);

ALTER TABLE process_configurations
 ADD CONSTRAINT fk_pc_user_created_by
 FOREIGN KEY (created_by)
 REFERENCES users (user_id);

ALTER TABLE process_configurations
 ADD CONSTRAINT fk_pc_users_updated_by
 FOREIGN KEY (updated_by)
 REFERENCES users (user_id);

ALTER TABLE process_configurations
 ADD CONSTRAINT fk_pc_status_from_status
 FOREIGN KEY (evnt_from_status)
 REFERENCES status (stat_id);

ALTER TABLE process_configurations
 ADD CONSTRAINT fk_pc_status_to_status
 FOREIGN KEY (evnt_to_status)
 REFERENCES status (stat_id);

-- Foreign Key

ALTER TABLE requirements
 ADD CONSTRAINT fk_reqt_requirement_forms
 FOREIGN KEY (reqt_form_id)
 REFERENCES requirement_forms (rf_id)
 ON DELETE SET NULL;

-- Check Constraint

ALTER TABLE status
 ADD CONSTRAINT chk_stat_type
 CHECK (stat_type IN ('AR','NM','M'));

-- Foreign Key

ALTER TABLE users
 ADD CONSTRAINT fk_usr_sunlife_offices
 FOREIGN KEY (slo_id)
 REFERENCES sunlife_offices (slo_office_code)
 ON DELETE SET NULL;

ALTER TABLE users
 ADD CONSTRAINT fk_usr_users_created_by
 FOREIGN KEY (created_by)
 REFERENCES users (user_id)
 ON DELETE SET NULL;

ALTER TABLE users
 ADD CONSTRAINT fk_usr_users_updated_by
 FOREIGN KEY (updated_by)
 REFERENCES users (user_id)
 ON DELETE SET NULL;

-- Foreign Key

ALTER TABLE user_accesses
 ADD CONSTRAINT fk_uac_users
 FOREIGN KEY (user_id)
 REFERENCES users (user_id)
 ON DELETE SET NULL;

ALTER TABLE user_accesses
 ADD CONSTRAINT fk_uac_accesses
 FOREIGN KEY (acc_id)
 REFERENCES accesses (acc_id)
 ON DELETE SET NULL;
 
 ALTER TABLE user_accesses
 ADD CONSTRAINT fk_uac_pages
 FOREIGN KEY (page_id)
 REFERENCES pages (page_id)
 ON DELETE SET NULL;

-- Foreign Key

ALTER TABLE user_process_preferences
 ADD CONSTRAINT fk_upp_users
 FOREIGN KEY (user_id)
 REFERENCES users (user_id)
 ON DELETE SET NULL;

ALTER TABLE user_process_preferences
 ADD CONSTRAINT fk_upp_process_configurations
 FOREIGN KEY (evnt_id)
 REFERENCES process_configurations (evnt_id)
 ON DELETE SET NULL;

-- Check Constraint

ALTER TABLE user_process_preferences
 ADD CONSTRAINT chk_upp_notification_ind
 CHECK (upp_notification_ind IN ('0','1'));

-- Constraints for USER_ROLES

ALTER TABLE user_roles
 ADD CONSTRAINT pk_user_roles
 PRIMARY KEY (role_code, user_id)
 USING INDEX;

-- Foreign Key
ALTER TABLE user_roles
 ADD CONSTRAINT fk_ur_roles
 FOREIGN KEY (role_code)
 REFERENCES roles (role_code) 
 ON DELETE SET NULL;

ALTER TABLE user_roles
 ADD CONSTRAINT fk_users
 FOREIGN KEY (user_id)
 REFERENCES users (user_id) 
 ON DELETE SET NULL;
