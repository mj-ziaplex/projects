/*Assessment Request*/
INSERT INTO status (stat_id, stat_desc,stat_type)
	VALUES (seq_status.NEXTVAL, 'NB Review Action','AR');
	
INSERT INTO status (stat_id, stat_desc,stat_type)
	VALUES (seq_status.NEXTVAL, 'For Transmittal USD','AR');
	
INSERT INTO status (stat_id, stat_desc,stat_type)
	VALUES (seq_status.NEXTVAL, 'For Facilitator''s Action','AR');

INSERT INTO status (stat_id, stat_desc,stat_type)
	VALUES (seq_status.NEXTVAL, 'For Assessment','AR');

INSERT INTO status (stat_id, stat_desc,stat_type)
	VALUES (seq_status.NEXTVAL, 'Undergoing Assessment','AR');

INSERT INTO status (stat_id, stat_desc,stat_type)
	VALUES (seq_status.NEXTVAL, 'For Approval','AR');

INSERT INTO status (stat_id, stat_desc,stat_type)
	VALUES (seq_status.NEXTVAL, 'Approved','AR');

INSERT INTO status (stat_id, stat_desc,stat_type)
	VALUES (seq_status.NEXTVAL, 'Declined','AR');

INSERT INTO status (stat_id, stat_desc,stat_type)
	VALUES (seq_status.NEXTVAL, 'For Offer','AR');

INSERT INTO status (stat_id, stat_desc,stat_type)
	VALUES (seq_status.NEXTVAL, 'Awaiting Requirements','AR');

INSERT INTO status (stat_id, stat_desc,stat_type)
	VALUES (seq_status.NEXTVAL, 'Awaiting Medical','AR');

INSERT INTO status (stat_id, stat_desc,stat_type)
	VALUES (seq_status.NEXTVAL, 'Not Proceeded With','AR');

/*Non-Medical Requirements*/
INSERT INTO status (stat_id, stat_desc,stat_type)
	VALUES (seq_status.NEXTVAL, 'Ordered','NM');

INSERT INTO status (stat_id, stat_desc,stat_type)
	VALUES (seq_status.NEXTVAL, 'Received in site','NM');

INSERT INTO status (stat_id, stat_desc,stat_type)
	VALUES (seq_status.NEXTVAL, 'Waived','NM');

INSERT INTO status (stat_id, stat_desc,stat_type)
	VALUES (seq_status.NEXTVAL, 'Reviewed And Rejected','NM');

INSERT INTO status (stat_id, stat_desc,stat_type)
	VALUES (seq_status.NEXTVAL, 'Reviewed And Accepted','NM');

/*Medical Requirement*/
INSERT INTO status (stat_id, stat_desc,stat_type)
	VALUES (seq_status.NEXTVAL, 'Requested','M');

INSERT INTO status (stat_id, stat_desc,stat_type)
	VALUES (seq_status.NEXTVAL, 'Confirmed','M');
	
INSERT INTO status (stat_id, stat_desc,stat_type)
	VALUES (seq_status.NEXTVAL, 'Valid','M');
	
INSERT INTO status (stat_id, stat_desc,stat_type)
	VALUES (seq_status.NEXTVAL, 'Expired','M');
	
INSERT INTO status (stat_id, stat_desc,stat_type)
	VALUES (seq_status.NEXTVAL, 'Not Submitted','M');

INSERT INTO status (stat_id, stat_desc,stat_type)
	VALUES (seq_status.NEXTVAL, 'Cancelled','M');

INSERT INTO status (stat_id, stat_desc,stat_type)
	VALUES (seq_status.NEXTVAL, 'NTO','NM');

INSERT INTO status (stat_id, stat_desc,stat_type)
	VALUES (seq_status.NEXTVAL, 'SAA','NM');

INSERT INTO status (stat_id, stat_desc,stat_type)
	VALUES (seq_status.NEXTVAL, 'SAA','NM');

INSERT INTO status (stat_id, stat_desc,stat_type)
	VALUES (seq_status.NEXTVAL, 'SIR','NM');
/*Generic*/
INSERT INTO status (stat_id, stat_desc,stat_type)
	VALUES (121, 'Cancelled', 'NM');

INSERT INTO status (stat_id, stat_desc,stat_type)
	VALUES (141, 'Cancelled', 'AR');
