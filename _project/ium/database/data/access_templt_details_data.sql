
/* START Assessment Request */
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (1, 'NBSTF', 1, 2);
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (2, 'NBSTF', 2, 2);
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (3, 'NBSTF', 3, 2);
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (4, 'NBSTF', 4, 2);
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (5, 'NBSTF', 5, 2);
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (6, 'NBSTF', 6, 2);

INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (7, 'NBSUP', 1, 2);
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (8, 'NBSUP', 2, 2);
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (9, 'NBSUP', 3, 2);
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (10, 'NBSUP', 4, 2);
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (11, 'NBSUP', 5, 2);
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (12, 'NBSUP', 6, 2);

INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (13, 'FC', 1, 1);
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (14, 'FC', 2, 1);
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (15, 'FC', 3, 1);
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (16, 'FC', 4, 1);
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (17, 'FC', 5, 1);
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (18, 'FC', 6, 1);

INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (19, 'UW', 1, 3);
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (20, 'UW', 2, 3);
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (21, 'UW', 3, 3);
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (22, 'UW', 4, 3);
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (23, 'UW', 5, 3);
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (24, 'UW', 6, 3);

INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (25, 'UWSUP', 1, 3);
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (26, 'UWSUP', 2, 3);
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (27, 'UWSUP', 3, 3);
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (28, 'UWSUP', 4, 3);
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (29, 'UWSUP', 5, 3);
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (30, 'UWSUP', 6, 3);

INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (31, 'MKSTF', 1, 1);
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (32, 'MKSTF', 2, 1);
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (33, 'MKSTF', 3, 1);
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (34, 'MKSTF', 4, 1);
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (35, 'MKSTF', 5, 1);
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (36, 'MKSTF', 6, 1);

INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (37, 'AGENT', 1, 1);
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (38, 'AGENT', 2, 1);
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (39, 'AGENT', 3, 1);
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (40, 'AGENT', 4, 1);
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (41, 'AGENT', 5, 1);
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (42, 'AGENT', 6, 1);

INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (43, 'OUSER', 1, 1);
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (44, 'OUSER', 2, 1);
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (45, 'OUSER', 3, 1);
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (46, 'OUSER', 4, 1);
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (47, 'OUSER', 5, 1);
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (48, 'OUSER', 6, 1);
/* END Assessment Request */

/* START UW Assessment */
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (49, 'UW', 1, 2);
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (50, 'UW', 5, 2);
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (51, 'UW', 1, 4);
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (52, 'UW', 5, 4);
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (53, 'UW', 1, 5);
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (54, 'UW', 5, 5);

INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (55, 'UWSUP', 1, 2);
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (56, 'UWSUP', 5, 2);
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (57, 'UWSUP', 1, 4);
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (58, 'UWSUP', 5, 4);
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (59, 'UWSUP', 1, 5);
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (60, 'UWSUP', 5, 5);
/* END UW Assessment */

/* START CREATION OF LEGAL FORMS */
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (61, 'FC', 4, 3);
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (62, 'FC', 7, 3);

INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (63, 'UW', 4, 2);
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (64, 'UW', 7, 2);
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (65, 'UW', 7, 3);
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (66, 'UW', 4, 5);
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (67, 'UW', 7, 5);

INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (68, 'UWSUP', 4, 2);
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (69, 'UWSUP', 7, 2);
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (70, 'UWSUP', 7, 3);
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (71, 'UWSUP', 4, 5);
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (72, 'UWSUP', 7, 5);
/* END  CREATION OF LEGAL FORMS */

/* START SENDING EMAIL MESSAGES */
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (73, 'FC', 1, 5);
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (74, 'FC', 2, 5);
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (75, 'FC', 4, 5);
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (76, 'FC', 5, 5);
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (77, 'FC', 7, 5);
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (78, 'FC', 8, 5);

INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (79, 'UW', 2, 5);
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (80, 'UW', 8, 5);


INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (81, 'UWSUP', 2, 5);

INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (82, 'UWSUP', 8, 5);
/* END SENDING EMAIL MESSAGES */

/* START SENDING SMS */
/* SAME AS SENDING EMAIL */
/* END SENDING SMS */

/* START MEDICAL EXAM REQUEST */
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (83, 'NBSTF', 7, 2);
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (84, 'NBSTF', 8, 2);

INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (85, 'NBSUP', 7, 2);
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (86, 'NBSUP', 8, 2);

INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (87, 'FC', 7, 2);
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (88, 'FC', 8, 2);


INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (89, 'UW', 8, 2);

INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (90, 'UWSUP', 8, 2);

/* INSERT FOR mds??? HERE*/
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (91, 'MDADM', 7, 2);
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (92, 'MDADM', 8, 2);
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (93, 'MDADM', 7, 3);
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (94, 'MDADM', 8, 3);
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (95, 'MDSUP', 7, 2);
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (96, 'MDSUP', 8, 2);
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (97, 'MDSUP', 7, 3);
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (98, 'MDSUP', 8, 3);

INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (99, 'MKSTF', 7, 1);
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (100, 'MKSTF', 8, 1);

INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (101, 'AGENT', 7, 1);
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (102, 'AGENT', 8, 1);

INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (103, 'EX', 7, 1);
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (104, 'EX', 8, 1);

INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (105, 'LBSTF', 7, 1);
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (106, 'LBSTF', 8, 1);

INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (107, 'OUSER', 7, 1);
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (108, 'OUSER', 8, 1);
/* END MEDICAL EXAM REQUEST */

/* START MEDICAL CONSULTATION */
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (109, 'MDADM', 6, 2);
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (110, 'MDADM', 6, 3);
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (111, 'MDADM', 6, 4);
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (112, 'MDADM', 6, 5);

INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (113, 'MDSUP', 6, 2);
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (114, 'MDSUP', 6, 3);
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (115, 'MDSUP', 6, 4);
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (116, 'MDSUP', 6, 5);
/* END MEDICAL CONSULTATION */

/* START PARAMETERS/LOV ADMIN MEDICAL LOV */
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (117, 'UW', 9, 1);
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (118, 'UW', 10, 1);
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (119, 'UW', 11, 1);
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (120, 'UW', 12, 1);
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (121, 'UW', 13, 1);
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (122, 'UW', 14, 1);

INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (123, 'UWSUP', 9, 1);
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (124, 'UWSUP', 10, 1);
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (125, 'UWSUP', 11, 1);
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (126, 'UWSUP', 12, 1);
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (127, 'UWSUP', 13, 1);
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (128, 'UWSUP', 14, 1);

INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (129, 'MDADM', 9, 1);
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (130, 'MDADM', 10, 1);
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (131, 'MDADM', 11, 1);
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (132, 'MDADM', 12, 1);
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (133, 'MDADM', 13, 1);
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (134, 'MDADM', 14, 1);

INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (135, 'MDSUP', 9, 2);
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (136, 'MDSUP', 10, 2);
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (137, 'MDSUP', 11, 2);
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (138, 'MDSUP', 12, 2);
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (139, 'MDSUP', 13, 2);
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (140, 'MDSUP', 14, 2);
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (141, 'MDSUP', 9, 3);
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (142, 'MDSUP', 10, 3);
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (143, 'MDSUP', 11, 3);
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (144, 'MDSUP', 12, 3);
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (145, 'MDSUP', 13, 3);
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (146, 'MDSUP', 14, 3);

INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (147, 'LVADM', 9, 2);
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (148, 'LVADM', 10, 2);
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (149, 'LVADM', 11, 2);
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (150, 'LVADM', 12, 2);
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (151, 'LVADM', 13, 2);
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (152, 'LVADM', 14, 2);
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (153, 'LVADM', 9, 3);
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (154, 'LVADM', 10, 3);
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (155, 'LVADM', 11, 3);
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (156, 'LVADM', 12, 3);
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (157, 'LVADM', 13, 3);
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (158, 'LVADM', 14, 3);
/* END PARAMETERS/LOV ADMIN MEDICAL LOV */

/* START USER PROFILE */
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (159, 'FC', 15, 1);
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (160, 'FC', 16, 1);

INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (161, 'UWSUP', 15, 1);
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (162, 'UWSUP', 16, 1);

INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (163, 'MDSUP', 15, 1);
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (164, 'MDSUP', 16, 1);

INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (165, 'SCADM', 15, 2);
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (166, 'SCADM', 16, 2);
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (167, 'SCADM', 15, 3);
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (168, 'SCADM', 16, 3);
/* END USER PROFILE */

/* START SYSTEM MODULES SETUP */
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (169, 'SCADM', 17, 2);
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (170, 'SCADM', 17, 3);
/* END SYSTEM MODULES SETUP */

/* START USER ACCESS RIGHTS */
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (171, 'SCADM', 18, 2);
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (172, 'SCADM', 19, 2);
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (173, 'SCADM', 20, 2);
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (174, 'SCADM', 18, 3);
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (175, 'SCADM', 19, 3);
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (176, 'SCADM', 20, 3);
/* END USER ACCESS RIGHTS */

/* START LOGIN ID RESET */
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (177, 'SCADM', 16, 5);
/* END LOGIN ID RESET */

/* START CHANGE PASSWORD */
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (178, 'NBSTF', 21, 5);
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (179, 'NBSUP', 21, 5);
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (180, 'FC', 21, 5);
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (181, 'UW', 21, 5);
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (182, 'UWSUP', 21, 5);
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (183, 'MDADM', 21, 5);
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (184, 'MDSUP', 21, 5);
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (185, 'MKSTF', 21, 5);
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (186, 'AGENT', 21, 5);
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (187, 'EX', 21, 5);
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (188, 'LBSTF', 21, 5);
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (189, 'OUSER', 21, 5);
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (190, 'LVADM', 21, 5);
INSERT INTO ACCESS_TEMPLATE_DETAILS (ATD_ID, TEMPLT_ID, PAGE_ID, ACC_ID) VALUES (191, 'SCADM', 21, 5);

/* END LOGIN ID RESET */