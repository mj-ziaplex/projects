INSERT INTO lines_of_business (lob_code, lob_desc)
	VALUES ('IL', 'Individual Life');

INSERT INTO lines_of_business (lob_code, lob_desc)
	VALUES ('PN', 'Pre-need');

INSERT INTO lines_of_business (lob_code, lob_desc)
	VALUES ('MF', 'Mutual Funds');
	
INSERT INTO lines_of_business (lob_code, lob_desc)
	VALUES ('GL', 'Group Life');