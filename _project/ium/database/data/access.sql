INSERT INTO ACCESSES (acc_id, acc_desc) values (1, 'Inquire Only');
INSERT INTO ACCESSES (acc_id, acc_desc) values (2, 'Create Records');
INSERT INTO ACCESSES (acc_id, acc_desc) values (3, 'Maintain Records');
INSERT INTO ACCESSES (acc_id, acc_desc) values (4, 'Delete Records');
INSERT INTO ACCESSES (acc_id, acc_desc) values (5, 'Execute Functions');