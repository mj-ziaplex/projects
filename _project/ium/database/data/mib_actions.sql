INSERT INTO mib_actions (mib_act_code, mib_act_desc)
	VALUES ('A', 'Application is still under process');

INSERT INTO mib_actions (mib_act_code, mib_act_desc)
	VALUES ('B', 'BDR');
	
INSERT INTO mib_actions (mib_act_code, mib_act_desc)
	VALUES ('C', 'Cancelled');
	
INSERT INTO mib_actions (mib_act_code, mib_act_desc)
	VALUES ('D', 'Declined');
	
INSERT INTO mib_actions (mib_act_code, mib_act_desc)
	VALUES ('DR', 'Declined Rider');

INSERT INTO mib_actions (mib_act_code, mib_act_desc)
	VALUES ('I', 'Incomplete');

INSERT INTO mib_actions (mib_act_code, mib_act_desc)
	VALUES ('P', 'Postponed');

INSERT INTO mib_actions (mib_act_code, mib_act_desc)
	VALUES ('R', 'Rated');

INSERT INTO mib_actions (mib_act_code, mib_act_desc)
	VALUES ('RB', 'Rated Basic');

INSERT INTO mib_actions (mib_act_code, mib_act_desc)
	VALUES ('Rec', 'Reconsidered');

INSERT INTO mib_actions (mib_act_code, mib_act_desc)
	VALUES ('RR', 'Rated Rider');

INSERT INTO mib_actions (mib_act_code, mib_act_desc)
	VALUES ('S', 'Standard');