INSERT INTO document_types (doc_code, doc_desc)
	VALUES ('APL', 'Application');

INSERT INTO document_types (doc_code, doc_desc)
	VALUES ('COI', 'Certificate of Insurability');

INSERT INTO document_types (doc_code, doc_desc)
	VALUES ('ROF', 'Simplified Reinstatement Offer Form');

INSERT INTO document_types (doc_code, doc_desc)
	VALUES ('ROC', 'Request for Change');

INSERT INTO document_types (doc_code, doc_desc)
	VALUES ('POC', 'Policy Contract');

INSERT INTO document_types (doc_code, doc_desc)
	VALUES ('BOC', 'Bounced Cheque');

INSERT INTO document_types (doc_code, doc_desc)
	VALUES ('SCQ', 'Staled Cheque');