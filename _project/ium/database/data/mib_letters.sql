INSERT INTO mib_letters (mib_lttr_code, mib_lttr_desc)
	VALUES ('m', 'Death');

INSERT INTO mib_letters (mib_lttr_code, mib_lttr_desc)
	VALUES ('w', 'Discovery of the Impairment');
	
INSERT INTO mib_letters (mib_lttr_code, mib_lttr_desc)
	VALUES ('x', 'Operation');
	
INSERT INTO mib_letters (mib_lttr_code, mib_lttr_desc)
	VALUES ('y', 'Treatment');

INSERT INTO mib_letters (mib_lttr_code, mib_lttr_desc)
	VALUES ('z', 'Recovery from Impairment');