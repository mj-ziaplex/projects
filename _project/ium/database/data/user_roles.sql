/* 
 * for assessment request users 1 - shine vicencio
 *                              2 - engel
 *                              3 - cris
 */


/*
 * 1st Set
 */
  
/* For user REQNBAD1 - 1 role */
INSERT INTO user_roles (role_code, user_id) 
values ('GRPNBADMIN','REQNBAD1');

/* For user REQNBST1 - 1 role */
INSERT INTO user_roles (role_code, user_id) 
values ('NBSTAFF','REQNBST1');

/* For user REQNBRV1 - role */
INSERT INTO user_roles (role_code, user_id) 
values ('NBREVIEWER','REQNBRV1');

/* For user REQNBSM1 - 1 role */
INSERT INTO user_roles (role_code, user_id) 
values ('NBSUPMGR','REQNBSM1');

/* For user REQFCSM1 - 1 role */
INSERT INTO user_roles (role_code, user_id) 
values ('FACILITSUPMGR','REQFCSM1');

/* For user REQFC1 - 1 role */
INSERT INTO user_roles (role_code, user_id) 
values ('FACILITATOR','REQFC1');

/* For user REQUW1 - 1 role */
INSERT INTO user_roles (role_code, user_id) 
values ('UNDERWRITER','REQUW1');

/* For user REQUW* - 2 roles */
INSERT INTO user_roles (role_code, user_id) 
values ('UNDERWRITER','REQUW*1');
INSERT INTO user_roles (role_code, user_id) 
values ('UWTNGSUPMGR','REQUW*1');

/* For user REQUWSM1 - 1 role */
INSERT INTO user_roles (role_code, user_id) 
values ('UWTNGSUPMGR','REQUWSM1');

/* For user REQUWS*1 - 2 roles */
INSERT INTO user_roles (role_code, user_id) 
values ('UWTNGSUPMGR','REQUWS*1');
INSERT INTO user_roles (role_code, user_id) 
values ('UNDERWRITER','REQUWS*1');

/* For user REQAG1 - 1 role */
INSERT INTO user_roles (role_code, user_id) 
values ('AGENT','REQAG1');



/*
 * 2nd Set
 */
  
/* For user REQNBAD2 - 1 role */
INSERT INTO user_roles (role_code, user_id) 
values ('GRPNBADMIN','REQNBAD2');

/* For user REQNBST2 - 1 role */
INSERT INTO user_roles (role_code, user_id) 
values ('NBSTAFF','REQNBST2');

/* For user REQNBRV2 - role */
INSERT INTO user_roles (role_code, user_id) 
values ('NBREVIEWER','REQNBRV2');

/* For user REQNBSM2 - 1 role */
INSERT INTO user_roles (role_code, user_id) 
values ('NBSUPMGR','REQNBSM2');

/* For user REQFCSM2 - 1 role */
INSERT INTO user_roles (role_code, user_id) 
values ('FACILITSUPMGR','REQFCSM2');

/* For user REQFC2 - 1 role */
INSERT INTO user_roles (role_code, user_id) 
values ('FACILITATOR','REQFC2');

/* For user REQUW2 - 1 role */
INSERT INTO user_roles (role_code, user_id) 
values ('UNDERWRITER','REQUW2');

/* For user REQUW*2 - 2 roles */
INSERT INTO user_roles (role_code, user_id) 
values ('UNDERWRITER','REQUW*2');
INSERT INTO user_roles (role_code, user_id) 
values ('UWTNGSUPMGR','REQUW*2');

/* For user REQUWSM2 - 1 role */
INSERT INTO user_roles (role_code, user_id) 
values ('UWTNGSUPMGR','REQUWSM2');

/* For user REQUWS*2 - 2 roles */
INSERT INTO user_roles (role_code, user_id) 
values ('UWTNGSUPMGR','REQUWS*2');
INSERT INTO user_roles (role_code, user_id) 
values ('UNDERWRITER','REQUWS*2');

/* For user REQAG2 - 1 role */
INSERT INTO user_roles (role_code, user_id) 
values ('AGENT','REQAG2');

/*
 * 3rd Set
 */
 
/* For user REQNBAD3 - 1 role */
INSERT INTO user_roles (role_code, user_id) 
values ('GRPNBADMIN','REQNBAD3');

/* For user REQNBST3 - 1 role */
INSERT INTO user_roles (role_code, user_id) 
values ('NBSTAFF','REQNBST3');

/* For user REQNBRV2 - role */
INSERT INTO user_roles (role_code, user_id) 
values ('NBREVIEWER','REQNBRV3');

/* For user REQNBSM3 - 1 role */
INSERT INTO user_roles (role_code, user_id) 
values ('NBSUPMGR','REQNBSM3');

/* For user REQFCSM3 - 1 role */
INSERT INTO user_roles (role_code, user_id) 
values ('FACILITSUPMGR','REQFCSM3');

/* For user REQFC3 - 1 role */
INSERT INTO user_roles (role_code, user_id) 
values ('FACILITATOR','REQFC3');

/* For user REQUW3 - 1 role */
INSERT INTO user_roles (role_code, user_id) 
values ('UNDERWRITER','REQUW3');

/* For user REQUW*3 - 2 roles */
INSERT INTO user_roles (role_code, user_id) 
values ('UNDERWRITER','REQUW*3');
INSERT INTO user_roles (role_code, user_id) 
values ('UWTNGSUPMGR','REQUW*3');

/* For user REQUWSM3 - 1 role */
INSERT INTO user_roles (role_code, user_id) 
values ('UWTNGSUPMGR','REQUWSM3');

/* For user REQUWS*3 - 2 roles */
INSERT INTO user_roles (role_code, user_id) 
values ('UWTNGSUPMGR','REQUWS*3');
INSERT INTO user_roles (role_code, user_id) 
values ('UNDERWRITER','REQUWS*3');

/* For user REQAG3 - 1 role */
INSERT INTO user_roles (role_code, user_id) 
values ('AGENT','REQAG3');
 