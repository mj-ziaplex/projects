@data\departments.sql;

@data\document_types;

@data\client_types.sql;

@data\lines_of_business;

@data\mib_actions.sql;

@data\mib_letters.sql;

@data\requirement_forms.sql;

@data\roles.sql;

@data\sections.sql;

@data\sunlife_offices.sql;

@data\status.sql;
