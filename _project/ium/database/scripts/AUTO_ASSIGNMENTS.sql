CREATE TABLE auto_assignments 
(
  aa_id 				NUMBER(3) NOT NULL
, user_id 				VARCHAR2(10)
, aac_id 				NUMBER(3)
, criteria_field_value 	VARCHAR2(25)
, created_by			VARCHAR2(10)
, created_date			TIMESTAMP(6)
, updated_by			VARCHAR2(10)
, updated_date			TIMESTAMP(6)
);

-- Primary Key 
ALTER TABLE auto_assignments
 ADD CONSTRAINT pk_auto_assignments
 PRIMARY KEY (aa_id)
 USING INDEX;

-- Foreign Key
ALTER TABLE auto_assignments
 ADD CONSTRAINT fk_aac_users
 FOREIGN KEY (user_id)
 REFERENCES users (user_id);
 
ALTER TABLE auto_assignments
 ADD CONSTRAINT fk_aac_auto_assignment_cri
 FOREIGN KEY (aac_id)
 REFERENCES auto_assignment_criteria (aac_id);

-- Public synonym
CREATE PUBLIC SYNONYM auto_assignments FOR auto_assignments;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON auto_assignments TO ium_app_user;

-- Primary Key Sequence
CREATE SEQUENCE seq_auto_assignment
	START WITH 1
	INCREMENT BY 1
	MAXVALUE 999;
	 