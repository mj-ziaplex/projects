create or replace trigger TGR_PR_AUDIT_TRAIL
   after update on POLICY_REQUIREMENTS for each row
declare
   changed_from    varchar2(1000);
   changed_to      varchar2(1000);

   procedure APPEND_TO_CHANGED_FROM(old_value varchar2) is
   begin
      if LENGTH(changed_from) = 0 then
         changed_from := '<?xml version="1.0" encoding="ISO-8859-1"?>';
      end if;
      changed_from := changed_from || old_value;
   end;

   procedure APPEND_TO_CHANGED_TO(new_value varchar2) is
   begin
      if LENGTH(changed_to) = 0 then
         changed_to := '<?xml version="1.0" encoding="ISO-8859-1"?>';
      end if;
      changed_to := changed_to || new_value;
   end;

begin
      if nvl(:OLD.UAR_REFERENCE_NUM, '') != nvl(:NEW.UAR_REFERENCE_NUM, '') then
         APPEND_TO_CHANGED_FROM('<ReferenceNumber>' || :OLD.UAR_REFERENCE_NUM || '</ReferenceNumber>');
         APPEND_TO_CHANGED_TO('<ReferenceNumber>' || :NEW.UAR_REFERENCE_NUM || '</ReferenceNumber>');
      end if;
      if nvl(:OLD.PR_REQUIREMENT_ID, 0) != nvl(:NEW.PR_REQUIREMENT_ID, 0) then
         APPEND_TO_CHANGED_FROM('<RequirementID>' || to_char(:OLD.PR_REQUIREMENT_ID) || '</RequirementID>');
         APPEND_TO_CHANGED_TO('<RequirementID>' || to_char(:NEW.PR_REQUIREMENT_ID) || '</RequirementID>');
      end if;
      if nvl(:OLD.PR_ADMIN_SEQUENCE_NUM, 0) != nvl(:NEW.PR_ADMIN_SEQUENCE_NUM, 0) then
         APPEND_TO_CHANGED_FROM('<SequenceNumber>' || to_char(:OLD.PR_ADMIN_SEQUENCE_NUM) || '</SequenceNumber>');
         APPEND_TO_CHANGED_TO('<SequenceNumber>' || to_char(:NEW.PR_ADMIN_SEQUENCE_NUM) || '</SequenceNumber>');
      end if;
      if nvl(:OLD.PR_REQT_CODE, '') != nvl(:NEW.PR_REQT_CODE, '') then
         APPEND_TO_CHANGED_FROM('<RequirementCode>' || :OLD.PR_REQT_CODE || '</RequirementCode>');
         APPEND_TO_CHANGED_TO('<RequirementCode>' || :NEW.PR_REQT_CODE || '</RequirementCode>');
      end if;
      if nvl(:OLD.PR_ADMIN_CREATE_DATE, '') != nvl(:NEW.PR_ADMIN_CREATE_DATE, '') then
         APPEND_TO_CHANGED_FROM('<ABACUSCreatedDate>' || to_char(:OLD.PR_ADMIN_CREATE_DATE) || '</ABACUSCreatedDate>');
         APPEND_TO_CHANGED_TO('<ABACUSCreatedDate>' || to_char(:NEW.PR_ADMIN_CREATE_DATE) || '</ABACUSCreatedDate>');
      end if;
      if nvl(:OLD.PR_STATUS_ID, 0) != nvl(:NEW.PR_STATUS_ID, 0) then
         APPEND_TO_CHANGED_FROM('<Status>' || to_char(:OLD.PR_STATUS_ID) || '</Status>');
         APPEND_TO_CHANGED_TO('<Status>' || to_char(:NEW.PR_STATUS_ID) || '</Status>');
      end if;
      if nvl(:OLD.PR_STATUS_DATE, '') != nvl(:NEW.PR_STATUS_DATE, '') then
         APPEND_TO_CHANGED_FROM('<StatusDate>' || to_char(:OLD.PR_STATUS_DATE) || '</StatusDate>');
         APPEND_TO_CHANGED_TO('<StatusDate>' || to_char(:NEW.PR_STATUS_DATE) || '</StatusDate>');
      end if;
      if nvl(:OLD.PR_CLIENT_TYPE, '') != nvl(:NEW.PR_CLIENT_TYPE, '') then
         APPEND_TO_CHANGED_FROM('<ClientType>' || :OLD.PR_CLIENT_TYPE || '</ClientType>');
         APPEND_TO_CHANGED_TO('<ClientType>' || :NEW.PR_CLIENT_TYPE || '</ClientType>');
      end if;
      if nvl(:OLD.PR_VALIDITY_DATE, '') != nvl(:NEW.PR_VALIDITY_DATE, '') then
         APPEND_TO_CHANGED_FROM('<ValidityDate>' || to_char(:OLD.PR_VALIDITY_DATE) || '</ValidityDate>');
         APPEND_TO_CHANGED_TO('<ValidityDate>' || to_char(:NEW.PR_VALIDITY_DATE) || '</ValidityDate>');
      end if;
      if nvl(:OLD.PR_ORDER_DATE, '') != nvl(:NEW.PR_ORDER_DATE, '') then
         APPEND_TO_CHANGED_FROM('<OrderDate>' || to_char(:OLD.PR_ORDER_DATE) || '</OrderDate>');
         APPEND_TO_CHANGED_TO('<OrderDate>' || to_char(:NEW.PR_ORDER_DATE) || '</OrderDate>');
      end if;
      if nvl(:OLD.PR_DATE_SENT, '') != nvl(:NEW.PR_DATE_SENT, '') then
         APPEND_TO_CHANGED_FROM('<DateSent>' || to_char(:OLD.PR_DATE_SENT) || '</DateSent>');
         APPEND_TO_CHANGED_TO('<DateSent>' || to_char(:NEW.PR_DATE_SENT) || '</DateSent>');
      end if;
      if nvl(:OLD.PR_RECEIVED_DATE, '') != nvl(:NEW.PR_RECEIVED_DATE, '') then
         APPEND_TO_CHANGED_FROM('<ReceivedDate>' || to_char(:OLD.PR_RECEIVED_DATE) || '</ReceivedDate>');
         APPEND_TO_CHANGED_TO('<ReceivedDate>' || to_char(:NEW.PR_RECEIVED_DATE) || '</ReceivedDate>');
      end if;
      if nvl(:OLD.CREATED_BY, '') != nvl(:NEW.CREATED_BY, '') then
         APPEND_TO_CHANGED_FROM('<CreatedBy>' || :OLD.CREATED_BY || '</CreatedBy>');
         APPEND_TO_CHANGED_TO('<CreatedBy>' || :NEW.CREATED_BY || '</CreatedBy>');
      end if;
      if nvl(:OLD.CREATED_DATE, '') != nvl(:NEW.CREATED_DATE, '') then
         APPEND_TO_CHANGED_FROM('<CreatedDate>' || to_char(:OLD.CREATED_DATE) || '</CreatedDate>');
         APPEND_TO_CHANGED_TO('<CreatedDate>' || to_char(:NEW.CREATED_DATE) || '</CreatedDate>');
      end if;
      if nvl(:OLD.UPDATED_BY, '') != nvl(:NEW.UPDATED_BY, '') then
         APPEND_TO_CHANGED_FROM('<UpdatedBy>' || :OLD.UPDATED_BY || '</UpdatedBy>');
         APPEND_TO_CHANGED_TO('<UpdatedBy>' || :NEW.UPDATED_BY || '</UpdatedBy>');
      end if;
      if nvl(:OLD.UPDATED_DATE, '') != nvl(:NEW.UPDATED_DATE, '') then
         APPEND_TO_CHANGED_FROM('<UpdatedDate>' || to_char(:OLD.UPDATED_DATE) || '</UpdatedDate>');
         APPEND_TO_CHANGED_TO('<UpdatedDate>' || to_char(:NEW.UPDATED_DATE) || '</UpdatedDate>');
      end if;

      if LENGTH(changed_from) > 0 then
         PROC_CREATE_AUDIT_TRAIL(2, 'UPDATE', :NEW.UPDATED_BY, to_char(:NEW.PR_REQUIREMENT_ID), changed_from, changed_to);
      end if;
end;
/
