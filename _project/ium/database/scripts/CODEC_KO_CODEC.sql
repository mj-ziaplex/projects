CREATE TABLE CODEC_KO_CODEC
(
 PC_SEQ		NUMBER(4) NOT NULL
,KO_SEQ		NUMBER(5) NOT NULL
);

ALTER TABLE codec_ko_codec
 ADD CONSTRAINT pk_codec_ko_codec
 PRIMARY KEY (pc_seq, ko_seq)
 USING INDEX;


ALTER TABLE codec_ko_codec
 ADD CONSTRAINT fk_ckc_prism
 FOREIGN KEY (pc_seq) 
 REFERENCES prism_codec(pc_seq);

ALTER TABLE codec_ko_codec
 ADD CONSTRAINT fk_ckc_ko
 FOREIGN KEY (ko_seq)
 REFERENCES prism_codec (pc_seq);

-- Public synonym
CREATE PUBLIC SYNONYM codec_ko_codec FOR codec_ko_codec;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON codec_ko_codec TO ium_app_user;

