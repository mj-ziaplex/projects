create or replace trigger TGR_LOB_REQT_AUDIT_TRAIL
   after update or delete on LOB_REQUIREMENTS for each row

declare
   changed_from	varchar2(1000);
   changed_to	varchar2(1000);

   procedure APPEND_TO_CHANGED_FROM(old_value varchar2) is
   begin
      if LENGTH(changed_from) = 0 then
         changed_from := '<?xml version="1.0" encoding="ISO-8859-1"?>';
      end if;
      changed_from := changed_from || old_value;
   end;

   procedure APPEND_TO_CHANGED_TO(new_value varchar2) is
   begin
      if LENGTH(changed_to) = 0 then
         changed_to := '<?xml version="1.0" encoding="ISO-8859-1"?>';
      end if;
      changed_to := changed_to || new_value;
   end;

begin
   if updating then
      if nvl(:OLD.LOB_CODE, '') != nvl(:NEW.LOB_CODE, '') then
         APPEND_TO_CHANGED_FROM('<LineOf Business>' || :OLD.LOB_CODE || '</LineOf Business>');
         APPEND_TO_CHANGED_TO('<LineOf Business>' || :NEW.LOB_CODE || '</LineOf Business>');
      end if;
      if nvl(:OLD.REQT_CODE, '') != nvl(:NEW.REQT_CODE, '') then
         APPEND_TO_CHANGED_FROM('<RequirementCode>' || :OLD.REQT_CODE || '</RequirementCode>');
         APPEND_TO_CHANGED_TO('<RequirementCode>' || :NEW.REQT_CODE || '</RequirementCode>');
      end if;
      if nvl(:OLD.CREATED_BY, '') != nvl(:NEW.CREATED_BY, '') then
         APPEND_TO_CHANGED_FROM('<CreatedBy>' || :OLD.CREATED_BY || '</CreatedBy>');
         APPEND_TO_CHANGED_TO('<CreatedBy>' || :NEW.CREATED_BY || '</CreatedBy>');
      end if;
      if nvl(:OLD.CREATED_DATE, '') != nvl(:NEW.CREATED_DATE, '') then
         APPEND_TO_CHANGED_FROM('<CreatedDate>' || to_char(:OLD.CREATED_DATE) || '</CreatedDate>');
         APPEND_TO_CHANGED_TO('<CreatedDate>' || to_char(:NEW.CREATED_DATE) || '</CreatedDate>');
      end if;
      if nvl(:OLD.UPDATED_BY, '') != nvl(:NEW.UPDATED_BY, '') then
         APPEND_TO_CHANGED_FROM('<UpdatedBy>' || :OLD.UPDATED_BY || '</UpdatedBy>');
         APPEND_TO_CHANGED_TO('<UpdatedBy>' || :NEW.UPDATED_BY || '</UpdatedBy>');
      end if;
      if nvl(:OLD.UPDATED_DATE, '') != nvl(:NEW.UPDATED_DATE, '') then
         APPEND_TO_CHANGED_FROM('<UpdatedDate>' || to_char(:OLD.UPDATED_DATE) || '</UpdatedDate>');
         APPEND_TO_CHANGED_TO('<UpdatedDate>' || to_char(:NEW.UPDATED_DATE) || '</UpdatedDate>');
      end if;

      if LENGTH(changed_from) > 0 then
         PROC_CREATE_AUDIT_TRAIL(44, 'UPDATE', :NEW.UPDATED_BY, :NEW.LOB_CODE || '-' || :NEW.REQT_CODE, changed_from, changed_to);
      end if;
   elsif deleting then
      PROC_CREATE_AUDIT_TRAIL(44, 'DELETE', :OLD.UPDATED_BY, :OLD.LOB_CODE || '-' || :OLD.REQT_CODE, changed_from, changed_to);
   end if;
end;
/