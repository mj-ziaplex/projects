CREATE TABLE auto_assignment_criteria
(
  aac_id 				NUMBER(3) NOT NULL
, aac_field 			VARCHAR2(20)
, aac_desc 				VARCHAR2(40)
, aac_status			VARCHAR2(1)
, created_by			VARCHAR2(10)
, created_date			TIMESTAMP(6)
, updated_by			VARCHAR2(10)
, updated_date			TIMESTAMP(6)
);

-- Primary Key 
ALTER TABLE auto_assignment_criteria
 ADD CONSTRAINT pk_auto_assignment_criteria
 PRIMARY KEY (aac_id)
 USING INDEX;
 
-- Check Constraint
ALTER TABLE auto_assignment_criteria
 ADD CONSTRAINT chk_acc_status
 CHECK (aac_status IN ('0','1'));

-- Public synonym
CREATE PUBLIC SYNONYM auto_assignment_criteria FOR auto_assignment_criteria;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON auto_assignment_criteria TO ium_app_user;

-- Primary Key Sequence
CREATE SEQUENCE seq_auto_assignment_criteria
	START WITH 1
	INCREMENT BY 1
	MAXVALUE 999;
