CREATE TABLE policy_requirements 
(
  pr_requirement_id 	NUMBER(5) NOT NULL
, uar_reference_num 	VARCHAR2(15)
, pr_client_id		VARCHAR2(10)
, pr_admin_sequence_num NUMBER(3)
, pr_reqt_code 		VARCHAR2(5)
, pr_level 		VARCHAR2(1)
, pr_admin_create_date 	DATE
, pr_complt_reqt_ind 	VARCHAR2(1)
, pr_status_id 		NUMBER(4)
, pr_status_date 	DATE
, pr_admin_updtd_by 	VARCHAR2(8)
, pr_admin_updtd_date 	DATE
, pr_client_type 	VARCHAR2(10)
, pr_designation 	VARCHAR2(10)
, pr_test_date 		DATE
, pr_test_result_code 	VARCHAR2(4)
, pr_test_result_desc 	VARCHAR2(40)
, pr_resolved_ind 	VARCHAR2(1)
, pr_follow_up_num 	NUMBER(2)
, pr_follow_up_date 	DATE
, pr_validity_date 	DATE
, pr_paid_ind 		VARCHAR2(1)
, pr_new_test_only 	VARCHAR2(1)
, pr_order_date 	DATE
, pr_date_sent 		DATE
, pr_ccas_suggest_ind 	VARCHAR2(1)
, pr_received_date 	DATE
, pr_auto_order_ind 	VARCHAR2(1)
, pr_fld_comment_ind 	VARCHAR2(1)
, pr_comments 		VARCHAR2(50)
, created_by 		VARCHAR2(10) NOT NULL
, created_date 		TIMESTAMP
, updated_by 		VARCHAR2(10)
, updated_date 		TIMESTAMP
); 

-- Primary Key 

ALTER TABLE policy_requirements
 ADD CONSTRAINT pk_policy_requirements
 PRIMARY KEY (pr_requirement_id)
 USING INDEX;

-- Foreign Key

ALTER TABLE policy_requirements
 ADD CONSTRAINT fk_pr_assessment_requests
 FOREIGN KEY (uar_reference_num)
 REFERENCES assessment_requests (reference_num);

ALTER TABLE policy_requirements
 ADD CONSTRAINT fk_pr_clients
 FOREIGN KEY (pr_client_id)
 REFERENCES clients (client_id);

ALTER TABLE policy_requirements
 ADD CONSTRAINT fk_pr_requirements
 FOREIGN KEY (pr_reqt_code)
 REFERENCES requirements (reqt_code);

ALTER TABLE policy_requirements
 ADD CONSTRAINT fk_pr_status
 FOREIGN KEY (pr_status_id)
 REFERENCES status (stat_id);

ALTER TABLE policy_requirements
 ADD CONSTRAINT fk_pr_users_created_by
 FOREIGN KEY (created_by)
 REFERENCES users (user_id);

ALTER TABLE policy_requirements
 ADD CONSTRAINT fk_pr_users_updated_by
 FOREIGN KEY (updated_by)
 REFERENCES users (user_id);
 
-- Public synonym
CREATE PUBLIC SYNONYM policy_requirements FOR policy_requirements;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON policy_requirements TO ium_app_user;
 