CREATE TABLE lob_status 
(
  lob_code	VARCHAR2(2) NOT NULL
, admin_status_code VARCHAR2(5) NOT NULL 
, admin_status_type VARCHAR2(1) 
, stat_id	NUMBER(4) NOT NULL
, created_by	VARCHAR2(10)
, created_date	TIMESTAMP(6)
, updated_by	VARCHAR2(10)
, updated_date	TIMESTAMP(6)
);

-- Primary Key 

ALTER TABLE lob_status
 ADD CONSTRAINT pk_lob_status
 PRIMARY KEY (lob_code, stat_id)
 USING INDEX;

-- Foreign Key

ALTER TABLE lob_status
 ADD CONSTRAINT fk_lbs_lines_of_business
 FOREIGN KEY (lob_code)
 REFERENCES lines_of_business (lob_code);

ALTER TABLE lob_status
 ADD CONSTRAINT fk_lbs_status
 FOREIGN KEY (stat_id)
 REFERENCES status (stat_id);
 
-- Public synonym
CREATE PUBLIC SYNONYM lob_status FOR lob_status;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON lob_status TO ium_app_user;
 