CREATE TABLE EXCEPTION_DETAILS
(
  EXCEPTION_ID     NUMBER(8)
, MESSAGE          VARCHAR2(50)
);

ALTER TABLE exception_details
 ADD CONSTRAINT fk_exception_log
 FOREIGN KEY (exception_id)
 REFERENCES exception_log(exception_id);

-- Public synonym
CREATE PUBLIC SYNONYM exception_details FOR exception_details;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON exception_details TO ium_app_user;

