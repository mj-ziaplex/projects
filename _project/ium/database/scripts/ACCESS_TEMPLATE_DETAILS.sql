CREATE TABLE access_template_details
(
  templt_id			NUMBER(5)
, pa_id				NUMBER(5)
, atd_access_ind	VARCHAR2(1)
, created_by		VARCHAR2(10)
, created_date		TIMESTAMP(6)
, updated_by		VARCHAR2(10)
, updated_date		TIMESTAMP(6)
);

-- Primary Key
ALTER TABLE access_template_details
 ADD CONSTRAINT pk_access_template_details
 PRIMARY KEY (templt_id, pa_id)
 USING INDEX;
 
-- Foreign Keys
ALTER TABLE access_template_details
 ADD CONSTRAINT fk_atd_access_template
 FOREIGN KEY (templt_id)
 REFERENCES access_templates (templt_id)
 ON DELETE SET NULL;

ALTER TABLE access_template_details
 ADD CONSTRAINT fk_atd_pages_access
 FOREIGN KEY (pa_id)
 REFERENCES pages_access (pa_id)
 ON DELETE SET NULL; 

-- Public synonym
CREATE PUBLIC SYNONYM access_template_details FOR access_template_details;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON access_template_details TO ium_app_user;
