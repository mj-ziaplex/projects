CREATE TABLE process_configuration_roles
(
  evnt_id 	NUMBER(5) NOT NULL
, role_code VARCHAR2(15) NOT NULL
);

ALTER TABLE process_configuration_roles
	ADD CONSTRAINT pk_process_configuration_roles
	PRIMARY KEY (evnt_id, role_code)
	USING INDEX;
	
ALTER TABLE process_configuration_roles
	ADD CONSTRAINT fk_pc_evnt_id
	FOREIGN KEY (evnt_id)
	REFERENCES process_configurations (evnt_id);

	
ALTER TABLE process_configuration_roles
	ADD CONSTRAINT fk_pc_role_code
	FOREIGN KEY (role_code)
	REFERENCES roles(role_code);
	
-- Public synonym
CREATE PUBLIC SYNONYM process_configuration_roles FOR process_configuration_roles;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON process_configuration_roles TO ium_app_user;
	