CREATE TABLE medical_bills
(
  mb_bill_number	NUMBER(8) NOT NULL
, uar_reference_num 	VARCHAR2(15)
, exmnr_id 		NUMBER(5)
, lab_id 		NUMBER(5)
, mb_fee		NUMBER(8,2)
, mb_date_posted	TIMESTAMP
, created_by	VARCHAR2(10)
, created_date	TIMESTAMP(6)
, updated_by	VARCHAR2(10)
, updated_date	TIMESTAMP(6)
); 

-- Primary Key 

ALTER TABLE medical_bills
 ADD CONSTRAINT pk_medical_bills
 PRIMARY KEY (mb_bill_number)
 USING INDEX;

-- Primay Key Sequence

CREATE SEQUENCE seq_medical_bill
    START WITH 1
	INCREMENT BY 1
	NOMAXVALUE;

-- Foreign Key

ALTER TABLE medical_bills
 ADD CONSTRAINT fk_mb_assessment_requests
 FOREIGN KEY (uar_reference_num)
 REFERENCES assessment_requests (reference_num);

ALTER TABLE medical_bills
 ADD CONSTRAINT fk_mb_examiners
 FOREIGN KEY (exmnr_id)
 REFERENCES examiners (exmnr_id);

ALTER TABLE medical_bills
 ADD CONSTRAINT fk_mb_laboratories
 FOREIGN KEY (lab_id)
 REFERENCES laboratories (lab_id);

-- Public synonym
CREATE PUBLIC SYNONYM medical_bills FOR medical_bills;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON medical_bills TO ium_app_user;
