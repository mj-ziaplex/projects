create or replace trigger TGR_AA_AUDIT_TRAIL
   after update or delete on AUTO_ASSIGNMENTS for each row

declare
   changed_from	varchar2(1000);
   changed_to	varchar2(1000);

   procedure APPEND_TO_CHANGED_FROM(old_value varchar2) is
   begin
      if LENGTH(changed_from) = 0 then
         changed_from := '<?xml version="1.0" encoding="ISO-8859-1"?>';
      end if;
      changed_from := changed_from || old_value;
   end;

   procedure APPEND_TO_CHANGED_TO(new_value varchar2) is
   begin
      if LENGTH(changed_to) = 0 then
         changed_to := '<?xml version="1.0" encoding="ISO-8859-1"?>';
      end if;
      changed_to := changed_to || new_value;
   end;

begin
   if updating then
      if nvl(:OLD.USER_ID, '') != nvl(:NEW.USER_ID, '') then
         APPEND_TO_CHANGED_FROM('<UserCode>' || :OLD.USER_ID || '</UserCode>');
         APPEND_TO_CHANGED_TO('<UserCode>' || :NEW.USER_ID || '</UserCode>');
      end if;
      if nvl(:OLD.AAC_ID, 0) != nvl(:NEW.AAC_ID, 0) then
         APPEND_TO_CHANGED_FROM('<CriteriaID>' || to_char(:OLD.AAC_ID) || '</CriteriaID>');
         APPEND_TO_CHANGED_TO('<CriteriaID>' || to_char(:NEW.AAC_ID) || '</CriteriaID>');
      end if;
      if nvl(:OLD.CRITERIA_FIELD_VALUE, '') != nvl(:NEW.CRITERIA_FIELD_VALUE, '') then
         APPEND_TO_CHANGED_FROM('<FieldValue>' || :OLD.CRITERIA_FIELD_VALUE || '</FieldValue>');
         APPEND_TO_CHANGED_TO('<FieldValue>' || :NEW.CRITERIA_FIELD_VALUE || '</FieldValue>');
      end if;
      if nvl(:OLD.CREATED_BY, '') != nvl(:NEW.CREATED_BY, '') then
         APPEND_TO_CHANGED_FROM('<CreatedBy>' || :OLD.CREATED_BY || '</CreatedBy>');
         APPEND_TO_CHANGED_TO('<CreatedBy>' || :NEW.CREATED_BY || '</CreatedBy>');
      end if;
      if nvl(:OLD.CREATED_DATE, '') != nvl(:NEW.CREATED_DATE, '') then
         APPEND_TO_CHANGED_FROM('<CreatedDate>' || to_char(:OLD.CREATED_DATE) || '</CreatedDate>');
         APPEND_TO_CHANGED_TO('<CreatedDate>' || to_char(:NEW.CREATED_DATE) || '</CreatedDate>');
      end if;
      if nvl(:OLD.UPDATED_BY, '') != nvl(:NEW.UPDATED_BY, '') then
         APPEND_TO_CHANGED_FROM('<UpdatedBy>' || :OLD.UPDATED_BY || '</UpdatedBy>');
         APPEND_TO_CHANGED_TO('<UpdatedBy>' || :NEW.UPDATED_BY || '</UpdatedBy>');
      end if;
      if nvl(:OLD.UPDATED_DATE, '') != nvl(:NEW.UPDATED_DATE, '') then
         APPEND_TO_CHANGED_FROM('<UpdatedDate>' || to_char(:OLD.UPDATED_DATE) || '</UpdatedDate>');
         APPEND_TO_CHANGED_TO('<UpdatedDate>' || to_char(:NEW.UPDATED_DATE) || '</UpdatedDate>');
      end if;

      if LENGTH(changed_from) > 0 then
         PROC_CREATE_AUDIT_TRAIL(25, 'UPDATE', :NEW.UPDATED_BY, :NEW.USER_ID || '-' || to_char(:NEW.AAC_ID), changed_from, changed_to);
      end if;
   elsif deleting then
      PROC_CREATE_AUDIT_TRAIL(25, 'DELETE', :OLD.UPDATED_BY, :OLD.USER_ID || '-' || to_char(:OLD.AAC_ID), changed_from, changed_to);
   end if;
end;
/