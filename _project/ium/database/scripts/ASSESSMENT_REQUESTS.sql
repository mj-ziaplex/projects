CREATE TABLE assessment_requests
(
  reference_num 	VARCHAR2(15) NOT NULL
, lob				VARCHAR2(2)  
, source_system 	VARCHAR2(10)
, amount_covered 	NUMBER(12,2)
, premium			NUMBER(12,2)
, insured_client_id VARCHAR2(10)
, owner_client_id 	VARCHAR2(10)
, branch_id 		VARCHAR2(15) 
, agent_id			VARCHAR2(8)
, status_id 		NUMBER(4)
, status_date 		DATE
, assigned_to 		VARCHAR2(10)
, underwriter 		VARCHAR2(10)
, folder_location 	VARCHAR2(10)
, remarks			LONG
, app_received_date DATE
, date_forwarded 	DATE
, ar_transmit_ind	VARCHAR2(1)
, created_by 		VARCHAR2(10) NOT NULL
, created_date 		TIMESTAMP(6)
, updated_by 		VARCHAR2(10)
, updated_date 		TIMESTAMP(6)
, uw_analysis		LONG
, uw_remarks		LONG
);

-- Primary Key 
ALTER TABLE assessment_requests
 ADD CONSTRAINT pk_assessment_requests
 PRIMARY KEY (reference_num)
 USING INDEX;

-- Foreign Key
ALTER TABLE assessment_requests
 ADD CONSTRAINT fk_ar_lines_of_business
 FOREIGN KEY (lob)
 REFERENCES lines_of_business (lob_code);

ALTER TABLE assessment_requests
 ADD CONSTRAINT fk_ar_clients_insured
 FOREIGN KEY (insured_client_id)
 REFERENCES clients (client_id);

ALTER TABLE assessment_requests
 ADD CONSTRAINT fk_ar_clients_owner
 FOREIGN KEY (owner_client_id)
 REFERENCES clients (client_id);

ALTER TABLE assessment_requests
 ADD CONSTRAINT fk_ar_branches
 FOREIGN KEY (branch_id)
 REFERENCES sunlife_offices (slo_office_code);

ALTER TABLE assessment_requests
 ADD CONSTRAINT fk_ar_users_agent
 FOREIGN KEY (agent_id)
 REFERENCES users (user_id);

ALTER TABLE assessment_requests
 ADD CONSTRAINT fk_ar_status
 FOREIGN KEY (status_id)
 REFERENCES  status (stat_id);

ALTER TABLE assessment_requests
 ADD CONSTRAINT fk_ar_users_assigned_to
 FOREIGN KEY (assigned_to)
 REFERENCES users (user_id);

ALTER TABLE assessment_requests
 ADD CONSTRAINT fk_ar_users_underwriter
 FOREIGN KEY (underwriter)
 REFERENCES users (user_id);

ALTER TABLE assessment_requests
 ADD CONSTRAINT fk_ar_folder_location
 FOREIGN KEY (folder_location)
 REFERENCES users (user_id);

ALTER TABLE assessment_requests
 ADD CONSTRAINT fk_ar_users_created_by
 FOREIGN KEY (created_by)
 REFERENCES users (user_id);

ALTER TABLE assessment_requests
 ADD CONSTRAINT fk_ar_users_updated_by
 FOREIGN KEY (updated_by)
 REFERENCES users (user_id);

-- Public synonym
CREATE PUBLIC SYNONYM assessment_requests FOR assessment_requests;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON assessment_requests TO ium_app_user;
