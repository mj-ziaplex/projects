CREATE TABLE REQUIREMENT_FORM_FIELDS
(
  RFF_ID                 NUMBER(5) not null
, RF_ID                  NUMBER(5) not null
, FF_ID                  NUMBER(2) not null
, RFF_PAGE               NUMBER(1)
, RFF_POSX               NUMBER(3)
, RFF_POSY               NUMBER(3)
);

ALTER TABLE requirement_form_fields
 ADD CONSTRAINT pk_req_form_fields
 PRIMARY KEY (rff_id)
 USING INDEX;

ALTER TABLE requirement_form_fields
 ADD CONSTRAINT fk_form_fields
 FOREIGN KEY (ff_id)
 REFERENCES form_fields(ff_id);

ALTER TABLE requirement_form_fields
 ADD CONSTRAINT fk_req_form
 FOREIGN KEY (rf_id)
 REFERENCES requirement_forms (rf_id);
 
-- Public synonym
CREATE PUBLIC SYNONYM requirement_form_fields FOR requirement_form_fields;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON requirement_form_fields TO ium_app_user;
 