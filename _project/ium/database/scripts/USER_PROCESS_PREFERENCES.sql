CREATE TABLE user_process_preferences
(
  user_id			VARCHAR2(10) NOT NULL
, evnt_id			NUMBER(5) NOT NULL
, upp_notification_ind VARCHAR2(1)
);

ALTER TABLE user_process_preferences
 ADD CONSTRAINT pk_user_process_preferences
 PRIMARY KEY (evnt_id, user_id)
 USING INDEX;
 

ALTER TABLE user_process_preferences
 ADD CONSTRAINT fk_upp_process_configurations
 FOREIGN KEY (evnt_id)
 REFERENCES process_configurations(evnt_id);

ALTER TABLE user_process_preferences
 ADD CONSTRAINT fk_upp_users
 FOREIGN KEY (user_id)
 REFERENCES users(user_id);

-- Public synonym
CREATE PUBLIC SYNONYM user_process_preferences FOR user_process_preferences;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON user_process_preferences TO ium_app_user;
