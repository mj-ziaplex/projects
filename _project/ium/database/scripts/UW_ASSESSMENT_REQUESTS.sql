CREATE TABLE UW_ASSESSMENT_REQUESTS
(
  reference_num	VARCHAR2(15)
, uw_analysis	LONG
, created_by    VARCHAR2(10)
, created_date  TIMESTAMP(6)
, updated_by    VARCHAR2(10)
, updated_date  TIMESTAMP(6));

);

-- Public synonym
CREATE PUBLIC SYNONYM uw_assessment_requests FOR uw_assessment_requests;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON uw_assessment_requests TO ium_app_user;
