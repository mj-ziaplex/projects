CREATE TABLE departments
(
  dept_code	VARCHAR2(10) NOT NULL
, dept_desc	VARCHAR2(50)
, created_by	VARCHAR2(10)
, created_date	TIMESTAMP(6)
, updated_by	VARCHAR2(10)
, updated_date	TIMESTAMP(6)
);

-- Primary Key 

ALTER TABLE departments
 ADD CONSTRAINT pk_departments
 PRIMARY KEY (dept_code)
 USING INDEX;

-- Public synonym
CREATE PUBLIC SYNONYM departments FOR departments;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON departments TO ium_app_user;

