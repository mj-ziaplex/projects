CREATE TABLE accesses
(
  acc_id 			NUMBER(2) NOT NULL
, acc_desc 			VARCHAR2(25)
, created_by		VARCHAR2(10)
, created_date		TIMESTAMP(6)
, updated_by		VARCHAR2(10)
, updated_date		TIMESTAMP(6)
);

-- Primary Key
ALTER TABLE accesses
 ADD CONSTRAINT pk_accesses
 PRIMARY KEY (acc_id)
 USING INDEX;

-- Public synonym
CREATE PUBLIC SYNONYM accesses FOR accesses;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON accesses TO ium_app_user;

-- Primary Key Sequence
CREATE SEQUENCE seq_access
	INCREMENT BY 1
	START WITH 1
	MAXVALUE 99;
