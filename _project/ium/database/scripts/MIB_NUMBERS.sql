CREATE TABLE mib_numbers 
(
  mib_num_code 		VARCHAR2(1) NOT NULL
, mib_num_desc 		VARCHAR2(40)
, created_by	VARCHAR2(10)
, created_date	TIMESTAMP(6)
, updated_by	VARCHAR2(10)
, updated_date	TIMESTAMP(6)
);

-- Primary Key

ALTER TABLE mib_numbers
 ADD CONSTRAINT pk_mib_numbers
 PRIMARY KEY (mib_num_code)
 USING INDEX;

 -- Public synonym
CREATE PUBLIC SYNONYM mib_numbers FOR mib_numbers;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON mib_numbers TO ium_app_user;
 