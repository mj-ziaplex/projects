CREATE TABLE laboratories 
(
  lab_id 		NUMBER(5) NOT NULL
, lab_name 		VARCHAR2(40)
, lab_contact_person 	VARCHAR2(60)
, lab_office_num 	VARCHAR2(13)
, lab_office_num2 	VARCHAR2(13)
, lab_fax_num 		VARCHAR2(13)
, lab_bus_addr_line1 	VARCHAR2(60)
, lab_bus_addr_line2 	VARCHAR2(60)
, lab_bus_addr_line3 	VARCHAR2(60)
, lab_city 		VARCHAR2(25)
, lab_province 		VARCHAR2(25)
, lab_country 		VARCHAR2(25)
, lab_zipcode 		NUMBER(5)
, lab_branch_name 	VARCHAR2(40)
, lab_branch_addr_line1 VARCHAR2(60)
, lab_branch_addr_line2 VARCHAR2(60)
, lab_branch_addr_line3 VARCHAR2(60)
, lab_branch_city 	VARCHAR2(20)
, lab_branch_province 	VARCHAR2(20)
, lab_branch_country 	VARCHAR2(20)
, lab_branch_zipcode 	NUMBER(5)
, lab_branch_phone 	VARCHAR2(13)
, lab_branch_fax_num 	VARCHAR2(13)
, lab_accredit_ind 	VARCHAR2(1)
, lab_accredit_date 	DATE
, lab_email_address		VARCHAR2(40)
, created_by 		VARCHAR2(10) NOT NULL
, created_date 		TIMESTAMP
, updated_by 		VARCHAR2(10)
, updated_date 		TIMESTAMP
);

-- Primary Key 

ALTER TABLE laboratories
 ADD CONSTRAINT pk_laboratories
 PRIMARY KEY (lab_id)
 USING INDEX;

-- Primary Key Sequences

CREATE SEQUENCE seq_laboratory
    START WITH 1
	INCREMENT BY 1
	NOMAXVALUE;

-- Foreign Key

ALTER TABLE laboratories
 ADD CONSTRAINT fk_users_lab_created_by
 FOREIGN KEY (created_by)
 REFERENCES users (user_id);

ALTER TABLE laboratories
 ADD CONSTRAINT fk_users_lab_updated_by
 FOREIGN KEY (updated_by)
 REFERENCES users (user_id);

-- Public synonym
CREATE PUBLIC SYNONYM laboratories FOR laboratories;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON laboratories TO ium_app_user;
