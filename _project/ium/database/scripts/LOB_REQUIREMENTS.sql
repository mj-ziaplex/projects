CREATE TABLE lob_requirements
(
  lob_code			VARCHAR2(2) NOT NULL
, reqt_code			VARCHAR2(5) NOT NULL
, remarks			LONG
, created_by	VARCHAR2(10)
, created_date	TIMESTAMP(6)
, updated_by	VARCHAR2(10)
, updated_date	TIMESTAMP(6)
); 

-- Primary Key 

ALTER TABLE lob_requirements
 ADD CONSTRAINT pk_lob_requirements
 PRIMARY KEY (lob_code, reqt_code)
 USING INDEX;

-- Foreign Key

ALTER TABLE lob_requirements
 ADD CONSTRAINT fk_lrq_lines_of_business
 FOREIGN KEY (lob_code)
 REFERENCES lines_of_business (lob_code);

ALTER TABLE lob_requirements
 ADD CONSTRAINT fk_lrq_requirements
 FOREIGN KEY (reqt_code)
 REFERENCES requirements (reqt_code);

-- Public synonym
CREATE PUBLIC SYNONYM lob_requirements FOR lob_requirements;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON lob_requirements TO ium_app_user;
