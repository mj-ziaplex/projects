CREATE TABLE exception_batch
(
 BATCH_ID          NUMBER(8) NOT NULL
,TIMESTAMP         DATE
,INTERFACE         VARCHAR2(10)
,RECORD_TYPE       VARCHAR2(8)
,REQUEST           VARCHAR2(160)
,RESPONSE          LONG
);

ALTER TABLE exception_batch
 ADD CONSTRAINT pk_exception_batch
 PRIMARY KEY (batch_id)
 USING INDEX;
 

CREATE SEQUENCE SEQ_BATCH_ID
		INCREMENT BY 1
		START WITH 1
		MAXVALUE 99999999;

-- Public synonym
CREATE PUBLIC SYNONYM exception_batch FOR exception_batch;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON exception_batch TO ium_app_user;
