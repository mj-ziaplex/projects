CREATE TABLE test_profiles 
(
  test_id	NUMBER(5) NOT NULL
, test_desc 	VARCHAR2(40)
, test_validity	NUMBER(5)
, test_taxable_ind VARCHAR2(1)
, test_type		VARCHAR2(1)
, created_by	VARCHAR2(10)
, created_date	TIMESTAMP(6)
, updated_by	VARCHAR2(10)
, updated_date	TIMESTAMP(6)
); 

-- Primary Key 

ALTER TABLE test_profiles
 ADD CONSTRAINT pk_test_profiles
 PRIMARY KEY (test_id)
 USING INDEX;

-- Primary Key Sequence

CREATE SEQUENCE seq_test_profile
	START WITH 1
	INCREMENT BY 1
	NOMAXVALUE;

-- Check Constraint

ALTER TABLE test_profiles
 ADD CONSTRAINT chk_taxable_ind
 CHECK (test_taxable_ind IN ('0','1'));
 
-- Public synonym
CREATE PUBLIC SYNONYM test_profiles FOR test_profiles;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON test_profiles TO ium_app_user;
 