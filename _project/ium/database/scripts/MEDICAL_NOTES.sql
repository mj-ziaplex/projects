CREATE TABLE medical_notes
(
  mn_notes_id		NUMBER(8) NOT NULL
, uar_reference_num 	VARCHAR2(15)
, mn_posted_by 		VARCHAR2(10)
, mn_post_date 		DATE
, mn_notes  		LONG
); 

-- Primary Key 

ALTER TABLE medical_notes
 ADD CONSTRAINT pk_medical_notes
 PRIMARY KEY (mn_notes_id)
 USING INDEX;

-- Primary Key Sequences

CREATE SEQUENCE seq_medical_note
    START WITH 1
	INCREMENT BY 1
	NOMAXVALUE;

-- Foreign Key

ALTER TABLE medical_notes
 ADD CONSTRAINT fk_mn_assessment_requests
 FOREIGN KEY (uar_reference_num)
 REFERENCES assessment_requests (reference_num);

ALTER TABLE medical_notes
 ADD CONSTRAINT fk_mn_users
 FOREIGN KEY (mn_posted_by)
 REFERENCES users (user_id);

-- Public synonym
CREATE PUBLIC SYNONYM medical_notes FOR medical_notes;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON medical_notes TO ium_app_user;
