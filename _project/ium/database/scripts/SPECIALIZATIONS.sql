CREATE TABLE specializations 
(
  spl_id 		NUMBER(3) NOT NULL
, spl_desc 		VARCHAR2(40)
, created_by	VARCHAR2(10)
, created_date	TIMESTAMP(6)
, updated_by	VARCHAR2(10)
, updated_date	TIMESTAMP(6)
);

-- Primary Key 

ALTER TABLE specializations
 ADD CONSTRAINT pk_specializations
 PRIMARY KEY (spl_id)
 USING INDEX;

-- Public synonym
CREATE PUBLIC SYNONYM specializations FOR specializations;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON specializations TO ium_app_user;


-- Primary Key Sequence

CREATE SEQUENCE seq_specialization
	START WITH 1
	INCREMENT BY 1
	NOMAXVALUE;