create or replace trigger TGR_RF_AUDIT_TRAIL
   after update or delete on REQUIREMENT_FORMS for each row

declare
   changed_from	varchar2(1000);
   changed_to	varchar2(1000);

   procedure APPEND_TO_CHANGED_FROM(old_value varchar2) is
   begin
      if LENGTH(changed_from) = 0 then
         changed_from := '<?xml version="1.0" encoding="ISO-8859-1"?>';
      end if;
      changed_from := changed_from || old_value;
   end;

   procedure APPEND_TO_CHANGED_TO(new_value varchar2) is
   begin
      if LENGTH(changed_to) = 0 then
         changed_to := '<?xml version="1.0" encoding="ISO-8859-1"?>';
      end if;
      changed_to := changed_to || new_value;
   end;

begin
   if updating then
      if nvl(:OLD.RF_ID, 0) != nvl(:NEW.RF_ID, 0) then
         APPEND_TO_CHANGED_FROM('<FormID>' || to_char(:OLD.RF_ID) || '</FormID>');
         APPEND_TO_CHANGED_TO('<FormID>' || to_char(:NEW.RF_ID) || '</FormID>');
      end if;
      if nvl(:OLD.RF_NAME, '') != nvl(:NEW.RF_NAME, '') then
         APPEND_TO_CHANGED_FROM('<Name>' || :OLD.RF_NAME || '</Name>');
         APPEND_TO_CHANGED_TO('<Name>' || :NEW.RF_NAME || '</Name>');
      end if;
      if nvl(:OLD.RF_TEMPLATE_FORMAT, '') != nvl(:NEW.RF_TEMPLATE_FORMAT, '') then
         APPEND_TO_CHANGED_FROM('<TemplateFormat>' || :OLD.RF_TEMPLATE_FORMAT || '</TemplateFormat>');
         APPEND_TO_CHANGED_TO('<TemplateFormat>' || :NEW.RF_TEMPLATE_FORMAT || '</TemplateFormat>');
      end if;
      if nvl(:OLD.RF_TEMPLATE_NAME, 0) != nvl(:NEW.RF_TEMPLATE_NAME, 0) then
         APPEND_TO_CHANGED_FROM('<TemplateName>' || to_char(:OLD.RF_TEMPLATE_NAME) || '</TemplateName>');
         APPEND_TO_CHANGED_TO('<TemplateName>' || to_char(:NEW.RF_TEMPLATE_NAME) || '</TemplateName>');
      end if;
      if nvl(:OLD.RF_STATUS, '') != nvl(:NEW.RF_STATUS, '') then
         APPEND_TO_CHANGED_FROM('<Status>' || :OLD.RF_STATUS || '</Status>');
         APPEND_TO_CHANGED_TO('<Status>' || :NEW.RF_STATUS || '</Status>');
      end if;
      if nvl(:OLD.CREATED_BY, '') != nvl(:NEW.CREATED_BY, '') then
         APPEND_TO_CHANGED_FROM('<CreatedBy>' || :OLD.CREATED_BY || '</CreatedBy>');
         APPEND_TO_CHANGED_TO('<CreatedBy>' || :NEW.CREATED_BY || '</CreatedBy>');
      end if;
      if nvl(:OLD.CREATED_DATE, '') != nvl(:NEW.CREATED_DATE, '') then
         APPEND_TO_CHANGED_FROM('<CreatedDate>' || to_char(:OLD.CREATED_DATE) || '</CreatedDate>');
         APPEND_TO_CHANGED_TO('<CreatedDate>' || to_char(:NEW.CREATED_DATE) || '</CreatedDate>');
      end if;
      if nvl(:OLD.UPDATED_BY, '') != nvl(:NEW.UPDATED_BY, '') then
         APPEND_TO_CHANGED_FROM('<UpdatedBy>' || :OLD.UPDATED_BY || '</UpdatedBy>');
         APPEND_TO_CHANGED_TO('<UpdatedBy>' || :NEW.UPDATED_BY || '</UpdatedBy>');
      end if;
      if nvl(:OLD.UPDATED_DATE, '') != nvl(:NEW.UPDATED_DATE, '') then
         APPEND_TO_CHANGED_FROM('<UpdatedDate>' || to_char(:OLD.UPDATED_DATE) || '</UpdatedDate>');
         APPEND_TO_CHANGED_TO('<UpdatedDate>' || to_char(:NEW.UPDATED_DATE) || '</UpdatedDate>');
      end if;

      if LENGTH(changed_from) > 0 then
         PROC_CREATE_AUDIT_TRAIL(8, 'UPDATE', :NEW.UPDATED_BY, to_char(:NEW.RF_ID), changed_from, changed_to);
      end if;
   elsif deleting then
      PROC_CREATE_AUDIT_TRAIL(8, 'DELETE', :OLD.UPDATED_BY, to_char(:OLD.RF_ID), changed_from, changed_to);
   end if;
end;
/