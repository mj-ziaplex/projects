create or replace trigger TGR_ACC_AUDIT_TRAIL
   after update or delete on ACCESSES for each row

declare
   changed_from	varchar2(1000);
   changed_to	varchar2(1000);

   procedure APPEND_TO_CHANGED_FROM(old_value varchar2) is
   begin
      if LENGTH(changed_from) = 0 then
         changed_from := '<?xml version="1.0" encoding="ISO-8859-1"?>';
      end if;
      changed_from := changed_from || old_value;
   end;

   procedure APPEND_TO_CHANGED_TO(new_value varchar2) is
   begin
      if LENGTH(changed_to) = 0 then
         changed_to := '<?xml version="1.0" encoding="ISO-8859-1"?>';
      end if;
      changed_to := changed_to || new_value;
   end;

begin
   if updating then
      if nvl(:OLD.ACC_ID, 0) != nvl(:NEW.ACC_ID, 0) then
         APPEND_TO_CHANGED_FROM('<AccessID>' || to_char(:OLD.ACC_ID) || '</AccessID>');
         APPEND_TO_CHANGED_TO('<AccessID>' || to_char(:NEW.ACC_ID) || '</AccessID>');
      end if;
      if nvl(:OLD.ACC_DESC, '') != nvl(:NEW.ACC_DESC, '') then
         APPEND_TO_CHANGED_FROM('<Description>' || :OLD.ACC_DESC || '</Description>');
         APPEND_TO_CHANGED_TO('<Description>' || :NEW.ACC_DESC || '</Description>');
      end if;
      if nvl(:OLD.CREATED_BY, '') != nvl(:NEW.CREATED_BY, '') then
         APPEND_TO_CHANGED_FROM('<CreatedBy>' || :OLD.CREATED_BY || '</CreatedBy>');
         APPEND_TO_CHANGED_TO('<CreatedBy>' || :NEW.CREATED_BY || '</CreatedBy>');
      end if;
      if nvl(:OLD.CREATED_DATE, '') != nvl(:NEW.CREATED_DATE, '') then
         APPEND_TO_CHANGED_FROM('<CreatedDate>' || to_char(:OLD.CREATED_DATE) || '</CreatedDate>');
         APPEND_TO_CHANGED_TO('<CreatedDate>' || to_char(:NEW.CREATED_DATE) || '</CreatedDate>');
      end if;
      if nvl(:OLD.UPDATED_BY, '') != nvl(:NEW.UPDATED_BY, '') then
         APPEND_TO_CHANGED_FROM('<UpdatedBy>' || :OLD.UPDATED_BY || '</UpdatedBy>');
         APPEND_TO_CHANGED_TO('<UpdatedBy>' || :NEW.UPDATED_BY || '</UpdatedBy>');
      end if;
      if nvl(:OLD.UPDATED_DATE, '') != nvl(:NEW.UPDATED_DATE, '') then
         APPEND_TO_CHANGED_FROM('<UpdatedDate>' || to_char(:OLD.UPDATED_DATE) || '</UpdatedDate>');
         APPEND_TO_CHANGED_TO('<UpdatedDate>' || to_char(:NEW.UPDATED_DATE) || '</UpdatedDate>');
      end if;

      if LENGTH(changed_from) > 0 then
         PROC_CREATE_AUDIT_TRAIL(40, 'UPDATE', :NEW.UPDATED_BY, to_char(:NEW.ACC_ID), changed_from, changed_to);
      end if;
   elsif deleting then
      PROC_CREATE_AUDIT_TRAIL(40, 'DELETE', :OLD.UPDATED_BY, to_char(:OLD.ACC_ID), changed_from, changed_to);
   end if;
end;
/