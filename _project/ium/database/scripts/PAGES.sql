CREATE TABLE pages 
(
  page_id 		NUMBER(2) NOT NULL
, page_desc 		VARCHAR2(25)
, created_by	VARCHAR2(10)
, created_date	TIMESTAMP(6)
, updated_by	VARCHAR2(10)
, updated_date	TIMESTAMP(6)
); 


-- Primary Key 

ALTER TABLE pages
 ADD CONSTRAINT pk_pages
 PRIMARY KEY (page_id)
 USING INDEX;

-- Primary Key Sequence

CREATE SEQUENCE seq_page
	START WITH 1
	INCREMENT BY 1
	MAXVALUE 99;

-- Public synonym
CREATE PUBLIC SYNONYM pages FOR pages;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON pages TO ium_app_user;
