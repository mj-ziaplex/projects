create or replace trigger TGR_ACTIVITY_LOG
   after insert or update on ASSESSMENT_REQUESTS for each row

declare

   V_ACT_DATETIME	date;

   function COMPUTE_ELAPSE_TIME(P_FROM_DATE date, P_TO_DATE date) return number is

      V_ELAPSE_TIME	NUMBER(6,2);
      V_HOLIDAYS	NUMBER(3);

   begin
      begin
         select count(*) into V_HOLIDAYS
            from HOLIDAYS
            where trunc(HOL_DATE) between trunc(P_FROM_DATE) and trunc(P_TO_DATE)
            and upper(to_char(HOL_DATE, 'DY')) not in('SAT', 'SUN');
      end;
      V_ELAPSE_TIME := P_TO_DATE - P_FROM_DATE 
                     - (round((P_TO_DATE - P_FROM_DATE) / 7) * 2)
                     - case when to_char(P_FROM_DATE, 'd') > to_char(P_FROM_DATE, 'd') then 2 else 0 end
                     - V_HOLIDAYS;
      return V_ELAPSE_TIME;
   end;

   procedure INSERT_ACTIVITY_LOG(P_USER_ID	varchar2
                                ,P_ELAPSE_TIME	number) is
   begin
      insert into ACTIVITY_LOGS
         (ACT_ID
         ,UAR_REFERENCE_NUM
         ,STAT_ID
         ,ACT_DATETIME
         ,USER_ID
         ,ACT_ASSIGNED_TO
         ,ACT_ELAPSE_TIME)
      values
         (SEQ_ACTIVITY_LOGS.NEXTVAL
         ,:NEW.REFERENCE_NUM
         ,:NEW.STATUS_ID
         ,SYSDATE
         ,P_USER_ID
         ,:NEW.ASSIGNED_TO
         ,P_ELAPSE_TIME);
   end;

begin
   if inserting then
      INSERT_ACTIVITY_LOG(:NEW.CREATED_BY, 0);
   elsif updating then
      if :OLD.STATUS_ID != :NEW.STATUS_ID then
         begin
            select max(ACT_DATETIME) into V_ACT_DATETIME
               from ACTIVITY_LOGS
               where UAR_REFERENCE_NUM = :OLD.REFERENCE_NUM;
         end;
         INSERT_ACTIVITY_LOG(:NEW.UPDATED_BY, COMPUTE_ELAPSE_TIME(V_ACT_DATETIME, sysdate));
      end if;
   end if;
end;
/