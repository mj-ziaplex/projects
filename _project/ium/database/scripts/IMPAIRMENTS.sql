CREATE TABLE impairments 
(
  imp_impairement_id	NUMBER(5) NOT NULL
, cl_client_num		VARCHAR2(10)
, uar_reference_num 	VARCHAR2(15)
, mib_impairment_code   VARCHAR2(6)
, imp_desc 		VARCHAR2(25)
, imp_relationship	VARCHAR2(1)
, imp_origin		VARCHAR2(1)
, imp_date 		DATE
, imp_height_feet 	NUMBER(4,2)
, imp_height_inches 	NUMBER(5,2)
, imp_weight 		NUMBER(5,2)
, imp_blood_pressure	VARCHAR2(7)
, imp_confirmation 	VARCHAR2(1)
, imp_export_date 	DATE
, created_by 		VARCHAR2(10) NOT NULL
, created_date 		TIMESTAMP
, updated_by 		VARCHAR2(10)
, updated_date 		TIMESTAMP
);

-- Primary Key 

ALTER TABLE impairments
 ADD CONSTRAINT pk_impairments
 PRIMARY KEY (imp_impairement_id)
 USING INDEX;

-- Primary Key Sequence

CREATE SEQUENCE seq_impairment
	START WITH 1
	INCREMENT BY 1
	NOMAXVALUE;

-- Foreign Key

ALTER TABLE impairments
 ADD CONSTRAINT fk_imp_clients
 FOREIGN KEY (cl_client_num)
 REFERENCES clients (client_id);

ALTER TABLE impairments
 ADD CONSTRAINT fk_imp_assessment_requests
 FOREIGN KEY (uar_reference_num)
 REFERENCES assessment_requests (reference_num);

ALTER TABLE impairments
 ADD CONSTRAINT fk_imp_mib_impairments
 FOREIGN KEY (mib_impairment_code)
 REFERENCES mib_impairments (mib_impairment_code);

ALTER TABLE impairments
 ADD CONSTRAINT fk_imp_mib_numbers
 FOREIGN KEY (imp_relationship)
 REFERENCES mib_numbers (mib_num_code);

ALTER TABLE impairments
 ADD CONSTRAINT fk_imp_mib_letters
 FOREIGN KEY (imp_origin)
 REFERENCES mib_letters (mib_lttr_code);

ALTER TABLE impairments
 ADD CONSTRAINT fk_imp_users_created_by
 FOREIGN KEY (created_by)
 REFERENCES users (user_id);

ALTER TABLE impairments
 ADD CONSTRAINT fk_imp_users_updated_by
 FOREIGN KEY (updated_by)
 REFERENCES users (user_id);

-- Public synonym
CREATE PUBLIC SYNONYM impairments FOR impairments;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON impairments TO ium_app_user;
