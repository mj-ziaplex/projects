CREATE TABLE notification_recipients 
(
  not_id 		NUMBER(5) NOT NULL
, role_id 		VARCHAR2(15) NOT NULL
, lob_code      VARCHAR2(2) NOT NULL
, notification_type 	VARCHAR2(1)
, not_primary_ind		VARCHAR2(1)
, created_by	VARCHAR2(10)
, created_date	TIMESTAMP(6)
, updated_by	VARCHAR2(10)
, updated_date	TIMESTAMP(6)
);

-- Primary Key 

ALTER TABLE notification_recipients
 ADD CONSTRAINT pk_notification_recipients
 PRIMARY KEY (not_id, role_id, lob_code)
 USING INDEX;

-- Foreign Key

ALTER TABLE notification_recipients
 ADD CONSTRAINT fk_not_notification_templates
 FOREIGN KEY (not_id)
 REFERENCES notification_templates (not_id);

ALTER TABLE notification_recipients
 ADD CONSTRAINT fk_not_role
 FOREIGN KEY (role_id)
 REFERENCES roles (role_code);
 
 ALTER TABLE notification_recipients
 ADD CONSTRAINT fk_not_lines_of_business
 FOREIGN KEY (lob_code)
 REFERENCES lines_of_business (lob_code);

-- Public synonym
CREATE PUBLIC SYNONYM notification_recipients FOR notification_recipients;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON notification_recipients TO ium_app_user;
