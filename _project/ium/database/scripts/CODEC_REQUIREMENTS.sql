CREATE TABLE CODEC_REQUIREMENTS
(
  PC_SEQ		NUMBER(4) NOT NULL
, REQT_CODE		VARCHAR2(5) NOT NULL
);


ALTER TABLE codec_requirements
 ADD CONSTRAINT pk_codec_requirements
 PRIMARY KEY (pc_seq, reqt_code)
 USING INDEX;
 
ALTER TABLE codec_requirements
 ADD CONSTRAINT fk_cr_prism 
 FOREIGN KEY (pc_seq)
 REFERENCES prism_codec (pc_seq);

ALTER TABLE codec_requirements
 ADD CONSTRAINT fk_cr_reqt
 FOREIGN KEY (reqt_code)
 REFERENCES requirements (reqt_code);

-- Public synonym
CREATE PUBLIC SYNONYM codec_requirements FOR codec_requirements;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON codec_requirements TO ium_app_user;

