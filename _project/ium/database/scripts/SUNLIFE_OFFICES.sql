CREATE TABLE sunlife_offices
(
  slo_office_code	VARCHAR2(15) NOT NULL
, slo_office_name 	VARCHAR2(50)
, slo_type 		VARCHAR2(1)
, slo_address_line1 	VARCHAR2(60)
, slo_address_line2 	VARCHAR2(60)
, slo_address_line3 	VARCHAR2(60)
, slo_city 		VARCHAR2(25)
, slo_province 		VARCHAR2(25)
, slo_country 		VARCHAR2(25)
, slo_zip_code 		NUMBER(6)
, slo_contact_num 	VARCHAR2(13)
, slo_fax_num 		VARCHAR2(13)
, created_by	VARCHAR2(10)
, created_date	TIMESTAMP(6)
, updated_by	VARCHAR2(10)
, updated_date	TIMESTAMP(6)
); 

-- Primary Key 

ALTER TABLE sunlife_offices
 ADD CONSTRAINT pk_sunlife_offices
 PRIMARY KEY (slo_office_code)
 USING INDEX;
 
-- Public synonym
CREATE PUBLIC SYNONYM sunlife_offices FOR sunlife_offices;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON sunlife_offices TO ium_app_user;
 