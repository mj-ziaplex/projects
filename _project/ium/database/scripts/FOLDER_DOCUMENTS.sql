CREATE TABLE folder_documents 
(
  doc_id		NUMBER(8) NOT NULL
, uar_reference_num 	VARCHAR2(15)
, doc_code 		VARCHAR2(3)
, doc_date_rcvd 	DATE
, doc_received_by 	VARCHAR2(10)
, doc_reference_num 	NUMBER(8)
); 

-- Primary Key 

ALTER TABLE folder_documents
 ADD CONSTRAINT pk_folder_documents
 PRIMARY KEY (doc_id)
 USING INDEX;

-- Primary Key Sequence

CREATE SEQUENCE seq_folder_document
	START WITH 1
	INCREMENT BY 1
	NOMAXVALUE;

-- Foreign Key

ALTER TABLE folder_documents
 ADD CONSTRAINT fk_doc_assessment_requests
 FOREIGN KEY (uar_reference_num)
 REFERENCES assessment_requests (reference_num);

ALTER TABLE folder_documents
 ADD CONSTRAINT fk_doc_document_types
 FOREIGN KEY (doc_code)
 REFERENCES document_types (doc_code);

ALTER TABLE folder_documents
 ADD CONSTRAINT fk_doc_users
 FOREIGN KEY (doc_received_by)
 REFERENCES users (user_id);
 
-- Public synonym
CREATE PUBLIC SYNONYM folder_documents FOR folder_documents;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON folder_documents TO ium_app_user;
 