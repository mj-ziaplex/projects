CREATE TABLE audit_trails
(
  tran_num 			NUMBER(10) NOT NULL
, tran_record 		NUMBER(2)
, tran_type 		VARCHAR2(6)
, tran_datetime 	TIMESTAMP(6)
, user_id 			VARCHAR2(10)
, uar_reference_num VARCHAR2(30)
, tran_changed_from VARCHAR2(1000)
, tran_changed_to 	VARCHAR2(1000)
);

-- Primary Key 
ALTER TABLE audit_trails
 ADD CONSTRAINT pk_audit_trails
 PRIMARY KEY (tran_num)
 USING INDEX;

-- Public synonym
CREATE PUBLIC SYNONYM audit_trails FOR audit_trails;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON audit_trails TO ium_app_user;

-- Primary Key Sequence
CREATE SEQUENCE seq_audit_trail
	START WITH 1
	INCREMENT BY 1
	MAXVALUE 9999999999;
