CREATE TABLE access_templates
(
  templt_id 		NUMBER(5)
, role_id    		VARCHAR2(15)
, templt_desc 		VARCHAR2(25)
, created_by		VARCHAR2(10)
, created_date		TIMESTAMP(6)
, updated_by		VARCHAR2(10)
, updated_date		TIMESTAMP(6)
);

-- Primary Key
ALTER TABLE access_templates
 ADD CONSTRAINT pk_access_template 
 PRIMARY KEY (templt_id)
 USING INDEX;
 
-- Foreign Key
ALTER TABLE access_templates
 ADD CONSTRAINT fk_atp_roles 
 FOREIGN KEY (role_id)
 REFERENCES roles (role_code)
 ON DELETE SET NULL;

-- Public synonym
CREATE PUBLIC SYNONYM access_templates FOR access_templates;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON access_templates TO ium_app_user;
 
-- Primary Key Sequence
CREATE SEQUENCE seq_access_template
	INCREMENT BY 1
	START WITH 1
	MAXVALUE 99999;
