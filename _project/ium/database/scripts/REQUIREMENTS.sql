CREATE TABLE requirements 
(
  reqt_code 		VARCHAR2(5) NOT NULL
, reqt_desc 		VARCHAR2(70)
, reqt_level 		VARCHAR2(1)
, reqt_validity 	NUMBER(3)
, reqt_form_ind 	VARCHAR2(1)
, reqt_form_id 		NUMBER(5)
, reqt_follow_up_num	NUMBER(2)
, created_by	VARCHAR2(10)
, created_date	TIMESTAMP(6)
, updated_by	VARCHAR2(10)
, updated_date	TIMESTAMP(6)
); 

-- Primary Key 

ALTER TABLE requirements
 ADD CONSTRAINT pk_requirements
 PRIMARY KEY (reqt_code)
 USING INDEX;

-- Foreign Key

ALTER TABLE requirements
 ADD CONSTRAINT fk_reqt_requirement_forms
 FOREIGN KEY (reqt_form_id)
 REFERENCES requirement_forms (rf_id);
 
-- Public synonym
CREATE PUBLIC SYNONYM requirements FOR requirements;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON requirements TO ium_app_user;
 