CREATE TABLE user_roles
(  role_code                      VARCHAR2(15) NOT NULL
 , user_id                        VARCHAR2(10) NOT NULL
);

-- Constraints for USER_ROLES

ALTER TABLE user_roles
 ADD CONSTRAINT pk_user_roles
 PRIMARY KEY (role_code, user_id)
 USING INDEX;

-- Foreign Key
ALTER TABLE user_roles
 ADD CONSTRAINT fk_ur_roles
 FOREIGN KEY (role_code)
 REFERENCES roles (role_code);

ALTER TABLE user_roles
 ADD CONSTRAINT fk_users
 FOREIGN KEY (user_id)
 REFERENCES users (user_id);

-- Public synonym
CREATE PUBLIC SYNONYM user_roles FOR user_roles;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON user_roles TO ium_app_user;
