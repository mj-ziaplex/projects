CREATE TABLE job_schedules
(
  sched_type	VARCHAR2(2) NOT NULL
, sched_desc     VARCHAR2(100)
, sched_time     TIMESTAMP
, created_by	VARCHAR2(10)
, created_date	TIMESTAMP(6)
, updated_by	VARCHAR2(10)
, updated_date	TIMESTAMP(6)
);

-- Primary Key

ALTER TABLE job_schedules
 ADD CONSTRAINT pk_job_schedules
 PRIMARY KEY (sched_type);

-- Check Constraint
ALTER TABLE job_schedules
 ADD CONSTRAINT chk_sched_type
 CHECK (sched_type IN ('ME', 'AE'));

-- Public synonym
CREATE PUBLIC SYNONYM job_schedules FOR job_schedules;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON job_schedules TO ium_app_user;
