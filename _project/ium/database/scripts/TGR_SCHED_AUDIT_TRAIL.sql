create or replace trigger TGR_SCHED_AUDIT_TRAIL
   after update on JOB_SCHEDULES for each row

declare
   changed_from	varchar2(1000);
   changed_to	varchar2(1000);

   procedure APPEND_TO_CHANGED_FROM(old_value varchar2) is
   begin
      if LENGTH(changed_from) = 0 then
         changed_from := '<?xml version="1.0" encoding="ISO-8859-1"?>';
      end if;
      changed_from := changed_from || old_value;
   end;

   procedure APPEND_TO_CHANGED_TO(new_value varchar2) is
   begin
      if LENGTH(changed_to) = 0 then
         changed_to := '<?xml version="1.0" encoding="ISO-8859-1"?>';
      end if;
      changed_to := changed_to || new_value;
   end;

begin
      if nvl(:OLD.SCHED_TYPE, '') != nvl(:NEW.SCHED_TYPE, '') then
         APPEND_TO_CHANGED_FROM('<Type>' || :OLD.SCHED_TYPE || '</Type>');
         APPEND_TO_CHANGED_TO('<Type>' || :NEW.SCHED_TYPE || '</Type>');
      end if;
      if nvl(:OLD.SCHED_DESC, '') != nvl(:NEW.SCHED_DESC, '') then
         APPEND_TO_CHANGED_FROM('<Description>' || :OLD.SCHED_DESC || '</Description>');
         APPEND_TO_CHANGED_TO('<Description>' || :NEW.SCHED_DESC || '</Description>');
      end if;
      if nvl(:OLD.SCHED_TIME, '') != nvl(:NEW.SCHED_TIME, '') then
         APPEND_TO_CHANGED_FROM('<Time>' || to_char(:OLD.SCHED_TIME) || '</Time>');
         APPEND_TO_CHANGED_TO('<Time>' || to_char(:NEW.SCHED_TIME) || '</Time>');
      end if;
      if nvl(:OLD.CREATED_BY, '') != nvl(:NEW.CREATED_BY, '') then
         APPEND_TO_CHANGED_FROM('<CreatedBy>' || :OLD.CREATED_BY || '</CreatedBy>');
         APPEND_TO_CHANGED_TO('<CreatedBy>' || :NEW.CREATED_BY || '</CreatedBy>');
      end if;
      if nvl(:OLD.CREATED_DATE, '') != nvl(:NEW.CREATED_DATE, '') then
         APPEND_TO_CHANGED_FROM('<CreatedDate>' || to_char(:OLD.CREATED_DATE) || '</CreatedDate>');
         APPEND_TO_CHANGED_TO('<CreatedDate>' || to_char(:NEW.CREATED_DATE) || '</CreatedDate>');
      end if;
      if nvl(:OLD.UPDATED_BY, '') != nvl(:NEW.UPDATED_BY, '') then
         APPEND_TO_CHANGED_FROM('<UpdatedBy>' || :OLD.UPDATED_BY || '</UpdatedBy>');
         APPEND_TO_CHANGED_TO('<UpdatedBy>' || :NEW.UPDATED_BY || '</UpdatedBy>');
      end if;
      if nvl(:OLD.UPDATED_DATE, '') != nvl(:NEW.UPDATED_DATE, '') then
         APPEND_TO_CHANGED_FROM('<UpdatedDate>' || to_char(:OLD.UPDATED_DATE) || '</UpdatedDate>');
         APPEND_TO_CHANGED_TO('<UpdatedDate>' || to_char(:NEW.UPDATED_DATE) || '</UpdatedDate>');
      end if;

      if LENGTH(changed_from) > 0 then
         if :OLD.SCHED_TYPE = 'ME' then
            PROC_CREATE_AUDIT_TRAIL(37, 'UPDATE', :NEW.UPDATED_BY, :NEW.SCHED_TYPE, changed_from, changed_to);
         elsif :OLD.SCHED_TYPE = 'AE' then
            PROC_CREATE_AUDIT_TRAIL(38, 'UPDATE', :NEW.UPDATED_BY, :NEW.SCHED_TYPE, changed_from, changed_to);
         end if;
      end if;
end;
/