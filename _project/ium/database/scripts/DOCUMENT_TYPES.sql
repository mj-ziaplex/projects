CREATE TABLE document_types 
(
  doc_code 		VARCHAR2(3) NOT NULL
, doc_desc 		VARCHAR2(40)
, created_by	VARCHAR2(10)
, created_date	TIMESTAMP(6)
, updated_by	VARCHAR2(10)
, updated_date	TIMESTAMP(6)
);

-- Primary Key 

ALTER TABLE document_types
 ADD PRIMARY KEY (doc_code)
 USING INDEX;

-- Public synonym
CREATE PUBLIC SYNONYM document_types FOR document_types;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON document_types TO ium_app_user;
