create or replace trigger TGR_PC_AUDIT_TRAIL
   after update or delete on PROCESS_CONFIGURATIONS for each row

declare
   changed_from	varchar2(1000);
   changed_to	varchar2(1000);

   procedure APPEND_TO_CHANGED_FROM(old_value varchar2) is
   begin
      if LENGTH(changed_from) = 0 then
         changed_from := '<?xml version="1.0" encoding="ISO-8859-1"?>';
      end if;
      changed_from := changed_from || old_value;
   end;

   procedure APPEND_TO_CHANGED_TO(new_value varchar2) is
   begin
      if LENGTH(changed_to) = 0 then
         changed_to := '<?xml version="1.0" encoding="ISO-8859-1"?>';
      end if;
      changed_to := changed_to || new_value;
   end;

begin
   if updating then
      if nvl(:OLD.EVNT_ID, 0) != nvl(:NEW.EVNT_ID, 0) then
         APPEND_TO_CHANGED_FROM('<EventID>' || to_char(:OLD.EVNT_ID) || '</EventID>');
         APPEND_TO_CHANGED_TO('<EventID>' || to_char(:NEW.EVNT_ID) || '</EventID>');
      end if;
      if nvl(:OLD.PROC_CODE, '') != nvl(:NEW.PROC_CODE, '') then
         APPEND_TO_CHANGED_FROM('<ProcessCode>' || :OLD.PROC_CODE || '</ProcessCode>');
         APPEND_TO_CHANGED_TO('<ProcessCode>' || :NEW.PROC_CODE || '</ProcessCode>');
      end if;
      if nvl(:OLD.LOB_CODE, '') != nvl(:NEW.LOB_CODE, '') then
         APPEND_TO_CHANGED_FROM('<LOB>' || :OLD.LOB_CODE || '</LOB>');
         APPEND_TO_CHANGED_TO('<LOB>' || :NEW.LOB_CODE || '</LOB>');
      end if;
      if nvl(:OLD.EVNT_NAME, '') != nvl(:NEW.EVNT_NAME, '') then
         APPEND_TO_CHANGED_FROM('<EventName>' || :OLD.EVNT_NAME || '</EventName>');
         APPEND_TO_CHANGED_TO('<EventName>' || :NEW.EVNT_NAME || '</EventName>');
      end if;
      if nvl(:OLD.EVNT_FROM_STATUS, 0) != nvl(:NEW.EVNT_FROM_STATUS, 0) then
         APPEND_TO_CHANGED_FROM('<FromEvent>' || to_char(:OLD.EVNT_FROM_STATUS) || '</FromEvent>');
         APPEND_TO_CHANGED_TO('<FromEvent>' || to_char(:NEW.EVNT_FROM_STATUS) || '</FromEvent>');
      end if;
      if nvl(:OLD.EVNT_TO_STATUS, 0) != nvl(:NEW.EVNT_TO_STATUS, 0) then
         APPEND_TO_CHANGED_FROM('<ToEvent>' || to_char(:OLD.EVNT_TO_STATUS) || '</ToEvent>');
         APPEND_TO_CHANGED_TO('<ToEvent>' || to_char(:NEW.EVNT_TO_STATUS) || '</ToEvent>');
      end if;
      if nvl(:OLD.CREATED_BY, '') != nvl(:NEW.CREATED_BY, '') then
         APPEND_TO_CHANGED_FROM('<CreatedBy>' || :OLD.CREATED_BY || '</CreatedBy>');
         APPEND_TO_CHANGED_TO('<CreatedBy>' || :NEW.CREATED_BY || '</CreatedBy>');
      end if;
      if nvl(:OLD.CREATED_DATE, '') != nvl(:NEW.CREATED_DATE, '') then
         APPEND_TO_CHANGED_FROM('<CreatedDate>' || to_char(:OLD.CREATED_DATE) || '</CreatedDate>');
         APPEND_TO_CHANGED_TO('<CreatedDate>' || to_char(:NEW.CREATED_DATE) || '</CreatedDate>');
      end if;
      if nvl(:OLD.UPDATED_BY, '') != nvl(:NEW.UPDATED_BY, '') then
         APPEND_TO_CHANGED_FROM('<UpdatedBy>' || :OLD.UPDATED_BY || '</UpdatedBy>');
         APPEND_TO_CHANGED_TO('<UpdatedBy>' || :NEW.UPDATED_BY || '</UpdatedBy>');
      end if;
      if nvl(:OLD.UPDATED_DATE, '') != nvl(:NEW.UPDATED_DATE, '') then
         APPEND_TO_CHANGED_FROM('<UpdatedDate>' || to_char(:OLD.UPDATED_DATE) || '</UpdatedDate>');
         APPEND_TO_CHANGED_TO('<UpdatedDate>' || to_char(:NEW.UPDATED_DATE) || '</UpdatedDate>');
      end if;

      if LENGTH(changed_from) > 0 then
         PROC_CREATE_AUDIT_TRAIL(26, 'UPDATE', :NEW.UPDATED_BY, to_char(:NEW.EVNT_ID), changed_from, changed_to);
      end if;
   elsif deleting then
      PROC_CREATE_AUDIT_TRAIL(26, 'DELETE', :OLD.UPDATED_BY, to_char(:OLD.EVNT_ID), changed_from, changed_to);
   end if;
end;
/