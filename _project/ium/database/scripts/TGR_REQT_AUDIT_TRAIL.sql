create or replace trigger TGR_REQT_AUDIT_TRAIL
   after update or delete on REQUIREMENTS for each row

declare
   changed_from	varchar2(1000);
   changed_to	varchar2(1000);

   procedure APPEND_TO_CHANGED_FROM(old_value varchar2) is
   begin
      if LENGTH(changed_from) = 0 then
         changed_from := '<?xml version="1.0" encoding="ISO-8859-1"?>';
      end if;
      changed_from := changed_from || old_value;
   end;

   procedure APPEND_TO_CHANGED_TO(new_value varchar2) is
   begin
      if LENGTH(changed_to) = 0 then
         changed_to := '<?xml version="1.0" encoding="ISO-8859-1"?>';
      end if;
      changed_to := changed_to || new_value;
   end;

begin
   if updating then
      if nvl(:OLD.REQT_CODE, '') != nvl(:NEW.REQT_CODE, '') then
         APPEND_TO_CHANGED_FROM('<Code>' || :OLD.REQT_CODE || '</Code>');
         APPEND_TO_CHANGED_TO('<Code>' || :NEW.REQT_CODE || '</Code>');
      end if;
      if nvl(:OLD.REQT_LEVEL, '') != nvl(:NEW.REQT_LEVEL, '') then
         APPEND_TO_CHANGED_FROM('<Level>' || :OLD.REQT_LEVEL || '</Level>');
         APPEND_TO_CHANGED_TO('<Level>' || :NEW.REQT_LEVEL || '</Level>');
      end if;
      if nvl(:OLD.REQT_VALIDITY, 0) != nvl(:NEW.REQT_VALIDITY, 0) then
         APPEND_TO_CHANGED_FROM('<Validity>' || to_char(:OLD.REQT_VALIDITY) || '</Validity>');
         APPEND_TO_CHANGED_TO('<Validity>' || to_char(:NEW.REQT_VALIDITY) || '</Validity>');
      end if;
      if nvl(:OLD.REQT_FOLLOW_UP_NUM, 0) != nvl(:NEW.REQT_FOLLOW_UP_NUM, 0) then
         APPEND_TO_CHANGED_FROM('<FollowUpNumber>' || to_char(:OLD.REQT_FOLLOW_UP_NUM) || '</FollowUpNumber>');
         APPEND_TO_CHANGED_TO('<FollowUpNumber>' || to_char(:NEW.REQT_FOLLOW_UP_NUM) || '</FollowUpNumber>');
      end if;
      if nvl(:OLD.CREATED_BY, '') != nvl(:NEW.CREATED_BY, '') then
         APPEND_TO_CHANGED_FROM('<CreatedBy>' || :OLD.CREATED_BY || '</CreatedBy>');
         APPEND_TO_CHANGED_TO('<CreatedBy>' || :NEW.CREATED_BY || '</CreatedBy>');
      end if;
      if nvl(:OLD.CREATED_DATE, '') != nvl(:NEW.CREATED_DATE, '') then
         APPEND_TO_CHANGED_FROM('<CreatedDate>' || to_char(:OLD.CREATED_DATE) || '</CreatedDate>');
         APPEND_TO_CHANGED_TO('<CreatedDate>' || to_char(:NEW.CREATED_DATE) || '</CreatedDate>');
      end if;
      if nvl(:OLD.UPDATED_BY, '') != nvl(:NEW.UPDATED_BY, '') then
         APPEND_TO_CHANGED_FROM('<UpdatedBy>' || :OLD.UPDATED_BY || '</UpdatedBy>');
         APPEND_TO_CHANGED_TO('<UpdatedBy>' || :NEW.UPDATED_BY || '</UpdatedBy>');
      end if;
      if nvl(:OLD.UPDATED_DATE, '') != nvl(:NEW.UPDATED_DATE, '') then
         APPEND_TO_CHANGED_FROM('<UpdatedDate>' || to_char(:OLD.UPDATED_DATE) || '</UpdatedDate>');
         APPEND_TO_CHANGED_TO('<UpdatedDate>' || to_char(:NEW.UPDATED_DATE) || '</UpdatedDate>');
      end if;

      if LENGTH(changed_from) > 0 then
         PROC_CREATE_AUDIT_TRAIL(7, 'UPDATE', :NEW.UPDATED_BY, :NEW.REQT_CODE, changed_from, changed_to);
      end if;
   elsif deleting then
      PROC_CREATE_AUDIT_TRAIL(7, 'DELETE', :OLD.UPDATED_BY, :OLD.REQT_CODE, changed_from, changed_to);
   end if;
end;
/