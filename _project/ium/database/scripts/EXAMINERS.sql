CREATE TABLE examiners
(
  exmnr_id 			NUMBER(5) NOT NULL
, exmnr_last_name 		VARCHAR2(40)
, exmnr_given_name 		VARCHAR2(25)
, exmnr_middle_name 		VARCHAR2(25)
, exmnr_salutation 		VARCHAR2(25)
, exmnr_sex 			VARCHAR2(1)
, exmnr_rank_id 		NUMBER(3)
, exmnr_rank_eff_date 		DATE
, exmnr_dob 			DATE
, exmnr_contact_num 		VARCHAR2(13)
, exmnr_fax_num 		VARCHAR2(13)
, exmnr_bus_addr_line1 		VARCHAR2(60)
, exmnr_bus_addr_line2 		VARCHAR2(60)
, exmnr_bus_addr_line3 		VARCHAR2(60)
, exmnr_bus_city 		VARCHAR2(25)
, exmnr_province 		VARCHAR2(25)
, exmnr_country 		VARCHAR2(25)
, exmnr_zipcode 		NUMERIC(5)
, exmnr_mail_to_bus_ind 	VARCHAR2(1)
, exmnr_clinic_hrs_from 	TIMESTAMP
, exmnr_clinic_hrs_to 		TIMESTAMP
, exmnr_oth_ins_co 		VARCHAR2(60)
, exmnr_pre_emp_srvce_ind	VARCHAR2(1)
, exmnr_tin 			NUMBER(16)
, exmnr_accreditation_ind 	VARCHAR2(1)
, exmnr_accredit_date 		DATE
, exmnr_area 			VARCHAR2(5)
, exmnr_place 			NUMBER(3)
, exmnr_mobile_num		VARCHAR2(13)
, created_by 			VARCHAR2(10) NOT NULL
, created_date 			TIMESTAMP
, updated_by 			VARCHAR2(10)
, updated_date 			TIMESTAMP
);	

-- Primary Key 

ALTER TABLE examiners
 ADD CONSTRAINT pk_examiners
 PRIMARY KEY (exmnr_id)
 USING INDEX;

-- Primary Key Sequence

CREATE SEQUENCE seq_examiner
	START WITH 1
	INCREMENT BY 1
	NOMAXVALUE;

-- Foreign Keys

ALTER TABLE examiners
 ADD CONSTRAINT fk_exm_ranks
 FOREIGN KEY (exmnr_rank_id)
 REFERENCES ranks (rank_id);

ALTER TABLE examiners
 ADD CONSTRAINT fk_exm_examination_places
 FOREIGN KEY (exmnr_place)
 REFERENCES examination_places (ep_id);

ALTER TABLE examiners
 ADD CONSTRAINT fk_exm_users_created_by
 FOREIGN KEY (created_by)
 REFERENCES users (user_id);

ALTER TABLE examiners
 ADD CONSTRAINT fk_exm_users_updated_by
 FOREIGN KEY (updated_by)
 REFERENCES users (user_id);

-- Public synonym
CREATE PUBLIC SYNONYM examiners FOR examiners;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON examiners TO ium_app_user;
