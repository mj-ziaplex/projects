CREATE TABLE policy_coverage_details 
(
  pcd_policy_num 	VARCHAR2(15)
, uar_reference_num 	VARCHAR2(10) 
, pcd_client_num 	VARCHAR2(10)
, pcd_coverage_num	NUMBER(2)
, pcd_pol_relationship 	VARCHAR2(1)
, pcd_issue_date 	DATE
, pcd_coverage_status 	VARCHAR2(1)
, pcd_smoker_code 	VARCHAR2(1)
, pcd_plan_code 	VARCHAR2(5)
, pcd_medical_ind 	VARCHAR2(1)
, pcd_face_amount 	NUMBER(12)
, pcd_decision 		VARCHAR2(2)
, pcd_adb_face_amount 	NUMBER(12)
, pcd_ad_multiplier 	NUMBER(3)
, pcd_wp_multiplier 	NUMBER(3)
, pcd_reinsured_amount 	NUMBER(12)
);

-- Primary Key 

ALTER TABLE policy_coverage_details
 ADD CONSTRAINT pk_policy_coverage_details
 PRIMARY KEY (pcd_policy_num)
 USING INDEX;

-- Foreign Key

ALTER TABLE policy_coverage_details
 ADD CONSTRAINT fk_pcd_assessment_requests
 FOREIGN KEY (uar_reference_num)
 REFERENCES assessment_requests (reference_num);

ALTER TABLE policy_coverage_details
 ADD CONSTRAINT fk_pcd_clients
 FOREIGN KEY (pcd_client_num)
 REFERENCES clients (client_id);

-- Public synonym
CREATE PUBLIC SYNONYM policy_coverage_details FOR policy_coverage_details;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON policy_coverage_details TO ium_app_user;
