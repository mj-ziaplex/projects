create or replace trigger TGR_PAGE_ACCESS
   after update on PAGES_ACCESS for each row

declare

   cursor CURSOR_ACCESS_TEMPLATES is
      SELECT TEMPLT_ID FROM ACCESS_TEMPLATES;

   cursor CURSOR_USER_PAGE_ACCESS is
      SELECT USER_ID FROM USER_PAGE_ACCESS;
   
   procedure INSERT_ACCESS_TEMPLATE_DETAILS is

      V_TEMPLT_ID   NUMBER(5);

   begin
      open CURSOR_ACCESS_TEMPLATES;
      loop
         fetch CURSOR_ACCESS_TEMPLATES into V_TEMPLT_ID;
         exit when CURSOR_ACCESS_TEMPLATES%notfound;
         begin
            INSERT INTO ACCESS_TEMPLATE_DETAILS
               (TEMPLT_ID, PA_ID, ATD_ACCESS_IND)
               VALUES(V_TEMPLT_ID, :NEW.PA_ID, '0');
         exception
            when others then null;
         end;
      end loop;
      close CURSOR_ACCESS_TEMPLATES;
   end;

   procedure DELETE_ACCESS_TEMPLATE_DETAILS is
   begin
      DELETE ACCESS_TEMPLATE_DETAILS WHERE PA_ID = :NEW.PA_ID;
   end;

   procedure INSERT_USER_PAGE_ACCESS is

      V_USER_ID   VARCHAR2(10);

   begin
      open CURSOR_USER_PAGE_ACCESS;
      loop
         fetch CURSOR_USER_PAGE_ACCESS into V_USER_ID;
         exit when CURSOR_USER_PAGE_ACCESS%notfound;
         begin
            INSERT INTO USER_PAGE_ACCESS
               (USER_ID, PAGE_ID, ACC_ID, UPA_ACCESS_IND, CREATED_BY, CREATED_DATE)
               VALUES (V_USER_ID, :NEW.PAGE_ID, :NEW.ACC_ID, '0', V_USER_ID, SYSDATE);
         exception
            when others then null;
         end;
      end loop;
      close CURSOR_USER_PAGE_ACCESS;
   end;

   procedure DELETE_USER_PAGE_ACCESS is
   begin
      DELETE USER_PAGE_ACCESS WHERE PAGE_ID = :NEW.PAGE_ID AND ACC_ID = :NEW.ACC_ID;
   end;
   
begin
   if updating then
      if nvl(:OLD.PA_ENABLED_IND, '0') != nvl(:NEW.PA_ENABLED_IND, '0') then
         if :NEW.PA_ENABLED_IND = '1' then
            INSERT_ACCESS_TEMPLATE_DETAILS();
            INSERT_USER_PAGE_ACCESS();
         else
            DELETE_ACCESS_TEMPLATE_DETAILS();
            DELETE_USER_PAGE_ACCESS();
         end if;
      end if;
   end if;
end;
/