CREATE TABLE examiner_specializations 
(
  exmnr_id 		NUMBER(5) NOT NULL
, spl_id 		NUMBER(3) NOT NULL
, spl_effective_date 	DATE
, created_by        VARCHAR2(10)
, created_date      TIMESTAMP(6)
, updated_by        VARCHAR2(10)
, updated_date      TIMESTAMP(6)
);

-- Primary Key 

ALTER TABLE examiner_specializations
 ADD CONSTRAINT pk_examiner_specializations
 PRIMARY KEY (exmnr_id, spl_id)
 USING INDEX;

-- Foreign Key

ALTER TABLE examiner_specializations
 ADD CONSTRAINT fk_esp_examiners
 FOREIGN KEY (exmnr_id)
 REFERENCES examiners (exmnr_id);

ALTER TABLE examiner_specializations
 ADD CONSTRAINT fk_esp_specializations
 FOREIGN KEY (spl_id)
 REFERENCES specializations (spl_id);
 
-- Public synonym
CREATE PUBLIC SYNONYM examiner_specializations FOR examiner_specializations;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON examiner_specializations TO ium_app_user;
 