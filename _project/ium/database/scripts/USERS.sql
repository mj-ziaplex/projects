CREATE TABLE users 
(
  user_id 		VARCHAR2(10)
, usr_acf2id		VARCHAR2(8)
, usr_password		VARCHAR2(16)
, usr_last_name		VARCHAR2(40)
, usr_first_name	VARCHAR2(25)
, usr_middle_name	VARCHAR2(25)
, usr_code 		VARCHAR2(10)
, usr_type 		VARCHAR2(1)
, usr_email_address 	VARCHAR2(40)
, usr_mobile_num 	VARCHAR2(13)
, usr_contact_num VARCHAR2(15)
, usr_address1  VARCHAR2(60)
, usr_address2  VARCHAR2(60)
, usr_address3  VARCHAR2(60)
, usr_country   VARCHAR2(20)
, usr_city      VARCHAR2(20)
, usr_zipcode   VARCHAR2(5)
, usr_lock_ind  VARCHAR2(1)
, usr_active_ind VARCHAR2(1)
, usr_notification_ind 	VARCHAR2(1)
, usr_records_per_view 	NUMBER(2)
, usr_sex 		VARCHAR2(1)
, usr_role 		VARCHAR2(15)
, slo_id 		VARCHAR2(15)
, dept_id 		VARCHAR2(10)
, sec_id 		VARCHAR2(15)
, created_by 		VARCHAR2(10) NOT NULL
, created_date 		TIMESTAMP
, updated_by 		VARCHAR2(10)
, updated_date 		TIMESTAMP
);



-- Primary Key 

ALTER TABLE users
 ADD CONSTRAINT pk_users
 PRIMARY KEY (user_id)
 USING INDEX;

-- Foreign Key

ALTER TABLE users
 ADD CONSTRAINT fk_usr_sunlife_offices
 FOREIGN KEY (slo_id)
 REFERENCES sunlife_offices (slo_office_code);

ALTER TABLE users
 ADD CONSTRAINT fk_usr_users_created_by
 FOREIGN KEY (created_by)
 REFERENCES users (user_id);

ALTER TABLE users
 ADD CONSTRAINT fk_usr_users_updated_by
 FOREIGN KEY (updated_by)
 REFERENCES users (user_id);

-- Public synonym
CREATE PUBLIC SYNONYM users FOR users;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON users TO ium_app_user;
