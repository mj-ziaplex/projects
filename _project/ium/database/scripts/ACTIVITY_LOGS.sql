CREATE TABLE activity_logs 
(
  act_id			NUMBER(10)
, uar_reference_num	VARCHAR2(15) 
, stat_id 			NUMBER(4)
, act_datetime 		TIMESTAMP(6)
, user_id 			VARCHAR2(10)
, act_assigned_to 	VARCHAR2(10)
, act_elapse_time 	NUMBER(6,2)
); 

-- Primary Key 
ALTER TABLE activity_logs
 ADD CONSTRAINT pk_activity_logs
 PRIMARY KEY (act_id)
 USING INDEX;

-- Foreign Key
ALTER TABLE activity_logs
 ADD CONSTRAINT fk_act_users
 FOREIGN KEY (user_id)
 REFERENCES users (user_id)
 ON DELETE SET NULL;

ALTER TABLE activity_logs
 ADD CONSTRAINT fk_act_status
 FOREIGN KEY (stat_id)
 REFERENCES status (stat_id)
 ON DELETE SET NULL;

ALTER TABLE activity_logs
 ADD CONSTRAINT fk_act_assessment_requests
 FOREIGN KEY (uar_reference_num)
 REFERENCES assessment_requests (reference_num)
 ON DELETE SET NULL;

-- Public synonym
CREATE PUBLIC SYNONYM activity_logs FOR activity_logs;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON activity_logs TO ium_app_user;

-- Primary Key Sequence
CREATE SEQUENCE seq_activity_logs
 START WITH 0
 INCREMENT BY 1
 MAXVALUE 9999999999;
