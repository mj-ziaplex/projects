CREATE TABLE pages_access
(
  PA_ID		NUMBER(5) NOT NULL
, PAGE_ID	NUMBER(2)
, ACC_ID		NUMBER(2)
, PA_ACCESS_IND  VARCHAR2(1)
);

-- Primary Key constraint

ALTER TABLE pages_access
 ADD CONSTRAINT pk_pages_access
 PRIMARY KEY (pa_id)
 USING INDEX;

-- Primary Key Sequence
CREATE SEQUENCE seq_pages_access
 START WITH 1
 INCREMENT BY 1
 NOMAXVALUE;

-- Foreign Key Constraints
ALTER TABLE pages_access
 ADD CONSTRAINT fk_pa_pages
 FOREIGN KEY (page_id)
 REFERENCES pages (page_id); 

ALTER TABLE pages_access
 ADD CONSTRAINT fk_pa_access
 FOREIGN KEY (acc_id)
 REFERENCES accesses (acc_id);

-- Check Constraints
ALTER TABLE pages_acccess
 ADD CONSTRAINT chk_access_ind
 CHECK pa_access_ind in ('0','1')
 
-- Public synonym
CREATE PUBLIC SYNONYM page_access FOR page_access;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON page_access TO ium_app_user;

