CREATE TABLE mib_schedules  
(
  mib_sked_id 		NUMBER(3) NOT NULL
, mib_sked_frequency 	VARCHAR2(10)
, mib_sked_day 		VARCHAR2(10)
, mib_sked_time 	TIMESTAMP
, mib_sked_status	VARCHAR2(10)
);

-- Primary Key 

ALTER TABLE mib_schedules
 ADD CONSTRAINT pk_mib_schedules
 PRIMARY KEY (mib_sked_id)
 USING INDEX;

-- Primary Key Sequence

CREATE SEQUENCE seq_mib_schedule
	START WITH 1
	INCREMENT BY 1
	MAXVALUE 999;
	
-- Public synonym
CREATE PUBLIC SYNONYM mib_schedules FOR mib_schedules;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON mib_schedules TO ium_app_user;
	