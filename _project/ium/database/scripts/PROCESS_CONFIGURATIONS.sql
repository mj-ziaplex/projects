CREATE TABLE process_configurations 
(
  evnt_id		NUMBER(5) NOT NULL
, proc_code 		VARCHAR2(4)
, evnt_name		VARCHAR2(25)
, evnt_from_status	NUMBER(4)
, evnt_to_status	NUMBER(4)
, lob_code 		VARCHAR2(2)
, not_id 		NUMBER(3)
, proc_notification_ind VARCHAR2(1)
, created_by 		VARCHAR2(10)
, created_date 		TIMESTAMP
, updated_by 		VARCHAR2(10)
, updated_date 		TIMESTAMP
)
;

-- Primary Key

ALTER TABLE process_configurations
 ADD CONSTRAINT pk_process_configurations
 PRIMARY KEY (evnt_id)
 USING INDEX;
 
-- Primary Key Sequence
CREATE SEQUENCE seq_process_configuration
    START WITH 1
	INCREMENT BY 1
	MAXVALUE 99999;

-- Foreign Keys

ALTER TABLE process_configurations
 ADD CONSTRAINT fk_pc_lines_of_business
 FOREIGN KEY (lob_code)
 REFERENCES lines_of_business (lob_code);

ALTER TABLE process_configurations
 ADD CONSTRAINT fk_pc_user_created_by
 FOREIGN KEY (created_by)
 REFERENCES users (user_id);

ALTER TABLE process_configurations
 ADD CONSTRAINT fk_pc_users_updated_by
 FOREIGN KEY (updated_by)
 REFERENCES users (user_id);

ALTER TABLE process_configurations
 ADD CONSTRAINT fk_pc_status_from_status
 FOREIGN KEY (evnt_from_status)
 REFERENCES status (stat_id);

ALTER TABLE process_configurations
 ADD CONSTRAINT fk_pc_status_to_status
 FOREIGN KEY (evnt_to_status)
 REFERENCES status (stat_id);
 
-- Public synonym
CREATE PUBLIC SYNONYM process_configurations FOR process_configurations;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON process_configurations TO ium_app_user;
 