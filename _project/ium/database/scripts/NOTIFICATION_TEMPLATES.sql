CREATE TABLE notification_templates 
(
  not_id 		NUMBER(5) NOT NULL
, not_subj		VARCHAR2(100)
, not_desc 		VARCHAR2(40)
, not_email_content 	LONG
, not_sender		VARCHAR2(6)
, not_notify_mobile_ind VARCHAR2(1)
, not_mobile_content 	VARCHAR2(500)
, created_by 		VARCHAR2(10) 
, created_date 		TIMESTAMP
, updated_by 		VARCHAR2(10)
, updated_date 		TIMESTAMP
);

-- Primary Key 

ALTER TABLE notification_templates
 ADD CONSTRAINT pk_notification_templates
 PRIMARY KEY (not_id)
 USING INDEX;

-- Primary Key Sequence

CREATE SEQUENCE seq_notification_template
	START WITH 1
	INCREMENT BY 1
	MAXVALUE 99999;

-- Foreign Key

ALTER TABLE notification_templates
 ADD CONSTRAINT fk_ntt_users_created_by
 FOREIGN KEY (created_by)
 REFERENCES users (user_id);

ALTER TABLE notification_templates
 ADD CONSTRAINT fk_ntt_users_updated_by
 FOREIGN KEY (updated_by)
 REFERENCES users (user_id);

-- Public synonym
CREATE PUBLIC SYNONYM notification_templates FOR notification_templates;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON notification_templates TO ium_app_user;
