CREATE TABLE holidays 
(
  hol_id 		NUMBER(3) NOT NULL
, hol_date 		DATE
, hol_desc 		VARCHAR2(40)
, created_by	VARCHAR2(10)
, created_date	TIMESTAMP(6)
, updated_by	VARCHAR2(10)
, updated_date	TIMESTAMP(6)
);

-- Primary Key 

ALTER TABLE holidays
 ADD CONSTRAINT pk_holidays
 PRIMARY KEY (hol_id)
 USING INDEX;

-- Primary Key Sequence

CREATE SEQUENCE seq_holiday
    START WITH 1
	INCREMENT BY 1
	MAXVALUE 999;
	
-- Public synonym
CREATE PUBLIC SYNONYM holidays FOR holidays;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON holidays TO ium_app_user;
	