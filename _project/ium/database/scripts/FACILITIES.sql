CREATE TABLE facilities 
(
  fac_id 		NUMERIC(2) 
, fac_desc 		VARCHAR2(25)
); 

-- Primary Key 

ALTER TABLE facilities
 ADD CONSTRAINT pk_facilities
 PRIMARY KEY (fac_id)
 USING INDEX;

-- Primary Key Sequence

CREATE SEQUENCE seq_facility
	START WITH 1
	INCREMENT BY 1
	NOMAXVALUE;