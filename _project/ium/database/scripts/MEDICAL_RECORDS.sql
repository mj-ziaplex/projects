CREATE TABLE medical_records
(
  med_record_id 	NUMBER(8) NOT NULL
, cl_client_num 	VARCHAR2(10)
, agent_id 		VARCHAR2(8) 
, branch_id 		VARCHAR2(15)
, med_status 		NUMBER(4)
, med_lab_test_ind 	VARCHAR2(1)
, med_examination_place NUMBER(3)
, med_lab_id 		NUMBER(5)
, med_test_id 		NUMBER(5)
, exmnr_id 			NUMBER(5)          
, med_appointment_date 	TIMESTAMP
, med_date_conducted 	DATE
, med_date_received 	DATE
, med_follow_up_num 	NUMERIC(2)
, med_follow_up_date 	DATE
, med_remarks 		LONG
, med_validity_date 	DATE
, med_7day_memo_ind 	VARCHAR2(1)
, med_7day_memo_date 	DATE
, med_reqt_reason 	VARCHAR2(100)
, med_section_id 	VARCHAR2(15)
, med_requesting_party 	VARCHAR2(5)
, med_department_id 	VARCHAR2(10)
, med_chargeable_to 	VARCHAR2(5)
, med_paid_by_agent_ind VARCHAR2(1)
, med_amount 		NUMBER(8)
, med_request_date	DATE
, med_request_loa_ind 	VARCHAR2(1)
, med_results_rcvd 	VARCHAR2(1)
, created_by 		VARCHAR2(10) 
, created_date 		TIMESTAMP
, updated_by 		VARCHAR2(10)
, updated_date 		TIMESTAMP
, cl_last_name		VARCHAR2(40)
, cl_given_name		VARCHAR2(25)
, cl_birth_date		DATE
); 

-- Primary Key 

ALTER TABLE medical_records
 ADD CONSTRAINT pk_medical_records
 PRIMARY KEY (med_record_id)
 USING INDEX;

-- Primary Key Sequence

CREATE SEQUENCE seq_medical_record
	START WITH 1
	INCREMENT BY 1
	NOMAXVALUE;

-- Foreign Key

ALTER TABLE medical_records
 ADD CONSTRAINT fk_med_clients
 FOREIGN KEY (cl_client_num)
 REFERENCES clients (client_id);

ALTER TABLE medical_records
 ADD CONSTRAINT fk_med_users_agent
 FOREIGN KEY (agent_id)
 REFERENCES users (user_id);

ALTER TABLE medical_records
 ADD CONSTRAINT fk_med_branches
 FOREIGN KEY (branch_id)
 REFERENCES sunlife_offices (slo_office_code);

ALTER TABLE medical_records
 ADD CONSTRAINT fk_med_status
 FOREIGN KEY (med_status)
 REFERENCES status (stat_id);

ALTER TABLE medical_records
 ADD CONSTRAINT fk_med_examination_places
 FOREIGN KEY (med_examination_place)
 REFERENCES examination_places (ep_id);

ALTER TABLE medical_records
 ADD CONSTRAINT fk_med_laboratories
 FOREIGN KEY (med_lab_id)
 REFERENCES laboratories (lab_id);

ALTER TABLE medical_records
 ADD CONSTRAINT fk_med_examiners
 FOREIGN KEY (exmnr_id)
 REFERENCES examiners (exmnr_id);

ALTER TABLE medical_records
 ADD CONSTRAINT fk_med_test_profiles
 FOREIGN KEY (med_test_id)
 REFERENCES test_profiles (test_id);

ALTER TABLE medical_records
 ADD CONSTRAINT fk_med_sections
 FOREIGN KEY (med_section_id)
 REFERENCES sections (sec_code);

ALTER TABLE medical_records
 ADD CONSTRAINT fk_med_departments
 FOREIGN KEY (med_department_id)
 REFERENCES departments (dept_code);

ALTER TABLE medical_records
 ADD CONSTRAINT fk_med_users_created_by
 FOREIGN KEY (created_by)
 REFERENCES users (user_id);

ALTER TABLE medical_records
 ADD CONSTRAINT fk_med_users_updated_by
 FOREIGN KEY (updated_by)
 REFERENCES users (user_id);

-- Public synonym
CREATE PUBLIC SYNONYM medical_records FOR medical_records;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON medical_records TO ium_app_user;
