create table KICKOUT_CODES
(
  KO_SEQ		NUMBER(5) NOT NULL
, KO_MESSAGE	VARCHAR2(70)
);


ALTER TABLE kickout_codes
 ADD CONSTRAINT pk_kickout_codes
 PRIMARY KEY (ko_seq)
 USING INDEX;

-- Public synonym
CREATE PUBLIC SYNONYM kickout_codes FOR kickout_codes;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON kickout_codes TO ium_app_user;
