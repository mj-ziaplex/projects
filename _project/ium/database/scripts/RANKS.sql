CREATE TABLE ranks 
(
  rank_id 	NUMBER(3) NOT NULL
, rank_desc 	VARCHAR2(40)
, rank_fee 	NUMERIC(10,2)
, created_by	VARCHAR2(10)
, created_date	TIMESTAMP(6)
, updated_by	VARCHAR2(10)
, updated_date	TIMESTAMP(6)
);

-- Primary Key 

ALTER TABLE ranks
 ADD CONSTRAINT pk_ranks
 PRIMARY KEY (rank_id)
 USING INDEX;

-- Primary Key Sequence

CREATE SEQUENCE seq_rank
	START WITH 1
	INCREMENT BY 1
	MAXVALUE 999;

-- Public synonym
CREATE PUBLIC SYNONYM ranks FOR ranks;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON ranks TO ium_app_user;
