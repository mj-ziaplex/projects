CREATE TABLE EXCEPTION_LOG
(
  EXCEPTION_ID     NUMBER(8) NOT NULL
, BATCH_ID         NUMBER(8)
, REFERENCE_NUM    VARCHAR2(15)
, XML_RECORD       LONG
);

ALTER TABLE exception_log
 ADD CONSTRAINT pk_exception_log
 PRIMARY KEY (exception_id)
 USING INDEX;

ALTER TABLE exception_log
 ADD CONSTRAINT fk_exception_batch
 FOREIGN KEY (batch_id)
 REFERENCES exception_batch(batch_id);
 
CREATE SEQUENCE SEQ_EXCEPTION_ID
		INCREMENT BY 1
		START WITH 1
		MAXVALUE 99999999;

		
-- Public synonym
CREATE PUBLIC SYNONYM exception_log FOR exception_log;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON exception_log TO ium_app_user;
		