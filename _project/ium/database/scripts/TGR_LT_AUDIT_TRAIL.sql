create or replace trigger TGR_LT_AUDIT_TRAIL
   after update or delete on LABORATORY_TESTS for each row

declare
   changed_from	varchar2(1000);
   changed_to	varchar2(1000);

   procedure APPEND_TO_CHANGED_FROM(old_value varchar2) is
   begin
      if LENGTH(changed_from) = 0 then
         changed_from := '<?xml version="1.0" encoding="ISO-8859-1"?>';
      end if;
      changed_from := changed_from || old_value;
   end;

   procedure APPEND_TO_CHANGED_TO(new_value varchar2) is
   begin
      if LENGTH(changed_to) = 0 then
         changed_to := '<?xml version="1.0" encoding="ISO-8859-1"?>';
      end if;
      changed_to := changed_to || new_value;
   end;

begin
   if updating then
      if nvl(:OLD.LAB_ID, 0) != nvl(:NEW.LAB_ID, 0) then
         APPEND_TO_CHANGED_FROM('<Laboratory>' || to_char(:OLD.LAB_ID) || '</Laboratory>');
         APPEND_TO_CHANGED_TO('<Laboratory>' || to_char(:NEW.LAB_ID) || '</Laboratory>');
      end if;
      if nvl(:OLD.TEST_ID, 0) != nvl(:NEW.TEST_ID, 0) then
         APPEND_TO_CHANGED_FROM('<TestCode>' || to_char(:OLD.TEST_ID) || '</TestCode>');
         APPEND_TO_CHANGED_TO('<TestCode>' || to_char(:NEW.TEST_ID) || '</TestCode>');
      end if;
      if nvl(:OLD.LAB_TEST_STATUS, '') != nvl(:NEW.LAB_TEST_STATUS, '') then
         APPEND_TO_CHANGED_FROM('<Status>' || :OLD.LAB_TEST_STATUS || '</Status>');
         APPEND_TO_CHANGED_TO('<Status>' || :NEW.LAB_TEST_STATUS || '</Status>');
      end if;

      if LENGTH(changed_from) > 0 then
         PROC_CREATE_AUDIT_TRAIL(17, 'UPDATE', :NEW.UPDATED_BY, to_char(:NEW.LAB_ID) || '-' || to_char(:NEW.TEST_ID), changed_from, changed_to);
      end if;
   elsif deleting then
      PROC_CREATE_AUDIT_TRAIL(17, 'DELETE', :OLD.UPDATED_BY, to_char(:OLD.LAB_ID) || '-' || to_char(:OLD.TEST_ID), changed_from, changed_to);
   end if;
end;
/