create or replace trigger TGR_UPA_AUDIT_TRAIL 
   after update or delete on USER_PAGE_ACCESS for each row

declare
   changed_from	varchar2(1000);
   changed_to	varchar2(1000);

   procedure APPEND_TO_CHANGED_FROM(old_value varchar2) is
   begin
      if LENGTH(changed_from) = 0 then
         changed_from := '<?xml version="1.0" encoding="ISO-8859-1"?>';
      end if;
      changed_from := changed_from || old_value;
   end;

   procedure APPEND_TO_CHANGED_TO(new_value varchar2) is
   begin
      if LENGTH(changed_to) = 0 then
         changed_to := '<?xml version="1.0" encoding="ISO-8859-1"?>';
      end if;
      changed_to := changed_to || new_value;
   end;

begin
   if updating then
      if nvl(:OLD.USER_ID, '') != nvl(:NEW.USER_ID, '') then
         APPEND_TO_CHANGED_FROM('<UserID>' || :OLD.USER_ID || '</UserID>');
         APPEND_TO_CHANGED_TO('<UserID>' || :NEW.USER_ID || '</UserID>');
      end if;
      if nvl(:OLD.PAGE_ID, 0) != nvl(:NEW.PAGE_ID, 0) then
         APPEND_TO_CHANGED_FROM('<PageID>' || to_char(:OLD.PAGE_ID) || '</PageID>');
         APPEND_TO_CHANGED_TO('<PageID>' || to_char(:NEW.PAGE_ID) || '</PageID>');
      end if;
      if nvl(:OLD.ACC_ID, 0) != nvl(:NEW.ACC_ID, 0) then
         APPEND_TO_CHANGED_FROM('<AccessID>' || to_char(:OLD.ACC_ID) || '</AccessID>');
         APPEND_TO_CHANGED_TO('<AccessID>' || to_char(:NEW.ACC_ID) || '</AccessID>');
      end if;
      if nvl(:OLD.UPA_ACCESS_IND, '') != nvl(:NEW.UPA_ACCESS_IND, '') then
         APPEND_TO_CHANGED_FROM('<AccessIndicator>' || :OLD.UPA_ACCESS_IND || '</AccessIndicator>');
         APPEND_TO_CHANGED_TO('<AccessIndicator>' || :NEW.UPA_ACCESS_IND || '</AccessIndicator>');
      end if;
      if nvl(:OLD.CREATED_BY, '') != nvl(:NEW.CREATED_BY, '') then
         APPEND_TO_CHANGED_FROM('<CreatedBy>' || :OLD.CREATED_BY || '</CreatedBy>');
         APPEND_TO_CHANGED_TO('<CreatedBy>' || :NEW.CREATED_BY || '</CreatedBy>');
      end if;
      if nvl(:OLD.CREATED_DATE, '') != nvl(:NEW.CREATED_DATE, '') then
         APPEND_TO_CHANGED_FROM('<CreatedDate>' || to_char(:OLD.CREATED_DATE) || '</CreatedDate>');
         APPEND_TO_CHANGED_TO('<CreatedDate>' || to_char(:NEW.CREATED_DATE) || '</CreatedDate>');
      end if;
      if nvl(:OLD.UPDATED_BY, '') != nvl(:NEW.UPDATED_BY, '') then
         APPEND_TO_CHANGED_FROM('<UpdatedBy>' || :OLD.UPDATED_BY || '</UpdatedBy>');
         APPEND_TO_CHANGED_TO('<UpdatedBy>' || :NEW.UPDATED_BY || '</UpdatedBy>');
      end if;
      if nvl(:OLD.UPDATED_DATE, '') != nvl(:NEW.UPDATED_DATE, '') then
         APPEND_TO_CHANGED_FROM('<UpdatedDate>' || to_char(:OLD.UPDATED_DATE) || '</UpdatedDate>');
         APPEND_TO_CHANGED_TO('<UpdatedDate>' || to_char(:NEW.UPDATED_DATE) || '</UpdatedDate>');
      end if;

      if LENGTH(changed_from) > 0 then
         PROC_CREATE_AUDIT_TRAIL(6, 'UPDATE', :NEW.UPDATED_BY, :NEW.USER_ID || '-' || to_char(:NEW.PAGE_ID) || '-' || to_char(:NEW.ACC_ID), changed_from, changed_to);
      end if;
   elsif deleting then
      PROC_CREATE_AUDIT_TRAIL(6, 'DELETE', :OLD.UPDATED_BY, :OLD.USER_ID || '-' || to_char(:OLD.PAGE_ID) || '-' || to_char(:OLD.ACC_ID), changed_from, changed_to);
   end if;
end;
/