CREATE TABLE policy_medical_records
(
  med_record_id 	NUMBER(8) NOT NULL
, uar_reference_num	VARCHAR2(15) NOT NULL
, association_type	VARCHAR2(1)
); 

-- Primary Key 

ALTER TABLE policy_medical_records
 ADD CONSTRAINT pk_policy_medical_records
 PRIMARY KEY (med_record_id, uar_reference_num)
 USING INDEX;

-- Foreign Key

ALTER TABLE policy_medical_records
 ADD CONSTRAINT fk_pmr_assessment_request
 FOREIGN KEY (uar_reference_num)
 REFERENCES assessment_requests (reference_num);

-- Public synonym
CREATE PUBLIC SYNONYM policy_medical_records FOR policy_medical_records;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON policy_medical_records TO ium_app_user;
