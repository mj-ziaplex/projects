CREATE TABLE lines_of_business 
(
  lob_code 		VARCHAR2(2) NOT NULL
, lob_desc 		VARCHAR2(25)
, created_by	VARCHAR2(10)
, created_date	TIMESTAMP(6)
, updated_by	VARCHAR2(10)
, updated_date	TIMESTAMP(6)
);

-- Primary Key 

ALTER TABLE lines_of_business
 ADD CONSTRAINT pk_lines_of_business
 PRIMARY KEY (lob_code)
 USING INDEX;

-- Public synonym
CREATE PUBLIC SYNONYM lines_of_business FOR lines_of_business;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON lines_of_business TO ium_app_user;
