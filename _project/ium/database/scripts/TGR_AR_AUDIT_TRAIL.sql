create or replace trigger TGR_AR_AUDIT_TRAIL 
   after update on ASSESSMENT_REQUESTS for each row

declare
   changed_from	varchar2(1000);
   changed_to	varchar2(1000);

   procedure APPEND_TO_CHANGED_FROM(old_value varchar2) is
   begin
      if LENGTH(changed_from) = 0 then
         changed_from := '<?xml version="1.0" encoding="ISO-8859-1"?>';
      end if;
      changed_from := changed_from || old_value;
   end;

   procedure APPEND_TO_CHANGED_TO(new_value varchar2) is
   begin
      if LENGTH(changed_to) = 0 then
         changed_to := '<?xml version="1.0" encoding="ISO-8859-1"?>';
      end if;
      changed_to := changed_to || new_value;
   end;

begin
      if nvl(:OLD.REFERENCE_NUM, '') != nvl(:NEW.REFERENCE_NUM, '') then
         APPEND_TO_CHANGED_FROM('<ReferenceNumber>' || :OLD.REFERENCE_NUM || '</ReferenceNumber>');
         APPEND_TO_CHANGED_TO('<ReferenceNumber>' || :NEW.REFERENCE_NUM || '</ReferenceNumber>');
      end if;
      if nvl(:OLD.LOB, '') != nvl(:NEW.LOB, '') then
         APPEND_TO_CHANGED_FROM('<LOB>' || :OLD.LOB || '</LOB>');
         APPEND_TO_CHANGED_TO('<LOB>' || :NEW.LOB || '</LOB>');
      end if;
      if nvl(:OLD.INSURED_CLIENT_ID, '') != nvl(:NEW.INSURED_CLIENT_ID, '') then
         APPEND_TO_CHANGED_FROM('<InsuredClientID>' || :OLD.INSURED_CLIENT_ID || '</InsuredClientID>');
         APPEND_TO_CHANGED_TO('<InsuredClientID>' || :NEW.INSURED_CLIENT_ID || '</InsuredClientID>');
      end if;
      if nvl(:OLD.OWNER_CLIENT_ID, '') != nvl(:NEW.OWNER_CLIENT_ID, '') then
         APPEND_TO_CHANGED_FROM('<OwnerClientID>' || :OLD.OWNER_CLIENT_ID || '</OwnerClientID>');
         APPEND_TO_CHANGED_TO('<OwnerClientID>' || :NEW.OWNER_CLIENT_ID || '</OwnerClientID>');
      end if;
      if nvl(:OLD.BRANCH_ID, '') != nvl(:NEW.BRANCH_ID, '') then
         APPEND_TO_CHANGED_FROM('<BranchID>' || :OLD.BRANCH_ID || '</BranchID>');
         APPEND_TO_CHANGED_TO('<BranchID>' || :NEW.BRANCH_ID || '</BranchID>');
      end if;
      if nvl(:OLD.AGENT_ID, '') != nvl(:NEW.AGENT_ID, '') then
         APPEND_TO_CHANGED_FROM('<AgentID>' || :OLD.AGENT_ID || '</AgentID>');
         APPEND_TO_CHANGED_TO('<AgentID>' || :NEW.AGENT_ID || '</AgentID>');
      end if;
      if nvl(:OLD.STATUS_ID, 0) != nvl(:NEW.STATUS_ID, 0) then
         APPEND_TO_CHANGED_FROM('<RequestStatusCode>' || to_char(:OLD.STATUS_ID) || '</RequestStatusCode>');
         APPEND_TO_CHANGED_TO('<RequestStatusCode>' || to_char(:NEW.STATUS_ID) || '</RequestStatusCode>');
      end if;
      if nvl(:OLD.STATUS_DATE, '') != nvl(:NEW.STATUS_DATE, '') then
         APPEND_TO_CHANGED_FROM('<StatusDate>' || to_char(:OLD.STATUS_DATE) || '</StatusDate>');
         APPEND_TO_CHANGED_TO('<StatusDate>' || to_char(:NEW.STATUS_DATE) || '</StatusDate>');
      end if;
      if nvl(:OLD.ASSIGNED_TO, 0) != nvl(:NEW.ASSIGNED_TO, 0) then
         APPEND_TO_CHANGED_FROM('<AssignedTo>' || :OLD.ASSIGNED_TO || '</AssignedTo>');
         APPEND_TO_CHANGED_TO('<AssignedTo>' || :NEW.ASSIGNED_TO || '</AssignedTo>');
      end if;
      if nvl(:OLD.UNDERWRITER, 0) != nvl(:NEW.UNDERWRITER, 0) then
         APPEND_TO_CHANGED_FROM('<Underwriter>' || :OLD.UNDERWRITER || '</Underwriter>');
         APPEND_TO_CHANGED_TO('<Underwriter>' || :NEW.UNDERWRITER || '</Underwriter>');
      end if;
      if nvl(:OLD.FOLDER_LOCATION, 0) != nvl(:NEW.FOLDER_LOCATION, 0) then
         APPEND_TO_CHANGED_FROM('<FolderLocation>' || :OLD.FOLDER_LOCATION || '</FolderLocation>');
         APPEND_TO_CHANGED_TO('<FolderLocation>' || :NEW.FOLDER_LOCATION || '</FolderLocation>');
      end if;
      if nvl(:OLD.CREATED_BY, '') != nvl(:NEW.CREATED_BY, '') then
         APPEND_TO_CHANGED_FROM('<CreatedBy>' || :OLD.CREATED_BY || '</CreatedBy>');
         APPEND_TO_CHANGED_TO('<CreatedBy>' || :NEW.CREATED_BY || '</CreatedBy>');
      end if;
      if nvl(:OLD.CREATED_DATE, '') != nvl(:NEW.CREATED_DATE, '') then
         APPEND_TO_CHANGED_FROM('<CreatedDate>' || to_char(:OLD.CREATED_DATE) || '</CreatedDate>');
         APPEND_TO_CHANGED_TO('<CreatedDate>' || to_char(:NEW.CREATED_DATE) || '</CreatedDate>');
      end if;
      if nvl(:OLD.UPDATED_BY, '') != nvl(:NEW.UPDATED_BY, '') then
         APPEND_TO_CHANGED_FROM('<UpdatedBy>' || :OLD.UPDATED_BY || '</UpdatedBy>');
         APPEND_TO_CHANGED_TO('<UpdatedBy>' || :NEW.UPDATED_BY || '</UpdatedBy>');
      end if;
      if nvl(:OLD.UPDATED_DATE, '') != nvl(:NEW.UPDATED_DATE, '') then
         APPEND_TO_CHANGED_FROM('<UpdatedDate>' || to_char(:OLD.UPDATED_DATE) || '</UpdatedDate>');
         APPEND_TO_CHANGED_TO('<UpdatedDate>' || to_char(:NEW.UPDATED_DATE) || '</UpdatedDate>');
      end if;

      if LENGTH(changed_from) > 0 then
         PROC_CREATE_AUDIT_TRAIL(1, 'UPDATE', :NEW.UPDATED_BY, :NEW.REFERENCE_NUM, changed_from, changed_to);
      end if;
end;
/