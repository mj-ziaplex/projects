CREATE TABLE user_page_access
(
  user_id 			VARCHAR2(10) NOT NULL
, page_id 			NUMBER(2) NOT NULL
, acc_id          	NUMBER(2) NOT NULL
, upa_access_ind	VARCHAR2(1)
, created_by		VARCHAR2(10)
, created_date		TIMESTAMP
, updated_by		VARCHAR2(10)
, updated_date		TIMESTAMP
);

-- Primary Key
ALTER TABLE user_page_access
 ADD CONSTRAINT pk_user_page_access
 PRIMARY KEY (user_id, page_id, acc_id)
 USING INDEX;
 
-- Foreign Keys
ALTER TABLE user_page_access
 ADD CONSTRAINT fk_upa_users
 FOREIGN KEY (user_id)
 REFERENCES users (user_id);
 
ALTER TABLE user_page_access
 ADD CONSTRAINT fk_upa_pages
 FOREIGN KEY (page_id)
 REFERENCES pages (page_id);
 
ALTER TABLE user_page_access
 ADD CONSTRAINT fk_upa_users_created_by
 FOREIGN KEY (created_by)
 REFERENCES users (user_id);

ALTER TABLE user_page_access
 ADD CONSTRAINT fk_upa_users_updated_by
 FOREIGN KEY (updated_by)
 REFERENCES users (user_id);

 -- Public synonym
CREATE PUBLIC SYNONYM user_page_access FOR user_page_access;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON user_page_access TO ium_app_user;
 