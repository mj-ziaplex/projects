create or replace trigger TGR_USER_AUDIT_TRAIL 
   after update or delete on USERS for each row

declare
   changed_from	varchar2(1000);
   changed_to	varchar2(1000);

   procedure APPEND_TO_CHANGED_FROM(old_value varchar2) is
   begin
      if LENGTH(changed_from) = 0 then
         changed_from := '<?xml version="1.0" encoding="ISO-8859-1"?>';
      end if;
      changed_from := changed_from || old_value;
   end;

   procedure APPEND_TO_CHANGED_TO(new_value varchar2) is
   begin
      if LENGTH(changed_to) = 0 then
         changed_to := '<?xml version="1.0" encoding="ISO-8859-1"?>';
      end if;
      changed_to := changed_to || new_value;
   end;

begin
   if updating then
      if nvl(:OLD.USER_ID, '') != nvl(:NEW.USER_ID, '') then
         APPEND_TO_CHANGED_FROM('<UserID>' || :OLD.USER_ID || '</UserID>');
         APPEND_TO_CHANGED_TO('<UserID>' || :NEW.USER_ID || '</UserID>');
      end if;
      if nvl(:OLD.USR_ACF2ID, '') != nvl(:NEW.USR_ACF2ID, '') then
         APPEND_TO_CHANGED_FROM('<ACF2ID>' || :OLD.USR_ACF2ID || '</ACF2ID>');
         APPEND_TO_CHANGED_TO('<ACF2ID>' || :NEW.USR_ACF2ID || '</ACF2ID>');
      end if;
      if nvl(:OLD.USR_CODE, '') != nvl(:NEW.USR_CODE, '') then
         APPEND_TO_CHANGED_FROM('<UserCode>' || :OLD.USR_CODE || '</UserCode>');
         APPEND_TO_CHANGED_TO('<UserCode>' || :NEW.USR_CODE || '</UserCode>');
      end if;
      if nvl(:OLD.USR_TYPE, '') != nvl(:NEW.USR_TYPE, '') then
         APPEND_TO_CHANGED_FROM('<UserType>' || :OLD.USR_TYPE || '</UserType>');
         APPEND_TO_CHANGED_TO('<UserType>' || :NEW.USR_TYPE || '</UserType>');
      end if;
      if nvl(:OLD.USR_ROLE, '') != nvl(:NEW.USR_ROLE, '') then
         APPEND_TO_CHANGED_FROM('<Role>' || :OLD.USR_ROLE || '</Role>');
         APPEND_TO_CHANGED_TO('<Role>' || :NEW.USR_ROLE || '</Role>');
      end if;
      if nvl(:OLD.SLO_ID, '') != nvl(:NEW.SLO_ID, '') then
         APPEND_TO_CHANGED_FROM('<OfficeCode>' || :OLD.SLO_ID || '</OfficeCode>');
         APPEND_TO_CHANGED_TO('<OfficeCode>' || :NEW.SLO_ID || '</OfficeCode>');
      end if;
      if nvl(:OLD.DEPT_ID, '') != nvl(:NEW.DEPT_ID, '') then
         APPEND_TO_CHANGED_FROM('<DepartmentCode>' || :OLD.DEPT_ID || '</DepartmentCode>');
         APPEND_TO_CHANGED_TO('<DepartmentCode>' || :NEW.DEPT_ID || '</DepartmentCode>');
      end if;
      if nvl(:OLD.SEC_ID, '') != nvl(:NEW.SEC_ID, '') then
         APPEND_TO_CHANGED_FROM('<SectionCode>' || :OLD.SEC_ID || '</SectionCode>');
         APPEND_TO_CHANGED_TO('<SectionCode>' || :NEW.SEC_ID || '</SectionCode>');
      end if;
      if nvl(:OLD.CREATED_BY, '') != nvl(:NEW.CREATED_BY, '') then
         APPEND_TO_CHANGED_FROM('<CreatedBy>' || :OLD.CREATED_BY || '</CreatedBy>');
         APPEND_TO_CHANGED_TO('<CreatedBy>' || :NEW.CREATED_BY || '</CreatedBy>');
      end if;
      if nvl(:OLD.CREATED_DATE, '') != nvl(:NEW.CREATED_DATE, '') then
         APPEND_TO_CHANGED_FROM('<CreatedDate>' || to_char(:OLD.CREATED_DATE) || '</CreatedDate>');
         APPEND_TO_CHANGED_TO('<CreatedDate>' || to_char(:NEW.CREATED_DATE) || '</CreatedDate>');
      end if;
      if nvl(:OLD.UPDATED_BY, '') != nvl(:NEW.UPDATED_BY, '') then
         APPEND_TO_CHANGED_FROM('<UpdatedBy>' || :OLD.UPDATED_BY || '</UpdatedBy>');
         APPEND_TO_CHANGED_TO('<UpdatedBy>' || :NEW.UPDATED_BY || '</UpdatedBy>');
      end if;
      if nvl(:OLD.UPDATED_DATE, '') != nvl(:NEW.UPDATED_DATE, '') then
         APPEND_TO_CHANGED_FROM('<UpdatedDate>' || to_char(:OLD.UPDATED_DATE) || '</UpdatedDate>');
         APPEND_TO_CHANGED_TO('<UpdatedDate>' || to_char(:NEW.UPDATED_DATE) || '</UpdatedDate>');
      end if;

      if LENGTH(changed_from) > 0 then
         PROC_CREATE_AUDIT_TRAIL(5, 'UPDATE', :NEW.UPDATED_BY, :NEW.USER_ID, changed_from, changed_to);
      end if;
   elsif deleting then
         PROC_CREATE_AUDIT_TRAIL(5, 'DELETE', :OLD.UPDATED_BY, :OLD.USER_ID, changed_from, changed_to);
   end if;
end;
/