CREATE TABLE kickout_messages
(
  ko_sequence_num 	NUMBER(5) NOT NULL
, uar_reference_num 	VARCHAR2(15)
, ko_message_text 	VARCHAR2(68)
, client_id 		VARCHAR2(10)
, ko_fail_response 	VARCHAR2(15)
); 

-- Primary Key 

ALTER TABLE kickout_messages
 ADD CONSTRAINT pk_kickout_messages
 PRIMARY KEY (ko_sequence_num)
 USING INDEX;

-- Foreign Key

ALTER TABLE kickout_messages
 ADD CONSTRAINT fk_ko_assessment_requests
 FOREIGN KEY (uar_reference_num)
 REFERENCES assessment_requests (reference_num);

ALTER TABLE kickout_messages
 ADD CONSTRAINT fk_ko_clients
 FOREIGN KEY (client_id)
 REFERENCES clients (client_id);

-- Public synonym
CREATE PUBLIC SYNONYM kickout_messages FOR kickout_messages;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON kickout_messages TO ium_app_user;
