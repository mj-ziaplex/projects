CREATE TABLE lob_documents 
(
  lob_code 		VARCHAR2(2) NOT NULL
, doc_code 		VARCHAR2(3) NOT NULL
, created_by	VARCHAR2(10)
, created_date	TIMESTAMP(6)
, updated_by	VARCHAR2(10)
, updated_date	TIMESTAMP(6)
);

-- Primary Key 

ALTER TABLE lob_documents
 ADD CONSTRAINT pk_lob_documents
 PRIMARY KEY (lob_code, doc_code)
 USING INDEX;

-- Foreign Key

ALTER TABLE lob_documents 
 ADD CONSTRAINT fk_ldc_lines_of_business
 FOREIGN KEY (lob_code)
 REFERENCES lines_of_business (lob_code);

ALTER TABLE lob_documents 
 ADD CONSTRAINT fk_ldc_document_types
 FOREIGN KEY (doc_code)
 REFERENCES document_types (doc_code);

-- Public synonym
CREATE PUBLIC SYNONYM lob_documents FOR lob_documents;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON lob_documents TO ium_app_user;
