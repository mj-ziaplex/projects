/* Oracle-generated values used for unique keys

/* general format */
//CREATE SEQUENCE <sequence_name>
//	INCREMENT BY 1
//	START WITH 1
//	NOMAXVALUE;
	
// mib_schedules (3)
CREATE SEQUENCE seq_mib_schedule
	INCREMENT BY 1
	START WITH 1
	MAXVALUE 999;

// audit_trails (6)
CREATE SEQUENCE seq_audit_trail
	INCREMENT BY 1
	START WITH 1
	NOMAXVALUE;

// holidays (3)
CREATE SEQUENCE seq_holiday
	INCREMENT BY 1
	START WITH 1
	MAXVALUE 999;

// ranks (3)
CREATE SEQUENCE seq_rank
	INCREMENT BY 1
	START WITH 1
	MAXVALUE 999;

// facilities (2)
CREATE SEQUENCE seq_facility
	INCREMENT BY 1
	START WITH 1
	NOMAXVALUE;

// examination_places (3)
CREATE SEQUENCE seq_examination_place
	INCREMENT BY 1
	START WITH 1
	NOMAXVALUE;

// specializations (3)
CREATE SEQUENCE seq_specialization
	INCREMENT BY 1
	START WITH 1
	NOMAXVALUE;

// auto_assignment_criteria (3)
CREATE SEQUENCE seq_auto_assignment
	INCREMENT BY 1
	START WITH 1
	MAXVALUE 999;

// status (4)
CREATE SEQUENCE seq_status
	INCREMENT BY 1
	START WITH 1
	MAXVALUE 9999;

// processconfigurations (2)
CREATE SEQUENCE seq_process_configuration
	INCREMENT BY 1
	START WITH 1
	MAXVALUE 99;

// events (5)
CREATE SEQUENCE seq_event
	INCREMENT BY 1
	START WITH 1
	NOMAXVALUE;

// notification_templates (5)
CREATE SEQUENCE seq_notification_template
	INCREMENT BY 1
	START WITH 1
	MAXVALUE 99999;

// test_profiles (5)
CREATE SEQUENCE seq_test_profile
	INCREMENT BY 1
	START WITH 1
	NOMAXVALUE;

// laboratories (5)
CREATE SEQUENCE seq_laboratory
	INCREMENT BY 1
	START WITH 1
	NOMAXVALUE;

// examiner (5)
CREATE SEQUENCE seq_examiner
	INCREMENT BY 1
	START WITH 1
	NOMAXVALUE;

// access_templates (5)
CREATE SEQUENCE seq_access_template
	INCREMENT BY 1
	START WITH 1
	NOMAXVALUE;

// page_functions (2)
CREATE SEQUENCE seq_page
	INCREMENT BY 1
	START WITH 1
	MAXVALUE 99;

// access (2)
CREATE SEQUENCE seq_access
	INCREMENT BY 1
	START WITH 1
	MAXVALUE 99;

// roles (2)
CREATE SEQUENCE seq_role
	INCREMENT BY 1
	START WITH 1
	MAXVALUE 99;

// users (8)
CREATE SEQUENCE seq_user
	INCREMENT BY 1
	START WITH 1
	NOMAXVALUE;

// section (5)
CREATE SEQUENCE seq_section
	INCREMENT BY 1
	START WITH 1
	MAXVALUE 99999;

// department (5)
CREATE SEQUENCE seq_department
	INCREMENT BY 1
	START WITH 1
	MAXVALUE 99999;

// requirement_forms (5)
CREATE SEQUENCE seq_requirement_form
	INCREMENT BY 1
	START WITH 1
	NOMAXVALUE;

// folder_documents (8)
CREATE SEQUENCE seq_folder_document
	INCREMENT BY 1
	START WITH 1
	NOMAXVALUE;

// medical_bills (8)
CREATE SEQUENCE seq_medical_bill
	INCREMENT BY 1
	START WITH 1
	NOMAXVALUE;

// medical_notes (8)
CREATE SEQUENCE seq_medical_note
	INCREMENT BY 1
	START WITH 1
	NOMAXVALUE;

// medical_records (8)
CREATE SEQUENCE seq_medical_record
	INCREMENT BY 1
	START WITH 1
	NOMAXVALUE;

// impairments (5)
CREATE SEQUENCE seq_impairment
	INCREMENT BY 1
	START WITH 1
	NOMAXVALUE;





