create or replace trigger TGR_MB_AUDIT_TRAIL 
   after update on MEDICAL_BILLS for each row

declare
   changed_from	varchar2(2000);
   changed_to	varchar2(2000);

   procedure APPEND_TO_CHANGED_FROM(old_value varchar2) is
   begin
      if LENGTH(changed_from) = 0 then
         changed_from := '<?xml version="1.0" encoding="ISO-8859-1"?>';
      end if;
      changed_from := changed_from || old_value;
   end;

   procedure APPEND_TO_CHANGED_TO(new_value varchar2) is
   begin
      if LENGTH(changed_to) = 0 then
         changed_to := '<?xml version="1.0" encoding="ISO-8859-1"?>';
      end if;
      changed_to := changed_to || new_value;
   end;

begin
      if nvl(:OLD.MB_BILL_NUMBER, 0) != nvl(:NEW.MB_BILL_NUMBER, 0) then
         APPEND_TO_CHANGED_FROM('<BillNumber>' || to_char(:OLD.MB_BILL_NUMBER) || '</BillNumber>');
         APPEND_TO_CHANGED_TO('<BillNumber>' || to_char(:NEW.MB_BILL_NUMBER) || '</BillNumber>');
      end if;
      if nvl(:OLD.UAR_REFERENCE_NUM, '') != nvl(:NEW.UAR_REFERENCE_NUM, '') then
         APPEND_TO_CHANGED_FROM('<ReferenceNumber>' || :OLD.UAR_REFERENCE_NUM || '</ReferenceNumber>');
         APPEND_TO_CHANGED_TO('<ReferenceNumber>' || :NEW.UAR_REFERENCE_NUM || '</ReferenceNumber>');
      end if;
      if nvl(:OLD.EXMNR_ID, 0) != nvl(:NEW.EXMNR_ID, 0) then
         APPEND_TO_CHANGED_FROM('<Examiner>' || to_char(:OLD.EXMNR_ID) || '</Examiner>');
         APPEND_TO_CHANGED_TO('<Examiner>' || to_char(:NEW.EXMNR_ID) || '</Examiner>');
      end if;
      if nvl(:OLD.LAB_ID, 0) != nvl(:NEW.LAB_ID, 0) then
         APPEND_TO_CHANGED_FROM('<Laboratory>' || to_char(:OLD.LAB_ID) || '</Laboratory>');
         APPEND_TO_CHANGED_TO('<Laboratory>' || to_char(:NEW.LAB_ID) || '</Laboratory>');
      end if;
      if nvl(:OLD.MB_FEE, 0) != nvl(:NEW.MB_FEE, 0) then
         APPEND_TO_CHANGED_FROM('<Fee>' || to_char(:OLD.MB_FEE) || '</Fee>');
         APPEND_TO_CHANGED_TO('<Fee>' || to_char(:NEW.MB_FEE) || '</Fee>');
      end if;
      if nvl(:OLD.MB_DATE_POSTED, '') != nvl(:NEW.MB_DATE_POSTED, '') then
         APPEND_TO_CHANGED_FROM('<DatePosted>' || to_char(:OLD.MB_DATE_POSTED) || '</DatePosted>');
         APPEND_TO_CHANGED_TO('<DatePosted>' || to_char(:NEW.MB_DATE_POSTED) || '</DatePosted>');
      end if;
      if nvl(:OLD.CREATED_BY, '') != nvl(:NEW.CREATED_BY, '') then
         APPEND_TO_CHANGED_FROM('<CreatedBy>' || :OLD.CREATED_BY || '</CreatedBy>');
         APPEND_TO_CHANGED_TO('<CreatedBy>' || :NEW.CREATED_BY || '</CreatedBy>');
      end if;
      if nvl(:OLD.CREATED_DATE, '') != nvl(:NEW.CREATED_DATE, '') then
         APPEND_TO_CHANGED_FROM('<CreatedDate>' || to_char(:OLD.CREATED_DATE) || '</CreatedDate>');
         APPEND_TO_CHANGED_TO('<CreatedDate>' || to_char(:NEW.CREATED_DATE) || '</CreatedDate>');
      end if;
      if nvl(:OLD.UPDATED_BY, '') != nvl(:NEW.UPDATED_BY, '') then
         APPEND_TO_CHANGED_FROM('<UpdatedBy>' || :OLD.UPDATED_BY || '</UpdatedBy>');
         APPEND_TO_CHANGED_TO('<UpdatedBy>' || :NEW.UPDATED_BY || '</UpdatedBy>');
      end if;
      if nvl(:OLD.UPDATED_DATE, '') != nvl(:NEW.UPDATED_DATE, '') then
         APPEND_TO_CHANGED_FROM('<UpdatedDate>' || to_char(:OLD.UPDATED_DATE) || '</UpdatedDate>');
         APPEND_TO_CHANGED_TO('<UpdatedDate>' || to_char(:NEW.UPDATED_DATE) || '</UpdatedDate>');
      end if;

      if LENGTH(changed_from) > 0 then
         PROC_CREATE_AUDIT_TRAIL(39, 'UPDATE', :NEW.UPDATED_BY, to_char(:NEW.MB_BILL_NUMBER), changed_from, changed_to);
      end if;
end;
/