create or replace trigger TGR_SLO_AUDIT_TRAIL
   after update or delete on SUNLIFE_OFFICES for each row

declare
   changed_from	varchar2(2000);
   changed_to	varchar2(2000);

   procedure APPEND_TO_CHANGED_FROM(old_value varchar2) is
   begin
      if LENGTH(changed_from) = 0 then
         changed_from := '<?xml version="1.0" encoding="ISO-8859-1"?>';
      end if;
      changed_from := changed_from || old_value;
   end;

   procedure APPEND_TO_CHANGED_TO(new_value varchar2) is
   begin
      if LENGTH(changed_to) = 0 then
         changed_to := '<?xml version="1.0" encoding="ISO-8859-1"?>';
      end if;
      changed_to := changed_to || new_value;
   end;

begin
   if updating then
      if nvl(:OLD.SLO_OFFICE_CODE, '') != nvl(:NEW.SLO_OFFICE_CODE, '') then
         APPEND_TO_CHANGED_FROM('<SunLifeOfficeID>' || :OLD.SLO_OFFICE_CODE || '</SunLifeOfficeID>');
         APPEND_TO_CHANGED_TO('<SunLifeOfficeID>' || :NEW.SLO_OFFICE_CODE || '</SunLifeOfficeID>');
      end if;
      if nvl(:OLD.SLO_OFFICE_NAME, '') != nvl(:NEW.SLO_OFFICE_NAME, '') then
         APPEND_TO_CHANGED_FROM('<OfficeName>' || :OLD.SLO_OFFICE_NAME || '</OfficeName>');
         APPEND_TO_CHANGED_TO('<OfficeName>' || :NEW.SLO_OFFICE_NAME || '</OfficeName>');
      end if;
      if nvl(:OLD.SLO_TYPE, '') != nvl(:NEW.SLO_TYPE, '') then
         APPEND_TO_CHANGED_FROM('<Type>' || :OLD.SLO_TYPE || '</Type>');
         APPEND_TO_CHANGED_TO('<Type>' || :NEW.SLO_TYPE || '</Type>');
      end if;
      if nvl(:OLD.CREATED_BY, '') != nvl(:NEW.CREATED_BY, '') then
         APPEND_TO_CHANGED_FROM('<CreatedBy>' || :OLD.CREATED_BY || '</CreatedBy>');
         APPEND_TO_CHANGED_TO('<CreatedBy>' || :NEW.CREATED_BY || '</CreatedBy>');
      end if;
      if nvl(:OLD.CREATED_DATE, '') != nvl(:NEW.CREATED_DATE, '') then
         APPEND_TO_CHANGED_FROM('<CreatedDate>' || to_char(:OLD.CREATED_DATE) || '</CreatedDate>');
         APPEND_TO_CHANGED_TO('<CreatedDate>' || to_char(:NEW.CREATED_DATE) || '</CreatedDate>');
      end if;
      if nvl(:OLD.UPDATED_BY, '') != nvl(:NEW.UPDATED_BY, '') then
         APPEND_TO_CHANGED_FROM('<UpdatedBy>' || :OLD.UPDATED_BY || '</UpdatedBy>');
         APPEND_TO_CHANGED_TO('<UpdatedBy>' || :NEW.UPDATED_BY || '</UpdatedBy>');
      end if;
      if nvl(:OLD.UPDATED_DATE, '') != nvl(:NEW.UPDATED_DATE, '') then
         APPEND_TO_CHANGED_FROM('<UpdatedDate>' || to_char(:OLD.UPDATED_DATE) || '</UpdatedDate>');
         APPEND_TO_CHANGED_TO('<UpdatedDate>' || to_char(:NEW.UPDATED_DATE) || '</UpdatedDate>');
      end if;

      if LENGTH(changed_from) > 0 then
         PROC_CREATE_AUDIT_TRAIL(24, 'UPDATE', :NEW.UPDATED_BY, :NEW.SLO_OFFICE_CODE, changed_from, changed_to);
      end if;
   elsif deleting then
      PROC_CREATE_AUDIT_TRAIL(24, 'DELETE', :OLD.UPDATED_BY, :OLD.SLO_OFFICE_CODE, changed_from, changed_to);
   end if;
end;
/