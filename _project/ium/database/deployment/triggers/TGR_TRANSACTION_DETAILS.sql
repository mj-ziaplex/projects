create or replace trigger TGR_TRANSACTION_DETAILS
	after insert or update or delete on IMPAIRMENTS for each row

declare 
	V_DATE date;
	V_CONTROL_NUMBER varchar2(255);
	V_CTR number;
	V_USER varchar2(10);
	
	function GET_DATE_DIFF (DATE1 date, DATE2 date) return number is 
	V_RESULT number;
	begin
		V_RESULT := trunc(DATE1) - trunc(DATE2);
		return round(V_RESULT, 0);
	end;
	
	function GET_CONTROL_NO (IMP_DATE date, TD_DATE date) return number is
		V_DIFF number;
		V_TEMP number;
		V_COUNTER number;
	begin 
		V_DIFF := GET_DATE_DIFF(IMP_DATE, TD_DATE);
		if V_DIFF = 0 then
			select max(counter) into V_COUNTER from TD_COUNTER; /* Initially the table (TD_COUNTER) must have an initial value of 1*/
			V_TEMP := V_COUNTER + 1;
			insert into TD_COUNTER(counter, last_date) values (V_TEMP, V_DATE);
			select max(counter) into V_COUNTER from TD_COUNTER;
		else 
			delete from TD_COUNTER;
			V_COUNTER := 1;
			insert into TD_COUNTER  (counter, last_date) values (V_COUNTER, sysdate);
		end if;
		return V_COUNTER;
	end;
	
	function FORMAT_DATE (INPUT varchar2) return varchar2 is
		V_TEMP varchar2(15);
		V_TEMP2 varchar2(10);
	begin
		V_TEMP := substr(INPUT, 0,3);
		V_TEMP2 := substr(INPUT, 4, 7);
		V_TEMP := replace(V_TEMP, 0);
		V_TEMP2 := V_TEMP || V_TEMP2;
		return V_TEMP2;
	end;
	
begin
	select max(last_date) into V_DATE from td_counter;
	
	if inserting then 
		V_CTR := GET_CONTROL_NO(:new.created_date, V_DATE);
		V_CONTROL_NUMBER := 'A' || FORMAT_DATE(to_char(:new.created_date, 'mmddyyyy')) || to_char(V_CTR); 
		INSERT INTO TRANSACTION_DETAILS (impairment_id, control_no, user_cd, company_cd, tran_cd, tran_dt, record_affected, no_of_hit) 
		VALUES (:new.imp_impairement_id, V_CONTROL_NUMBER, :new.created_by, 'A', '1', sysdate, 1, 0);
	elsif updating then
		if :new.created_date != null then
			V_CTR := GET_CONTROL_NO(:new.updated_date, V_DATE);
			V_CONTROL_NUMBER := 'A' || FORMAT_DATE(to_char(:new.updated_date, 'mmddyyyy')) || to_char(V_CTR); 
			INSERT INTO TRANSACTION_DETAILS (impairment_id, control_no, user_cd, company_cd, tran_cd, tran_dt, record_affected, no_of_hit) 
			VALUES (:new.imp_impairement_id, V_CONTROL_NUMBER, :new.updated_by, 'A', '1', sysdate, 1, 0);
		end if;
	elsif deleting then
		V_CTR := GET_CONTROL_NO(sysdate, V_DATE);
		V_CONTROL_NUMBER := 'A' || FORMAT_DATE(to_char(sysdate, 'mmddyyyy')) || to_char(V_CTR);
		INSERT INTO TRANSACTION_DETAILS (impairment_id, control_no, user_cd, company_cd, tran_cd, tran_dt, record_affected, no_of_hit) 
		VALUES (:old.imp_impairement_id, V_CONTROL_NUMBER, :old.updated_by, 'A', '1', sysdate, 1, 0);
	end if;
end;
/