create or replace trigger TGR_NOT_AUDIT_TRAIL 
   after update or delete on NOTIFICATION_TEMPLATES for each row

declare
   changed_from	varchar2(2000);
   changed_to	varchar2(2000);

   procedure APPEND_TO_CHANGED_FROM(old_value varchar2) is
   begin
      if LENGTH(changed_from) = 0 then
         changed_from := '<?xml version="1.0" encoding="ISO-8859-1"?>';
      end if;
      changed_from := changed_from || old_value;
   end;

   procedure APPEND_TO_CHANGED_TO(new_value varchar2) is
   begin
      if LENGTH(changed_to) = 0 then
         changed_to := '<?xml version="1.0" encoding="ISO-8859-1"?>';
      end if;
      changed_to := changed_to || new_value;
   end;

begin
   if updating then
      if nvl(:OLD.NOT_ID, 0) != nvl(:NEW.NOT_ID, 0) then
         APPEND_TO_CHANGED_FROM('<NotificationID>' || to_char(:OLD.NOT_ID) || '</NotificationID>');
         APPEND_TO_CHANGED_TO('<NotificationID>' || to_char(:NEW.NOT_ID) || '</NotificationID>');
      end if;
      if nvl(:OLD.NOT_SUBJ, '') != nvl(:NEW.NOT_SUBJ, '') then
         APPEND_TO_CHANGED_FROM('<NotificationSubject>' || :OLD.NOT_SUBJ || '</NotificationSubject>');
         APPEND_TO_CHANGED_TO('<NotificationSubject>' || :NEW.NOT_SUBJ || '</NotificationSubject>');
      end if;
      if nvl(:OLD.NOT_DESC, '') != nvl(:NEW.NOT_DESC, '') then
         APPEND_TO_CHANGED_FROM('<NotificationDescription>' || :OLD.NOT_DESC || '</NotificationDescription>');
         APPEND_TO_CHANGED_TO('<NotificationDescription>' || :NEW.NOT_DESC || '</NotificationDescription>');
      end if;
      if nvl(:OLD.NOT_SENDER, '') != nvl(:NEW.NOT_SENDER, '') then
         APPEND_TO_CHANGED_FROM('<Sender>' || :OLD.NOT_SENDER || '</Sender>');
         APPEND_TO_CHANGED_TO('<Sender>' || :NEW.NOT_SENDER || '</Sender>');
      end if;
      if nvl(:OLD.NOT_NOTIFY_MOBILE_IND, '') != nvl(:NEW.NOT_NOTIFY_MOBILE_IND, '') then
         APPEND_TO_CHANGED_FROM('<NotifyMobile>' || :OLD.NOT_NOTIFY_MOBILE_IND || '</NotifyMobile>');
         APPEND_TO_CHANGED_TO('<NotifyMobile>' || :NEW.NOT_NOTIFY_MOBILE_IND || '</NotifyMobile>');
      end if;
      if nvl(:OLD.CREATED_BY, '') != nvl(:NEW.CREATED_BY, '') then
         APPEND_TO_CHANGED_FROM('<CreatedBy>' || :OLD.CREATED_BY || '</CreatedBy>');
         APPEND_TO_CHANGED_TO('<CreatedBy>' || :NEW.CREATED_BY || '</CreatedBy>');
      end if;
      if nvl(:OLD.CREATED_DATE, '') != nvl(:NEW.CREATED_DATE, '') then
         APPEND_TO_CHANGED_FROM('<CreatedDate>' || to_char(:OLD.CREATED_DATE) || '</CreatedDate>');
         APPEND_TO_CHANGED_TO('<CreatedDate>' || to_char(:NEW.CREATED_DATE) || '</CreatedDate>');
      end if;
      if nvl(:OLD.UPDATED_BY, '') != nvl(:NEW.UPDATED_BY, '') then
         APPEND_TO_CHANGED_FROM('<UpdatedBy>' || :OLD.UPDATED_BY || '</UpdatedBy>');
         APPEND_TO_CHANGED_TO('<UpdatedBy>' || :NEW.UPDATED_BY || '</UpdatedBy>');
      end if;
      if nvl(:OLD.UPDATED_DATE, '') != nvl(:NEW.UPDATED_DATE, '') then
         APPEND_TO_CHANGED_FROM('<UpdatedDate>' || to_char(:OLD.UPDATED_DATE) || '</UpdatedDate>');
         APPEND_TO_CHANGED_TO('<UpdatedDate>' || to_char(:NEW.UPDATED_DATE) || '</UpdatedDate>');
      end if;

      if LENGTH(changed_from) > 0 then
         PROC_CREATE_AUDIT_TRAIL(29, 'UPDATE', :NEW.UPDATED_BY, to_char(:NEW.NOT_ID), changed_from, changed_to);
      end if;
   elsif deleting then
      PROC_CREATE_AUDIT_TRAIL(29, 'DELETE', :OLD.UPDATED_BY, to_char(:OLD.NOT_ID), changed_from, changed_to);
   end if;
end;
/