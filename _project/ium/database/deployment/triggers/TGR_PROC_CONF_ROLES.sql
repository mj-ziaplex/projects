create or replace trigger TGR_PROC_CONF_ROLES
	after insert or delete on PROCESS_CONFIGURATION_ROLES for each row

declare 
	cursor CURSOR_USER_ID (roleCode in VARCHAR2) is 
		select user_id from user_roles where role_code = roleCode;
	V_USER_ID VARCHAR2(10);
	V_EXISTING NUMBER;

begin 
	if inserting then
		open CURSOR_USER_ID (:new.ROLE_CODE);
		loop
			fetch CURSOR_USER_ID into V_USER_ID;
			exit when CURSOR_USER_ID%notfound;
			begin
				select count(*) into V_EXISTING from user_process_preferences where user_id = V_USER_ID and evnt_id = :new.EVNT_ID;
				if (V_EXISTING = 0 ) then 
					insert into user_process_preferences 
					values(V_USER_ID, :new.EVNT_ID, '1');
				end if;
			end;
		end loop;
		close CURSOR_USER_ID;
	
	elsif deleting then
		open CURSOR_USER_ID (:old.ROLE_CODE);
		loop
			fetch CURSOR_USER_ID into V_USER_ID;
			exit when CURSOR_USER_ID%notfound;
			begin
				delete from user_process_preferences where user_id = V_USER_ID and evnt_id = :old.EVNT_ID;
			end;
		end loop;
		close CURSOR_USER_ID;
	end if;
end;
/