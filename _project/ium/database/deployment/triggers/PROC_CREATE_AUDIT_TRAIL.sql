create or replace procedure PROC_CREATE_AUDIT_TRAIL
   (in_record       in number
   ,in_type         in varchar2
   ,in_userid       in varchar2
   ,in_refnum       in varchar2
   ,in_changed_from in varchar2
   ,in_changed_to   in varchar2) AS 
begin
   insert into AUDIT_TRAILS
   (TRAN_NUM, TRAN_RECORD, TRAN_TYPE, TRAN_DATETIME, USER_ID, UAR_REFERENCE_NUM, TRAN_CHANGED_FROM, TRAN_CHANGED_TO)
   values(SEQ_AUDIT_TRAIL.NEXTVAL, in_record, in_type, SYSDATE, in_userid, in_refnum, in_changed_from, in_changed_to);
end;
/