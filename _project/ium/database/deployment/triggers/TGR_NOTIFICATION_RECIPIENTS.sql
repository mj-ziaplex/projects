create or replace trigger TGR_NOTIFICATION_RECIPIENTS
	after insert on NOTIFICATION_RECIPIENTS for each row
	when (new.NOT_PRIMARY_IND != '1')

declare
	cursor CURSOR_USER_ID (roleCode in VARCHAR2) is
		select user_id from user_roles where role_code = roleCode;
	
	cursor CURSOR_EVENT_ID (notId in NUMBER, lobCode in VARCHAR2) is 
		select evnt_id from process_configurations where not_id = notId and lob_code = lobCode;

	V_EVENT_ID NUMBER;
	V_USER_ID VARCHAR2(10);
	V_EXISTING NUMBER;

begin
	if inserting then 
		open CURSOR_USER_ID(:new.ROLE_ID);
		loop
			fetch CURSOR_USER_ID into V_USER_ID;
			open CURSOR_EVENT_ID (:new.NOT_ID, :new.LOB_CODE);
			exit when CURSOR_USER_ID%notfound;
			begin	
				loop
					fetch CURSOR_EVENT_ID into V_EVENT_ID;
					exit when CURSOR_EVENT_ID%notfound;
					begin 
						select count(*) into V_EXISTING from user_process_preferences where user_id = V_USER_ID and evnt_id = V_EVENT_ID;
						if (V_EXISTING = 0 ) then
							insert into user_process_preferences (user_id, evnt_id,upp_notification_ind) 
							values (V_USER_ID, V_EVENT_ID, '1');
						end if;
					end;
				end loop;
				close CURSOR_EVENT_ID;
			end;
		end loop;
		close CURSOR_USER_ID;
	end if;
end;
/