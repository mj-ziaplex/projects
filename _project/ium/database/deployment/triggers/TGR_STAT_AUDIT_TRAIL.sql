create or replace trigger TGR_STAT_AUDIT_TRAIL
   after update or delete on STATUS for each row

declare
   changed_from	varchar2(2000);
   changed_to	varchar2(2000);

   procedure APPEND_TO_CHANGED_FROM(old_value varchar2) is
   begin
      if LENGTH(changed_from) = 0 then
         changed_from := '<?xml version="1.0" encoding="ISO-8859-1"?>';
      end if;
      changed_from := changed_from || old_value;
   end;

   procedure APPEND_TO_CHANGED_TO(new_value varchar2) is
   begin
      if LENGTH(changed_to) = 0 then
         changed_to := '<?xml version="1.0" encoding="ISO-8859-1"?>';
      end if;
      changed_to := changed_to || new_value;
   end;

begin
   if updating then
      if nvl(:OLD.STAT_ID, 0) != nvl(:NEW.STAT_ID, 0) then
         APPEND_TO_CHANGED_FROM('<StatusID>' || to_char(:OLD.STAT_ID) || '</StatusID>');
         APPEND_TO_CHANGED_TO('<StatusID>' || to_char(:NEW.STAT_ID) || '</StatusID>');
      end if;
      if nvl(:OLD.STAT_DESC, '') != nvl(:NEW.STAT_DESC, '') then
         APPEND_TO_CHANGED_FROM('<StatusDescription>' || :OLD.STAT_DESC || '</StatusDescription>');
         APPEND_TO_CHANGED_TO('<StatusDescription>' || :NEW.STAT_DESC || '</StatusDescription>');
      end if;
      if nvl(:OLD.STAT_TYPE, '') != nvl(:NEW.STAT_TYPE, '') then
         APPEND_TO_CHANGED_FROM('<StatusType>' || :OLD.STAT_TYPE || '</StatusType>');
         APPEND_TO_CHANGED_TO('<StatusType>' || :NEW.STAT_TYPE || '</StatusType>');
      end if;
      if nvl(:OLD.CREATED_BY, '') != nvl(:NEW.CREATED_BY, '') then
         APPEND_TO_CHANGED_FROM('<CreatedBy>' || :OLD.CREATED_BY || '</CreatedBy>');
         APPEND_TO_CHANGED_TO('<CreatedBy>' || :NEW.CREATED_BY || '</CreatedBy>');
      end if;
      if nvl(:OLD.CREATED_DATE, '') != nvl(:NEW.CREATED_DATE, '') then
         APPEND_TO_CHANGED_FROM('<CreatedDate>' || to_char(:OLD.CREATED_DATE) || '</CreatedDate>');
         APPEND_TO_CHANGED_TO('<CreatedDate>' || to_char(:NEW.CREATED_DATE) || '</CreatedDate>');
      end if;
      if nvl(:OLD.UPDATED_BY, '') != nvl(:NEW.UPDATED_BY, '') then
         APPEND_TO_CHANGED_FROM('<UpdatedBy>' || :OLD.UPDATED_BY || '</UpdatedBy>');
         APPEND_TO_CHANGED_TO('<UpdatedBy>' || :NEW.UPDATED_BY || '</UpdatedBy>');
      end if;
      if nvl(:OLD.UPDATED_DATE, '') != nvl(:NEW.UPDATED_DATE, '') then
         APPEND_TO_CHANGED_FROM('<UpdatedDate>' || to_char(:OLD.UPDATED_DATE) || '</UpdatedDate>');
         APPEND_TO_CHANGED_TO('<UpdatedDate>' || to_char(:NEW.UPDATED_DATE) || '</UpdatedDate>');
      end if;

      if LENGTH(changed_from) > 0 then
         PROC_CREATE_AUDIT_TRAIL(19, 'UPDATE', :NEW.UPDATED_BY, to_char(:NEW.STAT_ID), changed_from, changed_to);
      end if;
   elsif deleting then
      PROC_CREATE_AUDIT_TRAIL(19, 'DELETE', :OLD.UPDATED_BY, to_char(:OLD.STAT_ID), changed_from, changed_to);
   end if;
end;
/