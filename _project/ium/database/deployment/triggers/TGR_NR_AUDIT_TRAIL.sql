create or replace trigger TGR_NR_AUDIT_TRAIL 
   after update or delete on NOTIFICATION_RECIPIENTS for each row

declare
   changed_from	varchar2(2000);
   changed_to	varchar2(2000);

   procedure APPEND_TO_CHANGED_FROM(old_value varchar2) is
   begin
      if LENGTH(changed_from) = 0 then
         changed_from := '<?xml version="1.0" encoding="ISO-8859-1"?>';
      end if;
      changed_from := changed_from || old_value;
   end;

   procedure APPEND_TO_CHANGED_TO(new_value varchar2) is
   begin
      if LENGTH(changed_to) = 0 then
         changed_to := '<?xml version="1.0" encoding="ISO-8859-1"?>';
      end if;
      changed_to := changed_to || new_value;
   end;

begin
   if updating then
      if nvl(:OLD.NOT_ID, 0) != nvl(:NEW.NOT_ID, 0) then
         APPEND_TO_CHANGED_FROM('<Notification_ID>' || to_char(:OLD.NOT_ID) || '</Notification_ID>');
         APPEND_TO_CHANGED_TO('<Notification_ID>' || to_char(:NEW.NOT_ID) || '</Notification_ID>');
      end if;
      if nvl(:OLD.ROLE_ID, '') != nvl(:NEW.ROLE_ID, '') then
         APPEND_TO_CHANGED_FROM('<RoleID>' || :OLD.ROLE_ID || '</RoleID>');
         APPEND_TO_CHANGED_TO('<RoleID>' || :NEW.ROLE_ID || '</RoleID>');
      end if;
      if nvl(:OLD.LOB_CODE, '') != nvl(:NEW.LOB_CODE, '') then
         APPEND_TO_CHANGED_FROM('<LineOf Business>' || :OLD.LOB_CODE || '</LineOf Business>');
         APPEND_TO_CHANGED_TO('<LineOf Business>' || :NEW.LOB_CODE || '</LineOf Business>');
      end if;
      if nvl(:OLD.NOTIFICATION_TYPE, '') != nvl(:NEW.NOTIFICATION_TYPE, '') then
         APPEND_TO_CHANGED_FROM('<NotificationType>' || :OLD.NOTIFICATION_TYPE || '</NotificationType>');
         APPEND_TO_CHANGED_TO('<NotificationType>' || :NEW.NOTIFICATION_TYPE || '</NotificationType>');
      end if;
      if nvl(:OLD.CREATED_BY, '') != nvl(:NEW.CREATED_BY, '') then
         APPEND_TO_CHANGED_FROM('<CreatedBy>' || :OLD.CREATED_BY || '</CreatedBy>');
         APPEND_TO_CHANGED_TO('<CreatedBy>' || :NEW.CREATED_BY || '</CreatedBy>');
      end if;
      if nvl(:OLD.CREATED_DATE, '') != nvl(:NEW.CREATED_DATE, '') then
         APPEND_TO_CHANGED_FROM('<CreatedDate>' || to_char(:OLD.CREATED_DATE) || '</CreatedDate>');
         APPEND_TO_CHANGED_TO('<CreatedDate>' || to_char(:NEW.CREATED_DATE) || '</CreatedDate>');
      end if;
      if nvl(:OLD.UPDATED_BY, '') != nvl(:NEW.UPDATED_BY, '') then
         APPEND_TO_CHANGED_FROM('<UpdatedBy>' || :OLD.UPDATED_BY || '</UpdatedBy>');
         APPEND_TO_CHANGED_TO('<UpdatedBy>' || :NEW.UPDATED_BY || '</UpdatedBy>');
      end if;
      if nvl(:OLD.UPDATED_DATE, '') != nvl(:NEW.UPDATED_DATE, '') then
         APPEND_TO_CHANGED_FROM('<UpdatedDate>' || to_char(:OLD.UPDATED_DATE) || '</UpdatedDate>');
         APPEND_TO_CHANGED_TO('<UpdatedDate>' || to_char(:NEW.UPDATED_DATE) || '</UpdatedDate>');
      end if;

      if LENGTH(changed_from) > 0 then
         PROC_CREATE_AUDIT_TRAIL(29, 'UPDATE', :NEW.UPDATED_BY, to_char(:NEW.NOT_ID) || '-' || :NEW.ROLE_ID || '-' || :NEW.LOB_CODE, changed_from, changed_to);
      end if;
   elsif deleting then
      PROC_CREATE_AUDIT_TRAIL(29, 'DELETE', :OLD.UPDATED_BY, to_char(:OLD.NOT_ID) || '-' || :OLD.ROLE_ID || '-' || :OLD.LOB_CODE, changed_from, changed_to);
   end if;
end;
/