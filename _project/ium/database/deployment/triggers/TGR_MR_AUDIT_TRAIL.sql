create or replace trigger TGR_MR_AUDIT_TRAIL 
   after update on MEDICAL_RECORDS for each row

declare
   changed_from	varchar2(2000);
   changed_to	varchar2(2000);

   procedure APPEND_TO_CHANGED_FROM(old_value varchar2) is
   begin
      if LENGTH(changed_from) = 0 then
         changed_from := '<?xml version="1.0" encoding="ISO-8859-1"?>';
      end if;
      changed_from := changed_from || old_value;
   end;

   procedure APPEND_TO_CHANGED_TO(new_value varchar2) is
   begin
      if LENGTH(changed_to) = 0 then
         changed_to := '<?xml version="1.0" encoding="ISO-8859-1"?>';
      end if;
      changed_to := changed_to || new_value;
   end;

begin
      if nvl(:OLD.MED_RECORD_ID, 0) != nvl(:NEW.MED_RECORD_ID, 0) then
         APPEND_TO_CHANGED_FROM('<MedicalRecordID>' || to_char(:OLD.MED_RECORD_ID) || '</MedicalRecordID>');
         APPEND_TO_CHANGED_TO('<MedicalRecordID>' || to_char(:NEW.MED_RECORD_ID) || '</MedicalRecordID>');
      end if;
      if nvl(:OLD.CL_CLIENT_NUM, '') != nvl(:NEW.CL_CLIENT_NUM, '') then
         APPEND_TO_CHANGED_FROM('<ClientNumber>' || :OLD.CL_CLIENT_NUM || '</ClientNumber>');
         APPEND_TO_CHANGED_TO('<ClientNumber>' || :NEW.CL_CLIENT_NUM || '</ClientNumber>');
      end if;
      if nvl(:OLD.AGENT_ID, '') != nvl(:NEW.AGENT_ID, '') then
         APPEND_TO_CHANGED_FROM('<Agent>' || :OLD.AGENT_ID || '</Agent>');
         APPEND_TO_CHANGED_TO('<Agent>' || :NEW.AGENT_ID || '</Agent>');
      end if;
      if nvl(:OLD.BRANCH_ID, '') != nvl(:NEW.BRANCH_ID, '') then
         APPEND_TO_CHANGED_FROM('<Branch>' || :OLD.BRANCH_ID || '</Branch>');
         APPEND_TO_CHANGED_TO('<Branch>' || :NEW.BRANCH_ID || '</Branch>');
      end if;
      if nvl(:OLD.MED_STATUS, 0) != nvl(:NEW.MED_STATUS, 0) then
         APPEND_TO_CHANGED_FROM('<Status>' || to_char(:OLD.MED_STATUS) || '</Status>');
         APPEND_TO_CHANGED_TO('<Status>' || to_char(:NEW.MED_STATUS) || '</Status>');
      end if;
      if nvl(:OLD.MED_LAB_TEST_IND, '') != nvl(:NEW.MED_LAB_TEST_IND, '') then
         APPEND_TO_CHANGED_FROM('<LabTestIndicator>' || :OLD.MED_LAB_TEST_IND || '</LabTestIndicator>');
         APPEND_TO_CHANGED_TO('<LabTestIndicator>' || :NEW.MED_LAB_TEST_IND || '</LabTestIndicator>');
      end if;
      if nvl(:OLD.MED_LAB_ID, 0) != nvl(:NEW.MED_LAB_ID, 0) then
         APPEND_TO_CHANGED_FROM('<Laboratory>' || to_char(:OLD.MED_LAB_ID) || '</Laboratory>');
         APPEND_TO_CHANGED_TO('<Laboratory>' || to_char(:NEW.MED_LAB_ID) || '</Laboratory>');
      end if;
      if nvl(:OLD.MED_TEST_ID, 0) != nvl(:NEW.MED_TEST_ID, 0) then
         APPEND_TO_CHANGED_FROM('<TestCode>' || to_char(:OLD.MED_TEST_ID) || '</TestCode>');
         APPEND_TO_CHANGED_TO('<TestCode>' || to_char(:NEW.MED_TEST_ID) || '</TestCode>');
      end if;
      if nvl(:OLD.MED_DATE_CONDUCTED, '') != nvl(:NEW.MED_DATE_CONDUCTED, '') then
         APPEND_TO_CHANGED_FROM('<DateConducted>' || to_char(:OLD.MED_DATE_CONDUCTED) || '</DateConducted>');
         APPEND_TO_CHANGED_TO('<DateConducted>' || to_char(:NEW.MED_DATE_CONDUCTED) || '</DateConducted>');
      end if;
      if nvl(:OLD.MED_DATE_RECEIVED, '') != nvl(:NEW.MED_DATE_RECEIVED, '') then
         APPEND_TO_CHANGED_FROM('<DateReceived>' || to_char(:OLD.MED_DATE_RECEIVED) || '</DateReceived>');
         APPEND_TO_CHANGED_TO('<DateReceived>' || to_char(:NEW.MED_DATE_RECEIVED) || '</DateReceived>');
      end if;
      if nvl(:OLD.MED_VALIDITY_DATE, '') != nvl(:NEW.MED_VALIDITY_DATE, '') then
         APPEND_TO_CHANGED_FROM('<ValidityDate>' || to_char(:OLD.MED_VALIDITY_DATE) || '</ValidityDate>');
         APPEND_TO_CHANGED_TO('<ValidityDate>' || to_char(:NEW.MED_VALIDITY_DATE) || '</ValidityDate>');
      end if;
      if nvl(:OLD.MED_7DAY_MEMO_IND, '') != nvl(:NEW.MED_7DAY_MEMO_IND, '') then
         APPEND_TO_CHANGED_FROM('<SevenDayMemoIndicator>' || :OLD.MED_7DAY_MEMO_IND || '</SevenDayMemoIndicator>');
         APPEND_TO_CHANGED_TO('<SevenDayMemoIndicator>' || :NEW.MED_7DAY_MEMO_IND || '</SevenDayMemoIndicator>');
      end if;
      if nvl(:OLD.MED_7DAY_MEMO_DATE, '') != nvl(:NEW.MED_7DAY_MEMO_DATE, '') then
         APPEND_TO_CHANGED_FROM('<SevenDayMemoDate>' || to_char(:OLD.MED_7DAY_MEMO_DATE) || '</SevenDayMemoDate>');
         APPEND_TO_CHANGED_TO('<SevenDayMemoDate>' || to_char(:NEW.MED_7DAY_MEMO_DATE) || '</SevenDayMemoDate>');
      end if;
      if nvl(:OLD.MED_REQUESTING_PARTY, '') != nvl(:NEW.MED_REQUESTING_PARTY, '') then
         APPEND_TO_CHANGED_FROM('<RequestingParty>' || :OLD.MED_REQUESTING_PARTY || '</RequestingParty>');
         APPEND_TO_CHANGED_TO('<RequestingParty>' || :NEW.MED_REQUESTING_PARTY || '</RequestingParty>');
      end if;
      if nvl(:OLD.MED_DEPARTMENT_ID, '') != nvl(:NEW.MED_DEPARTMENT_ID, '') then
         APPEND_TO_CHANGED_FROM('<Department>' || :OLD.MED_DEPARTMENT_ID || '</Department>');
         APPEND_TO_CHANGED_TO('<Department>' || :NEW.MED_DEPARTMENT_ID || '</Department>');
      end if;
      if nvl(:OLD.MED_CHARGEABLE_TO, '') != nvl(:NEW.MED_CHARGEABLE_TO, '') then
         APPEND_TO_CHANGED_FROM('<ChargeableTo>' || :OLD.MED_CHARGEABLE_TO || '</ChargeableTo>');
         APPEND_TO_CHANGED_TO('<ChargeableTo>' || :NEW.MED_CHARGEABLE_TO || '</ChargeableTo>');
      end if;
      if nvl(:OLD.MED_PAID_BY_AGENT_IND, '') != nvl(:NEW.MED_PAID_BY_AGENT_IND, '') then
         APPEND_TO_CHANGED_FROM('<PaidByAgentIndicator>' || :OLD.MED_PAID_BY_AGENT_IND || '</PaidByAgentIndicator>');
         APPEND_TO_CHANGED_TO('<PaidByAgentIndicator>' || :NEW.MED_PAID_BY_AGENT_IND || '</PaidByAgentIndicator>');
      end if;
      if nvl(:OLD.MED_REQUEST_LOA_IND, '') != nvl(:NEW.MED_REQUEST_LOA_IND, '') then
         APPEND_TO_CHANGED_FROM('<RequestForLOAIndicator>' || :OLD.MED_REQUEST_LOA_IND || '</RequestForLOAIndicator>');
         APPEND_TO_CHANGED_TO('<RequestForLOAIndicator>' || :NEW.MED_REQUEST_LOA_IND || '</RequestForLOAIndicator>');
      end if;
      if nvl(:OLD.MED_RESULTS_RCVD, '') != nvl(:NEW.MED_RESULTS_RCVD, '') then
         APPEND_TO_CHANGED_FROM('<ResultsReceived>' || :OLD.MED_RESULTS_RCVD || '</ResultsReceived>');
         APPEND_TO_CHANGED_TO('<ResultsReceived>' || :NEW.MED_RESULTS_RCVD || '</ResultsReceived>');
      end if;
      if nvl(:OLD.CREATED_BY, '') != nvl(:NEW.CREATED_BY, '') then
         APPEND_TO_CHANGED_FROM('<CreatedBy>' || :OLD.CREATED_BY || '</CreatedBy>');
         APPEND_TO_CHANGED_TO('<CreatedBy>' || :NEW.CREATED_BY || '</CreatedBy>');
      end if;
      if nvl(:OLD.CREATED_DATE, '') != nvl(:NEW.CREATED_DATE, '') then
         APPEND_TO_CHANGED_FROM('<CreatedDate>' || to_char(:OLD.CREATED_DATE) || '</CreatedDate>');
         APPEND_TO_CHANGED_TO('<CreatedDate>' || to_char(:NEW.CREATED_DATE) || '</CreatedDate>');
      end if;
      if nvl(:OLD.UPDATED_BY, '') != nvl(:NEW.UPDATED_BY, '') then
         APPEND_TO_CHANGED_FROM('<UpdatedBy>' || :OLD.UPDATED_BY || '</UpdatedBy>');
         APPEND_TO_CHANGED_TO('<UpdatedBy>' || :NEW.UPDATED_BY || '</UpdatedBy>');
      end if;
      if nvl(:OLD.UPDATED_DATE, '') != nvl(:NEW.UPDATED_DATE, '') then
         APPEND_TO_CHANGED_FROM('<UpdatedDate>' || to_char(:OLD.UPDATED_DATE) || '</UpdatedDate>');
         APPEND_TO_CHANGED_TO('<UpdatedDate>' || to_char(:NEW.UPDATED_DATE) || '</UpdatedDate>');
      end if;

      if LENGTH(changed_from) > 0 then
         PROC_CREATE_AUDIT_TRAIL(4, 'UPDATE', :NEW.UPDATED_BY, to_char(:NEW.MED_RECORD_ID), changed_from, changed_to);
      end if;
end;
/