create or replace trigger TGR_INSERT_USER_ROLES after
	insert on USER_ROLES for each row

declare
	cursor CURSOR_EVENT_ID (roleCode in VARCHAR2) is
		select evnt_id from process_configuration_roles where role_code = roleCode;
	V_EXISTING NUMBER;
	V_EVENT_ID NUMBER;
	
begin
	if inserting then
		open CURSOR_EVENT_ID (:new.ROLE_CODE);
		loop
			fetch CURSOR_EVENT_ID into V_EVENT_ID;
			exit when CURSOR_EVENT_ID%notfound;
			begin
				select count(*) into V_EXISTING from user_process_preferences where user_id = :new.USER_ID and evnt_id = V_EVENT_ID;
				if (V_EXISTING = 0) then
					insert into USER_PROCESS_PREFERENCES (USER_ID, EVNT_ID, UPP_NOTIFICATION_IND) values (:new.USER_ID, V_EVENT_ID, '1');
				end if;
			end; 
		end loop;
		close CURSOR_EVENT_ID;
	end if;
end;
/