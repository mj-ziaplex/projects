CREATE TABLE purge_statistics
(
  purge_date 		TIMESTAMP NOT NULL
, start_date 		DATE
, end_date 			DATE
, criteria	 		VARCHAR2(20)
, number_of_records NUMBER
, result			CHAR(1)
);


-- Public synonym
CREATE PUBLIC SYNONYM purge_statistics FOR purge_statistics;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON purge_statistics TO ium_app_user;
