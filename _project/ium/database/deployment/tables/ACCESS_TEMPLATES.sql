CREATE TABLE access_templates
(
  templt_id 		NUMBER(5) NOT NULL
, role_id    		VARCHAR2(15)
, templt_desc 		VARCHAR2(25)
, created_by		VARCHAR2(10)
, created_date		TIMESTAMP(6)
, updated_by		VARCHAR2(10)
, updated_date		TIMESTAMP(6)
);

-- Public synonym
CREATE PUBLIC SYNONYM access_templates FOR access_templates;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON access_templates TO ium_app_user;
 
