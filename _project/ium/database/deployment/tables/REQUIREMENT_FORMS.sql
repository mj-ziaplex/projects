CREATE TABLE requirement_forms
(
  rf_id 		NUMBER(5) NOT NULL
, rf_name 		VARCHAR2(110)
, rf_template_format 	VARCHAR2(3)
, rf_template_name 	VARCHAR2(100)
, rf_status 		VARCHAR2(1)
, created_by	VARCHAR2(10)
, created_date	TIMESTAMP(6)
, updated_by	VARCHAR2(10)
, updated_date	TIMESTAMP(6)
); 

-- Public synonym
CREATE PUBLIC SYNONYM requirement_forms FOR requirement_forms;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON requirement_forms TO ium_app_user;
	