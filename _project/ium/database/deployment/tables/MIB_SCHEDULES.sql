CREATE TABLE mib_schedules  
(
  mib_sked_id 		NUMBER(3) NOT NULL
, mib_sked_frequency 	VARCHAR2(10)
, mib_sked_day 		VARCHAR2(10)
, mib_sked_time 	TIMESTAMP
, mib_sked_status	VARCHAR2(10)
);

-- Public synonym
CREATE PUBLIC SYNONYM mib_schedules FOR mib_schedules;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON mib_schedules TO ium_app_user;
	