CREATE TABLE medical_bills
(
  mb_bill_number	NUMBER(8) NOT NULL
, uar_reference_num 	VARCHAR2(15)
, exmnr_id 		NUMBER(5)
, lab_id 		NUMBER(5)
, mb_fee		NUMBER(8,2)
, mb_date_posted	TIMESTAMP
, created_by	VARCHAR2(10)
, created_date	TIMESTAMP(6)
, updated_by	VARCHAR2(10)
, updated_date	TIMESTAMP(6)
); 

-- Public synonym
CREATE PUBLIC SYNONYM medical_bills FOR medical_bills;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON medical_bills TO ium_app_user;
