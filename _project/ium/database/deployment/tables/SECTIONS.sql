CREATE TABLE sections
(
  sec_code	VARCHAR2(15) NOT NULL
, sec_desc 	VARCHAR2(50)
, created_by	VARCHAR2(10)
, created_date	TIMESTAMP(6)
, updated_by	VARCHAR2(10)
, updated_date	TIMESTAMP(6)
); 

-- Public synonym
CREATE PUBLIC SYNONYM sections FOR sections;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON sections TO ium_app_user;
 