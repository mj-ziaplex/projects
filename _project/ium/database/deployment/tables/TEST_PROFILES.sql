CREATE TABLE test_profiles 
(
  test_id	NUMBER(5) NOT NULL
, test_desc 	VARCHAR2(40)
, test_validity	NUMBER(5)
, test_taxable_ind VARCHAR2(1)
, test_type		VARCHAR2(1)
, created_by	VARCHAR2(10)
, created_date	TIMESTAMP(6)
, updated_by	VARCHAR2(10)
, updated_date	TIMESTAMP(6)
, test_follow_up_num number(2)
); 

-- Public synonym
CREATE PUBLIC SYNONYM test_profiles FOR test_profiles;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON test_profiles TO ium_app_user;
 