CREATE TABLE status 
(
  stat_id 	NUMBER(4) NOT NULL
, stat_type VARCHAR2(2)
, stat_desc VARCHAR2(40)
, created_by	VARCHAR2(10)
, created_date	TIMESTAMP(6)
, updated_by	VARCHAR2(10)
, updated_date	TIMESTAMP(6)
);

-- Public synonym
CREATE PUBLIC SYNONYM status FOR status;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON status TO ium_app_user;
 