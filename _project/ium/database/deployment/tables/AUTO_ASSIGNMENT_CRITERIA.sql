CREATE TABLE auto_assignment_criteria
(
  aac_id 				NUMBER(3) NOT NULL
, aac_field 			VARCHAR2(20)
, aac_desc 				VARCHAR2(40)
, aac_status			VARCHAR2(1)
, created_by			VARCHAR2(10)
, created_date			TIMESTAMP(6)
, updated_by			VARCHAR2(10)
, updated_date			TIMESTAMP(6)
);


-- Public synonym
CREATE PUBLIC SYNONYM auto_assignment_criteria FOR auto_assignment_criteria;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON auto_assignment_criteria TO ium_app_user;

