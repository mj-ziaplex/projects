CREATE TABLE EXCEPTION_LOG
(
  EXCEPTION_ID     NUMBER(8) NOT NULL
, BATCH_ID         NUMBER(8)
, REFERENCE_NUM    VARCHAR2(15)
, XML_RECORD       LONG
);

-- Public synonym
CREATE PUBLIC SYNONYM exception_log FOR exception_log;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON exception_log TO ium_app_user;
		