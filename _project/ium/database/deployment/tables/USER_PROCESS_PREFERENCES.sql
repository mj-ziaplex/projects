CREATE TABLE user_process_preferences
(
  user_id			VARCHAR2(10) NOT NULL
, evnt_id			NUMBER(5) NOT NULL
, upp_notification_ind VARCHAR2(1)
);

-- Public synonym
CREATE PUBLIC SYNONYM user_process_preferences FOR user_process_preferences;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON user_process_preferences TO ium_app_user;
