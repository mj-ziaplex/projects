CREATE TABLE examiner_specializations 
(
  exmnr_id 		NUMBER(5) NOT NULL
, spl_id 		NUMBER(3) NOT NULL
, spl_effective_date 	DATE
, created_by        VARCHAR2(10)
, created_date      TIMESTAMP(6)
, updated_by        VARCHAR2(10)
, updated_date      TIMESTAMP(6)
);

-- Public synonym
CREATE PUBLIC SYNONYM examiner_specializations FOR examiner_specializations;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON examiner_specializations TO ium_app_user;
 