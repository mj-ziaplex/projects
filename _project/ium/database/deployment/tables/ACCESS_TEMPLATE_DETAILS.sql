CREATE TABLE access_template_details
(
  templt_id			NUMBER(5) NOT NULL
, pa_id				NUMBER(5) NOT NULL
, atd_access_ind	VARCHAR2(1)
, created_by		VARCHAR2(10)
, created_date		TIMESTAMP(6)
, updated_by		VARCHAR2(10)
, updated_date		TIMESTAMP(6)
);

-- Public synonym
CREATE PUBLIC SYNONYM access_template_details FOR access_template_details;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON access_template_details TO ium_app_user;
