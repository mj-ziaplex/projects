CREATE TABLE CODEC_KO_CODEC
(
 PC_SEQ		NUMBER(4) NOT NULL
,KO_SEQ		NUMBER(5) NOT NULL
);


-- Public synonym
CREATE PUBLIC SYNONYM codec_ko_codec FOR codec_ko_codec;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON codec_ko_codec TO ium_app_user;

