CREATE TABLE notification_templates 
(
  not_id 		NUMBER(5) NOT NULL
, not_subj		VARCHAR2(100)
, not_desc 		VARCHAR2(40)
, not_email_content 	LONG
, not_sender		VARCHAR2(6)
, not_notify_mobile_ind VARCHAR2(1)
, not_mobile_content 	VARCHAR2(500)
, created_by 		VARCHAR2(10) 
, created_date 		TIMESTAMP
, updated_by 		VARCHAR2(10)
, updated_date 		TIMESTAMP
);

-- Public synonym
CREATE PUBLIC SYNONYM notification_templates FOR notification_templates;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON notification_templates TO ium_app_user;
