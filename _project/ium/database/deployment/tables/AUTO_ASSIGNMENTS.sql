CREATE TABLE auto_assignments 
(
  aa_id 				NUMBER(3) NOT NULL
, user_id 				VARCHAR2(10)
, aac_id 				NUMBER(3)
, criteria_field_value 	VARCHAR2(25)
, created_by			VARCHAR2(10)
, created_date			TIMESTAMP(6)
, updated_by			VARCHAR2(10)
, updated_date			TIMESTAMP(6)
);



-- Public synonym
CREATE PUBLIC SYNONYM auto_assignments FOR auto_assignments;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON auto_assignments TO ium_app_user;

