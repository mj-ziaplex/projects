CREATE TABLE lob_requirements
(
  lob_code			VARCHAR2(2) NOT NULL
, reqt_code			VARCHAR2(5) NOT NULL
, remarks			LONG
, created_by	VARCHAR2(10)
, created_date	TIMESTAMP(6)
, updated_by	VARCHAR2(10)
, updated_date	TIMESTAMP(6)
); 

-- Public synonym
CREATE PUBLIC SYNONYM lob_requirements FOR lob_requirements;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON lob_requirements TO ium_app_user;
