CREATE TABLE user_roles
(  role_code                      VARCHAR2(15) NOT NULL
 , user_id                        VARCHAR2(10) NOT NULL
);

-- Public synonym
CREATE PUBLIC SYNONYM user_roles FOR user_roles;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON user_roles TO ium_app_user;
