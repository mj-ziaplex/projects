CREATE TABLE examination_areas
(
  ea_id 		NUMBER(3)
, ea_desc 		VARCHAR2(25)
, created_by 	VARCHAR2(10)
, created_date	TIMESTAMP(6)
, updated_by	VARCHAR2(10)
, updated_date	TIMESTAMP(6)
);


-- Public synonym
CREATE PUBLIC SYNONYM examination_areas FOR examination_areas;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON examination_areas TO ium_app_user;




