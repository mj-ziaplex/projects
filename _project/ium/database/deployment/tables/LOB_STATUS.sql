CREATE TABLE lob_status 
(
  lob_code	VARCHAR2(2) NOT NULL
, admin_status_code VARCHAR2(5) 
, admin_status_type VARCHAR2(1) 
, stat_id	NUMBER(4) NOT NULL
, created_by	VARCHAR2(10)
, created_date	TIMESTAMP(6)
, updated_by	VARCHAR2(10)
, updated_date	TIMESTAMP(6)
);

-- Public synonym
CREATE PUBLIC SYNONYM lob_status FOR lob_status;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON lob_status TO ium_app_user;
 