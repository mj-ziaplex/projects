CREATE TABLE audit_trails
(
  tran_num 			NUMBER(10) NOT NULL
, tran_record 		NUMBER(2)
, tran_type 		VARCHAR2(6)
, tran_datetime 	TIMESTAMP(6)
, user_id 			VARCHAR2(10)
, uar_reference_num VARCHAR2(30)
, tran_changed_from VARCHAR2(2000)
, tran_changed_to 	VARCHAR2(2000)
);


-- Public synonym
CREATE PUBLIC SYNONYM audit_trails FOR audit_trails;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON audit_trails TO ium_app_user;

