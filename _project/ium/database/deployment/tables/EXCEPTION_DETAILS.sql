CREATE TABLE EXCEPTION_DETAILS
(
  EXCEPTION_ID     NUMBER(8)
, MESSAGE          VARCHAR2(50)
);

-- Public synonym
CREATE PUBLIC SYNONYM exception_details FOR exception_details;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON exception_details TO ium_app_user;

