CREATE TABLE pages 
(
  page_id 		NUMBER(2) NOT NULL
, page_desc 		VARCHAR2(25)
, created_by	VARCHAR2(10)
, created_date	TIMESTAMP(6)
, updated_by	VARCHAR2(10)
, updated_date	TIMESTAMP(6)
); 

-- Public synonym
CREATE PUBLIC SYNONYM pages FOR pages;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON pages TO ium_app_user;
