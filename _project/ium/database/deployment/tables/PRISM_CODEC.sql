CREATE TABLE PRISM_CODEC
(
  PC_SEQ		NUMBER(4) NOT NULL
, PC_POSITION	NUMBER(2)
, PC_CODE		VARCHAR2(1)
, PC_DESC		VARCHAR2(100)
);

-- Public synonym
CREATE PUBLIC SYNONYM prism_codec FOR prism_codec;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON prism_codec TO ium_app_user;

