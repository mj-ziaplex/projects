CREATE TABLE examination_places 
(
  ep_id 		NUMERIC(3) NOT NULL
, ep_desc 		VARCHAR2(25)
, created_by	VARCHAR2(10)
, created_date	TIMESTAMP(6)
, updated_by	VARCHAR2(10)
, updated_date	TIMESTAMP(6)
);


-- Public synonym
CREATE PUBLIC SYNONYM examination_places FOR examination_places;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON examination_places TO ium_app_user;
