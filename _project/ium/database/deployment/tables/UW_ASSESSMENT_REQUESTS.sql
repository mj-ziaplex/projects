CREATE TABLE UW_ASSESSMENT_REQUESTS
(
  uw_analysis_id NUMBER
, reference_num	VARCHAR2(15)
, uw_analysis	LONG
, posted_by    VARCHAR2(10)
, posted_date  TIMESTAMP(6)
);

-- Public synonym
CREATE PUBLIC SYNONYM uw_assessment_requests FOR uw_assessment_requests;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON uw_assessment_requests TO ium_app_user;
