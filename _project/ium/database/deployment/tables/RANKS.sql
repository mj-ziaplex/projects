CREATE TABLE ranks 
(
  rank_id 	NUMBER(3) NOT NULL
, rank_desc 	VARCHAR2(40)
, rank_fee 	NUMERIC(10,2)
, created_by	VARCHAR2(10)
, created_date	TIMESTAMP(6)
, updated_by	VARCHAR2(10)
, updated_date	TIMESTAMP(6)
);

-- Public synonym
CREATE PUBLIC SYNONYM ranks FOR ranks;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON ranks TO ium_app_user;
