CREATE TABLE client_types 
(
  clt_code 		VARCHAR2(1) NOT NULL
, clt_desc 		VARCHAR2(25)
, created_by	VARCHAR2(10)
, created_date	TIMESTAMP(6)
, updated_by	VARCHAR2(10)
, updated_date	TIMESTAMP(6)
);



-- Public synonym
CREATE PUBLIC SYNONYM client_types FOR client_types;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON client_types TO ium_app_user;
