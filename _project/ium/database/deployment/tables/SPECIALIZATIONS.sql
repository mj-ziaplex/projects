CREATE TABLE specializations 
(
  spl_id 		NUMBER(3) NOT NULL
, spl_desc 		VARCHAR2(40)
, created_by	VARCHAR2(10)
, created_date	TIMESTAMP(6)
, updated_by	VARCHAR2(10)
, updated_date	TIMESTAMP(6)
);

-- Public synonym
CREATE PUBLIC SYNONYM specializations FOR specializations;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON specializations TO ium_app_user;


