CREATE TABLE process_configurations 
(
  evnt_id		NUMBER(5) NOT NULL
, proc_code 		VARCHAR2(4)
, evnt_name		VARCHAR2(25)
, evnt_from_status	NUMBER(4)
, evnt_to_status	NUMBER(4)
, lob_code 		VARCHAR2(2)
, not_id 		NUMBER(3)
, proc_notification_ind VARCHAR2(1)
, created_by 		VARCHAR2(10)
, created_date 		TIMESTAMP
, updated_by 		VARCHAR2(10)
, updated_date 		TIMESTAMP
)
;
-- Public synonym
CREATE PUBLIC SYNONYM process_configurations FOR process_configurations;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON process_configurations TO ium_app_user;
 