CREATE TABLE notification_recipients 
(
  not_id 		NUMBER(5) NOT NULL
, role_id 		VARCHAR2(15) NOT NULL
, lob_code      VARCHAR2(2) NOT NULL
, notification_type 	VARCHAR2(1)
, not_primary_ind		VARCHAR2(1)
, created_by	VARCHAR2(10)
, created_date	TIMESTAMP(6)
, updated_by	VARCHAR2(10)
, updated_date	TIMESTAMP(6)
);



-- Public synonym
CREATE PUBLIC SYNONYM notification_recipients FOR notification_recipients;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON notification_recipients TO ium_app_user;
