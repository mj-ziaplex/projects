CREATE TABLE REQUIREMENT_FORM_FIELDS
(
  RFF_ID                 NUMBER(5) not null
, RF_ID                  NUMBER(5) not null
, FF_ID                  NUMBER(2) not null
, RFF_PAGE               NUMBER(1)
, RFF_POSX               NUMBER(3)
, RFF_POSY               NUMBER(3)
);

 
-- Public synonym
CREATE PUBLIC SYNONYM requirement_form_fields FOR requirement_form_fields;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON requirement_form_fields TO ium_app_user;
 