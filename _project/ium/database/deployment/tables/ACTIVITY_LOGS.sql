CREATE TABLE activity_logs 
(
  act_id			NUMBER(10) NOT NULL
, uar_reference_num	VARCHAR2(15) 
, stat_id 			NUMBER(4)
, act_datetime 		TIMESTAMP(6)
, user_id 			VARCHAR2(10)
, act_assigned_to 	VARCHAR2(10)
, act_elapse_time 	NUMBER(6,2)
); 

-- Public synonym
CREATE PUBLIC SYNONYM activity_logs FOR activity_logs;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON activity_logs TO ium_app_user;


