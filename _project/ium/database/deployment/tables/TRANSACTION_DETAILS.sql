Create table transaction_details(
	 impairment_id number(5) not null
	,control_no varchar2(15) not null
	,user_cd	varchar2(10)
	,company_cd varchar2(1)
	,tran_cd	varchar2(25)
	,tran_dt	date
	,record_affected	number(1)
	,no_Of_Hit			number(1)
);

-- Public synonym
CREATE PUBLIC SYNONYM transaction_details FOR transaction_details;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON transaction_details TO ium_app_user;



