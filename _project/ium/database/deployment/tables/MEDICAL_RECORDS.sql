CREATE TABLE medical_records
(
  med_record_id 	NUMBER(8) NOT NULL
, cl_client_num 	VARCHAR2(10)
, agent_id 		VARCHAR2(8) 
, branch_id 		VARCHAR2(15)
, med_status 		NUMBER(4)
, med_lab_test_ind 	VARCHAR2(1)
, med_examination_place NUMBER(3)
, med_lab_id 		NUMBER(5)
, med_test_id 		NUMBER(5)
, exmnr_id 			NUMBER(5)          
, med_appointment_date 	TIMESTAMP
, med_date_conducted 	DATE
, med_date_received 	DATE
, med_follow_up_num 	NUMERIC(2)
, med_follow_up_date 	DATE
, med_remarks 		LONG
, med_validity_date 	DATE
, med_7day_memo_ind 	VARCHAR2(1)
, med_7day_memo_date 	DATE
, med_reqt_reason 	VARCHAR2(100)
, med_section_id 	VARCHAR2(15)
, med_requesting_party 	VARCHAR2(5)
, med_department_id 	VARCHAR2(10)
, med_chargeable_to 	VARCHAR2(10)
, med_paid_by_agent_ind VARCHAR2(1)
, med_amount 		NUMBER(8)
, med_request_date	DATE
, med_request_loa_ind 	VARCHAR2(1)
, med_results_rcvd 	VARCHAR2(1)
, created_by 		VARCHAR2(10) 
, created_date 		TIMESTAMP
, updated_by 		VARCHAR2(10)
, updated_date 		TIMESTAMP
, cl_last_name		VARCHAR2(40)
, cl_given_name		VARCHAR2(25)
, cl_birth_date		DATE
); 

-- Public synonym
CREATE PUBLIC SYNONYM medical_records FOR medical_records;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON medical_records TO ium_app_user;
