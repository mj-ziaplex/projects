CREATE TABLE impairments 
(
  imp_impairement_id	NUMBER(5) NOT NULL
, cl_client_num		VARCHAR2(10)
, uar_reference_num 	VARCHAR2(15)
, mib_impairment_code   VARCHAR2(6)
, imp_desc 		VARCHAR2(25)
, imp_relationship	VARCHAR2(1)
, imp_origin		VARCHAR2(1)
, imp_date 		DATE
, imp_height_feet 	NUMBER(4,2)
, imp_height_inches 	NUMBER(5,2)
, imp_weight 		NUMBER(5,2)
, imp_blood_pressure	VARCHAR2(7)
, imp_confirmation 	VARCHAR2(1)
, imp_export_date 	DATE
, created_by 		VARCHAR2(10) 
, created_date 		TIMESTAMP
, updated_by 		VARCHAR2(10)
, updated_date 		TIMESTAMP
);

-- Public synonym
CREATE PUBLIC SYNONYM impairments FOR impairments;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON impairments TO ium_app_user;
