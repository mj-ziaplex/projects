CREATE TABLE exception_batch
(
 BATCH_ID          NUMBER(8) NOT NULL
,TIMESTAMP         DATE
,INTERFACE         VARCHAR2(10)
,RECORD_TYPE       VARCHAR2(30)
,REQUEST           VARCHAR2(160)
,RESPONSE          LONG
);


-- Public synonym
CREATE PUBLIC SYNONYM exception_batch FOR exception_batch;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON exception_batch TO ium_app_user;
