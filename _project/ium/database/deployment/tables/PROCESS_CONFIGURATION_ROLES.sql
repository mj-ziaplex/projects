CREATE TABLE process_configuration_roles
(
  evnt_id 	NUMBER(5) NOT NULL
, role_code VARCHAR2(15) NOT NULL
, lob_code  VARCHAR2(2) NOT NULL
);


	
-- Public synonym
CREATE PUBLIC SYNONYM process_configuration_roles FOR process_configuration_roles;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON process_configuration_roles TO ium_app_user;
	