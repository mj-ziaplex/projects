CREATE TABLE folder_documents 
(
  doc_id		NUMBER(8) NOT NULL
, uar_reference_num 	VARCHAR2(15)
, doc_code 		VARCHAR2(3)
, doc_date_rcvd 	DATE
, doc_received_by 	VARCHAR2(10)
, doc_reference_num 	NUMBER(8)
, doc_date_requested	DATE
); 

-- Public synonym
CREATE PUBLIC SYNONYM folder_documents FOR folder_documents;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON folder_documents TO ium_app_user;
 