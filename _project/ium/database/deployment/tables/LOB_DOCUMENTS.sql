CREATE TABLE lob_documents 
(
  lob_code 		VARCHAR2(2) NOT NULL
, doc_code 		VARCHAR2(3) NOT NULL
, created_by	VARCHAR2(10)
, created_date	TIMESTAMP(6)
, updated_by	VARCHAR2(10)
, updated_date	TIMESTAMP(6)
);


-- Public synonym
CREATE PUBLIC SYNONYM lob_documents FOR lob_documents;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON lob_documents TO ium_app_user;
