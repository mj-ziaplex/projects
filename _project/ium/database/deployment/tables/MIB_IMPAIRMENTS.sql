CREATE TABLE mib_impairments
(
  mib_impairment_code	VARCHAR2(6) NOT NULL
, mib_impairment_desc	VARCHAR2(104)
, created_by	VARCHAR2(10)
, created_date	TIMESTAMP(6)
, updated_by	VARCHAR2(10)
, updated_date	TIMESTAMP(6)
);

-- Public synonym
CREATE PUBLIC SYNONYM mib_impairments FOR mib_impairments;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON mib_impairments TO ium_app_user;

