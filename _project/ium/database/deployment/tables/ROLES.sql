CREATE TABLE roles 
(
  role_code	VARCHAR2(15) NOT NULL
, role_desc 	VARCHAR2(40)
, created_by	VARCHAR2(10)
, created_date	TIMESTAMP(6)
, updated_by	VARCHAR2(10)
, updated_date	TIMESTAMP(6)
);

-- Public synonym
CREATE PUBLIC SYNONYM roles FOR roles;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON roles TO ium_app_user;
