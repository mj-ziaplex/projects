CREATE TABLE job_schedules
(
  sched_type	VARCHAR2(2) NOT NULL
, sched_desc     VARCHAR2(100)
, sched_time     TIMESTAMP
, created_by	VARCHAR2(10)
, created_date	TIMESTAMP(6)
, updated_by	VARCHAR2(10)
, updated_date	TIMESTAMP(6)
);


-- Public synonym
CREATE PUBLIC SYNONYM job_schedules FOR job_schedules;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON job_schedules TO ium_app_user;
