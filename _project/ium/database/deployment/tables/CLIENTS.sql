CREATE TABLE clients 
(
  client_id 		VARCHAR2(10) NOT NULL
, cl_last_name 		VARCHAR2(40)
, cl_given_name 	VARCHAR2(25)
, cl_middle_name 	VARCHAR2(25)
, cl_oth_last_name 	VARCHAR2(40)
, cl_oth_given_name 	VARCHAR2(25)
, cl_oth_middle_name 	VARCHAR2(25)
, cl_title 		VARCHAR2(15)
, cl_suffix 		VARCHAR2(10)
, cl_age 		NUMBER(2)
, cl_sex 		VARCHAR2(1)
, cl_birth_date 	DATE
, cl_smoker 		VARCHAR2(1)
);


  
-- Public synonym
CREATE PUBLIC SYNONYM clients FOR clients;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON clients TO ium_app_user;
  