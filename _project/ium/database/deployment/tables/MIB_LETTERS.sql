CREATE TABLE mib_letters 
(
  mib_lttr_code 	VARCHAR2(1) NOT NULL
, mib_lttr_desc 	VARCHAR2(40)
, created_by	VARCHAR2(10)
, created_date	TIMESTAMP(6)
, updated_by	VARCHAR2(10)
, updated_date	TIMESTAMP(6)
); 

-- Public synonym
CREATE PUBLIC SYNONYM mib_letters FOR mib_letters;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON mib_letters TO ium_app_user;
