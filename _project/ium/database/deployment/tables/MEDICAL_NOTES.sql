CREATE TABLE medical_notes
(
  mn_notes_id		NUMBER(8) NOT NULL
, uar_reference_num 	VARCHAR2(15)
, mn_posted_by 		VARCHAR2(10)
, mn_post_date 		DATE
, mn_notes  		LONG
, mn_recipient		VARCHAR2(10)
); 

-- Public synonym
CREATE PUBLIC SYNONYM medical_notes FOR medical_notes;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON medical_notes TO ium_app_user;
