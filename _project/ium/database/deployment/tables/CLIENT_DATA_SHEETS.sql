CREATE TABLE client_data_sheets 
(
  cds_reference_num 		VARCHAR2(15) NOT NULL
, cds_client_num 		VARCHAR2(10) NOT NULL
, cds_client_type 		VARCHAR2(1)
, cds_cl_height_feet 		NUMBER(1)
, cds_cl_height_inches 		NUMBER(2)
, cds_cl_weight 		NUMBER(3)
, cds_basic_mr 			NUMBER(4,1)
, cds_basic_ccr_mr 		NUMBER(4,1)
, cds_basic_apdp_mr 		NUMBER(4,1)
, cds_smkng_basic_mr 		NUMBER(4,1)
, cds_smkng_ccr_mr 		NUMBER(4,1)
, cds_smkng_apdb_mr 		NUMBER(4,1)
, cds_fhb_basic_mr 		NUMBER(4,1)
, cds_fhb_ccr_mr 		NUMBER(4,1)
, cds_fhb_apdb_mr 		NUMBER(4,1)
, cds_tam_basic_mr 		NUMBER(4,1)
, cds_tam_ccr_mr 		NUMBER(4,1)
, cds_tam_apdb_mr 		NUMBER(4,1)
, cds_app_br_amt        NUMBER(13,5)
, cds_app_ad_amt        NUMBER(13,5)
, cds_pend_br_amt       NUMBER(13,5)
, cds_pend_ad_amt       NUMBER(13,5)
, cds_infc_br_amt       NUMBER(13,5)
, cds_infc_ad_amt       NUMBER(13,5)
, cds_laps_br_amt       NUMBER(13,5)
, cds_laps_ad_amt       NUMBER(13,5)
, cds_oins_br_amt       NUMBER(13,5)
, cds_oins_ad_amt       NUMBER(13,5)
, cds_infc_ccr_amt      NUMBER(13,5)
, cds_infc_apdb_amt     NUMBER(13,5)
, cds_infc_hib_amt      NUMBER(13,5)
, cds_infc_fmb_amt      NUMBER(13,5)
, cds_tot_other_companies 	NUMBER(13,2)
, cds_tot_ccr_coverage 		NUMBER(13,2)
, cds_tot_apdb_coverage 	NUMBER(13,2)
, cds_tot_hib_coverage 		NUMBER(13,2)
, cds_tot_fbbmb_coverage 	NUMBER(13,2)
, cds_tot_reinsured_amount 	NUMBER(13,2)
, cds_tot_reins_amt			NUMBER(13,5)
, cds_tot_existing_sl_br    NUMBER(13,5)
, cds_tot_existing_sl_ad	NUMBER(13,5)
, cds_tot_sl_oins_br		NUMBER(13,5)
, cds_tot_sl_oins_ad		NUMBER(13,5)
);


-- Public synonym
CREATE PUBLIC SYNONYM client_data_sheets FOR client_data_sheets;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON client_data_sheets TO ium_app_user;
