CREATE TABLE CODEC_REQUIREMENTS
(
  PC_SEQ		NUMBER(4) NOT NULL
, REQT_CODE		VARCHAR2(5) NOT NULL
);


-- Public synonym
CREATE PUBLIC SYNONYM codec_requirements FOR codec_requirements;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON codec_requirements TO ium_app_user;

