CREATE TABLE assessment_requests
(
  reference_num 	VARCHAR2(15) NOT NULL
, lob				VARCHAR2(2)  
, source_system 	VARCHAR2(10)
, amount_covered 	NUMBER(12,2)
, premium			NUMBER(12,2)
, insured_client_id VARCHAR2(10)
, owner_client_id 	VARCHAR2(10)
, branch_id 		VARCHAR2(15) 
, agent_id			VARCHAR2(8)
, status_id 		NUMBER(4)
, status_date 		DATE
, assigned_to 		VARCHAR2(10)
, underwriter 		VARCHAR2(10)
, folder_location 	VARCHAR2(10)
, remarks			VARCHAR2(255)
, app_received_date DATE
, date_forwarded 	DATE
, ar_transmit_ind	VARCHAR2(1)
, created_by 		VARCHAR2(10) 
, created_date 		TIMESTAMP(6)
, updated_by 		VARCHAR2(10)
, updated_date 		TIMESTAMP(6)
, uw_remarks		LONG
, ar_auto_settle_ind VARCHAR2(1)
);


-- Public synonym
CREATE PUBLIC SYNONYM assessment_requests FOR assessment_requests;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON assessment_requests TO ium_app_user;
