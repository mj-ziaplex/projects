CREATE TABLE user_page_access
(
  user_id 			VARCHAR2(10) NOT NULL
, page_id 			NUMBER(2) NOT NULL
, acc_id          	NUMBER(2) NOT NULL
, upa_access_ind	VARCHAR2(1)
, created_by		VARCHAR2(10)
, created_date		TIMESTAMP
, updated_by		VARCHAR2(10)
, updated_date		TIMESTAMP
);

 -- Public synonym
CREATE PUBLIC SYNONYM user_page_access FOR user_page_access;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON user_page_access TO ium_app_user;
 