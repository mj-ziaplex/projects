CREATE TABLE policy_medical_records
(
  med_record_id 	NUMBER(8) NOT NULL
, uar_reference_num	VARCHAR2(15) NOT NULL
, association_type	VARCHAR2(1)
); 

-- Public synonym
CREATE PUBLIC SYNONYM policy_medical_records FOR policy_medical_records;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON policy_medical_records TO ium_app_user;
