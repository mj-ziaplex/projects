CREATE TABLE pages_access
(
  PA_ID		NUMBER(5) NOT NULL
, PAGE_ID	NUMBER(2)
, ACC_ID		NUMBER(2)
, PA_ENABLED_IND  VARCHAR2(1)
);

-- Public synonym
CREATE PUBLIC SYNONYM page_access FOR pages_access;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON page_access TO ium_app_user;

