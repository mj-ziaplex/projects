CREATE TABLE mib_actions 
(
  mib_act_code 		VARCHAR2(3) NOT NULL
, mib_act_desc 		VARCHAR2(40)
, created_by	VARCHAR2(10)
, created_date	TIMESTAMP(6)
, updated_by	VARCHAR2(10)
, updated_date	TIMESTAMP(6)
);


-- Public synonym
CREATE PUBLIC SYNONYM mib_actions FOR mib_actions;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON mib_actions TO ium_app_user;
 