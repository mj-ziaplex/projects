CREATE TABLE ATTACHMENTS
(
  uar_reference_num  varchar2(15) not null
  ,requirement_id	 number(5) not null
);

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON ATTACHMENTS TO ium_app_user;

