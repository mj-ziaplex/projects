CREATE TABLE FORM_FIELDS
(
  FF_ID                  NUMBER(2) not null
, FF_NAME                VARCHAR2(25)
, FF_APP_MAPPING         NUMBER(2)
);
 
-- Public synonym
CREATE PUBLIC SYNONYM form_fields FOR form_fields;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON form_fields TO ium_app_user;

 