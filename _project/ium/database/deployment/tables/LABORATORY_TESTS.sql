CREATE TABLE laboratory_tests 
(
  lab_id 		NUMBER(5) NOT NULL
, test_id 		NUMBER(5) NOT NULL
, test_fee 		NUMBER(9,2)
, lab_test_status 	VARCHAR2(10)
, created_by	VARCHAR2(10)
, created_date	TIMESTAMP(6)
, updated_by	VARCHAR2(10)
, updated_date	TIMESTAMP(6)
);

-- Public synonym
CREATE PUBLIC SYNONYM laboratory_tests FOR laboratory_tests;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON laboratory_tests TO ium_app_user;
