@data\access.sql;

@data\access_templates.sql;

@data\access_templt_details_data.sql;

@data\client_types.sql;

@data\departments.sql;

@data\document_types.sql;

@data\lines_of_business.sql;

@data\lob_status.sql;

@data\mib_actions.sql;

@data\mib_letters.sql;

@data\requirement_forms.sql;

@data\requirements.sql;

@data\roles.sql;

@data\sections.sql;

@data\status.sql;

@data\sunlife_offices.sql;

@data\user_roles.sql;

@data\users.sql;

@data\workflow.sql;
