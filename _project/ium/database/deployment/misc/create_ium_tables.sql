@tables\ACCESSES.sql;
@tables\ACCESS_TEMPLATES.sql;
@tables\ACCESS_TEMPLATE_DETAILS.sql;
@tables\ACTIVITY_LOGS.sql;
@tables\ASSESSMENT_REQUESTS.sql;
@tables\AUDIT_TRAILS.sql;
@tables\AUTO_ASSIGNMENTS.sql;
@tables\AUTO_ASSIGNMENT_CRITERIA.sql;
@tables\CLIENTS.sql;
@tables\CLIENT_DATA_SHEETS.sql;
@tables\CLIENT_TYPES.sql;
@tables\CODEC_KO_CODEC.sql;
@tables\CODEC_REQUIREMENTS.sql;
@tables\DEPARTMENTS.sql;
@tables\DOCUMENT_TYPES.sql;
@tables\EXAMINATION_AREAS.sql;
@tables\EXAMINATION_PLACES.sql;
@tables\EXAMINERS.sql;
@tables\EXAMINER_SPECIALIZATIONS.sql;
@tables\EXCEPTION_BATCH.sql;
@tables\EXCEPTION_DETAILS.sql;
@tables\EXCEPTION_LOG.sql;
@tables\FOLDER_DOCUMENTS.sql;
@tables\FORM_FIELDS.sql;
@tables\HOLIDAYS.sql;
@tables\IMPAIRMENTS.sql;
@tables\JOB_SCHEDULES.sql;
@tables\KICKOUT_CODES.sql;
@tables\KICKOUT_MESSAGES.sql;
@tables\LABORATORIES.sql;
@tables\LABORATORY_TESTS.sql;
@tables\LINES_OF_BUSINESS.sql;
@tables\LOB_DOCUMENTS.sql;
@tables\LOB_REQUIREMENTS.sql;
@tables\LOB_STATUS.sql;
@tables\MEDICAL_BILLS.sql;
@tables\MEDICAL_NOTES.sql;
@tables\MEDICAL_RECORDS.sql;
@tables\MIB_ACTIONS.sql;
@tables\MIB_IMPAIRMENTS.sql;
@tables\MIB_LETTERS.sql;
@tables\MIB_NUMBERS.sql;
@tables\MIB_SCHEDULES.sql;
@tables\NOTIFICATION_RECIPIENTS.sql;
@tables\NOTIFICATION_TEMPLATES.sql;
@tables\PAGES.sql;
@tables\PAGES_ACCESS.sql;
@tables\PLAN_TABLE.sql;
@tables\POLICY_COVERAGE_DETAILS.sql;
@tables\POLICY_MEDICAL_RECORDS.sql;
@tables\POLICY_REQUIREMENTS.sql;
@tables\PRISM_CODEC.sql;
@tables\PROCESS_CONFIGURATIONS.sql;
@tables\PROCESS_CONFIGURATION_ROLES.sql;
@tables\RANKS.sql;
@tables\REQUIREMENTS.sql;
@tables\REQUIREMENT_FORMS.sql;
@tables\REQUIREMENT_FORM_FIELDS.sql;
@tables\ROLES.sql;
@tables\SECTIONS.sql;
@tables\SPECIALIZATIONS.sql;
@tables\STATUS.sql;
@tables\SUNLIFE_OFFICES.sql;
@tables\TEST_PROFILES.sql;
@tables\USERS.sql;
@tables\USER_PAGE_ACCESS.sql;
@tables\USER_PROCESS_PREFERENCES.sql;
@tables\USER_ROLES.sql;
@tables\UW_ASSESSMENT_REQUESTS.sql;
@tables\TRANSACTION_DETAILS.sql;
@tables\TD_COUNTER.sql;
@tables\ATTACHMENTS.sql;
@tables\PURGE_STATISTICS.sql;

