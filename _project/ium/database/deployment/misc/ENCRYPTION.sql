create or replace function encryptData (encryptThis varchar2, key varchar2) return varchar2 is
-- Use DES to encrypt a VARCHAR
--
-- R. Sherman 
-- 
-- ARGUMENTS
-- 
-- encryptThis       -- Data to be encrypted
-- key        		 -- Key used to encrypt the data
-- return       	 -- The encrypted data 
-- 
-- NOTES
-- 
--       This function appends spaces to the end of the data until its
--       length is a multiple of 8 bytes (required by DES). You may
--       to append them in the beginning, depending on your needs. 
--       A similar requirement exists for the key itself, but in 
--       order to cut down on processing overhead, the key used should
--       already conform to these rules.
--

        theLen number;
        preferredLen number;
        data2 varchar2(50);
begin
          data2:=encryptThis;
          preferredLen:=length(data2)/8+1;
          preferredLen:=trunc(preferredLen)*8;
          
          theLen:=length(data2);
          while (theLen!=preferredLen) 
            loop
               data2:=data2||' ';
               theLen:=length(data2);
            end loop;
          return dbms_obfuscation_toolkit.desencrypt(input_string=>data2,key_string=>key);
end;
/


create or replace function decryptData (data varchar2, key varchar2) return varchar2 is
-- Use DES to decrypt a VARCHAR
--
-- R. Sherman 
-- 
-- ARGUMENTS
-- 
-- data       -- Data to be decrypted
-- key        -- Key used to decrypt the data
-- return     -- The decrypted data 
-- 


begin
   return       dbms_obfuscation_toolkit.desdecrypt(input_string=>data,key_string=>key);
end;
/

