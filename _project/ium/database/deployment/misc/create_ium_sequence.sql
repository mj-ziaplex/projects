-- Primary Key Sequence ACCESSES
CREATE SEQUENCE seq_access
	INCREMENT BY 1
	START WITH 1
	MAXVALUE 99;

-- Primary Key Sequence ACCESS_TEMPLATES
CREATE SEQUENCE seq_access_template
	INCREMENT BY 1
	START WITH 1
	MAXVALUE 99999;

-- Primary Key Sequence ACCESS_TEMPLATE_DETAILS
CREATE SEQUENCE seq_access_template_dtls
	INCREMENT BY 1
	START WITH 1
	MAXVALUE 99999;

-- Primary Key Sequence ACTIVITY_LOGS
CREATE SEQUENCE seq_activity_logs
 START WITH 1
 INCREMENT BY 1
 MAXVALUE 9999999999;

 -- Primary Key Sequence AUDIT_TRAILS
CREATE SEQUENCE seq_audit_trail
	START WITH 1
	INCREMENT BY 1
	MAXVALUE 9999999999;
 
 -- Primary Key Sequence AUTO_ASSIGNMENT
CREATE SEQUENCE seq_auto_assignment
	START WITH 1
	INCREMENT BY 1
	MAXVALUE 999;

-- Primary Key Sequence AUTO_ASSIGNMENT_CRITERIA
CREATE SEQUENCE seq_auto_assignment_criteria
	START WITH 1
	INCREMENT BY 1
	MAXVALUE 999;

-- Primary Key Sequence EXAMINATION_AREA	
CREATE SEQUENCE seq_examination_area
		INCREMENT BY 1
		START WITH 1
		MAXVALUE 999;

-- Primary Key Sequence EXAMINATION_PLACES
CREATE SEQUENCE seq_examination_place
	START WITH 1
	INCREMENT BY 1
	NOMAXVALUE;

-- Primary Key Sequence EXAMINERS
CREATE SEQUENCE seq_examiner
	START WITH 1
	INCREMENT BY 1
	NOMAXVALUE;

-- Primary Key Sequence EXCEPTION_BATCH
CREATE SEQUENCE SEQ_BATCH_ID
		INCREMENT BY 1
		START WITH 1
		MAXVALUE 99999999;

-- Primary Key Sequence EXCEPTION_LOG
CREATE SEQUENCE SEQ_EXCEPTION_ID
		INCREMENT BY 1
		START WITH 1
		MAXVALUE 99999999;

-- Primary Key Sequence FOLDER_DOCUMENTS
CREATE SEQUENCE seq_folder_document
	START WITH 1
	INCREMENT BY 1
	NOMAXVALUE;

-- Primary Key Sequence FORM_FIELDS
CREATE SEQUENCE seq_form_fields
	INCREMENT BY 1
	START WITH 1
	MAXVALUE 99;

-- Primary Key Sequence HOLIDAYS
CREATE SEQUENCE seq_holiday
    START WITH 1
	INCREMENT BY 1
	MAXVALUE 999;

-- Primary Key Sequence IMPAIRMENTS
CREATE SEQUENCE seq_impairment
	START WITH 1
	INCREMENT BY 1
	NOMAXVALUE;

-- Primary Key Sequence JOB_SCHEDULES
CREATE SEQUENCE seq_job_schedules
	INCREMENT BY 1
	START WITH 1
	MAXVALUE 99999;
	

-- Primary Key Sequences LABORATORIES
CREATE SEQUENCE seq_laboratory
    START WITH 1
	INCREMENT BY 1
	NOMAXVALUE;

-- Primay Key Sequence MEDICAL_BILLS
CREATE SEQUENCE seq_medical_bill
    START WITH 1
	INCREMENT BY 1
	NOMAXVALUE;

-- Primary Key Sequences MEDICAL_NOTES
CREATE SEQUENCE seq_medical_note
    START WITH 1
	INCREMENT BY 1
	NOMAXVALUE;

-- Primary Key Sequence MEDICAL_RECORDS
CREATE SEQUENCE seq_medical_record
	START WITH 1
	INCREMENT BY 1
	NOMAXVALUE;

-- Primary Key Sequence MIB_SCHEDULES
CREATE SEQUENCE seq_mib_schedule
	START WITH 1
	INCREMENT BY 1
	MAXVALUE 999;
	
-- Primary Key Sequence NOTIFICATION_TEMPLATES
CREATE SEQUENCE seq_notification_template
	START WITH 1
	INCREMENT BY 1
	MAXVALUE 99999;
	
-- Primary Key Sequence PAGES
CREATE SEQUENCE seq_page
	START WITH 1
	INCREMENT BY 1
	MAXVALUE 99;

-- Primary Key Sequence PAGE_ACCESS
CREATE SEQUENCE seq_pages_access
 START WITH 1
 INCREMENT BY 1
 NOMAXVALUE;

-- Primary Key Sequence PROCESS_CONFIGURATIONS
CREATE SEQUENCE seq_process_configuration
    START WITH 1
	INCREMENT BY 1
	MAXVALUE 99999;

-- Primary Key Sequence RANKS
CREATE SEQUENCE seq_rank
	START WITH 1
	INCREMENT BY 1
	MAXVALUE 999;
	
-- Primary Key Sequence REQUIREMENT_FORMS
CREATE SEQUENCE seq_requirement_form
	START WITH 1
	INCREMENT BY 1
	NOMAXVALUE;
	
-- Primary Key Sequence REQUIREMENT_FORM_FIELDS
CREATE SEQUENCE seq_requirement_form_fields
	INCREMENT BY 1
	START WITH 1
	MAXVALUE 99;

-- Primary Key Sequence SPECIALIZATIONS
CREATE SEQUENCE seq_specialization
	START WITH 1
	INCREMENT BY 1
	NOMAXVALUE;
	
-- Primary Key Sequence STATUS
CREATE SEQUENCE seq_status
	START WITH 1
	INCREMENT BY 1
	MAXVALUE 9999;
	
-- Primary Key Sequence TEST_PROFILES
CREATE SEQUENCE seq_test_profile
	START WITH 1
	INCREMENT BY 1
	NOMAXVALUE;
	
-- Primary Key Sequence UW_ASSESSMENT_REQUESTS
CREATE SEQUENCE seq_uw_assessment_requests
 START WITH 1
 INCREMENT BY 1
 NOMAXVALUE;