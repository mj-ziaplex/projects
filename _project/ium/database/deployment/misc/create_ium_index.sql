CREATE INDEX idx_pend_recs_exmnr ON medical_records
(
med_record_id ASC,
med_status ASC,
med_request_date ASC,
med_lab_id ASC
);
 
 
CREATE INDEX idx_pend_recs_lab ON medical_records
(
med_record_id ASC,
med_status ASC,
med_request_date ASC,
exmnr_id ASC
);
 