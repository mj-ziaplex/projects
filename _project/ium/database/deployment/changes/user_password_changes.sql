ALTER TABLE users add (usr_1login_ind varchar2(1), pswd_update_date timestamp);

CREATE TABLE user_passwords (
	 userid varchar2(10)  not null
	,password1 varchar2(24) default('0')
	,password2 varchar2(24) default('0')
	,password3 varchar2(24) default('0')
	,password4 varchar2(24) default('0')
	,password5 varchar2(24) default('0')
);

ALTER TABLE user_passwords add constraint fk_pswd_usr 
	FOREIGN KEY (user_id)
	REFERENCES users(user_id);
	

