-- Primary Key ACCESSES
ALTER TABLE accesses
 ADD CONSTRAINT pk_accesses
 PRIMARY KEY (acc_id)
 USING INDEX;
 
-- Primary Key ACCESS_TEMPLATES
ALTER TABLE access_templates
 ADD CONSTRAINT pk_access_template 
 PRIMARY KEY (templt_id)
 USING INDEX;

-- Primary Key ACCESS_TEMPLATE DETAILS
ALTER TABLE access_template_details
 ADD CONSTRAINT pk_access_template_details
 PRIMARY KEY (templt_id, pa_id)
 USING INDEX;
 
-- Primary Key ACTIVITY_LOGS
ALTER TABLE activity_logs
 ADD CONSTRAINT pk_activity_logs
 PRIMARY KEY (act_id)
 USING INDEX;

-- Primary Key ASSESSMENT_REQUESTS
ALTER TABLE assessment_requests
 ADD CONSTRAINT pk_assessment_requests
 PRIMARY KEY (reference_num)
 USING INDEX;

-- Primary Key AUDIT_TRAILS
ALTER TABLE audit_trails
 ADD CONSTRAINT pk_audit_trails
 PRIMARY KEY (tran_num)
 USING INDEX;

-- Primary Key AUTO_ASSIGNMENTS
ALTER TABLE auto_assignments
 ADD CONSTRAINT pk_auto_assignments
 PRIMARY KEY (aa_id)
 USING INDEX;

-- Primary Key AUTO_ASSIGNMENT_CRITERIA
ALTER TABLE auto_assignment_criteria
 ADD CONSTRAINT pk_auto_assignment_criteria
 PRIMARY KEY (aac_id)
 USING INDEX;

-- Primary Key CLIENTS
ALTER TABLE clients
 ADD CONSTRAINT pk_clients
 PRIMARY KEY (client_id)
 USING INDEX;

-- Primary Key CLIENT_DATA_SHEETS
ALTER TABLE client_data_sheets
 ADD CONSTRAINT pk_client_data_sheets
 PRIMARY KEY (cds_reference_num, cds_client_num)
 USING INDEX;

-- Primary Key CLIENT_TYPES
ALTER TABLE client_types
 ADD CONSTRAINT pk_client_types
 PRIMARY KEY (clt_code)
 USING INDEX;

-- Primary key CODEC_KO_CODEC
ALTER TABLE codec_ko_codec
 ADD CONSTRAINT pk_codec_ko_codec
 PRIMARY KEY (pc_seq, ko_seq)
 USING INDEX;

-- Primary key CODEC_REQUIREMENTS
ALTER TABLE codec_requirements
 ADD CONSTRAINT pk_codec_requirements
 PRIMARY KEY (pc_seq, reqt_code)
 USING INDEX;

-- Primary Key DEPARTMENTS
ALTER TABLE departments
 ADD CONSTRAINT pk_departments
 PRIMARY KEY (dept_code)
 USING INDEX;

-- Primary Key  DOCUMENT_TYPES
ALTER TABLE document_types
 ADD PRIMARY KEY (doc_code)
 USING INDEX;

-- Primary Key EXAMINATION_PLACES
ALTER TABLE examination_places
 ADD CONSTRAINT pk_examination_places
 PRIMARY KEY (ep_id)
 USING INDEX;

-- Primary Key EXAMINERS
ALTER TABLE examiners
 ADD CONSTRAINT pk_examiners
 PRIMARY KEY (exmnr_id)
 USING INDEX;

-- Primary Key EXAMINER_SPECIALIZATIONS
ALTER TABLE examiner_specializations
 ADD CONSTRAINT pk_examiner_specializations
 PRIMARY KEY (exmnr_id, spl_id)
 USING INDEX;

-- Primary key EXCEPTION_BATCH
ALTER TABLE exception_batch
 ADD CONSTRAINT pk_exception_batch
 PRIMARY KEY (batch_id)
 USING INDEX;

-- Primary key EXCEPTION_LOG
ALTER TABLE exception_log
 ADD CONSTRAINT pk_exception_log
 PRIMARY KEY (exception_id)
 USING INDEX;

-- Primary Key FOLDER_DOCUMENTS
ALTER TABLE folder_documents
 ADD CONSTRAINT pk_folder_documents
 PRIMARY KEY (doc_id)
 USING INDEX;

-- Primary key FORM_FIELDS
ALTER TABLE FORM_FIELDS
 ADD CONSTRAINT pk_form_fields
 PRIMARY KEY (ff_id)
 USING INDEX;

-- Primary Key HOLIDAYS
ALTER TABLE holidays
 ADD CONSTRAINT pk_holidays
 PRIMARY KEY (hol_id)
 USING INDEX;

-- Primary Key IMPAIRMENTS
ALTER TABLE impairments
 ADD CONSTRAINT pk_impairments
 PRIMARY KEY (imp_impairement_id)
 USING INDEX;
 
-- Primary Key JOB_SCHEDULES
ALTER TABLE job_schedules
 ADD CONSTRAINT pk_job_schedules
 PRIMARY KEY (sched_type);

-- Primary key KICKOUT_CODES
ALTER TABLE kickout_codes
 ADD CONSTRAINT pk_kickout_codes
 PRIMARY KEY (ko_seq)
 USING INDEX;

-- Primary Key LABORATORIES
ALTER TABLE laboratories
 ADD CONSTRAINT pk_laboratories
 PRIMARY KEY (lab_id)
 USING INDEX;

-- Primary Key LABORATORY_TESTS
ALTER TABLE laboratory_tests
 ADD CONSTRAINT pk_laboratory_tests
 PRIMARY KEY (lab_id, test_id)
 USING INDEX;

-- Primary Key LINES_OF_BUSINESS
ALTER TABLE lines_of_business
 ADD CONSTRAINT pk_lines_of_business
 PRIMARY KEY (lob_code)
 USING INDEX;

-- Primary Key LOB_DOCUMENTS
ALTER TABLE lob_documents
 ADD CONSTRAINT pk_lob_documents
 PRIMARY KEY (lob_code, doc_code)
 USING INDEX;

-- Primary Key LOB_REQUIREMENTS
ALTER TABLE lob_requirements
 ADD CONSTRAINT pk_lob_requirements
 PRIMARY KEY (lob_code, reqt_code)
 USING INDEX;

-- Primary Key LOB_STATUS
ALTER TABLE lob_status
 ADD CONSTRAINT pk_lob_status
 PRIMARY KEY (lob_code, stat_id)
 USING INDEX;
 
-- Primary Key MEDICAL_BILLS
ALTER TABLE medical_bills
 ADD CONSTRAINT pk_medical_bills
 PRIMARY KEY (mb_bill_number)
 USING INDEX;

-- Primary Key MEDICAL_NOTES
ALTER TABLE medical_notes
 ADD CONSTRAINT pk_medical_notes
 PRIMARY KEY (mn_notes_id)
 USING INDEX;

-- Primary Key MEDICAL_RECORDS
ALTER TABLE medical_records
 ADD CONSTRAINT pk_medical_records
 PRIMARY KEY (med_record_id)
 USING INDEX;

-- Primary Key MIB_ACTIONS
ALTER TABLE mib_actions
 ADD CONSTRAINT pk_mib_actions
 PRIMARY KEY (mib_act_code)
 USING INDEX;

-- Primary Key MIB_IMPAIRMENTS
ALTER TABLE mib_impairments
 ADD CONSTRAINT pk_mib_impairments
 PRIMARY KEY (mib_impairment_code)
 USING INDEX;

-- Primary Key MIB_LETTERS
ALTER TABLE mib_letters
 ADD CONSTRAINT pk_mib_letters
 PRIMARY KEY (mib_lttr_code)
 USING INDEX;

-- Primary Key MIB_NUMBERS
ALTER TABLE mib_numbers
 ADD CONSTRAINT pk_mib_numbers
 PRIMARY KEY (mib_num_code)
 USING INDEX;

-- Primary Key MIB_SCHEDULES
ALTER TABLE mib_schedules
 ADD CONSTRAINT pk_mib_schedules
 PRIMARY KEY (mib_sked_id)
 USING INDEX;

-- Primary Key NOTIFICATION_RECIPIENTS
ALTER TABLE notification_recipients
 ADD CONSTRAINT pk_notification_recipients
 PRIMARY KEY (not_id, role_id, lob_code)
 USING INDEX;

-- Primary Key NOTIFICATION_TEMPLATES
ALTER TABLE notification_templates
 ADD CONSTRAINT pk_notification_templates
 PRIMARY KEY (not_id)
 USING INDEX;

-- Primary Key PAGES
ALTER TABLE pages
 ADD CONSTRAINT pk_pages
 PRIMARY KEY (page_id)
 USING INDEX;

-- Primary Key PAGE_ACCESS
ALTER TABLE pages_access
 ADD CONSTRAINT pk_pages_access
 PRIMARY KEY (pa_id)
 USING INDEX;


-- Primary Key POLICY_COVERAGE_DETAILS
ALTER TABLE policy_coverage_details
 ADD CONSTRAINT pk_policy_coverage_details
 PRIMARY KEY (pcd_policy_num, uar_reference_num)
 USING INDEX;

-- Primary Key POLICY_MEDICAL_RECORDS
ALTER TABLE policy_medical_records
 ADD CONSTRAINT pk_policy_medical_records
 PRIMARY KEY (med_record_id, uar_reference_num)
 USING INDEX;

-- Primary Key POLICY_REQUIREMENTS
ALTER TABLE policy_requirements
 ADD CONSTRAINT pk_policy_requirements
 PRIMARY KEY (pr_requirement_id)
 USING INDEX;

-- Primary key PRISM_CODEC
ALTER TABLE prism_codec
 ADD CONSTRAINT pk_prism_codec
 PRIMARY KEY (pc_seq) 
 USING INDEX;

-- Primary Key PROCESS_CONFIGURATIONS
ALTER TABLE process_configurations
 ADD CONSTRAINT pk_process_configurations
 PRIMARY KEY (evnt_id)
 USING INDEX;

-- Primary Key PURGE_STATISTICS
ALTER TABLE purge_statistics
 ADD CONSTRAINT pk_purge_statistics
 PRIMARY KEY (purge_date)
 USING INDEX;

-- Primary Key RANKS
ALTER TABLE ranks
 ADD CONSTRAINT pk_ranks
 PRIMARY KEY (rank_id)
 USING INDEX;

-- Primary Key REQUIREMENTS
ALTER TABLE requirements
 ADD CONSTRAINT pk_requirements
 PRIMARY KEY (reqt_code)
 USING INDEX;

-- Primary Key REQUIREMENT_FORMS
ALTER TABLE requirement_forms
 ADD CONSTRAINT pk_requirement_forms
 PRIMARY KEY (rf_id)
 USING INDEX;

-- Primary key REQUIREMENT_FORM_FIELDS
ALTER TABLE requirement_form_fields
 ADD CONSTRAINT pk_req_form_fields
 PRIMARY KEY (rff_id)
 USING INDEX;

-- Primary Key ROLES
ALTER TABLE roles
 ADD CONSTRAINT pk_roles
 PRIMARY KEY (role_code)
 USING INDEX;

-- Primary Key SECTIONS
ALTER TABLE sections
 ADD CONSTRAINT pk_sections
 PRIMARY KEY (sec_code)
 USING INDEX;

-- Primary Key SPECALIZATIONS
ALTER TABLE specializations
 ADD CONSTRAINT pk_specializations
 PRIMARY KEY (spl_id)
 USING INDEX;

-- Primary Key STATUS
ALTER TABLE status
 ADD CONSTRAINT pk_status
 PRIMARY KEY (stat_id)
 USING INDEX;

-- Primary Key SUNLIFE_OFFICES
ALTER TABLE sunlife_offices
 ADD CONSTRAINT pk_sunlife_offices
 PRIMARY KEY (slo_office_code)
 USING INDEX;

-- Primary Key TEST_PROFILES
ALTER TABLE test_profiles
 ADD CONSTRAINT pk_test_profiles
 PRIMARY KEY (test_id)
 USING INDEX;

-- Primary Key TRANSACTION_DETAILS
ALTER TABLE transaction_details
 ADD CONSTRAINT pk_transaction_details
 PRIMARY KEY(impairment_id, control_no)
 USING INDEX;

-- Primary Key USERS
ALTER TABLE users
 ADD CONSTRAINT pk_users
 PRIMARY KEY (user_id)
 USING INDEX;

-- Primary Key USER_PAGE_ACCESS
ALTER TABLE user_page_access
 ADD CONSTRAINT pk_user_page_access
 PRIMARY KEY (user_id, page_id, acc_id)
 USING INDEX;

-- Primary key USER_PROCESS_PREFERENCES
ALTER TABLE user_process_preferences
 ADD CONSTRAINT pk_user_process_preferences
 PRIMARY KEY (evnt_id, user_id)
 USING INDEX;

-- Primary key USER_ROLES
ALTER TABLE user_roles
 ADD CONSTRAINT pk_user_roles
 PRIMARY KEY (role_code, user_id)
 USING INDEX;

-- Foreign key UW_ASSESSMENT_REQUESTS
ALTER TABLE uw_assessment_requests
 ADD CONSTRAINT fk_uwa_users_created_by
 FOREIGN KEY (created_by)
 REFERENCES users(user_id);

ALTER TABLE uw_assessment_requests
 ADD CONSTRAINT fk_uwa_users_updated_by
 FOREIGN KEY (updated_by)
 REFERENCES users(user_id);

-- Foreign Key USER_ROLES
ALTER TABLE user_roles
 ADD CONSTRAINT fk_ur_roles
 FOREIGN KEY (role_code)
 REFERENCES roles (role_code);

ALTER TABLE user_roles
 ADD CONSTRAINT fk_users
 FOREIGN KEY (user_id)
 REFERENCES users (user_id);
 
-- Foreign key USER_PROCESS_PREFERENCES
ALTER TABLE user_process_preferences
 ADD CONSTRAINT fk_upp_process_configurations
 FOREIGN KEY (evnt_id)
 REFERENCES process_configurations(evnt_id);

ALTER TABLE user_process_preferences
 ADD CONSTRAINT fk_upp_users
 FOREIGN KEY (user_id)
 REFERENCES users(user_id);
 
-- Foreign Keys USER_PAGE_ACCESS
ALTER TABLE user_page_access
 ADD CONSTRAINT fk_upa_users
 FOREIGN KEY (user_id)
 REFERENCES users (user_id);
 
ALTER TABLE user_page_access
 ADD CONSTRAINT fk_upa_pages
 FOREIGN KEY (page_id)
 REFERENCES pages (page_id);
 
ALTER TABLE user_page_access
 ADD CONSTRAINT fk_upa_users_created_by
 FOREIGN KEY (created_by)
 REFERENCES users (user_id);

ALTER TABLE user_page_access
 ADD CONSTRAINT fk_upa_users_updated_by
 FOREIGN KEY (updated_by)
 REFERENCES users (user_id);

-- Foreign Key USERS
ALTER TABLE users
 ADD CONSTRAINT fk_usr_sunlife_offices
 FOREIGN KEY (slo_id)
 REFERENCES sunlife_offices (slo_office_code);

ALTER TABLE users
 ADD CONSTRAINT fk_usr_users_created_by
 FOREIGN KEY (created_by)
 REFERENCES users (user_id);

ALTER TABLE users
 ADD CONSTRAINT fk_usr_users_updated_by
 FOREIGN KEY (updated_by)
 REFERENCES users (user_id);
 
-- Foreign key TRANSACTION_DETAILS
ALTER TABLE transaction_details
 ADD CONSTRAINT fk_users_tran_details
 FOREIGN KEY(user_cd)
 REFERENCES users (user_id);
 
-- Foreign key TEST_PROFILES
ALTER TABLE test_profiles
 ADD CONSTRAINT fk_tp_users_created_by
 FOREIGN KEY (created_by)
 REFERENCES users (user_id);

ALTER TABLE test_profiles
 ADD CONSTRAINT fk_tp_users_updated_by
 FOREIGN KEY (updated_by)
 REFERENCES users (user_id);
 
-- Foreign key STATUS
ALTER TABLE status
 ADD CONSTRAINT fk_stat_users_created_by
 FOREIGN KEY (created_by)
 REFERENCES users(user_id);

ALTER TABLE status
 ADD CONSTRAINT fk_stat_users_updated_by
 FOREIGN KEY (updated_by)
 REFERENCES users(user_id);

-- Foreign key SUNLIFE_OFFICES
ALTER TABLE sunlife_offices
 ADD CONSTRAINT fk_slo_users_created_by
 FOREIGN KEY (created_by)
 REFERENCES users (user_id);

ALTER TABLE sunlife_offices
 ADD CONSTRAINT fk_slo_users_updated_by
 FOREIGN KEY (updated_by)
 REFERENCES users (user_id);
 
-- Foreign key SPECIALIZATIONS
ALTER TABLE specializations
 ADD CONSTRAINT fk_spz_users_created_by
 FOREIGN KEY (created_by)
 REFERENCES users(user_id);

ALTER TABLE specializations
 ADD CONSTRAINT fk_spz_users_updated_by
 FOREIGN KEY (updated_by)
 REFERENCES users(user_id);

-- Foreign key SECTIONS
ALTER TABLE sections
 ADD CONSTRAINT fk_sec_users_created_by
 FOREIGN KEY (created_by)
 REFERENCES users (user_id);

ALTER TABLE sections
 ADD CONSTRAINT fk_sec_users_updated_by
 FOREIGN KEY (updated_by)
 REFERENCES users (user_id);

-- Foreign key ROLES
ALTER TABLE roles
 ADD CONSTRAINT fk_rle_users_created_by
 FOREIGN KEY (created_by)
 REFERENCES users(user_id);
 
ALTER TABLE roles
 ADD CONSTRAINT fk_rle_users_updated_by
 FOREIGN KEY (updated_by)
 REFERENCES users(user_id);
 
-- Foreign key REQUIREMENT_FORMS
ALTER TABLE requirement_forms
 ADD CONSTRAINT fk_rf_users_created_by
 FOREIGN KEY (created_by)
 REFERENCES users(user_id);

ALTER TABLE requirement_forms
 ADD CONSTRAINT fk_rf_users_updated_by
 FOREIGN KEY (updated_by)
 REFERENCES users(user_id);

-- Foreign key REQUIREMENT_FORM_FIELDS
ALTER TABLE requirement_form_fields
 ADD CONSTRAINT fk_form_fields
 FOREIGN KEY (ff_id)
 REFERENCES form_fields(ff_id);

ALTER TABLE requirement_form_fields
 ADD CONSTRAINT fk_req_form
 FOREIGN KEY (rf_id)
 REFERENCES requirement_forms (rf_id);


-- Foreign Key REQUIREMENTS
ALTER TABLE requirements
 ADD CONSTRAINT fk_reqt_requirement_forms
 FOREIGN KEY (reqt_form_id)
 REFERENCES requirement_forms (rf_id);

ALTER TABLE requirements
 ADD CONSTRAINT fk_reqt_users_created_by
 FOREIGN KEY (created_by)
 REFERENCES users(user_id);

ALTER TABLE requirements
 ADD CONSTRAINT fk_reqt_users_updated_by
 FOREIGN KEY (updated_by)
 REFERENCES users(user_id);


-- Foreign key RANKS
ALTER TABLE ranks
 ADD CONSTRAINT fk_rnk_users_created_by
 FOREIGN KEY (created_by)
 REFERENCES users(user_id);

ALTER TABLE ranks
 ADD CONSTRAINT fk_rnk_users_updated_by
 FOREIGN KEY (updated_by)
 REFERENCES users(user_id);
	
-- Foreign key PROCESS_CONFIGURATION_ROLES
ALTER TABLE process_configuration_roles
	ADD CONSTRAINT fk_pc_evnt_id
	FOREIGN KEY (evnt_id)
	REFERENCES process_configurations (evnt_id);

	
ALTER TABLE process_configuration_roles
	ADD CONSTRAINT fk_pc_role_code
	FOREIGN KEY (role_code)
	REFERENCES roles(role_code);
	
ALTER TABLE process_configuration_roles
 ADD CONSTRAINT fk_pc_lob_code
 FOREIGN KEY (lob_code)
 REFERENCES lines_of_business(lob_code);

-- Foreign Keys PROCESS_CONFIGURATIONS
ALTER TABLE process_configurations
 ADD CONSTRAINT fk_pc_lines_of_business
 FOREIGN KEY (lob_code)
 REFERENCES lines_of_business (lob_code);

ALTER TABLE process_configurations
 ADD CONSTRAINT fk_pc_user_created_by
 FOREIGN KEY (created_by)
 REFERENCES users (user_id);

ALTER TABLE process_configurations
 ADD CONSTRAINT fk_pc_users_updated_by
 FOREIGN KEY (updated_by)
 REFERENCES users (user_id);

ALTER TABLE process_configurations
 ADD CONSTRAINT fk_pc_status_from_status
 FOREIGN KEY (evnt_from_status)
 REFERENCES status (stat_id);

ALTER TABLE process_configurations
 ADD CONSTRAINT fk_pc_status_to_status
 FOREIGN KEY (evnt_to_status)
 REFERENCES status (stat_id);

-- Foreign Key POLICY_REQUIREMENTS
ALTER TABLE policy_requirements
 ADD CONSTRAINT fk_pr_assessment_requests
 FOREIGN KEY (uar_reference_num)
 REFERENCES assessment_requests (reference_num);

ALTER TABLE policy_requirements
 ADD CONSTRAINT fk_pr_clients
 FOREIGN KEY (pr_client_id)
 REFERENCES clients (client_id);

ALTER TABLE policy_requirements
 ADD CONSTRAINT fk_pr_requirements
 FOREIGN KEY (pr_reqt_code)
 REFERENCES requirements (reqt_code);

ALTER TABLE policy_requirements
 ADD CONSTRAINT fk_pr_status
 FOREIGN KEY (pr_status_id)
 REFERENCES status (stat_id);

ALTER TABLE policy_requirements
 ADD CONSTRAINT fk_pr_users_created_by
 FOREIGN KEY (created_by)
 REFERENCES users (user_id);

ALTER TABLE policy_requirements
 ADD CONSTRAINT fk_pr_users_updated_by
 FOREIGN KEY (updated_by)
 REFERENCES users (user_id);
 

-- Foreign Key POLICY_MEDICAL_RECORDS
ALTER TABLE policy_medical_records
 ADD CONSTRAINT fk_pmr_assessment_request
 FOREIGN KEY (uar_reference_num)
 REFERENCES assessment_requests (reference_num);
 

-- Foreign Key POLICY_COVERAGE_DETAILS
ALTER TABLE policy_coverage_details
 ADD CONSTRAINT fk_pcd_assessment_requests
 FOREIGN KEY (uar_reference_num)
 REFERENCES assessment_requests (reference_num);

ALTER TABLE policy_coverage_details
 ADD CONSTRAINT fk_pcd_clients
 FOREIGN KEY (pcd_client_num)
 REFERENCES clients (client_id);


-- Foreign Key PAGES_ACCESS
ALTER TABLE pages_access
 ADD CONSTRAINT fk_pa_pages
 FOREIGN KEY (page_id)
 REFERENCES pages (page_id); 

ALTER TABLE pages_access
 ADD CONSTRAINT fk_pa_access
 FOREIGN KEY (acc_id)
 REFERENCES accesses (acc_id);

-- Foreign Key PAGES
Alter table pages
 ADD CONSTRAINT fk_pages_user_created_by
 FOREIGN KEY (created_by)
 REFERENCES users(user_id);
 
Alter table pages
 ADD CONSTRAINT fk_pages_user_updated_by
 FOREIGN KEY (updated_by)
 REFERENCES users(user_id);
 
-- Foreign Key NOTIFICATION_TEMPLATES
ALTER TABLE notification_templates
 ADD CONSTRAINT fk_ntt_users_created_by
 FOREIGN KEY (created_by)
 REFERENCES users (user_id);

ALTER TABLE notification_templates
 ADD CONSTRAINT fk_ntt_users_updated_by
 FOREIGN KEY (updated_by)
 REFERENCES users (user_id);
 
-- Foreign Key NOTIFICATION_RECIPIENTS
ALTER TABLE notification_recipients
 ADD CONSTRAINT fk_not_notification_templates
 FOREIGN KEY (not_id)
 REFERENCES notification_templates (not_id);

ALTER TABLE notification_recipients
 ADD CONSTRAINT fk_not_role
 FOREIGN KEY (role_id)
 REFERENCES roles (role_code);
 
ALTER TABLE notification_recipients
 ADD CONSTRAINT fk_not_lines_of_business
 FOREIGN KEY (lob_code)
 REFERENCES lines_of_business (lob_code);

ALTER TABLE notification_recipients
 ADD CONSTRAINT fk_nr_users_created_by
 FOREIGN KEY (created_by)
 REFERENCES users(user_id);
 
ALTER TABLE notification_recipients
 ADD CONSTRAINT fk_nr_users_updated_by
 FOREIGN KEY (updated_by)
 REFERENCES users(user_id);


-- Foreign key MIB_NUMBERS
ALTER TABLE mib_numbers
 ADD CONSTRAINT fk_mn_users_created_by
 FOREIGN KEY (created_by)
 REFERENCES users(user_id);

ALTER TABLE mib_numbers
 ADD CONSTRAINT fk_mn_users_updated_by
 FOREIGN KEY (updated_by)
 REFERENCES users(user_id);


-- Foreign key MIB_LETTERS
ALTER TABLE mib_letters
 ADD CONSTRAINT fk_ml_users_created_by
 FOREIGN KEY (created_by)
 REFERENCES users(user_id);

ALTER TABLE mib_letters
 ADD CONSTRAINT fk_ml_users_updated_by
 FOREIGN KEY (updated_by)
 REFERENCES users(user_id);
 
-- Foreign key MIB_IMPAIRMENTS
ALTER TABLE mib_impairments
 ADD CONSTRAINT fk_mi_users_created_by
 FOREIGN KEY (created_by)
 REFERENCES users(user_id);

ALTER TABLE mib_impairments
 ADD CONSTRAINT fk_mi_users_updated_by
 FOREIGN KEY (updated_by)
 REFERENCES users(user_id);

-- Foreign key MIB_ACTIONS
ALTER TABLE mib_actions
 ADD CONSTRAINT fk_ma_users_created_by
 FOREIGN KEY (created_by)
 REFERENCES users(user_id);

ALTER TABLE mib_actions
 ADD CONSTRAINT fk_ma_users_updated_by
 FOREIGN KEY (updated_by)
 REFERENCES users(user_id);


-- Foreign Key MEDICAL_RECORDS
ALTER TABLE medical_records
 ADD CONSTRAINT fk_med_clients
 FOREIGN KEY (cl_client_num)
 REFERENCES clients (client_id);

ALTER TABLE medical_records
 ADD CONSTRAINT fk_med_users_agent
 FOREIGN KEY (agent_id)
 REFERENCES users (user_id);

ALTER TABLE medical_records
 ADD CONSTRAINT fk_med_branches
 FOREIGN KEY (branch_id)
 REFERENCES sunlife_offices (slo_office_code);

ALTER TABLE medical_records
 ADD CONSTRAINT fk_med_status
 FOREIGN KEY (med_status)
 REFERENCES status (stat_id);

ALTER TABLE medical_records
 ADD CONSTRAINT fk_med_examination_places
 FOREIGN KEY (med_examination_place)
 REFERENCES examination_places (ep_id);

ALTER TABLE medical_records
 ADD CONSTRAINT fk_med_laboratories
 FOREIGN KEY (med_lab_id)
 REFERENCES laboratories (lab_id);

ALTER TABLE medical_records
 ADD CONSTRAINT fk_med_examiners
 FOREIGN KEY (exmnr_id)
 REFERENCES examiners (exmnr_id);

ALTER TABLE medical_records
 ADD CONSTRAINT fk_med_test_profiles
 FOREIGN KEY (med_test_id)
 REFERENCES test_profiles (test_id);

ALTER TABLE medical_records
 ADD CONSTRAINT fk_med_sections
 FOREIGN KEY (med_section_id)
 REFERENCES sections (sec_code);

ALTER TABLE medical_records
 ADD CONSTRAINT fk_med_departments
 FOREIGN KEY (med_department_id)
 REFERENCES departments (dept_code);

ALTER TABLE medical_records
 ADD CONSTRAINT fk_med_users_created_by
 FOREIGN KEY (created_by)
 REFERENCES users (user_id);

ALTER TABLE medical_records
 ADD CONSTRAINT fk_med_users_updated_by
 FOREIGN KEY (updated_by)
 REFERENCES users (user_id);

-- Foreign Key MEDICAL_NOTES
ALTER TABLE medical_notes
 ADD CONSTRAINT fk_mn_assessment_requests
 FOREIGN KEY (uar_reference_num)
 REFERENCES assessment_requests (reference_num);

ALTER TABLE medical_notes
 ADD CONSTRAINT fk_mn_users
 FOREIGN KEY (mn_posted_by)
 REFERENCES users (user_id);


-- Foreign Key MEDICAL_BILLS
ALTER TABLE medical_bills
 ADD CONSTRAINT fk_mb_assessment_requests
 FOREIGN KEY (uar_reference_num)
 REFERENCES assessment_requests (reference_num);

ALTER TABLE medical_bills
 ADD CONSTRAINT fk_mb_examiners
 FOREIGN KEY (exmnr_id)
 REFERENCES examiners (exmnr_id);

ALTER TABLE medical_bills
 ADD CONSTRAINT fk_mb_laboratories
 FOREIGN KEY (lab_id)
 REFERENCES laboratories (lab_id);

ALTER TABLE medical_bills
 ADD CONSTRAINT fk_mb_users_created_by
 FOREIGN KEY (created_by)
 REFERENCES users(user_id);

ALTER TABLE medical_bills
 ADD CONSTRAINT fk_mb_users_updated_by
 FOREIGN KEY (updated_by)
 REFERENCES users(user_id);

-- Foreign Key LOB_STATUS
ALTER TABLE lob_status
 ADD CONSTRAINT fk_lbs_lines_of_business
 FOREIGN KEY (lob_code)
 REFERENCES lines_of_business (lob_code);

ALTER TABLE lob_status
 ADD CONSTRAINT fk_lbs_status
 FOREIGN KEY (stat_id)
 REFERENCES status (stat_id);
 
ALTER TABLE lob_status
 ADD CONSTRAINT fk_lbs_users_created_by
 FOREIGN KEY (created_by)
 REFERENCES users(user_id);

ALTER TABLE lob_status
 ADD CONSTRAINT fk_lbs_users_updated_by
 FOREIGN KEY (updated_by)
 REFERENCES users(user_id);

-- Foreign Key LOB_REQUIREMENTS
ALTER TABLE lob_requirements
 ADD CONSTRAINT fk_lrq_lines_of_business
 FOREIGN KEY (lob_code)
 REFERENCES lines_of_business (lob_code);

ALTER TABLE lob_requirements
 ADD CONSTRAINT fk_lrq_requirements
 FOREIGN KEY (reqt_code)
 REFERENCES requirements (reqt_code);

ALTER TABLE lob_requirements
 ADD CONSTRAINT fk_lrq_users_created_by
 FOREIGN KEY (created_by)
 REFERENCES users (user_id);

ALTER TABLE lob_requirements
 ADD CONSTRAINT fk_lrq_users_updated_by
 FOREIGN KEY (updated_by)
 REFERENCES users (user_id);

-- Foreign Key LOB_DOCUMENTS
ALTER TABLE lob_documents 
 ADD CONSTRAINT fk_ldc_lines_of_business
 FOREIGN KEY (lob_code)
 REFERENCES lines_of_business (lob_code);

ALTER TABLE lob_documents 
 ADD CONSTRAINT fk_ldc_document_types
 FOREIGN KEY (doc_code)
 REFERENCES document_types (doc_code);

ALTER TABLE lob_documents
 ADD CONSTRAINT fk_ldc_users_created_by
 FOREIGN KEY (created_by)
 REFERENCES users (user_id);

ALTER TABLE lob_documents
 ADD CONSTRAINT fk_ldc_users_updated_by
 FOREIGN KEY (updated_by)
 REFERENCES users (user_id);
 

-- Foreign key LINES_OF_BUSINESS
ALTER TABLE LINES_OF_BUSINESS
 ADD CONSTRAINT fk_lob_users_created_by
 FOREIGN KEY (created_by)
 REFERENCES users(user_id);

ALTER TABLE LINES_OF_BUSINESS
 ADD CONSTRAINT fk_lob_users_updated_by
 FOREIGN KEY (updated_by)
 REFERENCES users(user_id);
 

-- Foreign Key LABORATORY_TESTS
ALTER TABLE laboratory_tests
 ADD CONSTRAINT fk_test_laboratories
 FOREIGN KEY (lab_id)
 REFERENCES laboratories (lab_id);

ALTER TABLE laboratory_tests
 ADD CONSTRAINT fk_test_test_profiles
 FOREIGN KEY (test_id)
 REFERENCES test_profiles (test_id);
 
ALTER TABLE laboratory_tests
 ADD CONSTRAINT fk_lt_users_created_by
 FOREIGN KEY (created_by)
 REFERENCES users(user_id);
 
ALTER TABLE laboratory_tests
 ADD CONSTRAINT fk_lt_users_updated_by
 FOREIGN KEY (updated_by)
 REFERENCES users(user_id);

-- Foreign Key LABORATORIES
ALTER TABLE laboratories
 ADD CONSTRAINT fk_users_lab_created_by
 FOREIGN KEY (created_by)
 REFERENCES users (user_id);

ALTER TABLE laboratories
 ADD CONSTRAINT fk_users_lab_updated_by
 FOREIGN KEY (updated_by)
 REFERENCES users (user_id);

-- Foreign Key KICKOUT_MESSAGES
ALTER TABLE kickout_messages
 ADD CONSTRAINT fk_ko_assessment_requests
 FOREIGN KEY (uar_reference_num)
 REFERENCES assessment_requests (reference_num);

ALTER TABLE kickout_messages
 ADD CONSTRAINT fk_ko_clients
 FOREIGN KEY (client_id)
 REFERENCES clients (client_id);
 
-- Foreign key JOB_SCHEDULES
ALTER TABLE job_schedules
 ADD CONSTRAINT fk_js_users_created_by
 FOREIGN KEY (created_by)
 REFERENCES users(user_id);

ALTER TABLE job_schedules
 ADD CONSTRAINT fk_js_users_updated_by
 FOREIGN KEY (updated_by)
 REFERENCES users(user_id);

-- Foreign Key IMPAIRMENTS
ALTER TABLE impairments
 ADD CONSTRAINT fk_imp_clients
 FOREIGN KEY (cl_client_num)
 REFERENCES clients (client_id);

ALTER TABLE impairments
 ADD CONSTRAINT fk_imp_assessment_requests
 FOREIGN KEY (uar_reference_num)
 REFERENCES assessment_requests (reference_num);

ALTER TABLE impairments
 ADD CONSTRAINT fk_imp_mib_impairments
 FOREIGN KEY (mib_impairment_code)
 REFERENCES mib_impairments (mib_impairment_code);

ALTER TABLE impairments
 ADD CONSTRAINT fk_imp_mib_numbers
 FOREIGN KEY (imp_relationship)
 REFERENCES mib_numbers (mib_num_code);

ALTER TABLE impairments
 ADD CONSTRAINT fk_imp_mib_letters
 FOREIGN KEY (imp_origin)
 REFERENCES mib_letters (mib_lttr_code);

ALTER TABLE impairments
 ADD CONSTRAINT fk_imp_users_created_by
 FOREIGN KEY (created_by)
 REFERENCES users (user_id);

ALTER TABLE impairments
 ADD CONSTRAINT fk_imp_users_updated_by
 FOREIGN KEY (updated_by)
 REFERENCES users (user_id);

-- Foreign key HOLIDAYS
ALTER TABLE holidays
 ADD CONSTRAINT fk_hol_users_created_by
 FOREIGN KEY (created_by)
 REFERENCES users(user_id);

ALTER TABLE holidays
 ADD CONSTRAINT fk_hol_users_updated_by
 FOREIGN KEY (updated_by)
 REFERENCES users(user_id);


-- Foreign Key FOLDER_DOCUMENTS
ALTER TABLE folder_documents
 ADD CONSTRAINT fk_doc_assessment_requests
 FOREIGN KEY (uar_reference_num)
 REFERENCES assessment_requests (reference_num);

ALTER TABLE folder_documents
 ADD CONSTRAINT fk_doc_document_types
 FOREIGN KEY (doc_code)
 REFERENCES document_types (doc_code);

ALTER TABLE folder_documents
 ADD CONSTRAINT fk_doc_users
 FOREIGN KEY (doc_received_by)
 REFERENCES users (user_id);

-- Foreign key EXCEPTION_LOG
ALTER TABLE exception_log
 ADD CONSTRAINT fk_exception_batch
 FOREIGN KEY (batch_id)
 REFERENCES exception_batch(batch_id);

-- Foreign Key EXAMINER_SPECIALIZATIONS
ALTER TABLE examiner_specializations
 ADD CONSTRAINT fk_esp_examiners
 FOREIGN KEY (exmnr_id)
 REFERENCES examiners (exmnr_id);

ALTER TABLE examiner_specializations
 ADD CONSTRAINT fk_esp_specializations
 FOREIGN KEY (spl_id)
 REFERENCES specializations (spl_id);

ALTER TABLE examiner_specializations
 ADD CONSTRAINT fk_esp_users_created_by
 FOREIGN KEY (created_by)
 REFERENCES users(user_id);
 
ALTER TABLE examiner_specializations
 ADD CONSTRAINT fk_esp_users_updated_by
 FOREIGN KEY (updated_by)
 REFERENCES users(user_id);
 
-- Foreign key EXCEPTION_DETAILS
ALTER TABLE exception_details
 ADD CONSTRAINT fk_exception_log
 FOREIGN KEY (exception_id)
 REFERENCES exception_log(exception_id);
 
-- Foreign key EXAMINATION_PLACES
ALTER TABLE examination_places
 ADD CONSTRAINT fk_ep_users_created_by
 FOREIGN KEY (created_by)
 REFERENCES users(user_id);

ALTER TABLE examination_places
 ADD CONSTRAINT fk_ep_users_updated_by
 FOREIGN KEY (updated_by)
 REFERENCES users(user_id);

-- Foreign Keys EXAMINERS
ALTER TABLE examiners
 ADD CONSTRAINT fk_exm_ranks
 FOREIGN KEY (exmnr_rank_id)
 REFERENCES ranks (rank_id);

ALTER TABLE examiners
 ADD CONSTRAINT fk_exm_examination_places
 FOREIGN KEY (exmnr_place)
 REFERENCES examination_places (ep_id);

ALTER TABLE examiners
 ADD CONSTRAINT fk_exm_users_created_by
 FOREIGN KEY (created_by)
 REFERENCES users (user_id);

ALTER TABLE examiners
 ADD CONSTRAINT fk_exm_users_updated_by
 FOREIGN KEY (updated_by)
 REFERENCES users (user_id);

-- Foreign key DOCUMENT_TYPES
ALTER TABLE document_types
 ADD CONSTRAINT fk_dt_users_created_by
 FOREIGN KEY (created_by)
 REFERENCES users(user_id);

ALTER TABLE document_types
 ADD CONSTRAINT fk_dt_users_updated_by
 FOREIGN KEY (updated_by)
 REFERENCES users(user_id);

-- Foreign key DEPARTMENTS
ALTER TABLE departments
 ADD CONSTRAINT fk_dept_users_created_by
 FOREIGN KEY (created_by)
 REFERENCES users (user_id);

ALTER TABLE departments
 ADD CONSTRAINT fk_dept_users_updated_by
 FOREIGN KEY (updated_by)
 REFERENCES users (user_id);


-- Foreign key CODEC_REQUIREMENTS
ALTER TABLE codec_requirements
 ADD CONSTRAINT fk_cr_prism 
 FOREIGN KEY (pc_seq)
 REFERENCES prism_codec (pc_seq);

ALTER TABLE codec_requirements
 ADD CONSTRAINT fk_cr_reqt
 FOREIGN KEY (reqt_code)
 REFERENCES requirements (reqt_code);


-- Foreign key CODEC_KO_CODEC
ALTER TABLE codec_ko_codec
 ADD CONSTRAINT fk_ckc_prism
 FOREIGN KEY (pc_seq) 
 REFERENCES prism_codec(pc_seq);

ALTER TABLE codec_ko_codec
 ADD CONSTRAINT fk_ckc_ko
 FOREIGN KEY (ko_seq)
 REFERENCES prism_codec (pc_seq);

-- Foreign key CLIENT_TYPES
ALTER TABLE client_types
 ADD CONSTRAINT fk_ct_users_created_by
 FOREIGN KEY (created_by)
 REFERENCES users(user_id);

ALTER TABLE client_types
 ADD CONSTRAINT fk_ct_users_updated_by
 FOREIGN KEY (updated_by)
 REFERENCES users(user_id);

-- Foreign Key CLIENT_DATA_SHEETS
ALTER TABLE client_data_sheets
 ADD CONSTRAINT fk_cds_assessment_requests
 FOREIGN KEY (cds_reference_num)
 REFERENCES assessment_requests (reference_num);

ALTER TABLE client_data_sheets
 ADD CONSTRAINT fk_cds_clients
 FOREIGN KEY (cds_client_num)
 REFERENCES clients (client_id);
 
-- Foreign key AUTO_ASSIGNMENT_CRITERIA
ALTER TABLE auto_assignment_criteria
 ADD CONSTRAINT fk_aac_users_created_by
 FOREIGN KEY (created_by)
 REFERENCES users(user_id);

ALTER TABLE auto_assignment_criteria
 ADD CONSTRAINT fk_aac_users_updated_by
 FOREIGN KEY (updated_by)
 REFERENCES users(user_id);
 
-- Foreign Key AUTO_ASSIGNMENTS
ALTER TABLE auto_assignments
 ADD CONSTRAINT fk_aa_users
 FOREIGN KEY (user_id)
 REFERENCES users (user_id);
 
ALTER TABLE auto_assignments
 ADD CONSTRAINT fk_aa_auto_assignment_cri
 FOREIGN KEY (aac_id)
 REFERENCES auto_assignment_criteria (aac_id);

ALTER TABLE auto_assignments
 ADD CONSTRAINT fk_aa_users_created_by
 FOREIGN KEY (created_by)
 REFERENCES users(user_id);

ALTER TABLE auto_assignments
 ADD CONSTRAINT fk_aa_users_updated_by
 FOREIGN KEY (updated_by)
 REFERENCES users(user_id);

-- Foreign Key ASSESSMENT_REQUESTS
ALTER TABLE assessment_requests
 ADD CONSTRAINT fk_ar_lines_of_business
 FOREIGN KEY (lob)
 REFERENCES lines_of_business (lob_code);

ALTER TABLE assessment_requests
 ADD CONSTRAINT fk_ar_clients_insured
 FOREIGN KEY (insured_client_id)
 REFERENCES clients (client_id);

ALTER TABLE assessment_requests
 ADD CONSTRAINT fk_ar_clients_owner
 FOREIGN KEY (owner_client_id)
 REFERENCES clients (client_id);

ALTER TABLE assessment_requests
 ADD CONSTRAINT fk_ar_branches
 FOREIGN KEY (branch_id)
 REFERENCES sunlife_offices (slo_office_code);

ALTER TABLE assessment_requests
 ADD CONSTRAINT fk_ar_users_agent
 FOREIGN KEY (agent_id)
 REFERENCES users (user_id);

ALTER TABLE assessment_requests
 ADD CONSTRAINT fk_ar_status
 FOREIGN KEY (status_id)
 REFERENCES  status (stat_id);

ALTER TABLE assessment_requests
 ADD CONSTRAINT fk_ar_users_assigned_to
 FOREIGN KEY (assigned_to)
 REFERENCES users (user_id);

ALTER TABLE assessment_requests
 ADD CONSTRAINT fk_ar_users_underwriter
 FOREIGN KEY (underwriter)
 REFERENCES users (user_id);

ALTER TABLE assessment_requests
 ADD CONSTRAINT fk_ar_folder_location
 FOREIGN KEY (folder_location)
 REFERENCES users (user_id);

ALTER TABLE assessment_requests
 ADD CONSTRAINT fk_ar_users_created_by
 FOREIGN KEY (created_by)
 REFERENCES users (user_id);

ALTER TABLE assessment_requests
 ADD CONSTRAINT fk_ar_users_updated_by
 FOREIGN KEY (updated_by)
 REFERENCES users (user_id);


-- Foreign Key ACTIVITY_LOGS
ALTER TABLE activity_logs
 ADD CONSTRAINT fk_act_users
 FOREIGN KEY (user_id)
 REFERENCES users (user_id);

ALTER TABLE activity_logs
 ADD CONSTRAINT fk_act_status
 FOREIGN KEY (stat_id)
 REFERENCES status (stat_id);

ALTER TABLE activity_logs
 ADD CONSTRAINT fk_act_assessment_requests
 FOREIGN KEY (uar_reference_num)
 REFERENCES assessment_requests (reference_num);

 
-- Foreign Keys ACCESS_TEMPLATE_DETAILS
ALTER TABLE access_template_details
 ADD CONSTRAINT fk_atd_access_template
 FOREIGN KEY (templt_id)
 REFERENCES access_templates (templt_id);

ALTER TABLE access_template_details
 ADD CONSTRAINT fk_atd_pages_access
 FOREIGN KEY (pa_id)
 REFERENCES pages_access (pa_id); 

ALTER TABLE access_template_details
 ADD CONSTRAINT fk_atd_users_created_by
 FOREIGN KEY (created_by)
 REFERENCES users(user_id);

ALTER TABLE access_template_details
 ADD CONSTRAINT fk_atd_users_updated_by
 FOREIGN KEY (updated_by)
 REFERENCES users(user_id);

-- Foreign Key ACCESS_TEMPLATES
ALTER TABLE access_templates
 ADD CONSTRAINT fk_at_role 
 FOREIGN KEY (role_id)
 REFERENCES roles (role_code);
 
ALTER TABLE access_templates
 ADD CONSTRAINT fk_at_users_created_by
 FOREIGN KEY (created_by)
 REFERENCES users(user_id);

ALTER TABLE access_templates
 ADD CONSTRAINT fk_at_users_updated_by
 FOREIGN KEY (updated_by)
 REFERENCES users(user_id);

-- Foreign key ACCESSES
ALTER TABLE accesses
 ADD CONSTRAINT fk_acc_users_created_by
 FOREIGN KEY (created_by)
 REFERENCES users(user_id);
 
ALTER TABLE accesses
 ADD CONSTRAINT fk_acc_users_updated_by
 FOREIGN KEY (updated_by)
 REFERENCES users(user_id);

-- Foreign key UW_ASSESSMENT_REQUESTS
ALTER TABLE uw_assessment_requests
 ADD CONSTRAINT fk_uw_assessment_requests
 FOREIGN KEY (reference_num)
 REFERENCES assessment_requests(reference_num);
 
-- Foreign key ATTACHMENTS
ALTER TABLE attachments
 ADD CONSTRAINT fk_attachments
 FOREIGN KEY (uar_reference_num)
 REFERENCES assessment_requests(reference_num);

Alter table attachments
 ADD CONSTRAINT fk_req_attachments
 FOREIGN KEY (requirement_id)
 REFERENCES policy_requirements(pr_requirement_id);
 
-- Check Constraint STATUS
ALTER TABLE status
 ADD CONSTRAINT chk_stat_type
 CHECK (stat_type IN ('AR','NM','M'));

-- Check Constraints PAGES_ACCESS
ALTER TABLE pages_access
 ADD CONSTRAINT chk_pa_enabled_ind
 CHECK (pa_enabled_ind in ('0','1')); 
 
-- Check Constraint JOB_SCHEDULES
ALTER TABLE job_schedules
 ADD CONSTRAINT chk_sched_type
 CHECK (sched_type IN ('ME', 'AE'));
 
-- Check Constraints CLIENTS
ALTER TABLE clients
  ADD CONSTRAINT chk_smoker
  CHECK (cl_smoker IN ('0','1'));
 
-- Check Constraint AUTO_ASSIGNMENT_CRITERIA
ALTER TABLE auto_assignment_criteria
 ADD CONSTRAINT chk_acc_status
 CHECK (aac_status IN ('0','1'));
 
-- Check Constraint ACCESS_TEMPLATE_DETAILS
ALTER TABLE access_template_details
 ADD CONSTRAINT chk_atd_access_ind
 CHECK (atd_access_ind in ('0','1'));
 