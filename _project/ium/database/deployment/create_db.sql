PROMPT IUM_APP_USER role created
CREATE ROLE ium_app_user;


PROMPT CREATE TABLE accesses
CREATE TABLE accesses
(
  acc_id 			NUMBER(2) NOT NULL
, acc_desc 			VARCHAR2(25)
, created_by		VARCHAR2(10)
, created_date		TIMESTAMP(6)
, updated_by		VARCHAR2(10)
, updated_date		TIMESTAMP(6)
);

-- Public synonym
CREATE PUBLIC SYNONYM accesses FOR accesses;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON accesses TO ium_app_user;


PROMPT CREATE TABLE access_templates
CREATE TABLE access_templates
(
  templt_id 		NUMBER(5) NOT NULL
, role_id    		VARCHAR2(15)
, templt_desc 		VARCHAR2(25)
, created_by		VARCHAR2(10)
, created_date		TIMESTAMP(6)
, updated_by		VARCHAR2(10)
, updated_date		TIMESTAMP(6)
);

-- Public synonym
CREATE PUBLIC SYNONYM access_templates FOR access_templates;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON access_templates TO ium_app_user;


PROMPT CREATE TABLE access_template_details
CREATE TABLE access_template_details
(
  templt_id			NUMBER(5) NOT NULL
, pa_id				NUMBER(5) NOT NULL
, atd_access_ind	VARCHAR2(1)
, created_by		VARCHAR2(10)
, created_date		TIMESTAMP(6)
, updated_by		VARCHAR2(10)
, updated_date		TIMESTAMP(6)
);

-- Public synonym
CREATE PUBLIC SYNONYM access_template_details FOR access_template_details;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON access_template_details TO ium_app_user;


PROMPT CREATE TABLE activity_logs 
CREATE TABLE activity_logs 
(
  act_id			NUMBER(10) NOT NULL
, uar_reference_num	VARCHAR2(15) 
, stat_id 			NUMBER(4)
, act_datetime 		TIMESTAMP(6)
, user_id 			VARCHAR2(10)
, act_assigned_to 	VARCHAR2(10)
, act_elapse_time 	NUMBER(6,2)
); 

-- Public synonym
CREATE PUBLIC SYNONYM activity_logs FOR activity_logs;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON activity_logs TO ium_app_user;

PROMPT CREATE TABLE assessment_requests
CREATE TABLE assessment_requests
(
  reference_num 	VARCHAR2(15) NOT NULL
, lob				VARCHAR2(2)  
, source_system 	VARCHAR2(10)
, amount_covered 	NUMBER(12,2)
, premium			NUMBER(12,2)
, insured_client_id VARCHAR2(10)
, owner_client_id 	VARCHAR2(10)
, branch_id 		VARCHAR2(15) 
, agent_id			VARCHAR2(8)
, status_id 		NUMBER(4)
, status_date 		DATE
, assigned_to 		VARCHAR2(10)
, underwriter 		VARCHAR2(10)
, folder_location 	VARCHAR2(10)
, remarks			VARCHAR2(255)
, app_received_date DATE
, date_forwarded 	DATE
, ar_transmit_ind	VARCHAR2(1)
, created_by 		VARCHAR2(10) 
, created_date 		TIMESTAMP(6)
, updated_by 		VARCHAR2(10)
, updated_date 		TIMESTAMP(6)
, uw_remarks		LONG
, ar_auto_settle_ind VARCHAR2(1)
);


-- Public synonym
CREATE PUBLIC SYNONYM assessment_requests FOR assessment_requests;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON assessment_requests TO ium_app_user;

PROMPT CREATE TABLE ATTACHMENTS
CREATE TABLE ATTACHMENTS
(
  uar_reference_num  varchar2(15) not null
  ,requirement_id	 number(5) not null
);

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON ATTACHMENTS TO ium_app_user;


PROMPT CREATE TABLE audit_trails
CREATE TABLE audit_trails
(
  tran_num 			NUMBER(10) NOT NULL
, tran_record 		NUMBER(2)
, tran_type 		VARCHAR2(6)
, tran_datetime 	TIMESTAMP(6)
, user_id 			VARCHAR2(10)
, uar_reference_num VARCHAR2(30)
, tran_changed_from VARCHAR2(2000)
, tran_changed_to 	VARCHAR2(2000)
);


-- Public synonym
CREATE PUBLIC SYNONYM audit_trails FOR audit_trails;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON audit_trails TO ium_app_user;


PROMPT CREATE TABLE auto_assignments 
CREATE TABLE auto_assignments 
(
  aa_id 				NUMBER(3) NOT NULL
, user_id 				VARCHAR2(10)
, aac_id 				NUMBER(3)
, criteria_field_value 	VARCHAR2(25)
, created_by			VARCHAR2(10)
, created_date			TIMESTAMP(6)
, updated_by			VARCHAR2(10)
, updated_date			TIMESTAMP(6)
);



-- Public synonym
CREATE PUBLIC SYNONYM auto_assignments FOR auto_assignments;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON auto_assignments TO ium_app_user;


PROMPT CREATE TABLE auto_assignment_criteria
CREATE TABLE auto_assignment_criteria
(
  aac_id 				NUMBER(3) NOT NULL
, aac_field 			VARCHAR2(20)
, aac_desc 				VARCHAR2(40)
, aac_status			VARCHAR2(1)
, created_by			VARCHAR2(10)
, created_date			TIMESTAMP(6)
, updated_by			VARCHAR2(10)
, updated_date			TIMESTAMP(6)
);


-- Public synonym
CREATE PUBLIC SYNONYM auto_assignment_criteria FOR auto_assignment_criteria;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON auto_assignment_criteria TO ium_app_user;


PROMPT CREATE TABLE clients 
CREATE TABLE clients 
(
  client_id 		VARCHAR2(10) NOT NULL
, cl_last_name 		VARCHAR2(40)
, cl_given_name 	VARCHAR2(25)
, cl_middle_name 	VARCHAR2(25)
, cl_oth_last_name 	VARCHAR2(40)
, cl_oth_given_name 	VARCHAR2(25)
, cl_oth_middle_name 	VARCHAR2(25)
, cl_title 		VARCHAR2(15)
, cl_suffix 		VARCHAR2(10)
, cl_age 		NUMBER(2)
, cl_sex 		VARCHAR2(1)
, cl_birth_date 	DATE
, cl_smoker 		VARCHAR2(1)
);


  
-- Public synonym
CREATE PUBLIC SYNONYM clients FOR clients;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON clients TO ium_app_user;
  

PROMPT CREATE TABLE client_data_sheets 
CREATE TABLE client_data_sheets 
(
  cds_reference_num 		VARCHAR2(15) NOT NULL
, cds_client_num 		VARCHAR2(10) NOT NULL
, cds_client_type 		VARCHAR2(1)
, cds_cl_height_feet 		NUMBER(1)
, cds_cl_height_inches 		NUMBER(2)
, cds_cl_weight 		NUMBER(3)
, cds_basic_mr 			NUMBER(4,1)
, cds_basic_ccr_mr 		NUMBER(4,1)
, cds_basic_apdp_mr 		NUMBER(4,1)
, cds_smkng_basic_mr 		NUMBER(4,1)
, cds_smkng_ccr_mr 		NUMBER(4,1)
, cds_smkng_apdb_mr 		NUMBER(4,1)
, cds_fhb_basic_mr 		NUMBER(4,1)
, cds_fhb_ccr_mr 		NUMBER(4,1)
, cds_fhb_apdb_mr 		NUMBER(4,1)
, cds_tam_basic_mr 		NUMBER(4,1)
, cds_tam_ccr_mr 		NUMBER(4,1)
, cds_tam_apdb_mr 		NUMBER(4,1)
, cds_app_br_amt        NUMBER(13,5)
, cds_app_ad_amt        NUMBER(13,5)
, cds_pend_br_amt       NUMBER(13,5)
, cds_pend_ad_amt       NUMBER(13,5)
, cds_infc_br_amt       NUMBER(13,5)
, cds_infc_ad_amt       NUMBER(13,5)
, cds_laps_br_amt       NUMBER(13,5)
, cds_laps_ad_amt       NUMBER(13,5)
, cds_oins_br_amt       NUMBER(13,5)
, cds_oins_ad_amt       NUMBER(13,5)
, cds_infc_ccr_amt      NUMBER(13,5)
, cds_infc_apdb_amt     NUMBER(13,5)
, cds_infc_hib_amt      NUMBER(13,5)
, cds_infc_fmb_amt      NUMBER(13,5)
, cds_tot_other_companies 	NUMBER(13,2)
, cds_tot_ccr_coverage 		NUMBER(13,2)
, cds_tot_apdb_coverage 	NUMBER(13,2)
, cds_tot_hib_coverage 		NUMBER(13,2)
, cds_tot_fbbmb_coverage 	NUMBER(13,2)
, cds_tot_reinsured_amount 	NUMBER(13,2)
, cds_tot_reins_amt			NUMBER(13,5)
, cds_tot_existing_sl_br    NUMBER(13,5)
, cds_tot_existing_sl_ad	NUMBER(13,5)
, cds_tot_sl_oins_br		NUMBER(13,5)
, cds_tot_sl_oins_ad		NUMBER(13,5)
);


-- Public synonym
CREATE PUBLIC SYNONYM client_data_sheets FOR client_data_sheets;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON client_data_sheets TO ium_app_user;


PROMPT CREATE TABLE client_types 
CREATE TABLE client_types 
(
  clt_code 		VARCHAR2(1) NOT NULL
, clt_desc 		VARCHAR2(25)
, created_by	VARCHAR2(10)
, created_date	TIMESTAMP(6)
, updated_by	VARCHAR2(10)
, updated_date	TIMESTAMP(6)
);



-- Public synonym
CREATE PUBLIC SYNONYM client_types FOR client_types;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON client_types TO ium_app_user;


PROMPT CREATE TABLE CODEC_KO_CODEC
CREATE TABLE CODEC_KO_CODEC
(
 PC_SEQ		NUMBER(4) NOT NULL
,KO_SEQ		NUMBER(5) NOT NULL
);


-- Public synonym
CREATE PUBLIC SYNONYM codec_ko_codec FOR codec_ko_codec;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON codec_ko_codec TO ium_app_user;


PROMPT CREATE TABLE CODEC_REQUIREMENTS
CREATE TABLE CODEC_REQUIREMENTS
(
  PC_SEQ		NUMBER(4) NOT NULL
, REQT_CODE		VARCHAR2(5) NOT NULL
);


-- Public synonym
CREATE PUBLIC SYNONYM codec_requirements FOR codec_requirements;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON codec_requirements TO ium_app_user;


PROMPT CREATE TABLE departments
CREATE TABLE departments
(
  dept_code	VARCHAR2(10) NOT NULL
, dept_desc	VARCHAR2(50)
, created_by	VARCHAR2(10)
, created_date	TIMESTAMP(6)
, updated_by	VARCHAR2(10)
, updated_date	TIMESTAMP(6)
);


-- Public synonym
CREATE PUBLIC SYNONYM departments FOR departments;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON departments TO ium_app_user;


PROMPT CREATE TABLE document_types 
CREATE TABLE document_types 
(
  doc_code 		VARCHAR2(3) NOT NULL
, doc_desc 		VARCHAR2(40)
, created_by	VARCHAR2(10)
, created_date	TIMESTAMP(6)
, updated_by	VARCHAR2(10)
, updated_date	TIMESTAMP(6)
);


-- Public synonym
CREATE PUBLIC SYNONYM document_types FOR document_types;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON document_types TO ium_app_user;


PROMPT CREATE TABLE examination_areas
CREATE TABLE examination_areas
(
  ea_id 		NUMBER(3)
, ea_desc 		VARCHAR2(25)
, created_by 	VARCHAR2(10)
, created_date	TIMESTAMP(6)
, updated_by	VARCHAR2(10)
, updated_date	TIMESTAMP(6)
);


-- Public synonym
CREATE PUBLIC SYNONYM examination_areas FOR examination_areas;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON examination_areas TO ium_app_user;


PROMPT CREATE TABLE examination_places 
CREATE TABLE examination_places 
(
  ep_id 		NUMERIC(3) NOT NULL
, ep_desc 		VARCHAR2(25)
, created_by	VARCHAR2(10)
, created_date	TIMESTAMP(6)
, updated_by	VARCHAR2(10)
, updated_date	TIMESTAMP(6)
);


-- Public synonym
CREATE PUBLIC SYNONYM examination_places FOR examination_places;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON examination_places TO ium_app_user;


PROMPT CREATE TABLE examiners
CREATE TABLE examiners
(
  exmnr_id 			NUMBER(5) NOT NULL
, exmnr_last_name 		VARCHAR2(40)
, exmnr_given_name 		VARCHAR2(25)
, exmnr_middle_name 		VARCHAR2(25)
, exmnr_salutation 		VARCHAR2(25)
, exmnr_sex 			VARCHAR2(1)
, exmnr_rank_id 		NUMBER(3)
, exmnr_rank_eff_date 		DATE
, exmnr_dob 			DATE
, exmnr_contact_num 		VARCHAR2(13)
, exmnr_fax_num 		VARCHAR2(13)
, exmnr_bus_addr_line1 		VARCHAR2(60)
, exmnr_bus_addr_line2 		VARCHAR2(60)
, exmnr_bus_addr_line3 		VARCHAR2(60)
, exmnr_bus_city 		VARCHAR2(25)
, exmnr_province 		VARCHAR2(25)
, exmnr_country 		VARCHAR2(25)
, exmnr_zipcode 		NUMERIC(5)
, exmnr_mail_to_bus_ind 	VARCHAR2(1)
, exmnr_clinic_hrs_from 	TIMESTAMP
, exmnr_clinic_hrs_to 		TIMESTAMP
, exmnr_oth_ins_co 		VARCHAR2(60)
, exmnr_pre_emp_srvce_ind	VARCHAR2(1)
, exmnr_tin 			NUMBER(16)
, exmnr_accreditation_ind 	VARCHAR2(1)
, exmnr_accredit_date 		DATE
, exmnr_area 			VARCHAR2(5)
, exmnr_place 			NUMBER(3)
, exmnr_mobile_num		VARCHAR2(13)
, created_by 			VARCHAR2(10) 
, created_date 			TIMESTAMP
, updated_by 			VARCHAR2(10)
, updated_date 			TIMESTAMP
);	

-- Public synonym
CREATE PUBLIC SYNONYM examiners FOR examiners;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON examiners TO ium_app_user;


PROMPT CREATE TABLE examiner_specializations 
CREATE TABLE examiner_specializations 
(
  exmnr_id 		NUMBER(5) NOT NULL
, spl_id 		NUMBER(3) NOT NULL
, spl_effective_date 	DATE
, created_by        VARCHAR2(10)
, created_date      TIMESTAMP(6)
, updated_by        VARCHAR2(10)
, updated_date      TIMESTAMP(6)
);

-- Public synonym
CREATE PUBLIC SYNONYM examiner_specializations FOR examiner_specializations;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON examiner_specializations TO ium_app_user;


PROMPT CREATE TABLE exception_batch
CREATE TABLE exception_batch
(
 BATCH_ID          NUMBER(8) NOT NULL
,TIMESTAMP         DATE
,INTERFACE         VARCHAR2(10)
,RECORD_TYPE       VARCHAR2(30)
,REQUEST           VARCHAR2(160)
,RESPONSE          LONG
);


-- Public synonym
CREATE PUBLIC SYNONYM exception_batch FOR exception_batch;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON exception_batch TO ium_app_user;



PROMPT CREATE TABLE EXCEPTION_DETAILS
CREATE TABLE EXCEPTION_DETAILS
(
  EXCEPTION_ID     NUMBER(8)
, MESSAGE          VARCHAR2(50)
);

-- Public synonym
CREATE PUBLIC SYNONYM exception_details FOR exception_details;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON exception_details TO ium_app_user;



PROMPT CREATE TABLE EXCEPTION_LOG
CREATE TABLE EXCEPTION_LOG
(
  EXCEPTION_ID     NUMBER(8) NOT NULL
, BATCH_ID         NUMBER(8)
, REFERENCE_NUM    VARCHAR2(15)
, XML_RECORD       LONG
);

-- Public synonym
CREATE PUBLIC SYNONYM exception_log FOR exception_log;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON exception_log TO ium_app_user;



PROMPT CREATE TABLE folder_documents 
CREATE TABLE folder_documents 
(
  doc_id		NUMBER(8) NOT NULL
, uar_reference_num 	VARCHAR2(15)
, doc_code 		VARCHAR2(3)
, doc_date_rcvd 	DATE
, doc_received_by 	VARCHAR2(10)
, doc_reference_num 	NUMBER(8)
, doc_date_requested	DATE
); 

-- Public synonym
CREATE PUBLIC SYNONYM folder_documents FOR folder_documents;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON folder_documents TO ium_app_user;



PROMPT CREATE TABLE FORM_FIELDS
CREATE TABLE FORM_FIELDS
(
  FF_ID                  NUMBER(2) not null
, FF_NAME                VARCHAR2(25)
, FF_APP_MAPPING         NUMBER(2)
);
 
-- Public synonym
CREATE PUBLIC SYNONYM form_fields FOR form_fields;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON form_fields TO ium_app_user;



PROMPT CREATE TABLE holidays 
CREATE TABLE holidays 
(
  hol_id 		NUMBER(3) NOT NULL
, hol_date 		DATE
, hol_desc 		VARCHAR2(40)
, created_by	VARCHAR2(10)
, created_date	TIMESTAMP(6)
, updated_by	VARCHAR2(10)
, updated_date	TIMESTAMP(6)
);


-- Public synonym
CREATE PUBLIC SYNONYM holidays FOR holidays;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON holidays TO ium_app_user;



PROMPT CREATE TABLE impairments 
CREATE TABLE impairments 
(
  imp_impairement_id	NUMBER(5) NOT NULL
, cl_client_num		VARCHAR2(10)
, uar_reference_num 	VARCHAR2(15)
, mib_impairment_code   VARCHAR2(6)
, imp_desc 		VARCHAR2(25)
, imp_relationship	VARCHAR2(1)
, imp_origin		VARCHAR2(1)
, imp_date 		DATE
, imp_height_feet 	NUMBER(4,2)
, imp_height_inches 	NUMBER(5,2)
, imp_weight 		NUMBER(5,2)
, imp_blood_pressure	VARCHAR2(7)
, imp_confirmation 	VARCHAR2(1)
, imp_export_date 	DATE
, created_by 		VARCHAR2(10) 
, created_date 		TIMESTAMP
, updated_by 		VARCHAR2(10)
, updated_date 		TIMESTAMP
);

-- Public synonym
CREATE PUBLIC SYNONYM impairments FOR impairments;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON impairments TO ium_app_user;



PROMPT CREATE TABLE job_schedules
CREATE TABLE job_schedules
(
  sched_type	VARCHAR2(2) NOT NULL
, sched_desc     VARCHAR2(100)
, sched_time     TIMESTAMP
, created_by	VARCHAR2(10)
, created_date	TIMESTAMP(6)
, updated_by	VARCHAR2(10)
, updated_date	TIMESTAMP(6)
);


-- Public synonym
CREATE PUBLIC SYNONYM job_schedules FOR job_schedules;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON job_schedules TO ium_app_user;



PROMPT create table KICKOUT_CODES
create table KICKOUT_CODES
(
  KO_SEQ		NUMBER(5) NOT NULL
, KO_MESSAGE	VARCHAR2(100)
);

-- Public synonym
CREATE PUBLIC SYNONYM kickout_codes FOR kickout_codes;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON kickout_codes TO ium_app_user;



PROMPT CREATE TABLE kickout_messages
CREATE TABLE kickout_messages
(
  ko_sequence_num 	NUMBER(5) NOT NULL
, uar_reference_num 	VARCHAR2(15)
, ko_message_text 	VARCHAR2(68)
, client_id 		VARCHAR2(10)
, ko_fail_response 	VARCHAR2(15)
); 

-- Public synonym
CREATE PUBLIC SYNONYM kickout_messages FOR kickout_messages;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON kickout_messages TO ium_app_user;



PROMPT CREATE TABLE laboratories 
CREATE TABLE laboratories 
(
  lab_id 		NUMBER(5) NOT NULL
, lab_name 		VARCHAR2(40)
, lab_contact_person 	VARCHAR2(60)
, lab_office_num 	VARCHAR2(13)
, lab_office_num2 	VARCHAR2(13)
, lab_fax_num 		VARCHAR2(13)
, lab_bus_addr_line1 	VARCHAR2(60)
, lab_bus_addr_line2 	VARCHAR2(60)
, lab_bus_addr_line3 	VARCHAR2(60)
, lab_city 		VARCHAR2(25)
, lab_province 		VARCHAR2(25)
, lab_country 		VARCHAR2(25)
, lab_zipcode 		NUMBER(5)
, lab_branch_name 	VARCHAR2(40)
, lab_branch_addr_line1 VARCHAR2(60)
, lab_branch_addr_line2 VARCHAR2(60)
, lab_branch_addr_line3 VARCHAR2(60)
, lab_branch_city 	VARCHAR2(20)
, lab_branch_province 	VARCHAR2(20)
, lab_branch_country 	VARCHAR2(20)
, lab_branch_zipcode 	NUMBER(5)
, lab_branch_phone 	VARCHAR2(13)
, lab_branch_fax_num 	VARCHAR2(13)
, lab_accredit_ind 	VARCHAR2(1)
, lab_accredit_date 	DATE
, lab_email_address		VARCHAR2(40)
, created_by 		VARCHAR2(10) 
, created_date 		TIMESTAMP
, updated_by 		VARCHAR2(10)
, updated_date 		TIMESTAMP
);

-- Public synonym
CREATE PUBLIC SYNONYM laboratories FOR laboratories;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON laboratories TO ium_app_user;



PROMPT CREATE TABLE laboratory_tests 
CREATE TABLE laboratory_tests 
(
  lab_id 		NUMBER(5) NOT NULL
, test_id 		NUMBER(5) NOT NULL
, test_fee 		NUMBER(9,2)
, lab_test_status 	VARCHAR2(10)
, created_by	VARCHAR2(10)
, created_date	TIMESTAMP(6)
, updated_by	VARCHAR2(10)
, updated_date	TIMESTAMP(6)
);

-- Public synonym
CREATE PUBLIC SYNONYM laboratory_tests FOR laboratory_tests;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON laboratory_tests TO ium_app_user;



PROMPT CREATE TABLE lines_of_business 
CREATE TABLE lines_of_business 
(
  lob_code 		VARCHAR2(2) NOT NULL
, lob_desc 		VARCHAR2(25)
, created_by	VARCHAR2(10)
, created_date	TIMESTAMP(6)
, updated_by	VARCHAR2(10)
, updated_date	TIMESTAMP(6)
);

-- Public synonym
CREATE PUBLIC SYNONYM lines_of_business FOR lines_of_business;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON lines_of_business TO ium_app_user;



PROMPT CREATE TABLE lob_documents 
CREATE TABLE lob_documents 
(
  lob_code 		VARCHAR2(2) NOT NULL
, doc_code 		VARCHAR2(3) NOT NULL
, created_by	VARCHAR2(10)
, created_date	TIMESTAMP(6)
, updated_by	VARCHAR2(10)
, updated_date	TIMESTAMP(6)
);


-- Public synonym
CREATE PUBLIC SYNONYM lob_documents FOR lob_documents;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON lob_documents TO ium_app_user;



PROMPT CREATE TABLE lob_requirements
CREATE TABLE lob_requirements
(
  lob_code			VARCHAR2(2) NOT NULL
, reqt_code			VARCHAR2(5) NOT NULL
, remarks			LONG
, created_by	VARCHAR2(10)
, created_date	TIMESTAMP(6)
, updated_by	VARCHAR2(10)
, updated_date	TIMESTAMP(6)
); 

-- Public synonym
CREATE PUBLIC SYNONYM lob_requirements FOR lob_requirements;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON lob_requirements TO ium_app_user;



PROMPT CREATE TABLE lob_status 
CREATE TABLE lob_status 
(
  lob_code	VARCHAR2(2) NOT NULL
, admin_status_code VARCHAR2(5) 
, admin_status_type VARCHAR2(1) 
, stat_id	NUMBER(4) NOT NULL
, created_by	VARCHAR2(10)
, created_date	TIMESTAMP(6)
, updated_by	VARCHAR2(10)
, updated_date	TIMESTAMP(6)
);

-- Public synonym
CREATE PUBLIC SYNONYM lob_status FOR lob_status;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON lob_status TO ium_app_user;



PROMPT CREATE TABLE medical_bills
CREATE TABLE medical_bills
(
  mb_bill_number	NUMBER(8) NOT NULL
, uar_reference_num 	VARCHAR2(15)
, exmnr_id 		NUMBER(5)
, lab_id 		NUMBER(5)
, mb_fee		NUMBER(8,2)
, mb_date_posted	TIMESTAMP
, created_by	VARCHAR2(10)
, created_date	TIMESTAMP(6)
, updated_by	VARCHAR2(10)
, updated_date	TIMESTAMP(6)
); 

-- Public synonym
CREATE PUBLIC SYNONYM medical_bills FOR medical_bills;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON medical_bills TO ium_app_user;



PROMPT CREATE TABLE medical_notes
CREATE TABLE medical_notes
(
  mn_notes_id		NUMBER(8) NOT NULL
, uar_reference_num 	VARCHAR2(15)
, mn_posted_by 		VARCHAR2(10)
, mn_post_date 		DATE
, mn_notes  		LONG
, mn_recipient		VARCHAR2(10)
); 

-- Public synonym
CREATE PUBLIC SYNONYM medical_notes FOR medical_notes;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON medical_notes TO ium_app_user;



PROMPT CREATE TABLE medical_records
CREATE TABLE medical_records
(
  med_record_id 	NUMBER(8) NOT NULL
, cl_client_num 	VARCHAR2(10)
, agent_id 		VARCHAR2(8) 
, branch_id 		VARCHAR2(15)
, med_status 		NUMBER(4)
, med_lab_test_ind 	VARCHAR2(1)
, med_examination_place NUMBER(3)
, med_lab_id 		NUMBER(5)
, med_test_id 		NUMBER(5)
, exmnr_id 			NUMBER(5)          
, med_appointment_date 	TIMESTAMP
, med_date_conducted 	DATE
, med_date_received 	DATE
, med_follow_up_num 	NUMERIC(2)
, med_follow_up_date 	DATE
, med_remarks 		LONG
, med_validity_date 	DATE
, med_7day_memo_ind 	VARCHAR2(1)
, med_7day_memo_date 	DATE
, med_reqt_reason 	VARCHAR2(100)
, med_section_id 	VARCHAR2(15)
, med_requesting_party 	VARCHAR2(5)
, med_department_id 	VARCHAR2(10)
, med_chargeable_to 	VARCHAR2(10)
, med_paid_by_agent_ind VARCHAR2(1)
, med_amount 		NUMBER(8)
, med_request_date	DATE
, med_request_loa_ind 	VARCHAR2(1)
, med_results_rcvd 	VARCHAR2(1)
, created_by 		VARCHAR2(10) 
, created_date 		TIMESTAMP
, updated_by 		VARCHAR2(10)
, updated_date 		TIMESTAMP
, cl_last_name		VARCHAR2(40)
, cl_given_name		VARCHAR2(25)
, cl_birth_date		DATE
); 

-- Public synonym
CREATE PUBLIC SYNONYM medical_records FOR medical_records;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON medical_records TO ium_app_user;


PROMPT CREATE TABLE mib_actions 
CREATE TABLE mib_actions 
(
  mib_act_code 		VARCHAR2(3) NOT NULL
, mib_act_desc 		VARCHAR2(40)
, created_by	VARCHAR2(10)
, created_date	TIMESTAMP(6)
, updated_by	VARCHAR2(10)
, updated_date	TIMESTAMP(6)
);


-- Public synonym
CREATE PUBLIC SYNONYM mib_actions FOR mib_actions;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON mib_actions TO ium_app_user;



PROMPT CREATE TABLE mib_impairments
CREATE TABLE mib_impairments
(
  mib_impairment_code	VARCHAR2(6) NOT NULL
, mib_impairment_desc	VARCHAR2(104)
, created_by	VARCHAR2(10)
, created_date	TIMESTAMP(6)
, updated_by	VARCHAR2(10)
, updated_date	TIMESTAMP(6)
);

-- Public synonym
CREATE PUBLIC SYNONYM mib_impairments FOR mib_impairments;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON mib_impairments TO ium_app_user;



PROMPT CREATE TABLE mib_letters 
CREATE TABLE mib_letters 
(
  mib_lttr_code 	VARCHAR2(1) NOT NULL
, mib_lttr_desc 	VARCHAR2(40)
, created_by	VARCHAR2(10)
, created_date	TIMESTAMP(6)
, updated_by	VARCHAR2(10)
, updated_date	TIMESTAMP(6)
); 

-- Public synonym
CREATE PUBLIC SYNONYM mib_letters FOR mib_letters;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON mib_letters TO ium_app_user;




PROMPT CREATE TABLE mib_numbers 
CREATE TABLE mib_numbers 
(
  mib_num_code 		VARCHAR2(1) NOT NULL
, mib_num_desc 		VARCHAR2(40)
, created_by	VARCHAR2(10)
, created_date	TIMESTAMP(6)
, updated_by	VARCHAR2(10)
, updated_date	TIMESTAMP(6)
);

 -- Public synonym
CREATE PUBLIC SYNONYM mib_numbers FOR mib_numbers;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON mib_numbers TO ium_app_user;



PROMPT CREATE TABLE mib_schedules  
CREATE TABLE mib_schedules  
(
  mib_sked_id 		NUMBER(3) NOT NULL
, mib_sked_frequency 	VARCHAR2(10)
, mib_sked_day 		VARCHAR2(10)
, mib_sked_time 	TIMESTAMP
, mib_sked_status	VARCHAR2(10)
);

-- Public synonym
CREATE PUBLIC SYNONYM mib_schedules FOR mib_schedules;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON mib_schedules TO ium_app_user;



PROMPT CREATE TABLE notification_recipients 
CREATE TABLE notification_recipients 
(
  not_id 		NUMBER(5) NOT NULL
, role_id 		VARCHAR2(15) NOT NULL
, lob_code      VARCHAR2(2) NOT NULL
, notification_type 	VARCHAR2(1)
, not_primary_ind		VARCHAR2(1)
, created_by	VARCHAR2(10)
, created_date	TIMESTAMP(6)
, updated_by	VARCHAR2(10)
, updated_date	TIMESTAMP(6)
);



-- Public synonym
CREATE PUBLIC SYNONYM notification_recipients FOR notification_recipients;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON notification_recipients TO ium_app_user;



PROMPT CREATE TABLE notification_templates 
CREATE TABLE notification_templates 
(
  not_id 		NUMBER(5) NOT NULL
, not_subj		VARCHAR2(100)
, not_desc 		VARCHAR2(40)
, not_email_content 	LONG
, not_sender		VARCHAR2(6)
, not_notify_mobile_ind VARCHAR2(1)
, not_mobile_content 	VARCHAR2(500)
, created_by 		VARCHAR2(10) 
, created_date 		TIMESTAMP
, updated_by 		VARCHAR2(10)
, updated_date 		TIMESTAMP
);

-- Public synonym
CREATE PUBLIC SYNONYM notification_templates FOR notification_templates;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON notification_templates TO ium_app_user;



PROMPT CREATE TABLE pages 
CREATE TABLE pages 
(
  page_id 		NUMBER(2) NOT NULL
, page_desc 		VARCHAR2(25)
, created_by	VARCHAR2(10)
, created_date	TIMESTAMP(6)
, updated_by	VARCHAR2(10)
, updated_date	TIMESTAMP(6)
); 

-- Public synonym
CREATE PUBLIC SYNONYM pages FOR pages;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON pages TO ium_app_user;



PROMPT CREATE TABLE pages_access
CREATE TABLE pages_access
(
  PA_ID		NUMBER(5) NOT NULL
, PAGE_ID	NUMBER(2)
, ACC_ID		NUMBER(2)
, PA_ENABLED_IND  VARCHAR2(1)
);

-- Public synonym
CREATE PUBLIC SYNONYM page_access FOR pages_access;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON page_access TO ium_app_user;



PROMPT CREATE TABLE policy_coverage_details 
CREATE TABLE policy_coverage_details 
(
  pcd_policy_num 	VARCHAR2(15) NOT NULL
, uar_reference_num 	VARCHAR2(10) NOT NULL 
, pcd_client_num 	VARCHAR2(10)
, pcd_coverage_num	NUMBER(2)
, pcd_pol_relationship 	VARCHAR2(1)
, pcd_issue_date 	DATE
, pcd_coverage_status 	VARCHAR2(1)
, pcd_smoker_code 	VARCHAR2(1)
, pcd_plan_code 	VARCHAR2(5)
, pcd_medical_ind 	VARCHAR2(1)
, pcd_face_amount 	NUMBER(12)
, pcd_decision 		VARCHAR2(2)
, pcd_adb_face_amount 	NUMBER(12)
, pcd_ad_multiplier 	NUMBER(3)
, pcd_wp_multiplier 	NUMBER(3)
, pcd_reinsured_amount 	NUMBER(12)
);

-- Public synonym
CREATE PUBLIC SYNONYM policy_coverage_details FOR policy_coverage_details;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON policy_coverage_details TO ium_app_user;



PROMPT CREATE TABLE policy_medical_records
CREATE TABLE policy_medical_records
(
  med_record_id 	NUMBER(8) NOT NULL
, uar_reference_num	VARCHAR2(15) NOT NULL
, association_type	VARCHAR2(1)
); 

-- Public synonym
CREATE PUBLIC SYNONYM policy_medical_records FOR policy_medical_records;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON policy_medical_records TO ium_app_user;



PROMPT CREATE TABLE policy_requirements 
CREATE TABLE policy_requirements 
(
  pr_requirement_id 	NUMBER(5) NOT NULL
, uar_reference_num 	VARCHAR2(15)
, pr_client_id		VARCHAR2(10)
, pr_admin_sequence_num NUMBER(3)
, pr_reqt_code 		VARCHAR2(5)
, pr_level 		VARCHAR2(1)
, pr_admin_create_date 	DATE
, pr_complt_reqt_ind 	VARCHAR2(1)
, pr_status_id 		NUMBER(4)
, pr_status_date 	DATE
, pr_admin_updtd_by 	VARCHAR2(10)
, pr_admin_updtd_date 	DATE
, pr_client_type 	VARCHAR2(10)
, pr_designation 	VARCHAR2(10)
, pr_test_date 		DATE
, pr_test_result_code 	VARCHAR2(4)
, pr_test_result_desc 	VARCHAR2(40)
, pr_resolved_ind 	VARCHAR2(1)
, pr_follow_up_num 	NUMBER(2)
, pr_follow_up_date 	DATE
, pr_validity_date 	DATE
, pr_paid_ind 		VARCHAR2(1)
, pr_new_test_only 	VARCHAR2(1)
, pr_order_date 	DATE
, pr_date_sent 		DATE
, pr_ccas_suggest_ind 	VARCHAR2(1)
, pr_received_date 	DATE
, pr_auto_order_ind 	VARCHAR2(1)
, pr_fld_comment_ind 	VARCHAR2(1)
, pr_comments 		VARCHAR2(50)
, created_by 		VARCHAR2(10) 
, created_date 		TIMESTAMP
, updated_by 		VARCHAR2(10)
, updated_date 		TIMESTAMP
); 

-- Public synonym
CREATE PUBLIC SYNONYM policy_requirements FOR policy_requirements;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON policy_requirements TO ium_app_user;



PROMPT CREATE TABLE PRISM_CODEC
CREATE TABLE PRISM_CODEC
(
  PC_SEQ		NUMBER(4) NOT NULL
, PC_POSITION	NUMBER(2)
, PC_CODE		VARCHAR2(1)
, PC_DESC		VARCHAR2(100)
);

-- Public synonym
CREATE PUBLIC SYNONYM prism_codec FOR prism_codec;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON prism_codec TO ium_app_user;



PROMPT CREATE TABLE process_configurations 
CREATE TABLE process_configurations 
(
  evnt_id		NUMBER(5) NOT NULL
, proc_code 		VARCHAR2(4)
, evnt_name		VARCHAR2(25)
, evnt_from_status	NUMBER(4)
, evnt_to_status	NUMBER(4)
, lob_code 		VARCHAR2(2)
, not_id 		NUMBER(3)
, proc_notification_ind VARCHAR2(1)
, created_by 		VARCHAR2(10)
, created_date 		TIMESTAMP
, updated_by 		VARCHAR2(10)
, updated_date 		TIMESTAMP
)
;
-- Public synonym
CREATE PUBLIC SYNONYM process_configurations FOR process_configurations;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON process_configurations TO ium_app_user;



PROMPT CREATE TABLE process_configuration_roles
CREATE TABLE process_configuration_roles
(
  evnt_id 	NUMBER(5) NOT NULL
, role_code VARCHAR2(15) NOT NULL
, lob_code  VARCHAR2(2) NOT NULL
);

-- Public synonym
CREATE PUBLIC SYNONYM process_configuration_roles FOR process_configuration_roles;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON process_configuration_roles TO ium_app_user;



PROMPT CREATE TABLE purge_statistics
CREATE TABLE purge_statistics
(
  purge_date 		TIMESTAMP NOT NULL
, start_date 		DATE
, end_date 			DATE
, criteria	 		VARCHAR2(20)
, number_of_records NUMBER
, result			CHAR(1)
);

-- Public synonym
CREATE PUBLIC SYNONYM purge_statistics FOR purge_statistics;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON purge_statistics TO ium_app_user;



PROMPT CREATE TABLE ranks 
CREATE TABLE ranks 
(
  rank_id 	NUMBER(3) NOT NULL
, rank_desc 	VARCHAR2(40)
, rank_fee 	NUMERIC(10,2)
, created_by	VARCHAR2(10)
, created_date	TIMESTAMP(6)
, updated_by	VARCHAR2(10)
, updated_date	TIMESTAMP(6)
);

-- Public synonym
CREATE PUBLIC SYNONYM ranks FOR ranks;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON ranks TO ium_app_user;



PROMPT CREATE TABLE requirements 
CREATE TABLE requirements 
(
  reqt_code 		VARCHAR2(5) NOT NULL
, reqt_desc 		VARCHAR2(70)
, reqt_level 		VARCHAR2(1)
, reqt_validity 	NUMBER(3)
, reqt_form_ind 	VARCHAR2(1)
, reqt_form_id 		NUMBER(5)
, reqt_follow_up_num	NUMBER(2)
, created_by	VARCHAR2(10)
, created_date	TIMESTAMP(6)
, updated_by	VARCHAR2(10)
, updated_date	TIMESTAMP(6)
, reqt_test_date_indicator VARCHAR2(1)
); 

-- Public synonym
CREATE PUBLIC SYNONYM requirements FOR requirements;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON requirements TO ium_app_user;



PROMPT CREATE TABLE requirement_forms
CREATE TABLE requirement_forms
(
  rf_id 		NUMBER(5) NOT NULL
, rf_name 		VARCHAR2(110)
, rf_template_format 	VARCHAR2(3)
, rf_template_name 	VARCHAR2(100)
, rf_status 		VARCHAR2(1)
, created_by	VARCHAR2(10)
, created_date	TIMESTAMP(6)
, updated_by	VARCHAR2(10)
, updated_date	TIMESTAMP(6)
); 

-- Public synonym
CREATE PUBLIC SYNONYM requirement_forms FOR requirement_forms;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON requirement_forms TO ium_app_user;



PROMPT CREATE TABLE REQUIREMENT_FORM_FIELDS
CREATE TABLE REQUIREMENT_FORM_FIELDS
(
  RFF_ID                 NUMBER(5) not null
, RF_ID                  NUMBER(5) not null
, FF_ID                  NUMBER(2) not null
, RFF_PAGE               NUMBER(1)
, RFF_POSX               NUMBER(3)
, RFF_POSY               NUMBER(3)
);

 
-- Public synonym
CREATE PUBLIC SYNONYM requirement_form_fields FOR requirement_form_fields;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON requirement_form_fields TO ium_app_user;



PROMPT CREATE TABLE roles 
CREATE TABLE roles 
(
  role_code	VARCHAR2(15) NOT NULL
, role_desc 	VARCHAR2(40)
, created_by	VARCHAR2(10)
, created_date	TIMESTAMP(6)
, updated_by	VARCHAR2(10)
, updated_date	TIMESTAMP(6)
);

-- Public synonym
CREATE PUBLIC SYNONYM roles FOR roles;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON roles TO ium_app_user;



PROMPT CREATE TABLE sections
CREATE TABLE sections
(
  sec_code	VARCHAR2(15) NOT NULL
, sec_desc 	VARCHAR2(50)
, created_by	VARCHAR2(10)
, created_date	TIMESTAMP(6)
, updated_by	VARCHAR2(10)
, updated_date	TIMESTAMP(6)
); 

-- Public synonym
CREATE PUBLIC SYNONYM sections FOR sections;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON sections TO ium_app_user;



PROMPT CREATE TABLE specializations 
CREATE TABLE specializations 
(
  spl_id 		NUMBER(3) NOT NULL
, spl_desc 		VARCHAR2(40)
, created_by	VARCHAR2(10)
, created_date	TIMESTAMP(6)
, updated_by	VARCHAR2(10)
, updated_date	TIMESTAMP(6)
);

-- Public synonym
CREATE PUBLIC SYNONYM specializations FOR specializations;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON specializations TO ium_app_user;



PROMPT CREATE TABLE status 
CREATE TABLE status 
(
  stat_id 	NUMBER(4) NOT NULL
, stat_type VARCHAR2(2)
, stat_desc VARCHAR2(40)
, created_by	VARCHAR2(10)
, created_date	TIMESTAMP(6)
, updated_by	VARCHAR2(10)
, updated_date	TIMESTAMP(6)
);

-- Public synonym
CREATE PUBLIC SYNONYM status FOR status;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON status TO ium_app_user;



PROMPT CREATE TABLE sunlife_offices
CREATE TABLE sunlife_offices
(
  slo_office_code	VARCHAR2(15) NOT NULL
, slo_office_name 	VARCHAR2(50)
, slo_type 		VARCHAR2(1)
, slo_address_line1 	VARCHAR2(60)
, slo_address_line2 	VARCHAR2(60)
, slo_address_line3 	VARCHAR2(60)
, slo_city 		VARCHAR2(25)
, slo_province 		VARCHAR2(25)
, slo_country 		VARCHAR2(25)
, slo_zip_code 		NUMBER(6)
, slo_contact_num 	VARCHAR2(13)
, slo_fax_num 		VARCHAR2(13)
, created_by	VARCHAR2(10)
, created_date	TIMESTAMP(6)
, updated_by	VARCHAR2(10)
, updated_date	TIMESTAMP(6)
); 

 
-- Public synonym
CREATE PUBLIC SYNONYM sunlife_offices FOR sunlife_offices;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON sunlife_offices TO ium_app_user;



PROMPT CREATE TABLE TD_COUNTER (
CREATE TABLE TD_COUNTER (
	 counter number
	,last_date date
);



PROMPT CREATE TABLE test_profiles 
CREATE TABLE test_profiles 
(
  test_id	NUMBER(5) NOT NULL
, test_desc 	VARCHAR2(40)
, test_validity	NUMBER(5)
, test_taxable_ind VARCHAR2(1)
, test_type		VARCHAR2(1)
, created_by	VARCHAR2(10)
, created_date	TIMESTAMP(6)
, updated_by	VARCHAR2(10)
, updated_date	TIMESTAMP(6)
, test_follow_up_num number(2)
); 

-- Public synonym
CREATE PUBLIC SYNONYM test_profiles FOR test_profiles;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON test_profiles TO ium_app_user;
 
 
 
PROMPT Create table transaction_details(
Create table transaction_details(
	 impairment_id number(5) not null
	,control_no varchar2(15) not null
	,user_cd	varchar2(10)
	,company_cd varchar2(1)
	,tran_cd	varchar2(25)
	,tran_dt	date
	,record_affected	number(1)
	,no_Of_Hit			number(1)
);

-- Public synonym
CREATE PUBLIC SYNONYM transaction_details FOR transaction_details;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON transaction_details TO ium_app_user;



PROMPT CREATE TABLE users 
CREATE TABLE users 
(
  user_id 		VARCHAR2(10)
, usr_acf2id		VARCHAR2(8)
, usr_password		VARCHAR2(16)
, usr_last_name		VARCHAR2(40)
, usr_first_name	VARCHAR2(25)
, usr_middle_name	VARCHAR2(25)
, usr_code 		NUMBER(5)
, usr_type 		VARCHAR2(1)
, usr_email_address 	VARCHAR2(40)
, usr_mobile_num 	VARCHAR2(13)
, usr_notification_ind 	VARCHAR2(1)
, usr_records_per_view 	NUMBER(2)
, usr_sex 		VARCHAR2(1)
, usr_role 		VARCHAR2(15)
, slo_id 		VARCHAR2(15)
, dept_id 		VARCHAR2(10)
, sec_id 		VARCHAR2(15)
, created_by 		VARCHAR2(10)
, created_date 		TIMESTAMP
, updated_by 		VARCHAR2(10)
, updated_date 		TIMESTAMP
, usr_contact_num VARCHAR2(15)
, usr_address1  VARCHAR2(60)
, usr_address2  VARCHAR2(60)
, usr_address3  VARCHAR2(60)
, usr_country   VARCHAR2(20)
, usr_city      VARCHAR2(20)
, usr_zipcode   VARCHAR2(5)
, usr_lock_ind  VARCHAR2(1)
, usr_active_ind VARCHAR2(1)
);


-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON users TO ium_app_user;



PROMPT CREATE TABLE user_page_access
CREATE TABLE user_page_access
(
  user_id 			VARCHAR2(10) NOT NULL
, page_id 			NUMBER(2) NOT NULL
, acc_id          	NUMBER(2) NOT NULL
, upa_access_ind	VARCHAR2(1)
, created_by		VARCHAR2(10)
, created_date		TIMESTAMP
, updated_by		VARCHAR2(10)
, updated_date		TIMESTAMP
);

 -- Public synonym
CREATE PUBLIC SYNONYM user_page_access FOR user_page_access;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON user_page_access TO ium_app_user;



PROMPT CREATE TABLE user_process_preferences
CREATE TABLE user_process_preferences
(
  user_id			VARCHAR2(10) NOT NULL
, evnt_id			NUMBER(5) NOT NULL
, upp_notification_ind VARCHAR2(1)
);

-- Public synonym
CREATE PUBLIC SYNONYM user_process_preferences FOR user_process_preferences;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON user_process_preferences TO ium_app_user;



PROMPT CREATE TABLE user_roles
CREATE TABLE user_roles
(  role_code                      VARCHAR2(15) NOT NULL
 , user_id                        VARCHAR2(10) NOT NULL
);

-- Public synonym
CREATE PUBLIC SYNONYM user_roles FOR user_roles;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON user_roles TO ium_app_user;



PROMPT CREATE TABLE UW_ASSESSMENT_REQUESTS
CREATE TABLE UW_ASSESSMENT_REQUESTS
(
  uw_analysis_id NUMBER
, reference_num	VARCHAR2(15)
, uw_analysis	LONG
, posted_by    VARCHAR2(10)
, posted_date  TIMESTAMP(6)
);

-- Public synonym
CREATE PUBLIC SYNONYM uw_assessment_requests FOR uw_assessment_requests;

-- Grant access privileges
GRANT SELECT, INSERT, UPDATE, DELETE ON uw_assessment_requests TO ium_app_user;



-- SEQUENCE
PROMPT CREATING SEQUENCE
-- Primary Key Sequence ACCESSES
CREATE SEQUENCE seq_access
	INCREMENT BY 1
	START WITH 1
	MAXVALUE 99;

-- Primary Key Sequence ACCESS_TEMPLATES
CREATE SEQUENCE seq_access_template
	INCREMENT BY 1
	START WITH 1
	MAXVALUE 99999;

-- Primary Key Sequence ACCESS_TEMPLATE_DETAILS
CREATE SEQUENCE seq_access_template_dtls
	INCREMENT BY 1
	START WITH 1
	MAXVALUE 99999;

-- Primary Key Sequence ACTIVITY_LOGS
CREATE SEQUENCE seq_activity_logs
 START WITH 1
 INCREMENT BY 1
 MAXVALUE 9999999999;

 -- Primary Key Sequence AUDIT_TRAILS
CREATE SEQUENCE seq_audit_trail
	START WITH 1
	INCREMENT BY 1
	MAXVALUE 9999999999;
 
 -- Primary Key Sequence AUTO_ASSIGNMENT
CREATE SEQUENCE seq_auto_assignment
	START WITH 1
	INCREMENT BY 1
	MAXVALUE 999;

-- Primary Key Sequence AUTO_ASSIGNMENT_CRITERIA
CREATE SEQUENCE seq_auto_assignment_criteria
	START WITH 1
	INCREMENT BY 1
	MAXVALUE 999;

-- Primary Key Sequence EXAMINATION_AREA	
CREATE SEQUENCE seq_examination_area
		INCREMENT BY 1
		START WITH 1
		MAXVALUE 999;

-- Primary Key Sequence EXAMINATION_PLACES
CREATE SEQUENCE seq_examination_place
	START WITH 1
	INCREMENT BY 1
	NOMAXVALUE;

-- Primary Key Sequence EXAMINERS
CREATE SEQUENCE seq_examiner
	START WITH 1
	INCREMENT BY 1
	NOMAXVALUE;

-- Primary Key Sequence EXCEPTION_BATCH
CREATE SEQUENCE SEQ_BATCH_ID
		INCREMENT BY 1
		START WITH 1
		MAXVALUE 99999999;

-- Primary Key Sequence EXCEPTION_LOG
CREATE SEQUENCE SEQ_EXCEPTION_ID
		INCREMENT BY 1
		START WITH 1
		MAXVALUE 99999999;

-- Primary Key Sequence FOLDER_DOCUMENTS
CREATE SEQUENCE seq_folder_document
	START WITH 1
	INCREMENT BY 1
	NOMAXVALUE;

-- Primary Key Sequence FORM_FIELDS
CREATE SEQUENCE seq_form_fields
	INCREMENT BY 1
	START WITH 1
	MAXVALUE 99;

-- Primary Key Sequence HOLIDAYS
CREATE SEQUENCE seq_holiday
    START WITH 1
	INCREMENT BY 1
	MAXVALUE 999;

-- Primary Key Sequence IMPAIRMENTS
CREATE SEQUENCE seq_impairment
	START WITH 1
	INCREMENT BY 1
	NOMAXVALUE;

-- Primary Key Sequence JOB_SCHEDULES
CREATE SEQUENCE seq_job_schedules
	INCREMENT BY 1
	START WITH 1
	MAXVALUE 99999;
	

-- Primary Key Sequences LABORATORIES
CREATE SEQUENCE seq_laboratory
    START WITH 1
	INCREMENT BY 1
	NOMAXVALUE;

-- Primay Key Sequence MEDICAL_BILLS
CREATE SEQUENCE seq_medical_bill
    START WITH 1
	INCREMENT BY 1
	NOMAXVALUE;

-- Primary Key Sequences MEDICAL_NOTES
CREATE SEQUENCE seq_medical_note
    START WITH 1
	INCREMENT BY 1
	NOMAXVALUE;

-- Primary Key Sequence MEDICAL_RECORDS
CREATE SEQUENCE seq_medical_record
	START WITH 1
	INCREMENT BY 1
	NOMAXVALUE;

-- Primary Key Sequence MIB_SCHEDULES
CREATE SEQUENCE seq_mib_schedule
	START WITH 1
	INCREMENT BY 1
	MAXVALUE 999;
	
-- Primary Key Sequence NOTIFICATION_TEMPLATES
CREATE SEQUENCE seq_notification_template
	START WITH 1
	INCREMENT BY 1
	MAXVALUE 99999;
	
-- Primary Key Sequence PAGES
CREATE SEQUENCE seq_page
	START WITH 1
	INCREMENT BY 1
	MAXVALUE 99;

-- Primary Key Sequence PAGE_ACCESS
CREATE SEQUENCE seq_pages_access
 START WITH 1
 INCREMENT BY 1
 NOMAXVALUE;

-- Primary Key Sequence PROCESS_CONFIGURATIONS
CREATE SEQUENCE seq_process_configuration
    START WITH 1
	INCREMENT BY 1
	MAXVALUE 99999;

-- Primary Key Sequence RANKS
CREATE SEQUENCE seq_rank
	START WITH 1
	INCREMENT BY 1
	MAXVALUE 999;
	
-- Primary Key Sequence REQUIREMENT_FORMS
CREATE SEQUENCE seq_requirement_form
	START WITH 1
	INCREMENT BY 1
	NOMAXVALUE;
	
-- Primary Key Sequence REQUIREMENT_FORM_FIELDS
CREATE SEQUENCE seq_requirement_form_fields
	INCREMENT BY 1
	START WITH 1
	MAXVALUE 99;

-- Primary Key Sequence SPECIALIZATIONS
CREATE SEQUENCE seq_specialization
	START WITH 1
	INCREMENT BY 1
	NOMAXVALUE;
	
-- Primary Key Sequence STATUS
CREATE SEQUENCE seq_status
	START WITH 1
	INCREMENT BY 1
	MAXVALUE 9999;
	
-- Primary Key Sequence TEST_PROFILES
CREATE SEQUENCE seq_test_profile
	START WITH 1
	INCREMENT BY 1
	NOMAXVALUE;
	
-- Primary Key Sequence UW_ASSESSMENT_REQUESTS
CREATE SEQUENCE seq_uw_assessment_requests
 START WITH 1
 INCREMENT BY 1
 NOMAXVALUE;


-- CONSTRAINTS
PROMPT CREATING CONSTRAINTS
-- Primary Key ACCESSES
ALTER TABLE accesses
 ADD CONSTRAINT pk_accesses
 PRIMARY KEY (acc_id)
 USING INDEX;
 
-- Primary Key ACCESS_TEMPLATES
ALTER TABLE access_templates
 ADD CONSTRAINT pk_access_template 
 PRIMARY KEY (templt_id)
 USING INDEX;

-- Primary Key ACCESS_TEMPLATE DETAILS
ALTER TABLE access_template_details
 ADD CONSTRAINT pk_access_template_details
 PRIMARY KEY (templt_id, pa_id)
 USING INDEX;
 
-- Primary Key ACTIVITY_LOGS
ALTER TABLE activity_logs
 ADD CONSTRAINT pk_activity_logs
 PRIMARY KEY (act_id)
 USING INDEX;

-- Primary Key ASSESSMENT_REQUESTS
ALTER TABLE assessment_requests
 ADD CONSTRAINT pk_assessment_requests
 PRIMARY KEY (reference_num)
 USING INDEX;

-- Primary Key AUDIT_TRAILS
ALTER TABLE audit_trails
 ADD CONSTRAINT pk_audit_trails
 PRIMARY KEY (tran_num)
 USING INDEX;

-- Primary Key AUTO_ASSIGNMENTS
ALTER TABLE auto_assignments
 ADD CONSTRAINT pk_auto_assignments
 PRIMARY KEY (aa_id)
 USING INDEX;

-- Primary Key AUTO_ASSIGNMENT_CRITERIA
ALTER TABLE auto_assignment_criteria
 ADD CONSTRAINT pk_auto_assignment_criteria
 PRIMARY KEY (aac_id)
 USING INDEX;

-- Primary Key CLIENTS
ALTER TABLE clients
 ADD CONSTRAINT pk_clients
 PRIMARY KEY (client_id)
 USING INDEX;

-- Primary Key CLIENT_DATA_SHEETS
ALTER TABLE client_data_sheets
 ADD CONSTRAINT pk_client_data_sheets
 PRIMARY KEY (cds_reference_num, cds_client_num)
 USING INDEX;

-- Primary Key CLIENT_TYPES
ALTER TABLE client_types
 ADD CONSTRAINT pk_client_types
 PRIMARY KEY (clt_code)
 USING INDEX;

-- Primary key CODEC_KO_CODEC
ALTER TABLE codec_ko_codec
 ADD CONSTRAINT pk_codec_ko_codec
 PRIMARY KEY (pc_seq, ko_seq)
 USING INDEX;

-- Primary key CODEC_REQUIREMENTS
ALTER TABLE codec_requirements
 ADD CONSTRAINT pk_codec_requirements
 PRIMARY KEY (pc_seq, reqt_code)
 USING INDEX;

-- Primary Key DEPARTMENTS
ALTER TABLE departments
 ADD CONSTRAINT pk_departments
 PRIMARY KEY (dept_code)
 USING INDEX;

-- Primary Key  DOCUMENT_TYPES
ALTER TABLE document_types
 ADD CONSTRAINT pk_document_types
 PRIMARY KEY (doc_code)
 USING INDEX;

-- Primary Key EXAMINATION_PLACES
ALTER TABLE examination_places
 ADD CONSTRAINT pk_examination_places
 PRIMARY KEY (ep_id)
 USING INDEX;

-- Primary Key EXAMINERS
ALTER TABLE examiners
 ADD CONSTRAINT pk_examiners
 PRIMARY KEY (exmnr_id)
 USING INDEX;

-- Primary Key EXAMINER_SPECIALIZATIONS
ALTER TABLE examiner_specializations
 ADD CONSTRAINT pk_examiner_specializations
 PRIMARY KEY (exmnr_id, spl_id)
 USING INDEX;

-- Primary key EXCEPTION_BATCH
ALTER TABLE exception_batch
 ADD CONSTRAINT pk_exception_batch
 PRIMARY KEY (batch_id)
 USING INDEX;

-- Primary key EXCEPTION_LOG
ALTER TABLE exception_log
 ADD CONSTRAINT pk_exception_log
 PRIMARY KEY (exception_id)
 USING INDEX;

-- Primary Key FOLDER_DOCUMENTS
ALTER TABLE folder_documents
 ADD CONSTRAINT pk_folder_documents
 PRIMARY KEY (doc_id)
 USING INDEX;

-- Primary key FORM_FIELDS
ALTER TABLE FORM_FIELDS
 ADD CONSTRAINT pk_form_fields
 PRIMARY KEY (ff_id)
 USING INDEX;

-- Primary Key HOLIDAYS
ALTER TABLE holidays
 ADD CONSTRAINT pk_holidays
 PRIMARY KEY (hol_id)
 USING INDEX;

-- Primary Key IMPAIRMENTS
ALTER TABLE impairments
 ADD CONSTRAINT pk_impairments
 PRIMARY KEY (imp_impairement_id)
 USING INDEX;
 
-- Primary Key JOB_SCHEDULES
ALTER TABLE job_schedules
 ADD CONSTRAINT pk_job_schedules
 PRIMARY KEY (sched_type);

-- Primary key KICKOUT_CODES
ALTER TABLE kickout_codes
 ADD CONSTRAINT pk_kickout_codes
 PRIMARY KEY (ko_seq)
 USING INDEX;

-- Primary Key LABORATORIES
ALTER TABLE laboratories
 ADD CONSTRAINT pk_laboratories
 PRIMARY KEY (lab_id)
 USING INDEX;

-- Primary Key LABORATORY_TESTS
ALTER TABLE laboratory_tests
 ADD CONSTRAINT pk_laboratory_tests
 PRIMARY KEY (lab_id, test_id)
 USING INDEX;

-- Primary Key LINES_OF_BUSINESS
ALTER TABLE lines_of_business
 ADD CONSTRAINT pk_lines_of_business
 PRIMARY KEY (lob_code)
 USING INDEX;

-- Primary Key LOB_DOCUMENTS
ALTER TABLE lob_documents
 ADD CONSTRAINT pk_lob_documents
 PRIMARY KEY (lob_code, doc_code)
 USING INDEX;

-- Primary Key LOB_REQUIREMENTS
ALTER TABLE lob_requirements
 ADD CONSTRAINT pk_lob_requirements
 PRIMARY KEY (lob_code, reqt_code)
 USING INDEX;

-- Primary Key LOB_STATUS
ALTER TABLE lob_status
 ADD CONSTRAINT pk_lob_status
 PRIMARY KEY (lob_code, stat_id)
 USING INDEX;
 
-- Primary Key MEDICAL_BILLS
ALTER TABLE medical_bills
 ADD CONSTRAINT pk_medical_bills
 PRIMARY KEY (mb_bill_number)
 USING INDEX;

-- Primary Key MEDICAL_NOTES
ALTER TABLE medical_notes
 ADD CONSTRAINT pk_medical_notes
 PRIMARY KEY (mn_notes_id)
 USING INDEX;

-- Primary Key MEDICAL_RECORDS
ALTER TABLE medical_records
 ADD CONSTRAINT pk_medical_records
 PRIMARY KEY (med_record_id)
 USING INDEX;

-- Primary Key MIB_ACTIONS
ALTER TABLE mib_actions
 ADD CONSTRAINT pk_mib_actions
 PRIMARY KEY (mib_act_code)
 USING INDEX;

-- Primary Key MIB_IMPAIRMENTS
ALTER TABLE mib_impairments
 ADD CONSTRAINT pk_mib_impairments
 PRIMARY KEY (mib_impairment_code)
 USING INDEX;

-- Primary Key MIB_LETTERS
ALTER TABLE mib_letters
 ADD CONSTRAINT pk_mib_letters
 PRIMARY KEY (mib_lttr_code)
 USING INDEX;

-- Primary Key MIB_NUMBERS
ALTER TABLE mib_numbers
 ADD CONSTRAINT pk_mib_numbers
 PRIMARY KEY (mib_num_code)
 USING INDEX;

-- Primary Key MIB_SCHEDULES
ALTER TABLE mib_schedules
 ADD CONSTRAINT pk_mib_schedules
 PRIMARY KEY (mib_sked_id)
 USING INDEX;

-- Primary Key NOTIFICATION_RECIPIENTS
ALTER TABLE notification_recipients
 ADD CONSTRAINT pk_notification_recipients
 PRIMARY KEY (not_id, role_id, lob_code)
 USING INDEX;

-- Primary Key NOTIFICATION_TEMPLATES
ALTER TABLE notification_templates
 ADD CONSTRAINT pk_notification_templates
 PRIMARY KEY (not_id)
 USING INDEX;

-- Primary Key PAGES
ALTER TABLE pages
 ADD CONSTRAINT pk_pages
 PRIMARY KEY (page_id)
 USING INDEX;

-- Primary Key PAGE_ACCESS
ALTER TABLE pages_access
 ADD CONSTRAINT pk_pages_access
 PRIMARY KEY (pa_id)
 USING INDEX;


-- Primary Key POLICY_COVERAGE_DETAILS
ALTER TABLE policy_coverage_details
 ADD CONSTRAINT pk_policy_coverage_details
 PRIMARY KEY (pcd_policy_num, uar_reference_num, pcd_coverage_num)
 USING INDEX;

-- Primary Key POLICY_MEDICAL_RECORDS
ALTER TABLE policy_medical_records
 ADD CONSTRAINT pk_policy_medical_records
 PRIMARY KEY (med_record_id, uar_reference_num)
 USING INDEX;

-- Primary Key POLICY_REQUIREMENTS
ALTER TABLE policy_requirements
 ADD CONSTRAINT pk_policy_requirements
 PRIMARY KEY (pr_requirement_id)
 USING INDEX;

-- Primary key PRISM_CODEC
ALTER TABLE prism_codec
 ADD CONSTRAINT pk_prism_codec
 PRIMARY KEY (pc_seq) 
 USING INDEX;

-- Primary Key PROCESS_CONFIGURATIONS
ALTER TABLE process_configurations
 ADD CONSTRAINT pk_process_configurations
 PRIMARY KEY (evnt_id)
 USING INDEX;

-- Primary Key PURGE_STATISTICS
ALTER TABLE purge_statistics
 ADD CONSTRAINT pk_purge_statistics
 PRIMARY KEY (purge_date)
 USING INDEX;

-- Primary Key RANKS
ALTER TABLE ranks
 ADD CONSTRAINT pk_ranks
 PRIMARY KEY (rank_id)
 USING INDEX;

-- Primary Key REQUIREMENTS
ALTER TABLE requirements
 ADD CONSTRAINT pk_requirements
 PRIMARY KEY (reqt_code)
 USING INDEX;

-- Primary Key REQUIREMENT_FORMS
ALTER TABLE requirement_forms
 ADD CONSTRAINT pk_requirement_forms
 PRIMARY KEY (rf_id)
 USING INDEX;

-- Primary key REQUIREMENT_FORM_FIELDS
ALTER TABLE requirement_form_fields
 ADD CONSTRAINT pk_req_form_fields
 PRIMARY KEY (rff_id)
 USING INDEX;

-- Primary Key ROLES
ALTER TABLE roles
 ADD CONSTRAINT pk_roles
 PRIMARY KEY (role_code)
 USING INDEX;

-- Primary Key SECTIONS
ALTER TABLE sections
 ADD CONSTRAINT pk_sections
 PRIMARY KEY (sec_code)
 USING INDEX;

-- Primary Key SPECALIZATIONS
ALTER TABLE specializations
 ADD CONSTRAINT pk_specializations
 PRIMARY KEY (spl_id)
 USING INDEX;

-- Primary Key STATUS
ALTER TABLE status
 ADD CONSTRAINT pk_status
 PRIMARY KEY (stat_id)
 USING INDEX;

-- Primary Key SUNLIFE_OFFICES
ALTER TABLE sunlife_offices
 ADD CONSTRAINT pk_sunlife_offices
 PRIMARY KEY (slo_office_code)
 USING INDEX;

-- Primary Key TEST_PROFILES
ALTER TABLE test_profiles
 ADD CONSTRAINT pk_test_profiles
 PRIMARY KEY (test_id)
 USING INDEX;

-- Primary Key TRANSACTION_DETAILS
ALTER TABLE transaction_details
 ADD CONSTRAINT pk_transaction_details
 PRIMARY KEY(impairment_id, control_no)
 USING INDEX;

-- Primary Key USERS
ALTER TABLE users
 ADD CONSTRAINT pk_users
 PRIMARY KEY (user_id)
 USING INDEX;

-- Primary Key USER_PAGE_ACCESS
ALTER TABLE user_page_access
 ADD CONSTRAINT pk_user_page_access
 PRIMARY KEY (user_id, page_id, acc_id)
 USING INDEX;

-- Primary key USER_PROCESS_PREFERENCES
ALTER TABLE user_process_preferences
 ADD CONSTRAINT pk_user_process_preferences
 PRIMARY KEY (evnt_id, user_id)
 USING INDEX;

-- Primary key USER_ROLES
ALTER TABLE user_roles
 ADD CONSTRAINT pk_user_roles
 PRIMARY KEY (role_code, user_id)
 USING INDEX;

-- Primary key UW_ASSESSMENT_REQUESTS
ALTER TABLE uw_assessment_requests
 ADD CONSTRAINT pk_uw_asssesment_requests
 PRIMARY KEY (uw_analysis_id)
 USING INDEX;

-- Foreign Key USER_ROLES
ALTER TABLE user_roles
 ADD CONSTRAINT fk_ur_roles
 FOREIGN KEY (role_code)
 REFERENCES roles (role_code);

ALTER TABLE user_roles
 ADD CONSTRAINT fk_users
 FOREIGN KEY (user_id)
 REFERENCES users (user_id);
 
-- Foreign key USER_PROCESS_PREFERENCES
ALTER TABLE user_process_preferences
 ADD CONSTRAINT fk_upp_process_configurations
 FOREIGN KEY (evnt_id)
 REFERENCES process_configurations(evnt_id);

ALTER TABLE user_process_preferences
 ADD CONSTRAINT fk_upp_users
 FOREIGN KEY (user_id)
 REFERENCES users(user_id);
 
-- Foreign Keys USER_PAGE_ACCESS
ALTER TABLE user_page_access
 ADD CONSTRAINT fk_upa_users
 FOREIGN KEY (user_id)
 REFERENCES users (user_id);
 
ALTER TABLE user_page_access
 ADD CONSTRAINT fk_upa_pages
 FOREIGN KEY (page_id)
 REFERENCES pages (page_id);
 
ALTER TABLE user_page_access
 ADD CONSTRAINT fk_upa_users_created_by
 FOREIGN KEY (created_by)
 REFERENCES users (user_id);

ALTER TABLE user_page_access
 ADD CONSTRAINT fk_upa_users_updated_by
 FOREIGN KEY (updated_by)
 REFERENCES users (user_id);

-- Foreign Key USERS
ALTER TABLE users
 ADD CONSTRAINT fk_usr_sunlife_offices
 FOREIGN KEY (slo_id)
 REFERENCES sunlife_offices (slo_office_code);

ALTER TABLE users
 ADD CONSTRAINT fk_usr_users_created_by
 FOREIGN KEY (created_by)
 REFERENCES users (user_id);

ALTER TABLE users
 ADD CONSTRAINT fk_usr_users_updated_by
 FOREIGN KEY (updated_by)
 REFERENCES users (user_id);
 
-- Foreign key TRANSACTION_DETAILS
ALTER TABLE transaction_details
 ADD CONSTRAINT fk_users_tran_details
 FOREIGN KEY(user_cd)
 REFERENCES users (user_id);

-- Foreign key REQUIREMENT_FORM_FIELDS
ALTER TABLE requirement_form_fields
 ADD CONSTRAINT fk_form_fields
 FOREIGN KEY (ff_id)
 REFERENCES form_fields(ff_id);

ALTER TABLE requirement_form_fields
 ADD CONSTRAINT fk_req_form
 FOREIGN KEY (rf_id)
 REFERENCES requirement_forms (rf_id);


-- Foreign Key REQUIREMENTS
ALTER TABLE requirements
 ADD CONSTRAINT fk_reqt_requirement_forms
 FOREIGN KEY (reqt_form_id)
 REFERENCES requirement_forms (rf_id);

	
-- Foreign key PROCESS_CONFIGURATION_ROLES
ALTER TABLE process_configuration_roles
	ADD CONSTRAINT fk_pc_evnt_id
	FOREIGN KEY (evnt_id)
	REFERENCES process_configurations (evnt_id);

	
ALTER TABLE process_configuration_roles
	ADD CONSTRAINT fk_pc_role_code
	FOREIGN KEY (role_code)
	REFERENCES roles(role_code);
	
ALTER TABLE process_configuration_roles
 ADD CONSTRAINT fk_pc_lob_code
 FOREIGN KEY (lob_code)
 REFERENCES lines_of_business(lob_code);

-- Foreign Keys PROCESS_CONFIGURATIONS
ALTER TABLE process_configurations
 ADD CONSTRAINT fk_pc_lines_of_business
 FOREIGN KEY (lob_code)
 REFERENCES lines_of_business (lob_code);

ALTER TABLE process_configurations
 ADD CONSTRAINT fk_pc_user_created_by
 FOREIGN KEY (created_by)
 REFERENCES users (user_id);

ALTER TABLE process_configurations
 ADD CONSTRAINT fk_pc_users_updated_by
 FOREIGN KEY (updated_by)
 REFERENCES users (user_id);

ALTER TABLE process_configurations
 ADD CONSTRAINT fk_pc_status_from_status
 FOREIGN KEY (evnt_from_status)
 REFERENCES status (stat_id);

ALTER TABLE process_configurations
 ADD CONSTRAINT fk_pc_status_to_status
 FOREIGN KEY (evnt_to_status)
 REFERENCES status (stat_id);

-- Foreign Key POLICY_REQUIREMENTS
ALTER TABLE policy_requirements
 ADD CONSTRAINT fk_pr_assessment_requests
 FOREIGN KEY (uar_reference_num)
 REFERENCES assessment_requests (reference_num);

ALTER TABLE policy_requirements
 ADD CONSTRAINT fk_pr_clients
 FOREIGN KEY (pr_client_id)
 REFERENCES clients (client_id);

ALTER TABLE policy_requirements
 ADD CONSTRAINT fk_pr_requirements
 FOREIGN KEY (pr_reqt_code)
 REFERENCES requirements (reqt_code);

ALTER TABLE policy_requirements
 ADD CONSTRAINT fk_pr_status
 FOREIGN KEY (pr_status_id)
 REFERENCES status (stat_id);

ALTER TABLE policy_requirements
 ADD CONSTRAINT fk_pr_users_created_by
 FOREIGN KEY (created_by)
 REFERENCES users (user_id);

ALTER TABLE policy_requirements
 ADD CONSTRAINT fk_pr_users_updated_by
 FOREIGN KEY (updated_by)
 REFERENCES users (user_id);
 

-- Foreign Key POLICY_MEDICAL_RECORDS
ALTER TABLE policy_medical_records
 ADD CONSTRAINT fk_pmr_assessment_request
 FOREIGN KEY (uar_reference_num)
 REFERENCES assessment_requests (reference_num);
 

-- Foreign Key POLICY_COVERAGE_DETAILS
ALTER TABLE policy_coverage_details
 ADD CONSTRAINT fk_pcd_assessment_requests
 FOREIGN KEY (uar_reference_num)
 REFERENCES assessment_requests (reference_num);

ALTER TABLE policy_coverage_details
 ADD CONSTRAINT fk_pcd_clients
 FOREIGN KEY (pcd_client_num)
 REFERENCES clients (client_id);


-- Foreign Key PAGES_ACCESS
ALTER TABLE pages_access
 ADD CONSTRAINT fk_pa_pages
 FOREIGN KEY (page_id)
 REFERENCES pages (page_id); 

ALTER TABLE pages_access
 ADD CONSTRAINT fk_pa_access
 FOREIGN KEY (acc_id)
 REFERENCES accesses (acc_id);

-- Foreign Key PAGES
Alter table pages
 ADD CONSTRAINT fk_pages_user
 FOREIGN KEY (created_by)
 REFERENCES users(user_id);
 
-- Foreign Key NOTIFICATION_TEMPLATES
ALTER TABLE notification_templates
 ADD CONSTRAINT fk_ntt_users_created_by
 FOREIGN KEY (created_by)
 REFERENCES users (user_id);

ALTER TABLE notification_templates
 ADD CONSTRAINT fk_ntt_users_updated_by
 FOREIGN KEY (updated_by)
 REFERENCES users (user_id);
 
-- Foreign Key NOTIFICATION_RECIPIENTS
ALTER TABLE notification_recipients
 ADD CONSTRAINT fk_not_notification_templates
 FOREIGN KEY (not_id)
 REFERENCES notification_templates (not_id);

ALTER TABLE notification_recipients
 ADD CONSTRAINT fk_not_role
 FOREIGN KEY (role_id)
 REFERENCES roles (role_code);
 
 ALTER TABLE notification_recipients
 ADD CONSTRAINT fk_not_lines_of_business
 FOREIGN KEY (lob_code)
 REFERENCES lines_of_business (lob_code);
 
-- Foreign Key MEDICAL_RECORDS
ALTER TABLE medical_records
 ADD CONSTRAINT fk_med_clients
 FOREIGN KEY (cl_client_num)
 REFERENCES clients (client_id);

ALTER TABLE medical_records
 ADD CONSTRAINT fk_med_users_agent
 FOREIGN KEY (agent_id)
 REFERENCES users (user_id);

ALTER TABLE medical_records
 ADD CONSTRAINT fk_med_branches
 FOREIGN KEY (branch_id)
 REFERENCES sunlife_offices (slo_office_code);

ALTER TABLE medical_records
 ADD CONSTRAINT fk_med_status
 FOREIGN KEY (med_status)
 REFERENCES status (stat_id);

ALTER TABLE medical_records
 ADD CONSTRAINT fk_med_examination_places
 FOREIGN KEY (med_examination_place)
 REFERENCES examination_places (ep_id);

ALTER TABLE medical_records
 ADD CONSTRAINT fk_med_laboratories
 FOREIGN KEY (med_lab_id)
 REFERENCES laboratories (lab_id);

ALTER TABLE medical_records
 ADD CONSTRAINT fk_med_examiners
 FOREIGN KEY (exmnr_id)
 REFERENCES examiners (exmnr_id);

ALTER TABLE medical_records
 ADD CONSTRAINT fk_med_test_profiles
 FOREIGN KEY (med_test_id)
 REFERENCES test_profiles (test_id);

ALTER TABLE medical_records
 ADD CONSTRAINT fk_med_sections
 FOREIGN KEY (med_section_id)
 REFERENCES sections (sec_code);

ALTER TABLE medical_records
 ADD CONSTRAINT fk_med_departments
 FOREIGN KEY (med_department_id)
 REFERENCES departments (dept_code);

ALTER TABLE medical_records
 ADD CONSTRAINT fk_med_users_created_by
 FOREIGN KEY (created_by)
 REFERENCES users (user_id);

ALTER TABLE medical_records
 ADD CONSTRAINT fk_med_users_updated_by
 FOREIGN KEY (updated_by)
 REFERENCES users (user_id);

-- Foreign Key MEDICAL_NOTES
ALTER TABLE medical_notes
 ADD CONSTRAINT fk_mn_assessment_requests
 FOREIGN KEY (uar_reference_num)
 REFERENCES assessment_requests (reference_num);

ALTER TABLE medical_notes
 ADD CONSTRAINT fk_mn_users
 FOREIGN KEY (mn_posted_by)
 REFERENCES users (user_id);


-- Foreign Key MEDICAL_BILLS
ALTER TABLE medical_bills
 ADD CONSTRAINT fk_mb_assessment_requests
 FOREIGN KEY (uar_reference_num)
 REFERENCES assessment_requests (reference_num);

ALTER TABLE medical_bills
 ADD CONSTRAINT fk_mb_examiners
 FOREIGN KEY (exmnr_id)
 REFERENCES examiners (exmnr_id);

ALTER TABLE medical_bills
 ADD CONSTRAINT fk_mb_laboratories
 FOREIGN KEY (lab_id)
 REFERENCES laboratories (lab_id);

-- Foreign Key LOB_STATUS
ALTER TABLE lob_status
 ADD CONSTRAINT fk_lbs_lines_of_business
 FOREIGN KEY (lob_code)
 REFERENCES lines_of_business (lob_code);

ALTER TABLE lob_status
 ADD CONSTRAINT fk_lbs_status
 FOREIGN KEY (stat_id)
 REFERENCES status (stat_id);

-- Foreign Key LOB_REQUIREMENTS
ALTER TABLE lob_requirements
 ADD CONSTRAINT fk_lrq_lines_of_business
 FOREIGN KEY (lob_code)
 REFERENCES lines_of_business (lob_code);

ALTER TABLE lob_requirements
 ADD CONSTRAINT fk_lrq_requirements
 FOREIGN KEY (reqt_code)
 REFERENCES requirements (reqt_code);

-- Foreign Key LOB_DOCUMENTS
ALTER TABLE lob_documents 
 ADD CONSTRAINT fk_ldc_lines_of_business
 FOREIGN KEY (lob_code)
 REFERENCES lines_of_business (lob_code);

ALTER TABLE lob_documents 
 ADD CONSTRAINT fk_ldc_document_types
 FOREIGN KEY (doc_code)
 REFERENCES document_types (doc_code);


-- Foreign Key LABORATORY_TESTS
ALTER TABLE laboratory_tests
 ADD CONSTRAINT pk_test_laboratories
 FOREIGN KEY (lab_id)
 REFERENCES laboratories (lab_id);

ALTER TABLE laboratory_tests
 ADD CONSTRAINT fk_test_test_profiles
 FOREIGN KEY (test_id)
 REFERENCES test_profiles (test_id);

-- Foreign Key LABORATORIES
ALTER TABLE laboratories
 ADD CONSTRAINT fk_users_lab_created_by
 FOREIGN KEY (created_by)
 REFERENCES users (user_id);

ALTER TABLE laboratories
 ADD CONSTRAINT fk_users_lab_updated_by
 FOREIGN KEY (updated_by)
 REFERENCES users (user_id);

-- Foreign Key KICKOUT_MESSAGES
ALTER TABLE kickout_messages
 ADD CONSTRAINT fk_ko_assessment_requests
 FOREIGN KEY (uar_reference_num)
 REFERENCES assessment_requests (reference_num);

ALTER TABLE kickout_messages
 ADD CONSTRAINT fk_ko_clients
 FOREIGN KEY (client_id)
 REFERENCES clients (client_id);

-- Foreign Key IMPAIRMENTS
ALTER TABLE impairments
 ADD CONSTRAINT fk_imp_clients
 FOREIGN KEY (cl_client_num)
 REFERENCES clients (client_id);

ALTER TABLE impairments
 ADD CONSTRAINT fk_imp_assessment_requests
 FOREIGN KEY (uar_reference_num)
 REFERENCES assessment_requests (reference_num);

ALTER TABLE impairments
 ADD CONSTRAINT fk_imp_mib_impairments
 FOREIGN KEY (mib_impairment_code)
 REFERENCES mib_impairments (mib_impairment_code);

ALTER TABLE impairments
 ADD CONSTRAINT fk_imp_mib_numbers
 FOREIGN KEY (imp_relationship)
 REFERENCES mib_numbers (mib_num_code);

ALTER TABLE impairments
 ADD CONSTRAINT fk_imp_mib_letters
 FOREIGN KEY (imp_origin)
 REFERENCES mib_letters (mib_lttr_code);

ALTER TABLE impairments
 ADD CONSTRAINT fk_imp_users_created_by
 FOREIGN KEY (created_by)
 REFERENCES users (user_id);

ALTER TABLE impairments
 ADD CONSTRAINT fk_imp_users_updated_by
 FOREIGN KEY (updated_by)
 REFERENCES users (user_id);


-- Foreign Key FOLDER_DOCUMENTS
ALTER TABLE folder_documents
 ADD CONSTRAINT fk_doc_assessment_requests
 FOREIGN KEY (uar_reference_num)
 REFERENCES assessment_requests (reference_num);

ALTER TABLE folder_documents
 ADD CONSTRAINT fk_doc_document_types
 FOREIGN KEY (doc_code)
 REFERENCES document_types (doc_code);

ALTER TABLE folder_documents
 ADD CONSTRAINT fk_doc_users
 FOREIGN KEY (doc_received_by)
 REFERENCES users (user_id);

-- Foreign key EXCEPTION_LOG
ALTER TABLE exception_log
 ADD CONSTRAINT fk_exception_batch
 FOREIGN KEY (batch_id)
 REFERENCES exception_batch(batch_id);

-- Foreign Key EXAMINER_SPECIALIZATIONS
ALTER TABLE examiner_specializations
 ADD CONSTRAINT fk_esp_examiners
 FOREIGN KEY (exmnr_id)
 REFERENCES examiners (exmnr_id);

ALTER TABLE examiner_specializations
 ADD CONSTRAINT fk_esp_specializations
 FOREIGN KEY (spl_id)
 REFERENCES specializations (spl_id);

-- Foreign key EXCEPTION_DETAILS
ALTER TABLE exception_details
 ADD CONSTRAINT fk_exception_log
 FOREIGN KEY (exception_id)
 REFERENCES exception_log(exception_id);

-- Foreign Keys EXAMINERS
ALTER TABLE examiners
 ADD CONSTRAINT fk_exm_ranks
 FOREIGN KEY (exmnr_rank_id)
 REFERENCES ranks (rank_id);

ALTER TABLE examiners
 ADD CONSTRAINT fk_exm_examination_places
 FOREIGN KEY (exmnr_place)
 REFERENCES examination_places (ep_id);

ALTER TABLE examiners
 ADD CONSTRAINT fk_exm_users_created_by
 FOREIGN KEY (created_by)
 REFERENCES users (user_id);

ALTER TABLE examiners
 ADD CONSTRAINT fk_exm_users_updated_by
 FOREIGN KEY (updated_by)
 REFERENCES users (user_id);


-- Foreign key CODEC_REQUIREMENTS
ALTER TABLE codec_requirements
 ADD CONSTRAINT fk_cr_prism 
 FOREIGN KEY (pc_seq)
 REFERENCES prism_codec (pc_seq);

ALTER TABLE codec_requirements
 ADD CONSTRAINT fk_cr_reqt
 FOREIGN KEY (reqt_code)
 REFERENCES requirements (reqt_code);


-- Foreign key CODEC_KO_CODEC
ALTER TABLE codec_ko_codec
 ADD CONSTRAINT fk_ckc_prism
 FOREIGN KEY (pc_seq) 
 REFERENCES prism_codec(pc_seq);

ALTER TABLE codec_ko_codec
 ADD CONSTRAINT fk_ckc_ko
 FOREIGN KEY (ko_seq)
 REFERENCES prism_codec (pc_seq);

-- Foreign Key CLIENT_DATA_SHEETS
ALTER TABLE client_data_sheets
 ADD CONSTRAINT fk_cds_assessment_requests
 FOREIGN KEY (cds_reference_num)
 REFERENCES assessment_requests (reference_num);

ALTER TABLE client_data_sheets
 ADD CONSTRAINT fk_cds_clients
 FOREIGN KEY (cds_client_num)
 REFERENCES clients (client_id);
 
-- Foreign Key AUTO_ASSIGNMENTS
ALTER TABLE auto_assignments
 ADD CONSTRAINT fk_aac_users
 FOREIGN KEY (user_id)
 REFERENCES users (user_id);
 
ALTER TABLE auto_assignments
 ADD CONSTRAINT fk_aac_auto_assignment_cri
 FOREIGN KEY (aac_id)
 REFERENCES auto_assignment_criteria (aac_id);


-- Foreign Key ASSESSMENT_REQUESTS
ALTER TABLE assessment_requests
 ADD CONSTRAINT fk_ar_lines_of_business
 FOREIGN KEY (lob)
 REFERENCES lines_of_business (lob_code);

ALTER TABLE assessment_requests
 ADD CONSTRAINT fk_ar_clients_insured
 FOREIGN KEY (insured_client_id)
 REFERENCES clients (client_id);

ALTER TABLE assessment_requests
 ADD CONSTRAINT fk_ar_clients_owner
 FOREIGN KEY (owner_client_id)
 REFERENCES clients (client_id);

ALTER TABLE assessment_requests
 ADD CONSTRAINT fk_ar_branches
 FOREIGN KEY (branch_id)
 REFERENCES sunlife_offices (slo_office_code);

ALTER TABLE assessment_requests
 ADD CONSTRAINT fk_ar_users_agent
 FOREIGN KEY (agent_id)
 REFERENCES users (user_id);

ALTER TABLE assessment_requests
 ADD CONSTRAINT fk_ar_status
 FOREIGN KEY (status_id)
 REFERENCES  status (stat_id);

ALTER TABLE assessment_requests
 ADD CONSTRAINT fk_ar_users_assigned_to
 FOREIGN KEY (assigned_to)
 REFERENCES users (user_id);

ALTER TABLE assessment_requests
 ADD CONSTRAINT fk_ar_users_underwriter
 FOREIGN KEY (underwriter)
 REFERENCES users (user_id);

ALTER TABLE assessment_requests
 ADD CONSTRAINT fk_ar_folder_location
 FOREIGN KEY (folder_location)
 REFERENCES users (user_id);

ALTER TABLE assessment_requests
 ADD CONSTRAINT fk_ar_users_created_by
 FOREIGN KEY (created_by)
 REFERENCES users (user_id);

ALTER TABLE assessment_requests
 ADD CONSTRAINT fk_ar_users_updated_by
 FOREIGN KEY (updated_by)
 REFERENCES users (user_id);


-- Foreign Key ACTIVITY_LOGS
ALTER TABLE activity_logs
 ADD CONSTRAINT fk_act_users
 FOREIGN KEY (user_id)
 REFERENCES users (user_id);

ALTER TABLE activity_logs
 ADD CONSTRAINT fk_act_status
 FOREIGN KEY (stat_id)
 REFERENCES status (stat_id);

ALTER TABLE activity_logs
 ADD CONSTRAINT fk_act_assessment_requests
 FOREIGN KEY (uar_reference_num)
 REFERENCES assessment_requests (reference_num);

 
-- Foreign Keys ACCESS_TEMPLATE_DETAILS
ALTER TABLE access_template_details
 ADD CONSTRAINT fk_atd_access_template
 FOREIGN KEY (templt_id)
 REFERENCES access_templates (templt_id);

ALTER TABLE access_template_details
 ADD CONSTRAINT fk_atd_pages_access
 FOREIGN KEY (pa_id)
 REFERENCES pages_access (pa_id); 

-- Foreign Key ACCESS_TEMPLATES
ALTER TABLE access_templates
 ADD CONSTRAINT fk_at_role 
 FOREIGN KEY (role_id)
 REFERENCES roles (role_code);
 
ALTER TABLE access_templates
 ADD CONSTRAINT fk_at_users_created_by
 FOREIGN KEY (created_by)
 REFERENCES users(user_id);

ALTER TABLE access_templates
 ADD CONSTRAINT fk_at_users_updated_by
 FOREIGN KEY (updated_by)
 REFERENCES users(user_id);

-- Foreign key UW_ASSESSMENT_REQUESTS
ALTER TABLE uw_assessment_requests
 ADD CONSTRAINT fk_uw_assessment_requests
 FOREIGN KEY (reference_num)
 REFERENCES assessment_requests(reference_num);
 
-- Foreign key ATTACHMENTS
ALTER TABLE attachments
 ADD CONSTRAINT fk_attachments
 FOREIGN KEY (uar_reference_num)
 REFERENCES assessment_requests(reference_num);

Alter table attachments
 ADD CONSTRAINT fk_req_attachments
 FOREIGN KEY (requirement_id)
 REFERENCES policy_requirements(pr_requirement_id);
 
-- Check Constraint STATUS
ALTER TABLE status
 ADD CONSTRAINT chk_stat_type
 CHECK (stat_type IN ('AR','NM','M'));

-- Check Constraints PAGES_ACCESS
ALTER TABLE pages_access
 ADD CONSTRAINT chk_pa_enabled_ind
 CHECK (pa_enabled_ind in ('0','1')); 
 
-- Check Constraint JOB_SCHEDULES
ALTER TABLE job_schedules
 ADD CONSTRAINT chk_sched_type
 CHECK (sched_type IN ('ME', 'AE'));
 
-- Check Constraints CLIENTS
ALTER TABLE clients
  ADD CONSTRAINT chk_smoker
  CHECK (cl_smoker IN ('0','1'));
 
-- Check Constraint AUTO_ASSIGNMENT_CRITERIA
ALTER TABLE auto_assignment_criteria
 ADD CONSTRAINT chk_acc_status
 CHECK (aac_status IN ('0','1'));
 
-- Check Constraint ACCESS_TEMPLATE_DETAILS
ALTER TABLE access_template_details
 ADD CONSTRAINT chk_atd_access_ind
 CHECK (atd_access_ind in ('0','1'));
 


-- INDEX
PROMPT CREATING INDEX
CREATE INDEX idx_pend_recs_exmnr ON medical_records
(
med_record_id ASC,
med_status ASC,
med_request_date ASC,
med_lab_id ASC
);
 
 
CREATE INDEX idx_pend_recs_lab ON medical_records
(
med_record_id ASC,
med_status ASC,
med_request_date ASC,
exmnr_id ASC
);
 

PROMPT CREATING PROC_CREATE_AUDIT_TRAIL
create or replace procedure PROC_CREATE_AUDIT_TRAIL
   (in_record       in number
   ,in_type         in varchar2
   ,in_userid       in varchar2
   ,in_refnum       in varchar2
   ,in_changed_from in varchar2
   ,in_changed_to   in varchar2) AS 
begin
   insert into AUDIT_TRAILS
   (TRAN_NUM, TRAN_RECORD, TRAN_TYPE, TRAN_DATETIME, USER_ID, UAR_REFERENCE_NUM, TRAN_CHANGED_FROM, TRAN_CHANGED_TO)
   values(SEQ_AUDIT_TRAIL.NEXTVAL, in_record, in_type, SYSDATE, in_userid, in_refnum, in_changed_from, in_changed_to);
end;
/

PROMPT CREATING TGR_AAC_AUDIT_TRAIL
create or replace trigger TGR_AAC_AUDIT_TRAIL
   after update or delete on AUTO_ASSIGNMENT_CRITERIA for each row

declare
   changed_from	varchar2(2000);
   changed_to	varchar2(2000);

   procedure APPEND_TO_CHANGED_FROM(old_value varchar2) is
   begin
      if LENGTH(changed_from) = 0 then
         changed_from := '<?xml version="1.0" encoding="ISO-8859-1"?>';
      end if;
      changed_from := changed_from || old_value;
   end;

   procedure APPEND_TO_CHANGED_TO(new_value varchar2) is
   begin
      if LENGTH(changed_to) = 0 then
         changed_to := '<?xml version="1.0" encoding="ISO-8859-1"?>';
      end if;
      changed_to := changed_to || new_value;
   end;

begin
   if updating then
      if nvl(:OLD.AAC_FIELD, 0) != nvl(:NEW.AAC_FIELD, 0) then
         APPEND_TO_CHANGED_FROM('<CriteriaID>' || to_char(:OLD.AAC_FIELD) || '</CriteriaID>');
         APPEND_TO_CHANGED_TO('<CriteriaID>' || to_char(:NEW.AAC_FIELD) || '</CriteriaID>');
      end if;
      if nvl(:OLD.AAC_FIELD, '') != nvl(:NEW.AAC_FIELD, '') then
         APPEND_TO_CHANGED_FROM('<CriteriaField>' || :OLD.AAC_FIELD || '</CriteriaField>');
         APPEND_TO_CHANGED_TO('<CriteriaField>' || :NEW.AAC_FIELD || '</CriteriaField>');
      end if;
      if nvl(:OLD.AAC_STATUS, '') != nvl(:NEW.AAC_STATUS, '') then
         APPEND_TO_CHANGED_FROM('<Status>' || :OLD.AAC_STATUS || '</Status>');
         APPEND_TO_CHANGED_TO('<Status>' || :NEW.AAC_STATUS || '</Status>');
      end if;
      if nvl(:OLD.CREATED_BY, '') != nvl(:NEW.CREATED_BY, '') then
         APPEND_TO_CHANGED_FROM('<CreatedBy>' || :OLD.CREATED_BY || '</CreatedBy>');
         APPEND_TO_CHANGED_TO('<CreatedBy>' || :NEW.CREATED_BY || '</CreatedBy>');
      end if;
      if nvl(:OLD.CREATED_DATE, '') != nvl(:NEW.CREATED_DATE, '') then
         APPEND_TO_CHANGED_FROM('<CreatedDate>' || to_char(:OLD.CREATED_DATE) || '</CreatedDate>');
         APPEND_TO_CHANGED_TO('<CreatedDate>' || to_char(:NEW.CREATED_DATE) || '</CreatedDate>');
      end if;
      if nvl(:OLD.UPDATED_BY, '') != nvl(:NEW.UPDATED_BY, '') then
         APPEND_TO_CHANGED_FROM('<UpdatedBy>' || :OLD.UPDATED_BY || '</UpdatedBy>');
         APPEND_TO_CHANGED_TO('<UpdatedBy>' || :NEW.UPDATED_BY || '</UpdatedBy>');
      end if;
      if nvl(:OLD.UPDATED_DATE, '') != nvl(:NEW.UPDATED_DATE, '') then
         APPEND_TO_CHANGED_FROM('<UpdatedDate>' || to_char(:OLD.UPDATED_DATE) || '</UpdatedDate>');
         APPEND_TO_CHANGED_TO('<UpdatedDate>' || to_char(:NEW.UPDATED_DATE) || '</UpdatedDate>');
      end if;

      if LENGTH(changed_from) > 0 then
         PROC_CREATE_AUDIT_TRAIL(18, 'UPDATE', :NEW.UPDATED_BY, to_char(:NEW.AAC_ID), changed_from, changed_to);
      end if;
   elsif deleting then
      PROC_CREATE_AUDIT_TRAIL(18, 'DELETE', :OLD.UPDATED_BY, to_char(:OLD.AAC_ID), changed_from, changed_to);
   end if;
end;
/


PROMPT CREATING TGR_AA_AUDIT_TRAIL
create or replace trigger TGR_AA_AUDIT_TRAIL
   after update or delete on AUTO_ASSIGNMENTS for each row

declare
   changed_from	varchar2(2000);
   changed_to	varchar2(2000);

   procedure APPEND_TO_CHANGED_FROM(old_value varchar2) is
   begin
      if LENGTH(changed_from) = 0 then
         changed_from := '<?xml version="1.0" encoding="ISO-8859-1"?>';
      end if;
      changed_from := changed_from || old_value;
   end;

   procedure APPEND_TO_CHANGED_TO(new_value varchar2) is
   begin
      if LENGTH(changed_to) = 0 then
         changed_to := '<?xml version="1.0" encoding="ISO-8859-1"?>';
      end if;
      changed_to := changed_to || new_value;
   end;

begin
   if updating then
      if nvl(:OLD.USER_ID, '') != nvl(:NEW.USER_ID, '') then
         APPEND_TO_CHANGED_FROM('<UserCode>' || :OLD.USER_ID || '</UserCode>');
         APPEND_TO_CHANGED_TO('<UserCode>' || :NEW.USER_ID || '</UserCode>');
      end if;
      if nvl(:OLD.AAC_ID, 0) != nvl(:NEW.AAC_ID, 0) then
         APPEND_TO_CHANGED_FROM('<CriteriaID>' || to_char(:OLD.AAC_ID) || '</CriteriaID>');
         APPEND_TO_CHANGED_TO('<CriteriaID>' || to_char(:NEW.AAC_ID) || '</CriteriaID>');
      end if;
      if nvl(:OLD.CRITERIA_FIELD_VALUE, '') != nvl(:NEW.CRITERIA_FIELD_VALUE, '') then
         APPEND_TO_CHANGED_FROM('<FieldValue>' || :OLD.CRITERIA_FIELD_VALUE || '</FieldValue>');
         APPEND_TO_CHANGED_TO('<FieldValue>' || :NEW.CRITERIA_FIELD_VALUE || '</FieldValue>');
      end if;
      if nvl(:OLD.CREATED_BY, '') != nvl(:NEW.CREATED_BY, '') then
         APPEND_TO_CHANGED_FROM('<CreatedBy>' || :OLD.CREATED_BY || '</CreatedBy>');
         APPEND_TO_CHANGED_TO('<CreatedBy>' || :NEW.CREATED_BY || '</CreatedBy>');
      end if;
      if nvl(:OLD.CREATED_DATE, '') != nvl(:NEW.CREATED_DATE, '') then
         APPEND_TO_CHANGED_FROM('<CreatedDate>' || to_char(:OLD.CREATED_DATE) || '</CreatedDate>');
         APPEND_TO_CHANGED_TO('<CreatedDate>' || to_char(:NEW.CREATED_DATE) || '</CreatedDate>');
      end if;
      if nvl(:OLD.UPDATED_BY, '') != nvl(:NEW.UPDATED_BY, '') then
         APPEND_TO_CHANGED_FROM('<UpdatedBy>' || :OLD.UPDATED_BY || '</UpdatedBy>');
         APPEND_TO_CHANGED_TO('<UpdatedBy>' || :NEW.UPDATED_BY || '</UpdatedBy>');
      end if;
      if nvl(:OLD.UPDATED_DATE, '') != nvl(:NEW.UPDATED_DATE, '') then
         APPEND_TO_CHANGED_FROM('<UpdatedDate>' || to_char(:OLD.UPDATED_DATE) || '</UpdatedDate>');
         APPEND_TO_CHANGED_TO('<UpdatedDate>' || to_char(:NEW.UPDATED_DATE) || '</UpdatedDate>');
      end if;

      if LENGTH(changed_from) > 0 then
         PROC_CREATE_AUDIT_TRAIL(25, 'UPDATE', :NEW.UPDATED_BY, :NEW.USER_ID || '-' || to_char(:NEW.AAC_ID), changed_from, changed_to);
      end if;
   elsif deleting then
      PROC_CREATE_AUDIT_TRAIL(25, 'DELETE', :OLD.UPDATED_BY, :OLD.USER_ID || '-' || to_char(:OLD.AAC_ID), changed_from, changed_to);
   end if;
end;
/

PROMPT CREATING TGR_ACC_AUDIT_TRAIL
create or replace trigger TGR_ACC_AUDIT_TRAIL
   after update or delete on ACCESSES for each row

declare
   changed_from	varchar2(2000);
   changed_to	varchar2(2000);

   procedure APPEND_TO_CHANGED_FROM(old_value varchar2) is
   begin
      if LENGTH(changed_from) = 0 then
         changed_from := '<?xml version="1.0" encoding="ISO-8859-1"?>';
      end if;
      changed_from := changed_from || old_value;
   end;

   procedure APPEND_TO_CHANGED_TO(new_value varchar2) is
   begin
      if LENGTH(changed_to) = 0 then
         changed_to := '<?xml version="1.0" encoding="ISO-8859-1"?>';
      end if;
      changed_to := changed_to || new_value;
   end;

begin
   if updating then
      if nvl(:OLD.ACC_ID, 0) != nvl(:NEW.ACC_ID, 0) then
         APPEND_TO_CHANGED_FROM('<AccessID>' || to_char(:OLD.ACC_ID) || '</AccessID>');
         APPEND_TO_CHANGED_TO('<AccessID>' || to_char(:NEW.ACC_ID) || '</AccessID>');
      end if;
      if nvl(:OLD.ACC_DESC, '') != nvl(:NEW.ACC_DESC, '') then
         APPEND_TO_CHANGED_FROM('<Description>' || :OLD.ACC_DESC || '</Description>');
         APPEND_TO_CHANGED_TO('<Description>' || :NEW.ACC_DESC || '</Description>');
      end if;
      if nvl(:OLD.CREATED_BY, '') != nvl(:NEW.CREATED_BY, '') then
         APPEND_TO_CHANGED_FROM('<CreatedBy>' || :OLD.CREATED_BY || '</CreatedBy>');
         APPEND_TO_CHANGED_TO('<CreatedBy>' || :NEW.CREATED_BY || '</CreatedBy>');
      end if;
      if nvl(:OLD.CREATED_DATE, '') != nvl(:NEW.CREATED_DATE, '') then
         APPEND_TO_CHANGED_FROM('<CreatedDate>' || to_char(:OLD.CREATED_DATE) || '</CreatedDate>');
         APPEND_TO_CHANGED_TO('<CreatedDate>' || to_char(:NEW.CREATED_DATE) || '</CreatedDate>');
      end if;
      if nvl(:OLD.UPDATED_BY, '') != nvl(:NEW.UPDATED_BY, '') then
         APPEND_TO_CHANGED_FROM('<UpdatedBy>' || :OLD.UPDATED_BY || '</UpdatedBy>');
         APPEND_TO_CHANGED_TO('<UpdatedBy>' || :NEW.UPDATED_BY || '</UpdatedBy>');
      end if;
      if nvl(:OLD.UPDATED_DATE, '') != nvl(:NEW.UPDATED_DATE, '') then
         APPEND_TO_CHANGED_FROM('<UpdatedDate>' || to_char(:OLD.UPDATED_DATE) || '</UpdatedDate>');
         APPEND_TO_CHANGED_TO('<UpdatedDate>' || to_char(:NEW.UPDATED_DATE) || '</UpdatedDate>');
      end if;

      if LENGTH(changed_from) > 0 then
         PROC_CREATE_AUDIT_TRAIL(40, 'UPDATE', :NEW.UPDATED_BY, to_char(:NEW.ACC_ID), changed_from, changed_to);
      end if;
   elsif deleting then
      PROC_CREATE_AUDIT_TRAIL(40, 'DELETE', :OLD.UPDATED_BY, to_char(:OLD.ACC_ID), changed_from, changed_to);
   end if;
end;
/


PROMPT CREATING TGR_ACTIVITY_LOG
create or replace trigger TGR_ACTIVITY_LOG
   after insert or update on ASSESSMENT_REQUESTS for each row

declare

   V_ACT_DATETIME	date;
   V_ASSIGNED_TO	varchar2(10);

   function COMPUTE_ELAPSE_TIME(P_FROM_DATE date, P_TO_DATE date) return number is

      V_ELAPSE_TIME	NUMBER(6,2);
      V_HOLIDAYS	NUMBER(3);

   begin
      begin
         select count(*) into V_HOLIDAYS
            from HOLIDAYS
            where trunc(HOL_DATE) between trunc(P_FROM_DATE) and trunc(P_TO_DATE)
            and upper(to_char(HOL_DATE, 'DY')) not in('SAT', 'SUN');
      end;
      V_ELAPSE_TIME := P_TO_DATE - P_FROM_DATE 
                     - ((round(P_TO_DATE - P_FROM_DATE) / 7) * 2)
                     - case when to_char(P_FROM_DATE, 'd') > to_char(P_TO_DATE, 'd') then 2 else 0 end
                     - V_HOLIDAYS;
      return round(V_ELAPSE_TIME,2);
   end;

   procedure INSERT_ACTIVITY_LOG(P_USER_ID	varchar2
      							,P_ASSIGNED_TO varchar2
                                ,P_ELAPSE_TIME	number) is
   begin
      insert into ACTIVITY_LOGS
         (ACT_ID
         ,UAR_REFERENCE_NUM
         ,STAT_ID
         ,ACT_DATETIME
         ,USER_ID
         ,ACT_ASSIGNED_TO
         ,ACT_ELAPSE_TIME)
      values
         (SEQ_ACTIVITY_LOGS.NEXTVAL
         ,:NEW.REFERENCE_NUM
         ,:NEW.STATUS_ID
         ,SYSDATE
         ,P_USER_ID
         ,P_ASSIGNED_TO
         ,P_ELAPSE_TIME);
   end;

begin
   if inserting then
   	  V_ASSIGNED_TO := :NEW.ASSIGNED_TO;
      INSERT_ACTIVITY_LOG(:NEW.CREATED_BY, V_ASSIGNED_TO, 0);
   elsif updating then
      if :OLD.STATUS_ID != :NEW.STATUS_ID then
         begin
            select max(ACT_DATETIME) into V_ACT_DATETIME
               from ACTIVITY_LOGS
               where UAR_REFERENCE_NUM = :OLD.REFERENCE_NUM;
         end;
         V_ASSIGNED_TO := :OLD.ASSIGNED_TO;
         if :OLD.ASSIGNED_TO != :NEW.ASSIGNED_TO then
         	V_ASSIGNED_TO := :NEW.ASSIGNED_TO;
         end if;
         INSERT_ACTIVITY_LOG(:NEW.UPDATED_BY, V_ASSIGNED_TO, COMPUTE_ELAPSE_TIME(V_ACT_DATETIME, sysdate));
      elsif :OLD.ASSIGNED_TO != :NEW.ASSIGNED_TO then
         begin
            select max(ACT_DATETIME) into V_ACT_DATETIME
               from ACTIVITY_LOGS
               where UAR_REFERENCE_NUM = :OLD.REFERENCE_NUM;
         end;
		 V_ASSIGNED_TO := :NEW.ASSIGNED_TO;
         INSERT_ACTIVITY_LOG(:NEW.UPDATED_BY, V_ASSIGNED_TO, COMPUTE_ELAPSE_TIME(V_ACT_DATETIME, sysdate));
      end if;
   end if;
end;
/


PROMPT CREATING TGR_AR_AUDIT_TRAIL 
create or replace trigger TGR_AR_AUDIT_TRAIL 
   after update on ASSESSMENT_REQUESTS for each row

declare
   changed_from	varchar2(2000);
   changed_to	varchar2(2000);

   procedure APPEND_TO_CHANGED_FROM(old_value varchar2) is
   begin
      if LENGTH(changed_from) = 0 then
         changed_from := '<?xml version="1.0" encoding="ISO-8859-1"?>';
      end if;
      changed_from := changed_from || old_value;
   end;

   procedure APPEND_TO_CHANGED_TO(new_value varchar2) is
   begin
      if LENGTH(changed_to) = 0 then
         changed_to := '<?xml version="1.0" encoding="ISO-8859-1"?>';
      end if;
      changed_to := changed_to || new_value;
   end;

begin
      if nvl(:OLD.REFERENCE_NUM, '') != nvl(:NEW.REFERENCE_NUM, '') then
         APPEND_TO_CHANGED_FROM('<ReferenceNumber>' || :OLD.REFERENCE_NUM || '</ReferenceNumber>');
         APPEND_TO_CHANGED_TO('<ReferenceNumber>' || :NEW.REFERENCE_NUM || '</ReferenceNumber>');
      end if;
      if nvl(:OLD.LOB, '') != nvl(:NEW.LOB, '') then
         APPEND_TO_CHANGED_FROM('<LOB>' || :OLD.LOB || '</LOB>');
         APPEND_TO_CHANGED_TO('<LOB>' || :NEW.LOB || '</LOB>');
      end if;
      if nvl(:OLD.INSURED_CLIENT_ID, '') != nvl(:NEW.INSURED_CLIENT_ID, '') then
         APPEND_TO_CHANGED_FROM('<InsuredClientID>' || :OLD.INSURED_CLIENT_ID || '</InsuredClientID>');
         APPEND_TO_CHANGED_TO('<InsuredClientID>' || :NEW.INSURED_CLIENT_ID || '</InsuredClientID>');
      end if;
      if nvl(:OLD.OWNER_CLIENT_ID, '') != nvl(:NEW.OWNER_CLIENT_ID, '') then
         APPEND_TO_CHANGED_FROM('<OwnerClientID>' || :OLD.OWNER_CLIENT_ID || '</OwnerClientID>');
         APPEND_TO_CHANGED_TO('<OwnerClientID>' || :NEW.OWNER_CLIENT_ID || '</OwnerClientID>');
      end if;
      if nvl(:OLD.BRANCH_ID, '') != nvl(:NEW.BRANCH_ID, '') then
         APPEND_TO_CHANGED_FROM('<BranchID>' || :OLD.BRANCH_ID || '</BranchID>');
         APPEND_TO_CHANGED_TO('<BranchID>' || :NEW.BRANCH_ID || '</BranchID>');
      end if;
      if nvl(:OLD.AGENT_ID, '') != nvl(:NEW.AGENT_ID, '') then
         APPEND_TO_CHANGED_FROM('<AgentID>' || :OLD.AGENT_ID || '</AgentID>');
         APPEND_TO_CHANGED_TO('<AgentID>' || :NEW.AGENT_ID || '</AgentID>');
      end if;
      if nvl(:OLD.STATUS_ID, 0) != nvl(:NEW.STATUS_ID, 0) then
         APPEND_TO_CHANGED_FROM('<RequestStatusCode>' || to_char(:OLD.STATUS_ID) || '</RequestStatusCode>');
         APPEND_TO_CHANGED_TO('<RequestStatusCode>' || to_char(:NEW.STATUS_ID) || '</RequestStatusCode>');
      end if;
      if nvl(:OLD.STATUS_DATE, '') != nvl(:NEW.STATUS_DATE, '') then
         APPEND_TO_CHANGED_FROM('<StatusDate>' || to_char(:OLD.STATUS_DATE) || '</StatusDate>');
         APPEND_TO_CHANGED_TO('<StatusDate>' || to_char(:NEW.STATUS_DATE) || '</StatusDate>');
      end if;
      if nvl(:OLD.ASSIGNED_TO, 0) != nvl(:NEW.ASSIGNED_TO, 0) then
         APPEND_TO_CHANGED_FROM('<AssignedTo>' || :OLD.ASSIGNED_TO || '</AssignedTo>');
         APPEND_TO_CHANGED_TO('<AssignedTo>' || :NEW.ASSIGNED_TO || '</AssignedTo>');
      end if;
      if nvl(:OLD.UNDERWRITER, 0) != nvl(:NEW.UNDERWRITER, 0) then
         APPEND_TO_CHANGED_FROM('<Underwriter>' || :OLD.UNDERWRITER || '</Underwriter>');
         APPEND_TO_CHANGED_TO('<Underwriter>' || :NEW.UNDERWRITER || '</Underwriter>');
      end if;
      if nvl(:OLD.FOLDER_LOCATION, 0) != nvl(:NEW.FOLDER_LOCATION, 0) then
         APPEND_TO_CHANGED_FROM('<FolderLocation>' || :OLD.FOLDER_LOCATION || '</FolderLocation>');
         APPEND_TO_CHANGED_TO('<FolderLocation>' || :NEW.FOLDER_LOCATION || '</FolderLocation>');
      end if;
      if nvl(:OLD.CREATED_BY, '') != nvl(:NEW.CREATED_BY, '') then
         APPEND_TO_CHANGED_FROM('<CreatedBy>' || :OLD.CREATED_BY || '</CreatedBy>');
         APPEND_TO_CHANGED_TO('<CreatedBy>' || :NEW.CREATED_BY || '</CreatedBy>');
      end if;
      if nvl(:OLD.CREATED_DATE, '') != nvl(:NEW.CREATED_DATE, '') then
         APPEND_TO_CHANGED_FROM('<CreatedDate>' || to_char(:OLD.CREATED_DATE) || '</CreatedDate>');
         APPEND_TO_CHANGED_TO('<CreatedDate>' || to_char(:NEW.CREATED_DATE) || '</CreatedDate>');
      end if;
      if nvl(:OLD.UPDATED_BY, '') != nvl(:NEW.UPDATED_BY, '') then
         APPEND_TO_CHANGED_FROM('<UpdatedBy>' || :OLD.UPDATED_BY || '</UpdatedBy>');
         APPEND_TO_CHANGED_TO('<UpdatedBy>' || :NEW.UPDATED_BY || '</UpdatedBy>');
      end if;
      if nvl(:OLD.UPDATED_DATE, '') != nvl(:NEW.UPDATED_DATE, '') then
         APPEND_TO_CHANGED_FROM('<UpdatedDate>' || to_char(:OLD.UPDATED_DATE) || '</UpdatedDate>');
         APPEND_TO_CHANGED_TO('<UpdatedDate>' || to_char(:NEW.UPDATED_DATE) || '</UpdatedDate>');
      end if;

      if LENGTH(changed_from) > 0 then
         PROC_CREATE_AUDIT_TRAIL(1, 'UPDATE', :NEW.UPDATED_BY, :NEW.REFERENCE_NUM, changed_from, changed_to);
      end if;
end;
/

PROMPT CREATING TGR_ATD_AUDIT_TRAIL
create or replace trigger TGR_ATD_AUDIT_TRAIL
   after update or delete on ACCESS_TEMPLATE_DETAILS for each row

declare
   changed_from	varchar2(2000);
   changed_to	varchar2(2000);

   procedure APPEND_TO_CHANGED_FROM(old_value varchar2) is
   begin
      if LENGTH(changed_from) = 0 then
         changed_from := '<?xml version="1.0" encoding="ISO-8859-1"?>';
      end if;
      changed_from := changed_from || old_value;
   end;

   procedure APPEND_TO_CHANGED_TO(new_value varchar2) is
   begin
      if LENGTH(changed_to) = 0 then
         changed_to := '<?xml version="1.0" encoding="ISO-8859-1"?>';
      end if;
      changed_to := changed_to || new_value;
   end;

begin
   if updating then
      if nvl(:OLD.TEMPLT_ID, 0) != nvl(:NEW.TEMPLT_ID, 0) then
         APPEND_TO_CHANGED_FROM('<TemplateID>' || to_char(:OLD.TEMPLT_ID) || '</TemplateID>');
         APPEND_TO_CHANGED_TO('<TemplateID>' || to_char(:NEW.TEMPLT_ID) || '</TemplateID>');
      end if;
      if nvl(:OLD.PA_ID, 0) != nvl(:NEW.PA_ID, 0) then
         APPEND_TO_CHANGED_FROM('<PageAccessID>' || to_char(:OLD.PA_ID) || '</PageAccessID>');
         APPEND_TO_CHANGED_TO('<PageAccessID>' || to_char(:NEW.PA_ID) || '</PageAccessID>');
      end if;
      if nvl(:OLD.ATD_ACCESS_IND, '') != nvl(:NEW.ATD_ACCESS_IND, '') then
         APPEND_TO_CHANGED_FROM('<AccessIndicator>' || :OLD.ATD_ACCESS_IND || '</AccessIndicator>');
         APPEND_TO_CHANGED_TO('<AccessIndicator>' || :NEW.ATD_ACCESS_IND || '</AccessIndicator>');
      end if;
      if nvl(:OLD.CREATED_BY, '') != nvl(:NEW.CREATED_BY, '') then
         APPEND_TO_CHANGED_FROM('<CreatedBy>' || :OLD.CREATED_BY || '</CreatedBy>');
         APPEND_TO_CHANGED_TO('<CreatedBy>' || :NEW.CREATED_BY || '</CreatedBy>');
      end if;
      if nvl(:OLD.CREATED_DATE, '') != nvl(:NEW.CREATED_DATE, '') then
         APPEND_TO_CHANGED_FROM('<CreatedDate>' || to_char(:OLD.CREATED_DATE) || '</CreatedDate>');
         APPEND_TO_CHANGED_TO('<CreatedDate>' || to_char(:NEW.CREATED_DATE) || '</CreatedDate>');
      end if;
      if nvl(:OLD.UPDATED_BY, '') != nvl(:NEW.UPDATED_BY, '') then
         APPEND_TO_CHANGED_FROM('<UpdatedBy>' || :OLD.UPDATED_BY || '</UpdatedBy>');
         APPEND_TO_CHANGED_TO('<UpdatedBy>' || :NEW.UPDATED_BY || '</UpdatedBy>');
      end if;
      if nvl(:OLD.UPDATED_DATE, '') != nvl(:NEW.UPDATED_DATE, '') then
         APPEND_TO_CHANGED_FROM('<UpdatedDate>' || to_char(:OLD.UPDATED_DATE) || '</UpdatedDate>');
         APPEND_TO_CHANGED_TO('<UpdatedDate>' || to_char(:NEW.UPDATED_DATE) || '</UpdatedDate>');
      end if;

      if LENGTH(changed_from) > 0 then
         PROC_CREATE_AUDIT_TRAIL(43, 'UPDATE', :NEW.UPDATED_BY, to_char(:NEW.TEMPLT_ID) || '-' || to_char(:NEW.PA_ID), changed_from, changed_to);
      end if;
   elsif deleting then
      PROC_CREATE_AUDIT_TRAIL(43, 'DELETE', :OLD.UPDATED_BY, to_char(:OLD.TEMPLT_ID) || '-' || to_char(:OLD.PA_ID), changed_from, changed_to);
   end if;
end;
/


PROMPT CREATING TGR_CLT_AUDIT_TRAIL
create or replace trigger TGR_CLT_AUDIT_TRAIL
   after update or delete on CLIENT_TYPES for each row

declare
   changed_from	varchar2(2000);
   changed_to	varchar2(2000);

   procedure APPEND_TO_CHANGED_FROM(old_value varchar2) is
   begin
      if LENGTH(changed_from) = 0 then
         changed_from := '<?xml version="1.0" encoding="ISO-8859-1"?>';
      end if;
      changed_from := changed_from || old_value;
   end;

   procedure APPEND_TO_CHANGED_TO(new_value varchar2) is
   begin
      if LENGTH(changed_to) = 0 then
         changed_to := '<?xml version="1.0" encoding="ISO-8859-1"?>';
      end if;
      changed_to := changed_to || new_value;
   end;

begin
   if updating then
      if nvl(:OLD.CLT_CODE, '') != nvl(:NEW.CLT_CODE, '') then
         APPEND_TO_CHANGED_FROM('<TypeCode>' || :OLD.CLT_CODE || '</TypeCode>');
         APPEND_TO_CHANGED_TO('<TypeCode>' || :NEW.CLT_CODE || '</TypeCode>');
      end if;
      if nvl(:OLD.CLT_DESC, '') != nvl(:NEW.CLT_DESC, '') then
         APPEND_TO_CHANGED_FROM('<Description>' || :OLD.CLT_DESC || '</Description>');
         APPEND_TO_CHANGED_TO('<Description>' || :NEW.CLT_DESC || '</Description>');
      end if;
      if nvl(:OLD.CREATED_BY, '') != nvl(:NEW.CREATED_BY, '') then
         APPEND_TO_CHANGED_FROM('<CreatedBy>' || :OLD.CREATED_BY || '</CreatedBy>');
         APPEND_TO_CHANGED_TO('<CreatedBy>' || :NEW.CREATED_BY || '</CreatedBy>');
      end if;
      if nvl(:OLD.CREATED_DATE, '') != nvl(:NEW.CREATED_DATE, '') then
         APPEND_TO_CHANGED_FROM('<CreatedDate>' || to_char(:OLD.CREATED_DATE) || '</CreatedDate>');
         APPEND_TO_CHANGED_TO('<CreatedDate>' || to_char(:NEW.CREATED_DATE) || '</CreatedDate>');
      end if;
      if nvl(:OLD.UPDATED_BY, '') != nvl(:NEW.UPDATED_BY, '') then
         APPEND_TO_CHANGED_FROM('<UpdatedBy>' || :OLD.UPDATED_BY || '</UpdatedBy>');
         APPEND_TO_CHANGED_TO('<UpdatedBy>' || :NEW.UPDATED_BY || '</UpdatedBy>');
      end if;
      if nvl(:OLD.UPDATED_DATE, '') != nvl(:NEW.UPDATED_DATE, '') then
         APPEND_TO_CHANGED_FROM('<UpdatedDate>' || to_char(:OLD.UPDATED_DATE) || '</UpdatedDate>');
         APPEND_TO_CHANGED_TO('<UpdatedDate>' || to_char(:NEW.UPDATED_DATE) || '</UpdatedDate>');
      end if;

      if LENGTH(changed_from) > 0 then
         PROC_CREATE_AUDIT_TRAIL(16, 'UPDATE', :NEW.UPDATED_BY, :NEW.CLT_CODE, changed_from, changed_to);
      end if;
   elsif deleting then
      PROC_CREATE_AUDIT_TRAIL(16, 'DELETE', :OLD.UPDATED_BY, :OLD.CLT_CODE, changed_from, changed_to);
   end if;
end;
/


PROMPT CREATING TGR_DEPT_AUDIT_TRAIL
create or replace trigger TGR_DEPT_AUDIT_TRAIL
   after update or delete on DEPARTMENTS for each row

declare
   changed_from	varchar2(2000);
   changed_to	varchar2(2000);

   procedure APPEND_TO_CHANGED_FROM(old_value varchar2) is
   begin
      if LENGTH(changed_from) = 0 then
         changed_from := '<?xml version="1.0" encoding="ISO-8859-1"?>';
      end if;
      changed_from := changed_from || old_value;
   end;

   procedure APPEND_TO_CHANGED_TO(new_value varchar2) is
   begin
      if LENGTH(changed_to) = 0 then
         changed_to := '<?xml version="1.0" encoding="ISO-8859-1"?>';
      end if;
      changed_to := changed_to || new_value;
   end;

begin
   if updating then
      if nvl(:OLD.DEPT_CODE, '') != nvl(:NEW.DEPT_CODE, '') then
         APPEND_TO_CHANGED_FROM('<DepartmentID>' || :OLD.DEPT_CODE || '</DepartmentID>');
         APPEND_TO_CHANGED_TO('<DepartmentID>' || :NEW.DEPT_CODE || '</DepartmentID>');
      end if;
      if nvl(:OLD.DEPT_DESC, '') != nvl(:NEW.DEPT_DESC, '') then
         APPEND_TO_CHANGED_FROM('<Description>' || :OLD.DEPT_DESC || '</Description>');
         APPEND_TO_CHANGED_TO('<Description>' || :NEW.DEPT_DESC || '</Description>');
      end if;
      if nvl(:OLD.CREATED_BY, '') != nvl(:NEW.CREATED_BY, '') then
         APPEND_TO_CHANGED_FROM('<CreatedBy>' || :OLD.CREATED_BY || '</CreatedBy>');
         APPEND_TO_CHANGED_TO('<CreatedBy>' || :NEW.CREATED_BY || '</CreatedBy>');
      end if;
      if nvl(:OLD.CREATED_DATE, '') != nvl(:NEW.CREATED_DATE, '') then
         APPEND_TO_CHANGED_FROM('<CreatedDate>' || to_char(:OLD.CREATED_DATE) || '</CreatedDate>');
         APPEND_TO_CHANGED_TO('<CreatedDate>' || to_char(:NEW.CREATED_DATE) || '</CreatedDate>');
      end if;
      if nvl(:OLD.UPDATED_BY, '') != nvl(:NEW.UPDATED_BY, '') then
         APPEND_TO_CHANGED_FROM('<UpdatedBy>' || :OLD.UPDATED_BY || '</UpdatedBy>');
         APPEND_TO_CHANGED_TO('<UpdatedBy>' || :NEW.UPDATED_BY || '</UpdatedBy>');
      end if;
      if nvl(:OLD.UPDATED_DATE, '') != nvl(:NEW.UPDATED_DATE, '') then
         APPEND_TO_CHANGED_FROM('<UpdatedDate>' || to_char(:OLD.UPDATED_DATE) || '</UpdatedDate>');
         APPEND_TO_CHANGED_TO('<UpdatedDate>' || to_char(:NEW.UPDATED_DATE) || '</UpdatedDate>');
      end if;

      if LENGTH(changed_from) > 0 then
         PROC_CREATE_AUDIT_TRAIL(10, 'UPDATE', :NEW.UPDATED_BY, :NEW.DEPT_CODE, changed_from, changed_to);
      end if;
   elsif deleting then
      PROC_CREATE_AUDIT_TRAIL(10, 'DELETE', :OLD.UPDATED_BY, :OLD.DEPT_CODE, changed_from, changed_to);
   end if;
end;
/


PROMPT CREATING TGR_DT_AUDIT_TRAIL
create or replace trigger TGR_DT_AUDIT_TRAIL
   after update or delete on DOCUMENT_TYPES for each row

declare
   changed_from	varchar2(2000);
   changed_to	varchar2(2000);

   procedure APPEND_TO_CHANGED_FROM(old_value varchar2) is
   begin
      if LENGTH(changed_from) = 0 then
         changed_from := '<?xml version="1.0" encoding="ISO-8859-1"?>';
      end if;
      changed_from := changed_from || old_value;
   end;

   procedure APPEND_TO_CHANGED_TO(new_value varchar2) is
   begin
      if LENGTH(changed_to) = 0 then
         changed_to := '<?xml version="1.0" encoding="ISO-8859-1"?>';
      end if;
      changed_to := changed_to || new_value;
   end;

begin
   if updating then
      if nvl(:OLD.DOC_CODE, '') != nvl(:NEW.DOC_CODE, '') then
         APPEND_TO_CHANGED_FROM('<DocumentCode>' || :OLD.DOC_CODE || '</DocumentCode>');
         APPEND_TO_CHANGED_TO('<DocumentCode>' || :NEW.DOC_CODE || '</DocumentCode>');
      end if;
      if nvl(:OLD.DOC_DESC, '') != nvl(:NEW.DOC_DESC, '') then
         APPEND_TO_CHANGED_FROM('<Description>' || :OLD.DOC_DESC || '</Description>');
         APPEND_TO_CHANGED_TO('<Description>' || :NEW.DOC_DESC || '</Description>');
      end if;
      if nvl(:OLD.CREATED_BY, '') != nvl(:NEW.CREATED_BY, '') then
         APPEND_TO_CHANGED_FROM('<CreatedBy>' || :OLD.CREATED_BY || '</CreatedBy>');
         APPEND_TO_CHANGED_TO('<CreatedBy>' || :NEW.CREATED_BY || '</CreatedBy>');
      end if;
      if nvl(:OLD.CREATED_DATE, '') != nvl(:NEW.CREATED_DATE, '') then
         APPEND_TO_CHANGED_FROM('<CreatedDate>' || to_char(:OLD.CREATED_DATE) || '</CreatedDate>');
         APPEND_TO_CHANGED_TO('<CreatedDate>' || to_char(:NEW.CREATED_DATE) || '</CreatedDate>');
      end if;
      if nvl(:OLD.UPDATED_BY, '') != nvl(:NEW.UPDATED_BY, '') then
         APPEND_TO_CHANGED_FROM('<UpdatedBy>' || :OLD.UPDATED_BY || '</UpdatedBy>');
         APPEND_TO_CHANGED_TO('<UpdatedBy>' || :NEW.UPDATED_BY || '</UpdatedBy>');
      end if;
      if nvl(:OLD.UPDATED_DATE, '') != nvl(:NEW.UPDATED_DATE, '') then
         APPEND_TO_CHANGED_FROM('<UpdatedDate>' || to_char(:OLD.UPDATED_DATE) || '</UpdatedDate>');
         APPEND_TO_CHANGED_TO('<UpdatedDate>' || to_char(:NEW.UPDATED_DATE) || '</UpdatedDate>');
      end if;

      if LENGTH(changed_from) > 0 then
         PROC_CREATE_AUDIT_TRAIL(15, 'UPDATE', :NEW.UPDATED_BY, :NEW.DOC_CODE, changed_from, changed_to);
      end if;
   elsif deleting then
      PROC_CREATE_AUDIT_TRAIL(15, 'DELETE', :OLD.UPDATED_BY, :OLD.DOC_CODE, changed_from, changed_to);
   end if;
end;
/


PROMPT CREATING TGR_EA_AUDIT_TRAIL
create or replace trigger TGR_EA_AUDIT_TRAIL
   after update or delete on EXAMINATION_AREAS for each row

declare
   changed_from	varchar2(2000);
   changed_to	varchar2(2000);

   procedure APPEND_TO_CHANGED_FROM(old_value varchar2) is
   begin
      if LENGTH(changed_from) = 0 then
         changed_from := '<?xml version="1.0" encoding="ISO-8859-1"?>';
      end if;
      changed_from := changed_from || old_value;
   end;

   procedure APPEND_TO_CHANGED_TO(new_value varchar2) is
   begin
      if LENGTH(changed_to) = 0 then
         changed_to := '<?xml version="1.0" encoding="ISO-8859-1"?>';
      end if;
      changed_to := changed_to || new_value;
   end;

begin
   if updating then
      if nvl(:OLD.EA_ID, 0) != nvl(:NEW.EA_ID, 0) then
         APPEND_TO_CHANGED_FROM('<ExaminationAreaID>' || to_char(:OLD.EA_ID) || '</ExaminationAreaID>');
         APPEND_TO_CHANGED_TO('<ExaminationAreaID>' || to_char(:NEW.EA_ID) || '</ExaminationAreaID>');
      end if;
      if nvl(:OLD.EA_DESC, '') != nvl(:NEW.EA_DESC, '') then
         APPEND_TO_CHANGED_FROM('<Description>' || :OLD.EA_DESC || '</Description>');
         APPEND_TO_CHANGED_TO('<Description>' || :NEW.EA_DESC || '</Description>');
      end if;
      if nvl(:OLD.CREATED_BY, '') != nvl(:NEW.CREATED_BY, '') then
         APPEND_TO_CHANGED_FROM('<CreatedBy>' || :OLD.CREATED_BY || '</CreatedBy>');
         APPEND_TO_CHANGED_TO('<CreatedBy>' || :NEW.CREATED_BY || '</CreatedBy>');
      end if;
      if nvl(:OLD.CREATED_DATE, '') != nvl(:NEW.CREATED_DATE, '') then
         APPEND_TO_CHANGED_FROM('<CreatedDate>' || to_char(:OLD.CREATED_DATE) || '</CreatedDate>');
         APPEND_TO_CHANGED_TO('<CreatedDate>' || to_char(:NEW.CREATED_DATE) || '</CreatedDate>');
      end if;
      if nvl(:OLD.UPDATED_BY, '') != nvl(:NEW.UPDATED_BY, '') then
         APPEND_TO_CHANGED_FROM('<UpdatedBy>' || :OLD.UPDATED_BY || '</UpdatedBy>');
         APPEND_TO_CHANGED_TO('<UpdatedBy>' || :NEW.UPDATED_BY || '</UpdatedBy>');
      end if;
      if nvl(:OLD.UPDATED_DATE, '') != nvl(:NEW.UPDATED_DATE, '') then
         APPEND_TO_CHANGED_FROM('<UpdatedDate>' || to_char(:OLD.UPDATED_DATE) || '</UpdatedDate>');
         APPEND_TO_CHANGED_TO('<UpdatedDate>' || to_char(:NEW.UPDATED_DATE) || '</UpdatedDate>');
      end if;

      if LENGTH(changed_from) > 0 then
         PROC_CREATE_AUDIT_TRAIL(14, 'UPDATE', :NEW.UPDATED_BY, to_char(:NEW.EA_ID), changed_from, changed_to);
      end if;
   elsif deleting then
      PROC_CREATE_AUDIT_TRAIL(14, 'DELETE', :OLD.UPDATED_BY, to_char(:OLD.EA_ID), changed_from, changed_to);
   end if;
end;
/

PROMPT CREATING TGR_EP_AUDIT_TRAIL
create or replace trigger TGR_EP_AUDIT_TRAIL
   after update or delete on EXAMINATION_PLACES for each row

declare
   changed_from	varchar2(2000);
   changed_to	varchar2(2000);

   procedure APPEND_TO_CHANGED_FROM(old_value varchar2) is
   begin
      if LENGTH(changed_from) = 0 then
         changed_from := '<?xml version="1.0" encoding="ISO-8859-1"?>';
      end if;
      changed_from := changed_from || old_value;
   end;

   procedure APPEND_TO_CHANGED_TO(new_value varchar2) is
   begin
      if LENGTH(changed_to) = 0 then
         changed_to := '<?xml version="1.0" encoding="ISO-8859-1"?>';
      end if;
      changed_to := changed_to || new_value;
   end;

begin
   if updating then
      if nvl(:OLD.EP_ID, 0) != nvl(:NEW.EP_ID, 0) then
         APPEND_TO_CHANGED_FROM('<ExaminationPlaceID>' || to_char(:OLD.EP_ID) || '</ExaminationPlaceID>');
         APPEND_TO_CHANGED_TO('<ExaminationPlaceID>' || to_char(:NEW.EP_ID) || '</ExaminationPlaceID>');
      end if;
      if nvl(:OLD.EP_DESC, '') != nvl(:NEW.EP_DESC, '') then
         APPEND_TO_CHANGED_FROM('<Description>' || :OLD.EP_DESC || '</Description>');
         APPEND_TO_CHANGED_TO('<Description>' || :NEW.EP_DESC || '</Description>');
      end if;
      if nvl(:OLD.CREATED_BY, '') != nvl(:NEW.CREATED_BY, '') then
         APPEND_TO_CHANGED_FROM('<CreatedBy>' || :OLD.CREATED_BY || '</CreatedBy>');
         APPEND_TO_CHANGED_TO('<CreatedBy>' || :NEW.CREATED_BY || '</CreatedBy>');
      end if;
      if nvl(:OLD.CREATED_DATE, '') != nvl(:NEW.CREATED_DATE, '') then
         APPEND_TO_CHANGED_FROM('<CreatedDate>' || to_char(:OLD.CREATED_DATE) || '</CreatedDate>');
         APPEND_TO_CHANGED_TO('<CreatedDate>' || to_char(:NEW.CREATED_DATE) || '</CreatedDate>');
      end if;
      if nvl(:OLD.UPDATED_BY, '') != nvl(:NEW.UPDATED_BY, '') then
         APPEND_TO_CHANGED_FROM('<UpdatedBy>' || :OLD.UPDATED_BY || '</UpdatedBy>');
         APPEND_TO_CHANGED_TO('<UpdatedBy>' || :NEW.UPDATED_BY || '</UpdatedBy>');
      end if;
      if nvl(:OLD.UPDATED_DATE, '') != nvl(:NEW.UPDATED_DATE, '') then
         APPEND_TO_CHANGED_FROM('<UpdatedDate>' || to_char(:OLD.UPDATED_DATE) || '</UpdatedDate>');
         APPEND_TO_CHANGED_TO('<UpdatedDate>' || to_char(:NEW.UPDATED_DATE) || '</UpdatedDate>');
      end if;

      if LENGTH(changed_from) > 0 then
         PROC_CREATE_AUDIT_TRAIL(13, 'UPDATE', :NEW.UPDATED_BY, to_char(:NEW.EP_ID), changed_from, changed_to);
      end if;
   elsif deleting then
      PROC_CREATE_AUDIT_TRAIL(13, 'DELETE', :OLD.UPDATED_BY, to_char(:OLD.EP_ID), changed_from, changed_to);
   end if;
end;
/

PROMPT CREATING TGR_ES_AUDIT_TRAIL
create or replace trigger TGR_ES_AUDIT_TRAIL
   after update or delete on EXAMINER_SPECIALIZATIONS for each row

declare
   changed_from	varchar2(2000);
   changed_to	varchar2(2000);

   procedure APPEND_TO_CHANGED_FROM(old_value varchar2) is
   begin
      if LENGTH(changed_from) = 0 then
         changed_from := '<?xml version="1.0" encoding="ISO-8859-1"?>';
      end if;
      changed_from := changed_from || old_value;
   end;

   procedure APPEND_TO_CHANGED_TO(new_value varchar2) is
   begin
      if LENGTH(changed_to) = 0 then
         changed_to := '<?xml version="1.0" encoding="ISO-8859-1"?>';
      end if;
      changed_to := changed_to || new_value;
   end;

begin
   if updating then
      if nvl(:OLD.EXMNR_ID, 0) != nvl(:NEW.EXMNR_ID, 0) then
         APPEND_TO_CHANGED_FROM('<ExaminerID>' || to_char(:OLD.EXMNR_ID) || '</ExaminerID>');
         APPEND_TO_CHANGED_TO('<ExaminerID>' || to_char(:NEW.EXMNR_ID) || '</ExaminerID>');
      end if;
      if nvl(:OLD.SPL_ID, 0) != nvl(:NEW.SPL_ID, 0) then
         APPEND_TO_CHANGED_FROM('<SpecializationID>' || to_char(:OLD.SPL_ID) || '</SpecializationID>');
         APPEND_TO_CHANGED_TO('<SpecializationID>' || to_char(:NEW.SPL_ID) || '</SpecializationID>');
      end if;
      if nvl(:OLD.SPL_EFFECTIVE_DATE, '') != nvl(:NEW.SPL_EFFECTIVE_DATE, '') then
         APPEND_TO_CHANGED_FROM('<SpecializationEffectiveDate>' || to_char(:OLD.SPL_EFFECTIVE_DATE) || '</SpecializationEffectiveDate>');
         APPEND_TO_CHANGED_TO('<SpecializationEffectiveDate>' || to_char(:NEW.SPL_EFFECTIVE_DATE) || '</SpecializationEffectiveDate>');
      end if;
      if nvl(:OLD.CREATED_BY, '') != nvl(:NEW.CREATED_BY, '') then
         APPEND_TO_CHANGED_FROM('<CreatedBy>' || :OLD.CREATED_BY || '</CreatedBy>');
         APPEND_TO_CHANGED_TO('<CreatedBy>' || :NEW.CREATED_BY || '</CreatedBy>');
      end if;
      if nvl(:OLD.CREATED_DATE, '') != nvl(:NEW.CREATED_DATE, '') then
         APPEND_TO_CHANGED_FROM('<CreatedDate>' || to_char(:OLD.CREATED_DATE) || '</CreatedDate>');
         APPEND_TO_CHANGED_TO('<CreatedDate>' || to_char(:NEW.CREATED_DATE) || '</CreatedDate>');
      end if;
      if nvl(:OLD.UPDATED_BY, '') != nvl(:NEW.UPDATED_BY, '') then
         APPEND_TO_CHANGED_FROM('<UpdatedBy>' || :OLD.UPDATED_BY || '</UpdatedBy>');
         APPEND_TO_CHANGED_TO('<UpdatedBy>' || :NEW.UPDATED_BY || '</UpdatedBy>');
      end if;
      if nvl(:OLD.UPDATED_DATE, '') != nvl(:NEW.UPDATED_DATE, '') then
         APPEND_TO_CHANGED_FROM('<UpdatedDate>' || to_char(:OLD.UPDATED_DATE) || '</UpdatedDate>');
         APPEND_TO_CHANGED_TO('<UpdatedDate>' || to_char(:NEW.UPDATED_DATE) || '</UpdatedDate>');
      end if;

      if LENGTH(changed_from) > 0 then
         PROC_CREATE_AUDIT_TRAIL(44, 'UPDATE', :NEW.UPDATED_BY, to_char(:NEW.EXMNR_ID) || '-' || to_char(:NEW.SPL_ID), changed_from, changed_to);
      end if;
   elsif deleting then
      PROC_CREATE_AUDIT_TRAIL(44, 'DELETE', :OLD.UPDATED_BY, to_char(:OLD.EXMNR_ID) || '-' || to_char(:OLD.SPL_ID), changed_from, changed_to);
   end if;
end;
/

PROMPT CREATING TGR_EXMNR_AUDIT_TRAIL
create or replace trigger TGR_EXMNR_AUDIT_TRAIL
   after update or delete on EXAMINERS for each row

declare
   changed_from	varchar2(2000);
   changed_to	varchar2(2000);

   procedure APPEND_TO_CHANGED_FROM(old_value varchar2) is
   begin
      if LENGTH(changed_from) = 0 then
         changed_from := '<?xml version="1.0" encoding="ISO-8859-1"?>';
      end if;
      changed_from := changed_from || old_value;
   end;

   procedure APPEND_TO_CHANGED_TO(new_value varchar2) is
   begin
      if LENGTH(changed_to) = 0 then
         changed_to := '<?xml version="1.0" encoding="ISO-8859-1"?>';
      end if;
      changed_to := changed_to || new_value;
   end;

begin
   if updating then
      if nvl(:OLD.EXMNR_ID, 0) != nvl(:NEW.EXMNR_ID, 0) then
         APPEND_TO_CHANGED_FROM('<ExaminerID>' || to_char(:OLD.EXMNR_ID) || '</ExaminerID>');
         APPEND_TO_CHANGED_TO('<ExaminerID>' || to_char(:NEW.EXMNR_ID) || '</ExaminerID>');
      end if;
      if nvl(:OLD.CREATED_BY, '') != nvl(:NEW.CREATED_BY, '') then
         APPEND_TO_CHANGED_FROM('<CreatedBy>' || :OLD.CREATED_BY || '</CreatedBy>');
         APPEND_TO_CHANGED_TO('<CreatedBy>' || :NEW.CREATED_BY || '</CreatedBy>');
      end if;
      if nvl(:OLD.CREATED_DATE, '') != nvl(:NEW.CREATED_DATE, '') then
         APPEND_TO_CHANGED_FROM('<CreatedDate>' || to_char(:OLD.CREATED_DATE) || '</CreatedDate>');
         APPEND_TO_CHANGED_TO('<CreatedDate>' || to_char(:NEW.CREATED_DATE) || '</CreatedDate>');
      end if;
      if nvl(:OLD.UPDATED_BY, '') != nvl(:NEW.UPDATED_BY, '') then
         APPEND_TO_CHANGED_FROM('<UpdatedBy>' || :OLD.UPDATED_BY || '</UpdatedBy>');
         APPEND_TO_CHANGED_TO('<UpdatedBy>' || :NEW.UPDATED_BY || '</UpdatedBy>');
      end if;
      if nvl(:OLD.UPDATED_DATE, '') != nvl(:NEW.UPDATED_DATE, '') then
         APPEND_TO_CHANGED_FROM('<UpdatedDate>' || to_char(:OLD.UPDATED_DATE) || '</UpdatedDate>');
         APPEND_TO_CHANGED_TO('<UpdatedDate>' || to_char(:NEW.UPDATED_DATE) || '</UpdatedDate>');
      end if;

      if LENGTH(changed_from) > 0 then
         PROC_CREATE_AUDIT_TRAIL(22, 'UPDATE', :NEW.UPDATED_BY, to_char(:NEW.EXMNR_ID), changed_from, changed_to);
      end if;
   elsif deleting then
      PROC_CREATE_AUDIT_TRAIL(22, 'DELETE', :OLD.UPDATED_BY, to_char(:OLD.EXMNR_ID), changed_from, changed_to);
   end if;
end;
/

PROMPT CREATING TGR_HOL_AUDIT_TRAIL
create or replace trigger TGR_HOL_AUDIT_TRAIL
   after update or delete on HOLIDAYS for each row

declare
   changed_from	varchar2(2000);
   changed_to	varchar2(2000);

   procedure APPEND_TO_CHANGED_FROM(old_value varchar2) is
   begin
      if LENGTH(changed_from) = 0 then
         changed_from := '<?xml version="1.0" encoding="ISO-8859-1"?>';
      end if;
      changed_from := changed_from || old_value;
   end;

   procedure APPEND_TO_CHANGED_TO(new_value varchar2) is
   begin
      if LENGTH(changed_to) = 0 then
         changed_to := '<?xml version="1.0" encoding="ISO-8859-1"?>';
      end if;
      changed_to := changed_to || new_value;
   end;

begin
   if updating then
      if nvl(:OLD.HOL_ID, 0) != nvl(:NEW.HOL_ID, 0) then
         APPEND_TO_CHANGED_FROM('<HolidayCode>' || to_char(:OLD.HOL_ID) || '</HolidayCode>');
         APPEND_TO_CHANGED_TO('<HolidayCode>' || to_char(:NEW.HOL_ID) || '</HolidayCode>');
      end if;
      if nvl(:OLD.HOL_DATE, '') != nvl(:NEW.HOL_DATE, '') then
         APPEND_TO_CHANGED_FROM('<Date>' || to_char(:OLD.HOL_DATE) || '</Date>');
         APPEND_TO_CHANGED_TO('<Date>' || to_char(:NEW.HOL_DATE) || '</Date>');
      end if;
      if nvl(:OLD.HOL_DESC, '') != nvl(:NEW.HOL_DESC, '') then
         APPEND_TO_CHANGED_FROM('<Description>' || :OLD.HOL_DESC || '</Description>');
         APPEND_TO_CHANGED_TO('<Description>' || :NEW.HOL_DESC || '</Description>');
      end if;
      if nvl(:OLD.CREATED_BY, '') != nvl(:NEW.CREATED_BY, '') then
         APPEND_TO_CHANGED_FROM('<CreatedBy>' || :OLD.CREATED_BY || '</CreatedBy>');
         APPEND_TO_CHANGED_TO('<CreatedBy>' || :NEW.CREATED_BY || '</CreatedBy>');
      end if;
      if nvl(:OLD.CREATED_DATE, '') != nvl(:NEW.CREATED_DATE, '') then
         APPEND_TO_CHANGED_FROM('<CreatedDate>' || to_char(:OLD.CREATED_DATE) || '</CreatedDate>');
         APPEND_TO_CHANGED_TO('<CreatedDate>' || to_char(:NEW.CREATED_DATE) || '</CreatedDate>');
      end if;
      if nvl(:OLD.UPDATED_BY, '') != nvl(:NEW.UPDATED_BY, '') then
         APPEND_TO_CHANGED_FROM('<UpdatedBy>' || :OLD.UPDATED_BY || '</UpdatedBy>');
         APPEND_TO_CHANGED_TO('<UpdatedBy>' || :NEW.UPDATED_BY || '</UpdatedBy>');
      end if;
      if nvl(:OLD.UPDATED_DATE, '') != nvl(:NEW.UPDATED_DATE, '') then
         APPEND_TO_CHANGED_FROM('<UpdatedDate>' || to_char(:OLD.UPDATED_DATE) || '</UpdatedDate>');
         APPEND_TO_CHANGED_TO('<UpdatedDate>' || to_char(:NEW.UPDATED_DATE) || '</UpdatedDate>');
      end if;

      if LENGTH(changed_from) > 0 then
         PROC_CREATE_AUDIT_TRAIL(27, 'UPDATE', :NEW.UPDATED_BY, to_char(:NEW.HOL_ID), changed_from, changed_to);
      end if;
   elsif deleting then
      PROC_CREATE_AUDIT_TRAIL(27, 'DELETE', :OLD.UPDATED_BY, to_char(:OLD.HOL_ID), changed_from, changed_to);
   end if;
end;
/

PROMPT CREATING TGR_IMP_AUDIT_TRAIL 
create or replace trigger TGR_IMP_AUDIT_TRAIL 
   after update or delete on IMPAIRMENTS for each row

declare
   changed_from	varchar2(2000);
   changed_to	varchar2(2000);

   procedure APPEND_TO_CHANGED_FROM(old_value varchar2) is
   begin
      if LENGTH(changed_from) = 0 then
         changed_from := '<?xml version="1.0" encoding="ISO-8859-1"?>';
      end if;
      changed_from := changed_from || old_value;
   end;

   procedure APPEND_TO_CHANGED_TO(new_value varchar2) is
   begin
      if LENGTH(changed_to) = 0 then
         changed_to := '<?xml version="1.0" encoding="ISO-8859-1"?>';
      end if;
      changed_to := changed_to || new_value;
   end;

begin
   if updating then
      if nvl(:OLD.IMP_IMPAIREMENT_ID, 0) != nvl(:NEW.IMP_IMPAIREMENT_ID, 0) then
         APPEND_TO_CHANGED_FROM('<ImpairmentID>' || to_char(:OLD.IMP_IMPAIREMENT_ID) || '</ImpairmentID>');
         APPEND_TO_CHANGED_TO('<ImpairmentID>' || to_char(:NEW.IMP_IMPAIREMENT_ID) || '</ImpairmentID>');
      end if;
      if nvl(:OLD.MIB_IMPAIRMENT_CODE, '') != nvl(:NEW.MIB_IMPAIRMENT_CODE, '') then
         APPEND_TO_CHANGED_FROM('<ImpairmentCode>' || :OLD.MIB_IMPAIRMENT_CODE || '</ImpairmentCode>');
         APPEND_TO_CHANGED_TO('<ImpairmentCode>' || :NEW.MIB_IMPAIRMENT_CODE || '</ImpairmentCode>');
      end if;
      if nvl(:OLD.CL_CLIENT_NUM, '') != nvl(:NEW.CL_CLIENT_NUM, '') then
         APPEND_TO_CHANGED_FROM('<ClientNumber>' || :OLD.CL_CLIENT_NUM || '</ClientNumber>');
         APPEND_TO_CHANGED_TO('<ClientNumber>' || :NEW.CL_CLIENT_NUM || '</ClientNumber>');
      end if;
      if nvl(:OLD.UAR_REFERENCE_NUM, '') != nvl(:NEW.UAR_REFERENCE_NUM, '') then
         APPEND_TO_CHANGED_FROM('<ReferenceNumber>' || :OLD.UAR_REFERENCE_NUM || '</ReferenceNumber>');
         APPEND_TO_CHANGED_TO('<ReferenceNumber>' || :NEW.UAR_REFERENCE_NUM || '</ReferenceNumber>');
      end if;
      if nvl(:OLD.IMP_EXPORT_DATE, '') != nvl(:NEW.IMP_EXPORT_DATE, '') then
         APPEND_TO_CHANGED_FROM('<ExportDate>' || to_char(:OLD.IMP_EXPORT_DATE) || '</ExportDate>');
         APPEND_TO_CHANGED_TO('<ExportDate>' || to_char(:NEW.IMP_EXPORT_DATE) || '</ExportDate>');
      end if;
      if nvl(:OLD.CREATED_BY, '') != nvl(:NEW.CREATED_BY, '') then
         APPEND_TO_CHANGED_FROM('<CreatedBy>' || :OLD.CREATED_BY || '</CreatedBy>');
         APPEND_TO_CHANGED_TO('<CreatedBy>' || :NEW.CREATED_BY || '</CreatedBy>');
      end if;
      if nvl(:OLD.CREATED_DATE, '') != nvl(:NEW.CREATED_DATE, '') then
         APPEND_TO_CHANGED_FROM('<CreatedDate>' || to_char(:OLD.CREATED_DATE) || '</CreatedDate>');
         APPEND_TO_CHANGED_TO('<CreatedDate>' || to_char(:NEW.CREATED_DATE) || '</CreatedDate>');
      end if;
      if nvl(:OLD.UPDATED_BY, '') != nvl(:NEW.UPDATED_BY, '') then
         APPEND_TO_CHANGED_FROM('<UpdatedBy>' || :OLD.UPDATED_BY || '</UpdatedBy>');
         APPEND_TO_CHANGED_TO('<UpdatedBy>' || :NEW.UPDATED_BY || '</UpdatedBy>');
      end if;
      if nvl(:OLD.UPDATED_DATE, '') != nvl(:NEW.UPDATED_DATE, '') then
         APPEND_TO_CHANGED_FROM('<UpdatedDate>' || to_char(:OLD.UPDATED_DATE) || '</UpdatedDate>');
         APPEND_TO_CHANGED_TO('<UpdatedDate>' || to_char(:NEW.UPDATED_DATE) || '</UpdatedDate>');
      end if;

      if LENGTH(changed_from) > 0 then
         PROC_CREATE_AUDIT_TRAIL(3, 'UPDATE', :NEW.UPDATED_BY, to_char(:NEW.IMP_IMPAIREMENT_ID), changed_from, changed_to);
      end if;
   elsif deleting then
      PROC_CREATE_AUDIT_TRAIL(3, 'DELETE', :OLD.UPDATED_BY, to_char(:OLD.IMP_IMPAIREMENT_ID), changed_from, changed_to);
   end if;
end;
/

PROMPT CREATING TGR_INSERT_USER_ROLES
create or replace trigger TGR_INSERT_USER_ROLES after
	insert on USER_ROLES for each row

declare
	cursor CURSOR_EVENT_ID (roleCode in VARCHAR2) is
		select evnt_id from process_configuration_roles where role_code = roleCode;
	V_EXISTING NUMBER;
	V_EVENT_ID NUMBER;
	
begin
	if inserting then
		open CURSOR_EVENT_ID (:new.ROLE_CODE);
		loop
			fetch CURSOR_EVENT_ID into V_EVENT_ID;
			exit when CURSOR_EVENT_ID%notfound;
			begin
				select count(*) into V_EXISTING from user_process_preferences where user_id = :new.USER_ID and evnt_id = V_EVENT_ID;
				if (V_EXISTING = 0) then
					insert into USER_PROCESS_PREFERENCES (USER_ID, EVNT_ID, UPP_NOTIFICATION_IND) values (:new.USER_ID, V_EVENT_ID, '1');
				end if;
			end; 
		end loop;
		close CURSOR_EVENT_ID;
	end if;
end;
/

PROMPT CREATING TGR_LAB_AUDIT_TRAIL
create or replace trigger TGR_LAB_AUDIT_TRAIL
   after update or delete on LABORATORIES for each row

declare
   changed_from	varchar2(2000);
   changed_to	varchar2(2000);

   procedure APPEND_TO_CHANGED_FROM(old_value varchar2) is
   begin
      if LENGTH(changed_from) = 0 then
         changed_from := '<?xml version="1.0" encoding="ISO-8859-1"?>';
      end if;
      changed_from := changed_from || old_value;
   end;

   procedure APPEND_TO_CHANGED_TO(new_value varchar2) is
   begin
      if LENGTH(changed_to) = 0 then
         changed_to := '<?xml version="1.0" encoding="ISO-8859-1"?>';
      end if;
      changed_to := changed_to || new_value;
   end;

begin
   if updating then
      if nvl(:OLD.LAB_ID, 0) != nvl(:NEW.LAB_ID, 0) then
         APPEND_TO_CHANGED_FROM('<LaboratoryID>' || to_char(:OLD.LAB_ID) || '</LaboratoryID>');
         APPEND_TO_CHANGED_TO('<LaboratoryID>' || to_char(:NEW.LAB_ID) || '</LaboratoryID>');
      end if;
      if nvl(:OLD.CREATED_BY, '') != nvl(:NEW.CREATED_BY, '') then
         APPEND_TO_CHANGED_FROM('<CreatedBy>' || :OLD.CREATED_BY || '</CreatedBy>');
         APPEND_TO_CHANGED_TO('<CreatedBy>' || :NEW.CREATED_BY || '</CreatedBy>');
      end if;
      if nvl(:OLD.CREATED_DATE, '') != nvl(:NEW.CREATED_DATE, '') then
         APPEND_TO_CHANGED_FROM('<CreatedDate>' || to_char(:OLD.CREATED_DATE) || '</CreatedDate>');
         APPEND_TO_CHANGED_TO('<CreatedDate>' || to_char(:NEW.CREATED_DATE) || '</CreatedDate>');
      end if;
      if nvl(:OLD.UPDATED_BY, '') != nvl(:NEW.UPDATED_BY, '') then
         APPEND_TO_CHANGED_FROM('<UpdatedBy>' || :OLD.UPDATED_BY || '</UpdatedBy>');
         APPEND_TO_CHANGED_TO('<UpdatedBy>' || :NEW.UPDATED_BY || '</UpdatedBy>');
      end if;
      if nvl(:OLD.UPDATED_DATE, '') != nvl(:NEW.UPDATED_DATE, '') then
         APPEND_TO_CHANGED_FROM('<UpdatedDate>' || to_char(:OLD.UPDATED_DATE) || '</UpdatedDate>');
         APPEND_TO_CHANGED_TO('<UpdatedDate>' || to_char(:NEW.UPDATED_DATE) || '</UpdatedDate>');
      end if;

      if LENGTH(changed_from) > 0 then
         PROC_CREATE_AUDIT_TRAIL(20, 'UPDATE', :NEW.UPDATED_BY, to_char(:NEW.LAB_ID), changed_from, changed_to);
      end if;
   elsif deleting then
      PROC_CREATE_AUDIT_TRAIL(20, 'DELETE', :OLD.UPDATED_BY, to_char(:OLD.LAB_ID), changed_from, changed_to);
   end if;
end;
/

PROMPT CREATING TGR_LOB_AUDIT_TRAIL
create or replace trigger TGR_LOB_AUDIT_TRAIL
   after update or delete on LINES_OF_BUSINESS for each row

declare
   changed_from	varchar2(2000);
   changed_to	varchar2(2000);

   procedure APPEND_TO_CHANGED_FROM(old_value varchar2) is
   begin
      if LENGTH(changed_from) = 0 then
         changed_from := '<?xml version="1.0" encoding="ISO-8859-1"?>';
      end if;
      changed_from := changed_from || old_value;
   end;

   procedure APPEND_TO_CHANGED_TO(new_value varchar2) is
   begin
      if LENGTH(changed_to) = 0 then
         changed_to := '<?xml version="1.0" encoding="ISO-8859-1"?>';
      end if;
      changed_to := changed_to || new_value;
   end;

begin
   if updating then
      if nvl(:OLD.LOB_CODE, '') != nvl(:NEW.LOB_CODE, '') then
         APPEND_TO_CHANGED_FROM('<Code>' || :OLD.LOB_CODE || '</Code>');
         APPEND_TO_CHANGED_TO('<Code>' || :NEW.LOB_CODE || '</Code>');
      end if;
      if nvl(:OLD.LOB_DESC, '') != nvl(:NEW.LOB_DESC, '') then
         APPEND_TO_CHANGED_FROM('<Description>' || :OLD.LOB_DESC || '</Description>');
         APPEND_TO_CHANGED_TO('<Description>' || :NEW.LOB_DESC || '</Description>');
      end if;
      if nvl(:OLD.CREATED_BY, '') != nvl(:NEW.CREATED_BY, '') then
         APPEND_TO_CHANGED_FROM('<CreatedBy>' || :OLD.CREATED_BY || '</CreatedBy>');
         APPEND_TO_CHANGED_TO('<CreatedBy>' || :NEW.CREATED_BY || '</CreatedBy>');
      end if;
      if nvl(:OLD.CREATED_DATE, '') != nvl(:NEW.CREATED_DATE, '') then
         APPEND_TO_CHANGED_FROM('<CreatedDate>' || to_char(:OLD.CREATED_DATE) || '</CreatedDate>');
         APPEND_TO_CHANGED_TO('<CreatedDate>' || to_char(:NEW.CREATED_DATE) || '</CreatedDate>');
      end if;
      if nvl(:OLD.UPDATED_BY, '') != nvl(:NEW.UPDATED_BY, '') then
         APPEND_TO_CHANGED_FROM('<UpdatedBy>' || :OLD.UPDATED_BY || '</UpdatedBy>');
         APPEND_TO_CHANGED_TO('<UpdatedBy>' || :NEW.UPDATED_BY || '</UpdatedBy>');
      end if;
      if nvl(:OLD.UPDATED_DATE, '') != nvl(:NEW.UPDATED_DATE, '') then
         APPEND_TO_CHANGED_FROM('<UpdatedDate>' || to_char(:OLD.UPDATED_DATE) || '</UpdatedDate>');
         APPEND_TO_CHANGED_TO('<UpdatedDate>' || to_char(:NEW.UPDATED_DATE) || '</UpdatedDate>');
      end if;

      if LENGTH(changed_from) > 0 then
         PROC_CREATE_AUDIT_TRAIL(9, 'UPDATE', :NEW.UPDATED_BY, :NEW.LOB_CODE, changed_from, changed_to);
      end if;
   elsif deleting then
      PROC_CREATE_AUDIT_TRAIL(9, 'DELETE', :OLD.UPDATED_BY, :OLD.LOB_CODE, changed_from, changed_to);
   end if;
end;
/

PROMPT CREATING TGR_LOB_DOC_AUDIT_TRAIL
create or replace trigger TGR_LOB_DOC_AUDIT_TRAIL
   after update or delete on LOB_DOCUMENTS for each row

declare
   changed_from	varchar2(2000);
   changed_to	varchar2(2000);

   procedure APPEND_TO_CHANGED_FROM(old_value varchar2) is
   begin
      if LENGTH(changed_from) = 0 then
         changed_from := '<?xml version="1.0" encoding="ISO-8859-1"?>';
      end if;
      changed_from := changed_from || old_value;
   end;

   procedure APPEND_TO_CHANGED_TO(new_value varchar2) is
   begin
      if LENGTH(changed_to) = 0 then
         changed_to := '<?xml version="1.0" encoding="ISO-8859-1"?>';
      end if;
      changed_to := changed_to || new_value;
   end;

begin
   if updating then
      if nvl(:OLD.LOB_CODE, '') != nvl(:NEW.LOB_CODE, '') then
         APPEND_TO_CHANGED_FROM('<LineOf Business>' || :OLD.LOB_CODE || '</LineOf Business>');
         APPEND_TO_CHANGED_TO('<LineOf Business>' || :NEW.LOB_CODE || '</LineOf Business>');
      end if;
      if nvl(:OLD.DOC_CODE, '') != nvl(:NEW.DOC_CODE, '') then
         APPEND_TO_CHANGED_FROM('<DocumentCode>' || :OLD.DOC_CODE || '</DocumentCode>');
         APPEND_TO_CHANGED_TO('<DocumentCode>' || :NEW.DOC_CODE || '</DocumentCode>');
      end if;
      if nvl(:OLD.CREATED_BY, '') != nvl(:NEW.CREATED_BY, '') then
         APPEND_TO_CHANGED_FROM('<CreatedBy>' || :OLD.CREATED_BY || '</CreatedBy>');
         APPEND_TO_CHANGED_TO('<CreatedBy>' || :NEW.CREATED_BY || '</CreatedBy>');
      end if;
      if nvl(:OLD.CREATED_DATE, '') != nvl(:NEW.CREATED_DATE, '') then
         APPEND_TO_CHANGED_FROM('<CreatedDate>' || to_char(:OLD.CREATED_DATE) || '</CreatedDate>');
         APPEND_TO_CHANGED_TO('<CreatedDate>' || to_char(:NEW.CREATED_DATE) || '</CreatedDate>');
      end if;
      if nvl(:OLD.UPDATED_BY, '') != nvl(:NEW.UPDATED_BY, '') then
         APPEND_TO_CHANGED_FROM('<UpdatedBy>' || :OLD.UPDATED_BY || '</UpdatedBy>');
         APPEND_TO_CHANGED_TO('<UpdatedBy>' || :NEW.UPDATED_BY || '</UpdatedBy>');
      end if;
      if nvl(:OLD.UPDATED_DATE, '') != nvl(:NEW.UPDATED_DATE, '') then
         APPEND_TO_CHANGED_FROM('<UpdatedDate>' || to_char(:OLD.UPDATED_DATE) || '</UpdatedDate>');
         APPEND_TO_CHANGED_TO('<UpdatedDate>' || to_char(:NEW.UPDATED_DATE) || '</UpdatedDate>');
      end if;

      if LENGTH(changed_from) > 0 then
         PROC_CREATE_AUDIT_TRAIL(44, 'UPDATE', :NEW.UPDATED_BY, :NEW.LOB_CODE || '-' || :NEW.DOC_CODE, changed_from, changed_to);
      end if;
   elsif deleting then
      PROC_CREATE_AUDIT_TRAIL(44, 'DELETE', :OLD.UPDATED_BY, :OLD.LOB_CODE || '-' || :OLD.DOC_CODE, changed_from, changed_to);
   end if;
end;
/

PROMPT CREATING TGR_LOB_REQT_AUDIT_TRAIL
create or replace trigger TGR_LOB_REQT_AUDIT_TRAIL
   after update or delete on LOB_REQUIREMENTS for each row

declare
   changed_from	varchar2(2000);
   changed_to	varchar2(2000);

   procedure APPEND_TO_CHANGED_FROM(old_value varchar2) is
   begin
      if LENGTH(changed_from) = 0 then
         changed_from := '<?xml version="1.0" encoding="ISO-8859-1"?>';
      end if;
      changed_from := changed_from || old_value;
   end;

   procedure APPEND_TO_CHANGED_TO(new_value varchar2) is
   begin
      if LENGTH(changed_to) = 0 then
         changed_to := '<?xml version="1.0" encoding="ISO-8859-1"?>';
      end if;
      changed_to := changed_to || new_value;
   end;

begin
   if updating then
      if nvl(:OLD.LOB_CODE, '') != nvl(:NEW.LOB_CODE, '') then
         APPEND_TO_CHANGED_FROM('<LineOf Business>' || :OLD.LOB_CODE || '</LineOf Business>');
         APPEND_TO_CHANGED_TO('<LineOf Business>' || :NEW.LOB_CODE || '</LineOf Business>');
      end if;
      if nvl(:OLD.REQT_CODE, '') != nvl(:NEW.REQT_CODE, '') then
         APPEND_TO_CHANGED_FROM('<RequirementCode>' || :OLD.REQT_CODE || '</RequirementCode>');
         APPEND_TO_CHANGED_TO('<RequirementCode>' || :NEW.REQT_CODE || '</RequirementCode>');
      end if;
      if nvl(:OLD.CREATED_BY, '') != nvl(:NEW.CREATED_BY, '') then
         APPEND_TO_CHANGED_FROM('<CreatedBy>' || :OLD.CREATED_BY || '</CreatedBy>');
         APPEND_TO_CHANGED_TO('<CreatedBy>' || :NEW.CREATED_BY || '</CreatedBy>');
      end if;
      if nvl(:OLD.CREATED_DATE, '') != nvl(:NEW.CREATED_DATE, '') then
         APPEND_TO_CHANGED_FROM('<CreatedDate>' || to_char(:OLD.CREATED_DATE) || '</CreatedDate>');
         APPEND_TO_CHANGED_TO('<CreatedDate>' || to_char(:NEW.CREATED_DATE) || '</CreatedDate>');
      end if;
      if nvl(:OLD.UPDATED_BY, '') != nvl(:NEW.UPDATED_BY, '') then
         APPEND_TO_CHANGED_FROM('<UpdatedBy>' || :OLD.UPDATED_BY || '</UpdatedBy>');
         APPEND_TO_CHANGED_TO('<UpdatedBy>' || :NEW.UPDATED_BY || '</UpdatedBy>');
      end if;
      if nvl(:OLD.UPDATED_DATE, '') != nvl(:NEW.UPDATED_DATE, '') then
         APPEND_TO_CHANGED_FROM('<UpdatedDate>' || to_char(:OLD.UPDATED_DATE) || '</UpdatedDate>');
         APPEND_TO_CHANGED_TO('<UpdatedDate>' || to_char(:NEW.UPDATED_DATE) || '</UpdatedDate>');
      end if;

      if LENGTH(changed_from) > 0 then
         PROC_CREATE_AUDIT_TRAIL(44, 'UPDATE', :NEW.UPDATED_BY, :NEW.LOB_CODE || '-' || :NEW.REQT_CODE, changed_from, changed_to);
      end if;
   elsif deleting then
      PROC_CREATE_AUDIT_TRAIL(44, 'DELETE', :OLD.UPDATED_BY, :OLD.LOB_CODE || '-' || :OLD.REQT_CODE, changed_from, changed_to);
   end if;
end;
/

PROMPT CREATING TGR_LOB_STAT_AUDIT_TRAIL
create or replace trigger TGR_LOB_STAT_AUDIT_TRAIL
   after update or delete on LOB_STATUS for each row

declare
   changed_from	varchar2(2000);
   changed_to	varchar2(2000);

   procedure APPEND_TO_CHANGED_FROM(old_value varchar2) is
   begin
      if LENGTH(changed_from) = 0 then
         changed_from := '<?xml version="1.0" encoding="ISO-8859-1"?>';
      end if;
      changed_from := changed_from || old_value;
   end;

   procedure APPEND_TO_CHANGED_TO(new_value varchar2) is
   begin
      if LENGTH(changed_to) = 0 then
         changed_to := '<?xml version="1.0" encoding="ISO-8859-1"?>';
      end if;
      changed_to := changed_to || new_value;
   end;

begin
   if updating then
      if nvl(:OLD.LOB_CODE, '') != nvl(:NEW.LOB_CODE, '') then
         APPEND_TO_CHANGED_FROM('<LineOf Business>' || :OLD.LOB_CODE || '</LineOf Business>');
         APPEND_TO_CHANGED_TO('<LineOf Business>' || :NEW.LOB_CODE || '</LineOf Business>');
      end if;
      if nvl(:OLD.STAT_ID, 0) != nvl(:NEW.STAT_ID, 0) then
         APPEND_TO_CHANGED_FROM('<StatusID>' || to_char(:OLD.STAT_ID) || '</StatusID>');
         APPEND_TO_CHANGED_TO('<StatusID>' || to_char(:NEW.STAT_ID) || '</StatusID>');
      end if;
      if nvl(:OLD.CREATED_BY, '') != nvl(:NEW.CREATED_BY, '') then
         APPEND_TO_CHANGED_FROM('<CreatedBy>' || :OLD.CREATED_BY || '</CreatedBy>');
         APPEND_TO_CHANGED_TO('<CreatedBy>' || :NEW.CREATED_BY || '</CreatedBy>');
      end if;
      if nvl(:OLD.CREATED_DATE, '') != nvl(:NEW.CREATED_DATE, '') then
         APPEND_TO_CHANGED_FROM('<CreatedDate>' || to_char(:OLD.CREATED_DATE) || '</CreatedDate>');
         APPEND_TO_CHANGED_TO('<CreatedDate>' || to_char(:NEW.CREATED_DATE) || '</CreatedDate>');
      end if;
      if nvl(:OLD.UPDATED_BY, '') != nvl(:NEW.UPDATED_BY, '') then
         APPEND_TO_CHANGED_FROM('<UpdatedBy>' || :OLD.UPDATED_BY || '</UpdatedBy>');
         APPEND_TO_CHANGED_TO('<UpdatedBy>' || :NEW.UPDATED_BY || '</UpdatedBy>');
      end if;
      if nvl(:OLD.UPDATED_DATE, '') != nvl(:NEW.UPDATED_DATE, '') then
         APPEND_TO_CHANGED_FROM('<UpdatedDate>' || to_char(:OLD.UPDATED_DATE) || '</UpdatedDate>');
         APPEND_TO_CHANGED_TO('<UpdatedDate>' || to_char(:NEW.UPDATED_DATE) || '</UpdatedDate>');
      end if;

      if LENGTH(changed_from) > 0 then
         PROC_CREATE_AUDIT_TRAIL(44, 'UPDATE', :NEW.UPDATED_BY, :NEW.LOB_CODE || '-' || to_char(:NEW.STAT_ID), changed_from, changed_to);
      end if;
   elsif deleting then
      PROC_CREATE_AUDIT_TRAIL(44, 'DELETE', :OLD.UPDATED_BY, :OLD.LOB_CODE || '-' || to_char(:OLD.STAT_ID), changed_from, changed_to);
   end if;
end;
/

PROMPT CREATING TGR_LT_AUDIT_TRAIL
create or replace trigger TGR_LT_AUDIT_TRAIL
   after update or delete on LABORATORY_TESTS for each row

declare
   changed_from	varchar2(2000);
   changed_to	varchar2(2000);

   procedure APPEND_TO_CHANGED_FROM(old_value varchar2) is
   begin
      if LENGTH(changed_from) = 0 then
         changed_from := '<?xml version="1.0" encoding="ISO-8859-1"?>';
      end if;
      changed_from := changed_from || old_value;
   end;

   procedure APPEND_TO_CHANGED_TO(new_value varchar2) is
   begin
      if LENGTH(changed_to) = 0 then
         changed_to := '<?xml version="1.0" encoding="ISO-8859-1"?>';
      end if;
      changed_to := changed_to || new_value;
   end;

begin
   if updating then
      if nvl(:OLD.LAB_ID, 0) != nvl(:NEW.LAB_ID, 0) then
         APPEND_TO_CHANGED_FROM('<Laboratory>' || to_char(:OLD.LAB_ID) || '</Laboratory>');
         APPEND_TO_CHANGED_TO('<Laboratory>' || to_char(:NEW.LAB_ID) || '</Laboratory>');
      end if;
      if nvl(:OLD.TEST_ID, 0) != nvl(:NEW.TEST_ID, 0) then
         APPEND_TO_CHANGED_FROM('<TestCode>' || to_char(:OLD.TEST_ID) || '</TestCode>');
         APPEND_TO_CHANGED_TO('<TestCode>' || to_char(:NEW.TEST_ID) || '</TestCode>');
      end if;
      if nvl(:OLD.LAB_TEST_STATUS, '') != nvl(:NEW.LAB_TEST_STATUS, '') then
         APPEND_TO_CHANGED_FROM('<Status>' || :OLD.LAB_TEST_STATUS || '</Status>');
         APPEND_TO_CHANGED_TO('<Status>' || :NEW.LAB_TEST_STATUS || '</Status>');
      end if;

      if LENGTH(changed_from) > 0 then
         PROC_CREATE_AUDIT_TRAIL(17, 'UPDATE', :NEW.UPDATED_BY, to_char(:NEW.LAB_ID) || '-' || to_char(:NEW.TEST_ID), changed_from, changed_to);
      end if;
   elsif deleting then
      PROC_CREATE_AUDIT_TRAIL(17, 'DELETE', :OLD.UPDATED_BY, to_char(:OLD.LAB_ID) || '-' || to_char(:OLD.TEST_ID), changed_from, changed_to);
   end if;
end;
/

PROMPT CREATING TGR_MB_AUDIT_TRAIL 
create or replace trigger TGR_MB_AUDIT_TRAIL 
   after update on MEDICAL_BILLS for each row

declare
   changed_from	varchar2(2000);
   changed_to	varchar2(2000);

   procedure APPEND_TO_CHANGED_FROM(old_value varchar2) is
   begin
      if LENGTH(changed_from) = 0 then
         changed_from := '<?xml version="1.0" encoding="ISO-8859-1"?>';
      end if;
      changed_from := changed_from || old_value;
   end;

   procedure APPEND_TO_CHANGED_TO(new_value varchar2) is
   begin
      if LENGTH(changed_to) = 0 then
         changed_to := '<?xml version="1.0" encoding="ISO-8859-1"?>';
      end if;
      changed_to := changed_to || new_value;
   end;

begin
      if nvl(:OLD.MB_BILL_NUMBER, 0) != nvl(:NEW.MB_BILL_NUMBER, 0) then
         APPEND_TO_CHANGED_FROM('<BillNumber>' || to_char(:OLD.MB_BILL_NUMBER) || '</BillNumber>');
         APPEND_TO_CHANGED_TO('<BillNumber>' || to_char(:NEW.MB_BILL_NUMBER) || '</BillNumber>');
      end if;
      if nvl(:OLD.UAR_REFERENCE_NUM, '') != nvl(:NEW.UAR_REFERENCE_NUM, '') then
         APPEND_TO_CHANGED_FROM('<ReferenceNumber>' || :OLD.UAR_REFERENCE_NUM || '</ReferenceNumber>');
         APPEND_TO_CHANGED_TO('<ReferenceNumber>' || :NEW.UAR_REFERENCE_NUM || '</ReferenceNumber>');
      end if;
      if nvl(:OLD.EXMNR_ID, 0) != nvl(:NEW.EXMNR_ID, 0) then
         APPEND_TO_CHANGED_FROM('<Examiner>' || to_char(:OLD.EXMNR_ID) || '</Examiner>');
         APPEND_TO_CHANGED_TO('<Examiner>' || to_char(:NEW.EXMNR_ID) || '</Examiner>');
      end if;
      if nvl(:OLD.LAB_ID, 0) != nvl(:NEW.LAB_ID, 0) then
         APPEND_TO_CHANGED_FROM('<Laboratory>' || to_char(:OLD.LAB_ID) || '</Laboratory>');
         APPEND_TO_CHANGED_TO('<Laboratory>' || to_char(:NEW.LAB_ID) || '</Laboratory>');
      end if;
      if nvl(:OLD.MB_FEE, 0) != nvl(:NEW.MB_FEE, 0) then
         APPEND_TO_CHANGED_FROM('<Fee>' || to_char(:OLD.MB_FEE) || '</Fee>');
         APPEND_TO_CHANGED_TO('<Fee>' || to_char(:NEW.MB_FEE) || '</Fee>');
      end if;
      if nvl(:OLD.MB_DATE_POSTED, '') != nvl(:NEW.MB_DATE_POSTED, '') then
         APPEND_TO_CHANGED_FROM('<DatePosted>' || to_char(:OLD.MB_DATE_POSTED) || '</DatePosted>');
         APPEND_TO_CHANGED_TO('<DatePosted>' || to_char(:NEW.MB_DATE_POSTED) || '</DatePosted>');
      end if;
      if nvl(:OLD.CREATED_BY, '') != nvl(:NEW.CREATED_BY, '') then
         APPEND_TO_CHANGED_FROM('<CreatedBy>' || :OLD.CREATED_BY || '</CreatedBy>');
         APPEND_TO_CHANGED_TO('<CreatedBy>' || :NEW.CREATED_BY || '</CreatedBy>');
      end if;
      if nvl(:OLD.CREATED_DATE, '') != nvl(:NEW.CREATED_DATE, '') then
         APPEND_TO_CHANGED_FROM('<CreatedDate>' || to_char(:OLD.CREATED_DATE) || '</CreatedDate>');
         APPEND_TO_CHANGED_TO('<CreatedDate>' || to_char(:NEW.CREATED_DATE) || '</CreatedDate>');
      end if;
      if nvl(:OLD.UPDATED_BY, '') != nvl(:NEW.UPDATED_BY, '') then
         APPEND_TO_CHANGED_FROM('<UpdatedBy>' || :OLD.UPDATED_BY || '</UpdatedBy>');
         APPEND_TO_CHANGED_TO('<UpdatedBy>' || :NEW.UPDATED_BY || '</UpdatedBy>');
      end if;
      if nvl(:OLD.UPDATED_DATE, '') != nvl(:NEW.UPDATED_DATE, '') then
         APPEND_TO_CHANGED_FROM('<UpdatedDate>' || to_char(:OLD.UPDATED_DATE) || '</UpdatedDate>');
         APPEND_TO_CHANGED_TO('<UpdatedDate>' || to_char(:NEW.UPDATED_DATE) || '</UpdatedDate>');
      end if;

      if LENGTH(changed_from) > 0 then
         PROC_CREATE_AUDIT_TRAIL(39, 'UPDATE', :NEW.UPDATED_BY, to_char(:NEW.MB_BILL_NUMBER), changed_from, changed_to);
      end if;
end;
/

PROMPT CREATING TGR_MIB_ACT_AUDIT_TRAIL
create or replace trigger TGR_MIB_ACT_AUDIT_TRAIL
   after update on MIB_ACTIONS for each row

declare
   changed_from	varchar2(2000);
   changed_to	varchar2(2000);

   procedure APPEND_TO_CHANGED_FROM(old_value varchar2) is
   begin
      if LENGTH(changed_from) = 0 then
         changed_from := '<?xml version="1.0" encoding="ISO-8859-1"?>';
      end if;
      changed_from := changed_from || old_value;
   end;

   procedure APPEND_TO_CHANGED_TO(new_value varchar2) is
   begin
      if LENGTH(changed_to) = 0 then
         changed_to := '<?xml version="1.0" encoding="ISO-8859-1"?>';
      end if;
      changed_to := changed_to || new_value;
   end;

begin
      if nvl(:OLD.MIB_ACT_CODE, '') != nvl(:NEW.MIB_ACT_CODE, '') then
         APPEND_TO_CHANGED_FROM('<Code>' || :OLD.MIB_ACT_CODE || '</Code>');
         APPEND_TO_CHANGED_TO('<Code>' || :NEW.MIB_ACT_CODE || '</Code>');
      end if;
      if nvl(:OLD.CREATED_BY, '') != nvl(:NEW.CREATED_BY, '') then
         APPEND_TO_CHANGED_FROM('<CreatedBy>' || :OLD.CREATED_BY || '</CreatedBy>');
         APPEND_TO_CHANGED_TO('<CreatedBy>' || :NEW.CREATED_BY || '</CreatedBy>');
      end if;
      if nvl(:OLD.CREATED_DATE, '') != nvl(:NEW.CREATED_DATE, '') then
         APPEND_TO_CHANGED_FROM('<CreatedDate>' || to_char(:OLD.CREATED_DATE) || '</CreatedDate>');
         APPEND_TO_CHANGED_TO('<CreatedDate>' || to_char(:NEW.CREATED_DATE) || '</CreatedDate>');
      end if;
      if nvl(:OLD.UPDATED_BY, '') != nvl(:NEW.UPDATED_BY, '') then
         APPEND_TO_CHANGED_FROM('<UpdatedBy>' || :OLD.UPDATED_BY || '</UpdatedBy>');
         APPEND_TO_CHANGED_TO('<UpdatedBy>' || :NEW.UPDATED_BY || '</UpdatedBy>');
      end if;
      if nvl(:OLD.UPDATED_DATE, '') != nvl(:NEW.UPDATED_DATE, '') then
         APPEND_TO_CHANGED_FROM('<UpdatedDate>' || to_char(:OLD.UPDATED_DATE) || '</UpdatedDate>');
         APPEND_TO_CHANGED_TO('<UpdatedDate>' || to_char(:NEW.UPDATED_DATE) || '</UpdatedDate>');
      end if;

      if LENGTH(changed_from) > 0 then
         PROC_CREATE_AUDIT_TRAIL(34, 'UPDATE', :NEW.UPDATED_BY, :NEW.MIB_ACT_CODE, changed_from, changed_to);
      end if;
end;
/


PROMPT CREATING TGR_MIB_IMP_AUDIT_TRAIL
create or replace trigger TGR_MIB_IMP_AUDIT_TRAIL
   after update on MIB_IMPAIRMENTS for each row

declare
   changed_from	varchar2(2000);
   changed_to	varchar2(2000);

   procedure APPEND_TO_CHANGED_FROM(old_value varchar2) is
   begin
      if LENGTH(changed_from) = 0 then
         changed_from := '<?xml version="1.0" encoding="ISO-8859-1"?>';
      end if;
      changed_from := changed_from || old_value;
   end;

   procedure APPEND_TO_CHANGED_TO(new_value varchar2) is
   begin
      if LENGTH(changed_to) = 0 then
         changed_to := '<?xml version="1.0" encoding="ISO-8859-1"?>';
      end if;
      changed_to := changed_to || new_value;
   end;

begin
      if nvl(:OLD.MIB_IMPAIRMENT_CODE, '') != nvl(:NEW.MIB_IMPAIRMENT_CODE, '') then
         APPEND_TO_CHANGED_FROM('<Code>' || :OLD.MIB_IMPAIRMENT_CODE || '</Code>');
         APPEND_TO_CHANGED_TO('<Code>' || :NEW.MIB_IMPAIRMENT_CODE || '</Code>');
      end if;
      if nvl(:OLD.CREATED_BY, '') != nvl(:NEW.CREATED_BY, '') then
         APPEND_TO_CHANGED_FROM('<CreatedBy>' || :OLD.CREATED_BY || '</CreatedBy>');
         APPEND_TO_CHANGED_TO('<CreatedBy>' || :NEW.CREATED_BY || '</CreatedBy>');
      end if;
      if nvl(:OLD.CREATED_DATE, '') != nvl(:NEW.CREATED_DATE, '') then
         APPEND_TO_CHANGED_FROM('<CreatedDate>' || to_char(:OLD.CREATED_DATE) || '</CreatedDate>');
         APPEND_TO_CHANGED_TO('<CreatedDate>' || to_char(:NEW.CREATED_DATE) || '</CreatedDate>');
      end if;
      if nvl(:OLD.UPDATED_BY, '') != nvl(:NEW.UPDATED_BY, '') then
         APPEND_TO_CHANGED_FROM('<UpdatedBy>' || :OLD.UPDATED_BY || '</UpdatedBy>');
         APPEND_TO_CHANGED_TO('<UpdatedBy>' || :NEW.UPDATED_BY || '</UpdatedBy>');
      end if;
      if nvl(:OLD.UPDATED_DATE, '') != nvl(:NEW.UPDATED_DATE, '') then
         APPEND_TO_CHANGED_FROM('<UpdatedDate>' || to_char(:OLD.UPDATED_DATE) || '</UpdatedDate>');
         APPEND_TO_CHANGED_TO('<UpdatedDate>' || to_char(:NEW.UPDATED_DATE) || '</UpdatedDate>');
      end if;

      if LENGTH(changed_from) > 0 then
         PROC_CREATE_AUDIT_TRAIL(33, 'UPDATE', :NEW.UPDATED_BY, :NEW.MIB_IMPAIRMENT_CODE, changed_from, changed_to);
      end if;
end;
/

PROMPT CREATING TGR_MIB_LTTR_AUDIT_TRAIL
create or replace trigger TGR_MIB_LTTR_AUDIT_TRAIL
   after update on MIB_LETTERS for each row

declare
   changed_from	varchar2(2000);
   changed_to	varchar2(2000);

   procedure APPEND_TO_CHANGED_FROM(old_value varchar2) is
   begin
      if LENGTH(changed_from) = 0 then
         changed_from := '<?xml version="1.0" encoding="ISO-8859-1"?>';
      end if;
      changed_from := changed_from || old_value;
   end;

   procedure APPEND_TO_CHANGED_TO(new_value varchar2) is
   begin
      if LENGTH(changed_to) = 0 then
         changed_to := '<?xml version="1.0" encoding="ISO-8859-1"?>';
      end if;
      changed_to := changed_to || new_value;
   end;

begin
      if nvl(:OLD.MIB_LTTR_CODE, '') != nvl(:NEW.MIB_LTTR_CODE, '') then
         APPEND_TO_CHANGED_FROM('<Code>' || :OLD.MIB_LTTR_CODE || '/<Code>');
         APPEND_TO_CHANGED_TO('<Code>' || :NEW.MIB_LTTR_CODE || '/<Code>');
      end if;
      if nvl(:OLD.CREATED_BY, '') != nvl(:NEW.CREATED_BY, '') then
         APPEND_TO_CHANGED_FROM('<CreatedBy>' || :OLD.CREATED_BY || '</CreatedBy>');
         APPEND_TO_CHANGED_TO('<CreatedBy>' || :NEW.CREATED_BY || '</CreatedBy>');
      end if;
      if nvl(:OLD.CREATED_DATE, '') != nvl(:NEW.CREATED_DATE, '') then
         APPEND_TO_CHANGED_FROM('<CreatedDate>' || to_char(:OLD.CREATED_DATE) || '</CreatedDate>');
         APPEND_TO_CHANGED_TO('<CreatedDate>' || to_char(:NEW.CREATED_DATE) || '</CreatedDate>');
      end if;
      if nvl(:OLD.UPDATED_BY, '') != nvl(:NEW.UPDATED_BY, '') then
         APPEND_TO_CHANGED_FROM('<UpdatedBy>' || :OLD.UPDATED_BY || '</UpdatedBy>');
         APPEND_TO_CHANGED_TO('<UpdatedBy>' || :NEW.UPDATED_BY || '</UpdatedBy>');
      end if;
      if nvl(:OLD.UPDATED_DATE, '') != nvl(:NEW.UPDATED_DATE, '') then
         APPEND_TO_CHANGED_FROM('<UpdatedDate>' || to_char(:OLD.UPDATED_DATE) || '</UpdatedDate>');
         APPEND_TO_CHANGED_TO('<UpdatedDate>' || to_char(:NEW.UPDATED_DATE) || '</UpdatedDate>');
      end if;

      if LENGTH(changed_from) > 0 then
         PROC_CREATE_AUDIT_TRAIL(36, 'UPDATE', :NEW.UPDATED_BY, :NEW.MIB_LTTR_CODE, changed_from, changed_to);
      end if;
end;
/

PROMPT CREATING TGR_MIB_NUM_AUDIT_TRAIL
create or replace trigger TGR_MIB_NUM_AUDIT_TRAIL
   after update on MIB_NUMBERS for each row

declare
   changed_from	varchar2(2000);
   changed_to	varchar2(2000);

   procedure APPEND_TO_CHANGED_FROM(old_value varchar2) is
   begin
      if LENGTH(changed_from) = 0 then
         changed_from := '<?xml version="1.0" encoding="ISO-8859-1"?>';
      end if;
      changed_from := changed_from || old_value;
   end;

   procedure APPEND_TO_CHANGED_TO(new_value varchar2) is
   begin
      if LENGTH(changed_to) = 0 then
         changed_to := '<?xml version="1.0" encoding="ISO-8859-1"?>';
      end if;
      changed_to := changed_to || new_value;
   end;

begin
      if nvl(:OLD.MIB_NUM_CODE, '') != nvl(:NEW.MIB_NUM_CODE, '') then
         APPEND_TO_CHANGED_FROM('<Code>' || :OLD.MIB_NUM_CODE || '</Code>');
         APPEND_TO_CHANGED_TO('<Code>' || :NEW.MIB_NUM_CODE || '</Code>');
      end if;
      if nvl(:OLD.CREATED_BY, '') != nvl(:NEW.CREATED_BY, '') then
         APPEND_TO_CHANGED_FROM('<CreatedBy>' || :OLD.CREATED_BY || '</CreatedBy>');
         APPEND_TO_CHANGED_TO('<CreatedBy>' || :NEW.CREATED_BY || '</CreatedBy>');
      end if;
      if nvl(:OLD.CREATED_DATE, '') != nvl(:NEW.CREATED_DATE, '') then
         APPEND_TO_CHANGED_FROM('<CreatedDate>' || to_char(:OLD.CREATED_DATE) || '</CreatedDate>');
         APPEND_TO_CHANGED_TO('<CreatedDate>' || to_char(:NEW.CREATED_DATE) || '</CreatedDate>');
      end if;
      if nvl(:OLD.UPDATED_BY, '') != nvl(:NEW.UPDATED_BY, '') then
         APPEND_TO_CHANGED_FROM('<UpdatedBy>' || :OLD.UPDATED_BY || '</UpdatedBy>');
         APPEND_TO_CHANGED_TO('<UpdatedBy>' || :NEW.UPDATED_BY || '</UpdatedBy>');
      end if;
      if nvl(:OLD.UPDATED_DATE, '') != nvl(:NEW.UPDATED_DATE, '') then
         APPEND_TO_CHANGED_FROM('<UpdatedDate>' || to_char(:OLD.UPDATED_DATE) || '</UpdatedDate>');
         APPEND_TO_CHANGED_TO('<UpdatedDate>' || to_char(:NEW.UPDATED_DATE) || '</UpdatedDate>');
      end if;

      if LENGTH(changed_from) > 0 then
         PROC_CREATE_AUDIT_TRAIL(35, 'UPDATE', :NEW.UPDATED_BY, :NEW.MIB_NUM_CODE, changed_from, changed_to);
      end if;
end;
/

PROMPT CREATING TGR_MR_AUDIT_TRAIL 
create or replace trigger TGR_MR_AUDIT_TRAIL 
   after update on MEDICAL_RECORDS for each row

declare
   changed_from	varchar2(2000);
   changed_to	varchar2(2000);

   procedure APPEND_TO_CHANGED_FROM(old_value varchar2) is
   begin
      if LENGTH(changed_from) = 0 then
         changed_from := '<?xml version="1.0" encoding="ISO-8859-1"?>';
      end if;
      changed_from := changed_from || old_value;
   end;

   procedure APPEND_TO_CHANGED_TO(new_value varchar2) is
   begin
      if LENGTH(changed_to) = 0 then
         changed_to := '<?xml version="1.0" encoding="ISO-8859-1"?>';
      end if;
      changed_to := changed_to || new_value;
   end;

begin
      if nvl(:OLD.MED_RECORD_ID, 0) != nvl(:NEW.MED_RECORD_ID, 0) then
         APPEND_TO_CHANGED_FROM('<MedicalRecordID>' || to_char(:OLD.MED_RECORD_ID) || '</MedicalRecordID>');
         APPEND_TO_CHANGED_TO('<MedicalRecordID>' || to_char(:NEW.MED_RECORD_ID) || '</MedicalRecordID>');
      end if;
      if nvl(:OLD.CL_CLIENT_NUM, '') != nvl(:NEW.CL_CLIENT_NUM, '') then
         APPEND_TO_CHANGED_FROM('<ClientNumber>' || :OLD.CL_CLIENT_NUM || '</ClientNumber>');
         APPEND_TO_CHANGED_TO('<ClientNumber>' || :NEW.CL_CLIENT_NUM || '</ClientNumber>');
      end if;
      if nvl(:OLD.AGENT_ID, '') != nvl(:NEW.AGENT_ID, '') then
         APPEND_TO_CHANGED_FROM('<Agent>' || :OLD.AGENT_ID || '</Agent>');
         APPEND_TO_CHANGED_TO('<Agent>' || :NEW.AGENT_ID || '</Agent>');
      end if;
      if nvl(:OLD.BRANCH_ID, '') != nvl(:NEW.BRANCH_ID, '') then
         APPEND_TO_CHANGED_FROM('<Branch>' || :OLD.BRANCH_ID || '</Branch>');
         APPEND_TO_CHANGED_TO('<Branch>' || :NEW.BRANCH_ID || '</Branch>');
      end if;
      if nvl(:OLD.MED_STATUS, 0) != nvl(:NEW.MED_STATUS, 0) then
         APPEND_TO_CHANGED_FROM('<Status>' || to_char(:OLD.MED_STATUS) || '</Status>');
         APPEND_TO_CHANGED_TO('<Status>' || to_char(:NEW.MED_STATUS) || '</Status>');
      end if;
      if nvl(:OLD.MED_LAB_TEST_IND, '') != nvl(:NEW.MED_LAB_TEST_IND, '') then
         APPEND_TO_CHANGED_FROM('<LabTestIndicator>' || :OLD.MED_LAB_TEST_IND || '</LabTestIndicator>');
         APPEND_TO_CHANGED_TO('<LabTestIndicator>' || :NEW.MED_LAB_TEST_IND || '</LabTestIndicator>');
      end if;
      if nvl(:OLD.MED_LAB_ID, 0) != nvl(:NEW.MED_LAB_ID, 0) then
         APPEND_TO_CHANGED_FROM('<Laboratory>' || to_char(:OLD.MED_LAB_ID) || '</Laboratory>');
         APPEND_TO_CHANGED_TO('<Laboratory>' || to_char(:NEW.MED_LAB_ID) || '</Laboratory>');
      end if;
      if nvl(:OLD.MED_TEST_ID, 0) != nvl(:NEW.MED_TEST_ID, 0) then
         APPEND_TO_CHANGED_FROM('<TestCode>' || to_char(:OLD.MED_TEST_ID) || '</TestCode>');
         APPEND_TO_CHANGED_TO('<TestCode>' || to_char(:NEW.MED_TEST_ID) || '</TestCode>');
      end if;
      if nvl(:OLD.MED_DATE_CONDUCTED, '') != nvl(:NEW.MED_DATE_CONDUCTED, '') then
         APPEND_TO_CHANGED_FROM('<DateConducted>' || to_char(:OLD.MED_DATE_CONDUCTED) || '</DateConducted>');
         APPEND_TO_CHANGED_TO('<DateConducted>' || to_char(:NEW.MED_DATE_CONDUCTED) || '</DateConducted>');
      end if;
      if nvl(:OLD.MED_DATE_RECEIVED, '') != nvl(:NEW.MED_DATE_RECEIVED, '') then
         APPEND_TO_CHANGED_FROM('<DateReceived>' || to_char(:OLD.MED_DATE_RECEIVED) || '</DateReceived>');
         APPEND_TO_CHANGED_TO('<DateReceived>' || to_char(:NEW.MED_DATE_RECEIVED) || '</DateReceived>');
      end if;
      if nvl(:OLD.MED_VALIDITY_DATE, '') != nvl(:NEW.MED_VALIDITY_DATE, '') then
         APPEND_TO_CHANGED_FROM('<ValidityDate>' || to_char(:OLD.MED_VALIDITY_DATE) || '</ValidityDate>');
         APPEND_TO_CHANGED_TO('<ValidityDate>' || to_char(:NEW.MED_VALIDITY_DATE) || '</ValidityDate>');
      end if;
      if nvl(:OLD.MED_7DAY_MEMO_IND, '') != nvl(:NEW.MED_7DAY_MEMO_IND, '') then
         APPEND_TO_CHANGED_FROM('<SevenDayMemoIndicator>' || :OLD.MED_7DAY_MEMO_IND || '</SevenDayMemoIndicator>');
         APPEND_TO_CHANGED_TO('<SevenDayMemoIndicator>' || :NEW.MED_7DAY_MEMO_IND || '</SevenDayMemoIndicator>');
      end if;
      if nvl(:OLD.MED_7DAY_MEMO_DATE, '') != nvl(:NEW.MED_7DAY_MEMO_DATE, '') then
         APPEND_TO_CHANGED_FROM('<SevenDayMemoDate>' || to_char(:OLD.MED_7DAY_MEMO_DATE) || '</SevenDayMemoDate>');
         APPEND_TO_CHANGED_TO('<SevenDayMemoDate>' || to_char(:NEW.MED_7DAY_MEMO_DATE) || '</SevenDayMemoDate>');
      end if;
      if nvl(:OLD.MED_REQUESTING_PARTY, '') != nvl(:NEW.MED_REQUESTING_PARTY, '') then
         APPEND_TO_CHANGED_FROM('<RequestingParty>' || :OLD.MED_REQUESTING_PARTY || '</RequestingParty>');
         APPEND_TO_CHANGED_TO('<RequestingParty>' || :NEW.MED_REQUESTING_PARTY || '</RequestingParty>');
      end if;
      if nvl(:OLD.MED_DEPARTMENT_ID, '') != nvl(:NEW.MED_DEPARTMENT_ID, '') then
         APPEND_TO_CHANGED_FROM('<Department>' || :OLD.MED_DEPARTMENT_ID || '</Department>');
         APPEND_TO_CHANGED_TO('<Department>' || :NEW.MED_DEPARTMENT_ID || '</Department>');
      end if;
      if nvl(:OLD.MED_CHARGEABLE_TO, '') != nvl(:NEW.MED_CHARGEABLE_TO, '') then
         APPEND_TO_CHANGED_FROM('<ChargeableTo>' || :OLD.MED_CHARGEABLE_TO || '</ChargeableTo>');
         APPEND_TO_CHANGED_TO('<ChargeableTo>' || :NEW.MED_CHARGEABLE_TO || '</ChargeableTo>');
      end if;
      if nvl(:OLD.MED_PAID_BY_AGENT_IND, '') != nvl(:NEW.MED_PAID_BY_AGENT_IND, '') then
         APPEND_TO_CHANGED_FROM('<PaidByAgentIndicator>' || :OLD.MED_PAID_BY_AGENT_IND || '</PaidByAgentIndicator>');
         APPEND_TO_CHANGED_TO('<PaidByAgentIndicator>' || :NEW.MED_PAID_BY_AGENT_IND || '</PaidByAgentIndicator>');
      end if;
      if nvl(:OLD.MED_REQUEST_LOA_IND, '') != nvl(:NEW.MED_REQUEST_LOA_IND, '') then
         APPEND_TO_CHANGED_FROM('<RequestForLOAIndicator>' || :OLD.MED_REQUEST_LOA_IND || '</RequestForLOAIndicator>');
         APPEND_TO_CHANGED_TO('<RequestForLOAIndicator>' || :NEW.MED_REQUEST_LOA_IND || '</RequestForLOAIndicator>');
      end if;
      if nvl(:OLD.MED_RESULTS_RCVD, '') != nvl(:NEW.MED_RESULTS_RCVD, '') then
         APPEND_TO_CHANGED_FROM('<ResultsReceived>' || :OLD.MED_RESULTS_RCVD || '</ResultsReceived>');
         APPEND_TO_CHANGED_TO('<ResultsReceived>' || :NEW.MED_RESULTS_RCVD || '</ResultsReceived>');
      end if;
      if nvl(:OLD.CREATED_BY, '') != nvl(:NEW.CREATED_BY, '') then
         APPEND_TO_CHANGED_FROM('<CreatedBy>' || :OLD.CREATED_BY || '</CreatedBy>');
         APPEND_TO_CHANGED_TO('<CreatedBy>' || :NEW.CREATED_BY || '</CreatedBy>');
      end if;
      if nvl(:OLD.CREATED_DATE, '') != nvl(:NEW.CREATED_DATE, '') then
         APPEND_TO_CHANGED_FROM('<CreatedDate>' || to_char(:OLD.CREATED_DATE) || '</CreatedDate>');
         APPEND_TO_CHANGED_TO('<CreatedDate>' || to_char(:NEW.CREATED_DATE) || '</CreatedDate>');
      end if;
      if nvl(:OLD.UPDATED_BY, '') != nvl(:NEW.UPDATED_BY, '') then
         APPEND_TO_CHANGED_FROM('<UpdatedBy>' || :OLD.UPDATED_BY || '</UpdatedBy>');
         APPEND_TO_CHANGED_TO('<UpdatedBy>' || :NEW.UPDATED_BY || '</UpdatedBy>');
      end if;
      if nvl(:OLD.UPDATED_DATE, '') != nvl(:NEW.UPDATED_DATE, '') then
         APPEND_TO_CHANGED_FROM('<UpdatedDate>' || to_char(:OLD.UPDATED_DATE) || '</UpdatedDate>');
         APPEND_TO_CHANGED_TO('<UpdatedDate>' || to_char(:NEW.UPDATED_DATE) || '</UpdatedDate>');
      end if;

      if LENGTH(changed_from) > 0 then
         PROC_CREATE_AUDIT_TRAIL(4, 'UPDATE', :NEW.UPDATED_BY, to_char(:NEW.MED_RECORD_ID), changed_from, changed_to);
      end if;
end;
/


PROMPT CREATING TGR_NOTIFICATION_RECIPIENTS
create or replace trigger TGR_NOTIFICATION_RECIPIENTS
	after insert on NOTIFICATION_RECIPIENTS for each row
	when (new.NOT_PRIMARY_IND != '1')

declare
	cursor CURSOR_USER_ID (roleCode in VARCHAR2) is
		select user_id from user_roles where role_code = roleCode;
	
	cursor CURSOR_EVENT_ID (notId in NUMBER, lobCode in VARCHAR2) is 
		select evnt_id from process_configurations where not_id = notId and lob_code = lobCode;

	V_EVENT_ID NUMBER;
	V_USER_ID VARCHAR2(10);
	V_EXISTING NUMBER;

begin
	if inserting then 
		open CURSOR_USER_ID(:new.ROLE_ID);
		loop
			fetch CURSOR_USER_ID into V_USER_ID;
			open CURSOR_EVENT_ID (:new.NOT_ID, :new.LOB_CODE);
			exit when CURSOR_USER_ID%notfound;
			begin	
				loop
					fetch CURSOR_EVENT_ID into V_EVENT_ID;
					exit when CURSOR_EVENT_ID%notfound;
					begin 
						select count(*) into V_EXISTING from user_process_preferences where user_id = V_USER_ID and evnt_id = V_EVENT_ID;
						if (V_EXISTING = 0 ) then
							insert into user_process_preferences (user_id, evnt_id,upp_notification_ind) 
							values (V_USER_ID, V_EVENT_ID, '1');
						end if;
					end;
				end loop;
				close CURSOR_EVENT_ID;
			end;
		end loop;
		close CURSOR_USER_ID;
	end if;
end;
/

PROMPT CREATING TGR_NOT_AUDIT_TRAIL 
create or replace trigger TGR_NOT_AUDIT_TRAIL 
   after update or delete on NOTIFICATION_TEMPLATES for each row

declare
   changed_from	varchar2(2000);
   changed_to	varchar2(2000);

   procedure APPEND_TO_CHANGED_FROM(old_value varchar2) is
   begin
      if LENGTH(changed_from) = 0 then
         changed_from := '<?xml version="1.0" encoding="ISO-8859-1"?>';
      end if;
      changed_from := changed_from || old_value;
   end;

   procedure APPEND_TO_CHANGED_TO(new_value varchar2) is
   begin
      if LENGTH(changed_to) = 0 then
         changed_to := '<?xml version="1.0" encoding="ISO-8859-1"?>';
      end if;
      changed_to := changed_to || new_value;
   end;

begin
   if updating then
      if nvl(:OLD.NOT_ID, 0) != nvl(:NEW.NOT_ID, 0) then
         APPEND_TO_CHANGED_FROM('<NotificationID>' || to_char(:OLD.NOT_ID) || '</NotificationID>');
         APPEND_TO_CHANGED_TO('<NotificationID>' || to_char(:NEW.NOT_ID) || '</NotificationID>');
      end if;
      if nvl(:OLD.NOT_SUBJ, '') != nvl(:NEW.NOT_SUBJ, '') then
         APPEND_TO_CHANGED_FROM('<NotificationSubject>' || :OLD.NOT_SUBJ || '</NotificationSubject>');
         APPEND_TO_CHANGED_TO('<NotificationSubject>' || :NEW.NOT_SUBJ || '</NotificationSubject>');
      end if;
      if nvl(:OLD.NOT_DESC, '') != nvl(:NEW.NOT_DESC, '') then
         APPEND_TO_CHANGED_FROM('<NotificationDescription>' || :OLD.NOT_DESC || '</NotificationDescription>');
         APPEND_TO_CHANGED_TO('<NotificationDescription>' || :NEW.NOT_DESC || '</NotificationDescription>');
      end if;
      if nvl(:OLD.NOT_SENDER, '') != nvl(:NEW.NOT_SENDER, '') then
         APPEND_TO_CHANGED_FROM('<Sender>' || :OLD.NOT_SENDER || '</Sender>');
         APPEND_TO_CHANGED_TO('<Sender>' || :NEW.NOT_SENDER || '</Sender>');
      end if;
      if nvl(:OLD.NOT_NOTIFY_MOBILE_IND, '') != nvl(:NEW.NOT_NOTIFY_MOBILE_IND, '') then
         APPEND_TO_CHANGED_FROM('<NotifyMobile>' || :OLD.NOT_NOTIFY_MOBILE_IND || '</NotifyMobile>');
         APPEND_TO_CHANGED_TO('<NotifyMobile>' || :NEW.NOT_NOTIFY_MOBILE_IND || '</NotifyMobile>');
      end if;
      if nvl(:OLD.CREATED_BY, '') != nvl(:NEW.CREATED_BY, '') then
         APPEND_TO_CHANGED_FROM('<CreatedBy>' || :OLD.CREATED_BY || '</CreatedBy>');
         APPEND_TO_CHANGED_TO('<CreatedBy>' || :NEW.CREATED_BY || '</CreatedBy>');
      end if;
      if nvl(:OLD.CREATED_DATE, '') != nvl(:NEW.CREATED_DATE, '') then
         APPEND_TO_CHANGED_FROM('<CreatedDate>' || to_char(:OLD.CREATED_DATE) || '</CreatedDate>');
         APPEND_TO_CHANGED_TO('<CreatedDate>' || to_char(:NEW.CREATED_DATE) || '</CreatedDate>');
      end if;
      if nvl(:OLD.UPDATED_BY, '') != nvl(:NEW.UPDATED_BY, '') then
         APPEND_TO_CHANGED_FROM('<UpdatedBy>' || :OLD.UPDATED_BY || '</UpdatedBy>');
         APPEND_TO_CHANGED_TO('<UpdatedBy>' || :NEW.UPDATED_BY || '</UpdatedBy>');
      end if;
      if nvl(:OLD.UPDATED_DATE, '') != nvl(:NEW.UPDATED_DATE, '') then
         APPEND_TO_CHANGED_FROM('<UpdatedDate>' || to_char(:OLD.UPDATED_DATE) || '</UpdatedDate>');
         APPEND_TO_CHANGED_TO('<UpdatedDate>' || to_char(:NEW.UPDATED_DATE) || '</UpdatedDate>');
      end if;

      if LENGTH(changed_from) > 0 then
         PROC_CREATE_AUDIT_TRAIL(29, 'UPDATE', :NEW.UPDATED_BY, to_char(:NEW.NOT_ID), changed_from, changed_to);
      end if;
   elsif deleting then
      PROC_CREATE_AUDIT_TRAIL(29, 'DELETE', :OLD.UPDATED_BY, to_char(:OLD.NOT_ID), changed_from, changed_to);
   end if;
end;
/


PROMPT CREATING TGR_NR_AUDIT_TRAIL 
create or replace trigger TGR_NR_AUDIT_TRAIL 
   after update or delete on NOTIFICATION_RECIPIENTS for each row

declare
   changed_from	varchar2(2000);
   changed_to	varchar2(2000);

   procedure APPEND_TO_CHANGED_FROM(old_value varchar2) is
   begin
      if LENGTH(changed_from) = 0 then
         changed_from := '<?xml version="1.0" encoding="ISO-8859-1"?>';
      end if;
      changed_from := changed_from || old_value;
   end;

   procedure APPEND_TO_CHANGED_TO(new_value varchar2) is
   begin
      if LENGTH(changed_to) = 0 then
         changed_to := '<?xml version="1.0" encoding="ISO-8859-1"?>';
      end if;
      changed_to := changed_to || new_value;
   end;

begin
   if updating then
      if nvl(:OLD.NOT_ID, 0) != nvl(:NEW.NOT_ID, 0) then
         APPEND_TO_CHANGED_FROM('<Notification_ID>' || to_char(:OLD.NOT_ID) || '</Notification_ID>');
         APPEND_TO_CHANGED_TO('<Notification_ID>' || to_char(:NEW.NOT_ID) || '</Notification_ID>');
      end if;
      if nvl(:OLD.ROLE_ID, '') != nvl(:NEW.ROLE_ID, '') then
         APPEND_TO_CHANGED_FROM('<RoleID>' || :OLD.ROLE_ID || '</RoleID>');
         APPEND_TO_CHANGED_TO('<RoleID>' || :NEW.ROLE_ID || '</RoleID>');
      end if;
      if nvl(:OLD.LOB_CODE, '') != nvl(:NEW.LOB_CODE, '') then
         APPEND_TO_CHANGED_FROM('<LineOf Business>' || :OLD.LOB_CODE || '</LineOf Business>');
         APPEND_TO_CHANGED_TO('<LineOf Business>' || :NEW.LOB_CODE || '</LineOf Business>');
      end if;
      if nvl(:OLD.NOTIFICATION_TYPE, '') != nvl(:NEW.NOTIFICATION_TYPE, '') then
         APPEND_TO_CHANGED_FROM('<NotificationType>' || :OLD.NOTIFICATION_TYPE || '</NotificationType>');
         APPEND_TO_CHANGED_TO('<NotificationType>' || :NEW.NOTIFICATION_TYPE || '</NotificationType>');
      end if;
      if nvl(:OLD.CREATED_BY, '') != nvl(:NEW.CREATED_BY, '') then
         APPEND_TO_CHANGED_FROM('<CreatedBy>' || :OLD.CREATED_BY || '</CreatedBy>');
         APPEND_TO_CHANGED_TO('<CreatedBy>' || :NEW.CREATED_BY || '</CreatedBy>');
      end if;
      if nvl(:OLD.CREATED_DATE, '') != nvl(:NEW.CREATED_DATE, '') then
         APPEND_TO_CHANGED_FROM('<CreatedDate>' || to_char(:OLD.CREATED_DATE) || '</CreatedDate>');
         APPEND_TO_CHANGED_TO('<CreatedDate>' || to_char(:NEW.CREATED_DATE) || '</CreatedDate>');
      end if;
      if nvl(:OLD.UPDATED_BY, '') != nvl(:NEW.UPDATED_BY, '') then
         APPEND_TO_CHANGED_FROM('<UpdatedBy>' || :OLD.UPDATED_BY || '</UpdatedBy>');
         APPEND_TO_CHANGED_TO('<UpdatedBy>' || :NEW.UPDATED_BY || '</UpdatedBy>');
      end if;
      if nvl(:OLD.UPDATED_DATE, '') != nvl(:NEW.UPDATED_DATE, '') then
         APPEND_TO_CHANGED_FROM('<UpdatedDate>' || to_char(:OLD.UPDATED_DATE) || '</UpdatedDate>');
         APPEND_TO_CHANGED_TO('<UpdatedDate>' || to_char(:NEW.UPDATED_DATE) || '</UpdatedDate>');
      end if;

      if LENGTH(changed_from) > 0 then
         PROC_CREATE_AUDIT_TRAIL(29, 'UPDATE', :NEW.UPDATED_BY, to_char(:NEW.NOT_ID) || '-' || :NEW.ROLE_ID || '-' || :NEW.LOB_CODE, changed_from, changed_to);
      end if;
   elsif deleting then
      PROC_CREATE_AUDIT_TRAIL(29, 'DELETE', :OLD.UPDATED_BY, to_char(:OLD.NOT_ID) || '-' || :OLD.ROLE_ID || '-' || :OLD.LOB_CODE, changed_from, changed_to);
   end if;
end;
/


PROMPT CREATING TGR_PAGE_ACCESS
create or replace trigger TGR_PAGE_ACCESS
   after insert or update or delete on PAGES_ACCESS for each row

declare

   cursor CURSOR_ACCESS_TEMPLATES is
      SELECT TEMPLT_ID FROM ACCESS_TEMPLATES;

   cursor CURSOR_USER_PAGE_ACCESS is
      SELECT USER_ID FROM USER_PAGE_ACCESS;
   
   procedure INSERT_ACCESS_TEMPLATE_DETAILS is

      V_TEMPLT_ID   NUMBER(5);

   begin
      open CURSOR_ACCESS_TEMPLATES;
      loop
         fetch CURSOR_ACCESS_TEMPLATES into V_TEMPLT_ID;
         exit when CURSOR_ACCESS_TEMPLATES%notfound;
         begin
            INSERT INTO ACCESS_TEMPLATE_DETAILS
               (TEMPLT_ID, PA_ID, ATD_ACCESS_IND)
               VALUES(V_TEMPLT_ID, :NEW.PA_ID, '0');
         exception
            when others then null;
         end;
      end loop;
      close CURSOR_ACCESS_TEMPLATES;
   end;

   procedure DELETE_ACCESS_TEMPLATE_DETAILS is
   begin
      DELETE ACCESS_TEMPLATE_DETAILS WHERE PA_ID = :NEW.PA_ID;
   end;

   procedure INSERT_USER_PAGE_ACCESS is

      V_USER_ID   VARCHAR2(10);

   begin
      open CURSOR_USER_PAGE_ACCESS;
      loop
         fetch CURSOR_USER_PAGE_ACCESS into V_USER_ID;
         exit when CURSOR_USER_PAGE_ACCESS%notfound;
         begin
            INSERT INTO USER_PAGE_ACCESS
               (USER_ID, PAGE_ID, ACC_ID, UPA_ACCESS_IND, CREATED_BY, CREATED_DATE)
               VALUES (V_USER_ID, :NEW.PAGE_ID, :NEW.ACC_ID, '0', V_USER_ID, SYSDATE);
         exception
            when others then null;
         end;
      end loop;
      close CURSOR_USER_PAGE_ACCESS;
   end;

   procedure DELETE_USER_PAGE_ACCESS is
   begin
      DELETE USER_PAGE_ACCESS WHERE PAGE_ID = :NEW.PAGE_ID AND ACC_ID = :NEW.ACC_ID;
   end;
   
begin
   if inserting then
     if :NEW.PA_ENABLED_IND = '1' then
        INSERT_ACCESS_TEMPLATE_DETAILS();
        INSERT_USER_PAGE_ACCESS();
     else
        DELETE_ACCESS_TEMPLATE_DETAILS();
        DELETE_USER_PAGE_ACCESS();
     end if;
   elsif updating then
      if nvl(:OLD.PA_ENABLED_IND, '0') != nvl(:NEW.PA_ENABLED_IND, '0') then
         if :NEW.PA_ENABLED_IND = '1' then
            INSERT_ACCESS_TEMPLATE_DETAILS();
            INSERT_USER_PAGE_ACCESS();
         else
            DELETE_ACCESS_TEMPLATE_DETAILS();
            DELETE_USER_PAGE_ACCESS();
         end if;
      end if;
   elsif deleting then
      DELETE_ACCESS_TEMPLATE_DETAILS();
      DELETE_USER_PAGE_ACCESS();
   end if;
end;
/


PROMPT CREATING TGR_PAGE_AUDIT_TRAIL
create or replace trigger TGR_PAGE_AUDIT_TRAIL
   after update or delete on PAGES for each row

declare
   changed_from	varchar2(2000);
   changed_to	varchar2(2000);

   procedure APPEND_TO_CHANGED_FROM(old_value varchar2) is
   begin
      if LENGTH(changed_from) = 0 then
         changed_from := '<?xml version="1.0" encoding="ISO-8859-1"?>';
      end if;
      changed_from := changed_from || old_value;
   end;

   procedure APPEND_TO_CHANGED_TO(new_value varchar2) is
   begin
      if LENGTH(changed_to) = 0 then
         changed_to := '<?xml version="1.0" encoding="ISO-8859-1"?>';
      end if;
      changed_to := changed_to || new_value;
   end;

begin
   if updating then
      if nvl(:OLD.PAGE_ID, 0) != nvl(:NEW.PAGE_ID, 0) then
         APPEND_TO_CHANGED_FROM('<PageID>' || to_char(:OLD.PAGE_ID) || '</PageID>');
         APPEND_TO_CHANGED_TO('<PageID>' || to_char(:NEW.PAGE_ID) || '</PageID>');
      end if;
      if nvl(:OLD.PAGE_DESC, '') != nvl(:NEW.PAGE_DESC, '') then
         APPEND_TO_CHANGED_FROM('<PageDescription>' || :OLD.PAGE_DESC || '</PageDescription>');
         APPEND_TO_CHANGED_TO('<PageDescription>' || :NEW.PAGE_DESC || '</PageDescription>');
      end if;
      if nvl(:OLD.CREATED_BY, '') != nvl(:NEW.CREATED_BY, '') then
         APPEND_TO_CHANGED_FROM('<CreatedBy>' || :OLD.CREATED_BY || '</CreatedBy>');
         APPEND_TO_CHANGED_TO('<CreatedBy>' || :NEW.CREATED_BY || '</CreatedBy>');
      end if;
      if nvl(:OLD.CREATED_DATE, '') != nvl(:NEW.CREATED_DATE, '') then
         APPEND_TO_CHANGED_FROM('<CreatedDate>' || to_char(:OLD.CREATED_DATE) || '</CreatedDate>');
         APPEND_TO_CHANGED_TO('<CreatedDate>' || to_char(:NEW.CREATED_DATE) || '</CreatedDate>');
      end if;
      if nvl(:OLD.UPDATED_BY, '') != nvl(:NEW.UPDATED_BY, '') then
         APPEND_TO_CHANGED_FROM('<UpdatedBy>' || :OLD.UPDATED_BY || '</UpdatedBy>');
         APPEND_TO_CHANGED_TO('<UpdatedBy>' || :NEW.UPDATED_BY || '</UpdatedBy>');
      end if;
      if nvl(:OLD.UPDATED_DATE, '') != nvl(:NEW.UPDATED_DATE, '') then
         APPEND_TO_CHANGED_FROM('<UpdatedDate>' || to_char(:OLD.UPDATED_DATE) || '</UpdatedDate>');
         APPEND_TO_CHANGED_TO('<UpdatedDate>' || to_char(:NEW.UPDATED_DATE) || '</UpdatedDate>');
      end if;

      if LENGTH(changed_from) > 0 then
         PROC_CREATE_AUDIT_TRAIL(41, 'UPDATE', :NEW.UPDATED_BY, to_char(:NEW.PAGE_ID), changed_from, changed_to);
      end if;
   elsif deleting then
      PROC_CREATE_AUDIT_TRAIL(41, 'DELETE', :OLD.UPDATED_BY, to_char(:OLD.PAGE_ID), changed_from, changed_to);
   end if;
end;
/


PROMPT CREATING TGR_PC_AUDIT_TRAIL
create or replace trigger TGR_PC_AUDIT_TRAIL
   after update or delete on PROCESS_CONFIGURATIONS for each row

declare
   changed_from	varchar2(2000);
   changed_to	varchar2(2000);

   procedure APPEND_TO_CHANGED_FROM(old_value varchar2) is
   begin
      if LENGTH(changed_from) = 0 then
         changed_from := '<?xml version="1.0" encoding="ISO-8859-1"?>';
      end if;
      changed_from := changed_from || old_value;
   end;

   procedure APPEND_TO_CHANGED_TO(new_value varchar2) is
   begin
      if LENGTH(changed_to) = 0 then
         changed_to := '<?xml version="1.0" encoding="ISO-8859-1"?>';
      end if;
      changed_to := changed_to || new_value;
   end;

begin
   if updating then
      if nvl(:OLD.EVNT_ID, 0) != nvl(:NEW.EVNT_ID, 0) then
         APPEND_TO_CHANGED_FROM('<EventID>' || to_char(:OLD.EVNT_ID) || '</EventID>');
         APPEND_TO_CHANGED_TO('<EventID>' || to_char(:NEW.EVNT_ID) || '</EventID>');
      end if;
      if nvl(:OLD.PROC_CODE, '') != nvl(:NEW.PROC_CODE, '') then
         APPEND_TO_CHANGED_FROM('<ProcessCode>' || :OLD.PROC_CODE || '</ProcessCode>');
         APPEND_TO_CHANGED_TO('<ProcessCode>' || :NEW.PROC_CODE || '</ProcessCode>');
      end if;
      if nvl(:OLD.LOB_CODE, '') != nvl(:NEW.LOB_CODE, '') then
         APPEND_TO_CHANGED_FROM('<LOB>' || :OLD.LOB_CODE || '</LOB>');
         APPEND_TO_CHANGED_TO('<LOB>' || :NEW.LOB_CODE || '</LOB>');
      end if;
      if nvl(:OLD.EVNT_NAME, '') != nvl(:NEW.EVNT_NAME, '') then
         APPEND_TO_CHANGED_FROM('<EventName>' || :OLD.EVNT_NAME || '</EventName>');
         APPEND_TO_CHANGED_TO('<EventName>' || :NEW.EVNT_NAME || '</EventName>');
      end if;
      if nvl(:OLD.EVNT_FROM_STATUS, 0) != nvl(:NEW.EVNT_FROM_STATUS, 0) then
         APPEND_TO_CHANGED_FROM('<FromEvent>' || to_char(:OLD.EVNT_FROM_STATUS) || '</FromEvent>');
         APPEND_TO_CHANGED_TO('<FromEvent>' || to_char(:NEW.EVNT_FROM_STATUS) || '</FromEvent>');
      end if;
      if nvl(:OLD.EVNT_TO_STATUS, 0) != nvl(:NEW.EVNT_TO_STATUS, 0) then
         APPEND_TO_CHANGED_FROM('<ToEvent>' || to_char(:OLD.EVNT_TO_STATUS) || '</ToEvent>');
         APPEND_TO_CHANGED_TO('<ToEvent>' || to_char(:NEW.EVNT_TO_STATUS) || '</ToEvent>');
      end if;
      if nvl(:OLD.CREATED_BY, '') != nvl(:NEW.CREATED_BY, '') then
         APPEND_TO_CHANGED_FROM('<CreatedBy>' || :OLD.CREATED_BY || '</CreatedBy>');
         APPEND_TO_CHANGED_TO('<CreatedBy>' || :NEW.CREATED_BY || '</CreatedBy>');
      end if;
      if nvl(:OLD.CREATED_DATE, '') != nvl(:NEW.CREATED_DATE, '') then
         APPEND_TO_CHANGED_FROM('<CreatedDate>' || to_char(:OLD.CREATED_DATE) || '</CreatedDate>');
         APPEND_TO_CHANGED_TO('<CreatedDate>' || to_char(:NEW.CREATED_DATE) || '</CreatedDate>');
      end if;
      if nvl(:OLD.UPDATED_BY, '') != nvl(:NEW.UPDATED_BY, '') then
         APPEND_TO_CHANGED_FROM('<UpdatedBy>' || :OLD.UPDATED_BY || '</UpdatedBy>');
         APPEND_TO_CHANGED_TO('<UpdatedBy>' || :NEW.UPDATED_BY || '</UpdatedBy>');
      end if;
      if nvl(:OLD.UPDATED_DATE, '') != nvl(:NEW.UPDATED_DATE, '') then
         APPEND_TO_CHANGED_FROM('<UpdatedDate>' || to_char(:OLD.UPDATED_DATE) || '</UpdatedDate>');
         APPEND_TO_CHANGED_TO('<UpdatedDate>' || to_char(:NEW.UPDATED_DATE) || '</UpdatedDate>');
      end if;

      if LENGTH(changed_from) > 0 then
         PROC_CREATE_AUDIT_TRAIL(26, 'UPDATE', :NEW.UPDATED_BY, to_char(:NEW.EVNT_ID), changed_from, changed_to);
      end if;
   elsif deleting then
      PROC_CREATE_AUDIT_TRAIL(26, 'DELETE', :OLD.UPDATED_BY, to_char(:OLD.EVNT_ID), changed_from, changed_to);
   end if;
end;
/


PROMPT CREATING TGR_PROC_CONF_ROLES
create or replace trigger TGR_PROC_CONF_ROLES
	after insert or delete on PROCESS_CONFIGURATION_ROLES for each row

declare 
	cursor CURSOR_USER_ID (roleCode in VARCHAR2) is 
		select user_id from user_roles where role_code = roleCode;
	V_USER_ID VARCHAR2(10);
	V_EXISTING NUMBER;

begin 
	if inserting then
		open CURSOR_USER_ID (:new.ROLE_CODE);
		loop
			fetch CURSOR_USER_ID into V_USER_ID;
			exit when CURSOR_USER_ID%notfound;
			begin
				select count(*) into V_EXISTING from user_process_preferences where user_id = V_USER_ID and evnt_id = :new.EVNT_ID;
				if (V_EXISTING = 0 ) then 
					insert into user_process_preferences 
					values(V_USER_ID, :new.EVNT_ID, '1');
				end if;
			end;
		end loop;
		close CURSOR_USER_ID;
	
	elsif deleting then
		open CURSOR_USER_ID (:old.ROLE_CODE);
		loop
			fetch CURSOR_USER_ID into V_USER_ID;
			exit when CURSOR_USER_ID%notfound;
			begin
				delete from user_process_preferences where user_id = V_USER_ID and evnt_id = :old.EVNT_ID;
			end;
		end loop;
		close CURSOR_USER_ID;
	end if;
end;
/

PROMPT CREATING TGR_PR_AUDIT_TRAIL
create or replace trigger TGR_PR_AUDIT_TRAIL
   after update on POLICY_REQUIREMENTS for each row
declare
   changed_from    varchar2(2000);
   changed_to      varchar2(2000);

   procedure APPEND_TO_CHANGED_FROM(old_value varchar2) is
   begin
      if LENGTH(changed_from) = 0 then
         changed_from := '<?xml version="1.0" encoding="ISO-8859-1"?>';
      end if;
      changed_from := changed_from || old_value;
   end;

   procedure APPEND_TO_CHANGED_TO(new_value varchar2) is
   begin
      if LENGTH(changed_to) = 0 then
         changed_to := '<?xml version="1.0" encoding="ISO-8859-1"?>';
      end if;
      changed_to := changed_to || new_value;
   end;

begin
      if nvl(:OLD.UAR_REFERENCE_NUM, '') != nvl(:NEW.UAR_REFERENCE_NUM, '') then
         APPEND_TO_CHANGED_FROM('<ReferenceNumber>' || :OLD.UAR_REFERENCE_NUM || '</ReferenceNumber>');
         APPEND_TO_CHANGED_TO('<ReferenceNumber>' || :NEW.UAR_REFERENCE_NUM || '</ReferenceNumber>');
      end if;
      if nvl(:OLD.PR_REQUIREMENT_ID, 0) != nvl(:NEW.PR_REQUIREMENT_ID, 0) then
         APPEND_TO_CHANGED_FROM('<RequirementID>' || to_char(:OLD.PR_REQUIREMENT_ID) || '</RequirementID>');
         APPEND_TO_CHANGED_TO('<RequirementID>' || to_char(:NEW.PR_REQUIREMENT_ID) || '</RequirementID>');
      end if;
      if nvl(:OLD.PR_ADMIN_SEQUENCE_NUM, 0) != nvl(:NEW.PR_ADMIN_SEQUENCE_NUM, 0) then
         APPEND_TO_CHANGED_FROM('<SequenceNumber>' || to_char(:OLD.PR_ADMIN_SEQUENCE_NUM) || '</SequenceNumber>');
         APPEND_TO_CHANGED_TO('<SequenceNumber>' || to_char(:NEW.PR_ADMIN_SEQUENCE_NUM) || '</SequenceNumber>');
      end if;
      if nvl(:OLD.PR_REQT_CODE, '') != nvl(:NEW.PR_REQT_CODE, '') then
         APPEND_TO_CHANGED_FROM('<RequirementCode>' || :OLD.PR_REQT_CODE || '</RequirementCode>');
         APPEND_TO_CHANGED_TO('<RequirementCode>' || :NEW.PR_REQT_CODE || '</RequirementCode>');
      end if;
      if nvl(:OLD.PR_ADMIN_CREATE_DATE, '') != nvl(:NEW.PR_ADMIN_CREATE_DATE, '') then
         APPEND_TO_CHANGED_FROM('<ABACUSCreatedDate>' || to_char(:OLD.PR_ADMIN_CREATE_DATE) || '</ABACUSCreatedDate>');
         APPEND_TO_CHANGED_TO('<ABACUSCreatedDate>' || to_char(:NEW.PR_ADMIN_CREATE_DATE) || '</ABACUSCreatedDate>');
      end if;
      if nvl(:OLD.PR_STATUS_ID, 0) != nvl(:NEW.PR_STATUS_ID, 0) then
         APPEND_TO_CHANGED_FROM('<Status>' || to_char(:OLD.PR_STATUS_ID) || '</Status>');
         APPEND_TO_CHANGED_TO('<Status>' || to_char(:NEW.PR_STATUS_ID) || '</Status>');
      end if;
      if nvl(:OLD.PR_STATUS_DATE, '') != nvl(:NEW.PR_STATUS_DATE, '') then
         APPEND_TO_CHANGED_FROM('<StatusDate>' || to_char(:OLD.PR_STATUS_DATE) || '</StatusDate>');
         APPEND_TO_CHANGED_TO('<StatusDate>' || to_char(:NEW.PR_STATUS_DATE) || '</StatusDate>');
      end if;
      if nvl(:OLD.PR_CLIENT_TYPE, '') != nvl(:NEW.PR_CLIENT_TYPE, '') then
         APPEND_TO_CHANGED_FROM('<ClientType>' || :OLD.PR_CLIENT_TYPE || '</ClientType>');
         APPEND_TO_CHANGED_TO('<ClientType>' || :NEW.PR_CLIENT_TYPE || '</ClientType>');
      end if;
      if nvl(:OLD.PR_VALIDITY_DATE, '') != nvl(:NEW.PR_VALIDITY_DATE, '') then
         APPEND_TO_CHANGED_FROM('<ValidityDate>' || to_char(:OLD.PR_VALIDITY_DATE) || '</ValidityDate>');
         APPEND_TO_CHANGED_TO('<ValidityDate>' || to_char(:NEW.PR_VALIDITY_DATE) || '</ValidityDate>');
      end if;
      if nvl(:OLD.PR_ORDER_DATE, '') != nvl(:NEW.PR_ORDER_DATE, '') then
         APPEND_TO_CHANGED_FROM('<OrderDate>' || to_char(:OLD.PR_ORDER_DATE) || '</OrderDate>');
         APPEND_TO_CHANGED_TO('<OrderDate>' || to_char(:NEW.PR_ORDER_DATE) || '</OrderDate>');
      end if;
      if nvl(:OLD.PR_DATE_SENT, '') != nvl(:NEW.PR_DATE_SENT, '') then
         APPEND_TO_CHANGED_FROM('<DateSent>' || to_char(:OLD.PR_DATE_SENT) || '</DateSent>');
         APPEND_TO_CHANGED_TO('<DateSent>' || to_char(:NEW.PR_DATE_SENT) || '</DateSent>');
      end if;
      if nvl(:OLD.PR_RECEIVED_DATE, '') != nvl(:NEW.PR_RECEIVED_DATE, '') then
         APPEND_TO_CHANGED_FROM('<ReceivedDate>' || to_char(:OLD.PR_RECEIVED_DATE) || '</ReceivedDate>');
         APPEND_TO_CHANGED_TO('<ReceivedDate>' || to_char(:NEW.PR_RECEIVED_DATE) || '</ReceivedDate>');
      end if;
      if nvl(:OLD.CREATED_BY, '') != nvl(:NEW.CREATED_BY, '') then
         APPEND_TO_CHANGED_FROM('<CreatedBy>' || :OLD.CREATED_BY || '</CreatedBy>');
         APPEND_TO_CHANGED_TO('<CreatedBy>' || :NEW.CREATED_BY || '</CreatedBy>');
      end if;
      if nvl(:OLD.CREATED_DATE, '') != nvl(:NEW.CREATED_DATE, '') then
         APPEND_TO_CHANGED_FROM('<CreatedDate>' || to_char(:OLD.CREATED_DATE) || '</CreatedDate>');
         APPEND_TO_CHANGED_TO('<CreatedDate>' || to_char(:NEW.CREATED_DATE) || '</CreatedDate>');
      end if;
      if nvl(:OLD.UPDATED_BY, '') != nvl(:NEW.UPDATED_BY, '') then
         APPEND_TO_CHANGED_FROM('<UpdatedBy>' || :OLD.UPDATED_BY || '</UpdatedBy>');
         APPEND_TO_CHANGED_TO('<UpdatedBy>' || :NEW.UPDATED_BY || '</UpdatedBy>');
      end if;
      if nvl(:OLD.UPDATED_DATE, '') != nvl(:NEW.UPDATED_DATE, '') then
         APPEND_TO_CHANGED_FROM('<UpdatedDate>' || to_char(:OLD.UPDATED_DATE) || '</UpdatedDate>');
         APPEND_TO_CHANGED_TO('<UpdatedDate>' || to_char(:NEW.UPDATED_DATE) || '</UpdatedDate>');
      end if;

      if LENGTH(changed_from) > 0 then
         PROC_CREATE_AUDIT_TRAIL(2, 'UPDATE', :NEW.UPDATED_BY, to_char(:NEW.PR_REQUIREMENT_ID), changed_from, changed_to);
      end if;
end;
/


PROMPT CREATING TGR_RANK_AUDIT_TRAIL
create or replace trigger TGR_RANK_AUDIT_TRAIL
   after update or delete on RANKS for each row

declare
   changed_from	varchar2(2000);
   changed_to	varchar2(2000);

   procedure APPEND_TO_CHANGED_FROM(old_value varchar2) is
   begin
      if LENGTH(changed_from) = 0 then
         changed_from := '<?xml version="1.0" encoding="ISO-8859-1"?>';
      end if;
      changed_from := changed_from || old_value;
   end;

   procedure APPEND_TO_CHANGED_TO(new_value varchar2) is
   begin
      if LENGTH(changed_to) = 0 then
         changed_to := '<?xml version="1.0" encoding="ISO-8859-1"?>';
      end if;
      changed_to := changed_to || new_value;
   end;

begin
   if updating then
      if nvl(:OLD.RANK_ID, 0) != nvl(:NEW.RANK_ID, 0) then
         APPEND_TO_CHANGED_FROM('<Code>' || to_char(:OLD.RANK_ID) || '</Code>');
         APPEND_TO_CHANGED_TO('<Code>' || to_char(:NEW.RANK_ID) || '</Code>');
      end if;
      if nvl(:OLD.CREATED_BY, '') != nvl(:NEW.CREATED_BY, '') then
         APPEND_TO_CHANGED_FROM('<CreatedBy>' || :OLD.CREATED_BY || '</CreatedBy>');
         APPEND_TO_CHANGED_TO('<CreatedBy>' || :NEW.CREATED_BY || '</CreatedBy>');
      end if;
      if nvl(:OLD.CREATED_DATE, '') != nvl(:NEW.CREATED_DATE, '') then
         APPEND_TO_CHANGED_FROM('<CreatedDate>' || to_char(:OLD.CREATED_DATE) || '</CreatedDate>');
         APPEND_TO_CHANGED_TO('<CreatedDate>' || to_char(:NEW.CREATED_DATE) || '</CreatedDate>');
      end if;
      if nvl(:OLD.UPDATED_BY, '') != nvl(:NEW.UPDATED_BY, '') then
         APPEND_TO_CHANGED_FROM('<UpdatedBy>' || :OLD.UPDATED_BY || '</UpdatedBy>');
         APPEND_TO_CHANGED_TO('<UpdatedBy>' || :NEW.UPDATED_BY || '</UpdatedBy>');
      end if;
      if nvl(:OLD.UPDATED_DATE, '') != nvl(:NEW.UPDATED_DATE, '') then
         APPEND_TO_CHANGED_FROM('<UpdatedDate>' || to_char(:OLD.UPDATED_DATE) || '</UpdatedDate>');
         APPEND_TO_CHANGED_TO('<UpdatedDate>' || to_char(:NEW.UPDATED_DATE) || '</UpdatedDate>');
      end if;

      if LENGTH(changed_from) > 0 then
         PROC_CREATE_AUDIT_TRAIL(23, 'UPDATE', :NEW.UPDATED_BY, to_char(:NEW.RANK_ID), changed_from, changed_to);
      end if;
   elsif deleting then
      PROC_CREATE_AUDIT_TRAIL(23, 'DELETE', :OLD.UPDATED_BY, to_char(:OLD.RANK_ID), changed_from, changed_to);
   end if;
end;
/

PROMPT CREATING TGR_REQT_AUDIT_TRAIL
create or replace trigger TGR_REQT_AUDIT_TRAIL
   after update or delete on REQUIREMENTS for each row

declare
   changed_from	varchar2(2000);
   changed_to	varchar2(2000);

   procedure APPEND_TO_CHANGED_FROM(old_value varchar2) is
   begin
      if LENGTH(changed_from) = 0 then
         changed_from := '<?xml version="1.0" encoding="ISO-8859-1"?>';
      end if;
      changed_from := changed_from || old_value;
   end;

   procedure APPEND_TO_CHANGED_TO(new_value varchar2) is
   begin
      if LENGTH(changed_to) = 0 then
         changed_to := '<?xml version="1.0" encoding="ISO-8859-1"?>';
      end if;
      changed_to := changed_to || new_value;
   end;

begin
   if updating then
      if nvl(:OLD.REQT_CODE, '') != nvl(:NEW.REQT_CODE, '') then
         APPEND_TO_CHANGED_FROM('<Code>' || :OLD.REQT_CODE || '</Code>');
         APPEND_TO_CHANGED_TO('<Code>' || :NEW.REQT_CODE || '</Code>');
      end if;
      if nvl(:OLD.REQT_LEVEL, '') != nvl(:NEW.REQT_LEVEL, '') then
         APPEND_TO_CHANGED_FROM('<Level>' || :OLD.REQT_LEVEL || '</Level>');
         APPEND_TO_CHANGED_TO('<Level>' || :NEW.REQT_LEVEL || '</Level>');
      end if;
      if nvl(:OLD.REQT_VALIDITY, 0) != nvl(:NEW.REQT_VALIDITY, 0) then
         APPEND_TO_CHANGED_FROM('<Validity>' || to_char(:OLD.REQT_VALIDITY) || '</Validity>');
         APPEND_TO_CHANGED_TO('<Validity>' || to_char(:NEW.REQT_VALIDITY) || '</Validity>');
      end if;
      if nvl(:OLD.REQT_FOLLOW_UP_NUM, 0) != nvl(:NEW.REQT_FOLLOW_UP_NUM, 0) then
         APPEND_TO_CHANGED_FROM('<FollowUpNumber>' || to_char(:OLD.REQT_FOLLOW_UP_NUM) || '</FollowUpNumber>');
         APPEND_TO_CHANGED_TO('<FollowUpNumber>' || to_char(:NEW.REQT_FOLLOW_UP_NUM) || '</FollowUpNumber>');
      end if;
      if nvl(:OLD.CREATED_BY, '') != nvl(:NEW.CREATED_BY, '') then
         APPEND_TO_CHANGED_FROM('<CreatedBy>' || :OLD.CREATED_BY || '</CreatedBy>');
         APPEND_TO_CHANGED_TO('<CreatedBy>' || :NEW.CREATED_BY || '</CreatedBy>');
      end if;
      if nvl(:OLD.CREATED_DATE, '') != nvl(:NEW.CREATED_DATE, '') then
         APPEND_TO_CHANGED_FROM('<CreatedDate>' || to_char(:OLD.CREATED_DATE) || '</CreatedDate>');
         APPEND_TO_CHANGED_TO('<CreatedDate>' || to_char(:NEW.CREATED_DATE) || '</CreatedDate>');
      end if;
      if nvl(:OLD.UPDATED_BY, '') != nvl(:NEW.UPDATED_BY, '') then
         APPEND_TO_CHANGED_FROM('<UpdatedBy>' || :OLD.UPDATED_BY || '</UpdatedBy>');
         APPEND_TO_CHANGED_TO('<UpdatedBy>' || :NEW.UPDATED_BY || '</UpdatedBy>');
      end if;
      if nvl(:OLD.UPDATED_DATE, '') != nvl(:NEW.UPDATED_DATE, '') then
         APPEND_TO_CHANGED_FROM('<UpdatedDate>' || to_char(:OLD.UPDATED_DATE) || '</UpdatedDate>');
         APPEND_TO_CHANGED_TO('<UpdatedDate>' || to_char(:NEW.UPDATED_DATE) || '</UpdatedDate>');
      end if;

      if LENGTH(changed_from) > 0 then
         PROC_CREATE_AUDIT_TRAIL(7, 'UPDATE', :NEW.UPDATED_BY, :NEW.REQT_CODE, changed_from, changed_to);
      end if;
   elsif deleting then
      PROC_CREATE_AUDIT_TRAIL(7, 'DELETE', :OLD.UPDATED_BY, :OLD.REQT_CODE, changed_from, changed_to);
   end if;
end;
/


PROMPT CREATING TGR_RF_AUDIT_TRAIL
create or replace trigger TGR_RF_AUDIT_TRAIL
   after update or delete on REQUIREMENT_FORMS for each row

declare
   changed_from	varchar2(2000);
   changed_to	varchar2(2000);

   procedure APPEND_TO_CHANGED_FROM(old_value varchar2) is
   begin
      if LENGTH(changed_from) = 0 then
         changed_from := '<?xml version="1.0" encoding="ISO-8859-1"?>';
      end if;
      changed_from := changed_from || old_value;
   end;

   procedure APPEND_TO_CHANGED_TO(new_value varchar2) is
   begin
      if LENGTH(changed_to) = 0 then
         changed_to := '<?xml version="1.0" encoding="ISO-8859-1"?>';
      end if;
      changed_to := changed_to || new_value;
   end;

begin
   if updating then
      if nvl(:OLD.RF_ID, 0) != nvl(:NEW.RF_ID, 0) then
         APPEND_TO_CHANGED_FROM('<FormID>' || to_char(:OLD.RF_ID) || '</FormID>');
         APPEND_TO_CHANGED_TO('<FormID>' || to_char(:NEW.RF_ID) || '</FormID>');
      end if;
      if nvl(:OLD.RF_NAME, '') != nvl(:NEW.RF_NAME, '') then
         APPEND_TO_CHANGED_FROM('<Name>' || :OLD.RF_NAME || '</Name>');
         APPEND_TO_CHANGED_TO('<Name>' || :NEW.RF_NAME || '</Name>');
      end if;
      if nvl(:OLD.RF_TEMPLATE_FORMAT, '') != nvl(:NEW.RF_TEMPLATE_FORMAT, '') then
         APPEND_TO_CHANGED_FROM('<TemplateFormat>' || :OLD.RF_TEMPLATE_FORMAT || '</TemplateFormat>');
         APPEND_TO_CHANGED_TO('<TemplateFormat>' || :NEW.RF_TEMPLATE_FORMAT || '</TemplateFormat>');
      end if;
      if nvl(:OLD.RF_TEMPLATE_NAME, 0) != nvl(:NEW.RF_TEMPLATE_NAME, 0) then
         APPEND_TO_CHANGED_FROM('<TemplateName>' || to_char(:OLD.RF_TEMPLATE_NAME) || '</TemplateName>');
         APPEND_TO_CHANGED_TO('<TemplateName>' || to_char(:NEW.RF_TEMPLATE_NAME) || '</TemplateName>');
      end if;
      if nvl(:OLD.RF_STATUS, '') != nvl(:NEW.RF_STATUS, '') then
         APPEND_TO_CHANGED_FROM('<Status>' || :OLD.RF_STATUS || '</Status>');
         APPEND_TO_CHANGED_TO('<Status>' || :NEW.RF_STATUS || '</Status>');
      end if;
      if nvl(:OLD.CREATED_BY, '') != nvl(:NEW.CREATED_BY, '') then
         APPEND_TO_CHANGED_FROM('<CreatedBy>' || :OLD.CREATED_BY || '</CreatedBy>');
         APPEND_TO_CHANGED_TO('<CreatedBy>' || :NEW.CREATED_BY || '</CreatedBy>');
      end if;
      if nvl(:OLD.CREATED_DATE, '') != nvl(:NEW.CREATED_DATE, '') then
         APPEND_TO_CHANGED_FROM('<CreatedDate>' || to_char(:OLD.CREATED_DATE) || '</CreatedDate>');
         APPEND_TO_CHANGED_TO('<CreatedDate>' || to_char(:NEW.CREATED_DATE) || '</CreatedDate>');
      end if;
      if nvl(:OLD.UPDATED_BY, '') != nvl(:NEW.UPDATED_BY, '') then
         APPEND_TO_CHANGED_FROM('<UpdatedBy>' || :OLD.UPDATED_BY || '</UpdatedBy>');
         APPEND_TO_CHANGED_TO('<UpdatedBy>' || :NEW.UPDATED_BY || '</UpdatedBy>');
      end if;
      if nvl(:OLD.UPDATED_DATE, '') != nvl(:NEW.UPDATED_DATE, '') then
         APPEND_TO_CHANGED_FROM('<UpdatedDate>' || to_char(:OLD.UPDATED_DATE) || '</UpdatedDate>');
         APPEND_TO_CHANGED_TO('<UpdatedDate>' || to_char(:NEW.UPDATED_DATE) || '</UpdatedDate>');
      end if;

      if LENGTH(changed_from) > 0 then
         PROC_CREATE_AUDIT_TRAIL(8, 'UPDATE', :NEW.UPDATED_BY, to_char(:NEW.RF_ID), changed_from, changed_to);
      end if;
   elsif deleting then
      PROC_CREATE_AUDIT_TRAIL(8, 'DELETE', :OLD.UPDATED_BY, to_char(:OLD.RF_ID), changed_from, changed_to);
   end if;
end;
/


PROMPT CREATING TGR_ROLE_AUDIT_TRAIL
create or replace trigger TGR_ROLE_AUDIT_TRAIL
   after update or delete on ROLES for each row

declare
   changed_from	varchar2(2000);
   changed_to	varchar2(2000);

   procedure APPEND_TO_CHANGED_FROM(old_value varchar2) is
   begin
      if LENGTH(changed_from) = 0 then
         changed_from := '<?xml version="1.0" encoding="ISO-8859-1"?>';
      end if;
      changed_from := changed_from || old_value;
   end;

   procedure APPEND_TO_CHANGED_TO(new_value varchar2) is
   begin
      if LENGTH(changed_to) = 0 then
         changed_to := '<?xml version="1.0" encoding="ISO-8859-1"?>';
      end if;
      changed_to := changed_to || new_value;
   end;

begin
   if updating then
      if nvl(:OLD.ROLE_CODE, '') != nvl(:NEW.ROLE_CODE, '') then
         APPEND_TO_CHANGED_FROM('<RoleID>' || :OLD.ROLE_CODE || '</RoleID>');
         APPEND_TO_CHANGED_TO('<RoleID>' || :NEW.ROLE_CODE || '</RoleID>');
      end if;
      if nvl(:OLD.ROLE_DESC, '') != nvl(:NEW.ROLE_DESC, '') then
         APPEND_TO_CHANGED_FROM('<Description>' || :OLD.ROLE_DESC || '</Description>');
         APPEND_TO_CHANGED_TO('<Description>' || :NEW.ROLE_DESC || '</Description>');
      end if;
      if nvl(:OLD.CREATED_BY, '') != nvl(:NEW.CREATED_BY, '') then
         APPEND_TO_CHANGED_FROM('<CreatedBy>' || :OLD.CREATED_BY || '</CreatedBy>');
         APPEND_TO_CHANGED_TO('<CreatedBy>' || :NEW.CREATED_BY || '</CreatedBy>');
      end if;
      if nvl(:OLD.CREATED_DATE, '') != nvl(:NEW.CREATED_DATE, '') then
         APPEND_TO_CHANGED_FROM('<CreatedDate>' || to_char(:OLD.CREATED_DATE) || '</CreatedDate>');
         APPEND_TO_CHANGED_TO('<CreatedDate>' || to_char(:NEW.CREATED_DATE) || '</CreatedDate>');
      end if;
      if nvl(:OLD.UPDATED_BY, '') != nvl(:NEW.UPDATED_BY, '') then
         APPEND_TO_CHANGED_FROM('<UpdatedBy>' || :OLD.UPDATED_BY || '</UpdatedBy>');
         APPEND_TO_CHANGED_TO('<UpdatedBy>' || :NEW.UPDATED_BY || '</UpdatedBy>');
      end if;
      if nvl(:OLD.UPDATED_DATE, '') != nvl(:NEW.UPDATED_DATE, '') then
         APPEND_TO_CHANGED_FROM('<UpdatedDate>' || to_char(:OLD.UPDATED_DATE) || '</UpdatedDate>');
         APPEND_TO_CHANGED_TO('<UpdatedDate>' || to_char(:NEW.UPDATED_DATE) || '</UpdatedDate>');
      end if;

      if LENGTH(changed_from) > 0 then
         PROC_CREATE_AUDIT_TRAIL(28, 'UPDATE', :NEW.UPDATED_BY, :NEW.ROLE_CODE, changed_from, changed_to);
      end if;
   elsif deleting then
      PROC_CREATE_AUDIT_TRAIL(28, 'DELETE', :OLD.UPDATED_BY, :OLD.ROLE_CODE, changed_from, changed_to);
   end if;
end;
/


PROMPT CREATING TGR_SCHED_AUDIT_TRAIL
create or replace trigger TGR_SCHED_AUDIT_TRAIL
   after update on JOB_SCHEDULES for each row

declare
   changed_from	varchar2(2000);
   changed_to	varchar2(2000);

   procedure APPEND_TO_CHANGED_FROM(old_value varchar2) is
   begin
      if LENGTH(changed_from) = 0 then
         changed_from := '<?xml version="1.0" encoding="ISO-8859-1"?>';
      end if;
      changed_from := changed_from || old_value;
   end;

   procedure APPEND_TO_CHANGED_TO(new_value varchar2) is
   begin
      if LENGTH(changed_to) = 0 then
         changed_to := '<?xml version="1.0" encoding="ISO-8859-1"?>';
      end if;
      changed_to := changed_to || new_value;
   end;

begin
      if nvl(:OLD.SCHED_TYPE, '') != nvl(:NEW.SCHED_TYPE, '') then
         APPEND_TO_CHANGED_FROM('<Type>' || :OLD.SCHED_TYPE || '</Type>');
         APPEND_TO_CHANGED_TO('<Type>' || :NEW.SCHED_TYPE || '</Type>');
      end if;
      if nvl(:OLD.SCHED_DESC, '') != nvl(:NEW.SCHED_DESC, '') then
         APPEND_TO_CHANGED_FROM('<Description>' || :OLD.SCHED_DESC || '</Description>');
         APPEND_TO_CHANGED_TO('<Description>' || :NEW.SCHED_DESC || '</Description>');
      end if;
      if nvl(:OLD.SCHED_TIME, '') != nvl(:NEW.SCHED_TIME, '') then
         APPEND_TO_CHANGED_FROM('<Time>' || to_char(:OLD.SCHED_TIME) || '</Time>');
         APPEND_TO_CHANGED_TO('<Time>' || to_char(:NEW.SCHED_TIME) || '</Time>');
      end if;
      if nvl(:OLD.CREATED_BY, '') != nvl(:NEW.CREATED_BY, '') then
         APPEND_TO_CHANGED_FROM('<CreatedBy>' || :OLD.CREATED_BY || '</CreatedBy>');
         APPEND_TO_CHANGED_TO('<CreatedBy>' || :NEW.CREATED_BY || '</CreatedBy>');
      end if;
      if nvl(:OLD.CREATED_DATE, '') != nvl(:NEW.CREATED_DATE, '') then
         APPEND_TO_CHANGED_FROM('<CreatedDate>' || to_char(:OLD.CREATED_DATE) || '</CreatedDate>');
         APPEND_TO_CHANGED_TO('<CreatedDate>' || to_char(:NEW.CREATED_DATE) || '</CreatedDate>');
      end if;
      if nvl(:OLD.UPDATED_BY, '') != nvl(:NEW.UPDATED_BY, '') then
         APPEND_TO_CHANGED_FROM('<UpdatedBy>' || :OLD.UPDATED_BY || '</UpdatedBy>');
         APPEND_TO_CHANGED_TO('<UpdatedBy>' || :NEW.UPDATED_BY || '</UpdatedBy>');
      end if;
      if nvl(:OLD.UPDATED_DATE, '') != nvl(:NEW.UPDATED_DATE, '') then
         APPEND_TO_CHANGED_FROM('<UpdatedDate>' || to_char(:OLD.UPDATED_DATE) || '</UpdatedDate>');
         APPEND_TO_CHANGED_TO('<UpdatedDate>' || to_char(:NEW.UPDATED_DATE) || '</UpdatedDate>');
      end if;

      if LENGTH(changed_from) > 0 then
         if :OLD.SCHED_TYPE = 'ME' then
            PROC_CREATE_AUDIT_TRAIL(37, 'UPDATE', :NEW.UPDATED_BY, :NEW.SCHED_TYPE, changed_from, changed_to);
         elsif :OLD.SCHED_TYPE = 'AE' then
            PROC_CREATE_AUDIT_TRAIL(38, 'UPDATE', :NEW.UPDATED_BY, :NEW.SCHED_TYPE, changed_from, changed_to);
         end if;
      end if;
end;
/

PROMPT CREATING TGR_SEC_AUDIT_TRAIL
create or replace trigger TGR_SEC_AUDIT_TRAIL
   after update or delete on SECTIONS for each row

declare
   changed_from	varchar2(2000);
   changed_to	varchar2(2000);

   procedure APPEND_TO_CHANGED_FROM(old_value varchar2) is
   begin
      if LENGTH(changed_from) = 0 then
         changed_from := '<?xml version="1.0" encoding="ISO-8859-1"?>';
      end if;
      changed_from := changed_from || old_value;
   end;

   procedure APPEND_TO_CHANGED_TO(new_value varchar2) is
   begin
      if LENGTH(changed_to) = 0 then
         changed_to := '<?xml version="1.0" encoding="ISO-8859-1"?>';
      end if;
      changed_to := changed_to || new_value;
   end;

begin
   if updating then
      if nvl(:OLD.SEC_CODE, '') != nvl(:NEW.SEC_CODE, '') then
         APPEND_TO_CHANGED_FROM('<SectionID>' || :OLD.SEC_CODE || '</SectionID>');
         APPEND_TO_CHANGED_TO('<SectionID>' || :NEW.SEC_CODE || '</SectionID>');
      end if;
      if nvl(:OLD.SEC_DESC, '') != nvl(:NEW.SEC_DESC, '') then
         APPEND_TO_CHANGED_FROM('<Description>' || :OLD.SEC_DESC || '</Description>');
         APPEND_TO_CHANGED_TO('<Description>' || :NEW.SEC_DESC || '</Description>');
      end if;
      if nvl(:OLD.CREATED_BY, '') != nvl(:NEW.CREATED_BY, '') then
         APPEND_TO_CHANGED_FROM('<CreatedBy>' || :OLD.CREATED_BY || '</CreatedBy>');
         APPEND_TO_CHANGED_TO('<CreatedBy>' || :NEW.CREATED_BY || '</CreatedBy>');
      end if;
      if nvl(:OLD.CREATED_DATE, '') != nvl(:NEW.CREATED_DATE, '') then
         APPEND_TO_CHANGED_FROM('<CreatedDate>' || to_char(:OLD.CREATED_DATE) || '</CreatedDate>');
         APPEND_TO_CHANGED_TO('<CreatedDate>' || to_char(:NEW.CREATED_DATE) || '</CreatedDate>');
      end if;
      if nvl(:OLD.UPDATED_BY, '') != nvl(:NEW.UPDATED_BY, '') then
         APPEND_TO_CHANGED_FROM('<UpdatedBy>' || :OLD.UPDATED_BY || '</UpdatedBy>');
         APPEND_TO_CHANGED_TO('<UpdatedBy>' || :NEW.UPDATED_BY || '</UpdatedBy>');
      end if;
      if nvl(:OLD.UPDATED_DATE, '') != nvl(:NEW.UPDATED_DATE, '') then
         APPEND_TO_CHANGED_FROM('<UpdatedDate>' || to_char(:OLD.UPDATED_DATE) || '</UpdatedDate>');
         APPEND_TO_CHANGED_TO('<UpdatedDate>' || to_char(:NEW.UPDATED_DATE) || '</UpdatedDate>');
      end if;

      if LENGTH(changed_from) > 0 then
         PROC_CREATE_AUDIT_TRAIL(11, 'UPDATE', :NEW.UPDATED_BY, :NEW.SEC_CODE, changed_from, changed_to);
      end if;
   elsif deleting then
      PROC_CREATE_AUDIT_TRAIL(11, 'DELETE', :OLD.UPDATED_BY, :OLD.SEC_CODE, changed_from, changed_to);
   end if;
end;
/

PROMPT CREATING TGR_SLO_AUDIT_TRAIL
create or replace trigger TGR_SLO_AUDIT_TRAIL
   after update or delete on SUNLIFE_OFFICES for each row

declare
   changed_from	varchar2(2000);
   changed_to	varchar2(2000);

   procedure APPEND_TO_CHANGED_FROM(old_value varchar2) is
   begin
      if LENGTH(changed_from) = 0 then
         changed_from := '<?xml version="1.0" encoding="ISO-8859-1"?>';
      end if;
      changed_from := changed_from || old_value;
   end;

   procedure APPEND_TO_CHANGED_TO(new_value varchar2) is
   begin
      if LENGTH(changed_to) = 0 then
         changed_to := '<?xml version="1.0" encoding="ISO-8859-1"?>';
      end if;
      changed_to := changed_to || new_value;
   end;

begin
   if updating then
      if nvl(:OLD.SLO_OFFICE_CODE, '') != nvl(:NEW.SLO_OFFICE_CODE, '') then
         APPEND_TO_CHANGED_FROM('<SunLifeOfficeID>' || :OLD.SLO_OFFICE_CODE || '</SunLifeOfficeID>');
         APPEND_TO_CHANGED_TO('<SunLifeOfficeID>' || :NEW.SLO_OFFICE_CODE || '</SunLifeOfficeID>');
      end if;
      if nvl(:OLD.SLO_OFFICE_NAME, '') != nvl(:NEW.SLO_OFFICE_NAME, '') then
         APPEND_TO_CHANGED_FROM('<OfficeName>' || :OLD.SLO_OFFICE_NAME || '</OfficeName>');
         APPEND_TO_CHANGED_TO('<OfficeName>' || :NEW.SLO_OFFICE_NAME || '</OfficeName>');
      end if;
      if nvl(:OLD.SLO_TYPE, '') != nvl(:NEW.SLO_TYPE, '') then
         APPEND_TO_CHANGED_FROM('<Type>' || :OLD.SLO_TYPE || '</Type>');
         APPEND_TO_CHANGED_TO('<Type>' || :NEW.SLO_TYPE || '</Type>');
      end if;
      if nvl(:OLD.CREATED_BY, '') != nvl(:NEW.CREATED_BY, '') then
         APPEND_TO_CHANGED_FROM('<CreatedBy>' || :OLD.CREATED_BY || '</CreatedBy>');
         APPEND_TO_CHANGED_TO('<CreatedBy>' || :NEW.CREATED_BY || '</CreatedBy>');
      end if;
      if nvl(:OLD.CREATED_DATE, '') != nvl(:NEW.CREATED_DATE, '') then
         APPEND_TO_CHANGED_FROM('<CreatedDate>' || to_char(:OLD.CREATED_DATE) || '</CreatedDate>');
         APPEND_TO_CHANGED_TO('<CreatedDate>' || to_char(:NEW.CREATED_DATE) || '</CreatedDate>');
      end if;
      if nvl(:OLD.UPDATED_BY, '') != nvl(:NEW.UPDATED_BY, '') then
         APPEND_TO_CHANGED_FROM('<UpdatedBy>' || :OLD.UPDATED_BY || '</UpdatedBy>');
         APPEND_TO_CHANGED_TO('<UpdatedBy>' || :NEW.UPDATED_BY || '</UpdatedBy>');
      end if;
      if nvl(:OLD.UPDATED_DATE, '') != nvl(:NEW.UPDATED_DATE, '') then
         APPEND_TO_CHANGED_FROM('<UpdatedDate>' || to_char(:OLD.UPDATED_DATE) || '</UpdatedDate>');
         APPEND_TO_CHANGED_TO('<UpdatedDate>' || to_char(:NEW.UPDATED_DATE) || '</UpdatedDate>');
      end if;

      if LENGTH(changed_from) > 0 then
         PROC_CREATE_AUDIT_TRAIL(24, 'UPDATE', :NEW.UPDATED_BY, :NEW.SLO_OFFICE_CODE, changed_from, changed_to);
      end if;
   elsif deleting then
      PROC_CREATE_AUDIT_TRAIL(24, 'DELETE', :OLD.UPDATED_BY, :OLD.SLO_OFFICE_CODE, changed_from, changed_to);
   end if;
end;
/

PROMPT CREATING TGR_SPL_AUDIT_TRAIL
create or replace trigger TGR_SPL_AUDIT_TRAIL
   after update or delete on SPECIALIZATIONS for each row

declare
   changed_from	varchar2(2000);
   changed_to	varchar2(2000);

   procedure APPEND_TO_CHANGED_FROM(old_value varchar2) is
   begin
      if LENGTH(changed_from) = 0 then
         changed_from := '<?xml version="1.0" encoding="ISO-8859-1"?>';
      end if;
      changed_from := changed_from || old_value;
   end;

   procedure APPEND_TO_CHANGED_TO(new_value varchar2) is
   begin
      if LENGTH(changed_to) = 0 then
         changed_to := '<?xml version="1.0" encoding="ISO-8859-1"?>';
      end if;
      changed_to := changed_to || new_value;
   end;

begin
   if updating then
      if nvl(:OLD.SPL_ID, 0) != nvl(:NEW.SPL_ID, 0) then
         APPEND_TO_CHANGED_FROM('<SpecializationID>' || to_char(:OLD.SPL_ID) || '</SpecializationID>');
         APPEND_TO_CHANGED_TO('<SpecializationID>' || to_char(:NEW.SPL_ID) || '</SpecializationID>');
      end if;
      if nvl(:OLD.CREATED_BY, '') != nvl(:NEW.CREATED_BY, '') then
         APPEND_TO_CHANGED_FROM('<CreatedBy>' || :OLD.CREATED_BY || '</CreatedBy>');
         APPEND_TO_CHANGED_TO('<CreatedBy>' || :NEW.CREATED_BY || '</CreatedBy>');
      end if;
      if nvl(:OLD.CREATED_DATE, '') != nvl(:NEW.CREATED_DATE, '') then
         APPEND_TO_CHANGED_FROM('<CreatedDate>' || to_char(:OLD.CREATED_DATE) || '</CreatedDate>');
         APPEND_TO_CHANGED_TO('<CreatedDate>' || to_char(:NEW.CREATED_DATE) || '</CreatedDate>');
      end if;
      if nvl(:OLD.UPDATED_BY, '') != nvl(:NEW.UPDATED_BY, '') then
         APPEND_TO_CHANGED_FROM('<UpdatedBy>' || :OLD.UPDATED_BY || '</UpdatedBy>');
         APPEND_TO_CHANGED_TO('<UpdatedBy>' || :NEW.UPDATED_BY || '</UpdatedBy>');
      end if;
      if nvl(:OLD.UPDATED_DATE, '') != nvl(:NEW.UPDATED_DATE, '') then
         APPEND_TO_CHANGED_FROM('<UpdatedDate>' || to_char(:OLD.UPDATED_DATE) || '</UpdatedDate>');
         APPEND_TO_CHANGED_TO('<UpdatedDate>' || to_char(:NEW.UPDATED_DATE) || '</UpdatedDate>');
      end if;

      if LENGTH(changed_from) > 0 then
         PROC_CREATE_AUDIT_TRAIL(12, 'UPDATE', :NEW.UPDATED_BY, to_char(:NEW.SPL_ID), changed_from, changed_to);
      end if;
   elsif deleting then
      PROC_CREATE_AUDIT_TRAIL(12, 'DELETE', :OLD.UPDATED_BY, to_char(:OLD.SPL_ID), changed_from, changed_to);
   end if;
end;
/

PROMPT CREATING TGR_STAT_AUDIT_TRAIL
create or replace trigger TGR_STAT_AUDIT_TRAIL
   after update or delete on STATUS for each row

declare
   changed_from	varchar2(2000);
   changed_to	varchar2(2000);

   procedure APPEND_TO_CHANGED_FROM(old_value varchar2) is
   begin
      if LENGTH(changed_from) = 0 then
         changed_from := '<?xml version="1.0" encoding="ISO-8859-1"?>';
      end if;
      changed_from := changed_from || old_value;
   end;

   procedure APPEND_TO_CHANGED_TO(new_value varchar2) is
   begin
      if LENGTH(changed_to) = 0 then
         changed_to := '<?xml version="1.0" encoding="ISO-8859-1"?>';
      end if;
      changed_to := changed_to || new_value;
   end;

begin
   if updating then
      if nvl(:OLD.STAT_ID, 0) != nvl(:NEW.STAT_ID, 0) then
         APPEND_TO_CHANGED_FROM('<StatusID>' || to_char(:OLD.STAT_ID) || '</StatusID>');
         APPEND_TO_CHANGED_TO('<StatusID>' || to_char(:NEW.STAT_ID) || '</StatusID>');
      end if;
      if nvl(:OLD.STAT_DESC, '') != nvl(:NEW.STAT_DESC, '') then
         APPEND_TO_CHANGED_FROM('<StatusDescription>' || :OLD.STAT_DESC || '</StatusDescription>');
         APPEND_TO_CHANGED_TO('<StatusDescription>' || :NEW.STAT_DESC || '</StatusDescription>');
      end if;
      if nvl(:OLD.STAT_TYPE, '') != nvl(:NEW.STAT_TYPE, '') then
         APPEND_TO_CHANGED_FROM('<StatusType>' || :OLD.STAT_TYPE || '</StatusType>');
         APPEND_TO_CHANGED_TO('<StatusType>' || :NEW.STAT_TYPE || '</StatusType>');
      end if;
      if nvl(:OLD.CREATED_BY, '') != nvl(:NEW.CREATED_BY, '') then
         APPEND_TO_CHANGED_FROM('<CreatedBy>' || :OLD.CREATED_BY || '</CreatedBy>');
         APPEND_TO_CHANGED_TO('<CreatedBy>' || :NEW.CREATED_BY || '</CreatedBy>');
      end if;
      if nvl(:OLD.CREATED_DATE, '') != nvl(:NEW.CREATED_DATE, '') then
         APPEND_TO_CHANGED_FROM('<CreatedDate>' || to_char(:OLD.CREATED_DATE) || '</CreatedDate>');
         APPEND_TO_CHANGED_TO('<CreatedDate>' || to_char(:NEW.CREATED_DATE) || '</CreatedDate>');
      end if;
      if nvl(:OLD.UPDATED_BY, '') != nvl(:NEW.UPDATED_BY, '') then
         APPEND_TO_CHANGED_FROM('<UpdatedBy>' || :OLD.UPDATED_BY || '</UpdatedBy>');
         APPEND_TO_CHANGED_TO('<UpdatedBy>' || :NEW.UPDATED_BY || '</UpdatedBy>');
      end if;
      if nvl(:OLD.UPDATED_DATE, '') != nvl(:NEW.UPDATED_DATE, '') then
         APPEND_TO_CHANGED_FROM('<UpdatedDate>' || to_char(:OLD.UPDATED_DATE) || '</UpdatedDate>');
         APPEND_TO_CHANGED_TO('<UpdatedDate>' || to_char(:NEW.UPDATED_DATE) || '</UpdatedDate>');
      end if;

      if LENGTH(changed_from) > 0 then
         PROC_CREATE_AUDIT_TRAIL(19, 'UPDATE', :NEW.UPDATED_BY, to_char(:NEW.STAT_ID), changed_from, changed_to);
      end if;
   elsif deleting then
      PROC_CREATE_AUDIT_TRAIL(19, 'DELETE', :OLD.UPDATED_BY, to_char(:OLD.STAT_ID), changed_from, changed_to);
   end if;
end;
/

PROMPT CREATING TGR_TEMPLT_AUDIT_TRAIL 
create or replace trigger TGR_TEMPLT_AUDIT_TRAIL 
   after update or delete on ACCESS_TEMPLATES for each row

declare
   changed_from	varchar2(2000);
   changed_to	varchar2(2000);

   procedure APPEND_TO_CHANGED_FROM(old_value varchar2) is
   begin
      if LENGTH(changed_from) = 0 then
         changed_from := '<?xml version="1.0" encoding="ISO-8859-1"?>';
      end if;
      changed_from := changed_from || old_value;
   end;

   procedure APPEND_TO_CHANGED_TO(new_value varchar2) is
   begin
      if LENGTH(changed_to) = 0 then
         changed_to := '<?xml version="1.0" encoding="ISO-8859-1"?>';
      end if;
      changed_to := changed_to || new_value;
   end;

begin
   if updating then
      if nvl(:OLD.TEMPLT_ID, 0) != nvl(:NEW.TEMPLT_ID, 0) then
         APPEND_TO_CHANGED_FROM('<TemplateCode>' || to_char(:OLD.TEMPLT_ID) || '</TemplateCode>');
         APPEND_TO_CHANGED_TO('<TemplateCode>' || to_char(:NEW.TEMPLT_ID) || '</TemplateCode>');
      end if;
      if nvl(:OLD.TEMPLT_DESC, '') != nvl(:NEW.TEMPLT_DESC, '') then
         APPEND_TO_CHANGED_FROM('<TemplateDescription>' || :OLD.TEMPLT_DESC || '</TemplateDescription>');
         APPEND_TO_CHANGED_TO('<TemplateDescription>' || :NEW.TEMPLT_DESC || '</TemplateDescription>');
      end if;
      if nvl(:OLD.ROLE_ID, '') != nvl(:NEW.ROLE_ID, '') then
         APPEND_TO_CHANGED_FROM('<RoleCode>' || :OLD.ROLE_ID || '</RoleCode>');
         APPEND_TO_CHANGED_TO('<RoleCode>' || :NEW.ROLE_ID || '</RoleCode>');
      end if;
      if nvl(:OLD.CREATED_BY, '') != nvl(:NEW.CREATED_BY, '') then
         APPEND_TO_CHANGED_FROM('<CreatedBy>' || :OLD.CREATED_BY || '</CreatedBy>');
         APPEND_TO_CHANGED_TO('<CreatedBy>' || :NEW.CREATED_BY || '</CreatedBy>');
      end if;
      if nvl(:OLD.CREATED_DATE, '') != nvl(:NEW.CREATED_DATE, '') then
         APPEND_TO_CHANGED_FROM('<CreatedDate>' || to_char(:OLD.CREATED_DATE) || '</CreatedDate>');
         APPEND_TO_CHANGED_TO('<CreatedDate>' || to_char(:NEW.CREATED_DATE) || '</CreatedDate>');
      end if;
      if nvl(:OLD.UPDATED_BY, '') != nvl(:NEW.UPDATED_BY, '') then
         APPEND_TO_CHANGED_FROM('<UpdatedBy>' || :OLD.UPDATED_BY || '</UpdatedBy>');
         APPEND_TO_CHANGED_TO('<UpdatedBy>' || :NEW.UPDATED_BY || '</UpdatedBy>');
      end if;
      if nvl(:OLD.UPDATED_DATE, '') != nvl(:NEW.UPDATED_DATE, '') then
         APPEND_TO_CHANGED_FROM('<UpdatedDate>' || to_char(:OLD.UPDATED_DATE) || '</UpdatedDate>');
         APPEND_TO_CHANGED_TO('<UpdatedDate>' || to_char(:NEW.UPDATED_DATE) || '</UpdatedDate>');
      end if;

      if LENGTH(changed_from) > 0 then
         PROC_CREATE_AUDIT_TRAIL(42, 'UPDATE', :NEW.UPDATED_BY, to_char(:NEW.TEMPLT_ID), changed_from, changed_to);
      end if;
   elsif deleting then
      PROC_CREATE_AUDIT_TRAIL(42, 'DELETE', :OLD.UPDATED_BY, to_char(:OLD.TEMPLT_ID), changed_from, changed_to);
   end if;
end;
/

PROMPT CREATING TGR_TEST_AUDIT_TRAIL
create or replace trigger TGR_TEST_AUDIT_TRAIL
   after update or delete on TEST_PROFILES for each row

declare
   changed_from	varchar2(2000);
   changed_to	varchar2(2000);

   procedure APPEND_TO_CHANGED_FROM(old_value varchar2) is
   begin
      if LENGTH(changed_from) = 0 then
         changed_from := '<?xml version="1.0" encoding="ISO-8859-1"?>';
      end if;
      changed_from := changed_from || old_value;
   end;

   procedure APPEND_TO_CHANGED_TO(new_value varchar2) is
   begin
      if LENGTH(changed_to) = 0 then
         changed_to := '<?xml version="1.0" encoding="ISO-8859-1"?>';
      end if;
      changed_to := changed_to || new_value;
   end;

begin
   if updating then
      if nvl(:OLD.TEST_ID, 0) != nvl(:NEW.TEST_ID, 0) then
         APPEND_TO_CHANGED_FROM('<TestID>' || to_char(:OLD.TEST_ID) || '</TestID>');
         APPEND_TO_CHANGED_TO('<TestID>' || to_char(:NEW.TEST_ID) || '</TestID>');
      end if;
      if nvl(:OLD.TEST_VALIDITY, 0) != nvl(:NEW.TEST_VALIDITY, 0) then
         APPEND_TO_CHANGED_FROM('<Validity>' || to_char(:OLD.TEST_VALIDITY) || '</Validity>');
         APPEND_TO_CHANGED_TO('<Validity>' || to_char(:NEW.TEST_VALIDITY) || '</Validity>');
      end if;
      if nvl(:OLD.TEST_TAXABLE_IND, '') != nvl(:NEW.TEST_TAXABLE_IND, '') then
         APPEND_TO_CHANGED_FROM('<TaxableIndicator>' || :OLD.TEST_TAXABLE_IND || '</TaxableIndicator>');
         APPEND_TO_CHANGED_TO('<TaxableIndicator>' || :NEW.TEST_TAXABLE_IND || '</TaxableIndicator>');
      end if;
      if nvl(:OLD.CREATED_BY, '') != nvl(:NEW.CREATED_BY, '') then
         APPEND_TO_CHANGED_FROM('<CreatedBy>' || :OLD.CREATED_BY || '</CreatedBy>');
         APPEND_TO_CHANGED_TO('<CreatedBy>' || :NEW.CREATED_BY || '</CreatedBy>');
      end if;
      if nvl(:OLD.CREATED_DATE, '') != nvl(:NEW.CREATED_DATE, '') then
         APPEND_TO_CHANGED_FROM('<CreatedDate>' || to_char(:OLD.CREATED_DATE) || '</CreatedDate>');
         APPEND_TO_CHANGED_TO('<CreatedDate>' || to_char(:NEW.CREATED_DATE) || '</CreatedDate>');
      end if;
      if nvl(:OLD.UPDATED_BY, '') != nvl(:NEW.UPDATED_BY, '') then
         APPEND_TO_CHANGED_FROM('<UpdatedBy>' || :OLD.UPDATED_BY || '</UpdatedBy>');
         APPEND_TO_CHANGED_TO('<UpdatedBy>' || :NEW.UPDATED_BY || '</UpdatedBy>');
      end if;
      if nvl(:OLD.UPDATED_DATE, '') != nvl(:NEW.UPDATED_DATE, '') then
         APPEND_TO_CHANGED_FROM('<UpdatedDate>' || to_char(:OLD.UPDATED_DATE) || '</UpdatedDate>');
         APPEND_TO_CHANGED_TO('<UpdatedDate>' || to_char(:NEW.UPDATED_DATE) || '</UpdatedDate>');
      end if;

      if LENGTH(changed_from) > 0 then
         PROC_CREATE_AUDIT_TRAIL(21, 'UPDATE', :NEW.UPDATED_BY, to_char(:NEW.TEST_ID), changed_from, changed_to);
      end if;
   elsif deleting then
      PROC_CREATE_AUDIT_TRAIL(21, 'DELETE', :OLD.UPDATED_BY, to_char(:OLD.TEST_ID), changed_from, changed_to);
   end if;
end;
/

PROMPT CREATING TGR_TRANSACTION_DETAILS
create or replace trigger TGR_TRANSACTION_DETAILS
	after insert or update or delete on IMPAIRMENTS for each row

declare 
	V_DATE date;
	V_CONTROL_NUMBER varchar2(255);
	V_CTR number;
	V_USER varchar2(10);
	
	function GET_DATE_DIFF (DATE1 date, DATE2 date) return number is 
	V_RESULT number;
	begin
		V_RESULT := trunc(DATE1) - trunc(DATE2);
		return round(V_RESULT, 0);
	end;
	
	function GET_CONTROL_NO (IMP_DATE date, TD_DATE date) return number is
		V_DIFF number;
		V_TEMP number;
		V_COUNTER number;
	begin 
		V_DIFF := GET_DATE_DIFF(IMP_DATE, TD_DATE);
		if V_DIFF = 0 then
			select max(counter) into V_COUNTER from TD_COUNTER; /* Initially the table (TD_COUNTER) must have an initial value of 1*/
			V_TEMP := V_COUNTER + 1;
			insert into TD_COUNTER(counter, last_date) values (V_TEMP, V_DATE);
			select max(counter) into V_COUNTER from TD_COUNTER;
		else 
			delete from TD_COUNTER;
			V_COUNTER := 1;
			insert into TD_COUNTER  (counter, last_date) values (V_COUNTER, sysdate);
		end if;
		return V_COUNTER;
	end;
	
	function FORMAT_DATE (INPUT varchar2) return varchar2 is
		V_TEMP varchar2(15);
		V_TEMP2 varchar2(10);
	begin
		V_TEMP := substr(INPUT, 0,3);
		V_TEMP2 := substr(INPUT, 4, 7);
		V_TEMP := replace(V_TEMP, 0);
		V_TEMP2 := V_TEMP || V_TEMP2;
		return V_TEMP2;
	end;
	
begin
	select max(last_date) into V_DATE from td_counter;
	
	if inserting then 
		V_CTR := GET_CONTROL_NO(:new.created_date, V_DATE);
		V_CONTROL_NUMBER := 'A' || FORMAT_DATE(to_char(:new.created_date, 'mmddyyyy')) || to_char(V_CTR); 
		INSERT INTO TRANSACTION_DETAILS (impairment_id, control_no, user_cd, company_cd, tran_cd, tran_dt, record_affected, no_of_hit) 
		VALUES (:new.imp_impairement_id, V_CONTROL_NUMBER, :new.created_by, 'A', '1', sysdate, 1, 0);
	elsif updating then
		if :new.created_date != null then
			V_CTR := GET_CONTROL_NO(:new.updated_date, V_DATE);
			V_CONTROL_NUMBER := 'A' || FORMAT_DATE(to_char(:new.updated_date, 'mmddyyyy')) || to_char(V_CTR); 
			INSERT INTO TRANSACTION_DETAILS (impairment_id, control_no, user_cd, company_cd, tran_cd, tran_dt, record_affected, no_of_hit) 
			VALUES (:new.imp_impairement_id, V_CONTROL_NUMBER, :new.updated_by, 'A', '1', sysdate, 1, 0);
		end if;
	elsif deleting then
		V_CTR := GET_CONTROL_NO(sysdate, V_DATE);
		V_CONTROL_NUMBER := 'A' || FORMAT_DATE(to_char(sysdate, 'mmddyyyy')) || to_char(V_CTR);
		INSERT INTO TRANSACTION_DETAILS (impairment_id, control_no, user_cd, company_cd, tran_cd, tran_dt, record_affected, no_of_hit) 
		VALUES (:old.imp_impairement_id, V_CONTROL_NUMBER, :old.updated_by, 'A', '1', sysdate, 1, 0);
	end if;
end;
/

PROMPT CREATING TGR_UPA_AUDIT_TRAIL 
create or replace trigger TGR_UPA_AUDIT_TRAIL 
   after update or delete on USER_PAGE_ACCESS for each row

declare
   changed_from	varchar2(2000);
   changed_to	varchar2(2000);

   procedure APPEND_TO_CHANGED_FROM(old_value varchar2) is
   begin
      if LENGTH(changed_from) = 0 then
         changed_from := '<?xml version="1.0" encoding="ISO-8859-1"?>';
      end if;
      changed_from := changed_from || old_value;
   end;

   procedure APPEND_TO_CHANGED_TO(new_value varchar2) is
   begin
      if LENGTH(changed_to) = 0 then
         changed_to := '<?xml version="1.0" encoding="ISO-8859-1"?>';
      end if;
      changed_to := changed_to || new_value;
   end;

begin
   if updating then
      if nvl(:OLD.USER_ID, '') != nvl(:NEW.USER_ID, '') then
         APPEND_TO_CHANGED_FROM('<UserID>' || :OLD.USER_ID || '</UserID>');
         APPEND_TO_CHANGED_TO('<UserID>' || :NEW.USER_ID || '</UserID>');
      end if;
      if nvl(:OLD.PAGE_ID, 0) != nvl(:NEW.PAGE_ID, 0) then
         APPEND_TO_CHANGED_FROM('<PageID>' || to_char(:OLD.PAGE_ID) || '</PageID>');
         APPEND_TO_CHANGED_TO('<PageID>' || to_char(:NEW.PAGE_ID) || '</PageID>');
      end if;
      if nvl(:OLD.ACC_ID, 0) != nvl(:NEW.ACC_ID, 0) then
         APPEND_TO_CHANGED_FROM('<AccessID>' || to_char(:OLD.ACC_ID) || '</AccessID>');
         APPEND_TO_CHANGED_TO('<AccessID>' || to_char(:NEW.ACC_ID) || '</AccessID>');
      end if;
      if nvl(:OLD.UPA_ACCESS_IND, '') != nvl(:NEW.UPA_ACCESS_IND, '') then
         APPEND_TO_CHANGED_FROM('<AccessIndicator>' || :OLD.UPA_ACCESS_IND || '</AccessIndicator>');
         APPEND_TO_CHANGED_TO('<AccessIndicator>' || :NEW.UPA_ACCESS_IND || '</AccessIndicator>');
      end if;
      if nvl(:OLD.CREATED_BY, '') != nvl(:NEW.CREATED_BY, '') then
         APPEND_TO_CHANGED_FROM('<CreatedBy>' || :OLD.CREATED_BY || '</CreatedBy>');
         APPEND_TO_CHANGED_TO('<CreatedBy>' || :NEW.CREATED_BY || '</CreatedBy>');
      end if;
      if nvl(:OLD.CREATED_DATE, '') != nvl(:NEW.CREATED_DATE, '') then
         APPEND_TO_CHANGED_FROM('<CreatedDate>' || to_char(:OLD.CREATED_DATE) || '</CreatedDate>');
         APPEND_TO_CHANGED_TO('<CreatedDate>' || to_char(:NEW.CREATED_DATE) || '</CreatedDate>');
      end if;
      if nvl(:OLD.UPDATED_BY, '') != nvl(:NEW.UPDATED_BY, '') then
         APPEND_TO_CHANGED_FROM('<UpdatedBy>' || :OLD.UPDATED_BY || '</UpdatedBy>');
         APPEND_TO_CHANGED_TO('<UpdatedBy>' || :NEW.UPDATED_BY || '</UpdatedBy>');
      end if;
      if nvl(:OLD.UPDATED_DATE, '') != nvl(:NEW.UPDATED_DATE, '') then
         APPEND_TO_CHANGED_FROM('<UpdatedDate>' || to_char(:OLD.UPDATED_DATE) || '</UpdatedDate>');
         APPEND_TO_CHANGED_TO('<UpdatedDate>' || to_char(:NEW.UPDATED_DATE) || '</UpdatedDate>');
      end if;

      if LENGTH(changed_from) > 0 then
         PROC_CREATE_AUDIT_TRAIL(6, 'UPDATE', :NEW.UPDATED_BY, :NEW.USER_ID || '-' || to_char(:NEW.PAGE_ID) || '-' || to_char(:NEW.ACC_ID), changed_from, changed_to);
      end if;
   elsif deleting then
      PROC_CREATE_AUDIT_TRAIL(6, 'DELETE', :OLD.UPDATED_BY, :OLD.USER_ID || '-' || to_char(:OLD.PAGE_ID) || '-' || to_char(:OLD.ACC_ID), changed_from, changed_to);
   end if;
end;
/

PROMPT CREATING TGR_USER_AUDIT_TRAIL 
create or replace trigger TGR_USER_AUDIT_TRAIL 
   after update or delete on USERS for each row

declare
   changed_from	varchar2(2000);
   changed_to	varchar2(2000);

   procedure APPEND_TO_CHANGED_FROM(old_value varchar2) is
   begin
      if LENGTH(changed_from) = 0 then
         changed_from := '<?xml version="1.0" encoding="ISO-8859-1"?>';
      end if;
      changed_from := changed_from || old_value;
   end;

   procedure APPEND_TO_CHANGED_TO(new_value varchar2) is
   begin
      if LENGTH(changed_to) = 0 then
         changed_to := '<?xml version="1.0" encoding="ISO-8859-1"?>';
      end if;
      changed_to := changed_to || new_value;
   end;

begin
   if updating then
      if nvl(:OLD.USER_ID, '') != nvl(:NEW.USER_ID, '') then
         APPEND_TO_CHANGED_FROM('<UserID>' || :OLD.USER_ID || '</UserID>');
         APPEND_TO_CHANGED_TO('<UserID>' || :NEW.USER_ID || '</UserID>');
      end if;
      if nvl(:OLD.USR_ACF2ID, '') != nvl(:NEW.USR_ACF2ID, '') then
         APPEND_TO_CHANGED_FROM('<ACF2ID>' || :OLD.USR_ACF2ID || '</ACF2ID>');
         APPEND_TO_CHANGED_TO('<ACF2ID>' || :NEW.USR_ACF2ID || '</ACF2ID>');
      end if;
      if nvl(:OLD.USR_CODE, '') != nvl(:NEW.USR_CODE, '') then
         APPEND_TO_CHANGED_FROM('<UserCode>' || :OLD.USR_CODE || '</UserCode>');
         APPEND_TO_CHANGED_TO('<UserCode>' || :NEW.USR_CODE || '</UserCode>');
      end if;
      if nvl(:OLD.USR_TYPE, '') != nvl(:NEW.USR_TYPE, '') then
         APPEND_TO_CHANGED_FROM('<UserType>' || :OLD.USR_TYPE || '</UserType>');
         APPEND_TO_CHANGED_TO('<UserType>' || :NEW.USR_TYPE || '</UserType>');
      end if;
      if nvl(:OLD.USR_ROLE, '') != nvl(:NEW.USR_ROLE, '') then
         APPEND_TO_CHANGED_FROM('<Role>' || :OLD.USR_ROLE || '</Role>');
         APPEND_TO_CHANGED_TO('<Role>' || :NEW.USR_ROLE || '</Role>');
      end if;
      if nvl(:OLD.SLO_ID, '') != nvl(:NEW.SLO_ID, '') then
         APPEND_TO_CHANGED_FROM('<OfficeCode>' || :OLD.SLO_ID || '</OfficeCode>');
         APPEND_TO_CHANGED_TO('<OfficeCode>' || :NEW.SLO_ID || '</OfficeCode>');
      end if;
      if nvl(:OLD.DEPT_ID, '') != nvl(:NEW.DEPT_ID, '') then
         APPEND_TO_CHANGED_FROM('<DepartmentCode>' || :OLD.DEPT_ID || '</DepartmentCode>');
         APPEND_TO_CHANGED_TO('<DepartmentCode>' || :NEW.DEPT_ID || '</DepartmentCode>');
      end if;
      if nvl(:OLD.SEC_ID, '') != nvl(:NEW.SEC_ID, '') then
         APPEND_TO_CHANGED_FROM('<SectionCode>' || :OLD.SEC_ID || '</SectionCode>');
         APPEND_TO_CHANGED_TO('<SectionCode>' || :NEW.SEC_ID || '</SectionCode>');
      end if;
      if nvl(:OLD.CREATED_BY, '') != nvl(:NEW.CREATED_BY, '') then
         APPEND_TO_CHANGED_FROM('<CreatedBy>' || :OLD.CREATED_BY || '</CreatedBy>');
         APPEND_TO_CHANGED_TO('<CreatedBy>' || :NEW.CREATED_BY || '</CreatedBy>');
      end if;
      if nvl(:OLD.CREATED_DATE, '') != nvl(:NEW.CREATED_DATE, '') then
         APPEND_TO_CHANGED_FROM('<CreatedDate>' || to_char(:OLD.CREATED_DATE) || '</CreatedDate>');
         APPEND_TO_CHANGED_TO('<CreatedDate>' || to_char(:NEW.CREATED_DATE) || '</CreatedDate>');
      end if;
      if nvl(:OLD.UPDATED_BY, '') != nvl(:NEW.UPDATED_BY, '') then
         APPEND_TO_CHANGED_FROM('<UpdatedBy>' || :OLD.UPDATED_BY || '</UpdatedBy>');
         APPEND_TO_CHANGED_TO('<UpdatedBy>' || :NEW.UPDATED_BY || '</UpdatedBy>');
      end if;
      if nvl(:OLD.UPDATED_DATE, '') != nvl(:NEW.UPDATED_DATE, '') then
         APPEND_TO_CHANGED_FROM('<UpdatedDate>' || to_char(:OLD.UPDATED_DATE) || '</UpdatedDate>');
         APPEND_TO_CHANGED_TO('<UpdatedDate>' || to_char(:NEW.UPDATED_DATE) || '</UpdatedDate>');
      end if;

      if LENGTH(changed_from) > 0 then
         PROC_CREATE_AUDIT_TRAIL(5, 'UPDATE', :NEW.UPDATED_BY, :NEW.USER_ID, changed_from, changed_to);
      end if;
   elsif deleting then
         PROC_CREATE_AUDIT_TRAIL(5, 'DELETE', :OLD.UPDATED_BY, :OLD.USER_ID, changed_from, changed_to);
   end if;
end;
/

PROMPT CREATING ENCRYPTION FUNCTIONS
create or replace function encryptData (encryptThis varchar2, key varchar2) return varchar2 is
-- Use DES to encrypt a VARCHAR
--
-- R. Sherman 
-- 
-- ARGUMENTS
-- 
-- encryptThis       -- Data to be encrypted
-- key        		 -- Key used to encrypt the data
-- return       	 -- The encrypted data 
-- 
-- NOTES
-- 
--       This function appends spaces to the end of the data until its
--       length is a multiple of 8 bytes (required by DES). You may
--       to append them in the beginning, depending on your needs. 
--       A similar requirement exists for the key itself, but in 
--       order to cut down on processing overhead, the key used should
--       already conform to these rules.
--

        theLen number;
        preferredLen number;
        data2 varchar2(50);
begin
          data2:=encryptThis;
          preferredLen:=length(data2)/8+1;
          preferredLen:=trunc(preferredLen)*8;
          
          theLen:=length(data2);
          while (theLen!=preferredLen) 
            loop
               data2:=data2||' ';
               theLen:=length(data2);
            end loop;
          return dbms_obfuscation_toolkit.desencrypt(input_string=>data2,key_string=>key);
end;
/


create or replace function decryptData (data varchar2, key varchar2) return varchar2 is
-- Use DES to decrypt a VARCHAR
--
-- R. Sherman 
-- 
-- ARGUMENTS
-- 
-- data       -- Data to be decrypted
-- key        -- Key used to decrypt the data
-- return     -- The decrypted data 
-- 


begin
   return       dbms_obfuscation_toolkit.desdecrypt(input_string=>data,key_string=>key);
end;
/

-- ROLES
PROMPT INSERT ROLES ................
INSERT INTO roles (role_code, role_desc) VALUES ('GRPNBADMIN', 'GROUP NEW BUSINESS ADMINISTRATOR');
INSERT INTO roles (role_code, role_desc) VALUES ('NBSTAFF', 'NEW BUSINESS STAFF');
INSERT INTO roles (role_code, role_desc) VALUES ('NBREVIEWER', 'NEW BUSINESS REVIEWER');
INSERT INTO roles (role_code, role_desc) VALUES ('NBSUPMGR', 'NEW BUSINESS SUPERVISOR/MANAGER');
INSERT INTO roles (role_code, role_desc) VALUES ('FACILITSUPMGR', 'FACILITATOR SUPERVISOR/MANAGER');
INSERT INTO roles (role_code, role_desc) VALUES ('FACILITATOR', 'UNDERWRITING FACILITATOR');
INSERT INTO roles (role_code, role_desc) VALUES ('UNDERWRITER', 'UNDERWRITER');
INSERT INTO roles (role_code, role_desc) VALUES ('UWTNGSUPMGR', 'UNDERWRITING SUPERVISOR/MANAGER');
INSERT INTO roles (role_code, role_desc) VALUES ('MEDICALADMIN', 'MEDICAL ADMIN STAFF');
INSERT INTO roles (role_code, role_desc) VALUES ('MEDSUPMGR', 'MEDICAL SUPERVISOR/MANAGER/CONSULTANG');
INSERT INTO roles (role_code, role_desc) VALUES ('MEDCONSLT', 'MEDICAL CONSULTANT');
INSERT INTO roles (role_code, role_desc) VALUES ('MKTGSTAFF', 'MARKETING STAFF');
INSERT INTO roles (role_code, role_desc) VALUES ('AGENT', 'AGENTS');
INSERT INTO roles (role_code, role_desc) VALUES ('EXAMINER', 'ACCREDITED DOCTOR/EXAMINER');
INSERT INTO roles (role_code, role_desc) VALUES ('LABSTAFF', 'ACCREDITED LABORATORY STAFF');
INSERT INTO roles (role_code, role_desc) VALUES ('OTHERUSER', 'OTHER DEPARMENT USERS');
INSERT INTO roles (role_code, role_desc) VALUES ('LOVADMIN', 'LIST OF VALUES ADMINISTRATOR');
INSERT INTO roles (role_code, role_desc) VALUES ('SCTYADMIN', 'SECURITY ADMIN');
INSERT INTO roles (role_code, role_desc) VALUES ('SYSOPER', 'SYSTEM OPERATOR');

-- IUMDEV USER
PROMPT INSERT IUMADMIN ....................
INSERT INTO users (user_id, usr_acf2id, usr_password, usr_last_name, usr_middle_name,usr_first_name, usr_email_address,  dept_id, sec_id, created_by, usr_lock_ind, usr_active_ind,usr_role, usr_records_per_view) 
 VALUES ('IUMADMIN', '',ENCRYPTDATA('password', 'IUMDBKEY_06022004'), 'DEVELOPMENT', 'USER', 'IUM', 'IUMADMIN@POINTWEST.COM.PH','QAINFOSEC', 'SYSTEMS', 'IUMADMIN',0,1,'SYSOPER',10);

INSERT INTO users (user_id, usr_acf2id, usr_password, usr_last_name, usr_middle_name,usr_first_name, usr_email_address,  dept_id, sec_id, created_by, usr_lock_ind, usr_active_ind,usr_role, usr_records_per_view) 
 VALUES ('RECORDS', '',ENCRYPTDATA('password',  'IUMDBKEY_06022004'), 'RECORDS SECTION', null, null, null,null, 'SYSTEMS', 'IUMADMIN',0,0,'SYSOPER',10);
 
-- EXAMINATION_AREAS
PROMPT INSERT EXAMINATION AREAS ................
INSERT INTO EXAMINATION_AREAS (EA_ID, EA_DESC) VALUES(SEQ_EXAMINATION_AREA.NEXTVAL, 'MAKATI CITY');
INSERT INTO EXAMINATION_AREAS (EA_ID, EA_DESC) VALUES(SEQ_EXAMINATION_AREA.NEXTVAL, 'PARANAQUE');
INSERT INTO EXAMINATION_AREAS (EA_ID, EA_DESC) VALUES(SEQ_EXAMINATION_AREA.NEXTVAL, 'MANDALUYONG, PHILIPPINES');
INSERT INTO EXAMINATION_AREAS (EA_ID, EA_DESC) VALUES(SEQ_EXAMINATION_AREA.NEXTVAL, 'QUEZON CITY');
INSERT INTO EXAMINATION_AREAS (EA_ID, EA_DESC) VALUES(SEQ_EXAMINATION_AREA.NEXTVAL, 'LAGUNA, PHILIPPINES');
INSERT INTO EXAMINATION_AREAS (EA_ID, EA_DESC) VALUES(SEQ_EXAMINATION_AREA.NEXTVAL, 'PASAY CITY');
INSERT INTO EXAMINATION_AREAS (EA_ID, EA_DESC) VALUES(SEQ_EXAMINATION_AREA.NEXTVAL, 'LAS PINAS');
INSERT INTO EXAMINATION_AREAS (EA_ID, EA_DESC) VALUES(SEQ_EXAMINATION_AREA.NEXTVAL, 'DAVAO, PHILIPPINES');
INSERT INTO EXAMINATION_AREAS (EA_ID, EA_DESC) VALUES(SEQ_EXAMINATION_AREA.NEXTVAL, 'CEBU CITY');
INSERT INTO EXAMINATION_AREAS (EA_ID, EA_DESC) VALUES(SEQ_EXAMINATION_AREA.NEXTVAL, 'BAGUIO CITY, PHILIPPINES');
INSERT INTO EXAMINATION_AREAS (EA_ID, EA_DESC) VALUES(SEQ_EXAMINATION_AREA.NEXTVAL, 'MALABON');
INSERT INTO EXAMINATION_AREAS (EA_ID, EA_DESC) VALUES(SEQ_EXAMINATION_AREA.NEXTVAL, 'VALENZUELA CITY');
INSERT INTO EXAMINATION_AREAS (EA_ID, EA_DESC) VALUES(SEQ_EXAMINATION_AREA.NEXTVAL, 'NAVOTAS');
INSERT INTO EXAMINATION_AREAS (EA_ID, EA_DESC) VALUES(SEQ_EXAMINATION_AREA.NEXTVAL, 'TAGUIG');
INSERT INTO EXAMINATION_AREAS (EA_ID, EA_DESC) VALUES(SEQ_EXAMINATION_AREA.NEXTVAL, 'MANILA, PHILIPPINES');

--EXAMINATION_PLACES
PROMPT INSERT EXAMINATION PLACES ................
INSERT INTO EXAMINATION_PLACES (EP_ID, EP_DESC) VALUES(SEQ_EXAMINATION_PLACE.NEXTVAL, 'Not Stated');
INSERT INTO EXAMINATION_PLACES (EP_ID, EP_DESC) VALUES(SEQ_EXAMINATION_PLACE.NEXTVAL, 'Business');
INSERT INTO EXAMINATION_PLACES (EP_ID, EP_DESC) VALUES(SEQ_EXAMINATION_PLACE.NEXTVAL, 'Home');
INSERT INTO EXAMINATION_PLACES (EP_ID, EP_DESC) VALUES(SEQ_EXAMINATION_PLACE.NEXTVAL, 'Company''s/Doctor''s Room');
INSERT INTO EXAMINATION_PLACES (EP_ID, EP_DESC) VALUES(SEQ_EXAMINATION_PLACE.NEXTVAL, 'st. luke''s medical center');

--RANKS
PROMPT INSERT RANKS ................
INSERT INTO RANKS (RANK_ID, RANK_DESC, RANK_FEE) VALUES(SEQ_RANK.NEXTVAL, 'TEST DESCRIPTIONS', 12345);
INSERT INTO RANKS (RANK_ID, RANK_DESC, RANK_FEE) VALUES(SEQ_RANK.NEXTVAL, 'REGULAR NON-MOBILE', 350);
INSERT INTO RANKS (RANK_ID, RANK_DESC, RANK_FEE) VALUES(SEQ_RANK.NEXTVAL, 'REGULAR MOBILE', 400);
INSERT INTO RANKS (RANK_ID, RANK_DESC, RANK_FEE) VALUES(SEQ_RANK.NEXTVAL, 'PREFERRED NON-MOBILE', 400);
INSERT INTO RANKS (RANK_ID, RANK_DESC, RANK_FEE) VALUES(SEQ_RANK.NEXTVAL, 'PREFERRED MOBILE', 450);
INSERT INTO RANKS (RANK_ID, RANK_DESC, RANK_FEE) VALUES(SEQ_RANK.NEXTVAL, 'Examiner 1', -2);
INSERT INTO RANKS (RANK_ID, RANK_DESC, RANK_FEE) VALUES(SEQ_RANK.NEXTVAL, 'TEST DESCRIPTIONS', 9686489.36);
INSERT INTO RANKS (RANK_ID, RANK_DESC, RANK_FEE) VALUES(SEQ_RANK.NEXTVAL, 'SPECIAL LIMITED', 500);

--SPECIALIZATIONS
PROMPT INSERT SPECIALIZATIONS  ................
INSERT INTO SPECIALIZATIONS (SPL_ID, SPL_DESC) VALUES(SEQ_SPECIALIZATION.NEXTVAL, 'PEDIATRICS');
INSERT INTO SPECIALIZATIONS (SPL_ID, SPL_DESC) VALUES(SEQ_SPECIALIZATION.NEXTVAL, 'OBSTETRICIAN GYNECOLOGY');

--HOLIDAYS
PROMPT INSERT HOLIDAYS ................
INSERT INTO HOLIDAYS (HOL_ID, HOL_DATE, HOL_DESC) VALUES(SEQ_HOLIDAY.NEXTVAL, '01-JAN-04', 'New Year''s Day');
INSERT INTO HOLIDAYS (HOL_ID, HOL_DATE, HOL_DESC) VALUES(SEQ_HOLIDAY.NEXTVAL, '25-DEC-04', 'Christmas Day');
INSERT INTO HOLIDAYS (HOL_ID, HOL_DATE, HOL_DESC) VALUES(SEQ_HOLIDAY.NEXTVAL, '09-APR-04', 'Bataan day');
INSERT INTO HOLIDAYS (HOL_ID, HOL_DATE, HOL_DESC) VALUES(SEQ_HOLIDAY.NEXTVAL, '01-MAY-04', 'labor day');
INSERT INTO HOLIDAYS (HOL_ID, HOL_DATE, HOL_DESC) VALUES(SEQ_HOLIDAY.NEXTVAL, '30-NOV-04', 'bonifacio day');
INSERT INTO HOLIDAYS (HOL_ID, HOL_DATE, HOL_DESC) VALUES(SEQ_HOLIDAY.NEXTVAL, '25-FEB-04', 'edsa day');
INSERT INTO HOLIDAYS (HOL_ID, HOL_DATE, HOL_DESC) VALUES(SEQ_HOLIDAY.NEXTVAL, '21-AUG-04', 'ninoy aquino day');

--MIB_IMPAIRMENTS
PROMPT INSERT MIB IMPAIRMENTS ................
INSERT INTO MIB_IMPAIRMENTS (MIB_IMPAIRMENT_CODE, MIB_IMPAIRMENT_DESC) VALUES('A', 'DOUBTS CONCERNING OCCUPATION');
INSERT INTO MIB_IMPAIRMENTS (MIB_IMPAIRMENT_CODE, MIB_IMPAIRMENT_DESC) VALUES('A1', 'UNHEALTHY OR HAZARDOUS OCCUPATION');
INSERT INTO MIB_IMPAIRMENTS (MIB_IMPAIRMENT_CODE, MIB_IMPAIRMENT_DESC) VALUES('A10', 'ANISELECTION');
INSERT INTO MIB_IMPAIRMENTS (MIB_IMPAIRMENT_CODE, MIB_IMPAIRMENT_DESC) VALUES('A11', 'ABUSE OF NICOTINE/EXCESSIVE SMOKING');
INSERT INTO MIB_IMPAIRMENTS (MIB_IMPAIRMENT_CODE, MIB_IMPAIRMENT_DESC) VALUES('A12', 'ALCOHOL ABUSE/ALCOHOLISM');
INSERT INTO MIB_IMPAIRMENTS (MIB_IMPAIRMENT_CODE, MIB_IMPAIRMENT_DESC) VALUES('A13', 'USE/ADDICTION TO PROHIBITED DRUGS/SUBSTANCES/DRUG ABUSE/SUBSTANCE ABUSE');
INSERT INTO MIB_IMPAIRMENTS (MIB_IMPAIRMENT_CODE, MIB_IMPAIRMENT_DESC) VALUES('A13A', 'ABUSE OF HABIT FORMING MEDICATION');
INSERT INTO MIB_IMPAIRMENTS (MIB_IMPAIRMENT_CODE, MIB_IMPAIRMENT_DESC) VALUES('A13B', 'SEEKING AND ACCEPTING PHYSICAL');
INSERT INTO MIB_IMPAIRMENTS (MIB_IMPAIRMENT_CODE, MIB_IMPAIRMENT_DESC) VALUES('A13C', 'ALCOHOL OR DRUG ABUSE COUNSELLING AND / OR SURVEILLANCE');
INSERT INTO MIB_IMPAIRMENTS (MIB_IMPAIRMENT_CODE, MIB_IMPAIRMENT_DESC) VALUES('A15', 'PROBLEMS RELATED TO EDUCATION AND LITERACY ILLITERACY AND LOW-LEVEL LITERACY ');
INSERT INTO MIB_IMPAIRMENTS (MIB_IMPAIRMENT_CODE, MIB_IMPAIRMENT_DESC) VALUES('A17', 'PROBLEMS RELATED TO ANY PSYCHOSOCIAL CIRCUMSTANCES');
INSERT INTO MIB_IMPAIRMENTS (MIB_IMPAIRMENT_CODE, MIB_IMPAIRMENT_DESC) VALUES('A17A', 'HISTORY OF UNWANTED PREGNANCY');
INSERT INTO MIB_IMPAIRMENTS (MIB_IMPAIRMENT_CODE, MIB_IMPAIRMENT_DESC) VALUES('A17B', 'CONVICTION IN CIVIL AND CRIMINAL PROCEEDINGS WITHOUT IMPRISONMENT');
INSERT INTO MIB_IMPAIRMENTS (MIB_IMPAIRMENT_CODE, MIB_IMPAIRMENT_DESC) VALUES('A17C', 'IMPRISONMENT AND OTHER INCARCERATION');
INSERT INTO MIB_IMPAIRMENTS (MIB_IMPAIRMENT_CODE, MIB_IMPAIRMENT_DESC) VALUES('A17D', 'INVOLVEMENT IN MILITANT OR QUESTIONABLE ACTIVITIES');
INSERT INTO MIB_IMPAIRMENTS (MIB_IMPAIRMENT_CODE, MIB_IMPAIRMENT_DESC) VALUES('A17E', 'RECIPIENT OF THREAT OF BODILY HARM OR INJURY');
INSERT INTO MIB_IMPAIRMENTS (MIB_IMPAIRMENT_CODE, MIB_IMPAIRMENT_DESC) VALUES('A19', 'VICTIM OF CRIME OR TERRORISM OR TORTURE');
INSERT INTO MIB_IMPAIRMENTS (MIB_IMPAIRMENT_CODE, MIB_IMPAIRMENT_DESC) VALUES('A2', 'HAZARDOUS AVOCATION');
INSERT INTO MIB_IMPAIRMENTS (MIB_IMPAIRMENT_CODE, MIB_IMPAIRMENT_DESC) VALUES('A20', 'PROBLEMS RELATED TO EXPOSURE TO DISASTER');
INSERT INTO MIB_IMPAIRMENTS (MIB_IMPAIRMENT_CODE, MIB_IMPAIRMENT_DESC) VALUES('A18', 'PROBLEMS RELATED TO OTHER LEGAL CIRCUMSTANCES ARREST/CHILD CUSTODY OR SUPPORT PROCEEDINGS/LITIGATION');

--MIB_NUMBERS
PROMPT INSERT MIB NUMBERS ................
INSERT INTO MIB_NUMBERS (MIB_NUM_CODE, MIB_NUM_DESC) VALUES('0', 'Insured or life to be insured');
INSERT INTO MIB_NUMBERS (MIB_NUM_CODE, MIB_NUM_DESC) VALUES('1', 'Father of the insured');
INSERT INTO MIB_NUMBERS (MIB_NUM_CODE, MIB_NUM_DESC) VALUES('2', 'Mother of the insured');
INSERT INTO MIB_NUMBERS (MIB_NUM_CODE, MIB_NUM_DESC) VALUES('3', 'Brother of the insured');
INSERT INTO MIB_NUMBERS (MIB_NUM_CODE, MIB_NUM_DESC) VALUES('4', 'Sister of the insured');
INSERT INTO MIB_NUMBERS (MIB_NUM_CODE, MIB_NUM_DESC) VALUES('5', 'Spouse of the insured');
INSERT INTO MIB_NUMBERS (MIB_NUM_CODE, MIB_NUM_DESC) VALUES('6', 'Other persons living w/ insured');

--TEST_PROFILES
PROMPT INSERT TEST PROFILES ................
INSERT INTO TEST_PROFILES (TEST_ID, TEST_DESC, TEST_VALIDITY, TEST_TAXABLE_IND, TEST_TYPE) VALUES(SEQ_TEST_PROFILE.NEXTVAL, '2-D ECHO', 999, '0', 'L');
INSERT INTO TEST_PROFILES (TEST_ID, TEST_DESC, TEST_VALIDITY, TEST_TAXABLE_IND, TEST_TYPE) VALUES(SEQ_TEST_PROFILE.NEXTVAL, 'Afp', 99, '0', 'L');
INSERT INTO TEST_PROFILES (TEST_ID, TEST_DESC, TEST_VALIDITY, TEST_TAXABLE_IND, TEST_TYPE) VALUES(SEQ_TEST_PROFILE.NEXTVAL, 'Agent''s Contracting Manual', 99, '0', 'L');
INSERT INTO TEST_PROFILES (TEST_ID, TEST_DESC, TEST_VALIDITY, TEST_TAXABLE_IND, TEST_TYPE) VALUES(SEQ_TEST_PROFILE.NEXTVAL, 'Agent recruit pre-employment', 99, '0', 'L');
INSERT INTO TEST_PROFILES (TEST_ID, TEST_DESC, TEST_VALIDITY, TEST_TAXABLE_IND, TEST_TYPE) VALUES(SEQ_TEST_PROFILE.NEXTVAL, 'Ahcv', 99, '0', 'L');
INSERT INTO TEST_PROFILES (TEST_ID, TEST_DESC, TEST_VALIDITY, TEST_TAXABLE_IND, TEST_TYPE) VALUES(SEQ_TEST_PROFILE.NEXTVAL, 'Albumin', 99, '0', 'L');
INSERT INTO TEST_PROFILES (TEST_ID, TEST_DESC, TEST_VALIDITY, TEST_TAXABLE_IND, TEST_TYPE) VALUES(SEQ_TEST_PROFILE.NEXTVAL, 'Anti-HBC', 99, '0', 'L');
INSERT INTO TEST_PROFILES (TEST_ID, TEST_DESC, TEST_VALIDITY, TEST_TAXABLE_IND, TEST_TYPE) VALUES(SEQ_TEST_PROFILE.NEXTVAL, 'Anti-HBE', 99, '0', 'L');
INSERT INTO TEST_PROFILES (TEST_ID, TEST_DESC, TEST_VALIDITY, TEST_TAXABLE_IND, TEST_TYPE) VALUES(SEQ_TEST_PROFILE.NEXTVAL, 'Anti-hbs', 99, '0', 'L');
INSERT INTO TEST_PROFILES (TEST_ID, TEST_DESC, TEST_VALIDITY, TEST_TAXABLE_IND, TEST_TYPE) VALUES(SEQ_TEST_PROFILE.NEXTVAL, 'BP', 99, '', 'L');
INSERT INTO TEST_PROFILES (TEST_ID, TEST_DESC, TEST_VALIDITY, TEST_TAXABLE_IND, TEST_TYPE) VALUES(SEQ_TEST_PROFILE.NEXTVAL, 'Breast ultrasound', 99, '0', 'L');
INSERT INTO TEST_PROFILES (TEST_ID, TEST_DESC, TEST_VALIDITY, TEST_TAXABLE_IND, TEST_TYPE) VALUES(SEQ_TEST_PROFILE.NEXTVAL, 'BUN', 99, '0', 'L');
INSERT INTO TEST_PROFILES (TEST_ID, TEST_DESC, TEST_VALIDITY, TEST_TAXABLE_IND, TEST_TYPE) VALUES(SEQ_TEST_PROFILE.NEXTVAL, 'CBC', 99, '0', 'L');
INSERT INTO TEST_PROFILES (TEST_ID, TEST_DESC, TEST_VALIDITY, TEST_TAXABLE_IND, TEST_TYPE) VALUES(SEQ_TEST_PROFILE.NEXTVAL, 'Chest X-Ray', 99, '0', 'L');
INSERT INTO TEST_PROFILES (TEST_ID, TEST_DESC, TEST_VALIDITY, TEST_TAXABLE_IND, TEST_TYPE) VALUES(SEQ_TEST_PROFILE.NEXTVAL, 'Cholesterol', 99, '0', 'L');
INSERT INTO TEST_PROFILES (TEST_ID, TEST_DESC, TEST_VALIDITY, TEST_TAXABLE_IND, TEST_TYPE) VALUES(SEQ_TEST_PROFILE.NEXTVAL, 'Cotinine', 99, '0', 'L');
INSERT INTO TEST_PROFILES (TEST_ID, TEST_DESC, TEST_VALIDITY, TEST_TAXABLE_IND, TEST_TYPE) VALUES(SEQ_TEST_PROFILE.NEXTVAL, 'Creatinine', 99, '0', 'L');
INSERT INTO TEST_PROFILES (TEST_ID, TEST_DESC, TEST_VALIDITY, TEST_TAXABLE_IND, TEST_TYPE) VALUES(SEQ_TEST_PROFILE.NEXTVAL, 'ECG', 99, '0', 'L');
INSERT INTO TEST_PROFILES (TEST_ID, TEST_DESC, TEST_VALIDITY, TEST_TAXABLE_IND, TEST_TYPE) VALUES(SEQ_TEST_PROFILE.NEXTVAL, 'Free Promo Medical', 99, '0', 'L');
INSERT INTO TEST_PROFILES (TEST_ID, TEST_DESC, TEST_VALIDITY, TEST_TAXABLE_IND, TEST_TYPE) VALUES(SEQ_TEST_PROFILE.NEXTVAL, 'GGTP', 99, '0', 'L');
INSERT INTO TEST_PROFILES (TEST_ID, TEST_DESC, TEST_VALIDITY, TEST_TAXABLE_IND, TEST_TYPE) VALUES(SEQ_TEST_PROFILE.NEXTVAL, 'Glucose', 99, '0', 'L');
INSERT INTO TEST_PROFILES (TEST_ID, TEST_DESC, TEST_VALIDITY, TEST_TAXABLE_IND, TEST_TYPE) VALUES(SEQ_TEST_PROFILE.NEXTVAL, 'HBAIC', 99, '0', 'L');
INSERT INTO TEST_PROFILES (TEST_ID, TEST_DESC, TEST_VALIDITY, TEST_TAXABLE_IND, TEST_TYPE) VALUES(SEQ_TEST_PROFILE.NEXTVAL, 'HBEAG', 99, '0', 'L');
INSERT INTO TEST_PROFILES (TEST_ID, TEST_DESC, TEST_VALIDITY, TEST_TAXABLE_IND, TEST_TYPE) VALUES(SEQ_TEST_PROFILE.NEXTVAL, 'HBSAG', 99, '0', 'L');
INSERT INTO TEST_PROFILES (TEST_ID, TEST_DESC, TEST_VALIDITY, TEST_TAXABLE_IND, TEST_TYPE) VALUES(SEQ_TEST_PROFILE.NEXTVAL, 'Hepatitis Profile', 99, '0', 'L');
INSERT INTO TEST_PROFILES (TEST_ID, TEST_DESC, TEST_VALIDITY, TEST_TAXABLE_IND, TEST_TYPE) VALUES(SEQ_TEST_PROFILE.NEXTVAL, 'HIB', 99, '0', 'L');
INSERT INTO TEST_PROFILES (TEST_ID, TEST_DESC, TEST_VALIDITY, TEST_TAXABLE_IND, TEST_TYPE) VALUES(SEQ_TEST_PROFILE.NEXTVAL, 'Liver Function Test', 99, '0', 'L');
INSERT INTO TEST_PROFILES (TEST_ID, TEST_DESC, TEST_VALIDITY, TEST_TAXABLE_IND, TEST_TYPE) VALUES(SEQ_TEST_PROFILE.NEXTVAL, 'Mammography', 99, '0', 'L');
INSERT INTO TEST_PROFILES (TEST_ID, TEST_DESC, TEST_VALIDITY, TEST_TAXABLE_IND, TEST_TYPE) VALUES(SEQ_TEST_PROFILE.NEXTVAL, 'Microalbumin', 99, '0', 'L');
INSERT INTO TEST_PROFILES (TEST_ID, TEST_DESC, TEST_VALIDITY, TEST_TAXABLE_IND, TEST_TYPE) VALUES(SEQ_TEST_PROFILE.NEXTVAL, 'Pelvic ultrasound', 99, '0', 'L');
INSERT INTO TEST_PROFILES (TEST_ID, TEST_DESC, TEST_VALIDITY, TEST_TAXABLE_IND, TEST_TYPE) VALUES(SEQ_TEST_PROFILE.NEXTVAL, 'PROSTATIC SPECIFIC AG', 99, '0', 'L');
INSERT INTO TEST_PROFILES (TEST_ID, TEST_DESC, TEST_VALIDITY, TEST_TAXABLE_IND, TEST_TYPE) VALUES(SEQ_TEST_PROFILE.NEXTVAL, 'Renal function test', 99, '0', 'L');
INSERT INTO TEST_PROFILES (TEST_ID, TEST_DESC, TEST_VALIDITY, TEST_TAXABLE_IND, TEST_TYPE) VALUES(SEQ_TEST_PROFILE.NEXTVAL, 'SGOT', 99, '0', 'L');
INSERT INTO TEST_PROFILES (TEST_ID, TEST_DESC, TEST_VALIDITY, TEST_TAXABLE_IND, TEST_TYPE) VALUES(SEQ_TEST_PROFILE.NEXTVAL, 'SGPT', 99, '0', 'L');
INSERT INTO TEST_PROFILES (TEST_ID, TEST_DESC, TEST_VALIDITY, TEST_TAXABLE_IND, TEST_TYPE) VALUES(SEQ_TEST_PROFILE.NEXTVAL, 'Staff Pre-employment', 99, '0', 'L');
INSERT INTO TEST_PROFILES (TEST_ID, TEST_DESC, TEST_VALIDITY, TEST_TAXABLE_IND, TEST_TYPE) VALUES(SEQ_TEST_PROFILE.NEXTVAL, 'Stool examination', 99, '0', 'L');

--LABORATORIES
PROMPT INSERT LABORATORIES ................
INSERT INTO LABORATORIES (LAB_ID, LAB_NAME, CREATED_BY) VALUES(SEQ_LABORATORY.NEXTVAL, 'Accuvision', 'IUMADMIN');
INSERT INTO LABORATORIES (LAB_ID, LAB_NAME, CREATED_BY) VALUES(SEQ_LABORATORY.NEXTVAL, 'Associated Medical and Clinical Services', 'IUMADMIN');
INSERT INTO LABORATORIES (LAB_ID, LAB_NAME, CREATED_BY) VALUES(SEQ_LABORATORY.NEXTVAL, 'Best Diagnostic', 'IUMADMIN');
INSERT INTO LABORATORIES (LAB_ID, LAB_NAME, CREATED_BY) VALUES(SEQ_LABORATORY.NEXTVAL, 'Casa Medica, Inc.', 'IUMADMIN');
INSERT INTO LABORATORIES (LAB_ID, LAB_NAME, CREATED_BY) VALUES(SEQ_LABORATORY.NEXTVAL, 'CEDAR Scientific Clinical Laboratory', 'IUMADMIN');
INSERT INTO LABORATORIES (LAB_ID, LAB_NAME, CREATED_BY) VALUES(SEQ_LABORATORY.NEXTVAL, 'Centralle Medical Diagnostics and Polyc', 'IUMADMIN');
INSERT INTO LABORATORIES (LAB_ID, LAB_NAME, CREATED_BY) VALUES(SEQ_LABORATORY.NEXTVAL, 'CHONG HUA  HOSPITAL', 'IUMADMIN');
INSERT INTO LABORATORIES (LAB_ID, LAB_NAME, CREATED_BY) VALUES(SEQ_LABORATORY.NEXTVAL, 'HealthWay Medical Clinics', 'IUMADMIN');
INSERT INTO LABORATORIES (LAB_ID, LAB_NAME, CREATED_BY) VALUES(SEQ_LABORATORY.NEXTVAL, 'INTERCON DX', 'IUMADMIN');
INSERT INTO LABORATORIES (LAB_ID, LAB_NAME, CREATED_BY) VALUES(SEQ_LABORATORY.NEXTVAL, 'LC Diagnostic Center', 'IUMADMIN');
INSERT INTO LABORATORIES (LAB_ID, LAB_NAME, CREATED_BY) VALUES(SEQ_LABORATORY.NEXTVAL, 'MAKATI MEDICAL CENTER', 'IUMADMIN');
INSERT INTO LABORATORIES (LAB_ID, LAB_NAME, CREATED_BY) VALUES(SEQ_LABORATORY.NEXTVAL, 'NOT SPECIFIED', 'IUMADMIN');
INSERT INTO LABORATORIES (LAB_ID, LAB_NAME, CREATED_BY) VALUES(SEQ_LABORATORY.NEXTVAL, 'Olongapo HealthCare Specialist', 'IUMADMIN');
INSERT INTO LABORATORIES (LAB_ID, LAB_NAME, CREATED_BY) VALUES(SEQ_LABORATORY.NEXTVAL, 'Panay Health Care', 'IUMADMIN');
INSERT INTO LABORATORIES (LAB_ID, LAB_NAME, CREATED_BY) VALUES(SEQ_LABORATORY.NEXTVAL, 'PRIME LABORATORY', 'IUMADMIN');
INSERT INTO LABORATORIES (LAB_ID, LAB_NAME, CREATED_BY) VALUES(SEQ_LABORATORY.NEXTVAL, 'Sta. Elena Diagnostic Laboratory', 'IUMADMIN');
INSERT INTO LABORATORIES (LAB_ID, LAB_NAME, CREATED_BY) VALUES(SEQ_LABORATORY.NEXTVAL, 'Sun Life Medical Department', 'IUMADMIN');
INSERT INTO LABORATORIES (LAB_ID, LAB_NAME, CREATED_BY) VALUES(SEQ_LABORATORY.NEXTVAL, 'THE MEDICAL CITY', 'IUMADMIN');
INSERT INTO LABORATORIES (LAB_ID, LAB_NAME, CREATED_BY) VALUES(SEQ_LABORATORY.NEXTVAL, 'NAIC DIAGNOSTIC CENTER', 'IUMADMIN');

--EXAMINERS
PROMPT INSERT EXAMINERS ................
INSERT INTO EXAMINERS (EXMNR_ID, EXMNR_LAST_NAME, EXMNR_GIVEN_NAME, EXMNR_MIDDLE_NAME, EXMNR_RANK_ID, CREATED_BY) VALUES(SEQ_EXAMINER.NEXTVAL, 'ABUYABOR', 'FELICISIMO', 'III', 2, 'IUMADMIN');
INSERT INTO EXAMINERS (EXMNR_ID, EXMNR_LAST_NAME, EXMNR_GIVEN_NAME, EXMNR_MIDDLE_NAME, EXMNR_RANK_ID, CREATED_BY) VALUES(SEQ_EXAMINER.NEXTVAL, 'AGUILAR', 'MA. LUZ', 'M', 1, 'IUMADMIN');
INSERT INTO EXAMINERS (EXMNR_ID, EXMNR_LAST_NAME, EXMNR_GIVEN_NAME, EXMNR_MIDDLE_NAME, EXMNR_RANK_ID, CREATED_BY) VALUES(SEQ_EXAMINER.NEXTVAL, 'AGUSTIN', 'MA. THERESA', 'T', 5, 'IUMADMIN');
INSERT INTO EXAMINERS (EXMNR_ID, EXMNR_LAST_NAME, EXMNR_GIVEN_NAME, EXMNR_MIDDLE_NAME, EXMNR_RANK_ID, CREATED_BY) VALUES(SEQ_EXAMINER.NEXTVAL, 'ALA', 'VIRGINIA', 'G', 4, 'IUMADMIN');
INSERT INTO EXAMINERS (EXMNR_ID, EXMNR_LAST_NAME, EXMNR_GIVEN_NAME, EXMNR_MIDDLE_NAME, EXMNR_RANK_ID, CREATED_BY) VALUES(SEQ_EXAMINER.NEXTVAL, 'ALBANO', 'BIBIANA', 'C', 1, 'IUMADMIN');

-- REQUIREMENT FORMS
PROMPT INSERT REQUIREMENT FORMS ................
INSERT INTO requirement_forms (rf_id, rf_name, rf_template_name) VALUES (seq_requirement_form.NEXTVAL, 'Acceptance of Offer','Acceptance of Offer');
INSERT INTO requirement_forms (rf_id, rf_name, rf_template_name) VALUES (seq_requirement_form.NEXTVAL, 'Acceptance of Offer Form with Exclusion Provision',NULL);
INSERT INTO requirement_forms (rf_id, rf_name, rf_template_name) VALUES (seq_requirement_form.NEXTVAL, 'Additional Requirements for the Applicant Under the Anti-Money Laundering Act','Anti-Money Laundering Act');
INSERT INTO requirement_forms (rf_id, rf_name, rf_template_name) VALUES (seq_requirement_form.NEXTVAL, 'Agent''s Confidential Report','Agent''s Confidential Report');
INSERT INTO requirement_forms (rf_id, rf_name, rf_template_name) VALUES (seq_requirement_form.NEXTVAL, 'Alteration of Application','Alteration of Application');
INSERT INTO requirement_forms (rf_id, rf_name, rf_template_name) VALUES (seq_requirement_form.NEXTVAL, 'Arthritis Questionnaire','Arthritis Questionnaire');
INSERT INTO requirement_forms (rf_id, rf_name, rf_template_name) VALUES (seq_requirement_form.NEXTVAL, 'Attending Physician''s Statement','Attending Physician''s Statement');
INSERT INTO requirement_forms (rf_id, rf_name, rf_template_name) VALUES (seq_requirement_form.NEXTVAL, 'Automobile and Motorcycle Racing Questionnaire','Automobile and Motorcycle Racing Questionnaire');
INSERT INTO requirement_forms (rf_id, rf_name, rf_template_name) VALUES (seq_requirement_form.NEXTVAL, 'Aviation Questionnaire','Aviation Questionnaire');
INSERT INTO requirement_forms (rf_id, rf_name, rf_template_name) VALUES (seq_requirement_form.NEXTVAL, 'Business Financial Questionnaire','Financial Questionnaire - Business');
INSERT INTO requirement_forms (rf_id, rf_name, rf_template_name) VALUES (seq_requirement_form.NEXTVAL, 'Certificate of Insurability (age 16 and over)',null);
INSERT INTO requirement_forms (rf_id, rf_name, rf_template_name) VALUES (seq_requirement_form.NEXTVAL, 'Certificate of Insurability (child under age 16)',null);
INSERT INTO requirement_forms (rf_id, rf_name, rf_template_name) VALUES (seq_requirement_form.NEXTVAL, 'Certification re: New Signature','Certification re New Signature');
INSERT INTO requirement_forms (rf_id, rf_name, rf_template_name) VALUES (seq_requirement_form.NEXTVAL, 'Cession Form',null);
INSERT INTO requirement_forms (rf_id, rf_name, rf_template_name) VALUES (seq_requirement_form.NEXTVAL, 'Diving Questionnaire','Diving Questionnaire');
INSERT INTO requirement_forms (rf_id, rf_name, rf_template_name) VALUES (seq_requirement_form.NEXTVAL, 'Drug Usage Questionnaire','Drug Usage Questionnaire');
INSERT INTO requirement_forms (rf_id, rf_name, rf_template_name) VALUES (seq_requirement_form.NEXTVAL, 'Laboratory Exam LOA Form', 'Laboratory Exam LOA');
INSERT INTO requirement_forms (rf_id, rf_name, rf_template_name) VALUES (seq_requirement_form.NEXTVAL, 'Medical Appointment Form',null);
INSERT INTO requirement_forms (rf_id, rf_name, rf_template_name) VALUES (seq_requirement_form.NEXTVAL, 'Medical Exam LOA Form','Medical Exam LOA');
INSERT INTO requirement_forms (rf_id, rf_name, rf_template_name) VALUES (seq_requirement_form.NEXTVAL, 'Mountain Climbing Questionnaire','Mountain Climbing Questionnaire');
INSERT INTO requirement_forms (rf_id, rf_name, rf_template_name) VALUES (seq_requirement_form.NEXTVAL, 'Parachuting/Sky Diving Questionnaire','Parachuting and Sky Diving Questionnaire');
INSERT INTO requirement_forms (rf_id, rf_name, rf_template_name) VALUES (seq_requirement_form.NEXTVAL, 'Part II of Application for insurance on Child age 15 or under',null);
INSERT INTO requirement_forms (rf_id, rf_name, rf_template_name) VALUES (seq_requirement_form.NEXTVAL, 'Part II of Application for Insurance on Adult',null);
INSERT INTO requirement_forms (rf_id, rf_name, rf_template_name) VALUES (seq_requirement_form.NEXTVAL, 'Personal Financial Questionnaire','Financial Questionnaire - Personal');
INSERT INTO requirement_forms (rf_id, rf_name, rf_template_name) VALUES (seq_requirement_form.NEXTVAL, 'PN Non-Med Questionnaire',null);
INSERT INTO requirement_forms (rf_id, rf_name, rf_template_name) VALUES (seq_requirement_form.NEXTVAL, 'Residential Background Questionnaire','Residential Background Questionnaire');
INSERT INTO requirement_forms (rf_id, rf_name, rf_template_name) VALUES (seq_requirement_form.NEXTVAL, 'Secretary''s Certificate',null);
INSERT INTO requirement_forms (rf_id, rf_name, rf_template_name) VALUES (seq_requirement_form.NEXTVAL, 'Special Power of Attorney',null);
INSERT INTO requirement_forms (rf_id, rf_name, rf_template_name) VALUES (seq_requirement_form.NEXTVAL, 'Supplementary Questions concerning Asthma, Bronchitis or other Pulmonary Symptoms','Supplementary Questions re Asthma, Bronchitis');
INSERT INTO requirement_forms (rf_id, rf_name, rf_template_name) VALUES (seq_requirement_form.NEXTVAL, 'Supplementary Questions concerning Chest Pain or Discomfort','Supplementary Questions re Chest Pain or Discomfort');
INSERT INTO requirement_forms (rf_id, rf_name, rf_template_name) VALUES (seq_requirement_form.NEXTVAL, 'Supplementary Questions concerning Diabetes (or suspected Diabetes)','Supplementary Questions re Diabetes');
INSERT INTO requirement_forms (rf_id, rf_name, rf_template_name) VALUES (seq_requirement_form.NEXTVAL, 'Supplementary Questions concerning Digestive or Bowel Disorder','Supplementary Questions re Digestive or Bowel Disorder');
INSERT INTO requirement_forms (rf_id, rf_name, rf_template_name) VALUES (seq_requirement_form.NEXTVAL, 'Supplementary Questions concerning Fainting or Episodes of Loss of Consciousness','Supplementary Questions re Fainting');
INSERT INTO requirement_forms (rf_id, rf_name, rf_template_name) VALUES (seq_requirement_form.NEXTVAL, 'Supplementary Questions Concerning use of Alcohol','Supplementary Questions re Use of Alcohol');
INSERT INTO requirement_forms (rf_id, rf_name, rf_template_name) VALUES (seq_requirement_form.NEXTVAL, 'Supplementary Questions for Female/Maternity Benefit', 'Supplementary Questions for Female Maternity Benefit');
INSERT INTO requirement_forms (rf_id, rf_name, rf_template_name) VALUES (seq_requirement_form.NEXTVAL, 'Supplementary Questions re Growths, Cysts, Lumps and Tumors', 'Supplementary Questions re Growths, Cysts, Lumps and Tumors');
INSERT INTO requirement_forms (rf_id, rf_name, rf_template_name) VALUES (seq_requirement_form.NEXTVAL, 'Supplementary Questions re Gynecological Complaints','Supplementary Questions re Gynecological Complaints');
INSERT INTO requirement_forms (rf_id, rf_name, rf_template_name) VALUES (seq_requirement_form.NEXTVAL, 'Waiver of Benefit APDB (form for advance payment disability benefit)', 'Waiver for APDB');
INSERT INTO requirement_forms (rf_id, rf_name, rf_template_name) VALUES (seq_requirement_form.NEXTVAL, 'Waiver of Benefit CCR (form for critical condition rider)', 'Waiver for CCR');
INSERT INTO requirement_forms (rf_id, rf_name, rf_template_name) VALUES (seq_requirement_form.NEXTVAL, 'Waiver of Benefit F and MB (form for female and maternity benefit)', 'Waiver for F and M Benefit');
INSERT INTO requirement_forms (rf_id, rf_name, rf_template_name) VALUES (seq_requirement_form.NEXTVAL, 'Waiver of Benefit HIB (form for hospital income benefit)','Waiver for HIB');
INSERT INTO requirement_forms (rf_id, rf_name, rf_template_name) VALUES (seq_requirement_form.NEXTVAL, 'Parachuting / Sky Diving Questionnaire', 'Parachuting and Sky Diving Questionnaire');	

--REQUIREMENTS
PROMPT INSERT REQUIREMENTS ................
set escape on
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('AAREJ','AUTO-APP REJECT', to_number('12'),'C', to_number('3'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('AAR1','AOA-AGENT''S REPRT-INSURABILITY', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('AAR2','AOA-AGNT''S REPRT-BUS INSURANCE', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('ABQ','ASTHMA QUESTIONNAIRE', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('ABR','AWAITING BRANCH REQUIREMENT', to_number('3'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('ACR','AGENT''S CONFIDENTIAL REPORT', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('AFP','ALPHA FETO PROTEIN', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('AFS1','AUDITED FINANCIAL STATEMENT', to_number('12'),'P', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('AGR','AGENT''S REPORT', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('AHCV','ANTI-HCV', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('AHO1','AWAIT HEAD OFFICE', to_number('1'),'P', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('AIR','AWAITING INSPECTION REPORT', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('ALCR','ALIEN CERTIFICATE REGISTRATION', to_number('99'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('ALQ','ALCOHOL QUESTIONNAIRE', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('AMLA','ANTI-MONEY LAUNDERING FORM', to_number('12'),'P', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('AMLA1','AMLA FORM W/ VALID ID/S', to_number('12'),'P', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('AOAU','ALTERATION OF APPLICATION', to_number('12'),'P', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('AOO','ACCEPTANCE OF OFFER', to_number('12'),'P', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('APP','ADVANCE PREMIUM PAYMENT', to_number('12'),'P', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('APR','AWAITING PAYOR''S REQUIREMENT', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('APS','ATTDG PHYSICIAN''S STATEMENT', to_number('3'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('APS1','APS - WE WILL OBTAIN...', to_number('3'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('APS2','APS - PLEASE OBTAIN...', to_number('3'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('AREIN','AWAITING REINSURER''S ADVISE', to_number('12'),'P', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('ARQ','AUTO RACING QUESTIONNAIRE', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('ARTQ','ARTHRITIS QUESTIONNAIRE', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('AUL1','AUTHORIZATION LETTER', to_number('12'),'P', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('AUR','AWAIT AGE/AMT URINE RESULTS', to_number('3'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('AUTH','MEDICAL AUTHORIZATION', to_number('1'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('AVQ','AVIATION QUESTIONNAIRE', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('A11','AOA - SEC 1.1', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('A12','AOA - SECTION 1.2', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('A14','AOA -SECTION 1.4 MAILING ADD', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('A15','AOA - SEC 1.5', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('A21','AOA SECTION 2.1', to_number('12'),'P', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('A23','AOA - SEC 2.3', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('A30','AOA - SEC 3.0 TLIC', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('A41','AOA - SECTION 4.1', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('A42','AOA - SECTION 4.2', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('A42A','AOA - SECTION 4.2 ITEM A', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('A42B','AOA - SECTION 4.2 ITEM B', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('A42C','AOA - SEC 4.2 ITEM C', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('A42D','AOA - SECTION 4.2 ITEM D', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('A42E','AOA - SECTION 4.2 ITEM E', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('A42F','AOA - SECTION 4.2 ITEM F', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('A42G','AOA - SECTION 4.2 ITEM G', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('A42H','AOA - SECTION 4.2 ITEM H', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('A42I','AOA - SECTION 4.2 ITEM I', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('A42J','AOA - SECTION 4.2 ITEM J', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('A42K','AOA - SECTION 4.2 ITEM K', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('A42L','AOA - SECTION 4.2 ITEM L', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('A42M','AOA - SECTION 4.2 ITEM M', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('A42N','AOA - SECTION 4.2 ITEM N', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('A42O','AOA - SECTION 4.2 ITEM O', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('A42P','AOA - SECTION 4.2 ITEM P', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('A42Q','AOA - SECTION 4.2 ITEM Q', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('A42R','AOA - SECTION 4.2 ITEM R', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('A42S','AOA - SECTION 4.2 ITEM S', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('A42T','AOA - SECTION 4.2 ITEM T', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('A42U','AOA - SECTION 4.2 ITEM U', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('A60','AOA  - SECTION 6 SIGNATURES', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('BBDT','BALANCE OF BLOOD TEST', to_number('6'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('BDR','BORROW FROM DOCTOR', to_number('6'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('BIOP','REQUIRE BIOPSY REPORT', to_number('6'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('BOPA','BANKERS ORDER', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('BORRO','BORROW RECORD', to_number('1'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('BP1','1 AFTERNOON BLOOD PRESSURE', to_number('3'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('BR','BOARD RESOLUTION', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('CANC','CANCELLED CHEQUE', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('CAR1','AOA-AGENT''S REPRT-INSURABILITY', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('CBC','COMPLETE BLOOD COUNT', to_number('6'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('COE','CERTIFICATE OF EMPLOYMENT', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('COEI','CERTIFICATE EMPLOYMENT/INCOME', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('COI','CERTIFICATE OF INCOME', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('COTN','COTININE TEST', to_number('3'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('COVE','EXPLAIN SALE IN LETTER', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('CPQ','CHEST PAIN QUESTIONNAIRE', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('CRDCL','CREDITOR''S CLAUSE', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('CTR','COPY OF TRUST AGREEMENT', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('CWA','CASH WITH APPLICATION', to_number('1'),'P', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('CYSQ','GROWTH,CYSTS,LUMPS,TUMORS QUES', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('C11','AOA - SEC 1 RESIDE OUTSIDE PH', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('C12A','AOA - SEC 1.2', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('C23','AOA - APP SIGN DATE', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('C30','AOA - SECTION 3 TLIC', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('C41','AOA - SECTION 4.1', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('C42','AOA - SECTION 4.2', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('C42A','AOA - SECTION 4.2 ITEM A', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('C42B','AOA - SECTION 4.2 ITEM B', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('C42C','AOA - SECTION 4.2 ITEM C', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('C42D','AOA - SECTION 4.2 ITEM D', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('C42E','AOA - SECTION 4.2 ITEM E', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('C42F','AOA - SECTION 4.2 ITEM F', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('C42G','AOA - SECTION 4.2 ITEM G', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('C42H','AOA - SECTION 4.2 ITEM H', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('C42I','AOA - SECTION 4.2 ITEM I', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('C42J','AOA - SECTION 4.2 ITEM J', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('C42K','AOA - SECTION 4.2 ITEM K', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('C42L','AOA - SECTION 4.2 ITEM L', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('C42M','AOA - SECTION 4.2 ITEM M', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('C42N','AOA - SECTION 4.2 ITEM N', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('C42O','AOA - SECTION 4.2 ITEM O', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('C42P','AOA - SECTION 4.2 ITEM P', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('C42Q','AOA - SECTION 4.2 ITEM Q', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('C42R','AOA - SECTION 4.2 ITEM R', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('C42S','AOA - SECTION 4.2 ITEM S', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('C42T','AOA - SECTION 4.2 ITEM T', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('C42U','AOA - SECTION 4.2 ITEM U', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('C50','AOA - SECTION 5.0', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('C50A','AOA - SECTION 5.0 ITEM A', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('C50B','AOA - SECTION 5.0 ITEM B', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('C50C','AOA - SECTION 5.0 ITEM C', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('C50D','AOA - SECTION 5.2 ITEM D', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('C50E','AOA - SECTION 5.0 ITEM E', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('C50F','AOA - SECTION 5.0 ITEM F', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('C50G','AOA - SECTION 5.0 ITEM G', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('C50H','AOA - SECTION 4.2 ITEM H', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('C50I','AOA - SECTION 5.0 ITEM I', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('C50J','AOA - SECTION 5.0 ITEM J', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('C50K','AOA - SECTION 5.0 ITEM K', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('C50L','AOA - SECTION 5.0 ITEM L', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('C50M','AOA - SECTION 5.0 ITEM M', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('C50N','AOA - SECTION 5.0 ITEM N', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('C50O','AOA - SECTION 5.0 ITEM O', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('DBQ','DIABETES QUESTIONNAIRE', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('DDA1','DIRECT DEBIT AUTHORIZATION', to_number('1'),'P', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('DECL1','DECLARATION RE: SIGNATURE', to_number('12'),'P', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('DGQ','DIGESTIVE QUESTIONNAIRE', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('DRQ','DRUG USAGE QUESTIONNAIRE', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('DS1','DATE OF SIGNING', to_number('3'),'P', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('DUED1','SUBMIT AOA RE: DUE DATE', to_number('3'),'P', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('DVQ','DIVING QUESTIONNAIRE', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('ECG','12 LEAD RESTING EKG', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('ECGTR','SUBMIT ECG TRACINGS', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('ECHO','ECHOCARDIOGRAM', to_number('6'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('EOA','EVIDENCE OF AGE', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('EXCL','EXCLUSION CLAUSE', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('EXMA','EXAM ON ASSURED', to_number('3'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('EXMO','EXAM ON OWNER', to_number('3'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('EXRQ','MEDICAL EXAMINER''S REPORT QUES', to_number('3'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('EXT1','EXTENSION GRANTED', to_number('1'),'C', to_number('3'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('FMBQ','FEMALE/MATERNITY BENEFIT QUEST', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('FQ','FINANCIAL QUESTIONNAIRE', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('GYNQ','GYNECOLOGICAL COMPLAINTS QUEST', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('HBAIC','GLYCOSYLATED HEMOGLOBIN', to_number('6'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('HBSAG','BLOOD TESTS FOR HBSAG', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('HCH','HEART CHART BY MD', to_number('6'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('HEPA','BLOOD TEST FOR HBSAG AND HBEAG', to_number('6'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('HEPB','HBSAG,HBEAG,SGOT,SGPT,GGT,AFP', to_number('6'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('HIV','BLOOD TEST FOR HIV', to_number('6'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('ID','IDENTIFICATION (HK)', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('IDC','VALID ID/S', to_number('12'),'P', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('ID1','IDENTIFICATION (HK)', to_number('12'),'P', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('INC','AMOUNT OF INCOME', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('INSR','OKEY FOR INSERTION', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('ITR','INCOME TAX RETURN', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('LCQ','LOSS OF CONSCIOUSNESS QUES', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('LFTS','BLD TEST FOR SGOT, SGPT \& GGT', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('LOA','LETTER AGREEMENT(EMPLOYER/YEE)', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('LOAN','LOAN AGREEMENT FORM', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('LOCI','LOCATION - ISSUE', to_number('12'),'P', to_number('3'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('LOCU','LOCATION - UNDERWRITING', to_number('12'),'C', to_number('3'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('MCQ','MOUNTAIN CLIMBING QUES', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('MEDC','MEDICAL CERTIFICATE', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('MEDEX','EXAM NOT YET RECEIVED', to_number('3'),'C', to_number('7'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('MIB','MEDICAL INFORMATION BUREAU', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('MICR','MICRO-ALBUMIN', to_number('3'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('MO','FOR DISCUSSION W/ MED. OFFICER', to_number('1'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('NEWA','NEW APPLICATION', to_number('12'),'P', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('NEWAP','NEW APP WITNESSED BY UM/BM', to_number('3'),'P', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('NIC','NICOTINE TEST', to_number('3'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('NMA','NON MED ON ASSURED', to_number('3'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('NMOWN','NON MED ON OWNER', to_number('3'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('NPI','NEW PART I', to_number('3'),'P', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('OFF1','ADVISE OF OFFER', to_number('1'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('OOO1','OTHER REQUIREMENTS', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('PAYM','AWTNG. ADDITIONAL/INITIAL PREM', to_number('1'),'P', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('PAY1','AWAITING PAYMENT FOR WORKSITE', to_number('12'),'P', to_number('3'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('PBM','PREMIUM BELOW MINIMUM', to_number('1'),'P', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('PCI','PERSONAL CERT OF INSURABILITY', to_number('3'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('PFP','PERSONAL FINANCIAL PLANNING', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('PFT','PULMONARY FUNCTION TEST', to_number('6'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('PNE','POLICY NUMBER OF EXISTING', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('POLCH','POLICY CHANGE FILE', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('POO','PROOF OF OWNERSHIP (TCT ETC.)', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('POS','PROOF OF STATUS OF STAY', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('PREF','EXAM BY PREFERRED', to_number('3'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('PREL1','POLICY RELEASE', to_number('99'),'P', to_number('3'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('PREV1','REQUEST PREVIOUS FILE', to_number('1'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('PRM1','COLLECT PREMIUM', to_number('1'),'P', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('PSA','PROSTATIC SPECIFIC ANTIGEN', to_number('3'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('PSQ','PARACHUTING/SKYDIVING QUES', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('RAP','NAME \& ADD OF REG ATTDG DOCTOR', to_number('1'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('REBE1','RELATIONSHIP OF BENEFICIARIES', to_number('12'),'P', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('RECD','RECEIVED APPLICATION BY USD', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('RECD1','RECEIVED APPLICATION BY USD', to_number('12'),'P', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('RECON','RECONSIDERATION', to_number('1'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('REINS','REINSTATEMENT FILE', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('REIN1','REINSURANCE - LNL', to_number('1'),'P', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('REIN2','REINSURANCE - MANURE', to_number('1'),'P', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('REIN3','REINSURANCE - UNIRE', to_number('1'),'P', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('REIN4','REINSURANCE - SWISSRE', to_number('1'),'P', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('REIN5','REINSURANCE - RGA', to_number('1'),'P', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('RETU','RETURN PART I', to_number('3'),'P', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('RETUD','RETURN DOCUMENT/S', to_number('3'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('RETU2','RETURN PART II', to_number('3'),'P', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('REXM','REQUIRE EXAM ATTENTION TO:', to_number('3'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('RFT','BLD TEST FOR BUN \& CREATININE', to_number('6'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('RGA','REINSURANCE GROUP OF AMERICA', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('RIR','AWAITING INSPECTION REPORT', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('RMO1','REFERRED FOR MEDICAL OPINION', to_number('1'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('RTS','RETURN TO SENDER', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('RUR','REQUIRE MICROURINALYSIS', to_number('6'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('RUR2','2 MICROS ON 2 DIFFERENT DAYS', to_number('3'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('RXRY','REPEAT X-RAY', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('SCERT','SECRETARY''E CERTIFICATE', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('SIGN1','SIGNATURE MISSING', to_number('1'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('SLDE1','SALARY DEDUCTION FORM', to_number('12'),'P', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('SL12','SUNLIFE 12', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('SL13','SUNLIFE 13', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('SPA','SPECIAL POWER OF ATTORNEY', to_number('12'),'P', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('SPDYA','SPEEDY APPROVED', to_number('12'),'P', to_number('3'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('SPDYR','SPEEDY REJECT', to_number('12'),'C', to_number('3'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('SPLTD','EXM BY SPECIAL LIMITED', to_number('3'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('SPNO','EXAM BY SPECIAL NO LIMIT', to_number('3'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('SPO','STOP PAYMENT REQUEST', to_number('12'),'P', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('SPS','SIGNATURE ON PROPER SPACES', to_number('3'),'P', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('SREM','PAY SHORT REMITTANCE', to_number('1'),'P', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('SUBE','SURNAME OF BENEFICIARIES', to_number('12'),'P', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('SWISS','SWISS REINSURANCE', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('TC','TRANSLATION CLAUSE', to_number('12'),'P', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('TCHEC','TIME-BARRED CHEQUE', to_number('6'),'P', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('TCL','THUMB CLAUSE', to_number('12'),'P', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('THALL','TREADMILL WITH THALLIUM', to_number('6'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('TM','TREADMILL REPORT', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('TMTRA','SUBMIT TREADMILL TRACINGS', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('UD','UNDERWRITING DECISION', to_number('99'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('UWDE1','UNDERWRITING DECISION', to_number('1'),'P', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('WAOA','ALTERATION OF APP (WORKSITE)', to_number('12'),'P', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('WARN','GET PREV FILE-RECHECK DECISION', to_number('99'),'C', to_number('3'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('WPBA','WAIVER PREMIUM BENEFIT-APDB', to_number('12'),'P', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('WPBC','WAIVER PREMIUM BENEFIT -CCR', to_number('12'),'P', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('WPBF','WAIVER OF PREM BENEFIT-FEMALE', to_number('12'),'P', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('WPBH','WAIVER OF PREM BENEFIT - HIB', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('W11','AOA(WORKSITE) SEC 1.1', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('W12A','AOA(WORKSITE) SEC 1.2.A', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('W12B','AOA (WORKSITE) SEC 1.2.B', to_number('12'),'P', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('W22','AOA FOR ADD''L INFO (WKSITE)', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('W23','AOA 4 OTHER LIFE INSCE POLS WK', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('W31A','AOA (WKSITE) SEC 3.1.A', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('W31B','AOA (WKSITE) SEC 3.1.B', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('W31C','AOA (WKSITE) SEC 3.1.C', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('W31D','AOA (WKSITE) SEC 3.1.D', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('W31E','AOA (WKSITE) SEC 3.1.E', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('W32A','AOA (WKSITE) SEC 3.2.A', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('W32B','AOA (WKSITE) SEC 3.2.B', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('W32C','AOA (WKSITE) SEC 3.2.C', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('W32D','AOA (WKSITE) SEC 3.2.D', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('W32E','AOA (WKSITE) SEC 3.2.E', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('W32F','AOA (WKSITE) SEC 3.2.F', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('W32G','AOA (WORKSITE)-SEC.3.2 G', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('W32H','AOA (WORKSITE)-SEC.3.2H', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('XRY','CHEST X-RAY', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('XRYPL','SUBMIT CHEST X-RAY PLATE', to_number('6'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YAAI1','ALTERATION OF APPLICATION - I', to_number('12'),'P', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YAAI2','ALTERATION OF APPLICATION - I', to_number('12'),'P', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YAAI3','ALTERATION OF APPLICATION - I', to_number('12'),'P', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YAAU1','ALTERATION OF APPLICATION - U', to_number('12'),'C', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YAAU2','ALTERATION OF APPLICATION - U', to_number('12'),'C', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YAAU3','ALTERATION OF APPLICATION - U', to_number('12'),'C', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YABQ','ASTHMA QUESTIONNAIRE', to_number('12'),'C', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YABQ1','ASTHMA QUESTIONNAIRE', to_number('12'),'P', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YABR','AWAITING BRANCH REQUIREMENT', to_number('3'),'C', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YABR1','AWAITING BRANCH REQUIREMENT', to_number('3'),'P', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YABR2','AWAITING BRANCH REQUIREMENT', to_number('3'),'C', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YABR3','AWAITING BRANCH REQUIREMENT', to_number('3'),'C', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YAB11','AWAITING BRANCH REQUIREMENT', to_number('3'),'C', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YAB12','AWAITING BRANCH REQUIREMENT', to_number('3'),'P', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YAB13','AWAITING BRANCH REQUIREMENT', to_number('3'),'P', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YACR','AGENT''S CONFIDENTIAL REPORT', to_number('12'),'C', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YACR1','AGENT''S CONFIDENTIAL REPORT', to_number('12'),'P', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YAFP','ALPHA FETO PROTEIN', to_number('6'),'C', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YAFP1','ALPHA FETO PROTEIN', to_number('6'),'P', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YAFS','AUDITED FINANCIAL STATEMENT', to_number('12'),'C', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YAFS1','AUDITED FINANCIAL STATEMENT', to_number('12'),'P', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YAGR','AGENT''S REPORT', to_number('12'),'P', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YAGRU','AGENT''S REPORT', to_number('12'),'C', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YAGR1','AGENT''S REPORT', to_number('12'),'P', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YAGR2','AGENT''S REPORT', to_number('12'),'P', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YAGR3','AGENT''S REPORT', to_number('12'),'P', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YAGU1','AGENT''S REPORT', to_number('12'),'C', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YAGU2','AGENT''S REPORT', to_number('12'),'C', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YAGU3','AGENT''S REPORT', to_number('12'),'C', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YAHO','AWAIT HEAD OFFICE', to_number('1'),'C', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YAHO1','AWAIT HEAD OFFICE', to_number('1'),'P', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YAIR','AWAITING INSPECTION REPORT', to_number('12'),'C', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YAIR1','AWAITING INSPECTION REPORT', to_number('12'),'P', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YALQ','ALCOHOL QUESTIONNAIRE', to_number('12'),'C', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YALQ1','ALCOHOL QUESTIONNAIRE', to_number('12'),'P', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YALUS','AWAITING LUS REQUIREMENT', to_number('1'),'C', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YALU1','AWAITING LUS REQUIREMENT', to_number('1'),'P', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YAOAI','ALTERATION OF APPLICATION - I', to_number('12'),'P', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YAOAU','ALTERATION OF APPLICATION - U', to_number('12'),'C', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YAPR','AWAITING PAYOR''S REQUIREMNT', to_number('12'),'C', to_number('3'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YAPR1','AWAITING PAYOR''S REQUIREMENT', to_number('12'),'P', to_number('3'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YAPS','ATTDG PHYSICIAN''S STATEMENT', to_number('3'),'C', to_number('3'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YAPS1','ATTDG PHYSICIAN''S STATEMENT', to_number('3'),'P', to_number('3'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YAPS2','ATTDG PHYSICIAN''S STATEMENT', to_number('3'),'C', to_number('3'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YAPS3','ATTDG PHYSICIAN''S STATEMENT', to_number('3'),'C', to_number('3'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YAP11','ATTDG PHYSICIAN''S STATEMENT', to_number('3'),'C', to_number('3'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YAP12','ATTDG PHYSICIAN''S STATEMENT', to_number('3'),'P', to_number('3'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YAP13','ATTDG PHYSICIAN''S STATEMENT', to_number('3'),'P', to_number('3'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YARQ','AUTO RACING QUESTIONNAIRE', to_number('12'),'C', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YARQ1','AUTO RACING QUESTIONNAIRE', to_number('12'),'P', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YAUH1','MEDICAL AUTHORIZATION', to_number('1'),'P', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YAUR','AWAIT AGE/AMT URINE RESULTS', to_number('3'),'C', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YAUR1','AWAIT AGE/AMT URINE RESULTS', to_number('3'),'P', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YAUTH','MEDICAL AUTHORIZATION', to_number('1'),'C', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YAVQ','AVIATION QUESTIONNAIRE', to_number('12'),'C', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YAVQ1','AVIATION QUESTIONNAIRE', to_number('12'),'P', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YBBDT','BALANCE OF BLOOD TEST', to_number('6'),'C', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YBBT1','BALANCE OF BLOOD TEST', to_number('6'),'P', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YBDR','BORROW FROM DOCTOR', to_number('6'),'C', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YBDR1','BORROW FROM DOCTOR', to_number('6'),'P', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YBIOP','REQUIRE BIOPSY REPORT', to_number('6'),'C', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YBIP1','REQUIRE BIOPSY REPORT', to_number('6'),'P', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YBOA1','BANKERS ORDER', to_number('12'),'P', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YBOPA','BANKERS ORDER', to_number('12'),'C', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YBORR','BORROW RECORD', to_number('1'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YBP1','1 AFTERNOON BLOOD PRESSURE', to_number('3'),'C', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YBP11','1 AFTERNOON BLOOD PRESSURE', to_number('3'),'P', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YBP2','2 AFTERNOON BLOOD PRESSURE', to_number('3'),'C', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YBP21','2 AFTERNOON BLOOD PRESSURE', to_number('3'),'P', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YBR','BOARD RESOLUTION', to_number('12'),'C', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YBR1','BOARD RESOLUTION', to_number('12'),'P', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YCBC','COMPLETE BLOOD COUNT', to_number('6'),'C', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YCBC1','COMPLETE BLOOD COUNT', to_number('6'),'P', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YCOTN','COTININE TEST', to_number('3'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YCOT1','COTININE TEST', to_number('3'),'P', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YCOVE','EXPLAIN SALE IN LETTER', to_number('12'),'C', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YCOV1','EXPLAIN SALE IN LETTER', to_number('12'),'P', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YCPQ','CHEST PAIN QUESTIONNAIRE', to_number('12'),'C', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YCPQ1','CHEST PAIN QUESTIONNAIRE', to_number('12'),'P', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YCRD','MED. EXAMINER''S QUALIFICATION', to_number('12'),'C', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YCRD1','MED. EXAMINER''S QUALIFICATION', to_number('12'),'P', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YCTR','COPY OF TRUST AGREEMENT', to_number('12'),'C', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YCTR1','COPY OF TRUST AGREEMENT', to_number('12'),'P', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YDBQ','DIABETES QUESTIONNAIRE', to_number('12'),'C', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YDBQ1','DIABETES QUESTIONNAIRE', to_number('12'),'P', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YDDA','DIRECT DEBIT AUTHORIZATION', to_number('1'),'C', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YDDA1','DIRECT DEBIT AUTHORIZATION', to_number('1'),'P', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YDECL','DECLARATION RE: SIGNATURE', to_number('12'),'C', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YDEC1','DECLARATION RE: SIGNATURE', to_number('12'),'P', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YDGQ','DIGESTIVE QUESTIONNAIRE', to_number('12'),'C', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YDGQ1','DIGESTIVE QUESTIONNAIRE', to_number('12'),'P', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YDRQ','DRUG USAGE QUESTIONNAIRE', to_number('12'),'C', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YDRQ1','DRUG USAGE QUESTIONNAIRE', to_number('12'),'P', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YDS','DATE OF SIGNING', to_number('3'),'C', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YDS1','DATE OF SIGNING', to_number('3'),'P', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YDUED','SUBMIT AOA RE: DUE DATE', to_number('3'),'C', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YDUE1','SUBMIT AOA RE: DUE DATE', to_number('3'),'P', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YDVQ','DIVING QUESTIONNAIRE', to_number('12'),'C', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YDVQ1','DIVING QUESTIONNAIRE', to_number('12'),'P', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YECG','12 LEAD RESTING EKG', to_number('6'),'C', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YECGT','SUBMIT ECG TRACINGS', to_number('6'),'C', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YECG1','12 LEAD RESTING EKG', to_number('6'),'P', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YECHO','ECHOCARDIOGRAM', to_number('6'),'C', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YECH1','ECHOCARDIOGRAM', to_number('6'),'P', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YECT1','SUBMIT ECG TRACINGS', to_number('6'),'P', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YEOA','EVIDENCE OF AGE', to_number('12'),'C', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YEOA1','EVIDENCE OF AGE', to_number('12'),'P', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YERQ1','MEDICAL EXAMINER''S REPORT QUES', to_number('3'),'P', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YEXA1','EXAM ON ASSURED', to_number('3'),'P', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YEXCL','EXCLUSION CLAUSE', to_number('12'),'C', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YEXC1','EXCLUSION CLAUSE', to_number('12'),'P', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YEXMA','EXAM ON ASSURED', to_number('3'),'C', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YEXMO','EXAM ON OWNER', to_number('3'),'C', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YEXO1','EXAM ON OWNER', to_number('3'),'P', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YEXRQ','MEDICAL EXAMINER''S REPORT QS', to_number('3'),'C', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YEXT','EXTENSION GRANTED', to_number('1'),'C', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YEXT1','EXTENSION GRANTED', to_number('1'),'P', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YFQ','FINANCIAL QUESTIONNAIRE', to_number('12'),'C', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YFQ1','FINANCIAL QUESTIONNAIRE', to_number('12'),'P', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YHBAI','GLYCOSYLATED HEMOGLOBIN', to_number('6'),'C', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YHBA1','GLYCOSYLATED HEMOGLOBIN', to_number('6'),'P', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YHCH','HEART CHART BY MD', to_number('6'),'C', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YHCH1','HEART CHART BY MD', to_number('6'),'P', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YHEPA','BLOOD TEST FOR HBSAG AND HBEAG', to_number('6'),'C', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YHEPB','HBSAG,HBEAG,SGOT,SGPT,GGT,AFP', to_number('6'),'C', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YHEP1','BLOOD TEST FOR HBSAG AND HBEAG', to_number('6'),'P', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YHIV','BLOOD TEST FOR HIV', to_number('6'),'C', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YHIV1','BLOOD TEST FOR HIV', to_number('6'),'P', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YHPB1','HBSAG,HBEAG,SGOT,SGPT,GGT,AFP', to_number('6'),'P', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YID','IDENTIFICATION (HK)', to_number('12'),'C', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YID1','IDENTIFICATION (HK)', to_number('12'),'P', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YIES','INFORMATION EXCHANGE SYSTEM', to_number('6'),'C', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YIES1','INFORMATION EXCHANGE SYSTEM', to_number('6'),'P', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YINC','AMOUNT OF INCOME', to_number('12'),'C', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YINC1','AMOUNT OF INCOME', to_number('12'),'P', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YITR','INCOME TAX RETURN', to_number('12'),'C', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YITR1','INCOME TAX RETURN', to_number('12'),'P', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YLCQ','LOSS OF CONSCIOUSNESS QUES', to_number('12'),'C', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YLCQ1','LOSS OF CONSCIOUSNESS QUES', to_number('12'),'P', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YLFTS','BLD TEST FOR SGOT, SGPT \& GGT', to_number('6'),'C', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YLFT1','BLD TEST FOR SGOT, SGPT \& GGT', to_number('6'),'P', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YLOAN','LOAN AGREEMENT FORM', to_number('12'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YLOCI','LOCATION - ISSUE', to_number('12'),'P', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YLOCU','LOCATION - UNDERWRITING', to_number('12'),'C', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YLUSR','LUS REJECT', to_number('12'),'C', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YMCQ','MOUNTAIN CLIMBING QUES', to_number('12'),'C', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YMCQ1','MOUNTAIN CLIMBING QUES', to_number('12'),'P', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YMEDX','EXAM NOT YET RECEIVED', to_number('3'),'C', to_number('3'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YMED1','EXAM NOT YET RECEIVED', to_number('3'),'P', to_number('3'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YMIB','MEDICAL INFORMATION BUREAU', to_number('12'),'C', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YMIB1','MEDICAL INFORMATION BUREAU', to_number('12'),'P', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YMICR','MICRO-ALBUMIN', to_number('3'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YMIC1','MICRO-ALBUMIN', to_number('3'),'P', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YNIC','NICOTINE TEST', to_number('3'),'C', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YNIC1','NICOTINE TEST', to_number('3'),'P', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YNMA','NON MED ON ASSURED', to_number('3'),'P', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YNMOW','NON MED ON OWNER', to_number('3'),'P', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YNPI','NEW PART I', to_number('3'),'C', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YNPI1','NEW PART I', to_number('3'),'P', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YOFF','ADVICE OF OFFER', to_number('1'),'P', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YOOO1','OTHER REQUIREMENTS', to_number('12'),'C', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YOOO2','OTHER REQUIREMENTS', to_number('12'),'C', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YOO11','OTHER REQUIREMENTS', to_number('12'),'P', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YOO12','OTHER REQUIREMENT', to_number('12'),'P', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YPARA','PARAMEDICAL', to_number('3'),'C', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YPAR1','PARAMEDICAL', to_number('3'),'P', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YPAY1','AWTG. ADDT''L./INITIAL PREMIUM', to_number('1'),'P', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YPBM','PREMIUM BELOW MINIMUM', to_number('1'),'P', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YPCI','PERSONAL CERT OF INSURABILITY', to_number('3'),'C', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YPCI1','PERSONAL CERT OF INSURABILITY', to_number('3'),'P', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YPFP','PERSONAL FINANCIAL PLANNING', to_number('12'),'C', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YPFP1','PERSONAL FINANCIAL PLANNING', to_number('12'),'P', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YPFT','PULMONARY FUNCTION TEST', to_number('6'),'C', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YPFT1','PULMONARY FUNCTION TEST', to_number('6'),'P', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YPNE','POLICY NUMBER OF EXISTING', to_number('12'),'C', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YPNE1','POLICY NUMBER OF EXISTING', to_number('12'),'P', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YPOO','PROOF OF OWNERSHIP (TCT ETC.)', to_number('12'),'C', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YPOO1','PROOF OF OWNERSHIP (TCT ETC.)', to_number('12'),'P', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YPOS','PROOF OF STATUS OF STAY', to_number('12'),'C', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YPOS1','PROOF OF STATUS OF STAY', to_number('12'),'P', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YPREF','REQUEST PREVIOUS FILE', to_number('1'),'C', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YPRE1','EXAM BY PREFERRED', to_number('3'),'P', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YPRM','COLLECT PREMIUM', to_number('1'),'C', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YPRM1','COLLECT PREMIUM', to_number('1'),'P', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YPRV1','REQUEST PREVIOUS FILE', to_number('1'),'P', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YPSA','PROSTATIC SPECIFIC ANTIGEN', to_number('3'),'C', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YPSA1','PROSTATIC SPECIFIC ANTIGEN', to_number('3'),'P', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YPSQ','PARACHUTING/SKYDIVING QUES', to_number('12'),'C', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YPSQ1','PARACHUTING/SKYDIVING QUES', to_number('12'),'P', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YRAP','NAME \& ADD OF REG ATTDG DOCTOR', to_number('1'),'C', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YRAP1','NAME \& ADD OF REG ATTDG DOCTOR', to_number('1'),'P', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YREBE','RELATIONSHIP OF BENEFICIARIES', to_number('12'),'C', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YREB1','RELATIONSHIP OF BENEFICIARIES', to_number('12'),'P', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YRECD','RECEIVED APPLICATION BY USD', to_number('12'),'C', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YRECO','RECONSIDERATION', to_number('1'),'C', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YREC1','RECONSIDERATION', to_number('1'),'P', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YRED1','RECEIVED APPLICATION BY USD', to_number('12'),'P', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YREI1','REINSURANCE - LNL', to_number('1'),'P', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YREI2','REINSURANCE - MANURE', to_number('1'),'P', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YREI3','REINSURANCE - UNIRE', to_number('1'),'P', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YRET1','RETURN PART I', to_number('3'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YRET2','RETURN PART II', to_number('3'),'C', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YREXM','REQUIRE EXAM ATTENTION TO:', to_number('3'),'C', to_number('3'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YREX1','REQUIRE EXAM ATTENTION TO:', to_number('3'),'P', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YRFT','BLD TEST FOR BUN \& CREATININE', to_number('6'),'C', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YRFT1','BLD TEST FOR BUN \& CREATININE', to_number('6'),'P', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YRIR','AWAITING INSPECTION REPORT', to_number('12'),'C', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YRIR1','AWAITING INSPECTION REPORT', to_number('12'),'P', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YRMO','REFERRED FOR MEDICAL OPINION', to_number('1'),'C', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YRMO1','REFERRED FOR MEDICAL OPINION', to_number('1'),'P', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YRUR','REQUIRE MICROURINALYSIS', to_number('6'),'C', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YRUR1','REQUIRE MICROURINALYSIS', to_number('6'),'P', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YRUR2','2 MICROS ON 2 DIFFERENT DAYS', to_number('3'),'C', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YRU21','2 MICROS ON 2 DIFFERENT DAYS', to_number('3'),'P', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YSIGN','SIGNATURE MISSING', to_number('1'),'C', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YSIG1','SIGNATURE MISSING', to_number('1'),'P', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YSLIM','SEE SLIMS:', to_number('1'),'C', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YSLI1','SEE SLIMS:', to_number('1'),'P', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YSM13','SUNLIFE 13', to_number('6'),'C', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YSM31','SUNLIFE 13', to_number('6'),'P', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YSPDR','SPEEDY REJECT', to_number('12'),'C', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YSPLT','EXAM BY SPECIAL LIMITED', to_number('3'),'C', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YSPL1','EXAM BY SPECIAL LIMITED', to_number('3'),'P', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YSPNO','EXAM BY SPECIAL NO LIMIT', to_number('3'),'C', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YSPN1','EXAM BY SPECIAL NO LIMIT', to_number('3'),'P', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YSPS','SIGNATURE ON PROPER SPACES', to_number('3'),'C', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YSPS1','SIGNATURE ON PROPER SPACES', to_number('3'),'P', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YSREM','PAY SHORT REMITTANCE', to_number('1'),'P', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YSUBE','SURNAME OF BENEFICIARIES', to_number('12'),'C', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YSUB1','SURNAME OF BENEFICIARIES', to_number('12'),'P', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YTC','TRANSLATION CLAUSE', to_number('12'),'C', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YTC1','TRANSLATION CLAUSE', to_number('12'),'P', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YTHAL','TREADMILL WITH THALLIUM', to_number('6'),'C', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YTHA1','TREADMILL WITH THALLIUM', to_number('6'),'P', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YTM','TREADMILL', to_number('6'),'C', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YTMTR','SUBMIT TREADMILL TRACINGS', to_number('6'),'C', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YTMT1','SUBMIT TREADMILL TRACINGS', to_number('6'),'P', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YTM1','TREADMILL', to_number('6'),'P', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YTRD','TRUST DECLARATION', to_number('3'),'C', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YTRD1','TRUST DECLARATION', to_number('3'),'P', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YUWD1','UNDERWRITING DECISION', to_number('1'),'P', to_number('1'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YWARN','GET PREV FILE-RECHECK DECISION', to_number('99'),'C', to_number('3'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YXRP1','SUBMIT CHEST X-RAY PLATE', to_number('6'),'P', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YXRY','CHEST X-RAY', to_number('6'),'C', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YXRYP','SUBMIT CHEST X-RAY PLATE', to_number('6'),'C', to_number('2'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_DESC,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('YXRY1','CHEST X-RAY', to_number('6'),'P', to_number('2'));
--no description
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('MEDEQ', to_number('1'),'C', to_number('0'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('PULMO', to_number('1'),'C', to_number('3'));
INSERT INTO REQUIREMENTS (REQT_CODE,REQT_VALIDITY,REQT_LEVEL,REQT_FOLLOW_UP_NUM) VALUES ('R', to_number('1'),'C', to_number('3'));

/* old values
INSERT INTO REQUIREMENTS (REQT_CODE, REQT_DESC, REQT_LEVEL, REQT_VALIDITY, REQT_FORM_IND, REQT_FORM_ID, REQT_FOLLOW_UP_NUM)
VALUES('AOO', 'Acceptance of Offer', 'C', 6, '1', 1, 9);
INSERT INTO REQUIREMENTS (REQT_CODE, REQT_DESC, REQT_LEVEL, REQT_VALIDITY, REQT_FORM_IND, REQT_FORM_ID, REQT_FOLLOW_UP_NUM)
VALUES('OFF', 'ACCEPTANCE OF OFFER W/ EXCLUSION PROVISION', 'C', 3, '1', 2, 9);
INSERT INTO REQUIREMENTS (REQT_CODE, REQT_DESC, REQT_LEVEL, REQT_VALIDITY, REQT_FORM_IND, REQT_FORM_ID, REQT_FOLLOW_UP_NUM)
VALUES('AMLA', 'ADDTL REQTS FOR THE APPLICANT UNDER THE ANTI-MONEY LAUNDERING ACT', 'C', 6, '1', 3, 9);
INSERT INTO REQUIREMENTS (REQT_CODE, REQT_DESC, REQT_LEVEL, REQT_VALIDITY, REQT_FORM_IND, REQT_FORM_ID, REQT_FOLLOW_UP_NUM)
VALUES('ACR', 'Agent''s Confidential Report', 'C', 6, '1', 4, 9);
INSERT INTO REQUIREMENTS (REQT_CODE, REQT_DESC, REQT_LEVEL, REQT_VALIDITY, REQT_FORM_IND, REQT_FORM_ID, REQT_FOLLOW_UP_NUM)
VALUES('AOAU', 'Alteration of Application', 'P', 6, '1', 5, 9);
INSERT INTO REQUIREMENTS (REQT_CODE, REQT_DESC, REQT_LEVEL, REQT_VALIDITY, REQT_FORM_IND, REQT_FORM_ID, REQT_FOLLOW_UP_NUM)
VALUES('ARTQ', 'Arthritis Questionnaire', 'C', 6, '1', 6, 9);
INSERT INTO REQUIREMENTS (REQT_CODE, REQT_DESC, REQT_LEVEL, REQT_VALIDITY, REQT_FORM_IND, REQT_FORM_ID, REQT_FOLLOW_UP_NUM)
VALUES('APS', 'Attending Physician''s Statement', 'C', 6, '1', 7, 9);
INSERT INTO REQUIREMENTS (REQT_CODE, REQT_DESC, REQT_LEVEL, REQT_VALIDITY, REQT_FORM_IND, REQT_FORM_ID, REQT_FOLLOW_UP_NUM)
VALUES('ARQ', 'Attending Physician''s Statement', 'C', 6, '1', 8, 9);
INSERT INTO REQUIREMENTS (REQT_CODE, REQT_DESC, REQT_LEVEL, REQT_VALIDITY, REQT_FORM_IND, REQT_FORM_ID, REQT_FOLLOW_UP_NUM)
VALUES('AVQ', 'Aviation Questionnaire', 'C', 6, '1', 9, 9);
INSERT INTO REQUIREMENTS (REQT_CODE, REQT_DESC, REQT_LEVEL, REQT_VALIDITY, REQT_FORM_IND, REQT_FORM_ID, REQT_FOLLOW_UP_NUM)
VALUES('FQ', 'Business Financial Questionnaire', 'C', 6, '1', 10, 9);
INSERT INTO REQUIREMENTS (REQT_CODE, REQT_DESC, REQT_LEVEL, REQT_VALIDITY, REQT_FORM_IND, REQT_FORM_ID, REQT_FOLLOW_UP_NUM)
VALUES('PCI', 'Certificate of Insurability (age 16 and over)', 'C', 6, '1', 11, 9);
INSERT INTO REQUIREMENTS (REQT_CODE, REQT_DESC, REQT_LEVEL, REQT_VALIDITY, REQT_FORM_IND, REQT_FORM_ID, REQT_FOLLOW_UP_NUM)
VALUES('PCI1', 'Certificate of Insurability (child under age 16)', 'C', 6, '1', 12, 9);
INSERT INTO REQUIREMENTS (REQT_CODE, REQT_DESC, REQT_LEVEL, REQT_VALIDITY, REQT_FORM_IND, REQT_FORM_ID, REQT_FOLLOW_UP_NUM)
VALUES('DECL1', 'Certification re: New Signature', 'C', 6, '1', 13, 9);
INSERT INTO REQUIREMENTS (REQT_CODE, REQT_DESC, REQT_LEVEL, REQT_VALIDITY, REQT_FORM_IND, REQT_FORM_ID, REQT_FOLLOW_UP_NUM)
VALUES('DVQ', 'Diving Questionnaire', 'C', 6, '1', 15, 9);
INSERT INTO REQUIREMENTS (REQT_CODE, REQT_DESC, REQT_LEVEL, REQT_VALIDITY, REQT_FORM_IND, REQT_FORM_ID, REQT_FOLLOW_UP_NUM)
VALUES('DRQ', 'Drug Usage Questionnaire', 'C', 6, '1', 16, 9);
INSERT INTO REQUIREMENTS (REQT_CODE, REQT_DESC, REQT_LEVEL, REQT_VALIDITY, REQT_FORM_IND, REQT_FORM_ID, REQT_FOLLOW_UP_NUM)
VALUES('MCQ', 'Mountain Climbing Questionnaire', 'C', 6, '1', 20, 9);
INSERT INTO REQUIREMENTS (REQT_CODE, REQT_DESC, REQT_LEVEL, REQT_VALIDITY, REQT_FORM_IND, REQT_FORM_ID, REQT_FOLLOW_UP_NUM)
VALUES('PSQ', 'Parachuting/Sky Diving Questionnaire', 'C', 6, '1', 21, 9);
INSERT INTO REQUIREMENTS (REQT_CODE, REQT_DESC, REQT_LEVEL, REQT_VALIDITY, REQT_FORM_IND, REQT_FORM_ID, REQT_FOLLOW_UP_NUM)
VALUES('EXMA1', 'Part II of Application for Insurance on Chile age 15 and  under', 'C', 6, '1', 22, 9);
INSERT INTO REQUIREMENTS (REQT_CODE, REQT_DESC, REQT_LEVEL, REQT_VALIDITY, REQT_FORM_IND, REQT_FORM_ID, REQT_FOLLOW_UP_NUM)
VALUES('EXMA', 'Part II for Insurance on Adult', 'C', 6, '1', 23, 9);
INSERT INTO REQUIREMENTS (REQT_CODE, REQT_DESC, REQT_LEVEL, REQT_VALIDITY, REQT_FORM_IND, REQT_FORM_ID, REQT_FOLLOW_UP_NUM)
VALUES('PFQ', 'Personal Financial Questionnaire', 'C', 6, '1', 24, 9);
INSERT INTO REQUIREMENTS (REQT_CODE, REQT_DESC, REQT_LEVEL, REQT_VALIDITY, REQT_FORM_IND, REQT_FORM_ID, REQT_FOLLOW_UP_NUM)
VALUES('RESQ', 'Residential Background Questionnaire', 'C', 6, '1', 26, 9);
INSERT INTO REQUIREMENTS (REQT_CODE, REQT_DESC, REQT_LEVEL, REQT_VALIDITY, REQT_FORM_IND, REQT_FORM_ID, REQT_FOLLOW_UP_NUM)
VALUES('SCERT', 'Secretary''s Certificate', 'C', 6, '0', 27, 9);
INSERT INTO REQUIREMENTS (REQT_CODE, REQT_DESC, REQT_LEVEL, REQT_VALIDITY, REQT_FORM_IND, REQT_FORM_ID, REQT_FOLLOW_UP_NUM)
VALUES('SPA', 'Special Power of Attorney', 'P', 6, '0', 28, 9);
INSERT INTO REQUIREMENTS (REQT_CODE, REQT_DESC, REQT_LEVEL, REQT_VALIDITY, REQT_FORM_IND, REQT_FORM_ID, REQT_FOLLOW_UP_NUM)
VALUES('ABQ', 'Supp Questns Asthma, Bronchitis or other Pul', 'C', 6, '1', 29, 9);
INSERT INTO REQUIREMENTS (REQT_CODE, REQT_DESC, REQT_LEVEL, REQT_VALIDITY, REQT_FORM_IND, REQT_FORM_ID, REQT_FOLLOW_UP_NUM)
VALUES('CPQ', 'Supplementary Questions concerning Chest Pain or Discomfort', 'C', 6, '1', 30, 9);
INSERT INTO REQUIREMENTS (REQT_CODE, REQT_DESC, REQT_LEVEL, REQT_VALIDITY, REQT_FORM_IND, REQT_FORM_ID, REQT_FOLLOW_UP_NUM)
VALUES('DBQ', 'Suppl Questns Diabetes(or suspctd Diabts', 'C', 6, '1', 31, 9);
INSERT INTO REQUIREMENTS (REQT_CODE, REQT_DESC, REQT_LEVEL, REQT_VALIDITY, REQT_FORM_IND, REQT_FORM_ID, REQT_FOLLOW_UP_NUM)
VALUES('DGQ', 'Suppl Questnr Digestive or BowelDisorder', 'C', 6, '1', 32, 9);
INSERT INTO REQUIREMENTS (REQT_CODE, REQT_DESC, REQT_LEVEL, REQT_VALIDITY, REQT_FORM_IND, REQT_FORM_ID, REQT_FOLLOW_UP_NUM)
VALUES('LCQ', 'Suppl Questns Fainting or Episodes of', 'C', 6, '1', 33, 9);
INSERT INTO REQUIREMENTS (REQT_CODE, REQT_DESC, REQT_LEVEL, REQT_VALIDITY, REQT_FORM_IND, REQT_FORM_ID, REQT_FOLLOW_UP_NUM)
VALUES('ALQ', 'Suppl Questns Concerning use of Alcohol', 'C', 6, '1', 34, 9);
INSERT INTO REQUIREMENTS (REQT_CODE, REQT_DESC, REQT_LEVEL, REQT_VALIDITY, REQT_FORM_IND, REQT_FORM_ID, REQT_FOLLOW_UP_NUM)
VALUES('FMBQ', 'Suppl Questns for Female/Maternity Benft', 'C', 6, '1', 35, 9);
INSERT INTO REQUIREMENTS (REQT_CODE, REQT_DESC, REQT_LEVEL, REQT_VALIDITY, REQT_FORM_IND, REQT_FORM_ID, REQT_FOLLOW_UP_NUM)
VALUES('CYSQ', 'Suppl Questns re Growth, Cysts, Lumps and', 'C', 6, '1', 36, 9);
INSERT INTO REQUIREMENTS (REQT_CODE, REQT_DESC, REQT_LEVEL, REQT_VALIDITY, REQT_FORM_IND, REQT_FORM_ID, REQT_FOLLOW_UP_NUM)
VALUES('GYNQ', 'Suppl Questns re Gynecological Complaints', 'C', 6, '1', 37, 9);
INSERT INTO REQUIREMENTS (REQT_CODE, REQT_DESC, REQT_LEVEL, REQT_VALIDITY, REQT_FORM_IND, REQT_FORM_ID, REQT_FOLLOW_UP_NUM)
VALUES('WPBA', 'Waiver of Benefit APDB (form for advance payment disability benefit', 'P', 6, '1', 38, 9);
INSERT INTO REQUIREMENTS (REQT_CODE, REQT_DESC, REQT_LEVEL, REQT_VALIDITY, REQT_FORM_IND, REQT_FORM_ID, REQT_FOLLOW_UP_NUM)
VALUES('WPBC', 'Waiver of Benefit CCR (form for critical condition rider)', 'P', 6, '1', 39, 9);
INSERT INTO REQUIREMENTS (REQT_CODE, REQT_DESC, REQT_LEVEL, REQT_VALIDITY, REQT_FORM_IND, REQT_FORM_ID, REQT_FOLLOW_UP_NUM)
VALUES('WPBF', 'Waiver of Benefit F and MB (form for female and maternity benefit)', 'P', 6, '1', 40, 9);
INSERT INTO REQUIREMENTS (REQT_CODE, REQT_DESC, REQT_LEVEL, REQT_VALIDITY, REQT_FORM_IND, REQT_FORM_ID, REQT_FOLLOW_UP_NUM)
VALUES('WPBH', 'Waiver of Benefit HIB (form for hospital)', 'C', 6, '1', 41, 9);
*/


-- KICKOUT_CODES
PROMPT Inserting to KICKOUT_CODES
INSERT INTO KICKOUT_CODES  (KO_SEQ, KO_MESSAGE) VALUES (1,'Planholder''s Age Out of Range');
INSERT INTO KICKOUT_CODES  (KO_SEQ, KO_MESSAGE) VALUES (2,'Non-medical Limit (Short Form) Required');
INSERT INTO KICKOUT_CODES  (KO_SEQ, KO_MESSAGE) VALUES (3,'Non-medical Limit (Long Form) Required');
INSERT INTO KICKOUT_CODES  (KO_SEQ, KO_MESSAGE) VALUES (4,'Full Medical Examination/Blood Test/Microurinalysis Required');
INSERT INTO KICKOUT_CODES  (KO_SEQ, KO_MESSAGE) VALUES (5,'Retention Limit Exceeded');
INSERT INTO KICKOUT_CODES  (KO_SEQ, KO_MESSAGE) VALUES (6,'ECG Required');
INSERT INTO KICKOUT_CODES  (KO_SEQ, KO_MESSAGE) VALUES (7,'Chest X-Ray Required');
INSERT INTO KICKOUT_CODES  (KO_SEQ, KO_MESSAGE) VALUES (8,'Treadmill Required');
INSERT INTO KICKOUT_CODES  (KO_SEQ, KO_MESSAGE) VALUES (9,'MIB Search is Exact Match');
INSERT INTO KICKOUT_CODES  (KO_SEQ, KO_MESSAGE) VALUES (10,'MIB Search is Partial Match');
INSERT INTO KICKOUT_CODES  (KO_SEQ, KO_MESSAGE) VALUES (11,'Question Omitted - D\&R 1 is yes or omitted');
INSERT INTO KICKOUT_CODES  (KO_SEQ, KO_MESSAGE) VALUES (12,'Question Omitted - D\&R 2 is yes or omitted');
INSERT INTO KICKOUT_CODES  (KO_SEQ, KO_MESSAGE) VALUES (13,'Question Omitted - D\&R 3 is yes or omitted');
INSERT INTO KICKOUT_CODES  (KO_SEQ, KO_MESSAGE) VALUES (14,'Question Omitted - D\&R 1 and 2 are yes or omitted');
INSERT INTO KICKOUT_CODES  (KO_SEQ, KO_MESSAGE) VALUES (15,'Question Omitted - D\&R 1 and 3 are yes or omitted');
INSERT INTO KICKOUT_CODES  (KO_SEQ, KO_MESSAGE) VALUES (16,'Question Omitted - D\&R 2 and 3 are yes or omitted');
INSERT INTO KICKOUT_CODES  (KO_SEQ, KO_MESSAGE) VALUES (17,'Question Omitted - D\&R 1, 2 and 3 are yes or omitted');
INSERT INTO KICKOUT_CODES  (KO_SEQ, KO_MESSAGE) VALUES (18,'Agent''s Report Required - Birthplace is missing');
INSERT INTO KICKOUT_CODES  (KO_SEQ, KO_MESSAGE) VALUES (19,'Agent''s Report Required - Occupation is missing');
INSERT INTO KICKOUT_CODES  (KO_SEQ, KO_MESSAGE) VALUES (20,'Agent''s Report Required - Religion is missing');
INSERT INTO KICKOUT_CODES  (KO_SEQ, KO_MESSAGE) VALUES (21,'Agent''s Report Required - Citizenship is missing');
INSERT INTO KICKOUT_CODES  (KO_SEQ, KO_MESSAGE) VALUES (22,'Agent''s Report Required - Birthplace and Occupation is missing');
INSERT INTO KICKOUT_CODES  (KO_SEQ, KO_MESSAGE) VALUES (23,'Agent''s Report Required - Birthplace and Religion are missing');
INSERT INTO KICKOUT_CODES  (KO_SEQ, KO_MESSAGE) VALUES (24,'Agent''s Report Required - Birthplace and Citizenship are missing');
INSERT INTO KICKOUT_CODES  (KO_SEQ, KO_MESSAGE) VALUES (25,'Agent''s Report Required - Occupation and Religion are missing');
INSERT INTO KICKOUT_CODES  (KO_SEQ, KO_MESSAGE) VALUES (26,'Agent''s Report Required - Occupation and Citizenship are missing');
INSERT INTO KICKOUT_CODES  (KO_SEQ, KO_MESSAGE) VALUES (27,'Agent''s Report Required - Religion and Citizenship are missing');
INSERT INTO KICKOUT_CODES  (KO_SEQ, KO_MESSAGE) VALUES (28,'Agent''s Report Required - Birthplace, Occupation and Religion are missing');
INSERT INTO KICKOUT_CODES  (KO_SEQ, KO_MESSAGE) VALUES (29,'Agent''s Report Required - Birthplace, Occupation and Citizenship are missing');
INSERT INTO KICKOUT_CODES  (KO_SEQ, KO_MESSAGE) VALUES (30,'Agent''s Report Required - Birthplace, Religion and Citizenship are missing');
INSERT INTO KICKOUT_CODES  (KO_SEQ, KO_MESSAGE) VALUES (31,'Agent''s Report Required - Occupation, Religion and Citizenship are missing');
INSERT INTO KICKOUT_CODES  (KO_SEQ, KO_MESSAGE) VALUES (32,'Agent''s Report Required - Birthplace, Occupation, Religion and Citizenship are missing');
INSERT INTO KICKOUT_CODES  (KO_SEQ, KO_MESSAGE) VALUES (33,'AMLA Form Required');
INSERT INTO KICKOUT_CODES  (KO_SEQ, KO_MESSAGE) VALUES (34,'Insurance Riders Forms Required');
INSERT INTO KICKOUT_CODES  (KO_SEQ, KO_MESSAGE) VALUES (35,'Enrolment Form for Monthly Mode and 11 PDCs Required');
INSERT INTO KICKOUT_CODES  (KO_SEQ, KO_MESSAGE) VALUES (36,'Agent Sharing of Commission Agreement Required');
INSERT INTO KICKOUT_CODES  (KO_SEQ, KO_MESSAGE) VALUES (37,'Deed of Agreement and Secretary''s Certificate Required');
INSERT INTO KICKOUT_CODES  (KO_SEQ, KO_MESSAGE) VALUES (38,'Failed Test - Citizenship is Not Filipino');
INSERT INTO KICKOUT_CODES  (KO_SEQ, KO_MESSAGE) VALUES (39,'Failed Test - Religion is Muslim');
INSERT INTO KICKOUT_CODES  (KO_SEQ, KO_MESSAGE) VALUES (40,'Failed Test - Application Sign Date > 90 days');
INSERT INTO KICKOUT_CODES  (KO_SEQ, KO_MESSAGE) VALUES (41,'Failed Test - Planholder Permanent Address is Outside of the Philippines');
INSERT INTO KICKOUT_CODES  (KO_SEQ, KO_MESSAGE) VALUES (42,'Failed Test - No Beneficiary Relationship Defined');
INSERT INTO KICKOUT_CODES  (KO_SEQ, KO_MESSAGE) VALUES (43,'Failed Test - Beneficiary Relationship is Employer or Others');
INSERT INTO KICKOUT_CODES  (KO_SEQ, KO_MESSAGE) VALUES (44,'Failed Test -  Payment > Speedy Approval Payment Tolerance');
INSERT INTO KICKOUT_CODES  (KO_SEQ, KO_MESSAGE) VALUES (45,'Failed Test -  Payment > No Payment');
INSERT INTO KICKOUT_CODES  (KO_SEQ, KO_MESSAGE) VALUES (46,'Failed Test -  Insufficient Deposit');
INSERT INTO KICKOUT_CODES  (KO_SEQ, KO_MESSAGE) VALUES (47,'Failed Test -  One or More Existing Rider is Not Standard');
INSERT INTO KICKOUT_CODES  (KO_SEQ, KO_MESSAGE) VALUES (48,'Writing Agent is Not Active');
INSERT INTO KICKOUT_CODES  (KO_SEQ, KO_MESSAGE) VALUES (49,'Failed Test -  Occupation is Not Standard');
INSERT INTO KICKOUT_CODES  (KO_SEQ, KO_MESSAGE) VALUES (50,'Failed Test -  Planholder Business Address is Foreign');



-- PRISM CODEC
PROMPT Inserting to PRISM_CODEC
INSERT INTO PRISM_CODEC (PC_SEQ, PC_POSITION, PC_CODE, PC_DESC) VALUES (1,1,'O','Out of the set speedy approval age limit.');
INSERT INTO PRISM_CODEC (PC_SEQ, PC_POSITION, PC_CODE, PC_DESC) VALUES (2,2,'S','Non-Medical Limit Short Form');
INSERT INTO PRISM_CODEC (PC_SEQ, PC_POSITION, PC_CODE, PC_DESC) VALUES (3,2,'L','Non-Medical Limit Long Form - Non-medical questionnaire for Group Insurance');
INSERT INTO PRISM_CODEC (PC_SEQ, PC_POSITION, PC_CODE, PC_DESC) VALUES (4,2,'1','Routine requirement - full exam only.');
INSERT INTO PRISM_CODEC (PC_SEQ, PC_POSITION, PC_CODE, PC_DESC) VALUES (5,2,'2','Routine requirement - full exam and ECG.');
INSERT INTO PRISM_CODEC (PC_SEQ, PC_POSITION, PC_CODE, PC_DESC) VALUES (6,2,'3','Routine requirement - full exam, ECG and chest x-ray.');
INSERT INTO PRISM_CODEC (PC_SEQ, PC_POSITION, PC_CODE, PC_DESC) VALUES (7,2,'4','Routine requirement - full exam, ECG and treadmill.');
INSERT INTO PRISM_CODEC (PC_SEQ, PC_POSITION, PC_CODE, PC_DESC) VALUES (8,2,'5','Routine requirement - full exam, ECG chest x-ray and treadmill.');
INSERT INTO PRISM_CODEC (PC_SEQ, PC_POSITION, PC_CODE, PC_DESC) VALUES (9,3,'E','MIB search result (with exact match)');
INSERT INTO PRISM_CODEC (PC_SEQ, PC_POSITION, PC_CODE, PC_DESC) VALUES (10,3,'M','MIB search result (with partial matches)');
INSERT INTO PRISM_CODEC (PC_SEQ, PC_POSITION, PC_CODE, PC_DESC) VALUES (11,4,'1','D\&R 1 is yes, 2 and 3 are no');
INSERT INTO PRISM_CODEC (PC_SEQ, PC_POSITION, PC_CODE, PC_DESC) VALUES (12,4,'2','D\&R 2 is yes, 1 and 3 are no');
INSERT INTO PRISM_CODEC (PC_SEQ, PC_POSITION, PC_CODE, PC_DESC) VALUES (13,4,'3','D\&R 3 is yes, 1 and 2 are no');
INSERT INTO PRISM_CODEC (PC_SEQ, PC_POSITION, PC_CODE, PC_DESC) VALUES (14,4,'4','D\&R 1 and 2 are yes, 3 is no');
INSERT INTO PRISM_CODEC (PC_SEQ, PC_POSITION, PC_CODE, PC_DESC) VALUES (15,4,'5','D\&R 1 and 3 are yes, 2 is no');
INSERT INTO PRISM_CODEC (PC_SEQ, PC_POSITION, PC_CODE, PC_DESC) VALUES (16,4,'6','D\&R 2 and 3 are yes, 1 is no');
INSERT INTO PRISM_CODEC (PC_SEQ, PC_POSITION, PC_CODE, PC_DESC) VALUES (17,4,'7','All D\&R are yes');
INSERT INTO PRISM_CODEC (PC_SEQ, PC_POSITION, PC_CODE, PC_DESC) VALUES (18,5,'A','Agent report checking - undefined birthplace');
INSERT INTO PRISM_CODEC (PC_SEQ, PC_POSITION, PC_CODE, PC_DESC) VALUES (19,5,'B','Agent report checking - undefined occupation');
INSERT INTO PRISM_CODEC (PC_SEQ, PC_POSITION, PC_CODE, PC_DESC) VALUES (20,5,'C','Agent report checking - undefined religion');
INSERT INTO PRISM_CODEC (PC_SEQ, PC_POSITION, PC_CODE, PC_DESC) VALUES (21,5,'D','Agent report checking - undefined citizenship');
INSERT INTO PRISM_CODEC (PC_SEQ, PC_POSITION, PC_CODE, PC_DESC) VALUES (22,5,'E','Agent report checking - undefined birthplace and occupation');
INSERT INTO PRISM_CODEC (PC_SEQ, PC_POSITION, PC_CODE, PC_DESC) VALUES (23,5,'F','Agent report checking - undefined birthplace and religion');
INSERT INTO PRISM_CODEC (PC_SEQ, PC_POSITION, PC_CODE, PC_DESC) VALUES (24,5,'G','Agent report checking - undefined birthplace and citizenship');
INSERT INTO PRISM_CODEC (PC_SEQ, PC_POSITION, PC_CODE, PC_DESC) VALUES (25,5,'H','agent report checking - undefined occupation and religion');
INSERT INTO PRISM_CODEC (PC_SEQ, PC_POSITION, PC_CODE, PC_DESC) VALUES (26,5,'I','Agent report checking - undefined occupation and citizenship');
INSERT INTO PRISM_CODEC (PC_SEQ, PC_POSITION, PC_CODE, PC_DESC) VALUES (27,5,'J','Agent report checking - undefined religion and citizenship');
INSERT INTO PRISM_CODEC (PC_SEQ, PC_POSITION, PC_CODE, PC_DESC) VALUES (28,5,'K','Agent report checking - undefined birthplace, occupation and religion');
INSERT INTO PRISM_CODEC (PC_SEQ, PC_POSITION, PC_CODE, PC_DESC) VALUES (29,5,'L','Agent report checking - undefined birthplace, occupation and citizenship');
INSERT INTO PRISM_CODEC (PC_SEQ, PC_POSITION, PC_CODE, PC_DESC) VALUES (30,5,'M','agent report checking - undefined birthplace, religion and citizenship');
INSERT INTO PRISM_CODEC (PC_SEQ, PC_POSITION, PC_CODE, PC_DESC) VALUES (31,5,'N','Agent report checking - undefined occupation, religion and citizenship');
INSERT INTO PRISM_CODEC (PC_SEQ, PC_POSITION, PC_CODE, PC_DESC) VALUES (32,5,'O','Agent report checking - undefined birthplace, occupation, religion and citizenship');
INSERT INTO PRISM_CODEC (PC_SEQ, PC_POSITION, PC_CODE, PC_DESC) VALUES (33,6,'F','AMLA Form checking');
INSERT INTO PRISM_CODEC (PC_SEQ, PC_POSITION, PC_CODE, PC_DESC) VALUES (34,7,'F','Insurance rider forms checking');
INSERT INTO PRISM_CODEC (PC_SEQ, PC_POSITION, PC_CODE, PC_DESC) VALUES (35,8,'F','Monthly mode requirement checking');
INSERT INTO PRISM_CODEC (PC_SEQ, PC_POSITION, PC_CODE, PC_DESC) VALUES (36,9,'F','Agent sharing requirement checking');
INSERT INTO PRISM_CODEC (PC_SEQ, PC_POSITION, PC_CODE, PC_DESC) VALUES (37,10,'F','Pseudo group requirement checking');
INSERT INTO PRISM_CODEC (PC_SEQ, PC_POSITION, PC_CODE, PC_DESC) VALUES (38,11,'F','Citizenship checking');
INSERT INTO PRISM_CODEC (PC_SEQ, PC_POSITION, PC_CODE, PC_DESC) VALUES (39,12,'F','Religion checking');
INSERT INTO PRISM_CODEC (PC_SEQ, PC_POSITION, PC_CODE, PC_DESC) VALUES (40,13,'F','Signing date checking');
INSERT INTO PRISM_CODEC (PC_SEQ, PC_POSITION, PC_CODE, PC_DESC) VALUES (41,14,'F','Permanent address checking');
INSERT INTO PRISM_CODEC (PC_SEQ, PC_POSITION, PC_CODE, PC_DESC) VALUES (42,15,'M','Beneficiary relationship checking');
INSERT INTO PRISM_CODEC (PC_SEQ, PC_POSITION, PC_CODE, PC_DESC) VALUES (43,15,'F','Beneficiary relationship checking');
INSERT INTO PRISM_CODEC (PC_SEQ, PC_POSITION, PC_CODE, PC_DESC) VALUES (44,16,'O','Payment is posted and over the set speedy approval payment tolerance');
INSERT INTO PRISM_CODEC (PC_SEQ, PC_POSITION, PC_CODE, PC_DESC) VALUES (45,16,'U','Payment is posted and over the set speedy approval payment tolerance');
INSERT INTO PRISM_CODEC (PC_SEQ, PC_POSITION, PC_CODE, PC_DESC) VALUES (46,16,'N','Payment is not yet loaded to PRISM');
INSERT INTO PRISM_CODEC (PC_SEQ, PC_POSITION, PC_CODE, PC_DESC) VALUES (47,16,'I','Application is with insufficient deposit');
INSERT INTO PRISM_CODEC (PC_SEQ, PC_POSITION, PC_CODE, PC_DESC) VALUES (48,17,'N','One of the existing plans is not standard');
INSERT INTO PRISM_CODEC (PC_SEQ, PC_POSITION, PC_CODE, PC_DESC) VALUES (49,18,'N','Agent status is not active');
INSERT INTO PRISM_CODEC (PC_SEQ, PC_POSITION, PC_CODE, PC_DESC) VALUES (50,19,'N','Failed occupation checking');
INSERT INTO PRISM_CODEC (PC_SEQ, PC_POSITION, PC_CODE, PC_DESC) VALUES (51,20,'N','Failed place of work checking');

-- SQL for the Pass Codes
INSERT INTO PRISM_CODEC (PC_SEQ, PC_POSITION, PC_CODE, PC_DESC) VALUES (52,1,'P','Pass');
INSERT INTO PRISM_CODEC (PC_SEQ, PC_POSITION, PC_CODE, PC_DESC) VALUES (53,2,'P','Pass');
INSERT INTO PRISM_CODEC (PC_SEQ, PC_POSITION, PC_CODE, PC_DESC) VALUES (54,3,'P','Pass');
INSERT INTO PRISM_CODEC (PC_SEQ, PC_POSITION, PC_CODE, PC_DESC) VALUES (55,4,'P','Pass');
INSERT INTO PRISM_CODEC (PC_SEQ, PC_POSITION, PC_CODE, PC_DESC) VALUES (56,5,'P','Pass');
INSERT INTO PRISM_CODEC (PC_SEQ, PC_POSITION, PC_CODE, PC_DESC) VALUES (57,6,'P','Pass');
INSERT INTO PRISM_CODEC (PC_SEQ, PC_POSITION, PC_CODE, PC_DESC) VALUES (58,7,'P','Pass');
INSERT INTO PRISM_CODEC (PC_SEQ, PC_POSITION, PC_CODE, PC_DESC) VALUES (59,8,'P','Pass');
INSERT INTO PRISM_CODEC (PC_SEQ, PC_POSITION, PC_CODE, PC_DESC) VALUES (60,9,'P','Pass');
INSERT INTO PRISM_CODEC (PC_SEQ, PC_POSITION, PC_CODE, PC_DESC) VALUES (61,10,'P','Pass');
INSERT INTO PRISM_CODEC (PC_SEQ, PC_POSITION, PC_CODE, PC_DESC) VALUES (62,11,'P','Pass');
INSERT INTO PRISM_CODEC (PC_SEQ, PC_POSITION, PC_CODE, PC_DESC) VALUES (63,12,'P','Pass');
INSERT INTO PRISM_CODEC (PC_SEQ, PC_POSITION, PC_CODE, PC_DESC) VALUES (64,13,'P','Pass');
INSERT INTO PRISM_CODEC (PC_SEQ, PC_POSITION, PC_CODE, PC_DESC) VALUES (65,14,'P','Pass');
INSERT INTO PRISM_CODEC (PC_SEQ, PC_POSITION, PC_CODE, PC_DESC) VALUES (66,15,'P','Pass');
INSERT INTO PRISM_CODEC (PC_SEQ, PC_POSITION, PC_CODE, PC_DESC) VALUES (67,16,'P','Pass');
INSERT INTO PRISM_CODEC (PC_SEQ, PC_POSITION, PC_CODE, PC_DESC) VALUES (68,17,'P','Pass');
INSERT INTO PRISM_CODEC (PC_SEQ, PC_POSITION, PC_CODE, PC_DESC) VALUES (69,18,'P','Pass');
INSERT INTO PRISM_CODEC (PC_SEQ, PC_POSITION, PC_CODE, PC_DESC) VALUES (70,19,'P','Pass');
INSERT INTO PRISM_CODEC (PC_SEQ, PC_POSITION, PC_CODE, PC_DESC) VALUES (71,20,'P','Pass');

-- CODEC_KO_CODEC
PROMPT Inserting to CODEC_KO_CODEC
INSERT INTO CODEC_KO_CODEC (PC_SEQ,KO_SEQ) VALUES (1,1);
INSERT INTO CODEC_KO_CODEC (PC_SEQ,KO_SEQ) VALUES (2,2);
INSERT INTO CODEC_KO_CODEC (PC_SEQ,KO_SEQ) VALUES (3,3);
INSERT INTO CODEC_KO_CODEC (PC_SEQ,KO_SEQ) VALUES (4,3);
INSERT INTO CODEC_KO_CODEC (PC_SEQ,KO_SEQ) VALUES (4,4);
INSERT INTO CODEC_KO_CODEC (PC_SEQ,KO_SEQ) VALUES (4,5);
INSERT INTO CODEC_KO_CODEC (PC_SEQ,KO_SEQ) VALUES (5,3);
INSERT INTO CODEC_KO_CODEC (PC_SEQ,KO_SEQ) VALUES (5,4);
INSERT INTO CODEC_KO_CODEC (PC_SEQ,KO_SEQ) VALUES (5,6);
INSERT INTO CODEC_KO_CODEC (PC_SEQ,KO_SEQ) VALUES (5,5);
INSERT INTO CODEC_KO_CODEC (PC_SEQ,KO_SEQ) VALUES (6,3);
INSERT INTO CODEC_KO_CODEC (PC_SEQ,KO_SEQ) VALUES (6,4);
INSERT INTO CODEC_KO_CODEC (PC_SEQ,KO_SEQ) VALUES (6,6);
INSERT INTO CODEC_KO_CODEC (PC_SEQ,KO_SEQ) VALUES (6,7);
INSERT INTO CODEC_KO_CODEC (PC_SEQ,KO_SEQ) VALUES (6,5);
INSERT INTO CODEC_KO_CODEC (PC_SEQ,KO_SEQ) VALUES (7,3);
INSERT INTO CODEC_KO_CODEC (PC_SEQ,KO_SEQ) VALUES (7,4);
INSERT INTO CODEC_KO_CODEC (PC_SEQ,KO_SEQ) VALUES (7,6);
INSERT INTO CODEC_KO_CODEC (PC_SEQ,KO_SEQ) VALUES (7,8);
INSERT INTO CODEC_KO_CODEC (PC_SEQ,KO_SEQ) VALUES (7,5);
INSERT INTO CODEC_KO_CODEC (PC_SEQ,KO_SEQ) VALUES (8,3);
INSERT INTO CODEC_KO_CODEC (PC_SEQ,KO_SEQ) VALUES (8,4);
INSERT INTO CODEC_KO_CODEC (PC_SEQ,KO_SEQ) VALUES (8,6);
INSERT INTO CODEC_KO_CODEC (PC_SEQ,KO_SEQ) VALUES (8,7);
INSERT INTO CODEC_KO_CODEC (PC_SEQ,KO_SEQ) VALUES (8,8);
INSERT INTO CODEC_KO_CODEC (PC_SEQ,KO_SEQ) VALUES (8,5);
INSERT INTO CODEC_KO_CODEC (PC_SEQ,KO_SEQ) VALUES (9,9);
INSERT INTO CODEC_KO_CODEC (PC_SEQ,KO_SEQ) VALUES (10,10);
INSERT INTO CODEC_KO_CODEC (PC_SEQ,KO_SEQ) VALUES (11,11);
INSERT INTO CODEC_KO_CODEC (PC_SEQ,KO_SEQ) VALUES (12,12);
INSERT INTO CODEC_KO_CODEC (PC_SEQ,KO_SEQ) VALUES (13,13);
INSERT INTO CODEC_KO_CODEC (PC_SEQ,KO_SEQ) VALUES (14,14);
INSERT INTO CODEC_KO_CODEC (PC_SEQ,KO_SEQ) VALUES (15,15);
INSERT INTO CODEC_KO_CODEC (PC_SEQ,KO_SEQ) VALUES (16,16);
INSERT INTO CODEC_KO_CODEC (PC_SEQ,KO_SEQ) VALUES (17,17);
INSERT INTO CODEC_KO_CODEC (PC_SEQ,KO_SEQ) VALUES (18,18);
INSERT INTO CODEC_KO_CODEC (PC_SEQ,KO_SEQ) VALUES (19,19);
INSERT INTO CODEC_KO_CODEC (PC_SEQ,KO_SEQ) VALUES (20,20);
INSERT INTO CODEC_KO_CODEC (PC_SEQ,KO_SEQ) VALUES (21,21);
INSERT INTO CODEC_KO_CODEC (PC_SEQ,KO_SEQ) VALUES (22,22);
INSERT INTO CODEC_KO_CODEC (PC_SEQ,KO_SEQ) VALUES (23,23);
INSERT INTO CODEC_KO_CODEC (PC_SEQ,KO_SEQ) VALUES (24,24);
INSERT INTO CODEC_KO_CODEC (PC_SEQ,KO_SEQ) VALUES (25,25);
INSERT INTO CODEC_KO_CODEC (PC_SEQ,KO_SEQ) VALUES (26,26);
INSERT INTO CODEC_KO_CODEC (PC_SEQ,KO_SEQ) VALUES (27,27);
INSERT INTO CODEC_KO_CODEC (PC_SEQ,KO_SEQ) VALUES (28,28);
INSERT INTO CODEC_KO_CODEC (PC_SEQ,KO_SEQ) VALUES (29,29);
INSERT INTO CODEC_KO_CODEC (PC_SEQ,KO_SEQ) VALUES (30,30);
INSERT INTO CODEC_KO_CODEC (PC_SEQ,KO_SEQ) VALUES (31,31);
INSERT INTO CODEC_KO_CODEC (PC_SEQ,KO_SEQ) VALUES (32,32);
INSERT INTO CODEC_KO_CODEC (PC_SEQ,KO_SEQ) VALUES (33,33);
INSERT INTO CODEC_KO_CODEC (PC_SEQ,KO_SEQ) VALUES (34,34);
INSERT INTO CODEC_KO_CODEC (PC_SEQ,KO_SEQ) VALUES (35,35);
INSERT INTO CODEC_KO_CODEC (PC_SEQ,KO_SEQ) VALUES (36,36);
INSERT INTO CODEC_KO_CODEC (PC_SEQ,KO_SEQ) VALUES (37,37);
INSERT INTO CODEC_KO_CODEC (PC_SEQ,KO_SEQ) VALUES (38,38);
INSERT INTO CODEC_KO_CODEC (PC_SEQ,KO_SEQ) VALUES (39,39);
INSERT INTO CODEC_KO_CODEC (PC_SEQ,KO_SEQ) VALUES (40,40);
INSERT INTO CODEC_KO_CODEC (PC_SEQ,KO_SEQ) VALUES (41,41);
INSERT INTO CODEC_KO_CODEC (PC_SEQ,KO_SEQ) VALUES (42,42);
INSERT INTO CODEC_KO_CODEC (PC_SEQ,KO_SEQ) VALUES (43,43);
INSERT INTO CODEC_KO_CODEC (PC_SEQ,KO_SEQ) VALUES (44,44);
INSERT INTO CODEC_KO_CODEC (PC_SEQ,KO_SEQ) VALUES (45,44);
INSERT INTO CODEC_KO_CODEC (PC_SEQ,KO_SEQ) VALUES (46,45);
INSERT INTO CODEC_KO_CODEC (PC_SEQ,KO_SEQ) VALUES (47,46);
INSERT INTO CODEC_KO_CODEC (PC_SEQ,KO_SEQ) VALUES (48,47);
INSERT INTO CODEC_KO_CODEC (PC_SEQ,KO_SEQ) VALUES (49,48);
INSERT INTO CODEC_KO_CODEC (PC_SEQ,KO_SEQ) VALUES (50,49);
INSERT INTO CODEC_KO_CODEC (PC_SEQ,KO_SEQ) VALUES (51,50);

set escape off

--PAGES
PROMPT INSERT PAGES ................
INSERT INTO pages
VALUES
(1,'Homepage',NULL,NULL,NULL,NULL)
/
INSERT INTO pages
VALUES
(2,'Assessment Requests',NULL,NULL,NULL,NULL)
/
INSERT INTO pages
VALUES
(3,'Assessment Request Detail',NULL,NULL,NULL,NULL)
/
INSERT INTO pages
VALUES
(4,'Requests Details',NULL,NULL,NULL,NULL)
/
INSERT INTO pages
VALUES
(5,'CDS',NULL,NULL,NULL,NULL)
/
INSERT INTO pages
VALUES
(6,'Requirements, KO, Med Rec',NULL,NULL,NULL,NULL)
/
INSERT INTO pages
VALUES
(7,'Underwriter Assessment',NULL,NULL,NULL,NULL)
/
INSERT INTO pages
VALUES
(8,'Doctor''s Notes',NULL,NULL,NULL,NULL)
/
INSERT INTO pages
VALUES
(9,'Medical Records List',NULL,NULL,NULL,NULL)
/
INSERT INTO pages
VALUES
(10,'Medical Record Details',NULL,NULL,NULL,NULL)
/
INSERT INTO pages
VALUES
(11,'Admin Homepage',NULL,NULL,NULL,NULL)
/
INSERT INTO pages
VALUES
(12,'Reference Codes',NULL,NULL,NULL,NULL)
/
INSERT INTO pages
VALUES
(13,'Process Configuration',NULL,NULL,NULL,NULL)
/
INSERT INTO pages
VALUES
(14,'User Profile',NULL,NULL,'IUMADMIN','31-MAR-04 06.26.45.000000 PM')
/
INSERT INTO pages
VALUES
(15,'Exeption Reports',NULL,NULL,NULL,NULL)
/
INSERT INTO pages
VALUES
(16,'Audit Trail Reports',NULL,NULL,NULL,NULL)
/
INSERT INTO pages
VALUES
(17,'Reports',NULL,NULL,NULL,NULL)
/
INSERT INTO pages
VALUES
(18,'Change Password',NULL,NULL,NULL,NULL)
/
INSERT INTO pages
VALUES
(19,'Sunlife Offices',NULL,NULL,NULL,NULL)
/
INSERT INTO pages
VALUES
(20,'Examiners',NULL,NULL,NULL,NULL)
/
INSERT INTO pages
VALUES
(21,'Laboratories',NULL,NULL,NULL,NULL)
/
INSERT INTO pages
VALUES
(22,'Requirements',NULL,NULL,NULL,NULL)
/
INSERT INTO pages
VALUES
(23,'Notification Template',NULL,NULL,NULL,NULL)
/
INSERT INTO pages
VALUES
(24,'Work Function',NULL,NULL,NULL,NULL)
/
INSERT INTO pages
VALUES
(25,'MIB Export',NULL,NULL,NULL,NULL)
/
INSERT INTO pages
VALUES
(26,'Medical Record Expiration',NULL,NULL,NULL,NULL)
/
INSERT INTO pages
VALUES
(27,'Access Template',NULL,NULL,NULL,NULL)
/

/*
INSERT INTO PAGES (PAGE_ID, PAGE_DESC) VALUES (1, 'Homepage');
INSERT INTO PAGES (PAGE_ID, PAGE_DESC) VALUES (2, 'Assessment Requests');
INSERT INTO PAGES (PAGE_ID, PAGE_DESC) VALUES (3, 'Assessment Request Detail');
INSERT INTO PAGES (PAGE_ID, PAGE_DESC) VALUES (4, 'Requests Details');
INSERT INTO PAGES (PAGE_ID, PAGE_DESC) VALUES (5, 'CDS');
INSERT INTO PAGES (PAGE_ID, PAGE_DESC) VALUES (6, 'Requirements, KO, Med Rec');
INSERT INTO PAGES (PAGE_ID, PAGE_DESC) VALUES (7, 'Underwriter Assessment');
INSERT INTO PAGES (PAGE_ID, PAGE_DESC) VALUES (8, 'Doctor''s Notes');
INSERT INTO PAGES (PAGE_ID, PAGE_DESC) VALUES (9, 'Medical Records List');
INSERT INTO PAGES (PAGE_ID, PAGE_DESC) VALUES (10,'Medical Record Details');
INSERT INTO PAGES (PAGE_ID, PAGE_DESC) VALUES (11, 'Admin Homepage');
INSERT INTO PAGES (PAGE_ID, PAGE_DESC) VALUES (12, 'Reference Codes');
INSERT INTO PAGES (PAGE_ID, PAGE_DESC) VALUES (13, 'Process Configuration');
INSERT INTO PAGES (PAGE_ID, PAGE_DESC) VALUES (14, 'User Access');
INSERT INTO PAGES (PAGE_ID, PAGE_DESC) VALUES (15, 'Exception Reports');
INSERT INTO PAGES (PAGE_ID, PAGE_DESC) VALUES (16, 'Audit Trail Reports');
INSERT INTO PAGES (PAGE_ID, PAGE_DESC) VALUES (17, 'Reports');
INSERT INTO PAGES (PAGE_ID, PAGE_DESC) VALUES (18, 'Change Password');
INSERT INTO PAGES (PAGE_ID, PAGE_DESC) VALUES (19, 'Sunlife Offices');
INSERT INTO PAGES (PAGE_ID, PAGE_DESC) VALUES (20, 'Examiners');
INSERT INTO PAGES (PAGE_ID, PAGE_DESC) VALUES (21, 'Laboratories');
INSERT INTO PAGES (PAGE_ID, PAGE_DESC) VALUES (22, 'Requirements');
INSERT INTO PAGES (PAGE_ID, PAGE_DESC) VALUES (23, 'Notification Template');
INSERT INTO PAGES (PAGE_ID, PAGE_DESC) VALUES (24, 'Work Function');
INSERT INTO PAGES (PAGE_ID, PAGE_DESC) VALUES (25, 'MIB Export');
INSERT INTO PAGES (PAGE_ID, PAGE_DESC) VALUES (26, 'Medical Record Expiration');
INSERT INTO PAGES (PAGE_ID, PAGE_DESC) VALUES (27, 'Access Template');
*/

--ACCESSES
PROMPT INSERT ACCESSES ................
INSERT INTO ACCESSES (acc_id, acc_desc) values (1, 'Inquire Only');
INSERT INTO ACCESSES (acc_id, acc_desc) values (2, 'Create Records');
INSERT INTO ACCESSES (acc_id, acc_desc) values (3, 'Maintain Records');
INSERT INTO ACCESSES (acc_id, acc_desc) values (4, 'Delete Records');
INSERT INTO ACCESSES (acc_id, acc_desc) values (5, 'Execute Functions');

-- ACCESS TEMPLATE
PROMPT INSERT ACCESS TEMPLATE ................
		--for Group New Business Admn 
INSERT INTO access_templates (templt_id, role_id,templt_desc) VALUES (seq_access_template.NEXTVAL,'GRPNBADMIN','Template for GRPNBADMIN');
		--for New Bus Staff 
INSERT INTO access_templates (templt_id, role_id,templt_desc) VALUES (seq_access_template.NEXTVAL,'NBSTAFF','Template for NBSTAFF');
		--for Nb Rev 
INSERT INTO access_templates (templt_id, role_id,templt_desc) VALUES (seq_access_template.NEXTVAL,'NBREVIEWER','Template for NBREVIEWER');
		-- for NBSUPMGR 
INSERT INTO access_templates (templt_id, role_id,templt_desc) VALUES (seq_access_template.NEXTVAL,'NBSUPMGR','Templ for NBSUPMGR');
		-- for FACILITSUPMGR 
INSERT INTO access_templates (templt_id, role_id,templt_desc) VALUES (seq_access_template.NEXTVAL,'FACILITSUPMGR','Templ for FACILITSUPMGR');
		-- for FACILITATOR 
INSERT INTO access_templates (templt_id, role_id,templt_desc) VALUES (seq_access_template.NEXTVAL,'FACILITATOR','Templ for FACILITATOR');
		-- for UNDERWRITER 
INSERT INTO access_templates (templt_id, role_id,templt_desc) VALUES (seq_access_template.NEXTVAL,'UNDERWRITER','Templ for UNDERWRITER');
		-- for UWTNGSUPMGR 
INSERT INTO access_templates (templt_id, role_id,templt_desc) VALUES (seq_access_template.NEXTVAL,'UWTNGSUPMGR','Templ for UWTNGSUPMGR');
		-- for MEDICALADMIN 
INSERT INTO access_templates (templt_id, role_id,templt_desc) VALUES (seq_access_template.NEXTVAL,'MEDICALADMIN','Templ for MEDICALADMIN');
		-- for MEDSUPMGR 
INSERT INTO access_templates (templt_id, role_id,templt_desc) VALUES (seq_access_template.NEXTVAL,'MEDSUPMGR','Templ for MEDSUPMGR');
		-- for MEDCONSLT 
INSERT INTO access_templates (templt_id, role_id,templt_desc) VALUES (seq_access_template.NEXTVAL,'MEDCONSLT','Templ for MEDCONSLT');
		-- for MKTGSTAFF 
INSERT INTO access_templates (templt_id, role_id,templt_desc) VALUES (seq_access_template.NEXTVAL,'MKTGSTAFF','Templ for MKTGSTAFF');
		-- for AGENT 
INSERT INTO access_templates (templt_id, role_id,templt_desc) VALUES (seq_access_template.NEXTVAL,'AGENT','Templ for AGENT');
		-- for EXAMINER 
INSERT INTO access_templates (templt_id, role_id,templt_desc) VALUES (seq_access_template.NEXTVAL,'EXAMINER','Templ for EXAMINER');
		-- for LABSTAFF 
INSERT INTO access_templates (templt_id, role_id,templt_desc) VALUES (seq_access_template.NEXTVAL,'LABSTAFF','Templ for LABSTAFF');
		-- for OTHERUSER 
INSERT INTO access_templates (templt_id, role_id,templt_desc) VALUES (seq_access_template.NEXTVAL,'OTHERUSER','Templ for OTHERUSER');
		-- for LOVADMIN 
INSERT INTO access_templates (templt_id, role_id,templt_desc) VALUES (seq_access_template.NEXTVAL,'LOVADMIN','Templ for LOVADMIN');
		-- for SCTYADMIN 
INSERT INTO access_templates (templt_id, role_id,templt_desc) VALUES (seq_access_template.NEXTVAL,'SCTYADMIN','Templ for SCTYADMIN');
		-- for SYSOPER 
INSERT INTO access_templates (templt_id, role_id,templt_desc) VALUES (seq_access_template.NEXTVAL,'SYSOPER','Templ for SYSOPER');

-- STATUS
PROMPT INSERT STATUS ................
INSERT INTO status (stat_id, stat_desc,stat_type) VALUES(1, 'NB Review Action', 'AR');
INSERT INTO status (stat_id, stat_desc,stat_type) VALUES(2, 'For Transmittal USD','AR');
INSERT INTO status (stat_id, stat_desc,stat_type) VALUES(3, 'For Facilitator''s Action','AR');
INSERT INTO status (stat_id, stat_desc,stat_type) VALUES(4, 'For Assessment','AR');
INSERT INTO status (stat_id, stat_desc,stat_type) VALUES(5, 'Undergoing Assessment','AR');
INSERT INTO status (stat_id, stat_desc,stat_type) VALUES(6, 'For Approval','AR');
INSERT INTO status (stat_id, stat_desc,stat_type) VALUES(7, 'Approved','AR');
INSERT INTO status (stat_id, stat_desc,stat_type) VALUES(8, 'Declined','AR');
INSERT INTO status (stat_id, stat_desc,stat_type) VALUES(9, 'For Offer','AR');
INSERT INTO status (stat_id, stat_desc,stat_type) VALUES(10, 'Awaiting Requirements','AR');
INSERT INTO status (stat_id, stat_desc,stat_type) VALUES(11, 'Awaiting Medical','AR');
INSERT INTO status (stat_id, stat_desc,stat_type) VALUES(12, 'Not Proceeded With','AR');
INSERT INTO status (stat_id, stat_desc,stat_type) VALUES(13, 'Ordered','NM');
INSERT INTO status (stat_id, stat_desc,stat_type) VALUES(14, 'Received in site','NM');
INSERT INTO status (stat_id, stat_desc,stat_type) VALUES(15, 'Waived','NM');
INSERT INTO status (stat_id, stat_desc,stat_type) VALUES(16, 'Reviewed And Rejected','NM');
INSERT INTO status (stat_id, stat_desc,stat_type) VALUES(17, 'Reviewed And Accepted','NM');
INSERT INTO status (stat_id, stat_desc,stat_type) VALUES(18, 'Requested','M');
INSERT INTO status (stat_id, stat_desc,stat_type) VALUES(19, 'Confirmed','M');
INSERT INTO status (stat_id, stat_desc,stat_type) VALUES(20, 'Valid','M');
INSERT INTO status (stat_id, stat_desc,stat_type) VALUES(21, 'Expired','M');
INSERT INTO status (stat_id, stat_desc,stat_type) VALUES(22, 'Not Submitted','M');
INSERT INTO status (stat_id, stat_desc,stat_type) VALUES(23, 'Cancelled','M');
INSERT INTO status (stat_id, stat_desc,stat_type) VALUES(24, 'NTO','NM');
INSERT INTO status (stat_id, stat_desc,stat_type) VALUES(25, 'SAA','NM');
INSERT INTO status (stat_id, stat_desc,stat_type) VALUES(26, 'SCC','NM');
INSERT INTO status (stat_id, stat_desc,stat_type) VALUES(27, 'SIR','NM');
INSERT INTO status (stat_id, stat_desc,stat_type) VALUES(121, 'Cancelled','NM');
INSERT INTO status (stat_id, stat_desc,stat_type) VALUES(141, 'Cancelled','AR');
INSERT INTO status (stat_id, stat_desc,stat_type) VALUES(181, 'For Referral','AR');

-- PAGES ACCESS
PROMPT INSERT PAGE ACCESS ................
INSERT INTO pages_access
VALUES
(376,26,1,'1')
/
INSERT INTO pages_access
VALUES
(377,26,2,'0')
/
INSERT INTO pages_access
VALUES
(378,26,3,'1')
/
INSERT INTO pages_access
VALUES
(379,26,4,'0')
/
INSERT INTO pages_access
VALUES
(380,26,5,'1')
/
INSERT INTO pages_access
VALUES
(382,27,1,'1')
/
INSERT INTO pages_access
VALUES
(383,27,2,'0')
/
INSERT INTO pages_access
VALUES
(384,27,3,'1')
/
INSERT INTO pages_access
VALUES
(385,27,4,'0')
/
INSERT INTO pages_access
VALUES
(386,27,5,'0')
/
INSERT INTO pages_access
VALUES
(140,19,1,'1')
/
INSERT INTO pages_access
VALUES
(141,19,2,'1')
/
INSERT INTO pages_access
VALUES
(142,19,3,'1')
/
INSERT INTO pages_access
VALUES
(143,19,4,'1')
/
INSERT INTO pages_access
VALUES
(144,19,5,'0')
/
INSERT INTO pages_access
VALUES
(146,20,1,'1')
/
INSERT INTO pages_access
VALUES
(147,20,2,'1')
/
INSERT INTO pages_access
VALUES
(148,20,3,'1')
/
INSERT INTO pages_access
VALUES
(149,20,4,'1')
/
INSERT INTO pages_access
VALUES
(150,20,5,'0')
/
INSERT INTO pages_access
VALUES
(152,21,1,'1')
/
INSERT INTO pages_access
VALUES
(153,21,2,'1')
/
INSERT INTO pages_access
VALUES
(154,21,3,'1')
/
INSERT INTO pages_access
VALUES
(155,21,4,'1')
/
INSERT INTO pages_access
VALUES
(156,21,5,'0')
/
INSERT INTO pages_access
VALUES
(158,22,1,'1')
/
INSERT INTO pages_access
VALUES
(159,22,2,'1')
/
INSERT INTO pages_access
VALUES
(160,22,3,'1')
/
INSERT INTO pages_access
VALUES
(161,22,4,'1')
/
INSERT INTO pages_access
VALUES
(162,22,5,'0')
/
INSERT INTO pages_access
VALUES
(164,23,1,'1')
/
INSERT INTO pages_access
VALUES
(165,23,2,'1')
/
INSERT INTO pages_access
VALUES
(166,23,3,'1')
/
INSERT INTO pages_access
VALUES
(167,23,4,'1')
/
INSERT INTO pages_access
VALUES
(168,23,5,'0')
/
INSERT INTO pages_access
VALUES
(170,24,1,'1')
/
INSERT INTO pages_access
VALUES
(171,24,2,'0')
/
INSERT INTO pages_access
VALUES
(172,24,3,'1')
/
INSERT INTO pages_access
VALUES
(173,24,4,'0')
/
INSERT INTO pages_access
VALUES
(174,24,5,'0')
/
INSERT INTO pages_access
VALUES
(176,25,1,'1')
/
INSERT INTO pages_access
VALUES
(177,25,2,'0')
/
INSERT INTO pages_access
VALUES
(178,25,3,'1')
/
INSERT INTO pages_access
VALUES
(179,25,4,'0')
/
INSERT INTO pages_access
VALUES
(180,25,5,'1')
/

INSERT INTO pages_access
VALUES
(1,1,1,'1')
/
INSERT INTO pages_access
VALUES
(2,1,2,'0')
/
INSERT INTO pages_access
VALUES
(3,1,3,'0')
/
INSERT INTO pages_access
VALUES
(4,1,4,'0')
/
INSERT INTO pages_access
VALUES
(5,1,5,'0')
/
INSERT INTO pages_access
VALUES
(7,2,1,'1')
/
INSERT INTO pages_access
VALUES
(8,2,2,'0')
/
INSERT INTO pages_access
VALUES
(9,2,3,'0')
/
INSERT INTO pages_access
VALUES
(10,2,4,'1')
/
INSERT INTO pages_access
VALUES
(11,2,5,'1')
/
INSERT INTO pages_access
VALUES
(13,3,1,'1')
/
INSERT INTO pages_access
VALUES
(14,3,2,'1')
/
INSERT INTO pages_access
VALUES
(15,3,3,'1')
/
INSERT INTO pages_access
VALUES
(16,3,4,'1')
/
INSERT INTO pages_access
VALUES
(17,3,5,'1')
/
INSERT INTO pages_access
VALUES
(19,4,1,'1')
/
INSERT INTO pages_access
VALUES
(20,4,2,'1')
/
INSERT INTO pages_access
VALUES
(21,4,3,'1')
/
INSERT INTO pages_access
VALUES
(22,4,4,'0')
/
INSERT INTO pages_access
VALUES
(23,4,5,'0')
/
INSERT INTO pages_access
VALUES
(25,5,1,'1')
/
INSERT INTO pages_access
VALUES
(26,5,2,'0')
/
INSERT INTO pages_access
VALUES
(27,5,3,'0')
/
INSERT INTO pages_access
VALUES
(28,5,4,'0')
/
INSERT INTO pages_access
VALUES
(29,5,5,'0')
/
INSERT INTO pages_access
VALUES
(31,6,1,'1')
/
INSERT INTO pages_access
VALUES
(32,6,2,'1')
/
INSERT INTO pages_access
VALUES
(33,6,3,'1')
/
INSERT INTO pages_access
VALUES
(34,6,4,'1')
/
INSERT INTO pages_access
VALUES
(35,6,5,'1')
/
INSERT INTO pages_access
VALUES
(37,7,1,'1')
/
INSERT INTO pages_access
VALUES
(38,7,2,'1')
/
INSERT INTO pages_access
VALUES
(39,7,3,'1')
/
INSERT INTO pages_access
VALUES
(40,7,4,'0')
/
INSERT INTO pages_access
VALUES
(41,7,5,'1')
/
INSERT INTO pages_access
VALUES
(43,8,1,'1')
/
INSERT INTO pages_access
VALUES
(44,8,2,'1')
/
INSERT INTO pages_access
VALUES
(45,8,3,'0')
/
INSERT INTO pages_access
VALUES
(46,8,4,'0')
/
INSERT INTO pages_access
VALUES
(47,8,5,'0')
/
INSERT INTO pages_access
VALUES
(49,9,1,'1')
/
INSERT INTO pages_access
VALUES
(50,9,2,'0')
/
INSERT INTO pages_access
VALUES
(51,9,3,'0')
/
INSERT INTO pages_access
VALUES
(52,9,4,'0')
/
INSERT INTO pages_access
VALUES
(53,9,5,'1')
/
INSERT INTO pages_access
VALUES
(55,10,1,'1')
/
INSERT INTO pages_access
VALUES
(56,10,2,'1')
/
INSERT INTO pages_access
VALUES
(57,10,3,'1')
/
INSERT INTO pages_access
VALUES
(58,10,4,'0')
/
INSERT INTO pages_access
VALUES
(59,10,5,'1')
/
INSERT INTO pages_access
VALUES
(61,11,1,'1')
/
INSERT INTO pages_access
VALUES
(62,11,2,'0')
/
INSERT INTO pages_access
VALUES
(63,11,3,'0')
/
INSERT INTO pages_access
VALUES
(64,11,4,'0')
/
INSERT INTO pages_access
VALUES
(65,11,5,'0')
/
INSERT INTO pages_access
VALUES
(67,12,1,'1')
/
INSERT INTO pages_access
VALUES
(68,12,2,'1')
/
INSERT INTO pages_access
VALUES
(69,12,3,'1')
/
INSERT INTO pages_access
VALUES
(70,12,4,'0')
/
INSERT INTO pages_access
VALUES
(71,12,5,'1')
/
INSERT INTO pages_access
VALUES
(73,13,1,'1')
/
INSERT INTO pages_access
VALUES
(74,13,2,'1')
/
INSERT INTO pages_access
VALUES
(75,13,3,'1')
/
INSERT INTO pages_access
VALUES
(76,13,4,'0')
/
INSERT INTO pages_access
VALUES
(77,13,5,'1')
/
INSERT INTO pages_access
VALUES
(79,14,1,'1')
/
INSERT INTO pages_access
VALUES
(80,14,2,'1')
/
INSERT INTO pages_access
VALUES
(81,14,3,'1')
/
INSERT INTO pages_access
VALUES
(82,14,4,'0')
/
INSERT INTO pages_access
VALUES
(83,14,5,'1')
/
INSERT INTO pages_access
VALUES
(85,15,1,'1')
/
INSERT INTO pages_access
VALUES
(86,15,2,'0')
/
INSERT INTO pages_access
VALUES
(87,15,3,'0')
/
INSERT INTO pages_access
VALUES
(88,15,4,'0')
/
INSERT INTO pages_access
VALUES
(89,15,5,'0')
/
INSERT INTO pages_access
VALUES
(91,16,1,'1')
/
INSERT INTO pages_access
VALUES
(92,16,2,'0')
/
INSERT INTO pages_access
VALUES
(93,16,3,'0')
/
INSERT INTO pages_access
VALUES
(94,16,4,'0')
/
INSERT INTO pages_access
VALUES
(95,16,5,'0')
/
INSERT INTO pages_access
VALUES
(97,17,1,'1')
/
INSERT INTO pages_access
VALUES
(98,17,2,'0')
/
INSERT INTO pages_access
VALUES
(99,17,3,'0')
/
INSERT INTO pages_access
VALUES
(100,17,4,'0')
/
INSERT INTO pages_access
VALUES
(101,17,5,'0')
/
INSERT INTO pages_access
VALUES
(103,18,1,'1')
/
INSERT INTO pages_access
VALUES
(104,18,2,'0')
/
INSERT INTO pages_access
VALUES
(105,18,3,'1')
/
INSERT INTO pages_access
VALUES
(106,18,4,'0')
/
INSERT INTO pages_access
VALUES
(107,18,5,'0')
/

-- IUMADMIN USER PAGE ACCESS
PROMPT INSERT IUMADMIN PAGE ACCESS ................
INSERT INTO user_page_access (user_id,page_id,acc_id,upa_access_ind,created_by)
VALUES
('IUMADMIN',20,4,'0','IUMADMIN')
/
INSERT INTO user_page_access (user_id,page_id,acc_id,upa_access_ind,created_by)
VALUES
('IUMADMIN',25,1,'1','IUMADMIN')
/
INSERT INTO user_page_access (user_id,page_id,acc_id,upa_access_ind,created_by)
VALUES
('IUMADMIN',24,3,'1','IUMADMIN')
/
INSERT INTO user_page_access (user_id,page_id,acc_id,upa_access_ind,created_by)
VALUES
('IUMADMIN',21,3,'0','IUMADMIN')
/
INSERT INTO user_page_access (user_id,page_id,acc_id,upa_access_ind,created_by)
VALUES
('IUMADMIN',22,1,'1','IUMADMIN')
/
INSERT INTO user_page_access (user_id,page_id,acc_id,upa_access_ind,created_by)
VALUES
('IUMADMIN',21,4,'0','IUMADMIN')
/
INSERT INTO user_page_access (user_id,page_id,acc_id,upa_access_ind,created_by)
VALUES
('IUMADMIN',22,2,'1','IUMADMIN')
/
INSERT INTO user_page_access (user_id,page_id,acc_id,upa_access_ind,created_by)
VALUES
('IUMADMIN',22,4,'0','IUMADMIN')
/
INSERT INTO user_page_access (user_id,page_id,acc_id,upa_access_ind,created_by)
VALUES
('IUMADMIN',23,2,'1','IUMADMIN')
/
INSERT INTO user_page_access (user_id,page_id,acc_id,upa_access_ind,created_by)
VALUES
('IUMADMIN',23,4,'0','IUMADMIN')
/
INSERT INTO user_page_access (user_id,page_id,acc_id,upa_access_ind,created_by)
VALUES
('IUMADMIN',19,1,'1','IUMADMIN')
/
INSERT INTO user_page_access (user_id,page_id,acc_id,upa_access_ind,created_by)
VALUES
('IUMADMIN',19,2,'1','IUMADMIN')
/
INSERT INTO user_page_access (user_id,page_id,acc_id,upa_access_ind,created_by)
VALUES
('IUMADMIN',19,3,'1','IUMADMIN')
/
INSERT INTO user_page_access (user_id,page_id,acc_id,upa_access_ind,created_by)
VALUES
('IUMADMIN',26,1,'1','IUMADMIN')
/
INSERT INTO user_page_access (user_id,page_id,acc_id,upa_access_ind,created_by)
VALUES
('IUMADMIN',22,3,'1','IUMADMIN')
/
INSERT INTO user_page_access (user_id,page_id,acc_id,upa_access_ind,created_by)
VALUES
('IUMADMIN',23,3,'1','IUMADMIN')
/
INSERT INTO user_page_access (user_id,page_id,acc_id,upa_access_ind,created_by)
VALUES
('IUMADMIN',20,1,'1','IUMADMIN')
/
INSERT INTO user_page_access (user_id,page_id,acc_id,upa_access_ind,created_by)
VALUES
('IUMADMIN',20,2,'1','IUMADMIN')
/
INSERT INTO user_page_access (user_id,page_id,acc_id,upa_access_ind,created_by)
VALUES
('IUMADMIN',26,3,'1','IUMADMIN')
/
INSERT INTO user_page_access (user_id,page_id,acc_id,upa_access_ind,created_by)
VALUES
('IUMADMIN',27,3,'1','IUMADMIN')
/
INSERT INTO user_page_access (user_id,page_id,acc_id,upa_access_ind,created_by)
VALUES
('IUMADMIN',4,2,'1','IUMADMIN')
/
INSERT INTO user_page_access (user_id,page_id,acc_id,upa_access_ind,created_by)
VALUES
('IUMADMIN',21,1,'1','IUMADMIN')
/
INSERT INTO user_page_access (user_id,page_id,acc_id,upa_access_ind,created_by)
VALUES
('IUMADMIN',21,2,'1','IUMADMIN')
/
INSERT INTO user_page_access (user_id,page_id,acc_id,upa_access_ind,created_by)
VALUES
('IUMADMIN',25,3,'0','IUMADMIN')
/
INSERT INTO user_page_access (user_id,page_id,acc_id,upa_access_ind,created_by)
VALUES
('IUMADMIN',25,5,'0','IUMADMIN')
/
INSERT INTO user_page_access (user_id,page_id,acc_id,upa_access_ind,created_by)
VALUES
('IUMADMIN',1,1,'1','IUMADMIN')
/
INSERT INTO user_page_access (user_id,page_id,acc_id,upa_access_ind,created_by)
VALUES
('IUMADMIN',2,1,'1','IUMADMIN')
/
INSERT INTO user_page_access (user_id,page_id,acc_id,upa_access_ind,created_by)
VALUES
('IUMADMIN',2,4,'0','IUMADMIN')
/
INSERT INTO user_page_access (user_id,page_id,acc_id,upa_access_ind,created_by)
VALUES
('IUMADMIN',2,5,'1','IUMADMIN')
/
INSERT INTO user_page_access (user_id,page_id,acc_id,upa_access_ind,created_by)
VALUES
('IUMADMIN',3,3,'1','IUMADMIN')
/
INSERT INTO user_page_access (user_id,page_id,acc_id,upa_access_ind,created_by)
VALUES
('IUMADMIN',3,4,'1','IUMADMIN')
/
INSERT INTO user_page_access (user_id,page_id,acc_id,upa_access_ind,created_by)
VALUES
('IUMADMIN',3,5,'1','IUMADMIN')
/
INSERT INTO user_page_access (user_id,page_id,acc_id,upa_access_ind,created_by)
VALUES
('IUMADMIN',4,1,'1','IUMADMIN')
/
INSERT INTO user_page_access (user_id,page_id,acc_id,upa_access_ind,created_by)
VALUES
('IUMADMIN',4,3,'1','IUMADMIN')
/
INSERT INTO user_page_access (user_id,page_id,acc_id,upa_access_ind,created_by)
VALUES
('IUMADMIN',5,1,'1','IUMADMIN')
/
INSERT INTO user_page_access (user_id,page_id,acc_id,upa_access_ind,created_by)
VALUES
('IUMADMIN',6,1,'1','IUMADMIN')
/
INSERT INTO user_page_access (user_id,page_id,acc_id,upa_access_ind,created_by)
VALUES
('IUMADMIN',6,3,'1','IUMADMIN')
/
INSERT INTO user_page_access (user_id,page_id,acc_id,upa_access_ind,created_by)
VALUES
('IUMADMIN',6,4,'1','IUMADMIN')
/
INSERT INTO user_page_access (user_id,page_id,acc_id,upa_access_ind,created_by)
VALUES
('IUMADMIN',6,5,'1','IUMADMIN')
/
INSERT INTO user_page_access (user_id,page_id,acc_id,upa_access_ind,created_by)
VALUES
('IUMADMIN',7,1,'1','IUMADMIN')
/
INSERT INTO user_page_access (user_id,page_id,acc_id,upa_access_ind,created_by)
VALUES
('IUMADMIN',7,2,'1','IUMADMIN')
/
INSERT INTO user_page_access (user_id,page_id,acc_id,upa_access_ind,created_by)
VALUES
('IUMADMIN',7,3,'1','IUMADMIN')
/
INSERT INTO user_page_access (user_id,page_id,acc_id,upa_access_ind,created_by)
VALUES
('IUMADMIN',7,5,'1','IUMADMIN')
/
INSERT INTO user_page_access (user_id,page_id,acc_id,upa_access_ind,created_by)
VALUES
('IUMADMIN',8,1,'1','IUMADMIN')
/
INSERT INTO user_page_access (user_id,page_id,acc_id,upa_access_ind,created_by)
VALUES
('IUMADMIN',8,2,'1','IUMADMIN')
/
INSERT INTO user_page_access (user_id,page_id,acc_id,upa_access_ind,created_by)
VALUES
('IUMADMIN',9,1,'1','IUMADMIN')
/
INSERT INTO user_page_access (user_id,page_id,acc_id,upa_access_ind,created_by)
VALUES
('IUMADMIN',9,5,'1','IUMADMIN')
/
INSERT INTO user_page_access (user_id,page_id,acc_id,upa_access_ind,created_by)
VALUES
('IUMADMIN',10,1,'1','IUMADMIN')
/
INSERT INTO user_page_access (user_id,page_id,acc_id,upa_access_ind,created_by)
VALUES
('IUMADMIN',10,2,'1','IUMADMIN')
/
INSERT INTO user_page_access (user_id,page_id,acc_id,upa_access_ind,created_by)
VALUES
('IUMADMIN',10,3,'1','IUMADMIN')
/
INSERT INTO user_page_access (user_id,page_id,acc_id,upa_access_ind,created_by)
VALUES
('IUMADMIN',10,5,'1','IUMADMIN')
/
INSERT INTO user_page_access (user_id,page_id,acc_id,upa_access_ind,created_by)
VALUES
('IUMADMIN',11,1,'1','IUMADMIN')
/
INSERT INTO user_page_access (user_id,page_id,acc_id,upa_access_ind,created_by)
VALUES
('IUMADMIN',12,1,'1','IUMADMIN')
/
INSERT INTO user_page_access (user_id,page_id,acc_id,upa_access_ind,created_by)
VALUES
('IUMADMIN',12,2,'1','IUMADMIN')
/
INSERT INTO user_page_access (user_id,page_id,acc_id,upa_access_ind,created_by)
VALUES
('IUMADMIN',12,3,'1','IUMADMIN')
/
INSERT INTO user_page_access (user_id,page_id,acc_id,upa_access_ind,created_by)
VALUES
('IUMADMIN',12,5,'1','IUMADMIN')
/
INSERT INTO user_page_access (user_id,page_id,acc_id,upa_access_ind,created_by)
VALUES
('IUMADMIN',13,1,'1','IUMADMIN')
/
INSERT INTO user_page_access (user_id,page_id,acc_id,upa_access_ind,created_by)
VALUES
('IUMADMIN',13,2,'1','IUMADMIN')
/
INSERT INTO user_page_access (user_id,page_id,acc_id,upa_access_ind,created_by)
VALUES
('IUMADMIN',13,3,'1','IUMADMIN')
/
INSERT INTO user_page_access (user_id,page_id,acc_id,upa_access_ind,created_by)
VALUES
('IUMADMIN',13,5,'1','IUMADMIN')
/
INSERT INTO user_page_access (user_id,page_id,acc_id,upa_access_ind,created_by)
VALUES
('IUMADMIN',14,1,'1','IUMADMIN')
/
INSERT INTO user_page_access (user_id,page_id,acc_id,upa_access_ind,created_by)
VALUES
('IUMADMIN',14,3,'1','IUMADMIN')
/
INSERT INTO user_page_access (user_id,page_id,acc_id,upa_access_ind,created_by)
VALUES
('IUMADMIN',14,5,'1','IUMADMIN')
/
INSERT INTO user_page_access (user_id,page_id,acc_id,upa_access_ind,created_by)
VALUES
('IUMADMIN',15,1,'1','IUMADMIN')
/
INSERT INTO user_page_access (user_id,page_id,acc_id,upa_access_ind,created_by)
VALUES
('IUMADMIN',16,1,'1','IUMADMIN')
/
INSERT INTO user_page_access (user_id,page_id,acc_id,upa_access_ind,created_by)
VALUES
('IUMADMIN',17,1,'1','IUMADMIN')
/
INSERT INTO user_page_access (user_id,page_id,acc_id,upa_access_ind,created_by)
VALUES
('IUMADMIN',3,1,'1',NULL)
/
INSERT INTO user_page_access (user_id,page_id,acc_id,upa_access_ind,created_by)
VALUES
('IUMADMIN',6,2,'1','IUMADMIN')
/
INSERT INTO user_page_access (user_id,page_id,acc_id,upa_access_ind,created_by)
VALUES
('IUMADMIN',3,2,'1','IUMADMIN')
/
INSERT INTO user_page_access (user_id,page_id,acc_id,upa_access_ind,created_by)
VALUES
('IUMADMIN',18,1,'1','IUMADMIN')
/
INSERT INTO user_page_access (user_id,page_id,acc_id,upa_access_ind,created_by)
VALUES
('IUMADMIN',18,3,'1','IUMADMIN')
/
INSERT INTO user_page_access (user_id,page_id,acc_id,upa_access_ind,created_by)
VALUES
('IUMADMIN',14,2,'0','IUMADMIN')
/
INSERT INTO user_page_access (user_id,page_id,acc_id,upa_access_ind,created_by)
VALUES
('IUMADMIN',19,4,'0','IUMADMIN')
/
INSERT INTO user_page_access (user_id,page_id,acc_id,upa_access_ind,created_by)
VALUES
('IUMADMIN',20,3,'0','IUMADMIN')
/
INSERT INTO user_page_access (user_id,page_id,acc_id,upa_access_ind,created_by)
VALUES
('IUMADMIN',23,1,'1','IUMADMIN')
/
INSERT INTO user_page_access (user_id,page_id,acc_id,upa_access_ind,created_by)
VALUES
('IUMADMIN',24,1,'1','IUMADMIN')
/
INSERT INTO user_page_access (user_id,page_id,acc_id,upa_access_ind,created_by)
VALUES
('IUMADMIN',27,1,'1','IUMADMIN')
/
INSERT INTO user_page_access (user_id,page_id,acc_id,upa_access_ind,created_by)
VALUES
('IUMADMIN',26,5,'0','IUMADMIN')
/

-- CLIENT TYPES
PROMPT INSERT CLIENT TYPES ................
INSERT INTO client_types (clt_code, clt_desc) VALUES ('O', 'Owner');
INSERT INTO client_types (clt_code, clt_desc) VALUES ('I', 'Insured');
INSERT INTO client_types (clt_code, clt_desc) VALUES ('P', 'Planholder');
INSERT INTO client_types (clt_code, clt_desc) VALUES ('M', 'Member');

-- LINES OF BUSINESS
PROMPT INSERT LINES OF BUSINESS ................
INSERT INTO lines_of_business (lob_code, lob_desc) VALUES ('IL', 'Individual Life');
INSERT INTO lines_of_business (lob_code, lob_desc) VALUES ('PN', 'Pre-need');
INSERT INTO lines_of_business (lob_code, lob_desc) VALUES ('MF', 'Mutual Funds');
INSERT INTO lines_of_business (lob_code, lob_desc) VALUES ('GL', 'Group Life');

-- PROCESS CONFIGURATION
PROMPT INSERT PROCESS CONFIGURATION ..................
INSERT INTO process_configurations
VALUES
(57,'AR','App Forwarded to UW',3,4,'GL',3,'1','IUMADMIN',NULL,NULL,NULL)
/
INSERT INTO process_configurations
VALUES
(59,'AR','Approved',5,7,'GL',13,'1','IUMADMIN',NULL,NULL,NULL)
/
INSERT INTO process_configurations
VALUES
(58,'AR','Approved',5,7,'PN',13,'1','IUMADMIN',NULL,NULL,NULL)
/
INSERT INTO process_configurations
VALUES
(60,'AR','App forwarded to USD',1,2,'PN',2,'1','IUMADMIN',NULL,NULL,NULL)
/
INSERT INTO process_configurations
VALUES
(61,'AR','App forwarded to USD',1,2,'GL',2,'1','IUMADMIN',NULL,NULL,NULL)
/
INSERT INTO process_configurations
VALUES
(62,'AR','App forwarded to USD',6,2,'IL',2,'1','IUMADMIN',NULL,NULL,NULL)
/
INSERT INTO process_configurations
VALUES
(63,'AR','App forwarded to USD',6,2,'PN',2,'1','IUMADMIN',NULL,NULL,NULL)
/
INSERT INTO process_configurations
VALUES
(64,'AR','App forwarded to USD',6,2,'GL',2,'1','IUMADMIN',NULL,NULL,NULL)
/
INSERT INTO process_configurations
VALUES
(65,'AR','Declined',5,8,'PN',13,'1','IUMADMIN',NULL,NULL,NULL)
/
INSERT INTO process_configurations
VALUES
(66,'AR','Declined',5,8,'GL',13,'1','IUMADMIN',NULL,NULL,NULL)
/
INSERT INTO process_configurations
VALUES
(35,'M','Medical Exam Request',NULL,18,'IL',9,'1','IUMADMIN',NULL,NULL,NULL)
/
INSERT INTO process_configurations
VALUES
(36,'M','Confirm Medical Exam',18,19,'IL',10,'1','IUMADMIN',NULL,NULL,NULL)
/
INSERT INTO process_configurations
VALUES
(37,'M','Receive Medical Results',NULL,20,'IL',12,'1','IUMADMIN',NULL,NULL,NULL)
/
INSERT INTO process_configurations
VALUES
(38,'M','Receive Medical Results',19,20,'IL',12,'1','IUMADMIN',NULL,NULL,NULL)
/
INSERT INTO process_configurations
VALUES
(39,'AR','Not Proceeded With',NULL,12,'IL',13,'1','IUMADMIN',NULL,NULL,NULL)
/
INSERT INTO process_configurations
VALUES
(40,'NM','Ordered Requirements',24,13,'IL',5,'1','IUMADMIN',NULL,NULL,NULL)
/
INSERT INTO process_configurations
VALUES
(41,'NM','Ordered Requirements',25,13,'IL',5,'1','IUMADMIN',NULL,NULL,NULL)
/
INSERT INTO process_configurations
VALUES
(42,'NM','Ordered Requirements',26,13,'IL',5,'1','IUMADMIN',NULL,NULL,NULL)
/
INSERT INTO process_configurations
VALUES
(43,'NM','Ordered Requirements',27,13,'IL',5,'1','IUMADMIN',NULL,NULL,NULL)
/
INSERT INTO process_configurations
VALUES
(44,'NM','Accept Requirement',13,17,'IL',NULL,'1','IUMADMIN',NULL,NULL,NULL)
/
INSERT INTO process_configurations
VALUES
(45,'NM','Accept Requirement',14,17,'IL',NULL,'1','IUMADMIN',NULL,NULL,NULL)
/
INSERT INTO process_configurations
VALUES
(46,'NM','Cancelled Requirements',13,121,'IL',6,'1','IUMADMIN',NULL,NULL,NULL)
/
INSERT INTO process_configurations
VALUES
(47,'NM','Cancelled Requirements',24,121,'IL',NULL,'1','IUMADMIN',NULL,'IUMADMIN','19-MAR-04 11.51.25.000000 PM')
/
INSERT INTO process_configurations
VALUES
(48,'NM','Cancelled Requirements',25,121,'IL',NULL,'1','IUMADMIN',NULL,'IUMADMIN','19-MAR-04 11.51.04.000000 PM')
/
INSERT INTO process_configurations
VALUES
(49,'NM','Cancelled Requirements',26,121,'IL',NULL,'1','IUMADMIN',NULL,'IUMADMIN','19-MAR-04 11.51.43.000000 PM')
/
INSERT INTO process_configurations
VALUES
(50,'NM','Cancelled Requirements',27,121,'IL',NULL,'1','IUMADMIN',NULL,'IUMADMIN','19-MAR-04 11.51.59.000000 PM')
/
INSERT INTO process_configurations
VALUES
(51,'NM','Waived Requirements',13,15,'IL',8,'1','IUMADMIN',NULL,'IUMADMIN','19-MAR-04 11.20.39.000000 PM')
/
INSERT INTO process_configurations
VALUES
(52,'NM','Waived Requirements',24,15,'IL',NULL,'1','IUMADMIN',NULL,'IUMADMIN','19-MAR-04 11.49.29.000000 PM')
/
INSERT INTO process_configurations
VALUES
(53,'NM','Waived Requirements',25,15,'IL',NULL,'1','IUMADMIN',NULL,'IUMADMIN','19-MAR-04 11.49.54.000000 PM')
/
INSERT INTO process_configurations
VALUES
(54,'NM','Waived Requirements',26,15,'IL',NULL,'1','IUMADMIN',NULL,'IUMADMIN','19-MAR-04 11.50.07.000000 PM')
/
INSERT INTO process_configurations
VALUES
(55,'NM','Waived Requirements',27,15,'IL',NULL,'1','IUMADMIN',NULL,'IUMADMIN','19-MAR-04 11.50.35.000000 PM')
/
INSERT INTO process_configurations
VALUES
(56,'AR','App Forwarded to UW',3,4,'PN',3,'1','IUMADMIN',NULL,NULL,NULL)
/
INSERT INTO process_configurations
VALUES
(67,'AR','Cancel Assessment Request',1,141,'IL',NULL,'1','IUMADMIN',NULL,NULL,NULL)
/
INSERT INTO process_configurations
VALUES
(68,'M','No Submission of Med Exam',18,22,'IL',NULL,'1','IUMADMIN',NULL,NULL,NULL)
/
INSERT INTO process_configurations
VALUES
(76,'M','No Submission of Med Exam',19,22,'IL',NULL,NULL,NULL,NULL,NULL,NULL)
/
INSERT INTO process_configurations
VALUES
(77,'M','No Submission of Med Exam',18,22,'GL',NULL,NULL,NULL,NULL,NULL,NULL)
/
INSERT INTO process_configurations
VALUES
(78,'M','No Submission of Med Exam',19,22,'GL',NULL,NULL,NULL,NULL,NULL,NULL)
/
INSERT INTO process_configurations
VALUES
(79,'M','No Submission of Med Exam',18,22,'PN',NULL,NULL,NULL,NULL,NULL,NULL)
/
INSERT INTO process_configurations
VALUES
(80,'M','No Submission of Med Exam',19,22,'PN',NULL,NULL,NULL,NULL,NULL,NULL)
/
INSERT INTO process_configurations
VALUES
(84,'NM','Reqt Received in Site',13,14,'IL',11,'1','IUMADMIN',NULL,NULL,NULL)
/
INSERT INTO process_configurations
VALUES
(85,'NM','Reqt Received in Site',13,14,'PN',11,'1','IUMADMIN',NULL,NULL,NULL)
/
INSERT INTO process_configurations
VALUES
(86,'NM','Reqt Received in Site',13,14,'GL',11,'1','IUMADMIN',NULL,NULL,NULL)
/
INSERT INTO process_configurations
VALUES
(87,'NM','Ordered Requirement',24,13,'PN',5,'1','IUMADMIN',NULL,NULL,NULL)
/
INSERT INTO process_configurations
VALUES
(88,'NM','Ordered Requirement',24,13,'GL',5,'1','IUMADMIN',NULL,NULL,NULL)
/
INSERT INTO process_configurations
VALUES
(89,'NM','Rejected Requirement',13,16,'IL',NULL,'0','IUMADMIN',NULL,NULL,NULL)
/
INSERT INTO process_configurations
VALUES
(90,'NM','Rejected Requirement',14,16,'IL',NULL,'0','IUMADMIN',NULL,NULL,NULL)
/
INSERT INTO process_configurations
VALUES
(91,'NM','Rejected Requirement',17,16,'IL',NULL,'0','IUMADMIN',NULL,NULL,NULL)
/
INSERT INTO process_configurations
VALUES
(92,'NM','Rejected Requirement',13,16,'PN',NULL,'0','IUMADMIN',NULL,NULL,NULL)
/
INSERT INTO process_configurations
VALUES
(93,'NM','Rejected Requirement',14,16,'PN',NULL,'0','IUMADMIN',NULL,NULL,NULL)
/
INSERT INTO process_configurations
VALUES
(94,'NM','Rejected Requirement',17,16,'PN',NULL,'0','IUMADMIN',NULL,NULL,NULL)
/
INSERT INTO process_configurations
VALUES
(95,'NM','Rejected Requirement',13,16,'GL',NULL,'0','IUMADMIN',NULL,NULL,NULL)
/
INSERT INTO process_configurations
VALUES
(96,'NM','Rejected Requirement',14,16,'GL',NULL,'0','IUMADMIN',NULL,NULL,NULL)
/
INSERT INTO process_configurations
VALUES
(97,'NM','Rejected Requirement',17,16,'GL',NULL,'0','IUMADMIN',NULL,NULL,NULL)
/
INSERT INTO process_configurations
VALUES
(121,'AR','Speedy Approval',6,7,'PN',14,'1','IUMADMIN',NULL,NULL,NULL)
/
INSERT INTO process_configurations
VALUES
(122,'AR','Speedy Approval',6,7,'GL',14,'1','IUMADMIN',NULL,NULL,NULL)
/
INSERT INTO process_configurations
VALUES
(123,'AR','Not Proceeded With',10,12,'GL',13,'1','IUMADMIN',NULL,NULL,NULL)
/
INSERT INTO process_configurations
VALUES
(124,'AR','Not Proceeded With',10,12,'PN',13,'1','IUMADMIN',NULL,NULL,NULL)
/
INSERT INTO process_configurations
VALUES
(125,'AR','Not Proceeded With',11,12,'GL',13,'1','IUMADMIN',NULL,NULL,NULL)
/
INSERT INTO process_configurations
VALUES
(126,'AR','Not Proceeded With',11,12,'PN',13,'1','IUMADMIN',NULL,NULL,NULL)
/
INSERT INTO process_configurations
VALUES
(127,'AR','Reassign Assessment',4,4,'GL',4,'1','IUMADMIN',NULL,NULL,NULL)
/
INSERT INTO process_configurations
VALUES
(128,'AR','Reassign Assessment',4,4,'PN',4,'1','IUMADMIN',NULL,NULL,NULL)
/
INSERT INTO process_configurations
VALUES
(129,'AR','Speedy Case',1,6,'GL',NULL,'1','IUMADMIN',NULL,'IUMADMIN','23-MAR-04 09.07.02.000000 PM')
/
INSERT INTO process_configurations
VALUES
(130,'AR','Speedy Case',1,6,'PN',NULL,'1','IUMADMIN',NULL,'IUMADMIN','23-MAR-04 09.03.49.000000 PM')
/
INSERT INTO process_configurations
VALUES
(131,'AR','Await Medical',1,11,'GL',NULL,'1','IUMADMIN',NULL,'IUMADMIN','23-MAR-04 09.04.42.000000 PM')
/
INSERT INTO process_configurations
VALUES
(132,'AR','Await Medical',1,11,'PN',NULL,'1','IUMADMIN',NULL,'IUMADMIN','23-MAR-04 09.00.23.000000 PM')
/
INSERT INTO process_configurations
VALUES
(133,'AR','Await Requirements',1,10,'GL',NULL,'1','IUMADMIN',NULL,'IUMADMIN','23-MAR-04 09.06.14.000000 PM')
/
INSERT INTO process_configurations
VALUES
(134,'AR','Await Requirements',1,10,'PN',NULL,'1','IUMADMIN',NULL,'IUMADMIN','23-MAR-04 09.01.00.000000 PM')
/
INSERT INTO process_configurations
VALUES
(135,'AR','Cancel Assessment Request',1,141,'GL',NULL,'1','IUMADMIN',NULL,'IUMADMIN','23-MAR-04 09.06.32.000000 PM')
/
INSERT INTO process_configurations
VALUES
(136,'AR','Cancel Assessment Request',1,141,'PN',NULL,'1','IUMADMIN',NULL,'IUMADMIN','23-MAR-04 09.01.50.000000 PM')
/
INSERT INTO process_configurations
VALUES
(137,'AR','For Facilitator''s Action',2,3,'GL',NULL,'1','IUMADMIN',NULL,'IUMADMIN','23-MAR-04 08.01.27.000000 PM')
/
INSERT INTO process_configurations
VALUES
(138,'AR','For Facilitator''s Action',2,3,'PN',NULL,'1','IUMADMIN',NULL,'IUMADMIN','23-MAR-04 09.02.06.000000 PM')
/
INSERT INTO process_configurations
VALUES
(139,'AR','Undergoing Assessment',4,5,'GL',NULL,'1','IUMADMIN',NULL,NULL,NULL)
/
INSERT INTO process_configurations
VALUES
(140,'AR','Undergoing Assessment',4,5,'PN',NULL,'1','IUMADMIN',NULL,'IUMADMIN','23-MAR-04 09.04.03.000000 PM')
/
INSERT INTO process_configurations
VALUES
(141,'AR','Await Medical',5,11,'GL',NULL,'1','IUMADMIN',NULL,'IUMADMIN','23-MAR-04 09.05.06.000000 PM')
/
INSERT INTO process_configurations
VALUES
(142,'AR','Await Medical',5,11,'PN',NULL,'1','IUMADMIN',NULL,'IUMADMIN','23-MAR-04 09.00.38.000000 PM')
/
INSERT INTO process_configurations
VALUES
(143,'AR','Requirement Submitted',10,4,'GL',NULL,'1','IUMADMIN',NULL,'IUMADMIN','23-MAR-04 09.06.49.000000 PM')
/
INSERT INTO process_configurations
VALUES
(144,'AR','Requirement Submitted',10,4,'PN',NULL,'1','IUMADMIN',NULL,'IUMADMIN','23-MAR-04 09.03.16.000000 PM')
/
INSERT INTO process_configurations
VALUES
(145,'M','Medical Exam Request',NULL,18,'PN',9,'1','IUMADMIN',NULL,NULL,NULL)
/
INSERT INTO process_configurations
VALUES
(146,'M','Medical Exam Request',NULL,18,'GL',9,'1','IUMADMIN',NULL,NULL,NULL)
/
INSERT INTO process_configurations
VALUES
(147,'M','Confirm Medical Exam',18,19,'PN',10,'1','IUMADMIN',NULL,NULL,NULL)
/
INSERT INTO process_configurations
VALUES
(148,'M','Confirm Medical Exam',18,19,'GL',10,'1','IUMADMIN',NULL,NULL,NULL)
/
INSERT INTO process_configurations
VALUES
(149,'M','Confirm Medical Exam',NULL,19,'IL',10,'1','IUMADMIN',NULL,NULL,NULL)
/
INSERT INTO process_configurations
VALUES
(150,'M','Confirm Medical Exam',NULL,19,'PN',10,'1','IUMADMIN',NULL,NULL,NULL)
/
INSERT INTO process_configurations
VALUES
(151,'M','Confirm Medical Exam',NULL,19,'GL',10,'1','IUMADMIN',NULL,NULL,NULL)
/
INSERT INTO process_configurations
VALUES
(152,'M','Receive Medical Results',NULL,20,'PN',12,'1','IUMADMIN',NULL,NULL,NULL)
/
INSERT INTO process_configurations
VALUES
(153,'M','Receive Medical Results',NULL,20,'GL',12,'1','IUMADMIN',NULL,NULL,NULL)
/
INSERT INTO process_configurations
VALUES
(154,'M','Receive Medical Results',19,20,'PN',12,'1','IUMADMIN',NULL,NULL,NULL)
/
INSERT INTO process_configurations
VALUES
(155,'M','Receive Medical Results',19,20,'GL',12,'1','IUMADMIN',NULL,NULL,NULL)
/
INSERT INTO process_configurations
VALUES
(159,'M','Cancel Med Exam Request',18,141,'IL',NULL,NULL,'IUMADMIN',NULL,NULL,NULL)
/
INSERT INTO process_configurations
VALUES
(160,'M','Cancel Med Exam Request',18,141,'PN',NULL,NULL,'IUMADMIN',NULL,NULL,NULL)
/
INSERT INTO process_configurations
VALUES
(161,'M','Cancel Med Exam Request',18,141,'GL',NULL,NULL,'IUMADMIN',NULL,NULL,NULL)
/
INSERT INTO process_configurations
VALUES
(182,'NM','Follow Up Requirement',13,13,'PN',7,'1','IUMADMIN',NULL,'IUMADMIN','25-MAR-04 01.03.03.000000 PM')
/
INSERT INTO process_configurations
VALUES
(181,'NM','Follow Up Requirement',13,13,'IL',7,'1','IUMADMIN',NULL,'IUMADMIN','25-MAR-04 01.02.39.000000 PM')
/
INSERT INTO process_configurations
VALUES
(183,'NM','Follow Up Requirement',13,13,'GL',7,'1','IUMADMIN',NULL,'IUMADMIN','25-MAR-04 01.07.05.000000 PM')
/
INSERT INTO process_configurations
VALUES
(201,'AR','App forwarded to USD',10,2,'IL',2,NULL,'IUMADMIN','18-MAR-04 03.27.49.000000 PM','IUMADMIN','24-MAR-04 12.57.55.000000 PM')
/
INSERT INTO process_configurations
VALUES
(202,'AR',NULL,NULL,5,'IL',NULL,NULL,'IUMADMIN','18-MAR-04 04.05.08.000000 PM',NULL,NULL)
/
INSERT INTO process_configurations
VALUES
(225,'AR','Escalate',5,4,'IL',NULL,NULL,'IUMADMIN','31-MAR-04 08.36.37.000000 PM',NULL,NULL)
/
INSERT INTO process_configurations
VALUES
(227,'AR','NB Review Reassignment',1,1,'IL',NULL,'1','IUMADMIN','01-APR-04 06.45.27.000000 PM','IUMADMIN','03-APR-04 07.48.02.000000 PM')
/
INSERT INTO process_configurations
VALUES
(228,'AR','Await Requirements',1,10,'IL',NULL,'1','IUMADMIN','02-APR-04 05.56.53.000000 PM',NULL,NULL)
/
INSERT INTO process_configurations
VALUES
(208,'AR','App forwarded to USD',10,2,'PN',2,'1','IUMADMIN','19-MAR-04 08.04.23.000000 PM',NULL,NULL)
/
INSERT INTO process_configurations
VALUES
(209,'AR','App forwarded to USD',11,2,'PN',2,'1','IUMADMIN','19-MAR-04 08.04.56.000000 PM','IUMADMIN','19-MAR-04 08.05.12.000000 PM')
/
INSERT INTO process_configurations
VALUES
(210,'AR','App forwarded to USD',10,2,'GL',2,'1','IUMADMIN','19-MAR-04 08.08.21.000000 PM','IUMADMIN','19-MAR-04 08.08.36.000000 PM')
/
INSERT INTO process_configurations
VALUES
(211,'AR','App forwarded to USD',11,2,'GL',2,'1','IUMADMIN','19-MAR-04 08.09.08.000000 PM','IUMADMIN','19-MAR-04 08.09.21.000000 PM')
/
INSERT INTO process_configurations
VALUES
(212,'AR','Await Requirements',11,10,'PN',NULL,'1','IUMADMIN','22-MAR-04 11.26.08.000000 AM',NULL,NULL)
/
INSERT INTO process_configurations
VALUES
(213,'AR','Await Medical',10,11,'PN',NULL,'1','IUMADMIN','22-MAR-04 11.26.39.000000 AM',NULL,NULL)
/
INSERT INTO process_configurations
VALUES
(214,'AR','Await Requirements',11,10,'GL',NULL,'1','IUMADMIN','22-MAR-04 11.27.13.000000 AM',NULL,NULL)
/
INSERT INTO process_configurations
VALUES
(215,'AR','Await Medical',10,11,'GL',NULL,'1','IUMADMIN','22-MAR-04 11.27.55.000000 AM',NULL,NULL)
/
INSERT INTO process_configurations
VALUES
(216,'M','Cancelled Medical Request',18,23,'GL',6,'1','IUMADMIN',NULL,NULL,NULL)
/
INSERT INTO process_configurations
VALUES
(217,'M','Cancelled Medical Request',18,23,'IL',6,'1','IUMADMIN',NULL,NULL,NULL)
/
INSERT INTO process_configurations
VALUES
(218,'M','Cancelled Medical Request',18,23,'PN',6,'1','IUMADMIN',NULL,NULL,NULL)
/
INSERT INTO process_configurations
VALUES
(219,'M','Expired Medical Request',20,21,'GL',NULL,NULL,'IUMADMIN',NULL,NULL,NULL)
/
INSERT INTO process_configurations
VALUES
(220,'M','Expired Medical Request',20,21,'IL',NULL,NULL,'IUMADMIN',NULL,NULL,NULL)
/
INSERT INTO process_configurations
VALUES
(221,'M','Expired Medical Request',20,21,'PN',NULL,NULL,'IUMADMIN',NULL,NULL,NULL)
/
INSERT INTO process_configurations
VALUES
(222,'AR','Await Medical',10,11,'IL',NULL,'1','IUMADMIN','24-MAR-04 12.59.17.000000 PM',NULL,NULL)
/
INSERT INTO process_configurations
VALUES
(223,'AR','Await Requirements',11,10,'IL',NULL,'1','IUMADMIN','24-MAR-04 12.59.37.000000 PM',NULL,NULL)
/
INSERT INTO process_configurations
VALUES
(224,'AR','App Forwarded to USD',11,2,'IL',NULL,'1','IUMADMIN','25-MAR-04 05.14.42.000000 PM','IUMADMIN','25-MAR-04 05.21.45.000000 PM')
/
INSERT INTO process_configurations
VALUES
(229,'AR','For Referral',5,181,'IL',16,'1','IUMADMIN','03-APR-04 06.18.26.000000 PM','IUMADMIN','03-APR-04 07.11.39.000000 PM')
/
INSERT INTO process_configurations
VALUES
(231,'AR','For Assessment',181,4,'IL',3,'1','IUMADMIN','05-APR-04 02.14.24.000000 PM',NULL,NULL)
/
INSERT INTO process_configurations
VALUES
(1,'AR','Speedy Case',1,6,'IL',NULL,'1','IUMADMIN',NULL,'IUMADMIN','02-APR-04 04.50.58.000000 PM')
/
INSERT INTO process_configurations
VALUES
(2,'AR','App forwarded to USD',1,2,'IL',2,'1','IUMADMIN',NULL,'IUMADMIN','02-APR-04 04.43.55.000000 PM')
/
INSERT INTO process_configurations
VALUES
(3,'AR','For Facilitator''s Action',2,3,'IL',NULL,'1','IUMADMIN',NULL,NULL,NULL)
/
INSERT INTO process_configurations
VALUES
(4,'AR','App Forwarded to UW',3,4,'IL',3,'1','IUMADMIN',NULL,NULL,NULL)
/
INSERT INTO process_configurations
VALUES
(5,'AR','Undergoing Assessment',4,5,'IL',NULL,'1','IUMADMIN',NULL,NULL,NULL)
/
INSERT INTO process_configurations
VALUES
(6,'AR','For offer',5,9,'IL',13,'1','IUMADMIN',NULL,NULL,NULL)
/
INSERT INTO process_configurations
VALUES
(8,'AR','Declined',5,8,'IL',13,'1','IUMADMIN',NULL,NULL,NULL)
/
INSERT INTO process_configurations
VALUES
(9,'AR','Approved',5,7,'IL',13,'1','IUMADMIN',NULL,NULL,NULL)
/
INSERT INTO process_configurations
VALUES
(10,'AR','Await requirements',5,10,'IL',NULL,'1','IUMADMIN',NULL,'IUMADMIN','25-MAR-04 01.01.46.000000 PM')
/
INSERT INTO process_configurations
VALUES
(11,'AR','Await Medical',5,11,'IL',NULL,'1','IUMADMIN',NULL,NULL,NULL)
/
INSERT INTO process_configurations
VALUES
(12,'AR','Not Proceeded With',10,12,'IL',13,'1','IUMADMIN',NULL,NULL,NULL)
/
INSERT INTO process_configurations
VALUES
(13,'AR','Requirement Submitted',10,4,'IL',NULL,'1','IUMADMIN',NULL,NULL,NULL)
/
INSERT INTO process_configurations
VALUES
(17,'AR','Speedy Approval',6,7,'IL',14,'1','IUMADMIN',NULL,'IUMADMIN','02-APR-04 03.52.31.000000 PM')
/
INSERT INTO process_configurations
VALUES
(19,'AR','Await requirements',5,10,'PN',5,'1','IUMADMIN',NULL,NULL,NULL)
/
INSERT INTO process_configurations
VALUES
(20,'AR','Await requirements',5,10,'GL',5,'1','IUMADMIN',NULL,NULL,NULL)
/
INSERT INTO process_configurations
VALUES
(22,'AR','Submit for review',NULL,1,'GL',1,'1','IUMADMIN',NULL,NULL,NULL)
/
INSERT INTO process_configurations
VALUES
(23,'AR','Submit for review',NULL,1,'PN',1,'1','IUMADMIN',NULL,NULL,NULL)
/
INSERT INTO process_configurations
VALUES
(25,'AR','For Assessment',NULL,4,'IL',3,'1','IUMADMIN',NULL,NULL,NULL)
/
INSERT INTO process_configurations
VALUES
(26,'AR','Reassign Assessment',4,4,'IL',4,'1','IUMADMIN',NULL,NULL,NULL)
/
INSERT INTO process_configurations
VALUES
(27,'AR','Approved as Applied',NULL,7,'IL',13,'1','IUMADMIN',NULL,NULL,NULL)
/
INSERT INTO process_configurations
VALUES
(28,'AR','Submit for review',NULL,1,'IL',1,'1','IUMADMIN',NULL,'IUMADMIN','02-APR-04 04.35.40.000000 PM')
/
INSERT INTO process_configurations
VALUES
(29,'AR','App forwarded to USD',NULL,2,'PN',2,'1','IUMADMIN',NULL,NULL,NULL)
/
INSERT INTO process_configurations
VALUES
(30,'AR','Not Proceeded With',11,12,'IL',13,'1','IUMADMIN',NULL,NULL,NULL)
/
INSERT INTO process_configurations
VALUES
(31,'AR','Declined',NULL,8,'IL',13,'1','IUMADMIN',NULL,NULL,NULL)
/
INSERT INTO process_configurations
VALUES
(32,'AR','Approved',NULL,7,'IL',13,'1','IUMADMIN',NULL,NULL,NULL)
/
INSERT INTO process_configurations
VALUES
(33,'AR','For Offer',NULL,9,'IL',13,'1','IUMADMIN',NULL,NULL,NULL)
/
INSERT INTO process_configurations
VALUES
(34,'AR','App forwarded to USD',NULL,2,'IL',2,'1','IUMADMIN',NULL,'IUMADMIN','23-MAR-04 09.09.42.000000 PM')
/
INSERT INTO process_configurations
VALUES
(98,'NM','Accept Requirement',13,17,'PN',NULL,'0','IUMADMIN',NULL,NULL,NULL)
/
INSERT INTO process_configurations
VALUES
(99,'NM','Accept Requirement',14,17,'PN',NULL,'0','IUMADMIN',NULL,NULL,NULL)
/
INSERT INTO process_configurations
VALUES
(100,'NM','Accept Requirement',13,17,'GL',NULL,'0','IUMADMIN',NULL,NULL,NULL)
/
INSERT INTO process_configurations
VALUES
(101,'NM','Accept Requirement',14,17,'GL',NULL,'0','IUMADMIN',NULL,NULL,NULL)
/
INSERT INTO process_configurations
VALUES
(102,'NM','Cancelled Requirements',13,121,'PN',6,'1','IUMADMIN',NULL,NULL,NULL)
/
INSERT INTO process_configurations
VALUES
(103,'NM','Cancelled Requirements',24,121,'PN',NULL,'1','IUMADMIN',NULL,'IUMADMIN','25-MAR-04 01.04.16.000000 PM')
/
INSERT INTO process_configurations
VALUES
(104,'NM','Cancelled Requirements',13,121,'GL',6,'1','IUMADMIN',NULL,NULL,NULL)
/
INSERT INTO process_configurations
VALUES
(105,'NM','Cancelled Requirements',24,121,'GL',NULL,'1','IUMADMIN',NULL,'IUMADMIN','25-MAR-04 01.07.21.000000 PM')
/
INSERT INTO process_configurations
VALUES
(106,'NM','Waived Requirements',13,15,'PN',8,'1','IUMADMIN',NULL,'IUMADMIN','19-MAR-04 11.19.32.000000 PM')
/
INSERT INTO process_configurations
VALUES
(107,'NM','Waived Requirements',24,15,'PN',NULL,'1','IUMADMIN',NULL,'IUMADMIN','25-MAR-04 01.09.16.000000 PM')
/
INSERT INTO process_configurations
VALUES
(108,'NM','Waived Requirements',13,15,'GL',8,'1','IUMADMIN',NULL,NULL,NULL)
/
INSERT INTO process_configurations
VALUES
(109,'NM','Waived Requirements',24,15,'GL',NULL,'1','IUMADMIN',NULL,'IUMADMIN','25-MAR-04 01.08.43.000000 PM')
/

-- SUNLIFE OFFICES
PROMPT INSERT SUNLIFE OFFICES
INSERT INTO sunlife_offices
VALUES
('SUNLIFEHO','SUN LIFE HEAD OFFICE','M','16/F THE ENTERPRISE CENTRE TOWER 2','6766 AYALA AVE.',NULL,'MAKATI',NULL,'PHILIPPINES',1200,NULL,NULL,NULL,NULL,NULL,NULL)
/
INSERT INTO sunlife_offices
VALUES
('MAINCUST','MAIN CUSTOMER CENTER','M','111 PASEO DE ROXAS BLDG.','PASEO DE ROXAS AVE.',NULL,'MAKATI',NULL,'PHILIPPINES',1200,NULL,NULL,NULL,NULL,NULL,NULL)
/
INSERT INTO sunlife_offices
VALUES
('ACECUST','ACE CUSTOMER CENTER','S','7/F ACE BUILDING','101-103 RADA ST.','LEGASPI VILLAGE','MAKATI',NULL,'PHILIPPINES',1200,NULL,NULL,NULL,NULL,NULL,NULL)
/
INSERT INTO sunlife_offices
VALUES
('ACACIABUT','ACACIA NBO - BUTUAN EXTENSION OFFICE','B','2ND FLOOR D and V PLAZA HOLDINGS J.C.','AQUINO AVENUE',NULL,'BUTUAN',NULL,'PHILIPPINES',8600,NULL,NULL,NULL,NULL,NULL,NULL)
/
INSERT INTO sunlife_offices
VALUES
('ACACIACDO','ACACIA NBO - CAGAYAN DE ORO SALES OFFICE','B','2/F PHILIPPINE FIRST INSURANCE BLDG.','PRESIDENT AGUINALDO','COR. BORJA STS.','CAGAYAN DE ORO',NULL,'PHILIPPINES',9000,NULL,NULL,NULL,NULL,NULL,NULL)
/
INSERT INTO sunlife_offices
VALUES
('ACACIACEBU','ACACIA NBO - CEBU','B','3/F GLOBE ISLA PLAZA','PANAY RD. COR. SAMAR LOOP','CEBU BUSINESS PARK','CEBU',NULL,'PHILIPPINES',NULL,NULL,NULL,NULL,NULL,NULL,NULL)
/
INSERT INTO sunlife_offices
VALUES
('ALABANGCUST','ALABANG CUSTOMER CENTER','S','G/F UNIT 102-103 ALPAP II BLDG.','INVESTMENT DRIVE COR. TRADE STS.','MADRIGAL BUSINESS PARK, ALABANG','MUNTINLUPA',NULL,'PHILIPPINES',NULL,NULL,NULL,NULL,NULL,NULL,NULL)
/
INSERT INTO sunlife_offices
VALUES
('ASPENPAS','ASPEN NEW BUSINESS OFFICE','B','30/F ORIENT SQUARE BLDG.','EMERALD AVE.','ORTIGAS CENTER','PASIG',NULL,'PHILIPPINES',1600,NULL,NULL,NULL,NULL,NULL,NULL)
/
INSERT INTO sunlife_offices
VALUES
('BANYANBAC','BANYAN NBO - BACOLOD','B','10TH COR. LACSON ST.',NULL,NULL,'BACOLOD',NULL,'PHILIPPINES',NULL,NULL,NULL,NULL,NULL,NULL,NULL)
/
INSERT INTO sunlife_offices
VALUES
('BAYWOODLAG','BAYWOOD NBO - LAGUNA NEW BUSINESS OFFICE','B','2/F and 3/F ROYAL STAR MARKETING INC.','(RSM) BLDG.','CROSSING, CALAMBA','LAGUNA',NULL,'PHILIPPINES',NULL,NULL,NULL,NULL,NULL,NULL,NULL)
/
INSERT INTO sunlife_offices
VALUES
('BINONDOCUST','BINONDO CUSTOMER CENTER','S','G/F DASMARINAS','COR. QUINTIN PAREDES STS.','BINONDO','MANILA',NULL,'PHILIPPINES',NULL,NULL,NULL,NULL,NULL,NULL,NULL)
/
INSERT INTO sunlife_offices
VALUES
('BIRCHMNLA','BIRCH NEW BUSINESS OFFICE','B','2/F SUN LIFE CENTER-BINONDO','DASMARINAS COR. QUINTIN PAREDES STS.','BINONDO','MANILA',NULL,'PHILIPPINES',NULL,NULL,NULL,NULL,NULL,NULL,NULL)
/
INSERT INTO sunlife_offices
VALUES
('BRISTLEILO','BRISTLECONE NBO - ILOILO','B','2/F 168 PLATINUM SQUARE BUILDING','GEN. LUNA ST.',NULL,'ILOILO',NULL,'PHILIPPINES',NULL,NULL,NULL,NULL,NULL,NULL,NULL)
/
INSERT INTO sunlife_offices
VALUES
('CEBUCUST','CEBU CUSTOMER CENTER','S','GROUND FLOOR GLOBE ISLA PLAZA BLDG','PANAY RD. COR. SAMAR LOOP','CEBU BUSINESS PARK','CEBU',NULL,'PHILIPPINES',NULL,NULL,NULL,NULL,NULL,NULL,NULL)
/
INSERT INTO sunlife_offices
VALUES
('CEDARMKTI','CEDAR NEW BUSINESS OFFICE','B','3/F ALL SEASONS BLDG.','112 AGUIRRE ST.','LEGASPI VILLAGE','MAKATI',NULL,'PHILIPPINES',1200,NULL,NULL,NULL,NULL,NULL,NULL)
/
INSERT INTO sunlife_offices
VALUES
('CHESTNUTMKTI','CHESTNUT NEW BUSINESS OFFICE','B','5/F ALL SEASONS BLDG.','112 AGUIRRE ST.','LEGASPI VILLAGE','MAKATI',NULL,'PHILIPPINES',1200,NULL,NULL,NULL,NULL,NULL,NULL)
/
INSERT INTO sunlife_offices
VALUES
('CYPRESSMKTI','CYPRESS NEW BUSINESS OFFICE','B','PENTHOUSE 108','HERRERA ST. FELIZA BLDG.','LEGASPI VILLAGE','MAKATI',NULL,'PHILIPPINES',1200,NULL,NULL,NULL,NULL,NULL,NULL)
/
INSERT INTO sunlife_offices
VALUES
('DAGUPANSO','DAGUPAN SALES OFFICE','B','3/F DON BENITO BLDG.','MAYOMBO DISTRICT',NULL,'DAGUPAN',NULL,'PHILIPPINES',NULL,NULL,NULL,NULL,NULL,NULL,NULL)
/
INSERT INTO sunlife_offices
VALUES
('DAVAOCUST','DAVAO CUSTOMER CENTER','S','GROUND FLOOR PLAZA DE LUISA BLDG.','RAMON MAGSAYSAY AVE.',NULL,'DAVAO',NULL,'PHILIPPINES',NULL,NULL,NULL,NULL,NULL,NULL,NULL)
/
INSERT INTO sunlife_offices
VALUES
('DURIANGSAN','DURIAN - GEN. SANTOS SALES OFFICE','B','3RD FLR PERFECT IMAGE BLDG.','ROXAS EAST AVE.','COR. OSMENA ST.','GENERAL SANTOS',NULL,'PHILIPPINES',9500,NULL,NULL,NULL,NULL,NULL,NULL)
/
INSERT INTO sunlife_offices
VALUES
('DURIANDAV','DURIAN NEW BUSINESS OFFICE DAVAO','B','2/F PLAZA DE LUISA COMMERCIAL COMPLEX','RAMON MAGSAYSAY AVE.',NULL,'DAVAO',NULL,'PHILIPPINES',NULL,NULL,NULL,NULL,NULL,NULL,NULL)
/
INSERT INTO sunlife_offices
VALUES
('EUCALYPTUSMKTI','EUCALYPTUS NEW BUSINESS OFFICE','B','3/F ALL SEASONS BLDG.','112 AGUIRRE ST.','LEGASPI VILLAGE','MAKATI',NULL,'PHILIPPINES',1200,NULL,NULL,NULL,NULL,NULL,NULL)
/
INSERT INTO sunlife_offices
VALUES
('FELIZACUST','FELIZA CUSTOMER CENTER','S','11/F FELIZA BLDG.','108 HERRERA ST.','LEGASPI VILLAGE','MAKATI',NULL,'PHILIPPINES',1200,NULL,NULL,NULL,NULL,NULL,NULL)
/
INSERT INTO sunlife_offices
VALUES
('HOMEOFFDIR','HOME OFFICE DIRECT','S','5/F ACE BUILDING','RADA COR. DELA ROSA ST.','LEGASPI VILLAGE','MAKATI',NULL,'PHILIPPINES',1200,NULL,NULL,NULL,NULL,NULL,NULL)
/
INSERT INTO sunlife_offices
VALUES
('ILOILOCUST','ILOILO CUSTOMER CENTER','S','GROUND FLOOR','PLATINUM SQUARE BLDG.','GEN. LUNA ST.','ILOILO',NULL,'PHILIPPINES',NULL,NULL,NULL,NULL,NULL,NULL,NULL)
/
INSERT INTO sunlife_offices
VALUES
('JACARANDAPAS','JACARANDA NEW BUSINESS OFFICE','B','30/F ORIENT SQUARE BLDG.','EMERALD AVE.','ORTIGAS CENTER','PASIG',NULL,'PHILIPPINES',1600,NULL,NULL,NULL,NULL,NULL,NULL)
/
INSERT INTO sunlife_offices
VALUES
('LEGASPISO','LEGASPI SALES OFFICE','B','3RD FLOOR, LCC EXPRESSMART BLDG.','PENARANDA ST.',NULL,'LEGASPI',NULL,'PHILIPPINES',NULL,NULL,NULL,NULL,NULL,NULL,NULL)
/
INSERT INTO sunlife_offices
VALUES
('LINDENMKTI','LINDEN NEW BUSINESS OFFICE','B','6/F PHILCOX BLDG.','172 SALCEDO ST.','LEGASPI VILLAGE','MAKATI',NULL,'PHILIPPINES',1200,NULL,NULL,NULL,NULL,NULL,NULL)
/
INSERT INTO sunlife_offices
VALUES
('MAGNOLIAOLO','MAGNOLIA WOODS NBO - OLONGAPO SALES OFFICE','B','2F PALM CREST BUSINESS CENTER','765 RIZAL AVENUE WEST TANPINAC','OLONGAPO','ZAMBALES',NULL,'PHILIPPINES',2200,NULL,NULL,NULL,NULL,NULL,NULL)
/
INSERT INTO sunlife_offices
VALUES
('MAGNOLIAPAM','MAGNOLIA WOODS NBO - PAMPANGA NEW BUSINESS OFFICE','B','2/F CHINA BANK BLDG.','DOLORES','SAN FERNANDO','PAMPANGA',NULL,'PHILIPPINES',NULL,NULL,NULL,NULL,NULL,NULL,NULL)
/
INSERT INTO sunlife_offices
VALUES
('MAHOGANYROX','MAHOGANY NBO - ROXAS CITY EXTENSION OFFICE','B','MCKINLEY CORNER','SAN JOSE STS.',NULL,'ROXAS',NULL,'PHILIPPINES',NULL,NULL,NULL,NULL,NULL,NULL,NULL)
/
INSERT INTO sunlife_offices
VALUES
('MAHOGANYKAL','MAHOGANY NBO - AKLAN','B','3/F ALG BLDG.','XIX MARTYRS ST.','KALIBO','AKLAN',NULL,'PHILIPPINES',NULL,NULL,NULL,NULL,NULL,NULL,NULL)
/
INSERT INTO sunlife_offices
VALUES
('MAPLEMNLA','MAPLE NEW BUSINESS OFFICE','B','3/F SUN LIFE CENTER - BINONDO','DASMARINAS COR. QUINTIN PAREDES STS.','BINONDO','MANILA',NULL,'PHILIPPINES',NULL,NULL,NULL,NULL,NULL,NULL,NULL)
/
INSERT INTO sunlife_offices
VALUES
('MARKETDEVDAG','MARKET DEVELOPMENT - DAGUPAN SALES OFFICE','B','3/F DON BENITO BLDG.','MAYOMBO DISTRICT',NULL,'DAGUPAN',NULL,'PHILIPPINES',NULL,NULL,NULL,NULL,NULL,NULL,NULL)
/
INSERT INTO sunlife_offices
VALUES
('MARKETDEVSAN','MARKET DEVELOPMENT - SANTIAGO SALES  OFFICE','B','3/F HERITAGE COMPLEX','MAHARLIKA HIGHWAY','SANTIAGO CITY','ISABELA',NULL,'PHILIPPINES',NULL,NULL,NULL,NULL,NULL,NULL,NULL)
/
INSERT INTO sunlife_offices
VALUES
('MARKETDEVTAR','MARKET DEVELOPMENT - TARLAC UNIT OFFICE','B','2ND FLOOR L and C BLDG','MACARTHUR HIGHWAY','BARRIO SAN ROQUE','TARLAC',NULL,'PHILIPPINES',NULL,NULL,NULL,NULL,NULL,NULL,NULL)
/
INSERT INTO sunlife_offices
VALUES
('MOLAVEMKTI','MOLAVE NEW BUSINESS OFFICE','B','7/F ACE BUILDING','101-103 RADA  COR DELA ROSA STS.','LEGASPI VILLAGE','MAKATI',NULL,'PHILIPPINES',1200,NULL,NULL,NULL,NULL,NULL,NULL)
/
INSERT INTO sunlife_offices
VALUES
('MULAWINMKTI','MULAWIN NEW BUSINESS OFFICE','B','5/F ACE BUILDING','101-103 RADA  COR DELA ROSA STS.','LEGASPI VILLAGE','MAKATI',NULL,'PHILIPPINES',1200,NULL,NULL,NULL,NULL,NULL,NULL)
/
INSERT INTO sunlife_offices
VALUES
('NAGAEXTOFF','NAGA EXTENSION OFFICE','B','4TH FLOOR ROMAR II BLDG.','COR. PADIAN and OJEDA STS.',NULL,'NAGA',NULL,'PHILIPPINES',4400,NULL,NULL,NULL,NULL,NULL,NULL)
/
INSERT INTO sunlife_offices
VALUES
('OLIVECAB','OLIVE NBO - CABANATUAN NEW BUSINESS OFFICE','B','2/F R and M BUILDING H. CONCEPCION','MAHARLIKA HIGHWAY','CABANATUAN CITY','NUEVA ECIJA',NULL,'PHILIPPINES',3100,NULL,NULL,NULL,NULL,NULL,NULL)
/
INSERT INTO sunlife_offices
VALUES
('ORTIGASCUST','ORTIGAS CUSTOMER CENTER','S','30/F ORIENT SQUARE BLDG.','EMERALD AVE.','ORTIGAS CENTER','PASIG',NULL,'PHILIPPINES',1600,NULL,NULL,NULL,NULL,NULL,NULL)
/
INSERT INTO sunlife_offices
VALUES
('OSMANTHUSMNLA','OSMANTHUS NEW BUSINESS OFFICE','B','5/F SUN LIFE CENTER - BINONDO','DASMARINAS COR. QUINTIN PAREDES STS.','BINONDO','MANILA',NULL,'PHILIPPINES',NULL,NULL,NULL,NULL,NULL,NULL,NULL)
/
INSERT INTO sunlife_offices
VALUES
('PALMMKTI','PALM NEW BUSINESS OFFICE','B','5/F ALL SEASONS BLDG.','112 AGUIRRE ST.','LEGASPI VILLAGE','MAKATI',NULL,'PHILIPPINES',1200,NULL,NULL,NULL,NULL,NULL,NULL)
/
INSERT INTO sunlife_offices
VALUES
('PHILCOXCUST','PHILCOX CUSTOMER CENTER','S','6/F PHILCOX BLDG.','172 SALCEDO ST.','LEGASPI VILLAGE','MAKATI',NULL,'PHILIPPINES',1200,NULL,NULL,NULL,NULL,NULL,NULL)
/
INSERT INTO sunlife_offices
VALUES
('QUEZONCITYCUST','QUEZON CITY CUSTOMER CENTER','S','2/F CENTURY IMPERIAL PALACE SUITE','TOMAS MORATO','COR TIMOG AVENUE','QUEZON CITY',NULL,'PHILIPPINES',NULL,NULL,NULL,NULL,NULL,NULL,NULL)
/
INSERT INTO sunlife_offices
VALUES
('REDWOODMKTI','REDWOOD NEW BUSINESS OFFICE','B','PENTHOUSE FELIZA BLDG.','108 HERRERA ST.','LEGASPI VILLAGE','MAKATI',NULL,'PHILIPPINES',1200,NULL,NULL,NULL,NULL,NULL,NULL)
/
INSERT INTO sunlife_offices
VALUES
('RICOGENMKTI','RICOGEN CUSTOMER CENTER','S','5/F ALL SEASONS BLDG.','112 AGUIRRE ST.','LEGASPI VILLAGE','MAKATI',NULL,'PHILIPPINES',1200,NULL,NULL,NULL,NULL,NULL,NULL)
/
INSERT INTO sunlife_offices
VALUES
('SANTIAGOSO','SANTIAGO SALES OFFICE','B','3/F HERITAGE COMPLEX','MAHARLIKA HIGHWAY','SANTIAGO CITY','ISABELA',NULL,'PHILIPPINES',NULL,NULL,NULL,NULL,NULL,NULL,NULL)
/
INSERT INTO sunlife_offices
VALUES
('SEQUOIAMKTI','SEQUOIA NEW BUSINESS OFFICE','B','5/F ALL SEASONS BLDG.','112 AGUIRRE ST.','LEGASPI VILLAGE','MAKATI',NULL,'PHILIPPINES',1200,NULL,NULL,NULL,NULL,NULL,NULL)
/
INSERT INTO sunlife_offices
VALUES
('SLFPICEBU','SUN LIFE FINANCIAL PLANS - REGIONAL SO CEBU','S','3/F GLOBE ISLA PLAZA','PANAY RD. COR. SAMAR LOOP','CEBU BUSINESS PARK','CEBU',NULL,'PHILIPPINES',NULL,NULL,NULL,NULL,NULL,NULL,NULL)
/
INSERT INTO sunlife_offices
VALUES
('SLFPIDAV','SUN LIFE FINANCIAL PLANS - REGIONAL SO DAVAO','S','MEZZANINE FLOOR','PLAZA DE LUISA COMMERCIAL COMPLEX','R. MAGSAYSAY AVE.','DAVAO',NULL,'PHILIPPINES',NULL,NULL,NULL,NULL,NULL,NULL,NULL)
/
INSERT INTO sunlife_offices
VALUES
('SLFPIILO','SUN LIFE FINANCIAL PLANS - REGIONAL SO ILOILO','S','3F 168 PLATINUM SQUARE BLDG','GEN. LUNA ST.',NULL,'ILOILO',NULL,'PHILIPPINES',NULL,NULL,NULL,NULL,NULL,NULL,NULL)
/
INSERT INTO sunlife_offices
VALUES
('TARLACSO','TARLAC SALES OFFICE','B','2ND FLOOR, L and C BLDG','ALONG NATIONAL HIGHWAY','BARRIO SAN ROQUE','TARLAC',NULL,'PHILIPPINES',NULL,NULL,NULL,NULL,NULL,NULL,NULL)
/
INSERT INTO sunlife_offices
VALUES
('TINDALOMKTI','TINDALO NEW BUSINESS OFFICE','B','5/F ACE BUILDING','101-103 RADA  COR DELA ROSA STS.','LEGASPI VILLAGE','MAKATI',NULL,'PHILIPPINES',1200,NULL,NULL,NULL,NULL,NULL,NULL)
/

-- FORM_FIELDS
PROMPT INSERTING FORM_FIELDS
INSERT INTO form_fields
(FF_ID,FF_NAME,FF_APP_MAPPING)
VALUES
(0,'Insured Name',NULL)
/
INSERT INTO form_fields
(FF_ID,FF_NAME,FF_APP_MAPPING)
VALUES
(1,'Client Number',NULL)
/
INSERT INTO form_fields
(FF_ID,FF_NAME,FF_APP_MAPPING)
VALUES
(2,'Policy Number',NULL)
/
INSERT INTO form_fields
(FF_ID,FF_NAME,FF_APP_MAPPING)
VALUES
(3,'Birthdate',NULL)
/
INSERT INTO form_fields
(FF_ID,FF_NAME,FF_APP_MAPPING)
VALUES
(4,'Other Legal Name',NULL)
/
INSERT INTO form_fields
(FF_ID,FF_NAME,FF_APP_MAPPING)
VALUES
(5,'Agent Name',NULL)
/
INSERT INTO form_fields
(FF_ID,FF_NAME,FF_APP_MAPPING)
VALUES
(6,'New Business Office',NULL)
/
INSERT INTO form_fields
(FF_ID,FF_NAME,FF_APP_MAPPING)
VALUES
(7,'Amount Coverage',NULL)
/
INSERT INTO form_fields
(FF_ID,FF_NAME,FF_APP_MAPPING)
VALUES
(8,'Owner Name',NULL)
/
INSERT INTO form_fields
(FF_ID,FF_NAME,FF_APP_MAPPING)
VALUES
(9,'Height in feet',NULL)
/
INSERT INTO form_fields
(FF_ID,FF_NAME,FF_APP_MAPPING)
VALUES
(10,'Height in inches',NULL)
/
INSERT INTO form_fields
(FF_ID,FF_NAME,FF_APP_MAPPING)
VALUES
(11,'Weight',NULL)
/


-- REQUIREMENT_FORM_FIELDS
PROMPT INSERTING REQUIREMENT_FORM_FIELDS
INSERT INTO requirement_form_fields
(RFF_ID,RF_ID,FF_ID,RFF_PAGE,RFF_POSX,RFF_POSY)
VALUES
(12,5,2,1,368,575)
/
INSERT INTO requirement_form_fields
(RFF_ID,RF_ID,FF_ID,RFF_PAGE,RFF_POSX,RFF_POSY)
VALUES
(13,3,0,1,145,590)
/
INSERT INTO requirement_form_fields
(RFF_ID,RF_ID,FF_ID,RFF_PAGE,RFF_POSX,RFF_POSY)
VALUES
(14,3,3,1,450,590)
/
INSERT INTO requirement_form_fields
(RFF_ID,RF_ID,FF_ID,RFF_PAGE,RFF_POSX,RFF_POSY)
VALUES
(15,3,4,1,145,568)
/
INSERT INTO requirement_form_fields
(RFF_ID,RF_ID,FF_ID,RFF_PAGE,RFF_POSX,RFF_POSY)
VALUES
(16,3,5,1,320,30)
/
INSERT INTO requirement_form_fields
(RFF_ID,RF_ID,FF_ID,RFF_PAGE,RFF_POSX,RFF_POSY)
VALUES
(17,6,0,1,145,617)
/
INSERT INTO requirement_form_fields
(RFF_ID,RF_ID,FF_ID,RFF_PAGE,RFF_POSX,RFF_POSY)
VALUES
(18,6,1,1,430,617)
/
INSERT INTO requirement_form_fields
(RFF_ID,RF_ID,FF_ID,RFF_PAGE,RFF_POSX,RFF_POSY)
VALUES
(19,6,2,1,142,592)
/
INSERT INTO requirement_form_fields
(RFF_ID,RF_ID,FF_ID,RFF_PAGE,RFF_POSX,RFF_POSY)
VALUES
(20,6,3,1,285,592)
/
INSERT INTO requirement_form_fields
(RFF_ID,RF_ID,FF_ID,RFF_PAGE,RFF_POSX,RFF_POSY)
VALUES
(21,6,6,1,430,592)
/
INSERT INTO requirement_form_fields
(RFF_ID,RF_ID,FF_ID,RFF_PAGE,RFF_POSX,RFF_POSY)
VALUES
(22,8,0,1,145,617)
/
INSERT INTO requirement_form_fields
(RFF_ID,RF_ID,FF_ID,RFF_PAGE,RFF_POSX,RFF_POSY)
VALUES
(24,8,2,1,142,592)
/
INSERT INTO requirement_form_fields
(RFF_ID,RF_ID,FF_ID,RFF_PAGE,RFF_POSX,RFF_POSY)
VALUES
(23,8,1,1,285,592)
/
INSERT INTO requirement_form_fields
(RFF_ID,RF_ID,FF_ID,RFF_PAGE,RFF_POSX,RFF_POSY)
VALUES
(25,8,6,1,430,592)
/
INSERT INTO requirement_form_fields
(RFF_ID,RF_ID,FF_ID,RFF_PAGE,RFF_POSX,RFF_POSY)
VALUES
(26,9,0,1,145,619)
/
INSERT INTO requirement_form_fields
(RFF_ID,RF_ID,FF_ID,RFF_PAGE,RFF_POSX,RFF_POSY)
VALUES
(27,9,2,1,142,598)
/
INSERT INTO requirement_form_fields
(RFF_ID,RF_ID,FF_ID,RFF_PAGE,RFF_POSX,RFF_POSY)
VALUES
(28,9,1,1,285,598)
/
INSERT INTO requirement_form_fields
(RFF_ID,RF_ID,FF_ID,RFF_PAGE,RFF_POSX,RFF_POSY)
VALUES
(29,9,6,1,438,598)
/
INSERT INTO requirement_form_fields
(RFF_ID,RF_ID,FF_ID,RFF_PAGE,RFF_POSX,RFF_POSY)
VALUES
(30,9,0,1,347,35)
/
INSERT INTO requirement_form_fields
(RFF_ID,RF_ID,FF_ID,RFF_PAGE,RFF_POSX,RFF_POSY)
VALUES
(31,13,0,1,145,579)
/
INSERT INTO requirement_form_fields
(RFF_ID,RF_ID,FF_ID,RFF_PAGE,RFF_POSX,RFF_POSY)
VALUES
(32,13,2,1,430,579)
/
INSERT INTO requirement_form_fields
(RFF_ID,RF_ID,FF_ID,RFF_PAGE,RFF_POSX,RFF_POSY)
VALUES
(33,15,0,1,145,617)
/
INSERT INTO requirement_form_fields
(RFF_ID,RF_ID,FF_ID,RFF_PAGE,RFF_POSX,RFF_POSY)
VALUES
(34,15,2,1,142,592)
/
INSERT INTO requirement_form_fields
(RFF_ID,RF_ID,FF_ID,RFF_PAGE,RFF_POSX,RFF_POSY)
VALUES
(35,15,1,1,285,592)
/
INSERT INTO requirement_form_fields
(RFF_ID,RF_ID,FF_ID,RFF_PAGE,RFF_POSX,RFF_POSY)
VALUES
(36,15,6,1,438,592)
/
INSERT INTO requirement_form_fields
(RFF_ID,RF_ID,FF_ID,RFF_PAGE,RFF_POSX,RFF_POSY)
VALUES
(37,15,0,1,145,617)
/
INSERT INTO requirement_form_fields
(RFF_ID,RF_ID,FF_ID,RFF_PAGE,RFF_POSX,RFF_POSY)
VALUES
(39,15,2,1,142,592)
/
INSERT INTO requirement_form_fields
(RFF_ID,RF_ID,FF_ID,RFF_PAGE,RFF_POSX,RFF_POSY)
VALUES
(40,15,3,1,285,592)
/
INSERT INTO requirement_form_fields
(RFF_ID,RF_ID,FF_ID,RFF_PAGE,RFF_POSX,RFF_POSY)
VALUES
(41,15,6,1,430,592)
/
INSERT INTO requirement_form_fields
(RFF_ID,RF_ID,FF_ID,RFF_PAGE,RFF_POSX,RFF_POSY)
VALUES
(42,24,0,1,143,608)
/
INSERT INTO requirement_form_fields
(RFF_ID,RF_ID,FF_ID,RFF_PAGE,RFF_POSX,RFF_POSY)
VALUES
(43,24,3,1,467,608)
/
INSERT INTO requirement_form_fields
(RFF_ID,RF_ID,FF_ID,RFF_PAGE,RFF_POSX,RFF_POSY)
VALUES
(44,24,7,1,198,582)
/
INSERT INTO requirement_form_fields
(RFF_ID,RF_ID,FF_ID,RFF_PAGE,RFF_POSX,RFF_POSY)
VALUES
(45,24,1,1,430,558)
/
INSERT INTO requirement_form_fields
(RFF_ID,RF_ID,FF_ID,RFF_PAGE,RFF_POSX,RFF_POSY)
VALUES
(46,24,0,2,347,250)
/
INSERT INTO requirement_form_fields
(RFF_ID,RF_ID,FF_ID,RFF_PAGE,RFF_POSX,RFF_POSY)
VALUES
(47,24,8,2,347,220)
/
INSERT INTO requirement_form_fields
(RFF_ID,RF_ID,FF_ID,RFF_PAGE,RFF_POSX,RFF_POSY)
VALUES
(48,20,0,1,145,617)
/
INSERT INTO requirement_form_fields
(RFF_ID,RF_ID,FF_ID,RFF_PAGE,RFF_POSX,RFF_POSY)
VALUES
(49,20,2,1,142,592)
/
INSERT INTO requirement_form_fields
(RFF_ID,RF_ID,FF_ID,RFF_PAGE,RFF_POSX,RFF_POSY)
VALUES
(50,20,1,1,285,592)
/
INSERT INTO requirement_form_fields
(RFF_ID,RF_ID,FF_ID,RFF_PAGE,RFF_POSX,RFF_POSY)
VALUES
(51,20,6,1,438,592)
/
INSERT INTO requirement_form_fields
(RFF_ID,RF_ID,FF_ID,RFF_PAGE,RFF_POSX,RFF_POSY)
VALUES
(52,21,0,1,145,617)
/
INSERT INTO requirement_form_fields
(RFF_ID,RF_ID,FF_ID,RFF_PAGE,RFF_POSX,RFF_POSY)
VALUES
(53,21,2,1,142,592)
/
INSERT INTO requirement_form_fields
(RFF_ID,RF_ID,FF_ID,RFF_PAGE,RFF_POSX,RFF_POSY)
VALUES
(54,21,1,1,285,592)
/
INSERT INTO requirement_form_fields
(RFF_ID,RF_ID,FF_ID,RFF_PAGE,RFF_POSX,RFF_POSY)
VALUES
(55,21,6,1,438,592)
/
INSERT INTO requirement_form_fields
(RFF_ID,RF_ID,FF_ID,RFF_PAGE,RFF_POSX,RFF_POSY)
VALUES
(56,26,0,1,152,619)
/
INSERT INTO requirement_form_fields
(RFF_ID,RF_ID,FF_ID,RFF_PAGE,RFF_POSX,RFF_POSY)
VALUES
(57,26,1,1,440,619)
/
INSERT INTO requirement_form_fields
(RFF_ID,RF_ID,FF_ID,RFF_PAGE,RFF_POSX,RFF_POSY)
VALUES
(58,26,2,1,152,594)
/
INSERT INTO requirement_form_fields
(RFF_ID,RF_ID,FF_ID,RFF_PAGE,RFF_POSX,RFF_POSY)
VALUES
(59,26,3,1,295,594)
/
INSERT INTO requirement_form_fields
(RFF_ID,RF_ID,FF_ID,RFF_PAGE,RFF_POSX,RFF_POSY)
VALUES
(60,26,6,1,440,594)
/
INSERT INTO requirement_form_fields
(RFF_ID,RF_ID,FF_ID,RFF_PAGE,RFF_POSX,RFF_POSY)
VALUES
(61,35,0,1,142,624)
/
INSERT INTO requirement_form_fields
(RFF_ID,RF_ID,FF_ID,RFF_PAGE,RFF_POSX,RFF_POSY)
VALUES
(62,35,2,1,142,603)
/
INSERT INTO requirement_form_fields
(RFF_ID,RF_ID,FF_ID,RFF_PAGE,RFF_POSX,RFF_POSY)
VALUES
(63,35,1,1,275,603)
/
INSERT INTO requirement_form_fields
(RFF_ID,RF_ID,FF_ID,RFF_PAGE,RFF_POSX,RFF_POSY)
VALUES
(64,35,6,1,425,603)
/
INSERT INTO requirement_form_fields
(RFF_ID,RF_ID,FF_ID,RFF_PAGE,RFF_POSX,RFF_POSY)
VALUES
(65,35,0,1,345,28)
/
INSERT INTO requirement_form_fields
(RFF_ID,RF_ID,FF_ID,RFF_PAGE,RFF_POSX,RFF_POSY)
VALUES
(66,29,0,1,142,617)
/
INSERT INTO requirement_form_fields
(RFF_ID,RF_ID,FF_ID,RFF_PAGE,RFF_POSX,RFF_POSY)
VALUES
(67,29,2,1,142,592)
/
INSERT INTO requirement_form_fields
(RFF_ID,RF_ID,FF_ID,RFF_PAGE,RFF_POSX,RFF_POSY)
VALUES
(68,29,1,1,285,592)
/
INSERT INTO requirement_form_fields
(RFF_ID,RF_ID,FF_ID,RFF_PAGE,RFF_POSX,RFF_POSY)
VALUES
(69,29,6,1,430,592)
/
INSERT INTO requirement_form_fields
(RFF_ID,RF_ID,FF_ID,RFF_PAGE,RFF_POSX,RFF_POSY)
VALUES
(70,30,0,1,142,621)
/
INSERT INTO requirement_form_fields
(RFF_ID,RF_ID,FF_ID,RFF_PAGE,RFF_POSX,RFF_POSY)
VALUES
(71,30,2,1,142,595)
/
INSERT INTO requirement_form_fields
(RFF_ID,RF_ID,FF_ID,RFF_PAGE,RFF_POSX,RFF_POSY)
VALUES
(72,30,1,1,285,595)
/
INSERT INTO requirement_form_fields
(RFF_ID,RF_ID,FF_ID,RFF_PAGE,RFF_POSX,RFF_POSY)
VALUES
(73,30,6,1,439,595)
/
INSERT INTO requirement_form_fields
(RFF_ID,RF_ID,FF_ID,RFF_PAGE,RFF_POSX,RFF_POSY)
VALUES
(74,31,0,1,142,621)
/
INSERT INTO requirement_form_fields
(RFF_ID,RF_ID,FF_ID,RFF_PAGE,RFF_POSX,RFF_POSY)
VALUES
(75,31,2,1,142,595)
/
INSERT INTO requirement_form_fields
(RFF_ID,RF_ID,FF_ID,RFF_PAGE,RFF_POSX,RFF_POSY)
VALUES
(76,31,1,1,285,595)
/
INSERT INTO requirement_form_fields
(RFF_ID,RF_ID,FF_ID,RFF_PAGE,RFF_POSX,RFF_POSY)
VALUES
(77,31,6,1,430,595)
/
INSERT INTO requirement_form_fields
(RFF_ID,RF_ID,FF_ID,RFF_PAGE,RFF_POSX,RFF_POSY)
VALUES
(78,31,9,1,180,570)
/
INSERT INTO requirement_form_fields
(RFF_ID,RF_ID,FF_ID,RFF_PAGE,RFF_POSX,RFF_POSY)
VALUES
(79,31,10,1,215,570)
/
INSERT INTO requirement_form_fields
(RFF_ID,RF_ID,FF_ID,RFF_PAGE,RFF_POSX,RFF_POSY)
VALUES
(80,31,11,1,460,570)
/
INSERT INTO requirement_form_fields
(RFF_ID,RF_ID,FF_ID,RFF_PAGE,RFF_POSX,RFF_POSY)
VALUES
(81,33,0,1,142,617)
/
INSERT INTO requirement_form_fields
(RFF_ID,RF_ID,FF_ID,RFF_PAGE,RFF_POSX,RFF_POSY)
VALUES
(82,33,2,1,142,592)
/
INSERT INTO requirement_form_fields
(RFF_ID,RF_ID,FF_ID,RFF_PAGE,RFF_POSX,RFF_POSY)
VALUES
(83,33,1,1,285,592)
/
INSERT INTO requirement_form_fields
(RFF_ID,RF_ID,FF_ID,RFF_PAGE,RFF_POSX,RFF_POSY)
VALUES
(84,33,6,1,440,592)
/
INSERT INTO requirement_form_fields
(RFF_ID,RF_ID,FF_ID,RFF_PAGE,RFF_POSX,RFF_POSY)
VALUES
(85,32,0,1,142,604)
/
INSERT INTO requirement_form_fields
(RFF_ID,RF_ID,FF_ID,RFF_PAGE,RFF_POSX,RFF_POSY)
VALUES
(86,32,2,1,142,578)
/
INSERT INTO requirement_form_fields
(RFF_ID,RF_ID,FF_ID,RFF_PAGE,RFF_POSX,RFF_POSY)
VALUES
(87,32,1,1,285,578)
/
INSERT INTO requirement_form_fields
(RFF_ID,RF_ID,FF_ID,RFF_PAGE,RFF_POSX,RFF_POSY)
VALUES
(88,32,6,1,430,578)
/
INSERT INTO requirement_form_fields
(RFF_ID,RF_ID,FF_ID,RFF_PAGE,RFF_POSX,RFF_POSY)
VALUES
(89,37,0,1,142,607)
/
INSERT INTO requirement_form_fields
(RFF_ID,RF_ID,FF_ID,RFF_PAGE,RFF_POSX,RFF_POSY)
VALUES
(90,37,2,1,142,581)
/
INSERT INTO requirement_form_fields
(RFF_ID,RF_ID,FF_ID,RFF_PAGE,RFF_POSX,RFF_POSY)
VALUES
(91,37,1,1,285,581)
/
INSERT INTO requirement_form_fields
(RFF_ID,RF_ID,FF_ID,RFF_PAGE,RFF_POSX,RFF_POSY)
VALUES
(92,37,6,1,430,581)
/
INSERT INTO requirement_form_fields
(RFF_ID,RF_ID,FF_ID,RFF_PAGE,RFF_POSX,RFF_POSY)
VALUES
(93,34,0,1,142,617)
/
INSERT INTO requirement_form_fields
(RFF_ID,RF_ID,FF_ID,RFF_PAGE,RFF_POSX,RFF_POSY)
VALUES
(94,34,2,1,142,592)
/
INSERT INTO requirement_form_fields
(RFF_ID,RF_ID,FF_ID,RFF_PAGE,RFF_POSX,RFF_POSY)
VALUES
(95,34,1,1,285,592)
/
INSERT INTO requirement_form_fields
(RFF_ID,RF_ID,FF_ID,RFF_PAGE,RFF_POSX,RFF_POSY)
VALUES
(96,34,6,1,440,592)
/
INSERT INTO requirement_form_fields
(RFF_ID,RF_ID,FF_ID,RFF_PAGE,RFF_POSX,RFF_POSY)
VALUES
(1,1,0,1,145,620)
/
INSERT INTO requirement_form_fields
(RFF_ID,RF_ID,FF_ID,RFF_PAGE,RFF_POSX,RFF_POSY)
VALUES
(2,1,1,1,145,595)
/
INSERT INTO requirement_form_fields
(RFF_ID,RF_ID,FF_ID,RFF_PAGE,RFF_POSX,RFF_POSY)
VALUES
(3,1,2,1,368,595)
/
INSERT INTO requirement_form_fields
(RFF_ID,RF_ID,FF_ID,RFF_PAGE,RFF_POSX,RFF_POSY)
VALUES
(4,4,0,1,368,640)
/
INSERT INTO requirement_form_fields
(RFF_ID,RF_ID,FF_ID,RFF_PAGE,RFF_POSX,RFF_POSY)
VALUES
(5,4,1,1,40,640)
/
INSERT INTO requirement_form_fields
(RFF_ID,RF_ID,FF_ID,RFF_PAGE,RFF_POSX,RFF_POSY)
VALUES
(6,4,2,1,220,640)
/
INSERT INTO requirement_form_fields
(RFF_ID,RF_ID,FF_ID,RFF_PAGE,RFF_POSX,RFF_POSY)
VALUES
(7,4,0,2,405,708)
/
INSERT INTO requirement_form_fields
(RFF_ID,RF_ID,FF_ID,RFF_PAGE,RFF_POSX,RFF_POSY)
VALUES
(8,4,1,2,145,708)
/
INSERT INTO requirement_form_fields
(RFF_ID,RF_ID,FF_ID,RFF_PAGE,RFF_POSX,RFF_POSY)
VALUES
(9,4,2,2,295,708)
/
INSERT INTO requirement_form_fields
(RFF_ID,RF_ID,FF_ID,RFF_PAGE,RFF_POSX,RFF_POSY)
VALUES
(10,5,0,1,145,602)
/
INSERT INTO requirement_form_fields
(RFF_ID,RF_ID,FF_ID,RFF_PAGE,RFF_POSX,RFF_POSY)
VALUES
(11,5,1,1,145,575)
/


-- NOTIFICATION_TEMPLATES
PROMPT INSERTING NOTIFICATION_TEMPLATES
INSERT INTO notification_templates
VALUES
(1,'New Business Review For <Reference No>','Creation of Assessment Request','Hi,
'|| chr(13) ||'
The following <LOB> application has been submitted for your review:
'|| chr(13) ||'
Reference No: <Reference No>'|| chr(13) ||'
Insured Client No: <Client No>
'|| chr(13) ||'
Link to Request: <URL>
','NBS',NULL,NULL,'',NULL,'IUMADMIN','26-MAR-04 11.23.52.000000 AM')
/
INSERT INTO notification_templates
VALUES
(2,'Assessment Request for <Reference No>','For Transmittal','Hi,
'|| chr(13) ||'
New Business has requested the assessment of a/an <LOB> application with the following details:
'|| chr(13) ||'
Reference No: <Reference No>'|| chr(13) ||'
Insured Client No: <Client No>
'|| chr(13) ||'
Link to Assessment Request: <URL>
','NBS',NULL,NULL,'IUMADMIN',NULL,'IUMADMIN','26-MAR-04 11.24.35.000000 AM')
/
INSERT INTO notification_templates
VALUES
(3,'Assessment Request for <Reference No>','For Assessment','Hi,
'|| chr(13) ||'
New Business has requested the assessment of a/an <LOB> application with the following details:
'|| chr(13) ||'
Reference No: <Reference No>'|| chr(13) ||'
Insured Client No: <Client No>
'|| chr(13) ||'
Link to Request: <URL>
','FCLTOR',NULL,NULL,'IUMADMIN',NULL,'IUMADMIN','26-MAR-04 11.24.55.000000 AM')
/
INSERT INTO notification_templates
VALUES
(4,'Reassignment of Assessment Requests','Reassignment','Please take note of the change on the Underwriter assignment for the following application:
'|| chr(13) ||'
<Reasssignment Details>
'|| chr(13) ||'
Regards,'|| chr(13) ||'
<USD Supervisor/Manager>
','USDSVR',NULL,NULL,'IUMADMIN',NULL,'IUMADMIN','11-MAR-04 08.26.51.000000 PM')
/
INSERT INTO notification_templates
VALUES
(5,'New Requirements for Reference No. <Reference No> - <Client Name>','Ordering of Requirements','Please submit the following requirement:
'|| chr(13) ||'
<Requirement Details>'|| chr(13) ||'
<PDF Attachment>
'|| chr(13) ||'
Regards,'|| chr(13) ||'
<Underwriter Name>'|| chr(13) ||'
','UDRWTR','1','RE: PN <Reference No> ON THE LIFE OF <Client Name>. PLEASE VIEW YOUR EMAIL OR CONTACT MKTG STAFF FOR THE U/W REQMTS NOW.','IUMADMIN',NULL,'IUMADMIN','08-MAR-04 07.21.17.000000 PM')
/
INSERT INTO notification_templates
VALUES
(6,'Cancelled Requirement for Reference No. <Reference No> - <Client Name>','Cancellation of Requirements','The following requirement has been cancelled.  Please do not submit anymore:
'|| chr(13) ||'
- <Requirement Description>
'|| chr(13) ||'
Regards,'|| chr(13) ||'
<Underwriter Name>
','UDRWTR','1','RE: PN <Reference No> ON THE LIFE OF <Client Name>. PLEASE VIEW YOUR EMAIL OR CONTACT MKTG STAFF FOR THE U/W REQMTS NOW.','IUMADMIN',NULL,'IUMADMIN','19-MAR-04 11.09.39.000000 PM')
/
INSERT INTO notification_templates
VALUES
(7,'Follow Up on Requirement for Reference No. <Reference No> - <Client Name>','Follow Up Requirements','This is to follow up on the following requirement:
'|| chr(13) ||'
- <Requirement Details>
'|| chr(13) ||'
Regards,'|| chr(13) ||'
<Underwriter Name>
','NBS',NULL,NULL,'IUMADMIN',NULL,'IUMADMIN','25-MAR-04 01.00.36.000000 PM')
/
INSERT INTO notification_templates
VALUES
(8,'Waived Requirement for Reference No. <Reference No> - <Client Name>','Waiver of Requirement','The following requirement has been cancelled.  Please do not submit anymore:
'|| chr(13) ||'
- <Requirement Description>
'|| chr(13) ||'
Regards,'|| chr(13) ||'
<Underwriter Name>
','NBRVW',NULL,NULL,'IUMADMIN',NULL,'IUMADMIN','19-MAR-04 11.46.01.000000 PM')
/
INSERT INTO notification_templates
VALUES
(9,'<Exam Type> Exam Requested for Your Attention: <Client Name>','Request for Med/Lab Exam','<URL>
','NBRVW',NULL,NULL,'IUMADMIN',NULL,'IUMADMIN','30-MAR-04 07.08.33.000000 PM')
/
INSERT INTO notification_templates
VALUES
(10,'Confirmation of <Exam Type> Exam Appointment: <Client Name>','Confirmation of Med Exam Request','Client Name:  <Client Name>'|| chr(13) ||'
Medical Exam:  <Exam Type> exam'|| chr(13) ||'
<Laboratory Examiner>: <Laboratory/Examiner Name>
'|| chr(13) ||'
Appointment Date:  <Appointment Date>'|| chr(13) ||'
Appointment Time:  <Appointment Time>
'|| chr(13) ||'
','MEDSTF',NULL,NULL,'IUMADMIN',NULL,'IUMADMIN','30-MAR-04 07.55.34.000000 PM')
/
INSERT INTO notification_templates
VALUES
(11,'Receipt of Requirements for Reference No.: <Reference No>','Receipt of Requirements at Site','The original copies of the following requirements were received:
'|| chr(13) ||'
<Requirement>
'|| chr(13) ||'
The said documents will be transmitted to your end as necessary.
'|| chr(13) ||'
Link to Assessment Request: <URL>
','NBS',NULL,NULL,'IUMADMIN',NULL,'IUMADMIN','26-MAR-04 11.27.20.000000 AM')
/
INSERT INTO notification_templates
VALUES
(12,'<Exam Type> Exam Edited for Your Attention: <Client Name>','Entry of Medical Results','Link to Medical Exam: <URL>
','MEDSTF',NULL,NULL,'IUMADMIN',NULL,'IUMADMIN','26-MAR-04 06.58.26.000000 PM')
/
INSERT INTO notification_templates
VALUES
(13,'USD Memo: Assessment Result for Reference No. <Reference No>','Assessment Result from USD','Hi,
'|| chr(13) ||'
This is to inform you of USD''s assessment for the following application:
'|| chr(13) ||'
Reference No: <Reference No>'|| chr(13) ||'
Insured Client No: <Client No>
'|| chr(13) ||'
Decision: <Application Status>
'|| chr(13) ||'
Regards,'|| chr(13) ||'
Underwriting Services Department
','UDRWTR','1','RE: PN <Reference No> ON THE LIFE OF <Client Name>. PLEASE VIEW YOUR EMAIL OR CONTACT MKTG STAFF FOR THE DECISION ON APP NOW.','IUMADMIN',NULL,'IUMADMIN','11-MAR-04 08.22.11.000000 PM')
/
INSERT INTO notification_templates
VALUES
(14,'New Business Memo: Assessment Result for Reference No. <Reference No>','Assessment Result from NB','Hi,
'|| chr(13) ||'
This is to inform you of the decision for the following application:
'|| chr(13) ||'
Reference No: <Reference No>'|| chr(13) ||'
Insured Client No: <Client No>
'|| chr(13) ||'
Decision: <Application Status>
'|| chr(13) ||'
Regards,'|| chr(13) ||'
New Business Department
','NBADMN','1','RE: PN <Reference No> ON THE LIFE OF <Client Name>. PLEASE VIEW YOUR EMAIL OR CONTACT MKTG STAFF FOR THE DECISION ON APP NOW.','IUMADMIN',NULL,'IUMADMIN','11-MAR-04 08.21.44.000000 PM')
/
INSERT INTO notification_templates
VALUES
(15,'Medical Assessment Referral for your attention. Reference No.: <Reference No>','Doctors Notes','Link to Assessment Request: <URL>','NBADMN',NULL,NULL,'IUMADMIN',NULL,'IUMADMIN','26-MAR-04 11.26.16.000000 AM')
/

-- DEPARTMENTS
PROMPT INSERTING DEPARTMENTS
INSERT INTO departments (dept_code, dept_desc)
	VALUES ('CLIENTSVCS', 'CLIENT SERVICES DEPARTMENT');

INSERT INTO departments (dept_code, dept_desc)
	VALUES ('UWSVCS', 'UNDERWRITING SERVICES DEPARTMENT');
	
INSERT INTO departments (dept_code, dept_desc)
	VALUES ('MEDICAL', 'MEDICAL DEPARTMENT');
	
INSERT INTO departments (dept_code, dept_desc)
	VALUES ('GROUPINS', 'GROUP INSURANCE DEPARTMENT');
	
INSERT INTO departments (dept_code, dept_desc)
	VALUES ('SALESMKTG', 'SALES AND MARKETING DEPARTMENT');

INSERT INTO departments (dept_code, dept_desc)
	VALUES ('MGMTSVCS', 'MANAGEMENT SERVICES DEPARTMENT');
	
INSERT INTO departments (dept_code, dept_desc)
	VALUES ('OTHERDEPT', 'OTHER DEPARTMENTS');
	
INSERT INTO departments (dept_code, dept_desc)
	VALUES ('SYSTEMS', 'CENTRAL SYSTEMS');

-- SECTIONS
PROMPT INSERTING SECTIONS
INSERT INTO sections (sec_code, sec_desc) 
	VALUES ('NBREVIEW', 'NEW BUSINESS REVIEW GROUP');

INSERT INTO sections (sec_code, sec_desc) 
	VALUES ('UWRITING', 'UNDERWRITING');
	
INSERT INTO sections (sec_code, sec_desc) 
	VALUES ('FACILITN', 'UNDERWRITING FACILITATION');
	
INSERT INTO sections (sec_code, sec_desc) 
	VALUES ('NBADMIN', 'NEW BUSINESS ADMIN');
	
INSERT INTO sections (sec_code, sec_desc) 
	VALUES ('POLICYCHG', 'POLICY CHANGE');
	
INSERT INTO sections (sec_code, sec_desc) 
	VALUES ('MEDCONSLT', 'MEDICAL CONSULTATION');
	
INSERT INTO sections (sec_code, sec_desc) 
	VALUES ('MEDADMIN', 'MEDICAL ADMINISTRATION');
	
INSERT INTO sections (sec_code, sec_desc) 
	VALUES ('MEDPROVIDER', 'ACCREDITED MEDICAL PROVIDERS');
	
INSERT INTO sections (sec_code, sec_desc) 
	VALUES ('GROUPADM', 'GROUP ADMINISTRATION');
	
INSERT INTO sections (sec_code, sec_desc) 
	VALUES ('AGENTS', 'AGENTS'' GROUP');
	
INSERT INTO sections (sec_code, sec_desc) 
	VALUES ('SALESADMIN', 'SALES ADMINISTRATION');
	
INSERT INTO sections (sec_code, sec_desc) 
	VALUES ('PMGMTOFF', 'PROJECT MANAGEMENT OFFICE');
	
INSERT INTO sections (sec_code, sec_desc) 
	VALUES ('QASSURANCE', 'QUALITY ASSURANCE');
	
INSERT INTO sections (sec_code, sec_desc) 
	VALUES ('OTHERSECT', 'OTHER DEPARTMENT SECTIONS');
	
INSERT INTO sections (sec_code, sec_desc) 
	VALUES ('QAINFOSEC', 'QUALITY ASSURANCE AND INFO SECURITY');

INSERT INTO sections (sec_code, sec_desc) 
	VALUES ('DATACTR', 'DATA CENTER');


-- TD_COUNTER
PROMPT INSERTING TD_COUNTER
Insert into TD_COUNTER values (1, sysdate);



