INSERT INTO roles (role_code, role_desc) 
	VALUES ('GRPNBADMIN', 'GROUP NEW BUSINESS ADMINISTRATOR');

INSERT INTO roles (role_code, role_desc) 
	VALUES ('NBSTAFF', 'NEW BUSINESS STAFF');

INSERT INTO roles (role_code, role_desc) 
	VALUES ('NBREVIEWER', 'NEW BUSINESS REVIEWER');

INSERT INTO roles (role_code, role_desc) 
	VALUES ('NBSUPMGR', 'NEW BUSINESS SUPERVISOR/MANAGER');

INSERT INTO roles (role_code, role_desc) 
	VALUES ('FACILITSUPMGR', 'FACILITATOR SUPERVISOR/MANAGER');

INSERT INTO roles (role_code, role_desc) 
	VALUES ('FACILITATOR', 'UNDERWRITING FACILITATOR');

INSERT INTO roles (role_code, role_desc) 
	VALUES ('UNDERWRITER', 'UNDERWRITER');

INSERT INTO roles (role_code, role_desc) 
	VALUES ('UWTNGSUPMGR', 'UNDERWRITING SUPERVISOR/MANAGER');

INSERT INTO roles (role_code, role_desc) 
	VALUES ('MEDICALADMIN', 'MEDICAL ADMIN STAFF');

INSERT INTO roles (role_code, role_desc) 
	VALUES ('MEDSUPMGR', 'MEDICAL SUPERVISOR/MANAGER/CONSULTANG');

INSERT INTO roles (role_code, role_desc) 
	VALUES ('MEDCONSLT', 'MEDICAL CONSULTANT');

INSERT INTO roles (role_code, role_desc) 
	VALUES ('MKTGSTAFF', 'MARKETING STAFF');

INSERT INTO roles (role_code, role_desc) 
	VALUES ('AGENT', 'AGENTS');

INSERT INTO roles (role_code, role_desc) 
	VALUES ('EXAMINER', 'ACCREDITED DOCTOR/EXAMINER');

INSERT INTO roles (role_code, role_desc) 
	VALUES ('LABSTAFF', 'ACCREDITED LABORATORY STAFF');

INSERT INTO roles (role_code, role_desc) 
	VALUES ('OTHERUSER', 'OTHER DEPARMENT USERS');

INSERT INTO roles (role_code, role_desc) 
	VALUES ('LOVADMIN', 'LIST OF VALUES ADMINISTRATOR');

INSERT INTO roles (role_code, role_desc) 
	VALUES ('SCTYADMIN', 'SECURITY ADMIN');

INSERT INTO roles (role_code, role_desc) 
	VALUES ('SYSOPER', 'SYSTEM OPERATOR');
