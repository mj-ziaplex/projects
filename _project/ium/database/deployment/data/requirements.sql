INSERT INTO requirements
	VALUES ('AOO','Acceptance of Offer', null,'C',null,'Y','001');

INSERT INTO requirements
	VALUES ('OFF','Accptnce of Offr Form w/ Exclsn Provisn', null,null,null,'N','002');

INSERT INTO requirements
	VALUES ('AMLA','Addtl Reqts for the Appl Under the Anti-',null,null,null, 'Y','003');

INSERT INTO requirements
	VALUES ('ACR','Agent''s Confidential Report',null,'C',null,'Y','004');

INSERT INTO requirements
	VALUES ('AOAU','Alteration of Application',null,'P',null,'Y','005');

INSERT INTO requirements
	VALUES ('ARTQ','Arthritis Questionnaire',null,'C',null,'Y','006');

INSERT INTO requirements
	VALUES ('APS','Attending Physician''s Statement',null,'C',null,'Y','007');

INSERT INTO requirements
	VALUES ('ARQ', 'Attending Physician''s Statement', null,'C', null, 'Y', '008');

INSERT INTO requirements
	VALUES ('AVQ', 'Aviation Questionnaire', null,'C',null, 'Y', '009');

INSERT INTO requirements
	VALUES ('FQ','Business Financial Questionnaire', null,null,null, 'Y', '010');

INSERT INTO requirements
	VALUES ('PCI','Certificate of Insurability (age 16 & over)', null,null,null, 'Y', '011');

INSERT INTO requirements
	VALUES ('PCI1','Certificate of Insurability (child under age 16)', null,null,null, 'Y', '012');

INSERT INTO requirements
	VALUES ('DECL1','Certification re: New Signature', null,null,null, 'Y', '013');

INSERT INTO requirements
	VALUES ('DVQ','Diving Questionnaire',null,'C',null, 'Y', '015');

INSERT INTO requirements
	VALUES ('DRQ','Drug Usage Questionnaire', null,'C',null, 'Y', '016');

INSERT INTO requirements
	VALUES ('MCQ','Mountain Climbing Questionnaire', null,'C',null, 'Y', '020');

INSERT INTO requirements
	VALUES ('PSQ','Parachuting/Sky Diving Questionnaire', null,'C',null, 'Y', '021');

INSERT INTO requirements
	VALUES ('EXMA1','Part II of Application for Insurance on Chile age 15 and  under', null,null,null, 'Y', '022');

INSERT INTO requirements
	VALUES ('EXMA','Part II for Insurance on Adult', null,null,null, 'Y', '023');

INSERT INTO requirements
	VALUES ('PFQ','Personal Financial Questionnaire', null,'C',null, 'Y', '024');

INSERT INTO requirements
	VALUES ('RESQ','Residential Background Questionnaire', null,null,null, 'Y', '026');

INSERT INTO requirements
	VALUES ('SCERT','Secretary''s Certificate', null,'C',null, 'N', '027');

INSERT INTO requirements
	VALUES ('SPA','Special Power of Attorney', null,'P',null, 'N', '028');

INSERT INTO requirements
	VALUES ('ABQ','Supp Questns Asthma, Bronchitis or other Pul', null,'C',null, 
'Y', '029');

INSERT INTO requirements
	VALUES ('CPQ','Supplementary Questions concerning Chest Pain or Discomfort' , null,'C',null, 'Y', '030');

INSERT INTO requirements
	VALUES ('DBQ','Suppl Questns Diabetes(or suspctd Diabts' , null,'C',null, 'Y', '031');

INSERT INTO requirements
	VALUES ('DGQ','Suppl Questnr Digestive or BowelDisorder', null,'C',null, 'Y', '032');

INSERT INTO requirements
	VALUES ('LCQ','Suppl Questns Fainting or Episodes of ', null,'C',null, 'Y', 
'033');

INSERT INTO requirements
	VALUES ('ALQ','Suppl Questns Concerning use of Alcohol', null,'C',null, 'Y', '034');

INSERT INTO requirements
	VALUES ('FMBQ','Suppl Questns for Female/Maternity Benft', null,'C',null, 'Y', '035');

INSERT INTO requirements
	VALUES ('CYSQ','Suppl Questns re Growth, Cysts, Lumps and', null,'C',null, 'Y', '036');

INSERT INTO requirements
	VALUES ('GYNQ','Suppl Questns re Gynecological Complaints', null,'C',null, 'Y', '037');

INSERT INTO requirements
	VALUES ('WPBA','Waiver of Benefit APDB (form for advance payment disability benefit', null,'P',null, 'Y', '038');

INSERT INTO requirements
	VALUES ('WPBC','Waiver of Benefit CCR (form for critical condition rider)', null,'P',null, 'Y', '039');

INSERT INTO requirements
	VALUES ('WPBF','Waiver of Benefit F&MB (form for female & maternity benefit)', null,'P',null, 'Y', '040');

INSERT INTO requirements
	VALUES ('WPBH','Waiver of Benefit HIB (form for hospital', null,'C',null, 'Y', '041');
