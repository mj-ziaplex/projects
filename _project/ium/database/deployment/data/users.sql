INSERT INTO users (user_id, usr_acf2id, usr_last_name, usr_middle_name, 
                   usr_first_name, usr_email_address, slo_id, dept_id, sec_id, created_by) 
 VALUES ('IUMDEV', '', 'DEVELOPMENT', 'USER', 'IUM', 'IUMDEV@POINTWEST.COM.PH', 
         'SUNLIFEHO','QAINFOSEC', 'SYSTEMS', 'IUMDEV');
         
INSERT INTO users (user_id, usr_acf2id, usr_last_name, usr_middle_name, 
                   usr_first_name, usr_email_address, slo_id, dept_id, sec_id, created_by) 
 VALUES ('DREPU', 'P458', 'REPUYAN', 'DEOGRACIA', 'LAZARO', '',
         'SUNLIFEHO', 'UWSVCS', 'UWRITING', 'IUMDEV');
         
INSERT INTO users (user_id, usr_acf2id, usr_last_name, usr_middle_name, 
                   usr_first_name, usr_email_address, slo_id, dept_id, sec_id, created_by) 
 VALUES ('MARAN', 'P139', 'ARANA', 'MICHELLE', 'JAO', '', 
         'SUNLIFEHO', 'UWSVCS', 'UWRITING', 'IUMDEV');
         
INSERT INTO users (user_id, usr_acf2id, usr_last_name, usr_middle_name, 
                   usr_first_name, usr_email_address, slo_id, dept_id, sec_id, created_by)          
 VALUES ('MRAMO', 'P483', 'RAMOS', 'MA. LUISA', 'EVANGELISTA', '', 
         'SUNLIFEHO', 'UWSVCS', 'UWRITING', 'IUMDEV');

INSERT INTO users (user_id, usr_acf2id, usr_last_name, usr_middle_name, 
                   usr_first_name, usr_email_address, slo_id, dept_id, sec_id, created_by) 
 VALUES ('SYAP', 'P533', 'YAP', 'SERGIA', 'RESURRECCION', '', 
         'SUNLIFEHO', 'UWSVCS', 'UWRITING', 'IUMDEV');

INSERT INTO users (user_id, usr_acf2id, usr_last_name, usr_middle_name, 
                   usr_first_name, usr_email_address, slo_id, dept_id, sec_id, created_by)          
 VALUES ('LMIGU', 'P782', 'MIGUEL', 'LARA MAE', 'TIMBOL', '', 
         'SUNLIFEHO', 'UWSVCS', 'UWRITING', 'IUMDEV');

INSERT INTO users (user_id, usr_acf2id, usr_last_name, usr_middle_name, 
                   usr_first_name, usr_email_address, slo_id, dept_id, sec_id, created_by)          
 VALUES ('ASORI', 'P718', 'SORIANO', 'APRIL CHRIS', 'GERONIMO', '', 
         'SUNLIFEHO', 'UWSVCS', 'UWRITING', 'IUMDEV');

INSERT INTO users (user_id, usr_acf2id, usr_last_name, usr_middle_name, 
                   usr_first_name, usr_email_address, slo_id, dept_id, sec_id, created_by)          
 VALUES ('MLATO', 'P746', 'LATOJA', 'MARIA CRISTINA', 'BORJA', '', 
         'SUNLIFEHO', 'UWSVCS', 'UWRITING', 'IUMDEV');

INSERT INTO users (user_id, usr_acf2id, usr_last_name, usr_middle_name, 
                   usr_first_name, usr_email_address, slo_id, dept_id, sec_id, created_by)          
 VALUES ('MALUC', 'P328', 'LUCILA', 'MARIANNE', 'RIVERA', '', 
         'SUNLIFEHO', 'UWSVCS', 'UWRITING', 'IUMDEV');

INSERT INTO users (user_id, usr_acf2id, usr_last_name, usr_middle_name, 
                   usr_first_name, usr_email_address, slo_id, dept_id, sec_id, created_by)          
 VALUES ('MPERM', 'P855', 'PERMEJO', 'MARY GRACE', 'SOBEJANA', '', 
         'SUNLIFEHO', 'UWSVCS', 'UWRITING', 'IUMDEV');

INSERT INTO users (user_id, usr_acf2id, usr_last_name, usr_middle_name, 
                   usr_first_name, usr_email_address, slo_id, dept_id, sec_id, created_by)          
 VALUES ('CALCA', 'P355', 'ALCAZAR', 'CYRIL', 'ALCAZAR', '', 
         'SUNLIFEHO', 'UWSVCS', 'UWRITING', 'IUMDEV');

INSERT INTO users (user_id, usr_acf2id, usr_last_name, usr_middle_name, 
                   usr_first_name, usr_email_address, slo_id, dept_id, sec_id, created_by)          
 VALUES ('SANTR', 'P058', 'SANTOS', 'ROCHELLE', 'BELMONTE', '', 
         'SUNLIFEHO', 'UWSVCS', 'UWRITING', 'IUMDEV');

INSERT INTO users (user_id, usr_acf2id, usr_last_name, usr_middle_name, 
                   usr_first_name, usr_email_address, slo_id, dept_id, sec_id, created_by)          
 VALUES ('MCANI', 'P432', 'CANLAS', 'MARILOU', 'MARIANO', '', 
         'SUNLIFEHO', 'UWSVCS', 'FACILITN', 'IUMDEV');

INSERT INTO users (user_id, usr_acf2id, usr_last_name, usr_middle_name, 
                   usr_first_name, usr_email_address, slo_id, dept_id, sec_id, created_by)          
 VALUES ('VCORD', 'P362', 'CORDERO', 'VANESSA', 'GALUPO', '', 
         'SUNLIFEHO', 'UWSVCS', 'FACILITN', 'IUMDEV');

INSERT INTO users (user_id, usr_acf2id, usr_last_name, usr_middle_name, 
                   usr_first_name, usr_email_address, slo_id, dept_id, sec_id, created_by)          
 VALUES ('BCUEV', 'PP71', 'CUEVAS', 'BEVERLY', 'PASCUAL', '', 
         'SUNLIFEHO', 'UWSVCS', 'FACILITN', 'IUMDEV');

INSERT INTO users (user_id, usr_acf2id, usr_last_name, usr_middle_name, 
                   usr_first_name, usr_email_address, slo_id, dept_id, sec_id, created_by)          
 VALUES ('RDIMA', 'PP54', 'DIMAGIBA', 'RICHELLE', 'MAGNAMPO', '', 
         'SUNLIFEHO', 'UWSVCS', 'FACILITN', 'IUMDEV');

INSERT INTO users (user_id, usr_acf2id, usr_last_name, usr_middle_name, 
                   usr_first_name, usr_email_address, slo_id, dept_id, sec_id, created_by)          
 VALUES ('DAYOJ', 'P146', 'DAYO', 'JOSEPHINE', 'GANIT', '', 
         'SUNLIFEHO', 'UWSVCS', 'FACILITN', 'IUMDEV');

INSERT INTO users (user_id, usr_acf2id, usr_last_name, usr_middle_name, 
                   usr_first_name, usr_email_address, slo_id, dept_id, sec_id, created_by)          
 VALUES ('VTUBI', 'P493', 'TUBIG', 'VILMA', 'SAYAS', '', 
         'SUNLIFEHO', 'CLIENTSVCS', 'NBREVIEW', 'IUMDEV');

INSERT INTO users (user_id, usr_acf2id, usr_last_name, usr_middle_name, 
                   usr_first_name, usr_email_address, slo_id, dept_id, sec_id, created_by)          
 VALUES ('MRAYM', 'P456', 'RAYMUNDO', 'MARIA CLARA', 'PAPA', '', 
         'SUNLIFEHO', 'CLIENTSVCS', 'NBREVIEW', 'IUMDEV');

INSERT INTO users (user_id, usr_acf2id, usr_last_name, usr_middle_name, 
                   usr_first_name, usr_email_address, slo_id, dept_id, sec_id, created_by)          
 VALUES ('JCALI', 'PP78', 'CALINGO', 'JOEL', '', '', 
         'SUNLIFEHO', 'CLIENTSVCS', 'NBREVIEW', 'IUMDEV');

INSERT INTO users (user_id, usr_acf2id, usr_last_name, usr_middle_name, 
                   usr_first_name, usr_email_address, slo_id, dept_id, sec_id, created_by)          
 VALUES ('ACALD', 'PP27', 'CALDERON', 'ALLAN', '', '', 
         'SUNLIFEHO', 'CLIENTSVCS', 'NBREVIEW', 'IUMDEV');

INSERT INTO users (user_id, usr_acf2id, usr_last_name, usr_middle_name, 
                   usr_first_name, usr_email_address, slo_id, dept_id, sec_id, created_by)          
 VALUES ('NJIME', 'P295', 'JIMENEZ', 'NERISSA', '', '', 
         'SUNLIFEHO', 'CLIENTSVCS', 'NBREVIEW', 'IUMDEV');

INSERT INTO users (user_id, usr_acf2id, usr_last_name, usr_middle_name, 
                   usr_first_name, usr_email_address, slo_id, dept_id, sec_id, created_by)          
 VALUES ('RLABI', 'P326', 'LABISTO', 'RODALLA', '', 'RLABI@SUNLIFE.COM', 
         'SUNLIFEHO', 'CLIENTSVCS', 'NBREVIEW', 'IUMDEV');

INSERT INTO users (user_id, usr_acf2id, usr_last_name, usr_middle_name, 
                   usr_first_name, usr_email_address, slo_id, dept_id, sec_id, created_by)          
 VALUES ('JDACA', 'P174', 'DACANAY', 'JOEL', '', 'JDACA@SUNLIFE.COM', 
         'SUNLIFEHO', 'CLIENTSVCS', 'NBREVIEW', 'IUMDEV');

INSERT INTO users (user_id, usr_acf2id, usr_last_name, usr_middle_name, 
                   usr_first_name, usr_email_address, slo_id, dept_id, sec_id, created_by)          
 VALUES ('ATUGA', 'PP26', 'TUGAOEN', 'ALLADIN', '', 'ATUGA@SUNLIFE.COM', 
         'SUNLIFEHO', 'CLIENTSVCS', 'NBREVIEW', 'IUMDEV');

INSERT INTO users (user_id, usr_acf2id, usr_last_name, usr_middle_name, 
                   usr_first_name, usr_email_address, slo_id, dept_id, sec_id, created_by) 
 VALUES ('ALMMAP', 'P812', 'PASCUAL', 'ALMA', 'PABIRAN', 'ALMAP@SUNLIFE.COM', 
         'SUNLIFEHO', 'CLIENTSVCS', 'NBREVIEW', 'IUMDEV');

INSERT INTO users (user_id, usr_acf2id, usr_last_name, usr_middle_name, 
                   usr_first_name, usr_email_address, slo_id, dept_id, sec_id, created_by) 
 VALUES ('NFERN', 'P007', 'FERNANDEZ', 'NOVITA', 'SISON', 'NFERN@SUNLIFE.COM', 
         'SUNLIFEHO', 'CLIENTSVCS', 'NBREVIEW', 'IUMDEV');

INSERT INTO users (user_id, usr_acf2id, usr_last_name, usr_middle_name, 
                   usr_first_name, usr_email_address, slo_id, dept_id, sec_id, created_by) 
 VALUES ('NSANT', 'P654', 'SANTOS', 'NOEL', 'ARELLANO', 'NSANT@SUNLIFE.COM', 'SUNLIFEHO', 
         'CLIENTSVCS', 'NBREVIEW', 'IUMDEV');


/* for assessment request users 1 - shine vicencio */
/*                              2 - engel          */
/*                              3 - cris           */


/*
 * first set
 */
 
/* group admin */
INSERT INTO users (user_id, usr_acf2id, usr_last_name, usr_middle_name, 
                   usr_first_name, usr_email_address, slo_id, dept_id, 
                   sec_id, created_by) 
VALUES ('REQNBAD1', 'PNBAD1', 'NB Group Admin User1', 'Request', 
		'Assessment', 'shine.vicencio@pointwest.com.ph', 'SUNLIFEHO', 
         'QAINFOSEC', 'NBADMIN', 'IUMDEV');

/* nb staff */
INSERT INTO users (user_id, usr_acf2id, usr_last_name, usr_middle_name, 
                   usr_first_name, usr_email_address, slo_id, dept_id, 
                   sec_id, created_by) 
VALUES ('REQNBST1', 'PNBST1', 'NB Staff User1', 'Request', 
		'Assessment', 'shine.vicencio@pointwest.com.ph', 'SUNLIFEHO', 
         'CLIENTSVCS', 'NBREVIEW', 'IUMDEV');

/* nb reviewer          */
INSERT INTO users (user_id, usr_acf2id, usr_last_name, usr_middle_name, 
                   usr_first_name, usr_email_address, slo_id, dept_id, 
                   sec_id, created_by) 
VALUES ('REQNBRV1', 'PNBST1', 'NB Reviewer User1', 'Request', 
		'Assessment', 'shine.vicencio@pointwest.com.ph', 'SUNLIFEHO', 
         'CLIENTSVCS', 'NBREVIEW', 'IUMDEV');

/* nb supervisor manager */
INSERT INTO users (user_id, usr_acf2id, usr_last_name, usr_middle_name, 
                   usr_first_name, usr_email_address, slo_id, dept_id, 
                   sec_id, created_by) 
VALUES ('REQNBSM1', 'PNBSM1', 'NB SupeMgr User1', 'Request', 
		'Assessment', 'shine.vicencio@pointwest.com.ph', 'SUNLIFEHO', 
         'CLIENTSVCS', 'NBADMIN', 'IUMDEV');

/* Facilitator Supervisor Manager         */
INSERT INTO users (user_id, usr_acf2id, usr_last_name, usr_middle_name, 
                   usr_first_name, usr_email_address, slo_id, dept_id, 
                   sec_id, created_by) 
VALUES ('REQFCSM1', 'PFCSM1', 'Facilitator SupMgr User1', 'Request', 
		'Assessment', 'shine.vicencio@pointwest.com.ph', 'SUNLIFEHO', 
         'UWSVCS', 'FACILITN', 'IUMDEV');

/* Facilitator */
INSERT INTO users (user_id, usr_acf2id, usr_last_name, usr_middle_name, 
                   usr_first_name, usr_email_address, slo_id, dept_id, 
                   sec_id, created_by) 
VALUES ('REQFC1', 'PFC1', 'Facilitator User1', 'Request', 
		'Assessment', 'shine.vicencio@pointwest.com.ph', 'SUNLIFEHO', 
         'UWSVCS', 'FACILITN', 'IUMDEV');         

/* Underwriter */
INSERT INTO users (user_id, usr_acf2id, usr_last_name, usr_middle_name, 
                   usr_first_name, usr_email_address, slo_id, dept_id, 
                   sec_id, created_by) 
VALUES ('REQUW1', 'PUW1', 'Underwriter User1', 'Request', 
		'Assessment', 'shine.vicencio@pointwest.com.ph', 'SUNLIFEHO', 
         'UWSVCS', 'UWRITING', 'IUMDEV');  
         
/* Underwriter with multiple roles */
INSERT INTO users (user_id, usr_acf2id, usr_last_name, usr_middle_name, 
                   usr_first_name, usr_email_address, slo_id, dept_id, 
                   sec_id, created_by) 
VALUES ('REQUW*1', 'PUW*1', 'Underwriter User*1', 'Request', 
		'Assessment', 'shine.vicencio@pointwest.com.ph', 'SUNLIFEHO', 
         'UWSVCS', 'UWRITING', 'IUMDEV');  

/* Underwriter Supervison Manager */
INSERT INTO users (user_id, usr_acf2id, usr_last_name, usr_middle_name, 
                   usr_first_name, usr_email_address, slo_id, dept_id, 
                   sec_id, created_by) 
VALUES ('REQUWSM1', 'PUWSM1', 'Underwriter SupMgr User1', 'Request', 
		'Assessment', 'shine.vicencio@pointwest.com.ph', 'SUNLIFEHO', 
         'UWSVCS', 'UWRITING', 'IUMDEV');  

/* Underwriter Supervison Manager with multiple roles */
INSERT INTO users (user_id, usr_acf2id, usr_last_name, usr_middle_name, 
                   usr_first_name, usr_email_address, slo_id, dept_id, 
                   sec_id, created_by) 
VALUES ('REQUWS*1', 'PUWSM*1', 'Underwriter SupMgr User*1', 'Request', 
		'Assessment', 'shine.vicencio@pointwest.com.ph', 'SUNLIFEHO', 
         'UWSVCS', 'UWRITING', 'IUMDEV');  

/* Agent         */
INSERT INTO users (user_id, usr_acf2id, usr_last_name, usr_middle_name, 
                   usr_first_name, usr_email_address, slo_id, dept_id, 
                   sec_id, created_by) 
VALUES ('REQAG1', 'PAG1', 'Agent User1', 'Request', 
		'Assessment', 'shine.vicencio@pointwest.com.ph', 'SUNLIFEHO', 
         'SALESMKTG', 'AGENTS', 'IUMDEV');  


/*
 * 2nd set 
 *
 */
 
/* group admin */
INSERT INTO users (user_id, usr_acf2id, usr_last_name, usr_middle_name, 
                   usr_first_name, usr_email_address, slo_id, dept_id, 
                   sec_id, created_by) 
VALUES ('REQNBAD2', 'PNBAD2', 'NB Group Admin User2', 'Request', 
		'Assessment', 'engelbert.go@pointwest.com.ph', 'SUNLIFEHO', 
         'QAINFOSEC', 'NBADMIN', 'IUMDEV');

/* nb staff */
INSERT INTO users (user_id, usr_acf2id, usr_last_name, usr_middle_name, 
                   usr_first_name, usr_email_address, slo_id, dept_id, 
                   sec_id, created_by) 
VALUES ('REQNBST2', 'PNBST2', 'NB Staff User2', 'Request', 
		'Assessment', 'engelbert.go@pointwest.com.ph', 'SUNLIFEHO', 
         'CLIENTSVCS', 'NBREVIEW', 'IUMDEV');

/* nb reviewer          */
INSERT INTO users (user_id, usr_acf2id, usr_last_name, usr_middle_name, 
                   usr_first_name, usr_email_address, slo_id, dept_id, 
                   sec_id, created_by) 
VALUES ('REQNBRV2', 'PNBST2', 'NB Reviewer User2', 'Request', 
		'Assessment', 'engelbert.go@pointwest.com.ph', 'SUNLIFEHO', 
         'CLIENTSVCS', 'NBREVIEW', 'IUMDEV');

/* nb supervisor manager */
INSERT INTO users (user_id, usr_acf2id, usr_last_name, usr_middle_name, 
                   usr_first_name, usr_email_address, slo_id, dept_id, 
                   sec_id, created_by) 
VALUES ('REQNBSM2', 'PNBSM2', 'NB SupeMgr User2', 'Request', 
		'Assessment', 'engelbert.go@pointwest.com.ph', 'SUNLIFEHO', 
         'CLIENTSVCS', 'NBADMIN', 'IUMDEV');

/* Facilitator Supervisor Manager         */
INSERT INTO users (user_id, usr_acf2id, usr_last_name, usr_middle_name, 
                   usr_first_name, usr_email_address, slo_id, dept_id, 
                   sec_id, created_by) 
VALUES ('REQFCSM2', 'PFCSM2', 'Facilitator SupMgr User2', 'Request', 
		'Assessment', 'engelbert.go@pointwest.com.ph', 'SUNLIFEHO', 
         'UWSVCS', 'FACILITN', 'IUMDEV');

/* Facilitator */
INSERT INTO users (user_id, usr_acf2id, usr_last_name, usr_middle_name, 
                   usr_first_name, usr_email_address, slo_id, dept_id, 
                   sec_id, created_by) 
VALUES ('REQFC2', 'PFC2', 'Facilitator User2', 'Request', 
		'Assessment', 'engelbert.go@pointwest.com.ph', 'SUNLIFEHO', 
         'UWSVCS', 'FACILITN', 'IUMDEV');         

/* Underwriter */
INSERT INTO users (user_id, usr_acf2id, usr_last_name, usr_middle_name, 
                   usr_first_name, usr_email_address, slo_id, dept_id, 
                   sec_id, created_by) 
VALUES ('REQUW2', 'PUW2', 'Underwriter User2', 'Request', 
		'Assessment', 'engelbert.go@pointwest.com.ph', 'SUNLIFEHO', 
         'UWSVCS', 'UWRITING', 'IUMDEV');  
         
/* Underwriter with multiple roles */
INSERT INTO users (user_id, usr_acf2id, usr_last_name, usr_middle_name, 
                   usr_first_name, usr_email_address, slo_id, dept_id, 
                   sec_id, created_by) 
VALUES ('REQUW*2', 'PUW*2', 'Underwriter User*2', 'Request', 
		'Assessment', 'engelbert.go@pointwest.com.ph', 'SUNLIFEHO', 
         'UWSVCS', 'UWRITING', 'IUMDEV');  

/* Underwriter Supervison Manager */
INSERT INTO users (user_id, usr_acf2id, usr_last_name, usr_middle_name, 
                   usr_first_name, usr_email_address, slo_id, dept_id, 
                   sec_id, created_by) 
VALUES ('REQUWSM2', 'PUWSM2', 'Underwriter SupMgr User2', 'Request', 
		'Assessment', 'engelbert.go@pointwest.com.ph', 'SUNLIFEHO', 
         'UWSVCS', 'UWRITING', 'IUMDEV');  

/* Underwriter Supervison Manager with multiple roles */
INSERT INTO users (user_id, usr_acf2id, usr_last_name, usr_middle_name, 
                   usr_first_name, usr_email_address, slo_id, dept_id, 
                   sec_id, created_by) 
VALUES ('REQUWS*2', 'PUWS*2', 'Underwriter SupMgr User*2', 'Request', 
		'Assessment', 'engelbert.go@pointwest.com.ph', 'SUNLIFEHO', 
         'UWSVCS', 'UWRITING', 'IUMDEV');  

/* Agent         */
INSERT INTO users (user_id, usr_acf2id, usr_last_name, usr_middle_name, 
                   usr_first_name, usr_email_address, slo_id, dept_id, 
                   sec_id, created_by) 
VALUES ('REQAG2', 'PAG2', 'Agent User2', 'Request', 
		'Assessment', 'engelbert.go@pointwest.com.ph', 'SUNLIFEHO', 
         'SALESMKTG', 'AGENTS', 'IUMDEV');  

/*
 * 3rd Set
 */
 
/* group admin */
INSERT INTO users (user_id, usr_acf2id, usr_last_name, usr_middle_name, 
                   usr_first_name, usr_email_address, slo_id, dept_id, 
                   sec_id, created_by) 
VALUES ('REQNBAD3', 'PNBAD3', 'NB Group Admin User3', 'Request', 
		'Assessment', 'cris.marfil@pointwest.com.ph', 'SUNLIFEHO', 
         'QAINFOSEC', 'NBADMIN', 'IUMDEV');

/* nb staff */
INSERT INTO users (user_id, usr_acf2id, usr_last_name, usr_middle_name, 
                   usr_first_name, usr_email_address, slo_id, dept_id, 
                   sec_id, created_by) 
VALUES ('REQNBST3', 'PNBST3', 'NB Staff User3', 'Request', 
		'Assessment', 'cris.marfil@pointwest.com.ph', 'SUNLIFEHO', 
         'CLIENTSVCS', 'NBREVIEW', 'IUMDEV');

/* nb reviewer          */
INSERT INTO users (user_id, usr_acf2id, usr_last_name, usr_middle_name, 
                   usr_first_name, usr_email_address, slo_id, dept_id, 
                   sec_id, created_by) 
VALUES ('REQNBRV3', 'PNBST3', 'NB Reviewer User3', 'Request', 
		'Assessment', 'cris.marfil@pointwest.com.ph', 'SUNLIFEHO', 
         'CLIENTSVCS', 'NBREVIEW', 'IUMDEV');

/* nb supervisor manager */
INSERT INTO users (user_id, usr_acf2id, usr_last_name, usr_middle_name, 
                   usr_first_name, usr_email_address, slo_id, dept_id, 
                   sec_id, created_by) 
VALUES ('REQNBSM3', 'PNBSM3', 'NB SupeMgr User3', 'Request', 
		'Assessment', 'cris.marfil@pointwest.com.ph', 'SUNLIFEHO', 
         'CLIENTSVCS', 'NBADMIN', 'IUMDEV');

/* Facilitator Supervisor Manager         */
INSERT INTO users (user_id, usr_acf2id, usr_last_name, usr_middle_name, 
                   usr_first_name, usr_email_address, slo_id, dept_id, 
                   sec_id, created_by) 
VALUES ('REQFCSM3', 'PFCSM3', 'Facilitator SupMgr User3', 'Request', 
		'Assessment', 'cris.marfil@pointwest.com.ph', 'SUNLIFEHO', 
         'UWSVCS', 'FACILITN', 'IUMDEV');

/* Facilitator */
INSERT INTO users (user_id, usr_acf2id, usr_last_name, usr_middle_name, 
                   usr_first_name, usr_email_address, slo_id, dept_id, 
                   sec_id, created_by) 
VALUES ('REQFC3', 'PFC3', 'Facilitator User3', 'Request', 
		'Assessment', 'cris.marfil@pointwest.com.ph', 'SUNLIFEHO', 
         'UWSVCS', 'FACILITN', 'IUMDEV');         

/* Underwriter */
INSERT INTO users (user_id, usr_acf2id, usr_last_name, usr_middle_name, 
                   usr_first_name, usr_email_address, slo_id, dept_id, 
                   sec_id, created_by) 
VALUES ('REQUW3', 'PUW3', 'Underwriter User3', 'Request', 
		'Assessment', 'cris.marfil@pointwest.com.ph', 'SUNLIFEHO', 
         'UWSVCS', 'UWRITING', 'IUMDEV');  
         
/* Underwriter with multiple roles */
INSERT INTO users (user_id, usr_acf2id, usr_last_name, usr_middle_name, 
                   usr_first_name, usr_email_address, slo_id, dept_id, 
                   sec_id, created_by) 
VALUES ('REQUW*3', 'PUW*3', 'Underwriter User*3', 'Request', 
		'Assessment', 'cris.marfil@pointwest.com.ph', 'SUNLIFEHO', 
         'UWSVCS', 'UWRITING', 'IUMDEV');  

/* Underwriter Supervison Manager */
INSERT INTO users (user_id, usr_acf2id, usr_last_name, usr_middle_name, 
                   usr_first_name, usr_email_address, slo_id, dept_id, 
                   sec_id, created_by) 
VALUES ('REQUWSM3', 'PUWSM3', 'Underwriter SupMgr User3', 'Request', 
		'Assessment', 'cris.marfil@pointwest.com.ph', 'SUNLIFEHO', 
         'UWSVCS', 'UWRITING', 'IUMDEV');  

/* Underwriter Supervison Manager with multiple roles */
INSERT INTO users (user_id, usr_acf2id, usr_last_name, usr_middle_name, 
                   usr_first_name, usr_email_address, slo_id, dept_id, 
                   sec_id, created_by) 
VALUES ('REQUWS*3', 'PUWS*3', 'Underwriter SupMgr User*3', 'Request', 
		'Assessment', 'cris.marfil@pointwest.com.ph', 'SUNLIFEHO', 
         'UWSVCS', 'UWRITING', 'IUMDEV');  

/* Agent         */
INSERT INTO users (user_id, usr_acf2id, usr_last_name, usr_middle_name, 
                   usr_first_name, usr_email_address, slo_id, dept_id, 
                   sec_id, created_by) 
VALUES ('REQAG3', 'PAG3', 'Agent User3', 'Request', 
		'Assessment', 'cris.marfil@pointwest.com.ph', 'SUNLIFEHO', 
         'SALESMKTG', 'AGENTS', 'IUMDEV');  
         
/* passwords are set to 'password */
UPDATE users SET usr_password = 'password';
  