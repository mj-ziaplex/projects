/* Activity Logs - for testing TAT Report
  
   Assumes that the following Assessment Requests
   are in the specified state:
	1. WFAR2002 (PN) : For Transmittal
	2. A1 (IL) : Awaiting Medical
	3. A2 (IL) : For Assessment
	4. A3 (IL) : For Transmittal
	5. A4 (IL) : For Assessment
	6. A5 (IL) : For Offer

   Note: Field act_nofitification sent is not populated by this script.
         It is not used in the TAT Report.
*/

/******** INDIVIDUAL LIFE *************/

/*
  ---------------------------------------------------
  Assessment Request A1: Creation to Awaiting Medical
  ---------------------------------------------------
*/

-- Creation
INSERT INTO activity_logs (act_id, uar_reference_num, stat_id, act_datetime, user_id)
        VALUES (seq_activity_logs.NEXTVAL, 'A1', 1, TO_TIMESTAMP('16-JAN-2004 14:31:54', 'DD-MON-YYYY HH24:MI:SS'), 'IUMDEV');


-- Creation to Awaiting Medical
INSERT INTO activity_logs (act_id, uar_reference_num, stat_id, act_datetime, user_id)
        VALUES (seq_activity_logs.NEXTVAL, 'A1', 11, TO_TIMESTAMP('16-JAN-2004 16:54:14', 'DD-MON-YYYY HH24:MI:SS'), 'VTUBI');

/*
  -----------------------------------------------------
  Assessment Request A2: Creation to For Assessment
  -----------------------------------------------------  
*/

-- Creation
INSERT INTO activity_logs (act_id, uar_reference_num, stat_id, act_datetime, user_id)
        VALUES (seq_activity_logs.NEXTVAL, 'A2', 1, TO_TIMESTAMP('16-JAN-2004 08:31:50', 'DD-MON-YYYY HH24:MI:SS'), 'IUMDEV');

-- Creation to For Transmittal
INSERT INTO activity_logs (act_id, uar_reference_num, stat_id, act_datetime, user_id)
        VALUES (seq_activity_logs.NEXTVAL, 'A2', 2, TO_TIMESTAMP('16-JAN-2004 08:35:14', 'DD-MON-YYYY HH24:MI:SS'), 'VTUBI');

-- For Transmittal to For Facilitator Action
INSERT INTO activity_logs (act_id, uar_reference_num, stat_id, act_datetime, user_id)
        VALUES (seq_activity_logs.NEXTVAL, 'A2', 3, TO_TIMESTAMP('16-JAN-2004 08:51:00', 'DD-MON-YYYY HH24:MI:SS'), 'VTUBI');

-- For Facilitator Action to For Assessment
INSERT INTO activity_logs (act_id, uar_reference_num, stat_id, act_datetime, user_id)
        VALUES (seq_activity_logs.NEXTVAL, 'A2', 4, TO_TIMESTAMP('17-JAN-2004 09:11:54', 'DD-MON-YYYY HH24:MI:SS'), 'VTUBI');

/*
  -----------------------------------------------------
  Assessment Request A3: Creation to For Transmittal
  -----------------------------------------------------
*/

-- Creation
INSERT INTO activity_logs (act_id, uar_reference_num, stat_id, act_datetime, user_id)
        VALUES (seq_activity_logs.NEXTVAL, 'A3', 1, TO_TIMESTAMP('18-JAN-2004 16:31:54', 'DD-MON-YYYY HH24:MI:SS'), 'IUMDEV');

-- Creation to For Transmittal
INSERT INTO activity_logs (act_id, uar_reference_num, stat_id, act_datetime, user_id)
        VALUES (seq_activity_logs.NEXTVAL, 'A3', 2, TO_TIMESTAMP('19-JAN-2004 08:12:45', 'DD-MON-YYYY HH24:MI:SS'), 'VTUBI');

/*
 -----------------------------------------------------
 Assessment Request A4: Creation to For Assessment
 -----------------------------------------------------
*/

-- Creation
INSERT INTO activity_logs (act_id, uar_reference_num, stat_id, act_datetime, user_id)
        VALUES (seq_activity_logs.NEXTVAL, 'A4', 1, TO_TIMESTAMP('16-JAN-2004 10:31:54', 'DD-MON-YYYY HH24:MI:SS'), 'VTUBI');

-- Creation to For Transmittal
INSERT INTO activity_logs (act_id, uar_reference_num, stat_id, act_datetime, user_id)
        VALUES (seq_activity_logs.NEXTVAL, 'A4', 2, TO_TIMESTAMP('16-JAN-2004 11:13:34', 'DD-MON-YYYY HH24:MI:SS'), 'VTUBI');

-- For Transmittal to For Facilitator Action
INSERT INTO activity_logs (act_id, uar_reference_num, stat_id, act_datetime, user_id)
        VALUES (seq_activity_logs.NEXTVAL, 'A4', 3, TO_TIMESTAMP('16-JAN-2004 13:35:14', 'DD-MON-YYYY HH24:MI:SS'), 'VTUBI');

-- For Facilitator Action to For Assessment
INSERT INTO activity_logs (act_id, uar_reference_num, stat_id, act_datetime, user_id)
        VALUES (seq_activity_logs.NEXTVAL, 'A4', 4, TO_TIMESTAMP('16-JAN-2004 13:51:54', 'DD-MON-YYYY HH24:MI:SS'), 'VTUBI');

/*
 -----------------------------------------------------
 Assessment Request A5: Creation to For Offer
 -----------------------------------------------------
*/

-- Creation
INSERT INTO activity_logs (act_id, uar_reference_num, stat_id, act_datetime, user_id)
        VALUES (seq_activity_logs.NEXTVAL, 'A5', 1, TO_TIMESTAMP('16-JAN-2004 08:31:24', 'DD-MON-YYYY HH24:MI:SS'), 'IUMDEV');

-- Creation to For Transmittal
INSERT INTO activity_logs (act_id, uar_reference_num, stat_id, act_datetime, user_id)
        VALUES (seq_activity_logs.NEXTVAL, 'A5', 2, TO_TIMESTAMP('16-JAN-2004 09:01:54', 'DD-MON-YYYY HH24:MI:SS'), 'VTUBI');

-- For Transmittal to For Facilitator Action
INSERT INTO activity_logs (act_id, uar_reference_num, stat_id, act_datetime, user_id)
        VALUES (seq_activity_logs.NEXTVAL, 'A5', 3, TO_TIMESTAMP('16-JAN-2004 09:31:34', 'DD-MON-YYYY HH24:MI:SS'), 'VTUBI');

-- For Facilitator Action to For Assessment
INSERT INTO activity_logs (act_id, uar_reference_num, stat_id, act_datetime, user_id)
        VALUES (seq_activity_logs.NEXTVAL, 'A5', 4, TO_TIMESTAMP('16-JAN-2004 10:51:24', 'DD-MON-YYYY HH24:MI:SS'), 'VTUBI');

-- For Assessment to Undergoing Assessment
INSERT INTO activity_logs (act_id, uar_reference_num, stat_id, act_datetime, user_id)
        VALUES (seq_activity_logs.NEXTVAL, 'A5', 5, TO_TIMESTAMP('16-JAN-2004 11:11:24', 'DD-MON-YYYY HH24:MI:SS'), 'VTUBI');

-- Undergoing Assessment Request to For Offer
INSERT INTO activity_logs (act_id, uar_reference_num, stat_id, act_datetime, user_id)
        VALUES (seq_activity_logs.NEXTVAL, 'A5', 9, TO_TIMESTAMP('16-JAN-2004 13:41:44', 'DD-MON-YYYY HH24:MI:SS'), 'VTUBI');

/***********  PRE NEED ****************/

/*
 -----------------------------------------------------
  Assessment Request: WFAR0002: For Transmittal
 -----------------------------------------------------
*/

-- For Transmittal
INSERT INTO activity_logs (act_id, uar_reference_num, stat_id, act_datetime, user_id)
        VALUES (seq_activity_logs.NEXTVAL, 'WFAR0002', 2, TO_TIMESTAMP('16-JAN-2004 12:31:54', 'DD-MON-YYYY HH24:MI:SS'),  'VTUBI');