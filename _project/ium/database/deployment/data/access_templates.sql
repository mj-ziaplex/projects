--for Group New Business Admn 

INSERT INTO access_templates (templt_id, role_id,templt_desc)
VALUES ('NBADM','GRPNBADMIN','Template for GRPNBADMIN');

--for New Bus Staff 

INSERT INTO access_templates (templt_id, role_id,templt_desc)
VALUES ('NBSTF','NBSTAFF','Template for NBSTAFF');

--for Nb Rev 

INSERT INTO access_templates (templt_id, role_id,templt_desc)
VALUES ('NBREV','NBREVIEWER','Template for NBREVIEWER');

-- for NBSUPMGR 

INSERT INTO access_templates (templt_id, role_id,templt_desc)
VALUES ('NBSUP','NBSUPMGR','Templ for NBSUPMGR');

-- for FACILITSUPMGR 

INSERT INTO access_templates (templt_id, role_id,templt_desc)
VALUES ('FCSUP','FACILITSUPMGR','Templ for FACILITSUPMGR');

-- for FACILITATOR 

INSERT INTO access_templates (templt_id, role_id,templt_desc)
VALUES ('FC','FACILITATOR','Templ for FACILITATOR');

-- for UNDERWRITER 

INSERT INTO access_templates (templt_id, role_id,templt_desc)
VALUES ('UW','UNDERWRITER','Templ for UNDERWRITER');

-- for UWTNGSUPMGR 

INSERT INTO access_templates (templt_id, role_id,templt_desc)
VALUES ('UWSUP','UWTNGSUPMGR','Templ for UWTNGSUPMGR');

-- for MEDICALADMIN 

INSERT INTO access_templates (templt_id, role_id,templt_desc)
VALUES ('MDADM','MEDICALADMIN','Templ for MEDICALADMIN');

-- for MEDSUPMGR 

INSERT INTO access_templates (templt_id, role_id,templt_desc)
VALUES ('MDSUP','MEDSUPMGR','Templ for MEDSUPMGR');

-- for MEDCONSLT 

INSERT INTO access_templates (templt_id, role_id,templt_desc)
VALUES ('MDCST','MEDCONSLT','Templ for MEDCONSLT');

-- for MKTGSTAFF 

INSERT INTO access_templates (templt_id, role_id,templt_desc)
VALUES ('MKSTF','MKTGSTAFF','Templ for MKTGSTAFF');

-- for AGENT 

INSERT INTO access_templates (templt_id, role_id,templt_desc)
VALUES ('AGENT','AGENT','Templ for AGENT');

-- for EXAMINER 

INSERT INTO access_templates (templt_id, role_id,templt_desc)
VALUES ('EX','EXAMINER','Templ for EXAMINER');

-- for LABSTAFF 

INSERT INTO access_templates (templt_id, role_id,templt_desc)
VALUES ('LBSTF','LABSTAFF','Templ for LABSTAFF');

-- for OTHERUSER 

INSERT INTO access_templates (templt_id, role_id,templt_desc)
VALUES ('OUSER','OTHERUSER','Templ for OTHERUSER');

-- for LOVADMIN 

INSERT INTO access_templates (templt_id, role_id,templt_desc)
VALUES ('LVADM','LOVADMIN','Templ for LOVADMIN');

-- for SCTYADMIN 

INSERT INTO access_templates (templt_id, role_id,templt_desc)
VALUES ('SCADM','SCTYADMIN','Templ for SCTYADMIN');

-- for SYSOPER 

INSERT INTO access_templates (templt_id, role_id,templt_desc)
VALUES ('SOPER','SYSOPER','Templ for SYSOPER');

