INSERT INTO requirement_forms (rf_id, rf_name, rf_template_name)
	VALUES (seq_requirement_form.NEXTVAL, 'Acceptance of Offer','Acceptance of Offer');
	
INSERT INTO requirement_forms (rf_id, rf_name, rf_template_name)
	VALUES (seq_requirement_form.NEXTVAL, 'Acceptance of Offer Form with Exclusion Provision',NULL);
	
INSERT INTO requirement_forms (rf_id, rf_name, rf_template_name)
	VALUES (seq_requirement_form.NEXTVAL, 'Additional Requirements for the Applicant Under the Anti-Money Laundering Act','Anti-Money Laundering Act');
   
INSERT INTO requirement_forms (rf_id, rf_name, rf_template_name)
	VALUES (seq_requirement_form.NEXTVAL, 'Agent''s Confidential Report','Agent''s Confidential Report');
	
INSERT INTO requirement_forms (rf_id, rf_name, rf_template_name)
	VALUES (seq_requirement_form.NEXTVAL, 'Alteration of Application','Alteration of Application');

INSERT INTO requirement_forms (rf_id, rf_name, rf_template_name)
	VALUES (seq_requirement_form.NEXTVAL, 'Arthritis Questionnaire','Arthritis Questionnaire');
	
INSERT INTO requirement_forms (rf_id, rf_name, rf_template_name)
	VALUES (seq_requirement_form.NEXTVAL, 'Attending Physician''s Statement','Attending Physician''s Statement');

INSERT INTO requirement_forms (rf_id, rf_name, rf_template_name)
	VALUES (seq_requirement_form.NEXTVAL, 'Automobile and Motorcycle Racing Questionnaire','Automobile and Motorcycle Racing Questionnaire');

INSERT INTO requirement_forms (rf_id, rf_name, rf_template_name)
	VALUES (seq_requirement_form.NEXTVAL, 'Aviation Questionnaire','Aviation Questionnaire');

INSERT INTO requirement_forms (rf_id, rf_name, rf_template_name)
	VALUES (seq_requirement_form.NEXTVAL, 'Business Financial Questionnaire','Financial Questionnaire - Business');

INSERT INTO requirement_forms (rf_id, rf_name, rf_template_name)
	VALUES (seq_requirement_form.NEXTVAL, 'Certificate of Insurability (age 16 & over)',null);

INSERT INTO requirement_forms (rf_id, rf_name, rf_template_name)
	VALUES (seq_requirement_form.NEXTVAL, 'Certificate of Insurability (child under age 16)',null);

INSERT INTO requirement_forms (rf_id, rf_name, rf_template_name)
	VALUES (seq_requirement_form.NEXTVAL, 'Certification re: New Signature','Certification re New Signature');

INSERT INTO requirement_forms (rf_id, rf_name, rf_template_name)
	VALUES (seq_requirement_form.NEXTVAL, 'Cession Form',null);

INSERT INTO requirement_forms (rf_id, rf_name, rf_template_name)
	VALUES (seq_requirement_form.NEXTVAL, 'Diving Questionnaire','Diving Questionnaire');

INSERT INTO requirement_forms (rf_id, rf_name, rf_template_name)
	VALUES (seq_requirement_form.NEXTVAL, 'Drug Usage Questionnaire','Drug Usage Questionnaire');

INSERT INTO requirement_forms (rf_id, rf_name, rf_template_name)
	VALUES (seq_requirement_form.NEXTVAL, 'Laboratory Exam LOA Form', 'Laboratory Exam LOA');

INSERT INTO requirement_forms (rf_id, rf_name, rf_template_name)
	VALUES (seq_requirement_form.NEXTVAL, 'Medical Appointment Form',null);

INSERT INTO requirement_forms (rf_id, rf_name, rf_template_name)
	VALUES (seq_requirement_form.NEXTVAL, 'Medical Exam LOA Form','Medical Exam LOA');

INSERT INTO requirement_forms (rf_id, rf_name, rf_template_name)
	VALUES (seq_requirement_form.NEXTVAL, 'Mountain Climbing Questionnaire','Mountain Climbing Questionnaire');

INSERT INTO requirement_forms (rf_id, rf_name, rf_template_name)
	VALUES (seq_requirement_form.NEXTVAL, 'Parachuting/Sky Diving Questionnaire','Parachuting and Sky Diving Questionnaire',null);

INSERT INTO requirement_forms (rf_id, rf_name, rf_template_name)
	VALUES (seq_requirement_form.NEXTVAL, 'Part II of Application for insurance on Child age 15 or under',null);

INSERT INTO requirement_forms (rf_id, rf_name, rf_template_name)
	VALUES (seq_requirement_form.NEXTVAL, 'Part II of Application for Insurance on Adult',null);

INSERT INTO requirement_forms (rf_id, rf_name, rf_template_name)
	VALUES (seq_requirement_form.NEXTVAL, 'Personal Financial Questionnaire','Financial Questionnaire - Personal');

INSERT INTO requirement_forms (rf_id, rf_name, rf_template_name)
	VALUES (seq_requirement_form.NEXTVAL, 'PN Non-Med Questionnaire',null);

INSERT INTO requirement_forms (rf_id, rf_name, rf_template_name)
	VALUES (seq_requirement_form.NEXTVAL, 'Residential Background Questionnaire','Residential Background Questionnaire');

INSERT INTO requirement_forms (rf_id, rf_name, rf_template_name)
	VALUES (seq_requirement_form.NEXTVAL, 'Secretary''s Certificate',null);

INSERT INTO requirement_forms (rf_id, rf_name, rf_template_name)
	VALUES (seq_requirement_form.NEXTVAL, 'Special Power of Attorney',null);

INSERT INTO requirement_forms (rf_id, rf_name, rf_template_name)
	VALUES (seq_requirement_form.NEXTVAL, 'Supplementary Questions concerning Asthma, Bronchitis or other Pulmonary Symptoms','Supplementary Questions re Asthma, Bronchitis');

INSERT INTO requirement_forms (rf_id, rf_name, rf_template_name)
	VALUES (seq_requirement_form.NEXTVAL, 'Supplementary Questions concerning Chest Pain or Discomfort','Supplementary Questions re Chest Pain or Discomfort');

INSERT INTO requirement_forms (rf_id, rf_name, rf_template_name)
	VALUES (seq_requirement_form.NEXTVAL, 'Supplementary Questions concerning Diabetes (or suspected Diabetes)','Supplementary Questions re Diabetes');

INSERT INTO requirement_forms (rf_id, rf_name, rf_template_name)
	VALUES (seq_requirement_form.NEXTVAL, 'Supplementary Questions concerning Digestive or Bowel Disorder','Supplementary Questions re Digestive or Bowel Disorder');

INSERT INTO requirement_forms (rf_id, rf_name, rf_template_name)
	VALUES (seq_requirement_form.NEXTVAL, 'Supplementary Questions concerning Fainting or Episodes of Loss of Consciousness','Supplementary Questions re Fainting');

INSERT INTO requirement_forms (rf_id, rf_name, rf_template_name)
	VALUES (seq_requirement_form.NEXTVAL, 'Supplementary Questions Concerning use of Alcohol','Supplementary Questions re Use of Alcohol');

INSERT INTO requirement_forms (rf_id, rf_name, rf_template_name)
	VALUES (seq_requirement_form.NEXTVAL, 'Supplementary Questions for Female/Maternity Benefit', 'Supplementary Questions for Female Maternity Benefit');

INSERT INTO requirement_forms (rf_id, rf_name, rf_template_name)
	VALUES (seq_requirement_form.NEXTVAL, 'Supplementary Questions re Growths, Cysts, Lumps and Tumors', 'Supplementary Questions re Growths, Cysts, Lumps and Tumors');

INSERT INTO requirement_forms (rf_id, rf_name, rf_template_name)
	VALUES (seq_requirement_form.NEXTVAL, 'Supplementary Questions re Gynecological Complaints','Supplementary Questions re Gynecological Complaints');

INSERT INTO requirement_forms (rf_id, rf_name, rf_template_name)
	VALUES (seq_requirement_form.NEXTVAL, 'Waiver of Benefit APDB (form for advance payment disability benefit)', 'Waiver for APDB');

INSERT INTO requirement_forms (rf_id, rf_name, rf_template_name)
	VALUES (seq_requirement_form.NEXTVAL, 'Waiver of Benefit CCR (form for critical condition rider)', 'Waiver for CCR');

INSERT INTO requirement_forms (rf_id, rf_name, rf_template_name)
	VALUES (seq_requirement_form.NEXTVAL, 'Waiver of Benefit F&MB (form for female & maternity benefit)', 'Waiver for F&M Benefit');

INSERT INTO requirement_forms (rf_id, rf_name, rf_template_name)
	VALUES (seq_requirement_form.NEXTVAL, 'Waiver of Benefit HIB (form for hospital income benefit)','Waiver for HIB');
	
INSERT INTO requirement_forms (rf_id, rf_name, rf_template_name)
	VALUES (seq_requirement_form.NEXTVAL, 'Parachuting / Sky Diving Questionnaire', 'Parachuting and Sky Diving Questionnaire');	
