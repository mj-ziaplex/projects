INSERT INTO departments (dept_code, dept_desc)
	VALUES ('CLIENTSVCS', 'CLIENT SERVICES DEPARTMENT');

INSERT INTO departments (dept_code, dept_desc)
	VALUES ('UWSVCS', 'UNDERWRITING SERVICES DEPARTMENT');
	
INSERT INTO departments (dept_code, dept_desc)
	VALUES ('MEDICAL', 'MEDICAL DEPARTMENT');
	
INSERT INTO departments (dept_code, dept_desc)
	VALUES ('GROUPINS', 'GROUP INSURANCE DEPARTMENT');
	
INSERT INTO departments (dept_code, dept_desc)
	VALUES ('SALESMKTG', 'SALES AND MARKETING DEPARTMENT');

INSERT INTO departments (dept_code, dept_desc)
	VALUES ('MGMTSVCS', 'MANAGEMENT SERVICES DEPARTMENT');
	
INSERT INTO departments (dept_code, dept_desc)
	VALUES ('OTHERDEPT', 'OTHER DEPARTMENTS');
	
INSERT INTO departments (dept_code, dept_desc)
	VALUES ('SYSTEMS', 'CENTRAL SYSTEMS');