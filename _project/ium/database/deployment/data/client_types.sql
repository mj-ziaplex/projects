INSERT INTO client_types (clt_code, clt_desc)
	VALUES ('O', 'Owner');

INSERT INTO client_types (clt_code, clt_desc)
	VALUES ('I', 'Insured');
	
INSERT INTO client_types (clt_code, clt_desc)
	VALUES ('P', 'Planholder');
	
INSERT INTO client_types (clt_code, clt_desc)
	VALUES ('M', 'Member');