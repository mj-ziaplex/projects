INSERT INTO sections (sec_code, sec_desc) 
	VALUES ('NBREVIEW', 'NEW BUSINESS REVIEW GROUP');

INSERT INTO sections (sec_code, sec_desc) 
	VALUES ('UWRITING', 'UNDERWRITING');
	
INSERT INTO sections (sec_code, sec_desc) 
	VALUES ('FACILITN', 'UNDERWRITING FACILITATION');
	
INSERT INTO sections (sec_code, sec_desc) 
	VALUES ('NBADMIN', 'NEW BUSINESS ADMIN');
	
INSERT INTO sections (sec_code, sec_desc) 
	VALUES ('POLICYCHG', 'POLICY CHANGE');
	
INSERT INTO sections (sec_code, sec_desc) 
	VALUES ('MEDCONSLT', 'MEDICAL CONSULTATION');
	
INSERT INTO sections (sec_code, sec_desc) 
	VALUES ('MEDADMIN', 'MEDICAL ADMINISTRATION');
	
INSERT INTO sections (sec_code, sec_desc) 
	VALUES ('MEDPROVIDER', 'ACCREDITED MEDICAL PROVIDERS');
	
INSERT INTO sections (sec_code, sec_desc) 
	VALUES ('GROUPADM', 'GROUP ADMINISTRATION');
	
INSERT INTO sections (sec_code, sec_desc) 
	VALUES ('AGENTS', 'AGENTS'' GROUP');
	
INSERT INTO sections (sec_code, sec_desc) 
	VALUES ('SALESADMIN', 'SALES ADMINISTRATION');
	
INSERT INTO sections (sec_code, sec_desc) 
	VALUES ('PMGMTOFF', 'PROJECT MANAGEMENT OFFICE');
	
INSERT INTO sections (sec_code, sec_desc) 
	VALUES ('QASSURANCE', 'QUALITY ASSURANCE');
	
INSERT INTO sections (sec_code, sec_desc) 
	VALUES ('OTHERSECT', 'OTHER DEPARTMENT SECTIONS');
	
INSERT INTO sections (sec_code, sec_desc) 
	VALUES ('QAINFOSEC', 'QUALITY ASSURANCE AND INFO SECURITY');

INSERT INTO sections (sec_code, sec_desc) 
	VALUES ('DATACTR', 'DATA CENTER');
