INSERT INTO sunlife_offices (slo_office_code, slo_office_name, slo_type
							, slo_address_line1, slo_address_line2
							, slo_address_line3, slo_city, slo_country
							, slo_zip_code) 
	VALUES ('SUNLIFEHO', 'SUN LIFE HEAD OFFICE', 'M', 
			'16/F THE ENTERPRISE CENTRE TOWER 2', 
			'6766 AYALA AVE.', '', 'MAKATI', 'PHILIPPINES', '1200');

INSERT INTO sunlife_offices (slo_office_code, slo_office_name, slo_type
							, slo_address_line1, slo_address_line2
							, slo_address_line3, slo_city, slo_country
							, slo_zip_code) 
	VALUES ('MAINCUST', 'MAIN CUSTOMER CENTER', 'M', 
			'111 PASEO DE ROXAS BLDG.', 'PASEO DE ROXAS AVE.', '', 
			'MAKATI', 'PHILIPPINES', '1200');

INSERT INTO sunlife_offices (slo_office_code, slo_office_name, slo_type
							, slo_address_line1, slo_address_line2
							, slo_address_line3, slo_city, slo_country
							, slo_zip_code) 
	VALUES ('ACECUST', 'ACE CUSTOMER CENTER', 'S', 
			'7/F ACE BUILDING', '101-103 RADA ST.', 'LEGASPI VILLAGE', 
			'MAKATI', 'PHILIPPINES', '1200');

INSERT INTO sunlife_offices (slo_office_code, slo_office_name, slo_type
							, slo_address_line1, slo_address_line2
							, slo_address_line3, slo_city, slo_country
							, slo_zip_code) 
	VALUES ('ACACIABUT', 'ACACIA NBO - BUTUAN EXTENSION OFFICE', 'B', 
			'2ND FLOOR D&V PLAZA HOLDINGS J.C.', 'AQUINO AVENUE', '', 
			'BUTUAN', 'PHILIPPINES', '8600');

INSERT INTO sunlife_offices (slo_office_code, slo_office_name, slo_type
							, slo_address_line1, slo_address_line2
							, slo_address_line3, slo_city, slo_country
							, slo_zip_code) 
	VALUES ('ACACIACDO', 'ACACIA NBO - CAGAYAN DE ORO SALES OFFICE', 'B', 
			'2/F PHILIPPINE FIRST INSURANCE BLDG.', 'PRESIDENT AGUINALDO', 
			'COR. BORJA STS.', 'CAGAYAN DE ORO', 'PHILIPPINES', '9000');

INSERT INTO sunlife_offices (slo_office_code, slo_office_name, slo_type
							, slo_address_line1, slo_address_line2
							, slo_address_line3, slo_city, slo_country
							, slo_zip_code) 
	VALUES ('ACACIACEBU', 'ACACIA NBO - CEBU', 'B', 
			'3/F GLOBE ISLA PLAZA', 'PANAY RD. COR. SAMAR LOOP', 
			'CEBU BUSINESS PARK', 'CEBU', 'PHILIPPINES', '');

INSERT INTO sunlife_offices (slo_office_code, slo_office_name, slo_type
							, slo_address_line1, slo_address_line2
							, slo_address_line3, slo_city, slo_country
							, slo_zip_code) 
	VALUES ('ALABANGCUST', 'ALABANG CUSTOMER CENTER', 'S', 
			'G/F UNIT 102-103 ALPAP II BLDG.', 'INVESTMENT DRIVE COR. TRADE STS.', 
			'MADRIGAL BUSINESS PARK, ALABANG', 'MUNTINLUPA', 'PHILIPPINES', '');

INSERT INTO sunlife_offices (slo_office_code, slo_office_name, slo_type
							, slo_address_line1, slo_address_line2
							, slo_address_line3, slo_city, slo_country
							, slo_zip_code) 
	VALUES ('ASPENPAS', 'ASPEN NEW BUSINESS OFFICE', 'B', 
			'30/F ORIENT SQUARE BLDG.', 'EMERALD AVE.', 
			'ORTIGAS CENTER', 'PASIG', 'PHILIPPINES', '1600');

INSERT INTO sunlife_offices (slo_office_code, slo_office_name, slo_type
							, slo_address_line1, slo_address_line2
							, slo_address_line3, slo_city, slo_country
							, slo_zip_code) 
	VALUES ('BANYANBAC', 'BANYAN NBO - BACOLOD', 'B', 
			'10TH COR. LACSON ST.', '', '', 'BACOLOD', 
			'PHILIPPINES', '');

INSERT INTO sunlife_offices (slo_office_code, slo_office_name, slo_type
							, slo_address_line1, slo_address_line2
							, slo_address_line3, slo_city, slo_country
							, slo_zip_code) 
	VALUES ('BAYWOODLAG', 'BAYWOOD NBO - LAGUNA NEW BUSINESS OFFICE', 'B', 
			'2/F & 3/F ROYAL STAR MARKETING INC.', '(RSM) BLDG.', 
			'CROSSING, CALAMBA', 'LAGUNA', 'PHILIPPINES', '');

INSERT INTO sunlife_offices (slo_office_code, slo_office_name, slo_type
							, slo_address_line1, slo_address_line2
							, slo_address_line3, slo_city, slo_country
							, slo_zip_code) 
	VALUES ('BINONDOCUST', 'BINONDO CUSTOMER CENTER', 'S', 
			'G/F DASMARINAS', 'COR. QUINTIN PAREDES STS.', 'BINONDO', 
			'MANILA', 'PHILIPPINES', '');

INSERT INTO sunlife_offices (slo_office_code, slo_office_name, slo_type
							, slo_address_line1, slo_address_line2
							, slo_address_line3, slo_city, slo_country
							, slo_zip_code) 
	VALUES ('BIRCHMNLA', 'BIRCH NEW BUSINESS OFFICE', 'B', 
			'2/F SUN LIFE CENTER-BINONDO', 'DASMARINAS COR. QUINTIN PAREDES STS.', 
			'BINONDO', 'MANILA', 'PHILIPPINES', '');

INSERT INTO sunlife_offices (slo_office_code, slo_office_name, slo_type
							, slo_address_line1, slo_address_line2
							, slo_address_line3, slo_city, slo_country
							, slo_zip_code) 
	VALUES ('BRISTLEILO', 'BRISTLECONE NBO - ILOILO', 'B', 
			'2/F 168 PLATINUM SQUARE BUILDING', 'GEN. LUNA ST.', '', 
			'ILOILO', 'PHILIPPINES', '');

INSERT INTO sunlife_offices (slo_office_code, slo_office_name, slo_type
							, slo_address_line1, slo_address_line2
							, slo_address_line3, slo_city, slo_country
							, slo_zip_code) 
	VALUES ('CEBUCUST', 'CEBU CUSTOMER CENTER', 'S', 
			'GROUND FLOOR GLOBE ISLA PLAZA BLDG', 
			'PANAY RD. COR. SAMAR LOOP', 'CEBU BUSINESS PARK', 
			'CEBU', 'PHILIPPINES', '');

INSERT INTO sunlife_offices (slo_office_code, slo_office_name, slo_type
							, slo_address_line1, slo_address_line2
							, slo_address_line3, slo_city, slo_country
							, slo_zip_code) 
	VALUES ('CEDARMKTI', 'CEDAR NEW BUSINESS OFFICE', 'B', 
			'3/F ALL SEASONS BLDG.', '112 AGUIRRE ST.', 
			'LEGASPI VILLAGE', 'MAKATI', 'PHILIPPINES', '1200');

INSERT INTO sunlife_offices (slo_office_code, slo_office_name, slo_type
							, slo_address_line1, slo_address_line2
							, slo_address_line3, slo_city, slo_country
							, slo_zip_code) 
	VALUES ('CHESTNUTMKTI', 'CHESTNUT NEW BUSINESS OFFICE', 'B', 
			'5/F ALL SEASONS BLDG.', '112 AGUIRRE ST.', 'LEGASPI VILLAGE', 
			'MAKATI', 'PHILIPPINES', '1200');

INSERT INTO sunlife_offices (slo_office_code, slo_office_name, slo_type
							, slo_address_line1, slo_address_line2
							, slo_address_line3, slo_city, slo_country
							, slo_zip_code) 
	VALUES ('CYPRESSMKTI', 'CYPRESS NEW BUSINESS OFFICE', 'B', 
			'PENTHOUSE 108', 'HERRERA ST. FELIZA BLDG.', 
			'LEGASPI VILLAGE', 'MAKATI', 'PHILIPPINES', '1200');

INSERT INTO sunlife_offices (slo_office_code, slo_office_name, slo_type
							, slo_address_line1, slo_address_line2
							, slo_address_line3, slo_city, slo_country
							, slo_zip_code) 
	VALUES ('DAGUPANSO', 'DAGUPAN SALES OFFICE', 'B', 
			'3/F DON BENITO BLDG.', 'MAYOMBO DISTRICT', '', 
			'DAGUPAN', 'PHILIPPINES', '');

INSERT INTO sunlife_offices (slo_office_code, slo_office_name, slo_type
							, slo_address_line1, slo_address_line2
							, slo_address_line3, slo_city, slo_country
							, slo_zip_code) 
	VALUES ('DAVAOCUST', 'DAVAO CUSTOMER CENTER', 'S', 
			'GROUND FLOOR PLAZA DE LUISA BLDG.', 'RAMON MAGSAYSAY AVE.', '', 
			'DAVAO', 'PHILIPPINES', '');

INSERT INTO sunlife_offices (slo_office_code, slo_office_name, slo_type
							, slo_address_line1, slo_address_line2
							, slo_address_line3, slo_city, slo_country
							, slo_zip_code) 
	VALUES ('DURIANGSAN', 'DURIAN - GEN. SANTOS SALES OFFICE', 'B', 
			'3RD FLR PERFECT IMAGE BLDG.', 'ROXAS EAST AVE.', 
			'COR. OSMENA ST.', 'GENERAL SANTOS', 'PHILIPPINES', '9500');

INSERT INTO sunlife_offices (slo_office_code, slo_office_name, slo_type
							, slo_address_line1, slo_address_line2
							, slo_address_line3, slo_city, slo_country
							, slo_zip_code) 
	VALUES ('DURIANDAV', 'DURIAN NEW BUSINESS OFFICE DAVAO', 'B', 
			'2/F PLAZA DE LUISA COMMERCIAL COMPLEX', 
			'RAMON MAGSAYSAY AVE.', '', 'DAVAO', 'PHILIPPINES', '');

INSERT INTO sunlife_offices (slo_office_code, slo_office_name, slo_type
							, slo_address_line1, slo_address_line2
							, slo_address_line3, slo_city, slo_country
							, slo_zip_code) 
	VALUES ('EUCALYPTUSMKTI', 'EUCALYPTUS NEW BUSINESS OFFICE', 'B', 
			'3/F ALL SEASONS BLDG.', '112 AGUIRRE ST.', 'LEGASPI VILLAGE', 
			'MAKATI', 'PHILIPPINES', '1200');
			
INSERT INTO sunlife_offices (slo_office_code, slo_office_name, slo_type
							, slo_address_line1, slo_address_line2
							, slo_address_line3, slo_city, slo_country
							, slo_zip_code) 
	VALUES ('FELIZACUST', 'FELIZA CUSTOMER CENTER', 'S', 
			'11/F FELIZA BLDG.', '108 HERRERA ST.', 'LEGASPI VILLAGE', 
			'MAKATI', 'PHILIPPINES', '1200');

INSERT INTO sunlife_offices (slo_office_code, slo_office_name, slo_type
							, slo_address_line1, slo_address_line2
							, slo_address_line3, slo_city, slo_country
							, slo_zip_code) 
	VALUES ('HOMEOFFDIR', 'HOME OFFICE DIRECT', 'S', 
			'5/F ACE BUILDING', 'RADA COR. DELA ROSA ST.', 
			'LEGASPI VILLAGE', 'MAKATI', 'PHILIPPINES', '1200');

INSERT INTO sunlife_offices (slo_office_code, slo_office_name, slo_type
							, slo_address_line1, slo_address_line2
							, slo_address_line3, slo_city, slo_country
							, slo_zip_code) 
	VALUES ('ILOILOCUST', 'ILOILO CUSTOMER CENTER', 'S', 'GROUND FLOOR', 
			'PLATINUM SQUARE BLDG.', 'GEN. LUNA ST.', 'ILOILO', 'PHILIPPINES', '');

INSERT INTO sunlife_offices (slo_office_code, slo_office_name, slo_type
							, slo_address_line1, slo_address_line2
							, slo_address_line3, slo_city, slo_country
							, slo_zip_code) 
	VALUES ('JACARANDAPAS', 'JACARANDA NEW BUSINESS OFFICE', 'B', 
			'30/F ORIENT SQUARE BLDG.', 'EMERALD AVE.', 'ORTIGAS CENTER', 
			'PASIG', 'PHILIPPINES', '1600');

INSERT INTO sunlife_offices (slo_office_code, slo_office_name, slo_type
							, slo_address_line1, slo_address_line2
							, slo_address_line3, slo_city, slo_country
							, slo_zip_code) 
	VALUES ('LEGASPISO', 'LEGASPI SALES OFFICE', 'B', 
			'3RD FLOOR, LCC EXPRESSMART BLDG.', 'PENARANDA ST.', '', 
			'LEGASPI', 'PHILIPPINES', '');

INSERT INTO sunlife_offices (slo_office_code, slo_office_name, slo_type
							, slo_address_line1, slo_address_line2
							, slo_address_line3, slo_city, slo_country
							, slo_zip_code) 
	VALUES ('LINDENMKTI', 'LINDEN NEW BUSINESS OFFICE', 'B', 
			'6/F PHILCOX BLDG.', '172 SALCEDO ST.', 'LEGASPI VILLAGE', 
			'MAKATI', 'PHILIPPINES', '1200');
			

INSERT INTO sunlife_offices (slo_office_code, slo_office_name, slo_type
							, slo_address_line1, slo_address_line2
							, slo_address_line3, slo_city, slo_country
							, slo_zip_code) 
	VALUES ('MAGNOLIAOLO', 'MAGNOLIA WOODS NBO - OLONGAPO SALES OFFICE', 'B', 
			'2F PALM CREST BUSINESS CENTER', '765 RIZAL AVENUE WEST TANPINAC', 
			'OLONGAPO', 'ZAMBALES', 'PHILIPPINES', '2200');
			
INSERT INTO sunlife_offices (slo_office_code, slo_office_name, slo_type
							, slo_address_line1, slo_address_line2
							, slo_address_line3, slo_city, slo_country
							, slo_zip_code) 
	VALUES ('MAGNOLIAPAM', 'MAGNOLIA WOODS NBO - PAMPANGA NEW BUSINESS OFFICE', 'B', 
			'2/F CHINA BANK BLDG.', 'DOLORES', 
			'SAN FERNANDO', 'PAMPANGA', 'PHILIPPINES', '');
			
INSERT INTO sunlife_offices (slo_office_code, slo_office_name, slo_type
							, slo_address_line1, slo_address_line2
							, slo_address_line3, slo_city, slo_country
							, slo_zip_code) 
	VALUES ('MAHOGANYROX', 'MAHOGANY NBO - ROXAS CITY EXTENSION OFFICE', 'B', 
			'MCKINLEY CORNER', 'SAN JOSE STS.', '', 
			'ROXAS', 'PHILIPPINES', '');
			
INSERT INTO sunlife_offices (slo_office_code, slo_office_name, slo_type
							, slo_address_line1, slo_address_line2
							, slo_address_line3, slo_city, slo_country
							, slo_zip_code) 
	VALUES ('MAHOGANYKAL', 'MAHOGANY NBO - AKLAN', 'B', 
			'3/F ALG BLDG.', 'XIX MARTYRS ST.', 'KALIBO', 
			'AKLAN', 'PHILIPPINES', '');
			
INSERT INTO sunlife_offices (slo_office_code, slo_office_name, slo_type
							, slo_address_line1, slo_address_line2
							, slo_address_line3, slo_city, slo_country
							, slo_zip_code) 
	VALUES ('MAPLEMNLA', 'MAPLE NEW BUSINESS OFFICE', 'B', 
			'3/F SUN LIFE CENTER - BINONDO', 'DASMARINAS COR. QUINTIN PAREDES STS.', 
			'BINONDO', 'MANILA', 'PHILIPPINES', '');
			
INSERT INTO sunlife_offices (slo_office_code, slo_office_name, slo_type
							, slo_address_line1, slo_address_line2
							, slo_address_line3, slo_city, slo_country
							, slo_zip_code) 
	VALUES ('MARKETDEVDAG', 'MARKET DEVELOPMENT - DAGUPAN SALES OFFICE', 'B', 
			'3/F DON BENITO BLDG.', 'MAYOMBO DISTRICT', '', 
			'DAGUPAN', 'PHILIPPINES', '');
			
INSERT INTO sunlife_offices (slo_office_code, slo_office_name, slo_type
							, slo_address_line1, slo_address_line2
							, slo_address_line3, slo_city, slo_country
							, slo_zip_code) 
	VALUES ('MARKETDEVSAN', 'MARKET DEVELOPMENT - SANTIAGO SALES  OFFICE', 'B', 
			'3/F HERITAGE COMPLEX', 'MAHARLIKA HIGHWAY', 'SANTIAGO CITY', 
			'ISABELA', 'PHILIPPINES', '');
			
INSERT INTO sunlife_offices (slo_office_code, slo_office_name, slo_type
							, slo_address_line1, slo_address_line2
							, slo_address_line3, slo_city, slo_country
							, slo_zip_code) 
	VALUES ('MARKETDEVTAR', 'MARKET DEVELOPMENT - TARLAC UNIT OFFICE', 'B', 
			'2ND FLOOR L&C BLDG', 'MACARTHUR HIGHWAY', 'BARRIO SAN ROQUE', 
			'TARLAC', 'PHILIPPINES', '');
			
INSERT INTO sunlife_offices (slo_office_code, slo_office_name, slo_type
							, slo_address_line1, slo_address_line2
							, slo_address_line3, slo_city, slo_country
							, slo_zip_code) 
	VALUES ('MOLAVEMKTI', 'MOLAVE NEW BUSINESS OFFICE', 'B', 
			'7/F ACE BUILDING', '101-103 RADA  COR DELA ROSA STS.', 
			'LEGASPI VILLAGE', 'MAKATI', 'PHILIPPINES', '1200');
			
INSERT INTO sunlife_offices (slo_office_code, slo_office_name, slo_type
							, slo_address_line1, slo_address_line2
							, slo_address_line3, slo_city, slo_country
							, slo_zip_code) 
	VALUES ('MULAWINMKTI', 'MULAWIN NEW BUSINESS OFFICE', 'B', 
			'5/F ACE BUILDING', '101-103 RADA  COR DELA ROSA STS.', 'LEGASPI VILLAGE', 
			'MAKATI', 'PHILIPPINES', '1200');
			
INSERT INTO sunlife_offices (slo_office_code, slo_office_name, slo_type
							, slo_address_line1, slo_address_line2
							, slo_address_line3, slo_city, slo_country
							, slo_zip_code) 
	VALUES ('NAGAEXTOFF', 'NAGA EXTENSION OFFICE', 'B', 
			'4TH FLOOR ROMAR II BLDG.', 'COR. PADIAN & OJEDA STS.', '', 
			'NAGA', 'PHILIPPINES', '4400');
			
INSERT INTO sunlife_offices (slo_office_code, slo_office_name, slo_type
							, slo_address_line1, slo_address_line2
							, slo_address_line3, slo_city, slo_country
							, slo_zip_code) 
	VALUES ('OLIVECAB', 'OLIVE NBO - CABANATUAN NEW BUSINESS OFFICE', 'B', 
			'2/F R&M BUILDING H. CONCEPCION', 'MAHARLIKA HIGHWAY', 
			'CABANATUAN CITY', 'NUEVA ECIJA', 'PHILIPPINES', '3100');
			
INSERT INTO sunlife_offices (slo_office_code, slo_office_name, slo_type
							, slo_address_line1, slo_address_line2
							, slo_address_line3, slo_city, slo_country
							, slo_zip_code) 
	VALUES ('ORTIGASCUST', 'ORTIGAS CUSTOMER CENTER', 'S', 
			'30/F ORIENT SQUARE BLDG.', 'EMERALD AVE.', 
			'ORTIGAS CENTER', 'PASIG', 'PHILIPPINES', '1600');

INSERT INTO sunlife_offices (slo_office_code, slo_office_name, slo_type
							, slo_address_line1, slo_address_line2
							, slo_address_line3, slo_city, slo_country
							, slo_zip_code) 
	VALUES ('OSMANTHUSMNLA', 'OSMANTHUS NEW BUSINESS OFFICE', 'B', 
			'5/F SUN LIFE CENTER - BINONDO', 'DASMARINAS COR. QUINTIN PAREDES STS.', 
			'BINONDO', 'MANILA', 'PHILIPPINES', '');

INSERT INTO sunlife_offices (slo_office_code, slo_office_name, slo_type
							, slo_address_line1, slo_address_line2
							, slo_address_line3, slo_city, slo_country
							, slo_zip_code) 
	VALUES ('PALMMKTI', 'PALM NEW BUSINESS OFFICE', 'B', 
			'5/F ALL SEASONS BLDG.', '112 AGUIRRE ST.', 'LEGASPI VILLAGE', 
			'MAKATI', 'PHILIPPINES', '1200');

INSERT INTO sunlife_offices (slo_office_code, slo_office_name, slo_type
							, slo_address_line1, slo_address_line2
							, slo_address_line3, slo_city, slo_country
							, slo_zip_code) 
	VALUES ('PHILCOXCUST', 'PHILCOX CUSTOMER CENTER', 'S', 
			'6/F PHILCOX BLDG.', '172 SALCEDO ST.', 'LEGASPI VILLAGE', 
			'MAKATI', 'PHILIPPINES', '1200');

INSERT INTO sunlife_offices (slo_office_code, slo_office_name, slo_type
							, slo_address_line1, slo_address_line2
							, slo_address_line3, slo_city, slo_country
							, slo_zip_code) 
	VALUES ('QUEZONCITYCUST', 'QUEZON CITY CUSTOMER CENTER', 'S', 
			'2/F CENTURY IMPERIAL PALACE SUITE', 'TOMAS MORATO', 'COR TIMOG AVENUE', 
			'QUEZON CITY', 'PHILIPPINES', '');

INSERT INTO sunlife_offices (slo_office_code, slo_office_name, slo_type
							, slo_address_line1, slo_address_line2
							, slo_address_line3, slo_city, slo_country
							, slo_zip_code) 
	VALUES ('REDWOODMKTI', 'REDWOOD NEW BUSINESS OFFICE', 'B', 
			'PENTHOUSE FELIZA BLDG.', '108 HERRERA ST.', 'LEGASPI VILLAGE', 
			'MAKATI', 'PHILIPPINES', '1200');

INSERT INTO sunlife_offices (slo_office_code, slo_office_name, slo_type
							, slo_address_line1, slo_address_line2
							, slo_address_line3, slo_city, slo_country
							, slo_zip_code) 
	VALUES ('RICOGENMKTI', 'RICOGEN CUSTOMER CENTER', 'S', 
			'5/F ALL SEASONS BLDG.', '112 AGUIRRE ST.', 'LEGASPI VILLAGE', 
			'MAKATI', 'PHILIPPINES', '1200');

INSERT INTO sunlife_offices (slo_office_code, slo_office_name, slo_type
							, slo_address_line1, slo_address_line2
							, slo_address_line3, slo_city, slo_country
							, slo_zip_code) 
	VALUES ('SANTIAGOSO', 'SANTIAGO SALES OFFICE', 'B', 
			'3/F HERITAGE COMPLEX', 'MAHARLIKA HIGHWAY', 'SANTIAGO CITY', 
			'ISABELA', 'PHILIPPINES', '');

INSERT INTO sunlife_offices (slo_office_code, slo_office_name, slo_type
							, slo_address_line1, slo_address_line2
							, slo_address_line3, slo_city, slo_country
							, slo_zip_code) 
	VALUES ('SEQUOIAMKTI', 'SEQUOIA NEW BUSINESS OFFICE', 'B', 
			'5/F ALL SEASONS BLDG.', '112 AGUIRRE ST.', 'LEGASPI VILLAGE', 
			'MAKATI', 'PHILIPPINES', '1200');

INSERT INTO sunlife_offices (slo_office_code, slo_office_name, slo_type
							, slo_address_line1, slo_address_line2
							, slo_address_line3, slo_city, slo_country
							, slo_zip_code) 
	VALUES ('SLFPICEBU', 'SUN LIFE FINANCIAL PLANS - REGIONAL SO CEBU', 'S', 
			'3/F GLOBE ISLA PLAZA', 'PANAY RD. COR. SAMAR LOOP', 'CEBU BUSINESS PARK', 
			'CEBU', 'PHILIPPINES', '');

INSERT INTO sunlife_offices (slo_office_code, slo_office_name, slo_type
							, slo_address_line1, slo_address_line2
							, slo_address_line3, slo_city, slo_country
							, slo_zip_code) 
	VALUES ('SLFPIDAV', 'SUN LIFE FINANCIAL PLANS - REGIONAL SO DAVAO', 'S', 
			'MEZZANINE FLOOR', 'PLAZA DE LUISA COMMERCIAL COMPLEX', 'R. MAGSAYSAY AVE.', 
			'DAVAO', 'PHILIPPINES', '');

INSERT INTO sunlife_offices (slo_office_code, slo_office_name, slo_type
							, slo_address_line1, slo_address_line2
							, slo_address_line3, slo_city, slo_country
							, slo_zip_code) 
	VALUES ('SLFPIILO', 'SUN LIFE FINANCIAL PLANS - REGIONAL SO ILOILO', 'S', 
			'3F 168 PLATINUM SQUARE BLDG', 'GEN. LUNA ST.', '', 
			'ILOILO', 'PHILIPPINES', '');

INSERT INTO sunlife_offices (slo_office_code, slo_office_name, slo_type
							, slo_address_line1, slo_address_line2
							, slo_address_line3, slo_city, slo_country
							, slo_zip_code) 
	VALUES ('TARLACSO', 'TARLAC SALES OFFICE', 'B', 
			'2ND FLOOR, L&C BLDG', 'ALONG NATIONAL HIGHWAY', 'BARRIO SAN ROQUE', 
			'TARLAC', 'PHILIPPINES', '');

INSERT INTO sunlife_offices (slo_office_code, slo_office_name, slo_type
							, slo_address_line1, slo_address_line2
							, slo_address_line3, slo_city, slo_country
							, slo_zip_code) 
	VALUES ('TINDALOMKTI', 'TINDALO NEW BUSINESS OFFICE', 'B', 
			'5/F ACE BUILDING', '101-103 RADA  COR DELA ROSA STS.', 'LEGASPI VILLAGE', 
			'MAKATI', 'PHILIPPINES', '1200');
