/*
 * DataAccessInterface.java
 *
 * Created on July 14, 2006, 2:54 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package ph.com.sunlife.wms.dto;

/**
 *
 * @author pm13
 */
public interface DataAccessInterface  {
    
    /** Adds an instance of the said object to itself */
    public abstract void add(DataAccessInterface dataAccessInterface);

}
