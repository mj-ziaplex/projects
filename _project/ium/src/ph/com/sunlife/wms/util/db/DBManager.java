/*
 * PersistenceLayer.java
 *
 * Created on July 14, 2006, 2:50 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package ph.com.sunlife.wms.util.db;

import java.lang.reflect.Method;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ph.com.sunlife.wms.dto.DataAccessInterface;
import ph.com.sunlife.wms.dto.DataAccessInterfaceObj;
import com.slocpi.ium.util.CodeHelper;
import com.sun.rowset.CachedRowSetImpl;
/**
 *
 * @author pm13
 * @edited Andre Ceasar Dacanay (July 31, 2008) from 1.2 to 1.3
 * 		ph.com.sunlife.wms.util.db - Optimization
 * 
 */

public class DBManager {

	  private static final Logger LOGGER = LoggerFactory.getLogger(DBManager.class);

    /** Creates a new instance of PersistenceLayer */
    public DBManager() {
    }
    
    public void ExecQuery(String strQuery){
    	
    	LOGGER.info("ExecQuery start");
    	try{
    		
    		WmsDbManager dbmgr = new WmsDbManager();
    		dbmgr.doSave(strQuery);
    	}catch(Exception e){
    		LOGGER.error(CodeHelper.getStackTrace(e));
    	}
    	LOGGER.info("ExecQuery end");
    }
    
   /* public void doExecute(String storedProc, String[] params){
    	
    	LOGGER.info("doExecute start");
    	try{
    		WmsDbManager dbmgr = new WmsDbManager();
    		dbmgr.ExecuteSP(storedProc, params);
    	}catch(Exception e){
    		LOGGER.error(CodeHelper.getStackTrace(e));
    	}
    	LOGGER.info("doExecute end");
    }*/
    
   /* public void doExecuteObj(String storedProc, Object[] params){
    	
    	LOGGER.info("doExecuteObj start");
    	try{
    		WmsDbManager dbmgr = new WmsDbManager();
    		dbmgr.ExecuteSPObj(storedProc, params);
    	}catch(Exception e){
    		LOGGER.error(CodeHelper.getStackTrace(e));
    	}
    	LOGGER.info("doExecuteObj end");
    }*/
    
    public DataAccessInterface doSelect(String[] className, String[] columnNames, String[] dataType, String query, String[] params ){
    	
    	LOGGER.info("doSelect start 1");
    	DataAccessInterface do1 = null;
    	LOGGER.info("doSelect end 1");
    	return do1;
    }    
    
    public DataAccessInterface doSelect(String className, String[] columnNames, String[] dataType, String query){
    	
    	LOGGER.info("doSelect start 2");
    	CachedRowSetImpl resultSet = null;
        int numRetry = 0;
        boolean retry = false; 
        Object obj = null;
        do{
            try{
            	WmsDbManager dbmgr = new WmsDbManager();
            	resultSet = (CachedRowSetImpl)dbmgr.doSelect(query);
            
	        	String[] getters = new String[columnNames.length];
	        	for(int i = 0; i < columnNames.length; i++){
	        		getters[i] = "set"+columnNames[i].substring(0,1).toUpperCase()+columnNames[i].substring(1);
                }
	        	
	        	Class retClazz = Class.forName(className);
              
	        	obj = new Object();
                obj = retClazz.newInstance();
                while (resultSet.next()){
                    
                    Object tmpObj = null;
                    
                    Class clazz = Class.forName(className);
                   
                    tmpObj = clazz.newInstance();
                    for(int i = 0; i < columnNames.length; i++){
                    	Object rsObj = resultSet.getObject(columnNames[i]);
                        Method setMethds = clazz.getMethod(getters[i], new Class[]{Class.forName(dataType[i])});
                        setMethds.invoke(tmpObj, new Object[]{rsObj});
                    
                    }		
                   
                    Method addMethd = retClazz.getMethod("add", new Class[]{tmpObj.getClass()});
                    addMethd.invoke(obj, new Object[]{tmpObj});
                }
                retry = false;
	        }catch (ClassNotFoundException cnfe){
	        	LOGGER.error(CodeHelper.getStackTrace(cnfe));
	        }catch (SQLException sqle){
	        	LOGGER.error(CodeHelper.getStackTrace(sqle));
	        }catch (Exception e){
	        	if(numRetry++ < 3){
	        		LOGGER.error(CodeHelper.getStackTrace(e));
	        	}
	        }finally{
	        	try{
	        		if(resultSet!=null){
	        			resultSet.close();
	        		}
                }catch (Exception sqlEx){
                	LOGGER.error(CodeHelper.getStackTrace(sqlEx));
		    	}
	        }
		}while(retry);
        
        LOGGER.info("doSelect end 2");
		return (DataAccessInterface)obj;
	}

   /**creates an object oriented model of the data using DTOs, making it possible to populate data in dtos within dtos.
    * Usage: make sure the queries are not ordered since the method will append an order query to it.
    * 			dai corelates to strQuery
    * 			class should be of type DataAccessInterfaceObj
    * @param daiObj
    * @param strQuery
    * @param strFields
    * @param strJoins
    * TODO: add validation for missing or invalid entries
    */ 
   public DataAccessInterface doSelectMultiObj(Class[] daiObj, String[] strQuery, String[][] strFields, String[][] strDataTypes, String[][] strJoins){
	   
	   LOGGER.info("doSelectMultiObj start");
	   Object[] dais = new Object[daiObj.length];
	   for( int i = 0; i < daiObj.length; i++ ){
		   try{
			   dais[i] = doSelect(daiObj[i].getName(), strFields[i], strDataTypes[i], strQuery[i]);
			   
		   }catch(Exception e){
			   LOGGER.error(CodeHelper.getStackTrace(e));
		   }
	   }
	   for( int i = daiObj.length - 1 ; i > 0; i--){
		   try{
			   joinObjs((DataAccessInterfaceObj )dais[i - 1], (DataAccessInterfaceObj)dais[i], strJoins[i][0], strJoins[i][1]);
		   }catch( Exception e ){
			   LOGGER.error(CodeHelper.getStackTrace(e));
		   }
	   }
	   LOGGER.info("doSelectMultiObj end");
	   return (DataAccessInterface)dais[0];
   }

   /**
    * creates a dto whithin a dto to support object oriented data functions, documentation to follow
    * @param daiDestination
    * @param daiSource
    * @param strJoinDest
    * @param strJoinSource
    */
   private void joinObjs(DataAccessInterfaceObj daiDestination, DataAccessInterfaceObj daiSource, String strJoinDest, String strJoinSource){
	   
	   LOGGER.info("joinObjs start");
	   try{
		   List lsDest = daiDestination.get();
		   List lsSource = daiSource.get();
		   Iterator it = lsDest.iterator();
		   while( it.hasNext()  ){
			   Object ob = it.next(); 
			   Object obJoin = useMethod( ob, new StringBuffer("get").append(strJoinDest).toString(),
					   new Object[]{}, new Class[]{} );
			   List ibTemp = new Vector();
			   for(int i = 0; i < lsSource.size();){
				   try{
					   Object ib = lsSource.get(i);
					   Object ibJoin = useMethod(ib, new StringBuffer("get").append(strJoinSource).toString(), 
							   new Object[]{}, new Class[]{} );
					   if( ibJoin.toString().toUpperCase().equals( obJoin.toString().toUpperCase() )){
						   ibTemp.add(ib);
						   lsSource.remove(i);
					   }else{
						   i++;
					   }
				   }catch(Exception e){
					   //e.printStackTrace();
					   LOGGER.error("value being comapared is null or there has been an error in comparison");
					   i++;
				   }
			   }
			   useMethod(ob, "setFK" + strJoinDest, new Object[]{ibTemp}, new Class[]{List.class});
		   }
	   }catch( Exception e ){
		   
		   LOGGER.error("Entry has no join data");
	   }
	   LOGGER.info("joinObjs end");
   }

   private Object useMethod(Object ob, String strMethodName, Object[] objArgs, Class[] clsArgs){
	   
	   LOGGER.info("useMethod intialize");
	   try{
		   Method m = ob.getClass().getMethod(strMethodName, clsArgs);
		   Object objRet = m.invoke(ob, objArgs);
		   return objRet;
	   }catch(Exception e){
		   LOGGER.error(CodeHelper.getStackTrace(e));
		   return null;
	   }
}
   
   /*public DataAccessInterface doSelectSP(String className, String[] columnNames, String[] dataType, String query, String[] params){
	   
	   LOGGER.info("doSelectSP start");
	   CachedRowSetImpl resultSet = null;
       int numRetry = 0;
       boolean retry = false;
       Object obj = new Object();
       do{
            try{
            	WmsDbManager dbmgr = new WmsDbManager();
            	if(params == null || params.length < 1)
            	{
            		resultSet = (CachedRowSetImpl)dbmgr.doSelectSP(query);
            	}else
            	{
            		resultSet = (CachedRowSetImpl)dbmgr.ExecuteSP(query,params);
            	}
               
            	String[] getters = new String[columnNames.length];
	        	for(int i = 0; i < columnNames.length; i++){
	        		getters[i] = "set"+columnNames[i].substring(0,1).toUpperCase()+columnNames[i].substring(1);
	                }
	        	
	        	Class retClazz = Class.forName(className);

        		obj = new Object();
                obj = retClazz.newInstance();
                while (resultSet.next()){
                  
                    Object tmpObj = null;
                    Class clazz = Class.forName(className);
                    tmpObj = clazz.newInstance();
                    for(int i = 0; i < columnNames.length; i++){
                        
                        Object rsObj = resultSet.getObject(columnNames[i]);
                        Method setMethds = clazz.getMethod(getters[i], new Class[]{Class.forName(dataType[i])});
                        setMethds.invoke(tmpObj, new Object[]{rsObj});
						
                    }		
                    
                    Method addMethd = retClazz.getMethod("add", new Class[]{tmpObj.getClass()});
                    addMethd.invoke(obj, new Object[]{tmpObj});
                }
                retry = false;
	        }catch (ClassNotFoundException cnfe){
	        	LOGGER.error(CodeHelper.getStackTrace(cnfe));
	        }catch (SQLException sqle){
	        	LOGGER.error(CodeHelper.getStackTrace(sqle));
	        }catch (Exception e){
	        	if(numRetry++ < 3){
	        		LOGGER.error(CodeHelper.getStackTrace(e));
	        	}
	        }finally{
	        	   try{
	        		   if(resultSet!=null){
	        			   resultSet.close();
	        			   resultSet = null;
	        		   }
                   }catch (Exception sqlEx){
                	   LOGGER.error(CodeHelper.getStackTrace(sqlEx));
		    	}
	        }
       }while(retry);
       LOGGER.info("doSelectSP end");
       return (DataAccessInterface)obj;
	}*/
   
   
  /* public DataAccessInterface doSelectSP_(String className, String[] columnNames, String[] dataType, String query){
	   
	   LOGGER.info("doSelectSP_ start");
	   CachedRowSetImpl resultSet = null;
       int numRetry = 0;
       boolean retry = false;
       Object obj = new Object();
       do{
            try{
            	WmsDbManager dbmgr = new WmsDbManager();
            		resultSet = (CachedRowSetImpl)dbmgr.ExecuteSP_(query);
               
                
	        	String[] getters = new String[columnNames.length];
	        	for(int i = 0; i < columnNames.length; i++){
	        		getters[i] = "set"+columnNames[i].substring(0,1).toUpperCase()+columnNames[i].substring(1);
	                }
	        	
	        	Class retClazz = Class.forName(className);
	        	
        		obj = new Object();
                obj = retClazz.newInstance();
                while (resultSet.next()){
                
                    Object tmpObj = null;
                    Class clazz = Class.forName(className);
                    tmpObj = clazz.newInstance();
                    for(int i = 0; i < columnNames.length; i++){
                        
                        Object rsObj = resultSet.getObject(columnNames[i]);
                        Method setMethds = clazz.getMethod(getters[i], new Class[]{Class.forName(dataType[i])});
                        setMethds.invoke(tmpObj, new Object[]{rsObj});
						
                    }		
                    
                    Method addMethd = retClazz.getMethod("add", new Class[]{tmpObj.getClass()});
                    addMethd.invoke(obj, new Object[]{tmpObj});
                }
                retry = false;
	        }catch (ClassNotFoundException cnfe){
	        	LOGGER.error(CodeHelper.getStackTrace(cnfe));
	        }catch (SQLException sqle){
	        	LOGGER.error(CodeHelper.getStackTrace(sqle));
	        }catch (Exception e){
	        	if(numRetry++ < 3){
	        		LOGGER.error(CodeHelper.getStackTrace(e));
	        	}
	        }finally{
	        	   try{
	        		   if(resultSet!=null){
	        			   resultSet.close();
	        			   resultSet = null;
	        		   }
                   }catch (Exception sqlEx){
                	   LOGGER.error(CodeHelper.getStackTrace(sqlEx));
		    	}
	        }
       }while(retry);
       LOGGER.info("doSelectSP_ end");
       return (DataAccessInterface)obj;
	}
   */
   
   
  /*public ArrayList doSelectSPArr(String[] columnNames, String[] dataType, String spName, String[] spParams){
	   
	   LOGGER.info("doSelectSPArr start");
	   ArrayList resultArr = null;
	   int numRetry = 0;
       boolean retry = false;
       Object obj = new Object();
       do{
            try{
            	WmsDbManager dbmgr = new WmsDbManager();
            	resultArr = dbmgr.ExecuteSPArr(columnNames,dataType,spName,spParams);
            	retry = false;

	        }catch (SQLException sqle){
	        	LOGGER.error(CodeHelper.getStackTrace(sqle));
	        }catch (Exception e){
	        	if(numRetry++ < 3){
	        		LOGGER.error(CodeHelper.getStackTrace(e));
	        	}
	        }finally{
	        	   try{
	        		   //if(resultSet!=null){
	        			//   resultSet.close();
	        			//   resultSet = null;
	        		  // }
                   }catch (Exception sqlEx){
                	   LOGGER.error(CodeHelper.getStackTrace(sqlEx));
		    	}
	        }
       }while(retry);
       
       LOGGER.info("doSelectSPArr end");
       return resultArr;
	}*/
   
   
   /**
    * insterts data from a dto into a table 
    * @param strTable table to be inserted into
    * @param dai dto data source
    * @return
    */
  /* public boolean doInsert(String strTable, DataAccessInterface dai){
	  
	   LOGGER.info("doInsert start");
	   StringBuffer sb = new StringBuffer();
	   
	   String[] straAvailableFields = compareTableFieldsWithDTOFields(getFieldNamesFromDTO(dai), strTable);
	   
	   if(strTable == null){
		   return false;
	   }
		   
	   try{
		   sb.append("INSERT INTO ")
	   		.append(strTable)
	   		.append("(")
	   		.append(commaSeparateArray(straAvailableFields))
	   		.append(") VALUES (")
	   		.append(commaSeparateArray(getValuesFromDTOForSQL(dai, straAvailableFields)))
	   		.append(")");
		   	ExecQuery(sb.toString());
	   }catch(Exception e){
		   LOGGER.error(CodeHelper.getStackTrace(e));
		   return false;
	   }
	   LOGGER.info("doInsert end");
	   return true; 
   }*/

   /**
    * updates a table using data from a dto
    * @param strTable table to be updated
    * @param dai dto datasource
    * @param strCriteria where query stating which records should be updated
    * @param straAvailableFields which fields should be affected
    * @return
    */
   /*public boolean doUpdate(String strTable, DataAccessInterface dai, String strCriteria, String[] straAvailableFields){
	   
	   LOGGER.info("doUpdate start 1");
	   StringBuffer sb = new StringBuffer();
	   
	   String[] staDTOValues = getValuesFromDTOForSQL(dai, straAvailableFields);
	   
	   if(strTable == null){
		   return false;
	   }

	   try{
		   sb.append("UPDATE ")
	   		.append(strTable)
	   		.append(" SET ")
	   		.append(createUpdateSet(straAvailableFields, staDTOValues))
	   		.append(" WHERE (")
	   		.append(strCriteria)
	   		.append(")");
		   	ExecQuery(sb.toString());
	   }catch(Exception e){
		   LOGGER.error(CodeHelper.getStackTrace(e));
		   return false;
	   }
	   LOGGER.info("doUpdate end 1");
	   return true;
   }*/

   /**
    * updates a table using data from a dto
    * @param strTable table to be updated
    * @param dai dto datasource
    * @param strCriteria where query stating which records should be updated
    * @return
    */
  
   /*public boolean doUpdate(String strTable, DataAccessInterface dai, String strCriteria){
	   
	   LOGGER.info("doUpdate start 2");
	   StringBuffer sb = new StringBuffer();
	   
	   String[] straAvailableFields = compareTableFieldsWithDTOFields(getFieldNamesFromDTO(dai), strTable);
	   String[] staDTOValues = getValuesFromDTOForSQL(dai, straAvailableFields);
	   if(strTable == null)
		   return false;
	   try{
		   sb.append("UPDATE " )
	   		.append(strTable)
	   		.append(" SET ")
	   		.append(createUpdateSet(straAvailableFields, staDTOValues))
	   		.append(" WHERE (")
	   		.append(strCriteria)
	   		.append(")");
		   	ExecQuery(sb.toString());
	   }catch(Exception e){
		   LOGGER.error(CodeHelper.getStackTrace(e));
		   return false;
	   }
	   LOGGER.info("doUpdate end 2");
	   return true;
   }*/
   
   private String createUpdateSet(String[] straFields, String[] straValues){

	   LOGGER.info("createUpdateSet initialize");
	   StringBuffer sb = new StringBuffer();
	   try{
		   for(int i = 0; i < straFields.length; i++){
			   sb.append(straFields[i])
			   	.append( " = "  )
			   	.append( straValues[i] );
			   	if(i < straFields.length - 1)
			   		sb.append(" , ");
		   }
		   
		   return sb.toString();
	   }catch(Exception e){
		   LOGGER.error(CodeHelper.getStackTrace(e));
		   return sb.toString();
	   }
	   
   }
   
   private String[] getFieldNamesFromDTO(DataAccessInterface dai){
	   
	   LOGGER.info("getFieldNamesFromDTO starts");
	   String[] ls = null;
	   try{
		   Method[] m = dai.getClass().getDeclaredMethods();
		   List v = new Vector();
		   
		   for(int i = 0; i < m.length; i++){
			   if( m[i].getName().startsWith("set"))
				   v.add( m[i].getName().substring(3));
		   }
		   
		   ls = new String[v.size()];
		   
		   for(int i = 0; i < v.size(); i++)
			   ls[i] = (String)v.get(i);
		   
	   }catch (Exception e){
		   LOGGER.error(CodeHelper.getStackTrace(e));
		   return ls;
	   }
	   LOGGER.info("getFieldNamesFromDTO end");
	   return ls; 
   }
   
   //This method will try to convert the return object to a string via toString()
   //TODO tighter value cast checking
   //TODO null m[] length
  /* private String[] getValuesFromDTOForSQL(DataAccessInterface dai, String[] straFields){
	   
	   LOGGER.info("getValuesFromDTOForSQL start");
	   String[] ls = null;
	   List l = new Vector(), lc = new Vector();
		   
	   for(int i = 0; i < straFields.length; i++){
		   try{
			   Method m = dai.getClass().getMethod("get" + straFields[i], new Class[]{});
			   l.add(m.invoke(dai, new Object[]{}));
			   lc.add(m.getReturnType());
		   }catch( Exception e ){
			   LOGGER.error(CodeHelper.getStackTrace(e));
			   LOGGER.error("method: get" + straFields[i] + " was not found");
		   }
	   }
	
	   ls = new String[l.size()];
	   
	   for(int i = 0; i < l.size(); i++){
		   ls[i] = new StringBuffer("")
	   			.append(convertObjectToString(l.get(i), (Class )lc.get(i), "MM-dd-yyyy hh:mm a"))
	   			.append("").toString();
	   }
	   LOGGER.info("getValuesFromDTOForSQL end");
	   return ls; 
   }
   */
   private String commaSeparateArray(String[] strSource){
	   
	   LOGGER.info("commaSeparateArray start");
	   StringBuffer sb = new StringBuffer();
	   try{
		   for(int i = 0; i < strSource.length; i++){
			   sb.append(strSource[i]);
			   if(i < ( strSource.length - 1))
				   sb.append(", ");
		   }
		   
	   }catch(Exception e){
		   LOGGER.error(CodeHelper.getStackTrace(e));
		   return sb.toString();
	   }
	   LOGGER.info("commaSeparateArray end");
	   return sb.toString();
   }
   
   private String[] compareTableFieldsWithDTOFields(String[] straDTOFields, String strTable){
	   
	   LOGGER.info("compareTableFieldsWithDTOFields start");
	   CachedRowSetImpl resultSet = null;
	   List ls = new Vector();
	   
	   try{
		   WmsDbManager dbmgr = new WmsDbManager();
		   resultSet = (CachedRowSetImpl)dbmgr.doSelect("SELECT TOP 1 * from " + strTable);
	   }catch( Exception e ){
		   LOGGER.error(CodeHelper.getStackTrace(e));
		   return new String[0];
	   }
	   
	   for(int i = 0; i < straDTOFields.length; i++){
		   try{
			   if (resultSet.findColumn( straDTOFields[i] ) > -1){
				   ls.add(straDTOFields[i]);
			   }
		   }catch(Exception e){
			   LOGGER.error("Column" + straDTOFields[i] + "does not exist");
			   LOGGER.error(CodeHelper.getStackTrace(e));
		   }
	   }
	   
	   try{
		   resultSet.close();
		   resultSet = null;
	   }catch (SQLException sqle){
		   sqle.printStackTrace();
	   }
	   
	   String[] ret = new String[ ls.size()];
	   for( int i = 0; i < ls.size(); i++ ){
		   ret[i] = ( String )ls.get( i );
	   }
	   LOGGER.info("compareTableFieldsWithDTOFields end");
	   return ret;
   }
   
  /* private String convertObjectToString(Object o, Class c, String strDateFormat){
	   
	   LOGGER.info("convertObjectToString start");
	   StringBuffer sb = new StringBuffer();
	   if(c == null){
		   c = o.getClass();
	   }
	   
	   if(c.getName().equals("java.lang.String"))
		   return sb.append("'")
		   			.append(o == null?"":o.toString())
		   			.append("'").toString();
	   
	   if(c.getName().equals("java.lang.Integer"))
		   return (o == null)?"0":((Integer)o).toString();
	   
	   if(c.getName().equals("java.lang.Double"))
		   return (o == null )?"0":((Double )o).toString();
	   
	   if(c.getName().equals("java.lang.Float"))
		   return (o == null )?"0":((Float)o).toString();
	   
	   if(c.getName().equals("java.util.Date")){
		   String temp;
		   try{
			   SimpleDateFormat sdf = new SimpleDateFormat(strDateFormat);
			   temp = sdf.format((Date )o);
		   }catch(Exception e){
			   return "'01-01-1900'";
		   }
		   return sb.append("'")
  			.append( temp )
   			.append("'").toString();
	   }
	   
	   if(c.getName().equals("java.lang.Boolean")){
		   if(o == null){
			   return "0";
		   }else{
			   if((( Boolean )o).booleanValue() == true){
				   return "1";
			   }
			   return "0";
		   }
	   }
	   else return "";
	   
   }*/
}
