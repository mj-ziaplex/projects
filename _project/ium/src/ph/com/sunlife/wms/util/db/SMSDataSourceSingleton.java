package ph.com.sunlife.wms.util.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ResourceBundle;

import javax.naming.NamingException;
import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.slocpi.ium.util.CodeHelper;

/**
 * A singleton object which holds the datasources that are used throughout 
 * WMS (Workflow Management System)
 * 
 * Since all database interaction shares the same datasource it is efficient to put 
 * datasource objects in a singleton object
 *  
 * @author Christian (Sept 4, 2006)
 * @edited Andre Ceasar Dacanay (July 31, 2008) from 1.1 to 1.2
 * 		ph.com.sunlife.wms.util.db - Optimization
 * 
 */
public class SMSDataSourceSingleton {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(SMSDataSourceSingleton.class);
	private DataSource wmsDS = null;

	/** Connection **/
	private Connection conn = null;
	
	/** An instance variable of this class*/
	private static SMSDataSourceSingleton instance = null;
	private static final String empty = "";
	private String url;
	private String databaseUserName = "";
	private String databasePassword = empty;
	private boolean createNewConnection;

	/**
	 * Contructor is made private so that it can't be instantiated anywhere else
	 * except within this class.
	 * @throws NamingException 
	 *
	 */
	private SMSDataSourceSingleton() throws NamingException, SQLException, ClassNotFoundException {
			
			LOGGER.info("SMSDataSourceSingleton Constructor start");

			try {
				ResourceBundle rb = ResourceBundle.getBundle("dbconfig");
				String conn_driver = rb.getString("connection-driver");
				url = rb.getString("sms_url");
				databaseUserName = rb.getString("sms_username");
				databasePassword = rb.getString("sms_password");
				Class.forName(conn_driver);
				conn = DriverManager.getConnection(url, databaseUserName, databasePassword);	
				createNewConnection = true;
			} catch (Exception e) {
				LOGGER.error(CodeHelper.getStackTrace(e));
			}
			
			LOGGER.info("SMSDataSourceSingleton Constructor end");
	}
	
	/**
	 * Returns an instance of this class. If no instance exists then create a new one,
	 * otherwise return the existing instance
	 * 
	 * @return an instance of this class
	 * @throws NamingException 
	 * @throws ClassNotFoundException 
	 * @throws SQLException 
	 */
	public static synchronized SMSDataSourceSingleton getInstance() throws NamingException, SQLException, ClassNotFoundException {
		
		LOGGER.info("getInstance start");
		if(instance == null){
			instance = new SMSDataSourceSingleton();
		}
		LOGGER.info("getInstance end");
		return instance;
	}
	
	/**
	 * Retrieves the datasource for WMS- SQL server
	 * @return DataSource of WMS - SQL Server
	 */
	public DataSource getWmsDataSource(){
		return wmsDS;
	} 
	
	/**
	 * Retrives the connection for WMS - SQL server
	 * @return Connection
	 * @throws SQLException
	 */
	public Connection getConnection() throws SQLException {
		
		LOGGER.info("getConnection intialize");
		if(!createNewConnection){
			if (wmsDS != null && conn == null) {
				return wmsDS.getConnection();
			} else {
				return conn;
			}
		} else {
			return DriverManager.getConnection(url, databaseUserName, databasePassword);	
		}
		
	}
}

