package ph.com.sunlife.wms.util.db;

import java.lang.reflect.Method;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import javax.naming.NamingException;
import javax.sql.DataSource;
import javax.sql.rowset.CachedRowSet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ph.com.sunlife.wms.dto.DataAccessInterface;
import com.slocpi.ium.util.CodeHelper;
import com.sun.rowset.CachedRowSetImpl;

/**
 * This class has the following responsibilities:<br />
 * <ul>
 * <li>Interact with the database (Select, update, delete, insert, SP Execution) </li>
 * <li>Create and populate the appropriate transfer objects </li>
 *</ul>
 * @author Christian (Sept 4, 2006)
 * @edited Andre Ceasar Dacanay (July 31, 2008) from 1.2 to 1.3
 * 		ph.com.sunlife.wms.util.db - Optimization
 * 
 */
public class WmsDbManager{
	private static final Logger LOGGER = LoggerFactory.getLogger(WmsDbManager.class);
	private Connection conn = null;
	private DataSource dataSource = null;
	private ResultSet rs = null;
	private Statement stmt = null;
	private boolean isBatch = false;
	
	/**
	 * Default WMS DB Manager constructor
	 */
	public WmsDbManager(){
		LOGGER.info("WmsDbManager instance created");
		if(dataSource == null){
			try{
				dataSource = DataSourceSingleton.getInstance().getWmsDataSource();
			}catch (NamingException e){
				LOGGER.error(CodeHelper.getStackTrace(e));
			}catch (SQLException e){
				LOGGER.error(CodeHelper.getStackTrace(e));
			}catch (ClassNotFoundException e){
				LOGGER.error(CodeHelper.getStackTrace(e));
			}
		}
	}
	
	
	
	/*public int doUpdateSP(String SPName, String[] strParameters) throws SQLException{
		
		LOGGER.info("doUpdateSP start");
		CallableStatement  cs = null;
		int update = -1;
        try{
        	conn = getConnection(); 
        	if(strParameters == null || strParameters.length < 1) {
            	cs = conn.prepareCall("{call " + SPName +"}");
        	} else {
                String sqlStatement = "{call " + SPName + " (";
                sqlStatement = sqlStatement + "?";
                for(int i = 1; i < strParameters.length; i++){
                    sqlStatement = sqlStatement + ",?";
                }
                sqlStatement = sqlStatement + ")}";
                cs = conn.prepareCall(sqlStatement);
                for(int i = 0; i < strParameters.length; i++){
                    if(strParameters[i] != null)
                    	cs.setString(i + 1, strParameters[i]);
                }
        	}
            update = cs.executeUpdate();   
        }catch(SQLException sqle){
        	LOGGER.error(CodeHelper.getStackTrace(sqle));
			throw sqle;
        }finally{
        	try{
        		if(cs!=null){
        			cs.close();
        			cs = null;
        		}
    			if(conn!=null){
	    			conn.close();
	    			conn = null; 
    			}
        	}catch(SQLException sqle){
        		throw sqle;
        	}       	
        }
        LOGGER.info("doUpdateSP end");
        return update;
    }
	*/
	
	/*public CachedRowSet doSelectSP(String SPName) throws SQLException{
		
		LOGGER.info("doSelectSP start");
        CallableStatement  cs = null;
        CachedRowSetImpl crset = null;
                   
        try{
        	conn = getConnection(); 
        	cs = conn.prepareCall("{call " + SPName +"}");
            rs = cs.executeQuery();  
          
			try{
				crset = new CachedRowSetImpl();
			}catch (Exception f){
				LOGGER.error(CodeHelper.getStackTrace(f));
				crset = new CachedRowSetImpl();
			}
			
            crset.populate(rs);
        }catch(SQLException sqle){
        	LOGGER.error(CodeHelper.getStackTrace(sqle));
			throw sqle;
        }finally{
        	try{
        		if(cs!=null){
        			cs.close();
        			cs = null;
        		}
        		if(rs!=null){
	    			rs.close();
	    			rs = null;
        		}
    			if(conn!=null){
	    			conn.close();
	    			conn = null; 
    			}
        	}catch(SQLException sqle){
        		LOGGER.error(CodeHelper.getStackTrace(sqle));
        		throw sqle;
        	}       	
        }
        LOGGER.info("doSelectSP end");
        return crset;
    }*/
	
 /*   public CachedRowSet ExecuteSP(String SPName, String[] strParameters) throws SQLException{
    	
    	LOGGER.info("ExecuteSP start");
        CallableStatement  cs = null;
        CachedRowSetImpl crset = null;
        
        try{
            String sqlStatement = "{call " + SPName + " (";
            sqlStatement = sqlStatement + "?";
            for(int i = 1; i < strParameters.length; i++){
                sqlStatement = sqlStatement + ",?";
            }
            sqlStatement = sqlStatement + ")}";
            conn = getConnection(); 
            cs = conn.prepareCall(sqlStatement);
            for(int i = 0; i < strParameters.length; i++){
                if(strParameters[i] != null){
                        cs.setString(i + 1, strParameters[i]);
                    }
            }
            rs = cs.executeQuery();

			try{
				crset = new CachedRowSetImpl();
			}catch (Exception f){
				LOGGER.error(CodeHelper.getStackTrace(f));
				crset = new CachedRowSetImpl();
			}
		
            crset.populate(rs);
        }catch(SQLException sqle){
        	LOGGER.error(CodeHelper.getStackTrace(sqle));
			throw sqle;
        }finally{
        	try{
        		if(cs!=null){
        			cs.close();
        			cs = null;
        		}
        		if(rs!=null){
	    			rs.close();
	    			rs = null;
        		}
    			if(conn!=null){
	    			conn.close();
	    			conn = null; 
    			}
        	}catch(SQLException sqle){
        		LOGGER.error(CodeHelper.getStackTrace(sqle));
        		throw sqle;
        	}     	
        }
        LOGGER.info("ExecuteSP end");
        return crset;
    }*/
	
	/*
    public CachedRowSet ExecuteSP_(String SPName) throws SQLException{
    	
    	LOGGER.info("ExecuteSP_ start");
        CallableStatement  cs = null;
        CachedRowSetImpl crset = null;
        
        try{
            String sqlStatement = "{call " + SPName + "}";
            conn = getConnection(); 
            cs = conn.prepareCall(sqlStatement);
            rs = cs.executeQuery();
			try{
				crset = new CachedRowSetImpl();
			}catch (Exception f){
				LOGGER.error(CodeHelper.getStackTrace(f));
				crset = new CachedRowSetImpl();
			}
            crset.populate(rs);
        }catch(SQLException sqle){
        	LOGGER.error(CodeHelper.getStackTrace(sqle));
			throw sqle;
        }finally{
        	try{
        		if(cs!=null){
        			cs.close();
        			cs = null;
        		}
        		if(rs!=null){
	    			rs.close();
	    			rs = null;
        		}
    			if(conn!=null){
	    			conn.close();
	    			conn = null; 
    			}
        	}catch(SQLException sqle){
        		LOGGER.error(CodeHelper.getStackTrace(sqle));
        		throw sqle;
        	}     	
        }
        LOGGER.info("ExecuteSP_ end");
        return crset;
    }
*/
	
   /* public CachedRowSet ExecuteSPObj(String SPName, Object[] strParameters) throws SQLException{
    	
    	LOGGER.info("ExecuteSPObj start");
        CallableStatement  cs = null;
        CachedRowSetImpl crset = null;
        try{
            String sqlStatement = "{call " + SPName + " (";
            sqlStatement = sqlStatement + "?";
            for(int i = 1; i < strParameters.length; i++){
                sqlStatement = sqlStatement + ",?";
            }
            sqlStatement = sqlStatement + ")}";
            conn = getConnection();
            cs = conn.prepareCall(sqlStatement);
            for(int i = 0; i < strParameters.length; i++){
                if(strParameters[i] != null){
                    cs.setObject(i + 1, strParameters[i]);
                }
            }
            
            rs = cs.executeQuery();
            crset = new CachedRowSetImpl();
            crset.populate(rs);
        }catch(SQLException sqle){
        	LOGGER.error(CodeHelper.getStackTrace(sqle));
			throw sqle;
        }finally{
        	try{
        		if(cs!=null){
        			cs.close();
        			cs = null;
        		}
        		if(rs!=null){
	    			rs.close();
	    			rs = null;
        		}
    			if(conn!=null){
	    			conn.close();
	    			conn = null; 
    			}
        	}catch(SQLException sqle){
        		LOGGER.error(CodeHelper.getStackTrace(sqle));
        		throw sqle;
        	}    	
        }
        LOGGER.info("ExecuteSPObj end");
        return crset;
    }*/
    
    public void initializeBatchSaving() throws SQLException{
    	
    	LOGGER.info("initializeBatchSaving start");
		conn = getConnection();
		conn.setAutoCommit(false);
		stmt = conn.createStatement();
		isBatch = true;
		LOGGER.info("initializeBatchSaving end");
	}
	
	public void addBatch(String sql) throws SQLException{
		
		LOGGER.info("addBatch start");
		if(isBatch){
			stmt.addBatch(sql);
		}else{
			LOGGER.error("Batch saving not initialized..");
			throw new SQLException("Batch saving not initialized..");
		}
		LOGGER.info("addBatch end");
	}
	
	public void executeBatch() throws SQLException {
		
		LOGGER.info("executeBatch start");
		int[] batchResult = null;
		boolean shouldRollBack = false;
		try{
			if (isBatch) {
				batchResult = stmt.executeBatch();
			}
			
			for (int i = 0; i < batchResult.length; i++) {
				if (batchResult[i] < 0) {
					shouldRollBack = true;
					rollBack();
					break;
				}
			}
			if (!shouldRollBack) {
				conn.commit();
			}
			
		}catch(SQLException sqle){
			LOGGER.error(CodeHelper.getStackTrace(sqle));
			throw sqle;
		}finally{
			if(stmt!=null){
				stmt.close();
				stmt = null;
			}
			if(conn!=null){
				conn.setAutoCommit(true);
				conn.close();
				conn = null;
			}
		}
		LOGGER.info("executeBatch end");
	}

	private void rollBack() throws SQLException{
		LOGGER.info("rollBack start");
		conn.rollback();
		LOGGER.info("rollBack end");
	}

	
	/**
	 * This method executes the passed SQL Statement
	 * 
	 * @param sql 				 The sql statement to be executed
	 * @return					 A disconnected implementation of a ResultSet
	 * @throws SQLException 
	 */
	public CachedRowSet doSelect(String sql) throws SQLException{
		
		LOGGER.info("doSelect start");
		Statement stmt = null;
		CachedRowSetImpl crset = null;
		try{
			 
			conn = getConnection();
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);
			try{
				crset = new CachedRowSetImpl();
			}catch (Exception f){
				crset = new CachedRowSetImpl();
			}
			crset.populate(rs);
		}catch(SQLException sqle){
			LOGGER.error(CodeHelper.getStackTrace(sqle));
			 throw sqle;
		}finally{
        	try{
        		if(stmt!=null){
        			stmt.close();
        			stmt = null;
        		}
        		if(rs!=null){
	    			rs.close();
	    			rs = null;
        		}
    			if(conn!=null){
	    			conn.close();
	    			conn = null; 
    			}
        	}catch(SQLException sqle){
        		LOGGER.error(CodeHelper.getStackTrace(sqle));
        		throw sqle;
        	}
		 }
		LOGGER.info("doSelect end");
		 return crset;
	}

	//WMSStepProcessor_Module - Andre Ceasar Dacanay - start
	/*public ArrayList ExecuteSPArr(String[] columnNames, String[] dataType, String SPName, String[] SPParameters) throws SQLException{
		
		LOGGER.info("ExecuteSPArr start");
		
        CallableStatement  cs = null;
        ArrayList resultArr = null;
        ArrayList resultDetArr = null;
                   
        try{
            conn = getConnection(); 
            
            if( (SPParameters!=null) && (SPParameters.length > 0) ){
            	
                String sqlStatement = "{call " + SPName + " (";
                sqlStatement = sqlStatement + "?";
                for(int i = 1; i < SPParameters.length; i++){
                    sqlStatement = sqlStatement + ",?";
                }
                sqlStatement = sqlStatement + ")}";
                 
                cs = conn.prepareCall(sqlStatement);
                for(int i = 0; i < SPParameters.length; i++){
                    if(SPParameters[i] != null){
                            cs.setString(i + 1, SPParameters[i]);
                        }
                }
                
            }else{
            	cs = conn.prepareCall("{call " + SPName +"}");
            }

            rs = cs.executeQuery();  
            
            HashMap tmpHm = null;

            resultArr = new ArrayList();

            while(rs.next()){
            	
            	resultDetArr = new ArrayList();

        		for(int i=0; i<columnNames.length;i++){
        			
        			tmpHm = new HashMap();

        			if(dataType[i].equals("java.lang.String")){
        				tmpHm.put(columnNames[i], rs.getString(columnNames[i]));
        			}else if(dataType[i].equals("java.lang.Integer")){
        				tmpHm.put(columnNames[i], new Integer(rs.getInt(columnNames[i])));
        			}else if(dataType[i].equals("java.math.BigDecimal")){
        				tmpHm.put(columnNames[i],rs.getBigDecimal(columnNames[i]));
        			}else if(dataType[i].equals("java.util.Date")){
        				tmpHm.put(columnNames[i],rs.getDate(columnNames[i]));
        			}else if(dataType[i].equals("java.sql.Timestamp")){
        				tmpHm.put(columnNames[i],rs.getTimestamp(columnNames[i]));
        			}else if(dataType[i].equals("java.lang.Boolean")){
        				tmpHm.put(columnNames[i], new Boolean (rs.getBoolean(columnNames[i])));
        			}
        			
        			resultDetArr.add(tmpHm);

        		}
        		
        		resultArr.add(resultDetArr);

        	}
            
        }catch(SQLException sqle){
        	LOGGER.error(CodeHelper.getStackTrace(sqle));
			throw sqle;
        }finally{
    		if(rs!=null){
    			rs.close();
    		}
    		if(cs!=null){
    			cs.close();
    		}
			if(conn!=null){
    			conn.close();
			}
		
        }
        LOGGER.info("ExecuteSPArr end");
        return resultArr;
        
    }
	*/
	
	/**
	 * Executes INSERT, UPDATE, and DELETE sql statements
	 * @param sql the sql string
	 * @throws SQLException If an error occured
	 */
	public void doSave(String sql) throws SQLException{
		
		LOGGER.info("doSave start");
		try{
			
			conn = getConnection();
			stmt = conn.createStatement();
			stmt.execute(sql);			
		}catch(SQLException sqle){
			throw sqle;
		}finally{
        	try{
        		if(stmt!=null){
        			stmt.close();
        			stmt = null;
        		}
    			if(conn!=null){
	    			conn.close();
	    			conn = null; 
    			}
        	}catch(SQLException sqle){
        		LOGGER.error(CodeHelper.getStackTrace(sqle));
        		throw sqle;
        	}
		}
		LOGGER.info("doSave end");
	}
	
	/**
	 * Retrieves the connection from the DataSource
	 * @return Connection object
	 * @throws SQLException 
	 */
	public Connection getConnection() throws SQLException{
		
		LOGGER.info("getConnection start");
		try{
			conn = DataSourceSingleton.getInstance().getConnection();
			
			LOGGER.info("getConnection end");
			return conn;
		} catch (NamingException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new SQLException(e.getMessage());
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new SQLException(e.getMessage());
		} catch (ClassNotFoundException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new SQLException(e.getMessage());
		}	
		
	}
	
	
	public DataAccessInterface doSelectRStoDTO(String className, String[] columnNames, String[] dataType, String query){
		
		LOGGER.info("doSelectRStoDTO start");
	    boolean retry = false; 
        Object obj = new Object();
        ResultSet resultSet = null;
		
        do{
            try{
            	conn = getConnection();
            	stmt = conn.createStatement();
            	resultSet = stmt.executeQuery(query);
    			
                
        	String[] getters = new String[columnNames.length];
        	for(int i = 0; i < columnNames.length; i++){
        		getters[i] = "set"+columnNames[i].substring(0,1)
  							.toUpperCase()+columnNames[i].substring(1);
        	
                }
        		
        		Class retClazz = Class.forName(className);
                
                 obj = retClazz.newInstance();
               
                 while (resultSet.next()) {
                    
                    Object tmpObj = null;
                    Class clazz = Class.forName(className);
                    tmpObj = clazz.newInstance();
                    for(int i = 0; i < columnNames.length; i++){
                    	Object rsObj = resultSet.getObject(columnNames[i]);
                        Method setMethds = clazz.getMethod(getters[i], new Class[]{Class.forName(dataType[i])});
                        setMethds.invoke(tmpObj, new Object[]{rsObj});
                    
                    }		
                    Method addMethd = retClazz.getMethod("add", new Class[]{tmpObj.getClass()});
                    addMethd.invoke(obj, new Object[]{tmpObj});
                   
                 }
                 retry = false;
	        }catch (ClassNotFoundException cnfe){
	        	LOGGER.error(CodeHelper.getStackTrace(cnfe));
	        }catch (SQLException sqle){
	        	LOGGER.error(CodeHelper.getStackTrace(sqle));
	        }catch (Exception e){
	        	LOGGER.error(CodeHelper.getStackTrace(e));
	        }finally{
	        	try{
	        		if(resultSet != null){
	        			resultSet.close();
	        			resultSet = null;
	        		}
	        		if(stmt != null ){
	        			stmt.close();
	        			stmt = null;
	        		}
	        		if(conn != null){
		        		conn.close();
		        		conn = null; 
	        		}
	        		
	            }catch (Exception sqlEx){
	            	LOGGER.error(CodeHelper.getStackTrace(sqlEx));
		    	}
	        }
		}while(retry);
        LOGGER.info("doSelectRStoDTO end");
		return (DataAccessInterface)obj;
	}
}
