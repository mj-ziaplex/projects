package ph.com.sunlife.wms.util.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ResourceBundle;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.slocpi.ium.util.CodeHelper;

/**
 * A singleton object which holds the datasources that are used throughout 
 * WMS (Workflow Management System)
 * 
 * Since all database interaction shares the same datasource it is efficient to put 
 * datasource objects in a singleton object
 *  
 * @author Christian (Sept 4, 2006)
 * @edited Andre Ceasar Dacanay (July 31, 2008) from 1.1 to 1.2
 * 		ph.com.sunlife.wms.util.db - Optimization
 * s
 */
public class DataSourceSingleton {
	private static final Logger LOGGER = LoggerFactory.getLogger(DataSourceSingleton.class);
	/** Datasource object for WMS */
	private DataSource wmsDS = null;

	/** Connection **/
	private Connection conn = null;
	
	/** An instance variable of this class*/
	private static DataSourceSingleton instance = null;
	private static final String empty = "";
	private String databaseServer = "";
	private String databasePort = "";
	private String databaseName = "";
	private String databaseUserName = "";
	private String databasePassword = empty;
	private String databaseDriverClass = "";	
	private boolean createNewConnection;
	private String url;

	/**
	 * Contructor is made private so that it can't be instantiated anywhere else
	 * except within this class.
	 * @throws NamingException 
	 *
	 */
	private DataSourceSingleton() throws NamingException, SQLException, ClassNotFoundException {
			
		LOGGER.info("DataSourceSingleton Constructor start");
			try {
				ResourceBundle rb = ResourceBundle.getBundle("JNDIConfig");
				InitialContext ctx = new InitialContext();
				wmsDS = (DataSource)ctx.lookup(rb.getString("JNDI_NAME"));
				conn = wmsDS.getConnection();
				createNewConnection = false;
			} catch (Exception e) {
				LOGGER.error(CodeHelper.getStackTrace(e));
				ResourceBundle rb = ResourceBundle
					.getBundle("com.ph.sunlife.component.properties.ComponentProperties");
				databaseServer = rb.getString("DatabaseServer");
				databasePort = rb.getString("DatabasePort");
				databaseName = rb.getString("DatabaseName");
				databaseUserName = rb.getString("DatabaseUserName");
				databasePassword = rb.getString("DatabasePassword");
				databaseDriverClass = rb.getString("DatabaseDriverClass");
				Class.forName(databaseDriverClass);
				url = ("jdbc:microsoft:sqlserver://" + databaseServer + ":"
					+ databasePort + ";User=" + databaseUserName + ";Password="
					+ databasePassword + ";DatabaseName=" + databaseName);
				conn = DriverManager.getConnection(url);
				createNewConnection = true;
			}
			
			LOGGER.info("DataSourceSingleton Constructor end");
	}
	
	/**
	 * Returns an instance of this class. If no instance exists then create a new one,
	 * otherwise return the existing instance
	 * 
	 * @return an instance of this class
	 * @throws NamingException 
	 * @throws ClassNotFoundException 
	 * @throws SQLException 
	 */
	public static synchronized DataSourceSingleton getInstance()
			throws NamingException, SQLException, ClassNotFoundException {
		
		LOGGER.info("getInstance start");
		if(instance == null){
			instance = new DataSourceSingleton();		
		}
		LOGGER.info("getInstance end");
		return instance;
	}
	/**
	 * Retrieves the datasource for WMS- SQL server
	 * @return DataSource of WMS - SQL Server
	 */
	public DataSource getWmsDataSource(){
		return wmsDS;
	} 
	
	/**
	 * Retrives the connection for WMS - SQL server
	 * @return Connection
	 * @throws SQLException
	 */
	public Connection getConnection() throws SQLException {
		
		LOGGER.info("getConnection initialize");
		if(!createNewConnection){
			if (wmsDS != null && conn == null) {
				return wmsDS.getConnection();
			} else {
				return conn;
			}
		} else {
			return conn = DriverManager.getConnection(url);
		}
	}
}

