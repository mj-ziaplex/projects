package ph.com.sunlife.wms.ium.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.slocpi.ium.util.CodeHelper;

public class DateHelper {
		private static final Logger LOGGER = LoggerFactory.getLogger(DateHelper.class);
	 public static final java.util.Date 	SMALLESTDATE = parse("1/1/1980");
	  public static final java.util.Date 	BIGGESTDATE = parse("12/31/2050");
	  public static final String			DEFAULT_PATTERN	=	"MM/dd/yyyy";
	  public static final String			MIB_PATTERN	=	"MMddyyyy";
	  
	  public static final int ADD_START_DAY_TIME = 0;
	  public static final int ADD_END_DAY_TIME = 1;
	  
	  public static final String START_DAY_TIME = " 00:00:00";
	  public static final String END_DAY_TIME = " 23:59:59";
	  
	/*
	  Symbol   Meaning                 Presentation        Example
	  ------   -------                 ------------        -------
	  G        era designator          (Text)              AD
	  y        year                    (Number)            1996
	  M        month in year           (Text & Number)     July & 07
	  d        day in month            (Number)            10
	  h        hour in am/pm (1~12)    (Number)            12
	  H        hour in day (0~23)      (Number)            0
	  m        minute in hour          (Number)            30
	  s        second in minute        (Number)            55
	  S        millisecond             (Number)            978
	  E        day in week             (Text)              Tuesday
	  D        day in year             (Number)            189
	  F        day of week in month    (Number)            2 (2nd Wed in July)
	  w        week in year            (Number)            27
	  W        week in month           (Number)            2
	  a        am/pm marker            (Text)              PM
	  k        hour in day (1~24)      (Number)            24
	  K        hour in am/pm (0~11)    (Number)            0
	  z        time zone               (Text)              Pacific Standard Time
	  '        escape for text         (Delimiter)
	  ''       single quote            (Literal)           '

	  Format Pattern                         Result
	  --------------                         -------
	  "yyyy.MM.dd G 'at' hh:mm:ss z"    ->>  1996.07.10 AD at 15:08:56 PDT
	  "EEE, MMM d, ''yy"                ->>  Wed, July 10, '96
	  "h:mm a"                          ->>  12:08 PM
	  "hh 'o''clock' a, zzzz"           ->>  12 o'clock PM, Pacific Daylight Time
	  "K:mm a, z"                       ->>  0:00 PM, PST
	  "yyyyy.MMMMM.dd GGG hh:mm aaa"    ->>  1996.July.10 AD 12:08 PM
	*/

	  public static boolean isEqual(Date original, Date target) {
		  
		boolean result = true;
	  	if ((original != null) && (target != null)) {
	  		int oYear = original.getYear();
	  		int oMon = original.getMonth();
	  		int oDay = original.getDay();
	  		int tYear = target.getYear();
	  		int tMon = target.getMonth();
	  		int tDay = target.getDay();
	  		
	  		if (oYear != tYear || oMon != tMon || oDay != tDay) {
	  			result = false;
	  		}

	  	} else if ((original == null) && (target == null)) {
	  		// do nothing
	  	} else {
	  		result = false;
	  	}
	  	
	  	return result;
	  }
	  
	  public static String format(java.util.Date date, String pattern) {
		  
		String string = "";
		if (date != null) {
		  SimpleDateFormat dateFormat = new SimpleDateFormat(pattern);
		  string = dateFormat.format(date);
		}
		
		return string;
	  }

	  public static String format(java.util.Date date) {
		return format(date, "M/d/yyyy");
	  }

	  public static String format(java.sql.Date date, String pattern) {
		return format(utilDate(date), pattern);
	  }

	  public static java.util.Date parse(String value) {
		
		java.util.Date date = null;
		if (value != null) {
		  DateFormat format = DateFormat.getDateInstance(DateFormat.SHORT, Locale.US);
		  try {
			date = format.parse(value);
		  }
		  catch(ParseException e) {
//			  LOGGER.error(CodeHelper.getStackTrace(e));
		  }
		}
		
		return date;
	  }

	  public static java.util.Date parse(String value, String pattern) {
		  
		
		java.util.Date date = null;
		if (value != null) {
		  SimpleDateFormat format = new SimpleDateFormat(pattern);
		  format.setLenient(false); 
		  try {
			date =format.parse(value);
		  }
		  catch(ParseException e) {
//			  LOGGER.error(CodeHelper.getStackTrace(e));
		  }
		}
		
		return date;
	  }
		
	  	
		
	  public static boolean isDate(String value) {
		  
		if (parse(value) == null) {
			return false;
		}
		else {
		 return true;
		}
	  }
	  
	  public static boolean isDate(String value,String pattern) {
		  
		if (parse(value,pattern) == null) {
			return false;
		}
		else {
			return true;
		}
	  }

	  public static java.util.Date add(java.util.Date date, int field, int amount) {
		  
		  
		java.util.Date newDate = null;
		if (date != null) {
		  GregorianCalendar calendar = new GregorianCalendar();
		  calendar.setTime(date);
		  calendar.add(field, amount);
		  newDate = calendar.getTime();
		}
		
		return newDate;
	  }

	  public static java.util.Date utilDate(java.sql.Date date) {
		  
		  
		java.util.Date newDate = null;
		if (date != null) {
		  newDate = new java.util.Date(date.getTime());
		}
		
		return newDate;
	  }

	  public static java.sql.Date sqlDate(java.util.Date date) {
		  
		java.sql.Date newDate = null;
		if (date != null) {
		  newDate = new java.sql.Date(date.getTime());
		}
		
		return newDate;
	  }

	public static java.sql.Timestamp sqlTimestamp(java.util.Date date) {
		
		
		java.sql.Timestamp newDate = null;
	  if (date != null) {
		newDate = new java.sql.Timestamp(date.getTime());
	  }
	  
	  return newDate;
	}

	  public static java.util.Date parse(String day, String mon, String year) {
		  
		  
		java.util.Date newDate = null;
		if ( (day != null) && (mon != null) && (year != null)) {
		  DateFormat format = DateFormat.getDateInstance(DateFormat.SHORT, Locale.US);
		  try {
			newDate = format.parse(mon + "/" + day + "/" + year);
		  }
		  catch(ParseException e) {
//			  LOGGER.error(CodeHelper.getStackTrace(e));
		  }
		}
		
		return newDate;
	  }

	  public static int[] parse(java.util.Date date) {
		  
		  
		int result[] = new int[3];
		if (date != null) {
		  String temp = format(date);
		  int index = temp.indexOf("/");
		  result[0] = Integer.parseInt(temp.substring(0,index));
		  int index2 = temp.lastIndexOf("/");
		  result[1] = Integer.parseInt(temp.substring(index + 1, index2));
		  result[2] = Integer.parseInt(temp.substring(index2 + 1,temp.length()));
		}
		
		return result;
	  }


	public static int computeAge(java.util.Date date) {
		
		
		// Create a calendar object with the date of birth
		Calendar birthDate = Calendar.getInstance();
		birthDate.setTime(date);
	    
		// Create a calendar object with today's date
		Calendar today = Calendar.getInstance();
	    
		// Get age based on year
		int age = today.get(Calendar.YEAR) - birthDate.get(Calendar.YEAR);
	    
		// Add the tentative age to the date of birth to get this year's birthday
		birthDate.add(Calendar.YEAR, age);
	    
		// If this year's birthday has not happened yet, subtract one from age
		if (today.before(birthDate)) {
			age--;
		}
		
		return (age);
	}

	public static int compare(Date date1, Date date2) {
		
		
		if (date1 == null && date2 == null) return 0;
		if (date1 != null && date2 == null) return 1;
		if (date1 == null && date2 != null) return -1;
		
		Date dateA = DateHelper.parse(DateHelper.format(date1));
		Date dateB = DateHelper.parse(DateHelper.format(date2));
		
		
		return dateA.compareTo(dateB);
	}
	
	/**
	 * 
	 * @param datePart	- date in dd-MON-yyyy format
	 * @param type		- determines which time part to add
	 * @return
	 */
	public static String addTimePart(String datePart, int type) {
		
		
		String newDate = new String(datePart);
		if (type == ADD_START_DAY_TIME) {	
			newDate += START_DAY_TIME;
		}
		else { 
			newDate += END_DAY_TIME;
		}
		
		return (newDate);
	}
	

	public static String getCurrentDate() {
		
		String currentDate = (DateHelper.format(new Date(), "ddMMMyyyy")).toUpperCase();
		
		return (currentDate);
	}
	
	
	public static long getDaysDifference(Date fromDate, Date toDate){
		
		
		long milliseconds = fromDate.getTime() - toDate.getTime();
		long seconds = milliseconds / 1000L;
		long minutes = seconds / 60L;
		long hours = minutes / 60L;
		long days = hours / 24L;
		long months = days/31L;
		
		return days;
	}

	
}