//Andre Ceasar Dacanay - Summary and Policy Coverage ING retrieval
package ph.com.sunlife.wms.ium.ui;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import org.apache.commons.beanutils.BeanComparator;
import org.apache.commons.collections.comparators.ComparatorChain;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ph.com.sunlife.wms.ium.data.NameValuePair;
import ph.com.sunlife.wms.ium.service.impl.IngeniumService;
import ph.com.sunlife.wms.ium.ui.form.PolicyCoverageForm;
import ph.com.sunlife.wms.ium.util.DateHelper;
import ph.com.sunlife.wms.modules.DateUtil;
import ph.com.sunlife.wms.ws.ingenium.IngeniumConstants;
import com.slocpi.ium.ingenium.transaction.response.ClientDataResponse;
import com.slocpi.ium.ingenium.transaction.response.detail.ExistingPolicyInformation;
import com.slocpi.ium.ingenium.transaction.response.detail.RelPolicyInformation;
import com.slocpi.ium.ui.form.CDSSummaryForm;
import com.slocpi.ium.util.CodeHelper;

public class CdsPopulatorx {
	private static final Logger LOGGER = LoggerFactory.getLogger(CdsPopulatorx.class);
	private NumberFormat nf;
	
	public CdsPopulatorx() {
		nf = NumberFormat.getInstance();
	}

	private void doubleFormat() {
		LOGGER.info("doubleFormat start");
		nf.setMinimumFractionDigits(2);
		nf.setMaximumFractionDigits(2);
		LOGGER.info("doubleFormat end");
	}
	
	public ClientDataResponse getCdResponse(String policyNumber, String policySuffix) {
		
		LOGGER.info("getCdResponse start");
		
		IngeniumService ingService = new IngeniumService(); 
		ClientDataResponse cdResponse = ingService.getClientDataResponse(policyNumber.concat(policySuffix));
		
		LOGGER.info("getCdResponse end");
		return cdResponse;
	}

	public List populateImpairmentConfirmList() {
		
		LOGGER.info("populateImpairmentConfirmList start");
		
		List confirmList = new ArrayList();
		confirmList.add(new NameValuePair("R", "R"));
		confirmList.add(new NameValuePair("V", "V"));
		
		LOGGER.info("populateImpairmentConfirmList end");
		return confirmList;
	}
	
	public CDSSummaryForm populateCdsSummaryForm(ExistingPolicyInformation existPolInfo) {
		
		LOGGER.info("populateCdsSummaryForm start");
		CDSSummaryForm cdsForm = new CDSSummaryForm();
		doubleFormat();
		
		double totalSunlifeBr = getTotalSunlifeBr(existPolInfo.getAppliedForBR(), existPolInfo.getPendingBRAmount(), existPolInfo.getExistingBR());
		cdsForm.setTotalSunLifeBr(nf.format(totalSunlifeBr));
		double totalSunlifeAd = getTotalSunlifeAd(existPolInfo.getAppliedForAD(), existPolInfo.getPendingADAmount(), existPolInfo.getExistingAD());
		cdsForm.setTotalSunLifeAd(nf.format(totalSunlifeAd));
		double totalSunlifeOtherCompanies = getTotalSunlifeOtherCompanies(totalSunlifeBr, existPolInfo.getOtherInsurerBR());
		cdsForm.setTotalSunLifeOtherCompaniesBr(nf.format(totalSunlifeOtherCompanies));
		double totalSunLifeOtherCompaniesAd = getTotalSunLifeOtherCompaniesAd(totalSunlifeAd, existPolInfo.getOtherInsurerAD());
		cdsForm.setTotalSunLifeOtherCompaniesAd(nf.format(totalSunLifeOtherCompaniesAd));
		cdsForm.setOtherCompaniesBr(nf.format(validate(existPolInfo.getOtherInsurerBR())));
		cdsForm.setOtherCompaniesAd(nf.format(validate(existPolInfo.getOtherInsurerAD())));
		cdsForm.setAppliedForBr(nf.format(validate(existPolInfo.getAppliedForBR())));
		cdsForm.setAppliedForAd(nf.format(validate(existPolInfo.getAppliedForAD())));
		cdsForm.setPendingAmountBr(nf.format(validate(existPolInfo.getPendingBRAmount())));
		cdsForm.setPendingAmountAd(nf.format(validate(existPolInfo.getPendingADAmount())));
		cdsForm.setExistingInForceBr(nf.format(validate(existPolInfo.getExistingBR())));
		cdsForm.setExistingInForceAd(nf.format(validate(existPolInfo.getExistingAD())));
		cdsForm.setTotalCCRCoverage(nf.format(validate(existPolInfo.getTotalCCRCoverage())));
		cdsForm.setTotalAPDBCoverage(nf.format(validate(existPolInfo.getTotalAPDBCoverage())));
		cdsForm.setTotalHIBCoverage(nf.format(validate(existPolInfo.getTotalHIBCoverage())));
		cdsForm.setTotalFBBMBCoverage(nf.format(validate(existPolInfo.getTotalFMBCoverage())));
		cdsForm.setTotalReinsuredAmount(nf.format(validate(existPolInfo.getTotalReinsuredAmount())));
		cdsForm.setTotalAdbReinsuredAmount(nf.format(validate(existPolInfo.getTotalAdbReinsuredAmount())));
		
		LOGGER.info("populateCdsSummaryForm end");
		return cdsForm;
	}

	public ArrayList populatePolicyCoverageList(List relPolicyInformationList, ExistingPolicyInformation existPolInfo) {
		
		LOGGER.info("populatePolicyCoverageList start");
		ArrayList policyCoverageList = new ArrayList();

		doubleFormat();
		if (relPolicyInformationList != null && existPolInfo != null) {
			for (int i=0; i < relPolicyInformationList.size(); i++) {
				RelPolicyInformation rpi = (RelPolicyInformation) relPolicyInformationList.get(i);
				PolicyCoverageForm pcf = new PolicyCoverageForm();
				pcf.setPolicyNo(rpi.getRelPolicyId());
				pcf.setCoverageNo(rpi.getRelCvgNumber());
				pcf.setRelationship(rpi.getRelTypCode());
				
				if(DateHelper.parse(rpi.getRelCvgIssEffDate())!=null || rpi.getRelCvgIssEffDate()!=null){
					pcf.setIssueDate(DateHelper.format(DateHelper.parse(rpi.getRelCvgIssEffDate(), "yyyy-MM-dd"), "ddMMMyyyy"));
					pcf.setIssueDateForSort(DateHelper.parse(rpi.getRelCvgIssEffDate(), "yyyy-MM-dd"));
				}else{
					pcf.setIssueDate(DateHelper.format(DateHelper.parse(DateUtil.ConvertDate(new Date(), "yyyy-MM-dd"), "yyyy-MM-dd"), "ddMMMyyyy"));
					pcf.setIssueDateForSort(DateHelper.parse(DateUtil.ConvertDate(new Date(), "yyyy-MM-dd"), "yyyy-MM-dd"));
				}
				pcf.setCoverageStatus(rpi.getRelCvgCStatCode());
				pcf.setCoverageStatusDesc(IngeniumConstants.getStatusDesc(IngeniumConstants.POLICY_STATUS_.concat(pcf.getCoverageStatus())));
				pcf.setSmokerInd(rpi.getRelCvgSmokerCode());
				pcf.setPlanCode(rpi.getRelPlanId());
				pcf.setMedicalInd(rpi.getRelEvidenceEvtyCode());
				pcf.setFaceAmount(nf.format(validate(rpi.getRelCvgFaceAmount())));
				pcf.setFaceAmountDouble(validate(rpi.getRelCvgFaceAmount()));
				pcf.setDecision(rpi.getRelCvgUwdeSubcode());
				pcf.setADBFaceAmount(nf.format(validate(rpi.getRelCvgAdbFaceAmount())));
				pcf.setADMultiplier(rpi.getRelCvgAdMultfct());
				pcf.setWPMultiplier(rpi.getRelCvgWpMultfct());
				pcf.setReinsuredAmount(nf.format(validate(rpi.getRelReinsfaceAmount())));
				pcf.setRelCvgMeFct(nf.format(validate(rpi.getRelCvgMeFct())));
				pcf.setRelCvgFeUpremAmt(nf.format(validate(rpi.getRelCvgFeUpremAmt())));
				pcf.setRelCvgFePermAmt(nf.format(validate(rpi.getRelCvgFePermAmt())));
				pcf.setAdbReinsuredAmount(nf.format(validate(rpi.getAdbFaceAmount())));
				if(DateHelper.format(DateHelper.parse(rpi.getReinstatementDate()), "ddMMMyyyy")!=null || rpi.getReinstatementDate()!=null){
					try{
						pcf.setReinstatementDate(DateHelper.format(DateHelper.parse(rpi.getReinstatementDate()), "ddMMMyyyy"));
					}catch(Exception e){
						pcf.setReinstatementDate(rpi.getReinstatementDate());
						LOGGER.error(CodeHelper.getStackTrace(e));
					}
				}else{
					pcf.setReinstatementDate(DateHelper.format(DateHelper.parse(rpi.getReinstatementDate()), "ddMMMyyyy"));
				}
				
				pcf.setReinsuranceType(rpi.getReinsuranceType());
				try {
					
					String planDesc = "";
					if(planDesc!=null){
						planDesc = planDesc.trim();
					}else{
						planDesc = "Not available.";
					}
					
					planDesc = planDesc.replaceAll("\\t", "");
					planDesc = planDesc.replaceAll("\\n", "");
					pcf.setPlanCodeDesc(planDesc);
					pcf.setReinsuranceSortAmount(pcf.getFaceAmountDouble() - nf.parse(pcf.getReinsuredAmount()).doubleValue());
				} catch (Exception e) {
					LOGGER.error("Exception "+rpi.getRelPlanId());
					LOGGER.error(CodeHelper.getStackTrace(e));
				}				
				policyCoverageList.add(pcf);
			}
			sort(policyCoverageList);
		}
		LOGGER.info("populatePolicyCoverageList end");
		return policyCoverageList;
	}
	
	
	private List sort(List list) {
		
		LOGGER.info("sort start");
		
		ArrayList sortFields = new ArrayList();
		sortFields.add(new BeanComparator("issueDateForSort"));
		sortFields.add(new BeanComparator("policyNo"));
		sortFields.add(new BeanComparator("coverageNo"));
		ComparatorChain multiSort = new ComparatorChain(sortFields);
		Collections.sort(list, multiSort);
		
		LOGGER.info("sort end");
		return list;
	}
	
	private double getTotalSunLifeOtherCompaniesAd(double totalSunlifeAd, String otherInsurerAD) {
		
		LOGGER.info("getTotalSunLifeOtherCompaniesAd start");
		Double totalSunLifeOtherCompaniesAd = new Double(totalSunlifeAd + 
				validate(otherInsurerAD));
		LOGGER.info("getTotalSunLifeOtherCompaniesAd end");
		return totalSunLifeOtherCompaniesAd.doubleValue();
	}

	private double getTotalSunlifeOtherCompanies(double totalSunlifeBr, String otherInsurerBR) {
		
		LOGGER.info("getTotalSunlifeOtherCompanies start");
		
		Double totalSunlifeOtherCompanies = new Double(totalSunlifeBr + 
				validate(otherInsurerBR));
		
		LOGGER.info("getTotalSunlifeOtherCompanies end");
		return totalSunlifeOtherCompanies.doubleValue();
	}

	private double getTotalSunlifeAd(String appliedForAD, String pendingADAmount, String existingAD) {
		
		LOGGER.info("getTotalSunlifeAd start");
		Double totalSunlifeAd = new Double(validate(appliedForAD) + 
				validate(pendingADAmount) + 
				validate(existingAD));
		LOGGER.info("getTotalSunlifeAd end");
		return totalSunlifeAd.doubleValue();
	}

	private double getTotalSunlifeBr(String appliedForBR, String pendingBRAmount, String existingBR) {
		
		LOGGER.info("getTotalSunlifeBr start");
		
		Double totalSunlifeBr = new Double(validate(appliedForBR) + 
				validate(pendingBRAmount) + 
				validate(existingBR));
		
		LOGGER.info("getTotalSunlifeBr end");
		return totalSunlifeBr.doubleValue();
	}
	
	private double validate(String number) {
		
		LOGGER.info("validate start");
		if (number == null || "".equals(number)) {
			return 0;
		}
		LOGGER.info("validate end");
		return Double.parseDouble(number);
	}

}
