/**
 * Converter.java				Copyright (c) 2004 Pointwest Technologies Corp.
 * TODO DOCUMENT ME!
 * 
 * @author aditalo	 @date Jan 6, 2004
 */
package ph.com.sunlife.wms.ium.util;

import java.math.BigDecimal;
import java.util.StringTokenizer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * TODO DOCUMENT ME!
 * 
 * @author aditalo		Copyright (c) 2004 Pointwest Technologies Corp.
 * @date Jan 6, 2004
 */
public class ValueConverter {
	private static final Logger LOGGER = LoggerFactory.getLogger(ValueConverter.class);
	
	public static String booleanToString(boolean b) {
		
		LOGGER.info("booleanToString start");
		if (b) {
			LOGGER.info("booleanToString end");
			return "Y";
		}else{
			LOGGER.info("booleanToString end");
			return "N";
		}
		
	}
	
	public static boolean stringToBoolean(String string) {
		
		LOGGER.info("stringToBoolean start");
		if (string != null && string.equalsIgnoreCase("Y")) {
			LOGGER.info("stringToBoolean end");
			return true;
		} else {
			LOGGER.info("stringToBoolean end");
			return false;
		}
	}
	
	public static double convertCurrency(String numberWithCommas){
		
		LOGGER.info("convertCurrency start");
		String value = "";
		if(numberWithCommas!=null && numberWithCommas.length()>0){
		
			StringTokenizer token = new StringTokenizer(numberWithCommas,",");
			
			while(token.hasMoreTokens()){
				value = value+ token.nextToken();	
			}
		}
		if (value.equals("")){
			LOGGER.info("convertCurrency end");
			return 0d;
		}else{
			LOGGER.info("convertCurrency end");
			return Double.parseDouble(value);
		}
		
	}
	
	public static String nullToString(String s) {
		
		LOGGER.info("nullToString start");
		if (s == null) {
			s = "";
		}
		LOGGER.info("nullToString end");
		return (s);
	}

	/**
	 * @param elapsedTimeLob
	 * @return
	 */
	public static BigDecimal convertToHours(BigDecimal value)
	{
		LOGGER.info("convertToHours start");
		BigDecimal result = new BigDecimal(0);
		BigDecimal hoursInADay = new BigDecimal(8);
		if (value != null) {
			if (!value.equals(result)) {
				result = value.multiply(hoursInADay);
			}
		}
		LOGGER.info("convertToHours end");
		return (result);
	}

	/**
	 * @param decimal
	 * @return
	 */
	public static BigDecimal nullToZero(BigDecimal decimal)
	{
		LOGGER.info("nullToZero start");
		BigDecimal result = new BigDecimal(0);
		if (decimal != null) result = decimal;
		LOGGER.info("nullToZero end");
		return (result);
	}

    /**
     * 
     * @author Nic Decapia
     *
     */
	public static String newLineToHTML(String arg) {
		
		LOGGER.info("newLineToHTML start");
		String result = "";
		StringTokenizer token = new StringTokenizer(arg, "\n");
		while (token.hasMoreTokens()) {
			result += token.nextToken() + " <br>";
		}
		LOGGER.info("newLineToHTML end");
		return (result); 
	}
	    		
}
