package ph.com.sunlife.wms.ium.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ResourceBundle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ph.com.sunlife.wms.ws.ingenium.IngeniumDispatcher;
import com.slocpi.ium.ingenium.transaction.Transaction;
import com.slocpi.ium.ingenium.transaction.TransactionBuilder;
import com.slocpi.ium.ingenium.transaction.response.ClientDataResponse;
import com.slocpi.ium.ingenium.transaction.response.detail.RelPolicyInformation;
import com.slocpi.ium.util.CodeHelper;

public class IngeniumService {

	private static final Logger LOGGER = LoggerFactory.getLogger(IngeniumService.class);
	private ResourceBundle rb = ResourceBundle.getBundle("ingenium");
	private String user = rb.getString("ingenium.userid");
	private String pwd = rb.getString("ingenium.password");

	public ClientDataResponse getClientDataResponse(String policyNumber, String policySuffix) {
		
		LOGGER.info("getClientDataResponse start 1");
		
		Transaction transaction = TransactionBuilder.buildClientDataTransaction(policyNumber, policySuffix);
		ClientDataResponse response = (ClientDataResponse) transaction.execute();
		
		LOGGER.info("getClientDataResponse end 1");
		return response;
	}
	
	public ClientDataResponse getClientDataResponse(String policyNo) {
		
		LOGGER.info("getClientDataResponse start 2");
		
		String moreIndElem = "MirMoreInd";
		String lastPolIdElem = "MirLastPolId";
		String lastCvgNumElem = "MirLastCvgNum";
		String moreInd = null;
		String lastPolId = null;
		String lastCvgNum = null;
		
		IngeniumDispatcher igDispatcher = new IngeniumDispatcher();
		igDispatcher.IngeniumInit(user, pwd);

		String response = igDispatcher.getCDSInquiry(policyNo); 
		ClientDataResponse resp = new ClientDataResponse();
			resp.parseCdsInquiry(response);
			
		String responseRPCList = igDispatcher.getCDSRPCListInquiry(policyNo, "", "");
		resp.parseCdsRPCListInquiry(responseRPCList);
		List RelatedPolicyList = resp.getRelatedPolicyList();
		 resp.setRelatedPolicyList(RelatedPolicyList);			
			
		try{
			 	moreInd = responseRPCList.substring(responseRPCList.indexOf("<"+moreIndElem+">") + moreIndElem.length()+2, responseRPCList.indexOf("</"+moreIndElem+">"));
			 	
			 	while(moreInd!=null && moreInd.equalsIgnoreCase("Y")){
					 
					 	moreInd = "N";
					 	lastPolId = responseRPCList.substring(responseRPCList.indexOf("<"+lastPolIdElem+">") + lastPolIdElem.length()+2, responseRPCList.indexOf("</"+lastPolIdElem+">"));
					 	lastCvgNum = responseRPCList.substring(responseRPCList.indexOf("<"+lastCvgNumElem+">") + lastCvgNumElem.length()+2, responseRPCList.indexOf("</"+lastCvgNumElem+">"));
					 	resp = new ClientDataResponse();
						resp.parseCdsInquiry(response);
					 	responseRPCList = "";
					 	responseRPCList = igDispatcher.getCDSRPCListInquiry(policyNo, lastPolId, lastCvgNum);
						resp.parseCdsRPCListInquiry(responseRPCList);
						
						List tmpRelatedPolicyList = new ArrayList();
						tmpRelatedPolicyList = resp.getRelatedPolicyList();
						if(resp.getRelatedPolicyList()!=null && resp.getRelatedPolicyList().size()>0){
							Iterator it = tmpRelatedPolicyList.iterator();
							RelPolicyInformation relPolicy = null;
							while(it.hasNext()){
								relPolicy = new RelPolicyInformation();
								relPolicy =(RelPolicyInformation)it.next();
								RelatedPolicyList.add(relPolicy);
							}

						}
						resp.setRelatedPolicyList(RelatedPolicyList);
						
					 	moreInd = responseRPCList.substring(responseRPCList.indexOf("<"+moreIndElem+">") + moreIndElem.length()+2, responseRPCList.indexOf("</"+moreIndElem+">"));
					 	
				 }
		}catch(Exception e){
			LOGGER.error(CodeHelper.getStackTrace(e));
			
		}
			
		LOGGER.info("getClientDataResponse end 2");
		return resp;
	}
	
	public String getClientDataResponseText(String policyNo) {
		
		LOGGER.info("getClientDataResponseText start");
		
		IngeniumDispatcher igDispatcher = new IngeniumDispatcher();
		igDispatcher.IngeniumInit(user);
		String response = igDispatcher.getCDSRPCListInquiry(policyNo, "", "");
		
		LOGGER.info("getClientDataResponseText end");
		return response;
	}
		
}
