package ph.com.sunlife.wms.ium.ui.form;


public class CDSSummaryForm {

	private String appliedForBr;
	private String appliedForAd;
	private String pendingAmountBr;
	private String pendingAmountAd;
	private String existingInForceBr;
	private String existingInForceAd;
	private String existingLapsedBr;
	private String existingLapsedAd;
	private String totalExistingSunLifeBr;
	private String totalExistingSunLifeAd;
	private String totalSunLifeBr;
	private String totalSunLifeAd;	
	private String otherCompaniesBr;
	private String otherCompaniesAd;
	private String totalSunLifeOtherCompaniesBr;
	private String totalSunLifeOtherCompaniesAd;
	private String totalCCRCoverage;
	private String totalAPDBCoverage;
	private String totalHIBCoverage;
	private String totalFBBMBCoverage;
	private String totalReinsuredAmount;
	private String totalAdbReinsuredAmount;
	
	public String getTotalAdbReinsuredAmount() {
		return totalAdbReinsuredAmount;
	}

	public void setTotalAdbReinsuredAmount(String totalAdbReinsuredAmount) {
		this.totalAdbReinsuredAmount = totalAdbReinsuredAmount;
	}

	/**
	 * @return
	 */
	public String getAppliedForAd() {
		return appliedForAd;
	}

	/**
	 * @return
	 */
	public String getAppliedForBr() {
		return appliedForBr;
	}

	/**
	 * @return
	 */
	public String getExistingInForceAd() {
		return existingInForceAd;
	}

	/**
	 * @return
	 */
	public String getExistingInForceBr() {
		return existingInForceBr;
	}

	/**
	 * @return
	 */
	public String getExistingLapsedAd() {
		return existingLapsedAd;
	}

	/**
	 * @return
	 */
	public String getExistingLapsedBr() {
		return existingLapsedBr;
	}

	/**
	 * @return
	 */
	public String getOtherCompaniesAd() {
		return otherCompaniesAd;
	}

	/**
	 * @return
	 */
	public String getOtherCompaniesBr() {
		return otherCompaniesBr;
	}

	/**
	 * @return
	 */
	public String getPendingAmountAd() {
		return pendingAmountAd;
	}

	/**
	 * @return
	 */
	public String getPendingAmountBr() {
		return pendingAmountBr;
	}

	/**
	 * @return
	 */
	public String getTotalAPDBCoverage() {
		return totalAPDBCoverage;
	}

	/**
	 * @return
	 */
	public String getTotalCCRCoverage() {
		return totalCCRCoverage;
	}

	/**
	 * @return
	 */
	public String getTotalExistingSunLifeAd() {
		return totalExistingSunLifeAd;
	}

	/**
	 * @return
	 */
	public String getTotalExistingSunLifeBr() {
		return totalExistingSunLifeBr;
	}

	/**
	 * @return
	 */
	public String getTotalFBBMBCoverage() {
		return totalFBBMBCoverage;
	}

	/**
	 * @return
	 */
	public String getTotalHIBCoverage() {
		return totalHIBCoverage;
	}

	/**
	 * @return
	 */
	public String getTotalReinsuredAmount() {
		return totalReinsuredAmount;
	}

	/**
	 * @return
	 */
	public String getTotalSunLifeAd() {
		return totalSunLifeAd;
	}

	/**
	 * @return
	 */
	public String getTotalSunLifeBr() {
		return totalSunLifeBr;
	}

	/**
	 * @return
	 */
	public String getTotalSunLifeOtherCompaniesAd() {
		return totalSunLifeOtherCompaniesAd;
	}

	/**
	 * @return
	 */
	public String getTotalSunLifeOtherCompaniesBr() {
		return totalSunLifeOtherCompaniesBr;
	}

	/**
	 * @param string
	 */
	public void setAppliedForAd(String string) {
		appliedForAd = string;
	}

	/**
	 * @param string
	 */
	public void setAppliedForBr(String string) {
		appliedForBr = string;
	}

	/**
	 * @param string
	 */
	public void setExistingInForceAd(String string) {
		existingInForceAd = string;
	}

	/**
	 * @param string
	 */
	public void setExistingInForceBr(String string) {
		existingInForceBr = string;
	}

	/**
	 * @param string
	 */
	public void setExistingLapsedAd(String string) {
		existingLapsedAd = string;
	}

	/**
	 * @param string
	 */
	public void setExistingLapsedBr(String string) {
		existingLapsedBr = string;
	}

	/**
	 * @param string
	 */
	public void setOtherCompaniesAd(String string) {
		otherCompaniesAd = string;
	}

	/**
	 * @param string
	 */
	public void setOtherCompaniesBr(String string) {
		otherCompaniesBr = string;
	}

	/**
	 * @param string
	 */
	public void setPendingAmountAd(String string) {
		pendingAmountAd = string;
	}

	/**
	 * @param string
	 */
	public void setPendingAmountBr(String string) {
		pendingAmountBr = string;
	}

	/**
	 * @param string
	 */
	public void setTotalAPDBCoverage(String string) {
		totalAPDBCoverage = string;
	}

	/**
	 * @param string
	 */
	public void setTotalCCRCoverage(String string) {
		totalCCRCoverage = string;
	}

	/**
	 * @param string
	 */
	public void setTotalExistingSunLifeAd(String string) {
		totalExistingSunLifeAd = string;
	}

	/**
	 * @param string
	 */
	public void setTotalExistingSunLifeBr(String string) {
		totalExistingSunLifeBr = string;
	}

	/**
	 * @param string
	 */
	public void setTotalFBBMBCoverage(String string) {
		totalFBBMBCoverage = string;
	}

	/**
	 * @param string
	 */
	public void setTotalHIBCoverage(String string) {
		totalHIBCoverage = string;
	}

	/**
	 * @param string
	 */
	public void setTotalReinsuredAmount(String string) {
		totalReinsuredAmount = string;
	}

	/**
	 * @param string
	 */
	public void setTotalSunLifeAd(String string) {
		totalSunLifeAd = string;
	}

	/**
	 * @param string
	 */
	public void setTotalSunLifeBr(String string) {
		totalSunLifeBr = string;
	}

	/**
	 * @param string
	 */
	public void setTotalSunLifeOtherCompaniesAd(String string) {
		totalSunLifeOtherCompaniesAd = string;
	}

	/**
	 * @param string
	 */
	public void setTotalSunLifeOtherCompaniesBr(String string) {
		totalSunLifeOtherCompaniesBr = string;
	}

}

