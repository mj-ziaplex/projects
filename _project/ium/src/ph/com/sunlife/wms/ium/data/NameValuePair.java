package ph.com.sunlife.wms.ium.data;

public class NameValuePair {
 

	private String name  = "";
	private String value = "";
	private String test;

	/**
	 * Creates a new NameValuePair object.
	 *
	 * @param name DOCUMENT ME!
	 * @param value DOCUMENT ME!
	 */
	public NameValuePair(String name, String value) {
		this.name  = name;
		this.value = value; 
	}

	/**
	 * Constructor NameValuePair.
	 */
	public NameValuePair() {
	}


	/**
	 * Returns the name.
	 * @return <{String}>
	 */
	public String getName() {
		return name;
	}

	/**
	 * Returns the value.
	 * @return <{String}>
	 */
	public String getValue() {
		return value;
	}

	/**
	 * Sets the name.
	 * @param name The name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Sets the value.
	 * @param value The value to set
	 */
	public void setValue(String value) {
		this.value = value;
	}

}