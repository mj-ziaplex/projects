package ph.com.sunlife.wms.ium.ui;


/**
 * This class contains the impairment details.
 * @author: Ma. Theresa Vicencio
 * @version: 1.0
 */
public class ImpairmentForm {

  private String policyNo = "";
  private String code = "";
  private String description = "";
  private String relationship = "";
  private String impairmentOrigin = "";
  private String impairmentDate = "";
  private String heightInFeet = "";
  private String heightInInches = "";
  private String weight = "";
  private String bloodPressure = "";
  private String confirm = "";
  private String exportDate = "";
  private String [] idSelected;
  private String impairmentId = "";
  private String impairmentToBeEdited = "";
  
  //add action code - jonbred
  private String actionCode = "";
  
  /**
   * Sets the action code attribute.
   * @param actionCode reference no. of the application
   */  
  public void setActionCode(String actionCode) {
	this.actionCode = actionCode;
  }

  /**
   * Retrieves the action code attribute.
   * @return String reference no. of the application
   */
  public String getActionCode() {
	return (this.actionCode);
  }
  
  //add action code - jonbred
  
  
  /**
   * Sets the policy number attribute.
   * @param policyNo reference no. of the application
   */  
  public void setPolicyNo(String policyNo) {
	this.policyNo = policyNo;
  }

  /**
   * Retrieves the policy number attribute.
   * @return String reference no. of the application
   */
  public String getPolicyNo() {
	return (this.policyNo);
  }
  
  /**
   * Sets the code attribute.
   * @param code impairment code
   */  
  public void setCode(String code) {
	this.code = code;
  }

  /**
   * Retrieves the code attribute.
   * @return String impairment code
   */
  public String getCode() {
	return (this.code);
  }
    
  /**
   * Sets the description attribute.
   * @param list description description of the impairment
   */	
  public void setDescription(String description) {
    this.description = description;
  }
	
  /**
   * Retrieves the description attribute.
   * @return String description description of the impairment
   */
  public String getDescription(){
    return (this.description);
  }

  /**
   * Sets the relationship attribute.
   * @param relationship code identifying the relationship of the impairment to the insured
   */	
  public void setRelationship(String relationship) {
	this.relationship = relationship;
  }
	
  /**
   * Retrieves the relationship attribute.
   * @return String code identifying the relationship of the impairment to the insured
   */
  public String getRelationship(){
	return (this.relationship);
  }

  /**
   * Sets the impairment origin attribute.
   * @param impairmentOrigin code identifying the cause of the impairment
   */	
  public void setImpairmentOrigin(String impairmentOrigin) {
	this.impairmentOrigin = impairmentOrigin;
  }
	
  /**
   * Retrieves the impairment origin attribute.
   * @return String code identifying the cause of the impairment
   */
  public String getImpairmentOrigin(){
	return (this.impairmentOrigin);
  }

  /**
   * Sets the impairment date attribute.
   * @param impairmentDate date when impairment was discovered or date of the client's operation
   */	
  public void setImpairmentDate(String impairmentDate) {
	this.impairmentDate = impairmentDate;
  }
	
  /**
   * Retrieves the impairment date attribute.
   * @return String date when impairment was discovered or date of the client's operation
   */
  public String getImpairmentDate(){
	return (this.impairmentDate);
  }

  /**
   * Sets the height in feet attribute.
   * @param heightInFeet height in feet
   */	
  public void setHeightInFeet(String heightInFeet) {
	this.heightInFeet = heightInFeet;
  }


  /**
   * Retrieves the height in feet attribute.
   * @return String client's height in feet
   */
  public String getHeightInFeet() {
	  return heightInFeet;
  }

  /**
   * Sets the height in inches attribute.
   * @param heightInInches height in inchess
   */	
  public void setHeightInInches(String heightInInches) {
	this.heightInInches = heightInInches;
  }
  
  /**
   * Retrieves the height in inches attribute.
   * @return String client's height in inchess
   */
  public String getHeightInInches() {
	  return heightInInches;
  }

  /**
   * Sets the weight attribute.
   * @param weight height in inchess
   */	
  public void setWeight(String weight) {
	this.weight = weight;
  }

  /**
   * Retrieves the weight attribute.
   * @return String client's weight in lbs
   */
  public String getWeight() {
	  return weight;
  }

  /**
   * Sets the blood pressure attribute.
   * @param bloodPressure height in inchess
   */	
  public void setBloodPressure(String bloodPressure) {
	this.bloodPressure = bloodPressure;
  }
  
  /**
   * Retrieves the blood pressure attribute.
   * @return String client's blood pressure
   */
  public String getBloodPressure() {
	  return bloodPressure;
  }  


  /**
   * Sets the confirm attribute.
   * @param confirm confirmation
   */	
  public void setConfirm(String confirm) {
	this.confirm = confirm;
  }
	
  /**
   * Retrieves the confirm attribute.
   * @return String confirmation
   */
  public String getConfirm(){
	return (this.confirm);
  }

  /**
   * Sets the export date attribute.
   * @param exportDate timstamp when the record is exported to the MIB database
   */	
  public void setExportDate(String exportDate) {
	this.exportDate = exportDate;
  }
	
  /**
   * Retrieves the export date attribute.
   * @return String timstamp when the record is exported to the MIB database
   */
  public String getExportDate(){
	return (this.exportDate);
  }

/**
 * @return
 */
public String[] getIdSelected() {
	return idSelected;
}

/**
 * @return
 */
public String getImpairmentId() {
	return impairmentId;
}

/**
 * @param strings
 */
public void setIdSelected(String[] strings) {
	idSelected = strings;
}

/**
 * @param string
 */
public void setImpairmentId(String string) {
	impairmentId = string;
}

/**
 * @return
 */

public String getImpairmentToBeEdited() {
	return impairmentToBeEdited;
}

/**
 * @param string
 */

public void setImpairmentToBeEdited(String string) {
	impairmentToBeEdited = string;
}

}

