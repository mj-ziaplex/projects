/**
 * Title:			Profiles
 * Descriptiom:		read/write key-pair values from/to source file <p>
 * Company:			Sun Life Financial<p>
 * @date created:	May 16, 2001 
 * @author Jonas Lim
 * @version 1.0
 */

package ph.com.sunlife.wms.ws.ingenium;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Properties;
import javax.swing.JOptionPane;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.slocpi.ium.util.CodeHelper;


public class Profiles {
	private static final Logger LOGGER = LoggerFactory.getLogger(Profiles.class);
     private String pathName;
     private Properties prop = new Properties();
     private File file;


/**
 * Constructor
 */
	public Profiles() {}
     
/**
 * Constructor with string and boolean parameters
 * @param	pathName, isAbsolute
 *												  
 */
	public Profiles(String pathName, boolean isAbsolute) {
		LOGGER.info("Profiles constructor start");
		if(isAbsolute)
			setAbsolutePath(pathName);
		else
			setRelativePath(pathName);  
		loadFile();
		LOGGER.info("Profiles constructor end");
	}

 /**
 * Set relative path
 *												  
 * @param	relativePath					  
 *												  
 */
	private void setRelativePath(String relativePath) {
		LOGGER.info("setRelativePath start");
		String path = this.getClass().getResource(relativePath).getPath();
		file = new File(path);
		LOGGER.info("setRelativePath end");
	}
	
 /**
 * Set absolute path
 *												  
 * @param	absolutePath
 *												  
 */
	private void setAbsolutePath(String absolutePath) {
		file = new File(absolutePath);
	}	

 /**
 * Load a file specified by its path				              
 *												  
 * @param	path					  
 *												  
 */
	public void loadFile() {
		LOGGER.info("loadFile start");
		try {
			FileInputStream fi = new FileInputStream(file);
			prop.load(fi);
			fi.close();
		} catch(FileNotFoundException ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			JOptionPane.showMessageDialog(null,"FileNotFoundException: " + ex.getMessage());
		} catch(IOException ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			JOptionPane.showMessageDialog(null,"IOException: " + ex.getMessage());
		}
		LOGGER.info("loadFile end");
	}

 /**
 * get the value of the corresponding key; returns null if key not found 
 *												  
 * @param	key 						   			        
 *												  
 * @return	<CODE>String</CODE>							  
 *												  
 */
	public String getValue(String key) {
		return prop.getProperty(key);
	}

 /**
 * get the values of all the keys	      	 				  
 *												  
 *												  
 * @return	<CODE>Enumeration</CODE>						  												 
 */
	public Enumeration getKeys() {
		return prop.propertyNames();
	}

 /**
 * Add a new key-pair value to the file 						  
 *												  
 * @param	key 						   			        
 * @param	value 						   		        
 *												  
 */
	public void putValue(String key, String value) {
		
		LOGGER.info("putValue start");
		if (file.exists()) {
			prop.setProperty(key,value);
			try {
				FileOutputStream fo = new FileOutputStream(pathName);
				prop.store(fo,file.getName());
				fo.close();
			} catch(FileNotFoundException ex) {
				LOGGER.error(CodeHelper.getStackTrace(ex));
				JOptionPane.showMessageDialog(null,"FileNotFoundException: " + ex.getMessage());
			} catch(IOException ex) {
				LOGGER.error(CodeHelper.getStackTrace(ex));
				JOptionPane.showMessageDialog(null,"IOException: " + ex.getMessage());
			}
		} else {
			JOptionPane.showMessageDialog(null,file.getAbsolutePath()+" does not exist");
		}
		LOGGER.info("putValue end");
	}

}
