package ph.com.sunlife.wms.ws.ingenium.soap;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.slocpi.ium.util.CodeHelper;

public class XMLSoapHandler{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(XMLSoapHandler.class);
	private String webRequest;
	
	public XMLSoapHandler(){
		LOGGER.info("initialized XMLSoapHandler");
	}

/*	public void setRequestFile(String fileName){
		
		LOGGER.info("setRequestFile start");
		
        StringBuffer xmlData = new StringBuffer();
        try{
            BufferedReader reader = new BufferedReader(new FileReader(fileName));
            String line = reader.readLine();
            while (line != null){
                xmlData.append(line.trim());
                line = reader.readLine();
            }
        }
        catch (IOException ioe){
        	LOGGER.error(CodeHelper.getStackTrace(ioe));
        }catch(Exception e)
        {
        	LOGGER.error(CodeHelper.getStackTrace(e));
        	e.printStackTrace();
        }
        
        webRequest = xmlData.toString();
        LOGGER.info("setRequestFile end");
	}*/
	
	public String getRequestString(){
		return webRequest;
	}
	
	public void setRequestString(String webRequest){
		this.webRequest = webRequest;        
		
	}
}
