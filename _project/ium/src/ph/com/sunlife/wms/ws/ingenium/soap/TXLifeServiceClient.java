// ******************************************************************************
// THE SOFTWARE COMPRISING THIS SYSTEM IS THE PROPERTY OF INSURANCE SOFTWARE
// SOLUTIONS CORP. OR ITS LICENSORS.
//
// ALL COPYRIGHT, PATENT, TRADE SECRET, AND OTHER INTELLECTUAL PROPERTY RIGHTS
// IN THE SOFTWARE COMPRISING THIS SYSTEM ARE, AND SHALL REMAIN, THE VALUABLE
// PROPERTY OF INSURANCE SOFTWARE SOLUTIONS CORP. OR ITS LICENSORS.
//
// USE, DISCLOSURE, OR REPRODUCTION OF THIS SOFTWARE IS STRICTLY PROHIBITED,
// EXCEPT UNDER WRITTEN LICENSE FROM INSURANCE SOFTWARE SOLUTIONS CORP. OR ITS
// LICENSORS.
// 
// (C) COPYRIGHT 2003 INSURANCE SOFTWARE SOLUTIONS CORP. ALL RIGHTS RESERVED
//******************************************************************************

package ph.com.sunlife.wms.ws.ingenium.soap;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/**
 * A sample client for TXLife web service exposed by ELINKS Web Services.
 */
public class TXLifeServiceClient
{
    /**
     * Entry point for running this client.
     * @param args the arguments passed into this method.
     */
    public static void main(String[] args)
    {
        try
        {

            TXLifeService TXLifeService = new TXLifeService_Impl();
            TXLifeServicePort myPort = TXLifeService.getTXLifeService();

            StringBuffer xmlData = new StringBuffer();

            String inputFileName = null;

            if (args.length == 1)
            {
                inputFileName = args[0];
            }

            if (inputFileName == null)
            {
                System.exit(0);
            }

            try
            {
                // get new file reader

                BufferedReader reader = new BufferedReader(new FileReader(inputFileName));

                String line = reader.readLine();
                while (line != null)
                {
                    xmlData.append(line.trim());
                    line = reader.readLine();
                }
            }
            catch (IOException ioe)
            {
                
                System.exit(1);
            }



            String inputDoc = xmlData.toString();

            String request = inputDoc;

            String response = myPort.callTXLife(request);

            
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }
}