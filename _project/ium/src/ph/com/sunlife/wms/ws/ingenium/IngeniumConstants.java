/*******************************************************************************
 * Copyright (c) 2005, 2006 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/*
 * IngeniumConstants.java
 *
 * Created on June 16, 2006, 10:30 AM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package ph.com.sunlife.wms.ws.ingenium;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author pm13
 */
public class IngeniumConstants {
	private static final Logger LOGGER = LoggerFactory.getLogger(IngeniumConstants.class);
	/**
	 * List of all the constants that will be used Sorted alphabetically
	 */
	public static final String ADDRESS_1 = "MirCliAddrLn1TxtT";

	public static final String ADDRESS_2 = "MirCliAddrLn2TxtT";

	public static final String ADDRESS_3 = "MirCliAddrLn3TxtT";

	public static final String ADDRESS_4 = "MirDvCliAddrLn4TxtT";

	public static final String ADDRESS_5 = "MirDvCliAddrLn5TxtT";

	public static final String ADDRESS_6 = "MirCliAddrLn6TxtT";

	public static final String AGENT_CODE = "MirServAgtId";

	public static final String SERVC_AGENT_CLIENT_CODE = "MirServAgtCliId";

	public static final String BRANCH = "MirServAgtBrNm";

	public static final String ADDRESS_3_PREVIOUS = "MirDvCliAddrLn3TxtT";

	public static final String AGENT_NAME = "MirDvAgtCliNmT";

	public static final String SERVICING_AGENT_NAME = "MirDvServAgtCliNm";

	public static final String CLIENT_BDAY = "MirCliBthDt";

	public static final String BENEFICIARY_NAME = "MirBnfyNmT";

	public static final String CITY = "MirCliCityNmTxtT";

	public static final String CITY_PREVIOUS = "MirDvCliCityNmTxtT";

	public static final String CLIENT_FIRSTNAME = "MirEntrGivNmT";

	public static final String CLIENT_LASTNAME = "MirEntrSurNmT";

	public static final String CLIENT_MIDDLENAME = "MirCliIndvMidNmT";

	public static final String CLIENT_NO = "MirCliIdT";
	
	public static final String CLIENT_FLAG_INDICIA = "MirCliFlgTypCd";
	
	public static final String CLIENT_FLAG_PERSON = "MirCliPersnInd";

	public static final String CLIENT_NUMBER = "MirCliId";

	public static final String COUNTRY_CODE = "MirCliCtryCdT";

	public static final String COUNTRY_CODE_PREVIOUS = "MirDvCliCtryCdT";

	public static final String COVERAGE_NUMBER = "MirCvgNum";

	public static final String COVERAGE_NUMBER_T = "MirCvgNumT";

	public static final String FACE_AMOUNT = "MirCvgFaceAmt";

	public static final String INDEX_NUMBER = "MirIndexNum";

	public static final String INDEX_NUMBERS = "MirIndexNumT";

	public static final String INSURED_CLIENT_NO = "MirInsrdCliIdT";

	public static final String INSURED_NAME = "MirDvInsrdCliNmT";

	public static final String ORIGINAL_FACE_AMOUNT = "MirCvgOrigFaceAmtT";

	public static final String OWNER_TITLE = "MirCliIndvTitlTxtT";

	public static final String PLAN_ID = "MirPlanId";

	public static final String PLAN_ID_T = "MirPlanIdT";

	public static final String PROVINCE_CODE = "MirCliCrntLocCdT";

	public static final String PROVINCE_PREVIOUS = "MirDvCliCrntLocCdT";

	public static final String REFUND_AMOUNT = "MirPolOsDisbAmt";

	public static final String REQUIREMENT_CODE = "MirReqirIdT";

	public static final String REQUIREMENT_ID = "MirReqirId";

	public static final String RETURN_CODE = "MirRetrnCd";

	public static final String LSIR_RETURN_CODE = "LsirReturnCd";

	public static final String SEQUENCE_NUMBER = "MirCpreqSeqNum";

	public static final String SEQUENCE_NUMBERS = "MirCpreqSeqNumT";

	public static final String STAT_CODE = "MirCpreqStatCd";

	public static final String STAT_CODES = "MirCpreqStatCdT";

	public static final String USER_MESSAGE = "MirUserMsgTxtT";

	public static final String ZIPCODE = "MirCliPstlCdT";

	public static final String ZIPCODE_PREVIOUS = "MirDvCliPstlCdT";

	public static final String SEX = "MirCliSexCd";

	public static final String RECEIVE_DATE = "MirCvgAppRecvDtT";

	public static final String APPLICATION_STATUS = "MirAppStatCd";

	public static final String COVERAGE_COUNT = "MirPolCvgRecCtr";

	public static final String BENEFIT_CODE = "MirBnfyTypCdT";

	public static final String POLICY_STATUS_DESC = "MirPolCstatDesc";

	public static final String COVERAGE_PLAN_DESC = "MirCvgPlanDescT";

	public static final String CURRENCY_CODE = "MirPolCrcyCd";

	public static final String PREMIUM_SUSPENSE = "MirPolPremSuspAmt";

	public static final String MODE_PREMIUM = "MirPolMpremAmt";

	public static final String MISC_SUSPENSE_AMOUNT = "MirPolMiscSuspAmt";

	public static final String COMPANY_NAME = "MirCliCoEntrNmT";

	public static final String OWNER_ID = "MirCliId";

	public static final String INSURED_ID = "MirInsrdCliIdT";

	public static final String INS_TYPE_CODE = "MirPolInsTypCd";

	public static final String CLEAR_CASE_RESP = "MirCcasMsgRespTxtT";

	public static final String CLIENT_INDV_SURNAME = "MirCliIndvSurNm";

	public static final String MESSAGE_TEXT = "MirMsgTxtT";

	public static final String POLICY_BASE = "MirPolIdBase";

	public static final String PREVIOUS_UPDATE_DATE = "MirDvPrevUpdtDt";

	public static final String MISS_INFO_IND = "MirMissInfoIndT";

	public static final String SEVERITY = "MirUserMsgSevrtyT";

	public static final String CLRCASE_SEQUENCE_NUMBER = "MirCcasMsgSeqNumT";

	public static final String RESULT_CODE = "ResultCode";

	public static final String TC = "tc";

	public static final String COVERAGE_DECISION = "MirCvgUwgdecnCd";

	public static final String APP_SIGN_DATE = "MirPolAppSignDt";

	public static final String BRANCH_CODE = "MirServBrId";

	public static final String POLICY_STATUS = "MirPolCstatCd";

	/*
	 * Start Added by Rommel 'Hackmel' Suarez December 23 2008
	 * 
	 */
	
	
	public static final String HI_OWNER_ID = "MirHiOwnCliId";

	public static final String OWNER_NAME = "MirHiDvOwnCliNm";

	public static final String OWNER_BIRTH_DATE = "MirHiDvOwnCliBthDt";

	public static final String HI_INSURED_ID = "MirHiInsrdCliId";

	public static final String HI_INSURED_NAME = "MirHiDvInsrdCliNm";

	public static final String INSURED_BIRTH_DATE = "MirHiDvInsrdCliBthDt";

	public static final String CURRENT_POLICY_STATUS = "MirHiPolCstatCd";

	public static final String PRODUCT_CODE  = "MirHiPlanId";
	

	public static final String BILLING_TYPE_CODE  = "MirHiPolBillTypCd";

	public static final String POLICY_ISSUE_DATE = "MirHiPolIssEffDt";

	public static final String POLICY_PAID_TO_DATE = "MirHiPolPdToDtNum";

	public static final String AMOUNT_BILLED = "MirHiPolTotBillAmt";

	public static final String PREMIUM_MODE = "MirHiPolBillModeCd";

	public static final String SUNDRY_AMOUNT = "MirHiPolSndryAmt";

	public static final String TRUE_PREMIUM = "MirHiPolTpremAmt";

	public static final String LAST_MODE_PREMIUM = "MirHiPolPrevMpremAmt";

	public static final String CURRENCY = "MirHiPolCrcyCd";

	public static final String SUSPENSE_DETAILS_OUTSTANDING_DISBURSEMENTS = "MirPdPolOsDisbAmt";
	public static final String SUSPENSE_DETAILS_MISCELLANEOUS_SUSPENSE = "MirPdPolMiscSuspAmt";

	
	//MirBiBnfyNmeT
	public static final String BENEFICIARY_INFORMATION_BENEFICIARY_NAME = "MirBiBnfyNmeT";

	public static final String BENEFICIARY_INFORMATION_BENEFICIARY_BIRTHDATE = "MirBiBnfyBrthdtT";

	public static final String BENEFICIARY_INFORMATION_BENEFICIARY_PROCEEDS = "MirBiBnfyPrcedsT";

	public static final String BENEFICIARY_INFORMATION_BENEFICIARY_DESIGNATION = "MirBiBnfyDesgntT";

	public static final String BENEFICIARY_INFORMATION_RELATION_TO_INSURED = "MirBiBnfyRelinsT";

	public static final String BENEFICIARY_INFORMATION_BENEFICIARY_TYPE = "MirBiBnfyTypT";

	
	/**/
	public static final String POLICY_FINANCIAL_ACTIVITY_DEFERRED_ACTIVITY_LIST_FINANCIAL_TRANSACTION_EFFECTIVE_DATE = "MirPolPayoEffDtT";

	public static final String POLICY_FINANCIAL_ACTIVITY_DEFERRED_ACTIVITY_LIST_FINANCIAL_TRANSACTION_TYPE = "MirPolPayoTypCdT";

	public static final String POLICY_FINANCIAL_ACTIVITY_DEFERRED_ACTIVITY_LIST_FINANCIAL_TRANSACTION_GROSS_AMOUNT = "MirDvPolPayoTotAmtT";

	public static final String POLICY_FINANCIAL_ACTIVITY_DEFERRED_ACTIVITY_LIST_FINANCIAL_TRANSACTION_STATUS = "MirPolPayoStatCdT";

	public static final String FUND_TRANSFER_PERCENT_TO_PERCENT_CONFIRMATION_FUND_ID = "MirFndIdT";

	public static final String FUND_TRANSFER_PERCENT_TO_PERCENT_CONFIRMATION_APPROXIMATE_VALUE_IN_FUND_CURRENCY = "MirDvCfnAproxAmtT";

	public static final String FUND_TRANSFER_PERCENT_TO_PERCENT_CONFIRMATION_PERCENTAGE_FROM = "MirFiaOutAllocPctT";

	public static final String FUND_TRANSFER_PERCENT_TO_PERCENT_CONFIRMATION_PERCENTAGE_TO = "MirFiaInAllocPctT";

	public static final String POLICY_ALLOCATION_LIST_ALLOCATION_TYPE_2ND_ROW = "MirCdiTypCdT";

	public static final String POLICY_ALLOCATION_LIST_EFFECTIVE_DATE_2ND_ROW = "MirCdiEffDtT";

	public static final String POLICY_ALLOCATION_LIST_APPLICATION_STATUS_2ND_ROW = "MirCdiStatCdT";

	public static final String POLICY_ALLOCATION_INQUIRY_APPLICATION_STATUS_OF_ALLOCATION_TYPE_SUBSEQUENT_PREMIUM = "MirCdiStatCd";

	public static final String POLICY_ALLOCATION_INQUIRY_ALLOCATION_PERCENTAGE_OF_ALLOCATION_TYPE_SUBSEQUENT_PREMIUM = "MirCdiAllocPctT";

	public static final String POLICY_ALLOCATION_INQUIRY_FUND_DESCRIPTION_OF_ALLOCATION_TYPE_SUBSEQUENT_PREMIUM = "MirDvFndDescTxtT";

	public static final String POLICY_ALLOCATION_INQUIRY_COVERAGE_STATUS_OF_ALLOCATION_TYPE_SUBSEQUENT_PREMIUM = "MirCvgCstatCdT";

	public static final String ASSIGNEE_NAME ="MirDvAsignCliNmT";
	
	public static final String EFFECTIVE_DATE_OF_ASSIGNMENT="MirPolCliAsignDtT";
	public static final String COMMENTS_REMARKS="MirOpiPolComntTxt";
	
	public static final String LOAN_INFORMATION_LOAN_AMOUNT = "MirPvDvLoanAmt";
	public static final String LOAN_INFORMATION_BASE_CASH_VALUE = "MirPvDvMcvCsvAmt";
	public static final String LOAN_INFORMATION_MAXIMUM_ADVANCE_AMOUNT_AVAILABLE = "MirPvDvMaxLoanAmt";
	public static final String OTHER_OPTIONS_NON_FORFEITURE_OPTION = "MirOpiPolNfoCd";

	public static final String OTHER_OPTIONS_PREMIUM_OFFSET_OPTION_STATUS_CODE = "MirPoStatCd";

	public static final String BENEFICIARY_INFORMATION_PERCENTAGE = "MirBiBnfyPrcdsPctT";

	public static final String BENEFICIARY_INSTRUCTIONS = "MirBiBnfyInstr";

	public static final String LOAN_INFORMATION_CASH_SURRENDER_VALUE = "MirPvDvPolCsvAmt";

	public static final String DIVIDEND_INFORMATION_POLICY_DIVIDEND_OPTION = "MirPvPolDivOptCd";

	public static final String DIVIDEND_INFORMATION_DIVIDENDS_ON_DEPOSIT = "MirPvPolDodAcumAmt";

	public static final String PUA_DETAILS_MAXIMUM_PUA_CV_WITHDRAWABLE_AMOUNT = "MirDvMaxWthdrwPuaCvAmt";

	public static final String AE_FUND_DETAILS_AE_FUND = "MirAfdPolAeFndAmt";

	public static final String AE_FUND_DETAILS_AE_OPTION = "MirAfdCdiPayoOptCd";

	public static final String DIVIDEND_INFORMATION_YEAR_LAST_DECLARED = "MirPvDvPrevDivYrQty";
	
	public static final String DIVIDEND_INFORMATION_DIVIDEND_DECLARED = "MirPvDivDclrDurAmt"; // Andre Ceasar Dacanay - Datasnapshot Values

	public static final String DIVIDEND_INFORMATION_MAXIMUM_DOD_WITHDRAWABLE_AMOUNT = "MirDvMaxWthdrwDodAmt";

	public static final String DIVIDEND_INFORMATION_ANNIVERSARY_PROCESSING_YEAR = "MirPvDvAnnvPyrQty";

	public static final String OTHER_OPTIONS_PREMIUM_OFFSET_OPTION = "MirPoOptCd";
	
	public static final String AGENT_INFORMATION_SERVICE_AGENT_CODE = "MirAsaServAgtId";
	
	public static final String AGENT_INFORMATION_SERVICE_AGENT_NAME = "MirAsaDvServAgtCliNm";
	
	public static final String AGENT_INFORMATION_SERVICE_AGENT_STATUS_CODE = "MirAsaAgtStatCd";
	
	public static final String AGENT_INFORMATION_SERVICE_NBO_CODE = "MirAsaServBrId";
	
	public static final String AGENT_INFORMATION_COVERAGE_CODE = "MirCdCvgCstatCdT";
	
	public static final String SMOKER_CODE = "MirCdCvgSmkrCdT";
	
	public static final String POLICY_INFORMATION_PAID_UP_ADD_CASH = "MirPvPuaLtdFaceAmt"; 
	
	public static final String POLICY_VALUES_INFORMATION_PAID_UP_ADD_CASH = "MirDvValuPuaAmt";
	
	public static final String POLICY_INFORMATION_ASSIGNEE = "MirDvAsignCliNmT"; 
	
	
	public static final String NBO_NAME = "NBO_NAME";
	
    public static final String PRODUCT_NAME = "PRODUCT_NAME";
	
	public static final String BILLING_TYPE = "BILLING_TYPE";
		
	public static final String PREMIUM_MODE_DESC = "PREMIUM_MODE_DESC";
	
	public static final String AGENT_INFORMATION_SERVICE_AGENT_STATUS_DESC = "AGENT_INFORMATION_SERVICE_AGENT_STATUS_DESC";
	
	public static final String CURRENT_POLICY_STATUS_DESC = "CURRENT_POLICY_STATUS_DESC";
	
	public static final String PREMIUM_OFFSET_OPTION_STATUS_DESC = "PREMIUM_OFFSET_OPTION_STATUS_DESC";
	
	
	public static final String BENEFICIARY_TYPE_DESC="BENEFICIARY_TYPE_DESC";
	public static final String BENEFICIARY_DESIGNATION_DESC="BENEFICIARY_DESIGNATION_DESC";
	public static final String BENEFICIARY_PROCEEDS_DESC="BENEFICIARY_PROCEEDS_DESC";
	
	
	public static final String POLICY_STATUS_="POLICY_STATUS_";
	public static final String PREMIUM_OFFSET_OPTION_STATUS_="PREMIUM_OFFSET_OPTION_STATUS_";
	public static final String AGENT_STATUS_="AGENT_STATUS_";
	public static final String PREMIUM_MODE_="PREMIUM_MODE_";
	public static final String BILLING_TYPE_="BILLING_TYPE_";
	public static final String BENEFICIARY_TYPE_="BENEFICIARY_TYPE_";
	public static final String BENEFICIARY_DESIGNATION_="BENEFICIARY_DESIGNATION_";
	public static final String BENEFICIARY_PROCEEDS_="BENEFICIARY_PROCEEDS_";
	
	public static final String OTHER_OPTIONS_PREMIUM_OFFSET_OPTION_DESC="OTHER_OPTIONS_PREMIUM_OFFSET_OPTION_DESC";
	
	
	public static final String DIVIDEND_INFORMATION_POLICY_DIVIDEND_OPTION_DESC = "DIVIDEND_INFORMATION_POLICY_DIVIDEND_OPTION_DESC";
	public static final String OTHER_OPTIONS_NON_FORFEITURE_OPTION_DESC ="OTHER_OPTIONS_NON_FORFEITURE_OPTION_DESC";
	public static final String AE_FUND_DETAILS_AE_OPTION_DESC = "AE_FUND_DETAILS_AE_OPTION_DESC";
	
	public static final String OTHER_LEGAL_FRIST_NAME = "MirCliOtherFirstNmT";
	public static final String OTHER_LEGAL_MID_NAME = "MirCliOtherMidNmT";
	public static final String OTHER_LEGAL_LAST_NAME = "MirCliOtherLastNmT";
	public static final String POLICY_INFORMATION_PREMIUM_REFUND = "MirDvPremRfndAmt";
	
	public static final String POLICY_INFORMATION_PREMIUM_SUSPENSE = "MirPdPolPremSuspAmt";
	
	public static final String POLICY_INFORMATION_1 = "MirPvPuaYtdFaceAmt";
	
	public static final String POLICY_INFORMATION_2 = "MirPdPolPdfSuspAmt";
	
	public static final String APP_FUND_AMT = "MirPolPdfAmt";
	public static final String APP_FUND_SUSP_AMT = "MirPolPdfSuspAmt";
	
	
	public static final String POLICY_VALUES_CASH_SURRENDER="MirDvPolCsvAmt";
	
	public static final String COVERAGE_DETAILS_AGE = "MirCdCvgRtAgeT";
	public static final String COVERAGE_DETAILS_SEX_CODE = "MirCdCvgSexCdT";
	public static final String COVERAGE_DETAILS_SMOKER_CODE = "MirCdCvgSmkrCdT";
	public static final String CUMULATIVE_EXCESS_PREMIUM_LIFE_TO_DATE = "MirExcsPremLifeToDtT";
	public static final String CUMULATIVE_SURRENDER_LIFE_TO_DATE = "MirCvgSurrLtdAmtT";
	public static final String ANNUAL_PREMIUM_FOR_COVERAGE = "MirCvgBasicPremAmtT";
	public static final String AMOUNT_OF_ADVANCE = "MirDvLoanAmt";
	public static final String APA_AMOUNT = "MirDvAplLoanAmt";
	public static final String ACCRUED_INTEREST_ON_ADVANCE = "MirDvLoanIntYtdAmt";
	public static final String ACCRUED_INTEREST_ON_APA = "MirDvAplIntYtdAmt";
	
	
	/**
	 * MirPolPremSuspAmt Creates a new instance of IngeniumConstants
	 */
	public IngeniumConstants() {
	}

	public static HashMap getHashMapValue(String key, HashMap hs) {

		LOGGER.info("getHashMapValue initializa");
		
		if (hs.containsKey(key))
			return  (HashMap) hs.get(key);
		
		return new HashMap();
	}

	public static List getHashMapListValue(String key, HashMap hs) {

		LOGGER.info("getHashMapListValue initialize");
		if (hs.containsKey(key))
			return (List) hs.get(key);
		return new ArrayList();
	}
	
	
	
	public static String getListValue(List list){
		
		LOGGER.info("getListValue initialize");
		if(list.size()>0){
			return (String)list.get(0);
			
		}else {
			return "&nbsp;";
		}
		
	}
    
    
	public static String  getStatusDesc(String code){
		
		LOGGER.info("getStatusDesc initialize");
		ResourceBundle rb=ResourceBundle.getBundle("com.ph.sunlife.component.properties.IngeniumCode");
	 try {
		 return rb.getString(code);
			
	 }catch(MissingResourceException e){
		 return "";
	 }
		
	}
}
