/*******************************************************************************
 * Copyright (c) 2005, 2006 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/*
 * Main.java
 *
 * Created on June 6, 2006, 8:05 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package ph.com.sunlife.wms.ws.ingenium;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * 
 * @author pm13
 */
public class Main {
	int i;

	char c;

	/** Creates a new instance of Main */
	public Main() {

	}

	public void hello() {
		System.out.println((int) c);
	}

	/**
	 * @param args
	 *            the command line arguments
	 */
	public static void main(String[] args) {
		Main m = new Main();
		m.hello();

		long time1;
		long time2;

		time1 = System.currentTimeMillis();
		// getClientRequirements
		IngeniumDispatcher igDispatcher = new IngeniumDispatcher();
		
		
		//igDispatcher.IngeniumInit("wrs1");
		igDispatcher.IngeniumInit("WF01");
		//igDispatcher.IngeniumInit("wudx1");
		//igDispatcher.IngeniumInit("wc15");
		
		//igDispatcher.IngeniumInit("fphwmsd1");
		
		//igDispatcher.IngeniumInit("wmf01");
		//igDispatcher.IngeniumInit("wmf02");
		//igDispatcher.IngeniumInit("wmf03");
		//igDispatcher.IngeniumInit("wmf04");
		//igDispatcher.IngeniumInit("wmf05");
		//igDispatcher.IngeniumInit("wmf06");
		//igDispatcher.IngeniumInit("wrs1");
		//igDispatcher.IngeniumInit("ws01");
		//igDispatcher.IngeniumInit("wch2");
		//igDispatcher.IngeniumInit("wch3");
		//igDispatcher.IngeniumInit("ws03");
		//igDispatcher.IngeniumInit("ws05");
	
		
		
		//igDispatcher.IngeniumInit("fphwmsd1");
		// String policyNo = "0515059552";
		// String policyNo = "0500058229";
		// String policyNo = "0500058199";
		// String policyNo = "0800012330";
		// String policyNo = "0500060592";
		// String policyNo = "0515126187";
		 //String policyNo = "RPU01";
		//String policyNo = "0800012372";
		// String policyNo = "0500065780";
		//String policyNo = "0500500115";
		 //String policyNo = "0800500114";
		//String policyNo = "0515136018";  // working with wudx1
		  //String policyNo = "0800500113";
		  //String policyNo="0800500121";
		 //String policyNo = "0500VERROR";
		//String policyNo = "0825038936";  // working with  wrs1
		//String policyNo = "0004ROMMEL";
		
		//String policyNo = "0800500103";
		//String policyNo = "0800022726";
		//String policyNo = "0825038111";
		
		//String policyNo = "0500001189";
		//String policyNo = "0839873980";
		//String policyNo = "0500000166";
		//String policyNo = "0500000700";
		//String policyNo = "0870187015";
 		//String policyNo = "0870187015";
		//String policyNo = "0845964593";
		String policyNo = "0822562251";
		
		
		

		//String clientNo = "3001006375";
		String clientNo = "3003000003";
		HashMap hashmap = new HashMap();
		/*
		 * try{ hashmap =
		 * (HashMap)igDispatcher.createClientRequirement(clientNo, "XRY",
		 * "RAC");
		 * 
		 * }catch (Exception e) { }
		 * 
		 * try{ hashmap = (HashMap)igDispatcher.getClientRequirements(clientNo);
		 * 
		 * }catch (Exception e) { }
		 * 
		 * try{ hashmap =
		 * (HashMap)igDispatcher.getCoverageBeneficiaries(policyNo, "01");
		 * 
		 * }catch (Exception e) { }
		 * 
		 * try{ hashmap =
		 * (HashMap)igDispatcher.getPolicyBeneficiaries(policyNo);
		 * 
		 * }catch (Exception e) { }
		 */
		try {
			
			
			hashmap = (HashMap)igDispatcher.getInquiryPolicyValues(policyNo);
			//igDispatcher.getPolicyInquiry("0500000050") ;
			//hashmap = (HashMap)igDispatcher.getInquiryBilling(policyNo);

			
			//hashmap =(HashMap) igDispatcher.getInquiryULDetails(policyNo, "02");
			//hashmap = (HashMap) igDispatcher.getInquiryConsolidatedInformation("0500000620");
			//hashmap = (HashMap) igDispatcher.getClientDetails(clientNo);
			
			//hashmap = (HashMap) igDispatcher.getInquiryConsolidatedInformation(policyNo);
					
			//hashmap = (HashMap) igDispatcher.getInquiryCoveragePremiums(policyNo, )
			
			//hashmap = (HashMap) igDispatcher.getPolicyAllocationList(policyNo);
			
			//hashmap=igDispatcher.getPolicyAllocationInquiry(policyNo);
			//hashmap = (HashMap) igDispatcher
			//.getFundTransferPercentToPercent(policyNo);
			
			
			//hashmap = (HashMap) igDispatcher.getDeferredActivityList(policyNo);
			
			//hashmap = (HashMap) igDispatcher.getFundTransferPercentToPercent(policyNo);
			
			 //hashmap = (HashMap) igDispatcher
			 //.getPolicyAllocationInquiry(policyNo);
			
			//hashmap =igDispatcher.getPolicyBeneficiaries(policyNo);
			
			//hashmap=igDispatcher.getCoverageBeneficiaries(policyNo,"01");
			//hashmap=igDispatcher.getCoverageBeneficiaries(policyNo,"2");
		} catch (Exception e) {

		}
		/*
		 * try{ hashmap =
		 * (HashMap)igDispatcher.getPolicyCoverageDetails(policyNo, "01");
		 * 
		 * }catch (Exception e) { }
		 * 
		 * try{ hashmap =
		 * (HashMap)igDispatcher.getPolicyInsuredDetails(policyNo, "01");
		 * 
		 * }catch (Exception e) { }
		 * 
		 * try{ hashmap = (HashMap)igDispatcher.getPolicyOwnerDetails(policyNo);
		 * 
		 * }catch (Exception e) { }
		 * 
		 * try{ hashmap =
		 * (HashMap)igDispatcher.getPolicyPaymentDetails(policyNo);
		 * 
		 * }catch (Exception e) { }
		 * 
		 * try{ hashmap = (HashMap)igDispatcher.getPolicyRequirements(policyNo);
		 * 
		 * }catch (Exception e) { }
		 * 
		 * try{ hashmap = (HashMap)igDispatcher.getRequirements(policyNo);
		 * 
		 * 
		 * }catch (Exception e) { } /* try{ hashmap =
		 * (HashMap)igDispatcher.getClientDetails(clientNo); }catch (Exception
		 * e) { }
		 * 
		 * try{ hashmap =
		 * (HashMap)igDispatcher.createPolicyRequirement(policyNo, "A11",
		 * "RAC"); }catch (Exception e) { }
		 * 
		 * try{ hashmap =
		 * (HashMap)igDispatcher.createClientRequirement(clientNo, "A14",
		 * "RAC"); }catch (Exception e) { }
		 * 
		 * try{ hashmap = (HashMap)igDispatcher.triggerAutoApp(policyNo,
		 * clientNo);
		 * 
		 * }catch(Exception e){}
		 * 
		 * try{ hashmap =
		 * (HashMap)igDispatcher.updatePolicyRequirement(policyNo, "004", "RAC",
		 * "A12", "0000000001");
		 * 
		 * }catch(Exception e){} try{ hashmap =
		 * (HashMap)igDispatcher.updateClientRequirement(clientNo, "004", "RAC",
		 * "A12", "0000000001");
		 * 
		 * }catch(Exception e){}
		 * 
		 * try{ hashmap = (HashMap)igDispatcher.CoverageAllDetails(policyNo);
		 * 
		 * }catch(Exception e){}
		 * 
		 * 
		 * 
		 * try{ hashmap = (HashMap)igDispatcher.getPolicyInquiry(policyNo);
		 * 
		 * }catch(Exception e){}
		 * 
		 * try{ hashmap = (HashMap)igDispatcher.createKoReqts(policyNo);
		 * 
		 * }catch(Exception e){} /* hashmap =
		 * (HashMap)igDispatcher.triggerAutoApp("JONATS-001"); hashmap =
		 * (HashMap)igDispatcher.triggerAutoApp("JONATS-002"); hashmap =
		 * (HashMap)igDispatcher.triggerAutoApp("JONATS-003");
		 */
		System.out.println("helloooooooooooooooooooooooooooooooooo 1");

		System.out.println("hashmap is not null :" + (hashmap != null));
		if (hashmap != null) {

			//System.out.println("MirPvPuaLtdFaceAmt:"
				//	+ hashmap.get("SMOKER_CODE"));

			Object strkeys[] = hashmap.keySet().toArray();
			System.out.println("length :" + strkeys.length);
			for (int i = 0; i < strkeys.length; i++) {
				if (hashmap.get(strkeys[i]) instanceof ArrayList) {
					ArrayList al_result = (ArrayList) hashmap.get(strkeys[i]);
					System.out.println("strkeysx :" + strkeys[i] + ": ");
					if (al_result != null)
						for (int j = 0; j < al_result.size(); j++)
							System.out.println("\t" + al_result.get(j));
				} else if (hashmap.get(strkeys[i]) instanceof HashMap) {
					HashMap al_result = (HashMap) hashmap.get(strkeys[i]);
					System.out.println("strkeys :" + strkeys[i] + ": ");
					Object strkeys1[] = al_result.keySet().toArray();
					for (int k = 0; k < strkeys1.length; k++) {
						ArrayList al_result1 = (ArrayList) al_result
								.get(strkeys1[k]);
						System.out.println("\t" + strkeys1[k] + ": ");
						if (al_result1 != null)
							for (int j = 0; j < al_result1.size(); j++)
								System.out.println("\t\t" + al_result1.get(j));
					}
				}
			}

			
			//System.out.println("PRODUCT_NAME:"
				//	+ hashmap.get(IngeniumConstants.PRODUCT_NAME));
			
			System.out.println("CURRENT_POLICY_STATUS:"
					+ hashmap.get("CURRENT_POLICY_STATUS"));
			
			
			
			System.out.println("SMOKER_CODE:"
					+ hashmap.get("SMOKER_CODE"));
			System.out.println("OTHER_OPTIONS_PREMIUM_OFFSET_OPTION xxx:"
					+ hashmap.get("OTHER_OPTIONS_PREMIUM_OFFSET_OPTION"));
			
			
			System.out.println("BILLING_TYPE:"
					+ hashmap.get(IngeniumConstants.BILLING_TYPE));
			
			System.out.println("PREMIUM_MODE_DESC:"
					+ hashmap.get(IngeniumConstants.PREMIUM_MODE_DESC));
			
			System.out.println("AGENT_INFORMATION_SERVICE_AGENT_STATUS_DESC:"
					+ hashmap.get(IngeniumConstants.AGENT_INFORMATION_SERVICE_AGENT_STATUS_DESC));
			
			System.out.println("CURRENT_POLICY_STATUS_DESC:"
					+ hashmap.get(IngeniumConstants.CURRENT_POLICY_STATUS_DESC));
			
			System.out.println("PREMIUM_OFFSET_OPTION_STATUS_DESC:"
					+ hashmap.get(IngeniumConstants.PREMIUM_OFFSET_OPTION_STATUS_DESC));
			
			System.out.println("OTHER_OPTIONS_PREMIUM_OFFSET_OPTION:"
					+ hashmap.get(IngeniumConstants.OTHER_OPTIONS_PREMIUM_OFFSET_OPTION));
			
			
			System.out.println("BENEFICIARY_INFORMATION_BENEFICIARY_DESIGNATION:"
					+ hashmap.get("BENEFICIARY_INFORMATION_BENEFICIARY_DESIGNATION"));
			
			
			
			
			System.out.println("BENEFICIARY_INFORMATION_BENEFICIARY_DESIGNATION desc:" + IngeniumConstants.getStatusDesc(IngeniumConstants.BENEFICIARY_DESIGNATION_ +((List)hashmap.get("BENEFICIARY_INFORMATION_BENEFICIARY_DESIGNATION")).get(0).toString()));
			System.out.println("BENEFICIARY_INFORMATION_BENEFICIARY_PROCEEDS desc:" + IngeniumConstants.getStatusDesc(IngeniumConstants.BENEFICIARY_PROCEEDS_ +((List)hashmap.get("BENEFICIARY_INFORMATION_BENEFICIARY_PROCEEDS")).get(0).toString()));
			
			
			System.out.println("BENEFICIARY_INFORMATION_BENEFICIARY_DESIGNATION desc:"
					+ hashmap.get(IngeniumConstants.BENEFICIARY_DESIGNATION_DESC));
			
			System.out.println("BENEFICIARY_INFORMATION_BENEFICIARY_DESIGNATION desc:"
					+ hashmap.get(IngeniumConstants.BENEFICIARY_PROCEEDS_DESC));
			
			System.out.println("BENEFICIARY_INFORMATION_BENEFICIARY_DESIGNATION desc:"
					+ hashmap.get(IngeniumConstants.BENEFICIARY_TYPE_DESC));
			
			List x=(List)hashmap.get(IngeniumConstants.BENEFICIARY_TYPE_DESC);
			List y=(List)hashmap.get(IngeniumConstants.BENEFICIARY_PROCEEDS_DESC);
			for(int i=0;i<x.size();i++){
				System.out.println("BF: " + x.get(i));
       	    }
			
			
			for(int i=0;i<x.size();i++){
				System.out.println("BF: " + y.get(i));
       	    }
       	 
			
			//System.out.println("BENEFICIARY_INFORMATION_BENEFICIARY_DESIGNATION desc:" + IngeniumConstants.getStatusDesc(IngeniumConstants.BENEFICIARY_DESIGNATION_+ ()
			
			
			
			
			//List x=(List)hashmap.get("SMOKER_CODE");
			//System.out.println("SMOKER_CODE:"
			//		+ x.size());
			

		}

		
		System.out.println("helloooooooooooooooooooooooooooooooooo 2");

		// igDispatcher.getPolicyClientDetails("P-6007-11");
		// igDispatcher.getClientDetails("SA10");
		// igDispatcher.getCoverageBeneficiaries("P-6007-11","00");
		// igDispatcher.getPolicyBeneficiaries("P-6007-11");
		// igDispatcher.getClientRequirements("C-6007-11");

		// igDispatcher.updateClientRequirement_void("AA001","001","","A11","000111");

		// igDispatcher.createPolicyRequirement("CC001", "A12", "RAC");
		// igDispatcher.getRequirements("AA001");
		// igDispatcher.updateClientRequirement("C-6007-21",
		// "001",null,"XRYPL");
		// System.out.println(CLIENT_NO);
		// igDispatcher.getPolicyCoverageDetails("P-6007-11");
		// igDispatcher.getPolicyPaymentDetails("P-6007-11");
		// igDispatcher.getPolicyInsuredDetails("P-6007-11");
		// igDispatcher.triggerAutoApp("P-6007-11");
		// igDispatcher.updatePolicyRequirement("P-6007-11",
		// "001",null,"XRYPL");
		// igDispatcher.getPolicyRequirements("P-6007-11");
		// TODO code application logic here
		// time2 = System.currentTimeMillis();
		// System.out.println("First :"+(time2 - time1));
		// TheLogger.log("start");

	}

}
