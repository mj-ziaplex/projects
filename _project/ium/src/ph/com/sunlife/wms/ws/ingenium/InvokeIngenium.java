/*******************************************************************************
 * Copyright (c) 2005, 2006 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/*
 * InvokeIngenium.java
 * 
 * Created on June 6, 2006, 6:39 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package ph.com.sunlife.wms.ws.ingenium;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.ResourceBundle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.xerces.parsers.SAXParser;
import org.xml.sax.InputSource;
import com.slocpi.ium.ingenium.transaction.Configuration;
import com.slocpi.ium.util.CodeHelper;
import com.solcorp.elink.webservices.TXLifeService;
import com.solcorp.elink.webservices.TXLifeServiceLocator;
import com.solcorp.elink.webservices.TXLifeServicePort;

/**
 *
 * @author pm13
 */
public class InvokeIngenium {
	private static final Logger LOGGER = LoggerFactory.getLogger(InvokeIngenium.class);
	static ResourceBundle rb = ResourceBundle.getBundle("com.ph.sunlife.component.properties.Ingenium");
	
	private String response = "";
    /**
     * Creates a new instance of InvokeIngenium
     */
   IngBusinessProcesses cc; 
    /*
     *  This constructor to class that will invoke Ingenium 
     */   
    public InvokeIngenium() {
    	LOGGER.debug("InvokeIngenium instance created");
    }
    /*
     *  This function automatically performs the marshalling of the object into an xml file
     *  then changes then submits the xml into ingenium and lastly unmarshalls the response
     *  back to the TXLife object.
     */
    
    
    public HashMap submit (String strTXLife, String BussinessProcess) 
    throws Exception{
    	
    	LOGGER.info("submit start");
    	 
    	String isXMLWrite = rb.getString("XMLWrite"); 
    	HashMap hs = null;
        File fileResponseXML = null;
        File fileRequestXML = null;
        long startTime;
        long endTime;
        long totalTime;
        
		WMSLoggerLog w = new WMSLoggerLog("testing");
        try { 
        	long longTime = System.currentTimeMillis();
        	
        	
        	FileWriter fw = null;
        	FileWriter fw1 = null;
        	if(isXMLWrite.equalsIgnoreCase("true")){
        		fileResponseXML = new File(rb.getString("log_dir")+BussinessProcess+"_Response"+ longTime+".xml");
	            fw = new FileWriter(fileResponseXML);
	            fileRequestXML = new File(rb.getString("log_dir")+BussinessProcess+"_Request"+ longTime+".xml");
	            fw1 = new FileWriter(fileRequestXML);
	            fw1.write(strTXLife);
	            fw1.flush();
	            fw1.close();
        	}
        	
            ph.com.sunlife.wms.ws.ingenium.soap.Configuration conf = ph.com.sunlife.wms.ws.ingenium.soap.Configuration.getInstanceOf();
            conf.setHost(rb.getString("Server"));
            
            Configuration config = new Configuration();
            
            String serviceWsdl = config.getServiceWSDL();
            TXLifeService txLifeService = new TXLifeServiceLocator(serviceWsdl, new TXLifeServiceLocator().getServiceName());
            TXLifeServicePort myPort = txLifeService.getTXLifeService();
            startTime = System.currentTimeMillis();
            response = myPort.callTXLife(strTXLife);
            endTime = System.currentTimeMillis();
            
            totalTime = endTime - startTime; 
            w.write(BussinessProcess +" took "+totalTime + " ms", "INFO");
            
            if(isXMLWrite.equalsIgnoreCase("true")){
                fw.write(response);
                fw.flush();
                fw.close(); 
            }
            
        } catch(java.rmi.RemoteException ex) {
        	LOGGER.error(CodeHelper.getStackTrace(ex));
        } catch(Exception ex) {
        	LOGGER.error(CodeHelper.getStackTrace(ex));
        }
        try { 
        	
        	SAXParser myparser = new SAXParser();
            IngeniumHandler handler = new IngeniumHandler();
            myparser.setContentHandler(handler);
            myparser.setErrorHandler(handler);
        	
        	if(isXMLWrite.equalsIgnoreCase("true")){
        		 FileReader reader = new FileReader(fileResponseXML);
                 myparser.parse(new InputSource(reader));
                 reader.close();
        	 }else{
            	 byte buf[] = response.getBytes();
            	 ByteArrayInputStream in = new ByteArrayInputStream(buf);
            	 BufferedInputStream f = new BufferedInputStream(in);
        		 InputStreamReader reader = new InputStreamReader(f);
                 myparser.parse(new InputSource(reader));
                 reader.close();
        	 }
        	
             hs = handler.getHashmap();
            
        } catch(Exception ex) {
        	LOGGER.error(CodeHelper.getStackTrace(ex));
        }
        
        LOGGER.info("submit end");
        return hs;
    }
  
    static public String getContents(File aFile) {
    
    LOGGER.info("getContents start");
    
    StringBuffer contents = new StringBuffer();
    BufferedReader input = null;
    try {
      input = new BufferedReader( new FileReader(aFile) );
      String line = null; 
      while (( line = input.readLine()) != null){
        contents.append(line);
        contents.append(System.getProperty("line.separator"));
      }  
    }
    catch (FileNotFoundException ex) {
    	LOGGER.error(CodeHelper.getStackTrace(ex));
    	
    }
    catch (IOException ex){
    	LOGGER.error(CodeHelper.getStackTrace(ex));
    	
    }
    finally {
      try {
        if (input!= null) {
          input.close();
        }
      }
      catch (IOException ex) {
    	  LOGGER.error(CodeHelper.getStackTrace(ex));
    	
      }
    }
    LOGGER.info("getContents end");
    return contents.toString();
  }

	public String getResponse() {
		return response;
	}
}

