/*******************************************************************************
 * Copyright (c) 2005, 2006 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/*
 * IngBusinessProcesses.java
 *
 * Created on May 26, 2006, 5:03 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package ph.com.sunlife.wms.ws.ingenium;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.ResourceBundle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.slocpi.ium.util.CodeHelper;




/**
 *s
 * @author PM13
 */
public class IngBusinessProcesses {
	private static final Logger LOGGER = LoggerFactory.getLogger(IngBusinessProcesses.class);
	
	boolean printParsedXML = true;
    private InvokeIngenium invokeIngenium;
    private ResourceBundle rbIngCode = ResourceBundle.getBundle("com.ph.sunlife.component.properties.IngeniumCode");
    /**
     * Creates a new instance of IngBusinessProcesses
     */
    public IngBusinessProcesses(){
    	LOGGER.info("IngBusinessProcesses instance created");
        invokeIngenium = new InvokeIngenium();
    }

    public String createLogin(String Authenticate[])
    {
    	LOGGER.info("createLogin start");
    	StringBuffer strBuffer = new StringBuffer();
        strBuffer.append("<?xml version=\"1.0\" encoding=\"UTF-8\" ?> ");
        strBuffer.append("<TXLife>");
        strBuffer.append("<UserAuthRequest>");
        strBuffer.append("<UserLoginName>");
        strBuffer.append(Authenticate[0]);
        strBuffer.append("</UserLoginName>");
        strBuffer.append("<UserPswd>");
        strBuffer.append("<CryptType>");
        strBuffer.append("NONE");
        strBuffer.append("</CryptType>");
        strBuffer.append("<Pswd>");
        strBuffer.append(Authenticate[1]);
        strBuffer.append("</Pswd>");
        strBuffer.append("</UserPswd>");
        strBuffer.append("</UserAuthRequest>");
        LOGGER.info("createLogin end");
        return strBuffer.toString();
    }


    
    
 
    
    public Object InquiryULDetails_Append(String Authenticate[],String strMirPolIdBase,String Coverage_Number){
        
    	LOGGER.info("InquiryULDetails_Append start 1");
    	ResourceBundle rb=ResourceBundle.getBundle("com.ph.sunlife.component.properties.Ingenium");
    	DateFormat dft=new SimpleDateFormat("yyyy-MM-dd");
    	
    	String effective_Date="";
    	if(!rb.getString("EFFECTIVE_DATE").trim().equals(""))
    		effective_Date=rb.getString("EFFECTIVE_DATE");
    	else {
    		effective_Date=dft.format(new Date());
    	}
    		
    	HashMap hs_result =(HashMap) InquiryULDetails_Append(Authenticate,strMirPolIdBase,Coverage_Number,effective_Date);
    	
    	LOGGER.info("InquiryULDetails_Append end 1");
    	return hs_result;
	  }
    
    
    
    public Object InquiryULDetails_Append(String Authenticate[],String strMirPolIdBase,String Coverage_Number,String Effective_Date){
    	
    	LOGGER.info("InquiryULDetails_Append start 2");
        HashMap hs_result = null;
        String PolicyNo = "";
        String suffix = "";
        StringBuffer strBuffer = new StringBuffer();
        if(strMirPolIdBase.length() > 9)
        {
        	PolicyNo = strMirPolIdBase.substring(0, 9);
        	suffix = ""+strMirPolIdBase.charAt(9);
        }
        else
        {
        	PolicyNo = strMirPolIdBase;
        }
        strBuffer.append(createLogin(Authenticate))
        .append("<TXLifeRequest>")
        .append("<TransRefGUID>2996b11f-e5b2-d00f-281b-03582683cd49</TransRefGUID> ")
        .append("<TransType tc=\"InquiryULDetails\">InquiryULDetails</TransType>")
       	.append("<TransExeDate/>")
       	.append("<TransExeTime/>")
        
   	 	.append("<OLifE>")
   		.append("<InquiryULDetailsData>")
   		.append("<MirCommonFields>")
   		.append(" <MirCvgNum>")
   		.append(Coverage_Number)
   		.append("</MirCvgNum>")
   		.append("<MirPolId>")
   		.append("<MirPolIdBase>")
   		.append(PolicyNo)
   		.append("</MirPolIdBase> ")
   		.append("<MirPolIdSfx>")
   		.append(suffix)
   		.append("</MirPolIdSfx> ")
   		.append("</MirPolId>")
   		.append("</MirCommonFields>")
   		.append("<MirDvEffDt>")
   		.append(Effective_Date)
   		.append("</MirDvEffDt>")
   		.append("</InquiryULDetailsData>")
   		.append("</OLifE>")
   		.append("</TXLifeRequest>")
   		.append("</TXLife>");
	      try {
	    	  StringBuffer sb = new StringBuffer();
	    	  sb.append("InquiryULDetailsData_PolNo-");
	    	  sb.append(strMirPolIdBase);
	          hs_result = invokeIngenium.submit(strBuffer.toString(),sb.toString());
	          
	          if(printParsedXML)
	          {
	          	Object strkeys[] = hs_result.keySet().toArray();
	          	for(int i=0;i<strkeys.length;i++)
	          	{
	          		ArrayList al_result = (ArrayList)hs_result.get(strkeys[i]);
	          		LOGGER.debug("TheLogger strkeys: " + strkeys[i] + ": ");
	          		for(int j=0;j<al_result.size();j++)
	          			LOGGER.debug("\t" + al_result.get(j));
	          	}
	          }
	          
	      } catch (Exception ex) {
	    	  LOGGER.error(CodeHelper.getStackTrace(ex));
	      }
	      LOGGER.info("InquiryULDetails_Append end 2");
	      return hs_result;
	  }
    
    
    
    public Object InquiryPolicyValues_Append(String Authenticate[],String strMirPolIdBase){
        
    	LOGGER.info("InquiryPolicyValues_Append start 1");
    	ResourceBundle rb=ResourceBundle.getBundle("com.ph.sunlife.component.properties.Ingenium");
    	DateFormat dft=new SimpleDateFormat("yyyy-MM-dd");
    	
    	String effective_Date="";
    	if(!rb.getString("EFFECTIVE_DATE").trim().equals(""))
    		effective_Date=rb.getString("EFFECTIVE_DATE");
    	else {
    		effective_Date=dft.format(new Date());
    	}
    	HashMap hs_result = (HashMap) InquiryPolicyValues_Append(Authenticate, strMirPolIdBase,effective_Date);
    	
    	LOGGER.info("InquiryPolicyValues_Append end 1");
        return hs_result;
	  }
    
    
    public Object InquiryPolicyValues_Append(String Authenticate[],String strMirPolIdBase,String Effective_Date){
    	
    	LOGGER.info("InquiryPolicyValues_Append start 2");
        HashMap hs_result = null;
        String PolicyNo = "";
        String suffix = "";
        StringBuffer strBuffer = new StringBuffer();
        if(strMirPolIdBase.length() > 9)
        {
        	PolicyNo = strMirPolIdBase.substring(0, 9);
        	suffix = ""+strMirPolIdBase.charAt(9);
        }
        else
        {
        	PolicyNo = strMirPolIdBase;
        }
        strBuffer.append(createLogin(Authenticate))
        .append("<TXLifeRequest>")
        .append("<TransRefGUID>2996b11f-e5b2-d00f-281b-03582683cd49</TransRefGUID> ")
        .append("<TransType tc=\"InquiryPolicyValues\">InquiryPolicyValues</TransType> ")
       	.append("<TransExeDate/>")
       	.append("<TransExeTime/>")
        
   	 	.append("<OLifE>")
   		.append("<InquiryPolicyValuesData>")
   		.append("<MirCommonFields>")
   		.append(" <MirCvgNum>")
   		.append("01")
   		.append("</MirCvgNum>")
   		.append("<MirPolId>")
   		.append("<MirPolIdBase>")
   		.append(PolicyNo)
   		.append("</MirPolIdBase> ")
   		.append("<MirPolIdSfx>")
   		.append(suffix)
   		.append("</MirPolIdSfx> ")
   		.append("</MirPolId>")
   		.append("<MirDvEffDt>")
   		.append(Effective_Date)
   		.append("</MirDvEffDt>")
   		.append("</MirCommonFields>")
   		.append("</InquiryPolicyValuesData>")
   		.append("</OLifE>")
   		.append("</TXLifeRequest>")
   		.append("</TXLife>");
	      try {
	    	  StringBuffer sb = new StringBuffer();
	    	  sb.append("InquiryPolicyValuesData_PolNo-");
	    	  sb.append(strMirPolIdBase);
	          hs_result = invokeIngenium.submit(strBuffer.toString(),sb.toString());
	          
	          if(printParsedXML)
	          {
	          	Object strkeys[] = hs_result.keySet().toArray();
	          	for(int i=0;i<strkeys.length;i++)
	          	{
	          		ArrayList al_result = (ArrayList)hs_result.get(strkeys[i]);
	          		for(int j=0;j<al_result.size();j++)
	          			LOGGER.debug("\t" + al_result.get(j));
	          	}
	          }
	          
	      } catch (Exception ex) {
	    	  LOGGER.error(CodeHelper.getStackTrace(ex));
	      }
	      LOGGER.info("InquiryPolicyValues_Append end 2");
	      return hs_result;
	  }
    
    
    
    public Object InquiryCoveragePremiums_Append(String Authenticate[],String strMirPolIdBase,String Coverage_Number){
    	
    	LOGGER.info("InquiryCoveragePremiums_Append start");
        HashMap hs_result = null;
        String PolicyNo = "";
        String suffix = "";
        StringBuffer strBuffer = new StringBuffer();
        if(strMirPolIdBase.length() > 9)
        {
        	PolicyNo = strMirPolIdBase.substring(0, 9);
        	suffix = ""+strMirPolIdBase.charAt(9);
        }
        else
        {
        	PolicyNo = strMirPolIdBase;
        }
        strBuffer.append(createLogin(Authenticate))
        .append("<TXLifeRequest>")
        .append("<TransRefGUID>2996b11f-e5b2-d00f-281b-03582683cd49</TransRefGUID> ")
        .append(" <TransType tc=\"InquiryCoveragePremiums\">InquiryCoveragePremiums</TransType>")
       	.append("<TransExeDate/>")
       	.append("<TransExeTime/>")
        
   	 	.append("<OLifE>")
   		.append("<InquiryCoveragePremiumsData>")
   		.append("<MirCommonFields>")
   		.append(" <MirCvgNum>")
   		.append(Coverage_Number)
   		.append("</MirCvgNum>")
   		.append("<MirPolId>")
   		.append("<MirPolIdBase>")
   		.append(PolicyNo)
   		.append("</MirPolIdBase> ")
   		.append("<MirPolIdSfx>")
   		.append(suffix)
   		.append("</MirPolIdSfx> ")
   		.append("</MirPolId>")
   		.append("</MirCommonFields>")
   		.append("</InquiryCoveragePremiumsData>")
   		.append("</OLifE>")
   		.append("</TXLifeRequest>")
   		.append("</TXLife>");
	      try {
	    	  StringBuffer sb = new StringBuffer();
	    	  sb.append("InquiryCoveragePremiums_PolNo-");
	    	  sb.append(strMirPolIdBase);
	          hs_result = invokeIngenium.submit(strBuffer.toString(),sb.toString());
	          
	          if(printParsedXML)
	          {
	          	Object strkeys[] = hs_result.keySet().toArray();
	          	for(int i=0;i<strkeys.length;i++)
	          	{
	          		ArrayList al_result = (ArrayList)hs_result.get(strkeys[i]);
	          		LOGGER.debug("TheLogger strkeys: " + strkeys[i] + ": ");
	          		for(int j=0;j<al_result.size();j++)
	          			LOGGER.debug("\t" + al_result.get(j));
	          	}
	          }
	          
	      } catch (Exception ex) {
	    	  LOGGER.error(CodeHelper.getStackTrace(ex));
	      }
	      LOGGER.info("InquiryCoveragePremiums_Append end");
	      return hs_result;
	  }
    
    
    
    
    public Object DeferredActivityList_Append(String Authenticate[],String strMirPolIdBase,String Effective_Date){
    	
    	LOGGER.info("DeferredActivityList_Append start 1");
        HashMap hs_result = null;
        String PolicyNo = "";
        String suffix = "";
        StringBuffer strBuffer = new StringBuffer();
        if(strMirPolIdBase.length() > 9)
        {
        	PolicyNo = strMirPolIdBase.substring(0, 9);
        	suffix = ""+strMirPolIdBase.charAt(9);
        }
        else
        {
        	PolicyNo = strMirPolIdBase;
        }
        strBuffer.append(createLogin(Authenticate))
        .append("<TXLifeRequest>")
        .append("<TransRefGUID>2996b11f-e5b2-d00f-281b-03582683cd49</TransRefGUID> ")
        .append("<TransType tc=\"DeferredActivityList\">DeferredActivityList</TransType> ")
       	.append("<TransExeDate/>")
       	.append("<TransExeTime/>")
        
   	 	.append("<OLifE>")
   		.append("<DeferredActivityListData>")
   		.append("<MirPolId>")
   		.append("<MirDvEffDt>")
   		.append(Effective_Date)
   		.append("</MirDvEffDt>")
   		.append("<MirPolIdBase>")
   		.append(PolicyNo)
   		.append("</MirPolIdBase> ")
   		.append("<MirPolIdSfx>")
   		.append(suffix)
   		.append("</MirPolIdSfx> ")
   		.append("</MirPolId>")
   		.append("</DeferredActivityListData>")
   		.append("</OLifE>")
   		.append("</TXLifeRequest>")
   		.append("</TXLife>");
	      try {
	    	  StringBuffer sb = new StringBuffer();
	    	  sb.append("DeferredActivityList_PolNo-");
	    	  sb.append(strMirPolIdBase);
	          hs_result = invokeIngenium.submit(strBuffer.toString(),sb.toString());
	          
	          if(printParsedXML)
	          {
	          	Object strkeys[] = hs_result.keySet().toArray();
	          	for(int i=0;i<strkeys.length;i++)
	          	{
	          		ArrayList al_result = (ArrayList)hs_result.get(strkeys[i]);
	          		LOGGER.debug("TheLogger strkeys: " + strkeys[i] + ": ");
	          		for(int j=0;j<al_result.size();j++)
	          			LOGGER.debug("\t" + al_result.get(j));
	          	}
	          }
	          
	      } catch (Exception ex) {
	    	  LOGGER.error(CodeHelper.getStackTrace(ex));
	      }
	      LOGGER.info("DeferredActivityList_Append end 1");
	      return hs_result;
	  }
    
    
    
    
    public Object DeferredActivityList_Append(String Authenticate[],String strMirPolIdBase){
        
    	LOGGER.info("DeferredActivityList_Append start 2");
    	
    	ResourceBundle rb=ResourceBundle.getBundle("com.ph.sunlife.component.properties.Ingenium");
    	DateFormat dft=new SimpleDateFormat("yyyy-MM-dd");
    	
    	String effective_Date="";
    	if(!rb.getString("EFFECTIVE_DATE").trim().equals(""))
    		effective_Date=rb.getString("EFFECTIVE_DATE");
    	else {
    		effective_Date=dft.format(new Date());
    	}
    	HashMap hs_result = (HashMap) DeferredActivityList_Append( Authenticate, strMirPolIdBase,effective_Date);
    	
    	LOGGER.info("DeferredActivityList_Append end 2");
        return hs_result;
	  }
    
    
    
    public Object FundTransferPercentToPercent_Append(String Authenticate[],String strMirPolIdBase){
    	
    	LOGGER.info("FundTransferPercentToPercent_Append start 1");
    	ResourceBundle rb=ResourceBundle.getBundle("com.ph.sunlife.component.properties.Ingenium");
    	DateFormat dft=new SimpleDateFormat("yyyy-MM-dd");
    	
    	String effective_Date="";
    	if(!rb.getString("EFFECTIVE_DATE").trim().equals(""))
    		effective_Date=rb.getString("EFFECTIVE_DATE");
    	else {
    		effective_Date=dft.format(new Date());
    	}
        HashMap hs_result = (HashMap) FundTransferPercentToPercent_Append(Authenticate, strMirPolIdBase,effective_Date);
        LOGGER.info("FundTransferPercentToPercent_Append end 1");
        return hs_result;
	  }
    
    
    
    
    public Object FundTransferPercentToPercent_Append(String Authenticate[],String strMirPolIdBase,String Effective_Date){
    	
    	LOGGER.info("FundTransferPercentToPercent_Append start 2");
        HashMap hs_result = null;
        String PolicyNo = "";
        String suffix = "";
        StringBuffer strBuffer = new StringBuffer();
        if(strMirPolIdBase.length() > 9)
        {
        	PolicyNo = strMirPolIdBase.substring(0, 9);
        	suffix = ""+strMirPolIdBase.charAt(9);
        }
        else
        {
        	PolicyNo = strMirPolIdBase;
        }
        strBuffer.append(createLogin(Authenticate))
        .append("<TXLifeRequest>")
        .append("<TransRefGUID>2996b11f-e5b2-d00f-281b-03582683cd49</TransRefGUID> ")
        .append("<TransType tc=\"FundTransferPercentToPercent\">FundTransferPercentToPercent</TransType> ")
       	.append("<TransExeDate/>")
       	.append("<TransExeTime/>")
        
   	 	.append("<OLifE>")
   		.append("<FundTransferPercentToPercentData>")
   		.append("<MirPolId>")
   		.append("<MirPolIdBase>")
   		.append(PolicyNo)
   		.append("</MirPolIdBase> ")
   		.append("<MirPolIdSfx>")
   		.append(suffix)
   		.append("</MirPolIdSfx> ")
   		.append("</MirPolId>")
   		.append("<MirCvgNum>01</MirCvgNum> ")
   		.append("<MirCiaEffDt>")

   		.append(Effective_Date)
   		.append("</MirCiaEffDt>")
   		.append("<MirCiaLoadAmt>0</MirCiaLoadAmt>")
   		.append("<MirCiaLoadForceInd>Y</MirCiaLoadForceInd>")
   		.append("<MirSupresCnfrmInd>Y</MirSupresCnfrmInd>")
   		.append("<MirDvPrcesStateCd>1</MirDvPrcesStateCd>")
   		.append("</FundTransferPercentToPercentData>")
   		.append("</OLifE>")
   		.append("</TXLifeRequest>")
   		.append("</TXLife>");
	      try {
	    	  StringBuffer sb = new StringBuffer();
	    	  sb.append("FundTransferPercentToPercent_PolNo-");
	    	  sb.append(strMirPolIdBase);
	          hs_result = invokeIngenium.submit(strBuffer.toString(),sb.toString());
	          
	          if(printParsedXML)
	          {
	          	Object strkeys[] = hs_result.keySet().toArray();
	          	for(int i=0;i<strkeys.length;i++)
	          	{
	          		ArrayList al_result = (ArrayList)hs_result.get(strkeys[i]);
	          		LOGGER.debug("TheLogger strkeys: " + strkeys[i] + ": ");
	          		for(int j=0;j<al_result.size();j++)
	          			LOGGER.debug("\t" + al_result.get(j));
	          	}
	          }
	          
	      } catch (Exception ex) {
	    	  LOGGER.error(CodeHelper.getStackTrace(ex));
	      }
	      LOGGER.info("FundTransferPercentToPercent_Append end 2");
	      return hs_result;
	  }
    
    public Object PolicyAllocationList_Append(String Authenticate[],String strMirPolIdBase,String Effective_Date){
    	
    	LOGGER.info("PolicyAllocationList_Append start 1");
        HashMap hs_result = null;
        String PolicyNo = "";
        String suffix = "";
        StringBuffer strBuffer = new StringBuffer();
        if(strMirPolIdBase.length() > 9)
        {
        	PolicyNo = strMirPolIdBase.substring(0, 9);
        	suffix = ""+strMirPolIdBase.charAt(9);
        }
        else
        {
        	PolicyNo = strMirPolIdBase;
        }
        strBuffer.append(createLogin(Authenticate))
        .append("<TXLifeRequest>")
        .append("<TransRefGUID>2996b11f-e5b2-d00f-281b-03582683cd49</TransRefGUID> ")
        .append("<TransType tc=\"PolicyAllocationList\">PolicyAllocationList</TransType> ")
       	.append("<TransExeDate>2006-05-31</TransExeDate> ")
       	.append("<TransExeTime>09:00:00-05:00</TransExeTime>")
        
   	 	.append("<OLifE>")
   		.append("<PolicyAllocationListData>")
   		.append("<MirPolId>")
   		.append("<MirPolIdBase>")
   		.append(PolicyNo)
   		.append("</MirPolIdBase> ")
   		.append("<MirPolIdSfx>")
   		.append(suffix)
   		.append("</MirPolIdSfx> ")
   		.append("</MirPolId>")
   		.append("<MirCdiTypCd/>")
   		.append("<MirCdiEffDt>" )
   		
   		.append(Effective_Date)
   		.append("</MirCdiEffDt>")
   		.append("</PolicyAllocationListData>")
   		.append("</OLifE>")
   		.append("</TXLifeRequest>")
   		.append("</TXLife>");
	      try {
	    	  StringBuffer sb = new StringBuffer();
	    	  sb.append("PolicyAllocationList_PolNo-");
	    	  sb.append(strMirPolIdBase);
	          hs_result = invokeIngenium.submit(strBuffer.toString(),sb.toString());
	          
	          if(printParsedXML)
	          {
	          	Object strkeys[] = hs_result.keySet().toArray();
	          	for(int i=0;i<strkeys.length;i++)
	          	{
	          		ArrayList al_result = (ArrayList)hs_result.get(strkeys[i]);
	          		LOGGER.debug("TheLogger strkeys: " + strkeys[i] + ": ");
	          		for(int j=0;j<al_result.size();j++)
	          			LOGGER.debug("\t" + al_result.get(j));
	          	}
	          }
	          
	      } catch (Exception ex) {
	    	  LOGGER.error(CodeHelper.getStackTrace(ex));
	      }
	      LOGGER.info("PolicyAllocationList_Append end 1");
	      return hs_result;
	  }
    
    
    
    
    
    public Object PolicyAllocationList_Append(String Authenticate[],String strMirPolIdBase){
        
    	LOGGER.info("PolicyAllocationList_Append start 2");
    	
    	ResourceBundle rb=ResourceBundle.getBundle("com.ph.sunlife.component.properties.Ingenium");
    	DateFormat dft=new SimpleDateFormat("yyyy-MM-dd");
    	
    	String effective_Date="";
    	if(!rb.getString("EFFECTIVE_DATE").trim().equals(""))
    		effective_Date=rb.getString("EFFECTIVE_DATE");
    	else {
    		effective_Date=dft.format(new Date());
    	}
    	HashMap hs_result = (HashMap) PolicyAllocationList_Append(Authenticate,strMirPolIdBase,effective_Date);
    	
    	LOGGER.info("PolicyAllocationList_Append end 2");
        return hs_result;
	  }
    
    public Object InquiryConsolidatedInformation_Append(String Authenticate[],String strMirPolIdBase, String effectiveDate){
    	
    	LOGGER.info("InquiryConsolidatedInformation_Append start 1");
        HashMap hs_result = null;
        String PolicyNo = "";
        String suffix = "";
        StringBuffer strBuffer = new StringBuffer();
        if(strMirPolIdBase.length() > 9)
        {
        	PolicyNo = strMirPolIdBase.substring(0, 9);
        	suffix = ""+strMirPolIdBase.charAt(9);
        }
        else
        {
        	PolicyNo = strMirPolIdBase;
        }
        strBuffer.append(createLogin(Authenticate))
        .append("<TXLifeRequest>")
        .append("<TransRefGUID>2996b11f-e5b2-d00f-281b-03582683cd49</TransRefGUID> ")
        .append("<TransType tc=\"InquiryConsolidatedInformation\">InquiryConsolidatedInformation</TransType> ")
       	.append("<TransExeDate>2008-07-04</TransExeDate> ")
       	.append("<TransExeTime>18:32:55</TransExeTime>")
        
   	 	.append("<OLifE>")
   		.append("<InquiryConsolidatedInformationData>")
   		.append("<SrcSystem></SrcSystem>")
   		.append("<MirPolId>")
   		.append("<MirDvEffDt>")
   		.append(effectiveDate)
   		.append("</MirDvEffDt>")
   		
   		.append("<MirPolIdBase>")
   		.append(PolicyNo)
   		.append("</MirPolIdBase> ")
   		.append("<MirPolIdSfx>")
   		.append(suffix)
   		.append("</MirPolIdSfx> ")
   		.append("</MirPolId>")
   		.append("</InquiryConsolidatedInformationData>")
   		.append("</OLifE>")
   		.append("</TXLifeRequest>")
   		.append("</TXLife>");
	      try {
	    	  StringBuffer sb = new StringBuffer();
	    	  sb.append("InquiryConsolidatedInformation_PolNo-");
	    	  sb.append(strMirPolIdBase);
	          hs_result = invokeIngenium.submit(strBuffer.toString(),sb.toString());
	          
	          if(printParsedXML)
	          {
	          	Object strkeys[] = hs_result.keySet().toArray();
	          	for(int i=0;i<strkeys.length;i++)
	          	{
	          		ArrayList al_result = (ArrayList)hs_result.get(strkeys[i]);
	          		LOGGER.debug("TheLogger strkeys: " + strkeys[i] + ": ");
	          		for(int j=0;j<al_result.size();j++)
	          			LOGGER.debug("\t" + al_result.get(j));
	          	}
	          }
	          
	      } catch (Exception ex) {
	    	  LOGGER.error(CodeHelper.getStackTrace(ex));
	      }
	      LOGGER.info("InquiryConsolidatedInformation_Append end 1");
	      return hs_result;
	  }
    
    
    
    
    
    public Object InquiryConsolidatedInformation_Append(String Authenticate[],String strMirPolIdBase){
    	
    	LOGGER.info("InquiryConsolidatedInformation_Append start 2");
    	ResourceBundle rb=ResourceBundle.getBundle("com.ph.sunlife.component.properties.Ingenium");
    	DateFormat dft=new SimpleDateFormat("yyyy-MM-dd");
    	
    	String effective_Date="";
    	if(!rb.getString("EFFECTIVE_DATE").trim().equals(""))
    		effective_Date=rb.getString("EFFECTIVE_DATE");
    	else {
    		effective_Date=dft.format(new Date());
    	}
     	HashMap hs_result = (HashMap) InquiryConsolidatedInformation_Append(Authenticate,strMirPolIdBase,effective_Date);
     	
     	LOGGER.info("InquiryConsolidatedInformation_Append end 2");
        return hs_result;
	      
	  }
    
    
    
    public Object PolicyAllocationInquiry_Append(String Authenticate[],String strMirPolIdBase){
    	
    	LOGGER.info("PolicyAllocationInquiry_Append start");
        HashMap hs_result = null;
        String PolicyNo = "";
        String suffix = "";
        StringBuffer strBuffer = new StringBuffer();
        if(strMirPolIdBase.length() > 9)
        {
        	PolicyNo = strMirPolIdBase.substring(0, 9);
        	suffix = ""+strMirPolIdBase.charAt(9);
        }
        else
        {
        	PolicyNo = strMirPolIdBase;
        }
        strBuffer.append(createLogin(Authenticate))
        .append("<TXLifeRequest>")
        .append("<TransRefGUID>2996b11f-e5b2-d00f-281b-03582683cd49</TransRefGUID> ")
        .append("<TransType tc=\"PolicyAllocationInquiry\">PolicyAllocationInquiry</TransType> ")
       	.append("<TransExeDate>2006-04-18</TransExeDate> ")
   		.append("<TransExeTime>16:55:46</TransExeTime>")
   		.append("<OLifE>")
   		.append("<PolicyAllocationData>")
   		
   		.append("<MirPolId>")
   		.append("<MirPolIdBase>")
   		.append(PolicyNo)
   		.append("</MirPolIdBase> ")
   		.append("<MirPolIdSfx>")
   		.append(suffix)
   		.append("</MirPolIdSfx> ")
   		.append("</MirPolId>")
   		.append("<MirCommonFields>")
   		.append("<MirCdiStatCd />")
   		.append("<MirCdiTypCd>")
   		.append("S")
   		.append("</MirCdiTypCd>")
   		.append("<MirCdiEffDt>2007-09-02</MirCdiEffDt>")
   		.append("</MirCommonFields>")
   		
   		.append("</PolicyAllocationData>")
   		.append("</OLifE>")
   		.append("</TXLifeRequest>")
   		.append("</TXLife>");
	      try {
	    	  StringBuffer sb = new StringBuffer();
	    	  sb.append("PolicyAllocationInquiry_PolNo-");
	    	  sb.append(strMirPolIdBase);
	          hs_result = invokeIngenium.submit(strBuffer.toString(),sb.toString());
	          
	          if(printParsedXML)
	          {
	          	Object strkeys[] = hs_result.keySet().toArray();
	          	for(int i=0;i<strkeys.length;i++)
	          	{
	          		ArrayList al_result = (ArrayList)hs_result.get(strkeys[i]);
	          		LOGGER.debug(strkeys[i] + ": ");
	          		for(int j=0;j<al_result.size();j++)
	          			LOGGER.debug("\t" + al_result.get(j));
	          	}
	          }
	          
	      } catch (Exception ex) {
	    	  LOGGER.error(CodeHelper.getStackTrace(ex));
	      }
	      LOGGER.info("PolicyAllocationInquiry_Append end");
	      return hs_result;
	  }
    
    
    public Object InquiryBilling_Append(String Authenticate[],String strMirPolIdBase){
    	
    	LOGGER.info("InquiryBilling_Append start");
        HashMap hs_result = null;
        String PolicyNo = "";
        String suffix = "";
        StringBuffer strBuffer = new StringBuffer();
        if(strMirPolIdBase.length() > 9)
        {
        	PolicyNo = strMirPolIdBase.substring(0, 9);
        	suffix = ""+strMirPolIdBase.charAt(9);
        }
        else
        {
        	PolicyNo = strMirPolIdBase;
        }
        strBuffer.append(createLogin(Authenticate))
        .append("<TXLifeRequest>")
        .append("<TransRefGUID>2996b11f-e5b2-d00f-281b-03582683cd49</TransRefGUID> ")
        .append("<TransType tc=\"InquiryBilling\">InquiryBilling</TransType> ")
       	.append("<TransExeDate>2006-05-31</TransExeDate> ")
   		.append("<TransExeTime>09:00:00-05:00</TransExeTime>")
   		.append("<OLifE>")
   		.append("<InquiryBillingData>")
   		.append("<MirCommonFields>")
   		.append("<MirPolId>")
   		.append("<MirPolIdBase>")
   		.append(PolicyNo)
   		.append("</MirPolIdBase> ")
   		.append("<MirPolIdSfx>")
   		.append(suffix)
   		.append("</MirPolIdSfx> ")
   		.append("</MirPolId>")
   		.append("</MirCommonFields>")
   		.append("<SrcSystem>WMS</SrcSystem> ")
   		.append("</InquiryBillingData>")
   		.append("</OLifE>")
   		.append("</TXLifeRequest>")
   		.append("</TXLife>");
	      try {
	    	  StringBuffer sb = new StringBuffer();
	    	  sb.append("InquiryBilling_PolNo-");
	    	  sb.append(strMirPolIdBase);
	          hs_result = invokeIngenium.submit(strBuffer.toString(),sb.toString());
	          
	          if(printParsedXML)
	          {
	          	Object strkeys[] = hs_result.keySet().toArray();
	          	for(int i=0;i<strkeys.length;i++)
	          	{
	          		ArrayList al_result = (ArrayList)hs_result.get(strkeys[i]);
	          		LOGGER.debug(strkeys[i] + ": ");
	          		for(int j=0;j<al_result.size();j++)
	          			LOGGER.debug("\t" + al_result.get(j));
	          	}
	          }
	          
	      } catch (Exception ex) {
	    	  LOGGER.error(CodeHelper.getStackTrace(ex));
	      }
	      LOGGER.info("InquiryBilling_Append end");
	      return hs_result;
	  }
    public Object BF1191C_Append(String Authenticate[],String strClientId, String ReqtId, String reqStat, String strNewIndexNo){
    	
    	LOGGER.info("BF1191C_Append start 1");
        HashMap hs_result = null;
        String PolicyNo = "";
        String suffix = "";
        StringBuffer strBuffer = new StringBuffer();
        strBuffer.append(createLogin(Authenticate));
        strBuffer.append("<TXLifeRequest>");
        strBuffer.append("<TransRefGUID>2996b11f-e5b2-d00f-281b-03582683cd49</TransRefGUID> ");
        strBuffer.append("<TransType tc=\"RequirementsCreate\">RequirementsCreate</TransType>");
        strBuffer.append("<TransExeDate>2006-05-31</TransExeDate> ");
        strBuffer.append("<TransExeTime>09:00:00-05:00</TransExeTime> ");
        strBuffer.append("<OLifE> ");
        strBuffer.append("<RequirementData> ");
        strBuffer.append("<MirCommonFields> ");
        strBuffer.append("<MirCmpltnessInd>C</MirCmpltnessInd> ");
        strBuffer.append("<MirCliId>"); 
        strBuffer.append(strClientId);
        strBuffer.append("</MirCliId>");
        strBuffer.append("<MirReqirId>");
        strBuffer.append(ReqtId);
        strBuffer.append("</MirReqirId>");
        strBuffer.append("<MirCpreqStatCd>");
        strBuffer.append(reqStat);
        strBuffer.append("</MirCpreqStatCd>");
        strBuffer.append("<MirIndexNum>");
        strBuffer.append(strNewIndexNo);
        strBuffer.append("</MirIndexNum>");
        strBuffer.append("</MirCommonFields>");
        strBuffer.append("</RequirementData>");
        strBuffer.append("</OLifE>");
        strBuffer.append("</TXLifeRequest> ");
        strBuffer.append("</TXLife> ");
        
        try {
        	StringBuffer sb = new StringBuffer();
	    	sb.append("BF1191 - RequirementsCreate_CliId-");
	    	sb.append(strClientId);
            hs_result = invokeIngenium.submit(strBuffer.toString(),sb.toString());
            
            if(printParsedXML)
            {
            	Object strkeys[] = hs_result.keySet().toArray();
            	for(int i=0;i<strkeys.length;i++)
            	{
            		ArrayList al_result = (ArrayList)hs_result.get(strkeys[i]);
            		LOGGER.debug(strkeys[i] + ": ");
            		for(int j=0;j<al_result.size();j++)
            			LOGGER.debug("\t" + al_result.get(j));
            	}
            }
            
        } catch (Exception ex) {
        	LOGGER.error(CodeHelper.getStackTrace(ex));
        }
        LOGGER.info("BF1191C_Append end 1");
        return hs_result;
    }
    
    public Object BF1191C_Append(String Authenticate[],String strClientId, String ReqtId, String reqStat){
    	
    	LOGGER.info("BF1191C_Append start 2");
        HashMap hs_result = null;
        String PolicyNo = "";
        String suffix = "";
        StringBuffer strBuffer = new StringBuffer();
        strBuffer.append(createLogin(Authenticate));
        strBuffer.append("<TXLifeRequest>");
        strBuffer.append("<TransRefGUID>2996b11f-e5b2-d00f-281b-03582683cd49</TransRefGUID> ");
        strBuffer.append("<TransType tc=\"RequirementsCreate\">RequirementsCreate</TransType>");
        strBuffer.append("<TransExeDate>2006-05-31</TransExeDate> ");
        strBuffer.append("<TransExeTime>09:00:00-05:00</TransExeTime> ");
        strBuffer.append("<OLifE> ");
        strBuffer.append("<RequirementData> ");
        strBuffer.append("<MirCommonFields> ");
        strBuffer.append("<MirCmpltnessInd>C</MirCmpltnessInd> ");
        strBuffer.append("<MirCliId>"); 
        strBuffer.append(strClientId);
        strBuffer.append("</MirCliId>");
        strBuffer.append("<MirReqirId>");
        strBuffer.append(ReqtId);
        strBuffer.append("</MirReqirId>");
        strBuffer.append("<MirCpreqStatCd>");
        strBuffer.append(reqStat);
        strBuffer.append("</MirCpreqStatCd>");
        strBuffer.append("</MirCommonFields>");
        strBuffer.append("</RequirementData>");
        strBuffer.append("</OLifE>");
        strBuffer.append("</TXLifeRequest> ");
        strBuffer.append("</TXLife> ");
        
        try {
        	StringBuffer sb = new StringBuffer();
	    	sb.append("BF1191 - RequirementsCreate_CliId-");
	    	sb.append(strClientId);
            hs_result = invokeIngenium.submit(strBuffer.toString(),sb.toString());
            
            if(printParsedXML)
            {
            	Object strkeys[] = hs_result.keySet().toArray();
            	for(int i=0;i<strkeys.length;i++)
            	{
            		ArrayList al_result = (ArrayList)hs_result.get(strkeys[i]);
            		LOGGER.debug(strkeys[i] + ": ");
            		for(int j=0;j<al_result.size();j++)
            			LOGGER.debug("\t" + al_result.get(j));
            	}
            }
            
        } catch (Exception ex) {
        	LOGGER.error(CodeHelper.getStackTrace(ex));
        }
        LOGGER.info("BF1191C_Append end 2");
        return hs_result;
    }
      
    public Object BF9892Retrieve_Append(String Authenticate[],String strMirPolIdBase)
    {
        LOGGER.info("BF9892Retrieve_Append start");
    	HashMap hs_result = null;
        String PolicyNo = "";
        String suffix = "";
        if(strMirPolIdBase.length() > 9)
        {
        	PolicyNo = strMirPolIdBase.substring(0, 9);
        	suffix = ""+strMirPolIdBase.charAt(9);
        }
        else
        {
        	PolicyNo = strMirPolIdBase;
        }
        StringBuffer strBuffer = new StringBuffer();
        strBuffer.append(createLogin(Authenticate));
        strBuffer.append("<TXLifeRequest>");
        strBuffer.append("<TransRefGUID>2996b11f-e5b2-d00f-281b-03582683cd49</TransRefGUID> ");
        strBuffer.append("<TransType tc=\"BF9892Retrieve\">BF9892Retrieve</TransType>");
        strBuffer.append("<TransExeDate>2006-05-31</TransExeDate> ");
        strBuffer.append("<TransExeTime>09:00:00-05:00</TransExeTime> ");
        strBuffer.append("<OLifE> ");
        strBuffer.append("<MirCommonFields> ");
        strBuffer.append("<MirPolId>");
        strBuffer.append("<MirPolIdBase>");
        strBuffer.append(PolicyNo);
        strBuffer.append("</MirPolIdBase>");
        strBuffer.append("<MirPolIdSfx>"); 
        strBuffer.append(suffix);
        strBuffer.append("</MirPolIdSfx>"); 
        strBuffer.append("</MirPolId>");
        strBuffer.append("</MirCommonFields>");
        strBuffer.append("</OLifE>");
        strBuffer.append("</TXLifeRequest> ");
        strBuffer.append("</TXLife> ");
        
        try {
        	StringBuffer sb = new StringBuffer();
	    	sb.append("BF9892Retrieve - Requirements and KO_PolNo-");
	    	sb.append(strMirPolIdBase);
            hs_result = invokeIngenium.submit(strBuffer.toString(),sb.toString());
            if(printParsedXML)
            {
            	Object strkeys[] = hs_result.keySet().toArray();
            
            	for(int i=0;i<strkeys.length;i++)
            	{
            		ArrayList al_result = (ArrayList)hs_result.get(strkeys[i]);
            		LOGGER.debug(strkeys[i] + ": ");
            		for(int j=0;j<al_result.size();j++)
            			LOGGER.debug("\t" + al_result.get(j));
            	}
            }
            
        } catch (Exception ex) {
        	LOGGER.error(CodeHelper.getStackTrace(ex));
        }
        LOGGER.info("BF9892Retrieve_Append end");
        return hs_result;
    }
    
    public Object BF1191_Append(String Authenticate[],String strMirPolIdBase, String ReqtId, String reqStat, String strNewIndexNo){
    	
    	LOGGER.info("BF1191_Append start 1");
        HashMap hs_result = null;
        String PolicyNo = "";
        String suffix = "";
        if(strMirPolIdBase.length() > 9)
        {
        	PolicyNo = strMirPolIdBase.substring(0, 9);
        	suffix = ""+strMirPolIdBase.charAt(9);
        }
        else
        {
        	PolicyNo = strMirPolIdBase;
        }
        StringBuffer strBuffer = new StringBuffer();
        strBuffer.append(createLogin(Authenticate));
        strBuffer.append("<TXLifeRequest>");
        strBuffer.append("<TransRefGUID>2996b11f-e5b2-d00f-281b-03582683cd49</TransRefGUID> ");
        strBuffer.append("<TransType tc=\"RequirementsCreate\">RequirementsCreate</TransType>");
        strBuffer.append("<TransExeDate>2006-05-31</TransExeDate> ");
        strBuffer.append("<TransExeTime>09:00:00-05:00</TransExeTime> ");
        strBuffer.append("<OLifE> ");
        strBuffer.append("<RequirementData> ");
        strBuffer.append("<MirCommonFields> ");
        strBuffer.append("<MirCmpltnessInd>C</MirCmpltnessInd> ");
        strBuffer.append("<MirPolId>");
        strBuffer.append("<MirPolIdBase>");
        strBuffer.append(PolicyNo);
        strBuffer.append("</MirPolIdBase>");
        strBuffer.append("<MirPolIdSfx>"); 
        strBuffer.append(suffix);
        strBuffer.append("</MirPolIdSfx>"); 
        strBuffer.append("</MirPolId>");
        strBuffer.append("<MirReqirId>");
        strBuffer.append(ReqtId);
        strBuffer.append("</MirReqirId>");
        strBuffer.append("<MirCpreqStatCd>");
        strBuffer.append(reqStat);
        strBuffer.append("</MirCpreqStatCd>");
        strBuffer.append("<MirIndexNum>");
        strBuffer.append(strNewIndexNo);
        strBuffer.append("</MirIndexNum>");
        strBuffer.append("</MirCommonFields>");
        strBuffer.append("</RequirementData>");
        strBuffer.append("</OLifE>");
        strBuffer.append("</TXLifeRequest> ");
        strBuffer.append("</TXLife> ");
        
        try {
        	StringBuffer sb = new StringBuffer();
	    	sb.append("BF1191 - RequirementsCreate_PolNo-");
	    	sb.append(strMirPolIdBase);
            hs_result = invokeIngenium.submit(strBuffer.toString(),sb.toString());
            if(printParsedXML)
            {
            	Object strkeys[] = hs_result.keySet().toArray();
            
            	for(int i=0;i<strkeys.length;i++)
            	{
            		ArrayList al_result = (ArrayList)hs_result.get(strkeys[i]);
            		LOGGER.debug(strkeys[i] + ": ");
            		for(int j=0;j<al_result.size();j++)
            			LOGGER.debug("\t" + al_result.get(j));
            	}
            }
            
        } catch (Exception ex) {
        	LOGGER.error(CodeHelper.getStackTrace(ex));
        }
        LOGGER.info("BF1191_Append end 1");
        return hs_result;
    }
    
    public Object BF1191_Append(String Authenticate[],String strMirPolIdBase, String ReqtId, String reqStat){
    	
    	LOGGER.info("BF1191_Append start 2");
        HashMap hs_result = null;
        String PolicyNo = "";
        String suffix = "";
        if(strMirPolIdBase.length() > 9)
        {
        	PolicyNo = strMirPolIdBase.substring(0, 9);
        	suffix = ""+strMirPolIdBase.charAt(9);
        }
        else
        {
        	PolicyNo = strMirPolIdBase;
        }
        StringBuffer strBuffer = new StringBuffer();
        strBuffer.append(createLogin(Authenticate));
        strBuffer.append("<TXLifeRequest>");
        strBuffer.append("<TransRefGUID>2996b11f-e5b2-d00f-281b-03582683cd49</TransRefGUID> ");
        strBuffer.append("<TransType tc=\"RequirementsCreate\">RequirementsCreate</TransType>");
        strBuffer.append("<TransExeDate>2006-05-31</TransExeDate> ");
        strBuffer.append("<TransExeTime>09:00:00-05:00</TransExeTime> ");
        strBuffer.append("<OLifE> ");
        strBuffer.append("<RequirementData> ");
        strBuffer.append("<MirCommonFields> ");
        strBuffer.append("<MirCmpltnessInd>C</MirCmpltnessInd> ");
        strBuffer.append("<MirPolId>");
        strBuffer.append("<MirPolIdBase>");
        strBuffer.append(PolicyNo);
        strBuffer.append("</MirPolIdBase>");
        strBuffer.append("<MirPolIdSfx>"); 
        strBuffer.append(suffix);
        strBuffer.append("</MirPolIdSfx>"); 
        strBuffer.append("</MirPolId>");
        strBuffer.append("<MirReqirId>");
        strBuffer.append(ReqtId);
        strBuffer.append("</MirReqirId>");
        strBuffer.append("<MirCpreqStatCd>");
        strBuffer.append(reqStat);
        strBuffer.append("</MirCpreqStatCd>");
        strBuffer.append("</MirCommonFields>");
        strBuffer.append("</RequirementData>");
        strBuffer.append("</OLifE>");
        strBuffer.append("</TXLifeRequest> ");
        strBuffer.append("</TXLife> ");
        
        try {
        	StringBuffer sb = new StringBuffer();
	    	sb.append("BF1191 - RequirementsCreate_PolNo-");
	    	sb.append(strMirPolIdBase);
            hs_result = invokeIngenium.submit(strBuffer.toString(),sb.toString());
            if(printParsedXML)
            {
            	Object strkeys[] = hs_result.keySet().toArray();
            
            	for(int i=0;i<strkeys.length;i++)
            	{
            		ArrayList al_result = (ArrayList)hs_result.get(strkeys[i]);
            		LOGGER.debug(strkeys[i] + ": ");
            		for(int j=0;j<al_result.size();j++)
            			LOGGER.debug("\t" + al_result.get(j));
            	}
            }
            
        } catch (Exception ex) {
        	LOGGER.error(CodeHelper.getStackTrace(ex));
        }
        LOGGER.info("BF1191_Append end 2");
        return hs_result;
    }
   
    /**
     * Handles the BF8000 business requests
     * retrieves the main Policy Details
     *
     *  P-6007-11
     **/  
    public Object BF8000_Append(String Authenticate[],String strPolicyId)
    {
    	LOGGER.info("BF8000_Append start");
    	HashMap hs_result = null;
    	String PolicyNo = "";
        String suffix = "";
        if(strPolicyId.length() > 9)
        {
        	PolicyNo = strPolicyId.substring(0, 9);
        	suffix = ""+strPolicyId.charAt(9);
        }
        else
        	PolicyNo = strPolicyId;
    	StringBuffer strBuffer = new StringBuffer();
    	strBuffer.append(createLogin(Authenticate));
    	strBuffer.append("<TXLifeRequest>");
    	strBuffer.append("<TransRefGUID>2996b11f-e5b2-d00f-281b-03582683cd49</TransRefGUID> ");
        strBuffer.append("<TransType tc=\"PolicyInquiry\">PolicyInquiry</TransType>");
        strBuffer.append("<TransExeDate>2006-05-31</TransExeDate> ");
        strBuffer.append("<TransExeTime>09:00:00-05:00</TransExeTime> ");
        strBuffer.append("<OLifE> ");
        strBuffer.append("<PolicyData> ");
        strBuffer.append("<MirPolId> ");
        strBuffer.append("<MirPolIdBase>");
        strBuffer.append(PolicyNo);       
        strBuffer.append("</MirPolIdBase>");
        strBuffer.append("<MirPolIdSfx>");
        strBuffer.append(suffix);
        strBuffer.append("</MirPolIdSfx>");
        strBuffer.append("</MirPolId> ");
        strBuffer.append("</PolicyData> ");
        strBuffer.append("</OLifE> ");
        strBuffer.append("</TXLifeRequest> ");
        strBuffer.append("</TXLife> ");
        try {
        	StringBuffer sb = new StringBuffer();
	    	sb.append("BF8000 - PolicyInquiry_PolNo-");
	    	sb.append(strPolicyId);
            hs_result = invokeIngenium.submit(strBuffer.toString(),sb.toString());
            if(printParsedXML)
            {
            	Object strkeys[] = hs_result.keySet().toArray();
            	for(int i=0;i<strkeys.length;i++)
            	{
            		ArrayList al_result = (ArrayList)hs_result.get(strkeys[i]);
            		LOGGER.debug(strkeys[i] + ": ");
            		for(int j=0;j<al_result.size();j++)
            			LOGGER.debug("\t" + al_result.get(j));
            	}
            }
            
        } catch (Exception ex) {
        	LOGGER.error(CodeHelper.getStackTrace(ex));
        }
        LOGGER.info("BF8000_Append end");
        return hs_result;
    }

     /**
     * Handles the BF6983 business requests
     * list the coverage of a specific policy
     **/
    
    public Object BF6983_Append(String Authenticate[],String strPolicyId)
    {

    	LOGGER.info("BF6983_Append start");
    	HashMap hs_result = null;
    	String PolicyNo = "";
        String suffix = "";
        if(strPolicyId.length() > 9)
        {
        	PolicyNo = strPolicyId.substring(0, 9);
        	suffix = ""+strPolicyId.charAt(9);
        }else
        	PolicyNo = strPolicyId;
    	StringBuffer strBuffer = new StringBuffer();
    	strBuffer.append(createLogin(Authenticate));
    	strBuffer.append("<TXLifeRequest>");
    	strBuffer.append("<TransRefGUID>2996b11f-e5b2-d00f-281b-03582683cd49</TransRefGUID> ");
        strBuffer.append("<TransType tc=\"InquiryOtherValues\">InquiryOtherValues</TransType>");
        strBuffer.append("<TransExeDate>2006-05-31</TransExeDate> ");
        strBuffer.append("<TransExeTime>09:00:00-05:00</TransExeTime> ");
        strBuffer.append("<OLifE>");
        strBuffer.append("<PolicyData>");
        strBuffer.append("<MirCommonFields>");
        strBuffer.append("<MirPolId>");
        strBuffer.append("<MirPolIdBase>");
        strBuffer.append(PolicyNo);       
        strBuffer.append("</MirPolIdBase>");
        strBuffer.append("<MirPolIdSfx>");
        strBuffer.append(suffix);
        strBuffer.append("</MirPolIdSfx>");
        strBuffer.append("</MirPolId>");
        strBuffer.append("</MirCommonFields>");
        strBuffer.append("</PolicyData>");
        strBuffer.append("</OLifE>");
        strBuffer.append("</TXLifeRequest>");
        strBuffer.append("</TXLife>");
        try {
        	StringBuffer sb = new StringBuffer();
	    	sb.append("BF6983 - InquiryOtherValues_PolNo-");
	    	sb.append(strPolicyId);
            hs_result = invokeIngenium.submit(strBuffer.toString(),sb.toString());
            if(printParsedXML)
            {
            	Object strkeys[] = hs_result.keySet().toArray();
            	for(int i=0;i<strkeys.length;i++)
            	{
            		ArrayList al_result = (ArrayList)hs_result.get(strkeys[i]);
            		LOGGER.debug(strkeys[i] + ": ");
            		for(int j=0;j<al_result.size();j++)
            			LOGGER.debug("\t" + al_result.get(j));
            	}
            }
            
        } catch (Exception ex) {
        	LOGGER.error(CodeHelper.getStackTrace(ex));
        }
        LOGGER.info("BF6983_Append end");
        return hs_result;
    }
   
    /**
     * Handles the BF1220 business requests
     * Retrieves the Client Information
     **/
    //ClientInquiry
    public Object BF1220C_Append(String Authenticate[],String strClientID)
    {
    	LOGGER.info("BF1220C_Append start");
    	HashMap hs_result = null;
    	String PolicyNo = "";
        String suffix = "";
        
    	StringBuffer strBuffer = new StringBuffer();
    	strBuffer.append(createLogin(Authenticate));
    	strBuffer.append("<TXLifeRequest>");
    	strBuffer.append("<TransRefGUID>2996b11f-e5b2-d00f-281b-03582683cd49</TransRefGUID> ");
        strBuffer.append("<TransType tc=\"ClientInquiry\">ClientInquiry</TransType>");
        strBuffer.append("<TransExeDate>2006-05-31</TransExeDate> ");
        strBuffer.append("<TransExeTime>09:00:00-05:00</TransExeTime> ");
        strBuffer.append("<OLifE>");
        strBuffer.append("<ClientData>");
        strBuffer.append("<MirCliInfo>");
        strBuffer.append("<MirCliId>");
        strBuffer.append(strClientID);
        strBuffer.append("</MirCliId>");
        strBuffer.append("</MirCliInfo>");
        strBuffer.append("</ClientData>");
        strBuffer.append("</OLifE> ");
        strBuffer.append("</TXLifeRequest>");
        strBuffer.append("</TXLife> ");
        try {
        	StringBuffer sb = new StringBuffer();
	    	sb.append("BF1220 - ClientInquiry_PolNo-");
	    	sb.append(strClientID);        	
            hs_result = invokeIngenium.submit(strBuffer.toString(),sb.toString());
            if(printParsedXML)
            {
            	Object strkeys[] = hs_result.keySet().toArray();
            	for(int i=0;i<strkeys.length;i++)
            	{
            		ArrayList al_result = (ArrayList)hs_result.get(strkeys[i]);
            		LOGGER.debug(strkeys[i] + ": ");
            		for(int j=0;j<al_result.size();j++)
            			LOGGER.debug("\t" + al_result.get(j));
            	}
            }
            
        } catch (Exception ex) {
        	LOGGER.error(CodeHelper.getStackTrace(ex));
        }
        LOGGER.info("BF1220C_Append end");
        return hs_result;
    }

     /**
     * Handles the BF6925 business requests
     * list the coverage of a specific policy
     **/
    public Object BF6925_Append(String Authenticate[], String strPolicyId, String CoverageNum)
    {

    	LOGGER.info("BF6925_Append start");
    	HashMap hs_result = null;
    	String PolicyNo = "";
        String suffix = "";
        if(strPolicyId.length() > 9)
        {
        	PolicyNo = strPolicyId.substring(0, 9);
        	suffix = ""+strPolicyId.charAt(9);
        }else
        	PolicyNo = strPolicyId;
    	StringBuffer strBuffer = new StringBuffer();
    	strBuffer.append(createLogin(Authenticate));
    	strBuffer.append("<TXLifeRequest>");
    	strBuffer.append("<TransRefGUID>2996b11f-e5b2-d00f-281b-03582683cd49</TransRefGUID> ");
        strBuffer.append("<TransType tc=\"CoverageList\">CoverageList</TransType>");
        strBuffer.append("<TransExeDate>2006-05-31</TransExeDate> ");
        strBuffer.append("<TransExeTime>09:00:00-05:00</TransExeTime> ");
        strBuffer.append("<OLifE>");
        strBuffer.append("<CoverageData>");
        strBuffer.append("<MirCommonFields>");
        strBuffer.append("<MirCvgNum>");
        strBuffer.append(CoverageNum);
        strBuffer.append("</MirCvgNum>");
        strBuffer.append("<MirPolId> ");
        strBuffer.append("<MirPolIdBase>");
        strBuffer.append(PolicyNo);       
        strBuffer.append("</MirPolIdBase>");
        strBuffer.append("<MirPolIdSfx>");
        strBuffer.append(suffix);
        strBuffer.append("</MirPolIdSfx>");
        strBuffer.append("</MirPolId>");
        strBuffer.append("</MirCommonFields>");
        strBuffer.append("</CoverageData>");
        strBuffer.append("</OLifE>");
        strBuffer.append("</TXLifeRequest>");
        strBuffer.append("</TXLife>");
        try {
        	StringBuffer sb = new StringBuffer();
	    	sb.append("BF6925 - CoverageList_PolNo-");
	    	sb.append(strPolicyId);          	
            hs_result = invokeIngenium.submit(strBuffer.toString(),sb.toString());
            if(printParsedXML)
            {
            	Object strkeys[] = hs_result.keySet().toArray();
            	for(int i=0;i<strkeys.length;i++)
            	{
            		ArrayList al_result = (ArrayList)hs_result.get(strkeys[i]);
            		LOGGER.debug(strkeys[i] + ": ");
            		for(int j=0;j<al_result.size();j++)
            			LOGGER.debug("\t" + al_result.get(j));
            	}
            }
            
        } catch (Exception ex) {
        	LOGGER.error(CodeHelper.getStackTrace(ex));
        }
        
        LOGGER.info("BF6925_Append end");
        return hs_result;
    }
   
    
     /**
     * Handles the BF1054 business requests
     * retrieves list of Policy Beneficiaries
     **/
    public Object BF8020_Append(String Authenticate[], String strPolicyId, String CoverageNo)
    {
    	LOGGER.info("BF8020_Append start");
    	HashMap hs_result = null;
    	String PolicyNo = "";
        String suffix = "";
        if(strPolicyId.length() > 9)
        {
        	PolicyNo = strPolicyId.substring(0,9);
        	suffix = ""+strPolicyId.charAt(9);
        }
        else
        {
        	PolicyNo = strPolicyId;
        }
    	StringBuffer strBuffer = new StringBuffer();
    	strBuffer.append(createLogin(Authenticate));
    	strBuffer.append("<TXLifeRequest>");
    	strBuffer.append("<TransRefGUID>2996b11f-e5b2-d00f-281b-03582683cd49</TransRefGUID> ");
        strBuffer.append("<TransType tc=\"CoverageInquiry\">CoverageInquiry</TransType>");
        strBuffer.append("<TransExeDate>2006-05-31</TransExeDate> ");
        strBuffer.append("<TransExeTime>09:00:00-05:00</TransExeTime> ");
        strBuffer.append("<OLifE>");
        strBuffer.append("<CoverageData>");
        strBuffer.append("<MirPolId>");
        strBuffer.append("<MirPolIdBase>");
        strBuffer.append(PolicyNo);       
        strBuffer.append("</MirPolIdBase>");
        strBuffer.append("<MirPolIdSfx>");
        strBuffer.append(suffix);
        strBuffer.append("</MirPolIdSfx>");
        strBuffer.append("</MirPolId>");
        strBuffer.append("<MirCvgNum>");
        strBuffer.append(CoverageNo);
        strBuffer.append("</MirCvgNum>");
        strBuffer.append("</CoverageData>");
        strBuffer.append("</OLifE>");
        strBuffer.append("</TXLifeRequest>");
        strBuffer.append("</TXLife>");
        try {
        	StringBuffer sb = new StringBuffer();
	    	sb.append("BF8020 - CoverageInquiry_PolNo-");
	    	sb.append(strPolicyId);     
            hs_result = invokeIngenium.submit(strBuffer.toString(),sb.toString());
            if(printParsedXML)
            {
            	Object strkeys[] = hs_result.keySet().toArray();
            	for(int i=0;i<strkeys.length;i++)
            	{
            		ArrayList al_result = (ArrayList)hs_result.get(strkeys[i]);
            		LOGGER.debug(strkeys[i] + ": ");
            		for(int j=0;j<al_result.size();j++)
            			LOGGER.debug("\t" + al_result.get(j));
            	}
            }
            
        } catch (Exception ex) {
        	LOGGER.error(CodeHelper.getStackTrace(ex));
        }
        LOGGER.info("BF8020_Append start");
        return hs_result;
    }
 
     /**
     * Handles the BF6945 business requests
     * retrieves Beneficiary Inquiry Details
     **/
    public Object BF6945_Append(String Authenticate[],String strPolicyId, String strCoverageNo)
    {
    	LOGGER.info("BF6945_Append start");
    	HashMap hs_result = null;
    	String PolicyNo = "";
        String suffix = "";
        if(strPolicyId.length() > 9)
        {
        	PolicyNo = strPolicyId.substring(0, 9);
        	suffix = ""+strPolicyId.charAt(9);
        }else
        	PolicyNo = strPolicyId;
    	StringBuffer strBuffer = new StringBuffer();
    	strBuffer.append(createLogin(Authenticate));
    	strBuffer.append("<TXLifeRequest>");
    	strBuffer.append("<TransRefGUID>2996b11f-e5b2-d00f-281b-03582683cd49</TransRefGUID> ");
        strBuffer.append("<TransType tc=\"InquiryBeneficiary\">InquiryBeneficiary</TransType>");
        strBuffer.append("<TransExeDate>2006-05-31</TransExeDate> ");
        strBuffer.append("<TransExeTime>09:00:00-05:00</TransExeTime> ");
        strBuffer.append("<OLifE>");
        strBuffer.append("<BeneficiaryData>");
        strBuffer.append("<MirCvgNum>");
        strBuffer.append(strCoverageNo);
        strBuffer.append("</MirCvgNum>");
        strBuffer.append("<MirPolId>");
        strBuffer.append("<MirPolIdBase>");
        strBuffer.append(PolicyNo);       
        strBuffer.append("</MirPolIdBase>");
        strBuffer.append("<MirPolIdSfx>");
        strBuffer.append(suffix);
        strBuffer.append("</MirPolIdSfx>");
        strBuffer.append("</MirPolId>");
        strBuffer.append("</BeneficiaryData>");
        strBuffer.append("</OLifE>");
        strBuffer.append("</TXLifeRequest>");
        strBuffer.append("</TXLife>");
        try {
        	StringBuffer sb = new StringBuffer();
	    	sb.append("BF6945 - InquiryBeneficiary_PolNo-");
	    	sb.append(strPolicyId);             	
            hs_result = invokeIngenium.submit(strBuffer.toString(),sb.toString());
            if(printParsedXML)
            {
            	Object strkeys[] = hs_result.keySet().toArray();
            	for(int i=0;i<strkeys.length;i++)
            	{
            		ArrayList al_result = (ArrayList)hs_result.get(strkeys[i]);
            		LOGGER.debug(strkeys[i] + ": ");
            		for(int j=0;j<al_result.size();j++)
            			LOGGER.debug("\t" + al_result.get(j));
            	}
            }
            
        } catch (Exception ex) {
        	LOGGER.error(CodeHelper.getStackTrace(ex));
        }
        LOGGER.info("BF6945_Append end");
        return hs_result;
    }
 
    /**
    * Handles the BF0592 business requests
    * performs Auto-App/Clear Case Processing
    **/
    //CLEAR CASE SUBMIT
    public Object BF0592_Append(String Authenticate[],String strPolicyId, String ClientID)
    {
    	
    	LOGGER.info("BF0592_Append start");
    	HashMap hs_result = null;
    	String PolicyNo = "";
        String suffix = "";
        if(strPolicyId.length() > 9)
        {
        	PolicyNo = strPolicyId.substring(0, 9);
        	suffix = ""+strPolicyId.charAt(9);
        }else
        	PolicyNo = strPolicyId;
    	StringBuffer strBuffer = new StringBuffer();
    	strBuffer.append(createLogin(Authenticate));
    	strBuffer.append("<TXLifeRequest>");
    	strBuffer.append("<TransRefGUID>2996b11f-e5b2-d00f-281b-03582683cd49</TransRefGUID> ");
        strBuffer.append("<TransType tc=\"ClearCaseSubmit\">ClearCaseSubmit</TransType>");
        strBuffer.append("<TransExeDate>2006-05-31</TransExeDate> ");
        strBuffer.append("<TransExeTime>09:00:00-05:00</TransExeTime> ");
        strBuffer.append("<OLifE>");
        strBuffer.append("<MirCommonFields>");
       
        strBuffer.append("<MirCliIdG>");
        strBuffer.append("<MirCliIdT>");
        strBuffer.append(ClientID);
        strBuffer.append("</MirCliIdT>");
        strBuffer.append("</MirCliIdG>");
        
        strBuffer.append("<MirPolId>");
        strBuffer.append("<MirPolIdBase>");
        strBuffer.append(PolicyNo);       
        strBuffer.append("</MirPolIdBase>");
        strBuffer.append("<MirPolIdSfx>");
        strBuffer.append(suffix);
        strBuffer.append("</MirPolIdSfx>");
        strBuffer.append("</MirPolId>");
        strBuffer.append("<MirDvUwPrcesCd>D</MirDvUwPrcesCd>"); 
        strBuffer.append("</MirCommonFields>");
        strBuffer.append("</OLifE>");
        strBuffer.append("</TXLifeRequest> ");
        strBuffer.append("</TXLife>");
        
        try {
        	StringBuffer sb = new StringBuffer();
	    	sb.append("BF0592 - ClearCaseSubmit_PolNo-");
	    	sb.append(strPolicyId);    
            hs_result = invokeIngenium.submit(strBuffer.toString(),sb.toString());
            if(printParsedXML)
            {
            	Object strkeys[] = hs_result.keySet().toArray();
            	for(int i=0;i<strkeys.length;i++)
            	{
            		ArrayList al_result = (ArrayList)hs_result.get(strkeys[i]);
            		LOGGER.debug(strkeys[i] + ": ");
            		for(int j=0;j<al_result.size();j++)
            			LOGGER.debug("\t" + al_result.get(j));
            	}
            }
            
        } catch (Exception ex) {
        	LOGGER.error(CodeHelper.getStackTrace(ex));
        }
        LOGGER.info("BF0592_Append end");
        return hs_result;
    }

    /**
     * Handles the BF1194 business requests
     * list the requirements for a given policy
     **/    
    public Object CLEARCASE(String Authenticate[],String strPolicyId){
   	 
    	LOGGER.info("CLEARCASE start");
        HashMap hs_result = null;
        String PolicyNo = "";
        String suffix = "";
        if(strPolicyId.length() > 9)
        {
        	PolicyNo = strPolicyId.substring(0,9);
        	suffix = ""+strPolicyId.charAt(9);
        }else
       	 PolicyNo =  strPolicyId;
        
        StringBuffer strBuffer = new StringBuffer();
        strBuffer.append(createLogin(Authenticate));
        strBuffer.append("<TXLifeRequest>");
        strBuffer.append("<TransRefGUID>2996b11f-e5b2-d00f-281b-03582683cd49</TransRefGUID> ");
        strBuffer.append("<TransType tc=\"ClearCaseMessages\">ClearCaseMessages</TransType>");
        strBuffer.append("<TransExeDate>2006-05-31</TransExeDate> ");
        strBuffer.append("<TransExeTime>09:00:00-05:00</TransExeTime> ");
        strBuffer.append("<OLifE>");
        strBuffer.append("<MirPolId>");
        strBuffer.append("<MirPolIdBase>");
        strBuffer.append(PolicyNo);
        strBuffer.append("</MirPolIdBase>");
        strBuffer.append("<MirPolIdSfx>");
        strBuffer.append(suffix);
        strBuffer.append("</MirPolIdSfx>");         
        strBuffer.append("</MirPolId>");
        strBuffer.append("</OLifE>");
        strBuffer.append("</TXLifeRequest>");
        strBuffer.append("</TXLife>");
        try {
        	StringBuffer sb = new StringBuffer();
	    	sb.append("CLEARCASE - ClearCaseMessages_PolNo-");
	    	sb.append(strPolicyId);  
       	 hs_result = invokeIngenium.submit(strBuffer.toString(),sb.toString());
       	 if(printParsedXML)
            { 
       		 Object strkeys[] = hs_result.keySet().toArray();
       		 for(int i=0;i<strkeys.length;i++)
       		 {
       			 ArrayList al_result = (ArrayList)hs_result.get(strkeys[i]);
       			 LOGGER.debug(strkeys[i] + ": ");
       			 for(int j=0;j<al_result.size();j++)
       				 LOGGER.debug("\t" + al_result.get(j));
       		 }
            }
       	 
        } catch (Exception ex) {
        	LOGGER.error(CodeHelper.getStackTrace(ex));
        }
        LOGGER.info("CLEARCASE end");
        return hs_result;
    }
    
    public Object clearCaseIum(String Authenticate[],String strPolicyId){
   	 
    	LOGGER.info("clearCaseIum start");
        HashMap hs_result = null;
        String PolicyNo = "";
        String suffix = "";
        if(strPolicyId.length() > 9)
        {
        	PolicyNo = strPolicyId.substring(0,9);
        	suffix = ""+strPolicyId.charAt(9);
        }else
       	 PolicyNo =  strPolicyId;
        
        StringBuffer strBuffer = new StringBuffer();
        strBuffer.append(createLogin(Authenticate));
        strBuffer.append("<TXLifeRequest>");
        strBuffer.append("<TransRefGUID>2996b11f-e5b2-d00f-281b-03582683cd49</TransRefGUID> ");
        strBuffer.append("<TransType tc=\"ClearCaseMessages\">ClearCaseMessages</TransType>");
        strBuffer.append("<TransExeDate>2006-05-31</TransExeDate> ");
        strBuffer.append("<TransExeTime>09:00:00-05:00</TransExeTime> ");
        strBuffer.append("<OLifE>");
        strBuffer.append("<SrcSystem>WMS</SrcSystem>");
        strBuffer.append("<MirPolId>");
        strBuffer.append("<MirPolIdBase>");
        strBuffer.append(PolicyNo);
        strBuffer.append("</MirPolIdBase>");
        strBuffer.append("<MirPolIdSfx>");
        strBuffer.append(suffix);
        strBuffer.append("</MirPolIdSfx>");         
        strBuffer.append("</MirPolId>");
        strBuffer.append("</OLifE>");
        strBuffer.append("</TXLifeRequest>");
        strBuffer.append("</TXLife>");
        try {
        	StringBuffer sb = new StringBuffer();
	    	sb.append("CLEARCASE - ClearCaseMessages_PolNo-");
	    	sb.append(strPolicyId);  
       	 hs_result = invokeIngenium.submit(strBuffer.toString(),sb.toString());
       	 if(printParsedXML)
            { 
       		 Object strkeys[] = hs_result.keySet().toArray();
       		 for(int i=0;i<strkeys.length;i++)
       		 {
       			 ArrayList al_result = (ArrayList)hs_result.get(strkeys[i]);
       			 LOGGER.debug(strkeys[i] + ": ");
       			 for(int j=0;j<al_result.size();j++)
       				 LOGGER.debug("\t" + al_result.get(j));
       		 }
            }
       	 
        } catch (Exception ex) {
        	LOGGER.error(CodeHelper.getStackTrace(ex));
        }
        LOGGER.info("clearCaseIum end");
        return hs_result;
    }
    
    /**
    * Handles the BF1194 business requests
    * list the requirements for a given policy
    **/    
    
    public Object BF1194C_Append(String Authenticate[],String strClientId){
    	
    	LOGGER.info("BF1194C_Append start");
        HashMap hs_result = null;
        StringBuffer strBuffer = new StringBuffer();
        strBuffer.append(createLogin(Authenticate));
        strBuffer.append("<TXLifeRequest>");
        strBuffer.append("<TransRefGUID>2996b11f-e5b2-d00f-281b-03582683cd49</TransRefGUID> ");
        strBuffer.append("<TransType tc=\"RequirementsList\">RequirementsList</TransType>");
        strBuffer.append("<TransExeDate>2006-05-31</TransExeDate> ");
        strBuffer.append("<TransExeTime>09:00:00-05:00</TransExeTime> ");
        strBuffer.append("<OLifE>");
        strBuffer.append("<RequirementData>");
        strBuffer.append("<MirCommonFields>");
        strBuffer.append("<MirCliId>");
        strBuffer.append(strClientId);
        strBuffer.append("</MirCliId>");
        strBuffer.append("<MirPolId />");
        strBuffer.append("</MirCommonFields>");
        strBuffer.append("</RequirementData>");
        strBuffer.append("</OLifE>");
        strBuffer.append("</TXLifeRequest>");
        strBuffer.append("</TXLife>");
        try {
        	StringBuffer sb = new StringBuffer();
	    	sb.append("BF1194 - RequirementsList_CliId-");
	    	sb.append(strClientId);  
            hs_result = invokeIngenium.submit(strBuffer.toString(),sb.toString());
            if(printParsedXML)
            {
            	Object strkeys[] = hs_result.keySet().toArray();
            	for(int i=0;i<strkeys.length;i++)
            	{
            		ArrayList al_result = (ArrayList)hs_result.get(strkeys[i]);
            		LOGGER.debug(strkeys[i] + ": ");
            		for(int j=0;j<al_result.size();j++)
            			LOGGER.debug("\t" + al_result.get(j));
            	}
            }
            
        } catch (Exception ex) {
        	LOGGER.error(CodeHelper.getStackTrace(ex));
        }
        LOGGER.info("BF1194C_Append end");
        return hs_result;
    }
    
 
    public Object BF1194_Append(String Authenticate[],String strPolicyId){
    	
    	LOGGER.info("BF1194_Append start");
        HashMap hs_result = null;
        String PolicyNo = "";
        String suffix = "";
        if(strPolicyId.length() > 9)
        {
        	PolicyNo = strPolicyId.substring(0, 9);
        	suffix = ""+strPolicyId.charAt(9);
        }else
       	 	PolicyNo =  strPolicyId;
        
        StringBuffer strBuffer = new StringBuffer();
        strBuffer.append(createLogin(Authenticate));
        strBuffer.append("<TXLifeRequest>");
        strBuffer.append("<TransRefGUID>2996b11f-e5b2-d00f-281b-03582683cd49</TransRefGUID> ");
        strBuffer.append("<TransType tc=\"RequirementsList\">RequirementsList</TransType>");
        strBuffer.append("<TransExeDate>2006-05-31</TransExeDate> ");
        strBuffer.append("<TransExeTime>09:00:00-05:00</TransExeTime> ");
        strBuffer.append("<OLifE>");
        strBuffer.append("<RequirementData>");
        strBuffer.append("<MirCommonFields>");
        strBuffer.append("<MirPolId>");
        strBuffer.append("<MirPolIdBase>"); 
        strBuffer.append(PolicyNo); 
        strBuffer.append("</MirPolIdBase>");
        strBuffer.append("<MirPolIdSfx>"); 
        strBuffer.append(suffix);
        strBuffer.append("</MirPolIdSfx>"); 
        strBuffer.append("</MirPolId>");
        strBuffer.append("</MirCommonFields>");
        strBuffer.append("</RequirementData>");
        strBuffer.append("</OLifE>");
        strBuffer.append("</TXLifeRequest>");
        strBuffer.append("</TXLife>");
        try {
           	StringBuffer sb = new StringBuffer();
	    	sb.append("BF1194 - RequirementsList_PolNo-");
	    	sb.append(strPolicyId);  
            hs_result = invokeIngenium.submit(strBuffer.toString(),sb.toString());
            if(printParsedXML)
            {
            	Object strkeys[] = hs_result.keySet().toArray();
            	for(int i=0;i<strkeys.length;i++)
            	{
            		ArrayList al_result = (ArrayList)hs_result.get(strkeys[i]);
            		LOGGER.debug(strkeys[i] + ": ");
            		for(int j=0;j<al_result.size();j++)
            			LOGGER.debug("\t" + al_result.get(j));

            	}
            }
            
        } catch (Exception ex) {
        	LOGGER.error(CodeHelper.getStackTrace(ex));
        }
        LOGGER.info("BF1194_Append end");
        return hs_result;
    }
    
    
    /**
    * Handles the BF1192 business requests
    * Updates the requirement records (status, image reference #) using PolicyID
    * paramters 
     * Authenticate[] = username and password screen
     * strPolicyID = policy ID
     * strSeqNum = sequence number of the requirement to be updated
     * strNewReqStatus = new status of the requirement
     * String strReqId = new Requirements ID
     * strNewIndexNo = new IndexNo
    **/    
    public Object BF1192C_Append(String Authenticate[],String strClientId, String strSeqNum, String strNewReqStatus, String strReqId, String strNewIndexNo){
    	
    	LOGGER.info("BF1192C_Append start");
        HashMap hs_result = null;
        String PolicyNo = "";
        String suffix = "";
        StringBuffer strBuffer = new StringBuffer();
        strBuffer.append(createLogin(Authenticate));
        strBuffer.append("<TXLifeRequest>");
        strBuffer.append("<TransRefGUID>2996b11f-e5b2-d00f-281b-03582683cd49</TransRefGUID> ");
        strBuffer.append("<TransType tc=\"RequirementsUpdate\">RequirementsUpdate</TransType>");
        strBuffer.append("<TransExeDate>2006-05-31</TransExeDate> ");
        strBuffer.append("<TransExeTime>09:00:00-05:00</TransExeTime> ");
        strBuffer.append("<OLifE> ");
        strBuffer.append("<RequirementData> ");
        strBuffer.append("<MirCommonFields> ");
        strBuffer.append("<MirCmpltnessInd>C</MirCmpltnessInd> ");
        strBuffer.append("<MirCliId>"); 
        strBuffer.append(strClientId);
        strBuffer.append("</MirCliId>");
        strBuffer.append("<MirReqirId>");
        strBuffer.append(strReqId);
        strBuffer.append("</MirReqirId>");
        strBuffer.append("<MirCpreqStatCd>");
        strBuffer.append(strNewReqStatus);
        strBuffer.append("</MirCpreqStatCd>");
        strBuffer.append("<MirCpreqSeqNum>");
        strBuffer.append(strSeqNum);
        strBuffer.append("</MirCpreqSeqNum>");
        strBuffer.append("<MirIndexNum>");
        strBuffer.append(strNewIndexNo);
        strBuffer.append("</MirIndexNum>");
        
        strBuffer.append("</MirCommonFields>");
        strBuffer.append("</RequirementData>");
        strBuffer.append("</OLifE>");
        strBuffer.append("</TXLifeRequest> ");
        strBuffer.append("</TXLife> ");
        
        try {
        	StringBuffer sb = new StringBuffer();
	    	sb.append("BF1192 - RequirementsUpdate_CliId-");
	    	sb.append(strClientId);  
            hs_result = invokeIngenium.submit(strBuffer.toString(),sb.toString());
            if(printParsedXML)
            {
            	Object strkeys[] = hs_result.keySet().toArray();
            	for(int i=0;i<strkeys.length;i++)
            	{
            		ArrayList al_result = (ArrayList)hs_result.get(strkeys[i]);
                	LOGGER.debug(strkeys[i] + ": ");
                	for(int j=0;j<al_result.size();j++)
                		LOGGER.debug("\t" + al_result.get(j));
            	}
            }
            
        } catch (Exception ex) {
        	LOGGER.error(CodeHelper.getStackTrace(ex));
        }
        LOGGER.info("BF1192C_Append end");
        return hs_result;
    }
      

    public Object BF1192_Append(String Authenticate[],String strMirPolIdBase, String strSeqNum, String strNewReqStatus, String strReqId, String strNewIndexNo)
    {  
    	LOGGER.info("BF1192_Append start");
    	HashMap hs_result = null;
        String PolicyNo = "";
        String suffix = "";
        if(strMirPolIdBase.length() > 9)
        {
        	PolicyNo = strMirPolIdBase.substring(0, 9);
        	suffix = ""+strMirPolIdBase.charAt(9);
        }else
        	PolicyNo = strMirPolIdBase;
        StringBuffer strBuffer = new StringBuffer();
        strBuffer.append(createLogin(Authenticate));
        strBuffer.append("<TXLifeRequest>");
        strBuffer.append("<TransRefGUID>2996b11f-e5b2-d00f-281b-03582683cd49</TransRefGUID> ");
        strBuffer.append("<TransType tc=\"RequirementsUpdate\">RequirementsUpdate</TransType>");
        strBuffer.append("<TransExeDate>2006-05-31</TransExeDate> ");
        strBuffer.append("<TransExeTime>09:00:00-05:00</TransExeTime> ");
        strBuffer.append("<OLifE>");
        strBuffer.append("<RequirementData> ");
        strBuffer.append("<MirCommonFields>");
        strBuffer.append("<MirCmpltnessInd>C</MirCmpltnessInd> ");
        strBuffer.append("<MirPolId>");
        strBuffer.append("<MirPolIdBase>");
        strBuffer.append(PolicyNo);
        strBuffer.append("</MirPolIdBase>");
        strBuffer.append("<MirPolIdSfx>"); 
        strBuffer.append(suffix);
        strBuffer.append("</MirPolIdSfx>"); 
        strBuffer.append("</MirPolId>");
        strBuffer.append("<MirReqirId>");
        strBuffer.append(strReqId);
        strBuffer.append("</MirReqirId>");
        strBuffer.append("<MirCpreqStatCd>");
        strBuffer.append(strNewReqStatus);
        strBuffer.append("</MirCpreqStatCd>");
        strBuffer.append("<MirCpreqSeqNum>");
        strBuffer.append(strSeqNum);
        strBuffer.append("</MirCpreqSeqNum>");
        strBuffer.append("<MirIndexNum>");
        strBuffer.append(strNewIndexNo);
        strBuffer.append("</MirIndexNum>");
        strBuffer.append("</MirCommonFields>");
        strBuffer.append("</RequirementData>");
        strBuffer.append("</OLifE>");
        strBuffer.append("</TXLifeRequest> ");
        strBuffer.append("</TXLife> ");
        
        try {
        	StringBuffer sb = new StringBuffer();
	    	sb.append("BF1192 - RequirementsUpdate_PolNo-");
	    	sb.append(strMirPolIdBase); 
            hs_result = invokeIngenium.submit(strBuffer.toString(),sb.toString());
            if(printParsedXML)
            {
            	Object strkeys[] = hs_result.keySet().toArray();
            	for(int i=0;i<strkeys.length;i++)
            	{
            		ArrayList al_result = (ArrayList)hs_result.get(strkeys[i]);
            		LOGGER.debug(strkeys[i] + ": ");
            		for(int j=0;j<al_result.size();j++)
            			LOGGER.debug("\t" + al_result.get(j));
            	}
            }
            
        } catch (Exception ex) {
        	LOGGER.error(CodeHelper.getStackTrace(ex));
        }
        LOGGER.info("BF1192_Append end");
        return hs_result;
    }
    
	public String getCDSInquiry(String[] authenticate, String policyNumber) {
		
		LOGGER.info("getCDSInquiry start");
		
		HashMap hs_result = null;
		String PolicyNo = "";
		String suffix = "";
		StringBuffer strBuffer = new StringBuffer();
		if(policyNumber.length() > 9)
		{
			PolicyNo = policyNumber.substring(0, 9);
			suffix = ""+policyNumber.charAt(9);
		}
		else
		{
			PolicyNo = policyNumber;
		}
		strBuffer.append(createLogin(authenticate))
			.append("<TXLifeRequest>")
			.append("<TransRefGUID></TransRefGUID> ")
			.append("<TransType tc=\"CDSInquiry\">CDSInquiry</TransType>  ")
			.append("<TransExeDate>2006-11-8</TransExeDate> ")
			.append("<TransExeTime>14:50:49</TransExeTime>")
		.append("<OLifE>")
		.append("<CDSInquiryData>")
			.append("<MirPolId>")
				.append("<MirPolIdBase>")
				.append(PolicyNo)
				.append("</MirPolIdBase>") 
				.append("<MirPolIdSfx>")
				.append(suffix)
				.append("</MirPolIdSfx>") 
			.append("</MirPolId>")
		.append("</CDSInquiryData>")
		.append("</OLifE>")  
		.append("</TXLifeRequest>")
		.append("</TXLife>");
		
		try {
			StringBuffer sb = new StringBuffer();
			sb.append("CDSInquiry_PolNo-");
			sb.append(policyNumber);
			hs_result = invokeIngenium.submit(strBuffer.toString(),sb.toString());
			if(printParsedXML)
			{
				Object strkeys[] = hs_result.keySet().toArray();
				for(int i=0;i<strkeys.length;i++)
				{
					ArrayList al_result = (ArrayList)hs_result.get(strkeys[i]);
					LOGGER.debug("TheLogger strkeys: " + strkeys[i] + ": ");
					
				}
			}
			
		} catch (Exception ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
		}
		
		LOGGER.info("getCDSInquiry ends");
		return invokeIngenium.getResponse();
	}
	
	public String getCDSRPCListInquiry(String[] authenticate, String policyNumber, String lastRelPolicyNumber, String lastRelPolicyNumberCovNum) {
		
		LOGGER.info("getCDSRPCListInquiry start");
		HashMap hs_result = null;
		String PolicyNo = "";
		String suffix = "";
		StringBuffer strBuffer = new StringBuffer();
		if(policyNumber.length() > 9)
		{
			PolicyNo = policyNumber.substring(0, 9);
			suffix = ""+policyNumber.charAt(9);
		}
		else
		{
			PolicyNo = policyNumber;
		}
		strBuffer.append(createLogin(authenticate))
			.append("<TXLifeRequest>")
			.append("<TransRefGUID /> ")
			.append("<TransType tc=\"CDSRPCList\" /> ")
			.append("<TransExeDate /> ")
			.append("<TransExeTime /> ")
		.append("<OLifE>")
		.append("<CDSRPCListData> ")
			.append("<MirPolId>")
				.append("<MirPolIdBase>")
				.append(PolicyNo)
				.append("</MirPolIdBase>") 
				.append("<MirPolIdSfx>")
				.append(suffix)
				.append("</MirPolIdSfx>") 
			.append("</MirPolId>")
			.append("<MirRelSysRefId>")
				.append("<MirRelSysRefPolId>")
				.append(lastRelPolicyNumber)
				.append("</MirRelSysRefPolId>")
				.append("<MirRelSysRefCvgNum>")
				.append(lastRelPolicyNumberCovNum)
				.append("</MirRelSysRefCvgNum>")
			.append("</MirRelSysRefId>")
			.append("<MirMoreCtr />")
			.append("<MirMoreInd />")
		.append("</CDSRPCListData>")
		.append("</OLifE>")  
		.append("</TXLifeRequest>")
		.append("</TXLife>");

		try {
			StringBuffer sb = new StringBuffer();
			sb.append("CDSRPCListInquiry_PolNo-");
			sb.append(policyNumber);
			sb.append("LastPolNo-");
			sb.append(lastRelPolicyNumber);
			sb.append("LastCvgNum-");
			sb.append(lastRelPolicyNumberCovNum);
			hs_result = invokeIngenium.submit(strBuffer.toString(),sb.toString());
			if(printParsedXML)
			{
				Object strkeys[] = hs_result.keySet().toArray();
				for(int i=0;i<strkeys.length;i++)
				{
					ArrayList al_result = (ArrayList)hs_result.get(strkeys[i]);
					LOGGER.debug("TheLogger strkeys: " + strkeys[i] + ": ");
				}
			}
			
		} catch (Exception ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
		}
		
		LOGGER.info("getCDSRPCListInquiry end");
		return invokeIngenium.getResponse();
	}
}
