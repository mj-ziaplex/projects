package ph.com.sunlife.wms.ws.ingenium;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.ResourceBundle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.slocpi.ium.util.CodeHelper;
import passwordgenerator.PasswordGenerator;
/**
 *
 * @author pm13
 */
public class IngeniumDispatcher {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(IngeniumDispatcher.class);
    PasswordGenerator pass;
    IngBusinessProcesses ingBusProcess;
    String Authenticate[] = new String[2];
    /** Creates a new instance of IngeniumDispatcher */
    
    static ResourceBundle rb = ResourceBundle.getBundle("com.ph.sunlife.component.properties.Ingenium");
    
    String effectiveDate = "";
    
    public IngeniumDispatcher() {
    }
    
    public void IngeniumInit(String p_username) {
    	
    	LOGGER.info("IngeniumInit start 1");
    	
    	ingBusProcess = new IngBusinessProcesses();
    	pass = new PasswordGenerator();
    	
    	StringBuffer sb = new StringBuffer();
 		sb.append(p_username);
 		if(rb.getString("Suffix") != null)
 			sb.append(rb.getString("Suffix"));
 		
    	Authenticate[0] = sb.toString();
        Authenticate[1] = "";

    	try{
    	
    		Authenticate[0] = sb.toString();
    		Authenticate[1] = pass.generatePassword (sb.toString());
    	
    	}catch(Exception ex){
    		LOGGER.error(CodeHelper.getStackTrace(ex));
        }
    	LOGGER.info("IngeniumInit end 1");
    }
    
    
    public void IngeniumInit(String p_username,String p_password) {
    	
    	LOGGER.info("IngeniumInit start 2");
    	Authenticate[0] = p_username;
        Authenticate[1] = p_password;

    	try{
    		LOGGER.debug("username is " + p_username +" password is " + p_password);
         }catch(Exception ex){
        	 LOGGER.error(CodeHelper.getStackTrace(ex));
        }
    	LOGGER.info("IngeniumInit end 2");
    }
    
    public HashMap getInquiryCoveragePremiums(String strMirPolIdBase, String Coverage_Number){
    	
    	LOGGER.info("getInquiryCoveragePremiums start");
    	
    	HashMap hashmap = null;
    	
    	try{
        	hashmap = (HashMap)ingBusProcess.InquiryCoveragePremiums_Append(Authenticate, strMirPolIdBase, Coverage_Number);
    	}catch(Exception ex){
    		LOGGER.error(CodeHelper.getStackTrace(ex));
        }
    	
    	LOGGER.info("getInquiryCoveragePremiums end");
        return hashmap;	
    }
    
    public HashMap getInquiryPolicyValues(String strPolicyNo, String EffectiveDt){
    	
    	LOGGER.info("getInquiryPolicyValues start 1");
    	
    	HashMap hashmap = null;
    	
    	try{
        	hashmap = (HashMap)ingBusProcess.InquiryPolicyValues_Append(Authenticate, strPolicyNo, EffectiveDt);
           
    	}catch(Exception ex){
    		LOGGER.error(CodeHelper.getStackTrace(ex));
        }
    	LOGGER.info("getInquiryPolicyValues end 1");
        return hashmap;	
    }
    
    
    public HashMap getInquiryPolicyValues(String strPolicyNo){
    	
    	LOGGER.info("getInquiryPolicyValues start 2");
    	HashMap hashmap = null;
    	
    	try{
        	hashmap = (HashMap)ingBusProcess.InquiryPolicyValues_Append(Authenticate, strPolicyNo);
    	}catch(Exception ex){
    		LOGGER.error(CodeHelper.getStackTrace(ex));
        }
    	LOGGER.info("getInquiryPolicyValues end 2");
        return hashmap;	
    }
    
    
    
    public HashMap getInquiryULDetails(String strPolicyNo, String Coverage_Number){
    	
    	LOGGER.info("getInquiryULDetails start 1");
    	HashMap hashmap = null;
    	
    	try{
        	hashmap = (HashMap)ingBusProcess.InquiryULDetails_Append(Authenticate, strPolicyNo, Coverage_Number);
    	}catch(Exception ex){
    		LOGGER.error(CodeHelper.getStackTrace(ex));
        }
    	LOGGER.info("getInquiryULDetails start 1");
        return hashmap;	
    }
    
    public HashMap getInquiryULDetails(String strPolicyNo, String Coverage_Number, String EffectiveDt){
    	
    	LOGGER.info("getInquiryULDetails start 2");
    	HashMap hashmap = null;
    	
    	try{
        	hashmap = (HashMap)ingBusProcess.InquiryULDetails_Append(Authenticate, strPolicyNo, Coverage_Number, EffectiveDt);
           //hashmap.put(key, value)
    	}catch(Exception ex){
    		LOGGER.error(CodeHelper.getStackTrace(ex));
        }
    	LOGGER.info("getInquiryULDetails end 2");
        return hashmap;	
    }

    
    public HashMap getDeferredActivityList(String strPolicyNo,String Effective_Date){
    	
    	LOGGER.info("getDeferredActivityList start 1");
    	HashMap hashmap = null;
    	
    	try{
        	hashmap = (HashMap)ingBusProcess.DeferredActivityList_Append(Authenticate, strPolicyNo,Effective_Date);
           //hashmap.put(key, value)
    	}catch(Exception ex){
    		LOGGER.error(CodeHelper.getStackTrace(ex));
        }
    	LOGGER.info("getDeferredActivityList end 1");
        return hashmap;	
    	
    }
    
    
    public HashMap getDeferredActivityList(String strPolicyNo){
    	
    	LOGGER.info("getDeferredActivityList start 2");
    	HashMap hashmap = null;
    	
    	try{
        	hashmap = (HashMap)ingBusProcess.DeferredActivityList_Append(Authenticate, strPolicyNo);           
    	}catch(Exception ex){
    		LOGGER.error(CodeHelper.getStackTrace(ex));
        }
    	LOGGER.info("getDeferredActivityList end 2");
        return hashmap;	
    	
    }
    
    
    
    public HashMap getFundTransferPercentToPercent(String strPolicyNo,String Effective_Date){
    	
    	LOGGER.info("getFundTransferPercentToPercent start");
    	HashMap hashmap = null;
    	
    	try{
        	hashmap = (HashMap)ingBusProcess.FundTransferPercentToPercent_Append(Authenticate, strPolicyNo,Effective_Date);
    	}catch(Exception ex){
    		LOGGER.error(CodeHelper.getStackTrace(ex));
        }
    	LOGGER.info("getFundTransferPercentToPercent end");
        return hashmap;	
    	
    }
    
    
    public HashMap getFundTransferPercentToPercent(String strPolicyNo){
    	
    	LOGGER.info("getFundTransferPercentToPercent start");
    	HashMap hashmap = null;
    	
    	try{
        	hashmap = (HashMap)ingBusProcess.FundTransferPercentToPercent_Append(Authenticate, strPolicyNo);
           //hashmap.put(key, value)
    	}catch(Exception ex){
    		LOGGER.error(CodeHelper.getStackTrace(ex));
        }
    	LOGGER.info("getFundTransferPercentToPercent end");
        return hashmap;	
    	
    }
    
    public HashMap getPolicyAllocationList(String strPolicyNo,String Effective_Date){
    	
    	LOGGER.info("getPolicyAllocationList start 1");
    	HashMap hashmap = null;
    	
    	try{
        	hashmap = (HashMap)ingBusProcess.PolicyAllocationList_Append(Authenticate, strPolicyNo,Effective_Date);
           
    	}catch(Exception ex){
    		LOGGER.error(CodeHelper.getStackTrace(ex));
        }
    	LOGGER.info("getPolicyAllocationList end 1");
        return hashmap;	
    	
    }
    
    
    public HashMap getPolicyAllocationList(String strPolicyNo){

    	LOGGER.info("getPolicyAllocationList start 2");
    	HashMap hashmap = null;
    	
    	try{
        	hashmap = (HashMap)ingBusProcess.PolicyAllocationList_Append(Authenticate, strPolicyNo);

    	}catch(Exception ex){
    		LOGGER.error(CodeHelper.getStackTrace(ex));
        }
    	LOGGER.info("getPolicyAllocationList end 2");
        return hashmap;	
    	
    }
    
    private String initEffectiveDate() {
    	
    	LOGGER.info("initEffectiveDate start");
    	ResourceBundle rbIngCode = ResourceBundle.getBundle("com.ph.sunlife.component.properties.Ingenium");
    	String effectiveDate = rbIngCode.getString("EFFECTIVE_DATE");
    	
    	Date currentDate = new Date();
    	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    	LOGGER.info("initEffectiveDate end");
    	return (effectiveDate.equals("")) ? sdf.format(currentDate):effectiveDate;
    }
    
    public void setEffectiveDate(String effectDate) {
    	effectiveDate = effectDate;
    }
    
    /*
     * Added by Rommel Suarez
     * Date December 19, 2008
     * New requirement for getting PolicyAllocation Inquiry
     */
    public HashMap getPolicyAllocationInquiry(String strPolicyNo)
    {
    	LOGGER.info("getPolicyAllocationInquiry start");
    	HashMap hashmap = null;
    	
    	try{
        	hashmap = (HashMap)ingBusProcess.PolicyAllocationInquiry_Append(Authenticate, strPolicyNo);
         }catch(Exception ex){
        	 LOGGER.error(CodeHelper.getStackTrace(ex));
        }
    	LOGGER.info("getPolicyAllocationInquiry end");
        return hashmap;	
    }
    
    
    public HashMap getInquiryBilling(String strPolicyNo)
    {
    	LOGGER.info("getInquiryBilling start");
    	HashMap hashmap = null;
    	
    	try{
        	hashmap = (HashMap)ingBusProcess.InquiryBilling_Append(Authenticate, strPolicyNo);
         }catch(Exception ex){
        	 LOGGER.error(CodeHelper.getStackTrace(ex));
        }
    	LOGGER.info("getInquiryBilling end");
        return hashmap;	
    }
    
    /*
    */    
    public HashMap createKoReqts(String strPolicyNo)
    {
    	
    	LOGGER.info("createKoReqts start");
    	HashMap hashmap = null;
    	
    	try{
        	hashmap = (HashMap)ingBusProcess.CLEARCASE(Authenticate, strPolicyNo);
         }catch(Exception ex){
        	 LOGGER.error(CodeHelper.getStackTrace(ex));
        }
    	LOGGER.info("createKoReqts end");
    	
        return hashmap;	
    }
    
    public HashMap createClientRequirement(String strClientId, String ReqtId, String reqStat){
    	
    	LOGGER.info("createClientRequirement start");
    	HashMap hashmap = null;
    	
    	try{
        	hashmap = (HashMap)ingBusProcess.BF1191C_Append(Authenticate,strClientId,ReqtId,reqStat);
        
         }catch(Exception ex){
        	 LOGGER.error(CodeHelper.getStackTrace(ex));
        }
    	LOGGER.info("createClientRequirement end");
        return hashmap;
    } 
    /*
    */    
    public HashMap createPolicyRequirement(String strMirPolIdBase, String ReqtId, String reqStat){
    	
    	LOGGER.info("createPolicyRequirement start");
    	HashMap hashmap = null;
    	try{
    		hashmap = (HashMap)ingBusProcess.BF1191_Append(Authenticate,strMirPolIdBase,ReqtId,reqStat);
    	    	
       }catch(Exception ex){
    	   LOGGER.error(CodeHelper.getStackTrace(ex));
       }
    	LOGGER.info("createPolicyRequirement end");
       return hashmap;
    } 
    
    public HashMap createClientRequirement(String strClientId, String ReqtId, String reqStat,String strIndexNo){
    	
    	LOGGER.info("createClientRequirement start");
    	HashMap hashmap = null;
    	
    	try{
        	hashmap = (HashMap)ingBusProcess.BF1191C_Append(Authenticate,strClientId,ReqtId,reqStat,strIndexNo);
        
         }catch(Exception ex){
        	 LOGGER.error(CodeHelper.getStackTrace(ex));
        }
    	LOGGER.info("createClientRequirement end");
        return hashmap;
    } 
    /*
    */    
    public HashMap createPolicyRequirement(String strMirPolIdBase, String ReqtId, String reqStat, String strIndexNo){
    	
    	LOGGER.info("createPolicyRequirement start");
    	HashMap hashmap = null;
    	try{
    		hashmap = (HashMap)ingBusProcess.BF1191_Append(Authenticate,strMirPolIdBase,ReqtId,reqStat,strIndexNo);
    		
       }catch(Exception ex){
    	   LOGGER.error(CodeHelper.getStackTrace(ex));
       }
    	LOGGER.info("createPolicyRequirement end");
       return hashmap;
    } 
    
    public HashMap CoverageAllDetails(String strPolicyId)
    {
    	LOGGER.info("CoverageAllDetails initialize");
    	return (HashMap)ingBusProcess.BF8020_Append(Authenticate,strPolicyId,"01");
    }
    /*
     *This function triggers auto-appraisal for a given policy.  
     It then returns to the caller whether the application was approved or rejected
     */
    public HashMap triggerAutoApp(String strPolicyId, String clientID){
        
    	LOGGER.info("triggerAutoApp start");
    	try{ 
        	HashMap hs = (HashMap)ingBusProcess.BF8020_Append(Authenticate,strPolicyId,"01");
        	HashMap hs2 = (HashMap)ingBusProcess.BF0592_Append(Authenticate,strPolicyId, clientID);  
        	hs2.put("COVERAGE_DECISION", hs.get("COVERAGE_DECISION"));
        	return hs2;
        	
        }catch(Exception ex){
        	ex.printStackTrace();
        	LOGGER.error(CodeHelper.getStackTrace(ex));
        }
    	LOGGER.info("triggerAutoApp end");
        return null;
    }
     /*
     *This function updates a requirement record (status and/or Filenet Image Reference #)
     */
    public void updateClientRequirement_void(String strClientId, String strSeqNum, String strNewReqStatus, String strReqId, String strNewRefNo){
    	
    	LOGGER.info("updateClientRequirement_void start 1");
    	HashMap hashmap = null;
    	try{
    		hashmap = (HashMap)ingBusProcess.BF1192C_Append(Authenticate,strClientId,strSeqNum,strNewReqStatus,strReqId,strNewRefNo);
    		
        }catch(Exception ex){
        	LOGGER.error(CodeHelper.getStackTrace(ex));
        }
    	LOGGER.info("updateClientRequirement_void end 1");
    } 
     /*
     *This function updates a requirement record (status and/or Filenet Image Reference #)
     */
    public void updateClientRequirement_void(String strClientId, String strSeqNum, String strNewReqStatus, String strReqId){
    	
    	LOGGER.info("updateClientRequirement_void start 2");
    	HashMap hashmap = null;
    	try{
            hashmap = (HashMap)ingBusProcess.BF1192C_Append(Authenticate,strClientId,strSeqNum,strNewReqStatus,strReqId,"");
           
        }catch(Exception ex){
        	LOGGER.error(CodeHelper.getStackTrace(ex));
        }
    	LOGGER.info("updateClientRequirement_void end 2");
    } 
     /*
     *This function updates a requirement record (status and/or Filenet Image Reference #)
     */
    public void updateClientRequirement_void(String strClientId, String strSeqNum, String strReqId){
    	
    	LOGGER.info("updateClientRequirement_void start 3");
    	HashMap hashmap = null;
    	try{
        	hashmap = (HashMap)ingBusProcess.BF1192C_Append(Authenticate,strClientId,strSeqNum,"",strReqId,"");
       
        }catch(Exception ex){
        	LOGGER.error(CodeHelper.getStackTrace(ex));
        }
    	LOGGER.info("updateClientRequirement_void end 3");
    } 
     /*
     *This function updates a requirement record (status and/or Filenet Image Reference #)
     */
    public void updatePolicyRequirement_void(String strPolicyId, String strSeqNum, String strNewReqStatus, String strReqId, String strNewRefNo){
    	
    	LOGGER.info("updatePolicyRequirement_void start 1");
        HashMap hashmap = null;
        try{
        hashmap = (HashMap)ingBusProcess.BF1192_Append(Authenticate,strPolicyId,strSeqNum,strNewReqStatus,strReqId,strNewRefNo);
   
        }catch(Exception ex){
        	LOGGER.error(CodeHelper.getStackTrace(ex));
        }
        LOGGER.info("updatePolicyRequirement_void end 1");
    } 
     /*
     *This function updates a requirement record (status and/or Filenet Image Reference #)
     */
    public void updatePolicyRequirement_void(String strPolicyId, String strSeqNum, String strNewReqStatus, String strReqId){
    	
    	LOGGER.info("updatePolicyRequirement_void start 2");
        HashMap hashmap = null;
        try{
        hashmap = (HashMap)ingBusProcess.BF1192_Append(Authenticate,strPolicyId,strSeqNum,strNewReqStatus,strReqId,"");
       
        }catch(Exception ex){
        	LOGGER.error(CodeHelper.getStackTrace(ex));
        }
        LOGGER.info("updatePolicyRequirement_void end 2");
    } 
    /*
     *This function updates a requirement record (status and/or Filenet Image Reference #)
     */
    public void updatePolicyRequirement_void(String strPolicyId, String strSeqNum, String strReqId){
        
    	LOGGER.info("updatePolicyRequirement_void start 3");
    	HashMap hashmap = null;
        try{
        hashmap = (HashMap)ingBusProcess.BF1192_Append(Authenticate,strPolicyId,strSeqNum,"",strReqId,"");
       
        }catch(Exception ex){
        	LOGGER.error(CodeHelper.getStackTrace(ex));
        }
        LOGGER.info("updatePolicyRequirement_void end 3");
    } 
    
    /*
     *This function updates a requirement record (status and/or Filenet Image Reference #)
     */
    public HashMap updateClientRequirement(String strClientId, String strSeqNum, String strNewReqStatus, String strReqId, String strNewRefNo){
    	
    	LOGGER.info("updateClientRequirement start 1");
    	HashMap hashmap = null;
    	try{
    		hashmap = (HashMap)ingBusProcess.BF1192C_Append(Authenticate,strClientId,strSeqNum,strNewReqStatus,strReqId,strNewRefNo);
    		
        }catch(Exception ex){
        	LOGGER.error(CodeHelper.getStackTrace(ex));
        }
    	LOGGER.info("updateClientRequirement end 1");
        return hashmap;
    } 
     /*
     *This function updates a requirement record (status and/or Filenet Image Reference #)
     */
    public HashMap updateClientRequirement(String strClientId, String strSeqNum, String strNewReqStatus, String strReqId){
    
    	LOGGER.info("updateClientRequirement start 2");
    	HashMap hashmap = null;
    	try{
        
            hashmap = (HashMap)ingBusProcess.BF1192C_Append(Authenticate,strClientId,strSeqNum,strNewReqStatus,strReqId,"");
          
        }catch(Exception ex){
        	LOGGER.error(CodeHelper.getStackTrace(ex));
        }
    	LOGGER.info("updateClientRequirement end 2");
        return hashmap;
    } 
     /*
     *This function updates a requirement record (status and/or Filenet Image Reference #)
     */
    public HashMap updateClientRequirement(String strClientId, String strSeqNum, String strReqId){
    
    	LOGGER.info("updatePolicyRequirement start");
    	HashMap hashmap = null;
    	try{
        	hashmap = (HashMap)ingBusProcess.BF1192C_Append(Authenticate,strClientId,strSeqNum,"",strReqId,"");
        	
        }catch(Exception ex){
        	LOGGER.error(CodeHelper.getStackTrace(ex));
        }
    	LOGGER.info("updatePolicyRequirement end");
        return hashmap;
    } 
     /*
     *This function updates a requirement record (status and/or Filenet Image Reference #)
     */
    public HashMap updatePolicyRequirement(String strPolicyId, String strSeqNum, String strNewReqStatus, String strReqId, String strNewRefNo){
        
    	LOGGER.info("updatePolicyRequirement start");
    	HashMap hashmap = null;
        try{
        hashmap = (HashMap)ingBusProcess.BF1192_Append(Authenticate,strPolicyId,strSeqNum,strNewReqStatus,strReqId,strNewRefNo);
        
        }catch(Exception ex){
        	LOGGER.error(CodeHelper.getStackTrace(ex));
        }
        LOGGER.info("updatePolicyRequirement start");
        return hashmap;
    } 
     /*
     *This function updates a requirement record (status and/or Filenet Image Reference #)
     */
    public HashMap updatePolicyRequirement(String strPolicyId, String strSeqNum, String strNewReqStatus, String strReqId){
    	
    	LOGGER.info("updatePolicyRequirement start");
        HashMap hashmap = null;
        try{
        hashmap = (HashMap)ingBusProcess.BF1192_Append(Authenticate,strPolicyId,strSeqNum,strNewReqStatus,strReqId,"");
       
        }catch(Exception ex){
        	LOGGER.error(CodeHelper.getStackTrace(ex));
        }
        LOGGER.info("updatePolicyRequirement end");
        return hashmap;
    } 
    /*
     *This function updates a requirement record (status and/or Filenet Image Reference #)
     */
    public HashMap updatePolicyRequirement(String strPolicyId, String strSeqNum, String strReqId){
    	
    	LOGGER.info("updatePolicyRequirement start");
        HashMap hashmap = null;
        try{
        hashmap = (HashMap)ingBusProcess.BF1192_Append(Authenticate,strPolicyId,strSeqNum,"",strReqId,"");
     
        }catch(Exception ex){
        	LOGGER.error(CodeHelper.getStackTrace(ex));
        }
        LOGGER.info("updatePolicyRequirement end");
        return hashmap;
    } 
    /*
     *This function retrieves all policy-related requirements for a given policy
     */
    public HashMap getPolicyRequirements(String strPolicyId){
    	
    	LOGGER.info("getPolicyRequirements start");
        HashMap hashmap = null;  
        try{
        hashmap = (HashMap)ingBusProcess.BF1194_Append(Authenticate,strPolicyId);
  
         }catch(Exception ex){
        	 LOGGER.error(CodeHelper.getStackTrace(ex));
        }
        LOGGER.info("getPolicyRequirements end");
        return hashmap;
    } 
    /*
     *This function retrieves all client-related requirements for a given client
     */
    public HashMap getClientRequirements(String strClientId){
    	LOGGER.info("getClientRequirements start");
        HashMap hashmap;  
        hashmap = (HashMap)ingBusProcess.BF1194C_Append(Authenticate,strClientId);
        LOGGER.info("getClientRequirements end");
        return hashmap;
    } 
    /*
     *This function retrieves all requirements (policy and client related) 
     for a given policy
     */
    public HashMap getRequirements(String strPolicyId, String insuredId, String OwnerId){
    	
    	LOGGER.info("getRequirements start");
        HashMap hashmap;  
        String strOwnerId; 
        String strInsuredId;
        ArrayList al_Temp;
        
        HashMap PolicyHashMap = (HashMap)ingBusProcess.BF1194_Append(Authenticate,strPolicyId);
        strOwnerId = OwnerId;
                
        strInsuredId = insuredId;
        
        hashmap = new HashMap();
        hashmap.put("POLICY", PolicyHashMap);
        if(strOwnerId.equals(strInsuredId)){
            hashmap.put("OWNER",(HashMap)ingBusProcess.BF1194C_Append(Authenticate,strInsuredId));
        }else{
            hashmap.put("OWNER",(HashMap)ingBusProcess.BF1194C_Append(Authenticate,strOwnerId));
            hashmap.put("INSURED",(HashMap)ingBusProcess.BF1194C_Append(Authenticate,strInsuredId));
        }
        
       
        al_Temp = null;
        LOGGER.info("getRequirements end");
        return hashmap;
    } 
    /*
     *This function retrieves all requirements (policy and client related) 
     for a given policy
     */
    private void consoliteRequirements(HashMap reqts, ArrayList SEQUENCE_NUMBERS, 
    		ArrayList REQUIREMENT_CODE,ArrayList INDEX_NUMBERS,
    		ArrayList STAT_CODES,String TYPE)
    {
    	LOGGER.info("consoliteRequirements start");
    	Iterator seq = SEQUENCE_NUMBERS.iterator();
    	Iterator reqt = REQUIREMENT_CODE.iterator();
    	Iterator index = INDEX_NUMBERS.iterator();
    	Iterator stat = STAT_CODES.iterator();
    	
    	ArrayList seqA = (ArrayList)reqts.get("SEQUENCE_NUMBERS");
    	ArrayList reqtA = (ArrayList)reqts.get("REQUIREMENT_CODE");
    	ArrayList indexA = (ArrayList)reqts.get("INDEX_NUMBERS");
    	ArrayList statA = (ArrayList)reqts.get("STAT_CODES");
    	ArrayList typeA = (ArrayList)reqts.get("TYPE");
    	
    	while(reqt.hasNext())
    	{
    		String sequence = "";
    		String requirements = (String)reqt.next();
    		String indexNumber = "";
    		String statsCodes = "";
    		
    		if(requirements.equalsIgnoreCase("*"))
    		{
    			seq.next();
    			index.next();
    			stat.next();
    			continue;
    		}
    			
    		try{
    			sequence = (String)seq.next();
    		}catch(Exception e)
    		{
    			LOGGER.error(CodeHelper.getStackTrace(e));
    		}
    		
    		try{
    			indexNumber = (String)index.next(); 
    		}catch(Exception e)
    		{
    			LOGGER.error(CodeHelper.getStackTrace(e));
    		}
    		
    		try{
    			statsCodes = (String)stat.next();
    		}catch(Exception e)
    		{
    			LOGGER.error(CodeHelper.getStackTrace(e));
    		}
    		seqA.add(sequence);
        	reqtA.add(requirements);
        	indexA.add(indexNumber);
        	statA.add(statsCodes);
        	typeA.add(TYPE);
        	LOGGER.info("consoliteRequirements end");
    	}
    	
    }
    public HashMap getPolicyInquiry(String strPolicyId)
    {
    	LOGGER.info("getPolicyInquiry initiated");
    	return (HashMap)ingBusProcess.BF8000_Append(Authenticate,strPolicyId);
    }
    
    
    public HashMap getRequirements(String strPolicyId){
    	
    	LOGGER.info("getRequirements start");
        HashMap hashmap;  
        String strOwnerId; 
        String strInsuredId;
        ArrayList al_Temp;
        
        HashMap PolicyHashMap = (HashMap)ingBusProcess.BF1194_Append(Authenticate,strPolicyId);
        HashMap OwnerHashMap = (HashMap)ingBusProcess.BF8000_Append(Authenticate,strPolicyId);
        HashMap InsuredHashMap = (HashMap)ingBusProcess.BF6925_Append(Authenticate,strPolicyId,"00");
        
        al_Temp = (ArrayList)OwnerHashMap.get("CLIENT_NO");
        strOwnerId = (String)al_Temp.get(0);
                
        al_Temp = (ArrayList)InsuredHashMap.get("INSURED_CLIENT_NO");
        strInsuredId = (String)al_Temp.get(0);
        
        hashmap = new HashMap();
        hashmap.put("SEQUENCE_NUMBERS",new ArrayList());
        hashmap.put("REQUIREMENT_CODE",new ArrayList());
        hashmap.put("INDEX_NUMBERS",new ArrayList());
        hashmap.put("STAT_CODES",new ArrayList());
        hashmap.put("TYPE",new ArrayList());
        
        try{
        	consoliteRequirements(hashmap, (ArrayList)PolicyHashMap.get("SEQUENCE_NUMBERS"),
        			(ArrayList)PolicyHashMap.get("REQUIREMENT_CODE"),(ArrayList)PolicyHashMap.get("INDEX_NUMBERS"),
        			(ArrayList)PolicyHashMap.get("STAT_CODES"),"Policy");
        }catch(Exception e)
        {
        	LOGGER.error(CodeHelper.getStackTrace(e));
        }
        
        if(strOwnerId.equals(strInsuredId)){
            HashMap hs = (HashMap)ingBusProcess.BF1194C_Append(Authenticate,strInsuredId);
            
            try{
            	consoliteRequirements(hashmap, (ArrayList)hs.get("SEQUENCE_NUMBERS"),
            			(ArrayList)hs.get("REQUIREMENT_CODE"),(ArrayList)hs.get("INDEX_NUMBERS"),
            			(ArrayList)hs.get("STAT_CODES"),"Owner");
            }catch(Exception e)
            {
            	LOGGER.error(CodeHelper.getStackTrace(e));
            }
        }else{ 
            try{
            	HashMap hs = (HashMap)ingBusProcess.BF1194C_Append(Authenticate,strOwnerId);
                consoliteRequirements(hashmap, (ArrayList)hs.get("SEQUENCE_NUMBERS"),
            			(ArrayList)hs.get("REQUIREMENT_CODE"),(ArrayList)hs.get("INDEX_NUMBERS"),
            			(ArrayList)hs.get("STAT_CODES"),"Owner");
            }catch(Exception e)
            {
            	LOGGER.error(CodeHelper.getStackTrace(e));
            }
            
            try{
            	HashMap hs = (HashMap)ingBusProcess.BF1194C_Append(Authenticate,strInsuredId);
                consoliteRequirements(hashmap, (ArrayList)hs.get("SEQUENCE_NUMBERS"),
            			(ArrayList)hs.get("REQUIREMENT_CODE"),(ArrayList)hs.get("INDEX_NUMBERS"),
            			(ArrayList)hs.get("STAT_CODES"),"Insured");
            }catch(Exception e)
            {
            	LOGGER.error(CodeHelper.getStackTrace(e));
            }
        }
        
  
        al_Temp = null;
        LOGGER.info("getRequirements end");
        return hashmap;
    } 
    /*
     *This function retrieves payment details about a given policy 
     (Amount Paid, Refund Amount, etc.)
     */
   public HashMap getPolicyPaymentDetails(String strPolicyId){
	   
	   LOGGER.info("getPolicyPaymentDetails start");
        HashMap hashmap;  
        hashmap = (HashMap)ingBusProcess.BF6983_Append(Authenticate,strPolicyId);
        Object strkeys[] = hashmap.keySet().toArray();
            for(int i=0;i<strkeys.length;i++)
            {
                ArrayList al_result = (ArrayList)hashmap.get(strkeys[i]);
                LOGGER.debug(strkeys[i] + ": ");
                for(int j=0;j<al_result.size();j++)
                    LOGGER.debug("\t" + al_result.get(j));
            }
            LOGGER.info("getPolicyPaymentDetails end");
        return hashmap;
    } 
   /*
    *This function retrieves a list of beneficiaries for all coverages in  a given Policy
    */
   public HashMap getPolicyBeneficiaries(String strPolicyId){
	   
	   LOGGER.info("getPolicyBeneficiaries start");
        HashMap hashmap;  
        
        hashmap = (HashMap)ingBusProcess.BF8000_Append(Authenticate,strPolicyId);
        
        if(hashmap!=null){
        	
            ArrayList tempArray = (ArrayList)hashmap.get("COVERAGE_COUNT");

            if(tempArray!=null){
	            String strTemp = (String)tempArray.get(0);
	            int nCounter = 0;
	            
	            while(nCounter < Integer.parseInt(strTemp))
	            {
	            	nCounter++;
	            }
            }
        }

        hashmap = getCoverageBeneficiaries(strPolicyId,"01");
        
        if(hashmap!=null){
	        Object strkeys[] = hashmap.keySet().toArray();
	            for(int i=0;i<strkeys.length;i++)
	            {
	                ArrayList al_result = (ArrayList)hashmap.get(strkeys[i]);
	                LOGGER.debug(strkeys[i] + ": ");
	                for(int j=0;j<al_result.size();j++)
	                	LOGGER.debug("\t" + al_result.get(j));
	            }
        }
        LOGGER.info("getPolicyBeneficiaries end");
        return hashmap;
    }
   /*
    *This function retrieves a list of beneficiaries for a specific coverage in  
    */
    public HashMap getCoverageBeneficiaries(String strPolicyId, String strCoverageNo){
    	
    	LOGGER.info("getCoverageBeneficiaries start");
        HashMap hashmap;  
        
        hashmap = (HashMap)ingBusProcess.BF6945_Append(Authenticate,strPolicyId,strCoverageNo);
        
        if(hashmap!=null){
	        Object strkeys[] = hashmap.keySet().toArray();
	            for(int i=0;i<strkeys.length;i++)
	            {
	                ArrayList al_result = (ArrayList)hashmap.get(strkeys[i]);
	                LOGGER.debug(strkeys[i] + ": ");
	                for(int j=0;j<al_result.size();j++)
	                    LOGGER.debug("\t" + al_result.get(j));
	        }
        }
        LOGGER.info("getCoverageBeneficiaries end");
        return hashmap;
    }
    /*
     *This function retrieves details for all coverages of a given policy
     *(Name of insured, policy number, plan, Face Amount, etc.) 
     */ 
    public HashMap getPolicyCoverageDetails(String strPolicyId, String CvgNum){
    	
    	LOGGER.info("getPolicyCoverageDetails start 1");
    	
         HashMap hashmap;  
         hashmap = (HashMap)ingBusProcess.BF6925_Append(Authenticate,strPolicyId, CvgNum);
                 
         LOGGER.info("getPolicyCoverageDetails end 1");
         return hashmap;
     }
    
    /*
     * This method is added for use by IUM. A different user/pwd is used for authentication hence it is passed
     * to this method and assigned to the Authenticate field
     */
    public HashMap getPolicyCoverageDetails(String strPolicyId, String CvgNum, String user, String pwd){
    	
    	LOGGER.info("getPolicyCoverageDetails start 2");
        HashMap hashmap;  
        Authenticate[0] = user;
        Authenticate[1] = pwd;
        ingBusProcess = new IngBusinessProcesses();
        hashmap = (HashMap)ingBusProcess.BF6925_Append(Authenticate,strPolicyId, CvgNum);
        LOGGER.info("getPolicyCoverageDetails end 2");
        return hashmap;
    }
    
    //Update Agent Details from Ingenium to IUM - Andre Ceasar Dacanay - start
    public HashMap getIngeniumConsolidatedInformation(String strPolicyId, String user, String pwd){
    	
    	LOGGER.info("getIngeniumConsolidatedInformation start");
        HashMap hashmap;  
        Authenticate[0] = user;
        Authenticate[1] = pwd;
        
        ingBusProcess = new IngBusinessProcesses();
        hashmap = (HashMap)ingBusProcess.InquiryConsolidatedInformation_Append(Authenticate,strPolicyId);
        
        LOGGER.info("getIngeniumConsolidatedInformation end");
        return hashmap;
    }
    //Update Agent Details from Ingenium to IUM - Andre Ceasar Dacanay - end
    
    public HashMap doClearCaseIum(String strPolicyId, String user, String pwd) {
    	
    	LOGGER.info("doClearCaseIum start");
    	HashMap hashmap;  
        Authenticate[0] = user;
        Authenticate[1] = pwd;
        ingBusProcess = new IngBusinessProcesses();
    	hashmap = (HashMap)ingBusProcess.clearCaseIum(Authenticate, strPolicyId);
    	
    	LOGGER.info("doClearCaseIum end");
    	return hashmap;
    }
    
    /*
     *This function retrieves the client information for a given policy 
     * (both for the Policy Owner and the Insured)
     * A new parameter is added which is to include the tradIndicator, VUL or TRAD are accepted as inputs
     */
    public HashMap getPolicyClientDetails(String strPolicyId){
        
    	LOGGER.info("getPolicyClientDetails start");
    	HashMap hashmap;  
        HashMap HashPolOwner = this.getPolicyOwnerDetails(strPolicyId);
        HashMap HasInsDetails = this.getPolicyInsuredDetails(strPolicyId,"01");
        hashmap = new HashMap();
        if(HashPolOwner != null)
       	 if(HasInsDetails!= null)
       	 {		 
       		 if(HashPolOwner.get("BRANCH_CODE") != null)
       			 hashmap.put("BRANCH_CODE",HashPolOwner.get("BRANCH_CODE"));
       		 if(HashPolOwner.get("SERVC_AGENT_CLIENT_CODE") != null)
       			 hashmap.put("SERVC_AGENT_CLIENT_CODE",HashPolOwner.get("SERVC_AGENT_CLIENT_CODE"));
       		 if(HashPolOwner.get("APP_SIGN_DATE") != null)
       			 hashmap.put("APP_SIGN_DATE",HashPolOwner.get("APP_SIGN_DATE"));
       		 if(HashPolOwner.get("OWNER_TITLE") != null)
       			 hashmap.put("OWNER_TITLE",HashPolOwner.get("OWNER_TITLE"));
       		 if(HashPolOwner.get("CLIENT_FIRSTNAME") != null)
       			 hashmap.put("CLIENT_FIRSTNAME",HashPolOwner.get("CLIENT_FIRSTNAME"));
       		 if(HashPolOwner.get("CLIENT_LASTNAME") != null)
       			 hashmap.put("CLIENT_LASTNAME",HashPolOwner.get("CLIENT_LASTNAME"));
       		 if(HashPolOwner.get("CLIENT_MIDDLENAME") != null)
       			 hashmap.put("CLIENT_MIDDLENAME",HashPolOwner.get("CLIENT_MIDDLENAME"));
       		 if(HashPolOwner.get("ADDRESS_1") != null)
       			 hashmap.put("ADDRESS_1",HashPolOwner.get("ADDRESS_1"));
       		 if(HashPolOwner.get("ADDRESS_2") != null)
       			 hashmap.put("ADDRESS_2",HashPolOwner.get("ADDRESS_2"));
       		 if(HashPolOwner.get("ADDRESS_3") != null)
       			 hashmap.put("ADDRESS_3",HashPolOwner.get("ADDRESS_3"));
       		 
       		 if(HashPolOwner.get("ADDRESS_4") != null)
       			 hashmap.put("ADDRESS_4",HashPolOwner.get("ADDRESS_4"));
       		 if(HashPolOwner.get("ADDRESS_5") != null)
       			 hashmap.put("ADDRESS_5",HashPolOwner.get("ADDRESS_5"));
       		 
       		 if(HashPolOwner.get("CITY") != null)
       			 hashmap.put("CITY",HashPolOwner.get("CITY"));
       		 if(HashPolOwner.get("PROVINCE_CODE") != null)
       			 hashmap.put("PROVINCE_CODE",HashPolOwner.get("PROVINCE_CODE"));
       		 if(HashPolOwner.get("CLIENT_BDAY") != null)
       			 hashmap.put("CLIENT_BDAY",HashPolOwner.get("CLIENT_BDAY"));
       		 if(HashPolOwner.get("PLAN_ID") != null)
       			 hashmap.put("PLAN_ID",HashPolOwner.get("PLAN_ID"));
       		 if(HashPolOwner.get("INS_TYPE_CODE") != null)
       			 hashmap.put("INS_TYPE_CODE",HashPolOwner.get("INS_TYPE_CODE"));
       		 if(HashPolOwner.get("OWNER_ID") != null)
       			 hashmap.put("OWNER_ID",HashPolOwner.get("OWNER_ID"));
       		 if(HashPolOwner.get("BRANCH") != null)
       			 hashmap.put("BRANCH",HashPolOwner.get("BRANCH"));
       		 if(HashPolOwner.get("COMPANY_NAME") != null)
       			 hashmap.put("COMPANY_NAME",HashPolOwner.get("COMPANY_NAME"));
       		 
       		 if(HashPolOwner.get("AGENT_CODE") != null)
       			 hashmap.put("AGENT_CODE",HashPolOwner.get("AGENT_CODE"));
       		 if(HashPolOwner.get("AGENT_NAME") != null)
       			 hashmap.put("AGENT_NAME",HashPolOwner.get("AGENT_NAME"));
       		 if(HashPolOwner.get("SERVICING_AGENT_NAME") != null)
       			 hashmap.put("SERVICING_AGENT_NAME",HashPolOwner.get("SERVICING_AGENT_NAME"));
       		 
       		 if(HashPolOwner.get("ZIPCODE") != null)
       			 hashmap.put("ZIPCODE",HashPolOwner.get("ZIPCODE"));
       		 else
       			 hashmap.put("ZIPCODE",HashPolOwner.get("ZIPCODE_PREVIOUS"));
       		 
       		 if(HashPolOwner.get("COUNTRY_CODE") != null)
       			 hashmap.put("COUNTRY_CODE",HashPolOwner.get("COUNTRY_CODE"));
       		 
       		 if(HashPolOwner.get("POLICY_STATUS_DESC") != null)
       			 hashmap.put("POLICY_STATUS_DESC",HashPolOwner.get("POLICY_STATUS_DESC"));
       		 
       		 if(HashPolOwner.get("CURRENCY_CODE") != null)
       			 hashmap.put("CURRENCY_CODE",HashPolOwner.get("CURRENCY_CODE"));
       		 if(HashPolOwner.get("SEX") != null)
       			 hashmap.put("SEX",HashPolOwner.get("SEX"));
       		 if(HasInsDetails.get("INSURED_NAME") != null)
       			 hashmap.put("INSURED_NAME",HasInsDetails.get("INSURED_NAME"));
       		 if(HasInsDetails.get("INSURED_CLIENT_NO") != null)
       			 hashmap.put("INSURED_CLIENT_NO",HasInsDetails.get("INSURED_CLIENT_NO"));
       		 
       		 if(HasInsDetails.get("ORIGINAL_FACE_AMOUNT") != null)
       			 hashmap.put("ORIGINAL_FACE_AMOUNT",HasInsDetails.get("ORIGINAL_FACE_AMOUNT"));
       		 if(HasInsDetails.get("COVERAGE_PLAN_DESC") != null)
       			 hashmap.put("COVERAGE_PLAN_DESC",HasInsDetails.get("COVERAGE_PLAN_DESC"));
       		 
        
       	 }
        
        
        Object strkeys[] = hashmap.keySet().toArray();
           for(int i=0;i<strkeys.length;i++)
           {
               ArrayList al_result = (ArrayList)hashmap.get(strkeys[i]);
               LOGGER.debug(strkeys[i] + ": ");
               for(int j=0;j<al_result.size();j++)
                   LOGGER.debug("\t" + al_result.get(j));
           } 
           LOGGER.info("getPolicyClientDetails end");
        return hashmap;
    }
   /*
    *This function retrieves information about the Client of a given policy
    */
    public HashMap getPolicyInsuredDetails(String strPolicyId, String CvgNum){
    	
    	LOGGER.info("getPolicyInsuredDetails start");
    	HashMap hashmap = (HashMap)ingBusProcess.BF6925_Append(Authenticate,strPolicyId, CvgNum);
    	LOGGER.info("getPolicyInsuredDetails end");
        return hashmap;
    }
    /*
     *This function retrieves the client information given client ID
     */
    public HashMap getClientDetails(String strClientId){
    	
    	LOGGER.info("getClientDetails start");
    	
         HashMap hashmap;  
         hashmap = (HashMap)ingBusProcess.BF1220C_Append(Authenticate,strClientId);
         if(hashmap == null)
        	 LOGGER.debug("equal to null");
         Object strkeys[] = hashmap.keySet().toArray();
            for(int i=0;i<strkeys.length;i++)
            {
                ArrayList al_result = (ArrayList)hashmap.get(strkeys[i]);
               
                for(int j=0;j<al_result.size();j++)
                    LOGGER.debug("\t" + al_result.get(j));
            }
            LOGGER.info("getClientDetails end");
            
         return hashmap;
     }
    
   /*
    *  This function retrieves information about the Owner of given policy 
    */
    public HashMap getPolicyOwnerDetails(String strPolicyId){
    	
    	LOGGER.info("getPolicyOwnerDetails start");
        HashMap hashmap;  
        String strClientNo;
        hashmap = (HashMap)ingBusProcess.BF8000_Append(Authenticate,strPolicyId);
      
        ArrayList planId = null;
        ArrayList ServicingAgent = null;
        ArrayList Agent = null;
        ArrayList Status = null;
        ArrayList Count = null;
        ArrayList Currency = null;
        ArrayList AgentCode = null;
        ArrayList Branch = null;
        ArrayList InsType = null;
        ArrayList BranchCode = null;
        ArrayList AppSignDate = null;
        ArrayList SrvcAgent = null;
        
        ArrayList Address1 = null;
        ArrayList Address2 = null;
        ArrayList Address3 = null;
        
        
        try{
        
        if(hashmap.get("PLAN_ID") != null)
       		 planId = (ArrayList)hashmap.get("PLAN_ID");
       	 if(hashmap.get("SERVC_AGENT_CLIENT_CODE") != null)
       		 SrvcAgent = (ArrayList)hashmap.get("SERVC_AGENT_CLIENT_CODE");
       	 if(hashmap.get("SERVICING_AGENT_NAME") != null)
       		 ServicingAgent = (ArrayList)hashmap.get("SERVICING_AGENT_NAME");
       	 if(hashmap.get("AGENT_NAME") != null)
       		 Agent = (ArrayList)hashmap.get("AGENT_NAME");
       	 if(hashmap.get("POLICY_STATUS_DESC") != null)
       		 Status = (ArrayList)hashmap.get("POLICY_STATUS_DESC");
       	 if(hashmap.get("COVERAGE_COUNT") != null)
       		 Count = (ArrayList)hashmap.get("COVERAGE_COUNT");
       	 if(hashmap.get("CURRENCY_CODE") != null)
       		 Currency = (ArrayList)hashmap.get("CURRENCY_CODE");
       	 if(hashmap.get("AGENT_CODE") != null)
       		 AgentCode = (ArrayList)hashmap.get("AGENT_CODE");
       	 if(hashmap.get("BRANCH") != null)
       		 Branch = (ArrayList)hashmap.get("BRANCH");
       	 if(hashmap.get("INS_TYPE_CODE") != null)
       		 InsType = (ArrayList)hashmap.get("INS_TYPE_CODE");
       	 if(hashmap.get("BRANCH_CODE") != null)
       		 BranchCode = (ArrayList)hashmap.get("BRANCH_CODE");
       	 if(hashmap.get("APP_SIGN_DATE") != null)
       		 AppSignDate = (ArrayList)hashmap.get("APP_SIGN_DATE");
       		
        }catch(Exception e)
        {
        	LOGGER.error(CodeHelper.getStackTrace(e));
        }
        
        try{
        	ArrayList al_Client_no = (ArrayList)hashmap.get("CLIENT_NO");
       	 strClientNo = (String)al_Client_no.get(0);
       	 hashmap = (HashMap)ingBusProcess.BF1220C_Append(Authenticate,strClientNo);
      
   	   	 if(planId != null);
       	 	hashmap.put("PLAN_ID", planId);
       	 if(ServicingAgent != null);
          	hashmap.put("SERVICING_AGENT_NAME",ServicingAgent);
         if(Agent != null);
          	hashmap.put("AGENT_NAME", Agent);
         if(Status != null);
       	 	hashmap.put("POLICY_STATUS_DESC", Status);
       	 if(Count != null)
       		 hashmap.put("COVERAGE_COUNT", Count);
       	 if(Currency != null)
       		 hashmap.put("CURRENCY_CODE", Currency);
       	 if(AgentCode != null)
       		 hashmap.put("AGENT_CODE", AgentCode);
       	 if(Branch != null)
       		 hashmap.put("BRANCH", Branch);
       	 if(InsType != null)
       		 hashmap.put("INS_TYPE_CODE", InsType);
       	 if(BranchCode != null)
       		 hashmap.put("BRANCH_CODE", BranchCode);
       	 if(AppSignDate != null)
       		 hashmap.put("APP_SIGN_DATE", AppSignDate);
       	 if(SrvcAgent != null)
       		 hashmap.put("SERVC_AGENT_CLIENT_CODE", SrvcAgent);
       	
       	
       	HashMap hs2 = (HashMap)ingBusProcess.InquiryBilling_Append(Authenticate,strPolicyId);

        if(hs2.get("ADDRESS_1") != null){
          	 Address1 = (ArrayList)hs2.get("ADDRESS_1");
           }
        if(hs2.get("ADDRESS_2") != null){
         	 Address2 = (ArrayList)hs2.get("ADDRESS_2");
          }
        if(hs2.get("ADDRESS_3") != null){
         	 Address3 = (ArrayList)hs2.get("ADDRESS_3");
          }
        List Address4 = null;
        if(hs2.get("ADDRESS_4") != null){
        	 Address4 = (ArrayList)hs2.get("ADDRESS_4");
         }
        List Address5 = null;
        if(hs2.get("ADDRESS_5") != null){
        	 Address5 = (ArrayList)hs2.get("ADDRESS_5");
         }
        
      	 if(Address1 != null){
 	 		hashmap.put("ADDRESS_1", Address1);
 	       	 Iterator it = Address1.iterator();
 	       	 while(it.hasNext()){
 	       		LOGGER.debug("dax ADDRESS_1 "+it.next().toString());
 	       	 }
      	 }
      	 if(Address2 != null){
 	 		hashmap.put("ADDRESS_2", Address2);
 	       	 Iterator it = Address2.iterator();
 	       	 while(it.hasNext()){
 	       		LOGGER.debug("dax ADDRESS_2 "+it.next().toString());
 	       	 }
      	 }
	  	 if(Address3 != null){
 	 		hashmap.put("ADDRESS_3", Address3);
 	       	 Iterator it = Address3.iterator();
 	       	 while(it.hasNext()){
 	       		LOGGER.debug("dax ADDRESS_3 "+it.next().toString());
 	       	 }
	  	 }
	  	 if(Address4 != null){
	 	 		hashmap.put("ADDRESS_4", Address4);
	 	       	 Iterator it = Address4.iterator();
	 	       	 while(it.hasNext()){
	 	       		LOGGER.debug("dax ADDRESS_4 "+it.next().toString());
	 	       	 }
		  	 }
	  	 if(Address5 != null){
	 	 		hashmap.put("ADDRESS_5", Address5);
	 	       	 Iterator it = Address5.iterator();
	 	       	 while(it.hasNext()){
	 	       		LOGGER.debug("dax ADDRESS_5 "+it.next().toString());
	 	       	 }
		  	 }
	  	
	  
        Object strkeys[] = hashmap.keySet().toArray();
         
        for(int i=0;i<strkeys.length;i++)
           {
               ArrayList al_result = (ArrayList)hashmap.get(strkeys[i]);
               for(int j=0;j<al_result.size();j++)
                   LOGGER.debug("\t" + al_result.get(j));
           }
        }catch(Exception e){
        	LOGGER.error(CodeHelper.getStackTrace(e));
        	return null; 
        }
        LOGGER.info("getPolicyOwnerDetails end");
        return hashmap;
    }
   
    public String getCDSInquiry(String policyNumber) {
    	
    	LOGGER.info("getCDSInquiry start");
		String response = "";

		try {
			response = ingBusProcess.getCDSInquiry(Authenticate, policyNumber);
			
		} catch (Exception ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
		}
		LOGGER.info("getCDSInquiry end");
		return response;
	}
    
    public String getCDSRPCListInquiry(String policyNumber, String lastRelPolicyNumber, String lastRelPolicyNumberCovNum) {
    	
    	LOGGER.info("getCDSRPCListInquiry start");
		String response = "";

		try {
			response = ingBusProcess.getCDSRPCListInquiry(Authenticate, policyNumber, lastRelPolicyNumber, lastRelPolicyNumberCovNum);
			
		} catch (Exception ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
		}
		LOGGER.info("getCDSRPCListInquiry end");
		return response;
	}
}