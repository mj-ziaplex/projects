/*******************************************************************************
 * Copyright (c) 2005, 2006 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/*
 * IngeniumHandler.java
 *
 * Created on June 13, 2006, 12:09 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package ph.com.sunlife.wms.ws.ingenium;

import java.util.ArrayList;
import java.util.HashMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

/**
 * 
 * @author pm13
 */

public class IngeniumHandler extends DefaultHandler {
	private static final Logger LOGGER = LoggerFactory.getLogger(IngeniumHandler.class);
	HashMap hm = new HashMap();

	String strHead = "";

	ArrayList alValuesFound = new ArrayList();

	StringBuffer sb;

	boolean bKeyFound;

	String strKey = "";

	IngeniumConstants ic = new IngeniumConstants();

	boolean ResultCode = false;

	/** Creates a new instance of IngeniumHandler */
	public IngeniumHandler() {
		super();
		LOGGER.info("initializing IngeniumHandler");
	}

	public void startDocument() {
	
	}

	public HashMap getHashmap() {

		return hm;
	}

	public void notationDecl(String name, String publicId, String systemId) {
		LOGGER.debug("notationDecl " + name + " , " + publicId + " , "
				+ systemId);
	}

	public void unparsedEntityDecl(String name, String publicId,
			String systemId, String notationName) {
		LOGGER.debug("unparsedEntityDecl " + name + " , " + publicId
				+ " , " + systemId);

	}

	public void endDocument() {
		
	}
	
	private void outLog(String methodName, String message, String value) {
		LOGGER.debug("[IngeniumHandler]["+methodName+"]["+message+"]["+value+"]");
	}

	public String translate(String strCode) {

		
		if (strCode.equals(ic.CITY_PREVIOUS)) {
			return "CITY_PREVIOUS";
		} else if (strCode.equals(ic.ADDRESS_3_PREVIOUS)) {
			return "ADDRESS_3_PREVIOUS";
		} else if (strCode.equals(ic.INSURED_CLIENT_NO)) {
			return "INSURED_CLIENT_NO";
		} else if (strCode.equals(ic.COUNTRY_CODE_PREVIOUS)) {
			return "COUNTRY_CODE_PREVIOUS";
		} else if (strCode.equals(ic.ADDRESS_2)) {
			return "ADDRESS_2";
		} else if (strCode.equals(ic.ADDRESS_4)) {
			return "ADDRESS_4";
		} else if (strCode.equals(ic.ADDRESS_5)) {
			return "ADDRESS_5";
		} else if (strCode.equals(ic.ADDRESS_6)) {
			return "ADDRESS_6";
		} else if (strCode.equals(ic.ADDRESS_3)) {
			return "ADDRESS_3";
		} else if (strCode.equals(ic.ADDRESS_1)) {
			return "ADDRESS_1";
		} else if (strCode.equals(ic.PROVINCE_CODE)) {
			return "PROVINCE_CODE";
		} else if (strCode.equals(ic.PROVINCE_PREVIOUS)) {
			return "PROVINCE_PREVIOUS";
		} else if (strCode.equals(ic.ZIPCODE_PREVIOUS)) {
			return "ZIPCODE_PREVIOUS";
		} else if (strCode.equals(ic.COUNTRY_CODE)) {
			return "COUNTRY_CODE";
		} else if (strCode.equals(ic.CITY)) {
			return "CITY";
		} else if (strCode.equals(ic.ZIPCODE)) {
			return "ZIPCODE";
		} else if (strCode.equals(ic.CLIENT_FIRSTNAME)) {
			return "CLIENT_FIRSTNAME";
		} else if (strCode.equals(ic.CLIENT_MIDDLENAME)) {
			return "CLIENT_MIDDLENAME";
		} else if (strCode.equals(ic.CLIENT_LASTNAME)) {
			return "CLIENT_LASTNAME";
		} else if (strCode.equals(ic.PLAN_ID)) {
			return "PLAN_ID";
		} else if (strCode.equals(ic.ORIGINAL_FACE_AMOUNT)) {
			return "ORIGINAL_FACE_AMOUNT";
		} else if (strCode.equals(ic.REFUND_AMOUNT)) {
			return "REFUND_AMOUNT";
		} else if (strCode.equals(ic.REQUIREMENT_ID)) {
			return "REQUIREMENT_ID";
		} else if (strCode.equals(ic.INDEX_NUMBER)) {
			return "INDEX_NUMBER";
		} else if (strCode.equals(ic.INDEX_NUMBERS)) {
			return "INDEX_NUMBERS";
		} else if (strCode.equals(ic.SEQUENCE_NUMBER)) {
			return "SEQUENCE_NUMBER";
		} else if (strCode.equals(ic.SEQUENCE_NUMBERS)) {
			return "SEQUENCE_NUMBERS";
		} else if (strCode.equals(ic.OWNER_TITLE)) {
			return "OWNER_TITLE";
		} else if (strCode.equals(ic.REQUIREMENT_CODE)) {
			return "REQUIREMENT_CODE";
		} else if (strCode.equals(ic.USER_MESSAGE)) {
			return "USER_MESSAGE";
		} else if (strCode.equals(ic.RETURN_CODE)) {
			return "RETURN_CODE";
		} else if (strCode.equals(ic.INSURED_NAME)) {
			return "INSURED_NAME";
		} else if (strCode.equals(ic.FACE_AMOUNT)) {
			return "FACE_AMOUNT";
		} else if (strCode.equals(ic.PLAN_ID_T)) {
			return "PLAN_ID";
		} else if (strCode.equals(ic.COVERAGE_NUMBER)) {
			return "COVERAGE_NUMBER";
		} else if (strCode.equals(ic.BENEFICIARY_NAME)) {
			return "BENEFICIARY_NAME";
		} else if (strCode.equals(ic.COVERAGE_NUMBER_T)) {
			return "COVERAGE_NUMBER";
		} else if (strCode.equals(ic.CLIENT_NO)) {
			return "CLIENT_NO";
		} else if (strCode.equals(ic.STAT_CODE)) {
			return "STAT_CODE";
		} else if (strCode.equals(ic.STAT_CODES)) {
			return "STAT_CODES";
		} else if (strCode.equals(ic.SEX)) {
			return "SEX";
		} else if (strCode.equals(ic.CLIENT_BDAY)) {
			return "CLIENT_BDAY";
		} else if (strCode.equals(ic.SERVICING_AGENT_NAME)) {
			return "SERVICING_AGENT_NAME";
		} else if (strCode.equals(ic.AGENT_NAME)) {
			return "AGENT_NAME";
		} else if (strCode.equals(ic.COVERAGE_COUNT)) {
			return "COVERAGE_COUNT";
		} else if (strCode.equals(ic.BENEFIT_CODE)) {
			return "BENEFIT_CODE";
		} else if (strCode.equals(ic.POLICY_STATUS_DESC)) {
			return "POLICY_STATUS_DESC";
		} else if (strCode.equals(ic.COVERAGE_PLAN_DESC)) {
			return "COVERAGE_PLAN_DESC";
		} else if (strCode.equals(ic.CURRENCY_CODE)) {
			return "CURRENCY_CODE";
		} else if (strCode.equals(ic.PREMIUM_SUSPENSE)) {
			return "PREMIUM_SUSPENSE";
		} else if (strCode.equals(ic.MODE_PREMIUM)) {
			return "MODE_PREMIUM";
		} else if (strCode.equals(ic.MISC_SUSPENSE_AMOUNT)) {
			return "MISC_SUSPENSE_AMOUNT";
		} else if (strCode.equals(ic.AGENT_CODE)) {
			return "AGENT_CODE";
		} else if (strCode.equals(ic.BRANCH)) {
			return "BRANCH";
		} else if (strCode.equals(ic.COMPANY_NAME)) {
			return "COMPANY_NAME";
		} else if (strCode.equals(ic.OWNER_ID)) {
			return "OWNER_ID";
		} else if (strCode.equals(ic.INSURED_ID)) {
			return "INSURED_ID";
		} else if (strCode.equals(ic.INS_TYPE_CODE)) {
			return "INS_TYPE_CODE";
		} else if (strCode.equals(ic.CLEAR_CASE_RESP)) {
			return "CLEAR_CASE_RESP";
		} else if (strCode.equals(ic.CLIENT_INDV_SURNAME)) {
			return "CLIENT_INDV_SURNAME";
		} else if (strCode.equals(ic.MESSAGE_TEXT)) {
			return "MESSAGE_TEXT";
		} else if (strCode.equals(ic.POLICY_BASE)) {
			return "POLICY_BASE";
		} else if (strCode.equals(ic.PREVIOUS_UPDATE_DATE)) {
			return "PREVIOUS_UPDATE_DATE";
		} else if (strCode.equals(ic.MISS_INFO_IND)) {
			return "MISS_INFO_IND";
		} else if (strCode.equals(ic.SEVERITY)) {
			return "SEVERITY";
		} else if (strCode.equals(ic.CLRCASE_SEQUENCE_NUMBER)) {
			return "CLRCASE_SEQUENCE_NUMBER";
		} else if (strCode.equals(ic.LSIR_RETURN_CODE)) {
			return "LSIR_RETURN_CODE";
		} else if (strCode.equals(ic.RESULT_CODE)) {
			return "RESULT_CODE";
		} else if (strCode.equals(ic.COVERAGE_DECISION)) {
			return "COVERAGE_DECISION";
		} else if (strCode.equals(ic.APP_SIGN_DATE)) {
			return "APP_SIGN_DATE";
		} else if (strCode.equals(ic.BRANCH_CODE)) {
			return "BRANCH_CODE";
		} else if (strCode.equals(ic.POLICY_STATUS)) {
			return "POLICY_STATUS";
		} else if (strCode.equals(ic.HI_OWNER_ID)) {
			return "HI_OWNER_ID";
		} else if (strCode.equals(ic.OWNER_NAME)) {
			return "OWNER_NAME";
		} else if (strCode.equals(ic.OWNER_BIRTH_DATE)) {
			return "OWNER_BIRTH_DATE";
		} else if (strCode.equals(ic.HI_INSURED_ID)) {
			return "HI_INSURED_ID";
		} else if (strCode.equals(ic.HI_INSURED_NAME)) {
			return "HI_INSURED_NAME";
		} else if (strCode.equals(ic.INSURED_BIRTH_DATE)) {
			return "INSURED_BIRTH_DATE";
		} else if (strCode.equals(ic.CURRENT_POLICY_STATUS)) {
			return "CURRENT_POLICY_STATUS";
		} else if (strCode.equals(ic.PRODUCT_CODE)) {
			return "PRODUCT_CODE";
		} else if (strCode.equals(ic.BILLING_TYPE_CODE)) {
			return "BILLING_TYPE_CODE";
		} else if (strCode.equals(ic.POLICY_ISSUE_DATE)) {
			return "POLICY_ISSUE_DATE";
		} else if (strCode.equals(ic.POLICY_PAID_TO_DATE)) {
			return "POLICY_PAID_TO_DATE";
		} else if (strCode.equals(ic.AMOUNT_BILLED)) {
			return "AMOUNT_BILLED";
		} else if (strCode.equals(ic.PREMIUM_MODE)) {
			return "PREMIUM_MODE";
		} else if (strCode.equals(ic.SUNDRY_AMOUNT)) {
			return "SUNDRY_AMOUNT";
		} else if (strCode.equals(ic.TRUE_PREMIUM)) {
			return "TRUE_PREMIUM";
		} else if (strCode.equals(ic.LAST_MODE_PREMIUM)) {
			return "LAST_MODE_PREMIUM";
		} else if (strCode.equals(ic.CURRENCY)) {
			return "CURRENCY";
		} else if (strCode.equals(ic.SUSPENSE_DETAILS_OUTSTANDING_DISBURSEMENTS)) {
			return "SUSPENSE_DETAILS_OUTSTANDING_DISBURSEMENTS";
		} else if (strCode.equals(ic.BENEFICIARY_INFORMATION_BENEFICIARY_NAME)) {
			return "BENEFICIARY_INFORMATION_BENEFICIARY_NAME";
		} else if (strCode
				.equals(ic.BENEFICIARY_INFORMATION_BENEFICIARY_BIRTHDATE)) {
			return "BENEFICIARY_INFORMATION_BENEFICIARY_BIRTHDATE";
		} else if (strCode
				.equals(ic.BENEFICIARY_INFORMATION_BENEFICIARY_PROCEEDS)) {
			return "BENEFICIARY_INFORMATION_BENEFICIARY_PROCEEDS";
		} else if (strCode
				.equals(ic.BENEFICIARY_INFORMATION_BENEFICIARY_DESIGNATION)) {
			return "BENEFICIARY_INFORMATION_BENEFICIARY_DESIGNATION";
		} else if (strCode.equals(ic.BENEFICIARY_INFORMATION_RELATION_TO_INSURED)) {
			return "BENEFICIARY_INFORMATION_RELATION_TO_INSURED";
		} else if (strCode.equals(ic.BENEFICIARY_INFORMATION_BENEFICIARY_TYPE)) {
			return "BENEFICIARY_INFORMATION_BENEFICIARY_TYPE";
		} else if (strCode
				.equals(ic.POLICY_FINANCIAL_ACTIVITY_DEFERRED_ACTIVITY_LIST_FINANCIAL_TRANSACTION_EFFECTIVE_DATE)) {
			return "POLICY_FINANCIAL_ACTIVITY_DEFERRED_ACTIVITY_LIST_FINANCIAL_TRANSACTION_EFFECTIVE_DATE";
		} else if (strCode
				.equals(ic.POLICY_FINANCIAL_ACTIVITY_DEFERRED_ACTIVITY_LIST_FINANCIAL_TRANSACTION_TYPE)) {
			return "POLICY_FINANCIAL_ACTIVITY_DEFERRED_ACTIVITY_LIST_FINANCIAL_TRANSACTION_TYPE";
		} else if (strCode
				.equals(ic.POLICY_FINANCIAL_ACTIVITY_DEFERRED_ACTIVITY_LIST_FINANCIAL_TRANSACTION_GROSS_AMOUNT)) {
			return "POLICY_FINANCIAL_ACTIVITY_DEFERRED_ACTIVITY_LIST_FINANCIAL_TRANSACTION_GROSS_AMOUNT";
		} else if (strCode
				.equals(ic.POLICY_FINANCIAL_ACTIVITY_DEFERRED_ACTIVITY_LIST_FINANCIAL_TRANSACTION_STATUS)) {
			return "POLICY_FINANCIAL_ACTIVITY_DEFERRED_ACTIVITY_LIST_FINANCIAL_TRANSACTION_STATUS";
		} else if (strCode
				.equals(ic.FUND_TRANSFER_PERCENT_TO_PERCENT_CONFIRMATION_FUND_ID)) {
			return "FUND_TRANSFER_PERCENT_TO_PERCENT_CONFIRMATION_FUND_ID";
		} else if (strCode
				.equals(ic.FUND_TRANSFER_PERCENT_TO_PERCENT_CONFIRMATION_APPROXIMATE_VALUE_IN_FUND_CURRENCY)) {
			return "FUND_TRANSFER_PERCENT_TO_PERCENT_CONFIRMATION_APPROXIMATE_VALUE_IN_FUND_CURRENCY";
		} else if (strCode
				.equals(ic.FUND_TRANSFER_PERCENT_TO_PERCENT_CONFIRMATION_PERCENTAGE_FROM)) {
			return "FUND_TRANSFER_PERCENT_TO_PERCENT_CONFIRMATION_PERCENTAGE_FROM";
		} else if (strCode
				.equals(ic.FUND_TRANSFER_PERCENT_TO_PERCENT_CONFIRMATION_PERCENTAGE_TO)) {
			return "FUND_TRANSFER_PERCENT_TO_PERCENT_CONFIRMATION_PERCENTAGE_TO";
		} else if (strCode
				.equals(ic.POLICY_ALLOCATION_LIST_ALLOCATION_TYPE_2ND_ROW)) {
			return "POLICY_ALLOCATION_LIST_ALLOCATION_TYPE_2ND_ROW";
		} else if (strCode
				.equals(ic.POLICY_ALLOCATION_LIST_EFFECTIVE_DATE_2ND_ROW)) {
			return "POLICY_ALLOCATION_LIST_EFFECTIVE_DATE_2ND_ROW";
		} else if (strCode
				.equals(ic.POLICY_ALLOCATION_LIST_APPLICATION_STATUS_2ND_ROW)) {
			return "POLICY_ALLOCATION_LIST_APPLICATION_STATUS_2ND_ROW";
		} else if (strCode
				.equals(ic.POLICY_ALLOCATION_INQUIRY_APPLICATION_STATUS_OF_ALLOCATION_TYPE_SUBSEQUENT_PREMIUM)) {
			return "POLICY_ALLOCATION_INQUIRY_APPLICATION_STATUS_OF_ALLOCATION_TYPE_SUBSEQUENT_PREMIUM";
		} else if (strCode
				.equals(ic.POLICY_ALLOCATION_INQUIRY_ALLOCATION_PERCENTAGE_OF_ALLOCATION_TYPE_SUBSEQUENT_PREMIUM)) {
			return "POLICY_ALLOCATION_INQUIRY_ALLOCATION_PERCENTAGE_OF_ALLOCATION_TYPE_SUBSEQUENT_PREMIUM";
		} else if (strCode
				.equals(ic.POLICY_ALLOCATION_INQUIRY_FUND_DESCRIPTION_OF_ALLOCATION_TYPE_SUBSEQUENT_PREMIUM)) {
			return "POLICY_ALLOCATION_INQUIRY_FUND_DESCRIPTION_OF_ALLOCATION_TYPE_SUBSEQUENT_PREMIUM";
		} else if (strCode
				.equals(ic.POLICY_ALLOCATION_INQUIRY_COVERAGE_STATUS_OF_ALLOCATION_TYPE_SUBSEQUENT_PREMIUM)) {
			return "POLICY_ALLOCATION_INQUIRY_COVERAGE_STATUS_OF_ALLOCATION_TYPE_SUBSEQUENT_PREMIUM";
		} else if (strCode.equals(ic.LOAN_INFORMATION_BASE_CASH_VALUE)) {
			return "LOAN_INFORMATION_BASE_CASH_VALUE";
		} else if (strCode
				.equals(ic.LOAN_INFORMATION_MAXIMUM_ADVANCE_AMOUNT_AVAILABLE)) {
			return "LOAN_INFORMATION_MAXIMUM_ADVANCE_AMOUNT_AVAILABLE";
		} else if (strCode.equals(ic.OTHER_OPTIONS_NON_FORFEITURE_OPTION)) {
			return "OTHER_OPTIONS_NON_FORFEITURE_OPTION";
		} else if (strCode.equals(ic.BENEFICIARY_INFORMATION_PERCENTAGE)) {
			return "BENEFICIARY_INFORMATION_PERCENTAGE";
		} else if (strCode.equals(ic.BENEFICIARY_INSTRUCTIONS)) {
			return "BENEFICIARY_INSTRUCTIONS";
		} else if (strCode.equals(ic.LOAN_INFORMATION_CASH_SURRENDER_VALUE)) {
			return "LOAN_INFORMATION_CASH_SURRENDER_VALUE";
		} else if (strCode.equals(ic.LOAN_INFORMATION_LOAN_AMOUNT)) {
			return "LOAN_INFORMATION_LOAN_AMOUNT";
		} else if (strCode.equals(ic.DIVIDEND_INFORMATION_POLICY_DIVIDEND_OPTION)) {
			return "DIVIDEND_INFORMATION_POLICY_DIVIDEND_OPTION";
		} else if (strCode.equals(ic.DIVIDEND_INFORMATION_DIVIDENDS_ON_DEPOSIT)) {
			return "DIVIDEND_INFORMATION_DIVIDENDS_ON_DEPOSIT";
		} else if (strCode
				.equals(ic.PUA_DETAILS_MAXIMUM_PUA_CV_WITHDRAWABLE_AMOUNT)) {
			return "PUA_DETAILS_MAXIMUM_PUA_CV_WITHDRAWABLE_AMOUNT";
		} else if (strCode.equals(ic.AE_FUND_DETAILS_AE_FUND)) {
			return "AE_FUND_DETAILS_AE_FUND";
		} else if (strCode.equals(ic.AE_FUND_DETAILS_AE_OPTION)) {
			return "AE_FUND_DETAILS_AE_OPTION";
		} else if (strCode.equals(ic.OTHER_OPTIONS_PREMIUM_OFFSET_OPTION_STATUS_CODE)) {
			return "OTHER_OPTIONS_PREMIUM_OFFSET_OPTION_STATUS_CODE";
		} else if (strCode.equals(ic.OTHER_OPTIONS_PREMIUM_OFFSET_OPTION)) {
			return "OTHER_OPTIONS_PREMIUM_OFFSET_OPTION";			
		} else if (strCode.equals(ic.DIVIDEND_INFORMATION_YEAR_LAST_DECLARED)) {
			return "DIVIDEND_INFORMATION_YEAR_LAST_DECLARED";
		} else if (strCode.equals(ic.DIVIDEND_INFORMATION_DIVIDEND_DECLARED)){
			return "DIVIDEND_INFORMATION_DIVIDEND_DECLARED";
		} else if (strCode
				.equals(ic.DIVIDEND_INFORMATION_MAXIMUM_DOD_WITHDRAWABLE_AMOUNT)) {
			return "DIVIDEND_INFORMATION_MAXIMUM_DOD_WITHDRAWABLE_AMOUNT";
		} else if (strCode
				.equals(ic.DIVIDEND_INFORMATION_ANNIVERSARY_PROCESSING_YEAR)) {
			return "DIVIDEND_INFORMATION_ANNIVERSARY_PROCESSING_YEAR";
		} else if (strCode.equals(ic.AGENT_INFORMATION_SERVICE_AGENT_CODE)) {
			return "AGENT_INFORMATION_SERVICE_AGENT_CODE";
		} else if (strCode.equals(ic.AGENT_INFORMATION_SERVICE_AGENT_NAME)) {
			return "AGENT_INFORMATION_SERVICE_AGENT_NAME";
		} else if (strCode.equals(ic.AGENT_INFORMATION_SERVICE_AGENT_STATUS_CODE)) {
			return "AGENT_INFORMATION_SERVICE_AGENT_STATUS_CODE";
		} else if (strCode.equals(ic.AGENT_INFORMATION_SERVICE_NBO_CODE)) {
			return "AGENT_INFORMATION_SERVICE_NBO_CODE";
		} else if (strCode.equals(ic.AGENT_INFORMATION_COVERAGE_CODE)) {
			return "AGENT_INFORMATION_COVERAGE_CODE";	
		} else if (strCode.equals(ic.POLICY_INFORMATION_PREMIUM_SUSPENSE)) {
			return "POLICY_INFORMATION_PREMIUM_SUSPENSE";
		} else if (strCode.equals(ic.POLICY_INFORMATION_PREMIUM_REFUND)) {
			return "POLICY_INFORMATION_PREMIUM_REFUND";
		}else if (strCode.equals(ic.SMOKER_CODE)){
			return "SMOKER_CODE";
		}else if (strCode.equals(ic.EFFECTIVE_DATE_OF_ASSIGNMENT)){
			return "EFFECTIVE_DATE_OF_ASSIGNMENT";
		}else if (strCode.equals(ic.COMMENTS_REMARKS)){
			return "COMMENTS_REMARKS";
		}else if (strCode.equals(ic.COVERAGE_DETAILS_AGE)){
			return "COVERAGE_DETAILS_AGE";
		}else if (strCode.equals(ic.COVERAGE_DETAILS_SEX_CODE)){
			return "COVERAGE_DETAILS_SEX_CODE";
		}else if (strCode.equals(ic.COVERAGE_DETAILS_SMOKER_CODE)){
			return "COVERAGE_DETAILS_SMOKER_CODE";
		}else if (strCode.equals(ic.CUMULATIVE_EXCESS_PREMIUM_LIFE_TO_DATE)){
			return "CUMULATIVE_EXCESS_PREMIUM_LIFE_TO_DATE";
		}else if (strCode.equals(ic.CUMULATIVE_SURRENDER_LIFE_TO_DATE)){
			return "CUMULATIVE_SURRENDER_LIFE_TO_DATE";
		}else if (strCode.equals(ic.ANNUAL_PREMIUM_FOR_COVERAGE)){
			return "ANNUAL_PREMIUM_FOR_COVERAGE";
		}else if (strCode.equals(ic.POLICY_INFORMATION_ASSIGNEE)){
			return "POLICY_INFORMATION_ASSIGNEE";
		}else if (strCode.equals(ic.OTHER_LEGAL_FRIST_NAME)){
			return "OTHER_LEGAL_FRIST_NAME";
		}else if (strCode.equals(ic.OTHER_LEGAL_MID_NAME)){
			return "OTHER_LEGAL_MID_NAME";
		}else if (strCode.equals(ic.OTHER_LEGAL_LAST_NAME)){
			return "OTHER_LEGAL_LAST_NAME";
		}else if (strCode.equals(ic.SUSPENSE_DETAILS_MISCELLANEOUS_SUSPENSE)){
			return "SUSPENSE_DETAILS_MISCELLANEOUS_SUSPENSE";	
		}else if (strCode.equals(ic.APP_FUND_AMT)){
			return "APP_FUND_AMT";		
		}else if (strCode.equals(ic.APP_FUND_SUSP_AMT)){
			return "APP_FUND_SUSP_AMT";		
		}else if (strCode.equals(ic.AMOUNT_OF_ADVANCE )){
			return "AMOUNT_OF_ADVANCE";		
		}else if (strCode.equals(ic.APA_AMOUNT )){
			return "APA_AMOUNT";		
		}else if (strCode.equals(ic.ACCRUED_INTEREST_ON_ADVANCE )){
			return "ACCRUED_INTEREST_ON_ADVANCE";		
		}else if (strCode.equals(ic.ACCRUED_INTEREST_ON_APA)){
			return "ACCRUED_INTEREST_ON_APA";
		}else if (strCode.equals(ic.POLICY_VALUES_CASH_SURRENDER )){
			return "POLICY_VALUES_CASH_SURRENDER";	
		}else if (strCode.equals(ic.POLICY_INFORMATION_1 )){ 
			return "POLICY_INFORMATION_1";	
		}else if (strCode.equals(ic.CLIENT_FLAG_INDICIA )){ 
			return "CLIENT_FLAG_INDICIA";	
		}else if (strCode.equals(ic.CLIENT_FLAG_PERSON )){ 
			return "CLIENT_FLAG_PERSON";	
	    }else if (strCode.equals("")) {
			return "";
		}
		
		return null;

	}

	public void startElement(String uri, String name, String qName,
			Attributes atts) {
		
		
		String Array[] = {
				ic.ADDRESS_1,
				ic.ADDRESS_2,
				ic.ADDRESS_3,
				ic.ADDRESS_4,
				ic.ADDRESS_5,
				ic.ADDRESS_6,
				ic.ADDRESS_3_PREVIOUS,
				ic.BENEFICIARY_NAME,
				ic.CITY,
				ic.CLIENT_FIRSTNAME,
				ic.CLIENT_LASTNAME,
				ic.CLIENT_MIDDLENAME,
				ic.CLIENT_NO,
				ic.COUNTRY_CODE,
				ic.COUNTRY_CODE_PREVIOUS,
				ic.COVERAGE_NUMBER,
				ic.COVERAGE_NUMBER_T,
				ic.FACE_AMOUNT,
				ic.INDEX_NUMBER,
				ic.INSURED_CLIENT_NO,
				ic.INSURED_NAME,
				ic.ORIGINAL_FACE_AMOUNT,
				ic.CITY_PREVIOUS,
				ic.PLAN_ID,
				ic.PLAN_ID_T,
				ic.PROVINCE_CODE,
				ic.PROVINCE_PREVIOUS,
				ic.REFUND_AMOUNT,
				ic.REQUIREMENT_CODE,
				ic.REQUIREMENT_ID,
				ic.RETURN_CODE,
				ic.SEQUENCE_NUMBER,
				ic.USER_MESSAGE,
				ic.ZIPCODE,
				ic.ZIPCODE_PREVIOUS,
				ic.OWNER_TITLE,
				ic.STAT_CODE,
				ic.STAT_CODES,
				ic.INDEX_NUMBERS,
				ic.SEQUENCE_NUMBERS,
				ic.SEX,
				ic.CLIENT_BDAY,
				ic.AGENT_NAME,
				ic.SERVICING_AGENT_NAME,
				ic.COVERAGE_COUNT,
				ic.BENEFIT_CODE,
				ic.POLICY_STATUS_DESC,
				ic.COVERAGE_PLAN_DESC,
				ic.CURRENCY_CODE,
				ic.MISC_SUSPENSE_AMOUNT,
				ic.MODE_PREMIUM,
				ic.PREMIUM_SUSPENSE,
				ic.BRANCH,
				ic.AGENT_CODE,
				ic.COMPANY_NAME,
				ic.INSURED_ID,
				ic.OWNER_ID,
				ic.INS_TYPE_CODE,
				ic.CLRCASE_SEQUENCE_NUMBER,
				ic.SEVERITY,
				ic.MISS_INFO_IND,
				ic.PREVIOUS_UPDATE_DATE,
				ic.POLICY_BASE,
				ic.MESSAGE_TEXT,
				ic.CLIENT_INDV_SURNAME,
				ic.CLEAR_CASE_RESP,
				ic.LSIR_RETURN_CODE,
				ic.RESULT_CODE,
				ic.COVERAGE_DECISION,
				ic.APP_SIGN_DATE,
				ic.BRANCH_CODE,
				ic.POLICY_STATUS,
				ic.OWNER_ID,
				ic.OWNER_NAME,
				ic.OWNER_BIRTH_DATE,
				ic.INSURED_ID,
				ic.INSURED_NAME,
				ic.INSURED_BIRTH_DATE,
				ic.CURRENT_POLICY_STATUS,
				ic.PRODUCT_CODE,
				ic.BILLING_TYPE_CODE,
				ic.POLICY_ISSUE_DATE,
				ic.POLICY_PAID_TO_DATE,
				ic.AMOUNT_BILLED,
				ic.PREMIUM_MODE,
				ic.SUNDRY_AMOUNT,
				ic.TRUE_PREMIUM,
				ic.LAST_MODE_PREMIUM,
				ic.CURRENCY,
				ic.SUSPENSE_DETAILS_OUTSTANDING_DISBURSEMENTS,
				ic.BENEFICIARY_INFORMATION_BENEFICIARY_NAME,
				ic.BENEFICIARY_INFORMATION_BENEFICIARY_BIRTHDATE,
				ic.BENEFICIARY_INFORMATION_BENEFICIARY_PROCEEDS,
				ic.BENEFICIARY_INFORMATION_BENEFICIARY_DESIGNATION,
				ic.BENEFICIARY_INFORMATION_RELATION_TO_INSURED,
				ic.BENEFICIARY_INFORMATION_BENEFICIARY_TYPE,
				ic.POLICY_FINANCIAL_ACTIVITY_DEFERRED_ACTIVITY_LIST_FINANCIAL_TRANSACTION_EFFECTIVE_DATE,
				ic.POLICY_FINANCIAL_ACTIVITY_DEFERRED_ACTIVITY_LIST_FINANCIAL_TRANSACTION_TYPE,
				ic.POLICY_FINANCIAL_ACTIVITY_DEFERRED_ACTIVITY_LIST_FINANCIAL_TRANSACTION_GROSS_AMOUNT,
				ic.POLICY_FINANCIAL_ACTIVITY_DEFERRED_ACTIVITY_LIST_FINANCIAL_TRANSACTION_STATUS,
				ic.FUND_TRANSFER_PERCENT_TO_PERCENT_CONFIRMATION_FUND_ID,
				ic.FUND_TRANSFER_PERCENT_TO_PERCENT_CONFIRMATION_APPROXIMATE_VALUE_IN_FUND_CURRENCY,
				ic.FUND_TRANSFER_PERCENT_TO_PERCENT_CONFIRMATION_PERCENTAGE_FROM,
				ic.FUND_TRANSFER_PERCENT_TO_PERCENT_CONFIRMATION_PERCENTAGE_TO,
				ic.POLICY_ALLOCATION_LIST_ALLOCATION_TYPE_2ND_ROW,
				ic.POLICY_ALLOCATION_LIST_EFFECTIVE_DATE_2ND_ROW,
				ic.POLICY_ALLOCATION_LIST_APPLICATION_STATUS_2ND_ROW,
				ic.POLICY_ALLOCATION_INQUIRY_APPLICATION_STATUS_OF_ALLOCATION_TYPE_SUBSEQUENT_PREMIUM,
				ic.POLICY_ALLOCATION_INQUIRY_ALLOCATION_PERCENTAGE_OF_ALLOCATION_TYPE_SUBSEQUENT_PREMIUM,
				ic.POLICY_ALLOCATION_INQUIRY_FUND_DESCRIPTION_OF_ALLOCATION_TYPE_SUBSEQUENT_PREMIUM,
				ic.POLICY_ALLOCATION_INQUIRY_COVERAGE_STATUS_OF_ALLOCATION_TYPE_SUBSEQUENT_PREMIUM,
				ic.LOAN_INFORMATION_LOAN_AMOUNT,
				ic.LOAN_INFORMATION_BASE_CASH_VALUE,
				ic.LOAN_INFORMATION_MAXIMUM_ADVANCE_AMOUNT_AVAILABLE,
				ic.OTHER_OPTIONS_NON_FORFEITURE_OPTION,
				ic.OTHER_OPTIONS_PREMIUM_OFFSET_OPTION_STATUS_CODE,
				ic.OTHER_OPTIONS_PREMIUM_OFFSET_OPTION,
				ic.BENEFICIARY_INSTRUCTIONS,
				ic.LOAN_INFORMATION_CASH_SURRENDER_VALUE,
				ic.DIVIDEND_INFORMATION_POLICY_DIVIDEND_OPTION,
				ic.DIVIDEND_INFORMATION_DIVIDENDS_ON_DEPOSIT,
				ic.PUA_DETAILS_MAXIMUM_PUA_CV_WITHDRAWABLE_AMOUNT,
				ic.AE_FUND_DETAILS_AE_FUND, ic.AE_FUND_DETAILS_AE_OPTION,
				ic.DIVIDEND_INFORMATION_YEAR_LAST_DECLARED,
				ic.DIVIDEND_INFORMATION_DIVIDEND_DECLARED,
				ic.DIVIDEND_INFORMATION_MAXIMUM_DOD_WITHDRAWABLE_AMOUNT,
				ic.DIVIDEND_INFORMATION_ANNIVERSARY_PROCESSING_YEAR,
				ic.AGENT_INFORMATION_SERVICE_AGENT_CODE,
				ic.AGENT_INFORMATION_SERVICE_AGENT_NAME,
				ic.AGENT_INFORMATION_SERVICE_AGENT_STATUS_CODE,
				ic.AGENT_INFORMATION_SERVICE_NBO_CODE,
				ic.AGENT_INFORMATION_COVERAGE_CODE,
				ic.HI_INSURED_ID,
				ic.HI_OWNER_ID,
				ic.HI_INSURED_NAME,
				ic.BENEFICIARY_INFORMATION_PERCENTAGE,
				ic.POLICY_INFORMATION_PREMIUM_REFUND,
				ic.POLICY_INFORMATION_PREMIUM_SUSPENSE,
				ic.SMOKER_CODE,
				ic.EFFECTIVE_DATE_OF_ASSIGNMENT,
				ic.COMMENTS_REMARKS,
				ic.COVERAGE_DETAILS_AGE,
				ic.COVERAGE_DETAILS_SEX_CODE,
				ic.COVERAGE_DETAILS_SMOKER_CODE,
				ic.CUMULATIVE_EXCESS_PREMIUM_LIFE_TO_DATE,
				ic.CUMULATIVE_SURRENDER_LIFE_TO_DATE,
				ic.ANNUAL_PREMIUM_FOR_COVERAGE,
				ic.POLICY_INFORMATION_ASSIGNEE,
				ic.OTHER_LEGAL_FRIST_NAME,
				ic.OTHER_LEGAL_LAST_NAME,
				ic.OTHER_LEGAL_MID_NAME,
				ic.SUSPENSE_DETAILS_MISCELLANEOUS_SUSPENSE,
				ic.APP_FUND_AMT,
				ic.APP_FUND_SUSP_AMT,
				ic.AMOUNT_OF_ADVANCE,
				ic.APA_AMOUNT,
				ic.ACCRUED_INTEREST_ON_ADVANCE,
				ic.ACCRUED_INTEREST_ON_APA,
				ic.POLICY_VALUES_CASH_SURRENDER,
				ic.POLICY_INFORMATION_1, 
				ic.CLIENT_FLAG_INDICIA,
				ic.CLIENT_FLAG_PERSON	
		       };
		
		sb = new StringBuffer();
		if (!strHead.equalsIgnoreCase(name) && bKeyFound) {
			if (!strHead.equalsIgnoreCase("ResultCode"))
				hm.put(translate(strHead), alValuesFound);
			bKeyFound = false;
			alValuesFound = null;
			alValuesFound = new ArrayList();

		}
		for (int i = 0; i < Array.length; i++) {
			if (name.equalsIgnoreCase(Array[i])) {
				strHead = name;
				bKeyFound = true;
			}
		}
		
	}

	public void endElement(String uri, String name, String qName) {
		
		
		if ("".equals(uri))
			; 
		else {
			if (bKeyFound) {
				alValuesFound.add(sb.toString());
			}
			if (!strHead.equalsIgnoreCase(name) && bKeyFound) {
				
				if (!strHead.equalsIgnoreCase("ResultCode"))
					hm.put(translate(strHead), alValuesFound);
				
				bKeyFound = false;
				alValuesFound = null;
				alValuesFound = new ArrayList();
			}
		}
		
	}

	public void characters(char ch[], int start, int length) {
		
		
		if (strHead.equalsIgnoreCase("ResultCode") && bKeyFound) {
			alValuesFound = new ArrayList();
			alValuesFound.add("" + ch[start - 3]);
			hm.put(translate(strHead), alValuesFound);
			bKeyFound = false;
			alValuesFound = null;
			alValuesFound = new ArrayList();
		}
		
		
		char ch1[] = new char[length];
		int j = 0;

		for (int i = start; i < start + length; i++) {
	
			switch (ch[i]) {

			case '\\':
				System.out.print("\\\\");
				break;
			case '"':
				System.out.print("\\\"");
				break;
			case '\n':
				System.out.print("\\n");
				break;
			case '\r':
				System.out.print("\\r");
				break;
			case '\t':
				System.out.print("\\t");
				break;
			default:
				ch1[j] = ch[i];
				j++;
				break;
			}
		}
		if (bKeyFound) {
			String strTemp = new String(ch1);
			sb.append(strTemp.toString());
		}
		
	}
}
