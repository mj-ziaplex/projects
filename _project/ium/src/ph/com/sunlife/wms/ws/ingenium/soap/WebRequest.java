package ph.com.sunlife.wms.ws.ingenium.soap;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.slocpi.ium.util.CodeHelper;

public class WebRequest implements Cloneable{
	private static final Logger LOGGER = LoggerFactory.getLogger(WebRequest.class);
	private String webRequest;
	
	public Object copy(){
		
		try{
			return super.clone();
		}catch(Exception x){
			LOGGER.error(CodeHelper.getStackTrace(x));
			return null;
		}
		
	  }
	
	public WebRequest(){
		
		LOGGER.info("WebRequest start");
        StringBuffer xmlData = new StringBuffer();
        try{
            BufferedReader reader = new BufferedReader(new FileReader("D:/java/TSS/Input1/BeneficiaryList.xml"));
            String line = reader.readLine();
            while (line != null)
            {
                xmlData.append(line.trim());
                line = reader.readLine();
               
            }
        }
        catch (IOException ioe){
        	LOGGER.error(CodeHelper.getStackTrace(ioe));
            
        }
        webRequest = xmlData.toString();
        LOGGER.info("WebRequest end");
	}


	public String getRequestString(){
		return webRequest;        
	}
	
}
