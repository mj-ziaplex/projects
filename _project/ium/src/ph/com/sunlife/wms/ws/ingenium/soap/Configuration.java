package ph.com.sunlife.wms.ws.ingenium.soap;

public class Configuration {
	

	private static Configuration conf = null;
	private String host;
	private String port = "80";
	
	private Configuration(){
		
	} 
	
	public static Configuration getInstanceOf(){
		if(conf == null) conf = new Configuration();
		return conf;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public String getPort() {
		return port;
	}

	public void setPort(String port) {
		this.port = port;
	}
	
	
	
}

	
