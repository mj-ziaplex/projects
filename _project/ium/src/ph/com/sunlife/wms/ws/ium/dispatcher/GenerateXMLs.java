package ph.com.sunlife.wms.ws.ium.dispatcher;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GenerateXMLs {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(GenerateXMLs.class);
	
	public String retrieveKOandreqts(String username, String password, String PolicyId)
	{
		LOGGER.info("retrieveKOandreqts start");
		
		StringBuffer sb = new StringBuffer();
		sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		sb.append("<IUM xmlns=\"http://ACORD.org/Standards/Life/2\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\">");
		sb.append("<IUMRequest>");
		sb.append("<AuthenticationInfo>");
		sb.append("<LoginName>");
		sb.append(username);
		sb.append("</LoginName>");
		sb.append("<Password>");
		sb.append(password);
		sb.append("</Password>");
		sb.append("</AuthenticationInfo>");
		sb.append("<RequestType>CreateUpdatePolicyRecord</RequestType>");
		sb.append("<PolicyInfo>");
		sb.append("<PolicyID>");
		sb.append(PolicyId);
		sb.append("</PolicyID>");
		sb.append("</PolicyInfo>");
		sb.append("</IUMRequest>");
		sb.append("</IUM>");
		
		
		LOGGER.info("retrieveKOandreqts end");
		return sb.toString();
	}
	
	public String MatchDocument(String username, String password, String PolicyId, String ClientID, String ReqtID, String ReqLvl, String ReqType, String imgRef, String reqStatus, String ScanDate)
	{
		
		LOGGER.info("MatchDocument start 1");
		
		StringBuffer sb = new StringBuffer();
		sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		sb.append("<IUM xmlns=\"http://ACORD.org/Standards/Life/2\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\">");
		sb.append("<IUMRequest>");
		sb.append("<AuthenticationInfo>");
		sb.append("<LoginName>");
		sb.append(username);
		sb.append("</LoginName>");
		sb.append("<Password>");
		sb.append(password);
		sb.append("</Password>");
		sb.append("</AuthenticationInfo>");
		sb.append("<RequestType>CreateUpdateRequirement</RequestType>");
		sb.append("<ReqtInfo>");
		sb.append("<PolicyID>");
		sb.append(PolicyId);
		sb.append("</PolicyID>");
		sb.append("<ClientID>");
		sb.append(ClientID);
		sb.append("</ClientID>");
		sb.append("<IUMReqtID>");
		sb.append( ReqtID );
		sb.append( "</IUMReqtID>" );
		sb.append("<ReqtLvlInd>");
		sb.append(ReqLvl);
		sb.append("</ReqtLvlInd>");
		sb.append("<ReqtTypeId>");
		sb.append(ReqType);
		sb.append("</ReqtTypeId>");
		sb.append("<ImageReferenceNo>");
		sb.append(imgRef);
		sb.append("</ImageReferenceNo>");
		sb.append("<ReqtStatus>");
		sb.append(reqStatus);
		sb.append("</ReqtStatus>");
		sb.append("<ScanDate>");
		sb.append(ScanDate);
		sb.append("</ScanDate>");
		sb.append("<TestDate>");
		sb.append("</TestDate>");
		sb.append("</ReqtInfo>");
		sb.append("</IUMRequest>");
		sb.append("</IUM>");
		
		LOGGER.info("MatchDocument end 1");
		
		return sb.toString();
	}
	public String MatchDocument(String username, String password, String PolicyId, String ClientID, String ReqtID, String ReqLvl, String ReqType, String imgRef, String reqStatus, String ScanDate, String TestDate)
	{
		LOGGER.info("MatchDocument start 2");
		
		StringBuffer sb = new StringBuffer();
		sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		sb.append("<IUM xmlns=\"http://ACORD.org/Standards/Life/2\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\">");
		sb.append("<IUMRequest>");
		sb.append("<AuthenticationInfo>");
		sb.append("<LoginName>");
		sb.append(username);
		sb.append("</LoginName>");
		sb.append("<Password>");
		sb.append(password);
		sb.append("</Password>");
		sb.append("</AuthenticationInfo>");
		sb.append("<RequestType>CreateUpdateRequirement</RequestType>");
		sb.append("<ReqtInfo>");
		sb.append("<PolicyID>");
		sb.append(PolicyId);
		sb.append("</PolicyID>");
		sb.append("<ClientID>");
		sb.append(ClientID);
		sb.append("</ClientID>");
		sb.append("<IUMReqtID>");
		sb.append( ReqtID );
		sb.append( "</IUMReqtID>" );
		sb.append("<ReqtLvlInd>");
		sb.append(ReqLvl);
		sb.append("</ReqtLvlInd>");
		sb.append("<ReqtTypeId>");
		sb.append(ReqType);
		sb.append("</ReqtTypeId>");
		sb.append("<ImageReferenceNo>");
		sb.append(imgRef);
		sb.append("</ImageReferenceNo>");
		sb.append("<ReqtStatus>");
		sb.append(reqStatus);
		sb.append("</ReqtStatus>");
		sb.append("<ScanDate>");
		sb.append(ScanDate);
		sb.append("</ScanDate>");
		sb.append("<TestDate>");
		sb.append(TestDate);
		sb.append("</TestDate>");
		sb.append("</ReqtInfo>");
		sb.append("</IUMRequest>");
		sb.append("</IUM>");
		
		LOGGER.info("MatchDocument end 2");
		return sb.toString();
	}

	public String UnmatchDocument(String username, String password, String PolicyID, String ImageRefNo,String reqtId){
		
		LOGGER.info("UnmatchDocument start 1");
		
		StringBuffer sb = new StringBuffer();
		sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		sb.append("<IUM xmlns=\"http://ACORD.org/Standards/Life/2\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\">");
		sb.append("<IUMRequest>");						
		sb.append("<AuthenticationInfo>");					
		sb.append("<LoginName>");
		sb.append(username);
		sb.append("</LoginName>");				
		sb.append("<Password>");
		sb.append(password);
		sb.append("</Password>");				
		sb.append("</AuthenticationInfo>");					
		sb.append("<RequestType>UnmatchRequirement</RequestType>");					
		sb.append("<ReqtInfo>");					
		sb.append("<PolicyID>");
		sb.append(PolicyID);
		sb.append("</PolicyID>");
		sb.append("<ImageReferenceNo>");
		sb.append(ImageRefNo);
		sb.append("</ImageReferenceNo>");
		sb.append("<ReqtTypeID>");
		sb.append(reqtId);
		sb.append("</ReqtTypeID>");
		sb.append("</ReqtInfo>");				
		sb.append("</IUMRequest>");						
		sb.append("</IUM>");
		
		LOGGER.info("UnmatchDocument end 1");
		return sb.toString();
	}
	public String UnmatchDocument(String username, String password, String reqtId){
		
		LOGGER.info("UnmatchDocument start 2");
		
		StringBuffer sb = new StringBuffer();
		sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		sb.append("<IUM xmlns=\"http://ACORD.org/Standards/Life/2\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\">");
		sb.append("<IUMRequest>");						
		sb.append("<AuthenticationInfo>");					
		sb.append("<LoginName>");
		sb.append(username);
		sb.append("</LoginName>");				
		sb.append("<Password>");
		sb.append(password);
		sb.append("</Password>");				
		sb.append("</AuthenticationInfo>");					
		sb.append("<RequestType>UnmatchRequirement</RequestType>");					
		sb.append("<ReqtInfo>");					
		sb.append("<IUMReqtID>");
		sb.append(reqtId);
		sb.append("</IUMReqtID>");
		sb.append("</ReqtInfo>");				
		sb.append("</IUMRequest>");						
		sb.append("</IUM>");
		
		LOGGER.info("UnmatchDocument end 2");
		return sb.toString();
	}
	
	public String CreateCDS(String username, String password, String reqtId){
		
		LOGGER.info("CreateCDS start");
		
		StringBuffer sb = new StringBuffer();
		sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		sb.append("<IUM xmlns=\"http://ACORD.org/Standards/Life/2\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\">");
		sb.append("<IUMRequest>");						
		sb.append("<AuthenticationInfo>");					
		sb.append("<LoginName>");
		sb.append(username);
		sb.append("</LoginName>");				
		sb.append("<Password>");
		sb.append(password);
		sb.append("</Password>");				
		sb.append("</AuthenticationInfo>");					
		sb.append("<RequestType>CreateCDS</RequestType>");					
		sb.append("<PolicyInfo>");					
		sb.append("<PolicyID>");
		sb.append(reqtId);
		sb.append("</PolicyID>");
		sb.append("</PolicyInfo>");				
		sb.append("</IUMRequest>");						
		sb.append("</IUM>");
		
		LOGGER.info("CreateCDS end");
		return sb.toString();
	}
}
