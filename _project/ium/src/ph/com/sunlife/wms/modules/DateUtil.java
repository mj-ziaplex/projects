
package ph.com.sunlife.wms.modules;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.slocpi.ium.util.CodeHelper;

/**
 * A utility class to help manipulate dates etc.
 * @author PD19 ccpj
 */
public class DateUtil extends GregorianCalendar {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(DateUtil.class);
	
	private static final String[] MONTHS = {"", "January","February", "March", "April", "May",
		"June", "July", "August", "September", "October", "November", "December" };
	public static final long MillisecondsInADay = 1000*3600*24; //A day in milliseconds
	
	public DateUtil(){
		super();
	}
	
	public DateUtil( Date d ){
		super( d.getYear() + 1900, d.getMonth(), d.getDate(), d.getHours(), d.getMinutes(), d.getSeconds() );
	}
	
	/**
	 * 
	 * @param iMonth integer that represents a month 1 for January, and 12 for December
	 * @return corresponding string month name
	 */
	public static String convertIntMonthToString( int iMonth ){
		LOGGER.info("convertIntMonthToString initialize");
		return ( iMonth < 13 )? MONTHS[iMonth]: "No Such Month";
	}

	public static long subtractDate( Date dStart, Date dEnd ) { 
		
		LOGGER.info("subtractDate start");
		try{
			long daterange = dStart.getTime() - dEnd.getTime(); 
			LOGGER.info("subtractDate end");
			return daterange / MillisecondsInADay;
		}catch( Exception e){
			LOGGER.error(CodeHelper.getStackTrace(e));
			LOGGER.info("subtractDate end");
			return 0;
		}
	} 

	public static String ConvertDate(String strOldFormat, String strNewFormat,
			String strDateToConvert) {
		
		LOGGER.info("ConvertDate start 1");
		try {
			SimpleDateFormat formatter = new SimpleDateFormat(strOldFormat);
			java.util.Date myDate = formatter.parse(strDateToConvert);
			SimpleDateFormat formatter2 = new SimpleDateFormat(strNewFormat);
			strDateToConvert = formatter2.format(myDate);
		} catch (Exception e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
		}
		LOGGER.info("ConvertDate end 1");
		return strDateToConvert;
	}
	
	public static String ConvertDate( Date dSource, String sFormat ){
		
		LOGGER.info("ConvertDate start 2");
		try{
			SimpleDateFormat sdf = new SimpleDateFormat( sFormat );
			LOGGER.info("ConvertDate end");
			return sdf.format( dSource );
		}catch( Exception e ){ 
			LOGGER.error(CodeHelper.getStackTrace(e));
			return null;
		}
		
	}
	
	public static Date convertStringToDate( String strSource, String strFormat ){
		
		LOGGER.info("convertStringToDate start");
		java.util.Date myDate;
		try {
			SimpleDateFormat formatter = new SimpleDateFormat( strFormat );
			 myDate = formatter.parse( strSource );
			
		} catch (Exception e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			return new Date( 0, 0, 1 );
		}
		LOGGER.info("convertStringToDate end");
		return myDate;
	}
	
	/**
	 * Adds a specified amount to a string representation of a date. Returns a string
	 * @param strDate string representation of a date
	 * @param iAmount amount to be added
	 * @param iField field to be added to, i.e. month, day, etc. use the Calendar constants
	 * @param strFormat string format of the date
	 * @return
	 */
	public static String addToStringDate( String strDate, int iAmount, int iField, String strFormatFrom, String strFormatTo ){
		
		LOGGER.info("addToStringDate start");
		if( strDate.length() > 0 ){
			try{
				Date d = convertStringToDate( strDate, strFormatFrom );
				DateUtil du = new DateUtil( d );
				du.add( iField, iAmount );
				String strRetDate = du.ConvertDate( new Date( du.getTimeInMillis() ), strFormatTo );
				return strRetDate;
			}catch( Exception e ){
				LOGGER.error(CodeHelper.getStackTrace(e));
				return "";
			}
		}
		LOGGER.info("addToStringDate end");
		return "";
	}
	
	
	/**
	 * SPI_Module - Andre Ceasar Dacanay
	 * Formats date with "01/01/1900" to null
	 * @param dt Date representation of a date
	 * @return dt
	 */
	public static Date dateToNull(Date dt){
		
		LOGGER.info("dateToNull start");
		if(ConvertDate(dt,"dd/MM/yyyy").equals("01/01/1900")){
			dt = null;
		}
		LOGGER.info("dateToNull end");
		return dt;		
	}
	
	public static Date setDateTimeToZero(Date date) {
		date.setHours(0);
		date.setMinutes(0);
		date.setSeconds(0);
		
		return date;
	}
}
