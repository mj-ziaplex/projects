package ph.com.sunlife.utils;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.slocpi.ium.util.CodeHelper;

public class Utils {
	private static final Logger LOGGER = LoggerFactory.getLogger(Utils.class);
	
	// Util Null checker for Strings
	public static String noNull(final String str) {
		
		
		
		String toRet = "";
		if(str == null) {
			return toRet;
		} else{
			toRet = str.trim();
		}
		
		
		return toRet;
	}
	
	// Util for closing Connection resource
	public static void closeResource(final Connection conn) {
		
		LOGGER.info("closeResource start 1");
		try{
			if(conn != null) {
				conn.close();
			}
		} catch (Exception ex){
			LOGGER.error(CodeHelper.getStackTrace(ex));
		}
		LOGGER.info("closeResource end 1");
	}
	
	// Util for closing CallableStatement resource
	public static void closeResource(final ResultSet rs) {
		
		LOGGER.info("closeResource start 2");
		try{
			if(rs != null) {
				rs.close();
			}
		} catch (Exception ex){
			LOGGER.error(CodeHelper.getStackTrace(ex));
		}
		LOGGER.info("closeResource end 2");
	}
	
	// Util for closing CallableStatement resource
	public static void closeResource(final Statement stmt) {
		
		LOGGER.info("closeResource start 3");
		try{
			if(stmt != null) {
				stmt.close();
			}
		} catch (Exception ex){
			LOGGER.error(CodeHelper.getStackTrace(ex));
		}
		LOGGER.info("closeResource end 3");
	}
	
	// Util for closing CallableStatement resource
	public static void closeResource(final CallableStatement cs) {
		
		LOGGER.info("closeResource start 4");
		try{
			if(cs != null) {
				cs.close();
			}
		} catch (Exception ex){
			LOGGER.error(CodeHelper.getStackTrace(ex));
		}
		LOGGER.info("closeResource end 4");
	}
}
