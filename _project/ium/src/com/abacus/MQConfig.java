package com.abacus;

import java.util.ResourceBundle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
//import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.sunlife.mq.SLMQInitException;


public class MQConfig {

	private static final Logger LOGGER = LoggerFactory.getLogger(MQConfig.class);
//	Logger LOGGER = LoggerFactory.getLogger(MQConfig.class.getName());
	private static final String MQ_CONFIG_SERVICENAME = "ServiceName";
	private static final String MQ_CONFIG_QUEUEMGR = "QMgrName";
	private static final String MQ_CONFIG_HOSTNAME = "HostName";
	private static final String MQ_CONFIG_PORTNUMBER = "Port";
	private static final String MQ_CONFIG_CHANNEL = "Channel";
	private static final String MQ_CONFIG_DEBUGMODE = "Debug";
	private static final String MQ_CONFIG_MAXWAIT = "MaxWait";
	private static final String MQ_CONFIG_SERVICETYPE = "ServiceType";
	private static final String MQ_CONFIG_PUTQUEUE = "PutQueue";
	private static final String MQ_CONFIG_GETQUEUE = "GetQueue";
	private static final String MQ_CONFIG_TRANSACTION = "Transaction";
	private static final String MQ_CONFIG_USERID = "UserID";
	private static final String MQ_CONFIG_PASSWORD = "Password";

	private String serviceName;
	private String queueManager;
	private String hostName;
	private int portNumber;
	private String channel;
	private String debugMode;
	private int maxWait;
	private String serviceType;
	private String putQueue;
	private String getQueue;
	private String transaction;
	private String userID;
	private String password;

	private boolean loadedConfig = false;

	/**
	 * @return Returns the channel.
	 */
	public String getChannel() {
		return channel;
	}
	/**
	 * @param channel The channel to set.
	 */
	public void setChannel(String channel) {
		this.channel = channel;
	}
	/**
	 * @return Returns the debugMode.
	 */
	public String getDebugMode() {
		return debugMode;
	}
	/**
	 * @param debugMode The debugMode to set.
	 */
	public void setDebugMode(String debugMode) {
		this.debugMode = debugMode;
	}
	/**
	 * @return Returns the getQueue.
	 */
	public String getGetQueue() {
		return getQueue;
	}
	/**
	 * @param getQueue The getQueue to set.
	 */
	public void setGetQueue(String getQueue) {
		this.getQueue = getQueue;
	}
	/**
	 * @return Returns the hostName.
	 */
	public String getHostName() {
		return hostName;
	}
	/**
	 * @param hostName The hostName to set.
	 */
	public void setHostName(String hostName) {
		this.hostName = hostName;
	}
	/**
	 * @return Returns the maxWait.
	 */
	public int getMaxWait() {
		return maxWait;
	}
	/**
	 * @param maxWait The maxWait to set.
	 */
	public void setMaxWait(int maxWait) {
		this.maxWait = maxWait;
	}
	/**
	 * @return Returns the password.
	 */
	public String getPassword() {
		return password;
	}
	/**
	 * @param password The password to set.
	 */
	public void setPassword(String password) {
		this.password = password;
	}
	/**
	 * @return Returns the portNumber.
	 */
	public int getPortNumber() {
		return portNumber;
	}
	/**
	 * @param portNumber The portNumber to set.
	 */
	public void setPortNumber(int portNumber) {
		this.portNumber = portNumber;
	}
	/**
	 * @return Returns the putQueue.
	 */
	public String getPutQueue() {
		return putQueue;
	}
	/**
	 * @param putQueue The putQueue to set.
	 */
	public void setPutQueue(String putQueue) {
		this.putQueue = putQueue;
	}
	/**
	 * @return Returns the queueManager.
	 */
	public String getQueueManager() {
		return queueManager;
	}
	/**
	 * @param queueManager The queueManager to set.
	 */
	public void setQueueManager(String queueManager) {
		this.queueManager = queueManager;
	}
	/**
	 * @return Returns the serviceName.
	 */
	public String getServiceName() {
		return serviceName;
	}
	/**
	 * @param serviceName The serviceName to set.
	 */
	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}
	/**
	 * @return Returns the serviceType.
	 */
	public String getServiceType() {
		return serviceType;
	}
	/**
	 * @param serviceType The serviceType to set.
	 */
	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}
	/**
	 * @return Returns the transaction.
	 */
	public String getTransaction() {
		return transaction;
	}
	/**
	 * @param transaction The transaction to set.
	 */
	public void setTransaction(String transaction) {
		this.transaction = transaction;
	}
	/**
	 * @return Returns the userID.
	 */
	public String getUserID() {
		return userID;
	}
	/**
	 * @param userID The userID to set.
	 */
	public void setUserID(String userID) {
		this.userID = userID;
	}

	/**
	 * @return Returns the loadedConfig.
	 */
	public boolean isLoadedConfig() {
		return loadedConfig;
	}

	public void loadConfig(ResourceBundle rb, String key) throws SLMQInitException {
		LOGGER.info("loadConfig start");
		if (null == rb) {
			throw new SLMQInitException("Unable to locate MQ Configuration File.");
		} else {
			this.serviceName = rb.getString(key + "." + MQConfig.MQ_CONFIG_SERVICENAME);
			this.queueManager = rb.getString(key + "." + MQConfig.MQ_CONFIG_QUEUEMGR);
			this.hostName = rb.getString(key + "." + MQConfig.MQ_CONFIG_HOSTNAME);
			this.portNumber = Integer.parseInt(rb.getString(key + "." + MQConfig.MQ_CONFIG_PORTNUMBER));
			this.channel = rb.getString(key + "." + MQConfig.MQ_CONFIG_CHANNEL);
			this.debugMode = rb.getString(key + "." + MQConfig.MQ_CONFIG_DEBUGMODE);
			this.maxWait = Integer.parseInt(rb.getString(key + "." + MQConfig.MQ_CONFIG_MAXWAIT));
			this.serviceType = rb.getString(key + "." + MQConfig.MQ_CONFIG_SERVICETYPE);
			this.putQueue = rb.getString(key + "." + MQConfig.MQ_CONFIG_PUTQUEUE);
			this.getQueue = rb.getString(key + "." + MQConfig.MQ_CONFIG_GETQUEUE);
			this.transaction = rb.getString(key + "." + MQConfig.MQ_CONFIG_TRANSACTION);
			this.userID = rb.getString(key + "." + MQConfig.MQ_CONFIG_USERID);
			this.password = rb.getString(key + "." + MQConfig.MQ_CONFIG_PASSWORD);
			this.loadedConfig = true;
		}
		LOGGER.info("loadConfig end");
	}

}
