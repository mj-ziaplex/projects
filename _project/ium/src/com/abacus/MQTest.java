

package com.abacus;
public class MQTest {

	public static void main(String[] args) {
		String idUsed = null;
		String str = null;
		String response = null;
		int waitInt = 10000;
		int priority = 1;		

		try {
			if(args.length>0){
				idUsed = args[0];
				str = args[1];//include if args.length
				System.out.println("message: "+str);
				waitInt = Integer.parseInt(args[2]);
				priority = Integer.parseInt(args[3]);
			}else{
				System.out.println("arguments : message waitInterval priority");
				System.out.println("arguments : message waitInterval");
				System.out.println("arguments : message");
				System.exit(0);
			}
			
			com.slocpi.ium.interfaces.messagequeue.MessageConnector obj = new com.slocpi.ium.interfaces.messagequeue.MessageConnector("PtcMQ.properties");
				
			System.out.println("\n***postMessage(String)===============");
			obj.postMessage(str, priority);
		
			System.out.println("\n***getMessage()===============");
			response = obj.getMessage();
			System.out.println("obj.getMessage(): "+response);
		
			System.out.println("\n***putRequestMsg(str, waitInt, priority)===============");
			//obj.postMessage("Top message", priority);
			if(idUsed.equalsIgnoreCase("MI")){
				response = obj.putRequestMsgMI(str, waitInt, priority);
			}else{
				response = obj.putRequestMsg(str, waitInt, priority);
			}
			System.out.println("response: "+response);			
		}catch(Exception e) {
			System.err.println("Error on entry: " + e.toString());
			return;
		}
		



	}
}
