

package com.abacus;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.Calendar;

public class SLMQTest {

	public static void main(String[] args) {

		String str = null;
		String response = null;
		String service = "";
		String messageFile = "";
		int waitInt = 10000;
		int priority = 1;		

		try {
			if(args.length == 3){

				service = args[0];
				messageFile = args[1];
				waitInt = Integer.parseInt(args[2]);
				FileReader fr = new FileReader(messageFile);
				BufferedReader br = new BufferedReader(fr);
				String line = null;
				while((line=br.readLine())!=null){
					if(str!=null){
						str += line;
					}else{
						str = line;
					}					
				}
				
		
			}else{
				System.out.println("arguments : service name");
				System.out.println("arguments : message file");
				System.out.println("arguments : wait interval");
				System.exit(0);
			}
			
			MessageConnector obj = new MessageConnector(service);
			obj.connect();	

			response = obj.putRequestMsg(str);

			if (response != null) {
				String key = "";
				key = Integer.toString(Calendar.getInstance().get(Calendar.MONTH)) + 
					Integer.toString(Calendar.getInstance().get(Calendar.DAY_OF_MONTH)) + 
					Integer.toString(Calendar.getInstance().get(Calendar.HOUR_OF_DAY)) + 
					Integer.toString(Calendar.getInstance().get(Calendar.MINUTE)) + 
					Integer.toString(Calendar.getInstance().get(Calendar.SECOND));				
				File f = new File("MQ" + key + ".txt");
				FileWriter fw = new FileWriter(f);
				fw.write("************** Message Sent \n");
				fw.write(str + "\n\n");
				fw.write("************** Response Received\n");
				fw.write(response);
				fw.close();
			} 
			obj.disconnect();	
		}catch(Exception e) {
			System.err.println("Error on entry: " + e.toString());
			return;
		}
		



	}
}
