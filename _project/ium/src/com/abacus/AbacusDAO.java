package com.abacus;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
//import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.DateHelper;

/*
 * Created on Dec 22, 2003
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */

/**
 * @author ypojol
 *
 * To change the  template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class AbacusDAO {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(AbacusDAO.class);
	private String dbURL = null;
	private String username = null;
	private String password = null;
	private String driver = null;
	private Properties prop = null;
	/**
	 * 
	 */
	public AbacusDAO(String file) {
		LOGGER.info("AbacusDAO constructor start");
		try{
			ClassLoader loader = AbacusDAO.class.getClassLoader();
			InputStream in = loader.getResourceAsStream(file);
			prop = new Properties();
			prop.load(in);			
			dbURL = prop.getProperty("dbURL");
			driver = prop.getProperty("driver");
			username = prop.getProperty("username");
			password = prop.getProperty("password");
		}catch(IOException e){
			LOGGER.error(CodeHelper.getStackTrace(e));
		}
		LOGGER.info("AbacusDAO constructor end");
	}

	private void init(){
		dbURL = prop.getProperty("dbURL");
		driver = prop.getProperty("driver");
		username = prop.getProperty("username");
		password = prop.getProperty("password");	
	}
	
	public void closeDB(Connection conn) {
		LOGGER.info("closeDB start");
		try {
			conn.close();
		} catch (Exception e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
		}
		LOGGER.info("closeDB end");
	}
	
	private Connection getConnection(){
		LOGGER.info("getConnection start");
		Connection conn = null;
		try{
			Class.forName(driver);
			conn = DriverManager.getConnection(dbURL, username, password);
		}catch(ClassNotFoundException cnf){
			LOGGER.error(CodeHelper.getStackTrace(cnf));
		}catch(SQLException e){
			LOGGER.error(CodeHelper.getStackTrace(e));
		} finally {
			
		}
		LOGGER.info("getConnection end");
		return conn;
		
	}
	

	public String getPolQueueInfo(String strMsg){
		LOGGER.info("getPolQueueInfo start");
		LOGGER.debug("strMsg-->" + strMsg);
		
		String userID = strMsg.substring(0, 8);
		String msgID = strMsg.substring(8, 18);
		String retStr = null;
		boolean recFound = false;
		Connection conn = null;
		
		try{
			conn = this.getConnection();
			String stmt = "SELECT POL_ID, CVG_FACE_AMT, POL_MPREM_AMT, APP_RECV_DT, NB_CNTCT_USER_ID, " + 
							"UW_USER_ID, INSR_CLI_ID, INSR_SUR_NM, INSR_GIV_NM, INSR_MID_NM, AGT_ID, SOFF_ID " +
							"FROM policy_queue";
			PreparedStatement ps = conn.prepareStatement(stmt);
			ResultSet rs = ps.executeQuery();
			StringBuffer sb = new StringBuffer();
			sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\" ?>");
			sb.append("<ROOT>");
			sb.append("<MSG_ID>" + msgID + "</MSG_ID>");
			
			StringBuffer tsb = new StringBuffer();
			while(rs.next()){
				recFound = true;
				tsb.append("<POL_DATA>");
				tsb.append("<POL_ID>" + checkValue(rs.getString("POL_ID")) + "</POL_ID>");
				tsb.append("<CVG_FACE_AMT>" + checkValue(rs.getString("CVG_FACE_AMT")) + "</CVG_FACE_AMT>");
				tsb.append("<POL_MPREM_AMT>" + checkValue(rs.getString("POL_MPREM_AMT")) + "</POL_MPREM_AMT>");
				tsb.append("<APP_RECV_DT>" + DateHelper.format(rs.getDate("APP_RECV_DT"), "yyyy-MM-DD") + "</APP_RECV_DT>");
				tsb.append("<NB_CNTCT_USER_ID>" + checkValue(rs.getString("NB_CNTCT_USER_ID")) + "</NB_CNTCT_USER_ID>");
				tsb.append("<UW_USER_ID>" + checkValue(rs.getString("UW_USER_ID")) + "</UW_USER_ID>");
				tsb.append("<INSR_CLI_ID>" + checkValue(rs.getString("INSR_CLI_ID")) + "</INSR_CLI_ID>");
				tsb.append("<INSR_SUR_NM>" + checkValue(rs.getString("INSR_SUR_NM")) + "</INSR_SUR_NM>");
				tsb.append("<INSR_GIV_NM>" + checkValue(rs.getString("INSR_GIV_NM")) + "</INSR_GIV_NM>");
				tsb.append("<INSR_MID_NM>" + checkValue(rs.getString("INSR_MID_NM")) + "</INSR_MID_NM>");
				tsb.append("<AGT_ID>" + checkValue(rs.getString("AGT_ID")) + "</AGT_ID>");
				tsb.append("<SOFF_ID>" + checkValue(rs.getString("SOFF_ID")) + "</SOFF_ID>");
				tsb.append("</POL_DATA>");
			}
			
			if(recFound){
				sb.append("<CONF_CD>00</CONF_CD>");
			}else{
				sb.append("<CONF_CD>01</CONF_CD>");				
			}
			sb.append("<POL_QUEUE_INFO>");
			sb.append(tsb);		
			sb.append("</POL_QUEUE_INFO>");	
			sb.append("</ROOT>");
			retStr = sb.toString();
			conn.close();
		}catch(SQLException e){
			LOGGER.error(CodeHelper.getStackTrace(e));
		} finally {
			closeDB(conn);
		}
		LOGGER.info("getPolQueueInfo end");
		return retStr;
	}

	public String getKOInfo(String strMsg){
		LOGGER.info("getKOInfo start");
		LOGGER.debug("strMsg-->" + strMsg);
		
		String retStr = null;
		String userID = strMsg.substring(0, 8);
		String msgID = strMsg.substring(8, 18);	
		String subPolNum = strMsg.substring(18, 28);
		boolean recFound = false;
		Connection conn = null;
		
		try{
			
			conn = this.getConnection();
			String stmt = "SELECT CLI_ID, SEQ_NUM, MSG_TXT, MSG_RESP_TXT FROM kick_out WHERE SUBJ_POL_ID = ?";
			PreparedStatement ps = conn.prepareStatement(stmt);
			ps.setString(1, subPolNum.trim());
			ResultSet rs = ps.executeQuery();
			StringBuffer sb = new StringBuffer();
			sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\" ?>");
			sb.append("<ROOT>");
			sb.append("<MSG_ID>" + msgID + "</MSG_ID>");
			
			StringBuffer tsb = new StringBuffer();
			while(rs.next()){
				recFound = true;
				tsb.append("<CCKO_DATA>");
				tsb.append("<CLI_ID>" + checkValue(rs.getString("CLI_ID")) + "</CLI_ID>");
				tsb.append("<SEQ_NUM>" + checkValue(rs.getString("SEQ_NUM")) + "</SEQ_NUM>");
				tsb.append("<MSG_TXT>" + checkValue(rs.getString("MSG_TXT")) + "</MSG_TXT>");
				tsb.append("<MSG_RESP_TXT>" + checkValue(rs.getString("MSG_RESP_TXT")) + "</MSG_RESP_TXT>");
				tsb.append("</CCKO_DATA>");
			}
			if(recFound){
				sb.append("<CONF_CD>00</CONF_CD>");
			}else{
				sb.append("<CONF_CD>01</CONF_CD>");				
			}
			sb.append("<SUBJ_POL_ID>" + subPolNum + "</SUBJ_POL_ID>");
			sb.append(tsb);			
			sb.append("</ROOT>");
			retStr = sb.toString();
			conn.close();
		}catch(SQLException e){
			LOGGER.error(CodeHelper.getStackTrace(e));
		} finally {
			closeDB(conn);
		}
		LOGGER.info("getKOInfo end");
		return retStr;
	}
	
	

	public String retrieveReqInfo(String strMsg){
		
		LOGGER.info("retrieveReqInfo start");
		LOGGER.debug("strMsg-->" + strMsg);
		String retStr = null;
		String userID = strMsg.substring(0, 8);
		String msgID = strMsg.substring(8, 18);	
		String subPolNum = strMsg.substring(18, 28);
		boolean recFound = false;
		Connection conn = null;
		
		try{
			conn = this.getConnection();
			String stmt = "SELECT A.POL_OR_CLI_CD, A.POL_OR_CLI_ID, A.CLI_TYPE, A.REQIR_ID, A.SEQ_NUM, A.STAT_CD, " + 
						  "A.STAT_DT, A.FOLWUP_DT, A.REQT_DESC, A.CREAT_DT, A.UPDT_USER_ID, A.UPDT_DT, " + 
						  "A.DESGNT_ID, A.TST_DT, A.TST_RSLT_CD, A.TST_RSLT_DESC, A.RESOLV_IND, A.FOLWUP_NUM, " + 
						  "A.VALID_DT, A.PD_IND, A.NEW_RSLT_IND, A.ORDR_DT, A.SUGST_IND, A.RECV_DT, " + 
						  "A.AUTO_ORDR_CD, A.FLD_COMUN_MSG_IND, A.UW_COMNT_TXT, B.SEQ_NUM, B.SUBJ_POL_ID " +
						  "FROM REQUIREMENT A, REQUIREMENT_POLICY B " + 
						  "WHERE B.SUBJ_POL_ID = ? AND A.SEQ_NUM = B.SEQ_NUM";
			PreparedStatement ps = conn.prepareStatement(stmt);
			ps.setString(1, subPolNum.trim());
			ResultSet rs = ps.executeQuery();
			
			StringBuffer sb = new StringBuffer();
			sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\" ?>");
			sb.append("<ROOT>");
			sb.append("<MSG_ID>" + msgID + "</MSG_ID>");	
			
			StringBuffer tsb = new StringBuffer();
			while(rs.next()){
				recFound = true;
				tsb.append("<REQT_DATA>");
				tsb.append("<POL_OR_CLI_CD>" + checkValue(rs.getString("POL_OR_CLI_CD")) + "</POL_OR_CLI_CD>");
				tsb.append("<POL_OR_CLI_ID>" + checkValue(rs.getString("POL_OR_CLI_ID")) + "</POL_OR_CLI_ID>");
				tsb.append("<CLI_TYPE>" + checkValue(rs.getString("CLI_TYPE")) + "</CLI_TYPE>");
				tsb.append("<REQIR_ID>" + checkValue(rs.getString("REQIR_ID")) + "</REQIR_ID>");
				tsb.append("<SEQ_NUM>" + checkValue(rs.getString("SEQ_NUM")) + "</SEQ_NUM>");
				tsb.append("<STAT_CD>" + checkValue(rs.getString("STAT_CD")) + "</STAT_CD>");
				tsb.append("<STAT_DT>" + DateHelper.format(rs.getDate("STAT_DT"), "yyyy-MM-DD")+ "</STAT_DT>");
				tsb.append("<FOLWUP_DT>" + DateHelper.format(rs.getDate("FOLWUP_DT"), "yyyy-MM-DD") + "</FOLWUP_DT>");
				tsb.append("<REQT_DESC>" + checkValue(rs.getString("REQT_DESC")) + "</REQT_DESC>");
				tsb.append("<CREAT_DT>" + DateHelper.format(rs.getDate("CREAT_DT"), "yyyy-MM-DD") + "</CREAT_DT>");
				tsb.append("<UPDT_USER_ID>" + checkValue(rs.getString("UPDT_USER_ID")) + "</UPDT_USER_ID>");
				tsb.append("<UPDT_DT>" + DateHelper.format(rs.getDate("UPDT_DT"), "yyyy-MM-DD") + "</UPDT_DT>");
				tsb.append("<DESGNT_ID>" + checkValue(rs.getString("DESGNT_ID")) + "</DESGNT_ID>");
				tsb.append("<TST_DT>" + DateHelper.format(rs.getDate("TST_DT"), "yyyy-MM-DD") + "</TST_DT>");
				tsb.append("<TST_RSLT_CD>" + checkValue(rs.getString("TST_RSLT_CD")) + "</TST_RSLT_CD>");
				tsb.append("<TST_RSLT_DESC>" + checkValue(rs.getString("TST_RSLT_DESC")) + "</TST_RSLT_DESC>");
				tsb.append("<RESOLV_IND>" + checkValue(rs.getString("RESOLV_IND")) + "</RESOLV_IND>");
				tsb.append("<FOLWUP_NUM>" + checkValue(rs.getString("FOLWUP_NUM")) + "</FOLWUP_NUM>");
				tsb.append("<VALID_DT>" + DateHelper.format(rs.getDate("VALID_DT"), "yyyy-MM-DD") + "</VALID_DT>");
				tsb.append("<PD_IND>" + checkValue(rs.getString("PD_IND")) + "</PD_IND>");
				tsb.append("<NEW_RSLT_IND>" + checkValue(rs.getString("NEW_RSLT_IND")) + "</NEW_RSLT_IND>");
				tsb.append("<ORDR_DT>" + DateHelper.format(rs.getDate("ORDR_DT"), "yyyy-MM-DD") + "</ORDR_DT>");
				tsb.append("<SUGST_IND>" + checkValue(rs.getString("SUGST_IND")) + "</SUGST_IND>");
				tsb.append("<RECV_DT>" + DateHelper.format(rs.getDate("RECV_DT"), "yyyy-MM-DD") + "</RECV_DT>");
				tsb.append("<AUTO_ORDR_CD>" + checkValue(rs.getString("AUTO_ORDR_CD")) + "</AUTO_ORDR_CD>");
				tsb.append("<FLD_COMUN_MSG_IND>" + checkValue(rs.getString("FLD_COMUN_MSG_IND")) + "</FLD_COMUN_MSG_IND>");
				tsb.append("<UW_COMNT_TXT>" + checkValue(rs.getString("UW_COMNT_TXT")) + "</UW_COMNT_TXT>");
				tsb.append("</REQT_DATA>");
			}
			if(recFound){
				sb.append("<CONF_CD>00</CONF_CD>");
			}else{
				sb.append("<CONF_CD>01</CONF_CD>");				
			}
			sb.append("<SUBJ_POL_ID>" + subPolNum + "</SUBJ_POL_ID>");
			sb.append(tsb);				
			sb.append("</ROOT>");
			retStr = sb.toString();
			conn.close();
		}catch(SQLException e){
			LOGGER.error(CodeHelper.getStackTrace(e));
		} finally {
			closeDB(conn);
		}
		LOGGER.info("retrieveReqInfo end");
		return retStr;
	}
	
		
	public String getPolCovInfo(String strMsg){
		
		LOGGER.info("getPolCovInfo start");
		LOGGER.debug("strMsg-->" + strMsg);
		String retStr = null;
		String userID = strMsg.substring(0, 8);
		String msgID = strMsg.substring(8, 18);	
		String subPolNum = strMsg.substring(18, 28);
		String transCode = strMsg.substring(28, 29);		
		boolean recFound = false;
		Connection conn = null;
		
		try{
			
			conn = this.getConnection();
			String stmt = "SELECT REL_POL_ID, CVG_NUM, REL_CD, CVG_ISS_EFF_DT, CVG_STAT_CD, CVG_SMKR_CD, " + 
						  "CVG_PLAN_ID, MEDIC_IND, CVG_FACE_AMT, DEC_TYP, AD_FACE_AMT, AD_MULT, WP_MULT, " + 
						  "REINS_AMT FROM policy_coverage_info WHERE SUBJ_POL_ID = ?";

			PreparedStatement ps = conn.prepareStatement(stmt);
			ps.setString(1, subPolNum.trim());
			ResultSet rs = ps.executeQuery();
			StringBuffer sb = new StringBuffer();
			sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\" ?>");
			sb.append("<ROOT>");
			sb.append("<MSG_ID>" + msgID + "</MSG_ID>");
			
			StringBuffer tsb = new StringBuffer();
			while(rs.next()){
				recFound = true;
				tsb.append("<REL_POL_INFO>");
				tsb.append("<REL_POL_ID>" + checkValue(rs.getString("REL_POL_ID")) + "</REL_POL_ID>");
				tsb.append("<CVG_NUM>" + checkValue(rs.getString("CVG_NUM")) + "</CVG_NUM>");
				tsb.append("<REL_CD>" + checkValue(rs.getString("REL_CD")) + "</REL_CD>");
				tsb.append("<CVG_ISS_EFF_DT>" + DateHelper.format(rs.getDate("CVG_ISS_EFF_DT"), "yyyy-MM-dd") + "</CVG_ISS_EFF_DT>");
				tsb.append("<CVG_STAT_CD>" + checkValue(rs.getString("CVG_STAT_CD")) + "</CVG_STAT_CD>");
				tsb.append("<CVG_SMKR_CD>" + checkValue(rs.getString("CVG_SMKR_CD")) + "</CVG_SMKR_CD>");
				tsb.append("<CVG_PLAN_ID>" + checkValue(rs.getString("CVG_PLAN_ID")) + "</CVG_PLAN_ID>");
				tsb.append("<MEDIC_IND>" + checkValue(rs.getString("MEDIC_IND")) + "</MEDIC_IND>");
				tsb.append("<CVG_FACE_AMT>" + checkValue(rs.getString("CVG_FACE_AMT")) + "</CVG_FACE_AMT>");
				tsb.append("<DEC_TYP>" + checkValue(rs.getString("DEC_TYP")) + "</DEC_TYP>");
				tsb.append("<AD_FACE_AMT>" + checkValue(rs.getString("AD_FACE_AMT")) + "</AD_FACE_AMT>");
				tsb.append("<AD_MULT>" + checkValue(rs.getString("AD_MULT")) + "</AD_MULT>");
				tsb.append("<WP_MULT>" + checkValue(rs.getString("WP_MULT")) + "</WP_MULT>");
				tsb.append("<REINS_AMT>" + checkValue(rs.getString("REINS_AMT")) + "</REINS_AMT>");
				tsb.append("</REL_POL_INFO>");
			}
			
			if(transCode.equalsIgnoreCase("D")){
				stmt = "DELETE FROM POLICY_COVERAGE_INFO WHERE SUBJ_POL_ID = ?";
				ps = conn.prepareStatement(stmt);
				ps.setString(1, subPolNum);
				int recDeleted = ps.executeUpdate();
			}
						
			if(recFound){
				sb.append("<CONF_CD>00</CONF_CD>");
			}else{
				sb.append("<CONF_CD>01</CONF_CD>");				
			}
			sb.append("<SUBJ_POL_ID>" + subPolNum + "</SUBJ_POL_ID>");
			sb.append(tsb);				
			sb.append("</ROOT>");
			
			retStr = sb.toString();
			conn.close();
			
		}catch(SQLException e){
			LOGGER.error(CodeHelper.getStackTrace(e));
		} finally {
			closeDB(conn);
		}
		LOGGER.info("getPolCovInfo end");
		return retStr;
	}


	public String updateClientInfo(String strMsg){
		
		LOGGER.info("updateClientInfo start");
		LOGGER.debug("strMsg-->" + strMsg);
		
		String retStr = null;
		String userID = strMsg.substring(0, 8);
		String msgID = strMsg.substring(8, 18);	
		String subPolNum = strMsg.substring(18, 28);
		String transCode = strMsg.substring(28, 29);	
		boolean recFound = false;
		Connection conn = null;
			
		try{
			
			conn = this.getConnection();
			String stmt = "SELECT A.INSR_CLI_ID, A.INSR_CLI_ID, A.INSR_SUR_NM, A.INSR_GIV_NM, A.INSR_MID_NM, " + 
						"A.INSR_OTH_SUR_NM, A.INSR_OTH_GIV_NM, A.INSR_OTH_MID_NM, A.INSR_TITL_TXT, 	A.INSR_SFX_NM, " + 
						"A.INSR_BTH_DT, A.INSR_BTH_LOC, A.INSR_SEX_CD, A.INSR_CLI_AGE, A.INSR_SMKR_CD, " + 
						"B.OWN_CLI_ID, B.OWN_SUR_NM, B.OWN_GIV_NM, B.OWN_MID_NM, B.OWN_OTH_SUR_NM, " + 
						"B.OWN_OTH_GIV_NM, B.OWN_OTH_MID_NM, B.OWN_TITL_TXT, B.OWN_SFX_NM, B.OWN_BTH_DT, " + 
						"B.OWN_BTH_LOC, B.OWN_SEX_CD, B.OWN_CLI_AGE, B.OWN_SMKR_CD, " + 
						"C.APP_BR_AMT, C.APP_AD_AMT, C.PEND_BR_AMT, C.PEND_AD_AMT, C.INFC_BR_AMT, " +
						"C.INFC_AD_AMT, C.LAPS_BR_AMT, C.LAPS_AD_AMT, C.OINS_BR_AMT, C.OINS_AD_AMT, " +
						"C.INFC_CCR_AMT, C.INFC_APDB_AMT, C.INFC_HIB_AMT, C.INFC_FMB_AMT, C.TOT_REINS_AMT, " +
						"D.INSR_CLI_HT_FT, D.INSR_CLI_HT_INCH, D.INSR_CLI_WT_LBS, D.INSR_BASIC_RAT1, " +
						"D.INSR_BASIC_RAT2, D.INSR_BASIC_RAT3, D.INSR_CCR_RAT1, D.INSR_CCR_RAT2, " +
						"D.INSR_CCR_RAT3, D.INSR_APDB_RAT1, D.INSR_APDB_RAT2, D.INSR_APDB_RAT3, " +
						"E.OWN_CLI_HT_FT, E.OWN_CLI_HT_INCH, E.OWN_CLI_WT_LBS, E.OWN_BASIC_RAT1, " +
						"E.OWN_BASIC_RAT2, E.OWN_BASIC_RAT3, E.OWN_CCR_RAT1, E.OWN_CCR_RAT2, E.OWN_CCR_RAT3, " +
						"E.OWN_APDB_RAT1, E.OWN_APDB_RAT2, E.OWN_APDB_RAT3, " +
						"F.SUBJ_POL_ID, F.STAT_CD " + 
						"FROM INSR_CLI_INFO A, OWN_CLI_INFO B, TOT_EXTG_INFO C, INSR_CLI_MORT D, OWN_CLI_MORT E, " +
						"CDS F " +
						"WHERE F.SUBJ_POL_ID = ? " +
						"AND F.SUBJ_POL_ID = B.SUBJ_POL_ID (+) " +
						"AND F.SUBJ_POL_ID = C.SUBJ_POL_ID (+) " +
						"AND F.SUBJ_POL_ID = D.SUBJ_POL_ID (+) " +
						"AND F.SUBJ_POL_ID = E.SUBJ_POL_ID (+) " +
						"AND F.SUBJ_POL_ID = A.SUBJ_POL_ID (+)";			
			
			PreparedStatement ps = conn.prepareStatement(stmt);
			ps.setString(1, subPolNum.trim());
			ResultSet rs = ps.executeQuery();
		
			
			StringBuffer sb = new StringBuffer();
			sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\" ?>");
			sb.append("<ROOT>");
			sb.append("<MSG_ID>" + msgID + "</MSG_ID>");			
			
			StringBuffer tsb = new StringBuffer();
			if(rs.next()){
				recFound = true;
				tsb.append("<SUBJ_POL_ID>" + checkValue(rs.getString("SUBJ_POL_ID")) + "</SUBJ_POL_ID>");
				tsb.append("<STAT_CD>" + checkValue(rs.getString("STAT_CD")) + "</STAT_CD>");
				
				tsb.append("<INSR_CLI_INFO>");
				tsb.append("<INSR_CLI_ID>" + checkValue(rs.getString("INSR_CLI_ID")) + "</INSR_CLI_ID>");
				tsb.append("<INSR_SUR_NM>" + checkValue(rs.getString("INSR_SUR_NM")) + "</INSR_SUR_NM>");
				tsb.append("<INSR_GIV_NM>" + checkValue(rs.getString("INSR_GIV_NM")) + "</INSR_GIV_NM>");
				tsb.append("<INSR_MID_NM>" + checkValue(rs.getString("INSR_MID_NM")) + "</INSR_MID_NM>");
				tsb.append("<INSR_OTH_SUR_NM>" + checkValue(rs.getString("INSR_OTH_SUR_NM")) + "</INSR_OTH_SUR_NM>");
				tsb.append("<INSR_OTH_GIV_NM>" + checkValue(rs.getString("INSR_OTH_GIV_NM")) + "</INSR_OTH_GIV_NM>");
				tsb.append("<INSR_OTH_MID_NM>" + checkValue(rs.getString("INSR_OTH_MID_NM")) + "</INSR_OTH_MID_NM>");
				tsb.append("<INSR_TITL_TXT>" + checkValue(rs.getString("INSR_TITL_TXT")) + "</INSR_TITL_TXT>");
				tsb.append("<INSR_SFX_NM>" + checkValue(rs.getString("INSR_SFX_NM")) + "</INSR_SFX_NM>");
				tsb.append("<INSR_BTH_DT>" + DateHelper.format(rs.getDate("INSR_BTH_DT"), "yyyy-MM-DD") + "</INSR_BTH_DT>");
				tsb.append("<INSR_BTH_LOC>" + checkValue(rs.getString("INSR_BTH_LOC")) + "</INSR_BTH_LOC>");
				tsb.append("<INSR_SEX_CD>" + checkValue(rs.getString("INSR_SEX_CD")) + "</INSR_SEX_CD>");
				tsb.append("<INSR_CLI_AGE>" + checkValue(rs.getString("INSR_CLI_AGE")) + "</INSR_CLI_AGE>");
				tsb.append("<INSR_SMKR_CD>" + checkValue(rs.getString("INSR_SMKR_CD")) + "</INSR_SMKR_CD>");
				tsb.append("</INSR_CLI_INFO>");
				
				tsb.append("<OWN_CLI_INFO>");
				tsb.append("<OWN_CLI_ID>" + checkValue(rs.getString("OWN_CLI_ID")) + "</OWN_CLI_ID>");
				tsb.append("<OWN_SUR_NM>" + checkValue(rs.getString("OWN_SUR_NM")) + "</OWN_SUR_NM>");
				tsb.append("<OWN_GIV_NM>" + checkValue(rs.getString("OWN_GIV_NM")) + "</OWN_GIV_NM>");
				tsb.append("<OWN_MID_NM>" + checkValue(rs.getString("OWN_MID_NM")) + "</OWN_MID_NM>");
				tsb.append("<OWN_OTH_SUR_NM>" + checkValue(rs.getString("OWN_OTH_SUR_NM")) + "</OWN_OTH_SUR_NM>");
				tsb.append("<OWN_OTH_GIV_NM>" + checkValue(rs.getString("OWN_OTH_GIV_NM")) + "</OWN_OTH_GIV_NM>");
				tsb.append("<OWN_OTH_MID_NM>" + checkValue(rs.getString("OWN_OTH_MID_NM")) + "</OWN_OTH_MID_NM>");
				tsb.append("<OWN_TITL_TXT>" + checkValue(rs.getString("OWN_TITL_TXT")) + "</OWN_TITL_TXT>");
				tsb.append("<OWN_SFX_NM>" + checkValue(rs.getString("OWN_SFX_NM")) + "</OWN_SFX_NM>");
				tsb.append("<OWN_BTH_DT>" + DateHelper.format(rs.getDate("OWN_BTH_DT"), "yyyy-MM-DD") + "</OWN_BTH_DT>");
				tsb.append("<OWN_BTH_LOC>" + checkValue(rs.getString("OWN_BTH_LOC")) + "</OWN_BTH_LOC>");
				tsb.append("<OWN_SEX_CD>" + checkValue(rs.getString("OWN_SEX_CD")) + "</OWN_SEX_CD>");
				tsb.append("<OWN_CLI_AGE>" + checkValue(rs.getString("OWN_CLI_AGE")) + "</OWN_CLI_AGE>");
				tsb.append("<OWN_SMKR_CD>" + checkValue(rs.getString("OWN_SMKR_CD")) + "</OWN_SMKR_CD>");
				tsb.append("</OWN_CLI_INFO>");
				
				tsb.append("<TOT_EXTG_INFO>");
				tsb.append("<APP_BR_AMT>" + checkValue(rs.getString("APP_BR_AMT")) + "</APP_BR_AMT>");
				tsb.append("<APP_AD_AMT>" + checkValue(rs.getString("APP_AD_AMT")) + "</APP_AD_AMT>");
				tsb.append("<PEND_BR_AMT>" + checkValue(rs.getString("PEND_BR_AMT")) + "</PEND_BR_AMT>");
				tsb.append("<PEND_AD_AMT>" + checkValue(rs.getString("PEND_AD_AMT")) + "</PEND_AD_AMT>");
				tsb.append("<INFC_BR_AMT>" + checkValue(rs.getString("INFC_BR_AMT")) + "</INFC_BR_AMT>");
				tsb.append("<INFC_AD_AMT>" + checkValue(rs.getString("INFC_AD_AMT")) + "</INFC_AD_AMT>");
				tsb.append("<LAPS_BR_AMT>" + checkValue(rs.getString("LAPS_BR_AMT")) + "</LAPS_BR_AMT>");
				tsb.append("<LAPS_AD_AMT>" + checkValue(rs.getString("LAPS_AD_AMT")) + "</LAPS_AD_AMT>");
				tsb.append("<OINS_BR_AMT>" + checkValue(rs.getString("OINS_BR_AMT")) + "</OINS_BR_AMT>");
				tsb.append("<OINS_AD_AMT>" + checkValue(rs.getString("OINS_AD_AMT")) + "</OINS_AD_AMT>");
				tsb.append("<INFC_CCR_AMT>" + checkValue(rs.getString("INFC_CCR_AMT")) + "</INFC_CCR_AMT>");
				tsb.append("<INFC_APDB_AMT>" + checkValue(rs.getString("INFC_APDB_AMT")) + "</INFC_APDB_AMT>");
				tsb.append("<INFC_HIB_AMT>" + checkValue(rs.getString("INFC_HIB_AMT")) + "</INFC_HIB_AMT>");
				tsb.append("<INFC_FMB_AMT>" + checkValue(rs.getString("INFC_FMB_AMT")) + "</INFC_FMB_AMT>");
				tsb.append("<TOT_REINS_AMT>" + checkValue(rs.getString("TOT_REINS_AMT")) + "</TOT_REINS_AMT>");
				tsb.append("</TOT_EXTG_INFO>");
				
				tsb.append("<MORTALITY_INFO>");
				tsb.append("<INSR_CLI_MORT>");
				tsb.append("<INSR_CLI_HT_FT>" + checkValue(rs.getString("INSR_CLI_HT_FT")) + "</INSR_CLI_HT_FT>");
				tsb.append("<INSR_CLI_HT_INCH>" + checkValue(rs.getString("INSR_CLI_HT_INCH")) + "</INSR_CLI_HT_INCH>");
				tsb.append("<INSR_CLI_WT_LBS>" + checkValue(rs.getString("INSR_CLI_WT_LBS")) + "</INSR_CLI_WT_LBS>");
				tsb.append("<INSR_BASIC_RAT1>" + checkValue(rs.getString("INSR_BASIC_RAT1")) + "</INSR_BASIC_RAT1>");
				tsb.append("<INSR_BASIC_RAT2>" + checkValue(rs.getString("INSR_BASIC_RAT2")) + "</INSR_BASIC_RAT2>");
				tsb.append("<INSR_BASIC_RAT3>" + checkValue(rs.getString("INSR_BASIC_RAT3")) + "</INSR_BASIC_RAT3>");
				tsb.append("<INSR_CCR_RAT1>" + checkValue(rs.getString("INSR_CCR_RAT1")) + "</INSR_CCR_RAT1>");
				tsb.append("<INSR_CCR_RAT2>" + checkValue(rs.getString("INSR_CCR_RAT2")) + "</INSR_CCR_RAT2>");
				tsb.append("<INSR_CCR_RAT3>" + checkValue(rs.getString("INSR_CCR_RAT3")) + "</INSR_CCR_RAT3>");
				tsb.append("<INSR_APDB_RAT1>" + checkValue(rs.getString("INSR_APDB_RAT1")) + "</INSR_APDB_RAT1>");
				tsb.append("<INSR_APDB_RAT2>" + checkValue(rs.getString("INSR_APDB_RAT2")) + "</INSR_APDB_RAT2>");
				tsb.append("<INSR_APDB_RAT3>" + checkValue(rs.getString("INSR_APDB_RAT3")) + "</INSR_APDB_RAT3>");
				tsb.append("</INSR_CLI_MORT>");
				tsb.append("<OWN_CLI_MORT>");
				tsb.append("<OWN_CLI_HT_FT>" + checkValue(rs.getString("OWN_CLI_HT_FT")) + "</OWN_CLI_HT_FT>");
				tsb.append("<OWN_CLI_HT_INCH>" + checkValue(rs.getString("OWN_CLI_HT_INCH")) + "</OWN_CLI_HT_INCH>");
				tsb.append("<OWN_CLI_WT_LBS>" + checkValue(rs.getString("OWN_CLI_WT_LBS")) + "</OWN_CLI_WT_LBS>");
				tsb.append("<OWN_BASIC_RAT1>" + checkValue(rs.getString("OWN_BASIC_RAT1")) + "</OWN_BASIC_RAT1>");
				tsb.append("<OWN_BASIC_RAT2>" + checkValue(rs.getString("OWN_BASIC_RAT2")) + "</OWN_BASIC_RAT2>");
				tsb.append("<OWN_BASIC_RAT3>" + checkValue(rs.getString("OWN_BASIC_RAT3")) + "</OWN_BASIC_RAT3>");
				tsb.append("<OWN_CCR_RAT1>" + checkValue(rs.getString("OWN_CCR_RAT1")) + "</OWN_CCR_RAT1>");
				tsb.append("<OWN_CCR_RAT2>" + checkValue(rs.getString("OWN_CCR_RAT2")) + "</OWN_CCR_RAT2>");
				tsb.append("<OWN_CCR_RAT3>" + checkValue(rs.getString("OWN_CCR_RAT3")) + "</OWN_CCR_RAT3>");
				tsb.append("<OWN_APDB_RAT1>" + checkValue(rs.getString("OWN_APDB_RAT1")) + "</OWN_APDB_RAT1>");
				tsb.append("<OWN_APDB_RAT2>" + checkValue(rs.getString("OWN_APDB_RAT2")) + "</OWN_APDB_RAT2>");
				tsb.append("<OWN_APDB_RAT3>" + checkValue(rs.getString("OWN_APDB_RAT3")) + "</OWN_APDB_RAT3>");		
				tsb.append("</OWN_CLI_MORT>");
				tsb.append("</MORTALITY_INFO>");				
			}

			if(transCode.equalsIgnoreCase("D")){
				String [] tableArray = {"REQUIREMENT_POLICY", "POLICY_COVERAGE_INFO", "KICK_OUT", "TOT_EXTG_INFO","INSR_CLI_INFO", "OWN_CLI_INFO", "INSR_CLI_MORT","OWN_CLI_MORT", "CDS"};
				for (int i=0; i<tableArray.length; i++){
					stmt = "DELETE FROM " + tableArray[i] +
						   " WHERE SUBJ_POL_ID = ?";
					ps = conn.prepareStatement(stmt);
					ps.setString(1, subPolNum);
					ps.executeUpdate();
				}
				stmt = "DELETE FROM REQUIREMENT WHERE POL_OR_CLI_ID = ?";
				ps = conn.prepareStatement(stmt);
				ps.setString(1, subPolNum);
				ps.executeUpdate();
				
				String [] tables = {"POLICY_QUEUE", "POLICY"};
				for (int i =0; i<tables.length; i++){
					stmt = "DELETE FROM " + tables[i] + " WHERE POL_ID = ?";
					ps = conn.prepareStatement(stmt);
					ps.setString(1, subPolNum);
					ps.executeUpdate();
				}
			}			
			
			if(recFound){
				sb.append("<CONF_CD>00</CONF_CD>");
			}else{
				sb.append("<CONF_CD>01</CONF_CD>");				
			}
			sb.append("<CDS_DATA>");
			sb.append(tsb);			
			sb.append("</CDS_DATA>");
			sb.append("</ROOT>");
			
			retStr = sb.toString();
			conn.close();
		}catch(SQLException e){
			LOGGER.error(CodeHelper.getStackTrace(e));
		} finally {
			closeDB(conn);
		}
		
		LOGGER.info("updateClientInfo end");
		return retStr;
	}
	

	public String maintainReqInfo(String strMsg){
		
		LOGGER.info("maintainReqInfo start");
		LOGGER.debug("strMsg-->" + strMsg);
		
		String retStr = null;
		String userID = strMsg.substring(0, 8);
		String msgID = strMsg.substring(8, 18);
		String transCode = strMsg.substring(18, 19);
		String pol_cli_Code = strMsg.substring(19, 20);
		String pol_cli_ID = strMsg.substring(20, 30);
		String reqID = strMsg.substring(30, 35);	
		String seqNumber = strMsg.substring(35, 38);
		String statusCode = strMsg.substring(38, 41);
		String updatedBy = strMsg.substring(41, 49);
		String updateDate = strMsg.substring(49, 58);
		String designationID = strMsg.substring(58, 68);
		String testDate = strMsg.substring(68, 77);
		String testResultCode = strMsg.substring(77, 81);
		String folwUpNumber = strMsg.substring(81, 82);
		String folwUpDate = strMsg.substring(82, 91);
		String paidIndctr = strMsg.substring(91, 92);
		String validDate = strMsg.substring(92, 101);
		String newTestIndctr = strMsg.substring(101, 102);
		String autoOrdrCode = strMsg.substring(102, 103);
		String fldComMsgIndctr = strMsg.substring(103, 104);
		String uwCommentTxt = strMsg.substring(104, 154);
		String resolveIndctr = strMsg.substring(154, 155);
		boolean recFound = false;		
		Connection conn = this.getConnection();
		
		try{
		
			conn.setAutoCommit(false);
			String stmt = null;
			if(transCode.equalsIgnoreCase("C")){
				stmt = "INSERT INTO REQUIREMENT (POL_OR_CLI_CD, POL_OR_CLI_ID, REQIR_ID, SEQ_NUM, STAT_CD, UPDT_USER_ID, " + 
					   "CREAT_DT, DESGNT_ID, TST_DT, TST_RSLT_CD, FOLWUP_NUM, FOLWUP_DT, PD_IND, VALID_DT, " + 
					   "NEW_RSLT_IND, AUTO_ORDR_CD, FLD_COMUN_MSG_IND, UW_COMNT_TXT, RESOLV_IND, STAT_DT) " + 
					   "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

			//	seqNumber = new Integer(getNextValue("seq_reqir")).toString();
				LOGGER.info("create : sequence number : " + seqNumber);
				PreparedStatement ps = conn.prepareStatement(stmt);
				ps.setString(1, pol_cli_Code.trim());
				ps.setString(2, pol_cli_ID.trim());
				ps.setString(3, reqID.trim());
				ps.setString(4, seqNumber.trim());
				ps.setString(5, statusCode.trim());
				ps.setString(6, updatedBy.trim());
				ps.setString(7, updateDate.trim());
				ps.setString(8, designationID.trim());
				ps.setString(9, testDate.trim());
				ps.setString(10, testResultCode.trim());
				ps.setString(11, folwUpNumber.trim());
				ps.setString(12, folwUpDate.trim());
				ps.setString(13, paidIndctr.trim());
				ps.setString(14, validDate.trim());
				ps.setString(15, newTestIndctr.trim());
				ps.setString(16, autoOrdrCode.trim());
				ps.setString(17, fldComMsgIndctr.trim());
				ps.setString(18, uwCommentTxt.trim());
				ps.setString(19, resolveIndctr.trim());
				ps.setString(20, updateDate.trim());
				ps.executeUpdate();
				
				stmt = "SELECT POL_ID FROM POLICY WHERE POL_ID = ?"; 
				if(pol_cli_Code.equalsIgnoreCase("C")){
					stmt = "SELECT POL_ID FROM POLICY WHERE CLI_ID = ?";
				}
				PreparedStatement ps1 = conn.prepareStatement(stmt);
				ps1.setString(1, pol_cli_ID.trim());
				ResultSet rs = ps1.executeQuery();
				LOGGER.info("pol_cli_ID" + pol_cli_ID);
	
				PreparedStatement ps2 = conn.prepareStatement("INSERT INTO REQUIREMENT_POLICY (SUBJ_POL_ID, SEQ_NUM) VALUES (?, ?)");
				while(rs.next()){
					ps2.setString(1, rs.getString("POL_ID"));
					LOGGER.info("POL_ID: " + rs.getString("POL_ID"));
					ps2.setString(2, seqNumber.trim());
					ps2.executeUpdate();
				}
				
				conn.commit();
										
			}else if(transCode.equalsIgnoreCase("M")){
				if(statusCode.equalsIgnoreCase("ORD")){
					stmt = "UPDATE REQUIREMENT SET STAT_CD=?, " +
					   "UPDT_DT=?, FOLWUP_DT=? WHERE SEQ_NUM=?";
					PreparedStatement ps = conn.prepareStatement(stmt);
					ps.setString(1, statusCode.trim());
					ps.setString(2, updateDate.trim());
					ps.setString(3, folwUpDate.trim());
					ps.setString(4, seqNumber.trim());
					ps.executeUpdate();
					conn.commit();	   
				}else if(statusCode.equalsIgnoreCase("RCD")){
					stmt = "UPDATE REQUIREMENT SET STAT_CD=?, " +
					   "UPDT_DT=?, FOLWUP_NUM=?, FOLWUP_DT=? " +
					   "WHERE SEQ_NUM=?";
					PreparedStatement ps = conn.prepareStatement(stmt);
					ps.setString(1, statusCode.trim());
					ps.setString(2, updateDate.trim());
					ps.setString(3, folwUpNumber.trim());
					ps.setString(4, folwUpDate.trim());
					ps.setString(5, seqNumber.trim());
					ps.executeUpdate();
					conn.commit();	   
				}else if(statusCode.equalsIgnoreCase("RAC")){
					stmt = "UPDATE REQUIREMENT SET STAT_CD=?, " +
					   "UPDT_DT=?, FOLWUP_NUM=?, FOLWUP_DT=?, " +
					   "VALID_DT=? WHERE SEQ_NUM=?";
					PreparedStatement ps = conn.prepareStatement(stmt);
					ps.setString(1, statusCode.trim());
					ps.setString(2, updateDate.trim());
					ps.setString(3, folwUpNumber.trim());
					ps.setString(4, folwUpDate.trim());
					ps.setString(5, validDate.trim());
					ps.setString(6, seqNumber.trim());
					ps.executeUpdate();
					conn.commit();	   
				}else if(statusCode.equalsIgnoreCase("RRJ") || statusCode.equalsIgnoreCase("WVD") || statusCode.equalsIgnoreCase("CAN")){
					stmt = "UPDATE REQUIREMENT SET STAT_CD=?, " +
					   "FOLWUP_NUM=?, FOLWUP_DT=? WHERE SEQ_NUM=?";
					PreparedStatement ps = conn.prepareStatement(stmt);
					ps.setString(1, statusCode.trim());
					ps.setString(2, folwUpNumber.trim());
					ps.setString(3, folwUpDate.trim());
					ps.setString(4, seqNumber.trim());
					ps.executeUpdate();
					conn.commit();	   
				}else{
					stmt = "UPDATE REQUIREMENT SET POL_OR_CLI_CD=?, POL_OR_CLI_ID=?, REQIR_ID=?, STAT_CD=?, " +
						   "UPDT_USER_ID=?, UPDT_DT=?, DESGNT_ID=?, TST_DT=?, TST_RSLT_CD=?, FOLWUP_NUM=?, FOLWUP_DT=?, " +
						   "PD_IND=?, VALID_DT=?, NEW_RSLT_IND=?, AUTO_ORDR_CD=?, FLD_COMUN_MSG_IND=?, UW_COMNT_TXT=?, " +
						   "RESOLV_IND=? WHERE SEQ_NUM=?";
				
					PreparedStatement ps = conn.prepareStatement(stmt);
					ps.setString(1, pol_cli_Code.trim());
					ps.setString(2, pol_cli_ID.trim());
					ps.setString(3, reqID.trim());
					ps.setString(4, statusCode.trim());
					ps.setString(5, updatedBy.trim());
					ps.setString(6, updateDate.trim());
					ps.setString(7, designationID.trim());
					ps.setString(8, testDate.trim());
					ps.setString(9, testResultCode.trim());
					ps.setString(10, folwUpNumber.trim());
					ps.setString(11, folwUpDate.trim());
					ps.setString(12, paidIndctr.trim());
					ps.setString(13, validDate.trim());
					ps.setString(14, newTestIndctr.trim());
					ps.setString(15, autoOrdrCode.trim());
					ps.setString(16, fldComMsgIndctr.trim());
					ps.setString(17, uwCommentTxt.trim());
					ps.setString(18, resolveIndctr.trim());
					ps.setString(19, seqNumber.trim());
					ps.executeUpdate();
					conn.commit();	
				}   
			}	//end maintain		
			stmt = "SELECT POL_OR_CLI_CD, POL_OR_CLI_ID, REQIR_ID, SEQ_NUM, CREAT_DT, STAT_DT " + 
				   "FROM REQUIREMENT WHERE SEQ_NUM = ?";
			PreparedStatement ps = conn.prepareStatement(stmt);
			ps = conn.prepareStatement(stmt);
			ps.setString(1, seqNumber.trim());
			ResultSet rs = ps.executeQuery();
			
			StringBuffer sb = new StringBuffer();
			sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\" ?>");
			sb.append("<ROOT>");
			sb.append("<MSG_ID>" + msgID + "</MSG_ID>");

			
			StringBuffer tsb = new StringBuffer();
			if(rs.next()){
				recFound = true;
				tsb.append("<MSG1>" + "Message Line 1 for Requirement Maintenance Information" + "</MSG1>");
				tsb.append("<MSG2>" + "Message Line 2 for Requirement Maintenance Information" + "</MSG2>");
				tsb.append("<MSG3>" + "Message Line 3 for Requirement Maintenance Information" + "</MSG3>");
				tsb.append("<POL_OR_CLI_CD>" + rs.getString("POL_OR_CLI_CD") + "</POL_OR_CLI_CD>");
				tsb.append("<POL_OR_CLI_ID>" + rs.getString("POL_OR_CLI_ID") + "</POL_OR_CLI_ID>");
				tsb.append("<REQIR_ID>" + rs.getString("REQIR_ID") + "</REQIR_ID>");
				tsb.append("<SEQ_NUM>" + rs.getString("SEQ_NUM") + "</SEQ_NUM>");
				tsb.append("<CREAT_DT>" + rs.getString("CREAT_DT") + "</CREAT_DT>");
				tsb.append("<STAT_DT>" + rs.getString("STAT_DT") + "</STAT_DT>");
			}

			if(recFound){
				sb.append("<CONF_CD>00</CONF_CD>");
			}else{
				sb.append("<CONF_CD>01</CONF_CD>");				
			}
			sb.append(tsb);			
			sb.append("</ROOT>");
			retStr = sb.toString();
			conn.close();
		}catch(SQLException e){
			LOGGER.error(CodeHelper.getStackTrace(e));
			try{
				if(!conn.isClosed())
					conn.rollback();
				conn.close();
			}catch(SQLException sqle){
				LOGGER.error(CodeHelper.getStackTrace(e));
			}
		} finally {
			closeDB(conn);
		}
		LOGGER.info("maintainReqInfo end");
		return retStr;
	}
	

	public String getPolAutoSettleInfo(String strMsg){
		
		LOGGER.info("getPolAutoSettleInfo start");
		LOGGER.debug("strMsg-->" + strMsg);
		String retStr = null;
		String userID = strMsg.substring(0, 8);
		String msgID = strMsg.substring(8, 18);	
		String subPolNum = strMsg.substring(18, 28);
		String nbContactUserID = strMsg.substring(28, 36);
		String uwUserID = strMsg.substring(36, 44);
		String transCode = strMsg.substring(44, 45);	
		Connection conn = null;
		
		try{
			
			conn = this.getConnection();
			String stmt = "SELECT POL_ID FROM POLICY WHERE POL_ID = ?";
			PreparedStatement ps = conn.prepareStatement(stmt);
			ps.setString(1, subPolNum.trim());
			ResultSet rs = ps.executeQuery();
			
			StringBuffer sb = new StringBuffer();
			String conf = "";
			String msg1 = "";
			String msg2 = "";
			String msg3 = "";
			sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\" ?>");
			sb.append("<ROOT>");
			sb.append("<MSG_ID>" + msgID + "</MSG_ID>");
			
			if(rs.next()){
				conf = "00";
				msg1 = "Message 1 - successful";
				msg2 = "Message 2 - successful";
				msg3 = "Message 3 - successful";
			}else{
				conf = "01";
				msg1 = "Message 1 - record not found";
				msg2 = "Message 2 - record not found";
				msg3 = "Message 3 - record not found";
			}
			
			if(subPolNum.toLowerCase().indexOf("fenc")!=-1){
				conf = "02";
				msg1 = "Message 1 - error in nb contact";
				msg2 = "Message 2 - error in nb contact";
				msg3 = "Message 3 - error in nb contact";				
			}else if(subPolNum.toLowerCase().indexOf("fpsn")!=-1){
				conf = "03";
				msg1 = "Message 1 - policy status not equal to PCCU";
				msg2 = "Message 2 - policy status not equal to PCCU";
				msg3 = "Message 3 - policy status not equal to PCCU";				
			}else if(subPolNum.toLowerCase().indexOf("fusc")!=-1){
				conf = "04";
				msg1 = "Message 1 - unsuccessful sccp";
				msg2 = "Message 2 - unsuccessful sccp";
				msg3 = "Message 3 - unsuccessful sccp";				
			}else if(subPolNum.toLowerCase().indexOf("fuuw")!=-1){
				conf = "05";
				msg1 = "Message 1 - unsuccessful uwde";
				msg2 = "Message 2 - unsuccessful uwde";
				msg3 = "Message 3 - unsuccessful uwde";				
			}
			
			sb.append("<CONF_CD>" + conf + "</CONF_CD>");
		
			sb.append("<MSG1>" + msg1 + "</MSG1>");
			sb.append("<MSG2>" + msg2 + "</MSG2>");
			sb.append("<MSG3>" + msg3 + "</MSG3>");

			sb.append("</ROOT>");
			retStr = sb.toString();
			conn.close();
		}catch(SQLException e){
			LOGGER.error(CodeHelper.getStackTrace(e));
		} finally {
			closeDB(conn);
		}
		LOGGER.info("getPolAutoSettleInfo end");
		return retStr;
	}
	

	private String checkValue(String str){
		LOGGER.info("checkValue start");
		if (str == null)
			return "";
		String encoded_str = "";
		for (int i=0; i<str.length(); i++)
		{
			if (str.charAt(i) == '&')
				encoded_str += "&amp;";
			else if (str.charAt(i) == '\'')
				encoded_str += "&apos;";
			else if (str.charAt(i) == '\"')
				encoded_str += "&quot;";
			else if (str.charAt(i) == '>')
				encoded_str += "&gt;";
			else if (str.charAt(i) == '<')
				encoded_str += "&lt;";
			else
				encoded_str += str.charAt(i);
		}
		LOGGER.info("checkValue end");
		return encoded_str;						
	}

	
/*private int getNextValue(String seqName){
	LOGGER.info("getNextValue start");
	int retSeqNo=1;
	Connection conn = null;
	
	try{
		
		conn = getConnection();
		PreparedStatement ps = conn.prepareStatement("select " + seqName + ".nextVal from dual");
		ResultSet rs = ps.executeQuery();
		if(rs.next()){
			retSeqNo = rs.getInt(1);
		}
		
	}catch(SQLException e){
		LOGGER.error(CodeHelper.getStackTrace(e));
	} finally {
		closeDB(conn);
	}
	LOGGER.info("getNextValue end");
	return retSeqNo;
}
	*/
	

}
