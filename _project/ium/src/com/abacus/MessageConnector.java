/*
 * Created on Dec 17, 2003
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.abacus;

import java.util.ArrayList;
import java.util.ResourceBundle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
//import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.IUMConstants;
import com.sunlife.mq.SLMQComm;
import com.sunlife.mq.SLMQInitException;

/**
 * @author daguila
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class MessageConnector {
	private static final Logger LOGGER = LoggerFactory.getLogger(MessageConnector.class);
	//Logger LOGGER = LoggerFactory.getLogger(MessageConnector.class.getName());
	private SLMQComm mqObj;
	private String serviceName;

	public MessageConnector(String aServiceName) throws SLMQInitException{
		
		LOGGER.info("MessageConnector start 1");
		serviceName = aServiceName;
		MQConfig config = this.getConfiguration();
		if (config.isLoadedConfig()) {
			mqObj = new SLMQComm(config.getServiceName(), config.getHostName(), config.getPortNumber(), config.getChannel(),
					config.getQueueManager(), config.getPutQueue(), config.getGetQueue(), config.getServiceType(),
					config.getMaxWait(), config.getTransaction(), config.getDebugMode(), config.getUserID(), config.getPassword());
		} else {
			LOGGER.warn("SLMQInitException Unable to load MQ Configuration");
			throw new SLMQInitException("Unable to load MQ Configuration");
		}
		LOGGER.info("MessageConnector end 1");
	}

	public MessageConnector(String aServiceName, String aUserID, String aPassword){
		
		LOGGER.info("MessageConnector start 1");
		try {
			serviceName = aServiceName;
			mqObj = new SLMQComm(serviceName, aUserID, aPassword);
		}
		catch (SLMQInitException e) {
			LOGGER.error("Error during object instantiation-->" + CodeHelper.getStackTrace(e));
			 return;
		}
		LOGGER.info("MessageConnector end 1");
	}

	public MessageConnector(String aServiceName, String aHostName, int aPortNumber, String aChannel,
					String aQMgrName, String aPutQueueName, String aGetQueueName, String aServiceType,
					int aMaxWait, String aTransaction, String aDebug, String aUserID, String aPassword){
		
		LOGGER.info("MessageConnector start 3");
		serviceName = aServiceName;
		mqObj = new SLMQComm( aServiceName, aHostName, aPortNumber, aChannel, aQMgrName,
							aPutQueueName, aGetQueueName, aServiceType, aMaxWait, aTransaction,
							aDebug, aUserID, aPassword);
		LOGGER.info("MessageConnector end 3");
	}

	/**
	 * Retrieves SLMQCOMM settings from a configuration file specified in dbconfig.properties
	 * @throws SLMQInitException
	 *
	 */
	private MQConfig getConfiguration() throws SLMQInitException {
		
		LOGGER.info("getConfiguration start");
		ResourceBundle rb = ResourceBundle.getBundle(getMQConfigFile());
		if (null == rb) 
			throw new SLMQInitException("Unable to locate MQ Config File");
			LOGGER.warn("SLMQInitException Unable to locate MQ Config File");
			MQConfig mqConfig = new MQConfig();
			mqConfig.loadConfig(rb, serviceName);
		LOGGER.info("getConfiguration end");
		return mqConfig;
		
	}

	private String getMQConfigFile() {
		
		LOGGER.info("getMQConfigFile start");
		String configFile = null;
		try {
			ResourceBundle rb = ResourceBundle.getBundle(IUMConstants.IUM_CONFIG);
			configFile = rb.getString(IUMConstants.IUM_MQ_CONFIG_FILE);
			if (configFile == "" || configFile == null) {
				configFile = IUMConstants.IUM_MQ_DEF_CONFIG_FILE;
			}
		} catch (Exception e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			configFile = IUMConstants.IUM_MQ_DEF_CONFIG_FILE;
			LOGGER.debug("configFile-->" + configFile);
		}
		LOGGER.info("getMQConfigFile end");
		return configFile;
	}

	public void connect() throws SLMQInitException{
		
		LOGGER.info("connect start");
		mqObj.ServiceConnect(serviceName);
		if (mqObj.GetCommReturnCode() != 0) {
			LOGGER.warn("Error on connect : " + mqObj.GetErrorMessage());
			throw new SLMQInitException("MQ connection error");
		}
		LOGGER.info("connect end");
	}

	public void disconnect(){
		
		LOGGER.info("disconnect start");
		mqObj.ServiceDisconnect();
		if (mqObj.GetCommReturnCode() != 0) {
			LOGGER.warn("Error on disconnect : " + mqObj.GetErrorMessage());
		}
		LOGGER.info("disconnect end");
	}

	public int commit(){
		
		LOGGER.info("commit start");
		mqObj.Commit();
		int retCode = mqObj.GetCommReturnCode();
		if(mqObj.GetCommReturnCode() != 0) {
			LOGGER.warn("Error on commit : " + mqObj.GetErrorMessage());
		}
		LOGGER.info("commit end");
		return retCode;
	}

	public int rollback(){
		
		LOGGER.info("rollback start");
		mqObj.Backout();
		int retCode = mqObj.GetCommReturnCode();
		if(mqObj.GetCommReturnCode() != 0) {
			LOGGER.warn("Error on rollback : " + mqObj.GetErrorMessage());
		}
		LOGGER.info("rollback end");
		return retCode;
	}


	public int postMessage(String msg){
		
		LOGGER.info("postMessage start");
		mqObj.SendMessage(msg, false);
		int retCode = mqObj.GetCommReturnCode();
		if (mqObj.GetCommReturnCode() != 0) {
			LOGGER.warn("Error while putting message " + mqObj.GetErrorMessage());
		}
		LOGGER.info("postMessage end");
		return retCode;
		
	}


	public String putRequestMsg(String msg){
		
		LOGGER.info("putRequestMsg start");
		mqObj.SendMessage(msg, true);
		String retStr = mqObj.ReceiveMessage(true);
		if (mqObj.GetCommReturnCode() != 0) {
			LOGGER.warn("Error in putRequestMsg " + mqObj.GetErrorMessage());
		}
		LOGGER.debug("retStr: " + retStr);
		LOGGER.info("putRequestMsg end");
		return retStr;
	}



	public String getMessage(){
		
		LOGGER.info("getMessage1 start");
		String l_ReturnMessage=null;

		l_ReturnMessage = mqObj.ReceiveMessage(false);
		

		if (mqObj.GetCommReturnCode() == 0) {
		
			if(l_ReturnMessage!=null){
				mqObj.Commit();
			}else{
				mqObj.Backout();
			}

		}else{
			LOGGER.debug("Error while getting message" + mqObj.GetErrorMessage());
			mqObj.Backout();
		}
		LOGGER.info("getMessage1 end");
		return l_ReturnMessage;
	}


	public ArrayList getMessage(String trxnID){
		
		LOGGER.info("getMessage2 start");
		ArrayList retArrList=new ArrayList();
		SLMQComm tmpMQObj=null;
		int ctr = 0;
		try{
			tmpMQObj = new SLMQComm(serviceName);
			tmpMQObj.ServiceConnect(serviceName);

			while(tmpMQObj.GetCommReturnCode() == 0){
				String tempMsg = tmpMQObj.ReceiveMessage(false);

				trxnID = trxnID.toLowerCase();
				if(tmpMQObj.GetCommReturnCode() == 0 && tempMsg.indexOf(trxnID)>-1 ){
					tmpMQObj.Backout();
					for(int i=0;i<ctr;i++){
						tmpMQObj.ReceiveMessage(false);
					}
					String msg = mqObj.ReceiveMessage(false);
					mqObj.Commit();
					retArrList.add(msg);
					break;
				}else{
				ctr=ctr+1;
				}
			}
		}catch(SLMQInitException e){
			LOGGER.error(CodeHelper.getStackTrace(e));
		}
		LOGGER.info("getMessage2 end");
		return retArrList;
	}



}
