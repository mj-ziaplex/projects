

package com.abacus;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.ibm.mq.MQC;
import com.ibm.mq.MQEnvironment;
import com.ibm.mq.MQException;
import com.ibm.mq.MQGetMessageOptions;
import com.ibm.mq.MQMessage;
import com.ibm.mq.MQPutMessageOptions;
import com.ibm.mq.MQQueue;
import com.ibm.mq.MQQueueManager;
import com.slocpi.ium.util.CodeHelper;


/**
 * @author Yves
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class ReceiverTest{

	/*
	 Logger LOGGER = LoggerFactory.getLogger(ReceiverTest.class.getName());
	private String qm = "";
	private String qreq = "";
	private String qrsp = "";
	private MQQueueManager qMgr = null;
	private String qmsg = "";
	private static String resOK = "JavaResult:OK ";
	private static String resNOTOK = "JavaResult:NOTOK "; 
	private boolean msgAvailable = true;
	private int waitInterval;
	
	public ReceiverTest(String file){
		
		LOGGER.info("ReceiverTest start");
		try{
			ClassLoader loader = ReceiverSLMQ.class.getClassLoader();
			InputStream in = loader.getResourceAsStream(file);
			Properties prop = new Properties();
			prop.load(in);		

			MQEnvironment.hostname = prop.getProperty("hostname");
			MQEnvironment.channel = prop.getProperty("channel");
			MQEnvironment.port = Integer.parseInt(prop.getProperty("port"));
			MQEnvironment.userID = prop.getProperty("userID");
			MQEnvironment.password = prop.getProperty("password");
			qm = prop.getProperty("queueManagerName");
			qreq = prop.getProperty("putQueue");
			qrsp = prop.getProperty("getQueue");	
			waitInterval = Integer.parseInt(prop.getProperty("waitInterval"));	
						
			qMgr = new MQQueueManager(qm);					
		}catch(IOException e){
			LOGGER.error("Error in Abacus constructor-->" + CodeHelper.getStackTrace(e));
		}catch (MQException ex) {
			LOGGER.error("MQ exception: CC = " + ex.completionCode + " RC = " + ex.reasonCode
					+ CodeHelper.getStackTrace(ex) );
		}
		LOGGER.info("ReceiverTest end");
	}


	public void start() {
		
		LOGGER.info("Start method started");
		try {
			
			qMgr = new MQQueueManager(qm);
			int openGetOptions = MQC.MQOO_INPUT_SHARED + MQC.MQOO_FAIL_IF_QUIESCING;
			MQQueue myGetQ = qMgr.accessQueue(qreq, openGetOptions, null, null, null);
			
			MQQueue myPutQ = null;
         	
			MQGetMessageOptions gmo = new MQGetMessageOptions(); 
			gmo.options = MQC.MQGMO_WAIT + MQC.MQGMO_CONVERT + MQC.MQGMO_SYNCPOINT + MQC.MQGMO_FAIL_IF_QUIESCING;
			gmo.waitInterval = waitInterval;
			MQPutMessageOptions pmo = new MQPutMessageOptions();
			pmo.options = MQC.MQPMO_NO_SYNCPOINT + MQC.MQPMO_FAIL_IF_QUIESCING;
      		
			   MQMessage myGetMsg = new MQMessage();
			   myGetMsg.clearMessage();
			   myGetMsg.format = MQC.MQFMT_STRING;
			   myGetMsg.messageType = MQC.MQMT_REQUEST;
			   myGetMsg.correlationId = MQC.MQCI_NONE;
			   myGetMsg.messageId     = MQC.MQMI_NONE;
			   myGetQ.get(myGetMsg, gmo);
			   String getMsg = myGetMsg.readString(myGetMsg.getMessageLength());

			   qrsp = myGetMsg.replyToQueueName;
			   
			   if(getMsg.indexOf("Q")!=-1 && getMsg.indexOf("Q")<2){
					qMgr.commit();
					  				
				   String transID = getMsg.substring(1, 9).trim();
				   String strXmlMsg = "";
				   
				   AbacusDAO dao = new AbacusDAO("Abacus.properties");
				   if(transID.equalsIgnoreCase("QPQI")){
						strXmlMsg = dao.getPolQueueInfo(getMsg.substring(9));
				   }else if(transID.equalsIgnoreCase("QCKO")){
						strXmlMsg = dao.getKOInfo(getMsg.substring(9));
				   }else if(transID.equalsIgnoreCase("QRRI")){
						strXmlMsg = dao.retrieveReqInfo(getMsg.substring(9));
				   }
				   else if(transID.equalsIgnoreCase("QPRP")){
						strXmlMsg = dao.getPolCovInfo(getMsg.substring(9));
				   }else if(transID.equalsIgnoreCase("QCRI")){
					 strXmlMsg = dao.updateClientInfo(getMsg.substring(9));
				   }else if(transID.equalsIgnoreCase("QPAS")){
					 strXmlMsg = dao.getPolAutoSettleInfo(getMsg.substring(9));
				   }else if(transID.equalsIgnoreCase("QRQM")){
					 strXmlMsg = dao.maintainReqInfo(getMsg.substring(9));
				   }
		
				   int openPutOptions = MQC.MQOO_OUTPUT + MQC.MQOO_FAIL_IF_QUIESCING;
				   myPutQ = qMgr.accessQueue(qrsp, openPutOptions, null, null, null);
				   MQMessage myPutMsg = new MQMessage();
				   myPutMsg.clearMessage();
				   myPutMsg.format = MQC.MQFMT_STRING;
				   myPutMsg.messageType = MQC.MQMT_REPLY;         
				   myPutMsg.replyToQueueName = qrsp;
				   myPutMsg.replyToQueueManagerName = qm;		  	   
				   myPutMsg.correlationId = myGetMsg.messageId; // for sync_putget
				   myPutMsg.writeString(strXmlMsg);
				   myPutQ = qMgr.accessQueue(qrsp, openPutOptions, null, null, null);
				   myPutQ.put(myPutMsg, pmo);
				   myPutQ.close();
					
			   }else{
				qMgr.backout();
			   }
			myGetQ.close();
		
			qMgr.disconnect();
		}
		catch(IOException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			System.exit(0);
		}
		catch(MQException mqe) {
			LOGGER.error(CodeHelper.getStackTrace(mqe));
			if(mqe.reasonCode == MQException.MQRC_NO_MSG_AVAILABLE) { 
				msgAvailable = false;
				System.exit(0);
			} else {
				msgAvailable = false;
				System.exit(0);
			}	
		}
		LOGGER.info("Start method ended");
	}
	
*/
	
			
}
