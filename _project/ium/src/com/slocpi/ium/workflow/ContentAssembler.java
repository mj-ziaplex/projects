/**
 * ContentAssembler.java				Copyright (c) 2004 Pointwest Technologies Corp.
 * TODO DOCUMENT ME!
 *
 * @author Barry Yu	 @date Jan 13, 2004
 */
package com.slocpi.ium.workflow;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.ResourceBundle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.regexp.RE;
import org.apache.regexp.RECompiler;

import com.slocpi.ium.data.AssessmentRequestData;
import com.slocpi.ium.data.ClientData;
import com.slocpi.ium.data.ExaminerData;
import com.slocpi.ium.data.LaboratoryData;
import com.slocpi.ium.data.MedicalRecordData;
import com.slocpi.ium.data.PolicyRequirementsData;
import com.slocpi.ium.data.RequirementData;
import com.slocpi.ium.data.RequirementFormData;
import com.slocpi.ium.data.UserProfileData;
import com.slocpi.ium.data.dao.AssessmentRequestDAO;
import com.slocpi.ium.data.dao.ClientDAO;
import com.slocpi.ium.data.dao.RequirementDAO;
import com.slocpi.ium.data.dao.WorkflowDAO;
import com.slocpi.ium.data.util.DataSourceProxy;
import com.slocpi.ium.underwriter.AssessmentRequest;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.DateHelper;
import com.slocpi.ium.util.IUMConstants;
import com.slocpi.ium.util.StringHelper;

/**
 * TODO DOCUMENT ME!
 *
 * @author Barry Yu		Copyright (c) 2004 Pointwest Technologies Corp.
 * @date Jan 13, 2004
 */
public class ContentAssembler {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ContentAssembler.class);

	/**
	 * @param string
	 * @param assessmentData
	 * @param wfi
	 */
	public String assembleARContent(
		String template,
		AssessmentRequestData assessmentData,
		WorkflowItem wfi)
		throws SQLException {
		
		LOGGER.info("assembleARContent start");
		
		RECompiler reComp = new RECompiler();
		RE re = new RE();
		re.setProgram(reComp.compile(IUMConstants.REFERENCE_NUM));
		if (re.match(template)) {
			template = re.subst(template, assessmentData.getReferenceNumber() + assessmentData.getPolicySuffix());
		}
		
		re.setProgram(reComp.compile(IUMConstants.CLIENT_NUM));
		if (re.match(template)) {
			template =
				re.subst(template, assessmentData.getInsured().getClientId());
		}
		re.setProgram(reComp.compile(IUMConstants.OWNER_NAME));
		if (re.match(template)) {
			StringBuffer ownerName = new StringBuffer();
			String firstName = assessmentData.getOwner().getGivenName();
			String lastName = assessmentData.getOwner().getLastName();
			if (firstName != null && !"".equals(firstName)) {
				ownerName.append(firstName + " ");
			}
			if (lastName != null && !"".equals(lastName)) {
				ownerName.append(lastName);
			}
			template = re.subst(template, ownerName.toString());
		}
		re.setProgram(reComp.compile(IUMConstants.CLIENT_NAME));
		if (re.match(template)) {
			StringBuffer clientName = new StringBuffer();
			String firstName = assessmentData.getInsured().getGivenName();
			String lastName = assessmentData.getInsured().getLastName();
			if (firstName != null && !"".equals(firstName)) {
				clientName.append(firstName + " ");
			}
			if (lastName != null && !"".equals(lastName)) {
				clientName.append(lastName);
			}
			template = re.subst(template, clientName.toString());
		}
		re.setProgram(reComp.compile(IUMConstants.LOB));
		String determiner = "";
		if (re.match(template)) {
			String LOB = null;
			if (IUMConstants.LOB_INDIVIDUAL_LIFE.equals(wfi.getLOB())) {
				LOB = "Individual Life";
				determiner = "an";
			} else if (IUMConstants.LOB_GROUP_LIFE.equals(wfi.getLOB())) {
				LOB = "Group Life";
				determiner = "a";
			} else if (IUMConstants.LOB_PRE_NEED.equals(wfi.getLOB())) {
				LOB = "Pre-need";
				determiner = "a";
			}
			template = re.subst(template, LOB);
		}

		re.setProgram(reComp.compile("a/an"));
		if (re.match(template)) {
			template = re.subst(template, determiner);
		}

		re.setProgram(reComp.compile(IUMConstants.APPLICATION_STATUS));
		if (re.match(template)) {
			template =
				re.subst(template, assessmentData.getStatus().getStatusDesc());
		}
		re.setProgram(reComp.compile(IUMConstants.REASSIGNMENT_DETAILS));
		if (re.match(template)) {
			StringBuffer detail = new StringBuffer();
			String refNum = assessmentData.getReferenceNumber();
			String policySuffix = assessmentData.getPolicySuffix();
			String firstName = assessmentData.getUnderwriter().getFirstName();
			String lastName = assessmentData.getUnderwriter().getLastName();
			if (refNum != null && !"".equals(refNum)) {
				detail.append(refNum);
				detail.append(policySuffix);
			}
			detail.append(" to ");
			if (firstName != null && !"".equals(firstName)) {
				detail.append(firstName + " ");
			}
	
			if (lastName != null && !"".equals(lastName)) {
				detail.append(lastName);
			}
			template = re.subst(template, String.valueOf(detail));
		}
		re.setProgram(reComp.compile(IUMConstants.USD_SPRVSOR_MNGR));
		if (re.match(template)) {
			String senderID = wfi.getSender();
			StringBuffer senderName = new StringBuffer();
			if (senderID != null && !"".equals(senderID)) {
				WorkflowDAO wfDAO = new WorkflowDAO();
				UserProfileData sender;
				try {
					sender = wfDAO.getSender(senderID);

					String firstName = sender.getFirstName();
					String lastName = sender.getLastName();
					if (firstName != null && !"".equals(firstName)) {
						senderName.append(firstName + " ");
					}
					if (lastName != null && !"".equals(lastName)) {
						senderName.append(lastName);
					}
				} catch (SQLException e) {
					LOGGER.error(CodeHelper.getStackTrace(e));	
					throw new SQLException(e.getMessage());
				} 
			}
			template = re.subst(template, senderName.toString());
		}
		re.setProgram(reComp.compile(IUMConstants.URL));
		if (re.match(template)) {
			ResourceBundle rb =
				ResourceBundle.getBundle(IUMConstants.IUM_CONFIG);
			template =
				re.subst(
					template,
					rb.getString(IUMConstants.IUM_HOME_PAGE)
						+ IUMConstants.URL_LINK_ACTION
						+ "?page=AR&id="
						+ assessmentData.getReferenceNumber());
		}
		
		LOGGER.info("assembleARContent end");
		return template;
	}

	/**
	 * @param string
	 * @param medRecData
	 * @param wfi
	 * @return
	 */
	public String assembleMRContent(
		String template,
		MedicalRecordData medRecData,
		WorkflowItem wfi)
		throws SQLException {
		
		LOGGER.info("assembleMRContent start");
		RECompiler reComp = new RECompiler();
		RE re = new RE();
		re.setProgram(reComp.compile(IUMConstants.EXAM_TYPE));
		String examType =
			IUMConstants.TEST_TYPE_LABORATORY.equals(
				medRecData.getLabTestInd())
				? "Laboratory"
				: "Medical";
		if (re.match(template)) {
			template = re.subst(template, examType);
		}

		try {
			ClientDAO clientDAO = new ClientDAO();
			ClientData clientData;

			clientData =
				clientDAO.retrieveClient(medRecData.getClient().getClientId());

			re.setProgram(reComp.compile(IUMConstants.CLIENT_NAME));
			String clientFirstName = "";
			String clientLastName = "";
			if (clientData != null) {
				clientFirstName = clientData.getGivenName();
				clientLastName = clientData.getLastName();
			}

			if (re.match(template)) {
				StringBuffer clientName = new StringBuffer();
				if (clientFirstName != null) {
					clientName.append(clientFirstName + " ");
				}
				if (clientLastName != null) {
					clientName.append(clientLastName);
				}
				template = re.subst(template, clientName.toString());
			}

			re.setProgram(reComp.compile(IUMConstants.LAB_OR_EXAMINER));
			String labOrExaminer =
				IUMConstants.TEST_TYPE_LABORATORY.equals(
					medRecData.getLabTestInd())
					? "Laboratory"
					: "Examiner";
			if (re.match(template)) {
				template = re.subst(template, labOrExaminer);
			}

			re.setProgram(reComp.compile(IUMConstants.LAB_EXAM_TYPE));
			if (re.match(template)) {
				String labExamType = medRecData.getTest().getTestDesc();
				template = re.subst(template, labExamType);
			}

			re.setProgram(reComp.compile(IUMConstants.LAB_OR_EXAMINER_NAME));
			if (re.match(template)) {
				String name = "";
				if (IUMConstants
					.TEST_TYPE_LABORATORY
					.equals(medRecData.getLabTestInd())) {
					LaboratoryData labData = medRecData.getLaboratory();
					if (labData != null) {
						name =
							labData.getLabName() != null
								? medRecData.getLaboratory().getLabName()
								: "";
					}
				} else {
					ExaminerData examData = medRecData.getExaminer();
					if (examData != null) {
						name =
							examData.getFirstName() != null
								? (medRecData.getExaminer().getFirstName() + " ")
								: "";
						name =
							examData.getLastName() != null
								? medRecData.getExaminer().getLastName()
								: "";
					}
				}
				template = re.subst(template, name);
			}

			Date appointment = medRecData.getAppointmentDateTime();
			re.setProgram(reComp.compile(IUMConstants.APPOINTMENT_DATE));
			if (re.match(template)) {
				template =
					re.subst(
						template,
						DateHelper.format(appointment, "MMMMM dd, yyyy EEEEE"));
			}

			re.setProgram(reComp.compile(IUMConstants.APPOINTMENT_TIME));
			if (re.match(template)) {
				template =
					re.subst(template, DateHelper.format(appointment, "hha"));
			}
			re.setProgram(reComp.compile(IUMConstants.URL));
			if (re.match(template)) {
				ResourceBundle rb =
					ResourceBundle.getBundle(IUMConstants.IUM_CONFIG);
				template =
					re.subst(
						template,
						rb.getString(IUMConstants.IUM_HOME_PAGE)
							+ IUMConstants.URL_LINK_ACTION
							+ "?page=M&id="
							+ medRecData.getMedicalRecordId());
			}
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));	
			throw new SQLException(e.getMessage());
		} 
		
		LOGGER.info("assembleMRContent end");
		return template;
	}
	/**
	 * @param string
	 * @param polReqData
	 * @param wfi
	 * @return
	 */
	public String assemblePRContent(String template,
		PolicyRequirementsData polReqData,
		WorkflowItem wfi)
		throws SQLException {

		LOGGER.info("assemblePRContent start 2");
		long statusId=0;
		if(null!=new Long(polReqData.getStatus().getStatusId()) ){
			statusId=polReqData.getStatus().getStatusId();
		}else {
			LOGGER.warn("assemblePRContent statusId is null");
		}
		
		if (polReqData.getRequirementCode().equals(IUMConstants.REQUIREMENT_CODE_CWA)
			|| statusId == IUMConstants.STATUS_REQ_CANCELLED) {
			template = "";
		} else {
			
			try {
				String deadlineTag = "";
				RequirementDAO requirementDAO =
					new RequirementDAO();
				RequirementData requirement;

				requirement =
					requirementDAO.retrieveRequirement(
						polReqData.getRequirementCode());

				RequirementFormData requirementForm =
					requirementDAO.retrieveRequirementForm(
						requirement.getFormID());
								
				AssessmentRequestDAO requestDAO = new AssessmentRequestDAO();
				String polSuffix = requestDAO.retrievePolicySuffix(polReqData.getReferenceNumber());

				RECompiler reComp = new RECompiler();
				RE re = new RE();
				re.setProgram(reComp.compile(IUMConstants.REFERENCE_NUM));
				if (re.match(template)) {
					template =
						re.subst(template, polReqData.getReferenceNumber() + polSuffix);
				}
				re.setProgram(reComp.compile(IUMConstants.OWNER_NAME));
				if (re.match(template)) {
					AssessmentRequestDAO arDao = new AssessmentRequestDAO();
					AssessmentRequestData arData = arDao.retrieveRequestDetail(polReqData.getReferenceNumber());
					StringBuffer ownerName = new StringBuffer();
					String firstName = arData.getOwner().getGivenName();
					String lastName = arData.getOwner().getLastName();
					if (firstName != null && !"".equals(firstName)) {
						ownerName.append(firstName + " ");
					}
					if (lastName != null && !"".equals(lastName)) {
						ownerName.append(lastName);
					}
					template = re.subst(template, ownerName.toString());
				}

				re.setProgram(reComp.compile(IUMConstants.CLIENT_NAME));
				if (re.match(template)) {
					ClientDAO clientDAO = new ClientDAO();
					ClientData clientData =
						clientDAO.retrieveClient(polReqData.getClientId());
					if (clientData != null) {
						String firstName = clientData.getGivenName();
						String lastName = clientData.getLastName();
						StringBuffer clientName = new StringBuffer();
						if (firstName != null && !"".equals(firstName)) {
							clientName.append(firstName + " ");
						}
						if (lastName != null && !"".equals(lastName)) {
							clientName.append(lastName);
						}
						template = re.subst(template, clientName.toString());
					}
				}
				
				
				re.setProgram(reComp.compile(IUMConstants.REQUIREMENT_DETAILS));
				if (re.match(template)) {
					String formName = "";
					StringBuffer details = new StringBuffer();
					if (requirementForm.getFormName() != null && !"".equals(requirementForm.getFormName())) {
						formName = requirementForm.getFormName();
					} else {
						formName = requirement.getReqtDesc();
					}

					details.append(formName);
					details.append("\n");

					String comments = polReqData.getComments();
					
					if (comments != null && !"".equals(comments)) {
						details.append("   ");
						details.append(comments);
						details.append("\n");
					}
					String deadLine =
						DateHelper.format(
							polReqData.getFollowUpDate(),
							"MMMMM dd, yyyy");
					if (deadLine != null && !"".equals(deadLine)) {
						details.append("Deadline: ");
						details.append(deadLine);
						deadlineTag = ("Deadline: " + deadLine + "\n\n");
						
					}
					details.append("\n\n");

					template = re.subst(template, details.toString());
				}

				
				re.setProgram(reComp.compile(IUMConstants.REQUIREMENT));
				if (re.match(template)) {
					String formName = requirementForm.getFormName();
					StringBuffer details = new StringBuffer();
					if (formName != null && !"".equals(formName)) {
						details.append("- ");
						details.append(formName);
					} else {
						details.append(requirement.getReqtDesc());
					}
					template = re.subst(template, details.toString());
				}

				
				re.setProgram(reComp.compile(IUMConstants.PDF_ATTACHMENT));
				if (re.match(template)) {
					String templateName = requirementForm.getTemplateName();
					String attachment = "";
					if (templateName != null) {
						attachment = templateName;
					}
					
					template = re.subst(template, "");
				}

				re.setProgram(reComp.compile(IUMConstants.UNDERWRITER_NAME));
				if (re.match(template)) {
					String senderID = wfi.getSender();
					StringBuffer senderName = new StringBuffer();
					if (senderID != null && !"".equals(senderID)) {
						WorkflowDAO wfDAO = new WorkflowDAO();
						UserProfileData sender = wfDAO.getSender(senderID);
						String firstName = sender.getFirstName();
						String lastName = sender.getLastName();
						if (firstName != null && !"".equals(firstName)) {
							senderName.append(firstName + " ");
						}
						if (lastName != null && !"".equals(lastName)) {
							senderName.append(lastName);
						}
					}
					template = re.subst(template, senderName.toString());
				}

				re.setProgram(reComp.compile(IUMConstants.REQUIREMENT_DESC));
				if (re.match(template)) {
					String requirementDesc = polReqData.getReqDesc();
					if (requirementDesc != null
						&& !"".equals(requirementDesc)) {
						template = re.subst(template, requirementDesc);
					}
				}

				re.setProgram(reComp.compile(IUMConstants.URL));
				if (re.match(template)) {
					ResourceBundle rb =
						ResourceBundle.getBundle(IUMConstants.IUM_CONFIG);
					template =
						re.subst(
							template,
							rb.getString(IUMConstants.IUM_HOME_PAGE));
				}

				re.setProgram(reComp.compile(IUMConstants.COMMENTS));
				if (re.match(template)) {
					String comments = polReqData.getComments();
					if (comments != null && !"".equals(comments)) {
						template = re.subst(template, comments);
					}
				}
				
				re.setProgram(reComp.compile(IUMConstants.SECTION_DESCRIPTION));
				if (re.match(template)) {
					String senderID = wfi.getSender();
					StringBuffer senderName = new StringBuffer();
					if (senderID != null && !"".equals(senderID)) {
						WorkflowDAO wfDAO = new WorkflowDAO();
						UserProfileData sender = wfDAO.getSender(senderID);
						String sectionDesc = sender.getSectionDesc();
				if (sectionDesc != null && !"".equals(sectionDesc)) {
					senderName.append(sectionDesc);
				}
			}
			template = re.subst(template, senderName.toString());
		}

				try {
					re.setProgram(reComp.compile(IUMConstants.DEADLINE));
				} catch (Exception e1) {
					LOGGER.error(CodeHelper.getStackTrace(e1));	
				}
				if (re.match(template)) {
					try {
						template = re.subst(template, deadlineTag);
					} catch (Exception e) {
						LOGGER.error(CodeHelper.getStackTrace(e));	
					}
				}
				
				AssessmentRequestData data = requestDAO.retrieveRequestDetail(polReqData.getReferenceNumber());
				UserProfileData agent = data.getAgent();
				template = TemplateHelper.substituteAgentId(agent, template);
				template = TemplateHelper.substituteAgentName(agent, template);

			} catch (Exception e) {
				LOGGER.error(CodeHelper.getStackTrace(e));	
				throw new SQLException(e.getMessage());
			} 
		}
		
		LOGGER.info("assemblePRContent end 1");
		return template;
	}
	
	

	public String assemblePRContent(String template,
		ArrayList listOfRequirements,
		WorkflowItem wfi)
		throws SQLException {
		
		LOGGER.info("assemblePRContent start 2");
		try {
			
			
			StringBuffer details = new StringBuffer();
			String deadlineTag = "";
			String commentsTag = "";
			StringBuffer requirementName = new StringBuffer();

			String referenceNumber = null;
			String policySuffix = "";
			String clientId = null;
			ArrayList partialReqDetailsList = new ArrayList();
			AssessmentRequestDAO requestDAO = new AssessmentRequestDAO();
		
			for (int i = 0; i < listOfRequirements.size(); i++) {
				
				PolicyRequirementsData polReqData =
					(PolicyRequirementsData) listOfRequirements.get(i);
				
				if (!polReqData.getRequirementCode().equals(IUMConstants.REQUIREMENT_CODE_CWA)
  					 && polReqData.getStatus().getStatusId() != IUMConstants.STATUS_REQ_CANCELLED) {
					String reqDetailArr [] = new String [3];
					if (referenceNumber == null) {
						referenceNumber = polReqData.getReferenceNumber();
						
						if (StringHelper.isEmpty(policySuffix)){
							policySuffix = requestDAO.retrievePolicySuffix(referenceNumber); 
						}
					}

					if (clientId == null) {
						clientId = polReqData.getClientId();
					}

					RequirementDAO requirementDAO =
						new RequirementDAO();
					RequirementData requirement;

					requirement =
						requirementDAO.retrieveRequirement(
							polReqData.getRequirementCode());

					RequirementFormData requirementForm =
						requirementDAO.retrieveRequirementForm(
							requirement.getFormID());

					String formName = "";
					if (requirementForm.getFormName() != null && !"".equals(requirementForm.getFormName())) {
						formName = requirementForm.getFormName();
					} else {
						formName = requirement.getReqtDesc();
					}
					details.append(formName);
					details.append("\n");

					requirementName.append(formName);
					requirementName.append("\n");

					String comments = polReqData.getComments();
					if (comments != null && !"".equals(comments)) {
						details.append("   ");
						details.append(comments);
						details.append("\n");
						commentsTag = (comments + "\n\n");
					}
					String deadLine =
						DateHelper.format(
							polReqData.getFollowUpDate(),
							"MMMMM dd, yyyy");
					if (deadLine != null && !"".equals(deadLine)) {
						details.append("Deadline: ");
						details.append(deadLine);
						deadlineTag = ("Deadline: " + deadLine + "\n\n");
					}
					details.append("\n\n");
					
					reqDetailArr[0] = formName;
					reqDetailArr[1] = comments;
					reqDetailArr[2] = deadLine;
					partialReqDetailsList.add(reqDetailArr);
				}
			}
			
			
			RECompiler reComp = new RECompiler();
			RE re = new RE();

			re.setProgram(reComp.compile(IUMConstants.REQUIREMENT_DETAILS));
			if (re.match(template)) {
				String reqDetails = "";
				reqDetails = formatRequirementDetails(partialReqDetailsList);
				template = re.subst(template, reqDetails);
			}

			re.setProgram(reComp.compile(IUMConstants.REQUIREMENT));
			if (re.match(template)) {
				template = re.subst(template, requirementName.toString());
			}

			re.setProgram(reComp.compile(IUMConstants.DEADLINE));
			if (re.match(template)) {
				template = re.subst(template, deadlineTag);
			}

			re.setProgram(reComp.compile(IUMConstants.REFERENCE_NUM));
			if (re.match(template)) {
				template = re.subst(template, referenceNumber + policySuffix);
			}

			re.setProgram(reComp.compile(IUMConstants.CLIENT_NAME));
			if (re.match(template)) {
				ClientDAO clientDAO = new ClientDAO();
				ClientData clientData = clientDAO.retrieveClient(clientId);
				if (clientData != null) {
					String firstName = clientData.getGivenName();
					String lastName = clientData.getLastName();
					StringBuffer clientName = new StringBuffer();
					if (firstName != null && !"".equals(firstName)) {
						clientName.append(firstName + " ");
					}
					if (lastName != null && !"".equals(lastName)) {
						clientName.append(lastName);
					}
					template = re.subst(template, clientName.toString());
				}
			}
			
			
			re.setProgram(reComp.compile(IUMConstants.PDF_ATTACHMENT));
			if (re.match(template)) {
				String templateName = "Test template";
				
				String attachment = "";
				if (templateName != null) {
					attachment = templateName;
				}
				
				template = re.subst(template, "");
			}

			re.setProgram(reComp.compile(IUMConstants.UNDERWRITER_NAME));
			if (re.match(template)) {
				String senderID = wfi.getSender();
				StringBuffer senderName = new StringBuffer();
				if (senderID != null && !"".equals(senderID)) {
					WorkflowDAO wfDAO = new WorkflowDAO();
					UserProfileData sender = wfDAO.getSender(senderID);
					String firstName = sender.getFirstName();
					String lastName = sender.getLastName();
					if (firstName != null && !"".equals(firstName)) {
						senderName.append(firstName + " ");
					}
					if (lastName != null && !"".equals(lastName)) {
						senderName.append(lastName);
					}
				}
				template = re.subst(template, senderName.toString());
			}
			
			re.setProgram(reComp.compile(IUMConstants.SECTION_DESCRIPTION));
			if (re.match(template)) {
				String senderID = wfi.getSender();
				StringBuffer senderName = new StringBuffer();
				if (senderID != null && !"".equals(senderID)) {
					WorkflowDAO wfDAO = new WorkflowDAO();
					UserProfileData sender = wfDAO.getSender(senderID);
					String sectionDesc = sender.getSectionDesc();
			if (sectionDesc != null && !"".equals(sectionDesc)) {
				senderName.append(sectionDesc);
			}
		}
		template = re.subst(template, senderName.toString());
	}
			
			re.setProgram(reComp.compile(IUMConstants.COMMENTS));
			if (re.match(template)) {
				if (commentsTag != null && !"".equals(commentsTag)) {
					template = re.subst(template, commentsTag);
				}
			}

			AssessmentRequestData data = requestDAO.retrieveRequestDetail(referenceNumber);
			UserProfileData agent = data.getAgent();                      
			template = TemplateHelper.substituteAgentId(agent, template);
			template = TemplateHelper.substituteAgentName(agent, template);
			
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));	
			throw new SQLException(e.getMessage());
		} 
		
		LOGGER.info("assemblePRContent end 2");
		return template;
	}
	
	private String formatRequirementDetails(ArrayList listOfReqDetailsArr){
		
		LOGGER.info("formatRequirementDetails start");
		StringBuffer buff = new StringBuffer();
		if (listOfReqDetailsArr != null && listOfReqDetailsArr.size() > 0){
			String tempDeadline = "";
			ArrayList dataHolder = null;
			HashMap masterMap = new HashMap();
			int index = 0;
			for (Iterator it = listOfReqDetailsArr.iterator(); it.hasNext();){
				String [] tempArr = (String []) it.next();
				String deadline = tempArr[2];
				if (StringHelper.isEmpty(tempDeadline) && index==0){
					dataHolder = new ArrayList();
					dataHolder.add(tempArr);
					masterMap.put(tempArr[2], dataHolder);
				} else if (StringHelper.isEqual(tempDeadline, deadline)){
					if (masterMap.containsKey(tempDeadline)){
						ArrayList tempList = (ArrayList) masterMap.get(tempDeadline);
						tempList.add(tempArr);
						masterMap.put(tempDeadline, tempList);
					}
				} else if (StringHelper.isNotEqual(tempDeadline, deadline)){
					if (masterMap.containsKey(deadline)){
						ArrayList tempList = (ArrayList) masterMap.get(deadline);
						tempList.add(tempArr);
						masterMap.put(deadline, tempList);
					} else {
						dataHolder = new ArrayList();
						dataHolder.add(tempArr);
						masterMap.put(deadline, dataHolder);
					}
				}
				tempDeadline = tempArr[2];
				index++;
			}
			if (masterMap != null && !masterMap.isEmpty()){
				for (Iterator it = masterMap.keySet().iterator(); it.hasNext();){
					ArrayList dataList = (ArrayList) masterMap.get(it.next());
					String arr[] = new String [3];
					boolean haveRequirement = true;
					for (int i=0; i<dataList.size();i++){
						arr = (String []) dataList.get(i);
						if (StringHelper.isEmpty(arr[0])){
							buff.append("");
							haveRequirement = false;
						} else {
							buff.append(arr[0]);
							buff.append("\n");
						}
						
						if (StringHelper.isEmpty(arr[1])){
							buff.append("");
						} else {
							buff.append("     " + arr[1]);
							buff.append("\n\n");
						}
					}
					if (haveRequirement){
						buff.append("\n");
						buff.append("Deadline: " + arr[2]);
						buff.append("\n\n");						
					}
				}
			}
		}
		
		LOGGER.info("formatRequirementDetails end");
		return buff.toString();
	}
	
	private static class TemplateHelper {
		
		private static final Logger LOGGER = LoggerFactory.getLogger(TemplateHelper.class);
		private static RE re = new RE();
		private static RECompiler compiler = new RECompiler(); 
		
		public static String substituteAgentId(UserProfileData agent, String template) {
			
			LOGGER.info("substituteAgentId start");
			
			re.setProgram(compiler.compile(IUMConstants.AGENT_ID));
			if (re.match(template)) {
				template = re.subst(template, agent.getUserId());
			}
			
			LOGGER.info("substituteAgentId end");
			return template;
		}
		
		public static String substituteAgentName(UserProfileData agent, String template) {
			
			LOGGER.info("substituteAgentName start");
			
			re.setProgram(compiler.compile(IUMConstants.AGENT_NAME));
			if (re.match(template)) {
				
				String firstName = agent.getFirstName();
				String lastName = agent.getLastName();

				String space = " ";
				StringBuffer agentName = new StringBuffer();
				agentName.append(firstName)
				.append(space)
				.append(lastName);

				template = re.subst(template, agentName.toString());
			}
			
			LOGGER.info("substituteAgentName end");
			return template;
		}
	}

		
	
	
}
