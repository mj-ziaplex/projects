/**
 * Workflow.java				Copyright (c) 2004 Pointwest Technologies Corp.
 * TODO DOCUMENT ME!
 * 
 * @author Barry Yu	 @date Jan 5, 2004
 */
package com.slocpi.ium.workflow;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.slocpi.ium.data.StatusData;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.IUMConstants;

/**
 * This class is the entry point for the workflow process.
 * 
 * @author Barry Yu Copyright (c) 2004 Pointwest Technologies Corp.
 * @date Jan 5, 2004
 * ***************************************************
 * @author Shellz - Sept 26, 2014
 * @version 1.1
 * TODO -Code clean up of commented codes and proper logging
 */
public class Workflow extends WFEngine {

	private static final Logger LOGGER = LoggerFactory.getLogger(Workflow.class);
	/**
	 * This method evaluates an object's state in the workflow process.
	 * 
	 * @param wfi
	 * @throws Exception
	 */
	public Object tmpGetObject(final WorkflowItem wfi) {
		
		LOGGER.info("tmpGetObject start");
		Object reqObj = null;
		try {
			reqObj = getObject(wfi);
		} catch (Exception e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
		}
		LOGGER.info("tmpGetObject end");
		return reqObj;
	}

	public String[] tmpGetNotificationDetails(final WorkflowItem wfi) {
		
		LOGGER.info("tmpGetNotificationDetails start");
		String notificationInfo[] = null;
		try {
			notificationInfo = getNotificationDetails(wfi);
		} catch (Exception e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
		}
		LOGGER.info("tmpGetNotificationDetails start");
		return notificationInfo;
	}

	public void processState(final WorkflowItem wfi) throws Exception {
		
		LOGGER.info("processState start 1");

				Object reqObj;
				try {
					
					reqObj = getObject(wfi);
										
					Class objClass = reqObj.getClass();
					if (wfi.getSpecialCase() == null) {
						StatusData statusData = (StatusData) objClass.getDeclaredMethod(IUMConstants.STATUS, null).invoke(reqObj, null);
						String currStatus = String.valueOf(statusData.getStatusId());
						wfi.setCurrentStatus(currStatus);
					} else if (wfi.getSpecialCase().equalsIgnoreCase(IUMConstants.RECEIVED_REQUIREMENT)) {
						wfi.setCurrentStatus(String.valueOf(IUMConstants.STATUS_RECEIVED_IN_SITE));
					} else {
						wfi.setCurrentStatus(String.valueOf(IUMConstants.STATUS_ORDERED));
					}
					
					LOGGER.debug("curr stat-->" + wfi.getCurrentStatus());
					LOGGER.debug("prev stat-->" + wfi.getPreviousStatus());
					
					String notificationInfo[] = getNotificationDetails(wfi);
					
					if (notificationInfo != null
							&& notificationInfo[1] != null
							&& notificationInfo[0] != null
							&& notificationInfo[2] != null
							&& IUMConstants.YES.equalsIgnoreCase(notificationInfo[0])) {
						
						NotificationBuilder nb = new NotificationBuilder();
						nb.sendNotification(reqObj, wfi, notificationInfo[1], notificationInfo[2]);
					} else {
						LOGGER.debug("There are no Notification Info Retrieved");
					}
				} catch (Exception e) {
					LOGGER.error(CodeHelper.getStackTrace(e));
				}			
				LOGGER.info("processState end 1");
	}

	
	public void processState(
			final WorkflowItem wfi,
			boolean doThread) throws Exception {
		
		LOGGER.info("processState start 2");
		if (!doThread) {
			
			Object reqObj;
			try {
				
				reqObj = getObject(wfi);
				Class objClass = reqObj.getClass();
				if (wfi.getSpecialCase() == null) {
					
					StatusData statusData = (StatusData) objClass.getDeclaredMethod(IUMConstants.STATUS, null).invoke(reqObj, null);
					String currStatus = String.valueOf(statusData.getStatusId());
					wfi.setCurrentStatus(currStatus);
				} else {
					wfi.setCurrentStatus(String.valueOf(IUMConstants.STATUS_ORDERED));
				}
				LOGGER.debug("curr stat-->" + wfi.getCurrentStatus());
				LOGGER.debug("prev stat-->" + wfi.getPreviousStatus());
				
				String notificationInfo[] = getNotificationDetails(wfi);
				
				LOGGER.debug("noti info-->" + notificationInfo);
				LOGGER.debug("wfi type-->" + wfi.getType());
				
				// Check notification info
				if (notificationInfo != null
						&& notificationInfo[1] != null
						&& notificationInfo[0] != null
						&& notificationInfo[2] != null
						&& IUMConstants.YES.equalsIgnoreCase(notificationInfo[0])) {
					
					NotificationBuilder nb = new NotificationBuilder();
					nb.sendNotification(reqObj, wfi, notificationInfo[1], notificationInfo[2]);
				} else {
					LOGGER.debug("There are no Notification Info Retrieved");
				}
			} catch (Exception e) {
				LOGGER.error(CodeHelper.getStackTrace(e));
			}
		} else {
			processState(wfi);
		}
	}
}
