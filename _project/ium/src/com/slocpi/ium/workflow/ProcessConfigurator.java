/** 
 * ProcessConfigurator.java				Copyright (c) 2004 Pointwest Technologies Corp.
 * TODO DOCUMENT ME!
 * 
 * @author Barry Yu	 @date Mar 10, 2004
 */
package com.slocpi.ium.workflow;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.TreeMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.slocpi.ium.IUMException;
import com.slocpi.ium.data.ProcessConfigData;
import com.slocpi.ium.data.dao.ProcessConfigDAO;
import com.slocpi.ium.data.dao.ProcessConfigRoleDAO;
import com.slocpi.ium.data.dao.StatusCHDao;
import com.slocpi.ium.data.dao.WorkflowDAO;
import com.slocpi.ium.data.util.DataSourceProxy;
import com.slocpi.ium.util.CodeHelper;

/**
 * TODO DOCUMENT ME!
 * 
 * @author Barry Yu		Copyright (c) 2004 Pointwest Technologies Corp.
 * @date Mar 10, 2004
 */
public class ProcessConfigurator {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(WFEngine.class);
	
	private Connection conn = null;
	
	public ProcessConfigurator() {		
	}
	
	private Connection getConnection() {
		
		LOGGER.info("getConnection start");
		if (this.conn == null) {
			this.conn = new DataSourceProxy().getConnection();
		}
		LOGGER.info("getConnection end");
		return this.conn;
	}
	
	private void closeConnection() throws SQLException {
		
		
		this.conn.close();
		this.conn = null;
		
	}
	
	public void updateProcessConfig(ProcessConfigData processConfData) throws IUMException {		
		
		LOGGER.info("updateProcessConfig start");
		Connection transConnection = getConnection();		
		try {
			transConnection.setAutoCommit(false);			
			WorkflowDAO wfDAO = new WorkflowDAO();
			wfDAO.updateProcessConfig(processConfData);
			
			ArrayList roles = processConfData.getRoles();
			if (roles != null) {							
				ProcessConfigRoleDAO roleDAO = new ProcessConfigRoleDAO(transConnection);
				roleDAO.updateRoles(roles,processConfData.getEventId(), processConfData.getLOB());
			}
			transConnection.commit();
		} catch (SQLException e) {
			try {
				transConnection.rollback();
			} catch (SQLException e1) {				
				LOGGER.error(CodeHelper.getStackTrace(e));	
			}
			LOGGER.error(CodeHelper.getStackTrace(e));	
		}		
		finally {		
			try {
				closeConnection();
			} catch (SQLException e1) {				
				LOGGER.error(CodeHelper.getStackTrace(e1));	
			}
		}
		LOGGER.info("updateProcessConfig end");
	}
	
	public void createProcessConfig(ProcessConfigData processConfData) throws IUMException {		
		
		LOGGER.info("createProcessConfig start");
		Connection transConnection = getConnection();
		try {		
			
			transConnection.setAutoCommit(false);
			WorkflowDAO wfDAO = new WorkflowDAO();		
			wfDAO.insertProcessConfig(processConfData);
			ArrayList roles = processConfData.getRoles();
			if (roles != null) {										
				ProcessConfigRoleDAO roleDAO = new ProcessConfigRoleDAO(transConnection);
				roleDAO.insertRoles(roles,processConfData.getLOB());
			}
			transConnection.commit();
		} catch (SQLException e) {
			try {
				transConnection.rollback();
			} catch (SQLException e1) {				
				LOGGER.error(CodeHelper.getStackTrace(e1));
			}
			LOGGER.error(CodeHelper.getStackTrace(e));	
		}
		finally {		
			try {
				closeConnection();
			} catch (SQLException e1) {				
				LOGGER.error(CodeHelper.getStackTrace(e1));	
			}
		}	
		LOGGER.info("createProcessConfig end");
	}
	
	public void addNewStatus(String lob, String status) throws NumberFormatException, SQLException {
		
		LOGGER.info("addNewStatus start");
		try {
			WorkflowDAO wfDAO = new WorkflowDAO();
			wfDAO.insertLOBStatus(lob,status);
		} finally {
			closeConnection();
		}
		LOGGER.info("addNewStatus end");
	}
	
	public ProcessConfigData getProcessConfig(String eventID) throws SQLException {	
		
		LOGGER.info("getProcessConfig start");
		ProcessConfigData configData;
		
		WorkflowDAO wfDAO = new WorkflowDAO();
		configData = wfDAO.getProcessConfig(eventID);
		 		
		LOGGER.info("getProcessConfig end");
		return configData;		
	}
	
	public ArrayList retrieveProcessConfigurations (String objectType, String lob) throws SQLException {
		
		LOGGER.info("retrieveProcessConfigurations start");
		ArrayList configEntries;
		try {
			WorkflowDAO wfDAO = new WorkflowDAO();
			configEntries = wfDAO.retrieveProcessConfig(objectType,lob);
		} finally {
			closeConnection();
		}
		LOGGER.info("retrieveProcessConfigurations end");
		return configEntries;
	}
	
	public Collection getAvailableStatuses(String lob, String objectType) throws SQLException {
		
		LOGGER.info("getAvailableStatuses start");
		Collection availableStatuses;

		StatusCHDao statusDAO = new StatusCHDao();
		availableStatuses = statusDAO.getCodeValue(lob,objectType);
		
		LOGGER.info("getAvailableStatuses end");
		return  availableStatuses;
	}
	
	
	public ArrayList getAssignedRoles(String lob,long eventId) throws SQLException{
		
		LOGGER.info("getAssignedRoles start");
		ArrayList roles;
		try {
			ProcessConfigRoleDAO pcrDAO = new ProcessConfigRoleDAO(getConnection());
			roles = pcrDAO.selectAssignedRoles(lob,eventId);
		} finally {
			closeConnection();
		}
		LOGGER.info("getAssignedRoles end");
		return roles;
	}
	
	
	public ArrayList getAvailableRoles(String lob,long eventId) throws SQLException{
	
		LOGGER.info("getAvailableRoles start");
		ArrayList roles;
		try {
			ProcessConfigRoleDAO pcrDAO = new ProcessConfigRoleDAO(getConnection());
			roles = pcrDAO.selectAvailableRoles(lob,eventId);
		} finally {
			closeConnection();
		}
		LOGGER.info("getAvailableRoles end");
		return roles;
	}
	
	public TreeMap getEvents(String userId) {
		
		LOGGER.info("getEvents start");
		ProcessConfigDAO pcrDAO = new ProcessConfigDAO();
		TreeMap events = null;
		try {
			events = pcrDAO.getEvents(userId);
		} catch (SQLException e) {						
			LOGGER.error(CodeHelper.getStackTrace(e));
		}
			
		LOGGER.info("getEvents end");
		return events;	
	}			
	
	public void updateUserProcessPreferences(ArrayList events, String userId) {
		
		LOGGER.info("updateUserProcessPreferences start");
		
		try {
			
			ProcessConfigDAO pcrDAO = new ProcessConfigDAO();	
			pcrDAO.updateNotificationEvents(events,userId);
			
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
		} 
		LOGGER.info("updateUserProcessPreferences end");
	}
			
}
