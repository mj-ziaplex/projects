package com.slocpi.ium.workflow;

import com.slocpi.ium.interfaces.notification.NotificationMessage;

/**
 * This class encapsulates the notification messages.
 * 
 * @author Barry Yu		Copyright (c) 2004 Pointwest Technologies Corp.
 * @date Jan 7, 2004
 */
public class NotificationMessages {
	private NotificationMessage mailMessage = null;	
	private NotificationMessage smsMessage = null; 
	
	/**
	 * @return
	 */
	public NotificationMessage getMailMessage() {
		return mailMessage;
	}

	/**
	 * @param message
	 */
	public void setMailMessage(NotificationMessage message) {
		mailMessage = message;
	}

	/**
	 * @return
	 */
	public NotificationMessage getSmsMessage() {
		return smsMessage;
	}

	/**
	 * @param message
	 */
	public void setSmsMessage(NotificationMessage message) {
		smsMessage = message;
	}

}
