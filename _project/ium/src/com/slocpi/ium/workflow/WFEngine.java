/**
 * WFEngine.java				Copyright (c) 2004 Pointwest Technologies Corp.
 * TODO DOCUMENT ME!
 * 
 * @author Barry Yu	 @date Jan 6, 2004
 */
package com.slocpi.ium.workflow;

import java.sql.Connection;
import java.sql.SQLException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.slocpi.ium.data.dao.AssessmentRequestDAO;
import com.slocpi.ium.data.dao.MedicalDAO;
import com.slocpi.ium.data.dao.PolicyRequirementDAO;
import com.slocpi.ium.data.dao.WorkflowDAO;
import com.slocpi.ium.data.util.DataSourceProxy;
import com.slocpi.ium.underwriter.PolicyRequirements;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.IUMConstants;


/**
 * This class is the parent class for workflow. Contains common methods used across the workflow process.
 * 
 * @author Barry Yu		Copyright (c) 2004 Pointwest Technologies Corp.
 * @date Jan 6, 2004
 * ***************************************************
 * @author Shellz - Sept 26, 2014
 * @version 1.1
 * TODO -Code clean up of commented codes and proper logging
 */
public class WFEngine {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(WFEngine.class);
	private Connection conn; 
	
	/**
	 * Returns the object (Assesment Request, Medical Record, Policy Requirement) for use by workflow.
	 * @param wfi
	 * @return
	 * @throws Exception
	 */
	protected Object getObject(WorkflowItem wfi) throws Exception {	
		
		LOGGER.info("getObject start");
		
		String type = wfi.getType();
		String id = wfi.getObjectID();
		String specialCase = wfi.getSpecialCase();	
		Object requestedObj = null;
		conn =  new DataSourceProxy().getConnection();
		try{
			
			if (IUMConstants.ASSESSMENT_REQUEST.equals(type)) {
				AssessmentRequestDAO arDAO = new AssessmentRequestDAO();			
				requestedObj = arDAO.retrieveRequestDetail(id);		
			}else if (IUMConstants.POLICY_REQUIREMENTS.equals(type) && specialCase == null) {
				PolicyRequirementDAO reqDAO = new PolicyRequirementDAO();
				requestedObj = reqDAO.retrieveRequirement(Long.parseLong(id));	
			}else if (IUMConstants.MEDICAL_RECORDS.equals(type)) {
				MedicalDAO medDAO = new MedicalDAO(conn);
				requestedObj = medDAO.retrieveMedicalRecord(Long.parseLong(id));	
			} else if (IUMConstants.POLICY_REQUIREMENTS.equals(type) && (specialCase != null && specialCase.equals(IUMConstants.ORDER_REQUIREMENT))){
				PolicyRequirements req = new PolicyRequirements();
				requestedObj = req.retrieveAttachmentHelper(id); 
			}else if (IUMConstants.POLICY_REQUIREMENTS.equals(type) && (specialCase != null && specialCase.equals(IUMConstants.RECEIVED_REQUIREMENT))){
				PolicyRequirementDAO reqDAO = new PolicyRequirementDAO();
				requestedObj = reqDAO.retrieveRequirement(Long.parseLong(id));	
			}else {
				LOGGER.debug("Case : NO CONDITIONS SATISFIED");
			}
		} catch (SQLException e){
			LOGGER.error(CodeHelper.getStackTrace(e));		
		} catch (Exception e){
			LOGGER.error(CodeHelper.getStackTrace(e));
		} finally{
			if (!conn.isClosed())
				conn.close();
		}
		LOGGER.info("getObject end");
		return requestedObj;
	}
	
	/**
	 * Gathers the basic notification details for use by the workflow.
	 * @param wfi
	 * @return
	 * @throws SQLException
	 */
	protected String[] getNotificationDetails(WorkflowItem wfi) throws SQLException {
		
		LOGGER.info("getNotificationDetails start");
		
		String notificationInfo[] = null;
		
		WorkflowDAO wfDAO = new WorkflowDAO();
		try{
			 notificationInfo = wfDAO.getNotificationInfo(wfi);
		}catch (SQLException e){
			LOGGER.error(CodeHelper.getStackTrace(e));		
		}catch (Exception e){
			LOGGER.error(CodeHelper.getStackTrace(e));
		}	
		
		LOGGER.info("getNotificationDetails end");			
		return notificationInfo;
	}

}
