package com.slocpi.ium.workflow;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.ResourceBundle;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

import org.apache.axis.i18n.RB;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import lotus.domino.NotesException;

import com.slocpi.ium.data.AssessmentRequestData;
import com.slocpi.ium.data.MedicalRecordData;
import com.slocpi.ium.data.NotificationTemplateData;
import com.slocpi.ium.data.PolicyRequirementsData;
import com.slocpi.ium.data.UserProfileData;
import com.slocpi.ium.data.dao.AssessmentRequestDAO;
import com.slocpi.ium.data.dao.SMSDAO;
import com.slocpi.ium.data.dao.WorkflowDAO;
import com.slocpi.ium.data.util.DataSourceProxy;
import com.slocpi.ium.interfaces.domino.DominoTranslator;
import com.slocpi.ium.interfaces.notification.Attachment;
import com.slocpi.ium.interfaces.notification.IUMNotificationException;
import com.slocpi.ium.interfaces.notification.NotificationMessage;
import com.slocpi.ium.interfaces.notification.Notifier;
import com.slocpi.ium.underwriter.PolicyRequirements;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.IUMConstants;
import com.slocpi.ium.util.forms.RequirementForm;


/**
 * TODO DOCUMENT ME!
 *
 * @author Barry Yu		Copyright (c) 2004 Pointwest Technologies Corp.
 * @date Jan 6, 2004
 */
public class NotificationBuilder {
	private static final Logger LOGGER = LoggerFactory.getLogger(NotificationBuilder.class);

	public NotificationBuilder() {}

	public void sendNotification(Object obj, WorkflowItem wfi, String notificationID, String eventID) throws Exception {
		LOGGER.info("sendNotification initialize");
		buildMessage(obj, wfi, notificationID, eventID);
	}


	private void buildMessage(
			final Object obj,
			final WorkflowItem wfi,
			final String notificationID,
			String eventID) {
		
		LOGGER.info("buildMessage start");
		String type = wfi.getType();
		String specialCase = wfi.getSpecialCase();
		Notifier notifier = new Notifier();
		

		try {
			if (IUMConstants.ASSESSMENT_REQUEST.equals(type)) {
				AssessmentRequestData assessmentData = (AssessmentRequestData) obj;
				NotificationMessages noticeMessages = buildAssessmentNotice(assessmentData, wfi, notificationID, eventID);
				NotificationMessage mail = noticeMessages.getMailMessage();
				
				if (mail.getRecipients().size() > 0) {
					notifier.sendMessages(noticeMessages);
				} else {
					LOGGER.warn("There are no recipients retrieved: Send Message not executed");
				}
			} else if (IUMConstants.POLICY_REQUIREMENTS.equals(type)
					&& specialCase == null) {
				PolicyRequirementsData polReqData = (PolicyRequirementsData) obj;
				NotificationMessages noticeMessages = buildPolicyRequirementsNotice(polReqData, wfi, notificationID, eventID);
				NotificationMessage mail = noticeMessages.getMailMessage();
				
				if (mail.getRecipients().size() > 0 && mail.getContent().length() > 0) {
					notifier.sendMessages(noticeMessages);
				} else {
					LOGGER.warn("There are no recipients retrieved: Send Message not executed");
				}
			} else if (IUMConstants.MEDICAL_RECORDS.equals(type)) {
				MedicalRecordData medRecData = (MedicalRecordData) obj;
				NotificationMessages noticeMessages = buildMedicalRecNotice(medRecData, wfi, notificationID, eventID);
				NotificationMessage mail = noticeMessages.getMailMessage();
				
				if (mail.getRecipients().size() > 0) {
					notifier.sendMessages(noticeMessages);
				} else {
					LOGGER.warn("There are no recipients retrieved: Send Message not executed");
				}
			} else if (IUMConstants.POLICY_REQUIREMENTS.equals(type)
					&& (specialCase != null && specialCase.equals(IUMConstants.ORDER_REQUIREMENT))) {
				
				ArrayList list = (ArrayList) obj;
				NotificationMessages noticeMessages = buildPolicyRequirementsNotice(list, wfi, notificationID, eventID);
				NotificationMessage mail = noticeMessages.getMailMessage();
				
				if (mail.getRecipients().size() > 0 && mail.getContent().length() > 0) {
					notifier.sendMessages(noticeMessages);
					removeAttachment(wfi.getObjectID());
				} else {
					LOGGER.warn("There are no recipients retrieved: Send Message not executed");
				}
			} else if (IUMConstants.POLICY_REQUIREMENTS.equals(type)
					&& (specialCase != null && specialCase.equals(IUMConstants.RECEIVED_REQUIREMENT))) {
				
				PolicyRequirementsData policyRequirementsData = (PolicyRequirementsData) obj;
				NotificationMessages noticeMessages = buildPolicyRequirementsNotice(policyRequirementsData, wfi, notificationID, eventID);
				NotificationMessage mail = noticeMessages.getMailMessage();

				if (mail.getRecipients().size() > 0 && mail.getContent().length() > 0) {
					notifier.sendMessages(noticeMessages);
					removeAttachment(wfi.getObjectID());
				} else {
					LOGGER.debug("There are no recipients retrieved: Send Message not executed");
				}
			}
		} catch (SQLException e) {
			LOGGER.error("BuildMessage SQLException error.Please see stacktrace.");
			LOGGER.error(CodeHelper.getStackTrace(e));	
		} catch (IUMNotificationException e) {
			LOGGER.error("BuildMessage IUMNotificationException error.Please see stacktrace.");
			LOGGER.error(CodeHelper.getStackTrace(e));	
		} catch (Exception e) {
			LOGGER.error("BuildMessage Exception error. Please see stacktrace.");
			LOGGER.error(CodeHelper.getStackTrace(e));	
		} 

		LOGGER.info("buildMessage end");
	}


	/**
	 * @param medRecData
	 * @param notificationID
	 */
	private NotificationMessages buildMedicalRecNotice(
			final MedicalRecordData medRecData,
			final WorkflowItem wfi,
			final String notificationID,
			final String eventID) throws Exception {
		
		LOGGER.info("buildMedicalRecNotice start");
		NotificationMessage mailMessage = new NotificationMessage();
		WorkflowDAO wfDAO = new WorkflowDAO();
		NotificationTemplateData notificationTempData = wfDAO.getNotificationTemplate(notificationID);
		NotificationMessages notificationMessages = new NotificationMessages();
	
		if (notificationTempData != null) {
			NotificationRecipients notificationRecipients  = null;
			ArrayList consolidatedRecipients = getRecipients(wfi,notificationID,wfi.getLOB(), medRecData.getPolicyMedicalRecordData().getReferenceNumber());
			notificationRecipients  = wfDAO.getNotificationRecipients(consolidatedRecipients,eventID,notificationID, wfi.getLOB());
			
			if (IUMConstants.MED_REQUESTED == medRecData.getStatus().getStatusId()) {
				if (medRecData.isSevenDayMemoInd()) {
					triggerLWS(medRecData,notificationRecipients.getEmails());
				}
			}
			
			if (IUMConstants.MED_EXAM_CONFIRMED == medRecData.getStatus().getStatusId() && medRecData.isRequestForLOAInd()) {
				RequirementForm rf = new RequirementForm();
				Attachment attachmentForm = rf.writeForm(generateLOANotification(medRecData));
				if (attachmentForm != null) {
					mailMessage.setAttachment(attachmentForm);
				}
			}
			
			// Get sender
			UserProfileData sender = new UserProfileData();
			if (wfi.getSender() != null) {
				sender = wfDAO.getSender(wfi.getSender());
			}else {
				sender.setEmailAddr(getDefaultEmail());
			}

			// Set sender and recipient
			mailMessage.setSender(sender.getEmailAddr());
			mailMessage.setRecipients(notificationRecipients.getEmails());
			
			//for debugging
			InternetAddress senderEmailAddress = new InternetAddress(mailMessage.getSender());
			ArrayList addressesTo =mailMessage.getRecipients();
			
			LOGGER.debug("SENDER-->" +  senderEmailAddress);
			LOGGER.debug("RECIPIENTS-->" +  addressesTo);
			
			// Set content assembler
			ContentAssembler ca = new ContentAssembler();
			String content = ca.assembleMRContent(notificationTempData.getEmailNotificationContent(),medRecData,wfi);
			mailMessage.setContent(content);
			String subject = ca.assembleMRContent(notificationTempData.getSubject(),medRecData,wfi);
			mailMessage.setSubject(subject);

			LOGGER.debug("EMAIL SUBJECT-->" +  subject);
			LOGGER.debug("EMAIL CONTENT-->" +  content);
			// Set mail message
			notificationMessages.setMailMessage(mailMessage);
			
			LOGGER.debug("NOTIFY SMS-->" + notificationTempData.isNotifyMobile());
			// Check and set mobile message
			if (notificationTempData.isNotifyMobile()) {
				NotificationMessage smsMessage = new NotificationMessage();
				smsMessage.setSMS(true);
				
				String smsContent = ca.assembleMRContent(notificationTempData.getMobileNotificationContent(),medRecData,wfi);
				smsMessage.setContent(smsContent);
				
				LOGGER.debug("SMS CONTENT-->" +  smsContent);
				
				smsMessage.setRecipients(formatMobileNums(notificationRecipients.getMobiles()));
				smsMessage.setSender(sender.getEmailAddr());
				notificationMessages.setSmsMessage(smsMessage);
			} else {
				LOGGER.warn("notificationTempData.isNotifyMobile equal: FALSE");
			}
		} else {
		  	LOGGER.warn("Notification Template is null");
		}
		
		LOGGER.info("buildMedicalRecNotice end");
		return notificationMessages;
	}


	/**
	 * @param polReqData
	 * @param notificationID
	 */
	private NotificationMessages buildPolicyRequirementsNotice(
			final PolicyRequirementsData polReqData,
			final WorkflowItem wfi,
			final String notificationID,
			final String eventID) throws Exception {
		
		LOGGER.info("buildPolicyRequirementsNotice start");
		WorkflowDAO wfDAO = new WorkflowDAO();
		NotificationTemplateData notificationTempData = wfDAO.getNotificationTemplate(notificationID);
		NotificationMessages notificationMessages = new NotificationMessages();
		
		if (notificationTempData != null) {
			
			ArrayList consolidatedRecipients = getRecipients(wfi,notificationID,wfi.getLOB(), polReqData.getReferenceNumber());
			NotificationRecipients notificationRecipients  = wfDAO.getNotificationRecipients(consolidatedRecipients,eventID,notificationID, wfi.getLOB());
			
			// Get sender
			UserProfileData sender = new UserProfileData();
			NotificationMessage mailMessage = new NotificationMessage();
			
			LOGGER.debug("Event ID : " +  eventID + " : notification ID: " + notificationID);
			// Set sender and recipient
			if(IUMConstants.NOTIFICATION_ID_RECEIPT_REQUIREMENTS.equals(notificationID)) {
				mailMessage.setSender(IUMConstants.NB_GENERIC_EMAIL);
			} else if (wfi.getSender() != null) {
				sender = wfDAO.getSender(wfi.getSender());
			} else {
				sender.setEmailAddr(getDefaultEmail());
			}
			
			mailMessage.setRecipients(notificationRecipients.getEmails());
			
			//for debugging
			InternetAddress senderEmailAddress = new InternetAddress(mailMessage.getSender());
			ArrayList addressesTo =mailMessage.getRecipients();
			
			LOGGER.debug("SENDER-->" +  senderEmailAddress);
			LOGGER.debug("RECIPIENTS-->" +  addressesTo);
			
			// Get content assembler
			ContentAssembler ca = new ContentAssembler();
			String content = ca.assemblePRContent(notificationTempData.getEmailNotificationContent(),polReqData,wfi);
			mailMessage.setContent(content);
			String subject = ca.assemblePRContent(notificationTempData.getSubject(),polReqData,wfi);
			mailMessage.setSubject(subject);

			LOGGER.debug("EMAIL SUBJECT-->" +  subject);
			LOGGER.debug("EMAIL CONTENT-->" +  content);
			
			// Get status
			long status = polReqData.getStatus().getStatusId();
			
			// Get attachment
			if (IUMConstants.STATUS_ORDERED == status) {
				RequirementForm rf = new RequirementForm();
				Attachment attachmentForm = rf.writeForm(polReqData);
				if (attachmentForm != null) {
					mailMessage.setAttachment(attachmentForm);
				}
			}

			// Set mail message
			notificationMessages.setMailMessage(mailMessage);
			
			LOGGER.debug("NOTIFY SMS-->" + notificationTempData.isNotifyMobile());
			// Check and set mobile message
			if (notificationTempData.isNotifyMobile()) {
				NotificationMessage smsMessage = new NotificationMessage();
				smsMessage.setSMS(true);
				
				String smsContent = ca.assemblePRContent(notificationTempData.getMobileNotificationContent(),polReqData,wfi);
				smsMessage.setContent(smsContent);
				
				LOGGER.debug("SMS CONTENT-->" +  smsContent);
				
				smsMessage.setRecipients(formatMobileNums(notificationRecipients.getMobiles()));
				smsMessage.setSender(sender.getEmailAddr());
				
				try{
					
					String agentId = wfi.getAgentId();
					System.out.println("SMS AgentId Recipient "+ agentId);
		            if(agentId!=null){
		            	SMSDAO smsDao = new SMSDAO();
		            	smsDao.insertIntoSMSNotification(smsMessage, agentId);
		            }
				}catch(Exception e){
					LOGGER.error("Exception error while inserting sms notification ");
					LOGGER.error(CodeHelper.getStackTrace(e));	
				}
				
				notificationMessages.setSmsMessage(smsMessage);
			} else {
				LOGGER.warn("notificationTempData.isNotifyMobile equal: FALSE");
			}
		} else {
			LOGGER.warn("Notification Template is null");
		}
		LOGGER.info("buildPolicyRequirementsNotice end");
		return notificationMessages;
	}


	/**
	 * @param assessmentData
	 * @param notificationID
	 * @throws AddressException 
	 */
	private NotificationMessages buildAssessmentNotice(
			final AssessmentRequestData assessmentData,
			final WorkflowItem wfi,
			final String notificationID,
			final String eventID) throws SQLException, AddressException {
		
		LOGGER.info("buildAssessmentNotice start");
		WorkflowDAO wfDAO = new WorkflowDAO();
		NotificationTemplateData notificationTempData = wfDAO.getNotificationTemplate(notificationID);
		NotificationMessages notificationMessages = new NotificationMessages();
		
		if (notificationTempData != null) {
			
			ArrayList consolidatedRecipients = getRecipients(wfi,notificationID,wfi.getLOB(), assessmentData.getReferenceNumber());
			NotificationRecipients notificationRecipients  = wfDAO.getNotificationRecipients(consolidatedRecipients,eventID,notificationID, wfi.getLOB());
			
			// Get sender
			UserProfileData sender = new UserProfileData();
			if (wfi.getSender() != null) {
				sender = wfDAO.getSender(wfi.getSender());
			}else {
				sender.setEmailAddr(getDefaultEmail());
			}
			
			// Set sender and recipients
			NotificationMessage mailMessage = new NotificationMessage();
			mailMessage.setSender(sender.getEmailAddr());
			mailMessage.setRecipients(notificationRecipients.getEmails());
			
			//for debugging
			InternetAddress senderEmailAddress = new InternetAddress(mailMessage.getSender());
			ArrayList addressesTo =mailMessage.getRecipients();
			
			LOGGER.debug("SENDER-->" +  senderEmailAddress);
			LOGGER.debug("RECIPIENTS-->" +  addressesTo);
			
			// Set content assembler
			ContentAssembler ca = new ContentAssembler();
			String content = ca.assembleARContent(notificationTempData.getEmailNotificationContent(),assessmentData,wfi);
			mailMessage.setContent(content);
			String subject = ca.assembleARContent(notificationTempData.getSubject(),assessmentData,wfi);
			
			mailMessage.setSubject(subject);

			LOGGER.debug("EMAIL SUBJECT-->" +  subject);
			LOGGER.debug("EMAIL CONTENT-->" +  content);
			
			// Set mail message
			notificationMessages.setMailMessage(mailMessage);
			
			LOGGER.debug("NOTIFY SMS-->" + notificationTempData.isNotifyMobile());
			// Check and set mobile message
			if (notificationTempData.isNotifyMobile()) {
				
				NotificationMessage smsMessage = new NotificationMessage();
				smsMessage.setSMS(true);
				
				String smsContent = ca.assembleARContent(notificationTempData.getMobileNotificationContent(),assessmentData,wfi);
				smsMessage.setContent(smsContent);
				
				LOGGER.debug("SMS CONTENT-->" +  smsContent);
				
				smsMessage.setRecipients(formatMobileNums(notificationRecipients.getMobiles()));
				smsMessage.setSender(sender.getEmailAddr());
				notificationMessages.setSmsMessage(smsMessage);
			} else {
				LOGGER.warn("notificationTempData.isNotifyMobile equal: FALSE");
			}
		} else {
			LOGGER.warn("Notification Template is null");
		}
		LOGGER.info("buildAssessmentNotice end");
		return notificationMessages;
	}

	/**
	 * 10/24/05: Nic Decapia: Added reference number parameter for CR#10136 fix
	 * @param wfi
	 * @return
	 */
	private ArrayList getRecipients(
			final WorkflowItem wfi,
			final String notificationID,
			final String lobCode,
			final String referenceNumber) throws SQLException {
		
		LOGGER.info("getRecipients start");
		ArrayList recipients = new ArrayList();
		WorkflowDAO wfDAO = new WorkflowDAO();
		ArrayList primaryRecipients = wfi.getPrimaryRecipient();
		
		ArrayList secondaryRecipients = new ArrayList();
		secondaryRecipients = wfDAO.getSecondaryRecipients(notificationID, lobCode, wfi.getBranchId(), referenceNumber);
		
		if(primaryRecipients!=null){
			Iterator it1 = primaryRecipients.iterator();
			while (it1.hasNext()) {
				recipients.add(it1.next());
			}
		}
		
		if(secondaryRecipients!=null){
			Iterator it2 = secondaryRecipients.iterator();
			while (it2.hasNext()) {
				recipients.add(it2.next());
			}
		}
		LOGGER.info("getRecipients end");
		return recipients;
	}

	private PolicyRequirementsData generateLOANotification(MedicalRecordData medRecData) {
		
		LOGGER.info("generateLOANotification start");
		String labInd = medRecData.getLabTestInd();
		PolicyRequirementsData polRecData = new PolicyRequirementsData();
		polRecData.setReqDesc(medRecData.getReferenceNo());
		polRecData.setClientId(medRecData.getClient().getClientId());
		if ( labInd != null && IUMConstants.YES.equals(labInd)) {
			polRecData.setRequirementCode(IUMConstants.REQUIREMENT_CODE_LAB_EXAM_LOA);
		} else {
			polRecData.setRequirementCode(IUMConstants.REQUIREMENT_CODE_MEDICAL_EXAM_LOA);
		}
		LOGGER.info("generateLOANotification end");
		return polRecData;
	}

	private void triggerLWS(
			final MedicalRecordData medRecData,
			final ArrayList recipients) throws NotesException {
		
		LOGGER.info("triggerLWS start");
		
		DominoTranslator dominoTranslator = new DominoTranslator();
		dominoTranslator.triggerLWS(medRecData, recipients);
		
		LOGGER.info("triggerLWS end");
	}

	private ArrayList formatMobileNums(final ArrayList mobileNums) {
		
		LOGGER.info("formatMobileNums start");
		ArrayList formattedNumbers = new ArrayList();
		Iterator it = mobileNums.iterator();
		ResourceBundle rb = ResourceBundle.getBundle(IUMConstants.IUM_CONFIG);

		while (it.hasNext()) {
			
			String rawNum = (String)it.next();
			if (rawNum != null) {
				
				StringBuffer mobileNum = new StringBuffer(rawNum);
				if (mobileNum.length() == IUMConstants.MOBILE_NUM_LEN
						&& rawNum.startsWith(IUMConstants.MOBILE_PREFIX)) {
					
					mobileNum.append(rb.getString("sms_domain"));
					formattedNumbers.add(mobileNum.toString());
				}else {
					
					int len = rawNum.length();
					int endlen = (len>0)?len:0;
					int startLen =(len>9)?len-9:0;
					
					mobileNum.replace(0,len,rawNum.substring(startLen,endlen));
					mobileNum.insert(0,IUMConstants.MOBILE_PREFIX);
					mobileNum.append(rb.getString("sms_domain"));
					
					formattedNumbers.add(mobileNum.toString());
				}
			}
		}
		LOGGER.info("formatMobileNums end");
		return formattedNumbers;
	}

	private String getDefaultEmail() {

		LOGGER.info("getDefaultEmail start");
		
		ResourceBundle rb = ResourceBundle.getBundle(IUMConstants.IUM_CONFIG);
		String account =  rb.getString(IUMConstants.IUM_SMTP_ACCNT);
		
		LOGGER.info("getDefaultEmail end");
		return account;
	}

	private NotificationMessages buildPolicyRequirementsNotice(
			final ArrayList listPolReq,
			final WorkflowItem wfi,
			final String notificationID,
			final String eventID) throws Exception {
		
		LOGGER.info("buildPolicyRequirementsNotice start");
		WorkflowDAO wfDAO = new WorkflowDAO();
		NotificationTemplateData notificationTempData = wfDAO.getNotificationTemplate(notificationID);
		NotificationMessages notificationMessages = new NotificationMessages();
		
		
		if (notificationTempData != null) {
			PolicyRequirementsData polReqData = new PolicyRequirementsData();
			if (listPolReq.size() > 0){
				int i = 0;
				polReqData = (PolicyRequirementsData) listPolReq.get(i);
			}

			ArrayList consolidatedRecipients = getRecipients(wfi, notificationID, wfi.getLOB(), polReqData.getReferenceNumber());
			NotificationRecipients notificationRecipients  = wfDAO.getNotificationRecipients(consolidatedRecipients, eventID, notificationID, wfi.getLOB());
			
			UserProfileData sender = new UserProfileData();
			LOGGER.debug("wfi.getSender() " + wfi.getSender());
			if (wfi.getSender() != null) {
				sender = wfDAO.getSender(wfi.getSender());
				LOGGER.debug("sender user email " + sender.getEmailAddr());
			}else {
				sender.setEmailAddr(getDefaultEmail());
			}
			LOGGER.debug("sender " + sender.getEmailAddr());
			
			// Set final sender and recipients
			NotificationMessage mailMessage = new NotificationMessage();
			mailMessage.setSender(sender.getEmailAddr());
			mailMessage.setRecipients(notificationRecipients.getEmails());

			//for debugging
			InternetAddress senderEmailAddress = new InternetAddress(mailMessage.getSender());
			ArrayList addressesTo =mailMessage.getRecipients();
			
			LOGGER.debug("SENDER-->" +  senderEmailAddress);
			LOGGER.debug("RECIPIENTS-->" +  addressesTo);
			
			// Set content assembler
			ContentAssembler ca = new ContentAssembler();
			String content = ca.assemblePRContent(notificationTempData.getEmailNotificationContent(), listPolReq, wfi);
			mailMessage.setContent(content);

			// Set subject
			
			AssessmentRequestDAO arDao = new AssessmentRequestDAO(); 
			AssessmentRequestData arData =  arDao.retrieveRequestDetail(polReqData.getReferenceNumber());
			
			//fix for null owner error on send notification button
			if(arData.getOwner().getClientId() != null && (arData.getInsured().getClientId()) != null)
			if(!arData.getOwner().getClientId().equals(arData.getInsured().getClientId())){
				notificationTempData.setSubject(IUMConstants.OWNER_INSURED_SUBJECT_TEMPLATE);
			}
			String subject = ca.assemblePRContent(notificationTempData.getSubject(),polReqData,wfi);
			mailMessage.setSubject(subject);

			LOGGER.debug("EMAIL SUBJECT-->" +  subject);
			LOGGER.debug("EMAIL CONTENT-->" +  content);
			
			// Sets the attachments
			PolicyRequirements policyRequirement = new PolicyRequirements();
			ArrayList listOfAttachments = policyRequirement.retrieveAttachment(wfi.getObjectID());
			mailMessage.setOrderSpecialCase(IUMConstants.ORDER_REQUIREMENT);
			mailMessage.setRequirementAttachment(listOfAttachments);

			// Set message content
			notificationMessages.setMailMessage(mailMessage);
			
			LOGGER.debug("NOTIFY SMS-->" + notificationTempData.isNotifyMobile());
			// Check if needed to send sms
			if (notificationTempData.isNotifyMobile()) {
				
				NotificationMessage smsMessage = new NotificationMessage();
				smsMessage.setSMS(true);
				
				String smsContent = ca.assemblePRContent(notificationTempData.getMobileNotificationContent(),listPolReq,wfi);
				smsMessage.setContent(smsContent);
				smsMessage.setRecipients(formatMobileNums(notificationRecipients.getMobiles()));
				smsMessage.setSender(sender.getEmailAddr());
				
				LOGGER.debug("SMS CONTENT-->"+ smsContent);
				try{
					String agentId =  wfi.getAgentId();
					System.out.println("SMS AgentId Recipient "+ agentId);
		            if(agentId!=null){
		            	SMSDAO smsDao = new SMSDAO();
		            	smsDao.insertIntoSMSNotification(smsMessage, agentId);
		            }
				}catch(Exception e){
					LOGGER.error("Exception error while inserting sms notification");
					LOGGER.error(CodeHelper.getStackTrace(e));	
				}
				
				notificationMessages.setSmsMessage(smsMessage);
			} else {
				LOGGER.warn("notificationTempData.isNotifyMobile equal: FALSE");
			}
		} else {
			LOGGER.warn("Notification Template is null");
		}
		
		LOGGER.info("buildPolicyRequirementsNotice end");
		return notificationMessages;
	}

	public void removeAttachment(final String referenceNumber) throws SQLException{
		
		LOGGER.info("removeAttachment start");
		
		WorkflowDAO wfDAO = new WorkflowDAO();
		wfDAO.removeAttachments(referenceNumber);
			
		LOGGER.info("removeAttachment end");
	}


	public void  sendFollowUpRequirement(
			final PolicyRequirementsData polReqData,
			final WorkflowItem wfi) throws Exception {
		
		LOGGER.info("sendFollowUpRequirement start");
		WorkflowDAO wfDAO = new WorkflowDAO();
		String notificationId = String.valueOf(IUMConstants.NOTIFICATION_ID_FOLLOW_UP);
		String eventId = String.valueOf(wfDAO.retrieveEventId(IUMConstants.STATUS_ORDERED,IUMConstants.STATUS_ORDERED, wfi.getLOB()));
		NotificationTemplateData notificationTempData = wfDAO.getNotificationTemplate(notificationId);
		NotificationMessages notificationMessages = new NotificationMessages();
		
		if (notificationTempData != null) {
			// Get recipients
			ArrayList consolidatedRecipients = getRecipients(wfi, notificationId, wfi.getLOB(), "");
			NotificationRecipients notificationRecipients  = wfDAO.getNotificationRecipients(consolidatedRecipients,eventId,notificationId, wfi.getLOB());
			
			// Get sender
			UserProfileData sender = new UserProfileData();
			if (wfi.getSender() != null) {
				sender = wfDAO.getSender(wfi.getSender());
			}else {
				sender.setEmailAddr(getDefaultEmail());
			}
			
			// Set sender and recipients
			NotificationMessage mailMessage = new NotificationMessage();
			mailMessage.setSender(sender.getEmailAddr());
			mailMessage.setRecipients(notificationRecipients.getEmails());
			
			//for debugging
			InternetAddress senderEmailAddress = new InternetAddress(mailMessage.getSender());
			ArrayList addressesTo =mailMessage.getRecipients();
			
			LOGGER.debug("SENDER-->" +  senderEmailAddress);
			LOGGER.debug("RECIPIENTS-->" +  addressesTo);
			
			// Set content assembler
			ContentAssembler ca = new ContentAssembler();
			String content = ca.assemblePRContent(notificationTempData.getEmailNotificationContent(),polReqData,wfi);
			mailMessage.setContent(content);
			String subject = ca.assemblePRContent(notificationTempData.getSubject(),polReqData,wfi);
			mailMessage.setSubject(subject);

			LOGGER.debug("EMAIL SUBJECT-->" +  subject);
			LOGGER.debug("EMAIL CONTENT-->" +  content);

			// Set mail message
			notificationMessages.setMailMessage(mailMessage);
			
			LOGGER.debug("NOTIFY SMS-->" + notificationTempData.isNotifyMobile());
			// Check and set mobile message
			if (notificationTempData.isNotifyMobile()) {
				NotificationMessage smsMessage = new NotificationMessage();
				smsMessage.setSMS(true);
				
				String smsContent = ca.assemblePRContent(notificationTempData.getMobileNotificationContent(),polReqData,wfi);
				smsMessage.setContent(smsContent);
				LOGGER.debug("SMS CONTENT-->" +  smsContent);
				smsMessage.setRecipients(formatMobileNums(notificationRecipients.getMobiles()));
				smsMessage.setSender(sender.getEmailAddr());
				notificationMessages.setSmsMessage(smsMessage);
			}
		}

		NotificationMessages noticeMessages = notificationMessages;
		NotificationMessage mail = noticeMessages.getMailMessage();
		Notifier notifier = new Notifier();
		
		if (mail.getRecipients().size() > 0) {
			notifier.sendMessages(noticeMessages);
		}
		LOGGER.info("sendFollowUpRequirement end");
	}

}
