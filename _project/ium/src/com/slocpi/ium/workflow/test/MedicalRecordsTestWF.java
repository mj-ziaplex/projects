/**
 * AssessmentRequestTestWF.java				Copyright (c) 2004 Pointwest Technologies Corp.
 * TODO DOCUMENT ME!
 * 
 * @author Barry Yu	 @date Jan 13, 2004
 */
package com.slocpi.ium.workflow.test;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import junit.textui.TestRunner;

import com.slocpi.ium.util.IUMConstants;
import com.slocpi.ium.workflow.Workflow;
import com.slocpi.ium.workflow.WorkflowItem;

/**
 * TODO DOCUMENT ME!
 * 
 * @author Barry Yu		Copyright (c) 2004 Pointwest Technologies Corp.
 * @date Jan 13, 2004
 */
public class MedicalRecordsTestWF extends TestCase {

	/**
	 * Constructor for AssessmentRequestTestWF.
	 * @param arg0
	 */
	public MedicalRecordsTestWF(String arg0) {
		super(arg0);
	}
	
	public static Test suite() {
		TestSuite suite = new TestSuite(MedicalRecordsTestWF.class);
		return suite;
	}
	
	public static void main(String[] args) {
		TestRunner.run(suite());
	}
	//Request Medical/Laboratory Exam
	public void test_MR_IUMF2100() throws Exception {
		WorkflowItem wfi = new WorkflowItem();
		wfi.setType(IUMConstants.MEDICAL_RECORDS);
		wfi.setObjectID("1");
		wfi.setLOB("IL");
		//wfi.setPreviousStatus("5");
		Workflow wf = new Workflow();		
		try {
			wf.processState(wfi);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			assertEquals("Error on TestCase MR_IUMF2100",true,false);
		}
	}	
	
		//	Request Medical/Laboratory Exam
	  public void test_MR_IUMF2110() throws Exception {
		  WorkflowItem wfi = new WorkflowItem();
		  wfi.setType(IUMConstants.MEDICAL_RECORDS);
		  wfi.setObjectID("3");
		  wfi.setLOB("IL");
		  wfi.setPreviousStatus(String.valueOf(IUMConstants.MED_REQUESTED));
		  Workflow wf = new Workflow();		
		  try {
			  wf.processState(wfi);
		  } catch (Exception e) {
			  // TODO Auto-generated catch block
			  e.printStackTrace();
			  assertEquals("Error on TestCase MR_IUMF2100",true,false);
		  }
	  }	
	  
		//	Request Medical/Laboratory Exam
	  public void test_MR_IUMF2500() throws Exception {
		  WorkflowItem wfi = new WorkflowItem();
		  wfi.setType(IUMConstants.MEDICAL_RECORDS);
		  wfi.setObjectID("2");
		  wfi.setLOB("IL");
		  wfi.setPreviousStatus("19");
		  Workflow wf = new Workflow();		
		  try {
			  wf.processState(wfi);
		  } catch (Exception e) {
			  // TODO Auto-generated catch block
			  e.printStackTrace();
			  assertEquals("Error on TestCase MR_IUMF2100",true,false);
		  }
	  }	
	  
//	Request Medical/Laboratory Exam
	  public void test_MR_IUMF2500_B() throws Exception {
		  WorkflowItem wfi = new WorkflowItem();
		  wfi.setType(IUMConstants.MEDICAL_RECORDS);
		  wfi.setObjectID("2");
		  wfi.setLOB("IL");
		  //wfi.setPreviousStatus("19");
		  Workflow wf = new Workflow();		
		  try {
			  wf.processState(wfi);
		  } catch (Exception e) {
			  // TODO Auto-generated catch block
			  e.printStackTrace();
			  assertEquals("Error on TestCase MR_IUMF2100",true,false);
		  }
	  }	
}
