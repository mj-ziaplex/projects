/**
 * AssessmentRequestTestWF.java				Copyright (c) 2004 Pointwest Technologies Corp.
 * TODO DOCUMENT ME!
 * 
 * @author Barry Yu	 @date Jan 13, 2004
 */
package com.slocpi.ium.workflow.test;

import java.util.ArrayList;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import junit.textui.TestRunner;

import com.slocpi.ium.util.IUMConstants;
import com.slocpi.ium.workflow.Workflow;
import com.slocpi.ium.workflow.WorkflowItem;

/**
 * TODO DOCUMENT ME!
 * 
 * @author Barry Yu		Copyright (c) 2004 Pointwest Technologies Corp.
 * @date Jan 13, 2004
 */
public class PolicyRequirementsTestWF extends TestCase {

	/**
	 * Constructor for AssessmentRequestTestWF.
	 * @param arg0
	 */
	public PolicyRequirementsTestWF(String arg0) {
		super(arg0);
	}
	
	public static Test suite() {
		TestSuite suite = new TestSuite(PolicyRequirementsTestWF.class);
		return suite;
	}
	
	public static void main(String[] args) {
		TestRunner.run(suite());
	}
	
	//Ordering of requirements
	public void test_PR_IUMF1430() throws Exception {
		WorkflowItem wfi = new WorkflowItem();
		wfi.setType(IUMConstants.POLICY_REQUIREMENTS);
		wfi.setObjectID("441");
		ArrayList recipients = new ArrayList();
		recipients.add("WFUser3");
		recipients.add("WFUser1");
		wfi.setPrimaryRecipient(recipients);
		wfi.setLOB("IL");
		//wfi.setPreviousStatus("24");
		wfi.setSender("JDACA");
		Workflow wf = new Workflow();		
		try {
			wf.processState(wfi);
			wfi.setPreviousStatus("25");
			wf.processState(wfi);
			wfi.setPreviousStatus("26");
			wf.processState(wfi);
			wfi.setPreviousStatus("27");
			wf.processState(wfi);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			assertEquals("Error on TestCase PR_IUMF1430",true,false);			
		}
	}	
	
		//Receive Requirements in Site
	/*  public void test_PR_IUMF1440() throws Exception {
		  WorkflowItem wfi = new WorkflowItem();
		  wfi.setType(IUMConstants.POLICY_REQUIREMENTS);
		  wfi.setObjectID("2");
		  //wfi.setReferenceNum("A1");
		  wfi.setLOB("IL");
		  wfi.setPreviousStatus("13");
		  wfi.setSender("RLABI");
		  Workflow wf = new Workflow();		
		  try {
			  wf.processState(wfi);			  
		  } catch (Exception e) {
			  // TODO Auto-generated catch block
			  e.printStackTrace();
			  assertEquals("Error on TestCase PR_IUMF1430",true,false);			
		  }
	  }	
	  
	//	Cancel Requirements
	public void test_PR_IUMF1470() throws Exception {
		WorkflowItem wfi = new WorkflowItem();
		wfi.setType(IUMConstants.POLICY_REQUIREMENTS);
		wfi.setObjectID("5");
		//wfi.setReferenceNum("A1");
		wfi.setLOB("IL");
		wfi.setPreviousStatus("13");
		wfi.setSender("RLABI");
		Workflow wf = new Workflow();		
		try {
			wf.processState(wfi);			  
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			assertEquals("Error on TestCase PR_IUMF1470",true,false);			
		}
	}	
		
	//	Waive Requirements
	public void test_PR_IUMF1480() throws Exception {
		WorkflowItem wfi = new WorkflowItem();
		wfi.setType(IUMConstants.POLICY_REQUIREMENTS);
		wfi.setObjectID("4");
		//wfi.setReferenceNum("A1");
		wfi.setLOB("IL");
		wfi.setPreviousStatus("13");
		wfi.setSender("RLABI");
		Workflow wf = new Workflow();		
		try {
			wf.processState(wfi);			  
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			assertEquals("Error on TestCase PR_IUMF1470",true,false);			
		}
	}		
	*/
}
