/**
 * AssessmentRequestTestWF.java				Copyright (c) 2004 Pointwest Technologies Corp.
 * TODO DOCUMENT ME!
 * 
 * @author Barry Yu	 @date Jan 13, 2004
 */
package com.slocpi.ium.workflow.test;

import java.util.ArrayList;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import junit.textui.TestRunner;

import com.slocpi.ium.util.IUMConstants;
import com.slocpi.ium.workflow.Workflow;
import com.slocpi.ium.workflow.WorkflowItem;

/**
 * TODO DOCUMENT ME!
 * 
 * @author Barry Yu		Copyright (c) 2004 Pointwest Technologies Corp.
 * @date Jan 13, 2004
 */
public class AssessmentRequestTestWF extends TestCase {

	/**
	 * Constructor for AssessmentRequestTestWF.
	 * @param arg0
	 */
	public AssessmentRequestTestWF(String arg0) {
		super(arg0);
	}
	
	public static Test suite() {
		TestSuite suite = new TestSuite(AssessmentRequestTestWF.class);
		return suite;
	}
	
	public static void main(String[] args) {
		TestRunner.run(suite());
	}
	//Generate Assessment Request Individual Life
	public void test_AR_IUMF1000_IL() throws Exception {
		WorkflowItem wfi = new WorkflowItem();
		wfi.setType(IUMConstants.ASSESSMENT_REQUEST);
		wfi.setObjectID("YVES123");
		wfi.setLOB("IL");
		//wfi.setPreviousStatus("5");
		Workflow wf = new Workflow();
		ArrayList recipients  = new ArrayList();
		recipients.add("DAYOJ");
		//recipients.add("JDACA");
		wfi.setPrimaryRecipient(recipients);
		wfi.setBranchId("SUNLIFEHO");
		try {
			wf.processState(wfi);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			assertEquals("Error on TestCase AR_IUMF1000",true,false);
		}
	}
	
	//	Generate Assessment Request Pre-need
	public void test_AR_IUMF1000_PN() throws Exception {
		WorkflowItem wfi = new WorkflowItem();
		wfi.setType(IUMConstants.ASSESSMENT_REQUEST);
		wfi.setObjectID("WFAR0002");
		wfi.setLOB("PN");
		//wfi.setPreviousStatus("5");
		Workflow wf = new Workflow();
		ArrayList recipients  = new ArrayList();
		recipients.add("WFUser4");
		wfi.setPrimaryRecipient(recipients);
		wfi.setBranchId("SUNLIFEHO");	
		try {
			wf.processState(wfi);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			assertEquals("Error on TestCase AR_IUMF1000",true,false);
		}
	}
	//Create Assessment Request
	public void test_AR_IUMF1010() throws Exception {
		WorkflowItem wfi = new WorkflowItem();
		wfi.setType(IUMConstants.ASSESSMENT_REQUEST);
		wfi.setObjectID("WFAR0003");
		wfi.setLOB("IL");
		//wfi.setPreviousStatus("");
		Workflow wf = new Workflow();
		
		try {
			wf.processState(wfi);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			assertEquals("Error on TestCase AR_IUMF1010",true,false);
		}
	}
	//Submit Assessment Request to Underwriter			
	public void test_AR_IUMF1090() throws Exception {
		WorkflowItem wfi = new WorkflowItem();
		wfi.setType(IUMConstants.ASSESSMENT_REQUEST);
		wfi.setObjectID("WFAR0004");
		wfi.setLOB("IL");		
		Workflow wf = new Workflow();
		ArrayList recipients  = new ArrayList();
		recipients.add("WFUser5");	
		wfi.setPrimaryRecipient(recipients);
		wfi.setBranchId("SUNLIFEHO");	
		try {
			wf.processState(wfi);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			assertEquals("Error on TestCase AR_IUMF1090",true,false);
		}
	}
	//Reassign Assessment Request
	public void test_AR_IUMF1100() throws Exception {
		WorkflowItem wfi = new WorkflowItem();
		wfi.setType(IUMConstants.ASSESSMENT_REQUEST);
		wfi.setObjectID("WFAR0004");
		wfi.setLOB("IL");
		wfi.setPreviousStatus("4");
		wfi.setSender("WFUser6");
		Workflow wf = new Workflow();
		ArrayList recipients  = new ArrayList();
		recipients.add("WFUser4");	
		wfi.setPrimaryRecipient(recipients);
		wfi.setBranchId("SUNLIFEHO");	
		try {
			wf.processState(wfi);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			assertEquals("Error on TestCase AR_IUMF1100",true,false);
		}
	}
	//Approve Application as Applied For	
	public void test_AR_IUMF1240() throws Exception {
		WorkflowItem wfi = new WorkflowItem();
		wfi.setType(IUMConstants.ASSESSMENT_REQUEST);
		wfi.setObjectID("WFAR0005");
		wfi.setLOB("IL");		
		Workflow wf = new Workflow();
		ArrayList recipients  = new ArrayList();
		recipients.add("WFUser1");
		recipients.add("WFUser3");
		wfi.setPrimaryRecipient(recipients);
		wfi.setBranchId("SUNLIFEHO");
		try {
			wf.processState(wfi);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			assertEquals("Error on TestCase AR_IUMF1240",true,false);
		}
	}
	
	//	Approve Application as Applied For	
	  public void test_AR_IUMF1270() throws Exception {
		  WorkflowItem wfi = new WorkflowItem();
		  wfi.setType(IUMConstants.ASSESSMENT_REQUEST);
		  wfi.setObjectID("WFAR0006");
		  wfi.setLOB("IL");				 
		  Workflow wf = new Workflow();
		  ArrayList recipients  = new ArrayList();
		  recipients.add("WFUser1");
		  recipients.add("WFUser3");
		  wfi.setPrimaryRecipient(recipients);
		  wfi.setBranchId("SUNLIFEHO");
		  try {
			  wf.processState(wfi);
		  } catch (Exception e) {
			  // TODO Auto-generated catch block
			  e.printStackTrace();
			  assertEquals("Error on TestCase AR_IUMF1260",true,false);
		  }
	  }
}
