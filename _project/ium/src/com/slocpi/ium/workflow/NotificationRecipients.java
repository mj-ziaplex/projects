package com.slocpi.ium.workflow;

import java.util.ArrayList;

/**
 * This class is used to encapsulate the notification recipients details.
 * 
 * @author Barry Yu		Copyright (c) 2004 Pointwest Technologies Corp.
 * @date Jan 6, 2004
 */
public class NotificationRecipients {
	private ArrayList emails  = null;
	private ArrayList mobiles = null;
	
	/**
	 * Returns the email address of the recipients.
	 * @return
	 */
	public ArrayList getEmails() {
		return emails;
	}

	/**
	 * Returns the mobile numbers of the recipients.
	 * @return
	 */
	public ArrayList getMobiles() {
		return mobiles;
	}

	/**
	 * Sets the email addresses of the recipients.
	 * @param list
	 */
	public void setEmails(ArrayList list) {
		emails = list;
	}

	/**
	 * Sets the mobile numbers of the recipients.
	 * @param list
	 */
	public void setMobiles(ArrayList list) {
		mobiles = list;
	}

}
