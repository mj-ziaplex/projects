package com.slocpi.ium.workflow;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * This bean class is used to hold workflow needed attributes and parameters.
 * 
 * @author Barry Yu		Copyright (c) 2004 Pointwest Technologies Corp.
 * @date Jan 6, 2004
 */
public class WorkflowItem implements Serializable{
	private String objectID         	= null;
	private String previousStatus   	= null;
	private String type             	= null;
	private String currentStatus    	= null;
	private String LOB              	= null;
	private String sender           	= null;
	private String branchId         	= null;
	private ArrayList primaryRecipient  = null;
	private String specialCase			= null;
	private String agentId;
	

	/**
	 * Returns the object's ID.
	 * @return
	 */
	public String getObjectID() {
		return objectID;
	}

	/**
	 * Returns the object's previous status.
	 * @return
	 */
	public String getPreviousStatus() {
		return previousStatus;
	}

	/**
	 * Returns the request type (Assessment Request, Medical, Non-Medical).
	 * @return
	 */
	public String getType() {
		return type;
	}

	/**
	 * Sets the Object's ID.
	 * @param objectId
	 */
	public void setObjectID(String objectId) {
		this.objectID = objectId;
	}
	
	public void setObjectID(long objID) {
		this.objectID = String.valueOf(objID);
	}
		
	/**
	 * Sets the Object's Previous Status.
	 * @param string
	 */
	public void setPreviousStatus(String previousStatus) {
		this.previousStatus = previousStatus;
	}

	/**
	 * Sets the request type.
	 * @param string
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * Returns the Object's current status.
	 * @return
	 */
	public String getCurrentStatus() {
		return currentStatus;
	}

	/**
	 * Sets the object's current status.
	 * @param currentStatus
	 */
	public void setCurrentStatus(String currentStatus) {
		this.currentStatus = currentStatus;
	}

	/**
	 * Returns the Line of Business.
	 * @return
	 */
	public String getLOB() {
		return LOB;
	}

	/**
	 * Sets the Line of Business.
	 * @param lob
	 */
	public void setLOB(String LOB) {
		this.LOB = LOB;
	}

	/**
	 * Returns the Sender.
	 * @return
	 */
	public String getSender() {
		return this.sender;
	}

	/**
	 * Sets the sender.
	 * @param sender
	 */
	public void setSender(String sender) {
		this.sender = sender;
	}
	

	/**
	 * Returns the branch ID.
	 * @return
	 */
	public String getBranchId() {
		return this.branchId;
	}

	/**
	 * Returns the primary recipients.
	 * @return
	 */
	public ArrayList getPrimaryRecipient() {
		return this.primaryRecipient;
	}

	/**
	 * Sets the branch ID.
	 * @param string
	 */
	public void setBranchId(String branchId) {
		this.branchId = branchId;
	}

	/**
	 * Sets the primary recipients.
	 * @param string
	 */
	public void setPrimaryRecipient(ArrayList primaryRecipient) {
		this.primaryRecipient = primaryRecipient;
	}

	/**
	 * @return
	 */
	public String getSpecialCase() {
		return specialCase;
	}

	/**
	 * @param string
	 */
	public void setSpecialCase(String string) {
		specialCase = string;
	}

	public String getAgentId() {
		return agentId;
	}

	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}

}
