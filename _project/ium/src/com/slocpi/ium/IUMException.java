/*
 * Created on Jan 14, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium;

/**
 * @author daguila
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class IUMException extends Exception {
	
	private Exception origException = null;
	
	public IUMException() {
	}
	
	public IUMException(String msg) {
		super(msg);
	}
	
	public IUMException(Exception e) {
		
		super(e.getMessage());
		origException = e;
	}
}
