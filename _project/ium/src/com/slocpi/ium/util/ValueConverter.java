/**
 * Converter.java				Copyright (c) 2004 Pointwest Technologies Corp.
 * TODO DOCUMENT ME!
 * 
 * @author aditalo	 @date Jan 6, 2004
 */
package com.slocpi.ium.util;

import java.math.BigDecimal;
import java.util.StringTokenizer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * TODO DOCUMENT ME!
 * 
 * @author aditalo		Copyright (c) 2004 Pointwest Technologies Corp.
 * @date Jan 6, 2004
 */
public class ValueConverter {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ValueConverter.class);
	
	public static String booleanToString(boolean b) {
		
		if (b) {
			return "Y";
		}else{
			return "N";
		}
		
	}
	
	public static boolean stringToBoolean(String string) {
		
		if (string != null && string.equalsIgnoreCase("Y")) {
			return true;
		} else {
			return false;
		}
	}
	
	public static double convertCurrency(String numberWithCommas){
		
		String value = "";
		if(numberWithCommas!=null && numberWithCommas.length()>0){
		
			StringTokenizer token = new StringTokenizer(numberWithCommas,",");
			
			while(token.hasMoreTokens()){
				value = value+ token.nextToken();	
			}
		}
		if (value.equals("")){
			return 0d;
		}else{
			return Double.parseDouble(value);
		}
		
	}
	
	public static String nullToString(String s) {
		
		if (s == null) {
			s = "";
		}
		
		return (s);
	}

	/**
	 * @param elapsedTimeLob
	 * @return
	 */
	public static BigDecimal convertToHours(BigDecimal value)
	{
		BigDecimal result = new BigDecimal(0);
		BigDecimal hoursInADay = new BigDecimal(8);
		if (value != null) {
			if (!value.equals(result)) {
				result = value.multiply(hoursInADay);
			}
		}
		
		return (result);
	}

	/**
	 * @param decimal
	 * @return
	 */
	public static BigDecimal nullToZero(BigDecimal decimal)
	{
		
		BigDecimal result = new BigDecimal(0);
		if (decimal != null) result = decimal;
		
		return (result);
	}

    /**
     * 
     * @author Nic Decapia
     *
     */
	public static String newLineToHTML(String arg) {
		
		String result = "";
		StringTokenizer token = new StringTokenizer(arg, "\n");
		while (token.hasMoreTokens()) {
			result += token.nextToken() + " <br>";
		}
		
		return (result); 
	}
	    		
}
