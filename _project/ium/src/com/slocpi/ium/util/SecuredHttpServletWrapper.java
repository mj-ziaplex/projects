package com.slocpi.ium.util;

import java.util.Enumeration;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SecuredHttpServletWrapper extends HttpServletRequestWrapper implements HttpServletRequest {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(SecuredHttpServletWrapper.class);
	public SecuredHttpServletWrapper(HttpServletRequest request) {
		super(request);
	}
	
	
	/**
	 * Return true if this Request has a parameter
	 * value that has a script which can be used
	 * for hacking.
	 * 
	 * @return
	 */
	public boolean hasScript() {
		LOGGER.info("hasScript start");
		
		Enumeration e = getParameterNames();
		
		while (e.hasMoreElements()) {
			String param = e.nextElement().toString();
			if (hasIllegalChars(getRequest().getParameter(param))) {
				LOGGER.debug("ILLEGAL: " + getRequest().getParameter(param));
				return true;
			}
		}
		LOGGER.info("hasScript end");
		return false;
	}
	
	/**
	 * Determine if the string is a script.
	 * 
	 * @param str
	 * @return
	 */
	private boolean hasIllegalChars(String str) {
		 
		LOGGER.info("hasIllegalChars start");
		int none = -1;
		
		if (str.toLowerCase().indexOf("javascript") != none || 
				str.toLowerCase().indexOf("script") != none ||
				str.indexOf("%3C") != none ||
				str.indexOf("<") != none ||
				str.indexOf(">") != none ) {
			LOGGER.info("hasIllegalChars end");
			return true;
		}
		LOGGER.info("hasIllegalChars end");
		return false;
	}

}
