/**
 * SortHelper.java				Copyright (c) 2004 Pointwest Technologies Corp.
 * TODO DOCUMENT ME!
 * 
 * @author aditalo	 @date Feb 19, 2004
 */
package com.slocpi.ium.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * TODO DOCUMENT ME!
 * 
 * @author aditalo		Copyright (c) 2004 Pointwest Technologies Corp.
 * @date Feb 19, 2004
 */
public class SortHelper {

	private static final Logger LOGGER = LoggerFactory.getLogger(SortHelper.class);
	private String table = "";
	private int column;
	private String sortOrder = "";
	private String orderBy = "";

	/**
	 * Constructor
	 * 
	 * @param tableName - Valid values are IUMConstants.TABLE_ASSESSMENT_REQUESTS and IUMConstants.TABLE_MEDICAL_RECORDS
	 */
	public SortHelper(String tableName) {
		table = tableName;
	}

	

	/**
	 * @return
	 */
	public String getOrderBy() {
		LOGGER.info("getOrderBy start");
		if (this.sortOrder == null) {
			this.sortOrder = "";
		}
		
		if (table.equalsIgnoreCase(IUMConstants.TABLE_ASSESSMENT_REQUESTS)) {
			switch(column){
				case IUMConstants.SORT_BY_LOB :
					this.orderBy = " ORDER BY 2 " + this.sortOrder + " ,upper(REFERENCE_NUM) " + this.sortOrder;
					break;
				case IUMConstants.SORT_BY_NAME_OF_INSURED :
					this.orderBy = " ORDER BY upper(INSURED.CL_LAST_NAME) " + this.sortOrder + ", upper(INSURED.CL_GIVEN_NAME) " + this.sortOrder+ ", upper(INSURED.CL_MIDDLE_NAME) " + this.sortOrder + ", upper(REFERENCE_NUM) " + this.sortOrder;
					break;
				case IUMConstants.SORT_BY_STATUS :
					this.orderBy = " ORDER BY 3 " + this.sortOrder + " ,upper(REFERENCE_NUM) " + this.sortOrder;
					break;
				case IUMConstants.SORT_BY_AMOUNT_COVERED :
					this.orderBy = " ORDER BY 7 " + this.sortOrder + " ,upper(REFERENCE_NUM) " + this.sortOrder;
					break;
				case IUMConstants.SORT_BY_ASSIGNED_TO :
					this.orderBy = " ORDER BY upper(ASSIGN.USR_LAST_NAME) " + this.sortOrder + ", upper(ASSIGN.USR_FIRST_NAME) " + this.sortOrder+ ", upper(ASSIGN.USR_MIDDLE_NAME) " + this.sortOrder + ", upper(REFERENCE_NUM) " + this.sortOrder;
					break;
				case IUMConstants.SORT_BY_BRANCH :
					this.orderBy = " ORDER BY upper(BR.SLO_OFFICE_NAME) " + this.sortOrder + " ,upper(REFERENCE_NUM) " + this.sortOrder;
					break;
				case IUMConstants.SORT_BY_AGENT :
					this.orderBy = " ORDER BY upper(AGNT.USR_LAST_NAME) " + this.sortOrder + ", upper(AGNT.USR_FIRST_NAME) " + this.sortOrder+ ", upper(AGNT.USR_MIDDLE_NAME) " + this.sortOrder + ", upper(REFERENCE_NUM) " + this.sortOrder;
					break;
				case IUMConstants.SORT_BY_APP_RECEIVED_DATE :
					this.orderBy = " ORDER BY 15 " + this.sortOrder + " ,upper(REFERENCE_NUM) " + this.sortOrder;
					break;
				case IUMConstants.SORT_BY_LOCATION :
					this.orderBy = " ORDER BY upper(LOC.USR_LAST_NAME) " + this.sortOrder + ", upper(LOC.USR_FIRST_NAME) " + this.sortOrder+ ", upper(LOC.USR_MIDDLE_NAME) " + this.sortOrder + ", upper(REFERENCE_NUM) " + this.sortOrder;
					break;
					
				default:
					this.orderBy = " ORDER BY upper(REFERENCE_NUM) " + this.sortOrder;
					break;					
			}//end-switch						
		}
		else if(table.equalsIgnoreCase(IUMConstants.TABLE_MEDICAL_RECORDS)) {
			switch (column) {
				case 1:
					this.orderBy = " ORDER BY MED.MED_DATE_CONDUCTED " + this.sortOrder;
					break;
				case 2:
					this.orderBy = " ORDER BY MED.MED_RECORD_ID " + this.sortOrder;
					break;
				case 3:
					this.orderBy = " ORDER BY UPPER(POL.UAR_REFERENCE_NUM) " + this.sortOrder;
					break;
				case 4:
					this.orderBy = " ORDER BY UPPER(MED.CL_CLIENT_NUM) " + this.sortOrder;
					break;
				case 5:
					this.orderBy = " ORDER BY UPPER(DECODE(MED.CL_CLIENT_NUM, NULL, MED.CL_LAST_NAME, CLI.CL_LAST_NAME)) " + this.sortOrder;
					break;
				case 6:
					this.orderBy = " ORDER BY UPPER(DECODE(MED.CL_CLIENT_NUM, NULL, MED.CL_GIVEN_NAME, CLI.CL_GIVEN_NAME)) " + this.sortOrder;
					break;
				case 7:
					this.orderBy = " ORDER BY UPPER(DECODE(MED.MED_LAB_TEST_IND, '1', LAB.LAB_NAME, EX.EXMNR_GIVEN_NAME || ' ' || EX.EXMNR_MIDDLE_NAME || ' ' || EX.EXMNR_LAST_NAME)) " + this.sortOrder;
					break;
				case 8:
					this.orderBy = " ORDER BY MED.MED_LAB_TEST_IND " + this.sortOrder;
					break;
				case 9:
					this.orderBy = " ORDER BY MED.BRANCH_ID " + this.sortOrder;
					break;
				case 10:
					this.orderBy = " ORDER BY UPPER(TSP.TEST_DESC) " + this.sortOrder;
					break;
				case 11:
					this.orderBy = " ORDER BY UPPER(STA.STAT_DESC) " + this.sortOrder;
					break;
				case 12:
					this.orderBy = " ORDER BY MED.MED_DATE_RECEIVED " + this.sortOrder;
					break;
				case 13:
					this.orderBy = " ORDER BY UPPER(MED.MED_TEST_ID) " + this.sortOrder;
					break;
				default:
					this.orderBy = " ORDER BY MED.MED_DATE_CONDUCTED " + this.sortOrder +
									", UPPER(DECODE(MED.CL_CLIENT_NUM, NULL, MED.CL_LAST_NAME, CLI.CL_LAST_NAME)) " + this.sortOrder +
									", UPPER(DECODE(MED.CL_CLIENT_NUM, NULL, MED.CL_GIVEN_NAME, CLI.CL_GIVEN_NAME)) " + this.sortOrder;
			}//end-switch
		}
		else if(table.equalsIgnoreCase(IUMConstants.TABLE_AUTO_ASSIGNMENTS)) {
			switch (column) {
				case IUMConstants.SORT_BY_REF_CRITERIA_CODE:
					this.orderBy = " ORDER BY C.AAC_FIELD " + this.sortOrder + ", A.CRITERIA_FIELD_VALUE "+ this.sortOrder;
					break;
				case IUMConstants.SORT_BY_REF_UNDERWRITER:
					this.orderBy = " ORDER BY A.USER_ID " + this.sortOrder;
					break;
			}//end-switch
		}
		else if(table.equalsIgnoreCase(IUMConstants.TABLE_REQUIREMENT_FORMS)) {
			switch (column) {
				case IUMConstants.SORT_BY_REF_FORM_NAME:
					this.orderBy = " ORDER BY RF_NAME " + this.sortOrder;
					break;
				case IUMConstants.SORT_BY_REF_TEMPLATE_FORMAT:
					this.orderBy = " ORDER BY RF_TEMPLATE_FORMAT " + this.sortOrder + ", RF_TEMPLATE_NAME "+ this.sortOrder;
					break;
				case IUMConstants.SORT_BY_REF_TEMPLATE_NAME:
					this.orderBy = " ORDER BY RF_TEMPLATE_NAME" + this.sortOrder + ", RF_TEMPLATE_FORMAT "+ this.sortOrder;
					break;
				case IUMConstants.SORT_BY_REF_ACTIVE:
					this.orderBy = " ORDER BY RF_STATUS" + this.sortOrder;
					break;
			}//end-switch
		}
		LOGGER.info("getOrderBy end");
		return orderBy;
	}

	/**
	 * This method sets the column number.
	 * 
	 * @param i
	 */
	public void setColumn(int i) {
		column = i;
	}

	/**
	 * This method sets the sort order.
	 * 
	 * @param string - Valid values are IUMConstants.ASCENDING and IUMConstants.DESCENDING
	 */
	public void setSortOrder(String string) {
		sortOrder = string;
	}

}
