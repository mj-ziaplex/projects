/*
 * Created on Feb 12, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.util.forms;

import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.ResourceBundle;
import com.slocpi.ium.data.AssessmentRequestData;
import com.slocpi.ium.data.ClientData;
import com.slocpi.ium.data.ClientDataSheetData;
import com.slocpi.ium.data.PolicyRequirementsData;
import com.slocpi.ium.data.RequirementData;
import com.slocpi.ium.data.RequirementFormData;
import com.slocpi.ium.data.RequirementFormFieldData;
import com.slocpi.ium.data.SunLifeOfficeData;
import com.slocpi.ium.data.UserProfileData;
import com.slocpi.ium.data.dao.AssessmentRequestDAO;
import com.slocpi.ium.data.dao.PolicyRequirementDAO;
import com.slocpi.ium.data.dao.RequirementDAO;
import com.slocpi.ium.data.util.DataSourceProxy;
import com.slocpi.ium.interfaces.notification.Attachment;
import com.slocpi.ium.util.DateHelper;
import com.slocpi.ium.util.IUMConstants;
import com.slocpi.ium.util.forms.writer.FormElement;
import com.slocpi.ium.util.forms.writer.FormWriter;

/**
 * @author Nic Decapia
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */


// note: 02012008 - this code was edited by JMA to ensure that all connections are closed  
public class RequirementForm
{
	private AssessmentRequestData policyAR = new AssessmentRequestData();
	
	public Attachment writeForm(final PolicyRequirementsData prd) {
		
		RequirementData requirementForm = new RequirementData();
		RequirementFormData requirementFormData = new RequirementFormData();
		ArrayList formContent = new ArrayList();

		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		// retrieve the AR for the Policy Requirement as reference point of all data
		this.getPolicyAR(prd.getReferenceNumber());
		
		// Determine the Form and the Form Fields
		requirementForm = this.getRequirementForm(prd.getRequirementCode());
		Attachment attachmentForm = null;
		if (IUMConstants.YES.equals(requirementForm.getFormIndicator())
				&& requirementForm.getFormID() != 0) {
			
			requirementFormData
				= this.getRequirementFormData(requirementForm.getFormID());
			ArrayList formFields
				= this.getFormFields(requirementForm.getFormID());
						
			// Fill in values using the Form Fields
			formContent = this.fillContent(formFields, prd);
		
			// Get the file template and path
			ResourceBundle rb = ResourceBundle.getBundle(IUMConstants.IUM_CONFIG);
			String templateName = rb.getString(IUMConstants.IUM_FORMSTEMPLATES_PATH) + requirementFormData.getTemplateName() + ".pdf";
			
			try{
				// Call the generateForm, passing the content and the template
				outputStream = FormWriter.generateForm(formContent, templateName, prd, policyAR, requirementFormData);
				attachmentForm = new Attachment();
				attachmentForm.setAttachment(outputStream);
				attachmentForm.setAttachmentName(requirementFormData.getTemplateName() + ".pdf");
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		
		return (attachmentForm);
	}

	
	/**
	 * @param l
	 * @return
	 */
	private RequirementFormData getRequirementFormData(final long formId) {
		
		
		RequirementFormData rfd = new RequirementFormData();
		try {
			RequirementDAO daoNM = new RequirementDAO();			
			rfd = daoNM.retrieveRequirementForm(formId);
		} catch (Exception e) {
			e.printStackTrace();
		} 
		
		return (rfd);
	}


	private void getPolicyAR(final String referenceNumber) {
		
		
		try {
			AssessmentRequestDAO daoAR = new AssessmentRequestDAO();
			policyAR = daoAR.retrieveRequestDetail(referenceNumber);
		} catch (Exception e){
			e.printStackTrace();
		} 
	}
	
	private RequirementData getRequirementForm(final String requirementCode) {
		
		RequirementData rd = new RequirementData();
		try {
			RequirementDAO daoNM = new RequirementDAO();			
			rd = daoNM.retrieveRequirement(requirementCode);
		} catch (Exception e) {
			e.printStackTrace();
		} 
		
		return (rd);
	}

	private ArrayList getFormFields(final long requirementCode) {
		
		
		ArrayList fields = new ArrayList();
		try {
			
			RequirementDAO daoNM = new RequirementDAO();
			fields = daoNM.retrieveRequirementFormFields(requirementCode);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return (fields);
	}
	
	/**
	 * 
	 * @param fields - fields of to fill in the requirement form
	 * @param policyReq - policy requirement that requires a form to be submitted
	 * @return - an ArrayList of form fields containing the values and coordinates in the template form
	 * 
	 * Notes: the FieldValueType should be defined in the database in the sequence specified in the
	 *        switch sequence below in order to properly determine the type of data needed.
	 * 
	 */
	private ArrayList fillContent(
			final ArrayList fields,
			final PolicyRequirementsData policyReq) {
		
		ArrayList content = new ArrayList();
		Iterator it = fields.iterator(); 
		while (it.hasNext()) {
			
			FormElement fe = new FormElement();
			RequirementFormFieldData ff = (RequirementFormFieldData) it.next();
			fe.setPage(ff.getFieldPage());	// set the page
			// set the coordinates
			fe.setXPos(ff.getFieldPosX());
			fe.setYPos(ff.getFieldPosY());
			
			// TODO: depending on the field type, set the value of the field
			String fieldValue = "";
			
			ClientData client = policyAR.getInsured();
			if(IUMConstants.CLIENT_TYPE_OWNER.equals(policyReq.getClientType())) {
				client = policyAR.getOwner();
			}
			
			switch (ff.getFieldValueType()) {
				case 0:	// Insured Name (LastName, FirstName, MiddleName)		
					fieldValue = formatName(client.getLastName(),client.getGivenName(),client.getMiddleName());
					break;
				case 1:	// Client No.
					fieldValue = policyReq.getClientId(); 
					break;
				case 2: // Policy No.
					if (null != policyAR.getPolicySuffix()) {
						fieldValue = policyReq.getReferenceNumber().concat(policyAR.getPolicySuffix());
					} else {
						fieldValue = policyReq.getReferenceNumber();
					}
					break;
				case 3: //Birthdate
					fieldValue = DateHelper.format(client.getBirthDate(),"dd/MMM/yyyy");
					break;
				case 4: //other legal name									
					fieldValue =  formatName(client.getOtherLastName(),client.getOtherGivenName(), client.getOtherMiddleName());
					break;
				case 5: //Agent Name
					UserProfileData agent = policyAR.getAgent();
					fieldValue = formatName(agent.getLastName(),agent.getFirstName(),agent.getMiddleName());					
					break;
				case 6: //New Business Office
					SunLifeOfficeData sod= policyAR.getBranch();
					fieldValue = sod.getOfficeName();					
					break;
				case 7: //Amount Coverage					
					fieldValue = String.valueOf(policyAR.getAmountCovered());					
					break;
				case 8:	// Owner Name (LastName, FirstName, MiddleName)
					ClientData owner = policyAR.getOwner();
					fieldValue = formatName(owner.getLastName(),owner.getGivenName(),owner.getMiddleName());
					break;
				case 9:	// height in feet
					ClientDataSheetData clientData = policyAR.getClientDataSheet();					
					fieldValue = String.valueOf(clientData.getMortalityRating().getHeightInFeet());
					break;
				case 10: // height in inches
					clientData = policyAR.getClientDataSheet();					
					fieldValue = String.valueOf(clientData.getMortalityRating().getHeightInInches());
					break;
				case 11: // height in inches
					clientData = policyAR.getClientDataSheet();					
					fieldValue = String.valueOf(clientData.getMortalityRating().getWeight());
					break;
			}
			
			fe.setValue(fieldValue);
			content.add(fe);
		}
		
		return (content);
	}
	
	private String formatName (
			final String lastName,
			final String firstName,
			final String middleName) {
		
		StringBuffer name = new StringBuffer();
		if (lastName != null && !"".equals(lastName)) {
			name.append(lastName);
			name.append(", ");
		}
		
		if (firstName != null && !"".equals(firstName)) {
			name.append(firstName);
			name.append(" ");
		}
		
		if (middleName != null && !"".equals(middleName)) {
			name.append(middleName);
			name.append(" ");
		}
		
		return name.toString();
	}
	
	
}
