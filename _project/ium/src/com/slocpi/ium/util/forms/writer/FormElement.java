/*
 * Created on Feb 4, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.util.forms.writer;

/**
 * @author nic.decapia
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class FormElement {
	
	private String value;
	private int page;
	private float posX;
	private float posY;
	
	/**
	 * @return
	 */
	public String getValue() {
		return value;
	}

	/**
	 * @param i
	 */
	public void setXPos(float i) {
		posX = i;
	}

	/**
	 * @param i
	 */
	public void setYPos(float i) {
		posY = i;
	}

	/**
	 * @param string
	 */
	public void setValue(String string) {
		value = string;
	}

	/**
	 * @return
	 */
	public int getPage() {
		return page;
	}

	/**
	 * @param i
	 */
	public void setPage(int i) {
		page = i;
	}

	/**
	 * @return
	 */
	public float getPosX() {
		return posX;
	}

	/**
	 * @return
	 */
	public float getPosY() {
		return posY;
	}

}
