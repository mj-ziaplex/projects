/*
 * Created on Feb 4, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.util.forms.writer;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.ResourceBundle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.lowagie.text.Document;
import com.lowagie.text.Element;
import com.lowagie.text.Image;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.Barcode128;
import com.lowagie.text.pdf.Barcode39;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfImportedPage;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.PdfTemplate;
import com.lowagie.text.pdf.PdfWriter;
import com.slocpi.ium.data.AssessmentRequestData;
import com.slocpi.ium.data.PolicyRequirementsData;
import com.slocpi.ium.data.RequirementFormData;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.IUMConstants;
import com.slocpi.ium.util.StringHelper;

/**
 * @author nic.decapia
 * 
 *         To change the template for this generated type comment go to
 *         Window>Preferences>Java>Code Generation>Code and Comments
 */
public class FormWriter {
	private static final Logger LOGGER = LoggerFactory.getLogger(FormWriter.class);
	
	public static ByteArrayOutputStream generateForm(ArrayList content,
			String templatePath) {
		
		
		LOGGER.info("generateForm start 1");
		ByteArrayOutputStream streamPDF = new ByteArrayOutputStream();
		Document doc = new Document();
		PdfWriter docWriter = null;

		try {
			
			PdfReader reader = new PdfReader(templatePath);
			int n = reader.getNumberOfPages();

			docWriter = PdfWriter.getInstance(doc, streamPDF);
			doc.open();
			PdfContentByte cb = docWriter.getDirectContent();

			for (int i = 1; i <= n; i++) {
				
				Rectangle psize = reader.getPageSize(i);
				float width = psize.getWidth();
				float height = psize.getHeight();

				PdfImportedPage pip = docWriter.getImportedPage(reader, i);
				cb.addTemplate(pip, 0f, 0f); // width, height);

				ArrayList pageElements = new ArrayList();
				Iterator it1 = content.iterator();
				while (it1.hasNext()) {
					
					FormElement fe = (FormElement) it1.next();
					if (fe.getPage() == i) {
						pageElements.add(fe);
					}
				}

				Iterator it2 = pageElements.iterator();
				while (it2.hasNext()) {
					
					FormElement fe = (FormElement) it2.next();
					PdfTemplate template = cb.createTemplate(width, height);
					template.beginText();
					
					BaseFont bf = BaseFont.createFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
					template.setFontAndSize(bf, 10);
					template.setTextMatrix(fe.getPosX(), fe.getPosY());
					template.showText(fe.getValue());
					template.endText();
					cb.addTemplate(template, 10, 10);
				}

				if (i < n) {
					doc.newPage();
				}
			}
			
			Barcode128 formInfoBarcode = new Barcode128();
			Barcode128 policyIdBarcode = new Barcode128();
			formInfoBarcode.setSize(19);
			formInfoBarcode.setCode("");
			policyIdBarcode.setSize(19);
			policyIdBarcode.setCode("");
			doc.add(formInfoBarcode.createImageWithBarcode(cb, null, null));
			doc.add(policyIdBarcode.createImageWithBarcode(cb, null, null));
			doc.close();

		} catch (IOException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
		} catch (Exception e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
		}
		
		LOGGER.info("generateForm end 1");
		return (streamPDF);
	}

	public static ByteArrayOutputStream generateForm(
			ArrayList content,
			String templatePath,
			PolicyRequirementsData prd,
			AssessmentRequestData ard,
			RequirementFormData rfd) {
		
		LOGGER.info("generateForm start 2");
		ByteArrayOutputStream streamPDF = new ByteArrayOutputStream();
		Document doc = new Document();
		PdfWriter docWriter = null;

		try {

			if (templatePath.indexOf("Secretarys Certificate") > -1) {
				
				docWriter = PdfWriter.getInstance(doc, streamPDF);
				doc.open();
				templatePath = "C:\\jdev10g\\j2ee\\home\\applications\\ium\\ium-web\\reqtforms\\Secretarys Certificate.pdf";
				File file = new File(templatePath);

				doc.add(new Paragraph(org.apache.commons.io.FileUtils.readFileToString(file)));
			} else {

				PdfReader reader = new PdfReader(templatePath);
				int n = reader.getNumberOfPages();

				docWriter = PdfWriter.getInstance(doc, streamPDF);
				
				doc.open();
				PdfContentByte cb = docWriter.getDirectContent();
				Rectangle psize = null;

				for (int i = 1; i <= n; i++) {
					
					psize = reader.getPageSize(i);
					float width = psize.getWidth();
					float height = psize.getHeight();

					PdfImportedPage pip = docWriter.getImportedPage(reader, i);
					cb.addTemplate(pip, 0f, 30);
					
					ArrayList pageElements = new ArrayList();
					Iterator it1 = content.iterator();
					
					while (it1.hasNext()) {
						
						FormElement fe = (FormElement) it1.next();
						
						if (fe.getPage() == i) {
							pageElements.add(fe);
						}
					}
					
					Iterator it2 = pageElements.iterator();
					
					while (it2.hasNext()) {
						
						FormElement fe = (FormElement) it2.next();
						PdfTemplate template = cb.createTemplate(width, height);
						template.beginText();
						
						BaseFont bf = BaseFont.createFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
						// assign the font to the template, include the size
						template.setFontAndSize(bf, 10);
						// set the cursor at the position
						template.setTextMatrix(fe.getPosX(), fe.getPosY());
						// write the value
						template.showText(fe.getValue());
						template.endText();
						// add the template to the document
						cb.addTemplate(template, 10, 40);
					}
					
					
					if (i == 1) {
						// place barcode on the first page
						// added barcodes for Form Info and Policy ID
						Barcode39 formInfoBarcode = new Barcode39();
						Barcode39 policyIdBarcode = new Barcode39();
						formInfoBarcode.setSize(8);
						formInfoBarcode.setBarHeight(20);
						formInfoBarcode.setStartStopText(false);
						formInfoBarcode.setBaseline(-1);
						formInfoBarcode.setTextAlignment(Element.ALIGN_LEFT);
						
						
						String reqType = "";
						if (prd.getLevel().equalsIgnoreCase(IUMConstants.LEVEL_CLIENT)) {
							reqType = prd.getClientType();
						} else {
							reqType = prd.getLevel();
						}
						
						String wmsFormId = "";
						if (rfd.getWmsFormId() != null) {
							wmsFormId = rfd.getWmsFormId();
						}
						
						
						formInfoBarcode.setCode(wmsFormId);
						policyIdBarcode.setSize(8);
						policyIdBarcode.setBarHeight(20);
						policyIdBarcode.setStartStopText(false);
						policyIdBarcode.setBaseline(-1);
						policyIdBarcode.setTextAlignment(Element.ALIGN_LEFT);

						if (ard.getPolicySuffix() != null) {
							policyIdBarcode.setCode(
									prd.getReferenceNumber()
									+ ard.getPolicySuffix()
									+ reqType
									+ prd.getRequirementId());
						} else {
							policyIdBarcode.setCode(
									prd.getReferenceNumber()
									+ reqType
									+ prd.getRequirementId());
						}

						if (!isFormToSuppress(rfd.getFormName())) {

							Image formInfoBarcodeImage = formInfoBarcode.createImageWithBarcode(cb, null, null);
							formInfoBarcodeImage.setAbsolutePosition(width - 600, 5);
							if(null != rfd.getWmsFormId()) {
								if(rfd.getWmsFormId().trim().length() > 1) {
									if(!rfd.getTemplateName().contains("SLGFI")) {
										doc.add(formInfoBarcodeImage);
									}
								}
							}
						}

						Image policyIdBarcodeImage = policyIdBarcode.createImageWithBarcode(cb, null, null);
						if (!isFormToSuppress(rfd.getFormName())) {
							policyIdBarcodeImage.setAbsolutePosition(width - 225, 5);
						} else {
							policyIdBarcodeImage.setAbsolutePosition(width - 225, 40);
						}
						
						doc.add(policyIdBarcodeImage);
						
					}
					
					if (i < n){
						doc.newPage();
					}
				}
			}

			doc.close();

		} catch (Exception e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
		}
		LOGGER.info("generateForm end 2");
		return (streamPDF);
	}

	private static boolean isFormToSuppress(String wmsFormId) {
		
		LOGGER.info("isFormToSuppress start");
		String[] formCodeArray = getFormsToSuppress();
		
		for (int i = 0; i < formCodeArray.length; i++) {
			LOGGER.info(formCodeArray[i]);
			LOGGER.info(wmsFormId);
			if (formCodeArray[i].equalsIgnoreCase(wmsFormId)) {
		
				return true;
			}
		}
		
		LOGGER.info("isFormToSuppress end");
		return false;
	}

	private static String[] getFormsToSuppress() {
		
		LOGGER.info("getFormsToSuppress start");
		
		ResourceBundle rb = ResourceBundle.getBundle("requirementcodes");
		String formIds = rb.getString(IUMConstants.SUPPRESS_LEFT_BARCODE);
		String[] formCodeArray = StringHelper.split(formIds, ",");
		
		LOGGER.info("getFormsToSuppress end");
		
		return formCodeArray;
	}
}
