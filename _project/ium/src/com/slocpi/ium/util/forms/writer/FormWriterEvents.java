/*
 * Created on Mar 25, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.util.forms.writer;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.lowagie.text.BadElementException;
import com.lowagie.text.Cell;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.Phrase;
import com.lowagie.text.Rectangle;
import com.lowagie.text.Table;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfPageEventHelper;
import com.lowagie.text.pdf.PdfTemplate;
import com.lowagie.text.pdf.PdfWriter;
import com.slocpi.ium.reports.IUMReportConstants;
import com.slocpi.ium.util.CodeHelper;

/**
 * @author Nic Decapia
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class FormWriterEvents extends PdfPageEventHelper
{
	private static final Logger LOGGER = LoggerFactory.getLogger(FormWriterEvents.class);
	// store final number of pages in a template
	PdfTemplate template;
	
	// content byte fo the writer
	PdfContentByte cb;

	
	// base font to use for header/footer
	BaseFont bf = null;
	
	Calendar now;
	
	// columnheaders
	private ArrayList columnTitles = new ArrayList();
	private int colTitleFontSize = 8;
	private int[] columnWidths = {};	
	
	/**
	 * 
	 */
	public FormWriterEvents()
	{
		// TODO Auto-generated constructor stub
		super();
		now = Calendar.getInstance();
	}

	// override the onEndPage method
	/* (non-Javadoc)
	 * @see com.lowagie.text.pdf.PdfPageEvent#onEndPage(com.lowagie.text.pdf.PdfWriter, com.lowagie.text.Document)
	 */
	public void onEndPage(PdfWriter arg0, Document arg1)
	{
		LOGGER.info("onEndPage start");
		float position = 0;
		if (arg1.getPageSize().getRotation() == 0 ) {
			position = 33;
		}
		else {
			position = 50;
		}		
		
		now = Calendar.getInstance();
		SimpleDateFormat dateFormat = new SimpleDateFormat("MMMMM d, yyyy h:mm a");
		
		cb.beginText();
		cb.setFontAndSize(bf, 6);
		cb.setTextMatrix(position, 20);
		cb.showText(dateFormat.format(now.getTime()).toString());
		cb.endText();
		
		int pageNum = arg0.getPageNumber();
		String text = "Page " + pageNum + " of ";
		
		if (arg1.getPageSize().getRotation() == 0 ) {
			position = 550;
		}
		else {
			position = 710;
		}		
		
		float len = bf.getWidthPoint(text, 6);
		cb.beginText();
		cb.setFontAndSize(bf, 6);
		cb.setTextMatrix(position, 20);
		cb.showText(text);
		cb.endText();
		
		cb.addTemplate(template, position + len, 20);

		LOGGER.info("onEndPage end");
	}

	/* (non-Javadoc)
	 * @see com.lowagie.text.pdf.PdfPageEvent#onCloseDocument(com.lowagie.text.pdf.PdfWriter, com.lowagie.text.Document)
	 */
	public void onCloseDocument(PdfWriter arg0, Document arg1)
	{
		
		LOGGER.info("onCloseDocument start");
		
		template.beginText();
		template.setFontAndSize(bf, 6);
		template.showText(String.valueOf(arg0.getPageNumber() - 1));
		template.endText();
		
		LOGGER.info("onCloseDocument end");
	}

	/* (non-Javadoc)
	 * @see com.lowagie.text.pdf.PdfPageEvent#onOpenDocument(com.lowagie.text.pdf.PdfWriter, com.lowagie.text.Document)
	 */
	public void onOpenDocument(PdfWriter arg0, Document arg1)
	{
		LOGGER.info("onOpenDocument start");
		try
		{
			bf = BaseFont.createFont(BaseFont.HELVETICA_OBLIQUE, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
			cb = arg0.getDirectContent();
			template = cb.createTemplate(50, 50);		
		}
		catch (DocumentException e)
		{
			LOGGER.error(CodeHelper.getStackTrace(e));	
		}
		catch (IOException e)
		{
			LOGGER.error(CodeHelper.getStackTrace(e));	
		}
		LOGGER.info("onOpenDocument end");
	}

	/* (non-Javadoc)
	 * @see com.lowagie.text.pdf.PdfPageEvent#onStartPage(com.lowagie.text.pdf.PdfWriter, com.lowagie.text.Document)
	 */
	public void onStartPage(PdfWriter arg0, Document arg1)
	{
		LOGGER.info("onStartPage start");
		try
		{
			Table hdr = new Table(2, 2);
			hdr.setBorder(0);
			hdr.setPadding(1);
			hdr.setSpacing(1);
				
			Font font = new Font(Font.HELVETICA, 6, Font.ITALIC);
				
			// App Title cell
			Phrase pAppTitle = new Phrase (IUMReportConstants.APPLICATION_NAME, font);
			Cell cAppTitle = new Cell(pAppTitle);
			cAppTitle.setBorder(Rectangle.NO_BORDER);
			cAppTitle.setVerticalAlignment(Element.ALIGN_TOP);
			cAppTitle.setHorizontalAlignment(Element.ALIGN_LEFT);
			hdr.addCell(cAppTitle);
		
			// Company Name cell
			Phrase pCompanyName = new Phrase (IUMReportConstants.COMPANY_NAME, font);
			Cell cCompanyName = new Cell(pCompanyName);
			cCompanyName.setBorder(Rectangle.NO_BORDER);
			cCompanyName.setVerticalAlignment(Element.ALIGN_TOP);
			cCompanyName.setHorizontalAlignment(Element.ALIGN_RIGHT);
			hdr.addCell(cCompanyName);

			arg1.add(hdr);
			if (arg0.getPageNumber() > 1) {
				arg1.add(writeColumnTitles());
				arg1.add(writeBlankRow());
			} 

		}
		catch (BadElementException e)
		{
			LOGGER.error(CodeHelper.getStackTrace(e));	
		} catch (DocumentException e)
		{
			LOGGER.error(CodeHelper.getStackTrace(e));	
		}
		
		LOGGER.info("onStartPage end");
	}

	/**
	 * @param i
	 */
	public void setColTitleFontSize(int i)
	{
		colTitleFontSize = i;
	}

	/**
	 * @param list
	 */
	public void setColumnTitles(ArrayList list)
	{
		columnTitles = list;
	}

	/**
	 * @param is
	 */
	public void setColumnWidths(int[] is)
	{
		columnWidths = is;
	}

	public Table writeColumnTitles() throws BadElementException
	{
		LOGGER.info("writeColumnTitles start");
		
		Table rptColumns = new Table(columnTitles.size(), 1);
		rptColumns.setBorder(Rectangle.BOX);
		rptColumns.setPadding(3);
		rptColumns.setSpacing(0);
				
		try
		{
			rptColumns.setWidths(columnWidths);
		}
		catch (DocumentException e)
		{
			LOGGER.error(CodeHelper.getStackTrace(e));	
		}
		for (int i=0; i<columnTitles.size(); i++) {
			if (columnTitles.get(i) instanceof Cell) {
				rptColumns.addCell((Cell) columnTitles.get(i));	
			}
			else {
				rptColumns.insertTable((Table) columnTitles.get(i));
			}
		}
		
		LOGGER.info("writeColumnTitles end");
		return (rptColumns);
	}
	
	/**
	 * @return
	 */
	public Table writeBlankRow() throws BadElementException
	{
	
		LOGGER.info("writeBlankRow start");
		
		Table tblBlank = new Table(1);
		tblBlank.setBorder(Rectangle.NO_BORDER);
		Cell cTotalBar = new Cell();
		cTotalBar.setBorder(Rectangle.NO_BORDER);
		tblBlank.addCell(cTotalBar);
		
		LOGGER.info("writeBlankRow end");
		return (tblBlank);		
	}

}
