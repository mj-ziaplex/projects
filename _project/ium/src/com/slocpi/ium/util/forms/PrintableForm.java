/*
 * Created on Mar 19, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.util.forms;

import java.awt.Color;
import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Date;
import com.lowagie.text.BadElementException;
import com.lowagie.text.Cell;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.PageSize;
import com.lowagie.text.Phrase;
import com.lowagie.text.Rectangle;
import com.lowagie.text.Table;
import com.lowagie.text.pdf.PdfWriter;
import com.slocpi.ium.reports.IUMReportConstants;
import com.slocpi.ium.util.DateHelper;
import com.slocpi.ium.util.ValueConverter;
import com.slocpi.ium.util.forms.writer.FormWriterEvents;

/**
 * @author Nic Decapia
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class PrintableForm extends Document
{
	
	private int[] columnValueWidths;
	private ByteArrayOutputStream baos;
	private Document doc;
	
	private String reportTitle = new String();
	private ArrayList filters = new ArrayList();
	private ArrayList columnTitles = new ArrayList();
	private ArrayList reportContent = new ArrayList(); // collection of Cells that make up a row
	private Table reportTable = null; // content cells get added here automatically
	private int[] columnWidths = {};
	private boolean writeEndBar = false;
	private int colTitleFontSize = 8;
	
	public static final int ALIGN_COL_LEFT = Element.ALIGN_LEFT;
	public static final int ALIGN_COL_RIGHT = Element.ALIGN_RIGHT;
	public static final int ALIGN_COL_CENTER = Element.ALIGN_CENTER;
	
	public static final int COL_TYPE_GROUP = Rectangle.BOTTOM;
	public static final int COL_TYPE_NORMAL = Rectangle.NO_BORDER;
	public static final int COL_TYPE_TOTAL = Rectangle.UNDEFINED;
	public static final int COL_TYPE_END = Rectangle.TOP;
	
	public static final int PAGE_PORTRAIT = 0;
	public static final int PAGE_LANDSCAPE = 1;  
		
	public PrintableForm(int pageOrientation) throws DocumentException {
		baos = new ByteArrayOutputStream();
		Rectangle orientation = PageSize.LETTER;
		if (pageOrientation == PAGE_LANDSCAPE) orientation = orientation.rotate();
		doc = new Document(orientation, -36, -36, 36, 36);
	}

    public ByteArrayOutputStream writeReport(ArrayList content) throws DocumentException {
		PdfWriter writer = PdfWriter.getInstance(doc, baos);
		FormWriterEvents evnt = new FormWriterEvents();
		evnt.setColTitleFontSize(colTitleFontSize);
		evnt.setColumnTitles(columnTitles);
		evnt.setColumnWidths(columnWidths);
		
		writer.setPageEvent(evnt);
		
		
		doc.open();
		
		doc.add(writeReportParms()); //	report-specific header
		doc.add(evnt.writeColumnTitles()); // column titles
		doc.add(evnt.writeBlankRow()); // actually it's a table
		doc.add(writeContent()); // write content
		if (writeEndBar) doc.add(writeGrandTotalBar());
		doc.close();		
				
		return (baos);
    }
	


	/**
	 * @return
	 */
	private Cell writeReportTitle() throws BadElementException
	{
		// TODO Auto-generated method stub
		
		Font rptTitleFont = new Font (Font.HELVETICA, 6, Font.BOLD, Color.black);
		Phrase pTitle = new Phrase (reportTitle, rptTitleFont);

		Cell cTitle = new Cell(pTitle);
		cTitle.setBorder(Rectangle.NO_BORDER);
		cTitle.setVerticalAlignment(Element.ALIGN_TOP);
		cTitle.setHorizontalAlignment(Element.ALIGN_LEFT);
		cTitle.setHeader(true);
		
		return(cTitle);		
	}

	/**
	 * @return
	 */
	private Table writeReportParms() throws BadElementException
	{
		// TODO Auto-generated method stub
		int rowcount = 1; // for report title
		rowcount += filters.size();
		Table rptFilters = new Table(1, rowcount);
		rptFilters.setBorder(0);
		rptFilters.setPadding(1);
		rptFilters.setSpacing(0);
		

		rptFilters.addCell(writeReportTitle());
		Font rptParmFont = new Font (Font.HELVETICA, 6, Font.NORMAL, Color.black);
		
		for (int i=0; i<filters.size(); i++) {
			Phrase pParm = new Phrase ((String) filters.get(i), rptParmFont);
			
			Cell cParm = new Cell(pParm);
			cParm.setBorder(Rectangle.NO_BORDER);
			cParm.setVerticalAlignment(Element.ALIGN_TOP);
			cParm.setHorizontalAlignment(Element.ALIGN_LEFT);
			cParm.setHeader(true);
			rptFilters.addCell(cParm);			
		}
		return (rptFilters);
	}

	public void defineColumnTitle(String colName) throws BadElementException {
		
		Font font = new Font(Font.HELVETICA, colTitleFontSize, Font.BOLD, Color.black);
		Phrase pColumnName = new Phrase (colName, font);
		
		Cell cColumn = new Cell(pColumnName);
		cColumn.setBorder(Rectangle.BOX);
		cColumn.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cColumn.setHorizontalAlignment(Element.ALIGN_CENTER);
		cColumn.setHeader(true);
		
		columnTitles.add(cColumn);
	}

	public void defineParameter(String date1, String date2) {
		String parm = new String();
		
		Date startDate = DateHelper.parse(date1, IUMReportConstants.REPORT_PARAM_DATE_FORMAT);
		Date endDate = DateHelper.parse(date2, IUMReportConstants.REPORT_PARAM_DATE_FORMAT);
		
		parm += DateHelper.format(startDate, IUMReportConstants.REPORT_DATE_FORMAT) + " to " + DateHelper.format(endDate, IUMReportConstants.REPORT_DATE_FORMAT);
						
		this.defineParameter(IUMReportConstants.PARAMETER_PERIOD, parm);
	}
	
	public void defineParameter(int parmType, String value) {
		String parm = new String();
		
		switch (parmType) {
			case IUMReportConstants.PARAMETER_PERIOD:
				parm = IUMReportConstants.REPORT_PARAMETER_PERIOD;
				break;				
			case IUMReportConstants.PARAMETER_TYPE:
				parm = IUMReportConstants.REPORT_PARAMETER_TYPE;
				break;
			case IUMReportConstants.PARAMETER_STATUS:
				parm = IUMReportConstants.REPORT_PARAMETER_AR_STATUS;
				break;
			case IUMReportConstants.PARAMETER_COUNT:
				parm = IUMReportConstants.REPORT_PARAMETER_COUNT_APP;
				break;
		}

		parm += value;		

		filters.add(parm);		
	}
	
	public void defineContentValue(String value, int align, int columnType) throws BadElementException {
		int fontStyle = Font.NORMAL;
		if (columnType == COL_TYPE_GROUP) fontStyle = Font.BOLD;
		Font font = new Font(Font.HELVETICA, 8, fontStyle, Color.black);

		value = ValueConverter.nullToString(value);
		Phrase pColValue = new Phrase (value, font);
		
		Cell cContent = new Cell(pColValue);
		cContent.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cContent.setHorizontalAlignment(align);

		cContent.setBorder(columnType);
		reportContent.add(cContent);
		
	}
	

	/**
	 * @param string
	 */
	public void setReportTitle(String string)
	{
		reportTitle = string;
	}


	/**
	 * @param is
	 */
	public void setColumnWidths(int[] is)
	{
		columnWidths = is;
		columnValueWidths = columnWidths;	// by default, set the column width of the content same as the title
	}
	
	public void setColumnValueWidths(int[] is) {
		columnValueWidths = is;
	}

	/**
	 * 
	 */
	private Table writeContent() throws DocumentException
	{
		// TODO Auto-generated method stub
		Table rptContent = new Table(columnValueWidths.length);
		try
		{
			rptContent.setBorder(Rectangle.NO_BORDER);
			rptContent.setPadding(2);
			rptContent.setSpacing(0);		

			rptContent.setWidths(columnValueWidths);
			for (int i=0; i<reportContent.size(); i++) {
				rptContent.addCell((Cell) reportContent.get(i));
			}			
		}
		catch (DocumentException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
				
		return (rptContent);
	}

	/**
	 * 
	 */
	public Table writeGrandTotalBar() throws BadElementException
	{
		Table totalBar = new Table(1, 1);
		totalBar.setBorder(Rectangle.NO_BORDER);
		Cell cTotalBar = new Cell();
		cTotalBar.setBorder(Rectangle.TOP);
		totalBar.addCell(cTotalBar);
		return (totalBar);
		
	}

	/**
	 * 
	 */
	public void writeEndBar()
	{
		// TODO Auto-generated method stub
		writeEndBar = true;		
	}

	/**
	 * 
	 */
	public void writeEndContent() throws BadElementException
	{
		// TODO Auto-generated method stub
		for (int i=0; i<columnValueWidths.length;i++) {
			this.defineContentValue("", PrintableForm.ALIGN_COL_LEFT, PrintableForm.COL_TYPE_GROUP);
		}
	}


	/**
	 * @param i
	 */
	public void setColTitleFontSize(int i)
	{
		colTitleFontSize = i;
	}

	/**
	 * @param string
	 * @param childCols
	 */
	public void defineComplexColumnTitle(String colName, ArrayList childCols) throws BadElementException
	{
		// TODO Auto-generated method stub
		Font font = new Font(Font.HELVETICA, colTitleFontSize, Font.BOLD, Color.black);
		Table complexTitle = new Table(childCols.size(), 2);
		complexTitle.setBorder(Rectangle.NO_BORDER);
		
		Phrase pColumnName = new Phrase (colName, font);
		Cell cMainTitle = new Cell(pColumnName);
		cMainTitle.setBorder(Rectangle.NO_BORDER);
		cMainTitle.setHorizontalAlignment(Rectangle.ALIGN_CENTER);
		cMainTitle.setVerticalAlignment(Rectangle.ALIGN_MIDDLE);
		cMainTitle.setColspan(childCols.size());
		complexTitle.addCell(cMainTitle);
		
		for (int i=0; i<childCols.size(); i++) {
			Phrase childColumnName = new Phrase ((String) childCols.get(i), font);
			Cell childTitle = new Cell(childColumnName);
			childTitle.setBorder(Rectangle.NO_BORDER);
			childTitle.setHorizontalAlignment(Rectangle.ALIGN_CENTER);
			childTitle.setVerticalAlignment(Rectangle.ALIGN_MIDDLE);
			complexTitle.addCell(childColumnName);
		}
		
		columnTitles.add(complexTitle);		
	}

}
