package com.slocpi.ium.util;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * A utility class that simplifies the most common String processing / 
 * manipulation in java.
 * @author 			Pointwest Tech Corp (c)
 * Mar 3, 2005
 */
public class StringHelper {

	private static final Logger LOGGER = LoggerFactory.getLogger(StringHelper.class);
	private StringHelper() {
	}
    
    
	/**
	 * Checks if a <code>String</code> object contains only spaces or is null.
	 * 
	 * @param s	<code>String</code> object to check
	 * @return	<code>true</code> if s is empty, otherwise returns <code>false
	 * </code>
	 * 
	 */
	public static boolean isEmpty(String s) {
		
		if (s == null) {
			return true;
		} else if (s.trim().equals("")) {
			return true;
		} else {
			return false;
		}
	}
    
	/**
	 * Checks if a <code>String</code> object contains only spaces or is null.
	 * 
	 * @param s	<code>String</code> object to check
	 * @return	<code>false</code> if s is empty, otherwise returns <code>true
	 * </code>
	 * 
	 */
	public static boolean isNotEmpty(String s) {
		return !isEmpty(s);
	}

    
	/**
	 * Checks if two <code>String</code> objects have exactly the same values
	 * 
	 * @param s1
	 * @param s2
	 * @return
	 * Jun 27, 2005
	 * 
	 */
	public static boolean isEqual(String s1, String s2) {
		
		if(StringHelper.isEmpty(s1)) {
			s1 = "";
		}
		
		return s1.equals(s2);
	}
	
	/**
	 * Checks if two String objects are not equal
	 * @param s1
	 * @param s2
	 * @return
	 */
	public static boolean isNotEqual(String s1, String s2){
		return !isEqual(s1,s2);
	}
    
	/**
	 * This method generates a new <code>String</code> object, generated by padding 
	 * an input number with an specified character if the input number's digits are 
	 * less than the specified length. 
	 * 
	 * @param number	the number to pad
	 * @param length	the specified length of the resulting String
	 * @param padChar	the character to be used when padding
	 * @return	the padded number as a <code>String</code>
	 * 
	 */
	public static String pad(int number, int length, char padChar){
		return StringHelper.pad(Integer.toString(number), length, padChar);
	}
    
	public static String pad(String string, int length, char padChar) {
		
		StringBuffer sb = new StringBuffer(string);
		while (sb.length() < length) {
			sb.insert(0, padChar);
		}
		
		return sb.toString(); 
	}    

	public static String padEnd(String string, int length, char padChar) {
		
		
		StringBuffer sb = new StringBuffer(string);
		while (sb.length() < length) {
			sb.insert(string.length(), padChar);
		}
		
		return sb.toString(); 
	}    
    
	public static boolean toBoolean(String value) {
		
		if (isNotEmpty(value) && value.equalsIgnoreCase("on")) {
			return true;
		} else {
			return false;
		}
	}
    
	/**
	 * Generates an <code>ArrayList</code> of values parsed from a 
	 * <code>String</code> input, using a <code>StringTokenizer</code>
	 * and a specified separator.
	 * 
	 * @param input
	 * @param separator
	 * @return
	 * Jun 27, 2005
	 * 
	 */
	public static ArrayList stringToArrayList(String input, String separator){
		
		ArrayList list = new ArrayList();
		StringTokenizer tokenizer = new StringTokenizer(input, separator);
		while (tokenizer.hasMoreElements()){
			list.add((String)tokenizer.nextElement());
		}
		
		return list;
	}

	public static String replace(String sourceString, 
								 String oldString, 
								 String newString) {
		
		StringBuffer sb = new StringBuffer();
        
		for (int i = 0; i < sourceString.length(); i++) {
			if (i + oldString.length() > sourceString.length() ) {
				sb.append(sourceString.charAt(i));
			} else if (sourceString.substring(i, i + oldString.length())
					.equals(oldString)) {
				sb.append(newString);
				i += oldString.length() - 1; 
			} else { 
				sb.append(sourceString.charAt(i));
			}
		}
		
		return sb.toString();
	}

	public static String decodeIntoXml(String decoded) {
		
		
		decoded = StringHelper.replace(decoded, "&", "&amp;");
		decoded = StringHelper.replace(decoded, "<", "&lt;");
		decoded = StringHelper.replace(decoded, ">", "&gt;");
		decoded = StringHelper.replace(decoded, "'", "&apos;");
		decoded = StringHelper.replace(decoded, "\"", "&quot;");
		
		return decoded;
	}
	
	
	public static String escapeQueryString(String url) {
		
		
		url = StringHelper.replace(url, "<", "&lt;");
		url = StringHelper.replace(url, ">", "&gt;");
		url = StringHelper.replace(url, "'", "&apos;");
		
		return url;
	}

	public static String arrayToString(String [] arr, String separator){
		
		
		StringBuffer result = new StringBuffer();
		if(arr != null){
			if (arr.length > 0){
				result.append(arr[0]);
				for (int i=1; i<arr.length; i++){
					result.append(separator);
					result.append(arr[i]);
				}
			}			
		}
		
		return result.toString();
	}
	
	public static String[] split(String string, String delimiters) {
		
		
		List tokenList = new ArrayList();
		StringTokenizer st = new StringTokenizer(string, delimiters);
		while (st.hasMoreTokens()) {
			tokenList.add(st.nextToken());       
		}
		String [] tokenArray = new String[tokenList.size()];
		for (int i =0; i < tokenArray.length; i++) {
			tokenArray[i] = (String) tokenList.get(i);
		}
		
		return tokenArray;
	}

	public static String[] split(String string) {
		return split(string, " ");
	}

	/**
	 * Determines if string parameter is an integer
	 *  
	 * @param s
	 * @return
	 */
	public static boolean isInteger(String s) {
		
		try {
			Integer.parseInt(s.trim());
		} catch (NumberFormatException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			return false;
		}
		
		return true;
	}

	/**
	 * Determines if string parameter is numeric
	 *  
	 * @param s
	 * @return
	 */
	public static boolean isNumeric(String s) {
		
		try {
			Double.parseDouble(s.trim());
		} catch (NumberFormatException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			return false;
		}
		
		return true;
	}
	
	/**
	 * Formats the string to be a double with the indicated number of decimal places
	 * 
	 * @param string
	 * @param places
	 * @return
	 */
	public static String formatDecimal(String string, int places) {
		
		int point = string.indexOf(".");
		if (point > -1) {
			String s = string.substring(0, point+1);
			String dec = string.substring(point+1, string.length());
			if (dec.length() < places) {
				dec = padEnd(dec, places, '0');
			} else {
				dec = dec.substring(0, places);
			}
			
			return s + dec;
		} else {
			
			return string + padEnd(".", places + 1, '0');
		}
	}

	/**
	 * Returns a trimmed version of the given string, 
	 * or returns the specified default string if null
	 *  
	 * @param field
	 * @param defaultString
	 * @return
	 */
	public static String formatField(String field, String defaultString) {
		
		if (isEmpty(field)) {
			return defaultString;
		} else {
			return field.trim();
		}
	}

	/**
	 * Eliminates the decimal places of a Double if inconsequential
	 * 
	 * @param d
	 * @return
	 */
/*	public static String formatDouble(Double d) {
		
		int i = d.intValue();
		if (i == d.doubleValue()) {
			return i + "";
		} else {
			return d.toString();
		}
	}*/
    
	/**
	 * Converts the contents of an arraylist to a string
	 * @param list
	 * @param separator
	 * @return
	 */
	/*public static String arrayListToString(ArrayList list, String separator){
		
		int idx = 0;
		String [] strArr = new String[list.size()];
		for(Iterator itr = list.iterator();itr.hasNext();idx++){
			String data = (String) itr.next();
			strArr[idx] = data;
		}
		
		return arrayToString(strArr,separator);
	}*/
    
	public static boolean isAlpha(char value){
		return (value >= 'a' && value <= 'z' || value >= 'A' && value <= 'Z');
	}
    
	public static boolean isNumeric(char value){
		return ((value >= '0') && (value <= '9'));
	}
    
	public static boolean isSpecialCharacter(char value, char specialChar){
		return (value==specialChar);
	}
	
	public static String [] stringToStringArray(String convertString, String delimiter){
		
		String arrString [] = null;
		if (isNotEmpty(convertString)){
			StringTokenizer st = new StringTokenizer(convertString, delimiter);
			int count = st.countTokens();
			arrString = new String [count];
			int index = 0;
			while(st.hasMoreTokens()){
				arrString[index] = st.nextToken();
				index++;
			}
		}
		
		return arrString;
	}	
	
	/*public static String[] arrayListToStringArray(ArrayList list){
		
		String tempString = arrayListToString(list,"/");
		String[] resultArr = stringToStringArray(tempString, "/");
		
		return resultArr;	
	}	*/

	public static int countInstanceOfString(String str, String stringToFind){
		
		int count = 0;
		if (isNotEmpty(str)){
			ArrayList list = stringToArrayList(str, stringToFind);
			if (list != null){
				count = list.size();
				if (count != 0){
					count--;
				}
			}
		}
		
		return count;
	}	 
}//EOF - StringHelper.java
