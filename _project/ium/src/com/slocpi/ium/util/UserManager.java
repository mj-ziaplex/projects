/*
 * Created on Jan 12, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.util;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Random;
import java.util.ResourceBundle;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.slocpi.ium.IUMException;
import com.slocpi.ium.data.AccessTemplateData;
import com.slocpi.ium.data.AccessTemplateDetailsData;
import com.slocpi.ium.data.UserAccessData;
import com.slocpi.ium.data.UserData;
import com.slocpi.ium.data.UserPasswords;
import com.slocpi.ium.data.UserProfileData;
import com.slocpi.ium.data.dao.AccessDAO;
import com.slocpi.ium.data.dao.SecurityDAO;
import com.slocpi.ium.data.dao.UserDAO;
import com.slocpi.ium.data.util.DataSourceProxy;
import com.slocpi.ium.interfaces.IUMInterfaceException;
import com.slocpi.ium.interfaces.notification.IUMNotificationException;
import com.slocpi.ium.interfaces.notification.NotificationMessage;
import com.slocpi.ium.interfaces.notification.Notifier;
import com.slocpi.ium.service.ldap.LDAPService;
import com.slocpi.ium.underwriter.UnderWriterException;
import com.slocpi.ium.workflow.NotificationMessages;

/**
 * @author daguila
 * 
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class UserManager {
	private static final Logger LOGGER = LoggerFactory.getLogger(UserManager.class);
	
	public boolean isValidUser(String userId, String password) throws IUMException, IUMInterfaceException {
		
		LOGGER.info("isValidUser start");
		boolean resIUM = false;
		boolean resLDAP = false;
		boolean res = false;
		
		try {

			UserDAO ud = new UserDAO();
			resIUM = ud.isUserValid(userId.toUpperCase());
			LOGGER.info("User Id is in IUM database-->" + resIUM);
			
			LDAPService ls = LDAPService.getInstance();
			resLDAP = ls.isValidUser(userId, password);
			LOGGER.info("User Id is in LDAP-->" + resLDAP);
			
			res = (resIUM && resLDAP);

		} catch (Exception sqlE) {
			LOGGER.error(CodeHelper.getStackTrace(sqlE));
			throw new IUMException(sqlE.getMessage());
		}
		
		LOGGER.info("isValidUser end");
		return res;
	}

	public UserData retrieveUserDetails(String userId) throws IUMException {
		
		LOGGER.info("retrieveUserDetails start");
		UserData user = null;
		
		try {
			
			UserDAO ud = new UserDAO();
			user = new UserData();
			user.setProfile(ud.selectUserProfile(userId));
			user.setProcessPreference(ud.selectProcessPreference(userId));
			user.setRoles(ud.getUserRoles(userId));
		} catch (SQLException sqlE) {
			LOGGER.error(CodeHelper.getStackTrace(sqlE));
			throw new IUMException(sqlE);
		} 
		LOGGER.info("retrieveUserDetails end");
		return user;

	}
// Launch IUM Enhancement - Andre Ceasar Dacanay - start
	public UserData retrieveUserDetails(String userId, HttpServletRequest request) throws IUMException {
		
		LOGGER.info("retrieveUserDetails start");
		UserData user = null;
		
		try {
			UserDAO ud = new UserDAO();
			user = new UserData();
			user.setProfile(ud.selectUserProfile(userId, request));
			user.setProcessPreference(ud.selectProcessPreference(userId));
			user.setRoles(ud.getUserRoles(userId));
		} catch (SQLException sqlE) {
			LOGGER.error(CodeHelper.getStackTrace(sqlE));
			throw new IUMException(sqlE);
		} 
		LOGGER.info("retrieveUserDetails end");
		return user;

	}

	public UserPasswords retrieveUserPasswords(String userId)
			throws IUMException {
		
		LOGGER.info("retrieveUserPasswords start");
		UserPasswords upswd = null;
		
		try {
			
			UserDAO ud = new UserDAO();
			upswd = ud.getUserPasswords(userId);
		} catch (SQLException sqlE) {
			LOGGER.error(CodeHelper.getStackTrace(sqlE));
			throw new IUMException(sqlE);
		} 
		LOGGER.info("retrieveUserPasswords end");
		return upswd;

	}

	/**
	 * @return
	 * @throws Exception
	 */
	public ArrayList getAccessTemplates() throws Exception {
		
		LOGGER.info("getAccessTemplates start");
		ArrayList templates = null;
		
		try {
			SecurityDAO securityDAO = new SecurityDAO();
			templates = securityDAO.selectTemplates();
		} catch (SQLException sqlE) {
			LOGGER.error(CodeHelper.getStackTrace(sqlE));
			
		} 
		LOGGER.info("getAccessTemplates end");
		return templates;
	}

	/**
	 * @param templateID
	 * @return
	 * @throws Exception
	 */
	public AccessTemplateData getAccessTemplate(long templateID)
			throws Exception {

		LOGGER.info("getAccessTemplate start");
		AccessTemplateData accessTemplate = new AccessTemplateData();
		
		try {
			SecurityDAO securityDAO = new SecurityDAO();
			accessTemplate = securityDAO.selectTemplate(templateID);
		} catch (SQLException sqlE) {
			LOGGER.error(CodeHelper.getStackTrace(sqlE));
		}
		LOGGER.info("getAccessTemplate end");
		return accessTemplate;
	}

	public void createAccessTemplate(AccessTemplateData accessTemplateData)
			throws IUMException {

		LOGGER.info("createAccessTemplate start");
		
		try {
			
			SecurityDAO securityDAO = new SecurityDAO();
			if (!securityDAO.isAccessTemplateExist(accessTemplateData)) {
				securityDAO.insertAccessTemplate(accessTemplateData);
			} else {
				throw new UnderWriterException("Template already exist.");
			}
		
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMException(e);
		} 
		LOGGER.info("createAccessTemplate end");
	}

	public void updateAccessTemplate(AccessTemplateData workFunctionData)
			throws IUMException {
		
		LOGGER.info("updateAccessTemplate start");
		
		try {
			SecurityDAO securityDAO = new SecurityDAO();

			if (!securityDAO.isAccessTemplateExist(workFunctionData)) {
				securityDAO.updateAccessTemplate(workFunctionData);
			} else {
				throw new UnderWriterException("Duplicate template.");
			}
			
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMException(e);
		} 
		LOGGER.info("updateAccessTemplate end");
	}

	public ArrayList getUserPageAccess(String userID, long pageID)
			throws IUMException {
		
		LOGGER.info("getUserPageAccess start");
		ArrayList list = new ArrayList();
		
		try {
			SecurityDAO dao = new SecurityDAO();
			list = dao.selectUserPageAccess(userID, pageID);
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMException(e);
		} 
		LOGGER.info("getUserPageAccess end");
		return list;
	}

	public ArrayList getUserAccess(String userID) throws IUMException {
		
		LOGGER.info("getUserAccess start");
		ArrayList list = new ArrayList();
		
		try {
			SecurityDAO dao = new SecurityDAO();
			list = dao.selectUserPageAccess(userID);
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMException(e);
		}
		LOGGER.info("getUserAccess end");
		return list;
	}

	public void saveUserAccess(ArrayList userAccessList) throws IUMException {
		
		LOGGER.info("saveUserAccess start");
		
		try {
			SecurityDAO dao = new SecurityDAO();
			dao.saveUserAccess(userAccessList);
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMException(e);
		} 
		LOGGER.info("saveUserAccess end");
	}

	public ArrayList getAccess() throws IUMException {
		
		LOGGER.info("getAccess start");
		ArrayList list = new ArrayList();
		Connection conn = null;
		try {
			conn = new DataSourceProxy().getConnection();
			AccessDAO dao = new AccessDAO(conn);
			list = dao.retrieveAccess();
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMException(e);
		} finally {
			closeConnection(conn);
		}
		LOGGER.info("getAccess end");
		return list;
	}

	public void applyTemplate(String userID, long templateID, String appliedBy)
			throws IUMException {
		
		LOGGER.info("applyTemplate start");
		ArrayList userAccessList = new ArrayList();
		
		try {
			SecurityDAO securityDAO = new SecurityDAO();
			ArrayList templateDetails = securityDAO
					.selectTemplateDetails(templateID);
			for (int i = 0; i < templateDetails.size(); i++) {
				AccessTemplateDetailsData templateDetail = (AccessTemplateDetailsData) templateDetails
						.get(i);
				UserAccessData userAccess = securityDAO.selectUserPageAccess(
						userID, templateDetail.getPageAccess().getPageID(),
						templateDetail.getPageAccess().getAccessID());
				if (userAccess == null) {
					userAccess = new UserAccessData();
					userAccess.setUserID(userID);
					userAccess.setPageID(templateDetail.getPageAccess()
							.getPageID());
					userAccess.setAccessID(templateDetail.getPageAccess()
							.getAccessID());
					userAccess.setAccess(templateDetail.hasAccess());
					userAccess.setCreatedBy(appliedBy);
					userAccess.setCreatedDate(DateHelper
							.sqlTimestamp(new java.util.Date()));
					userAccessList.add(userAccess);
				} else {
					if (!userAccess.hasAccess() && templateDetail.hasAccess()) {
						userAccess.setAccess(templateDetail.hasAccess());
						userAccess.setUpdatedBy(appliedBy);
						userAccess.setUpdatedDate(DateHelper
								.sqlTimestamp(new java.util.Date()));
						userAccessList.add(userAccess);
					}
				}
			}
			securityDAO.saveUserAccess(userAccessList);
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMException(e);
		} 
		LOGGER.info("applyTemplate end");
	}

	public void revokeTemplates(String userID, ArrayList templateIDs,
			String appliedBy) throws IUMException {
		
		LOGGER.info("revokeTemplates start");
		
		ArrayList userAccessList = new ArrayList();
		try {
			
			SecurityDAO securityDAO = new SecurityDAO();
			for (int i = 0; i < templateIDs.size(); i++) {
				long templateID = ((Long) templateIDs.get(i)).longValue();
				ArrayList templateDetails = securityDAO
						.selectTemplateDetails(templateID);
				for (int j = 0; j < templateDetails.size(); j++) {
					AccessTemplateDetailsData templateDetail = (AccessTemplateDetailsData) templateDetails
							.get(j);
					UserAccessData userAccess = securityDAO
							.selectUserPageAccess(userID, templateDetail
									.getPageAccess().getPageID(),
									templateDetail.getPageAccess()
											.getAccessID());
					if (userAccess != null) {
						if (userAccess.hasAccess()
								&& templateDetail.hasAccess()) {
							if (!securityDAO.hasOtherAccess(userID,
									templateDetail.getPageAccess()
											.getPageAccessId(), templateID)) {
								userAccess.setAccess(false);
								userAccess.setUpdatedBy(appliedBy);
								userAccess.setUpdatedDate(DateHelper
										.sqlTimestamp(new java.util.Date()));
								userAccessList.add(userAccess);
							}
						}
					}
				}

				securityDAO.saveUserAccess(userAccessList);
			}
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMException(e);
		}
		LOGGER.info("revokeTemplates end");
	}

	public ArrayList getWorkFunction() throws Exception {
		
		LOGGER.info("getWorkFunction start");
		ArrayList workFunctions = null;
		
		try {
			SecurityDAO securityDAO = new SecurityDAO();
			workFunctions = securityDAO.selectPageAccess();
		} catch (SQLException sqlE) {
			LOGGER.error(CodeHelper.getStackTrace(sqlE));
		} 
		LOGGER.info("getWorkFunction end");
		return workFunctions;
	}

	public void saveWorkFunctions(ArrayList list) throws IUMException {
		
		LOGGER.info("saveWorkFunctions start");
		
		try {
			SecurityDAO dao = new SecurityDAO();
			dao.savePageAccess(list);
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMException(e);
		} 
		LOGGER.info("saveWorkFunctions end");
	}

	private void closeConnection(Connection conn) throws IUMException {
		
		
		if (null != conn) {
			try {
				conn.close();
			} catch (SQLException sqlE) {
				LOGGER.error(CodeHelper.getStackTrace(sqlE));
				throw new IUMException(sqlE);
			}
		}
		
	}

	public void resetPassword(UserProfileData user) throws IUMException {
		
		LOGGER.info("resetPassword start");
		String Uppers = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		String Lowers = "abcdefghijklmnopqrstuvwxyz";
		String Numbers = "0123456789";
		String strings = Uppers + Numbers + Lowers;
		boolean newUser = false;
		String role = "None";
		int passlength = 8;

		String passwrd = "";
		Random r = new Random();
		for (int i = 0; i < passlength; i++) {
			char c = strings.charAt(Math.round(r.nextFloat() * 61));
			passwrd = passwrd + c;
		}

	
		try {
	
			UserDAO usrDAO = new UserDAO();

			user = usrDAO.selectUserProfile(user.getUserId());

			if (user.getRole() != null) {
				role = user.getRole();
			}

			if (user.getPassword() == null || user.getPassword().trim() == "") {
				newUser = true;
			}
			user.setPassword(passwrd);
			usrDAO.resetPassword(user);
			//usrDAO.update1LoginInd(user.getUserId(), false);
			//usrDAO.updateUserPasswords(user);

			if (user.getEmailAddr() != null) {
				NotificationMessage notificationMsg = new NotificationMessage();
				String content = "";
				ArrayList arr = new ArrayList();
				arr.add(user.getEmailAddr());
				notificationMsg.setRecipients(arr);
				
				ResourceBundle rb = ResourceBundle
						.getBundle(IUMConstants.IUM_CONFIG);
				String defaultSender = rb
						.getString(IUMConstants.IUM_SMTP_ACCNT);
				notificationMsg.setSender(defaultSender);

				if (newUser) {
					notificationMsg.setSubject("IUM: New User ID");
					content = "A new User ID has been defined for \nName: "
							+ user.getFirstName()
							+ " "
							+ user.getLastName()
							+ "\nUser ID: "
							+ user.getUserId()
							+ "\nPassword: "
							+ user.getPassword()
							+ "\nRole(s): "
							+ role
							+ "\nYou can now start using IUM using this User ID, Password and Access. You may change your password once you have logged-in. If there are any problems with this access, please contact the IUM System Administrator.";

				} else {
					notificationMsg.setSubject("IUM: Password Reset");
					content = "Your password has been reset. \nUser ID:  "
							+ user.getUserId()
							+ "\nNew Password: "
							+ user.getPassword()
							+ "\nPlease use this password the next time you login IUM. You may change your password once you have logged-in. If there are any problems with this access, please contact the IUM System Administrator.";
				}

				notificationMsg.setContent(content);

				Notifier notifier = new Notifier();
				NotificationMessages msgs = new NotificationMessages();
				msgs.setMailMessage(notificationMsg);
				notifier.sendMessages(msgs);
			}

		} catch (SQLException sqlE) {
			LOGGER.error(CodeHelper.getStackTrace(sqlE));
			throw new IUMException(sqlE);
		} catch (IUMNotificationException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMException(e);
		} 
		LOGGER.info("resetPassword end");
	}

	public void insertUserProfileData(UserProfileData user) throws IUMException {
		
		LOGGER.info("insertUserProfileData start");
		
		try {
			
			UserDAO dao = new UserDAO();
			dao.insertUserProfile(user);
			UserPasswords upswd = new UserPasswords();
			upswd.setUserid(user.getUserId());
			upswd.setPassword1(user.getPassword());
			upswd.setPassword2(user.getPassword());
			upswd.setPassword3(user.getPassword());
			upswd.setPassword4(user.getPassword());
			upswd.setPassword5(user.getPassword());
			dao.insertUserPasswords(upswd);
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMException(e);
		}
		LOGGER.info("insertUserProfileData end");
	}

	public void updateUserProfileData(UserProfileData user) throws IUMException {
		
		LOGGER.info("updateUserProfileData start");
		
		try {
			UserDAO dao = new UserDAO();
			dao.updateUserProfile(user);
			
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMException(e);
		} 
		
		LOGGER.info("updateUserProfileData end");
	}

	public void updateDefaultRole(String role, String userId, String updatedBy)
			throws IUMException {
		
		LOGGER.info("updateDefaultRole start");
		
		try {
			
			UserDAO dao = new UserDAO();
			dao.updateUserDefaultRole(role, userId, updatedBy);
			
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMException(e);
		} 
		LOGGER.info("updateDefaultRole end");

	}

	public HashMap getUserPrivileges(String userID) throws IUMException {
		
		LOGGER.info("getUserPrivileges start");
		HashMap map = new HashMap();
		ArrayList userAccess = this.getUserAccess(userID);
		
		if (userAccess.size() > 0) {
			HashMap pageAccess = new HashMap();
			String pageID = "";
			for (int i = 0; i < userAccess.size(); i++) {
				UserAccessData data = (UserAccessData) userAccess.get(i);
				if (!pageID.equals("")
						&& !pageID.equals(String.valueOf(data.getPageID()))) {
					map.put(pageID, pageAccess);
					pageAccess = new HashMap();
				}

				pageID = String.valueOf(data.getPageID());
				pageAccess.put(String.valueOf(data.getAccessID()), new Boolean(
						data.hasAccess()));

			}
			if (pageAccess.size() > 0) {
				map.put(pageID, pageAccess);
			}
		}
		LOGGER.info("getUserPrivileges end");
		return map;
	}

	public HashMap getUserPrivileges(
			final String userID,
			final String access) throws IUMException {
		
		LOGGER.info("getUserPrivileges start");
		HashMap map = new HashMap();
		ArrayList userAccess = this.getUserAccess(userID);
		
		if (userAccess.size() > 0) {
			
			HashMap pageAccess = new HashMap();
			String pageID = "";
			
			for (int i = 0; i < userAccess.size(); i++) {
				
				UserAccessData data = (UserAccessData) userAccess.get(i);
				if (!pageID.equals("")
						&& !pageID.equals(String.valueOf(data.getPageID()))) {
					
					map.put(pageID, pageAccess);
					pageAccess = new HashMap();
				}
				pageID = String.valueOf(data.getPageID());

				if (access.equalsIgnoreCase("R")) {
					if (pageID.equalsIgnoreCase(IUMConstants.PAGE_ASSESSMENT_DETAILS)
							|| pageID.equalsIgnoreCase(IUMConstants.PAGE_REQUEST_DETAILS)
							|| pageID.equalsIgnoreCase(IUMConstants.PAGE_REQUIREMENTS_KO)
							|| pageID.equalsIgnoreCase(IUMConstants.PAGE_CDS)
							|| pageID.equalsIgnoreCase(IUMConstants.PAGE_UW_ASSESSMENT)
							|| pageID.equalsIgnoreCase(IUMConstants.PAGE_DOCTORS_NOTES)) {
						
						if (String.valueOf(data.getAccessID()).equalsIgnoreCase(IUMConstants.ACC_INQUIRE)) {
							
							pageAccess.put(String.valueOf(data.getAccessID()), new Boolean(data.hasAccess()));
						} else {
							
							pageAccess.put(String.valueOf(data.getAccessID()), new Boolean(false));
						}
					} else {
						
						pageAccess.put(String.valueOf(data.getAccessID()), new Boolean(false));
					}
				} else {
					
					if (pageID.equalsIgnoreCase(IUMConstants.PAGE_ASSESSMENT_DETAILS)) {

						if (String.valueOf(data.getAccessID()).equalsIgnoreCase(IUMConstants.ACC_MAINTAIN)
								|| String.valueOf(data.getAccessID()).equalsIgnoreCase(IUMConstants.ACC_INQUIRE)) {
							
							pageAccess.put(String.valueOf(data.getAccessID()), new Boolean(data.hasAccess()));
						} else {
							
							pageAccess.put(String.valueOf(data.getAccessID()), new Boolean(false));
						}
					} else if (pageID.equalsIgnoreCase(IUMConstants.PAGE_REQUIREMENTS_KO)
							|| pageID.equalsIgnoreCase(IUMConstants.PAGE_REQUEST_DETAILS)
							|| pageID.equalsIgnoreCase(IUMConstants.PAGE_CDS)
							|| pageID.equalsIgnoreCase(IUMConstants.PAGE_UW_ASSESSMENT)
							|| pageID.equalsIgnoreCase(IUMConstants.PAGE_DOCTORS_NOTES)) {

						pageAccess.put(String.valueOf(data.getAccessID()), new Boolean(data.hasAccess()));
					} else {
						
						pageAccess.put(String.valueOf(data.getAccessID()), new Boolean(false));
					}
				}

			}
			
			if (pageAccess.size() > 0) {
				
				map.put(pageID, pageAccess);
			}
		}
		LOGGER.info("getUserPrivileges end");
		return map;
	}

	

	public void update1LoginInd(String userID) throws IUMException {
		
		LOGGER.info("update1LoginInd start");
		
		try {
			UserDAO dao = new UserDAO();
			dao.update1LoginInd(userID, true);
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMException(e);
		} 
		LOGGER.info("update1LoginInd end");
	}

	public long getPswdDays(String userID) throws IUMException {
		
		LOGGER.info("getPswdDays start");
	
		long dif = 0;
		try {
			
			UserDAO dao = new UserDAO();
			UserProfileData upd = dao.selectUserProfile(userID);
			Date tmpDate = upd.getPswd_update_date();
			Date curDate = new Date();
			if (tmpDate != null) {
				dif = DateHelper.getDaysDifference(curDate, tmpDate);
				LOGGER.debug("days dif " + dif);
			} else {
				dif = -1;
			}
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMException(e);
		} 
		LOGGER.debug("********days dif = " + dif);
		LOGGER.info("getPswdDays end");
		return dif;
	}



	public void addRoleToUser(String userId, String userAdminId, String roleCode)
			throws IUMException {

		LOGGER.info("addRoleToUser start");
		
		try {
			SecurityDAO securityDAO = new SecurityDAO();
			securityDAO.addRoleToUser(userId, roleCode);
			long templateId = securityDAO.getTemplateId(roleCode);
			if (templateId == 0) {
				throw new IUMException("No template Id for role code: "
						+ roleCode);
			} else {
				applyTemplate(userId, templateId, userAdminId);
			}
		} catch (SQLException sqlE) {
			LOGGER.error(CodeHelper.getStackTrace(sqlE));
		}
		LOGGER.info("addRoleToUser end");
	}

	public void removeRolesFromUser(String userId, String userAdminId,
			ArrayList roles) throws IUMException {

		LOGGER.info("removeRolesFromUser start");
		ArrayList templateIds = new ArrayList();
		
		try {
			
			SecurityDAO securityDAO = new SecurityDAO();

			for (int i = 0; i < roles.size(); i++) {
				String roleCode = (String) roles.get(i);
				securityDAO.removeRoleFromUser(userId, roleCode);
				long templateId = securityDAO.getTemplateId(roleCode);
				templateIds.add(new Long(templateId));
			}
		} catch (SQLException sqlE) {
			LOGGER.error(CodeHelper.getStackTrace(sqlE));
			throw new IUMException(sqlE.getMessage());
		} 
		revokeTemplates(userId, templateIds, userAdminId);
		LOGGER.info("removeRolesFromUser end");
	}

	public void lockUser(String userID) throws IUMException {
		
		LOGGER.info("lockUser start");

		try {
			
			UserDAO dao = new UserDAO();
			dao.updateLockIndicator(userID, true);
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMException(e);
		} 
		LOGGER.info("lockUser end");
	}

}
