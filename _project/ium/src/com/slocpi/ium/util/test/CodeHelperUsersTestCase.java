/*
 * Created on Jan 18, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.util.test;

import java.util.ArrayList;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.IUMConstants;

/**
 * @author daguila
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class CodeHelperUsersTestCase extends TestCase {

	public CodeHelperUsersTestCase(java.lang.String testName) {        
		super(testName);    
	}        

	public static Test suite() {        
		TestSuite suite = new TestSuite(CodeHelperUsersTestCase.class);                
		return suite;    
	}	
	
	public static void main(java.lang.String[] args) {   
		junit.textui.TestRunner.run(suite());    
	}	
	
	public void testUsersAgent() throws Exception {
		boolean res = false;
		CodeHelper ch = new CodeHelper();
		ArrayList list = (ArrayList)ch.getCodeValues("users",IUMConstants.ROLES_AGENTS);
		
		/*
		 * for now the agents have 4 users;
		 */
		 
		 if ((list != null) && (list.size() == 4)) {
		 	res = true;
		 }
		assertEquals("List of agents are incorrect.",true,res);
	}
	
	public void testUsersUnderwriter() throws Exception {
		boolean res = false;
		CodeHelper ch = new CodeHelper();
		ArrayList list = (ArrayList)ch.getCodeValues("users",IUMConstants.ROLES_UNDERWRITER);
		
		/*
		 * for now the underwriter have 10 users;
		 */
		 
		 if ((list != null) && (list.size() == 10)) {
			res = true;
		 }
		assertEquals("List of agents are incorrect.",true,res);		
	}
}
