package com.slocpi.ium.util;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class AutoLogin{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(AutoLogin.class);
	public static final String USERNAME = "XOXO";
	/*public static final String PASSWORD = "OXOX"; */
	public static final int ONE_HOUR = 60 * 60;
	public static final int PERSIST_UNTIL_CLOSED = -1;
	
	public static final String AUTO = "autoLogin";
	public static final String MANUAL = "manual";
	
	private String uname = "";
	private String pwd = "";
	private boolean auto = false;
	
	
	private AutoLogin() {}
	
	public AutoLogin(String uname, String pwd) {
		super();
		this.uname = uname;
		this.pwd = pwd;
	}
	
	/**
	 * Create AutoLogin object ONLY IF the value of
	 * param is equal to "autoLogin".
	 * If not, returns null.
	 * 
	 * @param param
	 * @return
	 */
	public static AutoLogin getAutoLogin(String param) {
		
		LOGGER.info("getAutoLogin start");
		if (param.equalsIgnoreCase(AUTO)) {
			AutoLogin autoLogin = new AutoLogin();
			autoLogin.setAuto(true);
			LOGGER.info("getAutoLogin end");
			return autoLogin;
		}
		LOGGER.info("getAutoLogin end");
		return null;
	}
	
	/**
	 * Create cookie to store username and password for automatic login.
	 * 
	 * @param request
	 * @param response
	 */
	/*public void createAuthCookie(HttpServletRequest request, HttpServletResponse response) {
		
		LOGGER.info("createAuthCookie start");
		response.setContentType("text/html");
		Cookie cookieUsername = createCookie(USERNAME, uname);
		response.addCookie(cookieUsername);
		
		Cookie cookiePassword = createCookie(PASSWORD, pwd);
		response.addCookie(cookiePassword);
		LOGGER.info("createAuthCookie end");
	}
	
	*//**
	 * Remove cookies for automatic user login.
	 * 
	 * @param response
	 *//*
	public static void removedAuthCookie(HttpServletResponse response) {
		
		LOGGER.info("removedAuthCookie start");
		
		response.setContentType("text/html");
		AutoLogin autoLogin = new AutoLogin();
		autoLogin.deleteCookie(USERNAME, response);
		autoLogin.deleteCookie(PASSWORD, response);
		
		LOGGER.info("removedAuthCookie end");
	}
	*/
	/**
	 * Delete cookie by name
	 * 
	 * @param name
	 * @param response
	 */
	private void deleteCookie(String name, HttpServletResponse response) {
		
		LOGGER.info("deleteCookie start");
		response.setContentType("text/html");
		Cookie cookie = new Cookie(name, "");
		cookie.setMaxAge(0);
		response.addCookie(cookie);
		LOGGER.info("deleteCookie end");
	}
	
	/**
	 * Get the value of the specified cookie name.
	 * 
	 * @param name
	 * @param request
	 * @return
	 */
	public static String getCookieValue(String name, HttpServletRequest request) {
		
		LOGGER.info("getCookieValue start");
		Cookie[] cookies = request.getCookies();
		if (cookies != null) {
			for (int i = 0; i < cookies.length; i++) {
				if (name.equals(cookies[i].getName())) {
					LOGGER.info("getCookieValue end");
					return cookies[i].getValue();
				}
			}
			LOGGER.info("getCookieValue end");
			return "";
		} else {
			LOGGER.info("getCookieValue end");
			return null;
		}
	}
	
	/**
	 * Create a cookie by specified name and value.
	 * 
	 * @param name
	 * @param value
	 * @return
	 */
	private Cookie createCookie(String name, String value) {
		
		LOGGER.info("createCookie start");
		Cookie cookie = new Cookie(name, value);
		cookie.setMaxAge(ONE_HOUR * 8);
		LOGGER.info("createCookie end");
		return cookie;
	}
	
	// Getters and Setters
	
	public String getPwd() {
		return pwd;
	}


	public void setPwd(String pwd) {
		this.pwd = pwd;
	}


	public String getUname() {
		return uname;
	}


	public void setUname(String uname) {
		this.uname = uname;
	}

	public boolean isAuto() {
		return auto;
	}

	public void setAuto(boolean auto) {
		this.auto = auto;
	}
	
}
