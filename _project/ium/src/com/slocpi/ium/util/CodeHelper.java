package com.slocpi.ium.util;

// java package
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.ResourceBundle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.slocpi.ium.data.ClientData;
import com.slocpi.ium.data.StatusData;
import com.slocpi.ium.data.SunLifeOfficeData;
import com.slocpi.ium.data.UserProfileData;
import com.slocpi.ium.data.dao.AutoAssignmentCriteriaCHDao;
import com.slocpi.ium.data.dao.BranchCHDao;
import com.slocpi.ium.data.dao.ClientDAO;
import com.slocpi.ium.data.dao.ClientTypeCHDao;
import com.slocpi.ium.data.dao.CodeHelperDao;
import com.slocpi.ium.data.dao.DepartmentCHDAO;
import com.slocpi.ium.data.dao.DocumentTypeCHDao;
import com.slocpi.ium.data.dao.ExaminerDAO;
import com.slocpi.ium.data.dao.HolidayDAO;
import com.slocpi.ium.data.dao.LOBCHDao;
import com.slocpi.ium.data.dao.LaboratoryDAO;
import com.slocpi.ium.data.dao.MIBActionCHDao;
import com.slocpi.ium.data.dao.MIBImpairmentsCHDao;
import com.slocpi.ium.data.dao.MIBLetterCHDao;
import com.slocpi.ium.data.dao.MIBNumberCHDao;
import com.slocpi.ium.data.dao.MedicalRecordStatusCHDao;
import com.slocpi.ium.data.dao.NotificationTemplateCHDao;
import com.slocpi.ium.data.dao.RequestStatusCHDao;
import com.slocpi.ium.data.dao.RequirementCodeCHDao;
import com.slocpi.ium.data.dao.RequirementStatusCHDao;
import com.slocpi.ium.data.dao.RolesCHDAO;
import com.slocpi.ium.data.dao.SectionCHDAO;
import com.slocpi.ium.data.dao.StatusCHDao;
import com.slocpi.ium.data.dao.TestProfileCHDao;
import com.slocpi.ium.data.dao.UserCHDao;
import com.slocpi.ium.data.dao.UserDAO;
import com.slocpi.ium.data.util.DataSourceProxy;


public class CodeHelper {
	private static final Logger LOGGER = LoggerFactory.getLogger(CodeHelper.class);
	private CodeHelperDao cols;
	private String filters;
	
	public Collection getCodeValues(String codeSetName, String _filters) {
		
		filters = _filters;
		return getCodeValues(codeSetName);	
	}

	public Collection getCodeValues(String codeSetName) {
		
		Collection list = new ArrayList();
		Connection conn = null;
		
		try {
			
			conn = new DataSourceProxy().getConnection();
			if (codeSetName.equalsIgnoreCase("requestStatus")) {		
				list = new RequestStatusCHDao(conn).getCodeValues(filters);
			}
			else if (codeSetName.equalsIgnoreCase("requirementStatus")) {
				list = new RequirementStatusCHDao().getCodeValues(filters);
			}
			else if (codeSetName.equalsIgnoreCase("medicalRecordStatus")) {
				list = new MedicalRecordStatusCHDao(conn).getCodeValues(filters);
			}
			else if (codeSetName.equalsIgnoreCase("requirementCode")) {
				cols = new RequirementCodeCHDao(conn); 
				list = cols.getCodeValues();
			}
			else if (codeSetName.equalsIgnoreCase("lob")) {
				cols = new LOBCHDao(conn); 
				list =  cols.getCodeValues();
			}
			else if (codeSetName.equalsIgnoreCase("branch")) {
				cols = new BranchCHDao(conn); 
				list =  cols.getCodeValues();
			}
			else if (codeSetName.equalsIgnoreCase("clientType")) {
				cols = new ClientTypeCHDao(conn); 
				list = cols.getCodeValues();
			}
			else if (codeSetName.equalsIgnoreCase("mibAction")) {
				cols = new MIBActionCHDao(conn); 
				list = cols.getCodeValues();
			}
			else if (codeSetName.equalsIgnoreCase("mibImpairment")) {
				cols = new MIBImpairmentsCHDao(); 
				list = cols.getCodeValues();
			}			
			else if (codeSetName.equalsIgnoreCase("mibLetter")) {
				cols = new MIBLetterCHDao(conn); 
				list = cols.getCodeValues();
			}
			else if (codeSetName.equalsIgnoreCase("mibNumber")) {
				cols = new MIBNumberCHDao(conn); 
				list = cols.getCodeValues();
			}			
			else if (codeSetName.equalsIgnoreCase("department")) {
				cols = new DepartmentCHDAO(conn); 
				list = cols.getCodeValues();
			}
			else if (codeSetName.equalsIgnoreCase("section")) {
				cols = new SectionCHDAO(conn); 
				list = cols.getCodeValues();
			}
			else if (codeSetName.equalsIgnoreCase("document")) {
				cols = new DocumentTypeCHDao(conn); 
				list = cols.getCodeValues();
			}
			else if (codeSetName.equalsIgnoreCase("users")) {
				list = new UserCHDao().getCodeValues(filters);
			}			
			else if (codeSetName.equalsIgnoreCase("branchAgents")) {
				list = new UserCHDao().getCodeValues(codeSetName, filters);
			}
			else if (codeSetName.equalsIgnoreCase("notificationTemplates")) {
				cols = new NotificationTemplateCHDao(conn); 
				list = cols.getCodeValues();
			}		
			else if (codeSetName.equalsIgnoreCase("roles")) {
				cols = new RolesCHDAO(conn);
				list = cols.getCodeValues();
			}		
			else if (codeSetName.equalsIgnoreCase("testProfiles")) {
				cols = new TestProfileCHDao(conn);
				list = cols.getCodeValues();			
			}
			else if (codeSetName.equalsIgnoreCase("laboratoryTestProfiles")) {
				list = new TestProfileCHDao(conn).getCodeValues(filters);			
			}
			else if (codeSetName.equalsIgnoreCase("holidayYear")) {
				HolidayDAO dao = new HolidayDAO(conn);
				list = dao.retrieveYears();
			}
			else if (codeSetName.equalsIgnoreCase("autoAssignmentCriteria")) {
				cols = new AutoAssignmentCriteriaCHDao(conn);
				list = cols.getCodeValues();			
			}
			else {
				list = null;	
			}
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
		} finally {
			closeConnection(conn);
		}
		
		return list;
	}
	
	private void closeConnection(Connection conn) {		
		
		if (conn!=null) {
			try {
				conn.close();
			}
			catch(SQLException e){
				LOGGER.error(CodeHelper.getStackTrace(e));
			}
		}
		
	}


	public Collection getCodeValue(String codeSetName, String codeValue) {
		
		Collection list = new ArrayList();
		Connection conn = null;
		try {
			conn = new DataSourceProxy().getConnection();

			if (codeSetName.equalsIgnoreCase("requestStatus")) {
				cols = new RequestStatusCHDao(conn); 
				list = cols.getCodeValue(codeValue);
			}
			else if (codeSetName.equalsIgnoreCase("requirementStatus")) {
				cols = new RequirementStatusCHDao(); 
				list = cols.getCodeValue(codeValue);
			}
			else if (codeSetName.equalsIgnoreCase("medicalRecordStatus")) {
				cols = new MedicalRecordStatusCHDao(conn); 
				list = cols.getCodeValue(codeValue);
			}
			else if (codeSetName.equalsIgnoreCase("lob")) {
				cols = new LOBCHDao(conn); 
				list = cols.getCodeValue(codeValue);
			}
			else if (codeSetName.equalsIgnoreCase("branch")) {
				cols = new BranchCHDao(conn); 
				list = cols.getCodeValue(codeValue);
			}
			else if (codeSetName.equalsIgnoreCase("clientType")) {
				cols = new ClientTypeCHDao(conn); 
				list = cols.getCodeValue(codeValue);
			}			
			else if (codeSetName.equalsIgnoreCase("mibAction")) {
				cols = new MIBActionCHDao(conn); 
				list = cols.getCodeValue(codeValue);
			}
			else if (codeSetName.equalsIgnoreCase("mibImpairment")) {
				cols = new MIBImpairmentsCHDao(); 
				list = cols.getCodeValue(codeValue);
			}			
			else if (codeSetName.equalsIgnoreCase("mibLetter")) {
				cols = new MIBLetterCHDao(conn); 
				list = cols.getCodeValue(codeValue);
			}
			else if (codeSetName.equalsIgnoreCase("mibNumber")) {
				cols = new MIBNumberCHDao(conn); 
				list = cols.getCodeValue(codeValue);
			}
			else if (codeSetName.equalsIgnoreCase("department")) {
				cols = new DepartmentCHDAO(conn); 
				list = cols.getCodeValue(codeValue);
			}
			else if (codeSetName.equalsIgnoreCase("section")) {
				cols = new SectionCHDAO(conn); 
				list = cols.getCodeValue(codeValue);
			}
			else if (codeSetName.equalsIgnoreCase("document")) {
				cols = new DocumentTypeCHDao(conn); 
				list = cols.getCodeValue(codeValue);
			}
			else if (codeSetName.equalsIgnoreCase("users")) {
				cols = new UserCHDao(); 
				list = cols.getCodeValue(codeValue);
			}			 
			else if (codeSetName.equalsIgnoreCase("notificationTemplates")) {
				cols = new NotificationTemplateCHDao(conn); 
				list = cols.getCodeValue(codeValue);
			}		
			else if (codeSetName.equalsIgnoreCase("testProfiles")) {
				cols = new TestProfileCHDao(conn); 
				list = cols.getCodeValue(codeValue);
			}
			else if (codeSetName.equalsIgnoreCase("autoAssignmentCriteria")) {
				cols = new AutoAssignmentCriteriaCHDao(conn); 
				list = cols.getCodeValue(codeValue);
			}
			else {
				list = null;	
			}
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
		}
		finally{
			closeConnection(conn);
		}
		
		return list;
	}
	
	/**
	 * Given a system source status code, it will retrieve the IUM equivalent
	 * 
	 * @param lobCode
	 * @param statusCode
	 * @return
	 * @throws SQLException
	 */
	public StatusData convertStatusData(String lobCode, String statusCode, String statusType) throws SQLException {
		
		StatusData status = null;
		
		StatusCHDao scd = new StatusCHDao();
		status =scd.convertStatus(lobCode,statusCode);
		
		return status;
	}
	
	/**
	 * Given a system source status code, it will retrieve the IUM equivalent in the properties file
	 * 
	 * @param lobCode
	 * @param statusCode
	 * @return
	 * @throws SQLException
	 */
	public StatusData convertStatusData(final String statusCode) throws SQLException {
		
		StatusData status = null;

		ResourceBundle rb = ResourceBundle.getBundle("statuscodes");
		final long statId = Long.parseLong(rb.getString(statusCode));
		final String statusType = rb.getString(statusCode+"_Type");
		status = new StatusData();
		status.setStatusId(statId);
		status.setStatusDesc(statusCode);
		status.setStatusType(statusType);
		
		
		return status;
	}
	
	/**
	 * Given an IUM status, it will retrieve the system source equivalent
	 * @param lobCode
	 * @param statusID
	 * @return
	 * @throws SQLException
	 */
	public StatusData translateStatusData(String lobCode, long statusID) throws SQLException {
		
		StatusData status =null;
		
		StatusCHDao scd = new StatusCHDao();
		status = scd.translateStatus(lobCode,statusID);
		
		return status;
	}
	
	public UserProfileData getUserProfile(String userId) throws SQLException {
		
		UserProfileData userprof = null;
		
		UserDAO ud = new UserDAO();
		userprof =ud.selectUserProfile(userId);
		
		return userprof;
		
	}
	

	public UserProfileData getUserProfileByACF2ID(String userId) throws SQLException {
		
		UserProfileData userprof = null;
		
		UserDAO ud = new UserDAO();
		userprof = ud.selectUserProfileByACF2ID(userId);
		 		
		return userprof;
	}
	
		
	
	public ClientData getClientData(String lastName, String firstName, String middleName) throws SQLException {
		
		ClientData client = null;
		
		ClientDAO cd = new ClientDAO();
		client =  cd.retrieveClient(lastName, firstName, middleName);
		
		return client;
	}
	
	public SunLifeOfficeData getSunLifeOffice(String officeId) throws SQLException {
		
		SunLifeOfficeData ofc = null;
		Connection conn = null;
		try {
			conn = new DataSourceProxy().getConnection();
			BranchCHDao bd = new BranchCHDao(conn);
			ofc = bd.getSunLifeOffice(officeId);
		}
		finally {		
			closeConnection(conn);
		} 		
		
		return ofc;
	}
	
	public ArrayList getExaminerData() throws SQLException {
		
		ArrayList list = null;
		Connection conn = null;
		try {
			conn = new DataSourceProxy().getConnection();
			ExaminerDAO ed = new ExaminerDAO(conn);
			list =  ed.selectExaminers();
		}
		finally {
			closeConnection(conn);
		}
		
		return list;		
	}
	
	public ArrayList getLaboratoryData() throws SQLException {
		
		ArrayList list = null;
		Connection conn = null;
		try {
			conn = new DataSourceProxy().getConnection();
			LaboratoryDAO ld = new LaboratoryDAO(conn);
			list =   ld.selectLaboratories();
		}
		finally {
			closeConnection(conn);
		}
		
		return list;		
	}
	

	public static String getStackTrace(Exception e) 
	{
		StringWriter sWriter = new StringWriter();
		PrintWriter pWriter = new PrintWriter(sWriter);
			e.printStackTrace(pWriter);
			String stackTrace = sWriter.toString();
			try {
				sWriter.close();
				pWriter.close();
			} catch (IOException e1) {
				LOGGER.error(CodeHelper.getStackTrace(e1));
			}
			
		return stackTrace;
	}
	
	
	
}
