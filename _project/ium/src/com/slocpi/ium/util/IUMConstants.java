package com.slocpi.ium.util;

/**
 * This interface contains contants used by the IUM system.
 *
 * @author Barry Yu		Copyright (c) 2004 Pointwest Technologies Corp.
 * @date Jan 6, 2004
 */
public interface IUMConstants {

	
	public static final String STATUS                = "getStatus";                   //Status field name
	public static final String YES                   = "1";                           //Yes,true Used for indicators
	public static final String NO                    = "0";                           //No, False Used for indicators
	public static final String SMS_ONLY              = "S";                           //SMS Only
	public static final String EMAIL_ONLY            = "E";                           //Email Only
	public static final String BOTH_NOTIFICATION     = "B";                           //SMS and Email
	public static final String DEFAULT_EMAIL         = "iumdev@pointwest.com.ph";     //Default Email address for notification sender
	public static final String REFERENCE_NUM         = "<Reference No>";              //Reference No tag
	public static final String CLIENT_NUM            = "<Client No>";                 //Client No tag
	public static final String LOB                   = "<LOB>";                       //Line of Business tag
	public static final String APPLICATION_STATUS    = "<Application Status>";        //Application status (eg Approved,Declined, For Offer, NPW)
	public static final String REASSIGNMENT_DETAILS  = "<Reasssignment Details>";     //Reassignment Details
	public static final String USD_SPRVSOR_MNGR      = "<USD Supervisor/Manager>";    //Reassignment Details
	public static final long MED_REQUESTED           = 18;                            //Requested status id for medical records
	public static final String LAB_EXAM_TYPE         = "<Lab Exam Type>";                 //Medical Exam Type (Laboratory/Medical)
	public static final String EXAM_TYPE             = "<Exam Type>";                 //Medical Exam Type (Laboratory/Medical)
	public static final String CLIENT_NAME           = "<Client Name>";               //Client Name tag
	public static final String LAB_OR_EXAMINER       = "<Laboratory Examiner>";       //Laboratory or Examiner tag
	public static final String LAB_OR_EXAMINER_NAME  = "<Laboratory Examiner Name>";  //Laboratory or Examiner Name tag
	public static final String APPOINTMENT_DATE      = "<Appointment Date>";          //Appointment Date
	public static final String APPOINTMENT_TIME      = "<Appointment Time>";          //Appointment Time
	public static final long MED_EXAM_CONFIRMED      = 19;                            //Confirmed status of medical record
	public static final String REQUIREMENT_DETAILS   = "<Requirement Details>";       //Requirement Details
	public static final String PDF_ATTACHMENT        = "<PDF Attachment>";            //PDF Form attachment
	public static final String UNDERWRITER_NAME      = "<Underwriter Name>";          //Underwriter Name as sender to a notification
	public static final String SECTION_DESCRIPTION   = "<User Section>";         	  //Section description of sender
	public static final String REQUIREMENT           = "<Requirement>";               //Requirement
	public static final String REQUIREMENT_DESC      = "<Requirement Description>";   //Requirement Description
	public static final String DEADLINE			     = "<Deadline>";   			   	  //Deadline
	public static final String URL                   = "<URL>";   					  //URL
	public static final String COMMENTS 			 = "<Comments>";                  //Requirement Comments/Remarks
	public static final String SMS_DOMAIN            = "@SMS.PH.SUNLIFE";             //SMS Domain
	public static final int    MOBILE_NUM_LEN        = 13;                            //Mobile Number length
	public static final String MOBILE_PREFIX         = "+639";                        //Mobile Prefix
	public final static String DEFAULT_SECTION_ID 	 = "POLICYISUE"; 				  //medical
	public final static String URL_LINK_ACTION  	 = "login.do"; 				      //url link action
	public static final String AGENT_NAME 			 =	"<Agent Name>";				  //Agent Name tag
	public static final String AGENT_ID 			 =	"<Agent Id>";				  //Agent Id tag
	public static final String NB_GENERIC_EMAIL		 =  "Phil-SLF-Rqmt-NewBusiness@sunlife.com"; //Generic Email for Document Matching
	public static final String OWNER_NAME 			 =  "<Owner Name>";
	public static final String OWNER_INSURED_SUBJECT_TEMPLATE = "<Reference No> - <Owner Name> / <Client Name> Agent:<Agent Id> <Agent Name>";

	//ListTag
	public final static String LIST_LOB 						= "1";
	public final static String LIST_BRANCHES 					= "2";
	public final static String LIST_REQUEST_STATUS 				= "3";
	public final static String LIST_REQUIREMENT_STATUS			= "4";
	public final static String LIST_MEDICAL_RECORD_STATUS		= "5";
	public final static String LIST_CLIENT_TYPE					= "6";
	public final static String LIST_MIB_IMPAIRMENT_CODE			= "7";
	public final static String LIST_MIB_NUMBER_CODE				= "8";
	public final static String LIST_MIB_LETTER_CODE				= "9";
	public final static String LIST_MIB_ACTION_CODE				= "10";
	public final static String LIST_AGENT 						= "11";
	public final static String LIST_LOCATION 					= "12";
	public final static String LIST_ASSIGN_TO 					= "13";
	public final static String LIST_REQUIREMENT_CODE			= "14";
	public final static String LIST_DOCUMENT_TYPE				= "15";
	public final static String LIST_DEPARTMENT	              	= "16";
	public final static String LIST_SECTION     	            = "17";
	public final static String LIST_USERS						= "18";
	public final static String LIST_USER_TYPE					= "19";
	public final static String LIST_ACCESS_LEVEL				= "20";
	public final static String LIST_EXAMINERS               	= "21";
	public final static String LIST_SCHEDULE                	= "22";
	public final static String LIST_IMPAIRMENT_CONFIRMATION		= "23";
	public final static String LIST_TEMPLATES					= "24";
	public final static String LIST_DOCTORS						= "25";
	public final static String LIST_SOURCE_SYSTEM				= "26";
	public final static String LIST_PC_STATUS			    	= "27";
	public final static String LIST_OBJ_STATUS			    	= "28";
	public final static String LIST_PC_STATUS_NULL				= "29";
	public final static String LIST_NOTIFICATION_TEMPLATES		= "30";
	public final static String LIST_ROLES						= "31";
	public final static String LIST_REFERENCE_CODES				= "32";
	public final static String LIST_TRANSACTION_RECORD      	= "33";
	public final static String LIST_TRANSACTION_TYPE  			= "34";
	public final static String LIST_STATUS_TYPE		  			= "35";
	public final static String LIST_FORM_TEMPLATE_FORMAT		= "36";
	public final static String LIST_LABORATORIES    			= "37";
	public final static String LIST_SEX                     	= "38";
	public final static String LIST_LOB_DOCUMENT_TYPE			= "39";
	public final static String LIST_LOB_REQUIREMENTS			= "40";
	public final static String LIST_HOLIDAY_YEAR				= "41";
	public final static String LIST_TEST_PROFILES  				= "42";
	public final static String LIST_REQUIREMENT_FORMS			= "43";
	public final static String LIST_AUTO_ASSIGN_CRITERIA		= "44";
	public final static String LIST_AUTO_ASSIGN_CRITERIA_LOV	= "45";
	public final static String LIST_RECORD_TYPES				= "46";
	public final static String LIST_RANKS					    = "47";
	public final static String LIST_SPECIALIZATIONS			    = "48";
	public final static String LIST_EXAMINATION_PLACES		    = "49";
	public final static String LIST_INTERFACE_TYPES			    = "50";
	public final static String LIST_PURGE_CRITERIA			    = "51";
	public final static String LIST_CHANGE_MED_RECORD_STATUS 	= "52";
	public final static String LIST_FILTER_MED_RECORD_STATUS 	= "53";
	public final static String LIST_LABORATORY_TEST_PROFILES 	= "54";
	public final static String LIST_BRANCH_AGENTS 						= "55";

	//Status
	public final static long STATUS_NB_REVIEW_ACTION       = 1;
	public final static long STATUS_FOR_TRANSMITTAL_USD    = 2;
	public final static long STATUS_FOR_FACILITATOR_ACTION = 3;
	public final static long STATUS_FOR_ASSESSMENT         = 4;
	public final static long STATUS_UNDERGOING_ASSESSMENT  = 5;
	public final static long STATUS_FOR_APPROVAL           = 6;
	public final static long STATUS_APPROVED               = 7;
	public final static long STATUS_DECLINED               = 8;
	public final static long STATUS_FOR_OFFER              = 9;
	public final static long STATUS_AWAITING_REQUIREMENTS  = 10;
	public final static long STATUS_AWAITING_MEDICAL       = 11;
	public final static long STATUS_NOT_PROCEEDED_WITH     = 12;
	public final static long STATUS_ORDERED                = 13;
	public final static long STATUS_RECEIVED_IN_SITE       = 14;
	public final static long STATUS_WAIVED                 = 15;
	public final static long STATUS_REVIEWED_AND_REJECTED  = 16;
	public final static long STATUS_REVIEWED_AND_ACCEPTED  = 17;
	public final static long STATUS_REQUESTED              = 18;
	public final static long STATUS_CONFIRMED              = 19;
	public final static long STATUS_VALID                  = 20;
	public final static long STATUS_EXPIRED                = 21;
	public final static long STATUS_NOT_SUBMITTED          = 22;
	public final static long STATUS_CANCELLED              = 23;
	public final static long STATUS_NTO			           = 24;
	public final static long STATUS_SAA			           = 25;
	public final static long STATUS_SCC			           = 26;
	public final static long STATUS_SIR			           = 27;
	public final static long STATUS_AR_CANCELLED		   = 141;
	public final static long STATUS_REQ_CANCELLED          = 121;
	public final static long STATUS_AR_FOR_REFERRAL		   = 181;

	// Default Value in Ingenium for Index Number
	public final static String DEFAULT_IMG_REF_NUM = "0000000000000000000000000000000000000000";	
	
	//Client type
	public final static String CLIENT_TYPE_OWNER		= "O";
	public final static String CLIENT_TYPE_INSURED		= "I";
	public final static String CLIENT_TYPE_MEMBER		= "M";
	public final static String CLIENT_TYPE_PLANHOLDER	= "P";

	//LOB
	public final static String LOB_INDIVIDUAL_LIFE = "IL";
	public final static String LOB_PRE_NEED        = "PN";
	public final static String LOB_MUTUAL_FUNDS    = "MF";
	public final static String LOB_GROUP_LIFE      = "GL";

	//Source Systems
	public final static String SYSTEM_ABACUS	= "1";
	public final static String SYSTEM_PRISM		= "2";
	public final static String SYSTEM_GLASS		= "3";
	public final static String SYSTEM_INGENIUM	= "4";

	//Source Systems Description
	public final static String SYSTEM_ABACUS_DESC	= "ABACUS";
	public final static String SYSTEM_PRISM_DESC	= "PRISM";
	public final static String SYSTEM_GLASS_DESC	= "GLASS";
	public final static String SYSTEM_INGENIUM_DESC	= "INGENIUM";

	public final static String USER_IUM		= "IUMDEV";

	//Access
	public final static long ACCESS_INQUIRE		= 1;
	public final static long ACCESS_CREATE		= 2;
	public final static long ACCESS_MAINTAIN	= 3;
	public final static long ACCESS_DELETE		= 4;
	public final static long ACCESS_EXECUTE		= 5;

	//Roles
	public final static String ROLES_AGENTS					= "AGENT";
	public final static String ROLES_NB_ADMIN				= "GRPNBADMIN";
	public final static String ROLES_NB_STAFF				= "NBSTAFF";
	public final static String ROLES_NB_REVIEWER			= "NBREVIEWER";
	public final static String ROLES_NB_SUPERVISOR			= "NBSUPMGR";
	public final static String ROLES_FACILITATOR_SUPERVISOR	= "FACILITSUPMGR";
	public final static String ROLES_FACILITATOR			= "FACILITATOR";
	public final static String ROLES_UNDERWRITER			= "UNDERWRITER";
	public final static String ROLES_UNDERWRITER_SUPERVISOR = "UWTNGSUPMGR";
	public final static String ROLES_MEDICAL_ADMIN			= "MEDICALADMIN";
	public final static String ROLES_MEDICAL_SUPERVISOR		= "MEDSUPMGR";
	public final static String ROLES_MEDICAL_CONSULTANT		= "MEDCONSLT";
	public final static String ROLES_EXAMINER				= "EXAMINER";
	public final static String ROLES_LAB_STAFF				= "LABSTAFF";
	public final static String ROLES_OTHER_USER				= "OTHERUSER";
	public final static String ROLES_LOV_ADMIN				= "LOVADMIN";
	public final static String ROLES_SECURITY_ADMIN			= "SCTYADMIN";
	public final static String ROLES_SYSTEM_OPERATOR		= "SYSOPER";
	public final static String ROLES_MARKETING_STAFF		= "MKTGSTAFF";

	//Level
	public final static String LEVEL_POLICY					= "P";
	public final static String LEVEL_CLIENT					= "C";

	//Requirement Code
	public final static String REQUIREMENT_CODE_RECEIVED_APPLICATION_BY_USD	="RECD1";
	public final static String REQUIREMENT_CODE_RECEIVED_APPLICATION		="RAC";
	public final static String REQUIREMENT_CODE_ACCEPTANCE_OF_OFFER			="AOO";
	public final static String REQUIREMENT_CODE_MEDICAL_EXAM_LOA			="LOAM";
	public final static String REQUIREMENT_CODE_LAB_EXAM_LOA			    ="LOAL";
	public final static String REQUIREMENT_CODE_CWA						    ="CWA";

	//Status Type in DB
	public final static String STAT_TYPE_AR			= "AR";
	public final static String STAT_TYPE_NM			= "NM";
	public final static String STAT_TYPE_M			= "M";

	//Status Type Description in DB
	public final static String STAT_TYPE_DESC_AR	= "Assessment Request";
	public final static String STAT_TYPE_DESC_NM	= "Non-medical";
	public final static String STAT_TYPE_DESC_M		= "Medical";

	//for workflow use, did not remove these since a lot of classes have been using this to call the WF and to distinguish it from status type but value is same
	public static final String ASSESSMENT_REQUEST    = STAT_TYPE_AR;                         //Process Code for Assessment Request
	public static final String POLICY_REQUIREMENTS   = STAT_TYPE_NM;                         //Process Code for Policy requirements
	public static final String MEDICAL_RECORDS       = STAT_TYPE_M;                          //Process Code for Medical Records
	public static final String ORDER_REQUIREMENT 	 = "ORDER_REQUIREMENT";					// Type - Send notification for ordering requirements
	public static final String RECEIVED_REQUIREMENT  = "RECEIVED_REQUIREMENT";

	//Configuration file
	public final static String IUM_CONFIG					= "dbconfig";
	public final static String IUM_DBLOCATION				= "db_location";
	public final static String IUM_DBLOCATION_MIB			= "db_location_mib";
	public final static String IUM_SMTP_KEY					= "smtp_prop_key";
	public final static String IUM_SMTP_PORT_KEY			= "smtp_prop_port_key";
	public final static String IUM_SMTP_HOST				= "smtp_prop_host";
	public final static String IUM_SMTP_PORT				= "smtp_prop_port";
	public final static String IUM_SMTP_ACCNT				= "smtp_accnt";
	public final static String IUM_SMTP_ACCNT_PASS		    = "smtp_accnt_password";
	public final static String IUM_FORMSTEMPLATES_PATH		= "reqt_forms_path";
	public final static String IUM_FORMSTEMPLATES_FOLDER	= "reqt_forms_folder";
	public final static String IUM_NOTESDB_HOST				= "notesdb_host";
	public final static String IUM_NOTESDB_USER				= "notesdb_user";
	public final static String IUM_NOTESDB_PASS				= "notesdb_password";
	public final static String IUM_NOTESDB_SERVER			= "notesdb_server";
	public final static String IUM_NOTESDB_PATH				= "notesdb_path";
	public final static String IUM_APPLICATION_RESOURCES	= "ApplicationResources";
	public final static String IUM_HOME_PAGE                = "home_page";                    //home page where deployed
	public final static String IUM_MQ_CONFIG_FILE           = "mqconfig_file";
	public final static String IUM_MQ_DEF_CONFIG_FILE       = "mqcomm";
	public static final String IUM_SYSTEM_ENV 				= "env"; 	//where the system operates in
	public final static String IUM_LDAP_CONFIG				= "ldapconfig";
	public final static String IUM_WMS_CONFIG				= "wmsconfig";


	//Update/Insert Type
	public final static String PROCESS_TYPE_MULTIPLE				= "MULTIPLE";
	public final static String PROCESS_TYPE_SINGLE					= "SINGLE";

	//Sort Order
	public final static String DESCENDING				= " DESC";
	public final static String ASCENDING				= "";

	//Assessment Request Sort Columns
	public final static int SORT_BY_REFERENCE_NUM		=  1;
	public final static int SORT_BY_LOB					=  2;
	public final static int SORT_BY_STATUS				=  3;
	public final static int SORT_BY_NAME_OF_INSURED		=  4;
	public final static int SORT_BY_AMOUNT_COVERED		=  5;
	public final static int SORT_BY_ASSIGNED_TO			=  6;
	public final static int SORT_BY_BRANCH				=  7;
	public final static int SORT_BY_AGENT				=  8;
	public final static int SORT_BY_APP_RECEIVED_DATE	=  9;
	public final static int SORT_BY_LOCATION			=  10;

	//Medical Records Sort Columns
	public final static int SORT_BY_MED_DATE_CONDUCTED	= 1;
	public final static int SORT_BY_MED_RECORD_ID		= 2;
	public final static int SORT_BY_MED_REFERENCE_NUM	= 3;
	public final static int SORT_BY_MED_CLIENT_NUM		= 4;
	public final static int SORT_BY_MED_TEST_BRANCH_ID	= 9;
	public final static int SORT_BY_MED_TEST_DESC		= 10;
	public final static int SORT_BY_MED_DATE_STATUS		= 11;
	public final static int SORT_BY_MED_DATE_RECEIVED	= 12;
	public final static int SORT_BY_MED_TEST_ID			= 13;

	//Auto-Assignments Sort Columns
	public final static int SORT_BY_REF_CRITERIA_CODE	=  1;
	public final static int SORT_BY_REF_UNDERWRITER		=  2;

	//Requirement Forms Sort Columns
	public final static int SORT_BY_REF_FORM_NAME		=  1;
	public final static int SORT_BY_REF_TEMPLATE_FORMAT	=  2;
	public final static int SORT_BY_REF_TEMPLATE_NAME	=  3;
	public final static int SORT_BY_REF_ACTIVE			=  4;

	//Abacus User ID
	public final static String ABACUS_USER					= "abacus_user_id";

	// Table Names
	public final static String TABLE_ASSESSMENT_REQUESTS	= "ASSESSMENT_REQUESTS";
	public final static String TABLE_MEDICAL_RECORDS		= "MEDICAL_RECORDS";
	public final static String TABLE_AUTO_ASSIGNMENTS		= "AUTO_ASSIGNMENTS";
	public final static String TABLE_REQUIREMENT_FORMS		= "REQUIREMENT_FORMS";

	//Reference Codes
	public final static String REF_CODE_REQUIREMENT_FORMS		= "REQUIREMENT_FORMS";
	public final static String REF_CODE_LOB						= "LOB";
	public final static String REF_CODE_DEPARTMENT				= "DEPARTMENT";
	public final static String REF_CODE_SECTION					= "SECTION";
	public final static String REF_CODE_EXAM_SPECIALIZATION		= "EXAM_SPECIALIZATION";
	public final static String REF_CODE_EXAM_PLACE				= "EXAM_PLACE";
	public final static String REF_CODE_EXAM_AREA				= "EXAM_AREA";
	public final static String REF_CODE_DOCUMENT_TYPE			= "DOCUMENT_TYPE";
	public final static String REF_CODE_CLIENT_TYPE				= "CLIENT_TYPE";
	public final static String REF_CODE_ROLES					= "ROLES";
	public final static String REF_CODE_AUTO_ASSIGN_CRITERIA	= "AUTO_ASSIGN_CRITERIA";
	public final static String REF_CODE_STATUS					= "STATUS";
	public final static String REF_CODE_TEST_PROFILE			= "TEST_PROFILE";
	public final static String REF_CODE_EXAM_RANK				= "EXAM_RANK";
	public final static String REF_CODE_AUTO_ASSIGN				= "AUTO_ASSIGN";
	public final static String REF_CODE_ACCESS_TEMPLATE			= "ACCESS_TEMPLATE";
	public final static String REF_CODE_MIB_IMPAIRMENT			= "MIB_IMPAIRMENT";
	public final static String REF_CODE_MIB_NUMBER				= "MIB_NUMBER";
	public final static String REF_CODE_MIB_ACTION				= "MIB_ACTION";
	public final static String REF_CODE_MIB_LETTER				= "MIB_LETTER";
	public final static String REF_CODE_HOLIDAY					= "HOLIDAY";
	public final static String REF_CODE_ACCESS					= "ACCESS";
	public final static String REF_CODE_PAGE					= "PAGE";


	//Test Types
	public final static String TEST_TYPE_MEDICAL			= "M";
	public final static String TEST_TYPE_LABORATORY			= "L";

	//Schedule Types
	public final static String SCHEDULE_TYPE_MIB_EXPORT			= "ME";
	public final static String SCHEDULE_TYPE_AUTO_EXPIRE		= "AE";

	//Page IDs - by Randall
	public final static String PAGE_HOME				=  "1";
	public final static String PAGE_ASSESSMENT_REQUEST	=  "2";
	public final static String PAGE_ASSESSMENT_DETAILS	=  "3";
	public final static String PAGE_REQUEST_DETAILS		=  "4";
	public final static String PAGE_CDS   				=  "5";
	public final static String PAGE_REQUIREMENTS_KO		=  "6";
	public final static String PAGE_UW_ASSESSMENT		=  "7";
	public final static String PAGE_DOCTORS_NOTES		=  "8";
	public final static String PAGE_MEDRECORD_LIST		=  "9";
	public final static String PAGE_MEDRECORD_DETAIL	=  "10";
	public final static String PAGE_ADMIN				=  "11";
	public final static String PAGE_REFERENCE_CODES		=  "12";
	public final static String PAGE_PROCESS_CONFIG		=  "13";
	public final static String PAGE_USER_ACCESS			=  "14";
	public final static String PAGE_EXCEPTION_REPORTS	=  "15";
	public final static String PAGE_AUDIT_TRAIL			=  "16";
	public final static String PAGE_REPORTS				=  "17";
	public final static String PAGE_CHANGE_PASSWORD		=  "18";
	public final static String PAGE_SUNLIFE_OFFICES		=  "19";
	public final static String PAGE_EXAMINERS		=  "20";
	public final static String PAGE_LABORATORIES		=  "21";
	public final static String PAGE_REQUIREMENTS		=  "22";
	public final static String PAGE_NOTIFICATION_STATEMENT		=  "23";
	public final static String PAGE_WORK_FUNCTION		=  "24";
	public final static String PAGE_MIB_EXPORT		=  "25";
	public final static String PAGE_MED_REC_EXPIRATION		=  "26";
	public final static String PAGE_ACCESS_TEMPLATES		=  "27";

	//Access IDs - by Randall
	public final static String ACC_INQUIRE	=  "1";
	public final static String ACC_CREATE	=  "2";
	public final static String ACC_MAINTAIN	=  "3";
	public final static String ACC_DELETE	=  "4";
	public final static String ACC_EXECUTE	=  "5";
	public final static String ACC_PRINT	=  "6";
	public final static String ACC_NOTIFY	=  "7";

	//Transaction Records for Audit Trail
	public final static long TRAN_RECORD_MIB_EXPORT = 30;
	public final static long TRAN_RECORD_AUTO_EXPIRE = 31;
	public final static long TRAN_RECORD_ARCHIVE = 32;

	//Auto-Assignment Criteria
	public final static String CRITERIA_BRANCH 			= "BRANCH";
	public final static String CRITERIA_AGENT			= "AGENT";
	public final static String CRITERIA_ASSIGNED_TO		= "ASSIGNED_TO";
	public final static String CRITERIA_LOB				= "LOB";
	public final static String CRITERIA_AMOUNT_COVERED	= "AMOUNT_COVERED";
	public final static String CRITERIA_PREMIUM			= "PREMIUM";
	public final static String CRITERIA_SOURCE_SYSTEM	= "SOURCE_SYSTEM";
	public final static String CRITERIA_LOCATION		= "LOCATION";

	// indicator
	public  final static String INDICATOR_SUCCESS = "Y";
	public  final static String INDICATOR_FAILED  = "N";

	// System user ID for batch audit trail
	public final static String USER_IUMS		= "IUMS";

    //Requirement code
    public final static String REQ_CODE_RECD1 = "RECD1";

    // Statement of Medical Exam Billing report types -- no longer needed, moved to MedicalExamBillingFilter
    public final static int REPORT_TYPE_EXAMINER	= 1;
	public final static int REPORT_TYPE_LABORATORY	= 2;
	public final static int REPORT_TYPE_AGENT		= 3;

	// Location Record Section
	public final static String LOCATION_RECORD_SECTION = "RECORDS";

    //Purge Criteria
    public final static String PURGE_ASSESSMENT_REQUESTS = "Assessment Requests";
    public final static String PURGE_MEDICAL_RECORDS     = "Medical Records";
    public final static String PURGE_AUDIT_TRAIL_LOGS    = "Audit Trail Logs";
    public final static String PURGE_ACTIVITY_LOGS       = "Activity Logs";

    //Flags
    public final static String SUCCESSFUL = "Successful";
	public final static String FAILED     = "Failed";

	//Notification ID
	public final static String NOTIFY_DOCTORS_NOTES     = "15";

	//Notification Parameters
	public final static String NOTIFY_PAGE_AR		= "AR";
	public final static String NOTIFY_PAGE_M		= "M";
	public final static String NOTIFY_PAGE_DN		= "DN";

	//User Types
	public final static String USER_TYPE_EXAMINER = "E";
	public final static String USER_TYPE_LABORATORY = "L";
	public final static String USER_TYPE_SUNLIFE = "S";
	public final static String USER_TYPE_AGENT = "A";

	//Notification Id
	public final static long NOTIFICATION_ID_FOLLOW_UP = 7;
	public final static String NOTIFICATION_ID_RECEIPT_REQUIREMENTS = "11";

	// key for the encryption of the database
	public final static String KEY = "'IUMDBKEY_06022004'";

	//
	public final static String PSWD_EXPIRATION  = "90";
	public final static String PSWD_CHANGE_ALERT  = "7";
	
	//LDAP 
	public final static String PROVIDER_URL  = "provider_url";
	public final static String SECURITY_AUTHENTHICATION  = "security_auth";
	public final static String LDAP_DOMAIN  = "ldap_domain";
	public final static String LDAP_ADMIN_ID  = "ldap_admin_id";
	public final static String LDAP_ADMIN_PSWD  = "ldap_admin_pswd";
	public final static String LDAP_ATTR_KEY  = "attr_key";
	public final static String LDAP_CNTXT  = "ldap_cntxt";
	
	//WMS
	public final static String REQT_STATUS_PARTIAL = "P";
	public final static String REQT_STATUS_COMPLETE = "C";
	// Request Type
	public final static String REQUEST_TYPE_CREATEPOLICY = "CreateUpdatePolicyRecord";
	public final static String REQUEST_TYPE_MATCH = "CreateUpdateRequirement";
	public final static String REQUEST_TYPE_UNMATCH = "UnmatchRequirement";
	
	//MIB
	public final static String MIB_COMPANY_CODE = "A";
	public final String MIB_STATUS = "1";
	public final String MIB_IMPAIRMENT_TRANSACTION_CODE = "3100";
	public static final String MIB_MEMBER_TRANSACTION_CODE = "2100";
	public final int MIB_RECORDS_AFFECTED = 1;
	public final int MIB_NO_OF_HITS = 0;

	//Requirement Codes
	public static final String REQUIREMENT_CODES_PROPERTY_FILE = "requirementcodes";
	public static final String REQUIREMENT_CODES_CONFIG = "requirement_codes";
	public static final String PLAN_CODES_PROPERTY_FILE = "plancodes";
	public static final String PLAN_CODES_CONFIG = "plan_codes";
	
	// Suppress Barcode
	public static final String SUPPRESS_LEFT_BARCODE = "suppress_left_barcode";
	
	// SMS
	public static final String SMS_DEFAULT_RECIPIENT_TYPE = "AGT";
	public static final String SMS_DEFAULT_EMAIL_TAG = "S";
	
	public static final String COMPANY_CODE_CP = "CP";
	public static final String COMPANY_CODE_GF = "GF";
	
	public static final String ORDER_DATE = "PR_ORDER_DATE";
	public static final String ORDERED_BY = "CREATED_BY";
	public static final String FOLLOW_UP_DATE = "PR_FOLLOW_UP_DATE";
	public static final String DATE_FORMAT = "yyyy-MM-dd";
}
