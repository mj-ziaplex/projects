package com.slocpi.ium.mib.dao.impl;

import java.util.Date;
import java.util.ResourceBundle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.slocpi.ium.data.ImpairmentData;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.DateHelper;

public class MibImpairmentDaoHelper {
	private static final Logger LOGGER = LoggerFactory.getLogger(MibImpairmentDaoHelper.class);
	public static final String TRANSACTION_INSERT_CALL = "{call usp_tran_dtl_ins(?,?,?,?,?)}";
	public static final String MEMBER_INSERT_CALL = "{call usp_member_ins(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
	public static final String MEMBER_SELECT_CALL = "{call usp_member_sel(?)}";
	
	public static final String IMPAIRMENT_SELECT_CALL = "{call usp_member_impairment_existing_sel(?,?,?,?,?,?)}";
	public static final String IMPAIRMENT_INSERT_CALL = "{call usp_member_impairment_staging_ins(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
	public static final String SELECT_ALL_IMPAIRMENT_SQL = 
		"SELECT mem_control_no,	" +
		"		num_let_cd,	" +
		"		let_cd_dt, " +
		"		impairment_cd, " +
		"		ver_flg, " +
		"		company_cd, " +
		"		policy_no, " +
		"		underwriting_dt, " +
		"		last_nm, " +
		"		first_nm, " +
		"		middle_nm, " +
		"		birth_dt, " +
		"		birth_place, " +
		"		rec_status, " +
		"		status, " +
		"		imp_remarks, " +
		"		rec_status " +
		"  FROM tbl_member_impairment " +
		" WHERE policy_no = ?";

	

	public static String getControlNo(ImpairmentData impairmentData, String impClientType) throws MibImpairmentDaoException {
		
		LOGGER.info("getControlNo start");
		String companyCode = "";
		try {
			ResourceBundle rbWMSSP = ResourceBundle.getBundle("dbconfig");
			companyCode = rbWMSSP.getString("MIB_COMPANY_CODE");
		} catch (Exception e){
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new MibImpairmentDaoException(e);
		}
		
		LOGGER.info("getControlNo end");
		return companyCode.concat(impairmentData.getReferenceNumber()).concat(impClientType);
		
	}

	public static String getRemarks(ImpairmentData impairmentData) {
		
		LOGGER.info("getRemarks start");
		final String SPACE = " ";
		return String.valueOf(impairmentData.getHeightInFeet()).concat(SPACE)
		.concat(String.valueOf(impairmentData.getHeightInInches())).concat(SPACE)
		.concat(String.valueOf(impairmentData.getWeight())).concat(SPACE)
		.concat(impairmentData.getBloodPressure());
	}

	public static String getLetCdDt(ImpairmentData impairmentData) {
		
		LOGGER.info("getLetCdDt start 1");
		final String EMPTY = "";
		Date impairmentDate = impairmentData.getImpairmentDate();
		LOGGER.info("getLetCdDt end 1");
		return (impairmentDate != null)? DateHelper.format(impairmentDate, DateHelper.MIB_PATTERN) : EMPTY;
	}
	
	public static String getLetCdDt(Date date) {
		LOGGER.info("getLetCdDt start 2");
		final String EMPTY = "";
		Date impairmentDate = date;
		LOGGER.info("getLetCdDt end 2");
		return (impairmentDate != null)? DateHelper.format(impairmentDate, DateHelper.MIB_PATTERN) : EMPTY;
	}

	public static String getNumLetCd(ImpairmentData impairmentData) {
		LOGGER.info("getNumLetCd");
		String numLetCd = impairmentData.getRelationship().concat(impairmentData.getImpairmentOrigin());
		return numLetCd;
	}

	public static String getRelationship(String numLetCd) {
		LOGGER.info("getRelationship");
		String relationship = numLetCd.substring(0, 1);
		return relationship;
	}

	public static String getOrigin(String numLetCd) {
		LOGGER.info("getOrigin");
		String relationship = numLetCd.substring(1, 2);
		return relationship;
	}
}