package com.slocpi.ium.mib.dao.impl;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.slocpi.ium.data.ClientData;
import com.slocpi.ium.data.ImpairmentData;
import com.slocpi.ium.mib.dao.MibImpairmentDao;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.DateHelper;
import com.slocpi.ium.util.IUMConstants;

/**
 * @author pz07
 *
 */
public class MibImpairmentDaoImpl extends BaseDaoImpl implements MibImpairmentDao {
	private static final Logger LOGGER = LoggerFactory.getLogger(MibImpairmentDaoImpl.class);
	private final String NUM_LET_CD_COL = "num_let_cd";
	private final String LET_CD_DT_COL = "let_cd_dt";
	private final String IMPAIRMENT_CD_COL = "impairment_cd";
	private final String VER_FLG_COL = "ver_flg";
	private final String POLICY_NO_COL = "policy_no";
		
	/* (non-Javadoc)
	 * @see com.slocpi.ium.interfaces.mib.dao.MibImpairmentsDao#retrieveImpairment(com.slocpi.ium.data.MIBImpairment)
	 */
	public ImpairmentData retrieveImpairment(ImpairmentData impairmentData, String impClientType) throws MibImpairmentDaoException {
		
		LOGGER.info("retrieveImpairment start");
		ImpairmentData returnData = null;
		String controlNo = MibImpairmentDaoHelper.getControlNo(impairmentData, impClientType);
		String numLetCd = MibImpairmentDaoHelper.getNumLetCd(impairmentData);
		String letCdDt = MibImpairmentDaoHelper.getLetCdDt(impairmentData);
		String remarks = MibImpairmentDaoHelper.getRemarks(impairmentData);
		try {
			connect();
			cs = con.prepareCall(MibImpairmentDaoHelper.IMPAIRMENT_SELECT_CALL);
			cs.setString(1, controlNo);
			cs.setString(2, numLetCd);			
			cs.setString(3, letCdDt);
			cs.setString(4, impairmentData.getImpairmentCode());
			cs.setString(5, impairmentData.getConfirmation());
			cs.setString(6, remarks);
			rs = cs.executeQuery();
			while (null != rs && rs.next()) {
				returnData = new ImpairmentData();
				final String numLetCdResult = rs.getString(NUM_LET_CD_COL);
				returnData.setReferenceNumber(rs.getString("mem_control_no"));
				returnData.setRelationship(MibImpairmentDaoHelper.getRelationship(numLetCdResult));
				returnData.setImpairmentOrigin(MibImpairmentDaoHelper.getOrigin(numLetCdResult));
				returnData.setImpairmentDate(DateHelper.parse(rs.getString(LET_CD_DT_COL), DateHelper.MIB_PATTERN));
				returnData.setImpairmentCode(rs.getString(IMPAIRMENT_CD_COL));
				returnData.setConfirmation(rs.getString(VER_FLG_COL));
				break;
			}
		} catch (MibImpairmentDaoException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new MibImpairmentDaoException(e);
		} finally {
			disconnect();
		}
		LOGGER.info("retrieveImpairment end");
		return returnData;
	}


	/* (non-Javadoc)
	 * @see com.slocpi.ium.mib.dao.MibImpairmentDao#retrieveImpairmentList(java.lang.String, java.lang.String)
	 */
	public List retrieveImpairmentList(String referenceNumber) throws MibImpairmentDaoException  {
		
		LOGGER.info("retrieveImpairmentList start");
		ImpairmentData returnData = null;
		List impairmentList = null;
		try {
			connect();
			ps = con.prepareStatement(MibImpairmentDaoHelper.SELECT_ALL_IMPAIRMENT_SQL);
			ps.setString(1, referenceNumber);
			rs = ps.executeQuery();
			if (null != rs) {
				impairmentList = new ArrayList();
				while (rs.next()) {
					returnData = new ImpairmentData();
					returnData.setImpairmentDate(DateHelper.parse(rs.getString(LET_CD_DT_COL), DateHelper.MIB_PATTERN));
					returnData.setImpairmentCode(rs.getString(IMPAIRMENT_CD_COL));
					returnData.setConfirmation(rs.getString(VER_FLG_COL));
					returnData.setReferenceNumber(rs.getString(POLICY_NO_COL));
					impairmentList.add(returnData);
				}
			}
		} catch (MibImpairmentDaoException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new MibImpairmentDaoException(e);
		} finally {
			disconnect();
		}
		LOGGER.info("retrieveImpairmentList end");
		return impairmentList;
	}

	/* (non-Javadoc)
	 * @see com.slocpi.ium.interfaces.mib.dao.MibImpairmentsDao#createImpairment(com.slocpi.ium.data.MIBImpairment)
	 */
	public int createImpairment(ImpairmentData impairmentData, ClientData clientData, String impClientType) throws MibImpairmentDaoException {
		
		LOGGER.info("createImpairment start");
		String numLetCd = MibImpairmentDaoHelper.getNumLetCd(impairmentData);
		String remarks = MibImpairmentDaoHelper.getRemarks(impairmentData);
		String letCdDt = MibImpairmentDaoHelper.getLetCdDt(impairmentData);
		
		
		String companyCode = "";
		try {
			ResourceBundle rbWMSSP = ResourceBundle.getBundle("dbconfig");
			companyCode = rbWMSSP.getString("MIB_COMPANY_CODE");
		} catch (Exception e){
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new MibImpairmentDaoException(e);
		}
		
		
		try {
			connect();
			cs = con.prepareCall(MibImpairmentDaoHelper.IMPAIRMENT_INSERT_CALL);
			cs.setString(1, MibImpairmentDaoHelper.getControlNo(impairmentData, impClientType));
			cs.setString(2, numLetCd);
			cs.setString(3, letCdDt);
			cs.setString(4, impairmentData.getImpairmentCode());
			cs.setString(5, impairmentData.getConfirmation());
			cs.setString(6, companyCode);
			cs.setString(7, impairmentData.getReferenceNumber());
			cs.setTimestamp(8, DateHelper.sqlTimestamp(new Date()));
			cs.setString(9, clientData.getLastName());
			cs.setString(10, clientData.getGivenName());
			cs.setString(11, clientData.getMiddleName());
			cs.setDate(12, DateHelper.sqlDate(clientData.getBirthDate()));
			cs.setString(13, clientData.getBirthLocation());
			cs.setString(14, IUMConstants.MIB_STATUS);
			cs.setString(15, remarks);
			cs.setString(16, clientData.getSex());
			cs.setString(17, clientData.getOtherGivenName());
			cs.setString(18, clientData.getTitle());
			cs.setString(19, clientData.getSuffix());
			cs.setString(20, "");
			cs.setString(21, impairmentData.getActionCode());
			cs.setString(22, clientData.getOtherMiddleName());
			cs.setString(23, impairmentData.getUpdatedBy());
			LOGGER.info("Client String-->"+ cs.toString());
			cs.execute();
		} catch (MibImpairmentDaoException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new MibImpairmentDaoException(e);
		} finally {
			disconnect(); 
		}
		LOGGER.info("createImpairment end");
		return 0;
	}
	
}
