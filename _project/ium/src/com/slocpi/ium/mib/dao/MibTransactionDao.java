package com.slocpi.ium.mib.dao;

import com.slocpi.ium.data.MibTransactionData;
import com.slocpi.ium.mib.dao.impl.MibImpairmentDaoException;

/**
 * @author pz07
 *
 */
public interface MibTransactionDao extends BaseDao {

	/**
	 * @param mibTransactionData
	 * @return
	 * @throws MibImpairmentDaoException
	 */
	public int createTransaction(MibTransactionData mibTransactionData) throws MibImpairmentDaoException;
	
}
