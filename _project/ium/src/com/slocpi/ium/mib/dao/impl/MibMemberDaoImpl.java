package com.slocpi.ium.mib.dao.impl;

import java.sql.SQLException;
import java.util.Date;
import java.util.ResourceBundle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.slocpi.ium.data.ImpairmentData;
import com.slocpi.ium.data.MibMemberData;
import com.slocpi.ium.mib.dao.MibMemberDao;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.DateHelper;
import com.slocpi.ium.util.IUMConstants;

/**
 * @author pz07
 *
 */
public class MibMemberDaoImpl extends BaseDaoImpl implements MibMemberDao {

	private static final Logger LOGGER = LoggerFactory.getLogger(MibMemberDaoImpl.class);
	
	/* (non-Javadoc) 
	 * @see com.slocpi.ium.mib.dao.MibMemberDao#createMember(com.slocpi.ium.data.MibMemberData)
	 */
	public int createMember(ImpairmentData impairmentData, MibMemberData mibMemberData, String impClientType) throws MibImpairmentDaoException {
		
		LOGGER.info("createMember start");
		String companyCode = "";
		try {
			ResourceBundle rbWMSSP = ResourceBundle.getBundle("dbconfig");
			companyCode = rbWMSSP.getString("MIB_COMPANY_CODE");
		} catch (Exception e){
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new MibImpairmentDaoException(e);
		}
	
		String controlNo = MibImpairmentDaoHelper.getControlNo(impairmentData, impClientType);
		connect();
		try {
			cs = con.prepareCall(MibImpairmentDaoHelper.MEMBER_INSERT_CALL);
			cs.setString(1, controlNo);
			cs.setString(2, mibMemberData.getLastName());
			cs.setString(3, mibMemberData.getGivenName());
			cs.setString(4, mibMemberData.getMiddleName());
			cs.setString(5, mibMemberData.getSex());
			cs.setDate(6, DateHelper.sqlDate(mibMemberData.getBirthDate()));
			cs.setString(7, mibMemberData.getBirthLocation());
			cs.setString(8, mibMemberData.getOtherGivenName());
			cs.setString(9, mibMemberData.getTitle());
			cs.setString(10, mibMemberData.getSuffix());
			cs.setString(11, mibMemberData.getOtherMiddleName());
			cs.setString(12, mibMemberData.getNationality());
			cs.setDate(13, DateHelper.sqlDate(new Date()));
			cs.setString(14, companyCode);
			cs.setString(15, mibMemberData.getPolicyNumber());
			cs.setString(16, mibMemberData.getActionCode());
			cs.setString(17, IUMConstants.MIB_STATUS);
			cs.execute();
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new MibImpairmentDaoException(e);
		} finally {
			disconnect();
		}
		LOGGER.info("createMember end");
		return 0;
	}

	/* (non-Javadoc)
	 * @see com.slocpi.ium.mib.dao.MibMemberDao#retrieveMember(com.slocpi.ium.data.MibMemberData)
	 */
	public MibMemberData retrieveMember(ImpairmentData impairmentData, MibMemberData mibMemberData, String impClientType) throws MibImpairmentDaoException {
		
		LOGGER.info("retrieveMember start");
		MibMemberData returnData = null;
		String controlNo = MibImpairmentDaoHelper.getControlNo(impairmentData, impClientType);
		connect();
		try {
			cs = con.prepareCall(MibImpairmentDaoHelper.MEMBER_SELECT_CALL);
			cs.setString(1, controlNo);
			rs = cs.executeQuery();
			while (null != rs && rs.next()) {
				returnData = new MibMemberData();
				returnData.setLastName(rs.getString("last_nm"));
				returnData.setGivenName("first_nm");
				returnData.setMiddleName(rs.getString("middle_nm"));
				returnData.setSex(rs.getString("sex"));
				returnData.setBirthDate(DateHelper.parse(rs.getString("birth_dt"), DateHelper.MIB_PATTERN));
				returnData.setBirthLocation(rs.getString("birth_place"));
				returnData.setOtherGivenName(rs.getString("alias"));
				returnData.setTitle(rs.getString("title"));
				returnData.setSuffix(rs.getString("suffix"));
				returnData.setOtherMiddleName(rs.getString("maiden_nm"));
				returnData.setNationality(rs.getString("nationality"));
				returnData.setUnderwritingDate(DateHelper.parse(rs.getString("underwriting_dt"), DateHelper.MIB_PATTERN));
				returnData.setCompanyCode(rs.getString("company_cd"));
				returnData.setPolicyNumber(rs.getString("policy_no"));
				returnData.setActionCode(rs.getString("action_cd"));
				returnData.setStatus(rs.getString("status"));
				break;
			}
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new MibImpairmentDaoException(e);
		} finally {
			disconnect();
		}
		LOGGER.info("retrieveMember end");
		return returnData;
	}

}
;