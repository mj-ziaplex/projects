package com.slocpi.ium.mib.dao.impl;

import java.sql.SQLException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.slocpi.ium.data.MibTransactionData;
import com.slocpi.ium.mib.dao.MibTransactionDao;
import com.slocpi.ium.util.CodeHelper;

/**
 * @author pz07
 *
 */
public class MibTransactionDaoImpl extends BaseDaoImpl implements MibTransactionDao {

	private static final Logger LOGGER = LoggerFactory.getLogger(MibTransactionDaoImpl.class);
	/* (non-Javadoc)
	 * @see com.slocpi.ium.mib.dao.MibTransactionDao#createTransaction(com.slocpi.ium.data.MibTransactionData)
	 */
	public int createTransaction(MibTransactionData mibTransactionData) throws MibImpairmentDaoException {
		
		LOGGER.info("createTransaction start");
		try {
			connect();
			cs = con.prepareCall(MibImpairmentDaoHelper.TRANSACTION_INSERT_CALL);
			cs.setString(1, mibTransactionData.getUserCode());
			cs.setString(2, mibTransactionData.getCompanyCode());
			cs.setString(3, mibTransactionData.getTransactionCode());
			cs.setInt(4, mibTransactionData.getRecordAffected());
			cs.setInt(5, mibTransactionData.getNoOfHits());
			cs.execute();
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new MibImpairmentDaoException(e);
		} finally {
			disconnect();
		}
		LOGGER.info("createTransaction end");
		return 0;
	}

}
