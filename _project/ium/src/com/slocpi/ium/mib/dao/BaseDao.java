package com.slocpi.ium.mib.dao;

import com.slocpi.ium.mib.dao.impl.MibImpairmentDaoException;

/**
 * @author pz07
 *
 */
public interface BaseDao {

	/**
	 * @throws MibImpairmentDaoException
	 */
	public void connect() throws MibImpairmentDaoException;
	
	/**
	 * This method is used for junit testing purposes only as this do not get connection from jndi.
	 * @throws MibImpairmentDaoException
	 */
	public void connectNonJndi() throws MibImpairmentDaoException;
	
	/**
	 * @throws MibImpairmentDaoException
	 */
	public void disconnect() throws MibImpairmentDaoException;
	
}
