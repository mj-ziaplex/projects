package com.slocpi.ium.mib.dao;

import com.slocpi.ium.data.ImpairmentData;
import com.slocpi.ium.data.MibMemberData;
import com.slocpi.ium.mib.dao.impl.MibImpairmentDaoException;

/**
 * @author pz07
 *
 */
public interface MibMemberDao extends BaseDao {

	/**
	 * @param impairmentData 
	 * @return
	 */
	public int createMember(ImpairmentData impairmentData, MibMemberData mibMemberData, String impClientType) throws MibImpairmentDaoException;
	
	/**
	 * @param mibMemberData
	 * @return
	 * @throws MibImpairmentDaoException
	 */
	public MibMemberData retrieveMember(ImpairmentData impairmentData, MibMemberData mibMemberData, String impClientType) throws MibImpairmentDaoException;
	
}
