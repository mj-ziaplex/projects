package com.slocpi.ium.mib.dao;

import java.util.List;
import com.slocpi.ium.data.ClientData;
import com.slocpi.ium.data.ImpairmentData;
import com.slocpi.ium.mib.dao.impl.MibImpairmentDaoException;

/**
 * @author pz07
 *
 */
public interface MibImpairmentDao extends BaseDao {
	
	/**
	 * @param impairment
	 * @return
	 */
	public ImpairmentData retrieveImpairment(ImpairmentData impairmentData, String impClientType) throws MibImpairmentDaoException;
	
	/**
	 * @param referenceNumber
	 * @param clientType
	 * @return
	 */
	public List retrieveImpairmentList(String referenceNumber) throws MibImpairmentDaoException;
	
	/**
	 * @param clientData 
	 * @param impClientType 
	 * @param impairment
	 * @return
	 * @throws MibImpairmentDaoException 
	 * @throws Exception 
	 */
	public int createImpairment(ImpairmentData impairmentData, ClientData clientData, String impClientType) throws MibImpairmentDaoException ;
	
}
