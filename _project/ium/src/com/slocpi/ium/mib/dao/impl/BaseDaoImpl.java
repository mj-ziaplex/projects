package com.slocpi.ium.mib.dao.impl;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.slocpi.ium.mib.dao.BaseDao;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.IUMConstants;

/**
 * @author pz07
 *
 */
public class BaseDaoImpl implements BaseDao {

	
	private static final Logger LOGGER = LoggerFactory.getLogger(BaseDaoImpl.class);
	
	public boolean isTest = false;
	protected Connection con;
	protected PreparedStatement ps;
	protected CallableStatement cs;
	protected ResultSet rs;
	
	/**
	 * @return
	 */
	public Connection getCon() {
		return con;
	}
	
	/* (non-Javadoc)
	 * @see com.slocpi.ium.interfaces.mib.dao.BaseDao#connect()
	 */
	public void connect() throws MibImpairmentDaoException {
		
		LOGGER.info("connect start");
		
		Context ic = null;
		DataSource ds = null;
		if (con == null) {
			try {
				if (isTest) {
					LOGGER.info("Connecting to mib sql server");
					connectNonJndi();
				} else {
					LOGGER.info("connecting to oracle");
					ic = new InitialContext();
					ds = (DataSource) ic.lookup(BaseDaoHelper.getDataSourceConfig());
					con = ds.getConnection();
				}
			} catch (NamingException e) {
				LOGGER.error(CodeHelper.getStackTrace(e));
				throw new MibImpairmentDaoException(e);
			} catch (SQLException e) {
				LOGGER.error(CodeHelper.getStackTrace(e));
				throw new MibImpairmentDaoException(e);
			}
		}
		LOGGER.info("connect end");
		
	}

	/* (non-Javadoc)
	 * For junit testing purposes only
	 * @see com.slocpi.ium.interfaces.mib.dao.BaseDao#connectNonContext()
	 */
	public void connectNonJndi() throws MibImpairmentDaoException {
		
		LOGGER.info("connectNonJndi start");
		String driver = "com.evermind.sql.DriverManagerDataSource";
		String user = "itsmib31";
		String pwd = "mib4iumdev";
		String url = "jdbc:microsoft:sqlserver://SV591202:1436;DatabaseName=WDMIBX51";
		if (con == null) {
			try {
				Class.forName(driver);
				con = DriverManager.getConnection(url, user, pwd);	
			} catch (ClassNotFoundException e) {
				LOGGER.error(CodeHelper.getStackTrace(e));
				throw new MibImpairmentDaoException(e);
			} catch (SQLException e) {
				LOGGER.error(CodeHelper.getStackTrace(e));
				throw new MibImpairmentDaoException(e);
			}
		}
		LOGGER.info("connectNonJndi end");
	}

	/* (non-Javadoc)
	 * @see com.slocpi.ium.interfaces.mib.dao.BaseDao#disconnect()
	 */
	public void disconnect() throws MibImpairmentDaoException {
		
		LOGGER.info("disconnect start");
		if (ps != null) {
			try {
				ps.close();
				ps = null;
			} catch (SQLException e) {
				LOGGER.error(CodeHelper.getStackTrace(e));
				throw new MibImpairmentDaoException(e);
			}
		} 
		if (cs != null) {
			try {
				cs.close();
				cs = null;
			} catch (SQLException e) {
				LOGGER.error(CodeHelper.getStackTrace(e));
				throw new MibImpairmentDaoException(e);
			}
		} 
		if (rs!= null) {
			try {
				rs.close();
				rs = null;
			} catch (SQLException e) {
				LOGGER.error(CodeHelper.getStackTrace(e));
				throw new MibImpairmentDaoException(e);
			}
		}
		if (con != null) {
			try {
				if (!con.isClosed()) {
					con.close();
					con = null;
				}
			} catch (SQLException e) {
				LOGGER.error(CodeHelper.getStackTrace(e));
				throw new MibImpairmentDaoException(e);
			}
		} 
		LOGGER.info("disconnect end");
		
	}

	static class BaseDaoHelper {
		public static String getDataSourceConfig() {
			
			LOGGER.info("get MIB DataSourceConfig start");
			ResourceBundle rb = ResourceBundle.getBundle(IUMConstants.IUM_CONFIG);
			String dataSourceConfig = rb.getString(IUMConstants.IUM_DBLOCATION_MIB);
			LOGGER.info("get MIB DataSourceConfig end");
			return dataSourceConfig;
		}
	}
}
