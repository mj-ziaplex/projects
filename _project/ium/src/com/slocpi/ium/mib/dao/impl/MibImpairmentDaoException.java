package com.slocpi.ium.mib.dao.impl;

public class MibImpairmentDaoException extends Exception {
	
	public MibImpairmentDaoException(Exception e) {
		super(e);
	}
	
}
