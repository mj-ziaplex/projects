package com.slocpi.ium.mib.service.impl;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.slocpi.ium.data.ClientData;
import com.slocpi.ium.data.ImpairmentData;
import com.slocpi.ium.data.MibMemberData;
import com.slocpi.ium.data.MibTransactionData;
import com.slocpi.ium.data.dao.MIBActionCHDao;
import com.slocpi.ium.data.dao.MIBImpairmentsCHDao;
import com.slocpi.ium.data.dao.MIBLetterCHDao;
import com.slocpi.ium.data.dao.MIBNumberCHDao;
import com.slocpi.ium.mib.dao.MibImpairmentDao;
import com.slocpi.ium.mib.dao.MibMemberDao;
import com.slocpi.ium.mib.dao.MibTransactionDao;
import com.slocpi.ium.mib.dao.impl.MibImpairmentDaoException;
import com.slocpi.ium.mib.dao.impl.MibImpairmentDaoImpl;
import com.slocpi.ium.mib.dao.impl.MibMemberDaoImpl;
import com.slocpi.ium.mib.dao.impl.MibTransactionDaoImpl;
import com.slocpi.ium.mib.service.MibImpairmentService;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.NameValuePair;

/**
 * @author pz07
 *
 */
public class MibImpairmentServiceImpl implements MibImpairmentService {

	private static final Logger LOGGER = LoggerFactory.getLogger(MibImpairmentServiceImpl.class);
	private MibImpairmentDao impDao = new MibImpairmentDaoImpl();
	private MibTransactionDao tranDao = new MibTransactionDaoImpl();
	private MibMemberDao memberDao = new MibMemberDaoImpl();
	
	/* (non-Javadoc)
	 * @see com.slocpi.ium.interfaces.mib.service.MibImpairmentsService#addImpairment(com.slocpi.ium.data.MIBImpairment)
	 */
	public int addImpairment(ClientData clientData, ImpairmentData impairmentData, String impClientType) throws MibImpairmentDaoException, SQLException {
		
		LOGGER.info("addImpairment start");
		int result = impDao.createImpairment(impairmentData, clientData, impClientType);
		LOGGER.info("addImpairment end");
		return result;
	}
 
	public int addMember(ClientData clientData, ImpairmentData impairmentData, String impClientType) throws MibImpairmentDaoException {
		
		LOGGER.info("addMember start");
		
		MibMemberData mibMemberData = new MibMemberData(clientData, impairmentData);
		MibMemberData retrieveMember = memberDao.retrieveMember(impairmentData, mibMemberData, impClientType);
		int result = 0;
		if (retrieveMember != null) {
			LOGGER.debug("MibImpairmentServiceImpl createMember(ClientData clientData, ImpairmentData impairmentData) member is existing");
		} else {
			result = memberDao.createMember(impairmentData, mibMemberData, impClientType);
			LOGGER.debug("MibImpairmentServiceImpl createMember(ClientData clientData, ImpairmentData impairmentData)  member is added");
		}
		
		LOGGER.info("addMember start");
		return result;
	}
	
	/* (non-Javadoc)
	 * @see com.slocpi.ium.mib.service.MibImpairmentService#addTransaction(com.slocpi.ium.data.MibTransactionData)
	 */
	public int addTransaction(MibTransactionData mibTransactionData) throws MibImpairmentDaoException {
		
		return tranDao.createTransaction(mibTransactionData);
	}
	
	/* (non-Javadoc)
	 * @see com.slocpi.ium.mib.service.MibImpairmentService#retrieveImpairmentList(java.lang.String)
	 */
	public List retrieveImpairmentList(String referenceNumber) throws MibImpairmentDaoException {
		
		LOGGER.info("retrieveImpairmentList start");
		List impairmentList = impDao.retrieveImpairmentList(referenceNumber);
		for (int i=0; i < impairmentList.size(); i++) {
			ImpairmentData impairmentData = (ImpairmentData) impairmentList.get(i);
			String impairmentCode = impairmentData.getImpairmentCode();
		}
		LOGGER.info("retrieveImpairmentList end");
		
		return impairmentList;
	}

	/* (non-Javadoc)
	 * @see com.slocpi.ium.interfaces.mib.service.MibImpairmentsService#isImpairmentExisting(com.slocpi.ium.data.MIBImpairment)
	 */
	public boolean isImpairmentExisting(ImpairmentData impairmentData, String impClientType) throws MibImpairmentDaoException {
		
		LOGGER.info("isImpairmentExisting start");
		ImpairmentData retrievedImpairmentData = impDao.retrieveImpairment(impairmentData, impClientType);
		if (retrievedImpairmentData == null) {
			LOGGER.info("isImpairmentExisting end");
			return false;
		}
		LOGGER.info("isImpairmentExisting end");
		return true;
	}

	private String getImpairmentDesc(String impairmentCode) throws MibImpairmentDaoException{
		
		LOGGER.info("getImpairmentDesc start");
		MIBImpairmentsCHDao impDao = null;
		String impairmentDesc = null;
		try {
			impDao = new MIBImpairmentsCHDao();
			List codeValue = (ArrayList) impDao.getCodeValue(impairmentCode);
			NameValuePair nvp = (NameValuePair) codeValue.get(0);
			impairmentDesc = nvp.getName();
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new MibImpairmentDaoException(e);
		}
		LOGGER.info("getImpairmentDesc end");
		return impairmentDesc;
	}
	
	private String getRelationship(String relationshipCode, Connection con)  throws MibImpairmentDaoException {
		
		LOGGER.info("getRelationship start");
		String relationshipDesc = null;
		MIBNumberCHDao numDao = null;
		try {
			numDao = new MIBNumberCHDao(con);
			List codeValue = (ArrayList) numDao.getCodeValue(relationshipCode);
			NameValuePair nvp = (NameValuePair) codeValue.get(0);
			relationshipDesc = nvp.getName();
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new MibImpairmentDaoException(e);
		}
		LOGGER.info("getRelationship end");
		return relationshipDesc;
	}
	
	private String getOrigin(String originCode, Connection con) throws MibImpairmentDaoException {
		
		LOGGER.info("getOrigin start");
		String originDesc = null;
		MIBLetterCHDao letDao = null;
		try {
			letDao = new MIBLetterCHDao(con);
			List codeValue = (ArrayList) letDao.getCodeValue(originCode);
			NameValuePair nvp = (NameValuePair) codeValue.get(0);
			originDesc = nvp.getName();
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new MibImpairmentDaoException(e);
		}LOGGER.info("getOrigin end");
		return originDesc;
	}
	
	private String getActionDesc(String actionCode, Connection con) throws MibImpairmentDaoException {
		
		LOGGER.info("getActionDesc start");
		String actionDesc = null;
		MIBActionCHDao actDao = null;
		try {
			actDao = new MIBActionCHDao(con);
			List codeValue = (ArrayList) actDao.getCodeValue(actionCode);
			NameValuePair nvp = (NameValuePair) codeValue.get(0);
			actionDesc = nvp.getName();
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new MibImpairmentDaoException(e);
		}
		LOGGER.info("getActionDesc end");
		return actionDesc;		
	}

}
