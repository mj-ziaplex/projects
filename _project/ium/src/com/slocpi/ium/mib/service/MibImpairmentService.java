package com.slocpi.ium.mib.service;

import java.sql.SQLException;
import java.util.List;
import com.slocpi.ium.data.ClientData;
import com.slocpi.ium.data.ImpairmentData;
import com.slocpi.ium.data.MibTransactionData;
import com.slocpi.ium.mib.dao.impl.MibImpairmentDaoException;

/**
 * @author pz07
 *
 */
public interface MibImpairmentService {

	/**
	 * @param clientData 
	 * @param impClientType 
	 * @param data
	 * @return
	 * @throws MibImpairmentDaoException 
	 * @throws SQLException 
	 */
	public int addImpairment(ClientData clientData, ImpairmentData impairmentData, String impClientType) throws MibImpairmentDaoException, SQLException;
	
	/**
	 * @param clientData
	 * @param impairmentData
	 * @return
	 * @throws MibImpairmentDaoException
	 */
	public int addMember(ClientData clientData, ImpairmentData impairmentData, String impClientType) throws MibImpairmentDaoException;
		
	/**
	 * @param mibImpairment
	 * @return
	 * @throws MibImpairmentDaoException 
	 */
	public boolean isImpairmentExisting(ImpairmentData data, String impClientType) throws MibImpairmentDaoException;

	/**
	 * @param referenceNumber
	 * @return
	 * @throws MibImpairmentDaoException
	 */
	public List retrieveImpairmentList(String referenceNumber) throws MibImpairmentDaoException;
	
	/**
	 * @param mibTransactionData
	 * @return
	 */
	public int addTransaction(MibTransactionData mibTransactionData) throws MibImpairmentDaoException;
}
