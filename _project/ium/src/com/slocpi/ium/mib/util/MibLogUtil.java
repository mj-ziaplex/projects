package com.slocpi.ium.mib.util;

public class MibLogUtil {

	public static void log(String className, String methodName, String message) {
		System.out.println("[" + className + ": " + methodName + "] " + message);
	}
}
