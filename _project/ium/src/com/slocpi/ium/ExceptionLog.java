/*
 * Created on Feb 19, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium;

import java.util.ArrayList;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

/**
 * @author ypojol
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class ExceptionLog {
	
	private String exceptionID;
	private String referenceNumber;
	private String systemInterface;
	private String requestMsg;
	private String reply;
	private String recordType;
	private String timestamp;
	private ArrayList details;
	private String xmlRecord;
	
	public String toString(){
		
		return ToStringBuilder.reflectionToString(this, ToStringStyle.DEFAULT_STYLE);
	}
	
	/**
	 * 
	 */
	public ExceptionLog() {
		super();
		// TODO Auto-generated constructor stub
	}


	/**
	 * @return
	 */
	public ArrayList getDetails() {
		return details;
	}

	/**
	 * @return
	 */
	public String getExceptionID() {
		return exceptionID;
	}

	/**
	 * @return
	 */
	public String getRecordType() {
		return recordType;
	}

	/**
	 * @return
	 */
	public String getReferenceNumber() {
		return referenceNumber;
	}


	/**
	 * @return
	 */
	public String getTimestamp() {
		return timestamp;
	}



	/**
	 * @param list
	 */
	public void setDetails(ArrayList list) {
		details = list;
	}

	/**
	 * @param string
	 */
	public void setExceptionID(String string) {
		exceptionID = string;
	}

	/**
	 * @param string
	 */
	public void setRecordType(String string) {
		recordType = string;
	}

	/**
	 * @param string
	 */
	public void setReferenceNumber(String string) {
		referenceNumber = string;
	}

	/**
	 * @param string
	 */
	public void setTimestamp(String string) {
		timestamp = string;
	}


	/**
	 * @return
	 */
	public String getReply() {
		return reply;
	}

	/**
	 * @return
	 */
	public String getRequestMsg() {
		return requestMsg;
	}

	/**
	 * @param string
	 */
	public void setReply(String sb) {
		reply = sb;
	}

	/**
	 * @param string
	 */
	public void setRequestMsg(String string) {
		requestMsg = string;
	}

	/**
	 * @return
	 */
	public String getSystemInterface() {
		return systemInterface;
	}

	/**
	 * @param string
	 */
	public void setSystemInterface(String string) {
		systemInterface = string;
	}

	/**
	 * @return
	 */
	public String getXmlRecord() {
		return xmlRecord;
	}

	/**
	 * @param string
	 */
	public void setXmlRecord(String string) {
		xmlRecord = string;
	}

}
