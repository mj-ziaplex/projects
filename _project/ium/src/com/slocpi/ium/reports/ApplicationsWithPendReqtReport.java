/*
 * Created on Feb 22, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.reports;

import java.io.ByteArrayOutputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.slocpi.ium.IUMException;
import com.slocpi.ium.data.ARWithPendingReqtData;
import com.slocpi.ium.data.ARWithPendingReqtFilterData;
import com.slocpi.ium.data.dao.PolicyRequirementDAO;
import com.slocpi.ium.data.util.DataSourceProxy;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.forms.PrintableForm;

/**
 * @author Nic Decapia
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class ApplicationsWithPendReqtReport
{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ApplicationsWithPendReqtReport.class);
	
	public ArrayList generate(ARWithPendingReqtFilterData criteria) throws IUMException {
		
		LOGGER.info("generate start");
		ArrayList report = new ArrayList();
		
		try{
			PolicyRequirementDAO daoPolicyReqt = new PolicyRequirementDAO();
			report = daoPolicyReqt.retrieveARWithPendingReqt(criteria);
			if (report.size() > 0) report = applyLayout(report, criteria);
		}
		catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMException (e);
		}
		
		LOGGER.info("generate end");
		return (report);
	}
	
	private ArrayList applyLayout(ArrayList content, ARWithPendingReqtFilterData filter) {
		
		LOGGER.info("applyLayout start");
		 ArrayList newContent = new ArrayList();
		 int grandTotal = 0;
		 int type = filter.getReportType();
		 
		ArrayList tmpContent = new ArrayList();
		
		 for (int i=0; i<content.size(); i++) {
			
			ARWithPendingReqtData data = (ARWithPendingReqtData) content.get(i);
			boolean isHeaderRecord = false;
			isHeaderRecord = (data.getReferenceNumber().length() == 0);

			if (isHeaderRecord) {  
				newContent.add(data); 
				grandTotal += data.getTotalCount(); 
				newContent.addAll(tmpContent);
				tmpContent.clear();
			}
			else { 
				switch (type) {
				   case ARWithPendingReqtFilterData.BRANCH_REPORT_TYPE:
						data.setBranchName("");
						break;
				   case ARWithPendingReqtFilterData.UNDWTR_REPORT_TYPE:
						data.setUnderwriterLastName("");
						data.setUnderwriterGivenName("");
						data.setUnderwriterMidName("");						
						break;	
				}
				data.setTotalCount(0);
				tmpContent.add(data); 
			}
		 }
		 
		 String GRAND_TOTAL = "Grand Total";
		ARWithPendingReqtData totalRec = new ARWithPendingReqtData();
		 switch (type) {
			case ARWithPendingReqtFilterData.BRANCH_REPORT_TYPE:
				totalRec.setBranchName(GRAND_TOTAL);
				break;	
			case ARWithPendingReqtFilterData.UNDWTR_REPORT_TYPE:
				totalRec.setUnderwriterLastName(GRAND_TOTAL);
				break;
		 }
		totalRec.setTotalCount(grandTotal);
		newContent.add(totalRec); 
		LOGGER.info("applyLayout end");
		 return (newContent);
	}

	/**
	 * @param filter
	 * @return
	 */
	public ByteArrayOutputStream generatePrinterFriendly(ARWithPendingReqtFilterData filter)
	{
		
		LOGGER.info("generatePrinterFriendly start");
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ArrayList report = new ArrayList();
		try
		{
			
			report = this.generate(filter); 

			PrintableForm pdfReport = new PrintableForm(PrintableForm.PAGE_LANDSCAPE);
			pdfReport.setReportTitle(IUMReportConstants.REPORT_TITLE_APPS_PENDS_REQT);
			String reportType = new String();
			switch (filter.getReportType()){
				case ARWithPendingReqtFilterData.BRANCH_REPORT_TYPE:
					reportType = IUMReportConstants.REPORT_TYPE_BY_BRANCH;
					pdfReport.defineColumnTitle(IUMReportConstants.REPORT_COLUMN_BRANCH);
					pdfReport.defineColumnTitle(IUMReportConstants.REPORT_COLUMN_UNDERWRITER);
					break;
				case ARWithPendingReqtFilterData.UNDWTR_REPORT_TYPE:
					reportType = IUMReportConstants.REPORT_TYPE_BY_UNDERWRITER;
					pdfReport.defineColumnTitle(IUMReportConstants.REPORT_COLUMN_UNDERWRITER);		
					pdfReport.defineColumnTitle(IUMReportConstants.REPORT_COLUMN_BRANCH);
					break;	
			}
			pdfReport.defineColumnTitle(IUMReportConstants.REPORT_COLUMN_AR_NO);
			pdfReport.defineColumnTitle(IUMReportConstants.REPORT_COLUMN_REQUIREMENT);
			pdfReport.defineColumnTitle(IUMReportConstants.REPORT_COLUMN_DATE_ORD);
			pdfReport.defineColumnTitle(IUMReportConstants.REPORT_COLUMN_DATE_SUB);
			pdfReport.defineColumnTitle(IUMReportConstants.REPORT_COLUMN_TOTAL_COUNT);
						
			pdfReport.defineParameter(filter.getStartDate(), filter.getEndDate());
			pdfReport.defineParameter(IUMReportConstants.PARAMETER_TYPE, reportType);
			
			pdfReport.setColumnWidths(new int[] {20, 20, 15, 20, 10, 10, 5});

			
			for (int i=0; i<report.size(); i++) {
			
				ARWithPendingReqtData data = (ARWithPendingReqtData) report.get(i);
				String referenceNumber = data.getReferenceNumber();
				String underwriterName = ""; 
				String totalCount = (data.getTotalCount() != 0 ? String.valueOf(data.getTotalCount()) : "");
				
				int rowType = PrintableForm.COL_TYPE_NORMAL;
				if (referenceNumber == "" && i != report.size()) rowType = PrintableForm.COL_TYPE_GROUP; // this is a header record
				pdfReport.setColumnValueWidths(new int[] {20, 20, 15, 5, 15, 10, 10, 5});

				switch (filter.getReportType()){
					case ARWithPendingReqtFilterData.BRANCH_REPORT_TYPE:
						underwriterName = (data.getUnderwriterLastName().length() != 0 ? data.getUnderwriterLastName() + ", " + data.getUnderwriterGivenName() : "");
						if (data.getBranchName().equals(IUMReportConstants.REPORT_REC_GTOTAL))  {
							rowType = PrintableForm.COL_TYPE_GROUP;
							pdfReport.writeEndContent(); 
						}
						pdfReport.defineContentValue(data.getBranchName(), PrintableForm.ALIGN_COL_LEFT, rowType);
						pdfReport.defineContentValue(underwriterName, PrintableForm.ALIGN_COL_LEFT, rowType);
						break;
					case ARWithPendingReqtFilterData.UNDWTR_REPORT_TYPE:
						if (data.getUnderwriterLastName().equals(IUMReportConstants.REPORT_REC_GTOTAL)) { 
							rowType = PrintableForm.COL_TYPE_GROUP;
							pdfReport.writeEndContent(); 
							underwriterName = data.getUnderwriterLastName();
						}
						else {
							underwriterName = (data.getUnderwriterLastName().length() != 0 ? data.getUnderwriterLastName() + ", " + data.getUnderwriterGivenName() : "");
						}
						pdfReport.defineContentValue(underwriterName, PrintableForm.ALIGN_COL_LEFT, rowType);
						pdfReport.defineContentValue(data.getBranchName(), PrintableForm.ALIGN_COL_LEFT, rowType);
						break;	
				}
				pdfReport.defineContentValue(referenceNumber, PrintableForm.ALIGN_COL_LEFT, rowType);
				pdfReport.defineContentValue(data.getRequirementCode(), PrintableForm.ALIGN_COL_LEFT, rowType);				
				pdfReport.defineContentValue(data.getRequirementName(), PrintableForm.ALIGN_COL_LEFT, rowType);
				pdfReport.defineContentValue(data.getDateOrdered(), PrintableForm.ALIGN_COL_LEFT, rowType);
				pdfReport.defineContentValue(data.getDateSubmit(), PrintableForm.ALIGN_COL_LEFT, rowType);
				pdfReport.defineContentValue(totalCount, PrintableForm.ALIGN_COL_RIGHT, rowType);
			}
			pdfReport.writeEndBar();
			
			
			baos = pdfReport.writeReport(report);			
						
		}
		catch (Exception e)
		{
			LOGGER.error(CodeHelper.getStackTrace(e));
		}
		LOGGER.info("generatePrinterFriendly end");
		return (baos);
	}	


}
