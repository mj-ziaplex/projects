/*
 * Created on Mar 19, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.reports;

/**
 * @author Nic Decapia
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class IUMReportConstants
{
	
	public static final String APPLICATION_NAME = "Integrated Underwriting and Medical (IUM) System";
	public static final String COMPANY_NAME = "Sun Life of Canada (Philippines), Inc.";

	// Report Titles
	public static final String REPORT_TITLE_TAT = "Turn Around Time (TAT) per Underwriting Assessment Request";
	public static final String REPORT_TITLE_REQT_ORD = "Number of Requirements Ordered";
	public static final String REPORT_TITLE_STMT_MED = "Statement of Medical Exam Billing";
	public static final String REPORT_TITLE_SUMM_MED = "Summary of Medical Exam Billing";
	public static final String REPORT_TITLE_APPS_APPROVED = "Number of Applications Approved";
	public static final String REPORT_TITLE_SUMM_APP_STATUS = "Summary of Applications by Status";
	public static final String REPORT_TITLE_APPS_PENDS_REQT = "List of Applications with Pending Requirements";
	public static final String REPORT_TITLE_APPS_PENDS_MED = "List of Applications with Pending Medical Results";

	// Report Types
	public static final int PARAMETER_PERIOD = 0;
	public static final int PARAMETER_TYPE = 1;
	public static final int PARAMETER_STATUS = 2;
	public static final int PARAMETER_COUNT = 3;

	// Report Parameter Titles
	public static final String REPORT_PARAMETER_PERIOD = "Period: ";
	public static final String REPORT_PARAMETER_TYPE = "Type: ";
	public static final String REPORT_PARAMETER_AR_STATUS = "Request Status: ";
	public static final String REPORT_PARAMETER_COUNT_APP = "Total No. of Applications Approved is ";
	
	// Report Type Titles
	public static final String REPORT_TYPE_BY_BRANCH = "By Branch";
	public static final String REPORT_TYPE_BY_REQUIREMENT = "By Requirement";
	public static final String REPORT_TYPE_BY_UNDERWRITER = "By Underwriter/User";
	public static final String REPORT_TYPE_BY_AGENT = "By Agent";
	public static final String REPORT_TYPE_BY_EXAMINER = "By Examiner";
	public static final String REPORT_TYPE_BY_LABORATORY = "By Laboratory";	
	public static final String REPORT_TYPE_PER_AGENT = "per Agent";
	public static final String REPORT_TYPE_PER_EXAMINER = "per Examiner";
	public static final String REPORT_TYPE_PER_LABORATORY = "per Laboratory";
	public static final String REPORT_TYPE_BY_LOB = "by LOB";

	// Report Column Titles
	public static final String REPORT_COLUMN_BRANCH = "Sun Life Office/Branch";
	public static final String REPORT_COLUMN_REQUIREMENT = "Requirement";
	public static final String REPORT_COLUMN_UNDERWRITER = "Underwriter/User";
	public static final String REPORT_COLUMN_ORDERED = "Ordered";
	public static final String REPORT_COLUMN_LAB_EXAM_AGENT = "Examiner/Laboratory Agent";
	public static final String REPORT_COLUMN_LAB_EXAM = "Examiner/Laboratory";
	public static final String REPORT_COLUMN_EXAM_AGENT = "Examiner Agent";
	public static final String REPORT_COLUMN_LAB_AGENT = "Laboratory Agent";
	public static final String REPORT_COLUMN_EXAM_DATE = "Exam Date";
	public static final String REPORT_COLUMN_EXAM_TYPE = "Exam Type";
	public static final String REPORT_COLUMN_AR_POL_NO  = "Application/Policy Number";
	public static final String REPORT_COLUMN_AR_NO  = "Request Reference No.";
	public static final String REPORT_COLUMN_NAME_INSURED = "Name of Insured";
	public static final String REPORT_COLUMN_LOB = "LOB";
	public static final String REPORT_COLUMN_BUSINESS_LINE = "Business Line";
	public static final String REPORT_COLUMN_REASON = "Reason";
	public static final String REPORT_COLUMN_EXAM_PLACE = "Place of Exam";
	public static final String REPORT_COLUMN_EXAM_FEE= "Exam Fee";
	public static final String REPORT_COLUMN_TAX_WHELD = "Tax W/held";
	public static final String REPORT_COLUMN_CHRG_AMT = "Chargable Amount";
	public static final String REPORT_COLUMN_PAYABLE = "Payable Amount";
	public static final String REPORT_COLUMN_ELAPSED_TIME = "Elapsed Time";
	public static final String REPORT_COLUMN_REQUESTS_PROC = "Requests Processed";
	public static final String REPORT_COLUMN_AVERAGE_TAT = "Average TAT";
	public static final String REPORT_COLUMN_STATUS_PERIOD = "Request Status Period";
	public static final String REPORT_COLUMN_START = "Start";
	public static final String REPORT_COLUMN_END = "End";
	public static final String REPORT_COLUMN_AGENT = "Agent";
	public static final String REPORT_COLUMN_EXAMINER = "Examiner";
	public static final String REPORT_COLUMN_LABORATORY = "Laboratory";
	public static final String REPORT_COLUMN_APPROVER = "Requesting Party";
	public static final String REPORT_COLUMN_REQUESTOR = "Approving Party";
	public static final String REPORT_COLUMN_DATE = "Date";
	public static final String REPORT_COLUMN_STATUS = "Status";
	public static final String REPORT_COLUMN_TOTAL_COUNT = "Total Count";
	public static final String REPORT_COLUMN_DATE_ORD = "Date Ordered";
	public static final String REPORT_COLUMN_DATE_REQUEST = "Date Requested";
	public static final String REPORT_COLUMN_DATE_SUB = "Target Date of Submission";
	public static final String REPORT_COLUMN_TEST = "Test";

	
	public static final String REPORT_COLUMN_MODIFIER_TOTAL = " Total ";

	// Period Type Formats (input and output)
	public static final String REPORT_PARAM_DATE_FORMAT = "ddMMMyyyy";
	public static final String REPORT_DATE_FORMAT = "MMMMM dd, yyyy";
	
	// TAT Report Units
	public static final String REPORT_TAT_DAYS = "(Days)";
	public static final String REPORT_TAT_HOUR = "(Hours)";
	
	public static final String REPORT_REC_GTOTAVE = "Total / Overall Average";
	public static final String REPORT_REC_GTOTAL = "Grand Total"; 	 
}
