/*
 * Created on Mar 9, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.reports;

import java.io.ByteArrayOutputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.slocpi.ium.IUMException;
import com.slocpi.ium.data.MedicalExamBillingData;
import com.slocpi.ium.data.MedicalExamBillingFilter;
import com.slocpi.ium.data.dao.MedicalDAO;
import com.slocpi.ium.data.util.DataSourceProxy;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.forms.PrintableForm;

/**
 * @author nic.decapia
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class SummaryMedicalBillReport {

	private static final Logger LOGGER = LoggerFactory.getLogger(SummaryMedicalBillReport.class);
	/**
	 * @param filter
	 * @return
	 */
	public ArrayList generate(MedicalExamBillingFilter filter) throws IUMException {
		
		LOGGER.info("generate start");
		ArrayList report = new ArrayList();
		DataSourceProxy dsp = new DataSourceProxy();
		Connection con = dsp.getConnection();
		try {
			MedicalDAO daoMed = new MedicalDAO(con);
			report = daoMed.retrieveMedicalExamBilling(filter);
			if (report.size() > 0) report = applyLayout(report, filter);
		}                
		catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMException (e);
		}
		finally {
			dsp.closeConnection(con);
		}
		LOGGER.info("generate end");
		return (report);
	}

	/**
	 * @param report
	 * @param filter
	 * @return
	 */
	private ArrayList applyLayout(ArrayList content, MedicalExamBillingFilter filter)
	{
		
		LOGGER.info("applyLayout start");
		ArrayList newContent = new ArrayList();

		int grandTotalExamFee = 0;
		int grandTotalTaxHeld = 0;
		int grandTotalPayable = 0;
		int grandTotalCharge = 0;
		
		for (int i=0; i<content.size(); i++) {
			MedicalExamBillingData rptData = (MedicalExamBillingData) content.get(i);
			
			if (rptData.getBranch() == null) {
				rptData.setInsured("");
				
				newContent.add(rptData); 
				grandTotalExamFee += Integer.parseInt(rptData.getExamFee() == null ? "0":rptData.getExamFee());
				grandTotalTaxHeld += Integer.parseInt(rptData.getTaxWithheld()== null ?"0":rptData.getTaxWithheld());
				grandTotalPayable += Integer.parseInt(rptData.getPayableAmt()==null?"0":rptData.getPayableAmt());
				grandTotalCharge += Integer.parseInt(rptData.getChargeableAmt()==null?"0":rptData.getChargeableAmt());				
			}
		}
		
		MedicalExamBillingData rptTotal = new MedicalExamBillingData();
		rptTotal.setName("Grand Total");
		rptTotal.setExamFee(String.valueOf(grandTotalExamFee));
		rptTotal.setTaxWithheld(String.valueOf(grandTotalTaxHeld));
		rptTotal.setPayableAmt(String.valueOf(grandTotalPayable));
		rptTotal.setChargeableAmt(String.valueOf(grandTotalCharge));
		newContent.add(rptTotal);
		
		LOGGER.info("applyLayout end");
		return (newContent);
	}

	/**
	 * @param filter
	 * @return
	 */
	public ByteArrayOutputStream generatePrinterFriendly(MedicalExamBillingFilter filter) {
		
		LOGGER.info("generatePrinterFriendly start");
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ArrayList report = new ArrayList();
		try
		{
			
			report = this.generate(filter); 
			PrintableForm pdfReport = new PrintableForm(PrintableForm.PAGE_LANDSCAPE);
			pdfReport.setReportTitle(IUMReportConstants.REPORT_TITLE_SUMM_MED);
			
			String reportType = new String();
			switch (filter.getReportType()){
				case MedicalExamBillingFilter.REPORT_TYPE_AGENT:
					reportType = IUMReportConstants.REPORT_TYPE_BY_AGENT;
					pdfReport.defineColumnTitle(IUMReportConstants.REPORT_COLUMN_AGENT);
					pdfReport.setColumnWidths(new int[] {40, 15, 15, 15, 15});
					break;
				case MedicalExamBillingFilter.REPORT_TYPE_EXAMINER:
					reportType = IUMReportConstants.REPORT_TYPE_BY_EXAMINER;
					pdfReport.defineColumnTitle(IUMReportConstants.REPORT_COLUMN_EXAMINER);		
					pdfReport.setColumnWidths(new int[] {55, 15, 15, 15});
					break;	
				case MedicalExamBillingFilter.REPORT_TYPE_LABORATORY:
					reportType = IUMReportConstants.REPORT_TYPE_BY_LABORATORY;
					pdfReport.defineColumnTitle(IUMReportConstants.REPORT_COLUMN_LABORATORY);
					pdfReport.setColumnWidths(new int[] {55, 15, 15, 15});
					break; 
			}

			pdfReport.defineColumnTitle(IUMReportConstants.REPORT_COLUMN_MODIFIER_TOTAL + IUMReportConstants.REPORT_COLUMN_EXAM_FEE);
			pdfReport.defineColumnTitle(IUMReportConstants.REPORT_COLUMN_MODIFIER_TOTAL + IUMReportConstants.REPORT_COLUMN_TAX_WHELD);
			if (reportType == IUMReportConstants.REPORT_TYPE_BY_AGENT) pdfReport.defineColumnTitle(IUMReportConstants.REPORT_COLUMN_PAYABLE);
			pdfReport.defineColumnTitle(IUMReportConstants.REPORT_COLUMN_CHRG_AMT);
			
			pdfReport.defineParameter(filter.getStartDate(), filter.getEndDate());		
			pdfReport.defineParameter(IUMReportConstants.PARAMETER_TYPE, reportType);
			
			
			for (int i=0; i<report.size(); i++) {
				
				MedicalExamBillingData data = (MedicalExamBillingData) report.get(i);
				int rowType = PrintableForm.COL_TYPE_NORMAL;

				if (data.getName().equals(IUMReportConstants.REPORT_REC_GTOTAL) && i == report.size() - 1) {
					pdfReport.writeEndContent(); 
					rowType = PrintableForm.COL_TYPE_GROUP; 
				} 
				
				pdfReport.defineContentValue(data.getName(), PrintableForm.ALIGN_COL_LEFT, rowType);
				pdfReport.defineContentValue(data.getExamFee(), PrintableForm.ALIGN_COL_RIGHT, rowType);
				pdfReport.defineContentValue(data.getTaxWithheld(), PrintableForm.ALIGN_COL_RIGHT, rowType);
				if (reportType == IUMReportConstants.REPORT_TYPE_BY_AGENT) pdfReport.defineContentValue(data.getPayableAmt(), PrintableForm.ALIGN_COL_RIGHT, rowType);
				pdfReport.defineContentValue(data.getChargeableAmt(), PrintableForm.ALIGN_COL_RIGHT, rowType);

			}
			pdfReport.writeEndBar();
			baos = pdfReport.writeReport(report);			
						
		}
		catch (Exception e)
		{
			LOGGER.error(CodeHelper.getStackTrace(e));
		}
		LOGGER.info("generatePrinterFriendly end");
		return (baos);

	}

	

}
