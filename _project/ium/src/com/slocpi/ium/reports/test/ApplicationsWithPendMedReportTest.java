/*
 * Created on Mar 5, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.reports.test;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import com.slocpi.ium.data.ARWithPendingMedData;
import com.slocpi.ium.data.ARWithPendingMedFilterData;
import com.slocpi.ium.data.util.DataSourceProxy;
import com.slocpi.ium.reports.ApplicationsWithPendMedReport;
import com.slocpi.ium.util.ValueConverter;

/**
 * @author Nic Decapia
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class ApplicationsWithPendMedReportTest extends TestCase
{
	/**
	 * Constructor for ApplicationsWithPendMedReportTest.
	 * @param arg0
	 */
	public ApplicationsWithPendMedReportTest(String arg0)
	{
		super(arg0);
	}

	public static Test suite() {
		TestSuite suite = new TestSuite(ApplicationsWithPendMedReportTest.class);
		return (suite);
	}
	
	public static void main(String[] args)
	{
		junit.textui.TestRunner.run(ApplicationsWithPendMedReportTest.class);
	}

	public void testGenerateByUnderwriter()
	{
		//TODO Implement generate().
		ArrayList report = new ArrayList();
		ARWithPendingMedFilterData filter = this.getFilter(ARWithPendingMedFilterData.UNDERWRITER_REPORT_TYPE);
		
		try {
			ApplicationsWithPendMedReport rpt = new ApplicationsWithPendMedReport();
			report = rpt.generate(filter);			
		}
		catch (Exception e){
			e.printStackTrace();
		}
		
		assertEquals("testGenerateByUnderwriter(): ", true, isEqual(report, this.getTestDataByUnderwriter(), ARWithPendingMedFilterData.UNDERWRITER_REPORT_TYPE));
	}

	public void testGenerateByBranch()
	{
		//TODO Implement generate().
		ArrayList report = new ArrayList();
		ARWithPendingMedFilterData filter = this.getFilter(ARWithPendingMedFilterData.BRANCH_REPORT_TYPE);
		
		try {
			ApplicationsWithPendMedReport rpt = new ApplicationsWithPendMedReport();
			report = rpt.generate(filter);			
		}
		catch (Exception e){
			e.printStackTrace();
		}
		
		assertEquals("testGenerateByBranch(): ", true, isEqual(report, this.getTestDataByBranch(), ARWithPendingMedFilterData.BRANCH_REPORT_TYPE));
	}

	public void testGenerateByLaboratory()
	{
		//TODO Implement generate().
		ArrayList report = new ArrayList();
		ARWithPendingMedFilterData filter = this.getFilter(ARWithPendingMedFilterData.LABORATORY_REPORT_TYPE);
		
		try {
			ApplicationsWithPendMedReport rpt = new ApplicationsWithPendMedReport();
			report = rpt.generate(filter);			
		}
		catch (Exception e){
			e.printStackTrace();
		}
		
		assertEquals("testGenerateByLaboratory(): ", true, isEqual(report, this.getTestDataByLaboratory(), ARWithPendingMedFilterData.LABORATORY_REPORT_TYPE));
	}

	public void testGenerateByExaminer()
	{
		//TODO Implement generate().
		ArrayList report = new ArrayList();
		ARWithPendingMedFilterData filter = this.getFilter(ARWithPendingMedFilterData.EXAMINER_REPORT_TYPE);
		
		try {
			ApplicationsWithPendMedReport rpt = new ApplicationsWithPendMedReport();
			report = rpt.generate(filter);			
		}
		catch (Exception e){
			e.printStackTrace();
		}
		
		assertEquals("testGenerateByExaminer(): ", true, isEqual(report, this.getTestDataByExaminer(), ARWithPendingMedFilterData.EXAMINER_REPORT_TYPE));
	}

	/**
	 * @param report
	 * @param object
	 */
	private boolean isEqual(ArrayList fromComponent, ArrayList fromTestCase, int typeReport)
	{
		boolean equal = true;
		
		// check the record counts
		if (fromComponent.size() != fromTestCase.size()) {
			return false;
		}

		// check the contents
		for (int i = 0; i < fromComponent.size(); i++) {
			ARWithPendingMedData dc = (ARWithPendingMedData) fromComponent.get(i);
			ARWithPendingMedData dt = (ARWithPendingMedData) fromTestCase.get(i);
			
			switch (typeReport) {
				case ARWithPendingMedFilterData.BRANCH_REPORT_TYPE:
				case ARWithPendingMedFilterData.LABORATORY_REPORT_TYPE:
				case ARWithPendingMedFilterData.EXAMINER_REPORT_TYPE:
					if (!dc.getBranchName().equals(dt.getBranchName())) equal = false;
					break;
				case ARWithPendingMedFilterData.UNDERWRITER_REPORT_TYPE:
					if (!dc.getUnderwriterGivenName().equals(dt.getUnderwriterGivenName())) equal = false;
					if (!dc.getUnderwriterLastName().equals(dt.getUnderwriterLastName())) equal = false;
					if (!dc.getUnderwriterMiddleName().equals(dt.getUnderwriterMiddleName())) equal = false;
					break;
			}

			if (!dc.getNameExaminerLab().equals(dt.getNameExaminerLab())) equal = false;
			if (!dc.getReferenceNumber().equals(dt.getReferenceNumber())) equal = false;
			if (!dc.getTestDescription().equals(dt.getTestDescription())) equal = false;
			if (!dc.getTestType().equals(dt.getTestType())) equal = false;
			if (!dc.getDateRequested().equals(dt.getDateRequested())) equal = false;
			if (!dc.getDateSubmitted().equals(dt.getDateSubmitted())) equal = false;
			if (dc.getTotalCount() != dt.getTotalCount()) equal = false;
			
			if (equal == false) break;	// stop testing if there is inequality
				
		}
		
		return equal;
		// TODO Auto-generated method stub
		
	}

	/**
	 * 
	 */
	private ArrayList getTestDataByUnderwriter()
	{
		ArrayList testData = new ArrayList();
		StringBuffer sql = new StringBuffer();

		sql.append(" select ");
		sql.append("  US.USR_LAST_NAME as LAST_NAME ");
		sql.append(",  US.USR_FIRST_NAME as FIRST_NAME ");
		sql.append(",  US.USR_MIDDLE_NAME as MIDDLE_NAME ");
		sql.append(", PM.UAR_REFERENCE_NUM as REFERENCE_NUMBER ");
		sql.append(", decode(MR.MED_LAB_TEST_IND, '0', (select EX.EXMNR_LAST_NAME || ', ' || EX.EXMNR_GIVEN_NAME || ' ' || EX.EXMNR_MIDDLE_NAME from EXAMINERS EX where EX.EXMNR_ID = MR.EXMNR_ID) ");
		sql.append("                            , '1', (select LB.LAB_NAME from LABORATORIES LB where LB.LAB_ID = MR.MED_LAB_ID)) as NAME ");
		sql.append(", TP.TEST_DESC as TEST_NAME");
		sql.append(", decode(MR.MED_LAB_TEST_IND, '0', 'N', '1', 'Y') as LAB_TEST_IND");
		sql.append(", MR.MED_REQUEST_DATE as REQUEST_DATE");
		sql.append(", MR.MED_FOLLOW_UP_DATE as FOLLOWUP_DATE");
		sql.append(", count(PM.MED_RECORD_ID) as TOTAL_COUNT");
		sql.append(" from ");
		sql.append("  MEDICAL_RECORDS MR ");
		sql.append(", TEST_PROFILES TP ");
		sql.append(", USERS US ");
		sql.append(", ASSESSMENT_REQUESTS AR ");
		sql.append(", POLICY_MEDICAL_RECORDS PM");
		sql.append(" where ");
		sql.append("     MR.MED_RECORD_ID = PM.MED_RECORD_ID  ");
		sql.append(" and AR.REFERENCE_NUM = PM.UAR_REFERENCE_NUM ");
		sql.append(" and AR.UNDERWRITER = US.USER_ID ");
		sql.append(" and MR.MED_TEST_ID = TP.TEST_ID ");
		sql.append(" and MR.MED_STATUS IN  (18, 19) ");
		sql.append(" and MR.MED_REQUEST_DATE ");
		sql.append("     between to_date('01-JAN-2004', 'dd-MON-yyyy hh24:mi:ss')");
		sql.append("         and to_date('31-DEC-2004', 'dd-MON-yyyy hh24:mi:ss')");
		sql.append(" GROUP BY GROUPING SETS ( ");
		sql.append("  (US.USR_LAST_NAME, US.USR_FIRST_NAME, US.USR_MIDDLE_NAME) ");
		sql.append(", (US.USR_LAST_NAME, US.USR_FIRST_NAME, US.USR_MIDDLE_NAME, ");
		sql.append("   PM.UAR_REFERENCE_NUM, MR.EXMNR_ID, MR.MED_LAB_ID, MR.MED_LAB_TEST_IND, MR.EXMNR_ID, " );
		sql.append("   TP.TEST_DESC, MR.MED_LAB_TEST_IND, MR.MED_REQUEST_DATE, MR.MED_FOLLOW_UP_DATE))" );
		sql.append(" order by ");
		sql.append("  US.USR_LAST_NAME, US.USR_FIRST_NAME, US.USR_MIDDLE_NAME, PM.UAR_REFERENCE_NUM ");

		System.out.println (sql.toString());
		
		try {
			PreparedStatement ps = new DataSourceProxy().getConnection().prepareStatement(sql.toString());
			
			// execute the query
			ResultSet rs = ps.executeQuery();
						
			// fill up the data object
			while (rs.next()) {
				ARWithPendingMedData data = new ARWithPendingMedData();
				
				// underwriter name
				data.setUnderwriterLastName(ValueConverter.nullToString(rs.getString("LAST_NAME")));
				data.setUnderwriterGivenName(ValueConverter.nullToString(rs.getString("FIRST_NAME")));
				data.setUnderwriterMiddleName(ValueConverter.nullToString(rs.getString("MIDDLE_NAME")));
				data.setNameExaminerLab(ValueConverter.nullToString(rs.getString("NAME"))); // examiner or laboratory
				data.setReferenceNumber(ValueConverter.nullToString(rs.getString("REFERENCE_NUMBER"))); // assessment request number
				data.setTestDescription(ValueConverter.nullToString(rs.getString("TEST_NAME"))); // test name
				data.setTestType(ValueConverter.nullToString(rs.getString("LAB_TEST_IND"))); // indicates lab or exam
				data.setDateRequested(ValueConverter.nullToString(rs.getString("REQUEST_DATE")));
				data.setDateSubmitted(ValueConverter.nullToString(rs.getString("FOLLOWUP_DATE")));
				data.setTotalCount(rs.getInt("TOTAL_COUNT"));               
				
				testData.add(data);
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}		
				
		return (testData);
//		TODO Auto-generated method stub		
		
	}

	private ArrayList getTestDataByBranch()
	{
		ArrayList testData = new ArrayList();
		StringBuffer sql = new StringBuffer();

		sql.append(" select ");
		sql.append("  SLO.SLO_OFFICE_NAME as BRANCH_NAME ");
		sql.append(", PM.UAR_REFERENCE_NUM as REFERENCE_NUMBER ");
		sql.append(", decode(MR.MED_LAB_TEST_IND, '0', (select EX.EXMNR_LAST_NAME || ', ' || EX.EXMNR_GIVEN_NAME || ' ' || EX.EXMNR_MIDDLE_NAME from EXAMINERS EX where EX.EXMNR_ID = MR.EXMNR_ID) ");
		sql.append("                            , '1', (select LB.LAB_NAME from LABORATORIES LB where LB.LAB_ID = MR.MED_LAB_ID)) as NAME ");
		sql.append(", TP.TEST_DESC as TEST_NAME");
		sql.append(", decode(MR.MED_LAB_TEST_IND, '0', 'N', '1', 'Y') as LAB_TEST_IND");
		sql.append(", MR.MED_REQUEST_DATE as REQUEST_DATE");
		sql.append(", MR.MED_FOLLOW_UP_DATE as FOLLOWUP_DATE");
		sql.append(", count(PM.MED_RECORD_ID) as TOTAL_COUNT");
		sql.append(" from ");
		sql.append("  MEDICAL_RECORDS MR ");
		sql.append(", TEST_PROFILES TP ");
		sql.append(", SUNLIFE_OFFICES SLO ");
		sql.append(", ASSESSMENT_REQUESTS AR ");
		sql.append(", POLICY_MEDICAL_RECORDS PM");
		sql.append(" where ");
		sql.append("     MR.MED_RECORD_ID = PM.MED_RECORD_ID  ");
		sql.append(" and AR.REFERENCE_NUM = PM.UAR_REFERENCE_NUM ");
		sql.append(" and AR.BRANCH_ID = SLO.SLO_OFFICE_CODE ");
		sql.append(" and MR.MED_TEST_ID = TP.TEST_ID ");
		sql.append(" and MR.MED_STATUS IN  (18, 19) ");
		sql.append(" and MR.MED_REQUEST_DATE ");
		sql.append("     between to_date('01-JAN-2004 00:00:00', 'dd-MON-yyyy hh24:mi:ss')");
		sql.append("         and to_date('31-DEC-2004 23:59:59', 'dd-MON-yyyy hh24:mi:ss')");
		sql.append(" GROUP BY GROUPING SETS ( ");
		sql.append("  (SLO.SLO_OFFICE_NAME) ");
		sql.append(", (SLO.SLO_OFFICE_NAME, ");
		sql.append("   PM.UAR_REFERENCE_NUM, MR.EXMNR_ID, MR.MED_LAB_ID, MR.MED_LAB_TEST_IND, MR.EXMNR_ID, " );
		sql.append("   TP.TEST_DESC, MR.MED_LAB_TEST_IND, MR.MED_REQUEST_DATE, MR.MED_FOLLOW_UP_DATE))" );
		sql.append(" order by ");
		sql.append("  SLO.SLO_OFFICE_NAME, PM.UAR_REFERENCE_NUM ");

		System.out.println (sql.toString());
		
		try {
			PreparedStatement ps = new DataSourceProxy().getConnection().prepareStatement(sql.toString());
			
			// execute the query
			ResultSet rs = ps.executeQuery();
						
			// fill up the data object
			while (rs.next()) {
				ARWithPendingMedData data = new ARWithPendingMedData();
				
				data.setBranchName(ValueConverter.nullToString(rs.getString("BRANCH_NAME")));
				data.setNameExaminerLab(ValueConverter.nullToString(rs.getString("NAME"))); // examiner or laboratory
				data.setReferenceNumber(ValueConverter.nullToString(rs.getString("REFERENCE_NUMBER"))); // assessment request number
				data.setTestDescription(ValueConverter.nullToString(rs.getString("TEST_NAME"))); // test name
				data.setTestType(ValueConverter.nullToString(rs.getString("LAB_TEST_IND"))); // indicates lab or exam
				data.setDateRequested(ValueConverter.nullToString(rs.getString("REQUEST_DATE")));
				data.setDateSubmitted(ValueConverter.nullToString(rs.getString("FOLLOWUP_DATE")));
				data.setTotalCount(rs.getInt("TOTAL_COUNT"));               
				
				testData.add(data);
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}		
				
		return (testData);
//		TODO Auto-generated method stub		
		
	}

	private ArrayList getTestDataByLaboratory()
	{
		ArrayList testData = new ArrayList();
		StringBuffer sql = new StringBuffer();

		sql.append(" select ");
		sql.append("  LB.LAB_NAME as NAME ");
		sql.append(", SLO.SLO_OFFICE_NAME as BRANCH_NAME ");
		sql.append(", PM.UAR_REFERENCE_NUM as REFERENCE_NUMBER ");
		sql.append(", TP.TEST_DESC as TEST_NAME");
		sql.append(", decode(MR.MED_LAB_TEST_IND, '0', 'N', '1', 'Y') as LAB_TEST_IND");
		sql.append(", MR.MED_REQUEST_DATE as REQUEST_DATE");
		sql.append(", MR.MED_FOLLOW_UP_DATE as FOLLOWUP_DATE");
		sql.append(", count(PM.MED_RECORD_ID) as TOTAL_COUNT");
		sql.append(" from ");
		sql.append("  MEDICAL_RECORDS MR ");
		sql.append(", TEST_PROFILES TP ");
		sql.append(", LABORATORIES LB ");
		sql.append(", SUNLIFE_OFFICES SLO ");
		sql.append(", ASSESSMENT_REQUESTS AR ");
		sql.append(", POLICY_MEDICAL_RECORDS PM");
		sql.append(" where ");
		sql.append("     MR.MED_RECORD_ID = PM.MED_RECORD_ID  ");
		sql.append(" and AR.REFERENCE_NUM = PM.UAR_REFERENCE_NUM ");
		sql.append(" and MR.MED_LAB_TEST_IND = '1' ");
		sql.append(" and MR.MED_LAB_ID = LB.LAB_ID ");		
		sql.append(" and AR.BRANCH_ID = SLO.SLO_OFFICE_CODE ");
		sql.append(" and MR.MED_TEST_ID = TP.TEST_ID ");
		sql.append(" and MR.MED_STATUS IN  (18, 19) ");
		sql.append(" and MR.MED_REQUEST_DATE ");
		sql.append("     between to_date('01-JAN-2004 00:00:00', 'dd-MON-yyyy hh24:mi:ss')");
		sql.append("         and to_date('31-DEC-2004 23:59:59', 'dd-MON-yyyy hh24:mi:ss')");
		sql.append(" GROUP BY GROUPING SETS ( ");
		sql.append("  (LB.LAB_NAME), ");
		sql.append("  (LB.LAB_NAME, SLO.SLO_OFFICE_NAME, PM.UAR_REFERENCE_NUM, TP.TEST_DESC, MR.MED_LAB_TEST_IND, MR.MED_REQUEST_DATE, MR.MED_FOLLOW_UP_DATE))" );
		sql.append(" order by ");
		sql.append("  LB.LAB_NAME, SLO.SLO_OFFICE_NAME, PM.UAR_REFERENCE_NUM ");


		System.out.println (sql.toString());
		
		try {
			PreparedStatement ps = new DataSourceProxy().getConnection().prepareStatement(sql.toString());
			
			// execute the query
			ResultSet rs = ps.executeQuery();
						
			// fill up the data object
			while (rs.next()) {
				ARWithPendingMedData data = new ARWithPendingMedData();
				
				data.setNameExaminerLab(ValueConverter.nullToString(rs.getString("NAME"))); // examiner or laboratory
				data.setBranchName(ValueConverter.nullToString(rs.getString("BRANCH_NAME")));
				data.setReferenceNumber(ValueConverter.nullToString(rs.getString("REFERENCE_NUMBER"))); // assessment request number
				data.setTestDescription(ValueConverter.nullToString(rs.getString("TEST_NAME"))); // test name
				data.setTestType(ValueConverter.nullToString(rs.getString("LAB_TEST_IND"))); // indicates lab or exam
				data.setDateRequested(ValueConverter.nullToString(rs.getString("REQUEST_DATE")));
				data.setDateSubmitted(ValueConverter.nullToString(rs.getString("FOLLOWUP_DATE")));
				data.setTotalCount(rs.getInt("TOTAL_COUNT"));               
				
				testData.add(data);
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}		
				
		return (testData);
//		TODO Auto-generated method stub		
		
	}

	private ArrayList getTestDataByExaminer()
	{
		ArrayList testData = new ArrayList();
		StringBuffer sql = new StringBuffer();

		sql.append(" select ");
		sql.append("  (EX.EXMNR_LAST_NAME || ', ' || EX.EXMNR_GIVEN_NAME || ' ' || EX.EXMNR_MIDDLE_NAME) as NAME ");
		sql.append(", SLO.SLO_OFFICE_NAME as BRANCH_NAME ");
		sql.append(", PM.UAR_REFERENCE_NUM as REFERENCE_NUMBER ");
		sql.append(", TP.TEST_DESC as TEST_NAME");
		sql.append(", decode(MR.MED_LAB_TEST_IND, '0', 'N', '1', 'Y') as LAB_TEST_IND");
		sql.append(", MR.MED_REQUEST_DATE as REQUEST_DATE");
		sql.append(", MR.MED_FOLLOW_UP_DATE as FOLLOWUP_DATE");
		sql.append(", count(PM.MED_RECORD_ID) as TOTAL_COUNT");
		sql.append(" from ");
		sql.append("  MEDICAL_RECORDS MR ");
		sql.append(", TEST_PROFILES TP ");
		sql.append(", SUNLIFE_OFFICES SLO ");
		sql.append(", EXAMINERS EX ");
		sql.append(", ASSESSMENT_REQUESTS AR ");
		sql.append(", POLICY_MEDICAL_RECORDS PM");
		sql.append(" where ");
		sql.append("     MR.MED_RECORD_ID = PM.MED_RECORD_ID  ");
		sql.append(" and AR.REFERENCE_NUM = PM.UAR_REFERENCE_NUM ");
		sql.append(" and MR.MED_LAB_TEST_IND = '0' ");
		sql.append(" and MR.EXMNR_ID = EX.EXMNR_ID ");
		sql.append(" and AR.BRANCH_ID = SLO.SLO_OFFICE_CODE ");		
		sql.append(" and MR.MED_TEST_ID = TP.TEST_ID ");
		sql.append(" and MR.MED_STATUS IN  (18, 19) ");
		sql.append(" and MR.MED_REQUEST_DATE ");
		sql.append("     between to_date('01-JAN-2004 00:00:00', 'dd-MON-yyyy hh24:mi:ss')");
		sql.append("         and to_date('31-DEC-2004 23:59:59', 'dd-MON-yyyy hh24:mi:ss')");
		sql.append(" GROUP BY GROUPING SETS ( ");
		sql.append("  (EX.EXMNR_LAST_NAME, EX.EXMNR_GIVEN_NAME, EX.EXMNR_MIDDLE_NAME) ");
		sql.append(", (EX.EXMNR_LAST_NAME, EX.EXMNR_GIVEN_NAME, EX.EXMNR_MIDDLE_NAME, SLO.SLO_OFFICE_NAME, ");
		sql.append("   PM.UAR_REFERENCE_NUM,TP.TEST_DESC, MR.MED_LAB_TEST_IND, MR.MED_REQUEST_DATE, MR.MED_FOLLOW_UP_DATE))" );
		sql.append(" order by ");
		sql.append("  EX.EXMNR_LAST_NAME, EX.EXMNR_GIVEN_NAME, EX.EXMNR_MIDDLE_NAME, ");
		sql.append("  SLO.SLO_OFFICE_NAME, PM.UAR_REFERENCE_NUM ");

		System.out.println (sql.toString());
		
		try {
			PreparedStatement ps = new DataSourceProxy().getConnection().prepareStatement(sql.toString());
			
			// execute the query
			ResultSet rs = ps.executeQuery();
						
			// fill up the data object
			while (rs.next()) {
				ARWithPendingMedData data = new ARWithPendingMedData();
				
				data.setNameExaminerLab(ValueConverter.nullToString(rs.getString("NAME"))); // examiner or laboratory
				data.setBranchName(ValueConverter.nullToString(rs.getString("BRANCH_NAME")));
				data.setReferenceNumber(ValueConverter.nullToString(rs.getString("REFERENCE_NUMBER"))); // assessment request number
				data.setTestDescription(ValueConverter.nullToString(rs.getString("TEST_NAME"))); // test name
				data.setTestType(ValueConverter.nullToString(rs.getString("LAB_TEST_IND"))); // indicates lab or exam
				data.setDateRequested(ValueConverter.nullToString(rs.getString("REQUEST_DATE")));
				data.setDateSubmitted(ValueConverter.nullToString(rs.getString("FOLLOWUP_DATE")));
				data.setTotalCount(rs.getInt("TOTAL_COUNT"));               
				
				testData.add(data);
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}		
				
		return (testData);
//		TODO Auto-generated method stub		
		
	}

	private ARWithPendingMedFilterData getFilter(int reportType) {
		ARWithPendingMedFilterData criteria = new ARWithPendingMedFilterData();
		
		criteria.setReportType(reportType);
		criteria.setDateStart("01-JAN-2004");
		criteria.setDateEnd("31-DEC-2004");
				
		return (criteria);
		
	}


}
