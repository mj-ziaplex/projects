/*
 * Created on Feb 19, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.reports.test;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import com.slocpi.ium.data.ApplicationsByStatusData;
import com.slocpi.ium.data.ApplicationsByStatusFilterData;
import com.slocpi.ium.data.util.DataSourceProxy;
import com.slocpi.ium.reports.ApplicationsByStatusReport;
import com.slocpi.ium.util.ValueConverter;

/**
 * @author Nic Decapia
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class ApplicationsByStatusReportTest extends TestCase
{
	/**
	 * Constructor for ApplicationsByStatusReportTest.
	 * @param arg0
	 */
	public ApplicationsByStatusReportTest(String arg0)
	{
		super(arg0);
	}
	
	public static Test suite() {
		TestSuite suite = new TestSuite(ApplicationsByStatusReportTest.class);
		return (suite);
	}
	
	public static void main(String[] args)
	{
		junit.textui.TestRunner.run(ApplicationsByStatusReportTest.class);
	}
	
	public void testGenerateReportByBranch()
	{
		//TODO Implement generateReport().
		ArrayList report = new ArrayList();

		ApplicationsByStatusFilterData filterBranch = this.getFilter();
		filterBranch.setFilterType(ApplicationsByStatusFilterData.FILTER_BY_BRANCH);			

		try {
			ApplicationsByStatusReport absr = new ApplicationsByStatusReport();
			report = absr.generateReport(filterBranch);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		
		assertEquals("testGenerateReportByBranch: ", true, isEqual(report, this.getReportByBranch()));
	}

	public void testGenerateReportByLOB()
	{
		//TODO Implement generateReport().
		ArrayList report = new ArrayList();

		ApplicationsByStatusFilterData filterLOB = this.getFilter();
		filterLOB.setFilterType(ApplicationsByStatusFilterData.FILTER_BY_LOB);			

		try {
			ApplicationsByStatusReport absr = new ApplicationsByStatusReport();
			report = absr.generateReport(filterLOB);
		}
		catch (Exception e) {
			e.printStackTrace();
		}

		assertEquals("testGenerateReportByLOB: ", true, isEqual(report, this.getReportByLOB()));		
	}

	public void testGenerateReportByUnderwriter()
	{
		//TODO Implement generateReport().
		ArrayList report = new ArrayList();

		ApplicationsByStatusFilterData filterUnderwriter = this.getFilter();
		filterUnderwriter.setFilterType(ApplicationsByStatusFilterData.FILTER_BY_UNDERWRITER);			

		try {
			ApplicationsByStatusReport absr = new ApplicationsByStatusReport();
			report = absr.generateReport(filterUnderwriter);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		
		assertEquals("testGenerateReportByUnderwriter: ", true, isEqual(report, this.getReportByUnderwriter()));		
	}
	
	private ApplicationsByStatusFilterData getFilter() {
		ApplicationsByStatusFilterData filter = new ApplicationsByStatusFilterData();
		filter.setStartDate("01-JAN-2004");
		filter.setEndDate("31-JAN-2004");
		return (filter); 
	}
	
	private boolean isEqual(ArrayList fromComponent, ArrayList fromTestCase) {
		boolean equal = true;
		
		// compare the contents
		for (int i = 0; i < fromComponent.size(); i++) {
			ApplicationsByStatusData d1 = (ApplicationsByStatusData) fromComponent.get(i);
			ApplicationsByStatusData d2 = (ApplicationsByStatusData) fromTestCase.get(i);
			
			if (!d1.getGroupName().equals(d2.getGroupName())) equal = false;	
			if (!d1.getStatusDesc().equals(d2.getStatusDesc())) equal = false;
			if (!d1.getReferenceNumber().equals(d2.getReferenceNumber())) equal = false;
			if (!d1.getStatusDate().equals(d2.getStatusDate())) equal = false;
			if (d1.getCountAR() != (d2.getCountAR())) equal = false;
			
			System.out.println(i + ": " + equal);
			
			if (!equal) break; // don't test further if there is inequality
		}
		
		// compare the size
		if (fromComponent.size() != fromTestCase.size()) equal = false;
		
		return (equal);
	}
	
	private ArrayList convertRStoAL(ResultSet rs) {
		ArrayList convertedRS = new ArrayList();
		
		try {
			while (rs.next()) {
				ApplicationsByStatusData absd = new ApplicationsByStatusData();
				absd.setGroupName(ValueConverter.nullToString(rs.getString(1)));
				absd.setStatusDesc(ValueConverter.nullToString(rs.getString("STAT_DESC")));
				absd.setReferenceNumber(ValueConverter.nullToString(rs.getString("REFERENCE_NUM")));
				absd.setStatusDate(rs.getString("STATUS_DATE"));
				absd.setCountAR(rs.getInt("COUNT")); 	
				
				convertedRS.add(absd);		
			}
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
		
		return (convertedRS);
		
	}
	


	private ArrayList getReportByBranch() {
		ArrayList list = new ArrayList();
		StringBuffer query = new StringBuffer();
		query.append(" select ");
		query.append("  SLO_OFFICE_NAME ");
		query.append(", STAT_DESC");
		query.append(", REFERENCE_NUM");
		query.append(", STATUS_DATE");
		query.append(", count(REFERENCE_NUM) as COUNT ");
		query.append(" from ");
		query.append("  ASSESSMENT_REQUESTS ");
		query.append(", STATUS ");
		query.append(", SUNLIFE_OFFICES ");
		query.append(" where ");
		query.append("      BRANCH_ID = SLO_OFFICE_CODE ");
		query.append("  and STATUS_ID = STAT_ID ");
		query.append("  and STAT_TYPE = 'AR' ");
		query.append("  and STATUS_DATE ");
		query.append("     between to_date('01-JAN-2004 00:00:00', 'dd-MON-yyyy hh24:mi:ss') ");
		query.append("         and to_date('31-JAN-2004 23:59:59', 'dd-MON-yyyy hh24:mi:ss') ");
		query.append(" group by grouping sets ( ");
		query.append("   (SLO_OFFICE_NAME) ");
		query.append(" , (SLO_OFFICE_NAME, STAT_DESC)");
		query.append(" , (SLO_OFFICE_NAME, STAT_DESC, REFERENCE_NUM, STATUS_DATE)");
		query.append(")");
		query.append(" order by 1, 2, 3, 4");
		
		try {
			PreparedStatement stmt = new DataSourceProxy().getConnection().prepareStatement(query.toString());
			ResultSet rs = stmt.executeQuery();
			list = convertRStoAL(rs);
		}
		catch (SQLException e){
			e.printStackTrace();
		}

		return (list);
	}

	private ArrayList getReportByLOB() {
		ArrayList list = new ArrayList();
		StringBuffer query = new StringBuffer();

		query.append(" select ");
		query.append("  LOB_DESC ");
		query.append(", STAT_DESC");
		query.append(", REFERENCE_NUM");
		query.append(", STATUS_DATE");
		query.append(", count(REFERENCE_NUM) as COUNT ");
		query.append(" from ");
		query.append("  ASSESSMENT_REQUESTS ");
		query.append(", STATUS ");
		query.append(", LINES_OF_BUSINESS ");
		query.append(" where ");
		query.append("      LOB = LOB_CODE ");
		query.append("  and STATUS_ID = STAT_ID ");
		query.append("  and STAT_TYPE = 'AR' ");
		query.append("  and STATUS_DATE ");
		query.append("     between to_date('01-JAN-2004 00:00:00', 'dd-MON-yyyy hh24:mi:ss') ");
		query.append("         and to_date('31-JAN-2004 23:59:59', 'dd-MON-yyyy hh24:mi:ss') ");
		query.append(" group by grouping sets ( ");
		query.append("   (LOB_DESC) ");
		query.append(" , (LOB_DESC, STAT_DESC)");
		query.append(" , (LOB_DESC, STAT_DESC, REFERENCE_NUM, STATUS_DATE)");
		query.append(")");
		query.append(" order by 1, 2, 3, 4");		

		try {
			PreparedStatement stmt = new DataSourceProxy().getConnection().prepareStatement(query.toString());
			ResultSet rs = stmt.executeQuery();
			list = convertRStoAL(rs);
		}
		catch (SQLException e){
			e.printStackTrace();
		}

		return (list);

	}
	
	private ArrayList getReportByUnderwriter() {
		ArrayList list = new ArrayList();
		StringBuffer query = new StringBuffer();
		query.append(" select ");
		query.append("  (USR_LAST_NAME || ', ' || USR_FIRST_NAME || ' ' || USR_MIDDLE_NAME) AS UW_NAME");
		query.append(", STAT_DESC");
		query.append(", REFERENCE_NUM");
		query.append(", STATUS_DATE");
		query.append(", count(REFERENCE_NUM) as COUNT ");
		query.append(" from ");
		query.append("  ASSESSMENT_REQUESTS ");
		query.append(", STATUS ");
		query.append(", USERS ");
		query.append(" where ");
		query.append("      UNDERWRITER = USER_ID ");
		query.append("  and STATUS_ID = STAT_ID ");
		query.append("  and STAT_TYPE = 'AR' ");
		query.append("  and STATUS_DATE ");
		query.append("     between to_date('01-JAN-2004 00:00:00', 'dd-MON-yyyy hh24:mi:ss') ");
		query.append("         and to_date('31-JAN-2004 23:59:59', 'dd-MON-yyyy hh24:mi:ss') ");
		query.append(" group by grouping sets ( ");
		query.append("   (USR_LAST_NAME, USR_FIRST_NAME, USR_MIDDLE_NAME) ");
		query.append(" , (USR_LAST_NAME, USR_FIRST_NAME, USR_MIDDLE_NAME, STAT_DESC)");
		query.append(" , (USR_LAST_NAME, USR_FIRST_NAME, USR_MIDDLE_NAME, STAT_DESC, REFERENCE_NUM, STATUS_DATE)");
		query.append(")");		
		query.append(" order by 1, 2, 3, 4");
		
		try {
			PreparedStatement stmt = new DataSourceProxy().getConnection().prepareStatement(query.toString());
			ResultSet rs = stmt.executeQuery();
			list = convertRStoAL(rs);
		}
		catch (SQLException e){
			e.printStackTrace();
		}

		return (list);
	}		
		
}
