/*
 * Created on Mar 4, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.reports.test;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import com.slocpi.ium.data.RequirementData;
import com.slocpi.ium.data.RequirementsOrderedData;
import com.slocpi.ium.data.RequirementsOrderedFilterData;
import com.slocpi.ium.data.SunLifeOfficeData;
import com.slocpi.ium.data.UserProfileData;
import com.slocpi.ium.data.util.DataSourceProxy;
import com.slocpi.ium.reports.RequirementsOrderedReport;

/**
 * @author Nic Decapia
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class RequirementsOrderedReportTest extends TestCase
{
	/**
	 * Constructor for RequirementsOrderedReportTest.
	 * @param arg0
	 */
	public RequirementsOrderedReportTest(String arg0)
	{
		super(arg0);
	}
	
	public static Test suite() {
		TestSuite suite = new TestSuite(RequirementsOrderedReportTest.class);
		return (suite);
	}
	
	public static void main(String[] args)
	{
		junit.textui.TestRunner.run(RequirementsOrderedReportTest.class);
	}
	
	public void testGenerateByBranch()
	{
		
		RequirementsOrderedFilterData filter = this.getFilter();
		filter.setReportType(RequirementsOrderedFilterData.TYPE_BRANCH);
		
		ArrayList list = new ArrayList();
		RequirementsOrderedReport rpt = new RequirementsOrderedReport();
		try {
			list = rpt.generate(filter);
		} catch (Exception e) {
			e.printStackTrace();
		}
		assertEquals("testGenerateByBranch: ", true, this.isEqual(list, this.getReportByBranch(), RequirementsOrderedFilterData.TYPE_BRANCH));
	}

	public void testGenerateByRequirement()
	{
		RequirementsOrderedFilterData filter = this.getFilter();
		filter.setReportType(RequirementsOrderedFilterData.TYPE_REQUIREMENT);

		ArrayList list = new ArrayList();
		RequirementsOrderedReport rpt = new RequirementsOrderedReport();
		try {
			list = rpt.generate(filter);
		} catch (Exception e) {
			e.printStackTrace();
		}
		assertEquals("testGenerateByRequirement: ", true, this.isEqual(list, this.getReportByRequirement(), RequirementsOrderedFilterData.TYPE_REQUIREMENT));
	}

	public void testGenerateByUnderwriter()
	{
		RequirementsOrderedFilterData filter = this.getFilter();
		filter.setReportType(RequirementsOrderedFilterData.TYPE_USER);

		ArrayList list = new ArrayList();
		RequirementsOrderedReport rpt = new RequirementsOrderedReport();
		try {
			list = rpt.generate(filter);
		} catch (Exception e) {
			e.printStackTrace();
		}
		assertEquals("testGenerateByUnderwriter: ", true, this.isEqual(list, this.getReportByUnderwriter(), RequirementsOrderedFilterData.TYPE_USER));
	}

    private RequirementsOrderedFilterData getFilter() {
		RequirementsOrderedFilterData filter = new RequirementsOrderedFilterData();
		filter.setStartDate("01-FEB-2004");
		filter.setEndDate("28-FEB-2004");
		return (filter);
	}

	private boolean isEqual(ArrayList fromComponent, ArrayList fromTestCase, int reportType) {
		boolean equal = true;
		
		// compare the contents
		for (int i = 0; i < fromComponent.size(); i++) {
			RequirementsOrderedData d1 = (RequirementsOrderedData) fromComponent.get(i);
			RequirementsOrderedData d2 = (RequirementsOrderedData) fromTestCase.get(i);
			
			if (reportType == RequirementsOrderedFilterData.TYPE_USER) {
				if (!d1.getUser().getLastName().equals(d2.getUser().getLastName())) equal = false;
				if (!d1.getUser().getFirstName().equals(d2.getUser().getFirstName())) equal = false;
				if (!d1.getUser().getMiddleName().equals(d2.getUser().getMiddleName())) equal = false;
			}
			else {
				if (!d1.getDepartment().getOfficeName().equals(d2.getDepartment().getOfficeName())) equal = false;
			}

			if (!d1.getRequirement().getReqtCode().equals(d2.getRequirement().getReqtCode())) equal = false;
			if (!d1.getRequirement().getReqtDesc().equals(d2.getRequirement().getReqtDesc())) equal = false;
			if (d1.getRequirementsOrdered() != d2.getRequirementsOrdered()) equal = false;
			
			System.out.println(i + ": " + equal);
			
			if (!equal) break; // don't test further if there is inequality
		}
		
		// compare the size
		if (fromComponent.size() != fromTestCase.size()) equal = false;
		
		return (equal);
	}
	
	private ArrayList convertRStoAL(ResultSet rs, int reportType) {
		ArrayList convertedRS = new ArrayList();
		
		try {
			while (rs.next()) {
				RequirementsOrderedData reqtOrderData = new RequirementsOrderedData();

				if (reportType == RequirementsOrderedFilterData.TYPE_USER) {
					
					// contains different fields
					UserProfileData userData = new UserProfileData();
					userData.setFirstName(rs.getString("FIRST_NAME"));
					userData.setMiddleName(rs.getString("MIDDLE_NAME"));
					userData.setLastName(rs.getString("LAST_NAME"));
					reqtOrderData.setUser(userData);
					
				}
				else {
					SunLifeOfficeData sloData = new SunLifeOfficeData();
					sloData.setOfficeName(rs.getString("BRANCH_NAME"));
					reqtOrderData.setDepartment(sloData);

				}
								
				RequirementData reqtData = new RequirementData();
				reqtData.setReqtCode(rs.getString("REQUIREMENT_CODE"));
				reqtData.setReqtDesc(rs.getString("REQUIREMENT_NAME"));
				reqtOrderData.setRequirement(reqtData);
				
				reqtOrderData.setRequirementsOrdered(rs.getLong("REQUIREMENT_COUNT"));				
				
				convertedRS.add(reqtOrderData);		
			}
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
		
		return (convertedRS);
		
	}

	private ArrayList getReportByUnderwriter() {
		ArrayList list = new ArrayList();
		
		StringBuffer sql = new StringBuffer();
		sql.append(" select ");
		sql.append("  UR.USR_LAST_NAME as LAST_NAME ");
		sql.append(", UR.USR_FIRST_NAME as FIRST_NAME ");
		sql.append(", UR.USR_MIDDLE_NAME as MIDDLE_NAME ");
		sql.append(", PR.PR_REQT_CODE as REQUIREMENT_CODE  "); 
		sql.append(", RQ.REQT_DESC as REQUIREMENT_NAME  ");
		sql.append(", count (PR.PR_REQUIREMENT_ID) as REQUIREMENT_COUNT ");
		sql.append(" from ");
		sql.append("  POLICY_REQUIREMENTS PR ");
		sql.append(", REQUIREMENTS        RQ ");
		sql.append(", SUNLIFE_OFFICES     SO ");
		sql.append(", ASSESSMENT_REQUESTS AR ");
		sql.append(", USERS               UR ");
		sql.append(" where ");
		sql.append("     AR.REFERENCE_NUM = PR.UAR_REFERENCE_NUM ");
		sql.append(" and RQ.REQT_CODE = PR.PR_REQT_CODE ");
		sql.append(" and SO.SLO_OFFICE_CODE = AR.BRANCH_ID ");
		sql.append(" and AR.UNDERWRITER = UR.USER_ID ");
		sql.append(" and PR.PR_ORDER_DATE between to_date ('01-FEB-2004 00:00:00', 'dd-MON-yyyy hh24:mi:ss')");
		sql.append("                          and to_date ('28-FEB-2004 23:59:59', 'dd-MON-yyyy hh24:mi:ss')");
		sql.append(" group by ");
		sql.append("  UR.USR_LAST_NAME ");
		sql.append(", UR.USR_FIRST_NAME ");
		sql.append(", UR.USR_MIDDLE_NAME "); 
		sql.append(", PR.PR_REQT_CODE " );
		sql.append(", RQ.REQT_DESC ");
		sql.append(" order by ");
		sql.append("  UR.USR_LAST_NAME ");
		sql.append(", UR.USR_FIRST_NAME ");
		sql.append(", UR.USR_MIDDLE_NAME "); 
		sql.append(", PR.PR_REQT_CODE ");
		sql.append(", RQ.REQT_DESC ");

		System.out.println(sql.toString());		
		
		try {
			PreparedStatement stmt = new DataSourceProxy().getConnection().prepareStatement(sql.toString());
			ResultSet rs = stmt.executeQuery();
			list = convertRStoAL(rs, RequirementsOrderedFilterData.TYPE_USER);
		}
		catch (SQLException e){
			e.printStackTrace();
		}
				
		return (list);
	}


	private ArrayList getReportByBranch() {
		ArrayList list = new ArrayList();
		
		StringBuffer sql = new StringBuffer();
		sql.append(" select ");
		sql.append("  SO.SLO_OFFICE_NAME as BRANCH_NAME ");
		sql.append(", PR.PR_REQT_CODE as REQUIREMENT_CODE  ");
		sql.append(", RQ.REQT_DESC as REQUIREMENT_NAME  ");
		sql.append(", count (PR.PR_REQUIREMENT_ID) as REQUIREMENT_COUNT ");
		sql.append(" from ");
		sql.append("  POLICY_REQUIREMENTS PR ");
		sql.append(", REQUIREMENTS        RQ ");
		sql.append(", SUNLIFE_OFFICES     SO ");
		sql.append(", ASSESSMENT_REQUESTS AR ");
		sql.append(", USERS               UR ");
		sql.append(" where ");
		sql.append("     AR.REFERENCE_NUM = PR.UAR_REFERENCE_NUM ");
		sql.append(" and RQ.REQT_CODE = PR.PR_REQT_CODE ");
		sql.append(" and SO.SLO_OFFICE_CODE = AR.BRANCH_ID ");
		sql.append(" and AR.UNDERWRITER = UR.USER_ID ");
		sql.append(" and PR.PR_ORDER_DATE between to_date ('01-FEB-2004 00:00:00', 'dd-MON-yyyy hh24:mi:ss')");
		sql.append("                          and to_date ('28-FEB-2004 23:59:59', 'dd-MON-yyyy hh24:mi:ss')");
		sql.append(" group by ");
		sql.append("  SO.SLO_OFFICE_NAME");
		sql.append(", PR.PR_REQT_CODE");
		sql.append(", RQ.REQT_DESC");
		sql.append(" order by ");
		sql.append("  SO.SLO_OFFICE_NAME");
		sql.append(", PR.PR_REQT_CODE ");
		sql.append(", RQ.REQT_DESC");


		System.out.println(sql.toString());		

		try {
			PreparedStatement stmt = new DataSourceProxy().getConnection().prepareStatement(sql.toString());
			ResultSet rs = stmt.executeQuery();
			list = convertRStoAL(rs, RequirementsOrderedFilterData.TYPE_BRANCH);
		}
		catch (SQLException e){
			e.printStackTrace();
		}
				
		return (list);
	}

	private ArrayList getReportByRequirement() {
		ArrayList list = new ArrayList();
		
		StringBuffer sql = new StringBuffer();
		sql.append(" select ");
		sql.append("  PR.PR_REQT_CODE as REQUIREMENT_CODE ");
		sql.append(", RQ.REQT_DESC as REQUIREMENT_NAME  ");
		sql.append(", SO.SLO_OFFICE_NAME as BRANCH_NAME ");		
		sql.append(", count (PR.PR_REQUIREMENT_ID) as REQUIREMENT_COUNT ");
		sql.append(" from ");
		sql.append("  POLICY_REQUIREMENTS PR ");
		sql.append(", REQUIREMENTS        RQ ");
		sql.append(", SUNLIFE_OFFICES     SO ");
		sql.append(", ASSESSMENT_REQUESTS AR ");
		sql.append(", USERS               UR ");
		sql.append(" where ");
		sql.append("     AR.REFERENCE_NUM = PR.UAR_REFERENCE_NUM ");
		sql.append(" and RQ.REQT_CODE = PR.PR_REQT_CODE ");
		sql.append(" and SO.SLO_OFFICE_CODE = AR.BRANCH_ID ");
		sql.append(" and AR.UNDERWRITER = UR.USER_ID ");
		sql.append(" and PR.PR_ORDER_DATE between to_date ('01-FEB-2004 00:00:00', 'dd-MON-yyyy hh24:mi:ss')");
		sql.append("                          and to_date ('28-FEB-2004 23:59:59', 'dd-MON-yyyy hh24:mi:ss')");
		sql.append(" group by ");
		sql.append("  PR.PR_REQT_CODE ");
		sql.append(", RQ.REQT_DESC ");
		sql.append(", SO.SLO_OFFICE_NAME ");
		sql.append(" order by ");
		sql.append("  PR.PR_REQT_CODE ");
		sql.append(", RQ.REQT_DESC ");
		sql.append(", SO.SLO_OFFICE_NAME ");
		
		System.out.println(sql.toString());
		
		try {
			PreparedStatement stmt = new DataSourceProxy().getConnection().prepareStatement(sql.toString());
			ResultSet rs = stmt.executeQuery();
			list = convertRStoAL(rs, RequirementsOrderedFilterData.TYPE_REQUIREMENT);
		}
		catch (SQLException e){
			e.printStackTrace();
		}
				
		return (list);
	}
	
}