/*
 * Created on Feb 17, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.reports.test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import com.slocpi.ium.data.ApplicationsApprovedFilterData;
import com.slocpi.ium.data.AssessmentRequestData;
import com.slocpi.ium.data.LOBData;
import com.slocpi.ium.data.SunLifeDeptData;
import com.slocpi.ium.data.util.DataSourceProxy;
import com.slocpi.ium.reports.ApplicationsApprovedReport;
/**
 * @author Nic Decapia
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class ApplicationsApprovedReportTest extends TestCase
{
	/**
	 * Constructor for ApplicationsApprovedReportTest.
	 * @param arg0
	 */
	public ApplicationsApprovedReportTest(String arg0)
	{
		super(arg0);
	}

	public static Test suite() {        
		TestSuite suite = new TestSuite(ApplicationsApprovedReportTest.class);                
		return suite;    
	}	
		
	public static void main(String[] args)
	{
		junit.textui.TestRunner.run(suite());
	}
	
	public void testGenerate()
	{
		ApplicationsApprovedFilterData aarf = new ApplicationsApprovedFilterData();
		ApplicationsApprovedReport aar = new ApplicationsApprovedReport();
		ArrayList result = new ArrayList();
		
		SunLifeDeptData dept = new SunLifeDeptData();
		dept.setDeptId("QAINFOSEC");
		
		LOBData lob = new LOBData();
		lob.setLOBCode("IL");
		
		aarf.setStartDate("01-FEB-2004");
		aarf.setEndDate("29-FEB-2004");
		aarf.setApprovingParty(dept);
		aarf.setRequestingParty(lob);
		
		try {
			result = aar.generate(aarf);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		
		assertEquals("testGenerate: ", true, isEqual(result, getApplicationsApprovedReportData()));
	}
	private boolean isEqual(ArrayList fromComponent, ArrayList fromTestCase) 
	{
		boolean equal = true;
		
		for (int i = 0; i < fromComponent.size(); i++) {
			AssessmentRequestData d1 = (AssessmentRequestData) fromComponent.get(i);	
			AssessmentRequestData d2 = (AssessmentRequestData) fromTestCase.get(i);
			if (!d1.getReferenceNumber().equals(d2.getReferenceNumber())) {
				equal = false;
			} 
		}

		// check sizes		
		if (fromComponent.size() != fromTestCase.size()) equal = false;
		return (equal);
	}
	
	private ArrayList getApplicationsApprovedReportData() {

		StringBuffer sql = new StringBuffer();
		sql.append(" select "); 
		sql.append(" REFERENCE_NUM ");
		sql.append(" from ");
		sql.append("  ASSESSMENT_REQUESTS AR ");
		sql.append(", USERS US ");
		sql.append(" where ");
		sql.append("     STATUS_ID in (7, 9) "); // choose only ARs that are 'For Offer' or 'Approve as Applied For'
		sql.append(" and STATUS_DATE ");
		sql.append("    between to_date('01-FEB-2004 00:00:00' ,'dd-mon-yyyy hh24:mi:ss')"); 
		sql.append("    and to_date('29-FEB-2004 23:59:59', 'dd-mon-yyyy hh24:mi:ss') ");
		sql.append(" and AR.UPDATED_BY = US.USER_ID ");
		sql.append(" and US.DEPT_ID = 'QAINFOSEC'"); // assuming that the filter passed contains a valid department code
		sql.append(" and AR.LOB = 'IL'");     // assuming that the filter passed contains a valid LOB code
		sql.append(" order by ");
		sql.append("  AR.REFERENCE_NUM ");	


		ArrayList list = new ArrayList();
		try {
			Connection conn = new DataSourceProxy().getConnection();
			PreparedStatement ps = conn.prepareStatement(sql.toString());
			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				AssessmentRequestData ard = new AssessmentRequestData();
				ard.setReferenceNumber(rs.getString("REFERENCE_NUM"));
				list.add(ard);
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return (list);
	}
	
}
