/*
 * Created on Feb 22, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.reports.test;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import com.slocpi.ium.data.ARWithPendingReqtData;
import com.slocpi.ium.data.ARWithPendingReqtFilterData;
import com.slocpi.ium.data.util.DataSourceProxy;
import com.slocpi.ium.reports.ApplicationsWithPendReqtReport;
import com.slocpi.ium.util.ValueConverter;

/**
 * @author Nic Decapia
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class ApplicationsWithPendReqtReportTest extends TestCase
{
	/**
	 * Constructor for ApplicationsWithPendReqtReportTest.
	 * @param arg0
	 */
	public ApplicationsWithPendReqtReportTest(String arg0)
	{
		super(arg0);
	}

	public static Test suite() {
		TestSuite suite = new TestSuite(ApplicationsWithPendReqtReportTest.class);
		return (suite);
	}
		
	public static void main(String[] args)
	{
		junit.textui.TestRunner.run(ApplicationsWithPendReqtReportTest.class);
	}
	
	public void testGenerateByBranch()
	{
		//TODO Implement generate().
		ArrayList report = new ArrayList();
		ARWithPendingReqtFilterData filter = this.getFilter(ARWithPendingReqtFilterData.BRANCH_REPORT_TYPE);
			
		try {
			ApplicationsWithPendReqtReport rpt = new ApplicationsWithPendReqtReport();
			report = rpt.generate(filter);
		}
		catch (Exception e){
			e.printStackTrace();
		}
		
		assertEquals("testGenerateByBranch(): ", true, isEqual(report, this.getTestData(ARWithPendingReqtFilterData.BRANCH_REPORT_TYPE)));
	}

	public void testGenerateByUnderwriter()
	{
		//TODO Implement generate().
		ArrayList report = new ArrayList();
		ARWithPendingReqtFilterData filter = this.getFilter(ARWithPendingReqtFilterData.UNDWTR_REPORT_TYPE);
		
		try {
			ApplicationsWithPendReqtReport rpt = new ApplicationsWithPendReqtReport();
			report = rpt.generate(filter);			
		}
		catch (Exception e){
			e.printStackTrace();
		}
		
		assertEquals("testGenerateByUnderwriter(): ", true, isEqual(report, this.getTestData(ARWithPendingReqtFilterData.UNDWTR_REPORT_TYPE)));
	}


	/**
	 * @param report
	 * @param object
	 */
	private boolean isEqual(ArrayList fromComponent, ArrayList fromTestCase)
	{
		boolean equal = true;
		
		// check the record counts
		if (fromComponent.size() != fromTestCase.size()) {
			return false;
		}

		// check the contents
		for (int i = 0; i < fromComponent.size(); i++) {
			ARWithPendingReqtData dataComponent = (ARWithPendingReqtData) fromComponent.get(i);
			ARWithPendingReqtData dataTestCase = (ARWithPendingReqtData) fromTestCase.get(i);

			if (!dataComponent.getBranchName().equals(dataTestCase.getBranchName())) equal = false;
			if (!dataComponent.getUnderwriterLastName().equals(dataTestCase.getUnderwriterLastName())) equal = false;
			if (!dataComponent.getUnderwriterGivenName().equals(dataTestCase.getUnderwriterGivenName())) equal = false;
			if (!dataComponent.getUnderwriterMiddleName().equals(dataTestCase.getUnderwriterMiddleName())) equal = false;
			if (!dataComponent.getReferenceNumber().equals(dataTestCase.getReferenceNumber())) equal = false;
			if (!dataComponent.getRequirementName().equals(dataTestCase.getRequirementName())) equal = false;
			if (!dataComponent.getDateOrdered().equals(dataTestCase.getDateOrdered())) equal = false;
			if (!dataComponent.getDateSubmit().equals(dataTestCase.getDateSubmit())) equal = false;
			if (dataComponent.getTotalCount() != dataTestCase.getTotalCount()) equal = false; 
			
			if (equal == false) break;	// stop testing if there is inequality
				
		}
		
		return equal;
		// TODO Auto-generated method stub
		
	}

	/**
	 * 
	 */
	private ArrayList getTestData(int rptType)
	{
		ArrayList testData = new ArrayList();
		StringBuffer sql = new StringBuffer();
		boolean isBranchType = rptType == ARWithPendingReqtFilterData.BRANCH_REPORT_TYPE;
		
		sql.append("select ");
		sql.append("  NVL(SLO.SLO_OFFICE_NAME, '') as SLO_OFFICE_NAME");
		sql.append(", NVL(US.USR_LAST_NAME, '') as USR_LAST_NAME ");
		sql.append(", NVL(US.USR_FIRST_NAME, '') as USR_FIRST_NAME "); 
		sql.append(", NVL(US.USR_MIDDLE_NAME, '') as  USR_MIDDLE_NAME");
		sql.append(", NVL(AR.REFERENCE_NUM, '') as REFERENCE_NUM ");
		sql.append(", NVL(PR.PR_REQT_CODE, '') as REQT_CODE");
		sql.append(", NVL(RQ.REQT_DESC, '') as REQT_DESC ");
		sql.append(", PR.PR_ORDER_DATE ");
		sql.append(", PR.PR_FOLLOW_UP_DATE ");
		sql.append(", count(PR.PR_REQUIREMENT_ID) as COUNT ");
		sql.append(" from ");
		sql.append("  ASSESSMENT_REQUESTS AR ");
		sql.append(",  REQUIREMENTS RQ ");
		sql.append(", SUNLIFE_OFFICES SLO ");
		sql.append(", USERS US ");
		sql.append(", POLICY_REQUIREMENTS PR ");
		sql.append(" where ");
		sql.append("     AR.REFERENCE_NUM = PR.UAR_REFERENCE_NUM ");
		sql.append(" and AR.BRANCH_ID = SLO.SLO_OFFICE_CODE ");
		sql.append(" and AR.UNDERWRITER = US.USER_ID ");
		sql.append(" and PR.PR_REQT_CODE = RQ.REQT_CODE ");
		sql.append(" and PR.PR_STATUS_ID = 13");
		sql.append(" and PR.PR_ORDER_DATE ");
		sql.append("        between to_date('01-FEB-2004', 'dd-MON-yyyy hh24:mi:ss') ");
		sql.append("            and to_date('29-FEB-2004', 'dd-MON-yyyy hh24:mi:ss') ");
		
//		sql.append(" and SLO.SLO_OFFICE_CODE = '" + branchId + "' ");
//		sql.append(" and US.USER_ID = '" + underwriterId + "' ");
		
		sql.append(" group by grouping sets ( ");
		
		if (isBranchType) { // depends on report type
			sql.append(" (SLO.SLO_OFFICE_NAME) ");
			sql.append(",(SLO.SLO_OFFICE_NAME, US.USR_LAST_NAME, US.USR_FIRST_NAME, US.USR_MIDDLE_NAME, AR.REFERENCE_NUM, PR.PR_REQT_CODE, RQ.REQT_DESC, PR.PR_ORDER_DATE, PR.PR_FOLLOW_UP_DATE)");
			sql.append(" )");
			sql.append(" order by 1, 2, 3, 4, 5, 6, 7 ");
			 
		}
		else {
			sql.append(" (US.USR_LAST_NAME, US.USR_FIRST_NAME, US.USR_MIDDLE_NAME) ");
			sql.append(",(US.USR_LAST_NAME, US.USR_FIRST_NAME, US.USR_MIDDLE_NAME, SLO.SLO_OFFICE_NAME, AR.REFERENCE_NUM, PR.PR_REQT_CODE, RQ.REQT_DESC, PR.PR_ORDER_DATE, PR.PR_FOLLOW_UP_DATE)");
			sql.append(" )");
			sql.append(" order by 2, 3, 4, 1, 5, 6, 7 ");			
		}		

		
		try {
			PreparedStatement ps = new DataSourceProxy().getConnection().prepareStatement(sql.toString());
			
			// execute the query
			ResultSet rs = ps.executeQuery();
			System.out.println (sql.toString());
			
			// fill up the data object
			while (rs.next()) {
				ARWithPendingReqtData reportData = new ARWithPendingReqtData();
				
				reportData.setBranchName(ValueConverter.nullToString(rs.getString("SLO_OFFICE_NAME")));
				reportData.setUnderwriterLastName(ValueConverter.nullToString(rs.getString("USR_LAST_NAME")));
				reportData.setUnderwriterGivenName(ValueConverter.nullToString(rs.getString("USR_FIRST_NAME")));
				reportData.setUnderwriterMidName(ValueConverter.nullToString(rs.getString("USR_MIDDLE_NAME")));
				reportData.setReferenceNumber(ValueConverter.nullToString(rs.getString("REFERENCE_NUM")));
				reportData.setRequirementCode(ValueConverter.nullToString(rs.getString("REQT_CODE")));
				reportData.setRequirementName(ValueConverter.nullToString(rs.getString("REQT_DESC")));
				reportData.setDateOrdered(rs.getString("PR_ORDER_DATE"));
				reportData.setDateSubmit(rs.getString("PR_FOLLOW_UP_DATE"));
				reportData.setTotalCount(rs.getInt("COUNT"));
				
				testData.add(reportData);
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}		
				
		return (testData);
//		TODO Auto-generated method stub		
		
	}

	private ARWithPendingReqtFilterData getFilter(int reportType) {
		ARWithPendingReqtFilterData criteria = new ARWithPendingReqtFilterData();
		
		criteria.setReportType(reportType);
		criteria.setStartDate("01-FEB-2004");
		criteria.setEndDate("29-FEB-2004");
				
		return (criteria);
		
	}
}
