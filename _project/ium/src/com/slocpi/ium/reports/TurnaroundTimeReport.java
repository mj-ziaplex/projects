/*
 * Created on Mar 9, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.reports;

import java.io.ByteArrayOutputStream;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.slocpi.ium.IUMException;
import com.slocpi.ium.data.TurnAroundTimeData;
import com.slocpi.ium.data.TurnAroundTimeLOBData;
import com.slocpi.ium.data.TurnAroundTimeParam;
import com.slocpi.ium.data.TurnAroundTimeReportData;
import com.slocpi.ium.data.dao.ActivityDAO;
import com.slocpi.ium.data.dao.StatusCHDao;
import com.slocpi.ium.data.util.DataSourceProxy;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.ValueConverter;
import com.slocpi.ium.util.forms.PrintableForm;


/**
 * @author nic.decapia
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class TurnaroundTimeReport {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(SummaryMedicalBillReport.class);
	
	public ArrayList generate(TurnAroundTimeParam filter) throws IUMException
	{
		LOGGER.info("generate start");
		ArrayList report = new ArrayList();
		DataSourceProxy dsp = new DataSourceProxy(); 
		Connection con = dsp.getConnection();
		try {
			ActivityDAO actLog = new ActivityDAO(con);
			report = actLog.retrieveTurnAroundTime(filter);
			if (report.size() > 0) report = applyLayout(report, filter.getUnit());
		}                
		catch (SQLException e) {
			e.printStackTrace();
			throw new IUMException (e);
		}
		finally {
			dsp.closeConnection(con);
		}
		LOGGER.info("generate end");
		return (report);
	}

	/**
	 * @param report
	 * @return
	 */
	private ArrayList applyLayout(ArrayList report, String reportType)
	{
		
		LOGGER.info("applyLayout start");
		ArrayList newContent = new ArrayList();
		BigDecimal totalElapsedTime = new BigDecimal(0);
		BigDecimal totalRequests = new BigDecimal(0);
		BigDecimal totalAverage = new BigDecimal(0);
		
	
		for (int i=0; i<report.size(); i++) {
			
			TurnAroundTimeReportData rpt = new TurnAroundTimeReportData();
			TurnAroundTimeLOBData header = (TurnAroundTimeLOBData) report.get(i);
		
			rpt.setLineOfBusiness(header.getLob());
			rpt.setReferenceNumber("");
			rpt.setDateStart("");
			rpt.setDateEnd("");
			
			BigDecimal elapsedTimeLob = ValueConverter.nullToZero(header.getElapsedTime());
			BigDecimal averageTAT = ValueConverter.nullToZero(header.getAveTAT());
			BigDecimal requestsProcessed = ValueConverter.nullToZero(header.getRequestsProcessed());
			if (Integer.parseInt(reportType) == TurnAroundTimeParam.TAT_IN_HOURS) {
				elapsedTimeLob = ValueConverter.convertToHours(elapsedTimeLob);
				averageTAT = ValueConverter.convertToHours(averageTAT);				
			}
			
			rpt.setElapsedTime(String.valueOf(elapsedTimeLob));
			rpt.setProcessedCount(String.valueOf(requestsProcessed));
			rpt.setAverageTAT(String.valueOf(averageTAT));
			
			newContent.add(rpt);
			
			totalElapsedTime = totalElapsedTime.add(elapsedTimeLob);
			totalRequests = totalRequests.add(requestsProcessed);
			totalAverage = totalAverage.add(averageTAT);
			
			ArrayList detail = header.getTurnAroundTimeDetails();

			for (int d=0; d<detail.size(); d++) {
				TurnAroundTimeReportData det = new TurnAroundTimeReportData();
				TurnAroundTimeData content = (TurnAroundTimeData) detail.get(d);

				det.setLineOfBusiness("");
				det.setReferenceNumber(content.getReferenceNum());
				det.setDateStart(content.getStartDate());
				det.setDateEnd(content.getEndDate());
				
				BigDecimal detailElapsedTime = content.getElapsedTime();
				if (Integer.parseInt(reportType) == TurnAroundTimeParam.TAT_IN_HOURS) {
					detailElapsedTime = ValueConverter.convertToHours(detailElapsedTime);
				}
				det.setElapsedTime(String.valueOf(detailElapsedTime));
				
				det.setProcessedCount("");
				det.setAverageTAT("");
				
				newContent.add(det);
			}
		}

		
		TurnAroundTimeReportData totDetails = new TurnAroundTimeReportData();
		totDetails.setLineOfBusiness(IUMReportConstants.REPORT_REC_GTOTAVE);
		totDetails.setReferenceNumber("");
		totDetails.setDateStart("");
		totDetails.setDateEnd("");
		totDetails.setElapsedTime(String.valueOf(totalElapsedTime));
		totDetails.setProcessedCount(String.valueOf(totalRequests));
		totDetails.setAverageTAT(String.valueOf(totalAverage));
		newContent.add(totDetails);
		LOGGER.info("applyLayout end");
		return (newContent);
	}
	
	
	/**
	 * @param filter
	 * @return
	 */
	public ByteArrayOutputStream generatePrinterFriendly(TurnAroundTimeParam filter) {
		
		LOGGER.info("generatePrinterFriendly start");
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ArrayList report = new ArrayList();
		
		try
		{
			
			report = this.generate(filter); 

			PrintableForm pdfReport = new PrintableForm(PrintableForm.PAGE_PORTRAIT);
			pdfReport.setReportTitle(IUMReportConstants.REPORT_TITLE_TAT);
			
			StatusCHDao daoStatus = new StatusCHDao();
			String statusParm =  daoStatus.getDescription(filter.getStartStatus()) + " to " + daoStatus.getDescription(filter.getEndStatus());
			
			pdfReport.defineParameter(filter.getStartDate(), filter.getEndDate());
			pdfReport.defineParameter(IUMReportConstants.PARAMETER_STATUS, statusParm);			
												
			pdfReport.setColumnWidths(new int[] {18, 18, 34, 10, 10, 10});
			
			pdfReport.defineColumnTitle(IUMReportConstants.REPORT_COLUMN_BUSINESS_LINE);
			pdfReport.defineColumnTitle(IUMReportConstants.REPORT_COLUMN_AR_NO);
			
			ArrayList childCols = new ArrayList();
			childCols.add(IUMReportConstants.REPORT_COLUMN_START);
			childCols.add(IUMReportConstants.REPORT_COLUMN_END);
			pdfReport.defineComplexColumnTitle(IUMReportConstants.REPORT_COLUMN_STATUS_PERIOD, childCols);
			
			String rptUnit = " " + (filter.getUnit().equals(String.valueOf(TurnAroundTimeParam.TAT_IN_DAYS)) ? IUMReportConstants.REPORT_TAT_DAYS : IUMReportConstants.REPORT_TAT_HOUR);
			
			pdfReport.defineColumnTitle(IUMReportConstants.REPORT_COLUMN_ELAPSED_TIME + rptUnit);
			pdfReport.defineColumnTitle(IUMReportConstants.REPORT_COLUMN_REQUESTS_PROC);
			pdfReport.defineColumnTitle(IUMReportConstants.REPORT_COLUMN_AVERAGE_TAT + rptUnit);
			

			for (int i=0; i<report.size(); i++) {
				
				pdfReport.setColumnValueWidths(new int[] {18, 18, 17, 17, 10, 10, 10});
				TurnAroundTimeReportData data = (TurnAroundTimeReportData) report.get(i);
				
				int rowType = PrintableForm.COL_TYPE_NORMAL;
				if (data.getReferenceNumber().length() == 0) rowType = PrintableForm.COL_TYPE_GROUP;
				if (data.getReferenceNumber().length() == 0 && i == report.size() - 1)  pdfReport.writeEndContent(); // add a row of empty cells with a line underneath
				pdfReport.defineContentValue(data.getLineOfBusiness(), PrintableForm.ALIGN_COL_LEFT, rowType);
				pdfReport.defineContentValue(data.getReferenceNumber(), PrintableForm.ALIGN_COL_LEFT, rowType);
				pdfReport.defineContentValue(data.getDateStart(), PrintableForm.ALIGN_COL_LEFT, rowType);
				pdfReport.defineContentValue(data.getDateEnd(), PrintableForm.ALIGN_COL_LEFT, rowType);
				pdfReport.defineContentValue(data.getElapsedTime(), PrintableForm.ALIGN_COL_RIGHT, rowType);
				pdfReport.defineContentValue(data.getProcessedCount(), PrintableForm.ALIGN_COL_RIGHT, rowType);
				pdfReport.defineContentValue(data.getAverageTAT(), PrintableForm.ALIGN_COL_RIGHT, rowType);
				
			}
			pdfReport.writeEndBar();
			baos = pdfReport.writeReport(report);			
						
		}
		catch (Exception e)
		{
			LOGGER.error(CodeHelper.getStackTrace(e));
		} 
		
		LOGGER.info("generatePrinterFriendly end");
		return (baos);
	}
	
	public void closeDB(Connection conn) {
		
		LOGGER.info("closeDB start");
		try {
			conn.close();
		} catch (Exception e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
		}
		LOGGER.info("closeDB end");
	}
}
