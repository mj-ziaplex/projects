/*
 * Created on Feb 19, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.reports;

import java.io.ByteArrayOutputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.slocpi.ium.IUMException;
import com.slocpi.ium.data.ApplicationsByStatusData;
import com.slocpi.ium.data.ApplicationsByStatusFilterData;
import com.slocpi.ium.data.dao.AssessmentRequestDAO;
import com.slocpi.ium.data.util.DataSourceProxy;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.forms.PrintableForm;

/**
 * @author Nic Decapia
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class ApplicationsByStatusReport
{

	private static final Logger LOGGER = LoggerFactory.getLogger(ApplicationsByStatusReport.class);
	
	public ArrayList generateReport(ApplicationsByStatusFilterData filter) throws IUMException {
		
		LOGGER.info("generateReport start");
		ArrayList report = new ArrayList();
		
		try {
		
			AssessmentRequestDAO daoAR = new AssessmentRequestDAO();
			report = daoAR.retrieveApplicationsByStatus(filter);
			if (report.size() > 0) 	report = applyLayout(report, filter);
		}
		catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMException (e);
		}
		
		LOGGER.info("generateReport end");
		return (report);
	}
	
	private ArrayList applyLayout(ArrayList content, ApplicationsByStatusFilterData filter) {
		
		LOGGER.info("applyLayout start");
		 ArrayList newContent = new ArrayList();
		 int grandTotal = 0;
		 int type = filter.getFilterType();
		 
		ArrayList tmpContent = new ArrayList();
		ArrayList tmpDetail = new ArrayList();
		
		 for (int i=0; i<content.size(); i++) {
			 ApplicationsByStatusData data = (ApplicationsByStatusData) content.get(i);

			boolean isHeaderRecord = (data.getReferenceNumber().length() == 0 && data.getStatusDesc().length() == 0);
			boolean isSubHeaderRecord = (data.getReferenceNumber().length() == 0 && data.getStatusDesc().length() != 0);

			if (isHeaderRecord) {  
				newContent.add(data); 
				grandTotal += data.getCountAR(); 
				newContent.addAll(tmpContent);
				tmpContent.clear();
			}
			else if (isSubHeaderRecord) {
				data.setGroupName("");								
				tmpContent.add(data);
				tmpContent.addAll(tmpDetail);
				tmpDetail.clear();
			}
			else { 
				data.setGroupName("");
				data.setStatusDesc("");
				data.setCountAR(0);
				tmpDetail.add(data); 
			}
		 }
		 
		String GRAND_TOTAL = "Grand Total";
		ApplicationsByStatusData totalRec = new ApplicationsByStatusData();
		totalRec.setGroupName(GRAND_TOTAL);
		totalRec.setCountAR(grandTotal);
		newContent.add(totalRec); 
		
		LOGGER.info("applyLayout end");
		return (newContent);
	}

	/**
	 * @param filter
	 * @return
	 */
	public ByteArrayOutputStream generatePrinterFriendly(ApplicationsByStatusFilterData filter) {
		
		LOGGER.info("generatePrinterFriendly start");
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ArrayList report = new ArrayList();
		try
		{
		
			report = this.generateReport(filter); 
			PrintableForm pdfReport = new PrintableForm(PrintableForm.PAGE_PORTRAIT);
			pdfReport.setReportTitle(IUMReportConstants.REPORT_TITLE_SUMM_APP_STATUS);
			
			String reportType = new String();
			switch (filter.getFilterType()){
				case ApplicationsByStatusFilterData.FILTER_BY_BRANCH:
					reportType = IUMReportConstants.REPORT_TYPE_BY_BRANCH;
					pdfReport.defineColumnTitle(IUMReportConstants.REPORT_COLUMN_BRANCH);
					break;
				case ApplicationsByStatusFilterData.FILTER_BY_LOB:
					reportType = IUMReportConstants.REPORT_TYPE_BY_LOB;
					pdfReport.defineColumnTitle(IUMReportConstants.REPORT_COLUMN_LOB);		
					break;	
				case ApplicationsByStatusFilterData.FILTER_BY_UNDERWRITER:
					reportType = IUMReportConstants.REPORT_TYPE_BY_REQUIREMENT;
					pdfReport.defineColumnTitle(IUMReportConstants.REPORT_COLUMN_REQUIREMENT);
					break; 
			}
			pdfReport.defineColumnTitle(IUMReportConstants.REPORT_COLUMN_STATUS);
			pdfReport.defineColumnTitle(IUMReportConstants.REPORT_COLUMN_AR_NO);
			pdfReport.defineColumnTitle(IUMReportConstants.REPORT_COLUMN_DATE);
			pdfReport.defineColumnTitle(IUMReportConstants.REPORT_COLUMN_ORDERED);

			pdfReport.defineParameter(IUMReportConstants.PARAMETER_TYPE, reportType);
			pdfReport.defineParameter(filter.getStartDate(), filter.getEndDate());			
			
			pdfReport.setColumnWidths(new int[] {35, 20, 20, 15, 10});

		
			for (int i=0; i<report.size(); i++) {
				
				ApplicationsByStatusData data = (ApplicationsByStatusData) report.get(i);
				int rowType = PrintableForm.COL_TYPE_NORMAL;
				String groupName = data.getGroupName();

				if (groupName != "" && i != report.size()) rowType = PrintableForm.COL_TYPE_GROUP; 
				if (groupName != "" && i == report.size() - 1)  pdfReport.writeEndContent(); 

				String orderedCount = (data.getCountAR() != 0 ? String.valueOf(data.getCountAR()) : "");
				pdfReport.defineContentValue(groupName, PrintableForm.ALIGN_COL_LEFT, rowType);
				pdfReport.defineContentValue(data.getStatusDesc(), PrintableForm.ALIGN_COL_LEFT, rowType);
				pdfReport.defineContentValue(data.getReferenceNumber(), PrintableForm.ALIGN_COL_LEFT, rowType);
				pdfReport.defineContentValue(data.getStatusDate(), PrintableForm.ALIGN_COL_LEFT, rowType);
				pdfReport.defineContentValue(orderedCount, PrintableForm.ALIGN_COL_RIGHT, rowType);

			}
			pdfReport.writeEndBar();
			baos = pdfReport.writeReport(report);			
						
		}
		catch (Exception e)
		{
			LOGGER.error(CodeHelper.getStackTrace(e));
		}
		LOGGER.info("generatePrinterFriendly start");
		return (baos);
	}

	
}
