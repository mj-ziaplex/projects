/*
 * Created on Mar 9, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.reports;

import java.io.ByteArrayOutputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.slocpi.ium.IUMException;
import com.slocpi.ium.data.MedicalExamBillingData;
import com.slocpi.ium.data.MedicalExamBillingFilter;
import com.slocpi.ium.data.dao.MedicalDAO;
import com.slocpi.ium.data.util.DataSourceProxy;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.forms.PrintableForm;

/**
 * @author nic.decapia
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class StatementMedicalBillReport {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(StatementMedicalBillReport.class);
	
	public ArrayList generate(MedicalExamBillingFilter filter) throws IUMException
	{
		LOGGER.info("generate start");
		ArrayList report = new ArrayList();
		DataSourceProxy dsp = new DataSourceProxy();
		Connection con = null;
		try {
			con = dsp.getConnection();
		    MedicalDAO daoMed = new MedicalDAO(con);
			report = daoMed.retrieveMedicalExamBilling(filter);
			if (report.size() > 0) report = applyLayout(report, filter);
		}                
		catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMException (e);
		}
		finally {
			dsp.closeConnection(con);
		}
		LOGGER.info("generate end");
		return (report);
	}

	private ArrayList applyLayout(ArrayList content, MedicalExamBillingFilter filter) {
		
		LOGGER.info("applyLayout start");
		 ArrayList newContent = new ArrayList();
		 ArrayList tmpContent = new ArrayList();
		 
		for (int i=0; i<content.size(); i++) {
		 	MedicalExamBillingData rptData = (MedicalExamBillingData) content.get(i);
			
		 	if (filter.getReportType() == MedicalExamBillingFilter.REPORT_TYPE_LABORATORY) {
				
				if (rptData.getBranch() == null) {
					rptData.setInsured("");	
				}
				else{
					rptData.setName(rptData.getSubName());
				}
				newContent.add(rptData);				
			}
			else {
				
				if (rptData.getBranch() == null) {
					
					rptData.setInsured("");	
					newContent.add(rptData);
					newContent.addAll(tmpContent);
					tmpContent.clear();
				}
				else {
					rptData.setName(rptData.getSubName());
					tmpContent.add(rptData);
				}
			}
		 }
		LOGGER.info("applyLayout end");
		 return (newContent);
	}

	public ByteArrayOutputStream generatePrinterFriendly(MedicalExamBillingFilter filter) {
		
		LOGGER.info("generatePrinterFriendly start");
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ArrayList report = new ArrayList();
		try
		{
			
			report = this.generate(filter);
			PrintableForm pdfReport = new PrintableForm(PrintableForm.PAGE_LANDSCAPE);

			pdfReport.setReportTitle(IUMReportConstants.REPORT_TITLE_STMT_MED);
		
			pdfReport.setColTitleFontSize(7);
			String reportType = new String();
			switch (filter.getReportType()){
				case MedicalExamBillingFilter.REPORT_TYPE_AGENT:
					reportType = IUMReportConstants.REPORT_TYPE_PER_AGENT;
					pdfReport.setColumnWidths(new int[] {10, 8, 7, 7, 7, 8, 8, 10, 7, 7, 7, 7, 7});
					pdfReport.defineColumnTitle(IUMReportConstants.REPORT_COLUMN_LAB_EXAM_AGENT);
					break;
				case MedicalExamBillingFilter.REPORT_TYPE_EXAMINER:
					reportType = IUMReportConstants.REPORT_TYPE_PER_EXAMINER;
					pdfReport.setColumnWidths(new int[] {10, 9, 8, 8, 8, 9, 9, 10, 8, 7, 7, 7});
					pdfReport.defineColumnTitle(IUMReportConstants.REPORT_COLUMN_EXAM_AGENT);
					break;	
				case MedicalExamBillingFilter.REPORT_TYPE_LABORATORY:
					reportType = IUMReportConstants.REPORT_TYPE_PER_LABORATORY;
					pdfReport.setColumnWidths(new int[] {10, 9, 8, 8, 8, 9, 9, 10, 8, 7, 7, 7});
					pdfReport.defineColumnTitle(IUMReportConstants.REPORT_COLUMN_LAB_AGENT);
					break; 
			}
			pdfReport.defineColumnTitle(IUMReportConstants.REPORT_COLUMN_BRANCH);
			pdfReport.defineColumnTitle(IUMReportConstants.REPORT_COLUMN_EXAM_DATE);
			pdfReport.defineColumnTitle(IUMReportConstants.REPORT_COLUMN_EXAM_TYPE);
			pdfReport.defineColumnTitle(IUMReportConstants.REPORT_COLUMN_AR_POL_NO);
			pdfReport.defineColumnTitle(IUMReportConstants.REPORT_COLUMN_NAME_INSURED);
			pdfReport.defineColumnTitle(IUMReportConstants.REPORT_COLUMN_LOB);
			pdfReport.defineColumnTitle(IUMReportConstants.REPORT_COLUMN_REASON);
			pdfReport.defineColumnTitle(IUMReportConstants.REPORT_COLUMN_EXAM_PLACE);
			pdfReport.defineColumnTitle(IUMReportConstants.REPORT_COLUMN_EXAM_FEE);
			pdfReport.defineColumnTitle(IUMReportConstants.REPORT_COLUMN_TAX_WHELD);
			if (filter.getReportType() == MedicalExamBillingFilter.REPORT_TYPE_AGENT) {
				pdfReport.defineColumnTitle(IUMReportConstants.REPORT_COLUMN_CHRG_AMT);
			}
			pdfReport.defineColumnTitle(IUMReportConstants.REPORT_COLUMN_PAYABLE);
			pdfReport.defineParameter(filter.getStartDate(), filter.getEndDate());	
			pdfReport.defineParameter(IUMReportConstants.PARAMETER_TYPE, reportType);
					
		
			for (int i=0; i<report.size(); i++) {
			
				MedicalExamBillingData data = (MedicalExamBillingData) report.get(i);
				String officeName = data.getBranch();
				
				int rowType = PrintableForm.COL_TYPE_NORMAL;
				if (officeName == null && i != report.size()) rowType = PrintableForm.COL_TYPE_GROUP; // this is a header record

				pdfReport.defineContentValue(data.getName(), PrintableForm.ALIGN_COL_LEFT, rowType);
				pdfReport.defineContentValue(officeName, PrintableForm.ALIGN_COL_LEFT, rowType);
				pdfReport.defineContentValue(data.getExamDate(), PrintableForm.ALIGN_COL_LEFT, rowType);
				pdfReport.defineContentValue(data.getExamType(), PrintableForm.ALIGN_COL_LEFT, rowType);
				pdfReport.defineContentValue(data.getPolicyNumber(), PrintableForm.ALIGN_COL_LEFT, rowType);
				pdfReport.defineContentValue(data.getInsured(), PrintableForm.ALIGN_COL_LEFT, rowType);
				pdfReport.defineContentValue(data.getLob(), PrintableForm.ALIGN_COL_LEFT, rowType);
				pdfReport.defineContentValue(data.getReason(), PrintableForm.ALIGN_COL_LEFT, rowType);
				pdfReport.defineContentValue(data.getExamPlace(), PrintableForm.ALIGN_COL_LEFT, rowType);
								
				pdfReport.defineContentValue(data.getExamFee(), PrintableForm.ALIGN_COL_RIGHT, rowType);
				pdfReport.defineContentValue(data.getTaxWithheld(), PrintableForm.ALIGN_COL_RIGHT, rowType);
				if (filter.getReportType() == MedicalExamBillingFilter.REPORT_TYPE_AGENT) {
					pdfReport.defineContentValue(data.getChargeableAmt(), PrintableForm.ALIGN_COL_RIGHT, rowType);
				}
				pdfReport.defineContentValue(data.getPayableAmt(), PrintableForm.ALIGN_COL_RIGHT, rowType);
			}
			
			baos = pdfReport.writeReport(report);			
						
		}
		catch (Exception e)
		{
			LOGGER.error(CodeHelper.getStackTrace(e));
		}
		LOGGER.info("generatePrinterFriendly end");
		return (baos);
	}
	
	
	
}
