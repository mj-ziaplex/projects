/*
 * Created on Mar 4, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.reports;

import java.io.ByteArrayOutputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.slocpi.ium.IUMException;
import com.slocpi.ium.data.RequirementData;
import com.slocpi.ium.data.RequirementsOrderedData;
import com.slocpi.ium.data.RequirementsOrderedFilterData;
import com.slocpi.ium.data.SunLifeOfficeData;
import com.slocpi.ium.data.UserProfileData;
import com.slocpi.ium.data.dao.PolicyRequirementDAO;
import com.slocpi.ium.data.util.DataSourceProxy;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.forms.PrintableForm;

/**
 * @author Nic Decapia
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class RequirementsOrderedReport
{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(RequirementsOrderedReport.class);
	
	public ArrayList generate(RequirementsOrderedFilterData filter) throws IUMException
	{
		LOGGER.info("generate start");
		ArrayList report = new ArrayList();
		
		try {
			
			PolicyRequirementDAO daoPR = new PolicyRequirementDAO();
			report = daoPR.retrieveRequirementsOrdered(filter);
			if (report.size() > 0) report = applyLayout(report, filter);
		}                
		catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMException (e);
		}
		
		LOGGER.info("generate end");
		return (report);
	}
	
	public ByteArrayOutputStream generatePrinterFriendly(RequirementsOrderedFilterData filter) {
		
		LOGGER.info("generatePrinterFriendly start");
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ArrayList report = new ArrayList();
		try
		{
			
			report = this.generate(filter); 

			PrintableForm pdfReport = new PrintableForm(PrintableForm.PAGE_PORTRAIT);

			
			pdfReport.setReportTitle(IUMReportConstants.REPORT_TITLE_REQT_ORD);
			
			String reportType = new String();
			switch (filter.getReportType()){
				case RequirementsOrderedFilterData.TYPE_BRANCH:
					reportType = IUMReportConstants.REPORT_TYPE_BY_BRANCH;
					pdfReport.defineColumnTitle(IUMReportConstants.REPORT_COLUMN_BRANCH);
					pdfReport.defineColumnTitle(IUMReportConstants.REPORT_COLUMN_REQUIREMENT);
					break;
				case RequirementsOrderedFilterData.TYPE_USER:
					reportType = IUMReportConstants.REPORT_TYPE_BY_UNDERWRITER;
					pdfReport.defineColumnTitle(IUMReportConstants.REPORT_COLUMN_UNDERWRITER);		
					pdfReport.defineColumnTitle(IUMReportConstants.REPORT_COLUMN_REQUIREMENT);
					break;	
				case RequirementsOrderedFilterData.TYPE_REQUIREMENT:
					reportType = IUMReportConstants.REPORT_TYPE_BY_REQUIREMENT;
					pdfReport.defineColumnTitle(IUMReportConstants.REPORT_COLUMN_REQUIREMENT);
					pdfReport.defineColumnTitle(IUMReportConstants.REPORT_COLUMN_BRANCH);
					break; 
			}
			pdfReport.defineParameter(IUMReportConstants.PARAMETER_TYPE, reportType);
			pdfReport.defineParameter(filter.getStartDate(), filter.getEndDate());			
			pdfReport.defineColumnTitle(IUMReportConstants.REPORT_COLUMN_ORDERED);
			pdfReport.setColumnWidths(new int[] {30, 60, 10});

			for (int i=0; i<report.size(); i++) {
			
				RequirementsOrderedData data = (RequirementsOrderedData) report.get(i);
				String reqtCode = data.getRequirement().getReqtCode();
				String reqtDesc = data.getRequirement().getReqtDesc();
				String officeName = data.getDepartment().getOfficeName();
				
				int rowType = PrintableForm.COL_TYPE_NORMAL;

				switch (filter.getReportType()){
					case RequirementsOrderedFilterData.TYPE_BRANCH:
						if (reqtCode == null && i != report.size()) rowType = PrintableForm.COL_TYPE_GROUP;
						if (reqtCode == null && i == report.size() - 1)  pdfReport.writeEndContent(); 
						pdfReport.setColumnValueWidths(new int[] {30, 10, 50, 10});
						pdfReport.defineContentValue(officeName, PrintableForm.ALIGN_COL_LEFT, rowType);
						pdfReport.defineContentValue(reqtCode, PrintableForm.ALIGN_COL_LEFT, rowType);
						pdfReport.defineContentValue(reqtDesc, PrintableForm.ALIGN_COL_LEFT, rowType);
						break;
					case RequirementsOrderedFilterData.TYPE_USER:
						String userName = "";
						String userFirstName = (data.getUser().getFirstName() != null ? data.getUser().getFirstName() : "");
						if (data.getUser().getLastName().length() > 0)  userName = data.getUser().getLastName() + (userFirstName.length() > 0 ? ", " + data.getUser().getFirstName() : "");
						if (reqtCode == null && i != report.size()) rowType = PrintableForm.COL_TYPE_GROUP; 					
						if (reqtCode == null && i == report.size() - 1)  pdfReport.writeEndContent(); 	
						pdfReport.setColumnValueWidths(new int[] {30, 10, 50, 10});
						pdfReport.defineContentValue(userName, PrintableForm.ALIGN_COL_LEFT, rowType);
						pdfReport.defineContentValue(reqtCode, PrintableForm.ALIGN_COL_LEFT, rowType);
						pdfReport.defineContentValue(reqtDesc, PrintableForm.ALIGN_COL_LEFT, rowType);
						break;	
					case RequirementsOrderedFilterData.TYPE_REQUIREMENT:
						if (officeName == null && i != report.size()) rowType = PrintableForm.COL_TYPE_GROUP;
						if (officeName == null && i == report.size() - 1)  pdfReport.writeEndContent(); 
						pdfReport.setColumnValueWidths(new int[] {10, 50, 30, 10});
						pdfReport.defineContentValue(reqtCode, PrintableForm.ALIGN_COL_LEFT, rowType);
						pdfReport.defineContentValue(reqtDesc, PrintableForm.ALIGN_COL_LEFT, rowType);
						pdfReport.defineContentValue(officeName, PrintableForm.ALIGN_COL_LEFT, rowType);
						break; 
				}
				pdfReport.defineContentValue(String.valueOf(data.getRequirementsOrdered()), PrintableForm.ALIGN_COL_RIGHT, rowType);
			}
			pdfReport.writeEndBar();
			
			
			baos = pdfReport.writeReport(report);			
						
		}
		catch (Exception e)
		{
			LOGGER.error(CodeHelper.getStackTrace(e));
		}
		LOGGER.info("generatePrinterFriendly end");
		return (baos);
	}
	

	private ArrayList applyLayout(ArrayList content, RequirementsOrderedFilterData filter) {
		
		LOGGER.info("applyLayout start");
		 ArrayList newContent = new ArrayList();
		 long grandTotal = 0;
		 int type = filter.getReportType();
		 
		ArrayList tmpContent = new ArrayList();
		
		 for (int i=0; i<content.size(); i++) {
			
			RequirementsOrderedData data = (RequirementsOrderedData) content.get(i);
			boolean isHeaderRecord = false;
			
			switch (type) {
			   case RequirementsOrderedFilterData.TYPE_BRANCH:
			   case RequirementsOrderedFilterData.TYPE_USER:
				   isHeaderRecord = (data.getRequirement().getReqtCode() == null);
				   break;	
			   case RequirementsOrderedFilterData.TYPE_REQUIREMENT:
				   isHeaderRecord = (data.getDepartment().getOfficeName() == null);
				   break;
			}

			if (isHeaderRecord) {  
				newContent.add(data); 
				grandTotal += data.getRequirementsOrdered(); 
				
				newContent.addAll(tmpContent);
				tmpContent.clear();

			}
			else { 
				switch (type) {
				   case RequirementsOrderedFilterData.TYPE_BRANCH:
				   		SunLifeOfficeData slo = new SunLifeOfficeData();
				   		slo.setOfficeName("");
						data.setDepartment(slo);
						break;
				   case RequirementsOrderedFilterData.TYPE_USER:
				   		UserProfileData usr = new UserProfileData();
						usr.setLastName("");
					   	data.setUser(usr);
					   	break;	
				   case RequirementsOrderedFilterData.TYPE_REQUIREMENT:
				   		RequirementData reqt = new RequirementData();
						reqt.setReqtCode("");
						reqt.setReqtDesc("");
				   		data.setRequirement(reqt);
					   	break;
				}
				tmpContent.add(data); 
			}
		 }
		 
		
		 String GRAND_TOTAL = "Grand Total";
		 RequirementsOrderedData totalRec = new RequirementsOrderedData();
		 switch (type) {
		 	case RequirementsOrderedFilterData.TYPE_BRANCH:
		 		SunLifeOfficeData slo = new SunLifeOfficeData();
		 		slo.setOfficeName(GRAND_TOTAL);
		 		totalRec.setDepartment(slo);
		 		break;	
			case RequirementsOrderedFilterData.TYPE_REQUIREMENT:
				RequirementData reqt = new RequirementData();
				reqt.setReqtCode(GRAND_TOTAL);
				totalRec.setRequirement(reqt);
				break;
			case RequirementsOrderedFilterData.TYPE_USER:
				UserProfileData usr = new UserProfileData();
				usr.setLastName(GRAND_TOTAL);
				totalRec.setUser(usr);
				break;
		 }
		totalRec.setRequirementsOrdered(grandTotal);
		newContent.add(totalRec); 
		LOGGER.info("applyLayout end");
		 return (newContent);
	}
}
