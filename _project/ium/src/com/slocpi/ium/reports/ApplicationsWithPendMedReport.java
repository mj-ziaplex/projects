/*
 * Created on Mar 5, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.reports;

import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.slocpi.ium.IUMException;
import com.slocpi.ium.data.ARWithPendingMedData;
import com.slocpi.ium.data.ARWithPendingMedFilterData;
import com.slocpi.ium.data.ExaminerData;
import com.slocpi.ium.data.LaboratoryData;
import com.slocpi.ium.data.dao.MedicalDAO;
import com.slocpi.ium.data.util.DataSourceProxy;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.forms.PrintableForm;

/**
 * @author Nic Decapia
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class ApplicationsWithPendMedReport
{
	private static final Logger LOGGER = LoggerFactory.getLogger(ApplicationsWithPendMedReport.class);
	
	public ArrayList generate(ARWithPendingMedFilterData criteria) throws IUMException {
		
		LOGGER.info("generate start");
		ArrayList report = new ArrayList();
		DataSourceProxy dsp = new DataSourceProxy();
		Connection con = null;
		try{
			con = dsp.getConnection();
			MedicalDAO daoMedical = new MedicalDAO(con);
			report = daoMedical.retrievePendingMedicalRecords(criteria);
			if (report.size() > 0) report = applyLayout(report, criteria);
		}
		catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMException (e);
		}
		finally {
			dsp.closeConnection(con);
		}
		LOGGER.info("generate end");
		return (report);
	}	

	private ArrayList applyLayout(ArrayList content, ARWithPendingMedFilterData filter) {
		
		LOGGER.info("applyLayout start");
		 ArrayList newContent = new ArrayList();
		 int grandTotal = 0;
		 int type = filter.getReportType();
		 
		ArrayList tmpContent = new ArrayList();
		 
		 for (int i=0; i<content.size(); i++) {
			 ARWithPendingMedData data = (ARWithPendingMedData) content.get(i);
			boolean isHeaderRecord = (data.getReferenceNumber().length() == 0);

			if (isHeaderRecord) { 
				newContent.add(data);
				grandTotal += data.getTotalCount();
				
				newContent.addAll(tmpContent);
				tmpContent.clear();
			}
			else {
				switch (type) {
					case ARWithPendingMedFilterData.BRANCH_REPORT_TYPE:
						data.setBranchName("");
						break;
					case ARWithPendingMedFilterData.EXAMINER_REPORT_TYPE:
					case ARWithPendingMedFilterData.LABORATORY_REPORT_TYPE:
						data.setNameExaminerLab("");
						break;
					case ARWithPendingMedFilterData.UNDERWRITER_REPORT_TYPE:
						data.setUnderwriterGivenName("");
						data.setUnderwriterLastName("");
						data.setUnderwriterMiddleName("");
						break;
				}
				data.setTotalCount(0);
				tmpContent.add(data); 
			}
		 }
		 
		String GRAND_TOTAL = "Grand Total";
		ARWithPendingMedData totalRec = new ARWithPendingMedData();
		switch (type) {
			case ARWithPendingMedFilterData.BRANCH_REPORT_TYPE:
				totalRec.setBranchName(GRAND_TOTAL);
				break;
			case ARWithPendingMedFilterData.EXAMINER_REPORT_TYPE:
			case ARWithPendingMedFilterData.LABORATORY_REPORT_TYPE:
				totalRec.setNameExaminerLab(GRAND_TOTAL);
				break;
			case ARWithPendingMedFilterData.UNDERWRITER_REPORT_TYPE:
				totalRec.setUnderwriterLastName(GRAND_TOTAL);
				break;
		}
		totalRec.setTotalCount(grandTotal);
		newContent.add(totalRec); 
		 
		LOGGER.info("applyLayout end");
		return (newContent);
	}

	/**
	 * @param filter
	 * @return
	 */
	public ByteArrayOutputStream generatePrinterFriendly(ARWithPendingMedFilterData filter) {
	
		LOGGER.info("generatePrinterFriendly start");
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ArrayList report = new ArrayList();
		try
		{
			report = this.generate(filter); 
			PrintableForm pdfReport = new PrintableForm(PrintableForm.PAGE_LANDSCAPE);
			pdfReport.setReportTitle(IUMReportConstants.REPORT_TITLE_APPS_PENDS_MED);	
			
			String reportType = new String();
			switch (filter.getReportType()){
				case ARWithPendingMedFilterData.BRANCH_REPORT_TYPE:
					reportType = IUMReportConstants.REPORT_TYPE_BY_BRANCH;
					pdfReport.defineColumnTitle(IUMReportConstants.REPORT_COLUMN_BRANCH);
					pdfReport.defineColumnTitle(IUMReportConstants.REPORT_COLUMN_LAB_EXAM);
					break;
				case ARWithPendingMedFilterData.UNDERWRITER_REPORT_TYPE:
					reportType = IUMReportConstants.REPORT_TYPE_BY_UNDERWRITER;
					pdfReport.defineColumnTitle(IUMReportConstants.REPORT_COLUMN_UNDERWRITER);		
					pdfReport.defineColumnTitle(IUMReportConstants.REPORT_COLUMN_LAB_EXAM);
					break;	
				case ARWithPendingMedFilterData.EXAMINER_REPORT_TYPE:
					reportType = IUMReportConstants.REPORT_TYPE_BY_EXAMINER;
					pdfReport.defineColumnTitle(IUMReportConstants.REPORT_COLUMN_EXAMINER);
					pdfReport.defineColumnTitle(IUMReportConstants.REPORT_COLUMN_BRANCH);
					break; 
				case ARWithPendingMedFilterData.LABORATORY_REPORT_TYPE:
					reportType = IUMReportConstants.REPORT_TYPE_BY_LABORATORY;
					pdfReport.defineColumnTitle(IUMReportConstants.REPORT_COLUMN_LABORATORY);
					pdfReport.defineColumnTitle(IUMReportConstants.REPORT_COLUMN_BRANCH);
					break; 
			}
			pdfReport.defineColumnTitle(IUMReportConstants.REPORT_COLUMN_AR_NO);
			pdfReport.defineColumnTitle(IUMReportConstants.REPORT_COLUMN_TEST);
			pdfReport.defineColumnTitle(IUMReportConstants.REPORT_COLUMN_DATE_REQUEST);
			pdfReport.defineColumnTitle(IUMReportConstants.REPORT_COLUMN_DATE_SUB);
			pdfReport.defineColumnTitle(IUMReportConstants.REPORT_COLUMN_TOTAL_COUNT);
			pdfReport.defineParameter(filter.getDateStart(), filter.getDateEnd());
			pdfReport.defineParameter(IUMReportConstants.PARAMETER_TYPE, reportType);
			pdfReport.setColumnWidths(new int[] {25, 20, 15, 15, 10, 10, 5});

		
			for (int i=0; i<report.size(); i++) {
				
				ARWithPendingMedData data = (ARWithPendingMedData) report.get(i);
				String referenceNumber = data.getReferenceNumber();
				
				int rowType = PrintableForm.COL_TYPE_NORMAL;
				if (referenceNumber.length() == 0 && i != report.size()) rowType = PrintableForm.COL_TYPE_GROUP; // this is a header record
				if (referenceNumber.length() == 0 && i == report.size() - 1)  pdfReport.writeEndContent(); // add a row of empty cells with a line underneath

				switch (filter.getReportType()){
					case ARWithPendingMedFilterData.BRANCH_REPORT_TYPE:
						pdfReport.defineContentValue(data.getBranchName(), PrintableForm.ALIGN_COL_LEFT, rowType);
						pdfReport.defineContentValue(data.getNameExaminerLab(), PrintableForm.ALIGN_COL_LEFT, rowType);
						break;
					case ARWithPendingMedFilterData.UNDERWRITER_REPORT_TYPE:
						String userName = "";
						String userFirstName = (data.getUnderwriterGivenName() != null ? data.getUnderwriterGivenName() : "");
						if (data.getUnderwriterLastName().length() > 0)  userName = data.getUnderwriterLastName() + (userFirstName.length() > 0 ? ", " + userFirstName : "");
						pdfReport.defineContentValue(userName, PrintableForm.ALIGN_COL_LEFT, rowType);
						pdfReport.defineContentValue(data.getNameExaminerLab(), PrintableForm.ALIGN_COL_LEFT, rowType);
						break;	
					case ARWithPendingMedFilterData.EXAMINER_REPORT_TYPE:
					case ARWithPendingMedFilterData.LABORATORY_REPORT_TYPE:
						pdfReport.defineContentValue(data.getNameExaminerLab(), PrintableForm.ALIGN_COL_LEFT, rowType);
						pdfReport.defineContentValue(data.getBranchName(), PrintableForm.ALIGN_COL_LEFT, rowType);
						break; 						 
				}
				pdfReport.defineContentValue(referenceNumber, PrintableForm.ALIGN_COL_LEFT, rowType);
				pdfReport.defineContentValue(data.getTestDescription(), PrintableForm.ALIGN_COL_LEFT, rowType);
				pdfReport.defineContentValue(data.getDateRequested(), PrintableForm.ALIGN_COL_LEFT, rowType);
				pdfReport.defineContentValue(data.getDateSubmitted(), PrintableForm.ALIGN_COL_LEFT, rowType);
				pdfReport.defineContentValue((data.getTotalCount() != 0 ? String.valueOf(data.getTotalCount()) : ""), PrintableForm.ALIGN_COL_RIGHT, rowType);

			}
			pdfReport.writeEndBar();
			baos = pdfReport.writeReport(report);			
						
		}
		catch (Exception e)
		{
			LOGGER.error(CodeHelper.getStackTrace(e));
		}
		LOGGER.info("generatePrinterFriendly end");
		return (baos);

	}

	/**
	 * main method for testing purposes only
	 * @param filter
	 * @return
	 * @throws Exception
	 */
	public static void main (String[] args) {
		ApplicationsWithPendMedReport report = new ApplicationsWithPendMedReport();
		try {
			ARWithPendingMedFilterData filter = new ARWithPendingMedFilterData();
			// set the filter values
			filter.setDateStart("01JAN2004");
			filter.setDateEnd("31DEC2004");
			filter.setReportType(2);
			ExaminerData ex = new ExaminerData();
			ex.setExaminerId(-1);
			filter.setExaminer(ex);
			LaboratoryData lab = new LaboratoryData();
			lab.setLabId(-1);
			filter.setLaboratory(lab);
			ByteArrayOutputStream baos = report.generatePrinterFriendly(filter);
			FileOutputStream fos = new FileOutputStream("C:/pdftesting/test.pdf");
			baos.writeTo(fos);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

}
