/*
 * Created on Feb 17, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.reports;

import java.io.ByteArrayOutputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.slocpi.ium.IUMException;
import com.slocpi.ium.data.ApplicationsApprovedFilterData;
import com.slocpi.ium.data.AssessmentRequestData;
import com.slocpi.ium.data.LOBData;
import com.slocpi.ium.data.UserProfileData;
import com.slocpi.ium.data.dao.AssessmentRequestDAO;
import com.slocpi.ium.data.util.DataSourceProxy;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.forms.PrintableForm;

/**
 * @author Nic Decapia
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class ApplicationsApprovedReport
{
	private static final Logger LOGGER = LoggerFactory.getLogger(ApplicationsApprovedReport.class);
	
	public ArrayList generate(ApplicationsApprovedFilterData aaf) throws Exception {
		
		LOGGER.info("generate start");
		ArrayList report = new ArrayList();
		
		try {
		
			AssessmentRequestDAO daoAR = new AssessmentRequestDAO();
			report = daoAR.retrieveApplicationsApprovedReport(aaf);
			if (report.size() > 0 ) report = applyLayout(report);
		}
		catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMException(e);
		}
		
		LOGGER.info("generate end");
		return (report); 
	}

	/**
	 * @param report
	 * @param aaf
	 * @return
	 */
	private ArrayList applyLayout(ArrayList content)
	{
		
		LOGGER.info("applyLayout start");
		ArrayList newContent = new ArrayList();
		UserProfileData usr = new UserProfileData();
		LOBData lob = new LOBData();
		String requestingParty = "";
		String approvingParty = "";

		for (int i=0; i<content.size(); i++) {
			AssessmentRequestData data = (AssessmentRequestData) content.get(i);
			
			if (!requestingParty.equals(data.getLob().getLOBDesc())) {
				requestingParty = data.getLob().getLOBDesc();
				AssessmentRequestData grpRequestor = new AssessmentRequestData();
				usr.setDeptCode("");
				grpRequestor.setLob(data.getLob());
				grpRequestor.setUpdatedBy(usr);
				grpRequestor.setReferenceNumber("");
				
				newContent.add(grpRequestor);
			}
			
			if (!approvingParty.equals(data.getUpdatedBy().getDeptCode())) {
				approvingParty = data.getUpdatedBy().getDeptCode();
				AssessmentRequestData grpApprover = new AssessmentRequestData();
				lob.setLOBDesc("");
				grpApprover.setLob(lob);
				grpApprover.setUpdatedBy(data.getUpdatedBy());
				grpApprover.setReferenceNumber("");
				newContent.add(grpApprover);
			}
			
			usr.setDeptCode("");
			data.setUpdatedBy(usr);

			lob.setLOBDesc("");
			data.setLob(lob);

			newContent.add(data);
		}
		LOGGER.info("applyLayout end");
		return (newContent);
	}

	/**
	 * @param filter
	 * @return
	 */
	public ByteArrayOutputStream generatePrinterFriendly(ApplicationsApprovedFilterData filter) {
		
		LOGGER.info("generatePrinterFriendly start");
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ArrayList report = new ArrayList();
		try
		{
		
			report = this.generate(filter); 
			PrintableForm pdfReport = new PrintableForm(PrintableForm.PAGE_PORTRAIT);

			pdfReport.setReportTitle(IUMReportConstants.REPORT_TITLE_APPS_APPROVED);
			
			pdfReport.defineParameter(filter.getStartDate(), filter.getEndDate());	
			int totalCount = 0;
					
			pdfReport.defineColumnTitle(IUMReportConstants.REPORT_COLUMN_APPROVER);
			pdfReport.defineColumnTitle(IUMReportConstants.REPORT_COLUMN_REQUESTOR);
			pdfReport.defineColumnTitle(IUMReportConstants.REPORT_COLUMN_AR_NO);
			
			pdfReport.setColumnWidths(new int[] {30, 50, 20});

			for (int i=0; i<report.size(); i++) {
			
				AssessmentRequestData data = (AssessmentRequestData) report.get(i);
				pdfReport.defineContentValue(data.getLob().getLOBDesc(), PrintableForm.ALIGN_COL_LEFT, PrintableForm.COL_TYPE_NORMAL);
				pdfReport.defineContentValue(data.getUpdatedBy().getDeptCode(), PrintableForm.ALIGN_COL_LEFT, PrintableForm.COL_TYPE_NORMAL);
				pdfReport.defineContentValue(data.getReferenceNumber(), PrintableForm.ALIGN_COL_LEFT, PrintableForm.COL_TYPE_NORMAL);
				
				if (data.getReferenceNumber().length() > 0) totalCount++;
			}
			
			pdfReport.defineParameter(IUMReportConstants.PARAMETER_COUNT, String.valueOf(totalCount));
			
			baos = pdfReport.writeReport(report);			
					
		}
		catch (Exception e)
		{
			LOGGER.error(CodeHelper.getStackTrace(e));
		}

		LOGGER.info("generatePrinterFriendly end");
		return (baos);
	}


}
