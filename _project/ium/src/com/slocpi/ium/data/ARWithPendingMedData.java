/*
 * Created on Feb 22, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.data;

/**
 * @author Nic Decapia
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class ARWithPendingMedData
{
	private String branchName = new String();
	private String underwriterLastName = new String();
	private String underwriterGivenName = new String();
	private String underwriterMiddleName = new String();
	
	private String referenceNumber = new String();
	private String nameExaminerLab = new String();
	private String testCode = new String();
	private String testDescription = new String();
	private String testType = new String();
	private String dateRequested = new String();
	private String dateSubmitted = new String();
	private int totalCount = 0;
	/**
	 * @return
	 */
	public String getBranchName()
	{
		return branchName;
	}

	/**
	 * @return
	 */
	public String getDateRequested()
	{
		return dateRequested;
	}

	/**
	 * @return
	 */
	public String getDateSubmitted()
	{
		return dateSubmitted;
	}

	/**
	 * @return
	 */
	public String getReferenceNumber()
	{
		return referenceNumber;
	}

	/**
	 * @return
	 */
	public String getTestCode()
	{
		return testCode;
	}

	/**
	 * @return
	 */
	public String getTestDescription()
	{
		return testDescription;
	}

	/**
	 * @return
	 */
	public String getTestType()
	{
		return testType;
	}

	/**
	 * @return
	 */
	public int getTotalCount()
	{
		return totalCount;
	}

	/**
	 * @return
	 */
	public String getUnderwriterGivenName()
	{
		return underwriterGivenName;
	}

	/**
	 * @return
	 */
	public String getUnderwriterLastName()
	{
		return underwriterLastName;
	}

	/**
	 * @return
	 */
	public String getUnderwriterMiddleName()
	{
		return underwriterMiddleName;
	}

	/**
	 * @param string
	 */
	public void setBranchName(String string)
	{
		branchName = string;
	}

	/**
	 * @param string
	 */
	public void setDateRequested(String string)
	{
		dateRequested = string;
	}

	/**
	 * @param string
	 */
	public void setDateSubmitted(String string)
	{
		dateSubmitted = string;
	}

	/**
	 * @param string
	 */
	public void setReferenceNumber(String string)
	{
		referenceNumber = string;
	}

	/**
	 * @param string
	 */
	public void setTestCode(String string)
	{
		testCode = string;
	}

	/**
	 * @param string
	 */
	public void setTestDescription(String string)
	{
		testDescription = string;
	}

	/**
	 * @param string
	 */
	public void setTestType(String string)
	{
		testType = string;
	}

	/**
	 * @param i
	 */
	public void setTotalCount(int i)
	{
		totalCount = i;
	}

	/**
	 * @param string
	 */
	public void setUnderwriterGivenName(String string)
	{
		underwriterGivenName = string;
	}

	/**
	 * @param string
	 */
	public void setUnderwriterLastName(String string)
	{
		underwriterLastName = string;
	}

	/**
	 * @param string
	 */
	public void setUnderwriterMiddleName(String string)
	{
		underwriterMiddleName = string;
	}

	/**
	 * @return
	 */
	public String getNameExaminerLab()
	{
		return nameExaminerLab;
	}

	/**
	 * @param string
	 */
	public void setNameExaminerLab(String string)
	{
		nameExaminerLab = string;
	}

}
