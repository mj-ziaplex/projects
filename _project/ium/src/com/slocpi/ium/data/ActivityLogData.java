/*
 * Created on Dec 23, 2003
 * package name = com.slocpi.ium.data
 * file name    = ActivityLogData.java
 */
package com.slocpi.ium.data;

import java.util.Date;

/**
 * Container for Activity Log reference information.
 * @author Engel
 * 
 */
public class ActivityLogData {

	private UserProfileData user;
	private StatusData status;
	private Date dateTime;
	private String lapseTime;
	private String assignedTo;
	
	
	/**
	 * @return
	 */
	public String getAssignedTo() {
		return assignedTo;
	}

	/**
	 * @return
	 */
	public Date getDateTime() {
		return dateTime;
	}

	/**
	 * @return
	 */
	public String getLapseTime() {
		return lapseTime;
	}

	/**
	 * @return
	 */
	public StatusData getStatus() {
		return status;
	}

	/**
	 * @return
	 */
	public UserProfileData getUser() {
		return user;
	}

	/**
	 * @param string
	 */
	public void setAssignedTo(String string) {
		assignedTo = string;
	}

	/**
	 * @param date
	 */
	public void setDateTime(Date date) {
		dateTime = date;
	}

	/**
	 * @param l
	 */
	public void setLapseTime(String l) {
		lapseTime = l;
	}

	/**
	 * @param data
	 */
	public void setStatus(StatusData data) {
		status = data;
	}

	/**
	 * @param data
	 */
	public void setUser(UserProfileData data) {
		user = data;
	}

}
