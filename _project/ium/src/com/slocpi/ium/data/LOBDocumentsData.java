/*
 * Created on Dec 22, 2003
 * package name = com.slocpi.ium.data
 * file name    = LOBDocumentsData.java
 */
package com.slocpi.ium.data;

/**
 * Container for LOB Document reference information.
 * @author Engel
 * 
 */
public class LOBDocumentsData {

	private String LOBCode;
	private String docCode;
	/**
	 * TODO method description getDocCode
	 * @return
	 */
	public String getDocCode() {
		return docCode;
	}

	/**
	 * TODO method description getLOBCode
	 * @return
	 */
	public String getLOBCode() {
		return LOBCode;
	}

	/**
	 * TODO method description setDocCode
	 * @param string
	 */
	public void setDocCode(String string) {
		docCode = string;
	}

	/**
	 * TODO method description setLOBCode
	 * @param string
	 */
	public void setLOBCode(String string) {
		LOBCode = string;
	}

}
