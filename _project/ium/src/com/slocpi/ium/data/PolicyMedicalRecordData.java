/*
 * Created on Dec 22, 2003
 * package name = com.slocpi.ium.data
 * file name    = PolicyMedicalRecordData.java
 */
package com.slocpi.ium.data;

/**
 * Contains information pertaining policy medical record of an applicant. 
 * @author Engel
 * 
 */
public class PolicyMedicalRecordData {

	private long medicalRecordId;
	private String referenceNumber;
	private String associationType;
	/**
	 * TODO method description getAssociationType
	 * @return
	 */
	public String getAssociationType() {
		return associationType;
	}

	/**
	 * TODO method description getMedicalRecordId
	 * @return
	 */
	public long getMedicalRecordId() {
		return medicalRecordId;
	}

	/**
	 * TODO method description getReferenceNumber
	 * @return
	 */
	public String getReferenceNumber() {
		return referenceNumber;
	}

	/**
	 * TODO method description setAssociationType
	 * @param string
	 */
	public void setAssociationType(String string) {
		associationType = string;
	}

	/**
	 * TODO method description setMedicalRecordId
	 * @param l
	 */
	public void setMedicalRecordId(long l) {
		medicalRecordId = l;
	}

	/**
	 * TODO method description setReferenceNumber
	 * @param string
	 */
	public void setReferenceNumber(String string) {
		referenceNumber = string;
	}

}
