/*
 * Created on Dec 18, 2003
 */
package com.slocpi.ium.data;

import java.util.ArrayList;
import java.util.Date;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Contains information pertaining to an assessment request.
 * @author Engel
 *
 */
public class AssessmentRequestData {

	private static final Logger LOGGER = LoggerFactory.getLogger(AssessmentRequestData.class);
	private String referenceNumber;
	private String policySuffix = "";	
	private LOBData lob;
	private String sourceSystem;
	private double amountCovered;
	private double premium;
	private ClientData	insured;
	private CDSMortalityRatingData insuredMortalityRating;
	private ClientData	owner;
	private CDSMortalityRatingData ownerMortalityRating;
	private ClientDataSheetData clientDataSheet;
	private SunLifeOfficeData branch;
	private UserProfileData agent;
	private StatusData status;
	private Date statusDate;
	private UserProfileData assignedTo;
	private UserProfileData underwriter;
	private UserProfileData folderLocation;
	private String remarks;
	private Date applicationReceivedDate;		
  	private Date forwardedDate;					
	private UserProfileData createdBy;
	private Date createdDate;					
	private UserProfileData updatedBy;
	private Date updatedDate;					
	private ArrayList kickOutMessages;
	private String xmlRecord;
	private String kickOutCode;
	private String transmitIndicator;
	private String currentUser;
	private String autoSettleIndicator;
	private ExceptionDetailsData autoSettleError;
	private String koCountInd;
	private String reqtCountInd;
	

	public String toString(){
		return ToStringBuilder.reflectionToString(this, ToStringStyle.DEFAULT_STYLE);
	}
	
	
	
	/**
	 * TODO method description setKickOutCode
	 * @param string
	 */
	public void setKickOutCode(String string) {
		kickOutCode = string;
	}
	/**
	 * TODO method description getKickOutCode
	 * @return
	 */
	public String getKickOutCode() {
		return kickOutCode;
	}

	/**
	 * TODO method description getAgent
	 * @return
	 */
	public UserProfileData getAgent() {
		return agent;
	}

	/**
	 * TODO method description getAmountCovered
	 * @return
	 */
	public double getAmountCovered() {
		return amountCovered;
	}

	/**
	 * TODO method description getApplicationReceivedDate
	 * @return
	 */
	public Date getApplicationReceivedDate() {
		return applicationReceivedDate;
	}

	/**
	 * TODO method description getAssignedTo
	 * @return
	 */
	public UserProfileData getAssignedTo() {
		return assignedTo;
	}

	/**
	 * TODO method description getBranch
	 * @return
	 */
	public SunLifeOfficeData getBranch() {
		return branch;
	}

	/**
	 * TODO method description getClientDataSheet
	 * @return
	 */
	public ClientDataSheetData getClientDataSheet() {
		return clientDataSheet;
	}

	/**
	 * TODO method description getCreatedBy
	 * @return
	 */
	public UserProfileData getCreatedBy() {
		return createdBy;
	}

	/**
	 * TODO method description getCreatedDate
	 * @return
	 */
	public Date getCreatedDate() {
		return createdDate;
	}

	/**
	 * TODO method description getFolderLocation
	 * @return
	 */
	public UserProfileData getFolderLocation() {
		return folderLocation;
	}

	/**
	 * TODO method description getForwardedDate
	 * @return
	 */
	public Date getForwardedDate() {
		return forwardedDate;
	}

	/**
	 * TODO method description getInsured
	 * @return
	 */
	public ClientData getInsured() {
		return insured;
	}

	/**
	 * TODO method description getInsuredMortalityRating
	 * @return
	 */
	public CDSMortalityRatingData getInsuredMortalityRating() {
		return insuredMortalityRating;
	}

	/**
	 * TODO method description getLob
	 * @return
	 */
	public LOBData getLob() {
		return lob;
	}

	/**
	 * TODO method description getOwner
	 * @return
	 */
	public ClientData getOwner() {
		return owner;
	}

	/**
	 * TODO method description getOwnerMortalityRating
	 * @return
	 */
	public CDSMortalityRatingData getOwnerMortalityRating() {
		return ownerMortalityRating;
	}

	/**
	 * TODO method description getPremium
	 * @return
	 */
	public double getPremium() {
		return premium;
	}

	/**
	 * TODO method description getReferenceNumber
	 * @return
	 */
	public String getReferenceNumber() {
		return referenceNumber;
	}

	/**
	 * TODO method description getRemarks
	 * @return
	 */
	public String getRemarks() {
		return remarks;
	}

	/**
	 * TODO method description getSourceSystem
	 * @return
	 */
	public String getSourceSystem() {
		return sourceSystem;
	}

	/**
	 * TODO method description getStatus
	 * @return
	 */
	public StatusData getStatus() {
		return status;
	}

	/**
	 * TODO method description getStatusDate
	 * @return
	 */
	public Date getStatusDate() {
		return statusDate;
	}

	/**
	 * TODO method description getUnderwriter
	 * @return
	 */
	public UserProfileData getUnderwriter() {
		return underwriter;
	}

	/**
	 * TODO method description getUpdatedBy
	 * @return
	 */
	public UserProfileData getUpdatedBy() {
		return updatedBy;
	}

	/**
	 * TODO method description getUpdatedDate
	 * @return
	 */
	public Date getUpdatedDate() {
		return updatedDate;
	}

	/**
	 * @return
	 */
	public String getPolicySuffix() {
		return policySuffix;
	}

	/**
	 * TODO method description setAgent
	 * @param data
	 */
	public void setAgent(UserProfileData data) {
		agent = data;
	}

	/**
	 * TODO method description setAmountCovered
	 * @param d
	 */
	public void setAmountCovered(double d) {
		amountCovered = d;
	}

	/**
	 * TODO method description setApplicationReceivedDate
	 * @param date
	 */
	public void setApplicationReceivedDate(Date date) {
		applicationReceivedDate = date;
	}

	/**
	 * TODO method description setAssignedTo
	 * @param data
	 */
	public void setAssignedTo(UserProfileData data) {
		assignedTo = data;
	}

	/**
	 * TODO method description setBranch
	 * @param data
	 */
	public void setBranch(SunLifeOfficeData data) {
		branch = data;
	}

	/**
	 * TODO method description setClientDataSheet
	 * @param data
	 */
	public void setClientDataSheet(ClientDataSheetData data) {
		clientDataSheet = data;
	}

	/**
	 * TODO method description setCreatedBy
	 * @param data
	 */
	public void setCreatedBy(UserProfileData data) {
		createdBy = data;
	}

	/**
	 * TODO method description setCreatedDate
	 * @param date
	 */
	public void setCreatedDate(Date date) {
		createdDate = date;
	}

	/**
	 * TODO method description setFolderLocation
	 * @param data
	 */
	public void setFolderLocation(UserProfileData data) {
		folderLocation = data;
	}

	/**
	 * TODO method description setForwardedDate
	 * @param date
	 */
	public void setForwardedDate(Date date) {
		forwardedDate = date;
	}

	/**
	 * TODO method description setInsured
	 * @param data
	 */
	public void setInsured(ClientData data) {
		insured = data;
	}

	/**
	 * TODO method description setInsuredMortalityRating
	 * @param data
	 */
	public void setInsuredMortalityRating(CDSMortalityRatingData data) {
		insuredMortalityRating = data;
	}

	/**
	 * TODO method description setLob
	 * @param data
	 */
	public void setLob(LOBData data) {
		lob = data;
	}

	/**
	 * TODO method description setOwner
	 * @param data
	 */
	public void setOwner(ClientData data) {
		owner = data;
	}

	/**
	 * TODO method description setOwnerMortalityRating
	 * @param data
	 */
	public void setOwnerMortalityRating(CDSMortalityRatingData data) {
		ownerMortalityRating = data;
	}

	/**
	 * TODO method description setPremium
	 * @param d
	 */
	public void setPremium(double d) {
		premium = d;
	}

	/**
	 * TODO method description setReferenceNumber
	 * @param string
	 */
	public void setReferenceNumber(String string) {
		referenceNumber = string;
	}

	/**
	 * TODO method description setRemarks
	 * @param string
	 */
	public void setRemarks(String string) {
		remarks = string;
	}

	/**
	 * TODO method description setSourceSystem
	 * @param string
	 */
	public void setSourceSystem(String string) {
		sourceSystem = string;
	}

	/**
	 * TODO method description setStatus
	 * @param data
	 */
	public void setStatus(StatusData data) {
		status = data;
	}

	/**
	 * TODO method description setStatusDate
	 * @param date
	 */
	public void setStatusDate(Date date) {
		statusDate = date;
	}

	/**
	 * TODO method description setUnderwriter
	 * @param data
	 */
	public void setUnderwriter(UserProfileData data) {
		underwriter = data;
	}

	/**
	 * TODO method description setUpdatedBy
	 * @param data
	 */
	public void setUpdatedBy(UserProfileData data) {
		updatedBy = data;
	}

	/**
	 * TODO method description setUpdatedDate
	 * @param date
	 */
	public void setUpdatedDate(Date date) {
		updatedDate = date;
	}

	/**
	 * @return
	 */
	public ArrayList getKickOutMessages() {
		return kickOutMessages;
	}

	/**
	 * @param list
	 */
	public void setKickOutMessages(ArrayList list) {
		kickOutMessages = list;
	}

	/**
	 * @return
	 */
	public String getXmlRecord() {
		return xmlRecord;
	}

	/**
	 * @param string
	 */
	public void setXmlRecord(String string) {
		xmlRecord = string;
	}

	/**
	 * @return
	 */
	public String getTransmitIndicator() {
		return transmitIndicator;
	}

	/**
	 * @param string
	 */
	public void setTransmitIndicator(String string) {
		transmitIndicator = string;
	}

	/**
	 * @return
	 */
	public String getCurrentUser() {
		return currentUser;
	}

	/**
	 * @param string
	 */
	public void setCurrentUser(String string) {
		currentUser = string;
	}

	/**
	 * @return
	 */
	public String getAutoSettleIndicator() {
		return autoSettleIndicator;
	}

	/**
	 * @param string
	 */
	public void setAutoSettleIndicator(String string) {
		autoSettleIndicator = string;
	}

	/**
	 * @param string
	 */
	public void setPolicySuffix(String string) {
		policySuffix = string;
	}
	/**
	 * @return Returns the autoSettleError.
	 */
	public ExceptionDetailsData getAutoSettleError() {
		return autoSettleError;
	}
	/**
	 * @param autoSettleError The autoSettleError to set.
	 */
	public void setAutoSettleError(ExceptionDetailsData autoSettleError) {
		if (autoSettleError != null) {
			this.autoSettleError = autoSettleError;
		}
	}
	public String getKoCountInd() {
		return koCountInd;
	}
	public void setKoCountInd(String koCountInd) {
		this.koCountInd = koCountInd;
	}
	public String getReqtCountInd() {
		return reqtCountInd;
	}
	public void setReqtCountInd(String reqtCountInd) {
		this.reqtCountInd = reqtCountInd;
	}
}