package com.slocpi.ium.data;

import java.util.Date;

/**
 * Container for Examination Area reference
 * @author tvicencio 
 */
public class ExaminationAreaData {

	private long examinationAreaId;
	private String examinationAreaDesc;
	private String createdBy;
	private Date createDate;
	private String updatedBy;
	private Date updateDate;

	/**
	 * Sets the examination area id attribute.
	 * @param examinationAreaId examination area id
	 */  
	public void setExaminationAreaId(long examinationAreaId) {
		this.examinationAreaId = examinationAreaId;
	}
	
	/**
	 * Retrieves the examination area id attribute.
	 * @return long examination area id
	 */
	public long getExaminationAreaId() {
		return examinationAreaId;
	}
	
	/**
	 * Sets the examination area description attribute.
	 * @param examinationAreaDesc examination area description
	 */  
	public void setExaminationAreaDesc(String examinationAreaDesc) {
		this.examinationAreaDesc = examinationAreaDesc;
	}
		
	/**
	 * Retrieves the examination area description attribute.
	 * @return String examination area description
	 */
	public String getExaminationAreaDesc() {
		return examinationAreaDesc;
	}

	/**
	 * Sets the created date attribute.
	 * @param createDate date when the record was created
	 */ 
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	
	/**
	 * Retrieves the created date attribute.
	 * @return Date date when the record was created
	 */
	public Date getCreateDate() {
		return createDate;
	}

	/**
	 * Sets the created by attribute.
	 * @param createdBy user who created the record
	 */ 
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * Retrieves the created by attribute.
	 * @return String user who created the record
	 */
	public String getCreatedBy() {
		return createdBy;
	}

	/**
	 * Sets the updated date attribute.
	 * @param updateDate date when the record was last updated
	 */ 
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
	
	/**
	 * Retrieves the updated date attribute.
	 * @return Date date when the record was last updated
	 */
	public Date getUpdateDate() {
		return updateDate;
	}

	/**
	 * Sets the updated by attribute.
	 * @param updatedBy user who last updated the record
	 */ 
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	
	/**
	 * Retrieves the updated by attribute.
	 * @return String user who last updated the record
	 */
	public String getUpdatedBy() {
		return updatedBy;
	}	

}
