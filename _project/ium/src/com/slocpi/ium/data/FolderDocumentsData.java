/*
 * Created on Dec 22, 2003
 * package name = com.slocpi.ium.data
 * file name    = FolderDocumentsData.java
 */
package com.slocpi.ium.data;

import java.util.Date;

/**
 * Contains information pertaining to the physical document of an application.
 * @author Engel
 * 
 */
public class FolderDocumentsData {

	private long documentId;
	private String referenceNumber;
	private String documentCode;
	private Date receivedDate;
	private String receivedBy;
	private long documentReferenceNumber;
	/**
	 * TODO method description getDocumentCode
	 * @return
	 */
	public String getDocumentCode() {
		return documentCode;
	}

	/**
	 * TODO method description getDocumentId
	 * @return
	 */
	public long getDocumentId() {
		return documentId;
	}

	/**
	 * TODO method description getDocumentReferenceNumber
	 * @return
	 */
	public long getDocumentReferenceNumber() {
		return documentReferenceNumber;
	}

	/**
	 * TODO method description getReceivedBy
	 * @return
	 */
	public String getReceivedBy() {
		return receivedBy;
	}

	/**
	 * TODO method description getReceivedDate
	 * @return
	 */
	public Date getReceivedDate() {
		return receivedDate;
	}

	/**
	 * TODO method description getReferenceNumber
	 * @return
	 */
	public String getReferenceNumber() {
		return referenceNumber;
	}

	/**
	 * TODO method description setDocumentCode
	 * @param string
	 */
	public void setDocumentCode(String string) {
		documentCode = string;
	}

	/**
	 * TODO method description setDocumentId
	 * @param l
	 */
	public void setDocumentId(long l) {
		documentId = l;
	}

	/**
	 * TODO method description setDocumentReferenceNumber
	 * @param l
	 */
	public void setDocumentReferenceNumber(long l) {
		documentReferenceNumber = l;
	}

	/**
	 * TODO method description setReceivedBy
	 * @param string
	 */
	public void setReceivedBy(String string) {
		receivedBy = string;
	}

	/**
	 * TODO method description setReceivedDate
	 * @param date
	 */
	public void setReceivedDate(Date date) {
		receivedDate = date;
	}

	/**
	 * TODO method description setReferenceNumber
	 * @param string
	 */
	public void setReferenceNumber(String string) {
		referenceNumber = string;
	}

}
