/*
 * Created on Feb 12, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.data;

/**
 * @author Nic Decapia
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class RequirementFormFieldData
{
	private RequirementFormData rfd = new RequirementFormData();
	private String fieldName = "";
	private int fieldValueType = 0;
	private int fieldPage = 0;
	private float fieldPosX = 0f;
	private float fieldPosY = 0f;
	
	
		
	/**
	 * @return
	 */
	public String getFieldName()
	{
		return fieldName;
	}

	/**
	 * @return
	 */
	public int getFieldPage()
	{
		return fieldPage;
	}

	/**
	 * @return
	 */
	public float getFieldPosX()
	{
		return fieldPosX;
	}

	/**
	 * @return
	 */
	public float getFieldPosY()
	{
		return fieldPosY;
	}

	/**
	 * @return
	 */
	public RequirementFormData getRfd()
	{
		return rfd;
	}

	/**
	 * @param string
	 */
	public void setFieldName(String string)
	{
		fieldName = string;
	}

	/**
	 * @param i
	 */
	public void setFieldPage(int i)
	{
		fieldPage = i;
	}

	/**
	 * @param f
	 */
	public void setFieldPosX(float f)
	{
		fieldPosX = f;
	}

	/**
	 * @param f
	 */
	public void setFieldPosY(float f)
	{
		fieldPosY = f;
	}

	/**
	 * @param data
	 */
	public void setRfd(RequirementFormData data)
	{
		rfd = data;
	}

	/**
	 * @return
	 */
	public int getFieldValueType()
	{
		return fieldValueType;
	}

	/**
	 * @param i
	 */
	public void setFieldValueType(int i)
	{
		fieldValueType = i;
	}

}
