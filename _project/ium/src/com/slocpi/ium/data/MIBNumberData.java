package com.slocpi.ium.data;

import java.util.Date;

/**
 * Container for MIB Number reference information.
 * @author Engel 
 */
public class MIBNumberData {

	private String MIBNumberCode;
	private String MIBNumberDesc;
	private String createdBy;
	private Date createDate;
	private String updatedBy;
	private Date updateDate;
		
	
	/**
	 * Sets the number code attribute.
	 * @param MIBNumberCode code identifying the relationship of the client to the insured
	 */
	public void setMIBNumberCode(String MIBNumberCode) {
		this.MIBNumberCode = MIBNumberCode;
	}
		
	/**
	 * Retrieves the number code attribute.
	 * @return String code identifying the relationship of the client to the insured
	 */
	public String getMIBNumberCode() {
		return MIBNumberCode;
	}

	/**
	 * Sets the number description attribute.
	 * @param MIBnumberDesc description of the action code
	 */
	public void setMIBNumberDesc(String MIBNumberDesc) {
		this.MIBNumberDesc = MIBNumberDesc;
	}

	/**
	 * Retrieves the number code attribute.
	 * @return String description of the action code
	 */
	public String getMIBNumberDesc() {
		return MIBNumberDesc;
	}

	/**
	 * Sets the created date attribute.
	 * @param createDate date when the record was created
	 */ 
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	
	/**
	 * Retrieves the created date attribute.
	 * @return Date date when the record was created
	 */
	public Date getCreateDate() {
		return createDate;
	}

	/**
	 * Sets the created by attribute.
	 * @param createdBy user who created the record
	 */ 
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * Retrieves the created by attribute.
	 * @return String user who created the record
	 */
	public String getCreatedBy() {
		return createdBy;
	}

	/**
	 * Sets the updated date attribute.
	 * @param updateDate date when the record was last updated
	 */ 
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
	
	/**
	 * Retrieves the updated date attribute.
	 * @return Date date when the record was last updated
	 */
	public Date getUpdateDate() {
		return updateDate;
	}

	/**
	 * Sets the updated by attribute.
	 * @param updatedBy user who last updated the record
	 */ 
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	
	/**
	 * Retrieves the updated by attribute.
	 * @return String user who last updated the record
	 */
	public String getUpdatedBy() {
		return updatedBy;
	}

}
