/*
 * Created on Dec 22, 2003
 * package name = com.slocpi.ium.data
 * file name    = KickOutMessageData.java
 */
package com.slocpi.ium.data;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Contains information pertaining to the kick out message of an application.
 * @author Engel
 * 
 */
public class KickOutMessageData {

	static final Logger LOGGER = LoggerFactory.getLogger(KickOutMessageData.class);
	private long sequenceNumber;
	private String referenceNumber;
	private String messageText;
	private String clientId;
	private String failResponse;
	private String xmlRecord;
	
	public String toString(){
		return ToStringBuilder.reflectionToString(this, ToStringStyle.DEFAULT_STYLE);
	}
	
	
	//jonbred
	private String policyID; 
	
	public String getPolicyId() {
		return policyID;
		}
		
	public void setPolicyId(String string) {
		policyID = string;
		}	
		
		
	
	/**
	 * @return
	 */
	public String getClientId() {
		return clientId;
	}

	/**
	 * @return
	 */
	public String getFailResponse() {
		return failResponse;
	}

	/**
	 * @return
	 */
	public String getMessageText() {
		return messageText;
	}

	/**
	 * @return
	 */
	public String getReferenceNumber() {
		return referenceNumber;
	}

	/**
	 * @return
	 */
	public long getSequenceNumber() {
		return sequenceNumber;
	}

	/**
	 * @param string
	 */
	public void setClientId(String string) {
		clientId = string;
	}

	/**
	 * @param string
	 */
	public void setFailResponse(String string) {
		failResponse = string;
	}

	/**
	 * @param string
	 */
	public void setMessageText(String string) {
		messageText = string;
	}

	/**
	 * @param string
	 */
	public void setReferenceNumber(String string) {
		referenceNumber = string;
	}

	/**
	 * @param l
	 */
	public void setSequenceNumber(long seqNum) {
		sequenceNumber = seqNum;
	}

	/**
	 * @return
	 */
	public String getXmlRecord() {
		return xmlRecord;
	}

	/**
	 * @param string
	 */
	public void setXmlRecord(String string) {
		xmlRecord = string;
	}
	
	

}
