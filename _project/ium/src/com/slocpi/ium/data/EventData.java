/*
 * Created on Dec 23, 2003
 * package name = com.slocpi.ium.data
 * file name    = EventData.java
 */
package com.slocpi.ium.data;

/**
 * Cntainer for Event reference information.
 * @author Engel
 * 
 */
public class EventData {

	private long eventId;
	private String eventName;
	private String fromEvent;
	private String toEvent;

	/**
	 * @return
	 */
	public long getEventId() {
		return eventId;
	}

	/**
	 * @return
	 */
	public String getEventName() {
		return eventName;
	}

	/**
	 * @return
	 */
	public String getFromEvent() {
		return fromEvent;
	}

	/**
	 * @return
	 */
	public String getToEvent() {
		return toEvent;
	}

	/**
	 * @param l
	 */
	public void setEventId(long l) {
		eventId = l;
	}

	/**
	 * @param string
	 */
	public void setEventName(String string) {
		eventName = string;
	}

	/**
	 * @param string
	 */
	public void setFromEvent(String string) {
		fromEvent = string;
	}

	/**
	 * @param string
	 */
	public void setToEvent(String string) {
		toEvent = string;
	}

}
