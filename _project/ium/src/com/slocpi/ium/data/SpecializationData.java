package com.slocpi.ium.data;

import java.util.Date;

/**
 * Container for Specialization reference information.
 * @author Engel 
 */
public class SpecializationData {

	private long specializationId;
	private String specializationDesc;
	private String createdBy;
	private Date createDate;
	private String updatedBy;
	private Date updateDate;
	
	/**
	 * Sets the created date attribute.
	 * @param createDate date when the record was created
	 */ 
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	
	/**
	 * Retrieves the created date attribute.
	 * @return Date date when the record was created
	 */
	public Date getCreateDate() {
		return createDate;
	}

	/**
	 * Sets the created by attribute.
	 * @param createdBy user who created the record
	 */ 
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * Retrieves the created by attribute.
	 * @return String user who created the record
	 */
	public String getCreatedBy() {
		return createdBy;
	}

	/**
	 * Sets the specialization id attribute.
	 * @param specializationId section code
	 */ 
	public void setSpecializationId(long specializationId) {
		this.specializationId = specializationId;
	}

	/**
	 * Retrieves the specialization id attribute.
	 * @return long specialization id
	 */
	public long getSpecializationId() {
		return specializationId;
	}

	/**
	 * Sets the specialization description attribute.
	 * @param specializationDesc specialization description
	 */ 
	public void setSpecializationDesc(String specializationDesc) {
		this.specializationDesc = specializationDesc;
	}

	/**
	 * Retrieves the section description attribute.
	 * @return String section description
	 */
	public String getSpecializationDesc() {
		return specializationDesc;
	}
	
	/**
	 * Sets the updated date attribute.
	 * @param updateDate date when the record was last updated
	 */ 
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
	
	/**
	 * Retrieves the updated date attribute.
	 * @return Date date when the record was last updated
	 */
	public Date getUpdateDate() {
		return updateDate;
	}

	/**
	 * Sets the updated by attribute.
	 * @param updatedBy user who last updated the record
	 */ 
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	
	/**
	 * Retrieves the updated by attribute.
	 * @return String user who last updated the record
	 */
	public String getUpdatedBy() {
		return updatedBy;
	}

}
