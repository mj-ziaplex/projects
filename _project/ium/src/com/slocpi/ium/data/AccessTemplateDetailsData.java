/*
 * Created on Dec 23, 2003
 * package name = com.slocpi.ium.data
 * file name    = AccessTemplateDetailsData.java
 */
package com.slocpi.ium.data;

import java.sql.Timestamp;

/**
 * Container for Access Template Details reference information.
 * @author Engel
 * 
 */
public class AccessTemplateDetailsData {

	private long templateID;
	private PageFunctionData pageAccess;
	private boolean access;
	private String updatedBy;
	private Timestamp updatedDate;
	
	/**
	 * Retrieves the page access attribute. 
	 * @return
	 */
	public PageFunctionData getPageAccess() {
		return pageAccess;
	}

	/**
	 * Retrieves the template ID attribute. 
	 * @return
	 */
	public long getTemplateID() {
		return templateID;
	}

	/**
	 * Sets the page access attribute. 
	 * @param data
	 */
	public void setPageAccess(PageFunctionData data) {
		pageAccess = data;
	}

	/**
	 * Sets the tmeplate ID attribute. 
	 * @param l
	 */
	public void setTemplateID(long l) {
		templateID = l;
	}

	/**
	 * Retrieves the access attribute. 
	 * @return
	 */
	public boolean hasAccess() {
		return access;
	}

	/**
	 * Sets the access attribute. 
	 * @param b
	 */
	public void setAccess(boolean b) {
		access = b;
	}

	/**
	 * Retrieves the updated by attribute. 
	 * @return
	 */
	public String getUpdatedBy() {
		return updatedBy;
	}

	/**
	 * Retrieves the updated date attribute. 
	 * @return
	 */
	public Timestamp getUpdatedDate() {
		return updatedDate;
	}

	/**
	 * Sets the updated by attribute. 
	 * @param string
	 */
	public void setUpdatedBy(String string) {
		updatedBy = string;
	}

	/**
	 * Sets the updated date attribute. 
	 * @param timestamp
	 */
	public void setUpdatedDate(Timestamp timestamp) {
		updatedDate = timestamp;
	}

}
