package com.slocpi.ium.data;

import java.util.Date;

/**
 * Container for Examination Place reference
 * @author Engel 
 */
public class ExaminationPlaceData {

	private long examinationPlaceId;
	private String examinationPlaceDesc;
	private String createdBy;
	private Date createDate;
	private String updatedBy;
	private Date updateDate;

	/**
	 * Sets the examination place id attribute.
	 * @param examinationPlaceId examination place description
	 */ 
	public void setExaminationPlaceId(long examinationPlaceId) {
		this.examinationPlaceId = examinationPlaceId;
	}

	/**
	 * Retrieves the examination place id attribute.
	 * @return long examination place id
	 */
	public long getExaminationPlaceId() {
		return examinationPlaceId;
	}

	/**
	 * Sets the examination place description attribute.
	 * @param examinationPlaceDesc examination place description
	 */ 
	public void setExaminationPlaceDesc(String examinationPlaceDesc) {
		this.examinationPlaceDesc = examinationPlaceDesc;
	}
	
	/**
	 * Retrieves the examination place description attribute.
	 * @return String examination place description
	 */
	public String getExaminationPlaceDesc() {
		return examinationPlaceDesc;
	}


	/**
	 * Sets the created date attribute.
	 * @param createDate date when the record was created
	 */ 
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	
	/**
	 * Retrieves the created date attribute.
	 * @return Date date when the record was created
	 */
	public Date getCreateDate() {
		return createDate;
	}

	/**
	 * Sets the created by attribute.
	 * @param createdBy user who created the record
	 */ 
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * Retrieves the created by attribute.
	 * @return String user who created the record
	 */
	public String getCreatedBy() {
		return createdBy;
	}

	/**
	 * Sets the updated date attribute.
	 * @param updateDate date when the record was last updated
	 */ 
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
	
	/**
	 * Retrieves the updated date attribute.
	 * @return Date date when the record was last updated
	 */
	public Date getUpdateDate() {
		return updateDate;
	}

	/**
	 * Sets the updated by attribute.
	 * @param updatedBy user who last updated the record
	 */ 
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	
	/**
	 * Retrieves the updated by attribute.
	 * @return String user who last updated the record
	 */
	public String getUpdatedBy() {
		return updatedBy;
	}	
}
