/*
 * Created on Dec 22, 2003
 * package name = com.slocpi.ium.data
 * file name    = ExaminationSpecializationData.java
 */
package com.slocpi.ium.data;

import java.util.Date;

/**
 * Container for Examination Specialization reference information.
 * @author Engel
 * 
 */
public class ExaminationSpecializationData {

	private long examinerId;
	private long specializationId;
	private Date effectivityDate;
	
	/**
	 * TODO method description getEffectivityDate
	 * @return
	 */
	public Date getEffectivityDate() {
		return effectivityDate;
	}

	/**
	 * TODO method description getExaminerId
	 * @return
	 */
	public long getExaminerId() {
		return examinerId;
	}

	/**
	 * TODO method description getSpecializationId
	 * @return
	 */
	public long getSpecializationId() {
		return specializationId;
	}

	/**
	 * TODO method description setEffectivityDate
	 * @param date
	 */
	public void setEffectivityDate(Date date) {
		effectivityDate = date;
	}

	/**
	 * TODO method description setExaminerId
	 * @param l
	 */
	public void setExaminerId(long l) {
		examinerId = l;
	}

	/**
	 * TODO method description setSpecializationId
	 * @param l
	 */
	public void setSpecializationId(long l) {
		specializationId = l;
	}

}
