/*
 * Created on Dec 22, 2003
 * package name = com.slocpi.ium.data
 * file name    = NotificationMessageData.java
 */
package com.slocpi.ium.data;

import java.util.ArrayList;

/**
 * Container for messages sent by the system.
 * @author Engel
 * 
 */
public class NotificationMessageData {

	private long notificationTemplateId;
	private ArrayList attachments;
	/**
	 * TODO method description getAttachments
	 * @return
	 */
	public ArrayList getAttachments() {
		return attachments;
	}

	/**
	 * TODO method description getNotificationTemplateId
	 * @return
	 */
	public long getNotificationTemplateId() {
		return notificationTemplateId;
	}

	/**
	 * TODO method description setAttachments
	 * @param list
	 */
	public void setAttachments(ArrayList list) {
		attachments = list;
	}

	/**
	 * TODO method description setNotificationTemplateId
	 * @param l
	 */
	public void setNotificationTemplateId(long l) {
		notificationTemplateId = l;
	}

}
