package com.slocpi.ium.data;

import java.util.Date;

/**
 * Container for status reference information.
 * @author Engel
 */
public class StatusData {
	
	private long statusId;
	private String statusDesc;
	private String statusType;
	private String createdBy;
	private Date createDate;
	private String updatedBy;
	private Date updateDate;
	private long oldStatusId;

	/**
	 * Sets the status id attribute.
	 * @param statusId status id
	 */  
	public void setStatusId(long statusId) {
		this.statusId = statusId;
	}
	
	/**
	 * Retrieves the status id attribute.
	 * @return String status id
	 */
	public long getStatusId() {
		return new Long (statusId).longValue();
	}

	/**
	 * Sets the status description attribute.
	 * @param statusDesc status description
	 */  
	public void setStatusDesc(String statusDesc) {
		this.statusDesc = statusDesc;
	}

	/**
	 * Retrieves the status description attribute.
	 * @return String status description
	 */
	public String getStatusDesc() {
		return statusDesc;
	}

	/**
	 * Sets the status type attribute.
	 * @param statusType status type
	 */  
	public void setStatusType(String statusType) {
		this.statusType = statusType;
	}

	/**
	 * Retrieves the status type attribute.
	 * @return String status type
	 */
	public String getStatusType() {
		return statusType;
	}

	/**
	 * Sets the created date attribute.
	 * @param createDate date when the record was created
	 */ 
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	
	/**
	 * Retrieves the created date attribute.
	 * @return Date date when the record was created
	 */
	public Date getCreateDate() {
		return createDate;
	}

	/**
	 * Sets the created by attribute.
	 * @param createdBy user who created the record
	 */ 
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * Retrieves the created by attribute.
	 * @return String user who created the record
	 */
	public String getCreatedBy() {
		return createdBy;
	}

	/**
	 * Sets the updated date attribute.
	 * @param updateDate date when the record was last updated
	 */ 
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
	
	/**
	 * Retrieves the updated date attribute.
	 * @return Date date when the record was last updated
	 */
	public Date getUpdateDate() {
		return updateDate;
	}

	/**
	 * Sets the updated by attribute.
	 * @param updatedBy user who last updated the record
	 */ 
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	
	/**
	 * Retrieves the updated by attribute.
	 * @return String user who last updated the record
	 */
	public String getUpdatedBy() {
		return updatedBy;
	}

	public long getOldStatusId() {
		return oldStatusId;
	}

	public void setOldStatusId(long oldStatusId) {
		this.oldStatusId = oldStatusId;
	}	
}
