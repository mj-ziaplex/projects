/**
 * MedicalExamBillingFilter.java				Copyright (c) 2004 Pointwest Technologies Corp.
 * TODO DOCUMENT ME!
 * 
 * @author aditalo	 @date Mar 12, 2004
 */
package com.slocpi.ium.data;

/**
 * TODO DOCUMENT ME!
 * 
 * @author aditalo		Copyright (c) 2004 Pointwest Technologies Corp.
 * @date Mar 12, 2004
 */
public class MedicalExamBillingFilter {

	// moved from IUMConstants
	public final static int REPORT_TYPE_EXAMINER	= 1;
	public final static int REPORT_TYPE_LABORATORY	= 2;
	public final static int REPORT_TYPE_AGENT		= 3;

	private String startDate;
	private String endDate;
	private int reportType;
	private ExaminerData examiner = null;
	private LaboratoryData laboratory = null;
	private UserProfileData agent = null;
	/**
	 * @return
	 */
	public UserProfileData getAgent() {
		return agent;
	}

	/**
	 * @return
	 */
	public String getEndDate() {
		return endDate;
	}

	/**
	 * @return
	 */
	public ExaminerData getExaminer() {
		return examiner;
	}

	/**
	 * @return
	 */
	public LaboratoryData getLaboratory() {
		return laboratory;
	}

	/**
	 * @return
	 */
	public int getReportType() {
		return reportType;
	}

	/**
	 * @return
	 */
	public String getStartDate() {
		return startDate;
	}

	/**
	 * @param data
	 */
	public void setAgent(UserProfileData data) {
		agent = data;
	}

	/**
	 * @param string
	 */
	public void setEndDate(String string) {
		endDate = string;
	}

	/**
	 * @param data
	 */
	public void setExaminer(ExaminerData data) {
		examiner = data;
	}

	/**
	 * @param data
	 */
	public void setLaboratory(LaboratoryData data) {
		laboratory = data;
	}

	/**
	 * @param i
	 */
	public void setReportType(int i) {
		reportType = i;
	}

	/**
	 * @param string
	 */
	public void setStartDate(String string) {
		startDate = string;
	}

}
