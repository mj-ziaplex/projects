/*
 * Created on Dec 18, 2003
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.data;

import java.util.Date;

/**
 * @author Engel
 *
 * @TODO class description of PolicyDetailData
 */
public class PolicyDetailData {
	
	private String 	referenceNumber;
	private String 	clientId;
	private String 	policyNumber;
	private long	coverageNumber;
	private String	relationship;
	private Date	issueDate;
	private String	coverageStatus;
	private boolean smoker;
	private String 	planCode;
	private String planDescription;
	private boolean medical;
	private double	faceAmount;
	private String	decision;
	private double	ADBFaceAmount;
	private double		ADMultiplier;
	private double		WPMultiplier;
	private double	reinsuredAmount;
	private String xmlRecord;
	private String policySuffix;
	private double relCvgMeFct;
	private double relCvgFeUpremAmt;
	private double relCvgFePermAmt;
	private double adbReinsuredAmount;
	private Date reinstatementDate;
	private String reinsuranceType;

	public double getAdbReinsuredAmount() {
		return adbReinsuredAmount;
	}

	public void setAdbReinsuredAmount(double adbReinsuredAmount) {
		this.adbReinsuredAmount = adbReinsuredAmount;
	}

	public Date getReinstatementDate() {
		return reinstatementDate;
	}

	public void setReinstatementDate(Date reinstatementDate) {
		this.reinstatementDate = reinstatementDate;
	}

	public String getReinsuranceType() {
		return reinsuranceType;
	}

	public void setReinsuranceType(String reinsuranceType) {
		this.reinsuranceType = reinsuranceType;
	}

	public double getRelCvgFePermAmt() {
		return relCvgFePermAmt;
	}

	public void setRelCvgFePermAmt(double relCvgFePermAmt) {
		this.relCvgFePermAmt = relCvgFePermAmt;
	}

	public double getRelCvgFeUpremAmt() {
		return relCvgFeUpremAmt;
	}

	public void setRelCvgFeUpremAmt(double relCvgFeUpremAmt) {
		this.relCvgFeUpremAmt = relCvgFeUpremAmt;
	}

	public double getRelCvgMeFct() {
		return relCvgMeFct;
	}

	public void setRelCvgMeFct(double relCvgMeFct) {
		this.relCvgMeFct = relCvgMeFct;
	}

	/**
	 * @return
	 */
	public double getADBFaceAmount() {
		return ADBFaceAmount;
	}

	/**
	 * @return
	 */
	public double getADMultiplier() {
		return ADMultiplier;
	}

	/**
	 * @return
	 */
	public String getClientId() {
		return clientId;
	}

	/**
	 * @return
	 */
	public long getCoverageNumber() {
		return coverageNumber;
	}

	/**
	 * @return
	 */
	public String getCoverageStatus() {
		return coverageStatus;
	}

	/**
	 * @return
	 */
	public String getDecision() {
		return decision;
	}

	/**
	 * @return
	 */
	public double getFaceAmount() {
		return faceAmount;
	}

	/**
	 * @return
	 */
	public Date getIssueDate() {
		return issueDate;
	}

	/**
	 * @return
	 */
	public boolean isMedical() {
		return medical;
	}

	/**
	 * @return
	 */
	public String getPlanCode() {
		return planCode;
	}

	/**
	 * @return
	 */
	public String getPolicyNumber() {
		return policyNumber;
	}

	/**
	 * @return
	 */
	public String getReferenceNumber() {
		return referenceNumber;
	}

	/**
	 * @return
	 */
	public double getReinsuredAmount() {
		return reinsuredAmount;
	}

	/**
	 * @return
	 */
	public String getRelationship() {
		return relationship;
	}

	/**
	 * @return
	 */
	public boolean isSmoker() {
		return smoker;
	}

	/**
	 * @return
	 */
	public double getWPMultiplier() {
		return WPMultiplier;
	}

	/**
	 * @param d
	 */
	public void setADBFaceAmount(double d) {
		ADBFaceAmount = d;
	}

	/**
	 * @param i
	 */
	public void setADMultiplier(double i) {
		ADMultiplier = i;
	}

	/**
	 * @param string
	 */
	public void setClientId(String string) {
		clientId = string;
	}

	/**
	 * @param l
	 */
	public void setCoverageNumber(long l) {
		coverageNumber = l;
	}

	/**
	 * @param string
	 */
	public void setCoverageStatus(String string) {
		coverageStatus = string;
	}

	/**
	 * @param string
	 */
	public void setDecision(String string) {
		decision = string;
	}

	/**
	 * @param d
	 */
	public void setFaceAmount(double d) {
		faceAmount = d;
	}

	/**
	 * @param date
	 */
	public void setIssueDate(Date date) {
		issueDate = date;
	}

	/**
	 * @param b
	 */
	public void setMedical(boolean b) {
		medical = b;
	}

	/**
	 * @param string
	 */
	public void setPlanCode(String string) {
		planCode = string;
	}

	/**
	 * @param string
	 */
	public void setPolicyNumber(String string) {
		policyNumber = string;
	}

	/**
	 * @param string
	 */
	public void setReferenceNumber(String string) {
		referenceNumber = string;
	}

	/**
	 * @param d
	 */
	public void setReinsuredAmount(double d) {
		reinsuredAmount = d;
	}

	/**
	 * @param string
	 */
	public void setRelationship(String string) {
		relationship = string;
	}

	/**
	 * @param b
	 */
	public void setSmoker(boolean b) {
		smoker = b;
	}

	/**
	 * @param i
	 */
	public void setWPMultiplier(double i) {
		WPMultiplier = i;
	}

	/**
	 * @return
	 */
	public String getXmlRecord() {
		return xmlRecord;
	}

	/**
	 * @param string
	 */
	public void setXmlRecord(String string) {
		xmlRecord = string;
	}

	public String getPolicySuffix() {
		return policySuffix;
	}

	public void setPolicySuffix(String policySuffix) {
		this.policySuffix = policySuffix;
	}

	public String getPlanDescription() {
		return planDescription;
	}

	public void setPlanDescription(String planDescription) {
		this.planDescription = planDescription;
	}

}
