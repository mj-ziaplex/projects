/*
 * Created on Dec 23, 2003
 * package name = com.slocpi.ium.data
 * file name    = LaboratoryData.java
 */
package com.slocpi.ium.data;

import java.util.Date;

/**
 * Container for Laboratory Test reference information.
 * @author Nic
 * 
 */
public class LaboratoryData {

  private long labId;
  private String labName;
  private String contactPerson;
  private String officeNum;
  private String officeNum2;
  private String faxNum;
  private String businessAddrLine1;
  private String businessAddrLine2;
  private String businessAddrLine3;
  private String city;
  private String province;
  private String country;
  private String zipCode;
  private String branchName;
  private String branchAddrLine1;
  private String branchAddrLine2;
  private String branchAddrLine3;
  private String branchCity;
  private String branchProvince;
  private String branchCountry;
  private String branchZipCode;
  private String branchPhone;
  private String branchFax_num;
  private String accreditInd;
  private Date accreditDate;
  private UserProfileData createdBy;
  private Date createdDate;
  private UserProfileData updatedBy;
  private Date updatedDate;
  private String emailAddress;
  
/**
 * @return
 */
public Date getAccreditDate()
{
	return accreditDate;
}

/**
 * @return
 */
public String getAccreditInd()
{
	return accreditInd;
}

/**
 * @return
 */
public String getBranchAddrLine1()
{
	return branchAddrLine1;
}

/**
 * @return
 */
public String getBranchAddrLine2()
{
	return branchAddrLine2;
}

/**
 * @return
 */
public String getBranchAddrLine3()
{
	return branchAddrLine3;
}

/**
 * @return
 */
public String getBranchCity()
{
	return branchCity;
}

/**
 * @return
 */
public String getBranchCountry()
{
	return branchCountry;
}

/**
 * @return
 */
public String getBranchFax_num()
{
	return branchFax_num;
}

/**
 * @return
 */
public String getBranchName()
{
	return branchName;
}

/**
 * @return
 */
public String getBranchPhone()
{
	return branchPhone;
}

/**
 * @return
 */
public String getBranchProvince()
{
	return branchProvince;
}

/**
 * @return
 */
public String getBranchZipCode()
{
	return branchZipCode;
}

/**
 * @return
 */
public String getBusinessAddrLine1()
{
	return businessAddrLine1;
}

/**
 * @return
 */
public String getBusinessAddrLine2()
{
	return businessAddrLine2;
}

/**
 * @return
 */
public String getBusinessAddrLine3()
{
	return businessAddrLine3;
}

/**
 * @return
 */
public String getCity()
{
	return city;
}

/**
 * @return
 */
public String getContactPerson()
{
	return contactPerson;
}

/**
 * @return
 */
public String getCountry()
{
	return country;
}

/**
 * @return
 */
public UserProfileData getCreatedBy()
{
	return createdBy;
}

/**
 * @return
 */
public Date getCreatedDate()
{
	return createdDate;
}

/**
 * @return
 */
public String getFaxNum()
{
	return faxNum;
}

/**
 * @return
 */
public long getLabId()
{
	return labId;
}

/**
 * @return
 */
public String getLabName()
{
	return labName;
}

/**
 * @return
 */
public String getOfficeNum()
{
	return officeNum;
}

/**
 * @return
 */
public String getOfficeNum2()
{
	return officeNum2;
}

/**
 * @return
 */
public String getProvince()
{
	return province;
}

/**
 * @return
 */
public UserProfileData getUpdatedBy()
{
	return updatedBy;
}

/**
 * @return
 */
public Date getUpdatedDate()
{
	return updatedDate;
}

/**
 * @return
 */
public String getZipCode()
{
	return zipCode;
}

/**
 * @param date
 */
public void setAccreditDate(Date date)
{
	accreditDate = date;
}

/**
 * @param string
 */
public void setAccreditInd(String string)
{
	accreditInd = string;
}

/**
 * @param string
 */
public void setBranchAddrLine1(String string)
{
	branchAddrLine1 = string;
}

/**
 * @param string
 */
public void setBranchAddrLine2(String string)
{
	branchAddrLine2 = string;
}

/**
 * @param string
 */
public void setBranchAddrLine3(String string)
{
	branchAddrLine3 = string;
}

/**
 * @param string
 */
public void setBranchCity(String string)
{
	branchCity = string;
}

/**
 * @param string
 */
public void setBranchCountry(String string)
{
	branchCountry = string;
}

/**
 * @param string
 */
public void setBranchFax_num(String string)
{
	branchFax_num = string;
}

/**
 * @param string
 */
public void setBranchName(String string)
{
	branchName = string;
}

/**
 * @param string
 */
public void setBranchPhone(String string)
{
	branchPhone = string;
}

/**
 * @param string
 */
public void setBranchProvince(String string)
{
	branchProvince = string;
}

/**
 * @param l
 */
public void setBranchZipCode(String l)
{
	branchZipCode = l;
}

/**
 * @param string
 */
public void setBusinessAddrLine1(String string)
{
	businessAddrLine1 = string;
}

/**
 * @param string
 */
public void setBusinessAddrLine2(String string)
{
	businessAddrLine2 = string;
}

/**
 * @param string
 */
public void setBusinessAddrLine3(String string)
{
	businessAddrLine3 = string;
}

/**
 * @param string
 */
public void setCity(String string)
{
	city = string;
}

/**
 * @param string
 */
public void setContactPerson(String string)
{
	contactPerson = string;
}

/**
 * @param string
 */
public void setCountry(String string)
{
	country = string;
}

/**
 * @param data
 */
public void setCreatedBy(UserProfileData data)
{
	createdBy = data;
}

/**
 * @param date
 */
public void setCreatedDate(Date date)
{
	createdDate = date;
}

/**
 * @param string
 */
public void setFaxNum(String string)
{
	faxNum = string;
}

/**
 * @param l
 */
public void setLabId(long l)
{
	labId = l;
}

/**
 * @param string
 */
public void setLabName(String string)
{
	labName = string;
}

/**
 * @param string
 */
public void setOfficeNum(String string)
{
	officeNum = string;
}

/**
 * @param string
 */
public void setOfficeNum2(String string)
{
	officeNum2 = string;
}

/**
 * @param string
 */
public void setProvince(String string)
{
	province = string;
}

/**
 * @param data
 */
public void setUpdatedBy(UserProfileData data)
{
	updatedBy = data;
}

/**
 * @param date
 */
public void setUpdatedDate(Date date)
{
	updatedDate = date;
}

/**
 * @param string
 */
public void setZipCode(String string)
{
	zipCode = string;
}

/**
 * Returns the emailAddress.
 * @return String
 */
public String getEmailAddress() {
	return emailAddress;
}

/**
 * Sets the emailAddress.
 * @param emailAddress The emailAddress to set
 */
public void setEmailAddress(String emailAddress) {
	this.emailAddress = emailAddress;
}

}
