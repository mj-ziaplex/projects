/*
 * Created on Dec 22, 2003
 * package name = com.slocpi.ium.data
 * file name    = CDSMortalityRating.java
 */
package com.slocpi.ium.data;

/**
 * Contains information pertaining to client mortality rating.
 * @author Engel
 * 
 */
public class CDSMortalityRatingData {

	private String referenceNumber;
	private long heightInFeet;
	private long heightInInches;
	private double weight;
	private double basic;
	private double CCR;
	private double APDB;
	private double smokingBasic;
	private double smokingCCR;
	private double smokingAPDB;
	private double familyHistoryBasic;
	private double familyHistoryCCR;
	private double familyHistoryAPDB;
	private double totalAssestMortBasic;
	private double totalAssestMortCCR;
	private double totalAssestMortAPDB;
	private long sequenceNumber;
	private long CDSSequenceNumber;
	
	/**
	 * TODO method description getAPDB
	 * @return
	 */
	public double getAPDB() {
		return APDB;
	}

	/**
	 * TODO method description getBasic
	 * @return
	 */
	public double getBasic() {
		return basic;
	}

	/**
	 * TODO method description getCCR
	 * @return
	 */
	public double getCCR() {
		return CCR;
	}

	/**
	 * TODO method description getCDSSequenceNumber
	 * @return
	 */
	public long getCDSSequenceNumber() {
		return CDSSequenceNumber;
	}

	/**
	 * TODO method description getFamilyHistoryAPDB
	 * @return
	 */
	public double getFamilyHistoryAPDB() {
		return familyHistoryAPDB;
	}

	/**
	 * TODO method description getFamilyHistoryBasic
	 * @return
	 */
	public double getFamilyHistoryBasic() {
		return familyHistoryBasic;
	}

	/**
	 * TODO method description getFamilyHistoryCCR
	 * @return
	 */
	public double getFamilyHistoryCCR() {
		return familyHistoryCCR;
	}

	/**
	 * TODO method description getHeightInFeet
	 * @return
	 */
	public long getHeightInFeet() {
		return heightInFeet;
	}

	/**
	 * TODO method description getHeightInInches
	 * @return
	 */
	public long getHeightInInches() {
		return heightInInches;
	}

	/**
	 * TODO method description getSequenceNumber
	 * @return
	 */
	public long getSequenceNumber() {
		return sequenceNumber;
	}

	/**
	 * TODO method description getSmokingBasic
	 * @return
	 */
	public double getSmokingBasic() {
		return smokingBasic;
	}

	/**
	 * TODO method description getSmokingCCR
	 * @return
	 */
	public double getSmokingCCR() {
		return smokingCCR;
	}

	/**
	 * TODO method description getTotalAssestMortBasic
	 * @return
	 */
	public double getTotalAssestMortBasic() {
		return totalAssestMortBasic;
	}

	/**
	 * TODO method description getTotalAssetMortAPDB
	 * @return
	 */
	public double getTotalAssestMortAPDB() {
		return totalAssestMortAPDB;
	}

	/**
	 * TODO method description getTotalAssetMortCCR
	 * @return
	 */
	public double getTotalAssestMortCCR() {
		return totalAssestMortCCR;
	}

	/**
	 * TODO method description getWeight
	 * @return
	 */
	public double getWeight() {
		return weight;
	}

	/**
	 * TODO method description setAPDB
	 * @param d
	 */
	public void setAPDB(double d) {
		APDB = d;
	}

	/**
	 * TODO method description setBasic
	 * @param d
	 */
	public void setBasic(double d) {
		basic = d;
	}

	/**
	 * TODO method description setCCR
	 * @param d
	 */
	public void setCCR(double d) {
		CCR = d;
	}

	/**
	 * TODO method description setCDSSequenceNumber
	 * @param l
	 */
	public void setCDSSequenceNumber(long l) {
		CDSSequenceNumber = l;
	}

	/**
	 * TODO method description setFamilyHistoryAPDB
	 * @param d
	 */
	public void setFamilyHistoryAPDB(double d) {
		familyHistoryAPDB = d;
	}

	/**
	 * TODO method description setFamilyHistoryBasic
	 * @param d
	 */
	public void setFamilyHistoryBasic(double d) {
		familyHistoryBasic = d;
	}

	/**
	 * TODO method description setFamilyHistoryCCR
	 * @param d
	 */
	public void setFamilyHistoryCCR(double d) {
		familyHistoryCCR = d;
	}

	/**
	 * TODO method description setHeightInFeet
	 * @param l
	 */
	public void setHeightInFeet(long l) {
		heightInFeet = l;
	}

	/**
	 * TODO method description setHeightInInches
	 * @param l
	 */
	public void setHeightInInches(long l) {
		heightInInches = l;
	}

	/**
	 * TODO method description setSequenceNumber
	 * @param l
	 */
	public void setSequenceNumber(long l) {
		sequenceNumber = l;
	}

	/**
	 * TODO method description setSmokingBasic
	 * @param d
	 */
	public void setSmokingBasic(double d) {
		smokingBasic = d;
	}

	/**
	 * TODO method description setSmokingCCR
	 * @param d
	 */
	public void setSmokingCCR(double d) {
		smokingCCR = d;
	}

	/**
	 * TODO method description setTotalAssestMortBasic
	 * @param d
	 */
	public void setTotalAssestMortBasic(double d) {
		totalAssestMortBasic = d;
	}

	/**
	 * TODO method description setTotalAssetMortAPDB
	 * @param d
	 */
	public void setTotalAssestMortAPDB(double d) {
		totalAssestMortAPDB = d;
	}

	/**
	 * TODO method description setTotalAssetMortCCR
	 * @param d
	 */
	public void setTotalAssestMortCCR(double d) {
		totalAssestMortCCR = d;
	}

	/**
	 * TODO method description setWeight
	 * @param d
	 */
	public void setWeight(double d) {
		weight = d;
	}

	/**
	 * @return
	 */
	public double getSmokingAPDB() {
		return smokingAPDB;
	}

	/**
	 * @param d
	 */
	public void setSmokingAPDB(double d) {
		smokingAPDB = d;
	}

	/**
	 * @return
	 */
	public String getReferenceNumber() {
		return referenceNumber;
	}

	/**
	 * @param string
	 */
	public void setReferenceNumber(String string) {
		referenceNumber = string;
	}

}
