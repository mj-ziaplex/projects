/*
 * Created on Dec 22, 2003
 * package name = com.slocpi.ium.data
 * file name    = DocumentTypeData.java
 */
package com.slocpi.ium.data;

import java.util.Date;


/**
 * Container for Document Type reference information.
 * @author Engel
 * 
 */
public class DocumentData {

	private long docId;
	private long docRefNo;
	private String refNo;
	private DocumentTypeData document;
	private Date dateReceived;
	private String receivedBy;
	private Date dateRequested;
	


	/**
	 * @return
	 */
	public Date getDateReceived() {
		return dateReceived;
	}

	/**
	 * @return
	 */
	public long getDocId() {
		return docId;
	}

	/**
	 * @return
	 */
	public long getDocRefNo() {
		return docRefNo;
	}

	/**
	 * @return
	 */
	public DocumentTypeData getDocument() {
		return document;
	}

	/**
	 * @return
	 */
	public String getReceivedBy() {
		return receivedBy;
	}

	/**
	 * @return
	 */
	public String getRefNo() {
		return refNo;
	}

	/**
	 * @param date
	 */
	public void setDateReceived(Date date) {
		dateReceived = date;
	}

	/**
	 * @param l
	 */
	public void setDocId(long l) {
		docId = l;
	}

	/**
	 * @param l
	 */
	public void setDocRefNo(long l) {
		docRefNo = l;
	}

	/**
	 * @param data
	 */
	public void setDocument(DocumentTypeData data) {
		document = data;
	}

	/**
	 * @param string
	 */
	public void setReceivedBy(String string) {
		receivedBy = string;
	}

	/**
	 * @param string
	 */
	public void setRefNo(String string) {
		refNo = string;
	}

	/**
	 * @return
	 */
	public Date getDateRequested() {
		return dateRequested;
	}

	/**
	 * @param date
	 */
	public void setDateRequested(Date date) {
		dateRequested = date;
	}

}
