/*
 * Created on Jan 21, 2004
 * package name = com.slocpi.ium.data
 * file name    = UserLoadData.java
 */
package com.slocpi.ium.data;

import java.util.HashMap;

/**
 * Container class for user Load.
 * @author Engel
 * 
 */
public class UserLoadData {
	
	private String user_id;
	private String first_name;
	private String last_name;
	private String middle_name;
	private HashMap loadByStatus;
	private long totalLoad;
	

	/**
	 * TODO method description getFirst_name
	 * @return
	 */
	public String getFirst_name() {
		return first_name;
	}

	/**
	 * TODO method description getLast_name
	 * @return
	 */
	public String getLast_name() {
		return last_name;
	}

	/**
	 * TODO method description getLoadByStatus
	 * @return
	 */
	public HashMap getLoadByStatus() {
		return loadByStatus;
	}

	/**
	 * TODO method description getMiddle_name
	 * @return
	 */
	public String getMiddle_name() {
		return middle_name;
	}

	/**
	 * TODO method description getTotalLoad
	 * @return
	 */
	public long getTotalLoad() {
		return totalLoad;
	}

	/**
	 * TODO method description getUser_id
	 * @return
	 */
	public String getUser_id() {
		return user_id;
	}

	/**
	 * TODO method description setFirst_name
	 * @param string
	 */
	public void setFirst_name(String string) {
		first_name = string;
	}

	/**
	 * TODO method description setLast_name
	 * @param string
	 */
	public void setLast_name(String string) {
		last_name = string;
	}

	/**
	 * TODO method description setLoadByStatus
	 * @param map
	 */
	public void setLoadByStatus(HashMap map) {
		loadByStatus = map;
	}

	/**
	 * TODO method description setMiddle_name
	 * @param string
	 */
	public void setMiddle_name(String string) {
		middle_name = string;
	}

	/**
	 * TODO method description setTotalLoad
	 * @param i
	 */
	public void setTotalLoad(long i) {
		totalLoad = i;
	}

	/**
	 * TODO method description setUser_id
	 * @param string
	 */
	public void setUser_id(String string) {
		user_id = string;
	}

}
