/**
 * ListLaboratoriesTestCase.java				Copyright (c) 2004 Pointwest Technologies Corp.
 * TODO DOCUMENT ME!
 * 
 * @author aditalo	 @date Jan 9, 2004
 */
package com.slocpi.ium.data.test;

import java.sql.Connection;

import com.slocpi.ium.data.LaboratoryData;
import com.slocpi.ium.data.dao.LaboratoryDAO;
import com.slocpi.ium.data.util.DataSourceProxy;

/**
 * TODO DOCUMENT ME!
 * 
 * @author aditalo		Copyright (c) 2004 Pointwest Technologies Corp.
 * @date Jan 9, 2004
 */
public class LaboratoriesTestCase {

	private static Connection conn = new DataSourceProxy().getConnection();

	public static void main(String[] args) {		
		testCreateLaboratories();
		
	}

//	public void testListLaboratories() {
//		LaboratoryDAO dao = new LaboratoryDAO(conn);
//		ArrayList list = new ArrayList();
//		try {
//			list = dao.selectLaboratories();
//		} catch (Exception e) {
//			e.printStackTrace();
//			assertEquals("testListLaboratories: " + e.getMessage(),true,false);
//		}
//		assertEquals("testListLaboratories: Select successful.",true,true);
//	}
	
	public static void testCreateLaboratories()
	{
		LaboratoryDAO dao = new LaboratoryDAO(conn);
		LaboratoryData labData = new LaboratoryData();
		
		labData.setLabId(Long.parseLong("12345"));		
		labData.setLabName("Lab 1");		
		labData.setContactPerson("Abie Plata");		
//		labData.setBusinessAddrLine1(labForm.getBusAdd1());		
//		labData.setBusinessAddrLine2(labForm.getBusAdd2());
//		labData.setBusinessAddrLine3(labForm.getBusAdd3());
//		labData.setCity(labForm.getBusCity());		
//		labData.setCountry(labForm.getBusCountry());		
//		labData.setZipCode(labForm.getBusZipCode());		
//		labData.setOfficeNum(labForm.getOfcPhone1());	
//		labData.setOfficeNum2(labForm.getOfcPhone2());		
//		labData.setFaxNum(labForm.getFaxNumber());		
//		labData.setBranchName(labForm.getBranchName());		
//		labData.setBranchAddrLine1(labForm.getBranchAdd1());		
//		labData.setBranchAddrLine2(labForm.getBranchAdd2());
//		labData.setBranchAddrLine3(labForm.getBranchAdd3());		
//		labData.setBranchCity(labForm.getBranchCity());		
//		labData.setBranchCountry(labForm.getBranchCountry());		
//		labData.setBranchZipCode(Long.parseLong(labForm.getBranchZipCode()));
//		labData.setBranchPhone(labForm.getBranchPhone());				
//		labData.setBranchFax_num(labForm.getBranchFaxNumber());		
//		labData.setAccredited(true);		
//		labData.setAccreditDate(new Date(labForm.getAccreditedDate()));
//		labData.setCreateDate(new Date(System.currentTimeMillis()));
//		labData.setCreatedBy("IUMDEV");
		
		try {
			dao.insertLaboratoryData(labData);
		} catch (Exception e) {
			e.printStackTrace();			
		}		
	}
}
