/*
 * Created on Dec 17, 2003
 */
package com.slocpi.ium.data;

import java.util.Date;

/**
 * Contains information pertaining to an impairment of an applicant.
 * @author Engel
 */
public class ImpairmentData {

	private String policyNo;
	private long impairmentId;
	private String clientNumber;
	private String referenceNumber;
	private String impairmentCode;
	private String impairmentDesc;
	private String relationship;
	private String impairmentOrigin;
	private Date impairmentDate;
	private double heightInFeet;
	private double heightInInches;
	private double weight;
	private String bloodPressure;
	private String confirmation;
	private Date exportDate;
	private String createdBy;
	private Date createDate;
	private String updatedBy;
	private Date updateDate;
	
	//add jonbred
	private String actionCode;
	

	/**
	 * TODO method description getPolicyNo
	 * @return
	 */
	public String getPolicyNo() {
		return policyNo;
	}

	/**
	 * TODO method description getBloodPressure
	 * @return
	 */
	public String getBloodPressure() {
		return bloodPressure;
	}

	/**
	 * TODO method description getClientNumber
	 * @return
	 */
	public String getClientNumber() {
		return clientNumber;
	}

	/**
	 * TODO method description getConfirmation
	 * @return
	 */
	public String getConfirmation() {
		return confirmation;
	}

	/**
	 * TODO method description getCreateDate
	 * @return
	 */
	public Date getCreateDate() {
		return createDate;
	}

	/**
	 * TODO method description getCreatedBy
	 * @return
	 */
	public String getCreatedBy() {
		return createdBy;
	}

	/**
	 * TODO method description getImpairmentCode
	 * @return
	 */
	public String getImpairmentCode() {
		return impairmentCode;
	}

	/**
	 * TODO method description getImpairmentDesc
	 * @return
	 */
	public String getImpairmentDesc() {
		return impairmentDesc;
	}

	/**
	 * TODO method description getExportDate
	 * @return
	 */
	public Date getExportDate() {
		return exportDate;
	}

	/**
	 * TODO method description getHeightInFeet
	 * @return
	 */
	public double getHeightInFeet() {
		return heightInFeet;
	}

	/**
	 * TODO method description getHeightInInches
	 * @return
	 */
	public double getHeightInInches() {
		return heightInInches;
	}

	/**
	 * TODO method description getImpairmentDate
	 * @return
	 */
	public Date getImpairmentDate() {
		return impairmentDate;
	}

	/**
	 * TODO method description getImpairmentId
	 * @return
	 */
	public long getImpairmentId() {
		return impairmentId;
	}

	/**
	 * TODO method description getImpairmentOrigin
	 * @return
	 */
	public String getImpairmentOrigin() {
		return impairmentOrigin;
	}

	/**
	 * TODO method description getReferenceNumber
	 * @return
	 */
	public String getReferenceNumber() {
		return referenceNumber;
	}

	/**
	 * TODO method description getRelationship
	 * @return
	 */
	public String getRelationship() {
		return relationship;
	}

	/**
	 * TODO method description getUpdateDate
	 * @return
	 */
	public Date getUpdateDate() {
		return updateDate;
	}

	/**
	 * TODO method description getUpdatedBy
	 * @return
	 */
	public String getUpdatedBy() {
		return updatedBy;
	}

	/**
	 * TODO method description getWeight
	 * @return
	 */
	public double getWeight() {
		return weight;
	}

	/**
	 * TODO method description setPolicyNo
	 * @param d
	 */
	public void setPolicyNo(String string) {
		policyNo = string;
	}

	/**
	 * TODO method description setBloodPressure
	 * @param d
	 */
	public void setBloodPressure(String string) {
		bloodPressure = string;
	}

	/**
	 * TODO method description setClientNumber
	 * @param string
	 */
	public void setClientNumber(String string) {
		clientNumber = string;
	}

	/**
	 * TODO method description setConfirmation
	 * @param string
	 */
	public void setConfirmation(String string) {
		confirmation = string;
	}

	/**
	 * TODO method description setCreateDate
	 * @param date
	 */
	public void setCreateDate(Date date) {
		createDate = date;
	}

	/**
	 * TODO method description setCreatedBy
	 * @param string
	 */
	public void setCreatedBy(String string) {
		createdBy = string;
	}

	/**
	 * TODO method description setImpairmentCode
	 * @param string
	 */
	public void setImpairmentCode(String string) {
		impairmentCode = string;
	}

	/**
	 * TODO method description setImpairmentDesc
	 * @param string
	 */
	public void setImpairmentDesc(String string) {
		impairmentDesc = string;
	}	

	/**
	 * TODO method description setExportDate
	 * @param date
	 */
	public void setExportDate(Date date) {
		exportDate = date;
	}

	/**
	 * TODO method description setHeightInFeet
	 * @param d
	 */
	public void setHeightInFeet(double d) {
		heightInFeet = d;
	}

	/**
	 * TODO method description setHeightInInches
	 * @param d
	 */
	public void setHeightInInches(double d) {
		heightInInches = d;
	}

	/**
	 * TODO method description setImpairmentDate
	 * @param date
	 */
	public void setImpairmentDate(Date date) {
		impairmentDate = date;
	}

	/**
	 * TODO method description setImpairmentId
	 * @param l
	 */
	public void setImpairmentId(long l) {
		impairmentId = l;
	}

	/**
	 * TODO method description setImpairmentOrigin
	 * @param string
	 */
	public void setImpairmentOrigin(String string) {
		impairmentOrigin = string;
	}

	/**
	 * TODO method description setReferenceNumber
	 * @param string
	 */
	public void setReferenceNumber(String string) {
		referenceNumber = string;
	}

	/**
	 * TODO method description setRelationship
	 * @param string
	 */
	public void setRelationship(String string) {
		relationship = string;
	}

	/**
	 * TODO method description setUpdateDate
	 * @param date
	 */
	public void setUpdateDate(Date date) {
		updateDate = date;
	}

	/**
	 * TODO method description setUpdatedBy
	 * @param string
	 */
	public void setUpdatedBy(String string) {
		updatedBy = string;
	}

	/**
	 * TODO method description setWeight
	 * @param d
	 */
	public void setWeight(double d) {
		weight = d;
	}

	public String getActionCode() {
		return actionCode;
	}

	public void setActionCode(String string) {
		actionCode = string;
	}

}
