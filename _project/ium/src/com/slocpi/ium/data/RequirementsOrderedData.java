/*
 * Created on Jan 26, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.data;

/**
 * @author nic.decapia
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class RequirementsOrderedData {
	
	private RequirementData requirement = new RequirementData();
	private SunLifeOfficeData department = new SunLifeOfficeData();
	private UserProfileData user = new UserProfileData();
	private long requirementsOrdered = 0;
	
	
	/**
	 * @return
	 */
	public SunLifeOfficeData getDepartment() {
		return department;
	}

	/**
	 * @return
	 */
	public RequirementData getRequirement() {
		return requirement;
	}

	/**
	 * @return
	 */
	public long getRequirementsOrdered() {
		return requirementsOrdered;
	}

	/**
	 * @return
	 */
	public UserProfileData getUser() {
		return user;
	}

	/**
	 * @param data
	 */
	public void setDepartment(SunLifeOfficeData data) {
		department = data;
	}

	/**
	 * @param data
	 */
	public void setRequirement(RequirementData data) {
		requirement = data;
	}

	/**
	 * @param l
	 */
	public void setRequirementsOrdered(long l) {
		requirementsOrdered = l;
	}

	/**
	 * @param data
	 */
	public void setUser(UserProfileData data) {
		user = data;
	}

}