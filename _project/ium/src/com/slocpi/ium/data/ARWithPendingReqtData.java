/*
 * Created on Feb 22, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.data;


/**
 * @author Nic Decapia
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class ARWithPendingReqtData
{
	private String branchName = new String();
	private String underwriterLastName = new String();
	private String underwriterGivenName = new String();
	private String underwriterMiddleName = new String();
	private String referenceNumber = new String();
	private String requirementCode = new String();
	private String requirementName = new String();
	private String dateOrdered = new String();
	private String dateSubmit = new String();
	private int totalCount = 0;
	/**
	 * @return
	 */
	public String getBranchName()
	{
		return branchName;
	}

	/**
	 * @return
	 */
	public String getDateOrdered()
	{
		return dateOrdered;
	}

	/**
	 * @return
	 */
	public String getDateSubmit()
	{
		return dateSubmit;
	}

	/**
	 * @return
	 */
	public String getReferenceNumber()
	{
		return referenceNumber;
	}

	/**
	 * @return
	 */
	public String getRequirementName()
	{
		return requirementName;
	}

	/**
	 * @return
	 */
	public int getTotalCount()
	{
		return totalCount;
	}

	/**
	 * @return
	 */
	public String getUnderwriterLastName()
	{
		return underwriterLastName;
	}

	/**
	 * @return
	 */
	public String getUnderwriterGivenName()
	{
		return underwriterGivenName;
	}
	
	/**
	 * @return
	 */
	public String getUnderwriterMiddleName()
	{
		return underwriterMiddleName;
	}	

	/**
	 * @param string
	 */
	public void setBranchName(String string)
	{
		branchName = string;
	}

	/**
	 * @param string
	 */
	public void setDateOrdered(String date)
	{
		dateOrdered = date;
	}

	/**
	 * @param string
	 */
	public void setDateSubmit(String date)
	{
		dateSubmit = date;
	}

	/**
	 * @param string
	 */
	public void setReferenceNumber(String string)
	{
		referenceNumber = string;
	}

	/**
	 * @param string
	 */
	public void setRequirementName(String string)
	{
		requirementName = string;
	}

	/**
	 * @param i
	 */
	public void setTotalCount(int i)
	{
		totalCount = i;
	}

	/**
	 * @param string
	 */
	public void setUnderwriterLastName(String string)
	{
		underwriterLastName = string;
	}

	/**
	 * @param string
	 */
	public void setUnderwriterGivenName(String string)
	{
		underwriterGivenName = string;
	}

	/**
	 * @param string
	 */
	public void setUnderwriterMidName(String string)
	{
		underwriterMiddleName = string;
	}	
	/**
	 * @return
	 */
	public String getRequirementCode()
	{
		return requirementCode;
	}

	/**
	 * @param string
	 */
	public void setRequirementCode(String string)
	{
		requirementCode = string;
	}

}
