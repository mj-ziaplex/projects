/*
 * Created on Dec 23, 2003
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.data;

import java.util.Date;

/**
 * @author Cris
 *
 * Container for Medical Laboratory reference information.
 */
public class MedicalLaboratoryData {
	private long labId;
	private String labName;
	private String contactPerson;
	private String officeNumber;
	private String officeNumber2;
	private String faxNumber;
	private String busAddrLine1;
	private String busAddrLine2;
	private String busAddrLine3;
	private String city;
	private String province;
	private String country;
	private String zipCode;
	private String branchName;
	private String branchAddrLine1;
	private String branchAddrLine2;
	private String branchAddrLine3;
	private String branchCity;
	private String branchProvince;
	private String branchCountry;
	private String branchZipCode;
	private String branchPhoneNumber;
	private String branchFaxNumber;
	private boolean accredited;
	private Date accreditationDate;
	private String createdBy;
	private Date createDate;
	private String updatedBy;
	private Date updateDate;

	/**
	 * @return
	 */
	public Date getAccreditationDate() {
		return accreditationDate;
	}

	/**
	 * @return
	 */
	public boolean isAccredited() {
		return accredited;
	}

	/**
	 * @return
	 */
	public String getBranchAddrLine1() {
		return branchAddrLine1;
	}

	/**
	 * @return
	 */
	public String getBranchAddrLine2() {
		return branchAddrLine2;
	}

	/**
	 * @return
	 */
	public String getBranchAddrLine3() {
		return branchAddrLine3;
	}

	/**
	 * @return
	 */
	public String getBranchCity() {
		return branchCity;
	}

	/**
	 * @return
	 */
	public String getBranchCountry() {
		return branchCountry;
	}

	/**
	 * @return
	 */
	public String getBranchFaxNumber() {
		return branchFaxNumber;
	}

	/**
	 * @return
	 */
	public String getBranchName() {
		return branchName;
	}

	/**
	 * @return
	 */
	public String getBranchPhoneNumber() {
		return branchPhoneNumber;
	}

	/**
	 * @return
	 */
	public String getBranchProvince() {
		return branchProvince;
	}

	/**
	 * @return
	 */
	public String getBranchZipCode() {
		return branchZipCode;
	}

	/**
	 * @return
	 */
	public String getBusAddrLine1() {
		return busAddrLine1;
	}

	/**
	 * @return
	 */
	public String getBusAddrLine2() {
		return busAddrLine2;
	}

	/**
	 * @return
	 */
	public String getBusAddrLine3() {
		return busAddrLine3;
	}

	/**
	 * @return
	 */
	public String getCity() {
		return city;
	}

	/**
	 * @return
	 */
	public String getContactPerson() {
		return contactPerson;
	}

	/**
	 * @return
	 */
	public String getCountry() {
		return country;
	}

	/**
	 * @return
	 */
	public Date getCreateDate() {
		return createDate;
	}

	/**
	 * @return
	 */
	public String getCreatedBy() {
		return createdBy;
	}

	/**
	 * @return
	 */
	public String getFaxNumber() {
		return faxNumber;
	}

	/**
	 * @return
	 */
	public long getLabId() {
		return labId;
	}

	/**
	 * @return
	 */
	public String getLabName() {
		return labName;
	}

	/**
	 * @return
	 */
	public String getOfficeNumber() {
		return officeNumber;
	}

	/**
	 * @return
	 */
	public String getOfficeNumber2() {
		return officeNumber2;
	}

	/**
	 * @return
	 */
	public String getProvince() {
		return province;
	}

	/**
	 * @return
	 */
	public Date getUpdateDate() {
		return updateDate;
	}

	/**
	 * @return
	 */
	public String getUpdatedBy() {
		return updatedBy;
	}

	/**
	 * @return
	 */
	public String getZipCode() {
		return zipCode;
	}

	/**
	 * @param date
	 */
	public void setAccreditationDate(Date date) {
		accreditationDate = date;
	}

	/**
	 * @param b
	 */
	public void setAccredited(boolean b) {
		accredited = b;
	}

	/**
	 * @param string
	 */
	public void setBranchAddrLine1(String string) {
		branchAddrLine1 = string;
	}

	/**
	 * @param string
	 */
	public void setBranchAddrLine2(String string) {
		branchAddrLine2 = string;
	}

	/**
	 * @param string
	 */
	public void setBranchAddrLine3(String string) {
		branchAddrLine3 = string;
	}

	/**
	 * @param string
	 */
	public void setBranchCity(String string) {
		branchCity = string;
	}

	/**
	 * @param string
	 */
	public void setBranchCountry(String string) {
		branchCountry = string;
	}

	/**
	 * @param string
	 */
	public void setBranchFaxNumber(String string) {
		branchFaxNumber = string;
	}

	/**
	 * @param string
	 */
	public void setBranchName(String string) {
		branchName = string;
	}

	/**
	 * @param string
	 */
	public void setBranchPhoneNumber(String string) {
		branchPhoneNumber = string;
	}

	/**
	 * @param string
	 */
	public void setBranchProvince(String string) {
		branchProvince = string;
	}

	/**
	 * @param string
	 */
	public void setBranchZipCode(String string) {
		branchZipCode = string;
	}

	/**
	 * @param string
	 */
	public void setBusAddrLine1(String string) {
		busAddrLine1 = string;
	}

	/**
	 * @param string
	 */
	public void setBusAddrLine2(String string) {
		busAddrLine2 = string;
	}

	/**
	 * @param string
	 */
	public void setBusAddrLine3(String string) {
		busAddrLine3 = string;
	}

	/**
	 * @param string
	 */
	public void setCity(String string) {
		city = string;
	}

	/**
	 * @param string
	 */
	public void setContactPerson(String string) {
		contactPerson = string;
	}

	/**
	 * @param string
	 */
	public void setCountry(String string) {
		country = string;
	}

	/**
	 * @param date
	 */
	public void setCreateDate(Date date) {
		createDate = date;
	}

	/**
	 * @param string
	 */
	public void setCreatedBy(String string) {
		createdBy = string;
	}

	/**
	 * @param string
	 */
	public void setFaxNumber(String string) {
		faxNumber = string;
	}

	/**
	 * @param l
	 */
	public void setLabId(long l) {
		labId = l;
	}

	/**
	 * @param string
	 */
	public void setLabName(String string) {
		labName = string;
	}

	/**
	 * @param string
	 */
	public void setOfficeNumber(String string) {
		officeNumber = string;
	}

	/**
	 * @param string
	 */
	public void setOfficeNumber2(String string) {
		officeNumber2 = string;
	}

	/**
	 * @param string
	 */
	public void setProvince(String string) {
		province = string;
	}

	/**
	 * @param date
	 */
	public void setUpdateDate(Date date) {
		updateDate = date;
	}

	/**
	 * @param string
	 */
	public void setUpdatedBy(String string) {
		updatedBy = string;
	}

	/**
	 * @param string
	 */
	public void setZipCode(String string) {
		zipCode = string;
	}

}
