/*
 * Created on Feb 22, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.data;

/**
 * @author Nic Decapia
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class ARWithPendingReqtFilterData
{
	// report types
	public static final int BRANCH_REPORT_TYPE = 0;
	public static final int UNDWTR_REPORT_TYPE = 1;
	
	// criteria
	private String startDate = new String();
	private String endDate = new String();
	private int reportType = BRANCH_REPORT_TYPE;
	
	// filters
	private SunLifeOfficeData branch = new SunLifeOfficeData();
	private UserProfileData underwriter = new UserProfileData();
	
	private boolean overdue = false;;
	
	public boolean isOverdue() {
		return overdue;
	}

	public void setOverdue(boolean overdue) {
		this.overdue = overdue;
	}

	/**
	 * @return
	 */
	public SunLifeOfficeData getBranch()
	{
		return branch;
	}

	/**
	 * @return
	 */
	public String getEndDate()
	{
		return endDate;
	}

	/**
	 * @return
	 */
	public int getReportType()
	{
		return reportType;
	}

	/**
	 * @return
	 */
	public String getStartDate()
	{
		return startDate;
	}

	/**
	 * @return
	 */
	public UserProfileData getUnderwriter()
	{
		return underwriter;
	}

	/**
	 * @param data
	 */
	public void setBranch(SunLifeOfficeData data)
	{
		branch = data;
	}

	/**
	 * @param string
	 */
	public void setEndDate(String string)
	{
		endDate = string;
	}

	/**
	 * @param i
	 */
	public void setReportType(int i)
	{
		reportType = i;
	}

	/**
	 * @param string
	 */
	public void setStartDate(String string)
	{
		startDate = string;
	}

	/**
	 * @param data
	 */
	public void setUnderwriter(UserProfileData data)
	{
		underwriter = data;
	}

}
