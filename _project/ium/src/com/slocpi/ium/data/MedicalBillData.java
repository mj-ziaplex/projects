/*
 * Created on Dec 22, 2003
 * package name = com.slocpi.ium.data
 * file name    = MedicalBillData.java
 */
package com.slocpi.ium.data;

/**
 * Contains information pertaining to the medical bill of an application.
 * @author Engel
 * 
 */
public class MedicalBillData {

	private long medicalBillId;
	private double amount;
	
	/**
	 * TODO method description getAmount
	 * @return
	 */
	public double getAmount() {
		return amount;
	}

	/**
	 * TODO method description getMedicalBillId
	 * @return
	 */
	public long getMedicalBillId() {
		return medicalBillId;
	}

	/**
	 * TODO method description setAmount
	 * @param d
	 */
	public void setAmount(double d) {
		amount = d;
	}

	/**
	 * TODO method description setMedicalBillId
	 * @param l
	 */
	public void setMedicalBillId(long l) {
		medicalBillId = l;
	}

}
