package com.slocpi.ium.data;

import java.util.Date;

/**
 * Container for MIB Impairment reference information.
 * @author Engel 
 */
public class MIBImpairmentData {

	private String MIBImpairmentCode;
	private String MIBImpairmentDesc;
	private String createdBy;
	private Date createDate;
	private String updatedBy;
	private Date updateDate;
		
	
	/**
	 * Sets the impairment code attribute.
	 * @param MIBImpairmentCode impairment code
	 */
	public void setMIBImpairmentCode(String MIBImpairmentCode) {
		this.MIBImpairmentCode = MIBImpairmentCode;
	}
		
	/**
	 * Retrieves the impairment code attribute.
	 * @return String impairment code
	 */
	public String getMIBImpairmentCode() {
		return MIBImpairmentCode;
	}

	/**
	 * Sets the impairment description attribute.
	 * @param MIBImpairmentDesc description of the action code
	 */
	public void setMIBImpairmentDesc(String MIBImpairmentDesc) {
		this.MIBImpairmentDesc = MIBImpairmentDesc;
	}

	/**
	 * Retrieves the impairment code attribute.
	 * @return String description of the action code
	 */
	public String getMIBImpairmentDesc() {
		return MIBImpairmentDesc;
	}

	/**
	 * Sets the created date attribute.
	 * @param createDate date when the record was created
	 */ 
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	
	/**
	 * Retrieves the created date attribute.
	 * @return Date date when the record was created
	 */
	public Date getCreateDate() {
		return createDate;
	}

	/**
	 * Sets the created by attribute.
	 * @param createdBy user who created the record
	 */ 
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * Retrieves the created by attribute.
	 * @return String user who created the record
	 */
	public String getCreatedBy() {
		return createdBy;
	}

	/**
	 * Sets the updated date attribute.
	 * @param updateDate date when the record was last updated
	 */ 
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
	
	/**
	 * Retrieves the updated date attribute.
	 * @return Date date when the record was last updated
	 */
	public Date getUpdateDate() {
		return updateDate;
	}

	/**
	 * Sets the updated by attribute.
	 * @param updatedBy user who last updated the record
	 */ 
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	
	/**
	 * Retrieves the updated by attribute.
	 * @return String user who last updated the record
	 */
	public String getUpdatedBy() {
		return updatedBy;
	}

}
