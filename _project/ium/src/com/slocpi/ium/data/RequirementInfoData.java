/*
 * Created on Dec 22, 2003
 * package name = com.slocpi.ium.data
 * file name    = RequirementInfoData.java
 */
package com.slocpi.ium.data;

/**
 * Container for Requirement reference information.
 * @author Engel
 * 
 */
public class RequirementInfoData {

	private String code;
	private String desc;
	private String clientType;
	private String level;
	private long validity;
	private String formInd;
	private String formId;
	/**
	 * TODO method description getClientType
	 * @return
	 */
	public String getClientType() {
		return clientType;
	}

	/**
	 * TODO method description getCode
	 * @return
	 */
	public String getCode() {
		return code;
	}

	/**
	 * TODO method description getDesc
	 * @return
	 */
	public String getDesc() {
		return desc;
	}

	/**
	 * TODO method description getFormId
	 * @return
	 */
	public String getFormId() {
		return formId;
	}

	/**
	 * TODO method description getFormInd
	 * @return
	 */
	public String getFormInd() {
		return formInd;
	}

	/**
	 * TODO method description getLevel
	 * @return
	 */
	public String getLevel() {
		return level;
	}

	/**
	 * TODO method description getValidity
	 * @return
	 */
	public long getValidity() {
		return validity;
	}

	/**
	 * TODO method description setClientType
	 * @param string
	 */
	public void setClientType(String string) {
		clientType = string;
	}

	/**
	 * TODO method description setCode
	 * @param string
	 */
	public void setCode(String string) {
		code = string;
	}

	/**
	 * TODO method description setDesc
	 * @param string
	 */
	public void setDesc(String string) {
		desc = string;
	}

	/**
	 * TODO method description setFormId
	 * @param string
	 */
	public void setFormId(String string) {
		formId = string;
	}

	/**
	 * TODO method description setFormInd
	 * @param string
	 */
	public void setFormInd(String string) {
		formInd = string;
	}

	/**
	 * TODO method description setLevel
	 * @param string
	 */
	public void setLevel(String string) {
		level = string;
	}

	/**
	 * TODO method description setValidity
	 * @param l
	 */
	public void setValidity(long l) {
		validity = l;
	}

}
