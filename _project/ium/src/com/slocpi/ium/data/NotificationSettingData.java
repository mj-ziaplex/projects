
package com.slocpi.ium.data;

/**
 * Container for messages sent by the system.
 * @author Engel
 * 
 */
public class NotificationSettingData {
	private String eventId;
	private String eventName;
	private String notificationInd;
	
	
	/**
	 * @return
	 */
	public String getEventId() {
		return this.eventId;
	}

	/**
	 * @return
	 */
	public String getEventName() {
		return this.eventName;
	}

	/**
	 * @return
	 */
	public String getNotificationInd() {
		return this.notificationInd;
	}

	/**
	 * @param string
	 */
	public void setEventId(String eventId) {
		this.eventId = eventId;
	}

	/**
	 * @param string
	 */
	public void setEventName(String eventName) {
		this.eventName = eventName;
	}

	/**
	 * @param string
	 */
	public void setNotificationInd(String notificationInd) {
		this.notificationInd = notificationInd;
	}

}
