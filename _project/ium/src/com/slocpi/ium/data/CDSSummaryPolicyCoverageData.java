/*
 * Created on Dec 22, 2003
 * package name = com.slocpi.ium.data
 * file name    = CDSSummaryPolicyCoverage.java
 */
package com.slocpi.ium.data;


/**
 * Contains information pertaining to the summary policy coverage of an applicant.
 * @author Engel
 * 
 */
public class CDSSummaryPolicyCoverageData {

	private double totalOtherCompanies;
	private double appliedForBr;
	private double appliedForAd;
	private double pendingAmountBr;
	private double pendingAmountAd;
	private double existingInForceBr;
	private double existingInForceAd;
	private double existingLapsedBr;
	private double existingLapsedAd;
	private double totalExistingSunLifeBr;
	private double totalExistingSunLifeAd;
	private double totalSunLifeBr;
	private double totalSunLifeAd;	
	private double otherCompaniesBr;
	private double otherCompaniesAd;
	private double totalSunLifeOtherCompaniesBr;
	private double totalSunLifeOtherCompaniesAd;
	private double totalCCRCoverage;
	private double totalAPDBCoverage;
	private double totalHIBCoverage;
	private double totalFBBMBCoverage;
	private double totalReinsuredAmount;
	private long sequenceNumber;
	private long CDSSequenceNumber;
	private double totalAdbReinsuredAmount;
	
	public double getTotalAdbReinsuredAmount() {
		return totalAdbReinsuredAmount;
	}

	public void setTotalAdbReinsuredAmount(double totalAdbReinsuredAmount) {
		this.totalAdbReinsuredAmount = totalAdbReinsuredAmount;
	}

	/**
	 * @return
	 */
	public double getAppliedForAd() {
		return appliedForAd;
	}

	/**
	 * @return
	 */
	public double getAppliedForBr() {
		return appliedForBr;
	}

	/**
	 * @return
	 */
	public long getCDSSequenceNumber() {
		return CDSSequenceNumber;
	}

	/**
	 * @return
	 */
	public double getExistingInForceAd() {
		return existingInForceAd;
	}

	/**
	 * @return
	 */
	public double getExistingInForceBr() {
		return existingInForceBr;
	}

	/**
	 * @return
	 */
	public double getExistingLapsedAd() {
		return existingLapsedAd;
	}

	/**
	 * @return
	 */
	public double getExistingLapsedBr() {
		return existingLapsedBr;
	}

	/**
	 * @return
	 */
	public double getOtherCompaniesAd() {
		return otherCompaniesAd;
	}

	/**
	 * @return
	 */
	public double getOtherCompaniesBr() {
		return otherCompaniesBr;
	}

	/**
	 * @return
	 */
	public double getPendingAmountAd() {
		return pendingAmountAd;
	}

	/**
	 * @return
	 */
	public double getPendingAmountBr() {
		return pendingAmountBr;
	}

	/**
	 * @return
	 */
	public long getSequenceNumber() {
		return sequenceNumber;
	}

	/**
	 * @return
	 */
	public double getTotalAPDBCoverage() {
		return totalAPDBCoverage;
	}

	/**
	 * @return
	 */
	public double getTotalCCRCoverage() {
		return totalCCRCoverage;
	}

	/**
	 * @return
	 */
	public double getTotalExistingSunLifeAd() {
		return totalExistingSunLifeAd;
	}

	/**
	 * @return
	 */
	public double getTotalExistingSunLifeBr() {
		return totalExistingSunLifeBr;
	}

	/**
	 * @return
	 */
	public double getTotalFBBMBCoverage() {
		return totalFBBMBCoverage;
	}

	/**
	 * @return
	 */
	public double getTotalHIBCoverage() {
		return totalHIBCoverage;
	}

	/**
	 * @return
	 */
	public double getTotalReinsuredAmount() {
		return totalReinsuredAmount;
	}

	/**
	 * @return
	 */
	public double getTotalSunLifeAd() {
		return totalSunLifeAd;
	}

	/**
	 * @return
	 */
	public double getTotalSunLifeBr() {
		return totalSunLifeBr;
	}

	/**
	 * @return
	 */
	public double getTotalSunLifeOtherCompaniesAd() {
		return totalSunLifeOtherCompaniesAd;
	}

	/**
	 * @return
	 */
	public double getTotalSunLifeOtherCompaniesBr() {
		return totalSunLifeOtherCompaniesBr;
	}

	/**
	 * @param d
	 */
	public void setAppliedForAd(double d) {
		appliedForAd = d;
	}

	/**
	 * @param d
	 */
	public void setAppliedForBr(double d) {
		appliedForBr = d;
	}

	/**
	 * @param l
	 */
	public void setCDSSequenceNumber(long l) {
		CDSSequenceNumber = l;
	}

	/**
	 * @param d
	 */
	public void setExistingInForceAd(double d) {
		existingInForceAd = d;
	}

	/**
	 * @param d
	 */
	public void setExistingInForceBr(double d) {
		existingInForceBr = d;
	}

	/**
	 * @param d
	 */
	public void setExistingLapsedAd(double d) {
		existingLapsedAd = d;
	}

	/**
	 * @param d
	 */
	public void setExistingLapsedBr(double d) {
		existingLapsedBr = d;
	}

	/**
	 * @param d
	 */
	public void setOtherCompaniesAd(double d) {
		otherCompaniesAd = d;
	}

	/**
	 * @param d
	 */
	public void setOtherCompaniesBr(double d) {
		otherCompaniesBr = d;
	}

	/**
	 * @param d
	 */
	public void setPendingAmountAd(double d) {
		pendingAmountAd = d;
	}

	/**
	 * @param d
	 */
	public void setPendingAmountBr(double d) {
		pendingAmountBr = d;
	}

	/**
	 * @param l
	 */
	public void setSequenceNumber(long l) {
		sequenceNumber = l;
	}

	/**
	 * @param d
	 */
	public void setTotalAPDBCoverage(double d) {
		totalAPDBCoverage = d;
	}

	/**
	 * @param d
	 */
	public void setTotalCCRCoverage(double d) {
		totalCCRCoverage = d;
	}

	/**
	 * @param d
	 */
	public void setTotalExistingSunLifeAd(double d) {
		totalExistingSunLifeAd = d;
	}

	/**
	 * @param d
	 */
	public void setTotalExistingSunLifeBr(double d) {
		totalExistingSunLifeBr = d;
	}

	/**
	 * @param d
	 */
	public void setTotalFBBMBCoverage(double d) {
		totalFBBMBCoverage = d;
	}

	/**
	 * @param d
	 */
	public void setTotalHIBCoverage(double d) {
		totalHIBCoverage = d;
	}

	/**
	 * @param d
	 */
	public void setTotalReinsuredAmount(double d) {
		totalReinsuredAmount = d;
	}

	/**
	 * @param d
	 */
	public void setTotalSunLifeAd(double d) {
		totalSunLifeAd = d;
	}

	/**
	 * @param d
	 */
	public void setTotalSunLifeBr(double d) {
		totalSunLifeBr = d;
	}

	/**
	 * @param d
	 */
	public void setTotalSunLifeOtherCompaniesAd(double d) {
		totalSunLifeOtherCompaniesAd = d;
	}

	/**
	 * @param d
	 */
	public void setTotalSunLifeOtherCompaniesBr(double d) {
		totalSunLifeOtherCompaniesBr = d;
	}

	/**
	 * @return
	 */
	public double getTotalOtherCompanies() {
		return totalOtherCompanies;
	}

	/**
	 * @param d
	 */
	public void setTotalOtherCompanies(double d) {
		totalOtherCompanies = d;
	}

}
