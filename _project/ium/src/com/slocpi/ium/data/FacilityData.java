/*
 * Created on Dec 22, 2003
 * package name = com.slocpi.ium.data
 * file name    = FacilityData.java
 */
package com.slocpi.ium.data;

/**
 * Container for Facility reference information.
 * @author Engel
 * 
 */
public class FacilityData {

	private long facilityCode;
	private String facilityDesc;
	/**
	 * TODO method description getFacilityCode
	 * @return
	 */
	public long getFacilityCode() {
		return facilityCode;
	}

	/**
	 * TODO method description getFacilityDesc
	 * @return
	 */
	public String getFacilityDesc() {
		return facilityDesc;
	}

	/**
	 * TODO method description setFacilityCode
	 * @param l
	 */
	public void setFacilityCode(long l) {
		facilityCode = l;
	}

	/**
	 * TODO method description setFacilityDesc
	 * @param string
	 */
	public void setFacilityDesc(String string) {
		facilityDesc = string;
	}

}
