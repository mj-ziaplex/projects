/**
 * TurnAroundTimeLOBData.java				Copyright (c) 2004 Pointwest Technologies Corp.
 * TODO DOCUMENT ME!
 * 
 * @author aditalo	 @date Mar 21, 2004
 */
package com.slocpi.ium.data;

import java.math.BigDecimal;
import java.util.ArrayList;

/**
 * TODO DOCUMENT ME!
 * 
 * @author aditalo		Copyright (c) 2004 Pointwest Technologies Corp.
 * @date Mar 21, 2004
 */
public class TurnAroundTimeLOBData {

	private String lob;
	private BigDecimal elapsedTime;
	private BigDecimal requestsProcessed;
	private BigDecimal aveTAT;
	private ArrayList turnAroundTimeDetails;

	/**
	 * @return
	 */
	public BigDecimal getAveTAT() {
		return aveTAT;
	}

	/**
	 * @return
	 */
	public BigDecimal getElapsedTime() {
		return elapsedTime;
	}

	/**
	 * @return
	 */
	public String getLob() {
		return lob;
	}

	/**
	 * @return
	 */
	public BigDecimal getRequestsProcessed() {
		return requestsProcessed;
	}

	/**
	 * @return
	 */
	public ArrayList getTurnAroundTimeDetails() {
		return turnAroundTimeDetails;
	}

	/**
	 * @param decimal
	 */
	public void setAveTAT(BigDecimal decimal) {
		aveTAT = decimal;
	}

	/**
	 * @param decimal
	 */
	public void setElapsedTime(BigDecimal decimal) {
		elapsedTime = decimal;
	}

	/**
	 * @param string
	 */
	public void setLob(String string) {
		lob = string;
	}

	/**
	 * @param decimal
	 */
	public void setRequestsProcessed(BigDecimal decimal) {
		requestsProcessed = decimal;
	}

	/**
	 * @param list
	 */
	public void setTurnAroundTimeDetails(ArrayList list) {
		turnAroundTimeDetails = list;
	}

}
