package com.slocpi.ium.data;

import java.util.Date;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.slocpi.ium.util.DateHelper;

public class MedExamFiltersData {

	private static final Logger LOGGER = LoggerFactory.getLogger(MedExamFiltersData.class);
	private String referenceNo;
	private String clientNo;
	private String lastName;
	private String firstName;
	private long status;
	private Date sevenDayMemoDate;
	private Date dateRequested;
	private Date startFollowUpDate;
	private Date endFollowUpDate;
	private long laboratory;
	private long examiner;
	private String unAssignedOnly;
	private long followUpNo;
	private long medicalRecordId;
	private Date validityDate;

    /**
     * @return
     */
    public String getClientNo() {
        return clientNo;
    }

    /**
     * @return
     */
    public Date getDateRequested() {
        return dateRequested;
    }

	public String getDateRequested_String(){
		return DateHelper.format(getDateRequested(),"ddMMMyyyy");
	}

    /**
     * @return
     */
    public Date getEndFollowUpDate() {
        return endFollowUpDate;
    }
    
	public String getEndFollowUpDate_String(){
		return DateHelper.format(getEndFollowUpDate(),"ddMMMyyyy");
	}

    /**
     * @return
     */
    public long getExaminer() {
        return examiner;
    }

    /**
     * @return
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * @return
     */
    public long getLaboratory() {
        return laboratory;
    }

    /**
     * @return
     */
    public String getLastName() {
        return lastName;
    }

	/**
	 * @return
	 */
	public String getReferenceNo() {
	    return referenceNo;
	}

    /**
     * @return
     */
    public Date getSevenDayMemoDate() {
        return sevenDayMemoDate;
    }

	public String getSevenDayMemoDate_String(){
		return DateHelper.format(getSevenDayMemoDate(),"ddMMMyyyy");
	}
	
    /**
     * @return
     */
    public Date getStartFollowUpDate() {
        return startFollowUpDate;
    }

	public String getStartFollowUpDate_String(){
		return DateHelper.format(getStartFollowUpDate(),"ddMMMyyyy");
	}
	
    /**
     * @return
     */
    public long getStatus() {
        return status;
    }

    /**
     * @return
     */
    public String getUnAssignedOnly() {
        return unAssignedOnly;
    }

    /**
     * @param string
     */
    public void setClientNo(String string) {
        clientNo = string;
    }

    /**
     * @param date
     */
    public void setDateRequested(Date date) {
        dateRequested = date;
    }

    /**
     * @param date
     */
    public void setEndFollowUpDate(Date date) {
        endFollowUpDate = date;
    }

    /**
     * @param string
     */
    public void setExaminer(long l) {
        examiner = l;
    }

    /**
     * @param string
     */
    public void setFirstName(String string) {
        firstName = string;
    }

    /**
     * @param string
     */
    public void setLaboratory(long l) {
        laboratory = l;
    }

    /**
     * @param string
     */
    public void setLastName(String string) {
        lastName = string;
    }

	/**
	 * @param string
	 */
	public void setReferenceNo(String string) {
	    referenceNo = string;
	}

    /**
     * @param date
     */
    public void setSevenDayMemoDate(Date date) {
        sevenDayMemoDate = date;
    }

    /**
     * @param date
     */
    public void setStartFollowUpDate(Date date) {
        startFollowUpDate = date;
    }

    /**
     * @param string
     */
    public void setStatus(long l) {
        status = l;
    }

    /**
     * @param string
     */
    public void setUnAssignedOnly(String string) {
        unAssignedOnly = string;
    }

	public String getFilter(){
		
		LOGGER.info("getFilter start");
		StringBuffer filter = new StringBuffer();
		if (!(getReferenceNo()==null) && !(getReferenceNo().equals(""))){
			filter.append(appendToFilter("upper(POL.UAR_REFERENCE_NUM) like upper('%" + getReferenceNo() + "%')"));
		}
		if (!(getClientNo()==null) && !(getClientNo().equals(""))){
			filter.append(appendToFilter("upper(MED.CL_CLIENT_NUM) like upper('%" + getClientNo())+ "%')");
		}
		if (!(getLastName()==null) && !(getLastName().equals(""))){
			filter.append(appendToFilter("upper(CLI.CL_LAST_NAME) like upper('%" + getLastName()) + "%')");
		}
		if (!(getFirstName()==null) && !(getFirstName().equals(""))){
			filter.append(appendToFilter("upper(CLI.CL_GIVEN_NAME) like upper('%" + getFirstName()) + "%')");
		}
		if (getStatus()!=0){
			filter.append(appendToFilter("MED.MED_STATUS='" + getStatus())+ "'");
		}
		if (!(getSevenDayMemoDate_String().equals("")) && !(getSevenDayMemoDate_String()==null)){
			filter.append(appendToFilter("MED.MED_7DAY_MEMO_DATE='" + getSevenDayMemoDate_String())+ "'");
		}
		if (!(getDateRequested_String().equals("")) && !(getDateRequested_String()==null)){
			filter.append(appendToFilter("MED.MED_DATE_RECEIVED='" + getDateRequested_String())+ "'");
		}
		if (!(getStartFollowUpDate_String().equals("")) && !(getStartFollowUpDate_String()==null)){
			filter.append(appendToFilter("MED.MED_FOLLOW_UP_DATE >= '" + getStartFollowUpDate_String())+ "'");
		}
		if (!(getEndFollowUpDate_String().equals("")) && !(getEndFollowUpDate_String()==null)){
			filter.append(appendToFilter("MED.MED_FOLLOW_UP_DATE <= '" + getEndFollowUpDate_String())+ "'");
		}
		if (getLaboratory()!=0){
			filter.append(appendToFilter("MED.MED_LAB_ID='" + getLaboratory())+ "'");
		}
		if (getExaminer()!=0){
			filter.append(appendToFilter("MED.EXMNR_ID='" + getExaminer())+ "'");
		}
		if (!(getUnAssignedOnly()==null)){
			if (getUnAssignedOnly().equals("Y"))
				filter.append(appendToFilter("MED.CL_CLIENT_NUM IS NULL"));

		}
		if ((getMedicalRecordId()!=0)){
			filter.append(appendToFilter("MED.MED_RECORD_ID=" + getMedicalRecordId()));
		}
		if (!(getValidityDate_String().equals("")) && !(getValidityDate_String()==null)){
			filter.append(appendToFilter("MED.MED_VALIDITY_DATE >= '" + getValidityDate_String())+ "'");
		}
		LOGGER.info("getFilter end");
		return filter.toString();
	}
	
	private String appendToFilter(String appendFilter){
        return " AND " + appendFilter;
	}

	/**
	 * @return
	 */
	public long getFollowUpNo() {
		return followUpNo;
	}

	/**
	 * @param string
	 */
	public void setFollowUpNo(long l) {
		followUpNo = l;
	}

	/**
	 * @return
	 */
	public long getMedicalRecordId() {
		return medicalRecordId;
	}

	/**
	 * @param l
	 */
	public void setMedicalRecordId(long l) {
		medicalRecordId = l;
	}

	/**
	 * TODO method description getValidityDate
	 * @return
	 */
	public Date getValidityDate() {
		return validityDate;
	}

	/**
	 * TODO method description getValidityDate
	 * @return
	 */
	public String getValidityDate_String() {
		return DateHelper.format(getValidityDate(), "ddMMMyyyy");
	}
	
	/**
	 * TODO method description setValidityDate
	 * @param
	 */
	public void setValidityDate(Date validityDate) {
		this.validityDate = validityDate;
	}	
}
