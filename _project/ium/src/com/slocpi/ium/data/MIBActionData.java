package com.slocpi.ium.data;

import java.util.Date;

/**
 * Container for MIB Action reference information.
 * @author Engel 
 */
public class MIBActionData {

	private String MIBActionCode;
	private String MIBActionDesc;
	private String createdBy;
	private Date createDate;
	private String updatedBy;
	private Date updateDate;
	

	/**
	 * Sets the action code attribute.
	 * @param MIBActionCode code identifying the state of the application
	 */
	public void setMIBActionCode(String MIBActionCode) {
		this.MIBActionCode = MIBActionCode;
	}
		
	/**
	 * Retrieves the action code attribute.
	 * @return String code identifying the state of the application
	 */
	public String getMIBActionCode() {
		return MIBActionCode;
	}

	/**
	 * Sets the action description attribute.
	 * @param MIBActionDesc description of the action code
	 */
	public void setMIBActionDesc(String MIBActionDesc) {
		this.MIBActionDesc = MIBActionDesc;
	}

	/**
	 * Retrieves the action code attribute.
	 * @return String description of the action code
	 */
	public String getMIBActionDesc() {
		return MIBActionDesc;
	}

	/**
	 * Sets the created date attribute.
	 * @param createDate date when the record was created
	 */ 
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	
	/**
	 * Retrieves the created date attribute.
	 * @return Date date when the record was created
	 */
	public Date getCreateDate() {
		return createDate;
	}

	/**
	 * Sets the created by attribute.
	 * @param createdBy user who created the record
	 */ 
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * Retrieves the created by attribute.
	 * @return String user who created the record
	 */
	public String getCreatedBy() {
		return createdBy;
	}

	/**
	 * Sets the updated date attribute.
	 * @param updateDate date when the record was last updated
	 */ 
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
	
	/**
	 * Retrieves the updated date attribute.
	 * @return Date date when the record was last updated
	 */
	public Date getUpdateDate() {
		return updateDate;
	}

	/**
	 * Sets the updated by attribute.
	 * @param updatedBy user who last updated the record
	 */ 
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	
	/**
	 * Retrieves the updated by attribute.
	 * @return String user who last updated the record
	 */
	public String getUpdatedBy() {
		return updatedBy;
	}

}
