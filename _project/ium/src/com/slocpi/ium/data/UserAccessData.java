/*
 * Created on Dec 22, 2003
 * package name = com.slocpi.ium.data
 * file name    = UserAccessData.java
 */
package com.slocpi.ium.data;

import java.sql.Timestamp;

/**
 * Container for User Access reference information.
 * @author Engel
 * 
 */
public class UserAccessData {

	
	private String userId;		
	private String accessCode;	
	private String accessStatus;
	private String userID;
	private String accessDesc;
	private String pageDesc;
	private long pageID;
	private long accessID;
	private boolean access;
	private String createdBy;
	private Timestamp createdDate;
	private String updatedBy;
	private Timestamp updatedDate;
	
	
	/**
	 * TODO method description getAccessCode
	 * @return
	 */
	public String getAccessCode() {
		return accessCode;
	}

	/**
	 * TODO method description getAccessStatus
	 * @return
	 */
	public String getAccessStatus() {
		return accessStatus;
	}

	/**
	 * TODO method description getUserId
	 * @return
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * TODO method description setAccessCode
	 * @param string
	 */
	public void setAccessCode(String string) {
		accessCode = string;
	}

	/**
	 * TODO method description setAccessStatus
	 * @param string
	 */
	public void setAccessStatus(String string) {
		accessStatus = string;
	}

	/**
	 * TODO method description setUserId
	 * @param string
	 */
	public void setUserId(String string) {
		userId = string;
	}

	/**
	 * @return
	 */
	public boolean hasAccess() {
		return access;
	}

	/**
	 * @return
	 */
	public long getAccessID() {
		return accessID;
	}

	/**
	 * @return
	 */
	public String getCreatedBy() {
		return createdBy;
	}

	/**
	 * @return
	 */
	public Timestamp getCreatedDate() {
		return createdDate;
	}

	/**
	 * @return
	 */
	public long getPageID() {
		return pageID;
	}

	/**
	 * @return
	 */
	public String getUpdatedBy() {
		return updatedBy;
	}

	/**
	 * @return
	 */
	public Timestamp getUpdatedDate() {
		return updatedDate;
	}

	/**
	 * @return
	 */
	public String getUserID() {
		return userID;
	}

	/**
	 * @param b
	 */
	public void setAccess(boolean b) {
		access = b;
	}

	/**
	 * @param l
	 */
	public void setAccessID(long l) {
		accessID = l;
	}

	/**
	 * @param string
	 */
	public void setCreatedBy(String string) {
		createdBy = string;
	}

	/**
	 * @param timestamp
	 */
	public void setCreatedDate(Timestamp timestamp) {
		createdDate = timestamp;
	}

	/**
	 * @param l
	 */
	public void setPageID(long l) {
		pageID = l;
	}

	/**
	 * @param string
	 */
	public void setUpdatedBy(String string) {
		updatedBy = string;
	}

	/**
	 * @param timestamp
	 */
	public void setUpdatedDate(Timestamp timestamp) {
		updatedDate = timestamp;
	}

	/**
	 * @param string
	 */
	public void setUserID(String string) {
		userID = string;
	}

	/**
	 * @return
	 */
	public String getAccessDesc() {
		return accessDesc;
	}

	/**
	 * @return
	 */
	public String getPageDesc() {
		return pageDesc;
	}

	/**
	 * @param string
	 */
	public void setAccessDesc(String string) {
		accessDesc = string;
	}

	/**
	 * @param string
	 */
	public void setPageDesc(String string) {
		pageDesc = string;
	}

}
