/*
 * Created on Dec 22, 2003
 * package name = com.slocpi.ium.data
 * file name    = LOBStatusData.java
 */
package com.slocpi.ium.data;

/**
 * Container for LOB status reference information.
 * @author Engel
 * 
 */
public class LOBStatusData {

	private String LOBCode;
	private String statusId;
	/**
	 * TODO method description getLOBCode
	 * @return
	 */
	public String getLOBCode() {
		return LOBCode;
	}

	/**
	 * TODO method description getStatusId
	 * @return
	 */
	public String getStatusId() {
		return statusId;
	}

	/**
	 * TODO method description setLOBCode
	 * @param string
	 */
	public void setLOBCode(String string) {
		LOBCode = string;
	}

	/**
	 * TODO method description setStatusId
	 * @param string
	 */
	public void setStatusId(String string) {
		statusId = string;
	}

}
