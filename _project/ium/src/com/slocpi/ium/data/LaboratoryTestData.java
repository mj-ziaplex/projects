/*
 * Created on Dec 23, 2003
 * package name = com.slocpi.ium.data
 * file name    = LaboratoryTestData.java
 */
package com.slocpi.ium.data;

/**
 * Container for Laboratory Test reference information.
 * @author Engel
 * 
 */
public class LaboratoryTestData {

	private long labId;
	private long testCode;
	private double testFee;
	private String testStatus;	
	
	/**
	 * TODO method description getLabId
	 * @return
	 */
	public long getLabId() {
		return labId;
	}

	/**
	 * TODO method description getTestCode
	 * @return
	 */
	public long getTestCode() {
		return testCode;
	}

	/**
	 * TODO method description getTestFee
	 * @return
	 */
	public double getTestFee() {
		return testFee;
	}

	/**
	 * TODO method description getTestStatus
	 * @return
	 */
	public String getTestStatus() {
		return testStatus;
	}

	/**
	 * TODO method description setLabId
	 * @param l
	 */
	public void setLabId(long l) {
		labId = l;
	}

	/**
	 * TODO method description setTestCode
	 * @param l
	 */
	public void setTestCode(long l) {
		testCode = l;
	}

	/**
	 * TODO method description setTestFee
	 * @param d
	 */
	public void setTestFee(double d) {
		testFee = d;
	}

	/**
	 * TODO method description setTestStatus
	 * @param string
	 */
	public void setTestStatus(String string) {
		testStatus = string;
	}


}
