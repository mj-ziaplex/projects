/*
 * Created on Dec 22, 2003
 * package name = com.slocpi.ium.data
 * file name    = MedicalRecordData.java
 */
package com.slocpi.ium.data;

import java.util.Date;

/**
 * Contains information pertaining to a medical record of an applicant.
 * @author Engel
 */
public class MedicalRecordData {

	private long medicalRecordId;
	private ClientData client;		
	private UserProfileData agent; 	
	private SunLifeOfficeData branch; 
	private StatusData status; 	
	private String prevStatus;
	private String labTestInd;
	private ExaminationPlaceData examinationPlace;
	private LaboratoryData laboratory; 
	private TestProfileData test;
	private Date appointmentDateTime; 
	private Date conductedDate;
	private Date receivedDate;
	private long followUpNumber;
	private Date followUpDate;
	private String remarks;
	private Date validityDate;
	private boolean sevenDayMemoInd;
	private Date sevenDayMemoDate;
	private String reasonForReqt;		
	private SectionData section; 
	private String requestingParty;
	private SunLifeDeptData department;   
	private String chargableTo;
	private boolean paidByAgentInd;
	private double amount;
	private boolean requestForLOAInd;
	private boolean receivedResults;
	private String createdBy;
	private Date createDate;
	private String updatedBy;
	private Date updatedDate;
	private ExaminerData examiner;
	private Date requestedDate;

	private String referenceNo;	
	private String facilitator;
	private StatusData applicationStatus; 	
	private PolicyMedicalRecordData policyMedicalRecordData; 

	/**
	 * @return
	 */
	public UserProfileData getAgent() {
		return agent;
	}

	/**
	 * @return
	 */
	public double getAmount() {
		return amount;
	}

	/**
	 * @return
	 */
	public Date getAppointmentDateTime() {
		return appointmentDateTime;
	}

	/**
	 * @return
	 */
	public SunLifeOfficeData getBranch() {
		return branch;
	}

	/**
	 * @return
	 */
	public String getChargableTo() {
		return chargableTo;
	}

	/**
	 * @return
	 */
	public ClientData getClient() {
		return client;
	}

	/**
	 * @return
	 */
	public Date getConductedDate() {
		return conductedDate;
	}

	/**
	 * @return
	 */
	public Date getCreateDate() {
		return createDate;
	}

	/**
	 * @return
	 */
	public String getCreatedBy() {
		return createdBy;
	}

	/**
	 * @return
	 */
	public SunLifeDeptData getDepartment() {
		return department;
	}

	/**
	 * @return
	 */
	public ExaminationPlaceData getExaminationPlace() {
		return examinationPlace;
	}

	/**
	 * @return
	 */
	public ExaminerData getExaminer() {
		return examiner;
	}

	/**
	 * @return
	 */
	public String getFacilitator() {
		return facilitator;
	}

	/**
	 * @return
	 */
	public Date getFollowUpDate() {
		return followUpDate;
	}

	/**
	 * @return
	 */
	public long getFollowUpNumber() {
		return followUpNumber;
	}

	/**
	 * @return
	 */
	public LaboratoryData getLaboratory() {
		return laboratory;
	}

	/**
	 * @return
	 */
	public String getLabTestInd() {
		return labTestInd;
	}

	/**
	 * @return
	 */
	public long getMedicalRecordId() {
		return medicalRecordId;
	}

	/**
	 * @return
	 */
	public boolean isPaidByAgentInd() {
		return paidByAgentInd;
	}

	/**
	 * @return
	 */
	public PolicyMedicalRecordData getPolicyMedicalRecordData() {
		return policyMedicalRecordData;
	}

	/**
	 * @return
	 */
	public String getReasonForReqt() {
		return reasonForReqt;
	}

	/**
	 * @return
	 */
	public Date getReceivedDate() {
		return receivedDate;
	}

	/**
	 * @return
	 */
	public boolean isReceivedResults() {
		return receivedResults;
	}

	/**
	 * @return
	 */
	public String getReferenceNo() {
		return referenceNo;
	}

	/**
	 * @return
	 */
	public String getRemarks() {
		return remarks;
	}

	/**
	 * @return
	 */
	public Date getRequestedDate() {
		return requestedDate;
	}

	/**
	 * @return
	 */
	public boolean isRequestForLOAInd() {
		return requestForLOAInd;
	}

	/**
	 * @return
	 */
	public String getRequestingParty() {
		return requestingParty;
	}

	/**
	 * @return
	 */
	public SectionData getSection() {
		return section;
	}

	/**
	 * @return
	 */
	public Date getSevenDayMemoDate() {
		return sevenDayMemoDate;
	}

	/**
	 * @return
	 */
	public boolean isSevenDayMemoInd() {
		return sevenDayMemoInd;
	}

	/**
	 * @return
	 */
	public StatusData getStatus() {
		return status;
	}

	/**
	 * @return
	 */
	public TestProfileData getTest() {
		return test;
	}

	/**
	 * @return
	 */
	public String getUpdatedBy() {
		return updatedBy;
	}

	/**
	 * @return
	 */
	public Date getUpdatedDate() {
		return updatedDate;
	}

	/**
	 * @return
	 */
	public Date getValidityDate() {
		return validityDate;
	}

	/**
	 * @param data
	 */
	public void setAgent(UserProfileData data) {
		agent = data;
	}

	/**
	 * @param d
	 */
	public void setAmount(double d) {
		amount = d;
	}

	/**
	 * @param date
	 */
	public void setAppointmentDateTime(Date date) {
		appointmentDateTime = date;
	}

	/**
	 * @param data
	 */
	public void setBranch(SunLifeOfficeData data) {
		branch = data;
	}

	/**
	 * @param string
	 */
	public void setChargableTo(String string) {
		chargableTo = string;
	}

	/**
	 * @param data
	 */
	public void setClient(ClientData data) {
		client = data;
	}

	/**
	 * @param date
	 */
	public void setConductedDate(Date date) {
		conductedDate = date;
	}

	/**
	 * @param date
	 */
	public void setCreateDate(Date date) {
		createDate = date;
	}

	/**
	 * @param string
	 */
	public void setCreatedBy(String string) {
		createdBy = string;
	}

	/**
	 * @param data
	 */
	public void setDepartment(SunLifeDeptData data) {
		department = data;
	}

	/**
	 * @param data
	 */
	public void setExaminationPlace(ExaminationPlaceData data) {
		examinationPlace = data;
	}

	/**
	 * @param data
	 */
	public void setExaminer(ExaminerData data) {
		examiner = data;
	}

	/**
	 * @param string
	 */
	public void setFacilitator(String string) {
		facilitator = string;
	}

	/**
	 * @param date
	 */
	public void setFollowUpDate(Date date) {
		followUpDate = date;
	}

	/**
	 * @param l
	 */
	public void setFollowUpNumber(long l) {
		followUpNumber = l;
	}

	/**
	 * @param data
	 */
	public void setLaboratory(LaboratoryData data) {
		laboratory = data;
	}

	/**
	 * @param string
	 */
	public void setLabTestInd(String string) {
		labTestInd = string;
	}

	/**
	 * @param l
	 */
	public void setMedicalRecordId(long l) {
		medicalRecordId = l;
	}

	/**
	 * @param b
	 */
	public void setPaidByAgentInd(boolean b) {
		paidByAgentInd = b;
	}

	/**
	 * @param data
	 */
	public void setPolicyMedicalRecordData(PolicyMedicalRecordData data) {
		policyMedicalRecordData = data;
	}

	/**
	 * @param string
	 */
	public void setReasonForReqt(String string) {
		reasonForReqt = string;
	}

	/**
	 * @param date
	 */
	public void setReceivedDate(Date date) {
		receivedDate = date;
	}

	/**
	 * @param b
	 */
	public void setReceivedResults(boolean b) {
		receivedResults = b;
	}

	/**
	 * @param string
	 */
	public void setReferenceNo(String string) {
		referenceNo = string;
	}

	/**
	 * @param string
	 */
	public void setRemarks(String string) {
		remarks = string;
	}

	/**
	 * @param date
	 */
	public void setRequestedDate(Date date) {
		requestedDate = date;
	}

	/**
	 * @param b
	 */
	public void setRequestForLOAInd(boolean b) {
		requestForLOAInd = b;
	}

	/**
	 * @param string
	 */
	public void setRequestingParty(String string) {
		requestingParty = string;
	}

	/**
	 * @param data
	 */
	public void setSection(SectionData data) {
		section = data;
	}

	/**
	 * @param date
	 */
	public void setSevenDayMemoDate(Date date) {
		sevenDayMemoDate = date;
	}

	/**
	 * @param b
	 */
	public void setSevenDayMemoInd(boolean b) {
		sevenDayMemoInd = b;
	}

	/**
	 * @param data
	 */
	public void setStatus(StatusData data) {
		status = data;
	}

	/**
	 * @param data
	 */
	public void setTest(TestProfileData data) {
		test = data;
	}

	/**
	 * @param string
	 */
	public void setUpdatedBy(String string) {
		updatedBy = string;
	}

	/**
	 * @param date
	 */
	public void setUpdatedDate(Date date) {
		updatedDate = date;
	}

	/**
	 * @param date
	 */
	public void setValidityDate(Date date) {
		validityDate = date;
	}

	/**
	 * @return
	 */
	public StatusData getApplicationStatus() {
		return applicationStatus;
	}

	/**
	 * @param data
	 */
	public void setApplicationStatus(StatusData data) {
		applicationStatus = data;
	}

	/**
	 * @return
	 */
	public String getPrevStatus() {
		return prevStatus;
	}

	/**
	 * @param string
	 */
	public void setPrevStatus(String string) {
		prevStatus = string;
	}

}
