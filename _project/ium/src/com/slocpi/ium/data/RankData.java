package com.slocpi.ium.data;

import java.util.Date;

/**
 * Container for Rank reference information.
 * @author Engel 
 */
public class RankData {
	
	private long rankCode;
	private String rankDesc;
	private double rankFee;
	private String createdBy;
	private Date createDate;
	private String updatedBy;
	private Date updateDate;

	/**
	 * Sets the rank code attribute.
	 * @param rankCode rank code
	 */ 
	public void setRankCode(long rankCode) {
		this.rankCode = rankCode;
	}
	
	/**
	 * Retrieves the rank code attribute.
	 * @return long rank code
	 */
	public long getRankCode() {
		return rankCode;
	}

	/**
	 * Sets the rank description attribute.
	 * @param rankDesc rank description
	 */ 
	public void setRankDesc(String rankDesc) {
		this.rankDesc = rankDesc;
	}
	
	/**
	 * Retrieves the rank description attribute.
	 * @return String rank description
	 */
	public String getRankDesc() {
		return rankDesc;
	}

	/**
	 * Sets the rank fee attribute.
	 * @param rankFee set consultation fee based on rank
	 */ 
	public void setRankFee(double rankFee) {
		this.rankFee = rankFee;
	}
	
	/**
	 * Retrieves the rank fee attribute.
	 * @return double set consultation fee based on rank
	 */
	public double getRankFee() {
		return rankFee;
	}

	/**
	 * Sets the created date attribute.
	 * @param createDate date when the record was created
	 */ 
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	
	/**
	 * Retrieves the created date attribute.
	 * @return Date date when the record was created
	 */
	public Date getCreateDate() {
		return createDate;
	}

	/**
	 * Sets the created by attribute.
	 * @param createdBy user who created the record
	 */ 
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * Retrieves the created by attribute.
	 * @return String user who created the record
	 */
	public String getCreatedBy() {
		return createdBy;
	}

	/**
	 * Sets the updated date attribute.
	 * @param updateDate date when the record was last updated
	 */ 
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
	
	/**
	 * Retrieves the updated date attribute.
	 * @return Date date when the record was last updated
	 */
	public Date getUpdateDate() {
		return updateDate;
	}

	/**
	 * Sets the updated by attribute.
	 * @param updatedBy user who last updated the record
	 */ 
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	
	/**
	 * Retrieves the updated by attribute.
	 * @return String user who last updated the record
	 */
	public String getUpdatedBy() {
		return updatedBy;
	}

}
