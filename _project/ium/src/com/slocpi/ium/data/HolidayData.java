package com.slocpi.ium.data;

import java.util.Date;

/**
 * Container for Holiday reference information.
 * @author Engel 
 */
public class HolidayData {
	
	private long holidayCode;
	private Date holidayDate;
	private String holidayDesc;
	private String createdBy;
	private Date createDate;
	private String updatedBy;
	private Date updateDate;
		

	/**
	 * Sets the holiday code attribute.
	 * @param holidayCode holiday code
	 */
	public void setHolidayCode(long holidayCode) {
		this.holidayCode = holidayCode;
	}
	
	/**
	 * Retrieves the holdiay code attribute.
	 * @return long holiday code
	 */
	public long getHolidayCode() {
		return holidayCode;
	}

	/**
	 * Sets the holiday description attribute.
	 * @param holidayDesc holiday description
	 */
	public void setHolidayDesc(String holidayDesc) {
		this.holidayDesc = holidayDesc;
	}

	/**
	 * Retrieves the holdiay description attribute.
	 * @return String holiday description
	 */
	public String getHolidayDesc() {
		return holidayDesc;
	}

	/**
	 * Sets the holiday date attribute.
	 * @param holidayDate date of the holiday
	 */
	public void setHolidayDate(Date holidayDate) {
		this.holidayDate = holidayDate;
	}

	/**
	 * Retrieves the holdiay date attribute.
	 * @return String date of the holiday
	 */
	public Date getHolidayDate() {
		return holidayDate;
	}

	/**
	 * Sets the created date attribute.
	 * @param createDate date when the record was created
	 */ 
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	
	/**
	 * Retrieves the created date attribute.
	 * @return Date date when the record was created
	 */
	public Date getCreateDate() {
		return createDate;
	}

	/**
	 * Sets the created by attribute.
	 * @param createdBy user who created the record
	 */ 
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * Retrieves the created by attribute.
	 * @return String user who created the record
	 */
	public String getCreatedBy() {
		return createdBy;
	}

	/**
	 * Sets the updated date attribute.
	 * @param updateDate date when the record was last updated
	 */ 
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
	
	/**
	 * Retrieves the updated date attribute.
	 * @return Date date when the record was last updated
	 */
	public Date getUpdateDate() {
		return updateDate;
	}

	/**
	 * Sets the updated by attribute.
	 * @param updatedBy user who last updated the record
	 */ 
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	
	/**
	 * Retrieves the updated by attribute.
	 * @return String user who last updated the record
	 */
	public String getUpdatedBy() {
		return updatedBy;
	}

}
