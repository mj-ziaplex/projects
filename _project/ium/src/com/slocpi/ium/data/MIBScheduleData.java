/*
 * Created on Dec 23, 2003
 * package name = com.slocpi.ium.data
 * file name    = MIBScheduleData.java
 */
package com.slocpi.ium.data;

import java.util.Date;

/**
 * Container for MIB Schedule reference information.
 * @author Engel
 * 
 */
public class MIBScheduleData {

	private long schedId;
	private String frequency;
	private String day;
	private Date time;
	private String scheduleStatus;

	/**
	 * TODO method description getDay
	 * @return
	 */
	public String getDay() {
		return day;
	}

	/**
	 * TODO method description getFrequency
	 * @return
	 */
	public String getFrequency() {
		return frequency;
	}

	/**
	 * TODO method description getSchedId
	 * @return
	 */
	public long getSchedId() {
		return schedId;
	}

	/**
	 * TODO method description getScheduleStatus
	 * @return
	 */
	public String getScheduleStatus() {
		return scheduleStatus;
	}

	/**
	 * TODO method description getTime
	 * @return
	 */
	public Date getTime() {
		return time;
	}

	/**
	 * TODO method description setDay
	 * @param string
	 */
	public void setDay(String string) {
		day = string;
	}

	/**
	 * TODO method description setFrequency
	 * @param string
	 */
	public void setFrequency(String string) {
		frequency = string;
	}

	/**
	 * TODO method description setSchedId
	 * @param l
	 */
	public void setSchedId(long l) {
		schedId = l;
	}

	/**
	 * TODO method description setScheduleStatus
	 * @param string
	 */
	public void setScheduleStatus(String string) {
		scheduleStatus = string;
	}

	/**
	 * TODO method description setTime
	 * @param date
	 */
	public void setTime(Date date) {
		time = date;
	}

}
