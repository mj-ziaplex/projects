/*
 * Created on Jun 24, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.data.dao.test;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;

import junit.framework.TestCase;

import com.slocpi.ium.data.NotificationTemplateData;
import com.slocpi.ium.data.ProcessConfigData;
import com.slocpi.ium.data.UserProfileData;
import com.slocpi.ium.data.dao.WorkflowDAO;
import com.slocpi.ium.data.util.DataSourceProxy;
import com.slocpi.ium.workflow.NotificationRecipients;
import com.slocpi.ium.workflow.WorkflowItem;
/**
 * @author Alain del Rosario
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class WorkflowDAOTest extends TestCase {

	/**
	 * Constructor for WorkflowDAOTest.
	 * @param arg0
	 */
	public WorkflowDAOTest(String arg0) {
		super(arg0);
	}

	

	public void testWorkflowDAO() {
		WorkflowDAO wfdao = new WorkflowDAO();
		assertNotNull(wfdao);
	}



	public void testGetNotificationInfo() throws SQLException {
		String notifications[] = null;
		notifications = new String[3];	
		
		WorkflowDAO wfdao = new WorkflowDAO();
		WorkflowItem wfitem = new WorkflowItem();
		wfitem.setLOB("GL");
		wfitem.setType("AR");
		wfitem.setPreviousStatus(null);
		wfitem.setCurrentStatus("7");
		notifications = wfdao.getNotificationInfo(wfitem);
		assertNull(notifications[0]);
		assertNull(notifications[1]);
		assertEquals(notifications[2],"150");
		
	}
	

	public void testGetNotificationTemplate() throws SQLException {
		String notifID = "3";
		WorkflowDAO wfdao = new WorkflowDAO();		
		NotificationTemplateData ntd = new NotificationTemplateData();
		ntd = wfdao.getNotificationTemplate(notifID);
		assertNull(ntd.getCreatedBy());
		assertEquals(ntd.getSubject(),"Assessment Request for <Reference No>");
		assertEquals(ntd.getNotificationDesc(),"For Assessment");
		assertEquals(ntd.getSender(),"FCLTOR");
		assertNull(ntd.getMobileNotificationContent());
		assertNull(ntd.getCreatedBy());
		assertNull(ntd.getCreationDate());
		assertNull(ntd.getUpdatedBy());
		assertNull(ntd.getUpdateDate());
		
		
	}
	
	

	public void testGetNotificationRecipients() throws SQLException {
		int three = 3;
		ArrayList recipient = new ArrayList(three);
		recipient.add(0,"c");
		recipient.add(1,"WFUser3");
		recipient.add(2,"DOCTOR2");
		
		WorkflowDAO wfdao = new WorkflowDAO();	
		
		NotificationRecipients nr = new NotificationRecipients();
		nr = wfdao.getNotificationRecipients(recipient,"145","1","IL");
		assertTrue(nr.getEmails().size()==0);
		assertTrue(nr.getMobiles().size()==0);
		// since there is no matching USER_ID in 
		// USERS and USER_PROCESS_PREFERENCES tables
		}




	public void testGetSender() throws SQLException {
		String userID = "INGRID";
		
		WorkflowDAO wfdao = new WorkflowDAO();	
		
		UserProfileData upd = new UserProfileData();
		upd = wfdao.getSender(userID);
		assertEquals(upd.getFirstName(),"INGRID");
		assertEquals(upd.getLastName(),"VILLANUEVA");
		assertEquals(upd.getEmailAddr(),"ingrid.villanueva@pointwest.com.ph");
		
	}

	public void testGetSecondaryRecipients() throws SQLException {
		ArrayList recipients = new ArrayList();
		WorkflowDAO wfdao = new WorkflowDAO();	
		recipients = wfdao.getSecondaryRecipients("5","IL","SUNLIFEHO", "");
		String laman = ((String)recipients.get(0));
		String laman1 = ((String)recipients.get(1));
		assertEquals(laman,"WFUser3");
		assertEquals(laman1,"ADITALO");

	}
	
	

	public void testRetrieveProcessConfig() throws SQLException {
		ProcessConfigData pcd = new ProcessConfigData();
		ArrayList alist = new ArrayList();
		
		WorkflowDAO wfdao = new WorkflowDAO();	
		alist = wfdao.retrieveProcessConfig("AR","PN");
		String strone = ((ProcessConfigData)alist.get(0)).getEventName();
		String strtwo = ((ProcessConfigData)alist.get(1)).getEventName();
		String strthree = ((ProcessConfigData)alist.get(2)).getEventName();
		String strfour = ((ProcessConfigData)alist.get(3)).getEventName();
		assertTrue(alist.size()==4);
		assertEquals(strone,"App forwarded to USD");
		assertEquals(strtwo,"Await requirements");
		assertEquals(strthree,"Submit for assessment");
		assertEquals(strfour,"Submit for review");
	}











	public void testGetProcessConfig() throws SQLException, ParseException {
		ProcessConfigData pcd = new ProcessConfigData();
		ArrayList alist = new ArrayList();
		WorkflowDAO wfdao = new WorkflowDAO();	
		pcd = wfdao.getProcessConfig("55");
		long num = 27;
		long notnum = 6;
		long num2 = 15;
		
		String ds1 = "March 26, 2004";
	    Date updatedate = pcd.getUpdateDate();
		DateFormat df = DateFormat.getDateInstance(DateFormat.LONG);
		String s1 = df.format(updatedate);
		

	
		
		
		assertNull(pcd.getCreateDate());
		assertEquals(pcd.getCreatedBy(),"JDACA");
		assertEquals(pcd.getEventName(),"Waived Requirements");
		assertEquals(pcd.getFromStatus(),num);
		assertEquals(pcd.getLOB(),"IL");
		assertEquals(pcd.getNotification(),notnum);
		assertTrue(pcd.isNotificationInd());
		assertEquals(pcd.getProcessCode(),"NM");
		assertEquals(pcd.getToStatus(),num2);
		assertEquals(s1,ds1);//create date, see process above: Date updatedate = pcd.getUpdateDate();
		assertEquals(pcd.getUpdatedBy(),"REQUW1");
		
	}





	public void testRetrieveAttachmentHelper() throws SQLException {
		ArrayList alist = new ArrayList();
		WorkflowDAO wfdao = new WorkflowDAO();	
		alist = wfdao.retrieveAttachmentHelper("ARTST");
		String reqid = (String)alist.get(0);
		assertEquals(reqid,"927");
	}
	

	public void testHasOrderedRequirements() throws SQLException {
		boolean isittrue = false;
		
		WorkflowDAO wfdao = new WorkflowDAO();	
		isittrue = wfdao.hasOrderedRequirements("ARTST");
		assertTrue(isittrue);
	}

	public void testRetrieveEventId() throws SQLException {
		long num;
		long eventfrstatus = 24;
		long tostatus = 13;
		long eventid = 58;
		
		WorkflowDAO wfdao = new WorkflowDAO();	
		num = wfdao.retrieveEventId(eventfrstatus,tostatus,"PN");
		assertEquals(num,eventid);
		
	}
	
	
	
	
	
	
	
		
	
		
		
		

	

		
		
				

	
	
	

}
