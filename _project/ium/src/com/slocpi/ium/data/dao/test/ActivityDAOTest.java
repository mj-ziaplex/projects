/*
 * Created on Jun 25, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.data.dao.test;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;

import junit.framework.TestCase;

import com.slocpi.ium.data.dao.ActivityDAO;
import com.slocpi.ium.data.util.DataSourceProxy;

/**
 * @author mlua
 *
 *	
 */
public class ActivityDAOTest extends TestCase {

	/**
	 * Constructor for ActivityDAOTest.
	 * @param arg0
	 */
	private Connection conn = null;
	public ActivityDAOTest(String arg0) {
		super(arg0);
	}
	public void testRetrieveActivityLogs() throws Exception {
		conn = (Connection) new DataSourceProxy().getConnection();
		ActivityDAO actlogs = new ActivityDAO(conn);
		/* 
		 * There is no data in activity logs table 
		 * no referece number so arraylist has no data
		 * 
		 */
		ArrayList logs = new ArrayList();
		/*logs = actlogs.retrieveActivityLogs("231321");*/
		assertTrue(logs.size() == 0);	
	}

	public void testRetrieveActivityLogsByPeriod() throws SQLException {
		conn = (Connection) new DataSourceProxy().getConnection();
		ActivityDAO actlogs2 = new ActivityDAO(conn);
		/* 
		 * There is no data in activity logs table 
		 * no date so arraylist has no data
		 * 
		 */
		ArrayList logs = new ArrayList();
		Date sdate = new Date();
		Date edate = new Date();
		/*logs = actlogs2.retrieveActivityLogsByPeriod(sdate,edate);*/
		assertTrue(logs.size() == 0);	
		
	}

	/*
	 * Test for int deleteActivityLogs(Date, Date)
	 */
	public void testDeleteActivityLogsDateDate() throws SQLException {
		conn = (Connection) new DataSourceProxy().getConnection();
		ActivityDAO actlogs3 = new ActivityDAO(conn);
		/* 
		 * There is no data in activity logs table 
		 * no date so arraylist has no data
		 * 
		 */

		Date sdate = new Date();
		Date edate = new Date();
		/*actlogs3.deleteActivityLogs(sdate,edate);*/
		
		ArrayList logs = new ArrayList();
		/*logs = actlogs3.retrieveActivityLogsByPeriod(sdate,edate);*/
		assertTrue(logs.size() == 0);		
	
	}

	/*
	 * Test for void deleteActivityLogs(String)
	 */
	public void testDeleteActivityLogsString() throws Exception {
		conn = (Connection) new DataSourceProxy().getConnection();
		ActivityDAO actlogs4 = new ActivityDAO(conn);
		/* 
		 * There is no data in activity logs table 
		 * no referece num so arraylist has no data
		 * 
		 * Will work if activity datawith reference num 123
		 * is cereated.
		 * 	actlogs4.deleteActivityLogs("123");
		 */
	
		ArrayList logs = new ArrayList();
	/*	logs = actlogs4.retrieveActivityLogs("123");*/
		assertTrue(logs.size() == 0);		
	}
}
