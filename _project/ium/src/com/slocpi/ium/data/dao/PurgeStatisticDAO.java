package com.slocpi.ium.data.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.slocpi.ium.data.PurgeStatisticData;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.DateHelper;

/**
 * Data access object for purge statistics logs.
 * 
 * @author aditalo		Copyright (c) 2004 Pointwest Technologies Corp.
 * @date May 6, 2004
 */
public class PurgeStatisticDAO {

	private static final Logger LOGGER = LoggerFactory.getLogger(PurgeStatisticDAO.class);
	private Connection conn;

	/**
	 * Sets connection upon construction of this class.
	 */
	public PurgeStatisticDAO(Connection _conn) {
		conn = _conn;
	}

	private void closeResources(PreparedStatement ps, ResultSet rs) throws SQLException{
		
		
		try {
			if (ps != null){
				ps.close();
			}
		}
		catch(SQLException e)
		{
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		}
		try {
			if (rs != null){
				rs.close();
			}			
		} catch (SQLException e){
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		}
		
	}
	/**
	 * This method inserts purge statistics log.
	 * 
	 * @param data
	 * @throws SQLException
	 */
	public void insertPurgeStatistic(PurgeStatisticData data) throws SQLException {
		
		LOGGER.info("insertPurgeStatistic start");
		String sql = "INSERT INTO PURGE_STATISTICS" +
					" (PURGE_DATE, START_DATE, END_DATE, CRITERIA, NUMBER_OF_RECORDS, RESULT)" +
					" VALUES(?, ?, ?, ?, ?, ?)";
		PreparedStatement ps = null;
		ResultSet rs = null;
		try{
		ps = conn.prepareStatement(sql);
		ps.setTimestamp(1, DateHelper.sqlTimestamp(data.getPurgeDate()));
		ps.setDate(2, DateHelper.sqlDate(data.getStartDate()));
		ps.setDate(3, DateHelper.sqlDate(data.getEndDate()));
		ps.setString(4, data.getCriteria());
		ps.setLong(5, data.getNumberOfRecords());
		ps.setString(6, data.getResult());
		ps.executeUpdate();
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			  throw e; 
		}finally{
			  this.closeResources(ps, rs);
		}	
		LOGGER.info("insertPurgeStatistic end");
	}
}
