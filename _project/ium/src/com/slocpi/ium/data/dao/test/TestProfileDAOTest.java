/*
 * Created on Jun 25, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.data.dao.test;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import junit.framework.TestCase;

import com.slocpi.ium.data.TestProfileData;
import com.slocpi.ium.data.dao.TestProfileDAO;
import com.slocpi.ium.data.util.DataSourceProxy;


/**
 * @author Alain del Rosario
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class TestProfileDAOTest extends TestCase {

	/**
	 * Constructor for TestProfileDAOTest.
	 * @param arg0
	 */
	public TestProfileDAOTest(String arg0) {
		super(arg0);
	}

	

	public void testTestProfileDAO() {
	}





	public void testSelectTestProfiles() throws SQLException {
		ArrayList alist = new ArrayList();
		Connection conn2 = null;
		conn2 = new DataSourceProxy().getConnection();
		TestProfileDAO tpd = new TestProfileDAO(conn2);
	
		alist = tpd.selectTestProfiles();
		assertTrue(alist.size()==36);
	}
	
	


	public void testRetrieveTestProfile() throws SQLException {
		long id = 69;
		int num = 99;
		int follupnum = 0;
		TestProfileData tpdt = new TestProfileData();
		Connection conn2 = null;
		conn2 = new DataSourceProxy().getConnection();
		TestProfileDAO tpd = new TestProfileDAO(conn2);
		tpdt = tpd.retrieveTestProfile(id);
		assertEquals(tpdt.getTestId(),id);
		assertEquals(tpdt.getTestDesc(),"HIB");
		assertEquals(tpdt.getTestType(),"L");
		assertEquals(tpdt.getValidity(),num);
		assertTrue(!tpdt.isTaxable());
		assertEquals(tpdt.getFollowUpNumber(),follupnum);
		
	}



	public void testInsertTestProfile() {
	}

	public void testUpdateTestProfile() {
	}

}
