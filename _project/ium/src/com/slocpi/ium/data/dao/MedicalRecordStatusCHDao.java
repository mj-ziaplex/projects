package com.slocpi.ium.data.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.StringTokenizer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.IUMConstants;
import com.slocpi.ium.util.NameValuePair;


public class MedicalRecordStatusCHDao extends CodeHelperDao {

	private static final Logger LOGGER = LoggerFactory.getLogger(MedicalRecordStatusCHDao.class);

	private Connection conn;
	
	public MedicalRecordStatusCHDao(Connection conn) throws SQLException {
		this.conn = conn;
	}// MedicalRecordStatusCHDao

	public Collection getCodeValues() throws SQLException {
		return getList(null, null);
	}

	public Collection getCodeValues(String filter) throws SQLException {
		
		LOGGER.info("getCodeValues start");
		ArrayList lob_status = generateFilters(filter);
				
		String lob = "";
		String status = "";
		if (lob_status.size() == 2) {
			lob = (String) lob_status.get(0);
			status = (String) lob_status.get(1);
		}
		LOGGER.info("getCodeValues end");
		return getList(lob, status);
	}

	public Collection getList(String lob, String status) throws SQLException {
		
	LOGGER.info("getList start");
	  Collection list = new ArrayList();
	  String sql = "";

	  boolean isNullStatus = false;
	  if ((status == null) || (status.equals("")) || (status.equals("null"))) {
		isNullStatus = true;
	  }
      
	  boolean isNullLOB = false;
	  if ((lob == null) || (lob.equals("")) || (lob.equals("null"))) {
		isNullLOB= true;
	  }
	  
	  if (!isNullLOB&& !isNullStatus) {
		
		sql = "SELECT " +
				"DISTINCT(S.STAT_ID) AS CODE, " + 
				"S.STAT_DESC AS DESCRIPTION " +
			"FROM " +
				"STATUS S, " +
				"PROCESS_CONFIGURATIONS PC, " +
				"LOB_STATUS LS " +
			"WHERE " +
				"(S.STAT_ID = PC.EVNT_TO_STATUS AND " +
				" S.STAT_ID = LS.STAT_ID AND " +
				" LS.LOB_CODE = ? AND " +
				" S.STAT_TYPE = ? AND " +
				" PC.EVNT_FROM_STATUS = ?) OR " +
				"(S.STAT_ID = ?)" +
			"ORDER BY S.STAT_ID ";
	  }
	  else if (isNullLOB&& !isNullStatus) {
		
		sql = "SELECT " +
				"DISTINCT(S.STAT_ID) AS CODE, " + 
				"S.STAT_DESC AS DESCRIPTION " +
			"FROM " +
				"STATUS S, " +
				"PROCESS_CONFIGURATIONS PC, " +
				"LOB_STATUS LS " +
			"WHERE " +
				"(S.STAT_ID = PC.EVNT_TO_STATUS AND " +
				" S.STAT_ID = LS.STAT_ID AND " +
				" S.STAT_TYPE = ? AND " +
				" PC.EVNT_FROM_STATUS = ?) OR " +
				"(S.STAT_ID = ?)" +
			"ORDER BY S.STAT_ID ";
	  }	
	  else if (!isNullLOB&& isNullStatus) {
		
		sql = "SELECT " +
				"DISTINCT(S.STAT_ID) AS CODE, " + 
				"S.STAT_DESC AS DESCRIPTION " +
			"FROM " +
				"STATUS S, " +
				"PROCESS_CONFIGURATIONS PC, " +
				"LOB_STATUS LS " +
			"WHERE " +
				"S.STAT_ID = PC.EVNT_TO_STATUS AND " +
				"S.STAT_ID = LS.STAT_ID AND " +
				"LS.LOB_CODE = ? AND " +
				"S.STAT_TYPE = ? " +
			"ORDER BY S.STAT_ID ";
	  }
	  else {	  	
		
		sql = "SELECT " +
				"DISTINCT(S.STAT_ID) AS CODE, " + 
				"S.STAT_DESC AS DESCRIPTION " +
			"FROM " +
				"STATUS S, " +
				"PROCESS_CONFIGURATIONS PC, " +
				"LOB_STATUS LS " +
			"WHERE " +
				"S.STAT_ID = PC.EVNT_TO_STATUS AND " +
				"S.STAT_ID = LS.STAT_ID AND " +
				"PC.EVNT_FROM_STATUS IS NULL AND " + //modified by JoyC, 04/29/2004
				"S.STAT_TYPE = ? " +
			"ORDER BY S.STAT_ID ";
	  }
	  
	  try {
		PreparedStatement ps = conn.prepareStatement(sql);
		if (!isNullLOB&& !isNullStatus) {
			ps.setString(1, lob);
			ps.setString(2, IUMConstants.STAT_TYPE_M);
			ps.setString(3, status);
			ps.setString(4, status);
		}
		else if (isNullLOB&& !isNullStatus) {
			ps.setString(1, IUMConstants.STAT_TYPE_M);
			ps.setString(2, status);
			ps.setString(3, status);
		}
		else if (!isNullLOB&& isNullStatus) {
			ps.setString(1, lob);
			ps.setString(2, IUMConstants.STAT_TYPE_M);
		}
		else {
			ps.setString(1, IUMConstants.STAT_TYPE_M);
		}
		ResultSet rs = ps.executeQuery();				    		
		while (rs.next()) {
		  NameValuePair bean = new NameValuePair();
		  bean.setName(rs.getString("DESCRIPTION"));
		  bean.setValue(rs.getString("CODE"));
		  list.add(bean);
		}
	  } 
	  catch (SQLException e) {
		 LOGGER.error(CodeHelper.getStackTrace(e));
		throw e; 
	  }
	  LOGGER.info("getList end");
	  return (list);
	}// getCodeValues


	public Collection getCodeValue(String codeValue) throws SQLException {
		
		LOGGER.info("getCodeValue start");
		Collection list = new ArrayList();
		String sql = "SELECT STAT_ID AS CODE, STAT_DESC AS DESCRIPTION FROM STATUS WHERE STAT_TYPE = ? AND STAT_ID = ?";
		try {	  	
		  PreparedStatement ps = conn.prepareStatement(sql);
		  ps.setString(1, IUMConstants.STAT_TYPE_M);
		  ps.setString(2, codeValue);
		  ResultSet rs = ps.executeQuery();				    		
		  while (rs.next()) {
			NameValuePair bean = new NameValuePair();
			bean.setName(rs.getString("DESCRIPTION"));
			bean.setValue(rs.getString("CODE"));
			list.add(bean);
		  }
		} 
		catch (SQLException e) {
		LOGGER.error(CodeHelper.getStackTrace(e));
		  throw e; 
		}
		LOGGER.info("getCodeValue end");
		return (list);
	}// getCodeValue


	private ArrayList generateFilters (String filter) {
		
		LOGGER.info("generateFilters start");
		ArrayList list = new ArrayList();
		StringTokenizer st = new StringTokenizer(filter,",");
		while (st.hasMoreTokens()) {
			String item = st.nextToken();
			list.add(item);
		}
		LOGGER.info("generateFilters end");
		return list;
	}// generateFilters
	


}