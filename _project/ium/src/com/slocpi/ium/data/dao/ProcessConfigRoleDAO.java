/**
 * ProcessConfigurationRolesDAO.java				Copyright (c) 2004 Pointwest Technologies Corp.
 * TODO DOCUMENT ME!
 * 
 * @author byu	 @date March 18, 2004
 */
package com.slocpi.ium.data.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.slocpi.ium.data.RolesData;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.IUMConstants;


public class ProcessConfigRoleDAO {

	private static final Logger LOGGER = LoggerFactory.getLogger(ProcessConfigRoleDAO.class);
	private Connection conn = null;
	
	/**
	 * 
	 */
	public ProcessConfigRoleDAO(Connection connection) {
		super();
		this.conn = connection;
	}
	
	public void closeDB() {
		
		LOGGER.info("closeDB start");
		try {
			this.conn.close();
		} catch (Exception e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
		}
		LOGGER.info("closeDB end");
	}
	
	private void closeResources(PreparedStatement ps, ResultSet rs) throws SQLException{
		
		
		try {
			if (ps != null){
				ps.close();
			}
		}
		catch(SQLException e)
		{
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		}
		try {
			if (rs != null){
				rs.close();
			}			
		} catch (SQLException e){
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		}
		
	}
	
	public void insertRoles(ArrayList roles, String lob) throws SQLException {
		
		LOGGER.info("insertRoles start 1");
		if (!roles.isEmpty()) {		
			String sql = "insert into PROCESS_CONFIGURATION_ROLES(EVNT_ID,ROLE_CODE,LOB_CODE) values (seq_process_configuration.currval,?,?)";
			PreparedStatement pstmt = null;
			ResultSet rs = null;
			try{
				
				pstmt = conn.prepareStatement(sql);
				Iterator it = roles.iterator();
				while (it.hasNext()) {				
					pstmt.setString(1,(String)it.next());
					pstmt.setString(2,lob);
					pstmt.executeUpdate();
					pstmt.clearParameters();
				}		
			} catch (SQLException e) {
				LOGGER.error(CodeHelper.getStackTrace(e));
				  throw e; 
			}finally{
				  this.closeResources(pstmt, rs);
			}	
			LOGGER.info("insertRoles end 1");
		} 
	}
	
	public void insertRoles(ArrayList roles,long eventId, String lob) throws SQLException { 
		
		LOGGER.info("insertRoles start");
		if (!roles.isEmpty()) {		
			String sql = "insert into PROCESS_CONFIGURATION_ROLES(EVNT_ID,ROLE_CODE,LOB_CODE) values (?,?,?)";
			PreparedStatement pstmt = null;
			ResultSet rs = null;
			try{
				pstmt = conn.prepareStatement(sql);
				Iterator it = roles.iterator();
				while (it.hasNext()) {				
					pstmt.setLong(1,eventId);
					pstmt.setString(2,(String)it.next());
					pstmt.setString(3,lob);
					pstmt.executeUpdate();
					pstmt.clearParameters();
				}
			} catch (SQLException e) {
				LOGGER.error(CodeHelper.getStackTrace(e));
				  throw e; 
			}finally{
				  this.closeResources(pstmt, rs);
			}	
		} 
		LOGGER.info("insertRoles end");
	}
		
	public void updateRoles (ArrayList roles, long eventId, String lob) throws SQLException {
		
		LOGGER.info("updateRoles start");
		ArrayList rolesToUpdate[] = consolidateRoles(roles, lob, eventId);
		deleteRoles(rolesToUpdate[1], eventId, lob);
		insertRoles(rolesToUpdate[0], eventId, lob);
		LOGGER.info("updateRoles end");
	}
	
	private void deleteRoles (ArrayList roles, long eventId, String lob) throws SQLException {

		LOGGER.info("deleteRoles start");
		if (!roles.isEmpty()) {					
			StringBuffer sqlDel = new StringBuffer("delete from PROCESS_CONFIGURATION_ROLES where EVNT_ID = ? and LOB_CODE = ? ");
			if (!roles.isEmpty()) {
				StringBuffer roleList = new StringBuffer();
				Iterator it = roles.iterator();
				while (it.hasNext()) {
					roleList.append("\'"); 
					roleList.append(it.next());
					roleList.append("\',");				 
				}		
				if (roleList.length() > 0) {
					roleList.replace(roleList.length() - 1,roleList.length(),")");				
					sqlDel.append(" and ROLE_CODE in (");			
					sqlDel.append(roleList.toString());				
				}
			}	
			PreparedStatement pstmt = null;
			ResultSet rs = null;
			try{
				pstmt = conn.prepareStatement(sqlDel.toString());
				pstmt.setLong(1,eventId);
				pstmt.setString(2,lob);
				pstmt.executeUpdate();
			} catch (SQLException e) {
				LOGGER.error(CodeHelper.getStackTrace(e));
				  throw e; 
			}finally{
				  this.closeResources(pstmt, rs);
			}
		}
		LOGGER.info("deleteRoles end");
	}
	
	private ArrayList[] consolidateRoles (ArrayList roles, String lob, long eventId) throws SQLException {
		
		LOGGER.info("consolidateRoles start");
		ArrayList rolesToRemove = new ArrayList();				
		StringBuffer sql = new StringBuffer("select ROLE_CODE from PROCESS_CONFIGURATION_ROLES where EVNT_ID = ? and LOB_CODE = ? ");						
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		ArrayList rolesToUpdate [] = {roles,rolesToRemove};
		try{
			pstmt = conn.prepareStatement(sql.toString());
			pstmt.setLong(1,eventId);
			pstmt.setString(2,lob);			
			rs = pstmt.executeQuery();
			String role;
			while (rs.next()) {
				role = rs.getString(1);			
				if ( role != null && roles.contains(role) ) {
					roles.remove(role);
				} else if ( role != null && !"".equals(role) ) {				
					rolesToRemove.add(role);
				}
			} 		
			
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			  throw e; 
		}finally{
			  this.closeResources(pstmt, rs);
		}	
		LOGGER.info("consolidateRoles end");
		return rolesToUpdate;
	}
	
	//ksantos 03252004
	public ArrayList selectAssignedRoles(String lob, long eventId) throws SQLException{
		
		LOGGER.info("selectAssignedRoles start");
		ArrayList roles = new ArrayList();
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT A.ROLE_CODE ROLE_CODE, B.ROLE_DESC ROLE_DESC ");
		sb.append("FROM PROCESS_CONFIGURATION_ROLES A, ROLES B ");
		sb.append("WHERE ");
		sb.append("A.EVNT_ID = ? AND ");
		sb.append("A.LOB_CODE = ? AND ");
		sb.append("A.ROLE_CODE = B.ROLE_CODE ");
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try{
			ps = conn.prepareStatement(sb.toString());
			ps.setLong(1,eventId);
			ps.setString(2,lob);
			rs = ps.executeQuery();
			while(rs.next()){
				RolesData role = new RolesData();
				role.setRolesId(rs.getString("ROLE_CODE"));
				role.setRolesDesc(rs.getString("ROLE_DESC"));
				roles.add(role);
			}
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			  throw e; 
		}finally{
			  this.closeResources(ps, rs);
		}	
		LOGGER.info("selectAssignedRoles end");
		return roles;
	}
	
	//ksantos 03252004
	public ArrayList selectAvailableRoles(String lob, long eventId) throws SQLException{
		
		LOGGER.info("selectAvailableRoles start");
		ArrayList roles = new ArrayList();
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT ROLE_CODE, ROLE_DESC FROM ROLES ");
		sb.append("WHERE ROLE_CODE NOT IN ( ");
		sb.append("SELECT ROLE_CODE FROM PROCESS_CONFIGURATION_ROLES ");
		sb.append("WHERE EVNT_ID = ? AND ");
		sb.append("LOB_CODE = ? ) ");
			
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try{
			ps = conn.prepareStatement(sb.toString());
			ps.setLong(1,eventId);
			ps.setString(2,lob);
			rs = ps.executeQuery();
			while(rs.next()){
				RolesData role = new RolesData();
				role.setRolesId(rs.getString("ROLE_CODE"));
				role.setRolesDesc(rs.getString("ROLE_DESC"));
				roles.add(role);
			}
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			  throw e; 
		}finally{
			  this.closeResources(ps, rs);
		}	
		LOGGER.info("selectAvailableRoles end");
		return roles;
	}
	

	/**
	 * This method returns the list of roles given the TO status and LOB.
	 * 
	 * @param lob
	 * @param type
	 * @param fromStatus
	 * @return boolean
	 * @throws SQLException
	 */
	public ArrayList retrieveAssignToRoles(String frStatus, String toStatus, String lob) throws SQLException {
		
		LOGGER.info("retrieveAssignToRoles start");
		String sql_grp = "";
		String sql_ind = "";
		String sql = "";
		
		if (frStatus != null) {
			//used if LOB is GL
			sql_grp = "SELECT DISTINCT(PCR.ROLE_CODE) AS ROLE " +
					  "FROM " + 
						"PROCESS_CONFIGURATION_ROLES PCR, " +
						"PROCESS_CONFIGURATIONS PC " +
					  "WHERE " +
						"PCR.EVNT_ID = PC.EVNT_ID AND " +
						"PC.EVNT_FROM_STATUS = ? AND " +
						"PC.EVNT_TO_STATUS = ? AND " +
						"PCR.LOB_CODE = ? " +
					  "ORDER BY " +
					   "PCR.ROLE_CODE";
			//used if LOB is IL or PN				   
			sql_ind = "SELECT DISTINCT(PCR.ROLE_CODE) AS ROLE " +
					  "FROM " + 
						"PROCESS_CONFIGURATION_ROLES PCR, " +
						"PROCESS_CONFIGURATIONS PC " +
					  "WHERE " +
						"PCR.EVNT_ID = PC.EVNT_ID AND " +
						"PC.EVNT_FROM_STATUS = ? AND " +
						"PC.EVNT_TO_STATUS = ? AND " +
						"(PCR.LOB_CODE = ? OR PCR.LOB_CODE = ?)" +
					  "ORDER BY " +
						"PCR.ROLE_CODE";
			//used if LOB is null
			sql = "SELECT DISTINCT(PCR.ROLE_CODE)  AS ROLE " +
				  "FROM " + 
					"PROCESS_CONFIGURATION_ROLES PCR, " +
					"PROCESS_CONFIGURATIONS PC " +
				  "WHERE " +
					"PCR.EVNT_ID = PC.EVNT_ID AND " +
					"PC.EVNT_FROM_STATUS = ? AND " +
					"PC.EVNT_TO_STATUS = ? " +
				  "ORDER BY " +
					"PCR.ROLE_CODE";			
		}
		else {
			//used if LOB is GL
			sql_grp = "SELECT DISTINCT(PCR.ROLE_CODE) AS ROLE " +
					  "FROM " + 
						"PROCESS_CONFIGURATION_ROLES PCR, " +
						"PROCESS_CONFIGURATIONS PC " +
					  "WHERE " +
						"PCR.EVNT_ID = PC.EVNT_ID AND " +
						"PC.EVNT_TO_STATUS = ? AND " +
						"PCR.LOB_CODE = ? " +
					  "ORDER BY " +
					   "PCR.ROLE_CODE";
			//used if LOB is IL or PN				   
			sql_ind = "SELECT DISTINCT(PCR.ROLE_CODE) AS ROLE " +
					  "FROM " + 
						"PROCESS_CONFIGURATION_ROLES PCR, " +
						"PROCESS_CONFIGURATIONS PC " +
					  "WHERE " +
						"PCR.EVNT_ID = PC.EVNT_ID AND " +
						"PC.EVNT_TO_STATUS = ? AND " +
						"(PCR.LOB_CODE = ? OR PCR.LOB_CODE = ?)" +
					  "ORDER BY " +
						"PCR.ROLE_CODE";
			//used if LOB is null
			sql = "SELECT DISTINCT(PCR.ROLE_CODE)  AS ROLE " +
				  "FROM " + 
					"PROCESS_CONFIGURATION_ROLES PCR, " +
					"PROCESS_CONFIGURATIONS PC " +
				  "WHERE " +
					"PCR.EVNT_ID = PC.EVNT_ID AND " +
					"PC.EVNT_TO_STATUS = ? " +
				  "ORDER BY " +
					"PCR.ROLE_CODE";		
		}


		if (lob != null) {
			if (lob.equals(IUMConstants.LOB_GROUP_LIFE)) {
				sql = sql_grp;
			}
			else {
				sql = sql_ind;
			}
		}
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList roles = null;
		try{
				
		ps = conn.prepareStatement(sql);
		if (frStatus != null) {
			ps.setString(1, frStatus);
			ps.setString(2, toStatus);
			if (lob != null) {
				if (lob.equals(IUMConstants.LOB_GROUP_LIFE)) {
					ps.setString(3, IUMConstants.LOB_GROUP_LIFE);
				}
				else {
					ps.setString(3, IUMConstants.LOB_INDIVIDUAL_LIFE);
					ps.setString(4, IUMConstants.LOB_PRE_NEED);
				}
			}
		}
		else {
			ps.setString(1, toStatus);
			if (lob != null) {
				if (lob.equals(IUMConstants.LOB_GROUP_LIFE)) {
					ps.setString(2, IUMConstants.LOB_GROUP_LIFE);
				}
				else {
					ps.setString(2, IUMConstants.LOB_INDIVIDUAL_LIFE);
					ps.setString(3, IUMConstants.LOB_PRE_NEED);
				}
			}
		}
		
		rs = ps.executeQuery();
		roles = new ArrayList();
		while (rs.next()) {
			roles.add(rs.getString("ROLE"));
		}
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			  throw e; 
		}finally{
			  this.closeResources(ps, rs);
		}	
		LOGGER.info("retrieveAssignToRoles end");
		return roles;
	}//retrieveAssignToRoles
}

	

