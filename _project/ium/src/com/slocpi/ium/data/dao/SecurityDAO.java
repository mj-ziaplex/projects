/**
 * SecurityDAO.java				Copyright (c) 2004 Pointwest Technologies Corp.
 * TODO DOCUMENT ME!
 * 
 * @author aditalo	 @date Jan 9, 2004
 */
package com.slocpi.ium.data.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.slocpi.ium.data.AccessTemplateData;
import com.slocpi.ium.data.AccessTemplateDetailsData;
import com.slocpi.ium.data.PageFunctionData;
import com.slocpi.ium.data.RolesData;
import com.slocpi.ium.data.UserAccessData;
import com.slocpi.ium.util.CodeHelper;

/**
 * TODO DOCUMENT ME!
 * 
 * @author aditalo		Copyright (c) 2004 Pointwest Technologies Corp.
 * @date Jan 9, 2004
 */
public class SecurityDAO extends BaseDAO {
	private static final Logger LOGGER = LoggerFactory.getLogger(SecurityDAO.class);
	
	public SecurityDAO() {}
	
	/**
	 * @return
	 * @throws Exception
	 */
	public ArrayList selectTemplates() throws SQLException {
		
		LOGGER.info("selectTemplates start");
		ArrayList list = new ArrayList();
		String sql = "SELECT TEMPLT_ID, TEMPLT_DESC, ROLE_ID, ROLE_DESC" +
					" FROM ROLES, ACCESS_TEMPLATES" +
					" WHERE ROLE_CODE = ROLE_ID " +
					" ORDER BY TEMPLT_DESC ";
		PreparedStatement prepStmt = null;
		ResultSet rs = null;
		Connection conn = null;
		
		try {
			conn = getConnection();	
			prepStmt = conn.prepareStatement(sql);
			rs = prepStmt.executeQuery();	
			while (rs.next()) {
				AccessTemplateData data = new AccessTemplateData();
				data.setTemplateID(rs.getLong("TEMPLT_ID"));
				data.setTemplateDesc(rs.getString("TEMPLT_DESC"));
				RolesData role = new RolesData();
				role.setRolesId(rs.getString("ROLE_ID"));
				role.setRolesDesc(rs.getString("ROLE_DESC"));
				data.setRole(role);
				list.add(data);
		}
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			 throw e; 
		}finally{
			  closeResources(conn, prepStmt, rs);
		}	
		LOGGER.info("selectTemplates end");
		return list;
	}
	
	public AccessTemplateData selectTemplate(long templateID) throws SQLException {
		
		LOGGER.info("selectTemplate start");
		AccessTemplateData accessTemplate = new AccessTemplateData();
		String sql = "SELECT TEMPLT_DESC, ROLE_ID, ROLE_DESC" +
					" FROM ROLES, ACCESS_TEMPLATES" +
					" WHERE ROLE_CODE = ROLE_ID" +
					"   AND TEMPLT_ID = ?";
		PreparedStatement prepStmt = null;
		ResultSet rs = null;
		Connection conn = null;
		
		try {
			conn = getConnection();
			prepStmt = conn.prepareStatement(sql);
			prepStmt.setLong(1, templateID);
			rs = prepStmt.executeQuery();
			if (rs.next()) {
				accessTemplate.setTemplateID(templateID);
				accessTemplate.setTemplateDesc(rs.getString("TEMPLT_DESC"));
				RolesData role = new RolesData();
				role.setRolesId(rs.getString("ROLE_ID"));
				role.setRolesDesc(rs.getString("ROLE_DESC"));
				accessTemplate.setRole(role);
				accessTemplate.setTemplateDetails(this.selectTemplateDetails(templateID));
			}
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			  throw e; 
		}finally{
			  closeResources(conn, prepStmt, rs);
		}	
		LOGGER.info("selectTemplate end");
		return accessTemplate;
	}
	
	public ArrayList selectTemplateDetails(long templateID) throws SQLException {
		
		LOGGER.info("selectTemplateDetails start");
		ArrayList list = new ArrayList();
		String sql = "SELECT PA.PA_ID, PA.PAGE_ID, PAGE_DESC, PA.ACC_ID, ACC_DESC, ATD_ACCESS_IND" +
					" FROM PAGES_ACCESS PA, PAGES P, ACCESSES A, ACCESS_TEMPLATE_DETAILS ATD" +
					" WHERE PA.PA_ID = ATD.PA_ID" +
					"   AND PA.PAGE_ID = P.PAGE_ID" +
					"   AND PA.ACC_ID = A.ACC_ID" +
					"   AND TEMPLT_ID = ?" +
					" ORDER BY PAGE_ID, ACC_ID";
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection conn = null;
		
		try {
			conn = getConnection();	
			ps = conn.prepareStatement(sql);
			ps.setLong(1, templateID);
			rs = ps.executeQuery();
			while (rs.next()) {
				AccessTemplateDetailsData details = new AccessTemplateDetailsData();
				PageFunctionData pageFunction = new PageFunctionData();
				pageFunction.setPageAccessId(rs.getLong("PA_ID"));
				pageFunction.setPageID(rs.getLong("PAGE_ID"));
				pageFunction.setPageDescription(rs.getString("PAGE_DESC"));
				pageFunction.setAccessID(rs.getLong("ACC_ID"));
				pageFunction.setAccessDescription(rs.getString("ACC_DESC"));
				details.setTemplateID(templateID);
				details.setPageAccess(pageFunction);
				details.setAccess(rs.getBoolean("ATD_ACCESS_IND"));
				list.add(details);
			}
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			  throw e; 
		}finally{
			  closeResources(conn, ps, rs);
		}	
		LOGGER.info("selectTemplateDetails end");
		return list;
	}

	public void insertAccessTemplate(AccessTemplateData data) throws SQLException {
		
		LOGGER.info("insertAccessTemplate start");
		long sequenceNumber = this.selectTemplateSequence();
		String sql = "INSERT INTO ACCESS_TEMPLATES (TEMPLT_ID, TEMPLT_DESC, ROLE_ID, CREATED_BY, CREATED_DATE)" +
					" VALUES(?, ?, ?, ?, ?)";
		PreparedStatement prepStmt = null;
		
		Connection conn = null;
		
		try {
			conn = getConnection();
			conn.setAutoCommit(false);
			prepStmt = conn.prepareStatement(sql);
			prepStmt.setLong(1,sequenceNumber);
			prepStmt.setString(2,data.getTemplateDesc());
			prepStmt.setString(3, data.getRole().getRolesId());
			prepStmt.setString(4, data.getCreatedBy());
			prepStmt.setTimestamp(5, data.getCreatedDate());
			prepStmt.executeUpdate();
			conn.commit();
			
			this.insertDefaultTemplateDetails(sequenceNumber);
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			if(conn != null){
				conn.rollback();
			}
			throw e; 
		}finally{
			  closeResources(conn, prepStmt, null);
		}	
		LOGGER.info("insertAccessTemplate end");
	}

	private long selectTemplateSequence() throws SQLException {
		
		LOGGER.info("selectTemplateSequence start");
		long sequence = 0;
		String sql = "SELECT SEQ_ACCESS_TEMPLATE.NEXTVAL SEQ_NUM FROM DUAL";
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection conn = null;
		
		try {
			conn = getConnection();	
			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery();
			if (rs.next()) {
				sequence = rs.getLong("SEQ_NUM");
			} else {
				throw new SQLException("Error in retrieving the sequence number.");
			}
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			 throw e; 
		}finally{
			  closeResources(conn, ps, rs);
		}	
		LOGGER.info("selectTemplateSequence end");
		return sequence;
	}

	private void insertDefaultTemplateDetails(long templateID) throws SQLException {
		
		LOGGER.info("insertDefaultTemplateDetails start");
		final String NO_ACCESS = "0"; 
		String sql = "INSERT INTO ACCESS_TEMPLATE_DETAILS" +
					" (TEMPLT_ID, PA_ID, ATD_ACCESS_IND)" +
					" 	SELECT ?, PA_ID, ?" +
					" FROM PAGES_ACCESS" +
					" WHERE PA_ENABLED_IND = '1'";
		PreparedStatement ps = null;
		Connection conn = null;
		
		try {
			conn = getConnection();	
			conn.setAutoCommit(false);
			ps = conn.prepareStatement(sql);
			ps.setLong(1, templateID);
			ps.setString(2, NO_ACCESS);
			ps.executeUpdate();
			conn.commit();
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			if(conn != null){
				conn.rollback();
			}
			 throw e; 
		}finally{
			  closeResources(conn, ps, null);
		}	
		LOGGER.info("insertDefaultTemplateDetails end");
	}
	
	public void updateAccessTemplate(AccessTemplateData data) throws SQLException {
		
		LOGGER.info("updateAccessTemplate start");
		String sql = "UPDATE ACCESS_TEMPLATES" +
					" SET TEMPLT_DESC = ?, ROLE_ID = ?, UPDATED_BY = ?, UPDATED_DATE = SYSDATE" +
					" WHERE TEMPLT_ID = ?";
		PreparedStatement prepStmt = null;
		
		Connection conn = null;
		
		try {
			conn = getConnection();	
			conn.setAutoCommit(false);
			prepStmt = conn.prepareStatement(sql);
			prepStmt.setString(1,data.getTemplateDesc());
			prepStmt.setString(2,data.getRole().getRolesId());
			prepStmt.setString(3,data.getUpdatedBy());
			prepStmt.setLong(4,data.getTemplateID());			
			prepStmt.executeUpdate();
			conn.commit();
			
		if (data.getTemplateDetails().size() > 0) {
			for (int i = 0; i < data.getTemplateDetails().size(); i++) {
				AccessTemplateDetailsData detail = (AccessTemplateDetailsData) data.getTemplateDetails().get(i);
				detail.setUpdatedBy(data.getUpdatedBy());
				if (this.isAccessTemplateDetailExist(detail)) {
					this.updateAccessTemplateDetail(detail);
				
				} else {
					this.insertAccessTemplateDetail(detail);
				
				}
			}
		}
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
				if(conn != null){
					conn.rollback();
				}
			throw e; 
		}finally{
			  closeResources(conn, prepStmt, null);
		}	
		LOGGER.info("updateAccessTemplate end");
	}
	
	public boolean isAccessTemplateExist(AccessTemplateData accessTemplate) throws SQLException {
		
		LOGGER.info("isAccessTemplateExist start");
		boolean returnValue = false;
		String sql = "SELECT TEMPLT_ID FROM ACCESS_TEMPLATES WHERE TEMPLT_ID != ? AND ROLE_ID = ?";		
		
		PreparedStatement prepStmt = null;
		ResultSet rs = null;
		Connection conn = null;
		
		try {
			conn = getConnection();	
			prepStmt = conn.prepareStatement(sql);
			prepStmt.setLong(1, accessTemplate.getTemplateID());
			prepStmt.setString(2, accessTemplate.getRole().getRolesId());
			rs = prepStmt.executeQuery();
		if (rs.next()) {

			returnValue = true;
		}
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			  throw e; 
		}finally{
			  closeResources(conn, prepStmt, rs);
		}	
		LOGGER.info("isAccessTemplateExist end");
		return returnValue;
	}
	
	private void insertAccessTemplateDetail(AccessTemplateDetailsData data) throws SQLException {
		
		LOGGER.info("insertAccessTemplateDetail start");
		String sql = "INSERT INTO ACCESS_TEMPLATE_DETAILS" +
					" (TEMPLT_ID, PA_ID, ATD_ACCESS_IND, UPDATED_BY, UPDATED_DATE)" +
					" VALUES(?, ?, ?, ?, SYSDATE)";
		PreparedStatement ps = null;
		Connection conn = null;
		
		try {
			conn = getConnection();	
			conn.setAutoCommit(false);
			ps = conn.prepareStatement(sql);
			ps.setLong(1, data.getTemplateID());
			ps.setLong(2, data.getPageAccess().getPageAccessId());
			ps.setBoolean(3, data.hasAccess());
			ps.setString(4, data.getUpdatedBy());
			ps.executeUpdate();
			conn.commit();
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			if(conn != null){
				conn.rollback();
			}
			throw e; 
		}finally{
			  closeResources(conn, ps, null);
		}	
		LOGGER.info("insertAccessTemplateDetail end");
	}

	
	private void updateAccessTemplateDetail(AccessTemplateDetailsData data) throws SQLException {
		
		LOGGER.info("updateAccessTemplateDetail start");
		String sql = "UPDATE ACCESS_TEMPLATE_DETAILS" +
					" SET ATD_ACCESS_IND = ?, UPDATED_BY = ?, UPDATED_DATE = SYSDATE" +
					" WHERE TEMPLT_ID = ? AND PA_ID = ?";
		PreparedStatement ps = null;
		Connection conn = null;
		
		try {
			conn = getConnection();	
			conn.setAutoCommit(false);
			ps = conn.prepareStatement(sql);
			ps.setBoolean(1, data.hasAccess());
			ps.setString(2, data.getUpdatedBy());
			ps.setLong(3, data.getTemplateID());
			ps.setLong(4, data.getPageAccess().getPageAccessId());
			ps.executeUpdate();
			conn.commit();
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			if(conn != null){
				conn.rollback();
			}
			throw e; 
		}finally{
			  closeResources(conn, ps, null);
		}	
		LOGGER.info("updateAccessTemplateDetail end");
	}
	
	private boolean isAccessTemplateDetailExist(AccessTemplateDetailsData accessTemplateDetails) throws SQLException {
		
		LOGGER.info("isAccessTemplateDetailExist start");
		boolean returnValue = false;
		String sql = "SELECT ATD_ACCESS_IND FROM ACCESS_TEMPLATE_DETAILS" +
					" WHERE TEMPLT_ID = ? AND PA_ID = ?";		
		PreparedStatement prepStmt = null;
		ResultSet rs = null;
		Connection conn = null;
		
		try {
			conn = getConnection();	
			prepStmt = conn.prepareStatement(sql);
			prepStmt.setLong(1, accessTemplateDetails.getTemplateID());
			prepStmt.setLong(2, accessTemplateDetails.getPageAccess().getPageAccessId());
			rs = prepStmt.executeQuery();
		if (rs.next()) {
			returnValue = true;
		}
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			 throw e; 
		}finally{
			  closeResources(conn, prepStmt, rs);
		}	
		LOGGER.info("isAccessTemplateDetailExist end");
		return returnValue;
	}

	public UserAccessData selectUserPageAccess(String userID, long pageID, long accessID) throws SQLException {
		
		LOGGER.info("selectUserPageAccess start");
		UserAccessData data = null;
		String sql = "SELECT USER_ID, PAGE_ID, ACC_ID, UPA_ACCESS_IND" +
					" FROM USER_PAGE_ACCESS WHERE USER_ID = ? AND PAGE_ID = ? AND ACC_ID = ?";
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection conn = null;
		
		try {
			conn = getConnection();	
			ps = conn.prepareStatement(sql);
			ps.setString(1, userID);
			ps.setLong(2, pageID);
			ps.setLong(3, accessID);
			rs = ps.executeQuery();
		if (rs.next()) {
			data = new UserAccessData();
			data.setUserID(rs.getString("USER_ID"));
			data.setPageID(rs.getLong("PAGE_ID"));
			data.setAccessID(rs.getLong("ACC_ID"));
			data.setAccess(rs.getBoolean("UPA_ACCESS_IND"));
		}
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			  throw e; 
		}finally{
			  closeResources(conn, ps, rs);
		}	
		LOGGER.info("selectUserPageAccess end");
		return data;
	}

	public ArrayList selectUserPageAccess(String userID, long pageID) throws SQLException {
		
		LOGGER.info("selectUserPageAccess start");
		ArrayList list = new ArrayList();
		String sql = "SELECT USER_ID, PAGE_ID, ACC_ID, UPA_ACCESS_IND" +
					" FROM USER_PAGE_ACCESS WHERE USER_ID = ? AND PAGE_ID = ?";
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection conn = null;
		
		try {
			conn = getConnection();	
			ps = conn.prepareStatement(sql);
			ps.setString(1, userID);
			ps.setLong(2, pageID);
			rs = ps.executeQuery();
		while (rs.next()) {
			UserAccessData data = new UserAccessData();
			data.setUserID(rs.getString("USER_ID"));
			data.setPageID(rs.getLong("PAGE_ID"));
			data.setAccessID(rs.getLong("ACC_ID"));
			data.setAccess(rs.getBoolean("UPA_ACCESS_IND"));
			list.add(data);
		}
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			  throw e; 
		}finally{
			  closeResources(conn, ps, rs);
		}	
		LOGGER.info("selectUserPageAccess end");
		return list;
	}

	public ArrayList selectUserPageAccess(String userID) throws SQLException {
		
		LOGGER.info("selectUserPageAccess start");
		ArrayList list = new ArrayList();
		String sql = "SELECT UPA.USER_ID, UPA.PAGE_ID, P.PAGE_DESC, UPA.ACC_ID, A.ACC_DESC, UPA.UPA_ACCESS_IND" +
					" FROM PAGES P, ACCESSES A, USER_PAGE_ACCESS UPA" +
					" WHERE UPA.PAGE_ID = P.PAGE_ID AND UPA.ACC_ID = A.ACC_ID AND UPA.USER_ID = ? order by upa.page_id, upa.acc_id ";
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection conn = null;
		
		try {
			conn = getConnection();	
			ps = conn.prepareStatement(sql);
			ps.setString(1, userID);
			rs = ps.executeQuery();
		while (rs.next()) {
			UserAccessData data = new UserAccessData();
			data.setUserID(rs.getString("USER_ID"));
			data.setPageID(rs.getLong("PAGE_ID"));
			data.setPageDesc(rs.getString("PAGE_DESC"));
			data.setAccessID(rs.getLong("ACC_ID"));
			data.setAccessDesc(rs.getString("ACC_DESC"));
			data.setAccess(rs.getBoolean("UPA_ACCESS_IND"));
			list.add(data);
		}
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			  throw e; 
		}finally{
			  closeResources(conn, ps, rs);
		}	
		LOGGER.info("selectUserPageAccess end");
		return list;
	}

	public void saveUserAccess(ArrayList list) throws SQLException {
		
		LOGGER.info("saveUserAccess start");
		if (list.size() > 0) {
			for (int i = 0; i < list.size(); i++) {
				UserAccessData data = (UserAccessData) list.get(i);
				if (this.isUserAccessExist(data)) {
					this.updateUserAccess(data);
				} else {
					this.insertUserAccess(data);
				}
			}
		}
		LOGGER.info("saveUserAccess end");
	}

	private boolean isUserAccessExist(UserAccessData data) throws SQLException {
		
		LOGGER.info("isUserAccessExist start");
		boolean returnValue = false;
		String sql = "SELECT UPA_ACCESS_IND FROM USER_PAGE_ACCESS WHERE USER_ID = ? AND PAGE_ID = ? AND ACC_ID = ?";
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection conn = null;
		
		try {
			conn = getConnection();	
			ps = conn.prepareStatement(sql);
			ps.setString(1, data.getUserID());
			ps.setLong(2, data.getPageID());
			ps.setLong(3, data.getAccessID());
			rs = ps.executeQuery();
		if (rs.next()) {
			returnValue = true;
		}
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			  throw e; 
		}finally{
			  closeResources(conn, ps, rs);
		}		
		LOGGER.info("isUserAccessExist end");
		return returnValue;
	}

	private void insertUserAccess(UserAccessData data) throws SQLException {
		
		LOGGER.info("insertUserAccess start");
		String sql = "INSERT INTO USER_PAGE_ACCESS" +
					" (USER_ID, PAGE_ID, ACC_ID, UPA_ACCESS_IND, CREATED_BY, CREATED_DATE)" +
					" VALUES (?, ?, ?, ?, ?, SYSDATE)";
		PreparedStatement ps = null;
		Connection conn = null;
		
		try {
			conn = getConnection();	
			conn.setAutoCommit(false);
			ps = conn.prepareStatement(sql);
			ps.setString(1, data.getUserID());
			ps.setLong(2, data.getPageID());
			ps.setLong(3, data.getAccessID());
			ps.setBoolean(4, data.hasAccess());
			ps.setString(5, data.getCreatedBy());
			ps.executeUpdate();
			conn.commit();
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			if(conn != null){
				conn.rollback();
			}
			throw e; 
		}finally{
			  closeResources(conn, ps, null);
		}	
		LOGGER.info("insertUserAccess end");
	}

	private void updateUserAccess(UserAccessData data) throws SQLException {
		
		LOGGER.info("updateUserAccess start");
		String sql = "UPDATE USER_PAGE_ACCESS" +
					" SET UPA_ACCESS_IND = ?, UPDATED_BY = ?, UPDATED_DATE = SYSDATE" +
					" WHERE USER_ID = ? AND PAGE_ID = ? AND ACC_ID = ?";
		PreparedStatement ps = null;
		Connection conn = null;
		
		try {
			conn = getConnection();	
			conn.setAutoCommit(false);
			ps = conn.prepareStatement(sql);
			ps.setBoolean(1, data.hasAccess());
			ps.setString(2, data.getUpdatedBy());
			ps.setString(3, data.getUserID());
			ps.setLong(4, data.getPageID());
			ps.setLong(5, data.getAccessID());
			ps.executeUpdate();
			conn.commit();
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			if(conn != null){
				conn.rollback();
			}
			throw e; 
		}finally{
			  closeResources(conn, ps, null);
		}	
		LOGGER.info("updateUserAccess end");
	}

	public ArrayList selectPageAccess() throws SQLException {
		
		LOGGER.info("selectPageAccess start");
		ArrayList list = new ArrayList();
		String sql = "SELECT PA_ID, PNA.PAGE_ID, PAGE_DESC, PNA.ACC_ID, ACC_DESC, NVL(PA_ENABLED_IND, '0') PA_ENABLED_IND" +
					" FROM PAGES_ACCESS PA," +
					" (SELECT PAGE_ID, PAGE_DESC, ACC_ID, ACC_DESC FROM PAGES, ACCESSES) PNA" +
					" WHERE PNA.PAGE_ID = PA.PAGE_ID (+)" +
					"   AND PNA.ACC_ID = PA.ACC_ID (+)" +
					" ORDER BY PNA.PAGE_ID, PNA.ACC_ID";
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection conn = null;
		
		try {
			conn = getConnection();	
			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery();
		while (rs.next()) {
			PageFunctionData data = new PageFunctionData();
			data.setPageAccessId(rs.getLong("PA_ID"));
			data.setPageID(rs.getLong("PAGE_ID"));
			data.setPageDescription(rs.getString("PAGE_DESC"));
			data.setAccessID(rs.getLong("ACC_ID"));
			data.setAccessDescription(rs.getString("ACC_DESC"));
			data.setEnabled(rs.getBoolean("PA_ENABLED_IND"));
			list.add(data);
		}
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			  throw e; 
		}finally{
			  closeResources(conn, ps, rs);
		}	
		LOGGER.info("selectPageAccess end");
		return list;
	}

	public void savePageAccess(ArrayList list) throws SQLException {
		
		LOGGER.info("savePageAccess start");
		if (list.size() > 0) {
			for (int i = 0; i < list.size(); i++) {
				PageFunctionData data = (PageFunctionData) list.get(i);
				if (this.isPageAccessExist(data)) {
					this.updatePageAccess(data);
				} else {
					this.insertPageAccess(data);
				}
			}
		}
		LOGGER.info("savePageAccess end");
	}

	private boolean isPageAccessExist(PageFunctionData data) throws SQLException {
		
		LOGGER.info("isPageAccessExist start");
		boolean returnValue = false;
		String sql = "SELECT PA_ID FROM PAGES_ACCESS WHERE PAGE_ID = ? AND ACC_ID = ?";
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection conn = null;
		
		try {
			conn = getConnection();	
			ps = conn.prepareStatement(sql);
			ps.setLong(1, data.getPageID());
			ps.setLong(2, data.getAccessID());
			rs = ps.executeQuery();
		if (rs.next()) {
			returnValue = true;
		}
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			  throw e; 
		}finally{
			  closeResources(conn, ps, rs);
		}	
		LOGGER.info("isPageAccessExist end");
		return returnValue;
	}

	private void insertPageAccess(PageFunctionData data) throws SQLException {
		
		LOGGER.info("insertPageAccess start");
		String sql = "INSERT INTO PAGES_ACCESS" +
					" (PA_ID, PAGE_ID, ACC_ID, PA_ENABLED_IND)" +
					" VALUES (SEQ_PAGES_ACCESS.NEXTVAL, ?, ?, ?)";
		PreparedStatement ps = null;
		Connection conn = null;
			
		try {
			conn = getConnection();	
			conn.setAutoCommit(false);
			ps = conn.prepareStatement(sql);
			ps.setLong(1, data.getPageID());
			ps.setLong(2, data.getAccessID());
			ps.setBoolean(3, data.isEnabled());
			ps.executeUpdate();
			conn.commit();
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			if(conn != null){
				conn.rollback();
			}
			throw e; 
		}finally{
			  closeResources(conn, ps, null);
		}	
		LOGGER.info("insertPageAccess end");
	}

	private void updatePageAccess(PageFunctionData data) throws SQLException {
		
		LOGGER.info("updatePageAccess start");
		String sql = "UPDATE PAGES_ACCESS" +
					" SET PA_ENABLED_IND = ?" +
					" WHERE PA_ID = ?";
		PreparedStatement ps = null;
		Connection conn = null;
		
		try {
			conn = getConnection();	
			conn.setAutoCommit(false);
			ps = conn.prepareStatement(sql);
			ps.setBoolean(1, data.isEnabled());
			ps.setLong(2, data.getPageAccessId());
			ps.executeUpdate();
			conn.commit();
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			if(conn != null){
				conn.rollback();
			}
			throw e; 
		}finally{
			  closeResources(conn, ps, null);
		}	
		LOGGER.info("updatePageAccess end");
	}



	public void addRoleToUser(String userId, String roleCode) throws SQLException{
		
		LOGGER.info("addRoleToUser start");
		String sql = "INSERT INTO USER_ROLES (ROLE_CODE,USER_ID) VALUES (?,?)";
		PreparedStatement ps = null;
		Connection conn = null;
		
		try {
			conn = getConnection();	
			conn.setAutoCommit(false);
			ps = conn.prepareStatement(sql);
			ps.setString(1, roleCode);
			ps.setString(2, userId);
			ps.executeUpdate();
			conn.commit();
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			if(conn != null){
				conn.rollback();
			}
			throw e; 
		}finally{
			  closeResources(conn, ps, null);
		}	
		LOGGER.info("addRoleToUser end");
	}
	
	public void removeRoleFromUser(String userId, String roleCode) throws SQLException{
		
		LOGGER.info("removeRoleFromUser start");
		String sql = "DELETE FROM USER_ROLES WHERE ROLE_CODE=? AND USER_ID=?";
		PreparedStatement ps = null;
		Connection conn = null;
		ResultSet rs = null;
		
		try {
			conn = getConnection();	
			conn.setAutoCommit(false);
			ps = conn.prepareStatement(sql);
			ps.setString(1, roleCode);
			ps.setString(2, userId);
			ps.executeUpdate();
			conn.commit();
		
		// Delete event id associated with the role deleterd for notification
		sql = "Select evnt_id from process_configuration_roles " +
			  "where " +
			  "role_code = ? and evnt_id not in " +
			  "(Select evnt_id from process_configuration_roles where role_code != ? and role_code in (Select role_code from user_roles where user_id = ?))";
		
			ps = conn.prepareStatement(sql);
			ps.setString(1, roleCode);
			ps.setString(2, roleCode);
			ps.setString(3, userId);
			rs = ps.executeQuery();
			ArrayList events = new ArrayList();
		
		while(rs.next()){
			String eventId = rs.getString("evnt_id");
			events.add(eventId);		
		}
		
		for (int i=0; i<events.size(); i++){
			String deleteEvent = (String)events.get(i);
			sql = "Delete from user_process_preferences where user_id = ? and evnt_id = ?";
			ps = conn.prepareStatement(sql);
			ps.setString(1, userId);
			ps.setString(2, deleteEvent);
			ps.executeUpdate();
			conn.commit();
		}
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			if(conn != null){
				conn.rollback();
			}
			throw e; 
		}finally{
			  closeResources(conn, ps, rs);
		}	
		LOGGER.info("removeRoleFromUser end");
	}
	
	public long getTemplateId(String roleCode) throws SQLException{
		
		LOGGER.info("getTemplateId start");
		String sql = "SELECT TEMPLT_ID FROM ACCESS_TEMPLATES WHERE ROLE_ID=?";
		PreparedStatement ps = null;
		ResultSet rs = null;
		long templateId=0;
		Connection conn = null;
		
		try {
			conn = getConnection();	
			ps = conn.prepareStatement(sql);
			ps.setString(1, roleCode);
			rs = ps.executeQuery();
		
		while(rs.next()){
			templateId = rs.getLong("TEMPLT_ID");
		}
		
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			  throw e; 
		}finally{
			  closeResources(conn, ps, rs);
		}	
		LOGGER.info("getTemplateId end");
		return templateId;
	}

	public boolean hasOtherAccess(String userID, long pageAccessID, long deletedTemplateID) throws SQLException {
		
		LOGGER.info("hasOtherAccess start");
    	boolean returnValue = false;
		String sql = "SELECT AT.TEMPLT_ID" +
					" FROM USER_ROLES UR, ACCESS_TEMPLATES AT, ACCESS_TEMPLATE_DETAILS ATD" +
					" WHERE AT.TEMPLT_ID != ?" +
					" AND AT.ROLE_ID = UR.ROLE_CODE" +
					" AND ATD.TEMPLT_ID = AT.TEMPLT_ID" +
					" AND ATD.ATD_ACCESS_IND = '1'" +
					" AND UR.USER_ID = ?" +
					" AND ATD.PA_ID = ?";
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection conn = null;
		
		try {
			conn = getConnection();	
			ps = conn.prepareStatement(sql);
			ps.setLong(1, deletedTemplateID);
			ps.setString(2, userID);
			ps.setLong(3, pageAccessID);
			rs = ps.executeQuery();
		if (rs.next()) {
			returnValue = true;
		} 
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			  throw e; 
		}finally{
			  closeResources(conn, ps, rs);
		}	
		LOGGER.info("hasOtherAccess end");
		return returnValue;
	}

}
