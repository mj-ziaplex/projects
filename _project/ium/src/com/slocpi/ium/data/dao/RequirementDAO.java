/*
 * Created on Dec 18, 2003
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.data.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.slocpi.ium.data.RequirementData;
import com.slocpi.ium.data.RequirementFormData;
import com.slocpi.ium.data.RequirementFormFieldData;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.DateHelper;

/**
 * @author Cris
 * Container for SQLs performed on a requirement
 */
public class RequirementDAO extends BaseDAO{

	private static final Logger LOGGER = LoggerFactory.getLogger(RequirementDAO.class);
	private Connection conn = null;

	public RequirementDAO() {}
	
	
	/**
	 * Retrieves Requirement Code given the Requirement Form ID
	 * @param reqtId
	 * @return
	 * @throws SQLException
	 */
	public String retrieveRequirementCode(String reqtId) throws SQLException{
		
		LOGGER.info("retrieveRequirementCode start");
		String reqtCode = "";
		String sql = "SELECT REQT_CODE FROM REQUIREMENTS WHERE REQT_FORM_ID = ?";
		
		PreparedStatement ps  = null;
		ResultSet rs = null;
		Connection conn = null;
		
		try {
			conn = getConnection();	
			ps = conn.prepareStatement(sql);
			ps.setString(1, reqtId);
			rs = ps.executeQuery();
			if (rs.next()) {
				reqtCode = rs.getString("REQT_CODE");
			}
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			  throw e; 
		} finally {
			  closeResources(conn, ps, rs);
		}
		LOGGER.info("retrieveRequirementCode end");
		return reqtCode;
	}
	
	/**
	 * Retrieves a requirement record given the requirement code;
	 * @param code
	 * @return
	 * @throws SQLException
	 */
	public RequirementData retrieveRequirement(final String code) throws SQLException {
		
		LOGGER.info("retrieveRequirement start");
		RequirementData data = null;
		String sql = "SELECT REQT_DESC, REQT_LEVEL, REQT_VALIDITY, REQT_FORM_IND, REQT_FORM_ID, REQT_FOLLOW_UP_NUM" +
					" FROM REQUIREMENTS WHERE REQT_CODE = ?";
		
		PreparedStatement ps  = null;
		ResultSet rs = null;
		Connection conn = null;
		
		try {
			conn = getConnection();	
			ps = conn.prepareStatement(sql);
			ps.setString(1, code);
			rs = ps.executeQuery();
			if (rs.next()) {
				data = new RequirementData();
				data.setReqtCode(code);
				data.setReqtDesc(rs.getString("REQT_DESC"));
				data.setLevel(rs.getString("REQT_LEVEL"));
				data.setValidity(rs.getLong("REQT_VALIDITY"));
				data.setFormIndicator(rs.getString("REQT_FORM_IND"));
				data.setFormID(rs.getLong("REQT_FORM_ID"));
				data.setFollowUpNum(rs.getInt("REQT_FOLLOW_UP_NUM"));
			}
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			  throw e; 
		} finally {
			  closeResources(conn, ps, rs);
		}
		LOGGER.info("retrieveRequirement end");
		return data;
	}

	/**
	 * Retrieves the requirement records.
	 * @return Array of requirementData.
	 * @throws SQLException
	 */
	public ArrayList retrieveRequirements() throws SQLException {
		
		LOGGER.info("retrieveRequirements start 1");
		ArrayList list = new ArrayList();
		String sql = "SELECT REQT_CODE, REQT_DESC, REQT_LEVEL, REQT_VALIDITY, REQT_FORM_IND, REQT_FORM_ID, REQT_FOLLOW_UP_NUM" +
					" FROM REQUIREMENTS ORDER BY REQT_CODE";
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection conn = null;
		
		try {
			conn = getConnection();	
			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery();
			while (rs.next()) {
				RequirementData data = new RequirementData();
				data.setReqtCode(rs.getString("REQT_CODE"));
				data.setReqtDesc(rs.getString("REQT_DESC"));
				data.setLevel(rs.getString("REQT_LEVEL"));
				data.setValidity(rs.getLong("REQT_VALIDITY"));
				data.setFormIndicator(rs.getString("REQT_FORM_IND"));
				data.setFormID(rs.getLong("REQT_FORM_ID"));
				data.setFollowUpNum(rs.getInt("REQT_FOLLOW_UP_NUM"));
				list.add(data);
			}
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			  throw e; 
		}finally{
			  closeResources(conn, ps, rs);
		}
		LOGGER.info("retrieveRequirements end 1");
		return list;
	}
	
	/**
	 * Retrieves the requirement records.
	 * @return Array of requirementData.
	 * @throws SQLException
	 */
	/*public ArrayList retrieveRequirements(String whereClause) throws SQLException {
		
		LOGGER.info("retrieveRequirements start 2");
		ArrayList list = new ArrayList();
		String sql = "SELECT REQT_CODE, REQT_DESC, REQT_LEVEL, REQT_VALIDITY, REQT_FORM_IND, REQT_FORM_ID, REQT_FOLLOW_UP_NUM" +
					" FROM REQUIREMENTS ORDER BY REQT_CODE ";
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try {
			ps = conn.prepareStatement(sql+whereClause);
			rs = ps.executeQuery();
			while (rs.next()) {
				RequirementData data = new RequirementData();
				data.setReqtCode(rs.getString("REQT_CODE"));
				data.setReqtDesc(rs.getString("REQT_DESC"));
				data.setLevel(rs.getString("REQT_LEVEL"));
				data.setValidity(rs.getLong("REQT_VALIDITY"));
				data.setFormIndicator(rs.getString("REQT_FORM_IND"));
				data.setFormID(rs.getLong("REQT_FORM_ID"));
				data.setFollowUpNum(rs.getInt("REQT_FOLLOW_UP_NUM"));
				list.add(data);
			}
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			  throw e; 
		} finally {
			  this.closeResources(ps, rs);
		}
		LOGGER.info("retrieveRequirements end 2");
		return list;
	}*/

	/**
	 * 
	 * Retrieves a requirementForm record given the formId.
	 * @param id
	 * @return
	 * @throws SQLException
	 */
	public RequirementFormData retrieveRequirementForm(long id) throws SQLException {
		
		LOGGER.info("retrieveRequirementForm start");
		RequirementFormData data = new RequirementFormData();
		String sql = "SELECT RF_NAME, RF_TEMPLATE_FORMAT, RF_TEMPLATE_NAME, RF_STATUS, REQT_WMS_FORM_ID " +
					" FROM REQUIREMENT_FORMS WHERE RF_ID = ?";
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection conn = null;
		
		try {
			conn = getConnection();	
			ps = conn.prepareStatement(sql);
			ps.setLong(1, id);
			rs = ps.executeQuery();
			if (rs.next()) {
				data.setFormId(id);
				data.setFormName(rs.getString("RF_NAME"));
				data.setTemplateFormat(rs.getString("RF_TEMPLATE_FORMAT"));
				data.setTemplateName(rs.getString("RF_TEMPLATE_NAME"));
				data.setStatus(rs.getBoolean("RF_STATUS"));
				data.setWmsFormId(rs.getString("REQT_WMS_FORM_ID"));
			}
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			  throw e; 
		}finally{
			  closeResources(conn, ps, rs);
		}
		LOGGER.info("retrieveRequirementForm end");
		return data;
	}
	
	/**
	 * Retrieves the requirementForm fields.
	 * @param id
	 * @return
	 * @throws SQLException
	 */
	
	public ArrayList retrieveRequirementFormFields(long id) throws SQLException {
		
		LOGGER.info("retrieveRequirementFormFields start");
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT ")
			.append("  RFF_ID")
			.append(", FF_ID")
			.append(", RFF_PAGE")
			.append(", RFF_POSX")
			.append(", RFF_POSY")
			.append(" from")
			.append("  REQUIREMENT_FORM_FIELDS")
			.append(" where")
			.append("  RF_ID = ?");
			
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList reqtFormFields = new ArrayList();
		Connection conn = null;
		
		try {
			conn = getConnection();	
			ps = conn.prepareStatement(sql.toString());
			ps.setLong(1, id);
			rs = ps.executeQuery();
			
			while (rs.next()) {
				RequirementFormFieldData reqtFields = new RequirementFormFieldData();
				reqtFields.setFieldName(rs.getString("RFF_ID"));
				reqtFields.setFieldValueType(rs.getInt("FF_ID"));
				reqtFields.setFieldPage(rs.getInt("RFF_PAGE"));
				reqtFields.setFieldPosX(rs.getFloat("RFF_POSX"));
				reqtFields.setFieldPosY(rs.getFloat("RFF_POSY"));
				reqtFormFields.add(reqtFields);	
			}
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			  throw e;
		} finally {
			  closeResources(conn, ps, rs);
		}
		LOGGER.info("retrieveRequirementFormFields end");
		return (reqtFormFields);
	}
	
	/**
	 * Inserts a requirement record into the Database. 
	 * @param data
	 * @throws SQLException
	 */
	public void insertRequirement(RequirementData data) throws SQLException{
		
		LOGGER.info("insertRequirement start");
		StringBuffer sql = new StringBuffer("INSERT INTO REQUIREMENTS ");
		sql.append("(REQT_CODE, REQT_DESC, REQT_LEVEL, REQT_VALIDITY, REQT_FORM_IND, REQT_FORM_ID, REQT_FOLLOW_UP_NUM, CREATED_BY, CREATED_DATE, REQT_TEST_DATE_INDICATOR ) ")
			.append(" VALUES ( ?,?,?,?,?,?,?,?,?,? )");
		
		PreparedStatement ps = null;
		Connection conn = null;
		
		try {
			conn = getConnection();	
			conn.setAutoCommit(false);
			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, data.getReqtCode().toUpperCase());
			ps.setString(2, data.getReqtDesc().toUpperCase());
			ps.setString(3, data.getLevel());
			ps.setLong(4, data.getValidity());
			
			boolean indicator = false;
			if( data.getFormIndicator().equals("1") ){
				indicator=true;
			}
			
			ps.setBoolean(5, indicator);
			if(indicator==true){
				ps.setLong(6, data.getFormID());
			} else {
				ps.setString(6,null);
			}
			
			ps.setLong(7, data.getFollowUpNum());
			ps.setString(8, data.getCreatedBy());
			ps.setTimestamp(9, DateHelper.sqlTimestamp(data.getCreateDate()) );
			ps.setString(10, data.getTestDateIndicator());
			
			ps.executeUpdate();
			conn.commit();
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			if(conn != null){
				conn.rollback();
			}
			throw e; 
		}finally{
			 closeResources(conn, ps, null);
		}
		LOGGER.info("insertRequirement end");
	}
	
	/**
	 * Updates a requirement record in the database.
	 * @param data
	 * @throws SQLException
	 */
	
	public void updateRequirement(RequirementData data) throws SQLException{
		
		LOGGER.info("updateRequirement start");
		StringBuffer sql = new StringBuffer("UPDATE REQUIREMENTS SET");
			sql.append(" REQT_DESC=?,")
			.append(" REQT_LEVEL=?,")
			.append(" REQT_VALIDITY=?,")
			.append(" REQT_FORM_IND=?,")
			.append(" REQT_FORM_ID=?,")
			.append(" REQT_FOLLOW_UP_NUM=?,")
			.append(" UPDATED_BY=?,")
			.append(" UPDATED_DATE=?,")
			.append(" REQT_TEST_DATE_INDICATOR=?")
			.append(" WHERE REQT_CODE=?");
		
		PreparedStatement ps = null;
		Connection conn = null;
		
		try {
			conn = getConnection();
			conn.setAutoCommit(false);
			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, data.getReqtDesc().toUpperCase());
			ps.setString(2, data.getLevel());
			ps.setLong(3, data.getValidity());
			
			boolean indicator = false; 
			if( data.getFormIndicator().equals("1") ){
				indicator=true;
			}
			
			ps.setBoolean(4, indicator);
			if(indicator==true){
				ps.setLong(5, data.getFormID());
			} else {
				ps.setString(5,null);
			}
			
			ps.setLong(6, data.getFollowUpNum());
			ps.setString(7, data.getCreatedBy());
			ps.setTimestamp(8, DateHelper.sqlTimestamp(data.getCreateDate()) );
			ps.setString(9, data.getTestDateIndicator());
			ps.setString(10, data.getReqtCode());
			
			ps.executeUpdate();
			conn.commit();
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			if(conn != null){
				conn.rollback();
			}
			throw e; 
		}
			closeResources(conn, ps, null);
		
		LOGGER.info("updateRequirement end");
		
	}
	
	public String checkTestDateIndicator(String requirementCode) throws SQLException{
		
		LOGGER.info("checkTestDateIndicator start");
		String sql = "SELECT REQT_TEST_DATE_INDICATOR FROM REQUIREMENTS WHERE REQT_CODE = ?";
		String tdIndicator = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection conn = null;
		
		try {
			conn = getConnection();	
			ps = conn.prepareStatement(sql);
			ps.setString(1, requirementCode);
			
			rs = ps.executeQuery();
			if (rs.next()){
				tdIndicator = rs.getString("REQT_TEST_DATE_INDICATOR");
			}
			
			if (tdIndicator == null){
				tdIndicator = "0";
			}
			
			rs.close();
			ps.close();
			
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			  throw e; 
		} finally {
			  closeResources(conn, ps, rs);
		}
		LOGGER.info("checkTestDateIndicator end");
		return tdIndicator;
	}
}
