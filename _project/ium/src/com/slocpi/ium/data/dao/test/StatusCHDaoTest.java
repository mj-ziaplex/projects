/*
 * Created on Jun 24, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.data.dao.test;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import junit.framework.TestCase;

import com.slocpi.ium.data.StatusData;
import com.slocpi.ium.data.dao.StatusCHDao;
import com.slocpi.ium.data.util.DataSourceProxy;
import com.slocpi.ium.util.NameValuePair;

/**
 * @author Jeffrey Canlas
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class StatusCHDaoTest extends TestCase {
	public Connection conn = null;
	
	public StatusCHDaoTest(String arg0) {
		super(arg0);
	}

	public void testconvertStatus() throws SQLException {
		/*TEST FOR public Collection convertStatus()
		 * test if the data in the dataset are the
		 * same as the one in the database. 
		 * Will check the first result and see 
		 * if it is the same from the database
		*/	
			
	   conn = new DataSourceProxy().getConnection();
	   StatusData list = new StatusData();
	   StatusCHDao dao = new StatusCHDao();
	    
	   list = dao.convertStatus("IL", "FA");
		
	   long statid = list.getStatusId();
	   String statdesc = list.getStatusDesc();
	   System.out.println(statid);
	   System.out.println(statdesc);
	   
	   assertTrue("test 1", statid == 3);
	   assertTrue("test 2", statdesc.equals("For Facilitator's Action"));	
	}
	
	public void testtransalteStatus() throws SQLException {
		/*TEST FOR public Collection translateStatus()
	 	* test if the data in the dataset are the
	 	* same as the one in the database. 
	 	* Will check the first result and see 
	 	* if it is the same from the database
		*/	
			
   	conn = new DataSourceProxy().getConnection();
   	StatusData list2 = new StatusData();
   	StatusCHDao dao2 = new StatusCHDao();
	    
   	list2 = dao2.translateStatus("IL", 4);
		
   	long statid2 = list2.getStatusId();
   	String statdesc2 = list2.getStatusDesc();
   	System.out.println(statid2);
   	System.out.println(statdesc2);
	   
   	assertTrue("test 3", statid2 == 4);
   	assertTrue("test 4", statdesc2.equals("AS"));	
   }

   public void testgetCodeValue() throws SQLException {
	
		  /*TEST FOR public Collection getCodeValue(String lob, String objectType)
		   * Test connection and see whether SQL statement 
		   * produced a result.  the entered sql statement
		   * is valid (tables and fields are existing)
		   * thus resulting arraylist should be 
		   * greater than zero. 
		  */
	
		  conn = new DataSourceProxy().getConnection();
		  ArrayList list3 = new ArrayList();
		  StatusCHDao dao3 = new StatusCHDao();
	    
		  list3 = (ArrayList)dao3.getCodeValue("IL", "AR");
			
		  assertTrue("test 5", list3.size()>0);
	    
		  /*TEST FOR public Collection getCodeValues()
		   * test if the data in the dataset are the
		   * same as the one in the database. 
		   * Will check the first result and see 
		   * if it is the same from the database
		  */	
	
		  String desc3 = ((NameValuePair)list3.get(0)).getName();
		  String code3 = ((NameValuePair)list3.get(0)).getValue();
		  System.out.println(desc3);
		  System.out.println(code3);
		  assertTrue("test 5.1",desc3.equals("Approved"));
		  assertTrue("test 5.2",code3.equals("7"));
	   }
	public void testgetCodeValue2() throws SQLException{
	   
		   /*TEST FOR public Collection getCodeValue(String codeValue)
			* Test connection and see whether SQL statement 
			* produced a result.  the entered sql statement
			* is invalid (tables and fields are existing but 
			* passed data not in database)
			* thus resulting arraylist should be 
			* zero. 
		   */
		   
		   ArrayList list4 = new ArrayList();
		   StatusCHDao dao4 = new StatusCHDao();
	    
		   list4 = (ArrayList)dao4.getCodeValue("54");
			
		   assertTrue("test 6", list4.size()==0);
		}
	
	

}
