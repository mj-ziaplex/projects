package com.slocpi.ium.data.dao;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.slocpi.ium.data.ActivityLogData;
import com.slocpi.ium.data.StatusData;
import com.slocpi.ium.data.TurnAroundTimeData;
import com.slocpi.ium.data.TurnAroundTimeLOBData;
import com.slocpi.ium.data.TurnAroundTimeParam;
import com.slocpi.ium.data.UserProfileData;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.DateHelper;

/**
 * Data Access Object of the Documents in Assessment Request. Access the FOLDER_DOCUMENTS table.
 * @author Engel
 * 
 */
public class ActivityDAO {

	static final Logger LOGGER = LoggerFactory.getLogger(ActivityDAO.class);
	private Connection conn;
	
	
	public ActivityDAO(Connection _conn) {
		conn = _conn;
	}
	
	public void closeDB() {
		
		LOGGER.info(" closeDB start");
		try {
			this.conn.close();
		} catch (Exception e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
		}
		LOGGER.info(" closeDB end");
	}
    
	
	public ArrayList retrieveActivityLogs(String refNo) throws Exception {
		
		LOGGER.info("retrieveActivityLogs start");
		String sql = "SELECT" +
						" AL.ACT_DATETIME as ACTIVITY_DATE, " +
                        " AL.ACT_ID as STAT_ID, " +
						" ST.STAT_DESC as STAT_DESC, " +
						" AL.USER_ID as USER_ID, " +
						" U.USR_LAST_NAME as LAST_NAME, " +
						" U.USR_FIRST_NAME as FIRST_NAME, " +
						" U.USR_MIDDLE_NAME as MIDDLE_NAME, " +
						" TO_CHAR(AL.ACT_ELAPSE_TIME, '90.99') as LAPSE_TIME, " +
						" AL.ACT_ASSIGNED_TO as ASSIGNED_TO " +
					"FROM" +
						" ASSESSMENT_REQUESTS AR, " +
						" ACTIVITY_LOGS AL, " +
						" STATUS ST, " +
						" USERS U " +
					"WHERE" +
						" AL.UAR_REFERENCE_NUM = AR.REFERENCE_NUM AND " +
						" AL.USER_ID = U.USER_ID AND " +
                        " AL.STAT_ID = ST.STAT_ID AND " +
						" AR.REFERENCE_NUM = ? " +
					"ORDER BY" +
						" AL.ACT_DATETIME DESC ";
		ResultSet rs = null;
		PreparedStatement ps = null;
		try {
			
			ps = conn.prepareStatement(sql);
			ps.setString(1, refNo);
			rs = ps.executeQuery();

			ArrayList list = new ArrayList();
			while (rs.next()) {
				ActivityLogData activityData = new ActivityLogData();
				
				StatusData status = new StatusData();
				status.setStatusId(rs.getLong("STAT_ID"));
				status.setStatusDesc(rs.getString("STAT_DESC"));
				
				UserProfileData user = new UserProfileData();
				user.setUserId(rs.getString("USER_ID"));
				user.setLastName(rs.getString("LAST_NAME"));
				user.setFirstName(rs.getString("FIRST_NAME"));
				user.setMiddleName(rs.getString("MIDDLE_NAME"));
								
				activityData.setDateTime(rs.getTimestamp("ACTIVITY_DATE"));
				activityData.setAssignedTo(rs.getString("ASSIGNED_TO"));
				activityData.setLapseTime(rs.getString("LAPSE_TIME"));
				activityData.setStatus(status);
				activityData.setUser(user);
								
				list.add(activityData);
		  	}
 
		  	return (list);	
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new Exception(e.getMessage());
		}finally
		{
			try{
				if(rs != null){
					rs.close();
				}
			}catch(Exception e1)
			{
				LOGGER.error(CodeHelper.getStackTrace(e1));
			}
			try{
				if(ps != null){
					ps.close();
				}
			}
			catch(Exception e1)
			{
				LOGGER.error(CodeHelper.getStackTrace(e1));
			}
			LOGGER.info("retrieveActivityLogs end");
		}
		
	}	
	
	public ArrayList retrieveActivityLogsByPeriod(Date startDate, Date endDate) throws SQLException{
		
		LOGGER.info("retrieveActivityLogsByPeriod start");
		ArrayList list = new ArrayList();
		String sql = "SELECT" +
						" AL.ACT_DATETIME as ACTIVITY_DATE, " +
						" AL.ACT_ID as STAT_ID, " +
						" AL.ACT_ELAPSE_TIME as LAPSE_TIME, " +
						" AL.ACT_ASSIGNED_TO as ASSIGNED_TO " +
						" ST.STAT_DESC as STAT_DESC, " +
						" AL.USER_ID as USER_ID, " +
						" U.USR_LAST_NAME as LAST_NAME, " +
						" U.USR_FIRST_NAME as FIRST_NAME, " +
						" U.USR_MIDDLE_NAME as MIDDLE_NAME " +
					"FROM" +
						" ASSESSMENT_REQUESTS AR, " +
						" ACTIVITY_LOGS AL, " +
						" STATUS ST, " +
						" USERS U " +
					"WHERE" +
						" AL.UAR_REFERENCE_NUM = AR.REFERENCE_NUM AND " +
						" AL.USER_ID = U.USER_ID AND " +
						" AL.STAT_ID = ST.STAT_ID AND " +
						" AL.ACT_DATETIME BETWEEN ? AND ? " +
					"ORDER BY" +
						" AL.ACT_DATETIME DESC ";
		PreparedStatement stmt = null;			 
		ResultSet rs = null;
		try {
			stmt = conn.prepareStatement(sql);
			stmt.setDate(1, DateHelper.sqlDate(startDate));
			stmt.setDate(2, DateHelper.sqlDate(endDate));
			rs = stmt.executeQuery();
			while(rs.next()){
				ActivityLogData activityData = new ActivityLogData();
				
				StatusData status = new StatusData();
				status.setStatusId(rs.getLong("STAT_ID"));
				status.setStatusDesc(rs.getString("STAT_DESC"));
				
				UserProfileData user = new UserProfileData();
				user.setUserId(rs.getString("USER_ID"));
				user.setLastName(rs.getString("LAST_NAME"));
				user.setFirstName(rs.getString("FIRST_NAME"));
				user.setMiddleName(rs.getString("MIDDLE_NAME"));
								
				activityData.setDateTime(rs.getTimestamp("ACTIVITY_DATE"));
				activityData.setAssignedTo(rs.getString("ASSIGNED_TO"));
				activityData.setLapseTime(rs.getString("LAPSE_TIME"));
				activityData.setStatus(status);
				activityData.setUser(user);
								
				list.add(activityData);
			}
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		}finally
		{
			try{
				if(rs != null){
					rs.close();
				}
			}catch(Exception e1)
			{
				LOGGER.error(CodeHelper.getStackTrace(e1));
			}
			try{
				if(stmt != null){
					stmt.close();
				}
			}
			catch(Exception e1)
			{
				LOGGER.error(CodeHelper.getStackTrace(e1));
			}
	
		}
		LOGGER.info("retrieveActivityLogsByPeriod end");
		return list;
			
	}
	
	
	
	public int deleteActivityLogs(Date startDate, Date endDate) throws SQLException {
		
		LOGGER.info("deleteActivityLogs start 1");
		String sql = "DELETE FROM ACTIVITY_LOGS WHERE to_date(to_char(ACT_DATETIME, 'DDMONYYYY'), 'DDMONYYYY') BETWEEN ? AND ?";
		int result = -1;
		PreparedStatement ps = null;
		try{
			ps = conn.prepareStatement(sql);
			ps.setDate(1, DateHelper.sqlDate(startDate));
			ps.setDate(2, DateHelper.sqlDate(endDate));
			result = ps.executeUpdate();
		}catch(Exception e)
		{
			LOGGER.error(CodeHelper.getStackTrace(e));
		}finally
		{
			try{
				if(ps != null){
					ps.close();
				}
			}
			catch(Exception e1)
			{
				LOGGER.error(CodeHelper.getStackTrace(e1));
			}
	
		}
		LOGGER.info("deleteActivityLogs end 1");
		return result;
	}
	
	
	public void deleteActivityLogs(String referenceNum) throws SQLException {
		
		LOGGER.info("deleteActivityLogs start 2");
		String sql = "DELETE ACTIVITY_LOGS WHERE UAR_REFERENCE_NUM = ?";
		PreparedStatement ps = null;
		try{
			ps = conn.prepareStatement(sql);
			ps.setString(1, referenceNum);
			ps.executeUpdate();
		}catch(Exception e)
		{
			LOGGER.error(CodeHelper.getStackTrace(e));
		}
		finally
		{
			try{
				ps.close();
			}
			catch(Exception e1)
			{
				LOGGER.error(CodeHelper.getStackTrace(e1));
			}
			LOGGER.info("deleteActivityLogs end 2");
		}
		
	}
	
	
	public ArrayList retrieveTurnAroundTime(TurnAroundTimeParam param) throws SQLException {
		
		LOGGER.info("retrieveTurnAroundTime start");
			ArrayList list = new ArrayList();
			String tempSql ="SELECT DISTINCT(AR.REFERENCE_NUM) REFERENCE_NUMBER, AR.LOB" +
							" FROM" +
							" ASSESSMENT_REQUESTS AR, " +
							" ACTIVITY_LOGS AL " +
							" WHERE " +
							" ACT_DATETIME BETWEEN to_timestamp(?, 'DDMONYYYY HH24:MI:SS') AND to_timestamp(?, 'DDMONYYYY HH24:MI:SS') " +
							" AND AL.STAT_ID = ? " +
							" AND (AR.REFERENCE_NUM = AL.UAR_REFERENCE_NUM) " +
							(param.getLob() != null ? "AND AR.LOB = '" + param.getLob().getLOBCode() + "'": "") +
							" AND (AR.REFERENCE_NUM IN " +
							" (SELECT UAR_REFERENCE_NUM FROM ACTIVITY_LOGS WHERE ACT_DATETIME BETWEEN to_timestamp(?, 'DDMONYYYY HH24:MI:SS') AND to_timestamp(?, 'DDMONYYYY HH24:MI:SS') AND STAT_ID = ?))" +
							" ORDER BY AR.LOB";
			
			PreparedStatement ps = null;
			ResultSet rs = null;
			try{
			ps = conn.prepareStatement(tempSql);
			ps.setString(1, DateHelper.addTimePart(param.getStartDate(), DateHelper.ADD_START_DAY_TIME));
			ps.setString(2, DateHelper.addTimePart(param.getEndDate(), DateHelper.ADD_END_DAY_TIME));
			ps.setLong(3, Long.parseLong(param.getStartStatus()));
			ps.setString(4, DateHelper.addTimePart(param.getStartDate(), DateHelper.ADD_START_DAY_TIME));
			ps.setString(5, DateHelper.addTimePart(param.getEndDate(), DateHelper.ADD_END_DAY_TIME));
			ps.setLong(6, Long.parseLong(param.getEndStatus()));
			rs = ps.executeQuery();
			ArrayList tempSQLResult = new ArrayList();
			while (rs.next()){
				tempSQLResult.add(rs.getString("REFERENCE_NUMBER"));
			}
			
			if (tempSQLResult.size() > 0){
				String previousLOB = null;
				TurnAroundTimeLOBData lob = new TurnAroundTimeLOBData();
				BigDecimal totalElapsedTime = new BigDecimal(0);
				ArrayList details = new ArrayList();
							
				for (int i=0; i< tempSQLResult.size(); i++){
					String referenceNumber = (String)tempSQLResult.get(i);
					tempSql = "SELECT AR.LOB LOB, MIN(to_char(ACT_DATETIME, 'DDMONYYYY HH:MM AM')) START_DATE FROM ACTIVITY_LOGS AL, ASSESSMENT_REQUESTS AR WHERE UAR_REFERENCE_NUM = ? AND STAT_ID = ? AND (AR.REFERENCE_NUM = AL.UAR_REFERENCE_NUM) GROUP BY AR.LOB";
					ps = conn.prepareStatement(tempSql);
					ps.setString(1, referenceNumber);
					ps.setLong(2, Long.parseLong(param.getStartStatus()));
					rs = ps.executeQuery();
					TurnAroundTimeData data = new TurnAroundTimeData();
					if (rs.next()){
						if (previousLOB == null) {
							/* First Loop */
							previousLOB = rs.getString("LOB");
						} else {
							if (!previousLOB.equals(rs.getString("LOB"))) {
								lob.setLob(previousLOB);
								lob.setElapsedTime(totalElapsedTime);
								lob.setRequestsProcessed(BigDecimal.valueOf(details.size()));
								lob.setAveTAT(totalElapsedTime.divide(lob.getRequestsProcessed(), BigDecimal.ROUND_UP));
								lob.setTurnAroundTimeDetails(details);
								list.add(lob);
								 /*Initialized variables */ 
								lob = new TurnAroundTimeLOBData();
								totalElapsedTime = new BigDecimal(0);
								details = new ArrayList();
								previousLOB = rs.getString("LOB");
							}
						}
						data.setReferenceNum(referenceNumber);
						data.setStartDate(rs.getString("START_DATE"));
					}
					
					tempSql = "SELECT MAX(ACT_ID) ACTIVITY_ID, MAX(to_char(ACT_DATETIME, 'DDMONYYYY HH:MM AM')) END_DATE FROM ACTIVITY_LOGS AL, ASSESSMENT_REQUESTS AR WHERE UAR_REFERENCE_NUM = ? AND STAT_ID = ? AND (AR.REFERENCE_NUM = AL.UAR_REFERENCE_NUM)";
					ps = conn.prepareStatement(tempSql);
					ps.setString(1, referenceNumber);
					ps.setLong(2, Long.parseLong(param.getEndStatus()));
					rs = ps.executeQuery();
					long activityID = 0; 
					if (rs.next()){
						activityID = rs.getLong("ACTIVITY_ID");
						data.setEndDate(rs.getString("END_DATE"));
					}
					
					tempSql = "SELECT ACT_ELAPSE_TIME ELAPSE_TIME FROM ACTIVITY_LOGS AL WHERE ACT_ID = ?";
					ps = conn.prepareStatement(tempSql);
					ps.setLong(1, activityID);
					rs = ps.executeQuery();
					if (rs.next()){
						data.setElapsedTime(rs.getBigDecimal("ELAPSE_TIME"));
						totalElapsedTime = totalElapsedTime.add(data.getElapsedTime());
					}
					details.add(data);
				}
				
				if (previousLOB != null) {
					lob.setLob(previousLOB);
					lob.setElapsedTime(totalElapsedTime);
					lob.setRequestsProcessed(BigDecimal.valueOf(details.size()));
					lob.setAveTAT(totalElapsedTime.divide(lob.getRequestsProcessed(), BigDecimal.ROUND_UP));
					lob.setTurnAroundTimeDetails(details);
					list.add(lob);
				}
			}
			}catch(Exception e)
			{
				LOGGER.error(CodeHelper.getStackTrace(e));
			}
			finally
			{
				try{
					if(rs != null){
						rs.close();
					}
				}catch(Exception e1)
				{
					LOGGER.error(CodeHelper.getStackTrace(e1));
				}
				try{
					if(ps != null){
						ps.close();
					}
				}
				catch(Exception e1)
				{
					LOGGER.error(CodeHelper.getStackTrace(e1));
				}
		
			}
			LOGGER.info("retrieveTurnAroundTime end");
			return list;
		}

	
	public static void closeDB(Connection conn) {
		
		LOGGER.info("closeDB start");
		try {
			conn.close();
		} catch (Exception e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
		}
		LOGGER.info("closeDB send");
	}
}
