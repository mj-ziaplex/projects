/**
 * ImpairmentDAO.java				Copyright (c) 2004 Pointwest Technologies Corp.
 * TODO DOCUMENT ME!
 * 
 * @author aditalo	 @date Jan 14, 2004
 */
package com.slocpi.ium.data.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.slocpi.ium.data.ImpairmentData;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.DateHelper;
import com.slocpi.ium.util.IUMConstants;

/**
 * TODO DOCUMENT ME!
 * 
 * @author aditalo Copyright (c) 2004 Pointwest Technologies Corp.
 * @date Jan 14, 2004
 */
public class ImpairmentDAO {

	private static final Logger LOGGER = LoggerFactory.getLogger(ImpairmentDAO.class);

	private Connection conn = null;

	/**
	 * 
	 */
	public ImpairmentDAO(Connection connection) {
		super();
		this.conn = connection;
	}
	
	public void closeDB() {
		
		LOGGER.info("closeDB start");
		try {
			this.conn.close();
		} catch (Exception e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
		}
		LOGGER.info("closeDB end");
	}

	private void closeResources(PreparedStatement ps, ResultSet rs)
			throws SQLException {
		
		
		try {
			if (ps != null) {
				ps.close();
			}
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		}
		try {
			if (rs != null) {
				rs.close();
			}
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		}
		
	}

	public ArrayList retrieveImpairments(String referenceNumber,
			String clientType) throws SQLException {
		
		LOGGER.info("retrieveImpairments start");
		ArrayList impairments = new ArrayList();
		String iSql = "SELECT" + " I.CL_CLIENT_NUM as CLIENT_NO, "
				+ " I.UAR_REFERENCE_NUM as REFERENCE_NO, "
				+ " I.MIB_IMPAIRMENT_CODE as CODE, "
				+ " MIB_IMP.MIB_IMPAIRMENT_DESC as DESCRIPTION, "
				+ " MIB_NUM.MIB_NUM_DESC as RELATIONSHIP, "
				+ " MIB_LET.MIB_LTTR_DESC as IMP_ORIGIN, "
				+ " I.IMP_DATE as IMP_DATE, "
				+ " I.IMP_ACTION_CODE as IMP_ACTION_CODE, "
				+ " I.IMP_HEIGHT_FEET as HEIGHT_FEET, "
				+ " I.IMP_HEIGHT_INCHES as HEIGHT_INCHES, "
				+ " I.IMP_WEIGHT as WEIGHT, "
				+ " I.IMP_BLOOD_PRESSURE as BLOOD_PRESSURE, "
				+ " I.IMP_CONFIRMATION as CONFIRMATION, "
				+ " I.IMP_EXPORT_DATE as EXPORT_DATE, "
				+ " I.CREATED_BY as CREATED_BY, "
				+ " I.CREATED_DATE as CREATED_DATE, "
				+ " I.UPDATED_BY as UPDATED_BY, "
				+ " I.UPDATED_DATE as UPDATED_DATE, "
				+ " I.IMP_IMPAIREMENT_ID as IMPAIRMENT_ID " + "FROM"
				+ " IMPAIRMENTS I, " + " MIB_NUMBERS MIB_NUM, "
				+ " MIB_LETTERS MIB_LET, " + " MIB_IMPAIRMENTS MIB_IMP, "
				+ " ASSESSMENT_REQUESTS AR " + "WHERE"
				+ " I.IMP_ORIGIN = MIB_LET.MIB_LTTR_CODE AND "
				+ " I.IMP_RELATIONSHIP = MIB_NUM.MIB_NUM_CODE AND "
				+ " I.MIB_IMPAIRMENT_CODE = MIB_IMP.MIB_IMPAIRMENT_CODE AND "
				+ " I.UAR_REFERENCE_NUM = AR.REFERENCE_NUM AND "
				+ " I.CL_CLIENT_NUM = AR.INSURED_CLIENT_ID AND "
				+ " I.UAR_REFERENCE_NUM = ? " + "ORDER BY"
				+ " I.IMP_IMPAIREMENT_ID ";

		String oSql = "SELECT" + " I.CL_CLIENT_NUM as CLIENT_NO, "
				+ " I.UAR_REFERENCE_NUM as REFERENCE_NO, "
				+ " I.MIB_IMPAIRMENT_CODE as CODE, "
				+ " MIB_IMP.MIB_IMPAIRMENT_DESC as DESCRIPTION, "
				+ " MIB_NUM.MIB_NUM_DESC as RELATIONSHIP, "
				+ " MIB_LET.MIB_LTTR_DESC as IMP_ORIGIN, "
				+ " I.IMP_DATE as IMP_DATE, "
				+ " I.IMP_ACTION_CODE as IMP_ACTION_CODE, "
				+ " I.IMP_HEIGHT_FEET as HEIGHT_FEET, "
				+ " I.IMP_HEIGHT_INCHES as HEIGHT_INCHES, "
				+ " I.IMP_WEIGHT as WEIGHT, "
				+ " I.IMP_BLOOD_PRESSURE as BLOOD_PRESSURE, "
				+ " I.IMP_CONFIRMATION as CONFIRMATION, "
				+ " I.IMP_EXPORT_DATE as EXPORT_DATE, "
				+ " I.CREATED_BY as CREATED_BY, "
				+ " I.CREATED_DATE as CREATED_DATE, "
				+ " I.UPDATED_BY as UPDATED_BY, "
				+ " I.UPDATED_DATE as UPDATED_DATE, "
				+ " I.IMP_IMPAIREMENT_ID as IMPAIRMENT_ID " + "FROM"
				+ " IMPAIRMENTS I, " + " MIB_NUMBERS MIB_NUM, "
				+ " MIB_LETTERS MIB_LET, " + " MIB_IMPAIRMENTS MIB_IMP, "
				+ " ASSESSMENT_REQUESTS AR " + "WHERE"
				+ " I.IMP_ORIGIN = MIB_LET.MIB_LTTR_CODE AND "
				+ " I.IMP_RELATIONSHIP = MIB_NUM.MIB_NUM_CODE AND "
				+ " I.MIB_IMPAIRMENT_CODE = MIB_IMP.MIB_IMPAIRMENT_CODE AND "
				+ " I.UAR_REFERENCE_NUM = AR.REFERENCE_NUM AND "
				+ " I.CL_CLIENT_NUM = AR.OWNER_CLIENT_ID AND "
				+ " I.UAR_REFERENCE_NUM = ? " + "ORDER BY"
				+ " I.IMP_IMPAIREMENT_ID ";

		String sql = iSql;
		if (clientType.equals(IUMConstants.CLIENT_TYPE_OWNER)) {
			sql = oSql;
		}

		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			ps = conn.prepareStatement(sql);
			ps.setString(1, referenceNumber);
			rs = ps.executeQuery();
			while (rs.next()) {
				ImpairmentData impairment = new ImpairmentData();
				impairment.setClientNumber(rs.getString("CLIENT_NO"));
				impairment.setReferenceNumber(rs.getString("REFERENCE_NO"));
				impairment.setImpairmentCode(rs.getString("CODE"));
				impairment.setImpairmentDesc(rs.getString("DESCRIPTION"));
				impairment.setRelationship(rs.getString("RELATIONSHIP"));
				impairment.setImpairmentOrigin(rs.getString("IMP_ORIGIN"));
				impairment.setImpairmentDate(rs.getDate("IMP_DATE"));
				impairment.setActionCode(rs.getString("IMP_ACTION_CODE"));
				impairment.setHeightInFeet(rs.getDouble("HEIGHT_FEET"));
				impairment.setHeightInInches(rs.getDouble("HEIGHT_INCHES"));
				impairment.setWeight(rs.getDouble("WEIGHT"));
				impairment.setBloodPressure(rs.getString("BLOOD_PRESSURE"));
				impairment.setConfirmation(rs.getString("CONFIRMATION"));
				impairment.setExportDate(rs.getDate("EXPORT_DATE"));
				impairment.setCreatedBy(rs.getString("CREATED_BY"));
				impairment.setCreateDate(rs.getTimestamp("CREATED_DATE"));
				impairment.setUpdatedBy(rs.getString("UPDATED_BY"));
				impairment.setUpdateDate(rs.getTimestamp("UPDATED_DATE"));
				impairment.setImpairmentId(rs.getLong("IMPAIRMENT_ID"));
				impairments.add(impairment);
			}
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		} finally {
			this.closeResources(ps, rs);
		}
		LOGGER.info("retrieveImpairments end");
		return impairments;
	}// retrieveImpairments

	public ArrayList retrieveImpairmentsForExport(Date startDate, Date endDate)
			throws SQLException {
		
		LOGGER.info("retrieveImpairmentsForExport start");
		ArrayList impairments = new ArrayList();
		String sql = "SELECT IMP_IMPAIREMENT_ID, CL_CLIENT_NUM, UAR_REFERENCE_NUM, MIB_IMPAIRMENT_CODE, IMP_DESC"
				+ " 	   , IMP_RELATIONSHIP, IMP_ORIGIN, IMP_DATE, IMP_HEIGHT_FEET, IMP_HEIGHT_INCHES"
				+ "	   , IMP_WEIGHT, IMP_BLOOD_PRESSURE, IMP_CONFIRMATION, IMP_EXPORT_DATE"
				+ "      , CREATED_BY, CREATED_DATE, UPDATED_BY, UPDATED_DATE"
				+ " FROM IMPAIRMENTS WHERE CREATED_DATE BETWEEN ? AND ?";

		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			ps = conn.prepareStatement(sql);
			ps.setDate(1, DateHelper.sqlDate(startDate));
			ps.setDate(2, DateHelper.sqlDate(endDate));
			rs = ps.executeQuery();
			while (rs.next()) {
				ImpairmentData impairment = new ImpairmentData();
				impairment.setImpairmentId(rs.getLong("IMP_IMPAIREMENT_ID"));
				impairment.setClientNumber(rs.getString("CL_CLIENT_NUM"));
				impairment
						.setReferenceNumber(rs.getString("UAR_REFERENCE_NUM"));
				impairment.setImpairmentCode(rs
						.getString("MIB_IMPAIRMENT_CODE"));
				impairment.setImpairmentDesc(rs.getString("IMP_DESC"));
				impairment.setRelationship(rs.getString("IMP_RELATIONSHIP"));
				impairment.setImpairmentOrigin(rs.getString("IMP_ORIGIN"));
				impairment.setImpairmentDate(rs.getDate("IMP_DATE"));
				impairment.setHeightInFeet(rs.getDouble("IMP_HEIGHT_FEET"));
				impairment.setHeightInInches(rs.getDouble("IMP_HEIGHT_INCHES"));
				impairment.setWeight(rs.getDouble("IMP_WEIGHT"));
				impairment.setBloodPressure(rs.getString("IMP_BLOOD_PRESSURE"));
				impairment.setConfirmation(rs.getString("IMP_CONFIRMATION"));
				impairment.setExportDate(rs.getDate("IMP_EXPORT_DATE"));
				impairment.setCreatedBy(rs.getString("CREATED_BY"));
				impairment.setCreateDate(rs.getDate("CREATED_DATE"));
				impairment.setUpdatedBy(rs.getString("UPDATED_BY"));
				impairment.setUpdateDate(rs.getDate("UPDATED_DATE"));
				impairments.add(impairment);
			}
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		} finally {
			this.closeResources(ps, rs);
		}
		LOGGER.info("retrieveImpairmentsForExport end");
		return impairments;
	}// retrieveImpairmentsForExport

	public void updateExportDate(long impairmentID, Date exportDate,
			String updatedBy) throws SQLException {
		
		LOGGER.info("updateExportDate start");
		String sql = "UPDATE IMPAIRMENTS"
				+ " SET IMP_EXPORT_DATE = ?, UPDATED_BY = ?, UPDATED_DATE = ?"
				+ " WHERE IMP_IMPAIREMENT_ID = ? AND IMP_EXPORT_DATE IS NULL";

		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement(sql);
			ps.setDate(1, DateHelper.sqlDate(exportDate));
			ps.setString(2, updatedBy);
			ps.setDate(3, DateHelper.sqlDate(new Date()));
			ps.setLong(4, impairmentID);
			ps.executeUpdate();
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		} finally {
			this.closeResources(ps, rs);
		}
		LOGGER.info("updateExportDate end");
	}// updateExportDate

	/*public void insertImpairment(ImpairmentData data) throws SQLException {
		
		LOGGER.info("insertImpairment start");

		String sql = "INSERT INTO IMPAIRMENTS ("
				+ "IMP_IMPAIREMENT_ID, " // --
				+ "CL_CLIENT_NUM, " // 1
				+ "UAR_REFERENCE_NUM, " // 2
				+ "MIB_IMPAIRMENT_CODE, " // 3
				+ "IMP_DESC, " // 4
				+ "IMP_RELATIONSHIP, " // 5
				+ "IMP_ORIGIN, " // 6
				+ "IMP_DATE, " // 7
				+ "IMP_HEIGHT_FEET, " // 8
				+ "IMP_HEIGHT_INCHES, " // 9
				+ "IMP_WEIGHT, " // 10
				+ "IMP_BLOOD_PRESSURE, " // 11
				+ "IMP_CONFIRMATION, " // 12
				+ "IMP_EXPORT_DATE, " // 13
				+ "CREATED_BY, " // 14
				+ "CREATED_DATE, " // 15
				+ "IMP_ACTION_CODE " // 16
				+ ")"
				+ "VALUES (SEQ_IMPAIRMENT.NEXTVAL,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			
			ps = conn.prepareStatement(sql);

			String clientNo = data.getClientNumber();
			if (clientNo != null) {
				ps.setString(1, clientNo);
			} else {
				ps.setString(1, null);
			}

			String refNo = data.getReferenceNumber();
			if (refNo != null) {
				ps.setString(2, refNo);
			} else {
				ps.setString(2, null);
			}

			String code = data.getImpairmentCode();
			if (code != null) {
				ps.setString(3, code);
			} else {
				ps.setString(3, null);
			}

			String desc = data.getImpairmentDesc();
			if (desc != null) {
				ps.setString(4, desc);
			} else {
				ps.setString(4, null);
			}

			String relationship = data.getRelationship();
			if (relationship != null) {
				ps.setString(5, relationship);
			} else {
				ps.setString(5, null);
			}

			String impOrigin = data.getImpairmentOrigin();
			if (impOrigin != null) {
				ps.setString(6, impOrigin);
			} else {
				ps.setString(6, null);
			}

			Date impDate = data.getImpairmentDate();
			if (impDate != null) {
				ps.setDate(7, DateHelper.sqlDate(impDate));
			} else {
				ps.setDate(7, null);
			}

			double heightFeet = data.getHeightInFeet();
			if (heightFeet != 0) {
				ps.setDouble(8, heightFeet);
			} else {
				ps.setDouble(8, 0);
			}

			double heightInch = data.getHeightInInches();
			if (heightInch != 0) {
				ps.setDouble(9, heightInch);
			} else {
				ps.setDouble(9, 0);
			}

			double weight = data.getWeight();
			if (weight != 0) {
				ps.setDouble(10, weight);
			} else {
				ps.setDouble(10, 0);
			}

			String bp = data.getBloodPressure();
			if (bp != null) {
				ps.setString(11, bp);
			} else {
				ps.setString(11, null);
			}

			String confirm = data.getConfirmation();
			if (confirm != null) {
				ps.setString(12, confirm);
			} else {
				ps.setString(12, null);
			}

			Date exportDate = data.getExportDate();
			if (exportDate != null) {
				ps.setDate(13, DateHelper.sqlDate(exportDate));
			} else {
				ps.setDate(13, null);
			}

			String createdBy = data.getCreatedBy();
			if (createdBy != null) {
				ps.setString(14, createdBy);
			} else {
				ps.setString(14, null);
			}

			String actCode = data.getActionCode();
			ps.setTimestamp(15, DateHelper.sqlTimestamp(data.getCreateDate()));

			if (actCode != null) {
				ps.setString(16, actCode);
			} else {
				ps.setString(16, null);
			}

			ps.executeUpdate();
			
			
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		} finally {
			this.closeResources(ps, rs);
		}
		LOGGER.info("insertImpairment end");
	}// insertImpairment
*/
	public boolean isDuplicateCode(String impairmentCode, String refNo,
			String clientNo) throws SQLException {
		
		LOGGER.info("isDuplicateCode start");
		String sql = "SELECT IMP_IMPAIREMENT_ID " + "FROM IMPAIRMENTS "
				+ "WHERE MIB_IMPAIRMENT_CODE = ? AND "
				+ "CL_CLIENT_NUM = ? AND " + "UAR_REFERENCE_NUM = ?";
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			
			ps = conn.prepareStatement(sql);
			ps.setString(1, impairmentCode);
			ps.setString(2, clientNo);
			ps.setString(3, refNo);

			rs = ps.executeQuery();
			if (rs.next()) {
				return true;
			} else {
				return false;
			}
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		} finally {
			this.closeResources(ps, rs);
			LOGGER.info("isDuplicateCode end");
		}
		
	}// isDuplicateCode

	public void deleteImpairment(long impairmentId, String userId)
			throws SQLException {
		
		LOGGER.info("deleteImpairment start");
		String sql = "DELETE FROM IMPAIRMENTS WHERE IMP_IMPAIREMENT_ID = ?";
		String sql1 = "UPDATE IMPAIRMENTS SET UPDATED_BY = ?, CREATED_DATE = ? WHERE IMP_IMPAIREMENT_ID = ?";

		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			stmt = conn.prepareStatement(sql1);
			stmt.setString(1, userId);
			stmt.setDate(2, null); 
			stmt.setLong(3, impairmentId);
			stmt.executeUpdate();
			stmt = conn.prepareStatement(sql);
			stmt.setLong(1, impairmentId);
			stmt.executeUpdate();
			
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		} finally {
			this.closeResources(stmt, rs);
			LOGGER.info("deleteImpairment start");
		}
	}

	/*
	 * added IMP_Action_Code field retrieval
	 * by Hackmel 11-18-2008 
	 */
	public ImpairmentData retrieveImpairment(long impairmentId)
			throws SQLException {
		
		LOGGER.info("retrieveImpairment start");
		String sql = "SELECT "
				+ "IMP_IMPAIREMENT_ID,CL_CLIENT_NUM,UAR_REFERENCE_NUM,MIB_IMPAIRMENT_CODE, IMP_DESC,IMP_RELATIONSHIP,IMP_ORIGIN,IMP_DATE,"
				+ "IMP_HEIGHT_FEET,IMP_HEIGHT_INCHES,IMP_WEIGHT,IMP_BLOOD_PRESSURE,IMP_CONFIRMATION,IMP_EXPORT_DATE,CREATED_BY,"
				+ "CREATED_DATE,UPDATED_BY,UPDATED_DATE,IMP_ACTION_CODE "
				+ "FROM IMPAIRMENTS " + "WHERE IMP_IMPAIREMENT_ID = ?";

		ImpairmentData impairment = new ImpairmentData();
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			
			stmt = conn.prepareStatement(sql);
			stmt.setLong(1, impairmentId);
			rs = stmt.executeQuery();
			while (rs.next()) {
				impairment.setImpairmentId(rs.getLong("IMP_IMPAIREMENT_ID"));
				impairment.setClientNumber(rs.getString("CL_CLIENT_NUM"));
				impairment
						.setReferenceNumber(rs.getString("UAR_REFERENCE_NUM"));
				impairment.setImpairmentCode(rs
						.getString("MIB_IMPAIRMENT_CODE"));
				impairment.setImpairmentDesc(rs.getString("IMP_DESC"));
				impairment.setRelationship(rs.getString("IMP_RELATIONSHIP"));
				impairment.setImpairmentOrigin(rs.getString("IMP_ORIGIN"));
				impairment.setImpairmentDate(rs.getDate("IMP_DATE"));
				impairment.setHeightInFeet(rs.getDouble("IMP_HEIGHT_FEET"));
				impairment.setHeightInInches(rs.getDouble("IMP_HEIGHT_INCHES"));
				impairment.setWeight(rs.getDouble("IMP_WEIGHT"));
				impairment.setBloodPressure(rs.getString("IMP_BLOOD_PRESSURE"));
				impairment.setConfirmation(rs.getString("IMP_CONFIRMATION"));
				impairment.setExportDate(rs.getDate("IMP_EXPORT_DATE"));
				impairment.setImpairmentCode(rs
						.getString("MIB_IMPAIRMENT_CODE"));
				impairment.setCreatedBy(rs.getString("CREATED_BY"));
				impairment.setCreateDate(rs.getTimestamp("CREATED_DATE"));
				impairment.setUpdatedBy(rs.getString("UPDATED_BY"));
				impairment.setUpdateDate(rs.getTimestamp("UPDATED_DATE"));
				impairment.setActionCode(rs.getString("IMP_ACTION_CODE"));
			}
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		} finally {
			this.closeResources(stmt, rs);
			LOGGER.info("retrieveImpairment end");
		}
		return impairment;
	}

	public void updateImpairment(ImpairmentData impData) throws SQLException {
		
		LOGGER.info("updateImpairment start");
		String sql = "UPDATE IMPAIRMENTS SET MIB_IMPAIRMENT_CODE = ?, IMP_RELATIONSHIP = ?, IMP_ORIGIN = ?, IMP_DATE = ?, IMP_HEIGHT_FEET = ?, "
				+ "IMP_HEIGHT_INCHES = ?, IMP_WEIGHT = ?, IMP_BLOOD_PRESSURE = ?, IMP_CONFIRMATION = ?, UPDATED_BY = ?, UPDATED_DATE = ? , IMP_ACTION_CODE=? "
				+ "WHERE IMP_IMPAIREMENT_ID = ?";

		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			stmt = conn.prepareStatement(sql);

			if (impData.getImpairmentCode() != null) {
				stmt.setString(1, impData.getImpairmentCode());
			} else {
				stmt.setString(1, null);
			}

			if (impData.getRelationship() != null) {
				stmt.setString(2, impData.getRelationship());
			} else {
				stmt.setString(2, null);
			}

			if (impData.getImpairmentOrigin() != null) {
				stmt.setString(3, impData.getImpairmentOrigin());
			} else {
				stmt.setString(3, null);
			}

			if (impData.getImpairmentDate() != null) {
				stmt.setDate(4, DateHelper.sqlDate(impData.getImpairmentDate()));
			} else {
				stmt.setDate(4, null);
			}

			if (impData.getHeightInFeet() != 0) {
				stmt.setDouble(5, impData.getHeightInFeet());
			} else {
				stmt.setDouble(5, 0);
			}

			if (impData.getHeightInInches() != 0) {
				stmt.setDouble(6, impData.getHeightInInches());
			} else {
				stmt.setDouble(6, 0);
			}

			if (impData.getWeight() != 0) {
				stmt.setDouble(7, impData.getWeight());
			} else {
				stmt.setDouble(7, 0);
			}

			if (impData.getBloodPressure() != null) {
				stmt.setString(8, impData.getBloodPressure());
			} else {
				stmt.setString(8, null);
			}

			if (impData.getConfirmation() != null) {
				stmt.setString(9, impData.getConfirmation());
			} else {
				stmt.setString(9, null);
			}

			stmt.setString(10, impData.getUpdatedBy());
			stmt.setTimestamp(11, DateHelper.sqlTimestamp(impData.getUpdateDate()));
			
			stmt.setString(12, impData.getActionCode());
			
			stmt.setLong(13, impData.getImpairmentId());
			
			
			stmt.executeUpdate();
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		} finally {
			this.closeResources(stmt, rs);
			LOGGER.info("updateImpairment end");
		}
	}

	public void deleteImpairments(String referenceNum) throws SQLException {
		
		LOGGER.info("deleteImpairments start");
		String sql = "DELETE IMPAIRMENTS WHERE UAR_REFERENCE_NUM = ?";
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement(sql);
			ps.setString(1, referenceNum);
			ps.executeUpdate();
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		} finally {
			this.closeResources(ps, rs);
			LOGGER.info("deleteImpairments end");
		}
	}


	public int countMIBForExport() throws SQLException {
		
		LOGGER.info("countMIBForExport start");
		int count = 0;
		String sql = "SELECT COUNT(*) FROM IMPAIRMENTS WHERE IMP_EXPORT_DATE IS NULL";
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery();
			if (rs.next()) {
				count = rs.getInt(1);
			}
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		} finally {
			this.closeResources(ps, rs);
		}
		LOGGER.info("countMIBForExport end");
		return count;
	}

	
	public static void closeDB(Connection conn) {
		
		LOGGER.info("closeDB start");
		try {
			conn.close();
		} catch (Exception e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
		}
		LOGGER.info("closeDB end");
	}

}
