/**
 * TestProfileDAO.java				Copyright (c) 2004 Pointwest Technologies Corp.
 * TODO DOCUMENT ME!
 * 
 * @author aditalo	 @date Jan 28, 2004
 */
package com.slocpi.ium.data.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.slocpi.ium.data.TestProfileData;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.DateHelper;

/**
 * TODO DOCUMENT ME!
 * 
 * @author aditalo		Copyright (c) 2004 Pointwest Technologies Corp.
 * @date Jan 28, 2004
 */
public class TestProfileDAO {

	private static final Logger LOGGER = LoggerFactory.getLogger(TestProfileDAO.class);
	private Connection conn = null;
	
	/* Constructor created to remove connection parameter */
	public TestProfileDAO() {
		
	}
	/**
	 * 
	 */
	public TestProfileDAO(Connection connection) {
		super();
		this.conn = connection;
	}
	private void closeResources(PreparedStatement ps, ResultSet rs) throws SQLException{
		
		
		try {
			if (ps != null){
				ps.close();
			}
		}
		catch(SQLException e)
		{
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		}
		try {
			if (rs != null){
				rs.close();
			}			
		} catch (SQLException e){
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		}
		
	}
	
	public ArrayList selectTestProfiles() throws SQLException {
		
		LOGGER.info("selectTestProfiles start");
		ArrayList testProfiles = new ArrayList();
		String sql = "SELECT TEST_ID, TEST_DESC, TEST_TYPE, TEST_VALIDITY, TEST_TAXABLE_IND, TEST_FOLLOW_UP_NUM " +
					 "FROM TEST_PROFILES " +
					 "ORDER BY TEST_ID";		
		PreparedStatement ps = null;
		ResultSet rs = null;
		try{
			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery();
			while (rs.next()) {
				TestProfileData testProfile = new TestProfileData();
				testProfile.setTestId(rs.getLong("TEST_ID"));
				testProfile.setTestDesc(rs.getString("TEST_DESC"));
				testProfile.setTestType(rs.getString("TEST_TYPE"));
				testProfile.setValidity(rs.getInt("TEST_VALIDITY"));
				testProfile.setTaxable(rs.getBoolean("TEST_TAXABLE_IND"));
				testProfile.setFollowUpNumber(rs.getLong("TEST_FOLLOW_UP_NUM"));				
				testProfiles.add(testProfile);
			}
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			  throw e; 
		}finally{
			  this.closeResources(ps, rs);
		}		
		LOGGER.info("selectTestProfiles end");
		return testProfiles;
	}

	public TestProfileData retrieveTestProfile(long code) throws SQLException {
		
		LOGGER.info("retrieveTestProfile start");
		String sql = "SELECT TEST_ID, TEST_DESC, TEST_TYPE, TEST_VALIDITY, TEST_TAXABLE_IND, TEST_FOLLOW_UP_NUM " +
					 "FROM TEST_PROFILES " +
					 "WHERE TEST_ID = ? " +
					 "ORDER BY TEST_ID ";
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		TestProfileData data = null;
		try{
		ps = conn.prepareStatement(sql);
		ps.setLong(1, code);
		rs = ps.executeQuery();

		
		while (rs.next()) {
			data = new TestProfileData();
			data.setTestId(rs.getLong("TEST_ID"));
			data.setTestDesc(rs.getString("TEST_DESC"));
			data.setTestType(rs.getString("TEST_TYPE"));
			data.setValidity(rs.getInt("TEST_VALIDITY"));
			data.setTaxable(rs.getBoolean("TEST_TAXABLE_IND"));
			data.setFollowUpNumber(rs.getLong("TEST_FOLLOW_UP_NUM"));
		}
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			  throw e; 
		}finally{
			  this.closeResources(ps, rs);
		}	
		LOGGER.info("retrieveTestProfile end");
		return (data);
	}//retrieveTestProfile


	public void insertTestProfile(TestProfileData data) throws SQLException {
		
		LOGGER.info("insertTestProfile start");
		String sql = "INSERT INTO TEST_PROFILES (TEST_ID, TEST_DESC, TEST_TYPE, TEST_VALIDITY, TEST_TAXABLE_IND, CREATED_BY, CREATED_DATE, TEST_FOLLOW_UP_NUM) " +
					 "VALUES(?,?,?,?,?,?,?,?)";

		PreparedStatement ps = null;
		ResultSet rs = null;
		try{
			
			ps = conn.prepareStatement(sql);

			long code = data.getTestId(); 
			ps.setLong(1, code);

			String desc = data.getTestDesc(); 
			if (desc != null) {
				ps.setString(2, desc);
			} else {
				ps.setString(2, null);
			}

			String type = data.getTestType(); 
			if (type != null) {
				ps.setString(3, type);
			} else {
				ps.setString(3, null);
			}

			int validity = data.getValidity(); 
			ps.setInt(4, validity);

			boolean isTaxable = data.isTaxable(); 
			ps.setBoolean(5, isTaxable);
			
			String createdBy = data.getCreatedBy(); 
			if (createdBy != null) {
				ps.setString(6, createdBy);
			} else {
				ps.setString(6, null);
			}

			ps.setTimestamp(7, DateHelper.sqlTimestamp(data.getCreateDate()));
			
			long followUpNo = data.getFollowUpNumber(); 
			ps.setLong(8, followUpNo);

									
			ps.executeUpdate();
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			  throw e; 
		}finally{
			  this.closeResources(ps, rs);
		}	
		LOGGER.info("insertTestProfile end");
	}//insertTestProfile


	public void updateTestProfile(TestProfileData data) throws SQLException{	
		
		LOGGER.info("updateTestProfile start");
		String sql = "UPDATE TEST_PROFILES " +
					 "SET TEST_DESC = ?, TEST_TYPE = ?, TEST_VALIDITY = ?, TEST_TAXABLE_IND = ?, UPDATED_BY = ?, UPDATED_DATE = ?, TEST_FOLLOW_UP_NUM = ? " +
					 "WHERE TEST_ID = ?";
		PreparedStatement ps = null;
		ResultSet rs = null;
		try{
			
			ps = conn.prepareStatement(sql);
			ps.setString(1, data.getTestDesc());
			ps.setString(2, data.getTestType());
			ps.setInt(3, data.getValidity());
			ps.setBoolean(4, data.isTaxable());
			ps.setString(5, data.getUpdatedBy());
			ps.setTimestamp(6, DateHelper.sqlTimestamp(data.getUpdateDate()));
			ps.setLong(7, data.getFollowUpNumber());
			ps.setLong(8, data.getTestId());										
			ps.executeUpdate();
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			  throw e; 
		}finally{
			  this.closeResources(ps, rs);
		}	
		LOGGER.info("updateTestProfile end");
	}//updateTestProfile
	

	
}
