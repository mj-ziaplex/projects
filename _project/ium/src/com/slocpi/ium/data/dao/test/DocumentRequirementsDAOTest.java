/*
 * Created on Jun 23, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.data.dao.test;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;

import junit.framework.TestCase;

import com.slocpi.ium.data.dao.DocumentRequirementsDAO;
import com.slocpi.ium.data.util.DataSourceProxy;
import com.slocpi.ium.util.NameValuePair;


/**
 * @author Alain del Rosario
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class DocumentRequirementsDAOTest extends TestCase {

	/**
	 * Constructor for DocumentRequirementsDAOTest.
	 * @param arg0
	 */
	public DocumentRequirementsDAOTest(String arg0) {
		super(arg0);
	}

	public void testGetCodeValuesDocs() throws SQLException {
		Connection conn2 = null;
		conn2 = new DataSourceProxy().getConnection();
		DocumentRequirementsDAO drdao = new DocumentRequirementsDAO(conn2);
		Collection alist = new ArrayList();
		String lobb = "PN";
		alist = drdao.getCodeValuesDocs(lobb);
		assertNotNull(alist);
		assertTrue(alist.size()==9);
		//String strone = ((NameValuePair)alist.get(0)).getValue();
		
		//System.out.println(strone);
		//System.out.println(alist.size());
		
	} 
	
	
	public void testGetCodeValuesReqs() throws SQLException{
		Connection conn2 = null;
		conn2 = new DataSourceProxy().getConnection();
		DocumentRequirementsDAO drdao = new DocumentRequirementsDAO(conn2);
		Collection alist = new ArrayList();
		String lobb = "PN";
	    alist = drdao.getCodeValuesReqs(lobb);
		assertNotNull(alist);
		assertTrue(alist.size()==41);
		
	}
	
	public void testGetCodeValueDocs() throws SQLException{
		Connection conn2 = null;
		conn2 = new DataSourceProxy().getConnection();
		DocumentRequirementsDAO drdao = new DocumentRequirementsDAO(conn2);
		Collection alist = new ArrayList();
		String cdvalue = "YYY";
		alist = drdao.getCodeValueDocs(cdvalue);
		assertNotNull(alist);
		assertTrue(alist.size()==1);
		
		
	}
	
	public void testGetLobDocTypes() throws SQLException{
		Connection conn2 = null;
		conn2 = new DataSourceProxy().getConnection();
		DocumentRequirementsDAO drdao = new DocumentRequirementsDAO(conn2);
	    ArrayList alist = new ArrayList();
		String lobb = "PN";
		alist = (ArrayList)drdao.getLobDocTypes(lobb);
		assertNotNull(alist);
		assertTrue(alist.size()>0);
		String strone = (String) (alist.get(0));
		String strtwo = "Bounced Cheque";
		assertEquals(strone,strtwo);
	    
	}
	
	
	
	public void testGetRequirements() throws SQLException{
			Connection conn2 = null;
			conn2 = new DataSourceProxy().getConnection();
			DocumentRequirementsDAO drdao = new DocumentRequirementsDAO(conn2);
			ArrayList alist = new ArrayList();
			String lobb = "PN";
			alist = (ArrayList)drdao.getRequirements(lobb);
		
			String strone = ((NameValuePair)alist.get(0)).getName();
			assertNotNull(alist);
			assertTrue(alist.size()==1);
		    System.out.println(strone);
		    String strtwo = "";
		}
	

}
