package com.slocpi.ium.data.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.StringTokenizer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.IUMConstants;
import com.slocpi.ium.util.NameValuePair;


public class RequirementStatusCHDao extends CodeHelperDao {

	private static final Logger LOGGER = LoggerFactory.getLogger(RequirementStatusCHDao.class);
	private Connection conn;
	
	public RequirementStatusCHDao() throws SQLException {}// RequirementStatusCHDao

		
	public Collection getCodeValues() throws SQLException {
		return getList(null, null);
	}

	public Collection getCodeValues(String filter) throws SQLException {
		
		LOGGER.info("getCodeValues start");
		ArrayList lob_status = generateFilters(filter);
		
		String lob = "";
		String status = "";
		
		if (lob_status.size() == 2) {
			lob = (String) lob_status.get(0);
			status = (String) lob_status.get(1);
		}
		LOGGER.info("getCodeValues end");
		return getList(lob, status);
	}

	public Collection getList(String lob, String status) throws SQLException {
		
	  LOGGER.info("getList start");
	  Collection list = new ArrayList();
	  StringBuilder sql = new StringBuilder();
	  PreparedStatement ps  = null;
	  ResultSet rs = null;
	  Connection conn = null;
	  
      boolean isNullStatus = false;
      if ((status == null) || (status.equals("")) || (status.equals("null"))) {
        isNullStatus = true;
      }
      
	  boolean isNullLOB = false;
	  if ((lob == null) || (lob.equals("")) || (lob.equals("null"))) {
		isNullLOB= true;
	  }
	  
	  if (!isNullLOB&& !isNullStatus) {
		  
		sql.append("SELECT ")
			.append("DISTINCT(S.STAT_ID) AS CODE, ") 
			.append("S.STAT_DESC AS DESCRIPTION ")
			.append("FROM ")
			.append("STATUS S, ")
			.append("PROCESS_CONFIGURATIONS PC, ")
			.append("LOB_STATUS LS ")
			.append("WHERE ")
			.append("(S.STAT_ID = PC.EVNT_TO_STATUS AND ")
			.append(" S.STAT_ID = LS.STAT_ID AND ")
			.append(" LS.LOB_CODE = ? AND ")
			.append(" S.STAT_TYPE = ? AND ")
			.append(" PC.EVNT_FROM_STATUS = ?) OR ")
			.append("(S.STAT_ID = ?)")
			.append("ORDER BY S.STAT_ID");
	  } else if (isNullLOB&& !isNullStatus) {
		sql.append("SELECT ")
			.append("DISTINCT(S.STAT_ID) AS CODE, ") 
			.append("S.STAT_DESC AS DESCRIPTION ")
			.append("FROM ")
			.append("STATUS S, ")
			.append("PROCESS_CONFIGURATIONS PC, ")
			.append("LOB_STATUS LS ")
			.append("WHERE ")
			.append("(S.STAT_ID = PC.EVNT_TO_STATUS AND ")
			.append(" S.STAT_ID = LS.STAT_ID AND ")
			.append(" S.STAT_TYPE = ? AND ")
			.append(" PC.EVNT_FROM_STATUS = ?) OR ")
			.append("(S.STAT_ID = ?)")
			.append("ORDER BY S.STAT_ID ");
	  }	else if (!isNullLOB&& isNullStatus) {
		sql.append("SELECT ")
			.append("DISTINCT(S.STAT_ID) AS CODE, ") 
			.append("S.STAT_DESC AS DESCRIPTION ")
			.append("FROM ")
			.append("STATUS S, ")
			.append("PROCESS_CONFIGURATIONS PC, ")
			.append("LOB_STATUS LS ")
			.append("WHERE ")
			.append("S.STAT_ID = PC.EVNT_TO_STATUS AND ")
			.append("S.STAT_ID = LS.STAT_ID AND ")
			.append("LS.LOB_CODE = ? AND ")
			.append("S.STAT_TYPE = ? ")
			.append("ORDER BY S.STAT_ID ");
	  } else {
		sql.append("SELECT ")
			.append("DISTINCT(S.STAT_ID) AS CODE, ") 
			.append("S.STAT_DESC AS DESCRIPTION ")
			.append("FROM ")
			.append("STATUS S, ")
			.append("PROCESS_CONFIGURATIONS PC, ")
			.append("LOB_STATUS LS ")
			.append("WHERE ")
			.append("S.STAT_ID = PC.EVNT_TO_STATUS AND ")
			.append("S.STAT_ID = LS.STAT_ID AND ")
			.append("S.STAT_TYPE = ? ")
			.append("ORDER BY S.STAT_ID ");
	  }
	  

		try {
			conn = getConnection();	
			ps = conn.prepareStatement(sql.toString());
		
		if (!isNullLOB&& !isNullStatus) {
			ps.setString(1, lob);
			ps.setString(2, IUMConstants.STAT_TYPE_NM);
			ps.setString(3, status);
			ps.setString(4, status);
		} else if (isNullLOB&& !isNullStatus) {
			ps.setString(1, IUMConstants.STAT_TYPE_NM);
			ps.setString(2, status);
			ps.setString(3, status);
		} else if (!isNullLOB&& isNullStatus) {
			ps.setString(1, lob);
			ps.setString(2, IUMConstants.STAT_TYPE_NM);
		} else {
			ps.setString(1, IUMConstants.STAT_TYPE_NM);
		}
		
		rs = ps.executeQuery();				    		
		while (rs.next()) {
			
		  NameValuePair bean = new NameValuePair();
		  bean.setName(rs.getString("DESCRIPTION"));
		  bean.setValue(rs.getString("CODE"));
		  list.add(bean);
		}
	  } catch (SQLException e) {
		  LOGGER.error(CodeHelper.getStackTrace(e));
		  throw e; 
	  }finally{
		  closeResources(conn, ps, rs);
	  }
	  LOGGER.info("getList end");
	  return (list);
	}// getCodeValues


	public Collection getCodeValue(String codeValue) throws SQLException {
		
		LOGGER.info("getCodeValue start");
		Collection list = new ArrayList();
		String sql = "SELECT STAT_ID AS CODE, STAT_DESC AS DESCRIPTION FROM STATUS WHERE STAT_TYPE = ? AND STAT_ID = ?";
		PreparedStatement ps  = null;
		ResultSet rs = null;
		Connection conn = null;
		
		try {	  	
		  conn = getConnection();	
		  ps = conn.prepareStatement(sql);
		  ps.setString(1, IUMConstants.STAT_TYPE_NM);
		  ps.setString(2, codeValue);
		  rs = ps.executeQuery();				    		
		  while (rs.next()) {
			NameValuePair bean = new NameValuePair();
			bean.setName(rs.getString("DESCRIPTION"));
			bean.setValue(rs.getString("CODE"));
			list.add(bean);
		  }
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			  throw e; 
		}finally{
			 closeResources(conn, ps, rs);
		}
		LOGGER.info("getCodeValue end");
		return (list);
	}// getCodeValue


	private ArrayList generateFilters (String filter) {
		
		LOGGER.info("generateFilters start");
		ArrayList list = new ArrayList();
		StringTokenizer st = new StringTokenizer(filter,",");
		
		while (st.hasMoreTokens()) {
			String item = st.nextToken();
			list.add(item);
		}
		LOGGER.info("generateFilters end");
		return list;
	}// generateFilters
	


}