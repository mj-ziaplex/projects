/*
 * Created on Jun 25, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.data.dao.test;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import junit.framework.TestCase;

import com.slocpi.ium.data.KickOutMessageData;
import com.slocpi.ium.data.dao.KickOutMessageDAO;
import com.slocpi.ium.data.util.DataSourceProxy;

/**
 * @author Jov Tuplano
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class KickOutMessageDAOTest extends TestCase {

	/**
	 * Constructor for KickOutMessageDAOTest.
	 * @param arg0
	 */
	public KickOutMessageDAOTest(String arg0) {
		super(arg0);
	}

	public static void main(String[] args) {
	}

	/*
	 * @see TestCase#setUp()
	 */
	protected void setUp() throws Exception {
		super.setUp();
	}

	/*
	 * @see TestCase#tearDown()
	 */
	protected void tearDown() throws Exception {
		super.tearDown();
	}

	public void testInsertKOMessage() throws SQLException {
		//TODO Implement insertKOMessage().
		
		KickOutMessageDAO DAOInsertKO = new KickOutMessageDAO();
		KickOutMessageData dataInsertKO= new KickOutMessageData();
		
		dataInsertKO.setSequenceNumber(100);
		dataInsertKO.setReferenceNumber("JOV");
		dataInsertKO.setMessageText("Message");
		dataInsertKO.setClientId(null);
		dataInsertKO.setFailResponse(null);
		
		//DAOInsertKO.insertKOMessage(dataInsertKO);
		
		
	}

	public void testRetrieveKOMessage() throws SQLException {
		//TODO Implement retrieveKOMessage().
		
		KickOutMessageDAO DAORetrieveKO = new KickOutMessageDAO();
		KickOutMessageData dataRetrieve= new KickOutMessageData();
		
		long seqNumber= 5;
		
				
		dataRetrieve= DAORetrieveKO.retrieveKOMessage(seqNumber);
		assertEquals(5, dataRetrieve.getSequenceNumber());
		assertEquals("ABACUS0001", dataRetrieve.getReferenceNumber());
		assertEquals("Not Approved", dataRetrieve.getMessageText());
		assertEquals(null, dataRetrieve.getClientId());
		assertEquals("Not Approved", dataRetrieve.getFailResponse());
		assertNotNull(dataRetrieve);
		
		
	}

	public void testRetrieveKOMessages() throws Exception {
		//TODO Implement retrieveKOMessages().
		
		KickOutMessageDAO DAORetrieveKO2 = new KickOutMessageDAO();
		
		String RefNumber= "PRISM_ID07";
		
		ArrayList retrieveKOList= new ArrayList(DAORetrieveKO2.retrieveKOMessages(RefNumber));
		
		long SeqNumber= ((KickOutMessageData)retrieveKOList.get(0)).getSequenceNumber();
		String MsgText= ((KickOutMessageData)retrieveKOList.get(0)).getMessageText();
		String ClientId= ((KickOutMessageData)retrieveKOList.get(0)).getClientId();
		String FailResp= ((KickOutMessageData)retrieveKOList.get(0)).getFailResponse();
		
		//checks if data is retrieved
		assertEquals(0, SeqNumber);
		assertEquals("second kickout message", MsgText);
		assertEquals(null, ClientId);
		assertEquals(null, FailResp);
		assertNotNull(retrieveKOList);
		
	
		
	}

	public void testIsKOMEssageExisting() throws SQLException {
		//TODO Implement isKOMEssageExisting().
		
		KickOutMessageDAO DAOExisting = new KickOutMessageDAO();
		
		long SeqNbr= 5;
		String RefNbr= "ABACUS0001";
		
		boolean say, say2;
		say= DAOExisting.isKOMEssageExisting(SeqNbr, RefNbr);
		//tests if statement is true
		assertTrue(say);
		System.out.println(say);
		
		say2= DAOExisting.isKOMEssageExisting(2, "PRISM_ID07");
		assertTrue(!say2);
		System.out.println(!say2);
		
		
	}

	public void testDeleteKOMessages() throws SQLException {
		//TODO Implement deleteKOMessages().
		
		KickOutMessageDAO DAODelete = new KickOutMessageDAO();
		
		//DAODelete.deleteKOMessages(null);
		
		
	}

}
