package com.slocpi.ium.data.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.slocpi.ium.data.HolidayData;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.DateHelper;

/**
 * Data Access Object of the holiday reference module. Access the HOLIDAYS table.
 * @author tvicencio 
 */
public class HolidayDAO {

	private static final Logger LOGGER = LoggerFactory.getLogger(HolidayDAO.class);

	private Connection conn = null;

	public HolidayDAO(Connection connection) {
		super();
		this.conn = connection;
	}
	
	private void closeResources(PreparedStatement ps, ResultSet rs) throws SQLException{
		
		
		try {
			if (ps != null){
				ps.close();
			}
		}
		catch(SQLException e)
		{
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		}
		try {
			if (rs != null){
				rs.close();
			}			
		} catch (SQLException e){
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		}
		
	}
	
	public ArrayList retrieveHolidays(String year) throws SQLException {
		
		LOGGER.info("retrieveHolidays start");
		String sql = "SELECT HOL_ID, HOL_DESC, HOL_DATE " +
					 "FROM HOLIDAYS " +
					 "WHERE TO_CHAR(HOL_DATE, 'YYYY') LIKE '%" + year + "%' " +
					 "ORDER BY HOL_DATE ";
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList list = new ArrayList();  
		try {	  	
		
			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery();
	
			while (rs.next()) {
				HolidayData data = new HolidayData();
				data.setHolidayCode(rs.getLong("HOL_ID"));
				data.setHolidayDesc(rs.getString("HOL_DESC"));
				data.setHolidayDate(rs.getDate("HOL_DATE"));
				list.add(data);
			}
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e; 
		}finally{
			this.closeResources(ps, rs);
		}		
		LOGGER.info("retrieveHolidays end");
		return (list);
	}//retrieveHolidays


	public HolidayData retrieveHolidayDetail(String code) throws SQLException {
		
		LOGGER.info("retrieveHolidayDetail start");
		String sql = "SELECT HOL_ID, HOL_DESC, HOL_DATE " +
					 "FROM HOLIDAYS " +
					 "WHERE HOL_ID = ? " +
					 "ORDER BY HOL_ID ";
	
		PreparedStatement ps = null;
		ResultSet rs = null;
		HolidayData data = null;  
		try {	  	

			ps = conn.prepareStatement(sql);
			ps.setString(1, code);
			rs = ps.executeQuery();
	
			
			if (rs.next()) {
				data = new HolidayData();
				data.setHolidayCode(rs.getLong("HOL_ID"));
				data.setHolidayDesc(rs.getString("HOL_DESC"));
				data.setHolidayDate(rs.getDate("HOL_DATE"));
			}
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e; 
		}finally{
			this.closeResources(ps, rs);
		}		
		LOGGER.info("retrieveHolidayDetail end");
		return (data);	
		
	}//retrieveHolidayDetail
	

	public void insertHoliday(HolidayData data) throws SQLException {
		
		LOGGER.info("insertHoliday start");
		String sql = "INSERT INTO HOLIDAYS (HOL_ID, HOL_DESC, HOL_DATE, CREATED_BY, CREATED_DATE) " +
					 "VALUES(SEQ_HOLIDAY.NEXTVAL,?,?,?,?)";

		PreparedStatement ps = null;
		ResultSet rs = null;
		  
		try {	  	

			ps = conn.prepareStatement(sql);
		
			String desc = data.getHolidayDesc();
			if (desc != null) {
				ps.setString(1, desc);
			} else {
				ps.setString(1, null);
			}

			Date date = data.getHolidayDate(); 
			if (date != null) {
				ps.setDate(2, DateHelper.sqlDate(date));
			} else {
				ps.setDate(2, null);
			}
			
			String createdBy = data.getCreatedBy(); 
			if (createdBy != null) {
				ps.setString(3, createdBy);
			} else {
				ps.setString(3, null);
			}

			ps.setTimestamp(4, DateHelper.sqlTimestamp(data.getCreateDate()));
									
			ps.executeUpdate();
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e; 
		}finally{
			this.closeResources(ps, rs);
		}		
		LOGGER.info("insertHoliday end");
	}//insertHoliday


	public void updateHoliday(HolidayData data) throws SQLException{		
		
		LOGGER.info("updateHoliday start");
		String sql = "UPDATE HOLIDAYS " +
					 "SET HOL_DESC = ?, HOL_DATE = ?, UPDATED_BY = ?, UPDATED_DATE = ? " +
					 "WHERE HOL_ID = ?";
		PreparedStatement ps = null;
		ResultSet rs = null;
		  
		try {	  	

			ps = conn.prepareStatement(sql);
			ps.setString(1, data.getHolidayDesc());
			ps.setDate(2, DateHelper.sqlDate(data.getHolidayDate()));
			ps.setString(3, data.getUpdatedBy());
			ps.setTimestamp(4, DateHelper.sqlTimestamp(data.getUpdateDate()));
			ps.setLong(5, data.getHolidayCode());										
			ps.executeUpdate();
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e; 
		}finally{
			this.closeResources(ps, rs);
		}		
		LOGGER.info("updateHoliday end");
	}//updateHoliday


	public ArrayList retrieveYears() throws SQLException {
		
		LOGGER.info("retrieveYears start");
		String sql = "SELECT MIN(DISTINCT(TO_CHAR(HOL_DATE, 'YYYY'))) AS MIN_YEAR, " +
							"MAX(DISTINCT(TO_CHAR(HOL_DATE, 'YYYY'))) AS MAX_YEAR " +
					 "FROM HOLIDAYS";
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList list = new ArrayList();  
		try {	  	

		ps = conn.prepareStatement(sql);
		rs = ps.executeQuery();

		
		if (rs.next()) {
			int min = rs.getInt("MIN_YEAR");
			int max = rs.getInt("MAX_YEAR");

			for (int i=min; i <= max; i++) {
				list.add(String.valueOf(i));
			}
		}
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e; 
		}finally{
			this.closeResources(ps, rs);
		}		
		LOGGER.info("retrieveYears end");
		return (list);
	}//retrieveHolidays

}
