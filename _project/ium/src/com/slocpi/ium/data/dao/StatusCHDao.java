/*
 * Created on Jan 11, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.data.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.slocpi.ium.data.StatusData;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.NameValuePair;

/**
 * @author daguila
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class StatusCHDao extends BaseDAO{
	private static final Logger LOGGER = LoggerFactory.getLogger(StatusCHDao.class);
	
	public StatusCHDao() {}
	
	public StatusData convertStatus(String lobCode, String statusCode) throws SQLException {
		
		LOGGER.info("convertStatus start");
		StatusData sd = null;
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT a.stat_id, a.stat_desc FROM status a, lob_status b");
		sb.append(" where b.lob_code = ? and b.admin_status_code = ? and b.stat_id = a.stat_id");
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection conn = null;
		
		try {
			conn = getConnection();	
			ps = conn.prepareStatement(sb.toString());
			ps.setString(1, lobCode);
			ps.setString(2, statusCode);
			rs = ps.executeQuery();
		if (rs.next()) {
			sd = new StatusData();
			sd.setStatusId(rs.getLong("stat_id"));
			sd.setStatusDesc(rs.getString("stat_desc"));
			
		}
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			  throw e; 
		}finally{
			  closeResources(conn, ps, rs);
		}	
		LOGGER.info("convertStatus end");
		return sd;
	}
	
	public StatusData translateStatus(String lobCode, long statusId) throws SQLException {
		
		LOGGER.info("translateStatus start");
		StatusData sd = null;
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT a.stat_id, a.admin_status_code FROM lob_status a, status b");
		sb.append(" where b.stat_id = ? and a.lob_code = ? and b.stat_id = a.stat_id");
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection conn = null;
		
		try {
			conn = getConnection();	
			ps = conn.prepareStatement(sb.toString());
			ps.setLong(1, statusId);
			ps.setString(2, lobCode);
			rs = ps.executeQuery();
		if (rs.next()) {
			sd = new StatusData();
			sd.setStatusId(rs.getLong("stat_id"));
			sd.setStatusDesc(rs.getString("admin_status_code"));
		}
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			  throw e; 
		}finally{
			  closeResources(conn, ps, rs);
		}	
		LOGGER.info("translateStatus end");
		return sd;
	}
	
		
	/**
	 * This method will return a collection of status id and description for a particular LOB and object type (AR,M,NM)
	 */
	public Collection getCodeValue(String lob, String objectType) throws SQLException {
		
		LOGGER.info("getCodeValue start 1");
		Collection list = new ArrayList();
		String sql = "select distinct T1.STAT_DESC, T1.STAT_ID from STATUS T1, LOB_STATUS T2 where T1.STAT_ID = T2.STAT_ID and T2.LOB_CODE=? and T1.STAT_TYPE=? ORDER BY T1.STAT_DESC ASC";
	
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection conn = null;
		
		try {
			  conn = getConnection();	
			  ps = conn.prepareStatement(sql);
			  ps.setString(1, lob);
			  ps.setString(2, objectType);
		  
			  rs = ps.executeQuery();				    		
		  while (rs.next()) {
			NameValuePair bean = new NameValuePair();
			bean.setName(rs.getString(1));
			bean.setValue(rs.getString(2));
			list.add(bean);
		  }
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			  throw e; 
		}finally{
			  closeResources(conn, ps, rs);
		}	
		LOGGER.info("getCodeValue end 1");
		return (list);
	}// getCodeValue
	
	/**
		 * This method will return a collection of status id and description for a particular object type (AR,M,NM)
		 */
		public Collection getCodeValue(String codeValue) throws SQLException {
			
			LOGGER.info("getCodeValue start 2");
			Collection list = new ArrayList();
			String sql = "select distinct STAT_ID, STAT_DESC from STATUS where STAT_TYPE =? AND STAT_ID NOT IN (select distinct STAT_ID from LOB_STATUS) ORDER BY STAT_DESC ASC";
			
			PreparedStatement ps = null;
			ResultSet rs = null;
			Connection conn = null;
			
			try {
				conn = getConnection();	
				ps = conn.prepareStatement(sql);
				ps.setString(1, codeValue);		  			
	  
				rs = ps.executeQuery();				    		
			  while (rs.next()) {
				NameValuePair bean = new NameValuePair();
				bean.setName(rs.getString(2));
				bean.setValue(rs.getString(1));
				list.add(bean);			
			  }
			} catch (SQLException e) {
				LOGGER.error(CodeHelper.getStackTrace(e));
				  throw e; 
			}finally{
				  closeResources(conn, ps, rs);
			}	
			LOGGER.info("getCodeValue end 2");
			return (list);
		}// getCodeValue
		
	/**
	 * This method will return a collection of status id and description for a particular object type (AR,M,NM)
	 */
	public Collection getCodeValue1(String codeValue,String lob) throws SQLException {
		
		LOGGER.info("getCodeValue1 start");
		Collection list = new ArrayList();
		String sql = "select distinct STAT_ID, STAT_DESC from STATUS where STAT_TYPE =? AND STAT_ID NOT IN (select distinct STAT_ID from LOB_STATUS where LOB_CODE=?) ORDER BY STAT_DESC ASC";
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection conn = null;
		
		try {
			conn = getConnection();		  	
			ps = conn.prepareStatement(sql);
			ps.setString(1, codeValue);		  
			ps.setString(2, lob);
	  
			rs = ps.executeQuery();				    		
		  while (rs.next()) {
			NameValuePair bean = new NameValuePair();
			bean.setName(rs.getString(2));
			bean.setValue(rs.getString(1));
			list.add(bean);			
		  }
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			  throw e; 
		}finally{
			  closeResources(conn, ps, rs);
		}	
		LOGGER.info("getCodeValue1 end");
		return (list);
	}// getCodeValue
	
	public String getDescription(String objectID) throws SQLException {
		
		LOGGER.info("getDescription start");
		String sql = "select stat_desc as description from status where stat_id=?";
		PreparedStatement ps = null;
		ResultSet rs = null;
		String description = "";
		Connection conn = null;
		
		try {
			conn = getConnection();			
			ps = conn.prepareStatement(sql);
			ps.setString(1,objectID);
			rs = ps.executeQuery();
		
		if (rs.next()) {
			description = rs.getString("description");
		}
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			  throw e; 
		}finally{
			  closeResources(conn, ps, rs);
		}	
		LOGGER.info("getDescription end");
		return description;
	}
	
	public boolean isStatusExist(long statId, String statusType) throws SQLException{
		
		LOGGER.info("isStatusExist start");
		boolean result = false;
		String sql = "SELECT COUNT(*) FROM STATUS WHERE STAT_ID = ? AND STAT_TYPE=?";
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection conn = null;
		
		try {
			conn = getConnection();			
			ps = conn.prepareStatement(sql);
			ps.setLong(1, statId);
			ps.setString(2, statusType);
			rs = ps.executeQuery();
		if (rs.next()){
			if (rs.getInt(1)>0){
				result = true;
			}
		}
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			  throw e; 
		}finally{
			  closeResources(conn, ps, rs);
		}			
		LOGGER.info("isStatusExist end");
		return result;
	}
}
