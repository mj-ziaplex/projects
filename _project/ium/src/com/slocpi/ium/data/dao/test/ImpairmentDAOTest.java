/*
 * Created on Jun 23, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.data.dao.test;

import java.sql.Connection;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;

import junit.framework.TestCase;

import com.slocpi.ium.data.ImpairmentData;
import com.slocpi.ium.data.dao.ImpairmentDAO;
import com.slocpi.ium.data.util.DataSourceProxy;

/**
 * @author Alain del Rosario
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class ImpairmentDAOTest extends TestCase {

	/**
	 * Constructor for ImpairmentDAOTest.
	 * @param arg0
	 */
	public ImpairmentDAOTest(String arg0) {
		super(arg0);
	}
	
	
	public void testImpairmentDAO(){
		Connection conn2 = null;
		conn2 = new DataSourceProxy().getConnection();
		ImpairmentDAO dao = new ImpairmentDAO(conn2);
		assertNotNull(dao);
		
	}
    



    
	public void testRetrieveImpairments() throws SQLException {
		Connection conn2 = null;
		conn2 = new DataSourceProxy().getConnection();
		ArrayList alist = new ArrayList();
		String num = "014172346";
		String ctype = "ENGEL002";
		ImpairmentDAO dao = new ImpairmentDAO(conn2);
		alist = dao.retrieveImpairments(num,ctype);
		assertNotNull(alist);
		assertTrue(alist.size()==3);
		
		long value1 = 61;
		long value2 = 62;
		long value3 = 63;
		long ID = ((ImpairmentData)alist.get(0)).getImpairmentId();
		long ID2 = ((ImpairmentData)alist.get(1)).getImpairmentId();
		long ID3 = ((ImpairmentData)alist.get(2)).getImpairmentId();
		assertEquals(ID,value1);
		assertEquals(ID2,value2);
		assertEquals(ID3,value3);
		
	}
	
	
	
	public void testIsDuplicateCode() throws SQLException{
		boolean isittrue = false;
		String impairmentCode = "A4";
		String refNo = "014172346";
		String clientNo = "ENGEL002";
		Connection conn2 = null;
	    conn2 = new DataSourceProxy().getConnection();
		ImpairmentDAO dao = new ImpairmentDAO(conn2);
		isittrue = dao.isDuplicateCode(impairmentCode, refNo, clientNo);
	    assertTrue(isittrue);
	}
	
	
	
	
	public void testRetrieveImpairment() throws SQLException, ParseException{
		ImpairmentData impairment = new ImpairmentData();
		long impairmentId = 7;
		String clientnum = "ENGEL002";
		Connection conn2 = null;
		String refnum = "014164728";
		String description = null;
		String relationship = "C";
		String origin = "x";
		String ds1 = "July 1, 2004";
		DateFormat df = DateFormat.getDateInstance();
		Date d1 = df.parse(ds1);
		double heightInFeet = 5;
		double heightInInches = 2;
		double weight = 115;
		String bloodPressure = "160-110";
		String confirmation = "R";
		String createdBy = "REQNBST1";
		String updatedBy = "REQNBST1";
		String ds2 = "Jan 27, 2004";
		String impairmentCode = "IC7";
		
		
		
		
		
		conn2 = new DataSourceProxy().getConnection();
		ImpairmentDAO dao = new ImpairmentDAO(conn2);
		impairment = dao.retrieveImpairment(impairmentId);
		assertEquals(impairment.getImpairmentId(),impairmentId);
		assertEquals(impairment.getClientNumber(),clientnum);
		assertEquals(impairment.getReferenceNumber(),refnum);
		assertEquals(impairment.getImpairmentDesc(),description);
		assertEquals(impairment.getRelationship(),relationship);
		assertEquals(impairment.getImpairmentOrigin(),origin);
		assertTrue(impairment.getImpairmentDate().equals(d1));
		assertEquals((byte)(impairment.getHeightInFeet()),(byte)(heightInFeet));
		assertEquals((byte)(impairment.getHeightInInches()),(byte)(heightInInches));
		assertEquals((byte)(impairment.getWeight()),(byte)(weight));
		assertEquals(impairment.getBloodPressure(),bloodPressure);
		assertEquals(impairment.getConfirmation(),confirmation);
		assertNull(impairment.getExportDate());
		assertEquals(impairment.getCreatedBy(),createdBy);
		
		
		Date createdate = impairment.getCreateDate();
		DateFormat dfcreatedate = DateFormat.getDateInstance();
		String s1 = dfcreatedate.format(createdate);
		assertEquals(s1,ds2);
        
		assertEquals(impairment.getUpdatedBy(),updatedBy);
		
		Date updatedate = impairment.getUpdateDate();
		DateFormat dfupdatedate = DateFormat.getDateInstance();
		String s2 = dfupdatedate.format(updatedate);
		assertEquals(s2,ds2);
		assertEquals(impairment.getImpairmentCode(),impairmentCode);
		
	}
	
	public void testCountMIBForExport() throws SQLException{
		int num = 0;
		Connection conn2 = null;
		conn2 = new DataSourceProxy().getConnection();
		ImpairmentDAO dao = new ImpairmentDAO(conn2);
		num = dao.countMIBForExport();
		assertEquals(num,37);
	}
	
	
	public void testRetrieveImpairmentsForExport() throws ParseException, SQLException{
		String ds1 = "February 14, 2004";
		DateFormat df = DateFormat.getDateInstance();
		Date start = df.parse(ds1);
		
		String ds2 = "February 19, 2004";
		DateFormat df2 = DateFormat.getDateInstance();
		Date end = df.parse(ds2);
		ArrayList alist = new ArrayList();
		Connection conn2 = null;
		conn2 = new DataSourceProxy().getConnection();
		ImpairmentDAO dao = new ImpairmentDAO(conn2);
		alist = dao.retrieveImpairmentsForExport(start,end);
		assertNotNull(alist);
		assertTrue(alist.size()==3);
		
		
		long ID = ((ImpairmentData)alist.get(0)).getImpairmentId();
		long ID2 = ((ImpairmentData)alist.get(1)).getImpairmentId();
		long ID3 = ((ImpairmentData)alist.get(2)).getImpairmentId();
		long value1 = 68;
		long value2 = 75;
		long value3 = 74;
		assertEquals(ID,value1);
		assertEquals(ID2,value2);
		assertEquals(ID3,value3);
		
		}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}

