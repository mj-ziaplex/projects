/*
 * Created on Jun 25, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.data.dao.test;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import junit.framework.TestCase;

import com.slocpi.ium.data.AccessTemplateData;
import com.slocpi.ium.data.RolesData;
import com.slocpi.ium.data.dao.SecurityDAO;
import com.slocpi.ium.data.util.DataSourceProxy;

/**
 * @author Jov Tuplano
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class SecurityDAOTest extends TestCase {

	/**
	 * Constructor for SecurityDAOTest.
	 * @param arg0
	 */
	public SecurityDAOTest(String arg0) {
		super(arg0);
	}

	public static void main(String[] args) {
	}

	/*
	 * @see TestCase#setUp()
	 */
	protected void setUp() throws Exception {
		super.setUp();
	}

	/*
	 * @see TestCase#tearDown()
	 */
	protected void tearDown() throws Exception {
		super.tearDown();
	}

	public void testSelectTemplates() throws SQLException {
		//TODO Implement selectTemplates().
		
		AccessTemplateData tempData= new AccessTemplateData();
		tempData.setTemplateID(84);
		tempData.setTemplateDesc("AGENTS");
		
		RolesData rolesData= new RolesData();
		rolesData.setRolesId("AGENT");
		rolesData.setRolesDesc("AGENTS");
		tempData.setRole(rolesData);
		
		ArrayList SelectTemp= new ArrayList();
		SelectTemp.add(tempData);
		
		//checks if data is retrieved.		
		assertEquals(84, ((AccessTemplateData)SelectTemp.get(0)).getTemplateID());
		assertEquals("AGENTS", ((AccessTemplateData)SelectTemp.get(0)).getTemplateDesc());
		
	}

	public void testSelectTemplate() {
		//TODO Implement selectTemplate().
	}

	public void testSelectTemplateDetails() {
		//TODO Implement selectTemplateDetails().
	}

	public void testInsertAccessTemplate() {
		//TODO Implement insertAccessTemplate().
	}

	public void testUpdateAccessTemplate() {
		//TODO Implement updateAccessTemplate().
	}

	public void testIsAccessTemplateExist() {
		//TODO Implement isAccessTemplateExist().
	}

	/*
	 * Test for UserAccessData selectUserPageAccess(String, long, long)
	 */
	public void testSelectUserPageAccessStringlonglong() {
		//TODO Implement selectUserPageAccess().
	}

	/*
	 * Test for ArrayList selectUserPageAccess(String, long)
	 */
	public void testSelectUserPageAccessStringlong() {
		//TODO Implement selectUserPageAccess().
	}

	/*
	 * Test for ArrayList selectUserPageAccess(String)
	 */
	public void testSelectUserPageAccessString() {
		//TODO Implement selectUserPageAccess().
	}

	public void testSaveUserAccess() {
		//TODO Implement saveUserAccess().
	}

	public void testSelectPageAccess() {
		//TODO Implement selectPageAccess().
	}

	public void testSavePageAccess() {
		//TODO Implement savePageAccess().
	}

	public void testAddRoleToUser() {
		//TODO Implement addRoleToUser().
	}

	public void testRemoveRoleFromUser() {
		//TODO Implement removeRoleFromUser().
	}

	public void testGetTemplateId() {
		//TODO Implement getTemplateId().
	}

	public void testHasOtherAccess() {
		//TODO Implement hasOtherAccess().
	}

}
