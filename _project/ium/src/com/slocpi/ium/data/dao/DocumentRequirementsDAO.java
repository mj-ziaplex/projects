/**
 * ProcessConfigurationDAO.java				Copyright (c) 2004 Pointwest Technologies Corp.
 * TODO DOCUMENT ME!
 * 
 * @author aditalo	 @date Feb 23, 2004
 */
package com.slocpi.ium.data.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.slocpi.ium.ui.form.DocumentsRequirementsForm;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.NameValuePair;

/**
 * TODO DOCUMENT ME!
 * 
 * @author byu		Copyright (c) 2004 Pointwest Technologies Corp.
 * @date Feb 23, 2004
 */
public class DocumentRequirementsDAO {

	private static final Logger LOGGER = LoggerFactory.getLogger(DocumentRequirementsDAO.class);

	private Connection conn;

	public DocumentRequirementsDAO(Connection conn) throws SQLException {
		this.conn = conn;
	}// DocumentTypeCHDao

	private void closeResources(PreparedStatement ps, ResultSet rs) throws SQLException{
		
		
		try {
			if (ps != null){
				ps.close();
			}
		}
		catch(SQLException e)
		{
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		}
		try {
			if (rs != null){
				rs.close();
			}			
		} catch (SQLException e){
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		}
		
	}


	/**
	 * This method will return a collection of all the client types.
	 */
	public Collection getCodeValuesDocs(String lob) throws SQLException {
		
	  LOGGER.info("getCodeValuesDocs start");
	  Collection list = new ArrayList();
	  String sql = "SELECT DOC_CODE AS CODE, DOC_DESC AS DESCRIPTION FROM DOCUMENT_TYPES where DOC_CODE not in (select distinct DOC_CODE from LOB_DOCUMENTS where LOB_CODE = ?) order by DOC_DESC";
	  
	  PreparedStatement ps = null;
	  ResultSet rs = null;
	  
	  try {	  	
			ps = conn.prepareStatement(sql);
			ps.setString(1,lob);
			rs = ps.executeQuery();				    		
			while (rs.next()) {
			  NameValuePair bean = new NameValuePair();
			  bean.setName(rs.getString("DESCRIPTION"));
			  bean.setValue(rs.getString("CODE"));
			  list.add(bean);
			}
	  } catch (SQLException e) {
		  LOGGER.error(CodeHelper.getStackTrace(e));
			throw e; 
	  }finally{
		  	this.closeResources(ps, rs);
	  }		
	  LOGGER.info("getCodeValuesDocs end");
	  return (list);
	}// getCodeValues

	/**
	 * This method will return a collection of all the client types.
	 */
	public Collection getCodeValuesReqs(String lob) throws SQLException {
		
		LOGGER.info("getCodeValuesReqs start");
	  Collection list = new ArrayList();
	  String sql = "SELECT REQT_CODE AS CODE, REQT_DESC AS DESCRIPTION FROM REQUIREMENTS where REQT_CODE not in (select distinct REQT_CODE from LOB_REQUIREMENTS where LOB_CODE = ?) order by REQT_DESC";
	  PreparedStatement ps = null;
	  ResultSet rs = null;

	  try {	  	
		ps = conn.prepareStatement(sql);
		ps.setString(1,lob);
		rs = ps.executeQuery();				    		
		while (rs.next()) {
		  NameValuePair bean = new NameValuePair();
		  bean.setName(rs.getString("DESCRIPTION"));
		  bean.setValue(rs.getString("CODE"));
		  list.add(bean);
		}
	  } catch (SQLException e) {
		  LOGGER.error(CodeHelper.getStackTrace(e));
			throw e; 
		}finally{
			this.closeResources(ps, rs);
		}	  
	  LOGGER.info("getCodeValuesReqs end");
	  return (list);
	}// getCodeValues
	
	/**
	 * This method will return a subset of client types based on the specified client type code.
	 */
	public Collection getCodeValueDocs(String codeValue) throws SQLException {
		
		LOGGER.info("getCodeValueDocs start");
		Collection list = new ArrayList();
		String sql = "SELECT DOC_CODE AS CODE, DOC_DESC AS DESCRIPTION FROM DOCUMENT_TYPES WHERE DOC_CODE = ?";
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try {	  	
			  ps = conn.prepareStatement(sql);
			  ps.setString(1, codeValue);
			  rs = ps.executeQuery();				    		
			  while (rs.next()) {
				NameValuePair bean = new NameValuePair();
				bean.setName(rs.getString("DESCRIPTION"));
				bean.setValue(rs.getString("CODE"));
				list.add(bean);
			  } 
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e; 
		}finally{
			this.closeResources(ps, rs);
		}	
		LOGGER.info("getCodeValueDocs end");
		return (list);
	}// getCodeValue

	
	public Collection getLobDocTypes(String lob) throws SQLException {
		
		LOGGER.info("getLobDocTypes start");
		Collection list = new ArrayList();
		String sql = "select T1.DOC_DESC as DESCRIPTION from DOCUMENT_TYPES T1, LOB_DOCUMENTS T2 where T1.DOC_CODE = T2.DOC_CODE and T2.LOB_CODE = ? order by T1.DOC_DESC";
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		try{
			ps = conn.prepareStatement(sql);
			ps.setString(1,lob);
			rs = ps.executeQuery();
			while (rs.next()) {
				list.add(rs.getString("DESCRIPTION"));
			}	
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e; 
		}finally{
			this.closeResources(ps, rs);
		}	
		LOGGER.info("getLobDocTypes end");
		return list;		
	}
	
	public Collection getRequirements(String lob) throws SQLException {
		
		LOGGER.info("getRequirements start");
		Collection list = new ArrayList();
		String sql = "select T1.REQT_CODE as code, T1.REQT_DESC as description from REQUIREMENTS T1, LOB_REQUIREMENTS T2 where T1.REQT_CODE = T2.REQT_CODE and T2.LOB_CODE = ? order by T1.REQT_DESC";
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try{
			ps = conn.prepareStatement(sql);
			ps.setString(1,lob);
			rs = ps.executeQuery();
			while (rs.next()) {
				NameValuePair bean = new NameValuePair();
				bean.setName(rs.getString("description"));
				bean.setValue(rs.getString("code"));
				list.add(bean);
			}	
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e; 
		}finally{
			this.closeResources(ps, rs);
		}	
		LOGGER.info("getRequirements end");
		return list;		
	}	
	
	public void insertDocType(DocumentsRequirementsForm docsRequirementsForm) throws SQLException {
		
		LOGGER.info("insertDocType start");
		String sql = "insert into LOB_DOCUMENTS(LOB_CODE,DOC_CODE) values (?,?)";
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		try{
			ps = conn.prepareStatement(sql);
			ps.setString(1,docsRequirementsForm.getLob());
			ps.setString(2,docsRequirementsForm.getDocumentType());
			ps.executeUpdate();
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e; 
		}finally{
			this.closeResources(ps, rs);
			LOGGER.info("insertDocType end");
		}	
	}
	
	public void insertRequirement(DocumentsRequirementsForm docsRequirementsForm) throws SQLException {
		
		LOGGER.info("insertRequirement start");
		String sql = "insert into LOB_REQUIREMENTS(LOB_CODE,REQT_CODE) values (?,?)";

		PreparedStatement ps = null;
		ResultSet rs = null;
		try{
			ps = conn.prepareStatement(sql);
			ps.setString(1,docsRequirementsForm.getLob());
			ps.setString(2,docsRequirementsForm.getRequirement());
			ps.executeUpdate();
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e; 
		}finally{
			this.closeResources(ps, rs);
			LOGGER.info("insertRequirement end");
		}	
	}

}
