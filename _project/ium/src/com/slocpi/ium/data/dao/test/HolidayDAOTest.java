/*
 * Created on Jun 23, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.data.dao.test;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;

import junit.framework.TestCase;

import com.slocpi.ium.data.HolidayData;
import com.slocpi.ium.data.dao.HolidayDAO;
import com.slocpi.ium.data.util.DataSourceProxy;
/*
 * @author Cris Gironella
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class HolidayDAOTest extends TestCase {

	/**
	 * Constructor for HolidayDAOTest.
	 * @param arg0
	 */
	public HolidayDAOTest(String arg0) {
		super(arg0);
	}

	public static void main(String[] args) {
	}

	/*
	 * @see TestCase#setUp()
	 */
	protected void setUp() throws Exception {
		super.setUp();
	}

	/*
	 * @see TestCase#tearDown()
	 */
	protected void tearDown() throws Exception {
		super.tearDown();
	}

	public void testHolidayDAO() {
		//TODO Implement HolidayDAO().
	}

	public void testRetrieveHolidays() throws SQLException {
		Connection conn =new DataSourceProxy().getConnection();
		HolidayDAO testDAO = new HolidayDAO(conn);
		ArrayList test = new ArrayList (testDAO.retrieveHolidays("2004"));
				  
		long holidayId = ((HolidayData)test.get(0)).getHolidayCode();
		String holidayDesc = ((HolidayData)test.get(0)).getHolidayDesc();
		Date holidayDate =((HolidayData)test.get(0)).getHolidayDate();
		long holidayId2 = ((HolidayData)test.get(1)).getHolidayCode();
		String holidayDesc2 = ((HolidayData)test.get(1)).getHolidayDesc();
		Date holidayDate2 =((HolidayData)test.get(1)).getHolidayDate();
		long holidayId3 = ((HolidayData)test.get(2)).getHolidayCode();
		String holidayDesc3 = ((HolidayData)test.get(2)).getHolidayDesc();
		Date holidayDate3 =((HolidayData)test.get(2)).getHolidayDate();
		long holidayId4 = ((HolidayData)test.get(3)).getHolidayCode();
		String holidayDesc4 = ((HolidayData)test.get(3)).getHolidayDesc();
		Date holidayDate4 =((HolidayData)test.get(3)).getHolidayDate();
				
	//check size of the result set
	    assertEquals(4, test.size());						 
		assertEquals(7, holidayId);
	    assertEquals(8, holidayId2);
	    assertEquals(21, holidayId3);
    	assertEquals(16, holidayId4);
    	
   //check that nothing is retrieved when a year is not in the database
        test =testDAO.retrieveHolidays("1998");
        ArrayList expected = new ArrayList();
        expected.clear();
        assertEquals(expected,test);	
	}

	public void testRetrieveHolidayDetail() throws SQLException {
		Connection conn = new DataSourceProxy().getConnection();
		HolidayDAO test = new HolidayDAO(conn);
		HolidayData retHoliday = new HolidayData();

		//testing a value that is known to be in the database	
	    retHoliday = test.retrieveHolidayDetail("11");
	    String holidayName = retHoliday.getHolidayDesc();
	    System.out.println(holidayName);
	    assertEquals("New Year", holidayName);
	    long holId = retHoliday.getHolidayCode();
	    assertEquals(11, holId);
	    Date holDate = retHoliday.getHolidayDate();
	    String holdate = holDate.toString();
	    System.out.println(holdate);
	    assertEquals("2003-01-01", holdate);   
	     
	    
	}

	public void testInsertHoliday() {
		//TODO Implement insertHoliday().
	}

	public void testUpdateHoliday() {
		//TODO Implement updateHoliday().
	}

	public void testRetrieveYears() throws SQLException {
	     Connection conn =  new DataSourceProxy().getConnection();
	     HolidayDAO testDAO = new HolidayDAO(conn);
	     ArrayList actual =  new ArrayList();
	     
	     //actual results
	     actual = testDAO.retrieveYears();
	     int actualLength= actual.size();
	     	    
	    //test size of array    
	     assertEquals(11, actualLength);
	    
	    //test min value returned
	    assertEquals("2000",actual.get(0));
	      
	    //test max value returned
	    assertEquals("2010", actual.get(actualLength-1));
	   	     
	     
	}

}
