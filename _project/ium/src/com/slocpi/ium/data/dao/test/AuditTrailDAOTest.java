/*
 * Created on Jun 25, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.data.dao.test;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;

import junit.framework.TestCase;

import com.slocpi.ium.data.AuditTrailData;
import com.slocpi.ium.data.AuditTrailFilterData;
import com.slocpi.ium.data.dao.AuditTrailDAO;
import com.slocpi.ium.data.util.DataSourceProxy;

/**
 * @author mlua
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class AuditTrailDAOTest extends TestCase {

	/**
	 * Constructor for AuditTrailDAOTest.
	 * @param arg0
	 */
	private Connection conn = null;
	public AuditTrailDAOTest(String arg0) {
		super(arg0);
	}

	public void testRetrieveAuditTrail() throws SQLException {
		conn = (Connection) new DataSourceProxy().getConnection();
		AuditTrailDAO audit = new AuditTrailDAO(conn);
		AuditTrailFilterData filter = new AuditTrailFilterData();

		/* 
		 * Test will retrieve data with transaction type
		 * UPDATE.
		 */

		filter.setTransactionType("UPDATE");
		ArrayList trail = new ArrayList();
		trail = audit.retrieveAuditTrail(filter);
		long transid = ((AuditTrailData)trail.get(0)).getTransId();

		assertTrue(trail.size() > 0);
		assertEquals(trail.size(),23418);
		assertEquals(transid,581);
		
		filter.setTransactionType("abcde");
		trail = audit.retrieveAuditTrail(filter);

		assertTrue(trail.size() == 0);
	}

/*	public void testInsertAuditTrail() throws SQLException {
		conn = (Connection) new DataSourceProxy().getConnection();
		AuditTrailDAO audit = new AuditTrailDAO(conn);
		AuditTrailFilterData filter = new AuditTrailFilterData();
		AuditTrailData data = new AuditTrailData();
		/* 
		 * Test will insert new audit trail data 
		 * 
		 */
		Date date = new Date("6/25/2004");
/*		data.setTransType("DELETE");
		data.setChangeFrom("old");
		data.setChangeTo("new");
		data.setReferenceNumber("IUMDEV");
		data.setTransDate(date);
		data.setTransId(999999);
		data.setTransRec(10);
		data.setUserID("IUMDEV");
		
		filter.setTransactionType("DELETE");
		ArrayList trail = new ArrayList();
		trail = audit.retrieveAuditTrail(filter);
		boolean transid = false; 

		for (int i = 0; i < trail.size();i++){
			if (((AuditTrailData)trail.get(i)).getTransId() == 999999 ){
				transid = true;
				break;
			}
		}
		assertTrue(transid);	
		
	}
*/
/*	public void testDeleteAuditTrails() throws SQLException {
		conn = (Connection) new DataSourceProxy().getConnection();
		AuditTrailDAO audit = new AuditTrailDAO(conn);
		AuditTrailFilterData filter = new AuditTrailFilterData();
		AuditTrailData data = new AuditTrailData();
		/* 
		 * Test will delete audit trail data 
		 * 
		 */
/*		Date date = new Date("6/25/2004");	
		audit.deleteAuditTrails(date,date);
		filter.setStartDate(date);
		filter.setEndDate(date);		
		ArrayList trail = new ArrayList();
		trail = audit.retrieveAuditTrail(filter);
		boolean transdate = true; 

		for (int i = 0; i < trail.size();i++){
			if (((AuditTrailData)trail.get(i)).getTransDate() == date ){
				transdate = false;
				break;
			}
		}
		assertTrue(transdate);
	}
*/
}
