/*
 * Created on Jun 23, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.data.dao.test;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import junit.framework.TestCase;

import com.slocpi.ium.data.DocumentData;
import com.slocpi.ium.data.dao.DocumentDAO;
import com.slocpi.ium.data.util.DataSourceProxy;



/**
 * @author Alain del Rosario
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class DocumentDAOTest extends TestCase {

	/**
	 * Constructor for DocumentDAOTest.
	 * @param arg0
	 */
	public DocumentDAOTest(String arg0) {
		super(arg0);
	}

	public void testRetrieveDocuments() throws Exception {
		Connection conn2 = null;
		conn2 = new DataSourceProxy().getConnection();
		DocumentDAO ddao = new DocumentDAO(conn2);
		ArrayList alist = new ArrayList();
		String refnum = "WFAR0002";
		alist = ddao.retrieveDocuments(refnum);
		assertNotNull(alist);
		assertTrue(alist.size()==1);
		long docId = 82;
		long docId2 = ((DocumentData)alist.get(0)).getDocId();
		assertEquals(docId,docId2);
	}
	
	public void testIsDuplicateCode() throws SQLException{
		boolean isittrue = false;
		String refnum = "ENGEL001";
		String dcode = "APL";
		Connection conn2 = null;
		conn2 = new DataSourceProxy().getConnection();
		DocumentDAO ddao = new DocumentDAO(conn2);
		isittrue = ddao.isDuplicateCode(refnum,dcode);
	}

}
