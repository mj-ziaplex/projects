/*
 * Created on Dec 18, 2003
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.data.dao;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.ResourceBundle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.slocpi.ium.data.ARWithPendingReqtData;
import com.slocpi.ium.data.ARWithPendingReqtFilterData;
import com.slocpi.ium.data.AssessmentRequestData;
import com.slocpi.ium.data.PolicyRequirementsData;
import com.slocpi.ium.data.RequirementData;
import com.slocpi.ium.data.RequirementsOrderedData;
import com.slocpi.ium.data.RequirementsOrderedFilterData;
import com.slocpi.ium.data.StatusData;
import com.slocpi.ium.data.SunLifeOfficeData;
import com.slocpi.ium.data.UserProfileData;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.DateHelper;
import com.slocpi.ium.util.IUMConstants;
import com.slocpi.ium.util.StringHelper;
import com.slocpi.ium.util.ValueConverter;

/**
 * @author Cris
 *
 * Container for SQLs performed on a requirement
 * ***************************************************
 * @author Shellz - Sept 25, 2014
 * @version 1.1
 * TODO -Created a new method that only fetch pr_test_date in policy_requirements
 * 		-pr_test_date will be used in cancellation of a requirement
 * ***************************************************
 * @author Shellz - Sept 25, 2014
 * @version 1.2
 * TODO -Made changes to method changeStatus and resetFollowUp
 * 		-Added logging and proper structuring
 */
public class PolicyRequirementDAO extends BaseDAO {

	private static final Logger LOGGER = LoggerFactory.getLogger(PolicyRequirementDAO.class);

	public PolicyRequirementDAO() {}
	
	/**
	 * 
	 * @param policyNumber
	 * @return result
	 * @throws Exception
	 */
	public boolean areRequirementsComplete(String policyNumber) throws Exception{
		
		LOGGER.info("areRequirementsComplete start");
		boolean result = false;
		String sql = "SELECT COUNT(*) FROM POLICY_REQUIREMENTS WHERE UAR_REFERENCE_NUM = ? AND PR_STATUS_ID IN (?,?,?)";
		
		PreparedStatement prepStmt = null;
		ResultSet rs = null;
		Connection conn = null;
		
		try {
			conn = getConnection();	
			prepStmt = conn.prepareStatement(sql);
			prepStmt.setString(1, policyNumber);
			prepStmt.setLong(2, IUMConstants.STATUS_ORDERED);
			prepStmt.setLong(3, IUMConstants.STATUS_NTO);
			prepStmt.setLong(4, IUMConstants.STATUS_SCC);

			rs = prepStmt.executeQuery();
			if (rs.next()){
				if (rs.getInt(1) > 0){
					result = false;	//Partial
				}else{
					result = true;	//Complete
				}
			}
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));	
			  throw e; 
		}finally{
			  closeResources(conn, prepStmt, rs);
		}		
		LOGGER.info("areRequirementsComplete end");
		return result;
	}
	
	/**
	 * 
	 * @param prd
	 * @return reqtStatusId
	 * @throws Exception
	 */
	public long getStatus(PolicyRequirementsData prd) throws Exception{
		
		LOGGER.info("getStatus start 1");
		long reqtStatusId = 0;
		String parameter = "";
		
		if(prd.getLevel().equalsIgnoreCase(IUMConstants.LEVEL_CLIENT)){
			parameter = "  AND PR_CLIENT_ID = ?";
		}else{
//			parameter = "  AND PR_ADMIN_SEQUENCE_NUM = ?";
		}
		
		String sql = "SELECT PR_STATUS_ID FROM POLICY_REQUIREMENTS WHERE UAR_REFERENCE_NUM = ? " +
						"  AND PR_LEVEL = ?" +
						"  AND PR_REQT_CODE = ?" +	parameter;
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection conn = null;
		
		try {
			conn = getConnection();	
			ps = conn.prepareStatement(sql);
			
			ps.setString(1, prd.getReferenceNumber());
			ps.setString(2, prd.getLevel());
			ps.setString(3, prd.getRequirementCode());
			
			if(prd.getLevel().equalsIgnoreCase(IUMConstants.LEVEL_CLIENT)){
				ps.setString(4, prd.getClientId());
			}else{
//				ps.setLong(4,prd.getSequenceNumber());
			}
			
			rs = ps.executeQuery();
			if(rs.next()){
				if(rs.getLong("PR_STATUS_ID") != 0){
					reqtStatusId = rs.getLong("PR_STATUS_ID");
				}				
			}
			
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			  throw e; 
		}finally{
			  closeResources(conn, ps, rs);
		}	
		LOGGER.info("getStatus end 1");
		return reqtStatusId;
	}
	
	/**
	 * 
	 * @param prd
	 * @return reqtStatusId
	 * @throws Exception
	 */
	public long getStatus(long reqtId) throws Exception{
		
		LOGGER.info("getStatus start 2");
		long reqtStatusId = 0;
				
		String sql = "SELECT PR_STATUS_ID FROM POLICY_REQUIREMENTS WHERE PR_REQUIREMENT_ID = ? ";
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection conn = null;
		
		try {
			conn = getConnection();	
			ps = conn.prepareStatement(sql);
			
			ps.setLong(1, reqtId);
						
			rs = ps.executeQuery();
			
			if(rs.next()){
				if(rs.getLong("PR_STATUS_ID") != 0){
					reqtStatusId = rs.getLong("PR_STATUS_ID");
				}				
			}
			
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));	
			  throw e; 
		}finally{
			  closeResources(conn, ps, rs);
		}
		LOGGER.info("getStatus end 2");
		return reqtStatusId;
	}
	
	private long getOldStatus(PolicyRequirementsData prd) throws Exception{
		
		LOGGER.info("getOldStatus start");
		long reqtStatusId = 0;				
		String sql = "SELECT PR_OLD_STATUS_ID FROM POLICY_REQUIREMENTS WHERE PR_REQUIREMENT_ID = ? ";
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection conn = null;
		
		try {
			conn = getConnection();	
			ps = conn.prepareStatement(sql);
			
			ps.setLong(1, prd.getRequirementId());
			
			rs = ps.executeQuery();
			if(rs.next()){
				reqtStatusId = rs.getLong("PR_OLD_STATUS_ID");			
			}
			
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));	
			  throw e; 
		}finally{
			  closeResources(conn, ps, rs);
		}
		LOGGER.info("getOldStatus end");
		return reqtStatusId;
	}
	
	// added BJ Edward M. Taduran
	private long getOldStatusWoutReqId(PolicyRequirementsData prd) throws Exception{
		
		LOGGER.info("getOldStatusWoutReqId start");
		long reqtStatusId = 0;				
		String sql = "SELECT PR_OLD_STATUS_ID FROM POLICY_REQUIREMENTS WHERE  UAR_REFERENCE_NUM = ? " +
				"AND PR_IMG_REFERENCE_NUM = ? AND PR_REQT_CODE = ? AND PR_ADMIN_SEQUENCE_NUM = ?";

		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection conn = null;
		
		try {
			conn = getConnection();	
			ps = conn.prepareStatement(sql);
			
			ps.setString(1,prd.getReferenceNumber());
			ps.setString(2,prd.getImageRef());
			ps.setString(3,prd.getRequirementCode());
			ps.setLong(4,prd.getSequenceNumber());
			
			rs = ps.executeQuery();
			if(rs.next()){
				reqtStatusId = rs.getLong("PR_OLD_STATUS_ID");			
			}
			
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));	
			  throw e; 
		}finally{
			  closeResources(conn, ps, rs);
		}
		
		LOGGER.info("getOldStatusWoutReqId end");
		return reqtStatusId;
	}
	
	/**
	 * @param requirement
	 * @return
	 * @throws Exception
	 */
	public long updateRequirement(long reqId, PolicyRequirementsData requirement) throws Exception{
		
		LOGGER.info("updateRequirement start");
		String sql = "UPDATE POLICY_REQUIREMENTS SET PR_FOLLOW_UP_DATE = ? WHERE pr_requirement_id = ?";
		
		PreparedStatement stmt = null;
		Connection conn = null;
		
		try {
			conn = getConnection();	
			conn.setAutoCommit(false);
			stmt = conn.prepareStatement(sql);
			stmt.setDate(1,DateHelper.sqlDate(requirement.getFollowUpDate()));
			stmt.setLong(2, reqId);
			stmt.executeUpdate();
			conn.commit();
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			if(conn != null){
				conn.rollback();
			}
		}finally{
			closeResources(conn, stmt, null);
		}
		LOGGER.info("updateRequirement end");
		return reqId;
	}
	
	/**
	 * @param requirement
	 * @return
	 * @throws Exception
	 */
	public long insertRequirement(PolicyRequirementsData requirement) throws Exception{
		
		LOGGER.info("insertRequirement start");
		long requirementID = 0;
		String sql = "INSERT INTO POLICY_REQUIREMENTS " +
		"(UAR_REFERENCE_NUM, PR_REQUIREMENT_ID, PR_ADMIN_SEQUENCE_NUM, PR_REQT_CODE, PR_LEVEL" +
		",PR_ADMIN_CREATE_DATE, PR_COMPLT_REQT_IND, PR_STATUS_ID, PR_STATUS_DATE, PR_ADMIN_UPDTD_BY" +
		",PR_ADMIN_UPDTD_DATE, PR_CLIENT_TYPE, PR_DESIGNATION, PR_TEST_DATE, PR_TEST_RESULT_CODE" +
		",PR_TEST_RESULT_DESC, PR_RESOLVED_IND, PR_FOLLOW_UP_NUM, PR_FOLLOW_UP_DATE, PR_VALIDITY_DATE" +
		",PR_PAID_IND, PR_NEW_TEST_ONLY, PR_ORDER_DATE, PR_DATE_SENT, PR_RECEIVED_DATE" +
		",PR_AUTO_ORDER_IND, PR_FLD_COMMENT_IND, PR_COMMENTS, PR_CLIENT_ID, CREATED_BY, CREATED_DATE, PR_IMG_REFERENCE_NUM)" +
		" VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		
		PreparedStatement prepStmt = null;
		Connection conn = null;
		
		try {
			conn = getConnection();	
			conn.setAutoCommit(false);
			requirementID = this.getRequirementID();
			prepStmt = conn.prepareStatement(sql);
			prepStmt.setString(1,requirement.getReferenceNumber());
			prepStmt.setLong(2,requirementID);
			prepStmt.setLong(3,requirement.getSequenceNumber());
			prepStmt.setString(4,requirement.getRequirementCode());
			prepStmt.setString(5,requirement.getLevel());
			prepStmt.setDate(6,DateHelper.sqlDate(requirement.getABACUScreateDate()));
			prepStmt.setBoolean(7,requirement.isCompletedRequirementInd());
			prepStmt.setLong(8,requirement.getStatus().getStatusId());
			prepStmt.setDate(9,DateHelper.sqlDate(requirement.getStatusDate()));
			prepStmt.setString(10,requirement.getABACUSUpdatedBy());
			prepStmt.setDate(11,DateHelper.sqlDate(requirement.getABACUSUpdateDate()));
			prepStmt.setString(12,requirement.getClientType());
			prepStmt.setString(13,requirement.getDesignation());
			prepStmt.setDate(14,DateHelper.sqlDate(requirement.getTestDate()));
			prepStmt.setString(15,requirement.getTestResultCode());
			prepStmt.setString(16,requirement.getTestResultDesc());
			prepStmt.setBoolean(17,requirement.isResolveInd());
			prepStmt.setLong(18,requirement.getFollowUpNumber());
			prepStmt.setDate(19,DateHelper.sqlDate(requirement.getFollowUpDate()));
			prepStmt.setDate(20,DateHelper.sqlDate(requirement.getValidityDate()));
			prepStmt.setBoolean(21,requirement.isPaidInd());
			prepStmt.setBoolean(22,requirement.getNewTestOnly());
			prepStmt.setDate(23,DateHelper.sqlDate(requirement.getOrderDate()));
			
			prepStmt.setDate(24,DateHelper.sqlDate(requirement.getDateSent()));
			
			prepStmt.setDate(25,DateHelper.sqlDate(requirement.getReceiveDate()));
			prepStmt.setBoolean(26,requirement.getAutoOrderInd());
			prepStmt.setBoolean(27,requirement.getFldCommentInd());
			prepStmt.setString(28,requirement.getComments());
			prepStmt.setString(29,requirement.getClientId());
			prepStmt.setString(30,requirement.getCreatedBy());
			prepStmt.setTimestamp(31,DateHelper.sqlTimestamp(requirement.getCreateDate()));
			prepStmt.setString(32, requirement.getImageRef());
			prepStmt.executeUpdate();
			conn.commit();
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			if(conn != null){
				conn.rollback();
			}
			throw e; 
		}finally{
			  closeResources(conn, prepStmt, null);
		}
		LOGGER.info("insertRequirement end");
		return requirementID;
	}
	
	public long insertRequirementWMS(PolicyRequirementsData requirement) throws Exception{
		
		LOGGER.info("insertRequirementWMS start");
		long requirementID = 0;
		String sql = "INSERT INTO POLICY_REQUIREMENTS " +
		"(UAR_REFERENCE_NUM, PR_REQUIREMENT_ID, PR_ADMIN_SEQUENCE_NUM, PR_REQT_CODE, PR_LEVEL" +
		",PR_ADMIN_CREATE_DATE, PR_COMPLT_REQT_IND, PR_STATUS_ID, PR_STATUS_DATE, PR_ADMIN_UPDTD_BY" +
		",PR_ADMIN_UPDTD_DATE, PR_CLIENT_TYPE, PR_DESIGNATION, PR_TEST_DATE, PR_TEST_RESULT_CODE" +
		",PR_TEST_RESULT_DESC, PR_RESOLVED_IND, PR_FOLLOW_UP_NUM, PR_FOLLOW_UP_DATE, PR_VALIDITY_DATE" +
		",PR_PAID_IND, PR_NEW_TEST_ONLY, PR_ORDER_DATE, PR_DATE_SENT, PR_RECEIVED_DATE" +
		",PR_AUTO_ORDER_IND, PR_FLD_COMMENT_IND, PR_COMMENTS, PR_CLIENT_ID, CREATED_BY, CREATED_DATE" + ", PR_IMG_REFERENCE_NUM, PR_SCAN_DATE, PR_OLD_STATUS_ID" +
		")" +
		" VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		
		PreparedStatement prepStmt = null;
		
		Connection conn = null;
		
		String reqLevel = requirement.getLevel();
		if ("I".equalsIgnoreCase(reqLevel)) {
			reqLevel = "C";
			requirement.setClientType("I");
		} else if ("O".equalsIgnoreCase(reqLevel)) {
			reqLevel = "C";
			requirement.setClientType("O");
		}
		
		try {
			conn = getConnection();	
			conn.setAutoCommit(false);
			requirementID = this.getRequirementID();
			prepStmt = conn.prepareStatement(sql);
			prepStmt.setString(1,requirement.getReferenceNumber());
			prepStmt.setLong(2,requirementID);
			prepStmt.setLong(3,requirement.getSequenceNumber());
			prepStmt.setString(4,requirement.getRequirementCode());
			prepStmt.setString(5,reqLevel);
			prepStmt.setDate(6,DateHelper.sqlDate(requirement.getABACUScreateDate()));
			prepStmt.setBoolean(7,requirement.isCompletedRequirementInd());
			prepStmt.setLong(8,requirement.getStatus().getStatusId());
			prepStmt.setDate(9,DateHelper.sqlDate(requirement.getStatusDate()));
			prepStmt.setString(10,requirement.getABACUSUpdatedBy());
			prepStmt.setDate(11,DateHelper.sqlDate(requirement.getABACUSUpdateDate()));
			prepStmt.setString(12,requirement.getClientType());
			prepStmt.setString(13,requirement.getDesignation());
			prepStmt.setDate(14,DateHelper.sqlDate(requirement.getTestDate()));
			prepStmt.setString(15,requirement.getTestResultCode());
			prepStmt.setString(16,requirement.getTestResultDesc());
			prepStmt.setBoolean(17,requirement.isResolveInd());
			prepStmt.setLong(18,requirement.getFollowUpNumber());
			prepStmt.setDate(19,DateHelper.sqlDate(requirement.getFollowUpDate()));
			prepStmt.setDate(20,DateHelper.sqlDate(requirement.getValidityDate()));
			prepStmt.setBoolean(21,requirement.isPaidInd());
			prepStmt.setBoolean(22,requirement.getNewTestOnly());
			prepStmt.setDate(23,DateHelper.sqlDate(requirement.getOrderDate()));
			prepStmt.setDate(24,DateHelper.sqlDate(requirement.getDateSent()));
			prepStmt.setDate(25,DateHelper.sqlDate(requirement.getReceiveDate()));
			prepStmt.setBoolean(26,requirement.getAutoOrderInd());
			prepStmt.setBoolean(27,requirement.getFldCommentInd());
			prepStmt.setString(28,requirement.getComments());
			prepStmt.setString(29,requirement.getClientId());
			prepStmt.setString(30,requirement.getUpdatedBy());
			prepStmt.setTimestamp(31,DateHelper.sqlTimestamp(requirement.getCreateDate()));
			prepStmt.setString(32, requirement.getImageRef());
			prepStmt.setTimestamp(33, DateHelper.sqlTimestamp(requirement.getScanDate()));
			prepStmt.setLong(34, requirement.getStatus().getOldStatusId());
			
			prepStmt.executeUpdate();
			conn.commit();
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			if(conn != null){
				conn.rollback();
			}
			 throw e; 
		}finally{
			  closeResources(conn, prepStmt, null);
		}
		LOGGER.info("insertRequirementWMS end");
		return requirementID;
	}
	
	/**
	 * @return
	 * @throws SQLException
	 */
	private long getRequirementID() throws SQLException {
		
		LOGGER.info("getRequirementID start");
		long requirementID = 0;
		String sql = "SELECT SEQ_REQUIREMENT_FORM.NEXTVAL REQUIREMENT_ID FROM DUAL";

		PreparedStatement ps = null;
		ResultSet rs = null;		
		Connection conn = null;
		
		try {
			conn = getConnection();	
			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery();
			if (rs.next()) {
				requirementID = rs.getLong("REQUIREMENT_ID");
			} else {
				LOGGER.warn("No sequence number retrieved.");
				throw new SQLException("No sequence number retrieved.");
			}
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			  throw e; 
		}finally{
			  closeResources(conn, ps, rs);
		}
		LOGGER.info("getRequirementID end");
		return requirementID;
	}

	public ArrayList retrieveOwnerIsInsuredRequirements(String clientId, String refNo) throws Exception {
		
		LOGGER.info("retrieveOwnerIsInsuredRequirements start");
		ArrayList requirements = new ArrayList();
		String fields = "PR_REQUIREMENT_ID, UAR_REFERENCE_NUM, PR_ADMIN_SEQUENCE_NUM, PR_REQT_CODE,PR_LEVEL, PR_ADMIN_CREATE_DATE, " +
						   "PR_COMPLT_REQT_IND, PR_STATUS_ID, STAT_DESC, PR_STATUS_DATE, PR_ADMIN_UPDTD_BY, PR_ADMIN_UPDTD_DATE, " +
						   "PR_CLIENT_TYPE, PR_DESIGNATION, PR_TEST_DATE, PR_TEST_RESULT_CODE, PR_TEST_RESULT_DESC, PR_RESOLVED_IND, " +
						   "PR_FOLLOW_UP_NUM, PR_FOLLOW_UP_DATE, PR_VALIDITY_DATE, PR_PAID_IND, PR_NEW_TEST_ONLY, PR_ORDER_DATE, " +
						   "PR_DATE_SENT, PR_CCAS_SUGGEST_IND, PR_RECEIVED_DATE, PR_AUTO_ORDER_IND, PR_FLD_COMMENT_IND, PR_COMMENTS, " +
						   "PR_CLIENT_ID, PR_IMG_REFERENCE_NUM, POLICY_REQUIREMENTS.CREATED_BY, POLICY_REQUIREMENTS.CREATED_DATE, " +
						   "POLICY_REQUIREMENTS.UPDATED_BY, POLICY_REQUIREMENTS.UPDATED_DATE ";
		String sql = 
			"SELECT " + fields +
			 "FROM STATUS, POLICY_REQUIREMENTS " +
			"WHERE PR_STATUS_ID = STAT_ID " +
			  "AND PR_CLIENT_ID = ? " +
			  "AND PR_LEVEL = 'C' " +
			"UNION " +
		   "SELECT " + fields +
			"FROM STATUS, POLICY_REQUIREMENTS " +
		   "WHERE PR_STATUS_ID = STAT_ID " +
		   	 "AND UAR_REFERENCE_NUM = ? " +
		     "AND PR_LEVEL = 'P'" +
		   "ORDER BY CREATED_DATE DESC";

		PreparedStatement ps = null;
		ResultSet rs = null;		
		Connection conn = null;
		
		try {
			conn = getConnection();	
			ps = conn.prepareStatement(sql);
			ps.setString(1, clientId);
			ps.setString(2, refNo);
			rs = ps.executeQuery();
			while (rs.next()) {
				requirements.add(getPolicyRequirementsData(rs));	
			}
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
 		} finally {
 			closeResources(conn, ps, rs);
 		}
		LOGGER.info("retrieveOwnerIsInsuredRequirements end");
		return requirements;
	}
	
	public ArrayList retrieveOwnerIsNotInsuredRequirements(String ownerClientId, String insuredClientId, String refNo) throws Exception {
		
		LOGGER.info("retrieveOwnerIsNotInsuredRequirements start");
		ArrayList requirements = new ArrayList();
		ResourceBundle rb = ResourceBundle.getBundle(IUMConstants.REQUIREMENT_CODES_PROPERTY_FILE);
		String reqCodes = rb.getString(IUMConstants.REQUIREMENT_CODES_CONFIG);
		String requirementCodes = PolicyRequirementDAOHelper.getRequirementCodes(reqCodes);
	
		String fields = "PR_REQUIREMENT_ID, UAR_REFERENCE_NUM, PR_ADMIN_SEQUENCE_NUM, PR_REQT_CODE,PR_LEVEL, PR_ADMIN_CREATE_DATE, " +
					       "PR_COMPLT_REQT_IND, PR_STATUS_ID, STAT_DESC, PR_STATUS_DATE, PR_ADMIN_UPDTD_BY, PR_ADMIN_UPDTD_DATE, " +
			   		       "PR_CLIENT_TYPE, PR_DESIGNATION, PR_TEST_DATE, PR_TEST_RESULT_CODE, PR_TEST_RESULT_DESC, PR_RESOLVED_IND, " +
					       "PR_FOLLOW_UP_NUM, PR_FOLLOW_UP_DATE, PR_VALIDITY_DATE, PR_PAID_IND, PR_NEW_TEST_ONLY, PR_ORDER_DATE, " +
					       "PR_DATE_SENT, PR_CCAS_SUGGEST_IND, PR_RECEIVED_DATE, PR_AUTO_ORDER_IND, PR_FLD_COMMENT_IND, PR_COMMENTS, " +
					       "PR_CLIENT_ID, PR_IMG_REFERENCE_NUM, POLICY_REQUIREMENTS.CREATED_BY, POLICY_REQUIREMENTS.CREATED_DATE, " +
					       "POLICY_REQUIREMENTS.UPDATED_BY, POLICY_REQUIREMENTS.UPDATED_DATE ";
		String sql = 
			"SELECT " + fields +
  		      "FROM STATUS, POLICY_REQUIREMENTS " +
		     "WHERE PR_STATUS_ID = STAT_ID " +
		       "AND PR_CLIENT_ID = ? " +
		       "AND PR_LEVEL = 'C' " +
		     "UNION " + 
			"SELECT " + fields +
	 		  "FROM STATUS, POLICY_REQUIREMENTS " +
			 "WHERE PR_STATUS_ID = STAT_ID " +
			   "AND PR_CLIENT_ID = ? " +
			   "AND PR_LEVEL = 'C' " +
			   "AND PR_REQT_CODE IN (" + requirementCodes + ") " +
	   	     "UNION " + 
			"SELECT " + fields +
			  "FROM STATUS, POLICY_REQUIREMENTS " +
			 "WHERE PR_STATUS_ID = STAT_ID " +
			   "AND UAR_REFERENCE_NUM = ? " +
			   "AND PR_LEVEL = 'P' " +
			 "ORDER BY CREATED_DATE DESC";

		PreparedStatement ps = null;
		ResultSet rs = null;		
		Connection conn = null;
		
		try {
			conn = getConnection();	
			ps = conn.prepareStatement(sql);
			ps.setString(1, insuredClientId);
			ps.setString(2, ownerClientId);
			ps.setString(3, refNo);
			rs = ps.executeQuery();
			while (rs.next()) {
				requirements.add(getPolicyRequirementsData(rs));	
			}
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
 		} finally {
 			closeResources(conn, ps, rs);
 		}
		LOGGER.info("retrieveOwnerIsNotInsuredRequirements end");
		return requirements;
	}
	
	/*public ArrayList retrieveChildIsInsuredRequirements(String refNo) throws Exception {*/
	public ArrayList retrieveChildIsInsuredRequirements(String ownerClientId, String insuredClientId, String refNo) throws Exception {
		
		LOGGER.info("retrieveChildIsInsuredRequirements start");
		ArrayList requirements = new ArrayList();
		String fields = "PR_REQUIREMENT_ID, UAR_REFERENCE_NUM, PR_ADMIN_SEQUENCE_NUM, PR_REQT_CODE,PR_LEVEL, PR_ADMIN_CREATE_DATE, " +
						   "PR_COMPLT_REQT_IND, PR_STATUS_ID, STAT_DESC, PR_STATUS_DATE, PR_ADMIN_UPDTD_BY, PR_ADMIN_UPDTD_DATE, " +
						   "PR_CLIENT_TYPE, PR_DESIGNATION, PR_TEST_DATE, PR_TEST_RESULT_CODE, PR_TEST_RESULT_DESC, PR_RESOLVED_IND, " +
						   "PR_FOLLOW_UP_NUM, PR_FOLLOW_UP_DATE, PR_VALIDITY_DATE, PR_PAID_IND, PR_NEW_TEST_ONLY, PR_ORDER_DATE, " +
						   "PR_DATE_SENT, PR_CCAS_SUGGEST_IND, PR_RECEIVED_DATE, PR_AUTO_ORDER_IND, PR_FLD_COMMENT_IND, PR_COMMENTS, " +
						   "PR_CLIENT_ID, PR_IMG_REFERENCE_NUM, POLICY_REQUIREMENTS.CREATED_BY, POLICY_REQUIREMENTS.CREATED_DATE, " +
						   "POLICY_REQUIREMENTS.UPDATED_BY, POLICY_REQUIREMENTS.UPDATED_DATE ";
		String sql = 
			"SELECT " + fields +
			  "FROM STATUS, POLICY_REQUIREMENTS " +
			 "WHERE PR_STATUS_ID = STAT_ID " +
			   "AND PR_CLIENT_ID = ? " +
			   "AND PR_LEVEL = 'C' " +
			 "UNION " +
			"SELECT " + fields +
			  "FROM STATUS, POLICY_REQUIREMENTS " +
			 "WHERE PR_STATUS_ID = STAT_ID " +
			   "AND PR_CLIENT_ID = ? " +
			   "AND PR_LEVEL = 'C' " +
			 "UNION " +
			"SELECT " + fields +
			  "FROM STATUS, POLICY_REQUIREMENTS " +
			 "WHERE PR_STATUS_ID = STAT_ID " +
			   "AND UAR_REFERENCE_NUM = ? " +
			   "AND PR_LEVEL = 'P' " +
			 "ORDER BY CREATED_DATE DESC";
			
		PreparedStatement ps = null;
		ResultSet rs = null;		
		Connection conn = null;
		
		try {
			conn = getConnection();	
			ps = conn.prepareStatement(sql);
			ps.setString(1, insuredClientId);
			ps.setString(2, ownerClientId);
			ps.setString(3, refNo);
			rs = ps.executeQuery();
			while (rs.next()) {
				requirements.add(getPolicyRequirementsData(rs));	
			}
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
 		} finally {
 			closeResources(conn, ps, rs);
 		}
		LOGGER.info("retrieveChildIsInsuredRequirements end");
		return requirements;
	}
	
	private PolicyRequirementsData getPolicyRequirementsData(ResultSet rs) throws SQLException {
		
		LOGGER.info("getPolicyRequirementsData start");
		PolicyRequirementsData policyReqData = new PolicyRequirementsData();
		policyReqData.setSequenceNumber(rs.getLong("pr_admin_sequence_num"));
		policyReqData.setReferenceNumber(rs.getString("uar_reference_num"));
		policyReqData.setRequirementId(rs.getLong("pr_requirement_id"));
		policyReqData.setRequirementCode(rs.getString("pr_reqt_code"));
		policyReqData.setLevel(rs.getString("pr_level"));
		policyReqData.setABACUScreateDate(DateHelper.utilDate(rs.getDate("pr_admin_create_date")));
		policyReqData.setCompletedRequirementInd(rs.getBoolean("pr_complt_reqt_ind"));
		policyReqData.setStatusDate(DateHelper.utilDate(rs.getDate("pr_status_date")));  
		policyReqData.setABACUSUpdatedBy(rs.getString("pr_admin_updtd_by"));
		policyReqData.setABACUSUpdateDate(DateHelper.utilDate(rs.getDate("pr_admin_updtd_date")));
		policyReqData.setClientType(rs.getString("pr_client_type"));
		policyReqData.setClientId(rs.getString("pr_client_id"));
		policyReqData.setDesignation(rs.getString("pr_designation"));
		policyReqData.setTestDate(DateHelper.utilDate(rs.getDate("pr_test_date")));
		policyReqData.setTestResultCode(rs.getString("pr_test_result_code"));
		policyReqData.setTestResultDesc(rs.getString("pr_test_result_desc"));
		policyReqData.setResolveInd(rs.getBoolean("pr_resolved_ind"));
		policyReqData.setFollowUpNumber(rs.getLong("pr_follow_up_num"));
		policyReqData.setFollowUpDate(DateHelper.utilDate(rs.getDate("pr_follow_up_date")));
		policyReqData.setValidityDate(DateHelper.utilDate(rs.getDate("pr_validity_date")));
		policyReqData.setPaidInd(rs.getBoolean("pr_paid_ind"));
		policyReqData.setNewTestOnly(rs.getBoolean("pr_new_test_only"));
		policyReqData.setOrderDate(DateHelper.utilDate(rs.getDate("pr_order_date")));
		policyReqData.setDateSent(DateHelper.utilDate(rs.getDate("pr_date_sent")));
		policyReqData.setCCASuggestInd(rs.getBoolean("pr_ccas_suggest_ind"));
		policyReqData.setReceiveDate(DateHelper.utilDate(rs.getDate("pr_received_date")));
		policyReqData.setAutoOrderInd(rs.getBoolean("pr_auto_order_ind"));
		policyReqData.setFldCommentInd(rs.getBoolean("pr_fld_comment_ind"));
		policyReqData.setComments(rs.getString("pr_comments")); 
		policyReqData.setCreatedBy(rs.getString("created_by"));
		policyReqData.setCreateDate(rs.getTimestamp("created_date"));
		policyReqData.setUpdatedBy(rs.getString("updated_by"));
		policyReqData.setUpdateDate(rs.getTimestamp("updated_date"));
		policyReqData.setImageRef(rs.getString("pr_img_reference_num"));
		StatusData statusData = new StatusData();
		statusData.setStatusId(rs.getLong("pr_status_id"));
		statusData.setStatusDesc(rs.getString("stat_desc"));
		policyReqData.setStatus(statusData);
		LOGGER.info("getPolicyRequirementsData end");
		return policyReqData;
	}


	/**
	 * @param refNo
	 * @return
	 * @throws Exception
	 */
	public ArrayList retrieveRequirements(String refNo) throws Exception {
		
		LOGGER.info("retrieveRequirements end");
		ArrayList list = new ArrayList();
        
		AssessmentRequestDAO ARDAO = new AssessmentRequestDAO();
		AssessmentRequestData ARData = ARDAO.retrieveRequestDetail(refNo);
		
		String sql = "SELECT " +
				"PR_REQUIREMENT_ID, " +
				"UAR_REFERENCE_NUM, " +
				"PR_ADMIN_SEQUENCE_NUM, " +
				"PR_REQT_CODE,PR_LEVEL, " +
				"PR_ADMIN_CREATE_DATE, " +
				"PR_COMPLT_REQT_IND, " +
				"PR_STATUS_ID, " +
				"STAT_DESC, " +
				"PR_STATUS_DATE, " +
				"PR_ADMIN_UPDTD_BY, " +
				"PR_ADMIN_UPDTD_DATE, " +
				"PR_CLIENT_TYPE, " +
				"PR_DESIGNATION, " +
				"PR_TEST_DATE, " +
				"PR_TEST_RESULT_CODE, " +
				"PR_TEST_RESULT_DESC, " +
				"PR_RESOLVED_IND, " +
				"PR_FOLLOW_UP_NUM, " +
				"PR_FOLLOW_UP_DATE, " +
				"PR_VALIDITY_DATE, " +
				"PR_PAID_IND, " +
				"PR_NEW_TEST_ONLY, " +
				"PR_ORDER_DATE, " +
				"PR_DATE_SENT, " +
				"PR_CCAS_SUGGEST_IND, " +
				"PR_RECEIVED_DATE, " +
				"PR_AUTO_ORDER_IND, " +
				"PR_FLD_COMMENT_IND, " +
				"PR_COMMENTS, " +
				"PR_CLIENT_ID, " +
				"PR_IMG_REFERENCE_NUM, " + 
				"POLICY_REQUIREMENTS.CREATED_BY, " +
				"POLICY_REQUIREMENTS.CREATED_DATE, " +
				"POLICY_REQUIREMENTS.UPDATED_BY, " +
				"POLICY_REQUIREMENTS.UPDATED_DATE " +
			"FROM STATUS, POLICY_REQUIREMENTS " +
			"WHERE (PR_STATUS_ID = STAT_ID AND UAR_REFERENCE_NUM = ?) " +
			 "OR ((PR_RECEIVED_DATE IS NULL or (PR_RECEIVED_DATE IS NOT NULL AND " + 
			 "(PR_VALIDITY_DATE IS NULL or TRUNC(PR_VALIDITY_DATE) >= TRUNC(SYSDATE)))) AND STAT_TYPE = ? " + 
			 "AND PR_CLIENT_ID in(? , ?) AND UAR_REFERENCE_NUM != ? AND PR_LEVEL = ? and PR_STATUS_ID = STAT_ID) " +
			 "ORDER BY CREATED_DATE DESC ";

		PreparedStatement ps = null;
		ResultSet rs = null;		
		Connection conn = null;
		
		try {
			conn = getConnection();	
			ps = conn.prepareStatement(sql);
			ps.setString(1, refNo);
			ps.setString(2, IUMConstants.STAT_TYPE_NM);
			ps.setString(3, ARData.getInsured().getClientId());
			ps.setString(4, ARData.getOwner().getClientId());
			ps.setString(5, refNo);
			ps.setString(6, IUMConstants.LEVEL_CLIENT);
			
			rs = ps.executeQuery();
			
			while (rs.next()) {
				PolicyRequirementsData policyReqData = new PolicyRequirementsData();
				policyReqData.setSequenceNumber(rs.getLong("pr_admin_sequence_num"));
				policyReqData.setReferenceNumber(rs.getString("uar_reference_num"));
				policyReqData.setRequirementId(rs.getLong("pr_requirement_id"));
				policyReqData.setRequirementCode(rs.getString("pr_reqt_code"));
				policyReqData.setLevel(rs.getString("pr_level"));
				policyReqData.setABACUScreateDate(DateHelper.utilDate(rs.getDate("pr_admin_create_date")));
				policyReqData.setCompletedRequirementInd(rs.getBoolean("pr_complt_reqt_ind"));
				StatusData statusData = new StatusData();
				statusData.setStatusId(rs.getLong("pr_status_id"));
				statusData.setStatusDesc(rs.getString("stat_desc"));
				policyReqData.setStatus(statusData);				
				policyReqData.setStatusDate(DateHelper.utilDate(rs.getDate("pr_status_date")));  
				policyReqData.setABACUSUpdatedBy(rs.getString("pr_admin_updtd_by"));
				policyReqData.setABACUSUpdateDate(DateHelper.utilDate(rs.getDate("pr_admin_updtd_date")));
				policyReqData.setClientType(rs.getString("pr_client_type"));
				policyReqData.setClientId(rs.getString("pr_client_id"));
				policyReqData.setDesignation(rs.getString("pr_designation"));
				policyReqData.setTestDate(DateHelper.utilDate(rs.getDate("pr_test_date")));
				policyReqData.setTestResultCode(rs.getString("pr_test_result_code"));
				policyReqData.setTestResultDesc(rs.getString("pr_test_result_desc"));
				policyReqData.setResolveInd(rs.getBoolean("pr_resolved_ind"));
				policyReqData.setFollowUpNumber(rs.getLong("pr_follow_up_num"));
				policyReqData.setFollowUpDate(DateHelper.utilDate(rs.getDate("pr_follow_up_date")));
				policyReqData.setValidityDate(DateHelper.utilDate(rs.getDate("pr_validity_date")));
				policyReqData.setPaidInd(rs.getBoolean("pr_paid_ind"));
				policyReqData.setNewTestOnly(rs.getBoolean("pr_new_test_only"));
				policyReqData.setOrderDate(DateHelper.utilDate(rs.getDate("pr_order_date")));
				policyReqData.setDateSent(DateHelper.utilDate(rs.getDate("pr_date_sent")));
				policyReqData.setCCASuggestInd(rs.getBoolean("pr_ccas_suggest_ind"));
				policyReqData.setReceiveDate(DateHelper.utilDate(rs.getDate("pr_received_date")));
				policyReqData.setAutoOrderInd(rs.getBoolean("pr_auto_order_ind"));
				policyReqData.setFldCommentInd(rs.getBoolean("pr_fld_comment_ind"));
				policyReqData.setComments(rs.getString("pr_comments")); 
				policyReqData.setCreatedBy(rs.getString("created_by"));
				policyReqData.setCreateDate(rs.getTimestamp("created_date"));
				policyReqData.setUpdatedBy(rs.getString("updated_by"));
				policyReqData.setUpdateDate(rs.getTimestamp("updated_date"));
				policyReqData.setImageRef(rs.getString("pr_img_reference_num"));
				list.add(policyReqData);	
				
			} 
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			  throw e; 
		}finally{
			  closeResources(conn, ps, rs);
		}

		LOGGER.info("retrieveRequirements end");
		return (list);	
	}// retrieveRequirements

	/**
	 * @param refNo
	 * @return
	 * @throws Exception
	 */
	public ArrayList retrieveRequirements2(String refNo) throws Exception {
		
		LOGGER.info("retrieveRequirements2 start");
		ArrayList list = new ArrayList();
		ArrayList ntoList = new ArrayList();
		ArrayList ordList = new ArrayList();
		ArrayList sccList = new ArrayList();
		ArrayList sirList = new ArrayList();
		ArrayList othList = new ArrayList();
		ArrayList outList = new ArrayList();
		AssessmentRequestDAO ARDAO = new AssessmentRequestDAO();
		AssessmentRequestData ARData = ARDAO.retrieveRequestDetail(refNo);
		
		
		String sql = "SELECT " +
				"PR_REQUIREMENT_ID, " +
				"UAR_REFERENCE_NUM, " +
				"PR_ADMIN_SEQUENCE_NUM, " +
				"PR_REQT_CODE,PR_LEVEL, " +
				"PR_ADMIN_CREATE_DATE, " +
				"PR_COMPLT_REQT_IND, " +
				"PR_STATUS_ID, " +
				"STAT_DESC, " +
				"PR_STATUS_DATE, " +
				"PR_ADMIN_UPDTD_BY, " +
				"PR_ADMIN_UPDTD_DATE, " +
				"PR_CLIENT_TYPE, " +
				"PR_DESIGNATION, " +
				"PR_TEST_DATE, " +
				"PR_TEST_RESULT_CODE, " +
				"PR_TEST_RESULT_DESC, " +
				"PR_RESOLVED_IND, " +
				"PR_FOLLOW_UP_NUM, " +
				"PR_FOLLOW_UP_DATE, " +
				"PR_VALIDITY_DATE, " +
				"PR_PAID_IND, " +
				"PR_NEW_TEST_ONLY, " +
				"PR_ORDER_DATE, " +
				"PR_DATE_SENT, " +
				"PR_CCAS_SUGGEST_IND, " +
				"PR_RECEIVED_DATE, " +
				"PR_AUTO_ORDER_IND, " +
				"PR_FLD_COMMENT_IND, " +
				"PR_COMMENTS, " +
				"PR_CLIENT_ID, " +
				"PR_IMG_REFERENCE_NUM, " + 
				"POLICY_REQUIREMENTS.CREATED_BY, " +
				"POLICY_REQUIREMENTS.CREATED_DATE, " +
				"POLICY_REQUIREMENTS.UPDATED_BY, " +
				"POLICY_REQUIREMENTS.UPDATED_DATE " +
			"FROM STATUS, POLICY_REQUIREMENTS " +
			"WHERE (PR_STATUS_ID = STAT_ID AND UAR_REFERENCE_NUM = ?) " +
			 "OR ((PR_RECEIVED_DATE IS NULL or (PR_RECEIVED_DATE IS NOT NULL AND " + 
			 "(PR_VALIDITY_DATE IS NULL or TRUNC(PR_VALIDITY_DATE) >= TRUNC(SYSDATE)))) AND STAT_TYPE = ? " + 
			 "AND PR_CLIENT_ID in(? , ?) AND UAR_REFERENCE_NUM != ? AND PR_LEVEL = ? and PR_STATUS_ID = STAT_ID) " +
			 "ORDER BY CREATED_DATE DESC , STAT_DESC ASC ";
		
		PreparedStatement ps = null;
		ResultSet rs = null;		 
		Connection conn = null;
		
		try {
			conn = getConnection();	
			ps = conn.prepareStatement(sql);
			ps.setString(1, refNo);
			ps.setString(2, IUMConstants.STAT_TYPE_NM);
			ps.setString(3, ARData.getInsured().getClientId());
			ps.setString(4, ARData.getOwner().getClientId());
			ps.setString(5, refNo);
			ps.setString(6, IUMConstants.LEVEL_CLIENT);
			
			rs = ps.executeQuery();
			
			
			while (rs.next()) {
				PolicyRequirementsData policyReqData = new PolicyRequirementsData();
				policyReqData.setSequenceNumber(rs.getLong("pr_admin_sequence_num"));
				policyReqData.setReferenceNumber(rs.getString("uar_reference_num"));
				policyReqData.setRequirementId(rs.getLong("pr_requirement_id"));
				policyReqData.setRequirementCode(rs.getString("pr_reqt_code"));
				policyReqData.setLevel(rs.getString("pr_level"));
				policyReqData.setABACUScreateDate(DateHelper.utilDate(rs.getDate("pr_admin_create_date")));
				policyReqData.setCompletedRequirementInd(rs.getBoolean("pr_complt_reqt_ind"));
				StatusData statusData = new StatusData();
				statusData.setStatusId(rs.getLong("pr_status_id"));
				statusData.setStatusDesc(rs.getString("stat_desc"));
				policyReqData.setStatus(statusData);				
				policyReqData.setStatusDate(DateHelper.utilDate(rs.getDate("pr_status_date")));  
				policyReqData.setABACUSUpdatedBy(rs.getString("pr_admin_updtd_by"));
				policyReqData.setABACUSUpdateDate(DateHelper.utilDate(rs.getDate("pr_admin_updtd_date")));
				policyReqData.setClientType(rs.getString("pr_client_type"));
				policyReqData.setClientId(rs.getString("pr_client_id"));
				policyReqData.setDesignation(rs.getString("pr_designation"));
				policyReqData.setTestDate(DateHelper.utilDate(rs.getDate("pr_test_date")));
				policyReqData.setTestResultCode(rs.getString("pr_test_result_code"));
				policyReqData.setTestResultDesc(rs.getString("pr_test_result_desc"));
				policyReqData.setResolveInd(rs.getBoolean("pr_resolved_ind"));
				policyReqData.setFollowUpNumber(rs.getLong("pr_follow_up_num"));
				policyReqData.setFollowUpDate(DateHelper.utilDate(rs.getDate("pr_follow_up_date")));
				policyReqData.setValidityDate(DateHelper.utilDate(rs.getDate("pr_validity_date")));
				policyReqData.setPaidInd(rs.getBoolean("pr_paid_ind"));
				policyReqData.setNewTestOnly(rs.getBoolean("pr_new_test_only"));
				policyReqData.setOrderDate(DateHelper.utilDate(rs.getDate("pr_order_date")));
				policyReqData.setDateSent(DateHelper.utilDate(rs.getDate("pr_date_sent")));
				policyReqData.setCCASuggestInd(rs.getBoolean("pr_ccas_suggest_ind"));
				policyReqData.setReceiveDate(DateHelper.utilDate(rs.getDate("pr_received_date")));
				policyReqData.setAutoOrderInd(rs.getBoolean("pr_auto_order_ind"));
				policyReqData.setFldCommentInd(rs.getBoolean("pr_fld_comment_ind"));
				policyReqData.setComments(rs.getString("pr_comments")); 
				policyReqData.setCreatedBy(rs.getString("created_by"));
				policyReqData.setCreateDate(rs.getTimestamp("created_date"));
				policyReqData.setUpdatedBy(rs.getString("updated_by"));
				policyReqData.setUpdateDate(rs.getTimestamp("updated_date"));
				policyReqData.setImageRef(rs.getString("pr_img_reference_num"));
				list.add(policyReqData);


				if(policyReqData!=null && policyReqData.getStatus()!=null && policyReqData.getStatus().getStatusDesc().equalsIgnoreCase("NTO")){
					ntoList.add(policyReqData);
				}else if(policyReqData!=null && policyReqData.getStatus()!=null && policyReqData.getStatus().getStatusDesc().equalsIgnoreCase("ORD")){
					ordList.add(policyReqData);
				}else if(policyReqData!=null && policyReqData.getStatus()!=null && policyReqData.getStatus().getStatusDesc().equalsIgnoreCase("SCC")){
					sccList.add(policyReqData);
				}else if(policyReqData!=null && policyReqData.getStatus()!=null && policyReqData.getStatus().getStatusDesc().equalsIgnoreCase("SIR")){
					sirList.add(policyReqData);
				}else{
					othList.add(policyReqData);
				}
				
				
			}
			
			Iterator ntoIt = ntoList.iterator();
			PolicyRequirementsData ntoData = null;
			while(ntoIt.hasNext()){
				ntoData = new PolicyRequirementsData();
				ntoData = (PolicyRequirementsData)ntoIt.next(); 
				outList.add(ntoData);
			}
			
			Iterator ordIt = ordList.iterator();
			PolicyRequirementsData ordData = null;
			while(ordIt.hasNext()){
				ordData = new PolicyRequirementsData();
				ordData = (PolicyRequirementsData)ordIt.next(); 
				outList.add(ordData);
			}
			
			Iterator sccIt = sccList.iterator();
			PolicyRequirementsData sccData = null;
			while(sccIt.hasNext()){
				sccData = new PolicyRequirementsData();
				sccData = (PolicyRequirementsData)sccIt.next(); 
				outList.add(sccData);
			}

			Iterator sirIt = sirList.iterator();
			PolicyRequirementsData sirData = null;
			while(sirIt.hasNext()){
				sirData = new PolicyRequirementsData();
				sirData = (PolicyRequirementsData)sirIt.next(); 
				outList.add(sirData);
			}
			
			Iterator othIt = othList.iterator();
			PolicyRequirementsData othData = null;
			while(othIt.hasNext()){
				othData = new PolicyRequirementsData();
				othData = (PolicyRequirementsData)othIt.next(); 
				outList.add(othData);
			}

		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			  throw e; 
		}finally{
			  closeResources(conn, ps, rs);
		}
		
		LOGGER.info("retrieveRequirements2 end");
		return (outList);	
	}// retrieveRequirements

	/**
	 * @param refNo
	 * @return
	 * @throws Exception
	 */
	/*
	  public ArrayList retrieveRequirements0(String refNo) throws Exception {
		
		LOGGER.info("retrieveRequirements0 start");
		ArrayList list = new ArrayList();
        
		//This SQL statement retrieves all requirements for the given reference number regardless of status.
		String sql1 = "SELECT " +
						"PR_REQUIREMENT_ID, " +
						"UAR_REFERENCE_NUM, " +
						"PR_ADMIN_SEQUENCE_NUM, " +
						"PR_REQT_CODE,PR_LEVEL, " +
						"PR_ADMIN_CREATE_DATE, " +
						"PR_COMPLT_REQT_IND, " +
						"PR_STATUS_ID, " +
						"STAT_DESC, " +
						"PR_STATUS_DATE, " +
						"PR_ADMIN_UPDTD_BY, " +
						"PR_ADMIN_UPDTD_DATE, " +
						"PR_CLIENT_TYPE, " +
						"PR_DESIGNATION, " +
						"PR_TEST_DATE, " +
						"PR_TEST_RESULT_CODE, " +
						"PR_TEST_RESULT_DESC, " +
						"PR_RESOLVED_IND, " +
						"PR_FOLLOW_UP_NUM, " +
						"PR_FOLLOW_UP_DATE, " +
						"PR_VALIDITY_DATE, " +
						"PR_PAID_IND, " +
						"PR_NEW_TEST_ONLY, " +
						"PR_ORDER_DATE, " +
						"PR_DATE_SENT, " +
						"PR_CCAS_SUGGEST_IND, " +
						"PR_RECEIVED_DATE, " +
						"PR_AUTO_ORDER_IND, " +
						"PR_FLD_COMMENT_IND, " +
						"PR_COMMENTS, " +
						"PR_CLIENT_ID, " +
						"PR_IMG_REFERENCE_NUM, " + 
						"POLICY_REQUIREMENTS.CREATED_BY, " +
						"POLICY_REQUIREMENTS.CREATED_DATE, " +
						"POLICY_REQUIREMENTS.UPDATED_BY, " +
						"POLICY_REQUIREMENTS.UPDATED_DATE " +
					"FROM STATUS, POLICY_REQUIREMENTS " +
					"WHERE PR_STATUS_ID = STAT_ID AND UAR_REFERENCE_NUM = ? ";
				
		//This SQL statement retrieves only valid client level requirements of different requests.  
		String sql2 = "SELECT " +
						"PR_REQUIREMENT_ID, " +
						"UAR_REFERENCE_NUM, " +
						"PR_ADMIN_SEQUENCE_NUM, " +
						"PR_REQT_CODE,PR_LEVEL, " +
						"PR_ADMIN_CREATE_DATE, " +
						"PR_COMPLT_REQT_IND, " +
						"PR_STATUS_ID, " +
						"STAT_DESC, " +
						"PR_STATUS_DATE, " +
						"PR_ADMIN_UPDTD_BY, " +
						"PR_ADMIN_UPDTD_DATE, " +
						"PR_CLIENT_TYPE, " +
						"PR_DESIGNATION, " +
						"PR_TEST_DATE, " +
						"PR_TEST_RESULT_CODE, " +
						"PR_TEST_RESULT_DESC, " +
						"PR_RESOLVED_IND, " +
						"PR_FOLLOW_UP_NUM, " +
						"PR_FOLLOW_UP_DATE, " +
						"PR_VALIDITY_DATE, " +
						"PR_PAID_IND, " +
						"PR_NEW_TEST_ONLY, " +
						"PR_ORDER_DATE, " +
						"PR_DATE_SENT, " +
						"PR_CCAS_SUGGEST_IND, " +
						"PR_RECEIVED_DATE, " +
						"PR_AUTO_ORDER_IND, " +
						"PR_FLD_COMMENT_IND, " +
						"PR_COMMENTS, " +
						"PR_CLIENT_ID, " +
						"PR_IMG_REFERENCE_NUM, " +
						"POLICY_REQUIREMENTS.CREATED_BY, " +
						"POLICY_REQUIREMENTS.CREATED_DATE, " +
						"POLICY_REQUIREMENTS.UPDATED_BY, " +
						"POLICY_REQUIREMENTS.UPDATED_DATE " +
					"FROM STATUS, POLICY_REQUIREMENTS " +
					"WHERE " +
						"(PR_RECEIVED_DATE IS NULL or (PR_RECEIVED_DATE IS NOT NULL AND " +
						"(PR_VALIDITY_DATE IS NULL or TRUNC(PR_VALIDITY_DATE) >= TRUNC(SYSDATE)))) AND " +
						" STAT_TYPE = ? AND " +
						" PR_CLIENT_ID in(?, ?) AND " +
						" UAR_REFERENCE_NUM != ? AND " +
						" PR_LEVEL = ? and PR_STATUS_ID = STAT_ID";
					
		PreparedStatement ps = null;
		CallableStatement stmt = null;
		ResultSet rs = null;		
		
		try {
			
			stmt = conn.prepareCall("BEGIN GetEmpRS(?); END;");
			stmt.execute();
		    
			while (rs.next()) {
				PolicyRequirementsData policyReqData = new PolicyRequirementsData();
				policyReqData.setSequenceNumber(rs.getLong("pr_admin_sequence_num"));
				policyReqData.setReferenceNumber(rs.getString("uar_reference_num"));
				policyReqData.setRequirementId(rs.getLong("pr_requirement_id"));
				policyReqData.setRequirementCode(rs.getString("pr_reqt_code"));
				policyReqData.setLevel(rs.getString("pr_level"));
				policyReqData.setABACUScreateDate(DateHelper.utilDate(rs.getDate("pr_admin_create_date")));
				policyReqData.setCompletedRequirementInd(rs.getBoolean("pr_complt_reqt_ind"));
				StatusData statusData = new StatusData();
				statusData.setStatusId(rs.getLong("pr_status_id"));
				statusData.setStatusDesc(rs.getString("stat_desc"));
				policyReqData.setStatus(statusData);				
				policyReqData.setStatusDate(DateHelper.utilDate(rs.getDate("pr_status_date")));  
				policyReqData.setABACUSUpdatedBy(rs.getString("pr_admin_updtd_by"));
				policyReqData.setABACUSUpdateDate(DateHelper.utilDate(rs.getDate("pr_admin_updtd_date")));
				policyReqData.setClientType(rs.getString("pr_client_type"));
				policyReqData.setClientId(rs.getString("pr_client_id"));
				policyReqData.setDesignation(rs.getString("pr_designation"));
				policyReqData.setTestDate(DateHelper.utilDate(rs.getDate("pr_test_date")));
				policyReqData.setTestResultCode(rs.getString("pr_test_result_code"));
				policyReqData.setTestResultDesc(rs.getString("pr_test_result_desc"));
				policyReqData.setResolveInd(rs.getBoolean("pr_resolved_ind"));
				policyReqData.setFollowUpNumber(rs.getLong("pr_follow_up_num"));
				policyReqData.setFollowUpDate(DateHelper.utilDate(rs.getDate("pr_follow_up_date")));
				policyReqData.setValidityDate(DateHelper.utilDate(rs.getDate("pr_validity_date")));
				policyReqData.setPaidInd(rs.getBoolean("pr_paid_ind"));
				policyReqData.setNewTestOnly(rs.getBoolean("pr_new_test_only"));
				policyReqData.setOrderDate(DateHelper.utilDate(rs.getDate("pr_order_date")));
				policyReqData.setDateSent(DateHelper.utilDate(rs.getDate("pr_date_sent")));
				policyReqData.setCCASuggestInd(rs.getBoolean("pr_ccas_suggest_ind"));
				policyReqData.setReceiveDate(DateHelper.utilDate(rs.getDate("pr_received_date")));
				policyReqData.setAutoOrderInd(rs.getBoolean("pr_auto_order_ind"));
				policyReqData.setFldCommentInd(rs.getBoolean("pr_fld_comment_ind"));
				policyReqData.setComments(rs.getString("pr_comments")); 
				policyReqData.setCreatedBy(rs.getString("created_by"));
				policyReqData.setCreateDate(rs.getTimestamp("created_date"));
				policyReqData.setUpdatedBy(rs.getString("updated_by"));
				policyReqData.setUpdateDate(rs.getTimestamp("updated_date"));
				policyReqData.setImageRef(rs.getString("pr_img_reference_num"));
				list.add(policyReqData);	
				
			} 
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			  throw e; 
		}finally{
			  this.closeResources(ps, rs);
		}
		LOGGER.info("retrieveRequirements0 end");
		return (list);	
	}
	*/
	
	public PolicyRequirementsData tmpRetrieveRequirement() throws SQLException {
		
		LOGGER.info("tmpRetrieveRequirement start");
		StringBuffer sql = new StringBuffer();
		
		sql.append("select p.*, r.reqt_desc, s.stat_desc ");
		sql.append("from requirements r, status s, policy_requirements p ");
		sql.append("where r.reqt_code = p.pr_reqt_code and ");
		sql.append(" s.stat_id = p.pr_status_id and s.stat_type = 'NM'  "); 
		sql.append("order by p.created_date desc ");			

		PreparedStatement ps = null;
		ResultSet rs = null;
		PolicyRequirementsData policyReqData = new PolicyRequirementsData();
		Connection conn = null;
		
		try {
			conn = getConnection();	
			ps = conn.prepareStatement(sql.toString());
			rs = ps.executeQuery();
       	
			if (rs.next()) {
				policyReqData.setReferenceNumber(rs.getString("uar_reference_num"));
				policyReqData.setRequirementId(rs.getLong("pr_requirement_id"));								
				policyReqData.setRequirementCode(rs.getString("pr_reqt_code"));
				policyReqData.setSequenceNumber(rs.getLong("pr_admin_sequence_num"));
				policyReqData.setReqDesc(rs.getString("reqt_desc"));
				policyReqData.setLevel(rs.getString("pr_level"));				
				policyReqData.setABACUScreateDate(DateHelper.utilDate(rs.getDate("pr_admin_create_date")));
				policyReqData.setCompletedRequirementInd(rs.getBoolean("pr_complt_reqt_ind"));
				StatusData statusData = new StatusData();
				statusData.setStatusId(rs.getLong("pr_status_id"));
				statusData.setStatusDesc(rs.getString("stat_desc"));
				policyReqData.setStatus(statusData);				
				policyReqData.setStatusDate(DateHelper.utilDate(rs.getDate("pr_status_date")));  
				policyReqData.setABACUSUpdatedBy(rs.getString("pr_admin_updtd_by"));
				policyReqData.setABACUSUpdateDate(DateHelper.utilDate(rs.getDate("pr_admin_updtd_date")));
				policyReqData.setClientType(rs.getString("pr_client_type"));
				policyReqData.setDesignation(rs.getString("pr_designation"));
				policyReqData.setTestDate(DateHelper.utilDate(rs.getDate("pr_test_date")));
				policyReqData.setTestResultCode(rs.getString("pr_test_result_code"));
				policyReqData.setTestResultDesc(rs.getString("pr_test_result_desc"));
				policyReqData.setResolveInd(rs.getBoolean("pr_resolved_ind"));
				policyReqData.setFollowUpNumber(rs.getLong("pr_follow_up_num"));
				policyReqData.setFollowUpDate(DateHelper.utilDate(rs.getDate("pr_follow_up_date")));
				policyReqData.setValidityDate(DateHelper.utilDate(rs.getDate("pr_validity_date")));
				policyReqData.setPaidInd(rs.getBoolean("pr_paid_ind"));
				policyReqData.setNewTestOnly(rs.getBoolean("pr_new_test_only"));
				policyReqData.setOrderDate(DateHelper.utilDate(rs.getDate("pr_order_date")));
				policyReqData.setDateSent(DateHelper.utilDate(rs.getDate("pr_date_sent")));
				policyReqData.setCCASuggestInd(rs.getBoolean("pr_ccas_suggest_ind"));
				policyReqData.setReceiveDate(DateHelper.utilDate(rs.getDate("pr_received_date")));
				policyReqData.setAutoOrderInd(rs.getBoolean("pr_auto_order_ind"));
				policyReqData.setFldCommentInd(rs.getBoolean("pr_fld_comment_ind"));
				policyReqData.setComments(rs.getString("pr_comments")); 
				policyReqData.setCreatedBy(rs.getString("created_by"));
				policyReqData.setCreateDate(rs.getTimestamp("created_date"));
				policyReqData.setUpdatedBy(rs.getString("updated_by"));
				policyReqData.setUpdateDate(rs.getTimestamp("updated_date"));
				policyReqData.setClientId(rs.getString("pr_client_id"));
				policyReqData.setImageRef(rs.getString("pr_img_reference_num"));
			} 
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			  throw e;
		
		}finally{
			closeResources(conn, ps, rs);
			
		}
		LOGGER.info("tmpRetrieveRequirement end");
			return (policyReqData);	
	
	}// retrieveRequirement
	
	/**
	 * @param refNo
	 * @param reqId
	 * @return
	 * @throws Exception
	 */
	public PolicyRequirementsData retrieveRequirement(final long reqId) throws SQLException {
		
		LOGGER.info("retrieveRequirement start 1");
		StringBuffer sql = new StringBuffer();
		
		sql.append("SELECT p.*, r.reqt_desc, s.stat_desc ")
			.append("FROM  requirements r, status s, policy_requirements p ")
			.append("WHERE r.reqt_code = p.pr_reqt_code ")
			.append(" AND s.stat_id = p.pr_status_id AND s.stat_type = 'NM'")
			.append(" AND p.pr_requirement_id = ? ")
			.append("ORDER BY p.created_date desc ");			

		PreparedStatement ps = null;
		ResultSet rs = null;
		PolicyRequirementsData policyReqData = new PolicyRequirementsData();
		Connection conn = null;
		
		try {
			conn = getConnection();	
			ps = conn.prepareStatement(sql.toString());
			ps.setLong(1, reqId);
			rs = ps.executeQuery();
       	
			if (rs.next()) {
				
				policyReqData.setReferenceNumber(rs.getString("uar_reference_num"));
				policyReqData.setRequirementId(rs.getLong("pr_requirement_id"));								
				policyReqData.setRequirementCode(rs.getString("pr_reqt_code"));
				policyReqData.setSequenceNumber(rs.getLong("pr_admin_sequence_num"));
				policyReqData.setReqDesc(rs.getString("reqt_desc"));
				policyReqData.setLevel(rs.getString("pr_level"));				
				policyReqData.setABACUScreateDate(DateHelper.utilDate(rs.getDate("pr_admin_create_date")));
				policyReqData.setCompletedRequirementInd(rs.getBoolean("pr_complt_reqt_ind"));
				StatusData statusData = new StatusData();
				statusData.setStatusId(rs.getLong("pr_status_id"));
				statusData.setStatusDesc(rs.getString("stat_desc"));
				policyReqData.setStatus(statusData);				
				policyReqData.setStatusDate(DateHelper.utilDate(rs.getDate("pr_status_date")));  
				policyReqData.setABACUSUpdatedBy(rs.getString("pr_admin_updtd_by"));
				policyReqData.setABACUSUpdateDate(DateHelper.utilDate(rs.getDate("pr_admin_updtd_date")));
				policyReqData.setClientType(rs.getString("pr_client_type"));
				policyReqData.setDesignation(rs.getString("pr_designation"));
				policyReqData.setTestDate(DateHelper.utilDate(rs.getDate("pr_test_date")));
				policyReqData.setTestResultCode(rs.getString("pr_test_result_code"));
				policyReqData.setTestResultDesc(rs.getString("pr_test_result_desc"));
				policyReqData.setResolveInd(rs.getBoolean("pr_resolved_ind"));
				policyReqData.setFollowUpNumber(rs.getLong("pr_follow_up_num"));
				policyReqData.setFollowUpDate(DateHelper.utilDate(rs.getDate("pr_follow_up_date")));
				policyReqData.setValidityDate(DateHelper.utilDate(rs.getDate("pr_validity_date")));
				policyReqData.setPaidInd(rs.getBoolean("pr_paid_ind"));
				policyReqData.setNewTestOnly(rs.getBoolean("pr_new_test_only"));
				policyReqData.setOrderDate(DateHelper.utilDate(rs.getDate("pr_order_date")));
				policyReqData.setDateSent(DateHelper.utilDate(rs.getDate("pr_date_sent")));
				policyReqData.setCCASuggestInd(rs.getBoolean("pr_ccas_suggest_ind"));
				policyReqData.setReceiveDate(DateHelper.utilDate(rs.getDate("pr_received_date")));
				policyReqData.setAutoOrderInd(rs.getBoolean("pr_auto_order_ind"));
				policyReqData.setFldCommentInd(rs.getBoolean("pr_fld_comment_ind"));
				policyReqData.setComments(rs.getString("pr_comments")); 
				policyReqData.setCreatedBy(rs.getString("created_by"));
				policyReqData.setCreateDate(rs.getTimestamp("created_date"));
				policyReqData.setUpdatedBy(rs.getString("updated_by"));
				policyReqData.setUpdateDate(rs.getTimestamp("updated_date"));
				policyReqData.setClientId(rs.getString("pr_client_id"));
				policyReqData.setImageRef(rs.getString("pr_img_reference_num"));
			} 
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			  throw e;
		} finally {
			closeResources(conn, ps, rs);
			
		}
		LOGGER.info("retrieveRequirement end 1");
		return (policyReqData);
	}// retrieveRequirement
	
//	 Method added by BJ Taduran for Ingenium Update of Requirements upon Unmatching (051807)
	public List retrieveRequirement(String referenceNum, String reqtCode, String imgRef) throws SQLException {
		
		LOGGER.info("retrieveRequirement start 2");
		List list = new ArrayList();
		StringBuffer sql = new StringBuffer();
		
		sql.append("select p.*, r.reqt_desc, s.stat_desc ");
		sql.append("from requirements r, status s, policy_requirements p ");
		sql.append("where r.reqt_code = p.pr_reqt_code and ");
		sql.append(" s.stat_id = p.pr_status_id and s.stat_type = 'NM' and "); 
		sql.append(" p.UAR_REFERENCE_NUM = ? and ");
		sql.append(" p.PR_REQT_CODE = ? and ");
		sql.append(" p.pr_img_reference_num = ? ");
		sql.append("order by p.created_date desc ");

		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection conn = null;
		
		try {
			conn = getConnection();	
			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, referenceNum);
			ps.setString(2, reqtCode);
			ps.setString(3, imgRef);
			rs = ps.executeQuery();
       	
			while (rs.next()) {
				PolicyRequirementsData policyReqData = new PolicyRequirementsData();
				policyReqData.setReferenceNumber(rs.getString("uar_reference_num"));
				policyReqData.setRequirementId(rs.getLong("pr_requirement_id"));								
				policyReqData.setRequirementCode(rs.getString("pr_reqt_code"));
				policyReqData.setSequenceNumber(rs.getLong("pr_admin_sequence_num"));
				policyReqData.setReqDesc(rs.getString("reqt_desc"));
				policyReqData.setLevel(rs.getString("pr_level"));				
				policyReqData.setABACUScreateDate(DateHelper.utilDate(rs.getDate("pr_admin_create_date")));
				policyReqData.setCompletedRequirementInd(rs.getBoolean("pr_complt_reqt_ind"));
				StatusData statusData = new StatusData();
				statusData.setStatusId(rs.getLong("pr_status_id"));
				statusData.setStatusDesc(rs.getString("stat_desc"));
				policyReqData.setStatus(statusData);				
				policyReqData.setStatusDate(DateHelper.utilDate(rs.getDate("pr_status_date")));  
				policyReqData.setABACUSUpdatedBy(rs.getString("pr_admin_updtd_by"));
				policyReqData.setABACUSUpdateDate(DateHelper.utilDate(rs.getDate("pr_admin_updtd_date")));
				policyReqData.setClientType(rs.getString("pr_client_type"));
				policyReqData.setDesignation(rs.getString("pr_designation"));
				policyReqData.setTestDate(DateHelper.utilDate(rs.getDate("pr_test_date")));
				policyReqData.setTestResultCode(rs.getString("pr_test_result_code"));
				policyReqData.setTestResultDesc(rs.getString("pr_test_result_desc"));
				policyReqData.setResolveInd(rs.getBoolean("pr_resolved_ind"));
				policyReqData.setFollowUpNumber(rs.getLong("pr_follow_up_num"));
				policyReqData.setFollowUpDate(DateHelper.utilDate(rs.getDate("pr_follow_up_date")));
				policyReqData.setValidityDate(DateHelper.utilDate(rs.getDate("pr_validity_date")));
				policyReqData.setPaidInd(rs.getBoolean("pr_paid_ind"));
				policyReqData.setNewTestOnly(rs.getBoolean("pr_new_test_only"));
				policyReqData.setOrderDate(DateHelper.utilDate(rs.getDate("pr_order_date")));
				policyReqData.setDateSent(DateHelper.utilDate(rs.getDate("pr_date_sent")));
				policyReqData.setCCASuggestInd(rs.getBoolean("pr_ccas_suggest_ind"));
				policyReqData.setReceiveDate(DateHelper.utilDate(rs.getDate("pr_received_date")));
				policyReqData.setAutoOrderInd(rs.getBoolean("pr_auto_order_ind"));
				policyReqData.setFldCommentInd(rs.getBoolean("pr_fld_comment_ind"));
				policyReqData.setComments(rs.getString("pr_comments")); 
				policyReqData.setCreatedBy(rs.getString("created_by"));
				policyReqData.setCreateDate(rs.getTimestamp("created_date"));
				policyReqData.setUpdatedBy(rs.getString("updated_by"));
				policyReqData.setUpdateDate(rs.getTimestamp("updated_date"));
				policyReqData.setClientId(rs.getString("pr_client_id"));
				policyReqData.setImageRef(rs.getString("pr_img_reference_num"));
				list.add(policyReqData);
			} 
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			  throw e;
		
		}finally{
			closeResources(conn, ps, rs);
			
		}
		LOGGER.info("retrieveRequirement end 2");
		return list;
	}
	
	public List retrieveRequirement(String referenceNum, String reqtCode) throws SQLException {
		
		LOGGER.info("retrieveRequirement start 3");
		List list = new ArrayList();
		StringBuffer sql = new StringBuffer();
		
		sql.append("select p.*, r.reqt_desc, s.stat_desc ");
		sql.append("from requirements r, status s, policy_requirements p ");
		sql.append("where r.reqt_code = p.pr_reqt_code and ");
		sql.append(" s.stat_id = p.pr_status_id and s.stat_type = 'NM' and "); 
		sql.append(" p.UAR_REFERENCE_NUM = ? and ");
		sql.append(" p.PR_REQT_CODE = ? and ");
		sql.append("order by p.created_date desc ");
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection conn = null;
		
		try {
			conn = getConnection();	
			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, referenceNum);
			ps.setString(2, reqtCode);
			rs = ps.executeQuery();
       	
			while (rs.next()) {
				PolicyRequirementsData policyReqData = new PolicyRequirementsData();
				policyReqData.setReferenceNumber(rs.getString("uar_reference_num"));
				policyReqData.setRequirementId(rs.getLong("pr_requirement_id"));								
				policyReqData.setRequirementCode(rs.getString("pr_reqt_code"));
				policyReqData.setSequenceNumber(rs.getLong("pr_admin_sequence_num"));
				policyReqData.setReqDesc(rs.getString("reqt_desc"));
				policyReqData.setLevel(rs.getString("pr_level"));				
				policyReqData.setABACUScreateDate(DateHelper.utilDate(rs.getDate("pr_admin_create_date")));
				policyReqData.setCompletedRequirementInd(rs.getBoolean("pr_complt_reqt_ind"));
				StatusData statusData = new StatusData();
				statusData.setStatusId(rs.getLong("pr_status_id"));
				statusData.setStatusDesc(rs.getString("stat_desc"));
				policyReqData.setStatus(statusData);				
				policyReqData.setStatusDate(DateHelper.utilDate(rs.getDate("pr_status_date")));  
				policyReqData.setABACUSUpdatedBy(rs.getString("pr_admin_updtd_by"));
				policyReqData.setABACUSUpdateDate(DateHelper.utilDate(rs.getDate("pr_admin_updtd_date")));
				policyReqData.setClientType(rs.getString("pr_client_type"));
				policyReqData.setDesignation(rs.getString("pr_designation"));
				policyReqData.setTestDate(DateHelper.utilDate(rs.getDate("pr_test_date")));
				policyReqData.setTestResultCode(rs.getString("pr_test_result_code"));
				policyReqData.setTestResultDesc(rs.getString("pr_test_result_desc"));
				policyReqData.setResolveInd(rs.getBoolean("pr_resolved_ind"));
				policyReqData.setFollowUpNumber(rs.getLong("pr_follow_up_num"));
				policyReqData.setFollowUpDate(DateHelper.utilDate(rs.getDate("pr_follow_up_date")));
				policyReqData.setValidityDate(DateHelper.utilDate(rs.getDate("pr_validity_date")));
				policyReqData.setPaidInd(rs.getBoolean("pr_paid_ind"));
				policyReqData.setNewTestOnly(rs.getBoolean("pr_new_test_only"));
				policyReqData.setOrderDate(DateHelper.utilDate(rs.getDate("pr_order_date")));
				policyReqData.setDateSent(DateHelper.utilDate(rs.getDate("pr_date_sent")));
				policyReqData.setCCASuggestInd(rs.getBoolean("pr_ccas_suggest_ind"));
				policyReqData.setReceiveDate(DateHelper.utilDate(rs.getDate("pr_received_date")));
				policyReqData.setAutoOrderInd(rs.getBoolean("pr_auto_order_ind"));
				policyReqData.setFldCommentInd(rs.getBoolean("pr_fld_comment_ind"));
				policyReqData.setComments(rs.getString("pr_comments")); 
				policyReqData.setCreatedBy(rs.getString("created_by"));
				policyReqData.setCreateDate(rs.getTimestamp("created_date"));
				policyReqData.setUpdatedBy(rs.getString("updated_by"));
				policyReqData.setUpdateDate(rs.getTimestamp("updated_date"));
				policyReqData.setClientId(rs.getString("pr_client_id"));
				policyReqData.setImageRef(rs.getString("pr_img_reference_num"));
				list.add(policyReqData);
			} 
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			  throw e;
		
		}finally{
			closeResources(conn, ps, rs);
			
		}
		LOGGER.info("retrieveRequirement end 3");
			return list;
	}
	
	
	/**
	 * 
	 * @return
	 */
	public ArrayList retrieveRequirementsOrdered(RequirementsOrderedFilterData reqtFilter) throws SQLException {
		
		LOGGER.info("retrieveRequirementsOrdered start");
		String sqlBranchFilter = " and AR.BRANCH_ID = ";
		String sqlReqtFilter = " and PR.PR_REQT_CODE = ";
		String sqlUserFilter = " and UR.USER_ID = ";

        StringBuffer sqlMain = new StringBuffer();
        sqlMain.append(" from ");
        sqlMain.append("  POLICY_REQUIREMENTS PR ");
	    sqlMain.append(", REQUIREMENTS        RQ ");
		sqlMain.append(", SUNLIFE_OFFICES     SO ");
		sqlMain.append(", ASSESSMENT_REQUESTS AR ");
		sqlMain.append(", USERS               UR ");
		sqlMain.append(" where ");
		sqlMain.append("     AR.REFERENCE_NUM = PR.UAR_REFERENCE_NUM ");
		sqlMain.append(" and RQ.REQT_CODE = PR.PR_REQT_CODE ");
		sqlMain.append(" and SO.SLO_OFFICE_CODE = AR.BRANCH_ID ");
		sqlMain.append(" and AR.UNDERWRITER = UR.USER_ID ");
		sqlMain.append(" and PR.PR_STATUS_ID IN (?, ?, ?, ?) ");
		sqlMain.append(" and PR.PR_ORDER_DATE between to_date (?, 'dd-MON-yyyy hh24:mi:ss')");
		sqlMain.append("                          and to_date (?, 'dd-MON-yyyy hh24:mi:ss')");
		
		StringBuffer sqlUserSelect = new StringBuffer();
		sqlUserSelect.append(" select ");
		sqlUserSelect.append("  UR.USR_LAST_NAME as LAST_NAME ");
		sqlUserSelect.append(", UR.USR_FIRST_NAME as FIRST_NAME ");
		sqlUserSelect.append(", UR.USR_MIDDLE_NAME as MIDDLE_NAME ");
		sqlUserSelect.append(", PR.PR_REQT_CODE as REQUIREMENT_CODE ");
		sqlUserSelect.append(", RQ.REQT_DESC as REQUIREMENT_NAME  ");
		sqlUserSelect.append(", count (PR.PR_REQUIREMENT_ID) as REQUIREMENT_COUNT ");

		StringBuffer sqlUserGroup = new StringBuffer();
		sqlUserGroup.append(" group by grouping sets (");
		sqlUserGroup.append(" (UR.USR_LAST_NAME, UR.USR_FIRST_NAME, UR.USR_MIDDLE_NAME), " );
		sqlUserGroup.append(" (UR.USR_LAST_NAME, UR.USR_FIRST_NAME, UR.USR_MIDDLE_NAME, PR.PR_REQT_CODE, RQ.REQT_DESC) ");
		sqlUserGroup.append(" ) ");
		sqlUserGroup.append(" order by ");
        sqlUserGroup.append("  UR.USR_LAST_NAME ");
		sqlUserGroup.append(", UR.USR_FIRST_NAME ");
		sqlUserGroup.append(", UR.USR_MIDDLE_NAME "); 
		sqlUserGroup.append(", PR.PR_REQT_CODE ");
		sqlUserGroup.append(", RQ.REQT_DESC ");

		StringBuffer sqlBranchSelect = new StringBuffer();
		sqlBranchSelect.append(" select ");
		sqlBranchSelect.append("  SO.SLO_OFFICE_NAME as BRANCH_NAME ");
		sqlBranchSelect.append(", PR.PR_REQT_CODE as REQUIREMENT_CODE  ");
		sqlBranchSelect.append(", RQ.REQT_DESC as REQUIREMENT_NAME  ");
		sqlBranchSelect.append(", count (PR.PR_REQUIREMENT_ID) as REQUIREMENT_COUNT ");
	
		StringBuffer sqlBranchGroup = new StringBuffer();
		sqlBranchGroup.append(" group by grouping sets (");
		sqlBranchGroup.append(" (SO.SLO_OFFICE_NAME), ");
		sqlBranchGroup.append(" (SO.SLO_OFFICE_NAME, PR.PR_REQT_CODE, RQ.REQT_DESC) ");
		sqlBranchGroup.append("  )");
		sqlBranchGroup.append(" order by ");
 		sqlBranchGroup.append("  SO.SLO_OFFICE_NAME");
		sqlBranchGroup.append(", PR.PR_REQT_CODE ");
		sqlBranchGroup.append(", RQ.REQT_DESC");
		
		StringBuffer sqlReqtSelect = new StringBuffer();
		sqlReqtSelect.append(" select ");
		sqlReqtSelect.append("  PR.PR_REQT_CODE as REQUIREMENT_CODE ");
		sqlReqtSelect.append(", RQ.REQT_DESC as REQUIREMENT_NAME  ");
		sqlReqtSelect.append(", SO.SLO_OFFICE_NAME as BRANCH_NAME ");		
		sqlReqtSelect.append(", count (PR.PR_REQUIREMENT_ID) as REQUIREMENT_COUNT ");

		StringBuffer sqlReqtGroup = new StringBuffer();
		sqlReqtGroup.append(" group by grouping sets (");
		sqlReqtGroup.append(" (PR.PR_REQT_CODE, RQ.REQT_DESC),  ");
		sqlReqtGroup.append(" (PR.PR_REQT_CODE, RQ.REQT_DESC, SO.SLO_OFFICE_NAME) ");
		sqlReqtGroup.append(" ) ");
		sqlReqtGroup.append(" order by ");
		sqlReqtGroup.append("  PR.PR_REQT_CODE ");
		sqlReqtGroup.append(", RQ.REQT_DESC ");
		sqlReqtGroup.append(", SO.SLO_OFFICE_NAME ");

		// check the optional filters, add if anything was specified
		String branch = reqtFilter.getBranch().getOfficeId();
		if (branch != null && branch != "") {
			// add the branch filter
			sqlBranchFilter += "'" + reqtFilter.getBranch().getOfficeId() + "'";
			sqlMain.append(sqlBranchFilter); 
		}
		
		String requirement = reqtFilter.getRequirement().getReqtCode();
		if (requirement != null && requirement != "") {
			// add the requirement filter
			sqlReqtFilter += "'" + reqtFilter.getRequirement().getReqtCode() + "'";
			sqlMain.append(sqlReqtFilter);
		}
		
		String user = reqtFilter.getUser().getUserId();
		if (user != null && user != "") {
			// add the underwriter filter
			sqlUserFilter += "'" + reqtFilter.getUser().getUserId() + "'";
			sqlMain.append(sqlUserFilter);			
		}		
		
		StringBuffer sql = new StringBuffer();
		// form the query based on available optional filters
		switch (reqtFilter.getReportType()) {
			case RequirementsOrderedFilterData.TYPE_BRANCH:
			    sql.append(sqlBranchSelect);
				sql.append(sqlMain);
			    sql.append(sqlBranchGroup);
			    break;
			case RequirementsOrderedFilterData.TYPE_REQUIREMENT:
				sql.append(sqlReqtSelect);
				sql.append(sqlMain);
				sql.append(sqlReqtGroup);
			    break;
			case RequirementsOrderedFilterData.TYPE_USER:
				sql.append(sqlUserSelect);
				sql.append(sqlMain);
				sql.append(sqlUserGroup);
		       	break;
		}
		
		String startDate = DateHelper.addTimePart(reqtFilter.getStartDate(),DateHelper.ADD_END_DAY_TIME);
		String endDate = DateHelper.addTimePart(reqtFilter.getEndDate(), DateHelper.ADD_END_DAY_TIME);

		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList list = new ArrayList();
		Connection conn = null;
		
		try {
			
		conn = getConnection();	
		ps = conn.prepareStatement(sql.toString());
		ps.setLong(1, IUMConstants.STATUS_ORDERED);
		ps.setLong(2, IUMConstants.STATUS_RECEIVED_IN_SITE);
		ps.setLong(3, IUMConstants.STATUS_REVIEWED_AND_ACCEPTED);
		ps.setLong(4, IUMConstants.STATUS_REVIEWED_AND_REJECTED);
		ps.setString(5, reqtFilter.getStartDate());
		ps.setString(6, reqtFilter.getEndDate());
		
		rs = ps.executeQuery();
		
		while (rs.next()) {	
			RequirementsOrderedData reqtOrderData = new RequirementsOrderedData();
			
			if (reqtFilter.getReportType() == RequirementsOrderedFilterData.TYPE_USER) {
				// contains different fields
				UserProfileData userData = new UserProfileData();
				userData.setFirstName(rs.getString("FIRST_NAME"));
				userData.setMiddleName(rs.getString("MIDDLE_NAME"));
				userData.setLastName(rs.getString("LAST_NAME"));
				reqtOrderData.setUser(userData);
				
			}
			else {
				SunLifeOfficeData sloData = new SunLifeOfficeData();
				sloData.setOfficeName(rs.getString("BRANCH_NAME"));
				reqtOrderData.setDepartment(sloData);
			}
							
			RequirementData reqtData = new RequirementData();
			reqtData.setReqtCode(rs.getString("REQUIREMENT_CODE"));
			reqtData.setReqtDesc(rs.getString("REQUIREMENT_NAME"));
			reqtOrderData.setRequirement(reqtData);
			
			reqtOrderData.setRequirementsOrdered(rs.getLong("REQUIREMENT_COUNT"));
			
			list.add(reqtOrderData);	
			
		}
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			  throw e;
		
		}finally{
			closeResources(conn, ps, rs);
			
		}
		LOGGER.info("retrieveRequirementsOrdered end");
		return (list);
	}
	
	
	/**
	 * @param referenceNumber
	 * @param newStatus
	 * @param updatedBy
	 * @throws SQLException 
	 */
	public void changeStatus (String referenceNumber, long newStatus, String updatedBy) throws SQLException{
		
		LOGGER.info("changeStatus start 1");
		String sql = "UPDATE POLICY_REQUIREMENTS SET PR_STATUS_ID = ?, UPDATED_BY = ?, UPDATED_DATE = ? WHERE UAR_REFERENCE_NUM = ?";
	
		PreparedStatement stmt = null;
		Connection conn = null;
		
		try {
			conn = getConnection();	
			conn.setAutoCommit(false);
			stmt = conn.prepareStatement(sql);
			stmt.setLong(1, newStatus);
			stmt.setString(2, updatedBy);
			stmt.setTimestamp(3, DateHelper.sqlTimestamp(new java.util.Date()));
			stmt.setString(4, referenceNumber);
			stmt.executeUpdate();
			conn.commit();
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			if(conn != null){
				conn.rollback();
			}
		}finally{
			closeResources(conn, stmt, null);
		} 
		LOGGER.info("changeStatus end 1");
	}

	/**
	 * Update status of requirement
	 * @param reqId main id in policy_requirements
	 * @param newStatus new status of requirement
	 * @param updatedBy user updater
	 * 
	 * ** Made changes according to v1.2 - Sept 25, 2014
	 * @throws SQLException 
	 */
	public void changeStatus (
			final long reqtId,
			final long newStatus,
			final String updatedBy) throws SQLException {
		
		LOGGER.info("changeStatus start 2");
		PreparedStatement ps = null;
		Connection conn = null;
		String sql = "UPDATE policy_requirements SET pr_status_id = ?, updated_by = ?, updated_date = ? WHERE pr_requirement_id = ?";

		try {
			conn = getConnection();	
			conn.setAutoCommit(false);
			StringBuffer params = new StringBuffer();
			params.append("[Line:1943] SQL: ").append(sql).append(" : ")
				.append("New Status: ").append(newStatus).append(" : ")
				.append("Updated By: ").append(updatedBy).append(" : ")
				.append("Requirement Id: ").append(reqtId);
			ps = conn.prepareStatement(sql);
			ps.setLong(1, newStatus);
			ps.setString(2, updatedBy);
			ps.setTimestamp(3, DateHelper.sqlTimestamp(new java.util.Date()));
			ps.setLong(4, reqtId);
			ps.executeUpdate();
			conn.commit();
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			if(conn != null){
				conn.rollback();
			}
		}finally{
			closeResources(conn, ps, null);
		}
		LOGGER.info("changeStatus end 2");
		
	}

	
	/**
	 * @param requirementID
	 * @param orderDate
	 * @throws SQLException
	 */
	public void updateOrderDate (long requirementID, Date orderDate) throws SQLException{
		
		LOGGER.info("updateOrderDate start");
		String sql = "UPDATE POLICY_REQUIREMENTS SET PR_ORDER_DATE = ? WHERE PR_REQUIREMENT_ID = ?";
		
		PreparedStatement stmt = null;
		Connection conn = null;
		
		try {
			conn = getConnection();	
			conn.setAutoCommit(false);
			stmt = conn.prepareStatement(sql);
			stmt.setDate(1, DateHelper.sqlDate(orderDate));
			stmt.setLong(2, requirementID);
			stmt.executeUpdate();
			conn.commit();
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			if(conn != null){
				conn.rollback();
			}
			 throw e; 
		}finally{
			  closeResources(conn, stmt, null);
		}
		LOGGER.info("updateOrderDate end");
	}

	
	/**
	 * @param requirementID
	 * @param followUpDate
	 * @throws SQLException
	 */
	public void updateFollowUpDate (long requirementID, Date followUpDate) throws SQLException{
		
		LOGGER.info("updateFollowUpDate start");
		String sql = "UPDATE POLICY_REQUIREMENTS SET PR_FOLLOW_UP_DATE = ? WHERE PR_REQUIREMENT_ID = ?";
		
		PreparedStatement stmt = null;
		Connection conn = null;
		
		try {
			conn = getConnection();	
			conn.setAutoCommit(false);
			stmt = conn.prepareStatement(sql);
			stmt.setDate(1, DateHelper.sqlDate(followUpDate));
			stmt.setLong(2, requirementID);
			stmt.executeUpdate();
			conn.commit();
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			if(conn != null){
				conn.rollback();
			}
			throw e; 
		}finally{
			  closeResources(conn, stmt, null);
		}
		LOGGER.info("updateFollowUpDate end");
	}

	/**
	 * @param requirementID
	 * @param followUpDate
	 * @throws SQLException
	 */
	public void updateComments (long requirementID, String comments) throws SQLException{
		
		LOGGER.info("updateComments start");
		String sql = "UPDATE POLICY_REQUIREMENTS SET PR_COMMENTS = ? WHERE PR_REQUIREMENT_ID = ?";
		
		PreparedStatement stmt = null;
		Connection conn = null;
		
		try {
			conn = getConnection();	
			conn.setAutoCommit(false);
			stmt = conn.prepareStatement(sql);
			stmt.setString(1, comments);
			stmt.setLong(2, requirementID);
			stmt.executeUpdate();
			conn.commit();
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			if(conn != null){
				conn.rollback();
			}
			throw e; 
		}finally{
			  this.closeResources(conn, stmt, null);
		}
		LOGGER.info("updateComments end");
	}
	
	
	/**
	 * @param requirementID
	 * @param dateSent
	 * @throws SQLException
	 */
	public void updateDateSent (long requirementID, Date dateSent) throws SQLException{
		
		LOGGER.info("updateDateSent start");
		String sql = "UPDATE POLICY_REQUIREMENTS SET PR_DATE_SENT = ? WHERE PR_REQUIREMENT_ID = ?";
		
		PreparedStatement stmt = null;
		Connection conn = null;
		
		try {
			conn = getConnection();	
			conn.setAutoCommit(false);
			stmt = conn.prepareStatement(sql);
			stmt.setDate(1, DateHelper.sqlDate(dateSent));
			stmt.setLong(2, requirementID);
			stmt.executeUpdate();
			conn.commit();
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			if(conn != null){
				conn.rollback();
			}
			throw e; 
		}finally{
			  closeResources(conn, stmt, null);
		}
		LOGGER.info("updateDateSent end");
	}

	
	/**
	 * @param requirementID
	 * @param receiveDate
	 * @throws SQLException
	 */
	public void updateReceiveDate (long requirementID, Date receiveDate) throws SQLException{
		
		LOGGER.info("updateReceiveDate start");
		String sql = "UPDATE POLICY_REQUIREMENTS SET PR_RECEIVED_DATE = ? WHERE PR_REQUIREMENT_ID = ?";
		
		PreparedStatement stmt = null;
		Connection conn = null;
		
		try {
			conn = getConnection();	
			conn.setAutoCommit(false);
			stmt = conn.prepareStatement(sql);
			stmt.setDate(1, DateHelper.sqlDate(receiveDate));
			stmt.setLong(2, requirementID);
			stmt.executeUpdate();
			conn.commit();
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			if(conn != null){
				conn.rollback();
			}
			throw e; 
		}finally{
			  closeResources(conn, stmt, null);
		}
		LOGGER.info("updateReceiveDate end");
	}


	public void updateValidityDate (long requirementID, Date validityDate) throws SQLException{
		
		LOGGER.info("updateValidityDate start");
		String sql = "UPDATE POLICY_REQUIREMENTS SET PR_VALIDITY_DATE = ? WHERE PR_REQUIREMENT_ID = ?";

		PreparedStatement ps = null;
		Connection conn = null;
		
		try {
			conn = getConnection();	
			conn.setAutoCommit(false);
			PreparedStatement stmt = conn.prepareStatement(sql);
			stmt.setDate(1, DateHelper.sqlDate(validityDate));
			stmt.setLong(2, requirementID);
			stmt.executeUpdate();
			conn.commit();
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			if(conn != null){
				conn.rollback();
			}
			throw e; 
		}finally{
			  closeResources(conn, ps, null);
		}
		LOGGER.info("updateValidityDate end");
	}
	
	/**
	 * Update follow up num and date of requirement
	 * @param reqId main id in policy_requirements
	 * 
	 * ** Made changes according to v1.2 - Sept 25, 2014
	 * @throws SQLException 
	 */
	public void resetFollowUp (final long reqtId) throws SQLException{
		
		LOGGER.info("resetFollowUp start");
		
		String sql = "UPDATE policy_requirements SET pr_follow_up_num = NULL, pr_follow_up_date = NULL  WHERE pr_requirement_id = ?";
		PreparedStatement ps = null;
		Connection conn = null;
		
		try {
			conn = getConnection();	
			conn.setAutoCommit(false);
			ps = conn.prepareStatement(sql);
			ps.setLong(1, reqtId);
			ps.executeUpdate();
			conn.commit();
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
		}finally{
			closeResources(conn, ps, null);
		}
		
		LOGGER.info("resetFollowUp end");
	}

	public boolean isRequirementExisting(String referenceNumber, long reqId) throws SQLException{
		
		LOGGER.info("isRequirementExisting start 1");
		boolean result=false;
		String sql = "SELECT COUNT(*) FROM POLICY_REQUIREMENTS WHERE UAR_REFERENCE_NUM = ? AND PR_REQUIREMENT_ID = ?";
		
		PreparedStatement ps = null;
		ResultSet rs = null;		
		Connection conn = null;
		
		try {
			conn = getConnection();	
			PreparedStatement stmt = conn.prepareStatement(sql);
			stmt.setString(1, referenceNumber);
			stmt.setLong(2, reqId);
			rs = stmt.executeQuery();
			if (rs.next()){
				if (rs.getInt(1) > 0){
					result = true;
				}
			}
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			  throw e; 
		}finally{
			  closeResources(conn, ps, rs);
		}
		LOGGER.info("isRequirementExisting end 1");
		return result;
	}
	
	public boolean isRequirementExisting(long reqtId) throws SQLException{
		
		LOGGER.info("isRequirementExisting start 2");
		boolean result=false;
		String sql = null;
				
		sql = "SELECT COUNT(*) FROM POLICY_REQUIREMENTS WHERE PR_REQUIREMENT_ID = ?";
		
		PreparedStatement stmt = null;
		ResultSet rs = null;	
		Connection conn = null;
		
		try {
			conn = getConnection();
			stmt = conn.prepareStatement(sql);
			stmt.setLong(1, reqtId);
			
			rs = stmt.executeQuery();
			
			if (rs.next()){
				if (rs.getInt(1) > 0){
					result = true;
				}
			}
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			  
		}finally{
			closeResources(conn, stmt, rs);
		}	
		LOGGER.info("isRequirementExisting end 2");
		return result;
		
	}
	// added by BJ Taduran
	public boolean isRequirementExisting(String policyId, String imgRefId, String refCode) throws SQLException{
		
		LOGGER.info("isRequirementExisting start 3");
		boolean result=false;
		String sql = null;
				
		sql = "SELECT COUNT(*) FROM POLICY_REQUIREMENTS WHERE UAR_REFERENCE_NUM = ? " +
			"AND PR_IMG_REFERENCE_NUM = ? AND PR_REQT_CODE = ?";
		
		PreparedStatement stmt = null;
		ResultSet rs = null;	
		Connection conn = null;
		
		try {
			conn = getConnection();
			stmt = conn.prepareStatement(sql);
			stmt.setString(1, policyId);
			stmt.setString(2, imgRefId);
			stmt.setString(3, refCode);
			rs = stmt.executeQuery();
			
			if (rs.next()){
				if (rs.getInt(1) > 0){
					result = true;
				}
			}
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			  
		}finally{
			closeResources(conn, stmt, rs);
		}
		LOGGER.info("isRequirementExisting end 3");
		return result;
		
	}
	
	public boolean isRequirementMatched(long reqtId) throws SQLException{
		
		LOGGER.info("isRequirementMatched start 1");
		boolean result=false;
		StringBuffer sql = new StringBuffer();
		
		sql.append("SELECT PR_STATUS_ID FROM POLICY_REQUIREMENTS WHERE PR_REQUIREMENT_ID = ? ");
		sql.append("AND PR_STATUS_ID = ").append(IUMConstants.STATUS_REVIEWED_AND_ACCEPTED);
		
		PreparedStatement stmt = null;
		ResultSet rs = null;	
		Connection conn = null;
		
		try {
			conn = getConnection();
			stmt = conn.prepareStatement(sql.toString());
			stmt.setLong(1, reqtId);
			
			rs = stmt.executeQuery();
			
			if (rs.next()){
				result = true;
			}
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
		}finally{
			closeResources(conn, stmt, rs);
		}
		LOGGER.info("isRequirementMatched end 1");
		return result;
		
	}
	// added by BJ Taduran
	public boolean isRequirementMatched(String policyId, String imgRefId, String refCode) throws SQLException{
		
		LOGGER.info("isRequirementMatched start 2");
		boolean result=false;
		StringBuffer sql = new StringBuffer();
		
		sql.append("SELECT PR_STATUS_ID FROM POLICY_REQUIREMENTS WHERE UAR_REFERENCE_NUM = ? " +
			"AND PR_IMG_REFERENCE_NUM = ? AND PR_REQT_CODE = ? ");
		sql.append("AND PR_STATUS_ID = ").append(IUMConstants.STATUS_REVIEWED_AND_ACCEPTED);
		
		
		PreparedStatement stmt = null;
		ResultSet rs = null;	
		Connection conn = null;
		
		try {
			conn = getConnection();
			stmt = conn.prepareStatement(sql.toString());
			stmt.setString(1, policyId);
			stmt.setString(2, imgRefId);
			stmt.setString(3, refCode);
			rs = stmt.executeQuery();
			if (rs.next()){
				result = true;
			}
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
		}finally{
			closeResources(conn, stmt, rs);
		}
		LOGGER.info("isRequirementMatched end 2");
		return result;
		
	}
	
	public boolean isRequirementMatched(PolicyRequirementsData prd) throws SQLException{
		
		LOGGER.info("isRequirementMatched start 3");
		boolean result=false;
		StringBuffer sql = new StringBuffer();
		
		sql.append("SELECT PR_STATUS_ID FROM POLICY_REQUIREMENTS WHERE UAR_REFERENCE_NUM = ? ");
		sql.append( "AND PR_REQT_CODE =? ");
		sql.append( "AND PR_LEVEL =? ");
		if(prd.getLevel().equalsIgnoreCase(IUMConstants.LEVEL_CLIENT)){
			sql.append("AND PR_CLIENT_ID=? ");
		}
		
		sql.append("AND PR_STATUS_ID = ").append(IUMConstants.STATUS_REVIEWED_AND_ACCEPTED);
		
		PreparedStatement stmt = null;
		ResultSet rs = null;	
		Connection conn = null;
		
		try {
			conn = getConnection();
			stmt = conn.prepareStatement(sql.toString());
			stmt.setString(1, prd.getReferenceNumber());
			stmt.setString(2,prd.getRequirementCode());
			stmt.setString(3,prd.getLevel());
			
			if(prd.getLevel().equalsIgnoreCase(IUMConstants.LEVEL_CLIENT)){
				stmt.setString(4,prd.getClientId());
			}
			
			rs = stmt.executeQuery();
			
			if (rs.next()){
				result = true;
			}
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
		}finally{
			closeResources(conn, stmt, rs);
		}
		
		LOGGER.info("isRequirementMatched end 3");
		return result;
		
	}
	
	public boolean isRequirementExisting(PolicyRequirementsData prd) throws SQLException{
		
		LOGGER.info("isRequirementExisting start");
		boolean result=false;
		String sql = "";
		if(prd.getLevel().equalsIgnoreCase(IUMConstants.LEVEL_CLIENT)){
		
			sql = "SELECT COUNT(*) " +
					"FROM POLICY_REQUIREMENTS " +
				   "WHERE PR_LEVEL = ? " +
				     "AND PR_REQT_CODE = ? " +
				     "AND PR_ADMIN_SEQUENCE_NUM = ? " +
				     "AND PR_CLIENT_ID = ?";
			
		} else {
			sql = "SELECT COUNT(*) FROM POLICY_REQUIREMENTS WHERE UAR_REFERENCE_NUM = ? AND PR_LEVEL = ?" +
			"  AND PR_REQT_CODE = ?" +
			"  AND PR_ADMIN_SEQUENCE_NUM = ?";
		}
		PreparedStatement stmt = null;
		ResultSet rs = null;		
		Connection conn = null;
		
		try {
			conn = getConnection();	
			stmt = conn.prepareStatement(sql);
			if (prd.getLevel().equalsIgnoreCase(IUMConstants.LEVEL_CLIENT)) {
				stmt.setString(1, prd.getLevel());
				stmt.setString(2, prd.getRequirementCode());
				stmt.setLong(3, prd.getSequenceNumber());
				stmt.setString(4, prd.getClientId());
			
			} else {
				stmt.setString(1, prd.getReferenceNumber());
				stmt.setString(2, prd.getLevel());
				stmt.setString(3, prd.getRequirementCode());
				stmt.setLong(4, prd.getSequenceNumber());
				
			}
						
			rs = stmt.executeQuery();
			if (rs.next()){
				int i = rs.getInt(1);
				if (i > 0){
					result = true;
				}
			}
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			  throw e; 
		}finally{
			closeResources(conn, stmt, rs);
		}
		LOGGER.info("isRequirementExisting end");
		return result;
	}
	

	public void updateRequirementRetainComment(PolicyRequirementsData requirement) throws SQLException {
		
		LOGGER.info("updateRequirementRetainComment start");
		String parameter = "";
		if(requirement.getLevel().equalsIgnoreCase(IUMConstants.LEVEL_CLIENT)){
			parameter = "  AND PR_CLIENT_ID = ?";
		}

		String receivedDate = " ";
		if(requirement.getStatus().getStatusId()==IUMConstants.STATUS_RECEIVED_IN_SITE){
			receivedDate =  "    , PR_RECEIVED_DATE = ?";
		}
		String sql = "UPDATE POLICY_REQUIREMENTS" +
					"    SET UAR_REFERENCE_NUM = ?" +
					"      , PR_ADMIN_SEQUENCE_NUM = ?" +
					"      , PR_REQT_CODE = ?" +
					"      , PR_LEVEL = ?" +
					"      , PR_ADMIN_CREATE_DATE = ?" +
					"      , PR_COMPLT_REQT_IND = ?" +
					"      , PR_STATUS_ID = ?" +
					"      , PR_STATUS_DATE = ?" +
					"      , PR_ADMIN_UPDTD_BY = ?" +
					"      , PR_ADMIN_UPDTD_DATE = ?" +
					"      , PR_DESIGNATION = ?" +
					"      , PR_TEST_DATE = ?" +
					"      , PR_TEST_RESULT_CODE = ?" +
					"      , PR_RESOLVED_IND = ?" +
					"      , PR_FOLLOW_UP_NUM = ?" +
					"      , PR_FOLLOW_UP_DATE = ?" +
					"      , PR_VALIDITY_DATE = ?" +
					"      , PR_PAID_IND = ?" +
					"      , PR_NEW_TEST_ONLY = ?" +
					"      , PR_DATE_SENT = ?" +
					receivedDate +
					"      , PR_CLIENT_ID = ?" +
					"      , UPDATED_BY = ?" +
					"      , UPDATED_DATE = ?" +
					"	   , PR_IMG_REFERENCE_NUM = ?" +	
					"  WHERE UAR_REFERENCE_NUM = ?" +
					"  AND PR_LEVEL = ?" +
					"  AND PR_REQT_CODE = ?" +
					parameter +
					"  AND PR_ADMIN_SEQUENCE_NUM = ?";
	
		PreparedStatement ps = null;
		Connection conn = null;
		
		try {
		conn = getConnection();	
		conn.setAutoCommit(false);
		ps = conn.prepareStatement(sql);
		ps.setString(1, requirement.getReferenceNumber());
		ps.setLong(2, requirement.getSequenceNumber());
		ps.setString(3, requirement.getRequirementCode());
		ps.setString(4, requirement.getLevel());
		ps.setDate(5, DateHelper.sqlDate(requirement.getABACUScreateDate()));
		ps.setBoolean(6, requirement.isCompletedRequirementInd());
		ps.setLong(7, requirement.getStatus().getStatusId());
		ps.setDate(8, DateHelper.sqlDate(requirement.getStatusDate()));
		ps.setString(9,requirement.getABACUSUpdatedBy());
		ps.setDate(10,DateHelper.sqlDate(requirement.getABACUSUpdateDate()));
		ps.setString(11,requirement.getDesignation());
		ps.setDate(12,DateHelper.sqlDate(requirement.getTestDate()));
		ps.setString(13,requirement.getTestResultCode());
		ps.setBoolean(14,requirement.isResolveInd());
		ps.setLong(15,requirement.getFollowUpNumber());
		ps.setDate(16,DateHelper.sqlDate(requirement.getFollowUpDate()));
		ps.setDate(17,DateHelper.sqlDate(requirement.getValidityDate()));
		ps.setBoolean(18,requirement.isPaidInd());
		ps.setBoolean(19,requirement.getNewTestOnly());
		ps.setDate(20,DateHelper.sqlDate(requirement.getDateSent()));
		if(requirement.getStatus().getStatusId()==IUMConstants.STATUS_RECEIVED_IN_SITE){
			ps.setDate(21,DateHelper.sqlDate(new Date()));
			ps.setString(22,requirement.getClientId());
			ps.setString(23,requirement.getUpdatedBy());
			ps.setTimestamp(24,DateHelper.sqlTimestamp(requirement.getUpdateDate()));
			ps.setString(25,requirement.getImageRef());
			ps.setString(26,requirement.getReferenceNumber());
			ps.setString(27,requirement.getLevel());
			ps.setString(28,requirement.getRequirementCode());
			if(requirement.getLevel().equalsIgnoreCase(IUMConstants.LEVEL_CLIENT)){
				ps.setString(29,requirement.getClientId());
				ps.setLong(30, requirement.getSequenceNumber());
			}else {
				ps.setLong(29, requirement.getSequenceNumber());
			}
		}else{
			ps.setString(21,requirement.getClientId());
			ps.setString(22,requirement.getUpdatedBy());
			ps.setTimestamp(23,DateHelper.sqlTimestamp(requirement.getUpdateDate()));
			ps.setString(24,requirement.getImageRef());
			ps.setString(25,requirement.getReferenceNumber());
			ps.setString(26,requirement.getLevel());
			ps.setString(27,requirement.getRequirementCode());
			if(requirement.getLevel().equalsIgnoreCase(IUMConstants.LEVEL_CLIENT)){
				ps.setString(28,requirement.getClientId());
				ps.setLong(29, requirement.getSequenceNumber());
			}else {
				ps.setLong(28, requirement.getSequenceNumber());
			}
		}
		
		ps.executeUpdate();
		conn.commit();
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			if(conn != null){
				conn.rollback();
			}
			 throw e; 
		}finally{
			  closeResources(conn, ps, null);
		}
		LOGGER.info("updateRequirementRetainComment end");
	}
	
	public void updateRequirement(PolicyRequirementsData requirement) throws SQLException {
		
		LOGGER.info("updateRequirement start");
		String sql = "UPDATE POLICY_REQUIREMENTS" +
					"    SET UAR_REFERENCE_NUM = ?" +
					"      , PR_ADMIN_SEQUENCE_NUM = ?" +
					"      , PR_REQT_CODE = ?" +
					"      , PR_LEVEL = ?" +
					"      , PR_ADMIN_CREATE_DATE = ?" +
					"      , PR_COMPLT_REQT_IND = ?" +
					"      , PR_STATUS_ID = ?" +
					"      , PR_STATUS_DATE = ?" +
					"      , PR_ADMIN_UPDTD_BY = ?" +
					"      , PR_ADMIN_UPDTD_DATE = ?" +
					"      , PR_CLIENT_TYPE = ?" +
					"      , PR_DESIGNATION = ?" +
					"      , PR_TEST_DATE = ?" +
					"      , PR_TEST_RESULT_CODE = ?" +
					"      , PR_TEST_RESULT_DESC = ?" +
					"      , PR_RESOLVED_IND = ?" +
					"      , PR_FOLLOW_UP_NUM = ?" +
					"      , PR_FOLLOW_UP_DATE = ?" +
					"      , PR_VALIDITY_DATE = ?" +
					"      , PR_PAID_IND = ?" +
					"      , PR_NEW_TEST_ONLY = ?" +
					"      , PR_ORDER_DATE = ?" +
					"      , PR_DATE_SENT = ?" +
					"      , PR_RECEIVED_DATE = ?" +
					"      , PR_AUTO_ORDER_IND = ?" +
					"      , PR_FLD_COMMENT_IND = ?" +
					"      , PR_COMMENTS = ?" +
					"      , PR_CLIENT_ID = ?" +
					"      , UPDATED_BY = ?" +
					"      , UPDATED_DATE = ?" +
					"  WHERE PR_REQUIREMENT_ID = ?";
		PreparedStatement ps = null;
		Connection conn = null;
		
		try {
		conn = getConnection();
		conn.setAutoCommit(false);
		ps = conn.prepareStatement(sql);
		ps.setString(1, requirement.getReferenceNumber());
		ps.setLong(2, requirement.getSequenceNumber());
		ps.setString(3, requirement.getRequirementCode());
		ps.setString(4, requirement.getLevel());
		ps.setDate(5, DateHelper.sqlDate(requirement.getABACUScreateDate()));
		ps.setBoolean(6, requirement.isCompletedRequirementInd());
		ps.setLong(7, requirement.getStatus().getStatusId());
		ps.setDate(8, DateHelper.sqlDate(requirement.getStatusDate()));
		ps.setString(9,requirement.getABACUSUpdatedBy());
		ps.setDate(10,DateHelper.sqlDate(requirement.getABACUSUpdateDate()));
		ps.setString(11,requirement.getClientType());
		ps.setString(12,requirement.getDesignation());
		ps.setDate(13,DateHelper.sqlDate(requirement.getTestDate()));
		ps.setString(14,requirement.getTestResultCode());
		ps.setString(15,requirement.getTestResultDesc());
		ps.setBoolean(16,requirement.isResolveInd());
		ps.setLong(17,requirement.getFollowUpNumber());
		ps.setDate(18,DateHelper.sqlDate(requirement.getFollowUpDate()));
		ps.setDate(19,DateHelper.sqlDate(requirement.getValidityDate()));
		ps.setBoolean(20,requirement.isPaidInd());
		ps.setBoolean(21,requirement.getNewTestOnly());
		ps.setDate(22,DateHelper.sqlDate(requirement.getOrderDate()));
		ps.setDate(23,DateHelper.sqlDate(requirement.getDateSent()));
		ps.setDate(24,DateHelper.sqlDate(requirement.getReceiveDate()));
		ps.setBoolean(25,requirement.getAutoOrderInd());
		ps.setBoolean(26,requirement.getFldCommentInd());
		ps.setString(27,requirement.getComments());
		ps.setString(28,requirement.getClientId());
		ps.setString(29,requirement.getUpdatedBy());
		ps.setTimestamp(30,DateHelper.sqlTimestamp(requirement.getUpdateDate()));
		ps.setLong(31,requirement.getRequirementId());
		ps.executeUpdate();
		conn.commit();

		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			if(conn != null){
				conn.rollback();
			}
			throw e; 
		}finally{
			  closeResources(conn, ps, null);
		}
		LOGGER.info("updateRequirement end");
	}
	
	
	public void updateRequirementMatch(PolicyRequirementsData requirement) throws SQLException {
		
		LOGGER.info("updateRequirementMatch start 1");
		String parameter = 	"  UAR_REFERENCE_NUM = ?" +
						 	"  AND PR_LEVEL = ?" +
						 	"  AND PR_REQT_CODE = ?";
		
		if(requirement.getLevel().equalsIgnoreCase(IUMConstants.LEVEL_CLIENT)){
				parameter = parameter + "  AND PR_CLIENT_ID = ?";
		}else{
//				parameter = parameter + "  AND PR_ADMIN_SEQUENCE_NUM = ?";
		}
		String sql = "UPDATE POLICY_REQUIREMENTS" +
					"    SET  PR_COMPLT_REQT_IND = ?"  +
					"      , PR_STATUS_ID = ?" +
					"      , UPDATED_BY = ?" +
					"      , UPDATED_DATE = ?" +	
					"	   , PR_IMG_REFERENCE_NUM = ?" +
					"	   , PR_SCAN_DATE = ?" +
					"	   , PR_OLD_STATUS_ID = ?" +
					"	   , PR_RECEIVED_DATE = ?" +
					"      , PR_TEST_DATE = ?" +
					"  WHERE " +	parameter;
		
		PreparedStatement ps = null;
			
		
		Connection conn = null;
		
		try {
		conn = getConnection();	
		conn.setAutoCommit(false);
		ps = conn.prepareStatement(sql);	
		ps.setBoolean(1, requirement.isCompletedRequirementInd());
		ps.setLong(2, requirement.getStatus().getStatusId());		
		ps.setString(3,requirement.getUpdatedBy());
		ps.setTimestamp(4,DateHelper.sqlTimestamp(requirement.getUpdateDate()));
		ps.setString(5, requirement.getImageRef());
		ps.setTimestamp(6, DateHelper.sqlTimestamp(requirement.getScanDate()));
		ps.setLong(7, requirement.getStatus().getOldStatusId());
		ps.setTimestamp(8,DateHelper.sqlTimestamp(requirement.getReceiveDate()));
		try {
			ps.setTimestamp(9,DateHelper.sqlTimestamp(requirement.getTestDate()));
		} catch (Exception e) {
			LOGGER.error(CodeHelper.getStackTrace(e));	
		}
		
		ps.setString(10,requirement.getReferenceNumber());
		ps.setString(11, requirement.getLevel());
		ps.setString(12,requirement.getRequirementCode());
		
		if(requirement.getLevel().equalsIgnoreCase(IUMConstants.LEVEL_CLIENT)){
			if( requirement.getClientId()!= null){
				ps.setString(16,requirement.getClientId());
			}
		}else{
			if( requirement.getSequenceNumber()!= 0){
//				ps.setLong(14,requirement.getSequenceNumber());
			}
		}
								
		ps.executeUpdate();
		conn.commit();
		} catch (SQLException e) {
				LOGGER.error(CodeHelper.getStackTrace(e));	
				if(conn != null){
					conn.rollback();
				}
				throw e; 
		}finally{
			  closeResources(conn, ps, null);
		}
		LOGGER.info("updateRequirementMatch end 1");
	
	}
	
	public void updateRequirementMatch(PolicyRequirementsData requirement, long reqtId) throws SQLException {
		
		LOGGER.info("updateRequirementMatch start 2");
		String sql = "UPDATE POLICY_REQUIREMENTS" +
					"    SET PR_COMPLT_REQT_IND = ?"  +
					"      , PR_STATUS_ID = ?" +
					"      , UPDATED_BY = ?" +
					"      , UPDATED_DATE = ?" +	
					"	   , PR_IMG_REFERENCE_NUM = ?" +
					"	   , PR_SCAN_DATE = ?" +
					"	   , PR_OLD_STATUS_ID = ?" +
					"	   , PR_RECEIVED_DATE = ?" +
					"      , PR_TEST_DATE = ?" +
					"  WHERE PR_REQUIREMENT_ID = ?";
		
		PreparedStatement ps = null;
		Connection conn = null;
		
		try {
		conn = getConnection();		
		conn.setAutoCommit(false);
		ps = conn.prepareStatement(sql);	
		ps.setBoolean(1, requirement.isCompletedRequirementInd());
		ps.setLong(2, requirement.getStatus().getStatusId());
				
		ps.setString(3,requirement.getUpdatedBy());
		ps.setTimestamp(4,DateHelper.sqlTimestamp(requirement.getUpdateDate()));
		ps.setString(5, requirement.getImageRef());
		ps.setTimestamp(6, DateHelper.sqlTimestamp(requirement.getScanDate()));
		ps.setLong(7, requirement.getStatus().getOldStatusId());
		ps.setTimestamp(8,DateHelper.sqlTimestamp(requirement.getReceiveDate()));
		ps.setTimestamp(9,DateHelper.sqlTimestamp(requirement.getTestDate()));
		ps.setLong(10,reqtId);
		ps.executeUpdate();
		conn.commit();
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			if(conn != null){
				conn.rollback();
			}
			throw e; 
		}finally{
			  closeResources(conn, ps, null);
		}
		LOGGER.info("updateRequirementMatch end 2");
	
	}
	
	public void updateRequirementUnmatch(PolicyRequirementsData prd) throws SQLException{
		
		LOGGER.info("updateRequirementUnmatch start");
				
		String sql = "UPDATE POLICY_REQUIREMENTS" +
					"    SET PR_STATUS_ID = ?"  +
					"      , UPDATED_BY = ?" +
					"      , UPDATED_DATE = ?" +
					"	   , PR_IMG_REFERENCE_NUM = ?" +
					"  WHERE PR_REQUIREMENT_ID = ?";
		
		PreparedStatement ps = null;
		Connection conn = null;
		
		try {
			
		conn = getConnection();
		conn.setAutoCommit(false);
		ps = conn.prepareStatement(sql);
		
		long oldStatusId = 0;
		try {
			oldStatusId = getOldStatus(prd);
		} catch (Exception e) {
			oldStatusId = 0;
			e.printStackTrace();
		}
		
		if(oldStatusId == 0){
			ps.setLong(1, IUMConstants.STATUS_REQ_CANCELLED);
		}else{
			ps.setLong(1, oldStatusId);
		}
		ps.setString(2,prd.getUpdatedBy());
		ps.setTimestamp(3,DateHelper.sqlTimestamp(prd.getUpdateDate()));
		ps.setString(4, IUMConstants.DEFAULT_IMG_REF_NUM);
		ps.setLong(5,prd.getRequirementId());
		
		ps.executeUpdate();
		conn.commit();
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
				if(conn != null){
					conn.rollback();
				}
			 throw e; 
		}finally{
			  closeResources(conn, ps, null);
		}
		LOGGER.info("updateRequirementUnmatch end");
	}
	
	public void updateRequirementUnmatchWoutReqId(PolicyRequirementsData prd) throws SQLException{

		LOGGER.info("updateRequirementUnmatchWoutReqId start");
		String sql = "UPDATE POLICY_REQUIREMENTS" +
					"    SET PR_STATUS_ID = ?"  +
					"     , UPDATED_BY = ?" +
					"     , UPDATED_DATE = ?" +
					"	  , PR_IMG_REFERENCE_NUM = ?" +
					"  WHERE UAR_REFERENCE_NUM = ?" +
					"	AND PR_IMG_REFERENCE_NUM = ?" +
					" AND PR_REQT_CODE = ?" +
					" AND PR_ADMIN_SEQUENCE_NUM = ?";
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection conn = null;
		
		try {
			
		conn = getConnection();	
		conn.setAutoCommit(false);
		ps = conn.prepareStatement(sql);
		
		long oldStatusId = 0;
		try {
			oldStatusId = getOldStatusWoutReqId(prd);
		} catch (Exception e) {
			oldStatusId = 0;
			e.printStackTrace();
		}
		
		if(oldStatusId == 0){
			ps.setLong(1, IUMConstants.STATUS_REQ_CANCELLED);
		}else{
			ps.setLong(1, oldStatusId);
		}
		ps.setString(2,prd.getUpdatedBy());
		ps.setTimestamp(3,DateHelper.sqlTimestamp(prd.getUpdateDate()));
		ps.setString(4, IUMConstants.DEFAULT_IMG_REF_NUM);
		
		ps.setString(5,prd.getReferenceNumber());
		ps.setString(6,prd.getImageRef());
		ps.setString(7,prd.getRequirementCode());
		ps.setLong(8,prd.getSequenceNumber());
		ps.executeUpdate();
		conn.commit();
		} catch (SQLException e) {
			  LOGGER.error(CodeHelper.getStackTrace(e));
			  if(conn != null){
					conn.rollback();
				}
			  throw e; 
		}finally{
			  closeResources(conn, ps, null);
		}
		LOGGER.info("updateRequirementUnmatchWoutReqId end");
	}
	
	public ArrayList retrieveClientReqts(String level, String clientID, String reqtCode) throws SQLException {
		
		LOGGER.info("retrieveClientReqts start");
		ArrayList list = new ArrayList();
		String sql = "SELECT PR_REQUIREMENT_ID, PR_CLIENT_ID, PR_REQT_CODE, PR_STATUS_ID, PR_LEVEL, TO_CHAR(PR_VALIDITY_DATE, 'DD-MON-YYYY') PR_VALIDITY_DATE" +
					" FROM POLICY_REQUIREMENTS" +
					" WHERE PR_LEVEL = ?" +
					" AND PR_CLIENT_ID = ?" +
					" AND PR_REQT_CODE = ?";
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection conn = null;
		
		try {
		conn = getConnection();	
		ps = conn.prepareStatement(sql);
		ps.setString(1, level);
		ps.setString(2, clientID);
		ps.setString(3, reqtCode);
		rs = ps.executeQuery();
		while (rs.next()) {
			PolicyRequirementsData data = new PolicyRequirementsData();
			data.setRequirementId(rs.getLong("PR_REQUIREMENT_ID"));
			data.setClientId(rs.getString("PR_CLIENT_ID"));
			data.setRequirementCode(rs.getString("PR_REQT_CODE"));
			StatusData status = new StatusData();
			status.setStatusId(rs.getLong("PR_STATUS_ID"));
			data.setStatus(status);
			data.setLevel(rs.getString("PR_LEVEL"));
			data.setValidityDate(DateHelper.parse(rs.getString("PR_VALIDITY_DATE"), "dd-MMM-yyyy"));
			list.add(data);
		}
		} catch (SQLException e) {
				LOGGER.error(CodeHelper.getStackTrace(e));	
			  throw e; 
		}finally{
			  closeResources(conn, ps, rs);
		}
		LOGGER.info("retrieveClientReqts end");
		return list;
	}
	
	public ArrayList retrievePolicyReqts(String level, String referenceNum, String reqtCode) throws SQLException {
		
		LOGGER.info("retrievePolicyReqts start");
		ArrayList list = new ArrayList();
		String sql = "SELECT PR_REQUIREMENT_ID, UAR_REFERENCE_NUM, PR_REQT_CODE, PR_STATUS_ID, PR_LEVEL, TO_CHAR(PR_VALIDITY_DATE, 'DD-MON-YYYY') PR_VALIDITY_DATE" +
					" FROM POLICY_REQUIREMENTS" +
					" WHERE PR_LEVEL = ?" +
					" AND UAR_REFERENCE_NUM = ?" +
					" AND PR_REQT_CODE = ?";
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection conn = null;
		
		try {
			conn = getConnection();
			ps = conn.prepareStatement(sql);
			ps.setString(1, level);
			ps.setString(2, referenceNum);
			ps.setString(3, reqtCode);
			rs = ps.executeQuery();
			while (rs.next()) {
				PolicyRequirementsData data = new PolicyRequirementsData();
				data.setRequirementId(rs.getLong("PR_REQUIREMENT_ID"));
				data.setReferenceNumber(rs.getString("UAR_REFERENCE_NUM"));
				data.setRequirementCode(rs.getString("PR_REQT_CODE"));
				StatusData status = new StatusData();
				status.setStatusId(rs.getLong("PR_STATUS_ID"));
				data.setStatus(status);
				data.setLevel(rs.getString("PR_LEVEL"));
				data.setValidityDate(DateHelper.parse(rs.getString("PR_VALIDITY_DATE"), "dd-MMM-yyyy"));
				list.add(data);
		}
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));	
			  throw e; 
		}finally{
			  closeResources(conn, ps, rs);
		}
		LOGGER.info("retrievePolicyReqts end");
		return list;
	}
	
	public ArrayList retrieveARWithPendingReqt(ARWithPendingReqtFilterData criteria) throws SQLException {
		
		LOGGER.info("retrieveARWithPendingReqt start");
		ArrayList list = new ArrayList();
		StringBuffer sql = new StringBuffer();
		boolean isBranchType = criteria.getReportType() == ARWithPendingReqtFilterData.BRANCH_REPORT_TYPE;
		
		sql.append("select ");
		sql.append("  SLO.SLO_OFFICE_NAME as SLO_OFFICE_NAME");
		sql.append(", US.USR_LAST_NAME as USR_LAST_NAME ");
		sql.append(", US.USR_FIRST_NAME as USR_FIRST_NAME "); 
		sql.append(", US.USR_MIDDLE_NAME as  USR_MIDDLE_NAME");
		sql.append(", AR.REFERENCE_NUM as REFERENCE_NUM ");
		sql.append(", PR.PR_REQT_CODE as REQT_CODE");
		sql.append(", RQ.REQT_DESC as REQT_DESC ");
		sql.append(", to_char(PR.PR_ORDER_DATE, 'ddMONyyyy') as PR_ORDER_DATE ");
		sql.append(", to_char(PR.PR_FOLLOW_UP_DATE, 'ddMONyyyy') as PR_FOLLOW_UP_DATE ");
		sql.append(", count(PR.PR_REQUIREMENT_ID) as COUNT ");
		sql.append(" from ");
		sql.append("  ASSESSMENT_REQUESTS AR ");
		sql.append(",  REQUIREMENTS RQ ");
		sql.append(", SUNLIFE_OFFICES SLO ");
		sql.append(", USERS US ");
		sql.append(", POLICY_REQUIREMENTS PR ");
		sql.append(" where ");
		sql.append("     AR.REFERENCE_NUM = PR.UAR_REFERENCE_NUM ");
		sql.append(" and AR.BRANCH_ID = SLO.SLO_OFFICE_CODE ");
		sql.append(" and AR.UNDERWRITER = US.USER_ID ");
		sql.append(" and PR.PR_REQT_CODE = RQ.REQT_CODE ");
		sql.append(" and PR.PR_STATUS_ID = ?");
		sql.append(" and PR.PR_ORDER_DATE ");
		sql.append("        between to_date(?, 'dd-MON-yyyy hh24:mi:ss') ");
		sql.append("            and to_date(?, 'dd-MON-yyyy hh24:mi:ss') ");
		
		String branchId = criteria.getBranch().getOfficeId();
		if (branchId != null) {
			sql.append(" and SLO.SLO_OFFICE_CODE = '" + branchId + "' ");
		}
		
		String underwriterId = criteria.getUnderwriter().getUserId();
		if (underwriterId != null) {
			sql.append(" and US.USER_ID = '" + underwriterId + "' ");
		}
		
		sql.append(" group by grouping sets ( ");
		
		if (isBranchType) { // depends on report type
			sql.append(" (SLO.SLO_OFFICE_NAME) ");
			sql.append(",(SLO.SLO_OFFICE_NAME, US.USR_LAST_NAME, US.USR_FIRST_NAME, US.USR_MIDDLE_NAME, AR.REFERENCE_NUM, PR.PR_REQT_CODE, RQ.REQT_DESC, PR.PR_ORDER_DATE, PR.PR_FOLLOW_UP_DATE)");
			sql.append(" )");
			sql.append(" order by 1, 2, 3, 4, 5, 6, 7 ");
			 
		}
		else {
			sql.append(" (US.USR_LAST_NAME, US.USR_FIRST_NAME, US.USR_MIDDLE_NAME) ");
			sql.append(",(US.USR_LAST_NAME, US.USR_FIRST_NAME, US.USR_MIDDLE_NAME, SLO.SLO_OFFICE_NAME, AR.REFERENCE_NUM, PR.PR_REQT_CODE, RQ.REQT_DESC, PR.PR_ORDER_DATE, PR.PR_FOLLOW_UP_DATE)");
			sql.append(" )");
			sql.append(" order by 2, 3, 4, 1, 5, 6, 7 ");			
		}
		
		String startDate = DateHelper.addTimePart(criteria.getStartDate(),DateHelper.ADD_START_DAY_TIME);
		String endDate = DateHelper.addTimePart(criteria.getEndDate(), DateHelper.ADD_END_DAY_TIME);
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection conn = null;
		
		try {
			conn = getConnection();
			ps = conn.prepareStatement(sql.toString());
			
			ps.setLong(1, IUMConstants.STATUS_ORDERED); // ordered REQTs only
			ps.setString(2, startDate);
			ps.setString(3, endDate);
			rs = ps.executeQuery();
			
			// fill up the data object
			while (rs.next()) {
				ARWithPendingReqtData reportData = new ARWithPendingReqtData();
				
				reportData.setBranchName(ValueConverter.nullToString(rs.getString("SLO_OFFICE_NAME")));
				reportData.setUnderwriterLastName(ValueConverter.nullToString(rs.getString("USR_LAST_NAME")));
				reportData.setUnderwriterGivenName(ValueConverter.nullToString(rs.getString("USR_FIRST_NAME")));
				reportData.setUnderwriterMidName(ValueConverter.nullToString(rs.getString("USR_MIDDLE_NAME")));
				reportData.setReferenceNumber(ValueConverter.nullToString(rs.getString("REFERENCE_NUM")));
				reportData.setRequirementCode(ValueConverter.nullToString(rs.getString("REQT_CODE")));
				reportData.setRequirementName(ValueConverter.nullToString(rs.getString("REQT_DESC")));
				reportData.setDateOrdered(rs.getString("PR_ORDER_DATE"));
				reportData.setDateSubmit(rs.getString("PR_FOLLOW_UP_DATE"));
				reportData.setTotalCount(rs.getInt("COUNT"));
				
				list.add(reportData);
			}
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));	
			  throw e; 
		}finally{
			  closeResources(conn, ps, rs);
		}	
		LOGGER.info("retrieveARWithPendingReqt end");
		return list;
	}

	public void deletePolicyRequirements(String referenceNum) throws SQLException {
		
		LOGGER.info("deletePolicyRequirements start");
		String sql = "DELETE FROM POLICY_REQUIREMENTS WHERE UAR_REFERENCE_NUM = ?";
		
		PreparedStatement ps = null;
		Connection conn = null;
		
		try {
			conn = getConnection();	
			conn.setAutoCommit(false);
			ps = conn.prepareStatement(sql);
			ps.setString(1, referenceNum);
			ps.executeUpdate();
			conn.commit();
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			if(conn != null){
				conn.rollback();
			}
			throw e; 
		}finally{
			  closeResources(conn, ps, null);
		}	
		LOGGER.info("deletePolicyRequirements end");
	}
	
	public void deletePolicyRequirement(long reqtId) throws SQLException {
		
		LOGGER.info("deletePolicyRequirement start 1");
		String sql1 = "DELETE FROM ATTACHMENTS WHERE REQUIREMENT_ID = ?";
		String sql2 = "DELETE FROM POLICY_REQUIREMENTS WHERE PR_REQUIREMENT_ID = ?";
		
		PreparedStatement ps = null;
		Connection conn = null;
		
		try {
			conn = getConnection();	
			conn.setAutoCommit(false);
		
			ps = conn.prepareStatement(sql1);
			ps.setLong(1, reqtId);
			ps.executeUpdate();
			
			ps = conn.prepareStatement(sql2);
			ps.setLong(1, reqtId);
			ps.executeUpdate();
			conn.commit();
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));	
			  throw e; 
		}finally{
			  closeResources(conn, ps, null);
		}	
		LOGGER.info("deletePolicyRequirement end 1");
	}
	
	public void deletePolicyRequirement(PolicyRequirementsData requirement) throws SQLException {
		
		LOGGER.info("deletePolicyRequirement start 2");
		String sql = "DELETE FROM POLICY_REQUIREMENTS WHERE UAR_REFERENCE_NUM = ?"
					+ " AND PR_LEVEL = ?"
					+ " AND PR_REQT_CODE = ?";
		
		if(requirement.getLevel().equalsIgnoreCase(IUMConstants.LEVEL_CLIENT)){
			sql = sql + " AND PR_CLIENT_ID = ?";
		}
		
		PreparedStatement ps = null;
		Connection conn = null;
		
		try {
			conn = getConnection();	
			conn.setAutoCommit(false);
		
			ps = conn.prepareStatement(sql);
			ps.setString(1, requirement.getReferenceNumber());
			ps.setString(2, requirement.getLevel());
			ps.setString(3, requirement.getRequirementCode());
			
			if(requirement.getLevel().equalsIgnoreCase(IUMConstants.LEVEL_CLIENT)){
				ps.setString(4, requirement.getClientId());
			}
			
			ps.executeUpdate();
			conn.commit();
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			if(conn != null){
				conn.rollback();
			}
			throw e; 
		}finally{
			  closeResources(conn, ps, null);
		}	
		LOGGER.info("deletePolicyRequirement end 2");
	}
	
	public ArrayList retrievePolicyRequirementsByReferenceNumber(String referenceNumber) throws SQLException{
		
		LOGGER.info("retrievePolicyRequirementsByReferenceNumber start");
		ArrayList list = new ArrayList();
		String sql1 = "select PR_REQUIREMENT_ID ,UAR_REFERENCE_NUM" +
							" ,PR_ADMIN_SEQUENCE_NUM ,PR_REQT_CODE" +
							" ,PR_LEVEL ,PR_ADMIN_CREATE_DATE" +
							" ,PR_COMPLT_REQT_IND ,PR_STATUS_ID" +
							" ,STAT_DESC ,PR_STATUS_DATE" +
							" ,PR_ADMIN_UPDTD_BY ,PR_ADMIN_UPDTD_DATE" +
							" ,PR_CLIENT_TYPE ,PR_DESIGNATION" +
							" ,PR_TEST_DATE ,PR_TEST_RESULT_CODE" +
							" ,PR_TEST_RESULT_DESC ,PR_RESOLVED_IND" +
							" ,PR_FOLLOW_UP_NUM ,PR_FOLLOW_UP_DATE" +
							" ,PR_VALIDITY_DATE ,PR_PAID_IND" +
							" ,PR_NEW_TEST_ONLY ,PR_ORDER_DATE" +
							" ,PR_DATE_SENT ,PR_CCAS_SUGGEST_IND" +
							" ,PR_RECEIVED_DATE ,PR_AUTO_ORDER_IND" +
							" ,PR_FLD_COMMENT_IND ,PR_COMMENTS ,PR_CLIENT_ID" + 
							" ,POLICY_REQUIREMENTS.CREATED_BY ,POLICY_REQUIREMENTS.CREATED_DATE" +
							" ,POLICY_REQUIREMENTS.UPDATED_BY ,POLICY_REQUIREMENTS.UPDATED_DATE" +
							" from STATUS, POLICY_REQUIREMENTS" +
							" where PR_STATUS_ID = STAT_ID" +
							" and UAR_REFERENCE_NUM = ?";
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection conn = null;
		
		try {
			conn = getConnection();
		ps = conn.prepareStatement(sql1);
		ps.setString(1, referenceNumber);
		rs = ps.executeQuery();
		
		while (rs.next()) {
			PolicyRequirementsData policyReqData = new PolicyRequirementsData();
			policyReqData.setSequenceNumber(rs.getLong("pr_admin_sequence_num"));
			policyReqData.setReferenceNumber(rs.getString("uar_reference_num"));
			policyReqData.setRequirementId(rs.getLong("pr_requirement_id"));
			policyReqData.setRequirementCode(rs.getString("pr_reqt_code"));
			policyReqData.setLevel(rs.getString("pr_level"));
			policyReqData.setABACUScreateDate(DateHelper.utilDate(rs.getDate("pr_admin_create_date")));
			policyReqData.setCompletedRequirementInd(rs.getBoolean("pr_complt_reqt_ind"));
			StatusData statusData = new StatusData();
			statusData.setStatusId(rs.getLong("pr_status_id"));
			statusData.setStatusDesc(rs.getString("stat_desc"));
			policyReqData.setStatus(statusData);				
			policyReqData.setStatusDate(DateHelper.utilDate(rs.getDate("pr_status_date")));  
			policyReqData.setABACUSUpdatedBy(rs.getString("pr_admin_updtd_by"));
			policyReqData.setABACUSUpdateDate(DateHelper.utilDate(rs.getDate("pr_admin_updtd_date")));
			policyReqData.setClientType(rs.getString("pr_client_type"));
			policyReqData.setClientId(rs.getString("pr_client_id"));
			policyReqData.setDesignation(rs.getString("pr_designation"));
			policyReqData.setTestDate(DateHelper.utilDate(rs.getDate("pr_test_date")));
			policyReqData.setTestResultCode(rs.getString("pr_test_result_code"));
			policyReqData.setTestResultDesc(rs.getString("pr_test_result_desc"));
			policyReqData.setResolveInd(rs.getBoolean("pr_resolved_ind"));
			policyReqData.setFollowUpNumber(rs.getLong("pr_follow_up_num"));
			policyReqData.setFollowUpDate(DateHelper.utilDate(rs.getDate("pr_follow_up_date")));
			policyReqData.setValidityDate(DateHelper.utilDate(rs.getDate("pr_validity_date")));
			policyReqData.setPaidInd(rs.getBoolean("pr_paid_ind"));
			policyReqData.setNewTestOnly(rs.getBoolean("pr_new_test_only"));
			policyReqData.setOrderDate(DateHelper.utilDate(rs.getDate("pr_order_date")));
			policyReqData.setDateSent(DateHelper.utilDate(rs.getDate("pr_date_sent")));
			policyReqData.setCCASuggestInd(rs.getBoolean("pr_ccas_suggest_ind"));
			policyReqData.setReceiveDate(DateHelper.utilDate(rs.getDate("pr_received_date")));
			policyReqData.setAutoOrderInd(rs.getBoolean("pr_auto_order_ind"));
			policyReqData.setFldCommentInd(rs.getBoolean("pr_fld_comment_ind"));
			policyReqData.setComments(rs.getString("pr_comments")); 
			policyReqData.setCreatedBy(rs.getString("created_by"));
			policyReqData.setCreateDate(rs.getTimestamp("created_date"));
			policyReqData.setUpdatedBy(rs.getString("updated_by"));
			policyReqData.setUpdateDate(rs.getTimestamp("updated_date"));
			list.add(policyReqData);				
		}
		
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));	
			  throw e; 
		}finally{
			  closeResources(conn, ps, rs);
		}	
		LOGGER.info("retrievePolicyRequirementsByReferenceNumber end");
		return list; 
	}


	public static class PolicyRequirementDAOHelper {
		
		public static String getRequirementCodes(String reqCodes) {
			return splitAndQuote(reqCodes);
		}
		
		public static String getPlanCodes(String planCodes) {
			return splitAndQuote(planCodes);
		}

		private static String splitAndQuote(String stringToSplit) {
			LOGGER.info("splitAndQuote start");
			String splitAndQuote = "";
			final String COMMA = ",";
			final String APOS = "'";
			String[] array = StringHelper.split(stringToSplit, COMMA);
			if (array.length > 0) {
				for (int i=0; i < array.length; i++) {
					array[i] = APOS.concat(array[i]).concat(APOS);
					splitAndQuote = splitAndQuote.concat(array[i].concat(COMMA));
				}
				splitAndQuote = splitAndQuote.substring(0, splitAndQuote.length()-1);
			} else {
				splitAndQuote = APOS.concat(stringToSplit).concat(APOS);
			}
			LOGGER.info("splitAndQuote end");
			return splitAndQuote;
		}
	}
	
	/**
	 * Retrieve pr_test_date in policy_requirements
	 * @param reqId main id in policy_requirements
	 * @return String Formatted pr_test_date
	 * 
	 * ** Made changes according to v1.1 - Sept 25, 2014
	 * @throws SQLException 
	 */
	public Date retrievePrTestDate(final long reqId) throws SQLException {
		
		LOGGER.info("retrievePrTestDate start");
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		Date prTestDate = null;
		String sql = "SELECT pr_test_date FROM policy_requirements WHERE pr_requirement_id = ?";
		Connection conn = null;
		
		try {
			conn = getConnection();
			ps = conn.prepareStatement(sql.toString());
			ps.setLong(1, reqId);
			rs = ps.executeQuery();
       	
			if (rs.next()) {
				prTestDate = DateHelper.utilDate(rs.getDate("pr_test_date"));
			} 
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));	
		} finally {
			closeResources(conn, ps, rs);
		}
			
		LOGGER.info("retrievePrTestDate end");
		return prTestDate;
	}// retrieveRequirement
	
	public HashMap retrieveInitialDatesByRefNumAndClientId(String referenceNum, String clientId) throws SQLException {
		LOGGER.info("retrieveInitialDatesByRefNumAndClientId start");
		String query = "Select * From (Select Distinct Pr.Pr_Requirement_Id, Pr.PR_ORDER_DATE, PR_FOLLOW_UP_DATE From Policy_Requirements Pr, Users U "
				+ "Where Pr.Created_By = U.User_Id And Pr.Uar_Reference_Num = ? And Pr.Pr_Client_Id = ? And U.Usr_Role = '" + IUMConstants.ROLES_UNDERWRITER 
				+ "' and Pr.PR_FOLLOW_UP_DATE is not null and Pr.PR_ORDER_DATE is not null order by Pr.PR_REQUIREMENT_ID) Where Rownum = 1 AND PR_Order_Date >= '24-MAY-19'";
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection conn = null;
		
		HashMap resultHm = new HashMap();
		
		try {
			conn = getConnection();
			ps = conn.prepareStatement(query);
			ps.setString(1, referenceNum);
			ps.setString(2, clientId);
			rs = ps.executeQuery();
			
			while (rs.next()) {
				resultHm.put(IUMConstants.ORDER_DATE, rs.getString(IUMConstants.ORDER_DATE));
				resultHm.put(IUMConstants.FOLLOW_UP_DATE, rs.getString(IUMConstants.FOLLOW_UP_DATE));
			}
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));	
			throw e; 
		} finally {
			closeResources(conn, ps, rs);
		}	
		LOGGER.info("retrieveInitialDatesByRefNumAndClientId end");
		return resultHm; 
	}
	
	public int countRequirementsByRefNumAndClientId(String referenceNum, String clientId) throws SQLException {
		LOGGER.info("countRequirementsByRefNumAndClientId start");
		
		String query = "Select Count(1) From (Select uar_reference_num, pr_client_id, pr_reqt_code, pr_date_sent, pr_order_date, pr_follow_up_date "
				+ "FROM Policy_Requirements Pr, Users U Where Pr.Created_By = U.User_Id And Pr.Uar_Reference_Num = ? AND PR.Pr_Client_Id = ? "
				+ "AND U.Usr_Role = '" + IUMConstants.ROLES_UNDERWRITER + "' And Pr.Pr_Order_Date Is Not Null And Pr.Pr_Follow_Up_Date Is Not Null "
				+ "And Pr.Pr_Order_Date >= '24-MAY-19' GROUP BY uar_reference_num, pr_client_id, pr_reqt_code, pr_date_sent, pr_order_date, pr_follow_up_date)";
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection conn = null;
		
		int result = 0;
		
		try {
			conn = getConnection();
			ps = conn.prepareStatement(query);
			ps.setString(1, referenceNum);
			ps.setString(2, clientId);
			rs = ps.executeQuery();
			
			while (rs.next()) {
				 result = rs.getInt(1);
				 LOGGER.info("requirement count: " + result);
			}
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));	
			throw e; 
		} finally {
			closeResources(conn, ps, rs);
		}
		LOGGER.info("countRequirementsByRefNumAndClientId end");	
		return result;
	}
	
	public int countRequirementsGreaterThanFollowUpDate(String referenceNum, String clientId, String initialFollowUpDate) throws SQLException {
		LOGGER.info("getFollowUpDateByRequirementId start");
		LOGGER.info("INITIAL FOLLOW UP DATE: " + initialFollowUpDate);
				
		String query = "Select Count(1) From Policy_Requirements Pr, Users U where Pr.created_by = u.user_id And Pr.Uar_Reference_Num = ? "
				+ "And Pr.Pr_Client_Id = ? And U.Usr_Role = '" + IUMConstants.ROLES_UNDERWRITER + "' and Pr.Pr_Follow_Up_Date Is Not Null "
				+ "And Pr.Pr_Order_Date Is Not Null And Pr.Pr_Follow_Up_Date > TO_DATE('" + initialFollowUpDate + "', '" + IUMConstants.DATE_FORMAT + "')";
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection conn = null;
		
		int result = 0;
		
		try {
			conn = getConnection();
			ps = conn.prepareStatement(query);
			ps.setString(1, referenceNum);
			ps.setString(2, clientId);
			rs = ps.executeQuery();
			
			while (rs.next()) {
				 result = rs.getInt(1);
				 LOGGER.info("overlap count: " + result);
			}
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));	
			throw e; 
		} finally {
			closeResources(conn, ps, rs);
		}	
		LOGGER.info("getFollowUpDateByRequirementId end");
		return result; 
	}

	public int countExistingRequirementsByRefNumAndClientId(String referenceNum, String clientId) throws SQLException {
		LOGGER.info("countExistingRequirementsByRefNumAndClientId start");
		
		String query = "Select Count(1) From (Select * FROM Policy_Requirements Pr, Users U Where Pr.Created_By = U.User_Id And "
				+ "Pr.Uar_Reference_Num = ? AND PR.Pr_Client_Id = ? AND U.Usr_Role = '" + IUMConstants.ROLES_UNDERWRITER + "' And "
				+ "Pr.Pr_Order_Date Is Not Null And Pr.Pr_Follow_Up_Date Is Not Null And Pr.Pr_Order_Date <= '24-MAY-19')";
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection conn = null;
		
		int result = 0;
		
		try {
			conn = getConnection();
			ps = conn.prepareStatement(query);
			ps.setString(1, referenceNum);
			ps.setString(2, clientId);
			rs = ps.executeQuery();
			
			while (rs.next()) {
				 result = rs.getInt(1);
				 LOGGER.info("requirement count: " + result);
			}
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));	
			throw e; 
		} finally {
			closeResources(conn, ps, rs);
		}
		LOGGER.info("countExistingRequirementsByRefNumAndClientId end");	
		return result;
	}
}
