/*
 * Created on Jun 25, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.data.dao.test;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.TreeMap;

import junit.framework.TestCase;

import com.slocpi.ium.data.dao.ProcessConfigDAO;
import com.slocpi.ium.data.util.DataSourceProxy;


/**
 * @author Alain del Rosario
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class ProcessConfigDAOTest extends TestCase {

	/**
	 * Constructor for ProcessConfigDAOTest.
	 * @param arg0
	 */
	public ProcessConfigDAOTest(String arg0) {
		super(arg0);
	}


	public void testProcessConfigDAO() {
				
	}



	public void testIsValidStatus() throws SQLException {
		
		ProcessConfigDAO pcd = new ProcessConfigDAO();
		long from = 24;
		long to = 13;
		boolean isittrue = pcd.isValidStatus("PN","NM",from,to);
		assertTrue(isittrue);
		
	}




	public void testIsEndStatus() throws SQLException {
		
		
		ProcessConfigDAO pcd = new ProcessConfigDAO();
		long from = 18;
		long to = 23;
		boolean isittrue = pcd.isEndStatus("IL","M",from);
		assertTrue(!isittrue);
	}




	public void testGetEvents() throws SQLException {
		
		
		ProcessConfigDAO pcd = new ProcessConfigDAO();
		
		TreeMap ntevents = new TreeMap();
		ntevents = pcd.getEvents("IUMDEV");
				
				
		ArrayList alist1 = new ArrayList();
		ArrayList alist2 = new ArrayList();		
				

		Iterator i = ntevents.keySet().iterator();
		String strone = (String)i.next();
		alist1 = (ArrayList) ntevents.get(strone);
		String strtwo = (String)i.next();
		alist2 = (ArrayList) ntevents.get(strtwo);
		assertEquals(strone,"IL");
		assertEquals(strtwo,"PN");
		assertEquals(alist1.size(),7);
		assertEquals(alist2.size(),2);

	 }
		



	
	
	
	
	
	
	
	




}
