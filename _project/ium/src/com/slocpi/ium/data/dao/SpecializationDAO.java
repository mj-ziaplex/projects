package com.slocpi.ium.data.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.slocpi.ium.data.SpecializationData;
import com.slocpi.ium.util.CodeHelper;

/**
 * SpecializationDAO.java 
 * Description:
 *
 * @version $Revision: 1.1 $
 * @author $Author: pw12 $ $Date: 2012/08/01 07:06:28 $
 */
public class SpecializationDAO {
	private static final Logger LOGGER = LoggerFactory.getLogger(SpecializationDAO.class);
	private Connection conn = null;
	
	/**
	 * 
	 */
	public SpecializationDAO(Connection connection) {
		super();
		this.conn = connection;
	}
	private void closeResources(PreparedStatement ps, ResultSet rs) throws SQLException{
		
		
		try {
			if (ps != null){
				ps.close();
			}
		}
		catch(SQLException e)
		{
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		}
		try {
			if (rs != null){
				rs.close();
			}			
		} catch (SQLException e){
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		}
		
	}
	
	public ArrayList selectSpecializations() throws SQLException{
		
		LOGGER.info("selectSpecializations start");
		String sql = "SELECT SPL_ID, SPL_DESC,CREATED_BY, " +
			"CREATED_DATE,UPDATED_BY,UPDATED_DATE " +
			"FROM SPECIALIZATIONS ";
		ArrayList list = new ArrayList();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery();
			while(rs.next()){
				SpecializationData sp = new SpecializationData();
				sp.setSpecializationId(rs.getLong("SPL_ID"));
				sp.setSpecializationDesc(rs.getString("SPL_DESC"));
				list.add(sp);
			}
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			  throw e; 
		}finally{
			  this.closeResources(ps, rs);
		}	
		LOGGER.info("selectSpecializations end");
		return list;
	}
	
	}
