/**
 * CodeValuePairDAO.java				Copyright (c) 2004 Pointwest Technologies Corp.
 * TODO DOCUMENT ME!
 * 
 * @author tvicencio				@date Feb 23, 2004
 */
package com.slocpi.ium.data.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.slocpi.ium.data.AccessData;
import com.slocpi.ium.data.AccessTemplateData;
import com.slocpi.ium.data.AutoAssignCriteriaData;
import com.slocpi.ium.data.ClientTypeData;
import com.slocpi.ium.data.DocumentTypeData;
import com.slocpi.ium.data.ExaminationAreaData;
import com.slocpi.ium.data.ExaminationPlaceData;
import com.slocpi.ium.data.LOBData;
import com.slocpi.ium.data.MIBActionData;
import com.slocpi.ium.data.MIBImpairmentData;
import com.slocpi.ium.data.MIBLetterData;
import com.slocpi.ium.data.MIBNumberData;
import com.slocpi.ium.data.PageData;
import com.slocpi.ium.data.RolesData;
import com.slocpi.ium.data.SectionData;
import com.slocpi.ium.data.SpecializationData;
import com.slocpi.ium.data.StatusData;
import com.slocpi.ium.data.SunLifeDeptData;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.DateHelper;


/**
 * TODO DOCUMENT ME!
 * 
 * @author tvicencio		Copyright (c) 2004 Pointwest Technologies Corp.
 * @date Feb 23, 2004
 */
public class CodeValuePairDAO {

	private static final Logger LOGGER = LoggerFactory.getLogger(CodeValuePairDAO.class);

	private Connection conn = null;	


	public CodeValuePairDAO(Connection connection) {
		super();
		this.conn = connection;
	}

	private void closeResources(PreparedStatement ps, ResultSet rs) throws SQLException{
		
		
		try {
			if (ps != null){
				ps.close();
			}
		}
		catch(SQLException e)
		{
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		}
		try {
			if (rs != null){
				rs.close();
			}			
		} catch (SQLException e){
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		}
		
	}
	
	public ArrayList retrieveLOB() throws SQLException {
		
		LOGGER.info("retrieveLOB start");
		String sql = "SELECT LOB_CODE, LOB_DESC " +
					 "FROM LINES_OF_BUSINESS " +
		             "ORDER BY LOB_CODE ";
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList list = new ArrayList();
		try{
			
		ps = conn.prepareStatement(sql);
		rs = ps.executeQuery();

		
		while (rs.next()) {
			LOBData data = new LOBData();
			data.setLOBCode(rs.getString("LOB_CODE"));
			data.setLOBDesc(rs.getString("LOB_DESC"));
			list.add(data);
		}
		}catch (SQLException e){
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		} finally {
			this.closeResources(ps, rs);
		}
		LOGGER.info("retrieveLOB end");
		return (list);	
	}//retrieveLOB


	public LOBData retrieveLOBDetail(String code) throws SQLException {
		
		LOGGER.info("retrieveLOBDetail start");
		String sql = "SELECT LOB_CODE, LOB_DESC " +
					 "FROM LINES_OF_BUSINESS " +
					 "WHERE LOB_CODE = ? " +
					 "ORDER BY LOB_CODE ";
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		LOBData data = null;
		
		try{

		ps = conn.prepareStatement(sql);
		ps.setString(1, code);
		rs = ps.executeQuery();

		if (rs.next()) {
			data = new LOBData();
			data.setLOBCode(rs.getString("LOB_CODE"));
			data.setLOBDesc(rs.getString("LOB_DESC"));
		}
		}catch (SQLException e){
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		} finally {
			this.closeResources(ps, rs);
		}
		
		LOGGER.info("retrieveLOBDetail end");
		return (data);	
	}//retrieveLOBDetail
	

	public void insertLOB(LOBData data) throws SQLException {
		
		LOGGER.info("insertLOB start");
		String sql = "INSERT INTO LINES_OF_BUSINESS (LOB_CODE, LOB_DESC, CREATED_BY, CREATED_DATE) " +
					 "VALUES(?,?,?,?)";

		PreparedStatement ps = null;
		ResultSet rs = null;

		try{

			ps = conn.prepareStatement(sql);
		
			String code = data.getLOBCode(); 
			if (code != null) {
				ps.setString(1, code);
			} else {
				ps.setString(1, null);
			}

			String desc = data.getLOBDesc(); 
			if (desc != null) {
				ps.setString(2, desc);
			} else {
				ps.setString(2, null);
			}
			
			String createdBy = data.getCreatedBy(); 
			if (createdBy != null) {
				ps.setString(3, createdBy);
			} else {
				ps.setString(3, null);
			}

			ps.setTimestamp(4, DateHelper.sqlTimestamp(data.getCreateDate()));
									
			ps.executeUpdate();
		}catch (SQLException e){
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		} finally {
			this.closeResources(ps, rs);
			LOGGER.info("insertLOB end");
		}
	}//insertLOB


	public void updateLOB(LOBData data) throws SQLException{	
		
		LOGGER.info("updateLOB start");
		String sql = "UPDATE LINES_OF_BUSINESS " +
					 "SET LOB_DESC = ?, UPDATED_BY = ?, UPDATED_DATE = ? " +
					 "WHERE LOB_CODE = ?";

		PreparedStatement ps = null;
		ResultSet rs = null;

		try{
		
			ps = conn.prepareStatement(sql);
			ps.setString(1, data.getLOBDesc());				
			ps.setString(2, data.getUpdatedBy());
			ps.setTimestamp(3, DateHelper.sqlTimestamp(data.getUpdateDate()));
			ps.setString(4, data.getLOBCode());										
			ps.executeUpdate();
		}catch (SQLException e){
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		} finally {
			this.closeResources(ps, rs);
			LOGGER.info("updateLOB end");
		}
	}//updateLOB
	

	public ArrayList retrieveDepartments() throws SQLException {
		
		LOGGER.info("retrieveDepartments start");
		String sql = "SELECT DEPT_CODE, DEPT_DESC " +
					 "FROM DEPARTMENTS " +
					 "ORDER BY DEPT_CODE ";
		
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList list = new ArrayList();
		
		try{

		ps = conn.prepareStatement(sql);
		rs = ps.executeQuery();

		while (rs.next()) {
			SunLifeDeptData data = new SunLifeDeptData();
			data.setDeptId(rs.getString("DEPT_CODE"));
			data.setDeptDesc(rs.getString("DEPT_DESC"));
			list.add(data);
		}
		}catch (SQLException e){
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		} finally {
			this.closeResources(ps, rs);
		}
		LOGGER.info("retrieveDepartments end");
		return (list);	
	}//retrieveDepartment


	public SunLifeDeptData retrieveDepartmentDetail(String code) throws SQLException {
		
		LOGGER.info("retrieveDepartmentDetail start");
		String sql = "SELECT DEPT_CODE, DEPT_DESC " +
					 "FROM DEPARTMENTS " +
					 "WHERE DEPT_CODE = ? " +
					 "ORDER BY DEPT_CODE ";
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		SunLifeDeptData data = null;
		
		try{

		ps = conn.prepareStatement(sql);
		ps.setString(1, code);
		rs = ps.executeQuery();

		if (rs.next()) {
			data = new SunLifeDeptData();
			data.setDeptId(rs.getString("DEPT_CODE"));
			data.setDeptDesc(rs.getString("DEPT_DESC"));
		}
		}catch (SQLException e){
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		} finally {
			this.closeResources(ps, rs);
		}
		LOGGER.info("retrieveDepartmentDetail end");
		return (data);	
	}//retrieveDepartmentDetail
	

	public void insertDepartment(SunLifeDeptData data) throws SQLException {
		
		LOGGER.info("insertDepartment start");
		String sql = "INSERT INTO DEPARTMENTS (DEPT_CODE, DEPT_DESC, CREATED_BY, CREATED_DATE) " +
					 "VALUES(?,?,?,?)";

		PreparedStatement ps = null;
		ResultSet rs = null;

		try{

			ps = conn.prepareStatement(sql);
		
			String code = data.getDeptId(); 
			if (code != null) {
				ps.setString(1, code);
			} else {
				ps.setString(1, null);
			}

			String desc = data.getDeptDesc(); 
			if (desc != null) {
				ps.setString(2, desc);
			} else {
				ps.setString(2, null);
			}
			
			String createdBy = data.getCreatedBy(); 
			if (createdBy != null) {
				ps.setString(3, createdBy);
			} else {
				ps.setString(3, null);
			}

			ps.setTimestamp(4, DateHelper.sqlTimestamp(data.getCreateDate()));
									
			ps.executeUpdate();
		}catch (SQLException e){
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		} finally {
			this.closeResources(ps, rs);
			LOGGER.info("insertDepartment end");
		}
	}//insertDepartment


	public void updateDepartment(SunLifeDeptData data) throws SQLException{	
		
		LOGGER.info("updateDepartment start");
		String sql = "UPDATE DEPARTMENTS " +
					 "SET DEPT_DESC = ?, UPDATED_BY = ?, UPDATED_DATE = ? " +
					 "WHERE DEPT_CODE = ?";

		PreparedStatement ps = null;
		ResultSet rs = null;

		try{
			ps = conn.prepareStatement(sql);
			ps.setString(1, data.getDeptDesc());				
			ps.setString(2, data.getUpdatedBy());
			ps.setTimestamp(3, DateHelper.sqlTimestamp(data.getUpdateDate()));
			ps.setString(4, data.getDeptId());										
			ps.executeUpdate();
		}catch (SQLException e){
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		} finally {
			this.closeResources(ps, rs);
		}
		LOGGER.info("updateDepartment end");
	}//updateDepartment
	

	public ArrayList retrieveSections() throws SQLException {
		
		LOGGER.info("retrieveSections start");
		String sql = "SELECT SEC_CODE, SEC_DESC " +
					 "FROM SECTIONS " +
					 "ORDER BY SEC_CODE ";
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList list = new ArrayList();
		
		try{

		ps = conn.prepareStatement(sql);
		rs = ps.executeQuery();

		while (rs.next()) {
			SectionData data = new SectionData();
			data.setSectionId(rs.getString("SEC_CODE"));
			data.setSectionDesc(rs.getString("SEC_DESC"));
			list.add(data);
		}
		}catch (SQLException e){
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		} finally {
			this.closeResources(ps, rs);
		}
		LOGGER.info("retrieveSections end");
		return (list);	
	}//retrieveSection


	public SectionData retrieveSectionDetail(String code) throws SQLException {
		
		LOGGER.info("retrieveSectionDetail start");
		String sql = "SELECT SEC_CODE, SEC_DESC " +
					 "FROM SECTIONS " +
					 "WHERE SEC_CODE = ? " +
					 "ORDER BY SEC_CODE ";
		

		PreparedStatement ps = null;
		ResultSet rs = null;
		SectionData data = null;

		try{
		
		ps = conn.prepareStatement(sql);
		ps.setString(1, code);
		rs = ps.executeQuery();

		if (rs.next()) {
			data = new SectionData();
			data.setSectionId(rs.getString("SEC_CODE"));
			data.setSectionDesc(rs.getString("SEC_DESC"));
		}
		}catch (SQLException e){
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		} finally {
			this.closeResources(ps, rs);
		}
		LOGGER.info("retrieveSectionDetail end");
		return (data);	
	}//retrieveSectionDetail


	public void insertSection(SectionData data) throws SQLException {
		
		LOGGER.info("insertSection start");
		String sql = "INSERT INTO SECTIONS (SEC_CODE, SEC_DESC, CREATED_BY, CREATED_DATE) " +
					 "VALUES(?,?,?,?)";


		PreparedStatement ps = null;
		ResultSet rs = null;

		try{
		
			ps = conn.prepareStatement(sql);
		
			String code = data.getSectionId(); 
			if (code != null) {
				ps.setString(1, code);
			} else {
				ps.setString(1, null);
			}

			String desc = data.getSectionDesc(); 
			if (desc != null) {
				ps.setString(2, desc);
			} else {
				ps.setString(2, null);
			}
			
			String createdBy = data.getCreatedBy(); 
			if (createdBy != null) {
				ps.setString(3, createdBy);
			} else {
				ps.setString(3, null);
			}

			ps.setTimestamp(4, DateHelper.sqlTimestamp(data.getCreateDate()));
									
			ps.executeUpdate();
		}catch (SQLException e){
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		} finally {
			this.closeResources(ps, rs);
			LOGGER.info("insertSection end");
		}
	}//insertSection


	public void updateSection(SectionData data) throws SQLException{
		
		LOGGER.info("updateSection start");
		String sql = "UPDATE SECTIONS " +
					 "SET SEC_DESC = ?, UPDATED_BY = ?, UPDATED_DATE = ? " +
					 "WHERE SEC_CODE = ?";

		PreparedStatement ps = null;
		ResultSet rs = null;

		try{
			ps = conn.prepareStatement(sql);
			ps.setString(1, data.getSectionDesc());				
			ps.setString(2, data.getUpdatedBy());
			ps.setTimestamp(3, DateHelper.sqlTimestamp(data.getUpdateDate()));
			ps.setString(4, data.getSectionId());										
			ps.executeUpdate();
		}catch (SQLException e){
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		} finally {
			this.closeResources(ps, rs);
			LOGGER.info("updateSection end");
		}
	}//updateSection
	

	public ArrayList retrieveExamSpecializations() throws SQLException {
		
		LOGGER.info("retrieveExamSpecializations start");
		String sql = "SELECT SPL_ID, SPL_DESC " +
					 "FROM SPECIALIZATIONS " +
					 "ORDER BY SPL_ID ";
		

		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList list = new ArrayList();
		
		try{
		
		ps = conn.prepareStatement(sql);
		rs = ps.executeQuery();

		while (rs.next()) {
			SpecializationData data = new SpecializationData();
			data.setSpecializationId(rs.getLong("SPL_ID"));
			data.setSpecializationDesc(rs.getString("SPL_DESC"));
			list.add(data);
		}
		}catch (SQLException e){
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		} finally {
			this.closeResources(ps, rs);
		}
		LOGGER.info("retrieveExamSpecializations end");
		return (list);	
	}//retrieveExamSpecialization


	public SpecializationData retrieveExamSpecializationDetail(long code) throws SQLException {
		
		LOGGER.info("retrieveExamSpecializationDetail start");
		String sql = "SELECT SPL_ID, SPL_DESC " +
					 "FROM SPECIALIZATIONS " +
					 "WHERE SPL_ID = ? " +
					 "ORDER BY SPL_ID ";
		PreparedStatement ps = null;
		ResultSet rs = null;
		SpecializationData data = null;
		
		try{
		
			ps = conn.prepareStatement(sql);
		ps.setLong(1, code);
		rs = ps.executeQuery();

		while (rs.next()) {
			data = new SpecializationData();
			data.setSpecializationId(rs.getLong("SPL_ID"));
			data.setSpecializationDesc(rs.getString("SPL_DESC"));
		}
		}catch (SQLException e){
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		} finally {
			this.closeResources(ps, rs);
		}
		LOGGER.info("retrieveExamSpecializationDetail end");
		return (data);
	}//retrieveExamSpecializationDetail


	public void insertExamSpecialization(SpecializationData data) throws SQLException {
		
		LOGGER.info("insertExamSpecialization start");
		String sql = "INSERT INTO SPECIALIZATIONS (SPL_ID, SPL_DESC, CREATED_BY, CREATED_DATE) " +
					 "VALUES(?,?,?,?)";


		PreparedStatement ps = null;
		ResultSet rs = null;

		try{
		
		
			ps = conn.prepareStatement(sql);

			long code = data.getSpecializationId();
			ps.setLong(1, code);
			
			String desc = data.getSpecializationDesc(); 
			if (desc != null) {
				ps.setString(2, desc);
			} else {
				ps.setString(2, null);
			}
			
			String createdBy = data.getCreatedBy(); 
			if (createdBy != null) {
				ps.setString(3, createdBy);
			} else {
				ps.setString(3, null);
			}

			ps.setTimestamp(4, DateHelper.sqlTimestamp(data.getCreateDate()));
									
			ps.executeUpdate();
		}catch (SQLException e){
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		} finally {
			this.closeResources(ps, rs);
			LOGGER.info("insertExamSpecialization end");
		}
		
	}//insertExamSpecialization


	public void updateExamSpecialization(SpecializationData data) throws SQLException{
		
		LOGGER.info("updateExamSpecialization start");
		String sql = "UPDATE SPECIALIZATIONS " +
					 "SET SPL_DESC = ?, UPDATED_BY = ?, UPDATED_DATE = ? " +
					 "WHERE SPL_ID = ?";

		PreparedStatement ps = null;
		ResultSet rs = null;

		try{
		
			ps = conn.prepareStatement(sql);
			ps.setString(1, data.getSpecializationDesc());				
			ps.setString(2, data.getUpdatedBy());
			ps.setTimestamp(3, DateHelper.sqlTimestamp(data.getUpdateDate()));
			ps.setLong(4, data.getSpecializationId());										
			ps.executeUpdate();
		}catch (SQLException e){
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		} finally {
			this.closeResources(ps, rs);
			LOGGER.info("updateExamSpecialization end");
		}
	}//updateExamSpecialization
	

	public ArrayList retrieveExamPlaces() throws SQLException {
		
		LOGGER.info("retrieveExamPlaces start");
		String sql = "SELECT EP_ID, EP_DESC " +
					 "FROM EXAMINATION_PLACES " +
					 "ORDER BY EP_ID ";
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList list = new ArrayList();
		try{

		ps = conn.prepareStatement(sql);
		rs = ps.executeQuery();

		
		while (rs.next()) {
			ExaminationPlaceData data = new ExaminationPlaceData();
			data.setExaminationPlaceId(rs.getLong("EP_ID"));
			data.setExaminationPlaceDesc(rs.getString("EP_DESC"));
			list.add(data);
		}
		}catch (SQLException e){
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		} finally {
			this.closeResources(ps, rs);
		} 

		LOGGER.info("retrieveExamPlaces end");
		return (list);	
	}//retrieveExamPlace


	public ExaminationPlaceData retrieveExamPlaceDetail(long code) throws SQLException {
		
		LOGGER.info("retrieveExamPlaceDetail start");
		String sql = "SELECT EP_ID, EP_DESC " +
					 "FROM EXAMINATION_PLACES " +
					 "WHERE EP_ID = ? " +
					 "ORDER BY EP_ID ";
		PreparedStatement ps = null;
		ResultSet rs = null;
		ExaminationPlaceData data = null;
		
		try{

		ps = conn.prepareStatement(sql);
		ps.setLong(1, code);
		rs = ps.executeQuery();

		while (rs.next()) {
			data = new ExaminationPlaceData();
			data.setExaminationPlaceId(rs.getLong("EP_ID"));
			data.setExaminationPlaceDesc(rs.getString("EP_DESC"));
		}
		}catch (SQLException e){
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		} finally {
			this.closeResources(ps, rs);
		} 
		LOGGER.info("retrieveExamPlaceDetail end");
		return (data);	
	}//retrieveExamPlaceDetail


	public void insertExamPlace(ExaminationPlaceData data) throws SQLException {
		
		LOGGER.info("insertExamPlace start");
		String sql = "INSERT INTO EXAMINATION_PLACES (EP_ID, EP_DESC, CREATED_BY, CREATED_DATE) " +
					 "VALUES(?,?,?,?)";

		PreparedStatement ps = null;
		ResultSet rs = null;

		try{

			ps = conn.prepareStatement(sql);

			long code = data.getExaminationPlaceId();
			ps.setLong(1, code);

			String desc = data.getExaminationPlaceDesc(); 
			if (desc != null) {
				ps.setString(2, desc);
			} else {
				ps.setString(2, null);
			}
			
			String createdBy = data.getCreatedBy(); 
			if (createdBy != null) {
				ps.setString(3, createdBy);
			} else {
				ps.setString(3, null);
			}

			ps.setTimestamp(4, DateHelper.sqlTimestamp(data.getCreateDate()));
									
			ps.executeUpdate();
		}catch (SQLException e){
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		} finally {
			this.closeResources(ps, rs);
			LOGGER.info("insertExamPlace end");
		}
	}//insertExamPlace


	public void updateExamPlace(ExaminationPlaceData data) throws SQLException{
		
		LOGGER.info("updateExamPlace start");
		String sql = "UPDATE EXAMINATION_PLACES " +
					 "SET EP_DESC = ?, UPDATED_BY = ?, UPDATED_DATE = ? " +
					 "WHERE EP_ID = ?";
		
		PreparedStatement ps = null;
		ResultSet rs = null;

		try{

			ps = conn.prepareStatement(sql);
			ps.setString(1, data.getExaminationPlaceDesc());				
			ps.setString(2, data.getUpdatedBy());
			ps.setTimestamp(3, DateHelper.sqlTimestamp(data.getUpdateDate()));
			ps.setLong(4, data.getExaminationPlaceId());										
			ps.executeUpdate();
		}catch (SQLException e){
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		} finally {
			this.closeResources(ps, rs);
			LOGGER.info("updateExamPlace end");
		}
	}//updateExamPlace
	

	public ArrayList retrieveExamAreas() throws SQLException {
		
		LOGGER.info("retrieveExamAreas start");
		String sql = "SELECT EA_ID, EA_DESC " +
					 "FROM EXAMINATION_AREAS " +
					 "ORDER BY EA_ID ";
		
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList list = new ArrayList();
		
		try{

		ps = conn.prepareStatement(sql);
		rs = ps.executeQuery();

		while (rs.next()) {
			ExaminationAreaData data = new ExaminationAreaData();
			data.setExaminationAreaId(rs.getLong("EA_ID"));
			data.setExaminationAreaDesc(rs.getString("EA_DESC"));
			list.add(data);
		}
		}catch (SQLException e){
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		} finally {
			this.closeResources(ps, rs);
		} 
		LOGGER.info("retrieveExamAreas end");
		return (list);	
	}//retrieveExamArea	


	public ExaminationAreaData retrieveExamAreaDetail(long code) throws SQLException {
		
		LOGGER.info("retrieveExamAreaDetail start");
		String sql = "SELECT EA_ID, EA_DESC " +
					 "FROM EXAMINATION_AREAS " +
					 "WHERE EA_ID = ? " +
					 "ORDER BY EA_ID ";
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		ExaminationAreaData data = null;
		
		try{

		ps = conn.prepareStatement(sql);
		ps.setLong(1, code);
		rs = ps.executeQuery();

		while (rs.next()) {
			data = new ExaminationAreaData();
			data.setExaminationAreaId(rs.getLong("EA_ID"));
			data.setExaminationAreaDesc(rs.getString("EA_DESC"));
		}
		}catch (SQLException e){
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		} finally {
			this.closeResources(ps, rs);
			
		}
		LOGGER.info("retrieveExamAreaDetail end");
		return (data);	
	}//retrieveExamAreaDetail	
		

	public void insertExamArea(ExaminationAreaData data) throws SQLException {
		
		LOGGER.info("insertExamArea start");
		String sql = "INSERT INTO EXAMINATION_AREAS (EA_ID, EA_DESC, CREATED_BY, CREATED_DATE) " +
					 "VALUES(?,?,?,?)";

		PreparedStatement ps = null;
		ResultSet rs = null;

		try{

			ps = conn.prepareStatement(sql);

			long code = data.getExaminationAreaId();
			ps.setLong(1, code);

			String desc = data.getExaminationAreaDesc(); 
			if (desc != null) {
				ps.setString(2, desc);
			} else {
				ps.setString(2, null);
			}
			
			String createdBy = data.getCreatedBy(); 
			if (createdBy != null) {
				ps.setString(3, createdBy);
			} else {
				ps.setString(3, null);
			}

			ps.setTimestamp(4, DateHelper.sqlTimestamp(data.getCreateDate()));
									
			ps.executeUpdate();
		}catch (SQLException e){
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		} finally {
			this.closeResources(ps, rs);
			LOGGER.info("insertExamArea end");
		}
	}//insertExamArea


	public void updateExamArea(ExaminationAreaData data) throws SQLException{
		
		LOGGER.info("updateExamArea start");
		String sql = "UPDATE EXAMINATION_AREAS " +
					 "SET EA_DESC = ?, UPDATED_BY = ?, UPDATED_DATE = ? " +
					 "WHERE EA_ID = ?";
		PreparedStatement ps = null;
		ResultSet rs = null;

		try{

			ps = conn.prepareStatement(sql);
			ps.setString(1, data.getExaminationAreaDesc());				
			ps.setString(2, data.getUpdatedBy());
			ps.setTimestamp(3, DateHelper.sqlTimestamp(data.getUpdateDate()));
			ps.setLong(4, data.getExaminationAreaId());										
			ps.executeUpdate();
		}catch (SQLException e){
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		} finally {
			this.closeResources(ps, rs);
			LOGGER.info("updateExamArea end");
		}
	}//updateExamArea
	

	public ArrayList retrieveDocumentTypes() throws SQLException {
		
		LOGGER.info("retrieveDocumentTypes start");
		String sql = "SELECT DOC_CODE, DOC_DESC " +
					 "FROM DOCUMENT_TYPES " +
					 "ORDER BY DOC_CODE ";
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList list = new ArrayList();
		
		try{

		ps = conn.prepareStatement(sql);
		rs = ps.executeQuery();

		while (rs.next()) {
			DocumentTypeData data = new DocumentTypeData();
			data.setDocCode(rs.getString("DOC_CODE"));
			data.setDocDesc(rs.getString("DOC_DESC"));
			list.add(data);
		}
		}catch (SQLException e){
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		} finally {
			this.closeResources(ps, rs);
		}
		LOGGER.info("retrieveDocumentTypes end");
		return (list);	
	}//retrieveDocumentType	


	public DocumentTypeData retrieveDocumentTypeDetail(String code) throws SQLException {
		
		LOGGER.info("retrieveDocumentTypeDetail start");
		String sql = "SELECT DOC_CODE, DOC_DESC " +
					 "FROM DOCUMENT_TYPES " +
					 "WHERE DOC_CODE = ?" +
					 "ORDER BY DOC_CODE ";
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		DocumentTypeData data = null;
		
		try{

		ps = conn.prepareStatement(sql);
		ps.setString(1, code);
		rs = ps.executeQuery();

		while (rs.next()) {
			data = new DocumentTypeData();
			data.setDocCode(rs.getString("DOC_CODE"));
			data.setDocDesc(rs.getString("DOC_DESC"));
		}
		}catch (SQLException e){
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		} finally {
			this.closeResources(ps, rs);
		}
		LOGGER.info("retrieveDocumentTypeDetail end");
		return (data);	
	}//retrieveDocumentTypeDetail	
	

	public void insertDocumentType(DocumentTypeData data) throws SQLException {
		
		LOGGER.info("insertDocumentType start");
		String sql = "INSERT INTO DOCUMENT_TYPES (DOC_CODE, DOC_DESC, CREATED_BY, CREATED_DATE) " +
					 "VALUES(?,?,?,?)";

		PreparedStatement ps = null;
		ResultSet rs = null;

		try{

			ps = conn.prepareStatement(sql);

			String code = data.getDocCode(); 
			if (code != null) {
				ps.setString(1, code);
			} else {
				ps.setString(1, null);
			}

			String desc = data.getDocDesc(); 
			if (desc != null) {
				ps.setString(2, desc);
			} else {
				ps.setString(2, null);
			}
			
			String createdBy = data.getCreatedBy(); 
			if (createdBy != null) {
				ps.setString(3, createdBy);
			} else {
				ps.setString(3, null);
			}

			ps.setTimestamp(4, DateHelper.sqlTimestamp(data.getCreateDate()));
			
			ps.executeUpdate();
			
		}catch (SQLException e){
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		} finally {
			this.closeResources(ps, rs);
			LOGGER.info("insertDocumentType end");
		}
	}//insertDocumentType


	public void updateDocumentType(DocumentTypeData data) throws SQLException{
		
		LOGGER.info("updateDocumentType start");
		String sql = "UPDATE DOCUMENT_TYPES " +
					 "SET DOC_DESC = ?, UPDATED_BY = ?, UPDATED_DATE = ? " +
					 "WHERE DOC_CODE = ?";
		PreparedStatement ps = null;
		ResultSet rs = null;

		try{

			ps = conn.prepareStatement(sql);
			ps.setString(1, data.getDocDesc());				
			ps.setString(2, data.getUpdatedBy());
			ps.setTimestamp(3, DateHelper.sqlTimestamp(data.getUpdateDate()));
			ps.setString(4, data.getDocCode());										
			ps.executeUpdate();
		}catch (SQLException e){
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		} finally {
			this.closeResources(ps, rs);
			LOGGER.info("updateDocumentType end");
		}
	}//updateDocumentType
			

	public ArrayList retrieveClientTypes() throws SQLException {
		
		LOGGER.info("retrieveClientTypes start");
		String sql = "SELECT CLT_CODE, CLT_DESC " +
					 "FROM CLIENT_TYPES " +
					 "ORDER BY CLT_CODE ";
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList list = new ArrayList();
		
		try{

		ps = conn.prepareStatement(sql);
		rs = ps.executeQuery();

		while (rs.next()) {
			ClientTypeData data = new ClientTypeData();
			data.setClientTypeCode(rs.getString("CLT_CODE"));
			data.setClientTypeDesc(rs.getString("CLT_DESC"));
			list.add(data);
		}
		}catch (SQLException e){
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		} finally {
			this.closeResources(ps, rs);
		}
		LOGGER.info("retrieveClientTypes end");
		return (list);	
	}//retrieveClientType


	public ClientTypeData retrieveClientTypeDetail(String code) throws SQLException {
		
		LOGGER.info("retrieveClientTypeDetail start");
		String sql = "SELECT CLT_CODE, CLT_DESC " +
					 "FROM CLIENT_TYPES " +
					 "WHERE CLT_CODE = ? " +
					 "ORDER BY CLT_CODE ";
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		ClientTypeData data = null;
		
		try{

		ps = conn.prepareStatement(sql);
		ps.setString(1, code);
		rs = ps.executeQuery();

		while (rs.next()) {
			data = new ClientTypeData();
			data.setClientTypeCode(rs.getString("CLT_CODE"));
			data.setClientTypeDesc(rs.getString("CLT_DESC"));
		}
		}catch (SQLException e){
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		} finally {
			this.closeResources(ps, rs);
		}
		LOGGER.info("retrieveClientTypeDetail end");
		return (data);	
	}//retrieveClientTypeDetail


	public void insertClientType(ClientTypeData data) throws SQLException {
		
		LOGGER.info("insertClientType start");
		String sql = "INSERT INTO CLIENT_TYPES (CLT_CODE, CLT_DESC, CREATED_BY, CREATED_DATE) " +
					 "VALUES(?,?,?,?)";

		PreparedStatement ps = null;
		ResultSet rs = null;

		try{

			ps = conn.prepareStatement(sql);

			String code = data.getClientTypeCode(); 
			if (code != null) {
				ps.setString(1, code);
			} else {
				ps.setString(1, null);
			}

			String desc = data.getClientTypeDesc(); 
			if (desc != null) {
				ps.setString(2, desc);
			} else {
				ps.setString(2, null);
			}
			
			String createdBy = data.getCreatedBy(); 
			if (createdBy != null) {
				ps.setString(3, createdBy);
			} else {
				ps.setString(3, null);
			}

			ps.setTimestamp(4, DateHelper.sqlTimestamp(data.getCreateDate()));
									
			ps.executeUpdate();
		}catch (SQLException e){
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		} finally {
			this.closeResources(ps, rs);
			LOGGER.info("insertClientType end");
		}
	}//insertClientType


	public void updateClientType(ClientTypeData data) throws SQLException{
		
		LOGGER.info("updateClientType start");
		String sql = "UPDATE CLIENT_TYPES " +
					 "SET CLT_DESC = ?, UPDATED_BY = ?, UPDATED_DATE = ? " +
					 "WHERE CLT_CODE = ?";
		PreparedStatement ps = null;
		ResultSet rs = null;

		try{

			ps = conn.prepareStatement(sql);
			ps.setString(1, data.getClientTypeDesc());				
			ps.setString(2, data.getUpdatedBy());
			ps.setTimestamp(3, DateHelper.sqlTimestamp(data.getUpdateDate()));
			ps.setString(4, data.getClientTypeCode());										
			ps.executeUpdate();
		}catch (SQLException e){
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		} finally {
			this.closeResources(ps, rs);
			LOGGER.info("updateClientType end");
		}
	}//updateClientType
		

	public ArrayList retrieveAutoAssignCriteria() throws SQLException {
		
		LOGGER.info("retrieveAutoAssignCriteria start");
		String sql = "SELECT AAC_ID, AAC_FIELD, AAC_DESC, AAC_STATUS " +
					 "FROM AUTO_ASSIGNMENT_CRITERIA " +
					 "ORDER BY AAC_ID ";
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList list = new ArrayList();
		
		try{

		ps = conn.prepareStatement(sql);
		rs = ps.executeQuery();

		while (rs.next()) {
			AutoAssignCriteriaData data = new AutoAssignCriteriaData();
			data.setCriteriaId(rs.getLong("AAC_ID"));
			data.setFieldCode(rs.getString("AAC_FIELD"));
			data.setFieldDesc(rs.getString("AAC_DESC"));
			data.setNotificationSent(rs.getBoolean("AAC_STATUS"));
			list.add(data);
		}
		}catch (SQLException e){
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		} finally {
			this.closeResources(ps, rs);
		}
		LOGGER.info("retrieveAutoAssignCriteria end");
		return (list);	
	}//retrieveAutoAssignCriteria


	public AutoAssignCriteriaData retrieveAutoAssignCriteriaDetail(String code) throws SQLException {
		
		LOGGER.info("retrieveAutoAssignCriteriaDetail start");
		String sql = "SELECT AAC_ID, AAC_FIELD, AAC_DESC, AAC_STATUS " +
					 "FROM AUTO_ASSIGNMENT_CRITERIA " +
					 "WHERE AAC_FIELD = ? " +
					 "ORDER BY AAC_ID ";
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		AutoAssignCriteriaData data = null;
		
		try{

		ps = conn.prepareStatement(sql);
		ps.setString(1, code);
		rs = ps.executeQuery();

		while (rs.next()) {
			data = new AutoAssignCriteriaData();
			data.setCriteriaId(rs.getLong("AAC_ID"));
			data.setFieldCode(rs.getString("AAC_FIELD"));
			data.setFieldDesc(rs.getString("AAC_DESC"));
			data.setNotificationSent(rs.getBoolean("AAC_STATUS"));
		}
		}catch (SQLException e){
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		} finally {
			this.closeResources(ps, rs);
		}
		LOGGER.info("retrieveAutoAssignCriteriaDetail end");
		return (data);	
	}//retrieveAutoAssignCriteriaDetail
	

	public void insertAutoAssignCriteria(AutoAssignCriteriaData data) throws SQLException {
		
		LOGGER.info("insertAutoAssignCriteria start");
		String sql = "INSERT INTO AUTO_ASSIGNMENT_CRITERIA (AAC_ID, AAC_FIELD, AAC_DESC, AAC_STATUS, CREATED_BY, CREATED_DATE) " +
					 "VALUES(SEQ_AUTO_ASSIGNMENT_CRITERIA.NEXTVAL,?,?,?,?,?)";

		PreparedStatement ps = null;
		ResultSet rs = null;

		try{

			ps = conn.prepareStatement(sql);

			String code = data.getFieldCode(); 
			if (code != null) {
				ps.setString(1, code);
			} else {
				ps.setString(1, null);
			}

			String desc = data.getFieldDesc(); 
			if (desc != null) {
				ps.setString(2, desc);
			} else {
				ps.setString(2, null);
			}

			boolean status = data.getNotificationSent(); 
			ps.setBoolean(3, status);			
			
			String createdBy = data.getCreatedBy(); 
			if (createdBy != null) {
				ps.setString(4, createdBy);
			} else {
				ps.setString(4, null);
			}

			ps.setTimestamp(5, DateHelper.sqlTimestamp(data.getCreateDate()));
									
			ps.executeUpdate();
		}catch (SQLException e){
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		} finally {
			this.closeResources(ps, rs);
			LOGGER.info("insertAutoAssignCriteria end");
		}
	}//insertAutoAssignCriteria


	public void updateAutoAssignCriteria(AutoAssignCriteriaData data) throws SQLException{
		
		LOGGER.info("updateAutoAssignCriteria start");
		String sql = "UPDATE AUTO_ASSIGNMENT_CRITERIA " +
					 "SET AAC_DESC = ?, AAC_STATUS = ?, UPDATED_BY = ?, UPDATED_DATE = ? " +
					 "WHERE AAC_FIELD = ?";
		PreparedStatement ps = null;
		ResultSet rs = null;

		try{

			ps = conn.prepareStatement(sql);
			ps.setString(1, data.getFieldDesc());
			ps.setBoolean(2, data.getNotificationSent());
			ps.setString(3, data.getUpdatedBy());
			ps.setTimestamp(4, DateHelper.sqlTimestamp(data.getUpdateDate()));
			ps.setString(5, data.getFieldCode());										
			ps.executeUpdate();
		}catch (SQLException e){
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		} finally {
			this.closeResources(ps, rs);
			LOGGER.info("updateAutoAssignCriteria end");
		}
	}//updateAutoAssignCriteria
	

	public ArrayList retrieveStatusCodes() throws SQLException {
		
		LOGGER.info("retrieveStatusCodes start");
		String sql = "SELECT STAT_ID, STAT_DESC, STAT_TYPE " +
					 "FROM STATUS " +
					 "ORDER BY STAT_ID ";
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList list = new ArrayList();
		
		try{

		ps = conn.prepareStatement(sql);
		rs = ps.executeQuery();

		while (rs.next()) {
			StatusData data = new StatusData();
			data.setStatusId(rs.getLong("STAT_ID"));
			data.setStatusDesc(rs.getString("STAT_DESC"));
			data.setStatusType(rs.getString("STAT_TYPE"));
			list.add(data);
		}
		}catch (SQLException e){
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		} finally {
			this.closeResources(ps, rs);
		}
		LOGGER.info("retrieveStatusCodes end");
		return (list);	
	}//retrieveStatusCodes


	public StatusData retrieveStatusCodeDetail(long code) throws SQLException {
		
		LOGGER.info("retrieveStatusCodeDetail start");
		String sql = "SELECT STAT_ID, STAT_DESC, STAT_TYPE " +
					 "FROM STATUS " +
					 "WHERE STAT_ID = ? " +
					 "ORDER BY STAT_ID ";
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		StatusData data = null;
		
		try{

		ps = conn.prepareStatement(sql);
		ps.setLong(1, code);
		rs = ps.executeQuery();

		while (rs.next()) {
			data = new StatusData();
			data.setStatusId(rs.getLong("STAT_ID"));
			data.setStatusDesc(rs.getString("STAT_DESC"));
			data.setStatusType(rs.getString("STAT_TYPE"));
		}
		}catch (SQLException e){
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		} finally {
			this.closeResources(ps, rs);
		}
		LOGGER.info("retrieveStatusCodeDetail end");
		return (data);	
	}//retrieveStatusCodeDetail
	

	public void insertStatusCode(StatusData data) throws SQLException {
		
		LOGGER.info("insertStatusCode start");
		String sql = "INSERT INTO STATUS (STAT_ID, STAT_DESC, STAT_TYPE, CREATED_BY, CREATED_DATE) " +
					 "VALUES(?,?,?,?,?)";

		PreparedStatement ps = null;
		ResultSet rs = null;

		try{

			ps = conn.prepareStatement(sql);

			long code = data.getStatusId(); 
			ps.setLong(1, code);

			String desc = data.getStatusDesc(); 
			if (desc != null) {
				ps.setString(2, desc);
			} else {
				ps.setString(2, null);
			}

			String type = data.getStatusType(); 
			if (type != null) {
				ps.setString(3, type);
			} else {
				ps.setString(3, null);
			}
			
			String createdBy = data.getCreatedBy(); 
			if (createdBy != null) {
				ps.setString(4, createdBy);
			} else {
				ps.setString(4, null);
			}

			ps.setTimestamp(5, DateHelper.sqlTimestamp(data.getCreateDate()));
									
			ps.executeUpdate();
		}catch (SQLException e){
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		} finally {
			this.closeResources(ps, rs);
			LOGGER.info("insertStatusCode end");
		}
	}//insertStatusCode


	public void updateStatusCode(StatusData data) throws SQLException{
		
		LOGGER.info("updateStatusCode start");
		String sql = "UPDATE STATUS " +
					 "SET STAT_DESC = ?, STAT_TYPE = ?, UPDATED_BY = ?, UPDATED_DATE = ? " +
					 "WHERE STAT_ID = ?";
		PreparedStatement ps = null;
		ResultSet rs = null;

		try{

			ps = conn.prepareStatement(sql);
			ps.setString(1, data.getStatusDesc());
			ps.setString(2, data.getStatusType());
			ps.setString(3, data.getUpdatedBy());
			ps.setTimestamp(4, DateHelper.sqlTimestamp(data.getUpdateDate()));
			ps.setLong(5, data.getStatusId());										
			ps.executeUpdate();
		}catch (SQLException e){
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		} finally {
			this.closeResources(ps, rs);
			LOGGER.info("updateStatusCode end");
		}
	}//updateStatusCode
	

	public ArrayList retrieveRoles() throws SQLException {
		
		LOGGER.info("retrieveRoles start");
		String sql = "SELECT ROLE_CODE, ROLE_DESC " +
					 "FROM ROLES " +
					 "ORDER BY ROLE_CODE ";
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList list = new ArrayList();
		
		try{

		ps = conn.prepareStatement(sql);
		rs = ps.executeQuery();

		while (rs.next()) {
			RolesData data = new RolesData();
			data.setRolesId(rs.getString("ROLE_CODE"));
			data.setRolesDesc(rs.getString("ROLE_DESC"));
			list.add(data);
		}
		}catch (SQLException e){
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		} finally {
			this.closeResources(ps, rs);
		}
		LOGGER.info("retrieveRoles end");
		return (list);	
	}//retrieveRoles


	public RolesData retrieveRoleDetail(String code) throws SQLException {
		
		LOGGER.info("retrieveRoleDetail start");
		String sql = "SELECT ROLE_CODE, ROLE_DESC " +
					 "FROM ROLES " +
					 "WHERE ROLE_CODE = ? " +
					 "ORDER BY ROLE_CODE ";
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		RolesData data = null;
		
		try{

		ps = conn.prepareStatement(sql);
		ps.setString(1, code);
		rs = ps.executeQuery();

		while (rs.next()) {
			data = new RolesData();
			data.setRolesId(rs.getString("ROLE_CODE"));
			data.setRolesDesc(rs.getString("ROLE_DESC"));
		}
		}catch (SQLException e){
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		} finally {
			this.closeResources(ps, rs);
		}
		LOGGER.info("retrieveRoleDetail end");
		return (data);
	}//retrieveRoleDetail


	public void insertRole(RolesData data) throws SQLException {
		
		LOGGER.info("insertRole start");
		String sql = "INSERT INTO ROLES (ROLE_CODE, ROLE_DESC, CREATED_BY, CREATED_DATE) " +
					 "VALUES(?,?,?,?)";

		PreparedStatement ps = null;
		ResultSet rs = null;

		try{

			ps = conn.prepareStatement(sql);

			String code = data.getRolesId(); 
			if (code != null) {
				ps.setString(1, code);
			} else {
				ps.setString(1, null);
			}

			String desc = data.getRolesDesc(); 
			if (desc != null) {
				ps.setString(2, desc);
			} else {
				ps.setString(2, null);
			}
			
			String createdBy = data.getCreatedBy(); 
			if (createdBy != null) {
				ps.setString(3, createdBy);
			} else {
				ps.setString(3, null);
			}

			ps.setTimestamp(4, DateHelper.sqlTimestamp(data.getCreateDate()));
									
			ps.executeUpdate();
		}catch (SQLException e){
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		} finally {
			this.closeResources(ps, rs);
			LOGGER.info("insertRole end");
		}
	}//insertRole


	public void updateRole(RolesData data) throws SQLException{
		
		LOGGER.info("updateRole start");
		String sql = "UPDATE ROLES " +
					 "SET ROLE_DESC = ?, UPDATED_BY = ?, UPDATED_DATE = ? " +
					 "WHERE ROLE_CODE = ?";
		PreparedStatement ps = null;
		ResultSet rs = null;

		try{
			ps = conn.prepareStatement(sql);
			ps.setString(1, data.getRolesDesc());
			ps.setString(2, data.getUpdatedBy());
			ps.setTimestamp(3, DateHelper.sqlTimestamp(data.getUpdateDate()));
			ps.setString(4, data.getRolesId());										
			ps.executeUpdate();
		}catch (SQLException e){
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		} finally {
			this.closeResources(ps, rs);
			LOGGER.info("updateRole end");
		}
	}//updateRole


	public ArrayList retrieveAccessTemplates() throws SQLException {
		
		LOGGER.info("retrieveAccessTemplates start");
		String sql = "SELECT TEMPLT_ID, TEMPLT_DESC, ROLE_ID " +
					 "FROM ACCESS_TEMPLATES " +
					 "ORDER BY TEMPLT_ID ";
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList list = new ArrayList();
		
		try{

		ps = conn.prepareStatement(sql);
		rs = ps.executeQuery();

		while (rs.next()) {
			AccessTemplateData data = new AccessTemplateData();
			data.setTemplateID(rs.getLong("TEMPLT_ID"));
			data.setTemplateDesc(rs.getString("TEMPLT_DESC"));
			RolesData role = new RolesData();
			role.setRolesId(rs.getString("ROLE_ID"));
			data.setRole(role);			
			list.add(data);
		}
		}catch (SQLException e){
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		} finally {
			this.closeResources(ps, rs);
		}
		LOGGER.info("retrieveAccessTemplates end");
		return (list);	
	}//retrieveAccessTemplates


	public AccessTemplateData retrieveAccessTemplate(long code) throws SQLException {
		
		LOGGER.info("retrieveAccessTemplate start");
		String sql = "SELECT TEMPLT_ID, TEMPLT_DESC, ROLE_ID " +
					 "FROM ACCESS_TEMPLATES " +
					 "WHERE TEMPLT_ID = ? " +
					 "ORDER BY TEMPLT_ID ";
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		AccessTemplateData data = null;
		
		try{

		ps = conn.prepareStatement(sql);
		ps.setLong(1, code);
		rs = ps.executeQuery();

		while (rs.next()) {
			data = new AccessTemplateData();
			data.setTemplateID(rs.getLong("TEMPLT_ID"));
			data.setTemplateDesc(rs.getString("TEMPLT_DESC"));
			RolesData role = new RolesData();
			role.setRolesId(rs.getString("ROLE_ID"));
			data.setRole(role);
		}
		}catch (SQLException e){
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		} finally {
			this.closeResources(ps, rs);
		}
		LOGGER.info("retrieveAccessTemplate end");
		return (data);
	}//retrieveAccessTemplate


	public void updateAccessTemplate(AccessTemplateData data) throws SQLException{
		
		LOGGER.info("updateAccessTemplate start");
		String sql = "UPDATE ACCESS_TEMPLATES " +
					 "SET TEMPLT_DESC = ?, UPDATED_BY = ?, UPDATED_DATE = ? " +
					 "WHERE TEMPLT_ID = ?";
		PreparedStatement ps = null;
		ResultSet rs = null;

		try{

			ps = conn.prepareStatement(sql);
			ps.setString(1, data.getTemplateDesc());
			ps.setString(2, data.getUpdatedBy());
			ps.setTimestamp(3, data.getUpdatedDate());
			ps.setLong(4, data.getTemplateID());
			ps.executeUpdate();
		}catch (SQLException e){
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		} finally {
			this.closeResources(ps, rs);
			LOGGER.info("updateAccessTemplate end");
		}
	}//updateAccessTemplate	


	public ArrayList retrieveMIBImpairmentCodes() throws SQLException {
		
		LOGGER.info("retrieveMIBImpairmentCodes start");
		String sql = "SELECT MIB_IMPAIRMENT_CODE, MIB_IMPAIRMENT_DESC " +
					 "FROM MIB_IMPAIRMENTS " +
					 "ORDER BY MIB_IMPAIRMENT_CODE ";
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList list = new ArrayList();
		
		try{

		ps = conn.prepareStatement(sql);
		rs = ps.executeQuery();

		while (rs.next()) {
			MIBImpairmentData data = new MIBImpairmentData();
			data.setMIBImpairmentCode(rs.getString("MIB_IMPAIRMENT_CODE"));
			data.setMIBImpairmentDesc(rs.getString("MIB_IMPAIRMENT_DESC"));
			list.add(data);
		}
		}catch (SQLException e){
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		} finally {
			this.closeResources(ps, rs);
		}
		LOGGER.info("retrieveMIBImpairmentCodes end");
		return (list);	
	}//retrieveMIBImpairmentCodes


	public MIBImpairmentData retrieveMIBImpairmentCodeDetail(String code) throws SQLException {
		
		LOGGER.info("retrieveMIBImpairmentCodeDetail start");
		String sql = "SELECT MIB_IMPAIRMENT_CODE, MIB_IMPAIRMENT_DESC " +
					 "FROM MIB_IMPAIRMENTS " +
					 "WHERE MIB_IMPAIRMENT_CODE = ? " +
					 "ORDER BY MIB_IMPAIRMENT_CODE ";					 
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		MIBImpairmentData data = null;
		
		try{

		ps = conn.prepareStatement(sql);
		ps.setString(1, code);
		rs = ps.executeQuery();

		if (rs.next()) {
			data = new MIBImpairmentData();
			data.setMIBImpairmentCode(rs.getString("MIB_IMPAIRMENT_CODE"));
			data.setMIBImpairmentDesc(rs.getString("MIB_IMPAIRMENT_DESC"));
		}
		}catch (SQLException e){
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		} finally {
			this.closeResources(ps, rs);
		}
		LOGGER.info("retrieveMIBImpairmentCodeDetail end");
		return (data);	
	}//retrieveMIBImpairmentCodeDetail
	

	public void insertMIBImpairmentCode(MIBImpairmentData data) throws SQLException {
		
		LOGGER.info("insertMIBImpairmentCode start");
		String sql = "INSERT INTO MIB_IMPAIRMENTS (MIB_IMPAIRMENT_CODE, MIB_IMPAIRMENT_DESC, CREATED_BY, CREATED_DATE) " +
					 "VALUES(?,?,?,?)";

		PreparedStatement ps = null;
		ResultSet rs = null;

		try{

			ps = conn.prepareStatement(sql);
		
			String code = data.getMIBImpairmentCode(); 
			if (code != null) {
				ps.setString(1, code);
			} else {
				ps.setString(1, null);
			}

			String desc = data.getMIBImpairmentDesc();
			if (desc != null) {
				ps.setString(2, desc);
			} else {
				ps.setString(2, null);
			}
			
			String createdBy = data.getCreatedBy(); 
			if (createdBy != null) {
				ps.setString(3, createdBy);
			} else {
				ps.setString(3, null);
			}

			ps.setTimestamp(4, DateHelper.sqlTimestamp(data.getCreateDate()));
									
			ps.executeUpdate();
		}catch (SQLException e){
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		} finally {
			this.closeResources(ps, rs);
			LOGGER.info("insertMIBImpairmentCode end");
		}
	}//insertMIBImpairmentCode


	public void updateMIBImpairmentCode(MIBImpairmentData data) throws SQLException{
		
		LOGGER.info("updateMIBImpairmentCode start");
		String sql = "UPDATE MIB_IMPAIRMENTS " +
					 "SET MIB_IMPAIRMENT_DESC = ?, UPDATED_BY = ?, UPDATED_DATE = ? " +
					 "WHERE MIB_IMPAIRMENT_CODE = ?";
		PreparedStatement ps = null;
		ResultSet rs = null;

		try{

			ps = conn.prepareStatement(sql);
			ps.setString(1, data.getMIBImpairmentDesc());				
			ps.setString(2, data.getUpdatedBy());
			ps.setTimestamp(3, DateHelper.sqlTimestamp(data.getUpdateDate()));
			ps.setString(4, data.getMIBImpairmentCode());										
			ps.executeUpdate();
		}catch (SQLException e){
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		} finally {
			this.closeResources(ps, rs);
			LOGGER.info("updateMIBImpairmentCode end");
		}
	}//updateMIBImpairmentCode	


	public ArrayList retrieveMIBNumberCodes() throws SQLException {
		
		LOGGER.info("retrieveMIBNumberCodes start");
		String sql = "SELECT MIB_NUM_CODE, MIB_NUM_DESC " +
					 "FROM MIB_NUMBERS " +
					 "ORDER BY MIB_NUM_CODE ";
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList list = new ArrayList();
		
		try{

		ps = conn.prepareStatement(sql);
		rs = ps.executeQuery();

		while (rs.next()) {
			MIBNumberData data = new MIBNumberData();
			data.setMIBNumberCode(rs.getString("MIB_NUM_CODE"));
			data.setMIBNumberDesc(rs.getString("MIB_NUM_DESC"));
			list.add(data);
		}
		}catch (SQLException e){
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		} finally {
			this.closeResources(ps, rs);
		}
		LOGGER.info("retrieveMIBNumberCodes end");
		return (list);	
	}//retrieveMIBNumberCodes


	public MIBNumberData retrieveMIBNumberCodeDetail(String code) throws SQLException {
		
		LOGGER.info("retrieveMIBNumberCodeDetail start");
		String sql = "SELECT MIB_NUM_CODE, MIB_NUM_DESC " +
					 "FROM MIB_NUMBERS " +
					 "WHERE MIB_NUM_CODE = ? " +
					 "ORDER BY MIB_NUM_CODE ";					 
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		MIBNumberData data = null;
		
		try{

		ps = conn.prepareStatement(sql);
		ps.setString(1, code);
		rs = ps.executeQuery();

		if (rs.next()) {
			data = new MIBNumberData();
			data.setMIBNumberCode(rs.getString("MIB_NUM_CODE"));
			data.setMIBNumberDesc(rs.getString("MIB_NUM_DESC"));
		}
		}catch (SQLException e){
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		} finally {
			this.closeResources(ps, rs);
		}
		LOGGER.info("retrieveMIBNumberCodeDetail end");
		return (data);	
	}//retrieveMIBNumberCodeDetail
	

	public void insertMIBNumberCode(MIBNumberData data) throws SQLException {
		
		LOGGER.info("insertMIBNumberCode start");
		String sql = "INSERT INTO MIB_NUMBERS (MIB_NUM_CODE, MIB_NUM_DESC, CREATED_BY, CREATED_DATE) " +
					 "VALUES(?,?,?,?)";

		PreparedStatement ps = null;
		ResultSet rs = null;

		try{
			ps = conn.prepareStatement(sql);
		
			String code = data.getMIBNumberCode(); 
			if (code != null) {
				ps.setString(1, code);
			} else {
				ps.setString(1, null);
			}

			String desc = data.getMIBNumberDesc();
			if (desc != null) {
				ps.setString(2, desc);
			} else {
				ps.setString(2, null);
			}
			
			String createdBy = data.getCreatedBy(); 
			if (createdBy != null) {
				ps.setString(3, createdBy);
			} else {
				ps.setString(3, null);
			}

			ps.setTimestamp(4, DateHelper.sqlTimestamp(data.getCreateDate()));
									
			ps.executeUpdate();
		}catch (SQLException e){
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		} finally {
			this.closeResources(ps, rs);
			LOGGER.info("insertMIBNumberCode end");
		}
	}//insertMIBNumberCode


	public void updateMIBNumberCode(MIBNumberData data) throws SQLException{
		
		LOGGER.info("updateMIBNumberCode start");
		String sql = "UPDATE MIB_NUMBERS " +
					 "SET MIB_NUM_DESC = ?, UPDATED_BY = ?, UPDATED_DATE = ? " +
					 "WHERE MIB_NUM_CODE = ?";
		PreparedStatement ps = null;
		ResultSet rs = null;

		try{

			ps = conn.prepareStatement(sql);
			ps.setString(1, data.getMIBNumberDesc());				
			ps.setString(2, data.getUpdatedBy());
			ps.setTimestamp(3, DateHelper.sqlTimestamp(data.getUpdateDate()));
			ps.setString(4, data.getMIBNumberCode());										
			ps.executeUpdate();
		}catch (SQLException e){
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		} finally {
			this.closeResources(ps, rs);
			LOGGER.info("updateMIBNumberCode end");
		}
	}//updateMIBNumberCode	


	public ArrayList retrieveMIBActionCodes() throws SQLException {
		
		LOGGER.info("retrieveMIBActionCodes start");
		String sql = "SELECT MIB_ACT_CODE, MIB_ACT_DESC " +
					 "FROM MIB_ACTIONS " +
					 "ORDER BY MIB_ACT_CODE ";
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList list = new ArrayList();
		
		try{

		ps = conn.prepareStatement(sql);
		rs = ps.executeQuery();

		while (rs.next()) {
			MIBActionData data = new MIBActionData();
			data.setMIBActionCode(rs.getString("MIB_ACT_CODE"));
			data.setMIBActionDesc(rs.getString("MIB_ACT_DESC"));
			list.add(data);
		}
		}catch (SQLException e){
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		} finally {
			this.closeResources(ps, rs);
		}
		LOGGER.info("retrieveMIBActionCodes end");
		return (list);	
	}//retrieveMIBActionCodes


	public MIBActionData retrieveMIBActionCodeDetail(String code) throws SQLException {
		
		LOGGER.info("retrieveMIBActionCodeDetail start");
		String sql = "SELECT MIB_ACT_CODE, MIB_ACT_DESC " +
					 "FROM MIB_ACTIONS " +
					 "WHERE MIB_ACT_CODE = ? " +
					 "ORDER BY MIB_ACT_CODE ";					 
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		MIBActionData data = null;
		
		try{

		ps = conn.prepareStatement(sql);
		ps.setString(1, code);
		rs = ps.executeQuery();

		if (rs.next()) {
			data = new MIBActionData();
			data.setMIBActionCode(rs.getString("MIB_ACT_CODE"));
			data.setMIBActionDesc(rs.getString("MIB_ACT_DESC"));
		}
		}catch (SQLException e){
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		} finally {
			this.closeResources(ps, rs);
		}
		LOGGER.info("retrieveMIBActionCodeDetail end");
		return (data);	
	}//retrieveMIBActionCodeDetail
	

	public void insertMIBActionCode(MIBActionData data) throws SQLException {
		
		LOGGER.info("insertMIBActionCode start");
		String sql = "INSERT INTO MIB_ACTIONS (MIB_ACT_CODE, MIB_ACT_DESC, CREATED_BY, CREATED_DATE) " +
					 "VALUES(?,?,?,?)";

		PreparedStatement ps = null;
		ResultSet rs = null;

		try{

			ps = conn.prepareStatement(sql);
		
			String code = data.getMIBActionCode(); 
			if (code != null) {
				ps.setString(1, code);
			} else {
				ps.setString(1, null);
			}

			String desc = data.getMIBActionDesc();
			if (desc != null) {
				ps.setString(2, desc);
			} else {
				ps.setString(2, null);
			}
			
			String createdBy = data.getCreatedBy(); 
			if (createdBy != null) {
				ps.setString(3, createdBy);
			} else {
				ps.setString(3, null);
			}

			ps.setTimestamp(4, DateHelper.sqlTimestamp(data.getCreateDate()));
									
			ps.executeUpdate();
		}catch (SQLException e){
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		} finally {
			this.closeResources(ps, rs);
			LOGGER.info("insertMIBActionCode end");
		}
	}//insertMIBActionCode


	public void updateMIBActionCode(MIBActionData data) throws SQLException{
		
		LOGGER.info("updateMIBActionCode start");
		String sql = "UPDATE MIB_ACTIONS " +
					 "SET MIB_ACT_DESC = ?, UPDATED_BY = ?, UPDATED_DATE = ? " +
					 "WHERE MIB_ACT_CODE = ?";
		PreparedStatement ps = null;
		ResultSet rs = null;

		try{

			ps = conn.prepareStatement(sql);
			ps.setString(1, data.getMIBActionDesc());				
			ps.setString(2, data.getUpdatedBy());
			ps.setTimestamp(3, DateHelper.sqlTimestamp(data.getUpdateDate()));
			ps.setString(4, data.getMIBActionCode());										
			ps.executeUpdate();
		}catch (SQLException e){
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		} finally {
			this.closeResources(ps, rs);
			LOGGER.info("updateMIBActionCode end");
		}
	}//updateMIBActionCode	


	public ArrayList retrieveMIBLetterCodes() throws SQLException {
		
		LOGGER.info("retrieveMIBLetterCodes start");
		String sql = "SELECT MIB_LTTR_CODE, MIB_LTTR_DESC " +
					 "FROM MIB_LETTERS " +
					 "ORDER BY MIB_LTTR_CODE ";
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList list = new ArrayList();
		
		try{

		ps = conn.prepareStatement(sql);
		rs = ps.executeQuery();

		while (rs.next()) {
			MIBLetterData data = new MIBLetterData();
			data.setMIBLetterCode(rs.getString("MIB_LTTR_CODE"));
			data.setMIBLetterDesc(rs.getString("MIB_LTTR_DESC"));
			list.add(data);
		}
		}catch (SQLException e){
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		} finally {
			this.closeResources(ps, rs);
		}
		LOGGER.info("retrieveMIBLetterCodes end");
		return (list);	
	}//retrieveMIBLetterCodes


	public MIBLetterData retrieveMIBLetterCodeDetail(String code) throws SQLException {
		
		LOGGER.info("retrieveMIBLetterCodeDetail start");
		String sql = "SELECT MIB_LTTR_CODE, MIB_LTTR_DESC " +
					 "FROM MIB_LETTERS " +
					 "WHERE MIB_LTTR_CODE = ? " +
					 "ORDER BY MIB_LTTR_CODE ";					 
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		MIBLetterData data = null;
		
		try{

		ps = conn.prepareStatement(sql);
		ps.setString(1, code);
		rs = ps.executeQuery();

		if (rs.next()) {
			data = new MIBLetterData();
			data.setMIBLetterCode(rs.getString("MIB_LTTR_CODE"));
			data.setMIBLetterDesc(rs.getString("MIB_LTTR_DESC"));
		}
		}catch (SQLException e){
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		} finally {
			this.closeResources(ps, rs);
		}
		LOGGER.info("retrieveMIBLetterCodeDetail end");
		return (data);	
	}//retrieveMIBLetterCodeDetail
	

	public void insertMIBLetterCode(MIBLetterData data) throws SQLException {
		
		LOGGER.info("insertMIBLetterCode start");
		String sql = "INSERT INTO MIB_LETTERS (MIB_LTTR_CODE, MIB_LTTR_DESC, CREATED_BY, CREATED_DATE) " +
					 "VALUES(?,?,?,?)";

		PreparedStatement ps = null;
		ResultSet rs = null;

		try{
			ps = conn.prepareStatement(sql);
		
			String code = data.getMIBLetterCode(); 
			if (code != null) {
				ps.setString(1, code);
			} else {
				ps.setString(1, null);
			}

			String desc = data.getMIBLetterDesc();
			if (desc != null) {
				ps.setString(2, desc);
			} else {
				ps.setString(2, null);
			}
			
			String createdBy = data.getCreatedBy(); 
			if (createdBy != null) {
				ps.setString(3, createdBy);
			} else {
				ps.setString(3, null);
			}

			ps.setTimestamp(4, DateHelper.sqlTimestamp(data.getCreateDate()));
									
			ps.executeUpdate();
		}catch (SQLException e){
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		} finally {
			this.closeResources(ps, rs);
			LOGGER.info("insertMIBLetterCode end");
		}
	}//insertMIBLetterCode


	public void updateMIBLetterCode(MIBLetterData data) throws SQLException{
		
		LOGGER.info("updateMIBLetterCode start");
		String sql = "UPDATE MIB_LETTERS " +
					 "SET MIB_LTTR_DESC = ?, UPDATED_BY = ?, UPDATED_DATE = ? " +
					 "WHERE MIB_LTTR_CODE = ?";
		PreparedStatement ps = null;
		ResultSet rs = null;

		try{
			ps = conn.prepareStatement(sql);
			ps.setString(1, data.getMIBLetterDesc());				
			ps.setString(2, data.getUpdatedBy());
			ps.setTimestamp(3, DateHelper.sqlTimestamp(data.getUpdateDate()));
			ps.setString(4, data.getMIBLetterCode());										
			ps.executeUpdate();
		}catch (SQLException e){
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		} finally {
			this.closeResources(ps, rs);
			LOGGER.info("updateMIBLetterCode end");
		}
	}//updateMIBLetterCode	


	public ArrayList retrieveAccesses() throws SQLException {
		
		LOGGER.info("retrieveAccesses start");
		String sql = "SELECT ACC_ID, ACC_DESC " +
					 "FROM ACCESSES " +
					 "ORDER BY ACC_ID ";
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList list = new ArrayList();
		
		try{

		ps = conn.prepareStatement(sql);
		rs = ps.executeQuery();

		while (rs.next()) {
			AccessData data = new AccessData();
			data.setAccessId(rs.getLong("ACC_ID"));
			data.setAccessDesc(rs.getString("ACC_DESC"));
			list.add(data);
		}
		}catch (SQLException e){
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		} finally {
			this.closeResources(ps, rs);
		}
		LOGGER.info("retrieveAccesses end");
		return (list);	
	}//retrieveAccesses


	public AccessData retrieveAccessDetail(long code) throws SQLException {
		
		LOGGER.info("retrieveAccessDetail start");
		String sql = "SELECT ACC_ID, ACC_DESC " +
					 "FROM ACCESSES " +
					 "WHERE ACC_ID = ? " +
					 "ORDER BY ACC_ID ";					 
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		AccessData data = null;
		
		try{

		ps = conn.prepareStatement(sql);
		ps.setLong(1, code);
		rs = ps.executeQuery();

		if (rs.next()) {
			data = new AccessData();
			data.setAccessId(rs.getLong("ACC_ID"));
			data.setAccessDesc(rs.getString("ACC_DESC"));
		}
		}catch (SQLException e){
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		} finally {
			this.closeResources(ps, rs);
		}
		LOGGER.info("retrieveAccessDetail end");
		return (data);	
	}//retrieveAccessDetail
	

	public void insertAccess(AccessData data) throws SQLException {
		
		LOGGER.info("insertAccess start");
		String sql = "INSERT INTO ACCESSES (ACC_ID, ACC_DESC, CREATED_BY, CREATED_DATE) " +
					 "VALUES(?,?,?,?)";

		PreparedStatement ps = null;
		ResultSet rs = null;

		try{

			ps = conn.prepareStatement(sql);
		
			long code = data.getAccessId(); 
			ps.setLong(1, code);

			String desc = data.getAccessDesc();
			if (desc != null) {
				ps.setString(2, desc);
			} else {
				ps.setString(2, null);
			}
			
			String createdBy = data.getCreatedBy(); 
			if (createdBy != null) {
				ps.setString(3, createdBy);
			} else {
				ps.setString(3, null);
			}

			ps.setTimestamp(4, DateHelper.sqlTimestamp(data.getCreateDate()));
									
			ps.executeUpdate();
		}catch (SQLException e){
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		} finally {
			this.closeResources(ps, rs);
			LOGGER.info("insertAccess end");
		}
	}//insertAccess


	public void updateAccess(AccessData data) throws SQLException{	
		
		LOGGER.info("updateAccess start");
		String sql = "UPDATE ACCESSES " +
					 "SET ACC_DESC = ?, UPDATED_BY = ?, UPDATED_DATE = ? " +
					 "WHERE ACC_ID = ?";
		PreparedStatement ps = null;
		ResultSet rs = null;

		try{

			ps = conn.prepareStatement(sql);
			ps.setString(1, data.getAccessDesc());				
			ps.setString(2, data.getUpdatedBy());
			ps.setTimestamp(3, DateHelper.sqlTimestamp(data.getUpdateDate()));
			ps.setLong(4, data.getAccessId());										
			ps.executeUpdate();
		}catch (SQLException e){
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		} finally {
			this.closeResources(ps, rs);
			LOGGER.info("updateAccess end");
		}
	}//updateAccess	


	public ArrayList retrievePages() throws SQLException {
		
		LOGGER.info("retrievePages start");
		String sql = "SELECT PAGE_ID, PAGE_DESC " +
					 "FROM PAGES " +
					 "ORDER BY PAGE_ID ";
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList list = new ArrayList();
		
		try{

		ps = conn.prepareStatement(sql);
		rs = ps.executeQuery();

		while (rs.next()) {
			PageData data = new PageData();
			data.setPageId(rs.getLong("PAGE_ID"));
			data.setPageDesc(rs.getString("PAGE_DESC"));
			list.add(data);
		}
		}catch (SQLException e){
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		} finally {
			this.closeResources(ps, rs);
		}
		LOGGER.info("retrievePages end");
		return (list);	
	}//retrievePages


	public PageData retrievePageDetail(long code) throws SQLException {
		
		LOGGER.info("retrievePageDetail start");
		String sql = "SELECT PAGE_ID, PAGE_DESC " +
					 "FROM PAGES " +
					 "WHERE PAGE_ID = ? " +
					 "ORDER BY PAGE_ID ";
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		PageData data = null;
		
		try{

		ps = conn.prepareStatement(sql);
		ps.setLong(1, code);
		rs = ps.executeQuery();

		if (rs.next()) {
			data = new PageData();
			data.setPageId(rs.getLong("PAGE_ID"));
			data.setPageDesc(rs.getString("PAGE_DESC"));
		}
		}catch (SQLException e){
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		} finally {
			this.closeResources(ps, rs);
		}
		LOGGER.info("retrievePageDetail end");
		return (data);	
	}//retrievePageDetail
	

	public void insertPage(PageData data) throws SQLException {
		
		LOGGER.info("insertPage start");
		String sql = "INSERT INTO PAGES (PAGE_ID, PAGE_DESC, CREATED_BY, CREATED_DATE) " +
					 "VALUES(SEQ_PAGE.NEXTVAL,?,?,?)";

		PreparedStatement ps = null;
		ResultSet rs = null;

		try{

			ps = conn.prepareStatement(sql);
		
			String desc = data.getPageDesc();
			if (desc != null) {
				ps.setString(1, desc);
			} else {
				ps.setString(1, null);
			}
			
			String createdBy = data.getCreatedBy(); 
			if (createdBy != null) {
				ps.setString(2, createdBy);
			} else {
				ps.setString(2, null);
			}

			ps.setTimestamp(3, DateHelper.sqlTimestamp(data.getCreateDate()));
									
			ps.executeUpdate();
		}catch (SQLException e){
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		} finally {
			this.closeResources(ps, rs);
			LOGGER.info("insertPage end");
		}
	}//insertPage


	public void updatePage(PageData data) throws SQLException{	
		
		LOGGER.info("updatePage start");
		String sql = "UPDATE PAGES " +
					 "SET PAGE_DESC = ?, UPDATED_BY = ?, UPDATED_DATE = ? " +
					 "WHERE PAGE_ID = ?";
		PreparedStatement ps = null;
		ResultSet rs = null;

		try{
			ps = conn.prepareStatement(sql);
			ps.setString(1, data.getPageDesc());				
			ps.setString(2, data.getUpdatedBy());
			ps.setTimestamp(3, DateHelper.sqlTimestamp(data.getUpdateDate()));
			ps.setLong(4, data.getPageId());										
			ps.executeUpdate();
		}catch (SQLException e){
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		} finally {
			this.closeResources(ps, rs);
			LOGGER.info("updatePage end");
		}
	}//updatePage
	
		
	


}
