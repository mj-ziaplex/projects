package com.slocpi.ium.data.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.IUMConstants;
import com.slocpi.ium.util.NameValuePair;


public class TestProfileCHDao extends CodeHelperDao {
	private static final Logger LOGGER = LoggerFactory.getLogger(SpecializationDAO.class);
	private Connection conn;

	public TestProfileCHDao(Connection conn) throws SQLException {
	  this.conn = conn;
	}// LOBCHDao

	
	/**
	 * This method will return a collection of all the client types.
	 */
	public Collection getCodeValues() throws SQLException {
		
	  LOGGER.info("getCodeValues start 1");
	  Collection list = new ArrayList();
	  String sql = "SELECT TEST_ID AS CODE, TEST_DESC AS DESCRIPTION FROM TEST_PROFILES WHERE TEST_TYPE = ? ";

	  PreparedStatement ps = null;
	  ResultSet rs = null;
	  try{	  
		ps = conn.prepareStatement(sql);
		ps.setString(1, IUMConstants.TEST_TYPE_LABORATORY);
		rs = ps.executeQuery();				    		
		while (rs.next()) {
		  NameValuePair bean = new NameValuePair();
		  bean.setName(rs.getString("DESCRIPTION"));
		  bean.setValue(rs.getString("CODE"));
		  list.add(bean);
		}
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			  throw e; 
		}finally{
			  this.closeResources(ps, rs);
		}		  
	  LOGGER.info("getCodeValues end 1");
	  return (list);
	}// getCodeValues
	
	public Collection getCodeValues(String labId) throws SQLException {
		
      LOGGER.info("getCodeValues start 2");
	  Collection list = new ArrayList();
	  String sql = "SELECT TEST_ID AS CODE, TEST_DESC AS DESCRIPTION FROM TEST_PROFILES WHERE TEST_ID in (SELECT TEST_ID FROM LABORATORY_TESTS WHERE LAB_ID = ?)";

		PreparedStatement ps = null;
		ResultSet rs = null;
		try{
		ps = conn.prepareStatement(sql);
		ps.setString(1, labId);
		rs = ps.executeQuery();				    		
		while (rs.next()) {
		  NameValuePair bean = new NameValuePair();
		  bean.setName(rs.getString("DESCRIPTION"));
		  bean.setValue(rs.getString("CODE"));
		  list.add(bean);
		}
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			  throw e; 
		}finally{
			  this.closeResources(ps, rs);
		}	
		
		LOGGER.info("getCodeValues end 2");
	  return (list);
	}// getCodeValues


	/**
	 * This method will return a subset of client types based on the specified client type code.
	 */
	public Collection getCodeValue(String codeValue) throws SQLException {
		
		LOGGER.info("getCodeValue start");
		Collection list = new ArrayList();
		String sql = "SELECT TEST_ID AS CODE, TEST_DESC AS DESCRIPTION FROM TEST_PROFILES WHERE TEST_ID = ?";
		PreparedStatement ps = null;
		ResultSet rs = null;
		try{	  	
		  ps = conn.prepareStatement(sql);
		  ps.setString(1, codeValue);
		  rs = ps.executeQuery();				    		
		  while (rs.next()) {
			NameValuePair bean = new NameValuePair();
			bean.setName(rs.getString("DESCRIPTION"));
			bean.setValue(rs.getString("CODE"));
			list.add(bean);
		  }
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			  throw e; 
		}finally{
			  this.closeResources(ps, rs);
		}	
		LOGGER.info("getCodeValue end");
		return (list);
	}// getCodeValue




}