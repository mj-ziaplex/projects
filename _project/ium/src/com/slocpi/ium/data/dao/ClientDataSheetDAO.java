/*
 * Created on Jan 12, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.data.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.slocpi.ium.data.CDSMortalityRatingData;
import com.slocpi.ium.data.CDSSummaryPolicyCoverageData;
import com.slocpi.ium.data.ClientDataSheetData;
import com.slocpi.ium.data.PolicyDetailData;
import com.slocpi.ium.interfaces.messages.IUMMessageUtility;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.DateHelper;

/**
 * @author Cris
 *
 * Data Access Object of the Client Data Sheet
 * 
 */
public class ClientDataSheetDAO extends BaseDAO {

	private static final Logger LOGGER = LoggerFactory.getLogger(ClientDataSheetDAO.class);
	
	public ClientDataSheetDAO() {}
	
	public void insertMortalityRatingData(ClientDataSheetData cds) throws SQLException{
		
		LOGGER.info("insertMortalityRatingData start");
		
		String sql = "INSERT INTO CLIENT_DATA_SHEETS (CDS_REFERENCE_NUM, CDS_CLIENT_NUM, CDS_CLIENT_TYPE, CDS_CL_HEIGHT_FEET, " +
					 "CDS_CL_HEIGHT_INCHES, CDS_CL_WEIGHT, CDS_BASIC_MR, CDS_BASIC_CCR_MR, CDS_BASIC_APDP_MR, CDS_SMKNG_BASIC_MR," +
					 "CDS_SMKNG_CCR_MR, CDS_SMKNG_APDB_MR, CDS_FHB_BASIC_MR, CDS_FHB_CCR_MR, CDS_FHB_APDB_MR, CDS_TAM_BASIC_MR, CDS_TAM_CCR_MR," +
					 "CDS_TAM_APDB_MR) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		
		PreparedStatement stmt = null;
		Connection conn = null;
		
		try {
			conn = getConnection();	
			conn.setAutoCommit(false);
			stmt = conn.prepareStatement(sql);
			stmt.setString(1, cds.getReferenceNumber());
			stmt.setString(2,cds.getClientId());
			stmt.setString(3, cds.getClientType());
			
			CDSMortalityRatingData mortalityRating = cds.getMortalityRating();
			
			stmt.setLong(4, mortalityRating.getHeightInFeet());
			stmt.setLong(5, mortalityRating.getHeightInInches());
			stmt.setDouble(6, mortalityRating.getWeight());
			stmt.setDouble(7, mortalityRating.getBasic());
			stmt.setDouble(8, mortalityRating.getCCR());
			stmt.setDouble(9, mortalityRating.getAPDB());
			stmt.setDouble(10, mortalityRating.getSmokingBasic());
			stmt.setDouble(11, mortalityRating.getSmokingCCR());
			stmt.setDouble(12, mortalityRating.getSmokingAPDB());
			stmt.setDouble(13, mortalityRating.getFamilyHistoryBasic());
			stmt.setDouble(14, mortalityRating.getFamilyHistoryCCR());
			stmt.setDouble(15, mortalityRating.getFamilyHistoryAPDB());
			stmt.setDouble(16, mortalityRating.getTotalAssestMortBasic());
			stmt.setDouble(17, mortalityRating.getTotalAssestMortCCR());
			stmt.setDouble(18, mortalityRating.getTotalAssestMortAPDB());
			
			stmt.executeUpdate();
			conn.commit();
		}catch (SQLException e){
			LOGGER.error(CodeHelper.getStackTrace(e));
			if(conn != null){
				conn.rollback();
			}
			throw e;
		} finally {
			closeResources(conn, stmt, null);
		}
		LOGGER.info("insertMortalityRatingData end");
		
	}
	
	public void updateMortalityRatingData(ClientDataSheetData cds)throws SQLException{
		LOGGER.info("updateMortalityRatingData start");
		
		String sql = "UPDATE CLIENT_DATA_SHEETS SET CDS_CLIENT_TYPE=?, CDS_CL_HEIGHT_FEET=?, " +
					 "CDS_CL_HEIGHT_INCHES=?, CDS_CL_WEIGHT=?, CDS_BASIC_MR=?, CDS_BASIC_CCR_MR=?, CDS_BASIC_APDP_MR=?, CDS_SMKNG_BASIC_MR=?," +
					 "CDS_SMKNG_CCR_MR=?, CDS_SMKNG_APDB_MR=?, CDS_FHB_BASIC_MR=?, CDS_FHB_CCR_MR=?, CDS_FHB_APDB_MR=?, CDS_TAM_BASIC_MR=?, CDS_TAM_CCR_MR=?," +
					 "CDS_TAM_APDB_MR=? WHERE CDS_CLIENT_NUM=? AND CDS_REFERENCE_NUM=?";
		
		PreparedStatement stmt = null;
		Connection conn = null;
		
		try {
			conn = getConnection();	
			conn.setAutoCommit(false);
			stmt = conn.prepareStatement(sql);
			stmt.setString(1, cds.getClientType());
			CDSMortalityRatingData mortalityRating = cds.getMortalityRating();
			stmt.setLong(2, mortalityRating.getHeightInFeet());
			stmt.setLong(3, mortalityRating.getHeightInInches());
			stmt.setDouble(4, mortalityRating.getWeight());
			stmt.setDouble(5, mortalityRating.getBasic());
			stmt.setDouble(6, mortalityRating.getCCR());
			stmt.setDouble(7, mortalityRating.getAPDB());
			stmt.setDouble(8, mortalityRating.getSmokingBasic());
			stmt.setDouble(9, mortalityRating.getSmokingCCR());
			stmt.setDouble(10, mortalityRating.getSmokingAPDB());
			stmt.setDouble(11, mortalityRating.getFamilyHistoryBasic());
			stmt.setDouble(12, mortalityRating.getFamilyHistoryCCR());
			stmt.setDouble(13, mortalityRating.getFamilyHistoryAPDB());
			stmt.setDouble(14, mortalityRating.getTotalAssestMortBasic());
			stmt.setDouble(15, mortalityRating.getTotalAssestMortCCR());
			stmt.setDouble(16, mortalityRating.getTotalAssestMortAPDB());
			stmt.setString(17, cds.getClientId());
			stmt.setString(18,cds.getReferenceNumber());
			stmt.executeUpdate();
			conn.commit();
		}catch (SQLException e){
			LOGGER.error(CodeHelper.getStackTrace(e));
			if(conn != null){
				conn.rollback();
			}
			throw e;
		} finally {
			closeResources(conn, stmt, null);
		}
		LOGGER.info("updateMortalityRatingData end");
		
	}
	
	public void updateSummaryPolicyCoverageData(ClientDataSheetData cds) throws SQLException{
		LOGGER.info("updateSummaryPolicyCoverageData start");
		
		String sql = "UPDATE CLIENT_DATA_SHEETS " +
				        "SET CDS_TOT_OTHER_COMPANIES = ?, " +
				            "CDS_APP_BR_AMT = ?, " +
				            "CDS_APP_AD_AMT = ?," +
					        "CDS_PEND_BR_AMT = ?, " +
					        "CDS_PEND_AD_AMT = ?, " +
					        "CDS_INFC_BR_AMT = ?, " +
					        "CDS_INFC_AD_AMT = ?, " +
					        "CDS_LAPS_BR_AMT = ?," +
					        "CDS_LAPS_AD_AMT = ?, " +
					        "CDS_OINS_BR_AMT = ?, " +
					        "CDS_OINS_AD_AMT = ?, " +
					        "CDS_INFC_CCR_AMT = ?, " +
					        "CDS_INFC_APDB_AMT = ?," +
					        "CDS_INFC_HIB_AMT = ?, " +
					        "CDS_INFC_FMB_AMT = ?, " +
					        "CDS_TOT_REINS_AMT = ?, " +
					        "CDS_TOT_EXISTING_SL_BR = ?, " +
					        "CDS_TOT_EXISTING_SL_AD = ?, " +
					        "CDS_TOT_SL_OINS_BR = ?, " +
					        "CDS_TOT_SL_OINS_AD = ?, " +
					        "CDS_TOT_ADB_REINSURED_AMOUNT = ? " +
					  "WHERE CDS_REFERENCE_NUM = ? " +
					    "AND CDS_CLIENT_NUM = ?";
		
		PreparedStatement stmt = null;
		Connection conn = null;
		
		try {
			conn = getConnection();	
			conn.setAutoCommit(false);
			stmt = conn.prepareStatement(sql);
				
			CDSSummaryPolicyCoverageData summaryOfPolicy = cds.getSummaryOfPolicies();
			
			stmt.setDouble(1, summaryOfPolicy.getTotalOtherCompanies());
			stmt.setDouble(2, summaryOfPolicy.getAppliedForBr());
			stmt.setDouble(3, summaryOfPolicy.getAppliedForAd());
			stmt.setDouble(4, summaryOfPolicy.getPendingAmountBr());
			stmt.setDouble(5, summaryOfPolicy.getPendingAmountAd());
			stmt.setDouble(6, summaryOfPolicy.getExistingInForceBr());
			stmt.setDouble(7, summaryOfPolicy.getExistingInForceAd());
			stmt.setDouble(8, summaryOfPolicy.getExistingLapsedBr());
			stmt.setDouble(9, summaryOfPolicy.getExistingLapsedAd());
			stmt.setDouble(10, summaryOfPolicy.getOtherCompaniesBr());
			stmt.setDouble(11, summaryOfPolicy.getOtherCompaniesAd());
			stmt.setDouble(12, summaryOfPolicy.getTotalCCRCoverage());
			stmt.setDouble(13, summaryOfPolicy.getTotalAPDBCoverage());
			stmt.setDouble(14, summaryOfPolicy.getTotalHIBCoverage());
			stmt.setDouble(15, summaryOfPolicy.getTotalFBBMBCoverage());
			stmt.setDouble(16, summaryOfPolicy.getTotalReinsuredAmount());
			stmt.setDouble(17, summaryOfPolicy.getTotalExistingSunLifeBr());
			stmt.setDouble(18, summaryOfPolicy.getTotalExistingSunLifeAd());
			stmt.setDouble(19, summaryOfPolicy.getTotalSunLifeOtherCompaniesBr());
			stmt.setDouble(20, summaryOfPolicy.getTotalSunLifeOtherCompaniesAd());
			stmt.setDouble(21, summaryOfPolicy.getTotalAdbReinsuredAmount());
			
			stmt.setString(22, cds.getReferenceNumber());
			stmt.setString(23, cds.getClientId());
		
			stmt.executeUpdate();
			conn.commit();
		}catch (SQLException e){
			LOGGER.error(CodeHelper.getStackTrace(e));
			if(conn != null){
				conn.rollback();
			}
			throw e;
		} finally {
			closeResources(conn, stmt, null);
		}
		LOGGER.info("updateSummaryPolicyCoverageData end");
	}
	
	
	
	public ClientDataSheetData retrieveCDS(String referenceNumber, String clientId) throws SQLException {
		
		LOGGER.info("retrieveCDS start");
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try {
			CDSMortalityRatingData mortalityRating = retrieveMortalityRating(referenceNumber, clientId);
			CDSSummaryPolicyCoverageData summaryOfPolicy = retrieveSummaryPolicy(referenceNumber, clientId);
			ArrayList policyCoverageList = retrievePolicyCoverageList(referenceNumber, clientId);
		    
			ClientDataSheetData cds = new ClientDataSheetData();
			cds.setMortalityRating(mortalityRating);
			cds.setSummaryOfPolicies(summaryOfPolicy);
			cds.setPolicyCoverageDetailsList(policyCoverageList);
						
			return (cds);	
		}catch(SQLException e)
		{
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		}finally
		{
			closeResources(ps, rs);
			LOGGER.info("retrieveCDS end");
		}	
		
	}// retrieveCDS


	public ArrayList retrievePolicyCoverageList(
			final String  referenceNumber,
			final String clientNo) throws SQLException {
		
		LOGGER.info("retrievePolicyCoverageList start");
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT")
			.append("  PCD.PCD_POLICY_NUM as POLICY_NO, ")
			.append("  PCD.PCD_COVERAGE_NUM as COVERAGE_NO, ")
			.append("  PCD.PCD_POL_RELATIONSHIP as RELATIONSHIP, ")
			.append("  PCD.PCD_ISSUE_DATE as ISSUE_DATE, ")
			.append("  PCD.PCD_COVERAGE_STATUS as COVERAGE_STATUS, ")
			.append("  PCD.PCD_SMOKER_CODE as SMOKER_CODE, ")
			.append("  PCD.PCD_PLAN_CODE as PLAN_CODE, ")
			.append("  PCD.PCD_MEDICAL_IND as MEDICAL_IND, ")
			.append("  PCD.PCD_FACE_AMOUNT as FACE_AMOUNT, ")
			.append("  PCD.PCD_DECISION as DECISION, ")
			.append("  PCD.PCD_ADB_FACE_AMOUNT as ADB_FACE_AMOUNT, ")
			.append("  PCD.PCD_AD_MULTIPLIER as AD_MULTIPLIER, ")
			.append("  PCD.PCD_WP_MULTIPLIER AS WP_MULTIPLIER, ")
			.append("  PCD.PCD_REINSURED_AMOUNT as REINSURED_AMOUNT, ")
			.append("  PCD.PCD_POLICY_SUFFIX as POLICY_SUFFIX, ")
			.append("  PCD.PCD_MORT_MORB_RATING as MORT_MORB_RATING, ")
			.append("  PCD.PCD_TEMP_FLAT_RATING as TEMP_FLAT_RATING, ")
			.append("  PCD.PCD_PERM_FLAT_RATING as PERM_FLAT_RATING, ")
			.append("  PCD.PCD_ADB_REINSURED_AMOUNT as ADB_REINSURED_AMOUNT, ")
			.append("  PCD.PCD_REINS_DATE as REINS_DATE, ")
			.append("  PCD.PCD_REINS_TYPE as REINS_TYPE ")
			.append(" FROM") 
			.append("	POLICY_COVERAGE_DETAILS PCD")
			//.append("	ASSESSMENT_REQUESTS AR ")
			.append("  WHERE")
			//.append("	PCD.UAR_REFERENCE_NUM = AR.REFERENCE_NUM AND ")
			.append("	PCD.UAR_REFERENCE_NUM = ? AND ")
			.append("	PCD.PCD_CLIENT_NUM = ? ")
			.append(" ORDER BY ISSUE_DATE ASC, POLICY_NO ASC, COVERAGE_NO ASC");
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection conn = null;
		try {
			conn = getConnection();	
			ArrayList list = new ArrayList();
			
			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, referenceNumber);
			ps.setString(2, clientNo);
	
			rs = ps.executeQuery();
       	
			while (rs.next()) {
				PolicyDetailData policyData = new PolicyDetailData();
				policyData.setPolicyNumber(rs.getString("POLICY_NO"));
				policyData.setCoverageNumber(rs.getLong("COVERAGE_NO"));
				policyData.setRelationship(rs.getString("RELATIONSHIP"));
				policyData.setIssueDate(rs.getDate("ISSUE_DATE"));
				policyData.setCoverageStatus(rs.getString("COVERAGE_STATUS"));
				policyData.setSmoker(IUMMessageUtility.isTrue(rs.getString("SMOKER_CODE")));
				policyData.setPlanCode(rs.getString("PLAN_CODE"));
				policyData.setMedical(IUMMessageUtility.isTrue(rs.getString("MEDICAL_IND")));
				policyData.setFaceAmount(rs.getDouble("FACE_AMOUNT"));
				policyData.setDecision(rs.getString("DECISION"));
				policyData.setADBFaceAmount(rs.getDouble("ADB_FACE_AMOUNT"));
				policyData.setADMultiplier(rs.getInt("AD_MULTIPLIER"));
				policyData.setWPMultiplier(rs.getInt("WP_MULTIPLIER"));
				policyData.setReinsuredAmount(rs.getDouble("REINSURED_AMOUNT"));
				policyData.setPolicySuffix(rs.getString("POLICY_SUFFIX"));
				policyData.setRelCvgMeFct(rs.getDouble("MORT_MORB_RATING"));
				policyData.setRelCvgFeUpremAmt(rs.getDouble("TEMP_FLAT_RATING"));
				policyData.setRelCvgFePermAmt(rs.getDouble("PERM_FLAT_RATING"));
				policyData.setAdbReinsuredAmount(rs.getDouble("ADB_REINSURED_AMOUNT"));
				policyData.setReinstatementDate(rs.getDate("REINS_DATE"));
				policyData.setReinsuranceType(rs.getString("REINS_TYPE"));
				list.add(policyData);				
			}
			
			return (list);	
		}catch(SQLException e)
		{
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		}finally
		{
			closeResources(conn, ps, rs);
			LOGGER.info("retrievePolicyCoverageList start");
		}
	}// retrievePolicyCoverageList


	public CDSMortalityRatingData retrieveMortalityRating(
			final String  referenceNumber,
			final String clientNo) throws SQLException {
		
		LOGGER.info("retrieveMortalityRating start");
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT")
		.append("  CDS.CDS_CL_HEIGHT_FEET AS HEIGHT_FEET, ")
		.append("  CDS.CDS_CL_HEIGHT_INCHES AS HEIGHT_INCHES, ")
		.append("  CDS.CDS_CL_WEIGHT AS WEIGHT, ")
		.append("  CDS.CDS_BASIC_MR AS BASIC, ")
		.append("  CDS.CDS_BASIC_CCR_MR AS CCR, ")
		.append("  CDS.CDS_BASIC_APDP_MR AS APDB, ")
		.append("  CDS.CDS_SMKNG_BASIC_MR AS SMOKING_BASIC, ")
		.append("  CDS.CDS_SMKNG_CCR_MR AS SMOKING_CCR, ")
		.append("  CDS.CDS_SMKNG_APDB_MR AS SMKOING_APDB, ")
		.append("  CDS.CDS_FHB_BASIC_MR AS FKB_BASIC, ")
		.append("  CDS.CDS_FHB_CCR_MR AS FHB_CCR, ")
		.append("  CDS.CDS_FHB_APDB_MR AS FHB_APDB, ")
		.append("  CDS.CDS_TAM_BASIC_MR AS TAM_BASIC, ")
		.append("  CDS.CDS_TAM_CCR_MR AS TAM_CCR, ")
		.append("  CDS.CDS_TAM_APDB_MR AS TAM_APDB ")
		.append(" FROM")
		.append(" 	CLIENT_DATA_SHEETS CDS ")
		//.append(" 	ASSESSMENT_REQUESTS AR ")
		.append("  WHERE")
		//.append(" 	CDS.CDS_REFERENCE_NUM = AR.REFERENCE_NUM AND ")
		.append(" 	CDS.CDS_REFERENCE_NUM = ? AND ")
		.append(" 	CDS.CDS_CLIENT_NUM = ? ");
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection conn = null;
		
		try {
			conn = getConnection();	
			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, referenceNumber);
			ps.setString(2, clientNo);
	
			rs = ps.executeQuery();

			CDSMortalityRatingData mrData = new CDSMortalityRatingData();

			if (rs.next()) {				
				mrData.setHeightInFeet(rs.getLong("HEIGHT_FEET"));
				mrData.setHeightInInches(rs.getLong("HEIGHT_INCHES"));
				mrData.setWeight(rs.getDouble("WEIGHT"));
				mrData.setBasic(rs.getDouble("BASIC"));
				mrData.setCCR(rs.getDouble("CCR"));
				mrData.setAPDB(rs.getDouble("APDB"));
				mrData.setSmokingBasic(rs.getDouble("SMOKING_BASIC"));
				mrData.setSmokingCCR(rs.getDouble("SMOKING_CCR"));
				mrData.setSmokingAPDB(rs.getDouble("SMKOING_APDB"));
				mrData.setFamilyHistoryBasic(rs.getDouble("FKB_BASIC"));
				mrData.setFamilyHistoryCCR(rs.getDouble("FHB_CCR"));
				mrData.setFamilyHistoryAPDB(rs.getDouble("FHB_APDB"));
				mrData.setTotalAssestMortBasic(rs.getDouble("TAM_BASIC"));
				mrData.setTotalAssestMortCCR(rs.getDouble("TAM_CCR"));
				mrData.setTotalAssestMortAPDB(rs.getDouble("TAM_APDB"));
			} 
			return (mrData);	
		}catch(SQLException e)
		{
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		}finally
		{
			closeResources(conn, ps, rs);
			LOGGER.info("retrieveMortalityRating end");
		}
		
	}// retrieveMortalityRating


	public CDSSummaryPolicyCoverageData retrieveSummaryPolicy(
			final String  referenceNumber,
			final String clientNo) throws SQLException {

		LOGGER.info("retrieveSummaryPolicy start");
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT")
			.append("  CDS.CDS_APP_BR_AMT AS APPLIED_FOR_BR, ")
			.append("  CDS.CDS_APP_AD_AMT AS APPLIED_FOR_AD, ")
			.append("  CDS.CDS_PEND_BR_AMT AS PENDING_AMOUNT_BR, ")
			.append("  CDS.CDS_PEND_AD_AMT AS PENDING_AMOUNT_AD, ")
			.append("  CDS.CDS_INFC_BR_AMT AS EXIST_SL_INFORCE_BR, ") 
			.append("  CDS.CDS_INFC_AD_AMT AS EXIST_SL_INFORCE_AD, ")						
			.append("  CDS.CDS_LAPS_BR_AMT AS EXIST_SL_LAPSED_BR, ") 
			.append("  CDS.CDS_LAPS_AD_AMT AS EXIST_SL_LAPSED_AD, ")						
			.append("  (CDS.CDS_INFC_BR_AMT) AS TOTAL_EXIST_SL_BR, ") 
			.append("  (CDS.CDS_INFC_AD_AMT) AS TOTAL_EXIST_SL_AD, ")
			.append("  (CDS.CDS_APP_BR_AMT + CDS.CDS_PEND_BR_AMT + CDS.CDS_INFC_BR_AMT) AS TOTAL_SL_BR, ") 
			.append("  (CDS.CDS_APP_AD_AMT + CDS.CDS_PEND_AD_AMT + CDS.CDS_INFC_AD_AMT) AS TOTAL_SL_AD, ")	
			.append("  CDS.CDS_OINS_BR_AMT AS OTHER_COMPANIES_BR, ") 
			.append("  CDS.CDS_OINS_AD_AMT AS OTHER_COMPANIES_AD, ")
			.append("  (CDS.CDS_APP_BR_AMT + CDS.CDS_PEND_BR_AMT + CDS.CDS_INFC_BR_AMT + CDS.CDS_OINS_BR_AMT) AS TOTAL_SL_OC_BR, ") 
			.append("  (CDS.CDS_APP_AD_AMT + CDS.CDS_PEND_AD_AMT + CDS.CDS_INFC_AD_AMT + CDS.CDS_OINS_AD_AMT) AS TOTAL_SL_OC_AD, ")	
			.append("  CDS.CDS_INFC_CCR_AMT AS CCR, ")
			.append("  CDS.CDS_INFC_APDB_AMT AS APDB, ")
			.append("  CDS.CDS_INFC_HIB_AMT AS HIB, ")
			.append("  CDS.CDS_INFC_FMB_AMT AS FMB, ")
			.append("  CDS.CDS_TOT_REINS_AMT AS TOTAL_REINSURED_AMOUNT, ")
			.append("  CDS.CDS_TOT_ADB_REINSURED_AMOUNT AS TOTAL_ADB_REINSURED_AMOUNT ")
			.append(" FROM")
			.append("	CLIENT_DATA_SHEETS CDS ")
			//.append("	ASSESSMENT_REQUESTS AR ")
			.append("  WHERE")
			//.append("	CDS.CDS_REFERENCE_NUM = AR.REFERENCE_NUM AND ")
			.append("	CDS.CDS_REFERENCE_NUM = ? AND ")
			.append("	CDS.CDS_CLIENT_NUM = ? ");
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection conn = null;
		
		try {
			conn = getConnection();	
			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, referenceNumber);
			ps.setString(2, clientNo);
	
			rs = ps.executeQuery();

			CDSSummaryPolicyCoverageData summaryData = new CDSSummaryPolicyCoverageData();

			if (rs.next()) {
				
				summaryData.setAppliedForBr(rs.getDouble("APPLIED_FOR_BR"));
				summaryData.setAppliedForAd(rs.getDouble("APPLIED_FOR_AD"));
				summaryData.setPendingAmountBr(rs.getDouble("PENDING_AMOUNT_BR"));
				summaryData.setPendingAmountAd(rs.getDouble("PENDING_AMOUNT_AD"));
				summaryData.setExistingInForceBr(rs.getDouble("EXIST_SL_INFORCE_BR"));
				summaryData.setExistingInForceAd(rs.getDouble("EXIST_SL_INFORCE_AD"));
				summaryData.setExistingLapsedBr(rs.getDouble("EXIST_SL_LAPSED_BR"));
				summaryData.setExistingLapsedAd(rs.getDouble("EXIST_SL_LAPSED_AD"));
				summaryData.setTotalExistingSunLifeBr(rs.getDouble("TOTAL_EXIST_SL_BR"));
				summaryData.setTotalExistingSunLifeAd(rs.getDouble("TOTAL_EXIST_SL_AD"));
				summaryData.setTotalSunLifeBr(rs.getDouble("TOTAL_SL_BR"));
				summaryData.setTotalSunLifeAd(rs.getDouble("TOTAL_SL_AD"));
				summaryData.setOtherCompaniesBr(rs.getDouble("OTHER_COMPANIES_BR"));
				summaryData.setOtherCompaniesAd(rs.getDouble("OTHER_COMPANIES_AD"));
				summaryData.setTotalSunLifeOtherCompaniesBr(rs.getDouble("TOTAL_SL_OC_BR"));
				summaryData.setTotalSunLifeOtherCompaniesAd(rs.getDouble("TOTAL_SL_OC_AD"));
				summaryData.setTotalCCRCoverage(rs.getDouble("CCR"));
				summaryData.setTotalAPDBCoverage(rs.getDouble("APDB"));
				summaryData.setTotalHIBCoverage(rs.getDouble("HIB"));
				summaryData.setTotalFBBMBCoverage(rs.getDouble("FMB"));
				summaryData.setTotalReinsuredAmount(rs.getDouble("TOTAL_REINSURED_AMOUNT"));
				summaryData.setTotalAdbReinsuredAmount(rs.getDouble("TOTAL_ADB_REINSURED_AMOUNT"));
			}
			
			return (summaryData);	
		} catch(SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		} finally {
			closeResources(conn, ps, rs);
			LOGGER.info("retrieveSummaryPolicy end");
		}
	}// retrieveSumaryPolicy
	
	public boolean isCDSExisting (String referenceNumber, String clientId) throws SQLException{
		
		LOGGER.info("isCDSExisting start");
		boolean result=false;
		
		String sql = "SELECT COUNT(*) FROM CLIENT_DATA_SHEETS WHERE CDS_REFERENCE_NUM = ? AND CDS_CLIENT_NUM = ?";
		PreparedStatement stmt = null;
		ResultSet rs = null;
		Connection conn = null;
		
		try {
			conn = getConnection();	
			stmt = conn.prepareStatement(sql);
			stmt.setString(1, referenceNumber);
			stmt.setString(2, clientId);
			rs = stmt.executeQuery();
			if (rs.next()){
				if (rs.getInt(1) > 0){
					result = true;
				}
			}			
		}catch(SQLException e)
		{
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		}finally
		{
			closeResources(conn, stmt, rs);
		}

		LOGGER.info("isCDSExisting end");
		return result;
	}
	
	public ArrayList retrieveClientType(String referenceNumber, String clientId) throws SQLException {
		
		LOGGER.info("retrieveClientType start");
		ArrayList typeList = new ArrayList();
		
		String sql = "SELECT " +
						"CDS_CLIENT_TYPE " +
					"FROM" +
						" CLIENT_DATA_SHEETS CDS, " +
						" ASSESSMENT_REQUESTS AR " +
					"WHERE" +
						" CDS.CDS_REFERENCE_NUM = AR.REFERENCE_NUM AND " +
						" CDS.CDS_REFERENCE_NUM = ? AND " +
						" CDS.CDS_CLIENT_NUM = ? ";
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection conn = null;
		
		try {
			conn = getConnection();	
			ps = conn.prepareStatement(sql);
			ps.setString(1, referenceNumber);
			ps.setString(2, clientId);
	
			rs = ps.executeQuery();
			while(rs.next()){
				typeList.add(rs.getString("CDS_CLIENT_TYPE"));
			}		
		
		}catch(SQLException e)
		{
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		}finally
		{
			closeResources(conn, ps, rs);
		}
		LOGGER.info("retrieveClientType end");
		return typeList;				
		
	}
	
	public boolean isPolicyDetailsExisting(String referenceNumber, String clientId, String policyNumber, long coverageNumber) throws SQLException{
		
		LOGGER.info("isPolicyDetailsExisting start");
		boolean result = false;
		String sql = "SELECT COUNT(*) FROM POLICY_COVERAGE_DETAILS WHERE UAR_REFERENCE_NUM = ? AND PCD_CLIENT_NUM = ? AND PCD_POLICY_NUM = ? AND PCD_COVERAGE_NUM = ?";
		
		PreparedStatement stmt = null;
		ResultSet rs = null;
		Connection conn = null;
		
		try {
			conn = getConnection();	
			stmt = conn.prepareStatement(sql);
			stmt.setString(1, referenceNumber);
			stmt.setString(2, clientId);
			stmt.setString(3, policyNumber);
			stmt.setLong(4, coverageNumber);
			rs = stmt.executeQuery();
			if (rs.next()){
				if (rs.getInt(1)> 0){
					result = true;
				}
			}
	}catch(SQLException e)
	{
		LOGGER.error(CodeHelper.getStackTrace(e));
		throw e;
	}finally
	{
		closeResources(conn, stmt, rs);
	}
		LOGGER.info("isPolicyDetailsExisting end");
		return result;
	}
	
	public void insertPolicyDetails(PolicyDetailData polData) throws SQLException {
		
		
		LOGGER.info("insertPolicyDetails start");
		String sql = "INSERT INTO POLICY_COVERAGE_DETAILS (UAR_REFERENCE_NUM, PCD_POLICY_NUM, PCD_COVERAGE_NUM," +
					 "PCD_POL_RELATIONSHIP, PCD_ISSUE_DATE, PCD_COVERAGE_STATUS, PCD_SMOKER_CODE, PCD_PLAN_CODE, PCD_MEDICAL_IND, " +
					 "PCD_FACE_AMOUNT, PCD_DECISION, PCD_ADB_FACE_AMOUNT, PCD_AD_MULTIPLIER, PCD_WP_MULTIPLIER, PCD_REINSURED_AMOUNT, PCD_CLIENT_NUM, PCD_POLICY_SUFFIX, " +
					 "PCD_MORT_MORB_RATING, PCD_TEMP_FLAT_RATING, PCD_PERM_FLAT_RATING, PCD_ADB_REINSURED_AMOUNT, PCD_REINS_DATE, PCD_REINS_TYPE) " +
					 "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		PreparedStatement stmt = null;
		ResultSet rs = null;
		Connection conn = null;
		
		try {
			conn = getConnection();	
			conn.setAutoCommit(false);
			stmt = conn.prepareStatement(sql);
			stmt.setString(1,polData.getReferenceNumber());
			stmt.setString(2, polData.getPolicyNumber());
			stmt.setLong(3, polData.getCoverageNumber());
			stmt.setString(4, polData.getRelationship());
			stmt.setDate(5, DateHelper.sqlDate(polData.getIssueDate()));
			stmt.setString(6, polData.getCoverageStatus());
			stmt.setBoolean(7, polData.isSmoker());
			stmt.setString(8, polData.getPlanCode());
			stmt.setBoolean(9, polData.isMedical());
			stmt.setDouble(10, polData.getFaceAmount());
			stmt.setString(11, polData.getDecision());
			stmt.setDouble(12, polData.getADBFaceAmount());
			stmt.setDouble(13, polData.getADMultiplier());
			stmt.setDouble(14, polData.getWPMultiplier());
			stmt.setDouble(15, polData.getReinsuredAmount());
			stmt.setString(16, polData.getClientId());
			stmt.setString(16, polData.getClientId());
			if (polData.getPolicySuffix() != null){
				stmt.setString(17, polData.getPolicySuffix());	
			} else {
				stmt.setString(17, null);
			}
			stmt.setDouble(18, polData.getRelCvgMeFct());
			stmt.setDouble(19, polData.getRelCvgFeUpremAmt());
			stmt.setDouble(20, polData.getRelCvgFePermAmt());
			stmt.setDouble(21, polData.getAdbReinsuredAmount());
			stmt.setDate(22, DateHelper.sqlDate(polData.getReinstatementDate()));
			stmt.setString(23, polData.getReinsuranceType());
			
			stmt.executeUpdate();
			conn.commit();
		} catch (SQLException e){
			LOGGER.error(CodeHelper.getStackTrace(e));
			try {
				if(conn != null){
					conn.rollback();
				}
			} catch (SQLException e1) {
				LOGGER.error(CodeHelper.getStackTrace(e1));
			}
		}finally
		{
			closeResources(conn, stmt, rs);
			LOGGER.info("insertPolicyDetails end");
		}
	}
	
	public void deleteCDS(String referenceNum) throws SQLException {
		
		LOGGER.info("deleteCDS start");
		String sql = "DELETE CLIENT_DATA_SHEETS WHERE CDS_REFERENCE_NUM = ?";
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection conn = null;
		
		try {
			conn = getConnection();	
			conn.setAutoCommit(false);
			ps = conn.prepareStatement(sql);
			ps.setString(1, referenceNum);
			ps.executeUpdate();
			conn.commit();
		}catch(SQLException e)
		{
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		}finally
		{
			closeResources(conn, ps, rs);
			LOGGER.info("deleteCDS end");
		}
	}
	
	public void deletePolicyCoverageDetails(String referenceNum) throws SQLException {
		
		LOGGER.info("deletePolicyCoverageDetails start");
		String sql = "DELETE POLICY_COVERAGE_DETAILS WHERE UAR_REFERENCE_NUM = ?";
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection conn = null;
		
		try {
			conn = getConnection();	
			conn.setAutoCommit(false);
			ps = conn.prepareStatement(sql);
			ps.setString(1, referenceNum);
			ps.executeUpdate();
			conn.commit();
		}catch(SQLException e)
		{
			LOGGER.error(CodeHelper.getStackTrace(e));
			if(conn != null){
				conn.rollback();
			}
			throw e;
		}finally
		{
			closeResources(conn, ps, rs);
			LOGGER.info("deletePolicyCoverageDetails end");
		}
	}

	
}
