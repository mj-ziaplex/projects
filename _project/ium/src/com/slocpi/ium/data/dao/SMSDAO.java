package com.slocpi.ium.data.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ph.com.sunlife.wms.util.db.SMSDbManager;
import com.slocpi.ium.interfaces.notification.NotificationMessage;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.IUMConstants;

public class SMSDAO {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(SMSDAO.class);
	
	public void getEmailSMSNotification() throws SQLException	{
		
		LOGGER.info("getEmailSMSNotification start");
		StringBuffer sql = new StringBuffer();
        sql.append("SELECT recipient_id ").append("FROM mo_email_sms_notification ");
        
        SMSDbManager db = new SMSDbManager();
        try{
			ResultSet rs = db.doSelect(sql.toString());
			String sCategory = "";
			while(rs.next()){
				sCategory = rs.getString("recipient_id");
				if(sCategory == null){
					sCategory = "";
				}
			}
        }catch(Exception e){
        	LOGGER.error(CodeHelper.getStackTrace(e));
        }
        LOGGER.info("getEmailSMSNotification end");
	}
	
	public boolean insertIntoSMSNotification(
			final NotificationMessage smsMessage,
			final String agentCode) {
		
		LOGGER.info("insertIntoSMSNotification start");
		LOGGER.info("agent code " + agentCode);
		StringBuffer sb = new StringBuffer();
		if(smsMessage.getContent() != null){
			String content = smsMessage.getContent().replace("'", "''");
			sb.append("INSERT INTO mo_email_sms_notification (")
			.append("sender_add, recipient_id, recipient_typ, ")
			.append("message, sms_email_tag, company_code ")
			.append(") VALUES (")
			.append("'").append(smsMessage.getSender()).append( "', ")
			.append("'").append(agentCode).append("', ")
			.append("'").append(IUMConstants.SMS_DEFAULT_RECIPIENT_TYPE).append("', ")
			.append("'").append(content).append("', ")
			.append("'").append(IUMConstants.SMS_DEFAULT_EMAIL_TAG).append("', ")
			.append("'").append(smsMessage.getCompCode()).append("' ")
			.append(") ");
		}
		try {
			SMSDbManager db = new SMSDbManager();
			db.doSave(sb.toString());
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			return false;
		}
		LOGGER.info("insertIntoSMSNotification end");
		return true;
	}
}
