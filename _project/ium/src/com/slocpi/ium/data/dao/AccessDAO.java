/**
 * AccessDAO.java				Copyright (c) 2004 Pointwest Technologies Corp.
 * TODO DOCUMENT ME!
 * 
 * @author aditalo	 @date Feb 17, 2004
 */
package com.slocpi.ium.data.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.slocpi.ium.data.AccessData;
import com.slocpi.ium.util.CodeHelper;

/**
 * TODO DOCUMENT ME!
 * 
 * @author aditalo		Copyright (c) 2004 Pointwest Technologies Corp.
 * @date Feb 17, 2004
 */
public class AccessDAO {

	static final Logger LOGGER = LoggerFactory.getLogger(AccessDAO.class);
	private Connection conn = null;
	
	/**
	 * 
	 */
	public AccessDAO(Connection connection) {
		conn = connection;
	}

	
	public ArrayList retrieveAccess() throws SQLException {
		
		LOGGER.info("retrieveAccess start");
		ArrayList list = new ArrayList();
		String sql = "SELECT ACC_ID, ACC_DESC FROM ACCESSES";
		
		ResultSet rs = null;
		PreparedStatement ps = null;
		
		try {
			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery();
			while (rs.next()) {
				AccessData data = new AccessData();
				data.setAccessId(rs.getLong("ACC_ID"));
				data.setAccessDesc(rs.getString("ACC_DESC"));
				list.add(data);
			}
		} catch (Exception e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
		}finally
		{
			try{
				if(rs != null){
					rs.close();
				}
			}catch(Exception e1)
			{
				LOGGER.error(CodeHelper.getStackTrace(e1));
			}
			try{
				if(ps != null){
					ps.close();
				}
			}
			catch(Exception e1)
			{
				LOGGER.error(CodeHelper.getStackTrace(e1));
			}
		
		}
		LOGGER.info("retrieveAccess end");
		return list;
	}

}
