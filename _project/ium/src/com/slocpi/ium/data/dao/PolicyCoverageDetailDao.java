package com.slocpi.ium.data.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.slocpi.ium.data.PolicyDetailData;
import com.slocpi.ium.util.CodeHelper;

public class PolicyCoverageDetailDao extends BaseDAO{

	private static final Logger LOGGER = LoggerFactory.getLogger(PolicyCoverageDetailDao.class);

	public PolicyCoverageDetailDao () {}
	
	
	public void createPolicyCoverageDetail(PolicyDetailData data) throws SQLException {
		
		LOGGER.info("createPolicyCoverageDetail start");
		String sql =  
			"INSERT INTO POLICY_COVERAGE_DETAIL " +
				   "(REFERENCE_NUM, CLIENT_NUM, COVERAGE_STATUS, FACE_AMOUNT, PLAN_CODE, PLAN_DESC, COVERAGE_NUM) " +
			"VALUES (?,?,?,?,?,?,?)";
		PreparedStatement ps = null;
		Connection conn = null;
		
		try {
			conn = getConnection();	
			conn.setAutoCommit(false);
			ps = conn.prepareStatement(sql);
			ps.setString(1, data.getReferenceNumber().substring(0, data.getReferenceNumber().length()-1));
			ps.setString(2, data.getClientId());
			ps.setString(3, data.getCoverageStatus());
			ps.setDouble(4, data.getFaceAmount());
			ps.setString(5, data.getPlanCode());
			ps.setString(6, data.getPlanDescription());
			ps.setLong(7, data.getCoverageNumber());
			ps.executeUpdate();
			conn.commit();
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));	
			if(conn != null){
				conn.rollback();
			}
			throw e;
		} finally {
			closeResources(conn, ps, null);
		}
		LOGGER.info("createPolicyCoverageDetail end");
	}

	public boolean isPolicyCoverageDetailExisting(PolicyDetailData data) throws SQLException {
		
		LOGGER.info("isPolicyCoverageDetailExisting start");
		String sql = 
			"SELECT count(*) " +
			  "FROM POLICY_COVERAGE_DETAIL " +
			 "WHERE REFERENCE_NUM = ? " +
			   "AND CLIENT_NUM = ? " +
			   "AND PLAN_CODE = ? " +
			   "AND COVERAGE_NUM = ? " +
			   "AND COVERAGE_STATUS = ?";
		PreparedStatement ps = null;
		ResultSet rs = null;
		boolean isExisting = false;
		Connection conn = null;
		
		try {
			conn = getConnection();	
			ps = conn.prepareStatement(sql);
			ps.setString(1, data.getReferenceNumber().substring(0, data.getReferenceNumber().length()-1));
			ps.setString(2, data.getClientId());
			ps.setString(3, data.getPlanCode());
			ps.setLong(4, data.getCoverageNumber());
			ps.setString(5, data.getCoverageStatus());
			rs = ps.executeQuery();
			if (rs != null) {
				while (rs.next()) {
					if (rs.getInt(1) > 0) {
						isExisting =  true;
					}
				}
			}
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));	
			throw e;
		} finally {
			closeResources(conn, ps, rs);
		}
		LOGGER.info("isPolicyCoverageDetailExisting end");
		return isExisting;
	}
	
	public boolean hasRiders(String refNo) throws SQLException {
		
		LOGGER.info("hasRiders start");
		String sql = 
			"SELECT plan_code " +
			  "FROM POLICY_COVERAGE_DETAIL " +
			 "WHERE REFERENCE_NUM = '" + refNo + "' ";
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		boolean isExisting = false;
		Connection conn = null;
		
		try {
			conn = getConnection();	
			Statement s = conn.createStatement();
			rs = s.executeQuery(sql);
			if (rs != null) {
				while (rs.next()) {
					String planCode = rs.getString("plan_code");
					boolean isRiderLookup = isRiderLookup(planCode);
					if (isRiderLookup) {
						isExisting = true;
						break;
					}
				}
			}
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));	
			throw e;
		} finally {
			closeResources(conn, ps, rs);
		}
		LOGGER.info("hasRiders end");
		return isExisting;
	}
	
	public boolean isRiderLookup(String planCode) throws SQLException {
		
		LOGGER.info("isRiderLookup start");
		String sql = 
			"SELECT count(*) as ridercount " +
			  "FROM riderlookup " +
			 "WHERE rider_code = '" + planCode + "' " +
			   "AND ind = 1";
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		boolean isExisting = false;
		Connection conn = null;
		
		try {
			conn = getConnection();	
			Statement s = conn.createStatement();
			rs = s.executeQuery(sql);
			if (rs != null) {
				while (rs.next()) {
					if (rs.getInt("ridercount") > 0) {
						isExisting =  true;
						break;
					}
				}
			}
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));	
			throw e;
		} finally {
			closeResources(conn, ps, rs);
		}
		LOGGER.info("isRiderLookup end");
		return isExisting;
	}
}
