/*
 * Created on Jun 23, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.data.dao.test;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;

import junit.framework.TestCase;

import com.slocpi.ium.data.ClientData;
import com.slocpi.ium.data.dao.ClientDAO;
import com.slocpi.ium.data.util.DataSourceProxy;

/**
 * @author cgironella
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class ClientDAOTest extends TestCase {

	/**
	 * Constructor for ClientDAOTest.
	 * @param arg0
	 */
	public ClientDAOTest(String arg0) {
		super(arg0);
	}

	public static void main(String[] args) {
	}

	/*
	 * @see TestCase#setUp()
	 */
	protected void setUp() throws Exception {
		super.setUp();
	}

	/*
	 * @see TestCase#tearDown()
	 */
	protected void tearDown() throws Exception {
		super.tearDown();
	}

	public void testClientDAO() {
		//TODO Implement ClientDAO().
	}

	/*
	 * Test for ClientData retrieveClient(String, String, String)
	 */
	public void testRetrieveClient() throws SQLException {
		
		ClientDAO testDAO = new ClientDAO();
		ClientData resultSet = new ClientData();
		
		//testing with data that is in the database
		String testFirstName = "FN";
		String testLastName = "LN";
		String testMI = "MN";
		
		resultSet = testDAO.retrieveClient(testLastName, testFirstName, testMI);
		
		assertEquals("LN", resultSet.getLastName());
		assertEquals("FN", resultSet.getGivenName());
		
		/*using other attributes in an assertEquals statement with this particular
		data (LN, FN) may result in an error if the first name + last name + middle name
		 occurs more than once in the database;
		 the method will get the details of the first instance that it
		comes across data matching these parameters*/
		
		//testing with data that is not in the database
		resultSet = testDAO.retrieveClient("Gironella", "Cris", "A");
		assertNull(resultSet);
		
		//test with mixed data (First name not in database, last name in database)
		resultSet = testDAO.retrieveClient("Villanueva", "Cris", "A");
		assertNull(resultSet);
			
	}

//this method is issuing an "ORA 01861: Literal does not match format string" error
	public void testRetrieveClientData() throws SQLException {
	/*	Connection conn = new DataSourceProxy().getConnection();
		ClientDAO testDAO = new ClientDAO(conn);
		ClientData resultSet = new ClientData();
		
		resultSet = testDAO.retrieveClientData("Aguilera","Christina", "1982-09-15");
		
		assertEquals("C26", resultSet.getClientId());
	}*/
	}
	
	/*
	 * Test for ClientData retrieveClient(String)
	 */
	public void testRetrieveClientString() throws SQLException {
		
		ClientDAO testDAO =  new ClientDAO();
		ClientData resultSet =  new ClientData();
		
		//for an existing client ID
		resultSet =  testDAO.retrieveClient("C26");
		
		String firstName =  resultSet.getGivenName();
		String lastName = resultSet.getLastName();
		Date birthDate =  resultSet.getBirthDate();
		
		assertEquals("Christina", firstName);
		assertEquals("Aguilera", lastName);
		assertEquals("1982-09-15", birthDate.toString());
		
		//for a non-existing client ID
		assertNull(testDAO.retrieveClient("Cris"));
		
		//for an existing Client ID in different lettercase
		assertNotNull(testDAO.retrieveClient("insured"));	
		resultSet =  testDAO.retrieveClient("insured");
		firstName =  resultSet.getGivenName();
		lastName = resultSet.getLastName();
		birthDate =  resultSet.getBirthDate();
		assertEquals("Marvin", firstName);
		assertEquals("Marquez", lastName);
		assertEquals("1977-02-01", birthDate.toString());
		}

	public void testInsertClient() {
		//TODO Implement insertClient().
	}

	public void testUpdateClient() {
		//TODO Implement updateClient().
	}

	public void testSelectClients() throws SQLException {
		
		ArrayList resultSet = new ArrayList();
		ClientDAO testDAO = new ClientDAO();
		resultSet = testDAO.selectClients();
		int numberOfClients =  resultSet.size();
		assertEquals(81, numberOfClients);
		for (int i=0; i<numberOfClients; i++)
		  {
		  	String clientID = ((ClientData)resultSet.get(i)).getClientId();
		  	System.out.println(clientID);
		  }
		
	}

	public void testIsClientExists() throws SQLException {
		
		ClientDAO testDAO = new ClientDAO();
		ClientData resultSet = new ClientData();
		
		//testing with a client ID that exists in database
		assertTrue(testDAO.isClientExists("WHATEVER"));
		
		//testing with a client ID that does not exist in database
		//use assertTrue with a ! in the condition tested
		assertTrue(!(testDAO.isClientExists("Cris")));
		
		//test with different case clientId
		//use assertTrue with a ! in the condition tested
		assertTrue(!(testDAO.isClientExists("whatever")));
	}

	public void testUpdateClientInfo() {
		//TODO Implement updateClientInfo().
	}

}
