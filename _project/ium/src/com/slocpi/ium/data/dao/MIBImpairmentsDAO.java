package com.slocpi.ium.data.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.slocpi.ium.data.MIBImpairmentsData;
import com.slocpi.ium.util.CodeHelper;

public class MIBImpairmentsDAO {

	private static final Logger BASE_LOGGER = LoggerFactory.getLogger(MIBImpairmentsCHDao.class);

	private Connection conn = null;


	public MIBImpairmentsDAO(Connection connection) {
		super();
		this.conn = connection;
	}
	
	private void closeResources(PreparedStatement ps, ResultSet rs) throws SQLException{
		
		
		try {
			if (ps != null){
				ps.close();
			}
		}
		catch(SQLException e)
		{
			BASE_LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		}
		try {
			if (rs != null){
				rs.close();
			}			
		} catch (SQLException e){
			BASE_LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		}
		
	}
	
	public ArrayList retrieveMIBImpairments(String code, String desc) throws Exception {
		
		BASE_LOGGER.info("retrieveMIBImpairments start");
		ArrayList list = new ArrayList();
		
		Statement stmt = null;
		ResultSet rs = null;		

		StringBuffer sb = new StringBuffer("SELECT mib_impairment_code, mib_impairment_desc FROM mib_impairments");
		
		if((code!=null && !code.equals("")) || (desc!=null && !desc.equals(""))){
			sb = sb.append(" where ");
			
			if(code!=null && !code.equals("")){
				sb = sb.append(" upper(mib_impairment_code) like '%");
				sb = sb.append(code.toUpperCase());
				sb = sb.append("%'");
			}
			
			if(code!=null && !code.equals("") && desc!=null && !desc.equals("")){
				sb = sb.append(" and ");
			}
			
			if(desc!=null && !desc.equals("")){
				sb = sb.append(" upper(mib_impairment_desc) like '%");
				sb = sb.append(desc.toUpperCase());
				sb = sb.append("%'");
			}
		}
		sb = sb.append(" ORDER BY mib_impairment_code");
		 
		try {
			
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sb.toString());
			
			while (rs.next()) {
				
				MIBImpairmentsData data = new MIBImpairmentsData();
				data.setCode(rs.getString("mib_impairment_code"));
				data.setDesc(rs.getString("mib_impairment_desc"));
				list.add(data);	
		
			} 
		} catch (SQLException e) {
			BASE_LOGGER.error(CodeHelper.getStackTrace(e));
			  throw e; 
		}finally{
			  this.closeResources(null, rs);
			  if(stmt!=null){
				  stmt.close();
			  }
		}
		
		BASE_LOGGER.info("retrieveMIBImpairments end");
		return (list);	
	}
	
	
}
