package com.slocpi.ium.data.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Collection;
import java.util.ResourceBundle;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.IUMConstants;


/**
 * This class will be a common utility class that
 * will be used to create collections of name-value
 * pairs that will primarily be used for populating
 * drop down lists for the user interface.
 * This class will be a "factory" type of a class.
 * Based on the code set name that is requested,
 * the appropriated DAO will be instantiated to
 * return the requested code values.
 *
 * @version v0.01
 * @author  rposadas Feb 22, 2003
 *
 */
public abstract class CodeHelperDao {

	private static final Logger BASE_LOGGER = LoggerFactory.getLogger(CodeHelperDao.class);

	/**
	 * Constructor for CodeHelperDao.
	 * @param inTableName
	 */
	public CodeHelperDao() {
		  super();
	 }

	/**
	 * Method getCodeValues.
	 * This method will return all the values for the request code set.
	 * 
	 * @return Collection
	 */
	public abstract Collection getCodeValues() throws SQLException ;

	/**
	 * Method getCodeValue.
	 * This method will return a subset of the requested code set based on the specified code value.
	 * 
	 * @param codeValue
	 * @return Collection
	 */
	public abstract Collection getCodeValue(String codeValue) throws SQLException ;
	
	public static Connection getConnection() throws SQLException{
		
	
		Connection connection = null;														
		
		ResourceBundle rb = ResourceBundle.getBundle(IUMConstants.IUM_CONFIG);
		String db_location = rb.getString(IUMConstants.IUM_DBLOCATION);
		
		boolean tryManual = false;
       	
       	try {
       		InitialContext ic = new InitialContext(); 
			DataSource ds = (DataSource) ic.lookup(db_location);
			connection = ds.getConnection();
			
       	}catch (NamingException e) {
       		BASE_LOGGER.error(CodeHelper.getStackTrace(e));
       		tryManual = true;
       	} catch (Exception e) {
       		BASE_LOGGER.error(CodeHelper.getStackTrace(e));
		}
       	
       	if (tryManual) {
        	BASE_LOGGER.warn("****************************ST DEV SET-UP. NO DATASOURCE CONFIGURATION******************************");
       		BASE_LOGGER.warn("****************************START MANUAL CONNECTION ON DATABASE*************************************");
       		try {
	    		String conn_driver = rb.getString("connection-driver");
	    		String url = rb.getString("url");
	    		String username = rb.getString("username");
	    		String password = rb.getString("password");
	    		Class.forName(conn_driver);
	    		connection = DriverManager.getConnection(url, username, password);
       		} catch (ClassNotFoundException e) {
       			BASE_LOGGER.error(CodeHelper.getStackTrace(e));
       			connection = null;
       		}
       	}
	
       	if (connection==null) {
       		BASE_LOGGER.warn("No Connection created");
			throw new SQLException("No Connection created");
		}
       	
       	BASE_LOGGER.info("getConnectionConfig end");
		return connection;			
	}
	
	
	public static void closeConnection(Connection conn) throws SQLException{
		try {
			if (conn != null) {
				conn.close();
			}
		}  catch (SQLException e){
			BASE_LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		}
	}
	
	public void closeResources(PreparedStatement ps, ResultSet rs) throws SQLException{
		
		try {
			if (rs != null){
				rs.close();
			}			
		} catch (SQLException e){
			BASE_LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		}
		
		try {
			if (ps != null){
				ps.close();
			}
		}
		catch(SQLException e)
		{
			BASE_LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		}
		
	}
	
	public void closeResources(Statement stmt, ResultSet rs) throws SQLException{
		
		try {
			if (rs != null){
				rs.close();
			}			
		} catch (SQLException e){
			BASE_LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		}
		
		try {
			if (stmt != null){
				stmt.close();
			}
		}
		catch(SQLException e)
		{
			BASE_LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		}
		
	}	
	
	public void closeResources(Connection conn, PreparedStatement ps, ResultSet rs) throws SQLException{
		
		try {
			if (rs != null){
				rs.close();
			}			
		} catch (SQLException e){
			BASE_LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		}
		
		try {
			if (ps != null){
				ps.close();
			}
		}
		catch(SQLException e)
		{
			BASE_LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		}

		closeConnection(conn);
		
	}
	
	public void closeResources(Connection conn, Statement stmt, ResultSet rs) throws SQLException{
		
		try {
			if (rs != null){
				rs.close();
			}			
		} catch (SQLException e){
			BASE_LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		}
		
		try {
			if (stmt != null){
				stmt.close();
			}
		}
		catch(SQLException e)
		{
			BASE_LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		}

		closeConnection(conn);
		
	}


}
