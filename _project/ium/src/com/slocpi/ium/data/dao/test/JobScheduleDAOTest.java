/*
 * Created on Jun 24, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.data.dao.test;

import java.sql.Connection;
import java.sql.SQLException;

import junit.framework.TestCase;

import com.slocpi.ium.data.JobScheduleData;
import com.slocpi.ium.data.dao.JobScheduleDAO;
import com.slocpi.ium.data.util.DataSourceProxy;

/**
 * @author Jov Tuplano
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class JobScheduleDAOTest extends TestCase {

	/**
	 * Constructor for JobScheduleDAOTest.
	 * @param arg0
	 */
	public JobScheduleDAOTest(String arg0) {
		super(arg0);
	}

	public static void main(String[] args) {
	}

	final public void testJobScheduleDAO() {
		//TODO Implement JobScheduleDAO().
		
	}

	final public void testSaveJobSchedule() throws SQLException {
		//TODO Implement saveJobSchedule().
		Connection con= new DataSourceProxy().getConnection();
		JobScheduleDAO job= new JobScheduleDAO(con);
		JobScheduleData jobData= new JobScheduleData();
		jobData.setCreatedBy("IUMDEV");
		jobData.setUpdatedBy("IUMDEV");
		jobData.setType("ME");
		jobData.setDescription("8pm sked");
		job.saveJobSchedule(jobData);
		//checks if saved
		assertNotNull(jobData);
		
		
		
	}

	final public void testRetrieveJobSchedule() throws SQLException {
		//TODO Implement retrieveJobSchedule().
		Connection con2= new DataSourceProxy().getConnection();
		JobScheduleDAO job2= new JobScheduleDAO(con2);
		JobScheduleData jobSked= new JobScheduleData();
		String type1= new String("ME");
		jobSked.setDescription("8pm sked");
		jobSked.setCreatedBy("IUMDEV");
		jobSked.setUpdatedBy("IUMDEV");
		job2.retrieveJobSchedule(type1);
		
		//checks if data is retrieved
		assertNotNull(job2.retrieveJobSchedule(type1));
		assertEquals("8pm sked", jobSked.getDescription());
		assertEquals("IUMDEV", jobSked.getCreatedBy());
				
		
	}

}
