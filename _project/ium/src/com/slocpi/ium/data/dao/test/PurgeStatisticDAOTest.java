/*
 * Created on Jun 25, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.data.dao.test;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Date;

import junit.framework.TestCase;

import com.slocpi.ium.data.PurgeStatisticData;
import com.slocpi.ium.data.dao.PurgeStatisticDAO;
import com.slocpi.ium.data.util.DataSourceProxy;

/**
 * @author mlua
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class PurgeStatisticDAOTest extends TestCase {

	/**
	 * Constructor for PurgeStatisticDAOTest.
	 * @param arg0
	 */
	private Connection conn = null;
	public PurgeStatisticDAOTest(String arg0) {
		super(arg0);
	}

	public void testInsertPurgeStatistic() throws SQLException {
		conn = (Connection)new DataSourceProxy().getConnection();
		PurgeStatisticDAO purge = new PurgeStatisticDAO(conn);
		PurgeStatisticData data = new PurgeStatisticData();
		Date sdate = new Date("6/25/2004");
		Date edate = new Date("6/25/2004");
		Date pdate = new Date("6/25/2004");
		
		data.setCriteria("Activity Logs");
		data.setEndDate(edate);
		data.setStartDate(sdate);
		data.setPurgeDate(pdate);
		data.setResult("Activity Logs");
		data.setNumberOfRecords(5);
		
		purge.insertPurgeStatistic(data);
		
		
	}

}
