package com.slocpi.ium.data.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.slocpi.ium.data.RankData;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.DateHelper;

/**
 * @author jcristobal
 *
 * To change this generated comment edit the template variable "typecomment":
 * Window>Preferences>Java>Templates.
 * To enable and disable the creation of type comments go to
 * Window>Preferences>Java>Code Generation.
 */
public class RanksDAO {

	private static final Logger LOGGER = LoggerFactory.getLogger(RanksDAO.class);
	private Connection conn = null;
	
	
	/* Constructor created to remove connection parameter */
	public RanksDAO() {
		
	}
	
	
	/**
	 * Constructor for RanksDAO.
	 */
	public RanksDAO(Connection connection) {
		super();
		this.conn = connection;
	}
	private void closeResources(PreparedStatement ps, ResultSet rs) throws SQLException{
		
		
		try {
			if (ps != null){
				ps.close();
			}
		}
		catch(SQLException e)
		{
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		}
		try {
			if (rs != null){
				rs.close();
			}			
		} catch (SQLException e){
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		}
		
	}
	public RankData retrieveRankData(long rankId) throws SQLException{		
		
		LOGGER.info("retrieveRankData start");
		String sql = "SELECT RANK_ID, RANK_DESC, RANK_FEE " +
					 "FROM RANKS " +
					 "WHERE RANK_ID = ?" +
					 "ORDER BY RANK_ID ";
		PreparedStatement ps = null;
		ResultSet rs = null;
		RankData rankData = null;
		try{
		ps = conn.prepareStatement(sql);
		ps.setLong(1, rankId);
		rs = ps.executeQuery();
		if (rs.next()) {
			rankData = new RankData();
			rankData.setRankCode(rs.getLong("RANK_ID"));
			rankData.setRankDesc(rs.getString("RANK_DESC"));
			rankData.setRankFee(rs.getDouble("RANK_FEE"));
		}
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			  throw e; 
		}finally{
			  this.closeResources(ps, rs);
		}	
		LOGGER.info("retrieveRankData end");
		return rankData;
	}

	public ArrayList retrieveRanks() throws SQLException {
		
		LOGGER.info("retrieveRanks start");
		String sql = "SELECT RANK_ID, RANK_DESC, RANK_FEE " +
					 "FROM RANKS " +
					 "ORDER BY RANK_ID ";
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList list = new ArrayList();
		try{
		ps = conn.prepareStatement(sql);
		rs = ps.executeQuery();

		
		while (rs.next()) {
			RankData data = new RankData();
			data.setRankCode(rs.getLong("RANK_ID"));
			data.setRankDesc(rs.getString("RANK_DESC"));
			data.setRankFee(rs.getDouble("RANK_FEE"));
			list.add(data);
		}
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			  throw e; 
		}finally{
			  this.closeResources(ps, rs);
		}	
		LOGGER.info("retrieveRanks end");
		return (list);	
	}//retrieveRanks


	public void insertRank(RankData data) throws SQLException {
		
		LOGGER.info("insertRank start");
		String sql = "INSERT INTO RANKS (RANK_ID, RANK_DESC, RANK_FEE, CREATED_BY, CREATED_DATE) " +
					 "VALUES(?,?,?,?,?)";

		PreparedStatement ps = null;
		ResultSet rs = null;
		try{
			
			ps = conn.prepareStatement(sql);

			long code = data.getRankCode(); 
			ps.setLong(1, code);

			String desc = data.getRankDesc(); 
			if (desc != null) {
				ps.setString(2, desc);
			} else {
				ps.setString(2, null);
			}

			double fee = data.getRankFee(); 
			ps.setDouble(3, fee);
			
			String createdBy = data.getCreatedBy(); 
			if (createdBy != null) {
				ps.setString(4, createdBy);
			} else {
				ps.setString(4, null);
			}

			ps.setTimestamp(5, DateHelper.sqlTimestamp(data.getCreateDate()));
									
			ps.executeUpdate();
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			  throw e; 
		}finally{
			  this.closeResources(ps, rs);
		}	
		LOGGER.info("insertRank end");
		}//insertRank


	public void updateRank(RankData data) throws SQLException{	
		
		LOGGER.info("updateRank start");
		String sql = "UPDATE RANKS " +
					 "SET RANK_DESC = ?, RANK_FEE = ?, UPDATED_BY = ?, UPDATED_DATE = ? " +
					 "WHERE RANK_ID = ?";
		PreparedStatement ps = null;
		ResultSet rs = null;
		try{
			
			ps = conn.prepareStatement(sql);
			ps.setString(1, data.getRankDesc());
			ps.setDouble(2, data.getRankFee());			
			ps.setString(3, data.getUpdatedBy());
			ps.setTimestamp(4, DateHelper.sqlTimestamp(data.getUpdateDate()));
			ps.setLong(5, data.getRankCode());										
			ps.executeUpdate();
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			  throw e; 
		}finally{
			  this.closeResources(ps, rs);
		}	
		LOGGER.info("updateRank end");
	}//updateRank
		

}
