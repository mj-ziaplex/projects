/*
 * Created on Dec 18, 2003
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.data.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.slocpi.ium.IUMException;
import com.slocpi.ium.data.RolesData;
import com.slocpi.ium.data.UserLoadData;
import com.slocpi.ium.data.UserPasswords;
import com.slocpi.ium.data.UserProcessPrefData;
import com.slocpi.ium.data.UserProfileData;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.DateHelper;
import com.slocpi.ium.util.IUMConstants;
import com.slocpi.ium.data.dao.BaseDAO;


/**
 * @author Cris
 *
 * Container for SQLs performed on a user.
 */
public class UserDAO extends BaseDAO{
	private static final Logger LOGGER = LoggerFactory.getLogger(UserDAO.class);
	
	/**
	 *
	 */
	

	public UserDAO() {}

	/**
	 * This method checks if a user is valid by checking its user id and password.
	 * @param userId
	 * @param password
	 * @return
	 * @throws SQLException
	 */
	public boolean isUserValid(String userId, String password) throws SQLException {
		
		LOGGER.info("isUserValid start 1");
		boolean res = false;
		String sql = "SELECT * FROM USERS WHERE USER_ID=? and USR_PASSWORD = ENCRYPTDATA(?, " + IUMConstants.KEY + ")";
		PreparedStatement stmt = null;
		ResultSet rs = null;
		Connection conn = null;
		
		try{
			conn = getConnection();
			stmt = conn.prepareStatement(sql);
			stmt.setString(1,userId);
			stmt.setString(2,password);
			rs = stmt.executeQuery();
			if (rs.next()) {
		
				res = true;
			}
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			  throw e; 
		}finally{
			  closeResources(conn, stmt, rs);
		}		
		LOGGER.info("isUserValid end 1");
		return res;
	}

	/**
	 * This method checks if a user is valid by checking its user id and password.
	 * @param userId
	 * @param password
	 * @return
	 * @throws SQLException
	 */
	public boolean isUserValid(String userId) throws SQLException {
		
		LOGGER.info("isUserValid start 2");
		boolean res = false;
		String sql = "SELECT USER_ID FROM USERS WHERE USER_ID=?";
		PreparedStatement stmt = null;
		ResultSet rs = null;
		Connection conn = null;
		
		try{
		conn = getConnection();
		stmt = conn.prepareStatement(sql);
		stmt.setString(1,userId);
		rs = stmt.executeQuery();

		if (rs.next()) {
			res = true;
		}
		
		} catch (SQLException e) {
			  LOGGER.error(CodeHelper.getStackTrace(e));
			  throw e; 
		}finally{
			closeResources(conn, stmt, rs);
		}		
		LOGGER.info("isUserValid end 2");
		return res;
	}	
	
	/**
	 * This method retrieves a set of user/s satisfying the given parameter.
	 * @param filter
	 * @return
	 * @throws Exception
	 */
	public ArrayList selectUserProfiles(UserProfileData filter) throws Exception {
		
		LOGGER.info("selectUserProfiles start");
		ArrayList userProfiles = new ArrayList();
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT USER_ID, USR_ACF2ID, USR_PASSWORD, USR_LAST_NAME, USR_FIRST_NAME") ;
 		sql.append(", USR_MIDDLE_NAME, USR_CODE, USR_TYPE, USR_EMAIL_ADDRESS, USR_MOBILE_NUM");
		sql.append(", USR_NOTIFICATION_IND, USR_RECORDS_PER_VIEW, USR_SEX, USR_ROLE, SLO_ID");
		sql.append(", DEPT_ID, SEC_ID, USR_CONTACT_NUM, USR_ADDRESS1, USR_ADDRESS2, USR_ADDRESS3");
		sql.append(", USR_COUNTRY, USR_CITY, USR_ZIPCODE, USR_LOCK_IND, USR_ACTIVE_IND");
		sql.append("  FROM USERS");
		sql.append(this.parseWhereClause(filter));
		sql.append(" ORDER BY USER_ID, USR_LAST_NAME, USR_FIRST_NAME, USR_ACF2ID");
		Statement stmt = null;
		ResultSet rs = null;
		Connection conn = getConnection();
		try {
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql.toString());
			while (rs.next()) {
				UserProfileData userProfile = new UserProfileData();
				userProfile.setUserId(rs.getString("USER_ID"));
				userProfile.setACF2ID(rs.getString("USR_ACF2ID"));
				userProfile.setPassword(rs.getString("USR_PASSWORD"));
				userProfile.setLastName(rs.getString("USR_LAST_NAME"));
				userProfile.setFirstName(rs.getString("USR_FIRST_NAME"));
				userProfile.setMiddleName(rs.getString("USR_MIDDLE_NAME"));
				userProfile.setUserCode(rs.getString("USR_CODE"));
				userProfile.setUserType(rs.getString("USR_TYPE"));
				userProfile.setEmailAddr(rs.getString("USR_EMAIL_ADDRESS"));
				userProfile.setMobileNumber(rs.getString("USR_MOBILE_NUM"));
				userProfile.setNotificationInd(rs.getBoolean("USR_NOTIFICATION_IND"));
				userProfile.setRecordsPerView(rs.getInt("USR_RECORDS_PER_VIEW"));
				userProfile.setSex(rs.getString("USR_SEX"));
				userProfile.setRole(rs.getString("USR_ROLE"));
				userProfile.setOfficeCode(rs.getString("SLO_ID"));
				userProfile.setDeptCode(rs.getString("DEPT_ID"));
				userProfile.setSectionCode(rs.getString("SEC_ID"));
				userProfile.setContactNum(rs.getString("USR_CONTACT_NUM"));
				userProfile.setAddress1(rs.getString("USR_ADDRESS1"));
				userProfile.setAddress2(rs.getString("USR_ADDRESS2"));
				userProfile.setAddress3(rs.getString("USR_ADDRESS3"));
				userProfile.setCountry(rs.getString("USR_COUNTRY"));
				userProfile.setCity(rs.getString("USR_CITY"));
				userProfile.setZipCode(rs.getString("USR_ZIPCODE"));
				userProfile.setLock(rs.getBoolean("USR_LOCK_IND"));
				userProfile.setActive(rs.getBoolean("USR_ACTIVE_IND"));
				userProfiles.add(userProfile);
			}
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			  throw e; 
		}finally{
			closeResources(conn, stmt, rs);
		}		
		LOGGER.info("selectUserProfiles end");
		return userProfiles;

	}
	/**
	 * This method parses the string parameter to produce a valid WHERE clause.
	 * @param filter
	 * @return
	 */

	private String parseWhereClause(UserProfileData filter) {
		
		LOGGER.info("parseWhereClause start");
		StringBuffer whereClause = new StringBuffer();
		if (filter.getUserId() != null && !filter.getUserId().equalsIgnoreCase("")) {
			whereClause.append(" WHERE UPPER(USER_ID) LIKE UPPER('%" + filter.getUserId() + "%')");
		}
		if (filter.getACF2ID() != null && !filter.getACF2ID().equalsIgnoreCase("")) {
			if (whereClause.length() > 0) {
				whereClause.append(" AND UPPER(USR_ACF2ID) LIKE UPPER('%" + filter.getACF2ID() + "%')");
			} else {
				whereClause.append(" WHERE UPPER(USR_ACF2ID) LIKE UPPER('%" + filter.getACF2ID() + "%')");
			}
		}
		if (filter.getLastName() != null && !filter.getLastName().equalsIgnoreCase("")) {
			if (whereClause.length() > 0) {
				whereClause.append(" AND UPPER(USR_LAST_NAME) LIKE UPPER('%" + filter.getLastName() + "%')");
			} else {
				whereClause.append(" WHERE UPPER(USR_LAST_NAME) LIKE UPPER('%" + filter.getLastName() + "%')");
			}
		}
		if (filter.getDeptCode() != null && !filter.getDeptCode().equalsIgnoreCase("")) {
			if (whereClause.length() > 0) {
				whereClause.append(" AND DEPT_ID = '" + filter.getDeptCode() + "'");
			} else {
				whereClause.append(" WHERE DEPT_ID = '" + filter.getDeptCode() + "'");
			}
		}
		if (filter.getOfficeCode() != null && !filter.getOfficeCode().equalsIgnoreCase("")) {
			if (whereClause.length() > 0  || filter.getOfficeCode().equalsIgnoreCase("")) {
				whereClause.append(" AND SLO_ID = '" + filter.getOfficeCode() + "'");
			} else {
				whereClause.append(" WHERE SLO_ID = '" + filter.getOfficeCode() + "'");
			}
		}
		LOGGER.info("parseWhereClause end");
		return whereClause.toString();
	}

	/**
	 * This mehtod inserts a new user profile into the DB.
	 * @param userProfile
	 * @throws SQLException
	 */
	public void insertUserProfile (UserProfileData userProfile) throws SQLException{
		
		LOGGER.info("insertUserProfile start");
		String sql = "INSERT INTO USERS " +
			"(USER_ID, USR_ACF2ID, USR_PASSWORD " +
			" ,USR_LAST_NAME, USR_FIRST_NAME, USR_MIDDLE_NAME, USR_CODE, USR_TYPE, USR_EMAIL_ADDRESS " +
			" ,USR_MOBILE_NUM, USR_NOTIFICATION_IND, USR_RECORDS_PER_VIEW, USR_SEX, USR_ROLE, SLO_ID " +
			" ,DEPT_ID, SEC_ID, CREATED_BY, CREATED_DATE, UPDATED_BY, UPDATED_DATE " +
			" ,USR_CONTACT_NUM, USR_ADDRESS1, USR_ADDRESS2, USR_ADDRESS3 " +
			" ,USR_COUNTRY, USR_CITY, USR_ZIPCODE, USR_LOCK_IND, USR_ACTIVE_IND, USR_1LOGIN_IND) " +
			" VALUES(?,?,ENCRYPTDATA(?, " + IUMConstants.KEY + "),?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

		PreparedStatement prepStmt = null;
		ResultSet rs = null;
		Connection conn = null;
		try{
			conn = getConnection();
			prepStmt = conn.prepareStatement(sql);
			prepStmt.setString(1,userProfile.getUserId());
			prepStmt.setString(2,userProfile.getACF2ID());

			if (userProfile.getPassword() != null){
				prepStmt.setString(3,userProfile.getPassword());
			} else {
				prepStmt.setString(3," ");
			}
			
			prepStmt.setString(4,userProfile.getLastName());
			prepStmt.setString(5,userProfile.getFirstName());
			prepStmt.setString(6,userProfile.getMiddleName());
			prepStmt.setString(7,userProfile.getUserCode());
			prepStmt.setString(8,userProfile.getUserType());
			prepStmt.setString(9,userProfile.getEmailAddr());
			prepStmt.setString(10,userProfile.getMobileNumber());
			prepStmt.setBoolean(11,userProfile.hasNotification());
			prepStmt.setLong(12,userProfile.getRecordsPerView());
			prepStmt.setString(13,userProfile.getSex());
			prepStmt.setString(14,userProfile.getRole());
			prepStmt.setString(15,userProfile.getOfficeCode());
			prepStmt.setString(16,userProfile.getDeptCode());
			prepStmt.setString(17,userProfile.getSectionCode());
			prepStmt.setString(18,userProfile.getCreatedBy());
			prepStmt.setDate(19,DateHelper.sqlDate(userProfile.getCreateDate()));
			prepStmt.setString(20,userProfile.getUpdatedBy());
			prepStmt.setDate(21,DateHelper.sqlDate(userProfile.getUpdateDate()));
			prepStmt.setString(22,userProfile.getContactNum());
			prepStmt.setString(23,userProfile.getAddress1());
			prepStmt.setString(24,userProfile.getAddress2());
			prepStmt.setString(25,userProfile.getAddress3());
			prepStmt.setString(26,userProfile.getCountry());
			prepStmt.setString(27,userProfile.getCity());
			prepStmt.setString(28,userProfile.getZipCode());
			prepStmt.setBoolean(29,userProfile.isLock());
			prepStmt.setBoolean(30,userProfile.isActive());
			prepStmt.setString(31,"0");
			prepStmt.executeUpdate();
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			  throw e; 
		}finally{
			closeResources(conn, prepStmt, rs);
		}				
		LOGGER.info("insertUserProfile end");
	}
	
	// Launch IUM Enhancement - Andre Ceasar Dacanay - start
	/**
	 * This method retrieves the profile of a user based on its id.
	 * @param userId
	 * @return
	 * @throws SQLException
	 */
	public UserProfileData selectUserProfile(String userId, HttpServletRequest request) throws SQLException {
		
		LOGGER.info("selectUserProfile start 1");
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT USER_ID, USR_ACF2ID, USR_PASSWORD, USR_LAST_NAME, USR_FIRST_NAME") ;
		sql.append(", USR_MIDDLE_NAME, USR_CODE, USR_TYPE, USR_EMAIL_ADDRESS, USR_MOBILE_NUM");
		sql.append(", USR_NOTIFICATION_IND, USR_RECORDS_PER_VIEW, USR_SEX, USR_ROLE, SLO_ID");
		sql.append(", DEPT_ID, SEC_ID, USR_CONTACT_NUM, USR_ADDRESS1, USR_ADDRESS2, USR_ADDRESS3");
		sql.append(", USR_COUNTRY, USR_CITY, USR_ZIPCODE, USR_LOCK_IND, USR_ACTIVE_IND, USR_1LOGIN_IND, PSWD_UPDATE_DATE");
		sql.append("  FROM USERS");
		sql.append("  WHERE USER_ID = ?");
		
		UserProfileData userProfile = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		Connection conn = null;
		try{
			conn = getConnection();
			stmt = conn.prepareStatement(sql.toString());
			stmt.setString(1,userId);
			rs = stmt.executeQuery();
			while (rs.next()){
				userProfile = new UserProfileData();
				userProfile.setUserId(rs.getString("user_id"));
				userProfile.setACF2ID(rs.getString("usr_acf2id"));
				userProfile.setPassword(rs.getString("usr_password"));
				//userProfile.setPassword(IUMDataCrypto.decode(rs.getString("usr_password"))); replace by the encrypt function of the db
				userProfile.setLastName(rs.getString("usr_last_name"));
				userProfile.setFirstName(rs.getString("usr_first_name"));
				userProfile.setMiddleName(rs.getString("usr_middle_name"));
				userProfile.setUserCode(rs.getString("usr_code"));
				userProfile.setUserType(rs.getString("usr_type"));
				userProfile.setEmailAddr(rs.getString("usr_email_address"));
				userProfile.setMobileNumber(rs.getString("usr_mobile_num"));
				userProfile.setNotificationInd(rs.getBoolean("usr_notification_ind"));
				userProfile.setRecordsPerView(rs.getInt("usr_records_per_view"));
				userProfile.setSex(rs.getString("usr_sex"));
				userProfile.setRole(rs.getString("usr_role"));
				userProfile.setOfficeCode(rs.getString("slo_id"));
				userProfile.setDeptCode(rs.getString("dept_id"));
				userProfile.setSectionCode(rs.getString("sec_id"));
				userProfile.setContactNum(rs.getString("USR_CONTACT_NUM"));
				userProfile.setAddress1(rs.getString("USR_ADDRESS1"));
				userProfile.setAddress2(rs.getString("USR_ADDRESS2"));
				userProfile.setAddress3(rs.getString("USR_ADDRESS3"));
				userProfile.setCountry(rs.getString("USR_COUNTRY"));
				userProfile.setCity(rs.getString("USR_CITY"));
				userProfile.setZipCode(rs.getString("USR_ZIPCODE"));
				userProfile.setLock(rs.getBoolean("USR_LOCK_IND"));
				userProfile.setActive(rs.getBoolean("USR_ACTIVE_IND"));
				userProfile.setLogin_ind(rs.getString("USR_1LOGIN_IND"));
				userProfile.setPswd_update_date(rs.getTimestamp("PSWD_UPDATE_DATE"));

			}
		
	} catch (SQLException e) {
		LOGGER.error(CodeHelper.getStackTrace(e));
		  throw e; 
	}finally{
		closeResources(conn, stmt, rs);
	}		
		LOGGER.info("selectUserProfile end 1");
		return userProfile;
	}
	// Launch IUM Enhancement - Andre Ceasar Dacanay - end
	/**
	 * This method retrieves the profile of a user based on its id.
	 * @param userId
	 * @return
	 * @throws SQLException
	 */
	public UserProfileData selectUserProfile(String userId) throws SQLException {
		
		LOGGER.info("selectUserProfile start 2");
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT USER_ID, USR_ACF2ID, USR_PASSWORD, USR_LAST_NAME, USR_FIRST_NAME") ;
		sql.append(", USR_MIDDLE_NAME, USR_CODE, USR_TYPE, USR_EMAIL_ADDRESS, USR_MOBILE_NUM");
		sql.append(", USR_NOTIFICATION_IND, USR_RECORDS_PER_VIEW, USR_SEX, USR_ROLE, SLO_ID");
		sql.append(", DEPT_ID, SEC_ID, USR_CONTACT_NUM, USR_ADDRESS1, USR_ADDRESS2, USR_ADDRESS3");
		sql.append(", USR_COUNTRY, USR_CITY, USR_ZIPCODE, USR_LOCK_IND, USR_ACTIVE_IND, USR_1LOGIN_IND, PSWD_UPDATE_DATE");
		sql.append("  FROM USERS");
		sql.append("  WHERE USER_ID = ?");
		
		UserProfileData userProfile = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		Connection conn = null;
		
		try{
			conn = getConnection();
			stmt = conn.prepareStatement(sql.toString());
			stmt.setString(1,userId);
			rs = stmt.executeQuery();
			while (rs.next()){
				userProfile = new UserProfileData();
				userProfile.setUserId(rs.getString("user_id"));
				userProfile.setACF2ID(rs.getString("usr_acf2id"));
				userProfile.setPassword(rs.getString("usr_password"));
				userProfile.setLastName(rs.getString("usr_last_name"));
				userProfile.setFirstName(rs.getString("usr_first_name"));
				userProfile.setMiddleName(rs.getString("usr_middle_name"));
				userProfile.setUserCode(rs.getString("usr_code"));
				userProfile.setUserType(rs.getString("usr_type"));
				userProfile.setEmailAddr(rs.getString("usr_email_address"));
				userProfile.setMobileNumber(rs.getString("usr_mobile_num"));
				userProfile.setNotificationInd(rs.getBoolean("usr_notification_ind"));
				userProfile.setRecordsPerView(rs.getInt("usr_records_per_view"));
				userProfile.setSex(rs.getString("usr_sex"));
				userProfile.setRole(rs.getString("usr_role"));
				userProfile.setOfficeCode(rs.getString("slo_id"));
				userProfile.setDeptCode(rs.getString("dept_id"));
				userProfile.setSectionCode(rs.getString("sec_id"));
				userProfile.setContactNum(rs.getString("USR_CONTACT_NUM"));
				userProfile.setAddress1(rs.getString("USR_ADDRESS1"));
				userProfile.setAddress2(rs.getString("USR_ADDRESS2"));
				userProfile.setAddress3(rs.getString("USR_ADDRESS3"));
				userProfile.setCountry(rs.getString("USR_COUNTRY"));
				userProfile.setCity(rs.getString("USR_CITY"));
				userProfile.setZipCode(rs.getString("USR_ZIPCODE"));
				userProfile.setLock(rs.getBoolean("USR_LOCK_IND"));
				userProfile.setActive(rs.getBoolean("USR_ACTIVE_IND"));
				userProfile.setLogin_ind(rs.getString("USR_1LOGIN_IND"));
				userProfile.setPswd_update_date(rs.getTimestamp("PSWD_UPDATE_DATE"));

			}
		
	} catch (SQLException e) {
		LOGGER.error(CodeHelper.getStackTrace(e));
		  throw e; 
	}finally{
		closeResources(conn, stmt, rs);
	}		
		LOGGER.info("selectUserProfile end 2");
		return userProfile;
	}

	/**
	 * This method retrieves the profile of a user based on its ACF2ID.
	 * @param userId
	 * @return
	 * @throws SQLException
	 */
	public UserProfileData selectUserProfileByACF2ID(String userId) throws SQLException {
		
		LOGGER.info("selectUserProfileByACF2ID start");
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT USER_ID, USR_ACF2ID, USR_PASSWORD, USR_LAST_NAME, USR_FIRST_NAME") ;
		sql.append(", USR_MIDDLE_NAME, USR_CODE, USR_TYPE, USR_EMAIL_ADDRESS, USR_MOBILE_NUM");
		sql.append(", USR_NOTIFICATION_IND, USR_RECORDS_PER_VIEW, USR_SEX, USR_ROLE, SLO_ID");
		sql.append(", DEPT_ID, SEC_ID, USR_CONTACT_NUM, USR_ADDRESS1, USR_ADDRESS2, USR_ADDRESS3");
		sql.append(", USR_COUNTRY, USR_CITY, USR_ZIPCODE, USR_LOCK_IND, USR_ACTIVE_IND");
		sql.append("  FROM USERS");
		sql.append("  WHERE TRIM(USR_ACF2ID)=?");
		UserProfileData userProfile = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		Connection conn = null;
		
		try{
			conn = getConnection();
			stmt = conn.prepareStatement(sql.toString());
			stmt.setString(1,userId);
			rs = stmt.executeQuery();
			while (rs.next()){
				if(userProfile==null){
					userProfile = new UserProfileData();
				}
				userProfile.setUserId(rs.getString("user_id"));
				userProfile.setACF2ID(rs.getString("usr_acf2id"));
				userProfile.setPassword(rs.getString("usr_password"));
				userProfile.setLastName(rs.getString("usr_last_name"));
				userProfile.setFirstName(rs.getString("usr_first_name"));
				userProfile.setMiddleName(rs.getString("usr_middle_name"));
				userProfile.setUserCode(rs.getString("usr_code"));
				userProfile.setUserType(rs.getString("usr_type"));
				userProfile.setEmailAddr(rs.getString("usr_email_address"));
				userProfile.setMobileNumber(rs.getString("usr_mobile_num"));
				userProfile.setNotificationInd(rs.getBoolean("usr_notification_ind"));
				userProfile.setRecordsPerView(rs.getInt("usr_records_per_view"));
				userProfile.setSex(rs.getString("usr_sex"));
				userProfile.setRole(rs.getString("usr_role"));
				userProfile.setOfficeCode(rs.getString("slo_id"));
				userProfile.setDeptCode(rs.getString("dept_id"));
				userProfile.setSectionCode(rs.getString("sec_id"));
				userProfile.setContactNum(rs.getString("USR_CONTACT_NUM"));
				userProfile.setAddress1(rs.getString("USR_ADDRESS1"));
				userProfile.setAddress2(rs.getString("USR_ADDRESS2"));
				userProfile.setAddress3(rs.getString("USR_ADDRESS3"));
				userProfile.setCountry(rs.getString("USR_COUNTRY"));
				userProfile.setCity(rs.getString("USR_CITY"));
				userProfile.setZipCode(rs.getString("USR_ZIPCODE"));
				userProfile.setLock(rs.getBoolean("USR_LOCK_IND"));
				userProfile.setActive(rs.getBoolean("USR_ACTIVE_IND"));
			}
		
	} catch (SQLException e) {
		LOGGER.error(CodeHelper.getStackTrace(e));
		  throw e; 
	}finally{
		closeResources(conn, stmt, rs);
	}		
		LOGGER.info("selectUserProfileByACF2ID end");
		return userProfile;
	}

	/**
	 * This method retrieves a set of users based on the given user role.
	 * @param userRole
	 * @return
	 * @throws SQLException 
	 */
	public ArrayList selectUserProfileByRole (String userRole) throws SQLException{
		
		LOGGER.info("selectUserProfileByRole start");
		ArrayList userProfileList = new ArrayList();
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT USER_ID, USR_ACF2ID, USR_PASSWORD, USR_LAST_NAME, USR_FIRST_NAME") ;
		sql.append(", USR_MIDDLE_NAME, USR_CODE, USR_TYPE, USR_EMAIL_ADDRESS, USR_MOBILE_NUM");
		sql.append(", USR_NOTIFICATION_IND, USR_RECORDS_PER_VIEW, USR_SEX, USR_ROLE, SLO_ID");
		sql.append(", DEPT_ID, SEC_ID, USR_CONTACT_NUM, USR_ADDRESS1, USR_ADDRESS2, USR_ADDRESS3");
		sql.append(", USR_COUNTRY, USR_CITY, USR_ZIPCODE, USR_LOCK_IND, USR_ACTIVE_IND");
		sql.append("  FROM USERS");
		sql.append("  WHERE USR_ROLE = ?");
		PreparedStatement stmt = null;
		ResultSet rs = null;
		Connection conn = null;
		
		try {
			conn = getConnection();
			stmt = conn.prepareStatement(sql.toString());
			stmt.setString(1, userRole);
			rs = stmt.executeQuery();
			while(rs.next()){
				UserProfileData userProfile = new UserProfileData();
				userProfile.setUserId(rs.getString("user_id"));
				userProfile.setACF2ID(rs.getString("usr_acf2id"));
				userProfile.setPassword(rs.getString("usr_password"));
				userProfile.setPassword(rs.getString("usr_password"));
				userProfile.setLastName(rs.getString("usr_last_name"));
				userProfile.setFirstName(rs.getString("usr_first_name"));
				userProfile.setMiddleName(rs.getString("usr_middle_name"));
				userProfile.setUserCode(rs.getString("usr_code"));
				userProfile.setUserType(rs.getString("usr_type"));
				userProfile.setEmailAddr(rs.getString("usr_email_address"));
				userProfile.setMobileNumber(rs.getString("usr_mobile_num"));
				userProfile.setNotificationInd(rs.getBoolean("usr_notification_ind"));
				userProfile.setRecordsPerView(rs.getInt("usr_records_per_view"));
				userProfile.setSex(rs.getString("usr_sex"));
				userProfile.setRole(rs.getString("usr_role"));
				userProfile.setOfficeCode(rs.getString("slo_id"));
				userProfile.setDeptCode(rs.getString("dept_id"));
				userProfile.setSectionCode(rs.getString("sec_id"));
				userProfile.setContactNum(rs.getString("USR_CONTACT_NUM"));
				userProfile.setAddress1(rs.getString("USR_ADDRESS1"));
				userProfile.setAddress2(rs.getString("USR_ADDRESS2"));
				userProfile.setAddress3(rs.getString("USR_ADDRESS3"));
				userProfile.setCountry(rs.getString("USR_COUNTRY"));
				userProfile.setCity(rs.getString("USR_CITY"));
				userProfile.setZipCode(rs.getString("USR_ZIPCODE"));
				userProfile.setLock(rs.getBoolean("USR_LOCK_IND"));
				userProfile.setActive(rs.getBoolean("USR_ACTIVE_IND"));
				userProfileList.add(userProfile);
			}
		
	} catch (SQLException e) {
		LOGGER.error(CodeHelper.getStackTrace(e));
	}finally{
		 closeResources(conn, stmt, rs);
	}	
		LOGGER.info("selectUserProfileByRole end");
		return userProfileList;
	}
	/**
	 * This method returns a UserProcessPrefData that has a null value.
	 * @param userId
	 * @return
	 * @throws SQLException
	 */
	public UserProcessPrefData selectProcessPreference(String userId) throws SQLException {
		LOGGER.info("selectProcessPreference start");
		UserProcessPrefData uppd = null;
		LOGGER.info("selectProcessPreference end");
		return uppd;
	}

	/**
	 * This method updates the profile of a user.
	 * @param user
	 * @throws SQLException
	 */
	public void updateUserProfile(UserProfileData user)throws SQLException{

		LOGGER.info("updateUserProfile start");
		StringBuffer sql =  new StringBuffer();
		sql.append("UPDATE USERS SET");
		sql.append(" USR_ACF2ID=?,");
		sql.append(" USR_LAST_NAME=?,");
		sql.append(" USR_FIRST_NAME=?,");
		sql.append(" USR_MIDDLE_NAME=?,");
		sql.append(" USR_EMAIL_ADDRESS=?,");
		sql.append(" USR_MOBILE_NUM=?,");
		sql.append(" USR_SEX=?,");
		sql.append(" USR_ROLE=?,");
		sql.append(" SLO_ID=?,");
		sql.append(" DEPT_ID=?,");
		sql.append(" SEC_ID=?,");
		sql.append(" UPDATED_BY=?,");
		sql.append(" UPDATED_DATE=?,");
		sql.append(" USR_CONTACT_NUM=?,");
		sql.append(" USR_ADDRESS1=?,");
		sql.append(" USR_ADDRESS2=?,");
		sql.append(" USR_ADDRESS3=?,");
		sql.append(" USR_COUNTRY=?,");
		sql.append(" USR_CITY=?,");
		sql.append(" USR_ZIPCODE=?,");
		sql.append(" USR_LOCK_IND=?,");
		sql.append(" USR_ACTIVE_IND=?,");
		sql.append(" USR_NOTIFICATION_IND=?,");
		sql.append(" USR_RECORDS_PER_VIEW=?");
		sql.append(" WHERE");
		sql.append(" USER_ID=?");
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection conn = null;
		
		try{
		conn = getConnection();
		conn.setAutoCommit(false);
		ps = conn.prepareStatement(sql.toString());
		ps.setString(1, user.getACF2ID());
		ps.setString(2, user.getLastName());
		ps.setString(3, user.getFirstName());
		ps.setString(4, user.getMiddleName());
		ps.setString(5, user.getEmailAddr());
		ps.setString(6, user.getMobileNumber());
		ps.setString(7, user.getSex());
		ps.setString(8, user.getRole());
		ps.setString(9, user.getOfficeCode());
		ps.setString(10, user.getDeptCode());
		ps.setString(11, user.getSectionCode());
		ps.setString(12, user.getUpdatedBy());
		ps.setTimestamp(13, DateHelper.sqlTimestamp(user.getUpdateDate()) );
		ps.setString(14, user.getContactNum());
		ps.setString(15, user.getAddress1());
		ps.setString(16, user.getAddress2());
		ps.setString(17, user.getAddress3());
		ps.setString(18, user.getCountry());
		ps.setString(19, user.getCity());
		ps.setString(20, user.getZipCode());
		ps.setBoolean(21,user.isLock());
		ps.setBoolean(22,user.isActive());
		ps.setBoolean(23, user.hasNotification());
		ps.setInt(24, user.getRecordsPerView());
		ps.setString(25, user.getUserId());
		ps.executeUpdate();
		conn.commit();
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			if(conn != null){
				conn.rollback();
			}
			throw e; 
		}finally{
			closeResources(conn, ps, rs);
		}		
		LOGGER.info("updateUserProfile end");
	}

	/**
	 * This method retrieves the user type of a user.
	 * @param userId
	 * @return
	 */
	public String getType(String userId){
		return userId;
	}

	/**
	 * This method retrieves a set of user load data based on the given roles and status.
	 * @param roles
	 * @param status
	 * @return
	 * @throws SQLException
	 */
	public ArrayList getUserLoadList(ArrayList roles,ArrayList status)throws SQLException{

		LOGGER.info("getUserLoadList start");
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT DISTINCT( US.USER_ID ) AS USER_ID,");
		sql.append(" US.USR_LAST_NAME AS LAST_NAME,");
		sql.append(" US.USR_FIRST_NAME AS FIRST_NAME,");
		sql.append(" US.USR_MIDDLE_NAME AS MIDDLE_NAME,");
		for (int i=0; i<status.size(); i++){
			if(i>0){
				sql.append(",");
			}
			sql.append(" (SELECT COUNT(*) FROM ASSESSMENT_REQUESTS");
			sql.append(" where STATUS_ID=? AND ASSIGNED_TO = US.USER_ID)");
			sql.append(" as status").append((Long)status.get(i));
		}
		sql.append(", (SELECT COUNT(*) FROM ASSESSMENT_REQUESTS");
		sql.append(" where STATUS_ID IN (");
		for (int i=0; i<status.size(); i++){
			if(i!=0){
				sql.append(",");
			}
			sql.append("?");
		}
		sql.append(") AND ASSIGNED_TO = US.USER_ID)");
		sql.append(" as total");
		sql.append(" FROM");
		sql.append(" USERS US,");
		sql.append(" USER_ROLES USR,");
		sql.append(" ASSESSMENT_REQUESTS AR");
		sql.append(" WHERE");
		sql.append(" US.USER_ID = USR.USER_ID AND ");
		sql.append(" (USR.ROLE_CODE IN(");
		for(int i=0; i<roles.size(); i++){
			if(i!=0){
				sql.append(",");
			}
			sql.append("?");
		}
		sql.append(" )) AND");
		sql.append(" US.USER_ID=AR.ASSIGNED_TO (+)");
		sql.append(" ORDER BY UPPER(US.USR_LAST_NAME), UPPER(US.USR_FIRST_NAME), UPPER(US.USR_MIDDLE_NAME)");

		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection conn = null;
		ArrayList result = new ArrayList();
		try{
			
			conn = getConnection();
			ps = conn.prepareStatement(sql.toString());
			int ctr=1;
			
			for(int i=0; i<status.size(); i++){
				ps.setLong( ctr,((Long)status.get(i)).longValue() );
				ctr++;
			}
			
			for(int i=0; i<status.size();i++){
				ps.setLong( ctr,((Long)status.get(i)).longValue() );
				ctr++;
			}
			
			for(int i=0; i<roles.size();i++){
				ps.setString( ctr,(String)roles.get(i) );
				ctr++;
			}
	
			rs = ps.executeQuery();

		
		while(rs.next()){

			UserLoadData userLoad = new UserLoadData();
			userLoad.setUser_id( rs.getString("USER_ID") );
			userLoad.setLast_name( rs.getString("LAST_NAME") );
			userLoad.setFirst_name( rs.getString("FIRST_NAME") );
			userLoad.setMiddle_name( rs.getString("MIDDLE_NAME") );
			HashMap loadByStatus = new HashMap();
			for(int i=0; i<status.size();i++){
				long val = rs.getLong( "STATUS"+(Long)status.get(i) );
				loadByStatus.put( status.get(i), new Long(val) );
			}
			userLoad.setLoadByStatus(loadByStatus);
			userLoad.setTotalLoad( rs.getLong( "total" ) );
			result.add(userLoad);


		}
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			  throw e; 
		}finally{
			closeResources(conn, ps, rs);
		}		
		LOGGER.info("getUserLoadList end");
		return result;
	}
	/**
	 * This method retrieves the number of locked users.
	 * @return
	 * @throws SQLException
	 */

	public int countLockedusers() throws SQLException {
		
		LOGGER.info("countLockedusers start");
		int count = 0;
		String sql = "SELECT COUNT(*) FROM USERS WHERE USR_LOCK_IND = '1'";
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection conn = null;
		
		try {
			conn = getConnection();
			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery();
			if (rs.next()) {
				count = rs.getInt(1);
			}
		
	} catch (SQLException e) {
		LOGGER.error(CodeHelper.getStackTrace(e));
		  throw e; 
	}finally{
		closeResources(conn, ps, rs);
	}				
		LOGGER.info("countLockedusers end");
		return count;
	}
	/**
	 * This method retrieves a set of user/s based on the given role and branch ID.
	 * @param roleCode
	 * @param branchID
	 * @return
	 * @throws SQLException
	 */
	public ArrayList selectUsersPerRoleBranch(String roleCode, String branchID) throws SQLException {
		
		LOGGER.info("selectUsersPerRoleBranch start");
		ArrayList list = new ArrayList();
		String sql = "SELECT U.USER_ID FROM USER_ROLES UR, USERS U" +
					" WHERE U.USER_ID = UR.USER_ID AND ROLE_CODE = ? AND SLO_ID = ?";
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection conn = null;
		
		try{
			conn = getConnection();
			ps = conn.prepareStatement(sql);
			ps.setString(1, roleCode);
			ps.setString(2, branchID);
			rs = ps.executeQuery();
			while (rs.next()) {
				String userID = new String();
				userID = rs.getString("USER_ID");
				list.add(userID);
			}
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			  throw e; 
		}finally{
			closeResources(conn, ps, rs);
		}				
		LOGGER.info("selectUsersPerRoleBranch end");
		return list;
	}


	/**
	 * Resets the user's password
	 * @param user
	 */
	public void resetPassword(UserProfileData user) throws SQLException{

		LOGGER.info("resetPassword start");
		String sql="UPDATE USERS SET USR_PASSWORD= ENCRYPTDATA(?, " + IUMConstants.KEY + "), USR_1LOGIN_IND = ?, PSWD_UPDATE_DATE = ? WHERE USER_ID=?";
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection conn = null;
		
		try{
			conn = getConnection();
			ps = conn.prepareStatement(sql);
			ps.setString(1, user.getPassword());
			ps.setString(2, "0");
			ps.setDate(3, null);
			ps.setString(4, user.getUserId());
			ps.executeUpdate();
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			  throw e; 
		}finally{
			closeResources(conn, ps, rs);
		}		
		LOGGER.info("resetPassword end");
	}

	/**
	 * Retrieves all user roles
	 * @param userId
	 */
	public ArrayList getUserRoles(String userId) throws SQLException{
		
		LOGGER.info("getUserRoles start");
		ArrayList result = new ArrayList();
		String sql = "SELECT USR.ROLE_CODE, ROLE_DESC FROM USER_ROLES USR, ROLES R WHERE USER_ID=? AND USR.ROLE_CODE = R.ROLE_CODE";
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection conn = null;
		
		try{
			conn = getConnection();
			ps = conn.prepareStatement(sql);
			ps.setString(1, userId);
			rs = ps.executeQuery();
		while(rs.next()){
			RolesData role = new RolesData();
			role.setRolesDesc(rs.getString("ROLE_DESC"));
			role.setRolesId(rs.getString("ROLE_CODE"));
			result.add(role);
		}
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			  throw e; 
		}finally{
			closeResources(conn, ps, rs);
		}	
		
		LOGGER.info("getUserRoles end");
		return result;
	}
	
	public ArrayList getRoleByUserId(String userId) throws SQLException {
		LOGGER.info("getRoleByUserId start");
		ArrayList userRolesList = new ArrayList();
		String sql = "SELECT ROLE_CODE FROM USER_ROLES WHERE USER_ID = ?";
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection conn = null;
		
		try {
			conn = getConnection();
			ps = conn.prepareStatement(sql);
			ps.setString(1, userId);
			rs = ps.executeQuery();
			while (rs.next()) {
				System.out.println("USER ROLE: " + rs.getString("ROLE_CODE"));
				userRolesList.add(rs.getString("ROLE_CODE"));
			}
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e; 
		} finally {
			closeResources(conn, ps, rs);
		}	
		
		LOGGER.info("getRoleByUserId end");
		return userRolesList;
	}

	/**
	 * 
	 * 
	 * This method compares the given password to the user's password stored in the DB.
	 * @param userID
	 * @param password
	 * @return
	 * @throws SQLException
	 */
	public boolean isValidPassword(String userID, String password) throws SQLException {
		
		LOGGER.info("isValidPassword start");
		boolean isValid = false;
		String sql = "SELECT USER_ID FROM USERS WHERE USER_ID = ? AND USR_PASSWORD = ENCRYPTDATA(?," + IUMConstants.KEY + ")";
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection conn = null;
		
		try{
			conn = getConnection();
			ps = conn.prepareStatement(sql);
			ps.setString(1, userID);
			rs = ps.executeQuery();
		if (rs.next()) {
			isValid = true;
		}
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			  throw e; 
		}finally{
			closeResources(conn, ps, rs);
		}				
		LOGGER.info("isValidPassword end");
		return isValid;
	}

	/**
	 * This method changes the user's password.
	 *
	 * @param userID
	 * @param password
	 * @throws SQLException
	 */
	public void updateUserPassword(String userID, String password, Connection conn) throws SQLException {
		
		LOGGER.info("updateUserPassword start");
		String sql = "UPDATE USERS SET USR_PASSWORD = ENCRYPTDATA(?, " + IUMConstants.KEY + "), PSWD_UPDATE_DATE = ? WHERE USER_ID = ?";
		PreparedStatement ps = null;
		ResultSet rs = null;
		try{
		ps = conn.prepareStatement(sql);

		if (password != null && !"".equals(password.trim())){
			ps.setString(1, password.trim());
		} else {
			ps.setString(1, " ");
		}
	
		ps.setTimestamp(2, DateHelper.sqlTimestamp(new Date()));
		ps.setString(3, userID);
		ps.executeUpdate();
	
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			  throw e; 
		}finally{
			closeResources(ps, rs);
		}			
		LOGGER.info("updateUserPassword end");
	}

	public void update1LoginInd(String userID, boolean value, Connection conn) throws SQLException {
		
		LOGGER.info("update1LoginInd start");
		String sql = "UPDATE USERS SET USR_1LOGIN_IND = ? WHERE USER_ID = ?";
		PreparedStatement ps = null;
		ResultSet rs = null;
		try{
		ps = conn.prepareStatement(sql);
		if(value){
			ps.setString(1, "1");
		}else{
			ps.setString(1, "0");
		}
		ps.setString(2, userID);
		ps.executeUpdate();
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			  throw e; 
		}finally{
			closeResources(ps, rs);
		}			
		LOGGER.info("update1LoginInd end");
	}
	
	public void update1LoginInd(String userID, boolean value) throws SQLException {
		
		LOGGER.info("update1LoginInd start");
		String sql = "UPDATE USERS SET USR_1LOGIN_IND = ? WHERE USER_ID = ?";
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection conn = null;
		
		try{
			conn = getConnection();
			conn.setAutoCommit(false);
			ps = conn.prepareStatement(sql);
		if(value){
			ps.setString(1, "1");
		}else{
			ps.setString(1, "0");
		}
		ps.setString(2, userID);
		ps.executeUpdate();
		conn.commit();
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			if(conn != null){
				conn.rollback();
			}
			  throw e; 
		}finally{
			closeResources(conn, ps, rs);
		}			
		LOGGER.info("update1LoginInd end");
	}

	/**
	 * This method changes the user lock indicator.
	 *
	 * @param userID
	 * @param lock
	 * @throws SQLException
	 */
	public void updateLockIndicator(String userID, boolean lock) throws SQLException {
		
		LOGGER.info("updateLockIndicator start");
		String sql = "UPDATE USERS SET USR_LOCK_IND = ? WHERE USER_ID = ?";
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection conn = null;
		
		try{
			conn = getConnection();
			ps = conn.prepareStatement(sql);
			ps.setBoolean(1, lock);
			ps.setString(2, userID);
			ps.executeUpdate();
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			  throw e; 
		}finally{
			closeResources(conn, ps, rs);
		}	
		LOGGER.info("updateLockIndicator end");
	}

	/**
	 * This method updates the default role of a user.
	 * @param role
	 * @param userId
	 * @param updatedBy
	 * @throws SQLException
	 */

	public void updateUserDefaultRole(String role, String userId, String updatedBy) throws SQLException{

		LOGGER.info("updateUserDefaultRole start");
		StringBuffer sql =  new StringBuffer();
		sql.append("UPDATE USERS SET");
		sql.append(" USR_ROLE=?,");
		if(role==null||role.equals("") ){
			sql.append("USR_ACTIVE_IND='0',");
		}
		sql.append(" UPDATED_BY=?,");
		sql.append(" UPDATED_DATE=?");
		sql.append(" WHERE");
		sql.append(" USER_ID=?");
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection conn = null;
		
		try{	
			conn = getConnection();
			conn.setAutoCommit(false);
			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, role);
			ps.setString(2, updatedBy);
			ps.setTimestamp(3, DateHelper.sqlTimestamp( new Date()) );
			ps.setString(4, userId);
			ps.executeUpdate();
			conn.commit();
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			if(conn != null){
				conn.rollback();
			}
			throw e; 
		}finally{
			closeResources(conn, ps, rs);
		}				
		LOGGER.info("updateUserDefaultRole end");
	}



	/**
	 * This method retrieves a set of user/s and its corresponding branch/es based on the given user type.
	 * @return
	 */
	public ArrayList getUserBranches(String userType) throws SQLException
	{
	
		LOGGER.info("getUserBranches start");
		StringBuffer sql = new StringBuffer();
		sql.append("select");
		sql.append("  US.USER_ID ");
		sql.append(", US.SLO_ID ");
		sql.append(", US.USR_FIRST_NAME ");
		sql.append(", US.USR_LAST_NAME ");
		sql.append(" from USERS US ");
		sql.append(" where ");
		sql.append("  US.USR_TYPE = ?");
		sql.append(" order by ");
		sql.append("  US.USR_LAST_NAME ");
		sql.append(", US.USR_FIRST_NAME ");

		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection conn = null;
		ArrayList list = new ArrayList();
		try{
			conn = getConnection();
			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, userType);
			rs = ps.executeQuery();
		while (rs.next()) {
			UserProfileData upd = new UserProfileData();
			upd.setUserId(rs.getString("USER_ID"));
			upd.setLastName(rs.getString("USR_LAST_NAME"));
			upd.setFirstName(rs.getString("USR_FIRST_NAME"));
			upd.setOfficeCode(rs.getString("SLO_ID"));
			list.add(upd);
		}
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			  throw e; 
		}finally{
			closeResources(conn, ps, rs);
		}		
		LOGGER.info("getUserBranches end");
		return list;
	}


	//added 08july2004
	public UserPasswords getUserPasswords(String userid)throws SQLException{
		
		LOGGER.info("getUserPasswords start");
		UserPasswords upswd = null;
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT");
		sql.append("  USERID");
		sql.append(", DECRYPTDATA(PASSWORD1," + IUMConstants.KEY + ") PASSWORD1");
		sql.append(", DECRYPTDATA(PASSWORD2," + IUMConstants.KEY + ") PASSWORD2");
		sql.append(", DECRYPTDATA(PASSWORD3," + IUMConstants.KEY + ") PASSWORD3");
		sql.append(", DECRYPTDATA(PASSWORD4," + IUMConstants.KEY + ") PASSWORD4");
		sql.append(", DECRYPTDATA(PASSWORD5," + IUMConstants.KEY + ") PASSWORD5");
		sql.append(" from USER_PASSWORDS");
		sql.append(" where ");
		sql.append("  USERID = ?");
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection conn = null;
		
		try{
			conn = getConnection();
			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, userid);
			rs = ps.executeQuery();
		if(rs.next()){
			upswd = new UserPasswords();
			upswd.setUserid(rs.getString("USERID"));
			upswd.setPassword1(rs.getString("PASSWORD1").trim());
			upswd.setPassword2(rs.getString("PASSWORD2").trim());
			upswd.setPassword3(rs.getString("PASSWORD3").trim());
			upswd.setPassword4(rs.getString("PASSWORD4").trim());
			upswd.setPassword5(rs.getString("PASSWORD5").trim());
		}
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			  throw e; 
		}finally{
			closeResources(conn, ps, rs);
		}		
		LOGGER.info("getUserPasswords end");
		return upswd;
	}

	public void insertUserPasswords(UserPasswords upswd)throws SQLException{
		
		LOGGER.info("insertUserPasswords start");
		StringBuffer sql = new StringBuffer();
		sql.append("INSERT INTO USER_PASSWORDS");
		sql.append("  (USERID, PASSWORD1, PASSWORD2, PASSWORD3, PASSWORD4, PASSWORD5)");
		sql.append(" VALUES(?, ENCRYPTDATA(?, " + IUMConstants.KEY + "), " +
			"ENCRYPTDATA(?, " + IUMConstants.KEY + "), ENCRYPTDATA(?, " + IUMConstants.KEY + "), " +
			"ENCRYPTDATA(?, " + IUMConstants.KEY + "), ENCRYPTDATA(?, " + IUMConstants.KEY + "))");
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection conn = null;
		
		try{
			conn = getConnection();
			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, upswd.getUserid());
			ps.setString(2, upswd.getPassword1()!=null?upswd.getPassword1().trim():"0");
			ps.setString(3, upswd.getPassword2()!=null?upswd.getPassword2().trim():"0");
			ps.setString(4, upswd.getPassword3()!=null?upswd.getPassword3().trim():"0");
			ps.setString(5, upswd.getPassword4()!=null?upswd.getPassword4().trim():"0");
			ps.setString(6, upswd.getPassword5()!=null?upswd.getPassword5().trim():"0");
			ps.executeUpdate();
			conn.commit();
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			  throw e; 
		}finally{
			closeResources(conn, ps, rs);
		}		
		LOGGER.info("insertUserPasswords end");
	}
	
	public void changeUserPassword(String userID, String newPassword) throws SQLException {
		
		Connection conn = null;
		try {
			conn = getConnection();
			UserProfileData upd = selectUserProfile(userID);
			updateUserPassword(userID, newPassword, conn);
			update1LoginInd(userID, true, conn);
			upd.setPassword(newPassword);
			updateUserPasswords(upd, conn);
			conn.commit();
		} catch (SQLException e) {
			if (conn != null) {
				conn.rollback();		
			}
		} finally {
			closeConnection(conn);
		}
		
	}
	

	public void updateUserPasswords(UserProfileData upd, Connection con)throws SQLException{
		
		LOGGER.info("updateUserPasswords start");
		UserPasswords retUP = new UserPasswords();
		UserPasswords tmp = getUserPasswords(upd.getUserId());
		if(tmp==null){
			
			tmp = new UserPasswords();
			tmp.setUserid(upd.getUserId());
		}

		StringBuffer sql1 = new StringBuffer();
		sql1.append("DELETE FROM USER_PASSWORDS WHERE USERID = ?");
		PreparedStatement ps1 = null;
		ResultSet rs = null;
		Connection conn = null;
		
		try{
		conn = getConnection();
		ps1 = conn.prepareStatement(sql1.toString());
		ps1.setString(1, upd.getUserId());
		ps1.executeUpdate();

		retUP.setUserid(upd.getUserId());
		retUP.setPassword1(upd.getPassword());
		retUP.setPassword2(tmp.getPassword1());
		retUP.setPassword3(tmp.getPassword2());
		retUP.setPassword4(tmp.getPassword3());
		retUP.setPassword5(tmp.getPassword4());
		insertUserPasswords(retUP);
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			  throw e; 
		}finally{
			  closeResources(conn, ps1, rs);
		}		
		LOGGER.info("updateUserPasswords end");
	}
	//

	/**
	 * This method checks if the user is a manager to be able to assign an AR
	 *
	 * @param userID
	 * @throws SQLException
	 */
	public boolean isUserAllowedToAssign(String userId) throws SQLException {
		
		LOGGER.info("isUserAllowedToAssign start");
		ResultSet 		  resultSet;
		String 			  userRole;
		String 			  sql = "SELECT ROLE_CODE FROM USER_ROLES " +
								"WHERE	USER_ID=?";

		PreparedStatement prepStmt = null;
		ResultSet rs = null;
		Connection conn = null;
		
		try{
			conn = getConnection();
			prepStmt = conn.prepareStatement(sql);
			prepStmt.setString(1, userId);
			prepStmt.executeQuery();
			resultSet = prepStmt.executeQuery();
		while(resultSet.next()) {
			userRole = resultSet.getString("ROLE_CODE");
			
			if (userRole.equals("FACILITSUPMGR") ||
				userRole.equals("UWTNGSUPMGR") ||
				userRole.equals(IUMConstants.ROLES_UNDERWRITER) ||
				userRole.equals(IUMConstants.ROLES_FACILITATOR)) { // Feb 21, CR# 10133 Enhancement: Added Underwriter and Facilitator
				return true;
			}
		}
		
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			  throw e; 
		}finally{
			closeResources(conn, prepStmt, rs);
		}		
		LOGGER.info("isUserAllowedToAssign end");
		return false;
	}

	
}
