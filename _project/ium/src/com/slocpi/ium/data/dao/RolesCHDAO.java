/*
 * Created on Feb 16, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.data.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.StringTokenizer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.NameValuePair;


/**
 * @author Abie
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class RolesCHDAO  extends CodeHelperDao {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(RolesCHDAO.class);
	
	private Connection conn  = null;
	
	public RolesCHDAO(Connection connection) {
		
			conn = connection;
		}
	public Collection getCodeValues() throws SQLException {
			return extractRoles();
		}
	
	public Collection extractRoles() throws SQLException{
		
		LOGGER.info("extractRoles start");
		Collection col = new ArrayList();
		String sb = "SELECT ROLE_CODE,ROLE_DESC FROM ROLES order by ROLE_DESC";
		PreparedStatement ps = null;
		ResultSet rs = null;
		try{										
		ps = conn.prepareStatement(sb);
		rs = ps.executeQuery();
		while (rs.next()) {
			NameValuePair nvp = new NameValuePair();
			nvp.setName(rs.getString("ROLE_DESC"));
			nvp.setValue(rs.getString("ROLE_CODE"));
			col.add(nvp);
		}
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			  throw e; 
		}finally{
			  this.closeResources(ps, rs);
		}	
		LOGGER.info("extractRoles end");
		return col;	
	
	}
	private ArrayList generateFilters (String filters) {
		
		LOGGER.info("generateFilters start");
		ArrayList list = new ArrayList();
		StringTokenizer st = new StringTokenizer(filters,",");
		while (st.hasMoreTokens()) {
			String item = st.nextToken();
			list.add(item);
		}
		LOGGER.info("generateFilters end");
		return list;
	}

	
	/* (non-Javadoc)
	 * @see com.slocpi.ium.data.dao.CodeHelperDao#getCodeValue(java.lang.String)
	 */
	public Collection getCodeValue(String codeValue) throws SQLException {
	
		LOGGER.info("getCodeValue start");
		Collection col = new ArrayList();
		String sb = "SELECT ROLE_CODE,ROLE_DESC FROM ROLES";
		PreparedStatement ps = null;
		ResultSet rs = null;
		try{									
		ps = conn.prepareStatement(sb);
		rs = ps.executeQuery();
		while (rs.next()) {
			NameValuePair nvp = new NameValuePair();
			nvp.setName(rs.getString("ROLE_DESC"));
			nvp.setValue(rs.getString("ROLE_CODE"));
			col.add(nvp);
		}
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			  throw e; 
		}finally{
			  this.closeResources(ps, rs);
		}	
		LOGGER.info("getCodeValue end");
		return col;	

	}	
	
	

}
