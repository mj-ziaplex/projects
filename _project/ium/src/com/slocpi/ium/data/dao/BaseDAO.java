package com.slocpi.ium.data.dao;

import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.IUMConstants;

import java.sql.Connection;
import java.util.ResourceBundle;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

public abstract class BaseDAO {
	
	private static final Logger BASE_LOGGER = LoggerFactory.getLogger(BaseDAO.class);
	
	public static Connection getConnection() throws SQLException{
		
		BASE_LOGGER.info("getConnectionConfig start");
		Connection connection = null;														
		
		ResourceBundle rb = ResourceBundle.getBundle(IUMConstants.IUM_CONFIG);
		String db_location = rb.getString(IUMConstants.IUM_DBLOCATION);
		
		boolean tryManual = false;
       	
       	try {
       		InitialContext ic = new InitialContext(); 
			DataSource ds = (DataSource) ic.lookup(db_location);
			connection = ds.getConnection();
			
       	}catch (NamingException e) {
       		BASE_LOGGER.error(CodeHelper.getStackTrace(e));
       		tryManual = true;
       	} catch (Exception e) {
       		BASE_LOGGER.error(CodeHelper.getStackTrace(e));
		}
       	
       	if (tryManual) {
        	BASE_LOGGER.warn("****************************ST DEV SET-UP. NO DATASOURCE CONFIGURATION******************************");
       		BASE_LOGGER.warn("****************************START MANUAL CONNECTION ON DATABASE*************************************");
       		try {
	    		String conn_driver = rb.getString("connection-driver");
	    		String url = rb.getString("url");
	    		String username = rb.getString("username");
	    		String password = rb.getString("password");
	    		Class.forName(conn_driver);
	    		connection = DriverManager.getConnection(url, username, password);
       		} catch (ClassNotFoundException e) {
       			BASE_LOGGER.error(CodeHelper.getStackTrace(e));
       			connection = null;
       		}
       	}
	
       	if (connection==null) {
       		BASE_LOGGER.warn("No Connection created");
			throw new SQLException("No Connection created");
		}
       	
       	BASE_LOGGER.info("getConnectionConfig end");
		return connection;			
	}
	
	
	public static void closeConnection(Connection conn) throws SQLException{
		try {
			if (conn != null) {
				conn.close();
			}
		}  catch (SQLException e){
			BASE_LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		}
	}
	
	public void closeResources(PreparedStatement ps, ResultSet rs) throws SQLException{
		
		try {
			if (rs != null){
				rs.close();
			}			
		} catch (SQLException e){
			BASE_LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		}
		
		try {
			if (ps != null){
				ps.close();
			}
		}
		catch(SQLException e)
		{
			BASE_LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		}
		
	}
	
	public void closeResources(Statement stmt, ResultSet rs) throws SQLException{
		
		try {
			if (rs != null){
				rs.close();
			}			
		} catch (SQLException e){
			BASE_LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		}
		
		try {
			if (stmt != null){
				stmt.close();
			}
		}
		catch(SQLException e)
		{
			BASE_LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		}
		
	}	
	
	public void closeResources(Connection conn, PreparedStatement ps, ResultSet rs) throws SQLException{
		
		try {
			if (rs != null){
				rs.close();
			}			
		} catch (SQLException e){
			BASE_LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		}
		
		try {
			if (ps != null){
				ps.close();
			}
		}
		catch(SQLException e)
		{
			BASE_LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		}

		closeConnection(conn);
		
	}
	
	public void closeResources(Connection conn, Statement stmt, ResultSet rs) throws SQLException{
		
		try {
			if (rs != null){
				rs.close();
			}			
		} catch (SQLException e){
			BASE_LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		}
		
		try {
			if (stmt != null){
				stmt.close();
			}
		}
		catch(SQLException e)
		{
			BASE_LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		}

		closeConnection(conn);
		
	}

}
