package com.slocpi.ium.data.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.NameValuePair;


public class DocumentTypeCHDao extends CodeHelperDao {

	private static final Logger LOGGER = LoggerFactory.getLogger(DocumentDAO.class);

	private Connection conn;

	public DocumentTypeCHDao(Connection conn) throws SQLException {
		this.conn = conn;
	}// DocumentTypeCHDao

	/**
	 * This method will return a collection of all the client types.
	 */
	public Collection getCodeValues() throws SQLException {
		
		LOGGER.info("getCodeValues start");
	  Collection list = new ArrayList();
	  String sql = "SELECT DOC_CODE AS CODE, DOC_DESC AS DESCRIPTION FROM DOCUMENT_TYPES";
	  
	  PreparedStatement ps = null;
	  ResultSet rs = null;
	  
	  try {	  	
		ps = conn.prepareStatement(sql);
		rs = ps.executeQuery();				    		
		while (rs.next()) {
		  NameValuePair bean = new NameValuePair();
		  bean.setName(rs.getString("DESCRIPTION"));
		  bean.setValue(rs.getString("CODE"));
		  list.add(bean);
		}
	  } catch (SQLException e) {
		  LOGGER.error(CodeHelper.getStackTrace(e));
			throw e; 
	  }finally{
		  	this.closeResources(ps, rs);
	  }		
	  LOGGER.info("getCodeValues end");
      return (list);
	}// getCodeValues


	/**
	 * This method will return a subset of client types based on the specified client type code.
	 */
	public Collection getCodeValue(String codeValue) throws SQLException {
		
		LOGGER.info("getCodeValue start");
		Collection list = new ArrayList();
		String sql = "SELECT DOC_CODE AS CODE, DOC_DESC AS DESCRIPTION FROM DOCUMENT_TYPES WHERE CLT_CODE = ?";
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		  
		
		try {	  	
		  ps = conn.prepareStatement(sql);
		  ps.setString(1, codeValue);
		  rs = ps.executeQuery();				    		
		  while (rs.next()) {
			NameValuePair bean = new NameValuePair();
			bean.setName(rs.getString("DESCRIPTION"));
			bean.setValue(rs.getString("CODE"));
			list.add(bean);
		  }
		  } catch (SQLException e) {
			  LOGGER.error(CodeHelper.getStackTrace(e));
				throw e; 
		  }finally{
			  	this.closeResources(ps, rs);
		  }		
		LOGGER.info("getCodeValue end");
		return (list);
	}// getCodeValue




}