/*
 * Created on Jun 22, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.data.dao.test;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import junit.framework.TestCase;

import com.slocpi.ium.data.AccessData;
import com.slocpi.ium.data.dao.AccessDAO;
import com.slocpi.ium.data.util.DataSourceProxy;

/**
 * @author Jeffrey Canlas
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class AccessDAOTest extends TestCase {
	
	static Connection conn = null;
    
	public AccessDAOTest(String arg0) {
		super(arg0);
	}



	public void testAccessDAO() throws SQLException {
    
    	/*
	   * Test connection and see whether SQL statement 
	   * produced a result.  the entered sql statement
	   * is valid (tables and fields are existing)
	   * thus resulting arraylist should be 
	   * greater than zero. 
	   * 
	   */
	  
	   ArrayList list = new ArrayList();
	   conn = new DataSourceProxy().getConnection();
	   AccessDAO dao = new AccessDAO(conn);
	   
	 /*  list = dao.retrieveAccess();
	  */
	   assertTrue("test 1", list.size() > 0);
	
       /*test if the data in the dataset are the
        * same as the one in the database. 
        * Will check the first result and see 
        * if it is the same from the database
        */	
        
        long ID = ((AccessData)list.get(0)).getAccessId();
        String desc = ((AccessData)list.get(0)).getAccessDesc();
        System.out.println(ID);
        System.out.println(desc); 
        assertTrue(ID == 1);
        assertTrue(desc.equals("Inquire Only"));
        
        
  
	}

}
