/*
 * Created on Jun 25, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.data.dao.test;

import junit.framework.TestCase;

/**
 * @author Alain del Rosario
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class UserDAOTest extends TestCase {

	/**
	 * Constructor for UserDAOTest.
	 * @param arg0
	 */
	public UserDAOTest(String arg0) {
		super(arg0);
	}

	public static void main(String[] args) {
	}

	public void testUserDAO() {
	}

	public void testIsUserValid() {
	}

	public void testSelectUserProfiles() {
	}

	public void testInsertUserProfile() {
	}

	public void testSelectUserProfile() {
	}

	public void testSelectUserProfileByACF2ID() {
	}

	public void testSelectUserProfileByRole() {
	}

	public void testSelectProcessPreference() {
	}

	public void testUpdateUserProfile() {
	}

	public void testGetType() {
	}

	public void testGetUserLoadList() {
	}

	public void testCountLockedusers() {
	}

	public void testSelectUsersPerRoleBranch() {
	}

	public void testResetPassword() {
	}

	public void testGetUserRoles() {
	}

	public void testIsValidPassword() {
	}

	public void testUpdateUserPassword() {
	}

	public void testUpdateLockIndicator() {
	}

	public void testUpdateUserDefaultRole() {
	}

	public void testGetUserBranches() {
	}

	public void testMain() {
	}

}
