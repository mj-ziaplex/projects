/*
 * Created on Jan 16, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.data.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.StringTokenizer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.IUMConstants;
import com.slocpi.ium.util.NameValuePair;

/**
 * @author daguila
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class UserCHDao extends CodeHelperDao{
	private static final Logger LOGGER = LoggerFactory.getLogger(UserCHDao.class);
	
	
	public UserCHDao() {}
	
	public Collection getCodeValues() throws SQLException {
		return extractUsers(null);
	}
	
	
	public Collection getCodeValues(String filters) throws SQLException {
		return extractUsers(generateFilters(filters));
	}
	
	public Collection getCodeValues(String codeSetName, String filters) throws SQLException {
		return extractUsers(codeSetName, generateFilters(filters));
	}
	
	private Collection extractUsers(ArrayList filters) throws SQLException {
		
		LOGGER.info("extractUsers start 1");
		Collection col = new ArrayList();
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT "); 
		sb.append("	US.USER_ID ");
		sb.append(" , (US.USR_LAST_NAME || CASE WHEN US.USR_FIRST_NAME IS NULL THEN '' ELSE ', ' || US.USR_FIRST_NAME END) USR_NAME ");
		sb.append(" FROM ");
		sb.append("  users US ");
		
		if ((filters != null) && (filters.size() > 0)) {
			sb.append(" where US.USER_ID in ");
			sb.append("  (select R.USER_ID from USER_ROLES R where R.ROLE_CODE IN (");
			for (int i=0; i< filters.size(); i++) {
				if (i!=0) sb.append(", ");
				String filter = (String) filters.get(i);
				sb.append("'" + filter + "'");
			}
			sb.append("))");
		}
		
		sb.append(" ORDER BY US.USR_LAST_NAME, US.USR_FIRST_NAME, US.USER_ID ");
		
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection conn = null;
		
		  try {
			conn = getConnection();	
			ps = conn.prepareStatement(sb.toString());
			rs = ps.executeQuery();
			
			while (rs.next()) {
				NameValuePair nvp = new NameValuePair();
				nvp.setName(rs.getString("usr_name"));
				nvp.setValue(rs.getString("user_id"));
				col.add(nvp);
			}
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			  throw e; 
		}finally{
				closeResources(conn, ps, rs);
		}
		LOGGER.info("extractUsers end 1");
		return col;
	}// extractUsers
	
	private Collection extractUsers(String codeSetName, ArrayList filters) throws SQLException {
		
		LOGGER.info("extractUsers start 2");
		Collection col = new ArrayList();
		StringBuffer sb = new StringBuffer();
		sb.append(" select "); 
		sb.append(" US.USER_ID ");
	
		sb.append(" , (US.USR_LAST_NAME || CASE WHEN US.USR_FIRST_NAME IS NULL THEN '' ELSE ', ' || US.USR_FIRST_NAME END) USR_NAME ");
		sb.append(" from ");
		sb.append("  USERS US ");
		
		sb.append(" where US.USER_ID in ");
		sb.append("  (select R.USER_ID from USER_ROLES R where R.ROLE_CODE = ? ) ");
		if ((filters != null) && (filters.size() > 0)) {			
			sb.append(" AND US.SLO_ID = ?");

		}
		
		sb.append(" order by US.USR_LAST_NAME, US.USR_FIRST_NAME, US.USER_ID ");
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection conn = null;
		
		  try {
			conn = getConnection();		
			ps = conn.prepareStatement(sb.toString());
			ps.setString(1, IUMConstants.ROLES_AGENTS);
			if ((filters != null) && (filters.size() > 0))
				ps.setString(2, (String) filters.get(0));
			rs = ps.executeQuery();
			while (rs.next()) {
				NameValuePair nvp = new NameValuePair();
				nvp.setName(rs.getString("usr_name"));
				nvp.setValue(rs.getString("user_id"));
				col.add(nvp);
			}
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			  throw e; 
		}finally{
			closeResources(conn, ps, rs);
		}			
		LOGGER.info("extractUsers end 2");
		return col;
	}// extractUsers
	
	
	private ArrayList generateFilters (String filters) {
		
		LOGGER.info("generateFilters start");
		ArrayList list = new ArrayList();
		StringTokenizer st = new StringTokenizer(filters,",");
		while (st.hasMoreTokens()) {
			String item = st.nextToken();
			list.add(item);
		}
		LOGGER.info("generateFilters end");
		return list;
	}// generateFilters


	public Collection getCodeValue(String codeValue) throws SQLException {
		
		LOGGER.info("getCodeValue start");
		Collection list = new ArrayList();
		String sql = "SELECT USER_ID, CONCAT(CONCAT(USR_FIRST_NAME, ' '), USR_LAST_NAME) AS FULLNAME " +
					 "FROM USERS " +
					 "WHERE USER_ID = ? ";
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection conn = null;
		
		  try {
			conn = getConnection();		
			ps = conn.prepareStatement(sql);
			ps.setString(1, codeValue);
			rs = ps.executeQuery();		
		  while (rs.next()) {
			NameValuePair bean = new NameValuePair();
			bean.setName(rs.getString("FULLNAME"));
			bean.setValue(rs.getString("USER_ID"));
			list.add(bean);
		  }
		} catch (SQLException e) {
			  LOGGER.error(CodeHelper.getStackTrace(e));
			  throw e; 
		}finally{
				closeResources(conn, ps, rs);
		}		
		LOGGER.info("getCodeValue end");
		return (list);
	}// getCodeValue


}
