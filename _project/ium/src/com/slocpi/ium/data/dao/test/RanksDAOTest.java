/*
 * Created on Jun 24, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.data.dao.test;

import java.sql.Connection;
import java.sql.Date;
import java.sql.SQLException;
import java.util.ArrayList;

import junit.framework.TestCase;

import com.slocpi.ium.data.RankData;
import com.slocpi.ium.data.dao.RanksDAO;
import com.slocpi.ium.data.util.DataSourceProxy;


/**
 * @author Alain del Rosario
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class RanksDAOTest extends TestCase {

	/**
	 * Constructor for RanksDAOTest.
	 * @param arg0
	 */
	public RanksDAOTest(String arg0) {
		super(arg0);
	}

	

	public void testRanksDAO() {
		Connection conn2 = null;
		conn2 = new DataSourceProxy().getConnection();
		RanksDAO rankdao = new RanksDAO(conn2);
		assertNotNull(rankdao);
	}

	public void testRetrieveRankData() throws SQLException {
		long rnkid = 21;
		double rnkfee = 50000.0;
		Connection conn2 = null;
		conn2 = new DataSourceProxy().getConnection();
		RanksDAO rankdao = new RanksDAO(conn2);
		RankData rd = new RankData();
		rd = rankdao.retrieveRankData(rnkid);
		assertEquals(rd.getRankDesc(),"Major");
		assertEquals((byte)rd.getRankFee(),(byte)rnkfee);
		
	}

	public void testRetrieveRanks() throws SQLException {
		ArrayList alist = new ArrayList();
		Connection conn2 = null;
		conn2 = new DataSourceProxy().getConnection();
		RanksDAO rankdao = new RanksDAO(conn2);
		alist = rankdao.retrieveRanks();
		assertTrue(alist.size()==13);/*be sure
		to change size when new rank is added */
	}
	
	
	
	


    // remove comment method below to test adding a rank
    /*
	public void testInsertRank() throws SQLException {
		long rnkid = 20;
		String rnkdesc = "test desc";
		double rnkfee = 199.99;
		Date cdate = null;
		String cdby = null;
		Connection conn2 = null;
		conn2 = new DataSourceProxy().getConnection();
		RanksDAO rankdao = new RanksDAO(conn2);
		
		RankData rd = new RankData();
		rd.setRankCode(rnkid);
		rd.setRankDesc(rnkdesc);
		rd.setRankFee(rnkfee);
		rd.setCreateDate(cdate);
		rd.setCreatedBy(cdby);
		rankdao.insertRank(rd);
	
	
		RankData rd2 = new RankData();
		rd2 = rankdao.retrieveRankData(rnkid);
		assertEquals(rd2.getRankDesc(),rnkdesc);
		assertEquals((byte)rd2.getRankFee(),(byte)rnkfee);
		
	}
	*/
	
	
    
	public void testUpdateRank() throws SQLException {
		long rnkid = 19;
		String rnkdesc = "test desc updated";
		double rnkfee = 199.99;
		Date cdate = null;
		String cdby = null;
		Connection conn2 = null;
		conn2 = new DataSourceProxy().getConnection();
		RanksDAO rankdao = new RanksDAO(conn2);
		RankData rd = new RankData();
		rd.setRankCode(rnkid);
		rd.setRankDesc(rnkdesc);
		rd.setRankFee(rnkfee);
		rd.setCreateDate(cdate);
		rd.setCreatedBy(cdby);
		rankdao.updateRank(rd);
		
		RankData rd2 = new RankData();
		rd2 = rankdao.retrieveRankData(rnkid);
		assertEquals(rd2.getRankDesc(),rnkdesc);
		assertEquals((byte)rd2.getRankFee(),(byte)rnkfee);
		
		
	}
    
    
}
