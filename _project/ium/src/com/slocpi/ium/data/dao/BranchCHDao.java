package com.slocpi.ium.data.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.slocpi.ium.data.SunLifeOfficeData;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.NameValuePair;


public class BranchCHDao extends CodeHelperDao {

	private static final Logger LOGGER = LoggerFactory.getLogger(BranchCHDao.class);
	private Connection conn;

	public BranchCHDao(Connection conn) throws SQLException {
		this.conn = conn;
	}
	

	
	public SunLifeOfficeData getSunLifeOffice(String officeId) throws SQLException {
		
		LOGGER.info("getSunLifeOffice start");
		SunLifeOfficeData slod = null;
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT slo_office_code, " 	+	
						"slo_office_name, "		+	
						"slo_type, " 			+ 	
						"slo_address_line1, "	+	
						"slo_address_line2, " 	+ 	
						"slo_address_line3, " 	+ 	
						"slo_city, " 			+	
						"slo_province, " 		+	
						"slo_country, " 		+ 	
						"slo_zip_code, "		+ 	
						"slo_contact_num, "		+ 	
						"slo_fax_num " 			+	
					" FROM sunlife_offices WHERE slo_office_code = ? "
				);
				
		PreparedStatement ps = null; 
		ResultSet rs = null;
		try{
			ps =conn.prepareStatement(sb.toString());
			ps.setString(1,officeId);
			rs =ps.executeQuery();
			if (rs.next()) {
				slod = new SunLifeOfficeData();
				slod.setAddr1(rs.getString("slo_address_line1"));
				slod.setAddr2(rs.getString("slo_address_line2"));
				slod.setAddr3(rs.getString("slo_address_line3"));
				slod.setCity(rs.getString("slo_city"));
				slod.setContactNumber(rs.getString("slo_contact_num"));
				slod.setFaxNumber(rs.getString("slo_fax_num"));
				slod.setOfficeId(rs.getString("slo_office_code"));
				slod.setOfficeName(rs.getString("slo_office_name"));
				slod.setOfficeType(rs.getString("slo_type"));
				slod.setProvince(rs.getString("slo_province"));
				slod.setZipCode(rs.getString("slo_zip_code"));				 		
			}
		}catch(SQLException e)
		{
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		}finally
		{
			this.closeResources(ps, rs);
		}
		LOGGER.info("getSunLifeOffice end");
		return slod;
	}
	
	
	/**
	 * This method will return a collection of all the client types.
	 */
	public Collection getCodeValues() throws SQLException {
		
		LOGGER.info("getCodeValues start");
	  Collection list = new ArrayList();
	  String sql = "SELECT SLO_OFFICE_CODE AS CODE, SLO_OFFICE_NAME AS DESCRIPTION FROM SUNLIFE_OFFICES";
	  ResultSet rs = null; 
	  PreparedStatement ps = null;
	  try {	  	
		ps = conn.prepareStatement(sql);
		rs = ps.executeQuery();				    		
		while (rs.next()) {
		  NameValuePair bean = new NameValuePair();
		  bean.setName(rs.getString("DESCRIPTION"));
		  bean.setValue(rs.getString("CODE"));
		  list.add(bean);
		}
	  } 
	  catch (SQLException e) {
		  LOGGER.error(CodeHelper.getStackTrace(e));
		throw e; 
	  }finally
	  {
		  this.closeResources(ps, rs);
	  }
	  LOGGER.info("getCodeValues end");
	  return (list);
	}// getCodeValues


	/**
	 * This method will return a subset of client types based on the specified client type code.
	 */
	public Collection getCodeValue(String codeValue) throws SQLException {
		
		LOGGER.info("getCodeValue start");
		Collection list = new ArrayList();
		String sql = "SELECT SLO_OFFICE_CODE AS CODE, SLO_OFFICE_NAME AS DESCRIPTION FROM SUNLIFE_OFFICES WHERE SLO_OFFICE_CODE = ?";
		ResultSet rs = null;
		PreparedStatement ps = null;
		try {	  	
		   ps =conn.prepareStatement(sql);
		   ps.setString(1, codeValue);
		   rs = ps.executeQuery();				    		
		  while (rs.next()) {
			NameValuePair bean = new NameValuePair();
			bean.setName(rs.getString("DESCRIPTION"));
			bean.setValue(rs.getString("CODE"));
			list.add(bean);
		  }
		} 
		catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
		  throw e; 
		}finally
		{
			  this.closeResources(ps, rs);
		}
		LOGGER.info("getCodeValue end");
		return (list);
	}// getCodeValue


}