/*
 * Created on Feb 23, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.data.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.slocpi.ium.data.AuditTrailData;
import com.slocpi.ium.data.AuditTrailFilterData;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.DateHelper;

/**
 * @author Cris
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class AuditTrailDAO {

	private static final Logger LOGGER = LoggerFactory.getLogger(AuditTrailDAO.class);
	private Connection conn;
	
	public AuditTrailDAO(Connection _conn){
		conn = _conn;
	}
	
	public void closeDB() {
		
		LOGGER.info("closeDB start");
		try {
			this.conn.close();
		} catch (Exception e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
		}
		LOGGER.info("closeDB end");
	}
	
	//edited by Aldrich Abrogena Jan 16,2008	
	public ArrayList retrieveAuditTrail(AuditTrailFilterData filter) throws SQLException{
		
		LOGGER.info("retrieveAuditTrail start");
		ArrayList list = new ArrayList();
		Timestamp startDate = DateHelper.sqlTimestamp(DateHelper.sqlDate(filter.getStartDate()));
		Timestamp endDate = DateHelper.sqlTimestamp(DateHelper.sqlDate(filter.getEndDate()));
		
		long table = filter.getTable();
		String type = filter.getTransactionType();
		
		String tempSql = "SELECT " +
					 " AT.TRAN_NUM, " +
					 " AT.TRAN_RECORD, " +
					 " AT.TRAN_TYPE, " +
					 " AT.TRAN_DATETIME, " +
					 " AT.UAR_REFERENCE_NUM, " +
					 " AT.TRAN_CHANGED_FROM, " +
					 " AT.TRAN_CHANGED_TO " +
					 
					 " FROM " +
					 " AUDIT_TRAILS AT " +
					 
					 " WHERE " +
					 " AT.TRAN_NUM IS NOT NULL ";
		
		StringBuffer sb = new StringBuffer(tempSql);
	    if (table != 0){
			sb.append(" AND AT.TRAN_RECORD = ? ");
		}
		
		if (startDate != null && endDate != null){
			sb.append(" AND AT.TRAN_DATETIME BETWEEN ? AND ? ");
		 }
					 
		if (type != null && !type.equals("")){
		 	sb.append(" AND AT.TRAN_TYPE = ? ");
		}
					 
		sb.append(" ORDER BY AT.TRAN_DATETIME");
		String sql = sb.toString();
		ResultSet rs = null;
		PreparedStatement ps = null;
		try {
			int index=1;
			ps = conn.prepareStatement(sql);
			if (table!= 0){
				
				ps.setLong(index, table);
				index++;
			}
			if(startDate != null && endDate != null){
				ps.setTimestamp(index, startDate);
				index++;
				ps.setTimestamp(index, endDate);
				index++;
			}
			if(type!= null && !type.equals("")){
				ps.setString(index, type);
			}
			
			rs = ps.executeQuery();
			while (rs.next()){
				AuditTrailData data = new AuditTrailData();
				data.setTransId(rs.getLong("TRAN_NUM"));
				data.setTransRec(rs.getLong("TRAN_RECORD"));
				data.setTransType(rs.getString("TRAN_TYPE"));
				data.setTransDate(rs.getTimestamp("TRAN_DATETIME"));
				data.setReferenceNumber(rs.getString("UAR_REFERENCE_NUM"));
				data.setChangeFrom(rs.getString("TRAN_CHANGED_FROM"));
				data.setChangeTo(rs.getString("TRAN_CHANGED_TO"));
				
				list.add(data);
			}

		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		}finally
		{
			this.closeResources(ps, rs);
		}
		LOGGER.info("retrieveAuditTrail end");
		return list;
	}
	//added by Aldrich Abrogena Jan 16,2008
	private void closeResources(PreparedStatement ps, ResultSet rs) throws SQLException{
		
		
		try {
			if (ps != null){
				ps.close();
			}
		}
		catch(SQLException e)
		{
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		}
		try {
			if (rs != null){
				rs.close();
			}			
		} catch (SQLException e){
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		}
		
	}
	
	//edited by Aldrich Abrogena Jan 16,2008
	public void insertAuditTrail(AuditTrailData data) throws SQLException {
		
		LOGGER.info("insertAuditTrail start");
		String sql = "INSERT INTO AUDIT_TRAILS" +
					" (TRAN_NUM, TRAN_RECORD, TRAN_TYPE, TRAN_DATETIME, USER_ID, UAR_REFERENCE_NUM, TRAN_CHANGED_FROM, TRAN_CHANGED_TO)" +
					" VALUES(SEQ_AUDIT_TRAIL.NEXTVAL, ?, ?, ?, ?, ?, ?, ?)";
		PreparedStatement ps = null;
		try{
			ps=conn.prepareStatement(sql);
			ps.setLong(1, data.getTransRec());
			ps.setString(2, data.getTransType());
			ps.setDate(3, DateHelper.sqlDate(data.getTransDate()));
			ps.setString(4, data.getUserID());
			ps.setString(5, data.getReferenceNumber());
			ps.setString(6, data.getChangeFrom());
			ps.setString(7, data.getChangeTo());
			ps.executeUpdate();
		}catch(SQLException e)
		{
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		}finally
		{
			this.closeResources(ps, null);
		}
		LOGGER.info("insertAuditTrail end");
	}

	//edited by Aldrich Abrogena Jan 16,2008
	public int deleteAuditTrails(Date startDate, Date endDate) throws SQLException {
		
		LOGGER.info("deleteAuditTrails start");
		String sql = "DELETE FROM AUDIT_TRAILS WHERE to_date(to_char(TRAN_DATETIME, 'DDMONYYYY'), 'DDMONYYYY') BETWEEN ? AND ?";
		PreparedStatement ps = null; 
		int result = -1;
		try{
			ps = conn.prepareStatement(sql);
			ps.setDate(1, DateHelper.sqlDate(startDate));
			ps.setDate(2, DateHelper.sqlDate(endDate));
			result = ps.executeUpdate();
		}catch(SQLException e)
		{
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		}
		finally
		{
			this.closeResources(ps, null);
		}
		LOGGER.info("deleteAuditTrails end");
		return result;
	}	

}
