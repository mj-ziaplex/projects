/**
 * ProcessConfigurationDAO.java				Copyright (c) 2004 Pointwest Technologies Corp.
 * TODO DOCUMENT ME!
 * 
 * @author aditalo	 @date Feb 23, 2004
 */
package com.slocpi.ium.data.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.TreeMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.slocpi.ium.data.NotificationSettingData;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.IUMConstants;

/**
 * 
 * @author aditalo Copyright (c) 2004 Pointwest Technologies Corp.
 * @date Feb 23, 2004
 */
public class ProcessConfigDAO extends BaseDAO {

	private static final Logger LOGGER = LoggerFactory.getLogger(ProcessConfigDAO.class);

	/**
	 * 
	 */
	public ProcessConfigDAO() {
	}

	/**
	 * This method returns true if fromStatus exists given the lob, status type
	 * and toStatus.
	 * 
	 * @param lob
	 * @param type
	 * @param fromStatus
	 * @param toStatus
	 * @return boolean
	 * @throws SQLException
	 */
	public boolean isValidStatus(String lob, String type, long fromStatus, long toStatus) throws SQLException {

		LOGGER.info("isValidStatus start");
		boolean isValid = false;
		String sql = "SELECT COUNT(*) RECORD_COUNT" + " FROM PROCESS_CONFIGURATIONS" + " WHERE LOB_CODE = ?"
				+ " AND PROC_CODE = ?" + " AND EVNT_FROM_STATUS = ?" + " AND EVNT_TO_STATUS = ?";
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection conn = null;

		try {
			conn = getConnection();
			ps = conn.prepareStatement(sql);
			ps.setString(1, lob);
			ps.setString(2, type);
			ps.setLong(3, fromStatus);
			ps.setLong(4, toStatus);
			rs = ps.executeQuery();

			if (rs.next()) {
				if (rs.getInt("RECORD_COUNT") > 0) {
					isValid = true;
				}
			}
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		} finally {
			closeResources(conn, ps, rs);
		}
		LOGGER.info("isValidStatus end");
		return isValid;
	}

	/**
	 * This method returns true if a status is an end status.
	 * 
	 * @param lob
	 * @param type
	 * @param fromStatus
	 * @return boolean
	 * @throws SQLException
	 */
	public boolean isEndStatus(String lob, String type, long fromStatus) throws SQLException {

		LOGGER.info("isEndStatus start");
		boolean isValid = true;
		String sql = "SELECT COUNT(*) RECORD_COUNT" + " FROM PROCESS_CONFIGURATIONS" + " WHERE LOB_CODE = ?"
				+ " AND PROC_CODE = ?" + " AND EVNT_FROM_STATUS = ?";
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection conn = null;

		try {
			conn = getConnection();
			ps = conn.prepareStatement(sql);
			ps.setString(1, lob);
			ps.setString(2, type);
			ps.setLong(3, fromStatus);
			rs = ps.executeQuery();
			if (rs.next()) {
				if (rs.getInt("RECORD_COUNT") > 0) {
					isValid = false;
				}
			}
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		} finally {
			closeResources(conn, ps, rs);
		}
		LOGGER.info("isEndStatus end");
		return isValid;
	}// isEndStatus

	public TreeMap getEvents(String userId) throws SQLException {

		LOGGER.info("getEvents start");
		TreeMap notEvents = new TreeMap();
		String sql = "select T1.evnt_id as eventID, T1.evnt_name as eventName, T1.LOB_CODE as lob, T2.upp_notification_ind as notificationInd from process_configurations T1, user_process_preferences T2 where T1.evnt_id = T2.evnt_id and T2.user_id = ? order by T1.LOB_CODE";
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection conn = null;

		try {
			conn = getConnection();
			ps = conn.prepareStatement(sql);
			ps.setString(1, userId);
			rs = ps.executeQuery();
			String lob = "";
			ArrayList lobEvents = null;
			while (rs.next()) {
				NotificationSettingData notSetData = new NotificationSettingData();
				notSetData.setEventId(rs.getString("eventID"));
				notSetData.setEventName(rs.getString("eventName"));
				notSetData.setNotificationInd(rs.getString("notificationInd"));
				if (!lob.equals(rs.getString("lob"))) {
					if (lobEvents != null && !lobEvents.isEmpty()) {
						notEvents.put(lob, lobEvents);
					}
					lobEvents = new ArrayList();
				}
				lob = rs.getString("lob");
				lobEvents.add(notSetData);
			}
			if (lobEvents != null) {
				notEvents.put(lob, lobEvents);
			}
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		} finally {
			closeResources(conn, ps, rs);
		}
		LOGGER.info("getEvents end");
		return notEvents;
	}

	public void updateNotificationEvents(ArrayList eventsList, String userId) throws SQLException {

		LOGGER.info("updateNotificationEvents start");
		StringBuffer updateSql = new StringBuffer(
				"update user_process_preferences set upp_notification_ind = ? where user_id = ?");
		StringBuffer activeEvnts = new StringBuffer(updateSql.toString());
		StringBuffer inactiveEvnts = new StringBuffer(updateSql.toString());
		Iterator it = eventsList.iterator();
		StringBuffer events = new StringBuffer();

		PreparedStatement ps1 = null;
		PreparedStatement ps2 = null;
		Connection conn = null;

		try {

			while (it.hasNext()) {
				events.append("'");
				events.append((String) it.next());
				events.append("'");
				if (it.hasNext()) {
					events.append(",");
				}
			}
			if (events.length() > 0) {
				activeEvnts.append(" and evnt_id in (");
				activeEvnts.append(events);
				activeEvnts.append(")");
				inactiveEvnts.append(" and evnt_id not in (");
				inactiveEvnts.append(events);
				inactiveEvnts.append(")");
			}

			conn = getConnection();
			conn.setAutoCommit(false);

			ps1 = conn.prepareStatement(activeEvnts.toString());
			ps2 = conn.prepareStatement(inactiveEvnts.toString());
			ps1.setString(1, IUMConstants.YES);
			ps1.setString(2, userId);
			ps2.setString(1, null);
			ps2.setString(2, userId);

			if (events.length() > 0) {
				ps1.executeUpdate();
				conn.commit();
			}

			ps2.executeUpdate();
			conn.commit();
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			if (conn != null) {
				conn.rollback();
			}
			throw e;
		} finally {
			closeResources(ps1, null);
			closeResources(ps2, null);
			closeConnection(conn);
		}
		LOGGER.info("updateNotificationEvents end");
	}

}
