/*
 * Created on Jun 25, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.data.dao.test;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import junit.framework.TestCase;

import com.slocpi.ium.data.RequirementData;
import com.slocpi.ium.data.RequirementFormData;
import com.slocpi.ium.data.dao.RequirementDAO;
import com.slocpi.ium.data.util.DataSourceProxy;


/**
 * @author Alain del Rosario
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class RequirementDAOTest extends TestCase {

	/**
	 * Constructor for RequirementDAOTest.
	 * @param arg0
	 */
	public RequirementDAOTest(String arg0) {
		super(arg0);
	}

	

	public void testRetrieveRequirement() throws SQLException {
		long val = 0;
		int num = 0;
		long formid = 420;
		
		RequirementDAO rdao = new RequirementDAO();
		RequirementData rd = new RequirementData();
		rd = rdao.retrieveRequirement("RBQ");
		assertEquals(rd.getReqtCode(),"RBQ");
		assertEquals(rd.getReqtDesc(),"Residential Background Questionnaire");
		assertEquals(rd.getLevel(),"C");
		assertEquals(rd.getValidity(),val);
		assertEquals(rd.getFormIndicator(),"1");
		assertEquals(rd.getFormID(),formid);
		assertEquals(rd.getFollowUpNum(),num);
	}



	public void testRetrieveRequirements() throws SQLException {
		ArrayList alist = new ArrayList();
		
		
		RequirementDAO rdao = new RequirementDAO();
		RequirementData rd = new RequirementData();
		alist = rdao.retrieveRequirements();
		assertTrue(alist.size()==42);
		
	}





	public void testRetrieveRequirementForm() throws SQLException {
		long num = 398;
		
		
		RequirementDAO rdao = new RequirementDAO();
		RequirementFormData  rfd = new RequirementFormData();
		rfd = rdao.retrieveRequirementForm(num);
		assertEquals(rfd.getFormId(),num);
		assertEquals(rfd.getFormName(),"Additional Requirements for the Applicant Under the Anti-Money Laundering Act");
		assertEquals(rfd.getTemplateFormat(),"pdf");
		assertEquals(rfd.getTemplateName(),"Anti-Money Laundering Act");
		assertTrue(!rfd.getStatus());
		
	}




	public void testRetrieveRequirementFormFields() throws SQLException {
		long num = 396;
		ArrayList alist = new ArrayList();
		
		RequirementDAO rdao = new RequirementDAO();
		RequirementData rd = new RequirementData();
		alist = rdao.retrieveRequirementFormFields(num);
		assertTrue(alist.size()==3);
	}



	public void testInsertRequirement() {
	}

	public void testUpdateRequirement() {
	}









	public void testCheckTestDateIndicator() throws SQLException {
		String exp;
		RequirementDAO rdao = new RequirementDAO();
		exp = rdao.checkTestDateIndicator("AMRQ");
		System.out.println(exp);
		assertEquals(exp,"1");
		
	}

















}
