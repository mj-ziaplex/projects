/**
 * ExaminerDAO.java				Copyright (c) 2004 Pointwest Technologies Corp.
 * TODO DOCUMENT ME!
 * 
 * @author aditalo	 @date Jan 9, 2004
 */
package com.slocpi.ium.data.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.slocpi.ium.data.ExaminerData;
import com.slocpi.ium.data.SpecializationData;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.DateHelper;

/**
 * TODO DOCUMENT ME!
 * 
 * @author aditalo		Copyright (c) 2004 Pointwest Technologies Corp.
 * @date Jan 9, 2004
 */
public class ExaminerDAO {

	private static final Logger LOGGER = LoggerFactory.getLogger(ExaminerDAO.class);

	private Connection conn = null;
	
	/**
	 * 
	 */
	public ExaminerDAO(Connection connection) {
		super();
		this.conn = connection;
	}
	
	/**
	 * Method selectExaminers.
	 * @return ArrayList
	 * @throws SQLException
	 */
	

	private void closeResources(PreparedStatement ps, ResultSet rs) throws SQLException{
		
		
		try {
			if (ps != null){
				ps.close();
			}
		}
		catch(SQLException e)
		{
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		}
		try {
			if (rs != null){
				rs.close();
			}			
		} catch (SQLException e){
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		}
		
	}
	
	public ArrayList selectExaminers() throws SQLException {
		
		LOGGER.info("selectExaminers start");
		ArrayList examiners = new ArrayList();
		String sql = "SELECT EXMNR_ID, EXMNR_LAST_NAME, EXMNR_GIVEN_NAME, EXMNR_MIDDLE_NAME, EXMNR_SALUTATION " +
					",EXMNR_SEX, EXMNR_RANK_ID, TO_CHAR(EXMNR_RANK_EFF_DATE, 'DD-MON-YYYY') EXMNR_RANK_EFF_DATE " +
					",TO_CHAR(EXMNR_DOB,'DD-MON-YYYY') EXMNR_DOB, EXMNR_CONTACT_NUM " +
					",EXMNR_FAX_NUM, EXMNR_BUS_ADDR_LINE1, EXMNR_BUS_ADDR_LINE2, EXMNR_BUS_ADDR_LINE3, EXMNR_BUS_CITY " +
					",EXMNR_PROVINCE, EXMNR_COUNTRY, EXMNR_ZIPCODE, EXMNR_MAIL_TO_BUS_IND " +
					",TO_CHAR(EXMNR_CLINIC_HRS_FROM,'HH24:MI') EXMNR_CLINIC_HRS_FROM ,TO_CHAR(EXMNR_CLINIC_HRS_TO,'HH24:MI')EXMNR_CLINIC_HRS_TO " +
					",EXMNR_OTH_INS_CO, EXMNR_PRE_EMP_SRVCE_IND, EXMNR_TIN, EXMNR_ACCREDITATION_IND " +
					",TO_CHAR(EXMNR_ACCREDIT_DATE,'DD-MON-YYYY') EXMNR_ACCREDIT_DATE, EXMNR_AREA, EP_DESC EXMNR_PLACE, A.CREATED_BY " +
					",TO_CHAR(A.CREATED_DATE,'DD-MON-YYYY') CREATED_DATE , A.UPDATED_BY, TO_CHAR(A.UPDATED_DATE,'DD-MON-YYYY') UPDATED_DATE " + 
					"FROM EXAMINERS A, EXAMINATION_PLACES B " +
					"WHERE EXMNR_PLACE = EP_ID(+) " +
					"ORDER BY EXMNR_LAST_NAME, EXMNR_GIVEN_NAME, EXMNR_MIDDLE_NAME ";  // Nic: Should be order by Examiner Name, instead of ID
				
		PreparedStatement prepStmt = conn.prepareStatement(sql);
		ResultSet rs = prepStmt.executeQuery();
		while (rs.next()) {
			ExaminerData examiner = new ExaminerData();
			examiner.setExaminerId(rs.getLong("EXMNR_ID"));
			examiner.setLastName(rs.getString("EXMNR_LAST_NAME"));
			examiner.setFirstName(rs.getString("EXMNR_GIVEN_NAME"));
			examiner.setMiddleName(rs.getString("EXMNR_MIDDLE_NAME"));
			examiner.setSalutation(rs.getString("EXMNR_SALUTATION"));
			examiner.setSex(rs.getString("EXMNR_SEX"));
			examiner.setRank(rs.getString("EXMNR_RANK_ID"));
			examiner.setRankEffectiveDate(DateHelper.parse(rs.getString("EXMNR_RANK_EFF_DATE"),"dd-MMM-yyyy"));
			examiner.setDateOfBirth(DateHelper.parse(rs.getString("EXMNR_DOB"),"dd-MMM-yyyy"));
			examiner.setContactNumber(rs.getString("EXMNR_CONTACT_NUM"));
			examiner.setFaxNumber(rs.getString("EXMNR_FAX_NUM"));
			examiner.setBusAddrLine1(rs.getString("EXMNR_BUS_ADDR_LINE1"));
			examiner.setBusAddrLine2(rs.getString("EXMNR_BUS_ADDR_LINE2"));
			examiner.setBusAddrLine3(rs.getString("EXMNR_BUS_ADDR_LINE3"));
			examiner.setCity(rs.getString("EXMNR_BUS_CITY"));
			examiner.setProvince(rs.getString("EXMNR_PROVINCE"));
			examiner.setCountry(rs.getString("EXMNR_COUNTRY"));
			examiner.setZipCode(rs.getString("EXMNR_ZIPCODE"));
			examiner.setMailToBusAddrInd(rs.getString("EXMNR_MAIL_TO_BUS_IND"));
			examiner.setClinicHrsFrom(DateHelper.parse(rs.getString("EXMNR_CLINIC_HRS_FROM"),"HH:mm"));
			examiner.setClinicHrsTo(DateHelper.parse(rs.getString("EXMNR_CLINIC_HRS_TO"),"HH:mm"));
			examiner.setOtherInsComp(rs.getString("EXMNR_OTH_INS_CO"));
			examiner.setPreEmploymentSrvcInd(rs.getString("EXMNR_PRE_EMP_SRVCE_IND"));
			//examiner.setTIN(rs.getLong("EXMNR_TIN"));
			examiner.setTIN(rs.getString("EXMNR_TIN"));
			examiner.setAccreditationInd(rs.getString("EXMNR_ACCREDITATION_IND"));
			examiner.setAccreditationDate(DateHelper.parse(rs.getString("EXMNR_ACCREDIT_DATE"),"dd-MMM-yyyy"));
			examiner.setExamineArea(rs.getString("EXMNR_AREA"));
			examiner.setExaminePlace(rs.getString("EXMNR_PLACE"));
			examiner.setCreatedBy(rs.getString("CREATED_BY"));
			examiner.setCreateDate(DateHelper.parse(rs.getString("CREATED_DATE"),"dd-MMM-yyyy"));
			examiner.setUpdatedBy(rs.getString("UPDATED_BY"));
			examiner.setUpdateDate(DateHelper.parse(rs.getString("UPDATED_DATE"),"dd-MMM-yyyy"));
			examiners.add(examiner);
		}
		LOGGER.info("selectExaminers end");
		return examiners;
		
	}
	
	/**
	 * Method insertExaminer.
	 * @param examiner
	 */
	//author:ksantos
	public void insertExaminer(ExaminerData examiner) throws SQLException{
		
		LOGGER.info("insertExaminer start");
		PreparedStatement ps = null;
						
		String sql1 = "INSERT INTO EXAMINERS (EXMNR_ID,EXMNR_LAST_NAME,EXMNR_GIVEN_NAME,EXMNR_MIDDLE_NAME, " +
			"EXMNR_SALUTATION,EXMNR_SEX,EXMNR_RANK_ID,EXMNR_RANK_EFF_DATE,EXMNR_DOB, " +
			"EXMNR_CONTACT_NUM,EXMNR_FAX_NUM,EXMNR_BUS_ADDR_LINE1,EXMNR_BUS_ADDR_LINE2, " +
			"EXMNR_BUS_ADDR_LINE3,EXMNR_BUS_CITY,EXMNR_PROVINCE,EXMNR_COUNTRY, " +
			"EXMNR_ZIPCODE,EXMNR_MAIL_TO_BUS_IND,EXMNR_CLINIC_HRS_FROM,EXMNR_CLINIC_HRS_TO, " +
			"EXMNR_OTH_INS_CO,EXMNR_PRE_EMP_SRVCE_IND,EXMNR_TIN,EXMNR_ACCREDITATION_IND, " +
			"EXMNR_ACCREDIT_DATE,EXMNR_AREA,EXMNR_PLACE,CREATED_BY,CREATED_DATE) " +
			"VALUES( " +
			" ?, ?, ?, ?, " +
			" ?, ?, ?, ?, ?, " +
			" ?, ?, ?, ?, " +
			" ?, ?, ?, ?, " +
			" ?, ?, ?, ?, " +
			" ?, ?, ?, ?, " +
			" ?, ?, ?, ?, ? " +
			" )";
		
		
		try{
			ps = conn.prepareStatement(sql1);
			ps.setLong(1,examiner.getExaminerId());
			ps.setString(2,examiner.getLastName());
			ps.setString(3,examiner.getFirstName());
			ps.setString(4,examiner.getMiddleName());
			ps.setString(5,examiner.getSalutation());
			ps.setString(6,examiner.getSex());
			ps.setString(7,examiner.getRank());
			ps.setDate(8,DateHelper.sqlDate(examiner.getRankEffectiveDate()));
			ps.setDate(9,DateHelper.sqlDate(examiner.getDateOfBirth()));
			ps.setString(10,examiner.getContactNumber());
			ps.setString(11,examiner.getFaxNumber());
			ps.setString(12,examiner.getBusAddrLine1());
			ps.setString(13,examiner.getBusAddrLine2());
			ps.setString(14,examiner.getBusAddrLine3());
			ps.setString(15,examiner.getCity());
			ps.setString(16,examiner.getProvince());
			ps.setString(17,examiner.getCountry());
			ps.setString(18,examiner.getZipCode());
			ps.setString(19,examiner.getMailToBusAddrInd());
			
			if(examiner.getClinicHrsFrom() != null){
				ps.setTimestamp(20,new Timestamp(examiner.getClinicHrsFrom().getTime()));
			}else{
				ps.setTimestamp(20,null);
			}
			
			if(examiner.getClinicHrsTo() != null){
				ps.setTimestamp(21,new Timestamp(examiner.getClinicHrsTo().getTime()));
			}else{
				ps.setTimestamp(21,null);
			}
						
			ps.setString(22,examiner.getOtherInsComp());
			ps.setString(23,examiner.getPreEmploymentSrvcInd());
			ps.setString(24,examiner.getTIN());
			ps.setString(25,examiner.getAccreditationInd());
			ps.setDate(26,DateHelper.sqlDate(examiner.getAccreditationDate()));
			ps.setString(27,examiner.getExamineArea());
			ps.setString(28,examiner.getExaminePlace());
			ps.setString(29,examiner.getCreatedBy());
			
			if(examiner.getCreateDate() != null){	
				ps.setTimestamp(30,new Timestamp(examiner.getCreateDate().getTime()));
			}else{
				ps.setTimestamp(30,null);
			}
			
			if(ps.executeUpdate() != 1){
				LOGGER.warn("Error in creating new examiner");
				throw new SQLException("Error in creating new examiner");
				
			}else{
				this.insertExaminerSpecializations(examiner); 
			}
			
		}finally{
			if(ps != null){
				ps.close();
			}
		}
		
		LOGGER.info("insertExaminer end");
	}
	
	//author: ksantos
	private void insertExaminerSpecializations(ExaminerData examiner) throws SQLException{
		
		LOGGER.info("insertExaminerSpecializations start");
		
		ArrayList specializations = examiner.getSpecializations();
		String sql = "INSERT INTO EXAMINER_SPECIALIZATIONS(EXMNR_ID,SPL_ID) " +
			"VALUES(?,?) ";
		PreparedStatement ps = null;
		
		try{
			if(specializations.size() > 0){
				ps = conn.prepareStatement(sql);
				for(int i=0;i<specializations.size();i++){
					ps.setLong(1,examiner.getExaminerId());
					ps.setLong(2,((SpecializationData)specializations.get(i)).getSpecializationId());
					if(ps.executeUpdate() != 1){
						LOGGER.warn("Error in creating Specialization for Examiner "+examiner.getExaminerId());
						throw new SQLException("Error in creating Specialization for Examiner "+examiner.getExaminerId());
					}	
				}
			}
		}finally{
			if(ps != null){
				ps.close();
			}
		}
		LOGGER.info("insertExaminerSpecializations end");
	}
	

	/**
	 * Method selectExaminer.
	 * @param examinerId
	 * @return ExaminerData
	 * @throws SQLException
	 */
	//author: ksantos
	//to_char(CREATED_DATE, 'dd-mon-yyyy') CREATED_DATE
	public ExaminerData selectExaminer(long examinerId) throws SQLException{
		
		LOGGER.info("selectExaminer start");
		
		ExaminerData examiner = null;
		String sql = "SELECT EXMNR_LAST_NAME, EXMNR_GIVEN_NAME, EXMNR_MIDDLE_NAME, EXMNR_SALUTATION " +
			",EXMNR_SEX, EXMNR_RANK_ID, TO_CHAR(EXMNR_RANK_EFF_DATE, 'DD-MON-YYYY') EXMNR_RANK_EFF_DATE " +
			",TO_CHAR(EXMNR_DOB,'DD-MON-YYYY') EXMNR_DOB, EXMNR_CONTACT_NUM " +
			",EXMNR_FAX_NUM, EXMNR_MOBILE_NUM, EXMNR_BUS_ADDR_LINE1, EXMNR_BUS_ADDR_LINE2, EXMNR_BUS_ADDR_LINE3 " +
			",EXMNR_BUS_CITY,EXMNR_PROVINCE, EXMNR_COUNTRY, EXMNR_ZIPCODE, EXMNR_MAIL_TO_BUS_IND " +
			",TO_CHAR(EXMNR_CLINIC_HRS_FROM,'HH24:MI') EXMNR_CLINIC_HRS_FROM, TO_CHAR(EXMNR_CLINIC_HRS_TO,'HH24:MI') EXMNR_CLINIC_HRS_TO " +
			",EXMNR_OTH_INS_CO ,EXMNR_PRE_EMP_SRVCE_IND, EXMNR_TIN, EXMNR_ACCREDITATION_IND " +
			",TO_CHAR(EXMNR_ACCREDIT_DATE,'DD-MON-YYYY') EXMNR_ACCREDIT_DATE , EXMNR_AREA, EXMNR_PLACE, CREATED_BY " +
			",TO_CHAR(CREATED_DATE,'DD-MON-YYYY') CREATED_DATE, UPDATED_BY, TO_CHAR(UPDATED_DATE,'DD-MON-YYYY') UPDATED_DATE " +
			"FROM EXAMINERS " +
			"WHERE EXMNR_ID = ? ";
		PreparedStatement ps = null;
		ResultSet rs = null;
			
		try{
			ps = conn.prepareStatement(sql);
			ps.setLong(1,examinerId);
			rs = ps.executeQuery();
			
			while(rs.next()){
				examiner = new ExaminerData();
				examiner.setExaminerId(examinerId);
				examiner.setLastName(rs.getString("EXMNR_LAST_NAME"));
				examiner.setFirstName(rs.getString("EXMNR_GIVEN_NAME"));
				examiner.setMiddleName(rs.getString("EXMNR_MIDDLE_NAME"));
				examiner.setSalutation(rs.getString("EXMNR_SALUTATION"));
				examiner.setSex(rs.getString("EXMNR_SEX"));
				examiner.setRank(rs.getString("EXMNR_RANK_ID"));
				examiner.setRankEffectiveDate(DateHelper.parse(rs.getString("EXMNR_RANK_EFF_DATE"),"dd-MMM-yyyy"));
				examiner.setDateOfBirth(DateHelper.parse(rs.getString("EXMNR_DOB"),"dd-MMM-yyyy"));
				examiner.setContactNumber(rs.getString("EXMNR_CONTACT_NUM"));
				examiner.setFaxNumber(rs.getString("EXMNR_FAX_NUM"));
				examiner.setMobileNumber(rs.getString("EXMNR_MOBILE_NUM"));
				examiner.setBusAddrLine1(rs.getString("EXMNR_BUS_ADDR_LINE1"));
				examiner.setBusAddrLine2(rs.getString("EXMNR_BUS_ADDR_LINE2"));
				examiner.setBusAddrLine3(rs.getString("EXMNR_BUS_ADDR_LINE3"));
				examiner.setCity(rs.getString("EXMNR_BUS_CITY"));
				examiner.setProvince(rs.getString("EXMNR_PROVINCE"));
				examiner.setCountry(rs.getString("EXMNR_COUNTRY"));
				examiner.setZipCode(rs.getString("EXMNR_ZIPCODE"));
				examiner.setMailToBusAddrInd(rs.getString("EXMNR_MAIL_TO_BUS_IND"));
				examiner.setClinicHrsFrom(DateHelper.parse(rs.getString("EXMNR_CLINIC_HRS_FROM"),"HH:mm"));
				examiner.setClinicHrsTo(DateHelper.parse(rs.getString("EXMNR_CLINIC_HRS_TO"),"HH:mm"));
				examiner.setOtherInsComp(rs.getString("EXMNR_OTH_INS_CO"));
				examiner.setPreEmploymentSrvcInd(rs.getString("EXMNR_PRE_EMP_SRVCE_IND"));
				examiner.setTIN(rs.getString("EXMNR_TIN"));
				examiner.setAccreditationInd(rs.getString("EXMNR_ACCREDITATION_IND"));
				examiner.setAccreditationDate(DateHelper.parse(rs.getString("EXMNR_ACCREDIT_DATE"),"dd-MMM-yyyy"));
				examiner.setExamineArea(rs.getString("EXMNR_AREA"));
				examiner.setExaminePlace(rs.getString("EXMNR_PLACE"));
				examiner.setSpecializations(this.selectExaminerSpecializations(examinerId));
				examiner.setCreatedBy(rs.getString("CREATED_BY"));
				examiner.setCreateDate(DateHelper.parse(rs.getString("CREATED_DATE"),"dd-MMM-yyyy"));
				examiner.setUpdatedBy(rs.getString("UPDATED_BY"));
				examiner.setUpdateDate(DateHelper.parse(rs.getString("UPDATED_DATE"),"dd-MMM-yyyy"));
				
			}			
		}finally{
			if(rs != null){
				rs.close();
			}
			if(ps != null){
				ps.close();
			}
		}
		
		LOGGER.info("selectExaminer end");
		return examiner;
	}
	
	//author: kristian
	private ArrayList selectExaminerSpecializations(long examinerId) throws SQLException{
		
		LOGGER.info("selectExaminerSpecializations start");
		ArrayList list = new ArrayList();
		String sql = "SELECT A.SPL_ID, B.SPL_DESC " +
			"FROM EXAMINER_SPECIALIZATIONS A, SPECIALIZATIONS B " +
			"WHERE A.EXMNR_ID = ? " +
			"AND A.SPL_ID = B.SPL_ID";
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try{
			ps = conn.prepareStatement(sql);
			ps.setLong(1,examinerId);
			rs = ps.executeQuery();
			
			while(rs.next()){
				SpecializationData sp = new SpecializationData();
				sp.setSpecializationId(rs.getLong("SPL_ID"));
				sp.setSpecializationDesc(rs.getString("SPL_DESC"));
				list.add(sp);
			}		
		}finally{
			if(rs != null){
				rs.close();
			}
			if(ps != null){
				ps.close();
			}
		}				
		LOGGER.info("selectExaminerSpecializations end");
				
		return list;
	}
	
	/**
	 * Method updateExaminer.
	 * @param examiner
	 * @throws SQLException
	 */
	//author: kristian
	public void updateExaminer(ExaminerData examiner) throws SQLException {
		
		LOGGER.info("updateExaminer start");
		String sql = "UPDATE EXAMINERS " +
			"SET EXMNR_LAST_NAME = ? , EXMNR_GIVEN_NAME = ? , EXMNR_MIDDLE_NAME = ? , " +
			"EXMNR_SALUTATION = ? , EXMNR_SEX = ? , EXMNR_RANK_ID = ? , EXMNR_RANK_EFF_DATE = ? , " +
			"EXMNR_DOB = ? , EXMNR_CONTACT_NUM = ? , EXMNR_FAX_NUM = ? , EXMNR_MOBILE_NUM = ? , " +
			"EXMNR_BUS_ADDR_LINE1 = ? , EXMNR_BUS_ADDR_LINE2 = ? , EXMNR_BUS_ADDR_LINE3 = ? , " +
			"EXMNR_BUS_CITY = ? , EXMNR_PROVINCE = ? , EXMNR_COUNTRY = ? , EXMNR_ZIPCODE = ? , " +
			"EXMNR_MAIL_TO_BUS_IND = ? , EXMNR_CLINIC_HRS_FROM = ? , EXMNR_CLINIC_HRS_TO = ? , " +
			"EXMNR_OTH_INS_CO = ? , EXMNR_PRE_EMP_SRVCE_IND = ? , EXMNR_TIN = ? , " +
			"EXMNR_ACCREDITATION_IND = ? , EXMNR_ACCREDIT_DATE = ? , " +
			"EXMNR_PLACE = ? , UPDATED_BY = ? , UPDATED_DATE = ? " +
			"WHERE EXMNR_ID = ? ";
		PreparedStatement ps = null;
		
		try{
			ps = conn.prepareStatement(sql);
			
			ps.setString(1,examiner.getLastName());
			ps.setString(2,examiner.getFirstName());
			ps.setString(3,examiner.getMiddleName());
			ps.setString(4,examiner.getSalutation());
			ps.setString(5,examiner.getSex());
			ps.setString(6,examiner.getRank());
			ps.setDate(7,DateHelper.sqlDate(examiner.getRankEffectiveDate()));
			ps.setDate(8,DateHelper.sqlDate(examiner.getDateOfBirth()));
			ps.setString(9,examiner.getContactNumber());
			ps.setString(10,examiner.getFaxNumber());
			ps.setString(11, examiner.getMobileNumber());
			ps.setString(12,examiner.getBusAddrLine1());
			ps.setString(13,examiner.getBusAddrLine2());
			ps.setString(14,examiner.getBusAddrLine3());
			ps.setString(15,examiner.getCity());
			ps.setString(16,examiner.getProvince());
			ps.setString(17,examiner.getCountry());
			ps.setString(18,examiner.getZipCode());
			ps.setString(19,examiner.getMailToBusAddrInd());
			
			if(examiner.getClinicHrsFrom() != null){	
				ps.setTimestamp(20,new Timestamp(examiner.getClinicHrsFrom().getTime()));
			}else{
				ps.setTimestamp(20,null);
			}
						
			if(examiner.getClinicHrsTo() != null){
				ps.setTimestamp(21,new Timestamp(examiner.getClinicHrsTo().getTime()));
			}else{
				ps.setTimestamp(21,null);
			}	
			ps.setString(22,examiner.getOtherInsComp());
			ps.setString(23,examiner.getPreEmploymentSrvcInd());
			ps.setString(24,examiner.getTIN());
			ps.setString(25,examiner.getAccreditationInd());
			ps.setDate(26,DateHelper.sqlDate(examiner.getAccreditationDate()));
			ps.setString(27,examiner.getExaminePlace());
			ps.setString(28,examiner.getUpdatedBy());
			
			if(examiner.getUpdateDate() != null){
				ps.setTimestamp(29,new Timestamp(examiner.getUpdateDate().getTime()));
			}else{
				ps.setTimestamp(29,null);
			}
			
			ps.setLong(30,examiner.getExaminerId());
			
			if(ps.executeUpdate() != 1){
				LOGGER.error("Error in updating examiner");
				throw new SQLException("Error in updating examiner");
			}else{
				this.deleteExaminerSpecializations(examiner);
				this.insertExaminerSpecializations(examiner);
			}
			
			
		}finally{
			if(ps != null){
				ps.close();
			}
		}	
		LOGGER.info("updateExaminer end");
	}
	
	private void deleteExaminerSpecializations(ExaminerData examiner) throws SQLException{
		
		LOGGER.info("deleteExaminerSpecializations start");
		
		ArrayList specializations = examiner.getSpecializations();
		String sql = "DELETE EXAMINER_SPECIALIZATIONS " +
			"WHERE EXMNR_ID = ? ";
		PreparedStatement ps = null;
		
		try{
			ps = conn.prepareStatement(sql);
			ps.setLong(1,examiner.getExaminerId());
			
			ps.executeUpdate();
			
		}finally{
			if(ps != null){
				ps.close();
			}
		}
		LOGGER.info("deleteExaminerSpecializations end");
	}
	
    
}
