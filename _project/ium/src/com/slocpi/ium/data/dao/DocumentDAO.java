package com.slocpi.ium.data.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.slocpi.ium.data.DocumentData;
import com.slocpi.ium.data.DocumentTypeData;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.DateHelper;


/**
 * Data Access Object of the Documents in Assessment Request. Access the FOLDER_DOCUMENTS table.
 * @author Engel
 * 
 */
public class DocumentDAO {

	private static final Logger LOGGER = LoggerFactory.getLogger(DocumentDAO.class);

	private Connection conn;
	
	
	public DocumentDAO(Connection _conn) {
		conn = _conn;
	}
	
	private void closeResources(PreparedStatement ps, ResultSet rs) throws SQLException{
		
		
		try {
			if (ps != null){
				ps.close();
			}
		}
		catch(SQLException e)
		{
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		}
		try {
			if (rs != null){
				rs.close();
			}			
		} catch (SQLException e){
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		}
		
	}

	
	public ArrayList retrieveDocuments(String refNo) throws Exception {
		
		LOGGER.info("retrieveDocuments start");
		String sql = "SELECT" +  
  					 	" DOC.DOC_ID as ID, " + 
	  				 	" DOC.UAR_REFERENCE_NUM as REFERENCE_NUMBER, " +
  					 	" DOC.DOC_CODE as CODE, " +
  					 	" TYP.DOC_DESC as DESCRIPTION, " +
  					 	" DOC.DOC_DATE_RCVD as RECEIVED_DATE, " + 
	  				 	" DOC.DOC_RECEIVED_BY as RECEIVED_BY, " +
  					 	" DOC.DOC_REFERENCE_NUM as DOC_REFERENCE_NUMBER, " +
  					 	" DOC.DOC_DATE_REQUESTED as DATE_REQUESTED " + 
					"FROM" + 
  					 	" FOLDER_DOCUMENTS DOC, DOCUMENT_TYPES TYP " +
					"WHERE" + 
  					 	" DOC.DOC_CODE = TYP.DOC_CODE AND " +
  					 	" DOC.UAR_REFERENCE_NUM = ? " +
					"ORDER BY" +
					 	" DOC.DOC_DATE_RCVD DESC ";
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try {

			ps = conn.prepareStatement(sql);
			ps.setString(1, refNo);
			rs = ps.executeQuery();

			ArrayList list = new ArrayList();
			while (rs.next()) {
				DocumentData docData = new DocumentData();
				
				DocumentTypeData document = new DocumentTypeData();
				document.setDocCode(rs.getString("CODE"));
				document.setDocDesc(rs.getString("DESCRIPTION"));
								
				docData.setDocId(rs.getLong("ID"));
				docData.setRefNo(rs.getString("REFERENCE_NUMBER"));
				docData.setDocument(document);
				docData.setDateReceived(rs.getDate("RECEIVED_DATE"));
				docData.setReceivedBy(rs.getString("RECEIVED_BY"));
				docData.setDocRefNo(rs.getLong("DOC_REFERENCE_NUMBER"));
				docData.setDateRequested(rs.getDate("DATE_REQUESTED"));
				
				list.add(docData);
		  	}
 
		  	return (list);	
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e; 
		}finally{
			this.closeResources(ps, rs);
			LOGGER.info("retrieveDocuments end");
		}	
	}// retrieveDocuments


	public void insertDocument(DocumentData data) throws SQLException {
		
		LOGGER.info("insertDocument start");
		String sql = "INSERT INTO FOLDER_DOCUMENTS (" +
						"DOC_ID, " +
						"UAR_REFERENCE_NUM, " +
						"DOC_CODE, " +
						"DOC_DATE_RCVD, " +
						"DOC_RECEIVED_BY, " +
						"DOC_REFERENCE_NUM," +
						"DOC_DATE_REQUESTED " +
						")" +
					"VALUES(SEQ_FOLDER_DOCUMENT.NEXTVAL,?,?,?,?,?,?)";					

		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			
			ps = conn.prepareStatement(sql);
		
			String refNo = data.getRefNo(); 
			if (refNo != null) {
				ps.setString(1, refNo);
			} else {
				ps.setString(1, null);
			}

			String docCode = data.getDocument().getDocCode(); 
			if (docCode != null) {
				ps.setString(2, docCode);
			} else {
				ps.setString(2, null);
			}			
		
			Date dateReceive = data.getDateReceived(); 
			if (dateReceive != null) {
				ps.setDate(3, DateHelper.sqlDate(dateReceive));
			} else {
				ps.setDate(3, null);
			}

			String receivedBy = data.getReceivedBy(); 
			if (receivedBy != null) {
				ps.setString(4, receivedBy);
			} else {
				ps.setString(4, null);
			}

			long docRefNo = data.getDocRefNo(); 
			if (docRefNo != 0) {
				ps.setLong(5, docRefNo);
			} else {
				ps.setLong(5, 0);
			}
			
			Date dateRequested = data.getDateRequested();
			if (dateRequested != null){
				ps.setDate(6, DateHelper.sqlDate(dateRequested));
			} else {
				ps.setDate(6, null);
			}
									
			ps.executeUpdate();
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e; 
		}finally{
			this.closeResources(ps, rs);
			LOGGER.info("insertDocument end");
		}
	}// insertDocument
	

	public boolean isDuplicateCode(String refNo, String docCode) throws SQLException {
		
		LOGGER.info("isDuplicateCode start");
		String sql = "SELECT UAR_REFERENCE_NUM, DOC_CODE " +
					 "FROM FOLDER_DOCUMENTS " +
					 "WHERE UAR_REFERENCE_NUM = ? AND " +
						"DOC_CODE = ?";
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			
			ps = conn.prepareStatement(sql);
			ps.setString(1, refNo);
			ps.setString(2, docCode);

			rs = ps.executeQuery();
			if (rs.next()) {
				return true;   
			}
			else {
				return false;
			}
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e; 
		}finally{
			this.closeResources(ps, rs);
			LOGGER.info("isDuplicateCode end");
		}		
	}// isDuplicateCode
	
	public void deleteDocuments(String referenceNum) throws SQLException {
		
		LOGGER.info("deleteDocuments start");
		String sql = "DELETE FOLDER_DOCUMENTS WHERE UAR_REFERENCE_NUM = ?";
		PreparedStatement ps = null;
		ResultSet rs = null;
		try{
			
		ps = conn.prepareStatement(sql);
		ps.setString(1, referenceNum);
		ps.executeUpdate();
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e; 
		}finally{
			this.closeResources(ps, rs);
			LOGGER.info("deleteDocuments end");
		}
	}
	


}
