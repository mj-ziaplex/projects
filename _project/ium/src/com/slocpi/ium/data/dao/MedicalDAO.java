package com.slocpi.ium.data.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.slocpi.ium.IUMException;
import com.slocpi.ium.data.ARWithPendingMedData;
import com.slocpi.ium.data.ARWithPendingMedFilterData;
import com.slocpi.ium.data.ClientData;
import com.slocpi.ium.data.ExaminationPlaceData;
import com.slocpi.ium.data.ExaminerData;
import com.slocpi.ium.data.LaboratoryData;
import com.slocpi.ium.data.MedExamFiltersData;
import com.slocpi.ium.data.MedicalExamBillingData;
import com.slocpi.ium.data.MedicalExamBillingFilter;
import com.slocpi.ium.data.MedicalNotes;
import com.slocpi.ium.data.MedicalRecordData;
import com.slocpi.ium.data.PolicyMedicalRecordData;
import com.slocpi.ium.data.SectionData;
import com.slocpi.ium.data.StatusData;
import com.slocpi.ium.data.SunLifeDeptData;
import com.slocpi.ium.data.SunLifeOfficeData;
import com.slocpi.ium.data.TestProfileData;
import com.slocpi.ium.data.UserProfileData;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.DateHelper;
import com.slocpi.ium.util.IUMConstants;
import com.slocpi.ium.util.SortHelper;
import com.slocpi.ium.util.ValueConverter;

/**
 * @author Cris
 *
 * Container for SQL�s performed on  medical.
 */
public class MedicalDAO {

	private static final Logger LOGGER = LoggerFactory.getLogger(MedicalDAO.class);

	private Connection conn = null;	

    public MedicalDAO(Connection conn) {
        super();
		this.conn = conn;
    }
    
	public void closeDB() {
		
		LOGGER.info("closeDB start");
		try {
			this.conn.close();
		} catch (Exception e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
		}
		LOGGER.info("closeDB end");
	}

	public void getMedicalDeptProfile(){
	}

	public ArrayList getMedicalRecords(MedExamFiltersData filter, SortHelper sort) throws SQLException{
		
		LOGGER.info("getMedicalRecords start");
		ArrayList records = new ArrayList();
		String selectClause = 
		"SELECT " +
			"MED.MED_RECORD_ID, " +
			"MED.CL_CLIENT_NUM, " +
			"DECODE(MED.CL_CLIENT_NUM, NULL, MED.CL_GIVEN_NAME, CLI.CL_GIVEN_NAME) CL_GIVEN_NAME, " +
			"DECODE(MED.CL_CLIENT_NUM, NULL, MED.CL_LAST_NAME, CLI.CL_LAST_NAME) CL_LAST_NAME, " +
			"TO_CHAR(DECODE(MED.CL_CLIENT_NUM, NULL, MED.CL_BIRTH_DATE, CLI.CL_BIRTH_DATE), 'DD-MON-YYYY') CL_BIRTH_DATE, " +
			"MED.AGENT_ID, " +
			"MED.BRANCH_ID, " +
			"MED.MED_STATUS, " +
			"STA.STAT_DESC, " +
			"MED.MED_LAB_TEST_IND, " +					    
			"MED.MED_LAB_ID, " +
			"LAB.LAB_NAME, " +
			"MED.EXMNR_ID, " +
			"EX.EXMNR_GIVEN_NAME, " +
			"EX.EXMNR_MIDDLE_NAME, " +
			"EX.EXMNR_LAST_NAME, " +
			"MED_EXAMINATION_PLACE, " +
			"EP_DESC, " +
			"MED.MED_TEST_ID, " +
			"TSP.TEST_DESC, " +
			"TO_CHAR(MED.MED_APPOINTMENT_DATE, 'DD-MON-YYYY HH:MI AM') MED_APPOINTMENT_DATE, " +
			"TO_CHAR(MED.MED_DATE_CONDUCTED, 'DD-MON-YYYY') MED_DATE_CONDUCTED, " +
			"TO_CHAR(MED.MED_DATE_RECEIVED, 'DD-MON-YYYY') MED_DATE_RECEIVED, " +
            "MED.MED_FOLLOW_UP_NUM, " +
		    "TO_CHAR(MED.MED_FOLLOW_UP_DATE, 'DD-MON-YYYY') MED_FOLLOW_UP_DATE, " +
		    "TO_CHAR(MED.MED_VALIDITY_DATE, 'DD-MON-YYYY') MED_VALIDITY_DATE, " + 
		    "MED.MED_7DAY_MEMO_IND, " +
		    "TO_CHAR(MED.MED_7DAY_MEMO_DATE, 'DD-MON-YYYY') MED_7DAY_MEMO_DATE, " +		    
		    "MED.MED_REQT_REASON, " +
			"MED.MED_SECTION_ID, " +
			"SEC.SEC_DESC, " +
		    "MED.MED_REQUESTING_PARTY, " +
            "MED.MED_DEPARTMENT_ID, " +
			"DEPT.DEPT_DESC, " +
		    "MED.MED_CHARGEABLE_TO, " + 
		    "MED.MED_PAID_BY_AGENT_IND, " +
		    "MED.MED_AMOUNT, " +
		    "MED.MED_REQUEST_LOA_IND, " +
		    "MED.MED_RESULTS_RCVD, " +
			"TO_CHAR(MED.MED_REQUEST_DATE, 'DD-MON-YYYY') MED_REQUEST_DATE, " +
			"POL.UAR_REFERENCE_NUM, " +
			"AR.ASSIGNED_TO, " +
			"AR.STATUS_ID, " +
			"MED.MED_REMARKS, " +
		    "MED.CREATED_BY, " +
		    "TO_CHAR(MED.CREATED_DATE, 'DD-MON-YYYY') CREATED_DATE, " +
		    "MED.UPDATED_BY, " +
			"TO_CHAR(MED.UPDATED_DATE, 'DD-MON-YYYY') UPDATED_DATE " +
		"FROM " +
			"CLIENTS CLI, " +
			"STATUS STA, " +  
			"LABORATORIES LAB, " +
			"EXAMINERS EX, " +
			"EXAMINATION_PLACES EP, " +
			"TEST_PROFILES TSP, " +
			"SECTIONS SEC, " +
			"DEPARTMENTS DEPT, " +
			"POLICY_MEDICAL_RECORDS POL, " +
			"ASSESSMENT_REQUESTS AR, " +
			"MEDICAL_RECORDS MED " +
		"WHERE " +
			"MED.CL_CLIENT_NUM = CLI.CLIENT_ID (+) AND " +
			"MED.MED_STATUS = STA.STAT_ID AND " +
			"MED.MED_LAB_ID = LAB.LAB_ID (+) AND " +			
			"MED.EXMNR_ID = EX.EXMNR_ID (+) AND " +
			"MED.MED_EXAMINATION_PLACE = EP.EP_ID (+) AND " +
			"MED.MED_TEST_ID = TSP.TEST_ID AND " +
			"MED.MED_SECTION_ID = SEC.SEC_CODE AND " +
			"MED.MED_DEPARTMENT_ID = DEPT.DEPT_CODE AND " +
			"MED.MED_RECORD_ID = POL.MED_RECORD_ID (+) AND " +
			"POL.UAR_REFERENCE_NUM = AR.REFERENCE_NUM (+) " +
			filter.getFilter() +
			sort.getOrderBy(); 

		Statement stmt = conn.createStatement();
		ResultSet rs = stmt.executeQuery(selectClause);
		while(rs.next()){
			MedicalRecordData medData = new MedicalRecordData();
			medData.setMedicalRecordId(rs.getLong("MED_RECORD_ID"));

			ClientData client = new ClientData();
			client.setClientId(rs.getString("CL_CLIENT_NUM"));
			client.setGivenName(rs.getString("CL_GIVEN_NAME"));
			client.setLastName(rs.getString("CL_LAST_NAME"));
			client.setBirthDate(DateHelper.parse(rs.getString("CL_BIRTH_DATE"), "dd-MMM-yyyy"));
			medData.setClient(client);

			UserProfileData agent = new UserProfileData();
			agent.setUserId(rs.getString("AGENT_ID"));
			medData.setAgent(agent);

			SunLifeOfficeData branch = new SunLifeOfficeData();
			branch.setOfficeId(rs.getString("BRANCH_ID"));
			medData.setBranch(branch);

			StatusData status = new StatusData();
			status.setStatusId(rs.getLong("MED_STATUS"));
			status.setStatusDesc(rs.getString("STAT_DESC"));
			medData.setStatus(status);

			medData.setLabTestInd(rs.getString("MED_LAB_TEST_IND"));

			if (rs.getLong("MED_LAB_ID") != 0) {
				LaboratoryData laboratory = new LaboratoryData();
				laboratory.setLabId(rs.getLong("MED_LAB_ID"));
				laboratory.setLabName(rs.getString("LAB_NAME"));				
				medData.setLaboratory(laboratory);			
			}

			if (rs.getLong("EXMNR_ID") != 0) {
				ExaminerData examiner = new ExaminerData();
				examiner.setExaminerId(rs.getLong("EXMNR_ID"));
				examiner.setLastName(rs.getString("EXMNR_LAST_NAME"));
				examiner.setFirstName(rs.getString("EXMNR_GIVEN_NAME"));
				examiner.setMiddleName(rs.getString("EXMNR_MIDDLE_NAME"));
				medData.setExaminer(examiner);
			}

			if (rs.getLong("MED_EXAMINATION_PLACE") != 0) {
				ExaminationPlaceData examinationPlace = new ExaminationPlaceData();
				examinationPlace.setExaminationPlaceId(rs.getLong("MED_EXAMINATION_PLACE"));
				examinationPlace.setExaminationPlaceDesc(rs.getString("EP_DESC"));
				medData.setExaminationPlace(examinationPlace);
			}

			TestProfileData test = new TestProfileData();
			test.setTestId(rs.getLong("MED_TEST_ID"));
			test.setTestDesc(rs.getString("TEST_DESC"));
			medData.setTest(test);

			medData.setAppointmentDateTime(DateHelper.parse(rs.getString("MED_APPOINTMENT_DATE"), "dd-MMM-yyyy hh:mm a"));
			medData.setConductedDate(DateHelper.parse(rs.getString("MED_DATE_CONDUCTED"), "dd-MMM-yyyy"));
			medData.setReceivedDate(DateHelper.parse(rs.getString("MED_DATE_RECEIVED"), "dd-MMM-yyyy"));
			medData.setFollowUpNumber(rs.getLong("MED_FOLLOW_UP_NUM"));
			medData.setFollowUpDate(DateHelper.parse(rs.getString("MED_FOLLOW_UP_DATE"), "dd-MMM-yyyy"));
			medData.setValidityDate(DateHelper.parse(rs.getString("MED_VALIDITY_DATE"), "dd-MMM-yyyy"));
			medData.setSevenDayMemoInd(rs.getBoolean("MED_7DAY_MEMO_IND"));
			medData.setSevenDayMemoDate(DateHelper.parse(rs.getString("MED_7DAY_MEMO_DATE"), "dd-MMM-yyyy"));
			medData.setReasonForReqt(rs.getString("MED_REQT_REASON"));

			SectionData section = new SectionData();
			section.setSectionId(rs.getString("MED_SECTION_ID"));
			section.setSectionDesc(rs.getString("SEC_DESC"));
			medData.setSection(section);

			medData.setRequestingParty(rs.getString("MED_REQUESTING_PARTY"));

			SunLifeDeptData department = new SunLifeDeptData();
			department.setDeptId(rs.getString("MED_DEPARTMENT_ID"));
			department.setDeptDesc(rs.getString("DEPT_DESC"));
			medData.setDepartment(department);

			medData.setChargableTo(rs.getString("MED_CHARGEABLE_TO"));
			medData.setPaidByAgentInd(rs.getBoolean("MED_PAID_BY_AGENT_IND"));
			medData.setAmount(rs.getDouble("MED_AMOUNT"));
			medData.setRequestForLOAInd(rs.getBoolean("MED_REQUEST_LOA_IND"));
			medData.setReceivedResults(rs.getBoolean("MED_RESULTS_RCVD"));
			medData.setRequestedDate(DateHelper.parse(rs.getString("MED_REQUEST_DATE"), "dd-MMM-yyyy"));
			medData.setRemarks(rs.getString("MED_REMARKS"));

			PolicyMedicalRecordData policyMedicalRecord = new PolicyMedicalRecordData();
			policyMedicalRecord.setReferenceNumber(rs.getString("UAR_REFERENCE_NUM"));
			medData.setPolicyMedicalRecordData(policyMedicalRecord);
			
			medData.setReferenceNo(rs.getString("UAR_REFERENCE_NUM"));
			medData.setFacilitator(rs.getString("ASSIGNED_TO"));
			
			if (rs.getLong("STATUS_ID") != 0) {
				StatusData ARStatus = new StatusData();
				ARStatus.setStatusId(rs.getLong("STATUS_ID"));
				medData.setApplicationStatus(ARStatus);
			}

			medData.setCreatedBy(rs.getString("CREATED_BY"));
			medData.setCreateDate(DateHelper.parse(rs.getString("CREATED_DATE"), "dd-MMM-yyyy"));
			medData.setUpdatedBy(rs.getString("UPDATED_BY"));
			medData.setUpdatedDate(DateHelper.parse(rs.getString("UPDATED_DATE"), "dd-MMM-yyyy"));

			records.add(medData);
		}
		LOGGER.info("getMedicalRecords end");
		return records;		
	}
	

	//Retrieves MedicalRecords by assessment request reference number
	public ArrayList retrieveMedicalRecords(String referenceNumber) throws SQLException {
		
		LOGGER.info("retrieveMedicalRecords start");
		ArrayList polMedRecList = new ArrayList();
		
		MedExamFiltersData filter = new MedExamFiltersData();
		filter.setReferenceNo(referenceNumber);
		SortHelper sort = new SortHelper(IUMConstants.TABLE_MEDICAL_RECORDS);
		polMedRecList = this.getMedicalRecords(filter, sort);

		LOGGER.info("retrieveMedicalRecords end");
		return polMedRecList;
	}
	

	// retrieves a medical record by medical record id
	public MedicalRecordData retrieveMedicalRecord(long medicalRecordId) throws SQLException{

		LOGGER.info("retrieveMedicalRecord start");
		MedicalRecordData medData = new MedicalRecordData();
		
		MedExamFiltersData filter = new MedExamFiltersData();
		filter.setMedicalRecordId(medicalRecordId);
		SortHelper sort = new SortHelper(IUMConstants.TABLE_MEDICAL_RECORDS);
		ArrayList polMedRecList = this.getMedicalRecords(filter, sort);
		if (polMedRecList.size() > 0) {
			medData = (MedicalRecordData) polMedRecList.get(0);
		}
		LOGGER.info("retrieveMedicalRecord end");
		return medData;
	}
	
	//updates chargeable to field of medical records data
	public void updateChargeableTo(String referenceNumber, String chargeTo, String updatedBy) throws SQLException{
		
		LOGGER.info("updateChargeableTo start");
		String sql = 
			"UPDATE MEDICAL_RECORDS " +
				"SET MED_CHARGEABLE_TO = ?, " +
				"UPDATED_BY = ?, " +
				"UPDATED_DATE = ? " +
			"WHERE " +
				"MEDICAL_RECORDS.med_record_id " +
				"IN (SELECT MED_RECORD_ID FROM POLICY_MEDICAL_RECORDS WHERE UAR_REFERENCE_NUM = ?)";
		
		PreparedStatement stmt = conn.prepareStatement(sql);
		stmt.setString(1, chargeTo);
		stmt.setString(2, updatedBy);
		stmt.setTimestamp(3,DateHelper.sqlTimestamp(new java.util.Date()));
		stmt.setString(4, referenceNumber);
		stmt.executeUpdate();
		LOGGER.info("updateChargeableTo end");
	}
	
	public void updateStatus(String referenceNumber, long newStatus, String updatedBy) throws SQLException{
		
		LOGGER.info("updateStatus start");
		String sql = 
			"UPDATE MEDICAL_RECORDS " +
				"SET MED_STATUS = ?, " +
				"UPDATED_BY = ?, " +
				"UPDATED_DATE = ? " +
			"WHERE MEDICAL_RECORDS.med_record_id IN " +
					 "(SELECT MED_RECORD_ID FROM POLICY_MEDICAL_RECORDS WHERE UAR_REFERENCE_NUM = ?)";

		PreparedStatement stmt = conn.prepareStatement(sql);
		stmt.setLong(1, newStatus);
		stmt.setString(2, updatedBy);
		stmt.setTimestamp(3, DateHelper.sqlTimestamp(new java.util.Date()));
		stmt.setString(4, referenceNumber);
		stmt.executeUpdate();
		
		LOGGER.info("updateStatus end");
	}

/*	public UserProfileData getMedicalDeptProfile(String deptId) throws SQLException {

		LOGGER.info("getMedicalDeptProfile start");
			String selectUser = 
				"SELECT " +
					"USER_ID, " +
					"USR_ACF2ID, " +
					"USR_PASSWORD, " +
					"USR_LAST_NAME, " +
					"USR_FIRST_NAME, " +
					"USR_MIDDLE_NAME, " +
					"USR_CODE, " +
					"USR_TYPE, " +
					"USR_EMAIL_ADDRESS, " +
					"USR_MOBILE_NUM, " +
					"USR_NOTIFICATION_IND, " +
					"USR_RECORDS_PER_VIEW, " +
					"USR_SEX, " +
					"USR_ROLE, " +
					"SLO_ID, " +
					"DEPT_ID, " +
					"SEC_ID, " +
					"CREATED_BY, " +
					"CREATED_DATE, " +
					"UPDATED_BY, " +
					"UPDATED_DATE " +
				"FROM " +
					"USERS " +
				"WHERE " +
					"DEPT_ID = '" + deptId + "' AND " +
					"USR_ROLE = 'MEDICALADMIN'";
			
			UserProfileData deptUser = new UserProfileData();
			
			Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(selectUser);
            while(rs.next()){
                deptUser.setUserId(rs.getString("USER_ID"));
                deptUser.setACF2ID(rs.getString("USR_ACF2ID"));
                deptUser.setPassword(rs.getString("USR_PASSWORD"));
                deptUser.setLastName(rs.getString("USR_LAST_NAME"));
                deptUser.setFirstName(rs.getString("USR_FIRST_NAME"));
                deptUser.setMiddleName(rs.getString("USR_MIDDLE_NAME"));
                deptUser.setUserCode(rs.getString("USR_CODE"));
                deptUser.setUserType(rs.getString("USR_TYPE"));
                deptUser.setEmailAddr(rs.getString("USR_EMAIL_ADDRESS"));
                deptUser.setMobileNumber(rs.getString("USR_MOBILE_NUM"));
                deptUser.setNotificationInd(rs.getBoolean("USR_NOTIFICATION_IND"));
                deptUser.setRecordsPerView(rs.getInt("USR_RECORDS_PER_VIEW"));
                deptUser.setSex(rs.getString("USR_SEX"));
                deptUser.setRole(rs.getString("USR_ROLE"));
                deptUser.setOfficeCode(rs.getString("SLO_ID"));
                deptUser.setDeptCode(rs.getString("DEPT_ID"));
                deptUser.setSectionCode(rs.getString("SEC_ID"));
                deptUser.setCreatedBy(rs.getString("CREATED_BY"));
                deptUser.setCreateDate(DateHelper.parse(rs.getString("CREATED_DATE")));
                deptUser.setUpdatedBy(rs.getString("UPDATED_BY"));
                deptUser.setUpdateDate(DateHelper.parse(rs.getString("UPDATED_DATE")));
            }
            LOGGER.info("getMedicalDeptProfile end");
			return deptUser;
	}*/
	
	public void createMedicalExam(MedicalRecordData record) throws SQLException{
		
		LOGGER.info("createMedicalExam start");
			String recordInsert = 
			"INSERT INTO MEDICAL_RECORDS " +
				"(MED_RECORD_ID, " +
				"CL_CLIENT_NUM, " +
				"AGENT_ID, " +
				"BRANCH_ID, " +
				"MED_STATUS, " +
				"MED_LAB_TEST_IND, " +
				"MED_EXAMINATION_PLACE, " +
				"MED_TEST_ID, " +				
				"MED_APPOINTMENT_DATE, " +
				"MED_DATE_CONDUCTED, " +
				"MED_DATE_RECEIVED, " +
				"MED_FOLLOW_UP_NUM, " +
				"MED_FOLLOW_UP_DATE, " +
				"MED_REMARKS, " +
				"MED_VALIDITY_DATE, " +
				"MED_7DAY_MEMO_IND, " +
				"MED_7DAY_MEMO_DATE, " +
				"MED_REQT_REASON, " +
				"MED_SECTION_ID, " +
				"MED_REQUESTING_PARTY, " +
				"MED_DEPARTMENT_ID, " +
				"MED_CHARGEABLE_TO, " +
				"MED_PAID_BY_AGENT_IND, " +
				"MED_AMOUNT, " +
				"MED_REQUEST_LOA_IND, " +
				"MED_RESULTS_RCVD, " +
				"EXMNR_ID, " +
				"MED_LAB_ID, " +
				"MED_REQUEST_DATE, " +
				"CL_LAST_NAME, " +
				"CL_GIVEN_NAME, " +
				"CL_BIRTH_DATE, " +
				"CREATED_BY, " +
				"CREATED_DATE) " +
			"VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

		try{
		PreparedStatement pStatement = conn.prepareStatement(recordInsert);
		record.setMedicalRecordId(this.getMedicalRecordID());
		pStatement.setLong(1, record.getMedicalRecordId());		
		pStatement.setString(2, record.getClient().getClientId());
		pStatement.setString(3, record.getAgent().getUserId());
		pStatement.setString(4, record.getBranch().getOfficeId());
		pStatement.setLong(5, record.getStatus().getStatusId());
		pStatement.setString(6, record.getLabTestInd());
		if (record.getExaminationPlace() == null) {
			pStatement.setString(7, null);		
		} else {
			if (record.getExaminationPlace().getExaminationPlaceId() == 0){
				pStatement.setString(7, null);
			}else {
				pStatement.setLong(7, record.getExaminationPlace().getExaminationPlaceId());
			}
		}
		if (record.getTest() == null) {
			pStatement.setString(8, null);
		} else {
			pStatement.setLong(8, new Long(record.getTest().getTestId()).longValue());
		}
		pStatement.setTimestamp(9, DateHelper.sqlTimestamp(record.getAppointmentDateTime()));
		pStatement.setDate(10, DateHelper.sqlDate(record.getConductedDate()));
		pStatement.setDate(11, DateHelper.sqlDate(record.getReceivedDate()));
		pStatement.setLong(12, record.getFollowUpNumber());
		pStatement.setDate(13, DateHelper.sqlDate(record.getFollowUpDate()));
		pStatement.setString(14, record.getRemarks());
		pStatement.setDate(15, DateHelper.sqlDate(record.getValidityDate()));
		pStatement.setBoolean(16, record.isSevenDayMemoInd()); 
		pStatement.setDate(17, DateHelper.sqlDate(record.getSevenDayMemoDate()));
		pStatement.setString(18, record.getReasonForReqt());
		pStatement.setString(19, record.getSection().getSectionId());
		pStatement.setString(20, record.getRequestingParty());
		pStatement.setString(21, record.getDepartment().getDeptId() );
		pStatement.setString(22, record.getChargableTo());
		pStatement.setBoolean(23, record.isPaidByAgentInd());
		pStatement.setDouble(24, record.getAmount());
		pStatement.setBoolean(25, record.isRequestForLOAInd());
		pStatement.setBoolean(26, record.isReceivedResults());
		if (record.getExaminer() == null){
			pStatement.setString(27, null);
		} else {
			pStatement.setLong(27, record.getExaminer().getExaminerId());
		}
		if (record.getLaboratory() == null) {
			pStatement.setString(28, null);
		} else {
			pStatement.setLong(28, record.getLaboratory().getLabId());
		}
		pStatement.setDate(29, DateHelper.sqlDate(record.getRequestedDate()));
		pStatement.setString(30, record.getClient().getLastName());
		pStatement.setString(31, record.getClient().getGivenName());
		pStatement.setDate(32, DateHelper.sqlDate(record.getClient().getBirthDate()));
		pStatement.setString(33, record.getCreatedBy());
		pStatement.setDate(34, DateHelper.sqlDate(record.getCreateDate()));
		
		pStatement.execute();
		}
		catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		}
		LOGGER.info("createMedicalExam end");
	}

	public void changeStatus(long id, long newStatus, String updatedBy) throws SQLException {
		
		LOGGER.info("changeStatus start");
		String sql = "UPDATE MEDICAL_RECORDS" +
					" SET MED_STATUS = ?, UPDATED_BY = ?, UPDATED_DATE = ?" +
					" WHERE MED_RECORD_ID = ?";
		try {
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setLong(1, newStatus);
			ps.setString(2, updatedBy);
			ps.setTimestamp(3, DateHelper.sqlTimestamp(new Date()));
			ps.setLong(4, id);
			ps.executeUpdate();
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		}
		LOGGER.info("changeStatus end");
	}

	public void updateAppointmentDate(long medicalRecordID, Date appointmentDate) throws SQLException {
		
		LOGGER.info("updateAppointmentDate start");
		String sql = "UPDATE MEDICAL_RECORDS SET MED_APPOINTMENT_DATE = ? WHERE MED_RECORD_ID = ?";
		PreparedStatement prepStmt;
		prepStmt = conn.prepareStatement(sql);
		prepStmt.setTimestamp(1, DateHelper.sqlTimestamp(appointmentDate));//(1, DateHelper.sqlDate(appointmentDate))
		prepStmt.setLong(2, medicalRecordID);
		prepStmt.executeUpdate();
		LOGGER.info("updateAppointmentDate end");
	}

	public void updateDateReceive(long medicalRecordID, Date dateReceive) throws SQLException {
		
		LOGGER.info("updateDateReceive start");
		
		String sql = 
			"UPDATE MEDICAL_RECORDS " +
			"SET MED_DATE_RECEIVED = ? " +
			"WHERE MED_RECORD_ID = ?";
		PreparedStatement prepStmt;
		prepStmt = conn.prepareStatement(sql);
		prepStmt.setDate(1, DateHelper.sqlDate(dateReceive));
		prepStmt.setLong(2, medicalRecordID);
		prepStmt.executeUpdate();
		
		LOGGER.info("updateDateReceive end");
	}
	
	public void updateRequestLOA(long medicalRecordID, boolean requestLOAInd) throws SQLException {
		
		LOGGER.info("updateRequestLOA start");
		String sql = 
			"UPDATE MEDICAL_RECORDS " +
			"SET MED_REQUEST_LOA_IND = ? " +
			"WHERE MED_RECORD_ID = ?";
		PreparedStatement prepStmt;
		prepStmt = conn.prepareStatement(sql);
		prepStmt.setBoolean(1, requestLOAInd);
		prepStmt.setLong(2, medicalRecordID);
		prepStmt.executeUpdate();
		LOGGER.info("updateRequestLOA end");
	}
	
	public void updateResultsReceived(long medicalRecordID, boolean receivedIndicator) throws SQLException {
		
		LOGGER.info("updateResultsReceived start");
		String sql = 
			"UPDATE MEDICAL_RECORDS " +
			"SET MED_RESULTS_RCVD = ? " +
			"WHERE MED_RECORD_ID = ?";
		PreparedStatement prepStmt;
		prepStmt = conn.prepareStatement(sql);
		prepStmt.setBoolean(1, receivedIndicator);
		prepStmt.setLong(2, medicalRecordID);
		prepStmt.executeUpdate();
		
		LOGGER.info("updateResultsReceived end");
	}

	public void updateDateConducted(long medicalRecordID, Date dateConducted) throws SQLException {
		
		LOGGER.info("updateDateConducted start");
		String sql = 
			"UPDATE MEDICAL_RECORDS " +
			"SET MED_DATE_CONDUCTED = ? " +
			"WHERE MED_RECORD_ID = ?";
		PreparedStatement prepStmt;
		prepStmt = conn.prepareStatement(sql);
		prepStmt.setDate(1, DateHelper.sqlDate(dateConducted));
		prepStmt.setLong(2, medicalRecordID);
		prepStmt.executeUpdate();
		
		LOGGER.info("updateDateConducted end");
	}

	public void updateValidityDate(long medicalRecordID, Date validityDate) throws SQLException {
		
		LOGGER.info("updateValidityDate start");
		String sql = 
			"UPDATE MEDICAL_RECORDS " +
			"SET MED_VALIDITY_DATE = ? " +
			"WHERE MED_RECORD_ID = ?";
		PreparedStatement prepStmt;
		prepStmt = conn.prepareStatement(sql);
		prepStmt.setDate(1, DateHelper.sqlDate(validityDate));
		prepStmt.setLong(2, medicalRecordID);
		prepStmt.executeUpdate();
		LOGGER.info("updateValidityDate end");
	}
	
	private long getMedicalRecordID() throws SQLException {
		
		LOGGER.info("getMedicalRecordID start");
		long medicalRecordID = 0;
		String sql = 
			"SELECT SEQ_MEDICAL_RECORD.NEXTVAL MEDICAL_RECORD_ID " +
			"FROM DUAL";
		PreparedStatement preStmt = conn.prepareStatement(sql);
		ResultSet rs = preStmt.executeQuery();
		if (rs.next()) {
			medicalRecordID = rs.getLong("MEDICAL_RECORD_ID");
		}
		LOGGER.info("getMedicalRecordID end");
		return medicalRecordID;
	}

	public void insertPolicyMedicalRecord(PolicyMedicalRecordData data) throws SQLException {
		
		LOGGER.info("insertPolicyMedicalRecord start");
		String sql = 
			"INSERT INTO POLICY_MEDICAL_RECORDS" +
			" (MED_RECORD_ID, UAR_REFERENCE_NUM, ASSOCIATION_TYPE)" +
			" VALUES(?,?,?)";

		PreparedStatement prepStmt = conn.prepareStatement(sql);
		prepStmt.setLong(1, data.getMedicalRecordId());
		prepStmt.setString(2, data.getReferenceNumber());
		prepStmt.setString(3, data.getAssociationType());
		prepStmt.executeUpdate();
		LOGGER.info("insertPolicyMedicalRecord end");
	}
	
	public boolean isPolicyMedicalRecordExist(PolicyMedicalRecordData data) throws SQLException {
		
		LOGGER.info("isPolicyMedicalRecordExist start");
		boolean recordExist = false;
		String sql = "SELECT MED_RECORD_ID FROM POLICY_MEDICAL_RECORDS " +
			"WHERE MED_RECORD_ID = ? AND UAR_REFERENCE_NUM = ?";
		PreparedStatement ps = conn.prepareStatement(sql);
		ps.setLong(1, data.getMedicalRecordId());
		ps.setString(2, data.getReferenceNumber());
		ResultSet rs = ps.executeQuery();
		if (rs.next()) {
			recordExist = true;
		}
		LOGGER.info("isPolicyMedicalRecordExist end");
		return recordExist;
	}

	public String getReferenceNum(long medRecordID) throws SQLException {
		
		LOGGER.info("getReferenceNum start");
		String referenceNum = null;
		String sql = 
			"SELECT UAR_REFERENCE_NUM " +
			"FROM POLICY_MEDICAL_RECORDS " +
			"WHERE MED_RECORD_ID = ? AND " +
			"ASSOCIATION_TYPE = '1'";
		PreparedStatement prepStmt = conn.prepareStatement(sql);
		prepStmt.setLong(1, medRecordID);
		ResultSet rs = prepStmt.executeQuery();
		if (rs.next()) {
			referenceNum = rs.getString("UAR_REFERENCE_NUM");
		}
		LOGGER.info("getReferenceNum end");
		return referenceNum;
	}

	public void updateMedicalRecordStatus(long refNo, String status) throws SQLException{
		
		LOGGER.info("updateMedicalRecordStatus start");
		String	updStatus = 
		"UPDATE MEDICAL_RECORDS " +
		"SET  MED_STATUS = ? " +
		"WHERE MED_RECORD_ID = ?";

        PreparedStatement ps = conn.prepareStatement(updStatus);
        ps.setString(1, status);
        ps.setLong(2, refNo);
        ps.executeUpdate();
        LOGGER.info("updateMedicalRecordStatus end");
	}
	
	public int countPendingMRs(String userID, long statusID) throws SQLException {
		
		LOGGER.info("countPendingMRs start");
		int count = 0;
		String sql = "SELECT COUNT(*) FROM MEDICAL_RECORDS WHERE CREATED_BY = ? AND MED_STATUS = ?";
		PreparedStatement ps = conn.prepareStatement(sql);
		ps.setString(1, userID);
		ps.setLong(2, statusID);
		ResultSet rs = ps.executeQuery();
		if (rs.next()) {
			count = rs.getInt(1);
		}
		LOGGER.info("countPendingMRs end");
		return count;
	}
	
	public int countPendingExaminerMRs(String userID, long statusID) throws SQLException {
		
		LOGGER.info("countPendingExaminerMRs start");
		int count = 0;
		int medLabId = Integer.parseInt(userID);
		String sql = "SELECT COUNT(*) FROM MEDICAL_RECORDS WHERE EXMNR_ID = ? AND MED_STATUS = ?";
		PreparedStatement ps = conn.prepareStatement(sql);
		ps.setInt(1, medLabId);
		ps.setLong(2, statusID);
		ResultSet rs = ps.executeQuery();
		if (rs.next()) {
			count = rs.getInt(1);
		}
		LOGGER.info("countPendingExaminerMRs end");
		return count;
	}
	
	public int countPendingLabMRs(String userID, long statusID) throws SQLException {
		
		LOGGER.info("countPendingLabMRs start");
		int count = 0;
		int medLabId = Integer.parseInt(userID);
		String sql = "SELECT COUNT(*) FROM MEDICAL_RECORDS WHERE MED_LAB_ID = ? AND MED_STATUS = ?";
		PreparedStatement ps = conn.prepareStatement(sql);
		ps.setInt(1, medLabId);
		ps.setLong(2, statusID);
		ResultSet rs = ps.executeQuery();
		if (rs.next()) {
			count = rs.getInt(1);
		}
		LOGGER.info("countPendingLabMRs end");
		return count;
	}

	public void expireMedicalRecords(long oldStatus, long newStatus) throws SQLException {
		
		LOGGER.info("expireMedicalRecords start");
		String sql = "UPDATE MEDICAL_RECORDS SET MED_STATUS = ? WHERE TRUNC(MED_VALIDITY_DATE) < TRUNC(SYSDATE) AND MED_STATUS = ?";
		PreparedStatement ps = conn.prepareStatement(sql);
		ps.setLong(1, newStatus);
		ps.setLong(2, oldStatus);
		ps.executeUpdate();
		LOGGER.info("expireMedicalRecords end");
	}


	//new added recipient
	public ArrayList getMedicalNotes(String refNum) throws SQLException {
		
		LOGGER.info("getMedicalNotes start");
		ArrayList medNotes = new ArrayList();
		String stmt = "SELECT" +
						" MN.UAR_REFERENCE_NUM AS REFERENCE_NO, " +
						" MN.MN_NOTES_ID AS NOTES_ID, " +
						" MN.MN_NOTES AS NOTES, " +
						" MN.MN_POST_DATE AS POSTED_DATE, " +
						" CONCAT(CONCAT(U.USR_FIRST_NAME, ' '), U.USR_LAST_NAME) AS POSTED_BY, " +
						" CONCAT(CONCAT(U2.USR_FIRST_NAME, ' '), U2.USR_LAST_NAME) AS RECIPIENT " +
					"FROM" +
						" MEDICAL_NOTES MN, " +
						" USERS U, " +
						" USERS U2 " +
					"WHERE" +
						" MN.MN_POSTED_BY = U.USER_ID AND " +
						" MN.MN_RECIPIENT = U2.USER_ID AND " +
						" UAR_REFERENCE_NUM = ? " +
					"ORDER" +
						" BY MN_POST_DATE DESC ";
		
		PreparedStatement ps = conn.prepareStatement(stmt);
		ps.setString(1, refNum);
		ResultSet rs = ps.executeQuery();
		while(rs.next()){
			MedicalNotes note = new MedicalNotes();
			note.setNotes(rs.getString("NOTES"));
			note.setPostDate(rs.getTimestamp("POSTED_DATE")); 
			note.setPostedBy(rs.getString("POSTED_BY"));
			note.setReferenceNumber(rs.getString("REFERENCE_NO"));
			note.setMedicalNotesId(rs.getLong("NOTES_ID"));
			note.setRecipient(rs.getString("RECIPIENT"));
			medNotes.add(note);			
		}
		LOGGER.info("getMedicalNotes end");
		return medNotes;
	}	

	public void createMedicalNotes(MedicalNotes note) throws SQLException {
		
		LOGGER.info("createMedicalNotes start 1 ");
		String stmt = "INSERT INTO MEDICAL_NOTES (UAR_REFERENCE_NUM, MN_NOTES_ID, MN_NOTES, MN_POST_DATE, MN_POSTED_BY) VALUES(?, SEQ_MEDICAL_NOTE.nextVal, ?, ?, ?)";
		
		PreparedStatement ps = conn.prepareStatement(stmt);
		ps.setString(1, note.getReferenceNumber());
		//ps.setLong(2, note.getMedicalNotesId());
		ps.setString(2, note.getNotes());
		ps.setTimestamp(3, DateHelper.sqlTimestamp(note.getPostDate()));
		ps.setString(4, note.getPostedBy());
		ps.executeUpdate();
		LOGGER.info("createMedicalNotes end 1");
	}
	
	public void createMedicalNotes(MedicalNotes note, String userId) throws SQLException {
		
		LOGGER.info("createMedicalNotes start 2");
		String stmt = "INSERT INTO MEDICAL_NOTES (UAR_REFERENCE_NUM, MN_NOTES_ID, MN_NOTES, MN_POST_DATE, MN_POSTED_BY, MN_RECIPIENT) VALUES(?, SEQ_MEDICAL_NOTE.nextVal, ?, ?, ?, ?)";
		PreparedStatement ps = conn.prepareStatement(stmt);
		ps.setString(1, note.getReferenceNumber());
		ps.setString(2, note.getNotes());
		ps.setTimestamp(3, DateHelper.sqlTimestamp(note.getPostDate()));
		ps.setString(4, note.getPostedBy());
		ps.setString(5, userId);
		ps.executeUpdate();
		LOGGER.info("createMedicalNotes start 2");
	}

	public void updateMedicalRecord(MedicalRecordData data) throws IUMException {
		
		LOGGER.info("updateMedicalRecord start");
		String sql = "UPDATE MEDICAL_RECORDS" +
					" SET CL_CLIENT_NUM = ?" +
					"   , AGENT_ID = ?" +
					"   , BRANCH_ID = ?" +
					"   , MED_STATUS = ?" +
					"   , MED_LAB_TEST_IND = ?" +
					"   , MED_EXAMINATION_PLACE = ?" +
					"   , MED_LAB_ID = ?" +             
					"   , MED_TEST_ID = ?" +
					"   , MED_APPOINTMENT_DATE = ?" +
					"   , MED_DATE_CONDUCTED = ?" +
					"   , MED_DATE_RECEIVED = ?" +
					"   , MED_FOLLOW_UP_NUM = ?" +
					"   , MED_FOLLOW_UP_DATE = ?" +
					"   , MED_REMARKS = ?" +
					"   , MED_VALIDITY_DATE = ?" +
					"   , MED_7DAY_MEMO_IND = ?" +
					"   , MED_7DAY_MEMO_DATE = ?" +
					"   , MED_REQT_REASON = ?" +
					"   , MED_SECTION_ID = ?" +
					"   , MED_REQUESTING_PARTY = ?" +
					"   , MED_DEPARTMENT_ID = ?" +
					"   , MED_CHARGEABLE_TO = ?" +
					"   , MED_PAID_BY_AGENT_IND = ?" +
					"   , MED_AMOUNT = ?" +
					"   , MED_REQUEST_LOA_IND = ?" +
					"   , MED_RESULTS_RCVD = ?" +
					"   , EXMNR_ID = ?" +
					"   , MED_REQUEST_DATE = ?" +
					"   , UPDATED_BY = ?" +
					"   , UPDATED_DATE = ?" +
					"   , CL_LAST_NAME = ?" +
					"   , CL_GIVEN_NAME = ?" +
					"   , CL_BIRTH_DATE = ?" +
					" WHERE MED_RECORD_ID = ?";
		try {
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, data.getClient().getClientId());
			ps.setString(2, data.getAgent().getUserId());
			ps.setString(3, data.getBranch().getOfficeId());
			ps.setLong(4, data.getStatus().getStatusId());
			ps.setString(5, data.getLabTestInd());
			if (data.getExaminationPlace() == null) {
				ps.setString(6, null);
			} else {
				ps.setLong(6, data.getExaminationPlace().getExaminationPlaceId());
			}
			if (data.getLaboratory() == null) {
				ps.setString(7, null);
			} else {
				ps.setLong(7, data.getLaboratory().getLabId());             
			}
			if (data.getTest() == null) {
				ps.setString(8, null);
			} else {
				ps.setLong(8, data.getTest().getTestId());
			}
			ps.setTimestamp(9, DateHelper.sqlTimestamp(data.getAppointmentDateTime()));
			ps.setDate(10, DateHelper.sqlDate(data.getConductedDate()));
			ps.setDate(11, DateHelper.sqlDate(data.getReceivedDate()));
			ps.setLong(12, data.getFollowUpNumber());
			ps.setDate(13, DateHelper.sqlDate(data.getFollowUpDate()));
			ps.setString(14, data.getRemarks());
			ps.setDate(15, DateHelper.sqlDate(data.getValidityDate()));
			ps.setBoolean(16, data.isSevenDayMemoInd());
			ps.setDate(17, DateHelper.sqlDate(data.getSevenDayMemoDate()));
			ps.setString(18, data.getReasonForReqt());
			ps.setString(19, data.getSection().getSectionId());
			ps.setString(20, data.getRequestingParty());
			ps.setString(21, data.getDepartment().getDeptId());
			ps.setString(22, data.getChargableTo());
			ps.setBoolean(23, data.isPaidByAgentInd());
			ps.setDouble(24, data.getAmount());
			ps.setBoolean(25, data.isRequestForLOAInd());
			ps.setBoolean(26, data.isReceivedResults());
			if (data.getExaminer() == null) {
				ps.setString(27, null);
			} else {
				ps.setLong(27, data.getExaminer().getExaminerId());
			}
			ps.setDate(28, DateHelper.sqlDate(data.getRequestedDate()));
			ps.setString(29, data.getUpdatedBy());
			ps.setTimestamp(30, DateHelper.sqlTimestamp(data.getUpdatedDate()));
			ps.setString(31, data.getClient().getLastName());
			ps.setString(32, data.getClient().getGivenName());
			ps.setDate(33, DateHelper.sqlDate(data.getClient().getBirthDate()));
			ps.setLong(34, data.getMedicalRecordId());			
			ps.executeUpdate();
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMException(e);
		}
		LOGGER.info("updateMedicalRecord end");
	}
	
	public void updateClientInfo(MedicalRecordData data) throws IUMException {
		
		LOGGER.info("updateClientInfo start");
		String sql = "UPDATE MEDICAL_RECORDS" +
					" SET CL_CLIENT_NUM = ?" +
					"   , AGENT_ID = ?" +
					"   , BRANCH_ID = ?" +
					"   , CL_LAST_NAME = ?" +
					"   , CL_GIVEN_NAME = ?" +
					"   , CL_BIRTH_DATE = ?" +
					" WHERE MED_RECORD_ID = ?";
		try {
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, data.getClient().getClientId());
			ps.setString(2, data.getAgent().getUserId());
			ps.setString(3, data.getBranch().getOfficeId());
			ps.setString(4, data.getClient().getLastName());
			ps.setString(5, data.getClient().getGivenName());
			ps.setDate(6, DateHelper.sqlDate(data.getClient().getBirthDate()));
			ps.setLong(7, data.getMedicalRecordId());			
			ps.executeUpdate();
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMException(e);
		}
		LOGGER.info("updateClientInfo end");
	}

	public void updateClientNum(long medicalRecordID, String clientNum, String updatedBy) throws SQLException{
		
		LOGGER.info("updateClientNum start");
		String sql = 
			"UPDATE MEDICAL_RECORDS " +
			"SET CL_CLIENT_NUM = ? " +
			"  , UPDATED_BY = ? " +
			"  , UPDATED_DATE = ? " +
			"WHERE MED_RECORD_ID = ?";
		
		PreparedStatement stmt = conn.prepareStatement(sql);
		stmt.setString(1, clientNum);
		stmt.setString(2, updatedBy);
		stmt.setTimestamp(3, DateHelper.sqlTimestamp(new java.util.Date()));
		stmt.setLong(4, medicalRecordID);
		stmt.executeUpdate();
		LOGGER.info("updateClientNum end");
	}

	public void updateChargeableTo(long medicalRecordID, String chargableTo, String updatedBy) throws SQLException{
		
		LOGGER.info("updateChargeableTo start");
		String sql = 
			"UPDATE MEDICAL_RECORDS " +
			"SET MED_CHARGEABLE_TO = ? " +
			"  , UPDATED_BY = ? " +
			"  , UPDATED_DATE = ? " +
			"WHERE MED_RECORD_ID = ?";
		
		PreparedStatement stmt = conn.prepareStatement(sql);
		stmt.setString(1, chargableTo);
		stmt.setString(2, updatedBy);
		stmt.setTimestamp(3, DateHelper.sqlTimestamp(new java.util.Date()));
		stmt.setLong(4, medicalRecordID);
		stmt.executeUpdate();
		LOGGER.info("updateChargeableTo end");
	}

	/**
	 * 
	 * @param criteria
	 * @return ArrayList of ARWithPendingMedData, missing examiner/lab name; it's up to the business object
	 *         to fill this in
	 * @throws SQLException
	 */
	public ArrayList retrievePendingMedicalRecords(ARWithPendingMedFilterData criteria) throws SQLException {
		
		LOGGER.info("retrievePendingMedicalRecords start");
		ArrayList result = new ArrayList();
		
		StringBuffer sql = new StringBuffer();
		sql.append(" select ");
		
		switch (criteria.getReportType()) {
			case ARWithPendingMedFilterData.BRANCH_REPORT_TYPE:
				sql.append("  SLO.SLO_OFFICE_NAME as BRANCH_NAME ");
				sql.append(", decode(MR.MED_LAB_TEST_IND, '0', (select EX.EXMNR_LAST_NAME || ', ' || EX.EXMNR_GIVEN_NAME || ' ' || EX.EXMNR_MIDDLE_NAME from EXAMINERS EX where EX.EXMNR_ID = MR.EXMNR_ID) ");
				sql.append("                            , '1', (select LB.LAB_NAME from LABORATORIES LB where LB.LAB_ID = MR.MED_LAB_ID)) as NAME ");
				break;
			case ARWithPendingMedFilterData.UNDERWRITER_REPORT_TYPE:
				sql.append("  US.USR_LAST_NAME as LAST_NAME ");
				sql.append(", US.USR_FIRST_NAME as FIRST_NAME ");
				sql.append(", US.USR_MIDDLE_NAME as MIDDLE_NAME ");
			sql.append(", decode(MR.MED_LAB_TEST_IND, 'M', (select EX.EXMNR_LAST_NAME || ', ' || EX.EXMNR_GIVEN_NAME || ' ' || EX.EXMNR_MIDDLE_NAME from EXAMINERS EX where EX.EXMNR_ID = MR.EXMNR_ID) " );
			sql.append("                            , 'L', (select LB.LAB_NAME from LABORATORIES LB where LB.LAB_ID = MR.MED_LAB_ID)) as NAME ");
			break;
			case ARWithPendingMedFilterData.LABORATORY_REPORT_TYPE:
				sql.append("  LB.LAB_NAME as NAME ");
				sql.append(", SLO.SLO_OFFICE_NAME as BRANCH_NAME ");
			    break;
			case ARWithPendingMedFilterData.EXAMINER_REPORT_TYPE:
			    sql.append("  (EX.EXMNR_LAST_NAME || ', ' || EX.EXMNR_GIVEN_NAME || ' ' || EX.EXMNR_MIDDLE_NAME) as NAME ");
				sql.append(", SLO.SLO_OFFICE_NAME as BRANCH_NAME ");
				break;
		}

		sql.append(", PM.UAR_REFERENCE_NUM as REFERENCE_NUMBER ");
		sql.append(", TP.TEST_DESC as TEST_NAME");
		sql.append(", decode(MR.MED_LAB_TEST_IND, 'M', 'N', 'L', 'Y') as LAB_TEST_IND");
		sql.append(", to_char(MR.MED_REQUEST_DATE, 'ddMONyyyy') as REQUEST_DATE");
		sql.append(", to_char(MR.MED_FOLLOW_UP_DATE, 'ddMONyyyy') as FOLLOWUP_DATE");
		sql.append(", count(PM.MED_RECORD_ID) as TOTAL_COUNT");
		sql.append(" from ");
		sql.append("  MEDICAL_RECORDS MR ");
		sql.append(", TEST_PROFILES TP ");
		
		switch (criteria.getReportType()) {
			case ARWithPendingMedFilterData.BRANCH_REPORT_TYPE:
				sql.append(", SUNLIFE_OFFICES SLO ");
				break;
			case ARWithPendingMedFilterData.UNDERWRITER_REPORT_TYPE:
				sql.append(", USERS US ");
				break;
			case ARWithPendingMedFilterData.LABORATORY_REPORT_TYPE:
				sql.append(", LABORATORIES LB ");
				sql.append(", SUNLIFE_OFFICES SLO ");
				break;
			case ARWithPendingMedFilterData.EXAMINER_REPORT_TYPE:
				sql.append(", EXAMINERS EX ");
				sql.append(", SUNLIFE_OFFICES SLO ");
				break;

		}
				
		sql.append(", ASSESSMENT_REQUESTS AR ");
		sql.append(", POLICY_MEDICAL_RECORDS PM");
		sql.append(" where ");
		sql.append("     MR.MED_RECORD_ID = PM.MED_RECORD_ID  ");
		sql.append(" and AR.REFERENCE_NUM = PM.UAR_REFERENCE_NUM ");

		switch (criteria.getReportType()) {
			case ARWithPendingMedFilterData.BRANCH_REPORT_TYPE:
				sql.append(" and AR.BRANCH_ID = SLO.SLO_OFFICE_CODE ");
				break;
			case ARWithPendingMedFilterData.UNDERWRITER_REPORT_TYPE:
				sql.append(" and AR.UNDERWRITER = US.USER_ID ");
				break;
			case ARWithPendingMedFilterData.LABORATORY_REPORT_TYPE:
			sql.append(" and MR.MED_LAB_TEST_IND = 'L' ");
				sql.append(" and MR.MED_LAB_ID = LB.LAB_ID ");
				sql.append(" and AR.BRANCH_ID = SLO.SLO_OFFICE_CODE ");
				break;
			case ARWithPendingMedFilterData.EXAMINER_REPORT_TYPE:
			sql.append(" and MR.MED_LAB_TEST_IND = 'M' ");
				sql.append(" and MR.EXMNR_ID = EX.EXMNR_ID ");
				sql.append(" and AR.BRANCH_ID = SLO.SLO_OFFICE_CODE ");
				break;
		}				
		
		sql.append(" and MR.MED_TEST_ID = TP.TEST_ID ");
		sql.append(" and MR.MED_STATUS IN  (?, ?) ");
		sql.append(" and MR.MED_REQUEST_DATE ");
		sql.append("     between to_date(?, 'dd-MON-yyyy hh24:mi:ss')");
		sql.append("         and to_date(?, 'dd-MON-yyyy hh24:mi:ss')");
		sql.append(applyFiltering(criteria));		

		sql.append(" GROUP BY GROUPING SETS ( ");
		switch (criteria.getReportType()) {
			case ARWithPendingMedFilterData.BRANCH_REPORT_TYPE:
				sql.append("  (SLO.SLO_OFFICE_NAME) ");
			    sql.append(", (SLO.SLO_OFFICE_NAME, ");
				sql.append("   PM.UAR_REFERENCE_NUM, MR.EXMNR_ID, MR.MED_LAB_ID, MR.MED_LAB_TEST_IND, MR.EXMNR_ID, " );
				sql.append("   TP.TEST_DESC, MR.MED_LAB_TEST_IND, MR.MED_REQUEST_DATE, MR.MED_FOLLOW_UP_DATE))" );
				break;
			case ARWithPendingMedFilterData.UNDERWRITER_REPORT_TYPE:
				sql.append("  (US.USR_LAST_NAME, US.USR_FIRST_NAME, US.USR_MIDDLE_NAME) ");
				sql.append(", (US.USR_LAST_NAME, US.USR_FIRST_NAME, US.USR_MIDDLE_NAME, ");
				sql.append("   PM.UAR_REFERENCE_NUM, MR.EXMNR_ID, MR.MED_LAB_ID, MR.MED_LAB_TEST_IND, MR.EXMNR_ID, " );
				sql.append("   TP.TEST_DESC, MR.MED_LAB_TEST_IND, MR.MED_REQUEST_DATE, MR.MED_FOLLOW_UP_DATE))" );
				break;
			case ARWithPendingMedFilterData.LABORATORY_REPORT_TYPE:
				sql.append("  (LB.LAB_NAME) ");
				sql.append(", (LB.LAB_NAME, SLO.SLO_OFFICE_NAME, ");
				sql.append("   PM.UAR_REFERENCE_NUM,TP.TEST_DESC, MR.MED_LAB_TEST_IND, MR.MED_REQUEST_DATE, MR.MED_FOLLOW_UP_DATE))" );
				break;
			case ARWithPendingMedFilterData.EXAMINER_REPORT_TYPE:
				sql.append("  (EX.EXMNR_LAST_NAME, EX.EXMNR_GIVEN_NAME, EX.EXMNR_MIDDLE_NAME) ");
				sql.append(", (EX.EXMNR_LAST_NAME, EX.EXMNR_GIVEN_NAME, EX.EXMNR_MIDDLE_NAME, SLO.SLO_OFFICE_NAME, ");
				sql.append("   PM.UAR_REFERENCE_NUM, TP.TEST_DESC, MR.MED_LAB_TEST_IND, MR.MED_REQUEST_DATE, MR.MED_FOLLOW_UP_DATE))" );
				break;
		}				

		sql.append(" order by ");
		switch (criteria.getReportType()) {
			case ARWithPendingMedFilterData.BRANCH_REPORT_TYPE:
				sql.append("  SLO.SLO_OFFICE_NAME, PM.UAR_REFERENCE_NUM ");
				break;
			case ARWithPendingMedFilterData.UNDERWRITER_REPORT_TYPE:
				sql.append("  US.USR_LAST_NAME, US.USR_FIRST_NAME, US.USR_MIDDLE_NAME, PM.UAR_REFERENCE_NUM ");
				break;
			case ARWithPendingMedFilterData.LABORATORY_REPORT_TYPE:
				sql.append("  LB.LAB_NAME, ");
				sql.append("  SLO.SLO_OFFICE_NAME, PM.UAR_REFERENCE_NUM ");
				break;
			case ARWithPendingMedFilterData.EXAMINER_REPORT_TYPE:
				sql.append("  EX.EXMNR_LAST_NAME, EX.EXMNR_GIVEN_NAME, EX.EXMNR_MIDDLE_NAME, ");
				sql.append("  SLO.SLO_OFFICE_NAME, PM.UAR_REFERENCE_NUM ");
				break;
		}
						
		try {
			PreparedStatement stmt = conn.prepareStatement(sql.toString());
			
			stmt.setLong(1,IUMConstants.STATUS_REQUESTED);
			stmt.setLong(2,IUMConstants.STATUS_CONFIRMED);
			stmt.setString(3, DateHelper.addTimePart(criteria.getDateStart(), DateHelper.ADD_START_DAY_TIME));
			stmt.setString(4, DateHelper.addTimePart(criteria.getDateEnd(), DateHelper.ADD_END_DAY_TIME));
			
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				ARWithPendingMedData data = new ARWithPendingMedData();
			
				switch (criteria.getReportType()) {
					case ARWithPendingMedFilterData.BRANCH_REPORT_TYPE:
					case ARWithPendingMedFilterData.LABORATORY_REPORT_TYPE:
					case ARWithPendingMedFilterData.EXAMINER_REPORT_TYPE:
						data.setBranchName(ValueConverter.nullToString(rs.getString("BRANCH_NAME")));
						break;
					case ARWithPendingMedFilterData.UNDERWRITER_REPORT_TYPE:
						// underwriter name
						data.setUnderwriterLastName(ValueConverter.nullToString(rs.getString("LAST_NAME")));
						data.setUnderwriterGivenName(ValueConverter.nullToString(rs.getString("FIRST_NAME")));
						data.setUnderwriterMiddleName(ValueConverter.nullToString(rs.getString("MIDDLE_NAME")));
						break;
				}
                
				data.setNameExaminerLab(ValueConverter.nullToString(rs.getString("NAME"))); // examiner or laboratory
				data.setReferenceNumber(ValueConverter.nullToString(rs.getString("REFERENCE_NUMBER"))); // assessment request number
				data.setTestDescription(ValueConverter.nullToString(rs.getString("TEST_NAME"))); // test name
				data.setTestType(ValueConverter.nullToString(rs.getString("LAB_TEST_IND"))); // indicates lab or exam
				data.setDateRequested(ValueConverter.nullToString(rs.getString("REQUEST_DATE")));
				data.setDateSubmitted(ValueConverter.nullToString(rs.getString("FOLLOWUP_DATE")));
				data.setTotalCount(rs.getInt("TOTAL_COUNT"));               
				
				result.add(data); 	
			}
		}
		catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		}
		LOGGER.info("retrievePendingMedicalRecords end");
		return (result);
			
	}

	/**
	 * @param criteria
	 * @return
	 */
	private String applyFiltering(ARWithPendingMedFilterData criteria)
	{

		LOGGER.info("applyFiltering start");
		StringBuffer addedFilter = new StringBuffer();
		boolean hasLaboratory = false;
		
		// Laboratory
		if (criteria.getLaboratory().getLabId() != -1) {	// assuming 0 is the lowest value of lab_id
			addedFilter.append(" and MR.MED_LAB_ID = " + criteria.getLaboratory().getLabId());
			hasLaboratory = true;
		}
		
		// Examiners
		if (criteria.getExaminer().getExaminerId() != -1) { // assuming 0 is the lowest value of exmnr_id
			if (hasLaboratory) { // its possible for underwriter and branch type to use both filters
				addedFilter.append(" or "); 
			}
			else {
				addedFilter.append(" and "); 
			}
			addedFilter.append(" MR.EXMNR_ID = " + criteria.getExaminer().getExaminerId());
		}
		
		// Branch
		if (criteria.getBranch().getOfficeId() != null) {
			addedFilter.append(" and AR.BRANCH_ID = '" + criteria.getBranch().getOfficeId() + "' ");
		}
		
		// Underwriter
		if (criteria.getUnderwriter().getUserId() != null) {
			addedFilter.append(" and AR.UNDERWRITER = '" + criteria.getUnderwriter().getUserId() + "' ");
		}
		LOGGER.info("applyFiltering end");
		return (addedFilter.toString());
	}

	public void updateRemarks(long medicalRecordID, String remarks, String updatedBy) throws SQLException{
		
		LOGGER.info("updateRemarks start");
		String sql = 
			"UPDATE MEDICAL_RECORDS " +
			"SET MED_REMARKS = ? " +
			"  , UPDATED_BY = ? " +
			"  , UPDATED_DATE = ? " +
			"WHERE MED_RECORD_ID = ?";
		
		PreparedStatement stmt = conn.prepareStatement(sql);
		stmt.setString(1, remarks);
		stmt.setString(2, updatedBy);
		stmt.setTimestamp(3, DateHelper.sqlTimestamp(new java.util.Date()));
		stmt.setLong(4, medicalRecordID);
		stmt.executeUpdate();
		LOGGER.info("updateRemarks end");
	}
	

	public int deleteMedicalRecords(Date startDate, Date endDate) throws SQLException {
		
		LOGGER.info("deleteMedicalRecords start");
		String sql = "DELETE MEDICAL_RECORDS WHERE to_date(to_char(CREATED_DATE, 'DDMONYYYY'), 'DDMONYYYY') BETWEEN ? AND ?";
		PreparedStatement ps = conn.prepareStatement(sql);
		ps.setDate(1, DateHelper.sqlDate(startDate));
		ps.setDate(2, DateHelper.sqlDate(endDate));
		LOGGER.info("deleteMedicalRecords end");
		return ps.executeUpdate();
	}

	public void deleteMedicalBills(String referenceNum) throws SQLException {
		
		LOGGER.info("deleteMedicalBills start");
		String sql = "DELETE MEDICAL_BILLS WHERE UAR_REFERENCE_NUM = ?";
		PreparedStatement ps = conn.prepareStatement(sql);
		ps.setString(1, referenceNum);
		ps.executeUpdate();
		LOGGER.info("deleteMedicalBills end");
	}

	public void deleteMedicalNotes(String referenceNum) throws SQLException {
		
		LOGGER.info("deleteMedicalNotes start");
		String sql = "DELETE MEDICAL_NOTES WHERE UAR_REFERENCE_NUM = ?";
		PreparedStatement ps = conn.prepareStatement(sql);
		ps.setString(1, referenceNum);
		ps.executeUpdate();
		LOGGER.info("deleteMedicalNotes end");
	}

	public void deletePolicyMedicalRecords(String referenceNum) throws SQLException {
		
		LOGGER.info("deletePolicyMedicalRecords start");
		String sql = "DELETE POLICY_MEDICAL_RECORDS WHERE UAR_REFERENCE_NUM = ?";
		PreparedStatement ps = conn.prepareStatement(sql);
		ps.setString(1, referenceNum);
		ps.executeUpdate();
		LOGGER.info("deletePolicyMedicalRecords end");
	}
	
	public int countMedRecordsForExpire() throws SQLException {
		
		LOGGER.info("countMedRecordsForExpire start");
		int count = 0;
		long medStatus = IUMConstants.STATUS_VALID;
		String sql = "SELECT COUNT(*) FROM MEDICAL_RECORDS WHERE MED_STATUS = ? AND TRUNC(MED_VALIDITY_DATE) < TRUNC(SYSDATE)";
		PreparedStatement ps = conn.prepareStatement(sql);
		ps.setLong(1, medStatus);
		ResultSet rs = ps.executeQuery();
		if (rs.next()) {
			count = rs.getInt(1);
		}
		LOGGER.info("countMedRecordsForExpire end");
		return count;
	}

	public ArrayList retrieveMedicalExamBilling(MedicalExamBillingFilter filter) throws SQLException {
		
		LOGGER.info("retrieveMedicalExamBilling start");
		ArrayList list = new ArrayList();
		StringBuffer sql = new StringBuffer();
		final String PERCENT_TAX = "0.10";
		switch (filter.getReportType()) {
			case MedicalExamBillingFilter.REPORT_TYPE_EXAMINER:
				sql.append("SELECT E.EXMNR_LAST_NAME || ', ' || E.EXMNR_GIVEN_NAME NAME");
				sql.append("       , U.USR_LAST_NAME || ', ' || U.USR_FIRST_NAME SUB_NAME ");
				sql.append("       , SLO.SLO_OFFICE_NAME BRANCH");
				sql.append("       , TO_CHAR(MR.MED_DATE_CONDUCTED, 'DDMONYYYY') EXAM_DATE");
			sql.append("       , MR.MED_LAB_TEST_IND EXAM_TYPE");
				sql.append("       , AR.REFERENCE_NUM POLICY_NUMBER");
				sql.append("       , C.CL_LAST_NAME || ', ' || C.CL_GIVEN_NAME INSURED");
				sql.append("       , LOB.LOB_DESC LOB");
				sql.append("       , MR.MED_REQT_REASON REASON");
				sql.append("       , EP.EP_DESC PLACE_OF_EXAM");
				sql.append("       , SUM(MR.MED_AMOUNT) EXAM_FEE");
				sql.append("       , SUM(CASE WHEN TP.TEST_TAXABLE_IND = '1' THEN"); 
				sql.append("             (MR.MED_AMOUNT * " + PERCENT_TAX + ") ELSE 0 END) TAX_WITHHELD");
				sql.append("       , 0 CHARGEABLE_AMOUNT");
				sql.append("       , SUM(CASE WHEN TP.TEST_TYPE = 'M' ");
				sql.append("              AND MR.MED_CHARGEABLE_TO = AR.LOB THEN");
				sql.append("              MR.MED_AMOUNT + (CASE WHEN TP.TEST_TAXABLE_IND = '1' THEN ");
				sql.append("             (MR.MED_AMOUNT * " + PERCENT_TAX + ") ELSE 0 END) ELSE 0 END) PAYABLE_AMOUNT");
				sql.append("  FROM EXAMINERS E");
				sql.append("     , SUNLIFE_OFFICES SLO");
				sql.append("     , TEST_PROFILES TP");
				sql.append("     , POLICY_MEDICAL_RECORDS PMR");
				sql.append("     , ASSESSMENT_REQUESTS AR");
				sql.append("     , CLIENTS C");
				sql.append("     , LINES_OF_BUSINESS LOB");
				sql.append("     , EXAMINATION_PLACES EP");
				sql.append("     , MEDICAL_RECORDS MR");
				sql.append("     , USERS U ");
				sql.append(" WHERE MR.MED_DATE_CONDUCTED BETWEEN ? AND ?");
				sql.append("   AND MR.MED_LAB_TEST_IND = 'M'");
				sql.append("   AND MR.EXMNR_ID = E.EXMNR_ID");
				sql.append("   AND MR.BRANCH_ID = SLO.SLO_OFFICE_CODE");
				sql.append("   AND MR.MED_TEST_ID = TP.TEST_ID");
				sql.append("   AND MR.MED_RECORD_ID = PMR.MED_RECORD_ID");
				sql.append("   AND PMR.UAR_REFERENCE_NUM = AR.REFERENCE_NUM");
				sql.append("   AND AR.INSURED_CLIENT_ID = C.CLIENT_ID");
				sql.append("   AND AR.LOB = LOB.LOB_CODE");
				sql.append("   AND MR.MED_EXAMINATION_PLACE = EP.EP_ID");
				sql.append("   AND MR.AGENT_ID = U.USER_ID");				

				if (filter.getExaminer() != null) {
					sql.append("   AND MR.EXMNR_ID = " + filter.getExaminer().getExaminerId());
				}

				sql.append("   GROUP BY GROUPING SETS (");
				sql.append("   (E.EXMNR_LAST_NAME, E.EXMNR_GIVEN_NAME),");
				sql.append("   (E.EXMNR_LAST_NAME, E.EXMNR_GIVEN_NAME, U.USR_LAST_NAME, U.USR_FIRST_NAME, SLO.SLO_OFFICE_NAME, MR.MED_DATE_CONDUCTED, ");
				sql.append("   MR.MED_LAB_TEST_IND, AR.REFERENCE_NUM, C.CL_LAST_NAME, C.CL_GIVEN_NAME, LOB.LOB_DESC, MR.MED_REQT_REASON,");
				sql.append("   EP.EP_DESC)");
				sql.append("   )");
				sql.append(" ORDER BY 1, 2, 3");
				break;
			case MedicalExamBillingFilter.REPORT_TYPE_LABORATORY:
				sql.append("SELECT L.LAB_NAME NAME");
				sql.append("     , U.USR_LAST_NAME || ', ' || U.USR_FIRST_NAME SUB_NAME ");
				sql.append("     , SLO.SLO_OFFICE_NAME BRANCH");
				sql.append("     , TO_CHAR(MR.MED_DATE_CONDUCTED, 'DDMONYYYY') EXAM_DATE");
			sql.append("     , MR.MED_LAB_TEST_IND EXAM_TYPE");
				sql.append("     , AR.REFERENCE_NUM POLICY_NUMBER");
				sql.append("     , C.CL_LAST_NAME || ', ' || C.CL_GIVEN_NAME INSURED");
				sql.append("     , LOB.LOB_DESC LOB");
				sql.append("     , MR.MED_REQT_REASON REASON");
				sql.append("     , EP.EP_DESC PLACE_OF_EXAM");
				sql.append("     , SUM(MR.MED_AMOUNT) EXAM_FEE");
				sql.append("     , SUM(CASE WHEN TP.TEST_TAXABLE_IND = '1' THEN"); 
				sql.append("	        (MR.MED_AMOUNT * " + PERCENT_TAX + ") ELSE 0 END) TAX_WITHHELD");
				sql.append("     , 0 CHARGEABLE_AMOUNT");
			sql.append("     , SUM(CASE WHEN TP.TEST_TYPE = 'L' ");
			sql.append("      AND MR.MED_PAID_BY_AGENT_IND = '1'"); // added this code
			sql.append("      AND MR.MED_CHARGEABLE_TO = AR.LOB THEN");
			sql.append("	  MR.MED_AMOUNT + (CASE WHEN TP.TEST_TAXABLE_IND = '1' THEN ");
				sql.append("           (MR.MED_AMOUNT * " + PERCENT_TAX + ") ELSE 0 END) ELSE 0 END) PAYABLE_AMOUNT");
				sql.append(" FROM LABORATORIES L");
				sql.append("    , LINES_OF_BUSINESS LOB");
				sql.append("    , SUNLIFE_OFFICES SLO");
				sql.append("    , TEST_PROFILES TP");
				sql.append("    , POLICY_MEDICAL_RECORDS PMR");
				sql.append("    , ASSESSMENT_REQUESTS AR");
				sql.append("    , CLIENTS C");
				sql.append("    , EXAMINATION_PLACES EP");
				sql.append("    , MEDICAL_RECORDS MR");
				sql.append("    , USERS U ");
				sql.append(" WHERE MR.MED_DATE_CONDUCTED BETWEEN ? AND ?");
				sql.append("   AND MR.MED_LAB_TEST_IND = 'L'");
				sql.append("   AND MR.MED_LAB_ID = L.LAB_ID");
				sql.append("   AND MR.AGENT_ID = U.USER_ID");				
				sql.append("   AND MR.BRANCH_ID = SLO.SLO_OFFICE_CODE");
				sql.append("   AND MR.MED_TEST_ID = TP.TEST_ID");
				sql.append("   AND MR.MED_RECORD_ID = PMR.MED_RECORD_ID");
				sql.append("   AND PMR.UAR_REFERENCE_NUM = AR.REFERENCE_NUM");
				sql.append("   AND AR.INSURED_CLIENT_ID = C.CLIENT_ID");
				sql.append("   AND AR.LOB = LOB.LOB_CODE");
				sql.append("   AND MR.MED_EXAMINATION_PLACE = EP.EP_ID");
			
				if (filter.getLaboratory() != null) {
					sql.append("   AND MR.MED_LAB_ID = " + filter.getLaboratory().getLabId());
				}

				if (filter.getAgent() != null) {
					sql.append("   AND MR.AGENT_ID = '" + filter.getAgent().getUserId() + "'");
				}				
				
				sql.append("   GROUP BY GROUPING SETS (");
				sql.append("   (L.LAB_NAME),");
				sql.append("   (L.LAB_NAME, U.USR_LAST_NAME, U.USR_FIRST_NAME, SLO.SLO_OFFICE_NAME, MR.MED_DATE_CONDUCTED, ");
				sql.append("   MR.MED_LAB_TEST_IND, AR.REFERENCE_NUM, C.CL_LAST_NAME, C.CL_GIVEN_NAME, LOB.LOB_DESC, MR.MED_REQT_REASON,");
				sql.append("   EP.EP_DESC)");
				sql.append("   )");
				sql.append(" ORDER BY 1, 2, 3");
				break;
			case MedicalExamBillingFilter.REPORT_TYPE_AGENT:
				sql.append("SELECT U.USR_LAST_NAME || ', ' || U.USR_FIRST_NAME NAME");
				sql.append("	  , DECODE(MR.MED_LAB_TEST_IND, 'M', EX.EXMNR_LAST_NAME || ', ' || EX.EXMNR_GIVEN_NAME "); 
				sql.append("                                  , 'L', LB.LAB_NAME ) SUB_NAME ");
				sql.append("      , SLO.SLO_OFFICE_NAME BRANCH");
				sql.append("      , TO_CHAR(MR.MED_DATE_CONDUCTED, 'DDMONYYYY') EXAM_DATE");
			sql.append("      , MR.MED_LAB_TEST_IND EXAM_TYPE");
				sql.append("      , AR.REFERENCE_NUM POLICY_NUMBER");
				sql.append("      , C.CL_LAST_NAME || ', ' || C.CL_GIVEN_NAME INSURED");
				sql.append("      , LOB.LOB_DESC LOB");
				sql.append("      , MR.MED_REQT_REASON REASON");
				sql.append("      , EP.EP_DESC PLACE_OF_EXAM");
				sql.append("      , SUM(MR.MED_AMOUNT) EXAM_FEE");
				sql.append("      , SUM(CASE WHEN TP.TEST_TAXABLE_IND = '1' THEN"); 
				sql.append("            (MR.MED_AMOUNT * " + PERCENT_TAX + ") ELSE 0 END) TAX_WITHHELD");
				sql.append("      , SUM(CASE WHEN ");
				sql.append("             MR.MED_PAID_BY_AGENT_IND = '0' ");
				sql.append("             AND AR.STATUS_ID IN(" + IUMConstants.STATUS_NOT_PROCEEDED_WITH + "," + IUMConstants.STATUS_DECLINED + ") THEN");
				sql.append("             MR.MED_AMOUNT + (CASE WHEN TP.TEST_TAXABLE_IND = '1' THEN ");
				sql.append("            (MR.MED_AMOUNT * " + PERCENT_TAX + ") ELSE 0 END) ELSE 0 END) CHARGEABLE_AMOUNT");
				sql.append("      , SUM(CASE WHEN ");
				sql.append("             MR.MED_PAID_BY_AGENT_IND = '1' ");
				sql.append("             AND AR.STATUS_ID IN(" + IUMConstants.STATUS_APPROVED + "," + IUMConstants.STATUS_FOR_OFFER + ") THEN");
				sql.append("             MR.MED_AMOUNT + (CASE WHEN TP.TEST_TAXABLE_IND = '1' THEN ");
				sql.append("            (MR.MED_AMOUNT * " + PERCENT_TAX + ") ELSE 0 END) ELSE 0 END) PAYABLE_AMOUNT");
				sql.append(" FROM USERS U");
				sql.append("    , SUNLIFE_OFFICES SLO");
				sql.append("    , TEST_PROFILES TP");
				sql.append("    , POLICY_MEDICAL_RECORDS PMR");
				sql.append("    , ASSESSMENT_REQUESTS AR");
				sql.append("    , CLIENTS C");
				sql.append("    , LINES_OF_BUSINESS LOB");
				sql.append("    , EXAMINATION_PLACES EP");
				sql.append("    , MEDICAL_RECORDS MR");
				sql.append("    , EXAMINERS EX ");
				sql.append("    , LABORATORIES LB ");
				sql.append(" WHERE MR.MED_DATE_CONDUCTED BETWEEN ? AND ?");
				sql.append("   AND MR.AGENT_ID = U.USER_ID");
				sql.append("   AND MR.BRANCH_ID = SLO.SLO_OFFICE_CODE");
				sql.append("   AND MR.MED_TEST_ID = TP.TEST_ID");
				sql.append("   AND MR.MED_RECORD_ID = PMR.MED_RECORD_ID");
				sql.append("   AND PMR.UAR_REFERENCE_NUM = AR.REFERENCE_NUM");
				sql.append("   AND AR.INSURED_CLIENT_ID = C.CLIENT_ID");
				sql.append("   AND AR.LOB = LOB.LOB_CODE");
				sql.append("   AND MR.MED_EXAMINATION_PLACE = EP.EP_ID");
				sql.append("   AND MR.MED_LAB_ID = LB.LAB_ID (+)");
				sql.append("   AND MR.EXMNR_ID = EX.EXMNR_ID (+)");

				if (filter.getAgent() != null) {
					sql.append("   AND MR.AGENT_ID = '" + filter.getAgent().getUserId() + "'");
				}

				sql.append("   GROUP BY GROUPING SETS (");
				sql.append("   (U.USR_LAST_NAME, U.USR_FIRST_NAME),");
				sql.append("   (U.USR_LAST_NAME, U.USR_FIRST_NAME, MR.MED_LAB_TEST_IND, EX.EXMNR_LAST_NAME, EX.EXMNR_GIVEN_NAME, LB.LAB_NAME, SLO.SLO_OFFICE_NAME, MR.MED_DATE_CONDUCTED, ");
			
			    sql.append("   MR.MED_LAB_TEST_IND, AR.REFERENCE_NUM, C.CL_LAST_NAME, C.CL_GIVEN_NAME, LOB.LOB_DESC, MR.MED_REQT_REASON,");
				sql.append("   EP.EP_DESC)");
				sql.append("   )");

				sql.append(" ORDER BY 1, 2, 3");
				break;

			default:
				sql = null;
		}
		if (sql != null) {
			
			PreparedStatement ps = conn.prepareStatement(sql.toString());
			ps.setString(1, filter.getStartDate());
			ps.setString(2, filter.getEndDate());
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				MedicalExamBillingData data = new MedicalExamBillingData();
				data.setName(rs.getString("NAME"));
				data.setSubName(rs.getString("SUB_NAME"));
				data.setBranch(rs.getString("BRANCH"));
				data.setExamDate(rs.getString("EXAM_DATE"));
				data.setExamType(rs.getString("EXAM_TYPE"));
				data.setPolicyNumber(rs.getString("POLICY_NUMBER"));
				data.setInsured(rs.getString("INSURED"));
				data.setLob(rs.getString("LOB"));
				data.setReason(rs.getString("REASON"));
				data.setExamPlace(rs.getString("PLACE_OF_EXAM"));
				data.setExamFee(rs.getString("EXAM_FEE"));
				data.setTaxWithheld(rs.getString("TAX_WITHHELD"));
				data.setChargeableAmt(rs.getString("CHARGEABLE_AMOUNT"));
				data.setPayableAmt(rs.getString("PAYABLE_AMOUNT"));
				list.add(data);
			}
		}
		LOGGER.info("retrieveMedicalExamBilling end");
		return list;
	}
	
	public boolean isMedicalRecordExist(String clientNum, long testID) throws SQLException {
		
		LOGGER.info("isMedicalRecordExist start");
		boolean recordExist = false;
		String sql = "SELECT MED_RECORD_ID FROM MEDICAL_RECORDS " +
			"WHERE CL_CLIENT_NUM = ? AND MED_TEST_ID = ? AND MED_STATUS NOT IN(?, ?, ?)";
		PreparedStatement ps = conn.prepareStatement(sql);
		ps.setString(1, clientNum);
		ps.setLong(2, testID);
		ps.setLong(3, IUMConstants.STATUS_EXPIRED);
		ps.setLong(4, IUMConstants.STATUS_NOT_SUBMITTED);
		ps.setLong(5, IUMConstants.STATUS_CANCELLED);
		ResultSet rs = ps.executeQuery();
		if (rs.next()) {
			recordExist = true;
		}
		LOGGER.info("isMedicalRecordExist end");
		return recordExist;
	}
	

}
