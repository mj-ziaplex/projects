/*
 * Created on Jun 25, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.data.dao.test;

import junit.framework.TestCase;

/**
 * @author Alain del Rosario
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class ClientDataSheetDAOTest extends TestCase {

	/**
	 * Constructor for ClientDataSheetDAOTest.
	 * @param arg0
	 */
	public ClientDataSheetDAOTest(String arg0) {
		super(arg0);
	}

	public static void main(String[] args) {
	}

	public void testClientDataSheetDAO() {
	}

	public void testInsertMortalityRatingData() {
	}

	public void testInsertSummaryPolicyCoverageData() {
	}

	public void testRetrieveCDS() {
	}

	public void testRetrievePolicyCoverageList() {
	}

	public void testRetrieveMortalityRating() {
	}

	public void testRetrieveSummaryPolicy() {
	}

	public void testIsCDSExisting() {
	}

	public void testRetrieveClientType() {
	}

	public void testIsPolicyDetailsExisting() {
	}

	public void testInsertPolicyDetails() {
	}

	public void testDeleteCDS() {
	}

	public void testDeletePolicyCoverageDetails() {
	}

}
