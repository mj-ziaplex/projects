/*
 * Created on Jun 23, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.data.dao.test;


import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import junit.framework.TestCase;

import com.slocpi.ium.data.dao.DocumentTypeCHDao;
import com.slocpi.ium.data.util.DataSourceProxy;
import com.slocpi.ium.util.NameValuePair;

/**
 * @author Jeffrey Canlas
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class DocumentTypeCHDaoTest extends TestCase {
	public Connection conn = null;
	
	
	public DocumentTypeCHDaoTest(String arg0) {
		super(arg0);
	}

	public void testDocumentTypeCHDao() throws SQLException {
		/*TEST FOR public Collection getCodeValues()
				 * Test connection and see whether SQL statement 
				 * produced a result.  the entered sql statement
				 * is valid (tables and fields are existing)
				 * thus resulting arraylist should be 
				 * greater than zero. 
				*/
	
				conn = new DataSourceProxy().getConnection();
				ArrayList list = new ArrayList();
				DocumentTypeCHDao dao = new DocumentTypeCHDao(conn);
	    
				list = (ArrayList)dao.getCodeValues();
			
				assertTrue("test 1", list.size()>0);
	    
				/*TEST FOR public Collection getCodeValues()
				 * test if the data in the dataset are the
				 * same as the one in the database. 
				 * Will check the first result and see 
				 * if it is the same from the database
				*/	
	
				String desc = ((NameValuePair)list.get(0)).getName();
				String code = ((NameValuePair)list.get(0)).getValue();
				System.out.println(desc);
				System.out.println(code);
				assertTrue("test 1.1",desc.equals("a1"));
				assertTrue("test1.2",code.equals("A1"));
		
				/*TEST FOR public Collection getCodeValue(String codeValue)
				 * Test connection and see whether SQL statement 
				 * produced a result.  the entered sql statement
				 * is valid (tables and fields are existing)
				 * thus resulting arraylist should be 
				 * greater than zero. 
				*/
		
				ArrayList list2 = new ArrayList();
				DocumentTypeCHDao dao2 = new DocumentTypeCHDao(conn);
	    
				list2 = (ArrayList)dao2.getCodeValue("APL");
			
				assertTrue("test 2", list2.size()>0);
	
	
				/*TEST FOR public Collection getCodeValue(String codeValue))
				 * test if the data in the dataset are the
				 * same as the one in the database. 
				 * Will check the result where DOC_CODE = "APL" 
				 * and see whether it is the same in the database
				*/		
	
				String desc2 = ((NameValuePair)list2.get(0)).getName();
				String code2 = ((NameValuePair)list2.get(0)).getValue();
				System.out.println(desc2);
				System.out.println(code2);
				assertTrue("test 2.1",desc2.equals("Application"));
				assertTrue("test 2.2",code2.equals("APL"));
	
	}

}
