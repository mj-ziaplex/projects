/*
 * Created on Jan 9, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.data.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.slocpi.ium.data.ClientData;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.DateHelper;

/**
 * @author Cris
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class ClientDAO extends BaseDAO{

	private static final Logger LOGGER = LoggerFactory.getLogger(ClientDAO.class);

	public ClientDAO (){}
	
	public ClientData retrieveClient(String lastName, String firstName, String middleName) throws SQLException {
		
		LOGGER.info("retrieveClient start");
		ClientData clientData = null;
		String sql = "SELECT CLIENT_ID, " +
						"CL_LAST_NAME, " +
						"CL_GIVEN_NAME, " +
						"CL_MIDDLE_NAME, " +
						"CL_OTH_LAST_NAME, " +
						"CL_OTH_GIVEN_NAME," +
					 	"CL_OTH_MIDDLE_NAME, " +
					 	"CL_TITLE, " +
					 	"CL_SUFFIX, " +
					 	"CL_AGE, " +
					 	"CL_SEX, " +
					 	"CL_BIRTH_DATE, " +
					 	"CL_SMOKER " +
					"FROM clients " +
					"WHERE cl_last_name = ? and cl_given_name = ? and cl_middle_name = ? ";
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection conn = null;
		
		try{
			conn = getConnection();
			ps = conn.prepareStatement(sql);
			ps.setString(1,lastName);
			ps.setString(2,firstName);
			ps.setString(3,middleName);
			rs = ps.executeQuery();
			if (rs.next()) {
				clientData = new ClientData();
				clientData.setClientId(rs.getString("client_Id"));
				clientData.setLastName(rs.getString("cl_last_name"));
				clientData.setGivenName(rs.getString("cl_given_name"));
				clientData.setMiddleName(rs.getString("cl_middle_name"));
				clientData.setOtherLastName(rs.getString("cl_oth_last_name"));
				clientData.setOtherGivenName(rs.getString("cl_oth_given_name"));
				clientData.setOtherMiddleName(rs.getString("cl_oth_middle_name"));
				clientData.setTitle(rs.getString("cl_title"));
				clientData.setSuffix(rs.getString("cl_suffix"));
				clientData.setAge(rs.getInt("cl_age"));
				clientData.setSex(rs.getString("cl_sex"));
				clientData.setBirthDate(DateHelper.utilDate(rs.getDate("cl_birth_date")));
				clientData.setSmoker(rs.getBoolean("cl_smoker"));
			}
		}catch(SQLException e)
		{
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		}finally
		{
			closeResources(conn, ps, rs);
		}
		LOGGER.info("retrieveClient end");
		return clientData;
	}
	
	public ClientData retrieveClientData(String lastName, String firstName, String birthDate) throws SQLException {
		
		LOGGER.info("retrieveClientData start");
		StringBuffer sb = new StringBuffer();
		ClientData clientData = null;
		sb.append("SELECT CLIENT_ID, " +
						"CL_LAST_NAME, " +
						"CL_GIVEN_NAME, " +
						"CL_MIDDLE_NAME, " +
						"CL_OTH_LAST_NAME, " +
						"CL_OTH_GIVEN_NAME," +
					 	"CL_OTH_MIDDLE_NAME, " +
					 	"CL_TITLE, " +
					 	"CL_SUFFIX, " +
					 	"CL_AGE, " +
					 	"CL_SEX, " +
					 	"CL_BIRTH_DATE, " +
					 	"CL_SMOKER " +
				   "FROM clients WHERE cl_last_name = ? and cl_given_name = ? and cl_birth_date = ? ");
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection conn = null;
		
		try{
			conn = getConnection();
			ps = conn.prepareStatement(sb.toString());
			ps.setString(1,lastName);
			ps.setString(2,firstName);
			ps.setString(3,birthDate);
			rs = ps.executeQuery();
			if (rs.next()) {
				clientData = new ClientData();
				clientData.setClientId(rs.getString("client_Id"));
				clientData.setLastName(rs.getString("cl_last_name"));
				clientData.setGivenName(rs.getString("cl_given_name"));
				clientData.setMiddleName(rs.getString("cl_middle_name"));
				clientData.setOtherLastName(rs.getString("cl_oth_last_name"));
				clientData.setOtherGivenName(rs.getString("cl_oth_given_name"));
				clientData.setOtherMiddleName(rs.getString("cl_oth_middle_name"));
				clientData.setTitle(rs.getString("cl_title"));
				clientData.setSuffix(rs.getString("cl_suffix"));
				clientData.setAge(rs.getInt("cl_age"));
				clientData.setSex(rs.getString("cl_sex"));
				clientData.setBirthDate(DateHelper.utilDate(rs.getDate("cl_birth_date")));
				clientData.setSmoker(rs.getBoolean("cl_smoker"));
			}
			
		}catch(SQLException e)
		{
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		}finally
		{
			closeResources(conn, ps, rs);
		}
		LOGGER.info("retrieveClientData end");
		return clientData;
	}
	
	public ClientData retrieveClient(String clientId)throws SQLException{
		
		LOGGER.info("retrieveClient start");
		ClientData clientData = null;
		String sql = "SELECT CLIENT_ID, " +
						"CL_LAST_NAME, " +
						"CL_GIVEN_NAME, " +
						"CL_MIDDLE_NAME, " +
						"CL_OTH_LAST_NAME, " +
						"CL_OTH_GIVEN_NAME, " +
						"CL_OTH_MIDDLE_NAME, " +
						"CL_TITLE, " +
						"CL_SUFFIX, " +
						"CL_AGE, " +
						"CL_SEX, " +
						"CL_BIRTH_DATE, " +
						"CL_BIRTH_LOCATION, " +
						"CL_SMOKER " +
					 "FROM CLIENTS " +
					 "WHERE CLIENT_ID = UPPER(?)";
		
		PreparedStatement stmt = null;
		ResultSet rs = null;
		Connection conn = null;
		
		try{
			conn = getConnection();
			stmt = conn.prepareStatement(sql);
			stmt.setString(1, clientId);
			rs = stmt.executeQuery();
			while(rs.next()){
				clientData = new ClientData();
				clientData.setClientId(rs.getString("client_Id"));
				clientData.setLastName(rs.getString("cl_last_name"));
				clientData.setGivenName(rs.getString("cl_given_name"));
				clientData.setMiddleName(rs.getString("cl_middle_name"));
				clientData.setOtherLastName(rs.getString("cl_oth_last_name"));
				clientData.setOtherGivenName(rs.getString("cl_oth_given_name"));
				clientData.setOtherMiddleName(rs.getString("cl_oth_middle_name"));
				clientData.setTitle(rs.getString("cl_title"));
				clientData.setSuffix(rs.getString("cl_suffix"));
				clientData.setAge(rs.getInt("cl_age"));
				clientData.setSex(rs.getString("cl_sex"));
				clientData.setBirthDate(rs.getDate("cl_birth_date"));
				clientData.setBirthLocation(rs.getString("CL_BIRTH_LOCATION"));
				clientData.setSmoker(rs.getBoolean("cl_smoker"));
			}
		
		}catch(SQLException e)
		{
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		}finally
		{
			closeResources(conn, stmt, rs);
		}
		LOGGER.info("retrieveClient end");
		return clientData; 
	} 

	public ClientData retrieveClientBirthLocation(String clientId)throws SQLException{
		
		LOGGER.info("retrieveClientBirthLocation start");
		ClientData clientData = null;
		String sql = "SELECT CLIENT_ID, " +
						"CL_LAST_NAME, " +
						"CL_GIVEN_NAME, " +
						"CL_MIDDLE_NAME, " +
						"CL_OTH_LAST_NAME, " +
						"CL_OTH_GIVEN_NAME, " +
						"CL_OTH_MIDDLE_NAME, " +
						"CL_TITLE, " +
						"CL_SUFFIX, " +
						"CL_AGE, " +
						"CL_SEX, " +
						"CL_BIRTH_DATE, " +
						"CL_BIRTH_LOCATION, " +
						"CL_SMOKER, " +
						"LOC_CODE, " +
						"LOC_NAME " +
					 "FROM CLIENTS, LOCATION " +
					 "WHERE CLIENT_ID = UPPER(?) " +
					 "AND CL_BIRTH_LOCATION = LOC_CODE";
		
		PreparedStatement stmt = null;
		ResultSet rs = null;
		Connection conn = null;
		
		try{
			conn = getConnection();		
		
			stmt = conn.prepareStatement(sql);
			stmt.setString(1, clientId);
			rs = stmt.executeQuery();
			while(rs.next()){
				clientData = new ClientData();
				clientData.setClientId(rs.getString("client_Id"));
				clientData.setLastName(rs.getString("cl_last_name"));
				clientData.setGivenName(rs.getString("cl_given_name"));
				clientData.setMiddleName(rs.getString("cl_middle_name"));
				clientData.setOtherLastName(rs.getString("cl_oth_last_name"));
				clientData.setOtherGivenName(rs.getString("cl_oth_given_name"));
				clientData.setOtherMiddleName(rs.getString("cl_oth_middle_name"));
				clientData.setTitle(rs.getString("cl_title"));
				clientData.setSuffix(rs.getString("cl_suffix"));
				clientData.setAge(rs.getInt("cl_age"));
				clientData.setSex(rs.getString("cl_sex"));
				clientData.setBirthDate(rs.getDate("cl_birth_date"));
				clientData.setSmoker(rs.getBoolean("cl_smoker"));
				clientData.setBirthLocation(rs.getString("LOC_NAME"));
			}
		
		}catch(SQLException e)
		{
			LOGGER.error(CodeHelper.getStackTrace(e));;
			throw e;
		}finally
		{
			closeResources(conn, stmt, rs);
		}
		LOGGER.info("retrieveClientBirthLocation end");
		return clientData; 
	} 
	
	public void insertClient(ClientData clientData, String type, String refNo) throws SQLException{
		
		LOGGER.info("insertClient start 1");
		String sql = "INSERT INTO CLIENTS (" +
				"CLIENT_ID, " +
				"CL_LAST_NAME, " +
				"CL_GIVEN_NAME, " +
				"CL_MIDDLE_NAME, " +
				"CL_OTH_LAST_NAME, " +
				"CL_OTH_GIVEN_NAME," +
					 "CL_OTH_MIDDLE_NAME, " +
					 "CL_TITLE, " +
					 "CL_SUFFIX, " +
					 "CL_AGE, " +
					 "CL_SEX, " +
					 "CL_BIRTH_DATE, " +
					 "CL_SMOKER) VALUES (UPPER(?),?,?,?,?,?,?,?,?,?,?,?,?)";
		
		PreparedStatement stmt = null;
		ResultSet rs = null;
		Connection conn = null;
		
		try{
			conn = getConnection();
			conn.setAutoCommit(false);
			stmt = conn.prepareStatement(sql);
			stmt.setString(1, clientData.getClientId());
			stmt.setString(2, clientData.getLastName());
			stmt.setString(3, clientData.getGivenName());
			stmt.setString(4, clientData.getMiddleName());
			stmt.setString(5, clientData.getOtherLastName());
			stmt.setString(6, clientData.getOtherGivenName());
			stmt.setString(7, clientData.getOtherMiddleName());
			stmt.setString(8, clientData.getTitle());
			stmt.setString(9, clientData.getSuffix());
			stmt.setInt(10, clientData.getAge());
			stmt.setString(11, clientData.getSex());
			stmt.setDate(12, DateHelper.sqlDate(clientData.getBirthDate()));
			stmt.setBoolean(13, clientData.isSmoker());
			stmt.executeUpdate();
			conn.commit();
			
			if(clientData.getClientId()!=null && !"".equals(clientData.getClientId()) && 
					refNo!=null && !"".equals(refNo) && type!=null && !"".equals(type)){
				String field = "";
				if("O".equalsIgnoreCase(type)){
					field = "OWNER_CLIENT_ID";
				}else{
					field = "INSURED_CLIENT_ID";
				}
				sql = "Update Assessment_Requests set "+field+"='"+clientData.getClientId()+"' where REFERENCE_NUM ='"+refNo+"'";
				stmt = conn.prepareStatement(sql);
				stmt.executeUpdate();
				conn.commit();
			}
			
		}catch(SQLException e)
		{
			LOGGER.error(CodeHelper.getStackTrace(e));
			if(conn != null){
				conn.rollback();
			}
			throw e;
		}finally
		{
			closeResources(conn, stmt, rs);
		}
		LOGGER.info("insertClient end 1");
	}
	
	public void insertClient(ClientData clientData) throws SQLException{
		LOGGER.info("insertClient start 2");
		
		
		String sql = "INSERT INTO CLIENTS (" +
				"CLIENT_ID, " +
				"CL_LAST_NAME, " +
				"CL_GIVEN_NAME, " +
				"CL_MIDDLE_NAME, " +
				"CL_OTH_LAST_NAME, " +
				"CL_OTH_GIVEN_NAME," +
					 "CL_OTH_MIDDLE_NAME, " +
					 "CL_TITLE, " +
					 "CL_SUFFIX, " +
					 "CL_AGE, " +
					 "CL_SEX, " +
					 "CL_BIRTH_DATE, " +
					 "CL_SMOKER) VALUES (UPPER(?),?,?,?,?,?,?,?,?,?,?,?,?)";
		
		PreparedStatement stmt = null;
		ResultSet rs = null;
		Connection conn = null;
		
		try {
			conn = getConnection();	
			conn.setAutoCommit(false);
			stmt = conn.prepareStatement(sql);
			stmt.setString(1, clientData.getClientId());
			stmt.setString(2, clientData.getLastName());
			stmt.setString(3, clientData.getGivenName());
			stmt.setString(4, clientData.getMiddleName());
			stmt.setString(5, clientData.getOtherLastName());
			stmt.setString(6, clientData.getOtherGivenName());
			stmt.setString(7, clientData.getOtherMiddleName());
			stmt.setString(8, clientData.getTitle());
			stmt.setString(9, clientData.getSuffix());
			stmt.setInt(10, clientData.getAge());
			stmt.setString(11, clientData.getSex());
			stmt.setDate(12, DateHelper.sqlDate(clientData.getBirthDate()));
			stmt.setBoolean(13, clientData.isSmoker());
			stmt.executeUpdate();
			conn.commit();
		}catch(SQLException e)
		{
			LOGGER.error(CodeHelper.getStackTrace(e));;
			if(conn != null){
				conn.rollback();
			}
			throw e;
		}finally
		{
			closeResources(conn, stmt, rs);
		}
		LOGGER.info("insertClient end 2");
	}
	
	public void updateClient(ClientData clientData, String type, String refNo) throws SQLException{
		
		LOGGER.info("updateClient start 1");
		String sql = "UPDATE CLIENTS SET CL_LAST_NAME = ?, CL_GIVEN_NAME = ?, CL_MIDDLE_NAME = ?, CL_OTH_LAST_NAME = ?, " +
					 "CL_OTH_GIVEN_NAME = ?,CL_OTH_MIDDLE_NAME = ?, CL_TITLE = ?, CL_SUFFIX = ?, CL_AGE = ?, CL_SEX = ?, " +
					 "CL_BIRTH_DATE = ?, CL_SMOKER = ?, CL_BIRTH_LOCATION = ? WHERE CLIENT_ID = UPPER(?)";
		
		PreparedStatement stmt = null;
		ResultSet rs = null;
		Connection conn = null;
		try {
			conn = getConnection();	
			conn.setAutoCommit(false);
			
			stmt = conn.prepareStatement(sql);
			stmt.setString(1, clientData.getLastName());
			stmt.setString(2, clientData.getGivenName());
			stmt.setString(3, clientData.getMiddleName());
			stmt.setString(4, clientData.getOtherLastName());
			stmt.setString(5, clientData.getOtherGivenName());
			stmt.setString(6, clientData.getOtherMiddleName());
			stmt.setString(7, clientData.getTitle());
			stmt.setString(8, clientData.getSuffix());
			stmt.setInt(9, clientData.getAge());
			stmt.setString(10, clientData.getSex());
			stmt.setDate(11, DateHelper.sqlDate(clientData.getBirthDate()));
			stmt.setBoolean(12, clientData.isSmoker());
			stmt.setString(13, clientData.getBirthLocation());
			stmt.setString(14, clientData.getClientId());

			stmt.executeUpdate();
			conn.commit();
			if(clientData.getClientId()!=null && !"".equals(clientData.getClientId()) && 
					refNo!=null && !"".equals(refNo) && type!=null && !"".equals(type)){
				String field = "";
				if("O".equalsIgnoreCase(type)){
					field = "OWNER_CLIENT_ID";
				}else{
					field = "INSURED_CLIENT_ID";
				}
				sql = "Update Assessment_Requests set "+field+"='"+clientData.getClientId()+"' where REFERENCE_NUM ='"+refNo+"'";
				stmt = conn.prepareStatement(sql);
				stmt.executeUpdate();
				conn.commit();
			}
			
		}catch(SQLException e)
		{
			LOGGER.error(CodeHelper.getStackTrace(e));
			if(conn != null){
				conn.rollback();
			}
			throw e;
		}finally
		{
			closeResources(conn, stmt, rs);
		}
		LOGGER.info("updateClient end 1");
	}
	
	public void updateClient(ClientData clientData) throws SQLException{
		
		LOGGER.info("updateClient start 2");
		String sql = "UPDATE CLIENTS SET CL_LAST_NAME = ?, CL_GIVEN_NAME = ?, CL_MIDDLE_NAME = ?, CL_OTH_LAST_NAME = ?, " +
					 "CL_OTH_GIVEN_NAME = ?,CL_OTH_MIDDLE_NAME = ?, CL_TITLE = ?, CL_SUFFIX = ?, CL_AGE = ?, CL_SEX = ?, " +
					 "CL_BIRTH_DATE = ?, CL_SMOKER = ?, CL_BIRTH_LOCATION = ? WHERE CLIENT_ID = UPPER(?)";
		
		PreparedStatement stmt = null;
		ResultSet rs = null;
		Connection conn = null;
		
		try {
			conn = getConnection();	
			conn.setAutoCommit(false);
			
			stmt = conn.prepareStatement(sql);
			stmt.setString(1, clientData.getLastName());
			stmt.setString(2, clientData.getGivenName());
			stmt.setString(3, clientData.getMiddleName());
			stmt.setString(4, clientData.getOtherLastName());
			stmt.setString(5, clientData.getOtherGivenName());
			stmt.setString(6, clientData.getOtherMiddleName());
			stmt.setString(7, clientData.getTitle());
			stmt.setString(8, clientData.getSuffix());
			stmt.setInt(9, clientData.getAge());
			stmt.setString(10, clientData.getSex());
			stmt.setDate(11, DateHelper.sqlDate(clientData.getBirthDate()));
			stmt.setBoolean(12, clientData.isSmoker());
			stmt.setString(13, clientData.getBirthLocation());
			stmt.setString(14, clientData.getClientId());
			stmt.executeUpdate();
			conn.commit();
		}catch(SQLException e)
		{
			LOGGER.error(CodeHelper.getStackTrace(e));
			if(conn != null){
				conn.rollback();
			}
			throw e;
		}finally
		{
			closeResources(conn, stmt, rs);
		}
		LOGGER.info("updateClient end 2");
	}
	
	public ArrayList selectClients() throws SQLException {
		
		LOGGER.info("selectClients start");
		String sql = "SELECT CLIENT_ID,CL_LAST_NAME, CL_GIVEN_NAME, CL_MIDDLE_NAME, CL_OTH_LAST_NAME, CL_OTH_GIVEN_NAME, " +
					 "CL_OTH_MIDDLE_NAME, CL_TITLE, CL_SUFFIX, CL_AGE, CL_SEX, CL_BIRTH_DATE, CL_SMOKER " +
					 "FROM CLIENTS";
		
		PreparedStatement stmt = null;
		ResultSet rs = null;
		ClientData clientData = new ClientData();
		ArrayList clientList = new ArrayList(); 
		Connection conn = null;
		
		try {
			conn = getConnection();	
			stmt = conn.prepareStatement(sql);
			rs = stmt.executeQuery();
			while(rs.next()){
				
				clientData.setClientId(rs.getString("client_Id"));
				clientData.setLastName(rs.getString("cl_last_name"));
				clientData.setGivenName(rs.getString("cl_given_name"));
				clientData.setMiddleName(rs.getString("cl_middle_name"));
				clientData.setOtherLastName(rs.getString("cl_oth_last_name"));
				clientData.setOtherGivenName(rs.getString("cl_oth_given_name"));
				clientData.setOtherMiddleName(rs.getString("cl_oth_middle_name"));
				clientData.setTitle(rs.getString("cl_title"));
				clientData.setSuffix(rs.getString("cl_suffix"));
				clientData.setAge(rs.getInt("cl_age"));
				clientData.setSex(rs.getString("cl_sex"));
				clientData.setBirthDate(rs.getDate("cl_birth_date"));
				clientData.setSmoker(rs.getBoolean("cl_smoker"));
				
				clientList.add(clientData);		
			}
		}catch(SQLException e)
		{
			LOGGER.error(CodeHelper.getStackTrace(e));;
			throw e;
		}finally
		{
			closeResources(conn, stmt, rs);
		}		
		LOGGER.info("selectClients end");
		return clientList;	
	}
	
	public boolean isClientExists(String clientID) throws SQLException {
		
		LOGGER.info("isClientExists start");
		boolean returnValue = false;
		String sql = "SELECT CLIENT_ID FROM CLIENTS WHERE CLIENT_ID = ?";
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection conn = null;
		
		try {
			conn = getConnection();	
			ps = conn.prepareStatement(sql);
			ps.setString(1, clientID);
			rs = ps.executeQuery();
			if (rs.next()) {
				returnValue = true;
			}
		}catch(SQLException e)
		{
			LOGGER.error(CodeHelper.getStackTrace(e));;
			throw e;
		}finally
		{
			closeResources(conn, ps, rs);
		}
		LOGGER.info("isClientExists end");
		return returnValue;
	}
	
	public void updateClientInfo(ClientData clientData) throws SQLException{
		
		LOGGER.info("updateClientInfo start");
		String sql = "UPDATE CLIENTS" +
					" SET CL_LAST_NAME = ?, CL_GIVEN_NAME = ?, CL_BIRTH_DATE = ?" +
					" WHERE CLIENT_ID = ?";
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection conn = null;
		
		try {
			
		
			conn = getConnection();
			conn.setAutoCommit(false);
			ps = conn.prepareStatement(sql);
			ps.setString(1, clientData.getLastName());
			ps.setString(2, clientData.getGivenName());
			ps.setDate(3, DateHelper.sqlDate(clientData.getBirthDate()));
			ps.setString(4, clientData.getClientId());
			ps.executeUpdate();
			conn.commit();
			
		}catch(SQLException e)
		{
			LOGGER.error(CodeHelper.getStackTrace(e));;
			throw e;
		}finally
		{
			closeResources(conn, ps, rs);
		}	
		LOGGER.info("updateClientInfo end");
	}
	

}

