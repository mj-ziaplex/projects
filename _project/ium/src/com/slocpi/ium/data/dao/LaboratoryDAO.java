/**
 * LaboratoryDAO.java				Copyright (c) 2004 Pointwest Technologies Corp.
 * TODO DOCUMENT ME!
 * 
 * @author aditalo	 @date Jan 9, 2004
 */
package com.slocpi.ium.data.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.slocpi.ium.data.LaboratoryData;
import com.slocpi.ium.data.LaboratoryTestData;
import com.slocpi.ium.data.UserProfileData;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.DateHelper;

/**
 * TODO DOCUMENT ME!
 * 
 * @author aditalo		Copyright (c) 2004 Pointwest Technologies Corp.
 * @date Jan 9, 2004
 */
public class LaboratoryDAO {

	private static final Logger LOGGER = LoggerFactory.getLogger(LaboratoryDAO.class);

	private Connection conn = null;
	
	/**
	 * 
	 */
	/* Constructor created to remove connection parameter */
	public LaboratoryDAO() {
		
	}
	
	public LaboratoryDAO(Connection connection) {
		super();
		this.conn = connection;
	}
	private void closeResources(PreparedStatement ps, ResultSet rs) throws SQLException{
		
		
		try {
			if (ps != null){
				ps.close();
			}
		}
		catch(SQLException e)
		{
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		}
		try {
			if (rs != null){
				rs.close();
			}			
		} catch (SQLException e){
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		}
		
	}	
	public ArrayList selectLaboratories() throws SQLException {
		
		LOGGER.info("selectLaboratories start");
		ArrayList laboratories = new ArrayList();
		String sql = "SELECT LAB_ID, LAB_NAME, LAB_CONTACT_PERSON, LAB_OFFICE_NUM, LAB_OFFICE_NUM2 " +
					",LAB_FAX_NUM, LAB_BUS_ADDR_LINE1, LAB_BUS_ADDR_LINE2, LAB_BUS_ADDR_LINE3, LAB_CITY " +
					",LAB_PROVINCE, LAB_COUNTRY, LAB_ZIPCODE, LAB_BRANCH_NAME, LAB_BRANCH_ADDR_LINE1 "+
					",LAB_BRANCH_ADDR_LINE2, LAB_BRANCH_ADDR_LINE3, LAB_BRANCH_CITY, LAB_BRANCH_PROVINCE, LAB_BRANCH_COUNTRY " +
					",LAB_BRANCH_ZIPCODE, LAB_BRANCH_PHONE, LAB_BRANCH_FAX_NUM, LAB_ACCREDIT_IND, to_char(LAB_ACCREDIT_DATE, 'dd-mon-yyyy') LAB_ACCREDIT_DATE " +
					",CREATED_BY, to_char(CREATED_DATE, 'dd-mon-yyyy') CREATED_DATE, UPDATED_BY, to_char(UPDATED_DATE, 'dd-mon-yyyy') UPDATED_DATE, LAB_EMAIL_ADDRESS " + 
					"FROM LABORATORIES ORDER BY LAB_ID";
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try{		
		ps = conn.prepareStatement(sql);
		rs = ps.executeQuery();
		while (rs.next()) {
			LaboratoryData laboratory = new LaboratoryData();
			laboratory.setLabId(rs.getLong("LAB_ID"));
			laboratory.setLabName(rs.getString("LAB_NAME"));
			laboratory.setContactPerson(rs.getString("LAB_CONTACT_PERSON"));
			laboratory.setOfficeNum(rs.getString("LAB_OFFICE_NUM"));
			laboratory.setOfficeNum2(rs.getString("LAB_OFFICE_NUM2"));
			laboratory.setFaxNum(rs.getString("LAB_FAX_NUM"));
			laboratory.setBusinessAddrLine1(rs.getString("LAB_BUS_ADDR_LINE1"));
			laboratory.setBusinessAddrLine2(rs.getString("LAB_BUS_ADDR_LINE2"));
			laboratory.setBusinessAddrLine3(rs.getString("LAB_BUS_ADDR_LINE3"));
			laboratory.setCity(rs.getString("LAB_CITY"));
			laboratory.setProvince(rs.getString("LAB_PROVINCE"));
			laboratory.setCountry(rs.getString("LAB_COUNTRY"));
			laboratory.setZipCode(rs.getString("LAB_ZIPCODE"));
			laboratory.setBranchName(rs.getString("LAB_BRANCH_NAME"));
			laboratory.setBranchAddrLine1(rs.getString("LAB_BRANCH_ADDR_LINE1"));
			laboratory.setBranchAddrLine2(rs.getString("LAB_BRANCH_ADDR_LINE2"));
			laboratory.setBranchAddrLine3(rs.getString("LAB_BRANCH_ADDR_LINE3"));
			laboratory.setBranchCity(rs.getString("LAB_BRANCH_CITY"));
			laboratory.setBranchProvince(rs.getString("LAB_BRANCH_PROVINCE"));
			laboratory.setBranchCountry(rs.getString("LAB_BRANCH_COUNTRY"));
			laboratory.setBranchZipCode(rs.getString("LAB_BRANCH_ZIPCODE"));
			laboratory.setBranchPhone(rs.getString("LAB_BRANCH_PHONE"));
			laboratory.setBranchFax_num(rs.getString("LAB_BRANCH_FAX_NUM"));
			laboratory.setAccreditInd(rs.getString("LAB_ACCREDIT_IND"));
			laboratory.setAccreditDate(DateHelper.parse(rs.getString("LAB_ACCREDIT_DATE"), "dd-MMM-yyyy"));
			
			UserProfileData profile = new UserProfileData();
			profile.setUserId(rs.getString("CREATED_BY"));
			laboratory.setCreatedBy(profile);
			
			laboratory.setCreatedDate(DateHelper.parse(rs.getString("CREATED_DATE"), "dd-MMM-yyyy"));
			
			UserProfileData profile2 = new UserProfileData();
			profile.setUserId(rs.getString("UPDATED_BY"));
			laboratory.setUpdatedBy(profile2);
			laboratory.setUpdatedDate(DateHelper.parse(rs.getString("UPDATED_DATE"), "dd-MMM-yyyy"));
			laboratory.setEmailAddress(rs.getString("LAB_EMAIL_ADDRESS"));
			
			laboratories.add(laboratory);
		}
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e; 
		}finally{
		 	this.closeResources(ps, rs);
		}
		LOGGER.info("selectLaboratories end");
		return laboratories;
	}
	
	public LaboratoryData selectLaboratory(long labId) throws SQLException {
		
		LOGGER.info("selectLaboratory start");
		String sql = "SELECT LAB_ID, LAB_NAME, LAB_CONTACT_PERSON, LAB_OFFICE_NUM, LAB_OFFICE_NUM2 " +
					",LAB_FAX_NUM, LAB_BUS_ADDR_LINE1, LAB_BUS_ADDR_LINE2, LAB_BUS_ADDR_LINE3, LAB_CITY " +
					",LAB_PROVINCE, LAB_COUNTRY, LAB_ZIPCODE, LAB_BRANCH_NAME, LAB_BRANCH_ADDR_LINE1 "+
					",LAB_BRANCH_ADDR_LINE2, LAB_BRANCH_ADDR_LINE3, LAB_BRANCH_CITY, LAB_BRANCH_PROVINCE, LAB_BRANCH_COUNTRY " +
					",LAB_BRANCH_ZIPCODE, LAB_BRANCH_PHONE, LAB_BRANCH_FAX_NUM, LAB_ACCREDIT_IND, to_char(LAB_ACCREDIT_DATE, 'dd-mon-yyyy') LAB_ACCREDIT_DATE " +
					",CREATED_BY, to_char(CREATED_DATE, 'dd-mon-yyyy') CREATED_DATE, UPDATED_BY, to_char(UPDATED_DATE, 'dd-mon-yyyy') UPDATED_DATE, LAB_EMAIL_ADDRESS " + 
					"FROM LABORATORIES WHERE LAB_ID = ? ORDER BY LAB_ID";
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		LaboratoryData laboratory = null;
		try{
		ps = conn.prepareStatement(sql);
		ps.setLong(1, labId);
		rs = ps.executeQuery();
		
		if (rs.next()) {			
			laboratory = new LaboratoryData();
			laboratory.setLabId(rs.getLong("LAB_ID"));
			laboratory.setLabName(rs.getString("LAB_NAME"));
			laboratory.setContactPerson(rs.getString("LAB_CONTACT_PERSON"));
			laboratory.setOfficeNum(rs.getString("LAB_OFFICE_NUM"));
			laboratory.setOfficeNum2(rs.getString("LAB_OFFICE_NUM2"));
			laboratory.setFaxNum(rs.getString("LAB_FAX_NUM"));
			laboratory.setBusinessAddrLine1(rs.getString("LAB_BUS_ADDR_LINE1"));
			laboratory.setBusinessAddrLine2(rs.getString("LAB_BUS_ADDR_LINE2"));
			laboratory.setBusinessAddrLine3(rs.getString("LAB_BUS_ADDR_LINE3"));
			laboratory.setCity(rs.getString("LAB_CITY"));
			laboratory.setProvince(rs.getString("LAB_PROVINCE"));
			laboratory.setCountry(rs.getString("LAB_COUNTRY"));
			laboratory.setZipCode(rs.getString("LAB_ZIPCODE"));
			laboratory.setBranchName(rs.getString("LAB_BRANCH_NAME"));
			laboratory.setBranchAddrLine1(rs.getString("LAB_BRANCH_ADDR_LINE1"));
			laboratory.setBranchAddrLine2(rs.getString("LAB_BRANCH_ADDR_LINE2"));
			laboratory.setBranchAddrLine3(rs.getString("LAB_BRANCH_ADDR_LINE3"));
			laboratory.setBranchCity(rs.getString("LAB_BRANCH_CITY"));
			laboratory.setBranchProvince(rs.getString("LAB_BRANCH_PROVINCE"));
			laboratory.setBranchCountry(rs.getString("LAB_BRANCH_COUNTRY"));
			laboratory.setBranchZipCode(rs.getString("LAB_BRANCH_ZIPCODE"));
			laboratory.setBranchPhone(rs.getString("LAB_BRANCH_PHONE"));
			laboratory.setBranchFax_num(rs.getString("LAB_BRANCH_FAX_NUM"));
			laboratory.setAccreditInd(rs.getString("LAB_ACCREDIT_IND"));
			laboratory.setAccreditDate(DateHelper.parse(rs.getString("LAB_ACCREDIT_DATE"), "dd-MMM-yyyy"));
			
			UserProfileData profile = new UserProfileData();
			profile.setUserId(rs.getString("CREATED_BY"));
			laboratory.setCreatedBy(profile);			
			laboratory.setCreatedDate(DateHelper.parse(rs.getString("CREATED_DATE"), "dd-MMM-yyyy"));
			
			UserProfileData profile2 = new UserProfileData();
			profile.setUserId(rs.getString("UPDATED_BY"));
			laboratory.setUpdatedBy(profile2);
			laboratory.setUpdatedDate(DateHelper.parse(rs.getString("UPDATED_DATE"), "dd-MMM-yyyy"));
			
			laboratory.setEmailAddress(rs.getString("LAB_EMAIL_ADDRESS"));			
		}
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e; 
		}finally{
		 	this.closeResources(ps, rs);
		}
		LOGGER.info("selectLaboratory end");
		return laboratory;
	}
	
	public LaboratoryTestData retrieveLaboratoryTestData(long testId, long labId) throws SQLException{
		
		LOGGER.info("retrieveLaboratoryTestData start");
		String sql = "SELECT * FROM LABORATORY_TESTS  WHERE LAB_ID = ? AND TEST_ID = ?";
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		LaboratoryTestData laboratoryTestData = new LaboratoryTestData();
		try{		
		ps = conn.prepareStatement(sql);
		ps.setLong(1, labId);
		ps.setLong(2, testId);
		rs = ps.executeQuery();
		
		while(rs.next()){  
			laboratoryTestData.setTestFee(rs.getLong("LAB_ID"));
			laboratoryTestData.setTestFee(rs.getLong("TEST_ID"));
			laboratoryTestData.setTestFee(rs.getDouble("TEST_FEE"));
			laboratoryTestData.setTestStatus(rs.getString("LAB_TEST_STATUS"));
		}
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e; 
		}finally{
		 	this.closeResources(ps, rs);
		}	
		LOGGER.info("retrieveLaboratoryTestData end");
		return laboratoryTestData;
	}
	
	public ArrayList selectAllLaboratoryTestData(long labId) throws SQLException{
		
		LOGGER.info("selectAllLaboratoryTestData start");
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT LAB_ID, TEST_ID, TEST_FEE, LAB_TEST_STATUS ");
		sb.append("FROM LABORATORY_TESTS  ");
		sb.append("WHERE LAB_ID = ? ORDER BY TEST_ID ");
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList list = new ArrayList();
		try{		
		ps = conn.prepareStatement(sb.toString());
		ps.setLong(1, labId);		
		rs = ps.executeQuery();		
		
		
		while(rs.next()){  
			LaboratoryTestData laboratoryTestData = new LaboratoryTestData();
			laboratoryTestData.setLabId(rs.getLong("LAB_ID"));
			laboratoryTestData.setTestCode(rs.getLong("TEST_ID"));
			laboratoryTestData.setTestFee(rs.getDouble("TEST_FEE"));
			laboratoryTestData.setTestStatus(rs.getString("LAB_TEST_STATUS"));		
			
			list.add(laboratoryTestData);
		}
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e; 
		}finally{
		 	this.closeResources(ps, rs);
		}	
		LOGGER.info("selectAllLaboratoryTestData end");
		return list;
	}
	
	public void insertLaboratoryTestData(LaboratoryTestData labTestData, LaboratoryData labData) throws SQLException{
		
		LOGGER.info("insertLaboratoryTestData start");
		StringBuffer sb = new StringBuffer();
		sb.append("INSERT INTO LABORATORY_TESTS ");
		sb.append("(LAB_ID, TEST_ID, TEST_FEE, CREATED_DATE, CREATED_BY, LAB_TEST_STATUS) ");
		sb.append("VALUES(?,?,?,?,?,?)  ");	

		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try{		
		
			ps = conn.prepareStatement(sb.toString());
			ps.setLong(1, labTestData.getLabId());
			ps.setLong(2, labTestData.getTestCode());
			ps.setDouble(3, labTestData.getTestFee());
			ps.setDate(4, DateHelper.sqlDate(labData.getUpdatedDate()));
			ps.setString(5, labData.getUpdatedBy().getUserId());
			ps.setString(6, labTestData.getTestStatus());
			ps.executeUpdate();
			
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e; 
		}finally{
		 	this.closeResources(ps, rs);
		}
		LOGGER.info("insertLaboratoryTestData end");
	}
	
	public void updateLaboratoryTestData(LaboratoryTestData labTestData, LaboratoryData labData) throws SQLException{
		
		LOGGER.info("updateLaboratoryTestData start");
		StringBuffer sb = new StringBuffer();
		sb.append("UPDATE LABORATORY_TESTS ");
		sb.append("set TEST_FEE = ?, ");
		sb.append("UPDATED_DATE = ?,  ");
		sb.append("UPDATED_BY = ?, ");
		sb.append("LAB_TEST_STATUS = ? ");
		sb.append("WHERE LAB_ID = ? AND TEST_ID = ? ");		
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try{		
		
			ps = conn.prepareStatement(sb.toString());			
			ps.setDouble(1, labTestData.getTestFee());
			ps.setDate(2, DateHelper.sqlDate(labData.getUpdatedDate()));
			ps.setString(3, labData.getUpdatedBy().getUserId());
			ps.setString(4, labTestData.getTestStatus());
			ps.setLong(5, labTestData.getLabId());
			ps.setLong(6, labTestData.getTestCode());
				
			ps.executeUpdate();
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e; 
		}finally{
		 	this.closeResources(ps, rs);
		 	LOGGER.info("updateLaboratoryTestData end");
		}
	}
	
	public void insertLaboratoryData(LaboratoryData labData) throws SQLException
	{
		LOGGER.info("insertLaboratoryData start");
		StringBuffer sb = new StringBuffer();
		
		sb.append("INSERT INTO LABORATORIES ");
		sb.append("(LAB_ID, LAB_NAME, LAB_CONTACT_PERSON, ");
		sb.append("LAB_OFFICE_NUM, LAB_OFFICE_NUM2, LAB_FAX_NUM, ");
		sb.append("LAB_BUS_ADDR_LINE1, LAB_BUS_ADDR_LINE2, LAB_BUS_ADDR_LINE3, ");
		sb.append("LAB_CITY, LAB_COUNTRY, LAB_ZIPCODE, ");
		sb.append("LAB_BRANCH_NAME, LAB_BRANCH_PHONE, LAB_BRANCH_FAX_NUM, ");
		sb.append("LAB_BRANCH_ADDR_LINE1, LAB_BRANCH_ADDR_LINE2, LAB_BRANCH_ADDR_LINE3, ");
		sb.append("LAB_BRANCH_CITY, LAB_BRANCH_COUNTRY, LAB_BRANCH_ZIPCODE, ");
		sb.append("LAB_ACCREDIT_IND, LAB_ACCREDIT_DATE, LAB_EMAIL_ADDRESS, ");
		sb.append("CREATED_BY, CREATED_DATE) ");
		sb.append("VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ");
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try{		

			ps = conn.prepareStatement(sb.toString());
			ps.setLong(1, labData.getLabId());
			ps.setString(2, labData.getLabName());
			ps.setString(3, labData.getContactPerson());
			ps.setString(4, labData.getOfficeNum());
			ps.setString(5, labData.getOfficeNum2());
			ps.setString(6, labData.getFaxNum());
			ps.setString(7, labData.getBusinessAddrLine1());
			ps.setString(8, labData.getBusinessAddrLine2());
			ps.setString(9, labData.getBusinessAddrLine3());
			ps.setString(10, labData.getCity());
			ps.setString(11, labData.getCountry());
			ps.setString(12, labData.getZipCode());
			ps.setString(13, labData.getBranchName());
			ps.setString(14, labData.getBranchPhone());
			ps.setString(15, labData.getBranchFax_num());
			ps.setString(16, labData.getBranchAddrLine1());
			ps.setString(17, labData.getBranchAddrLine2());
			ps.setString(18, labData.getBranchAddrLine3());
			ps.setString(19, labData.getBranchCity());
			ps.setString(20, labData.getBranchCountry());
			ps.setString(21, labData.getBranchZipCode());
			ps.setString(22, labData.getAccreditInd());			
			ps.setDate(23, DateHelper.sqlDate(labData.getAccreditDate()));		
			ps.setString(24, labData.getEmailAddress());
			ps.setString(25, labData.getCreatedBy().getUserId());
			ps.setDate(26, DateHelper.sqlDate(labData.getCreatedDate()));
			
			ps.executeUpdate();
		
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e; 
		}finally{
		 	this.closeResources(ps, rs);
		}
		LOGGER.info("insertLaboratoryData end");
	}
	
	public void updateLaboratoryData(LaboratoryData labData) throws SQLException
	{
		LOGGER.info("updateLaboratoryData start");
		StringBuffer sb = new StringBuffer();
		
		sb.append("UPDATE LABORATORIES ");
		sb.append("set LAB_NAME = ?, ");
		sb.append(" LAB_CONTACT_PERSON = ?, ");
		sb.append(" LAB_OFFICE_NUM = ?, ");
		sb.append(" LAB_OFFICE_NUM2 = ?, ");
		sb.append(" LAB_FAX_NUM = ?, ");
		sb.append(" LAB_BUS_ADDR_LINE1 = ?, ");
		sb.append(" LAB_BUS_ADDR_LINE2 = ?, ");
		sb.append(" LAB_BUS_ADDR_LINE3 = ?, ");
		sb.append(" LAB_CITY = ?, ");
		sb.append(" LAB_COUNTRY = ?, ");
		sb.append(" LAB_ZIPCODE = ?, ");
		sb.append(" LAB_BRANCH_NAME = ?, ");
		sb.append(" LAB_BRANCH_PHONE = ?, ");
		sb.append(" LAB_BRANCH_FAX_NUM = ?, ");
		sb.append(" LAB_BRANCH_ADDR_LINE1 = ?, ");
		sb.append(" LAB_BRANCH_ADDR_LINE2 = ?, ");
		sb.append(" LAB_BRANCH_ADDR_LINE3 = ?, ");
		sb.append(" LAB_BRANCH_CITY = ?, ");
		sb.append(" LAB_BRANCH_COUNTRY = ?, ");
		sb.append(" LAB_BRANCH_ZIPCODE = ?, ");
		sb.append(" LAB_ACCREDIT_IND = ?, ");
		sb.append(" LAB_ACCREDIT_DATE = ?, ");
		sb.append(" LAB_EMAIL_ADDRESS = ?, ");
		sb.append(" UPDATED_BY = ?, ");
		sb.append(" UPDATED_DATE = ? ");
		sb.append("WHERE LAB_ID = ? ");
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try{		
			ps = conn.prepareStatement(sb.toString());			
			ps.setString(1, labData.getLabName());
			ps.setString(2, labData.getContactPerson());
			ps.setString(3, labData.getOfficeNum());
			ps.setString(4, labData.getOfficeNum2());
			ps.setString(5, labData.getFaxNum());
			ps.setString(6, labData.getBusinessAddrLine1());
			ps.setString(7, labData.getBusinessAddrLine2());
			ps.setString(8, labData.getBusinessAddrLine3());
			ps.setString(9, labData.getCity());
			ps.setString(10, labData.getCountry());
			ps.setString(11, labData.getZipCode());
			ps.setString(12, labData.getBranchName());
			ps.setString(13, labData.getBranchPhone());
			ps.setString(14, labData.getBranchFax_num());
			ps.setString(15, labData.getBranchAddrLine1());
			ps.setString(16, labData.getBranchAddrLine2());
			ps.setString(17, labData.getBranchAddrLine3());
			ps.setString(18, labData.getBranchCity());
			ps.setString(19, labData.getBranchCountry());
			ps.setString(20, labData.getBranchZipCode());
			ps.setString(21, labData.getAccreditInd());
			ps.setDate(22, DateHelper.sqlDate(labData.getAccreditDate()));		
			ps.setString(23, labData.getEmailAddress());
			ps.setString(24, labData.getUpdatedBy().getUserId());
			ps.setDate(25, DateHelper.sqlDate(labData.getUpdatedDate()));
			ps.setLong(26, labData.getLabId());
			
			ps.executeUpdate();
		
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e; 
		}finally{
		 	this.closeResources(ps, rs);
		}
		LOGGER.info("updateLaboratoryData end");
	}
		

}
