// Andre Ceasar Dacanay - AuditLogs
package com.slocpi.ium.data.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.slocpi.ium.data.AuditLogsData;
import com.slocpi.ium.util.CodeHelper;

/**
 * @author Andre Ceasar Dacanay
 */
public class AuditLogslDAO {

	private static final Logger LOGGER = LoggerFactory.getLogger(AuditLogslDAO.class);
	private Connection conn;
	
	public AuditLogslDAO(Connection _conn){
		conn = _conn;
	}
	
	public void closeDB() {
		
		LOGGER.info("closeDB start");
		try {
			this.conn.close();
		} catch (Exception e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
		}
		LOGGER.info("closeDB end");
	}

	private void closeResources(PreparedStatement ps, ResultSet rs) throws SQLException{
		
		
		try {
			if (ps != null){
				ps.close();
			}
		}
		catch(SQLException e)
		{
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		}
		try {
			if (rs != null){
				rs.close();
			}			
		} catch (SQLException e){
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		}
		
	}

	public void insertAuditLogs(AuditLogsData data) throws SQLException {
		
		LOGGER.info("insertAuditLogs start");
		String sql = "INSERT INTO AUDIT_LOGS" +
					" (AUDIT_NUM, AUDIT_DATE, REMARKS, USER_ID, AUDIT_TYPE)" +
					" VALUES(SEQ_AUDIT_LOGS.NEXTVAL, SYSDATE, ?, ?, ?)";
		PreparedStatement ps = null;
		try{
			ps = conn.prepareStatement(sql);
			ps.setString(1, data.getRemarks());
			ps.setString(2, data.getUserId());
			ps.setString(3, data.getAuditType());
			ps.executeUpdate();
		}catch(SQLException e)
		{
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		}finally
		{
			this.closeResources(ps, null);
		}
		LOGGER.info("insertAuditLogs end");
	}
	
}
