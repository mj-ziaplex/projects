/**
 * CodeValuePairDAO.java				Copyright (c) 2004 Pointwest Technologies Corp.
 * TODO DOCUMENT ME!
 * 
 * @author tvicencio					@date Feb 23, 2004
 */
package com.slocpi.ium.data.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.slocpi.ium.data.AutoAssignmentData;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.DateHelper;
import com.slocpi.ium.util.SortHelper;


/**
 * TODO DOCUMENT ME!
 * 
 * @author tvicencio		Copyright (c) 2004 Pointwest Technologies Corp.
 * @date Feb 23, 2004
 */
public class AutoAssignmentDAO {

	private static final Logger LOGGER = LoggerFactory.getLogger(AutoAssignmentDAO.class);
	private Connection conn = null;	


	public AutoAssignmentDAO(Connection connection) {
		super();
		this.conn = connection;
	}

	//added by Aldrich Abrogena Jan 16,2008
	private void closeResources(PreparedStatement ps, ResultSet rs) throws SQLException{
		
		
		try {
			if (ps != null){
				ps.close();
			}
		}
		catch(SQLException e)
		{
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		}
		try {
			if (rs != null){
				rs.close();
			}			
		} catch (SQLException e){
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		}
		
	}
	//edited by Aldrich Abrogena - Jan 16,2008
	public ArrayList retrieveAutoAssignments(SortHelper sort) throws SQLException {
		
		LOGGER.info("retrieveAutoAssignments start");
		String sql = "SELECT A.AA_ID AS AUTO_ASSIGNMENT_ID, A.AAC_ID AS CRITERIA_CODE, A.CRITERIA_FIELD_VALUE AS CRITERIA_VALUE, A.USER_ID AS UNDERWRITER " +
					 "FROM AUTO_ASSIGNMENTS A, AUTO_ASSIGNMENT_CRITERIA C " +
					 "WHERE A.AAC_ID = C.AAC_ID " +
					 sort.getOrderBy();
		
		PreparedStatement ps = null;
		ResultSet rs = null; 
		ArrayList list = new ArrayList();
		try{
		ps = conn.prepareStatement(sql);
		rs = ps.executeQuery();

		while (rs.next()) {
			AutoAssignmentData data = new AutoAssignmentData();
			data.setAutoAssignmentId(rs.getLong("AUTO_ASSIGNMENT_ID"));
			data.setCriteriaId(rs.getLong("CRITERIA_CODE"));
			data.setFieldValue(rs.getString("CRITERIA_VALUE"));
			data.setUserCode(rs.getString("UNDERWRITER"));
			list.add(data);
		}
		}catch(SQLException e)
		{
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		}finally
		{
			this.closeResources(ps, rs);
		}
		LOGGER.info("retrieveAutoAssignments end");
		return (list);	
	}

	public AutoAssignmentData retrieveAutoAssignmentDetail(long id) throws SQLException {
		
		LOGGER.info("retrieveAutoAssignmentDetail start");
		String sql = "SELECT AA_ID AS AUTO_ASSIGNMENT_ID, AAC_ID AS CRITERIA_CODE, CRITERIA_FIELD_VALUE AS CRITERIA_VALUE, USER_ID AS UNDERWRITER " +
					 "FROM AUTO_ASSIGNMENTS " +
					 "WHERE AA_ID = ? " +
					 "ORDER BY AAC_ID, CRITERIA_FIELD_VALUE";
	
		PreparedStatement ps = null;
		ResultSet rs = null;
		AutoAssignmentData data = null;		
		try{
			ps= conn.prepareStatement(sql);
			ps.setLong(1, id);
			rs = ps.executeQuery();
	
			if (rs.next()) {
				data = new AutoAssignmentData();
				data.setAutoAssignmentId(rs.getLong("AUTO_ASSIGNMENT_ID"));
				data.setCriteriaId(rs.getLong("CRITERIA_CODE"));
				data.setFieldValue(rs.getString("CRITERIA_VALUE"));
				data.setUserCode(rs.getString("UNDERWRITER"));
			}
		}catch(SQLException e)
		{
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		}finally{
			this.closeResources(ps, rs);
		}
		LOGGER.info("retrieveAutoAssignmentDetail end");
		return (data);
	}//retrieveAutoAssignmentDetail


	public void insertMapping(AutoAssignmentData data) throws SQLException {
		
		LOGGER.info("insertMapping start");
		String sql = "INSERT INTO AUTO_ASSIGNMENTS (AA_ID, AAC_ID, CRITERIA_FIELD_VALUE, USER_ID, CREATED_BY, CREATED_DATE) " +
					 "VALUES(SEQ_AUTO_ASSIGNMENT.NEXTVAL,?,?,?,?,?)";
		PreparedStatement ps = null;
		try {
			
			ps = conn.prepareStatement(sql);
		
			long criteriaCode = data.getCriteriaId(); 
			ps.setLong(1, criteriaCode);

			String fieldValue = data.getFieldValue(); 
			if (fieldValue != null) {
				ps.setString(2, fieldValue);
			} else {
				ps.setString(2, null);
			}
			
			String underwriter = data.getUserCode(); 
			if (underwriter != null) {
				ps.setString(3, underwriter);
			} else {
				ps.setString(3, null);
			}			
			
			String createdBy = data.getCreatedBy(); 
			if (createdBy != null) {
				ps.setString(4, createdBy);
			} else {
				ps.setString(4, null);
			}

			ps.setTimestamp(5, DateHelper.sqlTimestamp(data.getCreateDate()));
									
			ps.executeUpdate();
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		
		}finally{
			this.closeResources(ps, null);
		}
		LOGGER.info("insertMapping end");
	}//insertMapping
	

	public void updateMapping(AutoAssignmentData data) throws SQLException{
		
		LOGGER.info("updateMapping start");
		String sql = "UPDATE AUTO_ASSIGNMENTS " +
					 "SET USER_ID = ?, CRITERIA_FIELD_VALUE = ?, UPDATED_BY = ?, UPDATED_DATE = ? " +
					 "WHERE AA_ID = ?";
		PreparedStatement ps = null;
		try {
			
			ps = conn.prepareStatement(sql);
			ps.setString(1, data.getUserCode());
			ps.setString(2, data.getFieldValue());
			ps.setString(3, data.getUpdatedBy());
			ps.setTimestamp(4, DateHelper.sqlTimestamp(data.getUpdateDate()));
			ps.setLong(5, data.getAutoAssignmentId());
			ps.executeUpdate();
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		}finally{
			this.closeResources(ps, null);
		}
		
		LOGGER.info("updateMapping end");
	}//updateMapping	
	

	public boolean isDuplicate(long id, long criteriaCode, String criteriaField, String underwriter) throws SQLException {
		
		LOGGER.info("isDuplicate start");
		String sql = "SELECT AA_ID AS AUTO_ASSIGNMENT_ID, AAC_ID AS CRITERIA_CODE, CRITERIA_FIELD_VALUE AS CRITERIA_VALUE, USER_ID AS UNDERWRITER " +
					 "FROM AUTO_ASSIGNMENTS " +
					 "WHERE (AAC_ID = ? AND CRITERIA_FIELD_VALUE = ? AND USER_ID = ?) AND (AA_ID <> ?) " +
					 "ORDER BY AAC_ID, CRITERIA_FIELD_VALUE";
				
		PreparedStatement ps = null;
		ResultSet rs = null;
		boolean isDuplicate = false;	
		try{
			ps = conn.prepareStatement(sql);
			ps.setLong(1, criteriaCode);
			ps.setString(2, criteriaField);
			ps.setString(3, underwriter);
			ps.setLong(4, id);
			rs = ps.executeQuery();
	
				
			if (rs.next()) {
				isDuplicate = true;
			}
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		}finally{
			this.closeResources(ps, null);
		}
		LOGGER.info("isDuplicate end");
		return (isDuplicate);
	}//isDuplicate
	



}
