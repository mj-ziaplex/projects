/**
 * ReferenceDAO.java				Copyright (c) 2004 Pointwest Technologies Corp.
 * TODO DOCUMENT ME!
 * 
 * @author aditalo	 @date Jan 21, 2004
 */
package com.slocpi.ium.data.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.slocpi.ium.data.RequirementFormData;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.DateHelper;
import com.slocpi.ium.util.SortHelper;


/**
 * TODO DOCUMENT ME!
 * 
 * @author tvicencio		Copyright (c) 2004 Pointwest Technologies Corp.
 * @date Feb 23, 2004
 */
public class RequirementFormDAO {

	private static final Logger LOGGER = LoggerFactory.getLogger(RequirementFormDAO.class);
	private Connection conn = null;	


	public RequirementFormDAO(Connection connection) {
		super();
		this.conn = connection;
	}
	
	private void closeResources(PreparedStatement ps, ResultSet rs) throws SQLException{
		
		
		try {
			if (ps != null){
				ps.close();
			}
		}
		catch(SQLException e)
		{
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		}
		try {
			if (rs != null){
				rs.close();
			}			
		} catch (SQLException e){
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		}
		
	}

	public ArrayList retrieveRequirementForms(SortHelper sort) throws SQLException {
		
		LOGGER.info("retrieveRequirementForms start");
		String sql = "SELECT RF_ID, RF_NAME, RF_TEMPLATE_FORMAT, RF_TEMPLATE_NAME, RF_STATUS " +
					 "FROM REQUIREMENT_FORMS " +
		             sort.getOrderBy();
		PreparedStatement ps  = null;
		ResultSet rs = null;
		ArrayList list = null;
		try{
			
		ps = conn.prepareStatement(sql);
		rs = ps.executeQuery();

		list = new ArrayList();
		while (rs.next()) {
			RequirementFormData data = new RequirementFormData();
			data.setFormId(rs.getLong("RF_ID"));
			data.setFormName(rs.getString("RF_NAME"));
			data.setTemplateFormat(rs.getString("RF_TEMPLATE_FORMAT"));
			data.setTemplateName(rs.getString("RF_TEMPLATE_NAME"));
			data.setStatus(rs.getBoolean("RF_STATUS"));
			list.add(data);
		}
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			  throw e; 
		}finally{
			  this.closeResources(ps, rs);
		}
		LOGGER.info("retrieveRequirementForms end");
		return (list);	
	}//retrieveRequirementForms


	public RequirementFormData retrieveRequirementFormDetail(String formName) throws SQLException {
		
		LOGGER.info("retrieveRequirementFormDetail start");
		String sql = "SELECT RF_ID, RF_NAME, RF_TEMPLATE_FORMAT, RF_TEMPLATE_NAME, RF_STATUS " +
					 "FROM REQUIREMENT_FORMS " +
					 "WHERE RF_NAME = ? " +
					 "ORDER BY RF_NAME ";
		PreparedStatement ps  = null;
		ResultSet rs = null;
		try {
			
			ps = conn.prepareStatement(sql);
			ps.setString(1, formName);
			rs = ps.executeQuery();

			RequirementFormData data = null;
			if (rs.next()) {
				data = new RequirementFormData();
				data.setFormId(rs.getLong("RF_ID"));
				data.setFormName(rs.getString("RF_NAME"));
				data.setTemplateFormat(rs.getString("RF_TEMPLATE_FORMAT"));
				data.setTemplateName(rs.getString("RF_TEMPLATE_NAME"));
				data.setStatus(rs.getBoolean("RF_STATUS"));
			}
 
			return (data);	
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			  throw e; 
		}finally{
			  this.closeResources(ps, rs);
			  LOGGER.info("retrieveRequirementFormDetail end");
		}
		
	}//retrieveRequirementFormDetail
	

	public void insertRequirementForm(RequirementFormData data) throws SQLException {
		
		LOGGER.info("insertRequirementForm start");
		String sql = "INSERT INTO REQUIREMENT_FORMS (RF_ID, RF_NAME, RF_TEMPLATE_FORMAT, RF_TEMPLATE_NAME, RF_STATUS, CREATED_BY, CREATED_DATE, UPDATED_BY, UPDATED_DATE)" +
					 "VALUES(SEQ_REQUIREMENT_FORM.NEXTVAL,?,?,?,?,?,?,?,?)";
		PreparedStatement ps  = null;
		ResultSet rs = null;
		try {
			
			ps = conn.prepareStatement(sql);
		
			String formName = data.getFormName(); 
			if (formName != null) {
				ps.setString(1, formName);
			} else {
				ps.setString(1, null);
			}

			String templateFormat = data.getTemplateFormat(); 
			if (templateFormat != null) {
				ps.setString(2, templateFormat);
			} else {
				ps.setString(2, null);
			}			
		
			String templateName = data.getTemplateName();
			if (templateName != null) {
				ps.setString(3, templateName);
			} else {
				ps.setString(3, null);
			}

			boolean status = data.getStatus(); 
			ps.setBoolean(4, status);
									
			String createdBy = data.getCreatedBy(); 
			if (createdBy != null) {
				ps.setString(5, createdBy);
			} else {
				ps.setString(5, null);
			}

			ps.setTimestamp(6, DateHelper.sqlTimestamp(data.getCreateDate()));
			
			String updatedBy = data.getUpdatedBy(); 
			if (updatedBy != null) {
				ps.setString(7, updatedBy);
			} else {
				ps.setString(7, null);
			}

			ps.setTimestamp(8, DateHelper.sqlTimestamp(data.getUpdateDate()));
												
			ps.executeUpdate();
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			  throw e; 
		}finally{
			  this.closeResources(ps, rs);
		}
		LOGGER.info("insertRequirementForm end");
	}//insertRequirementForm


	public void updateRequirementForm(RequirementFormData data) throws SQLException {
		
		LOGGER.info("updateRequirementForm start");
		String sql = "UPDATE REQUIREMENT_FORMS " +
					 "SET RF_TEMPLATE_FORMAT = ?, RF_TEMPLATE_NAME = ?, RF_STATUS = ?, UPDATED_BY = ?, UPDATED_DATE = ? " +
					 "WHERE RF_NAME = ?";
		
		PreparedStatement ps  = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement(sql);
			ps.setString(1, data.getTemplateFormat());
			ps.setString(2, data.getTemplateName());
			ps.setBoolean(3, data.getStatus());			
			ps.setString(4, data.getUpdatedBy());
			ps.setTimestamp(5, DateHelper.sqlTimestamp(data.getUpdateDate()));
			ps.setString(6, data.getFormName());					
			ps.executeUpdate();
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			  throw e; 
		}finally{
			  this.closeResources(ps, rs);
		}
		LOGGER.info("updateRequirementForm end");
	}//updateRequirementForm





}
