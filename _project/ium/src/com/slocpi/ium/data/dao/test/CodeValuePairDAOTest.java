/*
 * Created on Jun 24, 2004
 *
 */
package com.slocpi.ium.data.dao.test;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
//import java.util.Date;

import junit.framework.TestCase;

import com.slocpi.ium.data.AccessTemplateData;
import com.slocpi.ium.data.AutoAssignCriteriaData;
import com.slocpi.ium.data.ClientTypeData;
import com.slocpi.ium.data.DocumentTypeData;
import com.slocpi.ium.data.ExaminationAreaData;
import com.slocpi.ium.data.ExaminationPlaceData;
import com.slocpi.ium.data.LOBData;
import com.slocpi.ium.data.RolesData;
import com.slocpi.ium.data.SectionData;
import com.slocpi.ium.data.SpecializationData;
import com.slocpi.ium.data.StatusData;
import com.slocpi.ium.data.SunLifeDeptData;
import com.slocpi.ium.data.dao.CodeValuePairDAO;
import com.slocpi.ium.data.util.DataSourceProxy;

/**
 * @author Cris Gironella
 *
 */
public class CodeValuePairDAOTest extends TestCase {

	/**
	 * Constructor for CodeValuePairDAOTest.
	 * @param arg0
	 */
	public CodeValuePairDAOTest(String arg0) {
		super(arg0);
	}

	public static void main(String[] args) {
	}

	/*
	 * @see TestCase#setUp()
	 */
	protected void setUp() throws Exception {
		super.setUp();
	}

	/*
	 * @see TestCase#tearDown()
	 */
	protected void tearDown() throws Exception {
		super.tearDown();
	}

	public void testRetrieveLOB() throws SQLException {
		
		Connection conn = new DataSourceProxy().getConnection();
		CodeValuePairDAO testDAO = new CodeValuePairDAO(conn);
		ArrayList resultSet = new ArrayList();
		
		resultSet = testDAO.retrieveLOB();
		
		ArrayList expectedLOBCode =  new ArrayList();
		  expectedLOBCode.add("0");
		  expectedLOBCode.add("A1");
		  expectedLOBCode.add("GL");
		  expectedLOBCode.add("IL");
		  expectedLOBCode.add("JK");
		  expectedLOBCode.add("MF");
		  expectedLOBCode.add("PN");
		  expectedLOBCode.add("ZZ");
		
		ArrayList  expectedLOBDesc  =new ArrayList();
		  expectedLOBDesc.add("1234567890123456789012345");
		  expectedLOBDesc.add("A1 - TEST UPDATE");	  
		  expectedLOBDesc.add("Group Life");
		  expectedLOBDesc.add("Individual Life");
		  expectedLOBDesc.add("JUNE KUMAG");
		  expectedLOBDesc.add("Mutual Funds");
		  expectedLOBDesc.add("Pre-need");
		  expectedLOBDesc.add("1234567890123456789012345");
		
		for (int i = 0; i < resultSet.size(); i++){
			assertEquals(expectedLOBCode.get(i), ((LOBData)resultSet.get(i)).getLOBCode());
			assertEquals(expectedLOBDesc.get(i),((LOBData)resultSet.get(i)).getLOBDesc());
		}
		
	}

	public void testRetrieveLOBDetail() throws SQLException {
		Connection conn = new DataSourceProxy().getConnection();
		CodeValuePairDAO testDAO = new CodeValuePairDAO(conn);
		LOBData resultSet = new LOBData();
		
		//LOBData exists
		resultSet = testDAO.retrieveLOBDetail("JK");
		assertEquals("JK",resultSet.getLOBCode());
		assertEquals("JUNE KUMAG", resultSet.getLOBDesc());
		
		//LOBData does not exist
		assertNull(testDAO.retrieveLOBDetail("MK"));
		
		//LOBCode in different case
		assertNull(testDAO.retrieveLOBDetail("jk"));
		
		//Leading spaces
		assertNull(testDAO.retrieveLOBDetail(" JK"));
		
		//trailing spaces
		assertNull(testDAO.retrieveLOBDetail("JK "));
		
		//null string as parameter
		assertNull(testDAO.retrieveLOBDetail(" "));
}

	public void testRetrieveDepartments() throws SQLException {
		Connection conn = new DataSourceProxy().getConnection();
		CodeValuePairDAO testDAO = new CodeValuePairDAO(conn);
		ArrayList resultSet = new ArrayList();
		
		resultSet = testDAO.retrieveDepartments();
		
		ArrayList expectedDeptCode = new ArrayList(); 
		expectedDeptCode.add("0");
		expectedDeptCode.add("1234567890");
		expectedDeptCode.add("A1");
		expectedDeptCode.add("CLIENTSVCS");
		expectedDeptCode.add("GROUPINS");
		expectedDeptCode.add("MEDICAL");
		expectedDeptCode.add("MGMTSVCS");
		expectedDeptCode.add("OTHERDEPT");
		expectedDeptCode.add("SALESMKTG");
		expectedDeptCode.add("SYSTEMS");
		expectedDeptCode.add("UWSVCS");
		
		ArrayList expectedDeptDesc = new ArrayList();
		expectedDeptDesc.add("12345678901234567890123456789012345678901234567890");
		expectedDeptDesc.add("12345678901234567890123456789012345678901234567890");
		expectedDeptDesc.add("A1");
		expectedDeptDesc.add("CLIENT SERVICES DEPARTMENT");
		expectedDeptDesc.add("GROUP INSURANCE DEPARTMENT");
		expectedDeptDesc.add("MEDICAL DEPARTMENT");
		expectedDeptDesc.add("MANAGEMENT SERVICES DEPARTMENT");
		expectedDeptDesc.add("OTHER DEPARTMENTS");
		expectedDeptDesc.add("SALES AND MARKETING DEPARTMENT");
		expectedDeptDesc.add("CENTRAL SYSTEMS");
		expectedDeptDesc.add("UNDERWRITING SERVICES DEPARTMENT");
		
		for (int i = 0; i < resultSet.size(); i++){
			assertEquals(expectedDeptCode.get(i), ((SunLifeDeptData)resultSet.get(i)).getDeptId());
			assertEquals(expectedDeptDesc.get(i), ((SunLifeDeptData)resultSet.get(i)).getDeptDesc());
		}
	}

	public void testRetrieveDepartmentDetail() throws SQLException {
		Connection conn = new DataSourceProxy().getConnection();
		CodeValuePairDAO testDAO = new CodeValuePairDAO(conn);
		SunLifeDeptData resultData = new SunLifeDeptData();
		
		//existing in database
		resultData = testDAO.retrieveDepartmentDetail("MEDICAL");
		assertEquals("MEDICAL", resultData.getDeptId());
		assertEquals("MEDICAL DEPARTMENT", resultData.getDeptDesc());
		
		//dept id does not exist in database
		assertNull(testDAO.retrieveDepartmentDetail("IT"));
		
		//dept id in different letter case
		assertNull(testDAO.retrieveDepartmentDetail("mEdIcAl"));
		
		//leading spaces when passing the parameter
		assertNull(testDAO.retrieveDepartmentDetail(" MEDICAL"));
		
	}

	public void testRetrieveSections() throws SQLException {
		Connection conn = new DataSourceProxy().getConnection();
		CodeValuePairDAO testDAO = new CodeValuePairDAO(conn);
		ArrayList resultSet = new ArrayList();
		
		resultSet = testDAO.retrieveSections();
		
		ArrayList expectedSectionCode = new ArrayList();
		expectedSectionCode.add("0");
		expectedSectionCode.add("123456789012345");
		expectedSectionCode.add("A1");
		expectedSectionCode.add("AGENTS");
		expectedSectionCode.add("DATACTR");
		expectedSectionCode.add("FACILITN");
		expectedSectionCode.add("GROUPADM");
		expectedSectionCode.add("MEDADMIN");
		expectedSectionCode.add("MEDCONSLT");
		expectedSectionCode.add("MEDPROVIDER");
		expectedSectionCode.add("NBADMIN");
		expectedSectionCode.add("NBREVIEW");
		expectedSectionCode.add("OTHERSECT");
		expectedSectionCode.add("PMGMTOFF");
		expectedSectionCode.add("POLICYCHG");
		expectedSectionCode.add("POLICYISUE");
		expectedSectionCode.add("QAINFOSEC");
		expectedSectionCode.add("QASSURANCE");
		expectedSectionCode.add("SALESADMIN");
		expectedSectionCode.add("UWRITING");
		
		ArrayList expectedSectionDesc = new ArrayList();
		expectedSectionDesc.add("12345678901234567890123456789012345678901234567890");
		expectedSectionDesc.add("12345678901234567890123456789012345678901234567890");
		expectedSectionDesc.add("A1");
		expectedSectionDesc.add("AGENTS' GROUP");
		expectedSectionDesc.add("DATA CENTER");
		expectedSectionDesc.add("UNDERWRITING FACILITATION");
		expectedSectionDesc.add("GROUP ADMINISTRATION");
		expectedSectionDesc.add("MEDICAL ADMINISTRATION");
		expectedSectionDesc.add("MEDICAL CONSULTATION");
		expectedSectionDesc.add("ACCREDITED MEDICAL PROVIDERS");
		expectedSectionDesc.add("NEW BUSINESS ADMIN");
		expectedSectionDesc.add("NEW BUSINESS REVIEW GROUP");
		expectedSectionDesc.add("OTHER DEPARTMENT SECTIONS");
		expectedSectionDesc.add("PROJECT MANAGEMENT OFFICE");
		expectedSectionDesc.add("POLICY CHANGE");
		expectedSectionDesc.add("POLICY ISSUE");
		expectedSectionDesc.add("QUALITY ASSURANCE AND INFO SECURITY");
		expectedSectionDesc.add("QUALITY ASSURANCE");
		expectedSectionDesc.add("SALES ADMINISTRATION");
		expectedSectionDesc.add("UNDERWRITING");
		
		for (int i=0; i< resultSet.size(); i++){
			assertEquals(expectedSectionCode.get(i),((SectionData)resultSet.get(i)).getSectionId());
			assertEquals(expectedSectionDesc.get(i), ((SectionData)resultSet.get(i)).getSectionDesc());
		}
	
	}

	public void testRetrieveSectionDetail() throws SQLException {
		Connection conn = new DataSourceProxy().getConnection();
		CodeValuePairDAO testDAO = new CodeValuePairDAO(conn);
		SectionData resultSet = new SectionData();
		
		//Section Code exists in database
		resultSet = testDAO.retrieveSectionDetail("AGENTS");
		assertEquals("AGENTS", resultSet.getSectionId());
		assertEquals("AGENTS' GROUP", resultSet.getSectionDesc());
		
		//Section Code does NOT exist in database
		assertNull(testDAO.retrieveSectionDetail("UL"));
		
		//Section Code in small case / mixed case
		assertNull(testDAO.retrieveSectionDetail("aGeNtS"));
		
	}

	public void testRetrieveExamSpecializations() throws SQLException {
		Connection conn = new DataSourceProxy().getConnection();
		CodeValuePairDAO testDAO = new CodeValuePairDAO(conn);
		ArrayList resultSet = testDAO.retrieveExamSpecializations(); 
		
	    assertEquals(0, ((SpecializationData)resultSet.get(0)).getSpecializationId());	
	    assertEquals("TEST",((SpecializationData)resultSet.get(2)).getSpecializationDesc());
	}

	public void testRetrieveExamSpecializationDetail() throws SQLException {
		Connection conn =  new DataSourceProxy().getConnection();
		CodeValuePairDAO testDAO = new CodeValuePairDAO(conn);
		SpecializationData resultSet = new SpecializationData();
		//specialization exists in database 
		resultSet = testDAO.retrieveExamSpecializationDetail(3);
		assertEquals(3, resultSet.getSpecializationId());
		assertEquals("Pediatric", resultSet.getSpecializationDesc());
		//specialization does NOT exist in database
		assertNull(testDAO.retrieveExamSpecializationDetail(9));
	}

	public void testRetrieveExamPlaces() throws SQLException {
		Connection conn = new DataSourceProxy().getConnection();
		CodeValuePairDAO testDAO = new CodeValuePairDAO(conn);
		ArrayList resultSet = new ArrayList();
		resultSet = testDAO.retrieveExamPlaces();
		
		long ExamPlaceId = ((ExaminationPlaceData)resultSet.get(0)).getExaminationPlaceId();
		String ExamPlaceDesc = ((ExaminationPlaceData)resultSet.get(0)).getExaminationPlaceDesc();
		
		assertEquals(1, ExamPlaceId);
		assertEquals("SAN ANTONIO", ExamPlaceDesc);
		
		ArrayList expectedEpDesc = new ArrayList();
		expectedEpDesc.add("SAN ANTONIO");
		expectedEpDesc.add("MEDICAL CLINIC");
		expectedEpDesc.add("PEDIATRICIAN ROOM");
		
		long[] expectedEpId = new long[3];
		expectedEpId[0] = 1;
		expectedEpId[1] = 2;
		expectedEpId[2] = 3;
		
		
		for (int i=0; i < 3; i++){
			assertEquals(expectedEpId[i],((ExaminationPlaceData)resultSet.get(i)).getExaminationPlaceId());
			assertEquals(expectedEpDesc.get(i),((ExaminationPlaceData)resultSet.get(i)).getExaminationPlaceDesc());
		}
		
		
	}

	public void testRetrieveExamPlaceDetail() throws SQLException {
		Connection conn = new DataSourceProxy().getConnection();
		CodeValuePairDAO testDAO = new CodeValuePairDAO(conn);
		ExaminationPlaceData resultSet = new ExaminationPlaceData();
		
		//exam id and desc in database
		resultSet = testDAO.retrieveExamPlaceDetail(1);
		assertEquals(1, resultSet.getExaminationPlaceId());
		assertEquals("SAN ANTONIO", resultSet.getExaminationPlaceDesc());
		
		//exam id not in database
		assertNull(testDAO.retrieveExamPlaceDetail(4));
		
	}


	public void testRetrieveExamAreas() throws SQLException {
		
		Connection conn = new DataSourceProxy().getConnection();
		CodeValuePairDAO testDAO = new CodeValuePairDAO(conn);
		ArrayList resultSet = testDAO.retrieveExamAreas();
		
		assertEquals(999, ((ExaminationAreaData)resultSet.get(0)).getExaminationAreaId());
		assertEquals("1234567890123456789012345", ((ExaminationAreaData)resultSet.get(0)).getExaminationAreaDesc());
	}

	public void testRetrieveExamAreaDetail() throws SQLException {
		Connection conn = new DataSourceProxy().getConnection();
		CodeValuePairDAO testDAO = new CodeValuePairDAO(conn);
		//EA ID exists in database
		ExaminationAreaData resultSet = testDAO.retrieveExamAreaDetail(999);
		assertEquals("1234567890123456789012345", resultSet.getExaminationAreaDesc());
		assertEquals(999, resultSet.getExaminationAreaId());
		//EA ID does not exist in database
		assertNull(testDAO.retrieveExamAreaDetail(1));
				
	}
	public void insertExamArea(ExaminationAreaData data) throws SQLException {
	/*	Connection conn = new DataSourceProxy().getConnection();
		CodeValuePairDAO testDAO = new CodeValuePairDAO(conn);
		Date currentDate = new Date();
		ExaminationAreaData newExamArea = new ExaminationAreaData();
		newExamArea.setExaminationAreaId(432);
		newExamArea.setExaminationAreaDesc("Makati");
		newExamArea.setCreatedBy("IUMDEV");
		newExamArea.setCreateDate(currentDate);
		testDAO.insertExamArea(newExamArea);
		
		assertEquals(newExamArea, testDAO.retrieveExamAreaDetail(432));
		*/
	}
	
	public void testRetrieveDocumentTypes() throws SQLException {
		Connection conn = new DataSourceProxy().getConnection();
		CodeValuePairDAO testDAO = new CodeValuePairDAO(conn);
		ArrayList resultSet = testDAO.retrieveDocumentTypes();
		ArrayList expectedDocCodes = new ArrayList();
		ArrayList expectedDocTypes = new ArrayList();
		expectedDocCodes.add("A1");
		expectedDocTypes.add("a1");
		expectedDocCodes.add("APL");
		expectedDocTypes.add("Application");
		expectedDocCodes.add("BOC");
		expectedDocTypes.add("Bounced Cheque");
		expectedDocCodes.add("COI");
		expectedDocTypes.add("Certificate of Insurability");
		expectedDocCodes.add("POC");
		expectedDocTypes.add("Policy Contract");
		expectedDocCodes.add("ROC");
		expectedDocTypes.add("Request for Change");
		expectedDocCodes.add("ROF");
		expectedDocTypes.add("Simplified Reinstatement Offer Form");
		expectedDocCodes.add("SCQ");
		expectedDocTypes.add("Staled Cheque");
		expectedDocCodes.add("YYY");
		expectedDocTypes.add("TEST CAPS");
		expectedDocCodes.add("ZZZ");
		expectedDocTypes.add("1234567890123456789012345678901234567890");
		for (int i=0; i< resultSet.size(); i++){
			System.out.println(expectedDocCodes.get(i) + " " + expectedDocTypes.get(i));
			assertEquals(expectedDocCodes.get(i), ((DocumentTypeData)resultSet.get(i)).getDocCode());
			assertEquals(expectedDocTypes.get(i), ((DocumentTypeData)resultSet.get(i)).getDocDesc());
		}
	}
	

	public void testRetrieveDocumentTypeDetail() throws SQLException {
		Connection conn = new DataSourceProxy().getConnection();
		CodeValuePairDAO testDAO = new CodeValuePairDAO(conn);
		//Doc ID exists in database
		DocumentTypeData resultSet = testDAO.retrieveDocumentTypeDetail("YYY");
		assertEquals("TEST CAPS", resultSet.getDocDesc());
		assertEquals("YYY", resultSet.getDocCode());
		//Doc ID does not exist in database
		assertNull(testDAO.retrieveDocumentTypeDetail("mcg"));
		//mixed case / small case Doc Id
		assertNull(testDAO.retrieveDocumentTypeDetail("YyY"));
	}

	public void testRetrieveClientTypes() throws SQLException {
		Connection conn = new DataSourceProxy().getConnection();
		CodeValuePairDAO testDAO = new CodeValuePairDAO(conn);
		ArrayList resultSet = testDAO.retrieveClientTypes();
		
		ArrayList expectedClientCode = new ArrayList();
		expectedClientCode.add("A");
		expectedClientCode.add("I");
		expectedClientCode.add("M");
		expectedClientCode.add("O");
		expectedClientCode.add("P");
		expectedClientCode.add("X");
		expectedClientCode.add("Z");
				
		ArrayList expectedClientDesc = new ArrayList();
		expectedClientDesc.add("test - update");
		expectedClientDesc.add("Insured");
		expectedClientDesc.add("Member");
		expectedClientDesc.add("Owner");
		expectedClientDesc.add("Planholder");
		expectedClientDesc.add("1234567890123456789012345");
		expectedClientDesc.add("TEST CAPS");
		
	
		for (int i = 0; i < resultSet.size(); i++){
			assertEquals(expectedClientCode.get(i),(((ClientTypeData)resultSet.get(i)).getClientTypeCode()));
			assertEquals(expectedClientDesc.get(i),(((ClientTypeData)resultSet.get(i)).getClientTypeDesc()));
		}
	}
	

	public void testRetrieveClientTypeDetail() throws SQLException {
		Connection conn = new DataSourceProxy().getConnection();
		CodeValuePairDAO testDAO = new CodeValuePairDAO(conn);
		//Client Type Code exists in database
		ClientTypeData resultSet = testDAO.retrieveClientTypeDetail("M");
		assertEquals("Member", resultSet.getClientTypeDesc());
		assertEquals("M", resultSet.getClientTypeCode());
		//ClientType Code does not exist in database
		assertNull(testDAO.retrieveClientTypeDetail("mcg"));
		//mixed case / small case Doc Id
		assertNull(testDAO.retrieveDocumentTypeDetail("m"));
	}

	public void testRetrieveAutoAssignCriteria() throws SQLException {
		Connection conn = new DataSourceProxy().getConnection();
		CodeValuePairDAO testDAO = new CodeValuePairDAO(conn);
		ArrayList resultSet = testDAO.retrieveAutoAssignCriteria();
		
		long[] expectedAACId = new long[5];
		expectedAACId[0]= 14;
		expectedAACId[1] = 16;
		expectedAACId[2] = 17;
		expectedAACId[3] = 18;
		expectedAACId[4] = 41;
				
		ArrayList expectedAACField = new ArrayList();
		expectedAACField.add("AGENT");
		expectedAACField.add("ASSIGNED_TO");
		expectedAACField.add("BRANCH");
		expectedAACField.add("LOB");
		expectedAACField.add("AGENT");
		
		ArrayList expectedAACDesc = new ArrayList();
		expectedAACDesc.add("MED");
		expectedAACDesc.add("ASSIGNED TO");
		expectedAACDesc.add("BRANCH");
		expectedAACDesc.add("Lines of Business");
		expectedAACDesc.add("MED");
		
		for (int i = 0; i < resultSet.size(); i++){
		assertEquals(expectedAACId[i],(((AutoAssignCriteriaData)resultSet.get(i)).getCriteriaId()));
		assertEquals(expectedAACField.get(i),(((AutoAssignCriteriaData)resultSet.get(i)).getFieldCode()));
		assertEquals(expectedAACDesc.get(i),(((AutoAssignCriteriaData)resultSet.get(i)).getFieldDesc()));
		}
		
	}

	public void testRetrieveAutoAssignCriteriaDetail() throws SQLException {
		Connection conn = new DataSourceProxy().getConnection();
		CodeValuePairDAO testDAO = new CodeValuePairDAO(conn);
		//AAC Field Code exists in database
		AutoAssignCriteriaData resultSet = testDAO.retrieveAutoAssignCriteriaDetail("LOB");
		assertEquals("Lines of Business", resultSet.getFieldDesc());
		assertEquals(18, resultSet.getCriteriaId());
		assertEquals("LOB", resultSet.getFieldCode());
		//AAC Field Code does not exist in database
		assertNull(testDAO.retrieveAutoAssignCriteriaDetail("mcg"));
		//mixed case / small case AAC Field Code
		assertNull(testDAO.retrieveDocumentTypeDetail("LoB"));
	}

	public void testRetrieveStatusCodes() throws SQLException {
		Connection conn = new DataSourceProxy().getConnection();
		CodeValuePairDAO testDAO = new CodeValuePairDAO(conn);
		ArrayList resultSet = testDAO.retrieveStatusCodes();
		
		long[] expectedStatusId = new long[30];
		expectedStatusId[0]= 1;
		expectedStatusId[1]= 2;
		expectedStatusId[2]= 3;
		expectedStatusId[3]= 4;
		expectedStatusId[4]= 5;
		expectedStatusId[5]= 6;
		expectedStatusId[6]= 7;
		expectedStatusId[7]= 8;
		expectedStatusId[8]= 9;
		expectedStatusId[9]= 10;
		expectedStatusId[10]= 11;
		expectedStatusId[11]= 12;
		expectedStatusId[12]= 13;
		expectedStatusId[13]= 14;
		expectedStatusId[14]= 15;
		expectedStatusId[15]= 16;
		expectedStatusId[16]= 17;
		expectedStatusId[17]= 18;
		expectedStatusId[18]= 19;
		expectedStatusId[19]= 20;
		expectedStatusId[20]= 21;
		expectedStatusId[21]= 22;
		expectedStatusId[22]= 23;
		expectedStatusId[23]= 24;
		expectedStatusId[24]= 25;
		expectedStatusId[25]= 26;
		expectedStatusId[26]= 27;
		expectedStatusId[27]= 121;
		expectedStatusId[28]= 141;
		expectedStatusId[29]= 181;
		
		ArrayList expectedStatusDesc = new ArrayList();
		expectedStatusDesc.add("NB Review Action");
		expectedStatusDesc.add("For Transmittal USD");
		expectedStatusDesc.add("For Facilitator's Action");
		expectedStatusDesc.add("For Assessment");
		expectedStatusDesc.add("Undergoing Assessment");
		expectedStatusDesc.add("For Approval");
		expectedStatusDesc.add("Approved");
		expectedStatusDesc.add("Declined");
		expectedStatusDesc.add("For Offer");
		expectedStatusDesc.add("Awaiting Requirements");
		expectedStatusDesc.add("Awaiting Medical");
		expectedStatusDesc.add("Not Proceeded With");
		expectedStatusDesc.add("Ordered");
		expectedStatusDesc.add("Received in site");
		expectedStatusDesc.add("Waived");
		expectedStatusDesc.add("Reviewed And Rejected");
		expectedStatusDesc.add("Reviewed And Accepted");
		expectedStatusDesc.add("Requested");
		expectedStatusDesc.add("Confirmed");
		expectedStatusDesc.add("Valid");
		expectedStatusDesc.add("Expired");
		expectedStatusDesc.add("Not Submitted");
		expectedStatusDesc.add("Cancelled");
		expectedStatusDesc.add("NTO");
		expectedStatusDesc.add("SAA");
		expectedStatusDesc.add("SCC");
		expectedStatusDesc.add("SIR");
		expectedStatusDesc.add("Cancelled");
		expectedStatusDesc.add("Cancelled");
		expectedStatusDesc.add("For Referral");
		
		ArrayList expectedStatusType = new ArrayList();
			expectedStatusType.add("AR");
			expectedStatusType.add("AR");
			expectedStatusType.add("AR");	
			expectedStatusType.add("AR");;
			expectedStatusType.add("AR");
			expectedStatusType.add("AR");
			expectedStatusType.add("AR");
			expectedStatusType.add("AR");
			expectedStatusType.add("AR");
			expectedStatusType.add("AR");
			expectedStatusType.add("AR");
			expectedStatusType.add("AR");
			expectedStatusType.add("NM");
			expectedStatusType.add("NM");
			expectedStatusType.add("NM");
			expectedStatusType.add("NM");
			expectedStatusType.add("NM");
			expectedStatusType.add("M");
			expectedStatusType.add("M");
			expectedStatusType.add("M");
			expectedStatusType.add("M");
			expectedStatusType.add("M");
			expectedStatusType.add("M");
			expectedStatusType.add("NM");
			expectedStatusType.add("NM");
			expectedStatusType.add("NM");
			expectedStatusType.add("NM");
			expectedStatusType.add("NM");
			expectedStatusType.add("AR");
			expectedStatusType.add("AR");

		
				for (int i = 0; i < resultSet.size(); i++){
				assertEquals(expectedStatusId[i],(((StatusData)resultSet.get(i)).getStatusId()));
				assertEquals(expectedStatusDesc.get(i),(((StatusData)resultSet.get(i)).getStatusDesc()));
				assertEquals(expectedStatusType.get(i),(((StatusData)resultSet.get(i)).getStatusType()));
				}
		
	}

	public void testRetrieveStatusCodeDetail() throws SQLException {
		Connection conn = new DataSourceProxy().getConnection();
		CodeValuePairDAO testDAO = new CodeValuePairDAO(conn);
	//Status Code exists in database
		StatusData resultSet = testDAO.retrieveStatusCodeDetail(14);
		assertEquals("Received in site", resultSet.getStatusDesc());
	    assertEquals(14, resultSet.getStatusId());
		assertEquals("NM", resultSet.getStatusType());
	//Status Code does not exist in database
		assertNull(testDAO.retrieveStatusCodeDetail(81));
	
	}

	public void testRetrieveRoles() throws SQLException {
		Connection conn = new DataSourceProxy().getConnection();
		CodeValuePairDAO testDAO = new CodeValuePairDAO(conn);
		ArrayList resultSet = testDAO.retrieveRoles();
		
				ArrayList expectedRoleCode = new ArrayList();
				expectedRoleCode.add("A1");
				expectedRoleCode.add("AA");
				expectedRoleCode.add("AGENT");
				expectedRoleCode.add("EXAMINER");
				expectedRoleCode.add("FACILITATOR");
				expectedRoleCode.add("FACILITSUPMGR");
				expectedRoleCode.add("GRPNBADMIN");
				expectedRoleCode.add("LABSTAFF");
				expectedRoleCode.add("LOVADMIN");
				expectedRoleCode.add("MEDCONSLT");
				expectedRoleCode.add("MEDICALADMIN");
				expectedRoleCode.add("MEDSUPMGR");
				expectedRoleCode.add("MKTGSTAFF");
				expectedRoleCode.add("NBREVIEWER");
				expectedRoleCode.add("NBSTAFF");
				expectedRoleCode.add("NBSUPMGR");
				expectedRoleCode.add("OTHERUSER");
				expectedRoleCode.add("SCTYADMIN");
				expectedRoleCode.add("SYSOPER");
				expectedRoleCode.add("TEST");
				expectedRoleCode.add("UNDERWRITER");
				expectedRoleCode.add("UWTNGSUPMGR");
				
				ArrayList expectedRoleDesc = new ArrayList();
				expectedRoleDesc.add("TEST UPDATE");
				expectedRoleDesc.add("TEST UPDATE");
				expectedRoleDesc.add("AGENTS");
				expectedRoleDesc.add("ACCREDITED DOCTOR/EXAMINER");
				expectedRoleDesc.add("UNDERWRITING FACILITATOR");
				expectedRoleDesc.add("FACILITATOR SUPERVISOR/MANAGER");
				expectedRoleDesc.add("GROUP NEW BUSINESS ADMINISTRATOR");
				expectedRoleDesc.add("ACCREDITED LABORATORY STAFF");
				expectedRoleDesc.add("LIST OF VALUES ADMINISTRATOR");
				expectedRoleDesc.add("MEDICAL CONSULTANT");
				expectedRoleDesc.add("MEDICAL ADMIN STAFF");
				expectedRoleDesc.add("MEDICAL SUPERVISOR/MANAGER/CONSULTANG");
				expectedRoleDesc.add("MARKETING STAFF");
				expectedRoleDesc.add("NEW BUSINESS REVIEWER");
				expectedRoleDesc.add("NEW BUSINESS STAFF");
				expectedRoleDesc.add("NEW BUSINESS SUPERVISOR/MANAGER");
				expectedRoleDesc.add("OTHER DEPARMENT USERS");
				expectedRoleDesc.add("SECURITY ADMIN");
				expectedRoleDesc.add("SYSTEM OPERATOR");
				expectedRoleDesc.add("TEST_ROLE");
				expectedRoleDesc.add("UNDERWRITER");
				expectedRoleDesc.add("UNDERWRITING SUPERVISOR/MANAGER");

		
		for (int i = 0; i < resultSet.size(); i++){
			assertEquals(expectedRoleCode.get(i),(((RolesData)resultSet.get(i)).getRolesId()));
			assertEquals(expectedRoleDesc.get(i),(((RolesData)resultSet.get(i)).getRolesDesc()));
								}
	}

	public void testRetrieveRoleDetail() throws SQLException {
		Connection conn = new DataSourceProxy().getConnection();
		CodeValuePairDAO testDAO = new CodeValuePairDAO(conn);
		//Role Code exists in database
		RolesData resultSet = testDAO.retrieveRoleDetail("TEST");
		assertEquals("TEST_ROLE", resultSet.getRolesDesc());
		assertEquals("TEST", resultSet.getRolesId());
		//Role Code does not exist in database
		assertNull(testDAO.retrieveRoleDetail("mcg"));
		//mixed case / small case Doc Id
		assertNull(testDAO.retrieveRoleDetail("tEsT"));
	}

	public void testRetrieveAccessTemplates() throws SQLException {
		Connection conn = new DataSourceProxy().getConnection();
		CodeValuePairDAO testDAO = new CodeValuePairDAO(conn);
		ArrayList resultSet = testDAO.retrieveAccessTemplates();
		
			long[] expectedTempltId = new long[26];
			expectedTempltId[0]= 1;
			expectedTempltId[1]= 2;
			expectedTempltId[2]= 22;
			expectedTempltId[3]= 49;
			expectedTempltId[4]= 50;
			expectedTempltId[5]= 61;
			expectedTempltId[6]= 62;
			expectedTempltId[7]= 72;
			expectedTempltId[8]= 73;
			expectedTempltId[9]= 74;
			expectedTempltId[10]= 75;
			expectedTempltId[11]= 76;
			expectedTempltId[12]= 77;
			expectedTempltId[13]= 78;
			expectedTempltId[14]= 79;
			expectedTempltId[15]= 80;
			expectedTempltId[16]= 81;
			expectedTempltId[17]= 82;
			expectedTempltId[18]= 83;
			expectedTempltId[19]= 84;
			expectedTempltId[20]= 85;
			expectedTempltId[21]= 86;
			expectedTempltId[22]= 87;
			expectedTempltId[23]= 88;
			expectedTempltId[24]= 89;
			expectedTempltId[25]= 90;
		
			ArrayList expectedTempltDesc = new ArrayList();
			expectedTempltDesc.add("NB Staff Template");
			expectedTempltDesc.add("Security Admin Template");
			expectedTempltDesc.add("Underwriter Template");
			expectedTempltDesc.add("Template2");
			expectedTempltDesc.add("test");
			expectedTempltDesc.add("Facilitator");
			expectedTempltDesc.add(null);
			expectedTempltDesc.add("GROUP NB ADMINISTRATOR");
			expectedTempltDesc.add("NEW BUSINESS STAFF");
			expectedTempltDesc.add("NEW BUSINESS REVIEWER");
			expectedTempltDesc.add("NEW BUSINESS SUPERVISOR/M");
			expectedTempltDesc.add("FACILITATOR SUPERVISOR/MA");
			expectedTempltDesc.add("UNDERWRITING FACILITATOR");
			expectedTempltDesc.add("UNDERWRITER");
			expectedTempltDesc.add("USD SUPERVISOR/MANAGER");
			expectedTempltDesc.add("MEDICAL ADMIN STAFF");
			expectedTempltDesc.add("MEDICAL SUPERVISOR/MANAGE");
			expectedTempltDesc.add("MEDICAL CONSULTANT");
			expectedTempltDesc.add("MARKETING STAFF");
			expectedTempltDesc.add("AGENTS");
			expectedTempltDesc.add("DOCTOR/EXAMINER");
			expectedTempltDesc.add("LABORATORY STAFF");
			expectedTempltDesc.add("OTHER DEPARMENT USERS");
			expectedTempltDesc.add("LOV ADMINISTRATOR");
			expectedTempltDesc.add("SECURITY ADMIN");
			expectedTempltDesc.add("SYSTEM OPERATOR");
			
		
			ArrayList expectedRolesId = new ArrayList();
			expectedRolesId.add("NBSTAFF");
			expectedRolesId.add("SCTYADMIN");
			expectedRolesId.add("UNDERWRITER");
			expectedRolesId.add("A1");
			expectedRolesId.add("AA");
			expectedRolesId.add("FACILITATOR");
			expectedRolesId.add("TEST");
			expectedRolesId.add("GRPNBADMIN");
			expectedRolesId.add("NBSTAFF");
			expectedRolesId.add("NBREVIEWER");
			expectedRolesId.add("NBSUPMGR");
			expectedRolesId.add("FACILITSUPMGR");
			expectedRolesId.add("FACILITATOR");
			expectedRolesId.add("UNDERWRITER");
			expectedRolesId.add("UWTNGSUPMGR");
			expectedRolesId.add("MEDICALADMIN");
			expectedRolesId.add("MEDSUPMGR");
			expectedRolesId.add("MEDCONSLT");
			expectedRolesId.add("MKTGSTAFF");
			expectedRolesId.add("AGENT");
			expectedRolesId.add("EXAMINER");
			expectedRolesId.add("LABSTAFF");
			expectedRolesId.add("OTHERUSER");
			expectedRolesId.add("LOVADMIN");
			expectedRolesId.add("SCTYADMIN");
			expectedRolesId.add("SYSOPER");
		
		for (int i = 0; i < resultSet.size(); i++){
			assertEquals(expectedTempltId[i],(((AccessTemplateData)resultSet.get(i)).getTemplateID()));
			assertEquals(expectedTempltDesc.get(i),(((AccessTemplateData)resultSet.get(i)).getTemplateDesc()));
			RolesData expectedRoleId = ((AccessTemplateData)resultSet.get(i)).getRole();
			assertEquals(expectedRolesId.get(i),expectedRoleId.getRolesId());
		}
		
	}

	public void testRetrieveAccessTemplate() throws SQLException {
		Connection conn = new DataSourceProxy().getConnection();
		CodeValuePairDAO testDAO = new CodeValuePairDAO(conn);
	//Template Id exists in database
		AccessTemplateData resultSet = testDAO.retrieveAccessTemplate(50);
		RolesData expectedRole =  resultSet.getRole();
		assertEquals("test", resultSet.getTemplateDesc());
		assertEquals("AA", expectedRole.getRolesId());
		assertEquals(50, resultSet.getTemplateID());
	//Role Code does not exist in database
		assertNull(testDAO.retrieveAccessTemplate(91));
				
	}

	
}
