/*
 * Created on Jun 24, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.data.dao.test;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import junit.framework.TestCase;

import com.slocpi.ium.data.ExaminationPlaceData;
import com.slocpi.ium.data.dao.ExamPlaceDAO;
import com.slocpi.ium.data.util.DataSourceProxy;

/**
 * @author Jov Tuplano
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class ExamPlaceDAOTest extends TestCase {

	/**
	 * Constructor for ExamPlaceDAOTest.
	 * @param arg0
	 */
	public ExamPlaceDAOTest(String arg0) {
		super(arg0);
	}

	public static void main(String[] args) {
	}

	/*
	 * @see TestCase#setUp()
	 */
	protected void setUp() throws Exception {
		super.setUp();
	}

	/*
	 * @see TestCase#tearDown()
	 */
	protected void tearDown() throws Exception {
		super.tearDown();
	}

	

	final public void testSelectExamPlaces() throws SQLException {
		//TODO Implement selectExamPlaces().
		Connection conn= new DataSourceProxy().getConnection();
		ExamPlaceDAO exam= new ExamPlaceDAO(conn);
		
		
		ArrayList places= exam.selectExamPlaces();
		long id= ((ExaminationPlaceData)places.get(0)).getExaminationPlaceId();
		String desc=((ExaminationPlaceData)places.get(0)).getExaminationPlaceDesc();
		//places.add(data);
		assertEquals(3, id);
		assertEquals("PEDIATRICIAN ROOM", desc);
		assertNotNull(places);
		
		
		
		
		
	}

}
