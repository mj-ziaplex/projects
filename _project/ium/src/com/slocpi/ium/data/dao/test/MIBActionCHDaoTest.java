/*
 * Created on Jun 24, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.data.dao.test;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import junit.framework.TestCase;

import com.slocpi.ium.data.dao.MIBActionCHDao;
import com.slocpi.ium.data.util.DataSourceProxy;
import com.slocpi.ium.util.NameValuePair;

/**
 * @author Jeffrey Canlas
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class MIBActionCHDaoTest extends TestCase {
	public Connection conn = null;
	
	public MIBActionCHDaoTest(String arg0) {
		super(arg0);
	}

	public void testMIBActionCHDAO() throws SQLException {
	
	  /*TEST FOR public Collection getCodeValues()
		* Test connection and see whether SQL statement 
		* produced a result.  the entered sql statement
		* is valid (tables and fields are existing)
		* thus resulting arraylist should be 
		* greater than zero. 
	   */
	
	   conn = new DataSourceProxy().getConnection();
	   ArrayList list = new ArrayList();
	   MIBActionCHDao dao = new MIBActionCHDao(conn);
	    
	   list = (ArrayList)dao.getCodeValues();
			
	   assertTrue("test 1", list.size()>0);
	    
	   /*TEST FOR public Collection getCodeValues()
		* test if the data in the dataset are the
		* same as the one in the database. 
		* Will check the first result and see 
		* if it is the same from the database
	   */	
	
	   String desc = ((NameValuePair)list.get(5)).getName();
	   String code = ((NameValuePair)list.get(5)).getValue();
	   System.out.println(desc);
	   System.out.println(code);
	   assertTrue("test 1.1",desc.equals("Cancelled"));
	   assertTrue("test1.2",code.equals("C"));
		
	   /*TEST FOR public Collection getCodeValue(String codeValue)
		* Test connection and see whether SQL statement 
		* produced a result.  the entered sql statement
		* is valid (tables and fields are existing)
		* thus resulting arraylist should be 
		* greater than zero. 
	   */
		
	   ArrayList list2 = new ArrayList();
	   MIBActionCHDao dao2 = new MIBActionCHDao(conn);
	    
	   list2 = (ArrayList)dao2.getCodeValue("D");
			
	   assertTrue("test 2", list2.size()>0);
	
	
	   /*TEST FOR public Collection getCodeValue(String codeValue))
		* test if the data in the dataset are the
		* same as the one in the database. 
		* Will check the result where MIB_ACT_CODE = "D" 
		* and see whether it is the same in the database
	   */		
	
	   String desc2 = ((NameValuePair)list2.get(0)).getName();
	   String code2 = ((NameValuePair)list2.get(0)).getValue();
	   System.out.println(desc2);
	   System.out.println(code2);
	   assertTrue("test 2.1",desc2.equals("Declined"));
	   assertTrue("test 2.2",code2.equals("D"));
		
	}

}
