/*
 * Created on Feb 14, 2004
 * package name = com.slocpi.ium.data.dao
 * file name    = RequirementCodeCHDao.java
 */
package com.slocpi.ium.data.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.NameValuePair;



/**
 * TODO Class Description of RequirementCodeCHDao.java
 * @author Engel
 * 
 */
public class RequirementCodeCHDao extends CodeHelperDao {

	private static final Logger LOGGER = LoggerFactory.getLogger(RequirementCodeCHDao.class);
	private Connection conn;

	public RequirementCodeCHDao(Connection conn) throws SQLException {
		this.conn = conn;
	}// RequirementStatusCHDao

	
	/**
	 * This method will return a collection of all the client types.
	 */
	public Collection getCodeValues() throws SQLException {
	  
		LOGGER.info("getCodeValues start");
	   Collection list = new ArrayList();
	  String sql = "SELECT REQT_CODE AS CODE, REQT_DESC AS DESCRIPTION FROM REQUIREMENTS ORDER BY REQT_CODE";
	  PreparedStatement ps = null;
	  ResultSet rs = null;
	  try{	  	
		
		ps = conn.prepareStatement(sql);
		rs = ps.executeQuery();				    		
		while (rs.next()) {
		  NameValuePair bean = new NameValuePair();
		  bean.setName(rs.getString("DESCRIPTION"));
		  bean.setValue(rs.getString("CODE"));
		  list.add(bean);
		}
	   
	  } catch (SQLException e) {
		  LOGGER.error(CodeHelper.getStackTrace(e));
		  throw e; 
	  }finally{
		  this.closeResources(ps, rs);
	  }	
	  LOGGER.info("getCodeValues end");
	  return (list);
	}// getCodeValues


	/**
	 * This method will return a subset of client types based on the specified client type code.
	 */
	public Collection getCodeValue(String codeValue) throws SQLException {
		
		LOGGER.info("getCodeValue start");
		Collection list = new ArrayList();
		String sql = "SELECT REQT_CODE AS CODE, REQT_DESC AS DESCRIPTION FROM REQUIREMENTS WHERE REQT_CODE=?";
		PreparedStatement ps = null;
		ResultSet rs = null;
		try{
		
		  ps = conn.prepareStatement(sql);
		  ps.setString(1, codeValue);
		  rs = ps.executeQuery();				    		
		  while (rs.next()) {
			NameValuePair bean = new NameValuePair();
			bean.setName(rs.getString("DESCRIPTION"));
			bean.setValue(rs.getString("CODE"));
			list.add(bean);
		  }
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			  throw e; 
		}finally{
			  this.closeResources(ps, rs);
		}	
		LOGGER.info("getCodeValue end");
		return (list);
	}// getCodeValue



}
