/*
 * Created on Jan 6, 2004
 * package name = com.slocpi.ium.data.dao
 * file name    = AssessmentRequestDAO.java
 */
package com.slocpi.ium.data.dao;


import java.io.IOException;
import java.io.StringReader;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ph.com.sunlife.wms.modules.DateUtil;

import com.slocpi.ium.data.ApplicationsApprovedFilterData;
import com.slocpi.ium.data.ApplicationsByStatusData;
import com.slocpi.ium.data.ApplicationsByStatusFilterData;
import com.slocpi.ium.data.AssessmentRequestData;
import com.slocpi.ium.data.CDSMortalityRatingData;
import com.slocpi.ium.data.CDSSummaryPolicyCoverageData;
import com.slocpi.ium.data.ClientData;
import com.slocpi.ium.data.ClientDataSheetData;
import com.slocpi.ium.data.LOBData;
import com.slocpi.ium.data.RequestFilterData;
import com.slocpi.ium.data.StatusData;
import com.slocpi.ium.data.SunLifeOfficeData;
import com.slocpi.ium.data.UWAssessmentData;
import com.slocpi.ium.data.UserProfileData;
import com.slocpi.ium.underwriter.UnderWriterException;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.DateHelper;
import com.slocpi.ium.util.IUMConstants;
import com.slocpi.ium.util.SortHelper;
import com.slocpi.ium.util.ValueConverter;

/**
 * Data Access Object of the Assessment Request. Access the ASSESSMENT_REQUESTS
 * table.
 * 
 * @author Engel
 * 
 */
public class AssessmentRequestDAO extends BaseDAO{

	static final Logger LOGGER = LoggerFactory.getLogger(AssessmentRequestDAO.class);

	/**
	 * 
	 * TODO method description getRequestList
	 * 
	 * @param requestFilter
	 * @return
	 */
	

	public AssessmentRequestDAO() {}
	
	private String mapper(AssessmentRequestData request, String field)
			throws UnderWriterException {
		
		LOGGER.info("mapper start");

		String value = null;

		if (field != null) {
			if (field.equalsIgnoreCase(IUMConstants.CRITERIA_BRANCH)) {
				value = (request.getBranch() != null && (request.getBranch())
						.getOfficeId() != null) ? (request.getBranch())
						.getOfficeId() : null;
			} else if (field.equalsIgnoreCase(IUMConstants.CRITERIA_AGENT)) {
				value = (request.getAgent() != null && (request.getAgent())
						.getUserId() != null) ? (request.getAgent())
						.getUserId() : null;
			} else if (field
					.equalsIgnoreCase(IUMConstants.CRITERIA_ASSIGNED_TO)) {
				value = (request.getAssignedTo() != null && (request
						.getAssignedTo()).getUserId() != null) ? (request
						.getAssignedTo()).getUserId() : null;
			} else if (field.equalsIgnoreCase(IUMConstants.CRITERIA_LOB)) {
				value = (request.getLob() != null && (request.getLob())
						.getLOBCode() != null) ? (request.getLob())
						.getLOBCode() : null;
			} else {
				throw new UnderWriterException(
						"Unknown Auto-Assignment Criteria: " + field);
				
			}
		}
		LOGGER.info("mapper end");
		return value;
	}

	// edited by Aldrich Abrogena Jan 16,2008
	public String getAutoAssignUW(AssessmentRequestData request)
			throws SQLException, UnderWriterException {
		
		LOGGER.info("getAutoAssignUW start");
		StringBuffer aSQL = new StringBuffer();
		aSQL
				.append("SELECT AAC_ID, AAC_FIELD, AAC_DESC, AAC_STATUS FROM AUTO_ASSIGNMENT_CRITERIA ");
		aSQL.append(" where aac_status=?");
		PreparedStatement ps = null;
		ResultSet rs = null;
		PreparedStatement ps2 = null;
		ResultSet rs2 = null;
		String user = null;
		Connection conn = null;
		
		try {
			conn = getConnection();
			ps = conn.prepareStatement(aSQL.toString());

			ps.setBoolean(1, true);
			rs = ps.executeQuery();
			ArrayList values = new ArrayList();
			while (rs.next()) {
				String field = rs.getString("AAC_FIELD");
				String value = mapper(request, field);
				values.add(value);
			}
			if (values.size() < 1) {
				throw new UnderWriterException(
						"No Auto-Assignment Criteria is set to Active");
			}
			
			StringBuffer sql = new StringBuffer();

			sql.append("SELECT");
			sql.append(" USER_ID as USER_ID");
			sql.append(" FROM");
			sql.append(" AUTO_ASSIGNMENT_CRITERIA A , AUTO_ASSIGNMENTS B");
			sql.append(" WHERE");
			sql.append(" A.AAC_STATUS = ? AND	A.AAC_ID= B.AAC_ID AND");
			sql.append(" CRITERIA_FIELD_VALUE in(");
			for (int i = 0; i < values.size(); i++) {
				sql.append("?");
				if (i < (values.size() - 1)) {
					sql.append(",");
				}
			}
			sql.append(" )");
			sql.append(" having count(USER_ID)= ?");
			sql.append(" GROUP BY USER_ID");

			ps2 = conn.prepareStatement(sql.toString());
			int ctr = 1;
			ps2.setBoolean(ctr, true);
			ctr++;
			for (int i = 0; i < values.size(); i++, ctr++) {
				ps2.setString(ctr, (String) values.get(i));
			}
			ps2.setInt(ctr, values.size());
			rs2 = ps2.executeQuery();

			while (rs2.next()) {
				user = rs2.getString("USER_ID");
			}
			if (user == null) {
				throw new UnderWriterException(
						"No UnderWriter retrieved!  Please check Auto-Assignment Configuration");
			}
			
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		} finally {
			closeResources(ps, rs);
			closeResources(ps2, rs2);
			closeConnection(conn);
		}
		LOGGER.info("getAutoAssignUW end");
		return user;
	}

	// edited by Aldrich Abrogena Jan 16,2008
	public boolean isBranch(String branchCode) throws SQLException {
		
		LOGGER.info("isBranch start");
		boolean res = false;
		StringBuffer sb = new StringBuffer();
		sb
				.append("SELECT count(*) FROM SUNLIFE_OFFICES WHERE SLO_OFFICE_CODE = ?");
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection conn = null;
		
		try {
			conn = getConnection();
			ps = conn.prepareStatement(sb.toString());
			ps.setString(1, branchCode);
			rs = ps.executeQuery();
			if (rs.next()) {
				if (rs.getInt(1) > 0) {
					res = true;
				}
			}
			
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		} finally {
			closeResources(conn, ps, rs);
		}
		LOGGER.info("isBranch end");
		return res;
	}

	// edited by Aldrich Abrogena Jan 16,2008
	public boolean isLOB(String lobCode) throws SQLException {

		LOGGER.info("isLOB start");
		boolean res = false;
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT count(*) FROM LINES_OF_BUSINESS WHERE LOB_CODE = ?");
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection conn = null;
		
		try {
			conn = getConnection();
			ps = conn.prepareStatement(sb.toString());

			ps.setString(1, lobCode);
			rs = ps.executeQuery();
			if (rs.next()) {
				if (rs.getInt(1) > 0) {
					res = true;
				}
			}
			
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		} finally {
			closeResources(conn, ps, rs);
		}
		LOGGER.info("isLOB end");
		return res;
	}

	public boolean isClientExists(String clientNo) throws SQLException {
		
		LOGGER.info("isClientExists start 1");
		boolean res = false;
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT count(*) FROM CLIENTS WHERE CLIENT_ID = ?");
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection conn = null;
		
		try {
			conn = getConnection();
			ps = conn.prepareStatement(sb.toString());
			ps.setString(1, clientNo);
			rs = ps.executeQuery();
			if (rs.next()) {
				if (rs.getInt(1) > 0) {
					res = true;
				}
			}
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		} finally {
			closeResources(conn, ps, rs);
		}
		LOGGER.info("isClientExists end 1");
		return res;

	}

	// edited by Aldrich Abrogena Jan 16,2008
	public boolean isClientExists(String clientNo, String refNo)
			throws SQLException {
		
		LOGGER.info("isClientExists start 2");
		boolean res = false;
		StringBuffer sb = new StringBuffer();
		sb
				.append("SELECT count(*) FROM ASSESSMENT_REQUESTS WHERE REFERENCE_NUM = ? AND (INSURED_CLIENT_ID = ? OR OWNER_CLIENT_ID = ?) ");
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection conn = null;
		
		try {
			conn = getConnection();
			ps = conn.prepareStatement(sb.toString());
			ps.setString(1, refNo);
			ps.setString(2, clientNo);
			ps.setString(3, clientNo);
			rs = ps.executeQuery();
			if (rs.next()) {
				if (rs.getInt(1) > 0) {
					res = true;
				}
			}
			
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		} finally {
			closeResources(conn, ps, rs);
		}
		LOGGER.info("isClientExists end 2");
		return res;
	}

	// edited by Aldrich Abrogena - Jan 16,2008
	public boolean isUser(String userId) throws SQLException {
		
		LOGGER.info("isUser start");
		boolean res = false;
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT count(*) FROM USERS WHERE USER_ID = ?");
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection conn = null;
		
		try {
			conn = getConnection();
			ps = conn.prepareStatement(sb.toString());
			ps.setString(1, userId);
			rs = ps.executeQuery();
			if (rs.next()) {
				if (rs.getInt(1) > 0) {
					res = true;
				}
			}
			
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		} finally {
			closeResources(conn, ps, rs);
		}
		LOGGER.info("isUser end");
		
		return res;

	}

	public boolean isPolicyExists(String referenceNumber) throws SQLException {
		
		LOGGER.info("isPolicyExists start");
		boolean res = false;
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT count(*) FROM assessment_requests WHERE REFERENCE_NUM = ?");
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection conn = null;
		
		try {
			conn = getConnection();
			ps = conn.prepareStatement(sb.toString());
			ps.setString(1, referenceNumber);
			rs = ps.executeQuery();
			if (rs.next()) {
				if (rs.getInt(1) > 0) {
					res = true;
				}
			}
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		} finally {
			closeResources(conn, ps, rs);
		}
		
		LOGGER.info("isPolicyExists end");
		return res;
	}

	// edited by Aldrich Abrogena - Jan 16,2008
	public ArrayList getRequestList(RequestFilterData requestFilter)
			throws SQLException {
		
		LOGGER.info("getRequestList start");
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection conn = null;
		
		
		
		ArrayList data = null;
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT");
		/**
		 * **********[start] do not change the order of the ff. columns
		 * *********
		 */
		sql.append(" DISTINCT(REFERENCE_NUM) as REFERENCE_NUM,"); // 1
		sql.append(" AR.POLICY_SUFFIX as POLICY_SUFFIX, "); // 19
		sql.append(" LB.LOB_DESC as LOB_DESC,"); // 2
		sql.append(" AR.STATUS_ID as STATUS_ID,"); // 3
		sql.append(" INSURED.CL_LAST_NAME as INSURED_LAST_NAME,"); // 4
		sql.append(" INSURED.CL_GIVEN_NAME as INSURED_GIVEN_NAME,"); // 5
		sql.append(" INSURED.CL_MIDDLE_NAME as INSURED_MIDDLE_NAME,"); // 6
		sql.append(" AR.AMOUNT_COVERED as AMOUNT_COVERED,"); // 7
		sql.append(" ASSIGN.USR_LAST_NAME as ASSIGNED_TO_LAST_NAME,"); // 8
		sql.append(" ASSIGN.USR_FIRST_NAME AS ASSIGNED_TO_FIRST_NAME,"); // 9
		sql.append(" ASSIGN.USR_MIDDLE_NAME AS ASSIGNED_TO_MIDDLE_NAME,"); // 10
		sql.append(" BR.SLO_OFFICE_NAME as BRANCH_NAME,"); // 11
		sql.append(" AGNT.USR_LAST_NAME as AGENT_LAST_NAME,"); // 12
		sql.append(" AGNT.USR_FIRST_NAME as AGENT_FIRST_NAME,"); // 13
		sql.append(" AGNT.USR_MIDDLE_NAME as AGENT_MIDDLE_NAME,"); // 14
		sql.append(" AR.APP_RECEIVED_DATE as RECEIVED_DATE,"); // 15
		sql.append(" LOC.USR_LAST_NAME as LOC_LAST_NAME, "); // 16
		sql.append(" LOC.USR_FIRST_NAME AS LOC_FIRST_NAME,"); // 17
		sql.append(" LOC.USR_MIDDLE_NAME as LOC_MIDDLE_NAME, "); // 18
		/** **********[end] do not change the order of the ff. columns ********* */
		sql.append(" AR.BRANCH_ID as BRANCH_CODE,"); // 12
		sql.append(" AR.AGENT_ID as AGENT_ID,");
		sql.append(" OWNER.CL_GIVEN_NAME as OWNER_GIVEN_NAME,");
		sql.append(" OWNER.CL_LAST_NAME as OWNER_LAST_NAME,");
		sql.append(" AR.OWNER_CLIENT_ID as OWNER_ID,");
		sql.append(" AR.INSURED_CLIENT_ID as INSURED_ID,");
		sql.append(" AR.PREMIUM as PREMIUM, ");
		sql.append(" ST.STAT_DESC as STATUS_DESC,");
		sql.append(" AR.STATUS_DATE as STATUS_DATE,");
		sql.append(" AR.ASSIGNED_TO as ASSIGNED_TO_CODE,");
		sql.append(" AR.UNDERWRITER as UW_CODE,");
		sql.append(" UW.USR_FIRST_NAME AS UW_FIRST_NAME,");
		sql.append(" UW.USR_LAST_NAME as UW_LAST_NAME,");
		sql.append(" AR.FOLDER_LOCATION as LOC_CODE,");
		// sql.append(" AR.REMARKS as REMARKS,");

		sql.append(" AR.DATE_FORWARDED as DATE_FORWARDED,");
		sql.append(" AR.CREATED_BY as CREATED_BY_CODE,");
		sql.append(" CREATED_BY.USR_FIRST_NAME AS CREATED_BY_FIRST_NAME,");
		sql.append(" CREATED_BY.USR_LAST_NAME as CREATED_BY_LAST_NAME,");
		sql.append(" AR.CREATED_DATE as CREATED_DATE,");
		sql.append(" AR.UPDATED_BY as UPDATED_BY_CODE,");
		sql.append(" UPDATED_BY.USR_FIRST_NAME AS UPDATED_BY_FIRST_NAME,");
		sql.append(" UPDATED_BY.USR_LAST_NAME as UPDATED_BY_LAST_NAME,");
		sql.append(" AR.UPDATED_DATE as UPDATED_DATE,");
		sql.append(" AR.LOB as LOB_CODE,");
		sql.append(" AR.SOURCE_SYSTEM as SOURCE_SYSTEM, ");
		// sql.append(" POL. as UPDATED_DATE,");
		sql.append(" AR.KO_COUNT_IND as KO_COUNT_IND, ");
		sql.append(" AR.REQT_COUNT_IND as REQT_COUNT_IND ");
		sql.append(" FROM");
		sql.append(" CLIENTS OWNER,");
		sql.append(" CLIENTS INSURED,");
		sql.append(" ASSESSMENT_REQUESTS AR,");
		sql.append(" LINES_OF_BUSINESS LB,");
		sql.append(" SUNLIFE_OFFICES BR,");
		sql.append(" USERS AGNT,");
		sql.append(" STATUS ST,");
		sql.append(" USERS ASSIGN,");
		sql.append(" USERS UW,");
		sql.append(" USERS LOC,");
		sql.append(" USERS CREATED_BY,");
		sql.append(" USERS UPDATED_BY,");

		sql.append(" POLICY_REQUIREMENTS POL");
		sql.append(" WHERE");
		sql.append(" AR.OWNER_CLIENT_ID=OWNER.CLIENT_ID (+) AND");
		sql.append(" AR.INSURED_CLIENT_ID=INSURED.CLIENT_ID (+) AND");
		sql.append(" AR.LOB = LB.LOB_CODE (+) AND ");
		sql.append(" AR.BRANCH_ID = BR.SLO_OFFICE_CODE (+) AND");
		sql.append(" AR.AGENT_ID = AGNT.USER_ID (+) AND");
		sql.append(" AR.STATUS_ID = ST.STAT_ID (+) AND");
		sql.append(" AR.ASSIGNED_TO = ASSIGN.USER_ID (+) AND");
		sql.append(" AR.UNDERWRITER = UW.USER_ID (+) AND");
		sql.append(" AR.FOLDER_LOCATION = LOC.USER_ID (+) AND");
		sql.append(" AR.CREATED_BY = CREATED_BY.USER_ID (+) AND");
		sql.append(" AR.UPDATED_BY = UPDATED_BY.USER_ID (+) AND");
		sql.append(" AR.REFERENCE_NUM = POL.UAR_REFERENCE_NUM (+) AND");

		// ASSIGNED_TO
		if ((requestFilter.getAssignedTo() != null && !requestFilter
				.getAssignedTo().equals(""))) {
			sql.append(" AR.ASSIGNED_TO=? AND");
		}
		// AGENT
		if ((requestFilter.getAgent() != null && !requestFilter.getAgent()
				.equals(""))) {
			sql.append(" AR.AGENT_ID=? AND");
		}
		// LOCATION
		if ((requestFilter.getLocation() != null && !requestFilter
				.getLocation().equals(""))) {
			sql.append(" AR.FOLDER_LOCATION=? AND");
		}

		// STATUS
		if ((requestFilter.getStatus() != null && !requestFilter.getStatus()
				.equals(""))) {
			sql.append(" AR.STATUS_ID=? AND");
		}
		// REFERENCE NO.
		if ((requestFilter.getReferenceNumber() != null && !requestFilter
				.getReferenceNumber().equals(""))) {
			sql.append(" upper(AR.REFERENCE_NUM) like upper(?) AND");
		}
		// LOB
		if ((requestFilter.getLob() != null && !requestFilter.getLob().equals(
				""))) {
			sql.append(" AR.LOB=? AND");
		}
		// CLIENT NO
		if ((requestFilter.getClientNumber() != null && !requestFilter
				.getClientNumber().equals(""))) {
			sql
					.append(" ( upper(AR.INSURED_CLIENT_ID) like upper(?) OR upper(AR.OWNER_CLIENT_ID) like upper(?) ) AND");
		}
		// name of insured
		if ((requestFilter.getNameOfInsured() != null && !requestFilter
				.getNameOfInsured().equals(""))) {
			sql
					.append(" (upper(INSURED.CL_GIVEN_NAME) LIKE upper(?) OR upper(INSURED.CL_LAST_NAME) LIKE upper(?)) AND");

		}

		// COVERAGE AMOUNT
		if (requestFilter.getCoverageAmount() > 0d) {
			sql.append(" AR.AMOUNT_COVERED >=? AND");

		}

		// ApplicationReceivedDate
		if ((requestFilter.getApplicationReceivedDate() != null && !requestFilter
				.getApplicationReceivedDate().equals(""))) {
			sql.append(" (AR.APP_RECEIVED_DATE=?) AND");
		}

		// REQUIREMENT CODE
		if ((requestFilter.getRequirementCode() != null && !requestFilter
				.getRequirementCode().equals(""))) {
			sql.append(" (POL.PR_REQT_CODE=?) AND");
		}

		// BRANCH
		if (!(requestFilter.getBranch() != null && !requestFilter.getBranch()
				.equals(""))) {
			sql.append(" AR.BRANCH_ID LIKE '%'");
		} else {
			sql.append(" AR.BRANCH_ID=?");
		
		}
		try {
			SortHelper sorthelp = new SortHelper(
					IUMConstants.TABLE_ASSESSMENT_REQUESTS);
			sorthelp.setSortOrder(requestFilter.getSortOrder());
			sorthelp.setColumn(requestFilter.getColumns());
			sql.append(sorthelp.getOrderBy());

			int ctr = 1;
			// debug(sql.toString());
			conn = getConnection();
			ps = conn.prepareStatement(sql.toString());
			// AssignedTo
			if ((requestFilter.getAssignedTo() != null && !requestFilter
					.getAssignedTo().equals(""))) {
				ps.setString(ctr, requestFilter.getAssignedTo());
				ctr++;
			}
			// Agent
			if ((requestFilter.getAgent() != null && !requestFilter.getAgent()
					.equals(""))) {
				ps.setString(ctr, requestFilter.getAgent());
				ctr++;
			}
			// Location
			if ((requestFilter.getLocation() != null && !requestFilter
					.getLocation().equals(""))) {
				ps.setString(ctr, requestFilter.getLocation());
				ctr++;
			}

			// Status
			if ((requestFilter.getStatus() != null && !requestFilter
					.getStatus().equals(""))) {
				ps.setString(ctr, requestFilter.getStatus());
				ctr++;
			}
			// REFERENCE NO.
			if ((requestFilter.getReferenceNumber() != null && !requestFilter
					.getReferenceNumber().equals(""))) {
				ps.setString(ctr, "%" + requestFilter.getReferenceNumber()
						+ "%");
				ctr++;
			}
			// LOB
			if ((requestFilter.getLob() != null && !requestFilter.getLob()
					.equals(""))) {
				ps.setString(ctr, requestFilter.getLob());
				ctr++;
			}
			// CLIENT_NUMBER
			if ((requestFilter.getClientNumber() != null && !requestFilter
					.getClientNumber().equals(""))) {
				ps.setString(ctr, "%" + requestFilter.getClientNumber() + "%");
				ctr++;
				ps.setString(ctr, "%" + requestFilter.getClientNumber() + "%");
				ctr++;

			}
			// NAME OF INSURED
			if ((requestFilter.getNameOfInsured() != null && !requestFilter
					.getNameOfInsured().equals(""))) {
				ps.setString(ctr, "%" + requestFilter.getNameOfInsured() + "%");
				ctr++;
				ps.setString(ctr, "%" + requestFilter.getNameOfInsured() + "%");
				ctr++;

			}

			// COVERAGE AMOUNT
			if (requestFilter.getCoverageAmount() > 0d) {
				ps.setDouble(ctr, requestFilter.getCoverageAmount());
				ctr++;
			}

			// ApplicationReceivedDate
			if ((requestFilter.getApplicationReceivedDate() != null && !requestFilter
					.getApplicationReceivedDate().equals(""))) {
				ps.setDate(ctr, DateHelper.sqlDate(requestFilter
						.getApplicationReceivedDate()));
				ctr++;
			}

			// REQUIREMENT CODE
			if ((requestFilter.getRequirementCode() != null && !requestFilter
					.getRequirementCode().equals(""))) {
				ps.setString(ctr, requestFilter.getRequirementCode());
				ctr++;
			}

			// Branch
			if (requestFilter.getBranch() != null
					&& !requestFilter.getBranch().equals("")) {
				ps.setString(ctr, requestFilter.getBranch());
				ctr++;
			}

			rs = ps.executeQuery();
		

			data = new ArrayList();
			while (rs.next()) {
				AssessmentRequestData assessment = new AssessmentRequestData();

				assessment.setReferenceNumber(rs.getString("REFERENCE_NUM"));
				assessment.setPolicySuffix(rs.getString("POLICY_SUFFIX"));
				LOBData lob = new LOBData();
				lob.setLOBCode(rs.getString("LOB_CODE"));
				lob.setLOBDesc(rs.getString("LOB_DESC"));
				assessment.setLob(lob);
				assessment.setSourceSystem(rs.getString("SOURCE_SYSTEM"));
				assessment.setAmountCovered(rs.getDouble("AMOUNT_COVERED"));
				assessment.setPremium(rs.getDouble("PREMIUM"));
				ClientData insured = new ClientData();
				insured.setClientId(rs.getString("INSURED_ID"));
				insured.setGivenName(rs.getString("INSURED_GIVEN_NAME"));
				insured.setLastName(rs.getString("INSURED_LAST_NAME"));
				assessment.setInsured(insured);
				ClientData owner = new ClientData();
				owner.setClientId(rs.getString("OWNER_ID"));
				owner.setGivenName(rs.getString("OWNER_GIVEN_NAME"));
				owner.setLastName(rs.getString("OWNER_LAST_NAME"));
				assessment.setOwner(owner);
				SunLifeOfficeData branch = new SunLifeOfficeData();
				branch.setOfficeId(rs.getString("BRANCH_CODE"));
				branch.setOfficeName(rs.getString("BRANCH_NAME"));
				assessment.setBranch(branch);
				UserProfileData agent = new UserProfileData();
				agent.setUserId(rs.getString("AGENT_ID"));
				agent.setFirstName(rs.getString("AGENT_FIRST_NAME"));
				agent.setLastName(rs.getString("AGENT_LAST_NAME"));
				assessment.setAgent(agent);
				StatusData status = new StatusData();
				status.setStatusId(Long.parseLong(rs.getString("STATUS_ID")));
				status.setStatusDesc(rs.getString("STATUS_DESC"));
				assessment.setStatus(status);
				assessment.setStatusDate(DateHelper.utilDate(rs
						.getDate("STATUS_DATE")));
				UserProfileData assignedTo = new UserProfileData();
				assignedTo.setUserId(rs.getString("ASSIGNED_TO_CODE"));
				assignedTo.setFirstName(rs.getString("ASSIGNED_TO_FIRST_NAME"));
				assignedTo.setLastName(rs.getString("ASSIGNED_TO_LAST_NAME"));
				assessment.setAssignedTo(assignedTo);
				UserProfileData uw = new UserProfileData();
				uw.setUserId(rs.getString("UW_CODE"));
				uw.setFirstName(rs.getString("UW_FIRST_NAME"));
				uw.setLastName(rs.getString("UW_LAST_NAME"));
				assessment.setUnderwriter(uw);
				UserProfileData location = new UserProfileData();
				location.setUserId(rs.getString("LOC_CODE"));
				location.setFirstName(rs.getString("LOC_FIRST_NAME"));
				location.setLastName(rs.getString("LOC_LAST_NAME"));
				assessment.setFolderLocation(location);
				assessment.setApplicationReceivedDate(DateHelper.utilDate(rs
						.getDate("RECEIVED_DATE")));
				assessment.setForwardedDate(DateHelper.utilDate(rs
						.getDate("DATE_FORWARDED")));
				UserProfileData createdBy = new UserProfileData();
				createdBy.setUserId(rs.getString("CREATED_BY_CODE"));
				createdBy.setFirstName(rs.getString("CREATED_BY_FIRST_NAME"));
				createdBy.setLastName(rs.getString("CREATED_BY_LAST_NAME"));
				assessment.setCreatedBy(createdBy);
				assessment.setCreatedDate(rs.getTimestamp("CREATED_DATE"));
				UserProfileData updatedBy = new UserProfileData();
				updatedBy.setUserId(rs.getString("UPDATED_BY_CODE"));
				updatedBy.setFirstName(rs.getString("UPDATED_BY_FIRST_NAME"));
				updatedBy.setLastName(rs.getString("UPDATED_BY_LAST_NAME"));
				assessment.setUpdatedBy(updatedBy);
				assessment.setUpdatedDate(rs.getTimestamp("UPDATED_DATE"));
				assessment.setKoCountInd(rs.getString("KO_COUNT_IND"));
				assessment.setReqtCountInd(rs.getString("REQT_COUNT_IND"));
				data.add(assessment);
			}
			
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		} finally {
			closeResources(conn, ps, rs);
		}
		LOGGER.info("getRequestList end");
		return data;

	}

	// edited by Aldrich Abrogena - Jan 16,2008
	public int selectNoOfPendingAssessmentRequest(String userId)
			throws SQLException {
		
		LOGGER.info("selectNoOfPendingAssessmentRequest start");
		int res = 0;
		StringBuffer sb = new StringBuffer();
		sb
				.append("SELECT count(*) FROM assessment_requests WHERE assigned_to = ?");
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection conn = null;
		
		try{
			conn = getConnection();
			ps = conn.prepareStatement(sb.toString());
			ps.setString(1, userId);
			rs = ps.executeQuery();
			if (rs.next()) {
				res = rs.getInt(1);
			}
			
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;			
		} finally {
			closeResources(conn, ps, rs);
		}
		LOGGER.info("selectNoOfPendingAssessmentRequest end");
		return res;
	}

	/**
	 * Change the assessment request status
	 * 
	 * @param referenceNumber
	 *            assessment request reference number
	 * @param status
	 *            new status for this assessment request
	 * @param updatedBy
	 *            user that updated the assessment request
	 * @author cris
	 */
	// edited by Aldrich Abrogena - Jan 16,2008
	public void changeARStatus(String referenceNumber, long status,
			String updatedBy) throws SQLException {

		LOGGER.info("changeARStatus start");
		String sql = "UPDATE ASSESSMENT_REQUESTS SET STATUS_ID = ?,STATUS_DATE = ?,UPDATED_BY = ?,UPDATED_DATE = ?"
				+ "WHERE REFERENCE_NUM = ?";

		PreparedStatement stmt = null;
		Connection conn = null;
		try {
			conn = getConnection();
			conn.setAutoCommit(false);
			stmt = conn.prepareStatement(sql);
			stmt.setLong(1, status);
			stmt.setDate(2, DateHelper.sqlDate(new java.util.Date()));
			stmt.setString(3, updatedBy);
			stmt.setTimestamp(4, DateHelper.sqlTimestamp(new java.util.Date()));
			stmt.setString(5, referenceNumber);
			stmt.executeUpdate();
			conn.commit();
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			if(conn != null){
					conn.rollback();
			}
			throw e;
		} finally {
			closeResources(conn,stmt, null);
		}
		LOGGER.info("changeARStatus end");
	}

	/**
	 * Change the assessment request assign to field
	 * 
	 * @param referenceNumber
	 *            assessment request reference number
	 * @param assignedTo
	 *            user whom the assessment request will be assigned
	 * @param updatedBy
	 *            user that updated the assessment request
	 * @author cris
	 */
	// Edited by Aldrich Abrogena - Jan 16,2008
	public void changeAssignedTo(String referenceNumber, String assignedTo,
			String updatedBy) throws SQLException {
		
		LOGGER.info("changeAssignedTo start");
		String sql = "UPDATE ASSESSMENT_REQUESTS SET ASSIGNED_TO = ?, UPDATED_BY = ?, UPDATED_DATE = ? WHERE REFERENCE_NUM = ? ";
		PreparedStatement stmt = null;
		Connection conn = null;
		try {
			conn = getConnection();
			conn.setAutoCommit(false);
			stmt = conn.prepareStatement(sql);
			stmt.setString(1, assignedTo);
			stmt.setString(2, updatedBy);
			stmt.setTimestamp(3, DateHelper.sqlTimestamp(new java.util.Date()));
			stmt.setString(4, referenceNumber);
			stmt.executeUpdate();
			conn.commit();
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			if(conn != null){
				conn.rollback();
			}
			throw e;
		} finally {
			closeResources(conn, stmt, null);
		}
		LOGGER.info("changeAssignedTo end");
	}

	// edited by Aldrich Abrogena - Jan 16,2008
	public void reassignedTo(String referenceNumber, String assignedTo,
			String underwriter, String updatedBy) throws SQLException {
		
		LOGGER.info("reassignedTo start");
		String sql = "UPDATE ASSESSMENT_REQUESTS SET ASSIGNED_TO = ?,UNDERWRITER=?, UPDATED_BY = ?, UPDATED_DATE = ? WHERE REFERENCE_NUM = ? ";
		PreparedStatement stmt = null;
		Connection conn = null;
		try {
			conn = getConnection();
			conn.setAutoCommit(false);
			stmt = conn.prepareStatement(sql);
			stmt.setString(1, assignedTo);
			stmt.setString(2, underwriter);
			stmt.setString(3, updatedBy);
			stmt.setTimestamp(4, DateHelper.sqlTimestamp(new java.util.Date()));
			stmt.setString(5, referenceNumber);
			stmt.executeUpdate();
			conn.commit();
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			if(conn != null){
				conn.rollback();
			}
			throw e;
		} finally {
			closeResources(conn, stmt, null);
		}
		LOGGER.info("reassignedTo end");
	}

	/**
	 * Change the assessment request assign to field
	 * 
	 * @param referenceNum
	 * @param assignedTo
	 * @param updatedBy
	 * @author cris
	 */

	public void updateLocation(String referenceNumber, String location,
			String updatedBy) throws SQLException {
		
		LOGGER.info("updateLocation start");

		String sql = "UPDATE ASSESSMENT_REQUESTS SET FOLDER_LOCATION = ?, UPDATED_BY = ?, UPDATED_DATE = ? WHERE REFERENCE_NUM = ?";
		PreparedStatement stmt = null;
		Connection conn = null;
		try {
			conn = getConnection();
			conn.setAutoCommit(false);
			stmt = conn.prepareStatement(sql);
			stmt.setString(1, location);
			stmt.setString(2, updatedBy);
			stmt.setTimestamp(3, DateHelper.sqlTimestamp(new java.util.Date()));
			stmt.setString(4, referenceNumber);
			stmt.executeUpdate();
			conn.commit();
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			if(conn != null){
				conn.rollback();
			}
			throw e;
		} finally {
			closeResources(conn, stmt, null);
		}
		LOGGER.info("updateLocation end");
	}

	// edited by Aldrich Abrogena - Jan 16,2008
	public void updateRemarks(String referenceNumber, String remarks,
			String user) throws SQLException {
		
		LOGGER.info("updateRemarks start");

		String sql = "UPDATE ASSESSMENT_REQUESTS SET REMARKS = ?, UPDATED_BY = ?, UPDATED_DATE = ? WHERE REFERENCE_NUM = ?";
		PreparedStatement stmt = null;
		Connection conn = null;
		try {
			conn = getConnection();
			conn.setAutoCommit(false);
			stmt = conn.prepareStatement(sql);
			stmt.setString(1, remarks);
			stmt.setString(2, user);
			stmt.setDate(3, DateHelper.sqlDate(new java.util.Date()));
			stmt.setString(4, referenceNumber);
			stmt.executeUpdate();
			conn.commit();
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			if(conn != null){
				conn.rollback();
			}
			throw e;
		} finally {
			closeResources(conn, stmt, null);
		}
		LOGGER.info("updateRemarks end");
	}

	// For testing purposes
	public void deleteAssessment(String referenceNumber) throws SQLException {
		
		LOGGER.info("deleteAssessment start");
		String sql = "Delete from assessment_requests where reference_num = ?";
		PreparedStatement stmt = null;
		Connection conn = null;
		try {
			conn = getConnection();
			conn.setAutoCommit(false);
			stmt = conn.prepareStatement(sql);
			stmt.setString(1, referenceNumber);
			stmt.executeUpdate();
			conn.commit();
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			if(conn != null){
				conn.rollback();
			}
			throw e;
		} finally {
			closeResources(conn, stmt, null);
		}
		LOGGER.info("deleteAssessment end");
	}

	// edited by Aldrich Abrogena - Jan 16,2008
	public AssessmentRequestData retrieveRequestDetail(
			final String refNo) throws SQLException {
		
		LOGGER.info("retrieveRequestDetail start");
		
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT")
		.append(" 	REFERENCE_NUM as REFERENCE_NUM,")
		.append(" 	AR.POLICY_SUFFIX as POLICY_SUFFIX, ")
		.append(" 	AR.LOB as LOB_CODE,")
		.append(" 	LB.LOB_DESC as LOB_DESC,")
		.append(" 	AR.SOURCE_SYSTEM as SOURCE_SYSTEM,")
		.append(" 	AR.AMOUNT_COVERED as AMOUNT_COVERED,")
		.append(" 	AR.PREMIUM as PREMIUM, ")
		.append(" 	AR.INSURED_CLIENT_ID as INSURED_ID,")
		.append(" 	INSURED.CL_GIVEN_NAME as INSURED_GIVEN_NAME,")
		.append(" 	INSURED.CL_MIDDLE_NAME as INSURED_MIDDLE_NAME,")
		.append(" 	INSURED.CL_LAST_NAME as INSURED_LAST_NAME,")
		.append(" 	INSURED.CL_TITLE as INSURED_TITLE,")
		.append(" 	INSURED.CL_SUFFIX as INSURED_SUFFIX,")
		.append(" 	INSURED.CL_AGE as INSURED_AGE,")
		.append(" 	INSURED.CL_SEX as INSURED_SEX,")
		.append(" 	INSURED.CL_BIRTH_DATE as INSURED_BIRTH_DATE,")
		.append(" 	AR.OWNER_CLIENT_ID as OWNER_ID,")
		.append(" 	OWNER.CL_GIVEN_NAME as OWNER_GIVEN_NAME,")
		.append(" 	OWNER.CL_MIDDLE_NAME as OWNER_MIDDLE_NAME,")
		.append(" 	OWNER.CL_LAST_NAME as OWNER_LAST_NAME,")
		.append("	OWNER.CL_TITLE as OWNER_TITLE,")
		.append("	OWNER.CL_SUFFIX as OWNER_SUFFIX,")
		.append("	OWNER.CL_AGE as OWNER_AGE,")
		.append("	OWNER.CL_SEX as OWNER_SEX,")
		.append(" 	OWNER.CL_BIRTH_DATE as OWNER_BIRTH_DATE,")
		.append(" 	AR.BRANCH_ID as BRANCH_CODE,")
		.append(" 	BR.SLO_OFFICE_NAME as BRANCH_NAME,")
		.append(" 	AR.AGENT_ID as AGENT_ID,")
		.append(" 	AGNT.USR_FIRST_NAME as AGENT_FIRST_NAME,")
		.append(" 	AGNT.USR_LAST_NAME as AGENT_LAST_NAME,")
		.append(" 	AR.STATUS_ID as STATUS_ID,")
		.append(" 	ST.STAT_DESC as STATUS_DESC,")
		.append(" 	AR.STATUS_DATE as STATUS_DATE,")
		.append(" 	AR.ASSIGNED_TO as ASSIGNED_TO_CODE,")
		.append(" 	ASSIGN.USR_FIRST_NAME AS ASSIGNED_TO_FIRST_NAME,")
		.append(" 	ASSIGN.USR_LAST_NAME as ASSIGNED_TO_LAST_NAME,")
		.append(" 	ASSIGN.USR_ACF2ID as ASSIGN_ACF2,")
		.append(" 	AR.UNDERWRITER as UW_CODE,")
		.append(" 	UW.USR_FIRST_NAME AS UW_FIRST_NAME,")
		.append(" 	UW.USR_LAST_NAME as UW_LAST_NAME,")
		.append(" 	UW.USR_ACF2ID as UW_ACF2,")
		.append(" 	AR.FOLDER_LOCATION as LOC_CODE,")
		.append(" 	LOC.USR_LAST_NAME as LOC_LAST_NAME,")
		.append(" 	LOC.USR_FIRST_NAME AS LOC_FIRST_NAME,")
		.append(" 	AR.APP_RECEIVED_DATE as RECEIVED_DATE,")
		.append(" 	AR.DATE_FORWARDED as DATE_FORWARDED,")
		.append(" 	AR.CREATED_BY as CREATED_BY_CODE,")
		.append(" 	CREATED_BY.USR_FIRST_NAME AS CREATED_BY_FIRST_NAME,")
		.append(" 	CREATED_BY.USR_LAST_NAME as CREATED_BY_LAST_NAME,")
		.append(" 	AR.CREATED_DATE as CREATED_DATE,")
		.append(" 	AR.UPDATED_BY as UPDATED_BY_CODE,")
		.append(" 	UPDATED_BY.USR_FIRST_NAME AS UPDATED_BY_FIRST_NAME,")
		.append(" 	UPDATED_BY.USR_LAST_NAME as UPDATED_BY_LAST_NAME,")
		.append(" 	AR.UPDATED_DATE as UPDATED_DATE,")
		.append(" 	AR.AR_TRANSMIT_IND as TRANSMIT_INDICATOR,")
		.append(" 	AR.AR_AUTO_SETTLE_IND as AUTOSETTLE_INDICATOR, ")
		.append(" 	CASE (AR.LOB)")
		.append(" 		WHEN 'GL' THEN 'M'")
		.append(" 		WHEN 'PN' THEN 'P'")
		.append(" 		WHEN 'IL' THEN 'I'")
		.append(" 	END as CLIENT_TYPE,")
		.append("	AR.REMARKS as REMARKS, ")
		.append("	AR.KO_COUNT_IND as KO_COUNT_IND, ")
		.append("	AR.REQT_COUNT_IND as REQT_COUNT_IND ")
		.append(" FROM")
		.append(" 	CLIENTS OWNER,")
		.append(" 	CLIENTS INSURED,")
		.append(" 	ASSESSMENT_REQUESTS AR,")
		.append(" 	LINES_OF_BUSINESS LB,")
		.append(" 	SUNLIFE_OFFICES BR,")
		.append(" 	USERS AGNT,")
		.append(" 	STATUS ST,")
		.append(" 	USERS ASSIGN,")
		.append(" 	USERS UW,")
		.append(" 	USERS LOC,")
		.append(" 	USERS CREATED_BY,")
		.append("	 USERS UPDATED_BY")
		.append("  WHERE")
		.append("	REFERENCE_NUM = ? AND")
		.append("	AR.OWNER_CLIENT_ID=OWNER.CLIENT_ID (+) AND")
		.append("	AR.INSURED_CLIENT_ID=INSURED.CLIENT_ID (+) AND")
		.append("	AR.LOB = LB.LOB_CODE (+) AND ")
		.append("	AR.BRANCH_ID = BR.SLO_OFFICE_CODE (+) AND")
		.append("	AR.AGENT_ID = AGNT.USER_ID (+) AND")
		.append("	AR.STATUS_ID = ST.STAT_ID (+) AND")
		.append("	AR.ASSIGNED_TO = ASSIGN.USER_ID (+) AND")
		.append("	AR.UNDERWRITER = UW.USER_ID (+) AND")
		.append("	AR.FOLDER_LOCATION = LOC.USER_ID (+) AND")
		.append("	AR.CREATED_BY = CREATED_BY.USER_ID (+) AND")
		.append("	AR.UPDATED_BY = UPDATED_BY.USER_ID (+)");

		PreparedStatement ps = null;
		ResultSet rs = null;
		AssessmentRequestData assessReqData = null;
		Connection conn = null;
		try {
			conn = getConnection();
			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, refNo);

			rs = ps.executeQuery();

			if (rs.next()) {
				assessReqData = new AssessmentRequestData();
				assessReqData.setReferenceNumber(rs.getString("REFERENCE_NUM"));
				assessReqData.setPolicySuffix(rs.getString("POLICY_SUFFIX"));

				LOBData lobData = new LOBData();
				lobData.setLOBCode(rs.getString("LOB_CODE"));
				lobData.setLOBDesc(rs.getString("LOB_DESC"));

				assessReqData.setLob(lobData);
				assessReqData.setSourceSystem(rs.getString("SOURCE_SYSTEM"));
				assessReqData.setAmountCovered(rs.getDouble("AMOUNT_COVERED"));
				assessReqData.setPremium(rs.getDouble("PREMIUM"));

				ClientData insuredClientData = new ClientData();
				insuredClientData.setClientId(rs.getString("INSURED_ID"));
				insuredClientData.setGivenName(rs
						.getString("INSURED_GIVEN_NAME"));
				insuredClientData.setMiddleName(rs
						.getString("INSURED_MIDDLE_NAME"));
				insuredClientData
						.setLastName(rs.getString("INSURED_LAST_NAME"));
				insuredClientData.setTitle(rs.getString("INSURED_TITLE"));
				insuredClientData.setSuffix(rs.getString("INSURED_SUFFIX"));
				insuredClientData.setAge(rs.getInt("INSURED_AGE"));
				insuredClientData.setSex(rs.getString("INSURED_SEX"));
				insuredClientData.setBirthDate(DateHelper.utilDate(rs
						.getDate("INSURED_BIRTH_DATE")));

				assessReqData.setInsured(insuredClientData);

				ClientData ownerClientData = new ClientData();
				ownerClientData.setClientId(rs.getString("OWNER_ID"));
				ownerClientData.setGivenName(rs.getString("OWNER_GIVEN_NAME"));
				ownerClientData
						.setMiddleName(rs.getString("OWNER_MIDDLE_NAME"));
				ownerClientData.setLastName(rs.getString("OWNER_LAST_NAME"));
				ownerClientData.setTitle(rs.getString("OWNER_TITLE"));
				ownerClientData.setSuffix(rs.getString("OWNER_SUFFIX"));
				ownerClientData.setAge(rs.getInt("OWNER_AGE"));
				ownerClientData.setSex(rs.getString("OWNER_SEX"));
				ownerClientData.setBirthDate(DateHelper.utilDate(rs
						.getDate("OWNER_BIRTH_DATE")));

				assessReqData.setOwner(ownerClientData);

				SunLifeOfficeData branchData = new SunLifeOfficeData();
				branchData.setOfficeId(rs.getString("BRANCH_CODE"));
				branchData.setOfficeName(rs.getString("BRANCH_NAME"));

				assessReqData.setBranch(branchData);

				UserProfileData agentData = new UserProfileData();
				agentData.setUserId(rs.getString("AGENT_ID"));
				agentData.setFirstName(rs.getString("AGENT_FIRST_NAME"));
				agentData.setLastName(rs.getString("AGENT_LAST_NAME"));

				assessReqData.setAgent(agentData);

				StatusData statusData = new StatusData();
				statusData.setStatusId(rs.getLong("STATUS_ID"));
				statusData.setStatusDesc(rs.getString("STATUS_DESC"));

				assessReqData.setStatus(statusData);
				assessReqData.setStatusDate(DateHelper.utilDate(rs
						.getDate("STATUS_DATE")));

				UserProfileData assignedTo = new UserProfileData();
				assignedTo.setUserId(rs.getString("ASSIGNED_TO_CODE"));
				assignedTo.setFirstName(rs.getString("ASSIGNED_TO_FIRST_NAME"));
				assignedTo.setLastName(rs.getString("ASSIGNED_TO_LAST_NAME"));
				assignedTo.setACF2ID(rs.getString("ASSIGN_ACF2"));

				assessReqData.setAssignedTo(assignedTo);

				UserProfileData underwriter = new UserProfileData();
				underwriter.setUserId(rs.getString("UW_CODE"));
				underwriter.setFirstName(rs.getString("UW_FIRST_NAME"));
				underwriter.setLastName(rs.getString("UW_LAST_NAME"));
				underwriter.setACF2ID(rs.getString("UW_ACF2"));

				assessReqData.setUnderwriter(underwriter);

				UserProfileData folderLocation = new UserProfileData();
				folderLocation.setUserId(rs.getString("LOC_CODE"));
				folderLocation.setFirstName(rs.getString("LOC_FIRST_NAME"));
				folderLocation.setLastName(rs.getString("LOC_LAST_NAME"));

				assessReqData.setFolderLocation(folderLocation);

				assessReqData.setApplicationReceivedDate(DateHelper.utilDate(rs
						.getDate("RECEIVED_DATE")));
				assessReqData.setForwardedDate(DateHelper.utilDate(rs
						.getDate("DATE_FORWARDED")));

				UserProfileData createdBy = new UserProfileData();
				createdBy.setUserId(rs.getString("CREATED_BY_CODE"));
				createdBy.setFirstName(rs.getString("CREATED_BY_FIRST_NAME"));
				createdBy.setLastName(rs.getString("CREATED_BY_LAST_NAME"));

				assessReqData.setCreatedBy(createdBy);
				assessReqData.setCreatedDate(rs.getTimestamp("CREATED_DATE"));

				UserProfileData updatedBy = new UserProfileData();
				updatedBy.setUserId(rs.getString("UPDATED_BY_CODE"));
				updatedBy.setFirstName(rs.getString("UPDATED_BY_FIRST_NAME"));
				updatedBy.setLastName(rs.getString("UPDATED_BY_LAST_NAME"));

				assessReqData.setUpdatedBy(updatedBy);
				assessReqData.setUpdatedDate(rs.getTimestamp("UPDATED_DATE"));
				assessReqData.setRemarks(rs.getString("REMARKS"));
				assessReqData.setTransmitIndicator(rs
						.getString("TRANSMIT_INDICATOR"));
				assessReqData.setAutoSettleIndicator(rs
						.getString("AUTOSETTLE_INDICATOR"));

				ClientDataSheetDAO cdsDAO = new ClientDataSheetDAO();
				// summary policy
				CDSSummaryPolicyCoverageData summaryData = cdsDAO
						.retrieveSummaryPolicy(refNo, rs
								.getString("INSURED_ID"));
				// mortality rating
				CDSMortalityRatingData mrData = cdsDAO.retrieveMortalityRating(
						refNo, rs.getString("INSURED_ID"));
				// policy coverage list
				ArrayList policyCovList = cdsDAO.retrievePolicyCoverageList(
						refNo, rs.getString("INSURED_ID"));

				ClientDataSheetData cdsData = new ClientDataSheetData();
				cdsData.setClientType(rs.getString("CLIENT_TYPE"));
				cdsData.setSummaryOfPolicies(summaryData);
				cdsData.setMortalityRating(mrData);
				cdsData.setPolicyCoverageDetailsList(policyCovList);

				assessReqData.setClientDataSheet(cdsData);

				// auto-settle error detail
				if (assessReqData.getAutoSettleIndicator() != null
						&& assessReqData.getAutoSettleIndicator().equals(IUMConstants.INDICATOR_FAILED)) {
					
					ExceptionDAO expDAO = new ExceptionDAO();
					assessReqData.setAutoSettleError(
							expDAO.retrieveExceptionDetails(assessReqData.getReferenceNumber()));
				}

				assessReqData.setKoCountInd(rs.getString("KO_COUNT_IND"));
				assessReqData.setReqtCountInd(rs.getString("REQT_COUNT_IND"));
			}
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		} finally {
			closeResources(conn, ps, rs);
		}
		LOGGER.info("retrieveRequestDetail end");
		return (assessReqData);
	}

	/**
	 * This method will insert an Assessment Request Data into the database. The
	 * method expected the sequence number to be defined.
	 * 
	 * @param ard
	 * @throws SQLException
	 */
	// edited by Aldrich Abrogena - Jan 16,2008
	public void insertAssessmentRequest(AssessmentRequestData ard)
			throws SQLException, IOException {
		
		LOGGER.info("insertAssessmentRequest start");
		StringBuffer sb = new StringBuffer();
	
		sb.append("INSERT INTO assessment_requests (");
		sb.append("reference_num, ");
		sb.append("lob, "); 
		sb.append("source_system, "); 
		sb.append("amount_covered, ");
		sb.append("premium, "); 
		sb.append("insured_client_id, "); 
		sb.append("owner_client_id, "); 
		sb.append("branch_id, "); 
		sb.append("agent_id, "); 
		sb.append("status_date, "); 
		sb.append("assigned_to, "); 
		sb.append("underwriter, "); 
		sb.append("folder_location, "); 
		sb.append("remarks, "); 
		sb.append("app_received_date, ");
		sb.append("date_forwarded, "); 
		sb.append("created_by, "); 
		sb.append("created_date, "); 
		sb.append("status_id, "); 
		sb.append("policy_suffix "); 

		sb
				.append(") values (UPPER(?), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, 1, ?)");
		

		PreparedStatement ps = null;
		Connection conn = null;
		try {
			conn = getConnection();
			conn.setAutoCommit(false);
			ps = conn.prepareStatement(sb.toString());
			ps.setString(1, ard.getReferenceNumber());
			
			if (ard.getLob() != null) {
				ps.setString(2, ard.getLob().getLOBCode());
			} else {
				ps.setString(2, null);
			}
			
			ps.setString(3, ard.getSourceSystem());
			ps.setDouble(4, ard.getAmountCovered());
			ps.setDouble(5, ard.getPremium());
			
			if (ard.getInsured() != null) {
				ps.setString(6, ard.getInsured().getClientId());
			} else {
				ps.setString(6, null);
			}

			if (ard.getOwner() != null) {
				ps.setString(7, ard.getOwner().getClientId());
			} else {
				ps.setString(7, null);
			}

			if (ard.getBranch() != null) {
				ps.setString(8, ard.getBranch().getOfficeId());
			} else {
				ps.setString(8, null);
			}

			if (ard.getAgent() != null) {
				ps.setString(9, ard.getAgent().getUserId());
			} else {
				ps.setString(9, null);
			}

			ps.setDate(10, DateHelper.sqlDate(ard.getStatusDate()));
			

			if (ard.getAssignedTo() != null) {
				ps.setString(11, ard.getAssignedTo().getUserId());
			} else {
				ps.setString(11, null);
			}

			if (ard.getUnderwriter() != null) {
				ps.setString(12, ard.getUnderwriter().getUserId());
			} else {
				ps.setString(12, null);
			}

			if (ard.getFolderLocation() != null) {
				ps.setString(13, ard.getFolderLocation().getUserId());
			} else {
				ps.setString(13, null);
			}

			if (ard.getRemarks() != null) {
				ps.setString(14, ard.getRemarks());
			} else {
				ps.setString(14, null);
			}

			ps
					.setDate(15, DateHelper.sqlDate(ard
							.getApplicationReceivedDate()));
			ps.setDate(16, DateHelper.sqlDate(ard.getForwardedDate()));
			
			if (ard.getCreatedBy() != null) {
				ps.setString(17, ard.getCreatedBy().getUserId());
			} else {
				ps.setString(17, null);
			}

			ps.setTimestamp(18, DateHelper.sqlTimestamp(ard.getCreatedDate()));
		
			if (ard.getPolicySuffix() != null) {
				ps.setString(19, ard.getPolicySuffix());
			} else {
				ps.setString(19, null);
			}

			ps.executeUpdate();
			conn.commit();
		} catch (SQLException e2) {
			LOGGER.error(CodeHelper.getStackTrace(e2));
			if(conn != null){
				conn.rollback();
			}
			throw e2;
		} finally {
			closeResources(conn, ps, null);
		}
		LOGGER.info("insertAssessmentRequest end");
	}

	// edited by Aldrich Abrogena - Jan 16,2008
	public int countPendingARs(String userID) throws SQLException {
		
		LOGGER.info("countPendingARs start");
		int count = 0;
		String sql = "SELECT COUNT(*) FROM ASSESSMENT_REQUESTS WHERE ASSIGNED_TO = ? AND FOLDER_LOCATION != ?";
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection conn = null;
		try {
			conn = getConnection();
			ps = conn.prepareStatement(sql);
			ps.setString(1, userID);
			ps.setString(2, IUMConstants.LOCATION_RECORD_SECTION);
			rs = ps.executeQuery();
			if (rs.next()) {
				count = rs.getInt(1);
			}
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		} finally {
			closeResources(conn, ps, rs);
		}
		LOGGER.info("countPendingARs end");
		return count;
	}

	/**
	 * Returns the number of assessment request for a specific branch and status
	 * 
	 * @param branchID
	 * @param statusID
	 * @return number of assessment request
	 * @throws SQLException
	 */
	// edited by Aldrich Abrogena - Jan 16,2008
	public int countARsPerBranch(String branchID, long statusID)
			throws SQLException {
		
		LOGGER.info("countARsPerBranch start");
		int count = 0;
		String sql = "SELECT COUNT(*) FROM ASSESSMENT_REQUESTS WHERE BRANCH_ID = ? AND STATUS_ID = ?";
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection conn = null;
		try {
			conn = getConnection();
			ps = conn.prepareStatement(sql);
			ps.setString(1, branchID);
			ps.setLong(2, statusID);
			rs = ps.executeQuery();
			if (rs.next()) {
				count = rs.getInt(1);
			}
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		} finally {
			closeResources(conn, ps, rs);
		}
		LOGGER.info("countARsPerBranch end");
		return count;
	}

	/**
	 * Returns the count of assessment request for a specific status and agent
	 * 
	 * @param agentID
	 * @param statusID
	 * @return number of assessment request
	 * @throws SQLException
	 */
	// edited by Aldrich Abrogena - Jan 16,2008
	public int countARsPerAgent(String agentID, long statusID)
			throws SQLException {
		
		LOGGER.info("countARsPerAgent start");
		int count = 0;
		String sql = "SELECT COUNT(*) FROM ASSESSMENT_REQUESTS WHERE AGENT_ID = ? AND STATUS_ID = ?";
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection conn = null;
		try {
			conn = getConnection();
			ps = conn.prepareStatement(sql);
			ps.setString(1, agentID);
			ps.setLong(2, statusID);
			rs = ps.executeQuery();
			if(rs != null){
				if (rs.next()) {
					count = rs.getInt(1);
				}
			}
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		} finally {
			closeResources(conn, ps, rs);
		}
		LOGGER.info("countARsPerAgent end");
		return count;
	}

	/**
	 * Returns the number of assessment request for a specific status
	 * 
	 * @param statusID
	 * @return number of assessment request
	 * @throws SQLException
	 */
	// edited by Aldrich Abrogena - Jan 16,2008
	public int countARsPerStatus(long statusID) throws SQLException {
		
		LOGGER.info("countARsPerStatus start");
		int count = 0;
		String sql = "SELECT COUNT(*) FROM ASSESSMENT_REQUESTS WHERE STATUS_ID = ?";
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection conn = null;
		try {
			conn = getConnection();
			ps = conn.prepareStatement(sql);
			ps.setLong(1, statusID);
			rs = ps.executeQuery();
			if (rs.next()) {
				count = rs.getInt(1);
			}
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		} finally {
			closeResources(conn, ps, rs);
		}
		LOGGER.info("countARsPerStatus end");
		return count;
	}

	/**
	 * Updates the assessment request fields
	 * 
	 * @param referenceNum
	 * @param assignedTo
	 * @param updatedBy
	 * @author cris
	 */
	// edited by Aldrich Abrogena - Jan 16,2008
	public void updateAssessmentRequest(final AssessmentRequestData ard) throws SQLException, Exception {
		
		LOGGER.info("updateAssessmentRequest start");
		String sql = "UPDATE ASSESSMENT_REQUESTS " + "SET LOB = ?, "
				+ " SOURCE_SYSTEM = ?, " + " AMOUNT_COVERED = ?, "
				+ " PREMIUM = ?, " + " INSURED_CLIENT_ID = ?, "
				+ " OWNER_CLIENT_ID = ?, " + " BRANCH_ID = ?, "
				+ " AGENT_ID = ?, " + " UNDERWRITER = ?, "
				+ " APP_RECEIVED_DATE = ?, " + " DATE_FORWARDED = ?, "
				+ " UPDATED_BY = ?, " + " UPDATED_DATE = ?"
				+ "WHERE REFERENCE_NUM = ?";

		PreparedStatement ps = null;
		Connection conn = null;
		try {
			conn = getConnection();
			conn.setAutoCommit(false);
			ps = conn.prepareStatement(sql);
			
			LOBData lob = ard.getLob();
			if (lob != null) {
				ps.setString(1, lob.getLOBCode());
			} else {
				ps.setString(1, null);
			}

			String sourceSystem = ard.getSourceSystem();
			if (sourceSystem != null) {
				ps.setString(2, sourceSystem);
			} else {
				ps.setString(2, null);
			}

			ps.setDouble(3, ard.getAmountCovered());
			ps.setDouble(4, ard.getPremium());

			ClientData insured = ard.getInsured();
			if (insured != null) {
				ps.setString(5, insured.getClientId());
			} else {
				ps.setString(5, null);
			}

			ClientData owner = ard.getOwner();
			if (owner != null) {
				ps.setString(6, owner.getClientId());
			} else {
				ps.setString(6, null);
			}

			SunLifeOfficeData branch = ard.getBranch();
			if (branch != null) {
				ps.setString(7, branch.getOfficeId());
			} else {
				ps.setString(7, null);
			}

			UserProfileData agent = ard.getAgent();
			if (agent != null) {
				ps.setString(8, agent.getUserId());
			} else {
				ps.setString(8, null);
			}

			UserProfileData underwriter = ard.getUnderwriter();
			if (underwriter != null
					&& underwriter.getUserId() != null
					&& underwriter.getUserId().equals("")) {
				ps.setString(9, underwriter.getUserId());
			} else {
				String uw = this.getAutoAssignUW(ard);
				ps.setString(9, uw);
			}

			Date dateReceived = ard.getApplicationReceivedDate();
			if (dateReceived != null) {
				ps.setDate(10, DateHelper.sqlDate(dateReceived));
			} else {
				ps.setDate(10, null);
			}

			Date dateForwarded = ard.getForwardedDate();
			if (dateForwarded != null) {
				ps.setDate(11, DateHelper.sqlDate(dateForwarded));
			} else {
				ps.setDate(11, null);
			}

			UserProfileData updatedBy = ard.getUpdatedBy();
			if (updatedBy != null) {
				ps.setString(12, updatedBy.getUserId());
			} else {
				ps.setString(12, null);
			}

			Date updatedDate = ard.getUpdatedDate();
			if (updatedDate != null) {
				ps.setTimestamp(13, DateHelper.sqlTimestamp(updatedDate));
			} else {
				ps.setTimestamp(13, null);
			}
			
			ps.setString(14, ard.getReferenceNumber());
			
			// Execute update
			ps.executeUpdate();
			conn.commit();
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			if(conn != null){
				conn.rollback();
			}
			throw e;
		} finally {
			closeResources(conn, ps, null);
		}
		LOGGER.info("updateAssessmentRequest end");
	}

	/**
	 * 
	 * @return
	 */
	// edited by Aldrich Abrogena - Jan 16,2008
	public ArrayList retrieveApplicationsApprovedReport(
			ApplicationsApprovedFilterData aafd) throws SQLException, Exception {
		
		LOGGER.info("retrieveApplicationsApprovedReport start");
		StringBuffer sql = new StringBuffer();
		sql.append(" select ");
		sql.append(" LB.LOB_DESC ");
		sql.append(" , DP.DEPT_DESC ");
		sql.append(" , AR.REFERENCE_NUM ");
		sql.append(" from ");
		sql.append("  ASSESSMENT_REQUESTS AR ");
		sql.append(", LINES_OF_BUSINESS LB ");
		sql.append(", DEPARTMENTS DP ");
		sql.append(", USERS US ");
		sql.append(" where ");
		sql.append("     AR.STATUS_ID in (?,?) "); 
		sql.append(" and AR.STATUS_DATE ");
		sql.append("    between to_date(?,'dd-MON-yyyy hh24:mi:ss')");
		sql.append("    and to_date(?,'dd-MON-yyyy hh24:mi:ss') ");
		sql.append(" and AR.LOB = LB.LOB_CODE ");
		sql.append(" and US.DEPT_ID = DP.DEPT_CODE ");
		sql.append(" and AR.UPDATED_BY = US.USER_ID ");
		ResultSet rs = null;
		PreparedStatement ps = null;
		ArrayList result = null;
		Connection conn = null;
		
		try {
			if (aafd.getRequestingParty().getLOBCode() != null) {
				sql.append(" and AR.LOB = '"
						+ aafd.getRequestingParty().getLOBCode() + "'"); 
			}

			if (aafd.getApprovingParty().getDeptId() != null) {
				sql.append(" and US.DEPT_ID = '"
						+ aafd.getApprovingParty().getDeptId() + "'"); 
			}

			sql.append(" order by ");
			sql.append("  LB.LOB_DESC, DP.DEPT_DESC, AR.REFERENCE_NUM ");

			String startDate = DateHelper.addTimePart(aafd.getStartDate(),
					DateHelper.ADD_START_DAY_TIME);
			String endDate = DateHelper.addTimePart(aafd.getEndDate(),
					DateHelper.ADD_END_DAY_TIME);

			result = new ArrayList();

			conn = getConnection();
			ps = conn.prepareStatement(sql.toString());
			ps.setLong(1, IUMConstants.STATUS_APPROVED);
			ps.setLong(2, IUMConstants.STATUS_FOR_OFFER);
			ps.setString(3, startDate);
			ps.setString(4, endDate);

			rs = ps.executeQuery();

			while (rs.next()) {
				LOBData dataLOB = new LOBData();
				dataLOB.setLOBDesc(rs.getString("LOB_DESC"));

				UserProfileData dataUser = new UserProfileData();
				dataUser.setDeptCode(rs.getString("DEPT_DESC"));

				AssessmentRequestData ard = new AssessmentRequestData();
				ard.setLob(dataLOB);
				ard.setUpdatedBy(dataUser);
				ard.setReferenceNumber(rs.getString("REFERENCE_NUM"));
				result.add(ard);
			}
			
		} catch (Exception e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		} finally {
			closeResources(conn, ps, rs);
		}
		LOGGER.info("retrieveApplicationsApprovedReport end");
		return (result);
	}

	private StringBuffer queryARStatusByBranch(boolean hasBranch) {
		
		LOGGER.info("queryARStatusByBranch start");
		StringBuffer query = new StringBuffer();
		query.append(" select ");
		query.append("  SLO_OFFICE_NAME ");
		query.append(", STAT_DESC");
		query.append(", REFERENCE_NUM");
		query.append(", TO_CHAR(STATUS_DATE, 'ddMONyyyy') as STATUS_DATE");
		query.append(", count(REFERENCE_NUM) as COUNT ");
		query.append(" from ");
		query.append("  ASSESSMENT_REQUESTS ");
		query.append(", STATUS ");
		query.append(", SUNLIFE_OFFICES ");
		query.append(" where ");
		query.append("      BRANCH_ID = SLO_OFFICE_CODE ");
		query.append("  and STATUS_ID = STAT_ID ");
		query.append("  and STAT_TYPE = ? ");
		query.append("  and STATUS_DATE ");
		query.append("     between to_date(?, 'dd-MON-yyyy hh24:mi:ss') ");
		query.append("         and to_date(?, 'dd-MON-yyyy hh24:mi:ss') ");

		if (hasBranch) {
			query.append(" and BRANCH_ID = ? ");
		}

		query.append(" group by grouping sets ( ");
		query.append("   (SLO_OFFICE_NAME) ");
		query.append(" , (SLO_OFFICE_NAME, STAT_DESC)");
		query
				.append(" , (SLO_OFFICE_NAME, STAT_DESC, REFERENCE_NUM, STATUS_DATE)");
		query.append(")");
		query.append(" order by 1, 2, 3, 4");

		LOGGER.info("queryARStatusByBranch end");
		
		return (query);
	}

	private StringBuffer queryARStatusByLOB(boolean hasLOB) {
		
		LOGGER.info("queryARStatusByLOB start");

		StringBuffer query = new StringBuffer();

		query.append(" select ");
		query.append("  LOB_DESC ");
		query.append(", STAT_DESC");
		query.append(", REFERENCE_NUM");
		query.append(", TO_CHAR(STATUS_DATE, 'ddMONyyyy') as STATUS_DATE");
		query.append(", count(REFERENCE_NUM) as COUNT ");
		query.append(" from ");
		query.append("  ASSESSMENT_REQUESTS ");
		query.append(", STATUS ");
		query.append(", LINES_OF_BUSINESS ");
		query.append(" where ");
		query.append("      LOB = LOB_CODE ");
		query.append("  and STATUS_ID = STAT_ID ");
		query.append("  and STAT_TYPE = ? ");
		query.append("  and STATUS_DATE ");
		query.append("     between to_date(?, 'dd-MON-yyyy hh24:mi:ss') ");
		query.append("         and to_date(?, 'dd-MON-yyyy hh24:mi:ss') ");

		if (hasLOB) {
			query.append(" and LOB = ? ");
		}

		query.append(" group by grouping sets ( ");
		query.append("   (LOB_DESC) ");
		query.append(" , (LOB_DESC, STAT_DESC)");
		query.append(" , (LOB_DESC, STAT_DESC, REFERENCE_NUM, STATUS_DATE)");
		query.append(")");
		query.append(" order by 1, 2, 3, 4");

		LOGGER.info("queryARStatusByLOB end");
		
		return (query);
	}

	private StringBuffer queryARStatusByUnderwriter(boolean hasUnderwriter) {
		
		LOGGER.info("queryARStatusByUnderwriter start");

		StringBuffer query = new StringBuffer();

		query.append(" select ");
		query.append("  (USR_LAST_NAME || ', ' || USR_FIRST_NAME) AS UW_NAME");
		query.append(", STAT_DESC");
		query.append(", REFERENCE_NUM");
		query.append(", TO_CHAR(STATUS_DATE, 'ddMONyyyy') as STATUS_DATE");
		query.append(", count(REFERENCE_NUM) as COUNT ");
		query.append(" from ");
		query.append("  ASSESSMENT_REQUESTS ");
		query.append(", STATUS ");
		query.append(", USERS ");
		query.append(" where ");
		query.append("      UNDERWRITER = USER_ID ");
		query.append("  and STATUS_ID = STAT_ID ");
		query.append("  and STAT_TYPE = ? ");
		query.append("  and STATUS_DATE ");
		query.append("     between to_date(?, 'dd-MON-yyyy hh24:mi:ss') ");
		query.append("         and to_date(?, 'dd-MON-yyyy hh24:mi:ss') ");

		if (hasUnderwriter) {
			query.append(" and UNDERWRITER = ? ");
		}

		query.append(" group by grouping sets ( ");
		query.append("   (USR_LAST_NAME, USR_FIRST_NAME, USR_MIDDLE_NAME) ");
		query
				.append(" , (USR_LAST_NAME, USR_FIRST_NAME, USR_MIDDLE_NAME, STAT_DESC)");
		query
				.append(" , (USR_LAST_NAME, USR_FIRST_NAME, USR_MIDDLE_NAME, STAT_DESC, REFERENCE_NUM, STATUS_DATE)");
		query.append(")");
		query.append(" order by 1, 2, 3, 4");

		
		
		LOGGER.info("queryARStatusByUnderwriter end");
		return (query);
	}

	// edited by Aldrich Abrogena - Jan 16,2008
	public ArrayList retrieveApplicationsByStatus(
			ApplicationsByStatusFilterData filter) throws SQLException {

		LOGGER.info("retrieveApplicationsByStatus start");

		ArrayList result = new ArrayList();

		boolean hasBranchFilter = false;
		boolean hasUnderwriterFilter = false;
		boolean hasLOBFilter = false;

		SunLifeOfficeData branch = (SunLifeOfficeData) filter.getBranch();
		UserProfileData underwriter = (UserProfileData) filter.getUnderwriter();
		LOBData lob = (LOBData) filter.getLob();

		if (branch.getOfficeId() != null) {
			hasBranchFilter = true;
		}

		if (underwriter.getUserId() != null) {
			hasUnderwriterFilter = true;
		}

		if (lob.getLOBCode() != null) {
			hasLOBFilter = true;
		}

		StringBuffer sql = new StringBuffer();
		switch (filter.getFilterType()) {
		case ApplicationsByStatusFilterData.FILTER_BY_BRANCH:
			sql = queryARStatusByBranch(hasBranchFilter);
			break;
		case ApplicationsByStatusFilterData.FILTER_BY_LOB:
			sql = queryARStatusByLOB(hasLOBFilter);
			break;
		case ApplicationsByStatusFilterData.FILTER_BY_UNDERWRITER:
			sql = queryARStatusByUnderwriter(hasUnderwriterFilter);
			break;
		}

		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection conn = null;
		try {
			conn = getConnection();	
			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, IUMConstants.STAT_TYPE_AR);

			/*
			 * to get everything in between the dates, make sure the start date
			 * parameter has been set to the time at the start of the day and
			 * the end date to the time at end of the day
			 */
			String startDate = DateHelper.addTimePart(filter.getStartDate(),
					DateHelper.ADD_START_DAY_TIME);
			String endDate = DateHelper.addTimePart(filter.getEndDate(),
					DateHelper.ADD_END_DAY_TIME);

			ps.setString(2, startDate);
			ps.setString(3, endDate);

			if (hasBranchFilter)
				ps.setString(4, branch.getOfficeId());

			if (hasUnderwriterFilter)
				ps.setString(4, underwriter.getUserId());

			if (hasLOBFilter)
				ps.setString(4, lob.getLOBCode());

			rs = ps.executeQuery();

			while (rs.next()) {
				ApplicationsByStatusData arsd = new ApplicationsByStatusData();
				arsd.setGroupName(ValueConverter.nullToString(rs.getString(1))); 
				arsd.setStatusDesc(ValueConverter.nullToString(rs
						.getString("STAT_DESC")));
				arsd.setReferenceNumber(ValueConverter.nullToString(rs
						.getString("REFERENCE_NUM")));
				arsd.setStatusDate(rs.getString("STATUS_DATE"));
				arsd.setCountAR(rs.getInt("COUNT"));

				result.add(arsd);
			}

		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		} finally {
			closeResources(conn, ps, rs);
		}
		LOGGER.info("retrieveApplicationsByStatus end");
		return (result);
	}

	// edited by Aldrich Abrogena - Jan 16,2008
	public void updateTransmit(String referenceNumber, String result)
			throws SQLException {
		
		LOGGER.info("updateTransmit start");
		
		String sql = "UPDATE ASSESSMENT_REQUESTS SET AR_TRANSMIT_IND = ? WHERE  REFERENCE_NUM = ?";

		PreparedStatement ps = null;
		Connection conn = null;
		try {
			conn = getConnection();	
			conn.setAutoCommit(false);
			ps = conn.prepareStatement(sql);
			ps.setString(1, result);
			ps.setString(2, referenceNumber);
			ps.executeUpdate();
			conn.commit();
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			if(conn != null){
				conn.rollback();
			}
			throw e;
		} finally {
			closeResources(conn, ps, null);
		}
		LOGGER.info("updateTransmit end");
	}

	public void updateAutoSettleIndicator(String referenceNumber, String result)
			throws SQLException {
		
		LOGGER.info("updateAutoSettleIndicator start");
		String sql = "UPDATE ASSESSMENT_REQUESTS SET AR_AUTO_SETTLE_IND = ? WHERE  REFERENCE_NUM = ?";
		PreparedStatement ps = null;
		Connection conn = null;
		try {
			conn = getConnection();	
			conn.setAutoCommit(false);
			ps = conn.prepareStatement(sql);
			ps.setString(1, result);
			ps.setString(2, referenceNumber);
			ps.executeUpdate();
			conn.commit();
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			if(conn != null){
				conn.rollback();
			}
			throw e;
		} finally {
			closeResources(ps, null);
		}
		LOGGER.info("updateAutoSettleIndicator end");
	}

	public int deleteAssessmentRequests(Date startDate, Date endDate)
			throws SQLException {
		
		LOGGER.info("deleteAssessmentRequests start");
		
		String sql = "DELETE ASSESSMENT_REQUESTS WHERE to_date(to_char(CREATED_DATE, 'DDMONYYYY'), 'DDMONYYYY') BETWEEN ? AND ?";
		PreparedStatement ps = null;
		int result = -1;
		Connection conn = null;
		
		try {
			conn = getConnection();	
			conn.setAutoCommit(false);
			ps = conn.prepareStatement(sql);
			ps.setDate(1, DateHelper.sqlDate(startDate));
			ps.setDate(2, DateHelper.sqlDate(endDate));
			result = ps.executeUpdate();
			conn.commit();
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			if(conn != null){
				conn.rollback();
			}
			throw e;
		} finally {
			closeResources(conn, ps, null);
		}
		LOGGER.info("deleteAssessmentRequests end");
		return result;
	}

	public ArrayList retrieveAssessmentRequests(Date startDate, Date endDate)
			throws SQLException {
		
		LOGGER.info("retrieveAssessmentRequests start");
		ArrayList list = new ArrayList();
		String sql = "SELECT REFERENCE_NUM FROM ASSESSMENT_REQUESTS  WHERE to_date(to_char(CREATED_DATE, 'DDMONYYYY'), 'DDMONYYYY') BETWEEN ? AND ?";
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection conn = null;
		
		try {
			conn = getConnection();	
			ps = conn.prepareStatement(sql);
			ps.setDate(1, DateHelper.sqlDate(startDate));
			ps.setDate(2, DateHelper.sqlDate(endDate));
			rs = ps.executeQuery();
			while (rs.next()) {
				String referenceNum = rs.getString("REFERENCE_NUM");
				list.add(referenceNum);
			}

		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		} finally {
			closeResources(conn, ps, rs);
		}
		LOGGER.info("retrieveAssessmentRequests end");
		return list;
	}

	/**
	 * Update the assessment request date forwarded field
	 * 
	 * @param referenceNum
	 * @param assignedTo
	 * @param updatedBy
	 */
	// edited by Aldrich Abrogena - Jan 16,2008
	public void updateDateForwarded(String referenceNumber, Date dateForwarded,
			String updatedBy) throws SQLException {
		
		LOGGER.info("updateDateForwarded start");
		PreparedStatement stmt = null;
		Connection conn = null;
		String sql = "UPDATE ASSESSMENT_REQUESTS SET DATE_FORWARDED = ?, UPDATED_BY = ?, UPDATED_DATE = ? WHERE REFERENCE_NUM = ? ";
		
		try {
			conn = getConnection();
			conn.setAutoCommit(false);
			stmt = conn.prepareStatement(sql);
			stmt.setDate(1, DateHelper.sqlDate(dateForwarded));
			stmt.setString(2, updatedBy);
			stmt.setTimestamp(3, DateHelper.sqlTimestamp(new java.util.Date()));
			stmt.setString(4, referenceNumber);
			stmt.executeUpdate();
			conn.commit();
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			if(conn != null){
				conn.rollback();
			}
			throw e;
		} finally {
			closeResources(conn, stmt, null);
		}
		LOGGER.info("updateDateForwarded end");
	}

	private boolean isFinalDecisionNewValue(UWAssessmentData data)
			throws SQLException {
		
		LOGGER.info("isFinalDecisionNewValue start");
		boolean isFDNew = false;
		
		String pageFinalDecision = data.getFinalDecision();
		String sql = "SELECT FINAL_DECISION FROM ASSESSMENT_REQUESTS WHERE REFERENCE_NUM = ?";
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection conn = null;
		
		try {
			conn = getConnection();	
			ps = conn.prepareStatement(sql);
			ps.setString(1, data.getReferenceNumber());
			rs = ps.executeQuery();
			rs.next();
			String dbFinalDecision = (rs.getString("FINAL_DECISION") != null) ? rs
					.getString("FINAL_DECISION").trim()
					: "";
			if (!dbFinalDecision.equalsIgnoreCase(pageFinalDecision) && !pageFinalDecision.equals("") && !pageFinalDecision.equals(" ")) {
				isFDNew = true;
			}
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		} finally {
			closeResources(conn, ps, rs);
		}
		LOGGER.info("isFinalDecisionNewValue end");
		return isFDNew;
	}

	public void insertUWAssessmentRequestAnalysisFinalDecision(
			UWAssessmentData data) throws SQLException {
		
		LOGGER.info("insertUWAssessmentRequestAnalysisFinalDecision start");
		PreparedStatement ps = null;
		Connection conn = null;
		String sql = "INSERT INTO UW_ASSESSMENT_REQUESTS (REFERENCE_NUM, UW_ANALYSIS, POSTED_BY, POSTED_DATE, UW_ANALYSIS_ID) "
				+ " VALUES(?,?,?,?,?)";
		long sequenceNumber = getSequenceNo("SEQ_UW_ASSESSMENT_REQUESTS");
		
		try {
			
			conn = getConnection();	
			conn.setAutoCommit(false);
			ps = conn.prepareStatement(sql);
			String refNo = data.getReferenceNumber();

			if (refNo != null) {
				ps.setString(1, refNo);
			} else {
				ps.setString(1, null);
			}

			String analysis = data.getFinalDecision();
			if (analysis != null) {
				ps.setCharacterStream(2, new StringReader(analysis), analysis
						.length());
			} else {
				ps.setString(2, null);
			}

			String postedBy = data.getPostedBy();
			if (postedBy != null) {
				ps.setString(3, postedBy);
			} else {
				ps.setString(3, null);
			}

			ps.setTimestamp(4, DateHelper.sqlTimestamp(data.getPostedDate()));

			ps.setLong(5, sequenceNumber);

			ps.executeUpdate();
			conn.commit();
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			if(conn != null){
				conn.rollback();
			}
			throw e;
		} finally {
			closeResources(conn, ps, null);
		}
		LOGGER.info("insertUWAssessmentRequestAnalysisFinalDecision end");
	}

	/**
	 * Update the underwriter assessment remarks field
	 * 
	 * @param referenceNumber
	 * @param remarks
	 * @param updatedBy
	 */
	// edited by Aldrich Abrogena - Jan 16,2008
	public void updateUWAssessmentRequestRemarks(UWAssessmentData data)
			throws SQLException {
		
		LOGGER.info("updateUWAssessmentRequestRemarks start");
		PreparedStatement ps = null;
		Connection conn = null;
		String sql = "UPDATE ASSESSMENT_REQUESTS SET UW_REMARKS = ?, UPDATED_BY = ?, UPDATED_DATE = ?, FINAL_DECISION = ? WHERE REFERENCE_NUM = ? ";
		try {
			conn = getConnection();	

			// Check if session Final Decision is equal to the value in db.
			if (isFinalDecisionNewValue(data)) {

				// Insert it in UW_ASSESSMENT_REQUESTS if new value
				insertUWAssessmentRequestAnalysisFinalDecision(data);
			}

			conn.setAutoCommit(false);
			ps = conn.prepareStatement(sql);
			ps.setCharacterStream(1, new StringReader(data.getRemarks()), data
					.getRemarks().length());
			ps.setString(2, data.getUpdatedBy());
			ps.setTimestamp(3, DateHelper.sqlTimestamp(new java.util.Date()));
			ps.setString(4, data.getFinalDecision());
			ps.setString(5, data.getReferenceNumber());
			ps.executeUpdate();
			conn.commit();
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			if(conn != null){
				conn.rollback();
			}
			throw e;
		} finally {
			this.closeResources(conn, ps, null);
		}
		LOGGER.info("updateUWAssessmentRequestRemarks end");
	}

	/**
	 * Update the underwriter assessment analysis field
	 * 
	 * @param referenceNumber
	 * @param analysis
	 * @param updatedBy
	 */
	// edited Aldrich Abrogena - Jan 16,2008
	public void updateUWAssessmentRequestAnalysis(UWAssessmentData data)
			throws SQLException {
		
		LOGGER.info("updateUWAssessmentRequestAnalysis start");
		String sql = "UPDATE UW_ASSESSMENT_REQUESTS SET UW_ANALYSIS = ?, UPDATED_BY = ?, UPDATED_DATE = ? WHERE REFERENCE_NUM = ? ";
		PreparedStatement ps = null;
		Connection conn = null;
		
		try {
			
			conn = getConnection();	
			conn.setAutoCommit(false);
			ps = conn.prepareStatement(sql);
			ps.setCharacterStream(1, new StringReader(data.getAnalysis()), data
					.getAnalysis().length());
			ps.setString(2, data.getUpdatedBy());
			ps.setTimestamp(3, DateHelper.sqlTimestamp(new java.util.Date()));
			ps.setString(4, data.getReferenceNumber());
			ps.executeUpdate();
			conn.commit();
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			if(conn != null){
				conn.rollback();
			}
			throw e;
		} finally {
			closeResources(conn, ps, null);
		}
		LOGGER.info("updateUWAssessmentRequestAnalysis end");
	}

	/**
	 * Retrieves the underwriter assessment request details
	 * 
	 * @param referenceNumber
	 */
	// edited by Aldrich Abrogena - Jan 16,2008
	public UWAssessmentData retrieveUWAssessmentDetails(
			final String referenceNumber) throws SQLException {
		
		LOGGER.info("retrieveUWAssessmentDetails start");
		PreparedStatement ps = null;
		ResultSet rs = null;
		String sql = "SELECT AR.UW_REMARKS AS REMARKS, AR.FINAL_DECISION FROM ASSESSMENT_REQUESTS AR WHERE AR.REFERENCE_NUM = ? ";
		Connection conn = null;
		
		try {
			conn = getConnection();	
			ps = conn.prepareStatement(sql);
			ps.setString(1, referenceNumber);
			rs = ps.executeQuery();

			UWAssessmentData data = new UWAssessmentData();
			if (rs.next()) {
				data.setRemarks(rs.getString("REMARKS"));
				data.setFinalDecision(rs.getString("FINAL_DECISION"));
			}
			return (data);
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		} finally {
			closeResources(conn, ps, rs);
			LOGGER.info("retrieveUWAssessmentDetails end");
		}
		

	}

	/**
	 * Retrieves the underwriter assessment request details
	 * 
	 * @param referenceNumber
	 * @return boolean
	 */
	// edited by Aldrich Abrogena - Jan 16,2008
	public boolean isDuplicateUWAssessment(String referenceNumber)
			throws SQLException {
		
		LOGGER.info("isDuplicateUWAssessment start");
		ResultSet rs = null;
		PreparedStatement ps = null;
		Connection conn = null;
		String sql = "SELECT UW_ANALYSIS " + "FROM UW_ASSESSMENT_REQUESTS "
				+ "WHERE REFERENCE_NUM = ? ";
		boolean isDuplicate = false;
		
		try {
			conn = getConnection();
			ps = conn.prepareStatement(sql);
			ps.setString(1, referenceNumber);
			rs = ps.executeQuery();
			if (rs.next()) {
				isDuplicate = true;
			}
			return (isDuplicate);
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		} finally {
			closeResources(conn, ps, rs);
			LOGGER.info("isDuplicateUWAssessment end");
		}
	}

	/**
	 * Inserts an underwriter assessment record
	 * 
	 * @param referenceNumber
	 * @param analysis
	 * @param updatedBy
	 */
	// edited by Aldrich Abrogena - Jan 16,2008
	public void insertUWAssessmentRequestAnalysis(UWAssessmentData data)
			throws SQLException {
	
		LOGGER.info("insertUWAssessmentRequestAnalysis start");
		PreparedStatement ps = null;
		
		long sequenceNumber = getSequenceNo("SEQ_UW_ASSESSMENT_REQUESTS");
		String sql = "INSERT INTO UW_ASSESSMENT_REQUESTS (REFERENCE_NUM, UW_ANALYSIS, POSTED_BY, POSTED_DATE, UW_ANALYSIS_ID) "
				+ " VALUES(?,?,?,?,?)";
		Connection conn = null;
		
		try {
			conn = getConnection();
			conn.setAutoCommit(false);
			ps = conn.prepareStatement(sql);

			String refNo = data.getReferenceNumber();

			if (refNo != null) {
				ps.setString(1, refNo);
			} else {
				ps.setString(1, null);
			}

			String analysis = data.getAnalysis();
			if (analysis != null) {
				ps.setCharacterStream(2, new StringReader(data.getAnalysis()),
						data.getAnalysis().length());
			} else {
				ps.setString(2, null);
			}

			String postedBy = data.getPostedBy();
			if (postedBy != null) {
				ps.setString(3, postedBy);
			} else {
				ps.setString(3, null);
			}

			ps.setTimestamp(4, DateHelper.sqlTimestamp(data.getPostedDate()));

			ps.setLong(5, sequenceNumber);

			ps.executeUpdate();
			conn.commit();
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			if(conn != null){
				conn.rollback();
			}
			throw e;
		} finally {
			closeResources(conn, ps, null);
		}
		LOGGER.info("insertUWAssessmentRequestAnalysis end");
	}

	// edited by Aldrich Abrogena - Jan 16,2008
	public void updateClientId(String clientType, String clientId,
			String referenceNumber) throws SQLException {
		
		LOGGER.info("updateClientId start");

		StringBuffer tempSql = new StringBuffer(
				"UPDATE ASSESSMENT_REQUESTS SET ");
		if (clientType.equalsIgnoreCase(IUMConstants.CLIENT_TYPE_OWNER)) {
			tempSql.append(" OWNER_CLIENT_ID = ? ");
		} else if (clientType
				.equalsIgnoreCase(IUMConstants.CLIENT_TYPE_INSURED)) {
			tempSql.append(" INSURED_CLIENT_ID = ? ");
		}
		tempSql.append(" WHERE REFERENCE_NUM = ?");
		PreparedStatement ps = null;
		Connection conn = null;
		
		try {
			conn = getConnection();
			conn.setAutoCommit(false);
			ps = conn.prepareStatement(tempSql.toString());
			ps.setString(1, clientId);
			ps.setString(2, referenceNumber);
			ps.executeUpdate();
			conn.commit();
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			if(conn != null){
				conn.rollback();
			}
			throw e;
		} finally {
			closeResources(conn, ps, null);
		}
		LOGGER.info("updateClientId end");
	}

	// edited by Aldrich Abrogena - Jan 16,2008
	public ArrayList retrieveUWAnalysis(final String referenceNumber) throws SQLException {
		
		LOGGER.info("retrieveUWAnalysis start");
		
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT ")
			.append("  UW.REFERENCE_NUM, ")
			.append("  UW.UW_ANALYSIS, ")
			.append("  CONCAT(CONCAT(U.USR_FIRST_NAME, ' '), U.USR_LAST_NAME) AS POSTED_BY, ")
			.append("  UW.POSTED_DATE ")
			.append(" FROM UW_ASSESSMENT_REQUESTS UW, USERS U ")
			.append("  WHERE REFERENCE_NUM = ? AND UW.POSTED_BY = U.USER_ID ")
			.append(" ORDER BY POSTED_DATE");
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList list = new ArrayList();
		Connection conn = null;
		
		try {
			conn = getConnection();	
			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, referenceNumber);
			rs = ps.executeQuery();
			
			while (rs.next()) {
				
				String tempAnalysis = rs.getString("UW_ANALYSIS");
				if (tempAnalysis == null || tempAnalysis.trim().equals("")) {
					continue;
				}
				
				UWAssessmentData data = new UWAssessmentData();
				data.setReferenceNumber(rs.getString("REFERENCE_NUM"));
				data.setAnalysis(tempAnalysis);
				data.setPostedBy(rs.getString("POSTED_BY"));
				data.setPostedDate(rs.getTimestamp("POSTED_DATE"));
				
				list.add(data);
				
			}
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		} finally {
			closeResources(conn, ps, rs);
		}
		
		LOGGER.info("retrieveUWAnalysis end");
		return list;
	}

	// edited by Aldrich Abrogena - Jan 16,2008
	private long getSequenceNo(String sequenceName) throws SQLException {
		
		LOGGER.info("getSequenceNo start");
		long seqNumber = 1;
		String sql = "SELECT " + sequenceName + ".NEXTVAL FROM DUAL";
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection conn = null;
		
		try {
			conn = getConnection();	
			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery();
			if (rs.next()) {
				seqNumber = rs.getLong(1);
			}
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		} finally {
			closeResources(conn, ps, rs);
		}
		LOGGER.info("getSequenceNo end");
		return seqNumber;
	}


	

	/**
	 * FOR INGENIUM Change the assessment request status
	 * 
	 * @param referenceNumber
	 *            assessment request reference number
	 * @author cris
	 */
	public String retrievePolicySuffix(String referenceNumber)
			throws SQLException {
		
		LOGGER.info("retrievePolicySuffix start");

		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection conn = null;
		String sql = "SELECT POLICY_SUFFIX FROM ASSESSMENT_REQUESTS WHERE REFERENCE_NUM = ?";
		
		try {
			conn = getConnection();	
			ps = conn.prepareStatement(sql);
			ps.setString(1, referenceNumber);
			rs = ps.executeQuery();
			while (rs.next()) {
				return rs.getString("POLICY_SUFFIX");
			}
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		} finally {
			closeResources(conn, ps, rs);
		}
		LOGGER.info("retrievePolicySuffix end");
		return null;
	}

	// edited by Aldrich Abrogena Jan 16,2008
	public String retrieveARRetrievalStatus(String referenceNumber)
			throws SQLException {

		LOGGER.info("retrieveARRetrievalStatus start");

		String sql = "SELECT AR_TRANSMIT_IND FROM ASSESSMENT_REQUESTS WHERE REFERENCE_NUM = ?";
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection conn = null;
		
		try {
			conn = getConnection();	
			ps = conn.prepareStatement(sql);
			ps.setString(1, referenceNumber);
			rs = ps.executeQuery();
			while (rs.next()) {
				return rs.getString("AR_TRANSMIT_IND");
			}
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		} finally {
			closeResources(conn, ps, rs);
		}
		LOGGER.info("retrieveARRetrievalStatus end");
		return null;
	}

	
	public void updateKOreqtCountInd(AssessmentRequestData ard)
			throws SQLException, Exception {
		
		LOGGER.info("updateKOreqtCountInd start 1");

		String sql = "UPDATE ASSESSMENT_REQUESTS " + "SET KO_COUNT_IND = ?, "
				+ " REQT_COUNT_IND = ? " +

				"WHERE REFERENCE_NUM = ?";

		PreparedStatement ps = null;
		Connection conn = null;
		
		try {
			conn = getConnection();
			conn.setAutoCommit(true);
			ps = conn.prepareStatement(sql);
			ps.setString(1, ard.getKoCountInd());
			ps.setString(2, ard.getReqtCountInd());
			ps.setString(3, ard.getReferenceNumber());
			ps.executeUpdate();
			
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			if(conn != null){
				conn.rollback();
			}
			throw e;
		} finally {
			closeResources(conn, ps, null);
		}

		LOGGER.info("updateKOreqtCountInd end 1");
	}

	// edited by Aldrich Abrogena - Jan 16,2008
	public String retrieveReqtCountInd(String referenceNumber)
			throws SQLException {

		LOGGER.info("retrieveReqtCountInd start 2");

		String sql = "SELECT REQT_COUNT_IND FROM ASSESSMENT_REQUESTS WHERE REFERENCE_NUM = ?";
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection conn = null;
		
		try {
			conn = getConnection();	
			ps = conn.prepareStatement(sql);
			ps.setString(1, referenceNumber);
			rs = ps.executeQuery();
			while (rs.next()) {
				return rs.getString("REQT_COUNT_IND");
			}
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		} finally {
			closeResources(conn, ps, rs);
		}
		LOGGER.info("retrieveReqtCountInd end 2");
		return null;
	}

	// edited by Aldrich Abrogena - Jan 16,2008
	public String retrieveKOCountInd(String referenceNumber)
			throws SQLException {

		LOGGER.info("retrieveKOCountInd start");

		String sql = "SELECT KO_COUNT_IND FROM ASSESSMENT_REQUESTS WHERE REFERENCE_NUM = ?";
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection conn = null;
		
		try {
			conn = getConnection();	
			ps = conn.prepareStatement(sql);
			ps.setString(1, referenceNumber);
			rs = ps.executeQuery();
			while (rs.next()) {
				return rs.getString("KO_COUNT_IND");
			}
			
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		} finally {
			closeResources(conn, ps, rs);
		}
		LOGGER.info("retrieveKOCountInd end");
		return null;
	}

	//Update Agent Details from Ingenium to IUM - Andre Ceasar Dacanay - start
	public void updateAgent(HashMap hm) throws SQLException {
		
		LOGGER.info("updateAgent end");
		StringBuffer tempSql = new StringBuffer(
				"UPDATE ASSESSMENT_REQUESTS SET");
		tempSql.append(" AGENT_ID = ?, BRANCH_ID = ?, UPDATED_BY = ?, UPDATED_DATE = ? ");
		tempSql.append(" WHERE REFERENCE_NUM = ?");
		PreparedStatement ps = null;
		Connection conn = null;
		
		try {
			conn = getConnection();	
			conn.setAutoCommit(true);
			Timestamp tstamp = new Timestamp(00-00-00);
			Date dt = new Date();
			ps = conn.prepareStatement(tempSql.toString());
			ps.setString(1, (String)hm.get("AGENT_ID"));
			ps.setString(2, (String)hm.get("BRANCH_ID"));
			ps.setString(3, (String)hm.get("UPDATED_BY"));
			ps.setTimestamp(4, tstamp.valueOf(DateUtil.ConvertDate(dt, "yyyy-MM-dd HH:mm:ss")));
			ps.setString(5, (String)hm.get("REFERENCE_NUM"));
			ps.executeUpdate();
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		} finally {
			closeResources(conn, ps, null);
		}
		LOGGER.info("updateAgent end");
	}
	//Update Agent Details from Ingenium to IUM - Andre Ceasar Dacanay - end
}
