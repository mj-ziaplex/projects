/*
 * Created on Jan 12, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.data.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.slocpi.ium.data.KickOutMessageData;
import com.slocpi.ium.util.CodeHelper;

/**
 * @author Cris
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class KickOutMessageDAO extends BaseDAO{

	private static final Logger LOGGER = LoggerFactory.getLogger(KickOutMessageDAO.class);
	
	public KickOutMessageDAO(){}
	
	public void insertKOMessage(KickOutMessageData koData)throws SQLException{
	
		LOGGER.info("insertKOMessage start");
		String sql = "INSERT INTO KICKOUT_MESSAGES (KO_SEQUENCE_NUM, UAR_REFERENCE_NUM, KO_MESSAGE_TEXT, CLIENT_ID, KO_FAIL_RESPONSE)" +
					 "VALUES (?,?,?,?,?)";
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection conn = null;
		
		try {
			conn = getConnection();
			conn.setAutoCommit(false);
			ps = conn.prepareStatement(sql);
			ps.setLong(1, koData.getSequenceNumber());
			ps.setString(2, koData.getReferenceNumber());
			ps.setString(3, koData.getMessageText());
			ps.setString(4, koData.getClientId());
			ps.setString(5, koData.getFailResponse()); 
			ps.executeUpdate();
			conn.commit();
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			if(conn != null){
				conn.rollback();
			}
			throw e; 
		}finally{
		 	closeResources(conn, ps, rs);
		 	LOGGER.info("insertKOMessage end");
		}		
	}
	
	public KickOutMessageData retrieveKOMessage(long sequenceNumber) throws SQLException{
		
		LOGGER.info("retrieveKOMessage start");
		String sql = "SELECT KO_SEQUENCE_NUM, UAR_REFERENCE_NUM, KO_MESSAGE_TEXT, CLIENT_ID, KO_FAIL_RESPONSE FROM KICKOUT_MESSAGES WHERE KO_SEQUENCE_NUM = ?";
		KickOutMessageData koData = new KickOutMessageData();
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection conn = null;
		
		try {
			conn = getConnection();
			ps = conn.prepareStatement(sql);
			ps.setLong(1, sequenceNumber);
			rs = ps.executeQuery();
			while(rs.next()){
				koData.setSequenceNumber(rs.getLong("ko_sequence_num"));
				koData.setReferenceNumber(rs.getString("uar_reference_num"));
				koData.setMessageText(rs.getString("ko_message_text"));
				koData.setClientId(rs.getString("client_id"));
				koData.setFailResponse(rs.getString("ko_fail_response"));
			}
			
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e; 
		}finally{
		 	closeResources(conn, ps, rs);
		}		
		LOGGER.info("retrieveKOMessage end");
		return koData;
	}


	public ArrayList retrieveKOMessages(final String refNo) throws Exception {
		
		LOGGER.info("retrieveKOMessages start");
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT") 
			.append("  KM.KO_SEQUENCE_NUM as SEQUENCE_NUM, ")
			.append("KM.UAR_REFERENCE_NUM as REFERENCE_NUM, ")
			.append("  KM.KO_MESSAGE_TEXT as MESSAGE, ")
			.append("  KM.CLIENT_ID as CLIENT_ID, ")
			.append("  KM.KO_FAIL_RESPONSE as RESPONSE ")
			.append(" FROM")
			.append(" 	KICKOUT_MESSAGES KM ")
			.append("  WHERE")
			.append("	KM.UAR_REFERENCE_NUM = ?")
			.append(" ORDER BY KM.KO_SEQUENCE_NUM");					 
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection conn = null;
		
		try {
			
			conn = getConnection();
			ArrayList list = new ArrayList();
			
			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, refNo);
	
			rs = ps.executeQuery();
       	
			while (rs.next()) {
				
				KickOutMessageData koMsgData = new KickOutMessageData();
				koMsgData.setSequenceNumber(rs.getLong("SEQUENCE_NUM"));
				koMsgData.setMessageText(rs.getString("MESSAGE"));
				koMsgData.setClientId(rs.getString("CLIENT_ID"));
				koMsgData.setFailResponse(rs.getString("RESPONSE"));
				list.add(koMsgData);				
			}
			
			return (list);
		} catch (Exception e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e; 
		}finally{
		 	closeResources(conn, ps, rs);
		 	LOGGER.info("retrieveKOMessages end");
		}		
	}// retrieveKOMessages


	public boolean isKOMEssageExisting(long sequenceNumber, String referenceNumber) throws SQLException{
		
		LOGGER.info("isKOMEssageExisting start");
		boolean result = false;
		
		String sql = "SELECT COUNT(*) FROM KICKOUT_MESSAGES WHERE KO_SEQUENCE_NUM = ? AND UAR_REFERENCE_NUM = ?";
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection conn = null;
		
		try {
			conn = getConnection();	
			ps = conn.prepareStatement(sql);
			ps.setLong(1,sequenceNumber);
			ps.setString(2, referenceNumber);
			rs = ps.executeQuery();
			if (rs.next()){
				if (rs.getInt(1) > 0){
					result = true;
				}
			}
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e; 
		}finally{
		 	closeResources(conn, ps, rs);
		}		
		LOGGER.info("isKOMEssageExisting end");
		return result;
	}
	
	public void deleteKOMessages(String referenceNum) throws SQLException {
		
		LOGGER.info("deleteKOMessages start");
		String sql = "DELETE KICKOUT_MESSAGES WHERE UAR_REFERENCE_NUM = ?";
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection conn = null;
		
		try {
			conn = getConnection();	
			conn.setAutoCommit(false);
			ps = conn.prepareStatement(sql);
			ps.setString(1, referenceNum);
			ps.executeUpdate();
			conn.commit();
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			if(conn != null){
				conn.rollback();
			}
			throw e; 
		}finally{
		 	closeResources(conn, ps, rs);
		 	LOGGER.info("deleteKOMessages end");
		}		
	}

}
