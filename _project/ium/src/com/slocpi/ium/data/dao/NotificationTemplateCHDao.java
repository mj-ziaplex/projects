package com.slocpi.ium.data.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.slocpi.ium.data.NotificationTemplateData;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.DateHelper;
import com.slocpi.ium.util.IUMConstants;
import com.slocpi.ium.util.NameValuePair;


public class NotificationTemplateCHDao extends CodeHelperDao {

	private static final Logger LOGGER = LoggerFactory.getLogger(NotificationTemplateCHDao.class);

	private Connection conn;

	public NotificationTemplateCHDao(Connection conn) throws SQLException {
	  this.conn = conn;
	}// LOBCHDao

	

	/**
	 * This method will return a collection of all the client types.
	 */
	public Collection getCodeValues() throws SQLException {
		
		LOGGER.info("getCodeValues start");
	  Collection list = new ArrayList();
	  String sql = "select NOT_ID as CODE, NOT_DESC as DESCRIPTION from NOTIFICATION_TEMPLATES order by NOT_ID asc";
	  
	  PreparedStatement ps = null;
	  ResultSet rs = null;
		
	  try {	  	
		ps = conn.prepareStatement(sql);
		rs = ps.executeQuery();				    		
		while (rs.next()) {
		  NameValuePair bean = new NameValuePair();
		  bean.setName(rs.getString("DESCRIPTION"));
		  bean.setValue(rs.getString("CODE"));
		  list.add(bean);
		}
	   
	  } catch (SQLException e) {
		  LOGGER.error(CodeHelper.getStackTrace(e));	
		  throw e; 
	  }finally{
		  this.closeResources(ps, rs);
	 	
	  }
	  LOGGER.info("getCodeValues end");
	  return (list);
	}// getCodeValues


	/**
	 * This method will return a subset of client types based on the specified client type code.
	 */
	public Collection getCodeValue(String codeValue) throws SQLException {
		
		LOGGER.info("getCodeValue start");
		Collection list = new ArrayList();
		String sql = "select NOT_ID as CODE, NOT_DESC as DESCRIPTION from NOTIFICATION_TEMPLATES WHERE NOT_ID = ?";
		  
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try {	  	
		  ps = conn.prepareStatement(sql);
		  ps.setString(1, codeValue);
		  rs = ps.executeQuery();				    		
		  while (rs.next()) {
			NameValuePair bean = new NameValuePair();
			bean.setName(rs.getString("DESCRIPTION"));
			bean.setValue(rs.getString("CODE"));
			list.add(bean);
		  }
		
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));	
			throw e; 
		}finally{
		 	this.closeResources(ps, rs);
		 	
		}
		LOGGER.info("getCodeValue end");
		return (list);
	}// getCodeValue

	
	public void insertNotificationTemplate(NotificationTemplateData notTempData) throws SQLException {
		
		LOGGER.info("insertNotificationTemplate start");
		String sql = "insert into NOTIFICATION_TEMPLATES (NOT_ID, NOT_DESC, NOT_SUBJ, NOT_EMAIL_CONTENT, NOT_NOTIFY_MOBILE_IND,NOT_MOBILE_CONTENT,CREATED_BY,CREATED_DATE) values(seq_notification_template.nextval,?,?,?,?,?,?,?)";
		
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try{
			
		pstmt = conn.prepareStatement(sql);
		String desc = notTempData.getNotificationDesc();		
		if (desc != null && !"".equals(desc)) {
			pstmt.setString(1,desc);
		} else {
			pstmt.setString(1,null);
		}
		
		String subj = notTempData.getSubject();
		if (subj != null && !"".equals(subj)) {
			pstmt.setString(2,subj);
		} else {
			pstmt.setString(2,null);
		}
		
		String emailContent = notTempData.getEmailNotificationContent();
		if (emailContent != null && !"".equals(emailContent)) {
			pstmt.setString(3,emailContent);
		} else {
			pstmt.setString(3,null);
		}
		
		boolean mobileInd = notTempData.isNotifyMobile();
		if (mobileInd) {
			pstmt.setString(4,IUMConstants.YES);
		} else {
			pstmt.setString(4,null);
		}
		
		String mobileContent = notTempData.getMobileNotificationContent();
		if (mobileContent != null && !"".equals(mobileContent)) {
			pstmt.setString(5,mobileContent);
		} else {
			pstmt.setString(5,null);
		}
		
		String createdBy = notTempData.getCreatedBy();
		if (createdBy != null && !"".equals(createdBy)) {
			pstmt.setString(6,createdBy);
		} else {
			pstmt.setString(6,null);
		}
		
		Date createdDate =notTempData.getCreationDate();
		if (createdDate != null) {
			pstmt.setTimestamp(7,DateHelper.sqlTimestamp(createdDate));
		}else {
			pstmt.setTimestamp(7,null);
		}
		pstmt.executeUpdate();	
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));	
			throw e; 
		}finally{
		 	this.closeResources(pstmt, null);
		 	
		}
		LOGGER.info("insertNotificationTemplate end");
	}
	
	public void updateNotificationTemplate(NotificationTemplateData notTempData) throws SQLException {
		
		LOGGER.info("updateNotificationTemplate start");
		String sql = "update NOTIFICATION_TEMPLATES SET NOT_SUBJ = ?, NOT_DESC = ?, NOT_EMAIL_CONTENT = ?, NOT_NOTIFY_MOBILE_IND = ? , UPDATED_BY = ?, UPDATED_DATE = ?, NOT_MOBILE_CONTENT = ? where NOT_ID = ?";
		String sql2 = "update NOTIFICATION_TEMPLATES SET NOT_SUBJ = ?, NOT_DESC = ?, NOT_EMAIL_CONTENT = ?, NOT_NOTIFY_MOBILE_IND = ? , UPDATED_BY = ?, UPDATED_DATE = ? where NOT_ID = ?";
		PreparedStatement pstmt = null;
		
		try{
		boolean mobileInd = notTempData.isNotifyMobile();		
		if (mobileInd) {
			pstmt = conn.prepareStatement(sql);
		} else {
			pstmt = conn.prepareStatement(sql2);
		}
		
		String desc = notTempData.getNotificationDesc();		
		if (desc != null && !"".equals(desc)) {
			pstmt.setString(2,desc);
		} else {
			pstmt.setString(2,null);
		}
	
		String subj = notTempData.getSubject();		
		if (subj != null && !"".equals(subj)) {
			pstmt.setString(1,subj);
		} else {
			pstmt.setString(1,null);
		}
			
		String emailContent = notTempData.getEmailNotificationContent();
		if (emailContent != null && !"".equals(emailContent)) {
			pstmt.setString(3,emailContent);
		} else {
			pstmt.setString(3,null);
		}
			
		if (mobileInd) {
			pstmt.setString(4,IUMConstants.YES);
		} else {
			pstmt.setString(4,null);
		}
				
		String updatedBy = notTempData.getUpdatedBy();		
		if (updatedBy != null && !"".equals(updatedBy)) {
			pstmt.setString(5,updatedBy);
		} else {
			pstmt.setString(5,null);
		}
	
		Date updatedDate = notTempData.getUpdateDate();		
		if (updatedDate != null) {
			pstmt.setTimestamp(6,DateHelper.sqlTimestamp(updatedDate));
		}else {
			pstmt.setTimestamp(6,null);
		}		
		if (mobileInd) {		
			String mobileContent = notTempData.getMobileNotificationContent();
			if (mobileContent != null && !"".equals(mobileContent)) {
				pstmt.setString(7,mobileContent);
			} else {
				pstmt.setString(7,null);
			}
			pstmt.setLong(8,notTempData.getNotificationId());
		}else {
			pstmt.setLong(7,notTempData.getNotificationId());
		}
				
		pstmt.executeUpdate();
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));	
			throw e; 
		}finally{
		 	this.closeResources(pstmt, null);
		 	
		}	
		LOGGER.info("updateNotificationTemplate end");
	}
	
	public void updateNotificationRecipients(String roles[], long notId, boolean withSMS,String lob) throws SQLException {
		
		LOGGER.info("updateNotificationRecipients start");
		String sqlDel = "delete from notification_recipients where not_id = ? and lob_code = ?";
		String sqlInsert = "insert into notification_recipients (NOT_ID,ROLE_ID,NOTIFICATION_TYPE,LOB_CODE) VALUES (?,?,?,?)";
		
		PreparedStatement pstmtDel = null;
		PreparedStatement pstmtIns = null;
		ResultSet rs = null;
		try{
		pstmtDel = conn.prepareStatement(sqlDel);
		pstmtIns = conn.prepareStatement(sqlInsert);
		if (roles != null) {		
			int len = roles.length;
			
			if (len > 0) {
				pstmtDel.setLong(1,notId);
				pstmtDel.setString(2,lob);				
				pstmtDel.executeUpdate();				
				for (int i = 0;i<len;i++) {					
					pstmtIns.setLong(1,notId);
					pstmtIns.setString(2,roles[i]);
					if (withSMS) {
						pstmtIns.setString(3,IUMConstants.BOTH_NOTIFICATION);
					} else {
						pstmtIns.setString(3,IUMConstants.EMAIL_ONLY);
					}
					pstmtIns.setString(4,lob);
					pstmtIns.executeUpdate();
					pstmtIns.clearParameters();
				}		
			}
		} else {
			pstmtDel.setLong(1,notId);
			pstmtDel.setString(2,lob);
			pstmtDel.executeUpdate();
		}
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));	
			throw e; 
		}finally{
		 	this.closeResources(pstmtDel, rs);
		 	this.closeResources(pstmtIns, rs);
		}	
		LOGGER.info("updateNotificationRecipients end");
	}	
	
	public void insertNotificationRecipients(String roles[], boolean withSMS, String lob) throws SQLException {				
		
		LOGGER.info("insertNotificationRecipients start");
		String sqlInsert = "insert into notification_recipients (NOT_ID,ROLE_ID,NOTIFICATION_TYPE,LOB_CODE) VALUES (seq_notification_template.currval,?,?,?)";
		PreparedStatement pstmtIns = null;
		ResultSet rs = null;
		
		try{
		pstmtIns = conn.prepareStatement(sqlInsert);
		if (roles != null) {		
			int len = roles.length;		
			if (len > 0) {			
				for (int i = 0;i<len;i++) {				
					pstmtIns.setString(1,roles[i]);
					if (withSMS) {
						pstmtIns.setString(2,IUMConstants.BOTH_NOTIFICATION);
					} else {
						pstmtIns.setString(2,IUMConstants.EMAIL_ONLY);
					}
					pstmtIns.setString(3,lob);
					pstmtIns.executeUpdate();
					pstmtIns.clearParameters();
				}
			
			}
		}	
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));	
			throw e; 
		}finally{
		 	this.closeResources(pstmtIns, rs);
		}	
		LOGGER.info("insertNotificationRecipients end");
	}	
	
	
	public NotificationTemplateData getNotificationTemplate(long notID) throws SQLException {
		
		LOGGER.info("getNotificationTemplate start");
		NotificationTemplateData notData = new NotificationTemplateData();
		String sql = "select NOT_DESC, NOT_SUBJ, NOT_EMAIL_CONTENT, NOT_NOTIFY_MOBILE_IND, NOT_MOBILE_CONTENT from notification_templates where NOT_ID = ?";
		
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		try{
		pstmt = conn.prepareStatement(sql);
		pstmt.setLong(1, notID);
		rs = pstmt.executeQuery();
		while (rs.next()) {
			notData.setNotificationDesc(rs.getString("NOT_DESC"));
			notData.setSubject(rs.getString("NOT_SUBJ"));
			notData.setEmailNotificationContent(rs.getString("NOT_EMAIL_CONTENT"));
			notData.setNotifyMobile(IUMConstants.YES.equals(rs.getString("NOT_NOTIFY_MOBILE_IND"))?true:false);
			notData.setMobileNotificationContent(rs.getString("NOT_MOBILE_CONTENT"));			
		}
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));	
			throw e; 
		}finally{
		 	this.closeResources(pstmt, rs);
		}	
		LOGGER.info("getNotificationTemplate end");
		return notData;
	}
	
	public String[] getRoles(long notId, String lob) throws SQLException {		
		
		LOGGER.info("getRoles start");
		String sqlCount = "select count(ROLE_ID) from notification_recipients where not_id = ? and lob_code = ? and (not_primary_ind != ? or not_primary_ind is null)";
		
		PreparedStatement pstmtCount = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		ResultSet rsCount = null;
		String roles[] = null;
		try{
		pstmtCount = conn.prepareStatement(sqlCount);
		pstmtCount.setLong(1,notId);
		pstmtCount.setString(2,lob);
		pstmtCount.setString(3,IUMConstants.YES);
		rsCount = pstmtCount.executeQuery();
		int rowCount = 0;
		if (rsCount.next()) {
			rowCount=rsCount.getInt(1);
		}
		
		String sql = "select ROLE_ID from notification_recipients where not_id = ? and lob_code = ? and (not_primary_ind != ? or not_primary_ind is null)";
		pstmt = conn.prepareStatement(sql);				
		pstmt.setLong(1,notId);
		pstmt.setString(2,lob);
		pstmt.setString(3,IUMConstants.YES);
		rs = pstmt.executeQuery();
		
		if (rowCount > 0) {
			roles = new String[rowCount];
		}
		int i = 0;
		while (rs.next()) {
			roles[i] = rs.getString("ROLE_ID");
			i++;
		}		
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));	
			throw e; 
		}finally{
		 	this.closeResources(pstmtCount, rsCount);
			this.closeResources(pstmt, rs);
		}	
		LOGGER.info("getRoles end");
		return roles;		
	}
			


}