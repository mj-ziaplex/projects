/**
 * JobScheduleDAO.java				Copyright (c) 2004 Pointwest Technologies Corp.
 * TODO DOCUMENT ME!
 * 
 * @author aditalo	 @date Jan 22, 2004
 */
package com.slocpi.ium.data.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.slocpi.ium.data.JobScheduleData;
import com.slocpi.ium.util.CodeHelper;

/**
 * TODO DOCUMENT ME!
 * 
 * @author aditalo		Copyright (c) 2004 Pointwest Technologies Corp.
 * @date Jan 22, 2004
 */
public class JobScheduleDAO {

	private static final Logger LOGGER = LoggerFactory.getLogger(JobScheduleDAO.class);

	private Connection conn = null;

	/**
	 * 
	 */
	public JobScheduleDAO(Connection connection) {
		super();
		this.conn = connection;
	}
	
	private void closeResources(PreparedStatement ps, ResultSet rs) throws SQLException{
		
		
		try {
			if (ps != null){
				ps.close();
			}
		}
		catch(SQLException e)
		{
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		}
		try {
			if (rs != null){
				rs.close();
			}			
		} catch (SQLException e){
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		}
		
	}
	
	public void saveJobSchedule(JobScheduleData data) throws SQLException {
		
		LOGGER.info("saveJobSchedule start");
		if (this.retrieveJobSchedule(data.getType()) == null) {
			this.insertJobSchedule(data);
		} else {
			this.updateJobSchedule(data);
		}
		LOGGER.info("saveJobSchedule end");
	}

	public JobScheduleData retrieveJobSchedule(String type) throws SQLException {
		
		LOGGER.info("retrieveJobSchedule start");
		JobScheduleData data = null;
		String sql = "SELECT SCHED_DESC, SCHED_TIME, CREATED_BY, CREATED_DATE, UPDATED_BY, UPDATED_DATE" +
					" FROM JOB_SCHEDULES WHERE SCHED_TYPE = ?";
		PreparedStatement ps = null;
		ResultSet rs = null;
		  
		try {
			ps = conn.prepareStatement(sql);
			ps.setString(1, type);
			rs = ps.executeQuery();
			if (rs.next()) {
				data = new JobScheduleData();
				data.setType(type);
				data.setDescription(rs.getString("SCHED_DESC"));
				data.setTime(rs.getTimestamp("SCHED_TIME"));
				data.setCreatedBy(rs.getString("CREATED_BY"));
				data.setCreatedDate(rs.getTimestamp("CREATED_DATE"));
				data.setUpdatedBy(rs.getString("UPDATED_BY"));
				data.setUpdatedDate(rs.getTimestamp("UPDATED_DATE"));
			}
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e; 
		}finally{
		 	this.closeResources(ps, rs);
		}		
		LOGGER.info("retrieveJobSchedule end");
		return data;
	}

	private void insertJobSchedule(JobScheduleData data) throws SQLException {
		
		LOGGER.info("insertJobSchedule start");
		String sql = "INSERT INTO JOB_SCHEDULES" +
					" (SCHED_TYPE, SCHED_DESC, SCHED_TIME, CREATED_BY, CREATED_DATE)" +
					" VALUES(?,?,?,?,?)";
		PreparedStatement ps = null;
		ResultSet rs = null;
		  
		try {
			ps = conn.prepareStatement(sql);
			ps.setString(1, data.getType());
			ps.setString(2, data.getDescription());
			ps.setTimestamp(3, data.getTime());
			ps.setString(4, data.getCreatedBy());
			ps.setTimestamp(5, data.getCreatedDate());
			ps.executeUpdate();
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e; 
		}finally{
		 	this.closeResources(ps, rs);
		}		
		
		LOGGER.info("insertJobSchedule end");
		
	}

	private void updateJobSchedule(JobScheduleData data) throws SQLException {
		
		LOGGER.info("updateJobSchedule start");
		String sql = "UPDATE JOB_SCHEDULES" +
					" SET SCHED_DESC = ?, SCHED_TIME = ?, UPDATED_BY = ?, UPDATED_DATE = ?" +
					" WHERE SCHED_TYPE = ?";

		PreparedStatement ps = null;
		ResultSet rs = null;
		  
		try {
			ps = conn.prepareStatement(sql);
			ps.setString(1, data.getDescription());
			ps.setTimestamp(2, data.getTime());
			ps.setString(3, data.getUpdatedBy());
			ps.setTimestamp(4, data.getUpdatedDate());
			ps.setString(5, data.getType());
			ps.executeUpdate();
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e; 
		}finally{
		 	this.closeResources(ps, rs);
		}		
		LOGGER.info("updateJobSchedule end");
		
	}

}
