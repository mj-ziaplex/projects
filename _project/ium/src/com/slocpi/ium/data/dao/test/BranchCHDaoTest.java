/*
 * Created on Jun 23, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.data.dao.test;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import junit.framework.TestCase;

import com.slocpi.ium.data.SunLifeOfficeData;
import com.slocpi.ium.data.dao.BranchCHDao;
import com.slocpi.ium.data.util.DataSourceProxy;
import com.slocpi.ium.util.NameValuePair;

/**
 * @author Jeffrey Canlas
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class BranchCHDaoTest extends TestCase {
	public Connection conn = null;

	public BranchCHDaoTest(String arg0) {
		super(arg0);
	}

	public void testBranCHDao() throws SQLException { 
	  
	  /*TEST FOR public Collection getgetSunLifeOffice()
		* Test connection and see whether SQL statement 
		* produced a result.  test whether the passed
		* value "ORTIGASCUST" show the same data as in the
		* database
	   */
	
	   conn = new DataSourceProxy().getConnection();
	   BranchCHDao dao = new BranchCHDao(conn);
	   SunLifeOfficeData list = new SunLifeOfficeData(); 
	   
	   list = dao.getSunLifeOffice("ORTIGASCUST");
		
	   String officename = list.getOfficeName();
	   String type = list.getOfficeType();
	   String addr1 = list.getAddr1();
	   String addr2 = list.getAddr2();
	   String addr3 = list.getAddr3();
	   String city = list.getCity();
	   String zip = list.getZipCode(); 	
	   assertTrue("test 1", officename.equals("ORTIGAS CUSTOMER CENTER"));
	   assertTrue("test 2", type.equals("S"));
	   assertTrue("test 3", addr1.equals("30/F ORIENT SQUARE BLDG."));
	   assertTrue("test 4", addr2.equals("EMERALD AVE."));
	   assertTrue("test 5", addr3.equals("ORTIGAS CENTER"));
	   assertTrue("test 6", city.equals("PASIG"));
	   assertTrue("test 7", zip.equals("1600"));	
	   
	   
	   /*TEST FOR public Collection getCodeValues()
	   * Test connection and see whether SQL statement 
	   * produced a result.  the entered sql statement
	   * is valid (tables and fields are existing)
	   * thus resulting arraylist should be 
	   * greater than zero. 
	  */
	 
	  ArrayList list2 = new ArrayList();
	  BranchCHDao dao2 = new BranchCHDao(conn);
	    
	  list2 = (ArrayList)dao.getCodeValues();
			
	  assertTrue("test 8", list2.size()>0);
	    
	  /*TEST FOR public Collection getCodeValues()
	   * test if the data in the dataset are the
	   * same as the one in the database. 
	   * Will check the first result and see 
	   * if it is the same from the database
	  */	
	
	  String desc = ((NameValuePair)list2.get(5)).getName();
	  String code = ((NameValuePair)list2.get(5)).getValue();
	  System.out.println(desc);
	  System.out.println(code);
	  assertTrue("test 8.1",desc.equals("SUN LIFE HEAD OFFICE"));
	  assertTrue("test8.2",code.equals("SUNLIFEHO"));
		
	  /*TEST FOR public Collection getCodeValue(String codeValue)
	   * Test connection and see whether SQL statement 
	   * produced a result.  the entered sql statement
	   * is valid (tables and fields are existing)
	   * thus resulting arraylist should be 
	   * greater than zero. 
	  */
		
	  ArrayList list3 = new ArrayList();
	  BranchCHDao dao3 = new BranchCHDao(conn);
	    
	  list3 = (ArrayList)dao2.getCodeValue("MAINCUST");
			
	  assertTrue("test 9", list3.size()>0);
	
	
	  /*TEST FOR public Collection getCodeValue(String codeValue))
	   * test if the data in the dataset are the
	   * same as the one in the database. 
	   * Will check the result where SLO_OFFICE_CODE = "MAINCUST" 
	   * and see whether it is the same in the database
	  */		
	
	  String desc2 = ((NameValuePair)list3.get(0)).getName();
	  String code2 = ((NameValuePair)list3.get(0)).getValue();
	  System.out.println(desc2);
	  System.out.println(code2);
	  assertTrue("test 9.1",desc2.equals("MAIN CUSTOMER CENTER"));
	  assertTrue("test 9.2",code2.equals("MAINCUST"));
	   
	   
	   
	   	   
	    
	  

		
		
	}

}
