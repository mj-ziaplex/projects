/**
 * ExamPlaceDAO.java				Copyright (c) 2004 Pointwest Technologies Corp.
 * TODO DOCUMENT ME!
 * 
 * @author aditalo	 @date Jan 28, 2004
 */
package com.slocpi.ium.data.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.slocpi.ium.data.ExaminationPlaceData;
import com.slocpi.ium.util.CodeHelper;

/**
 * TODO DOCUMENT ME!
 * 
 * @author aditalo		Copyright (c) 2004 Pointwest Technologies Corp.
 * @date Jan 28, 2004
 */
public class ExamPlaceDAO {

	private static final Logger LOGGER = LoggerFactory.getLogger(ExamPlaceDAO.class);

	private Connection conn = null;
	
	/**
	 * 
	 */
	public ExamPlaceDAO(Connection connection) {
		super();
		this.conn = connection;
	}
	
	private void closeResources(PreparedStatement ps, ResultSet rs) throws SQLException{
		
		
		try {
			if (ps != null){
				ps.close();
			}
		}
		catch(SQLException e)
		{
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		}
		try {
			if (rs != null){
				rs.close();
			}			
		} catch (SQLException e){
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		}
		
	}
	
	public ArrayList selectExamPlaces() throws SQLException {
		
		LOGGER.info("selectExamPlaces start");
		ArrayList list = new ArrayList();
		String sql = "SELECT EP_ID, EP_DESC FROM EXAMINATION_PLACES";
		PreparedStatement ps = null;
		ResultSet rs = null;
		try{
			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery();
			while (rs.next()) {
				ExaminationPlaceData data = new ExaminationPlaceData();
				data.setExaminationPlaceId(rs.getLong("EP_ID"));
				data.setExaminationPlaceDesc(rs.getString("EP_DESC"));
				list.add(data);
			}
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e; 
		}finally{
		 	this.closeResources(ps, rs);
		}			
		LOGGER.info("selectExamPlaces end");
		return list;
	}

}
