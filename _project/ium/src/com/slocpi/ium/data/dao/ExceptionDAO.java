/*
 * Created on Feb 21, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.data.dao;

import java.io.StringReader;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.slocpi.ium.ExceptionDetail;
import com.slocpi.ium.ExceptionLog;
import com.slocpi.ium.data.ExceptionData;
import com.slocpi.ium.data.ExceptionDetailsData;
import com.slocpi.ium.data.ExceptionFilterData;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.DateHelper;

/**
 * @author ypojol
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class ExceptionDAO extends BaseDAO {

	private static final Logger LOGGER = LoggerFactory.getLogger(ExceptionDAO.class);
	

	/**
	 *
	 */
	public ExceptionDAO() {}
	
	
	public void logError(ArrayList arr) throws SQLException{
		
		LOGGER.info("logError start 1");
		String stmt1 = "INSERT INTO EXCEPTION_LOG (EXCEPTION_ID, BATCH_ID, REFERENCE_NUM, XML_RECORD)" +
					  " VALUES (?, ?, ?, ?)";
		String stmt2 = "INSERT INTO EXCEPTION_BATCH (BATCH_ID, TIMESTAMP, INTERFACE, RECORD_TYPE, REQUEST, RESPONSE) VALUES (?, ?, ?, ?, ?, ?)";
		String stmt3 = "INSERT INTO EXCEPTION_DETAILS (EXCEPTION_ID, MESSAGE) VALUES (?, ?)";
		long batchID = 0;
		
		PreparedStatement ps2 = null;
		PreparedStatement ps1 = null;
		PreparedStatement ps3 = null;
		Connection conn = null;
		
		try {
			conn = getConnection();	
			conn.setAutoCommit(false);
			
			for(int i=0; i< arr.size(); i++){
			ExceptionLog ex = (ExceptionLog)arr.get(i);

			long exceptionID = getNextValue("SEQ_EXCEPTION_ID");
			Timestamp timestamp = new Timestamp(System.currentTimeMillis());
			if(i==0){
				batchID = getNextValue("SEQ_BATCH_ID");
				ps2 = conn.prepareStatement(stmt2);
				ps2.setLong(1, batchID);
				ps2.setTimestamp(2, timestamp);
				ps2.setString(3, ex.getSystemInterface());
				ps2.setString(4, ex.getRecordType());
				ps2.setString(5, ex.getRequestMsg());
				if (ex.getReply() != null){
					ps2.setCharacterStream(6,new StringReader(ex.getReply()),ex.getReply()==null?0:ex.getReply().length()); //change to character stream
				}else {
					ps2.setString(6, ex.getReply());
				}

				ps2.executeUpdate();
				conn.commit();
			}

			ps1 = conn.prepareStatement(stmt1);
			ps1.setLong(1, exceptionID);
			ps1.setLong(2, batchID);
			ps1.setString(3, ex.getReferenceNumber());
			if (ex.getXmlRecord() != null){
				ps1.setCharacterStream(4,new StringReader(ex.getXmlRecord()),ex.getXmlRecord()==null?0:ex.getXmlRecord().length()); //change to character stream
			}else {
				ps1.setString(4, ex.getXmlRecord());
			}
			ps1.executeUpdate();
			conn.commit();
			
			ps3 = conn.prepareStatement(stmt3);
			ArrayList details = ex.getDetails();
			for(int j=0; j<details.size(); j++){
				ExceptionDetail exD = (ExceptionDetail)details.get(j);
				ps3.setLong(1, exceptionID);
				if (exD.getMessage() != null){
					if (exD.getMessage().length() > 50){
						ps3.setString(2, exD.getMessage().substring(0,49));
					} else {
						ps3.setString(2, exD.getMessage());
					}
				} else {
					ps3.setString(2, exD.getMessage());
				}
				ps3.executeUpdate();
				conn.commit();
			}
		}
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e; 
		}finally{
		 	closeResources(ps1, null);
		 	closeResources(ps2, null);
		 	closeResources(ps3, null);
		 	closeConnection(conn);
		 	LOGGER.info("logError end 1");
		}	
	}

	public void logError(ExceptionLog ex) throws SQLException{
		
		LOGGER.info("logError start 1");
		String stmt1 = "INSERT INTO EXCEPTION_LOG (EXCEPTION_ID, BATCH_ID, REFERENCE_NUM, XML_RECORD)" +
					  " VALUES (?, ?, ?, ?)";
		String stmt2 = "INSERT INTO EXCEPTION_BATCH (BATCH_ID, TIMESTAMP, INTERFACE, RECORD_TYPE, REQUEST, RESPONSE) VALUES (?, ?, ?, ?, ?, ?)";
		String stmt3 = "INSERT INTO EXCEPTION_DETAILS (EXCEPTION_ID, MESSAGE) VALUES (?, ?)";

		long batchID = getNextValue("SEQ_BATCH_ID");
		long exceptionID = getNextValue("SEQ_EXCEPTION_ID");
		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		
		PreparedStatement ps1 = null;
		PreparedStatement ps2 = null;
		PreparedStatement ps3 = null;
		Connection conn = null;
		
		try {
			conn = getConnection();	
			conn.setAutoCommit(false);
			ps2 = conn.prepareStatement(stmt2);
			ps2.setLong(1, batchID);
			ps2.setTimestamp(2, timestamp);
			ps2.setString(3, ex.getSystemInterface());
			ps2.setString(4, ex.getRecordType());
			ps2.setString(5, ex.getRequestMsg());
		if (ex.getReply() != null){
			ps2.setCharacterStream(6,new StringReader(ex.getReply()),ex.getReply()==null?0:ex.getReply().length()); //change to character stream
		}
		else {
			ps2.setString(6, ex.getReply());
		}
		ps2.executeUpdate();
		conn.commit();
		ps1 = conn.prepareStatement(stmt1);
		ps1.setLong(1, exceptionID);
		ps1.setLong(2, batchID);
		ps1.setString(3, ex.getReferenceNumber());
		if (ex.getXmlRecord() != null){
			ps1.setCharacterStream(4,new StringReader(ex.getXmlRecord()),ex.getXmlRecord()==null?0:ex.getXmlRecord().length()); //change to character stream
		} else {
			ps1.setString(4, ex.getXmlRecord());
		}
		ps1.executeUpdate();
		conn.commit();

		ps3 = conn.prepareStatement(stmt3);
		ArrayList details = ex.getDetails();
		for(int j=0; j<details.size(); j++){
			ExceptionDetail exD = (ExceptionDetail)details.get(j);
			ps3.setLong(1, exceptionID);
			if (exD.getMessage() != null){
				if (exD.getMessage().length() > 50){
					ps3.setString(2, exD.getMessage().substring(0,49));
				} else {
					ps3.setString(2, exD.getMessage());
				}
			} else {
				ps3.setString(2, exD.getMessage());
			}
			ps3.executeUpdate();
			conn.commit();
		}
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e; 
		}finally{
		 	closeResources(ps1, null);
		 	closeResources(ps2, null);
		 	closeResources(ps3, null);
		 	closeConnection(conn);
		 	LOGGER.info("logError end 1");
		}			
	}

	private long getNextValue(String seqName) throws SQLException{
		
		LOGGER.info("getNextValue start");
		long retSeqNo=1;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection conn = null;
		
		try {
			conn = getConnection();	
			ps = conn.prepareStatement("select " + seqName + ".nextVal from dual");
			rs = ps.executeQuery();
			if(rs.next()){
				retSeqNo = rs.getLong(1);
			}
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e; 
		}finally{
		 	closeResources(conn, ps, rs);
		}	
		LOGGER.info("getNextValue end");
		return retSeqNo;
	}

	public ArrayList retrieveExceptions(ExceptionFilterData filter) throws SQLException{
		
		LOGGER.info("retrieveExceptions start");
		ArrayList result = new ArrayList();
		Date startDate = DateHelper.sqlDate(filter.getStartDate());
		Date endDate = DateHelper.sqlDate(filter.getEndDate());
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection conn = null;
		
		
			
		String type = filter.getRecordType();
		String interfaceType = filter.getInterfaceType();
		String tempSql = "SELECT " +
						 " EL.EXCEPTION_ID as EXCEPTION_ID," +
						 " EL.REFERENCE_NUM as REFERENCE_NUMBER," +
						 " EL.XML_RECORD as XML_RECORD," +
						 " to_char(EB.TIMESTAMP, 'ddMONyyyy hh:mi AM') as EXCEPTION_DATE," +
						 " EB.INTERFACE as INTERFACE," +
						 " EB.RECORD_TYPE as RECORD_TYPE" +
						 " FROM" +
						 " EXCEPTION_LOG EL," +
						 " EXCEPTION_BATCH EB" +
						 " WHERE" +
						 " EL.BATCH_ID = EB.BATCH_ID";

		StringBuffer sb = new StringBuffer(tempSql);
		if (type != null && !type.equals("")){
			sb.append(" AND EB.RECORD_TYPE = ?");
		}

		if (interfaceType != null && !interfaceType.equals("")){
			sb.append(" AND EB.INTERFACE = ?");
		}

		if (startDate != null && endDate != null){
			sb.append(" AND TRUNC(EB.TIMESTAMP) BETWEEN ? AND ?");
		}

		sb.append(" ORDER BY EL.EXCEPTION_ID");

		try {
			conn = getConnection();	
			ps = conn.prepareStatement(sb.toString());
			int index=1;
			if (type != null && !type.equals("")){
				ps.setString(index, type);
				index++;
			}

			if (interfaceType != null && !interfaceType.equals("")){
				ps.setString(index, interfaceType);
				index++;
			}

			if (startDate != null && endDate != null){
				ps.setDate(index, startDate);
				index++;
				ps.setDate(index, endDate);
				index++;
			}

			rs = ps.executeQuery();
			while(rs.next()){
				ExceptionData exData = new ExceptionData();
				exData.setXmlRecord(rs.getString("XML_RECORD"));
				exData.setExceptionId(rs.getLong("EXCEPTION_ID"));
				exData.setExceptionInterface(rs.getString("INTERFACE"));
				exData.setExceptionDate(rs.getString("EXCEPTION_DATE"));
				exData.setReferenceNumber(rs.getString("REFERENCE_NUMBER"));
				exData.setRecordType(rs.getString("RECORD_TYPE"));


				result.add(exData);
			}
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e; 
		}finally{
		 	closeResources(conn, ps, rs);
		}	
		LOGGER.info("retrieveExceptions end");
		return result;

	}

	public ArrayList retrieveExceptionDetails(long exceptionId) throws SQLException{
		
		LOGGER.info("retrieveExceptionDetails start");
		ArrayList result = new ArrayList();
		String sql = "SELECT " +
					 " ED.MESSAGE as MESSAGE," +
					 " ED.EXCEPTION_ID as ID," +
					 " EL.XML_RECORD as XML" +
					 " FROM" +
					 " EXCEPTION_LOG EL," +
					 " EXCEPTION_DETAILS ED" +
					 " WHERE" +
					 " EL.EXCEPTION_ID = ED.EXCEPTION_ID AND" +
					 " ED.EXCEPTION_ID = ?";

		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection conn = null;
		
		try {
			conn = getConnection();	
			ps = conn.prepareStatement(sql);
			ps.setLong(1, exceptionId);
			rs = ps.executeQuery();
			while(rs.next()){
				ExceptionDetailsData exData = new ExceptionDetailsData();
				exData.setXmlRec(rs.getString("XML"));
				exData.setId(rs.getLong("ID"));
				exData.setMessage(rs.getString("MESSAGE"));

				result.add(exData);

			}
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e; 
		}finally{
		 	closeResources(conn, ps, rs);
		}	
		LOGGER.info("retrieveExceptionDetails end");
		return result;

	}

	public ExceptionDetailsData retrieveExceptionDetails(final String referenceNumber) throws SQLException {
		
		LOGGER.info("retrieveExceptionDetails start");
		ExceptionDetailsData edd = new ExceptionDetailsData();
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT")
			.append(" C.EXCEPTION_ID as ID, ")
			.append(" C.MESSAGE as MSG, ")
			.append(" B.XML_RECORD as XML ")
			.append(" FROM")
			.append(" 	EXCEPTION_BATCH A, ")
			.append(" 	EXCEPTION_LOG B, ")
			.append(" 	EXCEPTION_DETAILS C ")
			.append("  WHERE")
			.append("	A.BATCH_ID = B.BATCH_ID ")
			.append("	AND B.EXCEPTION_ID = C.EXCEPTION_ID ")
			.append("	AND A.RECORD_TYPE = ? ")
			.append("	AND B.REFERENCE_NUM = ? ")
			.append("ORDER BY A.TIMESTAMP DESC ");

		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection conn = null;
		
		try {
			conn = getConnection();	
			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, "QPAS");
			ps.setString(2, referenceNumber);

			rs = ps.executeQuery();
			if (rs != null && rs.next()) {
				edd.setId(rs.getInt("ID"));
				edd.setMessage(rs.getString("MSG"));
				edd.setXmlRec(rs.getString("XML"));
			}

		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e; 
		}finally{
		 	closeResources(conn, ps, rs);
		}	
		LOGGER.info("retrieveExceptionDetails end");
		return edd;
	}
}
