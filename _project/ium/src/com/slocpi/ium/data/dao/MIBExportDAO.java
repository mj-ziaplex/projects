/*
 * Created on Mar 12, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.data.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.slocpi.ium.util.CodeHelper;

/**
 * @author ypojol
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class MIBExportDAO {

	private static final Logger LOGGER = LoggerFactory.getLogger(MIBExportDAO.class);

	private Connection conn;

	/**
	 * 
	 */
	public MIBExportDAO(Connection _conn){
		conn = _conn;
	}
	
	public void closeDB() {
		
		LOGGER.info("closeDB start");
		try {
			this.conn.close();
		} catch (Exception e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
		}
		LOGGER.info("closeDB end");
	}
	
	private void closeResources(PreparedStatement ps, ResultSet rs) throws SQLException{
		
		
		try {
			if (ps != null){
				ps.close();
			}
		}
		catch(SQLException e)
		{
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		}
		try {
			if (rs != null){
				rs.close();
			}			
		} catch (SQLException e){
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		}
		
	}
	
	public HashMap getMemberImpairment() throws SQLException{
		
		LOGGER.info("getMemberImpairment start");
		String token = ", ";
		HashMap map = new HashMap();
		ArrayList arrImps = new ArrayList();
		ArrayList arrDtls = new ArrayList();
		ArrayList arrTrans = new ArrayList();
		ArrayList arrImpId = new ArrayList();

		String sql = "SELECT A.CL_CLIENT_NUM mem_control_num, A.IMP_RELATIONSHIP num_let_cd, A.IMP_ORIGIN let_cd_dt, "  
					+ "A.MIB_IMPAIRMENT_CODE impairment_cd, A.UAR_REFERENCE_NUM policy_no, A.IMP_DATE underwriting_dt, "
					+ "A.IMP_IMPAIREMENT_ID imp_impairment_id, "
					+ "B.CL_LAST_NAME last_nm, B.CL_GIVEN_NAME first_nm, B.CL_MIDDLE_NAME middle_nm, "
					+ "B.CL_TITLE title, B.CL_SUFFIX suffix, B.CL_SEX sex, B.CL_BIRTH_DATE birth_dt, "
					+ "C.CONTROL_NO tran_ctrl_no, C.IMPAIRMENT_ID impairment_id, C.USER_CD user_cd, "
					+ "C.COMPANY_CD company_code, C.TRAN_CD tran_cd, C.TRAN_DT tran_dt, "
					+ "C.RECORD_AFFECTED rec_affected, C.NO_OF_HIT no_of_hit "
					+ "FROM IMPAIRMENTS A, CLIENTS B, TRANSACTION_DETAILS C " 
					+ "WHERE A.IMP_EXPORT_DATE IS NULL "
					+ "AND A.CL_CLIENT_NUM = B.CLIENT_ID "
					+ "AND A.IMP_IMPAIREMENT_ID = C.IMPAIRMENT_ID "
					+ "ORDER BY A.CL_CLIENT_NUM";

		PreparedStatement ps = null;
		ResultSet rs = null;
		  
		try{
				
			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery();
			String mem_ctrl_no = "";
			String impairment_id = "";
			while(rs.next()){
				StringBuffer sb = new StringBuffer();
				sb.append(checkNull(rs.getString("mem_control_num")));
				sb.append(token);
				sb.append(checkNull(rs.getString("num_let_cd")));
				sb.append(token);
				sb.append(checkNull(rs.getString("let_cd_dt")));
				sb.append(token);
				sb.append(checkNull(rs.getString("impairment_cd")));
				sb.append(token);
				sb.append("");//verify flag blank
				sb.append(token);
				sb.append("A");//Sunlife company code
				sb.append(token);
				sb.append(checkNull(rs.getString("policy_no")));
				sb.append(token);
				sb.append(checkNull(rs.getString("underwriting_dt")));
				sb.append(token);
				sb.append(checkNull(rs.getString("last_nm")));//last name
				sb.append(token);
				sb.append(checkNull(rs.getString("first_nm")));//given name
				sb.append(token);
				sb.append(checkNull(rs.getString("middle_nm")));//middle name
				sb.append(token);
				sb.append(checkNull(rs.getString("birth_dt")));//birthdate
				sb.append(token);
				sb.append("");//TODO verify where to get the birthplace
				sb.append(token);
				sb.append("1");//record status
				sb.append(token);
				sb.append("1");//status
				sb.append(token);
				sb.append("");//imp remarks
				arrImps.add(sb.toString());
				
				StringBuffer tranSb = new StringBuffer();
				tranSb.append(checkNull(rs.getString("tran_ctrl_no")));
				tranSb.append(token);
				tranSb.append(checkNull(rs.getString("user_cd")));
				tranSb.append(token);
				tranSb.append(checkNull(rs.getString("company_code")));
				tranSb.append(token);
				tranSb.append(checkNull(rs.getString("tran_cd")));
				tranSb.append(token);
				tranSb.append(checkNull(rs.getString("tran_dt")));
				tranSb.append(token);
				tranSb.append(checkNull(rs.getString("rec_affected")));
				tranSb.append(token);
				tranSb.append(checkNull(rs.getString("no_of_hit")));
				tranSb.append(token);
				arrTrans.add(tranSb.toString());
				
				if(!mem_ctrl_no.equalsIgnoreCase(rs.getString("mem_control_num"))){
					mem_ctrl_no = rs.getString("mem_control_num");
					StringBuffer memDetail = new StringBuffer();
					memDetail.append(mem_ctrl_no);
					memDetail.append(token);
					memDetail.append(checkNull(rs.getString("last_nm")));
					memDetail.append(token);
					memDetail.append(checkNull(rs.getString("first_nm")));
					memDetail.append(token);
					memDetail.append(checkNull(rs.getString("middle_nm")));
					memDetail.append(token);
					memDetail.append(checkNull(rs.getString("sex")));
					memDetail.append(token);
					memDetail.append(checkNull(rs.getString("birth_dt")));
					memDetail.append(token);
					memDetail.append("");//TODO verify where to get the birthplace
					memDetail.append(token);
					memDetail.append("");//TODO verify where to get the alias
					memDetail.append(token);
					memDetail.append(checkNull(rs.getString("title")));
					memDetail.append(token);
					memDetail.append(checkNull(rs.getString("suffix")));
					memDetail.append(token);
					memDetail.append("");//TODO verify where to get the maiden name
					memDetail.append(token);
					memDetail.append("");//TODO verify where to get the nationality
					memDetail.append(token);
					memDetail.append(checkNull(rs.getString("underwriting_dt")));
					memDetail.append(token);
					memDetail.append("A");
					memDetail.append(token);
					memDetail.append(checkNull(rs.getString("policy_no")));
					memDetail.append(token);
					memDetail.append("A");//TODO verify what is the value for action code <AR status code???>
					memDetail.append(token);
					memDetail.append("1");
					arrDtls.add(memDetail.toString());
				}
				
				if(!impairment_id.equalsIgnoreCase(rs.getString("imp_impairment_id"))){
					impairment_id = rs.getString("imp_impairment_id");
					arrImpId.add(impairment_id);
				}
			}
			
			map.put("MEMBER_DETAILS", arrDtls);
			map.put("MEMBER_IMPAIRMENTS", arrImps);
			map.put("TRANSACTION_DETAILS", arrTrans);
			map.put("IMP_IDS", arrImpId);
	    } catch (SQLException e) {
	    	LOGGER.error(CodeHelper.getStackTrace(e));
			throw e; 
		}finally{
		 	this.closeResources(ps, rs);
		}
		LOGGER.info("getMemberImpairment end");
		return map;
	}
	
	
	public void updateExportedImpairment(ArrayList list) throws SQLException{
		
		LOGGER.info("updateExportedImpairment start");
		Date date = new Date(System.currentTimeMillis());
		String sql = "UPDATE IMPAIRMENTS SET IMP_EXPORT_DATE = ? "  
					+ "WHERE IMP_IMPAIREMENT_ID = ? ";
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		  
		try{
					
			ps = conn.prepareStatement(sql);
			for(int i=0; i<list.size(); i++){
				String id =  (String)list.get(i);
				ps.setDate(1, date);
				ps.setString(2, id);
				ps.addBatch();
			}
			ps.executeBatch();
	    } catch (SQLException e) {
	    	LOGGER.error(CodeHelper.getStackTrace(e));
			throw e; 
		}finally{
		 	this.closeResources(ps, rs);
		}
		LOGGER.info("updateExportedImpairment end");
	}
	
	public Timestamp getJobSchedule(String job) throws SQLException{
		
		LOGGER.info("getJobSchedule start");
		Timestamp ts = null;
		String sql = "SELECT SCHED_TIME FROM JOB_SCHEDULES WHERE SCHED_TYPE = ?";  
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		  
		try{
					
			ps = conn.prepareStatement(sql);
			ps.setString(1, job);
			rs = ps.executeQuery();
			if(rs.next()){
				ts = rs.getTimestamp("SCHED_TIME");
			}
	    } catch (SQLException e) {
	    	LOGGER.error(CodeHelper.getStackTrace(e));
			throw e; 
		}finally{
		 	this.closeResources(ps, rs);
		}
		LOGGER.info("getJobSchedule end");
		return ts;
	}
	
	private String checkNull(String str){
		
		LOGGER.info("checkNull start");
		if(str==null){
			str = "";
		}
		LOGGER.info("checkNull end");
		return str.trim();
	}

}
