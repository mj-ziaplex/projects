/**
 * SectionCHDAO.java				Copyright (c) 2004 Pointwest Technologies Corp.
 * TODO DOCUMENT ME!
 * 
 * @author aditalo	 @date Jan 12, 2004
 */
package com.slocpi.ium.data.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.NameValuePair;

/**
 * TODO DOCUMENT ME!
 * 
 * @author aditalo		Copyright (c) 2004 Pointwest Technologies Corp.
 * @date Jan 12, 2004
 */
public class SectionCHDAO extends CodeHelperDao {

	private static final Logger LOGGER = LoggerFactory.getLogger(SectionCHDAO.class);
	private Connection conn = null;

	/**
	 * 
	 */
	public SectionCHDAO(Connection connection) {
		super();
		this.conn = connection;
	}

	/* (non-Javadoc)
	 * @see com.slocpi.ium.data.dao.CodeHelperDao#getCodeValues()
	 */
	public Collection getCodeValues() throws SQLException {
		
		LOGGER.info("getCodeValues start");
		Collection list = new ArrayList();
		String sql = "SELECT SEC_CODE, SEC_DESC FROM SECTIONS";
		PreparedStatement ps = null;
		ResultSet rs = null;
		try{
			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery();
			while (rs.next()) {
				NameValuePair bean = new NameValuePair();
				bean.setName(rs.getString("SEC_DESC"));
				bean.setValue(rs.getString("SEC_CODE"));
				list.add(bean);
			}
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e; 
		}
		LOGGER.info("getCodeValues end");
		return list;
	}

	/* (non-Javadoc)
	 * @see com.slocpi.ium.data.dao.CodeHelperDao#getCodeValue(java.lang.String)
	 */
	public Collection getCodeValue(String codeValue) throws SQLException {
		
		LOGGER.info("getCodeValue start");
		Collection list = new ArrayList();
		String sql = "SELECT SEC_CODE, SEC_DESC FROM SECTIONS WHERE SEC_CODE = ?";
		PreparedStatement ps = null;
		ResultSet rs = null;
		try{
			ps = conn.prepareStatement(sql);
			ps.setString(1, codeValue);
			rs = ps.executeQuery();
			while (rs.next()) {
				NameValuePair bean = new NameValuePair();
				bean.setName(rs.getString("SEC_DESC"));
				bean.setValue(rs.getString("SEC_CODE"));
				list.add(bean);
			}
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			  throw e; 
		}finally{
			  this.closeResources(ps, rs);
		}	
		LOGGER.info("getCodeValue end");
		return list;
	}
}
