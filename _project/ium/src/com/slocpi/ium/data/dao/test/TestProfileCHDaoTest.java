/*
 * Created on Jun 24, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.data.dao.test;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import junit.framework.TestCase;

import com.slocpi.ium.data.dao.TestProfileCHDao;
import com.slocpi.ium.data.util.DataSourceProxy;
import com.slocpi.ium.util.NameValuePair;

/**
 * @author Jeffrey Canlas
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class TestProfileCHDaoTest extends TestCase {
    public Connection conn = null;
	
	public TestProfileCHDaoTest(String arg0) {
		super(arg0);
	}

	public void testTestProfileCHDao() throws SQLException {
	
	   /*TEST FOR public Collection getCodeValues()
		* Test connection and see whether SQL statement 
		* produced a result.  the entered sql statement
		* is valid (tables and fields are existing)
		* thus resulting arraylist should be 
		* greater than zero. 
	   */
	
	   conn = new DataSourceProxy().getConnection();
	   ArrayList list = new ArrayList();
	   TestProfileCHDao dao = new TestProfileCHDao(conn);
	    
	   list = (ArrayList)dao.getCodeValues();
			
	   assertTrue("test 1", list.size()>0);
	    
	   /*TEST FOR public Collection getCodeValues()
		* test if the data in the dataset are the
		* same as the one in the database. 
		* Will check the first result and see 
		* if it is the same from the database
	   */	
	
	   String desc = ((NameValuePair)list.get(5)).getName();
	   String code = ((NameValuePair)list.get(5)).getValue();
	   System.out.println(desc);
	   System.out.println(code);
	   assertTrue("test 1.1",desc.equals("Albumin"));
	   assertTrue("test1.2",code.equals("49"));
	}
	 
	public void testTestProfileCHDao2() throws SQLException{
	   
	   /*TEST FOR public Collection getCodeValue(String codeValue)
		* Test connection and see whether SQL statement 
		* produced a result.  the entered sql statement
		* is valid (tables and fields are existing)
		* thus resulting arraylist should be 
		* greater than zero. 
	   */
	   conn = new DataSourceProxy().getConnection();
	   ArrayList list2 = new ArrayList();
	   TestProfileCHDao dao2 = new TestProfileCHDao(conn);
	    
	   list2 = (ArrayList)dao2.getCodeValue("54");
			
	   assertTrue("test 2", list2.size()>0);
	
	
	   /*TEST FOR public Collection getCodeValue(String codeValue))
		* test if the data in the dataset are the
		* same as the one in the database. 
		* Will check the result where TEST_ID = "54" 
		* and see whether it is the same in the database
	   */		
	
	   String desc2 = ((NameValuePair)list2.get(0)).getName();
	   String code2 = ((NameValuePair)list2.get(0)).getValue();
	   System.out.println(desc2);
	   System.out.println(code2);
	   assertTrue("test 2.1",desc2.equals("Breast ultrasound"));
	   assertTrue("test 2.2",code2.equals("54"));
	}

	public void testTestProfileCHDao3() throws SQLException{
	   
	   /*TEST FOR public Collection getCodeValue(String labId)
		* Test connection and see whether SQL statement 
		* produced a result.  the entered sql statement
		* is valid (tables and fields are existing)
		* thus resulting arraylist should be 
		* greater than zero. 
	   */
	   conn = new DataSourceProxy().getConnection();
	   ArrayList list3 = new ArrayList();
	   TestProfileCHDao dao3 = new TestProfileCHDao(conn);
	    
	   list3 = (ArrayList)dao3.getCodeValues("1");
			
	   assertTrue("test 3", list3.size()>0);
	
	
	   /*TEST FOR public Collection getCodeValue(String codeValue))
		* test if the data in the dataset are the
		* same as the one in the database. 
		* Will check the result where LAB_ID = "1"
		* (This will get TEST_ID = 44 as seen in table
		* LABORATORY_TESTS) 
		* and see whether it is the same in the database
	   */		
	
	   String desc3 = ((NameValuePair)list3.get(0)).getName();
	   String code3 = ((NameValuePair)list3.get(0)).getValue();
	   System.out.println(desc3);
	   System.out.println(code3);
	   assertTrue("test 3.1",desc3.equals("2-D ECHO"));
	   assertTrue("test 3.2",code3.equals("44"));
	}


}
