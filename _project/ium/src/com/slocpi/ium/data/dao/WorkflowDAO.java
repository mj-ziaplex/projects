
package com.slocpi.ium.data.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.slocpi.ium.IUMException;
import com.slocpi.ium.data.AssessmentRequestData;
import com.slocpi.ium.data.NotificationTemplateData;
import com.slocpi.ium.data.ProcessConfigData;
import com.slocpi.ium.data.UserProfileData;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.DateHelper;
import com.slocpi.ium.util.IUMConstants;
import com.slocpi.ium.workflow.NotificationRecipients;
import com.slocpi.ium.workflow.WorkflowItem;

/**
 * @author Barry Yu		Copyright (c) 2004 Pointwest Technologies Corp.
 * @date Jan 6, 2004
 */
public class WorkflowDAO extends BaseDAO{
	private static final Logger LOGGER = LoggerFactory.getLogger(WorkflowDAO.class);
	
	public WorkflowDAO() {}
	
	public String[] tmpGetNotificationInfo (WorkflowItem wfi) throws SQLException {
		
		LOGGER.info("tmpGetNotificationInfo start");
		String notification[] = null;
		String sql1 = "select PROC_NOTIFICATION_IND, NOT_ID, EVNT_ID from PROCESS_CONFIGURATIONS ";
		String sql2 = "select PROC_NOTIFICATION_IND, NOT_ID, EVNT_ID from PROCESS_CONFIGURATIONS where LOB_CODE = ? and PROC_CODE = ? and " +
					 "EVNT_FROM_STATUS IS NULL and EVNT_TO_STATUS = ?";
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Connection conn = null;
		
		try{
			conn = getConnection();
		if (wfi.getPreviousStatus() != null) {
			
			pstmt = conn.prepareStatement(sql1);
		} else {
			pstmt = conn.prepareStatement(sql2);
			pstmt.setString(1,wfi.getLOB());
			pstmt.setString(2,wfi.getType());
			pstmt.setLong(3,Long.parseLong(wfi.getCurrentStatus()));
		}
		rs = pstmt.executeQuery();

		if (rs.next()) {
			notification = new String[3];
			notification[0] = rs.getString(1);
			notification[1] = rs.getString(2);
			notification[2] = rs.getString(3);
		}
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			  throw e; 
		}finally{
			closeResources(conn, pstmt, rs);
		}		
		LOGGER.info("tmpGetNotificationInfo end");
		return notification;
	}
	
	public String[] getNotificationInfo (WorkflowItem wfi) throws SQLException {
		
		LOGGER.info("getNotificationInfo start");
		String notification[] = null;
		String sql1 = "select PROC_NOTIFICATION_IND, NOT_ID, EVNT_ID from PROCESS_CONFIGURATIONS where LOB_CODE = ? and PROC_CODE = ? and " +
					 "EVNT_FROM_STATUS = ? and EVNT_TO_STATUS = ?";
		String sql2 = "select PROC_NOTIFICATION_IND, NOT_ID, EVNT_ID from PROCESS_CONFIGURATIONS where LOB_CODE = ? and PROC_CODE = ? and " +
					 "EVNT_FROM_STATUS IS NULL and EVNT_TO_STATUS = ?";
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Connection conn = null;
		
		try{
			conn = getConnection();
		if (wfi.getPreviousStatus() != null) {
			pstmt = conn.prepareStatement(sql1);
			pstmt.setString(1,wfi.getLOB());
			pstmt.setString(2,wfi.getType());
			pstmt.setLong(3,Long.parseLong(wfi.getPreviousStatus()));
			pstmt.setLong(4,Long.parseLong(wfi.getCurrentStatus()));
		} else {
			pstmt = conn.prepareStatement(sql2);
			pstmt.setString(1,wfi.getLOB());
			pstmt.setString(2,wfi.getType());
			pstmt.setLong(3,Long.parseLong(wfi.getCurrentStatus()));
		}
		rs = pstmt.executeQuery();

		if (rs.next()) {
			notification = new String[3];
			notification[0] = rs.getString(1);
			notification[1] = rs.getString(2);
			notification[2] = rs.getString(3);
		}
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			  throw e; 
		}finally{
			closeResources(conn, pstmt, rs);
		}		
		LOGGER.info("getNotificationInfo end");
		return notification;
	}


	public NotificationTemplateData getNotificationTemplate(String notificationID) throws SQLException {
		
		LOGGER.info("getNotificationTemplate start");
		String sqlRecipientRoles = "select NOT_EMAIL_CONTENT, NOT_SENDER, NOT_NOTIFY_MOBILE_IND, NOT_MOBILE_CONTENT, NOT_DESC,NOT_SUBJ " +
					"from NOTIFICATION_TEMPLATES where NOT_ID = ?";
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		NotificationTemplateData notificationData = null;
		Connection conn = null;
		
		try{
			
			conn = getConnection();
			pstmt = conn.prepareStatement(sqlRecipientRoles);
			pstmt.setString(1,notificationID);
			rs = pstmt.executeQuery();
			
			notificationData = new NotificationTemplateData();
			
			if (rs.next()) {
				notificationData = new NotificationTemplateData();
				notificationData.setEmailNotificationContent(rs.getString(1));
				notificationData.setSender(rs.getString(2));
				notificationData.setNotificationDesc(rs.getString(5));
				notificationData.setSubject(rs.getString(6));
				if (IUMConstants.YES.equalsIgnoreCase(rs.getString(3))) {
					notificationData.setNotifyMobile(true);
					notificationData.setMobileNotificationContent(rs.getString(4));
				} else {
					notificationData.setNotifyMobile(false);
				}
			}
			
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			  throw e; 
		}finally{
			closeResources(conn, pstmt, rs);
		}		
		LOGGER.info("getNotificationTemplate end");
		return notificationData;
	}


	/**
	 * @param recipient
	 * @return
	 */
	public NotificationRecipients getNotificationRecipients(ArrayList recipient, String eventID, String notificationID,String lobCode) throws SQLException {
		
		LOGGER.info("getNotificationRecipients start");
		ArrayList emails  = new ArrayList();
		ArrayList mobiles = new ArrayList();
		String notificationType = IUMConstants.EMAIL_ONLY;
		String sql2 = "select NOTIFICATION_TYPE from NOTIFICATION_RECIPIENTS where NOT_ID = ? and LOB_CODE = ?";
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		PreparedStatement pstmt1 = null;
		Statement stmtMgr = null;
		ResultSet rs1 = null;
		ResultSet rsMgr = null;
		NotificationRecipients noticeRecipients = new NotificationRecipients();
		Connection conn = null;
		
		try{
			
			conn = getConnection();
			pstmt1 = conn.prepareStatement(sql2);
			pstmt1.setString(1,notificationID);
			pstmt1.setString(2,lobCode);
			rs1 = pstmt1.executeQuery();
			if (rs1.next()) {
				notificationType = rs1.getString(1);
			}
			rs1.close();
			StringBuffer recipients =  new StringBuffer();
			Iterator it = recipient.iterator();
			while (it.hasNext()) {
				recipients.append("'"+it.next()+"'");
				recipients.append(", ");
			}
			String setRecipients = recipients.length()>0?recipients.substring(0,recipients.length()-2):"''";
			
			String sql = "select USR_EMAIL_ADDRESS, USR_MOBILE_NUM from USERS T1, USER_PROCESS_PREFERENCES T2 where " +
						  "T1.USER_ID IN ("+ setRecipients+ ") and T1.USR_NOTIFICATION_IND = ? and T1.USER_ID = T2.USER_ID and T2.EVNT_ID = ? and " +
						  "T2.UPP_NOTIFICATION_IND = ? ";
	
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1,IUMConstants.YES);
			pstmt.setString(2,eventID);
			pstmt.setString(3,IUMConstants.YES);
			rs = pstmt.executeQuery();
		while (rs.next()) {
			if (IUMConstants.EMAIL_ONLY.equalsIgnoreCase(notificationType)) {
				if (rs.getString(1) != null) {
					emails.add(rs.getString(1));
				}
			} else if (IUMConstants.SMS_ONLY.equalsIgnoreCase(notificationType)) {
				if (rs.getString(2) != null) {
					mobiles.add(rs.getString(2));
				}
			}else if (IUMConstants.BOTH_NOTIFICATION.equalsIgnoreCase(notificationType)) {
				if (rs.getString(1) != null) {
					emails.add(rs.getString(1));
				}
				if (rs.getString(2) != null) {
					mobiles.add(rs.getString(2));
				}
			}
		}
		
		
		String sqlMgr = "select distinct AGT_MGR_ID from USERS where USER_ID IN ("+ setRecipients+ ")";
		stmtMgr = conn.createStatement();
		rsMgr = stmtMgr.executeQuery(sqlMgr);
		String uid = null;
		
		
		
		while (rsMgr.next()) {
			
			uid = rsMgr.getString("AGT_MGR_ID");
		
			if(uid!=null && !uid.equals("")){
				  sql = "select USR_EMAIL_ADDRESS, USR_MOBILE_NUM from USERS T1, USER_PROCESS_PREFERENCES T2 where " +
				  "T1.USER_ID = '"+ uid+ "' and T1.USR_NOTIFICATION_IND = ? and T1.USER_ID = T2.USER_ID and T2.EVNT_ID = ? and " +
				  "T2.UPP_NOTIFICATION_IND = ? ";
	
				pstmt = conn.prepareStatement(sql);
				pstmt.setString(1,IUMConstants.YES);
				pstmt.setString(2,eventID);
				pstmt.setString(3,IUMConstants.YES);
				rs = pstmt.executeQuery();
				
				while (rs.next()) {
					if (rs.getString(1) != null) {
						emails.add(rs.getString(1));
					}
					if (rs.getString(2) != null) {
						mobiles.add(rs.getString(2));
					}
				}
			
			}
		}
		
		noticeRecipients.setEmails(emails);
		noticeRecipients.setMobiles(mobiles);
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			  throw e; 
		}finally{
			  closeResources(pstmt , rs);
			  closeResources(pstmt1 , rs1);
			  closeResources(null , rsMgr);
			  if(stmtMgr!=null){
				  closeResources(stmtMgr, null);
			  }
			 closeConnection(conn);
		}	
		LOGGER.info("getNotificationRecipients end");
		return noticeRecipients;
	}


	public UserProfileData getSender(String userID) throws SQLException {
		
		LOGGER.info("getSender start");
		UserProfileData userProfile = new UserProfileData();
		String sql = "select Y1.USR_EMAIL_ADDRESS, Y1.USR_LAST_NAME, Y1.USR_FIRST_NAME, Y2.SEC_DESC from USERS Y1, SECTIONS Y2 WHERE " +
					 "USER_ID = ? and Y1.SEC_ID=Y2.SEC_CODE";		
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Connection conn = null;
		
		try{
			
			conn = getConnection();
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1,userID);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				userProfile.setEmailAddr(rs.getString(1));
				userProfile.setLastName(rs.getString(2));
				userProfile.setFirstName(rs.getString(3));
				userProfile.setSectionDesc(rs.getString(4));
			}
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			  throw e; 
		}finally{
			closeResources(conn, pstmt, rs);
			 
		}	
		LOGGER.info("getSender end");
		return userProfile;
	}

	public ArrayList getSecondaryRecipients(
			final String notificationID,
			final String lobCode,
			final String branchCode,
			final String referenceNumber) throws SQLException {
		
		LOGGER.info("getSecondaryRecipients start");
		ArrayList recipients = new ArrayList();
		String sqlRoles = "SELECT role_id FROM notification_recipients WHERE not_id = ? AND lob_code = ? AND (not_primary_ind != ? OR not_primary_ind IS NULL)";
		String sqlUsers = "SELECT T1.USER_ID FROM USERS T1, USER_ROLES T2 WHERE " +
						  "T1.SLO_ID = ? AND T2.ROLE_CODE= ? AND T1.USER_ID = T2.USER_ID";
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		PreparedStatement pstmt2 = null;
		ResultSet rs2 = null;
		Connection conn = null;
		
		try{	
			conn = getConnection();
			pstmt = conn.prepareStatement(sqlRoles);
			pstmt.setLong(1,Long.parseLong(notificationID));
			pstmt.setString(2,lobCode);
			pstmt.setString(3,IUMConstants.YES);
	
			rs = pstmt.executeQuery();
			pstmt2 = conn.prepareStatement(sqlUsers);
			String roleID = null;
	
			while (rs.next()) {
				
				roleID = rs.getString(1);
				
				//	Fix for CR#10136: Retrieved assigned agent using reference number if agent is configured in event
				if (roleID.equalsIgnoreCase(IUMConstants.ROLES_AGENTS)) {
					
					String agentUserID = getAssignedAgent(referenceNumber);
					
					if (agentUserID != "") {
						recipients.add(agentUserID);
					}
				} else {
					pstmt2.setString(1,branchCode);
					pstmt2.setString(2,roleID);
	
					rs2 = pstmt2.executeQuery();
					while (rs2.next()) {
						
						recipients.add(rs2.getString(1));
					}
					
					pstmt2.clearParameters();
					rs2.close();
				}
			}
			
			
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		} finally {
			closeResources(pstmt , rs);
			closeResources(pstmt2 , rs2);
			closeConnection(conn);
		}
		LOGGER.info("getSecondaryRecipients end");
		return recipients;
	}

	/**
	 * Retrieves the assigned agent to be notified. (Fix for CR#10136)
	 * 
	 * @param referenceNumber - Assessment Request identifying which agent to retrieve
	 * @return String - Agent User Id.
	 */
	private String getAssignedAgent(final String referenceNumber) throws SQLException {
		
		LOGGER.info("getAssignedAgent start");
		String agentID = "";
		AssessmentRequestDAO daoAR = new AssessmentRequestDAO();
		AssessmentRequestData dataAR = null;
		
		if (referenceNumber != "") {
			
			dataAR = daoAR.retrieveRequestDetail(referenceNumber);
			if (dataAR != null) {
				agentID = dataAR.getAgent().getUserId();
			}
		}
		LOGGER.info("getAssignedAgent end");
		return agentID;
	}

	public ArrayList retrieveProcessConfig(String procCode, String lob) throws SQLException {
		
		LOGGER.info("retrieveProcessConfig start");
		ArrayList processConfigs = new ArrayList();
		String sql = "select evnt_id, proc_code, evnt_name, not_id, evnt_from_status, evnt_to_status, lob_code, proc_notification_ind, created_date, created_by, updated_by, updated_date" +
					" from process_configurations where proc_code = ? and lob_code = ? order by evnt_name";
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Connection conn = null;
		
		try{
			conn = getConnection();
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1,procCode);
			pstmt.setString(2,lob);
			rs = pstmt.executeQuery();
		while (rs.next()) {
			ProcessConfigData processConfData = new ProcessConfigData();
			processConfData.setEventId(rs.getLong("evnt_id"));
			processConfData.setProcessCode(rs.getString("proc_code"));
			processConfData.setEventName(rs.getString("evnt_name"));
			processConfData.setNotification(rs.getLong("not_id"));
			processConfData.setFromStatus(rs.getLong("evnt_from_status"));
			processConfData.setToStatus(rs.getLong("evnt_to_status"));
			processConfData.setLOB(rs.getString("lob_code"));
			processConfData.setNotificationInd("1".equals(rs.getString("proc_notification_ind"))?true:false);
			processConfData.setCreateDate(rs.getTimestamp("created_date"));
			processConfData.setCreatedBy(rs.getString("created_by"));
			processConfData.setUpdateDate(rs.getTimestamp("updated_date"));
			processConfData.setUpdatedBy(rs.getString("updated_by"));
			processConfigs.add(processConfData);
		}
		
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			  throw e; 
		}finally{
			  closeResources(conn, pstmt , rs);
		}	
		LOGGER.info("retrieveProcessConfig end");
		return processConfigs;
	}

	public void insertProcessConfig(ProcessConfigData procConfData) throws SQLException, IUMException {
		
		LOGGER.info("insertProcessConfig start");
		if (checkForDuplicates(procConfData)) {
			throw new IUMException("Duplicate Process Configuration.");
		}
		String sql = "INSERT INTO process_configurations (evnt_id,evnt_name, evnt_from_status, evnt_to_status, not_id, proc_notification_ind, lob_code, created_date, created_by,proc_code) VALUES (seq_process_configuration.nextval,?,?,?,?,?,?,?,?,?)";
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Connection conn = null;
		
		try {
			conn = getConnection();	
			conn.setAutoCommit(false);
			pstmt = conn.prepareStatement(sql);

			String eventName = procConfData.getEventName();
		if (eventName != null) {
			pstmt.setString(1,eventName);
		}else {
			pstmt.setString(1,null);
		}

		long fromStatus = procConfData.getFromStatus();
		if (fromStatus != 0) {
			pstmt.setLong(2,fromStatus);
		} else {
			pstmt.setString(2,null);
		}

		long toStatus = procConfData.getToStatus();
		if (toStatus != 0) {
			pstmt.setLong(3,toStatus);
		} else {
			pstmt.setString(3,null);
		}

		long notId = procConfData.getNotification();
		if (notId != 0) {
			pstmt.setLong(4,notId);
		} else {
			pstmt.setString(4,null);
		}

		boolean notificationInd = procConfData.isNotificationInd();
		if (notificationInd) {
			pstmt.setString(5,IUMConstants.YES);
		}else {
			pstmt.setString(5,null);
		}

		String lobCode = procConfData.getLOB();
		if (lobCode != null) {
			pstmt.setString(6,lobCode);
		}else {
			pstmt.setString(6,null);
		}

		Date createdDate = procConfData.getCreateDate();
		if (createdDate != null) {
			pstmt.setTimestamp(7,DateHelper.sqlTimestamp(createdDate));
		}else {
			pstmt.setTimestamp(7,null);
		}

		String createdBy = procConfData.getCreatedBy();
		if (createdBy != null) {
			pstmt.setString(8,createdBy);
		}else {
			pstmt.setString(8,null);
		}

		String procCode = procConfData.getProcessCode();
		if (procCode != null) {
			pstmt.setString(9,procCode);
		}else {
			pstmt.setString(9,null);
		}
		pstmt.executeUpdate();
		conn.commit();
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			if(conn != null){
				conn.rollback();
			}
			 throw e; 
		}finally{
			  closeResources(conn, pstmt , rs);
		}	
		LOGGER.info("insertProcessConfig end");
	}

	public void updateProcessConfig(ProcessConfigData procConfData) throws SQLException, IUMException {
		
		LOGGER.info("updateProcessConfig start");
		if (checkForDuplicatesUpdates(procConfData)) {
			throw new IUMException("Duplicate Process Configuration.");
		}
		String sql = "UPDATE process_configurations set evnt_name = ?, evnt_from_status = ?, evnt_to_status = ?, proc_notification_ind = ?, not_id = ?, updated_date = ?, updated_by = ? where evnt_id = ?";
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Connection conn = null;
		
		try {
			conn = getConnection();	
			conn.setAutoCommit(false);
			pstmt = conn.prepareStatement(sql);

			String eventName = procConfData.getEventName();
		if (eventName != null) {
			pstmt.setString(1,eventName);
		}else {
			pstmt.setString(1,null);
		}

		long fromStatus = procConfData.getFromStatus();
		if (fromStatus != 0) {
			pstmt.setLong(2,fromStatus);
		} else {
			pstmt.setString(2,null);
		}

		long toStatus = procConfData.getToStatus();
		if (toStatus != 0) {
			pstmt.setLong(3,toStatus);
		} else {
			pstmt.setString(3,null);
		}

		boolean notificationInd = procConfData.isNotificationInd();
		if (notificationInd) {
			pstmt.setString(4,IUMConstants.YES);
		}else {
			pstmt.setString(4,null);
		}

		long notId = procConfData.getNotification();
		if (notId != 0) {
			pstmt.setLong(5,notId);
		} else {
			pstmt.setString(5,null);
		}

		Date updatedDate = procConfData.getUpdateDate();
		if (updatedDate != null) {
			pstmt.setTimestamp(6,DateHelper.sqlTimestamp(updatedDate));
		}else {
			pstmt.setTimestamp(6,null);
		}

		String updatedBy = procConfData.getUpdatedBy();
		if (updatedBy != null) {
			pstmt.setString(7,updatedBy);
		}else {
			pstmt.setString(7,null);
		}
		pstmt.setLong(8,procConfData.getEventId());
		pstmt.executeUpdate();
		conn.commit();
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			if(conn != null){
				conn.rollback();
			}
			throw e; 
		}finally{
			  closeResources(conn, pstmt , rs);
		}	
		LOGGER.info("updateProcessConfig end");
	}

	public ProcessConfigData getProcessConfig(String eventId) throws SQLException {
		
		LOGGER.info("getProcessConfig start");
		String sql = "select evnt_id, proc_code, evnt_name, not_id, evnt_from_status, evnt_to_status, lob_code, proc_notification_ind, created_date, created_by, updated_by, updated_date" +
					" from process_configurations where evnt_id = ?";
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		ProcessConfigData processConfData = new ProcessConfigData();
		Connection conn = null;
		
		try {
			conn = getConnection();
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1,eventId);
			rs = pstmt.executeQuery();
		
		if (rs.next()) {
			processConfData.setEventId(rs.getLong("evnt_id"));
			processConfData.setProcessCode(rs.getString("proc_code"));
			processConfData.setEventName(rs.getString("evnt_name"));
			processConfData.setNotification(rs.getLong("not_id"));
			processConfData.setFromStatus(rs.getLong("evnt_from_status"));
			processConfData.setToStatus(rs.getLong("evnt_to_status"));
			processConfData.setLOB(rs.getString("lob_code"));
			processConfData.setNotificationInd("1".equals(rs.getString("proc_notification_ind"))?true:false);
			processConfData.setCreateDate(rs.getTimestamp("created_date"));
			processConfData.setCreatedBy(rs.getString("created_by"));
			processConfData.setUpdateDate(rs.getTimestamp("updated_date"));
			processConfData.setUpdatedBy(rs.getString("updated_by"));

		}
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			  throw e; 
		}finally{
			  closeResources(conn, pstmt , rs);
		}	
		LOGGER.info("getProcessConfig end");
		return processConfData;
	}

	public void insertLOBStatus (String lob, String statusId) throws NumberFormatException, SQLException {
		
		LOGGER.info("insertLOBStatus start");
		String sql = "insert into lob_status (lob_code,stat_id) values (?,?)";
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Connection conn = null;
		try {
			conn = getConnection();	
			conn.setAutoCommit(false);
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1,lob);
			pstmt.setLong(2,Long.parseLong(statusId));
			pstmt.executeUpdate();
			conn.commit();
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			if(conn != null){
				conn.rollback();
			}
			throw e; 
		}finally{
			  closeResources(conn, pstmt , rs);
		}	
		LOGGER.info("insertLOBStatus end");
	}

	private boolean checkForDuplicates(ProcessConfigData procConfData) throws SQLException {
		
		LOGGER.info("checkForDuplicates start");
		boolean isDuplicate = false;
		long fromStatus = procConfData.getFromStatus();
		long toStatus = procConfData.getToStatus();
		StringBuffer checkSql = new StringBuffer("select count(evnt_id) from process_configurations where evnt_from_status");

		if (fromStatus == 0) {
			checkSql.append(" is null ");
		}else {
			checkSql.append(" = ");
			checkSql.append(fromStatus);
		}
		checkSql.append (" and evnt_to_status ");
		if (toStatus == 0) {
			checkSql.append(" is null ");
		}else {
			checkSql.append(" = ");
			checkSql.append(toStatus);
		}
		checkSql.append(" and lob_code = ? and proc_code = ?");
		PreparedStatement pstmt1  = null;
		ResultSet rsCheck = null;
		Connection conn = null;
		try {
			conn = getConnection();	
			pstmt1 = conn.prepareStatement(checkSql.toString());
			pstmt1.setString(1,procConfData.getLOB());
			pstmt1.setString(2, procConfData.getProcessCode());
			rsCheck = pstmt1.executeQuery();
		if (rsCheck.next()) {
			if (rsCheck.getInt(1) != 0) {
				isDuplicate = true;
			}
		}
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			  throw e; 
		}finally{
			  closeResources(conn, pstmt1 , rsCheck);
		}	
		LOGGER.info("checkForDuplicates end");
		return isDuplicate;
	}

	private boolean checkForDuplicatesUpdates(ProcessConfigData procConfData) throws SQLException {
		
		LOGGER.info("checkForDuplicatesUpdates start");
		boolean isDuplicate = false;
		long fromStatus = procConfData.getFromStatus();
		long toStatus = procConfData.getToStatus();
		StringBuffer checkSql = new StringBuffer("select count(evnt_id) from process_configurations where evnt_from_status");

		if (fromStatus == 0) {
			checkSql.append(" is null ");
		}else {
			checkSql.append(" = ");
			checkSql.append(fromStatus);
		}
		checkSql.append (" and evnt_to_status ");
		if (toStatus == 0) {
			checkSql.append(" is null ");
		}else {
			checkSql.append(" = ");
			checkSql.append(toStatus);
		}
		checkSql.append(" and lob_code = ? and proc_code = ? and evnt_id != ?");
		PreparedStatement pstmt1 = null;
		ResultSet rsCheck = null;
		Connection conn = null;
		
		try {
			conn = getConnection();	
			pstmt1 = conn.prepareStatement(checkSql.toString());
			pstmt1.setString(1,procConfData.getLOB());
			pstmt1.setString(2, procConfData.getProcessCode());
			pstmt1.setLong(3,procConfData.getEventId());
			rsCheck = pstmt1.executeQuery();
		if (rsCheck.next()) {
			if (rsCheck.getInt(1) != 0) {
				isDuplicate = true;
			}
		}
		rsCheck.close();
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			  throw e; 
		}finally{
			  closeResources(conn, pstmt1, rsCheck);
		}	
		LOGGER.info("checkForDuplicatesUpdates end");
		return isDuplicate;
	}

	public void saveAttachmentHelper(String referenceNumber, long reqId) throws SQLException{
		
		LOGGER.info("saveAttachmentHelper start");
		String sql = "Insert into attachments(uar_reference_num, requirement_id) values (?,?)";
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection conn = null;
		
		try {
			conn = getConnection();	
			conn.setAutoCommit(false);
			ps = conn.prepareStatement(sql);
			ps.setString(1, referenceNumber);
			ps.setLong(2, reqId);

			ps.executeUpdate();
			conn.commit();
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			if(conn != null){
				conn.rollback();
			}
			  throw e; 
		}finally{
			  closeResources(conn, ps, rs);
		}
		LOGGER.info("saveAttachmentHelper end");
	}

	public ArrayList retrieveAttachmentHelper(final String referenceNumber) throws SQLException{
		
		LOGGER.info("retrieveAttachmentHelper start");
		ArrayList list = new ArrayList();
		String sql = "SELECT uar_reference_num, requirement_id FROM attachments WHERE uar_reference_num = ?";
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection conn = null;
		
		try {
			conn = getConnection();	
			ps = conn.prepareStatement(sql);
			ps.setString(1, referenceNumber);
			rs = ps.executeQuery();

			while (rs.next()){
				String reqId = String.valueOf(rs.getLong("requirement_id"));
				list.add(reqId);
			}
			
			return list;
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			  throw e; 
		} finally {
			  closeResources(conn, ps, rs);
		      LOGGER.info("retrieveAttachmentHelper end");
		}
	
	}

	public boolean hasOrderedRequirements (String referenceNumber) throws SQLException{
		
		LOGGER.info("hasOrderedRequirements start");
		String sql = "Select count(*) as RESULT from attachments where uar_reference_num = ?";
		int counter = 0;
		boolean result = false;

		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection conn = null;
		
		try {
			conn = getConnection();	
			ps = conn.prepareStatement(sql);
			ps.setString(1, referenceNumber);
			rs = ps.executeQuery();

			if (rs.next()){
				counter = rs.getInt("RESULT");
			}
			if (counter > 0){
				result = true;
			}
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			  throw e; 
		}finally{
			  closeResources(conn, ps, rs);
		}	
		LOGGER.info("hasOrderedRequirements end");
		return result;
	}

	public void removeAttachments(String referenceNumber) throws SQLException{
		
		LOGGER.info("removeAttachments start");
		String sql = "Delete from attachments where uar_reference_num = ?";
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection conn = null;
		
		try {
			conn = getConnection();	
			conn.setAutoCommit(false);
			ps = conn.prepareStatement(sql);
			ps.setString(1, referenceNumber);
			ps.executeUpdate();
			conn.commit();
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			if(conn != null){
				conn.rollback();
			}
			throw e; 
		}finally{
			  closeResources(conn, ps, rs);
		}	
		LOGGER.info("removeAttachments end");
	}

	public long retrieveEventId(long fromStatus, long toStatus, String lobCode) throws SQLException{
		
		LOGGER.info("retrieveEventId start");
		String sql = "Select evnt_id from process_configurations where evnt_from_status = ? and evnt_to_status = ? and lob_code = ?";
		long eventId = 0;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection conn = null;
		try {
			conn = getConnection();
			ps = conn.prepareStatement(sql);
			ps.setLong(1, fromStatus);
			ps.setLong(2,toStatus);
			ps.setString(3, lobCode);
			rs = ps.executeQuery();
			while (rs.next()){
				eventId = rs.getLong("evnt_id");
			}

		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			  throw e; 
		}finally{
			  closeResources(conn, ps, rs);
		}		
		LOGGER.info("retrieveEventId end");
		return eventId;
	}



}
