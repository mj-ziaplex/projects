/*
 * Created on Jun 25, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.data.dao.test;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import junit.framework.TestCase;

import com.slocpi.ium.data.LaboratoryData;
import com.slocpi.ium.data.LaboratoryTestData;
import com.slocpi.ium.data.UserProfileData;
import com.slocpi.ium.data.dao.LaboratoryDAO;
import com.slocpi.ium.data.util.DataSourceProxy;

/**
 * @author Alain del Rosario
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class LaboratoryDAOTest extends TestCase {

	/**
	 * Constructor for LaboratoryDAOTest.
	 * @param arg0
	 */
	public LaboratoryDAOTest(String arg0) {
		super(arg0);
	}



	public void testSelectLaboratories() throws SQLException {
		Connection conn2 = null;
		conn2 = new DataSourceProxy().getConnection();
		LaboratoryDAO ldao = new LaboratoryDAO(conn2);
		ArrayList alist = new ArrayList();
		alist = ldao.selectLaboratories();
		assertTrue(alist.size()==1);
	}

	public void testSelectLaboratory() throws SQLException {
		Connection conn2 = null;
		conn2 = new DataSourceProxy().getConnection();
		LaboratoryDAO ldao = new LaboratoryDAO(conn2);
		long labid = 1;
		LaboratoryData ld = new LaboratoryData();
		
		ld = ldao.selectLaboratory(labid);
		
		
		assertEquals(ld.getLabId(),labid);
		assertEquals(ld.getLabName(),"MAKATI MEDICAL CENTER");
		assertEquals(ld.getContactPerson(),"Dr. Loisa Esguerra");
		assertEquals(ld.getAccreditInd(),"1");
		assertEquals(((UserProfileData)ld.getCreatedBy()).getUserId(),"IUMDEV");
		
		
//assertEquals(((UserProfileData)ld.getUpdatedBy()).getUserId(),"IUMDEV");
/*
cannot work since there is a bug in the program...
		 
UserProfileData profile = new UserProfileData();
profile.setUserId(rs.getString("CREATED_BY"));
laboratory.setCreatedBy(profile);			
laboratory.setCreatedDate(DateHelper.parse(rs.getString("CREATED_DATE"), "dd-MMM-yyyy"));
UserProfileData profile2 = new UserProfileData();

profile.setUserId(rs.getString("UPDATED_BY"));  <--MUST BE profile2! ! !

laboratory.setUpdatedBy(profile2);
laboratory.setUpdatedDate(DateHelper.parse(rs.getString("UPDATED_DATE"), "dd-MMM-yyyy"));
			
*/
	    
		
	}
	
	


	public void testRetrieveLaboratoryTestData() throws SQLException {
		Connection conn2 = null;
		conn2 = new DataSourceProxy().getConnection();
		LaboratoryDAO ldao = new LaboratoryDAO(conn2);
		LaboratoryTestData ltd = new LaboratoryTestData();
		long labid = 1;
		long testid = 44;
		ltd = ldao.retrieveLaboratoryTestData(labid,testid);
		
		
	}
	
	//not finished yet ! ! ! ! ! !! 
	
	
	
	

	public void testSelectAllLaboratoryTestData() {
	}

	public void testInsertLaboratoryTestData() {
	}

	public void testUpdateLaboratoryTestData() {
	}

	public void testInsertLaboratoryData() {
	}

	public void testUpdateLaboratoryData() {
	}

}
