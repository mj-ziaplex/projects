/*
 * Created on Jun 25, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.data.dao.test;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import junit.framework.TestCase;

import com.slocpi.ium.data.RequirementData;
import com.slocpi.ium.data.SunLifeOfficeData;
import com.slocpi.ium.data.TestProfileData;
import com.slocpi.ium.data.dao.ReferenceDAO;
import com.slocpi.ium.data.util.DataSourceProxy;


/**
 * @author Alain del Rosario
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class ReferenceDAOTest extends TestCase {

	/**
	 * Constructor for ReferenceDAOTest.
	 * @param arg0
	 */
	public ReferenceDAOTest(String arg0) {
		super(arg0);
	}

	

	public void testReferenceDAO() {
		
	}




	public void testRetrieveTestProfile() throws SQLException {
		long testid = 50;
		int expnum= 99;
		Connection conn2 = null;
		conn2 = new DataSourceProxy().getConnection();
		ReferenceDAO rfdao = new ReferenceDAO(conn2);
		TestProfileData tpd = new TestProfileData();
		tpd = rfdao.retrieveTestProfile(testid);
		assertEquals((byte)testid,(byte)tpd.getTestId());
		assertEquals(tpd.getTestDesc(),"Anti-HBC");
		assertEquals(tpd.getValidity(),expnum);
		assertTrue(!tpd.isTaxable());

	}
	
	


	public void testRetrieveRequirement() throws SQLException {
		RequirementData rqdata = new RequirementData();
		long val = 0;
		long formid = 403;
		int follupnum = 0;
		Connection conn2 = null;
		conn2 = new DataSourceProxy().getConnection();
		ReferenceDAO rfdao = new ReferenceDAO(conn2);
		rqdata = rfdao.retrieveRequirement("AMRQ");
		assertEquals(rqdata.getReqtCode(),"AMRQ");
		assertEquals(rqdata.getReqtDesc(),"AUTOMOBILE AND MOTORCYCLE RACING QUESTIONNAIRE");
		assertEquals(rqdata.getLevel(),"C");
		assertEquals(rqdata.getValidity(),val);
		assertEquals(rqdata.getFormIndicator(),"1");
		assertEquals(rqdata.getFormID(),formid);
		assertEquals(rqdata.getFollowUpNum(),follupnum);
		assertEquals(rqdata.getTestDateIndicator(),"1");
		
	}




	public void testRetrieveRequirements() throws SQLException {
		ArrayList aresult = new ArrayList();
		Connection conn2 = null;
		conn2 = new DataSourceProxy().getConnection();
		ReferenceDAO rfdao = new ReferenceDAO(conn2);
		aresult = rfdao.retrieveRequirements();
		assertTrue(aresult.size()==42);//assuming nothing is added
	}



	public void testRetrieveSLOs() throws SQLException {
		ArrayList aresult = new ArrayList();
		Connection conn2 = null;
		conn2 = new DataSourceProxy().getConnection();
		ReferenceDAO rfdao = new ReferenceDAO(conn2);
		aresult = rfdao.retrieveSLOs();
		assertTrue(aresult.size()==61);//assuming nothing is added
	}

	public void testRetrieveSLO() throws SQLException {
		SunLifeOfficeData sod = new SunLifeOfficeData();
		Connection conn2 = null;
		conn2 = new DataSourceProxy().getConnection();
		ReferenceDAO rfdao = new ReferenceDAO(conn2);
		sod = rfdao.retrieveSLO("ACECUST");
		assertEquals(sod.getOfficeId(),"ACECUST");
		assertEquals(sod.getOfficeName(),"ACE CUSTOMER CENTER");
		assertEquals(sod.getOfficeType(),"S");
		assertEquals(sod.getAddr1(),"7/F ACE BUILDING");
		assertEquals(sod.getAddr2(),"101-103 RADA ST.");
		assertEquals(sod.getAddr3(),"LEGASPI VILLAGE");
		assertEquals(sod.getCity(),"MAKATI");
		assertNull(sod.getProvince());
		assertEquals(sod.getCountry(),"PHILIPPINES");
		assertEquals(sod.getZipCode(),"1200");
		assertNull(sod.getContactNumber());
		assertNull(sod.getFaxNumber());
	}



    
	public void testInsertSLO() throws SQLException {
		SunLifeOfficeData sod = new SunLifeOfficeData();
		Connection conn2 = null;
		conn2 = new DataSourceProxy().getConnection();
		ReferenceDAO rfdao = new ReferenceDAO(conn2);
		
		sod.setOfficeId("TESTB");
		sod.setOfficeName("TEST INSERT");
		sod.setOfficeType("T");
		sod.setAddr1("TEST INSERT");
		sod.setAddr2("TEST INSERT");
	    sod.setAddr3("TEST INSERT");
		sod.setCity("TEST");
		sod.setCountry("TEST");
		sod.setZipCode("TEST");
		rfdao.insertSLO(sod);
		}
		
	
		
		


	public void testEditSLO() {
	}

}
