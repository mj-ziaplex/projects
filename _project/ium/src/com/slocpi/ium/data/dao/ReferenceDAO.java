/**
 * ReferenceDAO.java				Copyright (c) 2004 Pointwest Technologies Corp.
 * TODO DOCUMENT ME!
 * 
 * @author aditalo	 @date Jan 21, 2004
 */
package com.slocpi.ium.data.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.slocpi.ium.data.RequirementData;
import com.slocpi.ium.data.SunLifeOfficeData;
import com.slocpi.ium.data.TestProfileData;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.DateHelper;

/**
 * TODO DOCUMENT ME!
 * 
 * @author aditalo		Copyright (c) 2004 Pointwest Technologies Corp.
 * @date Jan 21, 2004
 */
public class ReferenceDAO {

	private static final Logger LOGGER = LoggerFactory.getLogger(ReferenceDAO.class);
	private Connection conn = null;	

	/**
	 * 
	 */
	public ReferenceDAO(Connection connection) {
		super();
		this.conn = connection;
	}
	
	public void closeDB() {
		
		LOGGER.info("closeDB start");
		try {
			this.conn.close();
		} catch (Exception e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
		}
		LOGGER.info("closeDB end");
	}
	
	private void closeResources(PreparedStatement ps, ResultSet rs) throws SQLException{
		
		
		try {
			if (ps != null){
				ps.close();
			}
		}
		catch(SQLException e)
		{
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		}
		try {
			if (rs != null){
				rs.close();
			}			
		} catch (SQLException e){
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw e;
		}
		
	}
	public TestProfileData retrieveTestProfile(long testID) throws SQLException {
		
		LOGGER.info("retrieveTestProfile start");
		TestProfileData data = new TestProfileData();
		String sql = "SELECT TEST_DESC, TEST_VALIDITY, TEST_TAXABLE_IND" +
					" FROM TEST_PROFILES WHERE TEST_ID = ?";
		PreparedStatement prepStmt = null;
		ResultSet rs = null;
		try {
			prepStmt = conn.prepareStatement(sql);
			prepStmt.setLong(1, testID);
			rs = prepStmt.executeQuery();
			if (rs.next()) {
				data.setTestId(testID);
				data.setTestDesc(rs.getString("TEST_DESC"));
				data.setValidity(rs.getInt("TEST_VALIDITY"));
				data.setTaxable(rs.getBoolean("TEST_TAXABLE_IND"));
			}
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			  throw e; 
		}finally{
			  this.closeResources(prepStmt, rs);
		}	
		LOGGER.info("retrieveTestProfile end");
		return data;
	}
	
	public RequirementData retrieveRequirement(String requirementCode) throws SQLException{
		
		LOGGER.info("retrieveRequirement start");
		String sql = "SELECT REQT_CODE,REQT_DESC,REQT_LEVEL,REQT_VALIDITY,REQT_FORM_IND,REQT_FORM_ID,REQT_FOLLOW_UP_NUM, REQT_TEST_DATE_INDICATOR " +
					 "FROM REQUIREMENTS WHERE REQT_CODE = ?";
		PreparedStatement stmt = null;
		ResultSet rs = null;
		RequirementData reqData = new RequirementData();
		try{
			stmt = conn.prepareStatement(sql);
			stmt.setString(1, requirementCode);
			rs = stmt.executeQuery();
			while (rs.next()){
				reqData.setReqtCode(rs.getString("REQT_CODE"));
				reqData.setReqtDesc(rs.getString("REQT_DESC"));
				reqData.setLevel(rs.getString("REQT_LEVEL"));
				reqData.setValidity(rs.getLong("REQT_VALIDITY"));
				reqData.setFormIndicator(rs.getString("REQT_FORM_IND"));
				reqData.setFormID(rs.getLong("REQT_FORM_ID"));
				reqData.setFollowUpNum(rs.getInt("REQT_FOLLOW_UP_NUM"));
				reqData.setTestDateIndicator(rs.getString("REQT_TEST_DATE_INDICATOR"));
			}
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			  throw e; 
		}finally{
			  this.closeResources(stmt, rs);
		}	
		LOGGER.info("retrieveRequirement end");
		return reqData;
	}


	public ArrayList retrieveRequirements() throws SQLException{
		
		LOGGER.info("retrieveRequirements start");
		String sql = "SELECT REQT_CODE,REQT_DESC,REQT_LEVEL,REQT_VALIDITY,REQT_FORM_IND,REQT_FORM_ID,REQT_FOLLOW_UP_NUM, REQT_TEST_DATE_INDICATOR " +
					 "FROM REQUIREMENTS ORDER BY REQT_CODE";
		PreparedStatement stmt = null;
		ResultSet rs = null;
		ArrayList result = new ArrayList();		
		try{
	
			stmt = conn.prepareStatement(sql);
			rs = stmt.executeQuery();
			while (rs.next()){
				RequirementData reqData = new RequirementData();
				reqData.setReqtCode(rs.getString("REQT_CODE"));
				reqData.setReqtDesc(rs.getString("REQT_DESC"));
				reqData.setLevel(rs.getString("REQT_LEVEL"));
				reqData.setValidity(rs.getLong("REQT_VALIDITY"));
				reqData.setFormIndicator(rs.getString("REQT_FORM_IND"));
				reqData.setFormID(rs.getLong("REQT_FORM_ID"));
				reqData.setFollowUpNum(rs.getInt("REQT_FOLLOW_UP_NUM"));
				reqData.setTestDateIndicator(rs.getString("REQT_TEST_DATE_INDICATOR"));
				result.add(reqData);
			}
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			  throw e; 
		}finally{
			  this.closeResources(stmt, rs);
		}	
		LOGGER.info("retrieveRequirements end");
		return result;
	}
	/**
	 * 
	 * Retrieves all Sun Life Office/Branch records.
	 * @return
	 * @throws SQLException
	 */
	public ArrayList retrieveSLOs()throws SQLException{
		
		LOGGER.info("retrieveSLOs start");
		StringBuffer sql = new StringBuffer("SELECT");
		sql.append(" SLO_OFFICE_CODE,");
		sql.append(" SLO_OFFICE_NAME,");
		sql.append(" SLO_TYPE,");
		sql.append(" SLO_ADDRESS_LINE1,");
		sql.append(" SLO_ADDRESS_LINE2,");
		sql.append(" SLO_ADDRESS_LINE3,");
		sql.append(" SLO_CITY,");
		sql.append(" SLO_PROVINCE,");
		sql.append(" SLO_COUNTRY,");
		sql.append(" SLO_ZIP_CODE,");
		sql.append(" SLO_CONTACT_NUM,");
		sql.append(" SLO_FAX_NUM");
		sql.append(" FROM SUNLIFE_OFFICES");
		PreparedStatement stmt = null;
		ResultSet rs = null;
		ArrayList result = new ArrayList();		
		try{
			Connection reqConn = conn;
			stmt = conn.prepareStatement(sql.toString());
			rs = stmt.executeQuery();
			while (rs.next()){
				SunLifeOfficeData sloData = new SunLifeOfficeData();
				sloData.setOfficeId(rs.getString("SLO_OFFICE_CODE"));
				sloData.setOfficeName(rs.getString("SLO_OFFICE_NAME"));
				sloData.setOfficeType(rs.getString("SLO_TYPE"));
				sloData.setAddr1(rs.getString("SLO_ADDRESS_LINE1"));
				sloData.setAddr2(rs.getString("SLO_ADDRESS_LINE2"));
				sloData.setAddr3(rs.getString("SLO_ADDRESS_LINE3"));
				sloData.setCity(rs.getString("SLO_CITY"));
				sloData.setProvince(rs.getString("SLO_PROVINCE"));
				sloData.setCountry(rs.getString("SLO_COUNTRY"));
				sloData.setZipCode(rs.getString("SLO_ZIP_CODE"));
				sloData.setContactNumber(rs.getString("SLO_CONTACT_NUM"));
				sloData.setFaxNumber(rs.getString("SLO_FAX_NUM"));
				
				result.add(sloData);
			}
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			  throw e; 
		}finally{
			  this.closeResources(stmt , rs);
		}	
		LOGGER.info("retrieveSLOs end");
		return result;

		
		
	}


	/**
	 * 
	 * Retrieves a Sun Life Office/Branch records given an id.
	 * @return
	 * @throws SQLException
	 */
	public SunLifeOfficeData retrieveSLO(String sloId)throws SQLException{
		
		LOGGER.info("retrieveSLO start");
		StringBuffer sql = new StringBuffer("SELECT");
		sql.append(" SLO_OFFICE_CODE,");
		sql.append(" SLO_OFFICE_NAME,");
		sql.append(" SLO_TYPE,");
		sql.append(" SLO_ADDRESS_LINE1,");
		sql.append(" SLO_ADDRESS_LINE2,");
		sql.append(" SLO_ADDRESS_LINE3,");
		sql.append(" SLO_CITY,");
		sql.append(" SLO_PROVINCE,");
		sql.append(" SLO_COUNTRY,");
		sql.append(" SLO_ZIP_CODE,");
		sql.append(" SLO_CONTACT_NUM,");
		sql.append(" SLO_FAX_NUM");
		sql.append(" FROM SUNLIFE_OFFICES");
		sql.append(" WHERE SLO_OFFICE_CODE=?");
		
		PreparedStatement stmt = null;
		ResultSet rs = null;
		SunLifeOfficeData sloData=null;	
		try{
			
			stmt = conn.prepareStatement(sql.toString());
			stmt.setString(1,sloId);
			rs = stmt.executeQuery();
					
			if (rs.next()){
				sloData = new SunLifeOfficeData();
				sloData.setOfficeId(rs.getString("SLO_OFFICE_CODE"));
				sloData.setOfficeName(rs.getString("SLO_OFFICE_NAME"));
				sloData.setOfficeType(rs.getString("SLO_TYPE"));
				sloData.setAddr1(rs.getString("SLO_ADDRESS_LINE1"));
				sloData.setAddr2(rs.getString("SLO_ADDRESS_LINE2"));
				sloData.setAddr3(rs.getString("SLO_ADDRESS_LINE3"));
				sloData.setCity(rs.getString("SLO_CITY"));
				sloData.setProvince(rs.getString("SLO_PROVINCE"));
				sloData.setCountry(rs.getString("SLO_COUNTRY"));
				sloData.setZipCode(rs.getString("SLO_ZIP_CODE"));
				sloData.setContactNumber(rs.getString("SLO_CONTACT_NUM"));
				sloData.setFaxNumber(rs.getString("SLO_FAX_NUM"));
			}
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			  throw e; 
		}finally{
			  this.closeResources(stmt, rs);
		}	
		LOGGER.info("retrieveSLO end");
		return sloData;
	}

	/**
	 * Creates a new Sun Life Office Record.
	 * @param data
	 * @throws SQLException
	 */
	public void insertSLO(SunLifeOfficeData data) throws SQLException{
		
		LOGGER.info("insertSLO start");
		StringBuffer sql = new StringBuffer("INSERT INTO SUNLIFE_OFFICES (");
		sql.append(" SLO_OFFICE_CODE,");
		sql.append(" SLO_OFFICE_NAME,");
		sql.append(" SLO_TYPE,");
		sql.append(" SLO_ADDRESS_LINE1,");
		sql.append(" SLO_ADDRESS_LINE2,");
		sql.append(" SLO_ADDRESS_LINE3,");
		sql.append(" SLO_CITY,");
		sql.append(" SLO_PROVINCE,");
		sql.append(" SLO_COUNTRY,");
		sql.append(" SLO_ZIP_CODE,");
		sql.append(" SLO_CONTACT_NUM,");
		sql.append(" SLO_FAX_NUM, ");
		sql.append(" CREATED_BY, ");
		sql.append(" CREATED_DATE ");
		sql.append(" ) values ( ?,?,?,?,?,?,?,?,?,?,?,?,?,? ) ");
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try{

		
		PreparedStatement stmt = conn.prepareStatement(sql.toString());
		stmt.setString(1,data.getOfficeId().toUpperCase());
		stmt.setString(2,data.getOfficeName().toUpperCase());
		stmt.setString(3,data.getOfficeType());
		stmt.setString(4,data.getAddr1());
		stmt.setString(5,data.getAddr2());
		stmt.setString(6,data.getAddr3());
		stmt.setString(7,data.getCity());
		stmt.setString(8,data.getProvince());
		stmt.setString(9,data.getCountry());
		stmt.setString(10,data.getZipCode());
		stmt.setString(11,data.getContactNumber());
		stmt.setString(12,data.getFaxNumber());
		stmt.setString(13,data.getCreatedBy());
		stmt.setTimestamp(14,DateHelper.sqlTimestamp(data.getCreateDate()));
		
		stmt.executeUpdate();
		
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			  throw e; 
		}finally{
			  this.closeResources(ps, rs);
		}
		LOGGER.info("insertSLO end");
	}



	/**
	 * Creates a new Sun Life Office Record.
	 * @param data
	 * @throws SQLException
	 */
	public void editSLO(SunLifeOfficeData data) throws SQLException{
		
		LOGGER.info("editSLO start");
		StringBuffer sql = new StringBuffer("UPDATE SUNLIFE_OFFICES SET ");
		sql.append(" SLO_OFFICE_NAME=?,");
		sql.append(" SLO_TYPE=?,");
		sql.append(" SLO_ADDRESS_LINE1=?,");
		sql.append(" SLO_ADDRESS_LINE2=?,");
		sql.append(" SLO_ADDRESS_LINE3=?,");
		sql.append(" SLO_CITY=?,");
		sql.append(" SLO_PROVINCE=?,");
		sql.append(" SLO_COUNTRY=?,");
		sql.append(" SLO_ZIP_CODE=?,");
		sql.append(" SLO_CONTACT_NUM=?,");
		sql.append(" SLO_FAX_NUM=?, ");
		sql.append(" UPDATED_BY=?, ");
		sql.append(" UPDATED_DATE=?");
		sql.append(" WHERE SLO_OFFICE_CODE=?");
		
		PreparedStatement stmt = null;
		ResultSet rs = null;
		
		try{

		
		stmt = conn.prepareStatement(sql.toString());
		
		stmt.setString(1,data.getOfficeName().toUpperCase());
		stmt.setString(2,data.getOfficeType());
		stmt.setString(3,data.getAddr1());
		stmt.setString(4,data.getAddr2());
		stmt.setString(5,data.getAddr3());
		stmt.setString(6,data.getCity());
		stmt.setString(7,data.getProvince());
		stmt.setString(8,data.getCountry());
		stmt.setString(9,data.getZipCode());
		stmt.setString(10,data.getContactNumber());
		stmt.setString(11,data.getFaxNumber());
		stmt.setString(12,data.getUpdatedBy());
		stmt.setTimestamp(13,DateHelper.sqlTimestamp(data.getUpdateDate()));
		stmt.setString(14,data.getOfficeId());
		stmt.executeUpdate();
	
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			  throw e; 
		}finally{
			  this.closeResources(stmt, rs);
		}
		LOGGER.info("editSLO end");
	}

}
