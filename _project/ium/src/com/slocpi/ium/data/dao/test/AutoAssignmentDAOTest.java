/*
 * Created on Jun 25, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.data.dao.test;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import junit.framework.TestCase;

import com.slocpi.ium.data.AutoAssignmentData;
import com.slocpi.ium.data.dao.AutoAssignmentDAO;
import com.slocpi.ium.data.util.DataSourceProxy;
import com.slocpi.ium.util.SortHelper;

/**
 * @author mlua
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class AutoAssignmentDAOTest extends TestCase {

	/**
	 * Constructor for AutoAssignmentDAOTest.
	 * @param arg0
	 */
	private Connection conn = null;
	public AutoAssignmentDAOTest(String arg0) {
		super(arg0);
	}

	public void testRetrieveAutoAssignments() throws SQLException {
		conn = (Connection)new DataSourceProxy().getConnection();
		AutoAssignmentDAO auto = new AutoAssignmentDAO(conn);
		SortHelper sort = new SortHelper("AUTO_ASSIGNMENTS");
		AutoAssignmentData data = new AutoAssignmentData();
		
		sort.setColumn(1);
		sort.setSortOrder("");
		ArrayList assign = new ArrayList();
		assign = auto.retrieveAutoAssignments(sort);
		long aac_id = ((AutoAssignmentData)assign.get(0)).getCriteriaId();
		long aa_id = ((AutoAssignmentData)assign.get(0)).getAutoAssignmentId();
		String usercode = ((AutoAssignmentData)assign.get(0)).getUserCode();
	
		assertTrue(assign.size() > 0);
		assertEquals(aac_id,14);
		assertEquals(aa_id,41);
		assertEquals(usercode,"IUMDEV");	
	}

	public void testRetrieveAutoAssignmentDetail() throws SQLException {
		conn = (Connection)new DataSourceProxy().getConnection();
		AutoAssignmentDAO auto = new AutoAssignmentDAO(conn);
		AutoAssignmentData data = new AutoAssignmentData();

		/* Test if data is true*/
		data = auto.retrieveAutoAssignmentDetail(11);
		long aa_id = data.getAutoAssignmentId();
		long aac_id = data.getCriteriaId();
		String usercode = data.getUserCode();
		
		assertEquals(aa_id,11);
		assertEquals(aac_id,16);
		assertEquals(usercode,"IUMDEV");
						
		/* Test if data exist*/
		data = auto.retrieveAutoAssignmentDetail(13123);
		assertNull(data);

	}

/*	public void testInsertMapping() throws SQLException {
		conn = (Connection)new DataSourceProxy().getConnection();
		AutoAssignmentDAO auto = new AutoAssignmentDAO(conn);
		AutoAssignmentData data = new AutoAssignmentData();

		data.setAutoAssignmentId(10);
		data.setCriteriaId(20);
		data.setUserCode("IUMDEV");
		auto.insertMapping(data);

		/* Test if data is inserted*/
/*		data = auto.retrieveAutoAssignmentDetail(10);
		long aa_id = data.getAutoAssignmentId();
		long aac_id = data.getCriteriaId();
		String usercode = data.getUserCode();
		
		assertEquals(aa_id,10);
		assertEquals(aac_id,20);
		assertEquals(usercode,"IUMDEV");
	}

	public void testUpdateMapping() throws SQLException {
		conn = (Connection)new DataSourceProxy().getConnection();
		AutoAssignmentDAO auto = new AutoAssignmentDAO(conn);
		AutoAssignmentData data = new AutoAssignmentData();

		data.setAutoAssignmentId(10);
		data.setCriteriaId(30);
		data.setUserCode("IUMDEV2");
		auto.updateMapping(data);

		/* Test if data is inserted*/
/*		data = auto.retrieveAutoAssignmentDetail(10);
		long aa_id = data.getAutoAssignmentId();
		long aac_id = data.getCriteriaId();
		String usercode = data.getUserCode();
		
		assertEquals(aa_id,10);
		assertEquals(aac_id,30);
		assertEquals(usercode,"IUMDEV2");

	}
*/
	public void testIsDuplicate() throws SQLException {
		conn = (Connection)new DataSourceProxy().getConnection();
		AutoAssignmentDAO auto = new AutoAssignmentDAO(conn);
		AutoAssignmentData data = new AutoAssignmentData();
		
		/* TEST FOR CHECKING IF DUPLICATE */
		assertTrue(!auto.isDuplicate(16,11,"REQFC1","IUMDEV"));
	}

}
