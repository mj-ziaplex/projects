/*
 * Created on Dec 18, 2003
 *
 */
package com.slocpi.ium.data;

import java.util.Date;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Engel
 * @TODO class description RequestFilterData
 */
public class RequestFilterData {

	private static final Logger LOGGER = LoggerFactory.getLogger(RequestFilterData.class);
	private String referenceNumber="";
	private String branch="";
	private String agent="";
	private String clientNumber="";
	private String lob="";
	private String location="";
	private String status="";
	private String assignedTo="";
	private String requirementCode="";
	private String nameOfInsured="";
	private Date applicationReceivedDate;
	private double coverageAmount;
	private String sortOrder="";
	private int columns;
	
	public String toString(){
		return ToStringBuilder.reflectionToString(this, ToStringStyle.DEFAULT_STYLE);
	}
	
	/**
	 * TODO method description getAgent
	 * @return
	 */
	public String getAgent() {
		return agent;
	}

	/**
	 * TODO method description getApplicationReceivedDate
	 * @return
	 */
	public Date getApplicationReceivedDate() {
		return applicationReceivedDate;
	}

	/**
	 * TODO method description getAssignedTo
	 * @return
	 */
	public String getAssignedTo() {
		return assignedTo;
	}

	/**
	 * TODO method description getClientNumber
	 * @return
	 */
	public String getClientNumber() {
		return clientNumber;
	}

	/**
	 * TODO method description getCoverageAmout
	 * @return
	 */
	public double getCoverageAmount() {
		return coverageAmount;
	}

	/**
	 * TODO method description getLob
	 * @return
	 */
	public String getLob() {
		return lob;
	}

	/**
	 * TODO method description getNameOfInsured
	 * @return
	 */
	public String getNameOfInsured() {
		return nameOfInsured;
	}

	/**
	 * TODO method description getReferenceNumber
	 * @return
	 */
	public String getReferenceNumber() {
		return referenceNumber;
	}

	/**
	 * TODO method description getRequirementCode
	 * @return
	 */
	public String getRequirementCode() {
		return requirementCode;
	}

	/**
	 * TODO method description getStatus
	 * @return
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * TODO method description setAgent
	 * @param string
	 */
	public void setAgent(String string) {
		agent = string;
	}

	/**
	 * TODO method description setApplicationReceivedDate
	 * @param date
	 */
	public void setApplicationReceivedDate(Date date) {
		applicationReceivedDate = date;
	}

	/**
	 * TODO method description setAssignedTo
	 * @param string
	 */
	public void setAssignedTo(String string) {
		assignedTo = string;
	}

	/**
	 * TODO method description setClientNumber
	 * @param string
	 */
	public void setClientNumber(String string) {
		clientNumber = string;
	}

	/**
	 * TODO method description setCoverageAmout
	 * @param date
	 */
	public void setCoverageAmount(double value) {
		coverageAmount = value;
	}

	/**
	 * TODO method description setLob
	 * @param string
	 */
	public void setLob(String string) {
		lob = string;
	}

	/**
	 * TODO method description setNameOfInsured
	 * @param string
	 */
	public void setNameOfInsured(String string) {
		nameOfInsured = string;
	}

	/**
	 * TODO method description setReferenceNumber
	 * @param string
	 */
	public void setReferenceNumber(String string) {
		referenceNumber = string;
	}

	/**
	 * TODO method description setRequirementCode
	 * @param string
	 */
	public void setRequirementCode(String string) {
		requirementCode = string;
	}

	/**
	 * TODO method description setStatus
	 * @param string
	 */
	public void setStatus(String string) {
		status = string;
	}

	/**
	 * TODO method description getBranch
	 * @return
	 */
	public String getBranch() {
		return branch;
	}

	/**
	 * TODO method description setBranch
	 * @param string
	 */
	public void setBranch(String string) {
		branch = string;
	}



	/**
	 * TODO method description getSortOrder
	 * @return
	 */
	public String getSortOrder() {
		return sortOrder;
	}

	/**
	 * TODO method description setSortOrder
	 * @param string
	 */
	public void setSortOrder(String string) {
		sortOrder = string;
	}

	/**
	 * TODO method description getColumns
	 * @return
	 */
	public int getColumns() {
		return columns;
	}

	/**
	 * TODO method description setColumns
	 * @param list
	 */
	public void setColumns(int column) {
		columns = column;
	}

	/**
	 * TODO method description getLocation
	 * @return
	 */
	public String getLocation() {
		return location;
	}

	/**
	 * TODO method description setLocation
	 * @param string
	 */
	public void setLocation(String string) {
		location = string;
	}

}
