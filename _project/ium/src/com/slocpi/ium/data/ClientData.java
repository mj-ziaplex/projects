/*
 * Created on Dec 24, 2003
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.data;

import java.util.Date;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author daguila
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class ClientData {

	private static final Logger LOGGER = LoggerFactory.getLogger(ClientData.class);
	private String 	clientId;
	private String 	lastName;
	private String 	givenName;
	private String 	middleName;
	private String 	otherLastName;
	private String 	otherGivenName;
	private String 	otherMiddleName;
	private String 	title;
	private String 	suffix;
	private int		age;
	private String	sex;
	private Date	birthDate;
	private String	birthLocation;
	private boolean smoker;
	
	public String toString(){
		return ToStringBuilder.reflectionToString(this, ToStringStyle.DEFAULT_STYLE);
	}
	
	/**
	 * @return
	 */
	public int getAge() {
		return age;
	}

	/**
	 * @return
	 */
	public Date getBirthDate() {
		return birthDate;
	}

	/**
	 * @return
	 */
	public String getClientId() {
		return clientId;
	}

	/**
	 * @return
	 */
	public String getGivenName() {
		return givenName;
	}

	/**
	 * @return
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @return
	 */
	public String getMiddleName() {
		return middleName;
	}

	/**
	 * @return
	 */
	public String getOtherGivenName() {
		return otherGivenName;
	}

	/**
	 * @return
	 */
	public String getOtherLastName() {
		return otherLastName;
	}

	/**
	 * @return
	 */
	public String getOtherMiddleName() {
		return otherMiddleName;
	}

	/**
	 * @return
	 */
	public String getSex() {
		return sex;
	}

	/**
	 * @return
	 */
	public boolean isSmoker() {
		return smoker;
	}

	/**
	 * @return
	 */
	public String getSuffix() {
		return suffix;
	}

	/**
	 * @return
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param i
	 */
	public void setAge(int i) {
		age = i;
	}

	/**
	 * @param date
	 */
	public void setBirthDate(Date date) {
		birthDate = date;
	}

	/**
	 * @param string
	 */
	public void setClientId(String string) {
		clientId = string;
	}

	/**
	 * @param string
	 */
	public void setGivenName(String string) {
		givenName = string;
	}

	/**
	 * @param string
	 */
	public void setLastName(String string) {
		lastName = string;
	}

	/**
	 * @param string
	 */
	public void setMiddleName(String string) {
		middleName = string;
	}

	/**
	 * @param string
	 */
	public void setOtherGivenName(String string) {
		otherGivenName = string;
	}

	/**
	 * @param string
	 */
	public void setOtherLastName(String string) {
		otherLastName = string;
	}

	/**
	 * @param string
	 */
	public void setOtherMiddleName(String string) {
		otherMiddleName = string;
	}

	/**
	 * @param string
	 */
	public void setSex(String string) {
		sex = string;
	}

	/**
	 * @param b
	 */
	public void setSmoker(boolean b) {
		smoker = b;
	}

	/**
	 * @param string
	 */
	public void setSuffix(String string) {
		suffix = string;
	}

	/**
	 * @param string
	 */
	public void setTitle(String string) {
		title = string;
	}

	/**
	 * @return
	 */
	public String getBirthLocation() {
		return birthLocation;
	}

	/**
	 * @param string
	 */
	public void setBirthLocation(String string) {
		birthLocation = string;
	}

}
