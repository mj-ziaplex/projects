/*
 * Created on Mar 5, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.data;

import java.util.Date;

/**
 * @author Cris
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class ExceptionFilterData {
	private Date startDate;
	private Date endDate;
	private String recordType;
	private String interfaceType;

	/**
	 * @return
	 */
	public Date getEndDate() {
		return endDate;
	}

	/**
	 * @return
	 */
	public String getRecordType() {
		return recordType;
	}

	/**
	 * @return
	 */
	public Date getStartDate() {
		return startDate;
	}

	/**
	 * @param date
	 */
	public void setEndDate(Date date) {
		endDate = date;
	}

	/**
	 * @param string
	 */
	public void setRecordType(String string) {
		recordType = string;
	}

	/**
	 * @param date
	 */
	public void setStartDate(Date date) {
		startDate = date;
	}

	/**
	 * @return
	 */
	public String getInterfaceType() {
		return interfaceType;
	}

	/**
	 * @param string
	 */
	public void setInterfaceType(String string) {
		interfaceType = string;
	}

}
