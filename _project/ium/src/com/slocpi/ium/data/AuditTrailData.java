/*
 * Created on Dec 23, 2003
 * package name = com.slocpi.ium.data
 * file name    = AuditTrailData.java
 */
package com.slocpi.ium.data;

import java.util.Date;

/**
 * Container for Audit Trail reference information.
 * @author Engel
 * 
 */
public class AuditTrailData {
	
	private long transId;
	private long transRec;
	private String transType;
	private Date transDate;
	private String userID;
	private String referenceNumber;
	private String changeFrom;
	private String changeTo;
	
	/**
	 * TODO method description getChangeFrom
	 * @return
	 */
	public String getChangeFrom() {
		return changeFrom;
	}

	/**
	 * TODO method description getChangeTo
	 * @return
	 */
	public String getChangeTo() {
		return changeTo;
	}

	/**
	 * TODO method description getReferenceNumber
	 * @return
	 */
	public String getReferenceNumber() {
		return referenceNumber;
	}

	/**
	 * TODO method description getTransDate
	 * @return
	 */
	public Date getTransDate() {
		return transDate;
	}

	/**
	 * TODO method description getTransId
	 * @return
	 */
	public long getTransId() {
		return transId;
	}


	/**
	 * TODO method description getTransType
	 * @return
	 */
	public String getTransType() {
		return transType;
	}

	/**
	 * TODO method description setChangeFrom
	 * @param string
	 */
	public void setChangeFrom(String string) {
		changeFrom = string;
	}

	/**
	 * TODO method description setChangeTo
	 * @param string
	 */
	public void setChangeTo(String string) {
		changeTo = string;
	}

	/**
	 * TODO method description setReferenceNumber
	 * @param string
	 */
	public void setReferenceNumber(String string) {
		referenceNumber = string;
	}

	/**
	 * TODO method description setTransDate
	 * @param date
	 */
	public void setTransDate(Date date) {
		transDate = date;
	}

	/**
	 * TODO method description setTransId
	 * @param l
	 */
	public void setTransId(long l) {
		transId = l;
	}


	/**
	 * TODO method description setTransType
	 * @param string
	 */
	public void setTransType(String string) {
		transType = string;
	}

	/**
	 * @return
	 */
	public long getTransRec() {
		return transRec;
	}

	/**
	 * @param l
	 */
	public void setTransRec(long l) {
		transRec = l;
	}

	/**
	 * @return
	 */
	public String getUserID() {
		return userID;
	}

	/**
	 * @param string
	 */
	public void setUserID(String string) {
		userID = string;
	}

}
