/*
 * Created on Dec 18, 2003
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.data;

import java.util.Date;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Cris
 *
 *  Container for User Profile reference information.
 */
public class UserProfileData {

	private static final Logger LOGGER = LoggerFactory.getLogger(UserProfileData.class);
	private String userId;
	private String ACF2ID;
	private String password;
	private String lastName;
	private String firstName;
	private String middleName;
	private String sectionDesc;
	private String userCode;
	private String userType;
	private String emailAddr;
	private String mobileNumber;
	private boolean notification;
	private int recordsPerView;
	private String sex;
	private String role;
	private String officeCode;
	private String deptCode;
	private String sectionCode;
	private String createdBy;
	private Date createDate;
	private String updatedBy;
	private Date updateDate;
	private String contactNum;
	private String address1;	
	private String address2;
	private String address3;
	private String country;
	private String city;
	private String zipCode;
	private boolean lock;
	private boolean active;
	private String login_ind;
	private Date pswd_update_date;

	public String toString(){
		return ToStringBuilder.reflectionToString(this, ToStringStyle.DEFAULT_STYLE);
	}
	
	
	/**
	 * @return
	 */
	public String getACF2ID() {
		return ACF2ID;
	}

	/**
	 * @return
	 */
	public Date getCreateDate() {
		return createDate;
	}

	/**
	 * @return
	 */
	public String getCreatedBy() {
		return createdBy;
	}

	/**
	 * @return
	 */
	public String getDeptCode() {
		return deptCode;
	}

	/**
	 * @return
	 */
	public String getEmailAddr() {
		return emailAddr;
	}

	/**
	 * @return
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @return
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @return
	 */
	public String getMiddleName() {
		return middleName;
	}
	
	public String getSectionDesc() {
		return sectionDesc;
	}

	/**
	 * @return
	 */
	public String getMobileNumber() {
		return mobileNumber;
	}

	/**
	 * @return
	 */
	public boolean hasNotification() {
		return notification;
	}

	/**
	 * @return
	 */
	public String getOfficeCode() {
		return officeCode;
	}

	/**
	 * @return
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @return
	 */
	public String getRole() {
		return role;
	}

	/**
	 * @return
	 */
	public String getSectionCode() {
		return sectionCode;
	}

	/**
	 * @return
	 */
	public String getSex() {
		return sex;
	}

	/**
	 * @return
	 */
	public Date getUpdateDate() {
		return updateDate;
	}

	/**
	 * @return
	 */
	public String getUpdatedBy() {
		return updatedBy;
	}

	/**
	 * @return
	 */
	public String getUserCode() {
		return userCode;
	}

	/**
	 * @return
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * @return
	 */
	public String getUserType() {
		return userType;
	}

	/**
	 * @param string
	 */
	public void setACF2ID(String string) {
		ACF2ID = string;
	}

	/**
	 * @param date
	 */
	public void setCreateDate(Date date) {
		createDate = date;
	}

	/**
	 * @param string
	 */
	public void setCreatedBy(String string) {
		createdBy = string;
	}

	/**
	 * @param string
	 */
	public void setDeptCode(String string) {
		deptCode = string;
	}

	/**
	 * @param string
	 */
	public void setEmailAddr(String string) {
		emailAddr = string;
	}

	/**
	 * @param string
	 */
	public void setFirstName(String string) {
		firstName = string;
	}

	/**
	 * @param string
	 */
	public void setLastName(String string) {
		lastName = string;
	}

	/**
	 * @param string
	 */
	public void setMiddleName(String string) {
		middleName = string;
	}
	
	public void setSectionDesc(String string) {
		sectionDesc = string;
	}

	/**
	 * @param string
	 */
	public void setMobileNumber(String string) {
		mobileNumber = string;
	}

	/**
	 * @param b
	 */
	public void setNotificationInd(boolean b) {
		notification = b;
	}

	/**
	 * @param string
	 */
	public void setOfficeCode(String string) {
		officeCode = string;
	}

	/**
	 * @param string
	 */
	public void setPassword(String string) {
		password = string;
	}

	/**
	 * @param string
	 */
	public void setRole(String string) {
		role = string;
	}

	/**
	 * @param string
	 */
	public void setSectionCode(String string) {
		sectionCode = string;
	}

	/**
	 * @param string
	 */
	public void setSex(String string) {
		sex = string;
	}

	/**
	 * @param date
	 */
	public void setUpdateDate(Date date) {
		updateDate = date;
	}

	/**
	 * @param string
	 */
	public void setUpdatedBy(String string) {
		updatedBy = string;
	}

	/**
	 * @param string
	 */
	public void setUserCode(String string) {
		userCode = string;
	}

	/**
	 * @param string
	 */
	public void setUserId(String string) {
		userId = string;
	}

	/**
	 * @param string
	 */
	public void setUserType(String string) {
		userType = string;
	}

	/**
	 * @return
	 */
	public int getRecordsPerView() {
		return recordsPerView;
	}

	/**
	 * @param l
	 */
	public void setRecordsPerView(int i) {
		recordsPerView = i;
	}

	/**
	 * @return
	 */
	public boolean isActive() {
		return active;
	}

	/**
	 * @return
	 */
	public String getAddress1() {
		return address1;
	}

	/**
	 * @return
	 */
	public String getAddress2() {
		return address2;
	}

	/**
	 * @return
	 */
	public String getAddress3() {
		return address3;
	}

	/**
	 * @return
	 */
	public String getCity() {
		return city;
	}

	/**
	 * @return
	 */
	public String getContactNum() {
		return contactNum;
	}

	/**
	 * @return
	 */
	public String getCountry() {
		return country;
	}

	/**
	 * @return
	 */
	public boolean isLock() {
		return lock;
	}

	/**
	 * @return
	 */
	public String getZipCode() {
		return zipCode;
	}

	/**
	 * @param b
	 */
	public void setActive(boolean b) {
		active = b;
	}

	/**
	 * @param string
	 */
	public void setAddress1(String string) {
		address1 = string;
	}

	/**
	 * @param string
	 */
	public void setAddress2(String string) {
		address2 = string;
	}

	/**
	 * @param string
	 */
	public void setAddress3(String string) {
		address3 = string;
	}

	/**
	 * @param string
	 */
	public void setCity(String string) {
		city = string;
	}

	/**
	 * @param string
	 */
	public void setContactNum(String string) {
		contactNum = string;
	}

	/**
	 * @param string
	 */
	public void setCountry(String string) {
		country = string;
	}

	/**
	 * @param b
	 */
	public void setLock(boolean b) {
		lock = b;
	}

	/**
	 * @param string
	 */
	public void setZipCode(String string) {
		zipCode = string;
	}

	/**
	 * @return
	 */
	public String getLogin_ind() {
		return login_ind;
	}

	/**
	 * @return
	 */
	public boolean isNotification() {
		return notification;
	}

	/**
	 * @return
	 */
	public Date getPswd_update_date() {
		return pswd_update_date;
	}

	/**
	 * @param string
	 */
	public void setLogin_ind(String string) {
		login_ind = string;
	}

	/**
	 * @param b
	 */
	public void setNotification(boolean b) {
		notification = b;
	}

	/**
	 * @param date
	 */
	public void setPswd_update_date(Date date) {
		pswd_update_date = date;
	}

}
