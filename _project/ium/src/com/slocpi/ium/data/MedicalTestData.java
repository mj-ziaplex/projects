/*
 * Created on Dec 17, 2003
 *
 */
package com.slocpi.ium.data;

import java.util.ArrayList;
import java.util.Date;

/**
 *
 * Contains information pertaining to an medical test.
 * @author Engel 
 */
public class MedicalTestData {

	private String referenceNumber;  	
	private String clientNumber;		
	private String lastName;
	private String firstName;
	private String examStatus;
	private Date memoDate;
	private Date requestDate;
	private Date startFollowUpDate;
	private Date endFollowUpDate;
	private String laboratory;
	private String examiner;
	private boolean unassigned;
	private Date conductedDate;
	private String type;
	private String branch;
	private String testName;
	private Date receivedDate;
	private ArrayList medicalExamDetails;
	
	/**
	 * TODO method description getBranch
	 * @return
	 */
	public String getBranch() {
		return branch;
	}

	/**
	 * TODO method description getClientNumber
	 * @return
	 */
	public String getClientNumber() {
		return clientNumber;
	}

	/**
	 * TODO method description getConductedDate
	 * @return
	 */
	public Date getConductedDate() {
		return conductedDate;
	}

	/**
	 * TODO method description getEndFollowUpDate
	 * @return
	 */
	public Date getEndFollowUpDate() {
		return endFollowUpDate;
	}

	/**
	 * TODO method description getExaminer
	 * @return
	 */
	public String getExaminer() {
		return examiner;
	}

	/**
	 * TODO method description getExamStatus
	 * @return
	 */
	public String getExamStatus() {
		return examStatus;
	}

	/**
	 * TODO method description getFirstName
	 * @return
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * TODO method description getLaboratory
	 * @return
	 */
	public String getLaboratory() {
		return laboratory;
	}

	/**
	 * TODO method description getLastName
	 * @return
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * TODO method description getMedicalExamDetails
	 * @return
	 */
	public ArrayList getMedicalExamDetails() {
		return medicalExamDetails;
	}

	/**
	 * TODO method description getMemoDate
	 * @return
	 */
	public Date getMemoDate() {
		return memoDate;
	}

	/**
	 * TODO method description getReceivedDate
	 * @return
	 */
	public Date getReceivedDate() {
		return receivedDate;
	}

	/**
	 * TODO method description getReferenceNumber
	 * @return
	 */
	public String getReferenceNumber() {
		return referenceNumber;
	}

	/**
	 * TODO method description getRequestDate
	 * @return
	 */
	public Date getRequestDate() {
		return requestDate;
	}

	/**
	 * TODO method description getStartFollowUpDate
	 * @return
	 */
	public Date getStartFollowUpDate() {
		return startFollowUpDate;
	}

	/**
	 * TODO method description getTestName
	 * @return
	 */
	public String getTestName() {
		return testName;
	}

	/**
	 * TODO method description getType
	 * @return
	 */
	public String getType() {
		return type;
	}

	/**
	 * TODO method description isUnassigned
	 * @return
	 */
	public boolean isUnassigned() {
		return unassigned;
	}

	/**
	 * TODO method description setBranch
	 * @param string
	 */
	public void setBranch(String string) {
		branch = string;
	}

	/**
	 * TODO method description setClientNumber
	 * @param string
	 */
	public void setClientNumber(String string) {
		clientNumber = string;
	}

	/**
	 * TODO method description setConductedDate
	 * @param date
	 */
	public void setConductedDate(Date date) {
		conductedDate = date;
	}

	/**
	 * TODO method description setEndFollowUpDate
	 * @param date
	 */
	public void setEndFollowUpDate(Date date) {
		endFollowUpDate = date;
	}

	/**
	 * TODO method description setExaminer
	 * @param string
	 */
	public void setExaminer(String string) {
		examiner = string;
	}

	/**
	 * TODO method description setExamStatus
	 * @param string
	 */
	public void setExamStatus(String string) {
		examStatus = string;
	}

	/**
	 * TODO method description setFirstName
	 * @param string
	 */
	public void setFirstName(String string) {
		firstName = string;
	}

	/**
	 * TODO method description setLaboratory
	 * @param string
	 */
	public void setLaboratory(String string) {
		laboratory = string;
	}

	/**
	 * TODO method description setLastName
	 * @param string
	 */
	public void setLastName(String string) {
		lastName = string;
	}

	/**
	 * TODO method description setMedicalExamDetails
	 * @param list
	 */
	public void setMedicalExamDetails(ArrayList list) {
		medicalExamDetails = list;
	}

	/**
	 * TODO method description setMemoDate
	 * @param date
	 */
	public void setMemoDate(Date date) {
		memoDate = date;
	}

	/**
	 * TODO method description setReceivedDate
	 * @param date
	 */
	public void setReceivedDate(Date date) {
		receivedDate = date;
	}

	/**
	 * TODO method description setReferenceNumber
	 * @param string
	 */
	public void setReferenceNumber(String string) {
		referenceNumber = string;
	}

	/**
	 * TODO method description setRequestDate
	 * @param date
	 */
	public void setRequestDate(Date date) {
		requestDate = date;
	}

	/**
	 * TODO method description setStartFollowUpDate
	 * @param date
	 */
	public void setStartFollowUpDate(Date date) {
		startFollowUpDate = date;
	}

	/**
	 * TODO method description setTestName
	 * @param string
	 */
	public void setTestName(String string) {
		testName = string;
	}

	/**
	 * TODO method description setType
	 * @param string
	 */
	public void setType(String string) {
		type = string;
	}

	/**
	 * TODO method description setUnassigned
	 * @param b
	 */
	public void setUnassigned(boolean b) {
		unassigned = b;
	}

}
