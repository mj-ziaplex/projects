package com.slocpi.ium.data;

import java.util.Date;

/**
 * Container for Client Type reference information.
 * @author Engel 
 */
public class UWAssessmentData {

	private String referenceNumber;
	private String remarks;
	private String createdBy;
	private Date createDate;
	private String updatedBy;
	private Date updateDate;
	private String analysis;
	private String postedBy;
	private Date postedDate;
	private String finalDecision; 

	/**
	 * Sets the reference number attribute.
	 * @param referenceNumber reference number
	 */  
	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}
	
	/**
	 * Retrieves the reference number attribute.
	 * @return String reference number
	 */
	public String getReferenceNumber() {
		return referenceNumber;
	}


	
	/**
	 * Sets the remarks attribute.
	 * @param remarks remarks
	 */  
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
		
	/**
	 * Retrieves the remarks attribute.
	 * @return String remarks
	 */
	public String getRemarks() {
		return remarks;
	}

	/**
	 * Sets the created date attribute.
	 * @param createDate date when the record was created
	 */ 
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	
	/**
	 * Retrieves the created date attribute.
	 * @return Date date when the record was created
	 */
	public Date getCreateDate() {
		return createDate;
	}

	/**
	 * Sets the created by attribute.
	 * @param createdBy user who created the record
	 */ 
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * Retrieves the created by attribute.
	 * @return String user who created the record
	 */
	public String getCreatedBy() {
		return createdBy;
	}

	/**
	 * Sets the updated date attribute.
	 * @param updateDate date when the record was last updated
	 */ 
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
	
	/**
	 * Retrieves the updated date attribute.
	 * @return Date date when the record was last updated
	 */
	public Date getUpdateDate() {
		return updateDate;
	}

	/**
	 * Sets the updated by attribute.
	 * @param updatedBy user who last updated the record
	 */ 
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	
	/**
	 * Retrieves the updated by attribute.
	 * @return String user who last updated the record
	 */
	public String getUpdatedBy() {
		return updatedBy;
	}


	/**
	 * @return
	 */
	public String getAnalysis() {
		return analysis;
	}

	/**
	 * @param string
	 */
	public void setAnalysis(String string) {
		analysis = string;
	}
	
	public String getFinalDecision() {
		return finalDecision;
	}
	
	public void setFinalDecision(String string) {
		finalDecision = string;
	}

	/**
	 * @return
	 */
	public Date getPostedDate() {
		return postedDate;
	}

	/**
	 * @param date
	 */
	public void setPostedDate(Date date) {
		postedDate = date;
	}

	/**
	 * @return
	 */
	public String getPostedBy() {
		return postedBy;
	}

	/**
	 * @param string
	 */
	public void setPostedBy(String string) {
		postedBy = string;
	}

}
