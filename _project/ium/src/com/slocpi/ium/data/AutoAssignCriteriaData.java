package com.slocpi.ium.data;

import java.util.Date;

/**
 * Container for Auto Assign Criteria reference information.
 * @author Engel 
 */
public class AutoAssignCriteriaData {

	private long criteriaId;
	private String fieldCode;
	private String fieldDesc;
	private boolean notificationSent;	
	private String createdBy;
	private Date createDate;
	private String updatedBy;
	private Date updateDate;

	/**
	 * Sets the criteria id attribute.
	 * @param criteriaId criteria id
	 */  
	public void setCriteriaId(long criteriaId) {
		this.criteriaId = criteriaId;
	}
	
	/**
	 * Retrieves the criteria id attribute.
	 * @return long criteria id
	 */
	public long getCriteriaId() {
		return criteriaId;
	}

	/**
	 * Sets the field code attribute.
	 * @param fieldCode field code
	 */  
	public void setFieldCode(String fieldCode) {
		this.fieldCode = fieldCode;
	}
	
	/**
	 * Retrieves the field code attribute.
	 * @return String field code
	 */
	public String getFieldCode() {
		return fieldCode;
	}
	
	/**
	 * Sets the field description attribute.
	 * @param fieldDesc field description
	 */  
	public void setFieldDesc(String fieldDesc) {
		this.fieldDesc = fieldDesc;
	}
		
	/**
	 * Retrieves the field description attribute.
	 * @return String field description
	 */
	public String getFieldDesc() {
		return fieldDesc;
	}

	/**
	 * Sets the notification indicator attribute.
	 * @param notificationSent notification indicator
	 */  
	public void setNotificationSent(boolean notificationSent) {
		this.notificationSent = notificationSent;
	}
	
	/**
	 * Retrieves the notification indicator attribute.
	 * @return boolean notification indicator
	 */
	public boolean getNotificationSent() {
		return notificationSent;
	}

	/**
	 * Sets the created date attribute.
	 * @param createDate date when the record was created
	 */ 
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	
	/**
	 * Retrieves the created date attribute.
	 * @return Date date when the record was created
	 */
	public Date getCreateDate() {
		return createDate;
	}

	/**
	 * Sets the created by attribute.
	 * @param createdBy user who created the record
	 */ 
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * Retrieves the created by attribute.
	 * @return String user who created the record
	 */
	public String getCreatedBy() {
		return createdBy;
	}

	/**
	 * Sets the updated date attribute.
	 * @param updateDate date when the record was last updated
	 */ 
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
	
	/**
	 * Retrieves the updated date attribute.
	 * @return Date date when the record was last updated
	 */
	public Date getUpdateDate() {
		return updateDate;
	}

	/**
	 * Sets the updated by attribute.
	 * @param updatedBy user who last updated the record
	 */ 
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	
	/**
	 * Retrieves the updated by attribute.
	 * @return String user who last updated the record
	 */
	public String getUpdatedBy() {
		return updatedBy;
	}
}
