/**
 * MedicalExamBillingData.java				Copyright (c) 2004 Pointwest Technologies Corp.
 * TODO DOCUMENT ME!
 * 
 * @author aditalo	 @date Mar 12, 2004
 */
package com.slocpi.ium.data;

/**
 * TODO DOCUMENT ME!
 * 
 * @author aditalo		Copyright (c) 2004 Pointwest Technologies Corp.
 * @date Mar 12, 2004
 */
public class MedicalExamBillingData {
	private String name;
	private String subName;
	private String branch;
	private String examDate;
	private String examType;
	private String policyNumber;
	private String insured;
	private String lob;
	private String reason;
	private String examPlace;
	private String examFee;
	private String taxWithheld;
	private String chargeableAmt;
	private String payableAmt;
	/**
	 * @return
	 */
	public String getBranch() {
		return branch;
	}

	/**
	 * @return
	 */
	public String getChargeableAmt() {
		return chargeableAmt;
	}

	/**
	 * @return
	 */
	public String getExamDate() {
		return examDate;
	}

	/**
	 * @return
	 */
	public String getExamFee() {
		return examFee;
	}

	/**
	 * @return
	 */
	public String getExamPlace() {
		return examPlace;
	}

	/**
	 * @return
	 */
	public String getExamType() {
		return examType;
	}

	/**
	 * @return
	 */
	public String getInsured() {
		return insured;
	}

	/**
	 * @return
	 */
	public String getLob() {
		return lob;
	}

	/**
	 * @return
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return
	 */
	public String getPayableAmt() {
		return payableAmt;
	}

	/**
	 * @return
	 */
	public String getPolicyNumber() {
		return policyNumber;
	}

	/**
	 * @return
	 */
	public String getReason() {
		return reason;
	}

	/**
	 * @return
	 */
	public String getTaxWithheld() {
		return taxWithheld;
	}

	/**
	 * @param string
	 */
	public void setBranch(String string) {
		branch = string;
	}

	/**
	 * @param string
	 */
	public void setChargeableAmt(String string) {
		chargeableAmt = string;
	}

	/**
	 * @param string
	 */
	public void setExamDate(String string) {
		examDate = string;
	}

	/**
	 * @param string
	 */
	public void setExamFee(String string) {
		examFee = string;
	}

	/**
	 * @param string
	 */
	public void setExamPlace(String string) {
		examPlace = string;
	}

	/**
	 * @param string
	 */
	public void setExamType(String string) {
		examType = string;
	}

	/**
	 * @param string
	 */
	public void setInsured(String string) {
		insured = string;
	}

	/**
	 * @param string
	 */
	public void setLob(String string) {
		lob = string;
	}

	/**
	 * @param string
	 */
	public void setName(String string) {
		name = string;
	}

	/**
	 * @param string
	 */
	public void setPayableAmt(String string) {
		payableAmt = string;
	}

	/**
	 * @param string
	 */
	public void setPolicyNumber(String string) {
		policyNumber = string;
	}

	/**
	 * @param string
	 */
	public void setReason(String string) {
		reason = string;
	}

	/**
	 * @param string
	 */
	public void setTaxWithheld(String string) {
		taxWithheld = string;
	}

	/**
	 * @return
	 */
	public String getSubName()
	{
		return subName;
	}

	/**
	 * @param string
	 */
	public void setSubName(String string)
	{
		subName = string;
	}

}
