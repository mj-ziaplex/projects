/*
 * Created on Feb 19, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.data;

/**
 * @author Nic Decapia
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class ApplicationsByStatusData
{
	private String groupName = new String();
	private String statusDesc = new String();
	private String referenceNumber = new String();
	private String statusDate = new String();
	private int countAR = 0;
	
	/**
	 * @return
	 */
	public int getCountAR()
	{
		return countAR;
	}

	/**
	 * @return
	 */
	public String getReferenceNumber()
	{
		return referenceNumber;
	}

	/**
	 * @return
	 */
	public String getStatusDate()
	{
		return statusDate;
	}

	/**
	 * @return
	 */
	public String getStatusDesc()
	{
		return statusDesc;
	}

	/**
	 * @param i
	 */
	public void setCountAR(int i)
	{
		countAR = i;
	}

	/**
	 * @param string
	 */
	public void setReferenceNumber(String string)
	{
		referenceNumber = string;
	}

	/**
	 * @param date
	 */
	public void setStatusDate(String date)
	{
		statusDate = date;
	}

	/**
	 * @param string
	 */
	public void setStatusDesc(String string)
	{
		statusDesc = string;
	}

	/**
	 * @return
	 */
	public String getGroupName()
	{
		return groupName;
	}

	/**
	 * @param string
	 */
	public void setGroupName(String string)
	{
		groupName = string;
	}

}
