/*
 * Created on Dec 22, 2003
 * package name = com.slocpi.ium.data
 * file name    = MedicalNotes.java
 */
package com.slocpi.ium.data;

import java.util.Date;

/**
 * Contains information pertaining to medical notes of a doctor or an underwriter.
 * @author Engel
 * 
 */
public class MedicalNotes {

	public long medicalNotesId;
	public String referenceNumber;
	public String postedBy;
	public Date postDate;
	public String notes;
	public String recipient;
	/**
	 * TODO method description getMedicalNotesId
	 * @return
	 */
	public long getMedicalNotesId() {
		return medicalNotesId;
	}

	/**
	 * TODO method description getNotes
	 * @return
	 */
	public String getNotes() {
		return notes;
	}

	/**
	 * TODO method description getPostDate
	 * @return
	 */
	public Date getPostDate() {
		return postDate;
	}

	/**
	 * TODO method description getPostedBy
	 * @return
	 */
	public String getPostedBy() {
		return postedBy;
	}

	/**
	 * TODO method description getReferenceNumber
	 * @return
	 */
	public String getReferenceNumber() {
		return referenceNumber;
	}

	/**
	 * TODO method description setMedicalNotesId
	 * @param l
	 */
	public void setMedicalNotesId(long l) {
		medicalNotesId = l;
	}

	/**
	 * TODO method description setNotes
	 * @param string
	 */
	public void setNotes(String string) {
		notes = string;
	}

	/**
	 * TODO method description setPostDate
	 * @param date
	 */
	public void setPostDate(Date date) {
		postDate = date;
	}

	/**
	 * TODO method description setPostedBy
	 * @param string
	 */
	public void setPostedBy(String string) {
		postedBy = string;
	}

	/**
	 * TODO method description setReferenceNumber
	 * @param string
	 */
	public void setReferenceNumber(String string) {
		referenceNumber = string;
	}

	/**
	 * @return
	 */
	public String getRecipient() {
		return recipient;
	}

	/**
	 * @param string
	 */
	public void setRecipient(String string) {
		recipient = string;
	}

}
