package com.slocpi.ium.data;

import java.util.Date;

/**
 * Container for Roles reference information.
 * @author Engel 
 */
public class RolesData {

	private String rolesId;
	private String rolesDesc;	
	private String createdBy;
	private Date createDate;
	private String updatedBy;
	private Date updateDate;
		
	/**
	 * Sets the created date attribute.
	 * @param createDate date when the record was created
	 */ 
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	
	/**
	 * Retrieves the created date attribute.
	 * @return Date date when the record was created
	 */
	public Date getCreateDate() {
		return createDate;
	}

	/**
	 * Sets the created by attribute.
	 * @param createdBy user who created the record
	 */ 
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * Retrieves the created by attribute.
	 * @return String user who created the record
	 */
	public String getCreatedBy() {
		return createdBy;
	}

	/**
	 * Sets the role id attribute.
	 * @param rolesId role id
	 */ 
	public void setRolesId(String rolesId) {
		this.rolesId = rolesId;
	}

	/**
	 * Retrieves the role id attribute.
	 * @return String role id
	 */
	public String getRolesId() {
		return rolesId;
	}

	/**
	 * Sets the role description attribute.
	 * @param rolesDesc role description
	 */ 
	public void setRolesDesc(String sectionDesc) {
		this.rolesDesc = sectionDesc;
	}

	/**
	 * Retrieves the role description attribute.
	 * @return String role description
	 */
	public String getRolesDesc() {
		return rolesDesc;
	}
	
	/**
	 * Sets the updated date attribute.
	 * @param updateDate date when the record was last updated
	 */ 
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
	
	/**
	 * Retrieves the updated date attribute.
	 * @return Date date when the record was last updated
	 */
	public Date getUpdateDate() {
		return updateDate;
	}

	/**
	 * Sets the updated by attribute.
	 * @param updatedBy user who last updated the record
	 */ 
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	
	/**
	 * Retrieves the updated by attribute.
	 * @return String user who last updated the record
	 */
	public String getUpdatedBy() {
		return updatedBy;
	}
}
