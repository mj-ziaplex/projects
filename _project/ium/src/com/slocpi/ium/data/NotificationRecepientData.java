/*
 * Created on Dec 23, 2003
 * package name = com.slocpi.ium.data
 * file name    = NotificationRecepientData.java
 */
package com.slocpi.ium.data;

/**
 * Container for Notification Recepient reference information.
 * @author Engel
 * 
 */
public class NotificationRecepientData {

	private long notificationId;
	private long roleId;
	private String notificationType;
	
	/**
	 * TODO method description getNotificationId
	 * @return
	 */
	public long getNotificationId() {
		return notificationId;
	}

	/**
	 * TODO method description getNotificationType
	 * @return
	 */
	public String getNotificationType() {
		return notificationType;
	}

	/**
	 * TODO method description getRoleId
	 * @return
	 */
	public long getRoleId() {
		return roleId;
	}

	/**
	 * TODO method description setNotificationId
	 * @param l
	 */
	public void setNotificationId(long l) {
		notificationId = l;
	}

	/**
	 * TODO method description setNotificationType
	 * @param string
	 */
	public void setNotificationType(String string) {
		notificationType = string;
	}

	/**
	 * TODO method description setRoleId
	 * @param l
	 */
	public void setRoleId(long l) {
		roleId = l;
	}

}
