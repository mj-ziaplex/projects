package com.slocpi.ium.data;

import java.util.Date;

/**
 * Container for Section reference information.
 * @author Engel 
 */
public class SectionData {
	
	private String sectionId;
	private String sectionDesc;
	private String createdBy;
	private Date createDate;
	private String updatedBy;
	private Date updateDate;
		
	/**
	 * Sets the created date attribute.
	 * @param createDate date when the record was created
	 */ 
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	
	/**
	 * Retrieves the created date attribute.
	 * @return Date date when the record was created
	 */
	public Date getCreateDate() {
		return createDate;
	}

	/**
	 * Sets the created by attribute.
	 * @param createdBy user who created the record
	 */ 
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * Retrieves the created by attribute.
	 * @return String user who created the record
	 */
	public String getCreatedBy() {
		return createdBy;
	}

	/**
	 * Sets the section id attribute.
	 * @param sectionId section id
	 */ 
	public void setSectionId(String sectionId) {
		this.sectionId = sectionId;
	}

	/**
	 * Retrieves the section id attribute.
	 * @return String section id
	 */
	public String getSectionId() {
		return sectionId;
	}

	/**
	 * Sets the section description attribute.
	 * @param sectionDesc section description
	 */ 
	public void setSectionDesc(String sectionDesc) {
		this.sectionDesc = sectionDesc;
	}

	/**
	 * Retrieves the section description attribute.
	 * @return String section description
	 */
	public String getSectionDesc() {
		return sectionDesc;
	}
	
	/**
	 * Sets the updated date attribute.
	 * @param updateDate date when the record was last updated
	 */ 
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
	
	/**
	 * Retrieves the updated date attribute.
	 * @return Date date when the record was last updated
	 */
	public Date getUpdateDate() {
		return updateDate;
	}

	/**
	 * Sets the updated by attribute.
	 * @param updatedBy user who last updated the record
	 */ 
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	
	/**
	 * Retrieves the updated by attribute.
	 * @return String user who last updated the record
	 */
	public String getUpdatedBy() {
		return updatedBy;
	}

}
