/*
 * Created on Jan 26, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.data;

/**
 * @author nic.decapia
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class RequirementsOrderedFilterData {
	private String startDate = new String();
	private String endDate = new String();
	private int reportType = 0;
	
	private SunLifeOfficeData branch = new SunLifeOfficeData();
	private RequirementData requirement = new RequirementData();
	private UserProfileData user = new UserProfileData();
	
	public final static int TYPE_BRANCH = 0;
	public final static int TYPE_REQUIREMENT = 1;
	public final static int TYPE_USER = 2;
	/**
	 * @return
	 */
	public SunLifeOfficeData getBranch() {
		return branch;
	}

	/**
	 * @return
	 */
	public String getEndDate() {
		return endDate;
	}

	/**
	 * @return
	 */
	public int getReportType() {
		return reportType;
	}

	/**
	 * @return
	 */
	public RequirementData getRequirement() {
		return requirement;
	}

	/**
	 * @return
	 */
	public String getStartDate() {
		return startDate;
	}

	/**
	 * @return
	 */
	public UserProfileData getUser() {
		return user;
	}

	/**
	 * @param data
	 */
	public void setBranch(SunLifeOfficeData data) {
		branch = data;
	}

	/**
	 * @param date
	 */
	public void setEndDate(String date) {
		endDate = date;
	}

	/**
	 * @param i
	 */
	public void setReportType(int i) {
		reportType = i;
	}

	/**
	 * @param data
	 */
	public void setRequirement(RequirementData data) {
		requirement = data;
	}

	/**
	 * @param date
	 */
	public void setStartDate(String date) {
		startDate = date;
	}

	/**
	 * @param data
	 */
	public void setUser(UserProfileData data) {
		user = data;
	}

}
