package com.slocpi.ium.data;

import java.util.Date;

public class MibMemberData extends ClientData {

	private String nationality;
	private Date underwritingDate;
	private String companyCode;
	private String policyNumber;
	private String actionCode;
	private String status;

	public MibMemberData() {
		
	}
	
	public MibMemberData(ClientData clientData, ImpairmentData impairmentData) {
		this.setActionCode(impairmentData.getActionCode());
		this.setBirthDate(clientData.getBirthDate());
		this.setBirthLocation(clientData.getBirthLocation());
		this.setGivenName(clientData.getGivenName());
		this.setLastName(clientData.getLastName());
		this.setMiddleName(clientData.getMiddleName());
		this.setOtherGivenName(clientData.getOtherGivenName());
		this.setOtherMiddleName(clientData.getOtherMiddleName());
		this.setPolicyNumber(impairmentData.getReferenceNumber());
		this.setSex(clientData.getSex());
		this.setSuffix(clientData.getSuffix());
		this.setTitle(clientData.getTitle());
	}

	public String getNationality() {
		return nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	public Date getUnderwritingDate() {
		return underwritingDate;
	}

	public void setUnderwritingDate(Date underwritingDate) {
		this.underwritingDate = underwritingDate;
	}

	public String getCompanyCode() {
		return companyCode;
	}

	public void setCompanyCode(String companyCode) {
		this.companyCode = companyCode;
	}

	public String getPolicyNumber() {
		return policyNumber;
	}

	public void setPolicyNumber(String policyNumber) {
		this.policyNumber = policyNumber;
	}

	public String getActionCode() {
		return actionCode;
	}

	public void setActionCode(String actionCode) {
		this.actionCode = actionCode;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
}
