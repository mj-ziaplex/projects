/*
 * Created on Dec 22, 2003
 * package name = com.slocpi.ium.data
 * file name    = PolicyRequirementsData.java
 */
package com.slocpi.ium.data;

import java.util.Date;

/**
 * Contains information pertaining to a policy requirement
 * @author Engel
 * 
 */
public class PolicyRequirementsData {
	private long sequenceNumber;
	private String referenceNumber;
	private long requirementId;
	private String requirementCode;
	private String level;
	private Date ABACUScreateDate;
	private boolean completedRequirementInd;
	private StatusData status;
	private Date statusDate;
	private String ABACUSUpdatedBy;
	private Date ABACUSUpdateDate;
	private String clientType;
	private String designation;
	private Date testDate;
	private String testResultCode;
	private String testResultDesc;
	private boolean resolveInd;
	private long followUpNumber;
	private Date followUpDate;
	private Date validityDate;
	private boolean paidInd;
	private boolean newTestOnly;
	private Date orderDate;
	private Date dateSent;
	private boolean CCASuggestInd;
	private Date receiveDate;
	private boolean autoOrderInd;
	private boolean fldCommentInd;
	private String comments;
	private String createdBy;
	private Date createDate;
	private String updatedBy;
	private Date updateDate;
	
	private String clientId;
	private String reqDesc;
	
	private String xmlRecord;
		
	private String imageRef;
	private Date scanDate;
	private String reqtCountInd;
	private String reqtFollowUp;
	private String commentTo;

	/**
	 * TODO method description getABACUScreateDate
	 * @return
	 */
	public Date getABACUScreateDate() {
		return ABACUScreateDate;
	}

	/**
	 * TODO method description getABACUSUpdateDate
	 * @return
	 */
	public Date getABACUSUpdateDate() {
		return ABACUSUpdateDate;
	}

	/**
	 * TODO method description getAutoOrderInd
	 * @return
	 */
	public boolean getAutoOrderInd() {
		return autoOrderInd;
	}

	/**
	 * TODO method description getCCASuggestInd
	 * @return
	 */
	public boolean getCCASuggestInd() {
		return CCASuggestInd;
	}

	/**
	 * TODO method description getClientType
	 * @return
	 */
	public String getClientType() {
		return clientType;
	}

	/**
	 * TODO method description getComments
	 * @return
	 */
	public String getComments() {
		return comments;
	}

	/**
	 * TODO method description isCompletedRequirementInd
	 * @return
	 */
	public boolean isCompletedRequirementInd() {
		return completedRequirementInd;
	}

	/**
	 * TODO method description getCreateDate
	 * @return
	 */
	public Date getCreateDate() {
		return createDate;
	}

	/**
	 * TODO method description getCreatedBy
	 * @return
	 */
	public String getCreatedBy() {
		return createdBy;
	}

	/**
	 * TODO method description getDateSent
	 * @return
	 */
	public Date getDateSent() {
		return dateSent;
	}

	/**
	 * TODO method description getDesignation
	 * @return
	 */
	public String getDesignation() {
		return designation;
	}

	/**
	 * TODO method description getFldCommentInd
	 * @return
	 */
	public boolean getFldCommentInd() {
		return fldCommentInd;
	}

	/**
	 * TODO method description getFollowUpDate
	 * @return
	 */
	public Date getFollowUpDate() {
		return followUpDate;
	}

	/**
	 * TODO method description getFollowUpNumber
	 * @return
	 */
	public long getFollowUpNumber() {
		return followUpNumber;
	}

	/**
	 * TODO method description getLevel
	 * @return
	 */
	public String getLevel() {
		return level;
	}

	/**
	 * TODO method description getNewTestOnly
	 * @return
	 */
	public boolean getNewTestOnly() {
		return newTestOnly;
	}

	/**
	 * TODO method description getOrderDate
	 * @return
	 */
	public Date getOrderDate() {
		return orderDate;
	}

	/**
	 * TODO method description isPaidInd
	 * @return
	 */
	public boolean isPaidInd() {
		return paidInd;
	}

	/**
	 * TODO method description getReceiveDate
	 * @return
	 */
	public Date getReceiveDate() {
		return receiveDate;
	}

	/**
	 * TODO method description getReferenceNumber
	 * @return
	 */
	public String getReferenceNumber() {
		return referenceNumber;
	}

	/**
	 * TODO method description getRequirementCode
	 * @return
	 */
	public String getRequirementCode() {
		return requirementCode;
	}

	/**
	 * TODO method description getRequirementId
	 * @return
	 */
	public long getRequirementId() {
		return requirementId;
	}

	/**
	 * TODO method description isResolveInd
	 * @return
	 */
	public boolean isResolveInd() {
		return resolveInd;
	}

	/**
	 * TODO method description getSequenceNumber
	 * @return
	 */
	public long getSequenceNumber() {
		return sequenceNumber;
	}

	/**
	 * TODO method description getStatus
	 * @return
	 */
	public StatusData getStatus() {
		return status;
	}

	/**
	 * TODO method description getStatusDate
	 * @return
	 */
	public Date getStatusDate() {
		return statusDate;
	}

	/**
	 * TODO method description getTestDate
	 * @return
	 */
	public Date getTestDate() {
		return testDate;
	}

	/**
	 * TODO method description getTestResultCode
	 * @return
	 */
	public String getTestResultCode() {
		return testResultCode;
	}

	/**
	 * TODO method description getTestResultDesc
	 * @return
	 */
	public String getTestResultDesc() {
		return testResultDesc;
	}

	/**
	 * TODO method description getUpdateDate
	 * @return
	 */
	public Date getUpdateDate() {
		return updateDate;
	}

	/**
	 * TODO method description getUpdatedBy
	 * @return
	 */
	public String getUpdatedBy() {
		return updatedBy;
	}

	/**
	 * TODO method description getValidityDate
	 * @return
	 */
	public Date getValidityDate() {
		return validityDate;
	}

	/**
	 * TODO method description setABACUScreateDate
	 * @param date
	 */
	public void setABACUScreateDate(Date date) {
		ABACUScreateDate = date;
	}

	/**
	 * TODO method description setABACUSUpdateDate
	 * @param date
	 */
	public void setABACUSUpdateDate(Date date) {
		ABACUSUpdateDate = date;
	}

	/**
	 * TODO method description setAutoOrderInd
	 * @param string
	 */
	public void setAutoOrderInd(boolean b) {
		autoOrderInd = b;
	}

	/**
	 * TODO method description setCCASuggestInd
	 * @param string
	 */
	public void setCCASuggestInd(boolean b) {
		CCASuggestInd = b;
	}

	/**
	 * TODO method description setClientType
	 * @param string
	 */
	public void setClientType(String string) {
		clientType = string;
	}

	/**
	 * TODO method description setComments
	 * @param string
	 */
	public void setComments(String string) {
		comments = string;
	}

	/**
	 * TODO method description setCompletedRequirementInd
	 * @param b
	 */
	public void setCompletedRequirementInd(boolean b) {
		completedRequirementInd = b;
	}

	/**
	 * TODO method description setCreateDate
	 * @param date
	 */
	public void setCreateDate(Date date) {
		createDate = date;
	}

	/**
	 * TODO method description setCreatedBy
	 * @param string
	 */
	public void setCreatedBy(String string) {
		createdBy = string;
	}

	/**
	 * TODO method description setDateSent
	 * @param date
	 */
	public void setDateSent(Date date) {
		dateSent = date;
	}

	/**
	 * TODO method description setDesignation
	 * @param string
	 */
	public void setDesignation(String string) {
		designation = string;
	}

	/**
	 * TODO method description setFldCommentInd
	 * @param string
	 */
	public void setFldCommentInd(boolean b) {
		fldCommentInd = b;
	}

	/**
	 * TODO method description setFollowUpDate
	 * @param date
	 */
	public void setFollowUpDate(Date date) {
		followUpDate = date ;
	}

	/**
	 * TODO method description setFollowUpNumber
	 * @param l
	 */
	public void setFollowUpNumber(long l) {
		followUpNumber = l;
	}

	/**
	 * TODO method description setLevel
	 * @param string
	 */
	public void setLevel(String string) {
		level = string;
	}

	/**
	 * TODO method description setNewTestOnly
	 * @param string
	 */
	public void setNewTestOnly(boolean b) {
		newTestOnly = b;
	}

	/**
	 * TODO method description setOrderDate
	 * @param date
	 */
	public void setOrderDate(Date date) {
		orderDate = date;
	}

	/**
	 * TODO method description setPaidInd
	 * @param b
	 */
	public void setPaidInd(boolean b) {
		paidInd = b;
	}

	/**
	 * TODO method description setReceiveDate
	 * @param date
	 */
	public void setReceiveDate(Date date) {
		receiveDate = date;
	}

	/**
	 * TODO method description setReferenceNumber
	 * @param string
	 */
	public void setReferenceNumber(String string) {
		referenceNumber = string;
	}

	/**
	 * TODO method description setRequirementCode
	 * @param string
	 */
	public void setRequirementCode(String string) {
		requirementCode = string;
	}

	/**
	 * TODO method description setRequirementId
	 * @param l
	 */
	public void setRequirementId(long l) {
		requirementId = l;
	}

	/**
	 * TODO method description setResolveInd
	 * @param b
	 */
	public void setResolveInd(boolean b) {
		resolveInd = b;
	}

	/**
	 * TODO method description setSequenceNumber
	 * @param l
	 */
	public void setSequenceNumber(long l) {
		sequenceNumber = l;
	}

	/**
	 * TODO method description setStatus
	 * @param string
	 */
	public void setStatus(StatusData statusData) {
		status = statusData;
	}

	/**
	 * TODO method description setStatusDate
	 * @param date
	 */
	public void setStatusDate(Date date) {
		statusDate = date;
	}

	/**
	 * TODO method description setTestDate
	 * @param date
	 */
	public void setTestDate(Date date) {
		testDate = date;
	}

	/**
	 * TODO method description setTestResultCode
	 * @param string
	 */
	public void setTestResultCode(String string) {
		testResultCode = string;
	}

	/**
	 * TODO method description setTestResultDesc
	 * @param string
	 */
	public void setTestResultDesc(String string) {
		testResultDesc = string;
	}

	/**
	 * TODO method description setUpdateDate
	 * @param date
	 */
	public void setUpdateDate(Date date) {
		updateDate = date;
	}

	/**
	 * TODO method description setUpdatedBy
	 * @param string
	 */
	public void setUpdatedBy(String string) {
		updatedBy = string;
	}

	/**
	 * TODO method description setValidityDate
	 * @param date
	 */
	public void setValidityDate(Date date) {
		validityDate = date;
	}

	/**
	 * @return
	 */
	public String getABACUSUpdatedBy() {
		return ABACUSUpdatedBy;
	}

	/**
	 * @param l
	 */
	public void setABACUSUpdatedBy(String string) {
		ABACUSUpdatedBy = string;
	}

	/**
	 * @return
	 */
	public String getClientId() {
		return clientId;
	}

	/**
	 * @param l
	 */
	public void setClientId(String string) {
		clientId = string;
	}

	/**
	 * @return
	 */
	public String getReqDesc() {
		return reqDesc;
	}

	/**
	 * @param l
	 */
	public void setReqDesc(String string) {
		reqDesc = string;
	}	
	/**
	 * @return
	 */
	public String getXmlRecord() {
		return xmlRecord;
	}

	/**
	 * @param string
	 */
	public void setXmlRecord(String string) {
		xmlRecord = string;
	}
	
	public String getImageRef() {
		return imageRef;
	}

	public void setImageRef(String imageRef) {
		this.imageRef = imageRef;
	}

	public Date getScanDate() {
		return scanDate;
	}

	public void setScanDate(Date scanDate) {
		this.scanDate = scanDate;
	}

	public String getReqtCountInd() {
		return reqtCountInd;
	}

	public void setReqtCountInd(String reqtCountInd) {
		this.reqtCountInd = reqtCountInd;
	}

	public String getReqtFollowUp() {
		return reqtFollowUp;
	}

	public void setReqtFollowUp(String reqtFollowUp) {
		this.reqtFollowUp = reqtFollowUp;
	}

	public String getCommentTo() {
		return commentTo;
	}

	public void setCommentTo(String commentTo) {
		this.commentTo = commentTo;
	}
}
