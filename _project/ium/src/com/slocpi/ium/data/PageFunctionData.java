/*
 * Created on Dec 22, 2003
 * package name = com.slocpi.ium.data
 * file name    = PageFunctionData.java
 */
package com.slocpi.ium.data;

/**
 * Container for Page Function reference information.
 * @author Engel
 * 
 */
public class PageFunctionData {
	
	private long pageAccessId;
	private long pageID;
	private String pageDescription;
	private long accessID;
	private String accessDescription;
	private boolean enabled;

	/**
	 * @return
	 */
	public long getAccessID() {
		return accessID;
	}

	/**
	 * @return
	 */
	public long getPageAccessId() {
		return pageAccessId;
	}

	/**
	 * @return
	 */
	public long getPageID() {
		return pageID;
	}

	/**
	 * @param l
	 */
	public void setAccessID(long l) {
		accessID = l;
	}

	/**
	 * @param l
	 */
	public void setPageAccessId(long l) {
		pageAccessId = l;
	}

	/**
	 * @param l
	 */
	public void setPageID(long l) {
		pageID = l;
	}

	/**
	 * @return
	 */
	public boolean isEnabled() {
		return enabled;
	}

	/**
	 * @param b
	 */
	public void setEnabled(boolean b) {
		enabled = b;
	}

	/**
	 * @return
	 */
	public String getAccessDescription() {
		return accessDescription;
	}

	/**
	 * @return
	 */
	public String getPageDescription() {
		return pageDescription;
	}

	/**
	 * @param string
	 */
	public void setAccessDescription(String string) {
		accessDescription = string;
	}

	/**
	 * @param string
	 */
	public void setPageDescription(String string) {
		pageDescription = string;
	}

}
