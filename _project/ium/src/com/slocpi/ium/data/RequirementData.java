/*
 * Created on Dec 18, 2003
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.data;

import java.util.Date;

/**
 * @author Cris
 *
 * Contains information pertaining to an application requirement
 * 
 */
public class RequirementData {
	private String clientType;  
	private String reqtCode;
	private String reqtDesc;
	private String level;
	private long validity;
	private String formIndicator;
	private long formID;
	private int followUpNum;
	
	private String createdBy;
	private Date createDate;
	private String updatedBy;
	private Date updateDate;
	private String testDateIndicator;

	/**
	 * @return
	 */
	public long getFormID() {
		return formID;
	}

	/**
	 * @return
	 */
	public String getFormIndicator() {
		return formIndicator;
	}

	/**
	 * @return
	 */
	public String getLevel() {
		return level;
	}

	/**
	 * @return
	 */
	public String getReqtCode() {
		return reqtCode;
	}

	/**
	 * @return
	 */
	public String getReqtDesc() {
		return reqtDesc;
	}

	/**
	 * @return
	 */
	public long getValidity() {
		return validity;
	}

	/**
	 * @param l
	 */
	public void setFormID(long l) {
		formID = l;
	}

	/**
	 * @param string
	 */
	public void setFormIndicator(String string) {
		formIndicator = string;
	}

	/**
	 * @param string
	 */
	public void setLevel(String string) {
		level = string;
	}

	/**
	 * @param string
	 */
	public void setReqtCode(String string) {
		reqtCode = string;
	}

	/**
	 * @param string
	 */
	public void setReqtDesc(String string) {
		reqtDesc = string;
	}

	/**
	 * @param l
	 */
	public void setValidity(long l) {
		validity = l;
	}

	/**
	 * @return
	 */
	public int getFollowUpNum() {
		return followUpNum;
	}

	/**
	 * @param i
	 */
	public void setFollowUpNum(int i) {
		followUpNum = i;
	}

	/**
	 * @return
	 */
	public String getClientType()
	{
		return clientType;
	}

	/**
	 * @param string
	 */
	public void setClientType(String string)
	{
		clientType = string;
	}

	/**
	 * TODO method description getCreateDate
	 * @return
	 */
	public Date getCreateDate() {
		return createDate;
	}

	/**
	 * TODO method description getCreatedBy
	 * @return
	 */
	public String getCreatedBy() {
		return createdBy;
	}

	/**
	 * TODO method description getUpdateDate
	 * @return
	 */
	public Date getUpdateDate() {
		return updateDate;
	}

	/**
	 * TODO method description getUpdatedBy
	 * @return
	 */
	public String getUpdatedBy() {
		return updatedBy;
	}

	/**
	 * TODO method description setCreateDate
	 * @param date
	 */
	public void setCreateDate(Date date) {
		createDate = date;
	}

	/**
	 * TODO method description setCreatedBy
	 * @param string
	 */
	public void setCreatedBy(String string) {
		createdBy = string;
	}

	/**
	 * TODO method description setUpdateDate
	 * @param date
	 */
	public void setUpdateDate(Date date) {
		updateDate = date;
	}

	/**
	 * TODO method description setUpdatedBy
	 * @param string
	 */
	public void setUpdatedBy(String string) {
		updatedBy = string;
	}

	/**
	 * @return
	 */
	public String getTestDateIndicator() {
		return testDateIndicator;
	}

	/**
	 * @param string
	 */
	public void setTestDateIndicator(String string) {
		testDateIndicator = string;
	}

}
