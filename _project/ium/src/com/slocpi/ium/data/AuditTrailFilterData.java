/*
 * Created on Feb 23, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.data;

import java.util.Date;

/**
 * @author Cris
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class AuditTrailFilterData {
	
	private Date startDate;
	private Date endDate;
	private long table;
	private String transactionType;
	 
	 
	/**
	 * @return
	 */
	public Date getEndDate() {
		return endDate;
	}

	/**
	 * @return
	 */
	public Date getStartDate() {
		return startDate;
	}

	/**
	 * @return
	 */
	public String getTransactionType() {
		return transactionType;
	}

	/**
	 * @param date
	 */
	public void setEndDate(Date date) {
		endDate = date;
	}

	/**
	 * @param date
	 */
	public void setStartDate(Date date) {
		startDate = date;
	}


	/**
	 * @param string
	 */
	public void setTransactionType(String string) {
		transactionType = string;
	}

	/**
	 * @return
	 */
	public long getTable() {
		return table;
	}

	/**
	 * @param l
	 */
	public void setTable(long l) {
		table = l;
	}

}
