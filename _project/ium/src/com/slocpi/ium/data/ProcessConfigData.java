/*
 * Created on Dec 23, 2003
 * package name = com.slocpi.ium.data
 * file name    = ProcessConfigData.java
 */
package com.slocpi.ium.data;

import java.util.ArrayList;
import java.util.Date;

/**
 * Container for Process Configuration reference information.
 * @author Engel
 * 
 */
public class ProcessConfigData {

	private long eventId;
	private String processCode;
	private String eventName;
	private long fromStatus;
	private long toStatus;
	private String LOB;	
	private long notification;
	private boolean notificationInd;
	private String createdBy;
	private Date createDate;
	private String updatedBy;
	private Date updateDate;
	private ArrayList roles;
	
	
	/**
	 * @return
	 */
	public Date getCreateDate() {
		return createDate;
	}

	/**
	 * @return
	 */
	public String getCreatedBy() {
		return createdBy;
	}

	/**
	 * @return
	 */
	public long getEventId() {
		return eventId;
	}

	/**
	 * @return
	 */
	public String getEventName() {
		return eventName;
	}

	/**
	 * @return
	 */
	public long getFromStatus() {
		return fromStatus;
	}

	/**
	 * @return
	 */
	public String getLOB() {
		return LOB;
	}

	/**
	 * @return
	 */
	public long getNotification() {
		return notification;
	}

	/**
	 * @return
	 */
	public boolean isNotificationInd() {
		return notificationInd;
	}

	/**
	 * @return
	 */
	public String getProcessCode() {
		return processCode;
	}

	/**
	 * @return
	 */
	public long getToStatus() {
		return toStatus;
	}

	/**
	 * @return
	 */
	public Date getUpdateDate() {
		return updateDate;
	}

	/**
	 * @return
	 */
	public String getUpdatedBy() {
		return updatedBy;
	}

	/**
	 * @param date
	 */
	public void setCreateDate(Date date) {
		createDate = date;
	}

	/**
	 * @param string
	 */
	public void setCreatedBy(String string) {
		createdBy = string;
	}

	/**
	 * @param l
	 */
	public void setEventId(long l) {
		eventId = l;
	}

	/**
	 * @param string
	 */
	public void setEventName(String string) {
		eventName = string;
	}

	/**
	 * @param l
	 */
	public void setFromStatus(long l) {
		fromStatus = l;
	}

	/**
	 * @param string
	 */
	public void setLOB(String string) {
		LOB = string;
	}

	/**
	 * @param l
	 */
	public void setNotification(long l) {
		notification = l;
	}

	/**
	 * @param b
	 */
	public void setNotificationInd(boolean b) {
		notificationInd = b;
	}

	/**
	 * @param string
	 */
	public void setProcessCode(String string) {
		processCode = string;
	}

	/**
	 * @param l
	 */
	public void setToStatus(long l) {
		toStatus = l;
	}

	/**
	 * @param date
	 */
	public void setUpdateDate(Date date) {
		updateDate = date;
	}

	/**
	 * @param string
	 */
	public void setUpdatedBy(String string) {
		updatedBy = string;
	}

	/**
	 * @return
	 */
	public ArrayList getRoles() {
		return this.roles;
	}

	/**
	 * @param list
	 */
	public void setRoles(ArrayList roles) {
		this.roles = roles;
	}

}
