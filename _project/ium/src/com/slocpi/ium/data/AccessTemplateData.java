/*
 * Created on Dec 22, 2003
 * package name = com.slocpi.ium.data
 * file name    = AccessTemplateData.java
 */
package com.slocpi.ium.data;

import java.sql.Timestamp;
import java.util.ArrayList;

/**
 * Container for Access Template reference information.
 * @author Engel
 * 
 */
public class AccessTemplateData {

	private long templateID;
	private String templateDesc;
	private RolesData role;
	private ArrayList templateDetails;
	private String createdBy;
	private Timestamp createdDate;
	private String updatedBy;
	private Timestamp updatedDate;

	/**
	 * Retrieves the template description attribute. 
	 * @return
	 */
	public String getTemplateDesc() {
		return templateDesc;
	}

	/**
	 * Retrieves the template ID attribute. 
	 * @return
	 */
	public long getTemplateID() {
		return templateID;
	}

	/**
	 * Sets the template description attribute. 
	 * @param string
	 */
	public void setTemplateDesc(String string) {
		templateDesc = string;
	}

	/**
	 * Sets the template ID attribute. 
	 * @param l
	 */
	public void setTemplateID(long l) {
		templateID = l;
	}

	/**
	 * Retreives the template details attribute. 
	 * @return
	 */
	public ArrayList getTemplateDetails() {
		return templateDetails;
	}

	/**
	 * Sets the template details attribute. 
	 * @param list
	 */
	public void setTemplateDetails(ArrayList list) {
		templateDetails = list;
	}

	/**
	 * Retrieves the roles attribute. 
	 * @return
	 */
	public RolesData getRole() {
		return role;
	}

	/**
	 * Sets the roles attribute. 
	 * @param data
	 */
	public void setRole(RolesData data) {
		role = data;
	}

	/**
	 * Retrieves the created by attribute. 
	 * @return
	 */
	public String getCreatedBy() {
		return createdBy;
	}

	/**
	 * Retrieves the created date attribute. 
	 * @return
	 */
	public Timestamp getCreatedDate() {
		return createdDate;
	}

	/**
	 * Retrieves the updated by attribute. 
	 * @return
	 */
	public String getUpdatedBy() {
		return updatedBy;
	}

	/**
	 * Retrieves the updated date attribute. 
	 * @return
	 */
	public Timestamp getUpdatedDate() {
		return updatedDate;
	}

	/**
	 * Sets the created by attribute. 
	 * @param string
	 */
	public void setCreatedBy(String string) {
		createdBy = string;
	}

	/**
	 * Sets the created date attribute. 
	 * @param timestamp
	 */
	public void setCreatedDate(Timestamp timestamp) {
		createdDate = timestamp;
	}

	/**
	 * Sets the updated by attribute. 
	 * @param string
	 */
	public void setUpdatedBy(String string) {
		updatedBy = string;
	}

	/**
	 * Sets the updated date attribute. 
	 * @param timestamp
	 */
	public void setUpdatedDate(Timestamp timestamp) {
		updatedDate = timestamp;
	}

}
