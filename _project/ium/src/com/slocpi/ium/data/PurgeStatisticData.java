package com.slocpi.ium.data;

import java.util.Date;

/**
 * Container for the purge statistic logs.
 * 
 * @author aditalo		Copyright (c) 2004 Pointwest Technologies Corp.
 * @date May 6, 2004
 */
public class PurgeStatisticData {

	private Date purgeDate;
	private Date startDate;
	private Date endDate;
	private String criteria;
	private long numberOfRecords;
	private String result;

	/**
	 * Returns the purge criteria.
	 * 
	 * @return
	 */
	public String getCriteria() {
		return criteria;
	}

	/**
	 * Returns the end date.
	 * 
	 * @return
	 */
	public Date getEndDate() {
		return endDate;
	}

	/**
	 * Returns the number of records purged.
	 * 
	 * @return
	 */
	public long getNumberOfRecords() {
		return numberOfRecords;
	}

	/**
	 * Returns the purge date.
	 * 
	 * @return
	 */
	public Date getPurgeDate() {
		return purgeDate;
	}

	/**
	 * Returns the purge result.
	 * 
	 * @return
	 */
	public String getResult() {
		return result;
	}

	/**
	 * Returns the start date.
	 * 
	 * @return
	 */
	public Date getStartDate() {
		return startDate;
	}

	/**
	 * Sets the purge criteria.
	 * 
	 * @param string
	 */
	public void setCriteria(String string) {
		criteria = string;
	}

	/**
	 * Sets the end date.
	 * 
	 * @param date
	 */
	public void setEndDate(Date date) {
		endDate = date;
	}

	/**
	 * Sets the number of records purged.
	 * 
	 * @param l
	 */
	public void setNumberOfRecords(long l) {
		numberOfRecords = l;
	}

	/**
	 * Sets the purge date.
	 * 
	 * @param date
	 */
	public void setPurgeDate(Date date) {
		purgeDate = date;
	}

	/**
	 * Sets the purge result.
	 * 
	 * @param string
	 */
	public void setResult(String string) {
		result = string;
	}

	/**
	 * Sets the start date.
	 * 
	 * @param date
	 */
	public void setStartDate(Date date) {
		startDate = date;
	}

}
