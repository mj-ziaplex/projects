package com.slocpi.ium.data;

import java.util.Date;

/**
 * Container for Client Type reference information.
 * @author Engel 
 */
public class ClientTypeData {

	private String clientTypeCode;
	private String clientTypeDesc;
	private String createdBy;
	private Date createDate;
	private String updatedBy;
	private Date updateDate;

	/**
	 * Sets the client type code attribute.
	 * @param clientTypeCode client type code
	 */  
	public void setClientTypeCode(String clientTypeCode) {
		this.clientTypeCode = clientTypeCode;
	}
	
	/**
	 * Retrieves the client type code attribute.
	 * @return String client type code
	 */
	public String getClientTypeCode() {
		return clientTypeCode;
	}
	
	/**
	 * Sets the client type description attribute.
	 * @param clientTypeDesc client type description
	 */  
	public void setClientTypeDesc(String clientTypeDesc) {
		this.clientTypeDesc = clientTypeDesc;
	}
		
	/**
	 * Retrieves the client type description attribute.
	 * @return String client type description
	 */
	public String getClientTypeDesc() {
		return clientTypeDesc;
	}

	/**
	 * Sets the created date attribute.
	 * @param createDate date when the record was created
	 */ 
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	
	/**
	 * Retrieves the created date attribute.
	 * @return Date date when the record was created
	 */
	public Date getCreateDate() {
		return createDate;
	}

	/**
	 * Sets the created by attribute.
	 * @param createdBy user who created the record
	 */ 
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * Retrieves the created by attribute.
	 * @return String user who created the record
	 */
	public String getCreatedBy() {
		return createdBy;
	}

	/**
	 * Sets the updated date attribute.
	 * @param updateDate date when the record was last updated
	 */ 
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
	
	/**
	 * Retrieves the updated date attribute.
	 * @return Date date when the record was last updated
	 */
	public Date getUpdateDate() {
		return updateDate;
	}

	/**
	 * Sets the updated by attribute.
	 * @param updatedBy user who last updated the record
	 */ 
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	
	/**
	 * Retrieves the updated by attribute.
	 * @return String user who last updated the record
	 */
	public String getUpdatedBy() {
		return updatedBy;
	}

}
