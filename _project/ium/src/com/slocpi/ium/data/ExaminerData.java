/*
 * Created on Dec 23, 2003
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.data;

import java.util.ArrayList;
import java.util.Date;

/**
 * @author Cris
 *
 * Container for Examiner reference information
 */
public class ExaminerData {
	private long examinerId;
	private String lastName;
	private String firstName;
	private String middleName;
	private String salutation;
	private String sex;
	private String rank;
	private Date rankEffectiveDate;
	private Date dateOfBirth;
	private String contactNumber;
	private String faxNumber;
	private String mobileNumber;
	private String busAddrLine1;
	private String busAddrLine2;
	private String busAddrLine3;
	private String city;
	private String province;
	private String country;
	private String zipCode;
	private String mailToBusAddrInd;
	private Date clinicHrsFrom;
	private Date clinicHrsTo;
	private String otherInsComp;
	private String preEmploymentSrvcInd;
	private String TIN; 
	private String accreditationInd;
	private Date accreditationDate;
	private String examineArea;
	private String examinePlace;
	private ArrayList specializations;
	private String createdBy;
	private Date createDate;
	private String updatedBy;
	private Date updateDate;
	
	
	

	/**
	 * @return
	 */
	public Date getAccreditationDate() {
		return accreditationDate;
	}

	/**
	 * @return
	 */
	public String getAccreditationInd() {
		return accreditationInd;
	}

	/**
	 * @return
	 */
	public String getBusAddrLine1() {
		return busAddrLine1;
	}

	/**
	 * @return
	 */
	public String getBusAddrLine2() {
		return busAddrLine2;
	}

	/**
	 * @return
	 */
	public String getBusAddrLine3() {
		return busAddrLine3;
	}

	/**
	 * @return
	 */
	public String getCity() {
		return city;
	}

	/**
	 * @return
	 */
	public Date getClinicHrsFrom() {
		return clinicHrsFrom;
	}

	/**
	 * @return
	 */
	public Date getClinicHrsTo() {
		return clinicHrsTo;
	}

	/**
	 * @return
	 */
	public String getContactNumber() {
		return contactNumber;
	}

	/**
	 * @return
	 */
	public String getCountry() {
		return country;
	}

	/**
	 * @return
	 */
	public Date getCreateDate() {
		return createDate;
	}

	/**
	 * @return
	 */
	public String getCreatedBy() {
		return createdBy;
	}

	/**
	 * @return
	 */
	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	/**
	 * @return
	 */
	public String getExamineArea() {
		return examineArea;
	}

	
//	/**
//	 * @return
//	 */
//	public long getExaminerId() {
//		return examinerId;
//	}

	/**
	 * @return
	 */
	public String getFaxNumber() {
		return faxNumber;
	}

	/**
	 * @return
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @return
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @return
	 */
	public String getMailToBusAddrInd() {
		return mailToBusAddrInd;
	}

	/**
	 * @return
	 */
	public String getMiddleName() {
		return middleName;
	}

	/**
	 * @return
	 */
	public String getOtherInsComp() {
		return otherInsComp;
	}

	/**
	 * @return
	 */
	public String getPreEmploymentSrvcInd() {
		return preEmploymentSrvcInd;
	}

	/**
	 * @return
	 */
	public String getProvince() {
		return province;
	}

	
	/**
	 * @return
	 */
	public Date getRankEffectiveDate() {
		return rankEffectiveDate;
	}

	/**
	 * @return
	 */
	public String getSalutation() {
		return salutation;
	}

	/**
	 * @return
	 */
	public String getSex() {
		return sex;
	}


	/**
	 * @return
	 */
	public Date getUpdateDate() {
		return updateDate;
	}

	/**
	 * @return
	 */
	public String getUpdatedBy() {
		return updatedBy;
	}


	/**
	 * @param date
	 */
	public void setAccreditationDate(Date date) {
		accreditationDate = date;
	}

	/**
	 * @param string
	 */
	public void setAccreditationInd(String string) {
		accreditationInd = string;
	}

	/**
	 * @param string
	 */
	public void setBusAddrLine1(String string) {
		busAddrLine1 = string;
	}

	/**
	 * @param string
	 */
	public void setBusAddrLine2(String string) {
		busAddrLine2 = string;
	}

	/**
	 * @param string
	 */
	public void setBusAddrLine3(String string) {
		busAddrLine3 = string;
	}

	/**
	 * @param string
	 */
	public void setCity(String string) {
		city = string;
	}

	/**
	 * @param date
	 */
	public void setClinicHrsFrom(Date date) {
		clinicHrsFrom = date;
	}

	/**
	 * @param date
	 */
	public void setClinicHrsTo(Date date) {
		clinicHrsTo = date;
	}

	/**
	 * @param string
	 */
	public void setContactNumber(String string) {
		contactNumber = string;
	}

	/**
	 * @param string
	 */
	public void setCountry(String string) {
		country = string;
	}

	/**
	 * @param date
	 */
	public void setCreateDate(Date date) {
		createDate = date;
	}

	/**
	 * @param string
	 */
	public void setCreatedBy(String string) {
		createdBy = string;
	}

	/**
	 * @param date
	 */
	public void setDateOfBirth(Date date) {
		dateOfBirth = date;
	}

	/**
	 * @param string
	 */
	public void setExamineArea(String string) {
		examineArea = string;
	}

	

//	/**
//	 * @param l
//	 */
//	public void setExaminerId(long l) {
//		examinerId = l;
//	}

	/**
	 * @param string
	 */
	public void setFaxNumber(String string) {
		faxNumber = string;
	}

	/**
	 * @param string
	 */
	public void setFirstName(String string) {
		firstName = string;
	}

	/**
	 * @param string
	 */
	public void setLastName(String string) {
		lastName = string;
	}

	/**
	 * @param b
	 */
	public void setMailToBusAddrInd(String string) {
		mailToBusAddrInd = string;
	}

	/**
	 * @param string
	 */
	public void setMiddleName(String string) {
		middleName = string;
	}

	/**
	 * @param string
	 */
	public void setOtherInsComp(String string) {
		otherInsComp = string;
	}

	/**
	 * @param string
	 */
	public void setPreEmploymentSrvcInd(String string) {
		preEmploymentSrvcInd = string;
	}

	/**
	 * @param string
	 */
	public void setProvince(String string) {
		province = string;
	}

	
	/**
	 * @param date
	 */
	public void setRankEffectiveDate(Date date) {
		rankEffectiveDate = date;
	}

	/**
	 * @param string
	 */
	public void setSalutation(String string) {
		salutation = string;
	}

	/**
	 * @param string
	 */
	public void setSex(String string) {
		sex = string;
	}


	/**
	 * @param date
	 */
	public void setUpdateDate(Date date) {
		updateDate = date;
	}

	/**
	 * @param string
	 */
	public void setUpdatedBy(String string) {
		updatedBy = string;
	}

	
	/**
	 * Returns the specializations.
	 * @return ArrayList
	 */
	public ArrayList getSpecializations() {
		return specializations;
	}

	/**
	 * Sets the specializations.
	 * @param specializations The specializations to set
	 */
	public void setSpecializations(ArrayList specializations) {
		this.specializations = specializations;
	}

	/**
	 * Returns the mobileNumber.
	 * @return String
	 */
	public String getMobileNumber() {
		return mobileNumber;
	}

	/**
	 * Sets the mobileNumber.
	 * @param mobileNumber The mobileNumber to set
	 */
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	/**
	 * Returns the tIN.
	 * @return String
	 */
	public String getTIN() {
		return TIN;
	}

	/**
	 * Sets the tIN.
	 * @param tIN The tIN to set
	 */
	public void setTIN(String tIN) {
		TIN = tIN;
	}

	/**
	 * Returns the rank.
	 * @return String
	 */
	public String getRank() {
		return rank;
	}

	/**
	 * Sets the rank.
	 * @param rank The rank to set
	 */
	public void setRank(String rank) {
		this.rank = rank;
	}

	/**
	 * Returns the examinePlace.
	 * @return String
	 */
	public String getExaminePlace() {
		return examinePlace;
	}

	/**
	 * Returns the zipCode.
	 * @return String
	 */
	public String getZipCode() {
		return zipCode;
	}

	/**
	 * Sets the examinePlace.
	 * @param examinePlace The examinePlace to set
	 */
	public void setExaminePlace(String examinePlace) {
		this.examinePlace = examinePlace;
	}

	/**
	 * Sets the zipCode.
	 * @param zipCode The zipCode to set
	 */
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	/**
	 * Returns the examinerId.
	 * @return long
	 */
	public long getExaminerId() {
		return examinerId;
	}

	/**
	 * Sets the examinerId.
	 * @param examinerId The examinerId to set
	 */
	public void setExaminerId(long examinerId) {
		this.examinerId = examinerId;
	}

}
