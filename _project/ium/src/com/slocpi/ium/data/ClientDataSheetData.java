/*
 * Created on Dec 22, 2003
 * package name = com.slocpi.ium.data
 * file name    = ClientDataSheetData.java
 */
package com.slocpi.ium.data;

import java.util.ArrayList;

/**
 * Contains information pertaining to the information about the applicant.
 * @author Engel
 * 
 */
public class ClientDataSheetData {

	private String referenceNumber;
	private String clientId;
	private String clientType;
	private CDSMortalityRatingData mortalityRating;
	private CDSSummaryPolicyCoverageData summaryOfPolicies;
	private ArrayList policyCoverageDetailsList;
	

	/**
	 * @return
	 */
	public String getClientId() {
		return clientId;
	}

	/**
	 * @return
	 */
	public String getClientType() {
		return clientType;
	}

	/**
	 * @return
	 */
	public CDSMortalityRatingData getMortalityRating() {
		return mortalityRating;
	}

	/**
	 * @return
	 */
	public ArrayList getPolicyCoverageDetailsList() {
		return policyCoverageDetailsList;
	}

	/**
	 * @return
	 */
	public String getReferenceNumber() {
		return referenceNumber;
	}

	/**
	 * @return
	 */
	public CDSSummaryPolicyCoverageData getSummaryOfPolicies() {
		return summaryOfPolicies;
	}

	/**
	 * @param string
	 */
	public void setClientId(String string) {
		clientId = string;
	}

	/**
	 * @param string
	 */
	public void setClientType(String string) {
		clientType = string;
	}

	/**
	 * @param data
	 */
	public void setMortalityRating(CDSMortalityRatingData data) {
		mortalityRating = data;
	}

	/**
	 * @param list
	 */
	public void setPolicyCoverageDetailsList(ArrayList list) {
		policyCoverageDetailsList = list;
	}

	/**
	 * @param string
	 */
	public void setReferenceNumber(String string) {
		referenceNumber = string;
	}

	/**
	 * @param data
	 */
	public void setSummaryOfPolicies(CDSSummaryPolicyCoverageData data) {
		summaryOfPolicies = data;
	}

}
