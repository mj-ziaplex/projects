package com.slocpi.ium.data;

import java.util.Date;

/**
 * Container for Page reference information.
 * @author Engel 
 */
public class PageData {

	private long pageId;
	private String pageDesc;
	private String createdBy;
	private Date createDate;
	private String updatedBy;
	private Date updateDate;


	/**
	 * Sets the page id attribute.
	 * @param pageId page id
	 */ 
	public void setPageId(long pageId) {
		this.pageId = pageId;
	}

	/**
	 * Retrieves the page id attribute.
	 * @return long page id
	 */
	public long getPageId() {
		return pageId;
	}

	/**
	 * Sets the page description attribute.
	 * @param pageDesc page description
	 */ 
	public void setPageDesc(String pageDesc) {
		this.pageDesc = pageDesc;
	}

	/**
	 * Retrieves the page description attribute.
	 * @return String page description
	 */
	public String getPageDesc() {
		return pageDesc;
	}

	/**
	 * Sets the created date attribute.
	 * @param createDate date when the record was created
	 */ 
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	
	/**
	 * Retrieves the created date attribute.
	 * @return Date date when the record was created
	 */
	public Date getCreateDate() {
		return createDate;
	}

	/**
	 * Sets the created by attribute.
	 * @param createdBy user who created the record
	 */ 
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * Retrieves the created by attribute.
	 * @return String user who created the record
	 */
	public String getCreatedBy() {
		return createdBy;
	}

	/**
	 * Sets the updated date attribute.
	 * @param updateDate date when the record was last updated
	 */ 
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
	
	/**
	 * Retrieves the updated date attribute.
	 * @return Date date when the record was last updated
	 */
	public Date getUpdateDate() {
		return updateDate;
	}

	/**
	 * Sets the updated by attribute.
	 * @param updatedBy user who last updated the record
	 */ 
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	
	/**
	 * Retrieves the updated by attribute.
	 * @return String user who last updated the record
	 */
	public String getUpdatedBy() {
		return updatedBy;
	}

}
