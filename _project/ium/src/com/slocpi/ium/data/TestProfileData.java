package com.slocpi.ium.data;

import java.util.Date;

/**
 * Container for Test Profile reference information.
 * @author Engel 
 */
public class TestProfileData {

	private long testId;
	private String testDesc;
	private String testType;
	private int validity;	
	private boolean taxable;
	private String createdBy;
	private Date createDate;
	private String updatedBy;
	private Date updateDate;
	private long followUpNumber;

	/**
	 * Sets the test id attribute.
	 * @param testId user-defined test code
	 */ 
	public void setTestId(long testId) {
		this.testId = testId;
	}
			
	/**
	 * Retrieves the test id attribute.
	 * @return String user-defined test code
	 */
	public long getTestId() {
		return testId;
	}

	/**
	 * Sets the test description attribute.
	 * @param testDesc description of the test code
	 */ 
	public void setTestDesc(String testDesc) {
		this.testDesc = testDesc;
	}

	/**
	 * Retrieves the test description attribute.
	 * @return String description of the test code
	 */
	public String getTestDesc() {
		return testDesc;
	}

	/**
	 * Sets the test type attribute.
	 * @param testType accepted values: M-Medical; L-Laboratory
	 */ 
	public void setTestType(String testType) {
		this.testType = testType;
	}
	
	/**
	 * Retrieves the test type attribute.
	 * @return String accepted values: M-Medical; L-Laboratory
	 */
	public String getTestType() {
		return testType;
	}

	/**
	 * Sets the taxable indicator attribute.
	 * @param taxable signifies whether the test conducted is taxable or not
	 */ 
	public void setTaxable(boolean taxable) {
		this.taxable = taxable;
	}
	
	/**
	 * Retrieves the taxable indicator attribute.
	 * @return boolean signifies whether the test conducted is taxable or not
	 */
	public boolean isTaxable() {
		return taxable;
	}

	/**
	 * Sets the validity attribute.
	 * @param validity number of days the test result is considered valid from the date it was conducted
	 */ 
	public void setValidity(int validity) {
		this.validity = validity;
	}

	/**
	 * Retrieves the validity attribute.
	 * @return int number of days the test result is considered valid from the date it was conducted
	 */
	public int getValidity() {
		return validity;
	}

	/**
	 * Sets the created date attribute.
	 * @param createDate date when the record was created
	 */ 
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	
	/**
	 * Retrieves the created date attribute.
	 * @return Date date when the record was created
	 */
	public Date getCreateDate() {
		return createDate;
	}

	/**
	 * Sets the created by attribute.
	 * @param createdBy user who created the record
	 */ 
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * Retrieves the created by attribute.
	 * @return String user who created the record
	 */
	public String getCreatedBy() {
		return createdBy;
	}
	
	/**
	 * Sets the updated date attribute.
	 * @param updateDate date when the record was last updated
	 */ 
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
	
	/**
	 * Retrieves the updated date attribute.
	 * @return Date date when the record was last updated
	 */
	public Date getUpdateDate() {
		return updateDate;
	}

	/**
	 * Sets the updated by attribute.
	 * @param updatedBy user who last updated the record
	 */ 
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	
	/**
	 * Retrieves the updated by attribute.
	 * @return String user who last updated the record
	 */
	public String getUpdatedBy() {
		return updatedBy;
	}
	
	/**
	 * Returns the followUpNumber.
	 * @return long
	 */
	public long getFollowUpNumber() {
		return followUpNumber;
	}

	/**
	 * Sets the followUpNumber.
	 * @param followUpNumber The followUpNumber to set
	 */
	public void setFollowUpNumber(long followUpNumber) {
		this.followUpNumber = followUpNumber;
	}

}
