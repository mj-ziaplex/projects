/*
 * Created on Feb 22, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.data;

/**
 * @author Nic Decapia
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class ARWithPendingMedFilterData
{
	public static final int BRANCH_REPORT_TYPE = 0;
	public static final int LABORATORY_REPORT_TYPE = 1;
	public static final int UNDERWRITER_REPORT_TYPE = 2;
	public static final int EXAMINER_REPORT_TYPE = 3;
	
	private String dateStart = new String();
	private String dateEnd = new String();
	private int reportType = BRANCH_REPORT_TYPE;

	private LaboratoryData laboratory = new LaboratoryData();
	private ExaminerData examiner = new ExaminerData();
	private SunLifeOfficeData branch = new SunLifeOfficeData();
	private UserProfileData underwriter = new UserProfileData();
	
	/**
	 * @return
	 */
	public String getDateEnd()
	{
		return dateEnd;
	}

	/**
	 * @return
	 */
	public String getDateStart()
	{
		return dateStart;
	}

	/**
	 * @return
	 */
	public int getReportType()
	{
		return reportType;
	}

	/**
	 * @param string
	 */
	public void setDateEnd(String string)
	{
		dateEnd = string;
	}

	/**
	 * @param string
	 */
	public void setDateStart(String string)
	{
		dateStart = string;
	}

	/**
	 * @param i
	 */
	public void setReportType(int i)
	{
		reportType = i;
	}

	/**
	 * @return
	 */
	public SunLifeOfficeData getBranch()
	{
		return branch;
	}

	/**
	 * @return
	 */
	public ExaminerData getExaminer()
	{
		return examiner;
	}

	/**
	 * @return
	 */
	public LaboratoryData getLaboratory()
	{
		return laboratory;
	}

	/**
	 * @return
	 */
	public UserProfileData getUnderwriter()
	{
		return underwriter;
	}

	/**
	 * @param data
	 */
	public void setBranch(SunLifeOfficeData data)
	{
		branch = data;
	}

	/**
	 * @param data
	 */
	public void setExaminer(ExaminerData data)
	{
		examiner = data;
	}

	/**
	 * @param data
	 */
	public void setLaboratory(LaboratoryData data)
	{
		laboratory = data;
	}

	/**
	 * @param data
	 */
	public void setUnderwriter(UserProfileData data)
	{
		underwriter = data;
	}

}
