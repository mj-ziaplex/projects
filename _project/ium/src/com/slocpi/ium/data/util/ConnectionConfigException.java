package com.slocpi.ium.data.util;

import java.io.PrintStream;
import java.io.PrintWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ConnectionConfigException extends Exception
{
	private static final Logger LOGGER = LoggerFactory.getLogger(ConnectionConfigException.class);
	private Throwable cause = null;

    /**
     * Creates a new Ex object.
     *
     * @param message DOCUMENT ME!
     */
    public ConnectionConfigException(String message) {
        super(message);
    }

    /**
     * Creates a new Ex object.
     *
     * @param message DOCUMENT ME!
     * @param cause DOCUMENT ME!
     */

    public ConnectionConfigException( String   message,
               Throwable cause )
    {
        super( message );
        this.cause = cause;
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public Throwable getCause() {
        return cause;
    }

    /**
     * DOCUMENT ME!
     */
    public void printStackTrace() {
    	
    	LOGGER.info("printStackTrace start 1");
        super.printStackTrace();
        if (cause != null) {
            System.err.println("Caused by: ");
            cause.printStackTrace();
        }
        LOGGER.info("printStackTrace end 1");
    }

    /**
     * DOCUMENT ME!
     *
     * @param ps DOCUMENT ME!
     */
    public void printStackTrace(PrintStream ps) {
    	LOGGER.info("printStackTrace start 2");
        super.printStackTrace(ps);
        if (cause != null) {
            ps.println("Caused by: ");
            cause.printStackTrace(ps);
        }
        LOGGER.info("printStackTrace end 2");
    }

    /**
     * DOCUMENT ME!
     *
     * @param pw DOCUMENT ME!
     */
    public void printStackTrace(PrintWriter pw) {
    	
    	LOGGER.info("printStackTrace start 3");
        super.printStackTrace(pw);
        if (cause != null) {
            pw.println("Caused by: ");
            cause.printStackTrace(pw);
        }
        LOGGER.info("printStackTrace end 3");
    }
}