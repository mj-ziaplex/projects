package com.slocpi.ium.data.util;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.slocpi.ium.util.CodeHelper;

public class ServiceLocator {
	private static final Logger LOGGER = LoggerFactory.getLogger(ServiceLocator.class);

	private InitialContext ic;
	private Map cache;
	
	private static ServiceLocator sl;

    static {
    	try {
    		sl = new ServiceLocator();
    	}catch(Exception e) {
    		LOGGER.error(e.getMessage());
    		e.printStackTrace(System.err);
    	}
    }

    private ServiceLocator() throws Exception  {
        try {
          ic = new InitialContext();
          cache = Collections.synchronizedMap(new HashMap());
        } catch (NamingException ne) {
        	LOGGER.error(ne.getMessage());
              throw new Exception(ne.getMessage());
        }
      }

      static public ServiceLocator getInstance() {
        return sl;
      }
      
      
      /**
       * This method obtains the datasource itself for a caller
       * @return the DataSource corresponding to the name parameter
       */
      public DataSource getDataSource(String dataSourceName) throws Exception {
    	  
    	 LOGGER.info("getDataSource start");
        DataSource dataSource = null;

        try {
          if (cache.containsKey(dataSourceName)) {
             dataSource = (DataSource) cache.get(dataSourceName);
          } else {
              dataSource = (DataSource)ic.lookup(dataSourceName);
              cache.put(dataSourceName, dataSource );
          }
          LOGGER.info("getDataSource() done");
        } catch (NamingException ne) {
        	LOGGER.error(CodeHelper.getStackTrace(ne));
            throw new Exception(ne.getMessage());
        } 
        LOGGER.info("getDataSource end");
        return dataSource;
      }
      
	/**
	 * @param args
	 */
}
