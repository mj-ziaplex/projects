package com.slocpi.ium.data.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ResourceBundle;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.slocpi.ium.IUMException;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.IUMConstants;
/**
 * DOCUMENT ME! Copyright (c) 2002 CNF, Inc.
 *
 * @version $Revision: 1.1 $
 */
public class DataSourceProxy
{	
	
	private static final Logger LOGGER = LoggerFactory.getLogger(DataSourceProxy.class);
    
	public Connection getConnection() {
		
		Connection connection = null;
		try {
     		connection = getConnectionConfig();
		} catch (Exception ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			return null;
		}
		
        return connection;            
	}
	
	public void closeConnection(Connection conn) throws IUMException{
		
		
		if (null != conn){
			try {
				conn.close();
			} catch (SQLException sqlE) {
				LOGGER.error(CodeHelper.getStackTrace(sqlE));
				throw new IUMException(sqlE);
			}
		}
		
	}	
	
	/*	 
 	* @author ppe
 	* METHOD: getConnectionConfig()
 	* Get the connection by getting the details in the config file. 	
	*/
	private Connection getConnectionConfig() throws ConnectionConfigException, ClassNotFoundException, SQLException {
		
		LOGGER.info("getConnectionConfig start");
		Connection connection = null;														
		
		ResourceBundle rb = ResourceBundle.getBundle(IUMConstants.IUM_CONFIG);
		String db_location = rb.getString(IUMConstants.IUM_DBLOCATION);
       	
       	try {
       		InitialContext ic = new InitialContext(); 
			DataSource ds = (DataSource) ic.lookup(db_location);
			connection = ds.getConnection();
			
       	}catch (NamingException e) {
       		LOGGER.warn("DATASOURCE CONNECTION ERROR. START MANUAL CONNECTION ON DATABASE.");
			LOGGER.error(CodeHelper.getStackTrace(e));
			String conn_driver = rb.getString("connection-driver");
			String url = rb.getString("url");
			String username = rb.getString("username");
			String password = rb.getString("password");
			Class.forName(conn_driver);
			connection = DriverManager.getConnection(url, username, password);				
       	}catch (Exception e) {
       		LOGGER.error(CodeHelper.getStackTrace(e));
		}
       	LOGGER.info("getConnectionConfig end");
		return connection;		
	} 
		

}
