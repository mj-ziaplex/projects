// Andre Ceasar Dacanay - AuditLogs
package com.slocpi.ium.data;

import java.util.Date;

/**
 * @author Andre Ceasar Dacanay
 * 
 */
public class AuditLogsData {
	
	private long auditNum;
	private Date auditDate;
	private String remarks;
	private String userId;
	private String auditType;
	
	public Date getAuditDate() {
		return auditDate;
	}
	public void setAuditDate(Date auditDate) {
		this.auditDate = auditDate;
	}
	public long getAuditNum() {
		return auditNum;
	}
	public void setAuditNum(long auditNum) {
		this.auditNum = auditNum;
	}
	public String getAuditType() {
		return auditType;
	}
	public void setAuditType(String auditType) {
		this.auditType = auditType;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	
}
