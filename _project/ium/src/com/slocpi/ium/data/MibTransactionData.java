package com.slocpi.ium.data;

public class MibTransactionData {

	private String userCode;
	private String companyCode;
	private String transactionCode;
	private int recordAffected;
	private int noOfHits;

	public MibTransactionData() {
		
	}
	
	public MibTransactionData(String userCode, String companyCode, String transactionCode, int recordAffected, int noOfHits) {
		this.userCode = userCode;
		this.companyCode = companyCode;
		this.transactionCode = transactionCode;
		this.recordAffected = recordAffected;
		this.noOfHits = noOfHits;
	}
	
	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public String getCompanyCode() {
		return companyCode;
	}

	public void setCompanyCode(String companyCode) {
		this.companyCode = companyCode;
	}

	public String getTransactionCode() {
		return transactionCode;
	}

	public void setTransactionCode(String transactionCode) {
		this.transactionCode = transactionCode;
	}

	public int getRecordAffected() {
		return recordAffected;
	}

	public void setRecordAffected(int recordAffected) {
		this.recordAffected = recordAffected;
	}

	public int getNoOfHits() {
		return noOfHits;
	}

	public void setNoOfHits(int noOfHits) {
		this.noOfHits = noOfHits;
	}
	
}
