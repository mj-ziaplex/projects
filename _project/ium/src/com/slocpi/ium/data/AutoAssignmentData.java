package com.slocpi.ium.data;

import java.util.Date;

/**
 * Container for Auto Assignment reference information.
 * @author Engel 
 */
public class AutoAssignmentData {

	private long autoAssignmentId;
	private long criteriaId;
	private String userCode;
	private String fieldValue;
	private String createdBy;
	private Date createDate;
	private String updatedBy;
	private Date updateDate;
	
	
	/**
	 * Sets the auto-assignment id attribute.
	 * @param autoAssignmentId auto-assignment id
	 */ 
	public void setAutoAssignmentId(long autoAssignmentId) {
		this.autoAssignmentId = autoAssignmentId;
	}
		
	/**
	 * Retrieves the auto-assignment id attribute.
	 * @return String auto-assignment id
	 */
	public long getAutoAssignmentId() {
		return autoAssignmentId;
	}

	/**
	 * Sets the criteria id attribute.
	 * @param criteriaId criteria code
	 */ 
	public void setCriteriaId(long criteriaId) {
		this.criteriaId = criteriaId;
	}
		
	/**
	 * Retrieves the criteria id attribute.
	 * @return String criteria code
	 */
	public long getCriteriaId() {
		return criteriaId;
	}

	/**
	 * Sets the field value attribute.
	 * @param fieldValue value that will be the basis in assigning the underwriter
	 */ 
	public void setFieldValue(String fieldValue) {
		this.fieldValue = fieldValue;
	}

	/**
	 * Retrieves the field value attribute.
	 * @return String value that will be the basis in assigning the underwriter
	 */
	public String getFieldValue() {
		return fieldValue;
	}

	/**
	 * Sets the user code attribute.
	 * @param userCode underwriter to be mapped to the criterion
	 */ 
	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	/**
	 * Retrieves the user code attribute.
	 * @return String underwriter to be mapped to the criterion
	 */
	public String getUserCode() {
		return userCode;
	}

	/**
	 * Sets the created date attribute.
	 * @param createDate date when the record was created
	 */ 
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	
	/**
	 * Retrieves the created date attribute.
	 * @return Date date when the record was created
	 */
	public Date getCreateDate() {
		return createDate;
	}

	/**
	 * Sets the created by attribute.
	 * @param createdBy user who created the record
	 */ 
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * Retrieves the created by attribute.
	 * @return String user who created the record
	 */
	public String getCreatedBy() {
		return createdBy;
	}

	/**
	 * Sets the updated date attribute.
	 * @param updateDate date when the record was last updated
	 */ 
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
	
	/**
	 * Retrieves the updated date attribute.
	 * @return Date date when the record was last updated
	 */
	public Date getUpdateDate() {
		return updateDate;
	}

	/**
	 * Sets the updated by attribute.
	 * @param updatedBy user who last updated the record
	 */ 
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	
	/**
	 * Retrieves the updated by attribute.
	 * @return String user who last updated the record
	 */
	public String getUpdatedBy() {
		return updatedBy;
	}
}
