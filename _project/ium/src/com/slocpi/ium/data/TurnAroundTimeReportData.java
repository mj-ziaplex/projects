/*
 * Created on Mar 24, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.data;

/**
 * @author Nic Decapia
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class TurnAroundTimeReportData
{  
	
	private String lineOfBusiness;
	private String referenceNumber;
	private String dateStart;
	private String dateEnd;
	private String elapsedTime;
	private String processedCount;
	private String averageTAT;


	
	/**
	 * @return
	 */
	public String getAverageTAT()
	{
		return averageTAT;
	}

	/**
	 * @return
	 */
	public String getDateEnd()
	{
		return dateEnd;
	}

	/**
	 * @return
	 */
	public String getDateStart()
	{
		return dateStart;
	}

	/**
	 * @return
	 */
	public String getElapsedTime()
	{
		return elapsedTime;
	}

	/**
	 * @return
	 */
	public String getLineOfBusiness()
	{
		return lineOfBusiness;
	}

	/**
	 * @return
	 */
	public String getProcessedCount()
	{
		return processedCount;
	}

	/**
	 * @return
	 */
	public String getReferenceNumber()
	{
		return referenceNumber;
	}

	/**
	 * @param string
	 */
	public void setAverageTAT(String string)
	{
		averageTAT = string;
	}

	/**
	 * @param string
	 */
	public void setDateEnd(String string)
	{
		dateEnd = string;
	}

	/**
	 * @param string
	 */
	public void setDateStart(String string)
	{
		dateStart = string;
	}

	/**
	 * @param string
	 */
	public void setElapsedTime(String string)
	{
		elapsedTime = string;
	}

	/**
	 * @param string
	 */
	public void setLineOfBusiness(String string)
	{
		lineOfBusiness = string;
	}

	/**
	 * @param string
	 */
	public void setProcessedCount(String string)
	{
		processedCount = string;
	}

	/**
	 * @param string
	 */
	public void setReferenceNumber(String string)
	{
		referenceNumber = string;
	}

}