/*
 * Created on Dec 23, 2003
 * package name = com.slocpi.ium.data
 * file name    = NotificationTemplateData.java
 */
package com.slocpi.ium.data;

import java.util.ArrayList;
import java.util.Date;

/**
 * Container for Notification Template reference information.
 * @author Engel
 * 
 */
public class NotificationTemplateData {

	private long notificationId;
	private String notificationDesc;
	private String emailNotificationContent;
	private String sender;
	private boolean notifyMobile;
	private String mobileNotificationContent;
	private String createdBy;
	private Date creationDate;
	private String updatedBy;
	private Date updateDate;
	private ArrayList emailAttachments;
	private String subject;
	
	/**
	 * TODO method description getCreatedBy
	 * @return
	 */
	public String getCreatedBy() {
		return createdBy;
	}

	/**
	 * TODO method description getCreationDate
	 * @return
	 */
	public Date getCreationDate() {
		return creationDate;
	}

	/**
	 * TODO method description getEmailAttachments
	 * @return
	 */
	public ArrayList getEmailAttachments() {
		return emailAttachments;
	}

	/**
	 * TODO method description getEmailNotificationContent
	 * @return
	 */
	public String getEmailNotificationContent() {
		return emailNotificationContent;
	}

	/**
	 * TODO method description getMobileNotificationContent
	 * @return
	 */
	public String getMobileNotificationContent() {
		return mobileNotificationContent;
	}

	/**
	 * TODO method description getNotificationDesc
	 * @return
	 */
	public String getNotificationDesc() {
		return notificationDesc;
	}

	/**
	 * TODO method description getNotificationId
	 * @return
	 */
	public long getNotificationId() {
		return notificationId;
	}

	/**
	 * TODO method description isNotifyMobile
	 * @return
	 */
	public boolean isNotifyMobile() {
		return notifyMobile;
	}

	/**
	 * TODO method description getSender
	 * @return
	 */
	public String getSender() {
		return sender;
	}

	/**
	 * TODO method description getUpdateDate
	 * @return
	 */
	public Date getUpdateDate() {
		return updateDate;
	}

	/**
	 * TODO method description getUpdatedBy
	 * @return
	 */
	public String getUpdatedBy() {
		return updatedBy;
	}

	/**
	 * TODO method description setCreatedBy
	 * @param string
	 */
	public void setCreatedBy(String string) {
		createdBy = string;
	}

	/**
	 * TODO method description setCreationDate
	 * @param date
	 */
	public void setCreationDate(Date date) {
		creationDate = date;
	}

	/**
	 * TODO method description setEmailAttachments
	 * @param list
	 */
	public void setEmailAttachments(ArrayList list) {
		emailAttachments = list;
	}

	/**
	 * TODO method description setEmailNotificationContent
	 * @param string
	 */
	public void setEmailNotificationContent(String string) {
		emailNotificationContent = string;
	}

	/**
	 * TODO method description setMobileNotificationContent
	 * @param string
	 */
	public void setMobileNotificationContent(String string) {
		mobileNotificationContent = string;
	}

	/**
	 * TODO method description setNotificationDesc
	 * @param string
	 */
	public void setNotificationDesc(String string) {
		notificationDesc = string;
	}

	/**
	 * TODO method description setNotificationId
	 * @param l
	 */
	public void setNotificationId(long l) {
		notificationId = l;
	}

	/**
	 * TODO method description setNotifyMobile
	 * @param b
	 */
	public void setNotifyMobile(boolean b) {
		notifyMobile = b;
	}

	/**
	 * TODO method description setSender
	 * @param string
	 */
	public void setSender(String string) {
		sender = string;
	}

	/**
	 * TODO method description setUpdateDate
	 * @param date
	 */
	public void setUpdateDate(Date date) {
		updateDate = date;
	}

	/**
	 * TODO method description setUpdatedBy
	 * @param string
	 */
	public void setUpdatedBy(String string) {
		updatedBy = string;
	}

	/**
	 * @return
	 */
	public String getSubject() {
		return subject;
	}

	/**
	 * @param string
	 */
	public void setSubject(String subject) {
		this.subject = subject;
	}

}
