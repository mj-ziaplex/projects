/**
 * TurnAroundTimeParam.java				Copyright (c) 2004 Pointwest Technologies Corp.
 * TODO DOCUMENT ME!
 * 
 * @author aditalo	 @date Mar 21, 2004
 */
package com.slocpi.ium.data;

/**
 * TODO DOCUMENT ME!
 * 
 * @author aditalo		Copyright (c) 2004 Pointwest Technologies Corp.
 * @date Mar 21, 2004
 */
public class TurnAroundTimeParam {

    public static final int TAT_IN_DAYS = 0;
	public static final int TAT_IN_HOURS = 1;

	private String startDate;
	private String endDate;
	private String startStatus;
	private String endStatus;
	private String unit;
	private LOBData lob;

	/**
	 * @return
	 */
	public String getEndDate() {
		return endDate;
	}

	/**
	 * @return
	 */
	public String getEndStatus() {
		return endStatus;
	}

	/**
	 * @return
	 */
	public String getStartDate() {
		return startDate;
	}

	/**
	 * @return
	 */
	public String getStartStatus() {
		return startStatus;
	}

	/**
	 * @return
	 */
	public String getUnit() {
		return unit;
	}

	/**
	 * @param string
	 */
	public void setEndDate(String string) {
		endDate = string;
	}

	/**
	 * @param string
	 */
	public void setEndStatus(String string) {
		endStatus = string;
	}

	/**
	 * @param string
	 */
	public void setStartDate(String string) {
		startDate = string;
	}

	/**
	 * @param string
	 */
	public void setStartStatus(String string) {
		startStatus = string;
	}

	/**
	 * @param string
	 */
	public void setUnit(String string) {
		unit = string;
	}

	/**
	 * @return
	 */
	public LOBData getLob()
	{
		return lob;
	}

	/**
	 * @param data
	 */
	public void setLob(LOBData data)
	{
		lob = data;
	}

}
