package com.slocpi.ium.data;

import java.util.Date;

/**
 * Container for Sun Life�s department reference information.
 * @author Engel 
 */
public class SunLifeDeptData {

	private String deptId;
	private String deptDesc;
	private String createdBy;
	private Date createDate;
	private String updatedBy;
	private Date updateDate;

	/**
	 * Sets the created date attribute.
	 * @param createDate date when the record was created
	 */ 
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	
	/**
	 * Retrieves the created date attribute.
	 * @return Date date when the record was created
	 */
	public Date getCreateDate() {
		return createDate;
	}

	/**
	 * Sets the created by attribute.
	 * @param createdBy user who created the record
	 */ 
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * Retrieves the created by attribute.
	 * @return String user who created the record
	 */
	public String getCreatedBy() {
		return createdBy;
	}

	/**
	 * Sets the department id attribute.
	 * @param deptId department id
	 */ 
	public void setDeptId(String deptId) {
		this.deptId = deptId;
	}

	/**
	 * Retrieves the department id attribute.
	 * @return String department id
	 */
	public String getDeptId() {
		return deptId;
	}

	/**
	 * Sets the department description attribute.
	 * @param deptDesc department description
	 */ 
	public void setDeptDesc(String deptDesc) {
		this.deptDesc = deptDesc;
	}

	/**
	 * Retrieves the department description attribute.
	 * @return String department description
	 */
	public String getDeptDesc() {
		return deptDesc;
	}
	
	/**
	 * Sets the updated date attribute.
	 * @param updateDate date when the record was last updated
	 */ 
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
	
	/**
	 * Retrieves the updated date attribute.
	 * @return Date date when the record was last updated
	 */
	public Date getUpdateDate() {
		return updateDate;
	}

	/**
	 * Sets the updated by attribute.
	 * @param updatedBy user who last updated the record
	 */ 
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	
	/**
	 * Retrieves the updated by attribute.
	 * @return String user who last updated the record
	 */
	public String getUpdatedBy() {
		return updatedBy;
	}

}
