/*
 * Created on Jan 5, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.data;

import java.util.Date;

/**
 * @author daguila
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class IUMMessageData {

	private String 	confirmationCode;
	private String	messageLine1;
	private String	messageLine2;
	private String	messageLine3;
	private String 	policyClientCode;
	private String	policyClientId;
	private String	requirementId;
	private int	sequenceNumber;
	private Date	creationDate;
	private Date	statusDate;
	private String xmlRecord;
	
	/**
	 * @return
	 */
	public String getConfirmationCode() {
		return confirmationCode;
	}

	/**
	 * @return
	 */
	public String getMessageLine1() {
		return messageLine1;
	}

	/**
	 * @return
	 */
	public String getMessageLine2() {
		return messageLine2;
	}

	/**
	 * @return
	 */
	public String getMessageLine3() {
		return messageLine3;
	}

	/**
	 * @param string
	 */
	public void setConfirmationCode(String string) {
		confirmationCode = string;
	}

	/**
	 * @param string
	 */
	public void setMessageLine1(String string) {
		messageLine1 = string;
	}

	/**
	 * @param string
	 */
	public void setMessageLine2(String string) {
		messageLine2 = string;
	}

	/**
	 * @param string
	 */
	public void setMessageLine3(String string) {
		messageLine3 = string;
	}

	/**
	 * @return
	 */
	public Date getCreationDate() {
		return creationDate;
	}

	/**
	 * @return
	 */
	public String getPolicyClientCode() {
		return policyClientCode;
	}

	/**
	 * @return
	 */
	public String getPolicyClientId() {
		return policyClientId;
	}

	/**
	 * @return
	 */
	public String getRequirementId() {
		return requirementId;
	}

	/**
	 * @return
	 */
	public int getSequenceNumber() {
		return sequenceNumber;
	}

	/**
	 * @return
	 */
	public Date getStatusDate() {
		return statusDate;
	}

	/**
	 * @param date
	 */
	public void setCreationDate(Date date) {
		creationDate = date;
	}

	/**
	 * @param string
	 */
	public void setPolicyClientCode(String string) {
		policyClientCode = string;
	}

	/**
	 * @param string
	 */
	public void setPolicyClientId(String string) {
		policyClientId = string;
	}

	/**
	 * @param string
	 */
	public void setRequirementId(String string) {
		requirementId = string;
	}

	/**
	 * @param string
	 */
	public void setSequenceNumber(int seq) {
		sequenceNumber = seq;
	}

	/**
	 * @param date
	 */
	public void setStatusDate(Date date) {
		statusDate = date;
	}

	/**
	 * @return
	 */
	public String getXmlRecord() {
		return xmlRecord;
	}

	/**
	 * @param string
	 */
	public void setXmlRecord(String string) {
		xmlRecord = string;
	}

}
