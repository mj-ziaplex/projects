/*
 * Created on Jul 8, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.data;

/**
 * @author ypojol
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class UserPasswords {
	private String userid;
	private String password1;
	private String password2;
	private String password3;
	private String password4;
	private String password5;
	/**
	 * 
	 */
	public UserPasswords() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @return
	 */
	public String getPassword1() {
		return password1;
	}

	/**
	 * @return
	 */
	public String getPassword2() {
		return password2;
	}

	/**
	 * @return
	 */
	public String getPassword3() {
		return password3;
	}

	/**
	 * @return
	 */
	public String getPassword4() {
		return password4;
	}

	/**
	 * @return
	 */
	public String getPassword5() {
		return password5;
	}

	/**
	 * @return
	 */
	public String getUserid() {
		return userid;
	}

	/**
	 * @param string
	 */
	public void setPassword1(String string) {
		password1 = string;
	}

	/**
	 * @param string
	 */
	public void setPassword2(String string) {
		password2 = string;
	}

	/**
	 * @param string
	 */
	public void setPassword3(String string) {
		password3 = string;
	}

	/**
	 * @param string
	 */
	public void setPassword4(String string) {
		password4 = string;
	}

	/**
	 * @param string
	 */
	public void setPassword5(String string) {
		password5 = string;
	}

	/**
	 * @param string
	 */
	public void setUserid(String string) {
		userid = string;
	}

}
