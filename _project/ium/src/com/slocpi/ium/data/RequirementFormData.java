package com.slocpi.ium.data;

import java.util.Date;

/**
 * container for Requirement Form reference information.
 * @author Engel 
 */
public class RequirementFormData {

	private long formId;
	private String formName;
	private String templateFormat;
	private String templateName;
	private boolean status;
	private String createdBy;
	private Date createDate;
	private String updatedBy;
	private Date updateDate;
	private String wmsFormId;


	/**
	 * Sets the form id attribute.
	 * @param formId form id
	 */  
	public void setFormId(long formId) {
		this.formId = formId;
	}
	
	/**
	 * Retrieves the form id attribute.
	 * @return String form id
	 */ 
	public long getFormId() {
		return formId;
	}

	/**
	 * Sets the form name attribute.
	 * @param formName form name
	 */  
	public void setFormName(String formName) {
		this.formName = formName;
	}

	/**
	 * Retrieves the form name attribute.
	 * @return String form name
	 */ 
	public String getFormName() {
		return formName;
	}

	/**
	 * Sets the template format attribute.
	 * @param templateFormat file format used for the form
	 */  
	public void setTemplateFormat(String templateFormat) {
		this.templateFormat = templateFormat;
	}

	/**
	 * Retrieves the template format attribute.
	 * @return String file format used for the form
	 */
	public String getTemplateFormat() {
		return templateFormat;
	}

	/**
	 * Sets the template name attribute.
	 * @param templateName name of the file
	 */ 
	public void setTemplateName(String templateName) {
		this.templateName = templateName;
	}

	/**
	 * Retrieves the template name attribute.
	 * @return String name of the file
	 */
	public String getTemplateName() {
		return templateName;
	}

	/**
	 * Sets the status attribute.
	 * @param status defines if the form is still active and in-use
	 */  
	public void setStatus(boolean status) {
		this.status = status;
	}

	/**
	 * Retrieves the status attribute.
	 * @return String defines if the form is still active and in-use
	 */
	public boolean getStatus() {
		return status;
	}

	/**
	 * Sets the created date attribute.
	 * @param createDate date when the record was created
	 */ 
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	
	/**
	 * Retrieves the created date attribute.
	 * @return Date date when the record was created
	 */
	public Date getCreateDate() {
		return createDate;
	}

	/**
	 * Sets the created by attribute.
	 * @param createdBy user who created the record
	 */ 
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * Retrieves the created by attribute.
	 * @return String user who created the record
	 */
	public String getCreatedBy() {
		return createdBy;
	}

	/**
	 * Sets the updated date attribute.
	 * @param updateDate date when the record was last updated
	 */ 
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
	
	/**
	 * Retrieves the updated date attribute.
	 * @return Date date when the record was last updated
	 */
	public Date getUpdateDate() {
		return updateDate;
	}

	/**
	 * Sets the updated by attribute.
	 * @param updatedBy user who last updated the record
	 */ 
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	
	/**
	 * Retrieves the updated by attribute.
	 * @return String user who last updated the record
	 */
	public String getUpdatedBy() {
		return updatedBy;
	}

	public String getWmsFormId() {
		return wmsFormId;
	}

	public void setWmsFormId(String wmsFormId) {
		this.wmsFormId = wmsFormId;
	}
	
}
