/*
 * Created on Dec 23, 2003
 * package name = com.slocpi.ium.data
 * file name    = LOBRequirementData.java
 */
package com.slocpi.ium.data;

/**
 * Container for LOB Requirement reference information.
 * @author Engel
 * 
 */
public class LOBRequirementData {

	private long lineOfBusinessId;
	private String requirementCode;
	private String remarks;
	
	/**
	 * TODO method description getLineOfBusinessId
	 * @return
	 */
	public long getLineOfBusinessId() {
		return lineOfBusinessId;
	}

	/**
	 * TODO method description getRemarks
	 * @return
	 */
	public String getRemarks() {
		return remarks;
	}

	/**
	 * TODO method description getRequirementCode
	 * @return
	 */
	public String getRequirementCode() {
		return requirementCode;
	}

	/**
	 * TODO method description setLineOfBusinessId
	 * @param l
	 */
	public void setLineOfBusinessId(long l) {
		lineOfBusinessId = l;
	}

	/**
	 * TODO method description setRemarks
	 * @param string
	 */
	public void setRemarks(String string) {
		remarks = string;
	}

	/**
	 * TODO method description setRequirementCode
	 * @param string
	 */
	public void setRequirementCode(String string) {
		requirementCode = string;
	}

}
