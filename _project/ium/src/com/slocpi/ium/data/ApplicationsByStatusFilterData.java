/*
 * Created on Feb 19, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.data;

/**
 * @author Nic Decapia
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class ApplicationsByStatusFilterData
{
	public final static int FILTER_BY_BRANCH = 0;
	public final static int FILTER_BY_LOB = 1;
	public final static int FILTER_BY_UNDERWRITER = 2;
	
	private String startDate = new String();
	private String endDate = new String();
	private int filterType = FILTER_BY_BRANCH;
	
	private SunLifeOfficeData branch = new SunLifeOfficeData();
	private UserProfileData underwriter = new UserProfileData();
	private LOBData lob = new LOBData();
	
	/**
	 * @return
	 */
	public String getEndDate()
	{
		return endDate;
	}

	/**
	 * @return
	 */
	public int getFilterType()
	{
		return filterType;
	}

	/**
	 * @return
	 */
	public String getStartDate()
	{
		return startDate;
	}

	/**
	 * @param string
	 */
	public void setEndDate(String string)
	{
		endDate = string;
	}

	/**
	 * @param i
	 */
	public void setFilterType(int i)
	{
		filterType = i;
	}

	/**
	 * @param string
	 */
	public void setStartDate(String string)
	{
		startDate = string;
	}

	/**
	 * @return
	 */
	public SunLifeOfficeData getBranch()
	{
		return branch;
	}

	/**
	 * @return
	 */
	public UserProfileData getUnderwriter()
	{
		return underwriter;
	}

	/**
	 * @param data
	 */
	public void setBranch(SunLifeOfficeData data)
	{
		branch = data;
	}

	/**
	 * @param data
	 */
	public void setUnderwriter(UserProfileData data)
	{
		underwriter = data;
	}

	/**
	 * @return
	 */
	public LOBData getLob()
	{
		return lob;
	}

	/**
	 * @param data
	 */
	public void setLob(LOBData data)
	{
		lob = data;
	}

}
