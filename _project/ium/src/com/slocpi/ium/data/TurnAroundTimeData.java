/**
 * TurnAroundTimeData.java				Copyright (c) 2004 Pointwest Technologies Corp.
 * TODO DOCUMENT ME!
 * 
 * @author aditalo	 @date Mar 21, 2004
 */
package com.slocpi.ium.data;

import java.math.BigDecimal;

/**
 * TODO DOCUMENT ME!
 * 
 * @author aditalo		Copyright (c) 2004 Pointwest Technologies Corp.
 * @date Mar 21, 2004
 */
public class TurnAroundTimeData {
	
	private String referenceNum;
	private String startDate;
	private String endDate;
	private BigDecimal elapsedTime;
	
	/**
	 * @return
	 */
	public BigDecimal getElapsedTime() {
		return elapsedTime;
	}

	/**
	 * @return
	 */
	public String getEndDate() {
		return endDate;
	}

	/**
	 * @return
	 */
	public String getReferenceNum() {
		return referenceNum;
	}

	/**
	 * @return
	 */
	public String getStartDate() {
		return startDate;
	}

	/**
	 * @param decimal
	 */
	public void setElapsedTime(BigDecimal decimal) {
		elapsedTime = decimal;
	}

	/**
	 * @param date
	 */
	public void setEndDate(String date) {
		endDate = date;
	}

	/**
	 * @param string
	 */
	public void setReferenceNum(String string) {
		referenceNum = string;
	}

	/**
	 * @param date
	 */
	public void setStartDate(String date) {
		startDate = date;
	}

}
