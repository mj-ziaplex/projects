/*
 * Created on Dec 23, 2003
 * package name = com.slocpi.ium.data.dao
 * file name    = UserProcessPrefData.java
 */
package com.slocpi.ium.data;

/**
 * Container for User Process Preference reference information.
 * @author Engel
 * 
 */
public class UserProcessPrefData {

	private String userCode;
	private String processId;
	private long notificationInd;
	
	
	/**
	 * TODO method description getNotificationInd
	 * @return
	 */
	public long getNotificationInd() {
		return notificationInd;
	}

	/**
	 * TODO method description getProcessId
	 * @return
	 */
	public String getProcessId() {
		return processId;
	}

	/**
	 * TODO method description getUserCode
	 * @return
	 */
	public String getUserCode() {
		return userCode;
	}

	/**
	 * TODO method description setNotificationInd
	 * @param l
	 */
	public void setNotificationInd(long l) {
		notificationInd = l;
	}

	/**
	 * TODO method description setProcessId
	 * @param string
	 */
	public void setProcessId(String string) {
		processId = string;
	}

	/**
	 * TODO method description setUserCode
	 * @param string
	 */
	public void setUserCode(String string) {
		userCode = string;
	}

}
