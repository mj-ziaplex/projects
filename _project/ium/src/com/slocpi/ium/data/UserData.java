/*
 * Created on Jan 12, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.data;

import java.util.ArrayList;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author daguila
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class UserData {

	Logger LOGGER = LoggerFactory.getLogger(UserData.class.getName());
	private UserProcessPrefData processPreference;
	private UserProfileData profile;
	private ArrayList access;
	private ArrayList roles;
	
	public String toString(){
		return ToStringBuilder.reflectionToString(this, ToStringStyle.DEFAULT_STYLE);
	}
	
	
	/**
	 * Retrieves the access attribute. 
	 * @return
	 */
	public ArrayList getAccess() {
		return access;
	}

	/**
	 * Retrieves the process preference attribute. 
	 * @return
	 */
	public UserProcessPrefData getProcessPreference() {
		return processPreference;
	}

	/**
	 * Retrieves the profile attribute. 
	 * @return
	 */
	public UserProfileData getProfile() {
		return profile;
	}

	/**
	 * Retrieves the roles attribute. 
	 * @return
	 */
	public ArrayList getRoles() {
		return roles;
	}

	/**
	 * Sets the access attribute. 
	 * @param data
	 */
	public void setAccess(ArrayList list) {
		access = list;
	}

	/**
	 * Sets the process preference attribute. 
	 * @param data
	 */
	public void setProcessPreference(UserProcessPrefData data) {
		processPreference = data;
	}

	/**
	 * Sets the profile attribute. 
	 * @param data
	 */
	public void setProfile(UserProfileData data) {
		profile = data;
	}

	/**
	 * Sets the roles attribute. 
	 * @param list
	 */
	public void setRoles(ArrayList list) {
		roles = list;
	}
	
	/**
	 * This method checks if the parameter exists in the roles data of the user.  
	 * @param roleCode
	 * @return
	 */
	public boolean isRoleIncluded(String roleCode) {
		
		LOGGER.info("isRoleIncluded start");
		boolean res = false;
		if ((roles != null) && (roles.size() > 0) && (roleCode != null)) {
			for (int i=0; i<roles.size(); i++) {
				RolesData rd = (RolesData)roles.get(i);
				if ((rd.getRolesId() != null) && (rd.getRolesId().equals(roleCode))) {
					res = true;
					break;
				}
			}
		}
		LOGGER.info("isRoleIncluded end");
		return res;
	}

	

}
