/*
 * Created on Feb 17, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.data;

import com.slocpi.ium.ui.util.Page;

/**
 * @author Nic Decapia
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class ApplicationsApprovedFilterData
{
	private String startDate = new String();
	private String endDate = new String();
	private SunLifeDeptData approvingParty = new SunLifeDeptData();
	private LOBData requestingParty = new LOBData();
	
	private int pageNo = 1;
	private Page page;	
	
	/**
	 * @return
	 */
	public SunLifeDeptData getApprovingParty()
	{
		return approvingParty;
	}

	/**
	 * @return
	 */
	public String getEndDate()
	{
		return endDate;
	}

	/**
	 * @return
	 */
	public LOBData getRequestingParty()
	{
		return requestingParty;
	}

	/**
	 * @return
	 */
	public String getStartDate()
	{
		return startDate;
	}

	/**
	 * @param data
	 */
	public void setApprovingParty(SunLifeDeptData data)
	{
		approvingParty = data;
	}

	/**
	 * @param date
	 */
	public void setEndDate(String date)
	{
		endDate = date;
	}

	/**
	 * @param data
	 */
	public void setRequestingParty(LOBData data)
	{
		requestingParty = data;
	}

	/**
	 * @param date
	 */
	public void setStartDate(String date)
	{
		startDate = date;
	}

	/**
	 * @return
	 */
	public Page getPage()
	{
		return page;
	}

	/**
	 * @return
	 */
	public int getPageNo()
	{
		return pageNo;
	}

	/**
	 * @param page
	 */
	public void setPage(Page page)
	{
		this.page = page;
	}

	/**
	 * @param i
	 */
	public void setPageNo(int i)
	{
		pageNo = i;
	}

}
