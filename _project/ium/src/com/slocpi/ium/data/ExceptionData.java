/*
 * Created on Mar 5, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.data;

import java.util.Date;

/**
 * @author Cris
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class ExceptionData {
	private long exceptionId;
	private String referenceNumber;
	private String recordType;
	private String xmlRecord;
	private ExceptionDetailsData details;
	private String exceptionInterface;
	private Date exceptionTimeStamp;
	private String exceptionDate; 

	/**
	 * @return
	 */
	public ExceptionDetailsData getDetails() {
		return details;
	}

	/**
	 * @return
	 */
	public long getExceptionId() {
		return exceptionId;
	}

	/**
	 * @return
	 */
	public String getExceptionInterface() {
		return exceptionInterface;
	}

	/**
	 * @return
	 */
	public Date getExceptionTimeStamp() {
		return exceptionTimeStamp;
	}

	/**
	 * @return
	 */
	public String getRecordType() {
		return recordType;
	}

	/**
	 * @return
	 */
	public String getReferenceNumber() {
		return referenceNumber;
	}

	/**
	 * @return
	 */
	public String getXmlRecord() {
		return xmlRecord;
	}

	/**
	 * @param data
	 */
	public void setDetails(ExceptionDetailsData data) {
		details = data;
	}

	/**
	 * @param l
	 */
	public void setExceptionId(long l) {
		exceptionId = l;
	}

	/**
	 * @param string
	 */
	public void setExceptionInterface(String string) {
		exceptionInterface = string;
	}

	/**
	 * @param date
	 */
	public void setExceptionTimeStamp(Date date) {
		exceptionTimeStamp = date;
	}

	/**
	 * @param string
	 */
	public void setRecordType(String string) {
		recordType = string;
	}

	/**
	 * @param string
	 */
	public void setReferenceNumber(String string) {
		referenceNumber = string;
	}

	/**
	 * @param string
	 */
	public void setXmlRecord(String string) {
		xmlRecord = string;
	}

	/**
	 * @return
	 */
	public String getExceptionDate() {
		return exceptionDate;
	}

	/**
	 * @param string
	 */
	public void setExceptionDate(String string) {
		exceptionDate = string;
	}

}
