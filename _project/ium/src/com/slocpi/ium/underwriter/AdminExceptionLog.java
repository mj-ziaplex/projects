/*
 * Created on Mar 5, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.underwriter;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.slocpi.ium.IUMException;
import com.slocpi.ium.data.ExceptionFilterData;
import com.slocpi.ium.data.dao.ExceptionDAO;
import com.slocpi.ium.data.util.DataSourceProxy;
import com.slocpi.ium.util.CodeHelper;

/**
 * This is the business object for the Exceptions this contains the retrieval of exceptions.
 * @author Cris
 */
public class AdminExceptionLog {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(AdminExceptionLog.class);
	/**
	 * Retrieves exceptions for this filter
	 * @param filter 
	 * @return ArrayLis of ExceptionData
	 * @throws IUMException
	 */
	public ArrayList retrieveExceptions(ExceptionFilterData filter) throws IUMException{
		
		LOGGER.info("retrieveExceptions start");
		
		ArrayList list = new ArrayList();
		ExceptionDAO dao = new ExceptionDAO();
		try {
			list = dao.retrieveExceptions(filter);
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMException(e);
		}
		
		LOGGER.info("retrieveExceptions end");
		return list;
	}
	
	/**
	 * Retrieves the exception details for this exception id
	 * @param exceptionId 
	 * @return ArrayList of ExceptionDetailData
	 * @throws IUMException
	 */
	public ArrayList retrieveExceptionDetails(long exceptionId) throws IUMException{
		
		LOGGER.info("retrieveExceptionDetails start");
		
		ArrayList list = new ArrayList();
		ExceptionDAO dao = new ExceptionDAO();
		
		try {
			list = dao.retrieveExceptionDetails(exceptionId);
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMException(e);
		}
		
		LOGGER.info("retrieveExceptionDetails end");
		return list;
	}
	
	private void closeConn(Connection conn)throws IUMException{
		
		LOGGER.info("closeConn start");
		try{
			if (conn != null){
				conn.close();
				return;
			}
		}
		catch (Exception ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		LOGGER.info("closeConn end");
	}
	
}
