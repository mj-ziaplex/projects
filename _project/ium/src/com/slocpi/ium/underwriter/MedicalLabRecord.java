package com.slocpi.ium.underwriter;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.slocpi.ium.IUMException;
import com.slocpi.ium.data.AssessmentRequestData;
import com.slocpi.ium.data.AuditTrailData;
import com.slocpi.ium.data.MedExamFiltersData;
import com.slocpi.ium.data.MedicalRecordData;
import com.slocpi.ium.data.PolicyMedicalRecordData;
import com.slocpi.ium.data.PurgeStatisticData;
import com.slocpi.ium.data.StatusData;
import com.slocpi.ium.data.UserProfileData;
import com.slocpi.ium.data.dao.AuditTrailDAO;
import com.slocpi.ium.data.dao.ClientDAO;
import com.slocpi.ium.data.dao.MedicalDAO;
import com.slocpi.ium.data.dao.ProcessConfigDAO;
import com.slocpi.ium.data.dao.PurgeStatisticDAO;
import com.slocpi.ium.data.dao.ReferenceDAO;
import com.slocpi.ium.data.dao.UserDAO;
import com.slocpi.ium.data.util.DataSourceProxy;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.DateHelper;
import com.slocpi.ium.util.IUMConstants;
import com.slocpi.ium.util.SortHelper;
import com.slocpi.ium.workflow.Workflow;
import com.slocpi.ium.workflow.WorkflowItem;

/**
 * @author Engel
 * This is the business object for the Medical Records. 
 *
 */
public class MedicalLabRecord {
	private static final Logger LOGGER = LoggerFactory.getLogger(MedicalLabRecord.class);
	/**
	 * Retrieves sorted medical records based on the attributes defined in the medExamFilters
	 * @param medExamFilters filter to be applied in the search
	 * @param sort define how the result will be sorted
	 * @return ArrayList of MedicalRecordData
	 * @throws IUMException
	 */
	public ArrayList getMedicalExamRequest(MedExamFiltersData medExamFilters, SortHelper sort) throws IUMException {
		
		LOGGER.info("getMedicalExamRequest start");
		Connection conn = new DataSourceProxy().getConnection();
        try {
			MedicalDAO MedDAO = new MedicalDAO(conn);
			ArrayList records;
            records = MedDAO.getMedicalRecords(medExamFilters, sort);
			return records;
		} catch (SQLException ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		} catch (Exception ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		} finally {
			closeConnection(conn);
			LOGGER.info("getMedicalExamRequest end");
		}
	}

	
	/**
	 * Inserts a record in the MEDICAL_RECORDS and POLICY_MEDICAL_RECORDS table. It sends a notification to the defined recipients if 
	 * the records are successfully created. 
	 * @param medicalExam MedicalRecordData to be created
	 * @throws IUMException
	 */
	public void createMedicalExam(MedicalRecordData medicalExam) throws IUMException{
		
		LOGGER.info("createMedicalExam start");
		Connection conn = new DataSourceProxy().getConnection();
		
		try {			
			MedicalDAO dao = new MedicalDAO(conn);
			if (medicalExam.getClient().getClientId() != null)
				if (!medicalExam.getClient().getClientId().equals(""))
					if (dao.isMedicalRecordExist(medicalExam.getClient().getClientId(), medicalExam.getTest().getTestId())){
						LOGGER.warn("Duplicate  medical record.");
						throw new UnderWriterException("Duplicate medical record.");
					}		

			AssessmentRequest AR = new AssessmentRequest();
			AssessmentRequestData ARData = new AssessmentRequestData();
			PolicyMedicalRecordData policyMedicalRecordData = medicalExam.getPolicyMedicalRecordData(); 
			LOGGER.debug("Ref No : " + policyMedicalRecordData.getReferenceNumber());			
			if (policyMedicalRecordData.getReferenceNumber() != null) {
				ARData = AR.getDetails(policyMedicalRecordData.getReferenceNumber());
				if (!medicalExam.getRequestingParty().equalsIgnoreCase("Agent") &&
					!medicalExam.getRequestingParty().equalsIgnoreCase(ARData.getLob().getLOBCode())) {
					throw new UnderWriterException("Requesting party does not match the line of business.");
				}
			}
			conn.setAutoCommit(false);
			if (medicalExam.getClient().getClientId() != null) {
				if (!medicalExam.getClient().getClientId().equals("")) {
					ClientDAO clientDAO = new ClientDAO();
					if (clientDAO.isClientExists(medicalExam.getClient().getClientId())) {
						clientDAO.updateClientInfo(medicalExam.getClient());
					} else {
						throw new UnderWriterException("There are no existing client record with id " + medicalExam.getClient().getClientId() + "."); //throw new IUMException("There are no existing client record with id " + medicalExam.getClient().getClientId() + ".");
					}
				}
			}

			dao.createMedicalExam(medicalExam);
			conn.commit();
			if (policyMedicalRecordData.getReferenceNumber() != null) {
				PolicyMedicalRecordData policyMedicalRecord = new PolicyMedicalRecordData();
				policyMedicalRecord.setMedicalRecordId(medicalExam.getMedicalRecordId());
				policyMedicalRecord.setReferenceNumber(policyMedicalRecordData.getReferenceNumber());
				policyMedicalRecord.setAssociationType("1");
				dao.insertPolicyMedicalRecord(policyMedicalRecord);
				conn.commit();

				ArrayList recipients = new ArrayList();
				UserDAO userDAO = new UserDAO();
				recipients = userDAO.selectUsersPerRoleBranch(IUMConstants.ROLES_MEDICAL_ADMIN, ARData.getBranch().getOfficeId());
				LOGGER.debug("create = " + "lob : " + ARData.getLob().getLOBCode() + " created by : " + medicalExam.getCreatedBy() + " no of recipients : " + recipients.size());
				if (recipients.size() > 0)
					this.executeWorkflow(medicalExam, ARData.getLob().getLOBCode(), medicalExam.getCreatedBy(), recipients, null);
		
				if (ARData.getStatus().getStatusId() == IUMConstants.STATUS_AWAITING_REQUIREMENTS){
					UserProfileData userData = new UserProfileData();
					userData.setUserId(medicalExam.getCreatedBy());
					ARData.setUpdatedBy(userData);
					AR.setPendingRequest(ARData);
					AR.assignTo(ARData.getReferenceNumber(), medicalExam.getFacilitator(), medicalExam.getCreatedBy());
				}
				else if (ARData.getStatus().getStatusId() == IUMConstants.STATUS_AWAITING_MEDICAL)
					AR.assignTo(ARData.getReferenceNumber(), medicalExam.getFacilitator(), medicalExam.getCreatedBy());					
			}
			
		}
		catch (SQLException sqlE) {
			LOGGER.error(CodeHelper.getStackTrace(sqlE));
			try {
				conn.rollback();
			} catch (SQLException e) {
				LOGGER.error(CodeHelper.getStackTrace(e));
			}
			throw new IUMException(sqlE);
		}
		catch (Exception e){
			LOGGER.error(CodeHelper.getStackTrace(e));
			if (e instanceof UnderWriterException) {
				throw (UnderWriterException)e;
			}
			else {
				LOGGER.error(CodeHelper.getStackTrace(e));
				throw new IUMException(e);
			}			
		}
		finally{
			closeConnection(conn);
			LOGGER.info("createMedicalExam end");
		}

	}
	
	
	/**
	 * Retrieves medical records based on the attributes defined in the medExamFilters
	 * @param medExamFilters filter to be applied in the search
	 * @return ArrayList of MedicalRecordData
	 * @throws IUMException
	 */
	public ArrayList getMedicalExamDetails(MedExamFiltersData medExamFilters) throws IUMException {		
		
		LOGGER.info("getMedicalExamDetails start");
		Connection conn = new DataSourceProxy().getConnection();
        try {
			MedicalDAO MedDAO = new MedicalDAO(conn);
			ArrayList records;
            records = MedDAO.retrieveMedicalRecords(medExamFilters.getReferenceNo());
            conn.close();
			return records;
		} catch (SQLException ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		} catch (Exception ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		} finally {
			closeConnection(conn);
			LOGGER.info("getMedicalExamDetails end");
		}
	}


	/**
	 * Retrieves medical record details for the given medical record id
	 * @param medicalRecordId 
	 * @return MedicalRecordData
	 * @throws IUMException
	 */
	public MedicalRecordData getMedicalRecord(long medicalRecordId) throws IUMException {		
		
		LOGGER.info("getMedicalRecord start");
		Connection conn = new DataSourceProxy().getConnection();
        try {
			MedicalDAO MedDAO = new MedicalDAO(conn);
			MedicalRecordData record;
            record = MedDAO.retrieveMedicalRecord(medicalRecordId);
            conn.close();
			return record;
		} catch (SQLException ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		} catch (Exception ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		} finally {
			closeConnection(conn);		 
			LOGGER.info("getMedicalRecord end");
		}
	}
	
	
	/**
	 * @TODO method description receiveMedExamResult
	 * @param medExamResult
	 */
	
	public void receiveMedicalExamResult(ArrayList medicalRecords) throws IUMException {
		
		LOGGER.info("receiveMedicalExamResult start");
		StringBuffer invalidMedicalRecords = new StringBuffer();
		Connection conn = new DataSourceProxy().getConnection();

		try {
			for (int i = 0; i < medicalRecords.size(); i++) {
				MedicalRecordData data = (MedicalRecordData) medicalRecords.get(i);
				
				StatusData prevStatus= data.getStatus();
				data.setPrevStatus(Long.toString(prevStatus.getStatusId()));
    			
				if (data.getReferenceNo() != null) {
					AssessmentRequest AR = new AssessmentRequest();
					AssessmentRequestData ARData = new AssessmentRequestData();
					ARData = AR.getDetails(data.getReferenceNo());

					if (this.isStatusValid(ARData.getLob().getLOBCode(), data.getStatus().getStatusId(), IUMConstants.STATUS_VALID)){
					
						MedicalDAO dao = new MedicalDAO(conn);
						conn.setAutoCommit(false);
						dao.changeStatus(data.getMedicalRecordId(), IUMConstants.STATUS_VALID, data.getUpdatedBy());						
						dao.updateDateReceive(data.getMedicalRecordId(), new Date());
						dao.updateResultsReceived(data.getMedicalRecordId(), data.isReceivedResults());
						dao.updateRequestLOA(data.getMedicalRecordId(), data.isRequestForLOAInd());
						dao.updateDateConducted(data.getMedicalRecordId(), data.getConductedDate());						
						dao.updateValidityDate(data.getMedicalRecordId(), this.computeValidityDate(data.getTest().getTestId(), data.getConductedDate())); //dao.updateValidityDate(data.getMedicalRecordId(), data.getValidityDate());//
						dao.updateRemarks(data.getMedicalRecordId(), data.getRemarks(), data.getUpdatedBy());
						
						PolicyMedicalRecordData policyMedicalRecord = new PolicyMedicalRecordData();
						policyMedicalRecord.setMedicalRecordId(data.getMedicalRecordId());
						policyMedicalRecord.setReferenceNumber(data.getPolicyMedicalRecordData().getReferenceNumber());
						policyMedicalRecord.setAssociationType("1");
						if (!dao.isPolicyMedicalRecordExist(policyMedicalRecord))
							dao.insertPolicyMedicalRecord(policyMedicalRecord);
						conn.commit();
						ArrayList recipients = new ArrayList();
						recipients.add(ARData.getAssignedTo().getUserId());
						if (recipients.size() > 0){
							data.setPrevStatus(String.valueOf(data.getStatus().getStatusId()));
							this.executeWorkflow(data, ARData.getLob().getLOBCode(), data.getUpdatedBy(), recipients, null);
						}
					} else {
						if (invalidMedicalRecords.length() > 0) {
							invalidMedicalRecords.append(", ");
						} 
						invalidMedicalRecords.append(data.getMedicalRecordId());
					}
				} else {
					if (invalidMedicalRecords.length() > 0) {
						invalidMedicalRecords.append(", ");
					} 
					invalidMedicalRecords.append(data.getMedicalRecordId());
				}
			}
			
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			try {
				conn.rollback();
			} catch (SQLException e1) {
			
				LOGGER.error(CodeHelper.getStackTrace(e1));
			}
			throw new IUMException(e);
		} finally {
			closeConnection(conn);
		}
		
		if (invalidMedicalRecords.length() > 0) {
			LOGGER.warn("Medical record/s " + invalidMedicalRecords.toString() + " is/are invalid for receiving.");
			throw new UnderWriterException("Medical record/s " + invalidMedicalRecords.toString() + " is/are invalid for receiving.");
		}
		
		LOGGER.info("receiveMedicalExamResult end");
	}

	/**
	 * @TODO method description updateMedExamNoSubmission
	 * @param medExam
	 */
	public void updateMedExamNoSubmission(ArrayList medExam) throws IUMException {
		
		LOGGER.info("updateMedExamNoSubmission start");
		StringBuffer invalidMedicalRecords = new StringBuffer();
		Connection conn = new DataSourceProxy().getConnection();
		MedicalDAO medDao = new MedicalDAO(conn);	

		try {
			for (int i = 0; i < medExam.size(); i++) {
				MedicalRecordData record = (MedicalRecordData) medExam.get(i);
				if (record.getReferenceNo() != null) {
					AssessmentRequest AR = new AssessmentRequest();
					AssessmentRequestData ARData = new AssessmentRequestData();
					ARData = AR.getDetails(record.getReferenceNo());

					if (this.isStatusValid(ARData.getLob().getLOBCode(), record.getStatus().getStatusId(), IUMConstants.STATUS_NOT_SUBMITTED) &&
						record.getFollowUpDate() != null) {
						if (DateHelper.compare(record.getFollowUpDate(), new Date()) < 0) {
							conn.setAutoCommit(false);
							medDao.changeStatus(record.getMedicalRecordId(), IUMConstants.STATUS_NOT_SUBMITTED, record.getUpdatedBy());
							conn.commit();
						} else {
							if (invalidMedicalRecords.length() > 0) {
								invalidMedicalRecords.append(", ");
							} 
							invalidMedicalRecords.append(record.getMedicalRecordId());
						}
					} else {
						if (invalidMedicalRecords.length() > 0) {
							invalidMedicalRecords.append(", ");
						} 
						invalidMedicalRecords.append(record.getMedicalRecordId());
					}
				} else {
					if (invalidMedicalRecords.length() > 0) {
						invalidMedicalRecords.append(", ");
					} 
					invalidMedicalRecords.append(record.getMedicalRecordId());
				}
			}
		} catch (SQLException ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			try {
				conn.rollback();
			} catch (SQLException e) {
				LOGGER.error(CodeHelper.getStackTrace(e));
			}		
			throw new IUMException(ex);
		} finally {
			closeConnection(conn);
		}
		if (invalidMedicalRecords.length() > 0) {
			LOGGER.warn("Medical record/s " + invalidMedicalRecords.toString() + " is/are invalid to be tagged as Not Submitted.");
			throw new UnderWriterException("Medical record/s " + invalidMedicalRecords.toString() + " is/are invalid to be tagged as Not Submitted.");
		}
		LOGGER.info("updateMedExamNoSubmission end");
	}
	
	
	/**
	 * Retrieves medical records associated to this reference number
	 * @param referenceNumber
	 * @return ArrayList of MedicalRecordData
	 * @throws IUMException
	 */
	public ArrayList getMedicalLabRecords(String referenceNumber) throws IUMException{
		
		LOGGER.info("getMedicalLabRecords start");
		Connection conn = new DataSourceProxy().getConnection();
		ArrayList list = new ArrayList();
		MedicalDAO dao = new MedicalDAO(conn);
		try {
			list = dao.retrieveMedicalRecords(referenceNumber);
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMException(e);
		} finally{
			closeConnection(conn);
		}
		LOGGER.info("getMedicalLabRecords end");
		return list;
	}
	
	/**
	 * @Updates the chargeableTo field of the medical Record
	 * @param medicalRecordId
	 * @param LOBCode
	 */
	public void updateChargeTo(String referenceNumber, String chargeTo, String updatedBy) throws IUMException {	
		
		LOGGER.info("updateChargeTo start");
		Connection conn = new DataSourceProxy().getConnection();
        try {
			MedicalDAO medDAO = new MedicalDAO(conn);
			conn.setAutoCommit(false);
            medDAO.updateChargeableTo(referenceNumber,chargeTo,updatedBy);
            conn.commit();
		} catch (SQLException ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			try {
				conn.rollback();
			} catch (SQLException e) {
				LOGGER.error(CodeHelper.getStackTrace(e));
			}
			throw new IUMException(ex);
		} catch (Exception ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		} finally {
			closeConnection(conn);
			LOGGER.info("updateChargeTo end");
		}
	}
	
	/**
	 * @Sets the status of medical records to cancelled
	 * @param medicalRecordId
	 */
	public void setCancelled(String referenceNumber, String updatedBy) throws IUMException {	
		
		LOGGER.info("setCancelled start");
		Connection conn = new DataSourceProxy().getConnection();
        try {
			MedicalDAO medDAO = new MedicalDAO(conn);
			conn.setAutoCommit(false);
            medDAO.updateStatus(referenceNumber, IUMConstants.STATUS_CANCELLED, updatedBy);
            conn.commit();
		} catch (SQLException ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			try {
				conn.rollback();
			} catch (SQLException e) {
				LOGGER.error(CodeHelper.getStackTrace(e));
			}
			throw new IUMException(ex);
		} catch (Exception ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		} finally {
			closeConnection(conn);
			LOGGER.info("setCancelled end");
		}
	}

	/**
	 * Updates the status of the medical records in the arrayList to Confirmed. It will send a notification to the defined recipients if
	 * the process was successfully executed.
	 * @param medicalRecords ArrayList of MedicalRecordData
	 * @throws IUMException
	 */
	public void confirmMedicalExam(ArrayList medicalRecords) throws IUMException {
		
		LOGGER.info("confirmMedicalExam start");
		StringBuffer invalidMedicalRecords = new StringBuffer();
		Connection conn = new DataSourceProxy().getConnection();

		try {
		for (int i = 0; i < medicalRecords.size(); i++) {
			MedicalRecordData data = (MedicalRecordData) medicalRecords.get(i);
			
			StatusData prevStatus= data.getStatus();
			data.setPrevStatus(Long.toString(prevStatus.getStatusId()));
	        
				if (data.getReferenceNo() != null) {
					AssessmentRequest AR = new AssessmentRequest();
					AssessmentRequestData ARData = new AssessmentRequestData();
					ARData = AR.getDetails(data.getReferenceNo());

	            	if (this.isStatusValid(ARData.getLob().getLOBCode(), data.getStatus().getStatusId(), IUMConstants.STATUS_CONFIRMED) &&
	            		DateHelper.compare(data.getAppointmentDateTime(), data.getRequestedDate()) >= 0) {
	            			
            			MedicalDAO dao = new MedicalDAO(conn);
            			conn.setAutoCommit(false);
            			dao.changeStatus(data.getMedicalRecordId(), IUMConstants.STATUS_CONFIRMED, data.getUpdatedBy());
            			dao.updateAppointmentDate(data.getMedicalRecordId(), data.getAppointmentDateTime());
						dao.updateResultsReceived(data.getMedicalRecordId(), data.isReceivedResults());
						dao.updateRequestLOA(data.getMedicalRecordId(), data.isRequestForLOAInd());
						dao.updateRemarks(data.getMedicalRecordId(), data.getRemarks(), data.getUpdatedBy());
						
						PolicyMedicalRecordData policyMedicalRecord = new PolicyMedicalRecordData();
						policyMedicalRecord.setMedicalRecordId(data.getMedicalRecordId());
						policyMedicalRecord.setReferenceNumber(data.getPolicyMedicalRecordData().getReferenceNumber());
						policyMedicalRecord.setAssociationType("1");
						if (!dao.isPolicyMedicalRecordExist(policyMedicalRecord))
							dao.insertPolicyMedicalRecord(policyMedicalRecord);
						conn.commit();
						ArrayList recipients = new ArrayList();
						UserDAO userDAO = new UserDAO();
						recipients = userDAO.selectUsersPerRoleBranch(IUMConstants.ROLES_MARKETING_STAFF, ARData.getBranch().getOfficeId());
						String agentId = ARData.getAgent().getUserId();
						recipients.add(agentId);
						LOGGER.debug("recipients size : " + recipients.size());
						if (recipients.size() > 0){
							data.setPrevStatus(String.valueOf(data.getStatus().getStatusId()));
							this.executeWorkflow(data, ARData.getLob().getLOBCode(), data.getUpdatedBy(), recipients, agentId);
						}
					} else {
						if (invalidMedicalRecords.length() > 0) {
							invalidMedicalRecords.append(", ");
						} 
						invalidMedicalRecords.append(data.getMedicalRecordId());
            		}
            	} else {
            		if (invalidMedicalRecords.length() > 0) { 
            			invalidMedicalRecords.append(", ");
            		} 
            		invalidMedicalRecords.append(data.getMedicalRecordId());
            	}
			
		}
		 
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			try {
				conn.rollback();
			} catch (SQLException e1) {
				LOGGER.error(CodeHelper.getStackTrace(e1));
			}
			throw new IUMException(e.getMessage());
		} catch (Exception e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMException(e.getMessage());
		} finally {
			closeConnection(conn);
		}
		if (invalidMedicalRecords.length() > 0) {
			LOGGER.warn("Medical record/s " + invalidMedicalRecords.toString() + " is/are invalid for confirmation.");
			throw new UnderWriterException("Medical record/s " + invalidMedicalRecords.toString() + " is/are invalid for confirmation.");
		}
		LOGGER.info("confirmMedicalExam end");
	}
	
	
	/**
	 * Updates the status of medical records which validity date is less than the current date to Expire.
	 * @throws IUMException
	 */
	public void expireMedicalRecords() throws IUMException {	
		
		LOGGER.info("expireMedicalRecords start");
		Connection conn = new DataSourceProxy().getConnection();
		try {
			MedicalDAO dao = new MedicalDAO(conn);
			conn.setAutoCommit(false);
			dao.expireMedicalRecords(IUMConstants.STATUS_VALID, IUMConstants.STATUS_EXPIRED);
			conn.commit();
			this.createAuditTrail();
		} catch (SQLException ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			try {
				conn.rollback();
			} catch (SQLException e) {
				LOGGER.error(CodeHelper.getStackTrace(e));
			}
			throw new IUMException(ex);
		} catch (Exception ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		} finally {
			closeConnection(conn);
			LOGGER.info("expireMedicalRecords end");
		}
	}
	
	/**
	 * Updates the status of the medical record in the arrayList to Cancelled. It will send a notification to the defined recipients
	 * if the medical record was successfully cancelled else it will throw an UnderWriterException.
	 * @param medicalRecords ArrayList of MedicalRecordData
	 * @throws IUMException
	 */
	public void cancelMedicalExam(ArrayList medicalRecords) throws IUMException {

		LOGGER.info("cancelMedicalExam start");
		StringBuffer invalidMedicalRecords = new StringBuffer();
		Connection conn = new DataSourceProxy().getConnection();

		try {
			for (int i = 0; i < medicalRecords.size(); i++) {
				MedicalRecordData data = (MedicalRecordData) medicalRecords.get(i);
				if (data.getReferenceNo() != null) {
					AssessmentRequest AR = new AssessmentRequest();
					AssessmentRequestData ARData = new AssessmentRequestData();
					ARData = AR.getDetails(data.getReferenceNo());

					if (this.isStatusValid(ARData.getLob().getLOBCode(), data.getStatus().getStatusId(), IUMConstants.STATUS_CANCELLED)) {
						MedicalDAO dao = new MedicalDAO(conn);
						conn.setAutoCommit(false);
						dao.changeStatus(data.getMedicalRecordId(), IUMConstants.STATUS_CANCELLED, data.getUpdatedBy());
						conn.commit();
            
						ArrayList recipients = new ArrayList();
						recipients.add(ARData.getAssignedTo());
						if (recipients.size() > 0){
							data.getBranch().setOfficeId(ARData.getBranch().getOfficeId());
							data.setPrevStatus(String.valueOf(data.getStatus().getStatusId()));
							this.executeWorkflow(data, ARData.getLob().getLOBCode(), data.getUpdatedBy(), recipients, null);
						}
					} else {
						if (invalidMedicalRecords.length() > 0) {
							invalidMedicalRecords.append(", ");
						} 
						invalidMedicalRecords.append(data.getMedicalRecordId());
					}
				} else {
					if (invalidMedicalRecords.length() > 0) {
						invalidMedicalRecords.append(", ");
					} 
					invalidMedicalRecords.append(data.getMedicalRecordId());
				}
			}            
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			try {
				conn.rollback();
			} catch (SQLException e1) {
				LOGGER.error(CodeHelper.getStackTrace(e));
			}
			throw new IUMException(e.getMessage());
		} finally {
			closeConnection(conn);
		}
		
		if (invalidMedicalRecords.length() > 0) {
			LOGGER.warn("Medical record/s " + invalidMedicalRecords.toString() + " is/are invalid for cancellation.");
			throw new UnderWriterException("Medical record/s " + invalidMedicalRecords.toString() + " is/are invalid for cancellation.");
		}
		LOGGER.info("cancelMedicalExam end");
	}

	/**
	 * Updates the medical records and also the client information associated witht the medical record.
	 * @param data MedicalRecordData
	 * @throws IUMException
	 */
	public void maintainMedicalRecord(MedicalRecordData data) throws IUMException {
		
		LOGGER.info("maintainMedicalRecord start");
		Connection conn = new DataSourceProxy().getConnection();
			try {
				AssessmentRequest AR = new AssessmentRequest();
				AssessmentRequestData ARData = new AssessmentRequestData();
				PolicyMedicalRecordData policyMedicalRecordData = data.getPolicyMedicalRecordData(); //this is where to get the reference number
				LOGGER.debug("Ref No : " + policyMedicalRecordData.getReferenceNumber());			
				if (policyMedicalRecordData.getReferenceNumber() != null) {
					ARData = AR.getDetails(policyMedicalRecordData.getReferenceNumber());
					if (!data.getRequestingParty().equalsIgnoreCase("Agent") &&
						!data.getRequestingParty().equalsIgnoreCase(ARData.getLob().getLOBCode())) {
						throw new UnderWriterException("Requesting party does not match the line of business.");
					}
				}
			
				MedicalDAO dao = new MedicalDAO(conn);	
				conn.setAutoCommit(false);
				dao.updateMedicalRecord(data);
									
				if (data.getClient().getClientId() != null) {
					if (!data.getClient().getClientId().equals("")) {
						ClientDAO clientDAO = new ClientDAO();
						if (clientDAO.isClientExists(data.getClient().getClientId())) {
							clientDAO.updateClientInfo(data.getClient());
						} else {
							LOGGER.warn("There are no existing client record with id " + data.getClient().getClientId() + ".");
							throw new IUMException("There are no existing client record with id " + data.getClient().getClientId() + ".");
						}
					}
				}
				conn.commit();
				
				if (policyMedicalRecordData.getReferenceNumber() != null) {
					PolicyMedicalRecordData policyMedicalRecord = new PolicyMedicalRecordData();
					policyMedicalRecord.setMedicalRecordId(data.getMedicalRecordId());
					policyMedicalRecord.setReferenceNumber(policyMedicalRecordData.getReferenceNumber());
					policyMedicalRecord.setAssociationType("1");
					if (!dao.isPolicyMedicalRecordExist(policyMedicalRecord)){
						dao.insertPolicyMedicalRecord(policyMedicalRecord);
					}

					LOGGER.debug("create = " + "lob : " + ARData.getLob().getLOBCode() + " updated by : " + data.getUpdatedBy());
					if (ARData.getStatus().getStatusId() == IUMConstants.STATUS_AWAITING_REQUIREMENTS){
						UserProfileData userData = new UserProfileData();
						userData.setUserId(data.getUpdatedBy());
						ARData.setUpdatedBy(userData);
						AR.setPendingRequest(ARData);
						AR.assignTo(ARData.getReferenceNumber(), data.getFacilitator(), data.getUpdatedBy());
					}
					else if (ARData.getStatus().getStatusId() == IUMConstants.STATUS_AWAITING_MEDICAL)
						AR.assignTo(ARData.getReferenceNumber(), data.getFacilitator(), data.getUpdatedBy());					
				}
				
				//conn.close();
			} catch (SQLException e) {
				LOGGER.error(CodeHelper.getStackTrace(e));
				try {
					conn.rollback();
				} catch (SQLException e1) {
					LOGGER.error(CodeHelper.getStackTrace(e));
				}
				throw new IUMException(e);
			} finally {
				closeConnection(conn);
			}
			LOGGER.info("maintainMedicalRecord end");
	}
		
	/**
	 * Updates the client information in the MEDICAL_RECORDS and inserts a record in the POLICY_MEDICAL_RECORDS table.
	 * @param data MedicalRecordData 
	 * @throws IUMException
	 */
	public void maintainClientInfo(MedicalRecordData data) throws IUMException {
		
		LOGGER.info("maintainClientInfo start");
		Connection conn = new DataSourceProxy().getConnection();
		try {	
			MedicalDAO dao = new MedicalDAO(conn);
			conn.setAutoCommit(false);
			dao.updateClientInfo(data);
			
						
			if (data.getPolicyMedicalRecordData().getReferenceNumber() != null) {
				PolicyMedicalRecordData policyMedicalRecord = new PolicyMedicalRecordData();
				policyMedicalRecord.setMedicalRecordId(data.getMedicalRecordId());
				policyMedicalRecord.setReferenceNumber(data.getPolicyMedicalRecordData().getReferenceNumber());
				policyMedicalRecord.setAssociationType("1");
				dao.insertPolicyMedicalRecord(policyMedicalRecord);
			}
					
			if (data.getClient().getClientId() != null) {
				if (!data.getClient().getClientId().equals("")) {									
					ClientDAO clientDAO = new ClientDAO();
					if (clientDAO.isClientExists(data.getClient().getClientId()))
						clientDAO.updateClientInfo(data.getClient());
					else
						LOGGER.warn("There are no existing client record with id " + data.getClient().getClientId() + ".");
						throw new IUMException("There are no existing client record with id " + data.getClient().getClientId() + ".");
				}
			}
			conn.commit();
		}
		catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			try {
				conn.rollback();
			} catch (SQLException e1) {
				LOGGER.error(CodeHelper.getStackTrace(e));
			}
			throw new IUMException(e);
		}
		finally {
			closeConnection(conn);
			LOGGER.info("maintainClientInfo end");
		}
	}
		
	private Date computeValidityDate(long testID, Date dateConducted) throws IUMException {
		
		LOGGER.info("computeValidityDate start");
		Date validityDate;
		Connection conn = new DataSourceProxy().getConnection();
        try {
			int validityNum = 0;
			ReferenceDAO dao = new ReferenceDAO(conn);
            validityNum = dao.retrieveTestProfile(testID).getValidity();
            validityDate =DateHelper.add(dateConducted, Calendar.DATE, validityNum);
		} catch (SQLException ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		} catch (Exception ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		} finally {
			closeConnection(conn);
		}
        LOGGER.info("computeValidityDate end");
		return validityDate;
	}
	
	private void executeWorkflow(MedicalRecordData medicalRecord, String lob, String sender, ArrayList recipients, String agentId) throws IUMException {

		LOGGER.info("executeWorkflow start");
		Workflow workflow = new Workflow();
		WorkflowItem workflowItem = new WorkflowItem();
		workflowItem.setObjectID(medicalRecord.getMedicalRecordId());
		workflowItem.setPreviousStatus(medicalRecord.getPrevStatus());
		workflowItem.setType(IUMConstants.MEDICAL_RECORDS);
		workflowItem.setLOB(lob);
		workflowItem.setSender(sender);
		workflowItem.setBranchId(medicalRecord.getBranch().getOfficeId());
		workflowItem.setPrimaryRecipient(recipients);
		workflowItem.setAgentId(agentId);
		LOGGER.debug("object id : " + workflowItem.getObjectID() + "prev stat : " + workflowItem.getPreviousStatus() + " type : " + workflowItem.getType() + " lob : " + workflowItem.getLOB() + " sender : " + workflowItem.getSender() + " branch : " + workflowItem.getBranchId() + "  no of recipients : " + recipients.size());
		try {
			workflow.processState(workflowItem);
		} catch (Exception e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMException(e);
		}
		LOGGER.info("executeWorkflow end");
	}
	
	/**
	 * Updates the client number of the medical record in the arrayList. Only medical record with the following status will be updated: Requested, Confirmed,
	 * and Valid.
	 * @param list ArrayList of MedicalRecordData
	 * @param clientNum client number
	 * @throws IUMException
	 */
	public void applyClientNumber(ArrayList list, String clientNum) throws IUMException {
		
		LOGGER.info("applyClientNumber start");
		Connection conn = new DataSourceProxy().getConnection();
		try {
			ClientDAO clientDAO = new ClientDAO();
			if (clientDAO.retrieveClient(clientNum) == null) {
				throw new UnderWriterException("Invalid client number.");
			} else {
				StringBuffer invalidMedicalRecords = new StringBuffer();
				MedicalDAO medicalDAO = new MedicalDAO(conn);
				for (int i = 0; i < list.size(); i++) {
					MedicalRecordData data = (MedicalRecordData) list.get(i);
					if ((data.getStatus().getStatusId() == IUMConstants.STATUS_REQUESTED ||
						data.getStatus().getStatusId() == IUMConstants.STATUS_CONFIRMED ||
						data.getStatus().getStatusId() == IUMConstants.STATUS_VALID) &&
						(data.getClient().getClientId() == null ||
						data.getClient().getClientId().equalsIgnoreCase(""))) {
						conn.setAutoCommit(false);
						medicalDAO.updateClientNum(data.getMedicalRecordId(), clientNum, data.getUpdatedBy());
						conn.commit();
					} else {
						if (invalidMedicalRecords.length() > 0) {
							invalidMedicalRecords.append(", ");
						} 
						invalidMedicalRecords.append(data.getMedicalRecordId());
					}
				}
				if (invalidMedicalRecords.length() > 0) {
					LOGGER.warn("Medical record/s " + invalidMedicalRecords.toString() + " is/are invalid for editing.");
					throw new UnderWriterException("Medical record/s " + invalidMedicalRecords.toString() + " is/are invalid for editing.");
				}
			}
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			try {
				conn.rollback();
			} catch (SQLException e1) {
				LOGGER.error(CodeHelper.getStackTrace(e));
			}
			throw new IUMException(e);
		} finally {
			closeConnection(conn);
			LOGGER.info("applyClientNumber end");
		}
	}
	
	/**
	 * This updates the chargeableTo field of the medical records in the arrayList. Only medical records with the following status will be updated: Requested,
	 * Confirmed and Valid. 
	 * @param list ArrayList of MedicalRecordData
	 * @param chargableTO user whom the medical record will be charged to 
	 * @throws IUMException
	 */
	public void applyChargableTo(ArrayList list, String chargableTO) throws IUMException {
		
		LOGGER.info("applyChargableTo start");
		Connection conn = new DataSourceProxy().getConnection();
		try {
			StringBuffer invalidMedicalRecords = new StringBuffer();
			MedicalDAO medicalDAO = new MedicalDAO(conn);
			for (int i = 0; i < list.size(); i++) {
				MedicalRecordData data = (MedicalRecordData) list.get(i);
				if (data.getStatus().getStatusId() == IUMConstants.STATUS_REQUESTED ||
					data.getStatus().getStatusId() == IUMConstants.STATUS_CONFIRMED ||
					data.getStatus().getStatusId() == IUMConstants.STATUS_VALID) {
					conn.setAutoCommit(false);
					medicalDAO.updateChargeableTo(data.getMedicalRecordId(), chargableTO, data.getUpdatedBy());
					conn.commit();
				} else {
					if (invalidMedicalRecords.length() > 0) {
						invalidMedicalRecords.append(", ");
					} 
					invalidMedicalRecords.append(data.getMedicalRecordId());
				}
			}
			if (invalidMedicalRecords.length() > 0) {
				LOGGER.warn("Medical record/s " + invalidMedicalRecords.toString() + " is/are invalid for editing.");
				throw new UnderWriterException("Medical record/s " + invalidMedicalRecords.toString() + " is/are invalid for editing.");
			}
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			try {
				conn.rollback();
			} catch (SQLException e1) {
				LOGGER.error(CodeHelper.getStackTrace(e));
			}
			throw new IUMException(e);
		} finally {
			closeConnection(conn);
			LOGGER.info("applyChargableTo end");
		}
	}

	
	/**
	 * Checks if the a record is existing in the PROCESS_CONFIGURATIONS table given the "lob", "from status" and "to status" 
	 * @param lob line of business
	 * @param fromStatus current status of the request
	 * @param toStatus to status of the request
	 * @return returns true if a record is found
	 * @throws IUMException
	 */
	public boolean isStatusValid(String lob, long fromStatus, long toStatus) throws IUMException {
		
		LOGGER.info("isStatusValid start");
		boolean isValid = false;
		
		try {
			ProcessConfigDAO dao = new ProcessConfigDAO();
			isValid = dao.isValidStatus(lob, IUMConstants.STAT_TYPE_M, fromStatus, toStatus);
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMException(e);
		}
		
		LOGGER.info("isStatusValid end");
		return isValid;
	}

	/**
	 * Save the remarks 
	 * @param data MedicalRecordData 
	 * @throws IUMException
	 */
	public void saveRemarks(MedicalRecordData data) throws IUMException {
		
		LOGGER.info("saveRemarks start");
		Connection conn = new DataSourceProxy().getConnection();
		try {
			MedicalDAO dao = new MedicalDAO(conn);
			conn.setAutoCommit(false);
			dao.updateRemarks(data.getMedicalRecordId(), data.getRemarks(), data.getUpdatedBy());
			conn.commit();
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			try {
				conn.rollback();
			} catch (SQLException e1) {
				LOGGER.error(CodeHelper.getStackTrace(e));
			}
			throw new IUMException(e);
		} finally {
			closeConnection(conn);
		}
		LOGGER.info("saveRemarks end");
	}

	private void createAuditTrail() throws IUMException {
		
		LOGGER.info("createAuditTrail start");
		Connection conn = new DataSourceProxy().getConnection();
		AuditTrailData data = new AuditTrailData();
		data.setTransRec(IUMConstants.TRAN_RECORD_AUTO_EXPIRE);
		data.setTransDate(new Date());
		data.setUserID(IUMConstants.USER_IUMS);
		AuditTrailDAO dao = new AuditTrailDAO(conn);
		
		try {
			conn.setAutoCommit(false);
			dao.insertAuditTrail(data);
			conn.commit();
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMException(e);
		} finally {
			closeConnection(conn);
		}
		LOGGER.info("createAuditTrail end");
	}

	/**
	 * Deletes medical records within the given date range. Inserts a record in the PURGE_STATISTICS table to
	 * store the information about the pruge process. 
	 * @param purgeDate date w
	 * @param startDate
	 * @param endDate
	 * @return
	 * @throws IUMException
	 */
	public int purgeMedicalRecords(Date purgeDate, Date startDate, Date endDate) throws IUMException {
		
		LOGGER.info("purgeMedicalRecords start");
		int recordCount = 0;
		String result = "F";
		Connection conn = new DataSourceProxy().getConnection();
		try {
			MedicalDAO dao = new MedicalDAO(conn);
			conn.setAutoCommit(false);
			recordCount = dao.deleteMedicalRecords(startDate, endDate);
			conn.commit();
			result = "S";
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			try {
				conn.rollback();
			} catch (SQLException e1) {
				LOGGER.error(CodeHelper.getStackTrace(e));
			} 
			throw new IUMException(e);
		} finally {
			PurgeStatisticData data = new PurgeStatisticData();
			data.setPurgeDate(purgeDate);
			data.setStartDate(startDate);
			data.setEndDate(endDate);
			data.setCriteria(IUMConstants.PURGE_MEDICAL_RECORDS);
			data.setNumberOfRecords(recordCount);
			data.setResult(result);

			PurgeStatisticDAO purgeDao = new PurgeStatisticDAO(conn);
			try {
				purgeDao.insertPurgeStatistic(data);
				conn.commit();
			} catch (SQLException e) {
				LOGGER.error(CodeHelper.getStackTrace(e));
				try {
					conn.rollback();
				} catch (SQLException e1) {
					LOGGER.error(CodeHelper.getStackTrace(e));
				}
				throw new IUMException(e);				
			} finally {
				closeConnection(conn);
			}
		}
		LOGGER.info("purgeMedicalRecords end");
		return recordCount;
	}
	
	/**
	 * Counts the number of medical records for expiration. Only medical records with a Valid status and with a validity date
	 * not greater than the current date will be counted.
	 * @return number of medical records for expiration
	 * @throws IUMException
	 */
	public int countMedRecordsForExpire() throws IUMException {
		
		LOGGER.info("countMedRecordsForExpire start");
		int count = 0;
		Connection conn = new DataSourceProxy().getConnection();
		try {
			MedicalDAO dao = new MedicalDAO(conn);
			count = dao.countMedRecordsForExpire();
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMException(e);
		} finally {
			closeConnection(conn);
		}
		LOGGER.info("countMedRecordsForExpire end");
		return count;
	}
	
	private void closeConnection(Connection conn) throws IUMException{
		
		
		if (null != conn){
			try {
				conn.close();
			
			} catch (SQLException sqlE) {
				LOGGER.error(CodeHelper.getStackTrace(sqlE));
				throw new IUMException(sqlE);
			}
		}
		
	}	
	
	
	/**
	 * Updates the status of medical records 
	 * @throws IUMException
	 */
	public void genericMRChangeStatus(ArrayList medicalRecords, long toStatus) throws IUMException {
		
		LOGGER.info("genericMRChangeStatus start");
		StringBuffer invalidMedicalRecords = new StringBuffer();
		Connection conn = new DataSourceProxy().getConnection();

		try {
			for (int i = 0; i < medicalRecords.size(); i++) {
				MedicalRecordData data = (MedicalRecordData) medicalRecords.get(i);
				if (data.getReferenceNo() != null) {
					AssessmentRequest AR = new AssessmentRequest();
					AssessmentRequestData ARData = new AssessmentRequestData();
					ARData = AR.getDetails(data.getReferenceNo());
					
					LOGGER.debug("data " + ARData.getLob().getLOBCode() +  data.getStatus().getStatusId() + toStatus);
					if (this.isStatusValid(ARData.getLob().getLOBCode(), data.getStatus().getStatusId(), toStatus)) {
						MedicalDAO dao = new MedicalDAO(conn);
						conn.setAutoCommit(false);
						dao.changeStatus(data.getMedicalRecordId(), toStatus, data.getUpdatedBy());
						conn.commit();
						ArrayList recipients = new ArrayList();
						recipients.add(ARData.getAssignedTo().getUserId());
						if (recipients.size() > 0){
							data.getBranch().setOfficeId(ARData.getBranch().getOfficeId());
							data.setPrevStatus(String.valueOf(data.getStatus().getStatusId()));
							this.executeWorkflow(data, ARData.getLob().getLOBCode(), data.getUpdatedBy(), recipients, null);
						}
					} else {
						if (invalidMedicalRecords.length() > 0) {
							invalidMedicalRecords.append(", ");
						} 
						invalidMedicalRecords.append(data.getMedicalRecordId());
					}
				} else {
					if (invalidMedicalRecords.length() > 0) {
						invalidMedicalRecords.append(", ");
					} 
					invalidMedicalRecords.append(data.getMedicalRecordId());
				}
			}            
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			try {
				conn.rollback();
			} catch (SQLException e1) {
				
				LOGGER.error(CodeHelper.getStackTrace(e));
			}
			throw new IUMException(e.getMessage());
		} finally {
			closeConnection(conn);
		}
		
		if (invalidMedicalRecords.length() > 0) {
			LOGGER.warn("Medical record/s " + invalidMedicalRecords.toString() + " is/are invalid for this status.");
			throw new UnderWriterException("Medical record/s " + invalidMedicalRecords.toString() + " is/are invalid for this status.");
		}
		
		LOGGER.info("genericMRChangeStatus end");
	}
	

	/**
	 * Returns true if status is an end status
	 * @param lob line of business of the assessmnet request
	 * @param fromStatus previous status of the assessment request
	 * @return boolean
	 * @throws IUMException
	 */
	public boolean isEndStatus(String lob, long fromStatus) throws IUMException {
		
		LOGGER.info("isEndStatus start");
		
		try{
			ProcessConfigDAO dao = new ProcessConfigDAO();
			boolean isEndStatus = dao.isEndStatus(lob, IUMConstants.STAT_TYPE_M, fromStatus);
			
			LOGGER.info("isEndStatus end");
			return isEndStatus;
			
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMException(e);
		}
	}

}
