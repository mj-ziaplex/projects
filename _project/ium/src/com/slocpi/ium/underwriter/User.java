/**
 * User.java				Copyright (c) 2004 Pointwest Technologies Corp.
 * TODO DOCUMENT ME!
 * 
 * @author aditalo	 @date Jan 22, 2004
 */
package com.slocpi.ium.underwriter;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.slocpi.ium.IUMException;
import com.slocpi.ium.data.UserProfileData;
import com.slocpi.ium.data.dao.AssessmentRequestDAO;
import com.slocpi.ium.data.dao.MedicalDAO;
import com.slocpi.ium.data.dao.UserDAO;
import com.slocpi.ium.data.util.DataSourceProxy;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.IUMConstants;

/**
 * TODO DOCUMENT ME!
 * 
 * @author aditalo		Copyright (c) 2004 Pointwest Technologies Corp.
 * @date Jan 22, 2004
 */
public class User {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(User.class);
	
	public int getPendingStatMR(UserProfileData userData) throws UnderWriterException, IUMException {
		
		LOGGER.info("getPendingStatMR start");
		
		try{
			
			int pendingStat = 0;
			String role = null;
			UserDAO dao = new UserDAO();
			String userID = userData.getUserId();
			role = dao.selectUserProfile(userID).getRole();
			
			if (role == null) {
				LOGGER.warn("NO USER ROLE");
				throw new UnderWriterException("No user role.");
			}
			
			if (role.equalsIgnoreCase(IUMConstants.ROLES_MEDICAL_ADMIN)) {
				pendingStat = this.countPendingMRs(userID, IUMConstants.ROLES_MEDICAL_ADMIN);
			
			} else if (role.equalsIgnoreCase(IUMConstants.ROLES_MEDICAL_SUPERVISOR)) {
				pendingStat = this.countPendingMRs(userID, IUMConstants.ROLES_MEDICAL_SUPERVISOR);
			
			} else if (role.equalsIgnoreCase(IUMConstants.ROLES_MEDICAL_CONSULTANT)) {
				pendingStat = this.countPendingMRs(userID, IUMConstants.ROLES_MEDICAL_CONSULTANT);
			
			} else if (role.equalsIgnoreCase(IUMConstants.ROLES_EXAMINER)) {
				pendingStat = this.countPendingMRs(userData.getUserCode(), IUMConstants.ROLES_EXAMINER);
			
			} else if (role.equalsIgnoreCase(IUMConstants.ROLES_LAB_STAFF)) {
				pendingStat = this.countPendingMRs(userData.getUserCode(), IUMConstants.ROLES_LAB_STAFF);
			
			}else if (role.equalsIgnoreCase(IUMConstants.ROLES_NB_STAFF)) {
				// None
	
			} else if (role.equalsIgnoreCase(IUMConstants.ROLES_NB_REVIEWER)) {
				// None
			} else if (role.equalsIgnoreCase(IUMConstants.ROLES_NB_SUPERVISOR)) {
				// None
	
			} else if (role.equalsIgnoreCase(IUMConstants.ROLES_NB_ADMIN)) {
				// None
		
			} else if (role.equalsIgnoreCase(IUMConstants.ROLES_FACILITATOR)) {
				// None
	
			} else if (role.equalsIgnoreCase(IUMConstants.ROLES_FACILITATOR_SUPERVISOR)) {
				// None
	
			} else if (role.equalsIgnoreCase(IUMConstants.ROLES_UNDERWRITER)) {
				// None
	
			} else if (role.equalsIgnoreCase(IUMConstants.ROLES_UNDERWRITER_SUPERVISOR)) {
				// None
	
			}  else if (role.equalsIgnoreCase(IUMConstants.ROLES_MARKETING_STAFF)) {
				// None
	
			} else if (role.equalsIgnoreCase(IUMConstants.ROLES_AGENTS)) {
				// None
	
			} else if (role.equalsIgnoreCase(IUMConstants.ROLES_OTHER_USER)) {
				// None 
			
			} else if (role.equalsIgnoreCase(IUMConstants.ROLES_LOV_ADMIN)) {
				// None
			
			} else if (role.equalsIgnoreCase(IUMConstants.ROLES_SECURITY_ADMIN)) {
				// None
			
			} else if (role.equalsIgnoreCase(IUMConstants.ROLES_SYSTEM_OPERATOR)) {
				// None
			
			} else {
				throw new UnderWriterException("Invalid user role.");
			}
	
			LOGGER.info("getPendingStatMR end");
			return pendingStat;
		}
		catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMException(e);
		}
		catch (Exception ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		
		
		
		
	}

	public int getPendingStatAR(UserProfileData userData) throws UnderWriterException, IUMException {
		
		LOGGER.info("getPendingStatAR start");
		 
		try {			
			
			int pendingStat = 0;
			
			String role = null;
			UserDAO dao = new UserDAO();
			String userID = userData.getUserId();
			role = dao.selectUserProfile(userID).getRole();
			
			if (role == null) {
				throw new UnderWriterException("No user role.");
			}
			
			if (role.equalsIgnoreCase(IUMConstants.ROLES_NB_STAFF)) {
				pendingStat = this.countPendingARs(userID, IUMConstants.ROLES_NB_STAFF);
	
			} else if (role.equalsIgnoreCase(IUMConstants.ROLES_NB_REVIEWER)) {
				pendingStat = this.countPendingARs(userID, IUMConstants.ROLES_NB_REVIEWER);
	
			} else if (role.equalsIgnoreCase(IUMConstants.ROLES_NB_SUPERVISOR)) {
				pendingStat = this.countPendingARs(userID, IUMConstants.ROLES_NB_SUPERVISOR);
	
			} else if (role.equalsIgnoreCase(IUMConstants.ROLES_NB_ADMIN)) {
				pendingStat = this.countPendingARs(userID, IUMConstants.ROLES_NB_ADMIN);
	
			} else if (role.equalsIgnoreCase(IUMConstants.ROLES_FACILITATOR)) {
				pendingStat = this.countPendingARs(userID, IUMConstants.ROLES_FACILITATOR);
	
			} else if (role.equalsIgnoreCase(IUMConstants.ROLES_FACILITATOR_SUPERVISOR)) {
				pendingStat = this.countPendingARs(userID, IUMConstants.ROLES_FACILITATOR_SUPERVISOR);
	
			} else if (role.equalsIgnoreCase(IUMConstants.ROLES_UNDERWRITER)) {
				pendingStat = this.countPendingARs(userID, IUMConstants.ROLES_UNDERWRITER);
	
			} else if (role.equalsIgnoreCase(IUMConstants.ROLES_UNDERWRITER_SUPERVISOR)) {
				pendingStat = this.countPendingARs(userID, IUMConstants.ROLES_UNDERWRITER_SUPERVISOR);
	
			}  else if (role.equalsIgnoreCase(IUMConstants.ROLES_MARKETING_STAFF)) {
				pendingStat = this.countPendingARs(userID, IUMConstants.ROLES_MARKETING_STAFF);
	
			} else if (role.equalsIgnoreCase(IUMConstants.ROLES_AGENTS)) {
				pendingStat = this.countPendingARs(userID, IUMConstants.ROLES_AGENTS);
	
			} else if (role.equalsIgnoreCase(IUMConstants.ROLES_MEDICAL_ADMIN)) {
				// None
		
			} else if (role.equalsIgnoreCase(IUMConstants.ROLES_MEDICAL_SUPERVISOR)) {
				// None
				
			} else if (role.equalsIgnoreCase(IUMConstants.ROLES_MEDICAL_CONSULTANT)) {
				// None
				
			} else if (role.equalsIgnoreCase(IUMConstants.ROLES_EXAMINER)) {
				// None
				
			} else if (role.equalsIgnoreCase(IUMConstants.ROLES_LAB_STAFF)) {
				// None
				
			} else if (role.equalsIgnoreCase(IUMConstants.ROLES_OTHER_USER)) {
				// None 
	
			} else if (role.equalsIgnoreCase(IUMConstants.ROLES_LOV_ADMIN)) {
				// None
	
			} else if (role.equalsIgnoreCase(IUMConstants.ROLES_SECURITY_ADMIN)) {
				// None
	
			} else if (role.equalsIgnoreCase(IUMConstants.ROLES_SYSTEM_OPERATOR)) {
				// None
	
			} else {
			
				throw new UnderWriterException("Invalid user role.");
			}
			LOGGER.info("getPendingStatAR end");
			return pendingStat;
		}
		catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMException(e);
		}
		catch (Exception ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		
	}
	
	private int countPendingARs(String userID, String userRole) throws IUMException {
		
		LOGGER.info("countPendingARs start");

		try {			
			
			AssessmentRequestDAO dao = new AssessmentRequestDAO();
			int count = 0;
			if (userRole.equalsIgnoreCase(IUMConstants.ROLES_MARKETING_STAFF)) {
				UserDAO userDAO = new UserDAO();
				String branchID = null;
				branchID = userDAO.selectUserProfile(userID).getOfficeCode();
				if (branchID != null) {
					count = dao.countARsPerBranch(branchID, IUMConstants.STATUS_AWAITING_REQUIREMENTS);
				}

			} else if (userRole.equalsIgnoreCase(IUMConstants.ROLES_AGENTS)) {
				count = dao.countARsPerAgent(userID, IUMConstants.STATUS_AWAITING_REQUIREMENTS);

			} else if (userRole.equalsIgnoreCase(IUMConstants.ROLES_MEDICAL_CONSULTANT)) {
				count = dao.countARsPerStatus(IUMConstants.STATUS_UNDERGOING_ASSESSMENT);
			} else {
				count = dao.countPendingARs(userID);
			}
			LOGGER.info("countPendingARs end");
			return count;
		}
		catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMException(e);
		}
		catch (Exception ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
				
	}
	
	private int countPendingMRs(String userID, String userRole) throws IUMException {
		
		LOGGER.info("countPendingMRs start");
		Connection conn = null; 
		try {			
			conn = new DataSourceProxy().getConnection();
			MedicalDAO dao = new MedicalDAO(conn);
			int count = 0;
			if (userRole.equalsIgnoreCase(IUMConstants.ROLES_MEDICAL_ADMIN) ||
				userRole.equalsIgnoreCase(IUMConstants.ROLES_MEDICAL_SUPERVISOR)) {
				count = dao.countPendingMRs(userID, IUMConstants.STATUS_REQUESTED);

			} else if (userRole.equalsIgnoreCase(IUMConstants.ROLES_EXAMINER)) {
				count = dao.countPendingExaminerMRs(userID, IUMConstants.STATUS_REQUESTED);

			} else if (userRole.equalsIgnoreCase(IUMConstants.ROLES_LAB_STAFF)) {
				count = dao.countPendingLabMRs(userID, IUMConstants.STATUS_REQUESTED);
			}
			return count;
		}
		catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMException(e);
		}
		catch (Exception ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		finally {
			closeConn(conn);
			LOGGER.info("countPendingMRs end");
		}		
	}
	
	private int countLockedUsers() throws IUMException {
		
		LOGGER.info("countLockedUsers start");
	 
		try {			
			
			UserDAO dao = new UserDAO();
			int count = 0;
			count = dao.countLockedusers();
			LOGGER.info("countLockedUsers end");
			return count;
		}
		catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMException(e);
		}
		catch (Exception ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
				
	}
	
	
	public int countPasswordResetRequired() throws IUMException {		
		
		LOGGER.info("countPasswordResetRequired start");
		
		try {			
			
			UserDAO dao = new UserDAO();
			int count = 0;
			count = dao.countLockedusers();
			LOGGER.info("countPasswordResetRequired end");
			return count;
		}
		catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMException(e);
		}
		catch (Exception ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
				
	}

	public ArrayList getAgentBranches() throws IUMException {
		
		LOGGER.info("getAgentBranches start");
		ArrayList listAgentBranch = null;
		
		try {
			
			UserDAO dao = new UserDAO();
			listAgentBranch = dao.getUserBranches(IUMConstants.USER_TYPE_AGENT);
		}
		catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMException(e);
		}
		catch (Exception ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		
		LOGGER.info("getAgentBranches end");
		return listAgentBranch;
		
	}
	
	private void closeConn(Connection conn) throws IUMException {
		
		LOGGER.info("closeConn start");
		try {
			if (conn != null){
				conn.close();
			}
		}
		catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMException(e);
		}
		LOGGER.info("closeConn end");
	}
		
}
