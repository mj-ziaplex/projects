/*
 * Created on Dec 22, 2003
 * package name = com.slocpi.ium.underwriter
 * file name    = CDS.java
 */
package com.slocpi.ium.underwriter;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.slocpi.ium.IUMException;
import com.slocpi.ium.data.CDSMortalityRatingData;
import com.slocpi.ium.data.CDSSummaryPolicyCoverageData;
import com.slocpi.ium.data.ClientDataSheetData;
import com.slocpi.ium.data.dao.ClientDataSheetDAO;
import com.slocpi.ium.data.util.DataSourceProxy;
import com.slocpi.ium.util.CodeHelper;


/**
 * @author Engel
 * The business object for the ClientDataSheet that contains the retrieval of client data sheet info.
 * 
 */
public class CDS {
	private static final Logger LOGGER = LoggerFactory.getLogger(CDS.class);
	/**
	 * Retrieves the client data sheet for this reference number and client number.
	 * @param referenceNumber
	 * @param clientNo
	 * @return ClientDataSheetData
	 * @throws UnderWriterException
	 * @throws IUMException
	 */
	public ClientDataSheetData getCDS(String referenceNumber, String clientNo) throws UnderWriterException, IUMException {
		
		LOGGER.info("getCDS start");
		
		try {
			
			ClientDataSheetDAO cdsDAO = new ClientDataSheetDAO();	  
			ClientDataSheetData cdsData = cdsDAO.retrieveCDS(referenceNumber, clientNo);			
			cdsData.setMortalityRating(getMortalityRating(referenceNumber, clientNo));
			cdsData.setPolicyCoverageDetailsList(getPolicyCoverageList(referenceNumber, clientNo));
			cdsData.setSummaryOfPolicies(getSummaryPolicy(referenceNumber, clientNo));
			
			LOGGER.info("getCDS end");
			return (cdsData);
		}
		catch (SQLException ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		catch (Exception ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}	
				
		
		
	}
	
	/**
	 * Retrieves the policy details for this reference number and client number
	 * @param referenceNumber
	 * @param clientNo
	 * @return ArrayList of PolicyDetailData
	 * @throws UnderWriterException
	 * @throws IUMException
	 */
	public ArrayList getPolicyCoverageList(
			final String referenceNumber,
			final String clientNo) throws UnderWriterException, IUMException {
		
		LOGGER.info("getPolicyCoverageList start");
		
		
		try {
			ClientDataSheetDAO cdsDAO = new ClientDataSheetDAO();	  
			ArrayList policyCovList = cdsDAO.retrievePolicyCoverageList(referenceNumber, clientNo);
			
			LOGGER.info("getPolicyCoverageList end");
			return (policyCovList);
		}
		catch (SQLException ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		catch (Exception ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
	}
	
	/**
	 * Retrieves mortality rating data for this reference number and client number.
	 * @param referenceNumber
	 * @param clientNo
	 * @return CDSMortalityRatingData
	 * @throws UnderWriterException
	 * @throws IUMException
	 */
	public CDSMortalityRatingData getMortalityRating(
			final String referenceNumber,
			final String clientNo) throws UnderWriterException, IUMException {
		
		LOGGER.info("getMortalityRating start");
		
		try {
			
			ClientDataSheetDAO cdsDAO = new ClientDataSheetDAO();	  
			CDSMortalityRatingData mrData = cdsDAO.retrieveMortalityRating(referenceNumber, clientNo);
			
			LOGGER.info("getMortalityRating end");
			return (mrData);
		}
		catch (SQLException ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		catch (Exception ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}	
	}	
		
	/**
	 * Retrieves the summary of policies for this reference number and client number.
	 * @param referenceNumber
	 * @param clientNo
	 * @return CDSSummaryPolicyCoverageData
	 * @throws UnderWriterException
	 * @throws IUMException
	 */
	public CDSSummaryPolicyCoverageData getSummaryPolicy(
			final String referenceNumber,
			final String clientNo) throws UnderWriterException, IUMException {
		
		LOGGER.info("getSummaryPolicy start");
		
		try {
			
			ClientDataSheetDAO cdsDAO = new ClientDataSheetDAO();	  
			CDSSummaryPolicyCoverageData summaryData = cdsDAO.retrieveSummaryPolicy(referenceNumber, clientNo);
			
			LOGGER.info("getSummaryPolicy end");
			return (summaryData);
		}
		catch (SQLException ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		catch (Exception ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
	}

}
