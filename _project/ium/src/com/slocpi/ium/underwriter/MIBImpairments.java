package com.slocpi.ium.underwriter;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.slocpi.ium.IUMException;
import com.slocpi.ium.data.dao.MIBImpairmentsDAO;
import com.slocpi.ium.data.util.DataSourceProxy;
import com.slocpi.ium.util.CodeHelper;

public class MIBImpairments {

	private static final Logger LOGGER = LoggerFactory.getLogger(MIBImpairments.class);

	public ArrayList getMIBImpairments(String code, String desc) throws UnderWriterException, IUMException {
		
		LOGGER.info("getMIBImpairments start");
		Connection conn = new DataSourceProxy().getConnection();
		MIBImpairmentsDAO dao = new MIBImpairmentsDAO(conn);

		try {
			ArrayList policyReqDataList = dao.retrieveMIBImpairments(code, desc);
			return (policyReqDataList);
		} catch (SQLException ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		} catch (Exception ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		} finally {
			closeConnection(conn);
			LOGGER.info("getMIBImpairments end");
		}
	}

	private void closeConnection(Connection conn) throws IUMException{
		
		
		if (null != conn){
			try {
				conn.close();
			} catch (SQLException sqlE) {
				LOGGER.error(CodeHelper.getStackTrace(sqlE));
				throw new IUMException(sqlE);
			}
		}
		
	}

}
