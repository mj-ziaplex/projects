/**
 * ActivityLog.java				Copyright (c) 2004 Pointwest Technologies Corp.
 * This is a business object that contains the business rules for the Activity logs.
 * 
 * @author aditalo	 @date Jan 6, 2004
 */
package com.slocpi.ium.underwriter;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.slocpi.ium.IUMException;
import com.slocpi.ium.data.PurgeStatisticData;
import com.slocpi.ium.data.UserProfileData;
import com.slocpi.ium.data.dao.ActivityDAO;
import com.slocpi.ium.data.dao.PurgeStatisticDAO;
import com.slocpi.ium.data.dao.UserDAO;
import com.slocpi.ium.data.util.DataSourceProxy;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.IUMConstants;


/**
 * @author tvicencio		Copyright (c) 2004 Pointwest Technologies Corp.
 * @date Jan 6, 2004
 */
public class ActivityLog {
	private static final Logger LOGGER = LoggerFactory.getLogger(ActivityLog.class);
	/**
	 * Retrieves the activity log for this reference number
	 * @param referenceNumber assessment request reference number
	 * @return ArrayList of ActivityLogData
	 */
	public ArrayList getActivities(String referenceNumber) throws UnderWriterException, IUMException {
		
		LOGGER.info("getActivities start");
		Connection conn = null;
		try {
			conn = new DataSourceProxy().getConnection();
			ArrayList actList = new ArrayList();
			ActivityDAO docDAO = new ActivityDAO(conn);
			actList = docDAO.retrieveActivityLogs(referenceNumber);
			return actList;
		}
		catch (SQLException ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		catch (Exception ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		finally{
			closeConn(conn);
			LOGGER.info("getActivities end");
		}
	}

	/**
	 * Deletes activity log within the range specified
	 * @param purgeDate
	 * @param startDate
	 * @param endDate
	 * @return number of deleted activity logs
	 * @throws IUMException
	 */
	public int purgeActivityLogs(Date purgeDate, Date startDate, Date endDate) throws IUMException {
		
		LOGGER.info("purgeActivityLogs start");
		int recordCount = 0;
		String result = "F";
		Connection conn = new DataSourceProxy().getConnection();
		ActivityDAO dao = new ActivityDAO(conn);
		try {
			conn.setAutoCommit(false);
			recordCount = dao.deleteActivityLogs(startDate, endDate);
			conn.commit();
			result = "S";
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			try {
				conn.rollback();
			} catch (SQLException e1) {
				LOGGER.error(CodeHelper.getStackTrace(e1));
			}
			throw new IUMException(e);
		}
		finally{
			PurgeStatisticData data = new PurgeStatisticData();
			data.setPurgeDate(purgeDate);
			data.setStartDate(startDate);
			data.setEndDate(endDate);
			data.setCriteria(IUMConstants.PURGE_ACTIVITY_LOGS);
			data.setNumberOfRecords(recordCount);
			data.setResult(result);

			PurgeStatisticDAO purgeDao = new PurgeStatisticDAO(conn);
			try {				
				purgeDao.insertPurgeStatistic(data);
				conn.commit();
			} catch (SQLException e) {
				LOGGER.error(CodeHelper.getStackTrace(e));
				try {
					conn.rollback();
				} catch (SQLException e1) {
					LOGGER.error(CodeHelper.getStackTrace(e1));
				}
				throw new IUMException(e);
			} finally {
				closeConn(conn);
				
			}
		}
		LOGGER.info("purgeActivityLogs end");
		return recordCount;
		
	}

	/**
	 * Retrieves UserProfileData for this user
	 * @param userId user to be retrieve
	 * @return UserProfileData
	 * @throws IUMException
	 */
	public UserProfileData getUserProfile(String userId) throws IUMException{
		
		LOGGER.info("getUserProfile start");
		
		UserProfileData userData = new UserProfileData();
		try {
			
			UserDAO dao = new UserDAO();
			userData = dao.selectUserProfile(userId);
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMException(e);
		}
		LOGGER.info("getUserProfile end");
		return userData;
	}

	
	private void closeConn(Connection conn)throws IUMException{
		
		LOGGER.info("closeConn start");
		try{
			if (conn != null){
				conn.close();
				return;
			}
		}
		catch (Exception ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		LOGGER.info("closeConn send");
	}
	
}
