/**
 * Document.java				Copyright (c) 2004 Pointwest Technologies Corp.
 * TODO DOCUMENT ME!
 * 
 * @author aditalo	 @date Jan 6, 2004
 */
package com.slocpi.ium.underwriter;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.slocpi.ium.IUMException;
import com.slocpi.ium.data.AccessData;
import com.slocpi.ium.data.AccessTemplateData;
import com.slocpi.ium.data.AutoAssignCriteriaData;
import com.slocpi.ium.data.AutoAssignmentData;
import com.slocpi.ium.data.ClientTypeData;
import com.slocpi.ium.data.DocumentTypeData;
import com.slocpi.ium.data.ExaminationAreaData;
import com.slocpi.ium.data.ExaminationPlaceData;
import com.slocpi.ium.data.ExaminerData;
import com.slocpi.ium.data.HolidayData;
import com.slocpi.ium.data.JobScheduleData;
import com.slocpi.ium.data.LOBData;
import com.slocpi.ium.data.LaboratoryData;
import com.slocpi.ium.data.LaboratoryTestData;
import com.slocpi.ium.data.MIBActionData;
import com.slocpi.ium.data.MIBImpairmentData;
import com.slocpi.ium.data.MIBLetterData;
import com.slocpi.ium.data.MIBNumberData;
import com.slocpi.ium.data.PageData;
import com.slocpi.ium.data.RankData;
import com.slocpi.ium.data.RequirementData;
import com.slocpi.ium.data.RequirementFormData;
import com.slocpi.ium.data.RolesData;
import com.slocpi.ium.data.SectionData;
import com.slocpi.ium.data.SpecializationData;
import com.slocpi.ium.data.StatusData;
import com.slocpi.ium.data.SunLifeDeptData;
import com.slocpi.ium.data.SunLifeOfficeData;
import com.slocpi.ium.data.TestProfileData;
import com.slocpi.ium.data.dao.AutoAssignmentDAO;
import com.slocpi.ium.data.dao.CodeValuePairDAO;
import com.slocpi.ium.data.dao.ExamPlaceDAO;
import com.slocpi.ium.data.dao.ExaminerDAO;
import com.slocpi.ium.data.dao.HolidayDAO;
import com.slocpi.ium.data.dao.JobScheduleDAO;
import com.slocpi.ium.data.dao.LaboratoryDAO;
import com.slocpi.ium.data.dao.RanksDAO;
import com.slocpi.ium.data.dao.ReferenceDAO;
import com.slocpi.ium.data.dao.RequirementDAO;
import com.slocpi.ium.data.dao.RequirementFormDAO;
import com.slocpi.ium.data.dao.SpecializationDAO;
import com.slocpi.ium.data.dao.TestProfileDAO;
import com.slocpi.ium.data.util.DataSourceProxy;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.IUMConstants;
import com.slocpi.ium.util.SortHelper;
import com.slocpi.ium.util.UserManager;

/**
 * TODO DOCUMENT ME!
 * 
 * @author tvicencio		Copyright (c) 2004 Pointwest Technologies Corp.
 * @date Jan 6, 2004
 */
public class Reference {
	private static final Logger LOGGER = LoggerFactory.getLogger(Reference.class);
	/**
	 * Returns the list of access codes.
	 * @return ArrayList
	 */	
	public ArrayList getAccessList() throws UnderWriterException, IUMException {
		
		LOGGER.info("getAccessList start");
		Connection conn = null;		
		try {
			
			ArrayList list = new ArrayList();
			conn = new DataSourceProxy().getConnection();
			CodeValuePairDAO dao = new CodeValuePairDAO(conn);
			list = dao.retrieveAccesses();
			
			return list;
		}
		catch (SQLException ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		catch (Exception ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		finally {
			closeConn(conn);
			LOGGER.info("getAccessList end");
		}
	}//getAccessList


	/**
	 * Returns the access data object.
	 * @return AccessData
	 */	
	public AccessData getAccessDetail(long code) throws UnderWriterException, IUMException {
		
		LOGGER.info("getAccessDetail start");
		Connection conn = null;		
		try {
			
			conn = new DataSourceProxy().getConnection();
			CodeValuePairDAO dao = new CodeValuePairDAO(conn);
			AccessData data = dao.retrieveAccessDetail(code);
			
			return data;
		}
		catch (SQLException ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		catch (Exception ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		finally {
			closeConn(conn);
			LOGGER.info("getAccessDetail end");
		}		
	}//getAccessDetail
	

	/**
	 * Creates a new access record.
	 * @param data
	 */
	public void createAccess(AccessData data) throws UnderWriterException, IUMException {
		
		LOGGER.info("createAccess start");
		Connection conn = null;		
		try {
			
			long code = data.getAccessId();	
			String desc = data.getAccessDesc();

			if (desc == null) {
				throw new UnderWriterException("Description is required.");
			}

			conn = new DataSourceProxy().getConnection();
			CodeValuePairDAO dao = new CodeValuePairDAO(conn);  
			
			// do not allow duplicate records to be entered
			if (dao.retrieveAccessDetail(code) != null) {
				LOGGER.warn("Duplicate reference code : " + code);
				throw new UnderWriterException("Duplicate reference code : " + code);
			}
			dao.insertAccess(data);
				
		}
		catch (UnderWriterException ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new UnderWriterException(ex);
		}
		catch (SQLException ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		catch (Exception ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		finally {
			closeConn(conn);
			LOGGER.info("createAccess end");
		}		
	}//createAccess


	/**
	 * Updates an access record.
	 * @param data
	 */
	public void editAccess(AccessData data) throws UnderWriterException, IUMException {
		
		LOGGER.info("editAccess start");
		Connection conn = null;
		try {
				
			conn = new DataSourceProxy().getConnection();
			CodeValuePairDAO dao = new CodeValuePairDAO(conn);  
			dao.updateAccess(data);
			
		}
		catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMException(e);
		}
		finally {
			closeConn(conn);
			LOGGER.info("editAccess end");
		}
	}//editAccess


	/**
	 * Returns the list of pages.
	 * @return ArrayList
	 */	
	public ArrayList getPageList() throws UnderWriterException, IUMException {
		
		LOGGER.info("getPageList start");
		Connection conn = null;		
		try {
			
			ArrayList list = new ArrayList();
			conn = new DataSourceProxy().getConnection();
			CodeValuePairDAO dao = new CodeValuePairDAO(conn);
			list = dao.retrievePages();
			
			return list;
		}
		catch (SQLException ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		catch (Exception ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		finally {
			closeConn(conn);
			LOGGER.info("getPageList end");
		}
	}//getPageList


	/**
	 * Returns the page data object.
	 * @return PageData
	 */	
	public PageData getPageDetail(long code) throws UnderWriterException, IUMException {
		
		LOGGER.info("getPageDetail start");
		Connection conn = null;		
		try {
			
			conn = new DataSourceProxy().getConnection();
			CodeValuePairDAO dao = new CodeValuePairDAO(conn);
			PageData data = dao.retrievePageDetail(code);
			
			return data;
		}
		catch (SQLException ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		catch (Exception ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		finally {
			closeConn(conn);
			LOGGER.info("getPageDetail end");
		}
	}//getPageDetail
	

	/**
	 * Creates a new page record.
	 * @param data
	 */
	public void createPage(PageData data) throws UnderWriterException, IUMException {
		
		LOGGER.info("createPage start");
		Connection conn = null;		
		try {
			
			String desc = data.getPageDesc();

			if (desc == null) {
				LOGGER.warn("Description is required.");
				throw new UnderWriterException("Description is required.");
			}

			conn = new DataSourceProxy().getConnection();
			CodeValuePairDAO dao = new CodeValuePairDAO(conn);  
			
			dao.insertPage(data);
					
		}
		catch (UnderWriterException ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new UnderWriterException(ex);
		}
		catch (SQLException ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		catch (Exception ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		finally {
			closeConn(conn);
			LOGGER.info("createPage end");
		}
	}//createPage


	/**
	 * Updates a page record.
	 * @param data
	 */
	public void editPage(PageData data) throws UnderWriterException, IUMException {
		
		LOGGER.info("editPage start");
		Connection conn = null;
		try {
			
			conn = new DataSourceProxy().getConnection();
			CodeValuePairDAO dao = new CodeValuePairDAO(conn);  
			dao.updatePage(data);
			
		}
		catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMException(e);
		}
		finally {
			closeConn(conn);
			LOGGER.info("editPage end");
		}
	}//editPage


	/**
	 * Returns the list of requirement forms.
	 * @return ArrayList
	 */	
	public ArrayList getRequirementForms(SortHelper sort) throws UnderWriterException, IUMException {
		
		LOGGER.info("getRequirementForms start");
		Connection conn = null;		
		try {
			
			ArrayList list = new ArrayList();
			conn = new DataSourceProxy().getConnection();
			RequirementFormDAO dao = new RequirementFormDAO(conn);
			list = dao.retrieveRequirementForms(sort);
			
			return list;
		}
		catch (SQLException ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		catch (Exception ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		finally {
			closeConn(conn);
			LOGGER.info("getRequirementForms end");
		}
	}//getRequirementForms


	/**
	 * Returns the requirement form data object.
	 * @return LOBData
	 */	
	public RequirementFormData getRequirementFormDetail(String code) throws UnderWriterException, IUMException {
		
		LOGGER.info("getRequirementFormDetail start");
		Connection conn  = null;		
		try {
			
			conn = new DataSourceProxy().getConnection();
			RequirementFormDAO dao = new RequirementFormDAO(conn);
			RequirementFormData data = dao.retrieveRequirementFormDetail(code);
			
			return data;
		}
		catch (SQLException ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		catch (Exception ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		finally {
			closeConn(conn);
			LOGGER.info("getRequirementFormDetail end");
		}
	}//getRequirementFormDetail
	

	/**
	 * Creates a requirement form record.
	 * @param data
	 */
	public void createRequirementForm(RequirementFormData data) throws UnderWriterException, IUMException {
		
		LOGGER.info("createRequirementForm start");
		Connection conn = null;		
		try {
			
			long id = data.getFormId();
			String formName = data.getFormName();
			String templateFormat = data.getTemplateFormat();
			String templateName = data.getTemplateName();
			boolean status = data.getStatus();	

			if (formName == null) {
				throw new UnderWriterException("Form name is required.");
			}

			if (templateFormat == null) {
				throw new UnderWriterException("Template format is required.");
			}

			if (templateName == null) {
				throw new UnderWriterException("Template name is required.");
			}

			conn = new DataSourceProxy().getConnection();
			RequirementFormDAO dao = new RequirementFormDAO(conn);  
			
			// do not allow duplicate records to be entered
			if (dao.retrieveRequirementFormDetail(formName) != null) {
				LOGGER.warn("Duplicate requirement form : " + formName);
				throw new UnderWriterException("Duplicate requirement form : " + formName);
			}
			
			dao.insertRequirementForm(data);
				
		}
		catch (UnderWriterException ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new UnderWriterException(ex);
		}
		catch (SQLException ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		catch (Exception ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		finally {
			closeConn(conn);
			LOGGER.info("createRequirementForm end");
		}
	}//createRequirementForm


	/**
	 * Updates a requirement form record.
	 * @param data
	 */
	public void editRequirementForm(RequirementFormData data) throws UnderWriterException, IUMException {
		
		LOGGER.info("editRequirementForm start");
		Connection conn = null;
		try {
			
			conn = new DataSourceProxy().getConnection();
			RequirementFormDAO dao = new RequirementFormDAO(conn);
			dao.updateRequirementForm(data);
			
		}
		catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMException(e);
		}
		finally {
			closeConn(conn);
			LOGGER.info("editRequirementForm end");
		}
	}//editRequirementForm


	/**
	 * Creates a requirement record.
	 * @param data
	 * @throws IUMException
	 */
	public void createRequirement(RequirementData data) throws IUMException {
		
		LOGGER.info("createRequirement start");
		
		try {
			RequirementDAO dao = new RequirementDAO();
			dao.insertRequirement(data);
		}
		catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMException(e.getMessage());
		}
		
		LOGGER.info("createRequirement end");
		
	}//createRequirement


	/**
	 * Updates a requirement record.
	 * @param data
	 * @throws IUMException
	 */
	public void editRequirement(RequirementData data) throws IUMException {
		
		LOGGER.info("editRequirement start");
		
		try {
			RequirementDAO dao = new RequirementDAO();
			dao.updateRequirement(data);
		}
		catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMException(e.getMessage());
		}
		
			LOGGER.info("editRequirement end");
		
	}//editRequirement


	/**
	 * Returns the list of LOB.
	 * @return ArrayList
	 */	
	public ArrayList getLOBList() throws UnderWriterException, IUMException {
		
		LOGGER.info("getLOBList start");
		Connection conn = null;		
		try {
			
			ArrayList list = new ArrayList();
			conn = new DataSourceProxy().getConnection();
			CodeValuePairDAO dao = new CodeValuePairDAO(conn);
			list = dao.retrieveLOB();
			
			return list;
		}
		catch (SQLException ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		catch (Exception ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		finally {
			closeConn(conn);
			LOGGER.info("getLOBList end");
		}
	}//getLOBList


	/**
	 * Returns the LOB data object.
	 * @return LOBData
	 */	
	public LOBData getLOBDetail(String code) throws UnderWriterException, IUMException {
		
		LOGGER.info("getLOBDetail start");
		Connection conn = null;		
		try {
			
			conn = new DataSourceProxy().getConnection();
			CodeValuePairDAO dao = new CodeValuePairDAO(conn);
			LOBData data = dao.retrieveLOBDetail(code);
			
			return data;
		}
		catch (SQLException ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		catch (Exception ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		finally {
			closeConn(conn);
			LOGGER.info("getLOBDetail end");
		}
	}//getLOBDetail
	

	/**
	 * Creates a new LOB record.
	 * @param data
	 */
	public void createLOB(LOBData data) throws UnderWriterException, IUMException {
		
		LOGGER.info("createLOB start");
		Connection conn = null;		
		try {
			
			String code = data.getLOBCode();	
			String desc = data.getLOBDesc();

			if (code == null) {
				throw new UnderWriterException("Code is required.");
			}

			if (desc == null) {
				throw new UnderWriterException("Description is required.");
			}

			conn = new DataSourceProxy().getConnection();
			CodeValuePairDAO dao = new CodeValuePairDAO(conn);  
			
			// do not allow duplicate records to be entered
			if (dao.retrieveLOBDetail(code) != null) {
				LOGGER.warn("Duplicate reference code : " + code);
				throw new UnderWriterException("Duplicate reference code : " + code);
			}
			
			dao.insertLOB(data);
						
		}
		catch (UnderWriterException ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new UnderWriterException(ex);
		}
		catch (SQLException ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		catch (Exception ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		finally {
			closeConn(conn);
			LOGGER.info("createLOB end");
		}
	}//createLOB


	/**
	 * Updates an LOB record.
	 * @param data
	 */
	public void editLOB(LOBData data) throws UnderWriterException, IUMException {
		
		LOGGER.info("editLOB start");
		Connection conn = null;
		try {
			
			conn = new DataSourceProxy().getConnection();
			CodeValuePairDAO dao = new CodeValuePairDAO(conn);  
			dao.updateLOB(data);
			
		}
		catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMException(e);
		}
		finally {
			closeConn(conn);
			LOGGER.info("editLOB end");
		}
	}//editLOB
		

	/**
	 * Returns the list of departments.
	 * @return ArrayList
	 */	
	public ArrayList getDepartments() throws UnderWriterException, IUMException {
		
		LOGGER.info("getDepartments start");
		Connection conn = null;		
		try {
			
			ArrayList list = new ArrayList();
			conn = new DataSourceProxy().getConnection();
			CodeValuePairDAO dao = new CodeValuePairDAO(conn);
			list = dao.retrieveDepartments();
			
			return list;
		}
		catch (SQLException ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		catch (Exception ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		finally {
			closeConn(conn);
			LOGGER.info("getDepartments end");
		}
	}//getDepartments


	/**
	 * Returns the department data object.
	 * @return SunLifeDeptData
	 */	
	public SunLifeDeptData getDepartmentDetail(String code) throws UnderWriterException, IUMException {
		
		LOGGER.info("getDepartmentDetail start");
		Connection conn = null;		
		try {
			
			conn = new DataSourceProxy().getConnection();
			CodeValuePairDAO dao = new CodeValuePairDAO(conn);
			SunLifeDeptData data = dao.retrieveDepartmentDetail(code);
			
			return data;
		}
		catch (SQLException ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		catch (Exception ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		finally {
			closeConn(conn);
			LOGGER.info("getDepartmentDetail end");
		}
	}//getDepartmentDetail
	

	/**
	 * Creates a new department record.
	 * @param data
	 */
	public void createDepartment(SunLifeDeptData data) throws UnderWriterException, IUMException {
		
		LOGGER.info("createDepartment start");
		Connection conn = null;
		try {
			
			String code = data.getDeptId();	
			String desc = data.getDeptDesc();

			if (code == null) {
				throw new UnderWriterException("Code is required.");
			}

			if (desc == null) {
				throw new UnderWriterException("Description is required.");
			}

			conn = new DataSourceProxy().getConnection();
			CodeValuePairDAO dao = new CodeValuePairDAO(conn);  
			
			// do not allow duplicate records to be entered
			if (dao.retrieveDepartmentDetail(code) != null) {
				LOGGER.warn("Duplicate reference code : " + code);
				throw new UnderWriterException("Duplicate reference code : " + code);
			}	
			
			dao.insertDepartment(data);
				
		}
		catch (UnderWriterException ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new UnderWriterException(ex);
		}
		catch (SQLException ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		catch (Exception ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		finally {
			closeConn(conn);
			LOGGER.info("createDepartment end");
		}
	}//createDepartment


	/**
	 * Updates a department record.
	 * @param data
	 */
	public void editDepartment(SunLifeDeptData data) throws UnderWriterException, IUMException {
		
		LOGGER.info("editDepartment start");
		Connection conn = null;
		try {
			
			conn = new DataSourceProxy().getConnection();
			CodeValuePairDAO dao = new CodeValuePairDAO(conn);  
			dao.updateDepartment(data);
				
		}
		catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMException(e);
		}
		finally {
			closeConn(conn);
			LOGGER.info("editDepartment end");
		}
	}//editDepartment
		

	/**
	 * Returns the list of sections.
	 * @return ArrayList
	 */	
	public ArrayList getSections() throws UnderWriterException, IUMException {
		
		LOGGER.info("getSections start");
		Connection conn = null;
		try {
			
			ArrayList list = new ArrayList();
			conn = new DataSourceProxy().getConnection();
			CodeValuePairDAO dao = new CodeValuePairDAO(conn);
			list = dao.retrieveSections();
			
			return list;
		}
		catch (SQLException ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		catch (Exception ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		finally {
			closeConn(conn);
			LOGGER.info("getSections end");
		}
	}//getSections


	/**
	 * Returns the section data object.
	 * @return SectionData
	 */	
	public SectionData getSectionDetail(String code) throws UnderWriterException, IUMException {
		
		LOGGER.info("getSectionDetail start");
		Connection conn = null;
		try {
			
			conn = new DataSourceProxy().getConnection();
			CodeValuePairDAO dao = new CodeValuePairDAO(conn);
			SectionData data = dao.retrieveSectionDetail(code);
			
			return data;
		}
		catch (SQLException ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		catch (Exception ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		finally {
			closeConn(conn);
			LOGGER.info("getSectionDetail end");
		}
	}//getSectionDetail
	

	/**
	 * Creates a new section record.
	 * @param data
	 */
	public void createSection(SectionData data) throws UnderWriterException, IUMException {
		
		LOGGER.info("createSection start");
		Connection conn = null;
		try {
			
			String code = data.getSectionId();	
			String desc = data.getSectionDesc();

			if (code == null) {
				throw new UnderWriterException("Code is required.");
			}

			if (desc == null) {
				throw new UnderWriterException("Description is required.");
			}

			conn = new DataSourceProxy().getConnection();
			CodeValuePairDAO dao = new CodeValuePairDAO(conn);  
			
			// do not allow duplicate records to be entered
			if (dao.retrieveSectionDetail(code) != null) {
				LOGGER.warn("Duplicate reference code : " + code);
				throw new UnderWriterException("Duplicate reference code : " + code);
			}	
			
			dao.insertSection(data);
					
		}
		catch (UnderWriterException ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new UnderWriterException(ex);
		}
		catch (SQLException ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		catch (Exception ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		finally {
			closeConn(conn);
			LOGGER.info("createSection end");
		}
	}//createSection


	/**
	 * Updates a section record.
	 * @param data
	 */
	public void editSection(SectionData data) throws UnderWriterException, IUMException {
		
		LOGGER.info("editSection start");
		Connection conn = null;
		try {
			
			conn = new DataSourceProxy().getConnection();
			CodeValuePairDAO dao = new CodeValuePairDAO(conn);  
			dao.updateSection(data);
				
		}
		catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMException(e);
		}
		finally {
			closeConn(conn);
			LOGGER.info("editSection end");
		}
	}//editSection
			

	/**
	 * Returns the list of examiner specializations.
	 * @return ArrayList
	 */	
	public ArrayList getExamSpecializations() throws UnderWriterException, IUMException {
		
		LOGGER.info("getExamSpecializations start");
		Connection conn = null;
		try {
			
			ArrayList list = new ArrayList();
			conn = new DataSourceProxy().getConnection();
			CodeValuePairDAO dao = new CodeValuePairDAO(conn);
			list = dao.retrieveExamSpecializations();
			
			return list;
		}
		catch (SQLException ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		catch (Exception ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		finally {
			closeConn(conn);
			LOGGER.info("getExamSpecializations end");
		}
	}//getExamSpecializations


	/**
	 * Returns the specialization data object.
	 * @return SpecializationData
	 */	
	public SpecializationData getExamSpecializationDetail(long code) throws UnderWriterException, IUMException {
		
		LOGGER.info("getExamSpecializationDetail start");
		Connection conn = null;
		try {
			
			conn = new DataSourceProxy().getConnection();
			CodeValuePairDAO dao = new CodeValuePairDAO(conn);
			SpecializationData data = dao.retrieveExamSpecializationDetail(code);
			
			return data;
		}
		catch (SQLException ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		catch (Exception ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		finally {
			closeConn(conn);
			LOGGER.info("getExamSpecializationDetail end");
		}
	}//getExamSpecializationDetail
	

	/**
	 * Creates a new examiner specialization record.
	 * @param data
	 */
	public void createExamSpecialization(SpecializationData data) throws UnderWriterException, IUMException {
		
		LOGGER.info("createExamSpecialization start");
		Connection conn = null;
		try {
			
			long code = data.getSpecializationId();
			String desc = data.getSpecializationDesc();

			if (desc == null) {
				throw new UnderWriterException("Description is required.");
			}

			conn = new DataSourceProxy().getConnection();
			CodeValuePairDAO dao = new CodeValuePairDAO(conn);  

			// do not allow duplicate records to be entered
			if (dao.retrieveExamSpecializationDetail(code) != null) {
				LOGGER.warn("Duplicate reference code : " + code);
				throw new UnderWriterException("Duplicate reference code : " + code);
			}
			
			dao.insertExamSpecialization(data);
					
		}
		catch (UnderWriterException ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new UnderWriterException(ex);
		}
		catch (SQLException ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		catch (Exception ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		finally {
			closeConn(conn);
			LOGGER.info("createExamSpecialization end");
		}
	}//createExamSpecialization


	/**
	 * Updates an examiner specialization record.
	 * @param data
	 */
	public void editExamSpecialization(SpecializationData data) throws UnderWriterException, IUMException {
		
		LOGGER.info("editExamSpecialization start");
		Connection conn = null;
		try {
				
			conn = new DataSourceProxy().getConnection();
			CodeValuePairDAO dao = new CodeValuePairDAO(conn);  
			dao.updateExamSpecialization(data);
				
		}
		catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMException(e);
		}
		finally {
			closeConn(conn);
			LOGGER.info("editExamSpecialization end");
		}
	}//editExamSpecialization
	
	
	/**
	 * Returns the list of examination places.
	 * @return ArrayList
	 */	
	public ArrayList getExamPlaces() throws UnderWriterException, IUMException {
		
		LOGGER.info("getExamPlaces start");
		Connection conn = null;
		try {
			
			ArrayList list = new ArrayList();
			conn = new DataSourceProxy().getConnection();
			ExamPlaceDAO eDao = new ExamPlaceDAO(conn);
			list = eDao.selectExamPlaces();
			
			return list;
		}
		catch (SQLException ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		catch (Exception ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		finally {
			closeConn(conn);
			LOGGER.info("getExamPlaces end");
		}
	}//getExamPlaces


	/**
	 * Returns the examination place data object.
	 * @return ExaminationPlaceData
	 */	
	public ExaminationPlaceData getExamPlaceDetail(long code) throws UnderWriterException, IUMException {
		
		LOGGER.info("getExamPlaceDetail start");
		Connection conn = null;
		try {
			
			conn = new DataSourceProxy().getConnection();
			CodeValuePairDAO dao = new CodeValuePairDAO(conn);
			ExaminationPlaceData data = dao.retrieveExamPlaceDetail(code);
			
			return data;
		}
		catch (SQLException ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		catch (Exception ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		finally {
			closeConn(conn);
			LOGGER.info("getExamPlaceDetail end");
		}
	}//getExamPlaceDetail
	

	/**
	 * Creates a new examination place record.
	 * @param data
	 */
	public void createExamPlace(ExaminationPlaceData data) throws UnderWriterException, IUMException {
		
		LOGGER.info("createExamPlace start");
		Connection conn = null;
		try {
			
			long code = data.getExaminationPlaceId();
			String desc = data.getExaminationPlaceDesc();

			if (desc == null) {
				throw new UnderWriterException("Description is required.");
			}

			conn = new DataSourceProxy().getConnection();
			CodeValuePairDAO dao = new CodeValuePairDAO(conn);  

			// do not allow duplicate records to be entered
			if (dao.retrieveExamPlaceDetail(code) != null) {
				LOGGER.warn("Duplicate reference code : " + code);
				throw new UnderWriterException("Duplicate reference code : " + code);
			}
			
			dao.insertExamPlace(data);
						
		}
		catch (UnderWriterException ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new UnderWriterException(ex);
		}
		catch (SQLException ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		catch (Exception ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		finally {
			closeConn(conn);
			LOGGER.info("createExamPlace end");
		}
	}//createExamPlace


	/**
	 * Updates an examination place record.
	 * @param data
	 */
	public void editExamPlace(ExaminationPlaceData data) throws UnderWriterException, IUMException {
		
		LOGGER.info("editExamPlace start");
		Connection conn = null;
		try {
				
			conn = new DataSourceProxy().getConnection();
			CodeValuePairDAO dao = new CodeValuePairDAO(conn);  
			dao.updateExamPlace(data);
			
		}
		catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMException(e);
		}
		finally {
			closeConn(conn);
			LOGGER.info("editExamPlace end");
		}
	}//editExamPlace
		
	
	/**
	 * Returns the list of examination areas.
	 * @return ArrayList
	 */	
	public ArrayList getExamAreas() throws UnderWriterException, IUMException {
		
		LOGGER.info("getExamAreas start");
		Connection conn  = null;
		try {
			
			ArrayList list = new ArrayList();
			conn = new DataSourceProxy().getConnection();
			CodeValuePairDAO dao = new CodeValuePairDAO(conn);
			list = dao.retrieveExamAreas();
			
			return list;
		}
		catch (SQLException ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		catch (Exception ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		finally {
			closeConn(conn);
			LOGGER.info("getExamAreas end");
		}
	}//getExamAreas


	/**
	 * Returns the examination area data object.
	 * @return ExaminationAreaData
	 */	
	public ExaminationAreaData getExamAreaDetail(long code) throws UnderWriterException, IUMException {
		
		LOGGER.info("getExamAreaDetail start");
		Connection conn = null;
		try {
			
			conn = new DataSourceProxy().getConnection();
			CodeValuePairDAO dao = new CodeValuePairDAO(conn);
			ExaminationAreaData data = dao.retrieveExamAreaDetail(code);
			
			return data;
		}
		catch (SQLException ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		catch (Exception ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		finally {
			closeConn(conn);
			LOGGER.info("getExamAreaDetail end");
		}
	}//getExamAreaDetail
	

	/**
	 * Creates a new examination area record.
	 * @param data
	 */
	public void createExamArea(ExaminationAreaData data) throws UnderWriterException, IUMException {
		
		LOGGER.info("createExamArea start");
		Connection conn = null;
		try {
			
			long code = data.getExaminationAreaId();
			String desc = data.getExaminationAreaDesc();

			if (desc == null) {
				throw new UnderWriterException("Description is required.");
			}

			conn = new DataSourceProxy().getConnection();
			CodeValuePairDAO dao = new CodeValuePairDAO(conn);  

			// do not allow duplicate records to be entered
			if (dao.retrieveExamAreaDetail(code) != null) {
				LOGGER.warn("Duplicate reference code : " + code);
				throw new UnderWriterException("Duplicate reference code : " + code);
			}
			
			dao.insertExamArea(data);
					
		}
		catch (UnderWriterException ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new UnderWriterException(ex);
		}
		catch (SQLException ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		catch (Exception ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		finally {
			closeConn(conn);
			LOGGER.info("createExamArea end");
		}
	}//createExamArea


	/**
	 * Updates an examination place record.
	 * @param data
	 */
	public void editExamArea(ExaminationAreaData data) throws UnderWriterException, IUMException {
		
		LOGGER.info("editExamArea start");
		Connection conn = null;
		try {
			
			conn = new DataSourceProxy().getConnection();
			CodeValuePairDAO dao = new CodeValuePairDAO(conn);  
			dao.updateExamArea(data);
			
		}
		catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMException(e);
		}
		finally {
			closeConn(conn);
			LOGGER.info("editExamArea end");
		}
	}//editExamArea
		

	/**
	 * Returns the list of document types.
	 * @return ArrayList
	 */	
	public ArrayList getDocumentTypes() throws UnderWriterException, IUMException {
		
		LOGGER.info("getDocumentTypes start");
		Connection conn = null;
		try {
		
			ArrayList list = new ArrayList();
			 conn = new DataSourceProxy().getConnection();
			CodeValuePairDAO dao = new CodeValuePairDAO(conn);
			list = dao.retrieveDocumentTypes();

			return list;
		}
		catch (SQLException ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		catch (Exception ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		finally {
			closeConn(conn);
			LOGGER.info("getDocumentTypes end");
		}
	}//getDocumentTypes


	/**
	 * Returns the document type data object.
	 * @return DocumentTypeData
	 */	
	public DocumentTypeData getDocumentTypeDetail(String code) throws UnderWriterException, IUMException {
		
		LOGGER.info("getDocumentTypeDetail start");
		Connection conn = null;		
		try {
			
			conn = new DataSourceProxy().getConnection();
			CodeValuePairDAO dao = new CodeValuePairDAO(conn);
			DocumentTypeData data = dao.retrieveDocumentTypeDetail(code);
			
			return data;
		}
		catch (SQLException ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		catch (Exception ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		finally {
			closeConn(conn);
			LOGGER.info("getDocumentTypeDetail end");
		}
	}//getDocumentTypeDetail
	

	/**
	 * Creates a new document type record.
	 * @param data
	 */
	public void createDocumentType(DocumentTypeData data) throws UnderWriterException, IUMException {
		
		LOGGER.info("createDocumentType start");
		Connection conn = null;		
		try {
			
			String code = data.getDocCode();	
			String desc = data.getDocDesc();

			if (code == null) {
				throw new UnderWriterException("Code is required.");
			}

			if (desc == null) {
				throw new UnderWriterException("Description is required.");
			}

			conn = new DataSourceProxy().getConnection();
			CodeValuePairDAO dao = new CodeValuePairDAO(conn);  
			
			// do not allow duplicate records to be entered
			if (dao.retrieveDocumentTypeDetail(code) != null) {
				LOGGER.warn("Duplicate reference code : " + code);
				throw new UnderWriterException("Duplicate reference code : " + code);
			}	
			
			dao.insertDocumentType(data);
			
		}
		catch (UnderWriterException ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new UnderWriterException(ex);
		}
		catch (SQLException ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		catch (Exception ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		finally {
			closeConn(conn);
			LOGGER.info("createDocumentType end");
		}
	}//createDocumentType


	/**
	 * Updates a document type record.
	 * @param data
	 */
	public void editDocumentType(DocumentTypeData data) throws UnderWriterException, IUMException {
		
		LOGGER.info("editDocumentType start");
		Connection conn = null;
		try {
			
			conn = new DataSourceProxy().getConnection();
			CodeValuePairDAO dao = new CodeValuePairDAO(conn);  
			dao.updateDocumentType(data);
				
		}
		catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMException(e);
		}
		finally {
			closeConn(conn);
			LOGGER.info("editDocumentType end");
		}
	}//editDocumentType
		

	/**
	 * Returns the list of client types.
	 * @return ArrayList
	 */	
	public ArrayList getClientTypes() throws UnderWriterException, IUMException {
		
		LOGGER.info("getClientTypes start");
		Connection conn = null;		
		try {
			
			ArrayList list = new ArrayList();
			conn = new DataSourceProxy().getConnection();
			CodeValuePairDAO dao = new CodeValuePairDAO(conn);
			list = dao.retrieveClientTypes();
			
			return list;
		}
		catch (SQLException ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		catch (Exception ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		finally {
			closeConn(conn);
			LOGGER.info("getClientTypes end");
		}
	}//getClientTypes


	/**
	 * Returns the client type data object.
	 * @return ClientTypeData
	 */	
	public ClientTypeData getClientTypeDetail(String code) throws UnderWriterException, IUMException {
		
		LOGGER.info("getClientTypeDetail start");
		Connection conn = null;		
		try {
			
			conn = new DataSourceProxy().getConnection();
			CodeValuePairDAO dao = new CodeValuePairDAO(conn);
			ClientTypeData data = dao.retrieveClientTypeDetail(code);
			
			return data;
		}
		catch (SQLException ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		catch (Exception ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		finally {
			closeConn(conn);
			LOGGER.info("getClientTypeDetail end");
		}
	}//getClientTypeDetail
	

	/**
	 * Creates a new client type record.
	 * @param data
	 */
	public void createClientType(ClientTypeData data) throws UnderWriterException, IUMException {
		
		LOGGER.info("createClientType start");
		Connection conn = null;		
		try {
			
			String code = data.getClientTypeCode();	
			String desc = data.getClientTypeDesc();

			if (code == null) {
				throw new UnderWriterException("Code is required.");
			}

			if (desc == null) {
				throw new UnderWriterException("Description is required.");
			}

			conn = new DataSourceProxy().getConnection();
			CodeValuePairDAO dao = new CodeValuePairDAO(conn);  
			
			// do not allow duplicate records to be entered
			if (dao.retrieveClientTypeDetail(code) != null) {
				LOGGER.warn("Duplicate reference code : " + code);
				throw new UnderWriterException("Duplicate reference code : " + code);
			}	
			
			dao.insertClientType(data);
			
		}
		catch (UnderWriterException ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new UnderWriterException(ex);
		}
		catch (SQLException ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		catch (Exception ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		finally {
			closeConn(conn);
			LOGGER.info("createClientType end");
		}
	}//createClientType
	

	/**
	 * Updates a client type record.
	 * @param data
	 */
	public void editClientType(ClientTypeData data) throws UnderWriterException, IUMException {
		
		LOGGER.info("editClientType start");
		Connection conn = null;
		try {
			
			conn = new DataSourceProxy().getConnection();
			CodeValuePairDAO dao = new CodeValuePairDAO(conn);  
			dao.updateClientType(data);
			
		}
		catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMException(e);
		}
		finally {
			closeConn(conn);
			LOGGER.info("editClientType end");
		}
	}//editClientType
	
	
	/**
	 * Returns the list of auto-assignment criteria.
	 * @return ArrayList
	 */	
	public ArrayList getAutoAssignCriteria() throws UnderWriterException, IUMException {
		
		LOGGER.info("getAutoAssignCriteria start");
		Connection conn = null;		
		try {
			
			ArrayList list = new ArrayList();
			conn = new DataSourceProxy().getConnection();
			CodeValuePairDAO dao = new CodeValuePairDAO(conn);
			list = dao.retrieveAutoAssignCriteria();
			
			return list;
		}
		catch (SQLException ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		catch (Exception ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		finally {
			closeConn(conn);
			LOGGER.info("getAutoAssignCriteria end");
		}
	}//getAutoAssignCriteria


	/**
	 * Returns the auto-assignment criteria data object.
	 * @return AutoAssignCriteriaData
	 */	
	public AutoAssignCriteriaData getAutoAssignCriteriaDetail(String code) throws UnderWriterException, IUMException {
		
		LOGGER.info("getAutoAssignCriteriaDetail start");
		Connection conn = null;
		try {
			
			conn = new DataSourceProxy().getConnection();
			CodeValuePairDAO dao = new CodeValuePairDAO(conn);
			AutoAssignCriteriaData data = dao.retrieveAutoAssignCriteriaDetail(code);
			
			return data;
		}
		catch (SQLException ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		catch (Exception ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		finally {
			closeConn(conn);
			LOGGER.info("getAutoAssignCriteriaDetail end");
		}
	}//getAutoAssignCriteriaDetail
	

	/**
	 * Creates a new auto-assignement criteria record.
	 * @param data
	 */
	public void createAutoAssignCriteria(AutoAssignCriteriaData data) throws UnderWriterException, IUMException {
		
		LOGGER.info("createAutoAssignCriteria start");
		Connection conn = null;
		try {
			
			String code = data.getFieldCode();	
			String desc = data.getFieldDesc();

			if (code == null) {
				throw new UnderWriterException("Code is required.");
			}

			if (desc == null) {
				throw new UnderWriterException("Description is required.");
			}

			conn = new DataSourceProxy().getConnection();
			CodeValuePairDAO dao = new CodeValuePairDAO(conn);  
			
			// do not allow duplicate records to be entered
			if (dao.retrieveClientTypeDetail(code) != null) {
				LOGGER.warn("Duplicate reference code : " + code);
				throw new UnderWriterException("Duplicate reference code : " + code);
			}	
			
			dao.insertAutoAssignCriteria(data);
				
		}
		catch (UnderWriterException ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new UnderWriterException(ex);
		}
		catch (SQLException ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		catch (Exception ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		finally {
			closeConn(conn);
			LOGGER.info("createAutoAssignCriteria end");
		}
	}//createAutoAssignCriteria


	/**
	 * Updates an auto-assignment criteria record.
	 * @param data
	 */
	public void editAutoAssignCriteria(AutoAssignCriteriaData data) throws UnderWriterException, IUMException {
		
		LOGGER.info("editAutoAssignCriteria start");
		Connection conn = null;
		try {
			
			conn = new DataSourceProxy().getConnection();
			CodeValuePairDAO dao = new CodeValuePairDAO(conn);  
			dao.updateAutoAssignCriteria(data);
			
		}
		catch (SQLException e) {
			throw new IUMException(e);
		}
		finally {
			closeConn(conn);
			LOGGER.info("editAutoAssignCriteria end");
		}
	}//editAutoAssignCriteria
	

	/**
	 * Returns the list of status codes.
	 * @return ArrayList
	 */	
	public ArrayList getStatusCodes() throws UnderWriterException, IUMException {
		
		LOGGER.info("getStatusCodes start");
		Connection conn = null;
		try {
			
			ArrayList list = new ArrayList();
			conn = new DataSourceProxy().getConnection();
			CodeValuePairDAO dao = new CodeValuePairDAO(conn);
			list = dao.retrieveStatusCodes();
			
			return list;
		}
		catch (SQLException ex) {
			throw new IUMException(ex);
		}
		catch (Exception ex) {
			throw new IUMException(ex);
		}
		finally {
			closeConn(conn);
			LOGGER.info("getStatusCodes end");
		}
	}//getStatusCodes


	/**
	 * Returns the status data object.
	 * @return StatusData
	 */	
	public StatusData getStatusCodeDetail(long code) throws UnderWriterException, IUMException {
		
		LOGGER.info("getStatusCodeDetail start");
		Connection conn = null;
		try {
			
			conn = new DataSourceProxy().getConnection();
			CodeValuePairDAO dao = new CodeValuePairDAO(conn);
			StatusData data = dao.retrieveStatusCodeDetail(code);
			
			return data;
		}
		catch (SQLException ex) {
			throw new IUMException(ex);
		}
		catch (Exception ex) {
			throw new IUMException(ex);
		}
		finally {
			closeConn(conn);
			LOGGER.info("getStatusCodeDetail end");
		}
	}//getStatusCodeDetail
	

	/**
	 * Creates a new status code record.
	 * @param data
	 */
	public void createStatusCode(StatusData data) throws UnderWriterException, IUMException {
		
		LOGGER.info("createStatusCode start");
		Connection conn = null;
		try {
			
			long code = data.getStatusId();
			String desc = data.getStatusDesc();
			String type = data.getStatusType();

			if (desc == null) {
				throw new UnderWriterException("Description is required.");
			}

			if (type == null) {
				throw new UnderWriterException("Type is required.");
			}
			
			conn = new DataSourceProxy().getConnection();
			CodeValuePairDAO dao = new CodeValuePairDAO(conn);  

			// do not allow duplicate records to be entered
			if (dao.retrieveStatusCodeDetail(code) != null) {
				LOGGER.warn("Duplicate reference code : " + code);
				throw new UnderWriterException("Duplicate reference code : " + code);
			}
						
			dao.insertStatusCode(data);
						
		}
		catch (UnderWriterException ex) {
			throw new UnderWriterException(ex);
		}
		catch (SQLException ex) {
			throw new IUMException(ex);
		}
		catch (Exception ex) {
			throw new IUMException(ex);
		}
		finally {
			closeConn(conn);
			LOGGER.info("createStatusCode end");
		}
	}//createStatusCode


	/**
	 * Updates a status record.
	 * @param data
	 */
	public void editStatusCode(StatusData data) throws UnderWriterException, IUMException {
		
		LOGGER.info("editStatusCode start");
		Connection conn = null;
		try {
			
			conn = new DataSourceProxy().getConnection();
			CodeValuePairDAO dao = new CodeValuePairDAO(conn);  
			dao.updateStatusCode(data);
			
		}
		catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMException(e);
		}
		finally {
			closeConn(conn);
			LOGGER.info("editStatusCode end");
		}
	}//editStatusCode
		

	/**
	 * Returns the list of roles.
	 * @return ArrayList
	 */	
	public ArrayList getRoles() throws UnderWriterException, IUMException {
		
		LOGGER.info("getRoles start");
		Connection conn = null;
		try {
			
			ArrayList list = new ArrayList();
			conn = new DataSourceProxy().getConnection();
			CodeValuePairDAO dao = new CodeValuePairDAO(conn);
			list = dao.retrieveRoles();
			
			return list;
		}
		catch (SQLException ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		catch (Exception ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		finally {
			closeConn(conn);
			LOGGER.info("getRoles end");
		}
	}//getRoles


	/**
	 * Returns the roles data object.
	 * @return RolesData
	 */	
	public RolesData getRoleDetail(String code) throws UnderWriterException, IUMException {
		
		LOGGER.info("getRoleDetail start");
		Connection conn = null;
		try {
			
			conn = new DataSourceProxy().getConnection();
			CodeValuePairDAO dao = new CodeValuePairDAO(conn);
			RolesData data = dao.retrieveRoleDetail(code);
			
			return data;
		}
		catch (SQLException ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		catch (Exception ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		finally {
			closeConn(conn);
			LOGGER.info("getRoleDetail end");
		}
	}//getRoleDetail
	

	/**
	 * Creates a new role record.
	 * @param data
	 */
	public void createRole(RolesData data) throws UnderWriterException, IUMException {
		
		LOGGER.info("createRole start");
		Connection conn = null;
		try {
			
			String code = data.getRolesId();			
			String desc = data.getRolesDesc();

			if (code == null) {
				throw new UnderWriterException("Code is required.");
			}
			
			if (desc == null) {
				throw new UnderWriterException("Description is required.");
			}

			conn = new DataSourceProxy().getConnection();
			CodeValuePairDAO dao = new CodeValuePairDAO(conn);  

			
			if (dao.retrieveRoleDetail(code) != null) {
				LOGGER.warn("Duplicate reference code : " + code);
				throw new UnderWriterException("Duplicate reference code : " + code);
			}
			
			dao.insertRole(data);
			
			
			RolesData role = new RolesData();
			role.setRolesId(data.getRolesId());
						
			AccessTemplateData accessTemplateData = new AccessTemplateData();
			accessTemplateData.setRole(role);
					
			UserManager userManager = new UserManager();
			userManager.createAccessTemplate(accessTemplateData);

						
		}
		catch (UnderWriterException ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new UnderWriterException(ex);
		}
		catch (SQLException ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		catch (Exception ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		finally {
			closeConn(conn);
			LOGGER.info("createRole end");
		}
	}//createRole


	/**
	 * Updates a role record.
	 * @param data
	 */
	public void editRole(RolesData data) throws UnderWriterException, IUMException {
		
		LOGGER.info("editRole start");
		Connection conn = null;
		try {
			
			conn = new DataSourceProxy().getConnection();
			CodeValuePairDAO dao = new CodeValuePairDAO(conn);  
			dao.updateRole(data);
			
		}
		catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMException(e);
		}
		finally {
			closeConn(conn);
			LOGGER.info("editRole end");
		}
	}//editRole


	/**
	 * Returns the list of access templates.
	 * @return ArrayList
	 */	
	public ArrayList getAccessTemplates() throws UnderWriterException, IUMException {
		
		LOGGER.info("getAccessTemplates start");
		Connection conn = null;		
		try {
			
			ArrayList list = new ArrayList();
			conn = new DataSourceProxy().getConnection();
			CodeValuePairDAO dao = new CodeValuePairDAO(conn);
			list = dao.retrieveAccessTemplates();
			
			return list;
		}
		catch (SQLException ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		catch (Exception ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		finally {
			closeConn(conn);
			LOGGER.info("getAccessTemplates end");
		}
	}//getAccessTemplates


	/**
	 * Returns the status data object.
	 * @return AccessTemplateData
	 */	
	public AccessTemplateData getAccessTemplateDetail(long code) throws UnderWriterException, IUMException {
		
		LOGGER.info("getAccessTemplateDetail start");
		Connection conn = null;
		try {
			
			conn = new DataSourceProxy().getConnection();
			CodeValuePairDAO dao = new CodeValuePairDAO(conn);
			AccessTemplateData data = dao.retrieveAccessTemplate(code);
			
			return data;
		}
		catch (SQLException ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		catch (Exception ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		finally {
			closeConn(conn);
			LOGGER.info("getAccessTemplateDetail end");
		}
	}//getAccessTemplateDetail


	/**
	 * Updates an access template record.
	 * @param data
	 */
	public void editAccessTemplate(AccessTemplateData data) throws UnderWriterException, IUMException {
		
		LOGGER.info("editAccessTemplate start");
		Connection conn = null;
		try {
				
			conn = new DataSourceProxy().getConnection();
			CodeValuePairDAO dao = new CodeValuePairDAO(conn);  
			dao.updateAccessTemplate(data);
			
		}
		catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMException(e);
		}
		finally {
			closeConn(conn);
			LOGGER.info("editAccessTemplate end");
		}
	}//editAccessTemplate
		

	/**
	 * Returns the list of test profiles.
	 * @return ArrayList
	 */	
	public ArrayList getTestProfiles() throws UnderWriterException, IUMException {
		
		LOGGER.info("getTestProfiles start");
		Connection conn = null;
		try {
			
			ArrayList list = new ArrayList();
			conn = new DataSourceProxy().getConnection();
			TestProfileDAO dao = new TestProfileDAO(conn);
			list = dao.selectTestProfiles();
			
			return list;
		}
		catch (SQLException ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		catch (Exception ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		finally {
			closeConn(conn);
			LOGGER.info("getTestProfiles end");
		}
	}//getTestProfiles


	/**
	 * Returns the test profile data object.
	 * @return TestProfileData
	 */	
	public TestProfileData getTestProfileDetail(long code) throws UnderWriterException, IUMException {
		
		LOGGER.info("getTestProfileDetail start");
		Connection conn = null;
		try {
			
			conn = new DataSourceProxy().getConnection();
			TestProfileDAO dao = new TestProfileDAO(conn);
			TestProfileData data = dao.retrieveTestProfile(code);
			
			return data;
		}
		catch (SQLException ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		catch (Exception ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		finally {
			closeConn(conn);
			LOGGER.info("getTestProfileDetail end");
		}
	}//getTestProfileDetail
	

	/**
	 * Creates a new test profile.
	 * @param data
	 */
	public void createTestProfile(TestProfileData data) throws UnderWriterException, IUMException {
		
		LOGGER.info("createTestProfile start");
		Connection conn = null;
		try {
			
			long code = data.getTestId();		
			String desc = data.getTestDesc();

			if (desc == null) {
				throw new UnderWriterException("Description is required.");
			}

			conn = new DataSourceProxy().getConnection();
			TestProfileDAO dao = new TestProfileDAO(conn);  

			// do not allow duplicate records to be entered
			if (dao.retrieveTestProfile(code) != null) {
				
				throw new UnderWriterException("Duplicate reference code : " + code);
			}			

			dao.insertTestProfile(data);
						
		}
		catch (UnderWriterException ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new UnderWriterException(ex);
		}
		catch (SQLException ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		catch (Exception ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		finally {
			closeConn(conn);
			LOGGER.info("createTestProfile end");
		}
	}//createTestProfile


	/**
	 * Updates a test profile record.
	 * @param data
	 */
	public void editTestProfile(TestProfileData data) throws UnderWriterException, IUMException {
		
		LOGGER.info("editTestProfile start");
		Connection conn = null;
		try {
				
			conn = new DataSourceProxy().getConnection();
			TestProfileDAO dao = new TestProfileDAO(conn);  
			dao.updateTestProfile(data);
				
		}
		catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMException(e);
		}
		finally {
			closeConn(conn);
			LOGGER.info("editTestProfile end");
		}
	}//editTestProfile
		

	/**
	 * Returns the list of examiner ranks.
	 * @return ArrayList
	 */	
	public ArrayList getExamRanks() throws UnderWriterException, IUMException {
		
		LOGGER.info("getExamRanks start");
		Connection conn = null;
		try {
			
			ArrayList list = new ArrayList();
			conn = new DataSourceProxy().getConnection();
			RanksDAO dao = new RanksDAO(conn);
			list = dao.retrieveRanks();
			
			return list;
		}
		catch (SQLException ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		catch (Exception ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		finally {
			closeConn(conn);
			LOGGER.info("getExamRanks end");
		}
	}//getExamRanks


	/**
	 * Returns the examiner rank data object.
	 * @return RankData
	 */	
	public RankData getExamRankDetail(long code) throws UnderWriterException, IUMException {
		
		LOGGER.info("getExamRankDetail start");
		Connection conn = null;
		try {
			
			conn = new DataSourceProxy().getConnection();
			RanksDAO dao = new RanksDAO(conn);
			RankData data = dao.retrieveRankData(code);
			
			return data;
		}
		catch (SQLException ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		catch (Exception ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		finally {
			closeConn(conn);
			LOGGER.info("getExamRankDetail end");
		}
	}//RankData
	

	/**
	 * Creates a new examiner rank.
	 * @param data
	 */
	public void createExamRank(RankData data) throws UnderWriterException, IUMException {
		
		LOGGER.info("createExamRank start");
		Connection conn = null;
		try {
			
			long code = data.getRankCode();		
			String desc = data.getRankDesc();

			if (desc == null) {
				throw new UnderWriterException("Description is required.");
			}

			conn = new DataSourceProxy().getConnection();
			RanksDAO dao = new RanksDAO(conn);  

			// do not allow duplicate records to be entered
			if (dao.retrieveRankData(code) != null) {
				LOGGER.warn("Duplicate reference code : " + code);
				throw new UnderWriterException("Duplicate reference code : " + code);
			}

			dao.insertRank(data);
						
		}
		catch (UnderWriterException ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new UnderWriterException(ex);
		}
		catch (SQLException ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		catch (Exception ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		finally {
			closeConn(conn);
			LOGGER.info("createExamRank end");
		}
	}//createExamRank


	/**
	 * Updates a test profile record.
	 * @param data
	 */
	public void editExamRank(RankData data) throws UnderWriterException, IUMException {
		
		LOGGER.info("editExamRank start");
		Connection conn = null;
		try {
			
			conn = new DataSourceProxy().getConnection();
			RanksDAO dao = new RanksDAO(conn);  
			dao.updateRank(data);
			
		}
		catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMException(e);
		}
		finally {
			closeConn(conn);
			LOGGER.info("editExamRank end");
		}
	}//editExamRank
	

	/**
	 * Returns the list of MIB impairment codes.
	 * @return ArrayList
	 */	
	public ArrayList getMIBImpairmentList() throws UnderWriterException, IUMException {
		
		LOGGER.info("getMIBImpairmentList start");
		Connection conn = null;
		try {
			
			ArrayList list = new ArrayList();
			conn = new DataSourceProxy().getConnection();
			CodeValuePairDAO dao = new CodeValuePairDAO(conn);
			list = dao.retrieveMIBImpairmentCodes();
			
			return list;
		}
		catch (SQLException ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		catch (Exception ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		finally {
			closeConn(conn);
			LOGGER.info("getMIBImpairmentList end");
		}
	}//getMIBImpairmentList


	/**
	 * Returns the MIB impairment code data object.
	 * @return MIBImpairmentData
	 */	
	public MIBImpairmentData getMIBImpairmentDetail(String code) throws UnderWriterException, IUMException {
		
		LOGGER.info("getMIBImpairmentDetail start");
		Connection conn = null;
		try {
			
			conn = new DataSourceProxy().getConnection();
			CodeValuePairDAO dao = new CodeValuePairDAO(conn);
			MIBImpairmentData data = dao.retrieveMIBImpairmentCodeDetail(code);
			
			return data;
		}
		catch (SQLException ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		catch (Exception ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		finally {
			closeConn(conn);
			LOGGER.info("getMIBImpairmentDetail end");
		}
	}//getMIBImpairmentDetail
	

	/**
	 * Creates a new MIB impairment code record.
	 * @param data
	 */
	public void createMIBImpairmentCode(MIBImpairmentData data) throws UnderWriterException, IUMException {
		
		LOGGER.info("createMIBImpairmentCode start");
		Connection conn = null;
		try {
			
			String code = data.getMIBImpairmentCode();	
			String desc = data.getMIBImpairmentDesc();

			if (code == null) {
				throw new UnderWriterException("Code is required.");
			}

			if (desc == null) {
				throw new UnderWriterException("Description is required.");
			}

			conn = new DataSourceProxy().getConnection();
			CodeValuePairDAO dao = new CodeValuePairDAO(conn);  
			
			// do not allow duplicate records to be entered
			if (dao.retrieveMIBImpairmentCodeDetail(code) != null) {
				LOGGER.warn("Duplicate reference code : " + code);
				throw new UnderWriterException("Duplicate reference code : " + code);
			}
			
			dao.insertMIBImpairmentCode(data);
						
		}
		catch (UnderWriterException ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new UnderWriterException(ex);
		}
		catch (SQLException ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		catch (Exception ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		finally {
			closeConn(conn);
			LOGGER.info("createMIBImpairmentCode end");
		}
	}//createMIBImpairmentCode


	/**
	 * Updates an MIB impairment code record.
	 * @param data
	 */
	public void editMIBImpairmentCode(MIBImpairmentData data) throws UnderWriterException, IUMException {
		
		LOGGER.info("editMIBImpairmentCode start");
		Connection conn = null;
		try {
			
			conn = new DataSourceProxy().getConnection();
			CodeValuePairDAO dao = new CodeValuePairDAO(conn);  
			dao.updateMIBImpairmentCode(data);
				
		}
		catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMException(e);
		}
		finally {
			closeConn(conn);
			LOGGER.info("editMIBImpairmentCode end");
		}
	}//editMIBImpairmentCode


	/**
	 * Returns the list of MIB number codes.
	 * @return ArrayList
	 */	
	public ArrayList getMIBNumberList() throws UnderWriterException, IUMException {
		
		LOGGER.info("getMIBNumberList start");
		Connection conn = null;
		try {
			
			ArrayList list = new ArrayList();
			conn = new DataSourceProxy().getConnection();
			CodeValuePairDAO dao = new CodeValuePairDAO(conn);
			list = dao.retrieveMIBNumberCodes();
			
			return list;
		}
		catch (SQLException ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		catch (Exception ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		finally {
			closeConn(conn);
			LOGGER.info("getMIBNumberList end");
		}
	}//getMIBNumberList


	/**
	 * Returns the MIB number code data object.
	 * @return MIBNumberData
	 */	
	public MIBNumberData getMIBNumberDetail(String code) throws UnderWriterException, IUMException {
		
		LOGGER.info("getMIBNumberDetail start");
		Connection conn = null;
		try {
			
			conn = new DataSourceProxy().getConnection();
			CodeValuePairDAO dao = new CodeValuePairDAO(conn);
			MIBNumberData data = dao.retrieveMIBNumberCodeDetail(code);
			
			return data;
		}
		catch (SQLException ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		catch (Exception ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		finally {
			closeConn(conn);
			LOGGER.info("getMIBNumberDetail end");
		}
	}//getMIBNumberDetail
	

	/**
	 * Creates a new MIB number code record.
	 * @param data
	 */
	public void createMIBNumberCode(MIBNumberData data) throws UnderWriterException, IUMException {
		
		LOGGER.info("createMIBNumberCode start");
		Connection conn = null;
		try {
			
			String code = data.getMIBNumberCode();	
			String desc = data.getMIBNumberDesc();

			if (code == null) {
				throw new UnderWriterException("Code is required.");
			}

			if (desc == null) {
				throw new UnderWriterException("Description is required.");
			}

			conn = new DataSourceProxy().getConnection();
			CodeValuePairDAO dao = new CodeValuePairDAO(conn);  
			
			
			if (dao.retrieveMIBNumberCodeDetail(code) != null) {
				LOGGER.warn("Duplicate reference code : " + code);
				throw new UnderWriterException("Duplicate reference code : " + code);
			}
			
			dao.insertMIBNumberCode(data);
						
		}
		catch (UnderWriterException ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new UnderWriterException(ex);
		}
		catch (SQLException ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		catch (Exception ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		finally {
			closeConn(conn);
			LOGGER.info("createMIBNumberCode end");
		}
	}//createMIBNumberCode


	/**
	 * Updates an MIB number code record.
	 * @param data
	 */
	public void editMIBNumberCode(MIBNumberData data) throws UnderWriterException, IUMException {
		
		LOGGER.info("editMIBNumberCode start");
		Connection conn = null;
		try {
			
			conn = new DataSourceProxy().getConnection();
			CodeValuePairDAO dao = new CodeValuePairDAO(conn);  
			dao.updateMIBNumberCode(data);
				
		}
		catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMException(e);
		}
		finally {
			closeConn(conn);
			LOGGER.info("editMIBNumberCode end");
		}
	}//editMIBNumberCode


	/**
	 * Returns the list of MIB action codes.
	 * @return ArrayList
	 */	
	public ArrayList getMIBActionList() throws UnderWriterException, IUMException {
		
		LOGGER.info("getMIBActionList start");
		Connection conn = null;
		try {
			
			ArrayList list = new ArrayList();
			conn = new DataSourceProxy().getConnection();
			CodeValuePairDAO dao = new CodeValuePairDAO(conn);
			list = dao.retrieveMIBActionCodes();
			
			return list;
		}
		catch (SQLException ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		catch (Exception ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		finally {
			closeConn(conn);
			LOGGER.info("getMIBActionList end");
		}
	}//getMIBActionList


	/**
	 * Returns the MIB action code data object.
	 * @return MIBActionData
	 */	
	public MIBActionData getMIBActionDetail(String code) throws UnderWriterException, IUMException {
		
		LOGGER.info("getMIBActionDetail start");
		Connection conn = null;
		try {
			
			conn = new DataSourceProxy().getConnection();
			CodeValuePairDAO dao = new CodeValuePairDAO(conn);
			MIBActionData data = dao.retrieveMIBActionCodeDetail(code);
			
			return data;
		}
		catch (SQLException ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		catch (Exception ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		finally {
			closeConn(conn);
			LOGGER.info("getMIBActionDetail end");
		}
	}//getMIBActionDetail
	

	/**
	 * Creates a new MIB action code record.
	 * @param data
	 */
	public void createMIBActionCode(MIBActionData data) throws UnderWriterException, IUMException {
		
		LOGGER.info("createMIBActionCode start");
		Connection conn = null;
		try {
			
			String code = data.getMIBActionCode();	
			String desc = data.getMIBActionDesc();

			if (code == null) {
				throw new UnderWriterException("Code is required.");
			}

			if (desc == null) {
				throw new UnderWriterException("Description is required.");
			}

			conn = new DataSourceProxy().getConnection();
			CodeValuePairDAO dao = new CodeValuePairDAO(conn);  
			
			// do not allow duplicate records to be entered
			if (dao.retrieveMIBActionCodeDetail(code) != null) {
				LOGGER.warn("Duplicate reference code : " + code);
				throw new UnderWriterException("Duplicate reference code : " + code);
			}
			
			dao.insertMIBActionCode(data);
						
		}
		catch (UnderWriterException ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new UnderWriterException(ex);
		}
		catch (SQLException ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		catch (Exception ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		finally {
			closeConn(conn);
			LOGGER.info("createMIBActionCode end");
		}
	}//createMIBActionCode


	/**
	 * Updates an MIB action code record.
	 * @param data
	 */
	public void editMIBActionCode(MIBActionData data) throws UnderWriterException, IUMException {
		
		LOGGER.info("editMIBActionCode start");
		Connection conn = null;
		try {
			
			conn = new DataSourceProxy().getConnection();
			CodeValuePairDAO dao = new CodeValuePairDAO(conn);  
			dao.updateMIBActionCode(data);
			
		}
		catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMException(e);
		}
		finally {
			closeConn(conn);
			LOGGER.info("editMIBActionCode end");
		}
	}//editMIBActionCode
	

	/**
	 * Returns the list of MIB letter codes.
	 * @return ArrayList
	 */	
	public ArrayList getMIBLetterList() throws UnderWriterException, IUMException {
		
		LOGGER.info("getMIBLetterList start");
		Connection conn = null;
		try {
			
			ArrayList list = new ArrayList();
			conn = new DataSourceProxy().getConnection();
			CodeValuePairDAO dao = new CodeValuePairDAO(conn);
			list = dao.retrieveMIBLetterCodes();
			
			return list;
		}
		catch (SQLException ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		catch (Exception ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		finally {
			closeConn(conn);
			LOGGER.info("getMIBLetterList end");
		}
	}//getMIBLetterList


	/**
	 * Returns the MIB letter code data object.
	 * @return MIBLetterData
	 */	
	public MIBLetterData getMIBLetterDetail(String code) throws UnderWriterException, IUMException {
		
		LOGGER.info("getMIBLetterDetail start");
		Connection conn = null;
		try {
			
			conn = new DataSourceProxy().getConnection();
			CodeValuePairDAO dao = new CodeValuePairDAO(conn);
			MIBLetterData data = dao.retrieveMIBLetterCodeDetail(code);
			
			return data;
		}
		catch (SQLException ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		catch (Exception ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		finally {
			closeConn(conn);
			LOGGER.info("getMIBLetterDetail end");
		}
	}//getMIBLetterDetail
	

	/**
	 * Creates a new MIB letter code record.
	 * @param data
	 */
	public void createMIBLetterCode(MIBLetterData data) throws UnderWriterException, IUMException {
		
		LOGGER.info("createMIBLetterCode start");
		Connection conn = null;
		try {
			
			String code = data.getMIBLetterCode();	
			String desc = data.getMIBLetterDesc();

			if (code == null) {
				throw new UnderWriterException("Code is required.");
			}

			if (desc == null) {
				throw new UnderWriterException("Description is required.");
			}

			conn = new DataSourceProxy().getConnection();
			CodeValuePairDAO dao = new CodeValuePairDAO(conn);  
			
			// do not allow duplicate records to be entered
			if (dao.retrieveMIBLetterCodeDetail(code) != null) {
				LOGGER.warn("Duplicate reference code : " + code);
				throw new UnderWriterException("Duplicate reference code : " + code);
			}
			
			dao.insertMIBLetterCode(data);
						
		}
		catch (UnderWriterException ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new UnderWriterException(ex);
		}
		catch (SQLException ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		catch (Exception ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		finally {
			closeConn(conn);
			LOGGER.info("createMIBLetterCode end");
		}
	}//createMIBLetterCode


	/**
	 * Updates an MIB letter code record.
	 * @param data
	 */
	public void editMIBLetterCode(MIBLetterData data) throws UnderWriterException, IUMException {
		
		LOGGER.info("editMIBLetterCode start");
		Connection conn = null;
		try {
			
			conn = new DataSourceProxy().getConnection();
			CodeValuePairDAO dao = new CodeValuePairDAO(conn);  
			dao.updateMIBLetterCode(data);
				
		}
		catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMException(e);
		}
		finally {
			closeConn(conn);
			LOGGER.info("editMIBLetterCode end");
		}
	}//editMIBLetterCode


	/**
	 * Returns the list of holidays.
	 * @return ArrayList
	 */	
	public ArrayList getHolidayList(String year) throws UnderWriterException, IUMException {
		
		LOGGER.info("getHolidayList start");
		Connection conn = null;		
		try {
			
			ArrayList list = new ArrayList();
			conn = new DataSourceProxy().getConnection();
			HolidayDAO dao = new HolidayDAO(conn);
			list = dao.retrieveHolidays(year);
			
			return list;
		}
		catch (SQLException ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		catch (Exception ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		finally {
			closeConn(conn);
			LOGGER.info("getHolidayList end");
		}
	}//getHolidayList


	/**
	 * Returns the holiday data object.
	 * @return HolidayData
	 */	
	public HolidayData getHolidayDetail(String code) throws UnderWriterException, IUMException {
		
		LOGGER.info("getHolidayDetail start");
		Connection conn = null;		
		try {
			
			conn = new DataSourceProxy().getConnection();
			HolidayDAO dao = new HolidayDAO(conn);
			HolidayData data = dao.retrieveHolidayDetail(code);
			
			return data;
		}
		catch (SQLException ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		catch (Exception ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		finally {
			closeConn(conn);
			LOGGER.info("getHolidayDetail end");
		}
	}//getHolidayDetail
	

	/**
	 * Creates a new holiday record.
	 * @param data
	 */
	public void createHolidayCode(HolidayData data) throws UnderWriterException, IUMException {
		
		LOGGER.info("createHolidayCode start");
		Connection conn = null;
		try {
			
			Date holidayDate = data.getHolidayDate();

			if (holidayDate == null) {
				throw new UnderWriterException("Holiday date is required.");
			}

			conn = new DataSourceProxy().getConnection();
			HolidayDAO dao = new HolidayDAO(conn);  
			
			dao.insertHoliday(data);
					
		}
		catch (UnderWriterException ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new UnderWriterException(ex);
		}
		catch (SQLException ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		catch (Exception ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		finally {
			closeConn(conn);
			LOGGER.info("createHolidayCode end");
		}
	}//createHolidayCode


	/**
	 * Updates an holiday record.
	 * @param data
	 */
	public void editHolidayCode(HolidayData data) throws UnderWriterException, IUMException {
		
		LOGGER.info("editHolidayCode start");
		Connection conn = null;
		try {
			
			conn = new DataSourceProxy().getConnection();
			HolidayDAO dao = new HolidayDAO(conn);  
			dao.updateHoliday(data);
				
		}
		catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMException(e);
		}
		finally {
			closeConn(conn);
			LOGGER.info("editHolidayCode end");
		}
	}//editHolidayCode


	/**
	 * Returns the list of auto-assignments.
	 * @return ArrayList
	 */	
	public ArrayList getAutoAssignments(SortHelper sort) throws UnderWriterException, IUMException {
		
		LOGGER.info("getAutoAssignments start");
		Connection conn = null;
		try {
			
			ArrayList list = new ArrayList();
			conn = new DataSourceProxy().getConnection();
			AutoAssignmentDAO dao = new AutoAssignmentDAO(conn);
			list = dao.retrieveAutoAssignments(sort);
			
			return list;
		}
		catch (SQLException ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		catch (Exception ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		finally {
			closeConn(conn);
			LOGGER.info("getAutoAssignments end");
		}
	}//getAutoAssignments
	

	/**
	 * Returns the auto-assignment object.
	 * @return AutoAssignmentData
	 */	
	public AutoAssignmentData getAutoAssignmentDetail(long autoAssignementId) throws UnderWriterException, IUMException {
		
		LOGGER.info("getAutoAssignmentDetail start");
		Connection conn = null;
		try {
			
			conn = new DataSourceProxy().getConnection();
			AutoAssignmentDAO dao = new AutoAssignmentDAO(conn);
			AutoAssignmentData data = dao.retrieveAutoAssignmentDetail(autoAssignementId);
			
			return data;
		}
		catch (SQLException ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		catch (Exception ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		finally {
			closeConn(conn);
			LOGGER.info("getAutoAssignmentDetail end");
		}
	}//getAutoAssignmentDetail


	/**
	 * Map an Auto-Assignment criteria to an underwriter.
	 * @param data
	 */
	public void createMapping(AutoAssignmentData data) throws UnderWriterException, IUMException {
		
		LOGGER.info("createMapping start");
		Connection conn = null;
		try {
			
			long id = data.getAutoAssignmentId();
			long criteriaCode = data.getCriteriaId();	
			String criteriaField = data.getFieldValue();
			String underwriter = data.getUserCode();

			if (criteriaField == null) {
				throw new UnderWriterException("Criteria field is required.");
			}

			if (underwriter == null) {
				throw new UnderWriterException("Undewriter is required.");
			}

			conn = new DataSourceProxy().getConnection();
			AutoAssignmentDAO dao = new AutoAssignmentDAO(conn);  
			
			// do not allow duplicate records to be entered
			if (dao.isDuplicate(id, criteriaCode, criteriaField, underwriter)) {
				LOGGER.warn("Duplicate auto-assignment criteria mapping.");
				throw new UnderWriterException("Duplicate auto-assignment criteria mapping.");
			}
			
			dao.insertMapping(data);
						
		}
		catch (UnderWriterException ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new UnderWriterException(ex);
		}
		catch (SQLException ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		catch (Exception ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		finally {
			closeConn(conn);
			LOGGER.info("createMapping end");
		}
	}//createMapping


	/**
	 * Updates the description of an existing Auto-Assignment mapping.
	 * @param data
	 */
	public void editMapping(AutoAssignmentData data) throws UnderWriterException, IUMException {
		
		LOGGER.info("editMapping start");
		Connection conn = null;
		try {
			
			conn = new DataSourceProxy().getConnection();
			AutoAssignmentDAO dao = new AutoAssignmentDAO(conn);  
			long id = data.getAutoAssignmentId();
			long criteriaCode = data.getCriteriaId();		
			String criteriaField = data.getFieldValue();
			String underwriter = data.getUserCode();
						
			// do not allow duplicate records to be entered
			if (dao.isDuplicate(id, criteriaCode, criteriaField, underwriter)) {
				LOGGER.warn("Duplicate auto-assignment criteria mapping.");
				throw new UnderWriterException("Duplicate auto-assignment criteria mapping.");
			}
			
			dao.updateMapping(data);
				
		}
		catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMException(e);
		}
		finally {
			closeConn(conn);
			LOGGER.info("editMapping end");
		}
	}//editMapping
		

	public void createLaboratoryData(LaboratoryData labData) throws UnderWriterException, IUMException {
		
		LOGGER.info("createLaboratoryData start");
		Connection conn = null;
		try {
			conn = new DataSourceProxy().getConnection();
			LaboratoryDAO labDAO = new LaboratoryDAO(conn);
			
			if(labDAO.selectLaboratory(labData.getLabId()) == null) {
				labDAO.insertLaboratoryData(labData);
			}
			else {
				LOGGER.warn("Duplicate laboratory id : " + String.valueOf(labData.getLabId()));
				throw new UnderWriterException("Duplicate laboratory id : " + String.valueOf(labData.getLabId()));
			}
		}
		catch (SQLException ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		finally {
			closeConn(conn);
			LOGGER.info("createLaboratoryData end");
		}
	}//createLaboratoryData


	public LaboratoryData getLaboratoryData(long labId) throws IUMException {
		
		LOGGER.info("getLaboratoryData start");
		Connection conn = null;
		try {
			conn = new DataSourceProxy().getConnection();
			LaboratoryDAO labDAO = new LaboratoryDAO(conn);	  
			LaboratoryData laboratory = labDAO.selectLaboratory(labId);
			return (laboratory);
		}
		catch (SQLException ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		catch (Exception ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		finally {
			closeConn(conn);
			LOGGER.info("getLaboratoryData end");
		}
	}//getLaboratoryData
	
	
	public ArrayList getLaboratoryTestList(long labId) throws IUMException {
		
		LOGGER.info("getLaboratoryTestList start");
		Connection conn = null;
		try {
			conn = new DataSourceProxy().getConnection();
			LaboratoryDAO labDAO = new LaboratoryDAO(conn);	  
			ArrayList list = labDAO.selectAllLaboratoryTestData(labId);
			return (list);
		}
		catch (SQLException ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		catch (Exception ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		finally {
			closeConn(conn);
			LOGGER.info("getLaboratoryTestList end");
		}
	}//getLaboratoryTestList
	
	
	public void editLaboratoryData(LaboratoryData labData, ArrayList listLabTest) throws IUMException {
		
		LOGGER.info("editLaboratoryData start");
		Connection conn = null;
		try {
			conn = new DataSourceProxy().getConnection();
			conn.setAutoCommit(false);
			LaboratoryDAO labDAO = new LaboratoryDAO(conn);
					
			labDAO.updateLaboratoryData(labData);
			
			ArrayList listOldLabTest = labDAO.selectAllLaboratoryTestData(labData.getLabId());
			int len = listOldLabTest.size();
			
			//update lab test			
			for(int i=0; i<len; i++) {
				LaboratoryTestData oldLabTest = (LaboratoryTestData)listOldLabTest.get(i);
				int len2 = listLabTest.size();
				for(int j=0; j<len2; j++) {
					LaboratoryTestData labTest = (LaboratoryTestData)listLabTest.get(j);
					if(oldLabTest.getTestCode() == labTest.getTestCode()) {
						labDAO.updateLaboratoryTestData(labTest, labData);
						listLabTest.remove(j);
						break;
					}
				}
			}
			
			
			int len2 = listLabTest.size();
			
			for(int i=0; i<len2; i++) {
				LaboratoryTestData labTest = (LaboratoryTestData)listLabTest.get(i);
				labDAO.insertLaboratoryTestData(labTest, labData);
			}
						
			conn.commit();		  					
			
		}
		catch (SQLException ex) {
			try {
				if (conn != null) {
					conn.rollback();
				}				
			}
			catch (SQLException e) {		
				LOGGER.error(CodeHelper.getStackTrace(e));
				throw new IUMException(e);
			}
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		catch (Exception ex) {
			try {	
				if (conn != null) {
					conn.rollback();
				}				
			}	
			catch (SQLException e) {		
				LOGGER.error(CodeHelper.getStackTrace(e));
				throw new IUMException(e);
			}
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);				
		}
		finally {
			closeConn(conn);
			LOGGER.info("editLaboratoryData end");
		}
	}//editLaboratoryData
	

	/**
	 * Returns the list of laboratories.
	 * @return ArrayList
	 */	
	public ArrayList getLaboratoryList() throws IUMException {
		
		LOGGER.info("getLaboratoryList start");
		Connection conn = null;
		try {
			conn = new DataSourceProxy().getConnection();
			LaboratoryDAO labDAO = new LaboratoryDAO(conn);	  
			ArrayList laboratoryList = labDAO.selectLaboratories();
			return (laboratoryList);
		}
		catch (SQLException ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		catch (Exception ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		finally {
			closeConn(conn);
			LOGGER.info("getLaboratoryList end");
		}
	}//getLaboratoryList
	
	


	/**
	 * Retrieves Requirement Codes for LOV administration.
	 * @return
	 * @throws IUMException
	 */
	public ArrayList getRequirementDetails()throws IUMException {
		
		LOGGER.info("getRequirementDetails start");
		Connection conn = null;
		try {
			conn = new DataSourceProxy().getConnection();
			ReferenceDAO refDAO = new ReferenceDAO(conn);	
			return refDAO.retrieveRequirements();
		}
		catch(SQLException e){
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMException(e.getMessage());
		}
		finally {
			closeConn(conn);
			LOGGER.info("getRequirementDetails end");
		}
	}//getRequirementDetails


	/**
	 * Retrieves a specific RequirementData given the req Code as a parameter.
	 * @param reqcode
	 * @return
	 * @throws IUMException
	 */
	public RequirementData getRequirementDetail(String reqcode) throws IUMException{
		
		LOGGER.info("getRequirementDetail start");
		Connection conn = null;
		try {
			conn = new DataSourceProxy().getConnection();
			ReferenceDAO refDAO = new ReferenceDAO(conn);	
			return refDAO.retrieveRequirement(reqcode);
		}
		catch(SQLException e){
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMException(e.getMessage());
		}
		finally {
			closeConn(conn);
			LOGGER.info("getRequirementDetail end");
		}
	}//getRequirementDetail


	/**
	 * 
	 * TODO Class Description of Reference.java
	 * @author Engel
	 *
	 */
	public ArrayList getSLOs() throws IUMException {
		
		LOGGER.info("getSLOs start");
		Connection conn = null;
		try{
			conn = new DataSourceProxy().getConnection();
			ReferenceDAO refDAO = new ReferenceDAO(conn);	
			return refDAO.retrieveSLOs();
		}
		catch(SQLException e){
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMException(e.getMessage());
		}finally{
			closeConn(conn);
			LOGGER.info("getSLOs end");
		}
	}//getSLOs
	
	
	/**
	 * Retrieves a specific SunLifeOfficeData given the req Code as a parameter.
	 * @param sloId
	 * @return
	 * @throws IUMException
	 */
	public SunLifeOfficeData getSLODetail(String sloId) throws IUMException {
		
		LOGGER.info("getSLODetail start");
		Connection conn = null;
		try{
			conn = new DataSourceProxy().getConnection();
			ReferenceDAO refDAO = new ReferenceDAO(conn);	
			return refDAO.retrieveSLO(sloId);
		}
		catch(SQLException e){
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMException(e.getMessage());
		}
		finally {
			closeConn(conn);
			LOGGER.info("getSLODetail end");
		}
	}//getSLODetail


	//ksantos 02/27/2004
	public ArrayList getExaminers() throws IUMException {
		
		LOGGER.info("getExaminers start");
		Connection conn = null;
		try {
			
			ArrayList list = new ArrayList();
			conn = new DataSourceProxy().getConnection();
			ExaminerDAO dao = new ExaminerDAO(conn);
			list = dao.selectExaminers();
			
			return list;
		}
		catch (SQLException ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		catch (Exception ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		finally {
			closeConn(conn);
			LOGGER.info("getExaminers end");
		}		
	}//getExaminers
	
	//ksantos 03/02/2004
	public void createExaminer(ExaminerData examiner) throws IUMException, UnderWriterException {
		
		LOGGER.info("createExaminer start");
		Connection conn = null;
		try{
			
			conn = new DataSourceProxy().getConnection();
			ExaminerDAO dao = new ExaminerDAO(conn);
			if(dao.selectExaminer(examiner.getExaminerId()) == null){	
				dao.insertExaminer(examiner);
			}
			else {
				LOGGER.warn("Duplicate Examiner ID: "+String.valueOf(examiner.getExaminerId()));
				throw new UnderWriterException("Duplicate Examiner ID: "+String.valueOf(examiner.getExaminerId()));
			}
			
		}
		catch(SQLException ex){
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		finally {
			closeConn(conn);
			LOGGER.info("createExaminer end");
		}		
	}	
	//ksantos 03/02/2004
	
	
	/**
	 * Creates a sun life office record.
	 * @param data
	 * @throws IUMException
	 */
	public void createSLO(SunLifeOfficeData data) throws IUMException {
		
		LOGGER.info("createSLO start");
		Connection conn = null;
		try {
			
			conn = new DataSourceProxy().getConnection();	
			ReferenceDAO dao = new ReferenceDAO(conn);
			dao.insertSLO(data);
				
		}
		catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMException(e.getMessage());
		}
		finally {
			closeConn(conn);
			LOGGER.info("createSLO end");
		}
		
	}//createSLO


	/**
	 * Updates a sun life office record.
	 * @param data
	 * @throws IUMException
	 */
	public void editSLO(SunLifeOfficeData data) throws IUMException {
		
		LOGGER.info("editSLO start");
		Connection conn = null;
		try {
			
			conn = new DataSourceProxy().getConnection();
			ReferenceDAO dao = new ReferenceDAO(conn);
			dao.editSLO(data);
				
		}
		catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMException(e.getMessage());
		}
		finally {
			closeConn(conn);
			LOGGER.info("editSLO end");
		}
	}//editSLO
	
	
	//ksantos 03/04/2004
	public ExaminerData getExaminer(long examinerId) throws IUMException {
		
		LOGGER.info("getExaminer start");
		Connection conn = null;
		ExaminerData examiner = null;
		try{
			
			conn = new DataSourceProxy().getConnection();
			ExaminerDAO edao = new ExaminerDAO(conn);
			examiner = edao.selectExaminer(examinerId);
			
			return examiner;			
		}
		catch(SQLException ex){
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		catch(Exception ex){
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		finally {
			closeConn(conn);
			LOGGER.info("getExaminer end");
		}
	}//getExaminer
	//ksantos 03/04/2004

	//ksantos 03/05/2004
	public void editExaminer(ExaminerData examiner) throws IUMException {
		
		LOGGER.info("editExaminer start");
		Connection conn = null;
		try{
			
			conn = new DataSourceProxy().getConnection();
			ExaminerDAO edao = new ExaminerDAO(conn);
			edao.updateExaminer(examiner);
				
		}
		catch(SQLException ex){
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);			
		}
		catch(Exception ex){
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		finally {
			closeConn(conn);
			LOGGER.info("editExaminer end");
		}		
	}//editExaminer
	//ksantos 03/05/2004


	//ksantos 03/05/2004
	public ArrayList getSpecializations() throws IUMException {
		
		LOGGER.info("getSpecializations start");
		Connection conn = null;
		try {
			
			conn = new DataSourceProxy().getConnection();
			SpecializationDAO sDao = new SpecializationDAO(conn);
			ArrayList list = new ArrayList();
			list = sDao.selectSpecializations();
			
			return list;
		}
		catch(SQLException e){
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMException(e);
		}
		finally {
			closeConn(conn);
			LOGGER.info("getSpecializations end");
		}
	}//getSpecializations
	//ksantos 03/05/2004


	public void setupMIBSchedule(JobScheduleData newSchedule) throws IUMException {
		
		LOGGER.info("setupMIBSchedule start");
		Connection conn = null;
		try {
			conn = new DataSourceProxy().getConnection();
			JobScheduleDAO dao = new JobScheduleDAO(conn);			
			dao.saveJobSchedule(newSchedule);
		}
		catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMException(e);
		}
		finally {
			closeConn(conn);
			LOGGER.info("setupMIBSchedule end");
		}		
	}//setupMIBSchedule


	public JobScheduleData retrieveMIBSchedule() throws IUMException {
		
		LOGGER.info("retrieveMIBSchedule start");
		Connection conn = null;		
		JobScheduleData jobSched = null;
		try {
			conn = new DataSourceProxy().getConnection();
			JobScheduleDAO dao = new JobScheduleDAO(conn);
			jobSched = dao.retrieveJobSchedule(IUMConstants.SCHEDULE_TYPE_MIB_EXPORT);
			return jobSched;
		}
		catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMException(e);
		}
		finally {
			closeConn(conn);
			LOGGER.info("retrieveMIBSchedule end");
		}					
	}//retrieveMIBSchedule


	private void closeConn(Connection conn) throws IUMException {
		
		LOGGER.info("closeConn start");
		
		try {
			if (conn != null){
				conn.close();
			}
		}
		catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMException(e);
		}
		
		LOGGER.info("closeConn end");
	}
}
