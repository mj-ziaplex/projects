/*
 * Created on Dec 18, 2003
 * package name = com.slocpi.ium.underwriter
 * file name    = DoctorsNotes.java
 */
package com.slocpi.ium.underwriter;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.ResourceBundle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.slocpi.ium.IUMException;
import com.slocpi.ium.data.MedicalNotes;
import com.slocpi.ium.data.NotificationTemplateData;
import com.slocpi.ium.data.UserProfileData;
import com.slocpi.ium.data.dao.MedicalDAO;
import com.slocpi.ium.data.dao.UserDAO;
import com.slocpi.ium.data.dao.WorkflowDAO;
import com.slocpi.ium.data.util.DataSourceProxy;
import com.slocpi.ium.interfaces.notification.IUMNotificationException;
import com.slocpi.ium.interfaces.notification.NotificationMessage;
import com.slocpi.ium.interfaces.notification.Notifier;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.IUMConstants;


/**
 * @author Engel
 * This is the business object of the Doctors Notes that contains the retrieval of doctor notes and inserting
 * of new doctor notes.
 * 
 */
public class DoctorsNotes {

	private static final Logger LOGGER = LoggerFactory.getLogger(DoctorsNotes.class);
	/**
	 * Retrieves the list of medical notes.
	 * @param referenceNumber reference number
	 *  @return ArrayList of MedicalNotes
	 */
	public ArrayList getDoctorsNotesList(String referenceNumber) throws UnderWriterException, IUMException {
		
		LOGGER.info("getDoctorsNotesList start");
		Connection conn = null;
		try {
			conn = new DataSourceProxy().getConnection();
			MedicalDAO medDAO = new MedicalDAO(conn);	  
			ArrayList medNotesList = medDAO.getMedicalNotes(referenceNumber);
			return (medNotesList);
		}
		catch (SQLException ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		catch (Exception ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}	
		finally{
			closeConn(conn);		
			LOGGER.info("getDoctorsNotesList end");
		}
	}
	
	
	/**
	 * Inserts the medical note created and send a notification to the underwriter/medical exminer.
	 * @param note data object containing the medical notes detail
	 * @param userId user id of the recepient for the notification
	 */
	public void insertDoctorsNote(MedicalNotes note, String userId) throws UnderWriterException, IUMException, IUMNotificationException {
		
		LOGGER.info("insertDoctorsNote start");
		Connection conn = null;
		try {
			conn = new DataSourceProxy().getConnection();
			MedicalDAO medDAO = new MedicalDAO(conn);	  
			conn.setAutoCommit(false);
			medDAO.createMedicalNotes(note, userId);
			conn.commit();
			ArrayList recipients = new ArrayList();
			UserDAO rcpt = new UserDAO();
			UserProfileData rcptProfile = rcpt.selectUserProfile(userId);			
			String sendToId = rcptProfile.getUserId();
			String sendToName = rcptProfile.getFirstName() + " " + rcptProfile.getLastName();
			String email = rcptProfile.getEmailAddr();
			recipients.add(email);
			
			LOGGER.debug("sendTo ==> "+sendToId);
			LOGGER.debug("email ==> "+email);
			
			// get sender email address
			String sender = rcpt.selectUserProfile(note.postedBy).getEmailAddr();
						
			if (email != null) {
				//get content of email
				WorkflowDAO wfdao = new WorkflowDAO();
				NotificationTemplateData data = wfdao.getNotificationTemplate(IUMConstants.NOTIFY_DOCTORS_NOTES);
				String subject = data.getSubject();
				LOGGER.debug("subject : "+subject);
				ResourceBundle rb = ResourceBundle.getBundle(IUMConstants.IUM_CONFIG);
				String content = rb.getString(IUMConstants.IUM_HOME_PAGE) + "login.do?page=DN&id=" + note.getReferenceNumber();
				LOGGER.debug("content : "+content);
				
				//code to send notification
				NotificationMessage notificationMsg =  new NotificationMessage();
				notificationMsg.setRecipients(recipients);
				notificationMsg.setSender(sender);		
				notificationMsg.setSubject(subject + note.getReferenceNumber());
				notificationMsg.setContent(content);

				Notifier notifier = new Notifier();
				notifier.sendMail(notificationMsg);
			}
			else {
				LOGGER.warn("No email address for " + sendToId);
				throw new UnderWriterException("Unable to send notification. " + sendToName + " doesn't have a valid email address.");
			}
		}
		catch (UnderWriterException ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new UnderWriterException(ex);
		}	
		catch (IUMNotificationException ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMNotificationException(ex);
		}
		catch (SQLException ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			try {
				if (conn != null) {
					conn.rollback();
				}			
			} catch (SQLException e) {
				LOGGER.error(CodeHelper.getStackTrace(ex));
			}
			throw new IUMException(ex);
		}
		catch (Exception ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		finally{
			closeConn(conn);
			LOGGER.info("insertDoctorsNote end");
		}
	}


	private void closeConn(Connection conn)throws IUMException{
		
		LOGGER.info("closeConn start");
		try{
			if (conn != null){
				conn.close();
				return;
			}
		}
		catch (Exception ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		LOGGER.info("closeConn end");
		
	}
}
