/*
 * Created on Dec 22, 2003
 * package name = com.slocpi.ium.underwriter
 * file name    = Underwriter.java
 */
package com.slocpi.ium.underwriter;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.slocpi.ium.IUMException;
import com.slocpi.ium.data.dao.UserDAO;
import com.slocpi.ium.data.util.DataSourceProxy;
import com.slocpi.ium.util.CodeHelper;

/**
 * @author Engel
 * TODO Class Description of Underwriter.java
 * 
 */
public class Underwriter {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(Underwriter.class);
	/**
	 * @TODO method description updateAssignments
	 * @param referenceNumber
	 * @param assignee
	 */
	public void updateAssignments(String referenceNumber, String assignee){
		//@TODO method implementation updateAssignments
	}
	
	public ArrayList getListUserLoadByStatus(ArrayList roles,ArrayList status) throws IUMException {
		
		LOGGER.info("getListUserLoadByStatus start");
		
		try {
			ArrayList result = new ArrayList();
			UserDAO dao = new UserDAO();
			result = dao.getUserLoadList(roles, status);
			
			LOGGER.info("getListUserLoadByStatus end");
			return result;
		}
		catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMException(e);
		}
		catch (Exception ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		
		
				
		
	}//getListUserLoadByStatus


	private void closeConn(Connection conn) throws IUMException {
		
		LOGGER.info("closeConn start");
		try {
			if (conn != null){
				conn.close();
			}
		}
		catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMException(e);
		}
		LOGGER.info("closeConn end");
	}//closeConn
	
}
