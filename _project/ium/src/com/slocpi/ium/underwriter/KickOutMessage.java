/*
 * Created on Dec 18, 2003
 * package name = com.slocpi.ium.underwriter
 * file name    = KickOutMessage.java
 */
package com.slocpi.ium.underwriter;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.slocpi.ium.IUMException;
import com.slocpi.ium.data.KickOutMessageData;
import com.slocpi.ium.data.dao.KickOutMessageDAO;
import com.slocpi.ium.data.util.DataSourceProxy;
import com.slocpi.ium.util.CodeHelper;

/**
 * @author Engel
 * This is the business object for the KickOut messages. It includes methods for inserting and retrieval of records.
 * 
 */
public class KickOutMessage {
	private static final Logger LOGGER = LoggerFactory.getLogger(KickOutMessage.class);
	/**
	 * Retrieves the kickout messages for this reference number.
	 * @param referenceNumber
	 * @return ArrayList of KickOutMessageData
	 * @throws UnderWriterException
	 * @throws IUMException
	 */
	public ArrayList getMessages(final String referenceNumber) throws UnderWriterException, IUMException {
		
		LOGGER.info("getMessages start");
		
		try {
			
			KickOutMessageDAO koMsgDAO = new KickOutMessageDAO();	  
			ArrayList koMsgList = koMsgDAO.retrieveKOMessages(referenceNumber);
			
			LOGGER.info("getMessages end");
			return (koMsgList);
		}
		catch (SQLException ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		catch (Exception ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		
			
		
		
	}
	
	/**
	 * Inserts the KickOutMessageData to the KICKOUT_MESSAGES table.
	 * @param ko KickOutMessageData object
	 * @throws UnderWriterException
	 * @throws IUMException
	 */

	public void saveMessages(KickOutMessageData ko) throws UnderWriterException, IUMException {
		
		LOGGER.info("saveMessages start");
		
		try {
			
			KickOutMessageDAO koMsgDAO = new KickOutMessageDAO();	  
			koMsgDAO.insertKOMessage(ko);
		
		}
		catch (SQLException ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		catch (Exception ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}	
		
		LOGGER.info("saveMessages end");
				
		
	}
	
	
}