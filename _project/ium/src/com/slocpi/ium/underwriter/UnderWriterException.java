/*
 * Created on Jan 14, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.underwriter;

import com.slocpi.ium.IUMException;

/**
 * @author daguila
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class UnderWriterException extends IUMException {

	public UnderWriterException(String msg) {
		super(msg);
	}
	
	public UnderWriterException(Exception e) {
		super(e);
	}
}
