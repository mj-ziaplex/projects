/*
 * Created on Feb 24, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.underwriter;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.slocpi.ium.IUMException;
import com.slocpi.ium.data.AuditTrailFilterData;
import com.slocpi.ium.data.PurgeStatisticData;
import com.slocpi.ium.data.dao.AuditTrailDAO;
import com.slocpi.ium.data.dao.PurgeStatisticDAO;
import com.slocpi.ium.data.util.DataSourceProxy;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.IUMConstants;

/**
 * @author Cris
 * This is the business object of the Audit Trail that contains the retrieval and purging of
 * Audit Trail records.  
 */
public class AuditTrail {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(AuditTrail.class);
	/**
	 * Retrieves audit trail logs for this filter
	 * @param filterData filter to be used in the retrieval
	 * @return ArrayList of AuditTrailData
	 * @throws IUMException
	 */
	public ArrayList getAuditTrailLogs(AuditTrailFilterData filterData) throws IUMException{
		
		LOGGER.info("getAuditTrailLogs start");
		Connection conn = new DataSourceProxy().getConnection();
		ArrayList list = new ArrayList();
		AuditTrailDAO dao = new AuditTrailDAO (conn);
		try {
			list = dao.retrieveAuditTrail(filterData);
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMException(e);
		}
		finally{
			closeConn(conn);
		}
		LOGGER.info("getAuditTrailLogs end");
		return list;
	}

	/**
	 * Deletes audit trail logs within the given date range
	 * @param purgeDate
	 * @param startDate
	 * @param endDate
	 * @return number of deleted audit trails
	 * @throws IUMException
	 */
	public int purgeAuditTrails(Date purgeDate, Date startDate, Date endDate) throws IUMException {
		
		LOGGER.info("purgeAuditTrails start");
		int recordCount = 0;
		String result = "F";
		Connection conn = new DataSourceProxy().getConnection();
		AuditTrailDAO dao = new AuditTrailDAO(conn);
		try {
			conn.setAutoCommit(false);
			recordCount = dao.deleteAuditTrails(startDate, endDate);
			result = "S";
			conn.commit();
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			try {
				conn.rollback();
			} catch (SQLException e1) {
				LOGGER.error(CodeHelper.getStackTrace(e1));
			}
			throw new IUMException(e);
		} finally{
			PurgeStatisticData data = new PurgeStatisticData();
			data.setPurgeDate(purgeDate);
			data.setStartDate(startDate);
			data.setEndDate(endDate);
			data.setCriteria(IUMConstants.PURGE_AUDIT_TRAIL_LOGS);
			data.setNumberOfRecords(recordCount);
			data.setResult(result);

			PurgeStatisticDAO purgeDao = new PurgeStatisticDAO(conn);
			try {
				purgeDao.insertPurgeStatistic(data);
			} catch (SQLException e) {
				LOGGER.error(CodeHelper.getStackTrace(e));
				throw new IUMException(e);
			} finally {
				closeConn(conn);
			}
		}
		LOGGER.info("purgeAuditTrails end");
		return recordCount;
	}

	private void closeConn(Connection conn)throws IUMException{
		
		LOGGER.info("closeConn start");
		try{
			if (conn != null){
				conn.close();
				return;
			}
		}
		catch (Exception ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		LOGGER.info("closeConn end");
	}

}
