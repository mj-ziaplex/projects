/*
 * Created on Feb 2, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.underwriter.test.assessmentrequest;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import com.slocpi.ium.IUMException;
import com.slocpi.ium.data.AssessmentRequestData;
import com.slocpi.ium.underwriter.AssessmentRequest;
import com.slocpi.ium.underwriter.UnderWriterException;

/**
 * @author Cris
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class ReceiveARAtUSDTestCase extends TestCase {

	/**
	 * Constructor for ReceiveARAtUSDTestCase.
	 * @param arg0
	 */
	public ReceiveARAtUSDTestCase(String arg0) {
		super(arg0);
	}

	public static Test suite() {
		TestSuite suite = new TestSuite(ReceiveARAtUSDTestCase.class);
		return suite;
	}
	
	public static void main(String[] args) {
		junit.textui.TestRunner.run(suite());
	}
	
	public void testForAssessment() throws SQLException, IOException{
		boolean result = false;
		TestAssessmentRequestUtil data = new TestAssessmentRequestUtil();
		String user = "IUMDEV";
		AssessmentRequest ar = new AssessmentRequest();
		ArrayList arList = data.createAssessmentTestData();
		for (int i = 0; i<arList.size(); i++){
			AssessmentRequestData arData = (AssessmentRequestData)arList.get(i);
			debug("Assessment Number: " + arData.getReferenceNumber() + " Status: " + arData.getStatus().getStatusId());
			try {
				ar.setToForFacilitatorAction(arData.getReferenceNumber(), user);
			} catch (UnderWriterException e) {
				e.printStackTrace();
			} catch (IUMException e) {
				e.printStackTrace();
			}	
		}
		assertEquals("Error in submitting request for approval: ", true,result);
	}
	
	private void debug(String message){
		System.out.println("*** [TEST CASE: RECEIVE AR AT USD] *** " + message);
	}
}
