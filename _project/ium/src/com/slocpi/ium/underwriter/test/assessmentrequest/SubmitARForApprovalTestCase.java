	/*
 * Created on Jan 15, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.underwriter.test.assessmentrequest;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import junit.textui.TestRunner;

import com.slocpi.ium.data.AssessmentRequestData;
import com.slocpi.ium.data.ClientData;
import com.slocpi.ium.data.LOBData;
import com.slocpi.ium.data.StatusData;
import com.slocpi.ium.data.SunLifeOfficeData;
import com.slocpi.ium.data.UserProfileData;
import com.slocpi.ium.data.dao.AssessmentRequestDAO;
import com.slocpi.ium.data.util.DataSourceProxy;
import com.slocpi.ium.underwriter.AssessmentRequest;
import com.slocpi.ium.util.IUMConstants;

/**
 * @author Cris
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class SubmitARForApprovalTestCase extends TestCase {
	int i=0;
	ArrayList invalidStatusList = new ArrayList();
	
	public SubmitARForApprovalTestCase(java.lang.String testName) {
		super(testName);
	}
	
	public static Test suite() {
		TestSuite suite = new TestSuite(SubmitARForApprovalTestCase.class);
		return suite;
	}

	public static void main(String[] args) {
		TestRunner.run(suite());
	}
	
	public void testSubmitRequestNormal() throws Exception{
		boolean result = false;
		AssessmentRequest ar = new AssessmentRequest();
		AssessmentRequestData actualData = initializeARData();
				
		String referenceNumber = actualData.getReferenceNumber();
		long status = (IUMConstants.STATUS_FOR_APPROVAL);
		String assignee  = "user1";
		String updatedBy = "arDev";
		
		ar.submitRequestForApproval(referenceNumber,assignee,updatedBy);
		
		if (checkStatus(referenceNumber,status) && checkAssignedTo(referenceNumber,assignee)){
			result = true;
		}
		assertEquals("Error in submitting request for approval: ", true,result);
	}
	
	public void testSubmitMultipleRequest() throws Exception {
		boolean result = false;
		
		for (int index=0; index<5; index++){
			AssessmentRequest ar = new AssessmentRequest();
			AssessmentRequestData actualData = initializeARData();
				
			String referenceNumber = actualData.getReferenceNumber();
			long status = (IUMConstants.STATUS_FOR_APPROVAL);
			String assignee  = "user1";
			String updatedBy = "arDev";
		
			ar.submitRequestForApproval(referenceNumber,assignee,updatedBy);
		
			if (checkStatus(referenceNumber,status) && checkAssignedTo(referenceNumber,assignee)){
				result = true;
			}
			assertEquals("Error in submitting request for approval: ", true,result);
			i++;
		}
	}
	
	public void testSubmitWithInvalidStatus()throws Exception{
		boolean result = false;
		AssessmentRequest ar = new AssessmentRequest();
		AssessmentRequestData actualData = initializeARData();
				
		String referenceNumber = actualData.getReferenceNumber();
		
		long status = (IUMConstants.STATUS_FOR_APPROVAL);
		String assignee  = "user1";
		String updatedBy = "arDev";
		
		ar.changeStatus(actualData.getReferenceNumber(),IUMConstants.STATUS_APPROVED,updatedBy);
		ar.submitRequestForApproval(referenceNumber,assignee,updatedBy);
		
		if (checkStatus(referenceNumber,status) && checkAssignedTo(referenceNumber,assignee)){
			result = true;
		}
		assertEquals("Error in submitting request for approval: ", false,result);
	}
	
	private AssessmentRequestData initializeARData() throws Exception{
		String referenceNumber = "AR00" + i;
		String branchId = "LEGASPISO";
		String userId = "IUMDEV";
		String ownerId = "owner";
		String clientId = "insured";
		String agentId = "agent";
		String assigned = "REQUW1";
		String underwriterId = "uw";
		
		
		//1/28 JMA - added set Autocommit (false), and new connection variable
		
		long status = IUMConstants.STATUS_NB_REVIEW_ACTION;
		
		AssessmentRequestData arData = new AssessmentRequestData();
		AssessmentRequest ar = new AssessmentRequest();
		
		arData = ar.getDetails(referenceNumber);
		
		if (arData.getReferenceNumber() != null){
			AssessmentRequestDAO dao = new AssessmentRequestDAO();
			try{
				dao.deleteAssessment(referenceNumber);
			} catch (SQLException e){
				e.printStackTrace();
			}
			
		}
		
		LOBData lobdata = new LOBData();
		lobdata.setLOBCode(IUMConstants.LOB_INDIVIDUAL_LIFE);
		
		SunLifeOfficeData branchData = new SunLifeOfficeData();
		branchData.setOfficeId(branchId);
		
		StatusData statData = new StatusData();
		statData.setStatusId(status);
		
		UserProfileData user = new UserProfileData();
		user.setUserId(userId);
		
		UserProfileData agent = new UserProfileData();
		agent.setUserId(agentId);
		
		UserProfileData assignedTo = new UserProfileData();
		assignedTo.setUserId(assigned);
		
		UserProfileData underwriter = new UserProfileData();
		underwriter.setUserId(underwriterId);
		
		ClientData owner = new ClientData();
		owner.setClientId(ownerId);
		
		ClientData client = new ClientData();
		client.setClientId(clientId);
		

		arData.setReferenceNumber(referenceNumber);
		arData.setLob(lobdata);
		arData.setBranch(branchData);
		arData.setStatus(statData);
		arData.setCreatedBy(user);
		arData.setOwner(owner);
		arData.setInsured(client);
		arData.setAgent(agent);
		arData.setAssignedTo(assignedTo);
		arData.setUnderwriter(underwriter);
		arData.setFolderLocation(agent);
		arData.setApplicationReceivedDate(new java.util.Date());
		arData.setForwardedDate(new java.util.Date());
		
		ar.createAssessmentRequest(arData);
		ar.getDetails(referenceNumber);
		return arData;
	}
	
	private boolean checkStatus (String referenceNumber, long expectedStatus) throws Exception{
		boolean result = false;
		AssessmentRequest ar = new AssessmentRequest();

		AssessmentRequestData arData = ar.getDetails(referenceNumber);
		if (expectedStatus == arData.getStatus().getStatusId()){
			result = true;
		}
		return result;
	}
	
	private boolean checkAssignedTo (String referenceNumber, String expectedUser) throws Exception{
		boolean result = false;
		AssessmentRequest ar = new AssessmentRequest();

		AssessmentRequestData arData = ar.getDetails(referenceNumber);
		if (expectedUser.equals(arData.getAssignedTo().getUserId())){
			result = true;
		}
		return result;
	}
}
