/*
 * Created on Jan 5, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.underwriter.test.assessmentrequest;

import java.util.ArrayList;
import java.util.Date;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import com.slocpi.ium.data.PolicyRequirementsData;
import com.slocpi.ium.underwriter.PolicyRequirements;


/**
 * This test case will check the behaviour of the objects retrieved from
 * MQ.  This test case depends on the in records found in the ABACUS tables.
 * A method populateAbacus tables will be created.  
 * 
 * The policy and policy_queue table will be populated.
 * @author daguila
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class ReviewListRequirementsTestCase extends TestCase {

	public ReviewListRequirementsTestCase(java.lang.String testName) {        
		super(testName);    
	}        

	public static Test suite() {        
		TestSuite suite = new TestSuite(ReviewListRequirementsTestCase.class);                
		return suite;    
	}	
	
	public static void main(java.lang.String[] args) {   
		junit.textui.TestRunner.run(suite());    
	}
	
	public void testListRequirements() throws Exception {
		boolean result = false;

		String refNo = "A1";
		PolicyRequirements pr = new PolicyRequirements();
		ArrayList reqList = pr.getRequirements(refNo);
        
        result = validateListRequirements(reqList);

		assertEquals("List requirements failed.",true,result);
	}
	
	private boolean validateListRequirements (ArrayList reqList) throws Exception {
		boolean result = true;
		try {		
			int size = reqList.size();
			for (int i = 0; i < size; i++) {
			  PolicyRequirementsData reqData = (PolicyRequirementsData)reqList.get(i);
			  long reqId = reqData.getRequirementId();
			  long seqNo = reqData.getSequenceNumber();
			  String reqCode = reqData.getRequirementCode();
			  long status = reqData.getStatus().getStatusId();
			  Date statusDate = reqData.getStatusDate();
			  String comment = reqData.getComments();
			  String clientType = reqData.getClientType();
			  String level = reqData.getLevel();
			  Date followUpDate = reqData.getFollowUpDate();
			  Date dateSent = reqData.getDateSent();
			  String clientNo = reqData.getClientId();
			}
		}
		catch (Exception e) {
			result = false;
		}
		return result;
	}

}
