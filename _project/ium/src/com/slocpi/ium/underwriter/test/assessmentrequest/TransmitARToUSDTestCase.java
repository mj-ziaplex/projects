/*
 * Created on Jan 15, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.underwriter.test.assessmentrequest;

import java.util.ArrayList;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import junit.textui.TestRunner;

import com.slocpi.ium.data.AssessmentRequestData;
import com.slocpi.ium.data.ClientData;
import com.slocpi.ium.data.LOBData;
import com.slocpi.ium.data.StatusData;
import com.slocpi.ium.data.SunLifeOfficeData;
import com.slocpi.ium.data.UserProfileData;
import com.slocpi.ium.data.dao.AssessmentRequestDAO;
import com.slocpi.ium.data.util.DataSourceProxy;
import com.slocpi.ium.underwriter.AssessmentRequest;
import com.slocpi.ium.util.IUMConstants;

/**
 * @author Cris
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class TransmitARToUSDTestCase extends TestCase {
	
	int i=0;
	ArrayList statusList = new ArrayList();
	
	/**
	 * Constructor for TransmitARToUSDTestCase.
	 * @param arg0
	 */
	public TransmitARToUSDTestCase(String arg0) {
		super(arg0);
	}
	
	public static Test suite() {
		TestSuite suite = new TestSuite(TransmitARToUSDTestCase.class);
		return suite;
	}

	public static void main(String[] args) {
		TestRunner.run(suite());
	}
	
	public void testTransmitToUSD()throws Exception{
		boolean result = false;
		setStatusList();
		
		AssessmentRequest ar = new AssessmentRequest();
//		AssessmentRequestData actualData = initializeARData();
				
//		String referenceNumber = actualData.getReferenceNumber();
		long status = (IUMConstants.STATUS_FOR_TRANSMITTAL_USD);
		String referenceNumber = "AR003";
		String assignee  = "user1";
		String updatedBy = "arDev";
		
		ar.changeStatus(referenceNumber,IUMConstants.STATUS_APPROVED,"user1");
		
		ar.transmit(referenceNumber,assignee,updatedBy);
		
		// add checking for insertion of the client data sheet
		if (checkStatus(referenceNumber,status) && checkAssignedTo(referenceNumber,assignee)){
			result = true;
		}
		assertEquals("Error in submitting request for approval: ", true,result);
	}

/*	
	public void testMultipleTransmitToUSD()throws Exception{
		boolean result = false;
		setStatusList();
		
		for (int index=0; index<statusList.size();index++){
			AssessmentRequest ar = new AssessmentRequest();
			AssessmentRequestData actualData = initializeARData();
				
			String referenceNumber = actualData.getReferenceNumber();
			long status = (IUMConstants.STATUS_FOR_TRANSMITTAL_USD);
			String assignee  = "user1";
			String updatedBy = "arDev";
		
			ar.transmit(referenceNumber,assignee,status,updatedBy);
			
			//	add checking for insertion of the client data sheet
			if (checkStatus(referenceNumber,status) && checkAssignedTo(referenceNumber,assignee)){
				result = true;
			}
			assertEquals("Error in submitting request for approval: ", true,result);
			i++;
		}
	}
*/	
	private AssessmentRequestData initializeARData() throws Exception{
		String referenceNumber = "01523631";
		String branchId = "SUNLIFEHO";
		String userId = "IUMDEV";
		String ownerId = "owner";
		String clientId = "insured";
		String agentId = "agent";
		String assigned = "IUMDEV";
		String underwriterId = "uw";
		
		long status = getStatus(i);
		
		AssessmentRequestData arData = new AssessmentRequestData();
		AssessmentRequest ar = new AssessmentRequest();
		
		arData = ar.getDetails(referenceNumber);
		
		if (arData.getReferenceNumber() != null){
			AssessmentRequestDAO dao = new AssessmentRequestDAO();
			dao.deleteAssessment(referenceNumber);
		}
		
		LOBData lobdata = new LOBData();
		lobdata.setLOBCode(IUMConstants.LOB_INDIVIDUAL_LIFE);
		
		SunLifeOfficeData branchData = new SunLifeOfficeData();
		branchData.setOfficeId(branchId);
		
		StatusData statData = new StatusData();
		statData.setStatusId(status);
		
		UserProfileData user = new UserProfileData();
		user.setUserId(userId);
		
		UserProfileData agent = new UserProfileData();
		agent.setUserId(agentId);
		
		UserProfileData assignedTo = new UserProfileData();
		assignedTo.setUserId(assigned);
		
		UserProfileData underwriter = new UserProfileData();
		underwriter.setUserId(underwriterId);
		
		ClientData owner = new ClientData();
		owner.setClientId(ownerId);
		
		ClientData client = new ClientData();
		client.setClientId(clientId);
		

		arData.setReferenceNumber(referenceNumber);
		arData.setLob(lobdata);
		arData.setBranch(branchData);
		arData.setStatus(statData);
		arData.setCreatedBy(user);
		arData.setOwner(owner);
		arData.setInsured(client);
		arData.setAgent(agent);
		arData.setAssignedTo(assignedTo);
		arData.setUnderwriter(underwriter);
		arData.setFolderLocation(agent);
		
		ar.createAssessmentRequest(arData);
		ar.changeStatus(referenceNumber,status,assigned);
		ar.getDetails(referenceNumber);
		return arData;
	}
	
	private boolean checkStatus (String referenceNumber, long expectedStatus) throws Exception{
		boolean result = false;
		AssessmentRequest ar = new AssessmentRequest();

		AssessmentRequestData arData = ar.getDetails(referenceNumber);
		if (expectedStatus == arData.getStatus().getStatusId()){
			result = true;
		}
		return result;
	}
	
	private boolean checkAssignedTo (String referenceNumber, String expectedUser) throws Exception{
		boolean result = false;
		AssessmentRequest ar = new AssessmentRequest();

		AssessmentRequestData arData = ar.getDetails(referenceNumber);
		if (expectedUser.equals(arData.getAssignedTo().getUserId())){
			result = true;
		}
		return result;
	}
	
	private void setStatusList(){
		statusList.add(0, String.valueOf(IUMConstants.STATUS_NB_REVIEW_ACTION));
		statusList.add(1, String.valueOf(IUMConstants.STATUS_AWAITING_MEDICAL));
		statusList.add(2, String.valueOf(IUMConstants.STATUS_AWAITING_REQUIREMENTS));
		statusList.add(3, String.valueOf(IUMConstants.STATUS_FOR_APPROVAL));
	}
	
	private long getStatus(int i){
		String status = (String) statusList.get(i);
		return Long.parseLong(status);
	}
}
