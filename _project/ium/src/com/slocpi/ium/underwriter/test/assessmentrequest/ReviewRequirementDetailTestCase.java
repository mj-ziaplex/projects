/*
 * Created on Jan 5, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.underwriter.test.assessmentrequest;

import java.util.Date;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import com.slocpi.ium.data.PolicyRequirementsData;
import com.slocpi.ium.underwriter.PolicyRequirements;
import com.slocpi.ium.util.ValueConverter;


/**
 * This test case will check the behaviour of the objects retrieved from
 * MQ.  This test case depends on the in records found in the ABACUS tables.
 * A method populateAbacus tables will be created.  
 * 
 * The policy and policy_queue table will be populated.
 * @author daguila
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class ReviewRequirementDetailTestCase extends TestCase {

	public ReviewRequirementDetailTestCase(java.lang.String testName) {        
		super(testName);    
	}        

	public static Test suite() {        
		TestSuite suite = new TestSuite(ReviewRequirementDetailTestCase.class);                
		return suite;    
	}	
	
	public static void main(java.lang.String[] args) {   
		junit.textui.TestRunner.run(suite());    
	}
	
	public void testRequirementDetails() throws Exception {
		boolean result = false;
		
		String refNo = "A1";
		long reqId = 0;
		PolicyRequirements pr = new PolicyRequirements();
	    PolicyRequirementsData reqData = pr.getRequirement(reqId);
        
        result = validateRequirementDetail(reqData);

		assertEquals("Requirement detail failed.",true,result);
	}
	
	private boolean validateRequirementDetail (PolicyRequirementsData reqData) throws Exception {
		boolean result = true;
		try {		
			String reqCode = reqData.getRequirementCode();
			long seqNo = reqData.getSequenceNumber();
			String reqDesc = reqData.getReqDesc();
			String level = reqData.getLevel();
			Date createDate = reqData.getCreateDate();
			boolean isComplete = reqData.isCompletedRequirementInd();
			long status = reqData.getStatus().getStatusId();
			Date statusDate = reqData.getStatusDate();
			String upDatedBy = reqData.getUpdatedBy();
			Date updatedDate = reqData.getUpdateDate();
			String clientType = reqData.getClientType();
			String designation = reqData.getDesignation();
			Date testDate = reqData.getTestDate();
			String testResult = reqData.getTestResultCode();
			String testDesc = reqData.getTestResultDesc();
			boolean isResolced = reqData.isResolveInd();
			long followUpNo = reqData.getFollowUpNumber();
			Date followUpDate = reqData.getFollowUpDate();
			Date validityDate = reqData.getValidityDate();
			boolean isPaid = reqData.isPaidInd();
			String newTest = ValueConverter.booleanToString(reqData.getNewTestOnly());
			Date orderDate = reqData.getOrderDate();
			Date dateSent = reqData.getDateSent();
			String ccasSuggest = ValueConverter.booleanToString(reqData.getCCASuggestInd());
			Date receivedDate = reqData.getReceiveDate();
			String autoOrder = ValueConverter.booleanToString(reqData.getAutoOrderInd());	
			String fldComment = ValueConverter.booleanToString(reqData.getFldCommentInd());
			String comment = reqData.getComments();
			String clientNo = reqData.getClientId();
		}
		catch (Exception e) {
			result = false;
		}
		return result;
	}

}
