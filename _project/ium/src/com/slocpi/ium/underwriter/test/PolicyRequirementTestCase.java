/**
 * CreateRequirementTestCase.java				Copyright (c) 2004 Pointwest Technologies Corp.
 * TODO DOCUMENT ME!
 * 
 * @author aditalo	 @date Jan 6, 2004
 */
package com.slocpi.ium.underwriter.test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import com.slocpi.ium.data.AssessmentRequestData;
import com.slocpi.ium.data.LOBData;
import com.slocpi.ium.data.PolicyRequirementsData;
import com.slocpi.ium.data.StatusData;
import com.slocpi.ium.data.UserProfileData;
import com.slocpi.ium.data.util.DataSourceProxy;
import com.slocpi.ium.underwriter.PolicyRequirements;
import com.slocpi.ium.util.DateHelper;
import com.slocpi.ium.util.IUMConstants;

/**
 * TODO DOCUMENT ME!
 * 
 * @author aditalo		Copyright (c) 2004 Pointwest Technologies Corp.
 * @date Jan 6, 2004
 */
public class PolicyRequirementTestCase extends TestCase{

	private static long requirementID = 0;
	private PolicyRequirements document = new PolicyRequirements();
	private AssessmentRequestData request = new AssessmentRequestData();
	private LOBData lob = new LOBData();
	private UserProfileData user = new UserProfileData();
	private final int CREATE_REQUIREMENTS = 1;
	private final int ORDER_REQUIREMENTS = 2;
	private final int FOLLOW_UP_REQUIREMENTS = 3;
	private final int RECEIVE_REQUIREMENTS = 4;
	private final int ACCEPT_REQUIREMENTS = 5;
	
	public PolicyRequirementTestCase(java.lang.String testName) {        
		super(testName);    
	}        

	public static Test suite() {        
		TestSuite suite = new TestSuite(PolicyRequirementTestCase.class);                
		return suite;    
	}

	public static void main(String[] args) {
		junit.textui.TestRunner.run(suite());    
	}

	public void testCreateRequirement() {
		try {
			lob.setLOBCode(IUMConstants.LOB_INDIVIDUAL_LIFE);
			request.setLob(lob);
			requirementID = document.createRequirement(this.requirementData(), request, user.getUserId());
		} catch (Exception e) {
			e.printStackTrace();
		}
		assertEquals("testCreateRequirement: ",true,this.compare(CREATE_REQUIREMENTS, this.requirementData(),this.getRequirement()));
	}
	
	public void testOrderRequirements() {
		ArrayList list = new ArrayList();
		PolicyRequirementsData data = this.getRequirement();
		list.add(data);
		try {
			lob.setLOBCode(IUMConstants.LOB_INDIVIDUAL_LIFE);
			request.setLob(lob);
			user.setUserId("IUMDEV");
			request.setUpdatedBy(user);
			document.orderRequirement(list, request, user.getUserId());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		assertEquals("testOrderRequirements: ",true,true);
	}
	
	public void testFollowUpRequirements() {
		ArrayList list = new ArrayList();
		PolicyRequirementsData data = this.getRequirement();
		list.add(data);
		try {
			lob.setLOBCode(IUMConstants.LOB_INDIVIDUAL_LIFE);
			request.setLob(lob);
			user.setUserId("IUMDEV");
			request.setUpdatedBy(user);
//			document.sendRequirementNotification(list, request);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		assertEquals("testFollowUpRequirements: ",true,true);
	}
	
	public void testReceiveRequirements() {
		ArrayList list = new ArrayList();
		PolicyRequirementsData data = this.getRequirement();
		list.add(data);
		try {
			lob.setLOBCode(IUMConstants.LOB_INDIVIDUAL_LIFE);
			request.setLob(lob);
			user.setUserId("IUMDEV");
			request.setUpdatedBy(user);
			document.receiveRequirement(list, request);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		assertEquals("testReceiveRequirements: ",true,true);
	}
	
	public void testAcceptRequirements() {
		ArrayList list = new ArrayList();
		PolicyRequirementsData data = this.getRequirement();
		list.add(data);
		try {
			lob.setLOBCode(IUMConstants.LOB_INDIVIDUAL_LIFE);
			request.setLob(lob);
			user.setUserId("IUMDEV");
			request.setUpdatedBy(user);
			document.acceptRequirement(list, request);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		assertEquals("testAcceptRequirements: ",true,true);
	}

	private PolicyRequirementsData requirementData(){
		PolicyRequirementsData data = new PolicyRequirementsData();
		data.setReferenceNumber("A1");
//		data.setRequirementId(1);
		data.setSequenceNumber(1);
		data.setRequirementCode("R001");
		data.setLevel("P");
//		data.setABACUScreateDate(DateHelper.parse("01/01/2004","MM/dd/yyyy"));
//		data.setCompletedRequirementInd(true);
		StatusData statusData = new StatusData();
		statusData.setStatusId(IUMConstants.STATUS_NTO);
		data.setStatus(statusData);
		data.setStatusDate(DateHelper.parse("01/01/2004","MM/dd/yyyy"));
//		data.setABACUSUpdatedBy("1");
//		data.setABACUSUpdateDate(DateHelper.parse("01/01/2004","MM/dd/yyyy"));
		data.setClientType("O");
		data.setDesignation("DESIGNTION");
		data.setTestDate(DateHelper.parse("01/01/2004","MM/dd/yyyy"));
		data.setTestResultCode("TEST");
		data.setTestResultDesc("TEST RESULT DESCRIPTION");
//		data.setResolveInd(true);
		data.setFollowUpNumber(0);
//		data.setFollowUpDate(DateHelper.parse("01/01/2004","MM/dd/yyyy"));
//		data.setValidityDate(DateHelper.parse("01/01/2004","MM/dd/yyyy"));
//		data.setPaidInd(true);
//		data.setNewTestOnly("0");
//		data.setOrderDate(DateHelper.parse("01/01/2004","MM/dd/yyyy"));
		data.setDateSent(DateHelper.parse("01/01/2004","MM/dd/yyyy"));
		data.setCCASuggestInd(true);
//		data.setReceiveDate(DateHelper.parse("01/01/2004","MM/dd/yyyy"));
		data.setAutoOrderInd(false);
		data.setFldCommentInd(false);
		data.setComments("COMMENTS");
		data.setCreatedBy("IUMDEV");
		data.setCreateDate(new Date());
		data.setUpdatedBy("IUMDEV");
		data.setUpdateDate(new Date());
		return data;
	}

	private PolicyRequirementsData getRequirement(){
		PolicyRequirementsData data = new PolicyRequirementsData();
		String sql = "SELECT UAR_REFERENCE_NUM, PR_REQUIREMENT_ID, PR_ADMIN_SEQUENCE_NUM, PR_REQT_CODE, PR_LEVEL" +
		",PR_ADMIN_CREATE_DATE, PR_COMPLT_REQT_IND, PR_STATUS_ID, PR_STATUS_DATE, PR_ADMIN_UPDTD_BY" +
		",PR_ADMIN_UPDTD_DATE, PR_CLIENT_TYPE, PR_DESIGNATION, PR_TEST_DATE, PR_TEST_RESULT_CODE" +
		",PR_TEST_RESULT_DESC, PR_RESOLVED_IND, PR_FOLLOW_UP_NUM, PR_FOLLOW_UP_DATE, PR_VALIDITY_DATE" +
		",PR_PAID_IND, PR_NEW_TEST_ONLY, PR_ORDER_DATE, PR_DATE_SENT, PR_CCAS_SUGGEST_IND" +
		",PR_RECEIVED_DATE, PR_AUTO_ORDER_IND, PR_FLD_COMMENT_IND, PR_COMMENTS" +
		",CREATED_BY, CREATED_DATE, UPDATED_BY, UPDATED_DATE" +
		" FROM POLICY_REQUIREMENTS WHERE PR_REQUIREMENT_ID = ?";
		try {
			Connection reqmtConn = new DataSourceProxy().getConnection();
			PreparedStatement prepStmt = reqmtConn.prepareStatement(sql);
			prepStmt.setLong(1,requirementID);
			ResultSet rs = prepStmt.executeQuery();
			if (rs.next()) {
				data.setReferenceNumber(rs.getString("UAR_REFERENCE_NUM"));
				data.setRequirementId(rs.getLong("PR_REQUIREMENT_ID"));
				data.setSequenceNumber(rs.getLong("PR_ADMIN_SEQUENCE_NUM"));
				data.setRequirementCode(rs.getString("PR_REQT_CODE"));
				data.setLevel(rs.getString("PR_LEVEL"));
				data.setABACUScreateDate(rs.getDate("PR_ADMIN_CREATE_DATE"));
				data.setCompletedRequirementInd(this.stringToBoolean(rs.getString("PR_COMPLT_REQT_IND")));
				StatusData statusData = new StatusData();
				statusData.setStatusId(rs.getLong("PR_STATUS_ID"));
				data.setStatus(statusData);
				data.setStatusDate(rs.getDate("PR_STATUS_DATE"));
				data.setABACUSUpdatedBy(rs.getString("PR_ADMIN_UPDTD_BY"));
				data.setABACUSUpdateDate(rs.getDate("PR_ADMIN_UPDTD_DATE"));
				data.setClientType(rs.getString("PR_CLIENT_TYPE"));
				data.setDesignation(rs.getString("PR_DESIGNATION"));
				data.setTestDate(rs.getDate("PR_TEST_DATE"));
				data.setTestResultCode(rs.getString("PR_TEST_RESULT_CODE"));
				data.setTestResultDesc(rs.getString("PR_TEST_RESULT_DESC"));
				data.setResolveInd(this.stringToBoolean(rs.getString("PR_RESOLVED_IND")));
				data.setFollowUpNumber(rs.getLong("PR_FOLLOW_UP_NUM"));
				data.setFollowUpDate(rs.getDate("PR_FOLLOW_UP_DATE"));
				data.setValidityDate(rs.getDate("PR_VALIDITY_DATE"));
				data.setPaidInd(this.stringToBoolean(rs.getString("PR_PAID_IND")));
				data.setNewTestOnly(rs.getBoolean("PR_NEW_TEST_ONLY"));
				data.setOrderDate(rs.getDate("PR_ORDER_DATE"));
				data.setDateSent(rs.getDate("PR_DATE_SENT"));
				data.setCCASuggestInd(rs.getBoolean("PR_CCAS_SUGGEST_IND"));
				data.setReceiveDate(rs.getDate("PR_RECEIVED_DATE"));
				data.setAutoOrderInd(rs.getBoolean("PR_AUTO_ORDER_IND"));
				data.setFldCommentInd(rs.getBoolean("PR_FLD_COMMENT_IND"));
				data.setComments(rs.getString("PR_COMMENTS"));
				data.setCreatedBy(rs.getString("CREATED_BY"));
				data.setCreateDate(rs.getDate("CREATED_DATE"));
				data.setUpdatedBy(rs.getString("UPDATED_BY"));
				data.setUpdateDate(rs.getDate("UPDATED_DATE"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return data;
	}

	private boolean compare(int useCase, PolicyRequirementsData in1, PolicyRequirementsData in2) {
		if (!in1.getReferenceNumber().equals(in2.getReferenceNumber())) {
			return false;
		}
//		if (in1.getRequirementId() != in2.getRequirementId()) {
//			return false;
//		}
		if (in1.getSequenceNumber() != in2.getSequenceNumber()) {
			return false;
		}
		if (!in1.getRequirementCode().equals(in2.getRequirementCode())) {
			return false;
		}
		if (!in1.getLevel().equals(in2.getLevel())) {
			return false;
		}
//		if (!in1.getABACUScreateDate().equals(in2.getABACUScreateDate())) {
//			return false;
//		}
//		if (in1.isCompletedRequirementInd() != in2.isCompletedRequirementInd()) {
//			return false;
//		}
		if (in1.getStatus().getStatusId() != in2.getStatus().getStatusId()) {
			return false;
		}
		if (!in1.getStatusDate().equals(in2.getStatusDate())) {
			return false;
		}
//		if (in1.getABACUSUpdatedBy() != in2.getABACUSUpdatedBy()) {
//			return false;
//		}
//		if (!in1.getABACUSUpdateDate().equals(in2.getABACUSUpdateDate())) {
//			return false;
//		}
		if (!in1.getClientType().equals(in2.getClientType())) {
			return false;
		}
		if (!in1.getDesignation().equals(in2.getDesignation())) {
			return false;
		}
		if (!in1.getTestDate().equals(in2.getTestDate())) {
			return false;
		}
		if (!in1.getTestResultCode().equals(in2.getTestResultCode())) {
			return false;
		}
		if (!in1.getTestResultDesc().equals(in2.getTestResultDesc())) {
			return false;
		}
//		if (in1.isResolveInd() != in2.isResolveInd()) {
//			return false;
//		}
		if (in1.getFollowUpNumber() != in2.getFollowUpNumber()) {
			return false;
		}
//		if (!in1.getFollowUpDate().equals(in2.getFollowUpDate())) {
//			return false;
//		}
//		if (!in1.getValidityDate().equals(in2.getValidityDate())) {
//			return false;
//		}
//		if (in1.isPaidInd() != in2.isPaidInd()) {
//			return false;
//		}
//		if (!in1.getNewTestOnly().equals(in2.getNewTestOnly())) {
//			return false;
//		}
//		if (!in1.getOrderDate().equals(in2.getOrderDate())) {
//			return false;
//		}
		if (!DateHelper.isEqual(in1.getDateSent(), in2.getDateSent())) {
			return false;
		}
		if (in1.getCCASuggestInd() != in2.getCCASuggestInd()) {
			return false;
		}
//		if (!in1.getReceiveDate().equals(in2.getReceiveDate())) {
//			return false;
//		}
		if (in1.getAutoOrderInd() != in2.getAutoOrderInd()) {
			return false;
		}
		if (in1.getFldCommentInd() != in2.getFldCommentInd()) {
			return false;
		}
		if (!in1.getComments().equals(in2.getComments())) {
			return false;
		}
		if (!in1.getCreatedBy().equals(in2.getCreatedBy())) {
			return false;
		}
		if (!DateHelper.isEqual(in1.getCreateDate(), in2.getCreateDate())) {
			return false;
		}
		if (!in1.getUpdatedBy().equals(in2.getUpdatedBy())) {
			return false;
		}
		if (!DateHelper.isEqual(in1.getUpdateDate(), in2.getUpdateDate())) {
			return false;
		}
		return true;
	}

	private boolean stringToBoolean(String string) {
		if (string.equalsIgnoreCase("Y")) {
			return true;
		} else {
			return false;
		}
		
	}

}
