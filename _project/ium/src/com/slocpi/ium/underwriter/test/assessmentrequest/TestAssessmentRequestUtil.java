/*
 * Created on Feb 2, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.underwriter.test.assessmentrequest;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import com.slocpi.ium.data.AssessmentRequestData;
import com.slocpi.ium.data.ClientData;
import com.slocpi.ium.data.LOBData;
import com.slocpi.ium.data.StatusData;
import com.slocpi.ium.data.SunLifeOfficeData;
import com.slocpi.ium.data.UserProfileData;
import com.slocpi.ium.data.dao.AssessmentRequestDAO;
import com.slocpi.ium.data.util.DataSourceProxy;
import com.slocpi.ium.underwriter.AssessmentRequest;
import com.slocpi.ium.util.IUMConstants;

/**
 * @author Cris
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class TestAssessmentRequestUtil {
	
	public ArrayList createAssessmentTestData() throws SQLException, IOException{
		ArrayList arList = createData();
		
		AssessmentRequestDAO dao = new AssessmentRequestDAO();
		for (int i=0; i<arList.size(); i++){
			AssessmentRequestData arData =  (AssessmentRequestData) arList.get(i);
			String referenceNumber = arData.getReferenceNumber();
			dao.deleteAssessment(referenceNumber);
			dao.insertAssessmentRequest(arData);
		}
		return arList;
	}
	
	private ArrayList generateReferenceNumber(int number){
		ArrayList refNum = new ArrayList(number);
		for (int i =0; i<number; i++){
			String reference = "AR00" + i;
			refNum.add(reference);
		}
		return refNum;
	}
	
	private ArrayList createData(){
		String branchId = "LEGASPISO";
		String userId = "IUMDEV";
		String ownerId = "owner";
		String clientId = "insured";
		String agentId = "agent";
		String assigned = "WFUser1";
		String underwriterId = "uw";
		
		ArrayList arList = new ArrayList();
		long [] statusList = getStatusData();
		int numberOfAssessment = statusList.length;
		ArrayList refNumList = generateReferenceNumber(numberOfAssessment);
		for (int i = 0; i<numberOfAssessment; i++){
			AssessmentRequestData arData = new AssessmentRequestData();
			
			StatusData status = new StatusData();
			status.setStatusId(statusList[i]);
			
			LOBData lob = new LOBData();
			lob.setLOBCode(IUMConstants.LOB_INDIVIDUAL_LIFE);
			
			SunLifeOfficeData branchData = new SunLifeOfficeData();
			branchData.setOfficeId(branchId);
		
			UserProfileData user = new UserProfileData();
			user.setUserId(userId);
		
			UserProfileData agent = new UserProfileData();
			agent.setUserId(agentId);
		
			UserProfileData assignedTo = new UserProfileData();
			assignedTo.setUserId(assigned);
		
			UserProfileData underwriter = new UserProfileData();
			underwriter.setUserId(underwriterId);
		
			ClientData owner = new ClientData();
			owner.setClientId(ownerId);
		
			ClientData client = new ClientData();
			client.setClientId(clientId);
		
			arData.setReferenceNumber((String) refNumList.get(i));
			arData.setLob(lob);
			arData.setBranch(branchData);
			arData.setStatus(status);
			arData.setCreatedBy(user);
			arData.setOwner(owner);
			arData.setInsured(client);
			arData.setAgent(agent);
			arData.setAssignedTo(assignedTo);
			arData.setUnderwriter(underwriter);
			arData.setFolderLocation(agent);
			arData.setApplicationReceivedDate(new java.util.Date());
			arData.setForwardedDate(new java.util.Date());
			
			arList.add(arData);
		}
		return arList;
	}
	
	private long[] getStatusData(){
		long [] status = new long [9];
		status[0] = IUMConstants.STATUS_NB_REVIEW_ACTION;
		status[1] = IUMConstants.STATUS_APPROVED;
		status[2] = IUMConstants.STATUS_AWAITING_REQUIREMENTS;
		status[3] = IUMConstants.STATUS_AWAITING_MEDICAL;
		status[4] = IUMConstants.STATUS_FOR_ASSESSMENT;
		status[5] = IUMConstants.STATUS_CANCELLED;
		status[6] = IUMConstants.STATUS_FOR_APPROVAL;
		status[7] = IUMConstants.STATUS_FOR_FACILITATOR_ACTION;
		status[8] = IUMConstants.STATUS_NOT_PROCEEDED_WITH;
		return status;
	}
	
	public static void main(String[] args) throws SQLException, IOException{
		TestAssessmentRequestUtil test = new TestAssessmentRequestUtil();
		test.createAssessmentTestData();
	}
}

	