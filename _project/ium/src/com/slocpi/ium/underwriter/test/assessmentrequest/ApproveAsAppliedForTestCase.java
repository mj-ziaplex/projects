/*
 * Created on Jan 23, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.underwriter.test.assessmentrequest;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import junit.textui.TestRunner;

import com.slocpi.ium.underwriter.AssessmentRequest;

/**
 * @author Cris
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class ApproveAsAppliedForTestCase extends TestCase {

	/**
	 * Constructor for ApproveAsAppliedForTestCase.
	 * @param arg0
	 */
	public ApproveAsAppliedForTestCase(String arg0) {
		super(arg0);
	}

	public static Test suite() {
		TestSuite suite = new TestSuite(ApproveAsAppliedForTestCase.class);
		return suite;
	}

	public static void main(String[] args) {
		TestRunner.run(suite());
	}
	
	public void testApproveRequestNormal()throws Exception{
		AssessmentRequest ar = new AssessmentRequest();
		ar.changeStatus("AR005",5,"user1");
		ar.approveRequestAsAppliedFor("AR005","user1");
	}

}
