/*
 * Created on Jun 21, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.underwriter.test;

import java.util.ArrayList;
import java.util.Date;

import junit.framework.TestCase;

import com.slocpi.ium.IUMException;
import com.slocpi.ium.data.MedicalNotes;
import com.slocpi.ium.interfaces.notification.IUMNotificationException;
import com.slocpi.ium.underwriter.DoctorsNotes;
import com.slocpi.ium.underwriter.UnderWriterException;

/**
 * @author Jeffrey Canlas
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class DoctorsNotesTest extends TestCase {

	public DoctorsNotesTest(String arg0) {
		super(arg0);
	}

	public void test() throws UnderWriterException, IUMException, IUMNotificationException {
	
	ArrayList med = new ArrayList();
	MedicalNotes note = new MedicalNotes();
	DoctorsNotes newnotes = new DoctorsNotes();
	Date newdate = new Date();
	
	
	
	
	/*Test if size of array containing medNotesList 
	 * is greater than zero given referenceID
	 * "014164733". referenceID is valid thus 
	 * size of array containing MedNotesList
	 * should be greater than zero. 
	 */
	med=newnotes.getDoctorsNotesList("014164733");
	System.out.println(med);
	assertTrue(med.size()>0);
	
	/* Test of size of array containing medNOtesList is equal
	 * to zero given a non existing referenceID
	 */
	med=newnotes.getDoctorsNotesList("adlgjkhadg");
	System.out.println(med);
	assertTrue(med.size()==0);
	
	
	/* Test insertion method
	 * 
	 */
	note.setNotes("insert1");
	note.setPostDate(newdate);
	note.setPostedBy("1");
	note.setReferenceNumber("YVES123");
	
	newnotes.insertDoctorsNote(note, "2");
 	
	

    /*Test if we can "get" inserted data
     * also, test if element of the retrieved data is the
     * same as the once inserted
    */
    
     med=newnotes.getDoctorsNotesList("YVES123");
     MedicalNotes tmp = (MedicalNotes)med.get(0);
     String testnotes = tmp.getNotes();
	 String testposted = tmp.getPostedBy();
	 String testrecipient = tmp.getRecipient();
	 String testrefnum = tmp.getReferenceNumber();
     assertTrue(testnotes.equals("insert1"));
	 assertTrue(testrefnum.equals("YVES123"));
	}

		
}
