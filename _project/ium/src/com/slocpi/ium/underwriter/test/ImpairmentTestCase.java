/**
 * GetImpairmentListTestCase.java				Copyright (c) 2004 Pointwest Technologies Corp.
 * TODO DOCUMENT ME!
 * 
 * @author aditalo	 @date Jan 14, 2004
 */
package com.slocpi.ium.underwriter.test;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import com.slocpi.ium.data.ImpairmentData;
import com.slocpi.ium.underwriter.Impairment;
import com.slocpi.ium.util.DateHelper;

/**
 * TODO DOCUMENT ME!
 * 
 * @author aditalo		Copyright (c) 2004 Pointwest Technologies Corp.
 * @date Jan 14, 2004
 */
public class ImpairmentTestCase extends TestCase{


	public ImpairmentTestCase(java.lang.String testName) {        
		super(testName);    
	}        

	public static Test suite() {        
		TestSuite suite = new TestSuite(ImpairmentTestCase.class);                
		return suite;    
	}

	public static void main(String[] args) {
		junit.textui.TestRunner.run(suite());    
	}

	public void testExportMIBImpairments() {
		Impairment impairment = new Impairment();
		try {
			ArrayList list = impairment.getExportList(DateHelper.parse("01/01/2004", "MM/dd/yyyy"), new Date());
			Iterator iList = list.iterator();
			while (iList.hasNext()) {
				ImpairmentData data = (ImpairmentData) iList.next();
				data.setExportDate(new Date());
			}
			impairment.changeMIBStatus(list);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		assertEquals("testExportMIBImpairments: test ok.",true,true);
	}
}
