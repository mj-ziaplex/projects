package com.slocpi.ium.underwriter.test;

import java.util.ArrayList;

import junit.framework.TestCase;

import com.slocpi.ium.IUMException;
import com.slocpi.ium.data.MedExamFiltersData;
import com.slocpi.ium.data.MedicalRecordData;
import com.slocpi.ium.underwriter.MedicalLabRecord;
import com.slocpi.ium.util.IUMConstants;
import com.slocpi.ium.util.SortHelper;

/**
 * TODO DOCUMENT ME!
 * 
 * @author Raymond V. Posadas		Copyright (c) 2004 Pointwest Technologies Corp.
 * @date Jan 23, 2004
 */
public class MedicalLabRecordTestCase extends TestCase {

    /**
     * Constructor for MedicalLabRecordTest.
     * @param arg0
     */
    public MedicalLabRecordTestCase(String arg0) {
        super(arg0);
    }

    public static void main(String[] args) {
        junit.textui.TestRunner.run(MedicalLabRecordTestCase.class);
    }

//    /*
//     * @see TestCase#setUp()
//     */
//    protected void setUp() throws Exception {
//        super.setUp();
//    }
//
//    /*
//     * @see TestCase#tearDown()
//     */
//    protected void tearDown() throws Exception {
//        super.tearDown();
//    }

    public void testGetMedicalExamRequest() {
        MedicalLabRecord labRec = new MedicalLabRecord();
        MedExamFiltersData filter = new MedExamFiltersData();
        ArrayList labRecHolder = new ArrayList();
        
        filter.setReferenceNo("A4");
//        filter.setClientNo("C01");
//        filter.setLastName("Ballesteros");
//        filter.setFirstName("");
//		filter.setStatus("20");
//        filter.setSevenDayMemoDate(DateHelper.sqlDate(new Date()));
//		filter.setDateRequested(DateHelper.sqlDate(new Date()));
//        filter.setStartFollowUpDate(DateHelper.sqlDate(new Date()));
//        filter.setEndFollowUpDate(DateHelper.sqlDate(new Date()));
//        filter.setLaboratory("");
//        filter.setExaminer("");
//        filter.setUnAssignedOnly("");
		SortHelper sort = new SortHelper(IUMConstants.TABLE_MEDICAL_RECORDS);
        try {
            labRecHolder = labRec.getMedicalExamRequest(filter, sort);
        } catch (IUMException e) {
            e.printStackTrace();
        }
        
        for (int i=0; i<labRecHolder.size(); i++){
        	assertTrue("Is an instance of MedicalRecordData.", labRecHolder.get(i) instanceof MedicalRecordData);
        	
        	MedicalRecordData rData = (MedicalRecordData) labRecHolder.get(i); 
        	System.out.println(rData.getClient().getClientId());
        	assertTrue("Test 2", rData.getClient().getClientId().equals("C01") );
        	System.out.println(rData.getExaminer().getExaminerId());
        }
        
    }

    public void testCreateMedicalExam() {
        //TODO Implement createMedicalExam().
    }

    public void testGetMedicalExamDetails() {
		MedicalLabRecord labRec = new MedicalLabRecord();
		MedExamFiltersData filter = new MedExamFiltersData();
		ArrayList labRecHolder = new ArrayList();
		
		filter.setReferenceNo("A4");
		
		try {
            labRecHolder = labRec.getMedicalExamDetails(filter);
        } catch (IUMException e) {
            e.printStackTrace();
        }
        assertTrue("Test 3", labRecHolder.size() > 0 );
        MedicalRecordData record = (MedicalRecordData) labRecHolder.get(0);
        assertTrue("Test 4", record.getMedicalRecordId() == new Long(4).longValue() );
        
    }

    public void testGetMedicalRecord() {
        //TODO Implement getMedicalRecord().
    }

    public void testReceiveMedicalExamResult() {
        //TODO Implement receiveMedicalExamResult().
    }

    public void testUpdateMedExamNoSubmission() {
        //TODO Implement updateMedExamNoSubmission().
    }

    public void testGetMedicalLabRecords() {
        //TODO Implement getMedicalLabRecords().
    }

    public void testUpdateChargeTo() {
        //TODO Implement updateChargeTo().
    }

    public void testSetCancelled() {
        //TODO Implement setCancelled().
    }

    public void testConfirmMedicalExam() {
        //TODO Implement confirmMedicalExam().
    }

}
