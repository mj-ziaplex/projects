/**
 * Document.java				Copyright (c) 2004 Pointwest Technologies Corp.
 * TODO DOCUMENT ME!
 * 
 * @author aditalo	 @date Jan 6, 2004
 */
package com.slocpi.ium.underwriter;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.slocpi.ium.IUMException;
import com.slocpi.ium.data.DocumentData;
import com.slocpi.ium.data.DocumentTypeData;
import com.slocpi.ium.data.dao.DocumentDAO;
import com.slocpi.ium.data.util.DataSourceProxy;
import com.slocpi.ium.util.CodeHelper;

/**
 * This is the business object for the Document object that contains retrieval of Documents and creation of 
 * documents.
 * @author tvicencio		Copyright (c) 2004 Pointwest Technologies Corp.
 * @date Jan 6, 2004
 */
public class Document {
	private static final Logger LOGGER = LoggerFactory.getLogger(Document.class);
	/**
	 * Retrieves the documents for this reference number.
	 * @param referenceNumber
	 * @return ArrayList of DocumentData
	 */	
	public ArrayList getDocuments(String referenceNumber) throws UnderWriterException, IUMException {
		
		LOGGER.info("getDocuments start");
		Connection conn = null;
		try {
			conn = new DataSourceProxy().getConnection();
			ArrayList docList = new ArrayList();
			DocumentDAO docDAO = new DocumentDAO(conn);
			docList = docDAO.retrieveDocuments(referenceNumber);
			return docList;
		}
		catch (SQLException ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		catch (Exception ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		finally{
			closeConn(conn);
			LOGGER.info("getDocuments end");
		}
	}// getDocuments
	

	/**
	 * Creates a document record in the FOLDER_DOCUMENTS record.
	 * @param documentData
	 */
	public void createDocument(DocumentData documentData) throws UnderWriterException, IUMException {	
		
		LOGGER.info("createDocument start");
		Connection conn = null;
		try {
			
			String refNo = documentData.getRefNo();	
			DocumentTypeData document = documentData.getDocument();
			String documentCode = document.getDocCode();
			String documentDesc = document.getDocDesc();
			Date dateReceived = documentData.getDateReceived();
			String receivedBy = documentData.getReceivedBy();
			Date dateRequested = documentData.getDateRequested();

			if (refNo == null) {
				LOGGER.warn("Reference Number is required.");
				throw new UnderWriterException("Reference Number is required.");
			}

			if (documentCode == null) {
				LOGGER.warn("Document code is required.");
				throw new UnderWriterException("Document code is required.");
			}

			if (dateReceived == null) {
				LOGGER.warn("Date Received is required.");
				throw new UnderWriterException("Date Received is required.");
			}
			
			if (receivedBy == null) {
				LOGGER.warn("Received by is required.");
				throw new UnderWriterException("Received by is required.");
			}
			
			if (dateRequested == null){
				LOGGER.error("Date Requested is required");
				throw new UnderWriterException("Date Requested is required");
			}

			conn = new DataSourceProxy().getConnection();
			DocumentDAO docDAO = new DocumentDAO(conn);  
			boolean isDuplicate = docDAO.isDuplicateCode(refNo, documentCode);
			
			if (isDuplicate) {
				LOGGER.debug("Duplicate document : " + documentDesc);
				throw new UnderWriterException("Duplicate document : " + documentDesc);
			}	
			conn.setAutoCommit(false);
			docDAO.insertDocument(documentData);
			conn.commit();
					
		}
		catch (UnderWriterException ex) {
			throw new UnderWriterException(ex);
		}
		catch (SQLException ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			try {
				if (conn != null) {
					conn.rollback();
				}				
			} catch (SQLException e) {
				LOGGER.error(CodeHelper.getStackTrace(e));
			}
			throw new IUMException(ex);
		}
		catch (Exception ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		finally{
			closeConn(conn);
			LOGGER.info("createDocument end");
		}
	}// insertDocument


	
	private void closeConn(Connection conn)throws IUMException{
		
		LOGGER.info("closeConn start");
		try{
			if (conn != null){
				conn.close();
				return;
			}
		}
		catch (Exception ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		LOGGER.info("closeConn end");
	}
	
}
