/*
 * Created on Dec 17, 2003
 *
 */
package com.slocpi.ium.underwriter;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.slocpi.ium.IUMException;
import com.slocpi.ium.data.AssessmentRequestData;
import com.slocpi.ium.data.AuditTrailData;
import com.slocpi.ium.data.ImpairmentData;
import com.slocpi.ium.data.StatusData;
import com.slocpi.ium.data.dao.AssessmentRequestDAO;
import com.slocpi.ium.data.dao.AuditTrailDAO;
import com.slocpi.ium.data.dao.ImpairmentDAO;
import com.slocpi.ium.data.util.DataSourceProxy;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.IUMConstants;

/**
 * @author Engel
 * 
 */
public class Impairment {
	private static final Logger LOGGER = LoggerFactory.getLogger(Impairment.class);

	/**
	 * Retrieves list of impairment for this reference number and client type
	 * @param referenceNumber 
	 * @param clientType 
	 * @return ArrayList of ImpairmentData
	 * @throws UnderWriterException
	 * @throws IUMException
	 */
	public ArrayList getImpairmentList(
			final String referenceNumber,
			final String clientType) throws UnderWriterException, IUMException {
		
		LOGGER.info("getImpairmentList start");
		Connection conn = null;
		
		try {
			conn = new DataSourceProxy().getConnection();
			ArrayList impairments = new ArrayList(); 
			return (impairments);
		}
		catch (Exception ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		finally {
			closeConn(conn);
			LOGGER.info("getImpairmentList end");
		}
	}//getImpairmentList
	
	
	/**
	 * Creates an impairment record.
	 * @param impairmentData
	 */
	
	public void createImpairment(ImpairmentData impairmentData, boolean wms) throws UnderWriterException, IUMException {
		
		LOGGER.info("createImpairment start");
		Connection conn = null;
		try {
			
			String impairmentCode = impairmentData.getImpairmentCode();
			String refNo = impairmentData.getReferenceNumber();
			String clientNo = impairmentData.getClientNumber();
			
			if (clientNo == null) {
				throw new UnderWriterException("Client number is required.");
			}
			
			if (refNo == null) {
				throw new UnderWriterException("Policy number is required.");
			}

			if (impairmentCode == null) {
				throw new UnderWriterException("Impairment code is required.");
			}

			if (impairmentData.getRelationship() == null) {
				throw new UnderWriterException("Relationship is required.");
			}
			
			if (impairmentData.getImpairmentOrigin() == null) {
				throw new UnderWriterException("Impairment origin is required.");
			}

			if (impairmentData.getImpairmentDate() == null) {
				throw new UnderWriterException("Impairment date is required.");
			}

			if (impairmentData.getCreatedBy() == null) {
				throw new UnderWriterException("Created by is required.");
			}

			if (impairmentData.getCreateDate() == null) {
				throw new UnderWriterException("Created date is required.");
			}

			conn = new DataSourceProxy().getConnection();
			ImpairmentDAO impDAO = new ImpairmentDAO(conn);  
			boolean isDuplicate = impDAO.isDuplicateCode(impairmentCode, refNo, clientNo);
			
			
			if (isDuplicate) {
				LOGGER.warn("Duplicate impairment code : " + impairmentCode);
				throw new UnderWriterException("Duplicate impairment code : " + impairmentCode);
			}
			
			AssessmentRequestDAO arDAO = new AssessmentRequestDAO();
			AssessmentRequestData arData = arDAO.retrieveRequestDetail(refNo);
			
			
			StatusData status = arData.getStatus(); 
			if (status.getStatusId() != IUMConstants.STATUS_UNDERGOING_ASSESSMENT && !wms) {
				LOGGER.warn("Impairments can only be created for requests with status 'Undergoing Assessment'");
				throw new UnderWriterException("Impairments can only be created for requests with status 'Undergoing Assessment'.");
			}
			conn.setAutoCommit(false);
			/*impDAO.insertImpairment(impairmentData);*/
			conn.commit();
			
		}
		catch (UnderWriterException ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new UnderWriterException(ex);		
		}		
		catch (SQLException ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			try {
				if (conn != null) {
					conn.rollback();
				}			
			} catch (SQLException e) {
				LOGGER.error(CodeHelper.getStackTrace(e));
			}
			throw new IUMException(ex);
		}
		catch (Exception ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		finally {
			closeConn(conn);
			LOGGER.info("createImpairment end");
		}
	}//createImpairment
	
	
	/**
	 * Retrieves list of impairments where its created date is within the date range given. 
	 * @param startDate
	 * @param endDate
	 * @return ArrayList of ImpairmentData
	 * @throws UnderWriterException
	 * @throws IUMException
	 */
	public ArrayList getExportList(Date startDate, Date endDate) throws UnderWriterException, IUMException {
		
		LOGGER.info("getExportList start");
		Connection conn = null;
		try {
			
			conn = new DataSourceProxy().getConnection();
			ImpairmentDAO dao = new ImpairmentDAO(conn);  
			ArrayList impairments = dao.retrieveImpairmentsForExport(startDate, endDate);
			
			return (impairments);
		}		
		catch (SQLException ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		catch (Exception ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		finally {
			closeConn(conn);
			LOGGER.info("getExportList end");
		}
	}//getExportList
	
	
	/**
	 * Updates the export date of the ImpairmentData contained in the ArrayList
	 * @param impairments arraylist of ImpairmentData
	 * @throws IUMException
	 */
	public void changeMIBStatus(ArrayList impairments) throws IUMException {
		
		LOGGER.info("changeMIBStatus start");
		Connection conn = null;
		try {			
			
			conn = new DataSourceProxy().getConnection();
			Iterator iImpairments = impairments.iterator();
			ImpairmentDAO dao = new ImpairmentDAO(conn);
			conn.setAutoCommit(false);
			while (iImpairments.hasNext()) {
				ImpairmentData impairment = new ImpairmentData();
				try {
					dao.updateExportDate(impairment.getImpairmentId(), impairment.getExportDate(), impairment.getUpdatedBy());
					conn.commit();
				}
				catch (SQLException e) {
					if (conn != null) {
						conn.rollback();
					}
					LOGGER.error(CodeHelper.getStackTrace(e));
					throw new IUMException(e);
				}
			}
			createAuditTrail();
			
		}
		catch (Exception ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		finally {
			closeConn(conn);
			LOGGER.info("changeMIBStatus end");
		}
	}//changeMIBStatus
	
	
	/**
	 * Deletes an impairment record
	 * @param impairmentId id of the impairment to be deleted
	 * @param userId user that deleted the impairment record
	 * @return
	 * @throws IUMException
	 */
	public String deleteImpairment(long impairmentId, String userId) throws IUMException {
		
		LOGGER.info("deleteImpairment start");
		Connection conn = null;
		try {			
		
			conn = new DataSourceProxy().getConnection();
			ImpairmentDAO dao = new ImpairmentDAO(conn);
			String impairmentCode = "";

			ImpairmentData impairment = dao.retrieveImpairment(impairmentId);
			impairmentCode = impairment.getImpairmentCode();
			Date exportDate = impairment.getExportDate();
			 
			if (exportDate == null) {
				conn.setAutoCommit(false);
				dao.deleteImpairment(impairmentId, userId);
				conn.commit();
			}
			else {
				LOGGER.warn(impairmentCode + ": Deletion of already exported impairment is not allowed.");
				throw new UnderWriterException(impairmentCode + ": Deletion of already exported impairment is not allowed.");
			}
		
			return impairmentCode;
 		}
 		catch (SQLException e) {
 			LOGGER.error(CodeHelper.getStackTrace(e));
 			try {
 				if (conn != null) {
 					conn.rollback();
 				}				
			} catch (SQLException e1) {
				LOGGER.error(CodeHelper.getStackTrace(e1));
			}
			throw new IUMException(e);
		}
		catch (Exception ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		finally {
			closeConn(conn);
			LOGGER.info("deleteImpairment end");
		}
	}//deleteImpairment

	/**
	 * Retrieves the impairment data for this impairment id.
	 * @param impairmentId id of the impairment to be retrieved
	 * @return ImpairmentData
	 * @throws IUMException
	 */
	public ImpairmentData retrieveImpairment(long impairmentId) throws IUMException {
		
		LOGGER.info("retrieveImpairment start");
		Connection conn = null;
		try {			
			
			conn = new DataSourceProxy().getConnection();
			ImpairmentDAO dao = new ImpairmentDAO(conn);
			ImpairmentData data = dao.retrieveImpairment(impairmentId);
			
			return data;			
		}
		catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMException(e);
		}
		catch (Exception ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		finally {
			closeConn(conn);
			LOGGER.info("retrieveImpairment end");
		}
	}//retrieveImpairment
	
	
	/**
	 * Updates the impairment for this ImpairmentData and clientType. This method throws UnderWriterException if the passed
	 * impairment is already existing in the database.
	 * @param impData new ImpairmentData
	 * @param clientType
	 * @throws IUMException
	 * @throws UnderWriterException
	 */
	public void updateImpairment(ImpairmentData impData, String clientType) throws IUMException,UnderWriterException {
		
		LOGGER.info("updateImpairment start");
		Connection conn = null;
		try {		
			
			conn = new DataSourceProxy().getConnection();
			ImpairmentDAO dao = new ImpairmentDAO(conn);
			ImpairmentData tempImpData = retrieveImpairment(impData.getImpairmentId());

			if (impData.getImpairmentCode().equals(tempImpData.getImpairmentCode())) {
				
				dao.updateImpairment(impData);			
			} else {
				
				AssessmentRequest ar = new AssessmentRequest();
				AssessmentRequestData arData = ar.getDetails(impData.getReferenceNumber());
				String clientNo = "";
				
				if (clientType.equals(IUMConstants.CLIENT_TYPE_INSURED)){
					clientNo = arData.getInsured().getClientId();
				} else if (clientType.equals(IUMConstants.CLIENT_TYPE_OWNER)){
					clientNo = arData.getOwner().getClientId();
				}
				
				boolean isDuplicate = dao.isDuplicateCode(impData.getImpairmentCode(),impData.getReferenceNumber(),clientNo);
				LOGGER.warn("***************** IS DUPLICATE: ********************" + isDuplicate);
				
				if (isDuplicate) {
					LOGGER.warn("Impairment code is already existing.");
					throw new UnderWriterException("Impairment code is already existing.");
				} else {
					conn.setAutoCommit(false);
					dao.updateImpairment(impData);
					conn.commit();
				}
			}
			
		}
		catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			try {
				if (conn != null) {
					conn.rollback();
				}				
			} catch (SQLException e1) {
				LOGGER.error(CodeHelper.getStackTrace(e1));
			}
			throw new IUMException(e);
		}
		catch (Exception ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		finally {
			closeConn(conn);
			LOGGER.info("updateImpairment end");
		}
	}//updateImpairment
	

	private void createAuditTrail() throws IUMException {
		
		LOGGER.info("createAuditTrail start");
		Connection conn = null;
		try {			
			AuditTrailData data = new AuditTrailData();
			data.setTransRec(IUMConstants.TRAN_RECORD_MIB_EXPORT);
			data.setTransDate(new Date());
			data.setUserID(IUMConstants.USER_IUMS);
			
			conn = new DataSourceProxy().getConnection();
			AuditTrailDAO dao = new AuditTrailDAO(conn);
			conn.setAutoCommit(false);
			dao.insertAuditTrail(data);
			conn.commit();
		}
		catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			try {
				if (conn != null) {
					conn.rollback();
				}				
			} catch (SQLException e1) {
				LOGGER.error(CodeHelper.getStackTrace(e1));
			}
			throw new IUMException(e);
		}
		catch (Exception ex) {
			throw new IUMException(ex);
		}
		finally {
			closeConn(conn);
			LOGGER.info("createAuditTrail end");
		}
	}//createAuditTrail
	

	/**
	 * Counts the number of impairments  that for export(imp_export_date == null). 
	 * @return number of impairments to be exported
	 * @throws IUMException
	 */
	public int countMIBForExport() throws IUMException {
		
		LOGGER.info("countMIBForExport start");
		Connection conn = null;		
		try {
			int count = 0;
						
			conn = new DataSourceProxy().getConnection();
			ImpairmentDAO dao = new ImpairmentDAO(conn);
			
			count = dao.countMIBForExport();
			
			return count;
		}
		catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMException(e);
		}
		catch (Exception ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		finally {
			closeConn(conn);
			LOGGER.info("countMIBForExport end");
		}
	}

	private void closeConn(Connection conn) throws IUMException {
		
		LOGGER.info("closeConn start");
		try {
			if (conn != null){
				conn.close();
			}
		}
		catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMException(e);
		}
		LOGGER.info("closeConn end");
	}
	
}
