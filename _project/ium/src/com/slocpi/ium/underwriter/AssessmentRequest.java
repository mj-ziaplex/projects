/*
 * Created on Dec 18, 2003
 *
 */
package com.slocpi.ium.underwriter;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.slocpi.ium.IUMException;
import com.slocpi.ium.data.AssessmentRequestData;
import com.slocpi.ium.data.CDSMortalityRatingData;
import com.slocpi.ium.data.ClientData;
import com.slocpi.ium.data.ClientDataSheetData;
import com.slocpi.ium.data.KickOutMessageData;
import com.slocpi.ium.data.MedExamFiltersData;
import com.slocpi.ium.data.MedicalRecordData;
import com.slocpi.ium.data.PolicyDetailData;
import com.slocpi.ium.data.PolicyRequirementsData;
import com.slocpi.ium.data.PurgeStatisticData;
import com.slocpi.ium.data.RequestFilterData;
import com.slocpi.ium.data.RequirementData;
import com.slocpi.ium.data.StatusData;
import com.slocpi.ium.data.UWAssessmentData;
import com.slocpi.ium.data.UserProfileData;
import com.slocpi.ium.data.dao.ActivityDAO;
import com.slocpi.ium.data.dao.AssessmentRequestDAO;
import com.slocpi.ium.data.dao.ClientDAO;
import com.slocpi.ium.data.dao.ClientDataSheetDAO;
import com.slocpi.ium.data.dao.DocumentDAO;
import com.slocpi.ium.data.dao.ImpairmentDAO;
import com.slocpi.ium.data.dao.KickOutMessageDAO;
import com.slocpi.ium.data.dao.MedicalDAO;
import com.slocpi.ium.data.dao.PolicyCoverageDetailDao;
import com.slocpi.ium.data.dao.PolicyRequirementDAO;
import com.slocpi.ium.data.dao.ProcessConfigDAO;
import com.slocpi.ium.data.dao.ProcessConfigRoleDAO;
import com.slocpi.ium.data.dao.PurgeStatisticDAO;
import com.slocpi.ium.data.dao.ReferenceDAO;
import com.slocpi.ium.data.dao.UserDAO;
import com.slocpi.ium.data.util.DataSourceProxy;
import com.slocpi.ium.interfaces.IUMInterfaceException;
import com.slocpi.ium.service.AdminSystem;
import com.slocpi.ium.service.AdminSystemFactory;
import com.slocpi.ium.service.MessageController;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.DateHelper;
import com.slocpi.ium.util.IUMConstants;
import com.slocpi.ium.util.SortHelper;
import com.slocpi.ium.util.StringHelper;
import com.slocpi.ium.workflow.Workflow;
import com.slocpi.ium.workflow.WorkflowItem;

/**
 * @author Engel
 * Assessment Request business object, contains the business rule of the assessment request.
 *
 */
public class AssessmentRequest {
	private static final Logger LOGGER = LoggerFactory.getLogger(AssessmentRequest.class);
	
	//private ClientDataSheetData cdsData = null;

	/**
	 * Changes the status of the assessment request to Awaiting Medical
	 * @param referenceNumber assessment request reference number
	 */
	public void setPendingRequest(AssessmentRequestData request) throws IUMException{
		
		LOGGER.info("setPendingRequest start");
		this.changeStatus(request.getReferenceNumber(), IUMConstants.STATUS_AWAITING_MEDICAL, request.getUpdatedBy().getUserId());
		LOGGER.info("setPendingRequest end");
	}

	/**
	 * This method will try to insert a new Assessment Request.  The method will
	 * check if the policy exists.
	 * @param request assessment request data
	 * @throws SQLException 
	 */
	public void createAssessmentRequest(final AssessmentRequestData request) throws UnderWriterException, IUMException {
		
		LOGGER.info("createAssessmentRequest start");
		
		try {
			
			AssessmentRequestDAO ard = new AssessmentRequestDAO();
			StatusData sd = new StatusData();

			 // Check reference Number
			if(request.getReferenceNumber() == null){
				throw new UnderWriterException("Reference Number required");
			} else {
				request.setReferenceNumber(request.getReferenceNumber().toUpperCase());
			}

			//TODO GLENN : suffix FOR INGENIUM; exception will be used when we have fully migrated into INGENIUM
			if(request.getPolicySuffix() == null){
				request.setPolicySuffix("");
			} else {
				request.setPolicySuffix(request.getPolicySuffix().toUpperCase());
			}

			// Check for duplicate record
			if (ard.isPolicyExists(request.getReferenceNumber())) {
				LOGGER.warn("Duplicate Assessment Request record.");
				throw new UnderWriterException("Duplicate Assessment Request record.");
			}

			// Lob
			if(request.getLob() == null || (request.getLob()).getLOBCode() == null){
				throw new UnderWriterException("LOB code required");
			} else {
				String lobCode = (request.getLob()).getLOBCode();
				if(!ard.isLOB(lobCode)){
					LOGGER.warn("unknown LOB code: "+lobCode);
					throw new UnderWriterException("unknown LOB code: "+lobCode);
				}
			}

			// Branch
			if (request.getBranch() == null || (request.getBranch()).getOfficeId() == null){
				LOGGER.warn("Branch/ Office Id required");
				throw new UnderWriterException("Branch/ Office Id required");
			} else {
				String branchId = (request.getBranch()).getOfficeId();
				if(!ard.isBranch(branchId)){
					LOGGER.warn("unknown Branch Code: " + branchId);
					throw new UnderWriterException("unknown Branch Code: " + branchId);
				}
			}

			// Agent
			if(request.getAgent() == null || (request.getAgent()).getUserId() == null){
				LOGGER.warn("Agent Id required");
				throw new UnderWriterException("Agent Id required");
			} else {
				String agentId = (request.getAgent()).getUserId();
				if(!ard.isUser(agentId)){
					LOGGER.warn("unknown AgentId: " +  agentId);
					throw new UnderWriterException("unknown AgentId: " +  agentId);
				}
			}

			// Set Status
			sd.setStatusId(IUMConstants.STATUS_NB_REVIEW_ACTION);
			request.setStatus(sd);

			// AssignedTo (set to user that is currently logged in)--> to be set before calling Bus. Obj.
			if(request.getAssignedTo() == null || (request.getAssignedTo()).getUserId() == null){
				LOGGER.warn("AssignedTo Id required");
				throw new UnderWriterException("AssignedTo Id required");
			} else {
				String assignedToId = (request.getAssignedTo()).getUserId();
				if(!ard.isUser(assignedToId)){
					LOGGER.warn("unknown User Id: " +  assignedToId);
					throw new UnderWriterException("unknown User Id: " +  assignedToId);
				}
			}

			// Underwriter
			if(request.getUnderwriter() == null
					||(request.getUnderwriter()).getUserId() == null
					|| (request.getUnderwriter()).getUserId().equals("") ){
				
				String uw = ard.getAutoAssignUW(request);
				UserProfileData underwriter = new UserProfileData();
				underwriter.setUserId(uw);
				request.setUnderwriter(underwriter);
			}
			
			// Folder Location
			if(request.getFolderLocation() == null || (request.getFolderLocation()).getUserId() == null){
				LOGGER.warn("User (Folder Location) required");
				throw new UnderWriterException("User (Folder Location) required");
			} else {
				String folderId = (request.getFolderLocation()).getUserId();
				if(!ard.isUser(folderId)){
					LOGGER.warn("unknown User Id: " +  folderId);
					throw new UnderWriterException("unknown User Id: " +  folderId);
				}
			}

			// Application received date
			if (request.getApplicationReceivedDate() == null){
				LOGGER.warn("Application Received Date is required");
				throw new UnderWriterException("Application Received Date is required");
			}

			// forwarded date
			// added check for PRISM
			if (request.getForwardedDate() == null
					&& !request.getSourceSystem().equals(IUMConstants.SYSTEM_ABACUS)
					&& !request.getSourceSystem().equals(IUMConstants.SYSTEM_INGENIUM)
					&& !request.getSourceSystem().equals(IUMConstants.SYSTEM_PRISM)
					&& !request.getLob().equals(IUMConstants.LOB_INDIVIDUAL_LIFE)){
				LOGGER.warn("Date Forwarded is required");
				throw new UnderWriterException("Date Forwarded is required");
			}
			Date today = new Date();

			// Set update date and createDate
			request.setUpdatedDate(today);
			request.setCreatedDate(today);

			// Check LOB value
			isValidClient(request);
			
			// Insert Client Data
			// @TODO ENGEL: handle duplicate or existing Client Data
			String lobCode = (request.getLob()).getLOBCode();
			ClientDAO clientDAO = new ClientDAO();
			
			if (lobCode.equalsIgnoreCase("PN") ){
				
				// Check for duplicate
				ClientData ownerData = request.getOwner();
				ownerData.setClientId(ownerData.getClientId().toUpperCase());
				ClientData exist = clientDAO.retrieveClient( ownerData.getClientId() );
				request.setInsured(ownerData);
				//1-27 JMA added setAutoCommit (false)
				if(exist == null
						|| exist.getClientId() == null
						|| exist.getClientId().equals("") ) {
					clientDAO.insertClient(ownerData);	
				} else {
					// Update
					ClientData orig = clientDAO.retrieveClient(ownerData.getClientId());
					orig.setAge( ownerData.getAge() );
					orig.setTitle( ownerData.getTitle() );
					orig.setGivenName( ownerData.getGivenName() );
					orig.setMiddleName( ownerData.getMiddleName() );
					orig.setLastName( ownerData.getLastName() );
					orig.setSuffix( ownerData.getSuffix() );
					orig.setSex( ownerData.getSex() );
					orig.setBirthDate( ownerData.getBirthDate() );
				
					// 1-27 JMA - added commit/rollback to prevent locking
					clientDAO.updateClient(orig);
				}
			} else if (lobCode.equalsIgnoreCase("GL") ) {
				
				ClientData insured = request.getInsured();
				insured.setClientId(insured.getClientId().toUpperCase());
				ClientData exist = clientDAO.retrieveClient(insured.getClientId());
				
				if(exist == null
						|| exist.getClientId() == null
						|| exist.getClientId().equals("") ){
					clientDAO.insertClient(insured);
				} else {
					// Update
					ClientData orig = clientDAO.retrieveClient(insured.getClientId());
					orig.setAge( insured.getAge() );
					orig.setTitle( insured.getTitle() );
					orig.setGivenName( insured.getGivenName() );
					orig.setMiddleName( insured.getMiddleName() );
					orig.setLastName( insured.getLastName() );
					orig.setSuffix( insured.getSuffix() );
					orig.setSex( insured.getSex() );
					orig.setBirthDate( insured.getBirthDate() );
					
					clientDAO.updateClient( orig );
				}
			} else if (lobCode.equalsIgnoreCase("IL") ){
				
				ClientData insured = request.getInsured();
				insured.setClientId(insured.getClientId().toUpperCase());
				ClientData exist = clientDAO.retrieveClient(insured.getClientId());
				
				if(exist == null
						|| exist.getClientId() == null
						|| exist.getClientId().equals("") ){
					clientDAO.insertClient(insured);
				} else {
					// Update
					ClientData orig = clientDAO.retrieveClient(insured.getClientId());
					orig.setAge( insured.getAge() );
					orig.setTitle( insured.getTitle() );
					orig.setGivenName( insured.getGivenName() );
					orig.setMiddleName( insured.getMiddleName() );
					orig.setLastName( insured.getLastName() );
					orig.setSuffix( insured.getSuffix() );
					orig.setSex( insured.getSex() );
					orig.setBirthDate( insured.getBirthDate() );

					clientDAO.updateClient( orig );
				}

				if(request.getOwner() != null
						&& (request.getOwner()).getClientId() != null
						&& !(request.getOwner()).getClientId().equalsIgnoreCase( (request.getInsured()).getClientId())){
					
					ClientData ownerData = request.getOwner();
					ownerData.setClientId(ownerData.getClientId().toUpperCase());
					exist = clientDAO.retrieveClient(ownerData.getClientId());
					
					// Insert or Update
					if(exist == null
							|| exist.getClientId() == null
							|| exist.getClientId().equals("") ) {
						clientDAO.insertClient(ownerData);
					} else {
						ClientData orig = clientDAO.retrieveClient(ownerData.getClientId());
						orig.setAge( ownerData.getAge() );
						orig.setTitle( ownerData.getTitle() );
						orig.setGivenName( ownerData.getGivenName() );
						orig.setMiddleName( ownerData.getMiddleName() );
						orig.setLastName( ownerData.getLastName() );
						orig.setSuffix( ownerData.getSuffix() );
						orig.setSex( ownerData.getSex() );
						orig.setBirthDate( ownerData.getBirthDate() );
						
						clientDAO.updateClient( orig );
					}
				}

				// Mapping for sourceSystem
				if(request.getSourceSystem() != null){
					
					if(request.getSourceSystem().equals(IUMConstants.SYSTEM_ABACUS)){
						request.setSourceSystem(IUMConstants.SYSTEM_ABACUS);
					} else if( request.getSourceSystem().equals(IUMConstants.SYSTEM_PRISM)){
						request.setSourceSystem(IUMConstants.SYSTEM_PRISM);
					} else if( request.getSourceSystem().equals(IUMConstants.SYSTEM_GLASS)){
						request.setSourceSystem(IUMConstants.SYSTEM_GLASS);
					} else if( request.getSourceSystem().equals(IUMConstants.SYSTEM_INGENIUM)){
						request.setSourceSystem(IUMConstants.SYSTEM_INGENIUM);
					}
				}
	
				ard.insertAssessmentRequest(request);
				
				WorkflowItem wfi = new WorkflowItem();
				Workflow workflow = new Workflow();
				
				if(request.getSourceSystem().equals("")){
					wfi.setSender(request.getCurrentUser());
					LOGGER.warn("this has no source system");
				}
				
				wfi.setObjectID(request.getReferenceNumber());
				wfi.setType(IUMConstants.ASSESSMENT_REQUEST);
				wfi.setLOB(request.getLob().getLOBCode());
				wfi.setBranchId(request.getBranch().getOfficeId());
				
				ArrayList notify = new ArrayList();
				String userId = request.getAssignedTo().getUserId();
				notify.add(userId);
				wfi.setPrimaryRecipient(notify);
				wfi.setAgentId(userId);
				workflow.processState(wfi);
				
			}
		} catch (SQLException sqlE) {
			LOGGER.error(CodeHelper.getStackTrace(sqlE));
			throw new IUMException(sqlE);
		} catch (Exception e){
			LOGGER.error(CodeHelper.getStackTrace(e));
			if (e instanceof UnderWriterException) {
				LOGGER.error(CodeHelper.getStackTrace(e));
				throw (UnderWriterException)e;
			} else {
				LOGGER.error(CodeHelper.getStackTrace(e));
				throw new IUMException(e);
			}
		} 
			
		LOGGER.info("createAssessmentRequest end");
		
	}
	
	/**
	 * This method will try to insert a new Assessment Request.
	 * This is for used through retrieveCDS
	 * @param request assessment request data
	 * @throws SQLException 
	 */
	public void createAssessmentRequest2(final AssessmentRequestData request) throws UnderWriterException, IUMException {
		
		LOGGER.info("createAssessmentRequest2 start");
		
		try {
			
			
			AssessmentRequestDAO ard = new AssessmentRequestDAO();
			StatusData sd = new StatusData();

			 // Check reference Number
			if(request.getReferenceNumber() == null){
				LOGGER.warn("Reference Number required");
				throw new UnderWriterException("Reference Number required");
			} else {
				request.setReferenceNumber(request.getReferenceNumber().toUpperCase());
			}

			//TODO GLENN : suffix FOR INGENIUM; exception will be used when we have fully migrated into INGENIUM
			if(request.getPolicySuffix() == null){
				request.setPolicySuffix("");
			} else {
				request.setPolicySuffix(request.getPolicySuffix().toUpperCase());
			}

			// Lob
			if(request.getLob() == null || (request.getLob()).getLOBCode() == null){
				LOGGER.warn("LOB code required");
				throw new UnderWriterException("LOB code required");
			} else {
				String lobCode = (request.getLob()).getLOBCode();
				if(!ard.isLOB(lobCode)){
					LOGGER.warn("unknown LOB code: "+lobCode);
					throw new UnderWriterException("unknown LOB code: "+lobCode);
				}
			}

			// Branch
			if (request.getBranch() == null || (request.getBranch()).getOfficeId() == null){
				LOGGER.warn("Branch/ Office Id required");
				throw new UnderWriterException("Branch/ Office Id required");
			} else {
				String branchId = (request.getBranch()).getOfficeId();
				if(!ard.isBranch(branchId)){
					LOGGER.warn("unknown Branch Code: " + branchId);
					throw new UnderWriterException("unknown Branch Code: " + branchId);
				}
			}

			// Agent
			if(request.getAgent() == null || (request.getAgent()).getUserId() == null){
				LOGGER.warn("Agent Id required");
				throw new UnderWriterException("Agent Id required");
			} else {
				String agentId = (request.getAgent()).getUserId();
				if(!ard.isUser(agentId)){
					LOGGER.warn("unknown AgentId: " +  agentId);
					throw new UnderWriterException("unknown AgentId: " +  agentId);
				}
			}

			// Set Status
			sd.setStatusId(IUMConstants.STATUS_NB_REVIEW_ACTION);
			request.setStatus(sd);

			// AssignedTo (set to user that is currently logged in)--> to be set before calling Bus. Obj.
			if(request.getAssignedTo() == null || (request.getAssignedTo()).getUserId() == null){
				LOGGER.warn("AssignedTo Id required");
				throw new UnderWriterException("AssignedTo Id required");
			} else {
				String assignedToId = (request.getAssignedTo()).getUserId();
				if(!ard.isUser(assignedToId)){
					LOGGER.warn("unknown User Id: " +  assignedToId);
					throw new UnderWriterException("unknown User Id: " +  assignedToId);
				}
			}

			// Application received date
			if (request.getApplicationReceivedDate() == null){
				LOGGER.warn("Application Received Date is required");
				throw new UnderWriterException("Application Received Date is required");
			}

			// forwarded date
			// added check for PRISM
			if (request.getForwardedDate() == null
					&& !request.getSourceSystem().equals(IUMConstants.SYSTEM_ABACUS)
					&& !request.getSourceSystem().equals(IUMConstants.SYSTEM_INGENIUM)
					&& !request.getSourceSystem().equals(IUMConstants.SYSTEM_PRISM)
					&& !request.getLob().equals(IUMConstants.LOB_INDIVIDUAL_LIFE)){
				LOGGER.warn("Date Forwarded is required");
				throw new UnderWriterException("Date Forwarded is required");
			}
			Date today = new Date();

			// Set update date and createDate
			request.setUpdatedDate(today);
			request.setCreatedDate(today);

			// Check LOB value
			isValidClient(request);
			
			// Insert Client Data
			// @TODO ENGEL: handle duplicate or existing Client Data
			String lobCode = (request.getLob()).getLOBCode();
			ClientDAO clientDAO = new ClientDAO();
			
			if (lobCode.equalsIgnoreCase("IL") ){
				
				ClientData insured = request.getInsured();
				insured.setClientId(insured.getClientId().toUpperCase());
				ClientData exist = clientDAO.retrieveClient(insured.getClientId());

				if(exist == null
						|| exist.getClientId() == null
						|| exist.getClientId().equals("") ){
					clientDAO.insertClient(insured);
				} else {
					// Update
					ClientData orig = clientDAO.retrieveClient(insured.getClientId());
					orig.setAge( insured.getAge() );
					orig.setTitle( insured.getTitle() );
					orig.setGivenName( insured.getGivenName() );
					orig.setMiddleName( insured.getMiddleName() );
					orig.setLastName( insured.getLastName() );
					orig.setSuffix( insured.getSuffix() );
					orig.setSex( insured.getSex() );
					orig.setBirthDate( insured.getBirthDate() );

					clientDAO.updateClient( orig );
				}

				if(request.getOwner() != null
						&& (request.getOwner()).getClientId() != null
						&& !(request.getOwner()).getClientId().equalsIgnoreCase( (request.getInsured()).getClientId())){
					
					ClientData ownerData = request.getOwner();
					ownerData.setClientId(ownerData.getClientId().toUpperCase());
					exist = clientDAO.retrieveClient(ownerData.getClientId());
					
					// Insert or Update
					if(exist == null
							|| exist.getClientId() == null
							|| exist.getClientId().equals("") ) {
						clientDAO.insertClient(ownerData);
					} else {
						ClientData orig = clientDAO.retrieveClient(ownerData.getClientId());
						orig.setAge( ownerData.getAge() );
						orig.setTitle( ownerData.getTitle() );
						orig.setGivenName( ownerData.getGivenName() );
						orig.setMiddleName( ownerData.getMiddleName() );
						orig.setLastName( ownerData.getLastName() );
						orig.setSuffix( ownerData.getSuffix() );
						orig.setSex( ownerData.getSex() );
						orig.setBirthDate( ownerData.getBirthDate() );
						
						clientDAO.updateClient( orig );
					}
				}

				// Mapping for sourceSystem
				if(request.getSourceSystem() != null){
					
					if(request.getSourceSystem().equals(IUMConstants.SYSTEM_ABACUS)){
						request.setSourceSystem(IUMConstants.SYSTEM_ABACUS);
					} else if( request.getSourceSystem().equals(IUMConstants.SYSTEM_PRISM)){
						request.setSourceSystem(IUMConstants.SYSTEM_PRISM);
					} else if( request.getSourceSystem().equals(IUMConstants.SYSTEM_GLASS)){
						request.setSourceSystem(IUMConstants.SYSTEM_GLASS);
					} else if( request.getSourceSystem().equals(IUMConstants.SYSTEM_INGENIUM)){
						request.setSourceSystem(IUMConstants.SYSTEM_INGENIUM);
					}
				}
	
				ard.insertAssessmentRequest(request);
			}
		} catch (SQLException sqlE) {
			LOGGER.error(CodeHelper.getStackTrace(sqlE));
			throw new IUMException(sqlE);
		} catch (Exception e){
			LOGGER.error(CodeHelper.getStackTrace(e));
			if (e instanceof UnderWriterException) {
				throw (UnderWriterException)e;
			} else {
				LOGGER.error(CodeHelper.getStackTrace(e));
				throw new IUMException(e);
			}
		} 
			
		LOGGER.info("createAssessmentRequest2 end");
		
	}
	
		
		
		

	//Checks for Required ClientData/s
	/*  Business rule--> 	lob type	CD-req	clientType
	 * 						PN			owner	Planholder ('P')
	 * 						GL			insured	Member ('M')
	 *						IL			i only	Insured and Owner (only insured is required)
	 */
	private void isValidClient(final AssessmentRequestData request ) throws UnderWriterException{
		
		LOGGER.info("isValidClient start");
		String lobCode = (request.getLob()).getLOBCode();
		
		
		if(lobCode.equals("PN")){
			if( request.getOwner() == null || (request.getOwner().getClientId()) == null ){
				LOGGER.warn("Plan Holder Client Data is required!");
				throw new UnderWriterException("Plan Holder Client Data is required!");
			}
			
			
			request.setInsured(null);
		} else if(lobCode.equals("GL")) {
			
			if( request.getInsured() == null || (request.getInsured().getClientId()) == null ){
				LOGGER.warn("Member Client Data is required!");
				throw new UnderWriterException("Member Client Data is required!");
			}else{
				ClientData member = request.getInsured();
				if(!isCompleteClientData(member,request.getSourceSystem())){
					LOGGER.warn("Missing required Member Client Information");
					throw new UnderWriterException("Missing required Member Client Information");
				}
			}
			
			request.setOwner(null);
		} else if(lobCode.equals("IL")) {
			
			if( request.getInsured() == null || (request.getInsured().getClientId()) == null ){
				LOGGER.warn("Insured Client Data is required!");
				throw new UnderWriterException("Insured Client Data is required!");
			} else {
				ClientData insured = request.getInsured();
				if(!isCompleteClientData(insured,request.getSourceSystem())){
					LOGGER.warn("Missing required Insured Client Information");
					throw new UnderWriterException("Missing required Insured Client Information");
				}
			}
			
			if(request.getOwner() == null || (request.getOwner().getClientId()) == null ){
				request.setOwner(null);
			} else {
				if((request.getInsured().getClientId()).equals(request.getOwner().getClientId())){
					request.setOwner(request.getInsured());
				} else {
					ClientData owner = request.getOwner();
					if(!isCompleteClientData(owner,request.getSourceSystem())){
						LOGGER.warn("Missing required Owner Client Information");
						throw new UnderWriterException("Missing required Owner Client Information");
					}
				}
			}
		}
		LOGGER.info("isValidClient end");
		
	}

	private boolean isCompleteClientData(ClientData cd, String source){
		
		LOGGER.info("isCompleteClientData start");
		boolean result = false;
		if (source.equals("")){

			if ( (cd.getClientId()!=null && (cd.getClientId()).length()>0 )&&
				 (cd.getGivenName()!=null && (cd.getGivenName()).length() >0 )&&
				 (cd.getLastName()!=null && (cd.getLastName()).length() >0) &&
				 (cd.getMiddleName()!=null)){

				result=true;
			}

		}else if (source.equals(IUMConstants.SYSTEM_ABACUS) || source.equals(IUMConstants.SYSTEM_INGENIUM)){
			if ( (cd.getAge()>=0)&&( cd.getClientId()!=null && (cd.getClientId()).length()>0 )&&
				 (cd.getGivenName()!=null && (cd.getGivenName()).length() >0 )&&
				 (cd.getLastName()!=null && (cd.getLastName()).length() >0) &&
				 (cd.getMiddleName()!=null) ){

				result=true;
			}

		}else if (source.equals(IUMConstants.SYSTEM_GLASS)){

		}else if (source.equals(IUMConstants.SYSTEM_PRISM)){

		}
		LOGGER.info("isCompleteClientData end");
		return result;
	}


	/**
	 * Retrieves assessment request depending on the filter passed to this method
	 * @param requestFilter filter object
	 * @return ArrayList of AssessmentRequestData
	 */
	public ArrayList getRequestList(RequestFilterData requestFilter) throws IUMException {
		
		LOGGER.info("getRequestList start");
		
		ArrayList requestList = null;
		
		try{
			AssessmentRequestDAO dao = new AssessmentRequestDAO();
			requestList = dao.getRequestList(requestFilter);
		}catch(SQLException e){
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMException(e.getMessage());
		}
		
		LOGGER.info("getRequestList end");
		
		return requestList;
	}


	/**
	 * Changes the status of the assessment request to For Transmittal to USD, sends notification, assigns the assessment request to the defined
	 * facilitator and updates the date forwarded of the assessment request.
	 * @param referenceNumber assessment request reference number
	 * @param facilitator  user whom the assessment request will be assigned to
	 * @param user user that updated the assessment request
	 * @throws IUMException
	 * @throws UnderWriterException
	 * @throws IUMInterfaceException
	 */
	public void transmit(String referenceNumber, String facilitator, String user) throws IUMException, UnderWriterException, IUMInterfaceException {
		
		LOGGER.info("transmit start");
		ArrayList recipient = new ArrayList();
		long operation = IUMConstants.STATUS_FOR_TRANSMITTAL_USD;

		recipient.add(facilitator);
		AssessmentRequestData arData = new AssessmentRequestData();

		arData = getDetails(referenceNumber);

		long previousStatus = arData.getStatus().getStatusId();
		String lob = arData.getLob().getLOBCode();
		boolean passStatusValidation = false;

		try {
			
			ProcessConfigDAO processDao = new ProcessConfigDAO();
			passStatusValidation = processDao.isValidStatus(lob,IUMConstants.STAT_TYPE_AR,previousStatus,	operation);
		} catch (SQLException e1) {
			LOGGER.error(CodeHelper.getStackTrace(e1));
			throw new IUMException(e1);
		}

		if (passStatusValidation == false){
			LOGGER.warn("The status of the request is invalid");
			throw new UnderWriterException("The status of the request is invalid");
		}

		if (isAssignedToValid(facilitator,operation)){
			try {
				assignTo(referenceNumber, facilitator, user);
				updateDateForwarded(referenceNumber,new java.util.Date(),user);
				changeStatus(referenceNumber,operation,user);

				WorkflowItem wfi = new WorkflowItem();
				Workflow workflow = new Workflow();
				wfi.setSender(user);
				wfi.setObjectID(arData.getReferenceNumber());
				wfi.setPreviousStatus(String.valueOf(previousStatus));
				wfi.setType(IUMConstants.ASSESSMENT_REQUEST);
				wfi.setLOB(lob);
				wfi.setBranchId(arData.getBranch().getOfficeId());
				wfi.setPrimaryRecipient(recipient);
				workflow.processState(wfi);
			} catch (Exception e){
				LOGGER.error(CodeHelper.getStackTrace(e));
				throw new IUMException(e);
			}
		}
		LOGGER.info("transmit end");
	}


	/**
	 * Changes the status of the assessment request to Facilitator Action, creates policy requirement with a requirement code
	 * of Received Application By USD, sets the location of the assessment request to the user that updated the request.
	 * @param referenceNumber assessment request reference number
	 * @param user user that updated the assessment request
	 */
	public void setToForFacilitatorAction(String referenceNumber, String user) throws IUMException,UnderWriterException{
		
		LOGGER.info("setToForFacilitatorAction start");
		long operation = IUMConstants.STATUS_FOR_FACILITATOR_ACTION;
		AssessmentRequestData arData = new AssessmentRequestData();

		arData  = getDetails(referenceNumber);
		long previousStatus = arData.getStatus().getStatusId();

		String lob = arData.getLob().getLOBCode();
		boolean passStatusValidation = false;

		try {
			
			ProcessConfigDAO processDao = new ProcessConfigDAO();
			passStatusValidation = processDao.isValidStatus(lob,IUMConstants.STAT_TYPE_AR,previousStatus,	operation);
		} catch (SQLException e1) {
			LOGGER.error(CodeHelper.getStackTrace(e1));
			throw new IUMException(e1);
		}		
		
		if (passStatusValidation == false){
			LOGGER.warn("The status of the request is invalid");
			throw new UnderWriterException("The status of the request is invalid");
		}
		else {
			try {
				StatusData reqStatus = new StatusData();
				reqStatus.setStatusId(IUMConstants.STATUS_RECEIVED_IN_SITE);

				changeStatus(referenceNumber, operation, user);
				setLocation(referenceNumber, user, user);

				if (lob.equalsIgnoreCase(IUMConstants.LOB_INDIVIDUAL_LIFE)){
					PolicyRequirements polReq = new PolicyRequirements();
					PolicyRequirementsData reqData = new PolicyRequirementsData();
					reqData.setRequirementCode(IUMConstants.REQUIREMENT_CODE_RECEIVED_APPLICATION_BY_USD);
					reqData.setReferenceNumber(referenceNumber);
					reqData.setLevel(IUMConstants.LEVEL_POLICY);
					reqData.setCreateDate(new java.util.Date());
					reqData.setStatus(reqStatus);
					reqData.setUpdatedBy(user);
					reqData.setUpdateDate(new java.util.Date());
					reqData.setClientId(arData.getOwner().getClientId());
					reqData.setClientType(IUMConstants.CLIENT_TYPE_INSURED);
					reqData.setTestDate(new java.util.Date());
					reqData.setReceiveDate(new java.util.Date());

					Reference reference = new Reference();
					RequirementData requirementData = reference.getRequirementDetail(IUMConstants.REQUIREMENT_CODE_RECEIVED_APPLICATION_BY_USD);
					reqData.setFollowUpNumber(requirementData.getFollowUpNum());

					Date validityDate = computeValidityDate(IUMConstants.REQUIREMENT_CODE_RECEIVED_APPLICATION,new java.util.Date());
					reqData.setValidityDate(validityDate);

					String timestamp = DateHelper.sqlTimestamp(new java.util.Date()).toString();
					reqData.setComments(timestamp);

					arData = getDetails(referenceNumber);

					polReq.createRequirement(reqData, arData, user);


				}
			} catch (Exception e) {
				LOGGER.error(CodeHelper.getStackTrace(e));
				throw new IUMException(e);
			}
		}
		LOGGER.info("setToForFacilitatorAction end");
	}

	/**
	 * Changes the status of the assessment request to For Assessment, sends notification and assigns the assessment request
	 * to its predefined underwriter. If lob is Individual life and source system is Abacus it will trigger the
	 * processPolicyDetails(AssessmentRequestData) of the MessageController.
	 * @param referenceNumber assessment request reference number
	 * @param user user that updated the assessment request
 	 * @throws IUMException
	 * @throws UnderWriterException
	 */
	public void submitToUnderwriter(String referenceNumber, String user) throws IUMException, UnderWriterException{
		
		LOGGER.info("submitToUnderwriter start");
		long operation = IUMConstants.STATUS_FOR_ASSESSMENT;
		AssessmentRequestData arData = new AssessmentRequestData();

		arData = getDetails(referenceNumber);

		long previousStatus = arData.getStatus().getStatusId();
		String lob = arData.getLob().getLOBCode();
		String underwriter = arData.getUnderwriter().getUserId();

		ArrayList recipient = new ArrayList();
		recipient.add(underwriter);
		boolean passStatusValidation = false;

		try {
			ProcessConfigDAO processDao = new ProcessConfigDAO();
			passStatusValidation = processDao.isValidStatus(lob,IUMConstants.STAT_TYPE_AR,previousStatus,operation);
		} catch (SQLException e1) {
			LOGGER.error(CodeHelper.getStackTrace(e1));
			throw new IUMException(e1);
		} 

		if (passStatusValidation == false){
			LOGGER.warn("The status of the request is invalid");
			throw new UnderWriterException("The status of the request is invalid");
		}else {
			assignTo(referenceNumber,underwriter,user);
			changeStatus(referenceNumber,operation,user);

			try{
				WorkflowItem wfi = new WorkflowItem();
				Workflow workflow = new Workflow();
				wfi.setSender(user);
				wfi.setObjectID(arData.getReferenceNumber());
				wfi.setPreviousStatus(String.valueOf(previousStatus));
				wfi.setType(IUMConstants.ASSESSMENT_REQUEST);
				wfi.setLOB(lob);
				wfi.setBranchId(arData.getBranch().getOfficeId());
				wfi.setPrimaryRecipient(recipient);
				workflow.processState(wfi);

				String source = arData.getSourceSystem();
				if (lob.equalsIgnoreCase(IUMConstants.LOB_INDIVIDUAL_LIFE) && (source != null &&
					(source.equalsIgnoreCase(IUMConstants.SYSTEM_ABACUS) ||
					source.equalsIgnoreCase(IUMConstants.SYSTEM_INGENIUM)))) {

					setSuccessfulRetrieval(referenceNumber,IUMConstants.INDICATOR_FAILED);
					AdminSystem as = AdminSystemFactory.getAdminSystem(arData.getSourceSystem());
					AssessmentRequest ar = new AssessmentRequest();
					as.retrieveCDSDetails(referenceNumber, ar.getPolicySuffix(referenceNumber));
				}
				
			} catch (Exception e){
				LOGGER.error(CodeHelper.getStackTrace(e));
				throw new IUMException(e);
			}
		}
		LOGGER.info("submitToUnderwriter end");
	}


	/**
	 * Changes the status of the assessment request to Undergoing Assessment and sets the location of the assessment request
	 * to the user that invoked this method. Process configuration checking is also applied in this method.
	 * @param referenceNumber assessment request reference number
	 * @param user user that updated the assessment request
	 * @throws IUMException
	 * @throws UnderWriterException
	 */
	public void assessApplication(String referenceNumber, String user) throws IUMException,UnderWriterException{
		
		LOGGER.info("assessApplication start");
		long operation = IUMConstants.STATUS_UNDERGOING_ASSESSMENT;
		AssessmentRequestData arData = new AssessmentRequestData();

		arData = getDetails(referenceNumber);
		long previousStatus = arData.getStatus().getStatusId();

		String lob = arData.getLob().getLOBCode();

		
		boolean passStatusValidation = false;

		try {
			ProcessConfigDAO processDao = new ProcessConfigDAO();
			passStatusValidation = processDao.isValidStatus(lob,IUMConstants.STAT_TYPE_AR,previousStatus,operation);
		} catch (SQLException e1) {
			LOGGER.error(CodeHelper.getStackTrace(e1));
			throw new IUMException(e1);
		}

		if (passStatusValidation == false){
			LOGGER.warn("The status of the request is invalid");
			throw new UnderWriterException("The status of the request is invalid");
		} else {
			changeStatus(referenceNumber,operation,user);
			setLocation(referenceNumber, user, user);
		}
		LOGGER.info("assessApplication end");
	}


	/**
	 * Changes the status of the assessment request to For Approval and changes the assignTo.
	 * @param referenceNumber assessment request reference number
	 * @param assignee user whom the assessment request will be assigned to
	 * @param user user that updated the assessment request
	 * @throws IUMException
	 * @throws UnderWriterException
	 */
	public void submitRequestForApproval(String referenceNumber, String assignee, String user) throws IUMException,UnderWriterException{
		
		LOGGER.info("submitRequestForApproval start");
		long operation = IUMConstants.STATUS_FOR_APPROVAL;
		AssessmentRequestData arData = new AssessmentRequestData();

		arData = getDetails(referenceNumber);
		long previousStatus = arData.getStatus().getStatusId();

		String lob = arData.getLob().getLOBCode();
		boolean passStatusValidation = false;

		try {
			ProcessConfigDAO processDao = new ProcessConfigDAO();
			passStatusValidation = processDao.isValidStatus(lob,IUMConstants.STAT_TYPE_AR,previousStatus,operation);
		} catch (SQLException e1) {
			throw new IUMException(e1);
		} 

		if (passStatusValidation == false){
			LOGGER.warn("The status of the request is invalid");
			throw new UnderWriterException("The status of the request is invalid");
		} else if (isAssignedToValid(assignee,operation)){
			assignTo(referenceNumber,assignee,user);
			changeStatus(referenceNumber,operation,user);
		}
		LOGGER.info("submitRequestForApproval end");
	}

	/**
	 * This method changes the status of the assessment request to Approved, sends notification and triggers the processAutoSettleAssessmentRequest()
	 * of the MessageController. Checking is applied to the policy requirements, medical records associated with the assessment request before doing
	 * the processes mentioned above. Process configuration checking is also applied in this method.
	 * @param referenceNumber assessment request  reference number
	 * @param user user that updated the assessment request
	 * @throws IUMException
	 * @throws UnderWriterException
	 * @throws IUMInterfaceException
	 */
	public void approveRequestAsAppliedFor(String referenceNumber,String user) throws IUMException, UnderWriterException, IUMInterfaceException {
		
		LOGGER.info("approveRequestAsAppliedFor start");
		long operation = IUMConstants.STATUS_APPROVED;
		ArrayList recipients = new ArrayList();
		AssessmentRequestData arData = new AssessmentRequestData();

		arData = getDetails(referenceNumber);
		String lob = arData.getLob().getLOBCode();
		long previousStatus = arData.getStatus().getStatusId();

		
		boolean passStatusValidation = false;

		try {
			ProcessConfigDAO processDao = new ProcessConfigDAO();
			passStatusValidation = processDao.isValidStatus(lob,IUMConstants.STAT_TYPE_AR,previousStatus,operation);
		} catch (SQLException e1) {
			LOGGER.error(CodeHelper.getStackTrace(e1));
			throw new IUMException(e1);
		}

		if (passStatusValidation == false){
			LOGGER.warn("The status of the request is invalid");
			throw new UnderWriterException("The status of the request is invalid");
		} else if (passMedicalRecordValidation(referenceNumber, operation)
				&& passRequirementValidation(referenceNumber, operation)){
			try{
				MedicalLabRecord medicalRecord = new MedicalLabRecord();
				medicalRecord.updateChargeTo(referenceNumber,arData.getLob().getLOBCode(),user);

				changeStatus(referenceNumber,operation,user);

				WorkflowItem wfi = new WorkflowItem();
				Workflow workflow = new Workflow();
				wfi.setSender(user);
				wfi.setObjectID(arData.getReferenceNumber());
				wfi.setLOB(lob);
				wfi.setPreviousStatus(String.valueOf(previousStatus));
				wfi.setType(IUMConstants.ASSESSMENT_REQUEST);
				wfi.setBranchId(arData.getBranch().getOfficeId());
				
				String agentId = arData.getAgent().getUserId();
				recipients.add(agentId);
				wfi.setPrimaryRecipient(recipients);
				wfi.setAgentId(agentId);
				
				workflow.processState(wfi);

				MessageController msgController = new MessageController();
				setAutoSettleIndicator(referenceNumber, IUMConstants.INDICATOR_FAILED); // it will be set to successful in the processAutoSettle()
				msgController.processAutoSettleAssessmentRequest(arData);
				
			} catch(Exception e){
				LOGGER.error(CodeHelper.getStackTrace(e));
				throw new IUMException(e);
			}
		}
		LOGGER.info("approveRequestAsAppliedFor end");
	}

	/**
	 * This method changes the status of the assessment requests to For Offer, creates a policy requirement with a requirement code
	 * of Acceptance of Offer and sends notification to the define recipients.
	 * @param referenceNumber assessment request reference number
	 * @param user user that updated the assessment request
	 * @throws IUMException
	 * @throws UnderWriterException
	 */
	public void approveApplicationForOffer(String referenceNumber,String user) throws IUMException, UnderWriterException{
		
		LOGGER.info("approveApplicationForOffer start");
		ArrayList recipients = new ArrayList();
		AssessmentRequestData arData = new AssessmentRequestData();
		arData = getDetails(referenceNumber);

		long operation = IUMConstants.STATUS_FOR_OFFER;
		long previousStatus = arData.getStatus().getStatusId();
		String lob = arData.getLob().getLOBCode();

		boolean passStatusValidation = false;

		try {
			ProcessConfigDAO processDao = new ProcessConfigDAO();
			passStatusValidation = processDao.isValidStatus(lob,IUMConstants.STAT_TYPE_AR,previousStatus,operation);
		} catch (SQLException e1) {
			LOGGER.error(CodeHelper.getStackTrace(e1));
			throw new IUMException(e1);
		}

		if (passStatusValidation == false){
			LOGGER.warn("The status of the request is invalid");
			throw new UnderWriterException("The status of the request is invalid");
		}

		if (lob.equals(IUMConstants.LOB_INDIVIDUAL_LIFE)){
			if (passMedicalRecordValidation(referenceNumber,operation) &&
				passRequirementValidation(referenceNumber,operation)){

				try{
					StatusData reqStatus = new StatusData();
					reqStatus.setStatusId(IUMConstants.STATUS_ORDERED);

					PolicyRequirements polReq = new PolicyRequirements();
					PolicyRequirementsData reqData = new PolicyRequirementsData();
					reqData.setRequirementCode(IUMConstants.REQUIREMENT_CODE_ACCEPTANCE_OF_OFFER);
					reqData.setReferenceNumber(referenceNumber);
					reqData.setLevel(IUMConstants.LEVEL_POLICY);
					reqData.setCreateDate(new java.util.Date());
					reqData.setStatus(reqStatus);
					reqData.setUpdatedBy(user);
					reqData.setUpdateDate(new java.util.Date());
					reqData.setClientType(IUMConstants.CLIENT_TYPE_OWNER);
					reqData.setTestDate(new java.util.Date());

					Date validityDate = computeValidityDate(IUMConstants.REQUIREMENT_CODE_ACCEPTANCE_OF_OFFER,new java.util.Date());
					reqData.setValidityDate(validityDate);

					
					String timestamp = DateHelper.sqlTimestamp(new java.util.Date()).toString();
					reqData.setComments(timestamp);
					polReq.createRequirement(reqData, arData, user);

					changeStatus(referenceNumber,operation,user);

					WorkflowItem wfi = new WorkflowItem();
					Workflow workflow = new Workflow();
					wfi.setSender(user);
					wfi.setObjectID(arData.getReferenceNumber());
					wfi.setLOB(lob);
					wfi.setPreviousStatus(String.valueOf(previousStatus));
					wfi.setType(IUMConstants.ASSESSMENT_REQUEST);
					wfi.setBranchId(arData.getBranch().getOfficeId());
					String agentId = arData.getAgent().getUserId();
					recipients.add(agentId);
					wfi.setPrimaryRecipient(recipients);
					wfi.setAgentId(agentId);
					workflow.processState(wfi);

				} catch(Exception e){
					LOGGER.error(CodeHelper.getStackTrace(e));
					throw new IUMException(e);
				}


			}
		} else {
			LOGGER.warn("Only individual life applications can be approved for offer.");
			throw new UnderWriterException("Only individual life applications can be approved for offer.");
		}
		LOGGER.info("approveApplicationForOffer end");
	}

	/**
	 * Changes the status of the assessment requests to Declined, updates the chargeableTo of the medical records to the
	 * agent associated to the assessment request, and sends notification. Process configuration checking is also applied in this
	 * method.
	 * @param referenceNumber assessment request reference number
	 * @param user user that updated the assessment request
	 * @throws IUMException
	 * @throws UnderWriterException
	 */
	public void declineApplication(String referenceNumber, String user) throws IUMException,UnderWriterException{
		
		LOGGER.info("declineApplication start");
		long operation = IUMConstants.STATUS_DECLINED;
		ArrayList recipients = new ArrayList();

		AssessmentRequestData arData = getDetails(referenceNumber);
		long previousStatus = arData.getStatus().getStatusId();
		String lob = arData.getLob().getLOBCode();
		boolean passStatusValidation = false;

		try {
			ProcessConfigDAO processDao = new ProcessConfigDAO();
			passStatusValidation = processDao.isValidStatus(lob,IUMConstants.STAT_TYPE_AR,previousStatus,operation);
		} catch (SQLException e1) {
			LOGGER.error(CodeHelper.getStackTrace(e1));
			throw new IUMException(e1);
		}

		if (passStatusValidation == false){
			LOGGER.warn("The status of the request is invalid");
			throw new UnderWriterException("The status of the request is invalid");
		} else {
			try{
				MedicalLabRecord medRecords = new MedicalLabRecord();
				String userId = arData.getAgent().getUserId();
				medRecords.updateChargeTo(referenceNumber,userId,user);

				changeStatus(referenceNumber,operation,user);

				WorkflowItem wfi = new WorkflowItem();
				Workflow workflow = new Workflow();
				wfi.setSender(user);
				wfi.setObjectID(referenceNumber);
				wfi.setLOB(lob);
				wfi.setPreviousStatus(String.valueOf(previousStatus));
				wfi.setType(IUMConstants.ASSESSMENT_REQUEST);
				wfi.setBranchId(arData.getBranch().getOfficeId());
				recipients.add(userId);
				wfi.setPrimaryRecipient(recipients);
				wfi.setAgentId(userId);
				workflow.processState(wfi);
			} catch (Exception e){
				LOGGER.error(CodeHelper.getStackTrace(e));
				throw new IUMException(e);
			}
		}
		LOGGER.info("declineApplication end");
	}


	/**
	 * Changes the status of the assessment request to Not Proceeded With, cancels associated policy requirements and medical records,
	 * and sends notification. Medical Records chargeableTo is set to the assessment request' agent.
	 * @param referenceNumber assessment request reference number
	 * @param user user that updated the assessment request
	 * @throws IUMException
	 * @throws UnderWriterException
	 */
	public void notProceededWith(String referenceNumber,String user) throws IUMException,UnderWriterException{
		
		LOGGER.info("notProceededWith start");
		ArrayList recipients = new ArrayList();
		long operation = IUMConstants.STATUS_NOT_PROCEEDED_WITH;
		AssessmentRequestData arData = new AssessmentRequestData();

		arData = getDetails(referenceNumber);
		long previousStatus = arData.getStatus().getStatusId();
		String lob = arData.getLob().getLOBCode();

		
		boolean passStatusValidation = false;

		try {
			
			ProcessConfigDAO processDao = new ProcessConfigDAO();
			passStatusValidation = processDao.isValidStatus(lob,IUMConstants.STAT_TYPE_AR,previousStatus,operation);
		} catch (SQLException e1) {
			LOGGER.error(CodeHelper.getStackTrace(e1));
			throw new IUMException(e1);
		}

		if (passStatusValidation == false){
			LOGGER.warn("The status of the request is invalid");
			throw new UnderWriterException("The status of the request is invalid");
		} else {
			
			try{

				changeStatus(referenceNumber,operation,user);
				
				PolicyRequirementDAO reqDao = new PolicyRequirementDAO();
				ArrayList requirements = reqDao.retrievePolicyRequirementsByReferenceNumber(referenceNumber);
				for (int i =0; i<requirements.size(); i++){
					PolicyRequirementsData reqData =(PolicyRequirementsData) requirements.get(i);
					reqData.setUpdatedBy(user);
				}

				PolicyRequirements polReq = new PolicyRequirements();
				polReq.cancelRequirement(requirements,arData);

				MedicalLabRecord medicalRecord = new MedicalLabRecord();
				ArrayList medicalList = medicalRecord.getMedicalLabRecords(referenceNumber);
				for (int i=0; i<medicalList.size(); i++){
					MedicalRecordData medData = (MedicalRecordData) medicalList.get(i);
					medData.setUpdatedBy(user);
				}
				medicalRecord.updateChargeTo(referenceNumber,arData.getAgent().getUserId(),user);
				medicalRecord.cancelMedicalExam(medicalList);

				WorkflowItem wfi = new WorkflowItem();
				Workflow workflow = new Workflow();
				wfi.setSender(user);
				wfi.setObjectID(arData.getReferenceNumber());
				wfi.setLOB(arData.getLob().getLOBCode());
				wfi.setPreviousStatus(String.valueOf(previousStatus));
				wfi.setType(IUMConstants.ASSESSMENT_REQUEST);
				wfi.setBranchId(arData.getBranch().getOfficeId());
				String agentId = arData.getAgent().getUserId();
				recipients.add(agentId);
				wfi.setPrimaryRecipient(recipients);
				wfi.setAgentId(agentId);
				workflow.processState(wfi);
			} catch(Exception e){
				LOGGER.error(CodeHelper.getStackTrace(e));
				throw new IUMException(e);
			}
		}
		LOGGER.info("notProceededWith end");
	}

	/**
	 * @Changes the status of assessment request to Cancelled. The parameter remarks is required if remarks is empty or null
	 * this will throw an UnderWriterException. Process configuration checking is also applied in this method.
	 * @param referenceNumber assessment request reference number
	 * @param remarks reason for cancelling the assessment request
	 * @param user user that updated the assessment request
	 * @throws IUMException
	 * @throws UnderWriterException
	 */
	public void cancelAssessmentRequest(String referenceNumber, String remarks, String user) throws IUMException, UnderWriterException{
		
		LOGGER.info("cancelAssessmentRequest start");
		long operation = IUMConstants.STATUS_AR_CANCELLED;
		AssessmentRequestData arData = new AssessmentRequestData();

		arData = getDetails(referenceNumber);
		long previousStatus = arData.getStatus().getStatusId();
		String sourceSystem = arData.getSourceSystem();

		String lob = arData.getLob().getLOBCode();
		
		boolean passStatusValidation = false;

		try {
			ProcessConfigDAO processDao = new ProcessConfigDAO();
			passStatusValidation = processDao.isValidStatus(lob,IUMConstants.STAT_TYPE_AR,previousStatus,operation);
		} catch (SQLException e1) {
			LOGGER.error(CodeHelper.getStackTrace(e1));
			throw new IUMException(e1);
		}

		if (passStatusValidation == false){
			LOGGER.warn("The status of the request is invalid");
			throw new UnderWriterException("The status of the request is invalid");
		}

		if (remarks != null && !remarks.equals("")){
			if (isEmpty(sourceSystem)){
				changeStatus(referenceNumber,operation,user);
				setRemarks(referenceNumber,remarks,user);
			} else {
				LOGGER.warn("The request is not valid for cancellation");
				throw new UnderWriterException("The request is not valid for cancellation");
			}
		} else {
			LOGGER.warn("Enter the reason for cancelling in the Remarks field.");
			throw new UnderWriterException("Enter the reason for cancelling in the Remarks field.");
		}
		LOGGER.info("cancelAssessmentRequest end");
		
	}


	/**
	 * Changes the assessment request "assignedTo" with the parameter assignee.
	 * @param referenceNumber assessment request reference number
	 * @param assignee user whom the assessment request will be assigned to
	 * @param updatedBy user that updated the assessment request
	 * @throws IUMException
	 */
	public void assignTo(String referenceNumber, String assignee, String updatedBy) throws IUMException{
		
		LOGGER.info("assignTo start");
		
		try {
			if (assignee != null && !assignee.equals("")){
				AssessmentRequestDAO arDAO = new AssessmentRequestDAO();
				arDAO.changeAssignedTo(referenceNumber, assignee, updatedBy);
			} else {
				LOGGER.warn("Assigned To is required");
				throw new UnderWriterException("Assigned To is required");
			}
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMException(e);
		} 
			LOGGER.info("assignTo end");
		
	}

	/**
	 * Set the location of the physical folder of the assessment request
	 * @param referenceNumber assesment request reference number
	 * @param location user id that will be set to the location attribute of the assessment request
	 * @param updatedBy user that updated the assessment request
	 * @return
	 */
	public void setLocation(String referenceNumber, String location, String updatedBy) throws IUMException{
		
		LOGGER.info("setLocation start");
	
		try {
			AssessmentRequestDAO arDAO = new AssessmentRequestDAO();
			arDAO.updateLocation(referenceNumber, location, updatedBy);
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			
			throw new IUMException(e);
		}
			LOGGER.info("setLocation end");
		
	}

	/**
	 * Retrieve assessment request details from the database
	 * @param referenceNumber assessment request reference number
	 * @return AssessmentRequestData
	 */
	public AssessmentRequestData getDetails(String referenceNumber) throws IUMException {
		
		LOGGER.info("getDetails start");
		
		try {
			AssessmentRequestDAO arDAO = new AssessmentRequestDAO();
			AssessmentRequestData assesstReqData = arDAO.retrieveRequestDetail(referenceNumber);
			
			if (assesstReqData == null){
				assesstReqData = new AssessmentRequestData();
			}
			
			LOGGER.info("getDetails end");
			return (assesstReqData);
		}
		catch (SQLException ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		catch (Exception ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		} 
		
		
	}

	/**
	 * Retrieves the policy requirements of the supplied assessment request reference number
	 * @param referenceNumber assessment request reference number
	 * @return arrayList of policy requirements data
	*/
	public ArrayList getPolicyRequirements(String referenceNumber) throws IUMException{
		
		LOGGER.info("getPolicyRequirements start");
		
		ArrayList reqList = new ArrayList();
		try {
			PolicyRequirementDAO reqDAO = new PolicyRequirementDAO();
			reqList = reqDAO.retrieveRequirements(referenceNumber);
		} catch (Exception e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMException(e);
		} 
		LOGGER.info("getPolicyRequirements end");
		return reqList;
	}


	/**
	 * Changes the status of the assessment requests
	 * @param referenceNumber assessment request reference number
	 * @param newStatus new status of the assessment request
	 * @param updatedBy user that updated the assessment request
	 * @throws IUMException
	 */
	public void changeStatus(String referenceNumber, long newStatus, String updatedBy) throws IUMException{

		LOGGER.info("changeStatus start");
		
		try {
			
			AssessmentRequestDAO arDAO = new AssessmentRequestDAO();
			arDAO.changeARStatus(referenceNumber, newStatus, updatedBy);
			
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMException(e);
		} 	
		LOGGER.info("changeStatus end");
	}

	/**
	 * Retrieves pending assessment requests
	 * @param userId
	 * @return number of pending assessment requests
	 * @throws IUMException
	 */
	public int getPendingAssignedAssessmentRequest(String userId)throws IUMException {
		
		LOGGER.info("getPendingAssignedAssessmentRequest start");
		int res = 0;
		
		try {
			
			AssessmentRequestDAO ard = new AssessmentRequestDAO();
			res = ard.selectNoOfPendingAssessmentRequest(userId);
			
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMException(e.getMessage());
		} 
		
		LOGGER.info("getPendingAssignedAssessmentRequest end");
		return res;
	}


	/**
	 * Retrieves the medical records of the client
	 * @param clientNo
	 * @return ArrayList of Medical Records
	 * @throws IUMException
	 */
	public ArrayList getKOMedRecs(String clientNo) throws IUMException {
		
		LOGGER.info("getKOMedRecs start");
		Connection conn = null;
		try {
			conn = new DataSourceProxy().getConnection();
			MedicalDAO medDAO = new MedicalDAO(conn);
			MedExamFiltersData filter = new MedExamFiltersData();
			filter.setClientNo(clientNo);
			filter.setStatus(IUMConstants.STATUS_VALID);
			filter.setValidityDate(new Date());
			SortHelper sort = new SortHelper(IUMConstants.TABLE_MEDICAL_RECORDS);
			sort.setColumn(IUMConstants.SORT_BY_MED_TEST_ID);
			sort.setSortOrder(IUMConstants.ASCENDING);
			ArrayList records = medDAO.getMedicalRecords(filter, sort);
			conn.close();
			
			return records;
		}
		catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMException(e);
		}
		catch (Exception e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMException(e);
		}finally{
			closeConnection(conn);
			LOGGER.info("getKOMedRecs end");
		}
	}


	private void processClientData(ClientData ownerData, ClientData insuredData, AssessmentRequestData retrievedARData) throws IUMException{
		
		LOGGER.info("processClientData start 1");
		try{
			AssessmentRequestDAO arDAO = new AssessmentRequestDAO();
			ClientDAO clDAO = new ClientDAO();

			if (ownerData != null && ownerData.getClientId()!=null && !"".equals(ownerData.getClientId())){
				if (arDAO.isClientExists(ownerData.getClientId())){
					clDAO.updateClient(ownerData, "O" ,retrievedARData.getReferenceNumber());
				} else{
					clDAO.insertClient(ownerData, "O" ,retrievedARData.getReferenceNumber());
				}
			}
			if (insuredData != null && insuredData.getClientId()!=null && !"".equals(insuredData.getClientId())){
				if(arDAO.isClientExists(insuredData.getClientId())){
					clDAO.updateClient(insuredData);
				}else{
					LOGGER.warn("Insured client does not exist." + insuredData.getClientId());
					throw new UnderWriterException("Insured client does not exist." + insuredData.getClientId());
				}
			}
			
		}catch(SQLException e){
			LOGGER.error(CodeHelper.getStackTrace(e));
			
			throw new IUMException(e);
		}
		LOGGER.info("processClientData end 1");
	}
	
	// COMMENT: connection will be passed by the calling method
	// to handle transaction rollback or commit
	public void processClientData(ClientData ownerData, ClientData insuredData) throws IUMException{
		
		LOGGER.info("processClientData start 2");
		try{
			AssessmentRequestDAO arDAO = new AssessmentRequestDAO();
			ClientDAO clDAO = new ClientDAO();

			// Insert/Update Owner (clients table)
			if (ownerData != null && ownerData.getClientId()!=null && !"".equals(ownerData.getClientId())){
				if (arDAO.isClientExists(ownerData.getClientId())){
					clDAO.updateClient(ownerData);
				} else{
					clDAO.insertClient(ownerData);
				}
			}
			
			// Insert/Update Insured (clients table)
			if (insuredData != null && insuredData.getClientId()!=null && !"".equals(insuredData.getClientId())){
				if(arDAO.isClientExists(insuredData.getClientId())){
					clDAO.updateClient(insuredData);
				}else{
					LOGGER.warn("Insured client does not exist." + insuredData.getClientId());
					throw new UnderWriterException("Insured client does not exist." + insuredData.getClientId());
				}
			}
			
			
			
		}catch(SQLException e){
			LOGGER.error(CodeHelper.getStackTrace(e));
			
			throw new IUMException(e);
		}
		LOGGER.info("processClientData end 2");
	}

	// Inserts the owner and insured client data sheet to the CLIENT_DATA_SHEETS table of the database
	// closing of connection will be handled by the calling method
	private void processAssessmentRequestData(AssessmentRequestData arData) throws IUMException{
		
		LOGGER.info("processAssessmentRequestData start");
		ClientData owner = arData.getOwner();
		CDSMortalityRatingData ownerMortalityRating = arData.getOwnerMortalityRating();
		ClientData insured = arData.getInsured();
		CDSMortalityRatingData insuredMortalityRating = arData.getInsuredMortalityRating();

		// Set summation
		ownerMortalityRating.setTotalAssestMortAPDB(ownerMortalityRating.getAPDB() +
													ownerMortalityRating.getFamilyHistoryAPDB() +
													ownerMortalityRating.getSmokingAPDB());

		ownerMortalityRating.setTotalAssestMortBasic(ownerMortalityRating.getBasic() +
													 ownerMortalityRating.getFamilyHistoryBasic()+
													 ownerMortalityRating.getSmokingBasic());

		ownerMortalityRating.setTotalAssestMortCCR(ownerMortalityRating.getCCR()+
												   ownerMortalityRating.getFamilyHistoryCCR()+
												   ownerMortalityRating.getSmokingCCR());

		insuredMortalityRating.setTotalAssestMortAPDB(insuredMortalityRating.getAPDB() +
													  insuredMortalityRating.getFamilyHistoryAPDB()+
													  insuredMortalityRating.getSmokingAPDB());

		insuredMortalityRating.setTotalAssestMortBasic(insuredMortalityRating.getBasic()+
													   insuredMortalityRating.getFamilyHistoryBasic()+
													   insuredMortalityRating.getSmokingBasic());

		insuredMortalityRating.setTotalAssestMortCCR(insuredMortalityRating.getCCR()+
													 insuredMortalityRating.getFamilyHistoryCCR()+
													 insuredMortalityRating.getSmokingCCR());

		ClientDataSheetData ownerCDS = new ClientDataSheetData();
		ClientDataSheetData insuredCDS = arData.getClientDataSheet();

		String ownerClientId = "";
		String insuredClientId  = "";
		String referenceNumber = arData.getReferenceNumber();

		if (referenceNumber != null){
			
			// Insert/Update insured (related policies, client, mortality/cds)
			if (insuredCDS != null && insured != null){
				insuredClientId = insured.getClientId();
				
				if(insuredClientId !=null && !"".equals(insuredClientId)){
					
					insuredCDS.setClientId(insuredClientId);
					insuredCDS.setClientType(IUMConstants.CLIENT_TYPE_INSURED);
					insuredCDS.setReferenceNumber(referenceNumber);
					insuredCDS.setMortalityRating(insuredMortalityRating);
					insuredCDS.setSummaryOfPolicies(insuredCDS.getSummaryOfPolicies());
					
					// Insert related policies
					processRelatedPolicies(insuredCDS.getPolicyCoverageDetailsList(), insuredClientId);
					
					try{
						
						AssessmentRequestDAO arDao = new AssessmentRequestDAO();
						arDao.updateClientId(IUMConstants.CLIENT_TYPE_INSURED, insuredClientId, referenceNumber);
						
						ClientDataSheetDAO cdsDAO = new ClientDataSheetDAO();
						
						if (!isCDSExisting(referenceNumber, insuredClientId)){
							cdsDAO.insertMortalityRatingData(insuredCDS);
						} else {
							cdsDAO.updateMortalityRatingData(insuredCDS);
						}
						
						// Update Client Data Sheet
						cdsDAO.updateSummaryPolicyCoverageData(insuredCDS);
						
					} catch (SQLException e) {
						LOGGER.error(CodeHelper.getStackTrace(e));
						
						throw new IUMException(e);
					}
				}
			}

			// if (owner != null){
			// cris added the additional validation for checking if owner and insured are the same
			// since inserting the insured cds does not instantly reflect the changes in the database
			// thus causing Unique Constraint error 
			if (owner != null && insured!=null
					&& insured.getClientId() != null && owner.getClientId() != null
					&& !insured.getClientId().equals(owner.getClientId())) {
				
				ownerClientId = owner.getClientId();
				
				if(ownerClientId != null && !"".equals(ownerClientId)) {
					
					ownerCDS.setClientId(ownerClientId);
					ownerCDS.setClientType(IUMConstants.CLIENT_TYPE_OWNER);
					ownerCDS.setReferenceNumber(referenceNumber);
					ownerCDS.setMortalityRating(ownerMortalityRating);
					
					try{
						// added this to update assessment requests with  the owner retrieve from the abacus
						AssessmentRequestDAO arDao = new AssessmentRequestDAO();
						arDao.updateClientId(IUMConstants.CLIENT_TYPE_OWNER, ownerClientId, referenceNumber);
	
						// Checks the existence of the OWNER and INSURED client data sheet
						
						ClientDataSheetDAO cdsDAO = new ClientDataSheetDAO();
						if (!isCDSExisting(referenceNumber, ownerClientId)){
							cdsDAO.insertMortalityRatingData(ownerCDS);
						} else {
							cdsDAO.updateMortalityRatingData(ownerCDS);
						}
						
					} catch (SQLException e) {
						LOGGER.error(CodeHelper.getStackTrace(e));
						throw new IUMException(e);
					}
				}
			}
		}
		LOGGER.info("processAssessmentRequestData end");
	
	}

	// Inserts the kickout messages in the kickout messages table
	// Closing of connection will be handled by the calling method
	public void processKOMessages(AssessmentRequestData arData) throws IUMException{
		
		LOGGER.info("processKOMessages start");
		ArrayList koMsgList = arData.getKickOutMessages();
		if (koMsgList != null){
			if (koMsgList.size() > 0){
				try{
					KickOutMessageDAO koDAO = new KickOutMessageDAO();
					
					for (int i=0; i<koMsgList.size(); i++){
						KickOutMessageData koMessageData = (KickOutMessageData) koMsgList.get(i);
						koDAO.insertKOMessage(koMessageData);
					}
					
				} catch (SQLException e){
					LOGGER.error(CodeHelper.getStackTrace(e));
					throw new IUMException(e);
				} 
			}
		}
		LOGGER.info("processKOMessages end");
		
	}

	//	checks for existing client data sheet in the database
	private boolean isCDSExisting(String referenceNumber, String clientId) throws IUMException{
		
		LOGGER.info("isCDSExisting start");
		boolean result=false;
		
		try{
			ClientDataSheetDAO cdsDAO = new ClientDataSheetDAO();
			result = cdsDAO.isCDSExisting(referenceNumber,clientId);
		}catch(SQLException e){
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMException(e.getMessage());
		}
		LOGGER.info("isCDSExisting end");
		return result;
	}

	private boolean isPolicyDetailsExisting(String referenceNumber, String clientId, String policyNumber, long coverageNumber) throws IUMException{
		
		LOGGER.info("isPolicyDetailsExisting start");
		boolean result;
		
		try{
			ClientDataSheetDAO cdsDAO = new ClientDataSheetDAO();
			result = cdsDAO.isPolicyDetailsExisting(referenceNumber,clientId,policyNumber, coverageNumber);
		}catch(SQLException e){
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMException(e.getMessage());
		}
		LOGGER.info("isPolicyDetailsExisting end");
		return result;
	}

	private boolean passRequirementValidation(String referenceNumber, long operation) throws IUMException,UnderWriterException{
		
		LOGGER.info("passRequirementValidation start");
		ArrayList requirementList = getPolicyRequirements(referenceNumber);
		boolean result = true;

		int noOfRecords = requirementList.size();
		if (noOfRecords > 0){
			for (int i=0; i<noOfRecords; i++){
				PolicyRequirementsData polReqData = (PolicyRequirementsData) requirementList.get(i);
				long status = polReqData.getStatus().getStatusId();
				if (operation == IUMConstants.STATUS_APPROVED || operation == IUMConstants.STATUS_FOR_OFFER){
					if (status == IUMConstants.STATUS_ORDERED){
						LOGGER.warn("The request still has pending requirements.");
						throw new UnderWriterException("The request still has pending requirements.");
					}else{
						continue;
					}
				}
			}
		}
		LOGGER.info("passRequirementValidation end");
		return result;
	}

	private boolean passMedicalRecordValidation(String referenceNumber, long operation) throws IUMException{
		
		LOGGER.info("passMedicalRecordValidation start");
		ArrayList medRecList = getKOMedRecs(referenceNumber);
		boolean result = true;

		int noOfRecords = medRecList.size();
		if (noOfRecords > 0){
			for (int i=0; i<noOfRecords; i++){
				MedicalRecordData medData = (MedicalRecordData) medRecList.get(i);
				long medStatus = medData.getStatus().getStatusId();
				if (operation == IUMConstants.STATUS_APPROVED || operation == IUMConstants.STATUS_FOR_OFFER){
					if (medStatus == IUMConstants.STATUS_REQUESTED || medStatus == IUMConstants.STATUS_CONFIRMED || medStatus == IUMConstants.STATUS_EXPIRED || medStatus == IUMConstants.STATUS_NOT_SUBMITTED){
						LOGGER.warn("The request still has pending medical records.");
						throw new UnderWriterException("The request still has pending medical records.");
					} else {
						continue;
					}
				}
			}

		}
		LOGGER.info("passMedicalRecordValidation end");
		return result;
	}

	/**
	 * This method is associated with MessageController.processPolicyDetails(AssessmentRequestData).
	 * The following data will be inserted in the database if it will pass validation:
	 * ownerClientData, insuredClientData, insuredMortalityRating, ownerMortalityRating, insuredCDS,
	 * ownerCDS, and kickoutMessages.
	 * @param retrievedARData assesment request data that will be processed
	 * @throws Exception
	 */
	public void transmitMQDataToIUMDB(AssessmentRequestData retrievedARData) throws Exception {
		
		LOGGER.info("transmitMQDataToIUMDB start");
		Connection conn = null;
		try {
			conn = new DataSourceProxy().getConnection();
			conn.setAutoCommit(false);
			ClientData ownerClientData = retrievedARData .getOwner();
			ClientData insuredClientData = retrievedARData .getInsured();

			processClientData(ownerClientData,insuredClientData);
			processAssessmentRequestData(retrievedARData);
			processKOMessages(retrievedARData);
			conn.commit();	
		} catch (Exception e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			try {
				if (conn != null){
					conn.rollback();
				}
			} catch (SQLException e1) {
				LOGGER.error(CodeHelper.getStackTrace(e1));
			}
			throw new IUMException(e);
		} finally {
			closeConnection(conn);
			LOGGER.info("transmitMQDataToIUMDB end");
		}
		
	}
	
	/**
	 * The calling method will be the one to handle the connection.
	 * @param retrievedARData
	 * @param conn
	 * @throws Exception
	 */
	public void transmitIngeniumDataToIUMDB(AssessmentRequestData retrievedARData, Connection conn) throws Exception {
		
		LOGGER.info("transmitIngeniumDataToIUMDB start");
		try {
			ClientData ownerClientData = retrievedARData .getOwner();
			ClientData insuredClientData = retrievedARData .getInsured();

			processClientData(ownerClientData,insuredClientData);

			processAssessmentRequestData(retrievedARData);

			processKOMessages(retrievedARData);

		} catch (Exception e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMException(e);
		}
		LOGGER.info("transmitIngeniumDataToIUMDB end");
	}
	
	/**
	 * The calling method will be the one to handle the connection.
	 * @param retrievedARData
	 * @param conn
	 * @throws Exception
	 */
	public void transmitIngeniumDataToIUMDBWMS(AssessmentRequestData retrievedARData) throws Exception {

		LOGGER.info("transmitIngeniumDataToIUMDBWMS start");
		try {
			ClientData ownerClientData = retrievedARData.getOwner();
			ClientData insuredClientData = retrievedARData.getInsured();
			processClientData(ownerClientData,insuredClientData,retrievedARData);
			processKOMessages(retrievedARData);

		} catch (Exception e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMException(e);
		}
		LOGGER.info("transmitIngeniumDataToIUMDBWMS end");
	}
	
	public void transmitIngeniumCDSDataToIUMDBWMS(AssessmentRequestData retrievedARData) throws Exception {
		
		LOGGER.info("transmitIngeniumCDSDataToIUMDBWMS start 1");
		try {
			ClientData ownerClientData = retrievedARData.getOwner();
			ClientData insuredClientData = retrievedARData.getInsured();
			processClientData(ownerClientData, insuredClientData);
			processAssessmentRequestData(retrievedARData);

		} catch (Exception e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMException(e);
		}
		LOGGER.info("transmitIngeniumCDSDataToIUMDBWMS end 1");
	}
	
	// Launch IUM Enhancement - Andre Ceasar Dacanay - start
	public void transmitIngeniumCDSDataToIUMDBWMS(
			final AssessmentRequestData retrievedARData,
			final String hasIUMRecord) throws Exception {
		
		LOGGER.info("transmitIngeniumCDSDataToIUMDBWMS start 2");
		try {
			processAssessmentRequestData(retrievedARData);
		} catch (Exception e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMException(e);
		}
		
		LOGGER.info("transmitIngeniumCDSDataToIUMDBWMS end 2");
	}
	// Launch IUM Enhancement - Andre Ceasar Dacanay - end
	
	public void transmitIngeniumPolicyCoverageDetailToIUMDBWMS(ArrayList policyCoverageDetailList) throws Exception {
		
		LOGGER.info("transmitIngeniumPolicyCoverageDetailToIUMDBWMS start");
		try{
			PolicyCoverageDetailDao policyDao = new PolicyCoverageDetailDao();
			for (int i=0; i < policyCoverageDetailList.size(); i++) {
				PolicyDetailData data = (PolicyDetailData) policyCoverageDetailList.get(i);
				if (!policyDao.isPolicyCoverageDetailExisting(data)) {
					policyDao.createPolicyCoverageDetail(data);
				}
			}
		}catch(SQLException e){
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMException(e);
		}
		LOGGER.info("transmitIngeniumPolicyCoverageDetailToIUMDBWMS end");
	}
	
	private void processRelatedPolicies(final ArrayList policies, final String insuredClientId) throws IUMException{
		
		LOGGER.info("processRelatedPolicies start");
		
		if (policies != null && policies.size() > 0) {
			
			try{
					
				ClientDataSheetDAO cdsDAO = new ClientDataSheetDAO();
				for (int i =0; i < policies.size(); i++){
					PolicyDetailData policyDetail = (PolicyDetailData) policies.get(i);
					boolean polDetailsExist =
							isPolicyDetailsExisting(
									policyDetail.getReferenceNumber(),
									insuredClientId,
									policyDetail.getPolicyNumber(),
									policyDetail.getCoverageNumber());
					
					if (!polDetailsExist) {
						policyDetail.setClientId(insuredClientId);
						cdsDAO.insertPolicyDetails(policyDetail);
					}
				}
				
			} catch (SQLException e){
				LOGGER.error(CodeHelper.getStackTrace(e));e.printStackTrace();
			}
		}
		
		LOGGER.info("processRelatedPolicies end");
	}


	/**
	 * Retrieves the client data
	 * @param client_id
	 * @return ClientData
	 * @throws IUMException
	 */
	public ClientData getClientData(String client_id) throws IUMException{
		
		LOGGER.info("getClientData start");
		
		ClientData cdata = new ClientData();

		try{
			ClientDAO cdao = new ClientDAO();
			cdata = cdao.retrieveClient(client_id);
		}catch(SQLException e){
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMException(e.getMessage());
		}
		LOGGER.info("getClientData end");
		return cdata;
	}


	private Date computeValidityDate(String requirementCode, Date dateConducted) throws IUMException{
		
		LOGGER.info("computeValidityDate start");
		int validityNum = 0;
		ReferenceDAO dao = new ReferenceDAO(new DataSourceProxy().getConnection());
		Date validityDate;
		
		try {
			validityNum = new Long(dao.retrieveRequirement(requirementCode).getValidity()).intValue();
			validityDate = DateHelper.add(dateConducted, Calendar.DATE, validityNum);
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMException(e.getMessage());
		} finally {
			dao.closeDB();
		}
		
		LOGGER.info("computeValidityDate end");
		return validityDate;
	}

	private boolean isEmpty(String object){
		
		LOGGER.info("isEmpty start");
		boolean result = false;
		if (object != null){
			if (object.equals("")){
				result = true;;
			}
		}else {
			result = true;
		}
		LOGGER.info("isEmpty end");
		return result;
	}

	/**
	 * Set the remarks of the assessment request
	 * @param referenceNumber assessment request reference number
	 * @param remarks
	 * @param updatedBy user that updated the assessment request
	 * @throws IUMException
	 */

	public void setRemarks(String referenceNumber,String remarks,String updatedBy) throws IUMException {
		
		LOGGER.info("setRemarks start");
		
		try{
			
			AssessmentRequestDAO arDAO = new AssessmentRequestDAO();
			arDAO.updateRemarks(referenceNumber, remarks, updatedBy);
			
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMException(e);
		}
		LOGGER.info("setRemarks end");
	}

	/**
	 * Reassigned the assessment request to another underwriter and sends notification to the previous underwriter and
	 * newly assigned underwriter.
	 * @param referenceNumber assessment request reference number
	 * @param underwriter underwriter whom the assessment request will be reassigned to
	 * @param user user that updated the assessment request
	 * @throws IUMException
	 * @throws UnderWriterException
	 */
	public void reassignUnderwriter(String referenceNumber, String underwriter, String user) throws IUMException,UnderWriterException{
		
		LOGGER.info("reassignUnderwriter start");
		ArrayList recipient = new ArrayList();
		recipient.add(underwriter);

		AssessmentRequestData arData = new AssessmentRequestData();
		arData = getDetails(referenceNumber);	

		long status = arData.getStatus().getStatusId();
		String previousUW = arData.getUnderwriter().getUserId();
		if(previousUW.equals(underwriter)){
			LOGGER.warn("Cannot be reassigned to the same underwriter");
			throw new UnderWriterException("Cannot be reassigned to the same underwriter");
		}

		boolean isManager = isUserValid(arData.getAssignedTo().getUserId(), user);
	
		if (!isManager && (status!=IUMConstants.STATUS_FOR_ASSESSMENT)) {
			LOGGER.warn("You do not have sufficient rights to reassign this assessment request");
			throw new UnderWriterException("You do not have sufficient rights to " +
										   "reassign this assessment request");
		}

		String lob = arData.getLob().getLOBCode();
		recipient.add(previousUW);
		if (isManager || (status==IUMConstants.STATUS_FOR_ASSESSMENT)) {
			
			try{
				
				AssessmentRequestDAO ard = new AssessmentRequestDAO();
				ard.reassignedTo(referenceNumber, underwriter, underwriter, user);
				WorkflowItem wfi = new WorkflowItem();
				Workflow workflow = new Workflow();
				wfi.setObjectID(referenceNumber);
				wfi.setSender(user);
				wfi.setPreviousStatus(String.valueOf(status));
				wfi.setType(IUMConstants.ASSESSMENT_REQUEST);
				wfi.setLOB(lob);
				wfi.setBranchId(arData.getBranch().getOfficeId());
				wfi.setPrimaryRecipient(recipient);
				workflow.processState(wfi);
			} catch (Exception e){
				LOGGER.error(CodeHelper.getStackTrace(e));
				throw new IUMException(e);
			} 
		} else {
			LOGGER.warn("Status of the request is invalid");
			throw new UnderWriterException("Status of the request is invalid");
		}
		LOGGER.info("reassignUnderwriter end");
	}


	/**
	 * Saves the changes made to the assessmnet request
	 * @param request assessment request data object
	 * @throws UnderWriterException
	 * @throws IUMException
	 */
	public void maintainAssessmentRequest(AssessmentRequestData request) throws UnderWriterException, IUMException {
		
		LOGGER.info("maintainAssessmentRequest start");
		Connection conn = null;
		try{
			conn = new DataSourceProxy().getConnection();
			conn.setAutoCommit(false);
			
			ClientData ins = request.getInsured();
			ClientData own = request.getOwner();
			
			if (ins != null) {
				String lob = request.getLob().getLOBCode();
				String insured = ins.getClientId();
				
				ClientDAO clientDAO = new ClientDAO();
				ClientData insuredClientData = clientDAO.retrieveClient(insured);

				if (insuredClientData == null) {
					
					clientDAO.insertClient(request.getInsured());
				}
				else {
					
					clientDAO.updateClient(request.getInsured());
				}

				if (own != null) {
					String owner = own.getClientId();
					
					if ((lob.equals(IUMConstants.LOB_INDIVIDUAL_LIFE)) && (owner != null) && (!owner.equals(insured))) {
						ClientData ownerClientData = clientDAO.retrieveClient(owner);
						if (ownerClientData == null) {
							clientDAO.insertClient(request.getOwner());
						}
						else {
							
							clientDAO.updateClient(request.getOwner());
						}
					}
				}
			}

			//update assessment_requests table
			AssessmentRequestDAO arDAO = new AssessmentRequestDAO();
			
			arDAO.updateAssessmentRequest(request);
			conn.commit();
		}
		catch (SQLException ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			try {
				if (conn != null){
					conn.rollback();
				}
			} catch (SQLException e) {
				LOGGER.error(CodeHelper.getStackTrace(e));
			}
			throw new IUMException(ex);
		}
		catch (Exception ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}finally{
		    closeConnection(conn);
		}
		LOGGER.info("maintainAssessmentRequest end");
	}


	/**
	 * Sets the value for the transmit indicator of the assessment request
	 * @param referenceNumber assessment request reference number
	 * @param result value to be set to the transmit indicator
	 * @throws IUMException
	 */
	public void setSuccessfulRetrieval(String referenceNumber, String result) throws IUMException{
		
		LOGGER.info("setSuccessfulRetrieval start 1");
		Connection conn = null;
		try{
			conn = new DataSourceProxy().getConnection();
			setSuccessfulRetrieval(referenceNumber, result, conn);	
		} finally {
			closeConnection(conn);
		}
		LOGGER.info("setSuccessfulRetrieval end 1");
	}
	
	/**
	 * Sets the value for the transmit indicator of the assessment request
	 * @param referenceNumber assessment request reference number
	 * @param result value to be set to the transmit indicator
	 * @throws IUMException
	 */
	public void setSuccessfulRetrieval(String referenceNumber, String result, Connection conn) throws IUMException{
		
		LOGGER.info("setSuccessfulRetrieval start 2");
		
		try{
			
			AssessmentRequestDAO dao = new AssessmentRequestDAO();
			dao.updateTransmit(referenceNumber,result);
			
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMException(e);
		} 
		
		LOGGER.info("setSuccessfulRetrieval end 2");
		
	}

	/**
	 * Sets the auto-settle indicator of this assessment request
	 * @param referenceNumber assessment request reference number
	 * @param result value to be set to the auto-settle indicator
	 * @throws IUMException
	 */
	public void setAutoSettleIndicator(String referenceNumber, String result) throws IUMException{
		
		LOGGER.info("setAutoSettleIndicator start");
		
		try{
			
			AssessmentRequestDAO dao = new AssessmentRequestDAO();
			dao.updateAutoSettleIndicator(referenceNumber,result);
			
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMException(e);
		} 
			LOGGER.info("setAutoSettleIndicator end");
		
		
	}


	/**
	 * Deletes assessment request within the range set.
	 * @param purgeDate
	 * @param startDate
	 * @param endDate
	 * @return no. of assessment requests deleted
	 * @throws IUMException
	 * @throws SQLException 
	 */
	public int purgeAssessmentRequests(Date purgeDate, Date startDate, Date endDate) throws IUMException{
		
		LOGGER.info("purgeAssessmentRequests start");
		int recordCount = 0;
		String result = "F";
		Connection conn = null; 
		try{
			conn = new DataSourceProxy().getConnection();
			AssessmentRequestDAO ARDAO = new AssessmentRequestDAO();
			ActivityDAO activityDAO = new ActivityDAO(conn);
			ClientDataSheetDAO CDSDAO = new ClientDataSheetDAO();
			DocumentDAO documentDAO = new DocumentDAO(conn);
			ImpairmentDAO impairmentDAO = new ImpairmentDAO(conn);
			KickOutMessageDAO KOMassageDAO = new KickOutMessageDAO();
			MedicalDAO medicalDAO = new MedicalDAO(conn);
			PolicyRequirementDAO polReqtDAO = new PolicyRequirementDAO();

			ArrayList list = ARDAO.retrieveAssessmentRequests(startDate, endDate);
			conn.setAutoCommit(false);
			for (int i = 0; i < list.size(); i++) {
				String referenceNum = (String) list.get(i);
				activityDAO.deleteActivityLogs(referenceNum);
				CDSDAO.deleteCDS(referenceNum);
				CDSDAO.deletePolicyCoverageDetails(referenceNum);
				documentDAO.deleteDocuments(referenceNum);
				impairmentDAO.deleteImpairments(referenceNum);
				KOMassageDAO.deleteKOMessages(referenceNum);
				medicalDAO.deleteMedicalBills(referenceNum);
				medicalDAO.deleteMedicalNotes(referenceNum);
				medicalDAO.deletePolicyMedicalRecords(referenceNum);
				polReqtDAO.deletePolicyRequirements(referenceNum);
				conn.commit();
			}
			

			recordCount = ARDAO.deleteAssessmentRequests(startDate, endDate);
			conn.commit();
			result = "S";
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			try {
				conn.rollback();
			} catch (SQLException e1) {
				LOGGER.error(CodeHelper.getStackTrace(e));
			}
			throw new IUMException(e);
		} finally{
			PurgeStatisticData data = new PurgeStatisticData();
			data.setPurgeDate(purgeDate);
			data.setStartDate(startDate);
			data.setEndDate(endDate);
			data.setCriteria(IUMConstants.PURGE_ASSESSMENT_REQUESTS);
			data.setNumberOfRecords(recordCount);
			data.setResult(result);

			PurgeStatisticDAO purgeDao = new PurgeStatisticDAO(conn);
			try {
				purgeDao.insertPurgeStatistic(data);
				conn.commit();
			} catch (SQLException e) {
				LOGGER.error(CodeHelper.getStackTrace(e));
				try {
					conn.rollback();
				} catch (SQLException e1) {
					LOGGER.error(CodeHelper.getStackTrace(e));
				}
				throw new IUMException(e);
			} finally {
				closeConnection(conn);
			}
		}
		LOGGER.info("purgeAssessmentRequests end");
		return recordCount;
	}



	private boolean isAssignedToValid(String assignedTo, long operation) throws UnderWriterException{
		
		LOGGER.info("isAssignedToValid start");
		boolean result = true;
		if (operation == IUMConstants.STATUS_FOR_APPROVAL || operation == IUMConstants.STATUS_FOR_TRANSMITTAL_USD){
			if (assignedTo.equals("")){
				LOGGER.warn("\"Assigned To\" is required");
				throw new UnderWriterException("\"Assigned To\" is required");
			}
		}
		LOGGER.info("isAssignedToValid end");
		return result;
	}


	/**
	 * Returns true if status is an end status
	 * @param lob line of business of the assessmnet request
	 * @param fromStatus previous status of the assessment request
	 * @return boolean
	 * @throws IUMException
	 */
	public boolean isEndStatus(String lob, long fromStatus) throws IUMException {
		
		LOGGER.info("isEndStatus start");
		
		try{
			ProcessConfigDAO dao = new ProcessConfigDAO();
			boolean isEndStatus = dao.isEndStatus(lob, IUMConstants.STAT_TYPE_AR, fromStatus);
			
			LOGGER.info("isEndStatus end");
			return isEndStatus;
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMException(e);
		}
	}


	/**
	 * Updates the assessment request date forward date
	 * @param referenceNumber assessment request reference number
	 * @param dateForwarded forward date
	 * @param updatedBy user updating the assessment request
	 */
	public void updateDateForwarded(String referenceNumber, Date dateForwarded, String updatedBy) throws IUMException {
		
		LOGGER.info("updateDateForwarded start");
		
		try{
			
			AssessmentRequestDAO arDAO = new AssessmentRequestDAO();
			arDAO.updateDateForwarded(referenceNumber, dateForwarded, updatedBy);
			 
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMException(e);
		} 
			LOGGER.info("updateDateForwarded end");
		
	}


	/**
	 * Retrieves the underwriter assessment request details
	 * @param referenceNumber assessment request reference number
	 * @return UWAssessmentData
	 */
	public UWAssessmentData getUWAssessmentDetails(final String referenceNumber) throws IUMException {
		
		LOGGER.info("getUWAssessmentDetails start");
		
		try{
			
			AssessmentRequestDAO arDAO = new AssessmentRequestDAO();
			UWAssessmentData uwData = arDAO.retrieveUWAssessmentDetails(referenceNumber);
			
			LOGGER.info("getUWAssessmentDetails end");
			return (uwData);
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMException(e);
		} 
			
		
	}


	/**
	 * Retrieves the underwriter assessment request details
	 * @param data assessment request data
	 */
	public void updateUWAssessmentDetails(UWAssessmentData data) throws IUMException {
		
		LOGGER.info("updateUWAssessmentDetails start");
		
		try{
			
			AssessmentRequestDAO arDAO = new AssessmentRequestDAO();

			if (data.getAnalysis() != null && !"".equals(data.getAnalysis().trim())){
				arDAO.insertUWAssessmentRequestAnalysis(data);
			}
			
			arDAO.updateUWAssessmentRequestRemarks(data);
			
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMException(e);
		} 
		
		LOGGER.info("updateUWAssessmentDetails end");
		
		
	}


	/**
	 * Returns true if the underwriter assessment exists
	 * @param referenceNumber assessment request reference number
	 * @return boolean
	 */
	public boolean isDuplicateUWAssessment(String referenceNumber) throws IUMException {
		
		LOGGER.info("isDuplicateUWAssessment start");
		boolean isDuplicate = false;
		
		try {
			AssessmentRequestDAO arDAO = new AssessmentRequestDAO();

			if (arDAO.isDuplicateUWAssessment(referenceNumber)) {
				isDuplicate = true;
			}
			
			LOGGER.info("isDuplicateUWAssessment end");
			return isDuplicate;
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMException(e);
		} 
		
	}


	/**
	 * Changes the status of the assessment request to Awaiting Requirements and set the assessment request "assignedTo"
	 * to the value defined in the assignedTo parameter. Process configuration checking is also applied in this method.
	 * @param referenceNumber assessment request reference number
	 * @param assignedTo  user whom the assessment request will be assigned to
	 * @param updatedBy user that updated the assessment request
	 * @throws IUMException
	 */
	public void awaitingRequirements(String referenceNumber, String assignedTo, String updatedBy) throws IUMException{
		
		LOGGER.info("awaitingRequirements start");
		long operation = IUMConstants.STATUS_AWAITING_REQUIREMENTS;
		AssessmentRequestData arData = new AssessmentRequestData();

		arData = getDetails(referenceNumber);
		long previousStatus = arData.getStatus().getStatusId();
		String lob = arData.getLob().getLOBCode();
		boolean passStatusValidation = false;

		try{
			ProcessConfigDAO processDao = new ProcessConfigDAO();
			passStatusValidation = processDao.isValidStatus(lob,IUMConstants.STAT_TYPE_AR,previousStatus,operation);
		} catch (SQLException e1) {
			LOGGER.error(CodeHelper.getStackTrace(e1));
			throw new IUMException(e1);
		} 

		if (passStatusValidation == false){
			LOGGER.warn("The status of the request is invalid");
			throw new UnderWriterException("The status of the request is invalid");
		} else {
			assignTo(referenceNumber,assignedTo,updatedBy);
			changeStatus(referenceNumber,operation,updatedBy);
		}
		LOGGER.info("awaitingRequirements end");
	}

	/**
	 * Changes the status of the assessment request to Awaiting Medical, changes the assessment request "assignedTo" to the parameter
	 * assignedTo. Process configuration checking is also applied in this method.
	 * @param referenceNumber assessment request reference number
	 * @param assignedTo  user whom the assessment request will be assigned to
	 * @param updatedBy  user that updated the assessment request
	 * @throws IUMException
	 */
	public void awaitingMedical(String referenceNumber, String assignedTo, String updatedBy) throws IUMException{
		
		LOGGER.info("awaitingMedical start");
		long operation = IUMConstants.STATUS_AWAITING_MEDICAL;
		AssessmentRequestData arData = new AssessmentRequestData();

		arData = getDetails(referenceNumber);
		long previousStatus = arData.getStatus().getStatusId();
		String lob = arData.getLob().getLOBCode();

		boolean passStatusValidation = false;
		
		try{
			ProcessConfigDAO processDao = new ProcessConfigDAO();
			passStatusValidation = processDao.isValidStatus(lob,IUMConstants.STAT_TYPE_AR,previousStatus,operation);
		} catch (SQLException e1) {
			LOGGER.error(CodeHelper.getStackTrace(e1));
			throw new IUMException(e1);
		} 

		if (passStatusValidation == false){
			LOGGER.warn("The status of the request is invalid");
			throw new UnderWriterException("The status of the request is invalid");
		} else {
			assignTo(referenceNumber,assignedTo,updatedBy);
			changeStatus(referenceNumber,operation,updatedBy);
		}
		LOGGER.info("awaitingMedical end");
	}


	/**
	 * Retrieve list of roles given the TO status and lob. Return list as ArrayList.
	 * @param frStatus from status of assessment request
	 * @param toStatus to status of assessment request
	 * @param lob line of business
	 * @return ArrayList
	 */
	public ArrayList retrieveAssignToRoles(String frStatus, String toStatus, String lob) throws IUMException {
		
		LOGGER.info("retrieveAssignToRoles start");
		Connection conn = null;
		try {
			conn = new DataSourceProxy().getConnection();
			ProcessConfigRoleDAO dao = new ProcessConfigRoleDAO(conn);
			ArrayList roles = dao.retrieveAssignToRoles(frStatus, toStatus, lob);
			
			return (roles);
		} catch (SQLException ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		} catch (Exception ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		} finally {
			closeConnection(conn);
			LOGGER.info("retrieveAssignToRoles end");
		}
	}//retrieveAssignToRoles


	/**
	 * Retrieve list of roles given the TO status and lob. Return list as String, separated by commas.
	 * @param frStatus
	 * @param toStatus
	 * @param lob
	 * @return ArrayList
	 */
	public String retrieveAssignToRolesFilter(String frStatus, String toStatus, String lob) throws IUMException {
		
		LOGGER.info("retrieveAssignToRolesFilter start");
		Connection conn = null;
		try {
			conn = new DataSourceProxy().getConnection();
		
			ArrayList roles = retrieveAssignToRoles(frStatus, toStatus, lob);
			String filter = "";
			int size = roles.size();
			for (int i=0; i<size; i++) {
			  filter += roles.get(i);
			  if (i < size-1) {
				filter += ",";
			  }
			}
			
			return (filter);
		} catch (IUMException ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw ex;
		} catch (Exception ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		} finally {
			closeConnection(conn);
			LOGGER.info("retrieveAssignToRolesFilter end");
		}
		
	}//retrieveAssignToRolesFilter


	private void closeConnection(Connection conn)throws IUMException{
		
		
		try{
			if(conn!=null){
				conn.close();
			}
		}catch(SQLException e){
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMException(e.getMessage());
		}
		
	}

	/**
	 * Changes the status of the assessment request to For Referral, changes the assignedTo of the assessment request with the value of
	 * the parameter asssignedTo, and sends notification. Process configuration checking is also applied in this method.
	 * @param referenceNumber assessment request reference number
	 * @param assignedTo user whom the assessment request will be assigned to
	 * @param user user that updated the assessment request
	 * @throws IUMException
	 * @throws UnderWriterException
	 */
	public void forReferral (String referenceNumber, String assignedTo, String user) throws IUMException, UnderWriterException{
		
		LOGGER.info("forReferral start");
		long operation = IUMConstants.STATUS_AR_FOR_REFERRAL;
		AssessmentRequestData arData = new AssessmentRequestData();

		arData = getDetails(referenceNumber);
		long previousStatus = arData.getStatus().getStatusId();
		String lob = arData.getLob().getLOBCode();

		ArrayList recipient = new ArrayList();
		recipient.add(assignedTo);
		boolean passStatusValidation = false;

		try {
			ProcessConfigDAO processDao = new ProcessConfigDAO();
			passStatusValidation = processDao.isValidStatus(lob,IUMConstants.STAT_TYPE_AR,previousStatus,operation);
		} catch (SQLException e1) {
			throw new IUMException(e1);
		} 
		
		if (passStatusValidation == false){
			LOGGER.warn("The status of the request is invalid");
			throw new UnderWriterException("The status of the request is invalid");
		}else {
			assignTo(referenceNumber,assignedTo,user);
			changeStatus(referenceNumber,operation,user);

		try{
			WorkflowItem wfi = new WorkflowItem();
			Workflow workflow = new Workflow();
			wfi.setSender(user);
			wfi.setObjectID(arData.getReferenceNumber());
			wfi.setPreviousStatus(String.valueOf(previousStatus));
			wfi.setType(IUMConstants.ASSESSMENT_REQUEST);
			wfi.setLOB(lob);
			wfi.setBranchId(arData.getBranch().getOfficeId());
			wfi.setPrimaryRecipient(recipient);
			workflow.processState(wfi);
			} catch (Exception e){
				LOGGER.error(CodeHelper.getStackTrace(e));
				throw new IUMException(e);
			}
		}
		LOGGER.info("forReferral end");
	}

	/**
	 * Changes the assignedTo of the assessment request to user_id "RECORDS". The current status of the
	 * assessment request should be an end status (Approved, Declined , etc).
	 * @param referenceNumber assessment request reference number
	 * @param user user that updated the assessment request
	 * @throws IUMException
	 */
	public void forwardToRecords(String referenceNumber, String user) throws IUMException{
		
		LOGGER.info("forwardToRecords start");
		AssessmentRequestData arData = getDetails(referenceNumber);
		String records_id = "RECORDS";
		long status = arData.getStatus().getStatusId();
		if ((status == IUMConstants.STATUS_APPROVED) || (status == IUMConstants.STATUS_AR_CANCELLED) ||
			(status == IUMConstants.STATUS_DECLINED) || (status == IUMConstants.STATUS_FOR_OFFER) ||
			(status == IUMConstants.STATUS_NOT_PROCEEDED_WITH) ){
				
				AssessmentRequestDAO dao = new AssessmentRequestDAO();
				try {
					dao.changeAssignedTo(referenceNumber, records_id, user);
				
				} catch (SQLException e) {
					throw new IUMException(e);
				}
				
		} else {
			LOGGER.warn("The status of the request is invalid");
			throw new UnderWriterException("The status of the request is invalid");
		}
		LOGGER.info("forwardToRecords end");
	}

	public ArrayList retrieveUWAnalysis(
			final String referenceNumber) throws IUMException{
		
		LOGGER.info("retrieveUWAnalysis start");
		ArrayList list = new ArrayList();
		
		AssessmentRequestDAO dao = new AssessmentRequestDAO();

		try {
			list = dao.retrieveUWAnalysis(referenceNumber);
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMException(e);
		}
		
		LOGGER.info("retrieveUWAnalysis end");
		return list;
	}

	public void genericChangeARStatus(String referenceNumber, long operation, String assignedTo, String user) throws IUMException{
		
		LOGGER.info("genericChangeARStatus start");
		AssessmentRequestData arData = new AssessmentRequestData();

		ArrayList recipient = new ArrayList();
		recipient.add(assignedTo);

		arData = getDetails(referenceNumber);
		String lob = arData.getLob().getLOBCode();
		long previousStatus = arData.getStatus().getStatusId();

		
		boolean passStatusValidation = false;

		try {
			ProcessConfigDAO configDAO = new ProcessConfigDAO();
			passStatusValidation = configDAO.isValidStatus(lob, IUMConstants.STAT_TYPE_AR,previousStatus,operation);
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMException(e);
		} 

		if (passStatusValidation == false){
			LOGGER.warn("The status of the request is invalid");
			throw new UnderWriterException("The status of the request is invalid");
		}else {
			assignTo(referenceNumber,assignedTo,user);
			changeStatus(referenceNumber,operation,user);
		}

		try{
			WorkflowItem wfi = new WorkflowItem();
			Workflow workflow = new Workflow();
			wfi.setSender(user);
			wfi.setObjectID(arData.getReferenceNumber());
			wfi.setPreviousStatus(String.valueOf(previousStatus));
			wfi.setType(IUMConstants.ASSESSMENT_REQUEST);
			wfi.setLOB(lob);
			wfi.setBranchId(arData.getBranch().getOfficeId());
			wfi.setPrimaryRecipient(recipient);
			workflow.processState(wfi);
			} catch (Exception e){
				LOGGER.error(CodeHelper.getStackTrace(e));
				throw new IUMException(e);
			}
		LOGGER.info("genericChangeARStatus end");
	}

	/**
	 * Checks if the user is allowed to reassign the AR given that he has a
	 * managerial position
	 *
	 * @param userFromAR current user assigned to the AR
	 * @param user user that updated the assessment request
	 * @throws IUMException
	 */
	private boolean isUserValid(String userFromAR,
								String user)
								throws IUMException {
		
		LOGGER.info("isUserValid start");
		
		UserDAO userDAO = null;
		boolean userValid = true;

		try {
			
			userDAO = new UserDAO();
			userValid = userDAO.isUserAllowedToAssign(user);
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMException(e);
		} 
		LOGGER.info("isUserValid end");
		return userValid;
	}


  	/**
  	 * FOR INGENIUM
  	 * Gets the policy suffix given the reference number of the AR
	 * @param referenceNumber
	 * @return
	 * @throws IUMException
	 */
	public String getPolicySuffix(String referenceNumber) throws IUMException{
		
		LOGGER.info("getPolicySuffix start");
		
	  	try {
	  		
			AssessmentRequestDAO arDAO = new AssessmentRequestDAO();
			
			LOGGER.info("getPolicySuffix end");
			return arDAO.retrievePolicySuffix(referenceNumber);
			
	  	} catch (SQLException e) {
	  		LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMException(e);
	  	} 
	  	
	  	
  	}
	

	
  	/**
  	 * Checks if the AR is existing
	 * @param referenceNumber
	 * @return
	 * @throws IUMException
	 */
	public boolean isAssessmentRequestExist(
			final String referenceNumber
			) throws IUMException{

		LOGGER.info("isAssessmentRequestExist start 2");
		
		try {
			AssessmentRequestDAO arDAO = new AssessmentRequestDAO();
			LOGGER.info("isAssessmentRequestExist end 2");
			
			return arDAO.isPolicyExists(referenceNumber);
	  	} catch (SQLException e) {
	  		LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMException(e);
	  	} 
		
  	}	
	
	/**
	 * Returns if retrieval from the source systems are finished based on the AR_TRANSMIT_IND 
	 * contained in the database
	 * @param referenceNum
	 * @return 
	 * 	AR_TRANSMIT_IND = Y  - true 
	 *  AR_TRANSMIT_IND = N or empty - false
	 * @throws IUMException
	 */
	public boolean isFinishRetrieval(String referenceNum) throws IUMException{
		
		LOGGER.info("isFinishRetrieval start");
		boolean finishRetrieval = false;
	  	
	  	try {
	  		AssessmentRequestDAO arDAO = new AssessmentRequestDAO();
			String status = arDAO.retrieveARRetrievalStatus(referenceNum);
			if (StringHelper.isEmpty(status) || StringHelper.isEqual(status, IUMConstants.INDICATOR_FAILED)){
				finishRetrieval = false;
			} else if (StringHelper.isNotEmpty(status) && StringHelper.isEqual(status, IUMConstants.INDICATOR_SUCCESS)){
				finishRetrieval = true;
			}
	  	} catch (SQLException e) {
	  		LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMException(e);
	  	}
	  	
	  	LOGGER.info("isFinishRetrieval end");
	  	return finishRetrieval;
  	}
	
	public void updateKOandReqtCountInd(AssessmentRequestData request) throws UnderWriterException, IUMException {
		
		LOGGER.info("updateKOandReqtCountInd start");
		
		try{
			AssessmentRequestDAO arDAO = new AssessmentRequestDAO();
			arDAO.updateKOreqtCountInd(request);
		}
		
		catch (SQLException ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		catch (Exception ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}   
		LOGGER.info("updateKOandReqtCountInd end");
		
	
	}
	
	public boolean isReqtCountExceeded(String referenceNumber) throws IUMException{
		
		LOGGER.info("isReqtCountExceeded start");
		boolean result = false;
		String ind = "";
		
	  	try {
		
			AssessmentRequestDAO arDAO = new AssessmentRequestDAO();
			ind = arDAO.retrieveReqtCountInd(referenceNumber);
			if(ind!=null){
				if(ind.equals(IUMConstants.INDICATOR_SUCCESS)){
					result = true;
				}
			}
	  	} catch (SQLException e) {
	  		LOGGER.error(CodeHelper.getStackTrace(e));
		}
	  	
	  	LOGGER.info("isReqtCountExceeded end");
		return result;
	  	
  	}
	
	public boolean isKOCountExceeded(String referenceNumber) throws IUMException{
		
		LOGGER.info("isKOCountExceeded start");
		
		boolean result = false;
		String ind = "";
		
	  	try {
	  		
			AssessmentRequestDAO arDAO = new AssessmentRequestDAO();
			ind = arDAO.retrieveKOCountInd(referenceNumber);
			
			if(ind!=null){
				
				if(ind.equals(IUMConstants.INDICATOR_SUCCESS)){
					
					result = true;
				}
			}
	  	} catch (SQLException e) {
	  		LOGGER.error(CodeHelper.getStackTrace(e));
		} 
	  	
	  	LOGGER.info("isKOCountExceeded end");
		return result;
  	}
	
	public void addAnalysis(UWAssessmentData uwData) throws SQLException {
		
		LOGGER.info("addAnalysis start");
		AssessmentRequestDAO dao = null;
		
		try{
			dao =new AssessmentRequestDAO();
			dao.insertUWAssessmentRequestAnalysis(uwData);
			
		} catch (Exception e){
			LOGGER.error(CodeHelper.getStackTrace(e));
		} 
		
		LOGGER.info("addAnalysis end");
		
	}
}

