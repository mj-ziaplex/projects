/*
 * Created on Dec 22, 2003
 * package name = com.slocpi.ium.underwriter
 * file name    = PolicyRequirements.java
 */
package com.slocpi.ium.underwriter;

import java.sql.Connection;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.ResourceBundle;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ph.com.sunlife.wms.modules.DateUtil;

import com.slocpi.ium.IUMException;
import com.slocpi.ium.data.AssessmentRequestData;
import com.slocpi.ium.data.PolicyRequirementsData;
import com.slocpi.ium.data.StatusData;
import com.slocpi.ium.data.dao.AssessmentRequestDAO;
import com.slocpi.ium.data.dao.PolicyCoverageDetailDao;
import com.slocpi.ium.data.dao.PolicyRequirementDAO;
import com.slocpi.ium.data.dao.ProcessConfigDAO;
import com.slocpi.ium.data.dao.RequirementDAO;
import com.slocpi.ium.data.dao.UserDAO;
import com.slocpi.ium.data.dao.WorkflowDAO;
import com.slocpi.ium.data.util.DataSourceProxy;
import com.slocpi.ium.interfaces.IUMInterfaceException;
import com.slocpi.ium.interfaces.notification.Attachment;
import com.slocpi.ium.service.AdminSystem;
import com.slocpi.ium.service.AdminSystemFactory;
import com.slocpi.ium.service.ingenium.IngeniumGateway;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.DateHelper;
import com.slocpi.ium.util.IUMConstants;
import com.slocpi.ium.util.forms.RequirementForm;
import com.slocpi.ium.workflow.NotificationBuilder;
import com.slocpi.ium.workflow.Workflow;
import com.slocpi.ium.workflow.WorkflowItem;


/****************************************************
 * @author Engel
 * @version 1.0
 * TODO Class Description of PolicyRequirements.java
 * ***************************************************
 * @author Shellz - Sept 25, 2014
 * @version 1.1
 * TODO -Code clean up of commented codes and modify Cancel requirements method
 * 		-Included base policy no in assessment request obj
 * 		-Remove tempReqData variable as it is only being used to query pr_test_date
 * 		-Create a new method to query only the pr_test_date per requirement_id
 */
public class PolicyRequirements {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(PolicyRequirements.class);
	private String strNBStrucNotesSettleDate = "";
	
	public PolicyRequirements() {
	}
	
	public PolicyRequirements(String strNBStrucNotesSettleDate) {
		this.strNBStrucNotesSettleDate = strNBStrucNotesSettleDate;	 
	}

	/**
	 * Creates a policy requirement in the POLICY_REQUIREMENTS table. The requirement is also created in the ABACUS interface thru
	 * the MessageController.
	 * @param polRequirement PolicyRequirementsData
	 * @param request AssessmentRequestData
	 * @return requirement ID
	 * @throws IUMException
	 */
	public long createRequirement(PolicyRequirementsData polRequirement, AssessmentRequestData request, String userId) throws IUMException {
		
		LOGGER.info("createRequirement start");
		long requirementID = 0;
				 
		try {
			if (request.getStatus().getStatusId() != IUMConstants.STATUS_FOR_FACILITATOR_ACTION &&
				polRequirement.getRequirementCode().equalsIgnoreCase(IUMConstants.REQUIREMENT_CODE_RECEIVED_APPLICATION_BY_USD)) {
				throw new UnderWriterException("Invalid requirement code.");
			}
	
			if (polRequirement.getLevel().equalsIgnoreCase(IUMConstants.LEVEL_POLICY) ||
				polRequirement.getLevel().equalsIgnoreCase(IUMConstants.LEVEL_CLIENT)) {
	
				//checking for duplicate requirements
				//* allow duplicate except for REQUIREMENT_CODE_RECEIVED_APPLICATION_BY_USD
				if (polRequirement.getRequirementCode().equalsIgnoreCase(IUMConstants.REQUIREMENT_CODE_RECEIVED_APPLICATION_BY_USD) && this.hasDuplicateReqt(polRequirement)) {
					LOGGER.debug("Duplicate requirement : " + polRequirement.getRequirementId() + " " + polRequirement.getRequirementCode());
					
				} else {
		
					polRequirement.setCreateDate(new Date());
					if (polRequirement.isCompletedRequirementInd()) {
						StatusData statusData = new StatusData();
						statusData.setStatusId(IUMConstants.STATUS_RECEIVED_IN_SITE);
						polRequirement.setStatus(statusData);
						polRequirement.setReceiveDate(new Date());
					} else {
						if (polRequirement.getStatus()==null || polRequirement.getStatus().getStatusId() == 0) {
							StatusData statusData = new StatusData();
							statusData.setStatusId(IUMConstants.STATUS_NTO);
							polRequirement.setStatus(statusData);
						}
					}
					polRequirement.setStatusDate(new Date());
	
					PolicyRequirementDAO dao = new PolicyRequirementDAO();
					try {
						requirementID = dao.insertRequirement(polRequirement);
					} catch (SQLException sqlE){
						LOGGER.error(CodeHelper.getStackTrace(sqlE));
					} catch (Exception e) {
						LOGGER.error(CodeHelper.getStackTrace(e));
						throw new IUMException(e);
					}
					
					if (polRequirement.getRequirementCode().equalsIgnoreCase(IUMConstants.REQUIREMENT_CODE_RECEIVED_APPLICATION_BY_USD)){
						if (request.getLob().getLOBCode().equalsIgnoreCase(IUMConstants.LOB_INDIVIDUAL_LIFE)) {
							polRequirement.setRequirementId(requirementID);
							AdminSystem as = AdminSystemFactory.getAdminSystem(request.getSourceSystem());
							try {
								as.processCreateRequirement(polRequirement);
							} catch (IUMInterfaceException e) {
								LOGGER.error(CodeHelper.getStackTrace(e));
								throw new IUMException(e);
							}
							
						}					
					} else {
						ArrayList list = new ArrayList();
						polRequirement.setRequirementId(requirementID);
						list.add(polRequirement);
						orderRequirement(list,request, userId);
					}
				}	
			}
		} catch (Exception e){
			LOGGER.error(CodeHelper.getStackTrace(e));
		} 
		
		LOGGER.debug("Requirement ID: " + requirementID);
		LOGGER.info("createRequirement start");
		return requirementID;
	}


	public long updateRequirement(long reqId, PolicyRequirementsData data) throws IUMException {
		
		LOGGER.info("updateRequirement start");
		
		PolicyRequirementDAO dao = new PolicyRequirementDAO();
		try {
			dao.updateRequirement(reqId, data);
		} catch (Exception e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
		}
			
		LOGGER.info("updateRequirement end");
		return reqId;
	}

	/**
	 * Creates a requirement in IUM.
	 * @param requirement PolicyRequirementsData
	 * @param request AssessmentRequestData
	 * @return requirement ID
	 * @throws IUMException
	 */
	public long createRequirementIUM(PolicyRequirementsData requirement, AssessmentRequestData request) throws IUMException {
		
		LOGGER.info("createRequirementIUM start 1");
		long requirementID = 0;

		Connection conn = new DataSourceProxy().getConnection();
		try{
			requirementID = createRequirementIUM(requirement,request,conn);			
		} finally {
			closeConnection(conn);
		}

		LOGGER.info("createRequirementIUM end 1");
		return requirementID;
	}
	
	/**
	 * Creates a requirement in IUM.
	 * @param requirement PolicyRequirementsData
	 * @param request AssessmentRequestData
	 * @return requirement ID
	 * @throws IUMException
	 */
	public long createRequirementIUM(
			final PolicyRequirementsData requirement,
			final AssessmentRequestData request,
			final Connection connection) throws IUMException {
		
		LOGGER.info("createRequirementIUM start 2");
		long requirementID = 0;

		RequirementDAO requirementDAO = new RequirementDAO();
		try {
			requirement.setLevel(requirementDAO.retrieveRequirement(requirement.getRequirementCode()).getLevel());
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMException(e);
		}

		if (requirement.getLevel().equalsIgnoreCase(IUMConstants.LEVEL_POLICY)
				|| requirement.getLevel().equalsIgnoreCase(IUMConstants.LEVEL_CLIENT)) {
			
			if (requirement.getClientType().equalsIgnoreCase(IUMConstants.CLIENT_TYPE_INSURED)){
				requirement.setClientId(request.getInsured().getClientId());
			} else {
				requirement.setClientId(request.getOwner().getClientId());
			}
		
			
			if(requirement.getStatus().getStatusId()==IUMConstants.STATUS_RECEIVED_IN_SITE){
				requirement.setReceiveDate(DateHelper.sqlDate(new Date()));
			}
		
			PolicyRequirementDAO dao = new PolicyRequirementDAO();
			try {
				
				if (requirement.getRequirementCode().equalsIgnoreCase(IUMConstants.REQUIREMENT_CODE_RECEIVED_APPLICATION_BY_USD)
						&& this.hasDuplicateReqt(requirement)) {
					LOGGER.debug("Duplicate requirement : " + requirement.getRequirementId() + " " + requirement.getRequirementCode());
				} else {				
					requirementID = dao.insertRequirement(requirement);
					
				}
			} catch (SQLException sqlE){
				LOGGER.error(CodeHelper.getStackTrace(sqlE));
			}
			catch (Exception e) {
				LOGGER.error(CodeHelper.getStackTrace(e));
				throw new IUMException(e);
			} 
		}
		LOGGER.info("createRequirementIUM start 2");
		
		return requirementID;
	}	


	/**
	 * Sends Follow up notification for the list of requirements.
	 * @param requirements ArrayList of policy requirements
	 * @param request AssessmentRequestData
	 * @throws IUMException
	 */
	public void sendFollowUpRequirementNotification(ArrayList requirements, AssessmentRequestData request) throws IUMException{
		
		LOGGER.info("sendFollowUpRequirementNotification start");
		StringBuffer invalidRequirements = new StringBuffer();

		for (int i = 0; i < requirements.size(); i++) {
			PolicyRequirementsData policyRequirement = new PolicyRequirementsData();
			policyRequirement = (PolicyRequirementsData) requirements.get(i);

			if ((policyRequirement.getLevel().equalsIgnoreCase(IUMConstants.LEVEL_POLICY) ||
				policyRequirement.getLevel().equalsIgnoreCase(IUMConstants.LEVEL_CLIENT)) &&
				(policyRequirement.getStatus().getStatusId() == IUMConstants.STATUS_ORDERED) &&
				policyRequirement.getDateSent() != null &&
				DateHelper.compare(policyRequirement.getFollowUpDate(), new Date()) <= 0) {

				ArrayList recipients = new ArrayList();
				
				UserDAO userDAO = new UserDAO();
				PolicyRequirementDAO polDAO = new PolicyRequirementDAO();
				try {
					recipients = userDAO.selectUsersPerRoleBranch(IUMConstants.ROLES_MARKETING_STAFF, request.getBranch().getOfficeId());
					policyRequirement = polDAO.retrieveRequirement(policyRequirement.getRequirementId());
				} catch (Exception e) {
					LOGGER.error(CodeHelper.getStackTrace(e));
					throw new IUMException(e);
				} 
				recipients.add(request.getAgent().getUserId()); 

				WorkflowItem workflowItem = new WorkflowItem();
				workflowItem.setObjectID(policyRequirement.getRequirementId());
				workflowItem.setPreviousStatus(String.valueOf(IUMConstants.STATUS_ORDERED));
				workflowItem.setType(IUMConstants.POLICY_REQUIREMENTS);
				workflowItem.setLOB(request.getLob().getLOBCode());
				workflowItem.setSender(policyRequirement.getUpdatedBy());
				workflowItem.setBranchId(request.getBranch().getOfficeId());
				workflowItem.setPrimaryRecipient(recipients);

				NotificationBuilder nb = new NotificationBuilder();
				try {
					nb.sendFollowUpRequirement(policyRequirement, workflowItem);
				} catch (Exception e1) {
					LOGGER.error(CodeHelper.getStackTrace(e1));
					throw new IUMException(e1);
				}

			} else {
				if (invalidRequirements.length() > 0) {
					invalidRequirements.append(", ");
				}
				invalidRequirements.append(policyRequirement.getRequirementCode());
			}
		}

		// Throw invalid requirements
		if (invalidRequirements.length() > 0) {
			LOGGER.warn("Requirement/s " + invalidRequirements.toString() + " is/are invalid for follow-up.");
			throw new UnderWriterException("Requirement/s " + invalidRequirements.toString() + " is/are invalid for follow-up.");
		}
		LOGGER.info("sendFollowUpRequirementNotification end");
	}


	/**
	 * Saves the data needed to generate the attachments.
	 * @param referenceNumber reference number of the assessment request
	 * @param reqId requirement id of the policy requirement
	 * @throws IUMException
	 */
	public void saveAttachmentData(String referenceNumber, long reqId) throws IUMException{
		
		LOGGER.info("saveAttachmentData start 1");
		
		WorkflowDAO dao = new WorkflowDAO();
		try {
			dao.saveAttachmentHelper(referenceNumber,reqId);
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
		}
		
		LOGGER.info("saveAttachmentData end 1");
		
	}
	
	/**
	 * Saves the data needed to generate the attachments.
	 * @param referenceNumber reference number of the assessment request
	 * @param reqId requirement id of the policy requirement
	 * @throws IUMException
	 */
	public void saveAttachmentData(String referenceNumber, long reqId, Connection conn) throws IUMException{
		
		LOGGER.info("saveAttachmentData start 2");
		WorkflowDAO dao = new WorkflowDAO();

		try {
			dao.saveAttachmentHelper(referenceNumber,reqId);
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMException(e);
		} 
		LOGGER.info("saveAttachmentData end 2");
	}	

		/**
	 * Changes the status of the requirements included in the list to 'ORDERED'
	 * @param requirements ArrayList of policy requirements
	 * @param request AssessmentRequestData
	 * @throws IUMException
	 */
	public void orderRequirement(ArrayList requirements, AssessmentRequestData request, String userId) throws IUMException {
		
		LOGGER.info("orderRequirement start");
		StringBuffer invalidRequirements = new StringBuffer();
		ArrayList validRequirements = new ArrayList();
		
		
		for (int i = 0; i < requirements.size(); i++) {
			PolicyRequirementsData policyRequirement, tempReqData = new PolicyRequirementsData();
			policyRequirement = (PolicyRequirementsData) requirements.get(i);
			
			tempReqData = getRequirement(policyRequirement.getRequirementId());
			policyRequirement.setTestDate(tempReqData.getTestDate());

			if ((policyRequirement.getLevel().equalsIgnoreCase(IUMConstants.LEVEL_POLICY) ||
				policyRequirement.getLevel().equalsIgnoreCase(IUMConstants.LEVEL_CLIENT)) &&
				this.isStatusValid(request.getLob().getLOBCode(), policyRequirement.getStatus().getStatusId(), IUMConstants.STATUS_ORDERED)) 
			{
				
				PolicyRequirementDAO dao = new PolicyRequirementDAO();
				try {
					
					dao.changeStatus(policyRequirement.getRequirementId(), IUMConstants.STATUS_ORDERED, policyRequirement.getUpdatedBy());
					policyRequirement.setOrderDate(new Date());
					dao.updateOrderDate(policyRequirement.getRequirementId(), policyRequirement.getOrderDate());
					policyRequirement.setDateSent(new Date());
					dao.updateDateSent(policyRequirement.getRequirementId(), policyRequirement.getDateSent());
					
					policyRequirement.setFollowUpDate(this.computeFollowUpDate(policyRequirement.getFollowUpNumber(), policyRequirement.getReferenceNumber(), 
							userId, policyRequirement.getClientId()));
						
					int statInt = new Long(policyRequirement.getStatus().getStatusId()).intValue();

					try{
						if(policyRequirement!=null && policyRequirement.getReqtFollowUp()!=null && policyRequirement.getReqtFollowUp().equals("on")){
							dao.updateFollowUpDate(policyRequirement.getRequirementId(), policyRequirement.getFollowUpDate());
						}else if(statInt==new Long(IUMConstants.STATUS_SCC).intValue() ||
								statInt==new Long(IUMConstants.STATUS_NTO).intValue() ||
								statInt==new Long(IUMConstants.STATUS_SIR).intValue()){
							int reqtDefDueDaysInt = 10;
							try{
								ResourceBundle rb = ResourceBundle.getBundle(IUMConstants.IUM_CONFIG);
								String reqtDefDueDays = rb.getString("reqt_default_due_days");							
								reqtDefDueDaysInt = Integer.parseInt(reqtDefDueDays);
							}catch(Exception e){
								LOGGER.error("Please check dbconfig.properties for reqt_default_due_days.");
								LOGGER.error(CodeHelper.getStackTrace(e));
							}
							
							dao.updateFollowUpDate(policyRequirement.getRequirementId(), policyRequirement.getFollowUpDate());
							
						}
					}catch (Exception e) {
						LOGGER.error("********* policyRequirement.getFollowUpDate() e "+e.getMessage());
						LOGGER.error(CodeHelper.getStackTrace(e));
					}
					
					try{
						if(policyRequirement!=null && policyRequirement.getCommentTo()!=null && policyRequirement.getCommentTo().equals("Y")){
							dao.updateComments(policyRequirement.getRequirementId(), policyRequirement.getComments());
						}
					}catch (Exception e) {
						LOGGER.error("********* policyRequirement.getCommentTo() e "+e.getMessage());
						LOGGER.error(CodeHelper.getStackTrace(e));
					}
					
					//include all info
					
					policyRequirement = dao.retrieveRequirement(policyRequirement.getRequirementId());
				} catch (SQLException e) {
					LOGGER.error(CodeHelper.getStackTrace(e));
					throw new IUMException(e);
				}

				if (request.getStatus().getStatusId() != IUMConstants.STATUS_AWAITING_REQUIREMENTS) {
					AssessmentRequest assessmentRequest = new AssessmentRequest();
					assessmentRequest.changeStatus(policyRequirement.getReferenceNumber(),IUMConstants.STATUS_AWAITING_REQUIREMENTS, policyRequirement.getUpdatedBy());
					if (request.getAssignedTo() != null) {
						assessmentRequest.assignTo(policyRequirement.getReferenceNumber(), request.getAssignedTo().getUserId(), policyRequirement.getUpdatedBy());
					}
				}

				saveAttachmentData(policyRequirement.getReferenceNumber(), policyRequirement.getRequirementId());

				StatusData status = new StatusData();
				status.setStatusId(IUMConstants.STATUS_ORDERED);
				policyRequirement.setStatus(status);
				validRequirements.add(policyRequirement);

			} else {
				if (invalidRequirements.length() > 0) {
					invalidRequirements.append(", ");
				}
				invalidRequirements.append(policyRequirement.getRequirementCode());
			}
		}

		// Call message queue
		if (request.getLob().getLOBCode().equalsIgnoreCase(IUMConstants.LOB_INDIVIDUAL_LIFE) &&
			validRequirements.size() > 0) {
			AdminSystem as = AdminSystemFactory.getAdminSystem(request.getSourceSystem());
			try {
				as.processOrderRequirement(validRequirements);
			} catch (IUMInterfaceException e) {
				LOGGER.error(CodeHelper.getStackTrace(e));
				throw new IUMException(e);
			}
		}

		// Throw invalid requirements
		if (invalidRequirements.length() > 0) {
			LOGGER.warn("Requirement/s " + invalidRequirements.toString() + " is/are invalid for ordering.");
			throw new UnderWriterException("Requirement/s " + invalidRequirements.toString() + " is/are invalid for ordering.");
		}
		LOGGER.info("orderRequirement end");
		
	}
	
	
	/**
	 * Changes the status of the requirements included in the list to 'ORDERED'
	 * @param requirements ArrayList of policy requirements
	 * @param request AssessmentRequestData
	 * @throws IUMException
	 */
	public void updateFollowUpDate(ArrayList requirements, AssessmentRequestData request) throws IUMException {
		
		LOGGER.info("updateFollowUpDate start");
		
		StringBuffer invalidRequirements = new StringBuffer();

		for (int i = 0; i < requirements.size(); i++) {
			PolicyRequirementsData policyRequirement, tempReqData = new PolicyRequirementsData();
			policyRequirement = (PolicyRequirementsData) requirements.get(i);

			if ((policyRequirement.getLevel().equalsIgnoreCase(IUMConstants.LEVEL_POLICY) ||
				policyRequirement.getLevel().equalsIgnoreCase(IUMConstants.LEVEL_CLIENT)) &&
				this.isStatusValid(request.getLob().getLOBCode(), policyRequirement.getStatus().getStatusId(), IUMConstants.STATUS_ORDERED)) 
			{

				PolicyRequirementDAO dao = new PolicyRequirementDAO();
				
					try{
						dao.updateFollowUpDate(policyRequirement.getRequirementId(), policyRequirement.getFollowUpDate());
					}catch (Exception e) {
						LOGGER.error(CodeHelper.getStackTrace(e));
					}
					
					try {
						policyRequirement = dao.retrieveRequirement(policyRequirement.getRequirementId());
						maintainRequirement(policyRequirement, request);
					} catch (Exception e) {
						LOGGER.error(CodeHelper.getStackTrace(e));
						throw new IUMException(e);
					}
				
			} else {
				if (invalidRequirements.length() > 0) {
					invalidRequirements.append(", ");
				}
				invalidRequirements.append(policyRequirement.getRequirementCode());
			}
		}

		// Throw invalid requirements
		if (invalidRequirements.length() > 0) {
			LOGGER.warn("Requirement/s " + invalidRequirements.toString() + " is/are invalid for update.");
			throw new UnderWriterException("Requirement/s " + invalidRequirements.toString() + " is/are invalid for update.");
		}
		LOGGER.info("updateFollowUpDate end");
	}

	/**
	 * Change the status of the requirements included in the list to 'RECEIVED IN SITE'
	 * @param requirements ArrayList of policy requirements
	 * @param request AssessmentRequestData
	 * @throws IUMException
	 */
	public void receiveRequirement(ArrayList requirements, AssessmentRequestData request) throws IUMException {
		
		LOGGER.info("receiveRequirement start");
		StringBuffer invalidRequirements = new StringBuffer();
		ArrayList validRequirements = new ArrayList();

		for (int i = 0; i < requirements.size(); i++) {
			PolicyRequirementsData policyRequirement, tempReqData = new PolicyRequirementsData();
			policyRequirement = (PolicyRequirementsData) requirements.get(i);

			tempReqData = getRequirement(policyRequirement.getRequirementId());
			policyRequirement.setTestDate(tempReqData.getTestDate());
			
			long fromStatus = policyRequirement.getStatus().getStatusId();

			if ((policyRequirement.getLevel().equalsIgnoreCase(IUMConstants.LEVEL_POLICY) ||
				policyRequirement.getLevel().equalsIgnoreCase(IUMConstants.LEVEL_CLIENT)) &&
				this.isStatusValid(request.getLob().getLOBCode(), policyRequirement.getStatus().getStatusId(), IUMConstants.STATUS_RECEIVED_IN_SITE) &&
				(passTestDateIndChecking(policyRequirement))) {

				
				PolicyRequirementDAO dao = new PolicyRequirementDAO();
				try {
					
					dao.changeStatus(policyRequirement.getRequirementId(), IUMConstants.STATUS_RECEIVED_IN_SITE, policyRequirement.getUpdatedBy());
					policyRequirement.setReceiveDate(new Date());
					dao.updateReceiveDate(policyRequirement.getRequirementId(), policyRequirement.getReceiveDate());
					dao.resetFollowUp(policyRequirement.getRequirementId());
					
					policyRequirement = dao.retrieveRequirement(policyRequirement.getRequirementId());
					
				} catch (SQLException e) {
					LOGGER.error(CodeHelper.getStackTrace(e));
					throw new IUMException(e);
				} 

				ArrayList recipients = new ArrayList();
				recipients.add(request.getAssignedTo().getUserId()); // Add facilitator to recipients
				this.executeWorkflow(policyRequirement, request, recipients, fromStatus, null);

				StatusData status = new StatusData();
				status.setStatusId(IUMConstants.STATUS_RECEIVED_IN_SITE);
				policyRequirement.setStatus(status);
				validRequirements.add(policyRequirement);

			} else {
				if (invalidRequirements.length() > 0) {
					invalidRequirements.append(", ");
				}
				invalidRequirements.append(policyRequirement.getRequirementCode());
			}
		}

		// Call message queue
		if (request.getLob().getLOBCode().equalsIgnoreCase(IUMConstants.LOB_INDIVIDUAL_LIFE) &&
			validRequirements.size() > 0) {
			AdminSystem as = AdminSystemFactory.getAdminSystem(request.getSourceSystem());
			try {
				as.processReceiveRequirement(validRequirements);
			} catch (IUMInterfaceException e) {
				LOGGER.error(CodeHelper.getStackTrace(e));
				throw new IUMException(e);
			}
		}

		// Throw invalid requirements
		if (invalidRequirements.length() > 0) {
			LOGGER.warn("Requirement/s " + invalidRequirements.toString() + " is/are invalid for receiving.");
			throw new UnderWriterException("Requirement/s " + invalidRequirements.toString() + " is/are invalid for receiving.");
		}
		LOGGER.info("receiveRequirement end");
	}

	/**
	 * Changes the status of the requirements included in the list to 'REVIEWED AND ACCEPTED'
	 * @param requirements ArrayList of policy requirements
	 * @param request AssessmentRequestData
	 * @throws IUMException
	 */
	public void acceptRequirement(ArrayList requirements, AssessmentRequestData request) throws IUMException {
		
		LOGGER.info("acceptRequirement start");
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date now = new Date();
			
		StringBuffer invalidRequirements = new StringBuffer();
		ArrayList validRequirements = new ArrayList();
		
		for (int i = 0; i < requirements.size(); i++) {
			PolicyRequirementsData policyRequirement, tempReqData = new PolicyRequirementsData();
			policyRequirement = (PolicyRequirementsData) requirements.get(i);

			tempReqData = getRequirement(policyRequirement.getRequirementId());
			policyRequirement.setTestDate(tempReqData.getTestDate());
			
			long fromStatus = policyRequirement.getStatus().getStatusId();
						 
			if ((policyRequirement.getLevel().equalsIgnoreCase(IUMConstants.LEVEL_POLICY) ||
				policyRequirement.getLevel().equalsIgnoreCase(IUMConstants.LEVEL_CLIENT)) &&
				this.isStatusValid(request.getLob().getLOBCode(), policyRequirement.getStatus().getStatusId(), IUMConstants.STATUS_REVIEWED_AND_ACCEPTED) &&
				passTestDateIndChecking(policyRequirement))
			{
				
				PolicyRequirementDAO dao = new PolicyRequirementDAO();
				
				try {
					
					dao.changeStatus(policyRequirement.getRequirementId(), IUMConstants.STATUS_REVIEWED_AND_ACCEPTED, policyRequirement.getUpdatedBy());
					policyRequirement.setReceiveDate(new Date());
					dao.updateReceiveDate(policyRequirement.getRequirementId(), policyRequirement.getReceiveDate());
					dao.resetFollowUp(policyRequirement.getRequirementId());
					
					if (policyRequirement.getTestDate() != null){
						policyRequirement.setValidityDate(this.computeValidityDate(policyRequirement.getTestDate(), policyRequirement.getRequirementCode()));
					} else {
						policyRequirement.setValidityDate(this.computeValidityDate(new Date(), policyRequirement.getRequirementCode()));
					}
					
					dao.updateValidityDate(policyRequirement.getRequirementId(), policyRequirement.getValidityDate());
					policyRequirement = dao.retrieveRequirement(policyRequirement.getRequirementId());
					
				} catch (SQLException e) {
					LOGGER.error(CodeHelper.getStackTrace(e));
					throw new IUMException(e);
				} 
								
				ArrayList recipients = new ArrayList();
				recipients.add(request.getAssignedTo().getUserId()); 
				this.executeWorkflow(policyRequirement, request, recipients, fromStatus, null);
				StatusData status = new StatusData();
				status.setStatusId(IUMConstants.STATUS_REVIEWED_AND_ACCEPTED);
				policyRequirement.setStatus(status);
				validRequirements.add(policyRequirement);

			} else {
				if (invalidRequirements.length() > 0) {
					invalidRequirements.append(", ");
				}
				invalidRequirements.append(policyRequirement.getRequirementCode());
			}
		}

		if (request.getLob().getLOBCode().equalsIgnoreCase(IUMConstants.LOB_INDIVIDUAL_LIFE) &&
			validRequirements.size() > 0) {
			AdminSystem as = AdminSystemFactory.getAdminSystem(request.getSourceSystem());
			try {
				as.processAcceptRequirement(validRequirements);
			} catch (IUMInterfaceException e) {
				LOGGER.error(CodeHelper.getStackTrace(e));
				throw new IUMException(e);
			}
		}

		// Throw invalid requirements
		if (invalidRequirements.length() > 0) {
			LOGGER.warn("Requirement/s " + invalidRequirements.toString() + " is/are invalid for accepting.");
			throw new UnderWriterException("Requirement/s " + invalidRequirements.toString() + " is/are invalid for accepting.");
		}
		LOGGER.info("acceptRequirement end");
	}


	/**
	 * Retrieves the policy requirements  associated with this reference number
	 * @param refNo reference number of the Assessment Request
	 * @return list of policy requirements
	 * @throws Exception 
	 */
	public ArrayList getRequirements(String refNo)  throws UnderWriterException, IUMException {
		
		LOGGER.info("getRequirements start");
		
		PolicyRequirementDAO reqDAO = new PolicyRequirementDAO();
		ArrayList policyReqDataList = null;
		
		try {
			policyReqDataList = reqDAO.retrieveRequirements2(refNo);
			
		} catch (Exception ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		
		LOGGER.info("getRequirements end");
		return policyReqDataList;
	}

	public ArrayList getOwnerIsInsuredRequirements(String clientId, String refNo) throws UnderWriterException, IUMException {
		
		LOGGER.info("getOwnerIsInsuredRequirements start");
		
		PolicyRequirementDAO policyReqDao = new PolicyRequirementDAO();
		ArrayList requirements = null;
		
		try {
			requirements = policyReqDao.retrieveOwnerIsInsuredRequirements(clientId, refNo);
		}  catch (Exception ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		} 
		
		LOGGER.info("getOwnerIsInsuredRequirements end");	
		return requirements;
	}
	
	public ArrayList getOwnerIsNotInsuredRequirements(String ownerClientId, String insuredClientId, String refNo) throws UnderWriterException, IUMException {
		
		LOGGER.info("getOwnerIsNotInsuredRequirements start");
		//Connection conn = new DataSourceProxy().getConnection();
		PolicyRequirementDAO policyReqDao = new PolicyRequirementDAO();
		ArrayList requirements = null;
		try {
			requirements = policyReqDao.retrieveOwnerIsNotInsuredRequirements(ownerClientId, insuredClientId, refNo);
			
		} catch (Exception ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		
		LOGGER.info("getOwnerIsNotInsuredRequirements end");
		return requirements;
	}
	
	public ArrayList getChildIsInsuredRequirements(String ownerClientId, String insuredClientId, String refNo) throws UnderWriterException, IUMException {
		
		LOGGER.info("getChildIsInsuredRequirements start");
		
		PolicyRequirementDAO policyReqDao = new PolicyRequirementDAO();
		PolicyCoverageDetailDao coverageDao = new PolicyCoverageDetailDao(); 
		ArrayList requirements = null;
		try {
			if (coverageDao.hasRiders(refNo)) {
				
				requirements = policyReqDao.retrieveChildIsInsuredRequirements(ownerClientId, insuredClientId, refNo);
			} else {
				
				requirements = policyReqDao.retrieveOwnerIsNotInsuredRequirements(ownerClientId, insuredClientId, refNo);
			}
		} catch (Exception ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		}
		
		LOGGER.info("getChildIsInsuredRequirements end");
		return requirements;
	}
		
	/**
	 * Retrieves details of a policy requirement
	 * 
	 * @param reqId requirement id of the policy requirement
	 * @return PolicyRequirementsData containing the details of the policy
	 *         requirement
	 * @throws UnderWriterException
	 * @throws IUMException
	 */
	public PolicyRequirementsData getRequirement(final long reqId) throws UnderWriterException, IUMException {

		LOGGER.info("getRequirement start");
		
		PolicyRequirementDAO reqDAO = new PolicyRequirementDAO();
		
		try {
			PolicyRequirementsData prData = reqDAO.retrieveRequirement(reqId);
		
			LOGGER.info("getRequirement end");
			return (prData);
		}  catch (Exception ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			throw new IUMException(ex);
		} 
	}

	private Date computeFollowUpDate(long followUpNum, String refNo, String userId, String clientId) throws SQLException {
		
		LOGGER.info("computeFollowUpDate start");
		UserDAO userDao = new UserDAO();
		PolicyRequirementDAO prDao = new PolicyRequirementDAO();
		ArrayList userRolesList = null;
		
		try {
			userRolesList = userDao.getRoleByUserId(userId);
			if (userRolesList != null && !userRolesList.isEmpty()) {
				LOGGER.debug("ROLES LIST SIZE: " + userRolesList.size());
			}
		} catch (SQLException e1) {
			
			e1.printStackTrace();
		}
		
		ResourceBundle CONFIG_PROPERTIES = ResourceBundle.getBundle("dbconfig");
		String companyCode = CONFIG_PROPERTIES.getString("IUM_CSS_VERSION");
		LOGGER.info("COMPANY CODE: " + companyCode);
		
		Date followUpDate = new Date();
		if (followUpNum > 0) {
			int followUpNumber = new Long(followUpNum).intValue();

			if (followUpNumber == 2) {
				followUpNumber = 90;
				String billingType = getBillingType(refNo);
				if ("W".equalsIgnoreCase(billingType)) {
					followUpNumber = 120;
				}
			} else {
				followUpNumber = 10;
				if (userRolesList.contains(IUMConstants.ROLES_NB_REVIEWER)) {
					//SLOCPI Default
					followUpNumber = 15;
					if (IUMConstants.COMPANY_CODE_GF.equalsIgnoreCase(companyCode)) {
						followUpNumber = 30;
					}
				} else if (userRolesList.contains(IUMConstants.ROLES_UNDERWRITER) && prDao.countExistingRequirementsByRefNumAndClientId(refNo, clientId) == 0) {
					followUpNumber = computeUWFollowUpDate(refNo, clientId);
				}
			}
			
			LOGGER.debug("FOLLOW UP NUMBER: " + followUpNumber);
			
			followUpDate = DateHelper.add(followUpDate, Calendar.DATE, followUpNumber);
			
			DateFormat formatter = new SimpleDateFormat("ddMMMyyyy");
			Date dateNBStrucNotesSettleDate = new Date();
			
			try {
				dateNBStrucNotesSettleDate = (Date) formatter.parse(strNBStrucNotesSettleDate);
				if (dateNBStrucNotesSettleDate.before(followUpDate)) {
					followUpDate = dateNBStrucNotesSettleDate;
				}
			} catch (ParseException e) {
				LOGGER.error(CodeHelper.getStackTrace(e));
			}			
			
		}
		LOGGER.info("computeFollowUpDate end");
		return followUpDate;
	}

	private Date computeValidityDate(Date receivedDate, String requirementCode) throws IUMException {
		
		LOGGER.info("computeValidityDate start");
		Date followUpDate = receivedDate;
		
		RequirementDAO dao = new RequirementDAO();
		try {
			int validityNum = new Long(dao.retrieveRequirement(requirementCode).getValidity()).intValue();
			followUpDate = DateHelper.add(receivedDate, Calendar.MONTH, validityNum);
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMException(e);
		} 
		LOGGER.info("computeValidityDate end");
		return followUpDate;
	}

	/*Inserts policy requirements from Abacus to the IUM db*/
	public void processRequirements (ArrayList list) throws IUMException{
		
		LOGGER.info("processRequirements start");
		ArrayList req = list;
		if(req != null){
			if (req.size() > 0){
				for (int i=0; i<list.size(); i++){
					PolicyRequirementsData pol = (PolicyRequirementsData) list.get(i);
					
					try {
						if (!isRequirementExisting(pol.getReferenceNumber(),pol.getRequirementId())){
							PolicyRequirementDAO dao = new PolicyRequirementDAO();
							dao.insertRequirement(pol);
						}
					} catch (SQLException sqlE) {
						LOGGER.error(CodeHelper.getStackTrace(sqlE));
						throw new IUMException(sqlE); 
					} catch (Exception e) {
						LOGGER.error(CodeHelper.getStackTrace(e));
						throw new IUMException(e);
					} 
				}
			}
		}
		LOGGER.info("processRequirements end");
	}

	private boolean isRequirementExisting(String referenceNumber, long reqId) throws IUMException {
		
		LOGGER.info("isRequirementExisting start");
		boolean result = false;
		
		try {
			PolicyRequirementDAO dao = new PolicyRequirementDAO();
			result = dao.isRequirementExisting(referenceNumber,reqId);
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMException(e);
		}
		LOGGER.info("isRequirementExisting end");
		return result;
	}

	/**
	 * @param listOfRequirements
	 * @param request
	 * @throws IUMException
	 */
	public void rejectRequirements(ArrayList listOfRequirements, AssessmentRequestData request) throws IUMException {
		
		LOGGER.info("rejectRequirements start");
		StringBuffer invalidRequirements = new StringBuffer();
		ArrayList validRequirements = new ArrayList();

		for (int i = 0; i < listOfRequirements.size(); i++) {
			PolicyRequirementsData policyRequirement, tempReqData = new PolicyRequirementsData();
			policyRequirement = (PolicyRequirementsData) listOfRequirements.get(i);

			tempReqData = getRequirement(policyRequirement.getRequirementId());
			policyRequirement.setTestDate(tempReqData.getTestDate());
			
			if ((policyRequirement.getLevel().equalsIgnoreCase(IUMConstants.LEVEL_POLICY) ||
				policyRequirement.getLevel().equalsIgnoreCase(IUMConstants.LEVEL_CLIENT)) &&
				this.isStatusValid(request.getLob().getLOBCode(), policyRequirement.getStatus().getStatusId(), IUMConstants.STATUS_REVIEWED_AND_REJECTED)) {

				PolicyRequirementDAO dao = new PolicyRequirementDAO();
				try {
					dao.changeStatus(policyRequirement.getRequirementId(), IUMConstants.STATUS_REVIEWED_AND_REJECTED, policyRequirement.getUpdatedBy());
					dao.resetFollowUp(policyRequirement.getRequirementId());
					policyRequirement = dao.retrieveRequirement(policyRequirement.getRequirementId());
				} catch (SQLException e) {
					LOGGER.error(CodeHelper.getStackTrace(e));
					throw new IUMException(e);
				} 

				StatusData status = new StatusData();
				status.setStatusId(IUMConstants.STATUS_REVIEWED_AND_REJECTED);
				policyRequirement.setStatus(status);
				validRequirements.add(policyRequirement);

			} else {
				if (invalidRequirements.length() > 0) {
					invalidRequirements.append(", ");
				}
				invalidRequirements.append(policyRequirement.getRequirementCode());
			}
		}

		// Call message queue
		if (request.getLob().getLOBCode().equalsIgnoreCase(IUMConstants.LOB_INDIVIDUAL_LIFE) &&
			validRequirements.size() > 0) {
			AdminSystem as = AdminSystemFactory.getAdminSystem(request.getSourceSystem());
			try {
				as.processRejectRequirement(validRequirements);
			} catch (IUMInterfaceException e) {
				LOGGER.error(CodeHelper.getStackTrace(e));
				throw new IUMException(e);
			}
		}

		// Throw invalid requirements
		if (invalidRequirements.length() > 0) {
			LOGGER.warn("Requirement/s " + invalidRequirements.toString() + " is/are invalid for rejecting.");
			throw new UnderWriterException("Requirement/s " + invalidRequirements.toString() + " is/are invalid for rejecting.");
		}
		LOGGER.info("rejectRequirements end");
	}

	/**
	 * Cancel Requirements in IUM, WMS, ING
	 * @param requirements
	 * @param request
	 * @throws IUMException
	 * 
	 * ** Made changes according to v1.1 - Sept 25, 2014
	 */
	public void cancelRequirement(ArrayList requirements, AssessmentRequestData request) throws IUMException {
		
		LOGGER.info("cancelRequirement start");
		
		StringBuffer invalidRequirements = new StringBuffer();
		ArrayList validRequirements = new ArrayList();
		PolicyRequirementDAO prDao = null;
		PolicyRequirementsData policyRequirement;

		try {
			for (int i = 0; i < requirements.size(); i++) {
				
				policyRequirement = new PolicyRequirementsData();
				policyRequirement = (PolicyRequirementsData) requirements.get(i);
	
				Date prTestDate = getTestDate(policyRequirement.getRequirementId());
				policyRequirement.setTestDate(prTestDate);
				
				long fromStatus = policyRequirement.getStatus().getStatusId();
				boolean isStatusValid = isStatusValid(request.getLob().getLOBCode(), policyRequirement.getStatus().getStatusId(), IUMConstants.STATUS_REQ_CANCELLED);
	
				if ((policyRequirement.getLevel().equalsIgnoreCase(IUMConstants.LEVEL_POLICY)
						|| policyRequirement.getLevel().equalsIgnoreCase(IUMConstants.LEVEL_CLIENT))
						&& isStatusValid) {
	
					LOGGER.debug("PolNo: " + policyRequirement.getReferenceNumber() + " : " + "ReqtId: " + policyRequirement.getRequirementId() + " : Reqt is valid. ");
					try {
						prDao = new PolicyRequirementDAO();
						prDao.changeStatus(policyRequirement.getRequirementId(), IUMConstants.STATUS_REQ_CANCELLED, policyRequirement.getUpdatedBy());
						prDao.resetFollowUp(policyRequirement.getRequirementId());
						
					} catch (SQLException e) {
						LOGGER.debug("PolNo: " + policyRequirement.getReferenceNumber() + " : " + "ReqtId: " + policyRequirement.getRequirementId() + " : Error in rolling back. " + e);
						LOGGER.error(CodeHelper.getStackTrace(e));
					}
					
					
					ArrayList recipients = new ArrayList();
					try {
						UserDAO userDAO = new UserDAO();
						recipients = userDAO.selectUsersPerRoleBranch(IUMConstants.ROLES_MARKETING_STAFF, request.getBranch().getOfficeId());
					} catch (Exception e) {
						LOGGER.debug("PolNo: " + policyRequirement.getReferenceNumber() + " : " + "ReqtId: " + policyRequirement.getRequirementId() + " : Error in getting recipients. " + e);
						LOGGER.error(CodeHelper.getStackTrace(e));
					}
					String agentId = request.getAgent().getUserId();
					recipients.add(agentId); // Add agent to recipients
					
					try {
						//include all info
						policyRequirement = prDao.retrieveRequirement(policyRequirement.getRequirementId());
						executeWorkflow(policyRequirement, request, recipients, fromStatus, agentId);
					} catch (Exception e) {
						LOGGER.debug("PolNo: " + policyRequirement.getReferenceNumber() + " : " + "ReqtId: " + policyRequirement.getRequirementId() + " : Error in getting requirements data. " + e);
						LOGGER.error(CodeHelper.getStackTrace(e));
					}
					
					// Set status to cancel
					StatusData status = new StatusData();
					status.setStatusId(IUMConstants.STATUS_REQ_CANCELLED);
					policyRequirement.setStatus(status);
					validRequirements.add(policyRequirement);
	
				} else {
					LOGGER.debug("PolNo: " + policyRequirement.getReferenceNumber() + " : " + "ReqtId: " + policyRequirement.getRequirementId() + " : Reqt is valid. ");
					if (invalidRequirements.length() > 0) {
						invalidRequirements.append(", ");
					}
					
					invalidRequirements.append(policyRequirement.getRequirementCode());
				}
			}
			
			// Call message queue
			if (request.getLob().getLOBCode().equalsIgnoreCase(IUMConstants.LOB_INDIVIDUAL_LIFE)
					&& validRequirements.size() > 0) {
				
				try {
					AdminSystem as = AdminSystemFactory.getAdminSystem(request.getSourceSystem());
					as.processCancelRequirement(validRequirements);
				} catch (IUMInterfaceException e) {
					LOGGER.error("Error in updating reqt in ING. ");
					LOGGER.error(CodeHelper.getStackTrace(e));
					throw new IUMException(e);
				}
			}
			
			// Throw invalid requirements
			if (invalidRequirements.length() > 0) {
				LOGGER.debug("Have invalid reqt's : " + invalidRequirements.toString());
				throw new UnderWriterException("Requirement/s " + invalidRequirements.toString() + " is/are invalid for canceling.");
			}
		} catch (Exception e) {
			LOGGER.debug("Pol: " + request.getReferenceNumber() + " : Error encountered in calcelRequirements.");
			LOGGER.error(CodeHelper.getStackTrace(e));
		}
		
		LOGGER.debug("Pol: " + request.getReferenceNumber() + " : End of Cancellation of Requirements.");
		LOGGER.info("cancelRequirement end");
	}

	/**
	 * @param requirements
	 * @param request
	 * @throws IUMException
	 */
	public void waiveRequirement(ArrayList requirements, AssessmentRequestData request) throws IUMException {
		
		LOGGER.info("waiveRequirement start");
		StringBuffer invalidRequirements = new StringBuffer();
		ArrayList validRequirements = new ArrayList();

		for (int i = 0; i < requirements.size(); i++) {
			PolicyRequirementsData policyRequirement, tempReqData = new PolicyRequirementsData();
			policyRequirement = (PolicyRequirementsData) requirements.get(i);

			tempReqData = getRequirement(policyRequirement.getRequirementId());
			policyRequirement.setTestDate(tempReqData.getTestDate());
			
			long fromStatus = policyRequirement.getStatus().getStatusId();

			if ((policyRequirement.getLevel().equalsIgnoreCase(IUMConstants.LEVEL_POLICY) ||
				policyRequirement.getLevel().equalsIgnoreCase(IUMConstants.LEVEL_CLIENT)) &&
				this.isStatusValid(request.getLob().getLOBCode(), policyRequirement.getStatus().getStatusId(), IUMConstants.STATUS_WAIVED)){

				
				PolicyRequirementDAO dao = new PolicyRequirementDAO();
				try {
					dao.changeStatus(policyRequirement.getRequirementId(), IUMConstants.STATUS_WAIVED, policyRequirement.getUpdatedBy());
					dao.resetFollowUp(policyRequirement.getRequirementId());
					policyRequirement = dao.retrieveRequirement(policyRequirement.getRequirementId());
				} catch (SQLException e) {
					LOGGER.error(CodeHelper.getStackTrace(e));
					throw new IUMException(e);
				}

				ArrayList recipients = new ArrayList();
				UserDAO userDAO = new UserDAO();
				try {
					recipients = userDAO.selectUsersPerRoleBranch(IUMConstants.ROLES_MARKETING_STAFF, request.getBranch().getOfficeId());
				} catch (Exception e) {
					LOGGER.error(CodeHelper.getStackTrace(e));
					throw new IUMException(e);
				} 
				String agentId = request.getAgent().getUserId();
				recipients.add(agentId); 
				this.executeWorkflow(policyRequirement, request, recipients, fromStatus, agentId);

				StatusData status = new StatusData();
				status.setStatusId(IUMConstants.STATUS_WAIVED);
				policyRequirement.setStatus(status);
				validRequirements.add(policyRequirement);

			} else {
				if (invalidRequirements.length() > 0) {
					invalidRequirements.append(", ");
				}
				invalidRequirements.append(policyRequirement.getRequirementCode());
			}
		}

		// Call message queue
		if (request.getLob().getLOBCode().equalsIgnoreCase(IUMConstants.LOB_INDIVIDUAL_LIFE) &&
			validRequirements.size() > 0) {
			AdminSystem as = AdminSystemFactory.getAdminSystem(request.getSourceSystem());
			try {
				as.processWaiveRequirement(validRequirements);
			} catch (IUMInterfaceException e) {
				LOGGER.error(CodeHelper.getStackTrace(e));
				throw new IUMException(e);
			}
		}

		// Throw invalid requirements
		if (invalidRequirements.length() > 0) {
			LOGGER.warn("Requirement/s " + invalidRequirements.toString() + " is/are invalid for waiving.");
			throw new UnderWriterException("Requirement/s " + invalidRequirements.toString() + " is/are invalid for waiving.");
		}
		
		LOGGER.info("waiveRequirement end");
	}

	public void maintainRequirement(PolicyRequirementsData requirement, AssessmentRequestData request) throws IUMException {

		LOGGER.info("maintainRequirement start 1");
		
		if ((requirement.getLevel().equalsIgnoreCase(IUMConstants.LEVEL_POLICY) ||
			requirement.getLevel().equalsIgnoreCase(IUMConstants.LEVEL_CLIENT)) &&
			(requirement.getStatus().getStatusId() == IUMConstants.STATUS_NTO ||
			requirement.getStatus().getStatusId() == IUMConstants.STATUS_ORDERED)) {

			PolicyRequirementDAO dao = new PolicyRequirementDAO();
			
			
			if (requirement.getStatus().getStatusId() == IUMConstants.STATUS_RECEIVED_IN_SITE || 
					requirement.getStatus().getStatusId() == IUMConstants.STATUS_REVIEWED_AND_ACCEPTED){
				if (!passTestDateIndChecking(requirement)){
					LOGGER.warn("Test Date is required for requirement: " + requirement.getRequirementCode());
					throw new UnderWriterException("Test Date is required for requirement: " + requirement.getRequirementCode());
				}				
			}
			try {
				dao.updateRequirement(requirement);
			} catch (SQLException e1) {
				LOGGER.error(CodeHelper.getStackTrace(e1));
			}
				
			if (request.getLob().getLOBCode().equalsIgnoreCase(IUMConstants.LOB_INDIVIDUAL_LIFE)) {
				AdminSystem as = AdminSystemFactory.getAdminSystem(request.getSourceSystem());
				try {
					
					as.processUpdateRequirement(requirement);
				} catch (IUMInterfaceException e) {
					LOGGER.error(CodeHelper.getStackTrace(e));
					throw new IUMException(e);
				}
			}
		} else {
			LOGGER.warn("Invalid policy requirement for update.");
			throw new UnderWriterException("Invalid policy requirement for update.");
		}
		LOGGER.info("maintainRequirement end 1");
	}

	public void maintainRequirement(PolicyRequirementsData requirement) throws IUMException {
		
		LOGGER.info("maintainRequirement start 2");
		PolicyRequirementDAO dao = new PolicyRequirementDAO();
		
		try {
			
			dao.updateRequirement(requirement);
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMException(e);
		}
		
		LOGGER.info("maintainRequirement end 2");
		
	}

	/**
	 * @param policyRequirement
	 * @param request
	 * @param recipients
	 * @throws IUMException
	 */
	private void executeWorkflow(PolicyRequirementsData policyRequirement, AssessmentRequestData request, ArrayList recipients, long previousStatus, String agentId) throws IUMException {
		
		LOGGER.info("executeWorkflow start");
		Workflow workflow = new Workflow();
		WorkflowItem workflowItem = new WorkflowItem();
		workflowItem.setObjectID(policyRequirement.getRequirementId());
		workflowItem.setPreviousStatus(String.valueOf(previousStatus));
		workflowItem.setType(IUMConstants.POLICY_REQUIREMENTS);
		workflowItem.setLOB(request.getLob().getLOBCode());
		workflowItem.setSender(policyRequirement.getUpdatedBy());
		workflowItem.setBranchId(request.getBranch().getOfficeId());
		workflowItem.setPrimaryRecipient(recipients);
		workflowItem.setAgentId(agentId);
		try {
			workflow.processState(workflowItem);
		} catch (Exception e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMException(e);
		}
		LOGGER.info("executeWorkflow end");
	}

	private boolean hasDuplicateReqt(PolicyRequirementsData requirement) throws IUMException {
		
		LOGGER.info("hasDuplicateReqt start");
		boolean duplicate = false;

		if (requirement.getLevel().equalsIgnoreCase(IUMConstants.LEVEL_CLIENT) ||
			requirement.getLevel().equalsIgnoreCase(IUMConstants.LEVEL_POLICY)) {
			
			PolicyRequirementDAO dao = new PolicyRequirementDAO();
			ArrayList list = new ArrayList();
			try {
				if (requirement.getLevel().equalsIgnoreCase(IUMConstants.LEVEL_CLIENT)) {
					list = dao.retrieveClientReqts(requirement.getLevel(), requirement.getClientId(), requirement.getRequirementCode());
				} else if (requirement.getLevel().equalsIgnoreCase(IUMConstants.LEVEL_POLICY)) {
					list = dao.retrievePolicyReqts(requirement.getLevel(), requirement.getReferenceNumber(), requirement.getRequirementCode());
				}
				if (list.size() > 0) {
					for (int i = 0; i < list.size(); i++) {
						PolicyRequirementsData data = (PolicyRequirementsData) list.get(i);
						if ((data.getStatus().getStatusId() == IUMConstants.STATUS_NTO ||
							data.getStatus().getStatusId() == IUMConstants.STATUS_SAA ||
							data.getStatus().getStatusId() == IUMConstants.STATUS_SCC ||
							data.getStatus().getStatusId() == IUMConstants.STATUS_SIR ||
							data.getStatus().getStatusId() == IUMConstants.STATUS_ORDERED ||
							data.getStatus().getStatusId() == IUMConstants.STATUS_RECEIVED_IN_SITE ||
							data.getStatus().getStatusId() == IUMConstants.STATUS_REVIEWED_AND_ACCEPTED) &&
							(data.getValidityDate() == null ||
							DateHelper.compare(data.getValidityDate(), new Date()) >= 0)) {
							duplicate = true;
							break;
						}
					}
				}
			} catch (SQLException e) {
				LOGGER.error(CodeHelper.getStackTrace(e));
				throw new IUMException(e);
			} 
		}
		LOGGER.info("hasDuplicateReqt end");
		return duplicate;
	}

	public boolean isStatusValid(String lob, long fromStatus, long toStatus) throws IUMException {
		
		LOGGER.info("isStatusValid start");
		boolean isValid = false;
		
		try {
			ProcessConfigDAO dao = new ProcessConfigDAO();
			isValid = dao.isValidStatus(lob, IUMConstants.STAT_TYPE_NM, fromStatus, toStatus);
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMException(e);
		} 
		LOGGER.info("isStatusValid end");
		return isValid;
	}

	private void closeConnection(Connection conn) throws IUMException{
		
		
		if (null != conn){
			try {
				conn.close();
			} catch (SQLException sqlE) {
				LOGGER.error(CodeHelper.getStackTrace(sqlE));
				throw new IUMException(sqlE);
			}
		}
		
	}

	public ArrayList retrieveAttachmentHelper(final String referenceNumber) throws IUMException{
		
		LOGGER.info("retrieveAttachmentHelper start");
		
		WorkflowDAO dao = new WorkflowDAO();
		ArrayList listOfIds = new ArrayList();
		ArrayList reqList = new ArrayList();

		try {
			
			listOfIds = dao.retrieveAttachmentHelper(referenceNumber);
			for (int i=0; i<listOfIds.size(); i++){
				
				String reqId = (String) listOfIds.get(i);
				PolicyRequirementsData polReq = getRequirement(Long.parseLong(reqId));
				
				if (polReq.getStatus().getStatusId() != IUMConstants.STATUS_REQ_CANCELLED
						&& polReq.getStatus().getStatusId() == IUMConstants.STATUS_ORDERED) {
					reqList.add(polReq);
				}
			}
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMException(e);
		} 
		LOGGER.info("retrieveAttachmentHelper ends");
		return reqList;
	}

	public ArrayList retrieveAttachment(final String referenceNumber) throws IUMException{
		
		LOGGER.info("retrieveAttachment start");
		ArrayList attachments = new ArrayList();
		ArrayList reqList = new ArrayList();

		try {
			
			reqList = retrieveAttachmentHelper(referenceNumber);
			for (int i = 0; i< reqList.size(); i++){
				PolicyRequirementsData polReq = (PolicyRequirementsData) reqList.get(i);
				RequirementForm form = new RequirementForm();
				Attachment att = form.writeForm(polReq);
				if (att != null){
					attachments.add(att);
				}
			}
		} catch (IUMException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMException(e);
		}
		LOGGER.info("retrieveAttachment start");
		return attachments;
	}

	public void sendNotificationOrderReqt(String referenceNumber, String updatedBy) throws IUMException{
		
		LOGGER.info("sendNotificationOrderReqt start");
		
		ArrayList recipients = new ArrayList();
		
		LOGGER.debug("sendNotificationOrderReqt: "+ referenceNumber+" - "+updatedBy);

		//sets the recipient
		AssessmentRequest ar = new AssessmentRequest();
		AssessmentRequestData request = ar.getDetails(referenceNumber);
		UserDAO userDAO = new UserDAO();
		try {
			recipients = userDAO.selectUsersPerRoleBranch(IUMConstants.ROLES_MARKETING_STAFF, request.getBranch().getOfficeId());
		} catch (Exception e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMException(e);
		}
		String agentId = request.getAgent().getUserId();
		LOGGER.debug("agent id for referenceNo " + referenceNumber + " is agent id " + agentId);
		boolean r =recipients.add(agentId);
		
		Workflow workflow = new Workflow();
		WorkflowItem workflowItem = new WorkflowItem();
		workflowItem.setObjectID(referenceNumber);
		workflowItem.setPreviousStatus(String.valueOf(IUMConstants.STATUS_ORDERED));
		workflowItem.setType(IUMConstants.POLICY_REQUIREMENTS);
		workflowItem.setLOB(request.getLob().getLOBCode());
		workflowItem.setSpecialCase(IUMConstants.ORDER_REQUIREMENT);
		workflowItem.setSender(updatedBy);
		workflowItem.setBranchId(request.getBranch().getOfficeId());
		workflowItem.setPrimaryRecipient(recipients);
		workflowItem.setAgentId(agentId);
		try {
			workflow.processState(workflowItem);
		} catch (Exception e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMException(e);
		}
		LOGGER.info("sendNotificationOrderReqt end");
	}
	
	public void sendNotificationReceiveReqt(String referenceNumber, String updatedBy) throws IUMException{
		
		LOGGER.info("sendNotificationReceiveReqt start 1");
		
		ArrayList recipients = new ArrayList();
		
		//sets the recipient
		AssessmentRequest ar = new AssessmentRequest();
		AssessmentRequestData request = ar.getDetails(referenceNumber);
		UserDAO userDAO = new UserDAO();
		try {
			recipients = userDAO.selectUsersPerRoleBranch(IUMConstants.ROLES_MARKETING_STAFF, request.getBranch().getOfficeId());
		} catch (Exception e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMException(e);
		} 
		String agentId = request.getAgent().getUserId();
		boolean r =recipients.add(agentId);
		
		Workflow workflow = new Workflow();
		WorkflowItem workflowItem = new WorkflowItem();
		workflowItem.setObjectID(referenceNumber);
		workflowItem.setPreviousStatus(String.valueOf(IUMConstants.STATUS_ORDERED));
		workflowItem.setType(IUMConstants.POLICY_REQUIREMENTS);
		workflowItem.setLOB(request.getLob().getLOBCode());
		workflowItem.setSpecialCase(IUMConstants.RECEIVED_REQUIREMENT);
		workflowItem.setSender(updatedBy);
		workflowItem.setBranchId(request.getBranch().getOfficeId());
		workflowItem.setPrimaryRecipient(recipients);
		workflowItem.setAgentId(agentId);
		
		try {
			workflow.processState(workflowItem);
		} catch (Exception e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMException(e);
		}
		
		LOGGER.info("sendNotificationReceiveReqt end 1");
	}

	
	
	/**
	 * @author hackmel 
	 * @param referenceNumber
	 * @param reqIumId
	 * @param updatedBy
	 * @throws IUMException
	 */
	public void sendNotificationReceiveReqt(String referenceNumber, String reqIumId,String updatedBy) throws IUMException{
		
		LOGGER.info("sendNotificationReceiveReqt start 2");
		
		ArrayList recipients = new ArrayList();
		
		LOGGER.debug("sendNotificationReceiveReqt: "+ referenceNumber+" - "+updatedBy);

		//sets the recipient
		AssessmentRequest ar = new AssessmentRequest();
		AssessmentRequestData request = ar.getDetails(referenceNumber);
		UserDAO userDAO = new UserDAO();
		try {
			recipients = userDAO.selectUsersPerRoleBranch(IUMConstants.ROLES_MARKETING_STAFF, request.getBranch().getOfficeId());
		} catch (Exception e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMException(e);
		} 
		String agentId = request.getAgent().getUserId();
		boolean r =recipients.add(agentId);
		
		Workflow workflow = new Workflow();
		WorkflowItem workflowItem = new WorkflowItem();
		workflowItem.setObjectID(reqIumId);
		workflowItem.setPreviousStatus(String.valueOf(IUMConstants.STATUS_ORDERED));
		workflowItem.setType(IUMConstants.POLICY_REQUIREMENTS);
		workflowItem.setLOB(request.getLob().getLOBCode());
		workflowItem.setSpecialCase(IUMConstants.RECEIVED_REQUIREMENT);
		workflowItem.setSender(updatedBy);
		workflowItem.setBranchId(request.getBranch().getOfficeId());
		workflowItem.setPrimaryRecipient(recipients);
		workflowItem.setAgentId(agentId);
		
		try {
			workflow.processState(workflowItem);
		} catch (Exception e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMException(e);
		}
		
		LOGGER.info("sendNotificationReceiveReqt end 2");
	}

	public boolean hasOrderRequirements (String referenceNumber) throws IUMException{
		
		LOGGER.info("hasOrderRequirements start");
		
		WorkflowDAO dao = new WorkflowDAO();
		boolean result = false;

		try {
			result = dao.hasOrderedRequirements(referenceNumber);
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMException(e);
		}
		LOGGER.info("hasOrderRequirements end");
		return result;
	}

	public boolean passTestDateIndChecking(PolicyRequirementsData polRequirement) throws IUMException{
		
		LOGGER.info("passTestDateIndChecking start");
		boolean result = false;
		
		RequirementDAO dao = new RequirementDAO();
		Date testDate = null;
		try {
			String requirementCode = polRequirement.getRequirementCode();
			testDate = polRequirement.getTestDate();
			String tdIndicator = dao.checkTestDateIndicator(requirementCode);
			
			if (tdIndicator != null && tdIndicator.equals("1")){
				if (testDate != null){
					result = true;
				} else {
					result = false;
				}

			} else {
				result = true; 
			}
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMException(e);
		} 
		LOGGER.info("passTestDateIndChecking end");
		return result;
	}

	public void genericPRChangeStatus(ArrayList requirements, AssessmentRequestData request, long toStatus) throws UnderWriterException, IUMException{
		
		LOGGER.info("genericPRChangeStatus start");
		StringBuffer invalidRequirements = new StringBuffer();
		ArrayList validRequirements = new ArrayList();

		for (int i = 0; i < requirements.size(); i++) {
			PolicyRequirementsData policyRequirement, tempReqData = new PolicyRequirementsData();
			policyRequirement = (PolicyRequirementsData) requirements.get(i);

			AssessmentRequest ar = new AssessmentRequest();
			AssessmentRequestData reqData = ar.getDetails(request.getReferenceNumber());

			tempReqData = getRequirement(policyRequirement.getRequirementId());

			long fromStatus = policyRequirement.getStatus().getStatusId();

			if ((policyRequirement.getLevel().equalsIgnoreCase(IUMConstants.LEVEL_POLICY) ||
				policyRequirement.getLevel().equalsIgnoreCase(IUMConstants.LEVEL_CLIENT)) &&
				this.isStatusValid(request.getLob().getLOBCode(), policyRequirement.getStatus().getStatusId(), toStatus)) {

				
				PolicyRequirementDAO dao = new PolicyRequirementDAO();
				try {
					dao.changeStatus(policyRequirement.getRequirementId(), toStatus, policyRequirement.getUpdatedBy());
					policyRequirement = dao.retrieveRequirement(policyRequirement.getRequirementId());
				} catch (SQLException e) {
					LOGGER.error(CodeHelper.getStackTrace(e));
					throw new IUMException(e);
				} 

				ArrayList recipients = new ArrayList();
				recipients.add(reqData.getAssignedTo().getUserId());

				this.executeWorkflow(policyRequirement, request, recipients, fromStatus, null);

				StatusData status = new StatusData();
				status.setStatusId(toStatus);
				policyRequirement.setStatus(status);
				validRequirements.add(policyRequirement);

			} else {
				if (invalidRequirements.length() > 0) {
					invalidRequirements.append(", ");
				}
				invalidRequirements.append(policyRequirement.getRequirementCode());
			}
		}

		// Throw invalid requirements
		if (invalidRequirements.length() > 0) {
			LOGGER.warn("Requirement/s " + invalidRequirements.toString() + " is/are invalid for the status selected.");
			throw new UnderWriterException("Requirement/s " + invalidRequirements.toString() + " is/are invalid for the status selected.");
		}
		
		LOGGER.info("genericPRChangeStatus end");
	}
	
	public boolean matchDocument(PolicyRequirementsData prd) throws SQLException, Exception{
		
		LOGGER.info("matchDocument start");
		boolean result = false;
		boolean isCreate = false;
		
		AssessmentRequestDAO ard = new AssessmentRequestDAO();
		
		LOGGER.info("Policy/Assessment number" + prd.getReferenceNumber());
		
		try {
			if(ard.isPolicyExists(prd.getReferenceNumber())){
				LOGGER.info("Policy/Assessment exists in IUM");
				
				PolicyRequirementDAO requirementsDAO = new PolicyRequirementDAO();
				
				long oldReqtStatusId = 0;
				if(prd.getRequirementId() == 0){
					oldReqtStatusId = requirementsDAO.getStatus(prd);
					prd.getStatus().setOldStatusId(oldReqtStatusId);
				}else{
					oldReqtStatusId = requirementsDAO.getStatus(prd.getRequirementId());
					prd.getStatus().setOldStatusId(oldReqtStatusId);					
				}
				
				
				StatusData sd2 = prd.getStatus();
				PolicyRequirementsData prd2 = prd;
				if(prd2.getLevel().equalsIgnoreCase("O")||prd2.getLevel().equalsIgnoreCase("I")){
					
				}
				LOGGER.info("Get Requirement Id-->" + prd.getRequirementId());
				if(prd.getRequirementId() == 0){
					
					LOGGER.info("Requirement Id is NOT available. Insert new Requirement.");
						if(prd.getStatusDate()==null){
							Calendar cal = Calendar.getInstance();
							prd.setStatusDate(cal.getTime());
						}
						requirementsDAO.insertRequirementWMS(prd);
						isCreate=true;
						
				}else{
					
					if(requirementsDAO.isRequirementExisting(prd.getRequirementId())){
						LOGGER.info("Requirement Id is available.");
						
						if(!requirementsDAO.isRequirementMatched(prd.getRequirementId())){
							LOGGER.info("Requirement to be updated.");
							requirementsDAO.updateRequirementMatch(prd,prd.getRequirementId());
							
						}else{
							LOGGER.warn("Requirement is already matched.");
							throw new IUMInterfaceException("Requirement is already matched.");
						}
					}else{
						//throw error
						result = false;
						LOGGER.warn("Requirement does not exists in IUM.");
						throw new IUMInterfaceException("Requirement does not exists in IUM.");
					}
				}
				
				PolicyRequirementsData policyData = requirementsDAO.retrieveRequirement(prd.getRequirementId());
				
				
				try {
					IngeniumGateway ig = new IngeniumGateway();
					
					if(isCreate==true){
					
						if(prd.getLevel().equalsIgnoreCase("O")||prd.getLevel().equalsIgnoreCase("I")){
							prd.setLevel("C");
						}
						LOGGER.info("Requirement being created in Ingenium.");
						ig.processCreateRequirement(prd);
					
					}else{
						LOGGER.info("Requirement being updated in Ingenium.");
						ig.processUpdateRequirement(policyData);
					}
				} catch (IUMInterfaceException e) {
					LOGGER.error(CodeHelper.getStackTrace(e));
					throw new IUMException(e);
				}			
				
				result = true;
				
			}else{
				result = false;
				throw new IUMInterfaceException("Policy does not exists in IUM.");
			}
		}catch (SQLException sqlE){
			LOGGER.error(CodeHelper.getStackTrace(sqlE));
			
		}
		
		LOGGER.info("Match document final result-->" + result);
		LOGGER.info("matchDocument end");
		return result;
	}
	
	public boolean unmatchDocument(PolicyRequirementsData prd) throws Exception, SQLException{
		
		LOGGER.info("unmatchDocument start");
		Connection conn = new DataSourceProxy().getConnection();
		PolicyRequirementDAO requirementsDAO = new PolicyRequirementDAO();
		
		LOGGER.info("Get Requirement Id-->" + prd.getRequirementId());
		
		boolean result = false;
		try {
			conn.setAutoCommit(false);
			/**  */
			if(prd.getRequirementId() != 0){
				if(requirementsDAO.isRequirementExisting(prd.getRequirementId())){
					if (requirementsDAO.isRequirementMatched(prd.getRequirementId())) {
						LOGGER.info("PolicyData Requirement Id-->" + prd.getRequirementId());
						requirementsDAO.updateRequirementUnmatch(prd);
						conn.commit();
						PolicyRequirementsData policyData = requirementsDAO.retrieveRequirement(prd.getRequirementId());
						try {
							IngeniumGateway ig = new IngeniumGateway();
							LOGGER.info("Requirement being updated in Ingenium.");
							ig.processUpdateRequirement(policyData);
						} catch (IUMInterfaceException e) {
							LOGGER.error(CodeHelper.getStackTrace(e));
							throw new IUMException(e);
						}						
						result = true;
					}
					else {
						LOGGER.warn("Requirement is not yet matched.");
						throw new IUMInterfaceException("Requirement is not yet matched.");
					}
				}else{
					LOGGER.warn("Requirement does not exist in IUM. (Parameter used: reqId");
					throw new IUMInterfaceException("Requirement does not exist in IUM. (Parameter used: reqId");
				}
			}
			else{
				if (prd.getReferenceNumber() != "" && prd.getImageRef() != "" 
						&& prd.getRequirementCode() != ""){
					if(requirementsDAO.isRequirementExisting(prd.getReferenceNumber(),prd.getImageRef(),prd.getRequirementCode())){
						if (requirementsDAO.isRequirementMatched(prd.getReferenceNumber(),prd.getImageRef(),prd.getRequirementCode())) {
							
							List reqList = requirementsDAO.retrieveRequirement(prd.getReferenceNumber(), prd.getRequirementCode(), prd.getImageRef());
							Iterator list = reqList.iterator();
							while(list.hasNext()){
								PolicyRequirementsData policyData = new PolicyRequirementsData();
								policyData = (PolicyRequirementsData) list.next();
								LOGGER.info("PolicyData Requirement Id-->" + policyData.getRequirementId());
								requirementsDAO.updateRequirementUnmatch(policyData);
								conn.commit();
								policyData = requirementsDAO.retrieveRequirement(policyData.getRequirementId());
								try {
									IngeniumGateway ig = new IngeniumGateway(true);
									LOGGER.info("Requirement being updated in Ingenium.");
									ig.processUpdateRequirement(policyData);
								} catch (IUMInterfaceException e) {
									LOGGER.error(CodeHelper.getStackTrace(e));
									throw new IUMException(e);
								}
							}
							result = true;
						}
						else {
							LOGGER.warn("Requirement is not yet matched.");
							throw new IUMInterfaceException("Requirement is not yet matched.");
						}
					}
					else{
						LOGGER.warn("Requirement does not exist in IUM. (Parameters used: refNo, imgRef, reqCde)");
						throw new IUMInterfaceException("Requirement does not exist in IUM. (Parameters used: refNo, imgRef, reqCde)");
					}
				}
				else{
					LOGGER.warn("Policy ID, Image Reference or Requirement Type ID is/are missing.");	
					throw new IUMInterfaceException("Policy ID, Image Reference or" +
							"Requirement Type ID is/are missing.");	
				}
			}
			
		} catch (SQLException sqlE){
			LOGGER.error(CodeHelper.getStackTrace(sqlE));
			conn.rollback();
		}	
		
		finally{
			closeConnection(conn);
		}
		LOGGER.info("Unamtch document final result-->" + result);
		LOGGER.info("unmatchDocument end");
		return result;
	}
	
	
	public String checkReqtsSubmissionStatus(String policyNumber) throws Exception{
		
		LOGGER.info("checkReqtsSubmissionStatus start");
		
		PolicyRequirementDAO requirementsDAO = new PolicyRequirementDAO();
		
		boolean result = false;
		
		try {
			result = requirementsDAO.areRequirementsComplete(policyNumber);
		} catch (Exception e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
		}
		
		
		String status = "";
		if(result){
			status = IUMConstants.REQT_STATUS_COMPLETE;
		}else{
			status = IUMConstants.REQT_STATUS_PARTIAL;
		}
		LOGGER.info("checkReqtsSubmissionStatus end");
		return status;
	}
	
	/**
	 * Retrieves pr_test_date per requirement_id/s
	 * 
	 * @param reqId requirement id of the policy requirement
	 * @return String the pr_test_date column value in policy_requirements table
	 */
	public Date getTestDate(final long reqId) {

		LOGGER.info("getTestDate start");
		
		PolicyRequirementDAO reqDAO = new PolicyRequirementDAO();
		Date prTestDate = null;
		
		try {
			prTestDate = reqDAO.retrievePrTestDate(reqId);
		} catch (Exception e) {
			LOGGER.error("ReqtId: " + reqId + " : Error in getting pr_test_date. ");
			LOGGER.error(CodeHelper.getStackTrace(e));
		} 
		LOGGER.info("getTestDate end");
		return prTestDate;
	}
	
public String getBillingType(String refNo){
	
	LOGGER.info("getBillingType start");
	
	String billingType = null;
	
	IngeniumGateway ig = new IngeniumGateway(true);
	AssessmentRequest ar = new AssessmentRequest();
	String suffix = null;
	try {
		suffix = ar.getPolicySuffix(refNo);
	} catch (IUMException e) {
		LOGGER.error(CodeHelper.getStackTrace(e));
	}
	
	HashMap map = ig.retrieveIngeniumConsolidatedInformation(refNo, suffix);
	Iterator itConsol = map.keySet().iterator();
	
	while(itConsol.hasNext()){
		String key = (String) itConsol.next();
		LOGGER.debug("key-"+ key);
		if("BILLING_TYPE_CODE".equals(key)){
			LOGGER.debug("billing tyyype "+ key);
			List billingTypeList = (List)map.get(key);
			LOGGER.debug(StringUtils.join(billingTypeList, ", "));
			LOGGER.debug("getBillingType.. BillingTypesize-"+ billingTypeList.size());

			ArrayList listInfo = new ArrayList();
			for(Iterator it = billingTypeList.iterator(); it.hasNext();){
				billingType = (String)it.next();
				LOGGER.debug("getBillingType.. BillingType-"+ billingType);
			}
		}

	}
	LOGGER.info("getBillingType end");
	return billingType;
	}

	private int computeUWFollowUpDate(String referenceNum, String clientId) throws SQLException {
		LOGGER.info("computeUWFollowUpDate start");
	    int followUpNumber = 30;

	    PolicyRequirementDAO prDao = new PolicyRequirementDAO();
	    int reqtCount = prDao.countRequirementsByRefNumAndClientId(referenceNum, clientId);
	    
	    if (reqtCount >= 1) {
	      HashMap initialDatesHm = prDao.retrieveInitialDatesByRefNumAndClientId(referenceNum, clientId);
	      String orderDateStr = initialDatesHm.get("PR_ORDER_DATE").toString();
	      String followUpDateStr = initialDatesHm.get("PR_FOLLOW_UP_DATE").toString();

	      int overlapReqtCount = prDao.countRequirementsGreaterThanFollowUpDate(referenceNum, clientId, followUpDateStr.substring(0, 10));
	      
	      Date initialReqtOrderDate = DateUtil.convertStringToDate(orderDateStr, "yyyy-MM-dd");
	      LOGGER.info("initialReqtOrderDate: " + initialReqtOrderDate);
	      Date initialReqtFollowUpDate = DateUtil.convertStringToDate(followUpDateStr, "yyyy-MM-dd");
	      LOGGER.info("initialReqtFollowUpDate: " + initialReqtFollowUpDate);

	      Date dateToday = new Date();
	      DateUtil.setDateTimeToZero(dateToday);
	      DateUtil.ConvertDate(dateToday, "yyyy-MM-dd");

	      int orderDateDiffInDays = (int) DateHelper.getDaysDifference(dateToday, initialReqtOrderDate);
	      LOGGER.info("order date diff in days: " + orderDateDiffInDays);
	      int followUpDateDiffInDays = (int) DateHelper.getDaysDifference(initialReqtFollowUpDate, dateToday) + 1;
	      LOGGER.info("follow up date diff in days: " + followUpDateDiffInDays);

	      if (overlapReqtCount == 0) {
	    	  if (orderDateDiffInDays <= 20) {
	  	        followUpNumber = followUpDateDiffInDays;
	  	      } else if ((orderDateDiffInDays > 20) && (orderDateDiffInDays <= 30)) {
	  	        followUpNumber = followUpDateDiffInDays + 10;
	  	      }
	      } else {
	    	  if (orderDateDiffInDays >= 31) {
	    		  followUpNumber = 10;
	    	  }
	      }
	    }

	    LOGGER.info("followUpNumber: " + followUpNumber);
	    LOGGER.info("computeUWFollowUpDate end");
	    return followUpNumber;
	  }
}
