package com.slocpi.ium.ingenium.transaction;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.slocpi.ium.ingenium.transaction.parameter.TransactionParameter;
import com.slocpi.ium.ingenium.transaction.request.ClientTransactionRequest;
import com.slocpi.ium.ingenium.transaction.request.PolicyTransactionRequest;
import com.slocpi.ium.ingenium.transaction.request.RequirementTransactionRequest;


public class TransactionBuilder {

	private static final Logger LOGGER = LoggerFactory.getLogger(TransactionBuilder.class);

	public static Transaction buildPolicyListTransaction() {
		LOGGER.info("buildPolicyListTransaction 1");
		return new PolicyListTransaction(PolicyTransactionRequest.getPolicyListRequest());
	}
	
	public static Transaction buildPolicyListTransaction(String policyNumber, String suffix) {
		LOGGER.info("buildPolicyListTransaction 2");
		return new PolicyListTransaction(PolicyTransactionRequest.getPolicyListRequest(policyNumber,suffix));
	}
	
	public static Transaction buildClientDataTransaction(String policyNumber, String suffix)  {
		LOGGER.info("buildClientDataTransaction");
		return new ClientDataTransaction(PolicyTransactionRequest.getClientDataRequest(policyNumber, suffix));
	}

	public static Transaction buildKickoutTransaction(String policyNumber, String suffix)  {
		LOGGER.info("buildKickoutTransaction");
		return new KickoutTransaction(PolicyTransactionRequest.getKOReqtRequest(policyNumber, suffix));
	}

	public static Transaction buildDeletePolicyTransaction(String policyNumber, String suffix)  {
		LOGGER.info("buildDeletePolicyTransaction");
		return new DeletePolicyTransaction (PolicyTransactionRequest.getDeletePolicyRequest(policyNumber, suffix));
	}

	public static Transaction buildRequirementMaintenanceTransaction(TransactionParameter parm) {
		LOGGER.info("buildRequirementMaintenanceTransaction");
		return new RequirementMaintenanceTransaction(RequirementTransactionRequest.getRequirementMaintenanceRequest(parm));
	}

	public static Transaction buildClientInquiryTransaction(String clientId)  {
		LOGGER.debug("buildClientInquiryTransaction");
		return new ClientInquiryTransaction (ClientTransactionRequest.getClientInquiryRequest(clientId));
	}

}
