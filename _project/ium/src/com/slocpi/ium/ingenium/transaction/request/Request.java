package com.slocpi.ium.ingenium.transaction.request;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jdom.Attribute;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.Namespace;
import org.jdom.input.SAXBuilder;
import org.jdom.output.XMLOutputter;
import com.slocpi.ium.ingenium.transaction.data.UserAuthorization;
import com.slocpi.ium.ingenium.transaction.parameter.TransactionParameter;
import com.slocpi.ium.util.CodeHelper;

public abstract class Request {
	private static final Logger LOGGER = LoggerFactory.getLogger(Request.class);
	
	protected Document doc;
	protected Namespace ns;
	protected Element common;

	public static final String USER_AUTH_REQUEST = "UserAuthRequest";
	public static final String USER_LOGIN_NAME = "UserLoginName";
	public static final String USER_PASSWORD = "UserPswd";
	public static final String CRYPT_TYPE = "CryptType";
	public static final String PASSWORD = "Pswd";
	public static final String TXLIFE_REQUEST = "TXLifeRequest";
	public static final String TRANS_REF_GUID = "TransRefGUID";
	public static final String TRANS_TYPE = "TransType";
	public static final String TRANS_CODE = "tc";
	public static final String TRANS_LIFE = "OLifE";
	public static final String TRANS_LIFE_COMMON = "MirCommonFields";
	private String serviceWSDL;

	protected void loadTemplate(String templatePath) {
		
		LOGGER.info("loadTemplate start");
		
		SAXBuilder sb = new SAXBuilder();
		
		try {

			doc = sb.build(new FileReader(templatePath));
			ns = doc.getRootElement().getNamespace();
			common = doc.getRootElement().getChild(TXLIFE_REQUEST, ns)
						.getChild(TRANS_LIFE, ns)
						.getChild(TRANS_LIFE_COMMON, ns);
		} catch (FileNotFoundException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
		} catch (JDOMException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
		} catch (IOException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
		}
		LOGGER.info("loadTemplate end");
	}

	protected void setUserAuthorization(UserAuthorization ua) {
		
		LOGGER.info("setUserAuthorization start");
		Element userName = doc.getRootElement().getChild(Request.USER_AUTH_REQUEST, ns).getChild(Request.USER_LOGIN_NAME, ns);
		userName.setText(ua.getUserName());

		Element element = doc.getRootElement().getChild(Request.USER_AUTH_REQUEST, ns).getChild(Request.USER_PASSWORD, ns);

		// write crypt type
		Element cryptType = element.getChild(Request.CRYPT_TYPE, ns);
		cryptType.setText(ua.getEncryption());

		// write user password
		Element password = element.getChild(Request.PASSWORD, ns);
		password.setText(ua.getPassword());
		
		LOGGER.info("setUserAuthorization end");
	}


	public String toXML(){
		
		LOGGER.info("toXML start");
		XMLOutputter out = new XMLOutputter();
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		try {
			out.output(doc, bos);
		} catch (IOException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
		}
		LOGGER.info("toXML end");
		return bos.toString();
	}

	public void setTransactionDetails(String transactionName, String guid) {
		
		LOGGER.info("setTransactionDetails start");
		
		Element request = doc.getRootElement().getChild(TXLIFE_REQUEST, ns);
		Element guidElement = request.getChild(TRANS_REF_GUID, ns);
		guidElement.setText(guid);
		Element transaction = request.getChild(TRANS_TYPE, ns);
		transaction.setText(transactionName);
		Attribute attrib = transaction.getAttribute(TRANS_CODE);
		attrib.setValue(transactionName);
		
		LOGGER.info("setTransactionDetails end");
	}

	public abstract void setParameter(TransactionParameter tp);

	/**
	 * @return Returns the serviceWSDL.
	 */
	public String getServiceWSDL() {
		return serviceWSDL;
	}

	/**
	 * @param serviceWSDL The serviceWSDL to set.
	 */
	protected void setServiceWSDL(String serviceWSDL) {
		this.serviceWSDL = serviceWSDL;
	}
}
