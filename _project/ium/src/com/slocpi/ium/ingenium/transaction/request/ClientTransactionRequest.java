// Missing Company Details - Andre Ceasar Dacanay
package com.slocpi.ium.ingenium.transaction.request;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jdom.Element;
import com.slocpi.ium.ingenium.transaction.Configuration;
import com.slocpi.ium.ingenium.transaction.parameter.ClientInquiryParameter;
import com.slocpi.ium.ingenium.transaction.parameter.TransactionParameter;

public class ClientTransactionRequest extends Request {

	private static final Logger LOGGER = LoggerFactory.getLogger(ClientTransactionRequest.class);
	private static final String CLIENT_DATA = "ClientData";
	private static final String MIR_CLI_INFO = "MirCliInfo";
	private static final String MIR_CLI_ID = "MirCliId";
	
	public ClientTransactionRequest(Configuration config) {
		
		LOGGER.info("Instantiating ClientTransactionRequest start");
		super.loadTemplate(config.getCliTemplatePath());
		super.setUserAuthorization(config.getUser());
		super.setServiceWSDL(config.getServiceWSDL());
		LOGGER.info("Instantiating ClientTransactionRequest end");
		
	}

	public void setParameter(TransactionParameter tp) {
		
		LOGGER.info("setParameter start");
		String clientId = tp.getParameterValue(ClientInquiryParameter.PARM_CLIENT_NUMBER);
		Element clientDataElem = doc.getRootElement().getChild(TXLIFE_REQUEST, ns)
				.getChild(TRANS_LIFE, ns)
				.getChild(CLIENT_DATA, ns);
		Element clientInfoElem = clientDataElem.getChild(MIR_CLI_INFO, ns);
		Element clientIdElem = clientInfoElem.getChild(MIR_CLI_ID, ns);
		clientIdElem.setText(clientId);
		LOGGER.info("setParameter end");
	}

	public static Request getClientInquiryRequest(String clientId) {
		
		LOGGER.info("getClientInquiryRequest start");
		Configuration config = new Configuration();
		Request req = new ClientTransactionRequest(config);
		req.setTransactionDetails(config.getClientInquiryTransactionName(), config.getGUID());

		ClientInquiryParameter cip = new ClientInquiryParameter();
		cip.setParameterValue(ClientInquiryParameter.PARM_CLIENT_NUMBER, clientId);

		req.setParameter(cip);
		LOGGER.info("getClientInquiryRequest end");
		return req;
	}

	

}

