package com.slocpi.ium.ingenium.transaction;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.slocpi.ium.ingenium.transaction.request.Request;
import com.slocpi.ium.ingenium.transaction.response.Response;
import com.slocpi.ium.util.CodeHelper;
import com.solcorp.elink.webservices.TXLifeService;
import com.solcorp.elink.webservices.TXLifeServiceLocator;
import com.solcorp.elink.webservices.TXLifeServicePort;

/**
 * @author nic.decapia
 *
 */
public abstract class Transaction {

	private static final Logger LOGGER = LoggerFactory.getLogger(Transaction.class);

	private Request request;
	private String  response;

	public Transaction(Request request) {
		this.request = request;
	}

	/**
	 * @return Returns the request.
	 */
	public Request getRequest() {
		return request;
	}

	/**
	 * @return Returns the response.
	 */
	public String getResponse() {
		return response;
	}

	protected String callTXLife() {
		
		LOGGER.info("callTXLife start");
		String requestXML = request.toXML();
		String requestXMLLog = requestXML;
		LOGGER.info("XML request--> " + requestXMLLog.replaceAll("[\r\n]+", " "));
		
		TXLifeServicePort port;
		String response = null;
		try {
			TXLifeService txLifeService 
				= new TXLifeServiceLocator(request.getServiceWSDL(), new TXLifeServiceLocator().getServiceName());
			port = txLifeService.getTXLifeService();
   		    response = port.callTXLife(requestXML);
   		    LOGGER.info("XML response--> " + response);
   		    this.response = response;
		} catch (Exception e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
		}
		LOGGER.info("callTXLife end");
		return response;
	}

	public abstract Response execute();
}
