package com.slocpi.ium.ingenium.transaction;

import java.util.MissingResourceException;
import java.util.ResourceBundle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.slocpi.ium.ingenium.transaction.data.UserAuthorization;
import com.slocpi.ium.util.CodeHelper;

public class Configuration {

	private static final String INGENIUM_WS_CONFIG = "ingenium";
	private static final String INGENIUM_WS_TEMPLATE_PATH_POL = "ingenium.template.pol";
	private static final String INGENIUM_WS_TEMPLATE_PATH_REQ = "ingenium.template.req";
	private static final String INGENIUM_WS_TEMPLATE_PATH_CLI= "ingenium.template.cli";
	private static final String INGENIUM_WS_SERVICE_WSDL = "ingenium.service.wsdl";
	private static final String INGENIUM_WS_USER_NAME = "ingenium.userid";
	private static final String INGENIUM_WS_PASSWORD = "ingenium.password";
	private static final String INGENIUM_WS_PASSWORD_ENCRYPT = "ingenium.encrypt";
	private static final String INGENIUM_WS_GUID = "ingenium.tran.guid";
	public static final String INGENIUM_WS_TRAN_POLICY_LIST = "ingenium.tran.pollist";
	public static final String INGENIUM_WS_TRAN_KICKOUT = "ingenium.tran.kickout";
	public static final String INGENIUM_WS_TRAN_CDS = "ingenium.tran.cds";
	public static final String INGENIUM_WS_TRAN_DEL_POL = "ingenium.tran.delpollist";
	public static final String INGENIUM_WS_TRAN_REQT_CREATE = "ingenium.tran.reqt.create";
	public static final String INGENIUM_WS_TRAN_REQT_UPDATE = "ingenium.tran.reqt.update";
	public static final String INGENIUM_WS_TRAN_CLIENT_INQ = "ingenium.tran.client";

	private UserAuthorization user;
	private String polTemplatePath;
	private String reqTemplatePath;
	private String cliTemplatePath;
	private String GUID;
	private String policyListTransactionName;
	private String kickoutTransactionName;
	private String clientDataTransactionName;
	private String deletePolicyListTransactionName;
	private String createRequirementTransactionName;
	private String updateRequirementTransactionName;
	private String clientInquiryName;

	private String serviceWSDL;

	private static final Logger LOGGER = LoggerFactory.getLogger(Configuration.class);


	public Configuration() {
		
		LOGGER.info("Configuration start");
		try {
			
			ResourceBundle rb = ResourceBundle.getBundle(INGENIUM_WS_CONFIG);
			serviceWSDL = rb.getString(INGENIUM_WS_SERVICE_WSDL);
			
			// load the template location
			polTemplatePath = rb.getString(INGENIUM_WS_TEMPLATE_PATH_POL);
			reqTemplatePath = rb.getString(INGENIUM_WS_TEMPLATE_PATH_REQ);
			cliTemplatePath = rb.getString(INGENIUM_WS_TEMPLATE_PATH_CLI);
			
			// load user credentials
			user = new UserAuthorization();
			user.setUserName(rb.getString(INGENIUM_WS_USER_NAME));
			user.setPassword(rb.getString(INGENIUM_WS_PASSWORD));
			user.setEncryption(rb.getString(INGENIUM_WS_PASSWORD_ENCRYPT));

			// load transaction details
			GUID = rb.getString(INGENIUM_WS_GUID);
			policyListTransactionName = rb.getString(INGENIUM_WS_TRAN_POLICY_LIST);
			kickoutTransactionName = rb.getString(INGENIUM_WS_TRAN_KICKOUT);
			clientDataTransactionName = rb.getString(INGENIUM_WS_TRAN_CDS);
			deletePolicyListTransactionName = rb.getString(INGENIUM_WS_TRAN_DEL_POL);
			createRequirementTransactionName = rb.getString(INGENIUM_WS_TRAN_REQT_CREATE);
			updateRequirementTransactionName = rb.getString(INGENIUM_WS_TRAN_REQT_UPDATE);
			clientInquiryName = rb.getString(INGENIUM_WS_TRAN_CLIENT_INQ);

		} catch (MissingResourceException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
		}
		LOGGER.info("Configuration end");
	}

	/**
	 * @return Returns the user.
	 */
	public UserAuthorization getUser() {
		return user;
	}

	/**
	 * @return Returns the gUID.
	 */
	public String getGUID() {
		return GUID;
	}

	/**
	 * @return Returns the polTemplatePath.
	 */
	public String getPolTemplatePath() {
		return polTemplatePath;
	}
	
	/**
	 * @return Returns the cliTemplatePath.
	 */
	public String getCliTemplatePath() {
		return cliTemplatePath;
	}

	/**
	 * @return Returns the clientDataTransactionName.
	 */
	public String getClientDataTransactionName() {
		return clientDataTransactionName;
	}

	/**
	 * @return Returns the kickoutTransactionName.
	 */
	public String getKickoutTransactionName() {
		return kickoutTransactionName;
	}

	/**
	 * @return Returns the policyListTransactionName.
	 */
	public String getPolicyListTransactionName() {
		return policyListTransactionName;
	}

	/**
	 * @return Returns the deletePolicyListTransactionName.
	 */
	public String getDeletePolicyTransactionName() {
		return deletePolicyListTransactionName;
	}

	/**
	 * @return Returns the createRequirementTransactionName.
	 */
	public String getCreateRequirementTransactionName() {
		return createRequirementTransactionName;
	}

	/**
	 * @return Returns the updateRequirementTransactionName.
	 */
	public String getUpdateRequirementTransactionName() {
		return updateRequirementTransactionName;
	}

	/**
	 * @return Returns the reqTemplatePath.
	 */
	public String getReqTemplatePath() {
		return reqTemplatePath;
	}

	/**
	 * @return Returns the serviceWSDL.
	 */
	public String getServiceWSDL() {
		return serviceWSDL;
	}

	// Missing Company Details - Andre Ceasar Dacanay - start
	/**
	 * @return Returns the clientInquiryName.
	 */
	public String getClientInquiryTransactionName() {
		return clientInquiryName;
	}
	// Missing Company Details - Andre Ceasar Dacanay - end
}
