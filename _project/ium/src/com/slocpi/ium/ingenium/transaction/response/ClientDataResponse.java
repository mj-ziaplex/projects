package com.slocpi.ium.ingenium.transaction.response;

import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jdom.Element;
import com.slocpi.ium.ingenium.transaction.response.detail.ExistingPolicyInformation;
import com.slocpi.ium.ingenium.transaction.response.detail.InsuredInformation;
import com.slocpi.ium.ingenium.transaction.response.detail.OwnerInformation;
import com.slocpi.ium.ingenium.transaction.response.detail.RelPolicyInformation;



/*
 * author @nic.decapia
 *
 *
 *
 */
public class ClientDataResponse extends Response {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ClientDataResponse.class);
	// Insured/Owner Info
	private InsuredInformation ii = new InsuredInformation();
	private OwnerInformation oi = new OwnerInformation();
	private ExistingPolicyInformation epi = new ExistingPolicyInformation();
	private List rpi = new ArrayList();
	
	private static final String insuredInfo = "MirInsrdCliInfo";
	private static final String insuredMortality = "MirInsrdMortInfo";
	private static final String ownerInfo = "MirOwnrCliInfo";
	private static final String ownerMortality = "MirOwnrMortInfo";
	private static final String existingPolInfo = "MirExstngPolInfo";

	// Tags for related policy and coverage information
	private static final String relPolCoverage = "MirRelPolCovInfo";
	private static final String relPoliciesIds = "MirRpcRelSysRefPolId";
	private static final String relPolId = "MirRpcRelPolIdT";
	private static final String relPolSeq = "MirRpcRelCvgNumT";
	private static final String relPolTypeCode = "MirRpcRelTypCdT";
	private static final String REL_CVG_ISS_EFF_DATE_T = "MirRpcRelCvgIssEffDtT";
	private static final String REL_CVG_CSTAT_CD_T = "MirRpcRelCvgCstatCdT";
	private static final String REL_CVG_SMOKR_CD_T = "MirRpcRelCvgSmkrCdT";
	private static final String REL_PLAN_ID_T = "MirRpcRelPlanIdT";
	private static final String REL_EVDN_EVTY_CD_T = "MirRpcRelEvdnEvtypCdT";
	private static final String REL_CVG_FACE_AMOUNT_T = "MirRpcRelCvgFaceAmtT";
	private static final String REL_CVG_UWDE_SUB_CD_T = "MirRpcRelCvgUwdeSubCdT";
	private static final String REL_CVG_ADB_FACE_AMOUNT_T = "MirRpcRelCvgAdbFaceAmtT";
	private static final String REL_CVG_AD_MULT_FCT_T = "MirRpcRelCvgAdMultFctT";
	private static final String REL_CVG_WP_MULT_FCT_T = "MirRpcRelCvgWpMultFctT";
	private static final String REL_REINS_FACE_AMOUNT_T = "MirRpcRelReinsFaceAmtT";
	private static final String REL_CVG_ME_FCT_T = "MirRpcRelCvgMeFctT";
	private static final String REL_CVG_FE_UPREM_AMOUNT_T = "MirRpcRelCvgFeUpremAmtT";
	private static final String REL_CVG_FE_PERM_AMOUNT_T = "MirRpcRelCvgFePermAmtT";
	private static final String MIR_RPC_REL_POL_APP_REINST_DT = "MirRpcRelPolAppReinsDtT";
	private static final String MIR_RPC_REL_ADB_FACEAMT = "MirRpcRelAdbFaceAmtT";
	private static final String MIR_RPC_REL_TRTY_TYP_CD_T = "MirRpcRelTrtyTypCdT";
	private static final String CDSInquiryData = "CDSInquiryData";
	private static final String CDSRPCListData = "CDSRPCListData";

	// Lists for related policy and coverage information
	private List relPolicyIdList = new ArrayList();
	private List relCvgNumberList = new ArrayList();
	private List relTypCodeList = new ArrayList();
	private List relCvgIssEffDateList = new ArrayList();
	private List relCvgCStatCodeList = new ArrayList();
	private List relCvgSmokerCodeList = new ArrayList();
	private List relPlanIdList = new ArrayList();
	private List relEvidenceEvtyCodeList = new ArrayList();
	private List relCvgFaceAmountList = new ArrayList();
	private List relCvgUwdeSubCodeList = new ArrayList();
	private List relCvgAdbFaceAmountList = new ArrayList();
	private List relCvgAdMultFctList = new ArrayList();
	private List relCvgWpMultFctList = new ArrayList();
	private List relReinsFaceAmountList = new ArrayList();
	private List relCvgMeFct = new ArrayList();
	private List relCvgFeUpremAmt = new ArrayList();
	private List relCvgFePermAmt = new ArrayList();
	private List reinstatementDate = new ArrayList();
	private List reinsuranceType = new ArrayList();
	private List adbFaceAmount = new ArrayList();
	

	public void parse(String content) {
		
		super.parse(content);
		LOGGER.info("parse start");
		oi.parse(response.getChild(ownerInfo, ns), response.getChild(ownerMortality, ns));
		ii.parse(response.getChild(insuredInfo, ns), response.getChild(insuredMortality, ns));
		epi.parse(response.getChild(existingPolInfo, ns));
		
		Element relparent = response.getChild(relPolCoverage, ns).getChild(relPoliciesIds, ns);
		List relnodes = relparent.getChildren();
		for (int i = 0; i < relnodes.size(); i++) {
			
			Element element = (Element) relnodes.get(i);
			String attrib = element.getName();
			String value = super.setElementValue(element.getText());
			
			if (attrib.equalsIgnoreCase(relPolId)) {
				relPolicyIdList.add(value);
			} else if (attrib.equalsIgnoreCase(relPolSeq)) {
				relCvgNumberList.add(value);
			}				
		}
		
		relparent = response.getChild(relPolCoverage, ns);
		relnodes = relparent.getChildren();
		for (int i = 0; i < relnodes.size(); i++) {
			
			Element element = (Element) relnodes.get(i);
			String attrib = element.getName();
			String value = super.setElementValue(element.getText());
			
			if (attrib.equalsIgnoreCase(relPolTypeCode)) {
				relTypCodeList.add(value);												
			} else if (attrib.equalsIgnoreCase(REL_CVG_ISS_EFF_DATE_T)) {
				relCvgIssEffDateList.add(value);
			} else if (attrib.equalsIgnoreCase(REL_CVG_CSTAT_CD_T)) {
				relCvgCStatCodeList.add(value);
			} else if (attrib.equalsIgnoreCase(REL_CVG_SMOKR_CD_T)) {
				relCvgSmokerCodeList.add(value);
			} else if (attrib.equalsIgnoreCase(REL_PLAN_ID_T)) {
				relPlanIdList.add(value);
			} else if (attrib.equalsIgnoreCase(REL_EVDN_EVTY_CD_T)) {
				relEvidenceEvtyCodeList.add(value);
			} else if (attrib.equalsIgnoreCase(REL_CVG_FACE_AMOUNT_T)) {
				relCvgFaceAmountList.add(value);
			} else if (attrib.equalsIgnoreCase(REL_CVG_UWDE_SUB_CD_T)) {
				relCvgUwdeSubCodeList.add(value);
			} else if (attrib.equalsIgnoreCase(REL_CVG_ADB_FACE_AMOUNT_T)) {
				relCvgAdbFaceAmountList.add(value);
			} else if (attrib.equalsIgnoreCase(REL_CVG_AD_MULT_FCT_T)) {
				relCvgAdMultFctList.add(value);
			}  else if (attrib.equalsIgnoreCase(REL_CVG_WP_MULT_FCT_T)) {
				relCvgWpMultFctList.add(value);				
			}  else if (attrib.equalsIgnoreCase(REL_REINS_FACE_AMOUNT_T)) {
				relReinsFaceAmountList.add(value);
			} else if (attrib.equalsIgnoreCase(REL_CVG_ME_FCT_T)) {
				relCvgMeFct.add(value);
			} else if (attrib.equalsIgnoreCase(REL_CVG_FE_UPREM_AMOUNT_T)) {
				relCvgFeUpremAmt.add(value);
			} else if (attrib.equalsIgnoreCase(REL_CVG_FE_PERM_AMOUNT_T)) {
				relCvgFePermAmt.add(value);
			} else if (attrib.equalsIgnoreCase(MIR_RPC_REL_POL_APP_REINST_DT)) {
				reinstatementDate.add(value);
			} else if (attrib.equalsIgnoreCase(MIR_RPC_REL_TRTY_TYP_CD_T)) {
				reinsuranceType.add(value);
			} else if (attrib.equalsIgnoreCase(MIR_RPC_REL_ADB_FACEAMT)) {
				adbFaceAmount.add(value);
			} 
		}
		LOGGER.info("parse end");
		groupRelPolicyInformation();
	}
	
	public void parseCdsInquiry(String content) {
		
		super.parse(content);
		LOGGER.info("parseCdsInquiry start");
		final Element owner = response.getChild(CDSInquiryData, ns).getChild(ownerInfo, ns);
		if (owner != null) {
			oi.parseOwner(owner);
		}
		final Element insured = response.getChild(CDSInquiryData, ns).getChild(insuredInfo, ns);
		if (insured != null) {
			ii.parseInsured(insured);
		}
		epi.parse(response.getChild(CDSInquiryData, ns).getChild(existingPolInfo, ns));
		LOGGER.info("parseCdsInquiry end");
	}
	
	public void parseCdsRPCListInquiry(String content) {
		
		super.parse(content);
		LOGGER.info("parseCdsRPCListInquiry start");
		
		Element relparent = response.getChild(CDSRPCListData, ns).getChild(relPolCoverage, ns).getChild(relPoliciesIds, ns);
		
		List relnodes = new ArrayList();
		if(relparent!=null && relparent.getChildren()!=null){
			relnodes = relparent.getChildren();
		 
			for (int i = 0; i < relnodes.size(); i++) {
				Element element = (Element) relnodes.get(i);
				String attrib = element.getName();
				String value = super.setElementValue(element.getText());
				if (value != null && !"".equals(value)) {
					if (attrib.equalsIgnoreCase(relPolId)) {
						relPolicyIdList.add(value);
					} else if (attrib.equalsIgnoreCase(relPolSeq)) {
						relCvgNumberList.add(value);
					}		
				}
			}
		}
		
		relparent = response.getChild(CDSRPCListData, ns).getChild(relPolCoverage, ns);
		if(relparent!=null && relparent.getChildren()!=null){
			relnodes = relparent.getChildren();
			
			for (int i = 0; i < relnodes.size(); i++) {
				Element element = (Element) relnodes.get(i);
				String attrib = element.getName();
				String value = super.setElementValue(element.getText());
				if (value != null && !"".equals(value)) {
					if (attrib.equalsIgnoreCase(relPolTypeCode)) {
						relTypCodeList.add(value);												
					} else if (attrib.equalsIgnoreCase(REL_CVG_ISS_EFF_DATE_T)) {
						relCvgIssEffDateList.add(value);
					} else if (attrib.equalsIgnoreCase(REL_CVG_CSTAT_CD_T)) {
						relCvgCStatCodeList.add(value);
					} else if (attrib.equalsIgnoreCase(REL_CVG_SMOKR_CD_T)) {
						relCvgSmokerCodeList.add(value);
					} else if (attrib.equalsIgnoreCase(REL_PLAN_ID_T)) {
						relPlanIdList.add(value);
					} else if (attrib.equalsIgnoreCase(REL_EVDN_EVTY_CD_T)) {
						relEvidenceEvtyCodeList.add(value);
					} else if (attrib.equalsIgnoreCase(REL_CVG_FACE_AMOUNT_T)) {
						relCvgFaceAmountList.add(value);
					} else if (attrib.equalsIgnoreCase(REL_CVG_UWDE_SUB_CD_T)) {
						relCvgUwdeSubCodeList.add(value);
					} else if (attrib.equalsIgnoreCase(REL_CVG_ADB_FACE_AMOUNT_T)) {
						relCvgAdbFaceAmountList.add(value);
					} else if (attrib.equalsIgnoreCase(REL_CVG_AD_MULT_FCT_T)) {
						relCvgAdMultFctList.add(value);
					}  else if (attrib.equalsIgnoreCase(REL_CVG_WP_MULT_FCT_T)) {
						relCvgWpMultFctList.add(value);				
					}  else if (attrib.equalsIgnoreCase(REL_REINS_FACE_AMOUNT_T)) {
						relReinsFaceAmountList.add(value);
					} else if (attrib.equalsIgnoreCase(REL_CVG_ME_FCT_T)) {
						relCvgMeFct.add(value);
					} else if (attrib.equalsIgnoreCase(REL_CVG_FE_UPREM_AMOUNT_T)) {
						relCvgFeUpremAmt.add(value);
					} else if (attrib.equalsIgnoreCase(REL_CVG_FE_PERM_AMOUNT_T)) {
						relCvgFePermAmt.add(value);
					} else if (attrib.equalsIgnoreCase(MIR_RPC_REL_POL_APP_REINST_DT)) {
						reinstatementDate.add(value);
					} else if (attrib.equalsIgnoreCase(MIR_RPC_REL_TRTY_TYP_CD_T)) {
						reinsuranceType.add(value);
					} else if (attrib.equalsIgnoreCase(MIR_RPC_REL_ADB_FACEAMT)) {
						adbFaceAmount.add(value);
					} 
				}			
			}
		}
		LOGGER.info("parseCdsRPCListInquiry end");
		groupRelPolicyInformation();
		
	}

	public void parseCds(String content) {
		
		super.parse(content);
		LOGGER.info("parseCds start");
		// Get owner info
		final Element owner = response.getChild(CDSInquiryData, ns).getChild(ownerInfo, ns);
		if (owner != null) {
			oi.parseOwner(owner);
		}
		
		// Get insured info
		final Element insured = response.getChild(CDSInquiryData, ns).getChild(insuredInfo, ns);
		if (insured != null) {
			ii.parseInsured(insured);
		}
		epi.parse(response.getChild(CDSInquiryData, ns).getChild(existingPolInfo, ns));
		
		Element relparent = response.getChild(CDSInquiryData, ns).getChild(relPolCoverage, ns).getChild(relPoliciesIds, ns);
		List relnodes = relparent.getChildren();
		 
		for (int i = 0; i < relnodes.size(); i++) {
			Element element = (Element) relnodes.get(i);
			String attrib = element.getName();
			String value = super.setElementValue(element.getText());
			if (value != null && !"".equals(value)) {
				if (attrib.equalsIgnoreCase(relPolId)) {
					relPolicyIdList.add(value);
				} else if (attrib.equalsIgnoreCase(relPolSeq)) {
					relCvgNumberList.add(value);
				}		
			}
		}
		
		relparent = response.getChild(CDSInquiryData, ns).getChild(relPolCoverage, ns);
		relnodes = relparent.getChildren();
		
		for (int i = 0; i < relnodes.size(); i++) {
			Element element = (Element) relnodes.get(i);
			String attrib = element.getName();
			String value = super.setElementValue(element.getText());
			if (value != null && !"".equals(value)) {
				if (attrib.equalsIgnoreCase(relPolTypeCode)) {
					relTypCodeList.add(value);												
				} else if (attrib.equalsIgnoreCase(REL_CVG_ISS_EFF_DATE_T)) {
					relCvgIssEffDateList.add(value);
				} else if (attrib.equalsIgnoreCase(REL_CVG_CSTAT_CD_T)) {
					relCvgCStatCodeList.add(value);
				} else if (attrib.equalsIgnoreCase(REL_CVG_SMOKR_CD_T)) {
					relCvgSmokerCodeList.add(value);
				} else if (attrib.equalsIgnoreCase(REL_PLAN_ID_T)) {
					relPlanIdList.add(value);
				} else if (attrib.equalsIgnoreCase(REL_EVDN_EVTY_CD_T)) {
					relEvidenceEvtyCodeList.add(value);
				} else if (attrib.equalsIgnoreCase(REL_CVG_FACE_AMOUNT_T)) {
					relCvgFaceAmountList.add(value);
				} else if (attrib.equalsIgnoreCase(REL_CVG_UWDE_SUB_CD_T)) {
					relCvgUwdeSubCodeList.add(value);
				} else if (attrib.equalsIgnoreCase(REL_CVG_ADB_FACE_AMOUNT_T)) {
					relCvgAdbFaceAmountList.add(value);
				} else if (attrib.equalsIgnoreCase(REL_CVG_AD_MULT_FCT_T)) {
					relCvgAdMultFctList.add(value);
				}  else if (attrib.equalsIgnoreCase(REL_CVG_WP_MULT_FCT_T)) {
					relCvgWpMultFctList.add(value);				
				}  else if (attrib.equalsIgnoreCase(REL_REINS_FACE_AMOUNT_T)) {
					relReinsFaceAmountList.add(value);
				} else if (attrib.equalsIgnoreCase(REL_CVG_ME_FCT_T)) {
					relCvgMeFct.add(value);
				} else if (attrib.equalsIgnoreCase(REL_CVG_FE_UPREM_AMOUNT_T)) {
					relCvgFeUpremAmt.add(value);
				} else if (attrib.equalsIgnoreCase(REL_CVG_FE_PERM_AMOUNT_T)) {
					relCvgFePermAmt.add(value);
				} else if (attrib.equalsIgnoreCase(MIR_RPC_REL_POL_APP_REINST_DT)) {
					reinstatementDate.add(value);
				} else if (attrib.equalsIgnoreCase(MIR_RPC_REL_TRTY_TYP_CD_T)) {
					reinsuranceType.add(value);
				} else if (attrib.equalsIgnoreCase(MIR_RPC_REL_ADB_FACEAMT)) {
					adbFaceAmount.add(value);
				} 
			}			
		}
		LOGGER.info("parseCds end");
		groupRelPolicyInformation();
	}

	private void groupRelPolicyInformation() {
		
		LOGGER.info("groupRelPolicyInformation start");
		for (int i = 0; i < relPolicyIdList.size(); i++) {
			RelPolicyInformation relPolicy = new RelPolicyInformation();
			if (relPolicyIdList != null && i < relPolicyIdList.size()) {
				relPolicy.setRelPolicyId((String) relPolicyIdList.get(i));	
			}
			if (relCvgNumberList != null && i < relCvgNumberList.size()) {
				relPolicy.setRelCvgNumber((String) relCvgNumberList.get(i));
			}
			if (relTypCodeList != null && i < relTypCodeList.size()) {
				relPolicy.setRelTypCode((String) relTypCodeList.get(i));
			}
			if (relCvgIssEffDateList != null && i < relCvgIssEffDateList.size()) {
				relPolicy.setRelCvgIssEffDate((String) relCvgIssEffDateList.get(i));
			}
			if (relCvgCStatCodeList != null && i < relCvgCStatCodeList.size()) {
				relPolicy.setRelCvgCStatCode((String) relCvgCStatCodeList.get(i));
			}
			if (relCvgSmokerCodeList != null && i < relCvgSmokerCodeList.size()) {
				relPolicy.setRelCvgSmokerCode((String) relCvgSmokerCodeList.get(i));
			}
			if (relPlanIdList != null && i < relPlanIdList.size()) {
				relPolicy.setRelPlanId((String)relPlanIdList.get(i));
			}
			if (relEvidenceEvtyCodeList != null && i < relEvidenceEvtyCodeList.size()) {
				relPolicy.setRelEvidenceEvtyCode((String) relEvidenceEvtyCodeList.get(i));
			}
			if (relCvgFaceAmountList != null && i < relCvgFaceAmountList.size()) {
				relPolicy.setRelCvgFaceAmount((String) relCvgFaceAmountList.get(i));	
			}
			if (relCvgUwdeSubCodeList != null && i < relCvgUwdeSubCodeList.size()) {
				relPolicy.setRelCvgUwdeSubcode((String) relCvgUwdeSubCodeList.get(i));
			}
			if (relCvgAdbFaceAmountList != null && i < relCvgAdbFaceAmountList.size()) {
				relPolicy.setRelCvgAdbFaceAmount((String)relCvgAdbFaceAmountList.get(i));	
			}
			if (relCvgAdMultFctList != null && i < relCvgAdMultFctList.size()) {
				relPolicy.setRelCvgAdMultfct((String) relCvgAdMultFctList.get(i));	
			}
			if (relCvgWpMultFctList != null && i < relCvgWpMultFctList.size()) {
				relPolicy.setRelCvgWpMultfct((String) relCvgWpMultFctList.get(i));	
			}
			if (relReinsFaceAmountList != null && i < relReinsFaceAmountList.size()) {
				relPolicy.setRelReinsfaceAmount((String) relReinsFaceAmountList.get(i));
			}
			if (relCvgMeFct != null && i < relCvgMeFct.size()) {
				relPolicy.setRelCvgMeFct((String) relCvgMeFct.get(i));	
			}
			if (relCvgFeUpremAmt != null && i < relCvgFeUpremAmt.size()) {
				relPolicy.setRelCvgFeUpremAmt((String) relCvgFeUpremAmt.get(i));	
			}
			if (relCvgFePermAmt != null && i < relCvgFePermAmt.size()) {
				relPolicy.setRelCvgFePermAmt((String) relCvgFePermAmt.get(i));	
			}
			if (reinstatementDate != null && i < reinstatementDate.size()) {
				relPolicy.setReinstatementDate((String) reinstatementDate.get(i));
			} 
			if (reinsuranceType != null && i < reinsuranceType.size()) {
				relPolicy.setReinsuranceType((String) reinsuranceType.get(i));
			} 
			if (adbFaceAmount != null && i < adbFaceAmount.size()) {
				relPolicy.setAdbFaceAmount((String) adbFaceAmount.get(i));
			}
			
			LOGGER.info("groupRelPolicyInformation end");
			rpi.add(relPolicy);
		}
	}
	
	public String toString(){
		
		return ToStringBuilder.reflectionToString(this, ToStringStyle.DEFAULT_STYLE);
	}

	/**
	 * @void set the rpi.
	 */		
	public void setRelatedPolicyList(List rpi) {
		this.rpi = rpi;
	}
	
	/**
	 * @return Returns the rpi.
	 */		
	public List getRelatedPolicyList() {
		return rpi;
	}	
	
	/**
	 * @return Returns the epi.
	 */
	public ExistingPolicyInformation getExistingPolicyInformation() {
		return epi;
	}

	/**
	 * @return Returns the ii.
	 */
	public InsuredInformation getInsuredInformation() {
		return ii;
	}

	/**
	 * @return Returns the oi.
	 */
	public OwnerInformation getOwnerInformation() {
		return oi;
	}

}

