// Missing Company Details - Andre Ceasar Dacanay
package com.slocpi.ium.ingenium.transaction;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.slocpi.ium.ingenium.transaction.request.Request;
import com.slocpi.ium.ingenium.transaction.response.ClientInquiryResponse;
import com.slocpi.ium.ingenium.transaction.response.Response;

public class ClientInquiryTransaction extends Transaction {

	private static final Logger LOGGER = LoggerFactory.getLogger(ClientInquiryTransaction.class);

	public ClientInquiryTransaction(Request request) {
		super(request);
	}

	public Response execute() {
		LOGGER.info("execute start");
		String response = super.callTXLife();
		ClientInquiryResponse resp = new ClientInquiryResponse();;
		resp.parse(response);
		LOGGER.info("execute end");
		return resp;
	}
}

