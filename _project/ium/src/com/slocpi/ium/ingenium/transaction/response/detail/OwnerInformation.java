package com.slocpi.ium.ingenium.transaction.response.detail;

import java.util.Iterator;
import java.util.List;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jdom.Element;
import com.slocpi.ium.ingenium.transaction.data.Client;
import com.slocpi.ium.ingenium.transaction.data.Mortality;

/*
 * author @nic.decapia
 *
 *
 *
 */
public class OwnerInformation {

	private static final Logger LOGGER = LoggerFactory.getLogger(OwnerInformation.class);
	private static final String clientId = "MirOciOwnCliId";
	private static final String tittle = "MirOciDvOwnTitlTxt";
	private static final String firstname = "MirOciDvOwnGivNm";
	private static final String middlename = "MirOciDvOwnMidNm";
	private static final String surname = "MirOciDvOwnSurNm";
	private static final String suffix = "MirOciDvOwnSfxNm";
	private static final String age = "MirOciDvOwnRtAge";
	private static final String sex = "MirOciDvOwnSexCd";
	private static final String birthdate = "MirOciDvOwnBthDt";
	private static final String birthLocation = "MirOciDvOwnBlocCd";
	private static final String otherFirstname = "MirOciDvOwnOGivNm";
	private static final String otherMiddlename = "MirOciDvOwnOMidNm";
	private static final String otherSurname = "MirOciDvOwnOSurNm";
	private static final String smokerCode = "MirOciDvOwnSmkrCd";

	private static final String heigthInches = "MirOmiDvOwnrHtInch";
	private static final String heigthFeet = "MirOmiDvOwnrHtFt";
	private static final String weigthPounds = "MirOmiDvOwnrWgtLbs";
	private static final String basic = "MirOmiDvOwnrBasMort1";
	private static final String basicFamily = "MirOmiDvOwnrBasMort3";
	private static final String basicSmoking = "MirOmiDvOwnrBasMort2";
	private static final String ccr = "MirOmiDvOwnrCcrMort1";
	private static final String ccrFamily = "MirOmiDvOwnrCcrMort3";
	private static final String ccrSmoking = "MirOmiDvOwnrCcrMort2";
	private static final String apdb = "MirOmiDvOwnrApdMort1";
	private static final String adpbFamily = "MirOmiDvOwnrApdMort3";
	private static final String adpbSmoking = "MirOmiDvOwnrApdMort2";
	
	private Client client = new Client();
	private Mortality mort = new Mortality();
	
	public String toString(){
		
		LOGGER.info("toString start");
		StringBuffer str = new StringBuffer();
		str.append("CLIENT: " + client.getBirthDate())
			.append("\n:").append(client.getSurname())
			.append("\n:").append(client.getGivenName())
			.append("\n:").append(client.getMiddleName())
			.append("\n:").append(client.getSmokerCode())
			.append("\n:").append(client.getAge())
			.append("\n:").append(client.getBirthLocation())
			.append("\n:").append(client.getId())
			.append("\n:").append(client.getSexCode())
			.append("\n:").append(client.getOtherSurname())
			.append("\n:").append(client.getOtherGivenName())
			.append("\n:").append(client.getOtherMiddleName())
			.append("\n:").append(client.getTitle())
			.append("\n:").append(client.getSuffix());
		
		str.append("MORTALITY: " + mort.getHeightInFeet())
			.append("\n:").append(mort.getAPDBFamily())
			.append("\n:").append(mort.getAPDB())
			.append("\n:").append(mort.getCCRFamily())
			.append("\n:").append(mort.getCCR())
			.append("\n:").append(mort.getWeightInPounds())
			.append("\n:").append(mort.getBasicFamily())
			.append("\n:").append(mort.getBasicSmoking())
			.append("\n:").append(mort.getHeightInInches())
			.append("\n:").append(mort.getAPDBSmoking())
			.append("\n:").append(mort.getCCRSmoking())
			.append("\n:").append(mort.getBasic());
		LOGGER.info("toString end");
		return str.toString();
	}

	/**
	 *
	 */
	private void parseOwnerMortality(Element mortality) {
		
		LOGGER.info("parseOwnerMortality start");
		List nodes = mortality.getChildren();
		for (Iterator iter = nodes.iterator(); iter.hasNext();) {
			Element element = (Element) iter.next();
			String name = element.getName();
			String value = element.getText();
			if (name.equalsIgnoreCase(apdb)) {
				mort.setAPDB(value);
			} else if (name.equalsIgnoreCase(ccrSmoking)) {
				mort.setCCRSmoking(value);
			} else if (name.equalsIgnoreCase(basicSmoking)) {
				mort.setBasicSmoking(value);
			} else if (name.equalsIgnoreCase(adpbFamily)) {
				mort.setAPDBFamily(value);
			} else if (name.equalsIgnoreCase(adpbSmoking)) {
				mort.setAPDBSmoking(value);
			} else if (name.equalsIgnoreCase(basicFamily)) {
				mort.setBasicFamily(value);
			} else if (name.equalsIgnoreCase(ccrFamily)) {
				mort.setCCRFamily(value);
			} else if (name.equalsIgnoreCase(heigthFeet)) {
				mort.setHeightInFeet(value);
			} else if (name.equalsIgnoreCase(ccr)) {
				mort.setCCR(value);
			} else if (name.equalsIgnoreCase(basic)) {
				mort.setBasic(value);
			} else if (name.equalsIgnoreCase(weigthPounds)) {
				mort.setWeightInPounds(value);
			} else if (name.equalsIgnoreCase(heigthInches)) {
				mort.setHeightInInches(value);
			}
		}
		LOGGER.info("parseOwnerMortality end");
	}

	/**
	 * Get owner info
	 */
	private void parseOwnerInfo(Element owner) {
		
		LOGGER.info("parseOwnerInfo start");
		List nodes = owner.getChildren();
		for (Iterator iter = nodes.iterator(); iter.hasNext();) {
			Element element = (Element) iter.next();
			String name = element.getName();
			String value = element.getText();
			if (name.equalsIgnoreCase(smokerCode)) {
				client.setSmokerCode(value);
			} else if (name.equalsIgnoreCase(surname)) {
				client.setSurname(value);
			} else if (name.equalsIgnoreCase(firstname)) {
				client.setGivenName(value);
			} else if (name.equalsIgnoreCase(birthdate)) {
				client.setBirthDate(value);
			} else if (name.equalsIgnoreCase(middlename)) {
				client.setMiddleName(value);
			} else if (name.equalsIgnoreCase(sex)) {
				client.setSexCode(value);
			} else if (name.equalsIgnoreCase(age)) {
				client.setAge(value);
			} else if (name.equalsIgnoreCase(birthLocation)) {
				client.setBirthLocation(value);
			} else if (name.equalsIgnoreCase(clientId)) {
				client.setId(value);
			} else if (name.equalsIgnoreCase(otherSurname)) {
				client.setOtherSurname(value);
			} else if (name.equalsIgnoreCase(otherFirstname)) {
				client.setOtherGivenName(value);
			} else if (name.equalsIgnoreCase(otherMiddlename)) {
				client.setOtherMiddleName(value);
			} else if (name.equalsIgnoreCase(tittle)) {
				client.setTitle(value);
			} else if (name.equalsIgnoreCase(suffix)) {
				client.setSuffix(value);
			}
		}
		LOGGER.info("parseOwnerInfo end");
	}

	public void parse(Element owner, Element mortality) {
		parseOwnerInfo(owner);
		parseOwnerMortality(mortality);
	}
	
	public void parseOwner(Element owner) {
		parseOwnerInfo(owner);
	}

	/**
	 * @return Returns the client.
	 */
	public Client getClient() {
		return client;
	}

	/**
	 * @return Returns the mort.
	 */
	public Mortality getMortality() {
		return mort;
	}

	/**
	 * @param mort The mort to set.
	 */
	public void setMortality(Mortality mort) {
		this.mort = mort;
	}

	/**
	 * @param client The client to set.
	 */
	public void setClient(Client client) {
		this.client = client;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	public boolean equals(final Object other) {
		
		LOGGER.info("equals start");
		if (this == other)
			return true;
		if (!(other instanceof OwnerInformation))
			return false;
		OwnerInformation castOther = (OwnerInformation) other;
		LOGGER.info("equals end");
		return new EqualsBuilder().append(client, castOther.client).append(
				mort, castOther.mort).isEquals();
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	public int hashCode() {
		return new HashCodeBuilder().append(client).append(mort).toHashCode();
	}

}

