// Missing Company Details - Andre Ceasar Dacanay
package com.slocpi.ium.ingenium.transaction.response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jdom.Element;

public class ClientInquiryResponse extends Response {

	private static final Logger LOGGER = LoggerFactory.getLogger(ClientInquiryResponse.class);
	private static final String CLIENT_DATA = "ClientData";
	private static final String MIR_CLI_INFO = "MirCliInfo";
	private static final String MIR_DV_CLI_NM = "MirDvCliNm";

	private String clientName = null;;

	public void parse(String content) {
		super.parse(content);
		
		LOGGER.info("parse start");
		
		final Element clientInfoElem = response.getChild(CLIENT_DATA, ns).getChild(MIR_CLI_INFO, ns);
		final Element clientNmElem = clientInfoElem.getChild(MIR_DV_CLI_NM, ns);
		String clientName = clientNmElem.getText();
		LOGGER.debug("clientName "+clientName);
		setClientName(clientName);
		
		LOGGER.info("parse end");
	}

	public String getClientName() {
		return clientName;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}
}

