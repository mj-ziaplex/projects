package com.slocpi.ium.ingenium.transaction.request;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jdom.Element;
import com.slocpi.ium.ingenium.transaction.Configuration;
import com.slocpi.ium.ingenium.transaction.parameter.ClientInquiryParameter;
import com.slocpi.ium.ingenium.transaction.parameter.PolicyTransactionParameter;
import com.slocpi.ium.ingenium.transaction.parameter.TransactionParameter;

/*
 * author @nic.decapia
 *
 *
 *
 */
public class PolicyTransactionRequest extends Request {

	private static final Logger LOGGER = LoggerFactory.getLogger(PolicyTransactionRequest.class);
	private static final String TRANS_LIFE_COMMON_POLID = "MirPolId";
	private static final String TRANS_LIFE_COMMON_POLID_POLICY_NUMBER = "MirPolIdBase";
	private static final String MIR_POL_ID_SFX = "MirPolIdSfx";

	

	public PolicyTransactionRequest(Configuration config) {
		
		LOGGER.info("PolicyTransactionRequest start");
		super.loadTemplate(config.getPolTemplatePath());
		super.setUserAuthorization(config.getUser());
		super.setServiceWSDL(config.getServiceWSDL());
		LOGGER.info("PolicyTransactionRequest end");
	}

	public void setParameter(TransactionParameter tp) {
		
		LOGGER.info("setParameter start");
		String policyNumber = tp.getParameterValue(PolicyTransactionParameter.PARM_POL_NUMBER);
		String policySuffix = tp.getParameterValue(PolicyTransactionParameter.PARM_POL_SUFFIX);		
		Element policyId = common.getChild(TRANS_LIFE_COMMON_POLID, ns);
		Element polNumber = policyId.getChild(TRANS_LIFE_COMMON_POLID_POLICY_NUMBER, ns);
		polNumber.setText(policyNumber);
		Element polSuffix = policyId.getChild(MIR_POL_ID_SFX, ns);
		polSuffix.setText(policySuffix);
		LOGGER.info("setParameter end");
		
	}

	public static Request getPolicyListRequest() {
		
		LOGGER.info("getPolicyListRequest start 1");
		Configuration config = new Configuration();
		Request req = new PolicyTransactionRequest(config);
		req.setTransactionDetails(config.getPolicyListTransactionName(), config.getGUID());
		LOGGER.info("getPolicyListRequest end 1");
		return req;
	}
	
	public static Request getPolicyListRequest(String polNumber, String suffix) {
		
		LOGGER.info("getPolicyListRequest start 2");
		Configuration config = new Configuration();
		Request req = new PolicyTransactionRequest(config);
		req.setTransactionDetails(config.getPolicyListTransactionName(), config.getGUID());
		
		PolicyTransactionParameter ptp = new PolicyTransactionParameter();
		ptp.setParameterValue(PolicyTransactionParameter.PARM_POL_NUMBER, polNumber);
		ptp.setParameterValue(PolicyTransactionParameter.PARM_POL_SUFFIX, suffix);

		req.setParameter(ptp);
		LOGGER.info("getPolicyListRequest end 2");
		return req;
	}

	public static Request getClientDataRequest(String polNumber, String suffix) {
		
		LOGGER.info("getClientDataRequest start");
		Configuration config = new Configuration();
		Request req = new PolicyTransactionRequest(config);
		req.setTransactionDetails(config.getClientDataTransactionName(), config.getGUID());

		PolicyTransactionParameter ptp = new PolicyTransactionParameter();
		ptp.setParameterValue(PolicyTransactionParameter.PARM_POL_NUMBER, polNumber);
		ptp.setParameterValue(PolicyTransactionParameter.PARM_POL_SUFFIX, suffix);

		req.setParameter(ptp);
		LOGGER.info("getClientDataRequest end");
		return req;
	}
	
	// Missing Company Details - Andre Ceasar Dacanay - start
	public static Request getClientInquiryRequest(String clientId) {
		
		LOGGER.info("getClientInquiryRequest start");
		Configuration config = new Configuration();
		Request req = new PolicyTransactionRequest(config);
		req.setTransactionDetails(config.getClientInquiryTransactionName(), config.getGUID());

		ClientInquiryParameter cip = new ClientInquiryParameter();
		cip.setParameterValue(ClientInquiryParameter.PARM_CLIENT_NUMBER, clientId);

		req.setParameter(cip);
		LOGGER.info("getClientInquiryRequest end");
		return req;
	}
	// Missing Company Details - Andre Ceasar Dacanay - end
	

	public static Request getKOReqtRequest(String polNumber, String suffix) {
		
		LOGGER.info("getKOReqtRequest start");
		
		Configuration config = new Configuration();
		Request req = new PolicyTransactionRequest(config);
		req.setTransactionDetails(config.getKickoutTransactionName(), config.getGUID());
		PolicyTransactionParameter ptp = new PolicyTransactionParameter();
		ptp.setParameterValue(PolicyTransactionParameter.PARM_POL_NUMBER, polNumber);
		ptp.setParameterValue(PolicyTransactionParameter.PARM_POL_SUFFIX, suffix);
		req.setParameter(ptp);
		
		LOGGER.info("getKOReqtRequest end");
		return req;
	}

	public static Request getDeletePolicyRequest(String polNumber, String suffix) {
		
		LOGGER.info("getDeletePolicyRequest start");
		Configuration config = new Configuration();
		Request req = new PolicyTransactionRequest(config);
		req.setTransactionDetails(config.getDeletePolicyTransactionName(), config.getGUID());

		PolicyTransactionParameter ptp = new PolicyTransactionParameter();
		ptp.setParameterValue(PolicyTransactionParameter.PARM_POL_NUMBER, polNumber);
		ptp.setParameterValue(PolicyTransactionParameter.PARM_POL_SUFFIX, suffix);

		req.setParameter(ptp);
		LOGGER.info("getDeletePolicyRequest end");
		return req;
	}
}

