// Missing Company Details - Andre Ceasar Dacanay
package com.slocpi.ium.ingenium.transaction.parameter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ClientInquiryParameter extends TransactionParameter {
	private static final Logger LOGGER = LoggerFactory.getLogger(ClientInquiryParameter.class);
	public static final int PARM_CLIENT_NUMBER = 0;

	public ClientInquiryParameter() {
		super();
		LOGGER.info("ClientInquiryParameter start");
		parameterItems.add(PARM_CLIENT_NUMBER, new ParameterItem(PARM_CLIENT_NUMBER));
		LOGGER.info("ClientInquiryParameter end");
	}
}

