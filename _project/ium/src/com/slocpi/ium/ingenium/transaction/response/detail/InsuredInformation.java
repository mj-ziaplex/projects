/**
 *
 */
package com.slocpi.ium.ingenium.transaction.response.detail;

import java.util.Iterator;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jdom.Element;
import com.slocpi.ium.ingenium.transaction.data.Client;
import com.slocpi.ium.ingenium.transaction.data.Mortality;

/*
 * author @nic.decapia
 *
 *
 *
 */
/**
 * @author nic.decapia
 *
 */
public class InsuredInformation {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(InsuredInformation.class);
	private static final String clientId = "MirIciInsrdCliId";
	private static final String tittle = "MirIciDvCliTitlTxt";
	private static final String firstname = "MirIciDvCliGivNm";
	private static final String middlename = "MirIciDvCliMidNm";
	private static final String surname = "MirIciDvCliSurNm";
	private static final String suffix = "MirIciDvCliSfxNm";
	private static final String age = "MirIciDvCliRtAge";
	private static final String sex = "MirIciDvCliSexCd";
	private static final String birthdate = "MirIciDvCliBthDt";
	private static final String birthLocation = "MirIciDvCliBlocCd";
	private static final String otherMiddlename = "MirIciDvOthMidNm";
	private static final String otherFirstname = "MirIciDvOthGivNm";
	private static final String otherSurname = "MirIciDvOthSurNm";
	private static final String smokerCode = "MirIciDvCliSmkrCd";
	
	private static final String heigthFeet = "MirImiDvInsrdHtFt";
	private static final String weigthPounds = "MirImiDvInsrdWgtLbs";
	private static final String heigthInch = "MirImiDvInsrdHtInch";
	private static final String basic = "MirImiDvInsrdBasMort1";
	private static final String basicFamily = "MirImiDvInsrdBasMort3";
	private static final String basicSmoking = "MirImiDvInsrdBasMort2";
	private static final String ccr = "MirImiDvInsrdCcrMort1";
	private static final String ccrFamily = "MirImiDvInsrdCcrMort3";
	private static final String ccrSmoking = "MirImiDvInsrdCcrMort2";
	private static final String apdb = "MirImiDvInsrdApdMort1";
	private static final String apdbfamily = "MirImiDvInsrdApdMort3";
	private static final String apdbSmoking = "MirImiDvInsrdApdMort2";

	private Client client = new Client();
	private Mortality mort = new Mortality();
	
	public String toString(){
		
		LOGGER.info("toString start");
		StringBuffer str = new StringBuffer();
		str.append("CLIENT: " + client.getBirthDate())
			.append("\n:").append(client.getSurname())
			.append("\n:").append(client.getGivenName())
			.append("\n:").append(client.getMiddleName())
			.append("\n:").append(client.getSmokerCode())
			.append("\n:").append(client.getAge())
			.append("\n:").append(client.getBirthLocation())
			.append("\n:").append(client.getId())
			.append("\n:").append(client.getSexCode())
			.append("\n:").append(client.getOtherSurname())
			.append("\n:").append(client.getOtherGivenName())
			.append("\n:").append(client.getOtherMiddleName())
			.append("\n:").append(client.getTitle())
			.append("\n:").append(client.getSuffix());
		
		str.append("MORTALITY: " + mort.getHeightInFeet())
			.append("\n:").append(mort.getAPDBFamily())
			.append("\n:").append(mort.getAPDB())
			.append("\n:").append(mort.getCCRFamily())
			.append("\n:").append(mort.getCCR())
			.append("\n:").append(mort.getWeightInPounds())
			.append("\n:").append(mort.getBasicFamily())
			.append("\n:").append(mort.getBasicSmoking())
			.append("\n:").append(mort.getHeightInInches())
			.append("\n:").append(mort.getAPDBSmoking())
			.append("\n:").append(mort.getCCRSmoking())
			.append("\n:").append(mort.getBasic());
		LOGGER.info("toString end");
		return str.toString();
	}

	/**
	 * @return Returns the client.
	 */
	public Client getClient() {
		return client;
	}

	/**
	 * @return Returns the mort.
	 */
	public Mortality getMortality() {
		return mort;
	}

	public void parse(Element insured, Element mortality) {
		parseMortality(mortality);
		parseInfo(insured);
	}
	
	public void parseInsured(Element insured) {
		parseInfo(insured);
	}

	/**
	 *
	 */
	private void parseMortality(Element mortality) {
		
		LOGGER.info("parseMortality start");
		List nodes = mortality.getChildren();
		for (Iterator iter = nodes.iterator(); iter.hasNext();) {
			Element element = (Element) iter.next();
			String name = element.getName();
			String value = element.getText();
			if (name.equalsIgnoreCase(heigthFeet)) {
				mort.setHeightInFeet(value);
			} else if (name.equalsIgnoreCase(apdbfamily)) {
				mort.setAPDBFamily(value);
			} else if (name.equalsIgnoreCase(apdb)) {
				mort.setAPDB(value);
			} else if (name.equalsIgnoreCase(ccrFamily)) {
				mort.setCCRFamily(value);
			} else if (name.equalsIgnoreCase(ccr)) {
				mort.setCCR(value);
			} else if (name.equalsIgnoreCase(weigthPounds)) {
				mort.setWeightInPounds(value);
			} else if (name.equalsIgnoreCase(basicFamily)) {
				mort.setBasicFamily(value);
			} else if (name.equalsIgnoreCase(basicSmoking)) {
				mort.setBasicSmoking(value);
			} else if (name.equalsIgnoreCase(heigthInch)) {
				mort.setHeightInInches(value);
			} else if (name.equalsIgnoreCase(apdbSmoking)) {
				mort.setAPDBSmoking(value);
			} else if (name.equalsIgnoreCase(ccrSmoking)) {
				mort.setCCRSmoking(value);
			} else if (name.equalsIgnoreCase(basic)) {
				mort.setBasic(value);
			}
		}
		LOGGER.info("parseMortality end");
	}

	/**
	 *
	 */
	private void parseInfo(Element insured) {
		
		LOGGER.info("parseInfo start");
		List nodes = insured.getChildren();
		for (Iterator iter = nodes.iterator(); iter.hasNext();) {
			Element element = (Element) iter.next();
			String name = element.getName();
			String value = element.getText();
			if (name.equalsIgnoreCase(birthdate)) {
				client.setBirthDate(value);
			} else if (name.equalsIgnoreCase(middlename)) {
				client.setMiddleName(value);
			} else if (name.equalsIgnoreCase(age)) {
				client.setAge(value);
			} else if (name.equalsIgnoreCase(surname)) {
				client.setSurname(value);
			} else if (name.equalsIgnoreCase(birthLocation)) {
				client.setBirthLocation(value);
			} else if (name.equalsIgnoreCase(smokerCode)) {
				client.setSmokerCode(value);
			} else if (name.equalsIgnoreCase(sex)) {
				client.setSexCode(value);
			} else if (name.equalsIgnoreCase(firstname)) {
				client.setGivenName(value);
			} else if (name.equalsIgnoreCase(clientId)) {
				client.setId(value);
			} else if (name.equalsIgnoreCase(otherSurname)) {
				client.setOtherSurname(value);
			} else if (name.equalsIgnoreCase(otherFirstname)) {
				client.setOtherGivenName(value);
			} else if (name.equalsIgnoreCase(otherMiddlename)) {
				client.setOtherMiddleName(value);
			} else if (name.equalsIgnoreCase(tittle)) {
				client.setTitle(value);
			} else if (name.equalsIgnoreCase(suffix)) {
				client.setSuffix(value);
			}
		}
		LOGGER.info("parseInfo end");
	}

	public void setClient(Client cli) {
		client = cli;

	}

	public void setMortality(Mortality mortality) {
		mort = mortality;
	}

}

