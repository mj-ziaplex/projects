/*
 * Created on Feb 21, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.slocpi.ium.data.dao.ExceptionDAO;
import com.slocpi.ium.data.util.DataSourceProxy;
import com.slocpi.ium.util.CodeHelper;

/**
 * @author ypojol
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class ExceptionLogger {
	Logger LOGGER = LoggerFactory.getLogger(ExceptionLogger.class.getName());
	
	public void logError(ArrayList arr){
		
		LOGGER.info("logError start 1");
		ExceptionDAO dao = new ExceptionDAO();
		try {
			dao.logError(arr);
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
		}
		LOGGER.info("logError end 1");
	}
	
	public void logError(ExceptionLog ex){
		
		LOGGER.info("logError start 2");
		DataSourceProxy dataSourceProxy = new DataSourceProxy();
		Connection conn = dataSourceProxy.getConnection();
		ExceptionDAO dao = new ExceptionDAO();
		try {
			dao.logError(ex);
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
		}finally{
			closeConnection(conn);
		}
		LOGGER.info("logError end 2");
	}
	
	private void closeConnection(Connection conn){
		
		
		if (null != conn){
			try {
				conn.close();
			} catch (SQLException sqlE) {
				LOGGER.error(CodeHelper.getStackTrace(sqlE));
			}
		}
		
	}
}
