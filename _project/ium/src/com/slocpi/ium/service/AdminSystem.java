package com.slocpi.ium.service;

import java.util.ArrayList;
import java.util.HashMap;
import com.slocpi.ium.data.AssessmentRequestData;
import com.slocpi.ium.data.PolicyRequirementsData;
import com.slocpi.ium.interfaces.IUMInterfaceException;

/*
 * author @nic.decapia
 *
 *
 *
 */

public interface AdminSystem {

	/**
	 * This method is responsible for connecting to MQ-Abacus and retrieval of all the policies for review and assessment
	 * @throws IUMInterfaceException
	 */
	public abstract void processGetPolicyQueue() throws IUMInterfaceException;

	/**
	 * This method is responsible for retrieving the policy details from Abacus
	 *
	 * @param refNum policy reference number
	 * @throws IUMInterfaceException
	 */
	public abstract void processPolicyDetails(String refNum)
			throws IUMInterfaceException;

	/**
	 * This method is responsible for the auto-settlement of an assessment request
	 * @param ard Assessment Request Data
	 * @throws IUMInterfaceException
	 */
	public abstract void processAutoSettleAssessmentRequest(
			AssessmentRequestData ard) throws IUMInterfaceException;

	/**
	 * This method is responsible for the creation of requirements in Abacus
	 * @param prd - policy requirements data
	 * @throws IUMInterfaceException
	 */
	public abstract void processCreateRequirement(PolicyRequirementsData prd)
			throws IUMInterfaceException;

	/**
	 * This method is responsible for updating the requirement in Abacus
	 * @param list
	 * @throws IUMInterfaceException
	 */
	public abstract void processReceiveRequirement(ArrayList list)
			throws IUMInterfaceException;

	/**
	 * updates Abacus of changes in the requirements in IUM
	 * @param list ArrayList of policy requirements
	 * @throws IUMInterfaceException
	 */

	public abstract void processAcceptRequirement(ArrayList list)
			throws IUMInterfaceException;

	/**
	 * This method is responsible for updating the requirements in Abacus with the changes done in IUM
	 * @param list ArrayList of policy requirements
	 * @throws IUMInterfaceException
	 */
	public abstract void processOrderRequirement(ArrayList list)
			throws IUMInterfaceException;

	/**
	 * Updates Abacus as to which requirements were rejected
	 * @param list ArrayList of policy requirements
	 * @throws IUMInterfaceException
	 */

	public abstract void processRejectRequirement(ArrayList list)
			throws IUMInterfaceException;

	/**
	 * This method updates Abacus as to which requirements has been cancelled in IUM
	 * @param list ArrayList of policy requirements
	 * @throws IUMInterfaceException
	 */
	public abstract void processCancelRequirement(ArrayList list)
			throws IUMInterfaceException;

	/**
	 * Updates Abacus as to which requirements has been waived
	 * @param list ArrayList of policy requirements
	 * @throws IUMInterfaceException
	 */
	public abstract void processWaiveRequirement(ArrayList list)
			throws IUMInterfaceException;

	/**
	 * Updates Abacus with the requirement changes done in IUM
	 * @param prd policy requirement
	 * @throws IUMInterfaceException
	 */
	public abstract void processUpdateRequirement(PolicyRequirementsData prd)
			throws IUMInterfaceException;

	public abstract boolean retrieveCDSDetails(String referenceNumber, String suffix)
			throws IUMInterfaceException;
	
	//Update Agent Details from Ingenium to IUM - Andre Ceasar Dacanay - start
	public abstract HashMap retrieveIngeniumConsolidatedInformation(String referenceNumber, String suffix)
	throws IUMInterfaceException;
	//Update Agent Details from Ingenium to IUM - Andre Ceasar Dacanay - end
	
}	
