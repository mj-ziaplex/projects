/*
 * Created on Feb 28, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.service.threads;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.TimerTask;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.abacus.MessageConnector;
import com.slocpi.ium.ExceptionDetail;
import com.slocpi.ium.ExceptionLog;
import com.slocpi.ium.ExceptionLogger;
import com.slocpi.ium.IUMException;
import com.slocpi.ium.data.AssessmentRequestData;
import com.slocpi.ium.data.CDSMortalityRatingData;
import com.slocpi.ium.data.CDSSummaryPolicyCoverageData;
import com.slocpi.ium.data.ClientData;
import com.slocpi.ium.data.ClientDataSheetData;
import com.slocpi.ium.data.KickOutMessageData;
import com.slocpi.ium.data.LOBData;
import com.slocpi.ium.data.PolicyDetailData;
import com.slocpi.ium.data.PolicyRequirementsData;
import com.slocpi.ium.data.RequirementData;
import com.slocpi.ium.data.StatusData;
import com.slocpi.ium.data.dao.AssessmentRequestDAO;
import com.slocpi.ium.data.dao.ClientDAO;
import com.slocpi.ium.data.dao.RequirementDAO;
import com.slocpi.ium.data.dao.StatusCHDao;
import com.slocpi.ium.data.util.DataSourceProxy;
import com.slocpi.ium.interfaces.messages.IUMMessageUtility;
import com.slocpi.ium.interfaces.messages.MessageConverter;
import com.slocpi.ium.underwriter.AssessmentRequest;
import com.slocpi.ium.underwriter.PolicyRequirements;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.IUMConstants;
import com.sunlife.mq.SLMQInitException;

/**
 * @author ypojol
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class ProcessPolicyDetailsThread extends TimerTask {
	private static final Logger LOGGER = LoggerFactory.getLogger(ProcessPolicyDetailsThread.class);
	private String	iumAbacusUserId = "";
	private String	refNum = "";
	/**
	 * 
	 */
	public ProcessPolicyDetailsThread(String _userID, String _refNo) {
		super();
		iumAbacusUserId = _userID;
		refNum = _refNo;
	}

	/* (non-Javadoc)
	 * @see java.lang.Runnable#run()
	 */
	public void run() {
		
		LOGGER.info("run start");
		String trans = "";
		MessageConnector con = null;
		try {
			AssessmentRequestData ard = null;
			AssessmentRequest ar = new AssessmentRequest();
			MessageConverter mc = new MessageConverter();
		
		    con = getConnector(mc.TRANSACTION_KICK_OUT); 
			trans = mc.TRANSACTION_KICK_OUT;
			con.connect();
		
			String message = mc.generateKickOutMessage(iumAbacusUserId,IUMMessageUtility.generateIUMMQkey(),refNum);
		
			StringBuffer reply = new StringBuffer().append(con.putRequestMsg(message));
				
			ArrayList exceptionLogs = new ArrayList();
			ExceptionLogger errorLogger = new ExceptionLogger();
			boolean noExceptions = true;
			ArrayList kos = null;
			if(reply==null){
				ExceptionLog exLog = new ExceptionLog();
				exLog.setRecordType(mc.TRANSACTION_KICK_OUT);
				/*exLog.setReply(reply.toString());*/
				exLog.setSystemInterface(IUMConstants.SYSTEM_ABACUS_DESC);
				exLog.setRequestMsg(message);
				ArrayList arr = new ArrayList();
				arr.add(createExceptionDetail("MQ: No reply message available"));
				exLog.setDetails(arr);
				noExceptions = false;
				exceptionLogs.add(exLog);
			}else{
					
				
				kos = mc.translateKickOutMessage(reply);
				
				for (int i =0; i<kos.size(); i++){
					KickOutMessageData koData = (KickOutMessageData) kos.get(i);
					ExceptionLog exLog = validateKOMessages(koData);
						
					if (exLog != null){
						exLog.setRecordType(mc.TRANSACTION_KICK_OUT);
						exLog.setReferenceNumber(refNum);
						exLog.setReply(reply.toString());
						exLog.setSystemInterface(IUMConstants.SYSTEM_ABACUS_DESC);
						exLog.setRequestMsg(message);
						exLog.setXmlRecord(koData.getXmlRecord());
						exceptionLogs.add(exLog);
						noExceptions = false;
					}
				}
				
			}
			
			if (exceptionLogs.size()>0){
				errorLogger.logError(exceptionLogs);
			}
				
			
			con = getConnector(mc.TRANSACTION_CLIENT_DATA_SHEET);
			trans = mc.TRANSACTION_CLIENT_DATA_SHEET;
			con.connect();
			message = mc.generateClientRelatedInfoMessage(iumAbacusUserId,IUMMessageUtility.generateIUMMQkey(),refNum);
			reply = new StringBuffer().append(con.putRequestMsg(message));
				
			if(reply==null){
				ExceptionLog exLog = new ExceptionLog();
				exLog.setRecordType(mc.TRANSACTION_CLIENT_DATA_SHEET);
				/*exLog.setReply(reply.toString());*/
				exLog.setSystemInterface(IUMConstants.SYSTEM_ABACUS_DESC);
				exLog.setRequestMsg(message);
				ArrayList arr = new ArrayList();
				arr.add(createExceptionDetail("MQ: No reply message available"));
				exLog.setDetails(arr);
				noExceptions = false;
				exceptionLogs.add(exLog);
			}else{
				ard = mc.translateClientRelatedInfoMessage(reply);
				ClientData ownerData = ard.getOwner();
				ClientData insuredData = ard.getInsured();
					
				ExceptionLog ownerDataException = validateClientInfo(ownerData);
				if (ownerDataException != null){
					ownerDataException.setRecordType(mc.TRANSACTION_CLIENT_DATA_SHEET);
					ownerDataException.setReferenceNumber(refNum);
					ownerDataException.setReply(reply.toString());
					ownerDataException.setSystemInterface(IUMConstants.SYSTEM_ABACUS_DESC);
					ownerDataException.setRequestMsg(message);
					ownerDataException.setXmlRecord(ard.getXmlRecord());
	
					errorLogger.logError(ownerDataException);
					noExceptions = false; 
				}
					
				ExceptionLog insuredDataException = validateClientInfo(insuredData);
				if (insuredDataException != null){
					insuredDataException.setRecordType(mc.TRANSACTION_CLIENT_DATA_SHEET);
					insuredDataException.setReferenceNumber(refNum);
					insuredDataException.setReply(reply.toString());
					insuredDataException.setSystemInterface(IUMConstants.SYSTEM_ABACUS_DESC);
					insuredDataException.setRequestMsg(message);
					insuredDataException.setXmlRecord(ard.getXmlRecord());
						
					errorLogger.logError(insuredDataException);
					noExceptions = false; 
				}  
					
				CDSSummaryPolicyCoverageData policyCoverage = ard.getClientDataSheet().getSummaryOfPolicies();
				ExceptionLog policyCoverageException = validateSummaryPolicyCoverage(policyCoverage);
					
				if (policyCoverageException != null){
					policyCoverageException.setRecordType(mc.TRANSACTION_CLIENT_DATA_SHEET);
					policyCoverageException.setReferenceNumber(refNum);
					policyCoverageException.setReply(reply.toString());
					policyCoverageException.setSystemInterface(IUMConstants.SYSTEM_ABACUS_DESC);
					policyCoverageException.setRequestMsg(message);
					policyCoverageException.setXmlRecord(ard.getXmlRecord());
						
					errorLogger.logError(policyCoverageException);
					noExceptions = false;
						
				}
					
				CDSMortalityRatingData ownerMortality = ard.getOwnerMortalityRating();
				CDSMortalityRatingData insuredMortality = ard.getInsuredMortalityRating();
					
				ExceptionLog oMortalityException = validateMortalityRating(ownerMortality);
				if (oMortalityException != null){
					oMortalityException.setRecordType(mc.TRANSACTION_CLIENT_DATA_SHEET);
					oMortalityException.setReferenceNumber(refNum);
					oMortalityException.setReply(reply.toString());
					oMortalityException.setSystemInterface(IUMConstants.SYSTEM_ABACUS_DESC);
					oMortalityException.setRequestMsg(message);
					oMortalityException.setXmlRecord(ard.getXmlRecord());
											
					errorLogger.logError(oMortalityException);
					noExceptions = false;
				}
					
				ExceptionLog iMortalityException = validateMortalityRating(insuredMortality);
				if (iMortalityException != null){
					iMortalityException.setRecordType(mc.TRANSACTION_CLIENT_DATA_SHEET);
					iMortalityException.setReferenceNumber(refNum);
					iMortalityException.setReply(reply.toString());
					iMortalityException.setSystemInterface(IUMConstants.SYSTEM_ABACUS_DESC);
					iMortalityException.setRequestMsg(message);
					iMortalityException.setXmlRecord(ard.getXmlRecord());
						
					errorLogger.logError(iMortalityException);
					noExceptions = false;
				} 
	
				ard.setKickOutMessages(kos);
			}
				
		
			con = getConnector(mc.TRANSACTION_RELATED_POLICY_AND_COVERAGE);
			trans = mc.TRANSACTION_RELATED_POLICY_AND_COVERAGE;
			con.connect();
			message = mc.generateRelatedPolicyAndCoverageMessage(iumAbacusUserId,IUMMessageUtility.generateIUMMQkey(),refNum);
			reply = new StringBuffer().append(con.putRequestMsg(message));
		
				
			if(reply==null){
				ExceptionLog exLog = new ExceptionLog();
				exLog.setRecordType(mc.TRANSACTION_RELATED_POLICY_AND_COVERAGE);
				/*exLog.setReply(reply.toString());*/
				exLog.setSystemInterface(IUMConstants.SYSTEM_ABACUS_DESC);
				exLog.setRequestMsg(message);
				ArrayList arr = new ArrayList();
				arr.add(createExceptionDetail("MQ: No reply message available"));
				exLog.setDetails(arr);
				noExceptions = false;
				exceptionLogs.add(exLog);
			}else{
					
				ArrayList relatedPolicies = mc.translateRelatedPolicyAndCoverageInfoMessage(reply);
				ArrayList policyDetailExceptions = new ArrayList();
					
				for (int i=0; i<relatedPolicies.size(); i++){
					PolicyDetailData policyDetail = (PolicyDetailData) relatedPolicies.get(i);
					ExceptionLog polDetailException = validatePolicyCoverageDetails(policyDetail);
						
					if (polDetailException  != null){
						polDetailException.setRecordType(mc.TRANSACTION_RELATED_POLICY_AND_COVERAGE);
						polDetailException.setReferenceNumber(refNum);
						polDetailException.setReply(reply.toString());
						polDetailException.setSystemInterface(IUMConstants.SYSTEM_ABACUS_DESC);
						polDetailException.setRequestMsg(message);
						polDetailException.setXmlRecord(policyDetail.getXmlRecord());
							
						policyDetailExceptions.add(polDetailException);
						noExceptions = false;
					} 
				}
					
				
				if (policyDetailExceptions.size() > 0){
					errorLogger.logError(policyDetailExceptions);
				}
					
				
				ClientDataSheetData cds = new ClientDataSheetData();
				cds = ard.getClientDataSheet();
				cds.setPolicyCoverageDetailsList(relatedPolicies);
					
				 
				LOBData lob = new LOBData();
				lob.setLOBCode(IUMConstants.LOB_INDIVIDUAL_LIFE);
				ard.setLob(lob);
			}
				
		
			con = getConnector(mc.TRANSACTION_REQUIREMENT_INQUIRY);
			trans = mc.TRANSACTION_REQUIREMENT_INQUIRY;
			con.connect();
			message = mc.generateRetrieveRequirementsMessage(iumAbacusUserId,IUMMessageUtility.generateIUMMQkey(),refNum);
			reply = new StringBuffer().append(con.putRequestMsg(message));
				
			ArrayList requirements = null;
			if(reply==null){
				ExceptionLog exLog = new ExceptionLog();
				exLog.setRecordType(mc.TRANSACTION_REQUIREMENT_INQUIRY);
				/*exLog.setReply(reply.toString());*/
				exLog.setSystemInterface(IUMConstants.SYSTEM_ABACUS_DESC);
				exLog.setRequestMsg(message);
				ArrayList arr = new ArrayList();
				arr.add(createExceptionDetail("MQ: No reply message available"));
				exLog.setDetails(arr);
				noExceptions = false;
				
				exceptionLogs.add(exLog);
			}else{
			
				requirements = mc.translatePolicyRequirementInquiryMessage(reply);
				ArrayList requirementExceptions = new ArrayList();
					
				for (int i=0; i<requirements.size(); i++){
					PolicyRequirementsData reqData = (PolicyRequirementsData) requirements.get(i);
					ExceptionLog reqException = validatePolicyRequiremetns(reqData);
						
					if (reqException != null){
						reqException.setRecordType(mc.TRANSACTION_REQUIREMENT_INQUIRY);
						reqException.setReferenceNumber(refNum);
						reqException.setReply(reply.toString());
						reqException.setSystemInterface(IUMConstants.SYSTEM_ABACUS_DESC);
						reqException.setRequestMsg(message);
						reqException.setXmlRecord(reqData.getXmlRecord());
							
						errorLogger.logError(reqException);
						noExceptions = false;
					}
				}
			}
				
			if (noExceptions){
				ar.transmitMQDataToIUMDB(ard);
				if ((requirements != null) && (requirements.size() > 0)) {
					PolicyRequirements pr = new PolicyRequirements();
					for (int i=0; i < requirements.size(); i++) {
						PolicyRequirementsData prd = (PolicyRequirementsData)requirements.get(i);
						
						long requirementId = pr.createRequirementIUM(prd,ard);
						if (prd.getStatus().getStatusId() == IUMConstants.STATUS_ORDERED){
							pr.saveAttachmentData(ard.getReferenceNumber(), requirementId);
						}
					}
				}
				ar.setSuccessfulRetrieval(ard.getReferenceNumber(),IUMConstants.INDICATOR_SUCCESS);
				
				con = getConnector(mc.TRANSACTION_CLIENT_DATA_SHEET);
				trans = mc.TRANSACTION_CLIENT_DATA_SHEET;
				con.connect();
				String deleteMsg1 = mc.generateClientRelatedInfoDeleteMessage(iumAbacusUserId,IUMMessageUtility.generateIUMMQkey(),refNum);
				StringBuffer delteReply1 = new StringBuffer().append(con.putRequestMsg(deleteMsg1));
					
				con = getConnector(mc.TRANSACTION_RELATED_POLICY_AND_COVERAGE);
				trans = mc.TRANSACTION_RELATED_POLICY_AND_COVERAGE;
				con.connect();
				String deleteMsg2 = mc.generateRelatedPolicyAndCoverageDeleteMessage(iumAbacusUserId,IUMMessageUtility.generateIUMMQkey(),refNum);
				StringBuffer delteReply2 = new StringBuffer().append(con.putRequestMsg(deleteMsg2));
				
			}
		} catch (Exception e) {
			logError(e, trans);
			LOGGER.error(CodeHelper.getStackTrace(e));
		} finally {
			if (null != con) con.disconnect();
		}
		
		this.cancel();
		LOGGER.info("run end");
	}

	private ExceptionLog validatePolicyCoverageDetails(PolicyDetailData policyData){
		
		LOGGER.info("validatePolicyCoverageDetails start");
		ExceptionLog exLog = null;
		ArrayList details = new ArrayList();
		AssessmentRequest ar = new AssessmentRequest();
		if (policyData != null){
			if (policyData.getPolicyNumber() == null || policyData.getPolicyNumber().trim().equals("")){
				details.add(createExceptionDetail("Policy number is null or empty"));
			}
			boolean policyExist = false;
			try {
				policyExist = ar.isAssessmentRequestExist(policyData.getReferenceNumber());
				if (!policyExist){
					details.add(createExceptionDetail("Non-existing reference number"));
				}
			} catch (IUMException e) {
				details.add(createExceptionDetail(e.getMessage()));
			}
			if (policyData.getFaceAmount() < 0){
				details.add(createExceptionDetail("Invalid value for Face Amount"));
			}
			if (policyData.getCoverageNumber() < 0){
				details.add(createExceptionDetail("Invalid value for Coverage Number"));
			}
			if (policyData.getADBFaceAmount() < 0){
				details.add(createExceptionDetail("Invalid value for ADB Face Amount"));
			}
			if (policyData.getADMultiplier() < 0){
				details.add(createExceptionDetail("Invalid value for AD Multiplier"));
			}
			if (policyData.getWPMultiplier() < 0){
				details.add(createExceptionDetail("Invalid value for WP Multiplier"));
			}
			if (policyData.getReinsuredAmount() < 0){
				details.add(createExceptionDetail("Invalid value for Reinsured Amount"));
			}
		}
		
		if (details.size() > 0){
			exLog = new ExceptionLog();
			exLog.setDetails(details);
		}
		LOGGER.info("validatePolicyCoverageDetails end");
		return exLog;
	}
	
	
	private ExceptionLog validateClientInfo(ClientData clData){
		
		LOGGER.info("validateClientInfo start");
		ExceptionLog exLog = null;
		ArrayList details = new ArrayList();
		if(clData != null){
			if (clData.getAge() < 0){
				details.add(createExceptionDetail("Invalid value for age"));
			}
		}
		
		if (details.size()>0){
			exLog = new ExceptionLog();
			exLog.setDetails(details);
		}
		LOGGER.info("validateClientInfo end");
		return exLog;
	}
	
	
	private ExceptionLog validateSummaryPolicyCoverage(CDSSummaryPolicyCoverageData cdsSummary){
		
		LOGGER.info("validateSummaryPolicyCoverage start");
		ExceptionLog exLog = null;
		ArrayList details = new ArrayList();
		if (cdsSummary != null){
			if (cdsSummary.getTotalReinsuredAmount() < 0){
				details.add(createExceptionDetail("Invalid value for total reinsured amount"));
			}
			if (cdsSummary.getTotalFBBMBCoverage() < 0){
				details.add(createExceptionDetail("Invalid value for total FBBMB coverage"));
			}
			if (cdsSummary.getTotalHIBCoverage() < 0){
				details.add(createExceptionDetail("Invalid value for total HIB coverage"));
			}
			if (cdsSummary.getTotalAPDBCoverage() < 0){
				details.add(createExceptionDetail("Invalid value for total APDB coverage"));
			}
			if (cdsSummary.getTotalCCRCoverage() < 0){
				details.add(createExceptionDetail("Invalid value for total CCR coverage"));
			}
			if (cdsSummary.getOtherCompaniesAd() < 0){
				details.add(createExceptionDetail("Invalid value for Other Companies Ad"));
			}
			if (cdsSummary.getOtherCompaniesBr() < 0){
				details.add(createExceptionDetail("Invalid value for Other Companies Br"));
			}
			if (cdsSummary.getExistingInForceAd() < 0){
				details.add(createExceptionDetail("Invalid value for Existing In Force Ad"));
			}
			if (cdsSummary.getExistingInForceBr() < 0){
				details.add(createExceptionDetail("Invalid value for Existing In Force Br"));
			}
			if (cdsSummary.getExistingLapsedAd() < 0){
				details.add(createExceptionDetail("Invalid value for Existing Lapsed Ad"));
			}
			if (cdsSummary.getExistingLapsedBr() < 0){
				details.add(createExceptionDetail("Invalid value for Existing Lapsed Br"));
			}
			if (cdsSummary.getPendingAmountAd() < 0){
				details.add(createExceptionDetail("Invalid value for Pending Amount Ad"));
			}
			if (cdsSummary.getPendingAmountBr() < 0){
				details.add(createExceptionDetail("Invalid value for Pending Amount Br"));
			}
			if (cdsSummary.getAppliedForAd() < 0){
				details.add(createExceptionDetail("Invalid value for Pending Amount Ad"));
			}
			if (cdsSummary.getAppliedForBr() < 0){
				details.add(createExceptionDetail("Invalid value for Pending Amount Br"));
			}
		}
		
		if (details.size() > 0){
			exLog = new ExceptionLog();
			exLog.setDetails(details);
		}
		LOGGER.info("validateSummaryPolicyCoverage end");
		return exLog;
	}
	
	private ExceptionLog validatePolicyRequiremetns(PolicyRequirementsData reqData){
		
		LOGGER.info("validatePolicyRequiremetns start");
		ExceptionLog exLog = null;
		ArrayList details = new ArrayList();
		
		AssessmentRequestDAO dao = new AssessmentRequestDAO();
		RequirementDAO reqdao = new RequirementDAO();
		
		if (reqData != null){
			if(reqData.getLevel().equalsIgnoreCase(IUMConstants.LEVEL_POLICY)){
				if (reqData.getReferenceNumber() == null || reqData.getReferenceNumber().trim().equals("")){
					details.add(createExceptionDetail("Reference number is null or empty"));
				} else {
					boolean policyExist = false;
					try {
						policyExist = dao.isPolicyExists(reqData.getReferenceNumber());
						if (!policyExist){
							details.add(createExceptionDetail("Non-existing reference number"));
						}
					} catch (SQLException e) {
						details.add(createExceptionDetail(e.getMessage()));
						LOGGER.error(CodeHelper.getStackTrace(e));
					}
				
				}
			}else{
				if (reqData.getClientType() == null){
					details.add(createExceptionDetail("Client Type is null or empty"));
				} else {
					if (reqData.getClientType() == IUMConstants.CLIENT_TYPE_INSURED){
						if (reqData.getClientId() != null && !reqData.getClientId().trim().equals("")){
							ClientDAO clDao = new ClientDAO();
							ClientData clData = null;
							try {
								clData = clDao.retrieveClient(reqData.getClientId());
								if (clData == null){
									details.add(createExceptionDetail("Non-existing client"));
								}
							} catch (SQLException e) {
								details.add(createExceptionDetail(e.getMessage()));
								LOGGER.error(CodeHelper.getStackTrace(e));
							} 
						} 
					} 
				}
			}

			if (reqData.getRequirementCode() != null && !reqData.getRequirementCode().trim().equals("")){
				RequirementData requirement = null;
				try {
					requirement = reqdao.retrieveRequirement(reqData.getRequirementCode());
					if (requirement==null){
						details.add(createExceptionDetail("Non-existing requirement code"));
					}
				} catch (SQLException e) {
					details.add(createExceptionDetail(e.getMessage()));
					LOGGER.error(CodeHelper.getStackTrace(e));
				}
			}
			
			StatusData status = reqData.getStatus();
			if (status != null){
				if (status.getStatusId() != 0){
					StatusCHDao statDao = new StatusCHDao();
					boolean isStatusExist = false;
					try {
						isStatusExist = statDao.isStatusExist(reqData.getStatus().getStatusId(), IUMConstants.STAT_TYPE_NM);
						if (!isStatusExist){
							details.add(createExceptionDetail("Non-existing status"));
						}
					} catch (SQLException e) {
						details.add(createExceptionDetail(e.getMessage()));
						LOGGER.error(CodeHelper.getStackTrace(e));
					} 
				}
			} else {
				details.add(createExceptionDetail("Status is null or empty"));
			}
			if (reqData.getSequenceNumber() < 0){
				details.add(createExceptionDetail("Invalid value for sequence number"));
			} 
		}
	
		if (details.size() > 0){
			exLog = new ExceptionLog();
			exLog.setDetails(details);
		}
		
		LOGGER.info("validatePolicyRequiremetns end");
		return exLog;
		
	}
	
	private ExceptionLog validateMortalityRating(CDSMortalityRatingData mortalityData){
		
		LOGGER.info("validateMortalityRating start");
		ExceptionLog exLog = null;
		ArrayList details = new ArrayList();
		
		if (mortalityData != null){
			if (mortalityData.getHeightInFeet() < 0 || mortalityData.getHeightInInches() < 0){
				details.add(createExceptionDetail("Invalid value for height"));
			}
			if (mortalityData.getWeight() < 0){
				details.add(createExceptionDetail("Invalid value for weight"));
			}
			if (mortalityData.getAPDB() < 0){
				details.add(createExceptionDetail("Invalid value for APDB"));
			}
			if (mortalityData.getCCR() < 0){
				details.add(createExceptionDetail("Invalid value for CCR"));
			}
			if (mortalityData.getBasic() < 0){
				details.add(createExceptionDetail("Invalid value for Basic"));
			}
		}
		
		if (details.size() > 0){
			exLog = new ExceptionLog();
			exLog.setDetails(details);
		}
		LOGGER.info("validateMortalityRating end");
		return exLog;
	}
	
	private ExceptionLog validateKOMessages(KickOutMessageData koData){
		
		LOGGER.info("validateKOMessages start");
		ExceptionLog exLog = null;
		ArrayList details = new ArrayList();
		
		AssessmentRequestDAO dao = new AssessmentRequestDAO();
		if (koData != null){
			if (koData.getSequenceNumber()==0){
				details.add(createExceptionDetail("Invalid sequence number"));
			}
			if (koData.getReferenceNumber() == null || koData.getReferenceNumber().trim().equals("")){
				details.add(createExceptionDetail("Reference number is null or empty."));
			} else {
				boolean existingRequest = false;
				try {
					existingRequest = dao.isPolicyExists(koData.getReferenceNumber());
				} catch (SQLException e) {
					details.add(createExceptionDetail(e.getMessage()));
					LOGGER.error(CodeHelper.getStackTrace(e));
				}
				if (!existingRequest){
					details.add(createExceptionDetail("Non-existing reference number"));
				}
			}
		}
		
		if (details.size()>0){
			exLog = new ExceptionLog();
			exLog.setDetails(details);
		}
		
		LOGGER.info("validateKOMessages end");
		return exLog;
	}
	
	private MessageConnector getConnector(String transactionId) throws SLMQInitException {
		return new MessageConnector(transactionId);
	}
	
	private void logError(Exception e, String trans) {

		LOGGER.info("logError start");
		ExceptionLog el= new ExceptionLog();
		MessageConverter mc = new MessageConverter();
		el.setRecordType(trans);
		el.setSystemInterface(IUMConstants.SYSTEM_ABACUS_DESC);
		ArrayList arr = new ArrayList(); 
		arr.add(createExceptionDetail(e.getMessage()));
		el.setDetails(arr);
		ExceptionLogger errorLogger = new ExceptionLogger();
		errorLogger.logError(el);
		LOGGER.info("logError send");
	}
	
	private ExceptionDetail createExceptionDetail(String message){
		
		LOGGER.info("createExceptionDetail start");
		ExceptionDetail ed = new ExceptionDetail();
		ed.setMessage(message);
		LOGGER.info("createExceptionDetail end");
		return ed;
	}
			
	private void closeConnection(Connection _con){
		
		
		if(_con != null){
			try {
				_con.close();
			} catch (SQLException e) {
				LOGGER.error(CodeHelper.getStackTrace(e));
			}
		}
		
	}
	
}
