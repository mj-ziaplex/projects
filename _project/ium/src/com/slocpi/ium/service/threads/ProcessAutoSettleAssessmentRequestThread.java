/*
 * Created on Feb 28, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.service.threads;

import java.util.ArrayList;
import java.util.TimerTask;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.abacus.MessageConnector;
import com.slocpi.ium.ExceptionDetail;
import com.slocpi.ium.ExceptionLog;
import com.slocpi.ium.ExceptionLogger;
import com.slocpi.ium.data.AssessmentRequestData;
import com.slocpi.ium.data.IUMMessageData;
import com.slocpi.ium.interfaces.messages.IUMMessageUtility;
import com.slocpi.ium.interfaces.messages.MessageConverter;
import com.slocpi.ium.underwriter.AssessmentRequest;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.IUMConstants;
import com.sunlife.mq.SLMQInitException;

/**
 * @author ypojol
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class ProcessAutoSettleAssessmentRequestThread extends TimerTask {
	private static final Logger LOGGER = LoggerFactory.getLogger(ProcessAutoSettleAssessmentRequestThread.class);
	private String	iumAbacusUserId;
	private AssessmentRequestData ard;
	/**
	 *
	 */
	public ProcessAutoSettleAssessmentRequestThread(String userID, AssessmentRequestData _ard) {
		super();
		iumAbacusUserId = userID;
		ard = _ard;
	}

	/* (non-Javadoc)
	 * @see java.lang.Runnable#run()
	 */
	public void run() {
		
		LOGGER.info("run start");
		MessageConverter mc = new MessageConverter();
		MessageConnector con = null; 
		try {
		
			con = getConnector(mc.TRANSACTION_AUTO_SETTLE);
			con.connect();
			String msgSCCP = mc.generateAutoSettleSCCPMessage(iumAbacusUserId,IUMMessageUtility.generateIUMMQkey(), ard.getReferenceNumber(), ard.getAssignedTo().getACF2ID(), ard.getUnderwriter().getACF2ID());
			StringBuffer replySCCP = new StringBuffer().append(con.putRequestMsg(msgSCCP));
			if(replySCCP==null){
				ExceptionLog el= new ExceptionLog();
				el.setRecordType(mc.TRANSACTION_AUTO_SETTLE);
				el.setRequestMsg(msgSCCP);
				el.setSystemInterface(IUMConstants.SYSTEM_ABACUS_DESC);
				ArrayList arr = new ArrayList();
				arr.add(createExceptionDetail("MQ: No reply message available"));
				el.setDetails(arr);
				ExceptionLogger errorLogger = new ExceptionLogger();
				errorLogger.logError(el);
			}else{
				IUMMessageData imdS = mc.translateAutoSettleMessage(replySCCP);
				ExceptionLog exLogS = validateAutoSettleAR(imdS);
				if(exLogS==null){
					AssessmentRequest ar =  new AssessmentRequest();
					ar.setAutoSettleIndicator(ard.getReferenceNumber(), IUMConstants.INDICATOR_SUCCESS);
					String msgUWDE = mc.generateAutoSettleUWDEMessage(iumAbacusUserId,IUMMessageUtility.generateIUMMQkey(), ard.getReferenceNumber(), ard.getAssignedTo().getACF2ID(), ard.getUnderwriter().getACF2ID());
					StringBuffer replyUWDE = new StringBuffer().append(con.putRequestMsg(msgUWDE));
					if(replyUWDE==null){
						ExceptionLog el= new ExceptionLog();
						el.setRecordType(mc.TRANSACTION_AUTO_SETTLE);
						el.setRequestMsg(msgUWDE);
						el.setSystemInterface(IUMConstants.SYSTEM_ABACUS_DESC);
						ArrayList arr = new ArrayList();
						arr.add(createExceptionDetail("MQ: No reply message available"));
						el.setDetails(arr);
						ExceptionLogger errorLogger = new ExceptionLogger();
						errorLogger.logError(el);
					}else{
						IUMMessageData imdU = mc.translateAutoSettleMessage(replyUWDE);
						ExceptionLog exLogU = validateAutoSettleAR(imdU);
						if(exLogU!=null){
							//log error
							exLogU.setRecordType(mc.TRANSACTION_AUTO_SETTLE);
							exLogU.setReply(replyUWDE.toString());
							exLogU.setSystemInterface(IUMConstants.SYSTEM_ABACUS_DESC);
							exLogU.setRequestMsg(msgUWDE);
							exLogU.setReferenceNumber(ard.getReferenceNumber());
						/*	exLogS.setXmlRecord(imdU.getXmlRecord());*/
							ExceptionLogger errorLogger = new ExceptionLogger();
							errorLogger.logError(exLogU);
						}else{
							//TODO what will we do if uwde is successful?
						}
					}
				}else{
					
					exLogS.setRecordType(mc.TRANSACTION_AUTO_SETTLE);
					exLogS.setReply(replySCCP.toString());
					exLogS.setSystemInterface(IUMConstants.SYSTEM_ABACUS_DESC);
					exLogS.setRequestMsg(msgSCCP);
					exLogS.setReferenceNumber(ard.getReferenceNumber());
					exLogS.setXmlRecord(imdS.getXmlRecord());
					ExceptionLogger errorLogger = new ExceptionLogger();
					errorLogger.logError(exLogS);
				}
			}
		} catch (Exception e) {
			logError(e, mc.TRANSACTION_AUTO_SETTLE);
			LOGGER.error(CodeHelper.getStackTrace(e));
		} finally {
			if (null != con) con.disconnect();
		}
		this.cancel();
		LOGGER.info("run end");
	}

	private MessageConnector getConnector(String transactionId) throws SLMQInitException {
		return new MessageConnector(transactionId);
	}

	private ExceptionDetail createExceptionDetail(String message){
		LOGGER.info("createExceptionDetail start");
		ExceptionDetail ed = new ExceptionDetail();
		ed.setMessage(message);
		LOGGER.info("createExceptionDetail end");
		return ed;
	}

	private ExceptionLog validateAutoSettleAR(IUMMessageData imd){
		
			LOGGER.info("validateAutoSettleAR start");
			ExceptionLog exLog = null;
			ArrayList details = new ArrayList();
			if(imd!=null && imd.getConfirmationCode()!=null){
				if(imd.getConfirmationCode().trim().equals("00")){
				}else if(imd.getConfirmationCode().trim().equals("01")){
					details.add(createExceptionDetail("Record not found"));
				}else if(imd.getConfirmationCode().trim().equals("02")){
					details.add(createExceptionDetail("Error in NB contact"));
				}else if(imd.getConfirmationCode().trim().equals("03")){
					details.add(createExceptionDetail("Policy status not equal to PCCU"));
				}else if(imd.getConfirmationCode().trim().equals("04")){
					 details.add(createExceptionDetail("Unsuccessful SCCP"));
				}else if(imd.getConfirmationCode().trim().equals("05")){
					 details.add(createExceptionDetail("Unsuccessful UWDE"));
				}else if(imd.getConfirmationCode().trim().equals("06")){
					 details.add(createExceptionDetail("Invalid transaction"));
				}else{
					details.add(createExceptionDetail("Invalid confirmation code"));
				}
				if(details.size()>0){
					exLog = new ExceptionLog();
					exLog.setDetails(details);
				}
			}
			LOGGER.info("validateAutoSettleAR end");
			return exLog;
	}

	private void logError(Exception e, String trans) {
		
		LOGGER.info("logError start");
		ExceptionLog el= new ExceptionLog();
		MessageConverter mc = new MessageConverter();
		el.setRecordType(trans);
		el.setSystemInterface(IUMConstants.SYSTEM_ABACUS_DESC);
		ArrayList arr = new ArrayList();
		arr.add(createExceptionDetail(e.getMessage()));
		el.setDetails(arr);
		ExceptionLogger errorLogger = new ExceptionLogger();
		errorLogger.logError(el);
		LOGGER.info("logError end");
	}

}
