/*
 * Created on Feb 28, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.service.threads;

import java.util.ArrayList;
import java.util.TimerTask;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.abacus.MessageConnector;
import com.slocpi.ium.ExceptionDetail;
import com.slocpi.ium.ExceptionLog;
import com.slocpi.ium.ExceptionLogger;
import com.slocpi.ium.data.IUMMessageData;
import com.slocpi.ium.data.PolicyRequirementsData;
import com.slocpi.ium.interfaces.messages.IUMMessageUtility;
import com.slocpi.ium.interfaces.messages.MessageConverter;
import com.slocpi.ium.underwriter.PolicyRequirements;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.IUMConstants;
import com.sunlife.mq.SLMQInitException;

/**
 * @author ypojol
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class ProcessAcceptRequirementThread extends TimerTask {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ProcessAcceptRequirementThread.class);
	private String	iumAbacusUserId = "";
	private ArrayList list;
	/**
	 * 
	 */
	public ProcessAcceptRequirementThread(String userID, ArrayList al) {
		super();
		iumAbacusUserId = userID;
		list = al;
	}

	/* (non-Javadoc)
	 * @see java.lang.Runnable#run()
	 */
	public void run() {
		
		LOGGER.info("run start");
		MessageConverter mc = new MessageConverter();
		MessageConnector con = null; 
		try {
				
			con = getConnector(mc.TRANSACTION_REQUIREMENT_MAINTENANCE);
			con.connect();
			if ((list != null) && (list.size() > 0)) {
				for (int i=0; i <list.size(); i++) {
					PolicyRequirementsData prd = (PolicyRequirementsData)list.get(i);
					String message = mc.generateAcceptRequirementMessage(iumAbacusUserId,IUMMessageUtility.generateIUMMQkey(),prd);
					StringBuffer reply = new StringBuffer().append(con.putRequestMsg(message));
					
					if(reply==null){
						ExceptionLog el= new ExceptionLog();
						el.setRecordType(mc.TRANSACTION_REQUIREMENT_MAINTENANCE);
						el.setSystemInterface(IUMConstants.SYSTEM_ABACUS_DESC);
						el.setRequestMsg(message);
						el.setReferenceNumber(prd.getReferenceNumber());
						ArrayList arr = new ArrayList(); 
						arr.add(createExceptionDetail("MQ: No reply message available"));
						el.setDetails(arr);
						ExceptionLogger errorLogger = new ExceptionLogger();
						errorLogger.logError(el);
					}else{
						IUMMessageData imd = mc.translatePolicyRequirementMaintenanceMessage(reply); 
						ExceptionLog exLog = validateIUMMessageData(imd);
						if(exLog==null){
						 
							prd.setABACUScreateDate(imd.getCreationDate());
							prd.setSequenceNumber(imd.getSequenceNumber());
							PolicyRequirements pr = new PolicyRequirements();
							pr.maintainRequirement(prd);
							    
						}else{
							exLog.setRecordType(mc.TRANSACTION_REQUIREMENT_MAINTENANCE);
							exLog.setRequestMsg(message);
							exLog.setReply(reply.toString());
							exLog.setXmlRecord(reply.toString());
							exLog.setSystemInterface(IUMConstants.SYSTEM_ABACUS_DESC);
							ExceptionLogger errorLogger = new ExceptionLogger();
							errorLogger.logError(exLog);
						}
					}									
					
				}
			}
		} catch (Exception e) {
			logError(e, mc.TRANSACTION_REQUIREMENT_MAINTENANCE);
			LOGGER.error(CodeHelper.getStackTrace(e));
		} finally {
			if(con != null){
				con.disconnect();	
			}
		}
		this.cancel();
		LOGGER.info("run end");
	}

	private ExceptionLog validateIUMMessageData(IUMMessageData imd){
		
		LOGGER.info("validateIUMMessageData start");
		ExceptionLog exLog = null;
		ArrayList details = new ArrayList();
		if(imd.getConfirmationCode()==null || imd.getConfirmationCode().trim().equals("")){
			details.add(createExceptionDetail("Invalid confirmation code"));
		}
		if(imd.getPolicyClientCode()==null || imd.getPolicyClientCode().trim().equals("")){
			details.add(createExceptionDetail("Invalid policy client code"));
		}
		if(imd.getPolicyClientId()==null || imd.getPolicyClientId().trim().equals("")){
			details.add(createExceptionDetail("Invalid policy client id"));
		}
		if(imd.getRequirementId()==null || imd.getRequirementId().trim().equals("")){
			details.add(createExceptionDetail("Invalid requirment id"));
		}
		if(imd.getSequenceNumber()<0){
			details.add(createExceptionDetail("Invalid sequence number"));
		}
		
		if (details.size()>0){
			exLog = new ExceptionLog();
			exLog.setDetails(details);
		}
		LOGGER.info("validateIUMMessageData end");
		return exLog;
	}
	
	private ExceptionDetail createExceptionDetail(String message){
		
		LOGGER.info("createExceptionDetail start");
		ExceptionDetail ed = new ExceptionDetail();
		ed.setMessage(message);
		LOGGER.info("createExceptionDetail end");
		return ed;
	}
	
	private void logError(Exception e, String trans) {
			
		LOGGER.info("logError start");
		ExceptionLog el= new ExceptionLog();
		MessageConverter mc = new MessageConverter();
		el.setRecordType(trans);
		el.setSystemInterface(IUMConstants.SYSTEM_ABACUS_DESC);
		ArrayList arr = new ArrayList(); 
		arr.add(createExceptionDetail(e.getMessage()));
		el.setDetails(arr);
		ExceptionLogger errorLogger = new ExceptionLogger();
		errorLogger.logError(el);
		LOGGER.info("logError end");
	}
	
	private MessageConnector getConnector(String transactionId) throws SLMQInitException {
		return new MessageConnector(transactionId);
	}
		
	

}
