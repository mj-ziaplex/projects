/*
 * Created on Feb 28, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.service.threads;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.TimerTask;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.abacus.MessageConnector;
import com.slocpi.ium.ExceptionDetail;
import com.slocpi.ium.ExceptionLog;
import com.slocpi.ium.ExceptionLogger;
import com.slocpi.ium.data.AssessmentRequestData;
import com.slocpi.ium.data.LOBData;
import com.slocpi.ium.data.StatusData;
import com.slocpi.ium.data.SunLifeOfficeData;
import com.slocpi.ium.data.UserProfileData;
import com.slocpi.ium.interfaces.messages.IUMMessageUtility;
import com.slocpi.ium.interfaces.messages.MessageConverter;
import com.slocpi.ium.underwriter.AssessmentRequest;
import com.slocpi.ium.underwriter.UnderWriterException;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.IUMConstants;
import com.sunlife.mq.SLMQInitException;

/**
 * @author ypojol
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class ProcessGetPolicyQueueThread extends TimerTask {
	private static final Logger LOGGER = LoggerFactory.getLogger(ProcessGetPolicyQueueThread.class);
	private String	iumAbacusUserId = "";
	/**
	 * 
	 */
	public ProcessGetPolicyQueueThread(String userID) {
		super();
		iumAbacusUserId = userID;
	}

	/* (non-Javadoc)
	 * @see java.lang.Runnable#run()
	 */
	public void run() {
		
		LOGGER.info("run start");
		MessageConverter mc = new MessageConverter();
		MessageConnector con = null; //Fix for CR#10146
		try {
			ArrayList list = new ArrayList();
			con = getConnector(mc.TRANSACTION_POLICY_QUEUE);
			con.connect();
			String message = mc.generatePolicyQueueMessage(iumAbacusUserId,IUMMessageUtility.generateIUMMQkey());
			StringBuffer reply = new StringBuffer().append(con.putRequestMsg(message));
			if(reply==null){
				ExceptionLog exLog = new ExceptionLog();
				exLog.setRecordType(mc.TRANSACTION_POLICY_QUEUE);
				/*exLog.setReply(reply.toString());*/
				exLog.setSystemInterface(IUMConstants.SYSTEM_ABACUS_DESC);
				exLog.setRequestMsg(message);
				ArrayList arr = new ArrayList();
				arr.add(createExceptionDetail("MQ: No reply message available"));
				exLog.setDetails(arr);
				ExceptionLogger errorLogger = new ExceptionLogger();
				errorLogger.logError(exLog);
			}else{
				
				list = mc.translatePolicyQueMessage(reply);
				if ((list != null) && (list.size() > 0)) {
					AssessmentRequest ar = new AssessmentRequest();
					for (int i=0; i <list.size(); i++) {
						AssessmentRequestData ard = (AssessmentRequestData)list.get(i);
						setAbacusSpecificDetails(ard);
						ExceptionLog exLog = validateARD(ard);
						if(exLog!=null){
							//log error
							exLog.setRecordType(mc.TRANSACTION_POLICY_QUEUE);
							exLog.setReply(reply.toString());
							exLog.setSystemInterface(IUMConstants.SYSTEM_ABACUS_DESC);
							exLog.setRequestMsg(message);
							exLog.setReferenceNumber(ard.getReferenceNumber());
							exLog.setXmlRecord(ard.getXmlRecord());
							ExceptionLogger errorLogger = new ExceptionLogger();
							errorLogger.logError(exLog);
						}else{
							try {
								ar.createAssessmentRequest(ard);					
							} catch (UnderWriterException uwe) {
								exLog = new ExceptionLog();
								exLog.setRecordType(mc.TRANSACTION_POLICY_QUEUE);
								exLog.setReply(reply.toString());
								exLog.setSystemInterface(IUMConstants.SYSTEM_ABACUS_DESC);
								exLog.setRequestMsg(message);
								exLog.setReferenceNumber(ard.getReferenceNumber());
								exLog.setXmlRecord(ard.getXmlRecord());
								ArrayList arr = new ArrayList();
								arr.add(createExceptionDetail(uwe.getMessage()));
								exLog.setDetails(arr);
								ExceptionLogger errorLogger = new ExceptionLogger();
								errorLogger.logError(exLog);
							}
						}
					}
				}
			}
		} catch (Exception e) {
			logError(e, mc.TRANSACTION_POLICY_QUEUE);
			LOGGER.error(CodeHelper.getStackTrace(e));
		} finally {
			if (null != con) con.disconnect();
		}
		this.cancel();
		LOGGER.info("run end");
	}


	private void setAbacusSpecificDetails(AssessmentRequestData ard) {
		
		LOGGER.info("setAbacusSpecificDetails start");
		StatusData sd = new StatusData();
		sd.setStatusId(IUMConstants.STATUS_NB_REVIEW_ACTION);
		LOBData lob = new LOBData();
		lob.setLOBCode(IUMConstants.LOB_INDIVIDUAL_LIFE);
		
		ard.setStatus(sd);
		ard.setLob(lob);
		ard.setSourceSystem(IUMConstants.SYSTEM_ABACUS);
		ard.setStatusDate(new Date());
		ard.setCreatedDate(new Date());
		
		UserProfileData upd = new UserProfileData();
		upd.setUserId(IUMConstants.USER_IUM);
		ard.setCreatedBy(upd);
		
		ard.setFolderLocation(ard.getAssignedTo());
		LOGGER.info("setAbacusSpecificDetails end");
	}
	
	private ExceptionLog validateARD(AssessmentRequestData ard){
		
		LOGGER.info("validateARD start");
		ExceptionLog exLog = null;
		ArrayList details = new ArrayList();
		CodeHelper ch = new CodeHelper();
		if(ard.getReferenceNumber()==null || ard.getReferenceNumber().trim().equals("")){
			details.add(createExceptionDetail("Invalid reference number"));
		}
		if(ard.getAmountCovered()<0){
			details.add(createExceptionDetail("Invalid amount covered"));
		}
		if(ard.getPremium()<0){
			details.add(createExceptionDetail("Invalid premium amount"));
		}
		if(ard.getApplicationReceivedDate()==null){
			details.add(createExceptionDetail("Invalid application received date"));
		}
		
		try{
			if (ard.getAssignedTo() != null){
				UserProfileData at = ch.getUserProfileByACF2ID(ard.getAssignedTo().getACF2ID());
				if(at==null){
					details.add(createExceptionDetail("Invalid value for NB_CNTCT_USER_ID"));
				}else{
					ard.setAssignedTo(at);
					ard.setFolderLocation(at);
				}
			}
		}catch(Exception e){
				details.add(createExceptionDetail(e.getMessage()));
				LOGGER.error(CodeHelper.getStackTrace(e));
		}

		try{
			UserProfileData uw = ch.getUserProfileByACF2ID(ard.getUnderwriter().getACF2ID());
			if(uw==null){
				//details.add(createExceptionDetail("Underwriter not found in IUM"));
			}else{
				ard.setUnderwriter(uw);
			}
		}catch(SQLException e){
			//details.add(createExceptionDetail(e.getMessage()));
			LOGGER.error(CodeHelper.getStackTrace(e));
		}
	
		
		try{
			UserProfileData agt = ch.getUserProfile(ard.getAgent().getUserId());
			if(agt==null){
				details.add(createExceptionDetail("Agent not found in IUM"));
			}else{
				ard.setAgent(agt);
			}
		}catch(SQLException e){
			details.add(createExceptionDetail(e.getMessage()));
			LOGGER.error(CodeHelper.getStackTrace(e));
		}
		
		
		try{
			SunLifeOfficeData sod = ch.getSunLifeOffice(ard.getBranch().getOfficeId());
			if(sod==null){
				details.add(createExceptionDetail("SunLife office not defined in IUM"));
			}else{
				ard.setBranch(sod);
			}
		}catch(SQLException e){
			details.add(createExceptionDetail(e.getMessage()));
			LOGGER.error(CodeHelper.getStackTrace(e));
		}		
		if (details.size()>0){
			exLog = new ExceptionLog();
			exLog.setDetails(details);
		}
		LOGGER.info("validateARD end");
		return exLog;
	}
	
	private void logError(Exception e, String trans) {
		
		LOGGER.info("logError start");
		ExceptionLog el= new ExceptionLog();
		MessageConverter mc = new MessageConverter();
		el.setRecordType(trans);
		el.setSystemInterface(IUMConstants.SYSTEM_ABACUS_DESC);
		ArrayList arr = new ArrayList(); 
		arr.add(createExceptionDetail(e.getMessage()));
		el.setDetails(arr);
		ExceptionLogger errorLogger = new ExceptionLogger();
		errorLogger.logError(el);
		LOGGER.info("logError end");
	}
	
	private ExceptionDetail createExceptionDetail(String message){
		LOGGER.info("createExceptionDetail start");
		ExceptionDetail ed = new ExceptionDetail();
		ed.setMessage(message);
		LOGGER.info("createExceptionDetail end");
		return ed;
	}
	
	private MessageConnector getConnector(String transactionId) throws SLMQInitException {
		return new MessageConnector(transactionId);
	}

}
