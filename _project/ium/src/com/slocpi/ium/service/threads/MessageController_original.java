/*
 * Created on Jan 21, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.service.threads;

import java.util.ArrayList;
import java.util.Date;
import java.util.ResourceBundle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.abacus.MessageConnector;
import com.slocpi.ium.data.AssessmentRequestData;
import com.slocpi.ium.data.ClientDataSheetData;
import com.slocpi.ium.data.IUMMessageData;
import com.slocpi.ium.data.LOBData;
import com.slocpi.ium.data.PolicyRequirementsData;
import com.slocpi.ium.data.StatusData;
import com.slocpi.ium.data.UserProfileData;
import com.slocpi.ium.interfaces.messages.IUMMessageUtility;
import com.slocpi.ium.interfaces.messages.MessageConverter;
import com.slocpi.ium.underwriter.AssessmentRequest;
import com.slocpi.ium.underwriter.PolicyRequirements;
import com.slocpi.ium.underwriter.UnderWriterException;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.IUMConstants;
import com.sunlife.mq.SLMQInitException;

/**
 * @author daguila
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class MessageController_original {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(MessageController_original.class);
	private String	iumAbacusUserId = "";
	
	public MessageController_original() {
		LOGGER.info("constructor start");
		ResourceBundle rb = ResourceBundle.getBundle(IUMConstants.IUM_CONFIG);
		iumAbacusUserId = rb.getString(IUMConstants.ABACUS_USER);
		LOGGER.debug("Abacus userID: " + iumAbacusUserId);
		LOGGER.info("constructor end");
	}
	
	public void processGetPolicyQueue() {
		
		LOGGER.info("processGetPolicyQueue start");
		try {
					
			ArrayList list = new ArrayList();
			MessageConverter mc = new MessageConverter();
			MessageConnector con = getConnector(mc.TRANSACTION_POLICY_QUEUE);
			con.connect();
			String message = mc.generatePolicyQueueMessage(iumAbacusUserId,IUMMessageUtility.generateIUMMQkey());
			StringBuffer reply = new StringBuffer().append(con.putRequestMsg(message));
			list = mc.translatePolicyQueMessage(reply);
			
			if ((list != null) && (list.size() > 0)) {
				
				AssessmentRequest ar = new AssessmentRequest();
				
				for (int i=0; i <list.size(); i++) {
					AssessmentRequestData ard = (AssessmentRequestData)list.get(i);
					setAbacusSpecificDetails(ard);
					try {
						ar.createAssessmentRequest(ard);					
					} catch (UnderWriterException uwe) {
						LOGGER.error(CodeHelper.getStackTrace(uwe));
					}
					
				}
			}
			
			
		} catch (Exception e) {
			LOGGER.info("processGetPolicyQueue start");
		}
		LOGGER.info("processGetPolicyQueue end");
	}
	
	/**
	 * 
	 * @param list
	 */
	public void processReceiveRequirement(ArrayList list) {
		
		LOGGER.info("processReceiveRequirement start");
		try {
			MessageConverter mc = new MessageConverter();
			MessageConnector con = getConnector(mc.TRANSACTION_REQUIREMENT_MAINTENANCE);
			MessageConverter mt = new MessageConverter();
			con.connect();
			if ((list != null) && (list.size() > 0)) {
				for (int i=0; i <list.size(); i++) {
					PolicyRequirementsData prd = (PolicyRequirementsData)list.get(i);
					
					try {
						String message = mc.generateReceiveRequirementMessage(iumAbacusUserId,IUMMessageUtility.generateIUMMQkey(),prd);
						StringBuffer reply = new StringBuffer().append(con.putRequestMsg(message));
						IUMMessageData imd = mt.translatePolicyRequirementMaintenanceMessage(reply); 
					} catch (Exception e) {
						LOGGER.error(" Exception at Accept Requirement ");
						LOGGER.error(CodeHelper.getStackTrace(e));					
					}
				}
			}
			con.disconnect();
		} catch (Exception e) {
			LOGGER.error("Exception at Receive Requirement");	
			LOGGER.error(CodeHelper.getStackTrace(e));
		}
		LOGGER.info("processReceiveRequirement end");
	}
	
	/**
	 * 
	 * @param list
	 */
	public void processAcceptRequirement(ArrayList list) {
		
		LOGGER.info("processAcceptRequirement start");
		try {
			MessageConverter mc = new MessageConverter();
			
			MessageConnector con = getConnector(mc.TRANSACTION_REQUIREMENT_MAINTENANCE);
			con.connect();
			if ((list != null) && (list.size() > 0)) {
				for (int i=0; i <list.size(); i++) {
					PolicyRequirementsData prd = (PolicyRequirementsData)list.get(i);
					try {
						String message = mc.generateAcceptRequirementMessage(iumAbacusUserId,IUMMessageUtility.generateIUMMQkey(),prd);
						StringBuffer reply = new StringBuffer().append(con.putRequestMsg(message));
				
					} catch (Exception e) {
						LOGGER.error("Exception at Accept Requirement");
						LOGGER.error(CodeHelper.getStackTrace(e));					
					}
				}
			}
			con.disconnect();
		} catch (Exception e) {
			LOGGER.error("Exception at Accept Requirement");
			LOGGER.error(CodeHelper.getStackTrace(e));
		}
		LOGGER.info("processAcceptRequirement end");
	}
	
	/**
	 * 
	 * @param refNum
	 */
	public void processPolicyDetails(String refNum) {
		
		LOGGER.info("processPolicyDetails start");
		try {
			AssessmentRequestData ard = null;
			AssessmentRequest ar = new AssessmentRequest();
			MessageConverter mc = new MessageConverter();
			
			MessageConnector con = getConnector(mc.TRANSACTION_KICK_OUT);
			con.connect();
			
			String message = mc.generateKickOutMessage(iumAbacusUserId,IUMMessageUtility.generateIUMMQkey(),refNum);
			
			StringBuffer reply = new StringBuffer().append(con.putRequestMsg(message));
			con.disconnect();
			ArrayList kos = mc.translateKickOutMessage(reply);
			con = getConnector(mc.TRANSACTION_CLIENT_DATA_SHEET);
			con.connect();
			
			message = mc.generateClientRelatedInfoMessage(iumAbacusUserId,IUMMessageUtility.generateIUMMQkey(),refNum);
			reply = new StringBuffer().append(con.putRequestMsg(message));
			con.disconnect();
			
			ard = mc.translateClientRelatedInfoMessage(reply);
			ard.setKickOutMessages(kos);
			
			con = getConnector(mc.TRANSACTION_RELATED_POLICY_AND_COVERAGE);
			con.connect();
			message = mc.generateRelatedPolicyAndCoverageMessage(iumAbacusUserId,IUMMessageUtility.generateIUMMQkey(),refNum);
			
			reply = new StringBuffer().append(con.putRequestMsg(message));
			con.disconnect();
			
			ArrayList relatedPolicies = mc.translateRelatedPolicyAndCoverageInfoMessage(reply);
			
			ClientDataSheetData cds = new ClientDataSheetData();
			cds = ard.getClientDataSheet();
			cds.setPolicyCoverageDetailsList(relatedPolicies);
			
			
			LOBData lob = new LOBData();
			lob.setLOBCode(IUMConstants.LOB_INDIVIDUAL_LIFE);
			ard.setLob(lob);
			
			ar.transmitMQDataToIUMDB(ard);
			
			con = getConnector(mc.TRANSACTION_REQUIREMENT_INQUIRY);
			con.connect();
			 
			message = mc.generateRetrieveRequirementsMessage(iumAbacusUserId,IUMMessageUtility.generateIUMMQkey(),refNum);
			reply = new StringBuffer().append(con.putRequestMsg(message));
			ArrayList requirements = mc.translatePolicyRequirementInquiryMessage(reply);
			
			if ((requirements != null) && (requirements.size() > 0)) {
				PolicyRequirements pr = new PolicyRequirements();
				for (int i=0; i < requirements.size(); i++) {
					PolicyRequirementsData prd = (PolicyRequirementsData)requirements.get(i);
					pr.createRequirementIUM(prd,ard);
				}
			}
			
		} catch (Exception e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
		}
		LOGGER.info("processPolicyDetails end");
	}
	
	public void processCreateRequirement(PolicyRequirementsData prd) {
		
		LOGGER.info("processCreateRequirement start");
		try {
			MessageConverter mc = new MessageConverter();
			MessageConnector con = getConnector(mc.TRANSACTION_REQUIREMENT_MAINTENANCE);
			con.connect();
			String message = mc.generateCreateRequirementMessage(iumAbacusUserId,IUMMessageUtility.generateIUMMQkey(),prd);
			StringBuffer reply = new StringBuffer().append(con.putRequestMsg(message));
			
			 IUMMessageData imd =  mc.translatePolicyRequirementMaintenanceMessage(reply);
			 prd.setABACUScreateDate(imd.getCreationDate());
			 prd.setSequenceNumber(imd.getSequenceNumber());
			 
			PolicyRequirements pr = new PolicyRequirements();
			con.disconnect();
		} catch (Exception e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
		}
		LOGGER.info("processCreateRequirement end");
	}
	
	public void processAutoSettleAssessmentRequest(String refNum) {
		
		LOGGER.info("processAutoSettleAssessmentRequest start");
		try {
			MessageConverter mc = new MessageConverter();
			MessageConnector con = getConnector(mc.TRANSACTION_AUTO_SETTLE);
		} catch (Exception e) {
			LOGGER.error("Exception at Auto Settle");
			LOGGER.error(CodeHelper.getStackTrace(e));
		}
		LOGGER.info("processAutoSettleAssessmentRequest end");
	}
	
	public void processOrderRequirement(ArrayList list) {
		
		LOGGER.info("processOrderRequirement start");
		try {
			MessageConverter mc = new MessageConverter();
			MessageConnector con = getConnector(mc.TRANSACTION_REQUIREMENT_MAINTENANCE);
			con.connect();
			
			if ((list != null) && (list.size() > 0)) {
				for (int i=0; i <list.size(); i++) {
					PolicyRequirementsData prd = (PolicyRequirementsData)list.get(i);
					String message = mc.generateOrderRequirementMessage(iumAbacusUserId,IUMMessageUtility.generateIUMMQkey(),prd);
					StringBuffer reply = new StringBuffer().append(con.putRequestMsg(message));
					IUMMessageData imd = mc.translatePolicyRequirementMaintenanceMessage(reply);
				}
			}
			con.disconnect();
		} catch (Exception e) {
			LOGGER.error("Exception at Order Requirement");
			LOGGER.error(CodeHelper.getStackTrace(e));
		}
		LOGGER.info("processOrderRequirement end");
	}
	

	public void processRejectRequirement(ArrayList list) {
		
		LOGGER.info("processRejectRequirement start");
		try {
			MessageConverter mc = new MessageConverter();
			MessageConnector con = getConnector(mc.TRANSACTION_REQUIREMENT_MAINTENANCE);
			con.connect();
			
			if ((list != null) && (list.size() > 0)) {
				for (int i=0; i <list.size(); i++) {
					PolicyRequirementsData prd = (PolicyRequirementsData)list.get(i);
					String message = mc.generateUpdateRequirementMessage(iumAbacusUserId,IUMMessageUtility.generateIUMMQkey(),prd);
					StringBuffer reply = new StringBuffer().append(con.putRequestMsg(message));
					IUMMessageData imd = mc.translatePolicyRequirementMaintenanceMessage(reply);				
				}
			}
			con.disconnect();
		} catch (Exception e) {
			LOGGER.error("Exception at Reject Requirement");
			LOGGER.error(CodeHelper.getStackTrace(e));
		}		
		LOGGER.info("processRejectRequirement start");
	}

	public void processCancelRequirement(ArrayList list) {
		
		LOGGER.info("processCancelRequirement start");
		try {
			MessageConverter mc = new MessageConverter();
			MessageConnector con = getConnector(mc.TRANSACTION_REQUIREMENT_MAINTENANCE);
			con.connect();
			
			if ((list != null) && (list.size() > 0)) {
				for (int i=0; i <list.size(); i++) {
					PolicyRequirementsData prd = (PolicyRequirementsData)list.get(i);
					String message = mc.generateUpdateRequirementMessage(iumAbacusUserId,IUMMessageUtility.generateIUMMQkey(),prd);
					StringBuffer reply = new StringBuffer().append(con.putRequestMsg(message));
					IUMMessageData imd = mc.translatePolicyRequirementMaintenanceMessage(reply);				
				}
			}
			con.disconnect();
		} catch (Exception e) {
			LOGGER.error("Exception at Cancel Requirement");
			LOGGER.error(CodeHelper.getStackTrace(e));
		}		
		LOGGER.info("processCancelRequirement end");
	}

	public void processWaiveRequirement(ArrayList list) {
		
		LOGGER.info("processWaiveRequirement start");
		try {
			MessageConverter mc = new MessageConverter();
			MessageConnector con = getConnector(mc.TRANSACTION_REQUIREMENT_MAINTENANCE);
			con.connect();
			
			if ((list != null) && (list.size() > 0)) {
				for (int i=0; i <list.size(); i++) {
					PolicyRequirementsData prd = (PolicyRequirementsData)list.get(i);
					String message = mc.generateUpdateRequirementMessage(iumAbacusUserId,IUMMessageUtility.generateIUMMQkey(),prd);
					StringBuffer reply = new StringBuffer().append(con.putRequestMsg(message));
					IUMMessageData imd = mc.translatePolicyRequirementMaintenanceMessage(reply);				
				}
			}
			con.disconnect();
		} catch (Exception e) {
			LOGGER.error("Exception at Cancel Requirement");
			LOGGER.error(CodeHelper.getStackTrace(e));
		}
		LOGGER.info("processWaiveRequirement end");
	}


	private void setAbacusSpecificDetails(AssessmentRequestData ard) {

		LOGGER.info("setAbacusSpecificDetails start");
		StatusData sd = new StatusData();
		sd.setStatusId(IUMConstants.STATUS_NB_REVIEW_ACTION);
		LOBData lob = new LOBData();
		lob.setLOBCode(IUMConstants.LOB_INDIVIDUAL_LIFE);
		
		ard.setStatus(sd);
		ard.setLob(lob);
		ard.setSourceSystem(IUMConstants.SYSTEM_ABACUS);
		ard.setStatusDate(new Date());
		ard.setCreatedDate(new Date());
		
		UserProfileData upd = new UserProfileData();
		upd.setUserId(IUMConstants.USER_IUM);
		
		ard.setCreatedBy(upd);
		ard.setFolderLocation(ard.getAssignedTo());
		LOGGER.info("setAbacusSpecificDetails end");
	}
	
	
	private MessageConnector getConnector(String transactionId) throws SLMQInitException {
		return new MessageConnector(transactionId);
	}

}
