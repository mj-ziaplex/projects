/*
 * Created on Jan 24, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.service.test;

import java.util.ArrayList;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import com.slocpi.ium.interfaces.test.AbacusPolicy;
import com.slocpi.ium.interfaces.test.AbacusTestUtil;
import com.slocpi.ium.service.MessageController;

/**
 * This test case requires the generate assessment request to be
 * functioning properly.
 * 
 * @author daguila
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class TransmitToUSDTestCase extends TestCase {

	public TransmitToUSDTestCase(java.lang.String testName) {        
		super(testName);    
	}        

	public static Test suite() {        
		TestSuite suite = new TestSuite(TransmitToUSDTestCase.class);                
		return suite;    
	}	
	
	public static void main(java.lang.String[] args) {   
		junit.textui.TestRunner.run(suite());    
	}	
	
	public void testKickOutMessage() throws Exception {
		boolean res = false;
		MessageController mc = new MessageController();

		/*
		 * create records in ABACUS for reference
		 */
		AbacusTestUtil atu = new AbacusTestUtil(1,5,1);
		ArrayList policies = atu.getPolicyQueTestResults();
		AbacusPolicy ap = (AbacusPolicy)policies.get(0);
		
		/*
		 * execute the generate to create the policies in ium
		 */
		mc.processGetPolicyQueue(); 
		
		/*
		 * assume that the policy was created
		 */
		 
		mc.processPolicyDetails(ap.getPolicyId());
		
		assertEquals("Did not transmit properly",true,res);
	}
}
