/*
 * Created on Jan 5, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.service.test;

import java.util.ArrayList;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import com.slocpi.ium.interfaces.test.AbacusTestUtil;
import com.slocpi.ium.service.Poller;

/**
 * This test case will check the batch process for retrieving
 * policies for assessment requests.
 * 
 * For the test case, Abacus policy and policy queue tables will
 * be populated, IUM assessment requests will be populated.
 * 
 * The test case requires CreateAssessment to be fully functional.
 * 
 * @author daguila
 *
 */

public class GenerateAssessmentRequestTestCase extends TestCase {

	public GenerateAssessmentRequestTestCase(java.lang.String testName) {        
		super(testName);    
	}        

	public static Test suite() {        
		TestSuite suite = new TestSuite(GenerateAssessmentRequestTestCase.class);                
		return suite;    
	}	
	
	public static void main(java.lang.String[] args) {   
		junit.textui.TestRunner.run(suite());    
	}
	
	public void testGenerateSuccess1() throws Exception {
		boolean res = true;
		AbacusTestUtil artu = new AbacusTestUtil(5,5,1);
		ArrayList list = artu.getPolicyQueTestResults();
		Poller p = new Poller();
		p.generateAssessmentForAbacus();
		assertEquals("Generate Assessment Request failed using 1 record.",true,res);
	}

}
