/*
 * Created on Jan 20, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.service.test;

import java.util.Date;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import com.slocpi.ium.data.PolicyRequirementsData;
import com.slocpi.ium.service.MessageController;

/**
 * @author daguila
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class CreateRequirementTestCase extends TestCase {

	public CreateRequirementTestCase(java.lang.String testName) {        
		super(testName);    
	}        

	public static Test suite() {        
		TestSuite suite = new TestSuite(CreateRequirementTestCase.class);                
		return suite;    
	}	
	
	public static void main(java.lang.String[] args) {   
		junit.textui.TestRunner.run(suite());    
	}	
	
	public void testCreateRequirement() throws Exception {
		boolean res = false;
		MessageController mc = new MessageController();
		PolicyRequirementsData prd = new PolicyRequirementsData();
		prd.setAutoOrderInd(true);
		prd.setCCASuggestInd(true);
		prd.setClientId("client1");
		prd.setClientType("I");
		prd.setComments("whala");
		prd.setCompletedRequirementInd(false);
		prd.setCreateDate(new Date());
		prd.setCreatedBy("REQNBST1");
		prd.setDesignation("A");
		prd.setFldCommentInd(true);
		prd.setFollowUpNumber(1);
		prd.setLevel("C");
		prd.setNewTestOnly(true);
		prd.setPaidInd(false);
		prd.setReferenceNumber("derty");
		prd.setReqDesc("whyowhy");
		prd.setRequirementCode("R001");
		prd.setRequirementId(9999);
		/*
		 * for now just leave blank
		 */
		mc.processCreateRequirement(prd);
		assertEquals("Did not create requirement.",true,res); 
	}
		
}
