/*
 * Created on Jan 20, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.service.test;

import java.util.ArrayList;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import com.slocpi.ium.data.ClientData;
import com.slocpi.ium.interfaces.test.AbacusClientInfo;
import com.slocpi.ium.interfaces.test.AbacusPolicy;
import com.slocpi.ium.interfaces.test.AbacusTestUtil;
import com.slocpi.ium.service.MessageController;

/**
 * @author daguila
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class GetPolicyDetailsClientInfoTestCase extends TestCase {

	public GetPolicyDetailsClientInfoTestCase(java.lang.String testName) {        
		super(testName);    
	}        

	public static Test suite() {        
		TestSuite suite = new TestSuite(GetPolicyDetailsClientInfoTestCase.class);                
		return suite;    
	}	
	
	public static void main(java.lang.String[] args) {   
		junit.textui.TestRunner.run(suite());    
	}	
	
	public void testClientInfoMessage() throws Exception {
		boolean res = false;
		MessageController mc = new MessageController();
		
		//get the expected results
		AbacusTestUtil artu = new AbacusTestUtil(1,5,1);
		ArrayList list = artu.getPolicyQueTestResults();
		AbacusPolicy policy = (AbacusPolicy)list.get(0);  
		//AssessmentRequestData ard = mq.getPolicyDetails(policy.getPolicyId());
		mc.processPolicyDetails(policy.getPolicyId());
		
//		ClientData insrCD = ard.getInsured();
//		ClientData ownCD = ard.getOwner();
//		AbacusClientInfo insrACI = policy.getInsrClientInfo();
//		AbacusClientInfo ownACI = policy.getOwnClientInfo();
//
//		if (isClientInfoEqual(insrCD,insrACI) && (isClientInfoEqual(ownCD,ownACI))) {
//			res = true;
//		}
		assertEquals("Did not retrieve proper client info.", true,res);		
	}
	
	private boolean isClientInfoEqual(ClientData cd, AbacusClientInfo aci) {
		boolean res = false;
		if ((aci != null) && (cd != null)) {
			if (
				((cd.getClientId() != null)  && (cd.getClientId().equals(aci.getClientId())))
				)
			   {
				res = true;
			}
		} else if ((cd == null) && (aci == null)) {
			res = true;
		}		
		return res;
	}
}
