/*
 * Created on Jan 15, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.service.test;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import com.slocpi.ium.data.KickOutMessageData;
import com.slocpi.ium.interfaces.test.AbacusKickOutMessage;
import com.slocpi.ium.service.MessageController;

/**
 * @author daguila
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class GetPolicyDetailsKickOutMessagesTestCase extends TestCase {

	public GetPolicyDetailsKickOutMessagesTestCase(java.lang.String testName) {        
		super(testName);    
	}        

	public static Test suite() {        
		TestSuite suite = new TestSuite(GetPolicyDetailsKickOutMessagesTestCase.class);                
		return suite;    
	}	
	
	public static void main(java.lang.String[] args) {   
		junit.textui.TestRunner.run(suite());    
	}	
	
	public void testKickOutMessage() throws Exception {
		boolean res = false;

		MessageController mc = new MessageController();
		
		//get the expected results
//		AbacusTestUtil artu = new AbacusTestUtil(1,5);
//		ArrayList list = artu.getPolicyQueTestResults();
//		AbacusPolicy policy = (AbacusPolicy)list.get(0);  
//		AssessmentRequestData ard = mc.
//		ArrayList komds = ard.getKickOutMessages();
//		ArrayList koms = policy.getKickOutMessages();
//		if ((komds != null) && (koms != null) && (komds.size() == koms.size())) {
//			for (int i=0; i < komds.size(); i++) {
//				AbacusKickOutMessage kom = (AbacusKickOutMessage)koms.get(i);
//				KickOutMessageData komd = (KickOutMessageData)komds.get(i);
//				if (!isKickOutMessageEqual(komd,kom)) {
//					res = false;
//					break;
//				} else {
//					res = true;
//				}
//			}
//		} else if ((komds == null) && (koms == null)){
//			res = true;
//		}
		assertEquals("Did not retrieve the correct Kick Out Messages.",true,res);
		

	}
	
	private boolean isKickOutMessageEqual(KickOutMessageData komd, AbacusKickOutMessage kom) {
		boolean res = false;
		if ((komd != null) && (kom != null)) {
			if (
				(komd.getClientId().equals(kom.getClientId())) &&
				(komd.getFailResponse().equals(kom.getMessageResponseText())) &&
				(komd.getMessageText().equals(kom.getMessageText())) &&
				(komd.getReferenceNumber().equals(kom.getSubjectPolicyId())) &&
				(komd.getSequenceNumber() == kom.getSequenceNumber())
			   ) 
			{
				res = true;
			}
		} else if ((komd == null) && (kom == null)) {
			res = true;
		}
		return res;
	}

}
