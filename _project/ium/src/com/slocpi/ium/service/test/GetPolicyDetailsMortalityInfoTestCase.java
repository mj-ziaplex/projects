/*
 * Created on Jan 16, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.service.test;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import com.slocpi.ium.data.CDSMortalityRatingData;
import com.slocpi.ium.interfaces.test.AbacusMortalityRating;

/**
 * @author daguila
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class GetPolicyDetailsMortalityInfoTestCase extends TestCase {

	public GetPolicyDetailsMortalityInfoTestCase(java.lang.String testName) {        
		super(testName);    
	}        

	public static Test suite() {        
		TestSuite suite = new TestSuite(GetPolicyDetailsMortalityInfoTestCase.class);                
		return suite;    
	}	
	
	public static void main(java.lang.String[] args) {   
		junit.textui.TestRunner.run(suite());    
	}	
	
	public void testClientRelatedInfoMessage() throws Exception {
		boolean res = false;
//		MessageQueue mq = new MessageQueue();
		
		//get the expected results
//		AbacusTestUtil artu = new AbacusTestUtil(1,5);
//		ArrayList list = artu.getPolicyQueTestResults();
//		AbacusPolicy policy = (AbacusPolicy)list.get(0);  
//		AssessmentRequestData ard = mq.getPolicyDetails(policy.getPolicyId());
//
//		CDSMortalityRatingData insrMort = ard.getInsuredMortalityRating();
//		CDSMortalityRatingData ownMort = ard.getOwnerMortalityRating();
//		
//		AbacusMortalityRating aInsrMort = policy.getInsrMortRating();
//		AbacusMortalityRating aOwnMort = policy.getOwnMortRating();		
//		
//		if (isMortalityRatingEqual(insrMort,aInsrMort) && (isMortalityRatingEqual(ownMort,aOwnMort))) {
//			res = true;
//		}
		assertEquals("Did not retrieve proper client mortality info.", true,res);
	}
	
	private boolean isMortalityRatingEqual(CDSMortalityRatingData mrd, AbacusMortalityRating amr) {
		boolean res = false;
		if ((mrd != null) && (amr != null)) {
			if (
			    ((mrd.getReferenceNumber() != null)  && (mrd.getReferenceNumber().equals(amr.getSubjectPolicyId()))) &&
			    (mrd.getAPDB() == amr.getAPDBRating1())
			    )
			   {
				res = true;
			}
		} else if ((mrd == null) && (amr == null)) {
			res = true;
		}
		return res;
	}
}
