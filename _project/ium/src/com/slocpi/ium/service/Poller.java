/*
 * Created on Jan 9, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.service;

import java.sql.Timestamp;
import java.util.Timer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.slocpi.ium.interfaces.IUMInterfaceException;
import com.slocpi.ium.interfaces.mib.MIBController;
import com.slocpi.ium.interfaces.mib.MIBThread;
import com.slocpi.ium.interfaces.mib.MedLabRecThread;
import com.slocpi.ium.service.ingenium.IngeniumGateway;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.IUMConstants;

/**
 * This class is responsible for executing the different batch processes in IUM.
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */

public class Poller {
	private static final Logger LOGGER = LoggerFactory.getLogger(MessageController.class);
	/**
	 * This method triggers the Abacus batch process.
	 *
	 */
	public void generateAssessmentForAbacus(){
		
		LOGGER.info("generateAssessmentForAbacus start");
		IngeniumGateway	  ig = new IngeniumGateway(true);
		try {
			ig.processGetPolicyQueue();
		}catch(IUMInterfaceException e){
			LOGGER.error(CodeHelper.getStackTrace(e));
		}
		LOGGER.info("generateAssessmentForAbacus start");
	}
	
	public void exportMIB(){
		
		LOGGER.info("exportMIB start");
		Timestamp ts = new Timestamp(System.currentTimeMillis());
		
		MIBController mib = new MIBController();
		Timestamp meTS = mib.getJobSchedule(IUMConstants.SCHEDULE_TYPE_MIB_EXPORT);
		
		if(meTS!=null){
		
			if(ts.getHours()+1 == meTS.getHours()){
				if(ts.getMinutes()>=55){
					long min = (60 - ts.getMinutes()) * 60000;
					Timer timer = new Timer();
					timer.schedule(new MIBThread(), min);							
				}
			}
		}
		LOGGER.info("exportMIB end");
	}
	
	public void autoExpireMedRecords(){
		
		LOGGER.info("autoExpireMedRecords start");
		Timestamp ts = new Timestamp(System.currentTimeMillis());
		
		MIBController mib = new MIBController();
		Timestamp aeTS = mib.getJobSchedule(IUMConstants.SCHEDULE_TYPE_AUTO_EXPIRE);
		
		if(aeTS!=null){
		
			if(ts.getHours()+1 == aeTS.getHours()){
				if(ts.getMinutes()>=55){
					long min = (60 - ts.getMinutes()) * 60000;
					Timer timer = new Timer();
					timer.schedule(new MedLabRecThread(), min);
							
				}
			}
		}
		LOGGER.info("autoExpireMedRecords end");
	}
	


}
