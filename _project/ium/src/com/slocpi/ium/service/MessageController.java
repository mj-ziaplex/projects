/*
 * Created on Jan 21, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.service;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import java.util.Timer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.abacus.MessageConnector;
import com.slocpi.ium.ExceptionDetail;
import com.slocpi.ium.ExceptionLog;
import com.slocpi.ium.ExceptionLogger;
import com.slocpi.ium.IUMException;
import com.slocpi.ium.data.AssessmentRequestData;
import com.slocpi.ium.data.CDSMortalityRatingData;
import com.slocpi.ium.data.CDSSummaryPolicyCoverageData;
import com.slocpi.ium.data.ClientData;
import com.slocpi.ium.data.ClientDataSheetData;
import com.slocpi.ium.data.IUMMessageData;
import com.slocpi.ium.data.KickOutMessageData;
import com.slocpi.ium.data.LOBData;
import com.slocpi.ium.data.PolicyDetailData;
import com.slocpi.ium.data.PolicyRequirementsData;
import com.slocpi.ium.data.RequirementData;
import com.slocpi.ium.data.StatusData;
import com.slocpi.ium.data.SunLifeOfficeData;
import com.slocpi.ium.data.UserProfileData;
import com.slocpi.ium.data.dao.ClientDAO;
import com.slocpi.ium.data.dao.RequirementDAO;
import com.slocpi.ium.data.dao.StatusCHDao;
import com.slocpi.ium.data.util.DataSourceProxy;
import com.slocpi.ium.interfaces.IUMInterfaceException;
import com.slocpi.ium.interfaces.messages.IUMMessageUtility;
import com.slocpi.ium.interfaces.messages.MessageConverter;
import com.slocpi.ium.service.threads.ProcessAcceptRequirementThread;
import com.slocpi.ium.service.threads.ProcessAutoSettleAssessmentRequestThread;
import com.slocpi.ium.service.threads.ProcessCancelRequirementThread;
import com.slocpi.ium.service.threads.ProcessCreateRequirementThread;
import com.slocpi.ium.service.threads.ProcessGetPolicyQueueThread;
import com.slocpi.ium.service.threads.ProcessOrderRequirementThread;
import com.slocpi.ium.service.threads.ProcessPolicyDetailsThread;
import com.slocpi.ium.service.threads.ProcessReceiveRequirementThread;
import com.slocpi.ium.service.threads.ProcessRejectRequirementThread;
import com.slocpi.ium.service.threads.ProcessUpdateRequirementThread;
import com.slocpi.ium.service.threads.ProcessWaiveRequirementThread;
import com.slocpi.ium.underwriter.AssessmentRequest;
import com.slocpi.ium.underwriter.PolicyRequirements;
import com.slocpi.ium.underwriter.UnderWriterException;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.IUMConstants;
import com.sunlife.mq.SLMQInitException;

/**
 * This class is responsible for interfacing with Abacus system
 * @author daguila
 *
 */
public class MessageController implements AdminSystem {

	private static final Logger LOGGER = LoggerFactory.getLogger(MessageController.class);
	private String useThread;
	private String	iumAbacusUserId = "";

	public MessageController(){
	}


   /* (non-Javadoc)
 * @see com.slocpi.ium.service.AdminSystem#processGetPolicyQueue()
 */
	public void processGetPolicyQueue() throws IUMInterfaceException{
		
		LOGGER.info("processGetPolicyQueue start");
		init();
		if(useThread.trim().equalsIgnoreCase("true")){
			Timer timer = new Timer();
			timer.schedule(new ProcessGetPolicyQueueThread(iumAbacusUserId), 1000);
		}else{
			MessageConverter mc = new MessageConverter();
			MessageConnector con = null; 
			try {
				ArrayList list = new ArrayList();
				con = getConnector(mc.TRANSACTION_POLICY_QUEUE);
				con.connect();
				String message = mc.generatePolicyQueueMessage(iumAbacusUserId,IUMMessageUtility.generateIUMMQkey());
				
				StringBuffer reply = new StringBuffer().append(con.putRequestMsg(message));
				if(reply==null){
					ExceptionLog exLog = new ExceptionLog();
					exLog.setRecordType(mc.TRANSACTION_POLICY_QUEUE);
				/*	exLog.setReply(reply.toString());*/
					exLog.setSystemInterface(IUMConstants.SYSTEM_ABACUS_DESC);
					exLog.setRequestMsg(message);
					ArrayList arr = new ArrayList();
					arr.add(createExceptionDetail("MQ: No reply message available"));
					exLog.setDetails(arr);
					ExceptionLogger errorLogger = new ExceptionLogger();
					errorLogger.logError(exLog);
				}else{
					
					list = mc.translatePolicyQueMessage(reply);
					if ((list != null) && (list.size() > 0)) {
						AssessmentRequest ar = new AssessmentRequest();
						for (int i=0; i <list.size(); i++) {
							AssessmentRequestData ard = (AssessmentRequestData)list.get(i);
						    setAbacusSpecificDetails(ard);
							ExceptionLog exLog = validateARD(ard);
							if(exLog!=null){
								
								exLog.setRecordType(mc.TRANSACTION_POLICY_QUEUE);
								exLog.setReply(reply.toString());
								exLog.setSystemInterface(IUMConstants.SYSTEM_ABACUS_DESC);
								exLog.setRequestMsg(message);
								exLog.setReferenceNumber(ard.getReferenceNumber());
								exLog.setXmlRecord(ard.getXmlRecord());
								ExceptionLogger errorLogger = new ExceptionLogger();
								errorLogger.logError(exLog);
							}else{
								try {
									ar.createAssessmentRequest(ard);
								} catch (UnderWriterException uwe) {
								
									exLog = new ExceptionLog();
									exLog.setRecordType(mc.TRANSACTION_POLICY_QUEUE);
									exLog.setReply(reply.toString());
									exLog.setSystemInterface(IUMConstants.SYSTEM_ABACUS_DESC);
									exLog.setRequestMsg(message);
									exLog.setReferenceNumber(ard.getReferenceNumber());
									exLog.setXmlRecord(ard.getXmlRecord());
									ArrayList arr = new ArrayList();
									arr.add(createExceptionDetail(uwe.getMessage()));
									exLog.setDetails(arr);
									ExceptionLogger errorLogger = new ExceptionLogger();
									errorLogger.logError(exLog);
								}
							}
						}
					}
					
				}
			} catch (Exception e) {
				LOGGER.error(CodeHelper.getStackTrace(e));
				logError(e, mc.TRANSACTION_POLICY_QUEUE);
			} finally  {
				if (null != con) con.disconnect();
			}
		}
		LOGGER.info("processGetPolicyQueue end");
	}


	/* (non-Javadoc)
	 * @see com.slocpi.ium.service.AdminSystem#processPolicyDetails(java.lang.String)
	 */
	public void processPolicyDetails(String refNum) throws IUMInterfaceException{
		
		LOGGER.info("processPolicyDetails start");
		init();
		if(useThread.trim().equalsIgnoreCase("true")){
			Timer timer = new Timer();
			timer.schedule(new ProcessPolicyDetailsThread(iumAbacusUserId, refNum), 1000);
		}else{
			String trans = "";
			MessageConnector con = null; // Fix for CR#10146
			try {
				AssessmentRequestData ard = null;
				AssessmentRequest ar = new AssessmentRequest();
				MessageConverter mc = new MessageConverter();
				
				con = getConnector(mc.TRANSACTION_KICK_OUT);
				trans = mc.TRANSACTION_KICK_OUT;
				con.connect();
				String message = mc.generateKickOutMessage(iumAbacusUserId,IUMMessageUtility.generateIUMMQkey(),refNum);
				StringBuffer reply = new StringBuffer().append(con.putRequestMsg(message));
				con.disconnect();
				ArrayList exceptionLogs = new ArrayList();
				ExceptionLogger errorLogger = new ExceptionLogger();
				boolean noExceptions = true;
				ArrayList kos = null;
				if(reply==null){
					ExceptionLog exLog = new ExceptionLog();
					exLog.setRecordType(mc.TRANSACTION_KICK_OUT);
				/*	exLog.setReply(reply.toString());*/
					exLog.setSystemInterface(IUMConstants.SYSTEM_ABACUS_DESC);
					exLog.setRequestMsg(message);
					ArrayList arr = new ArrayList();
					arr.add(createExceptionDetail("MQ: No reply message available"));
					exLog.setDetails(arr);
					noExceptions = false;
					exceptionLogs.add(exLog);
					LOGGER.warn("MQ: No reply message available");
				}else{
					
					kos = mc.translateKickOutMessage(reply);
					for (int i =0; i<kos.size(); i++){
						KickOutMessageData koData = (KickOutMessageData) kos.get(i);
						ExceptionLog exLog = validateKOMessages(koData);

						if (exLog != null){
							exLog.setRecordType(mc.TRANSACTION_KICK_OUT);
							exLog.setReferenceNumber(refNum);
							exLog.setReply(reply.toString());
							exLog.setSystemInterface(IUMConstants.SYSTEM_ABACUS_DESC);
							exLog.setRequestMsg(message);
							exLog.setXmlRecord(koData.getXmlRecord());
							exceptionLogs.add(exLog);
							noExceptions = false;
						}
					}
					
				}
				
				if (exceptionLogs.size()>0){
					errorLogger.logError(exceptionLogs);
					
				}

				
				con = getConnector(mc.TRANSACTION_CLIENT_DATA_SHEET);
				trans = mc.TRANSACTION_CLIENT_DATA_SHEET;
				con.connect();
				message = mc.generateClientRelatedInfoMessage(iumAbacusUserId,IUMMessageUtility.generateIUMMQkey(),refNum);
				reply = new StringBuffer().append(con.putRequestMsg(message));
				con.disconnect();
				

				if(reply==null){
					ExceptionLog exLog = new ExceptionLog();
					exLog.setRecordType(mc.TRANSACTION_CLIENT_DATA_SHEET);
					/*exLog.setReply(reply.toString());*/
					exLog.setSystemInterface(IUMConstants.SYSTEM_ABACUS_DESC);
					exLog.setRequestMsg(message);
					ArrayList arr = new ArrayList();
					arr.add(createExceptionDetail("MQ: No reply message available"));
					exLog.setDetails(arr);
					noExceptions = false;
					exceptionLogs.add(exLog);
					LOGGER.warn("MQ: No reply message available");
				}else{
					ard = mc.translateClientRelatedInfoMessage(reply);
					ClientData ownerData = ard.getOwner();
					ClientData insuredData = ard.getInsured();

					ExceptionLog ownerDataException = validateClientInfo(ownerData);
					if (ownerDataException != null){
						ownerDataException.setRecordType(mc.TRANSACTION_CLIENT_DATA_SHEET);
						ownerDataException.setReferenceNumber(refNum);
						ownerDataException.setReply(reply.toString());
						ownerDataException.setSystemInterface(IUMConstants.SYSTEM_ABACUS_DESC);
						ownerDataException.setRequestMsg(message);
						ownerDataException.setXmlRecord(ard.getXmlRecord());

						errorLogger.logError(ownerDataException);
						noExceptions = false;
					}

					ExceptionLog insuredDataException = validateClientInfo(insuredData);
					if (insuredDataException != null){
						insuredDataException.setRecordType(mc.TRANSACTION_CLIENT_DATA_SHEET);
						insuredDataException.setReferenceNumber(refNum);
						insuredDataException.setReply(reply.toString());
						insuredDataException.setSystemInterface(IUMConstants.SYSTEM_ABACUS_DESC);
						insuredDataException.setRequestMsg(message);
						insuredDataException.setXmlRecord(ard.getXmlRecord());

						errorLogger.logError(insuredDataException);
						noExceptions = false;
					} // end of Validation

					//validates the summary policy coverage data
					CDSSummaryPolicyCoverageData policyCoverage = ard.getClientDataSheet().getSummaryOfPolicies();
					ExceptionLog policyCoverageException = validateSummaryPolicyCoverage(policyCoverage);

					if (policyCoverageException != null){
						policyCoverageException.setRecordType(mc.TRANSACTION_CLIENT_DATA_SHEET);
						policyCoverageException.setReferenceNumber(refNum);
						policyCoverageException.setReply(reply.toString());
						policyCoverageException.setSystemInterface(IUMConstants.SYSTEM_ABACUS_DESC);
						policyCoverageException.setRequestMsg(message);
						policyCoverageException.setXmlRecord(ard.getXmlRecord());
						errorLogger.logError(policyCoverageException);
						noExceptions = false;

					} // end of validation

					CDSMortalityRatingData ownerMortality = ard.getOwnerMortalityRating();
					CDSMortalityRatingData insuredMortality = ard.getInsuredMortalityRating();

					ExceptionLog oMortalityException = validateMortalityRating(ownerMortality);
					if (oMortalityException != null){
						oMortalityException.setRecordType(mc.TRANSACTION_CLIENT_DATA_SHEET);
						oMortalityException.setReferenceNumber(refNum);
						oMortalityException.setReply(reply.toString());
						oMortalityException.setSystemInterface(IUMConstants.SYSTEM_ABACUS_DESC);
						oMortalityException.setRequestMsg(message);
						oMortalityException.setXmlRecord(ard.getXmlRecord());

						errorLogger.logError(oMortalityException);
						noExceptions = false;
					}

					ExceptionLog iMortalityException = validateMortalityRating(insuredMortality);
					if (iMortalityException != null){
						iMortalityException.setRecordType(mc.TRANSACTION_CLIENT_DATA_SHEET);
						iMortalityException.setReferenceNumber(refNum);
						iMortalityException.setReply(reply.toString());
						iMortalityException.setSystemInterface(IUMConstants.SYSTEM_ABACUS_DESC);
						iMortalityException.setRequestMsg(message);
						iMortalityException.setXmlRecord(ard.getXmlRecord());

						errorLogger.logError(iMortalityException);
						noExceptions = false;
					} // end of validation

					ard.setKickOutMessages(kos);
				}

				
				con = getConnector(mc.TRANSACTION_RELATED_POLICY_AND_COVERAGE);
				trans = mc.TRANSACTION_RELATED_POLICY_AND_COVERAGE;
				con.connect();
				message = mc.generateRelatedPolicyAndCoverageMessage(iumAbacusUserId,IUMMessageUtility.generateIUMMQkey(),refNum);
				reply = new StringBuffer().append(con.putRequestMsg(message));
				con.disconnect();
				
				if(reply==null){
					ExceptionLog exLog = new ExceptionLog();
					exLog.setRecordType(mc.TRANSACTION_RELATED_POLICY_AND_COVERAGE);
					/*exLog.setReply(reply.toString());*/
					exLog.setSystemInterface(IUMConstants.SYSTEM_ABACUS_DESC);
					exLog.setRequestMsg(message);
					ArrayList arr = new ArrayList();
					arr.add(createExceptionDetail("MQ: No reply message available"));
					exLog.setDetails(arr);
					noExceptions = false;
					exceptionLogs.add(exLog);
				}else{

					ArrayList relatedPolicies = mc.translateRelatedPolicyAndCoverageInfoMessage(reply);
					ArrayList policyDetailExceptions = new ArrayList();
					for (int i=0; i<relatedPolicies.size(); i++){
						PolicyDetailData policyDetail = (PolicyDetailData) relatedPolicies.get(i);
						ExceptionLog polDetailException = validatePolicyCoverageDetails(policyDetail);

						if (polDetailException  != null){
							polDetailException.setRecordType(mc.TRANSACTION_RELATED_POLICY_AND_COVERAGE);
							polDetailException.setReferenceNumber(refNum);
							polDetailException.setReply(reply.toString());
							polDetailException.setSystemInterface(IUMConstants.SYSTEM_ABACUS_DESC);
							polDetailException.setRequestMsg(message);
							polDetailException.setXmlRecord(policyDetail.getXmlRecord());
							policyDetailExceptions.add(polDetailException);
							noExceptions = false;
						}
					}

					if (policyDetailExceptions.size() > 0){
						errorLogger.logError(policyDetailExceptions);
					}

					ClientDataSheetData cds = new ClientDataSheetData();
					cds = ard.getClientDataSheet();
					cds.setPolicyCoverageDetailsList(relatedPolicies);

					LOBData lob = new LOBData();
					lob.setLOBCode(IUMConstants.LOB_INDIVIDUAL_LIFE);
					ard.setLob(lob);
				}

				con = getConnector(mc.TRANSACTION_REQUIREMENT_INQUIRY);
				trans = mc.TRANSACTION_REQUIREMENT_INQUIRY;
				con.connect();
				message = mc.generateRetrieveRequirementsMessage(iumAbacusUserId,IUMMessageUtility.generateIUMMQkey(),refNum);
				reply = new StringBuffer().append(con.putRequestMsg(message));

				ArrayList requirements = null;
				if(reply==null){
					ExceptionLog exLog = new ExceptionLog();
					exLog.setRecordType(mc.TRANSACTION_REQUIREMENT_INQUIRY);
					/*exLog.setReply(reply.toString());*/
					exLog.setSystemInterface(IUMConstants.SYSTEM_ABACUS_DESC);
					exLog.setRequestMsg(message);
					ArrayList arr = new ArrayList();
					arr.add(createExceptionDetail("MQ: No reply message available"));
					exLog.setDetails(arr);
					noExceptions = false;
					exceptionLogs.add(exLog);
				}else{
				
					requirements = mc.translatePolicyRequirementInquiryMessage(reply);
					ArrayList requirementExceptions = new ArrayList();

					for (int i=0; i<requirements.size(); i++){
						PolicyRequirementsData reqData = (PolicyRequirementsData) requirements.get(i);
						ExceptionLog reqException = validatePolicyRequiremetns(reqData);

						if (reqException != null){
							reqException.setRecordType(mc.TRANSACTION_REQUIREMENT_INQUIRY);
							reqException.setReferenceNumber(refNum);
							reqException.setReply(reply.toString());
							reqException.setSystemInterface(IUMConstants.SYSTEM_ABACUS_DESC);
							reqException.setRequestMsg(message);
							reqException.setXmlRecord(reqData.getXmlRecord());

							errorLogger.logError(reqException);
							noExceptions = false;
						}
					}
				}

				if (noExceptions){
					ar.transmitMQDataToIUMDB(ard);
					if ((requirements != null) && (requirements.size() > 0)) {
						PolicyRequirements pr = new PolicyRequirements();
						for (int i=0; i < requirements.size(); i++) {
							PolicyRequirementsData prd = (PolicyRequirementsData)requirements.get(i);
							pr.createRequirementIUM(prd,ard);//???
						}
					}
					ar.setSuccessfulRetrieval(ard.getReferenceNumber(),IUMConstants.INDICATOR_SUCCESS);
					
					con = getConnector(mc.TRANSACTION_CLIENT_DATA_SHEET);
					trans = mc.TRANSACTION_CLIENT_DATA_SHEET;
					con.connect();
					String deleteMsg1 = mc.generateClientRelatedInfoDeleteMessage(iumAbacusUserId,IUMMessageUtility.generateIUMMQkey(),refNum);
					StringBuffer delteReply1 = new StringBuffer().append(con.putRequestMsg(deleteMsg1));

					con = getConnector(mc.TRANSACTION_RELATED_POLICY_AND_COVERAGE);
					trans = mc.TRANSACTION_RELATED_POLICY_AND_COVERAGE;
					con.connect();
					String deleteMsg2 = mc.generateRelatedPolicyAndCoverageDeleteMessage(iumAbacusUserId,IUMMessageUtility.generateIUMMQkey(),refNum);
					StringBuffer delteReply2 = new StringBuffer().append(con.putRequestMsg(deleteMsg2));
					
				}
			} catch (Exception e) {
				LOGGER.error(CodeHelper.getStackTrace(e));
				logError(e, trans);
			} finally {
				if (null != con) con.disconnect();
			}
		}
		LOGGER.info("processPolicyDetails end");
	}


	/* (non-Javadoc)
	 * @see com.slocpi.ium.service.AdminSystem#processAutoSettleAssessmentRequest(com.slocpi.ium.data.AssessmentRequestData)
	 */
	public void processAutoSettleAssessmentRequest(AssessmentRequestData ard) throws IUMInterfaceException{
		
		LOGGER.info("processAutoSettleAssessmentRequest start");
		init();
		if(useThread.trim().equalsIgnoreCase("true")){
			Timer timer = new Timer();
			timer.schedule(new ProcessAutoSettleAssessmentRequestThread(iumAbacusUserId, ard), 1000);
		}else{
			MessageConverter mc = new MessageConverter();
			MessageConnector con = null; // Fix for CR#10146
			try {
				
				con = getConnector(mc.TRANSACTION_AUTO_SETTLE);
				con.connect();
				String msgSCCP = mc.generateAutoSettleSCCPMessage(iumAbacusUserId,IUMMessageUtility.generateIUMMQkey(), ard.getReferenceNumber(), ard.getAssignedTo().getACF2ID(), ard.getUnderwriter().getACF2ID());
				StringBuffer replySCCP = new StringBuffer().append(con.putRequestMsg(msgSCCP));
				if(replySCCP==null){
					ExceptionLog el= new ExceptionLog();
					el.setRecordType(mc.TRANSACTION_AUTO_SETTLE);
					el.setRequestMsg(msgSCCP);
					el.setSystemInterface(IUMConstants.SYSTEM_ABACUS_DESC);
					ArrayList arr = new ArrayList();
					arr.add(createExceptionDetail("MQ: No reply message available"));
					el.setDetails(arr);
					ExceptionLogger errorLogger = new ExceptionLogger();
					errorLogger.logError(el);
				}else{
					IUMMessageData imdS = mc.translateAutoSettleMessage(replySCCP);
					ExceptionLog exLogS = validateAutoSettleAR(imdS);
					if(exLogS==null){
						
						AssessmentRequest ar =  new AssessmentRequest();
						ar.setAutoSettleIndicator(ard.getReferenceNumber(), IUMConstants.INDICATOR_SUCCESS);
						String msgUWDE = mc.generateAutoSettleUWDEMessage(iumAbacusUserId,IUMMessageUtility.generateIUMMQkey(), ard.getReferenceNumber(), ard.getAssignedTo().getACF2ID(), ard.getUnderwriter().getACF2ID());
						StringBuffer replyUWDE = new StringBuffer().append(con.putRequestMsg(msgUWDE));
						if(replyUWDE==null){
							ExceptionLog el= new ExceptionLog();
							el.setRecordType(mc.TRANSACTION_AUTO_SETTLE);
							el.setRequestMsg(msgUWDE);
							el.setSystemInterface(IUMConstants.SYSTEM_ABACUS_DESC);
							ArrayList arr = new ArrayList();
							arr.add(createExceptionDetail("MQ: No reply message available"));
							el.setDetails(arr);
							ExceptionLogger errorLogger = new ExceptionLogger();
							errorLogger.logError(el);
						}else{
							IUMMessageData imdU = mc.translateAutoSettleMessage(replyUWDE);
							ExceptionLog exLogU = validateAutoSettleAR(imdU);
							if(exLogU!=null){
								exLogU.setRecordType(mc.TRANSACTION_AUTO_SETTLE);
								exLogU.setReply(replyUWDE.toString());
								exLogU.setSystemInterface(IUMConstants.SYSTEM_ABACUS_DESC);
								exLogU.setRequestMsg(msgUWDE);
								exLogU.setReferenceNumber(ard.getReferenceNumber());
								/*exLogS.setXmlRecord(imdU.getXmlRecord());*/
								ExceptionLogger errorLogger = new ExceptionLogger();
								errorLogger.logError(exLogU);
							}else{
								//TODO what will we do if uwde is successful?
							}
						}
					}else{
						//log error
						exLogS.setRecordType(mc.TRANSACTION_AUTO_SETTLE);
						exLogS.setReply(replySCCP.toString());
						exLogS.setSystemInterface(IUMConstants.SYSTEM_ABACUS_DESC);
						exLogS.setRequestMsg(msgSCCP);
						exLogS.setReferenceNumber(ard.getReferenceNumber());
						exLogS.setXmlRecord(imdS.getXmlRecord());
						ExceptionLogger errorLogger = new ExceptionLogger();
						errorLogger.logError(exLogS);
					}
				}
			} catch (Exception e) {
				LOGGER.error(CodeHelper.getStackTrace(e));
				logError(e, mc.TRANSACTION_AUTO_SETTLE);
			} finally {
				if (null != con) con.disconnect();
			}
		}
		LOGGER.info("processAutoSettleAssessmentRequest end");
	}

	/* (non-Javadoc)
	 * @see com.slocpi.ium.service.AdminSystem#processCreateRequirement(com.slocpi.ium.data.PolicyRequirementsData)
	 */
	public void processCreateRequirement(PolicyRequirementsData prd) throws IUMInterfaceException{
		
		LOGGER.info("processCreateRequirement start");
		init();
		if(useThread.trim().equalsIgnoreCase("true")){
			Timer timer = new Timer();
			timer.schedule(new ProcessCreateRequirementThread(prd, iumAbacusUserId), 1000);
		}else{
			MessageConverter mc = new MessageConverter();
			MessageConnector con = null; //Fix for CR#10146
			try {
				con = getConnector(mc.TRANSACTION_REQUIREMENT_MAINTENANCE);
				con.connect();
				
				String message = mc.generateCreateRequirementMessage(iumAbacusUserId,IUMMessageUtility.generateIUMMQkey(),prd);
				StringBuffer reply = new StringBuffer().append(con.putRequestMsg(message));
				
				if(reply==null){
					ExceptionLog el= new ExceptionLog();
					el.setRecordType(mc.TRANSACTION_REQUIREMENT_MAINTENANCE);
					el.setSystemInterface(IUMConstants.SYSTEM_ABACUS_DESC);
					el.setRequestMsg(message);
					ArrayList arr = new ArrayList();
					arr.add(createExceptionDetail("MQ: No reply message available"));
					el.setDetails(arr);
					ExceptionLogger errorLogger = new ExceptionLogger();
					errorLogger.logError(el);
				}else{
					IUMMessageData imd =  mc.translatePolicyRequirementMaintenanceMessage(reply);
					ExceptionLog exLog = validateIUMMessageData(imd);
					
					if(exLog==null){
					   prd.setABACUScreateDate(imd.getCreationDate());
					   prd.setSequenceNumber(imd.getSequenceNumber());
					   PolicyRequirements pr = new PolicyRequirements();
					  pr.maintainRequirement(prd);

					}else{
					   exLog.setRecordType(mc.TRANSACTION_REQUIREMENT_MAINTENANCE);
					   exLog.setRequestMsg(message);
					   exLog.setReply(reply.toString());
					   exLog.setXmlRecord(reply.toString());
					   exLog.setSystemInterface(IUMConstants.SYSTEM_ABACUS_DESC);
					   ExceptionLogger errorLogger = new ExceptionLogger();
					   errorLogger.logError(exLog);
					}
				}
			} catch (Exception e) {
				LOGGER.error(CodeHelper.getStackTrace(e));
				logError(e, mc.TRANSACTION_REQUIREMENT_MAINTENANCE);
			} finally {
				if (null != con) con.disconnect();
			}
		}
		LOGGER.info("processCreateRequirement end");
	}

	/* (non-Javadoc)
	 * @see com.slocpi.ium.service.AdminSystem#processReceiveRequirement(java.util.ArrayList)
	 */
	public void processReceiveRequirement(ArrayList list) throws IUMInterfaceException{
		
		LOGGER.info("processReceiveRequirement start");
		init();
		if(useThread.trim().equalsIgnoreCase("true")){
			Timer timer = new Timer();
			timer.schedule(new ProcessReceiveRequirementThread(iumAbacusUserId, list), 1000);
		}else{
			MessageConverter mc = new MessageConverter();
			MessageConnector con = null; // Fix for CR#10146
			try {

				con = getConnector(mc.TRANSACTION_REQUIREMENT_MAINTENANCE);
				MessageConverter mt = new MessageConverter();
				con.connect();
				if ((list != null) && (list.size() > 0)) {
					for (int i=0; i <list.size(); i++) {
						PolicyRequirementsData prd = (PolicyRequirementsData)list.get(i);
						String message = mc.generateReceiveRequirementMessage(iumAbacusUserId,IUMMessageUtility.generateIUMMQkey(),prd);
						StringBuffer reply = new StringBuffer().append(con.putRequestMsg(message));
						if(reply==null){
							ExceptionLog el= new ExceptionLog();
							el.setRecordType(mc.TRANSACTION_REQUIREMENT_MAINTENANCE);
							el.setSystemInterface(IUMConstants.SYSTEM_ABACUS_DESC);
							el.setRequestMsg(message);
							el.setReferenceNumber(prd.getReferenceNumber());
							ArrayList arr = new ArrayList();
							arr.add(createExceptionDetail("MQ: No reply message available"));
							el.setDetails(arr);
							ExceptionLogger errorLogger = new ExceptionLogger();
							errorLogger.logError(el);
						}else{
							IUMMessageData imd = mt.translatePolicyRequirementMaintenanceMessage(reply);
							ExceptionLog exLog = validateIUMMessageData(imd);
							if(exLog==null){
								prd.setABACUScreateDate(imd.getCreationDate());
								prd.setSequenceNumber(imd.getSequenceNumber());
							    PolicyRequirements pr = new PolicyRequirements();
							    pr.maintainRequirement(prd);

							}else{
								exLog.setRecordType(mc.TRANSACTION_REQUIREMENT_MAINTENANCE);
								exLog.setRequestMsg(message);
								exLog.setReply(reply.toString());
								exLog.setXmlRecord(reply.toString());
								exLog.setSystemInterface(IUMConstants.SYSTEM_ABACUS_DESC);
								ExceptionLogger errorLogger = new ExceptionLogger();
								errorLogger.logError(exLog);
							}
						}
					}
				}
			} catch (Exception e) {
				LOGGER.error(CodeHelper.getStackTrace(e));
				logError(e, mc.TRANSACTION_REQUIREMENT_MAINTENANCE);
			} finally {
				if (null != con) con.disconnect();
			}
		}
		LOGGER.info("processReceiveRequirement end");
	}

	/* (non-Javadoc)
	 * @see com.slocpi.ium.service.AdminSystem#processAcceptRequirement(java.util.ArrayList)
	 */

	public void processAcceptRequirement(ArrayList list) throws IUMInterfaceException{
		
		LOGGER.info("processAcceptRequirement start");
		init();
		if(useThread.trim().equalsIgnoreCase("true")){
			Timer timer = new Timer();
			timer.schedule(new ProcessAcceptRequirementThread(iumAbacusUserId, list), 1000);
		}else{
			MessageConverter mc = new MessageConverter();
			MessageConnector con = null; //fix for CR#10146
			try {

				
				con = getConnector(mc.TRANSACTION_REQUIREMENT_MAINTENANCE);
				con.connect();
				if ((list != null) && (list.size() > 0)) {
					for (int i=0; i <list.size(); i++) {
						PolicyRequirementsData prd = (PolicyRequirementsData)list.get(i);
						String message = mc.generateAcceptRequirementMessage(iumAbacusUserId,IUMMessageUtility.generateIUMMQkey(),prd);
						StringBuffer reply = new StringBuffer().append(con.putRequestMsg(message));
						
						if(reply==null){
							ExceptionLog el= new ExceptionLog();
							el.setRecordType(mc.TRANSACTION_REQUIREMENT_MAINTENANCE);
							el.setSystemInterface(IUMConstants.SYSTEM_ABACUS_DESC);
							el.setRequestMsg(message);
							el.setReferenceNumber(prd.getReferenceNumber());
							ArrayList arr = new ArrayList();
							arr.add(createExceptionDetail("MQ: No reply message available"));
							el.setDetails(arr);
							ExceptionLogger errorLogger = new ExceptionLogger();
							errorLogger.logError(el);
						}else{
							IUMMessageData imd = mc.translatePolicyRequirementMaintenanceMessage(reply);
							ExceptionLog exLog = validateIUMMessageData(imd);
							if(exLog==null){
								// TODO: YVES call the update requirement
								prd.setABACUScreateDate(imd.getCreationDate());
								prd.setSequenceNumber(imd.getSequenceNumber());
							    PolicyRequirements pr = new PolicyRequirements();
							    pr.maintainRequirement(prd);

							}else{
								exLog.setRecordType(mc.TRANSACTION_REQUIREMENT_MAINTENANCE);
								exLog.setRequestMsg(message);
								exLog.setReply(reply.toString());
								exLog.setXmlRecord(reply.toString());
								exLog.setSystemInterface(IUMConstants.SYSTEM_ABACUS_DESC);
								ExceptionLogger errorLogger = new ExceptionLogger();
								errorLogger.logError(exLog);
							}
						}
						
					}
				}
			} catch (Exception e) {
				LOGGER.error(CodeHelper.getStackTrace(e));
				logError(e, mc.TRANSACTION_REQUIREMENT_MAINTENANCE);
			} finally {
				if (null != con) con.disconnect();
			}
		}
		LOGGER.info("processAcceptRequirement end");
	}


	/* (non-Javadoc)
	 * @see com.slocpi.ium.service.AdminSystem#processOrderRequirement(java.util.ArrayList)
	 */
	public void processOrderRequirement(ArrayList list) throws IUMInterfaceException{
		
		LOGGER.info("processOrderRequirement start");
		init();
		if(useThread.trim().equalsIgnoreCase("true")){
			Timer timer = new Timer();
			timer.schedule(new ProcessOrderRequirementThread(list, iumAbacusUserId), 1000);
		}else{
			MessageConverter mc = new MessageConverter();
			MessageConnector con = null; //Fix for CR#10146
			try {

				con = getConnector(mc.TRANSACTION_REQUIREMENT_MAINTENANCE);
				con.connect();

				if ((list != null) && (list.size() > 0)) {
					for (int i=0; i <list.size(); i++) {
						PolicyRequirementsData prd = (PolicyRequirementsData)list.get(i);
						String message = mc.generateOrderRequirementMessage(iumAbacusUserId,IUMMessageUtility.generateIUMMQkey(),prd);
						StringBuffer reply = new StringBuffer().append(con.putRequestMsg(message));
						if(reply==null){
							ExceptionLog el= new ExceptionLog();
							el.setRecordType(mc.TRANSACTION_REQUIREMENT_MAINTENANCE);
							el.setSystemInterface(IUMConstants.SYSTEM_ABACUS_DESC);
							el.setRequestMsg(message);
							el.setReferenceNumber(prd.getReferenceNumber());
							ArrayList arr = new ArrayList();
							arr.add(createExceptionDetail("MQ: No reply message available"));
							el.setDetails(arr);
							ExceptionLogger errorLogger = new ExceptionLogger();
							errorLogger.logError(el);
						}else{
							IUMMessageData imd = mc.translatePolicyRequirementMaintenanceMessage(reply);
							ExceptionLog exLog = validateIUMMessageData(imd);
							if(exLog==null){
								
								prd.setABACUScreateDate(imd.getCreationDate());
								prd.setSequenceNumber(imd.getSequenceNumber());
							    PolicyRequirements pr = new PolicyRequirements();
							    pr.maintainRequirement(prd);

							}else{
								exLog.setRecordType(mc.TRANSACTION_REQUIREMENT_MAINTENANCE);
								exLog.setRequestMsg(message);
								exLog.setReply(reply.toString());
								exLog.setXmlRecord(reply.toString());
								exLog.setSystemInterface(IUMConstants.SYSTEM_ABACUS_DESC);
								ExceptionLogger errorLogger = new ExceptionLogger();
								errorLogger.logError(exLog);
							}
						}

					}
				}
			} catch (Exception e) {
				LOGGER.error(CodeHelper.getStackTrace(e));
				logError(e, mc.TRANSACTION_REQUIREMENT_MAINTENANCE);
			} finally {
				if (null != con) con.disconnect();
			}
		}
		LOGGER.info("processOrderRequirement end");
	}


	/* (non-Javadoc)
	 * @see com.slocpi.ium.service.AdminSystem#processRejectRequirement(java.util.ArrayList)
	 */

	public void processRejectRequirement(ArrayList list) throws IUMInterfaceException{
		
		LOGGER.info("processRejectRequirement start");
		init();
		if(useThread.trim().equalsIgnoreCase("true")){
			Timer timer = new Timer();
			timer.schedule(new ProcessRejectRequirementThread(list, iumAbacusUserId), 1000);
		}else{
			MessageConverter mc = new MessageConverter();
			MessageConnector con = null; // Fix for CR#10146
			try {

				con = getConnector(mc.TRANSACTION_REQUIREMENT_MAINTENANCE);
				con.connect();

				if ((list != null) && (list.size() > 0)) {
					for (int i=0; i <list.size(); i++) {
						PolicyRequirementsData prd = (PolicyRequirementsData)list.get(i);
						String message = mc.generateUpdateRequirementMessage(iumAbacusUserId,IUMMessageUtility.generateIUMMQkey(),prd);
						StringBuffer reply = new StringBuffer().append(con.putRequestMsg(message));
						if(reply==null){
							ExceptionLog el= new ExceptionLog();
							el.setRecordType(mc.TRANSACTION_REQUIREMENT_MAINTENANCE);
							el.setSystemInterface(IUMConstants.SYSTEM_ABACUS_DESC);
							el.setRequestMsg(message);
							el.setReferenceNumber(prd.getReferenceNumber());
							ArrayList arr = new ArrayList();
							arr.add(createExceptionDetail("MQ: No reply message available"));
							el.setDetails(arr);
							ExceptionLogger errorLogger = new ExceptionLogger();
							errorLogger.logError(el);
						}else{
							IUMMessageData imd = mc.translatePolicyRequirementMaintenanceMessage(reply);
							ExceptionLog exLog = validateIUMMessageData(imd);
							if(exLog==null){
							
							}else{
								exLog.setRecordType(mc.TRANSACTION_REQUIREMENT_MAINTENANCE);
								exLog.setRequestMsg(message);
								exLog.setReply(reply.toString());
								exLog.setXmlRecord(reply.toString());
								exLog.setSystemInterface(IUMConstants.SYSTEM_ABACUS_DESC);
								ExceptionLogger errorLogger = new ExceptionLogger();
								errorLogger.logError(exLog);
							}
						}


					}
				}
			} catch (Exception e) {
				LOGGER.error(CodeHelper.getStackTrace(e));
				logError(e, mc.TRANSACTION_REQUIREMENT_MAINTENANCE);
			} finally {
				if (null != con) con.disconnect();
			}
		}
		LOGGER.info("processRejectRequirement end");
		
	}


	/* (non-Javadoc)
	 * @see com.slocpi.ium.service.AdminSystem#processCancelRequirement(java.util.ArrayList)
	 */
	public void processCancelRequirement(ArrayList list) throws IUMInterfaceException{
		
		LOGGER.info("processCancelRequirement start");
		init();
		if(useThread.trim().equalsIgnoreCase("true")){
			Timer timer = new Timer();
			timer.schedule(new ProcessCancelRequirementThread(list, iumAbacusUserId), 1000);
		}else{
			MessageConverter mc = new MessageConverter();
			MessageConnector con = null; //Fix for CR#10146
			try {
				con = getConnector(mc.TRANSACTION_REQUIREMENT_MAINTENANCE);
				con.connect();

				if ((list != null) && (list.size() > 0)) {
					for (int i=0; i <list.size(); i++) {
						PolicyRequirementsData prd = (PolicyRequirementsData)list.get(i);
						
						String message = mc.generateUpdateRequirementMessage(iumAbacusUserId,IUMMessageUtility.generateIUMMQkey(),prd);
						StringBuffer reply = new StringBuffer().append(con.putRequestMsg(message));
						if(reply==null){
							ExceptionLog el= new ExceptionLog();
							el.setRecordType(mc.TRANSACTION_REQUIREMENT_MAINTENANCE);
							el.setSystemInterface(IUMConstants.SYSTEM_ABACUS_DESC);
							el.setRequestMsg(message);
							el.setReferenceNumber(prd.getReferenceNumber());
							ArrayList arr = new ArrayList();
							arr.add(createExceptionDetail("MQ: No reply message available"));
							el.setDetails(arr);
							ExceptionLogger errorLogger = new ExceptionLogger();
							errorLogger.logError(el);
						}else{
							IUMMessageData imd = mc.translatePolicyRequirementMaintenanceMessage(reply);
							
							ExceptionLog exLog = validateIUMMessageData(imd);
							if(exLog==null){
								/*
								 * TODO verify do we update something in IUM?
								 * TODO dave Call the update methods to update abacus update date and by
								 */
							}else{
								exLog.setRecordType(mc.TRANSACTION_REQUIREMENT_MAINTENANCE);
								exLog.setRequestMsg(message);
								exLog.setReply(reply.toString());
								exLog.setXmlRecord(reply.toString());
								exLog.setSystemInterface(IUMConstants.SYSTEM_ABACUS_DESC);
								ExceptionLogger errorLogger = new ExceptionLogger();
								errorLogger.logError(exLog);
							}
						}

					}
				}
			} catch (Exception e) {
				LOGGER.error(CodeHelper.getStackTrace(e));
				logError(e, mc.TRANSACTION_REQUIREMENT_MAINTENANCE);
			} finally {
				if (null != con) con.disconnect();
			}
		}
		LOGGER.info("processCancelRequirement end");
	}

	/* (non-Javadoc)
	 * @see com.slocpi.ium.service.AdminSystem#processWaiveRequirement(java.util.ArrayList)
	 */
	public void processWaiveRequirement(ArrayList list) throws IUMInterfaceException{
		
		LOGGER.info("processWaiveRequirement start");
		init();
		if(useThread.trim().equalsIgnoreCase("true")){
			Timer timer = new Timer();
			timer.schedule(new ProcessWaiveRequirementThread(list, iumAbacusUserId), 1000);
		}else{
			MessageConverter mc = new MessageConverter();
			MessageConnector con = null; //Fix for CR#10146
			try {

				con = getConnector(mc.TRANSACTION_REQUIREMENT_MAINTENANCE);
				con.connect();

				if ((list != null) && (list.size() > 0)) {
					for (int i=0; i <list.size(); i++) {
						PolicyRequirementsData prd = (PolicyRequirementsData)list.get(i);
						String message = mc.generateUpdateRequirementMessage(iumAbacusUserId,IUMMessageUtility.generateIUMMQkey(),prd);
						StringBuffer reply = new StringBuffer().append(con.putRequestMsg(message));
						if(reply==null){
							ExceptionLog el= new ExceptionLog();
							el.setRecordType(mc.TRANSACTION_REQUIREMENT_MAINTENANCE);
							el.setSystemInterface(IUMConstants.SYSTEM_ABACUS_DESC);
							el.setRequestMsg(message);
							el.setReferenceNumber(prd.getReferenceNumber());
							ArrayList arr = new ArrayList();
							arr.add(createExceptionDetail("MQ: No reply message available"));
							el.setDetails(arr);
							ExceptionLogger errorLogger = new ExceptionLogger();
							errorLogger.logError(el);
						}else{
							IUMMessageData imd = mc.translatePolicyRequirementMaintenanceMessage(reply);
							ExceptionLog exLog = validateIUMMessageData(imd);
							if(exLog==null){
								/*
								 * TODO verify do we update something in IUM?
								 * TODO dave Call the update methods to update abacus update date and by
								 */
							}else{
								exLog.setRecordType(mc.TRANSACTION_REQUIREMENT_MAINTENANCE);
								exLog.setRequestMsg(message);
								exLog.setReply(reply.toString());
								exLog.setXmlRecord(reply.toString());
								exLog.setSystemInterface(IUMConstants.SYSTEM_ABACUS_DESC);
								ExceptionLogger errorLogger = new ExceptionLogger();
								errorLogger.logError(exLog);
							}
						}
					}
				}
			} catch (Exception e) {
				LOGGER.error(CodeHelper.getStackTrace(e));
				logError(e,mc.TRANSACTION_REQUIREMENT_MAINTENANCE);
			} finally {
				if (null != con) con.disconnect();
			}
		}
		LOGGER.info("processWaiveRequirement end");
	}


	/* (non-Javadoc)
	 * @see com.slocpi.ium.service.AdminSystem#processUpdateRequirement(com.slocpi.ium.data.PolicyRequirementsData)
	 */
	public void processUpdateRequirement(PolicyRequirementsData prd) throws IUMInterfaceException{
		
		LOGGER.info("processUpdateRequirement start");
		init();
		if(useThread.trim().equalsIgnoreCase("true")){
			Timer timer = new Timer();
			timer.schedule(new ProcessUpdateRequirementThread(prd, iumAbacusUserId), 1000);
		}else{
			MessageConverter mc = new MessageConverter();
			MessageConnector con = null; //Fix for CR#10146
			try {

				con = getConnector(mc.TRANSACTION_REQUIREMENT_MAINTENANCE);
				con.connect();
				String message = mc.generateUpdateRequirementMessage(iumAbacusUserId,IUMMessageUtility.generateIUMMQkey(),prd);
				StringBuffer reply = new StringBuffer().append(con.putRequestMsg(message));
				
				if(reply==null){
					ExceptionLog el= new ExceptionLog();
					el.setRecordType(mc.TRANSACTION_REQUIREMENT_MAINTENANCE);
					el.setSystemInterface(IUMConstants.SYSTEM_ABACUS_DESC);
					el.setRequestMsg(message);
					ArrayList arr = new ArrayList();
					arr.add(createExceptionDetail("MQ: No reply message available"));
					el.setDetails(arr);
					ExceptionLogger errorLogger = new ExceptionLogger();
					errorLogger.logError(el);
				}else{
					IUMMessageData imd =  mc.translatePolicyRequirementMaintenanceMessage(reply);
					ExceptionLog exLog = validateIUMMessageData(imd);
					
					if(exLog==null){
					   prd.setABACUScreateDate(imd.getCreationDate());
					   prd.setSequenceNumber(imd.getSequenceNumber());
					
					  PolicyRequirements pr = new PolicyRequirements();
					  pr.maintainRequirement(prd);

					}else{
					   exLog.setRecordType(mc.TRANSACTION_REQUIREMENT_MAINTENANCE);
					   exLog.setRequestMsg(message);
					   exLog.setReply(reply.toString());
					   exLog.setXmlRecord(reply.toString());
					   exLog.setSystemInterface(IUMConstants.SYSTEM_ABACUS_DESC);
					   ExceptionLogger errorLogger = new ExceptionLogger();
					   errorLogger.logError(exLog);
					}
				}
			} catch (Exception e) {
				LOGGER.error(CodeHelper.getStackTrace(e));
				logError(e, mc.TRANSACTION_REQUIREMENT_MAINTENANCE);
			} finally {
				if (null != con) con.disconnect();
			}
		}
		LOGGER.info("processUpdateRequirement end");
	}



	private void setAbacusSpecificDetails(AssessmentRequestData ard) {

		LOGGER.info("setAbacusSpecificDetails start");
		StatusData sd = new StatusData();
		sd.setStatusId(IUMConstants.STATUS_NB_REVIEW_ACTION);
		LOBData lob = new LOBData();
		lob.setLOBCode(IUMConstants.LOB_INDIVIDUAL_LIFE);

		ard.setStatus(sd);
		ard.setLob(lob);
		ard.setSourceSystem(IUMConstants.SYSTEM_ABACUS);
		ard.setStatusDate(new Date());
		ard.setCreatedDate(new Date());

	
		UserProfileData upd = new UserProfileData();
		upd.setUserId(IUMConstants.USER_IUM);
		ard.setCreatedBy(upd);
		
		ard.setFolderLocation(ard.getAssignedTo());
		LOGGER.info("setAbacusSpecificDetails end");
	}


	private ExceptionLog validateAutoSettleAR(IUMMessageData imd){
		
		LOGGER.info("validateAutoSettleAR start");
		ExceptionLog exLog = null;
		ArrayList details = new ArrayList();
		if(imd!=null && imd.getConfirmationCode()!=null){
			if(imd.getConfirmationCode().trim().equals("00")){
				//successful
			}else if(imd.getConfirmationCode().trim().equals("01")){
				details.add(createExceptionDetail("Record not found"));
			}else if(imd.getConfirmationCode().trim().equals("02")){
				details.add(createExceptionDetail("Error in NB contact"));
			}else if(imd.getConfirmationCode().trim().equals("03")){
				details.add(createExceptionDetail("Policy status not equal to PCCU"));
			}else if(imd.getConfirmationCode().trim().equals("04")){
				 details.add(createExceptionDetail("Unsuccessful SCCP"));
			}else if(imd.getConfirmationCode().trim().equals("05")){
				 details.add(createExceptionDetail("Unsuccessful UWDE"));
			}else if(imd.getConfirmationCode().trim().equals("06")){
				 details.add(createExceptionDetail("Invalid transaction"));
			}else{
				details.add(createExceptionDetail("Invalid confirmation code"));
			}
			if(details.size()>0){
				exLog = new ExceptionLog();
				exLog.setDetails(details);
			}
		}
		LOGGER.info("validateAutoSettleAR end");
		return exLog;
	}

	private ExceptionLog validateKOMessages(KickOutMessageData koData){
		
		LOGGER.info("validateKOMessages start");
		ExceptionLog exLog = null;
		ArrayList details = new ArrayList();
		AssessmentRequest ar = new AssessmentRequest();
		if (koData != null){
			if (koData.getSequenceNumber()==0){
				details.add(createExceptionDetail("Invalid sequence number"));
			}
			if (koData.getReferenceNumber() == null || koData.getReferenceNumber().trim().equals("")){
				details.add(createExceptionDetail("Reference number is null or empty."));
			} else {
				boolean existingRequest = false;
				try {
					existingRequest = ar.isAssessmentRequestExist(koData.getReferenceNumber());
				} catch (IUMException e) {
					details.add(createExceptionDetail(e.getMessage()));
				}
				if (!existingRequest){
					details.add(createExceptionDetail("Non-existing reference number"));
				}
			}
			
		}

		if (details.size()>0){
			exLog = new ExceptionLog();
			exLog.setDetails(details);
		}
		LOGGER.info("validateKOMessages end");
		return exLog;
	}

	private ExceptionLog validateClientInfo(ClientData clData){
		
		LOGGER.info("validateClientInfo start");
		ExceptionLog exLog = null;
		ArrayList details = new ArrayList();
		if(clData != null){
			if (clData.getAge() < 0){
				details.add(createExceptionDetail("Invalid value for age"));
			}
		}

		if (details.size()>0){
			exLog = new ExceptionLog();
			exLog.setDetails(details);
		}
		LOGGER.info("validateClientInfo end");
		return exLog;
	}

	private ExceptionLog validateSummaryPolicyCoverage(CDSSummaryPolicyCoverageData cdsSummary){
		
		LOGGER.info("validateSummaryPolicyCoverage start");
		ExceptionLog exLog = null;
		ArrayList details = new ArrayList();
		if (cdsSummary != null){
			if (cdsSummary.getTotalReinsuredAmount() < 0){
				details.add(createExceptionDetail("Invalid value for total reinsured amount"));
			}
			if (cdsSummary.getTotalFBBMBCoverage() < 0){
				details.add(createExceptionDetail("Invalid value for total FBBMB coverage"));
			}
			if (cdsSummary.getTotalHIBCoverage() < 0){
				details.add(createExceptionDetail("Invalid value for total HIB coverage"));
			}
			if (cdsSummary.getTotalAPDBCoverage() < 0){
				details.add(createExceptionDetail("Invalid value for total APDB coverage"));
			}
			if (cdsSummary.getTotalCCRCoverage() < 0){
				details.add(createExceptionDetail("Invalid value for total CCR coverage"));
			}
			if (cdsSummary.getOtherCompaniesAd() < 0){
				details.add(createExceptionDetail("Invalid value for Other Companies Ad"));
			}
			if (cdsSummary.getOtherCompaniesBr() < 0){
				details.add(createExceptionDetail("Invalid value for Other Companies Br"));
			}
			if (cdsSummary.getExistingInForceAd() < 0){
				details.add(createExceptionDetail("Invalid value for Existing In Force Ad"));
			}
			if (cdsSummary.getExistingInForceBr() < 0){
				details.add(createExceptionDetail("Invalid value for Existing In Force Br"));
			}
			if (cdsSummary.getExistingLapsedAd() < 0){
				details.add(createExceptionDetail("Invalid value for Existing Lapsed Ad"));
			}
			if (cdsSummary.getExistingLapsedBr() < 0){
				details.add(createExceptionDetail("Invalid value for Existing Lapsed Br"));
			}
			if (cdsSummary.getPendingAmountAd() < 0){
				details.add(createExceptionDetail("Invalid value for Pending Amount Ad"));
			}
			if (cdsSummary.getPendingAmountBr() < 0){
				details.add(createExceptionDetail("Invalid value for Pending Amount Br"));
			}
			if (cdsSummary.getAppliedForAd() < 0){
				details.add(createExceptionDetail("Invalid value for Pending Amount Ad"));
			}
			if (cdsSummary.getAppliedForBr() < 0){
				details.add(createExceptionDetail("Invalid value for Pending Amount Br"));
			}
		}

		if (details.size() > 0){
			exLog = new ExceptionLog();
			exLog.setDetails(details);
		}
		LOGGER.info("validateSummaryPolicyCoverage end");
		return exLog;
	}

	private ExceptionLog validatePolicyCoverageDetails(PolicyDetailData policyData){
		
		LOGGER.info("validatePolicyCoverageDetails start");
		ExceptionLog exLog = null;
		ArrayList details = new ArrayList();
		AssessmentRequest ar = new AssessmentRequest();
		if (policyData != null){
			if (policyData.getPolicyNumber() == null || policyData.getPolicyNumber().equals("")){
				details.add(createExceptionDetail("Policy number is null or empty"));
			}
			boolean policyExist = false;
			try {
				policyExist = ar.isAssessmentRequestExist(policyData.getReferenceNumber());
				if (!policyExist){
					details.add(createExceptionDetail("Non-existing reference number"));
				}
			} catch (IUMException e) {
				LOGGER.error(CodeHelper.getStackTrace(e));
				details.add(createExceptionDetail(e.getMessage()));
			}
			if (policyData.getFaceAmount() < 0){
				details.add(createExceptionDetail("Invalid value for Face Amount"));
			}
			if (policyData.getCoverageNumber() < 0){
				details.add(createExceptionDetail("Invalid value for Coverage Number"));
			}
			if (policyData.getADBFaceAmount() < 0){
				details.add(createExceptionDetail("Invalid value for ADB Face Amount"));
			}
			if (policyData.getADMultiplier() < 0){
				details.add(createExceptionDetail("Invalid value for AD Multiplier"));
			}
			if (policyData.getWPMultiplier() < 0){
				details.add(createExceptionDetail("Invalid value for WP Multiplier"));
			}
			if (policyData.getReinsuredAmount() < 0){
				details.add(createExceptionDetail("Invalid value for Reinsured Amount"));
			}
		}

		if (details.size() > 0){
			exLog = new ExceptionLog();
			exLog.setDetails(details);
		}
		LOGGER.info("validatePolicyCoverageDetails end");
		return exLog;
	}




	private ExceptionLog validateARD(AssessmentRequestData ard){
		
		LOGGER.info("validateARD start");
		ExceptionLog exLog = null;
		ArrayList details = new ArrayList();
		CodeHelper ch = new CodeHelper();
		if(ard.getReferenceNumber()==null || ard.getReferenceNumber().trim().equals("")){
			details.add(createExceptionDetail("Invalid reference number"));
		}

		if(ard.getAmountCovered()<0){
			details.add(createExceptionDetail("Invalid amount covered"));
		}
		if(ard.getPremium()<0){
			details.add(createExceptionDetail("Invalid premium amount"));
		}

		if(ard.getApplicationReceivedDate()==null){
			details.add(createExceptionDetail("Invalid application received date"));
		}

		try{
			if (ard.getAssignedTo() != null){
				UserProfileData at = ch.getUserProfileByACF2ID(ard.getAssignedTo().getACF2ID());
				if(at==null){
					details.add(createExceptionDetail("Invalid value for NB_CNTCT_USER_ID"));
				}else{
					ard.setAssignedTo(at);
					ard.setFolderLocation(at);
				}
			}
		}catch(Exception e){
				details.add(createExceptionDetail(e.getMessage()));
		}

		try{
			UserProfileData uw = ch.getUserProfileByACF2ID(ard.getUnderwriter().getACF2ID());
			if(uw==null){
				//details.add(createExceptionDetail("Underwriter not found in IUM"));
			}else{
				ard.setUnderwriter(uw);
			}
		}catch(SQLException e){
			LOGGER.error(CodeHelper.getStackTrace(e));
		}
//		*/
		try{
			UserProfileData agt = ch.getUserProfile(ard.getAgent().getUserId());
			if(agt==null){
				details.add(createExceptionDetail("Agent not found in IUM"));
			}else{
				ard.setAgent(agt);
			}
		}catch(SQLException e){
			LOGGER.error(CodeHelper.getStackTrace(e));
			details.add(createExceptionDetail(e.getMessage()));
		}


		try{
			SunLifeOfficeData sod = ch.getSunLifeOffice(ard.getBranch().getOfficeId());
			if(sod==null){
				details.add(createExceptionDetail("SunLife office not defined in IUM"));
			}else{
				ard.setBranch(sod);
			}
		}catch(SQLException e){
			LOGGER.error(CodeHelper.getStackTrace(e));
			details.add(createExceptionDetail(e.getMessage()));
		}
		if (details.size()>0){
			exLog = new ExceptionLog();
			exLog.setDetails(details);
		}
		LOGGER.info("validateARD end");
		return exLog;
	}

	private ExceptionLog validateIUMMessageData(IUMMessageData imd){
		
		LOGGER.info("validateIUMMessageData start");
		ExceptionLog exLog = null;
		ArrayList details = new ArrayList();
		if(imd.getConfirmationCode()==null || imd.getConfirmationCode().trim().equals("")){
			details.add(createExceptionDetail("Invalid confirmation code"));
		}
		if(imd.getConfirmationCode()==null || imd.getConfirmationCode().trim().equals("01")){
			return null;
		}
		if(imd.getPolicyClientCode()==null || imd.getPolicyClientCode().trim().equals("")){
			details.add(createExceptionDetail("Invalid policy client code"));
		}
		if(imd.getPolicyClientId()==null || imd.getPolicyClientId().trim().equals("")){
			details.add(createExceptionDetail("Invalid policy client id"));
		}
		if(imd.getRequirementId()==null || imd.getRequirementId().trim().equals("")){
			details.add(createExceptionDetail("Invalid requirment id"));
		}
		if(imd.getSequenceNumber()<0){
			details.add(createExceptionDetail("Invalid sequence number"));
		}
		
		if (details.size()>0){
			exLog = new ExceptionLog();
			exLog.setDetails(details);
		}
		LOGGER.info("validateIUMMessageData end");
		return exLog;
	}


	private ExceptionLog validateMortalityRating(CDSMortalityRatingData mortalityData){
		
		LOGGER.info("validateMortalityRating start");
		ExceptionLog exLog = null;
		ArrayList details = new ArrayList();

		if (mortalityData != null){
			if (mortalityData.getHeightInFeet() < 0 || mortalityData.getHeightInInches() < 0){
				details.add(createExceptionDetail("Invalid value for height"));
			}
			if (mortalityData.getWeight() < 0){
				details.add(createExceptionDetail("Invalid value for weight"));
			}
			if (mortalityData.getAPDB() < 0){
				details.add(createExceptionDetail("Invalid value for APDB"));
			}
			if (mortalityData.getCCR() < 0){
				details.add(createExceptionDetail("Invalid value for CCR"));
			}
			if (mortalityData.getBasic() < 0){
				details.add(createExceptionDetail("Invalid value for Basic"));
			}
		}

		if (details.size() > 0){
			exLog = new ExceptionLog();
			exLog.setDetails(details);
		}
		LOGGER.info("validateMortalityRating end");
		return exLog;
	}

	private ExceptionLog validatePolicyRequiremetns(PolicyRequirementsData reqData){
		
		LOGGER.info("validatePolicyRequiremetns start");
		ExceptionLog exLog = null;
		ArrayList details = new ArrayList();
		AssessmentRequest ar = new AssessmentRequest();
		RequirementDAO reqdao = new RequirementDAO();
		
		if (reqData != null){
			if(reqData.getLevel().equalsIgnoreCase(IUMConstants.LEVEL_POLICY)){
				if (reqData.getReferenceNumber() == null || reqData.getReferenceNumber().trim().equals("")){
					details.add(createExceptionDetail("Reference number is null or empty"));
				} else {
					boolean policyExist = false;
					try {
						policyExist = ar.isAssessmentRequestExist(reqData.getReferenceNumber());
						if (!policyExist){
							details.add(createExceptionDetail("Non-existing reference number"));
						}
					} catch (IUMException e) {
						LOGGER.error(CodeHelper.getStackTrace(e));
						details.add(createExceptionDetail(e.getMessage()));
					}

				}
			}else{
				
				if (reqData.getClientType() == null){
					details.add(createExceptionDetail("Client Type is null or empty"));
				} else {
					if (reqData.getClientType() == IUMConstants.CLIENT_TYPE_INSURED){
						if (reqData.getClientId() != null && !reqData.getClientId().trim().equals("")){
							ClientDAO clDao = new ClientDAO();
							ClientData clData = null;
							try {
								clData = clDao.retrieveClient(reqData.getClientId());
								if (clData == null){
									details.add(createExceptionDetail("Non-existing client"));
								}
							} catch (SQLException e) {
								LOGGER.error(CodeHelper.getStackTrace(e));
								details.add(createExceptionDetail(e.getMessage()));
							} 
						}
					}
				}
			}

			if (reqData.getRequirementCode() != null && !reqData.getRequirementCode().trim().equals("")){
				RequirementData requirement = null;
				try {
					requirement = reqdao.retrieveRequirement(reqData.getRequirementCode());
					if (requirement==null){
						details.add(createExceptionDetail("Non-existing requirement code"));
					}
				} catch (SQLException e) {
					LOGGER.error(CodeHelper.getStackTrace(e));
					details.add(createExceptionDetail(e.getMessage()));
				}
			}

			StatusData status = reqData.getStatus();
			if (status != null){
				
				if (status.getStatusId() != 0){
					
					StatusCHDao statDao = new StatusCHDao();
					boolean isStatusExist = false;
					
					try {
						isStatusExist = statDao.isStatusExist(reqData.getStatus().getStatusId(), IUMConstants.STAT_TYPE_NM);
						if (!isStatusExist){
							details.add(createExceptionDetail("Non-existing status"));
						}
					} catch (SQLException e) {
						LOGGER.error(CodeHelper.getStackTrace(e));
						details.add(createExceptionDetail(e.getMessage()));
					} 
				}
			} else {
				details.add(createExceptionDetail("Status is null or empty"));
			}
			
			if (reqData.getSequenceNumber() < 0){
				details.add(createExceptionDetail("Invalid value for sequence number"));
			}
		}

		if (details.size() > 0){
			exLog = new ExceptionLog();
			exLog.setDetails(details);
		}
		
		LOGGER.info("validatePolicyRequiremetns end");
		return exLog;
	}

	private void logError(Exception e, String trans) {
		
		LOGGER.info("logError start");
		ExceptionLog el= new ExceptionLog();
		MessageConverter mc = new MessageConverter();
		el.setRecordType(trans);
		el.setSystemInterface(IUMConstants.SYSTEM_ABACUS_DESC);
		ArrayList arr = new ArrayList();
		arr.add(createExceptionDetail(e.getMessage()));
		el.setDetails(arr);
		ExceptionLogger errorLogger = new ExceptionLogger();
		errorLogger.logError(el);
		LOGGER.info("logError end");
	}

	

	private MessageConnector getConnector(String transactionId) throws SLMQInitException {
		return new MessageConnector(transactionId);
	}

	private ExceptionDetail createExceptionDetail(String message){
		ExceptionDetail ed = new ExceptionDetail();
		ed.setMessage(message);
		return ed;
	}

	private void init() throws IUMInterfaceException{
		
		LOGGER.info("init start");
		try{
			ResourceBundle rb = ResourceBundle.getBundle(IUMConstants.IUM_CONFIG);
			iumAbacusUserId = rb.getString(IUMConstants.ABACUS_USER);
			useThread = rb.getString("usethreads");
		}catch(MissingResourceException e){
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMInterfaceException(e.getMessage());
		}
		LOGGER.info("init end");
	}


	public boolean retrieveCDSDetails(String referenceNumber, String suffix) throws IUMInterfaceException {
		return false;
	}


	public HashMap retrieveIngeniumConsolidatedInformation(String referenceNumber, String suffix) throws IUMInterfaceException {
		return null;
	}
}
