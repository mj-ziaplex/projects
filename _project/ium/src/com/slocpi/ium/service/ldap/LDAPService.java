package com.slocpi.ium.service.ldap;

import java.util.Hashtable;
import java.util.ResourceBundle;
import javax.naming.AuthenticationException;
import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attributes;
import javax.naming.directory.BasicAttribute;
import javax.naming.directory.BasicAttributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.slocpi.ium.interfaces.IUMInterfaceException;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.IUMConstants;

public class LDAPService {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(LDAPService.class);
	private String providerUrl;
	private String securityAuth;
	private String domain;
	private String cntxt;
	private String attrKey;
	private Hashtable env = null;
	
	private static class LDAPServiceHolder {
		private static LDAPService ldapInstance = new LDAPService();
	}

	public static LDAPService getInstance() {
		return LDAPServiceHolder.ldapInstance;
	}

	private LDAPService() {
		super();
		init();
	}

	private void init() {
		
		LOGGER.info("init start");
		ResourceBundle rb = ResourceBundle
				.getBundle(IUMConstants.IUM_LDAP_CONFIG);
		providerUrl = rb.getString(IUMConstants.PROVIDER_URL);
		securityAuth = rb.getString(IUMConstants.SECURITY_AUTHENTHICATION);
		domain = rb.getString(IUMConstants.LDAP_DOMAIN) + "\\";
		cntxt = rb.getString(IUMConstants.LDAP_CNTXT);

		attrKey = rb.getString(IUMConstants.LDAP_ATTR_KEY);
		if (env == null) {
			env = new Hashtable();
			env.put(Context.INITIAL_CONTEXT_FACTORY,
					"com.sun.jndi.ldap.LdapCtxFactory");
			env.put(Context.PROVIDER_URL, providerUrl);
			env.put(Context.SECURITY_AUTHENTICATION, securityAuth); 
		}
		LOGGER.info("init end");
	}

	public boolean isValidUser(String userName, String password)
			throws IUMInterfaceException {
		
		LOGGER.info("isValidUser start");
		boolean isValid = false;
		DirContext ctx = null;
		env.put(Context.SECURITY_PRINCIPAL, domain + userName); 
		env.put(Context.SECURITY_CREDENTIALS, password); 
	
		try {
			ctx = new InitialDirContext(env);
			isValid = true;
		} catch (AuthenticationException ae) {
			isValid = false;
			LOGGER.error(CodeHelper.getStackTrace(ae));
		} catch (NamingException ne) {
			LOGGER.error(CodeHelper.getStackTrace(ne));
		} catch (Exception ne) {
			LOGGER.error(CodeHelper.getStackTrace(ne));
		} finally {
			if (ctx != null) {
				try {
					ctx.close();
				} catch (NamingException e) {
					LOGGER.error(CodeHelper.getStackTrace(e));
				}
			}
		}
		
		LOGGER.info("isValidUser end");
		return isValid;
	}

	public boolean isLdapUser(String username) throws IUMInterfaceException {

		LOGGER.info("isLdapUser start");
		boolean isValid = false;
		DirContext ctx = null;

		try {
			ctx = new InitialDirContext(env);

			Attributes matchAttrs = new BasicAttributes(true);
			matchAttrs.put(new BasicAttribute(attrKey, username));
			NamingEnumeration answer = ctx.search(cntxt, matchAttrs);
			if (answer.hasMore()) {
				isValid = true;
			}
		} catch (AuthenticationException ae) {
			isValid = false;
			LOGGER.error(CodeHelper.getStackTrace(ae));
		} catch (NamingException ne) {
			LOGGER.error(CodeHelper.getStackTrace(ne));
		} catch (Exception ne) {
			LOGGER.error(CodeHelper.getStackTrace(ne));
		} finally {
			if (ctx != null) {
				try {
					ctx.close();
				} catch (NamingException e) {
					LOGGER.error(CodeHelper.getStackTrace(e));
				}
			}
		}
		LOGGER.debug("LDAP user: " + isValid);
		LOGGER.info("isLdapUser start");
		return isValid;
	}


}
