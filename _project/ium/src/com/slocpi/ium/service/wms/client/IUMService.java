/**
 * IUMService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package com.slocpi.ium.service.wms.client;

public interface IUMService extends java.rmi.Remote {
    public java.lang.String matchDocument(java.lang.String xmlInput) throws java.rmi.RemoteException;
    public java.lang.String unmatchDocument(java.lang.String xmlInput) throws java.rmi.RemoteException;
    public java.lang.String createCDS(java.lang.String xmlInput) throws java.rmi.RemoteException;
    public java.lang.String retrieveKOandReqts(java.lang.String xmlInput) throws java.rmi.RemoteException;
}
