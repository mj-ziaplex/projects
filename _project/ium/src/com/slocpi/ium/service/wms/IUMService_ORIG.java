package com.slocpi.ium.service.wms;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.slocpi.ium.service.wms.transaction.ARTransaction;
import com.slocpi.ium.service.wms.transaction.CreateCDSTransaction;
import com.slocpi.ium.service.wms.transaction.DocumentMatchTransaction;
import com.slocpi.ium.service.wms.transaction.DocumentUnmatchTransaction;

public class IUMService_ORIG implements IUMServiceInterface{
	private static final Logger LOGGER = LoggerFactory.getLogger(IUMService_ORIG.class);
	
	public String retrieveKOandReqts(String xmlInput){	
		LOGGER.info("retrieveKOandReqts start");
		ARTransaction transaction = new ARTransaction();
		String retrieveKOandReqts = transaction.retrieveKOandReqts(xmlInput);
		LOGGER.info("retrieveKOandReqts end");
		return retrieveKOandReqts;
	}
	
	public String matchDocument(String xmlInput){	
		LOGGER.info("matchDocument start");
		DocumentMatchTransaction transaction = new DocumentMatchTransaction();
		LOGGER.info("matchDocument end");
		return transaction.matchDocument(xmlInput);
	}
	
	public String unmatchDocument(String xmlInput){	
		LOGGER.info("unmatchDocument start");
		DocumentUnmatchTransaction transaction = new DocumentUnmatchTransaction();
		LOGGER.info("unmatchDocument end");
		return transaction.unmatchDocument(xmlInput);
	}
	
	public String createCDS(String xmlInput){
		LOGGER.info("createCDS start");
		CreateCDSTransaction transaction = new CreateCDSTransaction();
		LOGGER.info("createCDS end");
		return transaction.createCDS(xmlInput);
	}
}
