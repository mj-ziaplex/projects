package com.slocpi.ium.service.wms.transaction;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.slocpi.ium.service.ingenium.IngeniumGateway;
import com.slocpi.ium.service.wms.transaction.request.CreateCDSTransactionRequest;
import com.slocpi.ium.service.wms.transaction.response.CreateCDSTransactionResponse;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.UserManager;

public class CreateCDSTransaction {
	private static final Logger LOGGER = LoggerFactory.getLogger(CreateCDSTransaction.class);
	private String errMsg = "";
	private boolean result;
	
	public String createCDS(String xmlInput){
		
		LOGGER.info("createCDS start");
		String response = "";
		this.result = false;
		
			try{
				CreateCDSTransactionRequest request = new CreateCDSTransactionRequest();
				request.parseXML(xmlInput);
				UserManager um = new UserManager();
				if (um.isValidUser(request.getUsrId(),request.getPswd())) {
					if(request.getPolicyNumber() != null && !request.getPolicyNumber().equals("")){
						IngeniumGateway ingenium = new IngeniumGateway();
						result = ingenium.retrieveCDSDetails(request.getPolicyNumber(),request.getPolicySuffix());
					}else{
						this.errMsg = "Policy Number is missing."; 
					}				
				}else{
					this.errMsg = "User is invalid.";
				}
							
			}catch(Exception e){
				this.errMsg = e.getMessage();
				this.result = false;
				LOGGER.error(CodeHelper.getStackTrace(e));
			}
					
		CreateCDSTransactionResponse resp = new CreateCDSTransactionResponse(result,this.errMsg);
		resp.buildResponse();
		response = resp.getXmlResponse();
		
		LOGGER.info("createCDS end");
	return response;
	}
}
