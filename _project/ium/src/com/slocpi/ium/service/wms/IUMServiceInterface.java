package com.slocpi.ium.service.wms;

public interface IUMServiceInterface {
	public String retrieveKOandReqts(String refNum);
	public String matchDocument(String xmlInput);
	public String unmatchDocument(String xmlInput);
	public String createCDS(String xmlInput);
}
