package com.slocpi.ium.service.wms.test;

import java.rmi.RemoteException;

import javax.xml.rpc.ServiceException;

import com.slocpi.ium.service.wms.client.IUMService;
import com.slocpi.ium.service.wms.client.IUMServiceServiceLocator;

public class IUMServiceTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
IUMServiceServiceLocator service = new IUMServiceServiceLocator();
		
		IUMService ium = null;
		try {
			ium = service.getIUMService();
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
						

		String xml3 = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
			+ "<IUM xmlns=\"http://ACORD.org/Standards/Life/2\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\">" 
			+ "<IUMRequest>"
			+ "<AuthenticationInfo>"
			+ "<LoginName>PA79</LoginName>"
			+ "<Password>personal987</Password>"
			+ "</AuthenticationInfo>"
			+ "<RequestType>CreateUpdateRequirement</RequestType>"
			+ "<ReqtInfo>" 
			+ "<PolicyID>0500058059</PolicyID>" 
			+ "<ClientID></ClientID>" 
			+ "<IUMReqtID>110695</IUMReqtID>" 
			+ "<ReqtLvlInd></ReqtLvlInd>" 
			+ "<ReqtTypeId></ReqtTypeId>"
			+ "<ImageReferenceNo>{39BE4038-E60E-4363-BF6B-E4858F37BC76}</ImageReferenceNo>" 
			+ "<ReqtStatus>RAC</ReqtStatus>" 
			+ "<ScanDate>2/1/2007</ScanDate>"
			+ "<TestDate>5/21/2007</TestDate>"
			+ "</ReqtInfo>" 
			+ "</IUMRequest>"
			+ "</IUM>";
		
		String xml4 = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
			+	"<IUM xmlns=\"http://ACORD.org/Standards/Life/2\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\">" 
			+	"<IUMRequest>"
			+	"<AuthenticationInfo>"
			+	"<LoginName>WUDX1</LoginName>"
			+	"<Password>workflow2</Password>"
			+	"</AuthenticationInfo>"
			+	"<RequestType>UnmatchRequirement</RequestType>"
			+	"<ReqtInfo>"				
			+	"<IUMReqtID>108107</IUMReqtID>"	
			+	"</ReqtInfo>"				
			+	"</IUMRequest>"
			+	"</IUM>";
		
		String xml5 = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
			+	"<IUM xmlns=\"http://ACORD.org/Standards/Life/2\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\">" 
			+	"<IUMRequest>"
			+	"<AuthenticationInfo>"
			+	"<LoginName>WU11</LoginName>"
			+	"<Password>workflow2</Password>"
			+	"</AuthenticationInfo>"
			+	"<RequestType>UnmatchRequirement</RequestType>"
			+	"<ReqtInfo>"				
			+	"<PolicyID>0500058059</PolicyID>"
			+	"<ImageReferenceNo>{39BE4038-E60E-4363-BF6B-E4858F37BC76}</ImageReferenceNo>"
			+	"<ReqtTypeId>SPA</ReqtTypeId>"
			+	"</ReqtInfo>"
			+	"</IUMRequest>"
			+	"</IUM>";
		//0825030927

		String xml1 = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
		+	"<IUM xmlns=\"http://ACORD.org/Standards/Life/2\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\">" 
		+	"<IUMRequest>"
		+	"<AuthenticationInfo>"
		+	"<LoginName>pd57</LoginName>"
		+	"<Password>sunlife10</Password>"
		+	"</AuthenticationInfo>"
		+	"<RequestType>CreateUpdatePolicyRecord</RequestType>"
		+	"<PolicyInfo>"
		+	"<PolicyID>0825035600</PolicyID>"
		+	"</PolicyInfo>"
		+	"</IUMRequest>"
		+	"</IUM>"; 

		String xmlCDS = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
			+	"<IUM xmlns=\"http://ACORD.org/Standards/Life/2\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\">" 
			+	"<IUMRequest>"
			+	"<AuthenticationInfo>"
			+	"<LoginName>WUDX1</LoginName>"
			+	"<Password>workflow2</Password>"
			+	"</AuthenticationInfo>"
			+	"<RequestType>CreateCDS</RequestType>"
			+	"<PolicyInfo>"
			+	"<PolicyID>0515126160</PolicyID>"
			+	"</PolicyInfo>"
			+	"</IUMRequest>"
			+	"</IUM>";
	}
}
