package com.slocpi.ium.service.wms.transaction.request;


public interface Request {
	public void parseXML(String xmlInput) throws Exception;
}
