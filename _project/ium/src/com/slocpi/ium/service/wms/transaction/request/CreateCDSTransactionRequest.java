package com.slocpi.ium.service.wms.transaction.request;

import java.io.IOException;
import java.io.StringReader;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;

public class CreateCDSTransactionRequest implements Request{
	protected Document doc;

	private static final Logger LOGGER = LoggerFactory.getLogger(CreateCDSTransactionRequest.class);

	private String policyNumber;

	private String usrId;

	private String pswd;

	private String requestType;
	
	private String policySuffix;

	public void parseXML(String xml) throws Exception {
		
		LOGGER.info("parseXML start");
		createDocFromXML(xml);	
		
		Element root = doc.getRootElement();
		List children = root.getChildren();

		Element request = (Element) children.get(0); 
		List reqList = request.getChildren();

		Element auth = (Element) reqList.get(0); 
		List authList = auth.getChildren();

		Element usrId = (Element) authList.get(0); 
		this.usrId = usrId.getText();

		Element pswd = (Element) authList.get(1);
		this.pswd = pswd.getText();

		Element reqTyp = (Element) reqList.get(1); 
		this.requestType = reqTyp.getText();

		Element policyInfo = (Element) reqList.get(2); 
		List infoList = policyInfo.getChildren();

		Element policyNum = (Element) infoList.get(0); 
		if(policyNum.getText()!=null && !policyNum.getText().equals("")){
			this.policyNumber = policyNum.getText();
			this.policySuffix = "";
			if(this.policyNumber.length() > 9){
				this.policySuffix = this.policyNumber.substring(this.policyNumber.length()-1);
			
				this.policyNumber = this.policyNumber.substring(0, this.policyNumber.length()-1);
			}
		}
		
		LOGGER.info("parseXML end");
	}

	private void createDocFromXML(String xml) throws JDOMException, IOException {
		
		LOGGER.info("createDocFromXML start");
		SAXBuilder sb = new SAXBuilder();
		doc = sb.build(new StringReader(xml));
		LOGGER.info("createDocFromXML end");
	}

	public String getPolicyNumber() {
		return policyNumber;
	}

	public String getPswd() {
		return pswd;
	}

	public String getUsrId() {
		return usrId;
	}

	public String getPolicySuffix() {
		return policySuffix;
	}
}
