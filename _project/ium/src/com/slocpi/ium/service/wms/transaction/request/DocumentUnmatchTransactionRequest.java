package com.slocpi.ium.service.wms.transaction.request;

import java.io.IOException;
import java.io.StringReader;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;


public class DocumentUnmatchTransactionRequest implements Request {
	private static final Logger LOGGER = LoggerFactory.getLogger(DocumentUnmatchTransactionRequest.class);

	protected Document doc;

	private String usrId;

	private String pswd;

	private String iumReqId;
	
	private String requestType;
	
	private String policyId;
	
	private String imgRefNo;
	
	private String requestTypeId;

	public void parseXML(String xml) throws Exception {
		
		LOGGER.info("parseXML start");
		createDocFromXML(xml);
		
		Element root = doc.getRootElement(); 
		List children = root.getChildren();

		Element request = (Element) children.get(0); 
		List reqList = request.getChildren();

		Element auth = (Element) reqList.get(0); 
		List authList = auth.getChildren();

		Element usrId = (Element) authList.get(0); 
		this.usrId = usrId.getText();

		Element pswd = (Element) authList.get(1);
		this.pswd = pswd.getText();

		Element reqtTyp = (Element) reqList.get(1);
													
		this.requestType = reqtTyp.getText();

		Element reqtInfo = (Element) reqList.get(2); 	
		List infoList = reqtInfo.getChildren();
		
		if(infoList.size() == 1){
			Element iumReqId = (Element) infoList.get(0); 
			if(iumReqId.getText() != null){
				this.iumReqId = iumReqId.getText();
			}else{
				throw new Exception("Requirement ID is required.");
			}
		}
		else{
			Element policyId = (Element) infoList.get(0);	
			if(policyId.getText() != null){
				this.policyId = policyId.getText();
			}else{
				throw new Exception("Policy ID is empty.");
			}
			Element imgRefNo = (Element) infoList.get(1);	
			if(imgRefNo.getText() != null){
				this.imgRefNo = imgRefNo.getText();
			}else{
				throw new Exception("Image Reference Number is empty.");
			}
			Element reqTypeId = (Element) infoList.get(2);	
			if(reqTypeId.getText() != null){
				this.requestTypeId = reqTypeId.getText();
			}else{
				throw new Exception("Request Type ID is empty.");
			}			
		}
		LOGGER.info("parseXML end");
	}

	private void createDocFromXML(String xml) throws JDOMException, IOException {
		
		LOGGER.info("createDocFromXML start");
		SAXBuilder sb = new SAXBuilder();
		doc = sb.build(new StringReader(xml));
		LOGGER.info("createDocFromXML end");
	}

	public String getIumReqId() {
		return iumReqId;
	}

	public String getPswd() {
		return pswd;
	}

	public String getRequestType() {
		return requestType;
	}

	public String getUsrId() {
		return usrId;
	}

	public String getImgRefNo() {
		return imgRefNo;
	}

	public String getPolicyId() {
		return policyId;
	}

	public String getRequestTypeId() {
		return requestTypeId;
	}

}
