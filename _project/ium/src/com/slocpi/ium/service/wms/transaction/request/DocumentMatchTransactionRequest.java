package com.slocpi.ium.service.wms.transaction.request;

import java.io.IOException;
import java.io.StringReader;
import java.util.Date;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;

import com.slocpi.ium.util.IUMConstants;

public class DocumentMatchTransactionRequest implements Request {
	private static final Logger LOGGER = LoggerFactory.getLogger(DocumentMatchTransactionRequest.class);

	private Document doc;

	private String usrId;

	private String pswd;

	private String reqLvlIndicator;

	private String polId;

	private String clientId;

	private String iumReqId;

	private String iumReqTypeId;

	private String imgRefNumber;

	private String reqStatus;

	private Date scanDate;
	
	private Date testDate;

	private String requestType;
	
	private long reqStatusID;
	
	private String policySuffix;
	
	private String errMsg = "";
	
	public void parseXML(String xml) throws Exception {
		
		LOGGER.info("parseXML start");
		createDocFromXML(xml);	
		
		Element root = doc.getRootElement(); 
		List children = root.getChildren();
				
		Element request = (Element) children.get(0);
		List reqList = request.getChildren();

		Element auth = (Element) reqList.get(0);
		List authList = auth.getChildren();

		Element usrId = (Element) authList.get(0);
		this.usrId = usrId.getText();

		Element pswd = (Element) authList.get(1);
		this.pswd = pswd.getText();

		Element reqtTyp = (Element) reqList.get(1); 
		this.requestType = reqtTyp.getText();
		
		Element reqtInfo = (Element) reqList.get(2);
													
		List infoList = reqtInfo.getChildren();

		Element policyId = (Element) infoList.get(0);
		
		if(policyId.getText() != null){
			this.polId = policyId.getText();
			this.policySuffix = "";
			
			if(this.polId.length() > 9){
				this.policySuffix = this.polId.substring(this.polId.length()-1);
				this.polId = this.polId.substring(0, this.polId.length()-1);
			}
		}
		
		Element clientId = (Element) infoList.get(1); 
		if(clientId.getText() != null && !clientId.getText().equals("")){
			this.clientId = clientId.getText();
		}

		Element iumReqId = (Element) infoList.get(2);
		if(iumReqId.getText() != null && !iumReqId.getText().equals("")){
			this.iumReqId = iumReqId.getText();
		}

		Element reqLvlIndicator = (Element) infoList.get(3); 
		if(reqLvlIndicator.getText() != null && !reqLvlIndicator.getText().equals("")){
			this.reqLvlIndicator = reqLvlIndicator.getText();
		}
		
		Element reqTypId = (Element) infoList.get(4); 
		if(reqTypId.getText() != null && !reqTypId.getText().equals("")){
			this.iumReqTypeId = reqTypId.getText();
		}
		
		Element imgRef = (Element) infoList.get(5); 
		if(imgRef.getText() != null && !imgRef.getText().equals("")){
			this.imgRefNumber = imgRef.getText();
		}
		
		Element reqStatus = (Element) infoList.get(6); 
		this.reqStatus = reqStatus.getText();			
		if(this.reqStatus.equalsIgnoreCase("RAC")){
			this.reqStatusID = IUMConstants.STATUS_REVIEWED_AND_ACCEPTED;
		}
		if(this.reqStatus.equalsIgnoreCase("RCD")){
			this.reqStatusID = IUMConstants.STATUS_RECEIVED_IN_SITE;
		}
		
		Element scanDate = (Element) infoList.get(7); 
		if(scanDate.getText() != null && !scanDate.getText().equals("")){
			this.scanDate = new Date(scanDate.getText());
		}
		
		Element testDate = (Element) infoList.get(8); 
		
		if(testDate.getText() != null && !testDate.getText().equals("")){
			this.testDate = new Date(testDate.getText());
		}
		LOGGER.info("parseXML end");
		
	}

	private void createDocFromXML(String xml) throws JDOMException, IOException {
		
		LOGGER.info("createDocFromXML start");
		SAXBuilder sb = new SAXBuilder();
		doc = sb.build(new StringReader(xml));
		LOGGER.info("createDocFromXML end");
	}

	public String getClientId() {
		return clientId;
	}

	public String getImgRefNumber() {
		return imgRefNumber;
	}

	public String getIumReqId() {
		return iumReqId;
	}

	public String getIumReqTypeId() {
		return iumReqTypeId;
	}

	public String getPolId() {
		return polId;
	}

	public String getPswd() {
		return pswd;
	}

	public String getReqLvlIndicator() {
		return reqLvlIndicator;
	}

	public String getReqStatus() {
		return reqStatus;
	}

	public String getRequestType() {
		return requestType;
	}

	public Date getScanDate() {
		return scanDate;
	}

	public String getUsrId() {
		return usrId;
	}

	public long getReqStatusID() {
		return reqStatusID;
	}

	public Date getTestDate() {
		return testDate;
	}

}
