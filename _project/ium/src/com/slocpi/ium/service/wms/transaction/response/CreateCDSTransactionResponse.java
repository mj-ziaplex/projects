package com.slocpi.ium.service.wms.transaction.response;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import java.util.TimeZone;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.jdom.output.XMLOutputter;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.IUMConstants;

public class CreateCDSTransactionResponse implements Response {
private static final Logger LOGGER = LoggerFactory.getLogger(CreateCDSTransactionResponse.class);
	
	private static final String IUM_WS_TEMPLATE_RESP = "iumws.template.resp.ar";
	
	private String template = "ium_ar_response.xml";
	
	protected Document doc;
	
	private String xmlResponse;
	private String errMsg;
	private String resultCode;
	
	public CreateCDSTransactionResponse(boolean result,String errMsg){
		
		LOGGER.info("constructor start");
		if (result){
			this.resultCode = IUMConstants.YES;
		}else{
			this.resultCode = IUMConstants.NO;
		}
		
		this.errMsg = errMsg;
		
		try{
			ResourceBundle rb = ResourceBundle.getBundle(IUMConstants.IUM_WMS_CONFIG);
			template = rb.getString(IUM_WS_TEMPLATE_RESP);
		}catch(MissingResourceException e){
			LOGGER.error(CodeHelper.getStackTrace(e));
		}
		
		SAXBuilder sb = new SAXBuilder();
		try {
			doc = sb.build(new FileReader(template));
		} catch (FileNotFoundException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
		} catch (JDOMException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
		} catch (IOException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
		}
		LOGGER.info("constructor end");
	}
	
	public void buildResponse(){
		
		LOGGER.info("buildResponse start");
		Element root = doc.getRootElement(); 
		List children = root.getChildren();
		
		Element response = (Element)children.get(0); 
		List respList = response.getChildren();
		
		Element transResult = (Element)respList.get(0); 
		List resultList = transResult.getChildren();
		
		Element resultCode = (Element)resultList.get(0); 
		if(this.resultCode != null){
			resultCode.setText(this.resultCode);
		}
		
		Element errText = (Element)resultList.get(1); 
		if (this.errMsg != null){
			errText.setText(this.errMsg);
		}
		
		Element svrDate = (Element)respList.get(1); 
		SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
		String date = formatter.format(new Date());
		svrDate.setText(date);
		
		Element svrTime = (Element)respList.get(2); 
		Calendar cal = Calendar.getInstance(TimeZone.getDefault());	
		formatter = new SimpleDateFormat("HH:mm:ss");
		String time = formatter.format(cal.getTime());
		svrTime.setText(time);
		
		this.xmlResponse = toXML(doc);
		LOGGER.info("buildResponse end");
	}
	
	private String toXML(Document doc){
		
		LOGGER.info("toXML start");
		XMLOutputter out = new XMLOutputter();
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		try {
			out.output(doc, bos);
		} catch (IOException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
		}
		LOGGER.info("toXML end");
		return bos.toString();
	}
	
	public String getXmlResponse() {
		return xmlResponse;
	}

	public void setErrMsg(String errMsg) {
		this.errMsg = errMsg;
	}
}
