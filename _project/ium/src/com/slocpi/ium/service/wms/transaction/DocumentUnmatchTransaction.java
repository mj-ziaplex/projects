package com.slocpi.ium.service.wms.transaction;

import java.io.IOException;
import java.util.Date;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jdom.JDOMException;
import com.slocpi.ium.data.PolicyRequirementsData;
import com.slocpi.ium.service.wms.transaction.request.DocumentUnmatchTransactionRequest;
import com.slocpi.ium.service.wms.transaction.response.DocumentUnmatchTransactionResponse;
import com.slocpi.ium.underwriter.PolicyRequirements;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.UserManager;

public class DocumentUnmatchTransaction {
	private static final Logger LOGGER = LoggerFactory.getLogger(DocumentUnmatchTransaction.class);
	private PolicyRequirementsData prd;
	private String errMsg = "";
	private boolean result;
	
	public String unmatchDocument(String xmlInput){	
		
		LOGGER.info("unmatchDocument start");
		String response = "";
		String responseLog = "";
		this.result = false;
						
			try{
				
				DocumentUnmatchTransactionRequest request = new DocumentUnmatchTransactionRequest();
				request.parseXML(xmlInput);
				String xmlInputLog = xmlInput;
				LOGGER.info("XML input--> " + xmlInputLog.replaceAll("[\r\n]+", " "));
				createPolicyReqtsData(request);
				
				UserManager um = new UserManager();
				if (um.isValidUser(request.getUsrId(),request.getPswd())) {
					PolicyRequirements policy = new PolicyRequirements();
					result = policy.unmatchDocument(this.prd);	
				}else{
					this.errMsg = "User is invalid.";
				}
			} catch(Exception e){
				this.errMsg = e.getMessage();
				this.result = false;
				LOGGER.error(CodeHelper.getStackTrace(e));
			}
					
			
			try {
				LOGGER.info("result-->" + result);
				LOGGER.info("errMsg-->" + errMsg);
				
				DocumentUnmatchTransactionResponse resp = new DocumentUnmatchTransactionResponse(result,this.errMsg);
				resp.buildResponse();
				response = resp.getXmlResponse();
				responseLog = response;
				LOGGER.info("XML response--> " + responseLog.replaceAll("[\r\n]+", " "));
			} catch (JDOMException e) {
				LOGGER.error(CodeHelper.getStackTrace(e));
			} catch (IOException e) {
				LOGGER.error(CodeHelper.getStackTrace(e));
			}
			
			LOGGER.info("unmatchDocument end");
		return response;
	}

	private void createPolicyReqtsData(DocumentUnmatchTransactionRequest request) {
		
		LOGGER.info("createPolicyReqtsData start");
		prd = new PolicyRequirementsData();
		if(request.getIumReqId() != null){
			prd.setRequirementId(Long.parseLong(request.getIumReqId()));	
		}else{
			prd.setRequirementId(0);
		}
		
		if(request.getPolicyId() != null){
			prd.setReferenceNumber(String.valueOf(request.getPolicyId()).substring(0,request.getPolicyId().length()-1)); // PolicyId
		}else{
			prd.setReferenceNumber("");
		}
		if(request.getImgRefNo() != null){
			prd.setImageRef(String.valueOf(request.getImgRefNo()));	
		}else{
			prd.setImageRef("");
		}
		if(request.getRequestTypeId() != null){
			prd.setRequirementCode(String.valueOf(request.getRequestTypeId()));	
		}else{
			prd.setRequirementCode("");
		}
		prd.setUpdateDate(new Date());
		prd.setUpdatedBy(request.getUsrId().toUpperCase());
		LOGGER.info("createPolicyReqtsData end");
	}
}
