/**
 * IUMServiceServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package com.slocpi.ium.service.wms.client;

public class IUMServiceServiceLocator extends org.apache.axis.client.Service implements com.slocpi.ium.service.wms.client.IUMServiceService {

    public IUMServiceServiceLocator() {
    }


    public IUMServiceServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public IUMServiceServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for IUMService
    private java.lang.String IUMService_address = "http://sv591481.ph.sunlife:9012/ium/services/IUMService";

    public java.lang.String getIUMServiceAddress() {
        return IUMService_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String IUMServiceWSDDServiceName = "IUMService";

    public java.lang.String getIUMServiceWSDDServiceName() {
        return IUMServiceWSDDServiceName;
    }

    public void setIUMServiceWSDDServiceName(java.lang.String name) {
        IUMServiceWSDDServiceName = name;
    }

    public com.slocpi.ium.service.wms.client.IUMService getIUMService() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(IUMService_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getIUMService(endpoint);
    }

    public com.slocpi.ium.service.wms.client.IUMService getIUMService(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.slocpi.ium.service.wms.client.IUMServiceSoapBindingStub _stub = new com.slocpi.ium.service.wms.client.IUMServiceSoapBindingStub(portAddress, this);
            _stub.setPortName(getIUMServiceWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setIUMServiceEndpointAddress(java.lang.String address) {
        IUMService_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (com.slocpi.ium.service.wms.client.IUMService.class.isAssignableFrom(serviceEndpointInterface)) {
                com.slocpi.ium.service.wms.client.IUMServiceSoapBindingStub _stub = new com.slocpi.ium.service.wms.client.IUMServiceSoapBindingStub(new java.net.URL(IUMService_address), this);
                _stub.setPortName(getIUMServiceWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("IUMService".equals(inputPortName)) {
            return getIUMService();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("com.slocpi.ium.service.wms", "IUMServiceService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("com.slocpi.ium.service.wms", "IUMService"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("IUMService".equals(portName)) {
            setIUMServiceEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
