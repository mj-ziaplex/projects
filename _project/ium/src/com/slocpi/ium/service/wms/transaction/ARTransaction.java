package com.slocpi.ium.service.wms.transaction;

import java.io.FileNotFoundException;
import java.io.IOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jdom.JDOMException;
import com.slocpi.ium.service.ingenium.IngeniumGateway;
import com.slocpi.ium.service.wms.transaction.request.ARTransactionRequest;
import com.slocpi.ium.service.wms.transaction.response.ARTransactionResponse;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.UserManager;

public class ARTransaction {
	private static final Logger LOGGER = LoggerFactory.getLogger(ARTransaction.class);
	private String errMsg = "";
	private boolean result;
	
	public String retrieveKOandReqts(String xmlInput){
		
		LOGGER.info("retrieveKOandReqts start");
		String response = "";
		String responseLog = "";
		this.result = false;
			
			try{
				ARTransactionRequest request = new ARTransactionRequest();
				
				String xmlInputLog = xmlInput;
				LOGGER.info("XML input--> " + xmlInputLog.replaceAll("[\r\n]+", " "));
				
				request.parseXML(xmlInput);
				
				//valid user
				UserManager um = new UserManager();
				if (um.isValidUser(request.getUsrId(),request.getPswd())) {
					if(request.getPolicyNumber() != null && !request.getPolicyNumber().equals("")){
						IngeniumGateway ingenium = new IngeniumGateway(true);
						result = ingenium.processCreateUpdateRecord(request.getPolicyNumber(),request.getPolicySuffix());
					}else{
						this.errMsg = "Policy Number is missing."; 
					}				
				}else{
					this.errMsg = "User is invalid.";
				}
			} catch (Exception e){
				this.errMsg = e.getMessage();
				this.result = false;
				LOGGER.error(CodeHelper.getStackTrace(e));
			}
					
		
		try {
			LOGGER.info("result-->" + result);
			LOGGER.info("errMsg-->" + errMsg);
			ARTransactionResponse resp = new ARTransactionResponse(result,this.errMsg);
			resp.buildResponse();
			response = resp.getXmlResponse();
			responseLog = response;
			LOGGER.info("XML response--> " + responseLog.replaceAll("[\r\n]+", " "));
		} catch (FileNotFoundException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
		} catch (JDOMException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
		} catch (IOException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
		}
		
		LOGGER.info("retrieveKOandReqts end");
	return response;
	}
	
}
