package com.slocpi.ium.service.wms.transaction.response;

import java.io.IOException;

public interface Response {
	public void buildResponse() throws IOException;
}
