package com.slocpi.ium.service.wms.transaction;

import java.io.IOException;
import java.util.Date;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jdom.JDOMException;
import com.slocpi.ium.data.PolicyRequirementsData;
import com.slocpi.ium.data.StatusData;
import com.slocpi.ium.service.wms.transaction.request.DocumentMatchTransactionRequest;
import com.slocpi.ium.service.wms.transaction.response.DocumentMatchTransactionResponse;
import com.slocpi.ium.underwriter.PolicyRequirements;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.UserManager;

public class DocumentMatchTransaction_ORIG {
	private static final Logger LOGGER = LoggerFactory.getLogger(DocumentMatchTransaction_ORIG.class);
	private PolicyRequirementsData prd;
	private String errMsg = "";
	private String submissionStatus = "";
	private boolean result;
	
	public String matchDocument(String xmlInput){	
		
		LOGGER.info("matchDocument start");
		String response = "";
		
		this.result = false;
					
			try{
			
				DocumentMatchTransactionRequest request = new DocumentMatchTransactionRequest();
				request.parseXML(xmlInput);
				createPolicyReqtsData(request);
				UserManager um = new UserManager();
				if (um.isValidUser(request.getUsrId(),request.getPswd())) {
					PolicyRequirements policy = new PolicyRequirements();
					this.result = policy.matchDocument(this.prd);	
					
					if(this.result){
						this.submissionStatus = policy.checkReqtsSubmissionStatus(request.getPolId()); //Policy Number/Reference Number
						if (!prd.getRequirementCode().equalsIgnoreCase("EXT1")) { 
							policy.sendNotificationReceiveReqt(request.getPolId(),request.getIumReqId(), request.getUsrId());
						}
					}
					
				}else{
					this.errMsg = "User is invalid.";
				}
				
			}catch(Exception e){
				this.errMsg = e.getMessage();
				this.result = false;
				LOGGER.error(CodeHelper.getStackTrace(e));
			}
			
			// build response
			
			try {
				DocumentMatchTransactionResponse resp = new DocumentMatchTransactionResponse(result,this.errMsg,this.submissionStatus);
				resp.buildResponse();
				response = resp.getXmlResponse();
			} catch (JDOMException e) {
				LOGGER.error(CodeHelper.getStackTrace(e));
			} catch (IOException e) {
				LOGGER.error(CodeHelper.getStackTrace(e));
			}
			
			LOGGER.info("matchDocument end");
		return response;
	}

	private void createPolicyReqtsData(DocumentMatchTransactionRequest request) {
		
		LOGGER.info("createPolicyReqtsData start");
		prd = new PolicyRequirementsData();
		prd.setReferenceNumber(request.getPolId());
		
		if(request.getClientId() != null && !request.getClientId().equals("")){
			prd.setClientId(request.getClientId());
		}
				
		StatusData sdata = new StatusData();
		sdata.setStatusType(request.getReqStatus());
		sdata.setStatusId(request.getReqStatusID());
		prd.setStatus(sdata);
		
		if(request.getIumReqId() != null && !request.getIumReqId().equals("")){
			prd.setRequirementId(Long.parseLong(request.getIumReqId()));
		}else{
			prd.setRequirementId(0);
		}
		
		if(request.getIumReqTypeId() != null){
			prd.setRequirementCode(request.getIumReqTypeId());
		}
				
		if(request.getReqLvlIndicator() != null){
			prd.setLevel(request.getReqLvlIndicator());
		}
				
		if(request.getImgRefNumber() != null){
			prd.setImageRef(request.getImgRefNumber());
		}
				
		if(request.getScanDate() != null){
			prd.setReceiveDate(request.getScanDate());
		}
		
		if(request.getScanDate() != null){
			prd.setScanDate(request.getScanDate());
		}
				
		if(request.getTestDate() != null){
			prd.setTestDate(request.getTestDate());
		}
		
		prd.setCompletedRequirementInd(true);
		prd.setUpdatedBy(request.getUsrId().toUpperCase());
		prd.setUpdateDate(new Date());
		LOGGER.info("createPolicyReqtsData end");
		
	}
	
}
