/*
 * Created on Apr 19, 2006
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.service.ingenium;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.slocpi.ium.data.AssessmentRequestData;
import com.slocpi.ium.data.CDSMortalityRatingData;
import com.slocpi.ium.data.CDSSummaryPolicyCoverageData;
import com.slocpi.ium.data.ClientData;
import com.slocpi.ium.data.ClientDataSheetData;
import com.slocpi.ium.data.IUMMessageData;
import com.slocpi.ium.data.KickOutMessageData;
import com.slocpi.ium.data.PolicyDetailData;
import com.slocpi.ium.data.PolicyRequirementsData;
import com.slocpi.ium.data.StatusData;
import com.slocpi.ium.data.SunLifeOfficeData;
import com.slocpi.ium.data.UserProfileData;
import com.slocpi.ium.ingenium.transaction.data.Client;
import com.slocpi.ium.ingenium.transaction.data.Kickout;
import com.slocpi.ium.ingenium.transaction.data.Mortality;
import com.slocpi.ium.ingenium.transaction.data.Requirement;
import com.slocpi.ium.ingenium.transaction.parameter.RequirementTransactionParameter;
import com.slocpi.ium.ingenium.transaction.response.ClientDataResponse;
import com.slocpi.ium.ingenium.transaction.response.detail.ExistingPolicyInformation;
import com.slocpi.ium.ingenium.transaction.response.detail.InsuredInformation;
import com.slocpi.ium.ingenium.transaction.response.detail.OwnerInformation;
import com.slocpi.ium.ingenium.transaction.response.detail.PolicyInformation;
import com.slocpi.ium.ingenium.transaction.response.detail.RelPolicyInformation;
import com.slocpi.ium.interfaces.IUMInterfaceException;
import com.slocpi.ium.interfaces.messages.IUMMessageUtility;
import com.slocpi.ium.underwriter.AssessmentRequest;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.DateHelper;
import com.slocpi.ium.util.IUMConstants;
import com.slocpi.ium.util.StringHelper;
import com.slocpi.ium.util.ValueConverter;

/**
 * @author Glenn Gonzales
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 * 
 * June 25, 2014 - Cshellz Aries Sayan
 * Remove commented codes that was already not being used.
 */
public class IngeniumPopulator {

	private static final Logger LOGGER = LoggerFactory.getLogger(IngeniumPopulator.class);
	private String CODE_POLICY = "P";
	private String CODE_CLIENT = "C";

	/**
	 * This method is based on translateClientRelatedInfoMessage in ClientRelatedInfoTranslator
	 * @param ard						- ARD to fill up
	 * @param refNum					- reference number for this AR
	 * @param relatedPoliciesList		- related policies to fill up
	 * @param clntDataResp				- Ingenium CDS/KO/RELATED POLICIES response
	 * @throws IUMInterfaceException
	 */
	public void populateClientRelatedInfo(
			final AssessmentRequestData	ard,
			final String refNum,
			final ArrayList relatedPoliciesList,
			final ClientDataResponse clntDataResp) throws IUMInterfaceException {
		
		LOGGER.info("populateClientRelatedInfo start");
		InsuredInformation 			insInfo = null;
		Mortality 					mortInfoIns = null;
		OwnerInformation			ownInfo = null;
		Mortality 					mortInfoOwn = null;
		ExistingPolicyInformation	expInfo = null;
		List                	    relInfo = null;
		ClientDataSheetData			cds = new ClientDataSheetData();
		CDSMortalityRatingData		cdsMortIns = null;
		CDSMortalityRatingData		cdsMortOwn = null;
		
		// Get Insured info & mortality
		insInfo = clntDataResp.getInsuredInformation();
		if (insInfo != null && insInfo.getClient()!=null && insInfo.getClient().getId()!=null) {
			ard.setInsured(populateClientInfoInsured(insInfo));
		}
		
		mortInfoIns = insInfo.getMortality();
		if (mortInfoIns != null) {
			cdsMortIns = populateMortalityInfo(mortInfoIns, ard.getReferenceNumber());
			if (cdsMortIns != null) {
				ard.setInsuredMortalityRating(cdsMortIns);
			}
		}

		ownInfo = clntDataResp.getOwnerInformation();
		if (ownInfo != null && ownInfo.getClient()!=null && ownInfo.getClient().getId()!=null) {
			ard.setOwner(populateClientInfoOwner(ownInfo));
		}
		
		mortInfoOwn = ownInfo.getMortality();
		if (mortInfoOwn != null) {
			cdsMortOwn = populateMortalityInfo(mortInfoOwn, ard.getReferenceNumber());
			if (cdsMortOwn != null) {
				ard.setOwnerMortalityRating(cdsMortOwn);
			}
		}
		
		
		if (ard.getOwner() == null || StringHelper.isEmpty(ard.getOwner().getClientId())){
			ard.setOwner(ard.getInsured());
			ard.setOwnerMortalityRating(ard.getInsuredMortalityRating());
		} else if (ard.getInsured() == null || StringHelper.isEmpty(ard.getInsured().getClientId())){
			ard.setInsured(ard.getOwner());
			ard.setInsuredMortalityRating(ard.getOwnerMortalityRating());
		}

		// Set Clien Data Sheet
		expInfo = clntDataResp.getExistingPolicyInformation();
		if (expInfo != null) {
			cds.setSummaryOfPolicies(populateClientInfoExistingPolicy(expInfo));
			ard.setClientDataSheet(cds);
		}

		if (relatedPoliciesList != null) {
			relInfo = clntDataResp.getRelatedPolicyList();
			if (relInfo != null) {
				for (int i = 0; i < relInfo.size(); i++) {
					relatedPoliciesList.add(
							populateRelatedPolicyAndCoverageInfo(
									(RelPolicyInformation)relInfo.get(i), refNum));
				}
			}
		}
		
		ard.setReferenceNumber(refNum);
		LOGGER.info("populateClientRelatedInfo end");
	}

	/**
	 * This method is based on parseAssessmentRequestData in PolicyQueueTranslator
	 * @param ard			- ARD to fill up
	 * @param policyInfo	- policy information from Ingenium
	 * @throws IUMInterfaceException
	 */
	public void populateAssessmentRequestData(
			final AssessmentRequestData ard,
			final PolicyInformation	policyInfo) throws IUMInterfaceException {
		
		LOGGER.info("populateAssessmentRequestData start");
		try {
			
			ard.setReferenceNumber(policyInfo.getPolicyNumber());
			ard.setPolicySuffix(policyInfo.getPolicySuffix());
			ard.setAmountCovered(IUMMessageUtility.getDouble(policyInfo.getCoverageAmount()));
			ard.setPremium(IUMMessageUtility.getDouble(policyInfo.getPremiumAmount()));
			ard.setApplicationReceivedDate(DateHelper.parse(policyInfo.getReceivedDate(), IUMMessageUtility.DEFAULT_DATE_FORMAT));

			Client ingClientData = policyInfo.getInsured();
			ClientData cd = new ClientData();
			cd.setClientId(ingClientData.getId());
			cd.setLastName(ingClientData.getSurname());
			cd.setMiddleName(ingClientData.getMiddleName());
			cd.setGivenName(ingClientData.getGivenName());
			ard.setInsured(cd);

			UserProfileData usrAgentData = new UserProfileData();
			LOGGER.info("Agent Id-->" + policyInfo.getAgentId());
			usrAgentData.setUserId(policyInfo.getAgentId());
			ard.setAgent(usrAgentData);

			UserProfileData usrNbUserData = new UserProfileData();
			usrNbUserData.setACF2ID(policyInfo.getNbUserId());
			ard.setAssignedTo(usrNbUserData);
			ard.setFolderLocation(usrNbUserData);

			UserProfileData usrUnderwriterData = new UserProfileData();
			usrUnderwriterData.setACF2ID(policyInfo.getUnderwriterId());
			ard.setUnderwriter(usrUnderwriterData);

			SunLifeOfficeData sod = new SunLifeOfficeData();
			LOGGER.info("Sunlife Office/Brand Id-->" + policyInfo.getOfficeId());
			sod.setOfficeId(policyInfo.getOfficeId());
			ard.setBranch(sod);
		} catch (Exception e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMInterfaceException("Error in parsing Assessment Request Data : " + e.getMessage());
		}
		LOGGER.info("populateAssessmentRequestData end");
	}

	/**
	 * This method is based on parseKickOutMessageData in KickOutTranslator
	 * @param koDetails	- kickout details from Ingenium
	 * @param refNum	- reference number
	 * @return KickOutMessageData
	 * @throws IUMInterfaceException
	 */
	public KickOutMessageData populateKOMessageData(
			final Kickout koDetails,
			final String refNum) throws IUMInterfaceException {
		
		LOGGER.info("populateKOMessageData start");
		KickOutMessageData komd = new KickOutMessageData();
		
		if (koDetails != null) {
			
			komd.setReferenceNumber(refNum);
			komd.setClientId(koDetails.getClientId());
			komd.setMessageText(koDetails.getMessageText());
			komd.setFailResponse(koDetails.getMessageResponse());
			
			try{
				komd.setSequenceNumber(IUMMessageUtility.getLong(koDetails.getSequenceNumber()));
			} catch(Exception e){
				komd.setSequenceNumber(0);
			}
		} else {
			LOGGER.warn("Attribute not defined for Kick Out Message.");
			throw new IUMInterfaceException("Attribute not defined for Kick Out Message.");
		}
		LOGGER.info("populateKOMessageData end");
		return komd;
	}


	/**
	 * This method is based on parsePolicyRequirement in PolicyRequirementInquiryTranslator
	 * @param reqList	- requirements list from Ingenium
	 * @param refNum	- reference number
	 * @return ArrayList
	 * @throws IUMInterfaceException
	 */
	public ArrayList populateRequirements(
			final List reqtList,
			final String refNum) throws IUMInterfaceException {
		
		LOGGER.info("populateRequirements start");
		Requirement 			requirement = null;
		CodeHelper 				ch = new CodeHelper();
		ArrayList				polReqtList = new ArrayList();
		PolicyRequirementsData 	prd = new PolicyRequirementsData();

		for (int i = 0; i < reqtList.size(); i++) {
			try {
				requirement = (Requirement) reqtList.get(i);
				prd = new PolicyRequirementsData();
				prd.setLevel(requirement.getPolicyClientCode());
				prd.setClientType(requirement.getClientType());
				prd.setRequirementCode(requirement.getRequirementId());
				prd.setStatus(ch.convertStatusData(requirement.getStatusCode()));
				prd.setReqDesc(requirement.getDesc());
				prd.setUpdatedBy(requirement.getUpdatedBy());
				prd.setDesignation(requirement.getDesigntId());
				prd.setTestResultCode(requirement.getTestResultCode());
				prd.setTestResultDesc(requirement.getTestResultDesc());
				prd.setPaidInd(IUMMessageUtility.isTrue(requirement.getReqtPaidInd()));
				prd.setNewTestOnly(ValueConverter.stringToBoolean(requirement.getNewResultInd()));
				prd.setCCASuggestInd(ValueConverter.stringToBoolean(requirement.getSugstInd()));
				prd.setFollowUpNumber(1);

				if (prd.getLevel().equals(CODE_POLICY)) {
					prd.setReferenceNumber(requirement.getPolicyClientId());
				} else {
					prd.setClientId(requirement.getPolicyClientId());
				}
				try{
					prd.setSequenceNumber(IUMMessageUtility.getLong(requirement.getSequenceNum()));
				}catch (Exception e){
					prd.setSequenceNumber(-1);
				}

				try{
					prd.setStatusDate(DateHelper.parse(requirement.getStatusDate(),
									  IUMMessageUtility.DEFAULT_DATE_FORMAT));
				}catch(Exception e){
					// do nothing
				}

				try{
					prd.setFollowUpDate(DateHelper.parse(requirement.getFollowupDate(),
										IUMMessageUtility.DEFAULT_DATE_FORMAT));
				}catch(Exception e){
					// do nothing
				}

				try{
					prd.setCreateDate(DateHelper.parse(requirement.getCreateDate(),
									  IUMMessageUtility.DEFAULT_DATE_FORMAT));
				} catch (Exception e){
					// do nothing
				}

				try{
					prd.setUpdateDate(DateHelper.parse(requirement.getUpdateUserDate(),
									  IUMMessageUtility.DEFAULT_DATE_FORMAT));
				}catch (Exception e){
					// do nothing
				}

				try{
					prd.setTestDate(DateHelper.parse(requirement.getTestDate(), IUMMessageUtility.DEFAULT_DATE_FORMAT));
				} catch (Exception e){
					// do nothing
				}

				try {
					prd.setValidityDate(DateHelper.parse(requirement.getReqtValidityDate(), IUMMessageUtility.DEFAULT_DATE_FORMAT));
				} catch (Exception e){
					// do nothing
				}

				prd.setReferenceNumber(refNum);
				
			
				prd.setImageRef(requirement.getIndexNumber());
				prd.setReqtCountInd(requirement.getReqtCountIndicator());
				polReqtList.add(prd);
			} catch (SQLException sqle) {
				LOGGER.error(CodeHelper.getStackTrace(sqle));
				throw new IUMInterfaceException(sqle.getMessage());
			}
		}
		LOGGER.info("populateRequirements end");
		return polReqtList;
	}

	/**
	 * This method is based on generateMaintainRequirementMessage in RequirementMaintenanceTranslator
	 * @param prd		- policy requirements data to fill up
	 * @param isCreate	- TRUE if this is for create requirements; FALSE if otherwise
	 * @return RequirementTransactionParameter
	 * @throws IUMInterfaceException
	 */
	public RequirementTransactionParameter populateRequirementMaintenanceParm(
			final PolicyRequirementsData prd,
			final boolean isCreate) throws IUMInterfaceException {
		
		LOGGER.info("populateRequirementMaintenanceParm start");
		RequirementTransactionParameter parm = new RequirementTransactionParameter();
		CodeHelper 						ch   = new CodeHelper();
		AssessmentRequest 				ar 	 = new AssessmentRequest();

		try {
			if (isCreate) {
				parm.setParameterValue(RequirementTransactionParameter.PARM_TRANSACTION_CODE,
						RequirementTransactionParameter.TRANS_TYPE_CREATE);
			} else {
				// if seq = 0 create requirement
				if (prd.getSequenceNumber() == 0) {
					parm.setParameterValue(RequirementTransactionParameter.PARM_TRANSACTION_CODE,
							RequirementTransactionParameter.TRANS_TYPE_CREATE);
				} else {
					parm.setParameterValue(RequirementTransactionParameter.PARM_TRANSACTION_CODE,
							RequirementTransactionParameter.TRANS_TYPE_UPDATE);
				}
			}
			
			if (prd.getLevel() != null) {
				if (prd.getLevel().equals(CODE_CLIENT)){
					parm.setParameterValue(RequirementTransactionParameter.PARM_CLIENT_ID, prd.getClientId());	
				} else {
					parm.setParameterValue(RequirementTransactionParameter.PARM_POLICY_ID_BASE, prd.getReferenceNumber());
					String suffix = ar.getPolicySuffix(prd.getReferenceNumber());
					parm.setParameterValue(RequirementTransactionParameter.PARM_POLICY_ID_SFX, suffix);
				}
			}

			if (prd.getTestResultCode() != null) {
				parm.setParameterValue(RequirementTransactionParameter.PARM_TEST_RESULT_CODE, prd.getTestResultCode());
			}

			if (prd.getDesignation() != null) {
				parm.setParameterValue(RequirementTransactionParameter.PARM_DESIGNATION_ID, prd.getDesignation());
			}

			if (prd.getTestDate() != null) {
				parm.setParameterValue(RequirementTransactionParameter.PARM_TEST_DATE,
						(DateHelper.format(prd.getTestDate(),"yyyy-MM-dd")).toUpperCase());
			}

			if (prd.getValidityDate() != null) {
				parm.setParameterValue(RequirementTransactionParameter.PARM_VALIDITY_DATE,
						(DateHelper.format(prd.getValidityDate(),"yyyy-MM-dd")).toUpperCase());
			}
			
			// Added 06july2004 if follow-up number = 0, value = blank space and follow-up date = blank space
			if (prd.getFollowUpNumber() == 0){
				parm.setParameterValue(RequirementTransactionParameter.PARM_FOLLOW_UP_DATE, "");
				parm.setParameterValue(RequirementTransactionParameter.PARM_FOLLOW_UP_NUMBER, "");
			} else {
				parm.setParameterValue(RequirementTransactionParameter.PARM_FOLLOW_UP_DATE,
						(DateHelper.format(prd.getFollowUpDate(),"yyyy-MM-dd")).toUpperCase());
				parm.setParameterValue(RequirementTransactionParameter.PARM_FOLLOW_UP_NUMBER,
						Long.toString(prd.getFollowUpNumber()));
			}

			parm.setParameterValue(RequirementTransactionParameter.PARM_NEW_TEST_INDICATOR,  ValueConverter.booleanToString(prd.getNewTestOnly()));
			parm.setParameterValue(RequirementTransactionParameter.PARM_REQUIREMENT_ID, prd.getRequirementCode());
			
			if (isCreate){
				parm.setParameterValue(RequirementTransactionParameter.PARM_SEQUENCE_NUMBER,"");
			} else {
				String seqNum = "";
				if (prd.getSequenceNumber() == 0){
					seqNum = "";
				} else {
					seqNum = Long.toString(prd.getSequenceNumber());
				}
				parm.setParameterValue(RequirementTransactionParameter.PARM_SEQUENCE_NUMBER,seqNum);				
			}

			//	If block added by BJ Taduran last May 11, 2007
			if (prd.getImageRef() != null) {
				parm.setParameterValue(RequirementTransactionParameter.PARM_IMAGE_REF_NUM, prd.getImageRef());
			}

			parm.setParameterValue(RequirementTransactionParameter.PARM_PAID_INDICATOR, (prd.isPaidInd() ? "Y":"N"));

			try {
				StatusData sd = null;
				if (prd.getStatus() != null) {
					sd = ch.translateStatusData(IUMConstants.LOB_INDIVIDUAL_LIFE, prd.getStatus().getStatusId());
					parm.setParameterValue(RequirementTransactionParameter.PARM_REQT_STATUS, sd.getStatusDesc());
				} else {
					LOGGER.warn("Status is required");
					throw new IUMInterfaceException("Status is required");
				}
			} catch (SQLException sqle) {
				LOGGER.error(CodeHelper.getStackTrace(sqle));
				throw new IUMInterfaceException("Error in preparing requirement maintenance parameters " + sqle.getMessage());
	   		}
		} catch (Exception e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMInterfaceException("Error in preparing requirement maintenance parameters " + e.getMessage());
		}
		
		LOGGER.info("populateRequirementMaintenanceParm end");
		return parm;
	}

	/**
	 * This method is based on translateRequirementMaintenanceMessage in RequirementMaintenanceTranslator
	 * @param requirementData - requirement data from ingenium
	 * @param prd			  - policy requirements data to fill up
	 * @return IUMMessageData
	 * @throws IUMInterfaceException
	 */
	public IUMMessageData populateIUMMessageData (
			final Requirement requirementData,
			final PolicyRequirementsData 	prd) throws IUMInterfaceException {
		
		LOGGER.info("populateIUMMessageData start");
		IUMMessageData imd = null;
		try {
			imd = new IUMMessageData();
			imd.setRequirementId(requirementData.getRequirementId());
			imd.setPolicyClientId(prd.getLevel());
			imd.setPolicyClientCode(prd.getClientType());
			imd.setCreationDate(DateHelper.parse(requirementData.getCreateDate(),
								IUMMessageUtility.DEFAULT_DATE_FORMAT));
			imd.setStatusDate(DateHelper.parse(requirementData.getStatusDate(),
							  IUMMessageUtility.DEFAULT_DATE_FORMAT));
			try{
				imd.setSequenceNumber(IUMMessageUtility.getInteger(requirementData.getSequenceNum()));
			}catch(IUMInterfaceException iie){
				imd.setSequenceNumber(-1);
				LOGGER.error(CodeHelper.getStackTrace(iie));
			}
			imd.setConfirmationCode(requirementData.getStatusCode());
		} catch (Exception e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMInterfaceException(e.getMessage());
		}
		LOGGER.info("populateIUMMessageData end");
		return imd;
	}

	/**
	 * This method is based on parseMortalityRating in ClientRelatedInfoTranslator
	 * @param mortInfo	- mortality information
	 * @param refNum	- reference number
	 * @return CDSMortalityRatingData
	 * @throws IUMInterfaceException
	 */
	private CDSMortalityRatingData populateMortalityInfo(
			final Mortality	mortInfo,
			final String refNum) throws IUMInterfaceException {
		
		LOGGER.info("populateMortalityInfo start");
		CDSMortalityRatingData mrd = null;
		if (mortInfo != null) {
			mrd = new CDSMortalityRatingData();

			try{
				String height = mortInfo.getHeightInFeet();
				if (height != null && height.indexOf(".") > -1){
					height = height.substring(0, height.indexOf("."));
				}
				mrd.setHeightInFeet(IUMMessageUtility.getLong(height));
			}catch(Exception e){
				mrd.setHeightInFeet(-1);
			}

			try{
				String height = mortInfo.getHeightInInches();
				if (height != null && height.indexOf(".") > -1){
					height = height.substring(0, height.indexOf("."));
				}
				mrd.setHeightInInches(IUMMessageUtility.getLong(height));
			}catch(Exception e){
				mrd.setHeightInInches(-1);
			}

			try{
				mrd.setWeight(IUMMessageUtility.getDouble(mortInfo.getWeightInPounds()));
			}catch(Exception e){
				mrd.setWeight(-1);
			}

			try{
				mrd.setBasic(IUMMessageUtility.getDouble(mortInfo.getBasic()));

			}catch(Exception e){
				mrd.setBasic(-1);
			}

			try{
				mrd.setSmokingBasic(IUMMessageUtility.getDouble(mortInfo.getBasicSmoking()));
			}catch(Exception e){
				mrd.setBasic(-1);
			}

			try{
				mrd.setFamilyHistoryBasic(IUMMessageUtility.getDouble(mortInfo.getBasicFamily()));
			}catch(Exception e){
				mrd.setBasic(-1);
			}

			try{
				mrd.setCCR(IUMMessageUtility.getDouble(mortInfo.getCCR()));
			}catch(Exception e){
				mrd.setCCR(-1);
			}

			try{
				mrd.setSmokingCCR(IUMMessageUtility.getDouble(mortInfo.getCCRSmoking()));
			}catch(Exception e){
				mrd.setCCR(-1);
			}

			try{
				mrd.setFamilyHistoryCCR(IUMMessageUtility.getDouble(mortInfo.getCCRFamily()));
			}catch(Exception e){
				mrd.setCCR(-1);
			}

			try{
				mrd.setAPDB(IUMMessageUtility.getDouble(mortInfo.getAPDB()));
			}catch(Exception e){
				mrd.setAPDB(-1);
			}

			try{
				mrd.setSmokingAPDB(IUMMessageUtility.getDouble(mortInfo.getAPDBSmoking()));
			}catch(Exception e){
				mrd.setAPDB(-1);
			}

			try{
				mrd.setFamilyHistoryAPDB(IUMMessageUtility.getDouble(mortInfo.getAPDBFamily()));
			}catch(Exception e){
				mrd.setAPDB(-1);
			}

			mrd.setReferenceNumber(refNum);
		} else {
			LOGGER.warn("Mortality Rating is not defined.");
			throw new IUMInterfaceException("Mortality Rating is not defined.");
		}
		LOGGER.info("populateMortalityInfo end");
		return mrd;
	}

	/**
	 * This method is based on parseInsuredClientInfo in ClientRelatedInfoTranslator
	 * @param insInfo - insured information from Ingenium
	 * @return ClientData
	 * @throws IUMInterfaceException
	 */
	private ClientData populateClientInfoInsured(final InsuredInformation insInfo) throws IUMInterfaceException {
		
		LOGGER.info("populateClientInfoInsured start");
		ClientData cd = null;
		try {
			if (insInfo.getClient() != null) {
				cd = new ClientData();
				cd.setLastName(insInfo.getClient().getSurname());
				cd.setGivenName(insInfo.getClient().getGivenName());
				cd.setMiddleName(insInfo.getClient().getMiddleName());
				cd.setOtherLastName(insInfo.getClient().getOtherSurname());
				cd.setOtherGivenName(insInfo.getClient().getOtherGivenName());
				cd.setOtherMiddleName(insInfo.getClient().getOtherMiddleName());
				cd.setTitle(insInfo.getClient().getTitle());
				cd.setSuffix(insInfo.getClient().getSuffix());
				cd.setBirthLocation(insInfo.getClient().getBirthLocation());
				cd.setSex(insInfo.getClient().getSexCode());
				cd.setSmoker(IUMMessageUtility.isTrue(insInfo.getClient().getSmokerCode()));
				cd.setClientId(insInfo.getClient().getId());

				try{
					cd.setBirthDate(DateHelper.parse(insInfo.getClient().getBirthDate(), IUMMessageUtility.DEFAULT_DATE_FORMAT));
				}catch (Exception e){
					// do nothing
				}
				
				try {
					cd.setAge(IUMMessageUtility.getInteger(insInfo.getClient().getAge()));
				}catch (Exception e){
					cd.setAge(-1);
				}
			} else {
				LOGGER.warn("Insured client info is not defined.");
				throw new IUMInterfaceException("Insured client info is not defined.");
			}
		} catch (Exception e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMInterfaceException(e.getMessage());
		}
		LOGGER.info("populateClientInfoInsured end");
		return cd;
	}

	/**
	 * This method is based on parseInsuredClientInfo in ClientRelatedInfoTranslator
	 * @param ownInfo - owner information from Ingenium
	 * @return ClientData
	 * @throws IUMInterfaceException
	 */
	private ClientData populateClientInfoOwner(final OwnerInformation ownInfo) throws IUMInterfaceException {
		
		LOGGER.info("populateClientInfoOwner start");
		ClientData cd = null;
		try {
			if (ownInfo.getClient() != null) {
				cd = new ClientData();
				cd.setLastName(ownInfo.getClient().getSurname());
				cd.setGivenName(ownInfo.getClient().getGivenName());
				cd.setMiddleName(ownInfo.getClient().getMiddleName());
				cd.setOtherLastName(ownInfo.getClient().getOtherSurname());
				cd.setOtherGivenName(ownInfo.getClient().getOtherGivenName());
				cd.setOtherMiddleName(ownInfo.getClient().getOtherMiddleName());
				cd.setTitle(ownInfo.getClient().getTitle());
				cd.setSuffix(ownInfo.getClient().getSuffix());
				cd.setBirthLocation(ownInfo.getClient().getBirthLocation());
				cd.setSex(ownInfo.getClient().getSexCode());
				cd.setSmoker(IUMMessageUtility.isTrue(ownInfo.getClient().getSmokerCode()));
				cd.setClientId(ownInfo.getClient().getId());

				try{
					cd.setBirthDate(DateHelper.parse(ownInfo.getClient().getBirthDate(), IUMMessageUtility.DEFAULT_DATE_FORMAT));
				}catch (Exception e){
					// do nothing
				}
				try {
					cd.setAge(IUMMessageUtility.getInteger(ownInfo.getClient().getAge()));
				}catch (Exception e){
					cd.setAge(-1);
				}
			} else {
				LOGGER.warn("Owner client info is not defined.");
				throw new IUMInterfaceException("Owner client info is not defined.");
			}
		} catch (Exception e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMInterfaceException(e.getMessage());
		}
		LOGGER.info("populateClientInfoOwner start");
		return cd;
	}

	/**
	 * This method is based on parseSummaryCoverage in ClientRelatedInfoTranslator
	 * @param expInfo - existing policy information from Ingenium
	 * @return CDSSummaryPolicyCoverageData
	 * @throws IUMInterfaceException
	 */
	private CDSSummaryPolicyCoverageData populateClientInfoExistingPolicy(
			final ExistingPolicyInformation expInfo) throws IUMInterfaceException {
		
		LOGGER.info("populateClientInfoExistingPolicy start");
		CDSSummaryPolicyCoverageData spcd = null;

		if (expInfo != null) {
			spcd = new CDSSummaryPolicyCoverageData();

			try{
				spcd.setAppliedForBr(IUMMessageUtility.getDouble(expInfo.getAppliedForBR()));
			}catch(Exception e){
				spcd.setAppliedForBr(-1);
			}

			try{
				spcd.setAppliedForAd(IUMMessageUtility.getDouble(expInfo.getAppliedForAD()));
			}catch(Exception e){
				spcd.setAppliedForAd(-1);
			}
			
			try{
				spcd.setPendingAmountBr(IUMMessageUtility.getDouble(expInfo.getPendingBRAmount()));
			}catch(Exception e){
				spcd.setPendingAmountBr(-1);
			}
			
			try{
				spcd.setPendingAmountAd(IUMMessageUtility.getDouble(expInfo.getPendingADAmount()));
			}catch(Exception e){
				spcd.setPendingAmountAd(-1);
			}
			
			try{
				spcd.setExistingInForceBr(IUMMessageUtility.getDouble(expInfo.getExistingBR()));
			}catch(Exception e){
				spcd.setExistingInForceBr(-1);
			}
			
			try{
				spcd.setExistingInForceAd(IUMMessageUtility.getDouble(expInfo.getExistingAD()));
			}catch(Exception e){
				spcd.setExistingInForceAd(-1);
			}
			
			try{
				spcd.setExistingLapsedBr(IUMMessageUtility.getDouble(expInfo.getExistingElapsedBR()));
			}catch(Exception e){
				spcd.setExistingLapsedBr(-1);
			}
			
			try{
				spcd.setExistingLapsedAd(IUMMessageUtility.getDouble(expInfo.getExistingElapsedAD()));
			}catch(Exception e){
				spcd.setExistingInForceAd(-1);
			}
			
			try{
				spcd.setOtherCompaniesBr(IUMMessageUtility.getDouble(expInfo.getOtherInsurerBR()));
			}catch(Exception e){
				spcd.setOtherCompaniesBr(-1);
			}
			
			try{
				spcd.setOtherCompaniesAd(IUMMessageUtility.getDouble(expInfo.getOtherInsurerAD()));
			}catch(Exception e){
				spcd.setOtherCompaniesAd(-1);
			}
			
			try{
				spcd.setTotalCCRCoverage(IUMMessageUtility.getDouble(expInfo.getTotalCCRCoverage()));
			}catch(Exception e){
				spcd.setTotalCCRCoverage(-1);
			}
			
			try{
				spcd.setTotalAPDBCoverage(IUMMessageUtility.getDouble(expInfo.getTotalAPDBCoverage()));
			}catch(Exception e){
				spcd.setTotalAPDBCoverage(-1);
			}
			
			try{
				spcd.setTotalHIBCoverage(IUMMessageUtility.getDouble(expInfo.getTotalHIBCoverage()));
			}catch(Exception e){
				spcd.setTotalHIBCoverage(-1);
			}
			
			try{
				spcd.setTotalFBBMBCoverage(IUMMessageUtility.getDouble(expInfo.getTotalFMBCoverage()));
			}catch(Exception e){
				spcd.setTotalFBBMBCoverage(-1);
			}
			
			try{
				spcd.setTotalReinsuredAmount(IUMMessageUtility.getDouble(expInfo.getTotalReinsuredAmount()));
			}catch(Exception e){
				spcd.setTotalReinsuredAmount(-1);
			}
			
			try {
				spcd.setTotalAdbReinsuredAmount(IUMMessageUtility.getDouble(expInfo.getTotalAdbReinsuredAmount()));
			} catch (Exception e) {
				spcd.setTotalAdbReinsuredAmount(-1);
			}
		} else {
			LOGGER.warn("Summary Policy Coverage is not defined.");
			throw new IUMInterfaceException("Summary Policy Coverage is not defined.");
		}
		LOGGER.info("populateClientInfoExistingPolicy end");
		return spcd;
	}

	/**
	 * This method is based on translatePolicyAndCoverageInfoMessage in RelatedPolicyCoverageTranslator
	 * @param relPol - related policy information from Ingenium
	 * @return PolicyDetailData
	 * @throws IUMInterfaceException
	 */
	private PolicyDetailData populateRelatedPolicyAndCoverageInfo(
			final RelPolicyInformation relPol, String refNum) throws IUMInterfaceException {
		
		LOGGER.info("populateRelatedPolicyAndCoverageInfo start");
		PolicyDetailData pdd = new PolicyDetailData();

		pdd.setPolicyNumber(relPol.getRelPolicyId());
		pdd.setPolicySuffix(relPol.getRelPolicySuffix());
		pdd.setReferenceNumber(refNum);
		pdd.setIssueDate(DateHelper.parse(relPol.getRelCvgIssEffDate(), IUMMessageUtility.DEFAULT_DATE_FORMAT));
		pdd.setCoverageStatus(relPol.getRelCvgCStatCode());
		pdd.setSmoker(IUMMessageUtility.isTrue(relPol.getRelCvgSmokerCode()));
		pdd.setPlanCode(relPol.getRelPlanId());

		try {
			pdd.setCoverageNumber(IUMMessageUtility.getLong(relPol.getRelCvgNumber()));
		} catch (Exception e) {
			pdd.setCoverageNumber(-1);
		}
		
		try {
			pdd.setFaceAmount(IUMMessageUtility.getDouble(relPol.getRelCvgFaceAmount()));
		} catch (Exception e) {
			pdd.setFaceAmount(-1);
		}
		
		try {
			pdd.setADBFaceAmount(IUMMessageUtility.getDouble(relPol.getRelCvgAdbFaceAmount()));
		} catch (Exception e) {
			pdd.setADBFaceAmount(-1);
		}
		
		try {
			pdd.setADMultiplier(IUMMessageUtility.getDouble(relPol.getRelCvgAdMultfct()));
		} catch (Exception e) {
			pdd.setADMultiplier(-1);
		}
		
		try {
			pdd.setWPMultiplier(IUMMessageUtility.getDouble(relPol.getRelCvgWpMultfct()));
		} catch (Exception e) {
			pdd.setWPMultiplier(-1);
		}
		
		try {
			pdd.setReinsuredAmount(IUMMessageUtility.getDouble(relPol.getRelReinsfaceAmount()));
		} catch (Exception e) {
			pdd.setReinsuredAmount(-1);
		}
		
		pdd.setRelCvgMeFct(IUMMessageUtility.getDouble(relPol.getRelCvgMeFct()));
		pdd.setRelCvgFeUpremAmt(IUMMessageUtility.getDouble(relPol.getRelCvgFeUpremAmt()));
		pdd.setRelCvgFePermAmt(IUMMessageUtility.getDouble(relPol.getRelCvgFePermAmt()));
		pdd.setReinstatementDate(DateHelper.parse(relPol.getReinstatementDate(), IUMMessageUtility.DEFAULT_DATE_FORMAT));
		pdd.setReinsuranceType(relPol.getReinsuranceType());
		
		try {
			pdd.setAdbReinsuredAmount(IUMMessageUtility.getDouble(relPol.getAdbFaceAmount()));
		} catch (Exception e) {
			pdd.setAdbReinsuredAmount(-1);
		}
		LOGGER.info("populateRelatedPolicyAndCoverageInfo end");
		return pdd;
	}
}
