/*
 * Created on Apr 11, 2006
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.service.ingenium;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import java.util.Timer;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


//import ph.com.sunlife.wms.ws.ingenium.IngBusinessProcesses;
import ph.com.sunlife.wms.ws.ingenium.IngeniumDispatcher;

import com.slocpi.ium.ExceptionDetail;
import com.slocpi.ium.ExceptionLog;
import com.slocpi.ium.ExceptionLogger;
import com.slocpi.ium.IUMException;
import com.slocpi.ium.data.AssessmentRequestData;
import com.slocpi.ium.data.CDSMortalityRatingData;
import com.slocpi.ium.data.CDSSummaryPolicyCoverageData;
import com.slocpi.ium.data.ClientData;
import com.slocpi.ium.data.ClientDataSheetData;
import com.slocpi.ium.data.IUMMessageData;
import com.slocpi.ium.data.KickOutMessageData;
import com.slocpi.ium.data.LOBData;
import com.slocpi.ium.data.PolicyDetailData;
import com.slocpi.ium.data.PolicyRequirementsData;
import com.slocpi.ium.data.RequirementData;
import com.slocpi.ium.data.StatusData;
import com.slocpi.ium.data.SunLifeOfficeData;
import com.slocpi.ium.data.UserProfileData;
import com.slocpi.ium.data.dao.AssessmentRequestDAO;
import com.slocpi.ium.data.dao.ClientDAO;
import com.slocpi.ium.data.dao.ClientDataSheetDAO;
import com.slocpi.ium.data.dao.KickOutMessageDAO;
import com.slocpi.ium.data.dao.PolicyRequirementDAO;
import com.slocpi.ium.data.dao.RequirementDAO;
import com.slocpi.ium.data.dao.StatusCHDao;
import com.slocpi.ium.data.util.DataSourceProxy;
import com.slocpi.ium.ingenium.transaction.Transaction;
import com.slocpi.ium.ingenium.transaction.TransactionBuilder;
import com.slocpi.ium.ingenium.transaction.data.Kickout;
import com.slocpi.ium.ingenium.transaction.data.ResultStatus;
import com.slocpi.ium.ingenium.transaction.response.ClientDataResponse;
import com.slocpi.ium.ingenium.transaction.response.ClientInquiryResponse;
import com.slocpi.ium.ingenium.transaction.response.KickoutResponse;
import com.slocpi.ium.ingenium.transaction.response.PolicyListResponse;
import com.slocpi.ium.ingenium.transaction.response.RequirementMaintenanceResponse;
import com.slocpi.ium.ingenium.transaction.response.detail.PolicyInformation;
import com.slocpi.ium.interfaces.IUMInterfaceException;
import com.slocpi.ium.service.AdminSystem;
import com.slocpi.ium.underwriter.AssessmentRequest;
import com.slocpi.ium.underwriter.PolicyRequirements;
import com.slocpi.ium.underwriter.UnderWriterException;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.IUMConstants;
import com.slocpi.ium.util.StringHelper;


/**
 * @author Glenn Gonzales
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class IngeniumGateway implements AdminSystem {
 
	private static final Logger LOGGER = LoggerFactory.getLogger(IngeniumGateway.class);
	
	ResourceBundle rb = ResourceBundle.getBundle("ingenium");
	String user = rb.getString("ingenium.userid");
	String pwd = rb.getString("ingenium.password");
	
	public static final String TRANSACTION_POLICY_QUEUE 	= "QPQI";
	public static final String TRANSACTION_REQUIREMENT_MAINTENANCE = "QRQM";

	
	public static final String TRANSACTION_KICK_OUT		= "QCKO";
	public static final String TRANSACTION_CLIENT_DATA_SHEET = "QCRI";
	public static final String TRANSACTION_RELATED_POLICY_AND_COVERAGE = "QPRP";
	public static final String TRANSACTION_REQUIREMENT_INQUIRY = "QRRI";

	public static final int REQTTRANS_CREATE			= 1;
	public static final int REQTTRANS_RECEIVE			= 2;
	public static final int REQTTRANS_ACCEPT			= 3;
	public static final int REQTTRANS_ORDER				= 4;
	public static final int REQTTRANS_REJECT			= 5;
	public static final int REQTTRANS_CANCEL			= 6;
	public static final int REQTTRANS_WAIVE				= 7;
	public static final int REQTTRANS_UPDATE			= 8;

	private String 	useThread;
	private boolean isThread;

	
	private HttpServletRequest request;
	private boolean isPolicyExisting;
	
	/**
	 *
	 */
	public IngeniumGateway() {
		this.isThread = false;
	};

	/**
	 * @param isThread
	 */
	public IngeniumGateway(boolean isThread) {
		this.isThread = isThread;
	};
	
		
	public HttpServletRequest getRequest() {
		return request;
	}

	public void setRequest(HttpServletRequest request) {
		this.request = request;
	}
	
	public boolean isPolicyExisting() {
		return isPolicyExisting;
	}

	public void setPolicyExisting(boolean isPolicyExisting) {
		this.isPolicyExisting = isPolicyExisting;
	}
	

	
	public boolean processCreateUpdateRecord(
			final String refNum,
			final String polSuffix) throws IUMInterfaceException,Exception{
		
		LOGGER.info("processCreateUpdateRecord start");
		boolean result = false;
		boolean isPolicyExisting = false;
		AssessmentRequest ar = new AssessmentRequest();
		
		try {
			
			isPolicyExisting = ar.isAssessmentRequestExist(refNum);
			this.isPolicyExisting = isPolicyExisting;
			
			if(isPolicyExisting){
				processPolicyDetails(refNum, polSuffix);
				result = true;
			} else {
				processGetPolicyQueue(refNum, polSuffix);
				result = true;
			}
		} catch (IUMException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
		}
		LOGGER.info("processCreateUpdateRecord end");
		return result;
	}
	
	/**
	 * This method is responsible for retrieving the policy details from Ingenium
	 * @param refNum policy reference number
	 * @throws IUMInterfaceException
	 */
	public void processPolicyDetails(
			final String refNum,
			final String polSuffix) throws IUMInterfaceException,Exception {
		
		LOGGER.info("processPolicyDetails start 1");
		List 			   		koList = null;
		List			   		requirementList = null;
		ArrayList 				requirements = null;
		ArrayList 		   		exceptionLogs = new ArrayList();
		ArrayList 				relatedPolicies = new ArrayList();
		KickoutResponse			koResponse = null;
		KickOutMessageData 		koData = null;
		PolicyRequirementsData 	reqData = null;
		AssessmentRequestData 	ard = null;
		AssessmentRequest 		ar = new AssessmentRequest();;
		ClientData 				ownerData = null;
		ClientData 				insuredData = null;
		ExceptionLogger    		errorLogger = new ExceptionLogger();
		IngeniumPopulator  		ingPop = new IngeniumPopulator();
		boolean			   		noExceptions = true;
		String 			   		currentTrans = "";
		String 					suffix = null;
		Transaction 			tb = null;
		List 					kickOutList = new ArrayList();
		AssessmentRequestData	ardInd = new AssessmentRequestData();

		try {
						
			
			suffix = polSuffix;
			
			tb = TransactionBuilder.buildKickoutTransaction(refNum, suffix);
		
			/***TRANSACTION_KICK_OUT - KickoutTransaction***/
			currentTrans = TRANSACTION_KICK_OUT;
			koResponse = (KickoutResponse) tb.execute();
			
			koList = koResponse.getKickOutDetails();
			
			if ((koList != null) && (koList.size() > 0)) {
				
				Connection conn = new DataSourceProxy().getConnection();
				try{
					for (int i=0; i < koList.size(); i++) {
						koData = ingPop.populateKOMessageData((Kickout) koList.get(i), refNum);
						
						ExceptionLog exLog = validateKOMessages(koData, conn);
						if (exLog != null){
							exLog.setRecordType(TRANSACTION_KICK_OUT);
							exLog.setReferenceNumber(refNum);
							exLog.setReply(tb.getResponse());
							exLog.setSystemInterface(IUMConstants.SYSTEM_INGENIUM_DESC);
							exceptionLogs.add(exLog);					
							noExceptions = false;
							StringBuffer sb = getExceptionLogs(exLog);
							LOGGER.error(sb.toString());
							throw new IUMInterfaceException(sb.toString());
						} else {
							kickOutList.add(koData);
						}
						
						// wms
						if(i == 0){
							ardInd.setKoCountInd(((Kickout)koList.get(i)).getKOcountIndicator());
						}
					}					
				} finally {
					closeConnection(conn);					
				}
			}
			
			if (exceptionLogs.size() > 0) {
				errorLogger.logError(exceptionLogs);
			}

			/***TRANSACTION_CLIENT_DATA_SHEET - ClientDataTransaction***/
			tb = TransactionBuilder.buildClientDataTransaction(refNum, suffix);
			ard = new AssessmentRequestData();

			
			ingPop.populateClientRelatedInfo(ard,
											 refNum,
											 relatedPolicies,
											(ClientDataResponse) tb.execute());
			
			try{
				
				if(ard.getOwner().getSex() != null
						&& "C".equalsIgnoreCase(ard.getOwner().getSex())){
					Transaction tbClient = TransactionBuilder.buildClientInquiryTransaction(ard.getOwner().getClientId());
					ClientInquiryResponse res = (ClientInquiryResponse)tbClient.execute();
					ClientData clData = ard.getOwner(); 
					clData.setGivenName(res.getClientName());
					ard.setOwner(clData);
				}
				
			}catch(Exception e){
				LOGGER.error(CodeHelper.getStackTrace(e));
			}
			
			ownerData = ard.getOwner();
			insuredData = ard.getInsured();
			
			
			if (insuredData == null || (insuredData != null && StringHelper.isEmpty(insuredData.getClientId()))){
				insuredData = copyClientData(ownerData);
				ard.setInsured(insuredData);
			}			

			if(ownerData.getClientId() != null && insuredData.getClientId()!=null && ownerData.getClientId().equals(insuredData.getClientId())){
				ExceptionLog ownerDataException = validateClientInfo(ownerData);
				if (ownerDataException != null){
					ownerDataException.setRecordType(TRANSACTION_CLIENT_DATA_SHEET);
					ownerDataException.setReferenceNumber(refNum);
					ownerDataException.setReply(tb.getResponse());
					ownerDataException.setSystemInterface(IUMConstants.SYSTEM_INGENIUM_DESC);
					errorLogger.logError(ownerDataException);
					noExceptions = false;
					StringBuffer sb = getExceptionLogs(ownerDataException);
					LOGGER.error(sb.toString());
					throw new IUMInterfaceException(sb.toString());
				}
			}else{
				if(!"C".equalsIgnoreCase(ownerData.getSex())){
					ExceptionLog ownerDataException = validateClientInfo(ownerData);
					if (ownerDataException != null){
						ownerDataException.setRecordType(TRANSACTION_CLIENT_DATA_SHEET);
						ownerDataException.setReferenceNumber(refNum);
						ownerDataException.setReply(tb.getResponse());
						ownerDataException.setSystemInterface(IUMConstants.SYSTEM_INGENIUM_DESC);
						errorLogger.logError(ownerDataException);
						noExceptions = false;
						StringBuffer sb = getExceptionLogs(ownerDataException);
						LOGGER.error(sb.toString());
						throw new IUMInterfaceException(sb.toString());
					}
				}
				
				ExceptionLog insuredDataException = validateClientInfo(insuredData);
				if (insuredDataException != null){
					insuredDataException.setRecordType(TRANSACTION_CLIENT_DATA_SHEET);
					insuredDataException.setReferenceNumber(refNum);
					insuredDataException.setReply(tb.getResponse());
					insuredDataException.setSystemInterface(IUMConstants.SYSTEM_INGENIUM_DESC);
					errorLogger.logError(insuredDataException);
					noExceptions = false;
					StringBuffer sb = getExceptionLogs(insuredDataException);
					LOGGER.error(sb.toString());
					throw new IUMInterfaceException(sb.toString());
				} 
			}
		
			CDSSummaryPolicyCoverageData policyCoverage = ard.getClientDataSheet().getSummaryOfPolicies();
  			ExceptionLog policyCoverageException = validateSummaryPolicyCoverage(policyCoverage);

			if (policyCoverageException != null){
				policyCoverageException.setRecordType(TRANSACTION_CLIENT_DATA_SHEET);
				policyCoverageException.setReferenceNumber(refNum);
				policyCoverageException.setReply(tb.getResponse());
				policyCoverageException.setSystemInterface(IUMConstants.SYSTEM_INGENIUM_DESC);
				errorLogger.logError(policyCoverageException);
				noExceptions = false;
				StringBuffer sb = getExceptionLogs(policyCoverageException);
				LOGGER.error(sb.toString());
				throw new IUMInterfaceException(sb.toString());
			} 

			
			if(ownerData.getClientId() != null && insuredData.getClientId() != null && ownerData.getClientId().equals(insuredData.getClientId())){
				CDSMortalityRatingData ownerMortality = ard.getOwnerMortalityRating();
				ExceptionLog oMortalityException = validateMortalityRating(ownerMortality);
				if (oMortalityException != null){
					oMortalityException.setRecordType(TRANSACTION_CLIENT_DATA_SHEET);
					oMortalityException.setReferenceNumber(refNum);
					oMortalityException.setReply(tb.getResponse());
					oMortalityException.setSystemInterface(IUMConstants.SYSTEM_INGENIUM_DESC);
					errorLogger.logError(oMortalityException);
					noExceptions = false;
					StringBuffer sb = getExceptionLogs(oMortalityException);
					LOGGER.error(sb.toString());
					throw new IUMInterfaceException(sb.toString());
				}
			}else{

				if(ownerData.getSex() != null && !"C".equalsIgnoreCase(ownerData.getSex())){
					CDSMortalityRatingData ownerMortality = ard.getOwnerMortalityRating();
					ExceptionLog oMortalityException = validateMortalityRating(ownerMortality);
					if (oMortalityException != null){
						oMortalityException.setRecordType(TRANSACTION_CLIENT_DATA_SHEET);
						oMortalityException.setReferenceNumber(refNum);
						oMortalityException.setReply(tb.getResponse());
						oMortalityException.setSystemInterface(IUMConstants.SYSTEM_INGENIUM_DESC);
						errorLogger.logError(oMortalityException);
						noExceptions = false;
						StringBuffer sb = getExceptionLogs(oMortalityException);
						LOGGER.error(sb.toString());
						throw new IUMInterfaceException(sb.toString());
					}
				}

				CDSMortalityRatingData insuredMortality = ard.getInsuredMortalityRating();
				ExceptionLog iMortalityException = validateMortalityRating(insuredMortality);
				if (iMortalityException != null){
					iMortalityException.setRecordType(TRANSACTION_CLIENT_DATA_SHEET);
					iMortalityException.setReferenceNumber(refNum);
					iMortalityException.setReply(tb.getResponse());
					iMortalityException.setSystemInterface(IUMConstants.SYSTEM_INGENIUM_DESC);
					errorLogger.logError(iMortalityException);
					noExceptions = false;
					StringBuffer sb = getExceptionLogs(iMortalityException);
					LOGGER.error(sb.toString());
					throw new IUMInterfaceException(sb.toString());
				} 
			}
			
			
			ard.setKickOutMessages((ArrayList) kickOutList);

			currentTrans = TRANSACTION_RELATED_POLICY_AND_COVERAGE;
			ArrayList policyDetailExceptions = new ArrayList();

			
			if (relatedPolicies != null && relatedPolicies.size() > 0){
				
				Connection conn = new DataSourceProxy().getConnection();
				try {
					for (int i=0; i < relatedPolicies.size(); i++){
						PolicyDetailData policyDetail = (PolicyDetailData) relatedPolicies.get(i);
						ExceptionLog polDetailException = validatePolicyCoverageDetails(policyDetail);

						if (polDetailException  != null){
							polDetailException.setRecordType(TRANSACTION_RELATED_POLICY_AND_COVERAGE);
							polDetailException.setReferenceNumber(refNum);
							polDetailException.setReply(tb.getResponse());
							polDetailException.setSystemInterface(IUMConstants.SYSTEM_INGENIUM_DESC);
							policyDetailExceptions.add(polDetailException);
							noExceptions = false;
							StringBuffer sb = getExceptionLogs(polDetailException);
							LOGGER.error(sb.toString());
							throw new IUMInterfaceException(sb.toString());
						}
					} 
				} finally {
					closeConnection(conn);
				}
			}

			if (policyDetailExceptions.size() > 0){
				errorLogger.logError(policyDetailExceptions);
			}

			
			ClientDataSheetData cds = new ClientDataSheetData();
			cds = ard.getClientDataSheet();
			cds.setPolicyCoverageDetailsList(relatedPolicies);

			LOBData lob = new LOBData();
			lob.setLOBCode(IUMConstants.LOB_INDIVIDUAL_LIFE);
			ard.setLob(lob);

			
			currentTrans = TRANSACTION_REQUIREMENT_INQUIRY;
			requirementList = koResponse.getRequirementList();
			requirements = ingPop.populateRequirements(requirementList, refNum);
			
			if (requirements != null && requirements.size() > 0){
				
				Connection conn = new DataSourceProxy().getConnection();
				
				try {

					for (int i = 0; i < requirements.size(); i++){
						reqData = (PolicyRequirementsData) requirements.get(i);
						ExceptionLog reqException = validatePolicyRequirements(reqData, conn);
						if (reqException != null){
							reqException.setRecordType(TRANSACTION_REQUIREMENT_INQUIRY);
							reqException.setReferenceNumber(refNum);
							reqException.setReply(tb.getResponse());
							reqException.setSystemInterface(IUMConstants.SYSTEM_INGENIUM_DESC);
							errorLogger.logError(reqException);
							noExceptions = false;
							StringBuffer sb = getExceptionLogs(reqException);
							LOGGER.error(sb.toString());
							throw new IUMInterfaceException(sb.toString());
						}
					}					
				} finally {
					closeConnection(conn);
				}
			}

			if (noExceptions){
				
				try {
					
					
					KickOutMessageDAO ko = new KickOutMessageDAO();
					ko.deleteKOMessages(refNum);
					
					ar.transmitIngeniumDataToIUMDBWMS(ard);
					if ((requirements != null) && (requirements.size() > 0)) {
						PolicyRequirements pr = new PolicyRequirements();
						PolicyRequirementDAO requirementsDAO = new PolicyRequirementDAO();
						
						for (int i=0; i < requirements.size(); i++) {
							
							PolicyRequirementsData prd = (PolicyRequirementsData)requirements.get(i);
							
							if(!requirementsDAO.isRequirementExisting(prd)){
								pr.createRequirementIUM(prd, ard, null);//???
							}else{
								
								String updatedBy=isUpdatedByValid(prd.getUpdatedBy());
								prd.setUpdatedBy(updatedBy);
								requirementsDAO.updateRequirementRetainComment(prd);
							}
							
							
							if(i==0){
								ardInd.setReqtCountInd(((PolicyRequirementsData)requirements.get(i)).getReqtCountInd());
							}
						}
						
						List IUMReqts = new ArrayList();
						boolean isMatched = false;
	
						try{
							if(getRequest()!=null && getRequest().getAttribute("IUMReqts_"+refNum)!=null){
								IUMReqts = (ArrayList)getRequest().getAttribute("IUMReqts_"+refNum);
							}else{
								IUMReqts = requirementsDAO.retrieveRequirements(refNum);
							}
						}catch(Exception ex){
							LOGGER.error(CodeHelper.getStackTrace(ex));
							IUMReqts = requirementsDAO.retrieveRequirements(refNum);
						}
						for(int i=0; i < IUMReqts.size();i++){
							
							PolicyRequirementsData ium = (PolicyRequirementsData)IUMReqts.get(i);
							for(int j=0;j< requirements.size();j++){
								
								PolicyRequirementsData ingenium = (PolicyRequirementsData)requirements.get(j);
								
								
								if(ium.getLevel().equalsIgnoreCase(IUMConstants.LEVEL_CLIENT)){
									if(ium.getReferenceNumber().equalsIgnoreCase(ingenium.getReferenceNumber())
											&& ium.getRequirementCode().equalsIgnoreCase(ingenium.getRequirementCode())
											&& ium.getLevel().equalsIgnoreCase(ingenium.getLevel())
											&& ium.getClientId().equalsIgnoreCase(ingenium.getClientId())){
										isMatched = true;
										break;
									}else{
										isMatched = false;
									}
								}else{	
									if(ium.getReferenceNumber().equalsIgnoreCase(ingenium.getReferenceNumber())
											&& ium.getRequirementCode().equalsIgnoreCase(ingenium.getRequirementCode())
											&& ium.getLevel().equalsIgnoreCase(ingenium.getLevel())){
										
										isMatched = true;
										break;
									}else{
										isMatched = false;
									}
								}
							}
							
							// if no match found
							if (!isMatched){
								requirementsDAO.deletePolicyRequirement(ium.getRequirementId());
							}
						}
					}
					
					ar.setSuccessfulRetrieval(ard.getReferenceNumber(), IUMConstants.INDICATOR_SUCCESS, null);
										
				} catch (IUMInterfaceException e){
					
					logError(e,currentTrans);
					LOGGER.error(CodeHelper.getStackTrace(e));
				}
				
				try {
				
					ArrayList policyCoverageDetailList = retrievePolicyCoverageDetailList(refNum, suffix);
					ar.transmitIngeniumPolicyCoverageDetailToIUMDBWMS(policyCoverageDetailList);	
				} catch (Exception e){
					LOGGER.error(CodeHelper.getStackTrace(e));
				} 
			}
			
			// wms - sets the count indicators in the assessment request table
			ardInd.setReferenceNumber(refNum);
			ar.updateKOandReqtCountInd(ardInd);

		} catch (Exception e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			logError(e, currentTrans);
		}
		LOGGER.info("processPolicyDetails end 1");
	}
	
	
	/**
	 * This method is responsible for retrieving the policy details from Ingenium
	 * @param refNum policy reference number
	 * @throws IUMInterfaceException
	 */
	public void processPolicyDetails(String refNum)
									 throws IUMInterfaceException {
		
		LOGGER.info("processPolicyDetails start 2");
		List 			   		koList = null;
		List			   		requirementList = null;
		ArrayList 				requirements = null;
		ArrayList 		   		exceptionLogs = new ArrayList();
		ArrayList 				relatedPolicies = new ArrayList();
		KickoutResponse			koResponse = null;
		KickOutMessageData 		koData = null;
		PolicyRequirementsData 	reqData = null;
		AssessmentRequestData 	ard = null;
		AssessmentRequest 		ar = new AssessmentRequest();;
		ClientData 				ownerData = null;
		ClientData 				insuredData = null;
		ExceptionLogger    		errorLogger = new ExceptionLogger();
		IngeniumPopulator  		ingPop = new IngeniumPopulator();
		boolean			   		noExceptions = true;
		String 			   		currentTrans = "";
		String 					suffix = null;
		Transaction 			tb = null;
		List 					kickOutList = new ArrayList();

		try {
			suffix = ar.getPolicySuffix(refNum);
			if (suffix != null) {
				tb = TransactionBuilder.buildKickoutTransaction(refNum, suffix);
			} else {
				LOGGER.warn("Policy Suffix not available for reference number: " + refNum);
				throw new IUMInterfaceException("Policy Suffix not available for reference number: " + refNum);
			}
			
			/***TRANSACTION_KICK_OUT - KickoutTransaction***/
			currentTrans = TRANSACTION_KICK_OUT;
			koResponse = (KickoutResponse) tb.execute();
			koList = koResponse.getKickOutDetails();
			if ((koList != null) && (koList.size() > 0)) {
				
				Connection conn = new DataSourceProxy().getConnection();
				try{
					for (int i=0; i < koList.size(); i++) {
						koData = ingPop.populateKOMessageData((Kickout) koList.get(i), refNum);
						ExceptionLog exLog = validateKOMessages(koData, conn);
						if (exLog != null){
							exLog.setRecordType(TRANSACTION_KICK_OUT);
							exLog.setReferenceNumber(refNum);
							exLog.setReply(tb.getResponse());
							exLog.setSystemInterface(IUMConstants.SYSTEM_INGENIUM_DESC);
							exceptionLogs.add(exLog);
							noExceptions = false;
							LOGGER.error(exLog.toString());
						} else {
							kickOutList.add(koData);
						}
					}					
				} finally {
					closeConnection(conn);					
				}
			}
			
			requirementList = koResponse.getRequirementList();

			if (exceptionLogs.size() > 0) {
				errorLogger.logError(exceptionLogs);
			}

			/***TRANSACTION_CLIENT_DATA_SHEET - ClientDataTransaction***/
			tb = TransactionBuilder.buildClientDataTransaction(refNum, suffix);
			ard = new AssessmentRequestData();

			
			ingPop.populateClientRelatedInfo(ard,
											 refNum,
											 relatedPolicies,
											(ClientDataResponse) tb.execute());

			
			try{
				if(ard.getOwner().getSex()!=null && "C".equalsIgnoreCase(ard.getOwner().getSex())){
					Transaction tbClient = TransactionBuilder.buildClientInquiryTransaction(ard.getOwner().getClientId());
					ClientInquiryResponse res = (ClientInquiryResponse)tbClient.execute();
					ClientData clData = ard.getOwner(); 
					clData.setGivenName(res.getClientName());
					ard.setOwner(clData);
				}
				
			}catch(Exception e){
				LOGGER.error(CodeHelper.getStackTrace(e));
			}
			
			ownerData = ard.getOwner();
			insuredData = ard.getInsured();
			
			if (insuredData == null || (insuredData != null && StringHelper.isEmpty(insuredData.getClientId()))){
				insuredData = copyClientData(ownerData);
				ard.setInsured(insuredData);
			}			

			if(ownerData.getClientId()!=null && insuredData.getClientId()!=null && ownerData.getClientId().equals(insuredData.getClientId())){
				
				ExceptionLog ownerDataException = validateClientInfo(ownerData);
				if (ownerDataException != null){
					ownerDataException.setRecordType(TRANSACTION_CLIENT_DATA_SHEET);
					ownerDataException.setReferenceNumber(refNum);
					ownerDataException.setReply(tb.getResponse());
					ownerDataException.setSystemInterface(IUMConstants.SYSTEM_INGENIUM_DESC);
					errorLogger.logError(ownerDataException);
					noExceptions = false;
					LOGGER.error(ownerDataException.toString());
				}

			}else{
				
				ExceptionLog ownerDataException = validateClientInfo(ownerData);
				if (ownerDataException != null){
					ownerDataException.setRecordType(TRANSACTION_CLIENT_DATA_SHEET);
					ownerDataException.setReferenceNumber(refNum);
					ownerDataException.setReply(tb.getResponse());
					ownerDataException.setSystemInterface(IUMConstants.SYSTEM_INGENIUM_DESC);
					errorLogger.logError(ownerDataException);
					noExceptions = false;
					LOGGER.error(ownerDataException.toString());
				}

				ExceptionLog insuredDataException = validateClientInfo(insuredData);
				if (insuredDataException != null){
					insuredDataException.setRecordType(TRANSACTION_CLIENT_DATA_SHEET);
					insuredDataException.setReferenceNumber(refNum);
					insuredDataException.setReply(tb.getResponse());
					insuredDataException.setSystemInterface(IUMConstants.SYSTEM_INGENIUM_DESC);
					errorLogger.logError(insuredDataException);
					noExceptions = false;
					LOGGER.error(insuredDataException.toString());
				} 
				
			}
			
			
			CDSSummaryPolicyCoverageData policyCoverage = ard.getClientDataSheet().getSummaryOfPolicies();
  			ExceptionLog policyCoverageException = validateSummaryPolicyCoverage(policyCoverage);

			if (policyCoverageException != null){
				policyCoverageException.setRecordType(TRANSACTION_CLIENT_DATA_SHEET);
				policyCoverageException.setReferenceNumber(refNum);
				policyCoverageException.setReply(tb.getResponse());
				policyCoverageException.setSystemInterface(IUMConstants.SYSTEM_INGENIUM_DESC);
				errorLogger.logError(policyCoverageException);
				noExceptions = false;
				LOGGER.error(policyCoverageException.toString());
			} 

			
			// Launch IUM Enhancement - Andre Ceasar Dacanay - start
			if(ownerData.getClientId()!=null && insuredData.getClientId()!=null && ownerData.getClientId().equals(insuredData.getClientId())){
				CDSMortalityRatingData ownerMortality = ard.getOwnerMortalityRating();
				ExceptionLog oMortalityException = validateMortalityRating(ownerMortality);
				if (oMortalityException != null){
					oMortalityException.setRecordType(TRANSACTION_CLIENT_DATA_SHEET);
					oMortalityException.setReferenceNumber(refNum);
					oMortalityException.setReply(tb.getResponse());
					oMortalityException.setSystemInterface(IUMConstants.SYSTEM_INGENIUM_DESC);
					errorLogger.logError(oMortalityException);
					noExceptions = false;
					LOGGER.error(oMortalityException.toString());
				}
			}else{
				if(ownerData.getSex()!=null && !"C".equalsIgnoreCase(ownerData.getSex())){
					CDSMortalityRatingData ownerMortality = ard.getOwnerMortalityRating();
					ExceptionLog oMortalityException = validateMortalityRating(ownerMortality);
					if (oMortalityException != null){
						oMortalityException.setRecordType(TRANSACTION_CLIENT_DATA_SHEET);
						oMortalityException.setReferenceNumber(refNum);
						oMortalityException.setReply(tb.getResponse());
						oMortalityException.setSystemInterface(IUMConstants.SYSTEM_INGENIUM_DESC);
						errorLogger.logError(oMortalityException);
						noExceptions = false;
						LOGGER.error(oMortalityException.toString());
					}
				}
				
				
				CDSMortalityRatingData insuredMortality = ard.getInsuredMortalityRating();
				ExceptionLog iMortalityException = validateMortalityRating(insuredMortality);
				if (iMortalityException != null){
					iMortalityException.setRecordType(TRANSACTION_CLIENT_DATA_SHEET);
					iMortalityException.setReferenceNumber(refNum);
					iMortalityException.setReply(tb.getResponse());
					iMortalityException.setSystemInterface(IUMConstants.SYSTEM_INGENIUM_DESC);
					errorLogger.logError(iMortalityException);
					noExceptions = false;
					LOGGER.error(iMortalityException.toString());
				} 
			}

			ard.setKickOutMessages((ArrayList) kickOutList);

			/***TRANSACTION_RELATED_POLICY_AND_COVERAGE***/
			currentTrans = TRANSACTION_RELATED_POLICY_AND_COVERAGE;
			ArrayList policyDetailExceptions = new ArrayList();

			
			if (relatedPolicies != null && relatedPolicies.size() > 0){
				
				Connection conn = new DataSourceProxy().getConnection();
				try {
					for (int i=0; i < relatedPolicies.size(); i++){
						PolicyDetailData policyDetail = (PolicyDetailData) relatedPolicies.get(i);
						ExceptionLog polDetailException = validatePolicyCoverageDetails(policyDetail);

						if (polDetailException  != null){
							polDetailException.setRecordType(TRANSACTION_RELATED_POLICY_AND_COVERAGE);
							polDetailException.setReferenceNumber(refNum);
							polDetailException.setReply(tb.getResponse());
							polDetailException.setSystemInterface(IUMConstants.SYSTEM_INGENIUM_DESC);
							policyDetailExceptions.add(polDetailException);
							noExceptions = false;
							LOGGER.error(polDetailException.toString());
						}
					} 
				} finally {
					closeConnection(conn);
				}
			}


			
			if (policyDetailExceptions.size() > 0){
				errorLogger.logError(policyDetailExceptions);
			}

			ClientDataSheetData cds = new ClientDataSheetData();
			cds = ard.getClientDataSheet();
			cds.setPolicyCoverageDetailsList(relatedPolicies);

			LOBData lob = new LOBData();
			lob.setLOBCode(IUMConstants.LOB_INDIVIDUAL_LIFE);
			ard.setLob(lob);

			/***TRANSACTION_REQUIREMENT_INQUIRY - RequirementMaintenanceTransaction***/
			currentTrans = TRANSACTION_REQUIREMENT_INQUIRY;
			requirements = ingPop.populateRequirements(requirementList, refNum);
			
			if (requirements != null && requirements.size() > 0){
				
				Connection conn = new DataSourceProxy().getConnection();
				try {
					for (int i = 0; i < requirements.size(); i++){
						reqData = (PolicyRequirementsData) requirements.get(i);

						ExceptionLog reqException = validatePolicyRequirements(reqData, conn);
						if (reqException != null){
							reqException.setRecordType(TRANSACTION_REQUIREMENT_INQUIRY);
							reqException.setReferenceNumber(refNum);
							reqException.setReply(tb.getResponse());
							reqException.setSystemInterface(IUMConstants.SYSTEM_INGENIUM_DESC);
							errorLogger.logError(reqException);
							noExceptions = false;
							LOGGER.error(reqException.toString());
						}
					}					
				} finally {
					closeConnection(conn);
				}
				
			}

			if (noExceptions){
				Connection conn = null;
				try {
					conn = new DataSourceProxy().getConnection();
					conn.setAutoCommit(false);
					ar.transmitIngeniumDataToIUMDB(ard,conn);
					if ((requirements != null) && (requirements.size() > 0)) {
						PolicyRequirements pr = new PolicyRequirements();
						for (int i=0; i < requirements.size(); i++) {
							PolicyRequirementsData prd = (PolicyRequirementsData)requirements.get(i);
							pr.createRequirementIUM(prd,ard,conn);
						}
					}
					ar.setSuccessfulRetrieval(ard.getReferenceNumber(),IUMConstants.INDICATOR_SUCCESS,conn);
					conn.commit();					
				} catch (IUMInterfaceException e){
					rollbackTransaction(conn);
					logError(e,currentTrans);
					LOGGER.error(CodeHelper.getStackTrace(e));
				} finally {
					closeConnection(conn);
				}

				tb = TransactionBuilder.buildDeletePolicyTransaction(refNum,
																	 suffix);
				tb.execute();
				
			}

		} catch (Exception e) {
			logError(e, currentTrans);
			LOGGER.error(CodeHelper.getStackTrace(e));
		} 
		LOGGER.info("processPolicyDetails end 2");
	}

	
	/**
	 * (Overload)This method is responsible for retrieving the policy details from Ingenium 
	 * @param refNum policy reference number
	 * @param conn Connection object
	 * @throws IUMInterfaceException
	 */
	public void processPolicyDetails(String refNum, Connection conn)
									 throws IUMInterfaceException {
		
		LOGGER.info("processPolicyDetails start 3");
		List 			   		koList = null;
		List			   		requirementList = null;
		ArrayList 				requirements = null;
		ArrayList 		   		exceptionLogs = new ArrayList();
		ArrayList 				relatedPolicies = new ArrayList();
		KickoutResponse			koResponse = null;
		KickOutMessageData 		koData = null;
		PolicyRequirementsData 	reqData = null;
		AssessmentRequestData 	ard = null;
		AssessmentRequest 		ar = new AssessmentRequest();;
		ClientData 				ownerData = null;
		ClientData 				insuredData = null;
		ExceptionLogger    		errorLogger = new ExceptionLogger();
		IngeniumPopulator  		ingPop = new IngeniumPopulator();
		boolean			   		noExceptions = true;
		String 			   		currentTrans = "";
		String 					suffix = null;
		Transaction 			tb = null;
		List 					kickOutList = new ArrayList();

		try {
			suffix = ar.getPolicySuffix(refNum);
			if (suffix != null) {
				tb = TransactionBuilder.buildKickoutTransaction(refNum, suffix);
			} else {
				LOGGER.warn("Policy Suffix not available for reference number: " + refNum);
				throw new IUMInterfaceException("Policy Suffix not available for reference number: " + refNum);
			}
			
			

			/***TRANSACTION_KICK_OUT - KickoutTransaction***/
			currentTrans = TRANSACTION_KICK_OUT;
			koResponse = (KickoutResponse) tb.execute();
			koList = koResponse.getKickOutDetails();
			if ((koList != null) && (koList.size() > 0)) {
				for (int i=0; i < koList.size(); i++) {
						koData = ingPop.populateKOMessageData((Kickout) koList.get(i), refNum);
						ExceptionLog exLog = validateKOMessages(koData, conn);
						if (exLog != null){
							exLog.setRecordType(TRANSACTION_KICK_OUT);
							exLog.setReferenceNumber(refNum);
							exLog.setReply(tb.getResponse());
							exLog.setSystemInterface(IUMConstants.SYSTEM_INGENIUM_DESC);
							exceptionLogs.add(exLog);
							noExceptions = false;
							LOGGER.error(exLog.toString());
						} else {
							kickOutList.add(koData);
						}
					}					
			}
			
			requirementList = koResponse.getRequirementList();
			if (exceptionLogs.size() > 0) {
				errorLogger.logError(exceptionLogs);
			}

			/***TRANSACTION_CLIENT_DATA_SHEET - ClientDataTransaction***/
			currentTrans = TRANSACTION_CLIENT_DATA_SHEET;
			tb = TransactionBuilder.buildClientDataTransaction(refNum, suffix);
			ard = new AssessmentRequestData();

			
			ingPop.populateClientRelatedInfo(ard,
											 refNum,
											 relatedPolicies,
											(ClientDataResponse) tb.execute());

		
			try{
				if(ard.getOwner().getSex()!=null &&  "C".equalsIgnoreCase(ard.getOwner().getSex())){
					Transaction tbClient = TransactionBuilder.buildClientInquiryTransaction(ard.getOwner().getClientId());
					ClientInquiryResponse res = (ClientInquiryResponse)tbClient.execute();
					ClientData clData = ard.getOwner(); 
					clData.setGivenName(res.getClientName());
					ard.setOwner(clData);
				}
			}catch(Exception e){
				LOGGER.error(CodeHelper.getStackTrace(e));
			}
	
			ownerData = ard.getOwner();
			insuredData = ard.getInsured();
		
			if (insuredData == null || (insuredData != null && StringHelper.isEmpty(insuredData.getClientId()))){
				insuredData = copyClientData(ownerData);
				ard.setInsured(insuredData);
			}			

			ExceptionLog ownerDataException = validateClientInfo(ownerData);
			if (ownerDataException != null){
				ownerDataException.setRecordType(TRANSACTION_CLIENT_DATA_SHEET);
				ownerDataException.setReferenceNumber(refNum);
				ownerDataException.setReply(tb.getResponse());
				ownerDataException.setSystemInterface(IUMConstants.SYSTEM_INGENIUM_DESC);
				errorLogger.logError(ownerDataException);
				noExceptions = false;
				LOGGER.error(ownerDataException.toString());
			}

			ExceptionLog insuredDataException = validateClientInfo(insuredData);
			if (insuredDataException != null){
				insuredDataException.setRecordType(TRANSACTION_CLIENT_DATA_SHEET);
				insuredDataException.setReferenceNumber(refNum);
				insuredDataException.setReply(tb.getResponse());
				insuredDataException.setSystemInterface(IUMConstants.SYSTEM_INGENIUM_DESC);
				errorLogger.logError(insuredDataException);
				noExceptions = false;
				LOGGER.error(insuredDataException.toString());
			} // end of Validation

			CDSSummaryPolicyCoverageData policyCoverage = ard.getClientDataSheet().getSummaryOfPolicies();
  			ExceptionLog policyCoverageException = validateSummaryPolicyCoverage(policyCoverage);

			if (policyCoverageException != null){
				policyCoverageException.setRecordType(TRANSACTION_CLIENT_DATA_SHEET);
				policyCoverageException.setReferenceNumber(refNum);
				policyCoverageException.setReply(tb.getResponse());
				policyCoverageException.setSystemInterface(IUMConstants.SYSTEM_INGENIUM_DESC);
				errorLogger.logError(policyCoverageException);
				noExceptions = false;
				LOGGER.error(policyCoverageException.toString());
			} 

			CDSMortalityRatingData ownerMortality = ard.getOwnerMortalityRating();
			CDSMortalityRatingData insuredMortality = ard.getInsuredMortalityRating();

			ExceptionLog oMortalityException = validateMortalityRating(ownerMortality);
			if (oMortalityException != null){
				oMortalityException.setRecordType(TRANSACTION_CLIENT_DATA_SHEET);
				oMortalityException.setReferenceNumber(refNum);
				oMortalityException.setReply(tb.getResponse());
				oMortalityException.setSystemInterface(IUMConstants.SYSTEM_INGENIUM_DESC);
				errorLogger.logError(oMortalityException);
				noExceptions = false;
				LOGGER.error(oMortalityException.toString());
			}

			ExceptionLog iMortalityException = validateMortalityRating(insuredMortality);
			if (iMortalityException != null){
				iMortalityException.setRecordType(TRANSACTION_CLIENT_DATA_SHEET);
				iMortalityException.setReferenceNumber(refNum);
				iMortalityException.setReply(tb.getResponse());
				iMortalityException.setSystemInterface(IUMConstants.SYSTEM_INGENIUM_DESC);
				errorLogger.logError(iMortalityException);
				noExceptions = false;
				LOGGER.error(iMortalityException.toString());
			} 

			ard.setKickOutMessages((ArrayList) kickOutList);

			/***TRANSACTION_RELATED_POLICY_AND_COVERAGE***/
			currentTrans = TRANSACTION_RELATED_POLICY_AND_COVERAGE;
			ArrayList policyDetailExceptions = new ArrayList();

		
			if (relatedPolicies != null && relatedPolicies.size() > 0){
					for (int i=0; i < relatedPolicies.size(); i++){
						PolicyDetailData policyDetail = (PolicyDetailData) relatedPolicies.get(i);
						ExceptionLog polDetailException = validatePolicyCoverageDetails(policyDetail);

						if (polDetailException  != null){
							polDetailException.setRecordType(TRANSACTION_RELATED_POLICY_AND_COVERAGE);
							polDetailException.setReferenceNumber(refNum);
							polDetailException.setReply(tb.getResponse());
							polDetailException.setSystemInterface(IUMConstants.SYSTEM_INGENIUM_DESC);
							policyDetailExceptions.add(polDetailException);
							noExceptions = false;
							LOGGER.error(polDetailException.toString());
						}
					} 
				
			}

			if (policyDetailExceptions.size() > 0){
				errorLogger.logError(policyDetailExceptions);
			}


			ClientDataSheetData cds = new ClientDataSheetData();
			cds = ard.getClientDataSheet();
			cds.setPolicyCoverageDetailsList(relatedPolicies);

			LOBData lob = new LOBData();
			lob.setLOBCode(IUMConstants.LOB_INDIVIDUAL_LIFE);
			ard.setLob(lob);

			/***TRANSACTION_REQUIREMENT_INQUIRY - RequirementMaintenanceTransaction***/
			currentTrans = TRANSACTION_REQUIREMENT_INQUIRY;
			requirements = ingPop.populateRequirements(requirementList, refNum);
			
			if (requirements != null && requirements.size() > 0){
					for (int i = 0; i < requirements.size(); i++){
						reqData = (PolicyRequirementsData) requirements.get(i);

						ExceptionLog reqException = validatePolicyRequirements(reqData, conn);
						if (reqException != null){
							reqException.setRecordType(TRANSACTION_REQUIREMENT_INQUIRY);
							reqException.setReferenceNumber(refNum);
							reqException.setReply(tb.getResponse());
							reqException.setSystemInterface(IUMConstants.SYSTEM_INGENIUM_DESC);
							errorLogger.logError(reqException);
							noExceptions = false;
							LOGGER.error(reqException.toString());
						}
					}
			}

			if (noExceptions){
			
				try {
			
					
					ar.transmitIngeniumDataToIUMDB(ard,conn);
					if ((requirements != null) && (requirements.size() > 0)) {
						PolicyRequirements pr = new PolicyRequirements();
						PolicyRequirementDAO requirementsDAO = new PolicyRequirementDAO();
						for (int i=0; i < requirements.size(); i++) {
							PolicyRequirementsData prd = (PolicyRequirementsData)requirements.get(i);
							if(!requirementsDAO.isRequirementExisting(prd)){
								pr.createRequirementIUM(prd,ard,conn);//???
							}else{
								String updatedBy=isUpdatedByValid(prd.getUpdatedBy());
								prd.setUpdatedBy(updatedBy);
								requirementsDAO.updateRequirementRetainComment(prd);
							}
						}
						
						
						List IUMReqts = requirementsDAO.retrieveRequirements(refNum);
						boolean isMatched = false;
												
						for(int i=0; i < IUMReqts.size();i++){
							PolicyRequirementsData ium = (PolicyRequirementsData)IUMReqts.get(i);
							
							for(int j=0;j< requirements.size();j++){
								PolicyRequirementsData ingenium = (PolicyRequirementsData)requirements.get(j);
								
								if(ium.getLevel().equalsIgnoreCase(IUMConstants.LEVEL_CLIENT)){
									if(ium.getReferenceNumber().equalsIgnoreCase(ingenium.getReferenceNumber())
											&& ium.getRequirementCode().equalsIgnoreCase(ingenium.getRequirementCode())
											&& ium.getLevel().equalsIgnoreCase(ingenium.getLevel())
											&& ium.getClientId().equalsIgnoreCase(ingenium.getClientId())){
										
										isMatched = true;
										break;
										
									}else{
										isMatched = false;
									}
									
								}else{	
									if(ium.getReferenceNumber().equalsIgnoreCase(ingenium.getReferenceNumber())
											&& ium.getRequirementCode().equalsIgnoreCase(ingenium.getRequirementCode())
											&& ium.getLevel().equalsIgnoreCase(ingenium.getLevel())){
										
										isMatched = true;
										break;
										
									}else{
										isMatched = false;
									}
								}
							}
							
							
							if (!isMatched){
								requirementsDAO.deletePolicyRequirement(ium.getRequirementId());
							}
								
						}
					}
					
					ar.setSuccessfulRetrieval(ard.getReferenceNumber(),IUMConstants.INDICATOR_SUCCESS,conn);
					conn.commit();					
				} catch (IUMInterfaceException e){
					rollbackTransaction(conn);
					logError(e,currentTrans);
					LOGGER.error(CodeHelper.getStackTrace(e));
				} finally {
					closeConnection(conn);
				}

				tb = TransactionBuilder.buildDeletePolicyTransaction(refNum,
																	 suffix);
				tb.execute();
				
			}

		} catch (Exception e) {
			logError(e, currentTrans);
			LOGGER.error(CodeHelper.getStackTrace(e));
		} 
		LOGGER.info("processPolicyDetails end 3");
	}
	
	private String isUpdatedByValid(String updatedBy) throws Exception {
		
		LOGGER.info("isUpdatedByValid start");
		AssessmentRequestDAO arDAO = new AssessmentRequestDAO();
		if(arDAO.isUser(updatedBy)){
			LOGGER.info("isUpdatedByValid end");
			return updatedBy;
		} else {
			LOGGER.info("isUpdatedByValid end");
			return null;
		}
		
	}
	
	/**
	 * This method is responsible for connecting to Ingenium and
	 * retrieval of all the policies for review and assessment
	 * @throws IUMInterfaceException
	 */
	public void processGetPolicyQueue() throws IUMInterfaceException {
		
		LOGGER.info("processGetPolicyQueue start 1");
		List 					  policyList = null;
		AssessmentRequest 		  ar = null;
		AssessmentRequestData 	  ard = null;
		ExceptionLog 			  exLog = null;
		IngeniumPopulator		  ingPop = null;

		init();
		if(this.useThread.trim().equalsIgnoreCase("true") && !this.isThread)  {
			Timer timer = new Timer();
			timer.schedule(new IngeniumThread(TRANSACTION_POLICY_QUEUE, 0, null), 1000);
			return;
		}

		Transaction tb = TransactionBuilder.buildPolicyListTransaction();
		policyList = ((PolicyListResponse) tb.execute()).getPolicyList();
		if ((policyList != null) && (policyList.size() > 0)) {
			try {
				ingPop = new IngeniumPopulator();
				ar = new AssessmentRequest();
				for (int i=0; i < policyList.size(); i++) {
					exLog = null;
					ard = new AssessmentRequestData();
					ingPop.populateAssessmentRequestData(ard,(PolicyInformation) policyList.get(i));
					setIngeniumSpecificDetails(ard);
					boolean isPolicyExisting = ar.isAssessmentRequestExist(ard.getReferenceNumber());
					if (!isPolicyExisting){
						exLog = validateARD(ard);						
					}
					if(exLog != null){
					
						exLog.setRecordType(TRANSACTION_POLICY_QUEUE);
						exLog.setReply(tb.getResponse());
						exLog.setSystemInterface(IUMConstants.SYSTEM_INGENIUM_DESC);
						exLog.setReferenceNumber(ard.getReferenceNumber());
						ExceptionLogger errorLogger = new ExceptionLogger();
						errorLogger.logError(exLog);
						LOGGER.error(exLog.toString());
					}else{
						try {
							String referenceNumber = ard.getReferenceNumber();
							
							if (isPolicyExisting){
								boolean isFinishRetrieval = ar.isFinishRetrieval(referenceNumber);
								if (isFinishRetrieval){
									refresh(ard);
								} else {
									processPolicyDetails(referenceNumber);									
								}
							} else { 
								ar.createAssessmentRequest(ard);
								processPolicyDetails(referenceNumber);
							}
						} catch (UnderWriterException uwe) {
							exLog = new ExceptionLog();
							exLog.setRecordType(TRANSACTION_POLICY_QUEUE);
							exLog.setReply(tb.getResponse());
							exLog.setSystemInterface(IUMConstants.SYSTEM_INGENIUM_DESC);
							exLog.setReferenceNumber(ard.getReferenceNumber());
							ArrayList arr = new ArrayList();
							arr.add(createExceptionDetail(uwe.getMessage()));
							exLog.setDetails(arr);
							ExceptionLogger errorLogger = new ExceptionLogger();
							errorLogger.logError(exLog);
							LOGGER.error(exLog.toString());
							LOGGER.error(CodeHelper.getStackTrace(uwe));
						}
					}
				}
			} catch (Exception e) {
				logError(e, TRANSACTION_POLICY_QUEUE);
				LOGGER.error(CodeHelper.getStackTrace(e));
			}
		}
		LOGGER.info("processGetPolicyQueue end 1");	
	}
	
	/**
	 * This method is responsible for connecting to Ingenium and
	 * retrieval of one policy for review and assessment
	 * @throws IUMInterfaceException
	 */
	public void processGetPolicyQueue(final String refNum, final String suffix) throws IUMInterfaceException {
		
		LOGGER.info("processGetPolicyQueue start 2");	
		List 					  policyList = null;
		AssessmentRequest 		  ar = null;
		AssessmentRequestData 	  ard = null;
		ExceptionLog 			  exLog = null;
		IngeniumPopulator		  ingPop = null;
		
		
		Transaction tb = TransactionBuilder.buildPolicyListTransaction(refNum,suffix);
		policyList = ((PolicyListResponse) tb.execute()).getPolicyList();
		
		if ((policyList != null) && (policyList.size() > 0)) {
		
			try {
		
				ingPop = new IngeniumPopulator();
				ar = new AssessmentRequest();
				
				for (int i=0; i < policyList.size(); i++) {
					exLog = null;
					ard = new AssessmentRequestData();
					ingPop.populateAssessmentRequestData(ard, (PolicyInformation) policyList.get(i));
					setIngeniumSpecificDetails(ard);
					
					boolean isPolicyExisting = ar.isAssessmentRequestExist(ard.getReferenceNumber());
					
					if (!isPolicyExisting){
						exLog = validateARD(ard);						
					}
					
					if(exLog != null) {
					
						exLog.setRecordType(TRANSACTION_POLICY_QUEUE);
						exLog.setReply(tb.getResponse());
						exLog.setSystemInterface(IUMConstants.SYSTEM_INGENIUM_DESC);
						exLog.setReferenceNumber(ard.getReferenceNumber());
						ExceptionLogger errorLogger = new ExceptionLogger();
						errorLogger.logError(exLog);
						StringBuffer sb = getExceptionLogs(exLog);
						LOGGER.error(exLog.toString());
						throw new IUMInterfaceException(sb.toString());
					} else {
						try {
							String referenceNumber = ard.getReferenceNumber();
							
							if (isPolicyExisting){
								boolean isFinishRetrieval = ar.isFinishRetrieval(referenceNumber);
								if (isFinishRetrieval){
								
									
									AssessmentRequestDAO arDAO = new AssessmentRequestDAO();
									try{
										arDAO.updateAssessmentRequest(ard);
								
									} catch (SQLException e){
								
										LOGGER.error(CodeHelper.getStackTrace(e));
										throw new UnderWriterException (e);
									}
									
									processPolicyDetails(referenceNumber,suffix);
								} else {
									processPolicyDetails(referenceNumber,suffix);									
								}
							} else {
								ar.createAssessmentRequest(ard);
								processPolicyDetails(referenceNumber,suffix);
							}
						} catch (UnderWriterException uwe) {
							
							exLog = new ExceptionLog();
							exLog.setRecordType(TRANSACTION_POLICY_QUEUE);
							exLog.setReply(tb.getResponse());
							exLog.setSystemInterface(IUMConstants.SYSTEM_INGENIUM_DESC);
							exLog.setReferenceNumber(ard.getReferenceNumber());
							
							ArrayList arr = new ArrayList();
							arr.add(createExceptionDetail(uwe.getMessage()));
							exLog.setDetails(arr);
							
							ExceptionLogger errorLogger = new ExceptionLogger();
							errorLogger.logError(exLog);
							LOGGER.error(exLog.toString());
							throw new IUMInterfaceException(uwe.getMessage());
						}
					}
				}
			} catch (Exception e) {
				logError(e, TRANSACTION_POLICY_QUEUE);
				LOGGER.error(CodeHelper.getStackTrace(e));
				throw new IUMInterfaceException(e.getMessage());
			}
		}else{
			LOGGER.error("Policy does not exists.");
			throw new IUMInterfaceException("Policy does not exists.");
		}
		LOGGER.info("processGetPolicyQueue end 2");
	}

	private StringBuffer getExceptionLogs(final ExceptionLog exLog) {
		
		LOGGER.info("getExceptionLogs start");
		Iterator it = exLog.getDetails().iterator();
		StringBuffer sb = new StringBuffer();
		
		while(it.hasNext()){
			sb.append(((ExceptionDetail)it.next()).getMessage() + ". ");
		}
		LOGGER.info("getExceptionLogs end");
		return sb;
	}
	public List kickoutTransaction(
			final String refNum,
			final String suffix){

		LOGGER.info("kickoutTransaction start");
		final Date start = new Date();
		Date end;
		long elapse;
		long startTmp = System.currentTimeMillis();
		long endTmp;
		
		end = new Date(); endTmp = System.currentTimeMillis();
		elapse = ((end.getTime() - start.getTime()) % (60 * 1000)) / 1000;
		
		boolean	noExceptions = true;
		boolean	result = false; 
		boolean existingRequest = false;
		
		String currentTrans = "";
		String errMsg = "";
		
		ArrayList requirements = null; 
		ArrayList exceptionLogs = new ArrayList();
		ArrayList relatedPolicies = new ArrayList(); 
		
		List koList = null;
		List requirementList = null; 
		List kickOutList = new ArrayList();
		
	
		AssessmentRequest 		ar = new AssessmentRequest();
		AssessmentRequestData 	ard = null;
		ClientData 				ownerData = null;
		ClientData 				insuredData = null;
		ExceptionLogger    		errorLogger = new ExceptionLogger();
		IngeniumPopulator  		ingPop = new IngeniumPopulator();
		KickoutResponse			koResponse = null; 
		KickOutMessageData 		koData = null; 
		PolicyRequirementsData 	reqData = null;
		Transaction 			tb = null; 
		
		end = new Date(); endTmp = System.currentTimeMillis();
		elapse = ((end.getTime() - start.getTime()) % (60 * 1000)) / 1000;
		
		/***TRANSACTION_KICK_OUT - KickoutTransaction***/
			try {
				tb = TransactionBuilder.buildKickoutTransaction(refNum, suffix);
				end = new Date(); endTmp = System.currentTimeMillis();
				elapse = ((end.getTime() - start.getTime()) % (60 * 1000)) / 1000;
				
				currentTrans = TRANSACTION_KICK_OUT;
				koResponse = (KickoutResponse) tb.execute();
				end = new Date(); endTmp = System.currentTimeMillis();
				elapse = ((end.getTime() - start.getTime()) % (60 * 1000)) / 1000;
				
				koList = koResponse.getKickOutDetails();
			
			
				if (koList != null && koList.size() > 0) {
					for (int i=0; i < koList.size(); i++) {
						koData = ingPop.populateKOMessageData((Kickout) koList.get(i), refNum);
						end = new Date(); endTmp = System.currentTimeMillis();
						elapse = ((end.getTime() - start.getTime()) % (60 * 1000)) / 1000;
						
						ExceptionLog exLog = validateKOMessages(existingRequest, errMsg, koData);
						if (exLog != null) {
							exLog.setRecordType(TRANSACTION_KICK_OUT);
							exLog.setReferenceNumber(refNum);
							exLog.setReply(tb.getResponse());
							exLog.setSystemInterface(IUMConstants.SYSTEM_INGENIUM_DESC);
							exceptionLogs.add(exLog);
							noExceptions = false;
							StringBuffer sb = getExceptionLogs(exLog);
							LOGGER.error(exLog.toString());
						} else {
							kickOutList.add(koData);
						}
					}
				}
			} catch (Exception e) {
				LOGGER.debug("[Line:1710] Pol: " + refNum + suffix + " - ERROR in generating ko list: ");
				LOGGER.error(CodeHelper.getStackTrace(e));
			}
			
			LOGGER.info("kickoutTransaction end");
			return koList;
	}
	
	// Call web service in ingenium
	// Also checks if existing in WMSIUM DB
	public boolean retrieveCDSDetails(
			final String refNum,
			final String suffix) throws IUMInterfaceException{
		
		LOGGER.info("retrieveCDSDetails start");
		final Date start = new Date();
		Date end;
		long elapse;
		long startTmp = System.currentTimeMillis();
		long endTmp;
		
		end = new Date(); endTmp = System.currentTimeMillis();
		elapse = ((end.getTime() - start.getTime()) % (60 * 1000)) / 1000;
		
		boolean	noExceptions = true;
		boolean	result = false;
		boolean existingRequest = false;
		
		String currentTrans = "";
		String errMsg = "";
		
		ArrayList requirements = null;
		ArrayList exceptionLogs = new ArrayList();
		ArrayList relatedPolicies = new ArrayList();
		
		List koList = null;
		List requirementList = null;
		List kickOutList = new ArrayList();
		
		AssessmentRequest 		ar = new AssessmentRequest();
		AssessmentRequestData 	ard = null;
		ClientData 				ownerData = null;
		ClientData 				insuredData = null;
		ExceptionLogger    		errorLogger = new ExceptionLogger();
		IngeniumPopulator  		ingPop = new IngeniumPopulator();
		KickoutResponse			koResponse = null;
		KickOutMessageData 		koData = null;
		PolicyRequirementsData 	reqData = null;
		Transaction 			tb = null;
		
		end = new Date(); endTmp = System.currentTimeMillis();
		elapse = ((end.getTime() - start.getTime()) % (60 * 1000)) / 1000;
		
		try {
			
			Connection conn = new DataSourceProxy().getConnection();
			try {
				existingRequest = ar.isAssessmentRequestExist(refNum);
				LOGGER.info("existingRequest-->" + existingRequest);
			} catch (IUMException e) {
				LOGGER.error(CodeHelper.getStackTrace(e));
				errMsg = e.getMessage();
			} finally {
				closeConnection(conn);
			}
			
			if (!existingRequest) {
			
				ard = populateAssessment(refNum, suffix);
				if(!"".equals(ard.getReferenceNumber())
						&& ard.getReferenceNumber() != null) {
					existingRequest = true;
				}

				processPolicyDetails(refNum, suffix);
			}
			
		
			tb = TransactionBuilder.buildKickoutTransaction(refNum, suffix);
			koList = kickoutTransaction(refNum, suffix);
			end = new Date(); endTmp = System.currentTimeMillis();
			elapse = ((end.getTime() - start.getTime()) % (60 * 1000)) / 1000;
			koResponse = (KickoutResponse) tb.execute();
			
			try {
				ard.setKickOutMessages((ArrayList) kickOutList);
				ar.processKOMessages(ard);
			} catch (Exception e) {
				LOGGER.warn("[Line:1721] Pol: " + refNum + suffix + " - ERROR in saving ko: ");
				LOGGER.error(CodeHelper.getStackTrace(e));
			}
			
			requirementList = koResponse.getRequirementList();
			end = new Date(); endTmp = System.currentTimeMillis();
			elapse = ((end.getTime() - start.getTime()) % (60 * 1000)) / 1000;
			
			
				/***TRANSACTION_CLIENT_DATA_SHEET - ClientDataTransaction***/
				tb = TransactionBuilder.buildClientDataTransaction(refNum, suffix); 
				ard = new AssessmentRequestData();
				ingPop.populateClientRelatedInfo(ard,
						refNum,
						relatedPolicies,
						(ClientDataResponse) tb.execute());

			end = new Date(); endTmp = System.currentTimeMillis();
			elapse = ((end.getTime() - start.getTime()) % (60 * 1000)) / 1000;
			
			if(null == ard.getReferenceNumber()) {
				return false;
			}

			
			try{
				
				if(ard.getOwner().getSex() != null && "C".equalsIgnoreCase(ard.getOwner().getSex())){
					Transaction tbClient = TransactionBuilder.buildClientInquiryTransaction(ard.getOwner().getClientId());
					ClientInquiryResponse res = (ClientInquiryResponse) tbClient.execute();
					ClientData clData = ard.getOwner(); 
					clData.setGivenName(res.getClientName());
					ard.setOwner(clData);
				}
				
			}catch(Exception e){
				LOGGER.error(CodeHelper.getStackTrace(e));
			}
			end = new Date(); endTmp = System.currentTimeMillis();
			elapse = ((end.getTime() - start.getTime()) % (60 * 1000)) / 1000;
						
			ownerData = ard.getOwner();
			insuredData = ard.getInsured();
			
			if (insuredData == null || (insuredData != null && StringHelper.isEmpty(insuredData.getClientId()))){
				insuredData = copyClientData(ownerData);
				ard.setInsured(insuredData);
			}			
			
			String iIdTmp = removeNull(insuredData.getClientId());
			String oIdTmp = removeNull(ownerData.getClientId());
			
			if (!oIdTmp.equals("")
					&& !iIdTmp.equals("")
					&& (oIdTmp.equals(iIdTmp))) {
				
				ExceptionLog ownerDataException = validateClientInfo(ownerData);
				
				if (ownerDataException != null) {
					ownerDataException.setRecordType(TRANSACTION_CLIENT_DATA_SHEET);
					ownerDataException.setReferenceNumber(refNum);
					ownerDataException.setReply(tb.getResponse());
					ownerDataException.setSystemInterface(IUMConstants.SYSTEM_INGENIUM_DESC);
					errorLogger.logError(ownerDataException);
					noExceptions = false;
					StringBuffer sb = getExceptionLogs(ownerDataException);

				}
			} else {
				
			
				if (ownerData.getSex() != null && !"C".equalsIgnoreCase(ownerData.getSex())) {
			
					ExceptionLog ownerDataException = validateClientInfo(ownerData);
					if (ownerDataException != null) {
						ownerDataException.setRecordType(TRANSACTION_CLIENT_DATA_SHEET);
						ownerDataException.setReferenceNumber(refNum);
						ownerDataException.setReply(tb.getResponse());
						ownerDataException.setSystemInterface(IUMConstants.SYSTEM_INGENIUM_DESC);
						errorLogger.logError(ownerDataException);
						noExceptions = false;

						StringBuffer sb = getExceptionLogs(ownerDataException);
					}
				}
	
				ExceptionLog insuredDataException = validateClientInfo(insuredData);
				if (insuredDataException != null) {
					insuredDataException.setRecordType(TRANSACTION_CLIENT_DATA_SHEET);
					insuredDataException.setReferenceNumber(refNum);
					insuredDataException.setReply(tb.getResponse());
					insuredDataException.setSystemInterface(IUMConstants.SYSTEM_INGENIUM_DESC);
					errorLogger.logError(insuredDataException);
					noExceptions = false;
					StringBuffer sb = getExceptionLogs(insuredDataException);
				} 
			}
			end = new Date(); endTmp = System.currentTimeMillis();
			elapse = ((end.getTime() - start.getTime()) % (60 * 1000)) / 1000;
			
			if(ownerData.getClientId() != null
					&& insuredData.getClientId() != null
					&& ownerData.getClientId().equals(insuredData.getClientId())) {
						
				CDSMortalityRatingData ownerMortality = ard.getOwnerMortalityRating();
				ExceptionLog oMortalityException = validateMortalityRating(ownerMortality);
							
				if (oMortalityException != null) {
					oMortalityException.setRecordType(TRANSACTION_CLIENT_DATA_SHEET);
					oMortalityException.setReferenceNumber(refNum);
					oMortalityException.setReply(tb.getResponse());
					oMortalityException.setSystemInterface(IUMConstants.SYSTEM_INGENIUM_DESC);
					errorLogger.logError(oMortalityException);
					noExceptions = false;
					StringBuffer sb = getExceptionLogs(oMortalityException);
				}
			} else {
				
				if(ownerData.getSex()!=null && !"C".equalsIgnoreCase(ownerData.getSex())){
							
					CDSMortalityRatingData ownerMortality = ard.getOwnerMortalityRating();
					ExceptionLog oMortalityException = validateMortalityRating(ownerMortality);
								
					if (oMortalityException != null) {
						oMortalityException.setRecordType(TRANSACTION_CLIENT_DATA_SHEET);
						oMortalityException.setReferenceNumber(refNum);
						oMortalityException.setReply(tb.getResponse());
						oMortalityException.setSystemInterface(IUMConstants.SYSTEM_INGENIUM_DESC);
						errorLogger.logError(oMortalityException);
						noExceptions = false;
						StringBuffer sb = getExceptionLogs(oMortalityException);
					}
				}
								
				CDSMortalityRatingData insuredMortality = ard.getInsuredMortalityRating();
				ExceptionLog iMortalityException = validateMortalityRating(insuredMortality);
							
				if (iMortalityException != null) {
					iMortalityException.setRecordType(TRANSACTION_CLIENT_DATA_SHEET);
					iMortalityException.setReferenceNumber(refNum);
					iMortalityException.setReply(tb.getResponse());
					iMortalityException.setSystemInterface(IUMConstants.SYSTEM_INGENIUM_DESC);
					errorLogger.logError(iMortalityException);
					noExceptions = false;
					StringBuffer sb = getExceptionLogs(iMortalityException);
					
				} 
			}
			end = new Date(); endTmp = System.currentTimeMillis();
			elapse = ((end.getTime() - start.getTime()) % (60 * 1000)) / 1000;
			
			ClientDataSheetData cds = ard.getClientDataSheet();
			
			CDSSummaryPolicyCoverageData policyCoverage = cds.getSummaryOfPolicies();
			end = new Date(); endTmp = System.currentTimeMillis();
			elapse = ((end.getTime() - start.getTime()) % (60 * 1000)) / 1000;
			
	  		ExceptionLog policyCoverageException = validateSummaryPolicyCoverage(cds.getSummaryOfPolicies());
	  		end = new Date(); endTmp = System.currentTimeMillis();
			elapse = ((end.getTime() - start.getTime()) % (60 * 1000)) / 1000;
			
			if (policyCoverageException != null) {
				policyCoverageException.setRecordType(TRANSACTION_CLIENT_DATA_SHEET);
				policyCoverageException.setReferenceNumber(refNum);
				policyCoverageException.setReply(tb.getResponse());
				policyCoverageException.setSystemInterface(IUMConstants.SYSTEM_INGENIUM_DESC);
				errorLogger.logError(policyCoverageException);
				noExceptions = false;
				StringBuffer sb = getExceptionLogs(policyCoverageException);
				
			} 
			end = new Date(); endTmp = System.currentTimeMillis();
			elapse = ((end.getTime() - start.getTime()) % (60 * 1000)) / 1000;
			
			/***TRANSACTION_RELATED_POLICY_AND_COVERAGE***/
			currentTrans = TRANSACTION_RELATED_POLICY_AND_COVERAGE;
			ArrayList policyDetailExceptions = new ArrayList();
	
			if (relatedPolicies != null && relatedPolicies.size() > 0) {
				for (int i=0; i < relatedPolicies.size(); i++){
					
					PolicyDetailData policyDetail = (PolicyDetailData) relatedPolicies.get(i);
					ExceptionLog polDetailException = validatePolicyCoverageDetails(policyDetail);
	
					if (polDetailException != null) {
						polDetailException.setRecordType(TRANSACTION_RELATED_POLICY_AND_COVERAGE);
						polDetailException.setReferenceNumber(refNum);
						polDetailException.setReply(tb.getResponse());
						polDetailException.setSystemInterface(IUMConstants.SYSTEM_INGENIUM_DESC);
						policyDetailExceptions.add(polDetailException);
						noExceptions = false;
						StringBuffer sb = getExceptionLogs(polDetailException);
						
					}
				}
			}
			end = new Date(); endTmp = System.currentTimeMillis();
			elapse = ((end.getTime() - start.getTime()) % (60 * 1000)) / 1000;
			
			if (policyDetailExceptions.size() > 0){
				errorLogger.logError(policyDetailExceptions);
			}
			
			
			cds.setPolicyCoverageDetailsList(relatedPolicies);
			end = new Date(); endTmp = System.currentTimeMillis();
			elapse = ((end.getTime() - start.getTime()) % (60 * 1000)) / 1000;
			
			LOBData lob = new LOBData();
			lob.setLOBCode(IUMConstants.LOB_INDIVIDUAL_LIFE);
			ard.setLob(lob);
	
			/***TRANSACTION_REQUIREMENT_INQUIRY - RequirementMaintenanceTransaction***/
			currentTrans = TRANSACTION_REQUIREMENT_INQUIRY;
			requirements = ingPop.populateRequirements(requirementList, refNum);
			end = new Date(); endTmp = System.currentTimeMillis();
			elapse = ((end.getTime() - start.getTime()) % (60 * 1000)) / 1000;
			
			if (requirements != null && requirements.size() > 0){
			
				conn = new DataSourceProxy().getConnection();
				
				try {
				
					for (int i = 0; i < requirements.size(); i++) {
						reqData = (PolicyRequirementsData) requirements.get(i);
						ExceptionLog reqException = validatePolicyRequirements(conn, existingRequest, errMsg, reqData);
						
						if (reqException != null) {
							reqException.setRecordType(TRANSACTION_REQUIREMENT_INQUIRY);
							reqException.setReferenceNumber(refNum);
							reqException.setReply(tb.getResponse());
							reqException.setSystemInterface(IUMConstants.SYSTEM_INGENIUM_DESC);
							errorLogger.logError(reqException);
							noExceptions = false;
							StringBuffer sb = getExceptionLogs(reqException);
							
						}
					}
					end = new Date(); endTmp = System.currentTimeMillis();
					elapse = ((end.getTime() - start.getTime()) % (60 * 1000)) / 1000;
					
				} finally {
					closeConnection(conn);
				}
			}
			end = new Date(); endTmp = System.currentTimeMillis();
			elapse = ((end.getTime() - start.getTime()) % (60 * 1000)) / 1000;
			
			if (noExceptions){
				
				
				try {
					
				
					String hasIUMRecord = getRequest() != null
							&& getRequest().getSession().getAttribute("hasIUMRecord") != null
							&& !"".equals(getRequest().getSession().getAttribute("hasIUMRecord")) ? (String)getRequest().getSession().getAttribute("hasIUMRecord")
							: "";
					ar.transmitIngeniumCDSDataToIUMDBWMS(ard, hasIUMRecord);
					
					ar.setSuccessfulRetrieval(ard.getReferenceNumber(),IUMConstants.INDICATOR_SUCCESS,conn);
					
					result= true;
				} catch (IUMInterfaceException e){
					logError(e,currentTrans);
					LOGGER.error(CodeHelper.getStackTrace(e));
				} 
						
				tb = TransactionBuilder.buildDeletePolicyTransaction(refNum, suffix);
				tb.execute();
			
			}
			end = new Date(); endTmp = System.currentTimeMillis();
			elapse = ((end.getTime() - start.getTime()) % (60 * 1000)) / 1000;
			
			
		} catch (Exception e) {
			logError(e, currentTrans);
			LOGGER.error(CodeHelper.getStackTrace(e));
		}
		
		LOGGER.info("retrieveCDSDetails end");
		return result;
	}
	
	public HashMap doClearCase(String strPolicyId) {
		
		LOGGER.info("doClearCase start");
    	HashMap hashmap;  
    	IngeniumDispatcher igDispatcher = new IngeniumDispatcher();
    	hashmap = igDispatcher.doClearCaseIum(strPolicyId, user, pwd);
    	LOGGER.debug("HASHMAP-->" + hashmap);
    	
    	LOGGER.info("doClearCase end");
    	return hashmap;
	}
	
	public ArrayList retrievePolicyCoverageDetailList(final String refNum, final String suffix) {
		
		LOGGER.info("retrievePolicyCoverageDetailList start");
		
		final String INSURED_CLIENT_NO = "INSURED_CLIENT_NO";
		final String PLAN_ID = "PLAN_ID";
		final String COVERAGE_PLAN_DESC = "COVERAGE_PLAN_DESC";
		final String COVERAGE_STATUS = "POLICY_ALLOCATION_INQUIRY_COVERAGE_STATUS_OF_ALLOCATION_TYPE_SUBSEQUENT_PREMIUM";
		final String ORIGINAL_FACE_AMOUNT = "ORIGINAL_FACE_AMOUNT";
		final String COVERAGE_NUMBER = "COVERAGE_NUMBER";
		
		IngeniumDispatcher igDispatcher = new IngeniumDispatcher();
		String coverageNum = "01";
		HashMap hashmap = new HashMap();
		hashmap = (HashMap)igDispatcher.getPolicyCoverageDetails(refNum.concat(suffix), coverageNum, user, pwd);
		
		ArrayList clientNoList = (ArrayList) hashmap.get(INSURED_CLIENT_NO);
		ArrayList planCodeList = (ArrayList) hashmap.get(PLAN_ID);
		ArrayList planDescList = (ArrayList) hashmap.get(COVERAGE_PLAN_DESC);
		ArrayList coverageStatusList = (ArrayList) hashmap.get(COVERAGE_STATUS);
		ArrayList faceAmountList = (ArrayList) hashmap.get(ORIGINAL_FACE_AMOUNT);
		ArrayList coverageNumberList = (ArrayList) hashmap.get(COVERAGE_NUMBER);
		
		ArrayList policyCoverageDetailList = new ArrayList();
		for (int i=0; i < planCodeList.size(); i++) {
			PolicyDetailData data = new PolicyDetailData();
			data.setReferenceNumber(refNum.concat(suffix));
			data.setClientId((String) clientNoList.get(i));
			data.setPlanCode((String) planCodeList.get(i));
			data.setPlanDescription((String) planDescList.get(i));
			data.setCoverageStatus((String) coverageStatusList.get(i));
			data.setFaceAmount(Double.parseDouble((String) faceAmountList.get(i)));
			data.setCoverageNumber(Long.parseLong((String) coverageNumberList.get(i)));
			policyCoverageDetailList.add(data);
		}
		LOGGER.info("retrievePolicyCoverageDetailList end");
		
		return policyCoverageDetailList;
	}
	
	//Update Agent Details from Ingenium to IUM - Andre Ceasar Dacanay - start
	public HashMap retrieveIngeniumConsolidatedInformation(final String refNum, final String suffix) {
		
		LOGGER.info("retrieveIngeniumConsolidatedInformation start");
		
		IngeniumDispatcher igDispatcher = new IngeniumDispatcher();
		HashMap hashmap = new HashMap();
		hashmap = (HashMap)igDispatcher.getIngeniumConsolidatedInformation(refNum.concat(suffix), user, pwd);
		
		LOGGER.info("retrieveIngeniumConsolidatedInformation end");
		return hashmap;
	}
	//Update Agent Details from Ingenium to IUM - Andre Ceasar Dacanay - end
	
	/**
	 * Refresh method
	 * @param ard
	 * @throws IUMInterfaceException 
	 */
	private void refresh(final AssessmentRequestData ard) throws IUMInterfaceException{
		
		LOGGER.info("refresh start");
		
		try {
			String referenceNumber = ard.getReferenceNumber();
			
			KickOutMessageDAO KOMessageDAO = new KickOutMessageDAO();
			KOMessageDAO.deleteKOMessages(referenceNumber);

			ClientDataSheetDAO cdsDAO = new ClientDataSheetDAO();
			cdsDAO.deleteCDS(referenceNumber);	
			cdsDAO.deletePolicyCoverageDetails(referenceNumber);
			
			AssessmentRequestDAO arDAO = new AssessmentRequestDAO();
			arDAO.updateAssessmentRequest(ard);
			
			
			processPolicyDetails(referenceNumber);
		}catch (Exception e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
		}
		
		LOGGER.info("refresh end");
		
	}
	
	/**
	 * This method is responsible for the creation of requirements in Ingenium
	 * @param prd - policy requirements data
	 * @throws IUMInterfaceException
	 */
	public void processCreateRequirement(final PolicyRequirementsData prd) throws IUMInterfaceException {

		LOGGER.info("processCreateRequirement start");
		IngeniumPopulator 				ingPop = new IngeniumPopulator();
		Transaction 					tb = null;
		RequirementMaintenanceResponse 	reqtMaintResponse = null;
		
		init();
		if(this.useThread.trim().equalsIgnoreCase("true") && !this.isThread)  {
			Timer timer = new Timer();
			timer.schedule(new IngeniumThread(TRANSACTION_REQUIREMENT_MAINTENANCE,
						   REQTTRANS_CREATE, prd), 1000);
			return;
		}

		try {
			tb = TransactionBuilder.buildRequirementMaintenanceTransaction(
				 ingPop.populateRequirementMaintenanceParm(prd, true));

			reqtMaintResponse = ((RequirementMaintenanceResponse) tb.execute());
			IUMMessageData imd = ingPop.populateIUMMessageData(reqtMaintResponse.getRequirementData(), prd);

			ExceptionLog exLog = validateIUMMessageData(imd);
			
			if(exLog == null){
			   	prd.setABACUScreateDate(imd.getCreationDate());
			   	prd.setSequenceNumber(imd.getSequenceNumber());
			   	PolicyRequirements pr = new PolicyRequirements();
			   	pr.maintainRequirement(prd);
			}else{
				exLog.setRecordType(TRANSACTION_REQUIREMENT_MAINTENANCE);
			   	exLog.setReply(tb.getResponse());
			   	exLog.setSystemInterface(IUMConstants.SYSTEM_INGENIUM_DESC);
				ExceptionLogger errorLogger = new ExceptionLogger();
				errorLogger.logError(exLog);
			}
			
			validateMaintenanceResults(reqtMaintResponse.getMessages());
		} catch (Exception e) {
			logError(e, TRANSACTION_REQUIREMENT_MAINTENANCE);
			LOGGER.error(CodeHelper.getStackTrace(e));
		}
	}

	/**
	 * This method is responsible for updating the requirement in Ingenium
	 * @param list
	 * @throws IUMInterfaceException
	 */
	public void processReceiveRequirement(final ArrayList list) throws IUMInterfaceException {
		
		LOGGER.info("processReceiveRequirement start");
		IngeniumPopulator 				ingPop = new IngeniumPopulator();
		Transaction 					tb = null;
		PolicyRequirementsData			prd = null;
		RequirementMaintenanceResponse 	reqtMaintResponse = null;

		init();
		if(this.useThread.trim().equalsIgnoreCase("true") && !this.isThread)  {
			Timer timer = new Timer();
			timer.schedule(new IngeniumThread(TRANSACTION_REQUIREMENT_MAINTENANCE,
						   REQTTRANS_RECEIVE, list), 1000);
			return;
		}

		try {
			for (int i = 0; i < list.size(); i++) {
				prd = (PolicyRequirementsData) list.get(i);
				tb = TransactionBuilder.buildRequirementMaintenanceTransaction(
					 ingPop.populateRequirementMaintenanceParm(prd, false));

				reqtMaintResponse = (RequirementMaintenanceResponse) tb.execute();
				IUMMessageData imd = ingPop.populateIUMMessageData(reqtMaintResponse.getRequirementData(), prd);

				ExceptionLog exLog = validateIUMMessageData(imd);
				
				if(exLog == null){
					prd.setABACUScreateDate(imd.getCreationDate());
					prd.setSequenceNumber(imd.getSequenceNumber());
					PolicyRequirements pr = new PolicyRequirements();
					pr.maintainRequirement(prd);
				}else{
					exLog.setRecordType(TRANSACTION_REQUIREMENT_MAINTENANCE);
					exLog.setReply(tb.getResponse());
					exLog.setSystemInterface(IUMConstants.SYSTEM_INGENIUM_DESC);
					ExceptionLogger errorLogger = new ExceptionLogger();
					errorLogger.logError(exLog);
				}
			}
			
			validateMaintenanceResults(reqtMaintResponse.getMessages());
		} catch (Exception e) {
			logError(e, TRANSACTION_REQUIREMENT_MAINTENANCE);
			LOGGER.error(CodeHelper.getStackTrace(e));
		}
		LOGGER.info("processReceiveRequirement end");
	}

	/**
	 * updates Ingenium of changes in the requirements in IUM
	 * @param list ArrayList of policy requirements
	 * @throws IUMInterfaceException
	 */
	public void processAcceptRequirement(final ArrayList list) throws IUMInterfaceException {
		
		LOGGER.info("processAcceptRequirement start");
		IngeniumPopulator 				ingPop = new IngeniumPopulator();
		Transaction 					tb = null;
		PolicyRequirementsData			prd = null;
		RequirementMaintenanceResponse 	reqtMaintResponse = null;

		init();
		if(this.useThread.trim().equalsIgnoreCase("true") && !this.isThread)  {
			Timer timer = new Timer();
			timer.schedule(new IngeniumThread(TRANSACTION_REQUIREMENT_MAINTENANCE,
						   REQTTRANS_ACCEPT, list), 1000);
			return;
		}

		try {
			for (int i = 0; i < list.size(); i++) {
				prd = (PolicyRequirementsData) list.get(i);
				tb = TransactionBuilder.buildRequirementMaintenanceTransaction(
					 ingPop.populateRequirementMaintenanceParm(prd, false));

				reqtMaintResponse = (RequirementMaintenanceResponse) tb.execute();
				IUMMessageData imd = ingPop.populateIUMMessageData(reqtMaintResponse.getRequirementData(), prd);

				ExceptionLog exLog = validateIUMMessageData(imd);
				if(exLog == null){
					prd.setABACUScreateDate(imd.getCreationDate());
					prd.setSequenceNumber(imd.getSequenceNumber());
					PolicyRequirements pr = new PolicyRequirements();
					pr.maintainRequirement(prd);
  				}else{
					exLog.setRecordType(TRANSACTION_REQUIREMENT_MAINTENANCE);
					exLog.setReply(tb.getResponse());
					exLog.setSystemInterface(IUMConstants.SYSTEM_INGENIUM_DESC);
					ExceptionLogger errorLogger = new ExceptionLogger();
					errorLogger.logError(exLog);
				}
			}
			
			validateMaintenanceResults(reqtMaintResponse.getMessages());
		} catch (Exception e) {
			logError(e, TRANSACTION_REQUIREMENT_MAINTENANCE);
			LOGGER.error(CodeHelper.getStackTrace(e));
		}
		LOGGER.info("processAcceptRequirement end");
	}

	/**
	 * This method is responsible for updating the requirements in Abacus with the changes done in IUM
	 * @param list ArrayList of policy requirements
	 * @throws IUMInterfaceException
	 */
	public void processOrderRequirement(final ArrayList list) throws IUMInterfaceException{
		
		LOGGER.info("processOrderRequirement start");
		IngeniumPopulator 		ingPop = new IngeniumPopulator();
		Transaction 			tb = null;
		PolicyRequirementsData	prd = null;
		RequirementMaintenanceResponse 	reqtMaintResponse = null;

		init();
		if(this.useThread.trim().equalsIgnoreCase("true") && !this.isThread)  {
			Timer timer = new Timer();
			timer.schedule(new IngeniumThread(TRANSACTION_REQUIREMENT_MAINTENANCE,
						   REQTTRANS_ORDER, list), 1000);
			return;
		}

		try {
			for (int i = 0; i < list.size(); i++) {
				prd = (PolicyRequirementsData) list.get(i);
				tb = TransactionBuilder.buildRequirementMaintenanceTransaction(
					 ingPop.populateRequirementMaintenanceParm(prd, false));

				reqtMaintResponse = (RequirementMaintenanceResponse) tb.execute();
				IUMMessageData imd = ingPop.populateIUMMessageData(reqtMaintResponse.getRequirementData(), prd);

				ExceptionLog exLog = validateIUMMessageData(imd);
				
				if(exLog == null){
					prd.setABACUScreateDate(imd.getCreationDate());
					prd.setSequenceNumber(imd.getSequenceNumber());
					PolicyRequirements pr = new PolicyRequirements();
					pr.maintainRequirement(prd);
				}else{
					exLog.setRecordType(TRANSACTION_REQUIREMENT_MAINTENANCE);
					exLog.setReply(tb.getResponse());
					exLog.setSystemInterface(IUMConstants.SYSTEM_INGENIUM_DESC);
					ExceptionLogger errorLogger = new ExceptionLogger();
					errorLogger.logError(exLog);
				}
			}
			
			validateMaintenanceResults(reqtMaintResponse.getMessages());
		} catch (Exception e) {
			logError(e, TRANSACTION_REQUIREMENT_MAINTENANCE);
			LOGGER.error(CodeHelper.getStackTrace(e));
		}
		LOGGER.info("processOrderRequirement end");
	}

	/**
	 * Updates Ingenium as to which requirements were rejected
	 * @param list ArrayList of policy requirements
	 * @throws IUMInterfaceException
	 */
	public void processRejectRequirement(final ArrayList list) throws IUMInterfaceException {
		
		LOGGER.info("processRejectRequirement start");
		IngeniumPopulator 				ingPop = new IngeniumPopulator();
		Transaction 					tb = null;
		PolicyRequirementsData			prd = null;
		RequirementMaintenanceResponse 	reqtMaintResponse = null;

		init();
		if(this.useThread.trim().equalsIgnoreCase("true") && !this.isThread)  {
			Timer timer = new Timer();
			timer.schedule(new IngeniumThread(TRANSACTION_REQUIREMENT_MAINTENANCE,
						   REQTTRANS_REJECT, list), 1000);
			return;
		}
		
		try {
			for (int i = 0; i < list.size(); i++) {
				prd = (PolicyRequirementsData) list.get(i);
				tb = TransactionBuilder.buildRequirementMaintenanceTransaction(
					 ingPop.populateRequirementMaintenanceParm(prd, false));

				reqtMaintResponse = (RequirementMaintenanceResponse) tb.execute();
				IUMMessageData imd = ingPop.populateIUMMessageData(reqtMaintResponse.getRequirementData(), prd);

				ExceptionLog exLog = validateIUMMessageData(imd);
				if(exLog == null){
					/*
					 * TODO verify do we update something in IUM?
					 * TODO dave Call the update methods to update abacus update date and by
					 */
				}else{
					exLog.setRecordType(TRANSACTION_REQUIREMENT_MAINTENANCE);
					exLog.setReply(tb.getResponse());
					exLog.setSystemInterface(IUMConstants.SYSTEM_INGENIUM_DESC);
					ExceptionLogger errorLogger = new ExceptionLogger();
					errorLogger.logError(exLog);
				}
			}
			
			validateMaintenanceResults(reqtMaintResponse.getMessages());
		} catch (Exception e) {
			logError(e, TRANSACTION_REQUIREMENT_MAINTENANCE);
			LOGGER.error(CodeHelper.getStackTrace(e));
		}
		LOGGER.info("processRejectRequirement end");
	}

	/**
	 * This method updates Ingenium as to which requirements has been cancelled in IUM
	 * @param list ArrayList of policy requirements
	 * @throws IUMInterfaceException
	 */
	public void processCancelRequirement(final ArrayList list) throws IUMInterfaceException{
		
		LOGGER.info("processCancelRequirement start");
		IngeniumPopulator 				ingPop = new IngeniumPopulator();
		Transaction 					tb = null;
		PolicyRequirementsData			prd = null;
		RequirementMaintenanceResponse 	reqtMaintResponse = null;

		init();
		if(this.useThread.trim().equalsIgnoreCase("true") && !this.isThread)  {
			Timer timer = new Timer();
			timer.schedule(new IngeniumThread(TRANSACTION_REQUIREMENT_MAINTENANCE,
						   REQTTRANS_CANCEL, list), 1000);
			return;
		}

		try {
			for (int i = 0; i < list.size(); i++) {
				prd = (PolicyRequirementsData) list.get(i);
				tb = TransactionBuilder.buildRequirementMaintenanceTransaction(
					 ingPop.populateRequirementMaintenanceParm(prd, false));

				reqtMaintResponse = (RequirementMaintenanceResponse) tb.execute();
				IUMMessageData imd = ingPop.populateIUMMessageData(reqtMaintResponse.getRequirementData(), prd);

				ExceptionLog exLog = validateIUMMessageData(imd);
				if(exLog == null){
					/*
					 * TODO verify do we update something in IUM?
					 * TODO dave Call the update methods to update abacus update date and by
					 */
				}else{
					exLog.setRecordType(TRANSACTION_REQUIREMENT_MAINTENANCE);
					exLog.setReply(tb.getResponse());
					exLog.setSystemInterface(IUMConstants.SYSTEM_INGENIUM_DESC);
					ExceptionLogger errorLogger = new ExceptionLogger();
					errorLogger.logError(exLog);
				}
			}
			
			validateMaintenanceResults(reqtMaintResponse.getMessages());
		} catch (Exception e) {
			logError(e, TRANSACTION_REQUIREMENT_MAINTENANCE);
			LOGGER.error(CodeHelper.getStackTrace(e));
		}
		LOGGER.info("processCancelRequirement end");
	}

	/**
	 * Updates Ingenium as to which requirements has been waived
	 * @param list ArrayList of policy requirements
	 * @throws IUMInterfaceException
	 */
	public void processWaiveRequirement(ArrayList list)
										throws IUMInterfaceException {
		
		LOGGER.info("processWaiveRequirement start");
		IngeniumPopulator 				ingPop = new IngeniumPopulator();
		Transaction 					tb = null;
		PolicyRequirementsData			prd = null;
		RequirementMaintenanceResponse 	reqtMaintResponse = null;

		init();
		if(this.useThread.trim().equalsIgnoreCase("true") && !this.isThread)  {
			Timer timer = new Timer();
			timer.schedule(new IngeniumThread(TRANSACTION_REQUIREMENT_MAINTENANCE,
						   REQTTRANS_WAIVE, list), 1000);
			return;
		}

		try {
			for (int i = 0; i < list.size(); i++) {
				prd = (PolicyRequirementsData) list.get(i);
				tb = TransactionBuilder.buildRequirementMaintenanceTransaction(
					 ingPop.populateRequirementMaintenanceParm(prd, false));

				reqtMaintResponse = (RequirementMaintenanceResponse) tb.execute();
				IUMMessageData imd = ingPop.populateIUMMessageData(reqtMaintResponse.getRequirementData(), prd);

				ExceptionLog exLog = validateIUMMessageData(imd);
				if(exLog==null){
					prd.setABACUScreateDate(imd.getCreationDate());
					prd.setSequenceNumber(imd.getSequenceNumber());
					PolicyRequirements pr = new PolicyRequirements();
					pr.maintainRequirement(prd);
				}else{
					exLog.setRecordType(TRANSACTION_REQUIREMENT_MAINTENANCE);
					exLog.setReply(tb.getResponse());
					exLog.setSystemInterface(IUMConstants.SYSTEM_INGENIUM_DESC);
					ExceptionLogger errorLogger = new ExceptionLogger();
					errorLogger.logError(exLog);
				}
			}
			
			validateMaintenanceResults(reqtMaintResponse.getMessages());
		} catch (Exception e) {
			logError(e, TRANSACTION_REQUIREMENT_MAINTENANCE);
			LOGGER.error(CodeHelper.getStackTrace(e));
		}
		LOGGER.info("processWaiveRequirement end");
	}

	/**
	 * Updates Abacus with the requirement changes done in IUM
	 * @param prd policy requirement
	 * @throws IUMInterfaceException
	 */
	public void processUpdateRequirement(final PolicyRequirementsData prd) throws IUMInterfaceException{
		
		LOGGER.info("processUpdateRequirement start");
		IngeniumPopulator 				ingPop = new IngeniumPopulator();
		Transaction 					tb = null;
		RequirementMaintenanceResponse 	reqtMaintResponse = null;

		init();
		if(this.useThread.trim().equalsIgnoreCase("true") && !this.isThread)  {
			Timer timer = new Timer();
			timer.schedule(new IngeniumThread(TRANSACTION_REQUIREMENT_MAINTENANCE,
							REQTTRANS_UPDATE, prd), 1000);
			return;
		}

		try {
			
			tb = TransactionBuilder.buildRequirementMaintenanceTransaction(
				 ingPop.populateRequirementMaintenanceParm(prd, false));

			reqtMaintResponse = (RequirementMaintenanceResponse) tb.execute();
			IUMMessageData imd = ingPop.populateIUMMessageData(reqtMaintResponse.getRequirementData(), prd);			
			ExceptionLog exLog = validateIUMMessageData(imd);
			if(exLog==null){
				prd.setABACUScreateDate(imd.getCreationDate());
				prd.setSequenceNumber(imd.getSequenceNumber());
				PolicyRequirements pr = new PolicyRequirements();
				pr.maintainRequirement(prd);
			}else{
				exLog.setRecordType(TRANSACTION_REQUIREMENT_MAINTENANCE);
				exLog.setReply(tb.getResponse());
				exLog.setSystemInterface(IUMConstants.SYSTEM_INGENIUM_DESC);
				ExceptionLogger errorLogger = new ExceptionLogger();
				errorLogger.logError(exLog);
			}
			
			validateMaintenanceResults(reqtMaintResponse.getMessages());
		} catch (Exception e) {
			logError(e, TRANSACTION_REQUIREMENT_MAINTENANCE);
			LOGGER.error(CodeHelper.getStackTrace(e));
		}

		LOGGER.info("processUpdateRequirement end");
	}


	
	private ExceptionLog validateARD(final AssessmentRequestData ard) {
		
		LOGGER.info("validateARD start");
		ExceptionLog exLog = null;
		ArrayList details = new ArrayList();
		CodeHelper ch = new CodeHelper();
		
		if(ard.getReferenceNumber()==null || ard.getReferenceNumber().trim().equals("")){
			details.add(createExceptionDetail("Invalid reference number"));
			LOGGER.warn("Exception: Invalid reference number");
		}

		if(ard.getAmountCovered()<0){
			details.add(createExceptionDetail("Invalid amount covered"));
			LOGGER.warn("Exception: Invalid amount covered");
		}
		
		if(ard.getPremium()<0){
			details.add(createExceptionDetail("Invalid premium amount"));
			LOGGER.warn("Exception: Invalid premium amount");
		}

		if(ard.getApplicationReceivedDate()==null){
			details.add(createExceptionDetail("Invalid application received date"));
			LOGGER.warn("Exception: Invalid application received date");
		}

		try{
			if (ard.getAssignedTo() != null){
				UserProfileData at = ch.getUserProfile(ard.getAssignedTo().getACF2ID());

				if(at==null){
					details.add(createExceptionDetail("Invalid value for NB_CNTCT_USER_ID"));
					LOGGER.warn("Exception: Invalid value for NB_CNTCT_USER_ID");
				}else{
					ard.setAssignedTo(at);
					ard.setFolderLocation(at);
				}
			}
		}catch(Exception e){
			details.add(createExceptionDetail(e.getMessage()));
		}

		try{
			UserProfileData uw = null;
			
			if(ard.getUnderwriter().getACF2ID()!=null && !"".equals(ard.getUnderwriter().getACF2ID())){
				uw = ch.getUserProfile(ard.getUnderwriter().getACF2ID());
			}

			if(uw == null){
			}else{
				ard.setUnderwriter(uw);
			}
		}catch(SQLException e){
			LOGGER.error(CodeHelper.getStackTrace(e));
		}

		try{
			
			UserProfileData agt = ch.getUserProfile(ard.getAgent().getUserId());
			if(agt==null){
				details.add(createExceptionDetail("Agent not found in IUM"));
				LOGGER.warn("Exception: Agent not found in IUM");
			}else{
				ard.setAgent(agt);
			}
		}catch(SQLException e){
			details.add(createExceptionDetail(e.getMessage()));
		}

		try{
			
			SunLifeOfficeData sod = ch.getSunLifeOffice(ard.getBranch().getOfficeId());
			if(sod==null){
				details.add(createExceptionDetail("SunLife office not defined in IUM"));
				LOGGER.warn("Exception: SunLife office not defined in IUM");
			}else{
				ard.setBranch(sod);
			}
		}catch(SQLException e){
			details.add(createExceptionDetail(e.getMessage()));
			LOGGER.error(CodeHelper.getStackTrace(e));
		}
		
		if (details.size()>0){
			exLog = new ExceptionLog();
			exLog.setDetails(details);
		}
		LOGGER.info("validateARD end");
		return exLog;
	}

	private ExceptionLog validateKOMessages(
			final KickOutMessageData koData,
			final Connection conn) {
		
		LOGGER.info("validateKOMessages start");
		ExceptionLog exLog = null;
		ArrayList details = new ArrayList();
		AssessmentRequest ar = new AssessmentRequest();
		
		
		if (koData != null){
			
			if (koData.getSequenceNumber() == 0) {
				details.add(createExceptionDetail("Invalid sequence number"));
			}
			
			if (koData.getReferenceNumber() == null 
					|| koData.getReferenceNumber().trim().equals("")) {
				
				details.add(createExceptionDetail("Reference number is null or empty."));
				LOGGER.warn("Reference number is null or empty");
			} else {
				
				boolean existingRequest = false;
				try {
					existingRequest = ar.isAssessmentRequestExist(koData.getReferenceNumber());
					LOGGER.warn("EXISTING TRY: "+existingRequest);
				} catch (IUMException e) {
					details.add(createExceptionDetail(e.getMessage()));
				}
				
				if (!existingRequest){
					details.add(createExceptionDetail("Non-existing reference number"));
					LOGGER.warn("Non-existing reference number: Kickout ");
				}
			}
		}

		if (details.size()>0){
			exLog = new ExceptionLog();
			exLog.setDetails(details);
		}
		LOGGER.info("validateKOMessages end");
		return exLog;
	}
	
	// New validate KoMessage
	// RefNo is passed and not called
	private ExceptionLog validateKOMessages(
			final boolean existRefNo,
			final String errMsg,
			final KickOutMessageData koData) {
		
		LOGGER.info("validateKOMessages start");
		ExceptionLog exLog = null;
		ArrayList details = new ArrayList();
		AssessmentRequest ar = new AssessmentRequest();
		
		
		if (koData != null){
			if (koData.getSequenceNumber() == 0) {
				
				details.add(createExceptionDetail("Invalid sequence number"));
				LOGGER.warn("Invalid sequence number");
			}
			
			if (koData.getReferenceNumber() == null 
					|| koData.getReferenceNumber().trim().equals("")) {
				
				details.add(createExceptionDetail("Reference number is null or empty."));
				LOGGER.warn("Reference number is null or empty");
			} else {
				if(!errMsg.equals("")){
					details.add(createExceptionDetail(errMsg));
				}
				
				if (!existRefNo){
					details.add(createExceptionDetail("Non-existing reference number"));
					LOGGER.warn("Non-existing reference number: Kickout ");
				}
			}
		}

		if (details.size()>0){
			exLog = new ExceptionLog();
			exLog.setDetails(details);
		}
		LOGGER.info("validateKOMessages end");
		return exLog;
	}

	private ExceptionLog validateClientInfo(final ClientData clData){
		
		LOGGER.info("validateClientInfo start");
		ExceptionLog exLog = null;
		ArrayList details = new ArrayList();
		
		if(clData != null
				&& clData.getSex()!=null
				&& !"C".equalsIgnoreCase(clData.getSex())){
			if (clData.getAge() < 0){
				details.add(createExceptionDetail("Invalid value for age"));
				LOGGER.warn("Invalid value for age");
			}
		}

		if (details.size()>0){
			exLog = new ExceptionLog();
			exLog.setDetails(details);
		}
		LOGGER.info("validateClientInfo end");
		return exLog;
	}

	private ExceptionLog validateSummaryPolicyCoverage(CDSSummaryPolicyCoverageData cdsSummary){
		
		LOGGER.info("validateSummaryPolicyCoverage start");
		ExceptionLog exLog = null;
		ArrayList details = new ArrayList();
		if (cdsSummary != null){
			if (cdsSummary.getTotalReinsuredAmount() < 0){
				details.add(createExceptionDetail("Invalid value for total reinsured amount"));
				LOGGER.warn("Invalid value for total reinsured amount");
			}
			if (cdsSummary.getTotalFBBMBCoverage() < 0){
				details.add(createExceptionDetail("Invalid value for total FBBMB coverage"));
				LOGGER.warn("Invalid value for total FBBMB coverage");
			}
			if (cdsSummary.getTotalHIBCoverage() < 0){
				details.add(createExceptionDetail("Invalid value for total HIB coverage"));
				LOGGER.warn("Invalid value for total HIB coverage");
			}
			if (cdsSummary.getTotalAPDBCoverage() < 0){
				details.add(createExceptionDetail("Invalid value for total APDB coverage"));
				LOGGER.warn("Invalid value for total APDB coverage");
			}
			if (cdsSummary.getTotalCCRCoverage() < 0){
				details.add(createExceptionDetail("Invalid value for total CCR coverage"));
				LOGGER.warn("Invalid value for total CCR coverage");
			}
			if (cdsSummary.getOtherCompaniesAd() < 0){
				details.add(createExceptionDetail("Invalid value for Other Companies Ad"));
				LOGGER.warn("Invalid value for Other Companies Ad");
			}
			if (cdsSummary.getOtherCompaniesBr() < 0){
				details.add(createExceptionDetail("Invalid value for Other Companies Br"));
				LOGGER.warn("Invalid value for Other Companies Br");
			}
			if (cdsSummary.getExistingInForceAd() < 0){
				details.add(createExceptionDetail("Invalid value for Existing In Force Ad"));
				LOGGER.warn("Invalid value for Existing In Force Ad");
			}
			if (cdsSummary.getExistingInForceBr() < 0){
				details.add(createExceptionDetail("Invalid value for Existing In Force Br"));
				LOGGER.warn("Invalid value for Existing In Force Br");
			}
			if (cdsSummary.getExistingLapsedAd() < 0){
				details.add(createExceptionDetail("Invalid value for Existing Lapsed Ad"));
				LOGGER.warn("Invalid value for Existing Lapsed Ad");
			}
			if (cdsSummary.getExistingLapsedBr() < 0){
				details.add(createExceptionDetail("Invalid value for Existing Lapsed Br"));
				LOGGER.warn("Invalid value for Existing Lapsed Br");
			}
			if (cdsSummary.getPendingAmountAd() < 0){
				details.add(createExceptionDetail("Invalid value for Pending Amount Ad"));
				LOGGER.warn("Invalid value for Pending Amount Ad");
			}
			if (cdsSummary.getPendingAmountBr() < 0){
				details.add(createExceptionDetail("Invalid value for Pending Amount Br"));
				LOGGER.warn("Invalid value for Pending Amount Br");
			}
			if (cdsSummary.getAppliedForAd() < 0){
				details.add(createExceptionDetail("Invalid value for Pending Amount Ad"));
				LOGGER.warn("Invalid value for Pending Amount Ad");
			}
			if (cdsSummary.getAppliedForBr() < 0){
				details.add(createExceptionDetail("Invalid value for Pending Amount Br"));
				LOGGER.warn("Invalid value for Pending Amount Br");
			}
		}

		if (details.size() > 0){
			exLog = new ExceptionLog();
			exLog.setDetails(details);
		} 
		LOGGER.info("validateSummaryPolicyCoverage end");
		return exLog;
	}

	private ExceptionLog validateMortalityRating(final CDSMortalityRatingData mortalityData){
		
		LOGGER.info("validateMortalityRating start");
		ExceptionLog exLog = null;
		ArrayList details = new ArrayList();

		if (mortalityData != null){
			
			if (mortalityData.getHeightInFeet() < 0 || mortalityData.getHeightInInches() < 0){
				details.add(createExceptionDetail("Invalid value for height"));
				LOGGER.warn("Invalid value for height");
			}
			
			if (mortalityData.getWeight() < 0){
				details.add(createExceptionDetail("Invalid value for weight"));
				LOGGER.warn("Invalid value for weight");
			}
			
			if (mortalityData.getAPDB() < 0){
				details.add(createExceptionDetail("Invalid value for APDB"));
				LOGGER.warn("Invalid value for APDB");
			}
			
			if (mortalityData.getCCR() < 0){
				details.add(createExceptionDetail("Invalid value for CCR"));
				LOGGER.warn("Invalid value for CCR");
			}
			
			if (mortalityData.getBasic() < 0){
				details.add(createExceptionDetail("Invalid value for Basic"));
				LOGGER.warn("Invalid value for Basic");
			}
		}

		if (details.size() > 0){
			exLog = new ExceptionLog();
			exLog.setDetails(details);
		}
		LOGGER.info("validateMortalityRating end");
		return exLog;
	}

	private ExceptionLog validatePolicyRequirements(PolicyRequirementsData reqData, Connection conn) throws SQLException{
		
		LOGGER.info("validatePolicyRequirements start 1");
		ExceptionLog exLog = null;
		ArrayList details = new ArrayList();
		AssessmentRequest ar = new AssessmentRequest();
		RequirementDAO reqdao = new RequirementDAO();
		
		if (reqData != null){
			
			if(reqData.getLevel().equalsIgnoreCase(IUMConstants.LEVEL_POLICY)){
				
				if (reqData.getReferenceNumber() == null
						|| reqData.getReferenceNumber().trim().equals("")){
					
					details.add(createExceptionDetail("Reference number is null or empty"));
					LOGGER.warn("Reference number is null or empty");
				} else {
					
					boolean policyExist = false;
					try {
						
						policyExist = ar.isAssessmentRequestExist(reqData.getReferenceNumber());
						if (!policyExist){
							
							details.add(createExceptionDetail("Non-existing reference number"));
							LOGGER.warn("Non-existing reference number: Requirement " );
						}
					} catch (IUMException e) {
						details.add(createExceptionDetail(e.getMessage()));
					}
				}
			} else {
				
				if (reqData.getClientType() == null){
					
					details.add(createExceptionDetail("Client Type is null or empty"));
					LOGGER.warn("Client Type is null or empty");
				} else {
					
					if (IUMConstants.CLIENT_TYPE_INSURED.equalsIgnoreCase(reqData.getClientType())){
						
						if (reqData.getClientId() != null
								&& !reqData.getClientId().trim().equals("")){
							
							ClientDAO clDao = new ClientDAO();
							ClientData clData = null;
							
							try {
								
								clData = clDao.retrieveClient(reqData.getClientId());
								if (clData == null){
									
									details.add(createExceptionDetail("Non-existing client"));
									LOGGER.warn("Non-existing client");
								}
							} catch (SQLException e) {
								details.add(createExceptionDetail(e.getMessage()));
								LOGGER.error(CodeHelper.getStackTrace(e));
							}
						}
					}
				}
			}
		
			if (reqData.getRequirementCode() != null
					&& !reqData.getRequirementCode().trim().equals("")){
				
				RequirementData requirement = null;
				try {
					requirement = reqdao.retrieveRequirement(reqData.getRequirementCode());
					if (requirement == null){
						
						RequirementDAO reqmtDao = new RequirementDAO();
						RequirementData data = new RequirementData();
						data.setReqtCode(reqData.getRequirementCode().toUpperCase());
						data.setReqtDesc(reqData.getRequirementCode().toUpperCase() + " - <Description>" );
						data.setLevel(reqData.getLevel());
						long validity = 99;
						
						data.setValidity(validity);
						data.setFormIndicator("0");
						long formID = 1;
						data.setFormID(formID);
						data.setFollowUpNum(Integer.parseInt(String.valueOf(reqData.getFollowUpNumber())));
						data.setCreatedBy(reqData.getCreatedBy());
						data.setCreateDate(new Date());
						data.setTestDateIndicator("0");
						reqmtDao.insertRequirement(data);
						LOGGER.warn("Non-existing requirement code "+reqData.getRequirementCode());
					}
				} catch (SQLException e) {
					details.add(createExceptionDetail(e.getMessage()));
					LOGGER.error(CodeHelper.getStackTrace(e));
				}
			}
		
			StatusData status = reqData.getStatus();
			if (status != null){
				
				if (status.getStatusId() != 0){
					StatusCHDao statDao = new StatusCHDao();
					boolean isStatusExist = false;
					try {
						isStatusExist = statDao.isStatusExist(reqData.getStatus().getStatusId(),
															  IUMConstants.STAT_TYPE_NM);
						if (!isStatusExist){
							details.add(createExceptionDetail("Non-existing status"));
							LOGGER.warn("Non-existing status");
						}
					} catch (SQLException e) {
						details.add(createExceptionDetail(e.getMessage()));
						LOGGER.error(CodeHelper.getStackTrace(e));
					}
				}
			} else {
				details.add(createExceptionDetail("Status is null or empty"));
				LOGGER.warn("Status is null or empty");
			}
			
			if (reqData.getSequenceNumber() < 0){
				details.add(createExceptionDetail("Invalid value for sequence number"));
				LOGGER.warn("Invalid value for sequence number");
			}
		}
		
		if (details.size() > 0){
			exLog = new ExceptionLog();
			exLog.setDetails(details);
		}
		LOGGER.info("validatePolicyRequirements end 1");
		return exLog;
	}
	
	// New validatePolicyRequirements
	// * pass boolean value checking if refNo is existing
	private ExceptionLog validatePolicyRequirements(
			final Connection conn,
			final boolean existRefNo,
			final String errMsg,
			final PolicyRequirementsData reqData) throws SQLException{
		
		LOGGER.info("validatePolicyRequirements start 2");
		ExceptionLog exLog = null;
		ArrayList details = new ArrayList();
		AssessmentRequest ar = new AssessmentRequest();
		RequirementDAO reqdao = new RequirementDAO();

		if (reqData != null){
			
			if(reqData.getLevel().equalsIgnoreCase(IUMConstants.LEVEL_POLICY)) {
				
				if (reqData.getReferenceNumber() == null
						|| reqData.getReferenceNumber().trim().equals("")) {
					
					details.add(createExceptionDetail("Reference number is null or empty"));
					LOGGER.warn("Reference number is null or empty");
				} else {
					
					if (!existRefNo){
						details.add(createExceptionDetail(errMsg));
					}
				}
			} else {
				
				if (reqData.getClientType() == null){
					
					details.add(createExceptionDetail("Client Type is null or empty"));
					LOGGER.warn("Client Type is null or empty");
				} else {
					
					if (IUMConstants.CLIENT_TYPE_INSURED.equalsIgnoreCase(reqData.getClientType())){
						
						if (reqData.getClientId() != null
								&& !reqData.getClientId().trim().equals("")){
							
							ClientDAO clDao = new ClientDAO();
							ClientData clData = null;
							
							try {
								
								clData = clDao.retrieveClient(reqData.getClientId());
								if (clData == null){
									
									details.add(createExceptionDetail("Non-existing client"));
									LOGGER.warn("Non-existing client");
								}
							} catch (SQLException e) {
								details.add(createExceptionDetail(e.getMessage()));
								LOGGER.error(CodeHelper.getStackTrace(e));
							}
						}
					}
				}
			}
		
			if (reqData.getRequirementCode() != null
					&& !reqData.getRequirementCode().trim().equals("")){
				
				RequirementData requirement = null;
				try {
					requirement = reqdao.retrieveRequirement(reqData.getRequirementCode());
					if (requirement==null){
						
						RequirementDAO reqmtDao = new RequirementDAO();
						RequirementData data = new RequirementData();
						data.setReqtCode(reqData.getRequirementCode().toUpperCase());
						data.setReqtDesc(reqData.getRequirementCode().toUpperCase() + " - <Description>" );
						data.setLevel(reqData.getLevel());
						
						long validity = 99;
						data.setValidity(validity);
						data.setFormIndicator("0");
						
						long formID = 1;
						data.setFormID(formID);
						data.setFollowUpNum(Integer.parseInt(String.valueOf(reqData.getFollowUpNumber())));
						data.setCreatedBy(reqData.getCreatedBy());
						data.setCreateDate(new Date());
						data.setTestDateIndicator("0");
						reqmtDao.insertRequirement(data);
						LOGGER.warn("Non-existing requirement code "+reqData.getRequirementCode());
					}
				} catch (SQLException e) {
					details.add(createExceptionDetail(e.getMessage()));
					LOGGER.error(CodeHelper.getStackTrace(e));
				}
			}
		
			StatusData status = reqData.getStatus();
			if (status != null) {
				
				if (status.getStatusId() != 0) {
					if (!IUMConstants.STAT_TYPE_NM.equals(status.getStatusType())) {
						details.add(createExceptionDetail("Non-existing status"));
						LOGGER.warn("Non-existing status");
					}
				}
			} else {
				
				details.add(createExceptionDetail("Status is null or empty"));
				LOGGER.warn("Status is null or empty");
			}
			
			if (reqData.getSequenceNumber() < 0) {
				
				details.add(createExceptionDetail("Invalid value for sequence number"));
				LOGGER.warn("Invalid value for sequence number");
			}
		}
		
		if (details.size() > 0) {
			exLog = new ExceptionLog();
			exLog.setDetails(details);
		}
		LOGGER.info("validatePolicyRequirements end 2");
		return exLog;
	}

	// Method for checking policy coverage
	// * Removed param Connection as it is not being used
	private ExceptionLog validatePolicyCoverageDetails(
			final PolicyDetailData policyData){
		
		LOGGER.info("validatePolicyCoverageDetails start");
		ExceptionLog exLog = null;
		ArrayList details = new ArrayList();

		if (policyData != null) {
			
			if (policyData.getPolicyNumber() == null
					|| policyData.getPolicyNumber().equals("")){
				
				details.add(createExceptionDetail("Policy number is null or empty"));
				LOGGER.warn("Policy number is null or empty");
			}

			if (policyData.getFaceAmount() < 0){
				details.add(createExceptionDetail("Invalid value for Face Amount"));
				LOGGER.warn("Invalid value for Face Amount");
			}
			
			if (policyData.getCoverageNumber() < 0){
				details.add(createExceptionDetail("Invalid value for Coverage Number"));
				LOGGER.warn("Invalid value for Coverage Number");
			}
			
			if (policyData.getADBFaceAmount() < 0){
				details.add(createExceptionDetail("Invalid value for ADB Face Amount"));
				LOGGER.warn("Invalid value for ADB Face Amount");
			}
			
			if (policyData.getADMultiplier() < 0){
				details.add(createExceptionDetail("Invalid value for AD Multiplier"));
				LOGGER.warn("Invalid value for AD Multiplier");
			}
			
			if (policyData.getWPMultiplier() < 0){
				details.add(createExceptionDetail("Invalid value for WP Multiplier"));
				LOGGER.warn("Invalid value for WP Multiplier");
			}
			
			if (policyData.getReinsuredAmount() < 0){
				details.add(createExceptionDetail("Invalid value for Reinsured Amount"));
				LOGGER.warn("Invalid value for Reinsured Amount");
			}
		}

		if (details.size() > 0){
			exLog = new ExceptionLog();
			exLog.setDetails(details);
		}
		LOGGER.info("validatePolicyCoverageDetails end");
		return exLog;
	}

	private ExceptionLog validateIUMMessageData(final IUMMessageData imd){
		
		LOGGER.info("validateIUMMessageData start");
		ExceptionLog exLog = null;
		ArrayList details = new ArrayList();

		if(imd.getPolicyClientCode()==null || imd.getPolicyClientCode().trim().equals("")){
			details.add(createExceptionDetail("Invalid policy client code"));
			LOGGER.warn("Invalid policy client code");
		}
		
		if(imd.getPolicyClientId()==null || imd.getPolicyClientId().trim().equals("")){
			details.add(createExceptionDetail("Invalid policy client id"));
			LOGGER.warn("Invalid policy client id");
		}
		
		if(imd.getRequirementId()==null || imd.getRequirementId().trim().equals("")){
			details.add(createExceptionDetail("Invalid requirment id"));
			LOGGER.warn("Invalid requirment id");
		}
		
		if(imd.getSequenceNumber()<0){
			details.add(createExceptionDetail("Invalid sequence number"));
			LOGGER.warn("Invalid sequence number");
		}

		if (details.size()>0){
			exLog = new ExceptionLog();
			exLog.setDetails(details);
		}
		LOGGER.info("validateIUMMessageData end");
		return exLog;
	}

	private ExceptionDetail createExceptionDetail(final String message){
		
		LOGGER.info("createExceptionDetail start");
		ExceptionDetail ed = new ExceptionDetail();
		ed.setMessage(message);
		LOGGER.info("createExceptionDetail end");
		return ed;
	}

	private void logError(final Exception e, final String trans) {
		
		LOGGER.info("logError start");
		final Date start = new Date();
		Date end;
		long elapse;
		long startTmp = System.currentTimeMillis();
		long endTmp;
		
		end = new Date(); endTmp = System.currentTimeMillis();
		elapse = ((end.getTime() - start.getTime()) % (60 * 1000)) / 1000;
		
		end = new Date(); endTmp = System.currentTimeMillis();
		elapse = ((end.getTime() - start.getTime()) % (60 * 1000)) / 1000;
		
		ExceptionLog el= new ExceptionLog();
		el.setRecordType(trans);
		el.setSystemInterface(IUMConstants.SYSTEM_INGENIUM_DESC);
		
		ArrayList arr = new ArrayList();
		arr.add(createExceptionDetail(e.getMessage()));
		el.setDetails(arr);
		
		ExceptionLogger errorLogger = new ExceptionLogger();
		errorLogger.logError(el);
		end = new Date(); endTmp = System.currentTimeMillis();
		elapse = ((end.getTime() - start.getTime()) % (60 * 1000)) / 1000;
		LOGGER.info("logError end");
	}



	/****Utilities****/
	private void init() throws IUMInterfaceException{
		
		LOGGER.info("init start");
		try{
			ResourceBundle rb = ResourceBundle.getBundle(IUMConstants.IUM_CONFIG);
			useThread = rb.getString("usethreads");
			LOGGER.debug("Abacus [Ingenium] use threads? = " + useThread);
		}catch(MissingResourceException e){
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMInterfaceException(e.getMessage());
		}
		LOGGER.info("init end");
	}

	private void setIngeniumSpecificDetails(final AssessmentRequestData ard) {
		
		LOGGER.info("setIngeniumSpecificDetails start");
		StatusData sd = new StatusData();
		sd.setStatusId(IUMConstants.STATUS_NB_REVIEW_ACTION);
		LOBData lob = new LOBData();
		lob.setLOBCode(IUMConstants.LOB_INDIVIDUAL_LIFE);

		ard.setStatus(sd);
		ard.setLob(lob);
		ard.setSourceSystem(IUMConstants.SYSTEM_INGENIUM);
		ard.setStatusDate(new Date());
		ard.setCreatedDate(new Date());

		UserProfileData upd = new UserProfileData();
		upd.setUserId(IUMConstants.USER_IUM);
		ard.setCreatedBy(upd);
		ard.setFolderLocation(ard.getAssignedTo());
		
		LOGGER.info("setIngeniumSpecificDetails end");
	}


	/**
	 * This method will get the policy info and insert into IUM tables
	 * @param String polNo - first 9 digits of a policy
	 * @param String polSuf - last digit of a policy
	 */
	private AssessmentRequestData populateAssessment(final String polNo, final String polSuf) {
		
		LOGGER.info("populateAssessment start");
		AssessmentRequestData ard = new AssessmentRequestData();
		IngeniumPopulator ingPop = new IngeniumPopulator();
		AssessmentRequest ar = new AssessmentRequest();
		
		
		try{
			Transaction tb = TransactionBuilder.buildPolicyListTransaction(polNo, polSuf);
			PolicyListResponse polResponse = (PolicyListResponse) tb.execute();
			List polList = polResponse.getPolicyList();
			
			if ((polList != null) && (polList.size() > 0)) {
				try {
					
					
					for (int i=0; i < polList.size(); i++) {

						PolicyInformation polInfo = (PolicyInformation) polList.get(i);
						ingPop.populateAssessmentRequestData(ard, polInfo);
						setIngeniumSpecificDetails(ard);
						ard.setAssignedTo(ard.getAgent());
						ar.createAssessmentRequest2(ard);
					}
				} catch (Exception e) {
					LOGGER.error("Policy No-->" + polNo + polSuf + " - ERROR IN SAVING ASSESSMENT: ");
					logError(e, TRANSACTION_POLICY_QUEUE);
					LOGGER.error(CodeHelper.getStackTrace(e));
				}
			} else {
				LOGGER.warn("Policy No-->" + polNo + polSuf + " - Policy does not exists.");
			}
		} catch (Exception e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			LOGGER.error("Policy No--> + polNo + polSuf + Error in getting policy info in xml: ");
		}
		LOGGER.info("populateAssessment end");
		return ard;
	}

	/**
	 * This method will validate the success of a requirement maintenance transaction
	 * @param resultList
	 * @throws IUMInterfaceException
	 */
	private void validateMaintenanceResults(final List resultList) throws IUMInterfaceException {
		
		LOGGER.info("validateMaintenanceResults start");
		// NOTE : should we log every error message all at once?
		try {
			for (int i = 0; i < resultList.size(); i++)  {
				ResultStatus rs = (ResultStatus) resultList.get(i);
				if (Integer.parseInt(rs.getResultCode()) >=
					Integer.parseInt(ResultStatus.FAIL)) {
					throw new IUMInterfaceException(rs.getResultText());
				}
			}
		} catch (Exception e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
		}
		LOGGER.info("validateMaintenanceResults end");
	}
	
	private ClientData copyClientData(final ClientData clData){
		
		LOGGER.info("copyClientData start");
		ClientData newClData = new ClientData();
		if (clData != null){
			
			newClData.setAge(clData.getAge());
			newClData.setBirthDate(clData.getBirthDate());
			newClData.setBirthLocation(clData.getBirthLocation());
			newClData.setClientId(clData.getClientId());
			newClData.setGivenName(clData.getGivenName());
			newClData.setLastName(clData.getLastName());
			newClData.setMiddleName(clData.getMiddleName());
			newClData.setOtherGivenName(clData.getOtherGivenName());
			newClData.setOtherLastName(clData.getOtherLastName());
			newClData.setOtherMiddleName(clData.getOtherMiddleName());
			newClData.setSex(clData.getSex());
			newClData.setSmoker(clData.isSmoker());
			newClData.setSuffix(clData.getSuffix());
			newClData.setTitle(clData.getTitle());			
		}
		
		LOGGER.info("copyClientData end");
		return newClData;
	}	

	private void closeConnection(final Connection conn) throws IUMInterfaceException{
		
		
		try{
			if(conn!=null){
				conn.close();
			}
		}catch(SQLException e){
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMInterfaceException(e.getMessage());
		}
		
	}
	
	private void rollbackTransaction(final Connection conn) throws IUMInterfaceException{
			
		LOGGER.info("rollbackTransaction start");
		try {
			if (conn != null){
				conn.rollback();
			}
		} catch (SQLException e){
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMInterfaceException(e.getMessage());
		}
		LOGGER.info("rollbackTransaction end");
	}
	
	public String removeNull(final String str){
		
		LOGGER.info("removeNull start");
		String toRet;
		if(str == null) {
			toRet = "";
		} else {
			toRet = str.trim();
		}
		LOGGER.info("removeNull end");
		return toRet;
	}

	public void processAutoSettleAssessmentRequest(final AssessmentRequestData ard) throws IUMInterfaceException {
		// TODO Auto-generated method stub
	}
}
