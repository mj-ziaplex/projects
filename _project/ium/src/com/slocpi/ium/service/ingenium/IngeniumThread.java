/*
 * Created on Apr 25, 2006
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.service.ingenium;

import java.util.ArrayList;
import java.util.TimerTask;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.slocpi.ium.data.PolicyRequirementsData;
import com.slocpi.ium.util.CodeHelper;

/**
 * @author Glenn Gonzales
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class IngeniumThread extends TimerTask {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(IngeniumThread.class);
	private String  transactionType = "";
	private int   	transactionSubType;
	private Object  processParams = null;

	/**
	 * This class instantiates an IngeniumThread which runs a process 
	 * according to the calling IngeniumGateway process. If the process 
	 * from gateway has parameters, the paramter should be passed here
	 * @param transactionType		- Transaction type found in IngeniumGateway
	 * @param transactionSubType	- Transaction subtype only applicable for Requirements Maintenance
	 * @param processParams			- IngeniumGateway process parameter
	 */
	public IngeniumThread(String 	transactionType,
						  int		transactionSubType,
						  Object	processParams) {
		super();
		LOGGER.info("IngeniumThread start");
		this.transactionType = transactionType;
		this.transactionSubType = transactionSubType;
		this.processParams = processParams;
		LOGGER.debug("[IngeniumThread] transactionType: " + transactionType + 
						   "; transactionSubType : " + transactionSubType);
		LOGGER.info("IngeniumThread end");
	};
	
	public void run () {
		
		LOGGER.info("run start");
		IngeniumGateway ing = new IngeniumGateway(true);
		try {
			if (this.transactionType.equalsIgnoreCase(IngeniumGateway.TRANSACTION_POLICY_QUEUE)) {
				ing.processGetPolicyQueue();
			} else if (this.transactionType.equalsIgnoreCase(IngeniumGateway.TRANSACTION_KICK_OUT)) {
				ing.processPolicyDetails((String)this.processParams);
			} else if (this.transactionType.equalsIgnoreCase(IngeniumGateway.TRANSACTION_REQUIREMENT_MAINTENANCE)) {
				switch(this.transactionSubType) {
					case IngeniumGateway.REQTTRANS_ACCEPT:
						ing.processAcceptRequirement((ArrayList)this.processParams);
						break;
					case IngeniumGateway.REQTTRANS_CANCEL:
						ing.processCancelRequirement((ArrayList)this.processParams);
						break;
					case IngeniumGateway.REQTTRANS_CREATE:
						ing.processCreateRequirement((PolicyRequirementsData)this.processParams);
						break;
					case IngeniumGateway.REQTTRANS_ORDER:
						ing.processOrderRequirement((ArrayList)this.processParams);
						break;
					case IngeniumGateway.REQTTRANS_RECEIVE:
						ing.processReceiveRequirement((ArrayList)this.processParams);
						break;
					case IngeniumGateway.REQTTRANS_REJECT:
						ing.processRejectRequirement((ArrayList)this.processParams);
						break;
					case IngeniumGateway.REQTTRANS_UPDATE:
						PolicyRequirementsData prd =(PolicyRequirementsData)this.processParams;
						
						
						ing.processUpdateRequirement((PolicyRequirementsData)this.processParams);
						break;
					case IngeniumGateway.REQTTRANS_WAIVE:
						ing.processWaiveRequirement((ArrayList)this.processParams);
						break;
				}
			}
		} catch (Exception e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
		}
		LOGGER.info("run end");
	}
}
