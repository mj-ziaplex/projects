package com.slocpi.ium.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.slocpi.ium.service.ingenium.IngeniumGateway;
import com.slocpi.ium.util.IUMConstants;

/****************************************************
 * @author @nic.decapia
 * @version 1.0
 * TODO Class Description of AdminSystemFactory.java
 * ***************************************************
 * @author Shellz - Sept 25, 2014
 * @version 1.1
 * TODO -Additional more informed logging
 */
public class AdminSystemFactory {

	private static final Logger LOGGER = LoggerFactory.getLogger(AdminSystemFactory.class);

	public static AdminSystem getAdminSystem(final String sourceSystem) {
		
		LOGGER.info("start getAdminSystem");
		AdminSystem as = null;
		if (sourceSystem == null || sourceSystem.equalsIgnoreCase(IUMConstants.SYSTEM_INGENIUM)) {
			LOGGER.debug("creating Ingenium Gateway");
			as = new IngeniumGateway();
		} else {
			LOGGER.debug("creating ABACUS Controller");
			as = new MessageController();
		}
		
		LOGGER.info("end getAdminSystem");
		return as;
	}
}
