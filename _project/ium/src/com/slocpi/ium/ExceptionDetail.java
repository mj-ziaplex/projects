/*
 * Created on Feb 19, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium;

/**
 * @author ypojol
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class ExceptionDetail {
	private String exceptionID;
	private String message;
	/**
	 * 
	 */
	public ExceptionDetail() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @return
	 */
	public String getExceptionID() {
		return exceptionID;
	}

	/**
	 * @return
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param string
	 */
	public void setExceptionID(String string) {
		exceptionID = string;
	}

	/**
	 * @param string
	 */
	public void setMessage(String string) {
		message = string;
	}

}
