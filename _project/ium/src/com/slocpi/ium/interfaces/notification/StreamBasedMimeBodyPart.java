package com.slocpi.ium.interfaces.notification;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import javax.mail.MessagingException;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeUtility;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.slocpi.ium.util.CodeHelper;

public class StreamBasedMimeBodyPart extends MimeBodyPart {

	private static final Logger LOGGER = LoggerFactory.getLogger(StreamBasedMimeBodyPart.class);
	
public StreamBasedMimeBodyPart(InputStream in, String type, String filename) {
	super();
	
	LOGGER.info("constructor start");
	try {
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		OutputStream out = MimeUtility.encode(bos, "base64");
		byte[] b = new byte[1024];
		while(in.read(b) > 0){
		out.write(b);
		} 
		this.content = bos.toByteArray();		
		setHeader("Content-Type", type + "; name=\""+filename+"\"");
		setHeader("Content-Transfer-Encoding", "base64");
		setDisposition("attachment");
		setFileName(filename);		
	} catch (IOException e) {
		LOGGER.error(CodeHelper.getStackTrace(e));
	} catch (MessagingException e) {
		LOGGER.error(CodeHelper.getStackTrace(e));
	}
	LOGGER.info("constructor end");
}
}
