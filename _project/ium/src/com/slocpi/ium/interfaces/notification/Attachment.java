/**
 * Attachment.java				Copyright (c) 2004 Pointwest Technologies Corp.
 * TODO DOCUMENT ME!
 * 
 * @author Barry Yu	 @date Feb 18, 2004
 */
package com.slocpi.ium.interfaces.notification;

import java.io.ByteArrayOutputStream;

/**
 * TODO DOCUMENT ME!
 * 
 * @author Barry Yu		Copyright (c) 2004 Pointwest Technologies Corp.
 * @date Feb 18, 2004
 */
public class Attachment{
	ByteArrayOutputStream attachment = null;
	String attachmentName = null;
	

	/**
	 * @return
	 */
	public ByteArrayOutputStream getAttachment() {
		return attachment;
	}

	/**
	 * @return
	 */
	public String getAttachmentName() {
		return attachmentName;
	}

	/**
	 * @param stream
	 */
	public void setAttachment(ByteArrayOutputStream stream) {
		attachment = stream;
	}

	/**
	 * @param string
	 */
	public void setAttachmentName(String string) {
		attachmentName = string;
	}

}
