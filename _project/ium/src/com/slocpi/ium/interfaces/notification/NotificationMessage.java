/*
 * Created on Jan 7, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.interfaces.notification;

import java.util.ArrayList;
import java.util.ResourceBundle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.slocpi.ium.util.CodeHelper;

/**
 * @author daguila
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class NotificationMessage {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(NotificationMessage.class);
	private ArrayList 		recipients;
	private String			sender;
	private String			subject;
	private String			content;
	private Attachment		attachment;
	private boolean         isMail = true;
	private boolean         isSMS  = false;
	private String orderSpecialCase = null;
	private ArrayList 		requirementAttachment;
    private String          compCode;


	/**
	 * @return
	 */
	public String getContent() {
		return content;
	}

	/**
	 * @return
	 */
	public ArrayList getRecipients() {
		return recipients;
	}

	/**
	 * @return
	 */
	public String getSender() {
		return sender;
	}

	/**
	 * @return
	 */
	public String getSubject() {
		return subject;
	}

	/**
	 * @param string
	 */
	public void setContent(String string) {
		content = string;
	}

	/**
	 * @param list
	 */
	public void setRecipients(ArrayList list) {
		recipients = list;
	}

	/**
	 * @param string
	 */
	public void setSender(String string) {
		sender = string;
	}

	/**
	 * @param string
	 */
	public void setSubject(String string) {
		subject = string;
	}

	/**
	 * @return
	 */
	public boolean isMail() {
		return isMail;
	}

	/**
	 * @return
	 */
	public boolean isSMS() {
		return isSMS;
	}

	/**
	 * @param b
	 */
	public void setMail(boolean b) {
		isMail = b;
		isSMS  = !b;
	}

	/**
	 * @param b
	 */
	public void setSMS(boolean b) {
		isSMS = b;
		isMail = !b;
	}



	/**
	 * @return
	 */
	public Attachment getAttachment() {
		return attachment;
	}

	/**
	 * @param attachment
	 */
	public void setAttachment(Attachment attachment) {
		this.attachment = attachment;
	}

	/**
	 * @return
	 */
	public String getOrderSpecialCase() {
		return orderSpecialCase;
	}

	/**
	 * @param string
	 */
	public void setOrderSpecialCase(String string) {
		orderSpecialCase = string;
	}

	/**
	 * @return
	 */
	public ArrayList getRequirementAttachment() {
		return requirementAttachment;
	}

	/**
	 * @param list
	 */
	public void setRequirementAttachment(ArrayList list) {
		requirementAttachment = list;
	}

	public String getCompCode() {
		
		LOGGER.info("getCompCode start");
		if(compCode == null || compCode.length() <= 0){
			String companyCode = "";
			try {
				ResourceBundle rbWMSSP = ResourceBundle.getBundle("dbconfig");
				companyCode = rbWMSSP.getString("IUM_CSS_VERSION");
				if(companyCode!=null ){
					if("CP".equalsIgnoreCase(companyCode)){
						this.setCompCode("SLOCPI");
					}else if("GF".equalsIgnoreCase(companyCode)){
						this.setCompCode("SLGFI");
					}
				}
			} catch (Exception e){
				LOGGER.error(CodeHelper.getStackTrace(e));
			}
			LOGGER.debug("companyCode-->" +compCode);
		}
		LOGGER.info("getCompCode end");
		return compCode;
	}

	public void setCompCode(String compCode) {
		this.compCode = compCode;
	}
}
