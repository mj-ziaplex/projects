/*
 * Created on Dec 17, 2003
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.interfaces.notification;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.workflow.NotificationMessages;

/**
 * @author daguila
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class Notifier {
	private static final Logger LOGGER = LoggerFactory.getLogger(Notifier.class);
	
	public void sendMail(NotificationMessage msg) throws IUMNotificationException {
		
		LOGGER.info("sendMail start");
		try {
			SMTPConnector sc = new SMTPConnector();
			sc.sendMail(msg);
		} catch (Exception e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMNotificationException(e.getMessage());
		}
		LOGGER.info("sendMail end");
	}
	
	public void sendMessages(final NotificationMessages notificationMsgs) throws IUMNotificationException {
		
		LOGGER.info("sendMessages start");
		try {
			SMTPConnector sc = new SMTPConnector();
			
			if (notificationMsgs.getMailMessage() != null) {			
				sc.sendMail(notificationMsgs.getMailMessage());
			}
		} catch (Exception e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMNotificationException(e.getMessage());
		}
		
		LOGGER.info("sendMessages end");
	}
	
	
}
