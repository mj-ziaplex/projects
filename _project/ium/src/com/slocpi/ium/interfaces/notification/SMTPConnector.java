/*
 * Created on Dec 17, 2003
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.interfaces.notification;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.Properties;
import java.util.ResourceBundle;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.slocpi.ium.util.IUMConstants;
/**
 * @author daguila
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class SMTPConnector {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(SMTPConnector.class);
	private Properties props;
	
	public SMTPConnector() {
		
		LOGGER.info("constructor start");
		
		props = new Properties();
		ResourceBundle rb = ResourceBundle.getBundle(IUMConstants.IUM_CONFIG);
		String host = rb.getString(IUMConstants.IUM_SMTP_HOST);		
		String propKey = rb.getString(IUMConstants.IUM_SMTP_KEY);	
		props.put(propKey,host);
		String port = rb.getString(IUMConstants.IUM_SMTP_PORT);		
		String propPortKey = rb.getString(IUMConstants.IUM_SMTP_PORT_KEY);	
		props.put(propPortKey,port);
		
		LOGGER.info("constructor end");
	}
	
	public void sendMail(NotificationMessage msg) throws AddressException, MessagingException {
		
		LOGGER.info("sendMail start");
		Session s = Session.getInstance(props,null);
		InternetAddress from = new InternetAddress(msg.getSender());
		ArrayList addressesTo = msg.getRecipients();
		MimeMessage mime = new MimeMessage(s);
		mime.setFrom(from);
		
		ArrayList recipients = msg.getRecipients();
		if (recipients != null && recipients.size() > 0) {
			for (int i=0; i < addressesTo.size(); i++) {
				String address = (String)addressesTo.get(i).toString();
				if (address != null && !"".equals(address)) {
					InternetAddress to = new InternetAddress(address);
					mime.addRecipient(Message.RecipientType.TO,to);
				}
			}
			
		}
		
		String subject = msg.getSubject();
		
		if (subject != null && !"".equals(subject)) {		
			mime.setSubject(msg.getSubject());
		}
				
		 MimeBodyPart messageBodyPart = 
		   new MimeBodyPart();
		 
		 messageBodyPart.setText(msg.getContent());
		 Multipart multipart = new MimeMultipart();		
		 multipart.addBodyPart(messageBodyPart);;
		 
		 //special case for Ordered Requirements
		 if (msg.getOrderSpecialCase() != null && msg.getOrderSpecialCase().equals(IUMConstants.ORDER_REQUIREMENT)){
		 	ArrayList listOfAttachments = msg.getRequirementAttachment();
		 	
		 	for (int i=0; i<listOfAttachments.size(); i++){ 
		 		Attachment att = (Attachment) listOfAttachments.get(i);
				ByteArrayInputStream b = new ByteArrayInputStream(att.getAttachment().toByteArray());			
				StreamBasedMimeBodyPart sbmbp = new StreamBasedMimeBodyPart( new ByteArrayInputStream(att.getAttachment().toByteArray()), "application/pdf", att.getAttachmentName());		 			 
				multipart.addBodyPart(sbmbp);
		 	}
		 }

		// Part two is attachment
		 if (msg.getAttachment() != null) {		 		 	
			ByteArrayInputStream b = new ByteArrayInputStream(msg.getAttachment().getAttachment().toByteArray());			
			StreamBasedMimeBodyPart sbmbp = new StreamBasedMimeBodyPart( new ByteArrayInputStream(msg.getAttachment().getAttachment().toByteArray()), "application/pdf", msg.getAttachment().getAttachmentName());		 			 
			multipart.addBodyPart(sbmbp);
		 }	
		 // Put parts in message
		 mime.setContent(multipart);		 		
		Transport.send(mime,mime.getAllRecipients());	
		
		LOGGER.info("sendMail end");
	}
		
}


