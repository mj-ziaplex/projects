/*
 * Created on Jan 7, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.interfaces.notification;

/**
 * @author daguila
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class IUMNotificationException extends Exception {
	
	public IUMNotificationException(String msg) {
		super(msg);
	}
	
	public IUMNotificationException(Exception e) {
		super(e.getMessage());
	}

}
