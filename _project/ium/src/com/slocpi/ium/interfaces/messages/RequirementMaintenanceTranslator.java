package com.slocpi.ium.interfaces.messages;

import java.sql.SQLException;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jdom.Document;
import org.jdom.Element;
import com.slocpi.ium.data.IUMMessageData;
import com.slocpi.ium.data.PolicyRequirementsData;
import com.slocpi.ium.data.StatusData;
import com.slocpi.ium.interfaces.IUMInterfaceException;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.DateHelper;
import com.slocpi.ium.util.IUMConstants;
import com.slocpi.ium.util.ValueConverter;

/**
 * This class converts messages(Requirement Maintenance Information) to and from MQ - Abacus.
 * 
 * @author daguila
 *
 */
public class RequirementMaintenanceTranslator {

	private static final Logger LOGGER = LoggerFactory.getLogger(RequirementMaintenanceTranslator.class);
	private String TRANSACTION_TYPE_CREATE = "C";
	private String TRANSACTION_TYPE_MAINTAIN = "M";
	private String REQUIREMENT_POLICY = "P";
	private String REQUIREMENT_CLIENT = "C";
	
	/**
	 * 
	 * @param abacusUserId 
	 * @param msgId
	 * @param prd
	 * @return a StringBuffer containing the message to be sent to the queue
	 * @throws IUMInterfaceException
	 */
	public StringBuffer generateCreateRequirementMessage(String abacusUserId, String msgId,PolicyRequirementsData prd) throws IUMInterfaceException {
		return generateMaintainRequirementMessage(abacusUserId,msgId,prd,true);
	}
	
	/**
	 * 
	 * @param abacusUserId
	 * @param msgId
	 * @param prd
	 * @return a StringBuffer containing the message to be sent to the queue
	 * @throws IUMInterfaceException
	 */
	public StringBuffer generateOrderRequirementMessage(String abacusUserId, String msgId,PolicyRequirementsData prd) throws IUMInterfaceException {
		return generateMaintainRequirementMessage(abacusUserId,msgId,prd,false);
	}
	
	/**
	 * 
	 * @param abacusUserId
	 * @param msgId
	 * @param prd
	 * @return a StringBuffer containing the message to be sent to the queue
	 * @throws IUMInterfaceException
	 */
	public StringBuffer generateReceiveRequirementMessage(String abacusUserId, String msgId,PolicyRequirementsData prd) throws IUMInterfaceException {
		return generateMaintainRequirementMessage(abacusUserId,msgId,prd,false);
	}

	/**
	 * 
	 * @param abacusUserId
	 * @param msgId
	 * @param prd
	 * @return a StringBuffer containing the message to be sent to the queue
	 * @throws IUMInterfaceException
	 */
	public StringBuffer generateAcceptRequirementMessage(String abacusUserId, String msgId,PolicyRequirementsData prd) throws IUMInterfaceException {
		return generateMaintainRequirementMessage(abacusUserId,msgId,prd,false);
	}

	/**
	 * 
	 * @param abacusUserId
	 * @param msgId
	 * @param prd
	 * @return a StringBuffer containing the message to be sent to the queue
	 * @throws IUMInterfaceException
	 */
	public StringBuffer generateUpdateRequirementMessage(String abacusUserId, String msgId,PolicyRequirementsData prd) throws IUMInterfaceException {
		return generateMaintainRequirementMessage(abacusUserId,msgId,prd,false);
	}
		
	/**
	 * This method converts the Requirement Maintenance Message into a IUM Message Data.
	 * Format of the message should be:
	 * 
	 * <?xml version="1.0" encoding="UTF-8" ?> 
	 * <ROOT> 
	 * <MSG_ID> </MSG_ID>
	 * <CONF_CD> </CONF_CD>
	 * <MSG1> </MSG1>
	 * <MSG2> </MSG2>   
	 * <MSG3> </MSG3>
	 * </ROOT>  
	 * @param message
	 * @return IUMMessageData created using the information contained in the xml
	 * @throws IUMABACUSParseException
	 */	
	public IUMMessageData translateRequirementMaintenanceMessage(StringBuffer message) throws IUMInterfaceException {
		
		LOGGER.info("translateRequirementMaintenanceMessage start");
		IUMMessageData asd = new IUMMessageData();
		try {
			Document doc = IUMMessageUtility.getDocument(message);
			Element root = doc.getRootElement();
			List nodes = root.getChildren();
			String refNum = "";
			
			StringBuffer xmlRecord = new StringBuffer();
			xmlRecord.append("<?xml version=\"1.0\" encoding=\"UTF-8\" ?><ROOT>");
			xmlRecord.append(IUMMessageUtility.writeXML(root.getChildren()));
			xmlRecord.append("</ROOT>");
			asd.setXmlRecord(xmlRecord.toString());
			
			for (int i=0; i < nodes.size(); i++) {
				Element e = (Element)nodes.get(i);
				String attribute = e.getName();
				String value = e.getText();
				if (attribute == null) {
					attribute = "";
				}
				if (value == null) {
					value = "";
				} else {
					value = value.trim();
				}
				if (attribute.equalsIgnoreCase("MSG_ID")) {
				} else if (attribute.equalsIgnoreCase("CONF_CD")) {
					asd.setConfirmationCode(value);
				} else if (attribute.equalsIgnoreCase("MSG1")) {
					asd.setMessageLine1(value);
				} else if (attribute.equalsIgnoreCase("MSG2")) {
					asd.setMessageLine2(value);
				} else if (attribute.equalsIgnoreCase("MSG3")) {
					asd.setMessageLine3(value);
				} else if (attribute.equalsIgnoreCase("POL_OR_CLI_CD")) {
					asd.setPolicyClientCode(value);
				} else if (attribute.equalsIgnoreCase("POL_OR_CLI_ID")) {
					asd.setPolicyClientId(value);
				} else if (attribute.equalsIgnoreCase("REQIR_ID")) {
					asd.setRequirementId(value);
				} else if (attribute.equalsIgnoreCase("SEQ_NUM")) {
					try{
						asd.setSequenceNumber(IUMMessageUtility.getInteger(value));
					}catch(IUMInterfaceException iie){
						asd.setSequenceNumber(-1);
					}
				} else if (attribute.equalsIgnoreCase("CREAT_DT")) {
					asd.setCreationDate(DateHelper.parse(value,IUMMessageUtility.DEFAULT_DATE_FORMAT));
				} else if (attribute.equalsIgnoreCase("STAT_DT")) {
					asd.setStatusDate(DateHelper.parse(value,IUMMessageUtility.DEFAULT_DATE_FORMAT));
				} else {
					LOGGER.warn("Attribute [" + attribute + "] not defined for Auto Settle Info.");
					throw new IUMInterfaceException("Attribute [" + attribute + "] not defined for Auto Settle Info.");
				}
			}			
		} catch (Exception e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMInterfaceException(e.getMessage());
		}
		LOGGER.info("translateRequirementMaintenanceMessage end");
		return asd;		
	}
	
	private StringBuffer generateMaintainRequirementMessage(String abacusUserId, String msgId,PolicyRequirementsData prd, boolean create) throws IUMInterfaceException {
		
		LOGGER.info("generateMaintainRequirementMessage start");
		StringBuffer sb = new StringBuffer();
		CodeHelper ch = new CodeHelper();

		if (prd != null) {
			try {
			
				sb.append(IUMMessageUtility.fillSpace(abacusUserId,8));
				sb.append(IUMMessageUtility.fillSpace(msgId,10));
				if (create) {
					sb.append(TRANSACTION_TYPE_CREATE);
				} else { 
					if(prd.getSequenceNumber()==0){
						sb.append(TRANSACTION_TYPE_CREATE);
					}else{
						sb.append(TRANSACTION_TYPE_MAINTAIN);
					}
				}
				
				
				sb.append(IUMMessageUtility.fillSpace(prd.getLevel(),1));
				if (prd.getLevel().equals(REQUIREMENT_CLIENT)) {
					sb.append(IUMMessageUtility.fillSpace(prd.getClientId(),10));
				} else {
					sb.append(IUMMessageUtility.fillSpace(prd.getReferenceNumber(),10));
				}
				sb.append(IUMMessageUtility.fillSpace(prd.getRequirementCode(),5));
				sb.append(IUMMessageUtility.fillSpace(prd.getSequenceNumber(),3));
				
				// TODO: put status converter here
				StatusData sd = null;
				if(prd.getStatus()!=null){
					sd = ch.translateStatusData(IUMConstants.LOB_INDIVIDUAL_LIFE, prd.getStatus().getStatusId());
				}else{
					throw new IUMInterfaceException("Status is required");
				}
				sb.append(IUMMessageUtility.fillSpace(sd!=null?sd.getStatusDesc():"", 3)); 
				sb.append(IUMMessageUtility.fillSpace(prd.getUpdatedBy(),8));
				sb.append(IUMMessageUtility.fillSpace((DateHelper.format(prd.getUpdateDate(),"ddMMMyyyy")).toUpperCase(),9));
				sb.append(IUMMessageUtility.fillSpace(prd.getDesignation(),10));
				sb.append(IUMMessageUtility.fillSpace((DateHelper.format(prd.getTestDate(),"ddMMMyyyy")).toUpperCase(),9));
				sb.append(IUMMessageUtility.fillSpace(prd.getTestResultCode(),4));
				
				if(prd.getFollowUpNumber()==0){
					sb.append(IUMMessageUtility.fillSpace("",1));
					sb.append(IUMMessageUtility.fillSpace("",9));
				}else{
					sb.append(IUMMessageUtility.fillSpace(prd.getFollowUpNumber(),1));
					sb.append(IUMMessageUtility.fillSpace((DateHelper.format(prd.getFollowUpDate(),"ddMMMyyyy")).toUpperCase(),9));
				}
				
				sb.append(IUMMessageUtility.fillSpace(prd.isPaidInd(),1));
				sb.append(IUMMessageUtility.fillSpace((DateHelper.format(prd.getValidityDate(),"ddMMMyyyy")).toUpperCase(),9));
				String nto = ValueConverter.booleanToString(prd.getNewTestOnly());
				sb.append(nto);
				
				
				sb.append(IUMMessageUtility.fillSpace(prd.getAutoOrderInd(),1));
				sb.append(IUMMessageUtility.fillSpace(prd.getFldCommentInd(),1));
				sb.append(IUMMessageUtility.fillSpace(prd.getComments(),50));
				
				sb.append(ValueConverter.booleanToString(prd.isResolveInd()));
				
			} catch (SQLException sqle) {
				LOGGER.error(CodeHelper.getStackTrace(sqle));
				throw new IUMInterfaceException(sqle.getMessage());			
			}
		} else {
			LOGGER.warn("Cannot generate Requirement Message. Missing Policy Requirements Data");
			throw new IUMInterfaceException("Cannot generate Requirement Message. Missing Policy Requirements Data");
		}
		LOGGER.info("generateMaintainRequirementMessage end");
		return sb;
		
	}

}
