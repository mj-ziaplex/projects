package com.slocpi.ium.interfaces.messages;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jdom.Document;
import org.jdom.Element;
import com.slocpi.ium.data.PolicyRequirementsData;
import com.slocpi.ium.interfaces.IUMInterfaceException;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.DateHelper;
import com.slocpi.ium.util.IUMConstants;
import com.slocpi.ium.util.ValueConverter;

/**
 * This class is responsible for converting messages(Retrieve Requirement Information) to and from the MQ - Abacus.
 * 
 * @author daguila
 *
 */
public class PolicyRequirementInquiryTranslator {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(PolicyRequirementInquiryTranslator.class);
	public static final String RESPONSE_SUCCESS = "00";
	public String TRANSACTION_RETRIEVE = "R";
	public String TRANSACTION_DELETE = "D";
	private String CODE_POLICY = "P";
	private String CODE_CLIENT = "C";
	private boolean isEmpty;
	
	/**
	 * This method generates the retrieval message for the policy requirements.
	 * 
	 * @param abacuseUserId
	 * @param msgId
	 * @param refNum
	 * @param transactionType
	 * @return a StringBuffer containing the message to be sent to the queue
	 * @throws IUMInterfaceException
	 */
	public StringBuffer generatePolicyRequirementInquiryMessage(String abacuseUserId, String msgId, String refNum, String transactionType) throws IUMInterfaceException {
		
		LOGGER.info("generatePolicyRequirementInquiryMessage start");
		StringBuffer sb = new StringBuffer();
		
		if ((abacuseUserId == null) || (abacuseUserId.trim().equals(""))) {
			throw new IUMInterfaceException("Missing abacus user id");
		}
		
		if ((msgId == null) || (msgId.trim().equals(""))) {
			throw new IUMInterfaceException("Missing Message id");
		}
		
		if ((refNum == null) || (refNum.trim().equals(""))) {
			throw new IUMInterfaceException("Missing Policy Number");
		}
		
		if ((transactionType == null) || (transactionType.trim().equals("")) ||
		    ((!transactionType.equals(TRANSACTION_DELETE)) && (!transactionType.equals(TRANSACTION_RETRIEVE)))) {
			throw new IUMInterfaceException("Invalid transaction Code");
		}
		
		sb.append(IUMMessageUtility.fillSpace(abacuseUserId,8));
		sb.append(IUMMessageUtility.fillSpace(msgId,10));
		sb.append(IUMMessageUtility.fillSpace(refNum,10));
		sb.append(IUMMessageUtility.fillSpace(transactionType,1));
		LOGGER.info("generatePolicyRequirementInquiryMessage end");
		return sb;
	}
	
	/**
	 * This method translates the replies from MQ into Policy requirement data.
	 * 
	 * @param message
	 * @return ArrayList of policy requirement data
	 * @throws IUMInterfaceException
	 */
	public ArrayList translatePolicyRequirementInquiryMessage(StringBuffer message) throws IUMInterfaceException {
		
		LOGGER.info("translatePolicyRequirementInquiryMessage start");
		ArrayList list = new ArrayList();
		try {
			Document doc = IUMMessageUtility.getDocument(message);
			Element root = doc.getRootElement();
			List nodes = root.getChildren();
			String refNum = "";
			String msgId = "";
			String confCd = "";
			
			for (int i=0; i < nodes.size(); i++) {
				Element e = (Element)nodes.get(i);
				String attribute = e.getName();
				String value = e.getText();
				if (attribute == null) {
					attribute = "";
				}
				if (value == null) {
					value = "";
				} else {
					value = value.trim();
				}
				
				if (attribute.equalsIgnoreCase("MSG_ID")) {
					msgId = e.getText();
				} else if (attribute.equalsIgnoreCase("CONF_CD")) {
					confCd = e.getText();
					if (!value.equals(RESPONSE_SUCCESS)) {
						break;
					}
				} else if (attribute.equalsIgnoreCase("SUBJ_POL_ID")) {
					refNum = value;
				} else if (attribute.equalsIgnoreCase("REQT_DATA")) {
					PolicyRequirementsData polData = parsePolicyRequirement(e.getChildren(),refNum);
					StringBuffer xmlRecord = new StringBuffer();
					xmlRecord.append("<?xml version=\"1.0\" encoding=\"UTF-8\" ?><ROOT>");
					xmlRecord.append("<MSG_ID>" + msgId + "</MSG_ID>");
					xmlRecord.append("<CONF_CD>" + confCd + "</CONF_CD>");
					xmlRecord.append("<SUBJ_POL_ID>" + refNum + "</SUBJ_POL_ID>");
					xmlRecord.append("<REQT_DATA>");
					xmlRecord.append(IUMMessageUtility.writeXML(e.getChildren()));
					xmlRecord.append("</REQT_DATA>");
					xmlRecord.append("</ROOT>");
					polData.setXmlRecord(xmlRecord.toString());
					if(isEmpty){
						break;
					}
					list.add(polData);
				} else {
					LOGGER.warn("Attribute [" + attribute + "] not defined for Policy Requirement Inquiry Info.");
					throw new IUMInterfaceException("Attribute [" + attribute + "] not defined for Policy Requirement Inquiry Info.");
				}
			}			
		} catch (Exception e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMInterfaceException(e.getMessage());
		}
		LOGGER.info("translatePolicyRequirementInquiryMessage end");
		return list;
	}
	
	private PolicyRequirementsData parsePolicyRequirement(List children, String refNum) throws IUMInterfaceException {
		
		LOGGER.info("parsePolicyRequirement start");
		PolicyRequirementsData prd = new PolicyRequirementsData();
		String policyOrClientCode = "";
		CodeHelper ch = new CodeHelper();
		isEmpty = true;
		
		for (int i=0; i < children.size(); i++) {
			Element child = (Element)children.get(i);
			String attribute = child.getName();
			String value = child.getText();
			if (attribute == null) {
				attribute = "";
			}
			if (value == null || value.trim().equals("")) {
				value = "";
			} else {
				value = value.trim();
				
			}
		
			try {

				if (attribute.equalsIgnoreCase("POL_OR_CLI_CD")) {
					policyOrClientCode = value;
					prd.setLevel(value);
				} else if (attribute.equalsIgnoreCase("POL_OR_CLI_ID")) {
					if(!value.equals("")){
						isEmpty = false;
					}
					if (policyOrClientCode.equals(CODE_POLICY)) {
						prd.setReferenceNumber(value);
					} else {
						prd.setClientId(value);
					}
				} else if (attribute.equalsIgnoreCase("CLI_TYPE")) {
					prd.setClientType(value);
				} else if (attribute.equalsIgnoreCase("REQIR_ID")) {
					prd.setRequirementCode(value); 
				} else if (attribute.equalsIgnoreCase("SEQ_NUM")) {
					try{
						prd.setSequenceNumber(IUMMessageUtility.getLong(value));
					}catch (Exception e){
						prd.setSequenceNumber(-1);
					}
				} else if (attribute.equalsIgnoreCase("STAT_CD")) {
					prd.setStatus(ch.convertStatusData(IUMConstants.LOB_INDIVIDUAL_LIFE,value,"NM"));
				} else if (attribute.equalsIgnoreCase("STAT_DT")) {
					try{
						prd.setStatusDate(DateHelper.parse(value,IUMMessageUtility.DEFAULT_DATE_FORMAT));
					}catch(Exception e){
						// do nothing
					}
				} else if (attribute.equalsIgnoreCase("FOLWUP_DT")) {
					try{
						prd.setFollowUpDate(DateHelper.parse(value,IUMMessageUtility.DEFAULT_DATE_FORMAT));
					}catch(Exception e){
						// do nothing
					}
				} else if (attribute.equalsIgnoreCase("REQT_DESC")) {
					prd.setReqDesc(value);
				} else if (attribute.equalsIgnoreCase("CREAT_DT")) {
					try{
						prd.setCreateDate(DateHelper.parse(value,IUMMessageUtility.DEFAULT_DATE_FORMAT));
					} catch (Exception e){
						// do nothing
					}
				} else if (attribute.equalsIgnoreCase("UPDT_USER_ID")) {
					prd.setUpdatedBy(value);
				} else if (attribute.equalsIgnoreCase("UPDT_DT")) {
					try{
						prd.setUpdateDate(DateHelper.parse(value,IUMMessageUtility.DEFAULT_DATE_FORMAT));
					}catch (Exception e){
						// do nothing
					}
				} else if (attribute.equalsIgnoreCase("DESGNT_ID")) {
					prd.setDesignation(value);
				} else if (attribute.equalsIgnoreCase("TST_DT")) {
					try{
						prd.setTestDate(DateHelper.parse(value,IUMMessageUtility.DEFAULT_DATE_FORMAT));
					} catch (Exception e){
						// do nothing
					}
				} else if (attribute.equalsIgnoreCase("TST_RSLT_CD")) {
					prd.setTestResultCode(value);
				} else if (attribute.equalsIgnoreCase("TST_RSLT_DESC")) {
					prd.setTestResultDesc(value);
				} else if (attribute.equalsIgnoreCase("RESOLV_IND")) {
					prd.setResolveInd(IUMMessageUtility.isTrue(value));
				} else if (attribute.equalsIgnoreCase("FOLWUP_NUM")) {
					prd.setFollowUpNumber(1); 
				} else if (attribute.equalsIgnoreCase("VALID_DT")) {
					try {
						prd.setValidityDate(DateHelper.parse(value,IUMMessageUtility.DEFAULT_DATE_FORMAT));
					} catch (Exception e){
						// do nothing
					}
				} else if (attribute.equalsIgnoreCase("PD_IND")) {
					prd.setPaidInd(IUMMessageUtility.isTrue(value));
				} else if (attribute.equalsIgnoreCase("NEW_RSLT_IND")) {
					prd.setNewTestOnly(ValueConverter.stringToBoolean(value));
				} else if (attribute.equalsIgnoreCase("ORDR_DT")) {
					try {
						prd.setOrderDate(DateHelper.parse(value,IUMMessageUtility.DEFAULT_DATE_FORMAT));	
					} catch (Exception e){
						// do nothing 
					}
				} else if (attribute.equalsIgnoreCase("SUGST_IND")) {
					prd.setCCASuggestInd(ValueConverter.stringToBoolean(value));
				} else if (attribute.equalsIgnoreCase("RECV_DT")) {
					try {
						prd.setReceiveDate(DateHelper.parse(value,IUMMessageUtility.DEFAULT_DATE_FORMAT));
					} catch (Exception e){
						// do nothing
					}
				} else if (attribute.equalsIgnoreCase("AUTO_ORDR_CD")) {
					prd.setAutoOrderInd(ValueConverter.stringToBoolean(value));
				} else if (attribute.equalsIgnoreCase("FLD_COMUN_MSG_IND")) {
					prd.setFldCommentInd(ValueConverter.stringToBoolean(value));
				} else if (attribute.equalsIgnoreCase("UW_COMNT_TXT")) {
					prd.setComments(value);
				} else {
					LOGGER.warn("Attribute [" + attribute + "] not defined for Policy Requirement.");
					throw new IUMInterfaceException("Attribute [" + attribute + "] not defined for Policy Requirement.");
				}
			} catch (SQLException sqle) {
				LOGGER.error(CodeHelper.getStackTrace(sqle));
				throw new IUMInterfaceException(sqle.getMessage());
			}
		}
		
		prd.setReferenceNumber(refNum);
		LOGGER.info("parsePolicyRequirement end");
		return prd;
	}	
}
