package com.slocpi.ium.interfaces.messages;

import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jdom.Document;
import org.jdom.Element;
import com.slocpi.ium.data.AssessmentRequestData;
import com.slocpi.ium.data.CDSMortalityRatingData;
import com.slocpi.ium.data.CDSSummaryPolicyCoverageData;
import com.slocpi.ium.data.ClientData;
import com.slocpi.ium.data.ClientDataSheetData;
import com.slocpi.ium.interfaces.IUMInterfaceException;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.DateHelper;

/**
 * This class is responsible for translating messages(Client Related Information) to and from MQ - Abacus Interface.
 * 
 * @author daguila
 *
 */
public class ClientRelatedInfoTranslator {

	private static final Logger LOGGER = LoggerFactory.getLogger(ClientRelatedInfoTranslator.class);
	public static final String TRANSACTION_TYPE_RETRIEVE = "R";
	public static final String TRANSACTION_TYPE_DELETE   = "D";
	private String RESPONSE_SUCCESS			= "00";
	
	/**
	 * This method will generate the appropriate message to be sent to 
	 * the MQ - Abacus interface.
	 * 
	 * @param abacusUserId
	 * @param msgId
	 * @param refNum
	 * @param transactionType
	 * @return a StringBuffer containing the message to be sent to the queue
	 * @throws IUMInterfaceException
	 */
	public StringBuffer generateClientRelatedInfoMessage(String abacusUserId, String msgId, String refNum, String transactionType) throws IUMInterfaceException {
		
		LOGGER.info("generateClientRelatedInfoMessage start");
		StringBuffer result = new StringBuffer();
		String userId = "";
		String msg = "";
		
		if ((abacusUserId == null) || (abacusUserId.trim().equals(""))) {
			LOGGER.warn("Missing Abacus User Id");
			throw new IUMInterfaceException("Missing Abacus User Id");
		} else {
			userId = abacusUserId;
		}
		
		if ((msgId == null) || (msgId.trim().equals(""))){
			LOGGER.warn("Missing Message Id");
			throw new IUMInterfaceException("Missing Message Id");
		} else {
			msg = msgId;
		}

		if ((transactionType == null)||((!transactionType.equals(TRANSACTION_TYPE_DELETE)) && (!transactionType.equals(TRANSACTION_TYPE_RETRIEVE)))) {
			LOGGER.warn("Invalid transaction code for Client Related Info message");
			throw new IUMInterfaceException("Invalid transaction code for Client Related Info message.");
		}
		
		result.append(IUMMessageUtility.fillSpace(abacusUserId,8));
		result.append(IUMMessageUtility.fillSpace(msgId,10));
		result.append(IUMMessageUtility.fillSpace(refNum,10));
		result.append(IUMMessageUtility.fillSpace(transactionType,1));
		
		LOGGER.info("generateClientRelatedInfoMessage end");
		return result;			
	}
	
	
	public AssessmentRequestData translateClientRelatedInfoMessage(StringBuffer message) throws IUMInterfaceException {
		
		LOGGER.info("translateClientRelatedInfoMessage start");
		AssessmentRequestData ard = new AssessmentRequestData();

		try {
			Document doc = IUMMessageUtility.getDocument(message);
			Element root = doc.getRootElement();
			List nodes = root.getChildren();
			String msgId = null;

			for (int i=0; i < nodes.size(); i++) {
				Element e = (Element)nodes.get(i);
				String attribute = e.getName();
				String value = e.getText();

				if (attribute == null) {
					attribute = "";
				}
				
				if (attribute.equalsIgnoreCase("MSG_ID")) {
					msgId = e.getText();
				} else if (attribute.equalsIgnoreCase("CONF_CD")) {
					String recFound = e.getText();
					if ((recFound == null) || (!recFound.equals(RESPONSE_SUCCESS))) {
						break;
					}
				} else if (attribute.equalsIgnoreCase("CDS_DATA")) {
					parseCDS(e.getChildren(),ard);
				} else {
					LOGGER.warn("IUMInterfaceException Attribute [" + attribute + "] is not defined in the Kick Out Message.");
					throw new IUMInterfaceException("Attribute [" + attribute + "] is not defined in the Kick Out Message.");
				}
				
			}
			
		} catch (Exception e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMInterfaceException(e);
		}


		// TODO: Dave where to set the sequence number and cds data sheet
		ard.setXmlRecord(message.toString());
		LOGGER.info("translateClientRelatedInfoMessage end");
		return ard;
	}
	
	
	
	private void parseCDS(List list,AssessmentRequestData ard) throws IUMInterfaceException {
		
		LOGGER.info("parseCDS start");
		ClientDataSheetData cds = new ClientDataSheetData();
		CDSMortalityRatingData insrMortRating = new CDSMortalityRatingData();
		CDSMortalityRatingData ownMortRating = new CDSMortalityRatingData();
		String refNum = "";
		String statCode = "";		
		
		for (int i=0; i < list.size(); i++) {
			Element e = (Element)list.get(i);
			String attribute = e.getName();
			String value = e.getText();

			if (attribute == null) {
				attribute = "";
			}

			if (value == null) {
				value = "";
			} else {
				value = value.trim();
			}
	
			if (attribute.equalsIgnoreCase("SUBJ_POL_ID")) {
				refNum = value;
			} else if (attribute.equalsIgnoreCase("STAT_CD")) {
				statCode = value;
			} else if (attribute.equalsIgnoreCase("INSR_CLI_INFO")) {
				ClientData cd = parseInsuredClientInfo(e.getChildren());
				ard.setInsured(cd);
			} else if (attribute.equalsIgnoreCase("OWN_CLI_INFO")) {
				ClientData cd = parseOwnerClientInfo(e.getChildren());
				ard.setOwner(cd);
			} else if (attribute.equalsIgnoreCase("TOT_EXTG_INFO")) {
				cds.setSummaryOfPolicies(parseSummaryCoverage(e.getChildren()));					
			} else if (attribute.equalsIgnoreCase("MORTALITY_INFO")) {
							
				List children = e.getChildren();
				for (int j=0; j<children.size(); j++) {
					Element child = (Element)children.get(j);
					String itemAttribute = child.getName();
					String itemValue = child.getText();
					
					if (itemAttribute == null) {
						itemAttribute = "";
					}
					if (itemValue == null) {
						itemValue = "";
					} else {
						itemValue = itemValue.trim();
					}
								
					if (itemAttribute.equalsIgnoreCase("INSR_CLI_MORT")) {
						insrMortRating = parseMortalityRating(child.getChildren(),false,refNum);
					} else if (itemAttribute.equalsIgnoreCase("OWN_CLI_MORT")) {
						ownMortRating = parseMortalityRating(child.getChildren(),true,refNum);
					} else {
						LOGGER.warn("IUMInterfaceException Attribute [" + attribute + "]is not defined in the Mortality Info.");
						throw new IUMInterfaceException("Attribute [" + attribute + "] is not defined in the Mortality Info.");
					}
				} 
			} else {
				LOGGER.warn("Attribute [" + attribute + "] is not defined in the CDS Data Info.");
				throw new IUMInterfaceException("Attribute [" + attribute + "] is not defined in the CDS Data Info.");
			}
			
		}
		ard.setReferenceNumber(refNum);
		ard.setClientDataSheet(cds);
		ard.setInsuredMortalityRating(insrMortRating);
		ard.setOwnerMortalityRating(ownMortRating);
		LOGGER.info("parseCDS end");
	}

	private ClientData parseInsuredClientInfo(List children) throws IUMInterfaceException {
		return parseClientInfo(children,false);
	}
	
	private ClientData parseOwnerClientInfo(List children) throws IUMInterfaceException {
		return parseClientInfo(children,true);
	}
		
	private ClientData parseClientInfo(List children, boolean owner) throws IUMInterfaceException {
		
		LOGGER.info("parseClientInfo start");
		ClientData cd = new ClientData();
		String prefix = "INSR";
		if (owner) {
			prefix = "OWN";
		}
		for (int i=0; i <children.size(); i++) {
			Element child = (Element)children.get(i);
			String attribute = child.getName();
			String value = child.getText();
			
			if (attribute == null) {
				attribute = "";
			}
			
			if (value == null) {
				value = "";
			} else {
				value = value.trim();
			}
			
			if (attribute.equalsIgnoreCase(prefix + "_CLI_ID")) {
				cd.setClientId(value);
			} else if (attribute.equalsIgnoreCase(prefix + "_SUR_NM")) {
				cd.setLastName(value);
			} else if (attribute.equalsIgnoreCase(prefix + "_GIV_NM")) {
				cd.setGivenName(value);
			} else if (attribute.equalsIgnoreCase(prefix + "_MID_NM")) {
				cd.setMiddleName(value);
			} else if (attribute.equalsIgnoreCase(prefix + "_OTH_SUR_NM")) {
				cd.setOtherLastName(value);
			} else if (attribute.equalsIgnoreCase(prefix + "_OTH_GIV_NM")) {
				cd.setOtherGivenName(value);
			} else if (attribute.equalsIgnoreCase(prefix + "_OTH_MID_NM")) {
				cd.setOtherMiddleName(value);
			} else if (attribute.equalsIgnoreCase(prefix + "_TITL_TXT")) {
				cd.setTitle(value);
			} else if (attribute.equalsIgnoreCase(prefix + "_SFX_NM")) {
				cd.setSuffix(value);				
			} else if (attribute.equalsIgnoreCase(prefix + "_BTH_DT")) {
				try{
					cd.setBirthDate(DateHelper.parse(value,IUMMessageUtility.DEFAULT_DATE_FORMAT));
				}catch (Exception e){
					// do nothing 
				}
			} else if (attribute.equalsIgnoreCase(prefix + "_BTH_LOC")) {
				cd.setBirthLocation(value);
			} else if (attribute.equalsIgnoreCase(prefix + "_SEX_CD")) {
				cd.setSex(value);
			} else if (attribute.equalsIgnoreCase(prefix + "_CLI_AGE")) {
				try {
					cd.setAge(IUMMessageUtility.getInteger(value));
				}catch (Exception e){
					cd.setAge(-1);
				}
			} else if (attribute.equalsIgnoreCase(prefix + "_SMKR_CD")) {
				cd.setSmoker(IUMMessageUtility.isTrue(value));
			} else {
				LOGGER.warn("IUMInterfaceException Attribute [" + attribute + "]for" + prefix + "CLIENT INFO is not defined.");
				throw new IUMInterfaceException("Attribute [" + attribute + "] for" + prefix + "CLIENT INFO is not defined.");
			}
		}
		LOGGER.info("parseClientInfo end");
		return cd;
	}

	private CDSSummaryPolicyCoverageData parseSummaryCoverage(List children) throws IUMInterfaceException {
		
		LOGGER.info("parseSummaryCoverage start");
		CDSSummaryPolicyCoverageData spcd = new CDSSummaryPolicyCoverageData();

		for (int i=0; i <children.size(); i++) {
			Element child = (Element)children.get(i);
			String attribute = child.getName();
			String value = child.getText();
			if (attribute == null) {
				attribute = "";
			}
			
			if (value == null) {
				value = "";
			} else {
				value = value.trim();
			}
			
			if (attribute.equalsIgnoreCase("APP_BR_AMT")) {
				try{
					spcd.setAppliedForBr(IUMMessageUtility.getDouble(value));
				}catch(Exception e){
					spcd.setAppliedForBr(-1);
				}
			} else if (attribute.equalsIgnoreCase("APP_AD_AMT")) {
				try{
					spcd.setAppliedForAd(IUMMessageUtility.getDouble(value));
				}catch(Exception e){
					spcd.setAppliedForAd(-1);
				}
			} else if (attribute.equalsIgnoreCase("PEND_BR_AMT")) {
				try{
					spcd.setPendingAmountBr(IUMMessageUtility.getDouble(value));
				}catch(Exception e){
					spcd.setPendingAmountBr(-1);
				}
			} else if (attribute.equalsIgnoreCase("PEND_AD_AMT")) {
				try{
					spcd.setPendingAmountAd(IUMMessageUtility.getDouble(value));
				}catch(Exception e){
					spcd.setPendingAmountAd(-1);
				}
			} else if (attribute.equalsIgnoreCase("INFC_BR_AMT")) {
				try{
					spcd.setExistingInForceBr(IUMMessageUtility.getDouble(value));
				}catch(Exception e){
					spcd.setExistingInForceBr(-1);
				}
			} else if (attribute.equalsIgnoreCase("INFC_AD_AMT")) {
				try{
					spcd.setExistingInForceAd(IUMMessageUtility.getDouble(value));
				}catch(Exception e){
					spcd.setExistingInForceAd(-1);
				}
			} else if (attribute.equalsIgnoreCase("LAPS_BR_AMT")) {
				try{
					spcd.setExistingLapsedBr(IUMMessageUtility.getDouble(value));
				}catch(Exception e){
					spcd.setExistingLapsedBr(-1);
				}
			} else if (attribute.equalsIgnoreCase("LAPS_AD_AMT")) {
				try{
					spcd.setExistingLapsedAd(IUMMessageUtility.getDouble(value));
				}catch(Exception e){
					spcd.setExistingInForceAd(-1);
				}
			} else if (attribute.equalsIgnoreCase("OINS_BR_AMT")) {
				try{
					spcd.setOtherCompaniesBr(IUMMessageUtility.getDouble(value));
				}catch(Exception e){
					spcd.setOtherCompaniesBr(-1);
				}
			} else if (attribute.equalsIgnoreCase("OINS_AD_AMT")) {
				try{
					spcd.setOtherCompaniesAd(IUMMessageUtility.getDouble(value));
				}catch(Exception e){
					spcd.setOtherCompaniesAd(-1);
				}
			} else if (attribute.equalsIgnoreCase("INFC_CCR_AMT")) {
				try{
					spcd.setTotalCCRCoverage(IUMMessageUtility.getDouble(value));
				}catch(Exception e){
					spcd.setTotalCCRCoverage(-1);
				}
			} else if (attribute.equalsIgnoreCase("INFC_APDB_AMT")) {
				try{
					spcd.setTotalAPDBCoverage(IUMMessageUtility.getDouble(value));
				}catch(Exception e){
					spcd.setTotalAPDBCoverage(-1);
				}
			} else if (attribute.equalsIgnoreCase("INFC_HIB_AMT")) {
				try{
					spcd.setTotalHIBCoverage(IUMMessageUtility.getDouble(value));
				}catch(Exception e){
					spcd.setTotalHIBCoverage(-1);
				}
			} else if (attribute.equalsIgnoreCase("INFC_FMB_AMT")) {
				try{
					spcd.setTotalFBBMBCoverage(IUMMessageUtility.getDouble(value));
				}catch(Exception e){
					spcd.setTotalFBBMBCoverage(-1);
				}
			} else if (attribute.equalsIgnoreCase("TOT_REINS_AMT")) {
				try{
					spcd.setTotalReinsuredAmount(IUMMessageUtility.getDouble(value));
				}catch(Exception e){
					spcd.setTotalReinsuredAmount(-1);
				}
			} else {
				LOGGER.warn("Attribute [" + attribute + "] for Summary Policy Coverage is not defined.");
				throw new IUMInterfaceException("Attribute [" + attribute + "] for Summary Policy Coverage is not defined.");
			}
			
		}		
		LOGGER.info("parseSummaryCoverage end");
		return spcd;
	}
	
	private CDSMortalityRatingData parseMortalityRating(List children, boolean owner, String refNum) throws IUMInterfaceException {
		
		LOGGER.info("parseMortalityRating start");
		CDSMortalityRatingData mrd = new CDSMortalityRatingData();
		
		String prefix = "INSR";
		if (owner) {
			prefix = "OWN";
		}
		for (int i=0; i < children.size(); i++) {
			Element child = (Element)children.get(i);
			String attribute = child.getName();
			String value = child.getText();

			if (attribute == null) {
				attribute = "";
			}

			if (value == null) {
				value = "";
			} else {
				value = value.trim();
			}
			
			if (attribute.equalsIgnoreCase(prefix + "_CLI_HT_FT")) {
				try{
					mrd.setHeightInFeet(IUMMessageUtility.getLong(value));
				}catch(Exception e){
					mrd.setHeightInFeet(-1);
				}
			} else if (attribute.equalsIgnoreCase(prefix + "_CLI_HT_INCH")) {
				try{
					mrd.setHeightInInches(IUMMessageUtility.getLong(value));
				}catch(Exception e){
					mrd.setHeightInInches(-1);
				}
			} else if (attribute.equalsIgnoreCase(prefix + "_CLI_WT_LBS")) {
				try{
					mrd.setWeight(IUMMessageUtility.getDouble(value));
				}catch(Exception e){
					mrd.setWeight(-1);
				}				
			} else if (attribute.equalsIgnoreCase(prefix + "_BASIC_RAT1")) {
				try{
					mrd.setBasic(IUMMessageUtility.getDouble(value));
					
				}catch(Exception e){
					mrd.setBasic(-1);
				}	
			} else if (attribute.equalsIgnoreCase(prefix + "_BASIC_RAT2")) {
				try{
					mrd.setSmokingBasic(IUMMessageUtility.getDouble(value));
				}catch(Exception e){
					mrd.setBasic(-1);
				}	
			} else if (attribute.equalsIgnoreCase(prefix + "_BASIC_RAT3")) {
				try{
					mrd.setFamilyHistoryBasic(IUMMessageUtility.getDouble(value));
				}catch(Exception e){
					mrd.setBasic(-1);
				}	
			} else if (attribute.equalsIgnoreCase(prefix + "_CCR_RAT1")) {
				try{
					mrd.setCCR(IUMMessageUtility.getDouble(value));
				}catch(Exception e){
					mrd.setCCR(-1);
				}	
			} else if (attribute.equalsIgnoreCase(prefix + "_CCR_RAT2")) {
				try{
					mrd.setSmokingCCR(IUMMessageUtility.getDouble(value));
				}catch(Exception e){
					mrd.setCCR(-1);
				}	
			} else if (attribute.equalsIgnoreCase(prefix + "_CCR_RAT3")) {
				try{
					mrd.setFamilyHistoryCCR(IUMMessageUtility.getDouble(value));
				}catch(Exception e){
					mrd.setCCR(-1);
				}	
			} else if (attribute.equalsIgnoreCase(prefix + "_APDB_RAT1")) {
				try{
					mrd.setAPDB(IUMMessageUtility.getDouble(value));
				}catch(Exception e){
					mrd.setAPDB(-1);
				}	
			} else if (attribute.equalsIgnoreCase(prefix + "_APDB_RAT2")) {
				try{
					mrd.setSmokingAPDB(IUMMessageUtility.getDouble(value));
				}catch(Exception e){
					mrd.setAPDB(-1);
				}	
			} else if (attribute.equalsIgnoreCase(prefix + "_APDB_RAT3")) {
				try{
					mrd.setFamilyHistoryAPDB(IUMMessageUtility.getDouble(value));
				}catch(Exception e){
					mrd.setAPDB(-1);
				}	
			} else {
				LOGGER.warn("Attribute [" + attribute + "] for Mortality Rating is not defined.");
				throw new IUMInterfaceException("Attribute [" + attribute + "] for Mortality Rating is not defined.");
			}
		}
		mrd.setReferenceNumber(refNum);
		LOGGER.info("parseMortalityRating end");
		return mrd;
	}
		
	
}
