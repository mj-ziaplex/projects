/*
 * Created on Jan 5, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.interfaces.messages;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Calendar;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import com.slocpi.ium.interfaces.IUMInterfaceException;
import com.slocpi.ium.util.CodeHelper;

/**
 * 
 * This class is for facilitating the conversion and formatting of data
 * @author daguila
 *
 */
public class IUMMessageUtility {

	private static final Logger LOGGER = LoggerFactory.getLogger(IUMMessageUtility.class);
	public static final String DEFAULT_DATE_FORMAT = "yyyy-MM-dd";

	/**
	 * This method will convert the StringBuffer into a Document.
	 * 
	 * @param message
	 * @return a JDOM document
	 */
	public static Document getDocument(StringBuffer message) throws IOException, JDOMException {
		
		LOGGER.info("getDocument start");
		SAXBuilder sb = new SAXBuilder();
		InputStream in = new ByteArrayInputStream(message.toString().getBytes());
		LOGGER.info("getDocument end");
		return sb.build(in);
	}	
	
	/**
	 * Checks if the value is true 
	 * 
	 * @param value
	 * @return true if value is 'y' or 't', else false 
	 */
	public static boolean isTrue(String value) {
		
		
		if ((value != null) && ((value.equalsIgnoreCase("y")) || (value.equalsIgnoreCase("t")))) {
			return true;
		} else {
			return false;
		}
		
	}
	
	/**
	 * converts a boolean value into a StringBuffer 
	 * 
	 * @param bolValue
	 * @param noOfChars
	 * @return a StringBuffer of the specified length  
	 */
	public static StringBuffer fillSpace(boolean bolValue, int noOfChars) {
		
		LOGGER.info("fillSpace start 1");
		String res = ""	;
		if (bolValue) {
			res = "Y";
		} else {
			res = "N";
		}
		
		String value = res;
		StringBuffer sb = new StringBuffer();
		if (value != null) {
			if (value.length() > noOfChars) {
				sb.append(value.substring(0,noOfChars));
			} else {
				sb.append(value);
				for (int i=value.length(); i < noOfChars; i++) {
					sb.append(" ");					
				}
			}
		}
		LOGGER.info("fillSpace end 1");
		return sb;

	}
	
	/**
	 * converts a long value into a StringBuffer 
	 * 
	 * @param longValue
	 * @param noOfChars
	 * @return a StringBuffer of the specified length  
	 */
	public static StringBuffer fillSpace(long longValue, int noOfChars) {
		
		LOGGER.info("fillSpace start 2");
		String res = "";
		try {
			res = Long.toString(longValue);
		} catch (Exception e) {
			// just by pass
		}
		String value = res;
		StringBuffer sb = new StringBuffer();
		if (value != null) {
			if (value.length() > noOfChars) {
				sb.append(value.substring(0,noOfChars));
			} else {
				sb.append(value);
				for (int i=value.length(); i < noOfChars; i++) {
					sb.append(" ");					
				}
			}
		}
		LOGGER.info("fillSpace end 2");
		return sb;
	}
	
	/**
	 * converts an int value into a StringBuffer 
	 * 
	 * @param intValue
	 * @param noOfChars
	 * @return a StringBuffer of the specified length  
	 */
	public static StringBuffer fillSpace(int intValue, int noOfChars) {
		
		LOGGER.info("fillSpace start 3");
		String res = "";
		try {
			res = Integer.toString(intValue);
		} catch (Exception e) {
			// just bypass
		}
		String value = res;
		StringBuffer sb = new StringBuffer();
		if (value != null) {
			if (value.length() > noOfChars) {
				sb.append(value.substring(0,noOfChars));
			} else {
				sb.append(value);
				for (int i=value.length(); i < noOfChars; i++) {
					sb.append(" ");					
				}
			}
		}
		LOGGER.info("fillSpace end 3");
		return sb;
	}
	
	/**
	 * converts an int value into a StringBuffer and pads it with the specified filler up to the specified length
	 * 
	 * @param intValue
	 * @param noOfChars
	 * @param filler
	 * @return a StringBuffer of the specified length  
	 */
	public static StringBuffer fillSpace(int intValue, int noOfChars, String filler) {
		
		LOGGER.info("fillSpace start 4");
		String res = "";
		try {
			res = Integer.toString(intValue);
		} catch (Exception e) {
			// just bypass
		}
		String value = res;
		StringBuffer sb = new StringBuffer();
		if (value != null) {
			if (value.length() > noOfChars) {
				sb.append(value.substring(0,noOfChars));
			} else {
				for (int i=value.length(); i < noOfChars; i++) {
					sb.append(filler);					
				}
				sb.append(value);
			}
		}
		LOGGER.info("fillSpace end 4");
		return sb;
	}
	
	/**
	 * if the length of the value is less than the given length, it fills it with space/s
	 * 
	 * @param value
	 * @param noOfChars
	 * @return a StringBuffer of the specified length  
	 */
	public static StringBuffer fillSpace(String value, int noOfChars) {
		
		LOGGER.info("fillSpace start 5");
		StringBuffer sb = new StringBuffer();
		if (value != null) {
			if (value.length() > noOfChars) {
				sb.append(value.substring(0,noOfChars));
			} else {
				sb.append(value);
				for (int i=value.length(); i < noOfChars; i++) {
					sb.append(" ");					
				}
			}
			//sb = escapeElement(sb);
		}else{
			for (int i=0; i < noOfChars; i++) {
				sb.append(" ");					
			}			
		}
		LOGGER.info("fillSpace end 5");
		return sb;
	}
	
	/**
	 * converts the string into a double
	 * 
	 * @param value
	 * @return a double  
	 */
	public static double getDouble(String value) throws IUMInterfaceException {
		
		
		double d = 0;
		try {
			if(value!=null && !value.equals("")){
				d = Double.parseDouble(value);
			}
		} catch (Exception e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMInterfaceException(e); 
		}
		
		return d;
	}
	
	/**
	 * converts the string into long
	 * 
	 * @param value
	 * @return a long value of the string  
	 */
	public static long getLong(String value) throws IUMInterfaceException {
		
		
		long l = 0;
		try {
			if(value!=null && !value.equals("")){
				l = Long.parseLong(value);
			}
		} catch (Exception e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMInterfaceException(e);
		}
		
		return l;
	}
	
	/**
	 * converts the string into integer
	 * 
	 * @param value
	 * @return the int value of the string  
	 */
	public static int getInteger(String value) throws IUMInterfaceException {
		
		LOGGER.info("getInteger start");
		int i = 0;
		try {
			i = Integer.parseInt(value);
		} catch (Exception e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMInterfaceException(e);
		}
		LOGGER.info("getInteger end");
		return i;
	}
	
	/**
	 * generates a key by getting the hour, minute, second and millisecond of the timestamp
	 * 
	 * @param value
	 * @return string produced from concatenating the hour, minute, second and millisecond  
	 */
	public static String generateIUMMQkey(){
		
		LOGGER.info("generateIUMMQkey start");

		String key = "";
		key = Integer.toString(Calendar.getInstance().get(Calendar.HOUR_OF_DAY)) + 
			Integer.toString(Calendar.getInstance().get(Calendar.MINUTE)) + 
			Integer.toString(Calendar.getInstance().get(Calendar.SECOND)) +
			Integer.toString(Calendar.getInstance().get(Calendar.MILLISECOND));
			
		StringBuffer sb = new StringBuffer();	
		sb.append(fillSpace(Calendar.getInstance().get(Calendar.HOUR_OF_DAY), 2, "0"));
		sb.append(fillSpace(Calendar.getInstance().get(Calendar.MINUTE), 2, "0"));
		sb.append(fillSpace(Calendar.getInstance().get(Calendar.SECOND), 2, "0"));
		sb.append(fillSpace(Calendar.getInstance().get(Calendar.MILLISECOND), 3, "0"));	
		key = sb.toString();
		LOGGER.info("generateIUMMQkey end");
		return key;
	}	

	/**
	 * formats the given list of JDOM elements into xml
	 * 
	 * @param value
	 * @return string produced from concatenating the hour, minute, second and millisecond  
	 */
	public static String writeXML(List list){
		
		LOGGER.info("writeXML start");
		StringBuffer sb = new StringBuffer();
		for(int i=0; i<list.size(); i++){
			Element e = (Element)list.get(i);
			sb.append("<" + e.getName() + ">");
			sb.append(escapeElement(e.getText()));
			sb.append("</" + e.getName() + ">");
		}
		LOGGER.info("writeXML end");
		return sb.toString();
	}
		
	
	/**
	 * 
	 * adds escape character handling per element
	 * 
	 * @param sb - element value to escape
	 * @return StringBuffer containing escaped element
	 */
	protected static String escapeElement(String sb) {
		
		LOGGER.info("escapeElement start");
		
		StringBuffer retString = new StringBuffer(sb);
		retString.append("<![CDATA[");
		retString.append(sb.toString());
		retString.append("]]>");
		
		LOGGER.info("escapeElement end");
		return retString.toString();
	}
	
	/**
	 * method used to replace special characters such as &,<,> to &amp,&lt,&gt respectively
	 * 
	 * @param sb
	 * @param noOfChars
	 * @return a StringBuffer of the specified length  
	 */
	private static StringBuffer escapeChar(StringBuffer sb, int noOfChars) {

		LOGGER.info("escapeChar start");
		for (int i = 0; i < noOfChars; i++) {

			String str = sb.toString();
			int x = str.indexOf('&', i);
			if (x > -1) {

				sb.insert(x + 1, "amp;");
				i = x;
			}
		}
		for (int i = 0; i < noOfChars; i++) {

			String str = sb.toString();
			int x = str.indexOf('<', i);
			if (x > -1) {

				sb.replace(x, x + 1, "&lt;");
				i = x;

			}
		}
		for (int i = 0; i < noOfChars; i++) {

			String str = sb.toString();
			int x = str.indexOf('>', i);
			if (x > -1) {

				sb.replace(x, x + 1, "&gt;");
				i = x;

			}
		}
		LOGGER.info("escapeChar end");
		return sb;
	}	
}
