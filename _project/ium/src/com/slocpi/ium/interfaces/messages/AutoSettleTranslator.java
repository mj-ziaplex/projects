package com.slocpi.ium.interfaces.messages;

import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jdom.Document;
import org.jdom.Element;
import com.slocpi.ium.data.IUMMessageData;
import com.slocpi.ium.interfaces.IUMInterfaceException;
import com.slocpi.ium.util.CodeHelper;

/**
 * 
 * This class is responsible for converting auto settlement messages from
 * and to the MQ - Abacus interfaces.
 * 
 * @author daguila
 *
 * 
 */
public class AutoSettleTranslator {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(AutoSettleTranslator.class);
	public String TRANSACTION_SCCP = "S";
	public String TRANSACTION_UWDE = "U";
	public  String RESPONSE_SUCCESS = "00";
	public String RESPONSE_FAIL_US  = "04";
	public String RESPONSE_FAIL_UW = "05";
	
	
	/**
	 * This method will generate the appropriate auto settle message to be sent
	 * to the MQ - Abacus.
	 * 
	 * @param abacusUserId
	 * @param msgId
	 * @param refNum
	 * @param nbUserId
	 * @param underWriterId
	 * @param transactionCode
	 * @return a StringBuffer containing the message to be sent to the queue
	 * @throws IUMInterfaceException
	 */
	
	public StringBuffer generateAutoSettleMessage(String abacusUserId, String msgId, String refNum, String nbUserId, String underWriterId, String transactionCode) throws IUMInterfaceException {
		
		LOGGER.info("generateAutoSettleMessage start");
		StringBuffer sb = new StringBuffer();
		
		if ((abacusUserId == null) || (abacusUserId.trim().equals(""))) {
			throw new IUMInterfaceException("Missing Abacus User Id");
		}
		
		if ((msgId == null) || (msgId.trim().equals(""))) {
			throw new IUMInterfaceException("Missing Message Id");
		}
		
		if ((refNum == null) || (refNum.trim().equals(""))) {
			throw new IUMInterfaceException("Missing Policy No");
		}
		
		// TODO: For Auto Settlement please verify if the NB User ID and Under writer IDs are valid
		
		sb.append(IUMMessageUtility.fillSpace(abacusUserId,8));
		sb.append(IUMMessageUtility.fillSpace(msgId,10));
		sb.append(IUMMessageUtility.fillSpace(refNum,10));
		sb.append(IUMMessageUtility.fillSpace(nbUserId,8));
		sb.append(IUMMessageUtility.fillSpace(underWriterId,8));
		
		if ((transactionCode == null) || ((!transactionCode.equals(TRANSACTION_SCCP)) && (!transactionCode.equals(TRANSACTION_UWDE)))) {
			throw new IUMInterfaceException("Invalid transaction code passed for Auto Settlement.");
		}
		
		sb.append(IUMMessageUtility.fillSpace(transactionCode,1));
		LOGGER.info("generateAutoSettleMessage end");
		return sb;
	}
	
	/**
	 * This method converts the Policy Auto-Settle Information into an instance of IUMMessageData.
	 * Format of the message should be:
	 * 
	 * <?xml version="1.0" encoding="UTF-8" ?> 
	 * <ROOT> 
	 * <MSG_ID> </MSG_ID>
	 * <CONF_CD> </CONF_CD>
	 * <MSG1> </MSG1>
	 * <MSG2> </MSG2>   
	 * <MSG3> </MSG3>
	 * </ROOT>  
	 * @param message
	 * @return an IUMMessageData object created from the information contained in the input parameter
	 * @throws IUMABACUSParseException
	 */	
	
	public IUMMessageData translateAutoSettleMessage(StringBuffer message) throws IUMInterfaceException {
		
		LOGGER.info("translateAutoSettleMessage start");
		IUMMessageData asd = new IUMMessageData();
		try {
			Document doc = IUMMessageUtility.getDocument(message);
			Element root = doc.getRootElement();
			List nodes = root.getChildren();
			StringBuffer xmlRecord = new StringBuffer();
			xmlRecord.append("<?xml version=\"1.0\" encoding=\"UTF-8\" ?><ROOT>");
			xmlRecord.append(IUMMessageUtility.writeXML(nodes));
			xmlRecord.append("</ROOT>");
			asd.setXmlRecord(xmlRecord.toString());
			
			for (int i=0; i < nodes.size(); i++) {
				Element e = (Element)nodes.get(i);
				String attribute = e.getName();
				String value = e.getText();
				if (attribute == null) {
					attribute = "";
				}
				if (value == null) {
					value = "";
				}
				if (attribute.equalsIgnoreCase("MSG_ID")) {
				} else if (attribute.equalsIgnoreCase("CONF_CD")) {
					asd.setConfirmationCode(value);
				} else if (attribute.equalsIgnoreCase("MSG1")) {
					asd.setMessageLine1(value);
				} else if (attribute.equalsIgnoreCase("MSG2")) {
					asd.setMessageLine2(value);
				} else if (attribute.equalsIgnoreCase("MSG3")) {
					asd.setMessageLine3(value);
				} else {
					LOGGER.warn("IUMInterfaceException Attribute [" + attribute + "] not defined for Auto Settle Info.");
					throw new IUMInterfaceException("Attribute [" + attribute + "] not defined for Auto Settle Info.");
				}
			}						
		} catch (Exception e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMInterfaceException(e.getMessage());
		}
		LOGGER.info("translateAutoSettleMessage end");
		return asd;
	}	
	

}
