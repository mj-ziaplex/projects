/*
 * Created on Dec 17, 2003
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.interfaces.messages;

import java.util.ArrayList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.slocpi.ium.data.AssessmentRequestData;
import com.slocpi.ium.data.IUMMessageData;
import com.slocpi.ium.data.PolicyRequirementsData;
import com.slocpi.ium.interfaces.IUMInterfaceException;

/**
 * 
 * This class is responsible for converting messages to and from the MQ - Abacus.
 * @author daguila
 *
 */
public class MessageConverter {

	private static final Logger LOGGER = LoggerFactory.getLogger(MessageConverter.class);
	public String	TRANSACTION_POLICY_QUEUE 	= "QPQI";
	public String	TRANSACTION_KICK_OUT		= "QCKO";
	public String	TRANSACTION_CLIENT_DATA_SHEET = "QCRI";
	public String  	TRANSACTION_RELATED_POLICY_AND_COVERAGE = "QPRP";
	public String	TRANSACTION_REQUIREMENT_MAINTENANCE = "QRQM";
	public String   TRANSACTION_REQUIREMENT_INQUIRY = "QRRI";
	public String	TRANSACTION_AUTO_SETTLE	= "QPAS";
	
	
	/**
	 * This method will generate the message for retrieving requirements from Abacus.
	 * 
	 * @param abacusUserId
	 * @param msgId
	 * @param refNum
	 * @return a string containing the message to be sent to the queue
	 */
	public String generateRetrieveRequirementsMessage(String abacusUserId, String msgId, String refNum) throws IUMInterfaceException {
		
		LOGGER.info("generateRetrieveRequirementsMessage start");
		PolicyRequirementInquiryTranslator prit = new PolicyRequirementInquiryTranslator();
		LOGGER.info("generateRetrieveRequirementsMessage end");
		return prit.generatePolicyRequirementInquiryMessage(abacusUserId,msgId,refNum,prit.TRANSACTION_RETRIEVE).toString();
	}
	
	/**
	 * This method will generate the request message for related policy and coverage information.
	 * 
	 * @param abacusUserId
	 * @param msgId
	 * @param refNum
	 * @return a string containing the message to be sent to the queue
	 */
	public String	generateRelatedPolicyAndCoverageMessage(String abacusUserId, String msgId, String refNum) throws IUMInterfaceException {
		
		LOGGER.info("generateRelatedPolicyAndCoverageMessage start");
		RelatedPolicyCoverageTranslator rpct = new RelatedPolicyCoverageTranslator();
		LOGGER.info("generateRelatedPolicyAndCoverageMessage end");
		return rpct.generateRelatedPolicyAndCoverageMessage(abacusUserId,msgId,refNum,rpct.TRANSACTION_READ);
	}
	
	/**
	 * This method will generate the request message for deleting the related policy and coverage information of the specific policy in Abacus.
	 * 
	 * @param abacusUserId
	 * @param msgId
	 * @param refNum
	 * @return a string containing the message to be sent to the queue
	 */
	//delete PolicyCoverage 
	public String generateRelatedPolicyAndCoverageDeleteMessage(String abacusUserId, String msgId, String refNum) throws IUMInterfaceException {
		LOGGER.info("generateRelatedPolicyAndCoverageDeleteMessage start");
		RelatedPolicyCoverageTranslator rpct = new RelatedPolicyCoverageTranslator();
		LOGGER.info("generateRelatedPolicyAndCoverageDeleteMessage end");
		return rpct.generateRelatedPolicyAndCoverageMessage(abacusUserId,msgId,refNum,rpct.TRANSACTION_DELETE);
	}
	
	/**
	 * This method will generate the request message for all information of policies due for assessment.
	 * 
	 * @param abacusUserId
	 * @param msgId
	 * @param refNum
	 * @return a string containing the message to be sent to the queue
	 */
	public String generatePolicyQueueMessage(String abacusUserId, String msgId) throws IUMInterfaceException {
		LOGGER.info("generatePolicyQueueMessage start");
		PolicyQueueTranslator pqt = new PolicyQueueTranslator();
		LOGGER.info("generatePolicyQueueMessage end");
		return pqt.generatePolicyQueueMessage(abacusUserId,msgId).toString();
	}
	
	/**
	 * This method will generate the message for retrieving the client related information.
	 * 
	 * @param abacusUserId
	 * @param msgId
	 * @param refNum
	 * @return a string containing the message to be sent to the queue
	 */
	public String generateClientRelatedInfoMessage(String abacusUserId, String msgId, String refNum) throws IUMInterfaceException {
		
		LOGGER.info("generateClientRelatedInfoMessage start");
		ClientRelatedInfoTranslator crit = new ClientRelatedInfoTranslator();
		LOGGER.info("generateClientRelatedInfoMessage end");
		return crit.generateClientRelatedInfoMessage(abacusUserId,msgId,refNum,ClientRelatedInfoTranslator.TRANSACTION_TYPE_RETRIEVE).toString();
	}
	
	
	/**
	 * This method will generate the request message for deleting the client related information of the specific policy in Abacus.
	 * 
	 * @param abacusUserId
	 * @param msgId
	 * @param refNum
	 * @return a string containing the message to be sent to the queue
	 */
	//delete cri
	public String generateClientRelatedInfoDeleteMessage(String abacusUserId, String msgId, String refNum) throws IUMInterfaceException {
		
		LOGGER.info("generateClientRelatedInfoDeleteMessage start");
		ClientRelatedInfoTranslator crit = new ClientRelatedInfoTranslator();
		LOGGER.info("generateClientRelatedInfoDeleteMessage end");
		return crit.generateClientRelatedInfoMessage(abacusUserId,msgId,refNum,ClientRelatedInfoTranslator.TRANSACTION_TYPE_DELETE).toString();
	}
	
	/**
	 * This method will generate the message for retrieving the kick-out messages.
	 * 
	 * @param abacusUserId
	 * @param msgId
	 * @param refNum
	 * @return a string containing the message to be sent to the queue
	 */
	public String generateKickOutMessage(String abacusUserId, String msgId, String refNum) throws IUMInterfaceException {
		
		LOGGER.info("generateKickOutMessage start");
		KickOutTranslator kot = new KickOutTranslator();
		LOGGER.info("generateKickOutMessage end");
		return kot.generateKickOutMessage(abacusUserId,msgId,refNum).toString();
	}
	
	/**
	 * This method will generate the message for creating a requirement.
	 * 
	 * @param abacusUserId
	 * @param msgId
	 * @param prd
	 * @return a string containing the message to be sent to the queue
	 */
	public String generateCreateRequirementMessage(String abacusUserId, String msgId,PolicyRequirementsData prd) throws IUMInterfaceException {
		
		LOGGER.info("generateCreateRequirementMessage start");
		RequirementMaintenanceTranslator rmt = new RequirementMaintenanceTranslator();
		LOGGER.info("generateCreateRequirementMessage end");
		return rmt.generateCreateRequirementMessage(abacusUserId,msgId,prd).toString();
	}
	
	/**
	 * This method will generate the message for ordering a requirement.
	 * 
	 * @param abacusUserId
	 * @param msgId
	 * @param prd
	 * @return a string containing the message to be sent to the queue
	 */
	public String generateOrderRequirementMessage(String abacusUserId, String msgId, PolicyRequirementsData prd) throws IUMInterfaceException {
		
		LOGGER.info("generateOrderRequirementMessage start");
		RequirementMaintenanceTranslator rmt = new RequirementMaintenanceTranslator();
		LOGGER.info("generateOrderRequirementMessage end");
		return rmt.generateOrderRequirementMessage(abacusUserId,msgId,prd).toString();
	}
	
	/**
	 * This method will generate the message for receiving a requirement.
	 * 
	 * @param abacusUserId
	 * @param msgId
	 * @param prd
	 * @return a string containing the message to be sent to the queue
	 */
	public String generateReceiveRequirementMessage(String abacusUserId, String msgId, PolicyRequirementsData prd) throws IUMInterfaceException {
		
		LOGGER.info("generateReceiveRequirementMessage start");
		RequirementMaintenanceTranslator rmt = new RequirementMaintenanceTranslator();
		LOGGER.info("generateReceiveRequirementMessage end");
		return rmt.generateReceiveRequirementMessage(abacusUserId,msgId,prd).toString();
	}	
	
	/**
	 * This method will generate the message for accepting a requirement.
	 * 
	 * @param abacusUserId
	 * @param msgId
	 * @param prd
	 * @return a string containing the message to be sent to the queue
	 */
	public String generateAcceptRequirementMessage(String abacusUserId, String msgId, PolicyRequirementsData prd) throws IUMInterfaceException {
		
		LOGGER.info("generateAcceptRequirementMessage start");
		RequirementMaintenanceTranslator rmt = new RequirementMaintenanceTranslator();
		LOGGER.info("generateAcceptRequirementMessage end");
		return rmt.generateAcceptRequirementMessage(abacusUserId,msgId,prd).toString();
	}	
	
	/**
	 * This method will generate the appropriate auto settle message (SCCP) to be sent to the MQ - Abacus. 
	 * @param abacusUserId
	 * @param msgId
	 * @param refNum
	 * @param nbContactId
	 * @param underwriterId
	 * @return a string containing the message to be sent to the queue
	 */
	public String generateAutoSettleSCCPMessage(String abacusUserId, String msgId, String refNum, String nbContactId, String underwriterId) throws IUMInterfaceException {
		
		LOGGER.info("generateAutoSettleSCCPMessage start");
		AutoSettleTranslator ast = new AutoSettleTranslator();
		LOGGER.info("generateAutoSettleSCCPMessage end");
		return ast.generateAutoSettleMessage(abacusUserId,msgId,refNum,nbContactId,underwriterId,ast.TRANSACTION_SCCP).toString();
	}
	
	/**
	 * This method will generate the appropriate auto settle message (UWDE) to be sent to the MQ - Abacus. 
	 * @param abacusUserId
	 * @param msgId
	 * @param refNum
	 * @param nbContactId
	 * @param underwriterId
	 * @return a string containing the message to be sent to the queue
	 */
	public String generateAutoSettleUWDEMessage(String abacusUserId, String msgId, String refNum, String nbContactId, String underwriterId) throws IUMInterfaceException {
		
		LOGGER.info("generateAutoSettleUWDEMessage start");
		AutoSettleTranslator ast = new AutoSettleTranslator();
		LOGGER.info("generateAutoSettleUWDEMessage end");
		return ast.generateAutoSettleMessage(abacusUserId,msgId,refNum,nbContactId,underwriterId,ast.TRANSACTION_UWDE).toString();
	}
	
	/**
	 * This method converts the message into a Client Data Sheet.
	 * @param message - StringBuffer in xml format
	 * @return an AssessmentRequestData based on the input message 
	 */
	public AssessmentRequestData translateClientRelatedInfoMessage(StringBuffer message) throws IUMInterfaceException {
		
		LOGGER.info("translateClientRelatedInfoMessage start");
		ClientRelatedInfoTranslator crit = new ClientRelatedInfoTranslator();
		LOGGER.info("translateClientRelatedInfoMessage end");
		return crit.translateClientRelatedInfoMessage(message);
	}
	
	/**
	 * This method converts the message into Kick-out messages.
	 * @param message - StringBuffer in xml format
	 * @return an ArrayList of Kick-out messages 
	 */
	public ArrayList translateKickOutMessage(StringBuffer message) throws IUMInterfaceException {
		
		LOGGER.info("translateKickOutMessage start");
		KickOutTranslator kot = new KickOutTranslator();
		LOGGER.info("translateKickOutMessage end");
		return kot.translateKickOutMessage(message);
	}
	
	/**
	 * This method converts the message into Assessment Request Data.
	 * @param message - StringBuffer in xml format
	 * @return an ArrayList of assessment requests 
	 */
	public ArrayList translatePolicyQueMessage(StringBuffer message) throws IUMInterfaceException {
		
		LOGGER.info("translatePolicyQueMessage start");
		PolicyQueueTranslator pqt = new PolicyQueueTranslator();
		LOGGER.info("translatePolicyQueMessage end");
		return pqt.translatePolicyQueMessage(message);
	}
	
	/**
	 * This method converts the message into policy detail data.
	 * @param message - StringBuffer in xml format
	 * @return an ArrayList of policy details 
	 */
	public ArrayList translateRelatedPolicyAndCoverageInfoMessage(StringBuffer message) throws IUMInterfaceException {
		
		LOGGER.info("translateRelatedPolicyAndCoverageInfoMessage start");
		RelatedPolicyCoverageTranslator pct = new RelatedPolicyCoverageTranslator();
		LOGGER.info("translateRelatedPolicyAndCoverageInfoMessage end");
		return pct.translatePolicyAndCoverageInfoMessage(message);
	}
	
	/**
	 * This method converts the message into IUMMessageData.
	 * @param message - StringBuffer in xml format
	 * @return an IUMMessageData 
	 */
	public IUMMessageData translateAutoSettleMessage(StringBuffer message) throws IUMInterfaceException {
		
		LOGGER.info("translateAutoSettleMessage start");
		AutoSettleTranslator ast = new AutoSettleTranslator();
		LOGGER.info("translateAutoSettleMessage end");
		return ast.translateAutoSettleMessage(message);
	}
	
	/**
	 * This method converts the message into PolicyRequirementsData.
	 * @param message - StringBuffer in xml format
	 * @return an ArrayList of PolicyRequirementsData 
	 */
	public ArrayList translatePolicyRequirementInquiryMessage(StringBuffer message) throws IUMInterfaceException {
		
		LOGGER.info("translatePolicyRequirementInquiryMessage start");
		PolicyRequirementInquiryTranslator prit = new PolicyRequirementInquiryTranslator();
		LOGGER.info("translatePolicyRequirementInquiryMessage end");
		return prit.translatePolicyRequirementInquiryMessage(message);
	}
	
	/**
	 * This method converts the message into IUMMessageData.
	 * @param message - StringBuffer in xml format
	 * @return an IUMMessageData 
	 */
	public IUMMessageData translatePolicyRequirementMaintenanceMessage(StringBuffer message) throws IUMInterfaceException {
		
		LOGGER.info("translatePolicyRequirementMaintenanceMessage start");
		RequirementMaintenanceTranslator rmt = new RequirementMaintenanceTranslator();
		LOGGER.info("translatePolicyRequirementMaintenanceMessage end");
		return rmt.translateRequirementMaintenanceMessage(message);
	}

	
	/**
	 * This method will generate the message for updating a requirement in Abacus.
	 * 
	 * @param abacusUserId
	 * @param msgId
	 * @param prd
	 * @return a string containing the message to be sent to the queue
	 */
	public String generateUpdateRequirementMessage(String abacusUserId, String msgId, PolicyRequirementsData prd) throws IUMInterfaceException {
		
		LOGGER.info("generateUpdateRequirementMessage start");
		RequirementMaintenanceTranslator rmt = new RequirementMaintenanceTranslator();
		LOGGER.info("generateUpdateRequirementMessage end");
		return rmt.generateUpdateRequirementMessage(abacusUserId,msgId,prd).toString();
	}	
	
}
