package com.slocpi.ium.interfaces.messages;

import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jdom.Document;
import org.jdom.Element;
import com.slocpi.ium.data.KickOutMessageData;
import com.slocpi.ium.interfaces.IUMInterfaceException;
import com.slocpi.ium.util.CodeHelper;

/**
 * 
 * This class is responsible for converting messages(Kick-out Information) to and from the MQ - Abacus.
 * @author daguila
 *
 */
public class KickOutTranslator {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(KickOutTranslator.class);
	public static final String RESPONSE_SUCCESS = "00";
	private boolean isEmpty = false;
    /**
     * This method will generate the messages to send to MQ - Abacus.
     * 
     * @param abacusUserId
     * @param msgId
     * @param refNum
     * @return a StringBuffer containing the message to be sent to the queue
     */
    
	public StringBuffer generateKickOutMessage(String abacusUserId, String msgId, String refNum) throws IUMInterfaceException {
		
		LOGGER.info("generateKickOutMessage start");
		StringBuffer result = new StringBuffer();
		String userId = "";
		String msg = "";
		
		if (abacusUserId == null) {
			LOGGER.warn("Missing Abacus User Id");
			throw new IUMInterfaceException("Missing Abacus User Id");
		} else {
			userId = abacusUserId;
		}
		
		if (msgId == null) {
			LOGGER.warn("Missing Message Id");
			throw new IUMInterfaceException("Missing Message Id");
		} else {
			msg = msgId;
		}
		
		result.append(IUMMessageUtility.fillSpace(abacusUserId,8));
		result.append(IUMMessageUtility.fillSpace(msgId,10));
		result.append(IUMMessageUtility.fillSpace(refNum,10));
		LOGGER.info("generateKickOutMessage end");
		return result;			
	}
	
	/**
	 * This method extracts the Kick Out Message Data from the child nodes.
	 * Mappings for the attributes are as follows:
	 * From the header part SUBJ_POL_ID = referenceNumber
	 * CLI_ID = clientId
	 * SEQ_NUM = sequenceNumber
	 * MSG_TXT = messageText
	 * MSG_RESP_TXT = failResponse
	 * 
	 * @param children
	 * @param refNum
	 * @return 
	 * @throws IUMABACUSParseException
	 */

	private KickOutMessageData parseKickOutMessageData(List children, String refNum) throws IUMInterfaceException {
		
		LOGGER.info("parseKickOutMessageData start");
		KickOutMessageData komd = new KickOutMessageData();
		komd.setReferenceNumber(refNum);
		isEmpty = false;
		for (int i=0; i < children.size(); i++) {
			Element child = (Element)children.get(i);
			String attribute = child.getName();
			String value = child.getText();

			if (attribute == null) {
				attribute = "";
			}

			if (value == null || value.trim().equals("")) {
				value = "";
			} else {
				value = value.trim();
			}

			if (attribute.equalsIgnoreCase("CLI_ID")) {
				komd.setClientId(value);
			} else if (attribute.equalsIgnoreCase("SEQ_NUM")) {
				try{
					komd.setSequenceNumber(IUMMessageUtility.getLong(value));
				} catch(Exception e){
					komd.setSequenceNumber(0);
				}
				if(komd.getSequenceNumber()==0){
					isEmpty = true;
				}
			} else if (attribute.equalsIgnoreCase("MSG_TXT")) {
				komd.setMessageText(value);
			} else if (attribute.equalsIgnoreCase("MSG_RESP_TXT")) {
				komd.setFailResponse(value);
			} else {
				LOGGER.warn("Attribute [" + attribute + "] not defined for Kick Out Message.");
				throw new IUMInterfaceException("Attribute [" + attribute + "] not defined for Kick Out Message.");
			}
		}
		LOGGER.info("parseKickOutMessageData end");
		return komd;
	}

	/**
	 * This method converts the Kick Out Message into a Kick Out Message Data.
	 * Format of the message should be:
	 * <?xml version="1.0" encoding="UTF-8" ?> 
	 * <ROOT>
	 * <MSG_ID> </MSG_ID> 
	 * <CONF_CD> </CONF_CD>
	 * <SUBJ_POL_ID> </SUBJ_POL_ID>
	 * <CCKO_DATA>
	 *   <CLI_ID> </CLI_ID>
	 *   <SEQ_NUM> </SEQ_NUM>
	 *   <MSG_TXT> </MSG_TXT>
	 *   <MSG_RESP_TXT> </MSG_RESP_TXT>
	 * </CCKO_DATA>     
	 * </ROOT> 
	 * @param message
	 * @return an ArrayList of KickOutMessageData 
	 * @throws IUMABACUSParseException
	 */
	
	public ArrayList translateKickOutMessage(StringBuffer message) throws IUMInterfaceException {
		
		LOGGER.info("translateKickOutMessage start");
		ArrayList list = new ArrayList();
		try {
			Document doc = IUMMessageUtility.getDocument(message);
			Element root = doc.getRootElement();
			List nodes = root.getChildren();
			String refNum = "";
			String msgId = "";
			String confCd = "";
			
			for (int i=0; i < nodes.size(); i++) {
				Element e = (Element)nodes.get(i);
				//Node node = (Node)nodes.get(i);
				String attribute = e.getName();
				String value = e.getText();
				
				if (attribute == null) {
					attribute = "";
				}
				
				if (value == null) {
					value = "";
				} else {
					value = value.trim();
				}
				
				if (attribute.equalsIgnoreCase("MSG_ID")) { 
					msgId = e.getText();
				} else if (attribute.equalsIgnoreCase("CONF_CD")) {
					String recFound = e.getText();
					confCd = recFound;
					if ((recFound == null) || (!recFound.equals(KickOutTranslator.RESPONSE_SUCCESS))) {
						break;
					}
				} else if (attribute.equalsIgnoreCase("SUBJ_POL_ID")) {
					refNum = value;
				} else if (attribute.equalsIgnoreCase("CCKO_DATA")){
					KickOutMessageData koData = parseKickOutMessageData(e.getChildren(),refNum);
					StringBuffer xmlRecord = new StringBuffer();
					xmlRecord.append("<?xml version=\"1.0\" encoding=\"UTF-8\" ?><ROOT>");
					xmlRecord.append("<MSG_ID>" + msgId + "</MSG_ID>");
					xmlRecord.append("<CONF_CD>" + confCd + "</CONF_CD>");
					xmlRecord.append("<SUBJ_POL_ID>" + refNum + "</SUBJ_POL_ID>");
					xmlRecord.append("<CCKO_DATA>");
					xmlRecord.append(IUMMessageUtility.writeXML(e.getChildren()));
					xmlRecord.append("</CCKO_DATA>");
					xmlRecord.append("</ROOT>");
					koData.setXmlRecord(xmlRecord.toString()); 
					if(isEmpty){
						break;
					}
					list.add(koData);
				} else {
					LOGGER.warn("Attribute [" + attribute + "] not defined for Kick Out Message.");
					throw new IUMInterfaceException("Attribute [" + attribute + "] is not defined in the Kick Out Message.");
				}
				
			}			
		} catch (Exception e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMInterfaceException(e);
		}
		LOGGER.info("translateKickOutMessage end");
		return list;
	}
}
