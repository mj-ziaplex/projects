package com.slocpi.ium.interfaces.messages;

import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jdom.Document;
import org.jdom.Element;
import com.slocpi.ium.data.AssessmentRequestData;
import com.slocpi.ium.data.ClientData;
import com.slocpi.ium.data.SunLifeOfficeData;
import com.slocpi.ium.data.UserProfileData;
import com.slocpi.ium.interfaces.IUMInterfaceException;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.DateHelper;

/**
 * This class is responsible for translating messages(Policy Queue Information) to and from MQ - Abacus.
 * @author daguila
 *
 */
public class PolicyQueueTranslator {

	private static final Logger LOGGER = LoggerFactory.getLogger(PolicyQueueTranslator.class);
	public static final String RESPONSE_SUCCESS = "00";
	private boolean isEmpty;
	
	/**
	 * This method generates the message for retrieving policies for assessment.
	 * Format:
	 * 
	 * ABACUS user ID					8 Bytes		Alphanumeric
	 * Unique Message Identifier/Tag	10 Bytes	Alphanumeric
	 *
	 * @param abacusUserId
	 * @param msgId
	 * @return StringBuffer containing the message to be sent to the queue
	 */
	
	public StringBuffer generatePolicyQueueMessage(String abacusUserId, String msgId) throws IUMInterfaceException {
		
		LOGGER.info("generatePolicyQueueMessage start");
		StringBuffer result = new StringBuffer();
		String userId = "";
		String msg = "";
		
		if (abacusUserId == null) {
			userId = "";
		} else {
			userId = abacusUserId;
		}
		
		if (msgId == null) {
			LOGGER.warn("Unique message id is required.");
			throw new IUMInterfaceException("Unique message id is required.");
		} else {
			msg = msgId;
		}
		
		result.append(IUMMessageUtility.fillSpace(abacusUserId,8));
		result.append(IUMMessageUtility.fillSpace(msgId,10));
		LOGGER.info("generatePolicyQueueMessage end");
		return result;	
	}
	
	/**
	 * @param message
	 * @return ArrayList of assessment request data
	 * @throws IUMABACUSParseException
	 */
	
	public ArrayList translatePolicyQueMessage(StringBuffer message) throws IUMInterfaceException {
		
		LOGGER.info("translatePolicyQueMessage start");
		ArrayList list = new ArrayList();
		try {
			
			Document doc = IUMMessageUtility.getDocument(message);
			Element root = doc.getRootElement();
			List nodes = root.getChildren();
			String msgId = null;
			String recFound = null;
			for (int i=0; i < nodes.size(); i++) {
				Element e = (Element)nodes.get(i);
				String attribute = e.getName();
				if (attribute == null) {
					attribute = "";
				}
				if (attribute.equalsIgnoreCase("MSG_ID")) {
					msgId = e.getText();
				} else if (attribute.equalsIgnoreCase("CONF_CD")) {
					recFound = e.getText();
					if ((recFound == null) || (!recFound.equals(PolicyQueueTranslator.RESPONSE_SUCCESS))) {
						break;
					}
				} else if (attribute.equalsIgnoreCase("POL_QUEUE_INFO")) {
					List nodes2 = e.getChildren();
					for(int j=0; j<nodes2.size(); j++){
						Element elmnt = (Element)nodes2.get(j);
						String attr = elmnt.getName();
						if (attr == null) {
							attr = "";
						}
						if (attr.equalsIgnoreCase("POL_DATA")) {	
							AssessmentRequestData ard = parseAssessmentRequestData(elmnt.getChildren());
							StringBuffer xmlRecord = new StringBuffer();
							xmlRecord.append("<?xml version=\"1.0\" encoding=\"UTF-8\" ?><ROOT>");
							xmlRecord.append("<MSG_ID>" + msgId + "</MSG_ID>");
							xmlRecord.append("<CONF_CD>" + recFound + "</CONF_CD>");
							xmlRecord.append("<POL_QUEUE_INFO><POL_DATA>");
							xmlRecord.append(IUMMessageUtility.writeXML(elmnt.getChildren()));
							xmlRecord.append("</POL_DATA></POL_QUEUE_INFO>");
							xmlRecord.append("</ROOT>");
							ard.setXmlRecord(xmlRecord.toString());
							if(isEmpty){
								break;
							}else{
								list.add(ard);
							}
							
						}
					}
				} else {
					LOGGER.warn("Attribute [" + attribute + "] not defined for Policy Queue.");
					throw new IUMInterfaceException("Attribute [" + attribute + "] not defined for Policy Queue.");
				}
			}
			
		} catch (Exception e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMInterfaceException(e.getMessage());
		}
		LOGGER.info("translatePolicyQueMessage end");
		return list;
	}
	
	/**	
	 * @param children
	 * @return
	 * @throws IUMABACUSParseException
	 */
	
	private AssessmentRequestData parseAssessmentRequestData(List children) throws IUMInterfaceException {
		
		LOGGER.info("parseAssessmentRequestData start");
	    AssessmentRequestData ard = new AssessmentRequestData();
		String lastName = "";
		String firstName = "";
		String middleName = "";
		String clientId = "";
		isEmpty = true;
		
		try {
			for (int i=0; i <children.size(); i++) {
				Element child = (Element)children.get(i);
				String attribute = child.getName();
				String value = child.getText();
				if (attribute == null) {
					attribute = "";
				}
				if (value == null || value.trim().equals("")) {
					value = "";
				} else {
					value = value.trim();
				}
				if (attribute.equalsIgnoreCase("POL_ID")) {
					if(!value.equals("")){
						isEmpty = false;
					}
					ard.setReferenceNumber(value);
				} else if (attribute.equalsIgnoreCase("CVG_FACE_AMT")) {
					try{
						ard.setAmountCovered(IUMMessageUtility.getDouble(value));
					}catch(IUMInterfaceException iie){
						ard.setAmountCovered(-1);
					}
				} else if (attribute.equalsIgnoreCase("POL_MPREM_AMT")) {
					try{
						ard.setPremium(IUMMessageUtility.getDouble(value));
					}catch(IUMInterfaceException iie){
						ard.setPremium(-1);
					}
				} else if (attribute.equalsIgnoreCase("APP_RECV_DT")) {
					ard.setApplicationReceivedDate(DateHelper.parse(value,IUMMessageUtility.DEFAULT_DATE_FORMAT));
				} else if (attribute.equalsIgnoreCase("NB_CNTCT_USER_ID")) {
				
					UserProfileData upd = new UserProfileData();
					upd.setACF2ID(value);
					ard.setAssignedTo(upd);
					ard.setFolderLocation(upd);
				} else if (attribute.equalsIgnoreCase("UW_USER_ID")) {
				
					UserProfileData upd = new UserProfileData();
					upd.setACF2ID(value);
					ard.setUnderwriter(upd);	
				} else if (attribute.equalsIgnoreCase("INSR_CLI_ID")) {
					clientId = value;
				} else if (attribute.equalsIgnoreCase("INSR_SUR_NM")) {
					lastName = value;				
				} else if (attribute.equalsIgnoreCase("INSR_GIV_NM")) {
					firstName =value;
				} else if (attribute.equalsIgnoreCase("INSR_MID_NM")) {
					middleName=value;
				} else if (attribute.equalsIgnoreCase("AGT_ID")) {
				
					UserProfileData upd = new UserProfileData();
					upd.setUserId(value);
					ard.setAgent(upd);
				} else if (attribute.equalsIgnoreCase("SOFF_ID")) {
				
					SunLifeOfficeData sod = new SunLifeOfficeData();
					sod.setOfficeId(value);
					ard.setBranch(sod);
				} else {
					LOGGER.warn("Attribute [" + attribute + "] for POL_QUEUE_INFO is not defined.");
					throw new IUMInterfaceException("Attribute [" + attribute + "] for POL_QUEUE_INFO is not defined.");
				}
					
			}
			
			ClientData cd = new ClientData();
			cd.setClientId(clientId);
			cd.setLastName(lastName);
			cd.setMiddleName(middleName);
			cd.setGivenName(firstName);
			ard.setInsured(cd);			
			
			
		} catch (Exception e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMInterfaceException("Error in retrieving values [" + e.getMessage() + "]");
		}
		LOGGER.info("parseAssessmentRequestData end");
		return ard;
	}
	
}
