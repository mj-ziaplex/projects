/*
 * Created on Jan 5, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.interfaces.messages;

import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jdom.Document;
import org.jdom.Element;
import com.slocpi.ium.data.PolicyDetailData;
import com.slocpi.ium.interfaces.IUMInterfaceException;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.DateHelper;

/**
 * This class is responsible for converting messages(Related Policy and Coverage Information) to and from the MQ - Abacus.
 * @author daguila
 *
 */
public class RelatedPolicyCoverageTranslator {

	private static final Logger LOGGER = LoggerFactory.getLogger(RelatedPolicyCoverageTranslator.class);
	public String TRANSACTION_READ = "R";
	public String TRANSACTION_DELETE = "D";
	private String RESPONSE_SUCCESS = "00";
	private boolean isEmpty;
	/**
	 * This method generates the messages sent to MQ to retrieve related policies.
	 * @param abacusUserId
	 * @param msgId
	 * @param refNum
	 * @param transactionCode
	 * @return a string representation of the message to be sent to the queue
	 */
	public String generateRelatedPolicyAndCoverageMessage(String abacusUserId, String msgId, String refNum, String transactionCode) {
		
		LOGGER.info("generateRelatedPolicyAndCoverageMessage start");
		StringBuffer sb = new StringBuffer();
		String userId = "";
		String msg = "";
		
		if (abacusUserId == null) {
			userId = "";
		} else {
			userId = abacusUserId;
		}
		
		if (msgId == null) {
			msg = "";
		} else {
			msg = msgId;
		}
		
		sb.append(IUMMessageUtility.fillSpace(userId,8));
		sb.append(IUMMessageUtility.fillSpace(msgId,10));
		sb.append(IUMMessageUtility.fillSpace(refNum,10));
		sb.append(IUMMessageUtility.fillSpace(transactionCode,1));
		
		LOGGER.info("generateRelatedPolicyAndCoverageMessage end");
		return sb.toString();
	}
	
	/**
	 * @param message
	 * @return ArrayList of policy details
	 * @throws IUMABACUSParseException
	 */

	public ArrayList translatePolicyAndCoverageInfoMessage(StringBuffer message) throws IUMInterfaceException {
		
		LOGGER.info("translatePolicyAndCoverageInfoMessage start");
		ArrayList list = new ArrayList();
		try {
			Document doc = IUMMessageUtility.getDocument(message);
			Element root = doc.getRootElement();
			List nodes = root.getChildren();
			String refNum = "";
			String msgId = "";
			String confCd = "";
			
			for (int i=0; i < nodes.size(); i++) {
				Element e = (Element)nodes.get(i);
				String attribute = e.getName();
				String value = e.getText();
				
				if (attribute == null) {
					attribute = "";
				}
				
				if (value == null) {
					value = "";
				} else {
					value = value.trim();
				}
				
				if (attribute.equalsIgnoreCase("MSG_ID")) {
					msgId = e.getText();
				} else if (attribute.equalsIgnoreCase("CONF_CD")) {
					confCd = e.getText();
					if (!value.equals(RESPONSE_SUCCESS)) {
						break;
					}
				} else if (attribute.equalsIgnoreCase("SUBJ_POL_ID")) {
					refNum = value;
				} else if (attribute.equalsIgnoreCase("REL_POL_INFO")) {
					PolicyDetailData polDetailData = parsePolicyAndCoverageInfo(e.getChildren(),refNum); 
					StringBuffer xmlRecord = new StringBuffer();
					xmlRecord.append("<?xml version=\"1.0\" encoding=\"UTF-8\" ?><ROOT>");
					xmlRecord.append("<MSG_ID>" + msgId + "</MSG_ID>");
					xmlRecord.append("<CONF_CD>" + confCd + "</CONF_CD>");
					xmlRecord.append("<SUBJ_POL_ID>" + refNum + "</SUBJ_POL_ID>");
					xmlRecord.append("<REQT_POL_INFO>");
					xmlRecord.append(IUMMessageUtility.writeXML(e.getChildren()));
					xmlRecord.append("</REQT_POL_INFO>");
					xmlRecord.append("</ROOT>");
					polDetailData.setXmlRecord(xmlRecord.toString());
					if(isEmpty){
						break;
					}
					list.add(polDetailData); 
					
				} else {
					LOGGER.warn("Attribute [" + attribute + "] not defined for Policy and Coverage Info.");
					throw new IUMInterfaceException("Attribute [" + attribute + "] not defined for Policy and Coverage Info.");
				}
			}			
		} catch (Exception e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMInterfaceException(e.getMessage());
		}
		LOGGER.info("translatePolicyAndCoverageInfoMessage end");
		return list;
	}
	
	private PolicyDetailData parsePolicyAndCoverageInfo(List children, String refNum) throws IUMInterfaceException {
		
		LOGGER.info("parsePolicyAndCoverageInfo start");
		PolicyDetailData pdd = new PolicyDetailData();
		isEmpty = true;
		
		for (int i=0; i <children.size(); i++) {
			Element child = (Element)children.get(i);
			String attribute = child.getName();
			String value = child.getText();
			if (attribute == null) {
				attribute = "";
			}
			if (value == null || value.trim().equals("")) {
				value = "";
			} else {
				value = value.trim();
			}
			
			if (attribute.equalsIgnoreCase("REL_POL_ID")) {
				if(!value.equals("")){
					isEmpty = false;
				}
				pdd.setPolicyNumber(value);
			} else if (attribute.equalsIgnoreCase("CVG_NUM")) {
				try{
					pdd.setCoverageNumber(IUMMessageUtility.getLong(value));
				} catch (Exception e){
					pdd.setCoverageNumber(-1);
				}
			} else if (attribute.equalsIgnoreCase("REL_CD")) {
				pdd.setRelationship(value);
			} else if (attribute.equalsIgnoreCase("CVG_ISS_EFF_DT")) {
				pdd.setIssueDate(DateHelper.parse(value,IUMMessageUtility.DEFAULT_DATE_FORMAT));
			} else if (attribute.equalsIgnoreCase("CVG_STAT_CD")) {
				pdd.setCoverageStatus(value);
			} else if (attribute.equalsIgnoreCase("CVG_SMKR_CD")) {
				pdd.setSmoker(IUMMessageUtility.isTrue(value));
			} else if (attribute.equalsIgnoreCase("CVG_PLAN_ID")) {
				pdd.setPlanCode(value);
			} else if (attribute.equalsIgnoreCase("MEDIC_IND")) {
				pdd.setMedical(IUMMessageUtility.isTrue(value));
			} else if (attribute.equalsIgnoreCase("CVG_FACE_AMT")) {
				try{
					pdd.setFaceAmount(IUMMessageUtility.getDouble(value));
				} catch(Exception e){
					pdd.setFaceAmount(-1);
				}
			} else if (attribute.equalsIgnoreCase("DEC_TYP")) {
				pdd.setDecision(value);
			} else if (attribute.equalsIgnoreCase("AD_FACE_AMT")) {
				try{
					pdd.setADBFaceAmount(IUMMessageUtility.getDouble(value));
				} catch (Exception e){
					pdd.setADBFaceAmount(-1);
				}
			} else if (attribute.equalsIgnoreCase("AD_MULT")) {
				try {
					pdd.setADMultiplier(IUMMessageUtility.getDouble(value));
				}catch (Exception e){
					pdd.setADMultiplier(-1);
				}
			} else if (attribute.equalsIgnoreCase("WP_MULT")) {
				try{
					pdd.setWPMultiplier(IUMMessageUtility.getDouble(value));
				} catch (Exception e){
					pdd.setWPMultiplier(-1);
				}
			} else if (attribute.equalsIgnoreCase("REINS_AMT")) {
				try{
					pdd.setReinsuredAmount(IUMMessageUtility.getDouble(value));	
				}catch (Exception e){
					pdd.setReinsuredAmount(-1);
				}
			} else {
				LOGGER.warn("Attribute [" + attribute + "] for Policy and Coverage Info is not defined.");
				throw new IUMInterfaceException("Attribute [" + attribute + "] for Policy and Coverage Info is not defined.");
			}
		}
				
		pdd.setReferenceNumber(refNum);
		LOGGER.info("parsePolicyAndCoverageInfo end");
		return pdd;
	}
		
}
