/*
 * Created on Jan 15, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.interfaces.test;

/**
 * @author daguila
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class AbacusKickOutMessage {
	private String subjectPolicyId;
	private String clientId;
	private int sequenceNumber;
	private String messageText;
	private String messageResponseText;
	/**
	 * @return
	 */
	public String getClientId() {
		return clientId;
	}

	/**
	 * @return
	 */
	public String getMessageResponseText() {
		return messageResponseText;
	}

	/**
	 * @return
	 */
	public String getMessageText() {
		return messageText;
	}

	/**
	 * @return
	 */
	public int getSequenceNumber() {
		return sequenceNumber;
	}

	/**
	 * @return
	 */
	public String getSubjectPolicyId() {
		return subjectPolicyId;
	}

	/**
	 * @param string
	 */
	public void setClientId(String string) {
		clientId = string;
	}

	/**
	 * @param string
	 */
	public void setMessageResponseText(String string) {
		messageResponseText = string;
	}

	/**
	 * @param string
	 */
	public void setMessageText(String string) {
		messageText = string;
	}

	/**
	 * @param i
	 */
	public void setSequenceNumber(int i) {
		sequenceNumber = i;
	}

	/**
	 * @param string
	 */
	public void setSubjectPolicyId(String string) {
		subjectPolicyId = string;
	}

}
