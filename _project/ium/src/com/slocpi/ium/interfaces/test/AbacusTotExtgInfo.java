/*
 * Created on Jan 20, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.interfaces.test;

/**
 * @author daguila
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class AbacusTotExtgInfo {
	private String subjectPolicyId;
	private double appBRAmount;
	private double appADAmount;
	private double pendBRAmount;
	private double pendADAmount;
	private double infcBRAmount;
	private double infcADAmount;
	private double lapsADAMount;
	private double lapsBRAmount;
	private double oinsBRAmount;
	private double oinsADAmount;
	private double infcCCRAmount;
	private double infcAPDBAmount;
	private double infcHIBAmount;
	private double infcFMBAmount;
	private double totalReinsAmount;
	/**
	 * @return
	 */
	public double getAppADAmount() {
		return appADAmount;
	}

	/**
	 * @return
	 */
	public double getAppBRAmount() {
		return appBRAmount;
	}

	/**
	 * @return
	 */
	public double getInfcADAmount() {
		return infcADAmount;
	}

	/**
	 * @return
	 */
	public double getInfcAPDBAmount() {
		return infcAPDBAmount;
	}

	/**
	 * @return
	 */
	public double getInfcBRAmount() {
		return infcBRAmount;
	}

	/**
	 * @return
	 */
	public double getInfcCCRAmount() {
		return infcCCRAmount;
	}

	/**
	 * @return
	 */
	public double getInfcFMBAmount() {
		return infcFMBAmount;
	}

	/**
	 * @return
	 */
	public double getInfcHIBAmount() {
		return infcHIBAmount;
	}

	/**
	 * @return
	 */
	public double getLapsADAMount() {
		return lapsADAMount;
	}

	/**
	 * @return
	 */
	public double getLapsBRAmount() {
		return lapsBRAmount;
	}

	/**
	 * @return
	 */
	public double getOinsADAmount() {
		return oinsADAmount;
	}

	/**
	 * @return
	 */
	public double getOinsBRAmount() {
		return oinsBRAmount;
	}

	/**
	 * @return
	 */
	public double getPendADAmount() {
		return pendADAmount;
	}

	/**
	 * @return
	 */
	public double getPendBRAmount() {
		return pendBRAmount;
	}

	/**
	 * @return
	 */
	public String getSubjectPolicyId() {
		return subjectPolicyId;
	}

	/**
	 * @return
	 */
	public double getTotalReinsAmount() {
		return totalReinsAmount;
	}

	/**
	 * @param d
	 */
	public void setAppADAmount(double d) {
		appADAmount = d;
	}

	/**
	 * @param d
	 */
	public void setAppBRAmount(double d) {
		appBRAmount = d;
	}

	/**
	 * @param d
	 */
	public void setInfcADAmount(double d) {
		infcADAmount = d;
	}

	/**
	 * @param d
	 */
	public void setInfcAPDBAmount(double d) {
		infcAPDBAmount = d;
	}

	/**
	 * @param d
	 */
	public void setInfcBRAmount(double d) {
		infcBRAmount = d;
	}

	/**
	 * @param d
	 */
	public void setInfcCCRAmount(double d) {
		infcCCRAmount = d;
	}

	/**
	 * @param d
	 */
	public void setInfcFMBAmount(double d) {
		infcFMBAmount = d;
	}

	/**
	 * @param d
	 */
	public void setInfcHIBAmount(double d) {
		infcHIBAmount = d;
	}

	/**
	 * @param d
	 */
	public void setLapsADAMount(double d) {
		lapsADAMount = d;
	}

	/**
	 * @param d
	 */
	public void setLapsBRAmount(double d) {
		lapsBRAmount = d;
	}

	/**
	 * @param d
	 */
	public void setOinsADAmount(double d) {
		oinsADAmount = d;
	}

	/**
	 * @param d
	 */
	public void setOinsBRAmount(double d) {
		oinsBRAmount = d;
	}

	/**
	 * @param d
	 */
	public void setPendADAmount(double d) {
		pendADAmount = d;
	}

	/**
	 * @param d
	 */
	public void setPendBRAmount(double d) {
		pendBRAmount = d;
	}

	/**
	 * @param string
	 */
	public void setSubjectPolicyId(String string) {
		subjectPolicyId = string;
	}

	/**
	 * @param d
	 */
	public void setTotalReinsAmount(double d) {
		totalReinsAmount = d;
	}

}
