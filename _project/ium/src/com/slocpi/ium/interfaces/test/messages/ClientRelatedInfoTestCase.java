/*
 * Created on Jan 4, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.interfaces.test.messages;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import com.slocpi.ium.data.AssessmentRequestData;
import com.slocpi.ium.data.CDSMortalityRatingData;
import com.slocpi.ium.data.ClientData;
import com.slocpi.ium.data.ClientDataSheetData;
import com.slocpi.ium.interfaces.messages.MessageConverter;
import com.slocpi.ium.util.DateHelper;

/**
 * @author daguila
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class ClientRelatedInfoTestCase extends TestCase {

	public ClientRelatedInfoTestCase(java.lang.String testName) {        
		super(testName);    
	}        

	public static Test suite() {        
		TestSuite suite = new TestSuite(ClientRelatedInfoTestCase.class);                
		return suite;    
	}	
	public static void main(java.lang.String[] args) {   
		junit.textui.TestRunner.run(suite());    
	}
	
	public void testClientInfo() throws Exception {
		MessageConverter mb = new MessageConverter();
		AssessmentRequestData ard = mb.translateClientRelatedInfoMessage(buildClientRelateInfoMessage());
		AssessmentRequestData ard2 = getClientInfo();
		assertEquals("Did not translate Client Related Info message properly.",true,isAssessmentRequestEqual(ard,ard2));
	}
	
	private boolean isAssessmentRequestEqual(AssessmentRequestData obj1, AssessmentRequestData obj2) {
		boolean res = false;
		if ((obj1 != null) && (obj2 != null)) {
			// TODO: Dave Add more checking
			if (
				(obj1.getReferenceNumber().equals(obj2.getReferenceNumber()))
				) {
				res = true;
			}
		} else if ((obj1 == null) && (obj2 == null)) {
			res = true;
		}
		return res;
	}
	private AssessmentRequestData getClientInfo() {
		AssessmentRequestData ard = new AssessmentRequestData();
		ClientDataSheetData cdsd = new ClientDataSheetData();
		ClientData insr = new ClientData();
		ClientData own = new ClientData();
		CDSMortalityRatingData insrMrd = new CDSMortalityRatingData();
		CDSMortalityRatingData ownMrd = new CDSMortalityRatingData();
		
		ard.setReferenceNumber("1");
		cdsd.setReferenceNumber("1");
		
		// Client Data
		insr.setClientId("1");
		own.setClientId("1");
		insr.setLastName("InsrSurname");
		own.setLastName("OwnLastName");
		insr.setGivenName("InsrGiven");
		own.setGivenName("OwnGiven");
		insr.setMiddleName("InsrMiddle");
		own.setMiddleName("OwnMiddle");
		insr.setOtherLastName("InsrOthSurname");
		own.setOtherLastName("OwnOthSurname");
		insr.setOtherGivenName("InsrOthGiven");
		own.setOtherGivenName("OwnOthGiven");
		insr.setOtherMiddleName("InsrOthMiddle");
		own.setOtherMiddleName("OwnOthMiddle");
		insr.setTitle("Mr");
		own.setTitle("Mr");
		insr.setSuffix("Jr");     
		own.setSuffix("Jr");
		insr.setBirthDate(DateHelper.parse("07/24/1975"));
		own.setBirthDate(DateHelper.parse("07/24/1975"));
		insr.setBirthLocation("BLoc");
		own.setBirthLocation("BLoc");
		insr.setSex("M");
		own.setSex("M");
		insr.setAge(28);
		own.setAge(28);
		insr.setSmoker(true);
		own.setSmoker(true);
		 
		// TODO: Dave put population of summary policies here
		
		// Mortality Rating
		insrMrd.setHeightInFeet(1);
		ownMrd.setHeightInFeet(1);
		insrMrd.setHeightInInches(1);
		ownMrd.setHeightInInches(1);
		insrMrd.setWeight(1);
		ownMrd.setWeight(1);
		insrMrd.setBasic(1);
		ownMrd.setBasic(1);
		// TODO: Dave what about the rat 2 .. 3
		insrMrd.setCCR(1);
		ownMrd.setCCR(1);
		// TODO: Dave what about the rat 2 .. 3
		insrMrd.setAPDB(1);
		ownMrd.setAPDB(1);
		// TODO: Dave what about the rat 2 .. 3
		
		ard.setInsured(insr);
		ard.setOwner(own);
		ard.setClientDataSheet(cdsd);
		ard.setInsuredMortalityRating(insrMrd);
		ard.setOwnerMortalityRating(ownMrd);
		
		return ard;
	}
	
	private StringBuffer buildClientRelateInfoMessage() {
		StringBuffer res = new StringBuffer("<?xml version=\"1.0\" encoding=\"UTF-8\" ?>" +
						"<ROOT>" +
						"<MSG_ID> </MSG_ID>" + 
						"<CONF_CD>00</CONF_CD>");
		res.append("<CDS_DATA>" +
				"<SUBJ_POL_ID>1</SUBJ_POL_ID>" +
				"<STAT_CD>1</STAT_CD>");
		res.append("<INSR_CLI_INFO>" +
				"<INSR_CLI_ID>1</INSR_CLI_ID>" +
				"<INSR_SUR_NM>InsrSurname</INSR_SUR_NM>" +
				"<INSR_GIV_NM>InsrGiven</INSR_GIV_NM>" +
				"<INSR_MID_NM>InsrMiddle</INSR_MID_NM>" +
				"<INSR_OTH_SUR_NM>InsrOthSurname</INSR_OTH_SUR_NM>" +
				"<INSR_OTH_GIV_NM>InsrOthGiven</INSR_OTH_GIV_NM>" +
				"<INSR_OTH_MID_NM>InsrOthMiddle</INSR_OTH_MID_NM>" +     
				"<INSR_TITL_TXT>Mr</INSR_TITL_TXT>" +
				"<INSR_SFX_NM>Jr</INSR_SFX_NM>" +
				"<INSR_BTH_DT>1975-07-24</INSR_BTH_DT>" +
				"<INSR_BTH_LOC>BLoc</INSR_BTH_LOC>" +      
				"<INSR_SEX_CD>M</INSR_SEX_CD>" +
				"<INSR_CLI_AGE>28</INSR_CLI_AGE>" +  
				"<INSR_SMKR_CD>Y</INSR_SMKR_CD>" + 
				"</INSR_CLI_INFO>");
		res.append("<OWN_CLI_INFO>" +
				"<OWN_CLI_ID>2</OWN_CLI_ID>" +
				"<OWN_SUR_NM>OwnSurname</OWN_SUR_NM>" +
				"<OWN_GIV_NM>OwnGiven</OWN_GIV_NM>" +
				"<OWN_MID_NM>OwnMiddle</OWN_MID_NM>" +
				"<OWN_OTH_SUR_NM>OwnOthSurname</OWN_OTH_SUR_NM>" +
				"<OWN_OTH_GIV_NM>OwnOthGiven</OWN_OTH_GIV_NM>" +
				"<OWN_OTH_MID_NM>OwnOthMiddle</OWN_OTH_MID_NM>" +
				"<OWN_TITL_TXT>Mr</OWN_TITL_TXT>" +
				"<OWN_SFX_NM>Jr</OWN_SFX_NM>" +
				"<OWN_BTH_DT>1975-07-24</OWN_BTH_DT>" +
				"<OWN_BTH_LOC>BLoc</OWN_BTH_LOC>" +
				"<OWN_SEX_CD>M</OWN_SEX_CD>" +
				"<OWN_CLI_AGE>28</OWN_CLI_AGE>" +
				"<OWN_SMKR_CD>Y</OWN_SMKR_CD>" +                
				"</OWN_CLI_INFO>");  
		res.append("<TOT_EXTG_INFO>" +
				"<APP_BR_AMT>1</APP_BR_AMT>" +
				"<APP_AD_AMT>1</APP_AD_AMT>" +
				"<PEND_BR_AMT>1</PEND_BR_AMT>" +
				"<PEND_AD_AMT>1</PEND_AD_AMT>" +
				"<INFC_BR_AMT>1</INFC_BR_AMT>" +
				"<INFC_AD_AMT>1</INFC_AD_AMT>" +
				"<LAPS_BR_AMT>1</LAPS_BR_AMT>" +
				"<LAPS_AD_AMT>1</LAPS_AD_AMT>" +
				"<OINS_BR_AMT>1</OINS_BR_AMT>" +
				"<OINS_AD_AMT>1</OINS_AD_AMT>" +
				"<INFC_CCR_AMT>1</INFC_CCR_AMT>" +
				"<INFC_APDB_AMT>1</INFC_APDB_AMT>" +             
				"<INFC_HIB_AMT>1</INFC_HIB_AMT>" +
				"<INFC_FMB_AMT>1</INFC_FMB_AMT>" +
				"<TOT_REINS_AMT>1</TOT_REINS_AMT>" +   
				"</TOT_EXTG_INFO>"); 
		res.append("<MORTALITY_INFO>");
		res.append("<INSR_CLI_MORT>" +
				"<INSR_CLI_HT_FT>1</INSR_CLI_HT_FT>" +
				"<INSR_CLI_HT_INCH>1</INSR_CLI_HT_INCH>" +
				"<INSR_CLI_WT_LBS>1</INSR_CLI_WT_LBS>" +
				"<INSR_BASIC_RAT1>1</INSR_BASIC_RAT1>" +
				"<INSR_BASIC_RAT2>1</INSR_BASIC_RAT2>" +
				"<INSR_BASIC_RAT3>1</INSR_BASIC_RAT3>" +       
				"<INSR_CCR_RAT1>1</INSR_CCR_RAT1>" +
				"<INSR_CCR_RAT2>1</INSR_CCR_RAT2>" +
				"<INSR_CCR_RAT3>1</INSR_CCR_RAT3>" +  
				"<INSR_APDB_RAT1>1</INSR_APDB_RAT1>" +
				"<INSR_APDB_RAT2>1</INSR_APDB_RAT2>" +
				"<INSR_APDB_RAT3>1</INSR_APDB_RAT3>" +  
				"</INSR_CLI_MORT>"); 
		res.append("<OWN_CLI_MORT>" + 
				"<OWN_CLI_HT_FT>1</OWN_CLI_HT_FT>" +
				"<OWN_CLI_HT_INCH>1</OWN_CLI_HT_INCH>" +
				"<OWN_CLI_WT_LBS>1</OWN_CLI_WT_LBS>" +
				"<OWN_BASIC_RAT1>1</OWN_BASIC_RAT1>" +
				"<OWN_BASIC_RAT2>1</OWN_BASIC_RAT2>" +
				"<OWN_BASIC_RAT3>1</OWN_BASIC_RAT3>" +       
				"<OWN_CCR_RAT1>1</OWN_CCR_RAT1>" +
				"<OWN_CCR_RAT2>1</OWN_CCR_RAT2>" +
				"<OWN_CCR_RAT3>1</OWN_CCR_RAT3>" +  
				"<OWN_APDB_RAT1>1</OWN_APDB_RAT1>" +
				"<OWN_APDB_RAT2>1</OWN_APDB_RAT2>" +
				"<OWN_APDB_RAT3>1</OWN_APDB_RAT3>" +
				"</OWN_CLI_MORT>"); 
		res.append("</MORTALITY_INFO>");
		res.append("</CDS_DATA>");
		res.append("</ROOT>"); 		
		return res;
	}
	
}
