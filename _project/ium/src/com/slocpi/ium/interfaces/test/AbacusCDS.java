/*
 * Created on Jan 16, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.interfaces.test;

/**
 * @author daguila
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class AbacusCDS {
	
	private String subjectPolicyId;
	private String statusCode;
	private AbacusTotExtgInfo totalExtgInfo;

	/**
	 * @return
	 */
	public String getStatusCode() {
		return statusCode;
	}

	/**
	 * @return
	 */
	public String getSubjectPolicyId() {
		return subjectPolicyId;
	}

	/**
	 * @param string
	 */
	public void setStatusCode(String string) {
		statusCode = string;
	}

	/**
	 * @param string
	 */
	public void setSubjectPolicyId(String string) {
		subjectPolicyId = string;
	}

	/**
	 * @return
	 */
	public AbacusTotExtgInfo getTotalExtgInfo() {
		return totalExtgInfo;
	}

	/**
	 * @param info
	 */
	public void setTotalExtgInfo(AbacusTotExtgInfo info) {
		totalExtgInfo = info;
	}

}
