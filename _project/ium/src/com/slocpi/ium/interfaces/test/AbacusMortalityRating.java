/*
 * Created on Jan 18, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.interfaces.test;

/**
 * @author daguila
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class AbacusMortalityRating {
	private String	subjectPolicyId;
	private int	APDBRating3;
	private int  APDBRating2;
	private int  APDBRating1;
	private int  CCRRating3;
	private int  CCRRating2;
	private int  CCRRating1;
	private int  basicRating3;
	private int  basicRating2;
	private int  basicRating1;
	private int  weightPounds;
	private int  heightInches;
	private int  heightFeet;
	/**
	 * @return
	 */
	public int getAPDBRating1() {
		return APDBRating1;
	}

	/**
	 * @return
	 */
	public int getAPDBRating2() {
		return APDBRating2;
	}

	/**
	 * @return
	 */
	public int getAPDBRating3() {
		return APDBRating3;
	}

	/**
	 * @return
	 */
	public int getBasicRating1() {
		return basicRating1;
	}

	/**
	 * @return
	 */
	public int getBasicRating2() {
		return basicRating2;
	}

	/**
	 * @return
	 */
	public int getBasicRating3() {
		return basicRating3;
	}

	/**
	 * @return
	 */
	public int getCCRRating1() {
		return CCRRating1;
	}

	/**
	 * @return
	 */
	public int getCCRRating2() {
		return CCRRating2;
	}

	/**
	 * @return
	 */
	public int getCCRRating3() {
		return CCRRating3;
	}

	/**
	 * @return
	 */
	public int getHeightFeet() {
		return heightFeet;
	}

	/**
	 * @return
	 */
	public int getHeightInches() {
		return heightInches;
	}

	/**
	 * @return
	 */
	public String getSubjectPolicyId() {
		return subjectPolicyId;
	}

	/**
	 * @return
	 */
	public int getWeightPounds() {
		return weightPounds;
	}

	/**
	 * @param i
	 */
	public void setAPDBRating1(int i) {
		APDBRating1 = i;
	}

	/**
	 * @param i
	 */
	public void setAPDBRating2(int i) {
		APDBRating2 = i;
	}

	/**
	 * @param i
	 */
	public void setAPDBRating3(int i) {
		APDBRating3 = i;
	}

	/**
	 * @param i
	 */
	public void setBasicRating1(int i) {
		basicRating1 = i;
	}

	/**
	 * @param i
	 */
	public void setBasicRating2(int i) {
		basicRating2 = i;
	}

	/**
	 * @param i
	 */
	public void setBasicRating3(int i) {
		basicRating3 = i;
	}

	/**
	 * @param i
	 */
	public void setCCRRating1(int i) {
		CCRRating1 = i;
	}

	/**
	 * @param i
	 */
	public void setCCRRating2(int i) {
		CCRRating2 = i;
	}

	/**
	 * @param i
	 */
	public void setCCRRating3(int i) {
		CCRRating3 = i;
	}

	/**
	 * @param i
	 */
	public void setHeightFeet(int i) {
		heightFeet = i;
	}

	/**
	 * @param i
	 */
	public void setHeightInches(int i) {
		heightInches = i;
	}

	/**
	 * @param string
	 */
	public void setSubjectPolicyId(String string) {
		subjectPolicyId = string;
	}

	/**
	 * @param i
	 */
	public void setWeightPounds(int i) {
		weightPounds = i;
	}

}
