/*
 * Created on Jan 18, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.interfaces.test;

import java.util.Date;

/**
 * @author daguila
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class AbacusClientInfo {

	private String subjectPolicyId;
	private boolean smokerCode;
	private int age;
	private String sex;
	private String birthLocation;
	private Date   birthDate;
	private String suffix;
	private String title;
	private String middleName;
	private String givenName;
	private String lastName;
	private String otherLastName;
	private String otherMiddleName;
	private String otherGivenName;
	private String clientId;
	/**
	 * @return
	 */
	public int getAge() {
		return age;
	}

	/**
	 * @return
	 */
	public Date getBirthDate() {
		return birthDate;
	}

	/**
	 * @return
	 */
	public String getBirthLocation() {
		return birthLocation;
	}

	/**
	 * @return
	 */
	public String getClientId() {
		return clientId;
	}

	/**
	 * @return
	 */
	public String getGivenName() {
		return givenName;
	}

	/**
	 * @return
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @return
	 */
	public String getMiddleName() {
		return middleName;
	}

	/**
	 * @return
	 */
	public String getSex() {
		return sex;
	}

	/**
	 * @return
	 */
	public boolean isSmokerCode() {
		return smokerCode;
	}

	/**
	 * @return
	 */
	public String getSubjectPolicyId() {
		return subjectPolicyId;
	}

	/**
	 * @return
	 */
	public String getSuffix() {
		return suffix;
	}

	/**
	 * @return
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param i
	 */
	public void setAge(int i) {
		age = i;
	}

	/**
	 * @param date
	 */
	public void setBirthDate(Date date) {
		birthDate = date;
	}

	/**
	 * @param string
	 */
	public void setBirthLocation(String string) {
		birthLocation = string;
	}

	/**
	 * @param string
	 */
	public void setClientId(String string) {
		clientId = string;
	}

	/**
	 * @param string
	 */
	public void setGivenName(String string) {
		givenName = string;
	}

	/**
	 * @param string
	 */
	public void setLastName(String string) {
		lastName = string;
	}

	/**
	 * @param string
	 */
	public void setMiddleName(String string) {
		middleName = string;
	}

	/**
	 * @param string
	 */
	public void setSex(String string) {
		sex = string;
	}

	/**
	 * @param b
	 */
	public void setSmokerCode(boolean b) {
		smokerCode = b;
	}

	/**
	 * @param string
	 */
	public void setSubjectPolicyId(String string) {
		subjectPolicyId = string;
	}

	/**
	 * @param string
	 */
	public void setSuffix(String string) {
		suffix = string;
	}

	/**
	 * @param string
	 */
	public void setTitle(String string) {
		title = string;
	}

	/**
	 * @return
	 */
	public String getOtherGivenName() {
		return otherGivenName;
	}

	/**
	 * @return
	 */
	public String getOtherLastName() {
		return otherLastName;
	}

	/**
	 * @return
	 */
	public String getOtherMiddleName() {
		return otherMiddleName;
	}

	/**
	 * @param string
	 */
	public void setOtherGivenName(String string) {
		otherGivenName = string;
	}

	/**
	 * @param string
	 */
	public void setOtherLastName(String string) {
		otherLastName = string;
	}

	/**
	 * @param string
	 */
	public void setOtherMiddleName(String string) {
		otherMiddleName = string;
	}

}
