/*
 * Created on Jan 8, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.interfaces.test;

import java.io.File;
import java.io.FileWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import com.slocpi.ium.util.DateHelper;

/**
 * @author daguila
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class AbacusTestUtil {
	
	private File	sqlFile;
	private FileWriter file;
	private String	STATUS_OPEN = "NR";
	private Connection conn = null;
	private int noOfAR;
	private int noOfKO;
	private int noOfRP;
	
	public AbacusTestUtil(int _noOfAr, int _noOfKO, int _noOfRP) throws Exception {
		conn = getConnection();
		noOfAR = _noOfAr;
		noOfKO = _noOfKO;
		noOfRP = _noOfRP;
		sqlFile = new File("abacusdata.sql");
		file = new FileWriter(sqlFile);
		
	}
	
	public ArrayList getPolicyQueTestResults() throws Exception {
		ArrayList list=buildPolicyQueue(noOfAR);
		file.close(); 
		return  list;
	}
	
	public void deletePolicyQueue() throws Exception {
		Connection conn = getConnection();
		PreparedStatement ps = conn.prepareStatement("delete from policy_queue");
		ps.executeUpdate();
		ps.close();
	}
	
	private ArrayList buildPolicyQueue(int numOfRecs) throws Exception {

		ArrayList list = new ArrayList();
		String clientId = "client1";
		
		for (int i =0; i < numOfRecs; i++) {
			AbacusPolicy ap = new AbacusPolicy();
			String key = getKey();
			ap.setPolicyId(key);
			ap.setPolicyStatus(STATUS_OPEN);
			ap.setAgentId("REQAG1");
			ap.setInsuredGivenName("DAVE");
			ap.setInusredMiddleName("MN");
			ap.setInsuredSurname("AGUILA");
			ap.setNBContactUserId("REQNBST1");
			ap.setUWUserId("REQUW1");
			ap.setOfficeId("temp1");
			ap.setPolicyCoverageFaceAmount(1000000);
			ap.setPolicyMPremiumAmount(2000000);
			ap.setPolicyReceivedDate(new Date());
			ap.setPolicySeqNum(i + 1);
			insertPolicy(ap,clientId);
			ap.setKickOutMessages(createKickOutMessage(ap.getPolicyId(),clientId,noOfKO));
			ap.setCds(createCDS(ap.getPolicyId()));
			ap.setInsrMortRating(createMortalityRating(ap.getPolicyId(),true));
			ap.setOwnMortRating(createMortalityRating(ap.getPolicyId(),false));
			ap.setInsrClientInfo(createClientInfo(ap.getPolicyId(),clientId,true));
			ap.setOwnClientInfo(createClientInfo(ap.getPolicyId(),clientId,false));
			ap.setRelatedPolicies(createRelatedPolicies(ap.getPolicyId(),5));
			list.add(ap);
			
			delay();
		}
		
		
		return list;
	}
	
	private void insertPolicy(AbacusPolicy ap, String clientId) throws Exception {
		
		file.write("\n\n /*  inserting new policy */\n\n");
		StringBuffer sb = new StringBuffer();
		StringBuffer print = new StringBuffer();
		sb.append("insert into policy (pol_id, stat_cd, cli_id) values (?, ?, ?)");
		print.append("insert into policy (pol_id, stat_cd, cli_id) values (");
		PreparedStatement ps = conn.prepareStatement(sb.toString());
		ps.setString(1,ap.getPolicyId());
		print.append(printValue(ap.getPolicyId()));
		print.append(",");
		ps.setString(2,ap.getPolicyStatus());
		print.append(printValue(ap.getPolicyStatus()));
		print.append(",");
		ps.setString(3,clientId);
		print.append(printValue(clientId));
		ps.executeUpdate();
		print.append(");");
		ps.close();
		file.write(print.toString() + "\n");
		System.out.println("Have inserted successfully policy number " + ap.getPolicyId());
		
		sb = new StringBuffer();
		print = new StringBuffer();
		sb.append("select seq_policy_queue.nextval from dual");
		
		ps = conn.prepareStatement(sb.toString());
		ResultSet rs = ps.executeQuery();
		if (rs.next()) {
			ap.setPolicySeqNum(rs.getInt(1));
		}
		
		rs.close();
		ps.close();
		
		sb = new StringBuffer();
		sb.append("insert into policy_queue (pol_id," +
											"cvg_face_amt," +
											"pol_mprem_amt," +
											"APP_RECV_DT," +
											"NB_CNTCT_USER_ID," +
											"UW_USER_ID," +
											"INSR_SUR_NM," +
											"INSR_GIV_NM," +
											"INSR_MID_NM," +
											"AGT_ID," +
											"SOFF_ID," +
											"pol_seq_num) ");
		print.append(sb.toString());
		sb.append("values (?,?,?,?,?,?,?,?,?,?,?,?) ");
		print.append("values (");
		
		ps = conn.prepareStatement(sb.toString());
		ps.setString(1,ap.getPolicyId());
		print.append(printValue(ap.getPolicyId()));
		print.append(",");
		ps.setDouble(2,ap.getPolicyCoverageFaceAmount());
		print.append(printValue(ap.getPolicyCoverageFaceAmount()));
		print.append(",");				
		ps.setDouble(3,ap.getPolicyMPremiumAmount());
		print.append(printValue(ap.getPolicyMPremiumAmount()));
		print.append(",");
		ps.setDate(4,DateHelper.sqlDate(ap.getPolicyReceivedDate()));
		print.append(printValue(ap.getPolicyReceivedDate()));
		print.append(",");
		ps.setString(5,ap.getNBContactUserId());
		print.append(printValue(ap.getNBContactUserId()));
		print.append(",");
		ps.setString(6,ap.getUWUserId());
		print.append(printValue(ap.getUWUserId()));
		print.append(",");
		ps.setString(7,ap.getInsuredSurname());
		print.append(printValue(ap.getInsuredSurname()));
		print.append(",");
		ps.setString(8,ap.getInsuredGivenName());
		print.append(printValue(ap.getInsuredGivenName()));
		print.append(",");
		ps.setString(9,ap.getInusredMiddleName());
		print.append(printValue(ap.getInusredMiddleName()));
		print.append(",");
		ps.setString(10,ap.getAgentId());
		print.append(printValue(ap.getAgentId()));
		print.append(",");
		ps.setString(11,ap.getOfficeId());
		print.append(printValue(ap.getOfficeId()));
		print.append(",");
		ps.setInt(12,ap.getPolicySeqNum());
		print.append(printValue(ap.getPolicySeqNum()));
		ps.executeUpdate();
		print.append(");");
		ps.close();
		file.write(print.toString() + "\n");
		System.out.println("Insert policy " + ap.getPolicyId() + " in policy queue");
		
	}
	
	private ArrayList createKickOutMessage(String refNum, String clientId, int numOfRecs) throws Exception {
		ArrayList list = new ArrayList();
		for (int i=0; i < numOfRecs; i++) {
			AbacusKickOutMessage kom = new AbacusKickOutMessage();
			kom.setSubjectPolicyId(refNum);
			kom.setClientId(clientId); // policy id same with client id
			kom.setMessageResponseText("Not Approved");
			kom.setMessageText("Not Approved");
			kom.setSequenceNumber(getNextVal("seq_kick_out")); // will be generated by the sequence no;
			insertKickOut(kom);
			list.add(kom);	
		}
		
		return list;
	}
	
	private void insertKickOut(AbacusKickOutMessage kom) throws Exception {
		StringBuffer sb = new StringBuffer();
		StringBuffer print = new StringBuffer();
		sb.append("INSERT INTO kick_out (" +
							"SUBJ_POL_ID, " +
							"CLI_ID, " +
							"SEQ_NUM, " +
							"MSG_TXT, " +
							"MSG_RESP_TXT) ");
		print.append(sb.toString());
		sb.append("VALUES (?, ?, ?, ?, ?)");
		print.append("values (");
		PreparedStatement ps = 	conn.prepareStatement(sb.toString());
		ps.setString(1,kom.getSubjectPolicyId());
		print.append(printValue(kom.getSubjectPolicyId()));
		print.append(",");
		ps.setString(2,kom.getClientId());
		print.append(printValue(kom.getClientId()));
		print.append(",");
		ps.setInt(3,kom.getSequenceNumber());
		print.append(printValue(kom.getSequenceNumber()));
		print.append(",");
		ps.setString(4,kom.getMessageText());
		print.append(printValue(kom.getMessageText()));
		print.append(",");
		ps.setString(5,kom.getMessageResponseText());
		print.append(printValue(kom.getMessageResponseText()));
		ps.executeUpdate();
		print.append(");");
		ps.close();
		file.write(print.toString() + "\n");
		System.out.println("Inserted kickout message with seq " + kom.getSequenceNumber() + " policy " + kom.getSubjectPolicyId());
						
	}
	
	private AbacusCDS createCDS(String refNum) throws Exception {
		AbacusCDS cds = new AbacusCDS();
		cds.setStatusCode("A");
		cds.setSubjectPolicyId(refNum);
		insertCDS(cds);
		cds.setTotalExtgInfo(createTotExtgInfo(refNum));
		return cds;
	}
	
	private void insertCDS(AbacusCDS cds) throws Exception {
		StringBuffer sb = new StringBuffer();
		StringBuffer print = new StringBuffer();
		sb.append("insert into cds (subj_pol_id, stat_cd) values (?,?)");
		print.append("insert into cds (subj_pol_id, stat_cd) values (");
		PreparedStatement ps = conn.prepareStatement(sb.toString());
		ps.setString(1,cds.getSubjectPolicyId());
		print.append(printValue(cds.getSubjectPolicyId()));
		print.append(",");
		ps.setString(2,cds.getStatusCode());
		print.append(printValue(cds.getStatusCode()));
		ps.executeUpdate();
		print.append(");");
		ps.close();
		file.write(print.toString() + "\n");
		System.out.println("Have inserted CDS for policy " + cds.getSubjectPolicyId());
	}
	
	private AbacusMortalityRating createMortalityRating(String refNum, boolean insured) throws Exception {
		AbacusMortalityRating mort = new AbacusMortalityRating();
		mort.setAPDBRating1(1);
		mort.setAPDBRating2(1);
		mort.setAPDBRating3(1);
		mort.setBasicRating1(1);
		mort.setBasicRating2(1);
		mort.setBasicRating3(1);
		mort.setCCRRating1(1);
		mort.setCCRRating2(1);
		mort.setCCRRating3(1);
		mort.setHeightInches(12);
		mort.setHeightFeet(6);
		mort.setSubjectPolicyId(refNum);
		mort.setWeightPounds(12);
		insertMortalityRating(mort,insured);
		return mort;
	}
	
	private void insertMortalityRating(AbacusMortalityRating mort, boolean insured) throws Exception {
		StringBuffer sb = new StringBuffer();
		StringBuffer print = new StringBuffer();
		String prefix = "insr";
		sb.append("INSERT INTO ");
		if (insured) {
			sb.append(" insr_cli_mort ");
		} else {
			prefix = "own";
			sb.append(" own_cli_mort ");
		}
		sb.append(" ( ");
		sb.append("SUBJ_POL_ID, " +
				prefix + "_CLI_HT_FT, " +
				prefix + "_CLI_HT_INCH, " +
				prefix + "_CLI_WT_LBS, " +
				prefix + "_BASIC_RAT1, " +
				prefix + "_BASIC_RAT2, " + 
				prefix + "_BASIC_RAT3, " +
				prefix + "_CCR_RAT1, " +
				prefix + "_CCR_RAT2, " +
				prefix + "_CCR_RAT3, " +
				prefix + "_APDB_RAT1, " +
				prefix + "_APDB_RAT2, " +
				prefix + "_APDB_RAT3 ");
		sb.append(" ) ");
		print.append(sb.toString());
		sb.append(" VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)");
		print.append(" values (");
		PreparedStatement ps = conn.prepareStatement(sb.toString());
		ps.setString(1,mort.getSubjectPolicyId());
		print.append(printValue(mort.getSubjectPolicyId()));
		print.append(",");
		ps.setInt(2,mort.getHeightFeet());
		print.append(printValue(mort.getHeightFeet()));
		print.append(",");
		ps.setInt(3,mort.getHeightInches());
		print.append(printValue(mort.getHeightInches()));
		print.append(",");
		ps.setInt(4,mort.getWeightPounds());
		print.append(printValue(mort.getWeightPounds()));
		print.append(",");
		ps.setInt(5,mort.getBasicRating1());
		print.append(printValue(mort.getBasicRating1()));
		print.append(",");
		ps.setInt(6,mort.getBasicRating2());
		print.append(mort.getBasicRating2());
		print.append(",");
		ps.setInt(7,mort.getBasicRating3());
		print.append(mort.getBasicRating3());
		print.append(",");
		ps.setInt(8,mort.getCCRRating1());
		print.append(mort.getCCRRating1());
		print.append(",");
		ps.setInt(9,mort.getCCRRating2());
		print.append(mort.getCCRRating2());
		print.append(",");
		ps.setInt(10,mort.getCCRRating3());
		print.append(mort.getCCRRating3());
		print.append(",");
		ps.setInt(11,mort.getAPDBRating1());
		print.append(mort.getAPDBRating1());
		print.append(",");
		ps.setInt(12,mort.getAPDBRating2());
		print.append(printValue(mort.getAPDBRating2()));
		print.append(",");
		ps.setInt(13,mort.getAPDBRating3());
		print.append(printValue(mort.getAPDBRating3()));
		ps.executeUpdate();
		print.append(");");
		ps.close();
		file.write(print.toString() + "\n");
		if (insured) {
			System.out.println("Have inserted insured client mortality rating for policy " + mort.getSubjectPolicyId());
		} else {
			System.out.println("Have inserted owner client mortality rating for policy " + mort.getSubjectPolicyId());
		}
	}
	
	private AbacusClientInfo createClientInfo(String refNum,String clientId,boolean insured) throws Exception {
		AbacusClientInfo clientInfo = new AbacusClientInfo();
		clientInfo.setAge(24);
		clientInfo.setBirthDate(DateHelper.parse("24/12/1976","dd/mm/yyyy"));
		clientInfo.setBirthLocation("wala");
		clientInfo.setClientId(clientId);
		clientInfo.setGivenName("Hula");
		clientInfo.setLastName("Hoop");
		clientInfo.setMiddleName("Hula");
		clientInfo.setSex("M");
		clientInfo.setSmokerCode(true);
		clientInfo.setSubjectPolicyId(refNum);
		clientInfo.setSuffix("Jr.");
		clientInfo.setTitle("Mr.");
		clientInfo.setOtherGivenName("o Hula");
		clientInfo.setOtherLastName("o Hoop");
		clientInfo.setOtherMiddleName("o Hula");
		insertClientInfo(clientInfo,insured);
		return clientInfo;
	}
	
	private void insertClientInfo(AbacusClientInfo aci, boolean insured) throws Exception {
		StringBuffer sb = new StringBuffer();
		StringBuffer print = new StringBuffer();
		String prefix = "own";
		if (insured) {
			prefix = "insr";
		}
		sb.append("INSERT INTO " + prefix + "_cli_info (" +
				  		"SUBJ_POL_ID, " +
				  		prefix + "_CLI_ID, " +
				  		prefix + "_SUR_NM, " +
				  		prefix + "_GIV_NM, " +
				  		prefix + "_MID_NM, " +
				  		prefix + "_OTH_SUR_NM, " +
				  		prefix + "_OTH_GIV_NM, " +
				  		prefix + "_OTH_MID_NM, " +
				  		prefix + "_TITL_TXT, " +
				  		prefix + "_SFX_NM, " +
				  		prefix + "_BTH_DT, " +
				  		prefix + "_BTH_LOC, " +
				  		prefix + "_SEX_CD, " +
				  		prefix + "_CLI_AGE, " +
				  		prefix + "_SMKR_CD ) "
				);
		print.append(sb.toString());
		sb.append(" values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ");
		print.append(" values (");
		PreparedStatement ps = conn.prepareStatement(sb.toString());
		ps.setString(1, aci.getSubjectPolicyId());
		print.append(printValue(aci.getSubjectPolicyId()));
		print.append(",");
		ps.setString(2,aci.getClientId());
		print.append(printValue(aci.getClientId()));
		print.append(",");
		ps.setString(3,aci.getLastName());
		print.append(printValue(aci.getLastName()));
		print.append(",");
		ps.setString(4,aci.getGivenName());
		print.append(printValue(aci.getGivenName()));
		print.append(",");
		ps.setString(5,aci.getMiddleName());
		print.append(printValue(aci.getMiddleName()));
		print.append(",");
		ps.setString(6,aci.getOtherLastName());
		print.append(printValue(aci.getOtherLastName()));
		print.append(",");
		ps.setString(7,aci.getOtherGivenName());
		print.append(printValue(aci.getOtherGivenName()));
		print.append(",");
		ps.setString(8,aci.getOtherMiddleName());
		print.append(printValue(aci.getOtherMiddleName()));
		print.append(",");
		ps.setString(9,aci.getTitle());
		print.append(printValue(aci.getTitle()));
		print.append(",");
		ps.setString(10,aci.getSuffix());
		print.append(printValue(aci.getSuffix()));
		print.append(",");
		ps.setDate(11,DateHelper.sqlDate(aci.getBirthDate()));
		print.append(printValue(aci.getBirthDate()));
		print.append(",");
		ps.setString(12,aci.getBirthLocation());
		print.append(printValue(aci.getBirthLocation()));
		print.append(",");
		ps.setString(13,aci.getSex());
		print.append(printValue(aci.getSex()));
		print.append(",");
		ps.setInt(14,aci.getAge());
		print.append(printValue(aci.getAge()));
		print.append(",");
		if (aci.isSmokerCode()) {
			ps.setString(15,"Y");
		} else {
			ps.setString(15,"N");
		}
		print.append(printValue(aci.isSmokerCode()));
		ps.executeUpdate();
		print.append(");");
		ps.close();
		file.write(print.toString() + "\n");
		System.out.println("Have insert " + prefix + " client info for policy " + aci.getSubjectPolicyId());
	}
	
	private AbacusTotExtgInfo createTotExtgInfo(String refNum) throws Exception {
		AbacusTotExtgInfo atei = new AbacusTotExtgInfo();
		atei.setAppADAmount(1000);
		atei.setAppBRAmount(1000);
		atei.setInfcADAmount(1000);
		atei.setInfcAPDBAmount(1000);
		atei.setInfcBRAmount(1000);
		atei.setInfcCCRAmount(1000);
		atei.setInfcFMBAmount(1000);
		atei.setInfcHIBAmount(1000);
		atei.setLapsADAMount(1000);
		atei.setLapsBRAmount(1000);
		atei.setOinsADAmount(1000);
		atei.setOinsBRAmount(1000);
		atei.setPendADAmount(1000);
		atei.setPendBRAmount(1000);
		atei.setSubjectPolicyId(refNum);
		atei.setTotalReinsAmount(1000);
		insertTotExtg(atei);
		return atei;
	}
	
	private void insertTotExtg(AbacusTotExtgInfo atei) throws Exception {
		StringBuffer sb = new StringBuffer();
		StringBuffer print = new StringBuffer();
		sb.append("insert into tot_extg_info (");
		sb.append("SUBJ_POL_ID, " +
				  "APP_BR_AMT, " +
				  "APP_AD_AMT, " +
				  "PEND_BR_AMT, " +
				  "PEND_AD_AMT, " +
				  "INFC_BR_AMT, " +
				  "INFC_AD_AMT, " +
				  "LAPS_BR_AMT, " +
				  "LAPS_AD_AMT, " +
				  "OINS_BR_AMT, " +
				  "OINS_AD_AMT, " +
				  "INFC_CCR_AMT, " +
				  "INFC_APDB_AMT, " +
				  "INFC_HIB_AMT, " +
				  "INFC_FMB_AMT, " +
				  "TOT_REINS_AMT ");
		print.append(sb.toString());
		sb.append(") values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
		print.append(") values (");
		PreparedStatement ps = conn.prepareStatement(sb.toString());
		ps.setString(1,atei.getSubjectPolicyId());
		print.append(printValue(atei.getSubjectPolicyId()));
		print.append(",");
		ps.setDouble(2,atei.getAppBRAmount());
		print.append(printValue(atei.getAppBRAmount()));
		print.append(",");
		ps.setDouble(3,atei.getAppADAmount());
		print.append(printValue(atei.getAppADAmount()));
		print.append(",");
		ps.setDouble(4,atei.getPendBRAmount());
		print.append(printValue(atei.getPendBRAmount()));
		print.append(",");
		ps.setDouble(5,atei.getPendADAmount());
		print.append(printValue(atei.getAppADAmount()));
		print.append(",");		
		ps.setDouble(6,atei.getInfcBRAmount());
		print.append(printValue(atei.getInfcBRAmount()));
		print.append(",");
		ps.setDouble(7,atei.getInfcADAmount());
		print.append(printValue(atei.getInfcADAmount()));
		print.append(",");
		ps.setDouble(8,atei.getLapsBRAmount());
		print.append(printValue(atei.getLapsBRAmount()));
		print.append(",");
		ps.setDouble(9,atei.getLapsADAMount());
		print.append(printValue(atei.getLapsADAMount()));
		print.append(",");
		ps.setDouble(10,atei.getOinsBRAmount());
		print.append(printValue(atei.getOinsBRAmount()));
		print.append(",");
		ps.setDouble(11,atei.getOinsADAmount());
		print.append(printValue(atei.getOinsADAmount()));
		print.append(",");
		ps.setDouble(12,atei.getInfcCCRAmount());
		print.append(printValue(atei.getInfcCCRAmount()));
		print.append(",");
		ps.setDouble(13,atei.getInfcAPDBAmount());
		print.append(printValue(atei.getInfcBRAmount()));
		print.append(",");
		ps.setDouble(14,atei.getInfcHIBAmount());
		print.append(printValue(atei.getInfcHIBAmount()));
		print.append(",");
		ps.setDouble(15,atei.getInfcFMBAmount());
		print.append(printValue(atei.getInfcFMBAmount()));
		print.append(",");
		ps.setDouble(16,atei.getTotalReinsAmount());
		print.append(printValue(atei.getTotalReinsAmount()));
		ps.executeUpdate();
		print.append(");");
		ps.close();
		file.write(print.toString() + "\n");
		System.out.println("Insert tot extg info for policy " + atei.getSubjectPolicyId());						
	}
	
	private ArrayList createRelatedPolicies(String refNum, int noOfPolicies) throws Exception {
		ArrayList list = new ArrayList();
		for (int i=0; i < noOfPolicies; i++) {
			AbacusRelatedPolicy arp = new AbacusRelatedPolicy();
			arp.setAccidentalDeathCoverageAmount(1000);
			arp.setADMultiplier(1);
			arp.setCoverageFaceAmount(1000);
			arp.setCoverageIssueEffectiveDate(DateHelper.parse("12Jan2004","ddMMMyyyy"));
			arp.setCoverageNumber("01");
			arp.setCoveragePlanId("1");
			arp.setCoverageSmokerCode(true);
			arp.setCoverageStatusCode("A");
			arp.setDecisionType("A");
			arp.setMedicalInd(true);
			arp.setPolicyCoverageSeqNum(getNextVal("seq_pol_cvg"));
			arp.setPolicyId(refNum);
			arp.setReinsuredAmount(10000);
			arp.setRelatedPolicyId("refNum");
			arp.setRelationShipCode("a");
			arp.setWPMultiplier(1);
			insertRelatedPolicy(arp);
			list.add(arp);
		}
		return list;
	}
	
	private void insertRelatedPolicy(AbacusRelatedPolicy arp) throws Exception {
		StringBuffer sb = new StringBuffer();
		StringBuffer print = new StringBuffer();
		
		sb.append("INSERT INTO policy_coverage_info (");
		sb.append("POLCVG_SEQ_NUM, " +
				  "SUBJ_POL_ID, " +
				  "REL_POL_ID, " +
				  "CVG_NUM, " +
				  "REL_CD, " +
				  "CVG_ISS_EFF_DT, " +
				  "CVG_STAT_CD, " +
				  "CVG_SMKR_CD, " +
				  "CVG_PLAN_ID, " +
				  "MEDIC_IND, " +
				  "CVG_FACE_AMT, " +
				  "DEC_TYP, " +
				  "AD_FACE_AMT, " +
				  "AD_MULT, " +
				  "WP_MULT, " +
				  "REINS_AMT ) " +
				  " values ( ");
		print.append(sb.toString());
		sb.append("?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
		PreparedStatement ps = conn.prepareStatement(sb.toString());
		ps.setLong(1,arp.getPolicyCoverageSeqNum());
		print.append(printValue(arp.getPolicyCoverageSeqNum()));
		print.append(", ");
		ps.setString(2,arp.getPolicyId());
		print.append(printValue(arp.getPolicyId()));
		print.append(", ");
		ps.setString(3,arp.getRelatedPolicyId());
		print.append(printValue(arp.getRelatedPolicyId()));
		print.append(", ");
		ps.setString(4,arp.getCoverageNumber());
		print.append(printValue(arp.getCoverageNumber()));
		print.append(", ");
		ps.setString(5,arp.getRelationShipCode());		
		print.append(printValue(arp.getRelationShipCode()));
		print.append(", ");
		ps.setDate(6,DateHelper.sqlDate(arp.getCoverageIssueEffectiveDate()));
		print.append(printValue(arp.getCoverageIssueEffectiveDate()));
		print.append(", ");
		ps.setString(7,arp.getCoverageStatusCode());
		print.append(printValue(arp.getCoverageStatusCode()));
		print.append(", ");
		ps.setBoolean(8,arp.isCoverageSmokerCode());
		print.append(printValue(arp.isCoverageSmokerCode()));
		print.append(", ");
		ps.setString(9,arp.getCoveragePlanId());
		print.append(printValue(arp.getCoveragePlanId()));
		print.append(", ");
		ps.setBoolean(10,arp.isMedicalInd());
		print.append(printValue(arp.isMedicalInd()));
		print.append(", ");
		ps.setDouble(11,arp.getCoverageFaceAmount());
		print.append(printValue(arp.getCoverageFaceAmount()));
		print.append(", ");
		ps.setString(12,arp.getDecisionType());
		print.append(printValue(arp.getDecisionType()));
		print.append(", ");
		ps.setDouble(13,arp.getAccidentalDeathCoverageAmount());
		print.append(printValue(arp.getAccidentalDeathCoverageAmount()));
		print.append(", ");
		ps.setInt(14,arp.getADMultiplier());
		print.append(printValue(arp.getADMultiplier()));
		print.append(", ");
		ps.setInt(15,arp.getWPMultiplier());
		print.append(printValue(arp.getWPMultiplier()));
		print.append(", ");
		ps.setDouble(16,arp.getReinsuredAmount());
		print.append(printValue(arp.getReinsuredAmount()));
		print.append(");");
		ps.executeUpdate();
		file.write(print.toString() + "\n");
		System.out.println("Inserted related policy " + arp.getRelatedPolicyId() + " for policy " + arp.getPolicyId());
		
	}
	
	private int getNextVal(String sequenceName) throws Exception {
		int res = 0;
		StringBuffer sb = new StringBuffer();
		sb.append("select " + sequenceName + ".nextval from dual");
		PreparedStatement ps = conn.prepareStatement(sb.toString());
		ResultSet rs = ps.executeQuery();
		if (rs.next()) {
			res = rs.getInt(1);
		}
		rs.close();
		return res;
	}
	
	/*
	 * this method will cause the system to wait for one second
	 */
	private void delay() {
		int sec = Calendar.getInstance().get(Calendar.SECOND);
		while (true) {
			int cursec =  Calendar.getInstance().get(Calendar.SECOND);
			if (cursec != sec){
				break;
			}
		}
	}
	
	
	private String getKey() {
		String key = "";
		key = Integer.toString(Calendar.getInstance().get(Calendar.MONTH)) + 
			Integer.toString(Calendar.getInstance().get(Calendar.DAY_OF_MONTH)) + 
			Integer.toString(Calendar.getInstance().get(Calendar.HOUR_OF_DAY)) + 
			Integer.toString(Calendar.getInstance().get(Calendar.MINUTE)) + 
			Integer.toString(Calendar.getInstance().get(Calendar.SECOND));
		return key;
	}
	
	private Connection getConnection() throws SQLException, ClassNotFoundException {
		Class.forName("oracle.jdbc.driver.OracleDriver");
		Connection conn = DriverManager.getConnection("jdbc:oracle:thin:@192.168.0.112:1521:oracle","iumabacus","iumabacus");
		return conn;
	}
	
	private String printValue(String value) {
		String res = "";
		if (value == null) {
			res = "null";
		} else {
			res = "'" + value + "'";
		}
		return res;
	}
	
	private String printValue(int value) {
		String res = "";
		res = Integer.toString(value);
		return res;
	}
	
	private String printValue(boolean value) {
		String res = "";
		if (value) {
			res = "'Y'";
		} else {
			res = "'N'";
		}
		return res;
	}
	
	private String printValue(java.util.Date value) {
		String res = "";
		if (value == null) {
			res = "null";
		} else {
			res = "'" + DateHelper.format(value,"dd-MMM-yyyy") + "'";
		}
		return res;
	}
	
	private String printValue(double value) {
		String res = "";
		res = Double.toString(value);
		return res;
	}
	

	public static void main(String args[]) {
		try {
			System.out.println("Start");
			AbacusTestUtil artu = new AbacusTestUtil(1,5,5);
			artu.getPolicyQueTestResults();
			System.out.println("End");
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
	
	

}
