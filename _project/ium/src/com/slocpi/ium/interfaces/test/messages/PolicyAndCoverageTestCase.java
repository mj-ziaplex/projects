/*
 * Created on Jan 4, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.interfaces.test.messages;

import java.util.ArrayList;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import com.slocpi.ium.data.PolicyDetailData;
import com.slocpi.ium.interfaces.messages.MessageConverter;
import com.slocpi.ium.util.DateHelper;

/**
 * @author daguila
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class PolicyAndCoverageTestCase extends TestCase {

	public PolicyAndCoverageTestCase(java.lang.String testName) {        
		super(testName);    
	}        

	public static Test suite() {        
		TestSuite suite = new TestSuite(PolicyAndCoverageTestCase.class);                
		return suite;    
	}	
	public static void main(java.lang.String[] args) {   
		junit.textui.TestRunner.run(suite());    
	}
	
	public void testPolicyCoverageMessage1() throws Exception {
		boolean result = false;
		MessageConverter mc = new MessageConverter();
		ArrayList list = mc.translateRelatedPolicyAndCoverageInfoMessage(buildPolicyCoverageMessage(1, "1"));
		ArrayList resList = getPolicyCoverageList(1,"1");
		if ((list != null) && (list.size() == 1)) {
			for (int i=0; i < list.size(); i++) {
				PolicyDetailData pdd = (PolicyDetailData)list.get(i);
				PolicyDetailData resPDD = (PolicyDetailData)resList.get(i);
				if (isPolicyCoverageDataEqual(pdd,resPDD)) {
					result = true;
				} else {
					result = false;
					break;
				}
				
			}
		}
		assertEquals("Did not translate Policy and Coverage message properly using 1 record.",true,result);
	}

	public void testPolicyCoverageMessage3() throws Exception {
		boolean result = false;
		MessageConverter mc = new MessageConverter();
		ArrayList list = mc.translateRelatedPolicyAndCoverageInfoMessage(buildPolicyCoverageMessage(3, "1"));
		ArrayList resList = getPolicyCoverageList(3,"1");
		if ((list != null) && (list.size() == 3)) {
			for (int i=0; i < list.size(); i++) {
				PolicyDetailData pdd = (PolicyDetailData)list.get(i);
				PolicyDetailData resPDD = (PolicyDetailData)resList.get(i);
				if (isPolicyCoverageDataEqual(pdd,resPDD)) {
					result = true;
				} else {
					result = false;
					break;
				}
				
			}
		}
		assertEquals("Did not translate Policy and Coverage message properly using 3 record.",true,result);
	}	

	public void testPolicyCoverageMessage100() throws Exception {
		boolean result = false;
		MessageConverter mc = new MessageConverter();
		ArrayList list = mc.translateRelatedPolicyAndCoverageInfoMessage(buildPolicyCoverageMessage(100, "1"));
		ArrayList resList = getPolicyCoverageList(100,"1");
		if ((list != null) && (list.size() ==100)) {
			for (int i=0; i < list.size(); i++) {
				PolicyDetailData pdd = (PolicyDetailData)list.get(i);
				PolicyDetailData resPDD = (PolicyDetailData)resList.get(i);
				if (isPolicyCoverageDataEqual(pdd,resPDD)) {
					result = true;
				} else {
					result = false;
					break;
				}
				
			}
		}
		assertEquals("Did not translate Policy and Coverage message properly using 3 record.",true,result);
	}
	private boolean isPolicyCoverageDataEqual(PolicyDetailData obj1, PolicyDetailData obj2) {
		boolean res = false;
		if ((obj1 != null) && (obj2 != null)) {
			// TODO: verify if clientId is not set
			if (
				(obj1.getADBFaceAmount() == obj2.getADBFaceAmount()) &&
				(obj1.getADMultiplier() == obj2.getADMultiplier()) &&
				(obj1.getCoverageNumber() == obj2.getCoverageNumber()) &&
				(obj1.getCoverageStatus().equals(obj2.getCoverageStatus())) &&
				(obj1.getDecision().equals(obj2.getDecision())) &&
				(obj1.getFaceAmount() == obj2.getFaceAmount()) &&
				(DateHelper.isEqual(obj1.getIssueDate(),obj2.getIssueDate())) &&
				(obj1.getPlanCode().equals(obj2.getPlanCode())) &&
				(obj1.getPolicyNumber().equals(obj2.getPolicyNumber())) &&
				(obj1.getReferenceNumber().equals(obj2.getReferenceNumber())) &&
				(obj1.getReinsuredAmount() == obj2.getReinsuredAmount()) &&
				(obj1.getRelationship().equals(obj2.getRelationship())) &&
				(obj1.getWPMultiplier() == obj2.getWPMultiplier())
				) {
				res = true;
			}
		} else if ((obj1 == null) && (obj2 == null)) {
			res = true;
		}
		return res;
	}
	private ArrayList getPolicyCoverageList(int numOfRecs, String refNum) {
		ArrayList list = new ArrayList();
		for (int i=0; i < numOfRecs; i++) {
			PolicyDetailData pdd = new PolicyDetailData();
			pdd.setReferenceNumber(refNum);
			pdd.setReinsuredAmount(1);
			pdd.setWPMultiplier(1);
			pdd.setADMultiplier(1);
			pdd.setADBFaceAmount(1);
			pdd.setDecision("hh");
			pdd.setFaceAmount(1);
			pdd.setMedical(false);
			pdd.setPlanCode(Integer.toString(i + 1));
			pdd.setSmoker(true);
			pdd.setCoverageStatus("AA");
			pdd.setIssueDate(DateHelper.parse("2003-12-12","yyyy-MM-dd"));
			pdd.setRelationship("FA");
			pdd.setCoverageNumber(i + 1);
			pdd.setPolicyNumber(Integer.toString(i + 1));
			list.add(pdd);
		}
		return list;
	}
	
	private StringBuffer buildPolicyCoverageMessage(int numOfRecs, String refNum) {
		StringBuffer res = new StringBuffer("<?xml version=\"1.0\" encoding=\"UTF-8\" ?>" + 
						"<ROOT>" + 
						"<MSG_ID>QPRP</MSG_ID>" +
						"<CONF_CD>00</CONF_CD>" + 
						"<SUBJ_POL_ID>" + refNum + "</SUBJ_POL_ID>");
		for (int i=0; i< numOfRecs; i++) {						
			res.append("<REL_POL_INFO>" +
					"<REL_POL_ID>" + (i + 1) + "</REL_POL_ID>" +
					"<CVG_NUM>" + (i + 1) + "</CVG_NUM>" +
					"<REL_CD>FA</REL_CD>" +
					"<CVG_ISS_EFF_DT>2003-12-12</CVG_ISS_EFF_DT>" +
					"<CVG_STAT_CD>AA</CVG_STAT_CD>" +
					"<CVG_SMKR_CD>y</CVG_SMKR_CD>" +
					"<CVG_PLAN_ID>" + (i + 1) + "</CVG_PLAN_ID>" +
					"<MEDIC_IND>n</MEDIC_IND>" +
					"<CVG_FACE_AMT>1</CVG_FACE_AMT>" +
					"<DEC_TYP>hh</DEC_TYP>" +
					"<AD_FACE_AMT>1</AD_FACE_AMT>" +
					"<AD_MULT>1</AD_MULT>" +
					"<WP_MULT>1</WP_MULT>" + 
					"<REINS_AMT>1</REINS_AMT>" +              
					"</REL_POL_INFO>");
		}
		 
		res.append("</ROOT>");		
		return res;
	}

}
