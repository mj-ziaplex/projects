/*
 * Created on Jan 5, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.interfaces.test.messages;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import com.slocpi.ium.data.IUMMessageData;
import com.slocpi.ium.interfaces.messages.MessageConverter;

/**
 * @author daguila
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class AutoSettleTestCase extends TestCase {

	public AutoSettleTestCase(java.lang.String testName) {        
		super(testName);    
	}        

	public static Test suite() {        
		TestSuite suite = new TestSuite(AutoSettleTestCase.class);                
		return suite;    
	}	
	
	public static void main(java.lang.String[] args) {   
		junit.textui.TestRunner.run(suite());    
	}
	
	public void testAutoSettle00() throws Exception {
		MessageConverter mc = new MessageConverter();
		IUMMessageData asd = mc.translateAutoSettleMessage(buildAutoSettleMessage("00","success","",""));
		IUMMessageData resASD = getAutoSettleData("00","success","","");
		assertEquals("Did not translate Auto Settle message properly using a successful transaction.",true,isAutoSettleEqual(asd,resASD));
	}
	
	private boolean isAutoSettleEqual(IUMMessageData obj1, IUMMessageData obj2) {
		boolean res = false;
		if ((obj1 != null) && (obj2 != null)) {
			if (
				(obj1.getConfirmationCode().equals(obj2.getConfirmationCode())) &&
					(
						(
						(obj1.getMessageLine1() != null) && (obj2.getMessageLine1() != null) && (obj1.getMessageLine1().equals(obj2.getMessageLine1()))
						) 
						||
						(
						(obj1.getMessageLine1() == null) && (obj2.getMessageLine1() == null)
						) 
					)
					&&
					(
						(
						(obj1.getMessageLine2() != null) && (obj2.getMessageLine2() != null) && (obj1.getMessageLine2().equals(obj2.getMessageLine2()))
						) 
						||
						(
						(obj1.getMessageLine2() == null) && (obj2.getMessageLine2() == null)
						) 
					)
					&&
					(
						(
						(obj1.getMessageLine3() != null) && (obj2.getMessageLine3() != null) && (obj1.getMessageLine2().equals(obj2.getMessageLine3()))
						) 
						||
						(
						(obj1.getMessageLine3() == null) && (obj2.getMessageLine3() == null)
						) 
					)					
				) {
					
				res = true;
			}
		} else if ((obj1 == null) && (obj2 == null)) {
			res = true;
		}
		return res;
	}
	
	private IUMMessageData getAutoSettleData(String confCode, String msgLine1, String msgLine2, String msgLine3) {
		IUMMessageData asd = new IUMMessageData();
		asd.setConfirmationCode(confCode);
		asd.setMessageLine1(msgLine1);
		asd.setMessageLine2(msgLine2);
		asd.setMessageLine3(msgLine3);
		return asd;
	}
	
	private StringBuffer buildAutoSettleMessage(String confCode, String msgLine1, String msgLine2, String msgLine3) {
		StringBuffer res = new StringBuffer().append("<?xml version=\"1.0\" encoding=\"UTF-8\" ?>" + 
						"<ROOT>" + 
						"<MSG_ID> </MSG_ID>" +
						"<CONF_CD>" + confCode + "</CONF_CD>" +
						"<MSG1>" + msgLine1 + "</MSG1>" +
						"<MSG2>" + msgLine2 + "</MSG2>" +   
						"<MSG3>" + msgLine3 + "</MSG3>" +
						"</ROOT>"); 		
		return res;
	}
}
