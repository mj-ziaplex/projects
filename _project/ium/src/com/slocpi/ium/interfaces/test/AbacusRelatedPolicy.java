/*
 * Created on Jan 25, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.interfaces.test;

import java.util.Date;

/**
 * @author daguila
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class AbacusRelatedPolicy {
	
	private long   policyCoverageSeqNum;
	private String policyId;
	private String relatedPolicyId;
	private String coverageNumber;
	private String relationShipCode;
	private Date coverageIssueEffectiveDate;
	private String coverageStatusCode;
	private boolean coverageSmokerCode;
	private String coveragePlanId;
	private boolean medicalInd;
	private double coverageFaceAmount;
	private String	decisionType;
	private double accidentalDeathCoverageAmount;
	private int		ADMultiplier;
	private int		WPMultiplier;
	private double reinsuredAmount;
	
	/**
	 * @return
	 */
	public double getAccidentalDeathCoverageAmount() {
		return accidentalDeathCoverageAmount;
	}

	/**
	 * @return
	 */
	public int getADMultiplier() {
		return ADMultiplier;
	}

	/**
	 * @return
	 */
	public double getCoverageFaceAmount() {
		return coverageFaceAmount;
	}

	/**
	 * @return
	 */
	public Date getCoverageIssueEffectiveDate() {
		return coverageIssueEffectiveDate;
	}

	/**
	 * @return
	 */
	public String getCoverageNumber() {
		return coverageNumber;
	}

	/**
	 * @return
	 */
	public String getCoveragePlanId() {
		return coveragePlanId;
	}

	/**
	 * @return
	 */
	public boolean isCoverageSmokerCode() {
		return coverageSmokerCode;
	}


	/**
	 * @return
	 */
	public String getDecisionType() {
		return decisionType;
	}

	/**
	 * @return
	 */
	public boolean isMedicalInd() {
		return medicalInd;
	}

	/**
	 * @return
	 */
	public String getPolicyId() {
		return policyId;
	}

	/**
	 * @return
	 */
	public double getReinsuredAmount() {
		return reinsuredAmount;
	}

	/**
	 * @return
	 */
	public String getRelatedPolicyId() {
		return relatedPolicyId;
	}

	/**
	 * @return
	 */
	public String getRelationShipCode() {
		return relationShipCode;
	}

	/**
	 * @return
	 */
	public int getWPMultiplier() {
		return WPMultiplier;
	}

	/**
	 * @param d
	 */
	public void setAccidentalDeathCoverageAmount(double d) {
		accidentalDeathCoverageAmount = d;
	}

	/**
	 * @param i
	 */
	public void setADMultiplier(int i) {
		ADMultiplier = i;
	}

	/**
	 * @param d
	 */
	public void setCoverageFaceAmount(double d) {
		coverageFaceAmount = d;
	}

	/**
	 * @param string
	 */
	public void setCoverageIssueEffectiveDate(Date string) {
		coverageIssueEffectiveDate = string;
	}

	/**
	 * @param string
	 */
	public void setCoverageNumber(String string) {
		coverageNumber = string;
	}

	/**
	 * @param string
	 */
	public void setCoveragePlanId(String string) {
		coveragePlanId = string;
	}

	/**
	 * @param b
	 */
	public void setCoverageSmokerCode(boolean b) {
		coverageSmokerCode = b;
	}


	/**
	 * @param string
	 */
	public void setDecisionType(String string) {
		decisionType = string;
	}

	/**
	 * @param b
	 */
	public void setMedicalInd(boolean b) {
		medicalInd = b;
	}

	/**
	 * @param string
	 */
	public void setPolicyId(String string) {
		policyId = string;
	}

	/**
	 * @param d
	 */
	public void setReinsuredAmount(double d) {
		reinsuredAmount = d;
	}

	/**
	 * @param string
	 */
	public void setRelatedPolicyId(String string) {
		relatedPolicyId = string;
	}

	/**
	 * @param string
	 */
	public void setRelationShipCode(String string) {
		relationShipCode = string;
	}

	/**
	 * @param i
	 */
	public void setWPMultiplier(int i) {
		WPMultiplier = i;
	}

	/**
	 * @return
	 */
	public long getPolicyCoverageSeqNum() {
		return policyCoverageSeqNum;
	}

	/**
	 * @param l
	 */
	public void setPolicyCoverageSeqNum(long l) {
		policyCoverageSeqNum = l;
	}

	/**
	 * @return
	 */
	public String getCoverageStatusCode() {
		return coverageStatusCode;
	}

	/**
	 * @param string
	 */
	public void setCoverageStatusCode(String string) {
		coverageStatusCode = string;
	}

}
