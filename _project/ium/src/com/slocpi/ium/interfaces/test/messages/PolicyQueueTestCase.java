/*
 * Created on Dec 17, 2003
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.interfaces.test.messages;

import java.util.ArrayList;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import com.slocpi.ium.data.AssessmentRequestData;
import com.slocpi.ium.data.ClientData;
import com.slocpi.ium.data.UserProfileData;
import com.slocpi.ium.interfaces.messages.MessageConverter;

/**
 * @author daguila
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class PolicyQueueTestCase extends TestCase{

	public PolicyQueueTestCase(java.lang.String testName) {        
		super(testName);    
	}        

	public static Test suite() {        
		TestSuite suite = new TestSuite(PolicyQueueTestCase.class);                
		return suite;    
	}	
	public static void main(java.lang.String[] args) {   
		junit.textui.TestRunner.run(suite());    
	}
	
	/**
	 * This method will test the parsing of the Policy Que Message from the 
	 * Messaging Server.
	 * This method will test the Policy Message containing 1 record.
	 * @throws Exception
	 */	
	public void testPolicyQueMessage1() throws Exception {
		boolean result = false;
		MessageConverter mb = new MessageConverter();
		ArrayList list = mb.translatePolicyQueMessage(buildPolicyQueMessage(1));
		ArrayList resList = getPolicyQueueInfoList(1);
		if ((list != null) && (list.size() == 1)) {
			for (int i=0; i < list.size(); i++) {
				AssessmentRequestData ard = (AssessmentRequestData)list.get(i);
				if (!isAssessmentRequestEqual(ard,(AssessmentRequestData)resList.get(i))) {
					result = false;
					break;
				} else {
					result = true;
				}
				// TODO: add more checking
				
			}
		}
		assertEquals("Policy Queue Info did not translate the message properly using a single record.", true,result);
	}

	/**
	 * This method will test the parsing of the Policy Que Message from the 
	 * Messaging Server.
	 * This method will test the Policy Message containing 3 record.
	 * @throws Exception
	 */	
	public void testPolicyQueMessage3() throws Exception {
		boolean result = false;
		MessageConverter mb = new MessageConverter();
		ArrayList list = mb.translatePolicyQueMessage(buildPolicyQueMessage(3));
		ArrayList resList = getPolicyQueueInfoList(3);
		if ((list != null) && (list.size() == 3)) {
			for (int i=0; i < list.size(); i++) {
				AssessmentRequestData ard = (AssessmentRequestData)list.get(i);
				if (!isAssessmentRequestEqual(ard,(AssessmentRequestData)resList.get(i))) {
					result = false;
					break;
				} else {
					result = true;
				}
				// TODO: add more checking
				
			}
		}
		assertEquals("Policy Queue Info did not translate the message properly using 3 records.", true,result);
	}	

	/**
	 * This method will test the parsing of the Policy Que Message from the 
	 * Messaging Server.
	 * This method will test the Policy Message containing 50 record.
	 * @throws Exception
	 */	
	public void testPolicyQueMessage50() throws Exception {
		boolean result = false;
		MessageConverter mb = new MessageConverter();
		ArrayList list = mb.translatePolicyQueMessage(buildPolicyQueMessage(50));
		ArrayList resList = getPolicyQueueInfoList(50);
		if ((list != null) && (list.size() == 50)) {
			for (int i=0; i < list.size(); i++) {
				AssessmentRequestData ard = (AssessmentRequestData)list.get(i);
				if (!isAssessmentRequestEqual(ard,(AssessmentRequestData)resList.get(i))) {
					result = false;
					break;
				} else {
					result = true;
				}
				// TODO: add more checking
				
			}
		}
		assertEquals("Policy Queue Info did not translate the message properly using 3 records.", true,result);
	}	

	private boolean isAssessmentRequestEqual(AssessmentRequestData obj1, AssessmentRequestData obj2) {
		boolean result = false;
		if ((obj1 != null) && (obj2 != null)) {
			// check values
			if (
				(obj1.getReferenceNumber().equals(obj2.getReferenceNumber())) &&
				(obj1.getAmountCovered() == obj2.getAmountCovered()) &&
				(obj1.getPremium() == obj2.getPremium())
				)  {
					
					result = true;
				}
		} else if ((obj1 == null) && (obj2 == null)) {
			result = true;
		}
		return result;
	}
	
	private ArrayList getPolicyQueueInfoList(int numOfRecs) {
		ArrayList list = new ArrayList();
		for (int i=0; i < numOfRecs; i++) {
			AssessmentRequestData ard = new AssessmentRequestData();
			ClientData cd = new ClientData();
			ard.setInsured(cd);
			// TODO: add more fields
			ard.setReferenceNumber(String.valueOf(i + 1));
			ard.setAmountCovered((i + 1) * 1000);
			ard.setPremium((i + 1) * 100);
			UserProfileData upd = new UserProfileData();
			upd.setUserId("nbuser" + (i + 1));
			ard.setAssignedTo(upd);
			cd.setLastName("client" + i + 1);
			cd.setGivenName("client" + i + 1);
			cd.setMiddleName("client" + i + 1);
			list.add(ard);
		}
		return list;
	}
	
	
	
	private StringBuffer buildPolicyQueMessage(int numOfRecs) {
		StringBuffer res = new StringBuffer();
		res.append("<?xml version=\"1.0\" encoding=\"UTF-8\" ?>" + 
				"<ROOT>" + 
				"<MSG_ID> </MSG_ID>" + 
				"<CONF_CD>00</CONF_CD>" + "<POL_QUEUE_INFO>");

		for (int i=0; i < numOfRecs; i++) {
			res.append("<POL_DATA>" +
						"<POL_ID>" + (i + 1) + "</POL_ID>" +
						"<CVG_FACE_AMT>" + ((i + 1) * 1000) + "</CVG_FACE_AMT>" +
						"<POL_MPREM_AMT>" + ((i + 1) * 100)  + "</POL_MPREM_AMT>");
			res.append("<APP_RECV_DT>2003-12-24</APP_RECV_DT>" +
						"<NB_CNTCT_USER_ID>nbuser1</NB_CNTCT_USER_ID>" +
						"<UW_USER_ID>uw1</UW_USER_ID>" +
						"<INSR_SUR_NM>client" + (i + 1) + "</INSR_SUR_NM>" +
						"<INSR_GIV_NM>client" + (i + 1) + "</INSR_GIV_NM>" +
						"<INSR_MID_NM>client" + (i + 1) + "</INSR_MID_NM>" +
						"<AGT_ID>agt" + (i + 1) + "</AGT_ID>" +
						"<SOFF_ID>brn" + (i + 1) + "</SOFF_ID>" +
					    "</POL_DATA>");
		}
		res.append("</POL_QUEUE_INFO></ROOT>");
		 		
		return res;
	}
}
