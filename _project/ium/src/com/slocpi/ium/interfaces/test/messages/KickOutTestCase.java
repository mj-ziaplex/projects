/*
 * Created on Jan 2, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.interfaces.test.messages;

import java.util.ArrayList;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import com.slocpi.ium.data.KickOutMessageData;
import com.slocpi.ium.interfaces.messages.MessageConverter;

/**
 * @author daguila
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class KickOutTestCase extends TestCase {

	public KickOutTestCase(java.lang.String testName) {        
		super(testName);    
	}        

	public static Test suite() {        
		TestSuite suite = new TestSuite(KickOutTestCase.class);                
		return suite;    
	}	
	public static void main(java.lang.String[] args) {   
		junit.textui.TestRunner.run(suite());    
	}
	
	public void testKickOutMessage1() throws Exception {
		boolean result = false;
		MessageConverter mb = new MessageConverter();
		ArrayList list = mb.translateKickOutMessage(buildKickOutMessage(1,"1"));
		ArrayList resList = getKickOutMessageList(1,"1");
		if ((list != null) && (list.size() == 1)) {
			for (int i=0; i < list.size(); i++) {
				KickOutMessageData komd1 = (KickOutMessageData)list.get(i);
				KickOutMessageData komd2 = (KickOutMessageData)resList.get(i);
				if (!isKickOutMessageDataEqual(komd1,komd2)) {
					result = false;
					break;
				} else {
					result = true;
				}
			}
		}
		assertEquals("Did not translate Kick Out message properly using 1 record.",true,result);
	}
	
	private boolean isKickOutMessageDataEqual(KickOutMessageData obj1, KickOutMessageData obj2) {
		boolean res = false;
		if ((obj1 != null) && (obj2 != null)) {
			if (
				(obj1.getClientId().equals(obj2.getClientId())) &&
				(obj1.getFailResponse().equals(obj2.getFailResponse())) &&
				(obj1.getMessageText().equals(obj2.getMessageText())) &&
				(obj1.getReferenceNumber().equals(obj2.getReferenceNumber())) &&
				(obj1.getSequenceNumber() == obj2.getSequenceNumber())
			   ) {
			   	res = true;
			}
		} else if ((obj1 == null) && (obj2 == null)) {
			res = true;
		}
		return res; 
	}
	private ArrayList getKickOutMessageList(int numOfRecs,String refNo) {
		ArrayList list = new ArrayList();
		for (int i=0; i < numOfRecs; i++ ) {
			KickOutMessageData komd = new KickOutMessageData();
			komd.setReferenceNumber(refNo);
			komd.setClientId("Client" + (i + 1));
			komd.setFailResponse("Resp" + (i + 1));
			komd.setMessageText("Message" + (i + 1));
			komd.setSequenceNumber(i + 1);
			list.add(komd);
		}
		return list;
	}
	private StringBuffer buildKickOutMessage(int numOfRecs, String refNo) {
		StringBuffer res = new StringBuffer("<?xml version=\"1.0\" encoding=\"UTF-8\" ?>" +
					"<ROOT>" +
					"<MSG_ID> </MSG_ID>" + 
					"<CONF_CD>00</CONF_CD>" +
					"<SUBJ_POL_ID>" + refNo + "</SUBJ_POL_ID>");
		for (int i=0; i < numOfRecs; i++) {
			res.append("<CCKO_DATA>" +
					"<CLI_ID>Client" + (i + 1) + "</CLI_ID>" +
					"<SEQ_NUM>" + (i + 1) + "</SEQ_NUM>" +
					"<MSG_TXT>Message" + (i + 1) + "</MSG_TXT>" +
					"<MSG_RESP_TXT>Resp" + (i + 1) + "</MSG_RESP_TXT>" +
					"</CCKO_DATA>");     
		}
		res.append("</ROOT>");		
		return res;
	}
	
}
