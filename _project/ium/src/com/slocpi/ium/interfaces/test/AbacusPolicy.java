/*
 * Created on Jan 8, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.interfaces.test;

import java.util.ArrayList;
import java.util.Date;

/**
 * @author daguila
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class AbacusPolicy {
	
	private ArrayList kickOutMessages;
	
	private String 	policyId;
	private String 	policyStatus;
	private int		policySeqNum;
	private double	policyCoverageFaceAmount;
	private double 	policyMPremiumAmount;
	private Date	policyReceivedDate;
	private String	NBContactUserId;
	private String	UWUserId;
	private String	insuredSurname;
	private String	insuredGivenName;
	private String	inusredMiddleName;
	private String	agentId;
	private String	officeId;		
	private AbacusCDS cds;
	private AbacusMortalityRating insrMortRating;
	private AbacusMortalityRating ownMortRating;
	private AbacusClientInfo insrClientInfo;
	private AbacusClientInfo ownClientInfo;
	private ArrayList relatedPolicies;
	
	

	/**
	 * @return
	 */
	public String getPolicyId() {
		return policyId;
	}

	/**
	 * @return
	 */
	public String getPolicyStatus() {
		return policyStatus;
	}

	/**
	 * @param string
	 */
	public void setPolicyId(String string) {
		policyId = string;
	}

	/**
	 * @param string
	 */
	public void setPolicyStatus(String string) {
		policyStatus = string;
	}

	/**
	 * @return
	 */
	public String getAgentId() {
		return agentId;
	}

	/**
	 * @return
	 */
	public String getInsuredGivenName() {
		return insuredGivenName;
	}

	/**
	 * @return
	 */
	public String getInsuredSurname() {
		return insuredSurname;
	}

	/**
	 * @return
	 */
	public String getInusredMiddleName() {
		return inusredMiddleName;
	}

	/**
	 * @return
	 */
	public String getNBContactUserId() {
		return NBContactUserId;
	}

	/**
	 * @return
	 */
	public String getOfficeId() {
		return officeId;
	}

	/**
	 * @return
	 */
	public double getPolicyCoverageFaceAmount() {
		return policyCoverageFaceAmount;
	}

	/**
	 * @return
	 */
	public double getPolicyMPremiumAmount() {
		return policyMPremiumAmount;
	}

	/**
	 * @return
	 */
	public Date getPolicyReceivedDate() {
		return policyReceivedDate;
	}

	/**
	 * @return
	 */
	public int getPolicySeqNum() {
		return policySeqNum;
	}

	/**
	 * @return
	 */
	public String getUWUserId() {
		return UWUserId;
	}

	/**
	 * @param string
	 */
	public void setAgentId(String string) {
		agentId = string;
	}

	/**
	 * @param string
	 */
	public void setInsuredGivenName(String string) {
		insuredGivenName = string;
	}

	/**
	 * @param string
	 */
	public void setInsuredSurname(String string) {
		insuredSurname = string;
	}

	/**
	 * @param string
	 */
	public void setInusredMiddleName(String string) {
		inusredMiddleName = string;
	}

	/**
	 * @param string
	 */
	public void setNBContactUserId(String string) {
		NBContactUserId = string;
	}

	/**
	 * @param string
	 */
	public void setOfficeId(String string) {
		officeId = string;
	}

	/**
	 * @param d
	 */
	public void setPolicyCoverageFaceAmount(double d) {
		policyCoverageFaceAmount = d;
	}

	/**
	 * @param d
	 */
	public void setPolicyMPremiumAmount(double d) {
		policyMPremiumAmount = d;
	}

	/**
	 * @param date
	 */
	public void setPolicyReceivedDate(Date date) {
		policyReceivedDate = date;
	}

	/**
	 * @param i
	 */
	public void setPolicySeqNum(int i) {
		policySeqNum = i;
	}

	/**
	 * @param string
	 */
	public void setUWUserId(String string) {
		UWUserId = string;
	}

	/**
	 * @return
	 */
	public ArrayList getKickOutMessages() {
		return kickOutMessages;
	}

	/**
	 * @param list
	 */
	public void setKickOutMessages(ArrayList list) {
		kickOutMessages = list;
	}

	/**
	 * @return
	 */
	public AbacusCDS getCds() {
		return cds;
	}

	/**
	 * @param abacusCDS
	 */
	public void setCds(AbacusCDS abacusCDS) {
		cds = abacusCDS;
	}

	/**
	 * @return
	 */
	public AbacusMortalityRating getInsrMortRating() {
		return insrMortRating;
	}

	/**
	 * @return
	 */
	public AbacusMortalityRating getOwnMortRating() {
		return ownMortRating;
	}

	/**
	 * @param rating
	 */
	public void setInsrMortRating(AbacusMortalityRating rating) {
		insrMortRating = rating;
	}

	/**
	 * @param rating
	 */
	public void setOwnMortRating(AbacusMortalityRating rating) {
		ownMortRating = rating;
	}

	/**
	 * @return
	 */
	public AbacusClientInfo getInsrClientInfo() {
		return insrClientInfo;
	}

	/**
	 * @return
	 */
	public AbacusClientInfo getOwnClientInfo() {
		return ownClientInfo;
	}

	/**
	 * @param info
	 */
	public void setInsrClientInfo(AbacusClientInfo info) {
		insrClientInfo = info;
	}

	/**
	 * @param info
	 */
	public void setOwnClientInfo(AbacusClientInfo info) {
		ownClientInfo = info;
	}

	/**
	 * @return
	 */
	public ArrayList getRelatedPolicies() {
		return relatedPolicies;
	}

	/**
	 * @param list
	 */
	public void setRelatedPolicies(ArrayList list) {
		relatedPolicies = list;
	}

}
