/*
 * Created on Apr 27, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.interfaces.mib;

import java.util.ArrayList;
import java.util.TimerTask;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.slocpi.ium.ExceptionDetail;
import com.slocpi.ium.ExceptionLog;
import com.slocpi.ium.ExceptionLogger;
import com.slocpi.ium.util.CodeHelper;

/**
 * @author ypojol
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class MIBThread extends TimerTask {
	private static final Logger LOGGER = LoggerFactory.getLogger(MIBThread.class);
	/**
	 * 
	 */
	public MIBThread() {
		super();
		// TODO Auto-generated constructor stub
	}

	/* (non-Javadoc)
	 * @see java.lang.Runnable#run()
	 */
	public void run() {
		
		LOGGER.info("run start");
		try{
			
			MIBController mib = new MIBController();
			mib.export();	
			
		}catch(Exception e){
			LOGGER.error(CodeHelper.getStackTrace(e));
			ExceptionLog el= new ExceptionLog();
			el.setRecordType("");
			el.setSystemInterface("MIB");
			ArrayList arr = new ArrayList();
			arr.add(createExceptionDetail(e.getMessage()));
			el.setDetails(arr);
			ExceptionLogger errorLogger = new ExceptionLogger();
			errorLogger.logError(el);
		}finally{
			this.cancel();
		}
		LOGGER.info("run end");
		
	}

	private ExceptionDetail createExceptionDetail(String message){
		
		LOGGER.info("createExceptionDetail start");
		ExceptionDetail ed = new ExceptionDetail();
		ed.setMessage(message);
		LOGGER.info("createExceptionDetail end");
		return ed;
	}
	
	
}
