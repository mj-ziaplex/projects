/*
 * Created on Jan 24, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.interfaces.mib;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.ResourceBundle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.slocpi.ium.ExceptionDetail;
import com.slocpi.ium.ExceptionLog;
import com.slocpi.ium.ExceptionLogger;
import com.slocpi.ium.data.dao.MIBExportDAO;
import com.slocpi.ium.data.util.DataSourceProxy;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.DateHelper;
import com.slocpi.ium.util.IUMConstants;

/**
 * @author daguila
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class MIBController {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(MIBController.class);
	String export_directory;
	String impairment_file;
	String member_file;
	String transaction_file;
	String file_extension;
	/**
	 * 
	 */
	public MIBController() {
		
		super();
		LOGGER.info("MIBController constructor");
		
		String strDate = DateHelper.format(new Date(), "ddMMMyyyyHHmmss");
		ResourceBundle rb = ResourceBundle.getBundle(IUMConstants.IUM_CONFIG);
		export_directory = rb.getString("export_directory");
		impairment_file = rb.getString("impairment_file");
		member_file = rb.getString("member_file");
		transaction_file = rb.getString("transaction_file");
		file_extension = rb.getString("file_extension");
		
		LOGGER.info("MIBController end");
	}

	
	public void export(Date start, Date end, String userId) {
	
		LOGGER.info("export start 1");
		Connection conn = null;
		 try{
			conn = new DataSourceProxy().getConnection();
			MIBExportDAO dao = new MIBExportDAO(conn);
			HashMap map = dao.getMemberImpairment();
			ArrayList arrImps = (ArrayList)map.get("MEMBER_IMPAIRMENTS");
			ArrayList arrDtls = (ArrayList)map.get("MEMBER_DETAILS");
			ArrayList arrTrans = (ArrayList)map.get("TRANSACTION_DETAILS");
			ArrayList arrImpIds = (ArrayList)map.get("IMP_IDS");
			
			String strDate = DateHelper.format(new Date(), "ddMMMyyyy");
			writeFile(impairment_file + strDate + "." + file_extension, arrImps);
			writeFile(member_file + strDate + "." + file_extension, arrDtls);
			writeFile(transaction_file + strDate + "." + file_extension, arrTrans);
			conn.setAutoCommit(false);
			dao.updateExportedImpairment(arrImpIds);
			conn.commit();
		 }catch(SQLException e){
			 LOGGER.error(CodeHelper.getStackTrace(e));
			try {
				if(conn != null){
					conn.rollback();
				}
			} catch (SQLException e1) {
				LOGGER.error(CodeHelper.getStackTrace(e1));
			}
		 }finally{
			closeConnection(conn);
		 }
		 LOGGER.info("export end 1");

	}
	
	public void export() {
		
		LOGGER.info("export start 2");
		Connection conn = null;
		 try{
			conn = new DataSourceProxy().getConnection();
			MIBExportDAO dao = new MIBExportDAO(conn);
			HashMap map = dao.getMemberImpairment();
			ArrayList arrImps = (ArrayList)map.get("MEMBER_IMPAIRMENTS");
			ArrayList arrDtls = (ArrayList)map.get("MEMBER_DETAILS");
			ArrayList arrTrans = (ArrayList)map.get("TRANSACTION_DETAILS");
			ArrayList arrImpIds = (ArrayList)map.get("IMP_IDS");
			
			String strDate = DateHelper.format(new Date(), "ddMMMyyyyHHmmss");
			writeFile(impairment_file + strDate + "." + file_extension, arrImps);
			writeFile(member_file + strDate + "." + file_extension, arrDtls);
			writeFile(transaction_file + strDate + "." + file_extension, arrTrans);
			conn.setAutoCommit(false);
			dao.updateExportedImpairment(arrImpIds);
			conn.commit();
			
		 }catch(SQLException e){
			 LOGGER.error(CodeHelper.getStackTrace(e));
			try {
				if(conn != null){
					conn.rollback();
				}
			} catch (SQLException e1) {
				LOGGER.error(CodeHelper.getStackTrace(e1));
			}
			
			ExceptionLog el= new ExceptionLog();
			el.setRecordType("");
			el.setSystemInterface("MIB");
			ArrayList arr = new ArrayList(); 
			arr.add(createExceptionDetail(e.getMessage()));
			el.setDetails(arr);
			ExceptionLogger errorLogger = new ExceptionLogger();
			errorLogger.logError(el);
		 }finally{
			closeConnection(conn);
		 }
		 LOGGER.info("export end 2");

	}
	/*
	 * 
	 */
	private void writeFile(String filename, ArrayList list){
		
		LOGGER.info("writeFile start");
		try{
			File f = new File(export_directory + "/" +filename);
			FileWriter fw = new FileWriter(f);
			for(int i=0; i<list.size(); i++){
				fw.write((String)list.get(i));
				fw.write(System.getProperty("line.separator"));
			}
			fw.close();
		}catch(IOException ioe){
			LOGGER.error(CodeHelper.getStackTrace(ioe));
		}
		LOGGER.info("writeFile end");
	}
	
	
	private void closeConnection(Connection conn){
		
		
		if (null != conn){
			try {
				conn.close();
			} catch (SQLException sqlE) {
				LOGGER.error(CodeHelper.getStackTrace(sqlE));
			}
		}
		
	}
	
	public Timestamp getJobSchedule(String sched_type){
		
		LOGGER.info("getJobSchedule start");
		Timestamp ts = null;
		Connection conn = null;
		try{
			conn = new DataSourceProxy().getConnection();
			MIBExportDAO dao = new MIBExportDAO(conn);
			ts = dao.getJobSchedule(sched_type);
		}catch(SQLException e){
			LOGGER.error(CodeHelper.getStackTrace(e));
 		}finally{
			closeConnection(conn);
 		}
		LOGGER.info("getJobSchedule end");
		return ts;
	}
	
	private ExceptionDetail createExceptionDetail(String message){
		
		LOGGER.info("createExceptionDetail start");
		ExceptionDetail ed = new ExceptionDetail();
		ed.setMessage(message);
		LOGGER.info("createExceptionDetail end");
		return ed;
	}
	

}
