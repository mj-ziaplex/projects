/*
 * Created on Dec 17, 2003
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.interfaces.domino;

import java.io.IOException;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Vector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import lotus.domino.Database;
import lotus.domino.DateTime;
import lotus.domino.Document;
import lotus.domino.NotesException;
import lotus.domino.RichTextItem;
import lotus.domino.Session;
import com.slocpi.ium.data.MedicalRecordData;
import com.slocpi.ium.data.dao.ClientDataSheetDAO;
import com.slocpi.ium.data.util.DataSourceProxy;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.DateHelper;
import com.slocpi.ium.util.IUMConstants;
/**
 * @author daguila
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class DominoTranslator {
	private static final Logger LOGGER = LoggerFactory.getLogger(DominoTranslator.class);
	private Connection conn;
	//hard-coded values
	private static final String UMB_FORM = "UMB_FORM";
	private static final String FM_GENERATOR_7DAY = "FM_Generator_7Day";
	private static final String LETTER_NAME = "LetterName";
	private static final String LETTER_NAME_SEVEN_DAY_MEMO = "7-day Memo";
	private static final String TEMPLATE_FORM = "TemplateForm";
	private static final String TEMPLATE_SEVEN_DAY_MEMO = "7_Day_Memo";	
	private static final String LETTER_TYPE = "LetterType";
	private static final String LETTER_TYPE_SEVEN_DAY_MEMO = "7 Day Memo";	
	//
	private static final String FORM = "Form";
	private static final String LT_BASIC_LETTER_7DAY = "LT_BasicLetter_7Day";	
	private static final String POLICY_NUMBER = "policyNumber";
	private static final String _POLICY_NUM = "_PolicyNum";
	private static final String _INSURED_NAME = "_InsuredName";
	private static final String _DEADLINE = "_Deadline";
	private static final String _PROCESSOR = "_Processor";
    private static final String _REASON = "_Reason";
    private static final String _SECTION = "_Section";
	private static final String _STATUS = "_Status";
	private static final String TAG = "tag";
	private static final String SEND_TO = "SendTo";
	private static final String COPY_TO = "CopyTo";
	private static final String STATUS = "Status";	
	private static final String PARLIST = "parList";
	private static final String _REQ = "_Req";
	private static final String DATE_CREATED = "DateCreated";
	private static final String _DATE_TODAY = "_DateToday";
	private static final String SIMPLE_DATE = "SimpleDate";
	private static final String NEW_DOC = "NewDoc";	
	private static final String SUBJECT = "Subject";
	private static final String TEST1 = "Test1";
	private static final String BODY_FIELD = "BodyField";
	private static final String BODY = "Body";
	private static final String PRINCIPAL = "Principal";
	//
	private static final String STATUS_NEW = "New";
	private static final String STATUS_SENT = "Sent";
	private static final String FORM_MEMO = "Memo";
	private static final String CLIENT_TYPE_OWNER = "owner";
	private static final String CLIENT_TYPE_INSURED = "insured";
	private static final String TMP_DISPLAY_SENT_BY = "tmpDisplaySentBy";
	private static final String DISPLAY_FROM = "DisplayFrom";
	private static final String DISPLAY_FROM_PREVIEW = "DisplayFrom_Preview";
	private static final String DISPLAY_SENT = "DisplaySent";
	private static final String TMP_DISPLAY_FROM_PREVIEW = "tmpDisplayFrom_Preview";
	private static final String PLATES = "Plates";
	public  static final char eof = '\uffff';
	public  static final char eol = (char)13;
	public  static final char cr = '\r';

			
	public DominoTranslator () {}
	//assumption : all the required fields are not null
	public void triggerLWS(MedicalRecordData medicalRecordData, ArrayList recipients) throws NotesException{
		
		LOGGER.info("triggerLWS start");
		DominoConnector dominoConnector = new DominoConnector();
		Session session = dominoConnector.getNotesSession();
		try{			
			Database database = dominoConnector.getNotesDatabase(session);
			if (database != null){
				//get details from MedicalRecordData??? or create new data bean, create notes doc
				if (!database.isOpen())
        			LOGGER.debug("Database does not exist.");
      			else{
      				LOGGER.debug("Title of "+ database.getFileName() + " : " +database.getTitle());     				
      				Document doc = database.createDocument();
      				//Populate doc
   					if (this.populateSevenDayMemo(doc, medicalRecordData, recipients, session, database)){
   						Document mailDoc = database.createDocument();      		
   						if (recipients.size() > 0)			
   							this.send7DayMemo(mailDoc, doc, recipients, medicalRecordData.getAgent().getEmailAddr());
   						else
   							LOGGER.debug("No recipients for mail.");
   						mailDoc.recycle();
   					}      					

      				doc.recycle();
      				database.recycle();
				}
			}			
		}
    	catch (Exception e) {
    		LOGGER.error(CodeHelper.getStackTrace(e));
    	}    	
    	finally{
    		try {
				dominoConnector.closeNotesSession(session);
			}
			 catch (NotesException e) {
				 LOGGER.error(CodeHelper.getStackTrace(e));
			}
    	}
		LOGGER.info("triggerLWS end");
	}
	
	private boolean populateSevenDayMemo(Document doc, MedicalRecordData medicalRecordData, ArrayList recipients, Session session, Database database) throws Exception{

		LOGGER.info("populateSevenDayMemo start");
		doc.replaceItemValue(FORM, LT_BASIC_LETTER_7DAY);
      	doc.replaceItemValue(UMB_FORM, FM_GENERATOR_7DAY);
        doc.replaceItemValue(LETTER_NAME, LETTER_NAME_SEVEN_DAY_MEMO);
        
        String policyNumber = medicalRecordData.getPolicyMedicalRecordData().getReferenceNumber(); //not required
      	doc.replaceItemValue(POLICY_NUMBER, policyNumber);
      	
      	String insuredName = (medicalRecordData.getClient().getGivenName() + " " + medicalRecordData.getClient().getLastName()).toUpperCase();
      	doc.replaceItemValue(_INSURED_NAME, insuredName);
      	
      	doc.replaceItemValue(TEMPLATE_FORM, TEMPLATE_SEVEN_DAY_MEMO);
      	
      	int followUpNo = (int) medicalRecordData.getFollowUpNumber();
      	String currentDate7="";
      	if (followUpNo > 0)
			currentDate7 = DateHelper.format(DateHelper.add(new Date(), Calendar.DAY_OF_MONTH, followUpNo), "dd MMMMM yyyy");
		else
		 	currentDate7 = DateHelper.format(DateHelper.add(new Date(), Calendar.DAY_OF_MONTH, 7), "dd MMMMM yyyy");
		doc.replaceItemValue(_DEADLINE, currentDate7);
      	
      	doc.replaceItemValue(_PROCESSOR, medicalRecordData.getCreatedBy()); 
      	doc.replaceItemValue(_REASON, medicalRecordData.getReasonForReqt()); 
      	doc.replaceItemValue(_SECTION, medicalRecordData.getSection().getSectionDesc()); 
      	doc.replaceItemValue(_STATUS, medicalRecordData.getApplicationStatus().getStatusDesc()); 
      	doc.replaceItemValue(TAG, "");        

      	String copyTo = "";      				
      	if (recipients.size() > 0){
      		copyTo = (String) recipients.get(0);
      		if (recipients.size() > 1){
      			for (int i=1; i<recipients.size(); i++){
      				copyTo = copyTo + ", " + (String) recipients.get(i);
      			}   							
      		}
      		doc.replaceItemValue(SEND_TO, copyTo); 
      	}      				 
      				
      	doc.replaceItemValue(LETTER_TYPE, LETTER_TYPE_SEVEN_DAY_MEMO);
      	doc.replaceItemValue(STATUS, STATUS_NEW);
      	
      	 
      	ClientDataSheetDAO cdsDao = new ClientDataSheetDAO();
      	ArrayList typeList = cdsDao.retrieveClientType(policyNumber, medicalRecordData.getClient().getClientId());
      	if (typeList.size() <= 0){ 
			typeList.add(0, IUMConstants.CLIENT_TYPE_INSURED);
      	}

		for (int i=0; i<typeList.size(); i++){
			if (((String)typeList.get(i)).equalsIgnoreCase(IUMConstants.CLIENT_TYPE_OWNER))
				typeList.set(i, CLIENT_TYPE_OWNER);
			else 
				typeList.set(i, CLIENT_TYPE_INSURED);
		}
      	if (typeList.size() > 1)
      		doc.replaceItemValue(PARLIST, "Q" + typeList.size()); 
      	else if (typeList.size() == 1)
      		doc.replaceItemValue(PARLIST, "");
      				
      	if (typeList.size() > 0){
      		for (int i=0; i<typeList.size(); i++){
      			if (i+1 <= 6)
   					doc.replaceItemValue(_REQ + ( i+1), typeList.get(i));
      		}
      	}
      	
      	DateTime currentDate = session.createDateTime(new Date());      	
      	doc.replaceItemValue(DATE_CREATED, currentDate); 
      	doc.replaceItemValue(_DATE_TODAY, currentDate);      	
      	currentDate.recycle();
      	String simpleDate = DateHelper.format(new Date(), "mm/dd/yyyy");
      	doc.replaceItemValue(SIMPLE_DATE, simpleDate); 
      	doc.replaceItemValue(NEW_DOC, "0");
      	doc.replaceItemValue(SUBJECT, "LWS - " + policyNumber + " : " + insuredName); 
      	doc.replaceItemValue(TEST1, "");
      	doc.replaceItemValue("SaveOptions", "1");
      	doc.replaceItemValue("Author", medicalRecordData.getUpdatedBy()); 
	    
	  	
	   	RichTextItem bodyField = doc.createRichTextItem(BODY_FIELD);
	   	this.populateBodyField(bodyField, policyNumber, insuredName, typeList, medicalRecordData, currentDate7, database);
 	   	
 	   	if (doc.save()){
        	LOGGER.debug("Document has been saved");
        	LOGGER.info("populateSevenDayMemo end");
        	return true;
 	   	}
      	else{
      		LOGGER.debug("Unable to save document");
      		LOGGER.info("populateSevenDayMemo end");
        	return false;
      	}
 	   
	}
	
	private void send7DayMemo(Document mailDoc, Document doc, ArrayList recipients, String agent) throws NotesException{				

		LOGGER.info("send7DayMemo start");
		mailDoc.replaceItemValue(FORM, FORM_MEMO);
		mailDoc.replaceItemValue(SUBJECT, doc.getItemValueString(SUBJECT) + " - " + LETTER_NAME_SEVEN_DAY_MEMO);
		mailDoc.replaceItemValue(BODY, doc.getFirstItem(BODY_FIELD));
		
		Vector v = new Vector();
		if (recipients.size() > 0){			
   			for (int i=0; i<recipients.size(); i++){
   				v.addElement((String) recipients.get(i));
   			}
   			mailDoc.replaceItemValue(SEND_TO, v);   			   							
      	}      					

		mailDoc.replaceItemValue(PRINCIPAL, agent);
		mailDoc.replaceItemValue(TMP_DISPLAY_SENT_BY, agent);
		mailDoc.replaceItemValue(DISPLAY_FROM, agent);
		mailDoc.replaceItemValue(DISPLAY_FROM_PREVIEW, agent);
		mailDoc.replaceItemValue(DISPLAY_SENT, agent);
		mailDoc.replaceItemValue(TMP_DISPLAY_FROM_PREVIEW, agent);		
		mailDoc.send(true, v);  						
		
		doc.replaceItemValue(STATUS, STATUS_SENT);
		doc.save();
		
		LOGGER.info("send7DayMemo end");
	}
	
	private void populateBodyField(RichTextItem bodyField, String policyNumber, String insuredName, ArrayList typeList, MedicalRecordData medicalRecordData, String currentDate7, Database database) throws NotesException, IOException{		

		LOGGER.info("populateBodyField start");  
	   	bodyField.appendText("Re: Policy No. " + policyNumber + " - " + insuredName); bodyField.addNewLine(2);
	   	bodyField.appendText("Subject application requires: "); bodyField.addNewLine(2);	   	
	   	if (typeList.size() > 1){
	   	   	bodyField.appendText("1. " + "Medical exam - " + typeList.get(0)); bodyField.addNewLine(1);	   	   	
      	   	for (int i=1; i<typeList.size(); i++){
      			if (i+1 <= 6){
      				bodyField.appendText(( i+1) + ". " + "Medical exam - " +  typeList.get(i)); bodyField.addNewLine(1);      				
      			}
      		}
	   	}
	   	else
	   	bodyField.appendText("Medical exam - " + typeList.get(0)); bodyField.addNewLine(1);	   			   	
	   	bodyField.addNewLine(1);
	   	bodyField.appendText("This is in view of " + medicalRecordData.getReasonForReqt() + ".  We would appreciate receiving the "); bodyField.addNewLine(1);	   	
       	bodyField.appendText("requirement(s) on or before " + currentDate7 + " addressed to " + medicalRecordData.getSection().getSectionDesc() + ".  As "); bodyField.addNewLine(1);       	
 	   	bodyField.appendText("you know, lack of appropriate evidence as a means for evaluation usually "); bodyField.addNewLine(1); 	   	
 	   	bodyField.appendText("results to an application being classified as " + medicalRecordData.getApplicationStatus().getStatusDesc()); bodyField.addNewLine(2); 	   	
 	   	bodyField.appendText("For other U/W requirements, please refer to the revised U/W limits "); bodyField.addNewLine(1); 	   	
 	   	bodyField.appendText("effective 12 October 2001."); bodyField.addNewLine(2); 	   	
 	   	bodyField.appendText("Thank you."); bodyField.addNewLine(1); 	   	
 	   
 	   LOGGER.info("populateBodyField end");			
	}
	
}
