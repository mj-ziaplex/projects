/*
 * Created on Dec 17, 2003
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.interfaces.domino;

import java.util.ResourceBundle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import lotus.domino.Database;
import lotus.domino.NotesException;
import lotus.domino.NotesFactory;
import lotus.domino.Session;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.IUMConstants;
/**
 * @author daguila
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class DominoConnector {

	private static final Logger LOGGER = LoggerFactory.getLogger(DominoConnector.class);
	
	public Database getNotesDatabaseConnection(){
		
		LOGGER.info("getNotesDatabaseConnection start");
		Database database = null;
		try {
			Session session = getNotesSession();
       		if (session != null){
       			database = getNotesDatabase(session);
       		}
		}				
		catch (Exception ex){
			LOGGER.error(CodeHelper.getStackTrace(ex));
		}	
		LOGGER.info("getNotesDatabaseConnection end");
        return database;            
	}
	
	public Session getNotesSession() throws NotesException{
		
		LOGGER.info("getNotesSession start");
		Session session = null;

    	ResourceBundle rb = ResourceBundle.getBundle(IUMConstants.IUM_CONFIG);
		
		String notesdb_host = rb.getString(IUMConstants.IUM_NOTESDB_HOST);
		String notesdb_user = rb.getString(IUMConstants.IUM_NOTESDB_USER);
		String notesdb_password = rb.getString(IUMConstants.IUM_NOTESDB_PASS);		
			
		session = NotesFactory.createSession(notesdb_host, notesdb_user, notesdb_password);
		LOGGER.info("getNotesSession end");
    	return session;            
	}
	
	public Database getNotesDatabase(Session session) throws NotesException{
		
		LOGGER.info("getNotesDatabase start");
		Database database = null;

    	ResourceBundle rb = ResourceBundle.getBundle(IUMConstants.IUM_CONFIG);
    	
		String notesdb_server = rb.getString(IUMConstants.IUM_NOTESDB_SERVER);
		String notesdb_path = rb.getString(IUMConstants.IUM_NOTESDB_PATH);
		 
		database = session.getDatabase(notesdb_server, notesdb_path);//+"\\"+notesdb_name);		
		LOGGER.info("getNotesDatabase end");
    	return database;            
	}
	
	public void closeNotesSession(Session session) throws NotesException{
		LOGGER.info("closeNotesSession start");
		if (session != null){
       		session.recycle();
       	}
		LOGGER.info("closeNotesSession end");
	}
}
