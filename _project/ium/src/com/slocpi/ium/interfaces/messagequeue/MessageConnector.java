/*
 * Created on Dec 17, 2003
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.interfaces.messagequeue;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.Properties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.ibm.mq.MQC;
import com.ibm.mq.MQEnvironment;
import com.ibm.mq.MQException;
import com.ibm.mq.MQGetMessageOptions;
import com.ibm.mq.MQMessage;
import com.ibm.mq.MQPutMessageOptions;
import com.ibm.mq.MQQueue;
import com.ibm.mq.MQQueueManager;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.DateHelper;


/**
 * @author daguila
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class MessageConnector {

	private static final Logger LOGGER = LoggerFactory.getLogger(MessageConnector.class);
	private MQQueueManager qMgr;
	private String qMgrName;
	private String qReqName;
	private String qRespName;
	private String resOK = "JavaResult:OK ";
	private String resNOTOK = "JavaResult:NOTOK ";
	private int waitInterval; 
	private boolean msgAvailable = true;
	
	/**
	 * 
	 */
	public MessageConnector() {
		super();
	}

	/**
	 * 
	 */
	public MessageConnector(String file) {
		
		LOGGER.info("MessageConnector start 1");
		try{
			ClassLoader loader = MessageConnector.class.getClassLoader();
			InputStream in = loader.getResourceAsStream(file);
			Properties prop = new Properties();
			prop.load(in);		

			MQEnvironment.hostname = prop.getProperty("hostname");
			MQEnvironment.channel = prop.getProperty("channel");
			MQEnvironment.port = Integer.parseInt(prop.getProperty("port"));
			MQEnvironment.userID = prop.getProperty("userID");
			MQEnvironment.password = prop.getProperty("password");
			qMgrName = prop.getProperty("queueManagerName");
			qReqName = prop.getProperty("putQueue");
			qRespName = prop.getProperty("getQueue");	
			waitInterval = Integer.parseInt(prop.getProperty("waitInterval"));	
							
			qMgr = new MQQueueManager(qMgrName);
								
		}catch(IOException e){
			LOGGER.error(CodeHelper.getStackTrace(e));
		}catch (MQException ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			LOGGER.error("MQ exception: CC = " + ex.completionCode 
						   + " RC = " + ex.reasonCode);
		}
		LOGGER.info("MessageConnector end 1");
	}
		
	/**
	 * 
	 */
	public MessageConnector(String qmName, String qName, String replyToQueueName, String host, String channel, int port) {
		
		LOGGER.info("MessageConnector start 2");
		try{
			MQEnvironment.hostname = host;
			MQEnvironment.channel = channel;
			MQEnvironment.port = port;

			qMgrName = qmName;
			qReqName = qName;
			qRespName = replyToQueueName;
			qMgr = new MQQueueManager(qMgrName);	
		}catch (MQException ex) {
			LOGGER.error(CodeHelper.getStackTrace(ex));
			LOGGER.error("MQ exception: CC = " + ex.completionCode 
						   + " RC = " + ex.reasonCode);
		}
		LOGGER.info("MessageConnector end 2");
	}
		
	
	public void commit(){
		
		LOGGER.info("commit start");
		try{
			qMgr.commit();
		}catch(MQException mqe){
			LOGGER.error(CodeHelper.getStackTrace(mqe));
			LOGGER.error(resNOTOK + "MQ Error " + mqe.reasonCode);
		}
		LOGGER.info("commit end");
	}
	

	public void rollback(){
		
		LOGGER.info("rollback start");
		try{
			qMgr.backout();
		}catch(MQException mqe){
			LOGGER.error(CodeHelper.getStackTrace(mqe));
			LOGGER.error(resNOTOK + "MQ Error " + mqe.reasonCode);
		}
		LOGGER.info("rollback end");
	}
	
		
	public void postMessage(String str){
		
		LOGGER.info("postMessage start 1");
		try{
			int openPutOptions = MQC.MQOO_OUTPUT + MQC.MQOO_FAIL_IF_QUIESCING;
			MQQueue myQueue = qMgr.accessQueue(qReqName, openPutOptions, null, null, null);			
			MQMessage mqMsg = new MQMessage();
			mqMsg.format = MQC.MQFMT_STRING;
			mqMsg.writeString(str);
			MQPutMessageOptions pmo = new MQPutMessageOptions();
			myQueue.put(mqMsg,pmo);	
			qMgr.commit();
		}catch(IOException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			LOGGER.error(resNOTOK + "I/O Error " + e.toString());
		}catch(MQException mqe) {
			LOGGER.error(CodeHelper.getStackTrace(mqe));
			if(mqe.reasonCode == MQException.MQRC_NO_MSG_AVAILABLE) { 
				msgAvailable = false;
				LOGGER.error(resOK + "no message MQRC " + mqe.reasonCode);
			} else {
				LOGGER.error(resNOTOK + "MQ Error " + mqe.reasonCode);
			
			}	
		}	
		LOGGER.info("postMessage end 1");
	}
	
	public void postMessage(String str, int priority){
		
		LOGGER.info("postMessage start 2");
		try{
			int openPutOptions = MQC.MQOO_OUTPUT + MQC.MQOO_FAIL_IF_QUIESCING;
			MQQueue myQueue = qMgr.accessQueue(qReqName, openPutOptions, null, null, null);			
			MQMessage mqMsg = new MQMessage();
			mqMsg.format = MQC.MQFMT_STRING;
			mqMsg.writeString(str);
			mqMsg.priority = priority;
			MQPutMessageOptions pmo = new MQPutMessageOptions();
			myQueue.put(mqMsg,pmo);
			qMgr.commit();
		}catch(IOException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			LOGGER.error(resNOTOK + "I/O Error " + e.toString());
		}catch(MQException mqe) {
			LOGGER.error(CodeHelper.getStackTrace(mqe));
			if(mqe.reasonCode == MQException.MQRC_NO_MSG_AVAILABLE) { 
				msgAvailable = false;
				LOGGER.error(resOK + "no message MQRC " + mqe.reasonCode);
			} else {
				LOGGER.error(resNOTOK + "MQ Error " + mqe.reasonCode);
			}	
		}			
		LOGGER.info("postMessage end 2");
	}	
	
	public void postMessage(String transID, String str){
		
		LOGGER.info("postMessage start 3");
		try{
			int openPutOptions = MQC.MQOO_OUTPUT + MQC.MQOO_FAIL_IF_QUIESCING;
			MQQueue myQueue = qMgr.accessQueue(qReqName, openPutOptions, null, null, null);			
			MQMessage mqMsg = new MQMessage();
			MQPutMessageOptions pmo = new MQPutMessageOptions();
			pmo.options = MQC.MQPMO_NO_SYNCPOINT
						+ MQC.MQPMO_NEW_MSG_ID 
						+ MQC.MQPMO_FAIL_IF_QUIESCING;
						
			mqMsg.clearMessage();
			mqMsg.persistence = MQC.MQPER_NOT_PERSISTENT;
			mqMsg.format = MQC.MQFMT_STRING;
			mqMsg.replyToQueueName = qRespName;
			mqMsg.replyToQueueManagerName = qMgrName;
			mqMsg.writeString(str);
			mqMsg.correlationId = transID.getBytes();
			myQueue.put(mqMsg,pmo);		
			qMgr.commit();
			
		}catch(IOException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			LOGGER.error(resNOTOK + "I/O Error " + e.toString());
		}catch(MQException mqe) {
			LOGGER.error(CodeHelper.getStackTrace(mqe));
			if(mqe.reasonCode == MQException.MQRC_NO_MSG_AVAILABLE) { 
				msgAvailable = false;
				LOGGER.error(resOK + "no message MQRC " + mqe.reasonCode);
			} else {
				LOGGER.error(resNOTOK + "MQ Error " + mqe.reasonCode);
				
			}	
		}	
		LOGGER.info("postMessage end 3");
	}	


	private void postMessage(MQMessage mqMsg){
		
		LOGGER.info("postMessage start 4");
		try{
			int openPutOptions = MQC.MQOO_OUTPUT + MQC.MQOO_FAIL_IF_QUIESCING;
			MQQueue myQueue = qMgr.accessQueue(qReqName, openPutOptions, null, null, null);
			MQPutMessageOptions pmo = new MQPutMessageOptions();
			pmo.options = MQC.MQPMO_NONE;
			myQueue.put(mqMsg,pmo);	
			qMgr.commit();
		}catch(MQException mqe) {
			LOGGER.error(CodeHelper.getStackTrace(mqe));
			if(mqe.reasonCode == MQException.MQRC_NO_MSG_AVAILABLE) { 
				msgAvailable = false;
				LOGGER.error(resOK + "no message MQRC " + mqe.reasonCode);
			} else {
				LOGGER.error(resNOTOK + "MQ Error " + mqe.reasonCode);
			}	
		}
		LOGGER.info("postMessage end 4");
	}
	
	
	public String putRequestMsg(String strMsg, int timeOut, int priority){
		
		LOGGER.info("putRequestMsg start");
		String retStr = null;
		try{			
					            
			int openPutOptions = MQC.MQOO_OUTPUT + MQC.MQOO_FAIL_IF_QUIESCING;
			MQQueue myPutQ = qMgr.accessQueue(qReqName, openPutOptions, null, null, null);
			MQPutMessageOptions pmo = new MQPutMessageOptions();
			pmo.options = MQC.MQPMO_SYNCPOINT
						+ MQC.MQPMO_NEW_MSG_ID 
						+ MQC.MQPMO_FAIL_IF_QUIESCING;
			MQMessage myPutMsg = new MQMessage();
			myPutMsg.clearMessage();
			myPutMsg.persistence = MQC.MQPER_NOT_PERSISTENT; 
			myPutMsg.format = MQC.MQFMT_STRING;
			myPutMsg.messageType = MQC.MQMT_REQUEST;         
			myPutMsg.replyToQueueName = qRespName;
			myPutMsg.replyToQueueManagerName = qMgrName;
			myPutMsg.writeString(strMsg);
			myPutMsg.priority = priority;
			myPutQ.put(myPutMsg, pmo);
			qMgr.commit();
			
	        String filename;
	        if(MQEnvironment.port==1795){
	        	filename = "IUM_STAGING";
	        }else if(MQEnvironment.port==1795){
				filename = "IUM_TEST";
			}else{
				filename = "IUM_DEV";
			}
	        File file = new File("C:/IUM_MQLog/" + filename + ".log");
			StringBuffer sb = new StringBuffer();
	        if(file.exists()){  	
				FileReader fr = new FileReader(file);
				BufferedReader bf = new BufferedReader(fr);
				String line = null;
				while ((line = bf.readLine()) != null) {
					sb.append(line + "\r\n");
				}
				bf.close();
	        }
	        FileWriter fw = new FileWriter(file);
	        fw.write(sb.toString() + "\r\n");
	        fw.write(DateHelper.format(new Date(System.currentTimeMillis()), "yyyyMMMdd hh:mm:ss" ) + "\r\n");
	        fw.write("Message : " + strMsg + "\r\n");
			fw.write("===============\r\n");
	        fw.close();
	        
	        int openGetOptions = MQC.MQOO_INPUT_SHARED + MQC.MQOO_FAIL_IF_QUIESCING;
			MQQueue myGetQ = qMgr.accessQueue(qRespName, openGetOptions, null, null, null);
			MQGetMessageOptions gmo = new MQGetMessageOptions(); 
			gmo.options = MQC.MQGMO_WAIT
						+ MQC.MQGMO_CONVERT 
						+ MQC.MQGMO_NO_SYNCPOINT 
						+ MQC.MQGMO_FAIL_IF_QUIESCING;
			gmo.waitInterval = timeOut; 
			MQMessage myGetMsg = new MQMessage();
			myGetMsg.clearMessage();
			myGetMsg.correlationId = myPutMsg.messageId;
			
			myGetQ.get(myGetMsg, gmo);
			String getmsg = myGetMsg.readString(myGetMsg.getMessageLength());
			
			retStr = getmsg;
						
		}catch(IOException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			LOGGER.error(resNOTOK + "I/O Error " + e.toString());
		}catch(MQException mqe) {
			LOGGER.error(CodeHelper.getStackTrace(mqe));
			if(mqe.reasonCode == MQException.MQRC_NO_MSG_AVAILABLE) { 
				msgAvailable = false;
				LOGGER.error(resOK + "no message MQRC " + mqe.reasonCode);
			} else {
				LOGGER.error(resNOTOK + "MQ Error " + mqe.reasonCode);
			}	
		}	
		LOGGER.info("putRequestMsg end");
		return retStr; 		
	}


	public String putRequestMsgMI(String strMsg, int timeOut, int priority){
		
		LOGGER.info("putRequestMsgMI start");
		String retStr = null;
		try{			
				            
			int openPutOptions = MQC.MQOO_OUTPUT + MQC.MQOO_FAIL_IF_QUIESCING;
			MQQueue myPutQ = qMgr.accessQueue(qReqName, openPutOptions, null, null, null);
			MQPutMessageOptions pmo = new MQPutMessageOptions();
			pmo.options = MQC.MQPMO_SYNCPOINT
						+ MQC.MQPMO_NEW_MSG_ID 
						+ MQC.MQPMO_FAIL_IF_QUIESCING;
			MQMessage myPutMsg = new MQMessage();
			myPutMsg.clearMessage();
			myPutMsg.persistence = MQC.MQPER_NOT_PERSISTENT;
			myPutMsg.format = MQC.MQFMT_STRING;
			myPutMsg.messageType = MQC.MQMT_REQUEST;         
			myPutMsg.replyToQueueName = qRespName;
			myPutMsg.replyToQueueManagerName = qMgrName;
			myPutMsg.writeString(strMsg);
			myPutMsg.priority = priority;
			myPutMsg.correlationId = "QPQI".getBytes();
			
			myPutQ.put(myPutMsg, pmo);
			qMgr.commit();   
		      
			int openGetOptions = MQC.MQOO_INPUT_SHARED + MQC.MQOO_FAIL_IF_QUIESCING;
			MQQueue myGetQ = qMgr.accessQueue(qRespName, openGetOptions, null, null, null);
			MQGetMessageOptions gmo = new MQGetMessageOptions(); 
			gmo.options = MQC.MQGMO_WAIT
						+ MQC.MQGMO_CONVERT 
						+ MQC.MQGMO_NO_SYNCPOINT 
						+ MQC.MQGMO_FAIL_IF_QUIESCING;
			gmo.waitInterval = timeOut; 
			MQMessage myGetMsg = new MQMessage();
			myGetMsg.clearMessage();
			myGetMsg.messageId = myPutMsg.messageId;
						
			myGetQ.get(myGetMsg, gmo);
			String getmsg = myGetMsg.readString(myGetMsg.getMessageLength());
			
			retStr = getmsg;
						
		}catch(IOException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			LOGGER.error(resNOTOK + "I/O Error " + e.toString());
		}catch(MQException mqe) {
			LOGGER.error(CodeHelper.getStackTrace(mqe));
			if(mqe.reasonCode == MQException.MQRC_NO_MSG_AVAILABLE) { 
				msgAvailable = false;
				LOGGER.error(resOK + "no message MQRC " + mqe.reasonCode);
			} else {
				LOGGER.error(resNOTOK + "MQ Error " + mqe.reasonCode);
			}	
		}	
		LOGGER.info("putRequestMsgMI end");
		return retStr; 		
	}


	
	public String getMessage(){
		
		LOGGER.info("getMessage start 1");
		String retMsg = null;
		try{
			int openGetOptions = MQC.MQOO_INPUT_SHARED + MQC.MQOO_FAIL_IF_QUIESCING;
			MQQueue myGetQ = qMgr.accessQueue(qRespName, openGetOptions, null, null, null);		
			
			MQMessage theMessage    = new MQMessage();
			MQGetMessageOptions gmo = new MQGetMessageOptions();
			myGetQ.get(theMessage,gmo); 
			retMsg = theMessage.readString(theMessage.getMessageLength());
			qMgr.commit();
			
		}catch(IOException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			LOGGER.error("I/O Error " + e.toString());
		}catch(MQException mqe) {
			LOGGER.error(CodeHelper.getStackTrace(mqe));
			if(mqe.reasonCode == MQException.MQRC_NO_MSG_AVAILABLE) { 
				msgAvailable = false;
				LOGGER.error(resOK + "no message MQRC " + mqe.reasonCode);
				LOGGER.error(resOK + "completionCode " + mqe.completionCode);
			} else {
				LOGGER.error(resNOTOK + "MQ Error " + mqe.reasonCode);
		
			}	
		}
		LOGGER.info("getMessage end 1");
		return retMsg;		
	}	
	
	
	public ArrayList getMessage(String trxnID){
		
		LOGGER.info("getMessage start 2");
		ArrayList arrRetMsg = new ArrayList();
		MQGetMessageOptions gmo = new MQGetMessageOptions();
		gmo.options = MQC.MQGMO_WAIT | MQC.MQGMO_BROWSE_FIRST;
		MQMessage myMessage = new MQMessage();		
		boolean done = false;
		
		try{
			int openGetOptions = MQC.MQOO_INPUT_SHARED + MQC.MQOO_FAIL_IF_QUIESCING + MQC.MQOO_BROWSE;
			MQQueue myGetQ = qMgr.accessQueue(qRespName, openGetOptions, null, null, null);	

			do {
				myMessage.clearMessage();
				myMessage.correlationId = MQC.MQCI_NONE;
				myMessage.messageId = MQC.MQMI_NONE;			  
	 
				myGetQ.get(myMessage, gmo);
				String msg = myMessage.readString(myMessage.getMessageLength()); 
				
				if(msg.indexOf(trxnID)!=-1){
					gmo.options = MQC.MQGMO_MSG_UNDER_CURSOR;  
					myGetQ.get(myMessage, gmo);	  	
					arrRetMsg.add(myMessage.readString(myMessage.getMessageLength()));
										
				}
				  
				/************************************************/
				/* Reset the options to browse the next message */
				/************************************************/
				gmo.options = MQC.MQGMO_WAIT | MQC.MQGMO_BROWSE_NEXT;
			} while (!done);
			
			qMgr.commit();
				
		}catch(IOException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			LOGGER.error(resNOTOK + "I/O Error " + e.toString());
		}catch(MQException mqe) {
			LOGGER.error(CodeHelper.getStackTrace(mqe));
			if(mqe.reasonCode == MQException.MQRC_NO_MSG_AVAILABLE) { 
				done = true;
				commit();
				LOGGER.error(resOK + "no message MQRC " + mqe.reasonCode);
			} else {
				done = true;
				LOGGER.error(resNOTOK + "MQ Error " + mqe.reasonCode);
			}	
		}	
		LOGGER.info("getMessage end 2");
		return arrRetMsg;		
	}
	

	public ArrayList getMessageByTransID(String transID){
		
		LOGGER.info("getMessageByTransID start");
		ArrayList arrRetMsg = new ArrayList();
		MQMessage myMessage = new MQMessage();		
		boolean done = false;
		
		try{
			int openGetOptions = MQC.MQOO_INPUT_SHARED + MQC.MQOO_FAIL_IF_QUIESCING;
			MQQueue myGetQ = qMgr.accessQueue(qRespName, openGetOptions, null, null, null);		
			MQGetMessageOptions gmo = new MQGetMessageOptions();
			gmo.options = MQC.MQGMO_CONVERT 
						+ MQC.MQGMO_NO_SYNCPOINT 
						+ MQC.MQGMO_FAIL_IF_QUIESCING;
			do {
				myMessage.clearMessage();
				myMessage.format = MQC.MQFMT_STRING;
				myMessage.messageId = transID.getBytes(); 
				myGetQ.get(myMessage, gmo);
				String msg = myMessage.readString(myMessage.getMessageLength()); 
				arrRetMsg.add(msg);  
			} while (!done);
			
			qMgr.commit();
				
		}catch(IOException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			LOGGER.error(resNOTOK + "I/O Error " + e.toString());
		}catch(MQException mqe) {
			LOGGER.error(CodeHelper.getStackTrace(mqe));
			if(mqe.reasonCode == MQException.MQRC_NO_MSG_AVAILABLE) { 
				done = true;
				LOGGER.error(resOK + "no message MQRC " + mqe.reasonCode);
			} else {
				done = true;
				LOGGER.error(resNOTOK + "MQ Error " + mqe.reasonCode);
			}	
		}	
		LOGGER.info("getMessageByTransID end");
		return arrRetMsg;		
	}
		
}
