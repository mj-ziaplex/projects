/*
 * Created on Feb 4, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.interfaces.prism;

import java.sql.SQLException;
import java.util.ArrayList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.slocpi.ium.ExceptionDetail;
import com.slocpi.ium.ExceptionLog;
import com.slocpi.ium.ExceptionLogger;
import com.slocpi.ium.IUMException;
import com.slocpi.ium.data.AssessmentRequestData;
import com.slocpi.ium.data.KickOutMessageData;
import com.slocpi.ium.data.PolicyRequirementsData;
import com.slocpi.ium.underwriter.AssessmentRequest;
import com.slocpi.ium.underwriter.KickOutMessage;
import com.slocpi.ium.underwriter.PolicyRequirements;
import com.slocpi.ium.underwriter.UnderWriterException;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.IUMConstants;

/**
 * @author ypojol
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class RequestGenerator implements Runnable {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(RequestGenerator.class);
	private ArrayList arrList;
	private ArrayList exceptions;


	/**
	 * 
	 */
	public RequestGenerator(ArrayList _arr, ArrayList _exceptions) {
		arrList = _arr;
		exceptions = _exceptions;
	}
	
	
	/* (non-Javadoc)
	 * @see java.lang.Runnable#run()
	 */
	public void run() {
		System.out.println("run start");
		save(arrList);
		logExceptions();
		System.out.println("run end");

	}


	private void save(ArrayList arrList){
		
		System.out.println("save start");
		String retStr=null;
		try{
			AssessmentRequest ar = new AssessmentRequest();
			KickOutMessage km = new KickOutMessage();
			PolicyRequirements pr = new PolicyRequirements();
			
			int size = arrList.size();
			for(int i=0; i<size; i++){
					AssessmentRequestData ard = (AssessmentRequestData)arrList.get(i);
					PRISMCodec pc = new PRISMCodec();
					pc.retrieveCodec(ard);
					//create Assessment Request 
					ar.createAssessmentRequest(ard);
					
					//create requirements 
					ArrayList arrReqs = pc.getPolicyRequirements();
					for(int j=0; j<arrReqs.size(); j++){
						PolicyRequirementsData prd = (PolicyRequirementsData)arrReqs.get(j);
						pr.createRequirement(prd, ard, "");
					}
					
					ArrayList arrKOs = pc.getKickOutMessages();
					for(int k=0; k<arrKOs.size(); k++){
						KickOutMessageData kod = (KickOutMessageData)arrKOs.get(k);
						km.saveMessages(kod);
					}		
			}
					
		}catch(SQLException se){
			LOGGER.error(CodeHelper.getStackTrace(se));
			logError(se);		
		}catch(UnderWriterException uwe){
			LOGGER.error(CodeHelper.getStackTrace(uwe));
			logError(uwe);	
		}catch(IUMException ie){
			LOGGER.error(CodeHelper.getStackTrace(ie));
			logError(ie);
		}catch(Exception e){
			LOGGER.error(CodeHelper.getStackTrace(e));	
			logError(e);
		}
		System.out.println("save end");
	}
	
	private void logExceptions(){
		
		ExceptionLogger el = new ExceptionLogger();
		el.logError(exceptions);
	}
	private void logError(Exception e) {
		
		System.out.println("logError start");
		ExceptionLog el= new ExceptionLog();
		el.setRecordType(IUMConstants.SYSTEM_PRISM_DESC);
		el.setSystemInterface(IUMConstants.SYSTEM_PRISM_DESC);
		ArrayList arr = new ArrayList(); 
		arr.add(createExceptionDetail(e.getMessage()));
		el.setDetails(arr);
		ExceptionLogger errorLogger = new ExceptionLogger();
		errorLogger.logError(el);
		System.out.println("logError end");
	}
	
	private ExceptionDetail createExceptionDetail(String message){
		
		System.out.println("createExceptionDetail start");
		ExceptionDetail ed = new ExceptionDetail();
		ed.setMessage(message);
		System.out.println("createExceptionDetail end");
		return ed;
	}


}
