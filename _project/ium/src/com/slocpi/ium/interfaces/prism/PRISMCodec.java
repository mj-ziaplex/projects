/**
 * PrismCodec.java				Copyright (c) 2004 Pointwest Technologies Corp.
 * TODO DOCUMENT ME!
 * 
 * @author Pointwest	 @date Feb 24, 2004
 */
package com.slocpi.ium.interfaces.prism;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.slocpi.ium.IUMException;
import com.slocpi.ium.data.AssessmentRequestData;
import com.slocpi.ium.data.KickOutMessageData;
import com.slocpi.ium.data.PolicyRequirementsData;
import com.slocpi.ium.data.RequirementData;
import com.slocpi.ium.data.dao.RequirementDAO;
import com.slocpi.ium.data.util.DataSourceProxy;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.IUMConstants;

/**
 * TODO DOCUMENT ME!
 * 
 * @author Pointwest		Copyright (c) 2004 Pointwest Technologies Corp.
 * @date Feb 24, 2004
 */
public class PRISMCodec {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(PRISMCodec.class);
	private ArrayList   kickOutMsgs       = new ArrayList();
	private ArrayList   requirements      =  new ArrayList();
			
	public PRISMCodec() {
		super();
	}
		
	public void retrieveCodec(AssessmentRequestData req) throws SQLException, IUMException {
		
		LOGGER.info("retrieveCodec start");
		DataSourceProxy dataSourceProxy = new DataSourceProxy();
		Connection conn = dataSourceProxy.getConnection();
		String userId     = IUMConstants.USER_IUM;
		String clientType = "I";
		int    len        = 0;
		String codes = req.getKickOutCode();
		
		if (codes != null) len = codes.length();
		
		for (int i=0;i<len;i++) {
			String code = String.valueOf(codes.charAt(i));

			String sql = "SELECT B.KO_MESSAGE KOMESSAGE, D.REQT_CODE REQTCODE " + 
		    	         "FROM PRISM_CODEC A, KICKOUT_CODES B, CODEC_KO_CODEC C, CODEC_REQUIREMENTS D " +
		        	     "WHERE A.PC_SEQ = C.PC_SEQ(+) " +
		             	 "AND   C.KO_SEQ = B.KO_SEQ(+) " +
		             	 "AND   A.PC_SEQ = D.PC_SEQ(+) " +
		             	 "AND   A.PC_POSITION = ? " +
		                 "AND   A.PC_CODE = ? ";
		                 		             
			try {
				PreparedStatement prepStmt = conn.prepareStatement(sql);
				prepStmt.setInt(1, i+1);
				prepStmt.setString(2, code);
			
				ResultSet rs = prepStmt.executeQuery();
				while (rs.next()) {
					if (rs.getString("KOMESSAGE") != null) {
						KickOutMessageData ko = new KickOutMessageData();
						ko.setMessageText(rs.getString("KOMESSAGE"));
						if (req.getInsured() != null)
							ko.setClientId(req.getInsured().getClientId().toUpperCase());
						ko.setReferenceNumber(req.getReferenceNumber());
						kickOutMsgs.add(ko);
				   }
				   
				   if (rs.getString("REQTCODE") != null) {
				   	    String reqCode = rs.getString("REQTCODE");
				   	    RequirementData        reqData = retrieveRequirementData(reqCode);
				   	    
				   	    PolicyRequirementsData pol = new PolicyRequirementsData();
				   	    
				   	    pol.setRequirementCode(reqCode);
				   	    
				   	    if (req.getClientDataSheet() != null)
				   	    	pol.setClientId(req.getClientDataSheet().getClientId().toUpperCase());
				   	    	
				   	    pol.setReferenceNumber(req.getReferenceNumber());
				   	    pol.setReqDesc(reqData.getReqtDesc());
				   	    pol.setLevel(reqData.getLevel());
				   	    pol.setFollowUpNumber(reqData.getFollowUpNum());		
				   	    pol.setClientType(clientType);				   	    		   	   
				   	    pol.setCreateDate(new Date());				   	    
				   	    pol.setUpdateDate(new Date());
				   	    pol.setUpdatedBy(userId);
				   	    pol.setCreatedBy(userId);				   	    
				   	    pol.setABACUScreateDate(new Date());
				   	    pol.setABACUSUpdateDate(new Date());				   	    
				   	    requirements.add(pol);				   	    
				   }				   
				}
			} catch (SQLException e) {
				LOGGER.error(CodeHelper.getStackTrace(e));
				throw e;
			} finally {
				dataSourceProxy.closeConnection(conn);
			}
			LOGGER.info("retrieveCodec end");
		}
	}
	
	private RequirementData retrieveRequirementData(String code) throws SQLException {
		
		LOGGER.info("retrieveRequirementData start");
		RequirementData reqData = new RequirementData();
		
		try {
			RequirementDAO dao  = new RequirementDAO();
			reqData = dao.retrieveRequirement(code);
		} catch (Exception e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
		} 
		LOGGER.info("retrieveRequirementData start");
		return reqData;
	}
	
	public ArrayList getKickOutMessages() {
		return kickOutMsgs;
	}

    public ArrayList getPolicyRequirements() {
    	return requirements;
    }
	/**
	 * @param list
	 */
	public void setKickOutMsgs(ArrayList list) {
		kickOutMsgs = list;
	}

	/**
	 * @param list
	 */
	public void setRequirements(ArrayList list) {
		requirements = list;
	}

}
