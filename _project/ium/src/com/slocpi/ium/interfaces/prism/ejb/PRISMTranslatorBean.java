/*
 * Created on Feb 17, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.interfaces.prism.ejb;

import java.io.IOException;
import java.rmi.RemoteException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.SessionBean;
import javax.ejb.SessionContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import com.slocpi.ium.ExceptionDetail;
import com.slocpi.ium.ExceptionLog;
import com.slocpi.ium.ExceptionLogger;
import com.slocpi.ium.data.AssessmentRequestData;
import com.slocpi.ium.data.ClientData;
import com.slocpi.ium.data.LOBData;
import com.slocpi.ium.data.StatusData;
import com.slocpi.ium.data.SunLifeOfficeData;
import com.slocpi.ium.data.UserProfileData;
import com.slocpi.ium.interfaces.IUMInterfaceException;
import com.slocpi.ium.interfaces.messages.IUMMessageUtility;
import com.slocpi.ium.interfaces.prism.RequestGenerator;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.DateHelper;
import com.slocpi.ium.util.IUMConstants;

/**
 * @author ypojol
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class PRISMTranslatorBean implements SessionBean{

	private static final Logger LOGGER = LoggerFactory.getLogger(PRISMTranslatorBean.class);
	ArrayList exceptions = new ArrayList();


	/**
	 * @TODO method description 
	 * @param sb StringBuffer containing the policy information and kick-out codes in xml format
	 * @exception IOException if an input/output error occurs
	 * @return StringBuffer.
	 */
	public StringBuffer sendData(StringBuffer sb){
		
		LOGGER.info("sendData start");
		StringBuffer retXml = new StringBuffer();
		try{
			
			retXml.append("<?xml version=\"1.0\" encoding=\"UTF-8\" ?><ROOT>");
			ArrayList ar = translatePolicyInformation(sb);
			ArrayList validARs = new ArrayList();
			ArrayList exceptions = new ArrayList();
			
			for(int i=0; i<ar.size(); i++){
				AssessmentRequestData ard = (AssessmentRequestData)ar.get(i);
				ExceptionLog exLog = verifyData(ard);
				if(exLog==null){
					retXml.append(writeReturnReceipt(ard.getReferenceNumber(), true));
					validARs.add(ard);	
				}else{
					retXml.append(writeReturnReceipt(ard.getReferenceNumber(), false));
					exLog.setReferenceNumber(ard.getReferenceNumber());
					exLog.setRecordType("PRISM");
					exLog.setSystemInterface("PRISM");
					exLog.setReply(sb.toString());
					exceptions.add(exLog);
					
				}
			}
		
			RequestGenerator rg = new RequestGenerator(validARs, exceptions);
			new Thread(rg).start();
		
			retXml.append("</ROOT>");
			
			
		}catch(Exception e){
			LOGGER.error(CodeHelper.getStackTrace(e));
			ExceptionLog exLog = new ExceptionLog();
			exLog.setRecordType("PRISM");
			exLog.setSystemInterface("PRISM");
			exLog.setReply(sb.toString());
			ExceptionDetail exD = new ExceptionDetail();
			exD.setMessage(e.getMessage());
			ArrayList arr = new ArrayList();
			arr.add(exD);
			exLog.setDetails(arr);
			ExceptionLogger el = new ExceptionLogger();
			el.logError(exLog);
			retXml = new StringBuffer().append(e.getMessage());
		}
		LOGGER.info("sendData end");
		return retXml;
	}


	/**
	 * @TODO method description
	 * @param sb StringBuffer containing the policy information and kick-out codes in xml format
	 * @exception IOException if an input/output error occurs
	 * @return ReturnReceipt.
	 */
	private ArrayList translatePolicyInformation(StringBuffer message)throws JDOMException, IOException, IUMInterfaceException{
		
		LOGGER.info("translatePolicyInformation start");
		ArrayList list = new ArrayList();
			Document doc = IUMMessageUtility.getDocument(message);
			Element root = doc.getRootElement();
			List nodes = root.getChildren();
			for (int i=0; i < nodes.size(); i++) {
				Element e = (Element)nodes.get(i);
				String attribute = e.getName();
				if (attribute == null) {
					attribute = "";
				}
				if (attribute.equalsIgnoreCase("POL_QUEUE_INFO")) {
					AssessmentRequestData tmp = parseData(e.getChildren());
					list.add(tmp);				
				} else {
					LOGGER.warn("Attribute [" + attribute + "] not defined for Policy Queue.");
					throw new IUMInterfaceException("Attribute [" + attribute + "] not defined for Policy Queue.");
				}
			}
			LOGGER.info("translatePolicyInformation end");
		return list;
	}


	/**
	 * @TODO method description
	 * @param sb StringBuffer containing the policy information and kick-out codes in xml format
	 * @exception IOException if an input/output error occurs
	 * @return ReturnReceipt.
	 */
	private String writeReturnReceipt(String refNo, boolean isValid){
		
		LOGGER.info("writeReturnReceipt start");
		
		StringBuffer str = new StringBuffer();
		str.append("<POLICY>");
		str.append("<POL_ID>");
		str.append(refNo);
		str.append("</POL_ID>"); 
		str.append("<CONF_CD>");
		if(isValid){
			str.append("00");
		}else{
			str.append("01");
		}
		str.append("</CONF_CD>");
		str.append("</POLICY>");
		
		LOGGER.info("writeReturnReceipt end");
		return str.toString();
	}


	/**
	 * This method extracts the Assessment Request Data with Client Data from the child nodes.
	 * Mapping for the attribute message is as follows:
	 * POL_ID = referenceNumber
	 * CVS_FACE_AMT = amountCovered
	 * POL_MPREM_AMT = premium
	 * APP_RECV_DT = applicationReceivedDate
	 * NB_CNTCT_USER_ID = assignedTo
	 * UW_USER_ID = underwriter;
	 * INSR_SUR_NM = lastName (ClientData)
	 * INSR_GIV_NM = givenName (ClientData)
	 * INSR_MID_NM = middleName (ClientData)
	 * AGT_ID = agentId
	 * SOFF_ID = branchId
	 * 
	 * @param children
	 * @return
	 * @throws IUMABACUSParseException
	 */
	
	private AssessmentRequestData parseData(List children) throws IUMInterfaceException {
		
		LOGGER.info("parseData start");
		new CodeHelper();
		AssessmentRequestData ard = new AssessmentRequestData();
		ClientData cData = new ClientData();
		new ExceptionLog();
		new ArrayList();
		new StringBuffer();
//		try {
			for (int i=0; i <children.size(); i++) {
				Element child = (Element)children.get(i);
				String attribute = child.getName();
				String value = child.getText();
				if (attribute == null) {
					attribute = "";
				}
				if (value == null) {
					value = "";
				} else {
					value = value.trim();
				}
				if (attribute.equalsIgnoreCase("POL_ID")) {
					ard.setReferenceNumber(value.toUpperCase());
				} else if (attribute.equalsIgnoreCase("CVG_FACE_AMT")) {
					try{
						ard.setAmountCovered(Double.parseDouble(value));
					}catch(NumberFormatException nfe){

					}
				} else if (attribute.equalsIgnoreCase("POL_MPREM_AMT")) {
					try{
						ard.setPremium(Double.parseDouble(value));
					}catch(NumberFormatException nfe){

					}
				} else if (attribute.equalsIgnoreCase("APP_RECV_DT")) {
					ard.setApplicationReceivedDate(DateHelper.parse(value,IUMMessageUtility.DEFAULT_DATE_FORMAT));
				} else if (attribute.equalsIgnoreCase("NB_CNTCT_USER_ID")) {
					UserProfileData upd1 = new UserProfileData();
					upd1.setACF2ID(value);
					ard.setAssignedTo(upd1);	
					ard.setFolderLocation(upd1);	
				} else if (attribute.equalsIgnoreCase("UW_USER_ID")) {		
					UserProfileData upd2 = new UserProfileData();
					upd2.setACF2ID(value);
					ard.setUnderwriter(upd2);									
				} else if (attribute.equalsIgnoreCase("INSR_CLI_ID")) {
						cData.setClientId(value.toUpperCase());
				} else if (attribute.equalsIgnoreCase("INSR_SUR_NM")) {
					cData.setLastName(value);					
				} else if (attribute.equalsIgnoreCase("INSR_GIV_NM")) {
					cData.setGivenName(value);
				} else if (attribute.equalsIgnoreCase("INSR_MID_NM")) {
					cData.setMiddleName(value);
				} else if (attribute.equalsIgnoreCase("INSR_TITL_TXT")) {
					cData.setTitle(value);
				} else if (attribute.equalsIgnoreCase("INSR_SFX_NM")) {
					cData.setSuffix(value);
				} else if (attribute.equalsIgnoreCase("INSR_BTH_DT")) {
					cData.setBirthDate(DateHelper.parse(value,IUMMessageUtility.DEFAULT_DATE_FORMAT));
				} else if (attribute.equalsIgnoreCase("INSR_BTH_LOC")) {
					cData.setBirthLocation(value);
				} else if (attribute.equalsIgnoreCase("INSR_SEX_CD")) {
					cData.setSex(value);
				} else if (attribute.equalsIgnoreCase("INSR_CLI_AGE")) {
					try{
						cData.setAge(Integer.parseInt(value));
					}catch(NumberFormatException nfe){
						
					}					
				} else if (attribute.equalsIgnoreCase("AGT_ID")) {
					// TODO: Yves verify if this is the correct method to invoke
					UserProfileData upd3 = new UserProfileData();
					upd3.setUserId(value);
					ard.setAgent(upd3);
				} else if (attribute.equalsIgnoreCase("SOFF_ID")) {
					SunLifeOfficeData sod = new SunLifeOfficeData();
					sod.setOfficeId(value);
					ard.setBranch(sod);
				} else if (attribute.equalsIgnoreCase("KO_CODE")) { //add this to xml
					ard.setKickOutCode(value);						//
				} else {
					
				}
					
			}
			
					
			ard.setInsured(cData);
			ard.setOwner(cData);
			setPrismSpecificDetails(ard);
			
			ard.setXmlRecord(writeXML(children));
			LOGGER.info("parseData end");
		return ard;
	}
	
	
	private ExceptionLog verifyData(AssessmentRequestData ard){
		
		LOGGER.info("verifyData start");
		CodeHelper ch = new CodeHelper();
		ExceptionLog ex = new ExceptionLog();
		ArrayList arr = new ArrayList();
		if(ard.getReferenceNumber()==null || ard.getReferenceNumber().equals("")){
			ExceptionDetail ed = new ExceptionDetail();
			ed.setMessage("Reference Number is required.");
			arr.add(ed);
		}
		if(ard.getAmountCovered()<0){
			ExceptionDetail ed = new ExceptionDetail();
			ed.setMessage("Invalid value for amount covered.");
			arr.add(ed);
		}
		if(ard.getPremium()<0){
			ExceptionDetail ed = new ExceptionDetail();
			ed.setMessage("Invalid value for premium.");
			arr.add(ed);
		}	
		if(ard.getApplicationReceivedDate()==null){
			ExceptionDetail ed = new ExceptionDetail();
			ed.setMessage("Application Received Date is required (YYYY-MM-DD)");
			arr.add(ed);
		}	
		
		UserProfileData at = ard.getAssignedTo();
		String atAcf2id = null;
		try{
			atAcf2id = at.getACF2ID();
			at = ch.getUserProfileByACF2ID(atAcf2id);
			if(at!=null && at.getUserId()!=null){
				ard.setAssignedTo(at);
				ard.setFolderLocation(at);				
			}else{
				ExceptionDetail ed = new ExceptionDetail();
				ed.setMessage("User with ID " + atAcf2id + " not found in IUM");
				arr.add(ed);
			}
		}catch(SQLException e){
			ExceptionDetail ed = new ExceptionDetail();
			ed.setMessage(e.getMessage());
			arr.add(ed);
		}

		UserProfileData agent = ard.getAgent();
		String agentId = null;
		try{
			agentId = agent.getUserId();
			agent = ch.getUserProfile(agentId);
			if(agent!=null && agent.getUserId()!=null){
				ard.setAgent(agent);
			}else{
				ExceptionDetail ed = new ExceptionDetail();
				ed.setMessage("User with ID " + agentId + " not found in IUM");
				arr.add(ed);				
			}
		}catch(SQLException e){
			ExceptionDetail ed = new ExceptionDetail();
			ed.setMessage(e.getMessage());
			arr.add(ed);
		}
		
		SunLifeOfficeData branch = ard.getBranch();
		String branchId = branch.getOfficeId();
		try{
			branch = ch.getSunLifeOffice(branchId);
			if(branch!=null){
				ard.setBranch(branch);
			}else{
				ExceptionDetail ed = new ExceptionDetail();
				ed.setMessage("Invalid Sunlife Office");
				arr.add(ed);
			}
		}catch(SQLException e){
			ExceptionDetail ed = new ExceptionDetail();
			ed.setMessage(e.getMessage());
			arr.add(ed);
		}		
				
		ClientData cd = ard.getInsured();
		if(cd.getClientId()==null || cd.getClientId().trim().equals("")){
			ExceptionDetail ed = new ExceptionDetail();
			ed.setMessage("Client ID is required");
			arr.add(ed);			
		}
		if(cd.getLastName()==null || cd.getLastName().trim().equals("")){
			ExceptionDetail ed = new ExceptionDetail();
			ed.setMessage("Client's lastname is required");
			arr.add(ed);			
		}
		if(cd.getGivenName()==null || cd.getGivenName().trim().equals("")){
			ExceptionDetail ed = new ExceptionDetail();
			ed.setMessage("Client's given name is required");
			arr.add(ed);			
		}
		
		if(arr.size()>0){
			ex.setDetails(arr);
			ex.setXmlRecord(ard.getXmlRecord());
		}else{
			ex = null;
		}
		LOGGER.info("verifyData end");
		return ex;
	}

	
	private void setPrismSpecificDetails(AssessmentRequestData ard) {
		
		LOGGER.info("setPrismSpecificDetails start");
		StatusData sd = new StatusData();
		sd.setStatusId(IUMConstants.STATUS_NB_REVIEW_ACTION); // TODO: YVES what should be the status id
		LOBData lob = new LOBData();
		lob.setLOBCode(IUMConstants.LOB_PRE_NEED);
		
		ard.setStatus(sd);
		ard.setLob(lob);
		ard.setSourceSystem(IUMConstants.SYSTEM_PRISM); 
		ard.setStatusDate(new Date());
		ard.setCreatedDate(new Date());
		
		UserProfileData upd = new UserProfileData();
		upd.setUserId(IUMConstants.USER_IUM);
		ard.setCreatedBy(upd);
		ard.setFolderLocation(ard.getAssignedTo());
		
		LOGGER.info("setPrismSpecificDetails end");
	}
	
	private String writeXML(List children){
		
		LOGGER.info("writeXML start");
		StringBuffer sb = new StringBuffer();
		for(int i=0; i<children.size(); i++){
			Element e = (Element)children.get(i);
			sb.append("<");
			sb.append(e.getName());
			sb.append(">");
			sb.append(e.getText());	
			sb.append("</");
			sb.append(e.getName());
			sb.append(">");
		}
		LOGGER.info("writeXML end");
		return sb.toString();		
	}
						
	


	/* (non-Javadoc)
	 * @see javax.ejb.SessionBean#setSessionContext(javax.ejb.SessionContext)
	 */
	public void setSessionContext(SessionContext arg0) throws EJBException, RemoteException {
		// TODO Auto-generated method stub
		
	}


	/* (non-Javadoc)
	 * @see javax.ejb.SessionBean#ejbRemove()
	 */
	public void ejbRemove() throws EJBException, RemoteException {
		// TODO Auto-generated method stub
		
	}


	/* (non-Javadoc)
	 * @see javax.ejb.SessionBean#ejbActivate()
	 */
	public void ejbActivate() throws EJBException, RemoteException {
		// TODO Auto-generated method stub
		
	}


	/* (non-Javadoc)
	 * @see javax.ejb.SessionBean#ejbPassivate()
	 */
	public void ejbPassivate() throws EJBException, RemoteException {
		// TODO Auto-generated method stub
		
	}
    public void ejbCreate() throws RemoteException, CreateException{

    }
}
