package com.slocpi.ium.interfaces.prism;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Hashtable;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.rmi.PortableRemoteObject;
import com.slocpi.ium.interfaces.prism.ejb.PRISMTranslator;
import com.slocpi.ium.interfaces.prism.ejb.PRISMTranslatorHome;

/*
 * Created on May 25, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */

/**
 * @author ypojol
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class PrismClient {
	
	public static void main(String args[]){
	
		//System.out.println("client started...");
		try {
			String str = null;
			String providerURL = "ormi://192.168.0.101:3101/ium";
			String principal = "admin";
			String credential = "password";
			String messageFile = "prism.xml";
			String ejbName = "ejb/PRISMTranslatorBean";
			if(args.length >3){
				providerURL = args[0];
				principal = args[1];
				credential = args[2];
				messageFile = args[3];
				ejbName = args[4];
				FileReader fr = new FileReader(messageFile);
				BufferedReader br = new BufferedReader(fr);
				String line = null;
				while((line=br.readLine())!=null){
					if(str!=null){
						str += line;
					}else{
						str = line;
					}					
				}
			}else{
				System.out.println("arguments : provider-URL = " + providerURL);
				System.out.println("arguments : principal = " + principal);
				System.out.println("arguments : password = " + credential);
				System.out.println("arguments : file = " + messageFile);
				System.out.println("arguments : ejb-name = " + ejbName);
				//System.exit(0);
			}
		  /**
		   * initialize JNDI context by setting factory, url and credential
		   * in a hashtable
		   */

		  Hashtable env = new Hashtable();
		  env.put(Context.INITIAL_CONTEXT_FACTORY, "com.evermind.server.rmi.RMIInitialContextFactory");
		  env.put(Context.PROVIDER_URL, providerURL);
		  env.put(Context.SECURITY_PRINCIPAL, principal);
		  env.put(Context.SECURITY_CREDENTIALS, credential);

		  Context context = new InitialContext(env);
      
		  /**
		   * Lookup the EmployeeHome object. The reference is retrieved from the
		   * application-local context (java:comp/env). The variable is
		   * specified in the assembly descriptor (META-INF/application-client.xml).
		   */
		  Object homeObject = context.lookup(ejbName);
		  
		  // Narrow the reference to EmployeeHome.
		  com.slocpi.ium.interfaces.prism.ejb.PRISMTranslatorHome home = (com.slocpi.ium.interfaces.prism.ejb.PRISMTranslatorHome)PortableRemoteObject.narrow(homeObject, PRISMTranslatorHome.class);
		  System.out.println("Client.main(): home narrowed...");

		  //Create remote object and narrow the reference to Employee.
		  PRISMTranslator remote = (PRISMTranslator)PortableRemoteObject.narrow(home.create(), PRISMTranslator.class);
		  /*
		  StringBuffer sb = new StringBuffer();
		  sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\" ?>");
		  sb.append("<ROOT><POL_QUEUE_INFO>");
		  sb.append("<POL_ID>veck2</POL_ID>");
		  sb.append("<CVG_FACE_AMT>100000.00</CVG_FACE_AMT>");
		  sb.append("<POL_MPREM_AMT>WWW</POL_MPREM_AMT>");
		  sb.append("<APP_RECV_DT>2004-05-04</APP_RECV_DT>");
		  sb.append("<NB_CNTCT_USER_ID>ACF2</NB_CNTCT_USER_ID>");
		  sb.append("<UW_USER_ID>P782</UW_USER_ID>");
		  sb.append("<INSR_CLI_ID>PRISM10</INSR_CLI_ID>");
		  sb.append("<INSR_SUR_NM>emon2</INSR_SUR_NM>");
		  sb.append("<INSR_GIV_NM>f1</INSR_GIV_NM>");
		  sb.append("<INSR_MID_NM>MN</INSR_MID_NM>");
		  sb.append("<INSR_TITL_TXT>MR</INSR_TITL_TXT>");
		  sb.append("<INSR_SFX_NM>JR</INSR_SFX_NM>");
		  sb.append("<INSR_BTH_DT>1980-01-01</INSR_BTH_DT>");
		  sb.append("<INSR_BTH_LOC>MAKATI</INSR_BTH_LOC>");
		  sb.append("<INSR_SEX_CD>M</INSR_SEX_CD>");
		  sb.append("<INSR_CLI_AGE>24</INSR_CLI_AGE>");
		  sb.append("<AGT_ID>DRAMOS</AGT_ID>");
		  sb.append("<SOFF_ID>SUNLIFEHO</SOFF_ID>");
		  sb.append("<KO_CODE>XFPPPFPPLLMNPPPNNPP000000</KO_CODE>");
		  sb.append("</POL_QUEUE_INFO></ROOT>");	
		  //StringBuffer sbReply = remote.sendData(sb);
		  //System.out.println("Receipt" + sbReply);
		  */
		  StringBuffer sb1 = new StringBuffer();
		  sb1.append("<?xml version=\"1.0\" encoding=\"UTF-8\" ?><ROOT><POL_QUEUE_INFO><POL_ID>yves123</POL_ID>" +
		  "<CVG_FACE_AMT>100000.00</CVG_FACE_AMT><POL_MPREM_AMT>WWW</POL_MPREM_AMT><APP_RECV_DT>2004-05-04" +
		  "</APP_RECV_DT><NB_CNTCT_USER_ID>ACF2</NB_CNTCT_USER_ID><UW_USER_ID>P782</UW_USER_ID><INSR_CLI_ID>" +
		  "PRISM10</INSR_CLI_ID><INSR_SUR_NM>emon2</INSR_SUR_NM><INSR_GIV_NM>f1</INSR_GIV_NM><INSR_MID_NM>" +
		  "MN</INSR_MID_NM><INSR_TITL_TXT>MR</INSR_TITL_TXT><INSR_SFX_NM>JR</INSR_SFX_NM><INSR_BTH_DT>1980-01-01" +
		  "</INSR_BTH_DT><INSR_BTH_LOC>MAKATI</INSR_BTH_LOC><INSR_SEX_CD>M</INSR_SEX_CD><INSR_CLI_AGE>24" +
		  "</INSR_CLI_AGE><AGT_ID>DRAMOS</AGT_ID><SOFF_ID>SUNLIFEHO</SOFF_ID><KO_CODE>" +
		  "XFPPPFPPLLMNPPPNNPP000000</KO_CODE></POL_QUEUE_INFO><POL_QUEUE_INFO><POL_ID>P00000000004444</POL_ID>" +
		  "<CVG_FACE_AMT>475200.00</CVG_FACE_AMT><POL_MPREM_AMT>9028.80</POL_MPREM_AMT><APP_RECV_DT>" +
		  "May  4 2004 12:00AM</APP_RECV_DT><NB_CNTCT_USER_ID>super</NB_CNTCT_USER_ID><UW_USER_ID>        " +
		  "</UW_USER_ID><INSR_CLI_ID>0000005312</INSR_CLI_ID><INSR_SUR_NM>Cruz</INSR_SUR_NM><INSR_GIV_NM>" +
		  "Mary Melody</INSR_GIV_NM><INSR_MID_NM>V</INSR_MID_NM><AGT_ID>3000000041</AGT_ID><SOFF_ID>     </SOFF_ID>" +
		  "<KO_CODE>GFPPPPPPP3PPPFPPNPP000000</KO_CODE></POL_QUEUE_INFO></ROOT>	");		  
		  
		  
		  StringBuffer sb = new StringBuffer();
		  sb.append(str);
		  StringBuffer sbReply = remote.sendData(sb);
		  System.out.println("Receipt : " + sbReply);
		
		}catch(Exception e){
			e.printStackTrace();
		}

	}	


}
