/**
 * PRISMTranslator.java				Copyright (c) 2004 Pointwest Technologies Corp.
 * TODO DOCUMENT ME!
 * 
 * @author raymond.posadas	 @date May 24, 2004
 */
package com.slocpi.ium.interfaces.prism.ejb;

import java.rmi.RemoteException;
import javax.ejb.EJBObject;

/**
 * TODO DOCUMENT ME!
 * 
 * @author raymond.posadas		Copyright (c) 2004 Pointwest Technologies Corp.
 * @date May 24, 2004
 */
public interface PRISMTranslator extends EJBObject {
    public StringBuffer sendData(StringBuffer sb) throws RemoteException;
}
