/*
 * Created on Dec 23, 2003
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.interfaces;

/**
 * @author daguila
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class IUMInterfaceException extends Exception {

	public IUMInterfaceException (String msg) {
		super(msg);
	}
	
	public IUMInterfaceException (Exception e) {
		super(e.getMessage());
	}
}
