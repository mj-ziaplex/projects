/*
 * Created on Jan 25, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.slocpi.ium.data.AssessmentRequestData;
import com.slocpi.ium.data.ClientData;
import com.slocpi.ium.data.LOBData;
import com.slocpi.ium.data.StatusData;
import com.slocpi.ium.data.SunLifeOfficeData;
import com.slocpi.ium.data.UserProfileData;
import com.slocpi.ium.util.DateHelper;

/**
 * @author daguila
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class IUMTestUtil {

	private Connection conn;
		
	public IUMTestUtil (Connection _conn) {
		conn = _conn;	
	}
	
	/*public ArrayList getRequestList(String policies) throws SQLException{
		String sql = 	"SELECT"+ 
			" REFERENCE_NUM as REFERENCE_NUM," + 
			" AR.LOB as LOB_CODE," + 
			" LB.LOB_DESC as LOB_DESC,"+
			" AR.SOURCE_SYSTEM as SOURCE_SYSTEM," +
			" AR.AMOUNT_COVERED as AMOUNT_COVERED," + 
			" AR.PREMIUM as PREMIUM, " +
			" AR.INSURED_CLIENT_ID as INSURED_ID," +
			" INSURED.CL_GIVEN_NAME as INSURED_GIVEN_NAME," +	
			" INSURED.CL_LAST_NAME as INSURED_LAST_NAME," +		
			" AR.OWNER_CLIENT_ID as OWNER_ID," +
			" OWNER.CL_GIVEN_NAME as OWNER_GIVEN_NAME," + 
			" OWNER.CL_LAST_NAME as OWNER_LAST_NAME,"+
			" AR.BRANCH_ID as BRANCH_CODE," +
			" BR.SLO_OFFICE_NAME as BRANCH_NAME," +		
			" AR.AGENT_ID as AGENT_ID," + 
			" AGNT.USR_FIRST_NAME as AGENT_FIRST_NAME," +
			" AGNT.USR_LAST_NAME as AGENT_LAST_NAME," +  
			" AR.STATUS_ID as STATUS_ID," +
			" ST.STAT_DESC as STATUS_DESC," +
			" AR.STATUS_DATE as STATUS_DATE," + 
			" AR.ASSIGNED_TO as ASSIGNED_TO_CODE," +
			" ASSIGN.USR_FIRST_NAME AS ASSIGNED_TO_FIRST_NAME," +
			" ASSIGN.USR_LAST_NAME as ASSIGNED_TO_LAST_NAME," + 
			" AR.UNDERWRITER as UW_CODE," + 
			" UW.USR_FIRST_NAME AS UW_FIRST_NAME," +
			" UW.USR_LAST_NAME as UW_LAST_NAME," + 
			" AR.FOLDER_LOCATION as LOC_CODE," +
			" LOC.USR_FIRST_NAME AS LOC_LAST_NAME," +
			" LOC.USR_LAST_NAME as LOC_FIRST_NAME, " +
			" AR.REMARKS as REMARKS," +
			" AR.APP_RECEIVED_DATE as RECEIVED_DATE," +
			" AR.DATE_FORWARDED as DATE_FORWARDED," +
			" AR.CREATED_BY as CREATED_BY_CODE," +
			" CREATED_BY.USR_FIRST_NAME AS CREATED_BY_FIRST_NAME," +
			" CREATED_BY.USR_LAST_NAME as CREATED_BY_LAST_NAME," + 
			" AR.CREATED_DATE as CREATED_DATE," +
			" AR.UPDATED_BY as UPDATED_BY_CODE," + 
			" UPDATED_BY.USR_FIRST_NAME AS UPDATED_BY_FIRST_NAME," +
			" UPDATED_BY.USR_LAST_NAME as UPDATED_BY_LAST_NAME," +  
			" AR.UPDATED_DATE as UPDATED_DATE" +
		" FROM" +
			" CLIENTS OWNER," + 
			" CLIENTS INSURED," +
			" ASSESSMENT_REQUESTS AR," + 
			" LINES_OF_BUSINESS LB," + 
			" SUNLIFE_OFFICES BR," +
			" USERS AGNT," +
			" STATUS ST," +
			" USERS ASSIGN," +
			" USERS UW," +
			" USERS LOC," +
			" USERS CREATED_BY," +
			" USERS UPDATED_BY" +
		" WHERE" +
			" AR.OWNER_CLIENT_ID=OWNER.CLIENT_ID (+) AND" + 
			" AR.INSURED_CLIENT_ID=INSURED.CLIENT_ID (+) AND" + 
			" AR.LOB = LB.LOB_CODE (+) AND " +
			" AR.BRANCH_ID = BR.SLO_OFFICE_CODE (+) AND" +
			" AR.AGENT_ID = AGNT.USER_ID (+) AND" +
			" AR.STATUS_ID = ST.STAT_ID (+) AND" +
			" AR.ASSIGNED_TO = ASSIGN.USER_ID (+) AND" +
			" AR.UNDERWRITER = UW.USER_ID (+) AND" +
			" AR.FOLDER_LOCATION = LOC.USER_ID (+) AND" +
			" AR.CREATED_BY = CREATED_BY.USER_ID (+) AND" +
			" AR.UPDATED_BY = UPDATED_BY.USER_ID (+) AND" +
			" AR.REFERENCE_NUM IN (" + policies + ")"; 
			
		PreparedStatement ps = conn.prepareStatement(sql);
		ResultSet rs = ps.executeQuery();
		
		
		ArrayList data = new ArrayList();
		while(rs.next()){
			AssessmentRequestData assessment = new AssessmentRequestData();
			
			assessment.setReferenceNumber(	rs.getString("REFERENCE_NUM")	);			
				LOBData lob = new LOBData();
				lob.setLOBCode(		rs.getString("LOB_CODE") 	);
				lob.setLOBDesc(		rs.getString("LOB_DESC")	);
			assessment.setLob( lob );
			assessment.setSourceSystem(		rs.getString("SOURCE_SYSTEM")	);
			assessment.setAmountCovered(	rs.getDouble("AMOUNT_COVERED")	);
			assessment.setPremium(	rs.getDouble("PREMIUM")	);
				ClientData insured = new ClientData();
				insured.setClientId(	rs.getString("INSURED_ID") 	);
				insured.setGivenName( 	rs.getString("INSURED_GIVEN_NAME")	);
				insured.setLastName( 	rs.getString("INSURED_LAST_NAME")	);
			assessment.setInsured( insured );
				ClientData owner = new ClientData();
				owner.setClientId(		rs.getString("OWNER_ID") 	);
				owner.setGivenName( 	rs.getString("OWNER_GIVEN_NAME")	);
				owner.setLastName( 		rs.getString("OWNER_LAST_NAME")	);
			assessment.setOwner( owner );
				SunLifeOfficeData branch = new SunLifeOfficeData();
				branch.setOfficeId(		rs.getString("BRANCH_CODE")	);
				branch.setOfficeName(	rs.getString("BRANCH_NAME")	);
			assessment.setBranch( branch );
				UserProfileData agent = new UserProfileData();			 				
				agent.setUserId(	rs.getString("AGENT_ID")		);
				agent.setFirstName(	rs.getString("AGENT_FIRST_NAME")	);
				agent.setLastName(	rs.getString("AGENT_LAST_NAME")		);
			assessment.setAgent(agent);
				StatusData status = new StatusData();				
				status.setStatusId(Long.parseLong(rs.getString("STATUS_ID")));
				status.setStatusDesc(	rs.getString("STATUS_DESC")	);
			assessment.setStatus(status);
			assessment.setStatusDate(  DateHelper.utilDate(		rs.getDate("STATUS_DATE") ) );
				UserProfileData assignedTo = new UserProfileData();			 				
				assignedTo.setUserId(		rs.getString("ASSIGNED_TO_CODE")		);
				assignedTo.setFirstName(	rs.getString("ASSIGNED_TO_FIRST_NAME")	);
				assignedTo.setLastName(		rs.getString("ASSIGNED_TO_LAST_NAME")	);
			assessment.setAssignedTo( assignedTo );
				UserProfileData uw = new UserProfileData();			 				
				uw.setUserId(		rs.getString("UW_CODE")		);
				uw.setFirstName(	rs.getString("UW_FIRST_NAME")	);
				uw.setLastName(		rs.getString("UW_LAST_NAME")	);
			assessment.setUnderwriter( uw );		
				UserProfileData location = new UserProfileData();			 				
				location.setUserId(		rs.getString("LOC_CODE")		);
				location.setFirstName(	rs.getString("LOC_FIRST_NAME")	);
				location.setLastName(		rs.getString("LOC_LAST_NAME")	);
			assessment.setFolderLocation( location );
			assessment.setRemarks( rs.getString("REMARKS") );
			assessment.setApplicationReceivedDate( DateHelper.utilDate( rs.getDate("RECEIVED_DATE") ) );
			assessment.setForwardedDate( DateHelper.utilDate(rs.getDate("DATE_FORWARDED") ) );
				UserProfileData createdBy = new UserProfileData();			 				
				createdBy.setUserId(		rs.getString("CREATED_BY_CODE")		);
				createdBy.setFirstName(	rs.getString("CREATED_BY_FIRST_NAME")	);
				createdBy.setLastName(		rs.getString("CREATED_BY_LAST_NAME")	);
			assessment.setCreatedBy( createdBy );
			assessment.setCreatedDate( DateHelper.utilDate(rs.getDate("CREATED_DATE") ) );
				UserProfileData updatedBy = new UserProfileData();			 				
				updatedBy.setUserId(		rs.getString("UPDATED_BY_CODE")		);
				updatedBy.setFirstName(	rs.getString("UPDATED_BY_FIRST_NAME")	);
				updatedBy.setLastName(		rs.getString("UPDATED_BY_LAST_NAME")	);
			assessment.setUpdatedBy( updatedBy );
			assessment.setUpdatedDate( DateHelper.utilDate(rs.getDate("UPDATED_DATE") ) );			

			data.add(assessment);			
		}
		return data;
	}*/
//	public static void main(String[] args){
//		String s = "123";
//		int i=0;
//		try{
//			i = Integer.parseInt(s);
//			if(i!=0){
//				System.out.println(i);
//			}
//		} catch(NumberFormatException e){
//			if(!s.equals("Y")){
//				System.out.println(i);
//			}
//		}
//	}
}
