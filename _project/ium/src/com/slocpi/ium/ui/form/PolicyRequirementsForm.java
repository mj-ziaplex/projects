package com.slocpi.ium.ui.form;



/**
 * This class contains the requirement details.
 * @author: Ma. Theresa Vicencio
 * @version: 1.0
 */
public class PolicyRequirementsForm extends AssessmentRequestForm {

  private String seq = "";
  private String reqCode = "";
  private String reqCodeValue = "";
  private String reqDesc = "";
  private String reqLevel = "";
  private String createdDate = "";
  private String completeReq = "";
  private String reqStatusCode = "";
  private String reqStatusDesc = "";  
  private String reqStatusValue = "";
  private String statusDate = "";
  private String updatedBy = "";
  private String updatedDate = "";  
  private String clientType = "";
  private String designation = "";
  private String testDate = "";
  private String testResult = "";
  private String testDesc = "";
  private String resolve = "";
  private String followUpNo = "";
  private String followUpDate = "";
  private String validityDate = "";
  private String paidInd = "";
  private String newTest = "";
  private String orderDate = "";
  private String dateSent = "";
  private String ccasSuggest = "";
  private String receivedDate = "";
  private String autoOrder = "";
  private String fldComm = "";
  private String comment = "";  
  private String clientNo = "";
  private String reqId = "";
  private String formId = null;
  private String validity = "";	
  private String pageNumber = "";
  private String testDateIndicator = "0";
  private String imageReferenceNum = "";
  private String wmsImageLink = "";
  private String orderRequirementsTag = "";
  private String reqFollowUp = "";
  
    
  private String refNo = null;
  private String statusTo = null;
  private String followUpDateTo = null;
  private String commentTo = null;
  private String[] reqIds = null;
  private String[] statusCodes = null;
  private String[] indexTemp = null;
  private String[] followUpNos = null;
  private String[] seqs = null;
  private String[] followUpDates = null;
  private String[] reqLevels = null;
  private String[] reqCodes = null;
  private String[] dateSents = null;
  
  private String[] reqCodeArr = null;
  private String[] reqCodeValueArr = null;
  private String[] clientTypeArr = null;
  private String[] seqArr = null;
  private String[] designationArr = null;
  private String[] validityDateArr = null;
  private String[] testDateArr = null;
  private String[] followUpNoArr = null;
  private String[] resolveArr = null;
  private String[] paidIndArr = null;
  private String[] newTestArr = null;
  private String[] autoOrderArr = null;
  private String[] fldCommArr = null;
  private String[] commentArr = null;
  private String[] reqLevelArr = null;
  private String[] reqFollowUpArr = null;

/**
   * Sets the sequence number attribute.
   * @param seq sequence no. of the requirement code
   */  
  public void setSeq(String seq) {
	this.seq = seq;
  }

  /**
   * Retrieves the sequence number attribute.
   * @return String sequence no. of the requirement code
   */
  public String getSeq() {
	return (this.seq);
  }
  
  /**
   * Sets the requirement code attribute.
   * @param reqCode requirement code
   */  
  public void setReqCode(String reqCode) {
	this.reqCode = reqCode;
  }

  /**
   * Retrieves the requirement code attribute.
   * @return String requirement code
   */
  public String getReqCode() {
	return (this.reqCode);
  }

  /**
   * Sets the requirement description attribute.
   * @param reqDesc requirement description
   */	
  public void setReqDesc(String reqDesc) {
	this.reqDesc = reqDesc;
  }
	
  /**
   * Retrieves the requirement description attribute.
   * @return String requirement description
   */
  public String getReqDesc(){
	return (this.reqDesc);
  }

  /**
   * Sets the requirement level attribute.
   * @param reqLevel requirement level
   */	
  public void setReqLevel(String reqLevel) {
	this.reqLevel = reqLevel;
  }
	
  /**
   * Retrieves the requirement level attribute.
   * @return String requirement level
   */
  public String getReqLevel(){
	return (this.reqLevel);
  }
  
  /**
   * Sets the crated date attribute.
   * @param createdDate date when requirement was created
   */	
  public void setCreatedDate(String createdDate) {
	this.createdDate = createdDate;
  }
	
  /**
   * Retrieves the created date attribute.
   * @return String date when requirement was created
   */
  public String getCreatedDate(){
	return (this.createdDate);
  }
  
  /**
   * Sets the completed requirement attribute.
   * @param completeReq set to true if the requirement has been submitted prior to the creation of the record
   */	
  public void setCompleteReq(String completeReq) {
	this.completeReq = completeReq;
  }
	
  /**
   * Retrieves the comlpeted requirement attribute.
   * @return String set to true if the requirement has been submitted prior to the creation of the record
   */
  public String getCompleteReq(){
	return (this.completeReq);
  }  
      
  /**
   * Sets the requirement status code attribute.
   * @param reqStatusCode current status of the requirement
   */	
  public void setReqStatusCode(String reqStatusCode) {
	this.reqStatusCode = reqStatusCode;
  }
	
  /**
   * Retrieves the requirement status code attribute.
   * @return String current status of the requirement
   */
  public String getReqStatusCode(){
	return (this.reqStatusCode);
  }

  /**
   * Sets the status date attribute.
   * @param statusDate date when status was updated
   */	
  public void setStatusDate(String statusDate) {
	this.statusDate = statusDate;
  }
	
  /**
   * Retrieves the status date attribute.
   * @return String date when status was updated
   */
  public String getStatusDate(){
	return (this.statusDate);
  }

  /**
   * Sets the update by attribute.
   * @param updatedBy user who last updated the record
   */	
  public void setUpdatedBy(String updatedBy) {
	this.updatedBy = updatedBy;
  }
	
  /**
   * Retrieves the update by attribute.
   * @return String user who last updated the record
   */
  public String getUpdatedBy(){
	return (this.updatedBy);
  }

  /**
   * Sets the update date attribute.
   * @param updatedDate date when record was last updated
   */	
  public void setUpdatedDate(String updatedDate) {
	this.updatedDate = updatedDate;
  }
	
  /**
   * Retrieves the update date attribute.
   * @return String date when record was last updated
   */
  public String getUpdatedDate(){
	return (this.updatedDate);
  }
  
  /**
   * Sets the client type attribute.
   * @param clientType client type
   */	
  public void setClientType(String clientType) {
	this.clientType = clientType;
  }
	
  /**
   * Retrieves the client type attribute.
   * @return String client type
   */
  public String getClientType(){
	return (this.clientType);
  }  
  
  /**
   * Sets the designation attribute.
   * @param designation designation
   */	
  public void setDesignation(String designation) {
	this.designation = designation;
  }
	
  /**
   * Retrieves the designation attribute.
   * @return String designation
   */
  public String getDesignation(){
	return (this.designation);
  }
  
  /**
   * Sets the test date attribute.
   * @param testDate date test is conducted
   */	
  public void setTestDate(String testDate) {
	this.testDate = testDate;
  }
	
  /**
   * Retrieves the test date attribute.
   * @return String date test is conducted
   */
  public String getTestDate(){
	return (this.testDate);
  }
  
  /**
   * Sets the test result attribute.
   * @param testResult test results
   */	
  public void setTestResult(String testResult) {
	this.testResult = testResult;
  }
	
  /**
   * Retrieves the test result attribute.
   * @return String test results
   */
  public String getTestResult(){
	return (this.testResult);
  }  
  
  /**
   * Sets the test description attribute.
   * @param testDesc test description
   */	
  public void setTestDesc(String testDesc) {
	this.testDesc = testDesc;
  }
	
  /**
   * Retrieves the test description attribute.
   * @return String test description
   */
  public String getTestDesc(){
	return (this.testDesc);
  }  
  
  /**
   * Sets the resolve attribute.
   * @param resolve resolve
   */	
  public void setResolve(String resolve) {
	this.resolve = resolve;
  }
	
  /**
   * Retrieves the resolve attribute.
   * @return String resolve
   */
  public String getResolve(){
	return (this.resolve);
  }  

  /**
   * Sets the follow up number attribute.
   * @param followUpNo follow up number which indicates the number of days given for the requirement to be submitted
   */	
  public void setFollowUpNo(String followUpNo) {
	this.followUpNo = followUpNo;
  }
	
  /**
   * Retrieves the follow up number attribute.
   * @return String follow up number which indicates the number of days given for the requirement to be submitted
   */
  public String getFollowUpNo(){
	return (this.followUpNo);
  }  

  /**
   * Sets the follow up date attribute.
   * @param followUpDate date when the requirement will be followed up
   */	
  public void setFollowUpDate(String followUpDate) {
	this.followUpDate = followUpDate;
  }
	
  /**
   * Retrieves the follow up date attribute.
   * @return String date when the requirement will be followed up
   */
  public String getFollowUpDate(){
	return (this.followUpDate);
  }
  
  /**
   * Sets the validity date attribute.
   * @param validityDate validity date of the requirement
   */	
  public void setValidityDate(String validityDate) {
	this.validityDate = validityDate;
  }
	
  /**
   * Retrieves the validity date attribute.
   * @return String validity date of the requirement
   */
  public String getValidityDate(){
	return (this.validityDate);
  }
  
  /**
   * Sets the paid indicator attribute.
   * @param paidInd signifies if the requirement has been paid or not
   */	
  public void setPaidInd(String paidInd) {
	this.paidInd = paidInd;
  }
	
  /**
   * Retrieves the paid indicator attribute.
   * @return String signifies if the requirement has been paid or not
   */
  public String getPaidInd(){
	return (this.paidInd);
  }
  
  /**
   * Sets the new test attribute.
   * @param newTest new test only
   */	
  public void setNewTest(String newTest) {
	this.newTest = newTest;
  }
	
  /**
   * Retrieves the new test attribute.
   * @return String new test only
   */
  public String getNewTest(){
	return (this.newTest);
  }  
  
  /**
   * Sets the order date attribute.
   * @param orderDate date when status was changed to 'ordered'
   */	
  public void setOrderDate(String orderDate) {
	this.orderDate = orderDate;
  }
	
  /**
   * Retrieves the order date attribute.
   * @return String date when status was changed to 'ordered'
   */
  public String getOrderDate(){
	return (this.orderDate);
  }  

  /**
   * Sets the date sent attribute.
   * @param dateSent date when email/sms notification was sent
   */	
  public void setDateSent(String dateSent) {
	this.dateSent = dateSent;
  }
	
  /**
   * Retrieves the date sent date attribute.
   * @return String date when email/sms notification was sent
   */
  public String getDateSent(){
	return (this.dateSent);
  }  
  
  /**
   * Sets the CCAS suggest attribute.
   * @param ccasSuggest CCAS suggest
   */	
  public void setCcasSuggest(String ccasSuggest) {
	this.ccasSuggest = ccasSuggest;
  }
	
  /**
   * Retrieves the CCAS suggest attribute.
   * @return String CCAS suggest
   */
  public String getCcasSuggest(){
	return (this.ccasSuggest);
  }  
  
  /**
   * Sets the received date attribute.
   * @param receivedDate date when status was changed to 'received'
   */	
  public void setReceivedDate(String receivedDate) {
	this.receivedDate = receivedDate;
  }
	
  /**
   * Retrieves the received date attribute.
   * @return String date when status was changed to 'received'
   */
  public String getReceivedDate(){
	return (this.receivedDate);
  }  
  
  /**
   * Sets the auto-order attribute.
   * @param autoOrder auto-order code
   */	
  public void setAutoOrder(String autoOrder) {
	this.autoOrder = autoOrder;
  }
	
  /**
   * Retrieves the auto-order attribute.
   * @return String auto-order code
   */
  public String getAutoOrder(){
	return (this.autoOrder);
  }  

  /**
   * Sets the FLD comm attribute.
   * @param fldComm FLD comm
   */	
  public void setFldComm(String fldComm) {
	this.fldComm = fldComm;
  }
	
  /**
   * Retrieves the FLD comm attribute.
   * @return String FLD comm
   */
  public String getFldComm(){
	return (this.fldComm);
  }  

  /**
   * Sets the comment attribute.
   * @param comment remarks
   */	
  public void setComment(String comment) {
	this.comment = comment;
  }
	
  /**
   * Retrieves the comment attribute.
   * @return String remarks
   */
  public String getComment(){
	return (this.comment);
  }

  /**
   * Sets the client number attribute.
   * @param clientNo client id depending on client type
   */	
  public void setClientNo(String clientNo) {
	this.clientNo = clientNo;
  }
	
  /**
   * Retrieves the client number attribute.
   * @return String client id depending on client type
   */
  public String getClientNo(){
	return (this.clientNo);
  }
  
  /**
   * Sets the requirement id attribute.
   * @param reqId requirement id
   */	
  public void setReqId(String reqId) {
	this.reqId = reqId;
  }
	
  /**
   * Retrieves the requirement id attribute.
   * @return String requirement id requiement id
   */
  public String getReqId(){
	return (this.reqId);
  }
	
	/**
	 * @return
	 */
	public String getReqStatusValue() {
		return reqStatusValue;
	}
	
	/**
	 * @param string
	 */
	public void setReqStatusValue(String string) {
		reqStatusValue = string;
	}
	
	/**
	 * @return
	 */
	public String getReqCodeValue() {
		return reqCodeValue;
	}
	
	/**
	 * @param string
	 */
	public void setReqCodeValue(String string) {
		reqCodeValue = string;
	}
	
	/**
	 * @return
	 */
	public String getReqStatusDesc() {
		return reqStatusDesc;
	}
	
	/**
	 * @param string
	 */
	public void setReqStatusDesc(String string) {
		reqStatusDesc = string;
	}
	
	/**
	 * TODO method description getValidity
	 * @return
	 */
	public String getValidity() {
		return validity;
	}
	
	/**
	 * TODO method description setValidity
	 * @param string
	 */
	public void setValidity(String string) {
		validity = string;
	}
	
	/**
	 * TODO method description getFormId
	 * @return
	 */
	public String getFormId() {
		return formId;
	}
	
	/**
	 * TODO method description setFormId
	 * @param string
	 */
	public void setFormId(String string) {
		formId = string;
	}
	
	/**
	 * @return
	 */
	public String getPageNumber() {
		return pageNumber;
	}
	
	/**
	 * @param string
	 */
	public void setPageNumber(String string) {
		pageNumber = string;
	}
	
	/**
	 * @return
	 */
	public String getTestDateIndicator() {
		return testDateIndicator;
	}
	
	/**
	 * @param string
	 */
	public void setTestDateIndicator(String string) {
		testDateIndicator = string;
	}
	
	public String getImageReferenceNum() {
		return imageReferenceNum;
	}
	
	public void setImageReferenceNum(String imageReferenceNum) {
		this.imageReferenceNum = imageReferenceNum;
	}
	
	public String getWmsImageLink() {
		return wmsImageLink;
	}
	
	public void setWmsImageLink(String wmsImageLink) {
		this.wmsImageLink = wmsImageLink;
	}
	
	public String getOrderRequirementsTag() {
		return orderRequirementsTag;
	}
	
	public void setOrderRequirementsTag(String orderRequirementsTag) {
		this.orderRequirementsTag = orderRequirementsTag;
	}
	
	
	/**
	* @return
	*/
	public String getRefNo() {
		return refNo;
	}
	
	/**
	* @param strings
	*/
	public void setRefNo(String strings) {
		refNo = strings;
	}
	
	/**
	* @return
	*/
	public String getStatusTo() {
		return statusTo;
	}
	
	/**
	* @param string
	*/
	public void setStatusTo(String string) {
		statusTo = string;
	}
	
	/**
	* @return
	*/
	public String getFollowUpDateTo() {
		return followUpDateTo;
	}
	
	/**
	* @param string
	*/
	public void setFollowUpDateTo(String string) {
		followUpDateTo = string;
	}
	
	/**
	* @return
	*/
	public String getCommentTo() {
		return commentTo;
	}
	
	/**
	* @param string
	*/
	public void setCommentTo(String string) {
		commentTo = string;
	}
	
	public String getReqFollowUp() {
		return reqFollowUp;
	}
	
	public void setReqFollowUp(String reqFollowUp) {
		this.reqFollowUp = reqFollowUp;
	}
	
	/**
	* @return
	*/
	public String[] getReqIds() {
		return reqIds;
	}
	
	
	/**
	* @param strings
	*/
	public void setReqIds(String[] strings) {
		reqIds = strings;
	}
	
	/**
	* @return
	*/
	public String[] getIndexTemp() {
		return indexTemp;
	}
	
	/**
	* @param strings
	*/
	public void setIndexTemp(String[] strings) {
		indexTemp = strings;
	}
	
	/**
	* Returns the statusCode.
	* @return String[]
	*/
	public String[] getStatusCodes() {
		return statusCodes;
	}
	
	/**
	* Sets the statusCode.
	* @param statusCode The statusCode to set
	*/
	public void setStatusCodes(String[] statusCodes) {
		this.statusCodes = statusCodes;
	}
	
	
	/**
	* @return
	*/
	public String[] getFollowUpNos() {
		return followUpNos;
	}
	
	/**
	* @param strings
	*/
	public void setFollowUpNos(String[] strings) {
		followUpNos = strings;
	}
	
	/**
	* @return
	*/
	public String[] getFollowUpDates() {
		return followUpDates;
	}
	
	/**
	* @return
	*/
	public String[] getSeqs() {
		return seqs;
	}
	
	/**
	* @param strings
	*/
	public void setFollowUpDates(String[] strings) {
		followUpDates = strings;
	}
	
	/**
	* @param strings
	*/
	public void setSeqs(String[] strings) {
		seqs = strings;
	}
	
	/**
	* @return
	*/
	public String[] getReqLevels() {
		return reqLevels;
	}
	
	/**
	* @param strings
	*/
	public void setReqLevels(String[] strings) {
		reqLevels = strings;
	}
	
	/**
	* @return
	*/
	public String[] getReqCodes() {
		return reqCodes;
	}
	
	/**
	* @param strings
	*/
	public void setReqCodes(String[] strings) {
		reqCodes = strings;
	}
	
	/**
	* @return
	*/
	public String[] getDateSents() {
		return dateSents;
	}
	
	/**
	* @param strings
	*/
	public void setDateSents(String[] strings) {
		dateSents = strings;
	}
	
	public String[] getAutoOrderArr() {
		return autoOrderArr;
	}
	
	public void setAutoOrderArr(String[] autoOrderArr) {
		this.autoOrderArr = autoOrderArr;
	}
	
	public String[] getClientTypeArr() {
		return clientTypeArr;
	}
	
	public void setClientTypeArr(String[] clientTypeArr) {
		this.clientTypeArr = clientTypeArr;
	}
	
	public String[] getCommentArr() {
		return commentArr;
	}
	
	public void setCommentArr(String[] commentArr) {
		this.commentArr = commentArr;
	}
	
	public String[] getDesignationArr() {
		return designationArr;
	}
	
	public void setDesignationArr(String[] designationArr) {
		this.designationArr = designationArr;
	}
	
	public String[] getFldCommArr() {
		return fldCommArr;
	}
	
	public void setFldCommArr(String[] fldCommArr) {
		this.fldCommArr = fldCommArr;
	}
	
	public String[] getFollowUpNoArr() {
		return followUpNoArr;
	}
	
	public void setFollowUpNoArr(String[] followUpNoArr) {
		this.followUpNoArr = followUpNoArr;
	}
	
	public String[] getNewTestArr() {
		return newTestArr;
	}
	
	public void setNewTestArr(String[] newTestArr) {
		this.newTestArr = newTestArr;
	}
	
	public String[] getPaidIndArr() {
		return paidIndArr;
	}
	
	public void setPaidIndArr(String[] paidIndArr) {
		this.paidIndArr = paidIndArr;
	}
	
	public String[] getReqCodeArr() {
		return reqCodeArr;
	}
	
	public void setReqCodeArr(String[] reqCodeArr) {
		this.reqCodeArr = reqCodeArr;
	}
	
	public String[] getReqCodeValueArr() {
		return reqCodeValueArr;
	}
	
	public void setReqCodeValueArr(String[] reqCodeValueArr) {
		this.reqCodeValueArr = reqCodeValueArr;
	}
	
	public String[] getResolveArr() {
		return resolveArr;
	}
	
	public void setResolveArr(String[] resolveArr) {
		this.resolveArr = resolveArr;
	}
	
	public String[] getSeqArr() {
		return seqArr;
	}
	
	public void setSeqArr(String[] seqArr) {
		this.seqArr = seqArr;
	}
	
	public String[] getTestDateArr() {
		return testDateArr;
	}
	
	public void setTestDateArr(String[] testDateArr) {
		this.testDateArr = testDateArr;
	}
	
	public String[] getValidityDateArr() {
		return validityDateArr;
	}
	
	public void setValidityDateArr(String[] validityDateArr) {
		this.validityDateArr = validityDateArr;
	}
	
	public String[] getReqLevelArr() {
		return reqLevelArr;
	}
	
	public void setReqLevelArr(String[] reqLevelArr) {
		this.reqLevelArr = reqLevelArr;
	}
	
	public String[] getReqFollowUpArr() {
		return reqFollowUpArr;
	}
	
	public void setReqFollowUpArr(String[] reqFollowUpArr) {
		this.reqFollowUpArr = reqFollowUpArr;
	}
}

