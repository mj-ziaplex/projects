package com.slocpi.ium.ui.form;

// struts package
import org.apache.struts.action.ActionForm;

/**
 * @TODO Class Description UserRoleAccessForm
 * @author: Ma. Theresa Vicencio
 * @version: 1.0
 */
public class UserRoleAccessForm extends ActionForm {

  private String seqNo = "";
  private String name = "";
  private String userRole = "";
  private String workFunction = "";
    
    
  /**
    * sets the sequence number field of the user role form class
    * @param seqNo
    */  
  public void setSeqNo(String seqNo) {
    this.seqNo = seqNo;
  }

  /**
    * retrieves the sequence number field of the user role form class
    * @return String sequence number
    */
  public String getSeqNo() {
	return (this.seqNo);
  }

  /**
	* sets the name field of the user role form class
	* @param name
	*/  
  public void setName(String name) {
	this.name = name;
  }

  /**
	* retrieves the name field of the user role form class
	* @return String name
	*/
  public String getName() {
	return (this.name);
  }  

  /**
	* sets the user role field of the user role form class
	* @param uerRole
	*/  
  public void setUserRole(String userRole) {
	this.userRole = userRole;
  }

  /**
	* retrieves the user role field of the user role form class
	* @return String user role
	*/
  public String getUserRole() {
	return (this.userRole);
  }
  
  /**
	* sets the work function field of the user role form class
	* @param workFunction
	*/  
  public void setWorkFunction(String workFunction) {
	this.workFunction = workFunction;
  }

  /**
	* retrieves the work function field of the user role form class
	* @return String work function
	*/
  public String getWorkFunction() {
	return (this.workFunction);
  }  
    
}

