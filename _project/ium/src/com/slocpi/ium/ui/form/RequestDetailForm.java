package com.slocpi.ium.ui.form;

// java package
import java.util.ArrayList;

/**
 * This class contains the request details. It includes the list of documents and the activity log.
 * @author: Ma. Theresa Vicencio
 * @version: 1.0
 */
public class RequestDetailForm extends AssessmentRequestForm {

  private ArrayList documents = null;
  private ArrayList activities = null;
  
  /**
   * Sets the documents attribute.
   * @param documents list of documents
   */	
  public void setDocuments(ArrayList documents) {
	this.documents = documents;
  }
	
  /**
   * Retrieves the list of documents.
   * @return ArrayList list of documents 
   */
  public ArrayList getDocuments(){
    return (this.documents);
  }
  
  /**
   * Sets the activites attribute.
   * @param activities list of activities
   */	
  public void setActivities(ArrayList activities) {
	this.activities = activities;
  }
	
  /**
   * Retrieves the list of activities.
   * @return ArrayList list of activites 
   */
  public ArrayList getActivities(){
	return (this.activities);
  }  
	
}

