package com.slocpi.ium.ui.form;

// struts package


/**
 * This class contains the doctor's inputs and discussion details.
 * @author: Ma. Theresa Vicencio
 * @version: 1.0
 */
public class DiscussionForm extends AssessmentRequestForm {

  private String postedBy = "";
  private String datePosted = "";
  private String notes = "";
  private String doctor = "";
  private String recipient = "";
    
 
  /**
   * Sets the posted by attribute.
   * @param postedBy doctor who posted the note
   */  
  public void setPostedBy(String postedBy) {
	this.postedBy = postedBy;
  }

  /**
   * Retrieves the posted by attribute.
   * @return String doctor who posted the note
   */
  public String getPostedBy() {
	return (this.postedBy);
  }

  /**
   * Sets the date posted attribute.
   * @param datePosted date when the doctor posted the note
   */  
  public void setDatePosted(String datePosted) {
	this.datePosted = datePosted;
  }

  /**
   * Retrieves the  date posted attribute.
   * @return String date when the doctor posted the note
   */
  public String getDatePosted() {
	return (this.datePosted);
  }

  /**
   * Sets the notes attribute.
   * @param notes input and discussion
   */  
  public void setNotes(String notes) {
	this.notes = notes;
  }

  /**
   * Retrieves the notes attribute.
   * @return String input and discussion
   */
  public String getNotes() {
	return (this.notes);
  }

  /**
   * Sets the doctor attribute.
   * @param doctor doctor
   */  
  public void setDoctor(String doctor) {
	this.doctor = doctor;
  }

  /**
   * Retrieves the doctor attribute.
   * @return String doctor
   */
  public String getDoctor() {
	return (this.doctor);
  }
  
/**
 * @return
 */
public String getRecipient() {
	return recipient;
}

/**
 * @param string
 */
public void setRecipient(String string) {
	recipient = string;
}

}

