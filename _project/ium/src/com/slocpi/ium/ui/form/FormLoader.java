package com.slocpi.ium.ui.form;

import java.text.NumberFormat;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.slocpi.ium.IUMException;
import com.slocpi.ium.data.AssessmentRequestData;
import com.slocpi.ium.data.ClientData;
import com.slocpi.ium.data.ClientDataSheetData;
import com.slocpi.ium.data.LOBData;
import com.slocpi.ium.data.StatusData;
import com.slocpi.ium.data.SunLifeOfficeData;
import com.slocpi.ium.data.UWAssessmentData;
import com.slocpi.ium.data.UserProfileData;
import com.slocpi.ium.underwriter.AssessmentRequest;
import com.slocpi.ium.underwriter.UnderWriterException;
import com.slocpi.ium.util.DateHelper;

/*
 * author @nic.decapia
 *
 *
 *
 */
public class FormLoader {
	private static final Logger LOGGER = LoggerFactory.getLogger(FormLoader.class);
	// Launch IUM Enhancement - Andre Ceasar Dacanay - start
	private static HttpServletRequest request;
	
	public static HttpServletRequest getRequest() {
		return request;
	}

	public static void setRequest(HttpServletRequest request1) {
		request = request1;
	}

	
	public static AssessmentRequestForm loadAssessmentRequest(AssessmentRequestForm form, HttpServletRequest request) throws UnderWriterException, IUMException {
		
		LOGGER.info("loadAssessmentRequest start 1");
		LOGGER.info("form.getRefNo() "+form.getRefNo());
		setRequest(request);
		LOGGER.info("loadAssessmentRequest end 1");
		return loadAssessmentRequest(form);
	}

	
	public static AssessmentRequestForm loadAssessmentRequest(AssessmentRequestForm form) throws UnderWriterException, IUMException {
		
		LOGGER.info("loadAssessmentRequest start 2");
		AssessmentRequestData requestData = null;
		AssessmentRequest assessmentRequest = new AssessmentRequest();
		
		if(request.getSession().getAttribute("requestData_"+form.getRefNo())==null){
			requestData = assessmentRequest.getDetails(form.getRefNo());
			request.getSession().setAttribute("requestData_"+form.getRefNo(), requestData);
		}else{
			requestData = (AssessmentRequestData)request.getSession().getAttribute("requestData_"+form.getRefNo());
		}
		
		LOGGER.debug("requestData-->" +requestData.toString());
		LOBData lob = requestData.getLob();
		ClientData insured = requestData.getInsured();
		ClientData owner = requestData.getOwner();
		SunLifeOfficeData branch = requestData.getBranch();
		UserProfileData agent = requestData.getAgent();
		UserProfileData assignedTo = requestData.getAssignedTo();
		UserProfileData underwriter = requestData.getUnderwriter();
		UserProfileData location = requestData.getFolderLocation();
		ClientDataSheetData cds = requestData.getClientDataSheet();
		StatusData statusData = requestData.getStatus();
	
		if(owner == null
				|| (owner.getClientId() == null || "".equals(owner.getClientId()))) {
			owner = insured;
		}

		NumberFormat nf = NumberFormat.getInstance();
		nf.setMinimumFractionDigits(2);
		nf.setMaximumFractionDigits(2);
		
		form.setAutoSettleIndicator(noNull(requestData.getAutoSettleIndicator()));
		form.setTransmitIndicator(noNull(requestData.getTransmitIndicator()));
		form.setRefNo(noNull(form.getRefNo()));
		form.setPolicySuffix(noNull(requestData.getPolicySuffix()));
		form.setLob(noNull(lob.getLOBCode()));
		form.setLobDesc(noNull(lob.getLOBDesc()));
		form.setSourceSystem(noNull(requestData.getSourceSystem()));
		form.setAmountCovered(nf.format(requestData.getAmountCovered()));
		form.setPremium(nf.format(requestData.getPremium()));
		form.setInsuredName(noNull(insured.getGivenName()) + " " + noNull(insured.getLastName()));
		form.setBranchCode(noNull(branch.getOfficeId()));
		form.setBranchName(noNull(branch.getOfficeName()));
		form.setAgentCode(noNull(agent.getUserId()));
		form.setAgentName(noNull(agent.getFirstName()) + " " + noNull(agent.getLastName()));
		form.setRequestStatus(String.valueOf(statusData.getStatusId()));
		form.setAssignedTo(noNull(assignedTo.getUserId()));
		form.setUnderwriter(noNull(underwriter.getUserId()));
		form.setUnderwriterName(noNull(underwriter.getFirstName()) + " " + noNull(underwriter.getLastName()));
		form.setLocation(noNull(location.getUserId()));

		form.setRemarks(noNull(requestData.getRemarks()));
		form.setReceivedDate((DateHelper.format(requestData.getApplicationReceivedDate(), "ddMMMyyyy")).toUpperCase());
		form.setDateForwarded((DateHelper.format(requestData.getForwardedDate(), "ddMMMyyyy")).toUpperCase());
		form.setDateEdited((DateHelper.format(requestData.getUpdatedDate(), "ddMMMyyyy")).toUpperCase());
		form.setDateCreated((DateHelper.format(requestData.getCreatedDate(), "ddMMMyyyy")).toUpperCase());
		form.setStatusDate((DateHelper.format(requestData.getStatusDate(), "ddMMMyyyy")).toUpperCase());

		form.setInsuredClientType(noNull(cds.getClientType()));
		form.setInsuredClientNo(noNull(insured.getClientId()));
		form.setInsuredLastName(noNull(insured.getLastName()));
		form.setInsuredFirstName(noNull(insured.getGivenName()));
		form.setInsuredMiddleName(noNull(insured.getMiddleName()));
		form.setInsuredTitle(noNull(insured.getTitle()));
		form.setInsuredSuffix(noNull(insured.getSuffix()));
		
		if (insured.getAge() == 0){
			
			form.setInsuredAge("");
		}else {
			
			form.setInsuredAge(String.valueOf(insured.getAge()));
		}

		form.setInsuredSex(noNull(insured.getSex()));
		form.setInsuredBirthDate((DateHelper.format(insured.getBirthDate(), "ddMMMyyyy")).toUpperCase());
		form.setOwnerClientType(noNull(cds.getClientType()));
		form.setOwnerClientNo(noNull(owner.getClientId()));
		form.setOwnerLastName(noNull(owner.getLastName()));
		form.setOwnerFirstName(noNull(owner.getGivenName()));
		form.setOwnerMiddleName(noNull(owner.getMiddleName()));
		form.setOwnerTitle(noNull(owner.getTitle()));
		form.setOwnerSuffix(noNull(owner.getSuffix()));

		if (owner.getAge() == 0){
			
			form.setOwnerAge(String.valueOf(owner.getAge()));
		}else {
			
			form.setOwnerAge(String.valueOf(owner.getAge()));
		}

		form.setOwnerSex(noNull(owner.getSex()));
		form.setOwnerBirthDate((DateHelper.format(owner.getBirthDate(), "ddMMMyyyy")).toUpperCase());

		
		UWAssessmentData uwData = assessmentRequest.getUWAssessmentDetails(form.getRefNo());
		
		form.setUWRemarks(noNull(uwData.getRemarks()));
		form.setUWFinalDecision((uwData.getFinalDecision() != null) ? uwData.getFinalDecision().trim() : "");

		if (requestData.getAutoSettleError() != null) {
			
			form.setAutoSettleRemarks(requestData.getAutoSettleError().getMessage());
		}

		LOGGER.info("loadAssessmentRequest end 2");
		return (form);
	}// populateRequestDetails
	
	public static String noNull(final String str){
		
		
		String toRet;
		if(str == null) {
			toRet = "";
		} else {
			toRet = str.trim();
		}
		
		return toRet;
	}
}

