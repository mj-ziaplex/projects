package com.slocpi.ium.ui.form;

// struts package
import org.apache.struts.action.ActionForm;


/**
 * 
 * @author: Ingrid Villanueva
 * @version: 1.0
 */
public class MIBImpairmentsForm extends ActionForm {

  private String     startDate = "";
  private String     endDate   = "";


/**
 * @return
 */
public String getEndDate() {
	return endDate;
}

/**
 * @return
 */
public String getStartDate() {
	return startDate;
}

/**
 * @param string
 */
public void setEndDate(String string) {
	endDate = string;
}

/**
 * @param string
 */
public void setStartDate(String string) {
	startDate = string;
}

}

