package com.slocpi.ium.ui.form;


import java.util.Collection;
import org.apache.struts.action.ActionForm;


/**
 * 
 * @author: Ingrid Villanueva
 * @version: 1.0
 */
public class MedicalRecordDetailForm extends ActionForm {


  private String     refNo       = "";
  private String     clientNo          = "";
  private String     lastName          = "";
  private String     firstName         = "";
  private String     examiner          = "";
  private String     labName           = "";
  private String     lab               = "";
  private String     dateConducted = "";
  private String     test              = "";    
  private String     status            = "";
  private String     dateRequested     = "";
  private String     sevenDayMemo      = "";
  private String     dateReceived      = "";
  private String     agent             = "";
  private String     branch            = "";
  private String     birthDate         = "";
  private String     confirmed         = "";
  private String     remarks           = "";
  private String     examPlace         = "";
  private String     dateValidity      = "";
  private String     appointmentDate   = "";
  private String     appointmentTime   = "";
  private String     applicationStatus = "";
  private String     reason = ""; 
  private String     dateGenerated     = "";
  private String     section           = "";
  //
  private String     sevenDayMemoDate           = "";
  private String     reqParty           = "";
  private String     department           = "";
  private String     chargeTo           = "";
  private String     amount           = "";
  private String     payAgent           = "";
  private String     requestForLOAInd           = "";
  private String     resultsReceivedInd           = "";
  private String     receivedDate           = "";
  private String     type           = "";
  private String     followUpNumber           = "";
  private String     followUpDate           = "";
  private String     statusId          = "";
  private String     testId            = "";
  private String     medicalRecordId   = "";
  private String     createdDate       = "";
      
 private Collection medicalRecordData = null;
 private Collection examPlaceList = null;
 private Collection examinerList = null;
 private Collection labList = null;
 private Collection testList = null;
 private Collection clientList = null;
 private String actionType ="";
 private String facilitator ="";
 private String labTestInd="";
 private String prevStatus="";
 private String referenceNum="";
 private String err="";
 private String lob="";
 
          
  /**
   * Sets the date conducted attribute.
   * @param dateConducted date conducted
   */	
  public void setDateConducted(String dateConducted) {
	this.dateConducted = dateConducted;
  }
	
  /**
   * Retrieves the date conducted attribute.
   * @return String date conducted
   */
  public String getDateConducted(){
	return (this.dateConducted);
  }

  /**
   * Sets the date requested attribute.
   * @param dateRequested date received
   */	
  public void setDateRequested(String dateRequested) {
	this.dateRequested = dateRequested;
  }
	
  /**
   * Retrieves the date requested attribute.
   * @return String date requested
   */
  public String getDateRequested(){
	return (this.dateRequested);
  }

/**
 * Retrieves the client no attribute.
 * @return String client no
 */
public String getClientNo() {
	return clientNo;
}

/**
 * Retrieves the examiner attribute.
 * @return String examiner
 */
public String getExaminer() {
	return examiner;
}

/**
 * Retrieves the lab name attribute.
 * @return String lab name
 */
public String getLabName() {
	return labName;
}

/**
 * Retrieves the first name attribute.
 * @return String first name
 */
public String getFirstName() {
	return firstName;
}

/**
 * Retrieves the lab attribute.
 * @return String lab
 */
public String getLab() {
	return lab;
}

/**
 * Retrieves the last name attribute.
 * @return String last name
 */
public String getLastName() {
	return lastName;
}


/**
 * Retrieves the status attribute.
 * @return String status
 */
public String getStatus() {
	return status;
}

/**
 * Retrieves the test attribute.
 * @return String test
 */
public String getTest() {
	return test;
}

/**
 * Sets the client no attribute. 
 * @param String clientNo
 */
public void setClientNo(String clientNo) {
	this.clientNo = clientNo;
}

/**
 * Sets the examiner attribute.
 * @param String examiner
 */
public void setExaminer(String examiner) {
	this.examiner = examiner;
}

/**
 * Sets the lab name attribute.
 * @param String labName
 */
public void setLabName(String labName) {
	this.labName = labName;
}

/**
 * Sets the first name attribute.
 * @param String firstName
 */
public void setFirstName(String firstName) {
	this.firstName = firstName;
}

/**
 * Sets the lab attribute.
 * @param String lab
 */
public void setLab(String lab) {
	this.lab = lab;
}

/**
 * Sets the last name attribute.
 * @param String lastName
 */
public void setLastName(String lastName) {
	this.lastName = lastName;
}

/**
 * Sets the status attribute.
 * @param String status
 */
public void setStatus(String status) {
	this.status = status;
}

/**
 * Sets the test attribute.
 * @param String test
 */
public void setTest(String test) {
	this.test = test;
}

/**
 * Retrievest the 7 day memo date attribute.
 * @return String 7 day memo date
 */
public String getSevenDayMemo() {
	return sevenDayMemo;
}

/**
 * Sets the 7day memo date attribute
 * @param String sevenDayMemoDate
 */
public void setSevenDayMemo(String sevenDayMemo) {
	this.sevenDayMemo = sevenDayMemo;
}

/**
 * Returns the agent.
 * @return String
 */
public String getAgent() {
	return agent;
}

/**
 * Returns the amount.
 * @return String
 */
public String getAmount() {
	return amount;
}

/**
 * Returns the applicationStatus.
 * @return String
 */
public String getApplicationStatus() {
	return applicationStatus;
}

/**
 * Returns the appointmentDate.
 * @return String
 */
public String getAppointmentDate() {
	return appointmentDate;
}

/**
 * Returns the appointmentTime.
 * @return String
 */
public String getAppointmentTime() {
	return appointmentTime;
}

/**
 * Returns the birthDate.
 * @return String
 */
public String getBirthDate() {
	return birthDate;
}

/**
 * Returns the branch.
 * @return String
 */
public String getBranch() {
	return branch;
}

/**
 * Returns the chargeTo.
 * @return String
 */
public String getChargeTo() {
	return chargeTo;
}

/**
 * Returns the confirmed.
 * @return String
 */
public String getConfirmed() {
	return confirmed;
}

/**
 * Returns the dateGenerated.
 * @return String
 */
public String getDateGenerated() {
	return dateGenerated;
}

/**
 * Returns the dateReceived.
 * @return String
 */
public String getDateReceived() {
	return dateReceived;
}

/**
 * Returns the dateValidity.
 * @return String
 */
public String getDateValidity() {
	return dateValidity;
}

/**
 * Returns the department.
 * @return String
 */
public String getDepartment() {
	return department;
}

/**
 * Returns the examPlace.
 * @return String
 */
public String getExamPlace() {
	return examPlace;
}

/**
 * Returns the payAgent.
 * @return String
 */
public String getPayAgent() {
	return payAgent;
}

/**
 * Returns the reason.
 * @return String
 */
public String getReason() {
	return reason;
}

/**
 * Returns the refNo.
 * @return String
 */
public String getRefNo() {
	return refNo;
}

/**
 * Returns the remarks.
 * @return String
 */
public String getRemarks() {
	return remarks;
}

/**
 * Returns the reqParty.
 * @return String
 */
public String getReqParty() {
	return reqParty;
}

/**
 * Returns the section.
 * @return String
 */
public String getSection() {
	return section;
}

/**
 * Returns the sevenDayMemoDate.
 * @return String
 */
public String getSevenDayMemoDate() {
	return sevenDayMemoDate;
}

/**
 * Returns the type.
 * @return String
 */
public String getType() {
	return type;
}

/**
 * Sets the agent.
 * @param agent The agent to set
 */
public void setAgent(String agent) {
	this.agent = agent;
}

/**
 * Sets the amount.
 * @param amount The amount to set
 */
public void setAmount(String amount) {
	this.amount = amount;
}

/**
 * Sets the applicationStatus.
 * @param applicationStatus The applicationStatus to set
 */
public void setApplicationStatus(String applicationStatus) {
	this.applicationStatus = applicationStatus;
}

/**
 * Sets the appointmentDate.
 * @param appointmentDate The appointmentDate to set
 */
public void setAppointmentDate(String appointmentDate) {
	this.appointmentDate = appointmentDate;
}

/**
 * Sets the appointmentTime.
 * @param appointmentTime The appointmentTime to set
 */
public void setAppointmentTime(String appointmentTime) {
	this.appointmentTime = appointmentTime;
}

/**
 * Sets the birthDate.
 * @param birthDate The birthDate to set
 */
public void setBirthDate(String birthDate) {
	this.birthDate = birthDate;
}

/**
 * Sets the branch.
 * @param branch The branch to set
 */
public void setBranch(String branch) {
	this.branch = branch;
}

/**
 * Sets the chargeTo.
 * @param chargeTo The chargeTo to set
 */
public void setChargeTo(String chargeTo) {
	this.chargeTo = chargeTo;
}

/**
 * Sets the confirmed.
 * @param confirmed The confirmed to set
 */
public void setConfirmed(String confirmed) {
	this.confirmed = confirmed;
}

/**
 * Sets the dateGenerated.
 * @param dateGenerated The dateGenerated to set
 */
public void setDateGenerated(String dateGenerated) {
	this.dateGenerated = dateGenerated;
}

/**
 * Sets the dateReceived.
 * @param dateReceived The dateReceived to set
 */
public void setDateReceived(String dateReceived) {
	this.dateReceived = dateReceived;
}

/**
 * Sets the dateValidity.
 * @param dateValidity The dateValidity to set
 */
public void setDateValidity(String dateValidity) {
	this.dateValidity = dateValidity;
}

/**
 * Sets the department.
 * @param department The department to set
 */
public void setDepartment(String department) {
	this.department = department;
}

/**
 * Sets the examPlace.
 * @param examPlace The examPlace to set
 */
public void setExamPlace(String examPlace) {
	this.examPlace = examPlace;
}

/**
 * Sets the payAgent.
 * @param payAgent The payAgent to set
 */
public void setPayAgent(String payAgent) {
	this.payAgent = payAgent;
}

/**
 * Sets the reason.
 * @param reason The reason to set
 */
public void setReason(String reason) {
	this.reason = reason;
}

/**
 * Sets the refNo.
 * @param refNo The refNo to set
 */
public void setRefNo(String refNo) {
	this.refNo = refNo;
}

/**
 * Sets the remarks.
 * @param remarks The remarks to set
 */
public void setRemarks(String remarks) {
	this.remarks = remarks;
}

/**
 * Sets the reqParty.
 * @param reqParty The reqParty to set
 */
public void setReqParty(String reqParty) {
	this.reqParty = reqParty;
}

/**
 * Sets the section.
 * @param section The section to set
 */
public void setSection(String section) {
	this.section = section;
}

/**
 * Sets the sevenDayMemoDate.
 * @param sevenDayMemoDate The sevenDayMemoDate to set
 */
public void setSevenDayMemoDate(String _sevenDayMemoDate) {
	sevenDayMemoDate = _sevenDayMemoDate;
}

/**
 * Sets the type.
 * @param type The type to set
 */
public void setType(String type) {
	this.type = type;
}

/**
 * Returns the medicalRecordData.
 * @return Collection
 */
public Collection getMedicalRecordData() {
	return medicalRecordData;
}

/**
 * Sets the medicalRecordData.
 * @param medicalRecordData The medicalRecordData to set
 */
public void setMedicalRecordData(Collection medicalRecordData) {
	this.medicalRecordData = medicalRecordData;
}

/**
 * Returns the actionType.
 * @return String
 */
public String getActionType() {
	return actionType;
}

/**
 * Sets the actionType.
 * @param actionType The actionType to set
 */
public void setActionType(String actionType) {
	this.actionType = actionType;
}

/**
 * Returns the facilitator.
 * @return String
 */
public String getFacilitator() {
	return facilitator;
}

/**
 * Sets the facilitator.
 * @param facilitator The facilitator to set
 */
public void setFacilitator(String facilitator) {
	this.facilitator = facilitator;
}

/**
 * Returns the requestForLOAInd.
 * @return String
 */
public String getRequestForLOAInd() {
	return requestForLOAInd;
}

/**
 * Returns the resultsReceivedInd.
 * @return String
 */
public String getResultsReceivedInd() {
	return resultsReceivedInd;
}

/**
 * Sets the requestForLOAInd.
 * @param requestForLOAInd The requestForLOAInd to set
 */
public void setRequestForLOAInd(String requestForLOAInd) {
	this.requestForLOAInd = requestForLOAInd;
}

/**
 * Sets the resultsReceivedInd.
 * @param resultsReceivedInd The resultsReceivedInd to set
 */
public void setResultsReceivedInd(String resultsReceivedInd) {
	this.resultsReceivedInd = resultsReceivedInd;
}

/**
 * Returns the receivedDate.
 * @return String
 */
public String getReceivedDate() {
	return receivedDate;
}

/**
 * Sets the receivedDate.
 * @param receivedDate The receivedDate to set
 */
public void setReceivedDate(String receivedDate) {
	this.receivedDate = receivedDate;
}

/**
 * Returns the followUpDate.
 * @return String
 */
public String getFollowUpDate() {
	return followUpDate;
}

/**
 * Returns the followUpNumber.
 * @return String
 */
public String getFollowUpNumber() {
	return followUpNumber;
}

/**
 * Sets the followUpDate.
 * @param followUpDate The followUpDate to set
 */
public void setFollowUpDate(String followUpDate) {
	this.followUpDate = followUpDate;
}

/**
 * Sets the followUpNumber.
 * @param followUpNumber The followUpNumber to set
 */
public void setFollowUpNumber(String followUpNumber) {
	this.followUpNumber = followUpNumber;
}

/**
 * Returns the labTestInd.
 * @return String
 */
public String getLabTestInd() {
	return labTestInd;
}

/**
 * Sets the labTestInd.
 * @param labTestInd The labTestInd to set
 */
public void setLabTestInd(String labTestInd) {
	this.labTestInd = labTestInd;
}

/**
 * Returns the examPlaceList.
 * @return Collection
 */
public Collection getExamPlaceList() {
	return examPlaceList;
}

/**
 * Sets the examPlaceList.
 * @param examPlaceList The examPlaceList to set
 */
public void setExamPlaceList(Collection examPlaceList) {
	this.examPlaceList = examPlaceList;
}

/**
 * Returns the examinerList.
 * @return Collection
 */
public Collection getExaminerList() {
	return examinerList;
}

/**
 * Returns the labList.
 * @return Collection
 */
public Collection getLabList() {
	return labList;
}

/**
 * Returns the testList.
 * @return Collection
 */
public Collection getTestList() {
	return testList;
}

/**
 * Sets the examinerList.
 * @param examinerList The examinerList to set
 */
public void setExaminerList(Collection examinerList) {
	this.examinerList = examinerList;
}

/**
 * Sets the labList.
 * @param labList The labList to set
 */
public void setLabList(Collection labList) {
	this.labList = labList;
}

/**
 * Sets the testList.
 * @param testList The testList to set
 */
public void setTestList(Collection testList) {
	this.testList = testList;
}

/**
 * Returns the clientList.
 * @return Collection
 */
public Collection getClientList() {
	return clientList;
}

/**
 * Sets the clientList.
 * @param clientList The clientList to set
 */
public void setClientList(Collection clientList) {
	this.clientList = clientList;
}

/**
 * @return
 */
public String getStatusId() {
	return statusId;
}

/**
 * @return
 */
public String getTestId() {
	return testId;
}

/**
 * @param string
 */
public void setStatusId(String string) {
	statusId = string;
}

/**
 * @param string
 */
public void setTestId(String string) {
	testId = string;
}

/**
 * @return
 */
public String getMedicalRecordId() {
	return medicalRecordId;
}

/**
 * @param string
 */
public void setMedicalRecordId(String string) {
	medicalRecordId = string;
}

/**
 * Returns the prevStatus.
 * @return String
 */
public String getPrevStatus() {
	return prevStatus;
}

/**
 * Sets the prevStatus.
 * @param prevStatus The prevStatus to set
 */
public void setPrevStatus(String prevStatus) {
	this.prevStatus = prevStatus;
}


/**
 * Returns the referenceNum.
 * @return String
 */
public String getReferenceNum() {
	return referenceNum;
}

/**
 * Sets the referenceNum.
 * @param referenceNum The referenceNum to set
 */
public void setReferenceNum(String referenceNum) {
	this.referenceNum = referenceNum;
}




/**
 * @return
 */
public String getCreatedDate() {
	return createdDate;
}

/**
 * @param string
 */
public void setCreatedDate(String string) {
	createdDate = string;
}

/**
 * Returns the err.
 * @return String
 */
public String getErr() {
	return err;
}

/**
 * Sets the err.
 * @param err The err to set
 */
public void setErr(String err) {
	this.err = err;
}

/**
 * Returns the lob.
 * @return String
 */
public String getLob() {
	return lob;
}

/**
 * Sets the lob.
 * @param lob The lob to set
 */
public void setLob(String lob) {
	this.lob = lob;
}

}

