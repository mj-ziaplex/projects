package com.slocpi.ium.ui.form;

// struts package
import org.apache.struts.action.ActionForm;

/**
 * @TODO Class Description WorkForm
 * @author: Ma. Theresa Vicencio
 * @version: 1.0
 */
public class WorkForm extends ActionForm {

  private String actionCode = "";
  private String description = "";
  private String transactionType = "";
    
    
  /**
    * Sets the action code attribute.
    * @param actionCode
    */  
  public void setActionCode(String actionCode) {
    this.actionCode = actionCode;
  }

  /**
    * Retrieves the action code attribute.
    * @return String action code
    */
  public String getActionCode() {
	return (this.actionCode);
  }

  /**
	* Sets the description attribute.
	* @param description
	*/  
  public void setDescription(String description) {
	this.description = description;
  }

  /**
	* Retrieves the description attribute.
	* @return String descirption
	*/
  public String getDescription() {
	return (this.description);
  }  

  /**
	* Sets the transaction type attribute.
	* @param transactionType
	*/  
  public void setTransactionType(String transactionType) {
	this.transactionType = transactionType;
  }

  /**
	* Retrieves the transaction type attribute.
	* @return String transaction type
	*/
  public String getTransactionType() {
	return (this.transactionType);
  }
    
}

