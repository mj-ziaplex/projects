package com.slocpi.ium.ui.form;

// java package
import java.util.ArrayList;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

/**
 * This class contains the kickout reasons, list of requirements, and medical and lab records. 
 * It also includes the requirement details.
 * @author: Ma. Theresa Vicencio
 * @version: 1.0
 */
public class KOReqMedLabDetailForm extends AssessmentRequestForm {

  private ArrayList koReasons     = null;
  private ArrayList medLabRecords = null;
  private ArrayList requirements  = null;
  private ArrayList reqLevelList  = null;
  private String[]  reqId         = null;
  private String[]  statusCode    = null;
  private String    refNo         = null;
  private String    statusTo      = null;
  private String[]  indexTemp     = null;
  private String[]  followUpNo    = null;
  private String[]  seq           = null;
  private String[]  followUpDate  = null;
  private String[]  reqLevel      = null;
  private String[]  reqCodes      = null;
  private String[]  dateSent      = null;
  
  
  public String toString(){
	  
	  return ToStringBuilder.reflectionToString(this, ToStringStyle.DEFAULT_STYLE);
  }
  
  
  /**
   * Sets the clear case kick-out reasons attribute.
   * @param koReasons clear case kick-out reasons
   */	
  public void setKoReasons(ArrayList koReasons) {
    this.koReasons = koReasons;
  }
	
  /**
   * Retrieves the list of clear case kick-out reasons.
   * @return ArrayList list of clear case kick-out reasons
   */
  public ArrayList getKoReasons(){
    return (this.koReasons);
  }
  
  /**
   * Sets the medical and lab records attribute.
   * @param medLabRecords medical and lab records
   */	
  public void setMedLabRecords(ArrayList medLabRecords) {
	this.medLabRecords = medLabRecords;
  }
	
  /**
   * Retrieves the list of medical and lab records.
   * @return ArrayList medical and lab records
   */
  public ArrayList getMedLabRecords(){
	return (this.medLabRecords);
  }

  /**
   * Sets the requirements attribute.
   * @param requirements list of requirements
   */	
  public void setRequirements(ArrayList requirements) {
	this.requirements = requirements;
  }
	
  /**
   * Retrieves the list of requirements.
   * @return ArrayList list of requirements
   */
  public ArrayList getRequirements(){
	return (this.requirements);
  } 



/**
 * @return
 */
public String getStatusTo() {
	return statusTo;
}

/**
 * @param string
 */
public void setStatusTo(String string) {
	statusTo = string;
}

/**
 * @return
 */
public String getRefNo() {
	return refNo;
}

/**
 * @return
 */
public String[] getReqId() {
	return reqId;
}

/**
 * @param strings
 */
public void setRefNo(String strings) {
	refNo = strings;
}

/**
 * @param strings
 */
public void setReqId(String[] strings) {
	reqId = strings;
}

/**
 * @return
 */
public String[] getIndexTemp() {
	return indexTemp;
}

/**
 * @param strings
 */
public void setIndexTemp(String[] strings) {
	indexTemp = strings;
}

/**
 * Returns the statusCode.
 * @return String[]
 */
public String[] getStatusCode() {
	return statusCode;
}

/**
 * Sets the statusCode.
 * @param statusCode The statusCode to set
 */
public void setStatusCode(String[] statusCode) {
	this.statusCode = statusCode;
}

/**
 * @return
 */
public String[] getFollowUpNo() {
	return followUpNo;
}

/**
 * @param strings
 */
public void setFollowUpNo(String[] strings) {
	followUpNo = strings;
}

/**
 * @return
 */
public String[] getFollowUpDate() {
	return followUpDate;
}

/**
 * @return
 */
public String[] getSeq() {
	return seq;
}

/**
 * @param strings
 */
public void setFollowUpDate(String[] strings) {
	followUpDate = strings;
}

/**
 * @param strings
 */
public void setSeq(String[] strings) {
	seq = strings;
}

/**
 * @return
 */
public ArrayList getReqLevelList() {
	return reqLevelList;
}

/**
 * @param list
 */
public void setReqLevelList(ArrayList list) {
	reqLevelList = list;
}

/**
 * @return
 */
public String[] getReqLevel() {
	return reqLevel;
}

/**
 * @param strings
 */
public void setReqLevel(String[] strings) {
	reqLevel = strings;
}

/**
 * @return
 */
public String[] getReqCodes() {
	return reqCodes;
}

/**
 * @param strings
 */
public void setReqCodes(String[] strings) {
	reqCodes = strings;
}

/**
 * @return
 */
public String[] getDateSent() {
	return dateSent;
}

/**
 * @param strings
 */
public void setDateSent(String[] strings) {
	dateSent = strings;
}

}

