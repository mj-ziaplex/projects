/*
 * Created on Mar 4, 2004
 * package name = com.slocpi.ium.ui.form
 * file name    = RequirementFormList.java
 */
package com.slocpi.ium.ui.form;

import java.util.ArrayList;
import org.apache.struts.action.ActionForm;

/**
 * TODO Class Description of RequirementFormList.java
 * @author Engel
 * 
 */
public class RequirementFormList extends ActionForm {
	private String reqCode="";
	private String reqDesc="";
	private String reqLevel="";
	private String validity="";
	private String followUpNo="";
	private String formId="";
		
	private ArrayList reqlist;
	/**
	 * TODO method description getReqlist
	 * @return
	 */
	public ArrayList getReqlist() {
		return reqlist;
	}


	/**
	 * TODO method description setReqlist
	 * @param list
	 */
	public void setReqlist(ArrayList list) {
		reqlist = list;
	}

	/**
	 * TODO method description getFollowUpNo
	 * @return
	 */
	public String getFollowUpNo() {
		return followUpNo;
	}

	/**
	 * TODO method description getFormId
	 * @return
	 */
	public String getFormId() {
		return formId;
	}

	/**
	 * TODO method description getReqCode
	 * @return
	 */
	public String getReqCode() {
		return reqCode;
	}

	/**
	 * TODO method description getReqDesc
	 * @return
	 */
	public String getReqDesc() {
		return reqDesc;
	}

	/**
	 * TODO method description getReqLevel
	 * @return
	 */
	public String getReqLevel() {
		return reqLevel;
	}

	/**
	 * TODO method description getValidity
	 * @return
	 */
	public String getValidity() {
		return validity;
	}

	/**
	 * TODO method description setFollowUpNo
	 * @param string
	 */
	public void setFollowUpNo(String string) {
		followUpNo = string;
	}

	/**
	 * TODO method description setFormId
	 * @param string
	 */
	public void setFormId(String string) {
		formId = string;
	}

	/**
	 * TODO method description setReqCode
	 * @param string
	 */
	public void setReqCode(String string) {
		reqCode = string;
	}

	/**
	 * TODO method description setReqDesc
	 * @param string
	 */
	public void setReqDesc(String string) {
		reqDesc = string;
	}

	/**
	 * TODO method description setReqLevel
	 * @param string
	 */
	public void setReqLevel(String string) {
		reqLevel = string;
	}

	/**
	 * TODO method description setValidity
	 * @param string
	 */
	public void setValidity(String string) {
		validity = string;
	}

}
