/*
 * Created on Jan 6, 2004
 * package name = com.slocpi.ium.ui.form
 * file name    = RequestFilterForm.java
 */
package com.slocpi.ium.ui.form;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.apache.struts.action.ActionForm;
import com.slocpi.ium.ui.util.Page;
import com.slocpi.ium.util.IUMConstants;

/**
 * TODO Class Description of RequestFilterForm.java
 * @author Engel
 * 
 */
public class RequestFilterForm extends ActionForm {
	
	private String [] checkedRefNum;
	private String refNo;
	private String agent;
	private String clientNumber;
	private String lob;
	private String branch;
	private String status;
	private String location;
	private String assignedTo;
	private String requirementCode;
	private String nameOfInsured;
	private String applicationReceivedDate;
	private String coverageAmount;
	private String changeStatusTo;
	private String changeAssignTo;
	private String reassignTo;
	private String sortOrder;
	private String clickedRefNo = "none";
	private int columns;
	private String startSearch = IUMConstants.NO;
	
	//kristian
	private int pageNo = 1;
	private Page page;
	//kristian
	
	public String toString(){
		
		return ToStringBuilder.reflectionToString(this, ToStringStyle.DEFAULT_STYLE);
	}
	
	
	/**
	 * TODO method description getAgent
	 * @return
	 */
	public String getAgent() {
		return agent;
	}

	/**
	 * TODO method description getApplicationReceivedDate
	 * @return
	 */
	public String getApplicationReceivedDate() {
		return applicationReceivedDate;
	}

	/**
	 * TODO method description getAssignedTo
	 * @return
	 */
	public String getAssignedTo() {
		return assignedTo;
	}

	/**
	 * TODO method description getClientNumber
	 * @return
	 */
	public String getClientNumber() {
		return clientNumber;
	}

	/**
	 * TODO method description getCoverageAmount
	 * @return
	 */
	public String getCoverageAmount() {
		return coverageAmount;
	}

	/**
	 * TODO method description getLob
	 * @return
	 */
	public String getLob() {
		return lob;
	}

	/**
	 * TODO method description getNameOfInsured
	 * @return
	 */
	public String getNameOfInsured() {
		return nameOfInsured;
	}

	/**
	 * TODO method description getReferenceNumber
	 * @return
	 */
	public String getRefNo() {
		return refNo;
	} 
	
	/**
	 * TODO method description getRequirementCode
	 * @return
	 */
	public String getRequirementCode() {
		return requirementCode;
	}

	/**
	 * TODO method description getStatus
	 * @return
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * TODO method description setAgent
	 * @param string
	 */
	public void setAgent(String string) {
		agent = string;
	}

	/**
	 * TODO method description setApplicationReceivedDate
	 * @param string
	 */
	public void setApplicationReceivedDate(String string) {
		applicationReceivedDate = string;
	}

	/**
	 * TODO method description setAssignedTo
	 * @param string
	 */
	public void setAssignedTo(String string) {
		assignedTo = string;
	}

	/**
	 * TODO method description setClientNumber
	 * @param string
	 */
	public void setClientNumber(String string) {
		clientNumber = string;
	}

	/**
	 * TODO method description setCoverageAmount
	 * @param string
	 */
	public void setCoverageAmount(String string) {
		coverageAmount = string;
	}

	/**
	 * TODO method description setLob
	 * @param string
	 */
	public void setLob(String string) {
		lob = string;
	}

	/**
	 * TODO method description setNameOfInsured
	 * @param string
	 */
	public void setNameOfInsured(String string) {
		nameOfInsured = string;
	}

	/**
	 * TODO method description setReferenceNumber
	 * @param string
	 */
	public void setRefNo(String string) {
		refNo = string;
	} 

	/**
	 * TODO method description setRequirementCode
	 * @param string
	 */
	public void setRequirementCode(String string) {
		requirementCode = string;
	}

	/**
	 * TODO method description setStatus
	 * @param string
	 */
	public void setStatus(String string) {
		status = string;
	}

	/**
	 * TODO method description getBranch
	 * @return
	 */
	public String getBranch() {
		return branch;
	}

	/**
	 * TODO method description setBranch
	 * @param string
	 */
	public void setBranch(String string) {
		branch = string;
	}

	/**
	 * TODO method description getLocation
	 * @return
	 */
	public String getLocation() {
		return location;
	}

	/**
	 * TODO method description setLocation
	 * @param string
	 */
	public void setLocation(String string) {
		location = string;
	}
	/**
	 * @return
	 */
	public String[] getCheckedRefNum() {
		return checkedRefNum;
	}

	/**
	 * @param strings
	 */
	public void setCheckedRefNum(String[] strings) {
		checkedRefNum = strings;
	}

	/**
	 * @return
	 */
	public String getChangeStatusTo() {
		return changeStatusTo;
	}

	/**
	 * @param string
	 */
	public void setChangeStatusTo(String string) {
		changeStatusTo = string;
	}

	/**
	 * @return
	 */
	public String getChangeAssignTo() {
		return changeAssignTo;
	}

	/**
	 * @param string
	 */
	public void setChangeAssignTo(String string) {
		changeAssignTo = string;
	}


	/**
	 * Returns the pageNo.
	 * @return int
	 */
	public int getPageNo() {
		return pageNo;
	}

	
	/**
	 * Sets the pageNo.
	 * @param pageNo The pageNo to set
	 */
	public void setPageNo(int pageNo) {
		this.pageNo = pageNo;
	}

	/**
	 * Returns the page.
	 * @return Page
	 */
	public Page getPage() {
		return page;
	}

	/**
	 * Sets the page.
	 * @param page The page to set
	 */
	public void setPage(Page page) {
		this.page = page;
	}

	/**
	 * TODO method description getReassignTo
	 * @return
	 */
	public String getReassignTo() {
		return reassignTo;
	}

	/**
	 * TODO method description setReassignTo
	 * @param string
	 */
	public void setReassignTo(String string) {
		reassignTo = string;
	}

	/**
	 * TODO method description getSortOrder
	 * @return
	 */
	public String getSortOrder() {
		return sortOrder;
	}

	/**
	 * TODO method description setSortOrder
	 * @param string
	 */
	public void setSortOrder(String string) {
		sortOrder = string;
	}

	/**
	 * TODO method description getColumns
	 * @return
	 */
	public int getColumns() {
		return columns;
	}

	/**
	 * TODO method description setColumns
	 * @param string
	 */
	public void setColumns(int column) {
		columns = column;
	}

	/**
	 * @return
	 */
	public String getClickedRefNo() {
		return clickedRefNo;
	}

	/**
	 * @param string
	 */
	public void setClickedRefNo(String string) {
		clickedRefNo = string;
	}

	public String getStartSearch() {
		return startSearch;
	}

	public void setStartSearch(String startSearch) {
		this.startSearch = startSearch;
	}

}
