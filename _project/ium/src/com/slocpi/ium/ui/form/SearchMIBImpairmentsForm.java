package com.slocpi.ium.ui.form;

// struts package
import org.apache.struts.action.ActionForm;

/**
 * 
 * @author: Andre Ceasar Dacanay
 * @version: 1.0
 */
public class SearchMIBImpairmentsForm extends ActionForm {

  /**
	 * 
	 */
private static final long serialVersionUID = 1L;


private String code = "";
private String desc   = "";
private String pageNumber = "";


/**
 * @return
 */
public String getCode() {
	return code;
}

/**
 * @return
 */
public String getDesc() {
	return desc;
}

/**
 * @return
 */
public String getPageNumber() {
	return pageNumber;
}


/**
 * @param string
 */
public void setCode(String string) {
	code = string;
}

/**
 * @param string
 */
public void setDesc(String string) {
	desc = string;
}

/**
 * @param string
 */
public void setPageNumber(String string) {
	pageNumber = string;
}

}

