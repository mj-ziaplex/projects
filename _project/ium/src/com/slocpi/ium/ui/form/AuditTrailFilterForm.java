/*
 * Created on Feb 24, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.ui.form;

import java.util.ArrayList;
import org.apache.struts.action.ActionForm;


/**
 * @author Cris
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class AuditTrailFilterForm extends ActionForm{
	
	private String table = "";
	private String startDate = "";
	private String endDate = "";
	private String transactionType = "";
	private ArrayList auditTrailLog = null;
	
	

	/**
	 * @return
	 */
	public String getEndDate() {
		return endDate;
	}

	/**
	 * @return
	 */
	public String getStartDate() {
		return startDate;
	}

	/**
	 * @return
	 */
	public String getTable() {
		return table;
	}

	/**
	 * @return
	 */
	public String getTransactionType() {
		return transactionType;
	}

	/**
	 * @param string
	 */
	public void setEndDate(String string) {
		endDate = string;
	}

	/**
	 * @param string
	 */
	public void setStartDate(String string) {
		startDate = string;
	}

	/**
	 * @param string
	 */
	public void setTable(String string) {
		table = string;
	}

	/**
	 * @param string
	 */
	public void setTransactionType(String string) {
		transactionType = string;
	}

	/**
	 * @return
	 */
	public ArrayList getAuditTrailLog() {
		return auditTrailLog;
	}

	/**
	 * @param list
	 */
	public void setAuditTrailLog(ArrayList list) {
		auditTrailLog = list;
	}

}
