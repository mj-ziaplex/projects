package com.slocpi.ium.ui.form;


/**
 * This class contains the document details.
 * @author: Ma. Theresa Vicencio
 * @version: 1.0
 */
public class DocumentForm extends AssessmentRequestForm {

  private String docCode = "";
  private String docDesc = "";
  private String dateSubmit = "";
  private String dateRequested = "";

  /**
   * Sets the document code attribute.
   * @param docCode document code
   */  
  public void setDocCode(String docCode) {
	this.docCode = docCode;
  }

  /**
   * Retrieves the document code attribute.
   * @return String document code
   */
  public String getDocCode() {
	return (this.docCode);
  }   
 
  /**
   * Sets the document description attribute.
   * @param docDesc document description
   */  
  public void setDocDesc(String docDesc) {
	this.docDesc = docDesc;
  }

  /**
   * Retrieves the document description attribute.
   * @return String document description
   */
  public String getDocDesc() {
	return (this.docDesc);
  }

  /**
   * Sets the date submitted attribute.
   * @param dateSubmit date submitted
   */  
  public void setDateSubmit(String dateSubmit) {
	this.dateSubmit = dateSubmit;
  }

  /**
   * Retrieves the date submitted attribute.
   * @return String date submitted
   */
  public String getDateSubmit() {
	return (this.dateSubmit);
  }

/**
 * @return
 */
public String getDateRequested() {
	return dateRequested;
}

/**
 * @param string
 */
public void setDateRequested(String string) {
	dateRequested = string;
}

}

