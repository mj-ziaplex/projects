package com.slocpi.ium.ui.form;

// struts package
import org.apache.struts.action.ActionForm;


/**
 * This class contains the activity log details.
 * @author: Ma. Theresa Vicencio
 * @version: 1.0
 */
public class ActivityLogForm extends ActionForm {

  private String activityDate = "";
  private String activityName = "";
  private String userName = "";
  private String lapseTime = "";
  private String assignedTo = "";
  

  /**
   * Sets the activity date attribute.
   * @param activityDate date of activity  
   */  
  public void setActivityDate(String activityDate) {
	this.activityDate = activityDate;
  }

  /**
   * Retrieves the activity date attribute.
   * @return String date of activity
   */
  public String getActivityDate() {
	return (this.activityDate);
  }
  
  /**
   * Sets the activity name attribute.
   * @param activityName activity name  
   */  
  public void setActivityName(String activityName) {
	this.activityName = activityName;
  }

  /**
   * Retrieves the activity name attribute.
   * @return String activity name
   */
  public String getActivityName() {
	return (this.activityName);
  }

  /**
   * Sets the user name attribute.
   * @param userName user name  
   */  
  public void setUserName(String userName) {
	this.userName = userName;
  }

  /**
   * Retrieves the user name  attribute.
   * @return String user name
   */
  public String getUserName() {
	return (this.userName);
  }
  
/**
 * @return
 */
public String getAssignedTo() {
	return assignedTo;
}

/**
 * @return
 */
public String getLapseTime() {
	return lapseTime;
}

/**
 * @param string
 */
public void setAssignedTo(String string) {
	assignedTo = string;
}

/**
 * @param string
 */
public void setLapseTime(String string) {
	lapseTime = string;
}

}

