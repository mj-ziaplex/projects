package com.slocpi.ium.ui.form;

import org.apache.struts.action.ActionForm;

/**
 * @author jcristobal
 *
 * To change this generated comment edit the template variable "typecomment":
 * Window>Preferences>Java>Templates.
 * To enable and disable the creation of type comments go to
 * Window>Preferences>Java>Code Generation.
 */
public class AccessTemplateForm extends ActionForm {
	
	private String roleDesc = null;
	private String templateId = null;
	private String[] pageNameId = null;
	private String[] pageName = null; 
	private String[] pageAccess = null;
	private String[] pageAccessResult = null;
	private String[] pageHeader = null;
		
	private int rowSize;
	private int accessCount;
	

	
	

	/**
	 * Retrieves the page access attribute. 
	 * @return
	 */
	public String[] getPageAccess() {
		return pageAccess;
	}

	/**
	 * Retrieves the page access result attribute. 
	 * @return
	 */
	public String[] getPageAccessResult() {
		return pageAccessResult;
	}

	/**
	 * Retrieves the page name ID attribute.
	 * @return
	 */
	public String[] getPageNameId() {
		return pageNameId;
	}

	/**
	 * Retrieves the page name attribute. 
	 * @return
	 */
	public String[] getPageName() {
		return pageName;
	}

	/**
	 * Retrieves the role description attribute. 
	 * @return
	 */
	public String getRoleDesc() {
		return roleDesc;
	}

	/**
	 * Retrieves the row size attribute. 
	 * @return
	 */
	public int getRowSize() {
		return rowSize;
	}

	/**
	 * Retrieves the template ID attribute. 
	 * @return
	 */
	public String getTemplateId() {
		return templateId;
	}

	/**
	 * Sets the page access attribute. 
	 * @param strings
	 */
	public void setPageAccess(String[] strings) {
		pageAccess = strings;
	}

	/**
	 * Sets the page access result attribute. 
	 * @param strings
	 */
	public void setPageAccessResult(String[] strings) {
		pageAccessResult = strings;
	}

	/**
	 * Sets the page name id attribute. 
	 * @param strings
	 */
	public void setPageNameId(String[] strings) {
		pageNameId = strings;
	}

	/**
	 * Sets the page name attribute. 
	 * @param strings
	 */
	public void setPageName(String[] strings) {
		pageName = strings;
	}

	/**
	 * Sets the role description attribute. 
	 * @param string
	 */
	public void setRoleDesc(String string) {
		roleDesc = string;
	}

	/**
	 * Sets the row size attribute. 
	 * @param i
	 */
	public void setRowSize(int i) {
		rowSize = i;
	}

	/**
	 * Sets the template ID attribute. 
	 * @param string
	 */
	public void setTemplateId(String string) {
		templateId = string;
	}

	/**
	 * Retrieves the access count attribute. 
	 * @return
	 */
	public int getAccessCount() {
		return accessCount;
	}

	/**
	 * Sets the access count attribute.
	 * @param i
	 */
	public void setAccessCount(int i) {
		accessCount = i;
	}

	/**
	 * Retrieves the page header attribute. 
	 * @return
	 */
	public String[] getPageHeader() {
		return pageHeader;
	}

	/**
	 * Sets the page header attribute.
	 * @param strings
	 */
	public void setPageHeader(String[] strings) {
		pageHeader = strings;
	}

}
