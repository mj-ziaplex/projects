/*
 * Created on Mar 8, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.ui.form;

import java.util.ArrayList;
import org.apache.struts.action.ActionForm;
import com.slocpi.ium.ui.util.Page;

/**
 * @author Nic Decapia
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class ReportTurnaroundTimeForm extends ActionForm
{
	private String startDate = "";
	private String endDate = "";
	private String startStatus = "";
	private String endStatus ="";
	private String lineOfBusiness ="";
	private String reportType = "";	// unit actually, whether days or hours
	
	private ArrayList content = null;

	private int pageNo = 1;
	private Page page;		
	/**
	 * @return
	 */
	public ArrayList getContent() {
		return content;
	}

	/**
	 * @return
	 */
	public String getEndDate() {
		return endDate;
	}

	/**
	 * @return
	 */
	public String getEndStatus() {
		return endStatus;
	}

	/**
	 * @return
	 */
	public String getLineOfBusiness() {
		return lineOfBusiness;
	}

	/**
	 * @return
	 */
	public Page getPage() {
		return page;
	}

	/**
	 * @return
	 */
	public int getPageNo() {
		return pageNo;
	}

	/**
	 * @return
	 */
	public String getStartDate() {
		return startDate;
	}

	/**
	 * @return
	 */
	public String getStartStatus() {
		return startStatus;
	}

	/**
	 * @param list
	 */
	public void setContent(ArrayList list) {
		content = list;
	}

	/**
	 * @param string
	 */
	public void setEndDate(String string) {
		endDate = string;
	}

	/**
	 * @param string
	 */
	public void setEndStatus(String string) {
		endStatus = string;
	}

	/**
	 * @param string
	 */
	public void setLineOfBusiness(String string) {
		lineOfBusiness = string;
	}

	/**
	 * @param page
	 */
	public void setPage(Page page) {
		this.page = page;
	}

	/**
	 * @param i
	 */
	public void setPageNo(int i) {
		pageNo = i;
	}

	/**
	 * @param string
	 */
	public void setStartDate(String string) {
		startDate = string;
	}

	/**
	 * @param string
	 */
	public void setStartStatus(String string) {
		startStatus = string;
	}

	/**
	 * @return
	 */
	public String getReportType() {
		return reportType;
	}

	/**
	 * @param string
	 */
	public void setReportType(String string) {
		reportType = string;
	}

}
