package com.slocpi.ium.ui.form;


import java.util.ArrayList;
import org.apache.struts.action.ActionForm;

/**
 * This class contains the list of assessment requests.
 * @author: Ma. Theresa Vicencio
 * @version: 1.0
 */
public class AssessmentRequestListForm extends ActionForm {
	
  private ArrayList assessmentRequestList = null;
  
  /**
   * Sets the assessment request list  attribute.
   * @param assessmentRequestList assessment request list
   */	
  public void setAssessmentRequestList(ArrayList assessmentRequestList) {
	this.assessmentRequestList = assessmentRequestList;
  }
	
  /**
   * Retrieves the assessment request list  attribute.
   * @return ArrayList assessment request list
   */
  public ArrayList getAssessmentRequestList(){
    return (this.assessmentRequestList);
  }
		
}

