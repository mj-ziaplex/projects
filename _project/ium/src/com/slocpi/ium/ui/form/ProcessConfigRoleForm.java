package com.slocpi.ium.ui.form;

import java.util.ArrayList;
import org.apache.struts.action.ActionForm;

/**
 * ProcessConfigRoleForm.java 
 * Description:
 *
 * @version $Revision: 1.1 $
 * @author $Author: pw12 $ $Date: 2012/08/01 07:07:32 $
 */
public class ProcessConfigRoleForm extends ActionForm {
	private ArrayList availableRoles = null;
	private ArrayList assignedRoles = null;
	
	
	/**
	 * Returns the assignedRoles.
	 * @return ArrayList
	 */
	public ArrayList getAssignedRoles() {
		return assignedRoles;
	}

	/**
	 * Returns the availableRoles.
	 * @return ArrayList
	 */
	public ArrayList getAvailableRoles() {
		return availableRoles;
	}

	/**
	 * Sets the assignedRoles.
	 * @param assignedRoles The assignedRoles to set
	 */
	public void setAssignedRoles(ArrayList assignedRoles) {
		this.assignedRoles = assignedRoles;
	}

	/**
	 * Sets the availableRoles.
	 * @param availableRoles The availableRoles to set
	 */
	public void setAvailableRoles(ArrayList availableRoles) {
		this.availableRoles = availableRoles;
	}

}
