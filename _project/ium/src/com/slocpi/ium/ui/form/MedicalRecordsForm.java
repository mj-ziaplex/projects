package com.slocpi.ium.ui.form;

// struts package
import java.util.Collection;
import org.apache.struts.action.ActionForm;
import com.slocpi.ium.ui.util.Page;


/**
 * 
 * @author: Ingrid Villanueva
 * @version: 1.0
 */
public class MedicalRecordsForm extends ActionForm {

  private String     dateConducted     = "";
  private String     conductedDate     = "";
  private String     referenceNo       = "";
  private String     clientNo          = "";
  private String     lastName          = "";
  private String     firstName         = "";
  private String     examiner          = "";
  private String     labName           = "";
  private String     lab               = "";
  private String     officeBranch      = "";
  private String     test              = "";
  private String     status            = "";
  private String     dateRequested     = "";
  private String     sevenDayMemoDate  = "";
  private String     followUpNo        = "";
  private String     followUpStartDate = "";
  private String     followUpEndDate   = "";
  private String     unassigned        = "";
  private String     assignedClientNo  = "";
  private String     appointmentDate   = "";
  private String     appointmentTime   = "";
  private String     statusTo          = "";
  private String     receiveDate       = "";
  private String     sortOrder         = "";
  private String     sortBy            = "";
  private String[]   medicalStatus     = null; 
  private String[]   indexTemp         = null;
  private String[]   refNo             = null;
  private String[]   medicalRecordId   = null;
  private String[]   testId            = null;
  private String[]   followUpDate      = null;
  private String[]   clientId          = null;
  private String[]   lastNameArr       = null;
  private String[]   firstNameArr      = null;
  private String[]   branches          = null;
  private String[]   createdDate       = null;
  private Collection medicalRecordData = null;
  
  private int pageNo = 1;
  private Page page;
  
 
          
  /**
   * Sets the date conducted attribute.
   * @param dateConducted date conducted
   */	
  public void setDateConducted(String dateConducted) {
	this.dateConducted = dateConducted;
  }
	
  /**
   * Retrieves the date conducted attribute.
   * @return String date conducted
   */
  public String getDateConducted(){
	return (this.dateConducted);
  }

  /**
   * Sets the date requested attribute.
   * @param dateRequested date received
   */	
  public void setDateRequested(String dateRequested) {
	this.dateRequested = dateRequested;
  }
	
  /**
   * Retrieves the date requested attribute.
   * @return String date requested
   */
  public String getDateRequested(){
	return (this.dateRequested);
  }

/**
 * Retrieves the client no attribute.
 * @return String client no
 */
public String getClientNo() {
	return clientNo;
}

/**
 * Retrieves the examiner attribute.
 * @return String examiner
 */
public String getExaminer() {
	return examiner;
}

/**
 * Retrieves the lab name attribute.
 * @return String lab name
 */
public String getLabName() {
	return labName;
}

/**
 * Retrieves the first name attribute.
 * @return String first name
 */
public String getFirstName() {
	return firstName;
}

/**
 * Retrieves the lab attribute.
 * @return String lab
 */
public String getLab() {
	return lab;
}

/**
 * Retrieves the last name attribute.
 * @return String last name
 */
public String getLastName() {
	return lastName;
}

/**
 * Retrieves the office/branch attribute.
 * @return String office/branch
 */
public String getOfficeBranch() {
	return officeBranch;
}

/**
 * Retrieves the reference no attribute.
 * @return String reference no
 */
public String getReferenceNo() {
	return referenceNo;
}

/**
 * Retrieves the status attribute.
 * @return String status
 */
public String getStatus() {
	return status;
}

/**
 * Retrieves the test attribute.
 * @return String test
 */
public String getTest() {
	return test;
}

/**
 * Sets the client no attribute. 
 * @param String clientNo
 */
public void setClientNo(String clientNo) {
	this.clientNo = clientNo;
}

/**
 * Sets the examiner attribute.
 * @param String examiner
 */
public void setExaminer(String examiner) {
	this.examiner = examiner;
}

/**
 * Sets the lab name attribute.
 * @param String labName
 */
public void setLabName(String labName) {
	this.labName = labName;
}

/**
 * Sets the first name attribute.
 * @param String firstName
 */
public void setFirstName(String firstName) {
	this.firstName = firstName;
}

/**
 * Sets the lab attribute.
 * @param String lab
 */
public void setLab(String lab) {
	this.lab = lab;
}

/**
 * Sets the last name attribute.
 * @param String lastName
 */
public void setLastName(String lastName) {
	this.lastName = lastName;
}

/**
 * Sets the office/branch attribute.
 * @param String officeBranch
 */
public void setOfficeBranch(String officeBranch) {
	this.officeBranch = officeBranch;
}

/**
 * Sets the reference no attribute.
 * @param String referenceNo
 */
public void setReferenceNo(String referenceNo) {
	this.referenceNo = referenceNo;
}

/**
 * Sets the status attribute.
 * @param String status
 */
public void setStatus(String status) {
	this.status = status;
}

/**
 * Sets the test attribute.
 * @param String test
 */
public void setTest(String test) {
	this.test = test;
}

/**
 * Retrieves the collection of medical records
 * @return Collection medical record data
 */
public Collection getMedicalRecordData() {
	return medicalRecordData;
}

/**
 * Sets the collection of medical records
 * @param Collection medicalRecordData
 */
public void setMedicalRecordData(Collection medicalRecordData) {
	this.medicalRecordData = medicalRecordData;
}

/**
 * Retrieves the followup end date attribute.
 * @return String followup end date
 */
public String getFollowUpEndDate() {
	return followUpEndDate;
}

/**
 * Retrieves the followup no attribute.
 * @return String followup no
 */
public String getFollowUpNo() {
	return followUpNo;
}

/**
 * Retrieves the followup start date attribute.
 * @return String followup start date
 */
public String getFollowUpStartDate() {
	return followUpStartDate;
}

/**
 * Retrievest the 7 day memo date attribute.
 * @return String 7 day memo date
 */
public String getSevenDayMemoDate() {
	return sevenDayMemoDate;
}

/**
 * Retrieves the unassigned attribute.
 * @return String unassigned
 */
public String getUnassigned() {
	return unassigned;
}

/**
 * Sets the followup end date attribute.
 * @param String followUpEndDate
 */
public void setFollowUpEndDate(String followUpEndDate) {
	this.followUpEndDate = followUpEndDate;
}

/**
 * Sets the followup no attribute.
 * @param String followUpNo
 */
public void setFollowUpNo(String followUpNo) {
	this.followUpNo = followUpNo;
}

/**
 * Sets the followup start date attribute.
 * @param String followup start date
 */
public void setFollowUpStartDate(String followUpStartDate) {
	this.followUpStartDate = followUpStartDate;
}

/**
 * Sets the 7day memo date attribute
 * @param String sevenDayMemoDate
 */
public void setSevenDayMemoDate(String sevenDayMemoDate) {
	this.sevenDayMemoDate = sevenDayMemoDate;
}

/**
 * Sets the unassigned attribute.
 * @param String unassigned
 */
public void setUnassigned(String unassigned) {
	this.unassigned = unassigned;
}

/**
 * @return
 */
public String[] getRefNo() {
	return refNo;
}

/**
 * @param strings
 */
public void setRefNo(String[] strings) {
	refNo = strings;
}

/**
 * @return
 */
public String getAppointmentDate() {
	return appointmentDate;
}

/**
 * @return
 */
public String getAppointmentTime() {
	return appointmentTime;
}

/**
 * @return
 */
public String getAssignedClientNo() {
	return assignedClientNo;
}

/**
 * @return
 */
public String getReceiveDate() {
	return receiveDate;
}

/**
 * @return
 */
public String getStatusTo() {
	return statusTo;
}

/**
 * @param string
 */
public void setAppointmentDate(String string) {
	appointmentDate = string;
}

/**
 * @param string
 */
public void setAppointmentTime(String string) {
	appointmentTime = string;
}

/**
 * @param string
 */
public void setAssignedClientNo(String string) {
	assignedClientNo = string;
}

/**
 * @param string
 */
public void setReceiveDate(String string) {
	receiveDate = string;
}

/**
 * @param string
 */
public void setStatusTo(String string) {
	statusTo = string;
}

/**
 * @return
 */
public String[] getIndexTemp() {
	return indexTemp;
}

/**
 * @param strings
 */
public void setIndexTemp(String[] strings) {
	indexTemp = strings;
}

/**
 * @return
 */
public String getConductedDate() {
	return conductedDate;
}

/**
 * @param string
 */
public void setConductedDate(String string) {
	conductedDate = string;
}

/**
 * @return
 */
public String[] getMedicalStatus() {
	return medicalStatus;
}

/**
 * @param strings
 */
public void setMedicalStatus(String[] strings) {
	medicalStatus = strings;
}

/**
 * @return
 */
public String[] getTestId() {
	return testId;
}

/**
 * @param strings
 */
public void setTestId(String[] strings) {
	testId = strings;
}

/**
 * @return
 */
public Page getPage() {
	return page;
}

/**
 * @return
 */
public int getPageNo() {
	return pageNo;
}

/**
 * @param page
 */
public void setPage(Page page) {
	this.page = page;
}

/**
 * @param i
 */
public void setPageNo(int i) {
	pageNo = i;
}

/**
 * @return
 */
public String[] getFollowUpDate() {
	return followUpDate;
}

/**
 * @param strings
 */
public void setFollowUpDate(String[] strings) {
	followUpDate = strings;
}

/**
 * @return
 */
public String getSortOrder() {
	return sortOrder;
}

/**
 * @param string
 */
public void setSortOrder(String string) {
	sortOrder = string;
}

/**
 * @return
 */
public String getSortBy() {
	return sortBy;
}

/**
 * @param string
 */
public void setSortBy(String string) {
	sortBy = string;
}

/**
 * @return
 */
public String[] getClientId() {
	return clientId;
}

/**
 * @param strings
 */
public void setClientId(String[] strings) {
	clientId = strings;
}

/**
 * @return
 */
public String[] getFirstNameArr() {
	return firstNameArr;
}

/**
 * @return
 */
public String[] getLastNameArr() {
	return lastNameArr;
}

/**
 * @param strings
 */
public void setFirstNameArr(String[] strings) {
	firstNameArr = strings;
}

/**
 * @param strings
 */
public void setLastNameArr(String[] strings) {
	lastNameArr = strings;
}

/**
 * @return
 */
public String[] getMedicalRecordId() {
	return medicalRecordId;
}

/**
 * @param strings
 */
public void setMedicalRecordId(String[] strings) {
	medicalRecordId = strings;
}

/**
 * @return
 */
public String[] getBranches() {
	return branches;
}

/**
 * @param strings
 */
public void setBranches(String[] strings) {
	branches = strings;
}

/**
 * @return
 */
public String[] getCreatedDate() {
	return createdDate;
}

/**
 * @param strings
 */
public void setCreatedDate(String[] strings) {
	createdDate = strings;
}

}

