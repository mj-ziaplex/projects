package com.slocpi.ium.ui.form;

// struts package
import org.apache.struts.action.ActionForm;


/**
 * This class contains the medical and lab record details.
 * @author: Ma. Theresa Vicencio
 * @version: 1.0
 */
public class MedLabRecordsForm extends ActionForm {

  private String code = "";
  private String testDescription = "";
  private String type = "";
  private String dateConducted = "";
  private String dateReceived = "";
    

  /**
   * Sets the test code attribute.
   * @param code test code
   */  
  public void setCode(String code) {
	this.code = code;
  }

  /**
   * Retrieves the test code attribute.
   * @return String test code
   */
  public String getCode() {
	return (this.code);
  }
  
  /**
   * Sets the test description attribute.
   * @param testDescription test description
   */  
  public void setTestDescription(String testDescription) {
	this.testDescription = testDescription;
  }

  /**
   * Retrieves the test description attribute.
   * @return String test description
   */
  public String getTestDescription() {
	return (this.testDescription);
  }
    
  /**
   * Sets the type of test attribute.
   * @param type type of test
   */	
  public void setType(String type) {
	this.type = type;
  }
	
  /**
   * Retrieves the type of test attribute.
   * @return String type of test
   */
  public String getType(){
	return (this.type);
  }

  /**
   * Sets the date conducted attribute.
   * @param dateConducted date conducted
   */	
  public void setDateConducted(String dateConducted) {
	this.dateConducted = dateConducted;
  }
	
  /**
   * Retrieves the date conducted attribute.
   * @return String date conducted
   */
  public String getDateConducted(){
	return (this.dateConducted);
  }

  /**
   * Sets the date received attribute.
   * @param dateReceived date received
   */	
  public void setDateReceived(String dateReceived) {
	this.dateReceived = dateReceived;
  }
	
  /**
   * Retrieves the date received attribute.
   * @return String date received
   */
  public String getDateReceived(){
	return (this.dateReceived);
  }

}

