package com.slocpi.ium.ui.form;

import java.util.Collection;
import org.apache.struts.action.ActionForm;


/**
 * This class contains the process configuration details.
 * @author: Barry Yu
 * @version: 1.0
 */
public class ProcessConfigForm extends ActionForm {
	private String eventId                  = "";    
	private String lob  					= "";
	private String eventName                = "";
	private String objectType               = "";
	private String status1  				= "";	
	private String fromStatus 				= "";
	private String toStatus	 				= "";
	private String notificationTemplate 	= "";
	private String activeInd 				= "";
 	private Collection processConfigEntries = null;
 	private Collection availableStatuses    = null;
 	private String actionType               = "";
 	private String roles					 = "";
 	
 	public void reset() {
 		this.eventId              = "";
 		this.eventName            = "";
 		this.toStatus             = "";
 		this.fromStatus           = "";
 		this.notificationTemplate = "";
 		this.activeInd            = "";
 	} 	
	/**
	 * @return
	 */
	public String getActiveInd() {
		return this.activeInd;
	}

	/**
	 * @return
	 */
	public String getFromStatus() {
		return this.fromStatus;
	}

	/**
	 * @return
	 */
	public String getLob() {
		return this.lob;
	}

	/**
	 * @return
	 */
	public String getNotificationTemplate() {
		return this.notificationTemplate;
	}

	/**
	 * @return
	 */
	public Collection getProcessConfigEntries() {
		return this.processConfigEntries;
	}

	/**
	 * @return
	 */
	public String getStatus1() {
		return this.status1;
	}
	

	/**
	 * @return
	 */
	public String getToStatus() {
		return this.toStatus;
	}

	/**
	 * @param string
	 */
	public void setActiveInd(String activeInd) {
		this.activeInd = activeInd;
	}

	/**
	 * @param string
	 */
	public void setFromStatus(String fromStatus) {
		this.fromStatus = fromStatus;
	}

	/**
	 * @param string
	 */
	public void setLob(String lob) {
		this.lob = lob;
	}

	/**
	 * @param string
	 */
	public void setNotificationTemplate(String notificationTemplate) {
		this.notificationTemplate = notificationTemplate;
	}

	/**
	 * @param list
	 */
	public void setProcessConfigEntries(Collection processConfigEntries) {
		this.processConfigEntries = processConfigEntries;
	}

	/**
	 * @param string
	 */
	public void setStatus1(String status1) {
		this.status1 = status1;
	}
	
	/**
	 * @param string
	 */
	public void setToStatus(String toStatus) {
		this.toStatus = toStatus;
	}

	/**
	 * @return
	 */
	public Collection getAvailableStatuses() {
		return availableStatuses;
	}

	/**
	 * @param list
	 */
	public void setAvailableStatuses(Collection availableStatuses) {
		this.availableStatuses = availableStatuses;
	}

	/**
	 * @return
	 */
	public String getObjectType() {
		return objectType;
	}

	/**
	 * @param string
	 */
	public void setObjectType(String objectType) {
		this.objectType = objectType;
	}

	/**
	 * @return
	 */
	public String getEventName() {
		return eventName;
	}

	/**
	 * @param string
	 */
	public void setEventName(String eventName) {
		this.eventName = eventName;
	}

	/**
	 * @return
	 */
	public String getEventId() {
		return eventId;
	}

	/**
	 * @param string
	 */
	public void setEventId(String eventId) {
		this.eventId = eventId;
	}	
	/**
	 * @return
	 */
	public String getActionType() {
		return actionType;
	}

	/**
	 * @param string
	 */
	public void setActionType(String actionType) {
		this.actionType = actionType;
	}

	/**
	 * Returns the roles.
	 * @return String
	 */
	public String getRoles() {
		return roles;
	}

	/**
	 * Sets the roles.
	 * @param roles The roles to set
	 */
	public void setRoles(String roles) {
		this.roles = roles;
	}

}

