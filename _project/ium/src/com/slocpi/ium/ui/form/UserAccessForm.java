/*
 * Created on Feb 18, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.ui.form;

import org.apache.struts.action.ActionForm;

/**
 * @author Abie
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class UserAccessForm extends ActionForm{
	
	private String userId = "";
	private String templateId = "";
	private String fullName = "";
	private String roleDesc = "";	
	private String[] pageId = null;
	private String[] pageName = null; 
	private String[] pageAccess = null;
	private String[] pageAccessResult = null;
	private String[] pageHeader = null;
		
	private int rowSize;
	private int accessCount;
	/**
	 * Retrieves the access count attribute. 
	 * @return
	 */
	public int getAccessCount() {
		return accessCount;
	}

	/**
	 * Retrieves the page access attribute. 
	 * @return
	 */
	public String[] getPageAccess() {
		return pageAccess;
	}

	/**
	 * Retrieves the page access result attribute. 
	 * @return
	 */
	public String[] getPageAccessResult() {
		return pageAccessResult;
	}

	/**
	 * Retrieves the page header attribute. 
	 * @return
	 */
	public String[] getPageHeader() {
		return pageHeader;
	}

	/**
	 * Retrieves the page ID attribute. 
	 * @return
	 */
	public String[] getPageId() {
		return pageId;
	}

	/**
	 * Retrieves the page name attribute. 
	 * @return
	 */
	public String[] getPageName() {
		return pageName;
	}

	/**
	 * Retrieves the row size attribute. 
	 * @return
	 */
	public int getRowSize() {
		return rowSize;
	}

	/**
	 * Retrieves the user ID attribute. 
	 * @return
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * Sets the access count attribute. 
	 * @param i
	 */
	public void setAccessCount(int i) {
		accessCount = i;
	}

	/**
	 * Sets the page access attribute. 
	 * @param strings
	 */
	public void setPageAccess(String[] strings) {
		pageAccess = strings;
	}

	/**
	 * Sets the page access result attribute. 
	 * @param strings
	 */
	public void setPageAccessResult(String[] strings) {
		pageAccessResult = strings;
	}

	/**
	 * Sets the page header attribute. 
	 * @param strings
	 */
	public void setPageHeader(String[] strings) {
		pageHeader = strings;
	}

	/**
	 * Sets the page ID attribute. 
	 * @param strings
	 */
	public void setPageId(String[] strings) {
		pageId = strings;
	}

	/**
	 * Sets the page name attribute. 
	 * @param strings
	 */
	public void setPageName(String[] strings) {
		pageName = strings;
	}

	/**
	 * Sets the row size attribute. 
	 * @param i
	 */
	public void setRowSize(int i) {
		rowSize = i;
	}

	/**
	 * Sets the user ID attribute. 
	 * @param string
	 */
	public void setUserId(String string) {
		userId = string;
	}

	/**
	 * Retrieves the role description attribute. 
	 * @return
	 */
	public String getRoleDesc() {
		return roleDesc;
	}

	/**
	 * Retrieves the template ID attribute. 
	 * @return
	 */
	public String getTemplateId() {
		return templateId;
	}

	/**
	 * Sets the role description attribute. 
	 * @param string
	 */
	public void setRoleDesc(String string) {
		roleDesc = string;
	}

	/**
	 * Sets the template Id attribute. 
	 * @param string
	 */
	public void setTemplateId(String string) {
		templateId = string;
	}

	/**
	 * Retrieves the full name attribute.
	 * @return
	 */
	public String getFullName() {
		return fullName;
	}

	/**
	 * Sets the full name attribute. 
	 * @param string
	 */
	public void setFullName(String string) {
		fullName = string;
	}

}
