package com.slocpi.ium.ui.form;

// struts package
import org.apache.struts.action.ActionForm;


/**
 * 
 * @author: Ingrid Villanueva
 * @version: 1.0
 */
public class AutoExpireForm extends ActionForm {

  private String     schedule = "";
  private String     notes    = "";
  private String     type     = null;

/**
 * @return
 */
public String getNotes() {
	return notes;
}

/**
 * @return
 */
public String getSchedule() {
	return schedule;
}

/**
 * @param string
 */
public void setNotes(String string) {
	notes = string;
}

/**
 * @param string
 */
public void setSchedule(String string) {
	schedule = string;
}

/**
 * @return
 */
public String getType() {
	return type;
}

/**
 * @param string
 */
public void setType(String string) {
	type = string;
}

}

