/*
 * Created on Jan 9, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.ui.form;

import org.apache.struts.action.ActionForm;

/**
 * @author Cris
 *
 * This will be the container of the selected requests in a Request List
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class RequestListForm extends ActionForm{
	
	private String [] referenceNumbers;
	private String status;
	private String assignedTo;
	
	
	/**
	 * @return
	 */
	public String[] getReferenceNumbers() {
		return referenceNumbers;
	}

	/**
	 * @return
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param strings
	 */
	public void setReferenceNumbers(String[] strings) {
		referenceNumbers = strings;
	}

	/**
	 * @param string
	 */
	public void setStatus(String string) {
		status = string;
	}

	/**
	 * @return
	 */
	public String getAssignedTo() {
		return assignedTo;
	}

	/**
	 * @param string
	 */
	public void setAssignedTo(String string) {
		assignedTo = string;
	}

}
