package com.slocpi.ium.ui.form;

// struts package
import org.apache.struts.action.ActionForm;


/**
 * 
 * @author: Ingrid Villanueva
 * @version: 1.0
 */
public class PurgeRecordsForm extends ActionForm {

  private String     startDate = "";
  private String     endDate   = "";
  private String     criteria  = "";
  private String     purgeDate = "";
  private String     numOfRec  = "";
  private String     result    = "";  

/**
 * @return
 */
public String getCriteria() {
	return criteria;
}

/**
 * @return
 */
public String getEndDate() {
	return endDate;
}

/**
 * @return
 */
public String getStartDate() {
	return startDate;
}

/**
 * @param string
 */
public void setCriteria(String string) {
	criteria = string;
}

/**
 * @param string
 */
public void setEndDate(String string) {
	endDate = string;
}

/**
 * @param string
 */
public void setStartDate(String string) {
	startDate = string;
}

/**
 * @return
 */
public String getNumOfRec() {
	return numOfRec;
}

/**
 * @return
 */
public String getPurgeDate() {
	return purgeDate;
}

/**
 * @return
 */
public String getResult() {
	return result;
}

/**
 * @param string
 */
public void setNumOfRec(String string) {
	numOfRec = string;
}

/**
 * @param string
 */
public void setPurgeDate(String string) {
	purgeDate = string;
}

/**
 * @param string
 */
public void setResult(String string) {
	result = string;
}

}

