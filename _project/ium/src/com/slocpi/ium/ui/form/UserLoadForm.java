/*
 * Created on Jan 22, 2004
 * package name = com.slocpi.ium.ui.form
 * file name    = UserLoadForm.java
 */
package com.slocpi.ium.ui.form;

import java.util.ArrayList;
import org.apache.struts.action.ActionForm;

/**
 * TODO Class Description of UserLoadForm.java
 * @author Engel
 * 
 */
public class UserLoadForm extends ActionForm {

	private String user_id;
	private String first_name;
	private String last_name;
	private String middle_name;
	private ArrayList loadByStatus;
	private String totalLoad;	
	
	
	/**
	 * TODO method description getFirst_name
	 * @return
	 */
	public String getFirst_name() {
		return first_name;
	}

	/**
	 * TODO method description getLast_name
	 * @return
	 */
	public String getLast_name() {
		return last_name;
	}

	/**
	 * TODO method description getLoadByStatus
	 * @return
	 */
	public ArrayList getLoadByStatus() {
		return loadByStatus;
	}

	/**
	 * TODO method description getMiddle_name
	 * @return
	 */
	public String getMiddle_name() {
		return middle_name;
	}

	/**
	 * TODO method description getTotalLoad
	 * @return
	 */
	public String getTotalLoad() {
		return totalLoad;
	}

	/**
	 * TODO method description getUser_id
	 * @return
	 */
	public String getUser_id() {
		return user_id;
	}

	/**
	 * TODO method description setFirst_name
	 * @param string
	 */
	public void setFirst_name(String string) {
		first_name = string;
	}

	/**
	 * TODO method description setLast_name
	 * @param string
	 */
	public void setLast_name(String string) {
		last_name = string;
	}

	/**
	 * TODO method description setLoadByStatus
	 * @param list
	 */
	public void setLoadByStatus(ArrayList list) {
		loadByStatus = list;
	}

	/**
	 * TODO method description setMiddle_name
	 * @param string
	 */
	public void setMiddle_name(String string) {
		middle_name = string;
	}

	/**
	 * TODO method description setTotalLoad
	 * @param i
	 */
	public void setTotalLoad(String string) {
		totalLoad = string;
	}

	/**
	 * TODO method description setUser_id
	 * @param string
	 */
	public void setUser_id(String string) {
		user_id = string;
	}

}
