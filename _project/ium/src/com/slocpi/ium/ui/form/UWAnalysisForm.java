/*
 * Created on Jun 9, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.ui.form;

import org.apache.struts.action.ActionForm;

/**
 * @author Cris
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class UWAnalysisForm extends ActionForm {
	private String referenceNumber = "";
	private String analysis = "";
	private String postedBy = "";
	private String postedDate = "";

	/**
	 * @return
	 */
	public String getAnalysis() {
		return analysis;
	}

	/**
	 * @return
	 */
	public String getPostedBy() {
		return postedBy;
	}

	/**
	 * @return
	 */
	public String getPostedDate() {
		return postedDate;
	}

	/**
	 * @return
	 */
	public String getReferenceNumber() {
		return referenceNumber;
	}

	/**
	 * @param string
	 */
	public void setAnalysis(String string) {
		analysis = string;
	}

	/**
	 * @param string
	 */
	public void setPostedBy(String string) {
		postedBy = string;
	}

	/**
	 * @param string
	 */
	public void setPostedDate(String string) {
		postedDate = string;
	}

	/**
	 * @param string
	 */
	public void setReferenceNumber(String string) {
		referenceNumber = string;
	}

}
