package com.slocpi.ium.ui.form;

// struts package
import java.util.ArrayList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.struts.action.ActionForm;

/**
 * This class contains the generic details of an assessment request.
 * @author: Ma. Theresa Vicencio
 * @version: 1.0
 */
public class AssessmentRequestForm extends ActionForm {

  private static final Logger LOGGER = LoggerFactory.getLogger(AssessmentRequestForm.class);

  private String refNo			= "";
  private String policySuffix	= "";
  private String lob			= "";
  private String lobDesc		= "";
  private String sourceSystem	= "";
  private String amountCovered	= "";
  private String premium		= "";
  private String insuredName	= "";
  private String branchCode		= "";
  private String branchName		= "";
  private String agentCode		= "";
  private String agentName		= "";
  private String requestStatus	= "";
  private String assignedTo		= "";
  private String underwriter	= "";
  private String underwriterName	= "";
  private String location		= "";
  private String remarks		= "";
  private String receivedDate	= "";
  private String dateForwarded	= "";
  private String dateEdited		= "";
  private String dateCreated	= "";
  private String statusDate		= "";

  private String sameClientData		= "Y";
  private String clientToSearch		= "";
  private String insuredOriginal	= "";
  private String ownerOriginal		= "";

  private String insuredClientType	= "";
  private String insuredClientNo	= "";
  private String insuredLastName	= "";
  private String insuredFirstName	= "";
  private String insuredMiddleName	= "";
  private String insuredTitle		= "";
  private String insuredSuffix		= "";
  private String insuredAge			= "";
  private String insuredSex			= "";
  private String insuredBirthDate	= "";

  private String ownerClientType	= "";
  private String ownerClientNo		= "";
  private String ownerLastName		= "";
  private String ownerFirstName		= "";
  private String ownerMiddleName	= "";
  private String ownerTitle			= "";
  private String ownerSuffix		= "";
  private String ownerAge			= "";
  private String ownerSex			= "";
  private String ownerBirthDate		= "";

  private String UWRemarks = "";
  private String approvingDept = ""; 

  private String transmitIndicator = "";
  private String [] checkedRefNum;
  private String autoSettleIndicator = "";

  private ArrayList agentBranches = new ArrayList();
  private String UWAnalysis = "";
  private String UWFinalDecision = "";

  private String autoSettleRemarks = "";

  /**
   * Sets the reference number attribute.
   * @param refNo policy no. or plan no. of the application
   */
  public void setRefNo(String refNo) {
	  this.refNo = refNo;
  }

  /**
   * Retrieves the reference number attribute.
   * @return String policy no. or plan no. of the application
   */
  public String getRefNo() {
	  return (this.refNo);
  }

  /**
   * Sets the line of business attribute.
   * @param lob line of business
   */
  public void setLob(String lob) {
	  this.lob = lob;
  }

  /**
   * Retrieves the line of business attribute.
   * @return String line of business
   */
  public String getLob() {
	  return (this.lob);
  }

  /**
   * Sets the source system attribute.
   * @param sourceSystem admin system that generated the assessment request
   */
  public void setSourceSystem(String sourceSystem) {
	  this.sourceSystem = sourceSystem;
  }

  /**
   * Retrieves the source system attribute.
   * @return String admin system that generated the assessment request
   */
  public String getSourceSystem() {
	  return (this.sourceSystem);
  }

  /**
   * Sets the amount covered attribute.
   * @param amountCovered the coverage of the application
   */
  public void setAmountCovered(String amountCovered) {
	  this.amountCovered = amountCovered;
  }

  /**
   * Retrieves the amount covered attribute.
   * @return String the coverage of the application
   */
  public String getAmountCovered() {
	  return (this.amountCovered);
  }

  /**
   * Sets the premium attribute.
   * @param premium the premium to be paid
   */
  public void setPremium(String premium) {
	  this.premium = premium;
  }

  /**
   * Retrieves the premium attribute.
   * @return String the premium to be paid
   */
  public String getPremium() {
	  return (this.premium);
  }

  /**
   * Sets the insured name attribute.
   * @param insuredName name of insured
   */
  public void setInsuredName(String insuredName) {
	  this.insuredName = insuredName;
  }

  /**
   * Retrieves the insured name attribute.
   * @return String name of insured
   */
  public String getInsuredName() {
	  return (this.insuredName);
  }

  /**
   * Sets the branch code attribute.
   * @param branchCode branch code of the branch where the application was filed
   */
  public void setBranchCode(String branchCode) {
	  this.branchCode = branchCode;
  }

  /**
   * Retrieves the branch code attribute.
   * @return String branch code of the branch where the application was filed
   */
  public String getBranchCode() {
	  return (this.branchCode);
  }

  /**
   * Sets the branch name attribute.
   * @param branchCode name of branch code selected
   */
  public void setBranchName(String branchName) {
	  this.branchName = branchName;
  }

  /**
   * Retrieves the branch name attribute.
   * @return String name of branch code selected
   */
  public String getBranchName() {
	  return (this.branchName);
  }

  /**
   * Sets the agent code attribute.
   * @param agentCode agent code of the agent of the application
   */
  public void setAgentCode(String agentCode) {
	  this.agentCode = agentCode;
  }

  /**
   * Retrieves the agent code attribute.
   * @return String agent code of the agent of the application
   */
  public String getAgentCode() {
	  return (this.agentCode);
  }

  /**
   * Sets the agent name attribute.
   * @param agentName agent name of agent code
   */
  public void setAgentName(String agentName) {
	  this.agentName = agentName;
  }

  /**
   * Retrieves the agent name attribute.
   * @return String agent name of agent code
   */
  public String getAgentName() {
	  return (this.agentName);
  }

  /**
   * Sets the request status attribute.
   * @param requestStatus current status of the request
   */
  public void setRequestStatus(String requestStatus) {
	  this.requestStatus = requestStatus;
  }

  /**
   * Retrieves the request status attribute.
   * @return String current status of the request
   */
  public String getRequestStatus() {
	  return (this.requestStatus);
  }

  /**
   * Sets the assigned to attribute.
   * @param assignedTo NB or USD personnel currently handling the application
   */
  public void setAssignedTo(String assignedTo) {
	  this.assignedTo = assignedTo;
  }

  /**
   * Retrieves the assigned to attribute.
   * @return String NB or USD personnel currently handling the application
   */
  public String getAssignedTo() {
	  return (this.assignedTo);
  }

  /**
   * Sets the underwriter attribute.
   * @param underwriter underwriter that is automatically assigned to the assessment request based on the assignment criteria that is set up in the LOV
   */
  public void setUnderwriter(String underwriter) {
	  this.underwriter = underwriter;
  }

  /**
   * Retrieves the underwriter attribute.
   * @return String underwriter that is automatically assigned to the assessment request based on the assignment criteria that is set up in the LOV
   */
  public String getUnderwriter() {
	  return (this.underwriter);
  }

  /**
   * Sets the location attribute.
   * @param location contains the user id/name of the person to whom the file of the application (folder) is forwarded to
   */
  public void setLocation(String location) {
	  this.location = location;
  }

  /**
   * Retrieves the location attribute.
   * @return String contains the user id/name of the person to whom the file of the application (folder) is forwarded to
   */
  public String getLocation() {
	  return (this.location);
  }

  /**
   * Sets the remarks attribute.
   * @param remarks may be used to indicate comments
   */
  public void setRemarks(String remarks) {
	  this.remarks = remarks;
  }

  /**
   * Retrieves the remarks attribute.
   * @return String may be used to indicate comments
   */
  public String getRemarks() {
	  return (this.remarks);
  }

  /**
   * Sets the received date attribute.
   * @param receivedDate date when application was received
   */
  public void setReceivedDate(String receivedDate) {
	  this.receivedDate = receivedDate;
  }

  /**
   * Retrieves the received date attribute.
   * @return String date when application was received
   */
  public String getReceivedDate() {
	  return (this.receivedDate);
  }

  /**
   * Sets the date forwarded attribute.
   * @param dateForwarded date when application was forwarded to the person indicated in the location
   */
  public void setDateForwarded(String dateForwarded) {
	  this.dateForwarded = dateForwarded;
  }

  /**
   * Retrieves the date forwarded attribute.
   * @return String date when application was forwarded to the person indicated in the location
   */
  public String getDateForwarded() {
	  return (this.dateForwarded);
  }

  /**
   * Sets the date edited attribute.
   * @param dateEdited date stamp when the request record was last updated
   */
  public void setDateEdited(String dateEdited) {
	  this.dateEdited = dateEdited;
  }

  /**
   * Retrieves the date edited attribute.
   * @return String date stamp when the request record was last updated
   */
  public String getDateEdited() {
	  return (this.dateEdited);
  }

  /**
   * Sets the date created attribute.
   * @param dateCreated date stamp when the request was created
   */
  public void setDateCreated(String dateCreated) {
	  this.dateCreated = dateCreated;
  }

  /**
   * Retrieves the date edited attribute.
   * @return String date stamp when the request was created
   */
  public String getDateCreated() {
	  return (this.dateCreated);
  }

  /**
   * Sets the status date attribute.
   * @param statusDate date stamp when the status was changed
   */
  public void setStatusDate(String statusDate) {
	  this.statusDate = statusDate;
  }

  /**
   * Retrieves the status date attribute.
   * @return String date stamp when the status was changed
   */
  public String getStatusDate() {
	  return (this.statusDate);
  }

  /**
   * Sets the same client data attribute.
   * @param sameClientData indicates if the insured and owner has the same client data
   */
  public void setSameClientData(String sameClientData) {
	  this.sameClientData = sameClientData;
  }

  /**
   * Retrieves the same client data attribute.
   * @return String indicates if the insured and owner has the same client data
   */
  public String getSameClientData() {
	  return (this.sameClientData);
  }

  /**
   * Sets the client to search attribute.
   * @param clientToSearch
   */
  public void setClientToSearch(String clientToSearch) {
	  this.clientToSearch = clientToSearch;
  }

  /**
   * Retrieves the client to search attribute.
   * @return String
   */
  public String getClientToSearch() {
	  return (this.clientToSearch);
  }

  /**
   * Sets the insured client type attribute.
   * @param insuredClientType client-policy relationship
   */
  public void setInsuredClientType(String insuredClientType) {
	  this.insuredClientType = insuredClientType;
  }

  /**
   * Retrieves the insured client type attribute.
   * @return String client-policy relationship
   */
  public String getInsuredClientType() {
	  return (this.insuredClientType);
  }

  /**
   * Sets the insured client number attribute.
   * @param insuredClientNo client id of insured
   */
  public void setInsuredClientNo(String insuredClientNo) {
	  this.insuredClientNo = insuredClientNo;
  }

  /**
   * Retrieves the insured client number attribute.
   * @return String client id of insured
   */
  public String getInsuredClientNo() {
	  return (this.insuredClientNo);
  }

  /**
   * Sets the insured last name attribute.
   * @param insuredLastName last name of client
   */
  public void setInsuredLastName(String insuredLastName) {
	  this.insuredLastName = insuredLastName;
  }

  /**
   * Retrieves the insured last name attribute.
   * @return String last name of client
   */
  public String getInsuredLastName() {
	  return (this.insuredLastName);
  }

  /**
   * Sets the insured first name attribute.
   * @param insuredFirstName first name of client
   */
  public void setInsuredFirstName(String insuredFirstName) {
	  this.insuredFirstName = insuredFirstName;
  }

  /**
   * Retrieves the insured first name attribute.
   * @return String first name of client
   */
  public String getInsuredFirstName() {
	  return (this.insuredFirstName);
  }

  /**
   * Sets the insured middle name attribute.
   * @param insuredMiddleName middle name of client
   */
  public void setInsuredMiddleName(String insuredMiddleName) {
	  this.insuredMiddleName = insuredMiddleName;
  }

  /**
   * Retrieves the insured middle name attribute.
   * @return String middle name of client
   */
  public String getInsuredMiddleName() {
	  return (this.insuredMiddleName);
  }

  /**
   * Sets the insured title attribute.
   * @param insuredTitle title of client
   */
  public void setInsuredTitle(String insuredTitle) {
	  this.insuredTitle = insuredTitle;
  }

  /**
   * Retrieves the insured title attribute.
   * @return String title of client
   */
  public String getInsuredTitle() {
	  return (this.insuredTitle);
  }

  /**
   * Sets the insured suffix attribute.
   * @param insuredSuffix suffix of client
   */
  public void setInsuredSuffix(String insuredSuffix) {
	  this.insuredSuffix = insuredSuffix;
  }

  /**
   * Retrieves the insured title attribute.
   * @return String suffix of client
   */
  public String getInsuredSuffix() {
	  return (this.insuredSuffix);
  }

  /**
   * Sets the insured age attribute.
   * @param insuredAge age of client
   */
  public void setInsuredAge(String insuredAge) {
	  this.insuredAge = insuredAge;
  }

  /**
   * Retrieves the insured age attribute.
   * @return String age of client
   */
  public String getInsuredAge() {
	  return (this.insuredAge);
  }

  /**
   * Sets the insured sex attribute.
   * @param insuredSex sex of client
   */
  public void setInsuredSex(String insuredSex) {
	  this.insuredSex = insuredSex;
  }

  /**
   * Retrieves the insured sex attribute.
   * @return String sex of client
   */
  public String getInsuredSex() {
	  return (this.insuredSex);
  }

  /**
   * Sets the insured birth date attribute.
   * @param insuredBirthDate birth date of client
   */
  public void setInsuredBirthDate(String insuredBirthDate) {
	  this.insuredBirthDate = insuredBirthDate;
  }

  /**
   * Retrieves the insured birth date attribute.
   * @return String birth date of client
   */
  public String getInsuredBirthDate() {
	  return (this.insuredBirthDate);
  }

  /**
   * Sets the owner client type attribute.
   * @param ownerClientType client-policy relationship
   */
  public void setOwnerClientType(String ownerClientType) {
	  this.ownerClientType = ownerClientType;
  }

  /**
   * Retrieves the owner client type attribute.
   * @return String client-policy relationship
   */
  public String getOwnerClientType() {
	  return (this.ownerClientType);
  }

  /**
   * Sets the owner client number attribute.
   * @param ownerClientNo client id of owner
   */
  public void setOwnerClientNo(String ownerClientNo) {
	  this.ownerClientNo = ownerClientNo;
  }

  /**
   * Retrieves the owner client number attribute.
   * @return String client id of owner
   */
  public String getOwnerClientNo() {
	  return (this.ownerClientNo);
  }

  /**
   * Sets the owner last name attribute.
   * @param ownerLastName last name of client
   */
  public void setOwnerLastName(String ownerLastName) {
	  this.ownerLastName = ownerLastName;
  }

  /**
   * Retrieves the owner last name attribute.
   * @return String last name of client
   */
  public String getOwnerLastName() {
	  return (this.ownerLastName);
  }

  /**
   * Sets the owner first name attribute.
   * @param ownerFirstName first name of client
   */
  public void setOwnerFirstName(String ownerFirstName) {
	  this.ownerFirstName = ownerFirstName;
  }

  /**
   * Retrieves the owner first name attribute.
   * @return String first name of client
   */
  public String getOwnerFirstName() {
	  return (this.ownerFirstName);
  }

  /**
   * Sets the owner middle name attribute.
   * @param ownerMiddleName middle name of client
   */
  public void setOwnerMiddleName(String ownerMiddleName) {
	  this.ownerMiddleName = ownerMiddleName;
  }

  /**
   * Retrieves the owner middle name attribute.
   * @return String middle name of client
   */
  public String getOwnerMiddleName() {
	  return (this.ownerMiddleName);
  }

  /**
   * Sets the owner title attribute.
   * @param ownerTitle title of client
   */
  public void setOwnerTitle(String ownerTitle) {
	  this.ownerTitle = ownerTitle;
  }

  /**
   * Retrieves the owner title attribute.
   * @return String title of client
   */
  public String getOwnerTitle() {
	  return (this.ownerTitle);
  }

  /**
   * Sets the owner suffix attribute.
   * @param ownerSuffix suffix of client
   */
  public void setOwnerSuffix(String ownerSuffix) {
	  this.ownerSuffix = ownerSuffix;
  }

  /**
   * Retrieves the owner title attribute.
   * @return String suffix of client
   */
  public String getOwnerSuffix() {
	  return (this.ownerSuffix);
  }

  /**
   * Sets the owner age attribute.
   * @param ownerAge age of client
   */
  public void setOwnerAge(String ownerAge) {
	  this.ownerAge = ownerAge;
  }

  /**
   * Retrieves the owner age attribute.
   * @return String age of client
   */
  public String getOwnerAge() {
	  return (this.ownerAge);
  }

  /**
   * Sets the owner sex attribute.
   * @param ownerSex sex of client
   */
  public void setOwnerSex(String ownerSex) {
	  this.ownerSex = ownerSex;
  }

  /**
   * Retrieves the owner sex attribute.
   * @return String sex of client
   */
  public String getOwnerSex() {
	  return (this.ownerSex);
  }

  /**
   * Sets the owner birth date attribute.
   * @param ownerBirthDate birth date of client
   */
  public void setOwnerBirthDate(String ownerBirthDate) {
	  this.ownerBirthDate = ownerBirthDate;
  }

  /**
   * Retrieves the owner birth date attribute.
   * @return String birth date of client
   */
  public String getOwnerBirthDate() {
	  return (this.ownerBirthDate);
  }


  /**
   * Sets the remarks attribute.
   * @param UWRemarks
   */
  public void setUWRemarks(String UWRemarks) {
	this.UWRemarks = UWRemarks;
  }


  /**
   * Retrieves the remarks attribute.
   * @return String remarks
   */
  public String getUWRemarks() {
	return (this.UWRemarks);
  }

  /**
   * Sets the list of selected reference numbers attribute.
   * @param checkedRefNum list of selected reference numbers
   */
  public void setCheckedRefNum(String[] checkedRefNum) {
	this.checkedRefNum = checkedRefNum;
  }

  /**
   * Retrieves the list of selected reference numbers attribute.
   * @return String list of selected reference numbers
   */
  public String[] getCheckedRefNum() {
	return (this.checkedRefNum);
  }

  /**
   * Prints the assessment request form's attributes and values in the console
   * @return
   */
  public void printAttributes() {}


  /**
   * TODO method description getInsuredOriginal
   * @return
   */
  public String getInsuredOriginal() {
    return insuredOriginal;
  }

  /**
   * TODO method description getOwnerOriginal
   * @return
   */
  public String getOwnerOriginal() {
    return ownerOriginal;
  }

  /**
   * TODO method description setInsuredOriginal
   * @param string
   */
  public void setInsuredOriginal(String string) {
    insuredOriginal = string;
  }

  /**
   * TODO method description setOwnerOriginal
   * @param string
   */
  public void setOwnerOriginal(String string) {
    ownerOriginal = string;
  }


/**
 * @return
 */
public String getTransmitIndicator() {
	return transmitIndicator;
}

/**
 * @param string
 */
public void setTransmitIndicator(String string) {
	transmitIndicator = string;
}

/**
 * @return
 */
public String getApprovingDept()
{
	return approvingDept;
}

/**
 * @param string
 */
public void setApprovingDept(String string)
{
	approvingDept = string;
}

/**
 * @return
 */
public String getAutoSettleIndicator() {
	return autoSettleIndicator;
}

/**
 * @param string
 */
public void setAutoSettleIndicator(String string) {
	autoSettleIndicator = string;
}

/**
 * @return
 */
public ArrayList getAgentBranches()
{
	return agentBranches;
}

/**
 * @param list
 */
public void setAgentBranches(ArrayList list)
{
	agentBranches = list;
}


/**
 * @return
 */
public String getUWAnalysis() {
	return UWAnalysis;
}

/**
 * @param string
 */
public void setUWAnalysis(String string) {
	UWAnalysis = string;
}

/**
 * 
 * @return
 */
public String getUWFinalDecision() {
	return UWFinalDecision;
}

/**
 * 
 * @param string
 */
public void setUWFinalDecision(String string) {
	UWFinalDecision = string;
}

/**
 * @return Returns the autoSettleRemarks.
 */
public String getAutoSettleRemarks() {
	return autoSettleRemarks;
}

/**
 * @param autoSettleRemarks The autoSettleRemarks to set.
 */
public void setAutoSettleRemarks(String autoSettleRemarks) {
	this.autoSettleRemarks = autoSettleRemarks;
}

public String getPolicySuffix() {
	return policySuffix;
}

public void setPolicySuffix(String suffix) {
	this.policySuffix = suffix;
}

public String getLobDesc() {
	return lobDesc;
}

public void setLobDesc(String lobDesc) {
	this.lobDesc = lobDesc;
}

public String getUnderwriterName() {
	return underwriterName;
}

public void setUnderwriterName(String underwriterName) {
	this.underwriterName = underwriterName;
}



}

