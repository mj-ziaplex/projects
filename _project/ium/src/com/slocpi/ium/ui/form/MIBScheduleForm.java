/*
 * Created on Mar 9, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.ui.form;

import org.apache.struts.action.ActionForm;

/**
 * @author Cris
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class MIBScheduleForm extends ActionForm{
	private String schedule;
	private String functionSelected;
	private String description;

	/**
	 * @return
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @return
	 */
	public String getFunctionSelected() {
		return functionSelected;
	}

	/**
	 * @return
	 */
	public String getSchedule() {
		return schedule;
	}

	/**
	 * @param string
	 */
	public void setDescription(String string) {
		description = string;
	}

	/**
	 * @param string
	 */
	public void setFunctionSelected(String string) {
		functionSelected = string;
	}

	/**
	 * @param string
	 */
	public void setSchedule(String string) {
		schedule = string;
	}

}
