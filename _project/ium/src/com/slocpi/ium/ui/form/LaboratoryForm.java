package com.slocpi.ium.ui.form;

import java.util.ArrayList;
import org.apache.struts.action.ActionForm;

/**
 * @author ppe
 *
 * To change this generated comment edit the template variable "typecomment":
 * Window>Preferences>Java>Templates.
 * To enable and disable the creation of type comments go to
 * Window>Preferences>Java>Code Generation.
 */
public class LaboratoryForm extends ActionForm {

	private ArrayList laboratoryList = null;
	
	private String labId = "";
	private String labName = "";
	private String contactPerson = "";
	private String busAdd1 = "";
	private String busAdd2 = "";
	private String busAdd3 = "";
	private String busCity = "";
	private String busCountry = "";
	private String busZipCode = "";
	private String ofcPhone1 = "";
	private String ofcPhone2 = "";
	private String faxNumber = "";
	private String branchName = "";
	private String branchAdd1 = "";
	private String branchAdd2 = "";
	private String branchAdd3 = "";
	private String branchCity = "";
	private String branchCountry = "";
	private String branchZipCode = "";
	private String branchPhone = "";	
	private String branchFaxNumber = "";
	private String accredited = "";
	private String accreditedDate = "";
	private String emailAddress = "";
	private String[] labTest;
	private String[] labFee;
	private String[] labTestStatus;	
	private String actionType = "display";
		  
	 
	
	/**
	 * Returns the laboratoryList.
	 * @return ArrayList
	 */
	public ArrayList getLaboratoryList() {
		return laboratoryList;
	}

	/**
	 * Sets the laboratoryList.
	 * @param laboratoryList The laboratoryList to set
	 */
	public void setLaboratoryList(ArrayList laboratoryList) {
		this.laboratoryList = laboratoryList;
	}

	/**
	 * Returns the accreditedDate.
	 * @return String
	 */
	public String getAccreditedDate() {
		return accreditedDate;
	}

	/**
	 * Returns the actionType.
	 * @return String
	 */
	public String getActionType() {
		return actionType;
	}

	/**
	 * Returns the contactPerson.
	 * @return String
	 */
	public String getContactPerson() {
		return contactPerson;
	}

	/**
	 * Returns the faxNumber.
	 * @return String
	 */
	public String getFaxNumber() {
		return faxNumber;
	}

	/**
	 * Returns the accredited.
	 * @return String
	 */
	public String getAccredited() {
		return accredited;
	}

	/**
	 * Returns the labFee.
	 * @return String[]
	 */
	public String[] getLabFee() {
		return labFee;
	}

	/**
	 * Returns the labId.
	 * @return String
	 */
	public String getLabId() {
		return labId;
	}

	/**
	 * Returns the labName.
	 * @return String
	 */
	public String getLabName() {
		return labName;
	}

	/**
	 * Returns the labTest.
	 * @return String[]
	 */
	public String[] getLabTest() {
		return labTest;
	}

	/**
	 * Returns the ofcPhone1.
	 * @return String
	 */
	public String getOfcPhone1() {
		return ofcPhone1;
	}

	/**
	 * Returns the ofcPhone2.
	 * @return String
	 */
	public String getOfcPhone2() {
		return ofcPhone2;
	}

	/**
	 * Sets the accreditedDate.
	 * @param accreditedDate The accreditedDate to set
	 */
	public void setAccreditedDate(String accreditedDate) {
		this.accreditedDate = accreditedDate;
	}

	/**
	 * Sets the actionType.
	 * @param actionType The actionType to set
	 */
	public void setActionType(String actionType) {
		this.actionType = actionType;
	}

	/**
	 * Sets the contactPerson.
	 * @param contactPerson The contactPerson to set
	 */
	public void setContactPerson(String contactPerson) {
		this.contactPerson = contactPerson;
	}

	/**
	 * Sets the faxNumber.
	 * @param faxNumber The faxNumber to set
	 */
	public void setFaxNumber(String faxNumber) {
		this.faxNumber = faxNumber;
	}

	/**
	 * Sets the accredited.
	 * @param accredited The accredited to set
	 */
	public void setAccredited(String accredited) {
		this.accredited = accredited;
	}

	/**
	 * Sets the labFee.
	 * @param labFee The labFee to set
	 */
	public void setLabFee(String[] labFee) {
		this.labFee = labFee;
	}

	/**
	 * Sets the labId.
	 * @param labId The labId to set
	 */
	public void setLabId(String labId) {
		this.labId = labId;
	}

	/**
	 * Sets the labName.
	 * @param labName The labName to set
	 */
	public void setLabName(String labName) {
		this.labName = labName;
	}

	/**
	 * Sets the labTest.
	 * @param labTest The labTest to set
	 */
	public void setLabTest(String[] labTest) {
		this.labTest = labTest;
	}

	/**
	 * Sets the ofcPhone1.
	 * @param ofcPhone1 The ofcPhone1 to set
	 */
	public void setOfcPhone1(String ofcPhone1) {
		this.ofcPhone1 = ofcPhone1;
	}

	/**
	 * Sets the ofcPhone2.
	 * @param ofcPhone2 The ofcPhone2 to set
	 */
	public void setOfcPhone2(String ofcPhone2) {
		this.ofcPhone2 = ofcPhone2;
	}

	/**
	 * Returns the branchAdd1.
	 * @return String
	 */
	public String getBranchAdd1() {
		return branchAdd1;
	}

	/**
	 * Returns the branchAdd2.
	 * @return String
	 */
	public String getBranchAdd2() {
		return branchAdd2;
	}

	/**
	 * Returns the branchAdd3.
	 * @return String
	 */
	public String getBranchAdd3() {
		return branchAdd3;
	}

	/**
	 * Returns the branchCity.
	 * @return String
	 */
	public String getBranchCity() {
		return branchCity;
	}

	/**
	 * Returns the branchCountry.
	 * @return String
	 */
	public String getBranchCountry() {
		return branchCountry;
	}

	/**
	 * Returns the branchFaxNumber.
	 * @return String
	 */
	public String getBranchFaxNumber() {
		return branchFaxNumber;
	}

	/**
	 * Returns the branchName.
	 * @return String
	 */
	public String getBranchName() {
		return branchName;
	}

	/**
	 * Returns the branchZipCode.
	 * @return String
	 */
	public String getBranchZipCode() {
		return branchZipCode;
	}

	/**
	 * Returns the busAdd1.
	 * @return String
	 */
	public String getBusAdd1() {
		return busAdd1;
	}

	/**
	 * Returns the busAdd2.
	 * @return String
	 */
	public String getBusAdd2() {
		return busAdd2;
	}

	/**
	 * Returns the busAdd3.
	 * @return String
	 */
	public String getBusAdd3() {
		return busAdd3;
	}

	/**
	 * Returns the busCity.
	 * @return String
	 */
	public String getBusCity() {
		return busCity;
	}

	/**
	 * Returns the busCountry.
	 * @return String
	 */
	public String getBusCountry() {
		return busCountry;
	}

	/**
	 * Returns the busZipCode.
	 * @return String
	 */
	public String getBusZipCode() {
		return busZipCode;
	}

	/**
	 * Sets the branchAdd1.
	 * @param branchAdd1 The branchAdd1 to set
	 */
	public void setBranchAdd1(String branchAdd1) {
		this.branchAdd1 = branchAdd1;
	}

	/**
	 * Sets the branchAdd2.
	 * @param branchAdd2 The branchAdd2 to set
	 */
	public void setBranchAdd2(String branchAdd2) {
		this.branchAdd2 = branchAdd2;
	}

	/**
	 * Sets the branchAdd3.
	 * @param branchAdd3 The branchAdd3 to set
	 */
	public void setBranchAdd3(String branchAdd3) {
		this.branchAdd3 = branchAdd3;
	}

	/**
	 * Sets the branchCity.
	 * @param branchCity The branchCity to set
	 */
	public void setBranchCity(String branchCity) {
		this.branchCity = branchCity;
	}

	/**
	 * Sets the branchCountry.
	 * @param branchCountry The branchCountry to set
	 */
	public void setBranchCountry(String branchCountry) {
		this.branchCountry = branchCountry;
	}

	/**
	 * Sets the branchFaxNumber.
	 * @param branchFaxNumber The branchFaxNumber to set
	 */
	public void setBranchFaxNumber(String branchFaxNumber) {
		this.branchFaxNumber = branchFaxNumber;
	}

	/**
	 * Sets the branchName.
	 * @param branchName The branchName to set
	 */
	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	/**
	 * Sets the branchZipCode.
	 * @param branchZipCode The branchZipCode to set
	 */
	public void setBranchZipCode(String branchZipCode) {
		this.branchZipCode = branchZipCode;
	}

	/**
	 * Sets the busAdd1.
	 * @param busAdd1 The busAdd1 to set
	 */
	public void setBusAdd1(String busAdd1) {
		this.busAdd1 = busAdd1;
	}

	/**
	 * Sets the busAdd2.
	 * @param busAdd2 The busAdd2 to set
	 */
	public void setBusAdd2(String busAdd2) {
		this.busAdd2 = busAdd2;
	}

	/**
	 * Sets the busAdd3.
	 * @param busAdd3 The busAdd3 to set
	 */
	public void setBusAdd3(String busAdd3) {
		this.busAdd3 = busAdd3;
	}

	/**
	 * Sets the busCity.
	 * @param busCity The busCity to set
	 */
	public void setBusCity(String busCity) {
		this.busCity = busCity;
	}

	/**
	 * Sets the busCountry.
	 * @param busCountry The busCountry to set
	 */
	public void setBusCountry(String busCountry) {
		this.busCountry = busCountry;
	}

	/**
	 * Sets the busZipCode.
	 * @param busZipCode The busZipCode to set
	 */
	public void setBusZipCode(String busZipCode) {
		this.busZipCode = busZipCode;
	}

	/**
	 * Returns the branchPhone.
	 * @return String
	 */
	public String getBranchPhone() {
		return branchPhone;
	}

	/**
	 * Sets the branchPhone.
	 * @param branchPhone The branchPhone to set
	 */
	public void setBranchPhone(String branchPhone) {
		this.branchPhone = branchPhone;
	}

	/**
	 * Returns the emailAddress.
	 * @return String
	 */
	public String getEmailAddress() {
		return emailAddress;
	}

	/**
	 * Sets the emailAddress.
	 * @param emailAddress The emailAddress to set
	 */
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}
	

	/**
	 * Returns the labTestStatus.
	 * @return String[]
	 */
	public String[] getLabTestStatus() {
		return labTestStatus;
	}

	/**
	 * Sets the labTestStatus.
	 * @param labTestStatus The labTestStatus to set
	 */
	public void setLabTestStatus(String[] labTestStatus) {
		this.labTestStatus = labTestStatus;
	}

}
