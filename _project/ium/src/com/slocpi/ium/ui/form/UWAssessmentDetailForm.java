package com.slocpi.ium.ui.form;

// java package
import java.util.ArrayList;

/**
 * This class contains the kickout reasons, list of requirements, and medical and lab records. 
 * It also includes the requirement details.
 * @author: Ma. Theresa Vicencio
 * @version: 1.0
 */
public class UWAssessmentDetailForm extends AssessmentRequestForm {

  private String idSelected; 
  private ArrayList impairments = null;
  private ArrayList listOfAnalysis = null;


  /**
   * Sets the impairments attribute.
   * @param list impairments
   */	
  public void setImpairments(ArrayList impairments) {
	this.impairments = impairments;
  }
	
  /**
   * Retrieves the impairments attribute.
   * @return ArrayList impairments
   */
  public ArrayList getImpairments(){
	return (this.impairments);
  }

  /**
   * Sets the id selected attribute.
   * @param idSelected id of the selected impairment
   */	
  public void setIdSelected(String string) {
    idSelected = string;
  }    

  /**
   * Retrieves the id selected attribute.
   * @return String id of the selected impairment
   */
  public String getIdSelected() {
    return idSelected;
  }


/**
 * @return
 */
public ArrayList getListOfAnalysis() {
	return listOfAnalysis;
}

/**
 * @param list
 */
public void setListOfAnalysis(ArrayList list) {
	listOfAnalysis = list;
}

}

