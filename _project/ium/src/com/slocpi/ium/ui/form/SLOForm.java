/*
 * Created on Mar 6, 2004
 * package name = com.slocpi.ium.ui.form
 * file name    = SLOForm.java
 */
package com.slocpi.ium.ui.form;

import org.apache.struts.action.ActionForm;

/**
 * TODO Class Description of SLOForm.java
 * @author Engel
 * 
 */
public class SLOForm extends ActionForm {
	
	private String officeId;
	private String officeName;
	private String officeType;
	private String addr1;
	private String addr2;
	private String addr3;
	private String city;
	private String province;
	private String country;
	private String zipCode;
	private String contactNumber;
	private String faxNumber;
	
	private String pageNumber;

	/**
	 * TODO method description getAddr1
	 * @return
	 */
	public String getAddr1() {
		return addr1;
	}

	/**
	 * TODO method description getAddr2
	 * @return
	 */
	public String getAddr2() {
		return addr2;
	}

	/**
	 * TODO method description getAddr3
	 * @return
	 */
	public String getAddr3() {
		return addr3;
	}

	/**
	 * TODO method description getCity
	 * @return
	 */
	public String getCity() {
		return city;
	}

	/**
	 * TODO method description getContactNumber
	 * @return
	 */
	public String getContactNumber() {
		return contactNumber;
	}

	/**
	 * TODO method description getFaxNumber
	 * @return
	 */
	public String getFaxNumber() {
		return faxNumber;
	}

	/**
	 * TODO method description getOfficeId
	 * @return
	 */
	public String getOfficeId() {
		return officeId;
	}

	/**
	 * TODO method description getOfficeName
	 * @return
	 */
	public String getOfficeName() {
		return officeName;
	}

	/**
	 * TODO method description getOfficeType
	 * @return
	 */
	public String getOfficeType() {
		return officeType;
	}

	/**
	 * TODO method description getProvince
	 * @return
	 */
	public String getProvince() {
		return province;
	}

	/**
	 * TODO method description getZipCode
	 * @return
	 */
	public String getZipCode() {
		return zipCode;
	}

	/**
	 * TODO method description setAddr1
	 * @param string
	 */
	public void setAddr1(String string) {
		addr1 = string;
	}

	/**
	 * TODO method description setAddr2
	 * @param string
	 */
	public void setAddr2(String string) {
		addr2 = string;
	}

	/**
	 * TODO method description setAddr3
	 * @param string
	 */
	public void setAddr3(String string) {
		addr3 = string;
	}

	/**
	 * TODO method description setCity
	 * @param string
	 */
	public void setCity(String string) {
		city = string;
	}

	/**
	 * TODO method description setContactNumber
	 * @param string
	 */
	public void setContactNumber(String string) {
		contactNumber = string;
	}

	/**
	 * TODO method description setFaxNumber
	 * @param string
	 */
	public void setFaxNumber(String string) {
		faxNumber = string;
	}

	/**
	 * TODO method description setOfficeId
	 * @param string
	 */
	public void setOfficeId(String string) {
		officeId = string;
	}

	/**
	 * TODO method description setOfficeName
	 * @param string
	 */
	public void setOfficeName(String string) {
		officeName = string;
	}

	/**
	 * TODO method description setOfficeType
	 * @param string
	 */
	public void setOfficeType(String string) {
		officeType = string;
	}

	/**
	 * TODO method description setProvince
	 * @param string
	 */
	public void setProvince(String string) {
		province = string;
	}

	/**
	 * TODO method description setZipCode
	 * @param string
	 */
	public void setZipCode(String string) {
		zipCode = string;
	}

	/**
	 * TODO method description getCountry
	 * @return
	 */
	public String getCountry() {
		return country;
	}

	/**
	 * TODO method description setCountry
	 * @param string
	 */
	public void setCountry(String string) {
		country = string;
	}

	/**
	 * TODO method description getPageNumber
	 * @return
	 */
	public String getPageNumber() {
		return pageNumber;
	}

	/**
	 * TODO method description setPageNumber
	 * @param string
	 */
	public void setPageNumber(String string) {
		pageNumber = string;
	}

}
