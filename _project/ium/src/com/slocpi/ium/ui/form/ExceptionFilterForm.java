/*
 * Created on Mar 5, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.ui.form;

import java.util.ArrayList;
import org.apache.struts.action.ActionForm;


/**
 * @author Cris
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class ExceptionFilterForm extends ActionForm{
	private String startDate = "";
	private String endDate = "";
	private String recordType = "";
	private String interfaceSelected = "";
	private String idSelected;
	private ArrayList exceptions = null;
	private ArrayList details = null;

	
	/**
	 * @return
	 */
	public String getEndDate() {
		return endDate;
	}

	/**
	 * @return
	 */
	public ArrayList getExceptions() {
		return exceptions;
	}

	/**
	 * @return
	 */
	public String getRecordType() {
		return recordType;
	}

	/**
	 * @return
	 */
	public String getStartDate() {
		return startDate;
	}

	/**
	 * @param string
	 */
	public void setEndDate(String string) {
		endDate = string;
	}

	/**
	 * @param list
	 */
	public void setExceptions(ArrayList list) {
		exceptions = list;
	}

	/**
	 * @param string
	 */
	public void setRecordType(String string) {
		recordType = string;
	}

	/**
	 * @param string
	 */
	public void setStartDate(String string) {
		startDate = string;
	}

	/**
	 * @return
	 */
	public String getIdSelected() {
		return idSelected;
	}

	/**
	 * @param string
	 */
	public void setIdSelected(String string) {
		idSelected = string;
	}

	/**
	 * @return
	 */
	public ArrayList getDetails() {
		return details;
	}

	/**
	 * @param list
	 */
	public void setDetails(ArrayList list) {
		details = list;
	}

	/**
	 * @return
	 */
	public String getInterfaceSelected() {
		return interfaceSelected;
	}

	/**
	 * @param string
	 */
	public void setInterfaceSelected(String string) {
		interfaceSelected = string;
	}

}
