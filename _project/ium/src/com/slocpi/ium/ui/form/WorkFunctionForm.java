/*
 * Created on Feb 26, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.ui.form;

import org.apache.struts.action.ActionForm;

/**
 * @author Abie
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class WorkFunctionForm extends ActionForm{
		
	private String[] pageNameId = null;
	private String[] pageName = null; 
	private String[] pageAccess = null;
	private String[] pageAccessResult = null;
	private String[] pageHeader = null;
	private int rowSize;
	private int accessCount;
	

	/**
	 * Retrieves the access count attribute. 
	 * @return
	 */
	public int getAccessCount() {
		return accessCount;
	}

	/**
	 * Retrieves the page access attribute. 
	 * @return
	 */
	public String[] getPageAccess() {
		return pageAccess;
	}

	/**
	 * Retrieves the page access result attribute. 
	 * @return
	 */
	public String[] getPageAccessResult() {
		return pageAccessResult;
	}

	/**
	 * Retrieves the page header attribute. 
	 * @return
	 */
	public String[] getPageHeader() {
		return pageHeader;
	}

	/**
	 * Retrieves the page name attribute. 
	 * @return
	 */
	public String[] getPageName() {
		return pageName;
	}

	/**
	 * Retrieves the page name ID attribute.
	 * @return
	 */
	public String[] getPageNameId() {
		return pageNameId;
	}

	/**
	 * Retrieves the row size attribute. 
	 * @return
	 */
	public int getRowSize() {
		return rowSize;
	}

	/**
	 * Sets the access count attribute. 
	 * @param i
	 */
	public void setAccessCount(int i) {
		accessCount = i;
	}

	/**
	 * Sets the page access attribute. 
	 * @param strings
	 */
	public void setPageAccess(String[] strings) {
		pageAccess = strings;
	}

	/**
	 * Sets the page access result attribute. 
	 * @param strings
	 */
	public void setPageAccessResult(String[] strings) {
		pageAccessResult = strings;
	}

	/**
	 * Sets the page header attribute. 
	 * @param strings
	 */
	public void setPageHeader(String[] strings) {
		pageHeader = strings;
	}

	/**
	 * Sets the page name attribute. 
	 * @param strings
	 */
	public void setPageName(String[] strings) {
		pageName = strings;
	}

	/**
	 * Sets the page name attribute. 
	 * @param strings
	 */
	public void setPageNameId(String[] strings) {
		pageNameId = strings;
	}

	/**
	 * Sets the row size attribute. 
	 * @param i
	 */
	public void setRowSize(int i) {
		rowSize = i;
	}

}
