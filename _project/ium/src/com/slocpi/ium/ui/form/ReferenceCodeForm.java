package com.slocpi.ium.ui.form;

import org.apache.struts.action.ActionForm;
import com.slocpi.ium.util.IUMConstants;

/**
 * This class contains the reference code, description and list.
 * @author: Ma. Theresa Vicencio
 * @version: 1.0
 */
public class ReferenceCodeForm extends ActionForm {

	private String referenceCode = "";
	private String code = "";
	private String description = "";
	private String maintainFlag = IUMConstants.NO;
	private String idxCode = "";
	private String idxUpdateCode = "";

	//for requirement forms
	private String formName = "";
	private String templateFormat = "";
	private String templateName = "";
	private String urlFile = "";

	//status indicator
	private String status = "";

	//for auto-assignment and access template (role id)
	private String id = "";

	//for status code and test profile
	private String type = "";

	//for test profile
	private String daysValid = "";
	private String taxable = "";
	private String followUpNo = "";

	//for examiner rank
	private String fee = "";
	
	//for auto-assignment
	private String underwriter = "";

	//for holiday
	private String date = "";
	private String year = "";

	//for sorting
	private String sortBy = "";
	private String sortOrder = "";
		
	
	/**
	 * Sets the reference code attribute.
	 * @param referenceCode reference code
	 */  
	public void setReferenceCode(String referenceCode) {
		this.referenceCode = referenceCode;
	}

	/**
	 * Retrieves the reference code attribute.
	 * @return String reference code
	 */
	public String getReferenceCode() {
		return (this.referenceCode);
	}
    
	/**
	 * Sets the code attribute.
	 * @param code code
	 */  
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * Retrieves the code attribute.
	 * @return String code
	 */
	public String getCode() {
		return (this.code);
	}

	/**
	 * Sets the description attribute.
	 * @param description description
	 */  
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Retrieves the description attribute.
	 * @return String description
	 */
	public String getDescription() {
		return (this.description);
	}

	/**
	 * Sets the maintain flag attribute.
	 * @param maintainFlag indicates if the action is maintain
	 */  
	public void setMaintainFlag(String maintainFlag) {
		this.maintainFlag = maintainFlag;
	}

	/**
	 * Retrieves the maintain flag attribute.
	 * @return String indicates if the action is maintain
	 */
	public String getMaintainFlag() {
		return (this.maintainFlag);
	}

	/**
	 * Sets the index code attribute.
	 * @param idxCode code of the chosen record for view 
	 */  
	public void setIdxCode(String idxCode) {
		this.idxCode = idxCode;
	}

	/**
	 * Retrieves the index code attribute.
	 * @return String code of the chosen record for view
	 */
	public String getIdxCode() {
		return (this.idxCode);
	}

	/**
	 * Sets the index code attribute.
	 * @param idxUpdateCode code of the chosen record for update 
	 */  
	public void setIdxUpdateCode(String idxUpdateCode) {
		this.idxUpdateCode = idxUpdateCode;
	}

	/**
	 * Retrieves the index code attribute.
	 * @return String code of the chosen record for update
	 */
	public String getIdxUpdateCode() {
		return (this.idxUpdateCode);
	}
		
	//============================== FOR REQUIREMENT FORM ==============================	
	/**
	 * Sets the form name attribute.
	 * @param formName form name
	 */  
	public void setFormName(String formName) {
		this.formName = formName;
	}

	/**
	 * Retrieves the form name attribute.
	 * @return String form name
	 */
	public String getFormName() {
		return (this.formName);
	}
  
	/**
	 * Sets the template format attribute.
	 * @param templateFormat file format used for the form
	 */  
	public void setTemplateFormat(String templateFormat) {
		this.templateFormat = templateFormat;
	}

	/**
	 * Retrieves the template format attribute.
	 * @return String file format used for the form
	 */
	public String getTemplateFormat() {
		return (this.templateFormat);
	}

	/**
	 * Sets the template name attribute.
	 * @param templateName name of the file
	 */  
	public void setTemplateName(String templateName) {
		this.templateName = templateName;
	}

	/**
	 * Retrieves the template name attribute.
	 * @return String name of the file
	 */
	public String getTemplateName() {
		return (this.templateName);
	}

	/**
	 * Sets the status attribute.
	 * @param status defines if the form is still active and in-use
	 */  
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * Retrieves the status attribute.
	 * @return String defines if the form is still active and in-use
	 */
	public String getStatus() {
		return (this.status);
	}
	
	/**
	 * Sets the url file attribute.
	 * @param urlFile complete location and file name
	 */  
	public void setUrlFile(String urlFile) {
		this.urlFile = urlFile;
	}

	/**
	 * Retrieves the status attribute.
	 * @return String complete location and file name
	 */
	public String getUrlFile() {
		return (this.urlFile);
	}	
	//============================== FOR REQUIREMENT FORM ==============================


	//============================== FOR AUTO-ASSIGNMENT AND ACCESS TEMPLATE ==============================
	/**
	 * Sets the id attribute.
	 * @param type id
	 */  
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * Retrieves the id attribute.
	 * @return String id
	 */
	public String getId() {
		return (this.id);
	}
	//============================== FOR AUTO-ASSIGNMENT AND ACCESS TEMPLATE ==============================


	//============================== FOR STATUS CODE AND TEST PROFILE ==============================
	/**
	 * Sets the type attribute.
	 * @param type type
	 */  
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * Retrieves the type attribute.
	 * @return String type
	 */
	public String getType() {
		return (this.type);
	}
	//============================== FOR STATUS CODE AND TEST PROFILE ==============================
	

	//============================== FOR TEST PROFILE ==============================
	/**
	 * Sets the days valid attribute.
	 * @param daysValid number of days the test result is considered valid from the date it was conducted
	 */  
	public void setDaysValid(String daysValid) {
		this.daysValid = daysValid;
	}

	/**
	 * Retrieves the days valid attribute.
	 * @return String number of days the test result is considered valid from the date it was conducted
	 */
	public String getDaysValid() {
		return (this.daysValid);
	}
	/**
	 * Sets the taxable attribute.
	 * @param taxable signifies whether the test conducted is taxable or not
	 */  
	public void setTaxable(String taxable) {
		this.taxable = taxable;
	}

	/**
	 * Retrieves the taxable attribute.
	 * @return String signifies whether the test conducted is taxable or not
	 */
	public String getTaxable() {
		return (this.taxable);
	}	
	
	/**
	 * Returns the followUpNo.
	 * @return String
	 */
	public String getFollowUpNo() {
		return followUpNo;
	}

	/**
	 * Sets the followUpNo.
	 * @param followUpNo The followUpNo to set
	 */
	public void setFollowUpNo(String followUpNo) {
		this.followUpNo = followUpNo;
	}
	//============================== FOR TEST PROFILE ==============================		


	//============================== FOR EXAMINER RANK ==============================
	/**
	 * Sets the fee attribute.
	 * @param fee set consultation fee based on rank
	 */  
	public void setFee(String fee) {
		this.fee = fee;
	}

	/**
	 * Retrieves the fee attribute.
	 * @return String set consultation fee based on rank
	 */
	public String getFee() {
		return (this.fee);
	}
	//============================== FOR EXAMINER RANK ==============================


	//============================== FOR AUTO-ASSIGNMENT ==============================
	/**
	 * Sets the underwriter attribute.
	 * @param underwriter underwriter
	 */  
	public void setUnderwriter(String underwriter) {
		this.underwriter = underwriter;
	}

	/**
	 * Retrieves the underwriter attribute.
	 * @return String underwriter
	 */
	public String getUnderwriter() {
		return (this.underwriter);
	}
	//============================== FOR AUTO-ASSIGNMENT ==============================


	//============================== FOR HOLIDAY ==============================
	/**
	 * Sets the date attribute.
	 * @param date date
	 */  
	public void setDate(String date) {
		this.date = date;
	}

	/**
	 * Retrieves the date attribute.
	 * @return String date
	 */
	public String getDate() {
		return (this.date);
	}
	
	/**
	 * Sets the year attribute.
	 * @param year year
	 */  
	public void setYear(String year) {
		this.year = year;
	}

	/**
	 * Retrieves the year attribute.
	 * @return String year
	 */
	public String getYear() {
		return (this.year);
	}	
	//============================== FOR HOLIDAY ==============================


	//============================== FOR SORTING ==============================
	/**
	 * Sets the sort by attribute.
	 * @param sortBy sort by
	 */  
	public void setSortBy(String sortBy) {
		this.sortBy = sortBy;
	}

	/**
	 * Retrieves the sort by attribute.
	 * @return String sort by
	 */
	public String getSortBy() {
		return (this.sortBy);
	}
	
	/**
	 * Sets the sort order attribute.
	 * @param sortOrder sortOrder
	 */  
	public void setSortOrder(String sortOrder) {
		this.sortOrder = sortOrder;
	}

	/**
	 * Retrieves the sortOrder attribute.
	 * @return String sortOrder
	 */
	public String getSortOrder() {
		return (this.sortOrder);
	}	
	//============================== FOR SORTING ==============================				

}

