package com.slocpi.ium.ui.form;


import java.util.Collection;
import org.apache.struts.action.ActionForm;
import com.slocpi.ium.ui.util.Page;


/**
 * 
 * @author: Ingrid Villanueva
 * @version: 1.0
 */
public class UserProfileListForm extends ActionForm {

  private String     userIdFilter     = "";
  private String     acf2IdFilter     = "";
  private String     lastNameFilter   = "";
  private String     departmentFilter = "";
  private String     branchNameFilter = "";  
  private Collection userProfileData = null;

  private int pageNo = 1;
  private Page page;
  
 
/**
 * Retrieves the ACF2 Id attribute
 * @return String ACF2 Id
 */
public String getAcf2IdFilter() {
	return acf2IdFilter;
}

/**
 * Retrieves the branch name attribute
 * @return String branch name
 */
public String getBranchNameFilter() {
	return branchNameFilter;
}

/**
 * Retrieves the department attribute
 * @return String department
 */
public String getDepartmentFilter() {
	return departmentFilter;
}

/**
 * Retrieves the last name attribute
 * @return String last name
 */
public String getLastNameFilter() {
	return lastNameFilter;
}

/**
 * Retrieves the user Id attribute
 * @return String user Id
 */
public String getUserIdFilter() {
	return userIdFilter;
}

/**
 * Retrieves the collection of user profiles
 * @return Collection user profile data
 */
public Collection getUserProfileData() {
	return userProfileData;
}

/**
 * Sets the ACF2 Id attribute
 * @param String acf2Id
 */
public void setAcf2IdFilter(String acf2IdFilter) {
	this.acf2IdFilter = acf2IdFilter;
}

/**
 * Sets the branch name attribute
 * @param String branchName
 */
public void setBranchNameFilter(String branchNameFilter) {
	this.branchNameFilter = branchNameFilter;
}

/**
 * Sets the department attribute
 * @param String department
 */
public void setDepartmentFilter(String departmentFilter) {
	this.departmentFilter = departmentFilter;
}

/**
 * Sets the last name attribute
 * @param String lastName 
 */
public void setLastNameFilter(String lastNameFilter) {
	this.lastNameFilter = lastNameFilter;
}

/**
 * Sets the user id attribute
 * @param String userId
 */
public void setUserIdFilter(String userIdFilter) {
	this.userIdFilter = userIdFilter;
}

/**
 * Sets the collection of user profiles
 * @param Collection userProfileData
 */
public void setUserProfileData(Collection userProfileData) {
	this.userProfileData = userProfileData;
}

/**
 * @return
 */
public Page getPage() {
	return page;
}

/**
 * @return
 */
public int getPageNo() {
	return pageNo;
}

/**
 * @param page
 */
public void setPage(Page page) {
	this.page = page;
}

/**
 * @param i
 */
public void setPageNo(int i) {
	pageNo = i;
}

}

