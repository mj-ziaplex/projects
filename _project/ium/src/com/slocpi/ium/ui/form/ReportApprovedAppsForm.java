/*
 * Created on Mar 1, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.ui.form;

import java.util.ArrayList;
import org.apache.struts.action.ActionForm;
import com.slocpi.ium.ui.util.Page;

/**
 * @author Nic Decapia
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class ReportApprovedAppsForm extends ActionForm
{
	private String startDate = "";
	private String endDate = "";
	private String requestor = "";
	private String approver ="";
	private ArrayList content = null;

	private int pageNo = 1;
	private Page page;	
	
	/**
	 * @return
	 */
	public String getApprover()
	{
		return approver;
	}

	/**
	 * @return
	 */
	public ArrayList getContent()
	{
		return content;
	}

	/**
	 * @return
	 */
	public String getEndDate()
	{
		return endDate;
	}

	/**
	 * @return
	 */
	public String getRequestor()
	{
		return requestor;
	}

	/**
	 * @return
	 */
	public String getStartDate()
	{
		return startDate;
	}

	/**
	 * @param string
	 */
	public void setApprover(String string)
	{
		approver = string;
	}

	/**
	 * @param list
	 */
	public void setContent(ArrayList list)
	{
		content = list;
	}

	/**
	 * @param string
	 */
	public void setEndDate(String string)
	{
		endDate = string;
	}

	/**
	 * @param string
	 */
	public void setRequestor(String string)
	{
		requestor = string;
	}

	/**
	 * @param string
	 */
	public void setStartDate(String string)
	{
		startDate = string;
	}

	/**
	 * @return
	 */
	public Page getPage()
	{
		return page;
	}

	/**
	 * @return
	 */
	public int getPageNo()
	{
		return pageNo;
	}

	/**
	 * @param page
	 */
	public void setPage(Page page)
	{
		this.page = page;
	}

	/**
	 * @param i
	 */
	public void setPageNo(int i)
	{
		pageNo = i;
	}

}
