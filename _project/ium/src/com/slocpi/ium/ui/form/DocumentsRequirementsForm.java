package com.slocpi.ium.ui.form;

import java.util.Collection;
import org.apache.struts.action.ActionForm;

/**
 * This class contains the dosuments and requirements details.
 * @author: Barry Yu
 * @version: 1.0
 */
public class DocumentsRequirementsForm extends ActionForm {
	private String documentType  	= "";
	private String requirement   	= "";
	private String lob           	= "";
	private Collection docTypes  	= null;
	private Collection requirements = null;
	
	
	/**
	 * @return
	 */
	public String getDocumentType() {
		return documentType;
	}

	/**
	 * @return
	 */
	public String getRequirement() {
		return requirement;
	}
	
	/**
	 * @return
	 */
	public String getLob() {
		return lob;
	}
	
	/**
	 * @param string
	 */
	public void setDocumentType(String documentType) {
		this.documentType = documentType;
	}

	/**
	 * @param string
	 */
	public void setRequirement(String requirement) {
		this.requirement = requirement;
	}

	/**
	 * @param string
	 */
	public void setLob(String lob) {
		this.lob = lob;
	}

	/**
	 * @return
	 */
	public Collection getDocTypes() {
		return docTypes;
	}

	/**
	 * @return
	 */
	public Collection getRequirements() {
		return requirements;
	}

	/**
	 * @param collection
	 */
	public void setDocTypes(Collection docTypes) {
		this.docTypes = docTypes;
	}

	/**
	 * @param collection
	 */
	public void setRequirements(Collection requirements) {
		this.requirements = requirements;
	}

}

