/*
 * Created on Jan 22, 2004
 * package name = com.slocpi.ium.ui.form
 * file name    = UserLoadListForm.java
 */
package com.slocpi.ium.ui.form;

import java.util.ArrayList;
import org.apache.struts.action.ActionForm;

/**
 * Container class for underwriter load.
 * @author Engel
 * 
 */
public class UserLoadListForm extends ActionForm {

	private ArrayList userLoadList=null;


	/**
	 * TODO method description getUserLoadList
	 * @return
	 */
	public ArrayList getUserLoadList() {
		return userLoadList;
	}

	/**
	 * TODO method description setUserLoadList
	 * @param list
	 */
	public void setUserLoadList(ArrayList list) {
		userLoadList = list;
	}

}
