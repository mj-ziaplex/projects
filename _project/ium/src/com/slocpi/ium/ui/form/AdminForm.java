package com.slocpi.ium.ui.form;

// struts package
import org.apache.struts.action.ActionForm;

/**
 * @TODO Class Description AdminForm
 * @author: Randall C. Rivera
 * @version: 1.0
 */
public class AdminForm extends ActionForm {

  private int mibForExport = 0;
  private int medRecForExp = 0;
  private int pwdRequired = 0;
    
  
  public void setMibForExport(int mibForExport) {
    this.mibForExport = mibForExport;
  }

  public int getMibForExport() {
	return (this.mibForExport);
  }

  public void setMedRecForExp(int medRecForExp) {
	this.medRecForExp = medRecForExp;
  }

  public int getMedRecForExp() {
	return (this.medRecForExp);
  }  

  public void setPwdRequired(int pwdRequired) {
	this.pwdRequired= pwdRequired;
  }

  public int getPwdRequired() {
	return (this.pwdRequired);
  }
  
  
}

