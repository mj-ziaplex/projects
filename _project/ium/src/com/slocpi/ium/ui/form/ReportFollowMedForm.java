/*
 * Created on Mar 8, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.ui.form;

import java.util.ArrayList;
import org.apache.struts.action.ActionForm;
import com.slocpi.ium.ui.util.Page;

/**
 * @author Nic Decapia
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class ReportFollowMedForm extends ActionForm
{
	// parameters
	private String startDate = "";
	private String endDate = "";
	private String reportType = "";
	
	// filters
	private String branch = null;
	private String examiner = null;
	private String underwriter = null;
	private String laboratory = null;
		
	private ArrayList content = null;

	private int pageNo = 1;
	private Page page;		
	/**
	 * @return
	 */
	public String getBranch()
	{
		return branch;
	}

	/**
	 * @return
	 */
	public ArrayList getContent()
	{
		return content;
	}

	/**
	 * @return
	 */
	public String getEndDate()
	{
		return endDate;
	}

	/**
	 * @return
	 */
	public String getExaminer()
	{
		return examiner;
	}

	/**
	 * @return
	 */
	public String getLaboratory()
	{
		return laboratory;
	}

	/**
	 * @return
	 */
	public Page getPage()
	{
		return page;
	}

	/**
	 * @return
	 */
	public int getPageNo()
	{
		return pageNo;
	}

	/**
	 * @return
	 */
	public String getReportType()
	{
		return reportType;
	}

	/**
	 * @return
	 */
	public String getStartDate()
	{
		return startDate;
	}

	/**
	 * @return
	 */
	public String getUnderwriter()
	{
		return underwriter;
	}

	/**
	 * @param string
	 */
	public void setBranch(String string)
	{
		branch = string;
	}

	/**
	 * @param list
	 */
	public void setContent(ArrayList list)
	{
		content = list;
	}

	/**
	 * @param string
	 */
	public void setEndDate(String string)
	{
		endDate = string;
	}

	/**
	 * @param string
	 */
	public void setExaminer(String string)
	{
		examiner = string;
	}

	/**
	 * @param string
	 */
	public void setLaboratory(String string)
	{
		laboratory = string;
	}

	/**
	 * @param page
	 */
	public void setPage(Page page)
	{
		this.page = page;
	}

	/**
	 * @param i
	 */
	public void setPageNo(int i)
	{
		pageNo = i;
	}

	/**
	 * @param string
	 */
	public void setReportType(String string)
	{
		reportType = string;
	}

	/**
	 * @param string
	 */
	public void setStartDate(String string)
	{
		startDate = string;
	}

	/**
	 * @param string
	 */
	public void setUnderwriter(String string)
	{
		underwriter = string;
	}

}
