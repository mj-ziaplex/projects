package com.slocpi.ium.ui.form;


import java.util.Collection;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import com.slocpi.ium.IUMException;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.UserManager;


/**
 * 
 * @author: Ingrid Villanueva
 * @version: 1.0
 */
public class UserProfileForm extends ActionForm {
	private static final Logger LOGGER = LoggerFactory.getLogger(UserProfileForm.class);
	
  private static final String empty = "";
  private String     userId          = "";
  private String     acf2Id          = "";
  private String     lastName        = "";
  private String     firstName       = "";
  private String     middleName      = "";
  private String     emailAddr       = "";
  private String     contactNo       = "";
  private String     department      = "";
  private String     branchName      = "";
  private String     password        = empty;
  private String     confirmPassword = empty;
  private String 	 oldPassword     = empty;
  private String 	 oPassword	     = empty;
  private String 	 newPassword  	 = empty;
  private String     locked          = "";
  private String     activated       = "";
  private String     accessLevel     = "";
  private String     addr1           = "";
  private String     addr2           = "";
  private String     addr3           = "";
  private String     country         = "";
  private String     city            = "";
  private String     zipCode         = "";
  private String     sex             = "";
  private String     userType        = "";
  private String     userCode        = "";
  private String     section         = "";
  private String     mobileNo        = "";  
  private String 	 defaultRole	 = "";
  
  private Collection roles;
  private String roleToAdd			 = "";
  private String[] usrRoles;
  //for user passwords
  private String password1 = empty;
  private String password2 = empty;
  private String password3 = empty;
  private String password4 = empty;
  private String password5 = empty;
 
  /** 
   * This method checks the validity of the password fields in the ChangePasswordAction based on the non-functional rules on passwords.  
   * It displays corresponging error messages when one of the fields is invalid. It uses checkPwdValidity method to check the correctness
   * of the composition of the new password.  
   * @param ActionMapping Mapping of the flow of control
   * @param HttpServletRequest Request object
   * @return ActionErrors ActionError object
   */
  public ActionErrors validate(
	  ActionMapping mapping,
	  HttpServletRequest request) {
		
	  LOGGER.info("validate start");
	  ActionErrors errors = new ActionErrors();
	  String invalidPwd = "$\"'?<>,() @.";
	  
	  try {
		
		if(this.oldPassword.length()<1){
			errors.add(ActionErrors.GLOBAL_ERROR,new ActionError("error.password.blank","Old Password"));	
			/*this.oldPassword = "";	*/
			request.setAttribute("userProfileForm", this);
		} 
		else if(this.newPassword.length()<1){
			errors.add(ActionErrors.GLOBAL_ERROR,new ActionError("error.password.blank", "New Password"));
			/*this.newPassword = "";		*/
			request.setAttribute("userProfileForm", this);
		} 
    	else if(this.confirmPassword.length()<1){
			errors.add(ActionErrors.GLOBAL_ERROR,new ActionError("error.password.blank","Confirm New Password"));
			/*this.confirmPassword = "";*/		
			request.setAttribute("userProfileForm", this);
		}
	    else{
				//mismatch of old password entry
				if(!(this.oldPassword.trim()).equals(this.password.trim())){
					errors.add(ActionErrors.GLOBAL_ERROR,new ActionError("error.password.oldnomatch"));
					this.setOldPassword("");
					request.setAttribute("userProfileForm", this);
				}
				
				//invalid length of password entries
				else if(((this.confirmPassword.length()<6) && !(this.confirmPassword.length()>16)) ||
					((this.newPassword.length() <6) && !(this.newPassword.length()>16))){
					errors.add(ActionErrors.GLOBAL_ERROR,new ActionError("error.password.invalidlen"));		
							
			 				if((this.confirmPassword.length()<6) && !(this.confirmPassword.length()>16)){
			 					this.setConfirmPassword("");
			 				}
			 				if((this.newPassword.length()<6) && !(this.newPassword.length()>16)) {
								this.setNewPassword("");
			 				}
					request.setAttribute("userProfileForm", this);
				}
				
				//mistype of new passwords
				else if (!this.confirmPassword.equals(this.newPassword)) {
					 errors.add(ActionErrors.GLOBAL_ERROR,new ActionError("error.password.newnomatch"));
					 this.setConfirmPassword("");
					 this.setNewPassword("");		
					 request.setAttribute("userProfileForm", this);          	  
				}
				//no changes on password entries
				else if(this.confirmPassword.equals(this.oldPassword)){
					errors.add(ActionErrors.GLOBAL_ERROR,new ActionError("error.passwords.match"));
					this.setConfirmPassword("");
					this.setNewPassword("");								
					request.setAttribute("userProfileForm", this);
				}
				else if (! checkPwdValidity(this.newPassword, invalidPwd)){
					errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.password.invalidlen"));
					this.setConfirmPassword("");
					this.setNewPassword("");
					request.setAttribute("userProfileForm", this);
				}else if(this.sameAsPrevPasswords()){
					errors.add(ActionErrors.GLOBAL_ERROR,new ActionError("error.passwords.match"));
					this.setConfirmPassword("");
					this.setNewPassword("");								
					request.setAttribute("userProfileForm", this);
				}else if(!this.canChangePassword(userId)){//check if password has been changed today
					errors.add(ActionErrors.GLOBAL_ERROR,new ActionError("error.password.cantchngepswd"));
					this.setConfirmPassword("");
					this.setNewPassword("");								
					request.setAttribute("userProfileForm", this);
				}
			}
      } catch (Exception e) {
		  LOGGER.error(CodeHelper.getStackTrace(e));
	  }		
	  LOGGER.info("validate end");
	  return errors;
  }
  
  //check for previous password
  private boolean sameAsPrevPasswords(){
	  
    LOGGER.info("sameAsPrevPasswords start");
	
  		if(!newPassword.equals(password1) && !newPassword.equals(password2) && !newPassword.equals(password3)
			&& !newPassword.equals(password4) && !newPassword.equals(password5)){
			LOGGER.debug("sameAsPrevPasswords - false");	
			return false;	
		}
		LOGGER.debug("sameAsPrevPasswords - true");
		LOGGER.info("sameAsPrevPasswords end");
  		return true;	
  }
  
  /**
   * This method compares the new password to a series of invalid characters.  
   * @param newPwd
   * @param invalidStr
   * @return
   */
  private boolean checkPwdValidity(String newPwd, String invalidStr){
	  
	  LOGGER.info("checkPwdValidity start");
  	int pwdLen = newPwd.length();
  	int strLen = invalidStr.length();
  	boolean valid = true;
  	
  	for(int x=0; x<strLen; x++){
  		for(int y=0; y<pwdLen; y++){
  			if(newPwd.charAt(y) == invalidStr.charAt(x)){
  				valid = false;
  			}
  		}
  	}
  	LOGGER.info("checkPwdValidity end");
  	return valid;        
  }
  
  
  private boolean canChangePassword(String userid){
	  
	  LOGGER.info("canChangePassword start");
  	 boolean ret = true;
  	 try{
		UserManager um = new UserManager();
		long x = um.getPswdDays(userid);
		if(x==0){
			ret = false;
		}
  	 }catch(IUMException e){
  		LOGGER.error(CodeHelper.getStackTrace(e));
  	 }	
  	LOGGER.info("canChangePassword end");
  	 return ret;
  }
/**
 * Retrieves the ACF2 Id attribute.
 * @return String ACF2 Id
 */
public String getAcf2Id() {
	return acf2Id;
}

/**
 * Retrieves the branch name attribute.
 * @return String branch name
 */
public String getBranchName() {
	return branchName;
}

/**
 * Retrieves the department attribute.
 * @return String department
 */
public String getDepartment() {
	return department;
}

/**
 * Retrieves the last name attribute.
 * @return String last name
 */
public String getLastName() {
	return lastName;
}

/**
 * Retrieves the user Id attribute.
 * @return String user Id
 */
public String getUserId() {
	return userId;
}

/**
 * Sets the ACF2 Id attribute.
 * @param String acf2Id
 */
public void setAcf2Id(String acf2Id) {
	this.acf2Id = acf2Id;
}

/**
 * Sets the branch name attribute.
 * @param String branchName
 */
public void setBranchName(String branchName) {
	this.branchName = branchName;
}

/**
 * Sets the department attribute.
 * @param String department
 */
public void setDepartment(String department) {
	this.department = department;
}

/**
 * Sets the last name attribute.
 * @param String lastName 
 */
public void setLastName(String lastName) {
	this.lastName = lastName;
}

/**
 * Sets the user id attribute.
 * @param String userId
 */
public void setUserId(String userId) {
	this.userId = userId;
}

/**
 * Retrieves the contact number attribute.
 * @return
 */
public String getContactNo() {
	return contactNo;
}

/**
 * Retrieves the email address attribute. 
 * @return
 */
public String getEmailAddr() {
	return emailAddr;
}

/**
 * Retrieves the first name attribute. 
 * @return
 */
public String getFirstName() {
	return firstName;
}

/**
 * Retrieves the middle name attribute. 
 * @return
 */
public String getMiddleName() {
	return middleName;
}

/**
 * Sets the contact number attribute. 
 * @param string
 */
public void setContactNo(String string) {
	contactNo = string;
}

/**
 * Sets the email address attribute. 
 * @param string
 */
public void setEmailAddr(String string) {
	emailAddr = string;
}

/**
 * Sets the first name attribute. 
 * @param string
 */
public void setFirstName(String string) {
	firstName = string;
}

/**
 * Sets the middle name attribute.
 * @param string
 */
public void setMiddleName(String string) {
	middleName = string;
}


/**
 * Retrieves the password attribute. 
 * @return
 */
public String getPassword() {
	return password;
}

/**
 * Sets the password attribute. 
 * @param string
 */
public void setPassword(String string) {
	password = string;
}

/**
 * Retrieves the confirm password attribute. 
 * @return
 */
public String getConfirmPassword() {
	return confirmPassword;
}

/**
 * Sets the confirm password attribute. 
 * @param string
 */
public void setConfirmPassword(String string) {
	confirmPassword = string;
}

/**
 * Retrieves the access level attribute. 
 * @return
 */
public String getAccessLevel() {
	return accessLevel;
}

/**
 * Retrieves the activated attribute. 
 * @return
 */
public String getActivated() {
	return activated;
}

/**
 * Retrieves the address1 attribute.
 * @return
 */
public String getAddr1() {
	return addr1;
}

/**
 * Retrieves the address2 attribute. 
 * @return
 */
public String getAddr2() {
	return addr2;
}

/**
 * Retrieves the address3 attribute.
 * @return
 */
public String getAddr3() {
	return addr3;
}

/**
 * Retrieves the city attribute. 
 * @return
 */
public String getCity() {
	return city;
}

/**
 * Retrieve the country attribute. 
 * @return
 */
public String getCountry() {
	return country;
}

/**
 * Retrieves the locked attribute. 
 * @return
 */
public String getLocked() {
	return locked;
}

/**
 * Retrives the zip code attribute. 
 * @return
 */
public String getZipCode() {
	return zipCode;
}

/**
 * Sets the access level attribute. 
 * @param string
 */
public void setAccessLevel(String string) {
	accessLevel = string;
}

/**
 * Sets the activated attribute. 
 * @param string
 */
public void setActivated(String string) {
	activated = string;
}

/**
 * Sets the address1 attribute. 
 * @param string
 */
public void setAddr1(String string) {
	addr1 = string;
}

/**
 * Sets the address2 attribute. 
 * @param string
 */
public void setAddr2(String string) {
	addr2 = string;
}

/**
 * Sets the address3 attribute. 
 * @param string
 */
public void setAddr3(String string) {
	addr3 = string;
}

/**
 * Sets the city attribute. 
 * @param string
 */
public void setCity(String string) {
	city = string;
}

/**
 * Sets the country attribute. 
 * @param string
 */
public void setCountry(String string) {
	country = string;
}

/**
 * Sets the locked attribute. 
 * @param string
 */
public void setLocked(String string) {
	locked = string;
}

/**
 * Sets the zip code attribute. 
 * @param string
 */
public void setZipCode(String string) {
	zipCode = string;
}

/**
 * Retrieves the sex attribute. 
 * @return
 */
public String getSex() {
	return sex;
}

/**
 * Sets the sex attribute. 
 * @param string
 */
public void setSex(String string) {
	sex = string;
}

/**
 * Retrieves the user type attribute. 
 * @return
 */
public String getUserType() {
	return userType;
}

/**
 * Sets the user type attribute. 
 * @param string
 */
public void setUserType(String string) {
	userType = string;
}

/**
 * Retrieves the user code attribute. 
 * @return
 */
public String getUserCode() {
	return userCode;
}

/**
 * Sets the user code attribute. 
 * @param string
 */
public void setUserCode(String string) {
	userCode = string;
}

/**
 * Retrieves the section attribute. 
 * @return
 */
public String getSection() {
	return section;
}

/**
 * Sets the section attribute. 
 * @param string
 */
public void setSection(String string) {
	section = string;
}

/**
 * Retrieves the mobile number attribute. 
 * @return
 */
public String getMobileNo() {
	return mobileNo;
}

/**
 * Sets the mobile number attribute. 
 * @param string
 */
public void setMobileNo(String string) {
	mobileNo = string;
}

/**
 * Retrieves the roles attribute. 
 * @return
 */
public Collection getRoles() {
	return roles;
}

/**
 * Sets the roles attribute.
 * @param collection
 */
public void setRoles(Collection collection) {
	roles = collection;
}

/**
 * TODO method description getDefaultRole
 * @return
 */
public String getDefaultRole() {
	return defaultRole;
}

/**
 * TODO method description setDefaultRole
 * @param string
 */
public void setDefaultRole(String string) {
	defaultRole = string;
}

/**
 * Retrieves the role attribute that is to be added. 
 * @return
 */
public String getRoleToAdd() {
	return roleToAdd;
}

/**
 * Sets the role attribute that is to be added. 
 * @param string
 */
public void setRoleToAdd(String string) {
	roleToAdd = string;
}

/**
 * Retrieves the user roles attribute.
 * @return
 */
public String[] getUsrRoles() {
	return usrRoles;
}

/**
 * Sets the user roles attribute. 
 * @param string
 */
public void setUsrRoles(String[] string) {
	usrRoles = string;
}

/**
 * Retrieves the old password attribute. 
 * @return
 */
public String getOldPassword() {
	return oldPassword;
}

/**
 * Sets the old password attribute. 
 * @param string
 */
public void setOldPassword(String string) {
	oldPassword = string;
}

/**
 * Retrieves the opassword attribute. 
 * @return
 */
public String getOPassword() {
	return oPassword;
}

/**
 * Sets the opassword attribute. 
 * @param string
 */
public void setOPassword(String string) {
	oPassword = string;
}

/**
 * Retrieves the new password attribute. 
 * @return
 */
public String getNewPassword() {
	return newPassword;
}

/**
 * Sets the new password attribute. 
 * @param string
 */
public void setNewPassword(String string) {
	newPassword = string;
}

/**
 * @return
 */
public String getPassword1() {
	return password1;
}

/**
 * @return
 */
public String getPassword2() {
	return password2;
}

/**
 * @return
 */
public String getPassword3() {
	return password3;
}

/**
 * @return
 */
public String getPassword4() {
	return password4;
}

/**
 * @return
 */
public String getPassword5() {
	return password5;
}

/**
 * @param string
 */
public void setPassword1(String string) {
	password1 = string;
}

/**
 * @param string
 */
public void setPassword2(String string) {
	password2 = string;
}

/**
 * @param string
 */
public void setPassword3(String string) {
	password3 = string;
}

/**
 * @param string
 */
public void setPassword4(String string) {
	password4 = string;
}

/**
 * @param string
 */
public void setPassword5(String string) {
	password5 = string;
}

}

