package com.slocpi.ium.ui.form;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.apache.struts.action.ActionForm;


/**
 * This class contains the kick-out reasons details.
 * @author: Ma. Theresa Vicencio
 * @version: 1.0
 */
public class KOReasonsForm extends ActionForm {

  private String seq = "";
  private String messageText = "";
  private String clientNo = "";
  private String failResponse = "";
  
  public String toString(){
	  
	  return ToStringBuilder.reflectionToString(this, ToStringStyle.DEFAULT_STYLE);
  }
  
  //jonbred
  private String policyID = "";
  /**
   * Sets the sequence number attribute.
   * @param seq sequence no.
   */  
  public void setSeq(String seq) {
	this.seq = seq;
  }

  /**
   * Retrieves the sequence number attribute.
   * @return String sequence no.
   */
  public String getSeq() {
	return (this.seq);
  }
  
  /**
   * Sets the message text attribute.
   * @param messageText clear case kick-out reason
   */  
  public void setMessageText(String messageText) {
	this.messageText = messageText;
  }

  /**
   * Retrieves the message text attribute.
   * @return String clear case kick-out reason
   */
  public String getMessageText() {
	return (this.messageText);
  }
    
  /**
   * Sets the client number attribute.
   * @param clientNo client number
   */	
  public void setClientNo(String clientNo) {
	this.clientNo = clientNo;
  }
	
  /**
   * Retrieves the client number attribute.
   * @return String client number
   */
  public String getClientNo(){
	return (this.clientNo);
  }

  /**
   * Sets the fail response attribute.
   * @param failResponse fail response
   */	
  public void setFailResponse(String failResponse) {
	this.failResponse = failResponse;
  }
	
  /**
   * Retrieves the failResponse attribute.
   * @return String fail response
   */
  public String getFailResponse(){
	return (this.failResponse);
  }
  
  
  
  
  /**jonbred
   * Sets the policy ID attribute.
   * @param clientNo policy ID
   */	
  public void setPolicyID(String policyID) {
	this.clientNo = policyID;
  }
	
  /**
   * Retrieves the policy ID attribute.
   * @return String policy ID
   */
  public String getPolicyID(){
	return (this.policyID);
  }

}

