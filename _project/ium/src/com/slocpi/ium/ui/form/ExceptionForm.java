/*
 * Created on Mar 5, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.ui.form;






/**
 * @author Cris
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class ExceptionForm extends ExceptionFilterForm{
	private String exceptionId = "";
	private String exceptionRefNum = "";
	private String exceptionInterface = "";
	private String exceptionDate = "";
	private String recordType = "";
	
	/**
	 * @return
	 */
	public String getExceptionDate() {
		return exceptionDate;
	}

	/**
	 * @return
	 */
	public String getExceptionId() {
		return exceptionId;
	}

	/**
	 * @return
	 */
	public String getExceptionInterface() {
		return exceptionInterface;
	}

	/**
	 * @return
	 */
	public String getExceptionRefNum() {
		return exceptionRefNum;
	}

	/**
	 * @return
	 */
	public String getRecordType() {
		return recordType;
	}

	/**
	 * @param string
	 */
	public void setExceptionDate(String string) {
		exceptionDate = string;
	}

	/**
	 * @param string
	 */
	public void setExceptionId(String string) {
		exceptionId = string;
	}

	/**
	 * @param string
	 */
	public void setExceptionInterface(String string) {
		exceptionInterface = string;
	}

	/**
	 * @param string
	 */
	public void setExceptionRefNum(String string) {
		exceptionRefNum = string;
	}

	/**
	 * @param string
	 */
	public void setRecordType(String string) {
		recordType = string;
	}

}
