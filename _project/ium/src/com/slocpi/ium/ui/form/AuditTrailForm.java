/*
 * Created on Feb 24, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.ui.form;


/**
 * @author Cris
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class AuditTrailForm extends AuditTrailFilterForm{
	private String transactionDate = "";
	private String table = "";
	private String transactionType = "";
	private String fromValue = "";
	private String toValue = "";
	private String recordId = "";


	/**
	 * @return
	 */
	public String getFromValue() {
		return fromValue;
	}

	/**
	 * @return
	 */
	public String getTable() {
		return table;
	}

	/**
	 * @return
	 */
	public String getToValue() {
		return toValue;
	}

	/**
	 * @return
	 */
	public String getTransactionDate() {
		return transactionDate;
	}

	/**
	 * @return
	 */
	public String getTransactionType() {
		return transactionType;
	}

	/**
	 * @param string
	 */
	public void setFromValue(String string) {
		fromValue = string;
	}

	/**
	 * @param string
	 */
	public void setTable(String string) {
		table = string;
	}

	/**
	 * @param string
	 */
	public void setToValue(String string) {
		toValue = string;
	}

	/**
	 * @param string
	 */
	public void setTransactionDate(String string) {
		transactionDate = string;
	}

	/**
	 * @param string
	 */
	public void setTransactionType(String string) {
		transactionType = string;
	}


	/**
	 * @return
	 */
	public String getRecordId() {
		return recordId;
	}

	/**
	 * @param string
	 */
	public void setRecordId(String string) {
		recordId = string;
	}

}
