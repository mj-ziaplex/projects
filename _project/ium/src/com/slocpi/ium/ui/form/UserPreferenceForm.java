package com.slocpi.ium.ui.form;

import java.util.Map;
import org.apache.struts.action.ActionForm;

/**
 * This class contains the user preference details
 * @version: 1.0
 */
public class UserPreferenceForm extends ActionForm {
	private String events[]  =  null;
	private Map preferences  = null; 
	/**
	 * @return
	 */
	public String[] getEvents() {
		return events;
	}

	/**
	 * @param strings
	 */
	public void setEvents(String[] events) {
		this.events = events;
	}

	/**
	 * @return
	 */
	public Map getPreferences() {
		return preferences;
	}

	/**
	 * @param map
	 */
	public void setPreferences(Map preferences) {
		this.preferences = preferences;
	}

}

