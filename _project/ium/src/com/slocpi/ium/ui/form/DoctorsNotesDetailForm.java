package com.slocpi.ium.ui.form;

// java package
import java.util.ArrayList;

/**
 * This class contains the kickout reasons, list of requirements, and medical and lab records. 
 * It also includes the requirement details.
 * @author: Ma. Theresa Vicencio
 * @version: 1.0
 */
public class DoctorsNotesDetailForm extends AssessmentRequestForm {

  private String discussion = null;
  private ArrayList discussionList = null;

    
  /**
   * Sets the discussion attribute.
   * @param discussion discussion
   */  
  public void setDiscussion(String discussion) {
	this.discussion = discussion;
  }

  /**
   * Retrieves the discussion attribute.
   * @return String discussion
   */
  public String getDiscussion() {
	return (this.discussion);
  }

  /**
   * Sets the discussion list attribute.
   * @param discussionList discussion list
   */	
  public void setDiscussionList(ArrayList discussionList) {
	this.discussionList = discussionList;
  }

  /**
   * Retrieves the list of discussions.
   * @return ArrayList discussion list
   */
  public ArrayList getDiscussionList(){
	return (this.discussionList);
  }  
    
}

