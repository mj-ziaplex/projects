package com.slocpi.ium.ui.form;

// java package
import java.util.ArrayList;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

/**
 * This class contains the client data sheet request details. It includes the summary of client details, 
 * mortality rating and and policy coverage information.
 * @author: Ma. Theresa Vicencio
 * @version: 1.0
 */
public class CDSDetailForm extends AssessmentRequestForm {

  private ArrayList mortalityRatingList = null;  
  private ArrayList policyCoverageList = null;   
  
  //add uw assessment
  private String idSelected; 
  private ArrayList impairments = null;
  private ArrayList listOfAnalysis = null;
  //add uw assessment
  
  //add requirement KOReqMedLabDetailForm 
  private ArrayList koReasons     = null;
  private ArrayList medLabRecords = null;
  private ArrayList requirements  = null;
  private ArrayList reqLevelList  = null;
  private String[]  reqId         = null;
  private String[]  statusCode    = null;
  private String    refNo         = null;
  private String    statusTo      = null;
  private String[]  indexTemp     = null;
  private String[]  followUpNo    = null;
  private String[]  seq           = null;
  private String[]  followUpDate  = null;
  private String[]  reqLevel      = null;
  private String[]  reqCodes      = null;
  private String[]  dateSent      = null;
  //add requirement
  
  public String toString(){
	  
	  return ToStringBuilder.reflectionToString(this, ToStringStyle.DEFAULT_STYLE);
  }

/**
   * Sets the mortality rating of the insured and/or owner attribute.
   * @param mortalityRatingList mortality rating of the insured and/or owner
   */	
  public void setMortalityRatingList(ArrayList mortalityRatingList) {
	this.mortalityRatingList = mortalityRatingList;
  }

  /**
   * Retrieves the mortality rating of the insured and/or owner list.
   * @return ArrayList mortality rating of the insured and/or owner
   */
  public ArrayList getMortalityRatingList(){
	return (this.mortalityRatingList);
  }  

  /**
   * Sets the policy coverage list attribute.
   * @param policyCoverageList policy coverage list
   */	
  public void setPolicyCoverageList(ArrayList policyCoverageList) {
	this.policyCoverageList = policyCoverageList;
  }

  /**
   * Retrieves the policy coverage list.
   * @return ArrayList policy coverage list
   */
  public ArrayList getPolicyCoverageList(){
	return (this.policyCoverageList);
  }  
  
  //add uw assessment
  /**
   * Sets the impairments attribute.
   * @param list impairments
   */	
  public void setImpairments(ArrayList impairments) {
	this.impairments = impairments;
  }
	
  /**
   * Retrieves the impairments attribute.
   * @return ArrayList impairments
   */
  public ArrayList getImpairments(){
	return (this.impairments);
  }

  /**
   * Sets the id selected attribute.
   * @param idSelected id of the selected impairment
   */	
  public void setIdSelected(String string) {
    idSelected = string;
  }    

  /**
   * Retrieves the id selected attribute.
   * @return String id of the selected impairment
   */
  public String getIdSelected() {
    return idSelected;
  }


/**
 * @return
 */
public ArrayList getListOfAnalysis() {
	return listOfAnalysis;
}

/**
 * @param list
 */
public void setListOfAnalysis(ArrayList list) {
	listOfAnalysis = list;
}
//add uw assessment

//add requirement KOReqMedLabDetailForm
/**
 * Sets the clear case kick-out reasons attribute.
 * @param koReasons clear case kick-out reasons
 */	
public void setKoReasons(ArrayList koReasons) {
  this.koReasons = koReasons;
}
	
/**
 * Retrieves the list of clear case kick-out reasons.
 * @return ArrayList list of clear case kick-out reasons
 */
public ArrayList getKoReasons(){
  return (this.koReasons);
}

/**
 * Sets the medical and lab records attribute.
 * @param medLabRecords medical and lab records
 */	
public void setMedLabRecords(ArrayList medLabRecords) {
	this.medLabRecords = medLabRecords;
}
	
/**
 * Retrieves the list of medical and lab records.
 * @return ArrayList medical and lab records
 */
public ArrayList getMedLabRecords(){
	return (this.medLabRecords);
}

/**
 * Sets the requirements attribute.
 * @param requirements list of requirements
 */	
public void setRequirements(ArrayList requirements) {
	this.requirements = requirements;
}
	
/**
 * Retrieves the list of requirements.
 * @return ArrayList list of requirements
 */
public ArrayList getRequirements(){
	return (this.requirements);
} 



/**
* @return
*/
public String getStatusTo() {
	return statusTo;
}

/**
* @param string
*/
public void setStatusTo(String string) {
	statusTo = string;
}

/**
* @return
*/
public String getRefNo() {
	return refNo;
}

/**
* @return
*/
public String[] getReqId() {
	return reqId;
}

/**
* @param strings
*/
public void setRefNo(String strings) {
	refNo = strings;
}

/**
* @param strings
*/
public void setReqId(String[] strings) {
	reqId = strings;
}

/**
* @return
*/
public String[] getIndexTemp() {
	return indexTemp;
}

/**
* @param strings
*/
public void setIndexTemp(String[] strings) {
	indexTemp = strings;
}

/**
* Returns the statusCode.
* @return String[]
*/
public String[] getStatusCode() {
	return statusCode;
}

/**
* Sets the statusCode.
* @param statusCode The statusCode to set
*/
public void setStatusCode(String[] statusCode) {
	this.statusCode = statusCode;
}

/**
* @return
*/
public String[] getFollowUpNo() {
	return followUpNo;
}

/**
* @param strings
*/
public void setFollowUpNo(String[] strings) {
	followUpNo = strings;
}

/**
* @return
*/
public String[] getFollowUpDate() {
	return followUpDate;
}

/**
* @return
*/
public String[] getSeq() {
	return seq;
}

/**
* @param strings
*/
public void setFollowUpDate(String[] strings) {
	followUpDate = strings;
}

/**
* @param strings
*/
public void setSeq(String[] strings) {
	seq = strings;
}

/**
* @return
*/
public ArrayList getReqLevelList() {
	return reqLevelList;
}

/**
* @param list
*/
public void setReqLevelList(ArrayList list) {
	reqLevelList = list;
}

/**
* @return
*/
public String[] getReqLevel() {
	return reqLevel;
}

/**
* @param strings
*/
public void setReqLevel(String[] strings) {
	reqLevel = strings;
}

/**
* @return
*/
public String[] getReqCodes() {
	return reqCodes;
}

/**
* @param strings
*/
public void setReqCodes(String[] strings) {
	reqCodes = strings;
}

/**
* @return
*/
public String[] getDateSent() {
	return dateSent;
}

/**
* @param strings
*/
public void setDateSent(String[] strings) {
	dateSent = strings;
}
//add requirement
}

