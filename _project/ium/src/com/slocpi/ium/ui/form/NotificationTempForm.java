package com.slocpi.ium.ui.form;

import java.util.Collection;
import org.apache.struts.action.ActionForm;

/**
 * This class contains the dosuments and requirements details.
 * @author: Barry Yu
 * @version: 1.0
 */
public class NotificationTempForm extends ActionForm {
	private String notificationId   		 = "";
	private String notificationDesc 		 = "";
	private String subject          		 = "";
	private String emailContent     		 = "";
	private String roleIL2[] 				 = null;   
	private String roleGL2[] 				 = null;
	private String rolePN2[] 				 = null;
	private String mobileInd        		 = "";
	private String mobileContent    		 = "";
	private Collection notificationTemplates = null;
	private Collection pages        		 = null;
	private String pageNo           		 = "";
	private String notId            		 = "";
	private String actionType                = "";

	/**
	 * @return
	 */
	public String getEmailContent() {
		return this.emailContent;
	}

	/**
	 * @return
	 */
	public String getMobileContent() {
		return this.mobileContent;
	}

	/**
	 * @return
	 */
	public String getMobileInd() {
		return this.mobileInd;
	}

	/**
	 * @return
	 */
	public String getNotificationDesc() {
		return this.notificationDesc;
	}

	/**
	 * @return
	 */
	public String getNotificationId() {
		return this.notificationId;
	}

	/**
	 * @return
	 */
	public Collection getNotificationTemplates() {
		return this.notificationTemplates;
	}

	/**
	 * @return
	 */
	public String getPageNo() {
		return this.pageNo;
	}

	/**
	 * @return
	 */
	public Collection getPages() {
		return this.pages;
	}

	/**
	 * @return
	 */
	public String getSubject() {
		return this.subject;
	}

	/**
	 * @param string
	 */
	public void setEmailContent(String emailContent) {
		this.emailContent = emailContent;
	}

	/**
	 * @param string
	 */
	public void setMobileContent(String mobileContent) {
		this.mobileContent = mobileContent;
	}

	/**
	 * @param string
	 */
	public void setMobileInd(String mobileInd) {
		this.mobileInd = mobileInd;
	}

	/**
	 * @param string
	 */
	public void setNotificationDesc(String notificationDesc) {
		this.notificationDesc = notificationDesc;
	}

	/**
	 * @param string
	 */
	public void setNotificationId(String notificationInd) {
		this.notificationId = notificationInd;
	}

	/**
	 * @param collection
	 */
	public void setNotificationTemplates(Collection notificationTemplates) {
		this.notificationTemplates = notificationTemplates;
	}

	/**
	 * @param string
	 */
	public void setPageNo(String pageNo) {
		this.pageNo = pageNo;
	}

	/**
	 * @param collection
	 */
	public void setPages(Collection pages) {
		this.pages = pages;
	}	

	/**
	 * @param string
	 */
	public void setSubject(String subject) {
		this.subject = subject;
	}

	/**
	 * @return
	 */
	public String getNotId() {
		return this.notId;
	}

	/**
	 * @param string
	 */
	public void setNotId(String notId) {
		this.notId = notId;
	}
		
	/**
	 * @return
	 */
	public String getActionType() {
		return actionType;
	}

	/**
	 * @param string
	 */
	public void setActionType(String actionType) {
		this.actionType = actionType;
	}

	public void reset() {
		this.notificationId   		 = "";
		this.notificationDesc 		 = "";
		this.subject          		 = "";
		this.emailContent     		 = "";		
		this.mobileInd        		 = "";
		this.mobileContent    		 = "";
		this.actionType              = "";
		this.roleIL2     			 = null;
		this.roleGL2 				 = null;
		this.rolePN2				 = null;
	}
	/**
	 * @return
	 */
	public String[] getRoleGL2() {
		return roleGL2;
	}

	/**
	 * @return
	 */
	public String[] getRoleIL2() {
		return roleIL2;
	}

	/**
	 * @return
	 */
	public String[] getRolePN2() {
		return rolePN2;
	}

	/**
	 * @param strings
	 */
	public void setRoleGL2(String[] roleGL2) {
		this.roleGL2 = roleGL2;
	}

	/**
	 * @param strings
	 */
	public void setRoleIL2(String[] roleIL2) {
		this.roleIL2 = roleIL2;
	}

	/**
	 * @param strings
	 */
	public void setRolePN2(String[] rolePN2) {
		this.rolePN2 = rolePN2;
	}

}

