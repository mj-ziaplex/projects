package com.slocpi.ium.ui.form;

// struts package
import org.apache.struts.action.ActionForm;

/**
 * @TODO Class Description UserForm
 * @author: Ma. Theresa Vicencio
 * @version: 1.0
 */
public class UserForm extends ActionForm {

  
  private String userId = "";
  private String acf2id = "";
  private String lastName = "";
  private String firstName = "";
  private String middleName = "";
  private String email = "";
  /*private String password = "";*/
  private String department = "";
  private String section = "";
  private String branch = "";
  private String locked = "";
  private String activated = "";
  private String accessLevel = "";
    
    
    
  /**
    * sets the user id field of the user form class
    * @param userId
    */  
  public void setUserId(String userId) {
    this.userId = userId;
  }

  /**
    * retrieves the user id field of the user form class
    * @return String user id
    */
  public String getUserId() {
	return (this.userId);
  }

  /**
	* sets the acf2 id of the user form class
	* @param acf2id
	*/  
  public void setAcf2id(String acf2id) {
	this.acf2id = acf2id;
  }

  /**
	* retrieves the acf2 id field of the user form class
	* @return String ACF2 Id
	*/
  public String getAcf2id() {
	return (this.acf2id);
  }  

  /**
	* sets the last name field of the user form class
	* @param lastName
	*/  
  public void setLastName(String lastName) {
	this.lastName = lastName;
  }

  /**
	* retrieves the last name field of the user form class
	* @return String last name
	*/
  public String getLastName() {
	return (this.lastName);
  }
  
  /**
	* sets the first name field of the user form class
	* @param firstName
	*/  
  public void setFirstName(String firstName) {
	this.firstName = firstName;
  }

  /**
	* retrieves the first name field of the user form class
	* @return String first name
	*/
  public String getFirstName() {
	return (this.firstName);
  }  

  /**
	* sets the middle name field of the user form class
	* @param middleName
	*/  
  public void setMiddleName(String middleName) {
	this.middleName = middleName;
  }

  /**
	* retrieves the middle name field of the user form class
	* @return String middle name
	*/
  public String getMiddleName() {
	return (this.middleName);
  }  

  /**
	* sets the email field of the user form class
	* @param email
	*/  
  public void setEmail(String email) {
	this.email = email;
  }

  /**
	* retrieves the email field of the user form class
	* @return String email
	*/
  public String getEmail() {
	return (this.email);
  }  

  /**
	* sets the password field of the user form class
	* @param password
	*/  
/*  public void setPassword(String password) {
	this.password = password;
  }

  *//**
	* retrieves the password field of the user form class
	* @return String password
	*//*
  public String getPassword() {
	return (this.password);
  }  
*/
  /**
	* sets the department field of the user form class
	* @param department
	*/  
  public void setDepartment(String department) {
	this.department = department;
  }

  /**
	* retrieves the department field of the user form class
	* @return String department
	*/
  public String getDepartment() {
	return (this.department);
  }
  
  /**
	* sets the section field of the user form class
	* @param section
	*/  
  public void setSection(String section) {
	this.section = section;
  }

  /**
	* retrieves the section field of the user form class
	* @return String section
	*/
  public String getSection() {
	return (this.section);
  }
 
  /**
	* sets the branch field of the user form class
	* @param branch
	*/  
  public void setBranch(String branch) {
	this.branch = branch;
  }

  /**
	* retrieves the branch field of the user form class
	* @return String branch
	*/
  public String getBranch() {
	return (this.branch);
  }

  /**
	* sets the locked field of the user form class
	* @param locked
	*/  
  public void setLocked(String locked) {
	this.locked = locked;
  }

  /**
	* retrieves the locked field of the user form class
	* @return String locked
	*/
  public String getLocked() {
	return (this.locked);
  }

  /**
	* sets the activated field of the user form class
	* @param activated
	*/  
  public void setActivated(String activated) {
	this.activated = activated;
  }

  /**
	* retrieves the activated field of the user form class
	* @return String activated
	*/
  public String getActivated() {
	return (this.activated);
  }

  /**
	* sets the access level field of the user form class
	* @param accessLevel
	*/  
  public void setAccessLevel(String accessLevel) {
	this.accessLevel = accessLevel;
  }

  /**
	* retrieves the access level field of the user form class
	* @return String access level
	*/
  public String getAccessLevel() {
	return (this.accessLevel);
  }

}

