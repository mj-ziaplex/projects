package com.slocpi.ium.ui.form;

// java package
import java.util.ArrayList;

public class CDSDetailUWForm extends AssessmentRequestForm {
	private ArrayList mortalityRatingList = null;  
	private ArrayList policyCoverageList = null;
	
	private String idSelected; 
	private ArrayList impairments = null;
	private ArrayList listOfAnalysis = null;
	
	/**
	   * Sets the mortality rating of the insured and/or owner attribute.
	   * @param mortalityRatingList mortality rating of the insured and/or owner
	   */	
	  public void setMortalityRatingList(ArrayList mortalityRatingList) {
		this.mortalityRatingList = mortalityRatingList;
	  }

	  /**
	   * Retrieves the mortality rating of the insured and/or owner list.
	   * @return ArrayList mortality rating of the insured and/or owner
	   */
	  public ArrayList getMortalityRatingList(){
		return (this.mortalityRatingList);
	  }  

	  /**
	   * Sets the policy coverage list attribute.
	   * @param policyCoverageList policy coverage list
	   */	
	  public void setPolicyCoverageList(ArrayList policyCoverageList) {
		this.policyCoverageList = policyCoverageList;
	  }

	  /**
	   * Retrieves the policy coverage list.
	   * @return ArrayList policy coverage list
	   */
	  public ArrayList getPolicyCoverageList(){
		return (this.policyCoverageList);
	  }  
	  
	  /**
	   * Sets the impairments attribute.
	   * @param list impairments
	   */	
	  public void setImpairments(ArrayList impairments) {
		this.impairments = impairments;
	  }
		
	  /**
	   * Retrieves the impairments attribute.
	   * @return ArrayList impairments
	   */
	  public ArrayList getImpairments(){
		return (this.impairments);
	  }

	  /**
	   * Sets the id selected attribute.
	   * @param idSelected id of the selected impairment
	   */	
	  public void setIdSelected(String string) {
	    idSelected = string;
	  }    

	  /**
	   * Retrieves the id selected attribute.
	   * @return String id of the selected impairment
	   */
	  public String getIdSelected() {
	    return idSelected;
	  }


	/**
	 * @return
	 */
	public ArrayList getListOfAnalysis() {
		return listOfAnalysis;
	}

	/**
	 * @param list
	 */
	public void setListOfAnalysis(ArrayList list) {
		listOfAnalysis = list;
	}
}
