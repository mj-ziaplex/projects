package com.slocpi.ium.ui.form;

import java.util.ArrayList;
import org.apache.struts.action.ActionForm;

/**
 * ExaminerForm.java 
 * Description:
 *
 * @version $Revision: 1.1 $
 * @author $Author: pw12 $ $Date: 2012/08/01 07:07:30 $
 */
public class ExaminerForm extends ActionForm {
	private String status = "";
	private String pageNo = "";
	private String examinerId="";
	private String lastName="";
	private String firstName="";
	private String middleName="";
	private String salutation="";
	private String dateOfBirth="";
	private String sex="";
	private String rank="";
	private String rankEffectiveDate="";
	private String TIN="";
	private String officeNumber="";
	private String faxNumber="";
	private String mobileNumber="";
	private String busAddrLine1="";
	private String busAddrLine2="";
	private String busAddrLine3="";
	private String city="";
	private String province="";
	private String country="";
	private String zipCode="";
	private String mailToBusAddrInd="";
	private String preEmploymentSrvcInd="";
	private String accreditationInd="";
	private String clinicHrsFrom="";
	private String clinicHrsTo="";
	private String examineArea="";
	private String examinePlace="";
	private String[] specializations;
	private ArrayList specializationsList = new ArrayList();
	
		
	/**
	 * Returns the accreditationInd.
	 * @return String
	 */
	public String getAccreditationInd() {
		return accreditationInd;
	}

	/**
	 * Returns the busAddrLine1.
	 * @return String
	 */
	public String getBusAddrLine1() {
		return busAddrLine1;
	}

	/**
	 * Returns the busAddrLine2.
	 * @return String
	 */
	public String getBusAddrLine2() {
		return busAddrLine2;
	}

	/**
	 * Returns the busAddrLine3.
	 * @return String
	 */
	public String getBusAddrLine3() {
		return busAddrLine3;
	}

	/**
	 * Returns the city.
	 * @return String
	 */
	public String getCity() {
		return city;
	}

	/**
	 * Returns the clinicHrsFrom.
	 * @return String
	 */
	public String getClinicHrsFrom() {
		return clinicHrsFrom;
	}

	/**
	 * Returns the clinicHrsTo.
	 * @return String
	 */
	public String getClinicHrsTo() {
		return clinicHrsTo;
	}

	/**
	 * Returns the country.
	 * @return String
	 */
	public String getCountry() {
		return country;
	}

	/**
	 * Returns the dateOfBirth.
	 * @return String
	 */
	public String getDateOfBirth() {
		return dateOfBirth;
	}

	/**
	 * Returns the examineArea.
	 * @return String
	 */
	public String getExamineArea() {
		return examineArea;
	}

	/**
	 * Returns the examinePlace.
	 * @return String
	 */
	public String getExaminePlace() {
		return examinePlace;
	}

	/**
	 * Returns the examinerId.
	 * @return String
	 */
	public String getExaminerId() {
		return examinerId;
	}

	/**
	 * Returns the faxNumber.
	 * @return String
	 */
	public String getFaxNumber() {
		return faxNumber;
	}

	/**
	 * Returns the firstName.
	 * @return String
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * Returns the lastName.
	 * @return String
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * Returns the mailToBusAddrInd.
	 * @return String
	 */
	public String getMailToBusAddrInd() {
		return mailToBusAddrInd;
	}

	/**
	 * Returns the middleName.
	 * @return String
	 */
	public String getMiddleName() {
		return middleName;
	}

	/**
	 * Returns the mobileNumber.
	 * @return String
	 */
	public String getMobileNumber() {
		return mobileNumber;
	}

	/**
	 * Returns the officeNumber.
	 * @return String
	 */
	public String getOfficeNumber() {
		return officeNumber;
	}

	/**
	 * Returns the preEmploymentSrvcInd.
	 * @return String
	 */
	public String getPreEmploymentSrvcInd() {
		return preEmploymentSrvcInd;
	}

	/**
	 * Returns the province.
	 * @return String
	 */
	public String getProvince() {
		return province;
	}

	/**
	 * Returns the rank.
	 * @return String
	 */
	public String getRank() {
		return rank;
	}

	/**
	 * Returns the rankEffectiveDate.
	 * @return String
	 */
	public String getRankEffectiveDate() {
		return rankEffectiveDate;
	}

	/**
	 * Returns the salutation.
	 * @return String
	 */
	public String getSalutation() {
		return salutation;
	}

	/**
	 * Returns the sex.
	 * @return String
	 */
	public String getSex() {
		return sex;
	}

	/**
	 * Returns the specializations.
	 * @return String[]
	 */
	public String[] getSpecializations() {
		return specializations;
	}

	/**
	 * Returns the tIN.
	 * @return String
	 */
	public String getTIN() {
		return TIN;
	}

	/**
	 * Returns the zipCode.
	 * @return String
	 */
	public String getZipCode() {
		return zipCode;
	}

	/**
	 * Sets the accreditationInd.
	 * @param accreditationInd The accreditationInd to set
	 */
	public void setAccreditationInd(String accreditationInd) {
		this.accreditationInd = accreditationInd;
	}

	/**
	 * Sets the busAddrLine1.
	 * @param busAddrLine1 The busAddrLine1 to set
	 */
	public void setBusAddrLine1(String busAddrLine1) {
		this.busAddrLine1 = busAddrLine1;
	}

	/**
	 * Sets the busAddrLine2.
	 * @param busAddrLine2 The busAddrLine2 to set
	 */
	public void setBusAddrLine2(String busAddrLine2) {
		this.busAddrLine2 = busAddrLine2;
	}

	/**
	 * Sets the busAddrLine3.
	 * @param busAddrLine3 The busAddrLine3 to set
	 */
	public void setBusAddrLine3(String busAddrLine3) {
		this.busAddrLine3 = busAddrLine3;
	}

	/**
	 * Sets the city.
	 * @param city The city to set
	 */
	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * Sets the clinicHrsFrom.
	 * @param clinicHrsFrom The clinicHrsFrom to set
	 */
	public void setClinicHrsFrom(String clinicHrsFrom) {
		this.clinicHrsFrom = clinicHrsFrom;
	}

	/**
	 * Sets the clinicHrsTo.
	 * @param clinicHrsTo The clinicHrsTo to set
	 */
	public void setClinicHrsTo(String clinicHrsTo) {
		this.clinicHrsTo = clinicHrsTo;
	}

	/**
	 * Sets the country.
	 * @param country The country to set
	 */
	public void setCountry(String country) {
		this.country = country;
	}

	/**
	 * Sets the dateOfBirth.
	 * @param dateOfBirth The dateOfBirth to set
	 */
	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	/**
	 * Sets the examineArea.
	 * @param examineArea The examineArea to set
	 */
	public void setExamineArea(String examineArea) {
		this.examineArea = examineArea;
	}

	/**
	 * Sets the examinePlace.
	 * @param examinePlace The examinePlace to set
	 */
	public void setExaminePlace(String examinePlace) {
		this.examinePlace = examinePlace;
	}

	/**
	 * Sets the examinerId.
	 * @param examinerId The examinerId to set
	 */
	public void setExaminerId(String examinerId) {
		this.examinerId = examinerId;
	}

	/**
	 * Sets the faxNumber.
	 * @param faxNumber The faxNumber to set
	 */
	public void setFaxNumber(String faxNumber) {
		this.faxNumber = faxNumber;
	}

	/**
	 * Sets the firstName.
	 * @param firstName The firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * Sets the lastName.
	 * @param lastName The lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * Sets the mailToBusAddrInd.
	 * @param mailToBusAddrInd The mailToBusAddrInd to set
	 */
	public void setMailToBusAddrInd(String mailToBusAddrInd) {
		this.mailToBusAddrInd = mailToBusAddrInd;
	}

	/**
	 * Sets the middleName.
	 * @param middleName The middleName to set
	 */
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	/**
	 * Sets the mobileNumber.
	 * @param mobileNumber The mobileNumber to set
	 */
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	/**
	 * Sets the officeNumber.
	 * @param officeNumber The officeNumber to set
	 */
	public void setOfficeNumber(String officeNumber) {
		this.officeNumber = officeNumber;
	}

	/**
	 * Sets the preEmploymentSrvcInd.
	 * @param preEmploymentSrvcInd The preEmploymentSrvcInd to set
	 */
	public void setPreEmploymentSrvcInd(String preEmploymentSrvcInd) {
		this.preEmploymentSrvcInd = preEmploymentSrvcInd;
	}

	/**
	 * Sets the province.
	 * @param province The province to set
	 */
	public void setProvince(String province) {
		this.province = province;
	}

	/**
	 * Sets the rank.
	 * @param rank The rank to set
	 */
	public void setRank(String rank) {
		this.rank = rank;
	}

	/**
	 * Sets the rankEffectiveDate.
	 * @param rankEffectiveDate The rankEffectiveDate to set
	 */
	public void setRankEffectiveDate(String rankEffectiveDate) {
		this.rankEffectiveDate = rankEffectiveDate;
	}

	/**
	 * Sets the salutation.
	 * @param salutation The salutation to set
	 */
	public void setSalutation(String salutation) {
		this.salutation = salutation;
	}

	/**
	 * Sets the sex.
	 * @param sex The sex to set
	 */
	public void setSex(String sex) {
		this.sex = sex;
	}

	/**
	 * Sets the specializations.
	 * @param specializations The specializations to set
	 */
	public void setSpecializations(String[] specializations) {
		this.specializations = specializations;
	}

	/**
	 * Sets the tIN.
	 * @param tIN The tIN to set
	 */
	public void setTIN(String tIN) {
		TIN = tIN;
	}

	/**
	 * Sets the zipCode.
	 * @param zipCode The zipCode to set
	 */
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	/**
	 * Returns the specializationsList.
	 * @return ArrayList
	 */
	public ArrayList getSpecializationsList() {
		return specializationsList;
	}

	/**
	 * Sets the specializationsList.
	 * @param specializationsList The specializationsList to set
	 */
	public void setSpecializationsList(ArrayList specializationsList) {
		this.specializationsList = specializationsList;
	}

	/**
	 * Returns the pageNo.
	 * @return String
	 */
	public String getPageNo() {
		return pageNo;
	}

	/**
	 * Sets the pageNo.
	 * @param pageNo The pageNo to set
	 */
	public void setPageNo(String pageNo) {
		this.pageNo = pageNo;
	}

	
	/**
	 * Returns the status.
	 * @return String
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * Sets the status.
	 * @param status The status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

}
