package com.slocpi.ium.ui.form;

// java package
import java.util.ArrayList;

/**
 * This class contains the list of reference code details.
 * @author: Ma. Theresa Vicencio
 * @version: 1.0
 */
public class ReferenceCodeListForm extends ReferenceCodeForm {

	private ArrayList listDetails = null;
	private ArrayList listHeaders = null;


	/**
	 * Sets the details list attribute.
	 * @param listDetails list of details
	 */
	public void setListDetails(ArrayList listDetails) {
		this.listDetails = listDetails;
	}

	/**
	 * Retrieves the list of reference object details.
	 * @return ArrayList list of details
	 */
	public ArrayList getListDetails(){
		return (this.listDetails);
	}

	/**
	 * Sets the header list attribute.
	 * @param listHeaders list of column headers
	 */	
	public void setListHeaders(ArrayList listHeaders) {
		this.listHeaders = listHeaders;
	}

	/**
	 * Retrieves the list of reference object headers.
	 * @return ArrayList list of column headers
	 */
	public ArrayList getListHeaders(){
		return (this.listHeaders);
	}
	    
}

