package com.slocpi.ium.ui.form;

// struts package
import org.apache.struts.action.ActionForm;

/**
 * @TODO Class Description UserRoleForm
 * @author: Ma. Theresa Vicencio
 * @version: 1.0
 */
public class UserRoleForm extends ActionForm {

  private String userRoleCode = "";
  private String description = "";
  private String departmentCode = "";
  private String sectionCode = "";
    
    
  /**
    * sets the user role code field of the user role form class
    * @param userRoleCode
    */  
  public void setUserRoleCode(String userRoleCode) {
    this.userRoleCode = userRoleCode;
  }

  /**
    * retrieves the user role code field of the user role form class
    * @return String user role code
    */
  public String getUserRoleCode() {
	return (this.userRoleCode);
  }

  /**
	* sets the description id of the user role form class
	* @param description
	*/  
  public void setDescription(String description) {
	this.description = description;
  }

  /**
	* retrieves the description field of the user role form class
	* @return String descirption
	*/
  public String getDescription() {
	return (this.description);
  }  

  /**
	* sets the department code field of the user role form class
	* @param departmentCode
	*/  
  public void setDepartmentCode(String departmentCode) {
	this.departmentCode = departmentCode;
  }

  /**
	* retrieves the department code field of the user role form class
	* @return String department code
	*/
  public String getDepartmentCode() {
	return (this.departmentCode);
  }
  
  /**
	* sets the section code field of the user role form class
	* @param sectionCode
	*/  
  public void setSectionCode(String sectionCode) {
	this.sectionCode = sectionCode;
  }

  /**
	* retrieves the section code field of the user role form class
	* @return String section code
	*/
  public String getSectionCode() {
	return (this.sectionCode);
  }  
    
}

