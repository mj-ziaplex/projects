package com.slocpi.ium.ui.form;


/**
 * This class contains the client mortality rating.
 * @author: Ma. Theresa Vicencio
 * @version: 1.0
 */
public class CDSMortalityRatingForm extends CDSDetailForm {

	private String clientType;
	private String height;
	private String weight;
	private String basic;
	private String CCR;
	private String APDB;
	private String smokingBasic;
	private String smokingCCR;
	private String smokingAPDB;
	private String familyHistoryBasic;
	private String familyHistoryCCR;
	private String familyHistoryAPDB;
	private String totalAssestMortBasic;
	private String totalAssestMortCCR;
	private String totalAssestMortAPDB;


	/**
	 * Retrieves the client type.
	 * @return String client type
	 */
	public String getClientType() {
		return (this.clientType);
	}
	
	
	/**
	 * Sets the client type.
	 * @param clientType
	 */  
	public void setClientType(String clientType) {
	  this.clientType = clientType;
	}	
	
	

/**
 * @return
 */
public String getAPDB() {
	return APDB;
}

/**
 * @return
 */
public String getBasic() {
	return basic;
}

/**
 * @return
 */
public String getCCR() {
	return CCR;
}

/**
 * @return
 */
public String getFamilyHistoryAPDB() {
	return familyHistoryAPDB;
}

/**
 * @return
 */
public String getFamilyHistoryBasic() {
	return familyHistoryBasic;
}

/**
 * @return
 */
public String getFamilyHistoryCCR() {
	return familyHistoryCCR;
}

/**
 * @return
 */
public String getHeight() {
	return height;
}

/**
 * @return
 */
public String getSmokingAPDB() {
	return smokingAPDB;
}

/**
 * @return
 */
public String getSmokingBasic() {
	return smokingBasic;
}

/**
 * @return
 */
public String getSmokingCCR() {
	return smokingCCR;
}

/**
 * @return
 */
public String getTotalAssestMortAPDB() {
	return totalAssestMortAPDB;
}

/**
 * @return
 */
public String getTotalAssestMortBasic() {
	return totalAssestMortBasic;
}

/**
 * @return
 */
public String getTotalAssestMortCCR() {
	return totalAssestMortCCR;
}

/**
 * @return
 */
public String getWeight() {
	return weight;
}

/**
 * @param string
 */
public void setAPDB(String string) {
	APDB = string;
}

/**
 * @param string
 */
public void setBasic(String string) {
	basic = string;
}

/**
 * @param string
 */
public void setCCR(String string) {
	CCR = string;
}

/**
 * @param string
 */
public void setFamilyHistoryAPDB(String string) {
	familyHistoryAPDB = string;
}

/**
 * @param string
 */
public void setFamilyHistoryBasic(String string) {
	familyHistoryBasic = string;
}

/**
 * @param string
 */
public void setFamilyHistoryCCR(String string) {
	familyHistoryCCR = string;
}

/**
 * @param string
 */
public void setHeight(String string) {
	height = string;
}

/**
 * @param string
 */
public void setSmokingAPDB(String string) {
	smokingAPDB = string;
}

/**
 * @param string
 */
public void setSmokingBasic(String string) {
	smokingBasic = string;
}

/**
 * @param string
 */
public void setSmokingCCR(String string) {
	smokingCCR = string;
}

/**
 * @param string
 */
public void setTotalAssestMortAPDB(String string) {
	totalAssestMortAPDB = string;
}

/**
 * @param string
 */
public void setTotalAssestMortBasic(String string) {
	totalAssestMortBasic = string;
}

/**
 * @param string
 */
public void setTotalAssestMortCCR(String string) {
	totalAssestMortCCR = string;
}

/**
 * @param string
 */
public void setWeight(String string) {
	weight = string;
}

}

