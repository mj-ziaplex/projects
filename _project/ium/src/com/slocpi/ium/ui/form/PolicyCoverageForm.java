package com.slocpi.ium.ui.form;


/**
 * This class contains the policy coverage details.
 * @author: Ma. Theresa Vicencio
 * @version: 1.0
 */
public class PolicyCoverageForm extends AssessmentRequestForm {

	private String policyNo = "";
	private String coverageNo = "";
	private String relationship = "";
	private String issueDate = "";
	private String coverageStatus = "";
	private String smokerInd = "";
	private String planCode = "";
	private String medicalInd = "";
	private String faceAmount = "";
	private String decision = "";
	private String ADBFaceAmount = "";
	private String ADMultiplier = "";
	private String WPMultiplier = "";
	private String reinsuredAmount = "";
	private String relCvgMeFct = "";
	private String relCvgFeUpremAmt = "";
	private String relCvgFePermAmt = "";
	private String adbReinsuredAmount = "";
	private String reinstatementDate = "";
	private String reinsuranceType = "";

	public String getAdbReinsuredAmount() {
		return adbReinsuredAmount;
	}

	public void setAdbReinsuredAmount(String adbReinsuredAmount) {
		this.adbReinsuredAmount = adbReinsuredAmount;
	}

	public String getReinstatementDate() {
		return reinstatementDate;
	}

	public void setReinstatementDate(String reinstatementDate) {
		this.reinstatementDate = reinstatementDate;
	}

	public String getReinsuranceType() {
		return reinsuranceType;
	}

	public void setReinsuranceType(String reinsuranceType) {
		this.reinsuranceType = reinsuranceType;
	}

	public String getRelCvgFePermAmt() {
		return relCvgFePermAmt;
	}

	public void setRelCvgFePermAmt(String relCvgFePermAmt) {
		this.relCvgFePermAmt = relCvgFePermAmt;
	}

	public String getRelCvgFeUpremAmt() {
		return relCvgFeUpremAmt;
	}

	public void setRelCvgFeUpremAmt(String relCvgFeUpremAmt) {
		this.relCvgFeUpremAmt = relCvgFeUpremAmt;
	}

	public String getRelCvgMeFct() {
		return relCvgMeFct;
	}

	public void setRelCvgMeFct(String relCvgMeFct) {
		this.relCvgMeFct = relCvgMeFct;
	}

	/**
	 * Sets the policy number attribute.
	 * @param policyNo policy number of life to be insured
	 */  
	public void setPolicyNo(String policyNo) {
		this.policyNo = policyNo;
	}

	/**
	 * Retrieves the policy number attribute.
	 * @return String policy number of life to be insured
	 */
	public String getPolicyNo() {
		return (this.policyNo);
	}








	/**
	 * Sets the coverage number attribute.
	 * @param coverageNo coverage number for which the client has insured/owner relationship
	 */  
	public void setCoverageNo(String coverageNo) {
		this.coverageNo = coverageNo;
	}

	/**
	 * Retrieves the coverage number attribute.
	 * @return String coverage number for which the client has insured/owner relationship
	 */
	public String getCoverageNo() {
		return (this.coverageNo);
	}

	/**
	 * Sets the relationship attribute.
	 * @param relationship client policy relationship
	 */  
	public void setRelationship(String relationship) {
		this.relationship = relationship;
	}

	/**
	 * Retrieves the relationship attribute.
	 * @return String client policy relationship
	 */
	public String getRelationship() {
		return (this.relationship);
	}

	/**
	 * Sets the issue date attribute.
	 * @param issueDate policy issue date
	 */  
	public void setIssueDate(String issueDate) {
		this.issueDate = issueDate;
	}

	/**
	 * Retrieves the relationship attribute.
	 * @return String policy issue date
	 */
	public String getIssueDate() {
		return (this.issueDate);
	}

	/**
	 * Sets the coverage status attribute.
	 * @param coverageStatus current coverage status of the policy
	 */  
	public void setCoverageStatus(String coverageStatus) {
		this.coverageStatus = coverageStatus;
	}

	/**
	 * Retrieves the coverage status attribute.
	 * @return String current coverage status of the policy
	 */
	public String getCoverageStatus() {
		return (this.coverageStatus);
	}

	/**
	 * Sets the smoker indicator attribute.
	 * @param smokerInd smoker code
	 */  
	public void setSmokerInd(String smokerInd) {
		this.smokerInd = smokerInd;
	}

	/**
	 * Retrieves the smoker indicator attribute.
	 * @return String smoker code
	 */
	public String getSmokerInd() {
		return (this.smokerInd);
	}

	/**
	 * Sets the plan code attribute.
	 * @param planCode plan code
	 */  
	public void setPlanCode(String planCode) {
		this.planCode = planCode;
	}

	/**
	 * Retrieves the plan code attribute.
	 * @return String plan code
	 */
	public String getPlanCode() {
		return (this.planCode);
	}

	/**
	 * Sets the medical indicator attribute.
	 * @param medicalInd medical indicator
	 */  
	public void setMedicalInd(String medicalInd) {
		this.medicalInd = medicalInd;
	}

	/**
	 * Retrieves the medical indicator attribute.
	 * @return String medical indicator
	 */
	public String getMedicalInd() {
		return (this.medicalInd);
	}

	/**
	 * Sets the face amount attribute.
	 * @param faceAmount the amount of insurance the applicant has applied for on the coverage
	 */  
	public void setFaceAmount(String faceAmount) {
		this.faceAmount = faceAmount;
	}

	/**
	 * Retrieves the face amount attribute.
	 * @return String the amount of insurance the applicant has applied for on the coverage
	 */
	public String getFaceAmount() {
		return (this.faceAmount);
	}

	/**
	 * Sets the decision attribute.
	 * @param decision decision type made per coverage
	 */  
	public void setDecision(String decision) {
		this.decision = decision;
	}

	/**
	 * Retrieves the decision attribute.
	 * @return String decision type made per coverage
	 */
	public String getDecision() {
		return (this.decision);
	}

	/**
	 * Sets the ADB face amount attribute.
	 * @param ADBFaceAmount embedded ADB face amount
	 */  
	public void setADBFaceAmount(String ADBFaceAmount) {
		this.ADBFaceAmount = ADBFaceAmount;
	}

	/**
	 * Retrieves the ADB face amount attribute.
	 * @return String embedded ADB face amount
	 */
	public String getADBFaceAmount() {
		return (this.ADBFaceAmount);
	} 

	/**
	 * Sets the AD multiplier attribute.
	 * @param ADMultiplier AD multiplier
	 */  
	public void setADMultiplier(String ADMultiplier) {
		this.ADMultiplier = ADMultiplier;
	}

	/**
	 * Retrieves the AD multiplier attribute.
	 * @return String AD multiplier
	 */
	public String getADMultiplier() {
		return (this.ADMultiplier);
	} 

	/**
	 * Sets the WP multiplier attribute.
	 * @param WPMultiplier WP multiplier
	 */  
	public void setWPMultiplier(String WPMultiplier) {
		this.WPMultiplier = WPMultiplier;
	}

	/**
	 * Retrieves the WP multiplier attribute.
	 * @return String WP multiplier
	 */
	public String getWPMultiplier() {
		return (this.WPMultiplier);
	} 

	/**
	 * Sets the reinsured amount attribute.
	 * @param reinsuredAmount reinsured amount
	 */  
	public void setReinsuredAmount(String reinsuredAmount) {
		this.reinsuredAmount = reinsuredAmount;
	}

	/**
	 * Retrieves the reinsured amount attribute.
	 * @return String reinsured amount
	 */
	public String getReinsuredAmount() {
		return (this.reinsuredAmount);
	} 

}

