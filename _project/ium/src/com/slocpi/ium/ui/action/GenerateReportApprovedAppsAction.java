/*
 * Created on Jan 23, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.ui.action;

import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.slocpi.ium.data.ApplicationsApprovedFilterData;
import com.slocpi.ium.data.AssessmentRequestData;
import com.slocpi.ium.data.LOBData;
import com.slocpi.ium.data.SunLifeDeptData;
import com.slocpi.ium.data.UserData;
import com.slocpi.ium.data.UserProfileData;
import com.slocpi.ium.reports.ApplicationsApprovedReport;
import com.slocpi.ium.ui.form.AssessmentRequestForm;
import com.slocpi.ium.ui.form.ReportApprovedAppsForm;
import com.slocpi.ium.ui.util.Pagination;
import com.slocpi.ium.ui.util.StateHandler;
import com.slocpi.ium.util.CodeHelper;


/**
 * @author nic.decapia
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class GenerateReportApprovedAppsAction extends IUMAction {
	private static final Logger LOGGER = LoggerFactory.getLogger(GenerateReportApprovedAppsAction.class);
	/* (non-Javadoc)
	 * @see com.slocpi.ium.ui.action.IUMAction#handleAction(org.apache.struts.action.ActionMapping, org.apache.struts.action.ActionForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	public ActionForward handleAction(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response)
		throws Exception {
		
		LOGGER.info("handleAction start");
		String page = "reportsPage";
		
		try {
			page = "reportsPage";
			
			StateHandler sessionHandler = new StateHandler();
			UserData userData = sessionHandler.getUserData(request);
			UserProfileData userPref = userData.getProfile();
			
			ReportApprovedAppsForm frmReport = (ReportApprovedAppsForm) form;
			if (frmReport == null) {
				ActionErrors errors = new ActionErrors();
				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.system.exception", "Report Parameters are undefined."));
				saveErrors(request, errors);
				page = "errorPage";
			}

			
			ApplicationsApprovedFilterData filter = extractFilter(frmReport);
			
			ApplicationsApprovedReport rpt = new ApplicationsApprovedReport();
			ArrayList rptContent = rpt.generate(filter);
			
			ArrayList content = new ArrayList();
			for (int i=0; i<rptContent.size(); i++) {
				content.add(convertToForm((AssessmentRequestData) rptContent.get(i)));
			}
			
			int recPerPage = (userPref.getRecordsPerView()!=0)?userPref.getRecordsPerView():10;
			Pagination pgn = new Pagination(content,recPerPage);
			frmReport.setPage(pgn.getPage(frmReport.getPageNo()));
			
			request.setAttribute("reportFilter", frmReport);						
						        	
        }
        catch (Exception e) {
        	LOGGER.error(CodeHelper.getStackTrace(e));
			ActionErrors errors = new ActionErrors();
			errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.system.exception", e.getMessage()));
			saveErrors(request, errors);
			page = "errorPage";       	
        } 

		LOGGER.info("handleAction end");
		LOGGER.debug("PAGE FORWARD-->" + page);
		return (mapping.findForward(page));
	}

	/**
	 * @param object
	 * @return
	 */
	private AssessmentRequestForm convertToForm(AssessmentRequestData content)
	{
		LOGGER.info("convertToForm start");
		AssessmentRequestForm formAR = new AssessmentRequestForm();
		formAR.setRefNo(content.getReferenceNumber());
		formAR.setLob(content.getLob().getLOBDesc());
		formAR.setApprovingDept(content.getUpdatedBy().getDeptCode());
		LOGGER.info("convertToForm end");
		return (formAR);
	}

	/**
	 * @param form
	 * @return
	 */
	private ApplicationsApprovedFilterData extractFilter(ReportApprovedAppsForm form)
	{
		LOGGER.info("extractFilter start");
		ApplicationsApprovedFilterData filter = new ApplicationsApprovedFilterData();
		filter.setStartDate(form.getStartDate());
		filter.setEndDate(form.getEndDate());
		
		String approverId = form.getApprover();
		SunLifeDeptData deptFilter = new SunLifeDeptData();
        if (approverId.length() != 0) {
			deptFilter.setDeptId(approverId);		
        }
        else {
			deptFilter.setDeptId(null);
        }
		filter.setApprovingParty(deptFilter);

		String requestorId = form.getRequestor();
		LOBData lobFilter = new LOBData();
		if (requestorId.length() != 0) {
			lobFilter.setLOBCode(requestorId);
		}
		else {
			lobFilter.setLOBCode(null);
		}
		filter.setRequestingParty(lobFilter);
		LOGGER.info("extractFilter end");
		return (filter);
	}

}
