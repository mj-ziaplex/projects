package com.slocpi.ium.ui.action;


import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.slocpi.ium.IUMException;
import com.slocpi.ium.data.DocumentData;
import com.slocpi.ium.data.DocumentTypeData;
import com.slocpi.ium.data.UserData;
import com.slocpi.ium.data.UserProfileData;
import com.slocpi.ium.ui.form.DocumentForm;
import com.slocpi.ium.ui.util.StateHandler;
import com.slocpi.ium.underwriter.Document;
import com.slocpi.ium.underwriter.UnderWriterException;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.DateHelper;


/**
 * @TODO Class Description UpdateDocumentListAction
 * @author: Ma. Theresa Vicencio
 * @version: 1.0
 */
public class UpdateDocumentListAction extends IUMAction {
  
	private static final Logger LOGGER = LoggerFactory.getLogger(UpdateDocumentListAction.class);
  /**
   * @TODO method description for execute
   * @param mapping mapping of request to an instance of this class
   * @param form  object
   * @param request request object
   * @param response response object
   * @exception IOException if an input/output error occurs
   * @exception ServletException if a servlet exception occurs
   * @return ActionForward as defined in the mapping parameter.
   */
  public ActionForward handleAction(ActionMapping mapping, 
							   ActionForm form, 
							   HttpServletRequest request, 
							   HttpServletResponse response)
							   throws Exception {
	  
	  LOGGER.info("handleAction start");
	String page = "";
	try {
		
		StateHandler sh = new StateHandler();
		UserData ud = sh.getUserData(request);
		UserProfileData profile = ud.getProfile();
		String userId = profile.getUserId();
		String pageId = "";
		
		String clientNo = request.getParameter("clientNo");
		DocumentForm documentForm = (DocumentForm)form;
		saveDocumentRecord(documentForm, userId);
		page = "viewAssessmentRequest";
	
	} 
	catch (UnderWriterException e) {
	   LOGGER.error(CodeHelper.getStackTrace(e));
	  ActionErrors errors = new ActionErrors();
	  errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.request.generic", e.getMessage()));
	  saveErrors(request, errors);
	  page = "viewAssessmentRequest";
	}
	catch (IUMException e) {
	  LOGGER.error(CodeHelper.getStackTrace(e));
	  ActionErrors errors = new ActionErrors();
	  errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.system.exception", e.getMessage()));
	  saveErrors(request, errors);
	  page = "errorPage";
	}

	LOGGER.info("handleAction end");
	LOGGER.debug("PAGE FORWARD-->" + page);
	return (mapping.findForward(page));
  }

  private void saveDocumentRecord(DocumentForm form, String userId) throws Exception {
	
	LOGGER.info("saveDocumentRecord start");
    DocumentData data = new DocumentData();
	data.setRefNo(form.getRefNo());
	DocumentTypeData doc = new DocumentTypeData();
	doc.setDocCode(form.getDocCode());	
	doc.setDocDesc(form.getDocDesc());
	data.setDocument(doc);
	data.setDateReceived(DateHelper.sqlDate(DateHelper.parse(form.getDateSubmit(), "ddMMMyyyy")));
	data.setDateRequested(DateHelper.sqlDate(DateHelper.parse(form.getDateRequested(), "ddMMMyyyy")));
	data.setReceivedBy(userId);
	Document document = new Document();
	document.createDocument(data);
	LOGGER.info("saveDocumentRecord end");
  }


    
}

