/*
 * Created on Apr 29, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.ui.action;

import java.sql.Connection;
import java.sql.SQLException;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.slocpi.ium.IUMException;
import com.slocpi.ium.data.AssessmentRequestData;
import com.slocpi.ium.data.ClientData;
import com.slocpi.ium.data.ClientDataSheetData;
import com.slocpi.ium.data.KickOutMessageData;
import com.slocpi.ium.data.LOBData;
import com.slocpi.ium.data.MedicalRecordData;
import com.slocpi.ium.data.PolicyRequirementsData;
import com.slocpi.ium.data.StatusData;
import com.slocpi.ium.data.SunLifeOfficeData;
import com.slocpi.ium.data.TestProfileData;
import com.slocpi.ium.data.UserData;
import com.slocpi.ium.data.UserProfileData;
import com.slocpi.ium.data.dao.RequirementDAO;
import com.slocpi.ium.data.util.DataSourceProxy;
import com.slocpi.ium.ui.form.CDSDetailForm;
import com.slocpi.ium.ui.form.KOReasonsForm;
import com.slocpi.ium.ui.form.MedLabRecordsForm;
import com.slocpi.ium.ui.form.RequirementForm;
import com.slocpi.ium.ui.util.StateHandler;
import com.slocpi.ium.underwriter.AssessmentRequest;
import com.slocpi.ium.underwriter.KickOutMessage;
import com.slocpi.ium.underwriter.PolicyRequirements;
import com.slocpi.ium.underwriter.UnderWriterException;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.DateHelper;
import com.slocpi.ium.util.IUMConstants;
import com.slocpi.ium.util.ValueConverter;

/**
 * @author Cris
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class SendRequirementNotificationAction extends IUMAction {

	private static final Logger LOGGER = LoggerFactory.getLogger(SendRequirementNotificationAction.class);
	
	public ActionForward handleAction(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		
		LOGGER.info("handleAction start");
		String page = "cdsDetailPage";

		String session_id = request.getParameter("session_id").toString();
		request.setAttribute("session_id", session_id);

		CDSDetailForm arForm = (CDSDetailForm) form;
		String referenceNumber = arForm.getRefNo();
		
		UserData userData = new StateHandler().getUserData(request);
		UserProfileData userLogIn = userData.getProfile();
		String user = userLogIn.getUserId();

		PolicyRequirements req = new PolicyRequirements();
		ArrayList listReqIds = req.retrieveAttachmentHelper(referenceNumber);

		if (listReqIds.size() <= 0) {
			LOGGER.debug("There are no requirements for notification");
			constructError("error.submitRequest",
					"There are no requirements for notification", request);
		} else {
			req.sendNotificationOrderReqt(referenceNumber, user);
		}

		HttpSession session = request.getSession();
		if (session.getAttribute("requirementForm") != null)
			session.removeAttribute("requirementForm");
		
		String savedSection = (request.getParameter("savedSectionReq") != null) ? request
				.getParameter("savedSectionReq").trim()
				: "";
		
		try {
			session = request.getSession();
			if (session == null) {
				LOGGER.debug("request.getSession() is null");
			} else {
				LOGGER.debug("request.getSession() is not null");
				session
						.setAttribute("savedSection_" + session_id,
								savedSection);
				String temp = (String) session.getAttribute("savedSection_"
						+ session_id);
				String temp2 = (temp != null) ? temp.trim()
						: "session object savedSection is not saved";
			}
		} catch (Exception e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
		}
		
		arForm = populateRequestDetails(arForm);
		ArrayList reqLevelList = getRequestLevel();
		ArrayList requirements = populateRequirements(referenceNumber);
		ArrayList koReasons = populateKOReasons(referenceNumber);
		/*ArrayList medLabRecords = populateMedLabRecords(referenceNumber);*/
		RequirementForm requirementForm = populateRequirementDetails(
				referenceNumber, Long.parseLong(arForm.getReqId()[0]));

		arForm.setRequirements(requirements);
		arForm.setReqLevelList(reqLevelList);
		arForm.setKoReasons(koReasons);
		/*arForm.setMedLabRecords(medLabRecords);*/
		session.setAttribute("requirementForm", requirementForm);
		session.setAttribute("detailForm", arForm);
		LOGGER.info("handleAction end");
		LOGGER.debug("PAGE FORWARD-->" + page);
		return mapping.findForward(page);
	}

	private String constructError(String key, String parameter,
			HttpServletRequest request) {
		
		LOGGER.info("constructError start");
		String result = "error";
		ActionErrors error = new ActionErrors();
		error.add(ActionErrors.GLOBAL_ERROR, new ActionError(key, parameter));
		saveErrors(request, error);
		LOGGER.info("constructError end");
		return result;
	}

	private ArrayList populateRequirements(String refNo) throws Exception {
		
		LOGGER.info("populateRequirements start");
		PolicyRequirements pr = new PolicyRequirements();
		ArrayList reqList = pr.getRequirements(refNo);

		int size = reqList.size();
		ArrayList requirements = new ArrayList();
		for (int i = 0; i < size; i++) {
			PolicyRequirementsData reqData = (PolicyRequirementsData) reqList
					.get(i);
			RequirementForm reqForm = new RequirementForm();
			reqForm.setRefNo(reqData.getReferenceNumber());
			reqForm.setReqId(String.valueOf(reqData.getRequirementId()));
			reqForm.setSeq(String.valueOf(reqData.getSequenceNumber()));
			reqForm.setReqCode(reqData.getRequirementCode());
			reqForm.setReqLevel(reqData.getLevel());
			StatusData stat = reqData.getStatus();
			reqForm.setReqStatusCode(String.valueOf(stat.getStatusId()));
			reqForm.setReqStatusDesc(stat.getStatusDesc());
			reqForm.setStatusDate((DateHelper.format(reqData.getStatusDate(),
					"ddMMMyyyy")).toUpperCase());
			reqForm.setComment(reqData.getComments());
			reqForm.setClientType(reqData.getClientType());
			reqForm.setFollowUpDate((DateHelper.format(reqData
					.getFollowUpDate(), "ddMMMyyyy")).toUpperCase());
			reqForm.setFollowUpNo(Long.toString(reqData.getFollowUpNumber()));
			reqForm.setDateSent((DateHelper.format(reqData.getDateSent(),
					"ddMMMyyyy")).toUpperCase());
			reqForm.setClientNo(reqData.getClientId());
			reqForm.setTestDate((DateHelper.format(reqData.getTestDate(),
					"ddMMMyyyy")).toUpperCase());
			requirements.add(reqForm);

			reqForm.setImageReferenceNum(reqData.getImageRef());

			String fnURL = getWMSImageLink(reqData);
			reqForm.setWmsImageLink(fnURL);
		}

		LOGGER.info("populateRequirements end");
		return (requirements);
	}

	private CDSDetailForm populateRequestDetails(CDSDetailForm reqForm)
			throws UnderWriterException, IUMException {
		
		LOGGER.info("populateRequestDetails start");
		AssessmentRequest assessmentRequest = new AssessmentRequest();
		AssessmentRequestData requestData = assessmentRequest
				.getDetails(reqForm.getRefNo());

		LOBData lob = requestData.getLob();
		ClientData insured = requestData.getInsured();
		ClientData owner = requestData.getOwner();
		SunLifeOfficeData branch = requestData.getBranch();
		UserProfileData agent = requestData.getAgent();
		UserProfileData assignedTo = requestData.getAssignedTo();
		UserProfileData underwriter = requestData.getUnderwriter();
		UserProfileData location = requestData.getFolderLocation();
		ClientDataSheetData cds = requestData.getClientDataSheet();
		StatusData statusData = requestData.getStatus();

		NumberFormat nf = NumberFormat.getInstance();
		nf.setMinimumFractionDigits(2);
		nf.setMaximumFractionDigits(2);

		reqForm.setTransmitIndicator(requestData.getTransmitIndicator());
		reqForm.setRefNo(reqForm.getRefNo());
		reqForm.setLob(lob.getLOBCode());
		reqForm.setSourceSystem(requestData.getSourceSystem());
		reqForm.setAmountCovered(nf.format(requestData.getAmountCovered()));
		reqForm.setPremium(nf.format(requestData.getPremium()));
		reqForm.setInsuredName(insured.getGivenName() + " "
				+ insured.getLastName());
		reqForm.setBranchCode(branch.getOfficeId());
		reqForm.setBranchName(branch.getOfficeName());
		reqForm.setAgentCode(agent.getUserId());
		reqForm.setAgentName(agent.getFirstName() + " " + agent.getLastName());
		reqForm.setRequestStatus(String.valueOf(statusData.getStatusId()));
		reqForm.setAssignedTo(assignedTo.getUserId());
		reqForm.setUnderwriter(underwriter.getUserId());
		reqForm.setLocation(location.getUserId());

		reqForm.setRemarks(requestData.getRemarks());
		reqForm.setReceivedDate((DateHelper.format(requestData
				.getApplicationReceivedDate(), "ddMMMyyyy")).toUpperCase());
		reqForm.setDateForwarded((DateHelper.format(requestData
				.getForwardedDate(), "ddMMMyyyy")).toUpperCase());
		reqForm.setDateEdited((DateHelper.format(requestData.getUpdatedDate(),
				"ddMMMyyyy")).toUpperCase());
		reqForm.setDateCreated((DateHelper.format(requestData.getCreatedDate(),
				"ddMMMyyyy")).toUpperCase());
		reqForm.setStatusDate((DateHelper.format(requestData.getStatusDate(),
				"ddMMMyyyy")).toUpperCase());

		reqForm.setInsuredClientType(cds.getClientType());
		reqForm.setInsuredClientNo(insured.getClientId());
		reqForm.setInsuredLastName(insured.getLastName());
		reqForm.setInsuredFirstName(insured.getGivenName());
		reqForm.setInsuredMiddleName(insured.getMiddleName());
		reqForm.setInsuredTitle(insured.getTitle());
		reqForm.setInsuredSuffix(insured.getSuffix());
		reqForm.setInsuredAge(String.valueOf(insured.getAge()));
		reqForm.setInsuredSex(insured.getSex());
		reqForm.setInsuredBirthDate((DateHelper.format(insured.getBirthDate(),
				"ddMMMyyyy")).toUpperCase());

		reqForm.setOwnerClientType(cds.getClientType());
		reqForm.setOwnerClientNo(owner.getClientId());
		reqForm.setOwnerLastName(owner.getLastName());
		reqForm.setOwnerFirstName(owner.getGivenName());
		reqForm.setOwnerMiddleName(owner.getMiddleName());
		reqForm.setOwnerTitle(owner.getTitle());
		reqForm.setOwnerSuffix(owner.getSuffix());
		reqForm.setOwnerAge(String.valueOf(owner.getAge()));
		reqForm.setOwnerSex(owner.getSex());
		reqForm.setOwnerBirthDate((DateHelper.format(owner.getBirthDate(),
				"ddMMMyyyy")).toUpperCase());

		LOGGER.info("populateRequestDetails end");
		return (reqForm);
	}

	private ArrayList getRequestLevel() throws SQLException, IUMException {
		
		LOGGER.info("getRequestLevel start");
		
		try {
			RequirementDAO dao = new RequirementDAO();
			ArrayList list = new ArrayList();
			list = dao.retrieveRequirements();
			if (list != null){
				LOGGER.debug("size = " + list.size());
			}else{
				LOGGER.debug("list is null");
			}
			
			LOGGER.info("getRequestLevel end");
			return list;
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMException(e.getMessage());
		} 
	}

	private ArrayList populateKOReasons(String refNo)
			throws UnderWriterException, IUMException {
		
		LOGGER.info("populateKOReasons start");
		KickOutMessage ko = new KickOutMessage();
		ArrayList reqList = ko.getMessages(refNo);

		int size = reqList.size();
		ArrayList koReasons = new ArrayList();
		for (int i = 0; i < size; i++) {
			KickOutMessageData koData = (KickOutMessageData) reqList.get(i);
			KOReasonsForm koForm = new KOReasonsForm();
			koForm.setSeq(String.valueOf(koData.getSequenceNumber()));
			koForm.setMessageText(koData.getMessageText());
			koForm.setClientNo(koData.getClientId());
			koForm.setFailResponse(koData.getFailResponse());
			koReasons.add(koForm);
		}

		LOGGER.info("populateKOReasons end");
		return (koReasons);
	}

	/*private ArrayList populateMedLabRecords(String refNo)
			throws UnderWriterException, IUMException {
		
		LOGGER.info("populateMedLabRecords start");
		AssessmentRequest ar = new AssessmentRequest();
		ArrayList medReqList = ar.getKOMedRecs(refNo);

		int size = medReqList.size();
		ArrayList medicalRecs = new ArrayList();

		for (int i = 0; i < size; i++) {
			MedicalRecordData medData = (MedicalRecordData) medReqList.get(i);
			MedLabRecordsForm medRecForm = new MedLabRecordsForm();
			TestProfileData testData = medData.getTest();
			medRecForm.setCode(String.valueOf(testData.getTestId()));
			medRecForm.setTestDescription(testData.getTestDesc());

			String type = "";
			String labTestInd = medData.getLabTestInd();
			if (labTestInd != null) {
				if (labTestInd.equals("Y")) {
					type = "LAB";
				} else if (labTestInd.equals("Y")) {
					type = "MED";
				}
			}

			medRecForm.setType(type);
			medRecForm.setDateConducted((DateHelper.format(medData
					.getConductedDate(), "ddMMMyyyy")).toUpperCase());
			medRecForm.setDateReceived((DateHelper.format(medData
					.getReceivedDate(), "ddMMMyyyy")).toUpperCase());
			medicalRecs.add(medRecForm);
		}
		LOGGER.info("populateMedLabRecords end");
		return (medicalRecs);
	}*/

	private RequirementForm populateRequirementDetails(String refNo, long reqId)
			throws Exception {
	
		LOGGER.info("populateRequirementDetails start");
		PolicyRequirements pr = new PolicyRequirements();

		PolicyRequirementsData reqData = pr.getRequirement(reqId);

		RequirementForm reqForm = new RequirementForm();
		if (reqData != null) {
			reqForm.setRefNo(reqData.getReferenceNumber());
			reqForm.setReqId(Long.toString(reqId));
			reqForm.setReqCode(reqData.getRequirementCode());
			reqForm.setSeq(String.valueOf(reqData.getSequenceNumber()));
			reqForm.setReqDesc(reqData.getReqDesc());
			reqForm.setReqLevel(reqData.getLevel());
			reqForm.setCreatedDate((DateHelper.format(reqData.getCreateDate(),
					"ddMMMyyyy")).toUpperCase());
			reqForm.setCompleteReq(String.valueOf(reqData
					.isCompletedRequirementInd()));
			
			if (reqData.getStatus() != null) {
				reqForm.setReqStatusCode(String.valueOf(reqData.getStatus()
						.getStatusDesc()));
				reqForm.setReqStatusValue(String.valueOf(reqData.getStatus()
						.getStatusId()));
			}
			reqForm.setStatusDate((DateHelper.format(reqData.getStatusDate(),
					"ddMMMyyyy")).toUpperCase());
			reqForm.setUpdatedBy(reqData.getUpdatedBy());
			reqForm.setUpdatedDate((DateHelper.format(reqData.getUpdateDate(),
					"ddMMMyyyy")).toUpperCase());
			reqForm.setClientType(reqData.getClientType());
			reqForm.setDesignation(reqForm.getDesignation());
			reqForm.setTestDate((DateHelper.format(reqData.getTestDate(),
					"ddMMMyyyy")).toUpperCase());
			reqForm.setTestResult(reqData.getTestResultCode());
			reqForm.setTestDesc(reqData.getTestResultDesc());
			reqForm.setResolve(String.valueOf(reqData.isResolveInd()));
			
			reqForm.setFollowUpNo(String.valueOf(reqData.getFollowUpNumber()));
			reqForm.setFollowUpDate((DateHelper.format(reqData
					.getFollowUpDate(), "ddMMMyyyy")).toUpperCase());
			reqForm.setValidityDate((DateHelper.format(reqData
					.getValidityDate(), "ddMMMyyyy")).toUpperCase());
			reqForm.setPaidInd(String.valueOf(reqData.isPaidInd()));
			reqForm.setNewTest(ValueConverter.booleanToString(reqData
					.getNewTestOnly()));
			reqForm.setOrderDate((DateHelper.format(reqData.getOrderDate(),
					"ddMMMyyyy")).toUpperCase());
			reqForm.setDateSent((DateHelper.format(reqData.getDateSent(),
					"ddMMMyyyy")).toUpperCase());
			reqForm.setCcasSuggest(ValueConverter.booleanToString(reqData
					.getCCASuggestInd()));
			reqForm.setReceivedDate((DateHelper.format(
					reqData.getReceiveDate(), "ddMMMyyyy")).toUpperCase());
			reqForm.setAutoOrder(ValueConverter.booleanToString(reqData
					.getAutoOrderInd()));
			reqForm.setFldComm(ValueConverter.booleanToString(reqData
					.getFldCommentInd()));
			reqForm.setComment(reqData.getComments());
			reqForm.setClientNo(reqData.getClientId());

			reqForm.setImageReferenceNum(reqData.getImageRef());

			String fnURL = getWMSImageLink(reqData);
			reqForm.setWmsImageLink(fnURL);
		}
		LOGGER.info("populateRequirementDetails end");
		return (reqForm);
	}

	private String getWMSImageLink(PolicyRequirementsData reqData) {
	
		
		ResourceBundle rb = ResourceBundle
				.getBundle(IUMConstants.IUM_WMS_CONFIG);
		String WMSServer = rb.getString("WMS_Server");
		String paramObjectStoreName = rb.getString("param_Object_Store_Name");
		String fnURL = null;
		int imageRef = 0;
		if (reqData.getImageRef() != null) {
			try {
				imageRef = Integer.parseInt(reqData.getImageRef().trim());
				if (imageRef != 0) {
					fnURL = WMSServer + "/getContent?id="
							+ reqData.getImageRef() + "&objectStoreName="
							+ paramObjectStoreName + "&objectType=document";
				}
			} catch (NumberFormatException e) {
				LOGGER.error(CodeHelper.getStackTrace(e));
				if (!reqData.getImageRef().trim().equals("Y")
						|| !reqData.getImageRef().trim().equals("y")) {
					fnURL = WMSServer + "/getContent?id="
							+ reqData.getImageRef() + "&objectStoreName="
							+ paramObjectStoreName + "&objectType=document";
				}
			}
		}
		
		return fnURL;
	}

}
