package com.slocpi.ium.ui.action;

// struts package
import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.slocpi.ium.data.RolesData;
import com.slocpi.ium.data.UserAccessData;
import com.slocpi.ium.data.UserData;
import com.slocpi.ium.data.UserProfileData;
import com.slocpi.ium.ui.form.UserProfileForm;
import com.slocpi.ium.ui.form.UserRoleForm;
import com.slocpi.ium.ui.util.StateHandler;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.UserManager;


/**
 * @TODO Class Description ViewUserProfileAction
 * @author: Ingrid Villanueva
 * @version: 1.0
 */
public class ViewUserProfileAction extends IUMAction {
	private static final Logger LOGGER = LoggerFactory.getLogger(ViewUserProfileAction.class);
  /**
   * This handles the processes of the ViewUserProfileAction class. It uses the extractAccessPriveleges and retrieveUserProfielRecord methods
   * to display the details of the profile of the user.  
   * @param mapping mapping of request to an instance of this class
   * @param form  object
   * @param request request object
   * @param response response object
   * @exception IOException if an input/output error occurs
   * @exception ServletException if a servlet exception occurs
   * @return ActionForward as defined in the mapping parameter.
   */
  public ActionForward handleAction(ActionMapping mapping, 
							   ActionForm form, 
							   HttpServletRequest request, 
							   HttpServletResponse response)
							   throws Exception {
	 
	LOGGER.info("handleAction start");
	String page = "";
	try {
		
		StateHandler sh = new StateHandler();
		UserData ud = sh.getUserData(request);
		UserProfileData profile = ud.getProfile();
		String userId = profile.getUserId();
		String pageId = "";
		UserAccessData uad = extractAccessPriveleges(userId, pageId);
		
		if (uad.getAccessCode().equals("W")) {
			String userCode = request.getParameter("userId");
			if(userCode==null){
				userCode = ((UserProfileForm)form).getUserId();				
			}

			UserProfileForm userForm = retrieveUserProfileRecord(form, userCode);
			request.setAttribute("userProfileForm", userForm);
			page = "userProfileDetailPage";
		}
		else {

			page = "errorPage";
		}
	}// end-try 
	catch (Exception e) {
		LOGGER.error(CodeHelper.getStackTrace(e));
	  ActionErrors errors = new ActionErrors();
	  errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.system.exception", e.getMessage()));
	  saveErrors(request, errors); 
	  page = "errorPage"; 
	}
	
	LOGGER.info("handleAction end");
	LOGGER.debug("PAGE FORWARD-->" + page);
	return (mapping.findForward(page));
  }// execute


  private UserProfileForm retrieveUserProfileRecord(ActionForm form, String userId) throws Exception {
    
	LOGGER.info("retrieveUserProfileRecord start");
    UserProfileForm upForm = (UserProfileForm) form;
    UserData userData = new UserData();
    
    UserManager usrmgr = new UserManager();
	userData = usrmgr.retrieveUserDetails(upForm.getUserId());
	
	
    UserProfileData data = userData.getProfile();    
    upForm.setAccessLevel(data.getRole());
    upForm.setAcf2Id(data.getACF2ID());
    upForm.setBranchName(data.getOfficeCode());
    upForm.setContactNo(data.getContactNum());
    upForm.setMobileNo(data.getMobileNumber());
    upForm.setDepartment(data.getDeptCode());
    upForm.setEmailAddr(data.getEmailAddr());
    upForm.setFirstName(data.getFirstName());
    upForm.setLastName(data.getLastName());
    upForm.setMiddleName(data.getMiddleName());
    upForm.setUserCode(data.getUserCode());
    upForm.setUserType(data.getUserType());
    upForm.setSex(data.getSex());
    upForm.setSection(data.getSectionCode());
    upForm.setPassword(data.getPassword());
    upForm.setConfirmPassword(data.getPassword());
    upForm.setAddr1(data.getAddress1());
    upForm.setAddr2(data.getAddress2());
    upForm.setAddr3(data.getAddress3());
    upForm.setCountry(data.getCountry());
    upForm.setCity(data.getCity());
    upForm.setZipCode(data.getZipCode());
    upForm.setDefaultRole(data.getRole());


    if (data.isLock()==true) {
    	upForm.setLocked("Y");
    }
    else {
		upForm.setLocked("N");    	
    }
    if (data.isActive()==true) {
    	upForm.setActivated("Y");
    }
    else {
    	upForm.setActivated("N");
    }

	ArrayList roles = userData.getRoles();
	ArrayList frmRoles = new ArrayList();
	for(int i=0; i<roles.size(); i++){
		RolesData rdata = (RolesData)roles.get(i);
		UserRoleForm usrfrm = new UserRoleForm();
		usrfrm.setDescription(rdata.getRolesDesc());
		usrfrm.setUserRoleCode(rdata.getRolesId());
		frmRoles.add(usrfrm);
	}
	upForm.setRoles(frmRoles);

	LOGGER.info("retrieveUserProfileRecord end");
    return upForm;
    
  }

    
}

