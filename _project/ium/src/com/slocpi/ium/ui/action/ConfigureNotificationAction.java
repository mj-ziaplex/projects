package com.slocpi.ium.ui.action;

// struts package
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.slocpi.ium.data.NotificationTemplateData;
import com.slocpi.ium.data.UserData;
import com.slocpi.ium.data.UserProfileData;
import com.slocpi.ium.data.dao.NotificationTemplateCHDao;
import com.slocpi.ium.data.util.DataSourceProxy;
import com.slocpi.ium.ui.form.NotificationTempForm;
import com.slocpi.ium.ui.util.IUMWebException;
import com.slocpi.ium.ui.util.Page;
import com.slocpi.ium.ui.util.Pagination;
import com.slocpi.ium.ui.util.StateHandler;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.IUMConstants;



public class ConfigureNotificationAction extends IUMAction {
  
	private static final Logger LOGGER = LoggerFactory.getLogger(ConfigureNotificationAction.class);
  public ActionForward handleAction(ActionMapping mapping, 
							   ActionForm form, 
							   HttpServletRequest request, 
							   HttpServletResponse response)
							   throws Exception {
	
	LOGGER.info("handleAction start");
	String page = "refresh1";			
	String actionType = request.getParameter("actionType");
	NotificationTempForm notificationTempForm = (NotificationTempForm) form;		
	
	if (actionType != null && !"".equals(actionType)) {
		if (actionType.equals("refresh")) {						
				request.setAttribute("notificationTempForm",notificationTempForm);
		} else if (actionType.equals("maintain")) {	
			notificationTempForm = maintainNotification(notificationTempForm);	
			notificationTempForm = fillContent(notificationTempForm,request);		
			request.setAttribute("notificationTempForm",notificationTempForm);
		} else if (actionType.equals("create")) {	
			createNotificationTemplate(request,notificationTempForm);				
			notificationTempForm = fillContent(notificationTempForm,request);
			notificationTempForm.reset();				
			request.setAttribute("notificationTempForm",notificationTempForm);
		} else if (actionType.equals("update")) {	
			updateNotificationTemplate(request,notificationTempForm);	
			notificationTempForm = fillContent(notificationTempForm,request);			
			notificationTempForm.reset();			
			request.setAttribute("notificationTempForm",notificationTempForm);
		}
		
	}else {				
		notificationTempForm = fillContent(notificationTempForm,request);
		notificationTempForm.reset();
		request.setAttribute("notificationTempForm",notificationTempForm);
	}	
	LOGGER.info("handleAction end");
	LOGGER.debug("PAGE FORWARD-->" + page);
	return (mapping.findForward(page));
  }
	
		
	
	
	/**
	 * @param request
	 * @param notificationTempForm
	 * @return
	 */
	private void updateNotificationTemplate(HttpServletRequest request, NotificationTempForm notificationTempForm) throws SQLException {
		
		LOGGER.info("updateNotificationTemplate start");
		NotificationTemplateData notTempData = new NotificationTemplateData();
		notTempData.setNotificationDesc(notificationTempForm.getNotificationDesc());
		notTempData.setSubject(notificationTempForm.getSubject());
		notTempData.setEmailNotificationContent(notificationTempForm.getEmailContent());
		boolean withSMS = IUMConstants.YES.equals(notificationTempForm.getMobileInd());				
		if (withSMS) {
			notTempData.setNotifyMobile(true);
			notTempData.setMobileNotificationContent(notificationTempForm.getMobileContent());
		}else {
			notTempData.setNotifyMobile(false);
		}
		StateHandler sh = new StateHandler();		
		UserData ud = sh.getUserData(request);		
		UserProfileData profile = ud.getProfile();
		String userId = profile.getUserId();
		notTempData.setUpdatedBy(userId);
		notTempData.setUpdateDate(new Date());
		notTempData.setNotificationId(Long.parseLong(notificationTempForm.getNotificationId()));
		Connection conn=null;
		try{
			conn = new DataSourceProxy().getConnection();
			NotificationTemplateCHDao notTempDAO = new NotificationTemplateCHDao(conn);
			conn.setAutoCommit(false);
			notTempDAO.updateNotificationTemplate(notTempData);		
			notTempDAO.updateNotificationRecipients(notificationTempForm.getRoleIL2(),Long.parseLong(notificationTempForm.getNotificationId()),withSMS,IUMConstants.LOB_INDIVIDUAL_LIFE);		
			notTempDAO.updateNotificationRecipients(notificationTempForm.getRoleGL2(),Long.parseLong(notificationTempForm.getNotificationId()),withSMS,IUMConstants.LOB_GROUP_LIFE);		
			notTempDAO.updateNotificationRecipients(notificationTempForm.getRolePN2(),Long.parseLong(notificationTempForm.getNotificationId()),withSMS,IUMConstants.LOB_PRE_NEED);
			conn.commit();
		}catch (SQLException e){
			LOGGER.error(CodeHelper.getStackTrace(e));
			if (conn != null) {
				conn.rollback();
			}
		} finally {
			closeConnection(conn);
		}		
		LOGGER.info("updateNotificationTemplate end");
	}




/**
	 * @param notificationTempForm
	 * @return
	 */
	private NotificationTempForm maintainNotification(NotificationTempForm notificationTempForm) throws SQLException {
		
		LOGGER.info("maintainNotification start");
		Connection conn=null;
		try{
			conn = new DataSourceProxy().getConnection();
			NotificationTemplateCHDao notDAO =  new NotificationTemplateCHDao(conn);
			NotificationTemplateData notData = notDAO.getNotificationTemplate(Long.parseLong(notificationTempForm.getNotificationId()));		
			notificationTempForm.setNotificationDesc(notData.getNotificationDesc());
			notificationTempForm.setSubject(notData.getSubject());
			notificationTempForm.setEmailContent(notData.getEmailNotificationContent());
			notificationTempForm.setMobileInd(notData.isNotifyMobile()?IUMConstants.YES:IUMConstants.NO); 
			notificationTempForm.setMobileContent(notData.getMobileNotificationContent());		
			notificationTempForm.setRoleIL2(notDAO.getRoles(Long.parseLong(notificationTempForm.getNotId()),IUMConstants.LOB_INDIVIDUAL_LIFE));
			notificationTempForm.setRoleGL2(notDAO.getRoles(Long.parseLong(notificationTempForm.getNotId()),IUMConstants.LOB_GROUP_LIFE));
			notificationTempForm.setRolePN2(notDAO.getRoles(Long.parseLong(notificationTempForm.getNotId()),IUMConstants.LOB_PRE_NEED));
		} finally {
			closeConnection(conn);
		}		
		LOGGER.info("maintainNotification end");
		return notificationTempForm;
	}




/**
	 * @param request
	 * @param notificationTempForm
	 * @return
	 */
	private void createNotificationTemplate(HttpServletRequest request, NotificationTempForm notificationTempForm) throws SQLException {
		
		LOGGER.info("createNotificationTemplate start");
		NotificationTemplateData notTempData = new NotificationTemplateData();
		notTempData.setNotificationDesc(notificationTempForm.getNotificationDesc());
		notTempData.setSubject(notificationTempForm.getSubject());
		notTempData.setEmailNotificationContent(notificationTempForm.getEmailContent());
		boolean withSMS = IUMConstants.YES.equals(notificationTempForm.getMobileInd());
		if (withSMS) {
			notTempData.setNotifyMobile(true);
			notTempData.setMobileNotificationContent(notificationTempForm.getMobileContent());
		}else {
			notTempData.setNotifyMobile(false);
		}
		StateHandler sh = new StateHandler();		
		UserData ud = sh.getUserData(request);		
		UserProfileData profile = ud.getProfile();
		String userId = profile.getUserId();
		notTempData.setCreatedBy(userId);
		notTempData.setCreationDate(new Date());
		Connection conn=null;
		try{
			conn = new DataSourceProxy().getConnection();
			NotificationTemplateCHDao notTempDAO = new NotificationTemplateCHDao(conn);
			notTempDAO.insertNotificationTemplate(notTempData);		
		
			notTempDAO.insertNotificationRecipients(notificationTempForm.getRoleIL2(),withSMS,IUMConstants.LOB_INDIVIDUAL_LIFE);			
			notTempDAO.insertNotificationRecipients(notificationTempForm.getRoleGL2(),withSMS,IUMConstants.LOB_GROUP_LIFE);		
			notTempDAO.insertNotificationRecipients(notificationTempForm.getRolePN2(),withSMS,IUMConstants.LOB_PRE_NEED);
		}finally {
			closeConnection(conn);
		}		
		LOGGER.info("createNotificationTemplate end");
	}




	/**
	 * @param notificationTempForm
	 * @return
	 */
	private NotificationTempForm fillContent(NotificationTempForm notificationTempForm, HttpServletRequest request) throws SQLException, IUMWebException {
		
		LOGGER.info("fillContent start");
		Collection notificationTemplates = null;
		Connection conn=null;
		try{
			conn = new DataSourceProxy().getConnection();

			NotificationTemplateCHDao notificationDAO = new NotificationTemplateCHDao(conn);
			notificationTemplates = notificationDAO.getCodeValues();
		} finally{
			closeConnection(conn);
		}
		int pageNo = 1;
		String reqPageNum = notificationTempForm.getPageNo();		
		if ((reqPageNum != null) && !(reqPageNum.equals(""))) {
			try {
				pageNo = Integer.parseInt(reqPageNum);
			}
			catch (NumberFormatException e) {
				//pageNo will be the first page   
			}
		}
		StateHandler sh = new StateHandler();		
		UserData ud = sh.getUserData(request);		
		UserProfileData profile = ud.getProfile();
		int recPerPage = profile.getRecordsPerView();
		if (recPerPage == 0) {
			recPerPage = 1;
		}
		Pagination pgn = new Pagination((ArrayList)notificationTemplates,recPerPage);
		Page pg = pgn.getPage(pageNo);
		notificationTempForm.setPages(pg.getPageNumbers());		
		notificationTempForm.setNotificationTemplates(pg.getList());
		notificationTempForm.setPageNo(String.valueOf(pageNo));
		LOGGER.info("fillContent end");
		return notificationTempForm;
	}


	private void closeConnection(Connection conn)throws SQLException{
		
		
	  if(conn!=null){
		  conn.close();
	  } 
	  
	}

    
}

