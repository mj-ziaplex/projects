package com.slocpi.ium.ui.action;

// struts package
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.slocpi.ium.IUMException;
import com.slocpi.ium.data.UserData;
import com.slocpi.ium.data.UserProfileData;
import com.slocpi.ium.data.dao.UserDAO;
import com.slocpi.ium.data.util.DataSourceProxy;
import com.slocpi.ium.ui.form.UserProfileListForm;
import com.slocpi.ium.ui.util.Page;
import com.slocpi.ium.ui.util.Pagination;
import com.slocpi.ium.ui.util.StateHandler;
import com.slocpi.ium.util.CodeHelper;


/**
 * @TODO Class Description ViewMedicalRecordListAction
 * @author: Ingrid Villanueva
 * @version: 1.0
 */
public class ListUserProfileAction extends IUMAction {
	private static final Logger LOGGER = LoggerFactory.getLogger(ListUserProfileAction.class);
  /**
   * @TODO method description for execute
   * @param mapping mapping of request to an instance of this class
   * @param form  object
   * @param request request object
   * @param response response object
   * @exception IOException if an input/output error occurs
   * @exception ServletException if a servlet exception occurs
   * @return ActionForward as defined in the mapping parameter.
   */
  public ActionForward handleAction(ActionMapping mapping, 
                                    ActionForm form, 
                                    HttpServletRequest request, 
                                    HttpServletResponse response) throws Exception {
	
	LOGGER.info("handleAction start");
	String page = "";

	try {
		
		UserProfileListForm userform = (UserProfileListForm) form;
				
        UserProfileData user = searchUserProfileRecord(userform);				        
      
		UserDAO dao = new UserDAO();		
		ArrayList a = new ArrayList();
		a = dao.selectUserProfiles(user);
		
		StateHandler sh = new StateHandler();
		UserData ud = sh.getUserData(request);
		UserProfileData upd = ud.getProfile();    
		int numPages = upd.getRecordsPerView();
		int pageNo = 1;
		String reqPageNum = request.getParameter("pageNo");
		if (reqPageNum != null) {
			pageNo = Integer.parseInt(reqPageNum);
		}
			
		Pagination pgn = new Pagination(a, numPages);
		Page pg = pgn.getPage(pageNo);

		userform.setUserProfileData(pg.getList());
		userform.setPage(pg);	

		request.setAttribute("userProfileListForm", userform);
		request.setAttribute("pageNo", String.valueOf(pageNo));		 
		request.setAttribute("displayPage",pg);
	    
		page = "userProfileListPage";

	} 
	catch (IUMException e) {
	   LOGGER.error(CodeHelper.getStackTrace(e));
	  ActionErrors errors = new ActionErrors();
	  errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.system.exception", e.getMessage()));
	  saveErrors(request, errors); 
	  page = "errorPage"; 
	}
	
	LOGGER.info("handleAction end");
	LOGGER.debug("PAGE FORWARD-->" + page);
	return (mapping.findForward(page));
  }// execute

  private UserProfileData searchUserProfileRecord(UserProfileListForm userform) {
	  
	  LOGGER.info("searchUserProfileRecord start");
	   UserProfileData user = new UserProfileData();
	   user.setACF2ID(userform.getAcf2IdFilter());
	   user.setUserId(userform.getUserIdFilter());
	   user.setLastName(userform.getLastNameFilter());
	   user.setDeptCode(userform.getDepartmentFilter());
	   user.setOfficeCode(userform.getBranchNameFilter());
	   LOGGER.info("searchUserProfileRecord end");
  	   return user;
  }

    
  
}

