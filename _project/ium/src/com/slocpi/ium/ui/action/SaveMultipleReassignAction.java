/*
 * Created on Feb 10, 2004
 * package name = com.slocpi.ium.ui.action
 * file name    = SaveMultipleReassignAction.java
 */
package com.slocpi.ium.ui.action;

import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.slocpi.ium.IUMException;
import com.slocpi.ium.data.UserData;
import com.slocpi.ium.data.UserProfileData;
import com.slocpi.ium.ui.form.RequestFilterForm;
import com.slocpi.ium.ui.util.StateHandler;
import com.slocpi.ium.underwriter.AssessmentRequest;
import com.slocpi.ium.underwriter.UnderWriterException;
import com.slocpi.ium.util.CodeHelper;

/**
 * TODO Class Description of SaveMultipleReassignAction.java
 * @author Engel
 * 
 */
public class SaveMultipleReassignAction extends IUMAction {
	private static final Logger LOGGER = LoggerFactory.getLogger(SaveMultipleReassignAction.class);
	/* (non-Javadoc)
	 * @see com.slocpi.ium.ui.action.IUMAction#handleAction(org.apache.struts.action.ActionMapping, org.apache.struts.action.ActionForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	public ActionForward handleAction (ActionMapping mapping, 
								  ActionForm form, 
								  HttpServletRequest request,
								  HttpServletResponse response){
		
		LOGGER.info("handleAction start");
		String page = "";
		String reassignTo = "";
		
		RequestFilterForm rlForm = (RequestFilterForm) form;
	
		String [] requests = rlForm.getCheckedRefNum();
		
		reassignTo = rlForm.getReassignTo();
		
		UserData userData = new StateHandler().getUserData(request);
		UserProfileData userLogIn = userData.getProfile();
		String user = userLogIn.getUserId();  
		try {
			page = submitRequest(requests,user,reassignTo,request);
		} catch (Exception e){
			LOGGER.error(CodeHelper.getStackTrace(e));
			constructError("error.system.exception", e.getMessage(), request);
			page = "errorPage";
		} 	
		LOGGER.info("handleAction end");
		LOGGER.debug("PAGE FORWARD-->" + page);
		return mapping.findForward(page);
		
	}
	
	private String submitRequest(String [] requests , String user, String reassignTo , HttpServletRequest request) throws IUMException{
		
		LOGGER.info("submitRequest start");
		ArrayList failedRequest = new ArrayList();
		
		AssessmentRequest ar = new AssessmentRequest();
		
		String failedRequestList = new String();
		StringBuffer sb = new StringBuffer();
		int numberOfRequest = requests.length;
		
		String page="";
		
		for (int i=0; i<numberOfRequest; i++){
			String referenceNumber = requests[i];
			
			try{
				ar.reassignUnderwriter(referenceNumber,reassignTo, user);
			} catch(UnderWriterException e){
				failedRequest.add(referenceNumber + ": " +  e.getMessage() + "<br>&nbsp;");
			} 
		}
		
		if (failedRequest.size() >=1){
			StringBuffer errorMessage = new StringBuffer();
			for (int i=0; i < failedRequest.size(); i++){
				String message = (String) failedRequest.get(i);
				errorMessage.append(message);
			}
			constructError("error.submitRequest",errorMessage.toString(),request);
			page = "error";
		}
		else {
			page = "requestListPage";
		}
		LOGGER.debug("submitRequest page forward-->" + page);
		LOGGER.info("submitRequest end");
		return page;
	}
	
		
	private String constructError (String key, String parameter, HttpServletRequest request){
		
		LOGGER.info("constructError start");
		String page = "error";
		ActionErrors error = new ActionErrors();
		error.add(ActionErrors.GLOBAL_ERROR, new ActionError(key, parameter));
		saveErrors(request, error);
		LOGGER.info("constructError end");
		return page;
	}	

}
