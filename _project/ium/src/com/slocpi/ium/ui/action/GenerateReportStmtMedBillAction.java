/*
 * Created on Mar 7, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.ui.action;

import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.slocpi.ium.data.ExaminerData;
import com.slocpi.ium.data.LaboratoryData;
import com.slocpi.ium.data.MedicalExamBillingFilter;
import com.slocpi.ium.data.UserData;
import com.slocpi.ium.data.UserProfileData;
import com.slocpi.ium.reports.StatementMedicalBillReport;
import com.slocpi.ium.ui.form.ReportStmtMedBillForm;
import com.slocpi.ium.ui.util.Pagination;
import com.slocpi.ium.ui.util.StateHandler;
import com.slocpi.ium.util.CodeHelper;


/**
 * @author nic.decapia
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class GenerateReportStmtMedBillAction extends IUMAction {

	private static final Logger LOGGER = LoggerFactory.getLogger(GenerateReportStmtMedBillAction.class);
	/* (non-Javadoc)
	 * @see com.slocpi.ium.ui.action.IUMAction#handleAction(org.apache.struts.action.ActionMapping, org.apache.struts.action.ActionForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	public ActionForward handleAction(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response)
		throws Exception {
		
		LOGGER.info("handleAction start");
		String page = "reportsPage";
		try {
			page = "reportsPage";
			
			StateHandler sessionHandler = new StateHandler();
			UserData userData = sessionHandler.getUserData(request);
			UserProfileData userPref = userData.getProfile();
			
			ReportStmtMedBillForm frmReport = (ReportStmtMedBillForm) form;
			if (frmReport == null) {
				ActionErrors errors = new ActionErrors();
				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.system.exception", "Report Parameters are undefined."));
				saveErrors(request, errors);
				page = "errorPage";
			}

			MedicalExamBillingFilter filter = extractFilter(frmReport);

			StatementMedicalBillReport rpt = new StatementMedicalBillReport();
			ArrayList rptContent = rpt.generate(filter);
			
			int recPerPage = (userPref.getRecordsPerView()!=0)?userPref.getRecordsPerView():10;
			Pagination pgn = new Pagination(rptContent,recPerPage);
			frmReport.setPage(pgn.getPage(frmReport.getPageNo()));
			request.setAttribute("reportFilter", frmReport);							
		}
		catch (Exception e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			ActionErrors errors = new ActionErrors();
			errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.system.exception", e.getMessage()));
			saveErrors(request, errors);
			page = "errorPage"; 			
		}

		LOGGER.info("handleAction end");
		LOGGER.debug("PAGE FORWARD-->" + page);
		return (mapping.findForward(page));

	}

	/**
	 * @param frmReport
	 * @return
	 */
	private MedicalExamBillingFilter extractFilter(ReportStmtMedBillForm frm)
	{
		LOGGER.info("extractFilter start");
		MedicalExamBillingFilter filter = new MedicalExamBillingFilter();
		filter.setStartDate(frm.getStartDate()); 		
		filter.setEndDate(frm.getEndDate()); 			
		filter.setReportType(Integer.parseInt(frm.getReportType())); 		
		
		String agent = frm.getAgent();
		if (agent.length() != 0) {
			UserProfileData usr = new UserProfileData();
			usr.setUserId(agent);	
			filter.setAgent(usr);
		}
		
		String examiner = frm.getExaminer();
		if (examiner.length() != 0) {
			ExaminerData exm = new ExaminerData();
			exm.setExaminerId(Long.parseLong(examiner));
			filter.setExaminer(exm);
		}
		
		String laboratory = frm.getLaboratory();
		if (laboratory.length() != 0) {
			LaboratoryData lab = new LaboratoryData();
			lab.setLabId(Long.parseLong(laboratory));
			filter.setLaboratory(lab);
		}
		
		LOGGER.info("extractFilter end");
		return (filter);
	}

}
