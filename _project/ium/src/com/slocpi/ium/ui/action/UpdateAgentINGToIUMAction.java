// Update Agent Details from Ingenium to IUM - Andre Ceasar Dacanay
package com.slocpi.ium.ui.action;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.slocpi.ium.IUMException;
import com.slocpi.ium.data.dao.AssessmentRequestDAO;
import com.slocpi.ium.data.util.DataSourceProxy;
import com.slocpi.ium.service.ingenium.IngeniumGateway;
import com.slocpi.ium.util.CodeHelper;

/**
 * This class is responsible for displaying the IUM's list of requirements.
 * This class will display the list and allow the viewing of a specific requirement.
 * @author Engel
 * 
 */
public class UpdateAgentINGToIUMAction extends IUMAction {

	private static final Logger LOGGER = LoggerFactory.getLogger(UpdateAgentINGToIUMAction.class);
	
	/* (non-Javadoc)
	 * @see com.slocpi.ium.ui.action.IUMAction#handleAction(org.apache.struts.action.ActionMapping, org.apache.struts.action.ActionForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	public ActionForward handleAction(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response)
		throws Exception {
		
		LOGGER.info("handleAction start");
		String arFormRefNo = request.getParameter("arFormRefNo") != null ? (String)request.getParameter("arFormRefNo") : "";
		String polSuffix = request.getParameter("polSuffix") != null ? request.getParameter("polSuffix") : "";
		String agentCode = request.getParameter("agentCode") != null ? request.getParameter("agentCode") : "";
		String branchCode = request.getParameter("branchCode") != null ? request.getParameter("branchCode") : "";
		String userId = request.getParameter("userId") != null ? request.getParameter("userId") : "";
		
		String page = "viewUpdateAgentINGToIUM";

		IngeniumGateway ingenium = new IngeniumGateway(true);
		
		try {
			String message = "";
			String message2 = "";
			HashMap hsIngConsol = ingenium.retrieveIngeniumConsolidatedInformation(arFormRefNo, polSuffix);
			HashMap hsIngConsolAgt = new HashMap();
			Iterator itConsol = hsIngConsol.keySet().iterator();
			while(itConsol.hasNext()){
				String key = (String)itConsol.next();
				
				if(key.equals("AGENT_INFORMATION_SERVICE_AGENT_CODE") || 
						key.equals("AGENT_INFORMATION_SERVICE_AGENT_NAME") ||
						key.equals("AGENT_INFORMATION_SERVICE_NBO_CODE")){
				
				ArrayList list = (ArrayList)hsIngConsol.get(key);

				ArrayList listInfo = new ArrayList();
				for(Iterator it = list.iterator(); it.hasNext();){
					String value = (String)it.next();
					if(value!=null && !value.equals("*")){
						listInfo.add(value);
					}
				}
				hsIngConsolAgt.put(key, listInfo);
				
				}
			}
			
			
			if(hsIngConsolAgt!=null && hsIngConsolAgt.get("RESULT_CODE")!=null){
				if(!hsIngConsolAgt.get("RESULT_CODE").equals("1")){
					message2 = "CICS error. Check Pathfinder.txt for details.";
				}
			}
			
			if(hsIngConsolAgt!=null && hsIngConsolAgt.get("AGENT_INFORMATION_SERVICE_AGENT_CODE")!=null){
				HashMap hmAgt = new HashMap();
				
				String serAgtCd = null;
				ArrayList serAgtCdList = (ArrayList)hsIngConsolAgt.get("AGENT_INFORMATION_SERVICE_AGENT_CODE");
				Iterator serAgtCdIt = serAgtCdList.iterator();
				while(serAgtCdIt.hasNext()){
					String value = (String)serAgtCdIt.next();
					hmAgt.put("AGENT_ID",value);
					serAgtCd = value;
				}
				
				String serAgtNm = null;
				ArrayList serAgtNmList = (ArrayList)hsIngConsolAgt.get("AGENT_INFORMATION_SERVICE_AGENT_NAME");
				Iterator serAgtNmIt = serAgtNmList.iterator();
				while(serAgtNmIt.hasNext()){
					String value = (String)serAgtNmIt.next();
					serAgtNm = value;
				}
				
				String serAgtNboCd = null;
				ArrayList serAgtNboCdList = (ArrayList)hsIngConsolAgt.get("AGENT_INFORMATION_SERVICE_NBO_CODE");
				Iterator serAgtNboCdIt = serAgtNboCdList.iterator();
				while(serAgtNboCdIt.hasNext()){
					String value = (String)serAgtNboCdIt.next();
					hmAgt.put("BRANCH_ID",value);
					serAgtNboCd = value;
				}


				if(!agentCode.equals("") && !branchCode.equals("")&& agentCode.equals(serAgtCd) && branchCode.equals(serAgtNboCd)){
					message = "Current agent is already sync with Ingenium. \n";
					message2 = "No update will be done.\n";
				}else{
					
					
					AssessmentRequestDAO arDao = new AssessmentRequestDAO();
					
					hmAgt.put("REFERENCE_NUM",arFormRefNo);
					hmAgt.put("UPDATED_BY",userId);
					
					arDao.updateAgent(hmAgt);
					
					message = "Current agent is NOT sync with Ingenium. \n";
					message2 = "Updated agent to "+ serAgtCd+"-" + serAgtNm +".\n";
					request.setAttribute("updateTag", "Y");
					
					
				}

			}else{
				message = "Please check Ingenium. \n";
			
			}
			
			request.setAttribute("message", message);
			request.setAttribute("message2", message2);
		} catch (Exception e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			request.setAttribute("message", e.getMessage());
			throw new IUMException(e);
		}
		
		LOGGER.info("handleAction end");
		LOGGER.debug("PAGE FORWARD-->" + page);
		return (mapping.findForward(page));
	}

	public void closeDB(Connection conn) {
		
		LOGGER.info("closeDB start");
		try {
			conn.close();
		} catch (Exception e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
		}
		LOGGER.info("closeDB end");
	}
}
