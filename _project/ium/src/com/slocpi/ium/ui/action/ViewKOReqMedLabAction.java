package com.slocpi.ium.ui.action;

// struts package
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import com.slocpi.ium.IUMException;
import com.slocpi.ium.data.AssessmentRequestData;
import com.slocpi.ium.data.ClientData;
import com.slocpi.ium.data.ClientDataSheetData;
import com.slocpi.ium.data.KickOutMessageData;
import com.slocpi.ium.data.LOBData;
import com.slocpi.ium.data.MedicalRecordData;
import com.slocpi.ium.data.PolicyRequirementsData;
import com.slocpi.ium.data.StatusData;
import com.slocpi.ium.data.SunLifeOfficeData;
import com.slocpi.ium.data.TestProfileData;
import com.slocpi.ium.data.UWAssessmentData;
import com.slocpi.ium.data.UserData;
import com.slocpi.ium.data.UserProfileData;
import com.slocpi.ium.data.dao.RequirementDAO;
import com.slocpi.ium.data.util.DataSourceProxy;
import com.slocpi.ium.ui.form.FormLoader;
import com.slocpi.ium.ui.form.KOReasonsForm;
import com.slocpi.ium.ui.form.KOReqMedLabDetailForm;
import com.slocpi.ium.ui.form.MedLabRecordsForm;
import com.slocpi.ium.ui.form.RequirementForm;
import com.slocpi.ium.ui.util.StateHandler;
import com.slocpi.ium.underwriter.AssessmentRequest;
import com.slocpi.ium.underwriter.KickOutMessage;
import com.slocpi.ium.underwriter.PolicyRequirements;
import com.slocpi.ium.underwriter.UnderWriterException;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.DateHelper;
import com.slocpi.ium.util.IUMConstants;
import com.slocpi.ium.util.ValueConverter;


/**
 * @TODO Class Description ViewKOReqMedLabAction
 * @author: Ma. Theresa Vicencio
 * @version: 1.0
 */
public class ViewKOReqMedLabAction extends IUMAction {

	private static final Logger LOGGER = LoggerFactory.getLogger(ViewKOReqMedLabAction.class);
  /**
   * @TODO method description for execute
   * @param mapping mapping of request to an instance of this class
   * @param form  object
   * @param request request object
   * @param response response object
   * @exception IOException if an input/output error occurs
   * @exception ServletException if a servlet exception occurs
   * @return ActionForward as defined in the mapping parameter.
   */
  public ActionForward handleAction(ActionMapping mapping,
							   ActionForm form,
							   HttpServletRequest request,
							   HttpServletResponse response)
							   throws Exception {
	
   LOGGER.info("handleAction start");
	String page = "";
	try {
		
		StateHandler sh = new StateHandler();
		UserData ud = sh.getUserData(request);
		UserProfileData profile = ud.getProfile();
		String userId = profile.getUserId();
		String pageId = "";

		KOReqMedLabDetailForm koReqMedLabDetailForm = (KOReqMedLabDetailForm) form;

		if (koReqMedLabDetailForm == null) {
		  koReqMedLabDetailForm = new KOReqMedLabDetailForm();
		}

		String refNo = koReqMedLabDetailForm.getRefNo();
		String sReqId = request.getParameter("reqId");
		String reqId = null;

		String isMaintain = request.getParameter("isMaintain");
		
		if ((isMaintain == null) || (isMaintain.equals("false"))) {
			koReqMedLabDetailForm = (KOReqMedLabDetailForm) FormLoader.loadAssessmentRequest(koReqMedLabDetailForm);
		}
		else {
			
			String searchClientType = request.getParameter("searchClientType");
			if (searchClientType != null && !searchClientType.equals("")){
				if (searchClientType.equals(IUMConstants.CLIENT_TYPE_INSURED)){
					retrieveInsuredClientData(koReqMedLabDetailForm);
				} else if (searchClientType.equals(IUMConstants.CLIENT_TYPE_OWNER)){
					retrieveOwnerClientData(koReqMedLabDetailForm);
				}
				koReqMedLabDetailForm.setClientToSearch(searchClientType);
			}
			else{
				koReqMedLabDetailForm = (KOReqMedLabDetailForm) FormLoader.loadAssessmentRequest(koReqMedLabDetailForm);
			}
		}

		ArrayList requirements  = populateRequirements(refNo);
		ArrayList medLabRecords = populateMedLabRecords(koReqMedLabDetailForm.getInsuredClientNo());
		ArrayList koReasons     = populateKOReasons(refNo);
		ArrayList reqLevelList  = getRequestLevel();

		koReqMedLabDetailForm.setRequirements(requirements);
		koReqMedLabDetailForm.setMedLabRecords(medLabRecords);
		koReqMedLabDetailForm.setKoReasons(koReasons);
		koReqMedLabDetailForm.setReqLevelList(reqLevelList);

		RequirementForm  requirementForm = new RequirementForm();

		if (sReqId != null) {
		  reqId = sReqId;
		  requirementForm = populateRequirementDetails(reqId);
		}
		else if (requirements.size() > 0) {
		  reqId = ((RequirementForm)requirements.get(0)).getReqId();
		  requirementForm = populateRequirementDetails(reqId);
		}

		request.setAttribute("requirementForm", requirementForm);

		HttpSession session = request.getSession();
		session.setAttribute("detailForm", koReqMedLabDetailForm);

		page = "koReqMedLabDetailPage";

		AssessmentRequest assessmentReq = new AssessmentRequest();
		if(assessmentReq.isReqtCountExceeded(refNo) && page.equals("koReqMedLabDetailPage")){
			ActionMessages warning = new ActionMessages();
			warning.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("message.reqtCountExceeded"));
			saveMessages(request, warning);
		}
		
	}
	catch (UnderWriterException e) {
		LOGGER.error(CodeHelper.getStackTrace(e));
		ActionErrors errors = new ActionErrors();
		errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.request.generic", e.getMessage()));
		saveErrors(request, errors);
		page = "koReqMedLabDetailPage";
	}
	catch (IUMException e) {
		LOGGER.error(CodeHelper.getStackTrace(e));
		ActionErrors errors = new ActionErrors();
		errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.system.exception", e.getMessage()));
		saveErrors(request, errors);
		page = "errorPage";
	}
	catch (Exception e) {
		LOGGER.error(CodeHelper.getStackTrace(e));
		ActionErrors errors = new ActionErrors();
		errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.system.exception", e.getMessage()));
		saveErrors(request, errors);
		page = "errorPage";
	}
	LOGGER.info("handleAction end");
	LOGGER.debug("PAGE FORWARD-->" + page);
	return (mapping.findForward(page));
  }// handleAction

/**
 * @deprecated use FormLoader.loadAssessmentRequest() method
 * @param reqForm
 * @return
 * @throws UnderWriterException
 * @throws IUMException
 */
  private KOReqMedLabDetailForm populateRequestDetails(KOReqMedLabDetailForm reqForm) throws UnderWriterException, IUMException {
	
	LOGGER.info("populateRequestDetails start");
	AssessmentRequest assessmentRequest = new AssessmentRequest();
	AssessmentRequestData requestData = assessmentRequest.getDetails(reqForm.getRefNo());

	LOBData lob = requestData.getLob();
	ClientData insured = requestData.getInsured();
	ClientData owner = requestData.getOwner();
	SunLifeOfficeData branch = requestData.getBranch();
	UserProfileData agent = requestData.getAgent();
	UserProfileData assignedTo = requestData.getAssignedTo();
	UserProfileData underwriter = requestData.getUnderwriter();
	UserProfileData location = requestData.getFolderLocation();
	ClientDataSheetData cds = requestData.getClientDataSheet();
	StatusData statusData = requestData.getStatus();

	NumberFormat nf = NumberFormat.getInstance();
	nf.setMinimumFractionDigits(2);
	nf.setMaximumFractionDigits(2);

	reqForm.setAutoSettleIndicator(requestData.getAutoSettleIndicator());
	reqForm.setTransmitIndicator(requestData.getTransmitIndicator());
	reqForm.setRefNo(reqForm.getRefNo());
	reqForm.setLob(lob.getLOBCode());
	reqForm.setSourceSystem(requestData.getSourceSystem());
	reqForm.setAmountCovered(nf.format(requestData.getAmountCovered()));
	reqForm.setPremium(nf.format(requestData.getPremium()));
	reqForm.setInsuredName(insured.getGivenName() + " " + insured.getLastName());
	reqForm.setBranchCode(branch.getOfficeId());
	reqForm.setBranchName(branch.getOfficeName());
	reqForm.setAgentCode(agent.getUserId());
	reqForm.setAgentName(agent.getFirstName() + " " + agent.getLastName());
	reqForm.setRequestStatus(String.valueOf(statusData.getStatusId()));
	reqForm.setAssignedTo(assignedTo.getUserId());
	reqForm.setUnderwriter(underwriter.getUserId());
	reqForm.setLocation(location.getUserId());

	reqForm.setRemarks(requestData.getRemarks());
	reqForm.setReceivedDate((DateHelper.format(requestData.getApplicationReceivedDate(), "ddMMMyyyy")).toUpperCase());
	reqForm.setDateForwarded((DateHelper.format(requestData.getForwardedDate(), "ddMMMyyyy")).toUpperCase());
	reqForm.setDateEdited((DateHelper.format(requestData.getUpdatedDate(), "ddMMMyyyy")).toUpperCase());
	reqForm.setDateCreated((DateHelper.format(requestData.getCreatedDate(), "ddMMMyyyy")).toUpperCase());
	reqForm.setStatusDate((DateHelper.format(requestData.getStatusDate(), "ddMMMyyyy")).toUpperCase());

	reqForm.setInsuredClientType(cds.getClientType());
	reqForm.setInsuredClientNo(insured.getClientId());
	reqForm.setInsuredLastName(insured.getLastName());
	reqForm.setInsuredFirstName(insured.getGivenName());
	reqForm.setInsuredMiddleName(insured.getMiddleName());
	reqForm.setInsuredTitle(insured.getTitle());
	reqForm.setInsuredSuffix(insured.getSuffix());

	if (insured.getAge() == 0){
		reqForm.setInsuredAge("");
	}else {
		reqForm.setInsuredAge(String.valueOf(insured.getAge()));
	}

	reqForm.setInsuredSex(insured.getSex());
	reqForm.setInsuredBirthDate((DateHelper.format(insured.getBirthDate(), "ddMMMyyyy")).toUpperCase());

	reqForm.setOwnerClientType(cds.getClientType());
	reqForm.setOwnerClientNo(owner.getClientId());
	reqForm.setOwnerLastName(owner.getLastName());
	reqForm.setOwnerFirstName(owner.getGivenName());
	reqForm.setOwnerMiddleName(owner.getMiddleName());
	reqForm.setOwnerTitle(owner.getTitle());
	reqForm.setOwnerSuffix(owner.getSuffix());

	if (owner.getAge() == 0){
		reqForm.setOwnerAge(String.valueOf(owner.getAge()));
	}else {
		reqForm.setOwnerAge(String.valueOf(owner.getAge()));
	}

	reqForm.setOwnerSex(owner.getSex());
	reqForm.setOwnerBirthDate((DateHelper.format(owner.getBirthDate(), "ddMMMyyyy")).toUpperCase());

	UWAssessmentData uwData = assessmentRequest.getUWAssessmentDetails(reqForm.getRefNo());
	reqForm.setUWRemarks(uwData.getRemarks());

	LOGGER.info("populateRequestDetails end");
	return (reqForm);
  }

  private RequirementForm populateRequirementDetails(String reqId) throws UnderWriterException, IUMException {
	  
	LOGGER.info("populateRequirementDetails start");
	PolicyRequirements pr = new PolicyRequirements();
	PolicyRequirementsData reqData = pr.getRequirement(Long.parseLong(reqId));

	RequirementForm reqForm = new RequirementForm();
	if (reqData != null) {
		reqForm.setRefNo(reqData.getReferenceNumber());
		reqForm.setReqId(reqId);
		reqForm.setReqCode(reqData.getRequirementCode());
		reqForm.setSeq(String.valueOf(reqData.getSequenceNumber()));
		reqForm.setReqDesc(reqData.getReqDesc());
		reqForm.setReqLevel(reqData.getLevel());
		reqForm.setCreatedDate((DateHelper.format(reqData.getCreateDate(), "ddMMMyyyy")).toUpperCase());
		reqForm.setCompleteReq(String.valueOf(reqData.isCompletedRequirementInd()));

		StatusData stat = reqData.getStatus();
		String statusId = "";
		String desc     = "";
		if (stat != null) {
		  statusId = String.valueOf(stat.getStatusId());
		  desc     = String.valueOf(stat.getStatusDesc());
		}

		reqForm.setReqStatusCode(desc);
		reqForm.setReqStatusValue(statusId);
		reqForm.setStatusDate((DateHelper.format(reqData.getStatusDate(), "ddMMMyyyy")).toUpperCase());
		reqForm.setUpdatedBy(reqData.getUpdatedBy());
		reqForm.setUpdatedDate((DateHelper.format(reqData.getUpdateDate(), "ddMMMyyyy")).toUpperCase());
		reqForm.setClientType(reqData.getClientType());
		reqForm.setDesignation(reqData.getDesignation());
		reqForm.setTestDate((DateHelper.format(reqData.getTestDate(), "ddMMMyyyy")).toUpperCase());
		reqForm.setTestResult(reqData.getTestResultCode());
		reqForm.setTestDesc(reqData.getTestResultDesc());
		reqForm.setResolve(String.valueOf(reqData.isResolveInd()));
		reqForm.setFollowUpNo(String.valueOf(reqData.getFollowUpNumber()));
		reqForm.setFollowUpDate((DateHelper.format(reqData.getFollowUpDate(), "ddMMMyyyy")).toUpperCase());
		reqForm.setValidityDate((DateHelper.format(reqData.getValidityDate(), "ddMMMyyyy")).toUpperCase());
		reqForm.setPaidInd(String.valueOf(reqData.isPaidInd()));
		reqForm.setNewTest(ValueConverter.booleanToString(reqData.getNewTestOnly()));
		reqForm.setOrderDate((DateHelper.format(reqData.getOrderDate(), "ddMMMyyyy")).toUpperCase());
		reqForm.setDateSent((DateHelper.format(reqData.getDateSent(), "ddMMMyyyy")).toUpperCase());
		reqForm.setCcasSuggest(ValueConverter.booleanToString(reqData.getCCASuggestInd()));
		reqForm.setReceivedDate((DateHelper.format(reqData.getReceiveDate(), "ddMMMyyyy")).toUpperCase());
		reqForm.setAutoOrder(ValueConverter.booleanToString(reqData.getAutoOrderInd()));
		reqForm.setFldComm(ValueConverter.booleanToString(reqData.getFldCommentInd()));
		reqForm.setComment(reqData.getComments());
		reqForm.setClientNo(reqData.getClientId());
		
		reqForm.setImageReferenceNum(reqData.getImageRef());	
		
		String fnURL = getWMSImageLink(reqData);
		reqForm.setWmsImageLink(fnURL);
	}
	
	LOGGER.info("populateRequirementDetails end");
	return (reqForm);
  }

  private ArrayList getRequestLevel() throws SQLException, IUMException {
	  
	LOGGER.info("getRequestLevel start");
  	
    try {
		
		RequirementDAO  dao  = new RequirementDAO();
		ArrayList       list = new ArrayList();
		list = dao.retrieveRequirements();
			if (list != null){
				LOGGER.debug("size = " + list.size());
			}else{
				LOGGER.debug("list is null");
			}
			
		LOGGER.info("getRequestLevel end");	
		return list;
    }
    catch (SQLException e) {
    	LOGGER.error(CodeHelper.getStackTrace(e));
    	throw new IUMException(e.getMessage());
    }
  }

  
private ArrayList populateRequirements(String refNo) throws UnderWriterException, IUMException {
	
	LOGGER.info("populateRequirements start");
	PolicyRequirements pr = new PolicyRequirements();
	ArrayList reqList = pr.getRequirements(refNo);

	int size = reqList.size();
	ArrayList requirements = new ArrayList();
	for (int i = 0; i < size; i++) {
	  PolicyRequirementsData reqData = (PolicyRequirementsData)reqList.get(i);
	  RequirementForm reqForm = new RequirementForm();
	  reqForm.setRefNo(reqData.getReferenceNumber());
	  reqForm.setReqId(String.valueOf(reqData.getRequirementId()));
	  reqForm.setSeq(String.valueOf(reqData.getSequenceNumber()));
	  reqForm.setReqCode(reqData.getRequirementCode());
	  StatusData stat = reqData.getStatus();
	  reqForm.setReqStatusCode(String.valueOf(stat.getStatusId()));
	  reqForm.setReqStatusDesc(stat.getStatusDesc());
	  reqForm.setReqLevel(reqData.getLevel());
	  reqForm.setStatusDate((DateHelper.format(reqData.getStatusDate(), "ddMMMyyyy")).toUpperCase());
	  reqForm.setComment(reqData.getComments());
	  reqForm.setClientType(reqData.getClientType());
	  reqForm.setReqLevel(reqData.getLevel());
	  reqForm.setFollowUpDate((DateHelper.format(reqData.getFollowUpDate(), "ddMMMyyyy")).toUpperCase());
	  reqForm.setFollowUpNo(String.valueOf(reqData.getFollowUpNumber()));
	  reqForm.setDateSent((DateHelper.format(reqData.getDateSent(), "ddMMMyyyy")).toUpperCase());
	  reqForm.setClientNo(reqData.getClientId());
	  reqForm.setTestDate((DateHelper.format(reqData.getTestDate(), "ddMMMyyyy")).toUpperCase());
	  reqForm.setImageReferenceNum(reqData.getImageRef());	
		  
	  String fnURL = getWMSImageLink(reqData);
	  reqForm.setWmsImageLink(fnURL);
	  
	  requirements.add(reqForm);
	}

	LOGGER.info("populateRequirements end");
	return (requirements);
  }

private String getWMSImageLink(PolicyRequirementsData reqData) {
	
	
	ResourceBundle rb = ResourceBundle.getBundle(IUMConstants.IUM_WMS_CONFIG);
	String WMSServer = rb.getString("WMS_Server");
	String paramObjectStoreName = rb.getString("param_Object_Store_Name");
	String fnURL = null;
	int imageRef=0;
	if (reqData.getImageRef() != null){
		try{
			imageRef = Integer.parseInt(reqData.getImageRef().trim());
			if(imageRef!=0){
				fnURL = WMSServer + "/getContent?id=" + reqData.getImageRef() + "&objectStoreName=" + paramObjectStoreName + "&objectType=document";
			}
		} catch(NumberFormatException e){
			LOGGER.error(CodeHelper.getStackTrace(e));
			if(!reqData.getImageRef().trim().equals("Y")||!reqData.getImageRef().trim().equals("y")){
				fnURL = WMSServer + "/getContent?id=" + reqData.getImageRef() + "&objectStoreName=" + paramObjectStoreName + "&objectType=document";
			}
		}
	}
	
	return fnURL;
}


  private ArrayList populateMedLabRecords(String clientNo) throws UnderWriterException, IUMException {
	  
	LOGGER.info("populateMedLabRecords start");
	
	AssessmentRequest ar = new AssessmentRequest();
	ArrayList medReqList = ar.getKOMedRecs(clientNo);

	int size = medReqList.size();
	ArrayList medicalRecs = new ArrayList();

	for (int i = 0; i < size; i++) {
	  MedicalRecordData medData = (MedicalRecordData)medReqList.get(i);
	  MedLabRecordsForm medRecForm = new MedLabRecordsForm();
	  TestProfileData testData = medData.getTest();
	  medRecForm.setCode(String.valueOf(testData.getTestId()));
	  medRecForm.setTestDescription(testData.getTestDesc());

	  String type = "";
	  String labTestInd = medData.getLabTestInd();
	  type = labTestInd;
	  
	  medRecForm.setType(type);
	  medRecForm.setDateConducted((DateHelper.format(medData.getConductedDate(), "ddMMMyyyy")).toUpperCase());
	  medRecForm.setDateReceived((DateHelper.format(medData.getReceivedDate(), "ddMMMyyyy")).toUpperCase());
	  medicalRecs.add(medRecForm);
	}
	
	LOGGER.info("populateMedLabRecords end");
	return (medicalRecs);
  }


  private ArrayList populateKOReasons(String refNo) throws UnderWriterException, IUMException {
	  
	LOGGER.info("populateKOReasons start");
	KickOutMessage ko = new KickOutMessage();
	ArrayList reqList = ko.getMessages(refNo);

	int size = reqList.size();
	ArrayList koReasons = new ArrayList();
	for (int i = 0; i < size; i++) {
	  KickOutMessageData koData = (KickOutMessageData)reqList.get(i);
	  KOReasonsForm koForm = new KOReasonsForm();
	  koForm.setSeq(String.valueOf(koData.getSequenceNumber()));
	  koForm.setMessageText(koData.getMessageText());
	  koForm.setClientNo(koData.getClientId());
	  koForm.setFailResponse(koData.getFailResponse());
	  koReasons.add(koForm);
	}

	LOGGER.info("populateKOReasons end");
	return (koReasons);
  }


  private void retrieveInsuredClientData(KOReqMedLabDetailForm arf) throws IUMException{
	  
		  LOGGER.info("retrieveInsuredClientData start");
		  arf.setInsuredClientNo(arf.getInsuredClientNo().toUpperCase());
		  AssessmentRequest ar = new AssessmentRequest();
		  ClientData cdata = ar.getClientData(arf.getInsuredClientNo());
		  if(cdata!=null&&cdata.getGivenName()!=null && !cdata.getGivenName().equals("")){
			  
			  if (cdata.getAge() == 0){
			  	arf.setInsuredAge("");
			  } else {
				arf.setInsuredAge( String.valueOf(cdata.getAge()) );
			  }

			  if(cdata.getBirthDate()!=null){
				  arf.setInsuredBirthDate( (DateHelper.format( cdata.getBirthDate(),"ddMMMyyyy" ) ).toUpperCase());
			  }else{
				  arf.setInsuredBirthDate("");
			  }
			  if(cdata.getGivenName()!=null){
				  arf.setInsuredFirstName( cdata.getGivenName() );
			  }else{
				  arf.setInsuredFirstName( "" );
			  }
			  if(cdata.getLastName()!=null){
				  arf.setInsuredLastName( cdata.getLastName() );
			  }else{
				  arf.setInsuredLastName( "" );
			  }
			  if(cdata.getMiddleName()!=null){
				  arf.setInsuredMiddleName( cdata.getMiddleName() );
			  }else{
				  arf.setInsuredMiddleName( "" );
			  }
			  if(cdata.getTitle()!=null){
				  arf.setInsuredTitle( cdata.getTitle() );
			  }else{
				  arf.setInsuredTitle( "" );
			  }
			  if(cdata.getSex()!=null){
				  arf.setInsuredSex( cdata.getSex() );
			  }else{
				  arf.setInsuredSex( "" );
			  }
			  if(cdata.getSuffix()!=null){
				  arf.setInsuredSuffix( cdata.getSuffix() );
			  }else{
				  arf.setInsuredSuffix( "" );
			  }
			  arf.setInsuredOriginal( arf.getInsuredClientNo()+
									  arf.getInsuredFirstName()+
									  arf.getInsuredLastName()+
									  arf.getInsuredMiddleName()+
									  arf.getInsuredTitle()+
									  arf.getInsuredSuffix()+
									  arf.getInsuredSex());
		  }else{
			  arf.setInsuredAge("");
			  arf.setInsuredFirstName("");
			  arf.setInsuredMiddleName("");
			  arf.setInsuredLastName("");
			  arf.setInsuredBirthDate("");
			  arf.setInsuredSuffix("");
			  arf.setInsuredTitle("");
			  arf.setInsuredSex("");
			  arf.setInsuredOriginal("");
		  }
		  LOGGER.info("retrieveInsuredClientData end");

	  }

	private void retrieveOwnerClientData(KOReqMedLabDetailForm arf)throws IUMException{
		
		     LOGGER.info("retrieveOwnerClientData start");
			arf.setOwnerClientNo(arf.getOwnerClientNo().toUpperCase());
			AssessmentRequest ar = new AssessmentRequest();
			ClientData cdata = ar.getClientData(arf.getOwnerClientNo());
			if(cdata!=null&&cdata.getGivenName()!=null && !cdata.getGivenName().equals("")){

				if (cdata.getAge() == 0){
					arf.setOwnerAge("");
				}else{
					arf.setOwnerAge( String.valueOf(cdata.getAge()) );
				}

				if(cdata.getBirthDate()!=null){
					arf.setOwnerBirthDate( (DateHelper.format( cdata.getBirthDate(),"ddMMMyyyy" ) ).toUpperCase());
				}else{
					arf.setOwnerBirthDate("");
				}
				if(cdata.getGivenName()!=null){
					arf.setOwnerFirstName( cdata.getGivenName() );
				}else{
					arf.setOwnerFirstName( "" );
				}
				if(cdata.getLastName()!=null){
					arf.setOwnerLastName( cdata.getLastName() );
				}else{
					arf.setOwnerLastName( "" );
				}
				if(cdata.getMiddleName()!=null){
					arf.setOwnerMiddleName( cdata.getMiddleName() );
				}else{
					arf.setOwnerMiddleName( "" );
				}
				if(cdata.getTitle()!=null){
					arf.setOwnerTitle( cdata.getTitle() );
				}else{
					arf.setOwnerTitle( "" );
				}
				if(cdata.getSex()!=null){
					arf.setOwnerSex( cdata.getSex() );
				}else{
					arf.setOwnerSex( "" );
				}
				if(cdata.getSuffix()!=null){
					arf.setOwnerSuffix( cdata.getSuffix() );
				}else{
					arf.setOwnerSuffix( "" );
				}
				arf.setOwnerOriginal( arf.getOwnerClientNo()+
										arf.getOwnerFirstName()+
										arf.getOwnerLastName()+
										arf.getOwnerMiddleName()+
										arf.getOwnerTitle()+
										arf.getOwnerSuffix()+
										arf.getOwnerSex());

			}else{
				arf.setOwnerAge("");
				arf.setOwnerFirstName("");
				arf.setOwnerMiddleName("");
				arf.setOwnerLastName("");
				arf.setOwnerBirthDate("");
				arf.setOwnerSuffix("");
				arf.setOwnerTitle("");
				arf.setOwnerSex("");
				arf.setOwnerOriginal("");
			}
			LOGGER.info("retrieveOwnerClientData end");
		}

}

