package com.slocpi.ium.ui.action;

// struts package
import java.io.IOException;
import java.text.NumberFormat;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.slocpi.ium.IUMException;
import com.slocpi.ium.data.AssessmentRequestData;
import com.slocpi.ium.data.ClientData;
import com.slocpi.ium.data.ClientDataSheetData;
import com.slocpi.ium.data.ImpairmentData;
import com.slocpi.ium.data.LOBData;
import com.slocpi.ium.data.StatusData;
import com.slocpi.ium.data.SunLifeOfficeData;
import com.slocpi.ium.data.UWAssessmentData;
import com.slocpi.ium.data.UserData;
import com.slocpi.ium.data.UserProfileData;
import com.slocpi.ium.ui.form.FormLoader;
import com.slocpi.ium.ui.form.ImpairmentForm;
import com.slocpi.ium.ui.form.UWAnalysisForm;
import com.slocpi.ium.ui.form.UWAssessmentDetailForm;
import com.slocpi.ium.ui.util.Page;
import com.slocpi.ium.ui.util.Pagination;
import com.slocpi.ium.ui.util.StateHandler;
import com.slocpi.ium.underwriter.AssessmentRequest;
import com.slocpi.ium.underwriter.Impairment;
import com.slocpi.ium.underwriter.UnderWriterException;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.DateHelper;
import com.slocpi.ium.util.IUMConstants;
import com.slocpi.ium.util.ValueConverter;


/**
 * @TODO Class Description ViewUWAssessmentAction
 * @author: Ma. Theresa Vicencio
 * @version: 1.0
 */
public class ViewUWAssessmentAction extends IUMAction {

	private static final Logger LOGGER = LoggerFactory.getLogger(ViewUWAssessmentAction.class);
  /**
   * @TODO method description for execute
   * @param mapping mapping of request to an instance of this class
   * @param form  object
   * @param request request object
   * @param response response object
   * @exception IOException if an input/output error occurs
   * @exception ServletException if a servlet exception occurs
   * @return ActionForward as defined in the mapping parameter.
   */
  public ActionForward handleAction(ActionMapping mapping,
							   ActionForm form,
							   HttpServletRequest request,
							   HttpServletResponse response)
							   throws Exception {
	  
	LOGGER.info("handleAction start");
	String page = "";
	try {
		
		StateHandler sh = new StateHandler();
		UserData ud = sh.getUserData(request);
		UserProfileData profile = ud.getProfile();
		String userId = profile.getUserId();
		String role = profile.getRole();
		String pageId = "";

		ActionErrors errors;

		UWAssessmentDetailForm uwAssessmentDetailForm = (UWAssessmentDetailForm) form;

		if (uwAssessmentDetailForm == null) {
		  uwAssessmentDetailForm = new UWAssessmentDetailForm();
		}

		String impairmentId = uwAssessmentDetailForm.getIdSelected();
		String selectedMaintain = request.getParameter("selectedMaintain");
		
		if  (impairmentId != null && selectedMaintain.equals("1")){
			Impairment impairment = new Impairment();
			ImpairmentData data = new ImpairmentData();
			data = impairment.retrieveImpairment(Long.parseLong(impairmentId));
			ImpairmentForm impForm = setImpairment(data);
			request.setAttribute("maintainThisImpairment", impForm);
		}

		String clientType = request.getParameter("clientType");
        if (clientType == null) {
          clientType = uwAssessmentDetailForm.getInsuredClientType();
        }

		String isMaintain = request.getParameter("isMaintain");
		
		if ((isMaintain == null) || (isMaintain.equals("false"))) {
			uwAssessmentDetailForm = (UWAssessmentDetailForm) FormLoader.loadAssessmentRequest(uwAssessmentDetailForm);
			uwAssessmentDetailForm.setListOfAnalysis(populateUWAnalysis(uwAssessmentDetailForm.getRefNo()));
		}
		else {
			
			String searchClientType = request.getParameter("searchClientType");
			if (searchClientType != null && !searchClientType.equals("")){
				if (searchClientType.equals(IUMConstants.CLIENT_TYPE_INSURED)){
					retrieveInsuredClientData(uwAssessmentDetailForm);
				} else if (searchClientType.equals(IUMConstants.CLIENT_TYPE_OWNER)){
					retrieveOwnerClientData(uwAssessmentDetailForm);
				}
				uwAssessmentDetailForm.setClientToSearch(searchClientType);
			}
			else{
				
				uwAssessmentDetailForm = (UWAssessmentDetailForm) FormLoader.loadAssessmentRequest(uwAssessmentDetailForm);
				uwAssessmentDetailForm.setListOfAnalysis(populateUWAnalysis(uwAssessmentDetailForm.getRefNo()));
			}
		}

		String refNo = uwAssessmentDetailForm.getRefNo();
		String iClientNo = uwAssessmentDetailForm.getInsuredClientNo();
		String oClientNo = uwAssessmentDetailForm.getOwnerClientNo();

		if ((clientType.equals(IUMConstants.CLIENT_TYPE_INSURED)) && (iClientNo == null)) {
			errors = new ActionErrors();
			errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.request.generic", "There is no existing insured client."));
			saveErrors(request, errors);
		}
		else if ((clientType.equals(IUMConstants.CLIENT_TYPE_OWNER)) && (oClientNo == null)) {
			errors = new ActionErrors();
			errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.request.generic", "There is no existing owner client."));
			saveErrors(request, errors);
		}

		ArrayList impairments = populateImpairments(refNo, clientType);

		int recPerView = profile.getRecordsPerView();
		if (recPerView == 0) {
		  recPerView = 1;
		}
		int pageNo = 1;
		String reqPageNum = request.getParameter("pageNo");
		if ((reqPageNum != null) && (!reqPageNum.equals("")))  {
			pageNo = Integer.parseInt(reqPageNum);
		}

		Pagination pgn = new Pagination(impairments, recPerView);
		Page pg = pgn.getPage(pageNo);

		uwAssessmentDetailForm.setImpairments(pg.getList());

		request.setAttribute("detailForm", uwAssessmentDetailForm);
		request.setAttribute("page", pg);
		request.setAttribute("pageNo", String.valueOf(pageNo));
		request.setAttribute("role", role);

		page = "uwAssessmentDetailPage";
	
	}
	catch (UnderWriterException e) {
		LOGGER.error(CodeHelper.getStackTrace(e));
		ActionErrors errors = new ActionErrors();
		errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.request.generic", e.getMessage()));
		saveErrors(request, errors);
		page = "uwAssessmentDetailPage";
	}
	catch (IUMException e) {
		LOGGER.error(CodeHelper.getStackTrace(e));
		ActionErrors errors = new ActionErrors();
		errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.system.exception", e.getMessage()));
		saveErrors(request, errors);
		page = "errorPage";
	}
	catch (Exception e) {
		LOGGER.error(CodeHelper.getStackTrace(e));
		ActionErrors errors = new ActionErrors();
		errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.system.exception", e.getMessage()));
		saveErrors(request, errors);
		page = "errorPage";
	}
	
	LOGGER.info("handleAction end");
	LOGGER.debug("PAGE FORWARD-->" + page);
	return (mapping.findForward(page));
  }

/**
 * @deprecated use FormLoader.loadAssessmentRequest() method
 */
  private UWAssessmentDetailForm populateRequestDetails(UWAssessmentDetailForm reqForm) throws UnderWriterException, IUMException {
	
	LOGGER.info("populateRequestDetails start");
	AssessmentRequest assessmentRequest = new AssessmentRequest();
	AssessmentRequestData requestData = assessmentRequest.getDetails(reqForm.getRefNo());

	LOBData lob = requestData.getLob();
	ClientData insured = requestData.getInsured();
	ClientData owner = requestData.getOwner();
	SunLifeOfficeData branch = requestData.getBranch();
	UserProfileData agent = requestData.getAgent();
	UserProfileData assignedTo = requestData.getAssignedTo();
	UserProfileData underwriter = requestData.getUnderwriter();
	UserProfileData location = requestData.getFolderLocation();
	ClientDataSheetData cds = requestData.getClientDataSheet();
	StatusData statusData = requestData.getStatus();

	NumberFormat nf = NumberFormat.getInstance();
	nf.setMinimumFractionDigits(2);
	nf.setMaximumFractionDigits(2);

	reqForm.setAutoSettleIndicator(requestData.getAutoSettleIndicator());
	reqForm.setTransmitIndicator(requestData.getTransmitIndicator());
	reqForm.setRefNo(reqForm.getRefNo());
	reqForm.setLob(lob.getLOBCode());
	reqForm.setSourceSystem(requestData.getSourceSystem());
	reqForm.setAmountCovered(nf.format(requestData.getAmountCovered()));
	reqForm.setPremium(nf.format(requestData.getPremium()));
	reqForm.setInsuredName(insured.getGivenName() + " " + insured.getLastName());
	reqForm.setBranchCode(branch.getOfficeId());
	reqForm.setBranchName(branch.getOfficeName());
	reqForm.setAgentCode(agent.getUserId());
	reqForm.setAgentName(agent.getFirstName() + " " + agent.getLastName());
	reqForm.setRequestStatus(String.valueOf(statusData.getStatusId()));
	reqForm.setAssignedTo(assignedTo.getUserId());
	reqForm.setUnderwriter(underwriter.getUserId());
	reqForm.setLocation(location.getUserId());

	reqForm.setRemarks(requestData.getRemarks());
	reqForm.setReceivedDate((DateHelper.format(requestData.getApplicationReceivedDate(), "ddMMMyyyy")).toUpperCase());
	reqForm.setDateForwarded((DateHelper.format(requestData.getForwardedDate(), "ddMMMyyyy")).toUpperCase());
	reqForm.setDateEdited((DateHelper.format(requestData.getUpdatedDate(), "ddMMMyyyy")).toUpperCase());
	reqForm.setDateCreated((DateHelper.format(requestData.getCreatedDate(), "ddMMMyyyy")).toUpperCase());
	reqForm.setStatusDate((DateHelper.format(requestData.getStatusDate(), "ddMMMyyyy")).toUpperCase());

	reqForm.setInsuredClientType(cds.getClientType());
	reqForm.setInsuredClientNo(insured.getClientId());
	reqForm.setInsuredLastName(insured.getLastName());
	reqForm.setInsuredFirstName(insured.getGivenName());
	reqForm.setInsuredMiddleName(insured.getMiddleName());
	reqForm.setInsuredTitle(insured.getTitle());
	reqForm.setInsuredSuffix(insured.getSuffix());

	if (insured.getAge() == 0){
		reqForm.setInsuredAge("");
	}else {
		reqForm.setInsuredAge(String.valueOf(insured.getAge()));
	}

	reqForm.setInsuredSex(insured.getSex());
	reqForm.setInsuredBirthDate((DateHelper.format(insured.getBirthDate(), "ddMMMyyyy")).toUpperCase());

	reqForm.setOwnerClientType(cds.getClientType());
	reqForm.setOwnerClientNo(owner.getClientId());
	reqForm.setOwnerLastName(owner.getLastName());
	reqForm.setOwnerFirstName(owner.getGivenName());
	reqForm.setOwnerMiddleName(owner.getMiddleName());
	reqForm.setOwnerTitle(owner.getTitle());
	reqForm.setOwnerSuffix(owner.getSuffix());

	if (owner.getAge() == 0){
		reqForm.setOwnerAge("");
	}else {
		reqForm.setOwnerAge(String.valueOf(owner.getAge()));
	}

	reqForm.setOwnerSex(owner.getSex());
	reqForm.setOwnerBirthDate((DateHelper.format(owner.getBirthDate(), "ddMMMyyyy")).toUpperCase());

	UWAssessmentData uwData = assessmentRequest.getUWAssessmentDetails(reqForm.getRefNo());
	reqForm.setUWRemarks(uwData.getRemarks());
	reqForm.setListOfAnalysis(populateUWAnalysis(reqForm.getRefNo()));

	LOGGER.info("populateRequestDetails end");
	return (reqForm);
  }

  private ArrayList populateUWAnalysis(String referenceNumber) throws IUMException{
	  
	LOGGER.info("populateUWAnalysis start");
	ArrayList listOfAnalysis = new ArrayList();
	AssessmentRequest ar = new AssessmentRequest();
	ArrayList list = ar.retrieveUWAnalysis(referenceNumber);
	for (int i=0; i<list.size(); i++){
		UWAnalysisForm analysis = new UWAnalysisForm();
		UWAssessmentData data = (UWAssessmentData) list.get(i);
		analysis.setReferenceNumber(data.getReferenceNumber());
		analysis.setAnalysis(ValueConverter.newLineToHTML(data.getAnalysis()));
		analysis.setPostedBy(data.getPostedBy());
		analysis.setPostedDate(DateHelper.format(data.getPostedDate(), "ddMMMyyyy HH:mm").toUpperCase());
		listOfAnalysis.add(analysis);
	}
	LOGGER.info("populateUWAnalysis end");
	return listOfAnalysis;
  }

  private ArrayList populateImpairments(String refNo, String clientType) throws UnderWriterException, IUMException {
	
	LOGGER.info("populateImpairments start");
	Impairment impairment = new Impairment();
	ArrayList impList = impairment.getImpairmentList(refNo, clientType);

	int size = 0;
	ArrayList impairments = new ArrayList();

	NumberFormat nf = NumberFormat.getInstance();
	nf.setMinimumFractionDigits(0);
	nf.setMaximumFractionDigits(2);

	for (int i = 0; i < size; i++) {
	  ImpairmentData impData = (ImpairmentData)impList.get(i);
	  ImpairmentForm impForm = new ImpairmentForm();

	  impForm.setPolicyNo(impData.getReferenceNumber());
	  impForm.setCode(String.valueOf(impData.getImpairmentCode()));
	  impForm.setDescription(impData.getImpairmentDesc());
	  impForm.setRelationship(impData.getRelationship());
	  impForm.setImpairmentOrigin(impData.getImpairmentOrigin());
	  impForm.setImpairmentDate((DateHelper.format(impData.getImpairmentDate(), "ddMMMyyyy")).toUpperCase());

	  if (impData.getHeightInFeet() == 0){
	  	impForm.setHeightInFeet("");
	  } else {
		impForm.setHeightInFeet(nf.format(impData.getHeightInFeet()));
	  }

	  if (impData.getHeightInInches() == 0){
	  	impForm.setHeightInFeet("");
	  } else {
		impForm.setHeightInInches(nf.format(impData.getHeightInInches()));
	  }

	  if (impData.getWeight() == 0){
	  	impForm.setWeight("");
	  } else {
		impForm.setWeight(nf.format(impData.getWeight()));
	  }

	  if (impData.getBloodPressure() == null){
	  	impForm.setBloodPressure("");
	  }else {
		impForm.setBloodPressure(String.valueOf(impData.getBloodPressure()));
	  }

	  impForm.setConfirm(impData.getConfirmation());
	  impForm.setExportDate((DateHelper.format(impData.getExportDate(), "ddMMMyyyy")).toUpperCase());
	  impForm.setImpairmentId(String.valueOf(impData.getImpairmentId()));
	  impairments.add(impForm);
	}

	LOGGER.info("populateImpairments end");
	return (impairments);
  }


  private ImpairmentForm setImpairment(ImpairmentData impData){
	  
	LOGGER.info("setImpairment start");
  	ImpairmentForm form = new ImpairmentForm();
	NumberFormat nf = NumberFormat.getInstance();
	nf.setMinimumFractionDigits(0);
	nf.setMaximumFractionDigits(2);

  	form.setCode(impData.getImpairmentCode());
	form.setRelationship(impData.getRelationship());
	form.setImpairmentOrigin(impData.getImpairmentOrigin());
	form.setImpairmentDate((DateHelper.format(impData.getImpairmentDate(), "ddMMMyyyy")).toUpperCase());

	if (impData.getHeightInFeet() == 0){
		form.setHeightInFeet("");
	} else {
	   form.setHeightInFeet(nf.format(impData.getHeightInFeet()));
	}

	if (impData.getHeightInInches() == 0){
	   form.setHeightInFeet("");
	} else {
	   form.setHeightInInches(nf.format(impData.getHeightInInches()));
	}

	if (impData.getWeight() == 0){
		form.setWeight("");
	}else {
		form.setWeight(nf.format(impData.getWeight()));
	}

	if (impData.getBloodPressure() == null){
		form.setBloodPressure("");
	}else {
		form.setBloodPressure(impData.getBloodPressure());
	}

	form.setConfirm(impData.getConfirmation());
	form.setImpairmentId(String.valueOf(impData.getImpairmentId()));

	LOGGER.info("setImpairment end");
  	return form;
  }


  private void retrieveInsuredClientData(UWAssessmentDetailForm arf) throws IUMException{
	  
	  LOGGER.info("retrieveInsuredClientData start");
		  arf.setInsuredClientNo(arf.getInsuredClientNo().toUpperCase());
		  AssessmentRequest ar = new AssessmentRequest();
		  ClientData cdata = ar.getClientData(arf.getInsuredClientNo());
		  if(cdata!=null&&cdata.getGivenName()!=null && !cdata.getGivenName().equals("")){
			  if (cdata.getAge() == 0){
			  	arf.setInsuredAge("");
			  } else{
				arf.setInsuredAge( String.valueOf(cdata.getAge()) );
			  }

			  if(cdata.getBirthDate()!=null){
				  arf.setInsuredBirthDate( (DateHelper.format( cdata.getBirthDate(),"ddMMMyyyy" ) ).toUpperCase());
			  }else{
				  arf.setInsuredBirthDate("");
			  }
			  if(cdata.getGivenName()!=null){
				  arf.setInsuredFirstName( cdata.getGivenName() );
			  }else{
				  arf.setInsuredFirstName( "" );
			  }
			  if(cdata.getLastName()!=null){
				  arf.setInsuredLastName( cdata.getLastName() );
			  }else{
				  arf.setInsuredLastName( "" );
			  }
			  if(cdata.getMiddleName()!=null){
				  arf.setInsuredMiddleName( cdata.getMiddleName() );
			  }else{
				  arf.setInsuredMiddleName( "" );
			  }
			  if(cdata.getTitle()!=null){
				  arf.setInsuredTitle( cdata.getTitle() );
			  }else{
				  arf.setInsuredTitle( "" );
			  }
			  if(cdata.getSex()!=null){
				  arf.setInsuredSex( cdata.getSex() );
			  }else{
				  arf.setInsuredSex( "" );
			  }
			  if(cdata.getSuffix()!=null){
				  arf.setInsuredSuffix( cdata.getSuffix() );
			  }else{
				  arf.setInsuredSuffix( "" );
			  }
			  arf.setInsuredOriginal( arf.getInsuredClientNo()+
									  arf.getInsuredFirstName()+
									  arf.getInsuredLastName()+
									  arf.getInsuredMiddleName()+
									  arf.getInsuredTitle()+
									  arf.getInsuredSuffix()+
									  arf.getInsuredSex());
		  }else{
			  arf.setInsuredAge("");
			  arf.setInsuredFirstName("");
			  arf.setInsuredMiddleName("");
			  arf.setInsuredLastName("");
			  arf.setInsuredBirthDate("");
			  arf.setInsuredSuffix("");
			  arf.setInsuredTitle("");
			  arf.setInsuredSex("");
			  arf.setInsuredOriginal("");
		  }
		  LOGGER.info("retrieveInsuredClientData end");
	  }

	private void retrieveOwnerClientData(UWAssessmentDetailForm arf)throws IUMException{
		
		LOGGER.info("retrieveOwnerClientData start");
			arf.setOwnerClientNo(arf.getOwnerClientNo().toUpperCase());
			AssessmentRequest ar = new AssessmentRequest();
			ClientData cdata = ar.getClientData(arf.getOwnerClientNo());
			if(cdata!=null&&cdata.getGivenName()!=null && !cdata.getGivenName().equals("")){

				if (cdata.getAge() == 0){
					arf.setOwnerAge("");
				}else {
					arf.setOwnerAge( String.valueOf(cdata.getAge()) );
				}

				if(cdata.getBirthDate()!=null){
					arf.setOwnerBirthDate( (DateHelper.format( cdata.getBirthDate(),"ddMMMyyyy" ) ).toUpperCase());
				}else{
					arf.setOwnerBirthDate("");
				}
				if(cdata.getGivenName()!=null){
					arf.setOwnerFirstName( cdata.getGivenName() );
				}else{
					arf.setOwnerFirstName( "" );
				}
				if(cdata.getLastName()!=null){
					arf.setOwnerLastName( cdata.getLastName() );
				}else{
					arf.setOwnerLastName( "" );
				}
				if(cdata.getMiddleName()!=null){
					arf.setOwnerMiddleName( cdata.getMiddleName() );
				}else{
					arf.setOwnerMiddleName( "" );
				}
				if(cdata.getTitle()!=null){
					arf.setOwnerTitle( cdata.getTitle() );
				}else{
					arf.setOwnerTitle( "" );
				}
				if(cdata.getSex()!=null){
					arf.setOwnerSex( cdata.getSex() );
				}else{
					arf.setOwnerSex( "" );
				}
				if(cdata.getSuffix()!=null){
					arf.setOwnerSuffix( cdata.getSuffix() );
				}else{
					arf.setOwnerSuffix( "" );
				}
				arf.setOwnerOriginal( arf.getOwnerClientNo()+
										arf.getOwnerFirstName()+
										arf.getOwnerLastName()+
										arf.getOwnerMiddleName()+
										arf.getOwnerTitle()+
										arf.getOwnerSuffix()+
										arf.getOwnerSex());

			}else{
				arf.setOwnerAge("");
				arf.setOwnerFirstName("");
				arf.setOwnerMiddleName("");
				arf.setOwnerLastName("");
				arf.setOwnerBirthDate("");
				arf.setOwnerSuffix("");
				arf.setOwnerTitle("");
				arf.setOwnerSex("");
				arf.setOwnerOriginal("");
			}
			LOGGER.info("retrieveOwnerClientData end");
		}

}