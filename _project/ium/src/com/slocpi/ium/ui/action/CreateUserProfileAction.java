package com.slocpi.ium.ui.action;

// struts package
import java.io.IOException;
import java.util.Date;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.opensymphony.oscache.base.Cache;
import com.slocpi.ium.IUMException;
import com.slocpi.ium.data.UserAccessData;
import com.slocpi.ium.data.UserData;
import com.slocpi.ium.data.UserProfileData;
import com.slocpi.ium.ui.form.UserProfileForm;
import com.slocpi.ium.ui.util.StateHandler;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.UserManager;

/**
 * @TODO Class Description CreateImpairmentAction
 * @author: Ingrid Villanueva
 * @version: 1.0
 */
public class CreateUserProfileAction extends IUMAction {
	private static final Logger LOGGER = LoggerFactory.getLogger(CreateUserProfileAction.class);
	/**
	 * This method handles the processes of CreateUserProfileAction class. It
	 * uses the extractAccessPriveleges and saveUserProfileRecord methods to
	 * create and save a new user to the DB.
	 * 
	 * @param mapping
	 *            mapping of request to an instance of this class
	 * @param form
	 *            object
	 * @param request
	 *            request object
	 * @param response
	 *            response object
	 * @exception IOException
	 *                if an input/output error occurs
	 * @exception ServletException
	 *                if a servlet exception occurs
	 * @return ActionForward as defined in the mapping parameter.
	 */
	public ActionForward handleAction(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		
		LOGGER.info("handleAction start");
		String page = "";
		try {

			StateHandler sh = new StateHandler();
			UserData ud = sh.getUserData(request);
			UserProfileData profile = ud.getProfile();
			String userId = profile.getUserId();
			String pageId = "";
			UserAccessData uad = extractAccessPriveleges(userId, pageId);

			if (uad.getAccessCode().equals("W")) {

				String mode = request.getParameter("mode");
				UserProfileForm userForm = (UserProfileForm) form;
				saveUserProfileRecord(userForm, mode, userId);

				ServletContext app = getServlet().getServletContext();
				Cache cache = (Cache) app.getAttribute("ium_cache");
				if (cache != null) {
					cache.flushGroup("searchGroup");
				}

				if (mode != null && mode.equals("add")) {
					page = "resetPassword"; 
				} else {
					page = "userProfileList";
				}
			} else {
				page = "errorPage";
			}
		}
		catch (IUMException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			page = "userProfileDetailPage";
			UserProfileForm userForm = (UserProfileForm) form;
			request.setAttribute("userProfileForm", userForm);
			ActionErrors errors = new ActionErrors();
			if (e.getMessage().equalsIgnoreCase("Duplicate User Error.")) {
				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError(
						"error.duplicateUser", userForm.getUserId()));
			} else {
				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError(
						"error.system.exception", e.getMessage()));
			}
			saveErrors(request, errors);
		}
		LOGGER.info("handleAction end");
		LOGGER.debug("PAGE FORWARD-->" + page);
		
		return (mapping.findForward(page));
	}// execute

	private void saveUserProfileRecord(UserProfileForm userForm, String mode,
			String userId) throws IUMException {
		
		LOGGER.info("saveUserProfileRecord end");
		UserProfileData data = new UserProfileData();

		data.setUserId(userForm.getUserId());
		data.setACF2ID(userForm.getAcf2Id());

		data.setUserType(userForm.getUserType());
		data.setUserCode(userForm.getUserCode());
		if (userForm.getActivated().equalsIgnoreCase("Y"))
			data.setActive(true);
		else
			data.setActive(false);

		if (userForm.getLocked().equalsIgnoreCase("Y"))
			data.setLock(true);
		else
			data.setLock(false);

		data.setLastName(userForm.getLastName());
		data.setFirstName(userForm.getFirstName());
		data.setMiddleName(userForm.getMiddleName());

		data.setSex(userForm.getSex());

		data.setEmailAddr(userForm.getEmailAddr());

		data.setContactNum(userForm.getContactNo());
		data.setMobileNumber(userForm.getMobileNo());

		data.setOfficeCode(userForm.getBranchName());

		data.setDeptCode(userForm.getDepartment());
		data.setSectionCode(userForm.getSection());

		data.setRole(userForm.getDefaultRole());

		data.setNotificationInd(true);

		data.setRecordsPerView(10);

		data.setAddress1(userForm.getAddr1());
		data.setAddress2(userForm.getAddr2());
		data.setAddress3(userForm.getAddr3());
		data.setCity(userForm.getCity());
		data.setCountry((userForm.getCountry()));
		data.setZipCode(userForm.getZipCode());

		if (mode != null) {
			if (mode.equals("add")) {
				data.setCreateDate(new Date());
				data.setCreatedBy(userId);
			}
		} else {
			data.setUpdateDate(new Date());
			data.setUpdatedBy(userId);
		}

		UserManager usrmgr = new UserManager();
		

		boolean isVldUsr = false; 
		/***********************************************************************
		 * Comment by: Rommel A. Suarez Date: Dec 05 2008 Purpose: To bypass
		 * Ldap validation during profile creation
		 * 
		 * LDAPService ls = null; try { ls = LDAPService.getInstance(); } catch
		 * (Exception e1) { // TODO Auto-generated catch block
		 *  // LDAPService ls =
		 * LDAPService.getInstance(); boolean isVldUsr = true;
		 * 
		 * 
		 * try { isVldUsr = ls.isLdapUser(userForm.getUserId()); } catch
		 * (IUMInterfaceException e) { debug("LDAPService error");
		 * e.printStackTrace(); }
		 * 
		 **********************************************************************/
		
		if (mode != null) {
			if (mode.equals("add")) {
				LOGGER.debug("isVldUsr: " + isVldUsr);
					UserData checkExist = usrmgr.retrieveUserDetails(data
							.getUserId());
					if (checkExist.getProfile() != null) {
						LOGGER.error("Duplicate User Error.");
						throw new IUMException("Duplicate User Error.");
					} else {
						usrmgr.insertUserProfileData(data);
					}
			}
		} else {
			usrmgr.updateUserProfileData(data);
		}
		LOGGER.info("saveUserProfileRecord end");
	}

}
