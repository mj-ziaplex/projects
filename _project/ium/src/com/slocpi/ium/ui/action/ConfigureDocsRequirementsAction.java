
package com.slocpi.ium.ui.action;

// struts package
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Collection;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.slocpi.ium.data.dao.DocumentRequirementsDAO;
import com.slocpi.ium.data.util.DataSourceProxy;
import com.slocpi.ium.ui.form.DocumentsRequirementsForm;
import com.slocpi.ium.util.CodeHelper;



public class ConfigureDocsRequirementsAction extends IUMAction {
  
	private static final Logger LOGGER = LoggerFactory.getLogger(ConfigureDocsRequirementsAction.class);
  public ActionForward handleAction(ActionMapping mapping, 
							   ActionForm form, 
							   HttpServletRequest request, 
							   HttpServletResponse response)
							   throws Exception {
			   	
	LOGGER.info("handleAction start");
	String page = "refresh1";		
	String actionType = request.getParameter("actionType");
	DocumentsRequirementsForm docsRequirementsForm = (DocumentsRequirementsForm) form;		

	if (actionType != null && !"".equals(actionType)) {
		if (actionType.equals("refresh")) {
			docsRequirementsForm = fillContent(docsRequirementsForm);			
			request.setAttribute("docsRequirementsForm",docsRequirementsForm);
		} else if (actionType.equals("addDocType")) {
			insertDocType(docsRequirementsForm);
			docsRequirementsForm = fillContent(docsRequirementsForm);			
			request.setAttribute("docsRequirementsForm",docsRequirementsForm);
		} else if (actionType.equals("addRequirement")) {
			insertRequirement(docsRequirementsForm);
			docsRequirementsForm = fillContent(docsRequirementsForm);			
			request.setAttribute("docsRequirementsForm",docsRequirementsForm);
		}
		
	}	
	LOGGER.info("handleAction end");
	LOGGER.debug("PAGE FORWARD-->" + page);
	return (mapping.findForward(page));
  }
	
		
	
	/**
	 * @param docsRequirementsForm
	 */
	private void insertRequirement(DocumentsRequirementsForm docsRequirementsForm) throws SQLException {
		
		LOGGER.info("insertRequirement start");
		Connection conn = null;
		conn=new DataSourceProxy().getConnection();
		try{
			DocumentRequirementsDAO reqDAO = new DocumentRequirementsDAO(conn);
			conn.setAutoCommit(false);
			reqDAO.insertRequirement(docsRequirementsForm);
			conn.commit();
		} catch (SQLException e){
			LOGGER.error(CodeHelper.getStackTrace(e));
			conn.rollback();
		}finally{
			closeConnection(conn);
		}			
		LOGGER.info("insertRequirement end");
	}

	private void closeConnection(Connection conn)throws SQLException{
		
		
	  if(conn!=null){
		  conn.close();
	  } 
	  
	}


	/**
	 * @param docsRequirementsForm
	 */
	private void insertDocType(DocumentsRequirementsForm docsRequirementsForm) throws SQLException {
		
		LOGGER.info("insertDocType start");
		Connection conn = null;
		try{
			conn=new DataSourceProxy().getConnection();
			conn.setAutoCommit(false);
			DocumentRequirementsDAO docDAO = new DocumentRequirementsDAO(conn);
			docDAO.insertDocType(docsRequirementsForm);
			conn.commit();
		}
		catch (SQLException e){
			if (conn != null) {
				conn.rollback();
			}

			LOGGER.error(CodeHelper.getStackTrace(e));
		}finally{
			closeConnection(conn);
		}			
		LOGGER.info("insertDocType end");
	}



/**
	 * @param docsRequirementsForm
	 * @return
	 */
	private DocumentsRequirementsForm fillContent(DocumentsRequirementsForm docsRequirementsForm) throws SQLException {
		
		LOGGER.info("DocumentsRequirementsForm start");
		Connection conn = null;
		try{
			conn=new DataSourceProxy().getConnection();
			DocumentRequirementsDAO docsRequirementsDAO = new DocumentRequirementsDAO(conn);
			String lob = docsRequirementsForm.getLob();
			Collection docTypes = docsRequirementsDAO.getLobDocTypes(lob);
			docsRequirementsForm.setDocTypes(docTypes);
			Collection requirements = docsRequirementsDAO.getRequirements(lob);
			docsRequirementsForm.setRequirements(requirements);
		}finally{
			closeConnection(conn);
		}		
		LOGGER.info("DocumentsRequirementsForm end");
		return docsRequirementsForm;		
	}





    
}

