package com.slocpi.ium.ui.action;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.ResourceBundle;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.slocpi.ium.IUMException;
import com.slocpi.ium.data.AssessmentRequestData;
import com.slocpi.ium.data.LOBData;
import com.slocpi.ium.data.PolicyRequirementsData;
import com.slocpi.ium.data.StatusData;
import com.slocpi.ium.data.UserAccessData;
import com.slocpi.ium.data.UserData;
import com.slocpi.ium.data.UserProfileData;
import com.slocpi.ium.data.dao.RequirementDAO;
import com.slocpi.ium.data.util.DataSourceProxy;
import com.slocpi.ium.ui.form.PolicyRequirementsForm;
import com.slocpi.ium.ui.util.Page;
import com.slocpi.ium.ui.util.Pagination;
import com.slocpi.ium.ui.util.StateHandler;
import com.slocpi.ium.underwriter.PolicyRequirements;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.DateHelper;
import com.slocpi.ium.util.IUMConstants;
import com.slocpi.ium.util.ValueConverter;

/**
 * This class is responsible for displaying the IUM's list of requirements.
 * This class will display the list and allow the viewing of a specific requirement.
 * @author Engel
 * 
 */
public class CreatePolicyRequirementsAction extends IUMAction {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(CreatePolicyRequirementsAction.class);

	/* (non-Javadoc)
	 * @see com.slocpi.ium.ui.action.IUMAction#handleAction(org.apache.struts.action.ActionMapping, org.apache.struts.action.ActionForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	public ActionForward handleAction(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response)
		throws Exception {
		
		LOGGER.info("handleAction start");
		
		final long start = System.currentTimeMillis();
		long end;
		long elapse;
		end = System.currentTimeMillis();
		elapse = (end- start);
		String refNo = replaceNull(request.getParameter("refNo"));
		
		String sessionId = replaceNull(request.getParameter("sessionId"));
		String sourceSystem = replaceNull(request.getParameter("sourceSystem"));
		
		String mode = replaceNull(request.getParameter("mode"));
		String arFormRefNo = replaceNull(request.getParameter("arFormRefNo"));
		String policySuffix = replaceNull(request.getParameter("policySuffix"));
		
		String lob = replaceNull(request.getParameter("lob"));
		String arFormOwnerClientNo = replaceNull(request.getParameter("arFormOwnerClientNo"));
		String arFormInsuredClientNo = replaceNull(request.getParameter("arFormInsuredClientNo"));
		String agentCode = replaceNull(request.getParameter("agentCode"));
		String branchCode = replaceNull(request.getParameter("branchCode"));
		String assignedTo = replaceNull(request.getParameter("assignedTo"));		
		String currentStatus = replaceNull(request.getParameter("currentStatus"));
		
		String EXECUTE_KO = replaceNull(request.getParameter("EXECUTE_KO"));
		String sendButtonControl = replaceNull(request.getParameter("sendButtonControl"));
		String pageNumberParam = replaceNull(request.getParameter("pageNumber"));
		
		String strNBStrucNotesSettleDate = replaceNull((String) request.getSession().getAttribute("strNBStrucNotesSettleDate"));
		
		end = System.currentTimeMillis();
		elapse = (end- start);
		
		
		if(refNo.equals("")){
			refNo = arFormRefNo;
		}
		
		String page = "";
		String viewPage = "viewRequirements";
		String createPage = "createRequirements";
		
		PolicyRequirementsForm reqfrm = (PolicyRequirementsForm)form;
		StateHandler sessionHandler = new StateHandler();
		UserData userData = sessionHandler.getUserData(request);
		UserProfileData userPref = userData.getProfile();
		
		String userId = userPref.getUserId();
		String pageNumber = reqfrm.getPageNumber();
		
		if(mode.equals("") || mode.equals("saveUpdate") || mode.equals("update")){
			
			page = viewPage;			
			if(pageNumber==null || pageNumber.equals("")){
				pageNumber="1";
			}

			if (mode.equals("update")) {
				
				if( reqfrm!=null && reqfrm.getReqId()!=null && !reqfrm.getReqId().equals("") ){
					
					reqfrm = populateRequirementDetails(Long.parseLong(reqfrm.getReqId()));
					
					if(!pageNumberParam.equals("")){
						pageNumber = pageNumberParam;
						reqfrm.setPageNumber(pageNumber);
					}
				}
			}else if (mode.equals("saveUpdate")) {
				
				updateRequirementRecord(reqfrm, refNo,  userId,  lob, sourceSystem);
				reqfrm = populateRequirementDetails(Long.parseLong(reqfrm.getReqId()));
			}

			ArrayList arrForm = new ArrayList();
			arrForm = populateRequirements(refNo);
			
			int recPerPage = 20;
			Pagination pgn = new Pagination(arrForm,recPerPage);
			Page pageObj = pgn.getPage(Integer.parseInt(pageNumber));
			
			request.setAttribute("pageRecord", pageObj);
			request.setAttribute("requirement",reqfrm);
			request.setAttribute("lob", lob);
			request.setAttribute("currentStatus", currentStatus);
			request.setAttribute("arFormRequestStatus", currentStatus);
			request.setAttribute("arFormRefNo", arFormRefNo);
			request.setAttribute("arFormOwnerClientNo", arFormOwnerClientNo);
			request.setAttribute("arFormInsuredClientNo", arFormInsuredClientNo);
			request.setAttribute("sessionId", sessionId);
			request.setAttribute("branchCode", branchCode);
			request.setAttribute("agentCode", agentCode);
			request.setAttribute("assignedTo", assignedTo);
			request.setAttribute("sourceSystem", sourceSystem);
			request.setAttribute("EXECUTE_KO", EXECUTE_KO);
			request.setAttribute("sendButtonControl", sendButtonControl);
			request.setAttribute("policySuffix", policySuffix);
			
		}else if (mode.equals("create")){
			
			page = createPage;
			ArrayList requirementsList = getRequirementsList();
			
			request.setAttribute("requirementList",requirementsList);
			request.setAttribute("lob", lob);
			request.setAttribute("currentStatus", currentStatus);
			request.setAttribute("arFormRequestStatus", currentStatus);
			request.setAttribute("arFormRefNo", arFormRefNo);
			request.setAttribute("arFormOwnerClientNo", arFormOwnerClientNo);
			request.setAttribute("arFormInsuredClientNo", arFormInsuredClientNo);
			request.setAttribute("sessionId", sessionId);
			request.setAttribute("branchCode", branchCode);
			request.setAttribute("agentCode", agentCode);
			request.setAttribute("assignedTo", assignedTo);
			request.setAttribute("sourceSystem", sourceSystem);
			request.setAttribute("EXECUTE_KO", EXECUTE_KO);
			request.setAttribute("sendButtonControl", sendButtonControl);
			request.setAttribute("policySuffix", policySuffix);
			
		}else if(mode.equals("saveCreate")){
			
			String pageId = IUMConstants.PAGE_CDS;
			UserAccessData uad = extractAccessPriveleges(userId, pageId);
			
			if (uad.getAccessCode().equals("W")) {
				
				PolicyRequirementsForm reqfrmTmp;
				
				String[] reqCodeArr = reqfrm.getReqCodeArr();
				String reqCode = null;
				String[] seqArr = reqfrm.getSeqArr();
				String seq = null;
				String[] reqLevelArr = reqfrm.getReqLevelArr();
				String reqLevel = null;
				String[] designationArr = reqfrm.getDesignationArr();
				String designation = null;
				String[] validityDateArr = reqfrm.getValidityDateArr();
				String validityDate = null;
				String[] testDateArr = reqfrm.getTestDateArr();
				String testDate = null;
				String[] followUpNoArr = reqfrm.getFollowUpNoArr();
				String followUpNo = null;
				String[] resolveArr = reqfrm.getResolveArr();
				String resolve = null;
				String[] paidIndArr = reqfrm.getPaidIndArr();
				String paidInd = null;
				String[] newTestArr = reqfrm.getNewTestArr();
				String newTest = null;
				String[] autoOrderArr = reqfrm.getAutoOrderArr();
				String autoOrder = null;
				String[] fldCommArr = reqfrm.getFldCommArr();
				String fldComm = null;
				String[] commentArr = reqfrm.getCommentArr();
				String comment = null;
				String[] clientTypeArr = reqfrm.getClientTypeArr();
				String clientType = null;
				String[] reqFollowUpArr = reqfrm.getReqFollowUpArr();
				String reqFollowUp = null;
				
				for(int i = 0; i < reqCodeArr.length; i++){
					
					reqfrmTmp = new PolicyRequirementsForm();
					reqfrmTmp.setRefNo(refNo);
					if (IUMConstants.CLIENT_TYPE_OWNER.equalsIgnoreCase(clientTypeArr[i])){
						reqfrmTmp.setClientNo(arFormOwnerClientNo);
					}
					else{
						reqfrmTmp.setClientNo(arFormInsuredClientNo);
					}
					
					try{
						
						try{
							reqCode = reqCodeArr[i];
						}catch(Exception e){
							LOGGER.error(CodeHelper.getStackTrace(e));
						}
						try{
							seq = seqArr[i];
						}catch(Exception e){
							LOGGER.error(CodeHelper.getStackTrace(e));
						}
						try{
							reqLevel = reqLevelArr[i];
						}catch(Exception e){
							LOGGER.error(CodeHelper.getStackTrace(e));
						}
						try{
							designation = designationArr[i];
						}catch(Exception e){
							LOGGER.error(CodeHelper.getStackTrace(e));
						}
						try{
							validityDate = validityDateArr[i];
						}catch(Exception e){
							LOGGER.error(CodeHelper.getStackTrace(e));
						}
						try{
							testDate = testDateArr[i];
						}catch(Exception e){
							LOGGER.error(CodeHelper.getStackTrace(e));
						}
						try{
							followUpNo = followUpNoArr[i];
						}catch(Exception e){
							LOGGER.error(CodeHelper.getStackTrace(e));
						}
						try{
							resolve = resolveArr[i];
						}catch(Exception e){
							LOGGER.error(CodeHelper.getStackTrace(e));
						}
						try{
							paidInd = paidIndArr[i];
						}catch(Exception e){
							LOGGER.error(CodeHelper.getStackTrace(e));
						}
						try{
							newTest = newTestArr[i];
						}catch(Exception e){
							LOGGER.error(CodeHelper.getStackTrace(e));
						}
						try{
							autoOrder = autoOrderArr[i];
						}catch(Exception e){
							LOGGER.error(CodeHelper.getStackTrace(e));
						}
						try{
							fldComm = fldCommArr[i];
						}catch(Exception e){
							LOGGER.error(CodeHelper.getStackTrace(e));
						}
						try{
							comment = commentArr[i];
							if(reqCode.equals("EDD") && i==0){
								comment = commentArr[i+1];
							}
							else if(i!=0){
								comment = commentArr[i*2];	
								if(reqCode.equals("EDD")){
									comment = commentArr[(i*2)+1];
								}
							}						
						}catch(Exception e){
							LOGGER.error(CodeHelper.getStackTrace(e));
						}
						try{
							clientType = clientTypeArr[i];
						}catch(Exception e){
							LOGGER.error(CodeHelper.getStackTrace(e));
						}	
						try{
							reqFollowUp = reqFollowUpArr[i];
						}catch(Exception e){
							LOGGER.error(CodeHelper.getStackTrace(e));
						}
					
						reqfrmTmp.setReqCode(reqCode);
						reqfrmTmp.setReqCodeValue(reqCode);
						reqfrmTmp.setReqLevel(reqLevel);
						reqfrmTmp.setSeq(seq);
						reqfrmTmp.setReqStatusCode("ORD");
						reqfrmTmp.setReqStatusDesc("ORDERED");
						reqfrmTmp.setReqStatusValue("13");
						reqfrmTmp.setStatusDate("");
						reqfrmTmp.setDesignation(designation);
						reqfrmTmp.setValidityDate(validityDate);
						reqfrmTmp.setTestDate(testDate);
						reqfrmTmp.setFollowUpNo(followUpNo);
						reqfrmTmp.setResolve(resolve);
						reqfrmTmp.setPaidInd(paidInd);
						reqfrmTmp.setNewTest(newTest);
						reqfrmTmp.setAutoOrder(autoOrder);
						reqfrmTmp.setFldComm(fldComm);
						reqfrmTmp.setComment(comment);
						reqfrmTmp.setClientType(clientType);
						reqfrmTmp.setReqFollowUp(reqFollowUp);
					}catch(Exception e){
						LOGGER.error(CodeHelper.getStackTrace(e));
					}
					
					if(reqCode!=null && !reqCode.equals("")){
						
						long reqId = saveRequirementRecord(reqfrmTmp, refNo, userId, lob,
								currentStatus, sourceSystem, strNBStrucNotesSettleDate);
					}
				}
			} else {
				page = "errorPage";
			}

			request.setAttribute("lob", lob);
			request.setAttribute("currentStatus", currentStatus);
			request.setAttribute("arFormRequestStatus", currentStatus);
			request.setAttribute("arFormRefNo", arFormRefNo);
			request.setAttribute("arFormOwnerClientNo", arFormOwnerClientNo);
			request.setAttribute("arFormInsuredClientNo", arFormInsuredClientNo);
			request.setAttribute("sessionId", sessionId);
			request.setAttribute("branchCode", branchCode);
			request.setAttribute("agentCode", agentCode);
			request.setAttribute("assignedTo", assignedTo);
			request.setAttribute("sourceSystem", sourceSystem);
			request.setAttribute("EXECUTE_KO", EXECUTE_KO);
			request.setAttribute("sendButtonControl", sendButtonControl);
			request.setAttribute("policySuffix", policySuffix);
			
			page = "saveCreateRequirements";
		}
		request.setAttribute("systemDate",DateHelper.format(new Date(), DateHelper.MM_DDD_YYYY_PATTER));
		end = System.currentTimeMillis();
		elapse = (end- start);
		
		PolicyRequirements polReq = new PolicyRequirements();
		boolean hasOrderReqt = polReq.hasOrderRequirements(arFormRefNo);
		String ordReq = "";
		if(hasOrderReqt){
			ordReq = "YES";
		}
		
		end = System.currentTimeMillis();
		elapse = (end- start);
		
		request.setAttribute("hasOrderReqt", ordReq);
		
		reqfrm.setPageNumber(pageNumber);
		reqfrm.setOrderRequirementsTag(ordReq);
		
		
		end = System.currentTimeMillis();
		elapse = (end- start);
		LOGGER.info("handleAction end");
		LOGGER.debug("PAGE FORWARD-->" + page);
		return (mapping.findForward(page));
	}

	private ArrayList populateRequirements(final String refNo) throws Exception {
		
		LOGGER.info("populateRequirements start");
		ArrayList requirements = new ArrayList();

		PolicyRequirements pr = new PolicyRequirements();
		ArrayList reqList = pr.getRequirements(refNo);
		
		if (reqList != null && !reqList.isEmpty()) {
			
			Iterator it = reqList.iterator();
			while (it.hasNext()) {
				
				PolicyRequirementsData reqData =
						(PolicyRequirementsData) it.next();
				
				PolicyRequirementsForm reqForm = new PolicyRequirementsForm();
				
				reqForm.setRefNo(reqData.getReferenceNumber());
				reqForm.setReqId(String.valueOf(reqData.getRequirementId()));
				reqForm.setSeq(String.valueOf(reqData.getSequenceNumber()));
				reqForm.setReqCode(reqData.getRequirementCode());
				
				StatusData stat = reqData.getStatus();
				reqForm.setReqStatusCode(String.valueOf(stat.getStatusId()));
				reqForm.setReqStatusDesc(stat.getStatusDesc());
				
				reqForm.setReqLevel(reqData.getLevel());
				reqForm.setStatusDate(
						(DateHelper.format(reqData.getStatusDate(), "ddMMMyyyy")).toUpperCase());
				reqForm.setComment(reqData.getComments());
				reqForm.setClientType(reqData.getClientType());
				reqForm.setReqLevel(reqData.getLevel());
				reqForm.setFollowUpDate(
						(DateHelper.format(reqData.getFollowUpDate(), "ddMMMyyyy")).toUpperCase());
				reqForm.setFollowUpNo(String.valueOf(reqData.getFollowUpNumber()));
				reqForm.setDateSent(
						(DateHelper.format(reqData.getDateSent(), "ddMMMyyyy")).toUpperCase());
				reqForm.setClientNo(reqData.getClientId());
				reqForm.setTestDate(
						(DateHelper.format(reqData.getTestDate(), "ddMMMyyyy")).toUpperCase());
				reqForm.setImageReferenceNum(reqData.getImageRef());

				
				String fnURL = getWMSImageLink(reqData);
				reqForm.setWmsImageLink(fnURL);

				requirements.add(reqForm);
			}
		}
		LOGGER.info("populateRequirements end");
		
		return (requirements);
	}

	private PolicyRequirementsForm populateRequirementDetails(long reqId) throws Exception {
		
		LOGGER.info("populateRequirementDetails start");
		
		PolicyRequirements pr = new PolicyRequirements();
		PolicyRequirementsData reqData = pr.getRequirement(reqId);
		PolicyRequirementsForm reqForm = new PolicyRequirementsForm();
		if (reqData != null) {
			reqForm.setRefNo(reqData.getReferenceNumber());
			reqForm.setReqId(Long.toString(reqId));
			reqForm.setReqCode(reqData.getRequirementCode());
			reqForm.setSeq(String.valueOf(reqData.getSequenceNumber()));
			reqForm.setReqDesc(reqData.getReqDesc());
			reqForm.setReqLevel(reqData.getLevel());
			reqForm.setCreatedDate((DateHelper.format(reqData.getCreateDate(),
					"ddMMMyyyy")).toUpperCase());
			reqForm.setCompleteReq(String.valueOf(reqData
					.isCompletedRequirementInd()));			
			if (reqData.getStatus() != null) {
				reqForm.setReqStatusCode(String.valueOf(reqData.getStatus()
						.getStatusDesc()));
				reqForm.setReqStatusValue(String.valueOf(reqData.getStatus()
						.getStatusId()));
			}
			reqForm.setStatusDate((DateHelper.format(reqData.getStatusDate(),
					"ddMMMyyyy")).toUpperCase());
			reqForm.setUpdatedBy(reqData.getUpdatedBy());
			reqForm.setUpdatedDate((DateHelper.format(reqData.getUpdateDate(),
					"ddMMMyyyy")).toUpperCase());
			reqForm.setClientType(reqData.getClientType());
			reqForm.setDesignation(reqData.getDesignation()!=null ? reqData.getDesignation() : "" );
			reqForm.setTestDate((DateHelper.format(reqData.getTestDate(),
					"ddMMMyyyy")).toUpperCase());
			reqForm.setTestResult(reqData.getTestResultCode());
			reqForm.setTestDesc(reqData.getTestResultDesc());
			reqForm.setResolve(String.valueOf(reqData.isResolveInd()));
		
			reqForm.setFollowUpNo(String.valueOf(reqData.getFollowUpNumber()));
			reqForm.setFollowUpDate((DateHelper.format(reqData
					.getFollowUpDate(), "ddMMMyyyy")).toUpperCase());
			reqForm.setValidityDate((DateHelper.format(reqData
					.getValidityDate(), "ddMMMyyyy")).toUpperCase());
			
			reqForm.setPaidInd(String.valueOf(reqData.isPaidInd()));
			reqForm.setNewTest(ValueConverter.booleanToString(reqData.getNewTestOnly()));
			reqForm.setOrderDate((DateHelper.format(reqData.getOrderDate(),
					"ddMMMyyyy")).toUpperCase());
			reqForm.setDateSent((DateHelper.format(reqData.getDateSent(),
					"ddMMMyyyy")).toUpperCase());
			reqForm.setCcasSuggest(ValueConverter.booleanToString(reqData
					.getCCASuggestInd()));
			reqForm.setReceivedDate((DateHelper.format(
					reqData.getReceiveDate(), "ddMMMyyyy")).toUpperCase());
			reqForm.setAutoOrder(ValueConverter.booleanToString(reqData
					.getAutoOrderInd()));
			reqForm.setFldComm(ValueConverter.booleanToString(reqData
					.getFldCommentInd()));
			reqForm.setComment(reqData.getComments());
			reqForm.setClientNo(reqData.getClientId());
			
			
		}
		LOGGER.info("populateRequirementDetails end");
		return (reqForm);
	}
	
	private String getWMSImageLink(PolicyRequirementsData reqData) throws Exception {
		
		
		StringBuilder fnURL = new StringBuilder();
		
		ResourceBundle rb = ResourceBundle.getBundle(IUMConstants.IUM_WMS_CONFIG);
		String WMSServer = rb.getString("WMS_Server");
		String paramObjectStoreName = rb.getString("param_Object_Store_Name");
		
		int imageRef = 0;
		if (reqData.getImageRef() != null) {
			
			try {
				
				imageRef = Integer.parseInt(reqData.getImageRef().trim());
				if (imageRef != 0) {
					
					fnURL.append(WMSServer)
						.append("/getContent?id=").append(reqData.getImageRef())
						.append("&objectStoreName=").append(paramObjectStoreName)
						.append("&objectType=document");
				}
			} catch (NumberFormatException e) {
				
				if (!reqData.getImageRef().trim().equals("Y")
						|| !reqData.getImageRef().trim().equals("y")) {
					
					fnURL.append(WMSServer)
						.append("/getContent?id=").append(reqData.getImageRef())
						.append("&objectStoreName=").append(paramObjectStoreName)
						.append("&objectType=document");
				}
			}
		}
		
		return fnURL.toString();
	}
	
	private ArrayList getRequirementsList() throws Exception {
		
		LOGGER.info("getRequirementsList start");
		
		ArrayList list = new ArrayList();
		
		try {
			RequirementDAO dao = new RequirementDAO();
			list = dao.retrieveRequirements();
			
			if (list != null){
				LOGGER.debug("size = " + list.size());
			} else {
				LOGGER.debug("list is null");
			}
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMException(e.getMessage());
		} 
		
		LOGGER.info("getRequirementsList ends");
		return list;
	}

	
	private void updateRequirementRecord(
			final PolicyRequirementsForm reqForm,
			final String referenceNo,
			final String userId,
			final String lob,
			final String sourceSystem) throws Exception {
		
		LOGGER.info("updateRequirementRecord start");
		PolicyRequirementsData data = new PolicyRequirementsData();
		
		if (reqForm.getReqId() != null) {
			if (reqForm.getReqId().trim().length() > 0) {
				data.setRequirementId(Long.parseLong(reqForm.getReqId()));
			}
		}
		
		data.setReferenceNumber(referenceNo);
		
		if (reqForm.getSeq() != null) {
			if (reqForm.getSeq().trim().length() > 0) {
				data.setSequenceNumber(Long.parseLong(reqForm.getSeq()));
			}
		}
		
		data.setRequirementCode(reqForm.getReqCodeValue());
		data.setLevel(reqForm.getReqLevel());
		
		StatusData stat = new StatusData();
		if (reqForm.getReqStatusValue() != null) {
			if (reqForm.getReqStatusValue().trim().length() > 0) {
				stat.setStatusId(Long.parseLong(reqForm.getReqStatusValue()));
			}
		}
		data.setStatus(stat);
		
		if (!reqForm.getStatusDate().equals("")){
			data.setStatusDate(
					DateHelper.parse(reqForm.getStatusDate(), "ddMMMyyyy"));
		}else{
			data.setStatusDate(null);
		}
		
		data.setUpdatedBy(userId); 
		data.setUpdateDate(new Date());

		if (!reqForm.getReceivedDate().equals("")) {
			data.setReceiveDate(
					DateHelper.parse(reqForm.getReceivedDate(), "ddMMMyyyy"));
		} else {
			data.setReceiveDate(null);
		}
		
		if (reqForm.getCompleteReq() != null) {
			if (reqForm.getCompleteReq().equalsIgnoreCase("1") || reqForm.getCompleteReq().equalsIgnoreCase("Y")) {
				data.setCompletedRequirementInd(true);
			} else {
				data.setCompletedRequirementInd(false);
			}
		}

		if(reqForm.getCcasSuggest()!=null){
			if (reqForm.getCcasSuggest().equalsIgnoreCase("1") || reqForm.getCcasSuggest().equalsIgnoreCase("Y")){
				data.setCCASuggestInd(true);
			}else {
				data.setCCASuggestInd(false);
			}
		}
		
		
		if (!reqForm.getTestDate().equals("")){
			data.setTestDate(
					DateHelper.parse(reqForm.getTestDate(), "ddMMMyyyy"));
		}else{
			data.setTestDate(null);
		}
		
		data.setTestResultCode(reqForm.getTestResult());
		data.setTestResultDesc(reqForm.getTestDesc());
		data.setDesignation(reqForm.getDesignation());
		data.setClientType(reqForm.getClientType());
		data.setClientId(reqForm.getClientNo());
		
		if (reqForm.getResolve() != null) {
			if (reqForm.getResolve().equalsIgnoreCase("Y"))
				data.setResolveInd(true);
			else
				data.setResolveInd(false);
		}
		
		if (reqForm.getFollowUpNo() != null) {
			if (!reqForm.getFollowUpNo().equals("")) {
				data.setFollowUpNumber(Long.parseLong(reqForm.getFollowUpNo()));
			}
		}
		
		if (!reqForm.getFollowUpDate().equals("")){
			data.setFollowUpDate(
					DateHelper.parse(reqForm.getFollowUpDate(), "ddMMMyyyy"));
		}else{
			data.setFollowUpDate(
					DateHelper.add(new Date(), Calendar.DAY_OF_MONTH, (int) data.getFollowUpNumber()));
		}
		
		if (!reqForm.getValidityDate().equals("")){
			data.setValidityDate(
					DateHelper.parse(reqForm.getValidityDate(), "ddMMMyyyy"));
		}else{
			data.setValidityDate(null);
		}
		
		if (reqForm.getPaidInd() != null) {
			if (reqForm.getPaidInd().equalsIgnoreCase("Y")){
				data.setPaidInd(true);
			}else{
				data.setPaidInd(false);
			}
		}
		
		if (reqForm.getNewTest() != null) {
			if (reqForm.getNewTest().equalsIgnoreCase("Y")){
				data.setNewTestOnly(true);
			}else{
				data.setNewTestOnly(false);
			}
		}
		
		if (!reqForm.getOrderDate().equals("")){
			data.setOrderDate(
					DateHelper.parse(reqForm.getOrderDate(), "ddMMMyyyy"));
		}else{
			data.setOrderDate(null);
		}
		
		if (!reqForm.getDateSent().equals("")){
			data.setDateSent(
					DateHelper.parse(reqForm.getDateSent(), "ddMMMyyyy"));
		}else{
			data.setDateSent(null);
		}
		
		
		if (reqForm.getAutoOrder() != null) {
			if (reqForm.getAutoOrder().equalsIgnoreCase("Y")){
				data.setAutoOrderInd(true);
			}else{
				data.setAutoOrderInd(false);
			}
		}
		
		if (reqForm.getFldComm() != null) {
			
			if (reqForm.getFldComm().equalsIgnoreCase("Y")){
				data.setFldCommentInd(true);
			}else{
				data.setFldCommentInd(false);
			}
		}
		
		data.setComments(reqForm.getComment());
		data.setABACUScreateDate(new Date());
		data.setABACUSUpdateDate(new Date());
		data.setABACUSUpdatedBy(userId);
		
		
		PolicyRequirements req = new PolicyRequirements();
		AssessmentRequestData reqObj = new AssessmentRequestData();
		
		LOBData lobObj = new LOBData();
		lobObj.setLOBCode(lob);
		
		reqObj.setLob(lobObj);
		reqObj.setSourceSystem(sourceSystem);
		
		req.maintainRequirement(data, reqObj);
		LOGGER.info("updateRequirementRecord end");
	}

	private long saveRequirementRecord(
			final PolicyRequirementsForm reqForm,
			final String referenceNo,
			final String userId,
			final String lob,
			final String requestStatus,
			final String sourceSystem,
			final String strNBStrucNotesSettleDate) throws Exception {
		
		LOGGER.info("saveRequirementRecord start");
		
		PolicyRequirementsData data = new PolicyRequirementsData();
		data.setReferenceNumber(referenceNo);

		if (reqForm.getSeq() != null) {
			
			if (reqForm.getSeq().trim().length() > 0) {
				data.setSequenceNumber(Long.parseLong(reqForm.getSeq()));
			}else{
				data.setSequenceNumber(0);
			}
		}
		
		
		data.setRequirementCode(reqForm.getReqCodeValue());
		data.setLevel(reqForm.getReqLevel());
		
		StatusData stat = new StatusData();
		
		if (reqForm.getReqStatusValue() != null) {
			
			if (reqForm.getReqStatusValue().trim().length() > 0) {
				stat.setStatusId(Long.parseLong(reqForm.getReqStatusValue()));
			}
		}
		
		data.setStatus(stat);
		
		if (!"".equals(reqForm.getStatusDate())){
			
			data.setStatusDate(
					DateHelper.parse(reqForm.getStatusDate(), "ddMMMyyyy"));
		}else{
			data.setStatusDate(null);
		}
		
		data.setCreateDate(new Date());
		data.setCreatedBy(userId);
		data.setUpdatedBy(userId);
		data.setUpdateDate(new Date());
		
		if (!reqForm.getReceivedDate().equals("")){
			data.setReceiveDate(
					DateHelper.parse(reqForm.getReceivedDate(), "ddMMMyyyy"));
		}else{
			data.setReceiveDate(null);
		}
		
		
		if (reqForm.getCompleteReq() != null) {
			
			if (reqForm.getCompleteReq().equalsIgnoreCase("Y")) {
				data.setCompletedRequirementInd(true);
				data.setReceiveDate(new Date());
			} else
				data.setCompletedRequirementInd(false);
		}
		
		
		data.setCCASuggestInd(
				ValueConverter.stringToBoolean(reqForm.getCcasSuggest()));
		
		if (!reqForm.getTestDate().equals("")){
			data.setTestDate(DateHelper.parse(reqForm.getTestDate(),
					"ddMMMyyyy"));// 13
		}else{
			data.setTestDate(null);
		}
		
		
		data.setTestResultCode(reqForm.getTestResult());
		data.setTestResultDesc(reqForm.getTestDesc());
		data.setDesignation(reqForm.getDesignation());
		data.setClientType(reqForm.getClientType());
		data.setClientId(reqForm.getClientNo());
		
		
		if (reqForm.getResolve() != null) {
			
			if (reqForm.getResolve().equalsIgnoreCase("Y")){
				data.setResolveInd(true);
			}else{
				data.setResolveInd(false);
			}
		}
		
		
		if (reqForm.getFollowUpNo() != null) {
			
			if (!reqForm.getFollowUpNo().equals("")) {
				data.setFollowUpNumber(Long.parseLong(reqForm.getFollowUpNo()));// 19
			}
		}
		
		
		if(reqForm.getReqFollowUp()!=null && reqForm.getReqFollowUp().equals("on")){
			if (!reqForm.getFollowUpDate().equals("")){
				
				data.setFollowUpDate(
						DateHelper.parse(reqForm.getFollowUpDate(), "ddMMMyyyy"));// 20
				data.setReqtFollowUp("on");
			}else{
				data.setFollowUpDate(null);
				data.setReqtFollowUp("on");
			}
		}else{
			data.setFollowUpDate(null);
			data.setReqtFollowUp("off");
		}
		
		
		if (!reqForm.getValidityDate().equals("")){
			data.setValidityDate(
					DateHelper.parse(reqForm.getValidityDate(), "ddMMMyyyy"));// 21
		}else{
			data.setValidityDate(null);
		}
		
		if (reqForm.getPaidInd() != null) {
			
			if (reqForm.getPaidInd().equalsIgnoreCase("Y")){// 22
				data.setPaidInd(true);
			}else{
				data.setPaidInd(false);
			}
		}
		
		data.setNewTestOnly(ValueConverter.stringToBoolean(reqForm.getNewTest()));
		
		if (!reqForm.getOrderDate().equals("")){
			
			data.setOrderDate(
					DateHelper.parse(reqForm.getOrderDate(), "ddMMMyyyy"));
		}else{
			data.setOrderDate(null);
		}
		
		
		if (!reqForm.getDateSent().equals("")) {
			data.setDateSent(
					DateHelper.parse(reqForm.getDateSent(), "ddMMMyyyy"));
		} else {
			
			data.setDateSent(null);
		}

		data.setAutoOrderInd(
				ValueConverter.stringToBoolean(reqForm.getAutoOrder()));
		
		data.setFldCommentInd(
				ValueConverter.stringToBoolean(reqForm.getFldComm()));
		
		data.setComments(reqForm.getComment());
		data.setABACUScreateDate(new Date());
		data.setABACUSUpdateDate(new Date());
		data.setABACUSUpdatedBy(userId);
		
		PolicyRequirements req = new PolicyRequirements(strNBStrucNotesSettleDate);
		AssessmentRequestData reqObj = new AssessmentRequestData();
		LOBData lobObj = new LOBData();
		
		lobObj.setLOBCode(lob);
		reqObj.setLob(lobObj);
		reqObj.setSourceSystem(sourceSystem);
		
		StatusData statusObj = new StatusData();
		statusObj.setStatusId(Long.parseLong(requestStatus));
		
		reqObj.setStatus(statusObj);
		
		LOGGER.info("saveRequirementRecord end");
		return req.createRequirement(data, reqObj, userId);
	}
	
	private long updateFollowUpDateRequirementRecord(long reqId, PolicyRequirementsForm reqForm) throws Exception {
	
		LOGGER.info("updateFollowUpDateRequirementRecord start");
		
		PolicyRequirementsData data = new PolicyRequirementsData();

		if(reqForm.getReqFollowUp() != null
				&& reqForm.getReqFollowUp().equals("on")){
			
			if (!reqForm.getFollowUpDate().equals("")){
				
				data.setFollowUpDate(
						DateHelper.parse(reqForm.getFollowUpDate(), "ddMMMyyyy"));
			}else{
				data.setFollowUpDate(null);
			}
		}else{
			data.setFollowUpDate(null);
		}
		
		
		PolicyRequirements req = new PolicyRequirements();
		
		LOGGER.info("updateFollowUpDateRequirementRecord end");
		return req.updateRequirement(reqId, data);
	}
	
	private void closeConnection(final Connection conn) throws SQLException {
		
		
		if (!conn.isClosed()) {
			conn.close();
		}
		
	}

	
	
	public String replaceNull(final String str) {
		
		
		String toRet = "";
		if (str != null) {
			toRet = str.trim();
		}
		
		return toRet;
	}
}
