package com.slocpi.ium.ui.action;

import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.slocpi.ium.IUMException;
import com.slocpi.ium.data.UserData;
import com.slocpi.ium.data.UserProfileData;
import com.slocpi.ium.ui.form.LaboratoryForm;
import com.slocpi.ium.ui.util.IUMWebException;
import com.slocpi.ium.ui.util.Page;
import com.slocpi.ium.ui.util.Pagination;
import com.slocpi.ium.ui.util.StateHandler;
import com.slocpi.ium.underwriter.Reference;
import com.slocpi.ium.util.CodeHelper;

/**
 * @author ppe
 *
 * To change this generated comment edit the template variable "typecomment":
 * Window>Preferences>Java>Templates.
 * To enable and disable the creation of type comments go to
 * Window>Preferences>Java>Code Generation.
 */
public class ListLaboratoryAction extends IUMAction {
	private static final Logger LOGGER = LoggerFactory.getLogger(ListLaboratoryAction.class);
	
	/**
	 * @see com.slocpi.ium.ui.action.IUMAction#handleAction(org.apache.struts.action.ActionMapping, org.apache.struts.action.ActionForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	
	public ActionForward handleAction(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response)
		throws Exception {
		
		LOGGER.info("handleAction start");
		String page = "";
		
		try {
												
			StateHandler sh = new StateHandler();
			UserData ud = sh.getUserData(request);
			UserProfileData profile = ud.getProfile();
			
			String role = profile.getRole();
			
			LaboratoryForm laboratoryForm = (LaboratoryForm) form;			
			ArrayList laboratoryList = populateLaboratoryList();
			
			Page pg = createPage(request, profile, laboratoryList);
			 
			if(!laboratoryForm.getActionType().equalsIgnoreCase("update") && !laboratoryForm.getActionType().equalsIgnoreCase("create"))
			{
				laboratoryForm = new LaboratoryForm();
			}
			
			 	
			laboratoryForm.setLaboratoryList(pg.getList());
			    
			request.setAttribute("laboratoryForm", laboratoryForm);
			page = "listLaboratoryPage";
			
		} 
		catch (Exception e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			ActionErrors errors = new ActionErrors();
			errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.system.exception", e.getMessage()));
			saveErrors(request, errors);
			page = "errorPage";
		}
		LOGGER.info("handleAction end");
		LOGGER.debug("PAGE FORWARD-->" + page);
		return (mapping.findForward(page));
	}
	
	
	private ArrayList populateLaboratoryList() throws IUMException {
		
		LOGGER.info("populateLaboratoryList start");
		Reference lab = new Reference();
		ArrayList labList = lab.getLaboratoryList();
		LOGGER.info("populateLaboratoryList end");
		return labList;
	}
	
	// this will create the page object for the list
	private Page createPage(HttpServletRequest request, UserProfileData profile, ArrayList laboratoryList  ) throws IUMWebException
	{
		LOGGER.info("createPage start");
		int recPerView = profile.getRecordsPerView();
		
		if (recPerView == 0) {
		  recPerView = 1;
		}
			
		int pageNo = 1;
		String reqPageNum = request.getParameter("pageNo");
		
		if ((reqPageNum != null) && !(reqPageNum.equals(""))) {
			try {
				pageNo = Integer.parseInt(reqPageNum);
			}
			catch (NumberFormatException e) {
				LOGGER.error(CodeHelper.getStackTrace(e));
			}
		}	
						
		Pagination pgn = new Pagination(laboratoryList, recPerView);
		Page pg = pgn.getPage(pageNo);			
				
		request.setAttribute("page", pg);
		request.setAttribute("pageNo", String.valueOf(pageNo));
		
		LOGGER.info("createPage end");
		return pg;
	}

}
