/*
 * Created on Jan 12, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.ui.action;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.slocpi.ium.IUMException;
import com.slocpi.ium.data.AuditLogsData;
import com.slocpi.ium.data.UserData;
import com.slocpi.ium.data.UserProfileData;
import com.slocpi.ium.data.dao.AuditLogslDAO;
import com.slocpi.ium.data.util.DataSourceProxy;
import com.slocpi.ium.ui.form.DoctorsNotesDetailForm;
import com.slocpi.ium.ui.form.MedicalRecordDetailForm;
import com.slocpi.ium.ui.form.RequestDetailForm;
import com.slocpi.ium.ui.util.StateHandler;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.IUMConstants;
import com.slocpi.ium.util.SecuredHttpServletWrapper;
import com.slocpi.ium.util.UserManager;

/**
 * @author daguila
 * 
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class LoginAction extends Action {
	private static final Logger LOGGER = LoggerFactory.getLogger(LoginAction.class);
	private static final String empty = "";
	
	/**
	 * This method handles the processes of LoginAction class. It checks the
	 * validity of the user Id and password inputted by the user. It also checks
	 * the status of the newly logged-in user.
	 */
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		LOGGER.info("execute start");
		Connection conn = new DataSourceProxy().getConnection();
		AuditLogsData data = new AuditLogsData();
		AuditLogslDAO dao = new AuditLogslDAO(conn);
		
		String page = "";
		String temp_page = "";
		String id = "";

		SecuredHttpServletWrapper securedRequest = new SecuredHttpServletWrapper(
				request);

		boolean isHackAttempt = false;

		if (securedRequest.getMethod().equalsIgnoreCase("GET")
				&& securedRequest.getQueryString() != null
				&& hasIllegalParameter(securedRequest)) {
			isHackAttempt = true;
		}

		if (securedRequest.hasScript() || isHackAttempt) {
			StateHandler sh = new StateHandler();
			sh.removeUserData(request);
			sh.removeUserAccess(request);
			ActionErrors errors = new ActionErrors();
			errors.add(ActionErrors.GLOBAL_ERROR, new ActionError(
					"message.addillegalscript"));
			saveErrors(request, errors);
			data.setRemarks("An attempt to hack the system.");
			page = "loginPage";
			return (mapping.findForward(page));

		}

		String userId = request.getParameter("agentCode") != null ? request
				.getParameter("agentCode") : "";
		if (userId != null) {
			userId = userId.toUpperCase();
		}
		data.setUserId(userId);
		
		String task = request.getParameter("task");
		if (task == null) {
			task = "";
		}

		if (task != null && task.equals("logout")) {
			StateHandler sh = new StateHandler();
			UserData userlogged = sh.getUserData(request);
			if (userlogged != null) {
				UserProfileData udata = userlogged.getProfile();
				userId = udata.getUserId();
				userId = userId.toUpperCase();
				data.setUserId(userId);
				data.setAuditType("LO-SUC");
				data.setRemarks("Logout successful.");
			}
		}

		try {
			
			temp_page = (String) request.getParameter("page");
			id = (String) request.getParameter("id");
			StateHandler sh = new StateHandler();

			page = "loginPage";

			sh.removeUserData(request);
			sh.removeUserAccess(request);

			if (id != null) {
				sh.removeNotificationID(request);
				sh.setNotificationID(request, id);
			} else {
				id = sh.getNotificationID(request);
			}
			if (temp_page != null) {
				sh.removeNotificationPage(request);
				sh.setNotificationPage(request, temp_page);
			} else {
				temp_page = sh.getNotificationPage(request);
			}
			
			ResourceBundle rb = ResourceBundle
					.getBundle(IUMConstants.IUM_WMS_CONFIG);
			String contextPath = rb.getString("wms_context_path");

			if (request.getContextPath().equalsIgnoreCase(contextPath)
					&& task.equals("")) {
				page = "closeBrowser";
			}

			if (task.equals("login")) {
				UserManager um = new UserManager();
				UserData user = null;
				HashMap userAccess = new HashMap();
				String password = request.getParameter("password");

				if (userId == null) {
					userId = "";
				}

				if (password == null) {
					password = empty;
				}
				
				
				if (um.isValidUser(userId, password)) {
					userId = userId.toUpperCase();
					user = um.retrieveUserDetails(userId);
					UserProfileData usrProf = user.getProfile();
				
					request.getSession().setAttribute("loginInd",
							usrProf.getLogin_ind());
					
					if (usrProf.isLock() == true) {
						ActionErrors errors = new ActionErrors();
						errors.add(ActionErrors.GLOBAL_ERROR, new ActionError(
								"error.login.locked"));
						saveErrors(request, errors);

						
						data.setAuditType("LI-LOCKED");
						data
								.setRemarks("Your account has been locked.  Please contact the system administrator.");
						

					} else if (usrProf.isActive() != true) {
						ActionErrors errors = new ActionErrors();
						errors.add(ActionErrors.GLOBAL_ERROR, new ActionError(
								"error.login.deactivated"));
						saveErrors(request, errors);
						data.setAuditType("LI-DEACTV");
						data
								.setRemarks("Your account has been deactivated.  Please contact the system administrator.");
					} else {

						userAccess = um.getUserPrivileges(userId);
						sh.setUserData(request, user);
						sh.setUserAccess(request, userAccess);
						page = "success";

						if (temp_page != null) {
							if (temp_page
									.equalsIgnoreCase(IUMConstants.NOTIFY_PAGE_AR)) {
								page = "viewRequestDetail";
								RequestDetailForm requestDetailForm = new RequestDetailForm();
								requestDetailForm.setRefNo(id);
								request.setAttribute("requestDetailForm",
										requestDetailForm);
								request.setAttribute("refNo", id);
							} else if (temp_page
									.equalsIgnoreCase(IUMConstants.NOTIFY_PAGE_M)) {
								page = "viewMedicalDetail";
								MedicalRecordDetailForm medicalRecordDetailForm = new MedicalRecordDetailForm();
								medicalRecordDetailForm.setMedicalRecordId(id);

								request.setAttribute("medicalRecordDetailForm",
										medicalRecordDetailForm);
								request.setAttribute("refNo", id);

							} else if (temp_page
									.equalsIgnoreCase(IUMConstants.NOTIFY_PAGE_DN)) {
								page = "viewDoctorsNotes";
								DoctorsNotesDetailForm doctorsNotesDetailForm = new DoctorsNotesDetailForm();
								doctorsNotesDetailForm.setRefNo(id);

								request.setAttribute("doctorsNotesDetailForm",
										doctorsNotesDetailForm);
								request.setAttribute("refNo", id);
							}
							sh.removeNotificationID(request);
							sh.removeNotificationPage(request);
						}

						data.setAuditType("LI-SUC");
						data.setRemarks("Login successful.");
					
					}

				} else {

					if (request.getParameter("userID") != null
							&& userId.equals(request.getParameter("userID"))
							&& request.getParameter("attempts") != null) {
						request.setAttribute("attempts",
								(new Integer(Integer.parseInt(request
										.getParameter("attempts")) + 1)
										.toString()));
						if (Integer.parseInt((String) request
								.getAttribute("attempts")) == 3) {
							um.lockUser(userId);
						}
					} else {
						request.setAttribute("attempts", "1");
					}
					request.setAttribute("userID", userId);

					ActionErrors errors = new ActionErrors();
					errors.add(ActionErrors.GLOBAL_ERROR, new ActionError(
							"error.login"));
					saveErrors(request, errors);

					data.setAuditType("LI-INV-U-P");
					data.setRemarks("Invalid User ID or Password.");
					
				}
			}

			try {
				conn.setAutoCommit(false);
				dao.insertAuditLogs(data);
				conn.commit();
			} catch (SQLException sqle) {
				LOGGER.error(CodeHelper.getStackTrace(sqle));
			} finally {
				closeConnection(conn);
			}
		}
		catch (Exception e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			ActionErrors errors = new ActionErrors();
			errors.add(ActionErrors.GLOBAL_ERROR, new ActionError(
					"error.system.exception", e.getMessage()));
			saveErrors(request, errors);
			page = "errorPage";

			try {
				data.setAuditType("SYS-EXCEPT");
				data.setRemarks("System exception : Cause By : " + e.getCause()
						+ " - Message : " + e.getMessage());
				conn.setAutoCommit(false);
				dao.insertAuditLogs(data);
				conn.commit();
			} catch (SQLException sqle) {
				LOGGER.error(CodeHelper.getStackTrace(sqle));
			} finally {
				closeConnection(conn);
			}
		}
		
		LOGGER.info("execute end");
		LOGGER.debug("PAGE FORWARD-->" + page);
		return (mapping.findForward(page));
	}

	private boolean hasIllegalParameter(HttpServletRequest request) {

		LOGGER.info("hasIllegalParameter start");
		Map paramMap = request.getParameterMap();
		Iterator itr = paramMap.keySet().iterator();
		while (itr.hasNext()) {
			if (!itr.next().toString().equalsIgnoreCase("task")) {
				return true;
			}
		}
		LOGGER.info("hasIllegalParameter end");
		return false;
	}


	private void closeConnection(Connection conn) throws IUMException {
		
		
		if (null != conn) {
			try {
				conn.close();
			} catch (SQLException sqlE) {
				LOGGER.error(CodeHelper.getStackTrace(sqlE));
				throw new IUMException(sqlE);
			}
		}
		
	}
}
