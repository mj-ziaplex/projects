package com.slocpi.ium.ui.action;

import java.util.ArrayList;
import java.util.Date;
import java.util.StringTokenizer;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.slocpi.ium.IUMException;
import com.slocpi.ium.data.ExaminerData;
import com.slocpi.ium.data.SpecializationData;
import com.slocpi.ium.ui.form.ExaminerForm;
import com.slocpi.ium.underwriter.Reference;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.DateHelper;

/**
 * SaveExaminerAction.java 
 * Description:
 *
 * @version $Revision: 1.1 $
 * @author $Author: pw12 $ $Date: 2012/08/01 07:07:18 $
 */
public class SaveExaminerAction extends IUMAction{
	private static final Logger LOGGER = LoggerFactory.getLogger(SaveExaminerAction.class);
	/**
	 * @see com.slocpi.ium.ui.action.IUMAction#handleAction(org.apache.struts.action.ActionMapping, org.apache.struts.action.ActionForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	public ActionForward handleAction(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response)
		throws Exception {
		
		LOGGER.info("handleAction start");
		String page = "";
		try{
			ExaminerForm eForm = (ExaminerForm)form;
			ExaminerData examiner = this.createExaminerDataObject(eForm);
			this.saveExaminer(examiner);
			page = "listExaminerPage";
			
			
		}catch(Exception e){
			LOGGER.error(CodeHelper.getStackTrace(e));
			ActionErrors errors = new ActionErrors();
			errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.system.exception", e.getMessage()));
			saveErrors(request, errors);
			page = "errorPage"; 
		}
		LOGGER.info("handleAction end");
		LOGGER.debug("PAGE FORWARD-->" + page);
		return mapping.findForward(page)	;
	}
	
	private ExaminerData createExaminerDataObject(ExaminerForm examinerForm) throws Exception{
		
		LOGGER.info("createExaminerDataObject start");
		ExaminerData examinerData = new ExaminerData();
		
		try{
		
		if(examinerForm.getExaminerId() != null){
			examinerData.setExaminerId(Long.parseLong(examinerForm.getExaminerId()));	
		}
		
		examinerData.setLastName(examinerForm.getLastName());
		examinerData.setFirstName(examinerForm.getFirstName());
		examinerData.setMiddleName(examinerForm.getMiddleName());
		examinerData.setSalutation(examinerForm.getSalutation());
		if(examinerForm.getDateOfBirth() != null && !(examinerForm.getDateOfBirth().trim().equals(""))){ //date
			examinerData.setDateOfBirth(DateHelper.parse(examinerForm.getDateOfBirth(),"ddMMMyyyy"));
		}
		examinerData.setSex(examinerForm.getSex());
		examinerData.setRank(examinerForm.getRank());
		if(examinerForm.getRankEffectiveDate() != null && !(examinerForm.getRankEffectiveDate().trim().equals(""))){ //date
			examinerData.setRankEffectiveDate(DateHelper.parse(examinerForm.getRankEffectiveDate(),"ddMMMyyyy"));
		}
		if(examinerForm.getTIN() != null && !(examinerForm.getTIN().trim().equals(""))){ //date	
			examinerData.setTIN(this.tokenizeTIN(examinerForm.getTIN()));
		}
		examinerData.setContactNumber(examinerForm.getOfficeNumber()); 
		examinerData.setFaxNumber(examinerForm.getFaxNumber());
		examinerData.setMobileNumber(examinerForm.getMobileNumber());
		examinerData.setBusAddrLine1(examinerForm.getBusAddrLine1());
		examinerData.setBusAddrLine2(examinerForm.getBusAddrLine2());
		examinerData.setBusAddrLine3(examinerForm.getBusAddrLine3());
		examinerData.setCity(examinerForm.getCity());
		examinerData.setProvince(examinerForm.getProvince());
		examinerData.setCountry(examinerForm.getCountry());
		examinerData.setZipCode(examinerForm.getZipCode());
		examinerData.setMailToBusAddrInd(examinerForm.getMailToBusAddrInd());
		examinerData.setPreEmploymentSrvcInd(examinerForm.getPreEmploymentSrvcInd());
		examinerData.setAccreditationInd(examinerForm.getAccreditationInd());
		if(examinerForm.getClinicHrsFrom() != null && !(examinerForm.getClinicHrsTo().trim().equals(""))){ //time
			examinerData.setClinicHrsFrom(this.convertToDate(examinerForm.getClinicHrsFrom()));
		}
		if(examinerForm.getClinicHrsTo() != null && !(examinerForm.getClinicHrsTo().trim().equals(""))){
			examinerData.setClinicHrsTo(this.convertToDate(examinerForm.getClinicHrsTo()));
		}
	
	
		examinerData.setExaminePlace(examinerForm.getExaminePlace());
		
		String specializations[] = examinerForm.getSpecializations();
		ArrayList specials = new ArrayList();
		if(specializations==null){
			LOGGER.debug("specializations is null");
		}
		if(specializations != null){
			for(int i=0;i<specializations.length;i++){
				SpecializationData sp = new SpecializationData();
				sp.setSpecializationId(Long.parseLong(specializations[i]));
				specials.add(sp);
			}
		}
			
		examinerData.setSpecializations(specials);
		
		
		}catch(Exception e){
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMException(e);
		}
		LOGGER.info("createExaminerDataObject end");
		return examinerData;
	}
	
	private void saveExaminer(ExaminerData examiner) throws IUMException, Exception{
		LOGGER.info("saveExaminer start");
		Reference mgr = new Reference();
		mgr.editExaminer(examiner);
		LOGGER.info("saveExaminer end");
	}
	
	private String tokenizeTIN(String untokenized) throws Exception{
		
		LOGGER.info("tokenizeTIN start");
		StringTokenizer token = new StringTokenizer(untokenized,"-");
		String[] ar = new String[3];
		int i=0;
		while(token.hasMoreTokens()){
			ar[i] = token.nextToken();
			i++;
		}
		String tin = ar[0] + ar[1] + ar[2];
		LOGGER.debug("The TIN of the examiner: "+tin);
		LOGGER.info("tokenizeTIN end");
		return tin;
	}
	
	private Date convertToDate(String whole){
		
		LOGGER.info("convertToDate start");
		if( (whole.indexOf("PM")!=-1) || (whole.indexOf("AM")!=-1) ){
			java.util.Date date = DateHelper.parse(whole,"hh:mma");  
			LOGGER.info("convertToDate end");
			return date;
		
		}else{
			LOGGER.debug("military time");
			java.util.Date date = DateHelper.parse(whole,"HH:mm");
			LOGGER.info("convertToDate end");
			return date;		
		}
	}
	

	

}
