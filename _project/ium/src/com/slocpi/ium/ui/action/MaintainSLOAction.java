/*
 * Created on Mar 9, 2004
 * package name = com.slocpi.ium.ui.action
 * file name    = MaintainSLOAction.java
 */
package com.slocpi.ium.ui.action;

import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.slocpi.ium.IUMException;
import com.slocpi.ium.data.SunLifeOfficeData;
import com.slocpi.ium.data.UserData;
import com.slocpi.ium.data.UserProfileData;
import com.slocpi.ium.ui.form.SLOForm;
import com.slocpi.ium.ui.util.StateHandler;
import com.slocpi.ium.underwriter.Reference;
import com.slocpi.ium.underwriter.UnderWriterException;
import com.slocpi.ium.util.CodeHelper;

/**
 * TODO Class Description of MaintainSLOAction.java
 * @author Engel
 * 
 */
public class MaintainSLOAction extends IUMAction {

	private static final Logger LOGGER = LoggerFactory.getLogger(MaintainSLOAction.class);
	/* (non-Javadoc)
	 * @see com.slocpi.ium.ui.action.IUMAction#handleAction(org.apache.struts.action.ActionMapping, org.apache.struts.action.ActionForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	public ActionForward handleAction(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response)
		throws Exception {

		LOGGER.info("handleAction start");
		String page="reloadList";
		String mode = request.getParameter("mode");
		SLOForm slof = (SLOForm)form;
		StateHandler sh = new StateHandler();
		UserData ud = sh.getUserData(request);
		UserProfileData profile = ud.getProfile();
		String userId = profile.getUserId();
		
		try{
			saveOffice(slof,mode,userId);		
		} catch(UnderWriterException e){
			LOGGER.error(CodeHelper.getStackTrace(e));
			ActionErrors errors = new ActionErrors();
			errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.duplicateOfficeId", e.getMessage()));
			saveErrors(request, errors);
			page="viewDetails"; 
		}catch(IUMException e){
			LOGGER.error(CodeHelper.getStackTrace(e));
			ActionErrors errors = new ActionErrors();
			errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.system.exception", e.getMessage()));
			saveErrors(request, errors);
			page="errorPage"; 
		}
		LOGGER.info("handleAction end");
		LOGGER.debug("PAGE FORWARD-->" + page);
		return (mapping.findForward(page));
	}

	private void saveOffice(SLOForm slof,String mode,String userId)throws UnderWriterException, IUMException{
		
		LOGGER.info("saveOffice start");
		SunLifeOfficeData data = new SunLifeOfficeData();

		data.setOfficeId(slof.getOfficeId());
		data.setOfficeName(slof.getOfficeName());
		data.setOfficeType(slof.getOfficeType());
		data.setAddr1(slof.getAddr1());
		data.setAddr2(slof.getAddr2());
		data.setAddr3(slof.getAddr3());
		data.setCity(slof.getCity());
		data.setProvince(slof.getProvince());
		data.setCountry(slof.getCountry());
		data.setZipCode(slof.getZipCode());
		data.setContactNumber(slof.getContactNumber());
		data.setFaxNumber(slof.getFaxNumber());

		
		if (mode != null) {
			if (mode.equals("add")) {
			   data.setCreateDate(new Date());
			   data.setCreatedBy(userId);
			}
		} else {
			data.setUpdateDate(new Date());
			data.setUpdatedBy(userId);       	
		}
		
		
		Reference ref = new Reference();
		LOGGER.debug(mode);
			if (mode != null) {
			   	SunLifeOfficeData checkExist = ref.getSLODetail(data.getOfficeId());
			   	
				if(checkExist!=null&&checkExist.getOfficeId()!=null&&!checkExist.getOfficeId().equals("")){
					LOGGER.error("Duplicate Office Id Error.");
					throw new UnderWriterException("Duplicate Office Id Error.");
				}else{
					ref.createSLO(data);
				}
			}else {
				ref.editSLO(data);
			}		
			LOGGER.info("saveOffice end");
	
	}
	
}
