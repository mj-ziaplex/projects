/*
 * Created on Mar 7, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.ui.action;

import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.slocpi.ium.data.ARWithPendingMedFilterData;
import com.slocpi.ium.data.ExaminerData;
import com.slocpi.ium.data.LaboratoryData;
import com.slocpi.ium.data.SunLifeOfficeData;
import com.slocpi.ium.data.UserProfileData;
import com.slocpi.ium.reports.ApplicationsWithPendMedReport;
import com.slocpi.ium.ui.form.ReportFollowMedForm;
import com.slocpi.ium.ui.util.Pagination;
import com.slocpi.ium.ui.util.StateHandler;
import com.slocpi.ium.util.CodeHelper;

/**
 * @author nic.decapia
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class GenerateReportFollowMedAction extends IUMAction {
	private static final Logger LOGGER = LoggerFactory.getLogger(GenerateReportFollowMedAction.class);
	/* (non-Javadoc)
	 * @see com.slocpi.ium.ui.action.IUMAction#handleAction(org.apache.struts.action.ActionMapping, org.apache.struts.action.ActionForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	public ActionForward handleAction(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response)
		throws Exception {
		
		LOGGER.info("handleAction start");
		String page = "reportsPage";
		
		
		try {
			
			StateHandler sessionHandler = new StateHandler();
			UserProfileData userPref = sessionHandler.getUserData(request).getProfile();

			ReportFollowMedForm frmReport = (ReportFollowMedForm) form;
			if (frmReport == null) {
				ActionErrors errors = new ActionErrors();
				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.system.exception", "Report Parameters are undefined."));
				saveErrors(request, errors);
				page = "errorPage";
			}
			
			
			ARWithPendingMedFilterData filter = extractParamters(frmReport);

			
			ApplicationsWithPendMedReport rpt = new ApplicationsWithPendMedReport();
			ArrayList rptContent = rpt.generate(filter);
		 
			int recPerPage = (userPref.getRecordsPerView()!=0)?userPref.getRecordsPerView():10;
			Pagination pgn = new Pagination(rptContent, recPerPage);
			frmReport.setPage(pgn.getPage(frmReport.getPageNo()));
			
			
			request.setAttribute("reportFilter", frmReport);			
		}
		catch (Exception e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			ActionErrors errors = new ActionErrors();
			errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.system.exception", e.getMessage()));
			saveErrors(request, errors);
			page = "errorPage"; 			
		}
		
		LOGGER.info("handleAction end");
		LOGGER.debug("PAGE FORWARD-->" + page);	
		
		return (mapping.findForward(page));
	}
	
	/**
	 * @param frmReport
	 * @return
	 */
	private ARWithPendingMedFilterData extractParamters(ReportFollowMedForm frm)
	{
		LOGGER.info("extractParamters start");
		ARWithPendingMedFilterData filter = new ARWithPendingMedFilterData();
		filter.setDateStart(frm.getStartDate());
		filter.setDateEnd(frm.getEndDate());
		
		
		filter.setReportType(Integer.parseInt(frm.getReportType()));
		
		String branchId = frm.getBranch();
		
		SunLifeOfficeData slo = new SunLifeOfficeData();
		if (branchId.length() != 0) {
			slo.setOfficeId(branchId);
		}
		else {
			slo.setOfficeId(null);
		}
		filter.setBranch(slo);
		
		String examinerId = frm.getExaminer();
		if (examinerId == null) examinerId = "";
		ExaminerData ex = new ExaminerData();
		if (examinerId.length() != 0) {
			ex.setExaminerId(Long.parseLong(examinerId));
		}
		else {
			ex.setExaminerId(-1);
		}
		filter.setExaminer(ex);
		
		
		String labId = frm.getLaboratory();
		if (labId == null) labId = "";
		LaboratoryData lab = new LaboratoryData();
		if (labId.length() != 0) {
			lab.setLabId(Long.parseLong(labId));	
		}
		else {
			lab.setLabId(-1);
		}
		filter.setLaboratory(lab);
	
		
		String underwriterId = frm.getUnderwriter();
		UserProfileData user = new UserProfileData();
		if (underwriterId.length() != 0) {
			user.setUserId(underwriterId);
		}
		else {
			user.setUserId(null);
		}
		filter.setUnderwriter(user);
		LOGGER.info("extractParamters end");
		return (filter);
	}

}
