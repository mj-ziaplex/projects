/*
 * Created on Mar 1, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.ui.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.slocpi.ium.data.UserData;
import com.slocpi.ium.data.UserProfileData;
import com.slocpi.ium.data.dao.UserDAO;
import com.slocpi.ium.ui.form.UserProfileForm;
import com.slocpi.ium.ui.util.StateHandler;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.UserManager;



/**
 * @author Abie
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class ChangePasswordAction extends IUMAction{
	private static final Logger LOGGER = LoggerFactory.getLogger(ChangePasswordAction.class);
	/* (non-Javadoc)
	 * @see com.slocpi.ium.ui.action.IUMAction#handleAction(org.apache.struts.action.ActionMapping, org.apache.struts.action.ActionForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	public ActionForward handleAction(
	ActionMapping mapping, 
	ActionForm form, 
	HttpServletRequest request, 
	HttpServletResponse response) 
	throws Exception {
		
		LOGGER.info("handleAction start");
		String page = "";
		try{
			StateHandler sh = new StateHandler();
			UserData ud = sh.getUserData(request);
			UserProfileData profile = ud.getProfile();
			String userId = profile.getUserId();
			UserManager userManager = new UserManager();
			UserProfileForm userProfileForm = new UserProfileForm();
			UserProfileData userProfileData = new UserProfileData();
			UserData userData = new UserData();
			userProfileForm = (UserProfileForm) form;
			UserDAO userDao = new UserDAO();
			userDao.changeUserPassword(userId,userProfileForm.getNewPassword());
		
			userData = userManager.retrieveUserDetails(userId);				
			userProfileForm.setPassword(userData.getProfile().getPassword());
			userProfileForm.setOPassword(" ");
			userProfileForm.setOldPassword("");
			userProfileForm.setNewPassword("");
			userProfileForm.setUserId(userId);
			userProfileForm.setConfirmPassword("");
			request.setAttribute("userProfileForm",userProfileForm);
			page = "changePasswordPage";
			

			if(request.getSession().getAttribute("loginInd")!=null && ((String)request.getSession().getAttribute("loginInd")).equalsIgnoreCase("0")){
				userManager.update1LoginInd(userId);//set login_ind
				request.getSession().setAttribute("loginInd", "1");
				page = "success";
			}
			//
			long pwdDays = userManager.getPswdDays(userId);
			request.getSession().setAttribute("pwdDays", Long.toString(pwdDays));
		}
		catch(Exception e){
			LOGGER.error(CodeHelper.getStackTrace(e));
			ActionErrors errors = new ActionErrors();
			errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.system.exception", e.getMessage()));
			saveErrors(request, errors);
			page = "errorPage"; 
		}
		
		LOGGER.info("handleAction end");
		LOGGER.debug("PAGE FORWARD-->" + page);
		return (mapping.findForward(page));
	}
	

}
