package com.slocpi.ium.ui.action;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.slocpi.ium.data.ClientData;
import com.slocpi.ium.data.ExaminationPlaceData;
import com.slocpi.ium.data.ExaminerData;
import com.slocpi.ium.data.LaboratoryData;
import com.slocpi.ium.data.MedicalRecordData;
import com.slocpi.ium.data.PolicyMedicalRecordData;
import com.slocpi.ium.data.SectionData;
import com.slocpi.ium.data.StatusData;
import com.slocpi.ium.data.SunLifeDeptData;
import com.slocpi.ium.data.SunLifeOfficeData;
import com.slocpi.ium.data.TestProfileData;
import com.slocpi.ium.data.UserData;
import com.slocpi.ium.data.UserProfileData;
import com.slocpi.ium.data.dao.SectionCHDAO;
import com.slocpi.ium.data.dao.StatusCHDao;
import com.slocpi.ium.data.dao.TestProfileDAO;
import com.slocpi.ium.data.util.DataSourceProxy;
import com.slocpi.ium.ui.form.MedicalRecordDetailForm;
import com.slocpi.ium.ui.util.StateHandler;
import com.slocpi.ium.underwriter.MedicalLabRecord;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.DateHelper;
import com.slocpi.ium.util.IUMConstants;
import com.slocpi.ium.util.NameValuePair;

/**
 * @author jcristobal
 *
 * To change this generated comment edit the template variable "typecomment":
 * Window>Preferences>Java>Templates.
 * To enable and disable the creation of type comments go to
 * Window>Preferences>Java>Code Generation.
 */
public class SaveMedicalExamAction extends IUMAction {
	private static final Logger LOGGER = LoggerFactory.getLogger(SaveMedicalExamAction.class);
	/**
	 * @see com.slocpi.ium.ui.action.IUMAction#handleAction(org.apache.struts.action.ActionMapping, org.apache.struts.action.ActionForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	public ActionForward handleAction(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response)
		throws Exception {
		
		LOGGER.info("handleAction start");
		String page = "";
		Connection conn = new DataSourceProxy().getConnection();
		try {
			String key = "";
					    
			MedicalRecordDetailForm medicalRecordDetailForm = (MedicalRecordDetailForm)form;
			MedicalRecordData medicalRecordData = new MedicalRecordData();
			getMedicalExamRecord(medicalRecordDetailForm, medicalRecordData, request, conn);
					
			MedicalLabRecord medicalLabRecord = new MedicalLabRecord();

			if (medicalRecordDetailForm.getActionType().equalsIgnoreCase("create")){
				if (medicalRecordData.getPolicyMedicalRecordData().getReferenceNumber() == null && medicalRecordData.getClient().getClientId().equals("")){
					medicalRecordData.getStatus().setStatusId(IUMConstants.STATUS_VALID);
					medicalRecordData.setRequestForLOAInd(true);
					medicalRecordData.setReceivedResults(true);
					medicalRecordData.setSevenDayMemoInd(false);
					
					StatusData statusData = new StatusData();
					statusData.setStatusId(IUMConstants.STATUS_NOT_PROCEEDED_WITH);		
					medicalRecordData.setApplicationStatus(statusData);
					
					medicalRecordData.setReasonForReqt("");
					medicalRecordData.setSevenDayMemoDate(null);
					
					SectionData sectionData = new SectionData();		
					sectionData.setSectionId(IUMConstants.DEFAULT_SECTION_ID);
					medicalRecordData.setSection(sectionData);
					request.setAttribute("status", Long.toString(IUMConstants.STATUS_VALID));												
				}
				
				medicalLabRecord.createMedicalExam(medicalRecordData);					
			}			
			else if (medicalRecordDetailForm.getActionType().equalsIgnoreCase("maintain") || medicalRecordDetailForm.getActionType().equalsIgnoreCase("changestatus")){
				 if (medicalRecordDetailForm.getStatus().equals(medicalRecordDetailForm.getPrevStatus())){ //Maintain medical
				 	if (Long.parseLong(medicalRecordDetailForm.getStatus()) == IUMConstants.STATUS_REQUESTED || Long.parseLong(medicalRecordDetailForm.getStatus()) == IUMConstants.STATUS_VALID){
				 		medicalLabRecord.maintainMedicalRecord(medicalRecordData);				 		
				 	}
				 	else{
				 		medicalLabRecord.saveRemarks(medicalRecordData); 
				 	}
				 }
				 else{
				 	if (Long.parseLong(medicalRecordDetailForm.getStatus()) == IUMConstants.STATUS_CONFIRMED){
						
						ArrayList list = new ArrayList();
						medicalRecordData.getStatus().setStatusId(Long.parseLong(medicalRecordDetailForm.getPrevStatus()));
						list.add(medicalRecordData);
						medicalLabRecord.confirmMedicalExam(list); 
					}
					else if (Long.parseLong(medicalRecordDetailForm.getStatus()) == IUMConstants.STATUS_VALID){
						
						ArrayList list = new ArrayList();
						medicalRecordData.getStatus().setStatusId(Long.parseLong(medicalRecordDetailForm.getPrevStatus()));
						list.add(medicalRecordData);
						medicalLabRecord.receiveMedicalExamResult(list); 
					}
					else if (Long.parseLong(medicalRecordDetailForm.getStatus()) == IUMConstants.STATUS_NOT_SUBMITTED){
						
						ArrayList list = new ArrayList();
						medicalRecordData.getStatus().setStatusId(Long.parseLong(medicalRecordDetailForm.getPrevStatus()));
						if (!medicalRecordDetailForm.getFollowUpDate().equals(""))
							medicalRecordData.setFollowUpDate(DateHelper.parse(medicalRecordDetailForm.getFollowUpDate(),"ddMMMyyyy"));
						list.add(medicalRecordData);
						medicalLabRecord.updateMedExamNoSubmission(list); 
					}
					else if (Long.parseLong(medicalRecordDetailForm.getStatus()) == IUMConstants.STATUS_CANCELLED){
						medicalLabRecord.setCancelled(medicalRecordData.getReferenceNo(), medicalRecordData.getUpdatedBy()); 
					} else {
						ArrayList list = new ArrayList();
						medicalRecordData.getStatus().setStatusId(Long.parseLong(medicalRecordDetailForm.getPrevStatus()));
						list.add(medicalRecordData);
						medicalLabRecord.genericMRChangeStatus(list,Long.parseLong(medicalRecordDetailForm.getStatus()));
					}
				 }
			}

			request.setAttribute("refNo", String.valueOf(medicalRecordData.getMedicalRecordId()));
			request.setAttribute("actionType", "save");
			
			page = "saveMedicalExamPage";
			
		} 
		catch (Exception e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
	  		ActionErrors errors = new ActionErrors();
			errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.system.exception", e.getMessage()));
			saveErrors(request, errors); 
			page = "createMedicalExamPage";
	  	  
		}
		finally{
			closeConnection(conn);
		}

		LOGGER.info("handleAction end");
		LOGGER.debug("PAGE FORWARD-->" + page);
		return (mapping.findForward(page));
	}
	
	private void closeConnection(Connection conn) throws SQLException {
		
		if (!conn.isClosed()){
			conn.close();
		}
		
	}
	
	private void getMedicalExamRecord(MedicalRecordDetailForm medicalRecordDetailForm, MedicalRecordData medicalRecordData, HttpServletRequest request, Connection conn) throws Exception {

		LOGGER.info("getMedicalExamRecord start");
		if (!medicalRecordDetailForm.getActionType().equalsIgnoreCase("create")){
			if (medicalRecordDetailForm.getMedicalRecordId() != null){
				if (!medicalRecordDetailForm.getMedicalRecordId().equals("")){
					medicalRecordData.setMedicalRecordId(Long.parseLong(medicalRecordDetailForm.getMedicalRecordId()));
				}
			}
		}
		
		if (!medicalRecordDetailForm.getReferenceNum().equals("") && !medicalRecordDetailForm.getReferenceNum().equals(medicalRecordDetailForm.getMedicalRecordId()))
			medicalRecordData.setReferenceNo(medicalRecordDetailForm.getReferenceNum().toUpperCase());
						
		PolicyMedicalRecordData policyMedicalRecordData = new PolicyMedicalRecordData();
		if (!medicalRecordDetailForm.getRefNo().equals("")){
			policyMedicalRecordData.setReferenceNumber(medicalRecordDetailForm.getRefNo().toUpperCase());
			medicalRecordData.setReferenceNo(medicalRecordDetailForm.getRefNo().toUpperCase());
			
		
		}
		medicalRecordData.setPolicyMedicalRecordData(policyMedicalRecordData);		
		
		ClientData clientData = new ClientData();
		clientData.setClientId(medicalRecordDetailForm.getClientNo().toUpperCase());
		if (!medicalRecordDetailForm.getBirthDate().equals(""))
			clientData.setBirthDate(DateHelper.parse(medicalRecordDetailForm.getBirthDate(),"ddMMMyyyy"));
		clientData.setGivenName(medicalRecordDetailForm.getFirstName());
		clientData.setLastName(medicalRecordDetailForm.getLastName());
		medicalRecordData.setClient(clientData);
		
		UserProfileData userProfileData = new UserProfileData();
		userProfileData.setUserId(medicalRecordDetailForm.getAgent());
		medicalRecordData.setAgent(userProfileData);
		
		StateHandler sh = new StateHandler();
		UserData ud = sh.getUserData(request);
		userProfileData = ud.getProfile();
		medicalRecordData.setCreatedBy(userProfileData.getUserId());
				
		medicalRecordData.setUpdatedBy(userProfileData.getUserId()); 
		
		SunLifeOfficeData sunLifeOfficeData =  new SunLifeOfficeData();
		sunLifeOfficeData.setOfficeId(medicalRecordDetailForm.getBranch());
		medicalRecordData.setBranch(sunLifeOfficeData);
		
		StatusData statusData = new StatusData(); 
		if (!medicalRecordDetailForm.getStatus().equals(""))
			statusData.setStatusId(Long.parseLong(medicalRecordDetailForm.getStatus()));
		medicalRecordData.setStatus(statusData);
		
		StatusCHDao statusCHDao = new StatusCHDao();
		StatusData applicationStatus = new StatusData();
		if (!medicalRecordDetailForm.getApplicationStatus().equals("")){
			applicationStatus.setStatusId(Long.parseLong(medicalRecordDetailForm.getApplicationStatus()));
			applicationStatus.setStatusDesc(statusCHDao.getDescription(medicalRecordDetailForm.getApplicationStatus()));			
		}
		else{
			applicationStatus.setStatusId(IUMConstants.STATUS_NOT_PROCEEDED_WITH);
			applicationStatus.setStatusDesc(statusCHDao.getDescription(Long.toString(IUMConstants.STATUS_NOT_PROCEEDED_WITH)));
		}
		medicalRecordData.setApplicationStatus(applicationStatus);		
		
		if (medicalRecordDetailForm.getType().equalsIgnoreCase("Y")){
			LaboratoryData laboratoryData = new LaboratoryData();
			if (!medicalRecordDetailForm.getLabName().equals(""))
				laboratoryData.setLabId(Long.parseLong(medicalRecordDetailForm.getLabName()));
			medicalRecordData.setLaboratory(laboratoryData);
			medicalRecordData.setLabTestInd("L"); 
		}
		else if (medicalRecordDetailForm.getType().equalsIgnoreCase("N")){			
			ExaminerData examinerData = new ExaminerData();
			examinerData.setExaminerId(Long.parseLong(medicalRecordDetailForm.getExaminer()));
			medicalRecordData.setExaminer(examinerData);
			medicalRecordData.setLabTestInd("M"); 		
		}		
				
		ExaminationPlaceData examinationPlaceData = new ExaminationPlaceData();
		if (!medicalRecordDetailForm.getExamPlace().equals(""))
			examinationPlaceData.setExaminationPlaceId(Long.parseLong(medicalRecordDetailForm.getExamPlace()));
		medicalRecordData.setExaminationPlace(examinationPlaceData);
		
		TestProfileData testProfileData = new TestProfileData();
		
		if (!medicalRecordDetailForm.getTest().equals("")){
			testProfileData.setTestId(Long.parseLong(medicalRecordDetailForm.getTest()));
		
			if (!medicalRecordDetailForm.getDateConducted().equals("")){
				TestProfileDAO testProfileDAO = new TestProfileDAO(conn);
				testProfileData = testProfileDAO.retrieveTestProfile(Long.parseLong(medicalRecordDetailForm.getTest()));
				if (testProfileData != null){
					medicalRecordData.setValidityDate(this.add(medicalRecordDetailForm.getDateConducted(), testProfileData.getValidity()));
				}					
			}
		}
		medicalRecordData.setTest(testProfileData);
		
		if (!medicalRecordDetailForm.getAppointmentDate().equals("")){			
			if (!medicalRecordDetailForm.getAppointmentTime().equals(""))
				medicalRecordData.setAppointmentDateTime(DateHelper.parse((medicalRecordDetailForm.getAppointmentDate() + " " +medicalRecordDetailForm.getAppointmentTime()), "ddMMMyyyy hh:mm aaa"));
			else
				medicalRecordData.setAppointmentDateTime(DateHelper.parse(medicalRecordDetailForm.getAppointmentDate(),"ddMMMyyyy"));
					
		}
		if (!medicalRecordDetailForm.getDateConducted().equals(""))
			medicalRecordData.setConductedDate(DateHelper.parse(medicalRecordDetailForm.getDateConducted(),"ddMMMyyyy"));
		if (!medicalRecordDetailForm.getDateReceived().equals(""))
			medicalRecordData.setReceivedDate(DateHelper.parse(medicalRecordDetailForm.getDateReceived(),"ddMMMyyyy"));
		if (!medicalRecordDetailForm.getFollowUpNumber().equals(""))
			medicalRecordData.setFollowUpNumber(Long.parseLong(medicalRecordDetailForm.getFollowUpNumber())); 
		if (!medicalRecordDetailForm.getFollowUpDate().equals(""))
			medicalRecordData.setFollowUpDate(DateHelper.parse(medicalRecordDetailForm.getFollowUpDate(),"ddMMMyyyy")); 
		medicalRecordData.setRemarks(medicalRecordDetailForm.getRemarks());


		if (medicalRecordDetailForm.getSevenDayMemo().equalsIgnoreCase("Y")){
			medicalRecordData.setSevenDayMemoInd(true);
		}
		else if (medicalRecordDetailForm.getSevenDayMemo().equalsIgnoreCase("N")){
			medicalRecordData.setSevenDayMemoInd(false);
		}
		if (!medicalRecordDetailForm.getSevenDayMemoDate().equals(""))
			medicalRecordData.setSevenDayMemoDate(DateHelper.parse(medicalRecordDetailForm.getSevenDayMemoDate(),"ddMMMyyyy"));
		medicalRecordData.setReasonForReqt(medicalRecordDetailForm.getReason());
		
		SectionCHDAO sectionCHDAO = new SectionCHDAO(conn);
		SectionData sectionData = new SectionData();
		if (!medicalRecordDetailForm.getSection().equals("")){			  		
			sectionData.setSectionId(medicalRecordDetailForm.getSection());
			ArrayList list = (ArrayList) sectionCHDAO.getCodeValue(medicalRecordDetailForm.getSection());
			NameValuePair section = (NameValuePair) list.get(0);			
			sectionData.setSectionDesc(section.getName());
		}
		else{
			sectionData.setSectionId(IUMConstants.DEFAULT_SECTION_ID);
			ArrayList list = (ArrayList) sectionCHDAO.getCodeValue(IUMConstants.DEFAULT_SECTION_ID);
			if (list.size() > 0){
				NameValuePair section = (NameValuePair) list.get(0);			
				sectionData.setSectionDesc(section.getName());
			}
			else{
				sectionData.setSectionId("");
				sectionData.setSectionDesc("");
			}
		}
		medicalRecordData.setSection(sectionData);
		medicalRecordData.setRequestingParty(medicalRecordDetailForm.getReqParty());
		
		SunLifeDeptData sunLifeDeptData = new SunLifeDeptData();
		sunLifeDeptData.setDeptId(medicalRecordDetailForm.getDepartment());
		medicalRecordData.setDepartment(sunLifeDeptData);
		
		medicalRecordData.setChargableTo(medicalRecordDetailForm.getChargeTo());
		if (medicalRecordDetailForm.getPayAgent().equalsIgnoreCase("Y")){
			medicalRecordData.setPaidByAgentInd(true);
		}
		else if (medicalRecordDetailForm.getPayAgent().equalsIgnoreCase("N")){
			medicalRecordData.setPaidByAgentInd(false);
		}
		
		if (!medicalRecordDetailForm.getAmount().equals(""))
			medicalRecordData.setAmount(Double.parseDouble(medicalRecordDetailForm.getAmount()));
		
			
		if (medicalRecordDetailForm.getRequestForLOAInd().equalsIgnoreCase("Y"))
			medicalRecordData.setRequestForLOAInd(true);
		else if (medicalRecordDetailForm.getRequestForLOAInd().equalsIgnoreCase("N"))
			medicalRecordData.setRequestForLOAInd(false);

		
		if (medicalRecordDetailForm.getResultsReceivedInd().equalsIgnoreCase("Y"))
			medicalRecordData.setReceivedResults(true);
		else if (medicalRecordDetailForm.getResultsReceivedInd().equalsIgnoreCase("N")) 
			medicalRecordData.setReceivedResults(false);
		
		if (!medicalRecordDetailForm.getDateRequested().equals(""))
			medicalRecordData.setRequestedDate(DateHelper.parse(medicalRecordDetailForm.getDateRequested(),"ddMMMyyyy"));
		
		medicalRecordData.setCreateDate(DateHelper.parse(DateHelper.getCurrentDate(), "ddMMMyyyy"));
		
		
		
		if (!medicalRecordDetailForm.getReferenceNum().equals(""))
			medicalRecordData.setFacilitator(medicalRecordDetailForm.getFacilitator());
		
		LOGGER.info("getMedicalExamRecord end");
	}
	
	private Date add(String strDate, int noOfDays){
		
		LOGGER.info("add start");
		String [] months = {"JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC"};
	  	
	  	String dateConducted = strDate;
		String day = dateConducted.substring(0, 2);
      	String month = dateConducted.substring(2, 5);
      	String yr = dateConducted.substring(5,9);
      	
      	int mo = 0;
      	int i = 0;
      	for (i=0; i < months.length-1; i++) {
        	if (months[i].equals(month)) {
          		mo = i+1;        
          		break;
        	}
      	}      		
      
		Date validityDate = DateHelper.add(DateHelper.parse(mo + "/" + day + "/" + yr), Calendar.DAY_OF_MONTH,  (int) noOfDays);
		LOGGER.info("add end");
		return validityDate;		
	}
	
	

}
