package com.slocpi.ium.ui.action;

import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.slocpi.ium.IUMException;
import com.slocpi.ium.data.UserData;
import com.slocpi.ium.data.UserProfileData;
import com.slocpi.ium.ui.form.ExaminerForm;
import com.slocpi.ium.ui.util.IUMWebException;
import com.slocpi.ium.ui.util.Page;
import com.slocpi.ium.ui.util.Pagination;
import com.slocpi.ium.ui.util.StateHandler;
import com.slocpi.ium.underwriter.Reference;
import com.slocpi.ium.util.CodeHelper;

/**
 * ListExaminersAction.java 
 * Description:
 *
 * @version $Revision: 1.1 $
 * @author $Author: pw12 $ $Date: 2012/08/01 07:07:16 $
 */
public class ListExaminersAction extends IUMAction{
	private static final Logger LOGGER = LoggerFactory.getLogger(ListExaminersAction.class);
	/**
	 * @see com.slocpi.ium.ui.action.IUMAction#handleAction(org.apache.struts.action.ActionMapping, org.apache.struts.action.ActionForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	public ActionForward handleAction(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response)
		throws Exception {
		
		LOGGER.info("handleAction start");
		String page = "";
		String error = (String)request.getAttribute("error");
		
		try{
			
			StateHandler sh = new StateHandler();
			UserData ud = sh.getUserData(request);
			UserProfileData profile = ud.getProfile();
			
			ExaminerForm examinerForm = (ExaminerForm) form;
					
			ArrayList examinerList = this.populateExaminerList(); 
								
			Page pg = this.createPage(request, profile, examinerList,examinerForm);
			String pageNum = examinerForm.getPageNo();
						
			if(examinerForm.getStatus().equals("M") || examinerForm.getStatus().equals("C") || (error!=null)){
				if(error != null){
					examinerForm.setStatus("C");
				}
				request.setAttribute("examiner",examinerForm);	
			}else{
				ExaminerForm eForm = new ExaminerForm();
				eForm.setPageNo(pageNum);
				eForm.setStatus("L");
				request.setAttribute("examiner",eForm);
			} 
			
			page = "listExaminersPage";
		}catch(Exception e){
			LOGGER.error(CodeHelper.getStackTrace(e));
			ActionErrors errors = new ActionErrors();
			errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.system.exception", e.getMessage()));
			saveErrors(request, errors);
			page = "errorPage";
		}
		LOGGER.info("handleAction end");
		LOGGER.debug("PAGE FORWARD-->" + page);
		return (mapping.findForward(page));
	}
	
	/*
	 * returns an ArrayList of ExaminerData objects.
	 */
	private ArrayList populateExaminerList() throws IUMException {
		
		LOGGER.info("populateExaminerList start");
		Reference mgr = new Reference();
		ArrayList examList = mgr.getExaminers();
		LOGGER.info("populateExaminerList end");
		
		return examList;
	}
	
	/*
	 * returns the Page object that contains navigation links (pages) and the current list of Examiners
	 */
	private Page createPage(HttpServletRequest request, UserProfileData profile, ArrayList examinerList,ExaminerForm examinerForm) throws IUMWebException
	{
		LOGGER.info("createPage start");
		String reqPageNum = examinerForm.getPageNo();
		
		int recPerView = profile.getRecordsPerView();
		
		if (recPerView == 0) {
		  recPerView = 1;
		}
		
		if(reqPageNum==null || reqPageNum.trim().equals("")){
			LOGGER.warn("pagenumber is null or equal to blank");
			examinerForm.setPageNo("1");
		}
									
		Pagination pgn = new Pagination(examinerList, recPerView);
		Page pg = pgn.getPage(Integer.parseInt(examinerForm.getPageNo()));			
	
	    request.setAttribute("page", pg);
	    LOGGER.info("createPage end");			
		return pg;
	}
 	
	
}
