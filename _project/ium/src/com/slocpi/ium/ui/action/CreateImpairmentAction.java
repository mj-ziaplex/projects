package com.slocpi.ium.ui.action;

// struts package
import java.io.IOException;
import java.sql.SQLException;
import java.util.Date;
import java.util.ResourceBundle;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.slocpi.ium.IUMException;
import com.slocpi.ium.data.AssessmentRequestData;
import com.slocpi.ium.data.ClientData;
import com.slocpi.ium.data.ImpairmentData;
import com.slocpi.ium.data.UWAssessmentData;
import com.slocpi.ium.data.UserData;
import com.slocpi.ium.data.UserProfileData;
import com.slocpi.ium.data.dao.ClientDAO;
import com.slocpi.ium.data.util.DataSourceProxy;
import com.slocpi.ium.mib.dao.impl.MibImpairmentDaoHelper;
import com.slocpi.ium.mib.service.MibImpairmentService;
import com.slocpi.ium.mib.service.impl.MibImpairmentServiceImpl;
import com.slocpi.ium.ui.form.ImpairmentForm;
import com.slocpi.ium.ui.util.StateHandler;
import com.slocpi.ium.underwriter.AssessmentRequest;
import com.slocpi.ium.underwriter.UnderWriterException;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.DateHelper;
import com.slocpi.ium.util.IUMConstants;


/**
 * @TODO Class Description CreateImpairmentAction
 * @author: Ma. Theresa Vicencio
 * @version: 1.0
 */
public class CreateImpairmentAction extends IUMAction {
	private static final Logger LOGGER = LoggerFactory.getLogger(CreateImpairmentAction.class);
  /**
   * @TODO method description for execute
   * @param mapping mapping of request to an instance of this class
   * @param form  object
   * @param request request object
   * @param response response object
   * @exception IOException if an input/output error occurs
   * @exception ServletException if a servlet exception occurs
   * @return ActionForward as defined in the mapping parameter.
   */
  public ActionForward handleAction(ActionMapping mapping, 
							   ActionForm form, 
							   HttpServletRequest request, 
							   HttpServletResponse response)
							   throws Exception {
	  
	LOGGER.info("handleAction start");
	String page = "";
	try {
		
		StateHandler sh = new StateHandler();
		UserData ud = sh.getUserData(request);
		UserProfileData profile = ud.getProfile();
		String userId = profile.getUserId();
		String pageId = "";
		boolean wms = false;
		
		String session_id = request.getParameter("session_id").toString();
		request.setAttribute("session_id", session_id);
		
		ResourceBundle rb = ResourceBundle.getBundle(IUMConstants.IUM_WMS_CONFIG);
		String contextPath = rb.getString("wms_context_path");
		if(request.getContextPath().equalsIgnoreCase(contextPath)){
			wms = true;
		}
		
		ImpairmentForm impairmentForm = (ImpairmentForm)form;
		
		String clientType = request.getParameter("clientType");
		if (clientType == null) {	
			clientType = impairmentForm.getInsuredClientType();
		}
		
		impairmentForm = getClientNumbers(impairmentForm);
				
		String iClientNo = impairmentForm.getInsuredClientNo();
		String oClientNo = impairmentForm.getOwnerClientNo();
		
		String clientNo = iClientNo;
		if (clientType.equals(IUMConstants.CLIENT_TYPE_OWNER)) {
			clientNo = oClientNo;
		}

		String savedSection = (request.getParameter("savedSection") != null) ? request.getParameter("savedSection").trim(): "";
		
		try {
			HttpSession session = request.getSession();
			if (session == null) {
				LOGGER.debug("request.getSession() is null");
			} else {
				LOGGER.debug("request.getSession() is not null");
				session.setAttribute("savedSection_" + session_id, savedSection);
				String temp = (String) session.getAttribute("savedSection_" + session_id);
				String temp2 = (temp != null) ? temp.trim(): "session object savedSection is not saved" ;
			}
		} catch (Exception e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
		}
		
		ActionErrors errors;
		if ((clientType.equals(IUMConstants.CLIENT_TYPE_INSURED)) && (iClientNo == null)) {
			errors = new ActionErrors();
			errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.request.generic", "There is no existing insured client."));
			saveErrors(request, errors);
		}
		else if ((clientType.equals(IUMConstants.CLIENT_TYPE_OWNER)) && (oClientNo == null)) {
			errors = new ActionErrors();
			errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.request.generic", "There is no existing owner client."));
			saveErrors(request, errors);
		}
		else {
			UserData userData = new StateHandler().getUserData(request);
			UserProfileData userLogIn = userData.getProfile();
			String user = userLogIn.getUserId();
			
			String polSuffix = request.getParameter("polSuffix");
			String impClientType = request.getParameter("impClientType");
			if (impClientType != null && !"".equals(impClientType)) {
				if (IUMConstants.CLIENT_TYPE_INSURED.equals(impClientType)) {
					clientNo = iClientNo;
				} else if (IUMConstants.CLIENT_TYPE_OWNER.equals(impClientType)) {
					clientNo = oClientNo;
				}
			}
			impairmentForm.setPolicySuffix(polSuffix);
			saveImpairmentRecord(impairmentForm, userId, clientNo, wms, user, impClientType);		
		}
		
		page = "cdsDetailPage";
	
	} 
	catch (UnderWriterException e) {
	  ActionErrors errors = new ActionErrors();
	  errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.request.generic", e.getMessage()));
	  saveErrors(request, errors);
	  page = "cdsDetailPage";
	}
	catch (IUMException e) {
	  ActionErrors errors = new ActionErrors();
	  errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.system.exception", e.getMessage()));
	  saveErrors(request, errors);
	  page = "errorPage";
	}
	LOGGER.info("handleAction end");
	LOGGER.debug("PAGE FORWARD-->" + page);
	return (mapping.findForward(page));
  }


  private void saveImpairmentRecord(ImpairmentForm impairmentForm, String userId, String clientNo, boolean wms, String user, String impClientType) throws Exception {
	  
	LOGGER.info("saveImpairmentRecord start");
    ImpairmentData data = new ImpairmentData();
    data.setClientNumber(clientNo);
    data.setReferenceNumber(impairmentForm.getRefNo().concat(impairmentForm.getPolicySuffix()));
    data.setRelationship(impairmentForm.getRelationship());
    data.setImpairmentOrigin(impairmentForm.getImpairmentOrigin());
    data.setImpairmentDate(DateHelper.sqlDate(DateHelper.parse(impairmentForm.getImpairmentDate(), "ddMMMyyyy")));
    data.setActionCode(impairmentForm.getActionCode());
    
    double heightInFeet;
    double heightInInches;
	if (impairmentForm.getHeightInFeet() == null || impairmentForm.getHeightInFeet().trim().equals("") || impairmentForm.getHeightInFeet().equals("undefined")){
		heightInFeet = 0;
	} else {
		heightInFeet =  Double.parseDouble(impairmentForm.getHeightInFeet());
	}
    
	if (impairmentForm.getHeightInInches() == null || impairmentForm.getHeightInInches().trim().equals("") || impairmentForm.getHeightInInches().equals("undefined")){ 
		heightInInches = 0; 
	}else {
		heightInInches = Double.parseDouble(impairmentForm.getHeightInInches());
	}
			
	data.setHeightInFeet(heightInFeet);
	data.setHeightInInches(heightInInches);
	
	double weight;
	if (impairmentForm.getWeight() == null || impairmentForm.getWeight().trim().equals("")){
		weight = 0;
	} else {
		weight = (Double.parseDouble(impairmentForm.getWeight()));
	}
	
	data.setWeight(weight);
	
	if (impairmentForm.getBloodPressure() == null){
		data.setBloodPressure("");
	}else {
		data.setBloodPressure(impairmentForm.getBloodPressure());
	}
	
	data.setConfirmation(impairmentForm.getConfirm());
	data.setCreatedBy(userId);
	data.setCreateDate(new Date());
	data.setImpairmentCode(impairmentForm.getCode());
		
	MibImpairmentService mibImpService = new MibImpairmentServiceImpl();
	
	if (!mibImpService.isImpairmentExisting(data, impClientType)) {
		ClientData clientData = getClientData(data);
		data.setUpdatedBy(user);
		mibImpService.addImpairment(clientData, data, impClientType);

		UWAssessmentData uwData = new UWAssessmentData();
		uwData.setReferenceNumber(impairmentForm.getRefNo());
		uwData.setFinalDecision(impairmentForm.getUWFinalDecision());
		uwData.setPostedBy(user);
		uwData.setPostedDate(new Date());
		
		String fn = clientData.getGivenName()!=null && !clientData.getGivenName().equals("") ? clientData.getGivenName() : "";
		String mn = clientData.getMiddleName()!=null && !clientData.getMiddleName().equals("") ? clientData.getMiddleName() : "";
		String ln = clientData.getLastName()!=null && !clientData.getLastName().equals("") ? clientData.getLastName() : "";
		
		String fullName = "";
		
		if(!fn.equals("") && !ln.equals("") && !mn.equals("")){
			fullName = fn+" "+mn+" "+ln;
		}else if (mn.equals("")){
			fullName = fn+" "+ln;
		}
		
		String stat = "";
		if(!MibImpairmentDaoHelper.getRemarks(data).trim().equals("0.0 0.0 0.0")){
			stat = " Stat: " + MibImpairmentDaoHelper.getRemarks(data);
			if(stat.trim().equals(" Stat: 0.0 0.0 0.0")){
				stat = "";
			}
		}
		
		String confirm = "";
		if(!data.getConfirmation().equals("")){
			confirm = " Confirm: " + data.getConfirmation();
		}
		
		uwData.setAnalysis("MIB Reported -" +
				" Client Name: " + fullName+
				" Client Type: " + impClientType + 
				"\n" +
				" Impairment: " + data.getImpairmentCode() + 
				" Relationship: " + data.getRelationship() + 
				" Origin: " + data.getImpairmentOrigin() +
				" Impairment Date: " + DateHelper.format(data.getImpairmentDate(), DateHelper.MIB_PATTERN) +
				stat + 
				confirm);			
		AssessmentRequest ar = new AssessmentRequest();
		ar.addAnalysis(uwData);
	}

	LOGGER.info("saveImpairmentRecord end");
  }


  public ClientData getClientData(ImpairmentData data) throws SQLException {
	  
	  LOGGER.info("getClientData start");
	  ClientDAO clientDao = new ClientDAO();
	  ClientData clientData = null;
	  try {
		  clientData = clientDao.retrieveClient(data.getClientNumber());
		  if (hasBirthLocation(clientData)) {
			  ClientData dataWithBirthLoc = clientDao.retrieveClientBirthLocation(data.getClientNumber());
			  if (dataWithBirthLoc != null) {
				  clientData = dataWithBirthLoc;
			  }
		  }
	  } catch (SQLException e) {
		  LOGGER.error(CodeHelper.getStackTrace(e));
		  throw e;
	  } 	  
	  
	  LOGGER.info("getClientData end");
	  return clientData;
  }

  private boolean hasBirthLocation(ClientData clientData) {
	  
	  LOGGER.info("hasBirthLocation start");
	  final String EMPTY = "";
	  String birthLoc = clientData.getBirthLocation();
	  if (!EMPTY.equals(birthLoc) && birthLoc != null) {
		  LOGGER.info("hasBirthLocation end");
		  return true;
	  }
	  LOGGER.info("hasBirthLocation end");
	  return false;
  }
  
  private ImpairmentForm getClientNumbers(ImpairmentForm impForm) throws IUMException {
	
	  LOGGER.info("getClientNumbers start");
	AssessmentRequest assessmentRequest = new AssessmentRequest();
	AssessmentRequestData requestData = assessmentRequest.getDetails(impForm.getRefNo());

	impForm.setInsuredClientNo(requestData.getInsured().getClientId());
	impForm.setOwnerClientNo(requestData.getOwner().getClientId());

	LOGGER.info("getClientNumbers end");
	return (impForm);  
  }
 
}

