/*
 * Created on Apr 20, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.ui.action;

import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.slocpi.ium.IUMException;
import com.slocpi.ium.data.UserData;
import com.slocpi.ium.data.UserProfileData;
import com.slocpi.ium.ui.form.RequestFilterForm;
import com.slocpi.ium.ui.form.UserLoadListForm;
import com.slocpi.ium.ui.util.StateHandler;
import com.slocpi.ium.underwriter.AssessmentRequest;
import com.slocpi.ium.util.CodeHelper;

/**
 * @author Cris
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class MultipleForwardToRecordAction extends IUMAction{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(MultipleForwardToRecordAction.class);
	
	public ActionForward handleAction (ActionMapping mapping, 
											  ActionForm form, 
											  HttpServletRequest request,
											  HttpServletResponse response){
		
		LOGGER.info("handleAction start");
		String page = "";
				
		RequestFilterForm rlForm = (RequestFilterForm) form;
	
		String [] requests = rlForm.getCheckedRefNum();
		
		UserData userData = new StateHandler().getUserData(request);
		UserProfileData userLogIn = userData.getProfile();
		String user = userLogIn.getUserId(); 
				
		AssessmentRequest ar = new AssessmentRequest();
		ArrayList failedRequest = new ArrayList();
		for (int i=0; i<requests.length; i++){
			String referenceNumber  = requests[i];
			try {
				ar.forwardToRecords(referenceNumber, user);
			} catch (IUMException e) {
				LOGGER.error(CodeHelper.getStackTrace(e));
				failedRequest.add(referenceNumber + ": " +  e.getMessage() + "<br>&nbsp;");
			}
		}
		
		if (failedRequest.size() >=1){
			StringBuffer errorMessage = new StringBuffer();
			for (int i=0; i < failedRequest.size(); i++){
				String message = (String) failedRequest.get(i);
				errorMessage.append(message);
			}
			constructError("error.submitRequest",errorMessage.toString(),request);
			page = "error";
		} else {
			page = "requestListPage";
		}
		
		rlForm.setAssignedTo(user);
		ViewAssessmentRequestListAction vl = new ViewAssessmentRequestListAction();
		try {
			vl.handleAction(mapping, rlForm, request, response);
			
			// Added by BJ "Bujo" Taduran for requests.jsp
			ViewAssessmentRequestListAction uwLoad = new ViewAssessmentRequestListAction();
			UserLoadListForm userLoadListForm = uwLoad.ListUnderwriterLoad();
			request.setAttribute("userLoadListForm",userLoadListForm);
			
		} catch (Exception e1) {
			LOGGER.error(CodeHelper.getStackTrace(e1));
		}
		
		LOGGER.info("handleAction end");
		LOGGER.debug("PAGE FORWARD-->" + page);
		return mapping.findForward(page);
	}
	

	
	private String constructError (String key, String parameter, HttpServletRequest request){
		
		LOGGER.info("constructError start");
		String result = "error";
		ActionErrors error = new ActionErrors();
		error.add(ActionErrors.GLOBAL_ERROR, new ActionError(key, parameter));
		saveErrors(request, error);
		LOGGER.info("constructError end");
		return result;
	}

}
