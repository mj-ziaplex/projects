package com.slocpi.ium.ui.action;


import java.io.IOException;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.slocpi.ium.IUMException;
import com.slocpi.ium.data.AssessmentRequestData;
import com.slocpi.ium.data.UserData;
import com.slocpi.ium.data.UserProfileData;
import com.slocpi.ium.ui.form.AssessmentRequestForm;
import com.slocpi.ium.ui.util.StateHandler;
import com.slocpi.ium.underwriter.AssessmentRequest;
import com.slocpi.ium.underwriter.UnderWriterException;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.IUMConstants;


/**
 * @TODO Class Description UpdateLocationAction
 * @author: Ma. Theresa Vicencio
 * @version: 1.0
 */
public class UpdateLocationAction extends IUMAction {
  
	private static final Logger LOGGER = LoggerFactory.getLogger(UpdateLocationAction.class);
  /**
   * @TODO method description for execute
   * @param mapping mapping of request to an instance of this class
   * @param form  object
   * @param request request object
   * @param response response object
   * @exception IOException if an input/output error occurs
   * @exception ServletException if a servlet exception occurs
   * @return ActionForward as defined in the mapping parameter.
   */
  public ActionForward handleAction(ActionMapping mapping, 
							   ActionForm form, 
							   HttpServletRequest request, 
							   HttpServletResponse response)
							   throws Exception {
	  
	LOGGER.info("handleAction start");
	String page = "";
	try {
		
		StateHandler sh = new StateHandler();
		UserData ud = sh.getUserData(request);
		UserProfileData profile = ud.getProfile();
		String userId = profile.getUserId();
		String pageId = "";
		String updateType = request.getParameter("updateType");
		
		AssessmentRequestForm arForm = (AssessmentRequestForm)form;
		if (updateType != null && updateType.equals(IUMConstants.PROCESS_TYPE_MULTIPLE)) {
			String [] refNos = arForm.getCheckedRefNum();
			saveMultipleLocation(refNos, userId);
			page = "listAssessmentRequests";
		}
		else {
			String refNo = arForm.getRefNo();
			saveLocation(refNo, userId);
			page = "viewAssessmentRequest";
		}	
	} 
	catch (UnderWriterException e) {
		LOGGER.error(CodeHelper.getStackTrace(e));
		ActionErrors errors = new ActionErrors();
		errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.request.generic", e.getMessage()));
		saveErrors(request, errors);
		
		String updateType = request.getParameter("updateType");
		if (updateType != null && updateType.equals(IUMConstants.PROCESS_TYPE_MULTIPLE)) {
			page = "listAssessmentRequests";
		}
		else {
			page = "viewAssessmentRequest";
		}
	}
	catch (IUMException e) {
		LOGGER.error(CodeHelper.getStackTrace(e));
		ActionErrors errors = new ActionErrors();
		errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.system.exception", e.getMessage()));
		saveErrors(request, errors);
		page = "errorPage";
	}
	catch (Exception e) {
		LOGGER.error(CodeHelper.getStackTrace(e));
		ActionErrors errors = new ActionErrors();
		errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.system.exception", e.getMessage()));
		saveErrors(request, errors);
		page = "errorPage";
	}
	LOGGER.info("handleAction end");
	LOGGER.debug("PAGE FORWARD-->" + page);
	
	return (mapping.findForward(page));
  }


  private void saveLocation(String refNo, String userId) throws Exception {
	 
	LOGGER.info("saveLocation start"); 
    AssessmentRequest ar = new AssessmentRequest();
	AssessmentRequestData arData = ar.getDetails(refNo);
	String assignedTo = arData.getAssignedTo().getUserId();
	if (userId.equals(assignedTo)) {
	  ar.setLocation(refNo, userId, userId);
	}
	LOGGER.info("saveLocation end");
  }


  private void saveMultipleLocation(String[] refNos, String userId) throws Exception {
	  
	LOGGER.info("saveMultipleLocation start");     
	AssessmentRequest ar = new AssessmentRequest();	
	ArrayList errors = new ArrayList();

	int count = refNos.length;
	for (int i=0; i<count; i++) {
	  String refNo = refNos[i];	  
	  AssessmentRequestData arData = ar.getDetails(refNo);	  
	  String assignedTo = arData.getAssignedTo().getUserId();
	  
	  if (userId.equals(assignedTo)) {		
		long currentStatus = arData.getStatus().getStatusId();
		if ((currentStatus == IUMConstants.STATUS_APPROVED) ||  
			(currentStatus == IUMConstants.STATUS_AR_CANCELLED) ||
			(currentStatus == IUMConstants.STATUS_DECLINED) ||
			(currentStatus == IUMConstants.STATUS_NOT_PROCEEDED_WITH)) {
			ResourceBundle rb = ResourceBundle.getBundle(IUMConstants.IUM_APPLICATION_RESOURCES);
			String message = rb.getString("message.location");
			errors.add(refNo + ": " + message + "<br>&nbsp;");
		}
		else {
			ar.setLocation(refNo, userId, userId);
		}	    
	  }
	}

	int errorCount = errors.size();
	if (errorCount > 0)	{		
		StringBuffer errorMessage = new StringBuffer();
		for (int i = 0; i < errorCount; i++){
			String message = (String) errors.get(i);
			errorMessage.append(message);
		}
		LOGGER.debug(errorMessage.toString());
		throw new UnderWriterException(errorMessage.toString());
	}
			
	LOGGER.info("saveMultipleLocation end");     
  }


 
}

