/*
 * Created on Jan 7, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.ui.action;


import java.sql.SQLException;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.slocpi.ium.data.UserAccessData;
import com.slocpi.ium.data.UserData;
import com.slocpi.ium.data.UserProfileData;
import com.slocpi.ium.interfaces.mib.MIBController;
import com.slocpi.ium.ui.form.MIBImpairmentsForm;
import com.slocpi.ium.ui.util.StateHandler;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.DateHelper;

/**
 * @author Ingrid Villanueva
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class SetUpMIBImpairmentsAction extends IUMAction{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(SetUpMIBImpairmentsAction.class);
	public ActionForward handleAction (ActionMapping mapping, 
									  ActionForm form, 
									  HttpServletRequest request,
									  HttpServletResponse response)
									  throws Exception
									  {
		
		LOGGER.info("handleAction start");
        String EXPORT = "export"; 
		String page   = "";
		MIBImpairmentsForm mibForm = (MIBImpairmentsForm) form;
        								
		try {
			StateHandler sh = new StateHandler();
			UserData ud = sh.getUserData(request);
			UserProfileData profile = ud.getProfile();
			String userId = profile.getUserId();
			String pageId = "";
			UserAccessData uad = extractAccessPriveleges(userId, pageId);
		
			if (uad.getAccessCode().equals("W")) {
		
				String mode = request.getParameter("mode");
			    if (mode.equals(EXPORT)) {			    
					exportMIBImpairments(mibForm, userId);
			    }
				request.setAttribute("mibImpairmentsForm", mibForm);
				page = "mibImpairmentsPage";				
			}
			else {
				page = "errorPage";
			}
		}
		catch(Exception e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			page = "mibImpairmentsPage";
			 			
		}
		LOGGER.info("handleAction end");
		LOGGER.debug("PAGE FORWARD-->" + page);
		return mapping.findForward(page);
	}

	private void exportMIBImpairments(MIBImpairmentsForm form, String userId) throws SQLException, Exception {
		
		LOGGER.info("exportMIBImpairments start");
		MIBController mib       = new MIBController();
		Date          startDate = DateHelper.parse(form.getStartDate(),"ddMMMyyyy");
		Date          endDate   = DateHelper.parse(form.getEndDate(),"ddMMMyyyy"); 	
		/*mib.export(startDate, endDate, userId);*/
		LOGGER.info("exportMIBImpairments end");
		
	}
}


