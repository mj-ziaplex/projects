/*
 * Created on Mar 18, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.ui.action;

import java.io.ByteArrayOutputStream;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.slocpi.ium.data.ARWithPendingMedFilterData;
import com.slocpi.ium.data.ExaminerData;
import com.slocpi.ium.data.LaboratoryData;
import com.slocpi.ium.data.SunLifeOfficeData;
import com.slocpi.ium.data.UserData;
import com.slocpi.ium.data.UserProfileData;
import com.slocpi.ium.reports.ApplicationsWithPendMedReport;
import com.slocpi.ium.ui.util.StateHandler;
import com.slocpi.ium.util.CodeHelper;

/**
 * @author Nic Decapia
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class GenerateReportFollowMedPrintAction extends IUMAction
{
	private static final Logger LOGGER = LoggerFactory.getLogger(GenerateReportFollowMedPrintAction.class);
	/* (non-Javadoc)
	 * @see com.slocpi.ium.ui.action.IUMAction#handleAction(org.apache.struts.action.ActionMapping, org.apache.struts.action.ActionForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	public ActionForward handleAction(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response)
		throws Exception
	{
		
		LOGGER.info("handleAction start");
		String page = "";
			
		try {
		
			StateHandler sessionHandler = new StateHandler();
			UserData userData = sessionHandler.getUserData(request);
			UserProfileData userPref = userData.getProfile();
			
			ARWithPendingMedFilterData filter = extractFilter(request);
			ApplicationsWithPendMedReport rpt = new ApplicationsWithPendMedReport();
			ByteArrayOutputStream baos =  rpt.generatePrinterFriendly(filter);
			
			response.reset();	
			response.setHeader("Content-Disposition", "inline; filename=MyReport.pdf");
			response.setContentType("application/pdf");
			response.setContentLength(baos.size());
			ServletOutputStream out = response.getOutputStream();
			baos.writeTo(out);
			out.flush();

		}
		catch (Exception e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			ActionErrors errors = new ActionErrors();
			errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.system.exception", e.getMessage()));
			saveErrors(request, errors);
			page = "errorPage";       	
		} 

		LOGGER.info("handleAction end");
		LOGGER.debug("PAGE FORWARD-->" + page);
		return null;
	}

	/**
	 * @param frmReport
	 * @return
	 */
	private ARWithPendingMedFilterData extractFilter(HttpServletRequest frm) {
		
		LOGGER.info("extractFilter start");
		ARWithPendingMedFilterData filter = new ARWithPendingMedFilterData();
		
		filter.setDateStart(frm.getParameter("startDate"));
		filter.setDateEnd(frm.getParameter("endDate"));
		
		filter.setReportType(Integer.parseInt(frm.getParameter("reportType")));
		String branchId = frm.getParameter("branch");
		
		SunLifeOfficeData slo = new SunLifeOfficeData();
		if (branchId.length() != 0) {
			slo.setOfficeId(branchId);
		}
		else {
			slo.setOfficeId(null);
		}
		filter.setBranch(slo);
		
		String examinerId = frm.getParameter("examiner");
		if (examinerId == null) examinerId = "";
		ExaminerData ex = new ExaminerData();
		if (examinerId.length() != 0) {  
			ex.setExaminerId(Long.parseLong(examinerId));
		}
		else {
			ex.setExaminerId(-1); 
		}
		filter.setExaminer(ex);
		
		String labId = frm.getParameter("laboratory");
		if (labId == null) labId = "";
		LaboratoryData lab = new LaboratoryData();
		if (labId.length() != 0) {
			lab.setLabId(Long.parseLong(labId));	
		}
		else {
			lab.setLabId(-1);
		}
		filter.setLaboratory(lab);
	
		
		String underwriterId = frm.getParameter("underwriter");
		UserProfileData user = new UserProfileData();
		if (underwriterId.length() != 0) {
			user.setUserId(underwriterId);
		}
		else {
			user.setUserId(null);
		}
		filter.setUnderwriter(user);
		
		LOGGER.info("extractFilter end");
		return (filter);
	}

	
}
