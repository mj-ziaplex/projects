/*
 * Created on Jan 7, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.ui.action;


import java.sql.Connection;
import java.sql.SQLException;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.slocpi.ium.IUMException;
import com.slocpi.ium.data.JobScheduleData;
import com.slocpi.ium.data.UserAccessData;
import com.slocpi.ium.data.UserData;
import com.slocpi.ium.data.UserProfileData;
import com.slocpi.ium.data.dao.JobScheduleDAO;
import com.slocpi.ium.data.util.DataSourceProxy;
import com.slocpi.ium.ui.form.AutoExpireForm;
import com.slocpi.ium.ui.util.StateHandler;
import com.slocpi.ium.underwriter.MedicalLabRecord;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.DateHelper;
import com.slocpi.ium.util.IUMConstants;

/**
 * @author Ingrid Villanueva
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class AutoExpireMedicalRecordsAction extends IUMAction{
	private static final Logger LOGGER = LoggerFactory.getLogger(AutoExpireMedicalRecordsAction.class);
	
	public ActionForward handleAction (ActionMapping mapping, 
									  ActionForm form, 
									  HttpServletRequest request,
									  HttpServletResponse response)
									  throws Exception
									  {

		LOGGER.info("handleAction start");
		String page = "";
		AutoExpireForm aeForm = (AutoExpireForm) form;
        								
		try {
			StateHandler sh = new StateHandler();
			UserData ud = sh.getUserData(request);
			UserProfileData profile = ud.getProfile();
			String userId = profile.getUserId();
			String pageId = "";
			UserAccessData uad = extractAccessPriveleges(userId, pageId);
		
			if (uad.getAccessCode().equals("W")) {
				String mode = request.getParameter("mode");
			    if (mode != null) {
			    	
			    	if (mode.equals("expire")) {
			    		expireMedRecords();
			    	}
			    	else {			    		
			    		aeForm = retrieveAutoExpireSchedule();
			    	}
			    }
			    else {
					autoExpireRecords(aeForm, userId);			    	
			    }				
				request.setAttribute("autoExpireForm", aeForm);
				page = "autoExpirePage";
			}
			else {
				page = "errorPage";
			}
		}
		catch(IUMException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			page = "errorPage";
			ActionErrors errors = new ActionErrors();
			errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.system.exception", e.getMessage()));
			saveErrors(request, errors);  			
		}
		LOGGER.info("handleAction end");
		LOGGER.debug("PAGE FORWARD-->" + page);
		return mapping.findForward(page);
	}

	private void autoExpireRecords(AutoExpireForm form, String userId) throws SQLException, IUMException {
		
		LOGGER.info("autoExpireRecords start");
		Connection conn = null;
		try {
			JobScheduleData data = new JobScheduleData();
			data.setType(IUMConstants.SCHEDULE_TYPE_AUTO_EXPIRE);
			data.setDescription(form.getNotes());
			Date sched = DateHelper.parse(form.getSchedule(),"hh:mm a");		
			data.setTime(DateHelper.sqlTimestamp(sched));
			data.setCreatedBy(userId);
			data.setCreatedDate(DateHelper.sqlTimestamp(new Date()));
			data.setUpdatedBy(userId);
			data.setUpdatedDate(DateHelper.sqlTimestamp(new Date()));
			
			conn = new DataSourceProxy().getConnection();
			conn.setAutoCommit(false);
			JobScheduleDAO dao = new JobScheduleDAO(conn);
			dao.saveJobSchedule(data);
			conn.commit();
		}
		catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			if (conn != null) {
				conn.rollback();
			}
			throw new IUMException(e.getMessage());					
		}
		finally {
			closeConnection(conn);
		}
		LOGGER.info("autoExpireRecords end");
	}
	
	private AutoExpireForm retrieveAutoExpireSchedule() throws IUMException, SQLException {
		
		LOGGER.info("retrieveAutoExpireSchedule start");
		Connection conn = null;
		try {
			conn = new DataSourceProxy().getConnection();
			JobScheduleDAO dao   = new JobScheduleDAO(conn);
			JobScheduleData data = dao.retrieveJobSchedule(IUMConstants.SCHEDULE_TYPE_AUTO_EXPIRE);
			
			AutoExpireForm form = new AutoExpireForm();
			if (data != null){
				form.setSchedule(DateHelper.format(data.getTime(),"hh:mm a"));
				form.setNotes(data.getDescription());
			}
			else{
				form.setSchedule("");
				form.setNotes("");
			}
			return form;
		}
		catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMException(e.getMessage());
		}
		finally {
			closeConnection(conn);
			LOGGER.info("retrieveAutoExpireSchedule end");
		}
		
		
	}
	
	private void closeConnection(Connection conn) throws IUMException {
		
		
		try {
			if (!conn.isClosed())
				conn.close();
		}
		catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMException(e.getMessage());		
		}
		
	}
	
	private void expireMedRecords() throws IUMException {
		
		LOGGER.info("expireMedRecords start");
		MedicalLabRecord med = new MedicalLabRecord();
        med.expireMedicalRecords();
        LOGGER.info("expireMedRecords end");
	}
}


