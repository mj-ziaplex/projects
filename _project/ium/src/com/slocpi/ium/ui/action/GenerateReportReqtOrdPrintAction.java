/*
 * Created on Mar 18, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.ui.action;

import java.io.ByteArrayOutputStream;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.slocpi.ium.data.RequirementData;
import com.slocpi.ium.data.RequirementsOrderedFilterData;
import com.slocpi.ium.data.SunLifeOfficeData;
import com.slocpi.ium.data.UserData;
import com.slocpi.ium.data.UserProfileData;
import com.slocpi.ium.reports.RequirementsOrderedReport;
import com.slocpi.ium.ui.util.StateHandler;
import com.slocpi.ium.util.CodeHelper;

/**
 * @author Nic Decapia
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class GenerateReportReqtOrdPrintAction extends IUMAction
{
	private static final Logger LOGGER = LoggerFactory.getLogger(GenerateReportReqtOrdPrintAction.class);
	/* (non-Javadoc)
	 * @see com.slocpi.ium.ui.action.IUMAction#handleAction(org.apache.struts.action.ActionMapping, org.apache.struts.action.ActionForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	public ActionForward handleAction(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response)
		throws Exception
	{
		LOGGER.info("handleAction start");
		String page = "";
		
		try {
		
			StateHandler sessionHandler = new StateHandler();
			UserData userData = sessionHandler.getUserData(request);
			UserProfileData userPref = userData.getProfile();
			
			RequirementsOrderedFilterData filter = extractFilter(request);
	
			RequirementsOrderedReport rpt = new RequirementsOrderedReport();
			ByteArrayOutputStream baos =  rpt.generatePrinterFriendly(filter);
			response.reset();	
			
			response.setHeader("Content-Disposition", "inline; filename=RptReqtOrd.pdf");
			response.setContentType("application/pdf");
			response.setContentLength(baos.size());
			ServletOutputStream out = response.getOutputStream();
			
			baos.writeTo(out);
			out.flush();

		}
		catch (Exception e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			ActionErrors errors = new ActionErrors();
			errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.system.exception", e.getMessage()));
			saveErrors(request, errors);
			page = "errorPage";       	
		} 

		LOGGER.info("handleAction end");
		LOGGER.debug("PAGE FORWARD-->" + page);
		return null;
	}

	/**
	 * @param frmReport
	 * @return
	 */
	private RequirementsOrderedFilterData extractFilter(HttpServletRequest frm) {
		
		LOGGER.info("extractFilter start");
		RequirementsOrderedFilterData filter = new RequirementsOrderedFilterData();
		
		filter.setStartDate(frm.getParameter("startDate"));
		filter.setEndDate(frm.getParameter("endDate"));
		filter.setReportType(Integer.parseInt(frm.getParameter("reportType")));		
		
		String branchId = frm.getParameter("branch");
		SunLifeOfficeData slo = new SunLifeOfficeData();
		if (branchId.length() != 0) {
			slo.setOfficeId(branchId);
			
		}
		else {
			slo.setOfficeId(null);
		}
		filter.setBranch(slo);
		
		String requirementId = frm.getParameter("requirement");
		RequirementData reqt = new RequirementData();
		if (requirementId.length() != 0) {
			reqt.setReqtCode(requirementId);
		}
		else {
			reqt.setReqtCode(null);
		}
		filter.setRequirement(reqt);
		
		String userId = frm.getParameter("user");
		UserProfileData usr = new UserProfileData();
		if (userId.length() != 0) {
			usr.setUserId(userId);
		}
		else {
			usr.setUserId(null);
		}
		filter.setUser(usr);
		
		LOGGER.info("extractFilter end");
		return (filter);
	}

}
