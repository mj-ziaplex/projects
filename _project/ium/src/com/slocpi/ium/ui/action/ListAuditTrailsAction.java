/*
 * Created on Feb 24, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.ui.action;

import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.slocpi.ium.IUMException;
import com.slocpi.ium.data.AuditTrailData;
import com.slocpi.ium.data.AuditTrailFilterData;
import com.slocpi.ium.data.UserData;
import com.slocpi.ium.data.UserProfileData;
import com.slocpi.ium.ui.form.AuditTrailFilterForm;
import com.slocpi.ium.ui.form.AuditTrailForm;
import com.slocpi.ium.ui.util.IUMWebException;
import com.slocpi.ium.ui.util.Page;
import com.slocpi.ium.ui.util.Pagination;
import com.slocpi.ium.ui.util.StateHandler;
import com.slocpi.ium.underwriter.AuditTrail;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.DateHelper;

/**
 * @author Cris
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class ListAuditTrailsAction extends IUMAction{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ListAuditTrailsAction.class);
	
	public ActionForward handleAction(ActionMapping mapping, ActionForm form, 
									  HttpServletRequest request, 
									  HttpServletResponse response){

		LOGGER.info("handleAction start");
		String page = "";
		
		StateHandler sh = new StateHandler();
		UserData ud = sh.getUserData(request);		
		UserProfileData profile = ud.getProfile();
				
		AuditTrailFilterForm filter = (AuditTrailFilterForm)form;
		AuditTrailFilterData atFilterData = populateFilter(filter);

		AuditTrail auditTrail = new AuditTrail();
		ArrayList list = new ArrayList ();
		
		String search = request.getParameter("search");
		
		try {
			if (search != null && search.equals("1")){			
				request.setAttribute("searchIsTrue", "1");
				list = populateAuditTrailForm(auditTrail.getAuditTrailLogs(atFilterData));
			}
			
			AuditTrailFilterForm auditFilterForm = new AuditTrailFilterForm();
					
			int recPerView = profile.getRecordsPerView();
			if (recPerView == 0){
				recPerView = 1;
			}
		
			int pageNo = 1;
			String reqPageNum = request.getParameter("pageNo");
			if ((reqPageNum != null) && (!reqPageNum.equals("")))  {
				pageNo = Integer.parseInt(reqPageNum);
			}
		
			Pagination pgn = new Pagination(list, recPerView);
			Page pg = pgn.getPage(pageNo);
			
			auditFilterForm.setAuditTrailLog(pg.getList());
			
			request.setAttribute("auditTrailLogForm", auditFilterForm);
			request.setAttribute("filterForm", filter);
			request.setAttribute("page", pg);
			request.setAttribute("pageNo", String.valueOf(pageNo));
			page = "auditTrailPage";
		} catch (IUMException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			page = constructError("error.system.exception", e.getMessage(),request);
		} catch (IUMWebException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			page = constructError("error.system.exception", e.getMessage(),request);
		}
		
		LOGGER.info("handleAction end");
		LOGGER.debug("PAGE FORWARD-->" + page);
		
		return mapping.findForward(page);
	}
	
	private AuditTrailFilterData populateFilter(AuditTrailFilterForm filter){
		
		LOGGER.info("populateFilter start");
		AuditTrailFilterData filterData = new AuditTrailFilterData();
		filterData.setStartDate(DateHelper.parse(filter.getStartDate() + "000000","ddMMMyyyyHHmmss"));
		filterData.setEndDate(DateHelper.parse(filter.getEndDate() + "235959","ddMMMyyyyHHmmss"));
		
		if (filter.getTable().equals("")){
			filter.setTable("0");
		}
		
		filterData.setTable(Long.parseLong(filter.getTable()));
		filterData.setTransactionType(filter.getTransactionType());
		LOGGER.info("populateFilter end");
		return filterData;
	}
	
	private ArrayList populateAuditTrailForm(ArrayList list){
		
		LOGGER.info("populateAuditTrailForm start");
		ArrayList results = new ArrayList();
				
		for(int i=0; i<list.size(); i++){
			AuditTrailData data = (AuditTrailData) list.get(i);
			AuditTrailForm form = new AuditTrailForm();
			
			form.setTransactionDate(DateHelper.format(data.getTransDate(),"ddMMMyyyy"));
			
			form.setTable(String.valueOf(data.getTransRec()));
			form.setTransactionType(data.getTransType());
			form.setFromValue(data.getChangeFrom());
			form.setToValue(data.getChangeTo());
			form.setRecordId(data.getReferenceNumber());

			results.add(form);
		}
		LOGGER.info("populateAuditTrailForm end");
		
		return results;
	}
	
	private String constructError (String key, String parameter, HttpServletRequest request){
		
		LOGGER.info("constructError start");
		String page = "errorPage";
		ActionErrors error = new ActionErrors();
		error.add(ActionErrors.GLOBAL_ERROR, new ActionError(key, parameter));
		saveErrors(request, error);
		LOGGER.info("constructError end");
		return page;
	}

}
