/*
 * Created on Jan 7, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.ui.action;

import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.slocpi.ium.IUMException;
import com.slocpi.ium.data.AssessmentRequestData;
import com.slocpi.ium.data.ClientData;
import com.slocpi.ium.data.LOBData;
import com.slocpi.ium.data.SunLifeOfficeData;
import com.slocpi.ium.data.UWAssessmentData;
import com.slocpi.ium.data.UserData;
import com.slocpi.ium.data.UserProfileData;
import com.slocpi.ium.interfaces.IUMInterfaceException;
import com.slocpi.ium.ui.form.AssessmentRequestForm;
import com.slocpi.ium.ui.form.RequestDetailForm;
import com.slocpi.ium.ui.util.StateHandler;
import com.slocpi.ium.underwriter.AssessmentRequest;
import com.slocpi.ium.underwriter.UnderWriterException;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.DateHelper;
import com.slocpi.ium.util.IUMConstants;
import com.slocpi.ium.util.ValueConverter;

/**
 * @author Cris
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class SaveRequestAction extends IUMAction{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(SaveRequestAction.class);
	private static final String YES = "Y";
	private static final String NO = "N";
	
	public ActionForward handleAction (ActionMapping mapping, 
									  ActionForm form, 
									  HttpServletRequest request,
									  HttpServletResponse response){
		
		LOGGER.info("handleAction start");
		String page = "";
		try{
			RequestDetailForm arForm = (RequestDetailForm) form;
			String session_id = request.getAttribute("session_id").toString();
			request.setAttribute("session_id", session_id);
			
			long status = Long.parseLong(arForm.getRequestStatus());
			String referenceNumber = arForm.getRefNo();
			String assignedTo = arForm.getAssignedTo();	
			String remarks = arForm.getRemarks();
			
			Date dateForwarded = DateHelper.sqlDate(DateHelper.parse(arForm.getDateForwarded(), "ddMMMyyyy"));
		
			UserData userData = new StateHandler().getUserData(request);
			UserProfileData userLogIn = userData.getProfile();
			String user = userLogIn.getUserId();
			
			long currentStatus = getStatus(referenceNumber);
			
			boolean isUpdateValid = true;			
			page = "requestDetailsPage";
						
			if (currentStatus != status) {
								
				page = submitRequest(referenceNumber, status, assignedTo, remarks, user, request);
				if (page.equalsIgnoreCase("error")) {
					isUpdateValid = false;
					page = "requestDetailsPage";
				}
			}
			else {
				
				saveAssignedTo(referenceNumber, assignedTo, user);				
			}
					
			if (isUpdateValid) {
				if (currentStatus == IUMConstants.STATUS_NB_REVIEW_ACTION) {
					
					saveRequestDetails(arForm, user);
				}

				if (((currentStatus == IUMConstants.STATUS_FOR_TRANSMITTAL_USD) ||
					 (currentStatus == IUMConstants.STATUS_AWAITING_REQUIREMENTS) ||
					 (currentStatus == IUMConstants.STATUS_AWAITING_MEDICAL)) || 
					((!arForm.getSourceSystem().equals("") && status == IUMConstants.STATUS_FOR_TRANSMITTAL_USD))) {					
					
					if (!arForm.getSourceSystem().equals("") && status == IUMConstants.STATUS_FOR_TRANSMITTAL_USD){
						dateForwarded = DateHelper.sqlDate(new java.util.Date());
					}
					saveDateForwardedTo(referenceNumber, dateForwarded, user);			
				}
				
				saveRemarks(referenceNumber, remarks, user);
				
				String reqPage = request.getParameter("reqPage");
				
				if ((reqPage != null) && (reqPage.equals("uwAssessmentDetail"))) {
					saveUWAssessment(arForm, user);
					page = "uwAssessmentDetailPage";
				}
			}
						
		} catch (UnderWriterException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			constructError("error.submitRequest",e.getMessage(),request);
		} catch (IUMException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			constructError("error.system.exception", e.getMessage(), request);
			page = "errorPage";
		} catch (Exception e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			constructError("error.system.exception", e.getMessage(), request);
			page = "errorPage";
		}		
		LOGGER.info("handleAction end");
		LOGGER.debug("PAGE FORWARD-->" + page);
		return mapping.findForward(page);
	}
	
	private String submitRequest(String referenceNumber, long status, String assignedTo, String remarks, String user, HttpServletRequest request){
		
		LOGGER.info("submitRequest start");
		String page = "";
		AssessmentRequest ar = new AssessmentRequest();
		
		try{
			if (status == IUMConstants.STATUS_FOR_ASSESSMENT){
				page = submitForAssessment(referenceNumber, user);
			} else if (status == IUMConstants.STATUS_FOR_TRANSMITTAL_USD){ 
				page = transmitToUSD(referenceNumber,assignedTo,user);
			} else if (status == IUMConstants.STATUS_UNDERGOING_ASSESSMENT){
				page = assessApplication(referenceNumber,user);
			} else if (status == IUMConstants.STATUS_FOR_APPROVAL){
				page = submitForApproval(referenceNumber,assignedTo,user);
			} else if (status == IUMConstants.STATUS_FOR_FACILITATOR_ACTION){
				page = receiveARAtUSD(referenceNumber,user);
			} else if (status == IUMConstants.STATUS_APPROVED){
				page = approveRequestAsAppliedFor(referenceNumber,user);
			} else if (status == IUMConstants.STATUS_NOT_PROCEEDED_WITH){
				page = notProceededWith(referenceNumber,user);
			} else if (status == IUMConstants.STATUS_AR_CANCELLED){
				page = cancelRequest(referenceNumber,remarks,user);
			} else if (status == IUMConstants.STATUS_DECLINED){
				page = declineApplication(referenceNumber,user);
			} else if (status == IUMConstants.STATUS_FOR_OFFER){
				page = approveRequestForOffer(referenceNumber,user);
			} else if (status == IUMConstants.STATUS_AWAITING_REQUIREMENTS){
				page = awaitingRequirements(referenceNumber,assignedTo, user); 
			} else if (status == IUMConstants.STATUS_AR_FOR_REFERRAL){
				page = forReferral(referenceNumber,assignedTo, user); 
			} else if (status == IUMConstants.STATUS_AWAITING_MEDICAL){
				page = awaitingMedical(referenceNumber,assignedTo, user);
			} else if (status == IUMConstants.STATUS_NB_REVIEW_ACTION){
				page = constructError("error.submitRequest", "The request of the status is invalid.", request);
			} else {
				page = genericChangeStatus(referenceNumber, status, assignedTo, user);
			}
		} catch (Exception e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			page = constructError("error.submitRequest",e.getMessage(),request);
		} 
		LOGGER.debug("submitRequest page forward-->" + page);
		LOGGER.info("submitRequest end");
		return page;
		
	}
	
	private String constructError (String key, String parameter, HttpServletRequest request){
		
		LOGGER.info("constructError start");
		String result = "error";
		ActionErrors error = new ActionErrors();
		error.add(ActionErrors.GLOBAL_ERROR, new ActionError(key, parameter));
		saveErrors(request, error);
		LOGGER.info("constructError end");
		return result;
	}

	private String submitForAssessment(String referenceNumber, String user) throws UnderWriterException, IUMException{
		
		LOGGER.info("submitForAssessment start");
		AssessmentRequest aReq = new AssessmentRequest();
		aReq.submitToUnderwriter(referenceNumber,user);
		String pageReturn = "requestListPage";
		LOGGER.info("submitForAssessment end");
		return pageReturn;
	}
	
	private String transmitToUSD(String referenceNumber, String facilitator, String user) throws UnderWriterException, IUMException, IUMInterfaceException {
		
		LOGGER.info("transmitToUSD start");
		AssessmentRequest aReq = new AssessmentRequest();
		aReq.transmit(referenceNumber, facilitator, user);
		String pageReturn = "requestListPage";
		LOGGER.info("transmitToUSD end");
		return pageReturn;
	}
	
	private String assessApplication(String referenceNumber, String user) throws UnderWriterException, IUMException{
		
		LOGGER.info("assessApplication start");
		AssessmentRequest aReq = new AssessmentRequest();
		aReq.assessApplication(referenceNumber,user);
		String pageReturn = "requestDetailsPage";
		LOGGER.info("assessApplication end");
		return pageReturn;
	}
	                                                                                                                                                                                                                                   
	private String submitForApproval(String referenceNumber, String assignTo, String user) throws UnderWriterException, IUMException{

		LOGGER.info("submitForApproval start");
		AssessmentRequest aReq = new AssessmentRequest();
		aReq.submitRequestForApproval(referenceNumber, assignTo, user);
		String pageReturn = "requestListPage";
		LOGGER.info("submitForApproval end");
		return pageReturn;
	}
	
	private String receiveARAtUSD(String referenceNumber, String user) throws UnderWriterException, IUMException{
		
		LOGGER.info("receiveARAtUSD start");
		AssessmentRequest aReq = new AssessmentRequest();
		aReq.setToForFacilitatorAction(referenceNumber,user);
		String pageReturn = "requestDetailsPage";
		LOGGER.info("receiveARAtUSD end");
		return pageReturn;
	}
	
	private String approveRequestAsAppliedFor(String referenceNumber, String user) throws UnderWriterException, IUMException, IUMInterfaceException {
		
		LOGGER.info("approveRequestAsAppliedFor start");
		AssessmentRequest aReq = new AssessmentRequest();
		aReq.approveRequestAsAppliedFor(referenceNumber,user);
		String pageReturn = "requestListPage";
		LOGGER.info("approveRequestAsAppliedFor end");
		return pageReturn;
	}
	
	private String notProceededWith(String referenceNumber,String user) throws UnderWriterException, IUMException{
		
		LOGGER.info("notProceededWith start");
		AssessmentRequest aReq = new AssessmentRequest();
		aReq.notProceededWith(referenceNumber,user);
		String pageReturn = "requestListPage";
		LOGGER.info("notProceededWith end");
		return pageReturn;
	}
	
	private String cancelRequest(String referenceNumber, String remarks, String user) throws UnderWriterException, IUMException{
		
		LOGGER.info("cancelRequest start");
		AssessmentRequest aReq = new AssessmentRequest();
		aReq.cancelAssessmentRequest(referenceNumber,remarks,user);
		String pageReturn = "requestListPage";
		LOGGER.info("cancelRequest end");
		return pageReturn;
	}
	
	private String approveRequestForOffer(String referenceNumber, String user) throws UnderWriterException, IUMException{

		LOGGER.info("approveRequestForOffer start");
		AssessmentRequest aReq = new AssessmentRequest();
		aReq.approveApplicationForOffer(referenceNumber,user);
		String pageReturn = "requestDetailsPage";
		LOGGER.info("approveRequestForOffer end");
		return pageReturn;
	}
	
	private String declineApplication(String referenceNumber, String user) throws UnderWriterException, IUMException{
		
		LOGGER.info("declineApplication start");
		AssessmentRequest aReq = new AssessmentRequest();
		aReq.declineApplication(referenceNumber,user);
		String pageReturn = "requestListPage";
		LOGGER.info("declineApplication end");
		return pageReturn;
	}
	
	private String forReferral (String referenceNumber, String assignedTo, String user) throws IUMException{
		
		LOGGER.info("forReferral start");
		AssessmentRequest ar = new AssessmentRequest();
		ar.forReferral(referenceNumber,assignedTo,user);
		String pageReturn = "requestListPage";
		LOGGER.info("forReferral end");
		return pageReturn;
	}
	
	private String genericChangeStatus(String referenceNumber, long operation, String assignedTo, String user) throws IUMException{
		
		LOGGER.info("genericChangeStatus start");
		AssessmentRequest ar = new AssessmentRequest();
		ar.genericChangeARStatus(referenceNumber, operation, assignedTo, user);
		String pageReturn = "requestDetailsPage";
		LOGGER.info("genericChangeStatus end");
		return pageReturn;
	}

	
	private long getStatus(String referenceNumber) throws IUMException {
		
		LOGGER.info("getStatus start");
		AssessmentRequestData arData = new AssessmentRequestData();
		AssessmentRequest ar = new AssessmentRequest();
		arData = ar.getDetails(referenceNumber);
		long status = arData.getStatus().getStatusId();
		LOGGER.info("getStatus end");
		return status;
	}
	

	private void saveAssignedTo(String referenceNumber, String assignee, String updatedBy) throws IUMException {
		
		LOGGER.info("saveAssignedTo start");
		AssessmentRequest ar = new AssessmentRequest();
		ar.assignTo(referenceNumber, assignee, updatedBy);
		LOGGER.info("saveAssignedTo end");
	}


	private void saveDateForwardedTo(String referenceNumber, Date dateForwarded, String updatedBy) throws IUMException {
		
		LOGGER.info("saveDateForwardedTo start");
		AssessmentRequest ar = new AssessmentRequest();
		ar.updateDateForwarded(referenceNumber, dateForwarded, updatedBy);
		LOGGER.info("saveDateForwardedTo end");
	}


	private void saveRemarks(String referenceNumber, String remarks, String updatedBy) throws IUMException {
		
		LOGGER.info("saveRemarks start");
		AssessmentRequest ar = new AssessmentRequest();
		ar.setRemarks(referenceNumber, remarks, updatedBy);
		LOGGER.info("saveRemarks end");
	}
	
	private void saveUWAssessment(AssessmentRequestForm arForm, String user) throws IUMException {
		
		LOGGER.info("saveUWAssessment start");
		UWAssessmentData data = new UWAssessmentData();
		data.setReferenceNumber(arForm.getRefNo());
		data.setRemarks(arForm.getUWRemarks());
		data.setAnalysis(arForm.getUWAnalysis());
		AssessmentRequest ar = new AssessmentRequest();
		
		data.setUpdatedBy(user);
		data.setUpdateDate(new Date());
		
		data.setPostedBy(user);
		data.setPostedDate(new Date());

		LOGGER.info("saveUWAssessment end");
		ar.updateUWAssessmentDetails(data);	
	}
	

	private void saveRequestDetails(AssessmentRequestForm arForm, String user) throws IUMException {
		
		LOGGER.info("saveRequestDetails start");
		
		LOBData lob = new LOBData();
		lob.setLOBCode(arForm.getLob());
		
		UserProfileData agent = new UserProfileData();
		agent.setUserId(arForm.getAgentCode());
		
		UserProfileData underwriter = new UserProfileData();
		underwriter.setUserId(arForm.getUnderwriter());

		UserProfileData updatedBy = new UserProfileData();
		updatedBy.setUserId(user);
		
		SunLifeOfficeData branch = new SunLifeOfficeData();
		branch.setOfficeId(arForm.getBranchCode());
		
		AssessmentRequestData arData = new AssessmentRequestData();		
		arData.setReferenceNumber(arForm.getRefNo());
		arData.setLob(lob);
		arData.setSourceSystem(arForm.getSourceSystem());		
		arData.setAmountCovered(ValueConverter.convertCurrency(arForm.getAmountCovered()));
		arData.setPremium(ValueConverter.convertCurrency(arForm.getPremium()));
		arData.setBranch(branch);
		arData.setAgent(agent);
		arData.setUnderwriter(underwriter);
		arData.setApplicationReceivedDate(DateHelper.sqlDate(DateHelper.parse(arForm.getReceivedDate(), "ddMMMyyyy")));
		arData.setForwardedDate(DateHelper.sqlDate(DateHelper.parse(arForm.getDateForwarded(), "ddMMMyyyy")));
		arData.setUpdatedBy(updatedBy);
		arData.setUpdatedDate(new Date());
		
	
		String lobCode = arForm.getLob();
		if (lobCode.equals(IUMConstants.LOB_INDIVIDUAL_LIFE)) {
			
			ClientData insuredClientData = new ClientData();
			insuredClientData.setClientId(arForm.getInsuredClientNo());
			insuredClientData.setGivenName(arForm.getInsuredFirstName());
			insuredClientData.setMiddleName(arForm.getInsuredMiddleName());
			insuredClientData.setLastName(arForm.getInsuredLastName());
			insuredClientData.setTitle(arForm.getInsuredTitle());
			insuredClientData.setSuffix(arForm.getInsuredSuffix());
			Date insuredBDate = DateHelper.parse(arForm.getInsuredBirthDate(), "ddMMMyyyy");
			
			if (insuredBDate != null){
				insuredClientData.setAge(DateHelper.computeAge(insuredBDate));
			} else if (!"".equals(arForm.getInsuredAge()) && !arForm.getInsuredAge().equals("0")){
				insuredClientData.setAge(Integer.parseInt(arForm.getInsuredAge()));
			}else {
				insuredClientData.setAge(0);
			}
			
			insuredClientData.setSex(arForm.getInsuredSex());
			insuredClientData.setBirthDate(DateHelper.sqlDate(DateHelper.parse(arForm.getInsuredBirthDate(), "ddMMMyyyy")));		
			arData.setInsured(insuredClientData);
			
			String sameClientData = arForm.getSameClientData();
			 
			if (sameClientData.equals(YES)) {
				ClientData ownerClientData = new ClientData();
				ownerClientData.setClientId(arForm.getInsuredClientNo());
				arData.setOwner(ownerClientData);
			}
			else {
				ClientData ownerClientData = new ClientData();
				ownerClientData.setClientId(arForm.getOwnerClientNo());
				ownerClientData.setGivenName(arForm.getOwnerFirstName());
				ownerClientData.setMiddleName(arForm.getOwnerMiddleName());
				ownerClientData.setLastName(arForm.getOwnerLastName());
				ownerClientData.setTitle(arForm.getOwnerTitle());
				ownerClientData.setSuffix(arForm.getOwnerSuffix());
				
				Date ownerBDate = DateHelper.parse(arForm.getOwnerBirthDate(), "ddMMMyyyy");
				if(ownerBDate != null){
					ownerClientData.setAge(DateHelper.computeAge(ownerBDate));
				}else if (!"".equals(arForm.getOwnerAge()) && !arForm.getOwnerAge().equals("0")){
					ownerClientData.setAge(Integer.parseInt(arForm.getOwnerAge()));
				}else {
					ownerClientData.setAge(0);
				}
				
				ownerClientData.setSex(arForm.getOwnerSex());
				ownerClientData.setBirthDate(DateHelper.sqlDate(DateHelper.parse(arForm.getOwnerBirthDate(), "ddMMMyyyy")));		
				arData.setOwner(ownerClientData);
			}
		}
		else if (lobCode.equals(IUMConstants.LOB_PRE_NEED)) {
			
			ClientData insuredClientData = new ClientData();
			insuredClientData.setClientId(arForm.getInsuredClientNo());
			insuredClientData.setGivenName(arForm.getInsuredFirstName());
			insuredClientData.setMiddleName(arForm.getInsuredMiddleName());
			insuredClientData.setLastName(arForm.getInsuredLastName());
			insuredClientData.setTitle(arForm.getInsuredTitle());
			insuredClientData.setSuffix(arForm.getInsuredSuffix());
			Date insuredBDate = DateHelper.parse(arForm.getInsuredBirthDate(), "ddMMMyyyy");
			
			if (insuredBDate != null){
				insuredClientData.setAge(DateHelper.computeAge(insuredBDate));
			} else if (!"".equals(arForm.getInsuredAge()) && !arForm.getInsuredAge().equals("0")){
				insuredClientData.setAge(Integer.parseInt(arForm.getInsuredAge()));
			}else {
				insuredClientData.setAge(0);
			}
			
			
			insuredClientData.setSex(arForm.getInsuredSex());
			insuredClientData.setBirthDate(DateHelper.sqlDate(DateHelper.parse(arForm.getInsuredBirthDate(), "ddMMMyyyy")));		
			arData.setInsured(insuredClientData);
			
			ClientData ownerClientData = new ClientData();
			ownerClientData.setClientId(arForm.getInsuredClientNo());
			arData.setOwner(ownerClientData);
		}
		else if (lobCode.equals(IUMConstants.LOB_GROUP_LIFE)) {
			
			ClientData insuredClientData = new ClientData();
			insuredClientData.setClientId(arForm.getInsuredClientNo());
			insuredClientData.setGivenName(arForm.getInsuredFirstName());
			insuredClientData.setMiddleName(arForm.getInsuredMiddleName());
			insuredClientData.setLastName(arForm.getInsuredLastName());
			insuredClientData.setTitle(arForm.getInsuredTitle());
			insuredClientData.setSuffix(arForm.getInsuredSuffix());
			Date insuredBDate = DateHelper.parse(arForm.getInsuredBirthDate(), "ddMMMyyyy");
			
			if (insuredBDate != null){
				insuredClientData.setAge(DateHelper.computeAge(insuredBDate));
			} else if (!"".equals(arForm.getInsuredAge()) && !arForm.getInsuredAge().equals("0")){
				insuredClientData.setAge(Integer.parseInt(arForm.getInsuredAge()));
			}else {
				insuredClientData.setAge(0);
			}
			
			
			insuredClientData.setSex(arForm.getInsuredSex());
			insuredClientData.setBirthDate(DateHelper.sqlDate(DateHelper.parse(arForm.getInsuredBirthDate(), "ddMMMyyyy")));		
			arData.setInsured(insuredClientData);			
		}
		
		AssessmentRequest ar = new AssessmentRequest();
		ar.maintainAssessmentRequest(arData);
		LOGGER.info("saveRequestDetails end");
	}
	
	private String awaitingRequirements(String referenceNumber, String assignedTo, String user) throws UnderWriterException, IUMException{
			
			LOGGER.info("awaitingRequirements start");
			AssessmentRequest aReq = new AssessmentRequest();
			aReq.awaitingRequirements(referenceNumber, assignedTo, user);
			String pageReturn = "requestDetailsPage";
			LOGGER.info("awaitingRequirements end");
			return pageReturn;
	}
	
	private String awaitingMedical(String referenceNumber, String assignedTo, String user) throws UnderWriterException, IUMException{
		
			LOGGER.info("awaitingMedical start");
			AssessmentRequest aReq = new AssessmentRequest();
			aReq.awaitingMedical(referenceNumber, assignedTo, user);
			String pageReturn = "requestDetailsPage";
			LOGGER.info("awaitingMedical end");
			return pageReturn;
	}
}


