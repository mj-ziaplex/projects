package com.slocpi.ium.ui.action;

import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.slocpi.ium.data.UWAssessmentData;
import com.slocpi.ium.ui.form.FormLoader;
import com.slocpi.ium.ui.form.UWAnalysisForm;
import com.slocpi.ium.ui.form.UWAssessmentDetailForm;
import com.slocpi.ium.underwriter.AssessmentRequest;
import com.slocpi.ium.util.DateHelper;
import com.slocpi.ium.util.ValueConverter;


public class UWAssessmentPopupAction extends Action {
	private static final Logger LOGGER = LoggerFactory.getLogger(UWAssessmentPopupAction.class);
	
	public ActionForward execute(
			ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response)
			throws Exception { 
		
		LOGGER.info("execute start");
		
		String page = "uwPage";
		
		UWAssessmentDetailForm uwAssessmentDetailForm = (UWAssessmentDetailForm) form;
		
		if (uwAssessmentDetailForm == null) {
			uwAssessmentDetailForm = new UWAssessmentDetailForm();
		}
		
		String refNo = request.getParameter("refNo");
		ArrayList listOfAnalysis = new ArrayList();
		AssessmentRequest ar = new AssessmentRequest();
		
		uwAssessmentDetailForm = (UWAssessmentDetailForm) FormLoader.loadAssessmentRequest(uwAssessmentDetailForm);
		
		ArrayList list = ar.retrieveUWAnalysis(refNo);
		for (int i=0; i<list.size(); i++){
			UWAnalysisForm analysis = new UWAnalysisForm();
			UWAssessmentData data = (UWAssessmentData) list.get(i);
			analysis.setReferenceNumber(data.getReferenceNumber());
			analysis.setAnalysis(ValueConverter.newLineToHTML(data.getAnalysis()));
			analysis.setPostedBy(data.getPostedBy());
			analysis.setPostedDate(DateHelper.format(data.getPostedDate(), "ddMMMyyyy HH:mm").toUpperCase());
			listOfAnalysis.add(analysis);
		}
		uwAssessmentDetailForm.setListOfAnalysis(listOfAnalysis);
		
		request.setAttribute("detailForm", uwAssessmentDetailForm);
		
		LOGGER.info("execute end");
		LOGGER.debug("PAGE FORWARD-->" + page);
		return (mapping.findForward(page));
	}
}
