package com.slocpi.ium.ui.action;

import java.util.ArrayList;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.slocpi.ium.IUMException;
import com.slocpi.ium.data.LaboratoryData;
import com.slocpi.ium.data.LaboratoryTestData;
import com.slocpi.ium.data.UserData;
import com.slocpi.ium.data.UserProfileData;
import com.slocpi.ium.ui.form.LaboratoryForm;
import com.slocpi.ium.ui.util.StateHandler;
import com.slocpi.ium.underwriter.Reference;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.DateHelper;
import com.slocpi.ium.util.ValueConverter;

/**
 * @author ppe
 *
 * To change this generated comment edit the template variable "typecomment":
 * Window>Preferences>Java>Templates.
 * To enable and disable the creation of type comments go to
 * Window>Preferences>Java>Code Generation.
 */
public class UpdateLaboratoryAction extends IUMAction {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(UpdateLaboratoryAction.class);
	/**
	 * @see com.slocpi.ium.ui.action.IUMAction#handleAction(org.apache.struts.action.ActionMapping, org.apache.struts.action.ActionForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	public ActionForward handleAction(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response)
		throws Exception {
			
		LOGGER.info("handleAction start");
		String page = "";
		page = "listLaboratoryPage";
		
		LaboratoryForm labForm = (LaboratoryForm)form;
		
		try{
			if(labForm.getActionType().equalsIgnoreCase("update"))
			{					
				Reference ref = new Reference();
				LaboratoryData labData = ref.getLaboratoryData(Long.parseLong(request.getParameter("updateLabId")));						
				ArrayList labTestList = ref.getLaboratoryTestList(labData.getLabId());			
				
				labForm = populateLaboratoryForm(labForm, labData, labTestList);
			}
			else if(labForm.getActionType().equalsIgnoreCase("add_test"))
			{
				labForm = addLabTest(labForm, request);
			}		
			else if(labForm.getActionType().equalsIgnoreCase("delete_test"))
			{			
				labForm = removeLabTest(labForm);
			}
			else if(labForm.getActionType().equalsIgnoreCase("save_update"))
			{			
				StateHandler sh = new StateHandler();
				UserData ud = sh.getUserData(request);
				UserProfileData profile = ud.getProfile();
				
				LaboratoryData labData = populateLaboratoryDetails(labForm, profile);
				ArrayList listTest = populateLaboratoryTestDetails(labForm);
				Reference ref = new Reference();
				ref.editLaboratoryData(labData, listTest);
			}
			
			ListLaboratoryAction listLabAction = new ListLaboratoryAction();
			listLabAction.handleAction(mapping, labForm, request, response);
		}
		catch (IUMException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			ActionErrors errors = new ActionErrors();
		  	errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.system.exception", e.getMessage()));
		  	saveErrors(request, errors);
		  	page = "errorPage";
		}
		catch (Exception e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			ActionErrors errors = new ActionErrors();
		  	errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.system.exception", e.getMessage()));
		  	saveErrors(request, errors);
		  	page = "errorPage";
		}		
		LOGGER.info("handleAction end");
		LOGGER.debug("PAGE FORWARD-->" + page);
		return (mapping.findForward(page));
	}
	
	private LaboratoryForm removeLabTest(LaboratoryForm labForm)
	{
		LOGGER.info("removeLabTest start");
		int testLen = 0;
		if(labForm.getLabTest() != null)
		{
			testLen = labForm.getLabTest().length;
		}
		
		String[] labFee = new String[testLen];
		String[] labTest = new String[testLen];
		String[] labTestStatus = new String[testLen];
		
		int feeLen = 0; 
		
		for(int i=0; i<testLen; i++)
		{
			labTest[i] = labForm.getLabTest()[i];				
			labTestStatus[i] = labForm.getLabTestStatus()[i];
			if(labForm.getLabTestStatus()[i].equalsIgnoreCase("active"))
			{
				labFee[i] = labForm.getLabFee()[feeLen];
				feeLen++;
			}	
			else if(labForm.getLabTestStatus()[i].equalsIgnoreCase("toInactive"))
			{
				labTestStatus[i] = "inactive";
				labFee[i] = "0.0";
				feeLen++;
			}
			else
			{
				labFee[i] = "0.0";
			}
		}
					
		labForm.setLabFee(labFee);
		labForm.setLabTest(labTest);
		labForm.setLabTestStatus(labTestStatus);
		labForm.setActionType("update");
		
		LOGGER.info("removeLabTest end");
		return labForm;
	}
	
	private LaboratoryForm addLabTest(LaboratoryForm labForm, HttpServletRequest request)
	{
		
		LOGGER.info("addLabTest start");
		int testLen = 0;
		int feeLen = 0;
			
		String newTest = request.getParameter("testCode");
		boolean testExist = false;
		
		if(labForm.getLabTest() != null)
		{
			testLen = labForm.getLabTest().length;
		}
		
		String[] labFee = new String[testLen];
		String[] labTestStatus = new String[testLen];
		
		for(int i=0; i<testLen; i++)
		{
			labTestStatus[i] = labForm.getLabTestStatus()[i];
			
			if(labForm.getLabTestStatus()[i].equalsIgnoreCase("active"))
			{
				labFee[i] = labForm.getLabFee()[feeLen];
				feeLen++;
			}	
			else
			{
				labFee[i] = "0.0";
			}
			
			if(newTest.equalsIgnoreCase(labForm.getLabTest()[i]))
			{
				labTestStatus[i] = "active";
				testExist = true;	
			}
		}
		
		if(!testExist)
		{			 				
			String[] labTest = new String[testLen + 1];
			labFee = new String[testLen + 1];
			labTestStatus = new String[testLen+1];
			feeLen = 0;
					 			
			for(int i=0; i<testLen; i++)
			{
				labTest[i] = labForm.getLabTest()[i];				
				labTestStatus[i] = labForm.getLabTestStatus()[i];
				
				if(labForm.getLabTestStatus()[i].equalsIgnoreCase("active"))
				{
					labFee[i] = labForm.getLabFee()[feeLen];
					feeLen++;
				}	
				else
				{
					labFee[i] = "0.0";
				}
			
			}
		
			labTest[testLen] = newTest; 				
			labFee[testLen] = "0.0";
			labTestStatus[testLen] = "active";
			
			labForm.setLabTest(labTest);
		}
		
		labForm.setLabFee(labFee);			
		labForm.setLabTestStatus(labTestStatus);
		labForm.setActionType("update");
		
		LOGGER.info("addLabTest end");
		return labForm;
	}
	
	private LaboratoryData populateLaboratoryDetails(LaboratoryForm labForm, UserProfileData profile)
	{
		
		LOGGER.info("populateLaboratoryDetails start");
		LaboratoryData labData = new LaboratoryData();
		
		labData.setLabId(Long.parseLong(labForm.getLabId()));
		labData.setLabName(labForm.getLabName());
		labData.setContactPerson(labForm.getContactPerson());
		labData.setBusinessAddrLine1(labForm.getBusAdd1());
		labData.setBusinessAddrLine2(labForm.getBusAdd2());
		labData.setBusinessAddrLine3(labForm.getBusAdd3());
		labData.setCity(labForm.getBusCity());
		labData.setCountry(labForm.getBusCountry());
		labData.setZipCode(labForm.getBusZipCode());
		labData.setOfficeNum(labForm.getOfcPhone1());
		labData.setOfficeNum2(labForm.getOfcPhone2());
		labData.setFaxNum(labForm.getFaxNumber());
		labData.setBranchName(labForm.getBranchName());
		labData.setBranchAddrLine1(labForm.getBranchAdd1());
		labData.setBranchAddrLine2(labForm.getBranchAdd2());
		labData.setBranchAddrLine3(labForm.getBranchAdd3());
		labData.setBranchCity(labForm.getBranchCity());
		labData.setBranchCountry(labForm.getBranchCountry());
		labData.setBranchZipCode(labForm.getBranchZipCode());						
		labData.setBranchPhone(labForm.getBranchPhone());
		labData.setBranchFax_num(labForm.getBranchFaxNumber());
		labData.setAccreditInd(labForm.getAccredited());
		
		if(!labForm.getAccreditedDate().equals(""))
		{
			labData.setAccreditDate(DateHelper.parse(labForm.getAccreditedDate(), "ddMMMyyyy"));
		}	
		
		labData.setUpdatedDate(new Date(System.currentTimeMillis()));
		labData.setUpdatedBy(profile);
		labData.setEmailAddress(labForm.getEmailAddress());
		LOGGER.info("populateLaboratoryDetails end");
		return labData;
	}
	
	private LaboratoryForm populateLaboratoryForm(LaboratoryForm form, LaboratoryData labData, ArrayList labTestList)
	{				
		LOGGER.info("populateLaboratoryForm start");
		
		form.setLabId(String.valueOf(labData.getLabId()));
		form.setLabName(labData.getLabName());
		form.setContactPerson(labData.getContactPerson());
		form.setBusAdd1(labData.getBusinessAddrLine1());
		form.setBusAdd2(labData.getBusinessAddrLine2());
		form.setBusAdd3(labData.getBusinessAddrLine3());
		form.setBusCity(labData.getCity());
		form.setBusCountry(labData.getCountry());
		form.setBusZipCode(labData.getZipCode());
		form.setOfcPhone1(labData.getOfficeNum());
		form.setOfcPhone2(labData.getOfficeNum2());
		form.setFaxNumber(labData.getFaxNum());
		form.setBranchName(labData.getBranchName());
		form.setBranchAdd1(labData.getBranchAddrLine1());
		form.setBranchAdd2(labData.getBranchAddrLine2());
		form.setBranchAdd3(labData.getBranchAddrLine3());
		form.setBranchCity(labData.getBranchCity());
		form.setBranchCountry(labData.getBranchCountry());
		form.setBranchZipCode(labData.getBranchZipCode());						
		form.setBranchPhone(labData.getBranchPhone());
		form.setBranchFaxNumber(labData.getBranchFax_num());
		form.setAccredited(labData.getAccreditInd());
		
		if(labData.getAccreditDate() != null && !labData.getAccreditDate().equals("") )
		{
			form.setAccreditedDate(DateHelper.format(labData.getAccreditDate(), "ddMMMyyyy"));
		}	
		
		form.setEmailAddress(labData.getEmailAddress());
		
		int len = labTestList.size();
		String[] labFee = new String[len];
		String[] labTest = new String[len];
		String[] labTestStatus = new String[len];
		
				
		for(int i=0; i<len; i++)
		{
			LaboratoryTestData labTestData = (LaboratoryTestData)labTestList.get(i);
			labFee[i] = String.valueOf(labTestData.getTestFee());
			labTest[i] = String.valueOf(labTestData.getTestCode());	
			labTestStatus[i] = labTestData.getTestStatus()!=null?labTestData.getTestStatus():"active";	
		}
		
		form.setLabTest(labTest);
		form.setLabFee(labFee);		
		form.setLabTestStatus(labTestStatus);
		
		LOGGER.info("populateLaboratoryForm end");
		return form;
	}
	
	private ArrayList populateLaboratoryTestDetails(LaboratoryForm form)
	{
		
		LOGGER.info("populateLaboratoryTestDetails start");
		ArrayList list = new ArrayList();
		
		int len = 0;
		if(form.getLabTest() == null)
		{
			len = 0;
		}
		else
		{
			len  = form.getLabTest().length;
		}	
		
		int feeLen = 0;
		
		for(int i=0; i<len; i++)
		{
			LaboratoryTestData labTestData = new LaboratoryTestData();
			labTestData.setLabId(Long.parseLong(form.getLabId()));
			labTestData.setTestCode(Long.parseLong(form.getLabTest()[i]));			
			labTestData.setTestStatus(form.getLabTestStatus()[i]);
			if(form.getLabTestStatus()[i].equalsIgnoreCase("active"))
			{
				labTestData.setTestFee(ValueConverter.convertCurrency(form.getLabFee()[feeLen]));
				feeLen++;
			}	
			else
			{
				labTestData.setTestFee(0.0);
			}
			list.add(labTestData);
		}
		LOGGER.info("populateLaboratoryTestDetails end");
		return list;
	}
	

}
