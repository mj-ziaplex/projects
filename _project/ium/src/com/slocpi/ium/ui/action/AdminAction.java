/**
 * AdminAction.java				Copyright (c) 2004 Pointwest Technologies Corp.
 * TODO DOCUMENT ME!
 * 
 * @author aditalo	 @date Mar 10, 2004
 */
package com.slocpi.ium.ui.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.slocpi.ium.data.UserData;
import com.slocpi.ium.ui.form.AdminForm;
import com.slocpi.ium.ui.util.StateHandler;
import com.slocpi.ium.underwriter.Impairment;
import com.slocpi.ium.underwriter.MedicalLabRecord;
import com.slocpi.ium.underwriter.User;


/**
 * TODO DOCUMENT ME!
 * 
 * @author aditalo		Copyright (c) 2004 Pointwest Technologies Corp.
 * @date Mar 10, 2004
 */
public class AdminAction extends IUMAction {

	private static final Logger LOGGER = LoggerFactory.getLogger(AdminAction.class);
	/* (non-Javadoc)
	 * @see com.slocpi.ium.ui.action.IUMAction#handleAction(org.apache.struts.action.ActionMapping, org.apache.struts.action.ActionForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	public ActionForward handleAction(
			ActionMapping mapping, 
		ActionForm form, 
		HttpServletRequest request, 
		HttpServletResponse response) 
		throws Exception {
		
		LOGGER.info("handleAction start");
		ActionForward actionForward = mapping.findForward("adminPage");			

		StateHandler stateHandler = new StateHandler();
		UserData user = stateHandler.getUserData(request);
		
		AdminForm adminForm = new AdminForm();
		Impairment impairment = new Impairment();
		MedicalLabRecord medLabRecord = new MedicalLabRecord();
		User usr = new User();
		
		adminForm.setMibForExport(impairment.countMIBForExport());
		adminForm.setMedRecForExp(medLabRecord.countMedRecordsForExpire());
		adminForm.setPwdRequired(usr.countPasswordResetRequired());
		
		// TODO Auto-generated method stub
		request.setAttribute("userProfile", user.getProfile());
		request.setAttribute("adminForm", adminForm);
		
		LOGGER.info("handleAction end");
		return actionForward;
	}

}
