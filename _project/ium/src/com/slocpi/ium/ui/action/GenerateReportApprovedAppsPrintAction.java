/*
 * Created on Mar 18, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.ui.action;

import java.io.ByteArrayOutputStream;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.slocpi.ium.data.ApplicationsApprovedFilterData;
import com.slocpi.ium.data.LOBData;
import com.slocpi.ium.data.SunLifeDeptData;
import com.slocpi.ium.data.UserData;
import com.slocpi.ium.data.UserProfileData;
import com.slocpi.ium.reports.ApplicationsApprovedReport;
import com.slocpi.ium.ui.util.StateHandler;
import com.slocpi.ium.util.CodeHelper;

/**
 * @author Nic Decapia
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class GenerateReportApprovedAppsPrintAction extends IUMAction
{
	private static final Logger LOGGER = LoggerFactory
		.getLogger(GenerateReportApprovedAppsPrintAction.class);
	/* (non-Javadoc)
	 * @see com.slocpi.ium.ui.action.IUMAction#handleAction(org.apache.struts.action.ActionMapping, org.apache.struts.action.ActionForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	public ActionForward handleAction(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response)
		throws Exception
	{
		
		LOGGER.info("handleAction start");
		String page = "";
					
		try {
			StateHandler sessionHandler = new StateHandler();
			UserData userData = sessionHandler.getUserData(request);
			UserProfileData userPref = userData.getProfile();
			
			ApplicationsApprovedFilterData filter = extractFilter(request);
			
			ApplicationsApprovedReport rpt = new ApplicationsApprovedReport();
			ByteArrayOutputStream baos =  rpt.generatePrinterFriendly(filter);
			
			response.reset();	
			
			response.setHeader("Content-Disposition", "inline; filename=MyReport.pdf");
			response.setContentType("application/pdf");
			response.setContentLength(baos.size());
			ServletOutputStream out = response.getOutputStream();
			
			baos.writeTo(out);
			out.flush();

		}
		catch (Exception e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			ActionErrors errors = new ActionErrors();
			errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.system.exception", e.getMessage()));
			saveErrors(request, errors);
			page = "errorPage";       	
		} 

		LOGGER.info("handleAction end");
		LOGGER.debug("PAGE FORWARD-->" + page);
		return null;
	}

	/**
	 * @param frmReport
	 * @return
	 */
	private ApplicationsApprovedFilterData extractFilter(HttpServletRequest frm) {
		
		LOGGER.info("extractFilter start");
		ApplicationsApprovedFilterData filter = new ApplicationsApprovedFilterData();
		
		filter.setStartDate(frm.getParameter("startDate"));
		filter.setEndDate(frm.getParameter("endDate"));

		String deptId = frm.getParameter("approver");
		SunLifeDeptData slo = new SunLifeDeptData();
		if (deptId.length() != 0) {
			slo.setDeptId(deptId);
		}
		else {
			slo.setDeptId(null);
		}
		filter.setApprovingParty(slo);
		
		String requestorId = frm.getParameter("requestor");
		LOBData lobFilter = new LOBData();
		if (requestorId.length() != 0) {
			lobFilter.setLOBCode(requestorId);
			
		}
		else {
			
			lobFilter.setLOBCode(null);
		}
		filter.setRequestingParty(lobFilter);
		
		LOGGER.info("extractFilter end");
		return (filter);
	}	
}
