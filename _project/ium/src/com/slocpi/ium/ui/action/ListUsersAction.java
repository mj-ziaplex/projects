package com.slocpi.ium.ui.action;


import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.slocpi.ium.IUMException;
import com.slocpi.ium.data.UserAccessData;
import com.slocpi.ium.data.UserData;
import com.slocpi.ium.data.UserProfileData;
import com.slocpi.ium.ui.form.UserProfileForm;
import com.slocpi.ium.ui.util.StateHandler;
import com.slocpi.ium.util.CodeHelper;


/**
 * @TODO Class Description ViewMedicalRecordListAction
 * @author: Ingrid Villanueva
 * @version: 1.0
 */
public class ListUsersAction extends IUMAction {
  
	private static final Logger LOGGER = LoggerFactory.getLogger(ListUsersAction.class);
  /**
   * @TODO method description for execute
   * @param mapping mapping of request to an instance of this class
   * @param form  object
   * @param request request object
   * @param response response object
   * @exception IOException if an input/output error occurs
   * @exception ServletException if a servlet exception occurs
   * @return ActionForward as defined in the mapping parameter.
   */
  public ActionForward handleAction(ActionMapping mapping, 
							   ActionForm form, 
							   HttpServletRequest request, 
							   HttpServletResponse response)
							   throws Exception {
	
	LOGGER.info("handleAction start");
	String page = "";
	try {
		StateHandler sh = new StateHandler();
		UserData ud = sh.getUserData(request);
		UserProfileData profile = ud.getProfile();
		String userId = profile.getUserId();
		String pageId = "";
		UserAccessData uad = extractAccessPriveleges(userId, pageId);
		
		if (uad.getAccessCode().equals("W")) {

            UserProfileForm userForm = (UserProfileForm) form;	
			String filter = request.getParameter("filter");
			String mode   = request.getParameter("mode");
			
			request.setAttribute("filter", filter);
			userForm.setUserType(filter);	
			request.setAttribute("userProfileForm", userForm);
			page = "userProfileDetailPage";
		}
		else {
			page = "errorPage";
		}

	}// end-try 
	catch (IUMException e) {
		LOGGER.error(CodeHelper.getStackTrace(e));
	    ActionErrors errors = new ActionErrors();
	    errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.system.exception", e.getMessage()));
	    saveErrors(request, errors); 
        page = "errorPage"; 
	}
	LOGGER.info("handleAction end");
	LOGGER.debug("PAGE FORWARD-->" + page);
	return (mapping.findForward(page));
  }// execute

    
}

