package com.slocpi.ium.ui.action;


import java.io.IOException;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.ResourceBundle;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.slocpi.ium.IUMException;
import com.slocpi.ium.data.AccessData;
import com.slocpi.ium.data.AccessTemplateData;
import com.slocpi.ium.data.AutoAssignCriteriaData;
import com.slocpi.ium.data.AutoAssignmentData;
import com.slocpi.ium.data.ClientTypeData;
import com.slocpi.ium.data.DocumentTypeData;
import com.slocpi.ium.data.ExaminationAreaData;
import com.slocpi.ium.data.ExaminationPlaceData;
import com.slocpi.ium.data.HolidayData;
import com.slocpi.ium.data.LOBData;
import com.slocpi.ium.data.MIBActionData;
import com.slocpi.ium.data.MIBImpairmentData;
import com.slocpi.ium.data.MIBLetterData;
import com.slocpi.ium.data.MIBNumberData;
import com.slocpi.ium.data.PageData;
import com.slocpi.ium.data.RankData;
import com.slocpi.ium.data.RequirementFormData;
import com.slocpi.ium.data.RolesData;
import com.slocpi.ium.data.SectionData;
import com.slocpi.ium.data.SpecializationData;
import com.slocpi.ium.data.StatusData;
import com.slocpi.ium.data.SunLifeDeptData;
import com.slocpi.ium.data.TestProfileData;
import com.slocpi.ium.data.UserData;
import com.slocpi.ium.data.UserProfileData;
import com.slocpi.ium.ui.form.ReferenceCodeForm;
import com.slocpi.ium.ui.form.ReferenceCodeListForm;
import com.slocpi.ium.ui.util.Page;
import com.slocpi.ium.ui.util.Pagination;
import com.slocpi.ium.ui.util.StateHandler;
import com.slocpi.ium.underwriter.Reference;
import com.slocpi.ium.underwriter.UnderWriterException;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.DateHelper;
import com.slocpi.ium.util.IUMConstants;
import com.slocpi.ium.util.SortHelper;


/**
 * @TODO Class Description ViewReferenceCodesAction
 * @author: Ma. Theresa Vicencio
 * @version: 1.0
 */
public class ViewReferenceCodeAction extends IUMAction {

	private static final Logger LOGGER = LoggerFactory.getLogger(ViewReferenceCodeAction.class);
	private static final String YES = "YES";
	private static final String NO = "NO"; 
	  
  /**
   * @TODO method description for execute
   * @param mapping mapping of request to an instance of this class
   * @param form  object
   * @param request request object
   * @param response response object
   * @exception IOException if an input/output error occurs
   * @exception ServletException if a servlet exception occurs
   * @return ActionForward as defined in the mapping parameter.
   */
  public ActionForward handleAction(ActionMapping mapping, 
							   ActionForm form, 
							   HttpServletRequest request, 
							   HttpServletResponse response)
							   throws Exception {
	  
	LOGGER.info("handleAction start");    
	String page = "";
	try {
		
		StateHandler sh = new StateHandler();
		UserData ud = sh.getUserData(request);
		UserProfileData profile = ud.getProfile();
		String userId = profile.getUserId();
		String pageId = "";

		ReferenceCodeListForm refCodeForm = (ReferenceCodeListForm) form;
		
		if (refCodeForm == null) {
			refCodeForm = new ReferenceCodeListForm();
		}

		ArrayList listDetail = new ArrayList();
		ArrayList listHeader = new ArrayList();
		
		String refCode = refCodeForm.getReferenceCode();
		String sortBy = refCodeForm.getSortBy();
		String sortOrder = refCodeForm.getSortOrder();
		String flag = refCodeForm.getMaintainFlag();
		
		if (flag.equals(IUMConstants.YES)) {
			String idxCode = refCodeForm.getIdxCode();
			refCodeForm.setCode(idxCode);			
		}
		
		if (refCode != null) {			
			
			if (refCode.equals(IUMConstants.REF_CODE_REQUIREMENT_FORMS)) {
				if (sortBy.trim().length() == 0) {
					sortBy = String.valueOf(IUMConstants.SORT_BY_REF_FORM_NAME);
				}
				if (sortOrder.trim().length() == 0) {
					sortOrder = IUMConstants.ASCENDING;
				}
				listDetail = populateRequirementFormList(sortBy, sortOrder, request);
				listHeader = populateRequirementFormHeader();										
				if (flag.equals(IUMConstants.YES)) {
					refCodeForm = populateRequirementFormDetail(refCodeForm);
				}
			}
			
			else if (refCode.equals(IUMConstants.REF_CODE_LOB)) {
				listDetail = populateLOBList();
				listHeader = populateCodeValuePairHeader();
				if (flag.equals(IUMConstants.YES)) {
					refCodeForm = populateLOBDetail(refCodeForm);
				}
			}
			
			else if (refCode.equals(IUMConstants.REF_CODE_DEPARTMENT)) {
				listDetail = populateDepartmentList();
				listHeader = populateCodeValuePairHeader();
				if (flag.equals(IUMConstants.YES)) {
					refCodeForm = populateDepartmentDetail(refCodeForm);
				}
			}
		
			else if (refCode.equals(IUMConstants.REF_CODE_SECTION)) {
				listDetail = populateSectionList();
				listHeader = populateCodeValuePairHeader();
				if (flag.equals(IUMConstants.YES)) {
					refCodeForm = populateSectionDetail(refCodeForm);
				}
			}
		
			else if (refCode.equals(IUMConstants.REF_CODE_EXAM_SPECIALIZATION)) {
				listDetail = populateExamSpecializationList();
				listHeader = populateCodeValuePairHeader();
				if (flag.equals(IUMConstants.YES)) {						
					refCodeForm = populateExamSpecializationDetail(refCodeForm);
				}
			}
			
			else if (refCode.equals(IUMConstants.REF_CODE_EXAM_PLACE)) {
				listDetail = populateExamPlaceList();
				listHeader = populateCodeValuePairHeader();
				if (flag.equals(IUMConstants.YES)) {
					refCodeForm = populateExamPlaceDetail(refCodeForm);
				}
			}
			
			else if (refCode.equals(IUMConstants.REF_CODE_EXAM_AREA)) {
				listDetail = populateExamAreaList();
				listHeader = populateCodeValuePairHeader();
				if (flag.equals(IUMConstants.YES)) {
					refCodeForm = populateExamAreaDetail(refCodeForm);
				}
			}
			
			else if (refCode.equals(IUMConstants.REF_CODE_DOCUMENT_TYPE)) {
				listDetail = populateDocumentTypeList();
				listHeader = populateCodeValuePairHeader();
				if (flag.equals(IUMConstants.YES)) {
					refCodeForm = populateDocumentTypeDetail(refCodeForm);
				}
			}
			 
			else if (refCode.equals(IUMConstants.REF_CODE_CLIENT_TYPE)) {
				listDetail = populateClientTypeList();
				listHeader = populateCodeValuePairHeader();
				if (flag.equals(IUMConstants.YES)) {
					refCodeForm = populateClientTypeDetail(refCodeForm);
				}
			}
							
			else if (refCode.equals(IUMConstants.REF_CODE_AUTO_ASSIGN_CRITERIA)) {
				listDetail = populateAutoAssignCriteriaList();
				listHeader = populateAutoAssignCriteriaHeader();
				if (flag.equals(IUMConstants.YES)) {
					refCodeForm = populateAutoAssignCriteriaDetail(refCodeForm);
				}
			}
			
			else if (refCode.equals(IUMConstants.REF_CODE_STATUS)) {
				listDetail = populateStatusCodeList();
				listHeader = populateStatusCodeHeader();
				if (flag.equals(IUMConstants.YES)) {
					refCodeForm = populateStatusCodeDetail(refCodeForm);
				}
			}
			
			else if (refCode.equals(IUMConstants.REF_CODE_ROLES)) {
				listDetail = populateRoleList();
				listHeader = populateCodeValuePairHeader();
				if (flag.equals(IUMConstants.YES)) {
					refCodeForm = populateRoleDetail(refCodeForm);
				}
			}
			
			else if (refCode.equals(IUMConstants.REF_CODE_ACCESS_TEMPLATE)) {
				listDetail = populateAccessTemplateList();
				listHeader = populateAccessTemplateHeader();
				if (flag.equals(IUMConstants.YES)) {
					refCodeForm = populateAccessTemplateDetail(refCodeForm);
				}
			}
			
			else if (refCode.equals(IUMConstants.REF_CODE_TEST_PROFILE)) {
				listDetail = populateTestProfileList();
				listHeader = populateTestProfileHeader();
				if (flag.equals(IUMConstants.YES)) {
					refCodeForm = populateTestProfileDetail(refCodeForm);
				}
			}
			
			else if (refCode.equals(IUMConstants.REF_CODE_EXAM_RANK)) {
				listDetail = populateExamRankList();
				listHeader = populateExamRankHeader();
				if (flag.equals(IUMConstants.YES)) {
					refCodeForm = populateExamRankDetail(refCodeForm);
				}
			}				
			
			else if (refCode.equals(IUMConstants.REF_CODE_MIB_IMPAIRMENT)) {
				listDetail = populateMIBImpairmentList();
				listHeader = populateCodeValuePairHeader();
				if (flag.equals(IUMConstants.YES)) {
					refCodeForm = populateMIBImpairmentDetail(refCodeForm);
				}
			}
			
			else if (refCode.equals(IUMConstants.REF_CODE_MIB_NUMBER)) {
				listDetail = populateMIBNumberList();
				listHeader = populateCodeValuePairHeader();
				if (flag.equals(IUMConstants.YES)) {
					refCodeForm = populateMIBNumberDetail(refCodeForm);
				}
			}
			
			else if (refCode.equals(IUMConstants.REF_CODE_MIB_ACTION)) {
				listDetail = populateMIBActionList();
				listHeader = populateCodeValuePairHeader();
				if (flag.equals(IUMConstants.YES)) {
					refCodeForm = populateMIBActionDetail(refCodeForm);
				}
			}
			
			else if (refCode.equals(IUMConstants.REF_CODE_MIB_LETTER)) {
				listDetail = populateMIBLetterList();
				listHeader = populateCodeValuePairHeader();
				if (flag.equals(IUMConstants.YES)) {
					refCodeForm = populateMIBLetterDetail(refCodeForm);
				}
			}
			
			else if (refCode.equals(IUMConstants.REF_CODE_HOLIDAY)) {
				String year = refCodeForm.getYear();
				if ((year == null) || (year.equals(""))) {
					year = String.valueOf(Calendar.getInstance().get(Calendar.YEAR));
					refCodeForm.setYear(year);
				}
				listDetail = populateHolidayList(year);
				listHeader = populateHolidayHeader();
				if (flag.equals(IUMConstants.YES)) {
					refCodeForm = populateHolidayDetail(refCodeForm);
				}
			}				
			
			else if (refCode.equals(IUMConstants.REF_CODE_AUTO_ASSIGN)) {
				if (sortBy.trim().length() == 0) {
					sortBy = String.valueOf(IUMConstants.SORT_BY_REF_CRITERIA_CODE);
				}
				if (sortOrder.trim().length() == 0) {
					sortOrder = IUMConstants.ASCENDING;
				}
				listDetail = populateAutoAssignmentList(sortBy, sortOrder);
				listHeader = populateAutoAssignmentHeader();
				if (flag.equals(IUMConstants.YES)) {
					refCodeForm = populateAutoAssignDetail(refCodeForm);
				}
			}
		
			else if (refCode.equals(IUMConstants.REF_CODE_ACCESS)) {
				listDetail = populateAccessList();
				listHeader = populateCodeValuePairHeader();
				if (flag.equals(IUMConstants.YES)) {
					refCodeForm = populateAccessDetail(refCodeForm);
				}
			}
			
			else if (refCode.equals(IUMConstants.REF_CODE_PAGE)) {
				listDetail = populatePageList();
				listHeader = populateCodeValuePairHeader();
				if (flag.equals(IUMConstants.YES)) {
					refCodeForm = populatePageDetail(refCodeForm);
				}
			}
		}

		refCodeForm.setSortBy(sortBy);
		refCodeForm.setSortOrder(sortOrder);
		
		int recPerView = profile.getRecordsPerView();
		
		if (recPerView == 0) {
		  recPerView = 1;
		}
		int pageNo = 1;
		String reqPageNum = request.getParameter("pageNo");
		
		if ((reqPageNum != null) && !(reqPageNum.equals(""))) {
			try {
				pageNo = Integer.parseInt(reqPageNum);
			}
			catch (NumberFormatException e) {
				//pageNo will be the first page   
				LOGGER.error(CodeHelper.getStackTrace(e));
			}
		}
		
		Pagination pgn = new Pagination(listDetail, recPerView);
		Page pg = pgn.getPage(pageNo);			

		 
		refCodeForm.setListDetails(pg.getList());
		refCodeForm.setListHeaders(listHeader);

		request.setAttribute("refCodeForm", refCodeForm);
		request.setAttribute("page", pg);
		request.setAttribute("pageNo", String.valueOf(pageNo));				

		page = "viewAdminReferencePage";
		
	} 
	catch (UnderWriterException e) {
		LOGGER.error(CodeHelper.getStackTrace(e));
		ActionErrors errors = new ActionErrors();
		errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.admin.generic", e.getMessage()));
		saveErrors(request, errors);
	    page = "viewAdminReferencePage";
	}	
	catch (IUMException e) {
		LOGGER.error(CodeHelper.getStackTrace(e));
		ActionErrors errors = new ActionErrors();
		errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.system.exception", e.getMessage()));
		saveErrors(request, errors);
		page = "errorPage";
	}//end-catch
	catch (Exception e) {
		LOGGER.error(CodeHelper.getStackTrace(e));
		ActionErrors errors = new ActionErrors();
		errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.system.exception", e.getMessage()));
		saveErrors(request, errors);
		page = "errorPage";
	}
	
	LOGGER.info("handleAction end");
	LOGGER.debug("PAGE FORWARD-->" + page);
	return (mapping.findForward(page));
  }//handleAction


  private ArrayList populateRequirementFormList(String sortBy, String sortOrder, HttpServletRequest request) throws UnderWriterException, IUMException {
	  
	LOGGER.info("populateRequirementFormList start");
	SortHelper sort = new SortHelper(IUMConstants.TABLE_REQUIREMENT_FORMS);
	if (sortBy.trim().length() > 0) {
		sort.setColumn(Integer.parseInt(sortBy));
		sort.setSortOrder(sortOrder);
	}
	
	Reference ref = new Reference();
	ArrayList list = ref.getRequirementForms(sort);

	int size = list.size();
	ArrayList refList = new ArrayList();

	ResourceBundle rb = ResourceBundle.getBundle(IUMConstants.IUM_CONFIG);
	String contextPath = request.getContextPath();
	String folderLocation = rb.getString(IUMConstants.IUM_FORMSTEMPLATES_FOLDER);
	String url = contextPath + "\\" + folderLocation + "\\";
	
	for (int i = 0; i < size; i++) {
		RequirementFormData data = (RequirementFormData)list.get(i);

		String templateFormat = data.getTemplateFormat();
		String templateName = data.getTemplateName();

		ReferenceCodeForm form = new ReferenceCodeForm();
		form.setFormName(data.getFormName());
		form.setTemplateFormat(templateFormat);
		form.setTemplateName(templateName);
		boolean isActive = data.getStatus();
		String status = NO;
		if (isActive) {
			status = YES;
		}
		form.setStatus(status);
		form.setUrlFile(url + replaceAll(templateName,"'", "%27") + "." + templateFormat.toLowerCase());
		refList.add(form);
	}
	
	LOGGER.info("populateRequirementFormList end");
	return (refList);
  }

  private ReferenceCodeListForm populateRequirementFormDetail(ReferenceCodeListForm form) throws UnderWriterException, IUMException {
	
	LOGGER.info("populateRequirementFormDetail start");
	Reference ref = new Reference();
	RequirementFormData data = ref.getRequirementFormDetail(form.getCode());

	ResourceBundle rb = ResourceBundle.getBundle(IUMConstants.IUM_CONFIG);
	String location = rb.getString(IUMConstants.IUM_FORMSTEMPLATES_PATH);
	String templateFormat = data.getTemplateFormat();
	String templateName = data.getTemplateName();

	form.setFormName(data.getFormName());
	form.setTemplateFormat(templateFormat);
	form.setTemplateName(templateName);
	boolean isActive = data.getStatus();
	String status = IUMConstants.NO;
	if (isActive) {
		status = IUMConstants.YES;
	}
	form.setStatus(status);
	
	form.setUrlFile(location + templateName.replaceAll("'", "%27") + "." + templateFormat.toLowerCase());
	
	LOGGER.info("populateRequirementFormDetail end");
	return (form);
  }
    
  
  private ArrayList populateRequirementFormHeader() throws UnderWriterException, IUMException {
	
	LOGGER.info("populateRequirementFormHeader start");
	ArrayList header = new ArrayList();
	header.add("FORM NAME".toUpperCase());
	header.add("TEMPLATE FORMAT".toUpperCase());
	header.add("TEMPLATE NAME".toUpperCase());
	header.add("ACTIVE".toUpperCase());
	header.add(""); 
	
	LOGGER.info("populateRequirementFormHeader end");
	return (header);
  }


  private ArrayList populateLOBList() throws UnderWriterException, IUMException {
	
	LOGGER.info("populateLOBList start");
	Reference ref = new Reference();
	ArrayList list = ref.getLOBList();

	int size = list.size();
	ArrayList refList = new ArrayList();
	
	for (int i = 0; i < size; i++) {
		LOBData data = (LOBData)list.get(i);
		ReferenceCodeForm form = new ReferenceCodeForm();
		form.setCode(data.getLOBCode());
		form.setDescription(data.getLOBDesc());
		refList.add(form);
	}
	
	LOGGER.info("populateLOBList end");
	return (refList);
  }//populateLOBList


  private ReferenceCodeListForm populateLOBDetail(ReferenceCodeListForm form) throws UnderWriterException, IUMException {
	
	LOGGER.info("populateLOBDetail start");
	
	Reference ref = new Reference();
	LOBData data = ref.getLOBDetail(form.getCode());
	form.setCode(data.getLOBCode());
	form.setDescription(data.getLOBDesc());
	LOGGER.info("populateLOBDetail end");
	
	return (form);
  }//populateLOBDetail
  
  
  private ArrayList populateDepartmentList() throws UnderWriterException, IUMException {
	  
	LOGGER.info("populateDepartmentList start");
	Reference ref = new Reference();
	ArrayList list = ref.getDepartments();

	int size = list.size();
	ArrayList refList = new ArrayList();
	
	for (int i = 0; i < size; i++) {
		SunLifeDeptData data = (SunLifeDeptData)list.get(i);
		ReferenceCodeForm form = new ReferenceCodeForm();
		form.setCode(data.getDeptId());
		form.setDescription(data.getDeptDesc());
		refList.add(form);
	}
	LOGGER.info("populateDepartmentList end");
	return (refList);
  }


  private ReferenceCodeListForm populateDepartmentDetail(ReferenceCodeListForm form) throws UnderWriterException, IUMException {
	  
	LOGGER.info("populateDepartmentDetail start");
	
	Reference ref = new Reference();
	SunLifeDeptData data = ref.getDepartmentDetail(form.getCode());
	form.setCode(data.getDeptId());
	form.setDescription(data.getDeptDesc());
	LOGGER.info("populateDepartmentDetail end");
	
	return (form);
  }


  private ArrayList populateSectionList() throws UnderWriterException, IUMException {
	
	LOGGER.info("populateSectionList start");
	Reference ref = new Reference();
	ArrayList list = ref.getSections();

	int size = list.size();
	ArrayList refList = new ArrayList();
	
	for (int i = 0; i < size; i++) {
		SectionData data = (SectionData)list.get(i);
		ReferenceCodeForm form = new ReferenceCodeForm();
		form.setCode(data.getSectionId());
		form.setDescription(data.getSectionDesc());
		refList.add(form);
	}

	LOGGER.info("populateSectionList end");
	return (refList);
  }//populateSectionList


  private ReferenceCodeListForm populateSectionDetail(ReferenceCodeListForm form) throws UnderWriterException, IUMException {
	
	LOGGER.info("populateSectionDetail start");
	
	Reference ref = new Reference();
	SectionData data = ref.getSectionDetail(form.getCode());
	form.setCode(data.getSectionId());
	form.setDescription(data.getSectionDesc());
	
	LOGGER.info("populateSectionDetail end");
	return (form);
  }//populateSectionDetail


  private ArrayList populateExamSpecializationList() throws UnderWriterException, IUMException {
	
	LOGGER.info("populateExamSpecializationList start");
	Reference ref = new Reference();
	ArrayList list = ref.getExamSpecializations();

	int size = list.size();
	ArrayList refList = new ArrayList();
	
	for (int i = 0; i < size; i++) {
		SpecializationData data = (SpecializationData)list.get(i);
		ReferenceCodeForm form = new ReferenceCodeForm();
		form.setCode(String.valueOf(data.getSpecializationId()));
		form.setDescription(data.getSpecializationDesc());
		refList.add(form);
	}
	
	LOGGER.info("populateExamSpecializationList end");
	return (refList);
  }


  private ReferenceCodeListForm populateExamSpecializationDetail(ReferenceCodeListForm form) throws UnderWriterException, IUMException {
	  
	LOGGER.info("populateExamSpecializationDetail start");
	Reference ref = new Reference();
	SpecializationData data = ref.getExamSpecializationDetail(Long.parseLong(form.getCode()));
	form.setCode(String.valueOf(data.getSpecializationId()));
	form.setDescription(data.getSpecializationDesc());
	
	LOGGER.info("populateExamSpecializationDetail end");
	return (form);
  }//populateExamSpecializationDetail
  

  private ArrayList populateExamPlaceList() throws UnderWriterException, IUMException {
	
	LOGGER.info("populateExamPlaceList start");
	Reference ref = new Reference();
	ArrayList list = ref.getExamPlaces();

	int size = list.size();
	ArrayList refList = new ArrayList();
	
	for (int i = 0; i < size; i++) {
		ExaminationPlaceData data = (ExaminationPlaceData)list.get(i);
		ReferenceCodeForm form = new ReferenceCodeForm();
		form.setCode(String.valueOf(data.getExaminationPlaceId()));
		form.setDescription(data.getExaminationPlaceDesc());
		refList.add(form);
	}
	
	LOGGER.info("populateExamPlaceList end");
	return (refList);
  }


  private ReferenceCodeListForm populateExamPlaceDetail(ReferenceCodeListForm form) throws UnderWriterException, IUMException {
	
	LOGGER.info("populateExamPlaceDetail start");
	Reference ref = new Reference();
	ExaminationPlaceData data = ref.getExamPlaceDetail(Long.parseLong(form.getCode()));
	form.setCode(String.valueOf(data.getExaminationPlaceId()));
	form.setDescription(data.getExaminationPlaceDesc());
	
	LOGGER.info("populateExamPlaceDetail end");
	return (form);
  }
      

  private ArrayList populateExamAreaList() throws UnderWriterException, IUMException {
	
	LOGGER.info("populateExamAreaList start");
	Reference ref = new Reference();
	ArrayList list = ref.getExamAreas();

	int size = list.size();
	ArrayList refList = new ArrayList();
	
	for (int i = 0; i < size; i++) {
		ExaminationAreaData data = (ExaminationAreaData)list.get(i);
		ReferenceCodeForm form = new ReferenceCodeForm();
		form.setCode(String.valueOf(data.getExaminationAreaId()));
		form.setDescription(data.getExaminationAreaDesc());
		refList.add(form);
	}
	
	LOGGER.info("populateExamAreaList end");
	return (refList);
  }//populateExamAreaList


  private ReferenceCodeListForm populateExamAreaDetail(ReferenceCodeListForm form) throws UnderWriterException, IUMException {
	
	LOGGER.info("populateExamAreaDetail start");
	Reference ref = new Reference();
	ExaminationAreaData data = ref.getExamAreaDetail(Long.parseLong(form.getCode()));
	form.setCode(String.valueOf(data.getExaminationAreaId()));
	form.setDescription(data.getExaminationAreaDesc());
	LOGGER.info("populateExamAreaDetail end");
	return (form);
  }//populateExamAreaDetail
    

  private ArrayList populateDocumentTypeList() throws UnderWriterException, IUMException {
	
    LOGGER.info("populateDocumentTypeList start");
	Reference ref = new Reference();
	ArrayList list = ref.getDocumentTypes();

	int size = list.size();
	ArrayList refList = new ArrayList();
	
	for (int i = 0; i < size; i++) {
		DocumentTypeData data = (DocumentTypeData)list.get(i);
		ReferenceCodeForm form = new ReferenceCodeForm();
		form.setCode(data.getDocCode());
		form.setDescription(data.getDocDesc());
		refList.add(form);
	}
	
	LOGGER.info("populateDocumentTypeList end");
	return (refList);
  }//populateDocumentTypeList


  private ReferenceCodeListForm populateDocumentTypeDetail(ReferenceCodeListForm form) throws UnderWriterException, IUMException {
	
    LOGGER.info("populateDocumentTypeDetail start");
	
    Reference ref = new Reference();
	DocumentTypeData data = ref.getDocumentTypeDetail(form.getCode());
	form.setCode(data.getDocCode());
	form.setDescription(data.getDocDesc());
	
	LOGGER.info("populateDocumentTypeDetail end");
	return (form);
  }//populateDocumentTypeDetail
  

  private ArrayList populateClientTypeList() throws UnderWriterException, IUMException {
	
	LOGGER.info("populateClientTypeList start");
	Reference ref = new Reference();
	ArrayList list = ref.getClientTypes();

	int size = list.size();
	ArrayList refList = new ArrayList();
	
	for (int i = 0; i < size; i++) {
		ClientTypeData data = (ClientTypeData)list.get(i);
		ReferenceCodeForm form = new ReferenceCodeForm();
		form.setCode(data.getClientTypeCode());
		form.setDescription(data.getClientTypeDesc());
		refList.add(form);
	}
	
	LOGGER.info("populateClientTypeList end");
	return (refList);
  }


  private ReferenceCodeListForm populateClientTypeDetail(ReferenceCodeListForm form) throws UnderWriterException, IUMException {
	
	LOGGER.info("populateClientTypeDetail start");
	
	Reference ref = new Reference();
	ClientTypeData data = ref.getClientTypeDetail(form.getCode());
	form.setCode(data.getClientTypeCode());
	form.setDescription(data.getClientTypeDesc());
	
	LOGGER.info("populateClientTypeDetail end");
	return (form);
  }
  
  
  private ArrayList populateCodeValuePairHeader() throws UnderWriterException, IUMException {
	
    LOGGER.info("populateCodeValuePairHeader start");
	ArrayList header = new ArrayList();
	header.add("CODE".toUpperCase());
	header.add("DESCRIPTION".toUpperCase());
	
	LOGGER.info("populateCodeValuePairHeader end");
	return (header);
  }


  private ArrayList populateAutoAssignCriteriaList() throws UnderWriterException, IUMException {
	
	LOGGER.info("populateAutoAssignCriteriaList start");
	Reference ref = new Reference();
	ArrayList list = ref.getAutoAssignCriteria();

	int size = list.size();
	ArrayList refList = new ArrayList();
	
	for (int i = 0; i < size; i++) {
		AutoAssignCriteriaData data = (AutoAssignCriteriaData)list.get(i);
		ReferenceCodeForm form = new ReferenceCodeForm();
		form.setId(String.valueOf(data.getCriteriaId()));
		form.setCode(data.getFieldCode());
		form.setDescription(data.getFieldDesc());
		boolean isSent = data.getNotificationSent();
		String status = NO;
		if (isSent) {
			status = YES;
		}
		form.setStatus(status);
		refList.add(form);
	}
	
	LOGGER.info("populateAutoAssignCriteriaList end");
	return (refList);
  }


  private ReferenceCodeListForm populateAutoAssignCriteriaDetail(ReferenceCodeListForm form) throws UnderWriterException, IUMException {
	
	LOGGER.info("populateAutoAssignCriteriaDetail start");
	Reference ref = new Reference();
	AutoAssignCriteriaData data = ref.getAutoAssignCriteriaDetail(form.getCode());
	form.setId(String.valueOf(data.getCriteriaId()));
	form.setCode(data.getFieldCode());
	form.setDescription(data.getFieldDesc());
	boolean isSent = data.getNotificationSent();
	String status = IUMConstants.NO;
	if (isSent) {
		status = IUMConstants.YES;
	}
	form.setStatus(status);
	
	LOGGER.info("populateAutoAssignCriteriaDetail end");
	return (form);
  }
  

  private ArrayList populateAutoAssignCriteriaHeader() throws UnderWriterException, IUMException {
	
	LOGGER.info("populateAutoAssignCriteriaHeader start");
	ArrayList header = new ArrayList();
	header.add("CODE".toUpperCase());
	header.add("DESCRIPTION".toUpperCase());
	header.add("ACTIVE".toUpperCase());
	
	LOGGER.info("populateAutoAssignCriteriaHeader end");
	return (header);
  }//populateAutoAssignCriteriaHeader
    
    
  private ArrayList populateStatusCodeList() throws UnderWriterException, IUMException {
	
	LOGGER.info("populateStatusCodeList start");
	Reference ref = new Reference();
	ArrayList list = ref.getStatusCodes();

	int size = list.size();
	ArrayList refList = new ArrayList();
	
	for (int i = 0; i < size; i++) {
		StatusData data = (StatusData)list.get(i);
		ReferenceCodeForm form = new ReferenceCodeForm();
		form.setCode(String.valueOf(data.getStatusId()));
		form.setDescription(data.getStatusDesc());
		String type = data.getStatusType();
		if (type.equals(IUMConstants.STAT_TYPE_AR)) {
			type = IUMConstants.STAT_TYPE_DESC_AR;		
		}
		else if (type.equals(IUMConstants.STAT_TYPE_NM)) {
			type = IUMConstants.STAT_TYPE_DESC_NM;		
		}
		else if (type.equals(IUMConstants.STAT_TYPE_M)) {
			type = IUMConstants.STAT_TYPE_DESC_M;		
		}
		form.setType(type);
		refList.add(form);
	}
	
	LOGGER.info("populateStatusCodeList end");
	return (refList);
  }//populateStatusCodeList


  private ReferenceCodeListForm populateStatusCodeDetail(ReferenceCodeListForm form) throws UnderWriterException, IUMException {
	
	LOGGER.info("populateStatusCodeDetail start");
	
	Reference ref = new Reference();
	StatusData data = ref.getStatusCodeDetail(Long.parseLong(form.getCode()));
	form.setCode(String.valueOf(data.getStatusId()));
	form.setDescription(data.getStatusDesc());
	form.setType(data.getStatusType());
	
	LOGGER.info("populateStatusCodeDetail end");
	return (form);
  }
  

  private ArrayList populateRoleList() throws UnderWriterException, IUMException {
	
	LOGGER.info("populateRoleList start");
	Reference ref = new Reference();
	ArrayList list = ref.getRoles();

	int size = list.size();
	ArrayList refList = new ArrayList();
	
	for (int i = 0; i < size; i++) {
		RolesData data = (RolesData)list.get(i);
		ReferenceCodeForm form = new ReferenceCodeForm();
		form.setCode(data.getRolesId());
		form.setDescription(data.getRolesDesc());
		refList.add(form);
	}
	
	LOGGER.info("populateRoleList end");
	return (refList);
  }//populateRoles


  private ReferenceCodeListForm populateRoleDetail(ReferenceCodeListForm form) throws UnderWriterException, IUMException {
	
	LOGGER.info("populateRoleDetail start");
	Reference ref = new Reference();
	RolesData data = ref.getRoleDetail(form.getCode());
	form.setCode(data.getRolesId());
	form.setDescription(data.getRolesDesc());
	
	LOGGER.info("populateRoleDetail end");
	return (form);
  }//populateRoleDetail
  

  private ArrayList populateStatusCodeHeader() throws UnderWriterException, IUMException {
	
	LOGGER.info("populateStatusCodeHeader start");
	ArrayList header = new ArrayList();
	header.add("CODE".toUpperCase());
	header.add("DESCRIPTION".toUpperCase());
	header.add("TYPE".toUpperCase());
	LOGGER.info("populateStatusCodeHeader end");
	return (header);
  }  
  

  private ArrayList populateAccessTemplateList() throws UnderWriterException, IUMException {
	
	LOGGER.info("populateAccessTemplateList start");
	Reference ref = new Reference();
	ArrayList list = ref.getAccessTemplates();

	int size = list.size();
	ArrayList refList = new ArrayList();
	
	for (int i = 0; i < size; i++) {
		AccessTemplateData data = (AccessTemplateData)list.get(i);
		ReferenceCodeForm form = new ReferenceCodeForm();
		form.setCode(String.valueOf(data.getTemplateID()));
		form.setDescription(data.getTemplateDesc());
		RolesData role = data.getRole();
		form.setId(role.getRolesId());
		refList.add(form);
	}
	
	LOGGER.info("populateAccessTemplateList end");
	return (refList);
  }


  private ReferenceCodeListForm populateAccessTemplateDetail(ReferenceCodeListForm form) throws UnderWriterException, IUMException {
	
	LOGGER.info("populateAccessTemplateDetail start");
	
	Reference ref = new Reference();
	AccessTemplateData data = ref.getAccessTemplateDetail(Long.parseLong(form.getCode()));
	form.setCode(String.valueOf(data.getTemplateID()));
	form.setDescription(data.getTemplateDesc());
	RolesData role = data.getRole();
	form.setId(role.getRolesId());
	
	LOGGER.info("populateAccessTemplateDetail end");
	return (form);
  }
  

  private ArrayList populateAccessTemplateHeader() throws UnderWriterException, IUMException {
	  
	LOGGER.info("populateAccessTemplateHeader start");
	ArrayList header = new ArrayList();
	header.add("CODE".toUpperCase());
	header.add("DESCRIPTION".toUpperCase());
	header.add("ROLE ID".toUpperCase());
	
	LOGGER.info("populateAccessTemplateHeader end");
	return (header);
  }  
  
  
  private ArrayList populateTestProfileList() throws UnderWriterException, IUMException {
	  
	  LOGGER.info("populateTestProfileList start");
	Reference ref = new Reference();
	ArrayList list = ref.getTestProfiles();

	int size = list.size();
	ArrayList refList = new ArrayList();
	
	for (int i = 0; i < size; i++) {
		TestProfileData data = (TestProfileData)list.get(i);
		ReferenceCodeForm form = new ReferenceCodeForm();
		form.setCode(String.valueOf(data.getTestId()));		
		form.setDescription(data.getTestDesc());		
		String type = data.getTestType();
		if ((type != null) && (type.equals(IUMConstants.TEST_TYPE_MEDICAL))) {
			type = "Medical";		
		}
		else if ((type != null) && (type.equals(IUMConstants.TEST_TYPE_LABORATORY))) {
			type = "Laboratory";		
		}
		form.setType(type);
		form.setDaysValid(String.valueOf(data.getValidity()));
		boolean isTaxable = data.isTaxable();
		String taxable = NO;
		if (isTaxable) {
			taxable = YES;
		}		
		form.setTaxable(taxable);
		form.setFollowUpNo(String.valueOf(data.getFollowUpNumber()));
		refList.add(form);
	}
	LOGGER.info("populateTestProfileList end");
	return (refList);
  }//populateTestProfileList


  private ReferenceCodeListForm populateTestProfileDetail(ReferenceCodeListForm form) throws UnderWriterException, IUMException {
	
	  LOGGER.info("populateTestProfileDetail start");
	Reference ref = new Reference();
	TestProfileData data = ref.getTestProfileDetail(Long.parseLong(form.getCode()));
	form.setCode(String.valueOf(data.getTestId()));
	form.setDescription(data.getTestDesc());
	form.setType(data.getTestType());
	form.setDaysValid(String.valueOf(data.getValidity()));	
	boolean isTaxable = data.isTaxable();
	String taxable = IUMConstants.NO;
	if (isTaxable) {
		taxable = IUMConstants.YES;
	}
	form.setTaxable(taxable);
	form.setFollowUpNo(String.valueOf(data.getFollowUpNumber()));
	LOGGER.info("populateTestProfileDetail end");
	return (form);
  }
      
  
  private ArrayList populateTestProfileHeader() throws UnderWriterException, IUMException {
	
	  LOGGER.info("populateTestProfileHeader start");
	ArrayList header = new ArrayList();
	header.add("CODE".toUpperCase());
	header.add("DESCRIPTION".toUpperCase());
	header.add("TYPE".toUpperCase());
	header.add("NO. OF DAYS VALID".toUpperCase());
	header.add("TAXABLE".toUpperCase());
	header.add("FOLLOW UP NO.".toUpperCase());
	
	LOGGER.info("populateTestProfileHeader end");
	return (header);
  }


  private ArrayList populateExamRankList() throws UnderWriterException, IUMException {
	  
	LOGGER.info("populateExamRankList start");

	NumberFormat nf = NumberFormat.getInstance();
	nf.setMinimumFractionDigits(2);
	nf.setMaximumFractionDigits(2);

	Reference ref = new Reference();
	ArrayList list = ref.getExamRanks();

	int size = list.size();
	ArrayList refList = new ArrayList();
	
	for (int i = 0; i < size; i++) {
		RankData data = (RankData)list.get(i);
		ReferenceCodeForm form = new ReferenceCodeForm();
		form.setCode(String.valueOf(data.getRankCode()));		
		form.setDescription(data.getRankDesc());
		form.setFee(nf.format(data.getRankFee()));
		refList.add(form);
	}
	
	LOGGER.info("populateExamRankList end");
	return (refList);
  }//populateExamRankList


  private ReferenceCodeListForm populateExamRankDetail(ReferenceCodeListForm form) throws UnderWriterException, IUMException {
	
	LOGGER.info("populateExamRankDetail start");
	NumberFormat nf = NumberFormat.getInstance();
	nf.setMinimumFractionDigits(2);
	nf.setMaximumFractionDigits(2);
		
	Reference ref = new Reference();
	RankData data = ref.getExamRankDetail(Long.parseLong(form.getCode()));
	form.setCode(String.valueOf(data.getRankCode()));
	form.setDescription(data.getRankDesc());
	form.setFee(nf.format(data.getRankFee()));
	
	LOGGER.info("populateExamRankDetail end");
	
	return (form);
  }
      
  
  private ArrayList populateExamRankHeader() throws UnderWriterException, IUMException {
	
	LOGGER.info("populateExamRankHeader start");
	ArrayList header = new ArrayList();
	header.add("CODE".toUpperCase());
	header.add("DESCRIPTION".toUpperCase());
	header.add("FEE".toUpperCase());
	
	LOGGER.info("populateExamRankHeader end");
	return (header);
  }


  private ArrayList populateMIBImpairmentList() throws UnderWriterException, IUMException {
	  
	  LOGGER.info("populateMIBImpairmentList start");
	
	Reference ref = new Reference();
	ArrayList list = ref.getMIBImpairmentList();

	int size = list.size();
	ArrayList refList = new ArrayList();
	
	for (int i = 0; i < size; i++) {
		MIBImpairmentData data = (MIBImpairmentData)list.get(i);
		ReferenceCodeForm form = new ReferenceCodeForm();
		form.setCode(data.getMIBImpairmentCode());
		form.setDescription(data.getMIBImpairmentDesc());
		refList.add(form);
	}
	LOGGER.info("populateMIBImpairmentList end");
	return (refList);
  }


  private ReferenceCodeListForm populateMIBImpairmentDetail(ReferenceCodeListForm form) throws UnderWriterException, IUMException {
	
	LOGGER.info("populateMIBImpairmentDetail start");
	
	Reference ref = new Reference();
	MIBImpairmentData data = ref.getMIBImpairmentDetail(form.getCode());
	form.setCode(data.getMIBImpairmentCode());
	form.setDescription(data.getMIBImpairmentDesc());
	
	LOGGER.info("populateMIBImpairmentDetail end");
	
	return (form);
  }


  private ArrayList populateMIBNumberList() throws UnderWriterException, IUMException {
	
	LOGGER.info("populateMIBNumberList start");
	Reference ref = new Reference();
	ArrayList list = ref.getMIBNumberList();

	int size = list.size();
	ArrayList refList = new ArrayList();
	
	for (int i = 0; i < size; i++) {
		MIBNumberData data = (MIBNumberData)list.get(i);
		ReferenceCodeForm form = new ReferenceCodeForm();
		form.setCode(data.getMIBNumberCode());
		form.setDescription(data.getMIBNumberDesc());
		refList.add(form);
	}
	
	LOGGER.info("populateMIBNumberList end");
	return (refList);
  }//populateMIBNumberList


  private ReferenceCodeListForm populateMIBNumberDetail(ReferenceCodeListForm form) throws UnderWriterException, IUMException {
	
	 LOGGER.info("populateMIBNumberDetail start");
	Reference ref = new Reference();
	MIBNumberData data = ref.getMIBNumberDetail(form.getCode());
	form.setCode(data.getMIBNumberCode());
	form.setDescription(data.getMIBNumberDesc());
	LOGGER.info("populateMIBNumberDetail end");
	return (form);
  }//populateMIBNumberDetail


  private ArrayList populateMIBActionList() throws UnderWriterException, IUMException {
	
	  LOGGER.info("populateMIBActionList start");
	Reference ref = new Reference();
	ArrayList list = ref.getMIBActionList();

	int size = list.size();
	ArrayList refList = new ArrayList();
	
	for (int i = 0; i < size; i++) {
		MIBActionData data = (MIBActionData)list.get(i);
		ReferenceCodeForm form = new ReferenceCodeForm();
		form.setCode(data.getMIBActionCode());
		form.setDescription(data.getMIBActionDesc());
		refList.add(form);
	}
	LOGGER.info("populateMIBActionList end");
	return (refList);
  }//populateMIBActionList


  private ReferenceCodeListForm populateMIBActionDetail(ReferenceCodeListForm form) throws UnderWriterException, IUMException {
	
	  LOGGER.info("populateMIBActionDetail start");
	Reference ref = new Reference();
	MIBActionData data = ref.getMIBActionDetail(form.getCode());
	form.setCode(data.getMIBActionCode());
	form.setDescription(data.getMIBActionDesc());
	LOGGER.info("populateMIBActionDetail end");
	return (form);
  }


  private ArrayList populateMIBLetterList() throws UnderWriterException, IUMException {
	
	  LOGGER.info("populateMIBLetterList start");
	Reference ref = new Reference();
	ArrayList list = ref.getMIBLetterList();

	int size = list.size();
	ArrayList refList = new ArrayList();
	
	for (int i = 0; i < size; i++) {
		MIBLetterData data = (MIBLetterData)list.get(i);
		ReferenceCodeForm form = new ReferenceCodeForm();
		form.setCode(data.getMIBLetterCode());
		form.setDescription(data.getMIBLetterDesc());
		refList.add(form);
	}
	
	LOGGER.info("populateMIBLetterList end");
	return (refList);
  }

  private ReferenceCodeListForm populateMIBLetterDetail(ReferenceCodeListForm form) throws UnderWriterException, IUMException {
	  
	  LOGGER.info("populateMIBLetterDetail start");
	Reference ref = new Reference();
	MIBLetterData data = ref.getMIBLetterDetail(form.getCode());
	form.setCode(data.getMIBLetterCode());
	form.setDescription(data.getMIBLetterDesc());
	LOGGER.info("populateMIBLetterDetail end");
	return (form);
  }//populateMIBLetterDetail


  private ArrayList populateHolidayList(String year) throws UnderWriterException, IUMException {
	
	  LOGGER.info("populateHolidayList start");
	Reference ref = new Reference();
	ArrayList list = ref.getHolidayList(year);

	int size = list.size();
	ArrayList refList = new ArrayList();
	
	for (int i = 0; i < size; i++) {
		HolidayData data = (HolidayData)list.get(i);
		ReferenceCodeForm form = new ReferenceCodeForm();
		form.setCode(String.valueOf(data.getHolidayCode()));
		form.setDescription(data.getHolidayDesc());
		form.setDate((DateHelper.format(data.getHolidayDate(), "ddMMMyyyy")).toUpperCase());
		refList.add(form);
	}
	LOGGER.info("populateHolidayList end");
	return (refList);
  }//populateHolidayList


  private ReferenceCodeListForm populateHolidayDetail(ReferenceCodeListForm form) throws UnderWriterException, IUMException {
	
	LOGGER.info("populateHolidayDetail start");
	
	Reference ref = new Reference();
	HolidayData data = ref.getHolidayDetail(form.getCode());
	form.setCode(String.valueOf(data.getHolidayCode()));
	form.setDescription(data.getHolidayDesc());
	form.setDate((DateHelper.format(data.getHolidayDate(), "ddMMMyyyy")).toUpperCase());
	LOGGER.info("populateHolidayDetail end");
	
	return (form);
  }


  private ArrayList populateHolidayHeader() throws UnderWriterException, IUMException {
	
	LOGGER.info("populateHolidayHeader start");
	  
	ArrayList header = new ArrayList();
	header.add("DATE".toUpperCase());
	header.add("DESCRIPTION".toUpperCase());
	
	LOGGER.info("populateHolidayHeader end");
	return (header);
  }//populateHolidayHeader
    

  private ArrayList populateAutoAssignmentList(String sortBy, String sortOrder) throws UnderWriterException, IUMException {
	
	LOGGER.info("populateAutoAssignmentList start");
	SortHelper sort = new SortHelper(IUMConstants.TABLE_AUTO_ASSIGNMENTS);
	if (sortBy.trim().length() > 0) {
		sort.setColumn(Integer.parseInt(sortBy));
		sort.setSortOrder(sortOrder);
	}
	
	Reference ref = new Reference();
	ArrayList list = ref.getAutoAssignments(sort);

	int size = list.size();
	ArrayList refList = new ArrayList();
	
	for (int i = 0; i < size; i++) {
		AutoAssignmentData data = (AutoAssignmentData)list.get(i);
		ReferenceCodeForm form = new ReferenceCodeForm();
		form.setId(String.valueOf(data.getAutoAssignmentId()));
		form.setCode(String.valueOf(data.getCriteriaId()));		
		form.setDescription(data.getFieldValue());
		form.setUnderwriter(data.getUserCode());
		refList.add(form);
	}
	LOGGER.info("populateAutoAssignmentList end");
	return (refList);
  }


  private ReferenceCodeListForm populateAutoAssignDetail(ReferenceCodeListForm form) throws UnderWriterException, IUMException {
	  
	LOGGER.info("populateAutoAssignDetail start");		
	Reference ref = new Reference();
	AutoAssignmentData data = ref.getAutoAssignmentDetail(Long.parseLong(form.getCode()));
	form.setId(String.valueOf(data.getAutoAssignmentId()));
	form.setCode(String.valueOf(data.getCriteriaId()));
	form.setDescription(data.getFieldValue());
	form.setUnderwriter(data.getUserCode());
	
	LOGGER.info("populateAutoAssignDetail end");
	return (form);
  }//populateAutoAssignDetail
      
  
  private ArrayList populateAutoAssignmentHeader() throws UnderWriterException, IUMException {
	
	LOGGER.info("populateAutoAssignmentHeader start");
	ArrayList header = new ArrayList();
	header.add("CRITERIA CODE".toUpperCase());
	header.add("CRITERIA FIELD VALUE".toUpperCase());
	header.add("UNDERWRITER".toUpperCase());
	
	LOGGER.info("populateAutoAssignmentHeader end");
	return (header);
  }//populateAutoAssignmentHeader


  private ArrayList populateAccessList() throws UnderWriterException, IUMException {
	
	  LOGGER.info("populateAccessList start");
	Reference ref = new Reference();
	ArrayList list = ref.getAccessList();

	int size = list.size();
	ArrayList refList = new ArrayList();
	
	for (int i = 0; i < size; i++) {
		AccessData data = (AccessData)list.get(i);
		ReferenceCodeForm form = new ReferenceCodeForm();
		form.setCode(String.valueOf(data.getAccessId()));
		form.setDescription(data.getAccessDesc());
		refList.add(form);
	}
	
	LOGGER.info("populateAccessList end");
	return (refList);
  }//populateAccessList


  private ReferenceCodeListForm populateAccessDetail(ReferenceCodeListForm form) throws UnderWriterException, IUMException {
	
	LOGGER.info("populateAccessDetail start");
	
	Reference ref = new Reference();
	AccessData data = ref.getAccessDetail(Long.parseLong(form.getCode()));
	form.setCode(String.valueOf(data.getAccessId()));
	form.setDescription(data.getAccessDesc());
	LOGGER.info("populateAccessDetail end");
	
	return (form);
  }
  

  private ArrayList populatePageList() throws UnderWriterException, IUMException {
	
	LOGGER.info("populatePageList start");
	Reference ref = new Reference();
	ArrayList list = ref.getPageList();

	int size = list.size();
	ArrayList refList = new ArrayList();
	
	for (int i = 0; i < size; i++) {
		PageData data = (PageData)list.get(i);
		ReferenceCodeForm form = new ReferenceCodeForm();
		form.setCode(String.valueOf(data.getPageId()));
		form.setDescription(data.getPageDesc());
		refList.add(form);
	}
	
	LOGGER.info("populatePageList end");
	return (refList);
  }//populateAccessList


  private ReferenceCodeListForm populatePageDetail(ReferenceCodeListForm form) throws UnderWriterException, IUMException {
	
	LOGGER.info("populatePageDetail start");
	Reference ref = new Reference();
	PageData data = ref.getPageDetail(Long.parseLong(form.getCode()));
	form.setCode(String.valueOf(data.getPageId()));
	form.setDescription(data.getPageDesc());
	LOGGER.info("populatePageDetail end");
	return (form);
  }//populatePageDetail

  /* What this method does:
   * 	All occurences of the 'target' string in the bigger 'template' string
   *  	is replaced by the 'replace' string.
   * Added by BJ Taduran June 04, 2007 */
  private String replaceAll(String template, String target, String replace){
	  
	  LOGGER.info("replaceAll start");
	 String subTstart = "";
	 String subTend = "";
	 while(template.indexOf(target)>=0){
		 subTstart = template.substring(0,template.indexOf(target));
		 subTend = template.substring(template.indexOf(target));
		 template = subTstart + replace + subTend.substring(1);
	 }
	 String newTemplate = template;
	 LOGGER.info("replaceAll end");
	 return newTemplate;
  }

}

