package com.slocpi.ium.ui.action;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.ResourceBundle;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.slocpi.ium.data.AssessmentRequestData;
import com.slocpi.ium.data.RequestFilterData;
import com.slocpi.ium.data.UserData;
import com.slocpi.ium.data.UserLoadData;
import com.slocpi.ium.data.UserProfileData;
import com.slocpi.ium.ui.form.AssessmentRequestForm;
import com.slocpi.ium.ui.form.RequestFilterForm;
import com.slocpi.ium.ui.form.UserLoadForm;
import com.slocpi.ium.ui.form.UserLoadListForm;
import com.slocpi.ium.ui.util.Pagination;
import com.slocpi.ium.ui.util.StateHandler;
import com.slocpi.ium.underwriter.AssessmentRequest;
import com.slocpi.ium.underwriter.Underwriter;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.DateHelper;
import com.slocpi.ium.util.IUMConstants;
import com.slocpi.ium.util.ValueConverter;

/**
 * @TODO Class Description GetAssessmentRequestListAction
 * @author: Ma. Theresa Vicencio
 * @version: 1.0
 */
public class ViewAssessmentRequestListAction extends IUMAction {

	private static final Logger LOGGER = LoggerFactory.getLogger(ViewAssessmentRequestListAction.class);


	private RequestFilterData setRequestFilterData(
			RequestFilterForm reqFilterForm) {
		
		LOGGER.info("setRequestFilterData start");
		RequestFilterData reqFilterData = new RequestFilterData();

		if (reqFilterForm.getRefNo() != null) {
			reqFilterData.setReferenceNumber(reqFilterForm.getRefNo()
					.toUpperCase());
		}

		reqFilterData.setAgent(reqFilterForm.getAgent());

		if (reqFilterForm.getClientNumber() != null) {
			reqFilterData.setClientNumber(reqFilterForm.getClientNumber()
					.toUpperCase());
		}

		reqFilterData.setLob(reqFilterForm.getLob());
		reqFilterData.setStatus(reqFilterForm.getStatus());
		reqFilterData.setAssignedTo(reqFilterForm.getAssignedTo());
		reqFilterData.setRequirementCode(reqFilterForm.getRequirementCode());
		reqFilterData.setNameOfInsured(reqFilterForm.getNameOfInsured());
		reqFilterData.setBranch(reqFilterForm.getBranch());
		reqFilterData.setApplicationReceivedDate(DateHelper.parse(reqFilterForm
				.getApplicationReceivedDate(), "ddMMMyyyy"));
		reqFilterData.setLocation(reqFilterForm.getLocation());
		reqFilterData.setCoverageAmount(ValueConverter
				.convertCurrency(reqFilterForm.getCoverageAmount()));
		reqFilterData.setSortOrder(reqFilterForm.getSortOrder());

		reqFilterData.setColumns(reqFilterForm.getColumns());

		reqFilterData.setRequirementCode(reqFilterForm.getRequirementCode());
		
		LOGGER.info("setRequestFilterData end");
		return reqFilterData;
	}

	private AssessmentRequestForm changeToAssessmentRequestForm(
			AssessmentRequestData data) {
		
		LOGGER.info("changeToAssessmentRequestForm start");
		
		AssessmentRequestForm frm = new AssessmentRequestForm();

		frm.setRefNo(data.getReferenceNumber());
		frm.setPolicySuffix(data.getPolicySuffix());
		frm.setLob(data.getLob().getLOBCode());
		frm.setAmountCovered(String.valueOf(data.getAmountCovered()));
		frm.setPremium(String.valueOf(data.getPremium()));
		frm.setInsuredName(data.getInsured().getLastName() + ", "
				+ data.getInsured().getGivenName());
		frm.setBranchCode(data.getBranch().getOfficeId());
		frm.setBranchName(data.getBranch().getOfficeName());
		frm.setAgentName(data.getAgent().getLastName() + ", "
				+ data.getAgent().getFirstName());
		frm.setRequestStatus(data.getStatus().getStatusDesc());

		String assignedToName = data.getAssignedTo().getLastName();
		if (data.getAssignedTo().getFirstName() != null
				|| (data.getAssignedTo().getFirstName() != null && data
						.getAssignedTo().getFirstName().equals(""))) {
			assignedToName = assignedToName + ", "
					+ data.getAssignedTo().getFirstName();
		}
		
		frm.setAssignedTo(assignedToName);
		frm.setLocation(data.getFolderLocation().getLastName() + ", "
				+ data.getFolderLocation().getFirstName());
		frm.setAgentName(data.getAgent().getLastName() + ", "
				+ data.getAgent().getFirstName());
		frm.setReceivedDate((DateHelper.format(data
				.getApplicationReceivedDate(), "ddMMMyyyy")).toUpperCase());
		
		LOGGER.info("changeToAssessmentRequestForm end");
		return frm;
	}


	public ActionForward handleAction(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		
	    LOGGER.info("handleAction start");
		String page = "";
		
		final Date start = new Date();
		Date end;
		long elapse;
		
		try {

			StateHandler sessionHandler = new StateHandler();
			UserData userData = sessionHandler.getUserData(request);
			
			UserProfileData userPref = userData.getProfile();
			
			String role = userPref.getRole();

			AssessmentRequest assessmentReq = new AssessmentRequest();

			RequestFilterForm reqFilterForm = (RequestFilterForm) form;
			RequestFilterData reqFilterData = new RequestFilterData();

			if (request.getAttribute("policyNum") != null
					&& !request.getAttribute("policyNum").equals("")) {
				
				reqFilterForm.setRefNo(request.getAttribute("policyNum") + "");
				reqFilterForm.setAssignedTo("");
				reqFilterForm.setStartSearch(IUMConstants.YES);
			}

			if (reqFilterForm == null || reqFilterForm.getAssignedTo() == null) {

				reqFilterForm = new RequestFilterForm();
				if (role == null) {
					
					reqFilterData.setAssignedTo(userPref.getUserId());
					reqFilterForm.setAssignedTo(userPref.getUserId());
					
				} else if (role.equals(IUMConstants.ROLES_NB_SUPERVISOR)) {
					
					reqFilterData.setStatus(String.valueOf(IUMConstants.STATUS_NB_REVIEW_ACTION));
					reqFilterForm.setStatus(String.valueOf(IUMConstants.STATUS_NB_REVIEW_ACTION));
					reqFilterForm.setAssignedTo("");
					
				} else if (role.equals(IUMConstants.ROLES_UNDERWRITER_SUPERVISOR)) {
					
					reqFilterData.setStatus(String.valueOf(IUMConstants.STATUS_FOR_ASSESSMENT));
					reqFilterForm.setStatus(String.valueOf(IUMConstants.STATUS_FOR_ASSESSMENT));
					reqFilterForm.setAssignedTo("");
					
				} else if (role.equals(String.valueOf(IUMConstants.ROLES_MEDICAL_CONSULTANT))) {
					
					reqFilterData.setStatus(String.valueOf(IUMConstants.STATUS_UNDERGOING_ASSESSMENT));
					reqFilterForm.setStatus(String.valueOf(IUMConstants.STATUS_UNDERGOING_ASSESSMENT));
					reqFilterForm.setAssignedTo("");
					
				} else {
					
					reqFilterData.setAssignedTo(userPref.getUserId());
					reqFilterForm.setAssignedTo(userPref.getUserId());
				}

				reqFilterForm.setColumns(IUMConstants.SORT_BY_REFERENCE_NUM);
				reqFilterForm.setSortOrder(IUMConstants.ASCENDING);
				
			} else {
				reqFilterData = setRequestFilterData(reqFilterForm);
			}
			
			ArrayList result = new ArrayList();
			result = assessmentReq.getRequestList(reqFilterData);
			ArrayList assessmentList = new ArrayList();

			for (int i = 0; i < result.size(); i++) {
				assessmentList.add(
						changeToAssessmentRequestForm((AssessmentRequestData) result.get(i)));
			}

			int recPerPage = (userPref.getRecordsPerView() != 0) ? userPref.getRecordsPerView() : 10;

			Pagination pgn = new Pagination(assessmentList, recPerPage);

			reqFilterForm.setPage(pgn.getPage(reqFilterForm.getPageNo()));

			if (result.size() == 1
					&& (reqFilterForm.getStartSearch() != null && reqFilterForm.getStartSearch().equals(IUMConstants.YES))) {

				reqFilterForm.setRefNo(((AssessmentRequestData) result.get(0)).getReferenceNumber());
				page = "reqKOMedLabDetailPage";
				request.setAttribute("firstListReqRedirectFlag", "1");

				String global_session_id = "1";
				if (request.getSession().getAttribute("global_session_id") != null) {

					global_session_id = String.valueOf(Long.parseLong(request
							.getSession().getAttribute("global_session_id").toString()) + 1);
				}
				
				request.getSession().setAttribute("assessmentReqListResult_"+reqFilterForm.getRefNo(),null);
				
				if(reqFilterForm.getRefNo()!=null){
					
					request.getSession().setAttribute("assessmentReqListResult_"+reqFilterForm.getRefNo(),result);
				}
				
				request.getSession().setAttribute("global_session_id", global_session_id);
				String session_id = request.getSession().getAttribute("global_session_id").toString();
				request.setAttribute("session_id", session_id);
				
			} else {			

				
				ResourceBundle rb = ResourceBundle.getBundle(IUMConstants.IUM_WMS_CONFIG);
				String contextPath = rb.getString("wms_context_path");

				if (request.getContextPath().equalsIgnoreCase(contextPath)) {
					
					page = "closeBrowser";
				} else {
					
					UserLoadListForm userLoadListForm = ListUnderwriterLoad();
					request.setAttribute("userLoadListForm", userLoadListForm);
					page = "listAssessmentRequestsPage";
				}
			}

			request.setAttribute("requestFilter", reqFilterForm);

		} catch (Exception e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			ActionErrors errors = new ActionErrors();
			errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.system.exception", e.getMessage()));
			saveErrors(request, errors);
			return (mapping.findForward("errorPage"));
		}

		LOGGER.info("handleAction end");
		LOGGER.debug("PAGE FORWARD-->" + page);
		return (mapping.findForward(page));
	}

	/**
	 * ListUnderWriterLoad and dataForm methods came from
	 * ListUnderwriterLoadAction which is now deprecated because of the change
	 * from iFrame to Div of the 'Underwriter Load' table on requests.jsp
	 * 
	 * @return UserLoadListForm
	 * @throws Exception
	 * @author BJ "Bujo" Taduran
	 */

	public UserLoadListForm ListUnderwriterLoad() throws Exception {
		
		LOGGER.info("ListUnderwriterLoad start");
		
		Underwriter uw = new Underwriter();
		ArrayList roles = new ArrayList();
		roles.add(IUMConstants.ROLES_UNDERWRITER);
		roles.add(IUMConstants.ROLES_UNDERWRITER_SUPERVISOR);
		ArrayList status = new ArrayList();
		status.add(new Long(IUMConstants.STATUS_FOR_ASSESSMENT));
		status.add(new Long(IUMConstants.STATUS_UNDERGOING_ASSESSMENT));

		ArrayList result = uw.getListUserLoadByStatus(roles, status);
		UserLoadListForm userLoadListForm = dataToForm(result);

		LOGGER.info("ListUnderwriterLoad end");
		return userLoadListForm;
	}

	private UserLoadListForm dataToForm(ArrayList result) {
		
		LOGGER.info("dataToForm start");
		
		UserLoadListForm listForm = new UserLoadListForm();
		ArrayList list = new ArrayList();
		for (int i = 0; i < result.size(); i++) {
			UserLoadForm frm = new UserLoadForm();
			UserLoadData data = (UserLoadData) result.get(i);
			frm.setUser_id(data.getUser_id());
			frm.setFirst_name(data.getFirst_name());
			frm.setLast_name(data.getLast_name());
			frm.setMiddle_name(data.getMiddle_name());
			frm.setTotalLoad(String.valueOf(data.getTotalLoad()));
			ArrayList loadCount = new ArrayList();
			HashMap hm = data.getLoadByStatus();
			Iterator it = hm.keySet().iterator();
			loadCount.add(hm.get(new Long(IUMConstants.STATUS_FOR_ASSESSMENT)));
			loadCount.add(hm.get(new Long(
					IUMConstants.STATUS_UNDERGOING_ASSESSMENT)));
			frm.setLoadByStatus(loadCount);

			list.add(frm);

		}
		listForm.setUserLoadList(list);
		LOGGER.info("dataToForm end");
		return listForm;
	}

}