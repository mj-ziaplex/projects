/*
 * Created on Feb 16, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.ui.action;

import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.slocpi.ium.data.ImpairmentData;
import com.slocpi.ium.data.UserData;
import com.slocpi.ium.data.UserProfileData;
import com.slocpi.ium.ui.form.ImpairmentForm;
import com.slocpi.ium.ui.util.StateHandler;
import com.slocpi.ium.underwriter.Impairment;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.DateHelper;

/**
 * @author Cris
 * 
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class MaintainImpairmentAction extends IUMAction {

	private static final Logger LOGGER = LoggerFactory.getLogger(MaintainImpairmentAction.class);
	
	public ActionForward handleAction(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		
		LOGGER.info("handleAction start");
		
		String page = "";
		String session_id = request.getParameter("session_id").toString();
		request.setAttribute("session_id", session_id);
		String savedSection = (request.getParameter("savedSection") != null) ? request
				.getParameter("savedSection").trim()
				: "";
		
		try {
			HttpSession session = request.getSession();
			if (session == null) {
				LOGGER.debug("request.getSession() is null");
			} else {
				LOGGER.debug("request.getSession() is not null");
				session
						.setAttribute("savedSection_" + session_id,
								savedSection);
				String temp = (String) session.getAttribute("savedSection_"
						+ session_id);
				String temp2 = (temp != null) ? temp.trim()
						: "session object savedSection is not saved";
			}
		} catch (Exception e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
		}

		StateHandler sh = new StateHandler();
		UserData ud = sh.getUserData(request);
		UserProfileData userProfile = ud.getProfile();
		String userId = userProfile.getUserId();

		ImpairmentForm impForm = (ImpairmentForm) form;
		String id = impForm.getImpairmentToBeEdited();
		String clientType = request.getParameter("clientType");

		impForm.setImpairmentId(id);

		
		maintainImpairmentRecord(impForm, clientType, userId);
		
		page = "cdsDetailPage";
		
		LOGGER.info("handleAction end");
		LOGGER.debug("PAGE FORWARD-->" + page);
		return mapping.findForward(page);
	}

	private void maintainImpairmentRecord(ImpairmentForm impairmentForm,
			String clientType, String userId) { 
		
		LOGGER.info("maintainImpairmentRecord start");
		try {
			
			ImpairmentData data = new ImpairmentData();
			data.setReferenceNumber(impairmentForm.getRefNo());
			data.setImpairmentId(Long.parseLong(impairmentForm
					.getImpairmentId()));
			data.setRelationship(impairmentForm.getRelationship());
			data.setImpairmentOrigin(impairmentForm.getImpairmentOrigin());
			data.setImpairmentDate(DateHelper.sqlDate(DateHelper.parse(
					impairmentForm.getImpairmentDate(), "ddMMMyyyy")));
			
			double heightInFeet;
			double heightInInches;
			if (impairmentForm.getHeightInFeet() == null
					|| impairmentForm.getHeightInFeet().trim().equals("")
					|| impairmentForm.getHeightInFeet().equals("undefined")) {
				heightInFeet = 0;
			} else {
				heightInFeet = Double.parseDouble(impairmentForm
						.getHeightInFeet());
			}

			if (impairmentForm.getHeightInInches() == null
					|| impairmentForm.getHeightInInches().trim().equals("")
					|| impairmentForm.getHeightInInches().equals("undefined")) {
				heightInInches = 0;
			} else {
				heightInInches = Double.parseDouble(impairmentForm
						.getHeightInInches());
			}

			data.setHeightInFeet(heightInFeet);
			data.setHeightInInches(heightInInches);

			double weight;
			if (impairmentForm.getWeight() == null
					|| impairmentForm.getWeight().equals("")) {
				weight = 0;
			} else {
				weight = Double.parseDouble(impairmentForm.getWeight());
			}
			data.setWeight(weight);

			data.setBloodPressure(impairmentForm.getBloodPressure());
			data.setConfirmation(impairmentForm.getConfirm());
			data.setUpdatedBy(userId);
			data.setUpdateDate(new Date());
			data.setImpairmentCode(impairmentForm.getCode());
			data.setActionCode(impairmentForm.getActionCode());
			Impairment impairment = new Impairment();
			impairment.updateImpairment(data, clientType);

		} catch (Exception e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
		}
		LOGGER.info("maintainImpairmentRecord end");
	}


}
