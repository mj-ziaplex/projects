/*
 * Created on Jan 22, 2004
 * package name = com.slocpi.ium.ui.action
 * file name    = ListUnderwriterLoadAction.java
 */
package com.slocpi.ium.ui.action;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.slocpi.ium.data.UserLoadData;
import com.slocpi.ium.ui.form.UserLoadForm;
import com.slocpi.ium.ui.form.UserLoadListForm;
import com.slocpi.ium.underwriter.Underwriter;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.IUMConstants;


/**
 * TODO Class Description of ListUnderwriterLoadAction.java
 * @author Engel
 * 
 */
public class ListUnderwriterLoadAction extends IUMAction {
	private static final Logger LOGGER = LoggerFactory.getLogger(ListUnderwriterLoadAction.class);
	/* (non-Javadoc)
	 * @see com.slocpi.ium.ui.action.IUMAction#handleAction(org.apache.struts.action.ActionMapping, org.apache.struts.action.ActionForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	public ActionForward handleAction(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		LOGGER.info("handleAction start");
		String page = "";
		try{	
			page = "listUnderwriterLoadPage";
			Underwriter uw = new Underwriter();
			ArrayList roles = new ArrayList();
				roles.add(IUMConstants.ROLES_UNDERWRITER);
				roles.add(IUMConstants.ROLES_UNDERWRITER_SUPERVISOR);
			ArrayList status = new ArrayList();
				status.add(new Long(IUMConstants.STATUS_FOR_ASSESSMENT) );
				status.add(new Long(IUMConstants.STATUS_UNDERGOING_ASSESSMENT) );
				
			ArrayList result = uw.getListUserLoadByStatus(roles, status);
			UserLoadListForm userLoadListForm = dataToForm(result);
			request.setAttribute("userLoadListForm",userLoadListForm);
		}catch (Exception e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			ActionErrors errors = new ActionErrors();
			errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.system.exception", e.getMessage()));
			saveErrors(request, errors); 
			return (mapping.findForward("errorPage")); 
		}
		LOGGER.info("handleAction end");
		LOGGER.debug("PAGE FORWARD-->" + page);
		return (mapping.findForward(page));
	}
	
	
	private UserLoadListForm dataToForm(ArrayList result){
		
		LOGGER.info("dataToForm start");
		UserLoadListForm listForm = new UserLoadListForm();
		ArrayList list = new ArrayList();
		for (int i=0; i<result.size(); i++ ){
			UserLoadForm frm = new UserLoadForm();
			UserLoadData data  = (UserLoadData)result.get(i);
			frm.setUser_id( data.getUser_id() );
			frm.setFirst_name( data.getFirst_name() );
			frm.setLast_name( data.getLast_name() );
			frm.setMiddle_name( data.getMiddle_name() );
			frm.setTotalLoad( String.valueOf(data.getTotalLoad()) );
			ArrayList loadCount = new ArrayList();
			HashMap hm = data.getLoadByStatus();
			Iterator it = hm.keySet().iterator();
				loadCount.add( hm.get(new Long(IUMConstants.STATUS_FOR_ASSESSMENT)) );
				loadCount.add( hm.get(new Long(IUMConstants.STATUS_UNDERGOING_ASSESSMENT)) );			
			frm.setLoadByStatus(loadCount);
			list.add(frm);
			
		}
		listForm.setUserLoadList(list);
		LOGGER.info("dataToForm end");
		return listForm;	
	}

}
