/*
 * Created on Mar 4, 2004
 * package name = com.slocpi.ium.ui.action
 * file name    = ViewSLOAction.java
 */
package com.slocpi.ium.ui.action;

import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.slocpi.ium.data.SunLifeOfficeData;
import com.slocpi.ium.data.UserData;
import com.slocpi.ium.data.UserProfileData;
import com.slocpi.ium.ui.form.SLOForm;
import com.slocpi.ium.ui.util.Page;
import com.slocpi.ium.ui.util.Pagination;
import com.slocpi.ium.ui.util.StateHandler;
import com.slocpi.ium.underwriter.Reference;

/**
 * This class is responsible for displaying the IUM's list of offices.
 * This class will display the list and allow the viewing of a specific office.
 * @author Engel
 * 
 */
public class ViewSLOAction extends IUMAction {

	private static final Logger LOGGER = LoggerFactory.getLogger(ViewSLOAction.class);
	/* (non-Javadoc)
	 * @see com.slocpi.ium.ui.action.IUMAction#handleAction(org.apache.struts.action.ActionMapping, org.apache.struts.action.ActionForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	public ActionForward handleAction(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response)
		throws Exception {

		LOGGER.info("handleAction start");    
		String page="viewDetails";
		try{
		
		StateHandler sessionHandler = new StateHandler();
		UserData userData = sessionHandler.getUserData(request);
		UserProfileData userPref = userData.getProfile();
		String role = userPref.getRole();
						
		SLOForm slof = (SLOForm)form;
		String pageNumber = slof.getPageNumber();
		if(pageNumber==null || pageNumber.equals(""))
		   pageNumber="1";
		
		Reference refBO = new Reference();
		
		if( slof!=null&&slof.getOfficeId()!=null&&!slof.getOfficeId().equals("") ){
			
			SunLifeOfficeData data = refBO.getSLODetail(slof.getOfficeId());
			if(data.getOfficeId()!=null&&!data.getOfficeId().equals("")){
				LOGGER.debug("<<<<****before dataToForm****>>>>");
				slof = dataToForm(data);	
			}else{
				LOGGER.debug("data="+data);
				LOGGER.debug("officeId="+slof.getOfficeId());
			}
		}
		slof.setPageNumber(pageNumber);
		
		ArrayList list = refBO.getSLOs();
		ArrayList arrForm = new ArrayList();
		for(int i=0; i<list.size(); i++){
			SunLifeOfficeData data = (SunLifeOfficeData)list.get(i);
			SLOForm frm = dataToForm(data);
			arrForm.add(frm); 
		}
	
		int recPerPage = (userPref.getRecordsPerView()!=0)?userPref.getRecordsPerView():10;
			
		Pagination pgn = new Pagination(arrForm,recPerPage);
		
		Page pageObj = pgn.getPage(Integer.parseInt(pageNumber));
		
		request.setAttribute("sloform", slof);
		request.setAttribute("pageRecord", pageObj);
		}catch (Exception e) {
			ActionErrors errors = new ActionErrors();
			errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.system.exception", e.getMessage()));
			saveErrors(request, errors); 
			page= "errorPage";
		}						
		
		LOGGER.info("handleAction end");
		LOGGER.debug("PAGE FORWARD-->" + page);
		
		return (mapping.findForward(page));
	}


	private SLOForm dataToForm(SunLifeOfficeData data){
		
		LOGGER.info("dataToForm start");
		SLOForm slo = null;
		if(data!=null){
			slo = new SLOForm();
			
			slo.setOfficeId(data.getOfficeId());
			slo.setOfficeName(data.getOfficeName());
			slo.setOfficeType(data.getOfficeType());
			slo.setAddr1(data.getAddr1());
			slo.setAddr2(data.getAddr2());
			slo.setAddr3(data.getAddr3());
			slo.setCity(data.getCity());
			slo.setProvince(data.getProvince());
			slo.setCountry(data.getCountry());
			slo.setZipCode(data.getZipCode());
			slo.setContactNumber(data.getContactNumber());
			slo.setFaxNumber(data.getFaxNumber());
		}
		LOGGER.info("dataToForm end");
		return slo;
	}

}
