package com.slocpi.ium.ui.action;

import java.io.IOException;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Iterator;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import com.slocpi.ium.IUMException;
import com.slocpi.ium.data.CDSMortalityRatingData;
import com.slocpi.ium.data.CDSSummaryPolicyCoverageData;
import com.slocpi.ium.data.ClientData;
import com.slocpi.ium.data.KickOutMessageData;
import com.slocpi.ium.data.PolicyDetailData;
import com.slocpi.ium.data.UWAssessmentData;
import com.slocpi.ium.data.UserData;
import com.slocpi.ium.data.UserProfileData;
import com.slocpi.ium.service.ingenium.IngeniumGateway;
import com.slocpi.ium.ui.form.CDSDetailForm;
import com.slocpi.ium.ui.form.CDSMortalityRatingForm;
import com.slocpi.ium.ui.form.CDSSummaryForm;
import com.slocpi.ium.ui.form.FormLoader;
import com.slocpi.ium.ui.form.KOReasonsForm;
import com.slocpi.ium.ui.form.PolicyCoverageForm;
import com.slocpi.ium.ui.form.RequestFilterForm;
import com.slocpi.ium.ui.form.UWAnalysisForm;
import com.slocpi.ium.ui.util.Page;
import com.slocpi.ium.ui.util.Pagination;
import com.slocpi.ium.ui.util.StateHandler;
import com.slocpi.ium.underwriter.AssessmentRequest;
import com.slocpi.ium.underwriter.CDS;
import com.slocpi.ium.underwriter.KickOutMessage;
import com.slocpi.ium.underwriter.UnderWriterException;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.DateHelper;
import com.slocpi.ium.util.IUMConstants;
import com.slocpi.ium.util.ValueConverter;

/**
 * @TODO Class Description ViewCDSDetailAction
 * @author: Ma. Theresa Vicencio
 * @version: 1.0
 */
public class ViewCDSDetailAction extends IUMAction {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ViewCDSDetailAction.class);

	private static final String YES = "Y";
	private static final String NO = "N";
	private static final String CLIENT_TYPE_OWNER = "OWNER";
	private static final String CLIENT_TYPE_INSURED = "INSURED";

	private HttpServletRequest request;

	/**
	 * @TODO method description for execute
	 * 
	 * @param mapping mapping of request to an instance of this class
	 * @param form object
	 * @param request request object
	 * @param response response object
	 * 
	 * @exception IOException if an input/output error occurs
	 * @exception ServletException if a servlet exception occurs
	 * @return ActionForward as defined in the mapping parameter.
	 */
	public ActionForward handleAction(
			ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response)
	 throws Exception {
		
		LOGGER.info("handleAction start");
		HttpSession session = request.getSession();
		
		String page = "";

		String globalSessionId = "1";
		String sessionId = noNull((String) request.getAttribute("session_id"));
		String globelSessionIdTmp = noNull((String) request.getSession().getAttribute("global_session_id"));
		
		if (!"".equals(globelSessionIdTmp)) {
			globalSessionId = String.valueOf(Long.parseLong(globelSessionIdTmp) + 1);
		}
		
		sessionId = globalSessionId;
		request.getSession().setAttribute("global_session_id", globalSessionId);
		request.setAttribute("session_id", sessionId);
		
		AssessmentRequest assessmentReq = new AssessmentRequest();
		RequestFilterForm reqFilterForm = (RequestFilterForm) request.getSession().getAttribute("reqFilterForm");
		
		if (reqFilterForm == null){
			reqFilterForm = new RequestFilterForm();
			reqFilterForm.setRefNo(noNull((String) request.getAttribute("policyNum")));
			reqFilterForm.setAssignedTo("");
			reqFilterForm.setStartSearch(IUMConstants.YES);
		}
		
		final String tmpPol = noNull((String) request.getSession().getAttribute("policyNum"));
		if("".equals(reqFilterForm.getRefNo()) && !"".equals(tmpPol)){
			reqFilterForm.setRefNo(tmpPol);
		}

		if (!reqFilterForm.getClickedRefNo().equals("none")) {
			reqFilterForm.setRefNo(reqFilterForm.getClickedRefNo());
		}
		
		request.getSession().setAttribute("policyNum", reqFilterForm.getRefNo());

		try {
			
			StateHandler sh = new StateHandler();
			UserData ud = sh.getUserData(request);
			UserProfileData profile = ud.getProfile();
			
			final String role = noNull(profile.getRole());
			request.setAttribute("role", role);
			ActionErrors errors;
			
			CDSDetailForm cdsDetailForm = new CDSDetailForm();
			cdsDetailForm.setRefNo(reqFilterForm.getRefNo());

			String refNo = noNull(cdsDetailForm.getRefNo());
			String isMaintainRQ = noNull(request.getParameter("isMaintain"));
			String searchClientTypeRQ = noNull(request.getParameter("searchClientType"));
			
			Boolean refreshCDS = Boolean.valueOf(noNull((String) request.getAttribute("refreshCDS")));
			LOGGER.info("refreshCDS-->" +  refreshCDS);
			
			final String policySuffix = noNull((String) request.getAttribute("policySuffix"));
			if(refreshCDS != null && refreshCDS.equals(Boolean.TRUE)){
				refreshCds(refNo, policySuffix, request);
			}

			if((!isMaintainRQ.equals("") || !isMaintainRQ.equals("false"))
					&& !searchClientTypeRQ.equals("")){
				
				if (searchClientTypeRQ.equals(IUMConstants.CLIENT_TYPE_INSURED)) {
					retrieveInsuredClientData(cdsDetailForm);
				} else if (searchClientTypeRQ.equals(IUMConstants.CLIENT_TYPE_OWNER)) {
					retrieveOwnerClientData(cdsDetailForm);
				}
				cdsDetailForm.setClientToSearch(searchClientTypeRQ);
			} else {
				cdsDetailForm = (CDSDetailForm) FormLoader.loadAssessmentRequest(cdsDetailForm, request);
				
				session.setAttribute("CDSDetailForm_" + refNo, cdsDetailForm);
				setRequest(request);
			}
			
			
			cdsDetailForm.setListOfAnalysis(populateUWAnalysis(cdsDetailForm.getRefNo()));
			
			ArrayList koReasons = populateKOReasons(refNo);
			cdsDetailForm.setKoReasons(koReasons);
			session.setAttribute("detailForm_" + sessionId, cdsDetailForm);
			
			String iClientNo = cdsDetailForm.getInsuredClientNo();
			String oClientNo = cdsDetailForm.getOwnerClientNo();
			
			String clientTypeUW = request.getParameter("clientType");
			if (clientTypeUW == null) {
				
				clientTypeUW = cdsDetailForm.getInsuredClientType();
			}

			if ((clientTypeUW.equals(IUMConstants.CLIENT_TYPE_INSURED))
					&& (iClientNo == null)) {
				
				errors = new ActionErrors();
				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.request.generic", "There is no existing insured client."));
				saveErrors(request, errors);
				
			} else if ((clientTypeUW.equals(IUMConstants.CLIENT_TYPE_OWNER))
					&& (oClientNo == null)) {
				
				errors = new ActionErrors();
				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.request.generic", "There is no existing owner client."));
				saveErrors(request, errors);
			}
			
			ArrayList impairments = new ArrayList();
			
			int recPerView = profile.getRecordsPerView();
			if (recPerView == 0) {
				recPerView = 1;
			}
			
			int pageNo = 1;
			String reqPageNum = request.getParameter("pageNo");
			if ((reqPageNum != null) && (!reqPageNum.equals(""))) {
				
				try {
					pageNo = Integer.parseInt(reqPageNum);
				} catch (NumberFormatException e) {
					LOGGER.error("ERROR ING GETTING PAGENO:" + CodeHelper.getStackTrace(e));
				}
			}
			
			Pagination pgn = new Pagination(impairments, recPerView);
			Page pg = pgn.getPage(pageNo);
			cdsDetailForm.setImpairments(pg.getList());
			
			CDSSummaryForm cdsSummaryForm = new CDSSummaryForm();
			
			if (cdsDetailForm.getSourceSystem() != null) {
				
				boolean hasOwner = false;
				if (oClientNo != null) {
					hasOwner = true;
				}

				request.setAttribute("hasOwner", String.valueOf(hasOwner));
				
				cdsSummaryForm = populateSummary(refNo, iClientNo);

				ArrayList policyCoverageList = populatePolicyCoverageList(refNo, iClientNo);
				cdsDetailForm.setPolicyCoverageList(policyCoverageList);

				ArrayList cdsMortalityRatingFormList = new ArrayList();
				
				if(iClientNo != null && oClientNo != null) {
					
					if (iClientNo.equals(oClientNo)) {
						
						CDSMortalityRatingForm owner_CDSMortalityRatingForm = new CDSMortalityRatingForm();
						owner_CDSMortalityRatingForm = populateMortalityRating(refNo, oClientNo);
						owner_CDSMortalityRatingForm.setClientType(CLIENT_TYPE_OWNER);
						cdsMortalityRatingFormList.add(owner_CDSMortalityRatingForm);
						owner_CDSMortalityRatingForm.setClientType(CLIENT_TYPE_INSURED);
						cdsMortalityRatingFormList.add(owner_CDSMortalityRatingForm);
						
					} else {
						
						CDSMortalityRatingForm insured_CDSMortalityRatingForm = new CDSMortalityRatingForm();
						insured_CDSMortalityRatingForm = populateMortalityRating(refNo, iClientNo);
						insured_CDSMortalityRatingForm.setClientType(CLIENT_TYPE_INSURED);
						cdsMortalityRatingFormList.add(insured_CDSMortalityRatingForm);

						CDSMortalityRatingForm owner_CDSMortalityRatingForm = new CDSMortalityRatingForm();
						owner_CDSMortalityRatingForm = populateMortalityRating(refNo, oClientNo);
						owner_CDSMortalityRatingForm.setClientType(CLIENT_TYPE_OWNER);
						cdsMortalityRatingFormList.add(owner_CDSMortalityRatingForm);
					}
				}
				
				cdsDetailForm.setMortalityRatingList(cdsMortalityRatingFormList);
								
				pgn = new Pagination(policyCoverageList, recPerView);
				pg = pgn.getPage(pageNo);
			}
			
			request.setAttribute("page", pg);
			request.setAttribute("pageNo", String.valueOf(pageNo));
			request.setAttribute("detailForm", cdsDetailForm);
			request.setAttribute("cdsSummaryForm", cdsSummaryForm);

			page = "cdsDetailPage";

			
			boolean isKoExceeded = false;
			try {
				
				if (request.getSession().getAttribute("isKoExceeded_" + refNo) != null) {
					
					Boolean isKoExceededB = (Boolean) request.getSession().getAttribute("isKoExceeded_" + refNo);
					isKoExceeded = isKoExceededB.booleanValue();
					
				} else {
					
					isKoExceeded = assessmentReq.isKOCountExceeded(refNo);
					request.getSession().setAttribute("isKoExceeded_" + refNo, Boolean.valueOf(isKoExceeded));
				}
			} catch (Exception e) {
				LOGGER.error("ERROR ING GETTING KOEXCEED" + CodeHelper.getStackTrace(e));
			}
			
			
			if (isKoExceeded && page.equals("cdsDetailPage")) {
				ActionMessages warning = new ActionMessages();
				warning.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("message.kOCountExceeded"));
				saveMessages(request, warning);
			}

			
		} catch (UnderWriterException e) {
			ActionErrors errors = new ActionErrors();
			errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.request.generic", e.getMessage()));
			saveErrors(request, errors);
			page = "cdsDetailPage";
			LOGGER.error(CodeHelper.getStackTrace(e));
		} catch (IUMException e) {
			ActionErrors errors = new ActionErrors();
			errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.system.exception", e.getMessage()));
			saveErrors(request, errors);
			page = "errorPage";
			page = "closeBrowser";
			LOGGER.error(CodeHelper.getStackTrace(e));
		} catch (Exception e) {
			ActionErrors errors = new ActionErrors();
			errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.system.exception", e.getMessage()));
			saveErrors(request, errors);
			page = "errorPage";
			page = "closeBrowser";
			LOGGER.error(CodeHelper.getStackTrace(e));
		}

		LOGGER.info("handleAction end");
		LOGGER.debug("PAGE FORWARD-->" + page);
		return (mapping.findForward(page));
	}
	

	// **************************************** //
	// *** METHODS WITH POJO Objects RETURN *** //
	// **************************************** //
	
	/**
	 * @deprecated use FormLoader.loadAssessmentRequest() method
	 */
	// Method to get mortality rating
	//
	// @param String refno
	// @param String clientNo
	private CDSMortalityRatingForm populateMortalityRating(
			final String refNo,
			final String clientNo) throws UnderWriterException, IUMException {
		
		LOGGER.info("populateMortalityRating start");
		CDS cds = new CDS();

		NumberFormat nf = NumberFormat.getInstance();
		nf.setMinimumFractionDigits(2);
		nf.setMaximumFractionDigits(2);

		CDSMortalityRatingData data = cds.getMortalityRating(refNo, clientNo);

		String height = String.valueOf(data.getHeightInFeet()) + "'" + String.valueOf(data.getHeightInInches()) + "\"";
		String weight = String.valueOf(data.getWeight());

		CDSMortalityRatingForm form = new CDSMortalityRatingForm();
		form.setHeight(height);
		form.setWeight(weight);
		form.setBasic(nf.format(data.getBasic()));
		form.setCCR(nf.format(data.getCCR()));
		form.setAPDB(nf.format(data.getAPDB()));
		form.setSmokingBasic(nf.format(data.getSmokingBasic()));
		form.setSmokingCCR(nf.format(data.getSmokingCCR()));
		form.setSmokingAPDB(nf.format(data.getSmokingAPDB()));
		form.setFamilyHistoryBasic(nf.format(data.getFamilyHistoryBasic()));
		form.setFamilyHistoryCCR(nf.format(data.getFamilyHistoryCCR()));
		form.setFamilyHistoryAPDB(nf.format(data.getFamilyHistoryAPDB()));
		form.setTotalAssestMortBasic(nf.format(data.getTotalAssestMortBasic()));
		form.setTotalAssestMortCCR(nf.format(data.getTotalAssestMortCCR()));
		form.setTotalAssestMortAPDB(nf.format(data.getTotalAssestMortAPDB()));

		LOGGER.info("populateMortalityRating end");
		return (form);
	}
	
	
	private CDSSummaryForm populateSummary(
			final String refNo,
			final String clientNo) throws UnderWriterException, IUMException {
		
		LOGGER.info("populateSummary start");
		CDS cds = new CDS();

		NumberFormat nf = NumberFormat.getInstance();
		nf.setMinimumFractionDigits(2);
		nf.setMaximumFractionDigits(2);

		CDSSummaryPolicyCoverageData data = 
				cds.getSummaryPolicy(refNo, clientNo);

		CDSSummaryForm form = new CDSSummaryForm();
		form.setAppliedForBr(nf.format(data.getAppliedForBr()));
		form.setAppliedForAd(nf.format(data.getAppliedForAd()));
		form.setPendingAmountBr(nf.format(data.getPendingAmountBr()));
		form.setPendingAmountAd(nf.format(data.getPendingAmountAd()));
		form.setExistingInForceBr(nf.format(data.getExistingInForceBr()));
		form.setExistingInForceAd(nf.format(data.getExistingInForceAd()));
		form.setExistingLapsedBr(nf.format(data.getExistingLapsedBr()));
		form.setExistingLapsedAd(nf.format(data.getExistingLapsedAd()));
		form.setTotalExistingSunLifeBr(nf.format(data.getTotalExistingSunLifeBr()));
		form.setTotalExistingSunLifeAd(nf.format(data.getTotalExistingSunLifeAd()));
		form.setTotalSunLifeBr(nf.format(data.getTotalSunLifeBr()));
		form.setTotalSunLifeAd(nf.format(data.getTotalSunLifeAd()));
		form.setOtherCompaniesBr(nf.format(data.getOtherCompaniesBr()));
		form.setOtherCompaniesAd(nf.format(data.getOtherCompaniesAd()));
		form.setTotalSunLifeOtherCompaniesBr(nf.format(data.getTotalSunLifeOtherCompaniesBr()));
		form.setTotalSunLifeOtherCompaniesAd(nf.format(data.getTotalSunLifeOtherCompaniesAd()));
		form.setTotalCCRCoverage(nf.format(data.getTotalCCRCoverage()));
		form.setTotalAPDBCoverage(nf.format(data.getTotalAPDBCoverage()));
		form.setTotalHIBCoverage(nf.format(data.getTotalHIBCoverage()));
		form.setTotalFBBMBCoverage(nf.format(data.getTotalFBBMBCoverage()));
		form.setTotalReinsuredAmount(nf.format(data.getTotalReinsuredAmount()));
		form.setTotalAdbReinsuredAmount(nf.format(data.getTotalAdbReinsuredAmount()));

		LOGGER.info("populateSummary end");
		return (form);
	}// populateSummary

	
	// ************************************* //
	// *** METHODS WITH ArrayList RETURN *** //
	// ************************************* //
	
	// Method to get policyCoverage list form
	//
	// @param String refno
	// @param String clientNo
	private ArrayList populatePolicyCoverageList(
			final String refNo,
			final String clientNo) throws UnderWriterException, IUMException {
		
		LOGGER.info("populatePolicyCoverageList start");
		CDS cds = new CDS();

		ArrayList policyCoverageList = new ArrayList();
		ArrayList policyList = cds.getPolicyCoverageList(refNo, clientNo);
		int size = policyList.size();

		NumberFormat nf = NumberFormat.getInstance();
		nf.setMinimumFractionDigits(2);
		nf.setMaximumFractionDigits(2);

		for (int i = 0; i < size; i++) {
			PolicyDetailData policyData = (PolicyDetailData) policyList.get(i);

			String smoker = "";
			if (policyData.isSmoker()) {
				smoker = YES;
			} else {
				smoker = NO;
			}

			String medical = "";
			if (policyData.isMedical()) {
				medical = YES;
			} else {
				medical = NO;
			}

			PolicyCoverageForm polCovForm = new PolicyCoverageForm();
			polCovForm.setPolicyNo(noNull(policyData.getPolicyNumber()));
			polCovForm.setPolicySuffix(noNull(policyData.getPolicySuffix()));
			polCovForm.setCoverageNo(String.valueOf(policyData.getCoverageNumber()));
			polCovForm.setRelationship(noNull(policyData.getRelationship()));
			polCovForm.setIssueDate((DateHelper.format(policyData.getIssueDate(), "ddMMMyyyy")).toUpperCase());
			polCovForm.setCoverageStatus(noNull(policyData.getCoverageStatus()));
			polCovForm.setSmokerInd(smoker);
			polCovForm.setPlanCode(noNull(policyData.getPlanCode()));
			polCovForm.setMedicalInd(medical);
			polCovForm.setFaceAmount(nf.format(policyData.getFaceAmount()));
			polCovForm.setDecision(noNull(policyData.getDecision()));
			polCovForm.setADBFaceAmount(nf.format(policyData.getADBFaceAmount()));
			polCovForm.setADMultiplier(String.valueOf(policyData.getADMultiplier()));
			polCovForm.setWPMultiplier(String.valueOf(policyData.getWPMultiplier()));
			polCovForm.setReinsuredAmount(nf.format(policyData.getReinsuredAmount()));
			polCovForm.setRelCvgFePermAmt(nf.format(policyData.getRelCvgFePermAmt()));
			polCovForm.setRelCvgFeUpremAmt(nf.format(policyData.getRelCvgFeUpremAmt()));
			polCovForm.setRelCvgMeFct(nf.format(policyData.getRelCvgMeFct()));
			polCovForm.setAdbReinsuredAmount(nf.format(policyData.getAdbReinsuredAmount()));
			polCovForm.setReinstatementDate((DateHelper.format(policyData.getReinstatementDate(), "ddMMMyyyy")).toUpperCase());
			polCovForm.setReinsuranceType(noNull(policyData.getReinsuranceType()));
			policyCoverageList.add(polCovForm);
		}

		LOGGER.info("populatePolicyCoverageList end");
		return (policyCoverageList);
	}// populatePolicyCoverageList

	// Method to fetch list of UWAnalysisForm details based on referenceNo
	//
	// @param String refNo
	private ArrayList populateUWAnalysis(final String referenceNumber) throws IUMException {

		LOGGER.info("populateUWAnalysis start");
		ArrayList listOfAnalysis = new ArrayList();
		AssessmentRequest ar = new AssessmentRequest();
		ArrayList list = ar.retrieveUWAnalysis(referenceNumber);

		for (int i = 0; i < list.size(); i++) {

			UWAnalysisForm analysis = new UWAnalysisForm();
			UWAssessmentData data = (UWAssessmentData) list.get(i);
			analysis.setReferenceNumber(data.getReferenceNumber());
			analysis.setAnalysis(ValueConverter.newLineToHTML(data.getAnalysis()));
			analysis.setPostedBy(data.getPostedBy());
			analysis.setPostedDate(DateHelper.format(data.getPostedDate(),"ddMMMyyyy HH:mm").toUpperCase());
			listOfAnalysis.add(analysis);
		}
		LOGGER.info("populateUWAnalysis end");
		return listOfAnalysis;
	}
	
	// Method to get list of KickOutMessage through refNo
	//
	// @param String refno
	private ArrayList populateKOReasons(String refNo) throws Exception {
		
		LOGGER.info("populateKOReasons start");
		ArrayList koReasons = new ArrayList();

		KickOutMessage ko = new KickOutMessage();
		ArrayList reqList = ko.getMessages(refNo);

		if (reqList != null && !reqList.isEmpty()) {
			
			Iterator it = reqList.iterator();
			
			while (it.hasNext()) {

				KickOutMessageData koData = (KickOutMessageData) it.next();
				KOReasonsForm koForm = new KOReasonsForm();
				koForm.setSeq(String.valueOf(koData.getSequenceNumber()));
				koForm.setMessageText(koData.getMessageText());
				koForm.setClientNo(koData.getClientId());
				koForm.setFailResponse(koData.getFailResponse());
				koReasons.add(koForm);
			}
		}
		
		LOGGER.info("populateKOReasons end");
		return (koReasons);
	}// populateKOReasons
	
	
	// ********************************** //
	// *** METHODS WITH STRING RETURN *** //
	// ********************************** //
	
	// Method to convert null to a blank String or trim if not null
	//
	// @param String str
	public String noNull(final String str){
		
		
		String toRet = "";
		
		if(str == null){
			return toRet;
		} else {
			toRet = str.trim();
		}
		
		return toRet;
	}
	
	
	// ******************************** //
	// *** METHODS WITH VOID RETURN *** //
	// ******************************** //
	
	// Get HttpServletRequest for ViewCDSDetailAction
	public HttpServletRequest getRequest() {
		
		return request;
	}

	// Set HttpServletRequest for ViewCDSDetailAction
	//
	// @param HttpServletRequest request
	public void setRequest(HttpServletRequest request) {
		
		this.request = request;
	}
	
	// Method to get insured client data
	//
	// @param CDSDetailForm arf
	private void retrieveInsuredClientData(final CDSDetailForm arf) throws IUMException {

		LOGGER.info("retrieveInsuredClientData start");
		arf.setInsuredClientNo(arf.getInsuredClientNo().toUpperCase());
		AssessmentRequest ar = new AssessmentRequest();
		ClientData cdata = ar.getClientData(arf.getInsuredClientNo());

		if (cdata != null && cdata.getGivenName() != null
				&& !cdata.getGivenName().equals("")) {

			if (cdata.getAge() == 0) {
				arf.setInsuredAge("");
			} else {
				arf.setInsuredAge(String.valueOf(cdata.getAge()));
			}

			
			if (cdata.getBirthDate() != null) {
				arf.setInsuredBirthDate((DateHelper.format(
						cdata.getBirthDate(), "ddMMMyyyy")).toUpperCase());
			} else {
				arf.setInsuredBirthDate("");
			}
			
			if (cdata.getGivenName() != null) {
				arf.setInsuredFirstName(cdata.getGivenName());
			} else {
				arf.setInsuredFirstName("");
			}
			
			
			if (cdata.getLastName() != null) {
				arf.setInsuredLastName(cdata.getLastName());
			} else {
				arf.setInsuredLastName("");
			}
			
			
			if (cdata.getMiddleName() != null) {
				arf.setInsuredMiddleName(cdata.getMiddleName());
			} else {
				arf.setInsuredMiddleName("");
			}
			
			
			if (cdata.getTitle() != null) {
				arf.setInsuredTitle(cdata.getTitle());
			} else {
				arf.setInsuredTitle("");
			}
			
			
			if (cdata.getSex() != null) {
				arf.setInsuredSex(cdata.getSex());
			} else {
				arf.setInsuredSex("");
			}
			
			
			if (cdata.getSuffix() != null) {
				arf.setInsuredSuffix(cdata.getSuffix());
			} else {
				arf.setInsuredSuffix("");
			}
			arf.setInsuredOriginal(arf.getInsuredClientNo()
					+ arf.getInsuredFirstName() + arf.getInsuredLastName()
					+ arf.getInsuredMiddleName() + arf.getInsuredTitle()
					+ arf.getInsuredSuffix() + arf.getInsuredSex());

		} else {
			arf.setInsuredAge("");
			arf.setInsuredFirstName("");
			arf.setInsuredMiddleName("");
			arf.setInsuredLastName("");
			arf.setInsuredBirthDate("");
			arf.setInsuredSuffix("");
			arf.setInsuredTitle("");
			arf.setInsuredSex("");
			arf.setInsuredOriginal("");
		}
		
		LOGGER.info("retrieveInsuredClientData end");
	}
	
	// Method to get owner client data
	//
	// @param CDSDetailForm arf
	private void retrieveOwnerClientData(final CDSDetailForm arf) throws IUMException {

		LOGGER.info("retrieveOwnerClientData start");
		arf.setOwnerClientNo(arf.getOwnerClientNo().toUpperCase());
		AssessmentRequest ar = new AssessmentRequest();
		ClientData cdata = ar.getClientData(arf.getOwnerClientNo());

		if (cdata != null && cdata.getGivenName() != null
				&& !cdata.getGivenName().equals("")) {
			
			if (cdata.getAge() == 0) {
				
				arf.setOwnerAge("");
			} else {
				
				arf.setOwnerAge(String.valueOf(cdata.getAge()));
			}
			
			if (cdata.getBirthDate() != null) {
				
				arf.setOwnerBirthDate((DateHelper.format(cdata.getBirthDate(),
						"ddMMMyyyy")).toUpperCase());
			} else {
				
				arf.setOwnerBirthDate("");
			}
			if (cdata.getGivenName() != null) {
				arf.setOwnerFirstName(cdata.getGivenName());
			} else {
				arf.setOwnerFirstName("");
			}
			if (cdata.getLastName() != null) {
				arf.setOwnerLastName(cdata.getLastName());
			} else {
				arf.setOwnerLastName("");
			}
			if (cdata.getMiddleName() != null) {
				arf.setOwnerMiddleName(cdata.getMiddleName());
			} else {
				arf.setOwnerMiddleName("");
			}
			if (cdata.getTitle() != null) {
				arf.setOwnerTitle(cdata.getTitle());
			} else {
				arf.setOwnerTitle("");
			}
			if (cdata.getSex() != null) {
				arf.setOwnerSex(cdata.getSex());
			} else {
				arf.setOwnerSex("");
			}
			if (cdata.getSuffix() != null) {
				arf.setOwnerSuffix(cdata.getSuffix());
			} else {
				arf.setOwnerSuffix("");
			}
			arf.setOwnerOriginal(arf.getOwnerClientNo()
					+ arf.getOwnerFirstName() + arf.getOwnerLastName()
					+ arf.getOwnerMiddleName() + arf.getOwnerTitle()
					+ arf.getOwnerSuffix() + arf.getOwnerSex());

		} else {
			arf.setOwnerAge("");
			arf.setOwnerFirstName("");
			arf.setOwnerMiddleName("");
			arf.setOwnerLastName("");
			arf.setOwnerBirthDate("");
			arf.setOwnerSuffix("");
			arf.setOwnerTitle("");
			arf.setOwnerSex("");
			arf.setOwnerOriginal("");
		}
		LOGGER.info("retrieveOwnerClientData end");
	}
	
	// Method to refreshCD and connect to ingenium
	//
	// @param String policyNo
	// @param String suffix
	// @param HttpServletRequest request
	private void refreshCds(
			final String policyNo,
			final String suffix,
			final HttpServletRequest request) throws Exception {
		
		LOGGER.info("refreshCds start");
		final String referenceNo = policyNo.concat(suffix);
		LOGGER.debug("Refreshing CDS... Policy#:" + referenceNo);

		String hasIUMRecord = request.getSession().getAttribute("hasIUMRecord") != null ? (String) request.getSession().getAttribute("hasIUMRecord")
				: request.getParameter("hasIUMRecord") != null ? request.getParameter("hasIUMRecord") : "";
		
		IngeniumGateway ingenium = new IngeniumGateway();
		Integer clearCaseNo = (Integer) request.getSession().getAttribute("doClearCase_" + referenceNo);
		if ("true".equals(hasIUMRecord)) {
			if (clearCaseNo != null) {
				if (clearCaseNo.intValue() == 7) {
					ingenium.doClearCase(referenceNo);
					request.getSession().setAttribute("doClearCase_" + referenceNo, new Integer(1));
				}
				request.getSession().setAttribute("doClearCase_" + referenceNo, new Integer(clearCaseNo.intValue() + 1));
			} else {
				ingenium.doClearCase(referenceNo);
				request.getSession().setAttribute("doClearCase_" + referenceNo, new Integer(1));
			}
		}
		
		ingenium.setRequest(request);
		ingenium.retrieveCDSDetails(policyNo, suffix);
		LOGGER.info("refreshCds end");
	}
}
