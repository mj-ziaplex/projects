package com.slocpi.ium.ui.action;


import java.io.IOException;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.slocpi.ium.IUMException;
import com.slocpi.ium.data.AccessData;
import com.slocpi.ium.data.AccessTemplateData;
import com.slocpi.ium.data.AutoAssignCriteriaData;
import com.slocpi.ium.data.AutoAssignmentData;
import com.slocpi.ium.data.ClientTypeData;
import com.slocpi.ium.data.DocumentTypeData;
import com.slocpi.ium.data.ExaminationAreaData;
import com.slocpi.ium.data.ExaminationPlaceData;
import com.slocpi.ium.data.HolidayData;
import com.slocpi.ium.data.LOBData;
import com.slocpi.ium.data.MIBActionData;
import com.slocpi.ium.data.MIBImpairmentData;
import com.slocpi.ium.data.MIBLetterData;
import com.slocpi.ium.data.MIBNumberData;
import com.slocpi.ium.data.PageData;
import com.slocpi.ium.data.RankData;
import com.slocpi.ium.data.RequirementFormData;
import com.slocpi.ium.data.RolesData;
import com.slocpi.ium.data.SectionData;
import com.slocpi.ium.data.SpecializationData;
import com.slocpi.ium.data.StatusData;
import com.slocpi.ium.data.SunLifeDeptData;
import com.slocpi.ium.data.TestProfileData;
import com.slocpi.ium.data.UserData;
import com.slocpi.ium.data.UserProfileData;
import com.slocpi.ium.ui.form.ReferenceCodeForm;
import com.slocpi.ium.ui.util.StateHandler;
import com.slocpi.ium.underwriter.Reference;
import com.slocpi.ium.underwriter.UnderWriterException;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.DateHelper;
import com.slocpi.ium.util.IUMConstants;
import com.slocpi.ium.util.ValueConverter;


/**
 * @TODO Class Description CreateReferenceCodesAction
 * @author: Ma. Theresa Vicencio
 * @version: 1.0
 */
public class UpdateReferenceCodeAction extends IUMAction {
  
	private static final Logger LOGGER = LoggerFactory.getLogger(UpdateReferenceCodeAction.class);
	/**
	 * @TODO method description for execute
	 * @param mapping mapping of request to an instance of this class
	 * @param form  object
	 * @param request request object
	 * @param response response object
	 * @exception IOException if an input/output error occurs
	 * @exception ServletException if a servlet exception occurs
	 * @return ActionForward as defined in the mapping parameter.
	 */
	public ActionForward handleAction(ActionMapping mapping, 
									  ActionForm form, 
									  HttpServletRequest request, 
									  HttpServletResponse response)
									  throws Exception {
		
		LOGGER.info("handleAction start");
		String page = "";
		try {
			
			StateHandler sh = new StateHandler();
			UserData ud = sh.getUserData(request);
			UserProfileData profile = ud.getProfile();
			String userId = profile.getUserId();
			String pageId = "";
	
			ReferenceCodeForm refCodeForm = (ReferenceCodeForm) form;
			String refCode = refCodeForm.getReferenceCode();
			String flag = refCodeForm.getMaintainFlag();
			String idxUpdateCode = refCodeForm.getIdxUpdateCode();
			
			if (refCode.equals(IUMConstants.REF_CODE_AUTO_ASSIGN)) {
				refCodeForm.setId(idxUpdateCode);					
			}
			else {
				refCodeForm.setCode(idxUpdateCode);					
			}				
			
			if (refCode != null) {			
			
				if (refCode.equals(IUMConstants.REF_CODE_REQUIREMENT_FORMS)) {
					saveRequirementForm(refCodeForm, userId);
				}
				else if (refCode.equals(IUMConstants.REF_CODE_LOB)) {
					saveLOB(refCodeForm, userId);
				}
				else if (refCode.equals(IUMConstants.REF_CODE_DEPARTMENT)) {
					saveDepartment(refCodeForm, userId);
				}
				else if (refCode.equals(IUMConstants.REF_CODE_SECTION)) {
					saveSection(refCodeForm, userId);
				}
				else if (refCode.equals(IUMConstants.REF_CODE_EXAM_SPECIALIZATION)) {
					saveExamSpecialization(refCodeForm, userId);
				}
				else if (refCode.equals(IUMConstants.REF_CODE_EXAM_PLACE)) {
					saveExamPlace(refCodeForm, userId);
				}
				else if (refCode.equals(IUMConstants.REF_CODE_EXAM_AREA)) {
					saveExamArea(refCodeForm, userId);
				}
				else if (refCode.equals(IUMConstants.REF_CODE_DOCUMENT_TYPE)) {
					saveDocumentType(refCodeForm, userId);
				}
				else if (refCode.equals(IUMConstants.REF_CODE_CLIENT_TYPE)) {
					saveClientType(refCodeForm, userId);
				}
				else if (refCode.equals(IUMConstants.REF_CODE_AUTO_ASSIGN_CRITERIA)) {
					saveAutoAssignCriteria(refCodeForm, userId);
				}
				else if (refCode.equals(IUMConstants.REF_CODE_STATUS)) {
					saveStatusCode(refCodeForm, userId);
				}
				else if (refCode.equals(IUMConstants.REF_CODE_ROLES)) {
					saveRole(refCodeForm, userId);
				}
				else if (refCode.equals(IUMConstants.REF_CODE_ACCESS_TEMPLATE)) {
					saveAccessTemplate(refCodeForm, userId);
				}
				else if (refCode.equals(IUMConstants.REF_CODE_TEST_PROFILE)) {
					saveTestProfile(refCodeForm, userId);
				}
				else if (refCode.equals(IUMConstants.REF_CODE_EXAM_RANK)) {
					saveExamRank(refCodeForm, userId);
				}
				else if (refCode.equals(IUMConstants.REF_CODE_MIB_IMPAIRMENT)) {
					saveMIBImpairment(refCodeForm, userId);
				}
				else if (refCode.equals(IUMConstants.REF_CODE_MIB_NUMBER)) {
					saveMIBNumber(refCodeForm, userId);
				}
				else if (refCode.equals(IUMConstants.REF_CODE_MIB_ACTION)) {
					saveMIBAction(refCodeForm, userId);
				}
				else if (refCode.equals(IUMConstants.REF_CODE_MIB_LETTER)) {
					saveMIBLetter(refCodeForm, userId);
				}
				else if (refCode.equals(IUMConstants.REF_CODE_HOLIDAY)) {
					saveHoliday(refCodeForm, userId);
				}						
				else if (refCode.equals(IUMConstants.REF_CODE_AUTO_ASSIGN)) {
					saveAutoAssignMapping(refCodeForm, userId);
				}
				else if (refCode.equals(IUMConstants.REF_CODE_ACCESS)) {
					saveAccess(refCodeForm, userId);
				}
				else if (refCode.equals(IUMConstants.REF_CODE_PAGE)) {
					savePage(refCodeForm, userId);
				}
			}

			page = "viewAdminReferencePage";
		} 
		catch (UnderWriterException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			ActionErrors errors = new ActionErrors();
			errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.admin.generic", e.getMessage()));
			saveErrors(request, errors);
			page = "viewAdminReferencePage";
		}	
		catch (IUMException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			ActionErrors errors = new ActionErrors();
			errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.system.exception", e.getMessage()));
			saveErrors(request, errors);
			page = "errorPage";
		}
		catch (Exception e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			ActionErrors errors = new ActionErrors();
			errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.system.exception", e.getMessage()));
			saveErrors(request, errors);
			page = "errorPage";
		}
		
		LOGGER.info("handleAction end");
		LOGGER.debug("PAGE FORWARD-->" + page);
		
		return (mapping.findForward(page));
	}


	private void saveRequirementForm(ReferenceCodeForm form, String userId) throws UnderWriterException, IUMException {
		
		LOGGER.info("saveRequirementForm start");
		
		Date currentDate = new Date();
		RequirementFormData data = new RequirementFormData();
		data.setFormName(form.getFormName());
		data.setTemplateFormat(form.getTemplateFormat());
		data.setTemplateName(form.getTemplateName());
		data.setStatus(Boolean.valueOf(form.getStatus()).booleanValue());
		data.setUpdatedBy(userId);
		data.setUpdateDate(currentDate);	
		Reference reference = new Reference();
		reference.editRequirementForm(data);
		
		LOGGER.info("saveRequirementForm end");
	}
	

	private void saveLOB(ReferenceCodeForm form, String userId) throws UnderWriterException, IUMException {
		
		LOGGER.info("saveLOB start");
		
		Date currentDate = new Date();
		LOBData data = new LOBData();
		data.setLOBCode(form.getCode());
		data.setLOBDesc(form.getDescription().toUpperCase());
		data.setUpdatedBy(userId);
		data.setUpdateDate(currentDate);		
		Reference reference = new Reference();
		reference.editLOB(data);
		
		LOGGER.info("saveLOB end");	
	}


	private void saveDepartment(ReferenceCodeForm form, String userId) throws UnderWriterException, IUMException {

		LOGGER.info("saveDepartment start");
		
		Date currentDate = new Date();
		SunLifeDeptData data = new SunLifeDeptData();
		data.setDeptId(form.getCode());
		data.setDeptDesc(form.getDescription().toUpperCase());
		data.setUpdatedBy(userId);
		data.setUpdateDate(currentDate);
		Reference reference = new Reference();
		reference.editDepartment(data);
		
		LOGGER.info("saveDepartment end");
		
	}


	private void saveSection(ReferenceCodeForm form, String userId) throws UnderWriterException, IUMException {
		
		LOGGER.info("saveSection start");
		
		Date currentDate = new Date();
		SectionData data = new SectionData();
		data.setSectionId(form.getCode());
		data.setSectionDesc(form.getDescription().toUpperCase());
		data.setUpdatedBy(userId);
		data.setUpdateDate(currentDate);		
		Reference reference = new Reference();
		reference.editSection(data);
		
		LOGGER.info("saveSection end");
	}


	private void saveExamSpecialization(ReferenceCodeForm form, String userId) throws UnderWriterException, IUMException {
		
		LOGGER.info("saveExamSpecialization start");
		
		Date currentDate = new Date();
		SpecializationData data = new SpecializationData();
		data.setSpecializationId(Integer.parseInt(form.getCode()));
		data.setSpecializationDesc(form.getDescription().toUpperCase());
		data.setUpdatedBy(userId);
		data.setUpdateDate(currentDate);	
		Reference reference = new Reference();
		reference.editExamSpecialization(data);
		
		LOGGER.info("saveExamSpecialization end");
		
	}


	private void saveExamPlace(ReferenceCodeForm form, String userId) throws UnderWriterException, IUMException {

		LOGGER.info("saveExamPlace start");
		
		Date currentDate = new Date();
		ExaminationPlaceData data = new ExaminationPlaceData();
		data.setExaminationPlaceId(Integer.parseInt(form.getCode()));
		data.setExaminationPlaceDesc(form.getDescription().toUpperCase());
		data.setUpdatedBy(userId);
		data.setUpdateDate(currentDate);	
		Reference reference = new Reference();
		reference.editExamPlace(data);
		
		LOGGER.info("saveExamPlace end");
		
	}
	

	private void saveExamArea(ReferenceCodeForm form, String userId) throws UnderWriterException, IUMException {
		
		LOGGER.info("saveExamArea start");
		
		Date currentDate = new Date();
		ExaminationAreaData data = new ExaminationAreaData();
		data.setExaminationAreaId(Integer.parseInt(form.getCode()));
		data.setExaminationAreaDesc(form.getDescription().toUpperCase());
		data.setUpdatedBy(userId);
		data.setUpdateDate(currentDate);	
		Reference reference = new Reference();
		reference.editExamArea(data);
		
		LOGGER.info("saveExamArea end");	
	}


	private void saveDocumentType(ReferenceCodeForm form, String userId) throws UnderWriterException, IUMException {
		
		LOGGER.info("saveDocumentType start");
		
		Date currentDate = new Date();
		DocumentTypeData data = new DocumentTypeData();
		data.setDocCode(form.getCode());
		data.setDocDesc(form.getDescription().toUpperCase());
		data.setUpdatedBy(userId);
		data.setUpdateDate(currentDate);	
		Reference reference = new Reference();
		reference.editDocumentType(data);
		
		LOGGER.info("saveDocumentType end");
		
	}


	private void saveClientType(ReferenceCodeForm form, String userId) throws UnderWriterException, IUMException {
		
		LOGGER.info("saveClientType start");
		
		Date currentDate = new Date();
		ClientTypeData data = new ClientTypeData();
		data.setClientTypeCode(form.getCode());
		data.setClientTypeDesc(form.getDescription().toUpperCase());
		data.setUpdatedBy(userId);
		data.setUpdateDate(currentDate);	
		Reference reference = new Reference();
		reference.editClientType(data);
		
		LOGGER.info("saveClientType end");	
	}
	

	private void saveAutoAssignCriteria(ReferenceCodeForm form, String userId) throws UnderWriterException, IUMException {
		
		LOGGER.info("saveAutoAssignCriteria start");
		
		Date currentDate = new Date();
		AutoAssignCriteriaData data = new AutoAssignCriteriaData();
		
		data.setFieldCode(form.getCode());
		data.setFieldDesc(form.getDescription().toUpperCase());
		data.setNotificationSent(Boolean.valueOf(form.getStatus()).booleanValue());
		data.setUpdatedBy(userId);
		data.setUpdateDate(currentDate);	
		Reference reference = new Reference();
		reference.editAutoAssignCriteria(data);
		
		LOGGER.info("saveAutoAssignCriteria end");
	}


	private void saveStatusCode(ReferenceCodeForm form, String userId) throws UnderWriterException, IUMException {
		
		LOGGER.info("saveStatusCode start");
		
		Date currentDate = new Date();
		StatusData data = new StatusData();
		data.setStatusId(Long.parseLong(form.getCode()));
		data.setStatusDesc(form.getDescription().toUpperCase());
		data.setStatusType(form.getType());
		data.setUpdatedBy(userId);
		data.setUpdateDate(currentDate);	
		Reference reference = new Reference();
		reference.editStatusCode(data);
		
		LOGGER.info("saveStatusCode end");
	}


	private void saveRole(ReferenceCodeForm form, String userId) throws UnderWriterException, IUMException {
		
		LOGGER.info("saveRole start");
		
		Date currentDate = new Date();
		RolesData data = new RolesData();
		data.setRolesId(form.getCode());
		data.setRolesDesc(form.getDescription().toUpperCase());
		data.setUpdatedBy(userId);
		data.setUpdateDate(currentDate);	
		Reference reference = new Reference();
		reference.editRole(data);
		
		LOGGER.info("saveRole end");
		
	}


	private void saveAccessTemplate(ReferenceCodeForm form, String userId) throws UnderWriterException, IUMException {
		
		LOGGER.info("saveAccessTemplate start");
		
		Date currentDate = new Date();
		AccessTemplateData data = new AccessTemplateData();
		data.setTemplateID(Long.parseLong(form.getCode()));
		data.setTemplateDesc(form.getDescription());
		data.setUpdatedBy(userId);
		data.setUpdatedDate(DateHelper.sqlTimestamp(currentDate));	
		Reference reference = new Reference();
		reference.editAccessTemplate(data);
		
		LOGGER.info("saveAccessTemplate end");
		
	}
	

	private void saveTestProfile(ReferenceCodeForm form, String userId) throws UnderWriterException, IUMException {
		
		LOGGER.info("saveTestProfile start");
		
		Date currentDate = new Date();
		TestProfileData data = new TestProfileData();
		data.setTestId(Long.parseLong(form.getCode()));
		data.setTestDesc(form.getDescription().toUpperCase());
		data.setTestType(form.getType());
		data.setValidity(Integer.parseInt(form.getDaysValid()));
		data.setTaxable(Boolean.valueOf(form.getTaxable()).booleanValue());
		data.setFollowUpNumber(Long.parseLong(form.getFollowUpNo()));
		data.setUpdatedBy(userId);
		data.setUpdateDate(currentDate);	
		Reference reference = new Reference();
		reference.editTestProfile(data);
		
		LOGGER.info("saveTestProfile end");
	}


	private void saveExamRank(ReferenceCodeForm form, String userId) throws UnderWriterException, IUMException {
	
		LOGGER.info("saveExamRank start");
		
		Date currentDate = new Date();
		RankData data = new RankData();
		data.setRankCode(Long.parseLong(form.getCode()));
		data.setRankDesc(form.getDescription().toUpperCase());
		data.setRankFee(ValueConverter.convertCurrency(form.getFee()));
		data.setUpdatedBy(userId);
		data.setUpdateDate(currentDate);	
		Reference reference = new Reference();
		reference.editExamRank(data);
		
		LOGGER.info("saveExamRank end");	
	}


	private void saveMIBImpairment(ReferenceCodeForm form, String userId) throws UnderWriterException, IUMException {
		
		LOGGER.info("saveMIBImpairment start");
		
		Date currentDate = new Date();
		MIBImpairmentData data = new MIBImpairmentData();
		data.setMIBImpairmentCode(form.getCode());
		data.setMIBImpairmentDesc(form.getDescription().toUpperCase());
		data.setUpdatedBy(userId);
		data.setUpdateDate(currentDate);
		Reference reference = new Reference();
		reference.editMIBImpairmentCode(data);
		
		LOGGER.info("saveMIBImpairment end");
		
	}


	private void saveMIBNumber(ReferenceCodeForm form, String userId) throws UnderWriterException, IUMException {
		
		LOGGER.info("saveMIBNumber start");
		
		Date currentDate = new Date();
		MIBNumberData data = new MIBNumberData();
		data.setMIBNumberCode(form.getCode());
		data.setMIBNumberDesc(form.getDescription().toUpperCase());
		data.setUpdatedBy(userId);
		data.setUpdateDate(currentDate);
		Reference reference = new Reference();
		reference.editMIBNumberCode(data);
		
		LOGGER.info("saveMIBNumber end");	
	}
	

	private void saveMIBAction(ReferenceCodeForm form, String userId) throws UnderWriterException, IUMException {
		
		LOGGER.info("saveMIBAction start");
		
		Date currentDate = new Date();
		MIBActionData data = new MIBActionData();
		data.setMIBActionCode(form.getCode());
		data.setMIBActionDesc(form.getDescription().toUpperCase());
		data.setUpdatedBy(userId);
		data.setUpdateDate(currentDate);
		Reference reference = new Reference();
		reference.editMIBActionCode(data);
		
		LOGGER.info("saveMIBAction end");
	}


	private void saveMIBLetter(ReferenceCodeForm form, String userId) throws UnderWriterException, IUMException {
		
		LOGGER.info("saveMIBLetter start");
		
		Date currentDate = new Date();
		MIBLetterData data = new MIBLetterData();
		data.setMIBLetterCode(form.getCode());
		data.setMIBLetterDesc(form.getDescription().toUpperCase());
		data.setUpdatedBy(userId);
		data.setUpdateDate(currentDate);
		Reference reference = new Reference();
		reference.editMIBLetterCode(data);
		
		LOGGER.info("saveMIBLetter end");
		
	}
	
	private void saveHoliday(ReferenceCodeForm form, String userId) throws UnderWriterException, IUMException {
		
		LOGGER.info("saveHoliday start");
		
		Date currentDate = new Date();
		HolidayData data = new HolidayData();
		data.setHolidayCode(Long.parseLong(form.getCode()));
		data.setHolidayDesc(form.getDescription());
		data.setHolidayDate(DateHelper.sqlDate(DateHelper.parse(form.getDate(), "ddMMMyyyy")));
		data.setUpdatedBy(userId);
		data.setUpdateDate(currentDate);
		Reference reference = new Reference();
		reference.editHolidayCode(data);
		
		LOGGER.info("saveHoliday end");
		
	}


	private void saveAutoAssignMapping(ReferenceCodeForm form, String userId) throws UnderWriterException, IUMException {
		
		LOGGER.info("saveAutoAssignMapping start");
		
		Date currentDate = new Date();
		AutoAssignmentData data = new AutoAssignmentData();
		data.setAutoAssignmentId(Long.parseLong(form.getId()));
		data.setCriteriaId(Long.parseLong(form.getCode()));
		data.setFieldValue(form.getDescription());
		data.setUserCode(form.getUnderwriter());
		data.setUpdatedBy(userId);
		data.setUpdateDate(currentDate);
		Reference reference = new Reference();
		reference.editMapping(data);
		
		LOGGER.info("saveAutoAssignMapping end");
		
	}
		

	private void saveAccess(ReferenceCodeForm form, String userId) throws UnderWriterException, IUMException {
		
		LOGGER.info("saveAccess start");
		
		Date currentDate = new Date();
		AccessData data = new AccessData();
		data.setAccessId(Long.parseLong(form.getCode()));
		data.setAccessDesc(form.getDescription());
		data.setUpdatedBy(userId);
		data.setUpdateDate(currentDate);	
		Reference reference = new Reference();
		reference.editAccess(data);
		
		LOGGER.info("saveAccess end");
	}


	private void savePage(ReferenceCodeForm form, String userId) throws UnderWriterException, IUMException {
		
		LOGGER.info("savePage start");
		
		Date currentDate = new Date();
		PageData data = new PageData();
		data.setPageId(Long.parseLong(form.getCode()));
		data.setPageDesc(form.getDescription());
		data.setUpdatedBy(userId);
		data.setUpdateDate(currentDate);	
		Reference reference = new Reference();
		reference.editPage(data);
		
		LOGGER.info("savePage end");
	}

    
}

