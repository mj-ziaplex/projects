/*
 * Created on Feb 27, 2004
 * package name = com.slocpi.ium.ui.action
 * file name    = AddRoleToUserAction.java
 */
package com.slocpi.ium.ui.action;

import java.util.ArrayList;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.slocpi.ium.data.RolesData;
import com.slocpi.ium.data.UserData;
import com.slocpi.ium.data.UserProfileData;
import com.slocpi.ium.ui.form.UserProfileForm;
import com.slocpi.ium.ui.util.StateHandler;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.UserManager;

/**
 * TODO Class Description of AddRoleToUserAction.java
 * @author Engel
 * 
 */
public class AddRoleToUserAction extends IUMAction {
	private static final Logger LOGGER = LoggerFactory.getLogger(AddRoleToUserAction.class);
	/* (non-Javadoc)
	 * @see com.slocpi.ium.ui.action.IUMAction#handleAction(org.apache.struts.action.ActionMapping, org.apache.struts.action.ActionForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	public ActionForward handleAction(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response)
		throws Exception {
		
		LOGGER.info("handleAction start");
		String page="";
		try{		
			UserProfileForm upform = (UserProfileForm)form;
			StateHandler sh = new StateHandler();
			UserData userlogged = sh.getUserData(request);
			UserProfileData udata= userlogged.getProfile();
			
			UserManager usrmgr = new UserManager();
			usrmgr.addRoleToUser(upform.getUserId(),udata.getUserId(),upform.getRoleToAdd());
			
			upform.setRoleToAdd("");
			UserData userData = usrmgr.retrieveUserDetails(upform.getUserId());
			ArrayList roles = userData.getRoles();
			
			
			if(roles.size()==1){
				UserProfileData profile = userData.getProfile();
				profile.setUpdateDate(new Date());
				profile.setUpdatedBy(udata.getUserId());
				profile.setRole( ((RolesData)roles.get(0)).getRolesId() );
				usrmgr.updateUserProfileData(profile);	
			}
			page="userProfileDetailPage";
		}catch (Exception e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			ActionErrors errors = new ActionErrors();
			errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.system.exception", e.getMessage()));
			saveErrors(request, errors); 
			page= "errorPage";
		}		
		LOGGER.info("handleAction end");
		LOGGER.debug("PAGE FORWARD-->" + page);
		return (mapping.findForward(page));
	}
	
	

}
