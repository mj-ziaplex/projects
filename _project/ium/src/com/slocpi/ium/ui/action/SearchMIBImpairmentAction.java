/*
 * Created on Nov 2, 2010
 * package name = com.slocpi.ium.ui.action
 * file name    = CreatePolicyRequirementsAction.java
 */
package com.slocpi.ium.ui.action;

import java.util.ArrayList;
import java.util.Iterator;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.slocpi.ium.data.MIBImpairmentsData;
import com.slocpi.ium.ui.form.SearchMIBImpairmentsForm;
import com.slocpi.ium.ui.util.Page;
import com.slocpi.ium.ui.util.Pagination;
import com.slocpi.ium.underwriter.MIBImpairments;
import com.slocpi.ium.util.CodeHelper;

/**
 * This class is responsible for displaying the IUM's list of requirements.
 * This class will display the list and allow the viewing of a specific requirement.
 * @author Engel
 * 
 */
public class SearchMIBImpairmentAction extends IUMAction {

	private static final Logger LOGGER = LoggerFactory.getLogger(SearchMIBImpairmentAction.class);
	/* (non-Javadoc)
	 * @see com.slocpi.ium.ui.action.IUMAction#handleAction(org.apache.struts.action.ActionMapping, org.apache.struts.action.ActionForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	public ActionForward handleAction(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response)
		throws Exception {
		
		LOGGER.info("handleAction start");
		
		String refNo = request.getParameter("refNo")!=null && !request.getParameter("refNo").equals("") ? request.getParameter("refNo") : "" ;
		
		String lob = request.getParameter("lob") != null ? (String)request.getParameter("lob") : "";
		String currentStatus = request.getParameter("currentStatus") != null ? (String)request.getParameter("currentStatus") : "";
		String arFormRefNo = request.getParameter("arFormRefNo") != null ? (String)request.getParameter("arFormRefNo") : "";
		String arFormOwnerClientNo = request.getParameter("arFormOwnerClientNo") != null ? (String)request.getParameter("arFormOwnerClientNo") : "";
		String arFormInsuredClientNo = request.getParameter("arFormInsuredClientNo") != null ? (String)request.getParameter("arFormInsuredClientNo") : "";
		String sessionId = request.getParameter("sessionId") != null ? (String)request.getParameter("sessionId") : "";
		String branchCode = request.getParameter("branchCode") != null ? (String)request.getParameter("branchCode") : "";
		String agentCode = request.getParameter("agentCode") != null ? (String)request.getParameter("agentCode") : "";
		String assignedTo = request.getParameter("assignedTo") != null ? (String)request.getParameter("assignedTo") : "";
		String sourceSystem = request.getParameter("sourceSystem") != null ? (String)request.getParameter("sourceSystem") : "";
		String EXECUTE_KO = request.getParameter("EXECUTE_KO") != null ? request.getParameter("EXECUTE_KO") : "";
		String sendButtonControl = request.getParameter("sendButtonControl") != null ? request.getParameter("sendButtonControl") : "";
		String pageNumber = request.getParameter("pageNumber") != null ? request.getParameter("pageNumber") : "";
		String policySuffix = request.getParameter("policySuffix") != null ? request.getParameter("policySuffix") : "";
		String mode = request.getParameter("mode") != null ? request.getParameter("mode") : "";
		String code = request.getParameter("code") != null ? request.getParameter("code") : "";
		String desc = request.getParameter("desc") != null ? request.getParameter("desc") : "";
			
		if(refNo.equals("")){
			refNo = arFormRefNo;
		}
		
		String page = "";
		String viewPage = "viewImpairments";
		SearchMIBImpairmentsForm reqfrm = (SearchMIBImpairmentsForm)form;
		
		if(mode.equals("") || mode.equals("search")){
			page = viewPage;			
			if(pageNumber==null || pageNumber.equals("")){
				pageNumber="1";
			}

			ArrayList arrForm = new ArrayList();
			try{
				arrForm = populateMIBImpariments(code, desc);
			}catch(Exception e){
				LOGGER.error(CodeHelper.getStackTrace(e));
			}
			
			int recPerPage = 10;
			Pagination pgn = new Pagination(arrForm,recPerPage);
			Page pageObj = pgn.getPage(Integer.parseInt(pageNumber));
			
			request.setAttribute("pageRecord", pageObj);
			request.setAttribute("requirement",reqfrm);
			request.setAttribute("lob", lob);
			request.setAttribute("arFormRefNo", arFormRefNo);
			request.setAttribute("policySuffix", policySuffix);
			request.setAttribute("sourceSystem", sourceSystem);
			request.setAttribute("code", code);
			request.setAttribute("desc", desc);
		}
		
		reqfrm.setPageNumber(pageNumber);
		LOGGER.info("handleAction end");
		LOGGER.debug("PAGE FORWARD-->" + page);
		return (mapping.findForward(page));
	}


	
	private ArrayList populateMIBImpariments(String code, String desc) throws Exception {
		
		LOGGER.info("populateMIBImpariments start");
		ArrayList mibImpList = new ArrayList();

		MIBImpairments mib = new MIBImpairments();
		ArrayList list = mib.getMIBImpairments(code,desc);
		if (list != null && !list.isEmpty()) {
			Iterator it = list.iterator();
			while (it.hasNext()) {
				MIBImpairmentsData data = (MIBImpairmentsData) it.next();
				SearchMIBImpairmentsForm form = new SearchMIBImpairmentsForm();
				form.setCode(data.getCode());
				form.setDesc(data.getDesc());
				mibImpList.add(form);
			}
		}
		LOGGER.info("populateMIBImpariments end");
		return (mibImpList);
	}

}
