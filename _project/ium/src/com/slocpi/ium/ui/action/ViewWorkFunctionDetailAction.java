/*
 * Created on Feb 26, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.ui.action;

import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.slocpi.ium.data.AccessData;
import com.slocpi.ium.data.PageFunctionData;
import com.slocpi.ium.ui.form.WorkFunctionForm;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.UserManager;

/**
 * @author Abie
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class ViewWorkFunctionDetailAction extends IUMAction{
	private static final Logger LOGGER = LoggerFactory.getLogger(ViewWorkFunctionDetailAction.class);

	/* (non-Javadoc)
	 * @see com.slocpi.ium.ui.action.IUMAction#handleAction(org.apache.struts.action.ActionMapping, org.apache.struts.action.ActionForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	 /**
	  * This method handles the processes of the ViewWorkFunctionDetailAction class. 
	  * It uses the setDisplayAttributes and getWorkFunction methods to display the work function.
	  * This also handles the saving of changes made on the work function thru the saveWorkFunctions method of UserManager.     
	  */
	public ActionForward handleAction(
		ActionMapping mapping, 
		ActionForm form, 
		HttpServletRequest request, 
		HttpServletResponse response) 
		throws Exception {
		
		LOGGER.info("handleAction start");
		String page = "";
		try{
			UserManager userManager = new UserManager();
			ArrayList list = new ArrayList();
			WorkFunctionForm workFunctionForm = new WorkFunctionForm();
			
			int size = userManager.getAccess().size();
			String check ="";
			check = (String)request.getParameter("check");
			String[] header = new String[size];
			if(check == null)
				check = " ";
			if(check.equals("submit")){
				list = userManager.getWorkFunction();
				userManager.saveWorkFunctions(getWorkFunction(form, list));
			}

			
			list = userManager.getWorkFunction();
			
			workFunctionForm = setDisplayAttributes(list,size);
			for(int i = 0;i<size;i++)
				header[i] = ((AccessData)userManager.getAccess().get(i)).getAccessDesc();
			
			workFunctionForm.setPageHeader(header);
				
						
			request.setAttribute("workFunctionForm", workFunctionForm);
			
			page = "viewWorkFunctionDetailPage";					
		}
		catch(Exception e){
			LOGGER.error(CodeHelper.getStackTrace(e));
			ActionErrors errors = new ActionErrors();
			errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.system.exception", e.getMessage()));
			saveErrors(request, errors);
			page = "errorPage"; 
		}
		LOGGER.info("handleAction end");
		LOGGER.debug("PAGE FORWARD-->" + page);
		
		return (mapping.findForward(page));
	}
	
	
	 private WorkFunctionForm setDisplayAttributes(ArrayList list, int size){
		 
		 LOGGER.info("setDisplayAttributes start");
		int rowSize = 0;
		ArrayList pageAccess = new ArrayList();
		int listSize = list.size();
		String pageDesc = null;
		long pageId = 0;
		long prevPageId = 0;
		long currPageId = 0;
		long accessId = 0;
		boolean access = false;
		char checkVal = 'n';
		boolean isLast = false;
		WorkFunctionForm workFunctionForm = new WorkFunctionForm();
		workFunctionForm.setAccessCount(size);			
		char tempChar[] = new char[size];
		
		
		if(list.size()>0){
			for(int i=0;i<size;i++)
				tempChar[i] = 'n';
						
			accessId = ((PageFunctionData) list.get(0)).getAccessID();
			access = ((PageFunctionData) list.get(0)).isEnabled();
			pageId = ((PageFunctionData) list.get(0)).getPageID();
			prevPageId = pageId;
			currPageId = pageId;
			rowSize++;

			if(access)
				checkVal = 'y';
			
			
			tempChar[new Long(accessId).intValue()-1] = checkVal;

			for(int i=1;i<listSize;i++){
				checkVal = 'n';
				accessId = ((PageFunctionData) list.get(i)).getAccessID();
				access = ((PageFunctionData) list.get(i)).isEnabled();
				pageId = ((PageFunctionData) list.get(i)).getPageID();
				
				prevPageId = currPageId;
				currPageId = pageId;
			
				if(access)
					checkVal = 'y';
						
							
				if(prevPageId == currPageId){
					tempChar[new Long(accessId).intValue()-1] = checkVal;
					isLast = false;
				}
				else{
					String tempStr = new String(tempChar);
					pageAccess.add(tempStr);
					rowSize++;
					isLast = true;
			
					for(int k=0;k<size;k++)
						tempChar[k] = 'n';
				
					tempChar[new Long(accessId).intValue()-1] = checkVal;
		
				}
				if(i+1 == listSize){
					String tempStr = new String(tempChar);
					pageAccess.add(tempStr);
				}
				
			}
					
			String[] pageName = new String[rowSize];
			String[] pageNameId = new String[rowSize];
			int index = 0;
	
			
			for(int i=0;i<listSize;i++){
				pageId = ((PageFunctionData) list.get(i)).getPageID();
				pageDesc = ((PageFunctionData) list.get(i)).getPageDescription();
				
				if(i==0){
					prevPageId = pageId;
					currPageId = pageId;
					pageName[index] = pageDesc;
					pageNameId[index] = new Long(pageId).toString();
	
				}
				else{
					prevPageId = currPageId;
					currPageId = pageId;
			
				}
				if(prevPageId != currPageId){
					index++;
					pageName[index] = pageDesc;
					pageNameId[index] = new Long(pageId).toString();
	
				}
			}
			workFunctionForm.setPageAccess((String[])(pageAccess.toArray(new String[pageAccess.size()])));
			workFunctionForm.setPageName(pageName);
			workFunctionForm.setRowSize(rowSize);
			workFunctionForm.setPageNameId(pageNameId);
		}
		
		LOGGER.info("setDisplayAttributes end");
		return workFunctionForm;
	}
	
	private ArrayList getWorkFunction(ActionForm form, ArrayList list){

		LOGGER.info("getWorkFunction start");
		String[] resultStr = null;
		int size = 0;
		long tempPageId;
		long tempAccessId;
		long pId;
		long aId;
		char tempAccess = ' ';
		int ctr;
		WorkFunctionForm wForm = (WorkFunctionForm) form;
		UserManager u = new UserManager();
		ArrayList accessList = new ArrayList();
		boolean found = false;
	  	  	  	
		resultStr = wForm.getPageAccessResult();
		size = resultStr.length;
 				
		for(int i = 0;i<size; i++ ){
				
			resultStr[i].trim();
					
			tempPageId = new Long (resultStr[i].substring(0,resultStr[i].indexOf('~'))).longValue();
			tempAccessId = new Long (resultStr[i].substring(resultStr[i].indexOf('~')+1,resultStr[i].indexOf('@'))).longValue();
			tempAccess = resultStr[i].charAt(resultStr[i].length()-1);
			ctr = 0;
			found = false;
			
			while(!found && ctr <list.size()){
				pId = ((PageFunctionData)list.get(ctr)).getPageID();
				aId = ((PageFunctionData)list.get(ctr)).getAccessID();
				if(pId == tempPageId && aId == tempAccessId){
					found = true;
		
					if(tempAccess == 'y')
						((PageFunctionData)list.get(ctr)).setEnabled(true);
				
					else 
					((PageFunctionData)list.get(ctr)).setEnabled(false);
				
				}
				ctr++;
			}
		}
					
		accessList = list;
		LOGGER.info("getWorkFunction end");
		return accessList;
	}
	
	
}
