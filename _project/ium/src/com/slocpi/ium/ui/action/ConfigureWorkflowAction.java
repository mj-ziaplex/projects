package com.slocpi.ium.ui.action;

// struts package
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.StringTokenizer;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.slocpi.ium.IUMException;
import com.slocpi.ium.data.ProcessConfigData;
import com.slocpi.ium.data.UserData;
import com.slocpi.ium.data.UserProfileData;
import com.slocpi.ium.ui.form.ProcessConfigForm;
import com.slocpi.ium.ui.util.StateHandler;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.IUMConstants;
import com.slocpi.ium.workflow.ProcessConfigurator;



public class ConfigureWorkflowAction extends IUMAction {
  
	private static final Logger LOGGER = LoggerFactory.getLogger(ConfigureWorkflowAction.class);
  public ActionForward handleAction(ActionMapping mapping, 
							   ActionForm form, 
							   HttpServletRequest request, 
							   HttpServletResponse response)
							   throws Exception {
	
	LOGGER.info("handleAction start");
	String page = "refreshAR";		
	String actionType = request.getParameter("actionType");
	ProcessConfigForm processConfigForm = (ProcessConfigForm) form;
	page = getPage(processConfigForm.getObjectType());	
	try {
	if (actionType != null && !"".equals(actionType)) {
		if (actionType.equals("refresh")) {
			processConfigForm = fillProcessConfigForm(processConfigForm);
			processConfigForm.reset();
			request.setAttribute("processConfigForm",processConfigForm);
			
		} else if (actionType.equals("maintain")) {
			processConfigForm = getProcessConfig(processConfigForm);	
			processConfigForm = fillProcessConfigForm(processConfigForm);		
			request.setAttribute("processConfigForm",processConfigForm);
			
		} else if (actionType.equals("addNewStatus")) {
			processConfigForm = addNewStatus(processConfigForm);				
			request.setAttribute("processConfigForm",processConfigForm);
			
		} else if (actionType.equals("create")) {
			createNewProcessConfig(request,processConfigForm);	
			processConfigForm = fillProcessConfigForm(processConfigForm);
			processConfigForm.reset();			
			request.setAttribute("processConfigForm",processConfigForm);
						
		} else if (actionType.equals("update")) {
			updateProcessConfig(request,processConfigForm);	
			processConfigForm = fillProcessConfigForm(processConfigForm);
			processConfigForm.reset();			
			request.setAttribute("processConfigForm",processConfigForm);			
		} 
	}	
   } 
	   catch (IUMException e) {	
		LOGGER.error(CodeHelper.getStackTrace(e));
		processConfigForm = fillProcessConfigForm(processConfigForm);
		request.setAttribute("processConfigForm",processConfigForm);   	  		
		ActionErrors errors = new ActionErrors();
	  	errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.duplicateProcessConfiguration", e.getMessage()));
	  	saveErrors(request, errors);	  	
  
		}
	LOGGER.info("handleAction end");
	LOGGER.debug("PAGE FORWARD-->" + page);
	return (mapping.findForward(page));
  }
	
	
	/**
	 * @param string
	 * @return
	 */
	private String getPage(String objectType) {
		LOGGER.info("getPage start");
		
		String page = "refreshAR";
		if (IUMConstants.ASSESSMENT_REQUEST.equals(objectType)) {
			page = "refreshAR";
		} else if (IUMConstants.MEDICAL_RECORDS.equals(objectType)) {
			page = "refreshMed";
		} else if (IUMConstants.POLICY_REQUIREMENTS.equals(objectType)) {
			page = "refreshNM";
		} 
		LOGGER.info("getPage end");
		return page;
	}


/**
	 * @param request
	 * @param processConfigForm
	 */
	private void updateProcessConfig(HttpServletRequest request, ProcessConfigForm processConfigForm) throws SQLException, IUMException {
		
		LOGGER.info("updateProcessConfig start");
		ProcessConfigData processConfData = setFields(processConfigForm);		
		processConfData.setEventId(Long.parseLong(processConfigForm.getEventId()));
		StateHandler sh = new StateHandler();		
		UserData ud = sh.getUserData(request);		
		UserProfileData profile = ud.getProfile();
		String userId = profile.getUserId();
		processConfData.setUpdatedBy(userId);
		processConfData.setUpdateDate(new Date());
		ProcessConfigurator procConf = new ProcessConfigurator();
		procConf.updateProcessConfig(processConfData);
		LOGGER.info("updateProcessConfig end");
	}

	private ProcessConfigData setFields(ProcessConfigForm processConfigForm) {
		
		LOGGER.info("setFields start");
		ProcessConfigData processConfData = new ProcessConfigData();
		processConfData.setEventName(processConfigForm.getEventName());
		String toStatus = processConfigForm.getToStatus();
		if (!"".equals(toStatus)) {		
			processConfData.setToStatus(Long.parseLong(toStatus));
		}
		String fromStatus = processConfigForm.getFromStatus();
		if (!"".equals(fromStatus)) {		
			processConfData.setFromStatus(Long.parseLong(fromStatus));
		}		
		String notificationId = processConfigForm.getNotificationTemplate();
		if (!"".equals(notificationId)) {
			processConfData.setNotification(Long.parseLong(notificationId));
		}
		String notificationInd = processConfigForm.getActiveInd();
		if (!"".equals(notificationInd)) {			 
			processConfData.setNotificationInd(IUMConstants.YES.equals(notificationInd)?true:false);
		}
		processConfData.setLOB(processConfigForm.getLob());
		processConfData.setProcessCode(processConfigForm.getObjectType());
		//ksantos 03262004
		processConfData.setRoles(this.retrieveRoles(processConfigForm));
		
		LOGGER.info("setFields end");
		return processConfData;
	}
		
  	/**
	 * @param request
	 * @param processConfigForm
	 * @return
	 */
	private void createNewProcessConfig(HttpServletRequest request, ProcessConfigForm processConfigForm) throws IUMException, SQLException {
		
		LOGGER.info("createNewProcessConfig start");
		ProcessConfigData processConfData = setFields(processConfigForm);
		StateHandler sh = new StateHandler();		
		UserData ud = sh.getUserData(request);		
		UserProfileData profile = ud.getProfile();
		String userId = profile.getUserId();
		processConfData.setCreatedBy(userId);
		processConfData.setCreateDate(new Date());
		ProcessConfigurator procConf = new ProcessConfigurator();
		procConf.createProcessConfig(processConfData);		
		LOGGER.info("createNewProcessConfig end");
	}

	/**
	 * @param processConfigForm
	 */
	private ProcessConfigForm addNewStatus(ProcessConfigForm processConfigForm) throws SQLException {
		
		LOGGER.info("addNewStatus start");
		ProcessConfigurator procConf = new ProcessConfigurator();
		procConf.addNewStatus(processConfigForm.getLob(),processConfigForm.getStatus1());
		processConfigForm = fillProcessConfigForm(processConfigForm);
		LOGGER.info("addNewStatus end");
		return processConfigForm;		
	}

	/**
	 * @param string
	 */
	private ProcessConfigForm getProcessConfig(ProcessConfigForm processConfigForm) throws SQLException {
		
		LOGGER.info("getProcessConfig start");
		ProcessConfigurator procConf = new ProcessConfigurator();
		ProcessConfigData configData = procConf.getProcessConfig(processConfigForm.getEventId());
		processConfigForm.setEventId(String.valueOf(configData.getEventId()));
		processConfigForm.setEventName(configData.getEventName());
		processConfigForm.setToStatus(String.valueOf(configData.getToStatus()));
		processConfigForm.setFromStatus(String.valueOf(configData.getFromStatus()));
		processConfigForm.setNotificationTemplate(String.valueOf(configData.getNotification()));
		processConfigForm.setActiveInd(configData.isNotificationInd()?IUMConstants.YES:IUMConstants.NO);
		
		LOGGER.info("getProcessConfig end");
		return processConfigForm;
	}

	/**
	 * @param processConfigForm
	 */ 
	  private ProcessConfigForm fillProcessConfigForm(ProcessConfigForm processConfigForm) throws SQLException {
		  
		  LOGGER.info("fillProcessConfigForm start");
		String objectType = processConfigForm.getObjectType();
		String lob = processConfigForm.getLob();	
		ProcessConfigurator procConf = new ProcessConfigurator();	
		processConfigForm.setAvailableStatuses(procConf.getAvailableStatuses(lob,objectType));			
		processConfigForm.setProcessConfigEntries(procConf.retrieveProcessConfigurations(objectType,lob));
		LOGGER.info("fillProcessConfigForm end");
		return processConfigForm;
	  }

	//ksantos 03262004
	private ArrayList retrieveRoles(ProcessConfigForm pcForm) {
		
		LOGGER.info("retrieveRoles start");
		String roleUnparsed = pcForm.getRoles();
		StringTokenizer token = new StringTokenizer(roleUnparsed,"~");
	 	ArrayList roles = new ArrayList();
		while(token.hasMoreTokens()){
			roles.add(token.nextToken());
		}
		LOGGER.info("retrieveRoles end");
		return roles;		
	}
		
    
}

