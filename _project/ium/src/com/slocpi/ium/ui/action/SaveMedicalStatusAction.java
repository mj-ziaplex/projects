/*
 * Created on Jan 7, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.ui.action;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.slocpi.ium.IUMException;
import com.slocpi.ium.data.ClientData;
import com.slocpi.ium.data.MedExamFiltersData;
import com.slocpi.ium.data.MedicalRecordData;
import com.slocpi.ium.data.StatusData;
import com.slocpi.ium.data.SunLifeOfficeData;
import com.slocpi.ium.data.TestProfileData;
import com.slocpi.ium.data.UserAccessData;
import com.slocpi.ium.data.UserData;
import com.slocpi.ium.data.UserProfileData;
import com.slocpi.ium.ui.form.MedicalRecordDetailForm;
import com.slocpi.ium.ui.form.MedicalRecordsForm;
import com.slocpi.ium.ui.util.Page;
import com.slocpi.ium.ui.util.Pagination;
import com.slocpi.ium.ui.util.StateHandler;
import com.slocpi.ium.underwriter.MedicalLabRecord;
import com.slocpi.ium.underwriter.UnderWriterException;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.DateHelper;
import com.slocpi.ium.util.IUMConstants;
import com.slocpi.ium.util.SortHelper;

/**
 * @author Ingrid Villanueva
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class SaveMedicalStatusAction extends IUMAction{
	private static final Logger LOGGER = LoggerFactory.getLogger(SaveMedicalStatusAction.class);
	public ActionForward handleAction (ActionMapping mapping, 
									  ActionForm form, 
									  HttpServletRequest request,
									  HttpServletResponse response)
									  throws Exception
									  {

		LOGGER.info("handleAction start");
		String	page = "medicalRecordsListPage";
		StateHandler sh = new StateHandler();
		UserData ud = sh.getUserData(request);
		UserProfileData upd = ud.getProfile();    
		int numPages = upd.getRecordsPerView();

		int pageNo = 1;
		String reqPageNum = request.getParameter("pageNo");
		String sortBy     = getSortBy(request.getParameter("sortBy"));
		String sortOrder  = getSortOrder(request.getParameter("sortOrder"));
		if (reqPageNum != null) {
			pageNo = Integer.parseInt(reqPageNum);
		}

		try {
			String userId = upd.getUserId();
			String pageId = IUMConstants.PAGE_MEDRECORD_LIST;
			UserAccessData uad = extractAccessPriveleges(userId, pageId);
		
			if (uad.getAccessCode().equals("W")) {
				
				MedicalRecordsForm medForm = (MedicalRecordsForm) form;
				String   errorMessage    = "";		
				String   statusTo        = medForm.getStatusTo();
		
				if (statusTo.equals(Long.toString(IUMConstants.STATUS_REQUESTED))) {
					//requestMedicalExam(medForm);
				}
				
				else if (statusTo.equals(Long.toString(IUMConstants.STATUS_CONFIRMED))) {
					confirmMedicalExam(medForm, userId);

				}
				
				else if (statusTo.equals(Long.toString(IUMConstants.STATUS_VALID))){
					receiveMedicalExam(medForm, userId);
				}
				
		        else if (statusTo.equals(Long.toString(IUMConstants.STATUS_NOT_SUBMITTED))){
		        	notSubmitted(medForm, userId);
		        }
				else if (statusTo.equals(Long.toString(IUMConstants.STATUS_CANCELLED))){
					cancelMedicalExam(medForm, userId);
				}
		        
				ArrayList          a       = new ArrayList();
				MedicalRecordsForm medform = new MedicalRecordsForm();
				a = searchMedicalRecord(form, sortBy, sortOrder);
				
			    ArrayList  formList = formMedicalRecordList(a);
				Pagination pgn      = new Pagination(formList, numPages);
				Page       pg       = pgn.getPage(pageNo);

				medform.setMedicalRecordData(pg.getList());
				medform.setPage(pg);	
				medform.setPageNo(pageNo);
				medform.setSortOrder(sortOrder);
				medform.setSortBy(sortBy);

			    request.setAttribute("medicalRecordsForm", medform);
				request.setAttribute("pageNo", String.valueOf(pageNo));		 
				request.setAttribute("displayPage",pg);
								
			}
			else {
				page = "errorPage";
			}
		}
		catch(UnderWriterException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			ArrayList          a       = new ArrayList();
			MedicalRecordsForm medform = new MedicalRecordsForm();
			a = searchMedicalRecord(form, sortBy, sortOrder);
			ArrayList  formList = formMedicalRecordList(a);
			Pagination pgn = new Pagination(formList, numPages);
			Page pg = pgn.getPage(pageNo);

			medform.setMedicalRecordData(pg.getList());
			medform.setPage(pg);	
			medform.setPageNo(pageNo);
			request.setAttribute("medicalRecordsForm", medform);
			request.setAttribute("pageNo", String.valueOf(pageNo));		 
			request.setAttribute("displayPage",pg);
			
			String err = e.getMessage();
			if (err == null) err="";
			ActionErrors errors = new ActionErrors();
			errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.invalidRequirement", err));
			saveErrors(request, errors);			
		}
		catch(IUMException eIUM) {
			LOGGER.error(CodeHelper.getStackTrace(eIUM));
			ActionErrors errors = new ActionErrors();
			errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.system.exception", eIUM));
			saveErrors(request, errors);						
			page = "errorPage";
		}
		LOGGER.info("handleAction end");
		LOGGER.debug("PAGE FORWARD-->" + page);
		return mapping.findForward(page);
	}
	
	private ArrayList searchMedicalRecord(ActionForm form, String sortBy, String sortOrder) throws Exception {
		
		LOGGER.info("searchMedicalRecord start");
		 MedicalRecordsForm medForm = (MedicalRecordsForm) form;
  	   
		MedExamFiltersData  filter  = new MedExamFiltersData();
		filter.setClientNo(medForm.getClientNo());
		if (medForm.getDateRequested() != null) {
			if (medForm.getDateRequested().trim().length() > 0) {
				filter.setDateRequested(DateHelper.parse(medForm.getDateRequested(),"ddMMMyyyy"));
			}
		}
		if (medForm.getExaminer() != null) {
		   if (medForm.getExaminer().length() > 0) {
			  filter.setExaminer(Long.parseLong(medForm.getExaminer()));
		   }
		}
		filter.setFirstName(medForm.getFirstName());
		if (medForm.getFollowUpEndDate() != null) {
			if (medForm.getFollowUpEndDate().trim().length() > 0) {  	   
			   filter.setEndFollowUpDate(DateHelper.parse(medForm.getFollowUpEndDate(),"ddMMMyyyy"));
			}
		}
		if (medForm.getFollowUpStartDate() != null) {
			if (medForm.getFollowUpStartDate().trim().length() > 0) {   	   
			   filter.setStartFollowUpDate(DateHelper.parse(medForm.getFollowUpStartDate(),"ddMMMyyyy"));
			}
		}   
		if (medForm.getLab() != null) {
			if (medForm.getLab().trim().length() > 0) {
				filter.setLaboratory(Long.parseLong(medForm.getLab()));
			}
		}
		filter.setLastName(medForm.getLastName());
		filter.setReferenceNo(medForm.getReferenceNo());
		if (medForm.getSevenDayMemoDate() != null) {
			if (medForm.getSevenDayMemoDate().trim().length() > 0) {  	   
			   filter.setSevenDayMemoDate(DateHelper.parse(medForm.getSevenDayMemoDate(),"ddMMMyyyy"));
			}
		}
	    if (medForm.getStatus() != null) {
			 if(! medForm.getStatus().equals("")){
				 filter.setStatus(Long.parseLong(medForm.getStatus()));					
			 }		
 	    }

		filter.setUnAssignedOnly(medForm.getUnassigned());
		 SortHelper sort = new SortHelper(IUMConstants.TABLE_MEDICAL_RECORDS);
		 if (sortBy.trim().length() > 0) {
		    sort.setColumn(Integer.parseInt(sortBy));
		    sort.setSortOrder(sortOrder);
		 }		 
		 MedicalLabRecord medRec = new MedicalLabRecord();
		 ArrayList medList = medRec.getMedicalExamRequest(filter, sort);
		 LOGGER.info("searchMedicalRecord end");
		 return medList;
	}
	
	private void notSubmitted(MedicalRecordsForm form, String userId) throws IUMException{
		
		LOGGER.info("notSubmitted start");
		boolean                noErrors   = false;
		String                 pageReturn = "";
		String                 CHECKED    = "checked";		

		String[] refNo   = form.getRefNo();
		String[] medId   = form.getMedicalRecordId();		
		String[] index   = form.getIndexTemp();	
		String[] statArr = form.getMedicalStatus();
		String[] folDate = form.getFollowUpDate();
		String[] clientId= form.getClientId();
		String[] branches= form.getBranches();
        
		MedicalLabRecord medRec     = new MedicalLabRecord();
		ArrayList        medRecList = new ArrayList();
		for (int i=0;i<index.length;i++) {			
			if (index[i] != null) {				
				if (index[i].equals(CHECKED)) {					
					MedicalRecordData data = new MedicalRecordData();
					data.setMedicalRecordId(Long.parseLong(medId[i]));
					data.setReferenceNo(refNo[i]);
					StatusData stat = new StatusData();
					stat.setStatusId(Long.parseLong(statArr[i]));
					data.setStatus(stat);
					data.setUpdatedDate(new Date());
					data.setUpdatedBy(userId);
					data.setFollowUpDate(DateHelper.parse(folDate[i],"ddMMMyyyy"));
					SunLifeOfficeData ofc = new SunLifeOfficeData();
					ofc.setOfficeId(branches[i]);
					data.setBranch(ofc);
					ClientData client = new ClientData();
					client.setClientId(clientId[i]);
					data.setClient(client);
					medRecList.add(data);					
				}
			}
		}
		this.assignClientNo(medRecList, form.getAssignedClientNo());
		medRec.updateMedExamNoSubmission(medRecList);		
		LOGGER.info("notSubmitted end");
	}
	
	private void confirmMedicalExam(MedicalRecordsForm form, String userId) throws Exception {
		
		LOGGER.info("confirmMedicalExam start");
		boolean                noErrors   = false;
		String                 pageReturn = "";
		String                 CHECKED    = "checked";		

		String[] refNo   = form.getRefNo();		
		String[] medId   = form.getMedicalRecordId();
		String[] index   = form.getIndexTemp();	
		String[] statArr = form.getMedicalStatus();
		String[] clientId= form.getClientId();
		String[] frstName= form.getFirstNameArr();
		String[] lastName= form.getLastNameArr();
		String[] branches= form.getBranches();
		String[] created = form.getCreatedDate();		        
		        
		MedicalLabRecord medRec     = new MedicalLabRecord();
		ArrayList        medRecList = new ArrayList();
		for (int i=0;i<index.length;i++) {			
			if (index[i] != null) {				
				if (index[i].equals(CHECKED)) {					
					MedicalRecordData data = new MedicalRecordData();
					data.setMedicalRecordId(Long.parseLong(medId[i]));
					data.setReferenceNo(refNo[i]);
					StatusData stat = new StatusData();
					stat.setStatusId(Long.parseLong(statArr[i]));
					data.setStatus(stat);
					data.setAppointmentDateTime(DateHelper.parse(form.getAppointmentDate() + " " + form.getAppointmentTime(), "ddMMMyyyy hh:mm a"));
					
					ClientData client = new ClientData();
					client.setClientId(clientId[i]);
					client.setGivenName(frstName[i]);
					client.setLastName(lastName[i]);					
					data.setClient(client);
					SunLifeOfficeData ofc = new SunLifeOfficeData();
					ofc.setOfficeId(branches[i]);
					data.setBranch(ofc);					
					data.setUpdatedDate(new Date());
					data.setUpdatedBy(userId);
					data.setCreateDate(DateHelper.parse(created[i],"ddMMMyyyy"));
					medRecList.add(data);					
				}
			}
		}
		this.assignClientNo(medRecList, form.getAssignedClientNo());
		medRec.confirmMedicalExam(medRecList);
		LOGGER.info("confirmMedicalExam end");
	}
	
	private void receiveMedicalExam(MedicalRecordsForm form, String userId) throws Exception {
		
		LOGGER.info("receiveMedicalExam start");
		boolean                noErrors   = false;
		String                 pageReturn = "";
		String                 CHECKED    = "checked";		

		String[] refNo   = form.getRefNo();
		String[] medId   = form.getMedicalRecordId();		
		String[] index   = form.getIndexTemp();	
		String[] statArr = form.getMedicalStatus();
		String[] testId  =  form.getTestId();
		String[] clientId= form.getClientId();
		String[] frstName= form.getFirstNameArr();
		String[] lastName= form.getLastNameArr();
		String[] branches= form.getBranches();	
		String[] created = form.getCreatedDate();
				
		MedicalLabRecord medRec     = new MedicalLabRecord();
		ArrayList        medRecList = new ArrayList();
		for (int i=0;i<refNo.length;i++) {
			if (index[i] != null) {				
				if (index[i].equals(CHECKED)) {
					MedicalRecordData data = new MedicalRecordData();
					data.setMedicalRecordId(Long.parseLong(medId[i]));			
					data.setReferenceNo(refNo[i]);							
					StatusData stat = new StatusData();
					stat.setStatusId(Long.parseLong(statArr[i]));
					data.setStatus(stat);
					data.setConductedDate(DateHelper.parse(form.getConductedDate(), "ddMMMyyyy"));
					SunLifeOfficeData ofc = new SunLifeOfficeData();
					ofc.setOfficeId(branches[i]);
					data.setBranch(ofc);					
					ClientData client = new ClientData();
					client.setClientId(clientId[i]);
					client.setGivenName(frstName[i]);
					client.setLastName(lastName[i]);
					data.setClient(client);
					TestProfileData test = new TestProfileData();
					test.setTestId(Long.parseLong(testId[i]));
					data.setTest(test);
					data.setCreateDate(DateHelper.parse(created[i],"ddMMMyyyy"));					
					data.setUpdatedDate(new Date());
					data.setUpdatedBy(userId);
					medRecList.add(data);					
				}
			}
		}
		this.assignClientNo(medRecList, form.getAssignedClientNo());		
		medRec.receiveMedicalExamResult(medRecList);		
		LOGGER.info("receiveMedicalExam end");
	}
		
	private void cancelMedicalExam(MedicalRecordsForm form, String userId) throws Exception {
		
		LOGGER.info("cancelMedicalExam start");
		boolean                noErrors   = false;
		String                 pageReturn = "";
		String                 CHECKED    = "checked";		

		String[] refNo   = form.getRefNo();		
		String[] medId   = form.getMedicalRecordId();
		String[] index   = form.getIndexTemp();	
		String[] statArr = form.getMedicalStatus();
		String[] clientId= form.getClientId();
		String[] frstName= form.getFirstNameArr();
		String[] lastName= form.getLastNameArr();
		String[] branches= form.getBranches();		
				
		MedicalLabRecord medRec     = new MedicalLabRecord();
		ArrayList        medRecList = new ArrayList();
		if (refNo != null) {
		for (int i=0;i<refNo.length;i++) {
			if (index[i] != null) {				
				if (index[i].equals(CHECKED)) {
					MedicalRecordData data = new MedicalRecordData();
					data.setMedicalRecordId(Long.parseLong(medId[i]));
					data.setReferenceNo(refNo[i]);										
					StatusData stat = new StatusData();
					stat.setStatusId(Long.parseLong(statArr[i]));
					data.setStatus(stat);
					data.setConductedDate(DateHelper.parse(form.getConductedDate(), "ddMMMyyyy"));
					SunLifeOfficeData ofc = new SunLifeOfficeData();
					ofc.setOfficeId(branches[i]);
					data.setBranch(ofc);					
					ClientData client = new ClientData();
					client.setClientId(clientId[i]);
					client.setGivenName(frstName[i]);
					client.setLastName(lastName[i]);
					data.setClient(client);
					data.setUpdatedDate(new Date());
					data.setUpdatedBy(userId);
					medRecList.add(data);					
				}
			}
		}
		}
		this.assignClientNo(medRecList, form.getAssignedClientNo());		
		medRec.cancelMedicalExam(medRecList);
		LOGGER.info("cancelMedicalExam end");
	}//cancelMedicalExam

	private ArrayList formMedicalRecordList(ArrayList medList) {
		
		LOGGER.info("formMedicalRecordList start");
		ArrayList medFormList = new ArrayList();
		Iterator it = medList.iterator();
		while (it.hasNext()) {
			MedicalRecordData med = (MedicalRecordData) it.next();
			MedicalRecordDetailForm mForm = new MedicalRecordDetailForm();
			mForm.setMedicalRecordId(Long.toString(med.getMedicalRecordId()));
			if (med.getAgent() != null) {
				mForm.setAgent(med.getAgent().getUserId());
			}
			if (med.getClient() != null) {
				mForm.setClientNo(med.getClient().getClientId());
				mForm.setFirstName(med.getClient().getGivenName());
				mForm.setLastName(med.getClient().getLastName());
			}
			mForm.setRefNo(med.getReferenceNo());
			if (med.getBranch() != null) {
				mForm.setBranch(med.getBranch().getOfficeId());
			}
			mForm.setDateReceived(DateHelper.format(med.getReceivedDate(),"ddMMMyyyy"));
			mForm.setDateConducted(DateHelper.format(med.getConductedDate(),"ddMMMyyyy"));
			mForm.setLabTestInd(med.getLabTestInd());
			if (med.getLabTestInd() != null) {
				if (med.getLabTestInd().equals("L")) {
					if (med.getLaboratory() != null) {
						mForm.setLabName(med.getLaboratory().getLabName());
					}				  
				}
				else {
					if (med.getExaminer() != null) {
						mForm.setLabName(med.getExaminer().getFirstName() + " " + med.getExaminer().getLastName());
					}
				}
			}
			else {
				if (med.getExaminer() != null) {
					mForm.setLabName(med.getExaminer().getFirstName() + " " + med.getExaminer().getLastName());
				}			
			}
			mForm.setLabTestInd(med.getLabTestInd());			
			if (med.getStatus() != null) {		
				mForm.setStatusId(Long.toString(med.getStatus().getStatusId()));
				mForm.setStatus(med.getStatus().getStatusDesc());  			
			}
			if (med.getTest() != null) {
				mForm.setTestId(Long.toString(med.getTest().getTestId()));
				mForm.setTest(med.getTest().getTestDesc());  			
			}
			mForm.setFollowUpDate(DateHelper.format(med.getFollowUpDate(),"ddMMMyyyy"));
			mForm.setFollowUpNumber(Long.toString(med.getFollowUpNumber()));       
			mForm.setCreatedDate((DateHelper.format(med.getCreateDate(),"ddMMMyyyy")).toUpperCase());
			medFormList.add(mForm);
		}  	 	  	
		LOGGER.info("formMedicalRecordList end");
	  return medFormList;
	}

    private void assignClientNo(ArrayList medicalList, String assignedClientNo) throws IUMException {
    	
    	LOGGER.info("assignClientNo start");
    	MedicalLabRecord medRec = new MedicalLabRecord();
		if (assignedClientNo != null) {
			if (assignedClientNo.trim().length() > 0) {
				medRec.applyClientNumber(medicalList, assignedClientNo);				
			}			
		}
		LOGGER.info("assignClientNo end");
    }

	private String getSortBy(String sortBy) {
		
	   LOGGER.info("getSortBy start");
	   if (sortBy == null) sortBy = "0";
	   else if (sortBy.trim().length() == 0)
		   sortBy = "0";
	   LOGGER.info("getSortBy end");
	   return sortBy;  	
	}
  
	private String getSortOrder(String sortOrder) {
		
      LOGGER.info("getSortOrder start");
	  if (sortOrder == null) sortOrder = IUMConstants.ASCENDING;
	  else {
		  if (sortOrder.trim().length() > 0) {
			  if (sortOrder.equals("a")) {
				  sortOrder = IUMConstants.ASCENDING;
			  }
			  else if (sortOrder.equals("d")) {
				  sortOrder = IUMConstants.DESCENDING;
			  }
		  }
		  else {
			  sortOrder = IUMConstants.ASCENDING;
		  }
	  }
	  LOGGER.info("getSortOrder end");
	   return sortOrder;
	}
    
}


