package com.slocpi.ium.ui.action;

// struts package
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.slocpi.ium.IUMException;
import com.slocpi.ium.data.MedExamFiltersData;
import com.slocpi.ium.data.MedicalRecordData;
import com.slocpi.ium.data.UserAccessData;
import com.slocpi.ium.data.UserData;
import com.slocpi.ium.data.UserProfileData;
import com.slocpi.ium.ui.form.MedicalRecordDetailForm;
import com.slocpi.ium.ui.form.MedicalRecordsForm;
import com.slocpi.ium.ui.util.Page;
import com.slocpi.ium.ui.util.Pagination;
import com.slocpi.ium.ui.util.StateHandler;
import com.slocpi.ium.underwriter.MedicalLabRecord;
import com.slocpi.ium.underwriter.UnderWriterException;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.DateHelper;
import com.slocpi.ium.util.IUMConstants;
import com.slocpi.ium.util.SortHelper;


/**
 * @TODO Class Description ViewMedicalRecordListAction
 * @author: Ingrid Villanueva
 * @version: 1.0
 */
public class ViewMedicalRecordListAction extends IUMAction {
	private static final Logger LOGGER = LoggerFactory.getLogger(ViewMedicalRecordListAction.class);
  /**
   * @TODO method description for execute
   * @param mapping mapping of request to an instance of this class
   * @param form  object
   * @param request request object
   * @param response response object
   * @exception IOException if an input/output error occurs
   * @exception ServletException if a servlet exception occurs
   * @return ActionForward as defined in the mapping parameter.
   */
  public ActionForward handleAction(ActionMapping mapping, 
                                    ActionForm form, 
                                    HttpServletRequest request, 
                                    HttpServletResponse response) throws Exception {
	  
	  
	LOGGER.info("handleAction start");                               	
	String page = "";
	String sortBy     = getSortBy(request.getParameter("sortBy"));		
	String reqPageNum = request.getParameter("pageNo");
	String sortOrder  = getSortOrder(request.getParameter("sortOrder"));

    StateHandler sh = new StateHandler();
    UserData ud = sh.getUserData(request);
    UserProfileData upd = ud.getProfile();    
	int numPages = upd.getRecordsPerView();
	int pageNo = 1;
	String chStatus = "";
	if((String) request.getParameter("chStatus") != null){
		chStatus = (String) request.getParameter("chStatus");		
	}
	request.setAttribute("chStatus", chStatus);
	try {					    	
		
		String userId = upd.getUserId();
		String pageId = IUMConstants.PAGE_MEDRECORD_LIST;
		UserAccessData uad = extractAccessPriveleges(userId, pageId);					
		
		if (uad.getAccessCode().equals("W")) {
			ArrayList a = new ArrayList();		
			MedicalRecordsForm medform = (MedicalRecordsForm) form;
			a = searchMedicalRecord(form, sortBy, sortOrder, chStatus, upd);
		
			if (reqPageNum != null) {
				pageNo = Integer.parseInt(reqPageNum);
			}
		    ArrayList formList = formMedicalRecordList(a);
			Pagination pgn = new Pagination(formList, numPages);
			Page pg = pgn.getPage(pageNo);
	
	        medform.setSortOrder(sortOrder);
	        medform.setSortBy(sortBy);
			medform.setMedicalRecordData(pg.getList());
			medform.setPage(pg);	
			medform.setPageNo(pageNo);
	
			request.setAttribute("medicalRecordsForm", medform);
			request.setAttribute("pageNo", String.valueOf(pageNo));		 
			request.setAttribute("displayPage",pg);
				    
			page = "medicalRecordsListPage";
			
		}
		else {
			page = "errorPage";
		}

	} 
	catch (UnderWriterException e) {
		LOGGER.error(CodeHelper.getStackTrace(e));
		ArrayList   a       = new ArrayList();
		MedicalRecordsForm medform = new MedicalRecordsForm();
		a = searchMedicalRecord(form, sortBy, sortOrder,chStatus, upd);
		ArrayList  formList = formMedicalRecordList(a);
		Pagination pgn = new Pagination(formList, numPages);
		Page pg = pgn.getPage(pageNo);

		medform.setMedicalRecordData(pg.getList());
		medform.setPage(pg);	
		medform.setPageNo(pageNo);

		request.setAttribute("medicalRecordsForm", medform);
		request.setAttribute("pageNo", String.valueOf(pageNo));		 
		request.setAttribute("displayPage",pg);
		
		ActionErrors errors = new ActionErrors();
	    errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.invalidRequirement", e.getMessage()));
	    saveErrors(request, errors);  
	}
	catch(IUMException eIUM) {
		LOGGER.error(CodeHelper.getStackTrace(eIUM));
		ActionErrors errors = new ActionErrors();
		errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.system.exception", eIUM));
		saveErrors(request, errors);						
		page = "errorPage";
	}
	
	LOGGER.info("handleAction end");
	LOGGER.debug("PAGE FORWARD-->" + page);
	return (mapping.findForward(page));
  }// execute


  private String getSortBy(String sortBy) {
	  
	 LOGGER.info("sortBy start");
	  
	 if (sortBy == null) sortBy = "0";
	 else if (sortBy.trim().length() == 0)
	     sortBy = "0";
	 
	 LOGGER.info("sortBy end");
	 return sortBy;  	
  }
  
  private String getSortOrder(String sortOrder) {
	
	LOGGER.info("getSortOrder start");
	if (sortOrder == null) sortOrder = IUMConstants.ASCENDING;
	else {
		if (sortOrder.trim().length() > 0) {
			if (sortOrder.equals("a")) {
				sortOrder = IUMConstants.ASCENDING;
			}
			else if (sortOrder.equals("d")) {
				sortOrder = IUMConstants.DESCENDING;
			}
		}
		else {
			sortOrder = IUMConstants.ASCENDING;
		}
	}
	
	LOGGER.info("getSortOrder end");
  	 return sortOrder;
  }
  
  private ArrayList formMedicalRecordList(ArrayList medList) {
	  
	LOGGER.info("formMedicalRecordList start");
  	ArrayList medFormList = new ArrayList();
  	Iterator it = medList.iterator();
  	while (it.hasNext()) {
  		MedicalRecordData med = (MedicalRecordData) it.next();
  		MedicalRecordDetailForm mForm = new MedicalRecordDetailForm();
  		mForm.setMedicalRecordId(Long.toString(med.getMedicalRecordId()));
  		if (med.getAgent() != null) {
  			mForm.setAgent(med.getAgent().getUserId());
  		}
  		if (med.getClient() != null) {
  			mForm.setClientNo(med.getClient().getClientId());
			mForm.setFirstName(med.getClient().getGivenName());
			mForm.setLastName(med.getClient().getLastName());
  		}
  		mForm.setRefNo(med.getReferenceNo());
  		if (med.getBranch() != null) {
  			mForm.setBranch(med.getBranch().getOfficeId());
  		}
  		mForm.setDateReceived((DateHelper.format(med.getReceivedDate(),"ddMMMyyyy")).toUpperCase());
  		mForm.setDateConducted((DateHelper.format(med.getConductedDate(),"ddMMMyyyy")).toUpperCase());
  		mForm.setLabTestInd(med.getLabTestInd());
		if (med.getLabTestInd() != null) {
			if (med.getLabTestInd().equals("L")) {
				if (med.getLaboratory() != null) {
					mForm.setLabName(med.getLaboratory().getLabName());
				}				  
			}
			else {
				if (med.getExaminer() != null) {
					mForm.setLabName(med.getExaminer().getFirstName() + " " + med.getExaminer().getLastName());
				}				
			}
		}
		else {
			if (med.getExaminer() != null) {
				mForm.setLabName(med.getExaminer().getFirstName() + " " + med.getExaminer().getLastName());
			}			
		}
		mForm.setLabTestInd(med.getLabTestInd());		
		if (med.getStatus() != null) {		
  			mForm.setStatusId(Long.toString(med.getStatus().getStatusId()));
			mForm.setStatus(med.getStatus().getStatusDesc());  			
		}
		if (med.getTest() != null) {
  			mForm.setTestId(Long.toString(med.getTest().getTestId()));
			mForm.setTest(med.getTest().getTestDesc());  			
		}
  		mForm.setFollowUpDate((DateHelper.format(med.getFollowUpDate(),"ddMMMyyyy")).toUpperCase());
  		mForm.setFollowUpNumber(Long.toString(med.getFollowUpNumber()));
  		mForm.setCreatedDate((DateHelper.format(med.getCreateDate(),"ddMMMyyyy")).toUpperCase());      
  		medFormList.add(mForm);
  	}  	 	  	
  	LOGGER.info("formMedicalRecordList end");
  	return medFormList;
  }
  
  private ArrayList searchMedicalRecord(ActionForm form, String sortBy, String sortOrder, String chStatus, UserProfileData upd) throws Exception {
	  
	  LOGGER.info("searchMedicalRecord start");
  	   MedicalRecordsForm medForm = (MedicalRecordsForm) form;
  	   MedExamFiltersData  filter  = new MedExamFiltersData();
  	   filter.setClientNo(medForm.getClientNo());
  	   if (medForm.getDateRequested() != null) {
  	   	   if (medForm.getDateRequested().trim().length() > 0) {
  	   	   	   filter.setDateRequested(DateHelper.parse(medForm.getDateRequested(),"ddMMMyyyy"));
  	   	   }
  	   }
  	   if (medForm.getExaminer() != null) {
  	      if (medForm.getExaminer().length() > 0) {
  	   		 filter.setExaminer(Long.parseLong(medForm.getExaminer()));
  	      }
  	   }
  	   filter.setFirstName(medForm.getFirstName());
   	   if (medForm.getFollowUpEndDate() != null) {
		   if (medForm.getFollowUpEndDate().trim().length() > 0) {  	   
  	          filter.setEndFollowUpDate(DateHelper.parse(medForm.getFollowUpEndDate(),"ddMMMyyyy"));
		   }
   	   }
 	   if (medForm.getFollowUpStartDate() != null) {
		   if (medForm.getFollowUpStartDate().trim().length() > 0) {   	   
  	          filter.setStartFollowUpDate(DateHelper.parse(medForm.getFollowUpStartDate(),"ddMMMyyyy"));
		   }
 	   }
 	   if (medForm.getExaminer() != null) {
  	      if (medForm.getExaminer().length() > 0) {
  	   		 filter.setExaminer(Long.parseLong(medForm.getExaminer()));
  	      }
  	   }
  	   
  	   if (medForm.getLab() != null){
  	   	 if (medForm.getLab().length() > 0){
  	   	 	filter.setLaboratory(Long.parseLong(medForm.getLab()));
  	   	 }
  	   }
  	   
	   if (upd.getUserCode() != null){
 	   		if (upd.getUserType().equals(IUMConstants.USER_TYPE_LABORATORY))
				filter.setLaboratory(Long.parseLong(upd.getUserCode()));
			else if (upd.getUserType().equals(IUMConstants.USER_TYPE_EXAMINER))
				filter.setExaminer(Long.parseLong(upd.getUserCode()));
 	   }

  	   filter.setLastName(medForm.getLastName());
  	   filter.setReferenceNo(medForm.getReferenceNo());
	   if (medForm.getSevenDayMemoDate() != null) {
		   if (medForm.getSevenDayMemoDate().trim().length() > 0) {  	   
  	          filter.setSevenDayMemoDate(DateHelper.parse(medForm.getSevenDayMemoDate(),"ddMMMyyyy"));
		   }
	   }
	   
	   	if (chStatus.equalsIgnoreCase("true")){
			if (medForm.getStatus() != null) {
				if(! medForm.getStatus().equals("")){
					filter.setStatus(Long.parseLong(medForm.getStatus()));
				}
	   	   	}
		
	    }
		else{		
			filter.setStatus(IUMConstants.STATUS_REQUESTED);
		 }  	   

       filter.setUnAssignedOnly(medForm.getUnassigned());
       SortHelper sort = new SortHelper(IUMConstants.TABLE_MEDICAL_RECORDS);
       if (sortBy.trim().length() > 0) {
       	  sort.setColumn(Integer.parseInt(sortBy));
          sort.setSortOrder(sortOrder);
       }
       MedicalLabRecord medRec = new MedicalLabRecord();
       ArrayList medList = medRec.getMedicalExamRequest(filter, sort);
       
       LOGGER.info("searchMedicalRecord end");
       return medList;
  }

    
}

