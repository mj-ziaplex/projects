package com.slocpi.ium.ui.action;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.slocpi.ium.data.AccessData;
import com.slocpi.ium.data.AccessTemplateData;
import com.slocpi.ium.data.AccessTemplateDetailsData;
import com.slocpi.ium.data.UserData;
import com.slocpi.ium.data.UserProfileData;
import com.slocpi.ium.data.util.DataSourceProxy;
import com.slocpi.ium.ui.form.AccessTemplateForm;
import com.slocpi.ium.ui.util.StateHandler;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.DateHelper;
import com.slocpi.ium.util.UserManager;

/**
 * @author jcristobal
 *
 * To change this generated comment edit the template variable "typecomment":
 * Window>Preferences>Java>Templates.
 * To enable and disable the creation of type comments go to
 * Window>Preferences>Java>Code Generation.
 */
public class ViewAccessTemplateDetailAction extends IUMAction {
	private static final Logger LOGGER = LoggerFactory.getLogger(ViewAccessTemplateDetailAction.class);
	private Connection conn = new DataSourceProxy().getConnection();
	/**
	 * @see com.slocpi.ium.ui.action.IUMAction#handleAction(org.apache.struts.action.ActionMapping, org.apache.struts.action.ActionForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	
	/**
	 * This method handles the processes of ViewAccessTemplateDetailAction class. 
	 * It uses the setDisplayAttributes and getTemplateData methods to display the template accesses.
	 * This also handles the saving of changes made on the template accesses thru the updateAccessTemplate method of UserManager.    
	 */
	public ActionForward handleAction(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response)
		throws Exception {
		
		LOGGER.info("handleAction start");
		StateHandler sh = new StateHandler();
		UserData ud = sh.getUserData(request);
		UserProfileData profile = ud.getProfile();
		String userId = profile.getUserId();
		String page = "";
		try {
			String check = "";
			check = (String)request.getParameter("check");
			UserManager userManager = new UserManager();
			AccessTemplateData accessTemplateData = new AccessTemplateData();
			accessTemplateData.setUpdatedBy(userId);
			
			if(request.getParameter("entry") == null){

				AccessTemplateForm accessTemplateForm = new AccessTemplateForm();
				request.setAttribute("accessTemplateForm", accessTemplateForm);	
				request.setAttribute("accessTemplateData", accessTemplateData);
			}
		
			else{
				if(check == null)
					check = " ";
				if(check.equals("submit")){

					AccessTemplateForm accessTemplateForm = (AccessTemplateForm) form;
					accessTemplateData = userManager.getAccessTemplate(new Long(accessTemplateForm.getTemplateId()).intValue());
					accessTemplateData.equals(getTemplateData(form, accessTemplateData.getTemplateDetails()));
					accessTemplateData.setUpdatedBy(userId);
					accessTemplateData.setUpdatedDate(DateHelper.sqlTimestamp(new Date()));
					userManager.updateAccessTemplate(accessTemplateData);
				}

				AccessTemplateForm wFuncForm = (AccessTemplateForm) form;
			
				if(wFuncForm.equals(null)){
					wFuncForm = new AccessTemplateForm();
				}
			
				accessTemplateData = userManager.getAccessTemplate(new Long (wFuncForm.getTemplateId()).longValue());
				accessTemplateData.setUpdatedBy(userId);
				wFuncForm.setAccessCount(userManager.getAccess().size());
				String[] header = new String[wFuncForm.getAccessCount()];
				for(int i = 0;i<wFuncForm.getAccessCount();i++){
					header[i] = ((AccessData)userManager.getAccess().get(i)).getAccessDesc();
				}
									
				wFuncForm.setPageHeader(header);
				AccessTemplateForm accessTemplateForm = new AccessTemplateForm();
					
				accessTemplateForm = setDisplayAttributes(accessTemplateData, wFuncForm);
			
				request.setAttribute("accessTemplateData", accessTemplateData);					
				request.setAttribute("accessTemplateForm", accessTemplateForm);
				
			}		
		  		
			page = "viewAccessTemplateDetailPage";
		  
		} 
		
		catch (Exception e) {			
			ActionErrors errors = new ActionErrors();
	  		errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.system.exception", e.getMessage()));
	  		saveErrors(request, errors);
	  		page = "errorPage"; 
	  		LOGGER.error(CodeHelper.getStackTrace(e));
		}
		
		LOGGER.info("handleAction end");
		LOGGER.debug("PAGE FORWARD-->" + page);
		
		return (mapping.findForward(page));
  }// execute

  
  private AccessTemplateForm setDisplayAttributes(AccessTemplateData accessTemplateData, AccessTemplateForm accessTemplateForm){
	 
	 LOGGER.info("setDisplayAttributes start");
	  try{
	int rowSize = 0;
	ArrayList pageAccess = new ArrayList();
	int size = accessTemplateData.getTemplateDetails().size();
	String pageDesc = null;
	long pageId = 0;
	long prevPageId = 0;
	long currPageId = 0;
	long accessId = 0;
	boolean access = false;
	char checkVal = ' ';
	boolean isLast = false;
			
	char tempChar[] = new char[accessTemplateForm.getAccessCount()];
	
	for(int i=0;i<accessTemplateForm.getAccessCount();i++)
		tempChar[i] = 'd';
						
	accessId = ((AccessTemplateDetailsData) accessTemplateData.getTemplateDetails().get(0)).getPageAccess().getAccessID();
	access = ((AccessTemplateDetailsData) accessTemplateData.getTemplateDetails().get(0)).hasAccess();
	pageId = ((AccessTemplateDetailsData) accessTemplateData.getTemplateDetails().get(0)).getPageAccess().getPageID();
			
	prevPageId = pageId;
	currPageId = pageId;
	rowSize++;

	if(access)
		checkVal = 'y';
	else 
		checkVal = 'n';
			

	tempChar[new Long(accessId).intValue()-1] = checkVal;

	for(int i=1;i<size;i++){
									
		accessId = ((AccessTemplateDetailsData) accessTemplateData.getTemplateDetails().get(i)).getPageAccess().getAccessID();
		access = ((AccessTemplateDetailsData) accessTemplateData.getTemplateDetails().get(i)).hasAccess();
		pageId = ((AccessTemplateDetailsData) accessTemplateData.getTemplateDetails().get(i)).getPageAccess().getPageID();
				
		prevPageId = currPageId;
		currPageId = pageId;
			
		if(access)
			checkVal = 'y';
		else 
			checkVal = 'n';
							
		if(prevPageId == currPageId){
			tempChar[new Long(accessId).intValue()-1] = checkVal;
			isLast = false;
		}
		else{
			String tempStr = new String(tempChar);
					
			pageAccess.add(tempStr);
			rowSize++;
			isLast = true;
			
			for(int k=0;k<accessTemplateForm.getAccessCount();k++)
				tempChar[k] = 'd';
				
			tempChar[new Long(accessId).intValue()-1] = checkVal;
		
		}
		if(i+1 == size){
			String tempStr = new String(tempChar);
			pageAccess.add(tempStr);
		}
	}

			
	String[] pageName = new String[rowSize];
	String[] pageNameId = new String[rowSize];
	int index = 0;
	
	for(int i=0;i<size;i++){
		pageId = ((AccessTemplateDetailsData) accessTemplateData.getTemplateDetails().get(i)).getPageAccess().getPageID();
		pageDesc = ((AccessTemplateDetailsData)(accessTemplateData.getTemplateDetails().get(i))).getPageAccess().getPageDescription();
				
		if(i==0){
			prevPageId = pageId;
			currPageId = pageId;
			pageName[index] = pageDesc;
			pageNameId[index] = new Long(pageId).toString();
	
		}
		else{
			prevPageId = currPageId;
			currPageId = pageId;
			
		}
		if(prevPageId != currPageId){
			index++;
			pageName[index] = pageDesc;
			pageNameId[index] = new Long(pageId).toString();
	
		}
	}
	
	accessTemplateForm.setPageAccess((String[])(pageAccess.toArray(new String[pageAccess.size()])));
	accessTemplateForm.setPageName(pageName);
	accessTemplateForm.setRowSize(rowSize);
	accessTemplateForm.setPageNameId(pageNameId);
	accessTemplateForm.setRoleDesc(accessTemplateData.getRole().getRolesDesc());
	
	  }catch(Exception e){
		  LOGGER.error(CodeHelper.getStackTrace(e));
		  e.printStackTrace();
	  }
	  
	LOGGER.info("setDisplayAttributes end");
	return accessTemplateForm;
	
  	
  }
  
  
  private AccessTemplateData getTemplateData(ActionForm form, ArrayList template) throws NumberFormatException, Exception{
  	
	LOGGER.info("getTemplateData start");  
	String[] resultStr = null;
	int size = 0;
	long tempPageId;
	long tempAccessId;
	long pId;
	long aId;
	char tempAccess = ' ';
	int ctr;
	AccessTemplateForm wForm = (AccessTemplateForm) form;
	AccessTemplateData templateData = new AccessTemplateData();
	UserManager u = new UserManager();
	templateData = u.getAccessTemplate(new Long(wForm.getTemplateId()).longValue());
	boolean found = false;
	  	  	  	
	resultStr = wForm.getPageAccessResult();
	size = resultStr.length;
  				
	for(int i = 0;i<size; i++ ){
				
		resultStr[i].trim();
				
			
		tempPageId = new Long (resultStr[i].substring(0,resultStr[i].indexOf('~'))).longValue();
		tempAccessId = new Long (resultStr[i].substring(resultStr[i].indexOf('~')+1,resultStr[i].indexOf('@'))).longValue();
		tempAccess = resultStr[i].charAt(resultStr[i].length()-1);

		ctr = 0;
		found = false;
			
		while(!found && ctr <template.size()){
			pId = ((AccessTemplateDetailsData)template.get(ctr)).getPageAccess().getPageID();
			aId = ((AccessTemplateDetailsData)template.get(ctr)).getPageAccess().getAccessID();
			if(pId == tempPageId && aId == tempAccessId){
				found = true;
		
				if(tempAccess == 'y')
					((AccessTemplateDetailsData) template.get(ctr)).setAccess(true);
				else 
					((AccessTemplateDetailsData) template.get(ctr)).setAccess(false);
				
				templateData.setTemplateDetails(template);
			}
			ctr++;
		}
	}
	templateData.setTemplateDetails(template);
	
	LOGGER.info("getTemplateData end");
	return templateData;
  	
  }

}
