/*
 * Created on Jan 7, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.ui.action;

import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.slocpi.ium.IUMException;
import com.slocpi.ium.data.UserAccessData;
import com.slocpi.ium.data.UserData;
import com.slocpi.ium.data.UserProfileData;
import com.slocpi.ium.ui.form.PurgeRecordsForm;
import com.slocpi.ium.ui.util.StateHandler;
import com.slocpi.ium.underwriter.ActivityLog;
import com.slocpi.ium.underwriter.AssessmentRequest;
import com.slocpi.ium.underwriter.AuditTrail;
import com.slocpi.ium.underwriter.MedicalLabRecord;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.DateHelper;
import com.slocpi.ium.util.IUMConstants;

/**
 * @author Ingrid Villanueva
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class PurgeRecordsAction extends IUMAction{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(PurgeRecordsAction.class);
	private Date purgeDate;

	public ActionForward handleAction (ActionMapping mapping, 
									  ActionForm form, 
									  HttpServletRequest request,
									  HttpServletResponse response)
									  throws Exception
									  {

		LOGGER.info("handleAction start");
		String page = "purgeStatPage";
		PurgeRecordsForm pForm = (PurgeRecordsForm) form;
		int recCnt = 0;        								
		try {
			StateHandler sh = new StateHandler();
			UserData ud = sh.getUserData(request);
			UserProfileData profile = ud.getProfile();
			String userId = profile.getUserId();
			String pageId = "";
			UserAccessData uad = extractAccessPriveleges(userId, pageId);
		
			if (uad.getAccessCode().equals("W")) {
				purgeDate = new Date();

			    if (pForm.getCriteria().equals(IUMConstants.PURGE_ASSESSMENT_REQUESTS)) {
			    	recCnt = purgeAssessmentRequests(pForm);
			    }
			    else if (pForm.getCriteria().equals(IUMConstants.PURGE_MEDICAL_RECORDS)) {
			    	recCnt = purgeMedicalRecords(pForm);
			    }
			    else if (pForm.getCriteria().equals(IUMConstants.PURGE_AUDIT_TRAIL_LOGS)) {
			    	recCnt = purgeAuditTrailLogs(pForm);
			    }
			  /*  else if (pForm.getCriteria().equals(IUMConstants.PURGE_ACTIVITY_LOGS)) {
			    	recCnt = purgeActivityLogs(pForm);
			    }*/
			    request.setAttribute("mode","purge");
                buildPage(request, pForm, recCnt, true);
				
			}
			else {
				page = "errorPage";
			}
		}
		catch(IUMException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			buildPage(request, pForm, recCnt, false);
			  			
		}
		LOGGER.info("handleAction end");
		LOGGER.debug("PAGE FORWARD-->" + page);
		return mapping.findForward(page);
	}

    private void buildPage(HttpServletRequest request, PurgeRecordsForm form, int cnt, boolean successful) {
    	
    	LOGGER.info("buildPage start");
    	if (successful) {
    		form.setResult(IUMConstants.SUCCESSFUL);
    	}
    	else {
			form.setResult(IUMConstants.FAILED);
    	}
		form.setPurgeDate(DateHelper.format(purgeDate,"ddMMMyyyy").toUpperCase());
    	form.setNumOfRec(Integer.toString(cnt));
        request.setAttribute("purgeRecordsForm", form);		
        LOGGER.info("buildPage end");
    }
    
	private int purgeAssessmentRequests(PurgeRecordsForm form) throws IUMException {
		
		LOGGER.info("purgeAssessmentRequests start");
		AssessmentRequest ar = new AssessmentRequest();
		int cnt = ar.purgeAssessmentRequests(purgeDate, DateHelper.parse(form.getStartDate(),"ddMMMyyyy"), DateHelper.parse(form.getEndDate(),"ddMMMyyyy"));
		LOGGER.info("purgeAssessmentRequests end");
		return cnt;
	}
	
	private int purgeMedicalRecords(PurgeRecordsForm form) throws IUMException {
		
		LOGGER.info("purgeMedicalRecords start");
		MedicalLabRecord med = new MedicalLabRecord();
		int cnt = med.purgeMedicalRecords(purgeDate, DateHelper.parse(form.getStartDate(),"ddMMMyyyy"), DateHelper.parse(form.getEndDate(),"ddMMMyyyy"));
		LOGGER.info("purgeMedicalRecords end");
		return cnt;
	}

	private int purgeAuditTrailLogs(PurgeRecordsForm form) throws IUMException {
		
		LOGGER.info("purgeAuditTrailLogs start");
		AuditTrail at = new AuditTrail();
		int cnt = at.purgeAuditTrails(purgeDate, DateHelper.parse(form.getStartDate(),"ddMMMyyyy"), DateHelper.parse(form.getEndDate(),"ddMMMyyyy"));
		LOGGER.info("purgeAuditTrailLogs end");
		return cnt;
	}

	/*private int purgeActivityLogs(PurgeRecordsForm form) throws IUMException {
		
		LOGGER.info("purgeActivityLogs start");
		ActivityLog al = new ActivityLog();
		int cnt = al.purgeActivityLogs(purgeDate, DateHelper.parse(form.getStartDate(),"ddMMMyyyy"), DateHelper.parse(form.getEndDate(),"ddMMMyyyy"));
		LOGGER.info("purgeActivityLogs end");
		return cnt;
	}*/
	

}


