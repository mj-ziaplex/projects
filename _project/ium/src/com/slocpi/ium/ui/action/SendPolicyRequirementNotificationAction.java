/*
 * Created on Apr 29, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.ui.action;

import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.slocpi.ium.data.UserData;
import com.slocpi.ium.data.UserProfileData;
import com.slocpi.ium.ui.form.PolicyRequirementsForm;
import com.slocpi.ium.ui.util.StateHandler;
import com.slocpi.ium.underwriter.PolicyRequirements;
import com.slocpi.ium.util.CodeHelper;


/**
 * @author Cris
 * 
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class SendPolicyRequirementNotificationAction extends IUMAction {

	private static final Logger LOGGER = LoggerFactory.getLogger(SendPolicyRequirementNotificationAction.class);
	
	public ActionForward handleAction(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		
		LOGGER.info("handleAction start");
		String page = "cdsDetailPage";

		String sessionId = request.getParameter("sessionId");
		request.setAttribute("session_id", sessionId);
		request.setAttribute("sessionId", sessionId);
		
		PolicyRequirementsForm arForm = (PolicyRequirementsForm) form;
		String referenceNumber = arForm.getRefNo();
		
		String lob = request.getParameter("lob") != null ? request.getParameter("lob") : "";
		String currentStatus = request.getParameter("currentStatus") != null ? request.getParameter("currentStatus") : "";
		String arFormRefNo = request.getParameter("arFormRefNo") != null ? request.getParameter("arFormRefNo") : "";
		String arFormOwnerClientNo = request.getParameter("arFormOwnerClientNo") != null ? request.getParameter("arFormOwnerClientNo") : "";
		String arFormInsuredClientNo = request.getParameter("arFormInsuredClientNo") != null ? request.getParameter("arFormInsuredClientNo") : "";
		String branchCode = request.getParameter("branchCode") != null ? request.getParameter("branchCode") : "";
		String agentCode = request.getParameter("agentCode") != null ? request.getParameter("agentCode") : "";
		String assignedTo = request.getParameter("assignedTo") != null ? request.getParameter("assignedTo") : "";
		String sourceSystem = request.getParameter("sourceSystem") != null ? request.getParameter("sourceSystem") : "";
		String EXECUTE_KO = request.getParameter("EXECUTE_KO") != null ? request.getParameter("EXECUTE_KO") : "";
		String sendButtonControl = request.getParameter("sendButtonControl") != null ? request.getParameter("sendButtonControl") : "";
		
		UserData userData = new StateHandler().getUserData(request);
		UserProfileData userLogIn = userData.getProfile();
		String user = userLogIn.getUserId();
		
		PolicyRequirements req = new PolicyRequirements();
		ArrayList listReqIds = req.retrieveAttachmentHelper(referenceNumber);

		if (listReqIds.size() <= 0) {
			LOGGER.debug("listReqIds.size() <= 0");
			constructError("error.submitRequest",
					"There are no requirements for notification", request);
		} else {
			req.sendNotificationOrderReqt(referenceNumber, user);
		}

		HttpSession session = request.getSession();
		if (session.getAttribute("requirementForm") != null)
			session.removeAttribute("requirementForm");

		String savedSection = (request.getParameter("savedSectionReq") != null) ? request
				.getParameter("savedSectionReq").trim()
				: "";
		
		try {
			session = request.getSession();
			if (session == null) {
				LOGGER.debug("request.getSession() is null");
			} else {
				LOGGER.debug("request.getSession() is not null");
				session
						.setAttribute("savedSection_" + sessionId,
								savedSection);
				String temp = (String) session.getAttribute("savedSection_"
						+ sessionId);
				String temp2 = (temp != null) ? temp.trim()
						: "session object savedSection is not saved";
			}
		} catch (Exception e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			
		}
		
		PolicyRequirements polReq = new PolicyRequirements();
		boolean hasOrderReqt = polReq.hasOrderRequirements(arFormRefNo);
		String ordReq = "";
		if(hasOrderReqt){
			ordReq = "YES";
		}
		
		sendButtonControl = "SEND";
		
		request.setAttribute("hasOrderReqt", ordReq);
		request.setAttribute("lob", lob);
		request.setAttribute("currentStatus", currentStatus);
		request.setAttribute("arFormRefNo", arFormRefNo);
		request.setAttribute("arFormOwnerClientNo", arFormOwnerClientNo);
		request.setAttribute("arFormInsuredClientNo", arFormInsuredClientNo);
		request.setAttribute("sessionId", sessionId);
		request.setAttribute("branchCode", branchCode);
		request.setAttribute("agentCode", agentCode);
		request.setAttribute("assignedTo", assignedTo);
		request.setAttribute("sourceSystem", sourceSystem);
		request.setAttribute("EXECUTE_KO", EXECUTE_KO);
		request.setAttribute("sendButtonControl", sendButtonControl);
		
		LOGGER.info("handleAction end");
		LOGGER.debug("PAGE FORWARD-->" + page);

		return mapping.findForward(page);
	}

	private String constructError(String key, String parameter,
			HttpServletRequest request) {
		
		LOGGER.info("constructError start");
		String result = "error";
		ActionErrors error = new ActionErrors();
		error.add(ActionErrors.GLOBAL_ERROR, new ActionError(key, parameter));
		saveErrors(request, error);
		LOGGER.info("constructError end");
		return result;
	}







}
