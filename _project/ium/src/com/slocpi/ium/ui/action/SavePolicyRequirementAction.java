package com.slocpi.ium.ui.action;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import com.slocpi.ium.IUMException;
import com.slocpi.ium.data.AssessmentRequestData;
import com.slocpi.ium.data.LOBData;
import com.slocpi.ium.data.PolicyRequirementsData;
import com.slocpi.ium.data.StatusData;
import com.slocpi.ium.data.SunLifeOfficeData;
import com.slocpi.ium.data.UserAccessData;
import com.slocpi.ium.data.UserData;
import com.slocpi.ium.data.UserProfileData;
import com.slocpi.ium.interfaces.IUMInterfaceException;
import com.slocpi.ium.ui.form.PolicyRequirementsForm;
import com.slocpi.ium.ui.util.StateHandler;
import com.slocpi.ium.underwriter.AssessmentRequest;
import com.slocpi.ium.underwriter.PolicyRequirements;
import com.slocpi.ium.underwriter.UnderWriterException;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.DateHelper;
import com.slocpi.ium.util.IUMConstants;

/**
 * @author Ingrid Villanueva
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 * ***************************************************
 * @author Shellz - Sept 25, 2014
 * @version 1.1
 * TODO -Code clean up of commented codes
 */
public class SavePolicyRequirementAction extends IUMAction{

	private static final Logger LOGGER = LoggerFactory.getLogger(SavePolicyRequirementAction.class);
	
	public ActionForward handleAction (
			final ActionMapping mapping,
			final ActionForm form,
			final HttpServletRequest request,
			final HttpServletResponse response) throws Exception {
		
		LOGGER.info("handleAction start");
		AssessmentRequest assessmentReq = new AssessmentRequest();
		String page = "cdsDetailPage";
		PolicyRequirementsForm arForm = (PolicyRequirementsForm) form;
		String   referenceNumber = arForm.getRefNo();
		String savedSection = (request.getParameter("savedSection") != null) ? request.getParameter("savedSection").trim(): "";
		
		String lob = request.getParameter("lob") != null ? request.getParameter("lob") : "";
		String currentStatus = request.getParameter("currentStatus") != null ? request.getParameter("currentStatus") : "";
		String arFormRefNo = request.getParameter("arFormRefNo") != null ? request.getParameter("arFormRefNo") : "";
		String arFormOwnerClientNo = request.getParameter("arFormOwnerClientNo") != null ? request.getParameter("arFormOwnerClientNo") : "";
		String arFormInsuredClientNo = request.getParameter("arFormInsuredClientNo") != null ? request.getParameter("arFormInsuredClientNo") : "";
		String sessionId = request.getParameter("sessionId") != null ? request.getParameter("sessionId") : "";
		String branchCode = request.getParameter("branchCode") != null ? request.getParameter("branchCode") : "";
		String agentCode = request.getParameter("agentCode") != null ? request.getParameter("agentCode") : "";
		String assignedTo = request.getParameter("assignedTo") != null ? request.getParameter("assignedTo") : "";
		String sourceSystem = request.getParameter("sourceSystem") != null ? request.getParameter("sourceSystem") : "";
		String EXECUTE_KO = request.getParameter("EXECUTE_KO") != null ? request.getParameter("EXECUTE_KO") : "";
		String sendButtonControl = request.getParameter("sendButtonControl") != null ? request.getParameter("sendButtonControl") : "";
		String followUpDateTo = request.getParameter("followUpDateTo") != null ? request.getParameter("followUpDateTo") : "";
		
		followUpDateTo = arForm.getFollowUpDateTo() != null ? arForm.getFollowUpDateTo() : "";
		
		String strNBStrucNotesSettleDate = request.getSession().getAttribute("strNBStrucNotesSettleDate") != null ? 
				request.getSession().getAttribute("strNBStrucNotesSettleDate").toString() : "";
		
		try {
			HttpSession session = request.getSession();
			if (session == null) {
				LOGGER.debug("request.getSession() is null");
			} else {
				LOGGER.debug("request.getSession() is not null");
				session.setAttribute("savedSection", savedSection);
				String temp = (String) session.getAttribute("savedSection");
				String temp2 = (temp != null) ? temp.trim(): "session object savedSection is not saved" ;
			}
		} catch (Exception e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			
		}
		
		arForm.setRequestStatus(request.getParameter("arStatus"));
		try {
			StateHandler sh = new StateHandler();
			UserData ud = sh.getUserData(request);
			UserProfileData profile = ud.getProfile();
			String userId = profile.getUserId();
			String pageId = IUMConstants.PAGE_CDS;
			UserAccessData uad = extractAccessPriveleges(userId, pageId);

			if (uad.getAccessCode().equals("W")) {
				String   statusTo        = arForm.getStatusTo();
        		String   commentTo        = arForm.getCommentTo();
				String   facilitator     = request.getParameter("facilitator");
				if (facilitator == null) facilitator = "";
				String   followUp        = request.getParameter("followUp");
				String   branchId        = request.getParameter("branchCode");
				String   agentId         = request.getParameter("agentCode");
				
				if(statusTo!=null && !statusTo.equals("")){
					
					if (followUp == null) {
						if (statusTo.equals(Long.toString(IUMConstants.STATUS_ORDERED))) {
							
							
							orderRequirement(arForm, referenceNumber, userId, lob, branchId, agentId, arForm.getSourceSystem(), strNBStrucNotesSettleDate);
							page = "cdsDetailPage";
						}
						
						else if (statusTo.equals(Long.toString(IUMConstants.STATUS_RECEIVED_IN_SITE))) {
							receiveRequirement(arForm, referenceNumber, userId, lob, branchId, assignedTo, arForm.getSourceSystem());
						}
						else if (statusTo.equals(Long.toString(IUMConstants.STATUS_REVIEWED_AND_ACCEPTED))){
							acceptRequirement(arForm, referenceNumber, userId, lob, branchId, assignedTo, arForm.getSourceSystem());
						}
						else if (statusTo.equals(Long.toString(IUMConstants.STATUS_REQ_CANCELLED))){
							cancelRequirement(arForm, referenceNumber, userId, lob, branchId, agentId, arForm.getSourceSystem());
						}
						else if (statusTo.equals(Long.toString(IUMConstants.STATUS_WAIVED))){
							waiveRequirement(arForm, referenceNumber, userId, lob, branchId, agentId, arForm.getSourceSystem());
						}
						else if (statusTo.equals(Long.toString(IUMConstants.STATUS_REVIEWED_AND_REJECTED))){
							rejectRequirement(arForm, referenceNumber, userId, lob, branchId, arForm.getSourceSystem());
						}
						else{
							genericChangeStatus(arForm, referenceNumber, userId, lob, branchId, agentId, statusTo, arForm.getSourceSystem());
							
						}
					}
					else {
						followUp(arForm, referenceNumber, userId, lob, branchId, agentId);
					}
				}

				if(!followUpDateTo.equals("")){
					updateFollowUpDate(arForm, referenceNumber, userId, lob, branchId, assignedTo, arForm.getSourceSystem());
				}

				request.setAttribute("lob", lob);
				request.setAttribute("currentStatus", currentStatus);
				request.setAttribute("arFormRefNo", arFormRefNo);
				request.setAttribute("arFormOwnerClientNo", arFormOwnerClientNo);
				request.setAttribute("arFormInsuredClientNo", arFormInsuredClientNo);
				request.setAttribute("sessionId", sessionId);
				request.setAttribute("branchCode", branchCode);
				request.setAttribute("agentCode", agentCode);
				request.setAttribute("assignedTo", assignedTo);
				request.setAttribute("sourceSystem", sourceSystem);
				request.setAttribute("EXECUTE_KO", EXECUTE_KO);
				request.setAttribute("sendButtonControl", sendButtonControl);				

				page = "cdsDetailPage";
				
			}
			else {
				page = "errorPage";
			}

			if(assessmentReq.isReqtCountExceeded(referenceNumber) && page.equals("cdsDetailPage")){
				ActionMessages warning = new ActionMessages();
				warning.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("message.reqtCountExceeded"));
				saveMessages(request, warning);
			}
		}
		catch(UnderWriterException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			page = "cdsDetailPage";
			ActionErrors errors = new ActionErrors();
			String       err    = e.getMessage();
			if (err == null) err = "";
			errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.invalidRequirement", err));
			saveErrors(request, errors);
		}
		catch(IUMException eIUM) {
			LOGGER.error(CodeHelper.getStackTrace(eIUM));
			ActionErrors errors = new ActionErrors();
			errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.system.exception", eIUM.getMessage()));
			saveErrors(request, errors);
			request.setAttribute("errors", eIUM.getMessage());
			page = "errorPage";
		}
		catch (IUMInterfaceException eInterface) {
			LOGGER.error(CodeHelper.getStackTrace(eInterface));
			ActionErrors errors = new ActionErrors();
			errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.system.exception", eInterface.getMessage()));
			saveErrors(request, errors);
			request.setAttribute("errors", eInterface.getMessage());
			page = "errorPage";
		}
		LOGGER.info("handleAction end");
		LOGGER.debug("PAGE FORWARD-->" + page);
		return mapping.findForward(page);
	}

	private Date computeFollowUpDate(long followUpNum) {
		
		LOGGER.info("computeFollowUpDate start");
		Date followUpDate = new Date();
		if (followUpNum > 0) {
			int followUpNumber = new Long(followUpNum).intValue();

			switch(followUpNumber){
				case 0:
					followUpNumber = 0;
					break;
				case 1:
					followUpNumber = 10;
					break;
				case 2:
					followUpNumber = 15;
					break;
				default:
					followUpNumber = 10;
			}

			followUpDate = DateHelper.add(followUpDate, Calendar.DATE, followUpNumber);
		}
		LOGGER.info("computeFollowUpDate end");
		return followUpDate;
	}

	private void updateFollowUpDate(PolicyRequirementsForm arForm, String refNo, String userId, String lob, String branchId, String assignedTo, String sourceSystem) throws Exception {

		LOGGER.info("updateFollowUpDate start");
		String   followUpDateTo  = arForm.getFollowUpDateTo();
		String[] reqId           = arForm.getReqIds();
		String[] index           = arForm.getIndexTemp();
		String[] statusCode		 = arForm.getStatusCodes();
	
		String[] seq             = arForm.getSeqs();
		String[] level           = arForm.getReqLevels();
		String[] reqCodes        = arForm.getReqCodes();

		String                 CHECKED    = "checked";
		ArrayList              listReq    = new ArrayList();
		PolicyRequirementsData data       = null;
		PolicyRequirements req;
		for (int i=0;i<reqId.length;i++) {
			if (index[i] != null) {
				if (index[i].equals(CHECKED)) {
		    		data = new PolicyRequirementsData();
		    		StatusData stat = new StatusData();
					data.setReferenceNumber(refNo);
					data.setRequirementCode(reqCodes[i]);
					if (statusCode[i] != null) {
						if (statusCode[i].trim().length() > 0) {
							stat.setStatusId(Long.parseLong(statusCode[i]));
						}
					}
		    		data.setStatus(stat);
		    		if (reqId[i] != null) {
		    			if (reqId[i].trim().length() > 0) {
							data.setRequirementId(Long.parseLong(reqId[i]));
		    			}
		    		}
		    		data.setUpdatedBy(userId);
		    		data.setUpdateDate(new Date());
		    		if (seq[i] != null) {
		    			if (seq[i].trim().length() > 0) {
							data.setSequenceNumber(Long.parseLong(seq[i]));
		    			}
		    		}
					data.setLevel(level[i]);
					
					try{
						data.setFollowUpDate(DateHelper.parse(followUpDateTo,
						"ddMMMyyyy"));
					
					}catch(Exception e){
						LOGGER.error(CodeHelper.getStackTrace(e));
					}
					
		    		listReq.add(data);
				}
			}
		}
		req  = new PolicyRequirements();

		AssessmentRequestData reqObj = new AssessmentRequestData();
		UserProfileData uData = new UserProfileData();
		uData.setUserId(assignedTo);	
		reqObj.setAssignedTo(uData);		
		LOBData               lobObj = new LOBData();
		lobObj.setLOBCode(lob);
		reqObj.setLob(lobObj);
		SunLifeOfficeData     ofcObj = new SunLifeOfficeData();
		ofcObj.setOfficeId(branchId);
		reqObj.setBranch(ofcObj);
		reqObj.setSourceSystem(sourceSystem);

		req.updateFollowUpDate(listReq, reqObj);
		LOGGER.info("updateFollowUpDate end");
	}//
	
	private void followUp(PolicyRequirementsForm arForm, String refNo, String userId, String lob, String branchId, String agentId) throws Exception {

		LOGGER.info("followUp start");
        String[] reqId           = arForm.getReqIds();
        String[] index           = arForm.getIndexTemp();
		String[] statusCode		 = arForm.getStatusCodes();
		String[] followUpNo      = arForm.getFollowUpNos();
		String[] followUpDate    = arForm.getFollowUpDates();
		String[] seq             = arForm.getSeqs();
		String[] level           = arForm.getReqLevels();
		String[] reqCodes        = arForm.getReqCodes();
		String[] dateSent        = arForm.getDateSents();

		String                 CHECKED    = "checked";
		ArrayList              listReq    = new ArrayList();
		PolicyRequirementsData data       = null;
		
		for (int i=0;i<reqId.length;i++) {
			if (index[i] != null) {
				if (index[i].equals(CHECKED)) {
					data = new PolicyRequirementsData();
					data.setReferenceNumber(refNo);
					data.setRequirementCode(reqCodes[i]);
					if (reqId[i] != null) {
						if (reqId[i].trim().length() > 0) {
							data.setRequirementId(Long.parseLong(reqId[i]));
						}
					}
                    if (followUpNo[i] != null) {
                    	if (followUpNo[i].trim().length() > 0) {
							data.setFollowUpNumber(Long.parseLong(followUpNo[i]));
                    	}
                    }
					data.setUpdatedBy(userId);
					data.setUpdateDate(new Date());
					data.setFollowUpDate(DateHelper.parse(followUpDate[i],"ddMMMyyyy"));
					if (seq[i] != null) {
						if (seq[i].trim().length() > 0) {
							data.setSequenceNumber(Long.parseLong(seq[i]));
						}
					}
					StatusData stat = new StatusData();
					if (statusCode[i] != null) {
						if (statusCode[i].trim().length() > 0) {
							stat.setStatusId(Long.parseLong(statusCode[i]));
						}
					}
					data.setStatus(stat);
					data.setLevel(level[i]);
					data.setDateSent(DateHelper.parse(dateSent[i],"ddMMMyyy"));
					
					listReq.add(data);
					
				}
			}
		}
		PolicyRequirements    req    = new PolicyRequirements();

		AssessmentRequestData reqObj = new AssessmentRequestData();
		LOBData               lobObj = new LOBData();
		lobObj.setLOBCode(lob);
		reqObj.setLob(lobObj);
		SunLifeOfficeData     ofcObj = new SunLifeOfficeData();
		ofcObj.setOfficeId(branchId);
		reqObj.setBranch(ofcObj);
		UserProfileData       usrObj2 = new UserProfileData();
		usrObj2.setUserId(agentId);
		reqObj.setAgent(usrObj2);

		req.sendFollowUpRequirementNotification(listReq, reqObj);
		LOGGER.info("followUp end");
	}

	private void orderRequirement(PolicyRequirementsForm arForm, String refNo, String userId, String lob,
            String branchId, String agentId, String sourceSystem, String strNBStrucNotesSettleDate) throws Exception {
		
		LOGGER.info("orderRequirement start");
		String   commentTo        = arForm.getCommentTo();
	    String[] reqId           = arForm.getReqIds();
	    String[] index           = arForm.getIndexTemp();
	    String[] statusCode		 = arForm.getStatusCodes();
	    String[] followUpNo      = arForm.getFollowUpNos();
	    String[] followUpDate    = arForm.getFollowUpDates();
	    String[] seq             = arForm.getSeqs();
	    String[] level           = arForm.getReqLevels();
	    String[] reqCodes        = arForm.getReqCodes();
	    
	  	String                 CHECKED    = "checked";
		ArrayList              listReq    = new ArrayList();
		PolicyRequirementsData data       = null;

		PolicyRequirements req = new PolicyRequirements(strNBStrucNotesSettleDate);
		
		for (int i=0;i<reqId.length;i++) {
			if (index[i] != null) {
				if (index[i].equals(CHECKED)) {
		    		data = new PolicyRequirementsData();
		    		data.setReferenceNumber(refNo);
		    		data.setRequirementCode(reqCodes[i]);
		    		if (reqId[i] != null) {
		    			if (reqId[i].trim().length() > 0){
							data.setRequirementId(Long.parseLong(reqId[i]));
		    			}
		    		}
		    		if (followUpNo[i] != null) {
		    			if (followUpNo[i].trim().length() > 0) {
                    		data.setFollowUpNumber(Long.parseLong(followUpNo[i]));
		    			}
		    		}
		    		data.setUpdatedBy(userId);
		    		data.setUpdateDate(new Date());
					StatusData stat = new StatusData();
					if (statusCode[i] != null) {
						if (statusCode[i].trim().length() > 0) {
							LOGGER.debug("statusCode[i] "+statusCode[i]);
							
							
							if(statusCode[i].equals("14")){
								Date followUpDate1 = new Date();
								followUpDate1 = DateHelper.add(followUpDate1, Calendar.DATE, 10);
								data.setFollowUpDate(followUpDate1);
							}
							
							stat.setStatusId(Long.parseLong(statusCode[i]));
						}
					}
					data.setStatus(stat);
					if (seq[i] != null) {
						if (seq[i].trim().length() > 0) {
							data.setSequenceNumber(Long.parseLong(seq[i]));
						}
					}

					data.setLevel(level[i]);
					if(commentTo!=null && !commentTo.equals("")){
						data.setCommentTo("Y");
						data.setComments(commentTo);
					}
		    		listReq.add(data);
				}
			}
		}


		AssessmentRequestData reqObj  = new AssessmentRequestData();
		LOBData               lobObj  = new LOBData();
		lobObj.setLOBCode(lob);
		reqObj.setLob(lobObj);
		SunLifeOfficeData     ofcObj  = new SunLifeOfficeData();
		ofcObj.setOfficeId(branchId);
		reqObj.setBranch(ofcObj);
		UserProfileData       usrObj2 = new UserProfileData();
		usrObj2.setUserId(agentId);
		reqObj.setAgent(usrObj2);
        StatusData stat = new StatusData();
        stat.setStatusId(Long.parseLong(arForm.getRequestStatus()));
        reqObj.setStatus(stat);
		UserProfileData       usrObj1 = new UserProfileData();
		
		reqObj.setSourceSystem(sourceSystem);	
        req.orderRequirement(listReq, reqObj, userId);
        LOGGER.info("orderRequirement end");
	}

	private void receiveRequirement(PolicyRequirementsForm arForm, String refNo, String userId, String lob, String branchId, String assignedTo, String sourceSystem) throws Exception {
		
		LOGGER.info("receiveRequirement start");
		String[] reqId           = arForm.getReqIds();
		String[] index           = arForm.getIndexTemp();
		String[] statusCode		 = arForm.getStatusCodes();
	
		String[] seq             = arForm.getSeqs();
		String[] level           = arForm.getReqLevels();
		String[] reqCodes        = arForm.getReqCodes();

		String                 CHECKED    = "checked";
		ArrayList              listReq    = new ArrayList();
		PolicyRequirementsData data       = null;
		PolicyRequirements req;
		for (int i=0;i<reqId.length;i++) {
			if (index[i] != null) {
							
				if (index[i].equals(CHECKED)) {
		    		data = new PolicyRequirementsData();
					data.setReferenceNumber(refNo);
					data.setRequirementCode(reqCodes[i]);
		    		StatusData stat = new StatusData();
		    		if (statusCode[i] != null) {
		    			if (statusCode[i].trim().length() > 0) {
							stat.setStatusId(Long.parseLong(statusCode[i]));
		    			}
		    		}
		    		data.setStatus(stat);
		    		if (reqId[i] != null) {
		    			if (reqId[i].trim().length() > 0) {
							data.setRequirementId(Long.parseLong(reqId[i]));
		    			}
		    		}

		    		data.setUpdatedBy(userId);
		    		data.setUpdateDate(new Date());
		    		if (seq[i] != null) {
		    			if (seq[i].trim().length() > 0) {
							data.setSequenceNumber(Long.parseLong(seq[i]));
		    			}
		    		}
					data.setLevel(level[i]);
		    		listReq.add(data);
				}
			}
		}
		req  = new PolicyRequirements();

		AssessmentRequestData reqObj = new AssessmentRequestData();
		LOBData               lobObj = new LOBData();
		lobObj.setLOBCode(lob);
		reqObj.setLob(lobObj);
		SunLifeOfficeData     ofcObj = new SunLifeOfficeData();
		ofcObj.setOfficeId(branchId);
		reqObj.setBranch(ofcObj);
		UserProfileData uData = new UserProfileData();
		uData.setUserId(assignedTo);
		reqObj.setAssignedTo(uData);
		reqObj.setSourceSystem(sourceSystem);
		req.receiveRequirement(listReq, reqObj);
		LOGGER.info("receiveRequirement end");
		
	}//

	private void acceptRequirement(PolicyRequirementsForm arForm, String refNo, String userId, String lob, String branchId, String assignedTo, String sourceSystem) throws Exception {

		LOGGER.info("acceptRequirement start");
		String[] reqId           = arForm.getReqIds();
		String[] index           = arForm.getIndexTemp();
		String[] statusCode		 = arForm.getStatusCodes();
		
		String[] seq             = arForm.getSeqs();
		String[] level           = arForm.getReqLevels();
		String[] reqCodes        = arForm.getReqCodes();

		String                 CHECKED    = "checked";
		ArrayList              listReq    = new ArrayList();
		PolicyRequirementsData data       = null;
		PolicyRequirements req;
		for (int i=0;i<reqId.length;i++) {
			if (index[i] != null) {
				if (index[i].equals(CHECKED)) {
		    		data = new PolicyRequirementsData();
		    		StatusData stat = new StatusData();
					data.setReferenceNumber(refNo);
					data.setRequirementCode(reqCodes[i]);
					if (statusCode[i] != null) {
						if (statusCode[i].trim().length() > 0) {
							stat.setStatusId(Long.parseLong(statusCode[i]));
						}
					}
		    		data.setStatus(stat);
		    		if (reqId[i] != null) {
		    			if (reqId[i].trim().length() > 0) {
							data.setRequirementId(Long.parseLong(reqId[i]));
		    			}
		    		}
		    		data.setUpdatedBy(userId);
		    		data.setUpdateDate(new Date());
		    		if (seq[i] != null) {
		    			if (seq[i].trim().length() > 0) {
							data.setSequenceNumber(Long.parseLong(seq[i]));
		    			}
		    		}
					data.setLevel(level[i]);
		    		listReq.add(data);
				}
			}
		}
		req  = new PolicyRequirements();

		AssessmentRequestData reqObj = new AssessmentRequestData();
		UserProfileData uData = new UserProfileData();
		uData.setUserId(assignedTo);	
		reqObj.setAssignedTo(uData);		
		LOBData               lobObj = new LOBData();
		lobObj.setLOBCode(lob);
		reqObj.setLob(lobObj);
		SunLifeOfficeData     ofcObj = new SunLifeOfficeData();
		ofcObj.setOfficeId(branchId);
		reqObj.setBranch(ofcObj);
		reqObj.setSourceSystem(sourceSystem);
		req.acceptRequirement(listReq, reqObj);
		
		LOGGER.info("acceptRequirement end");
	}//

	private void cancelRequirement(PolicyRequirementsForm arForm, String refNo, String userId, String lob, String branchId, String agentId, String sourceSystem) throws Exception {

		LOGGER.info("cancelRequirement start");
		String[] reqId           = arForm.getReqIds();
		String[] index           = arForm.getIndexTemp();
		String[] statusCode		 = arForm.getStatusCodes();
		
		String[] seq             = arForm.getSeqs();
		String[] level           = arForm.getReqLevels();
		String[] reqCodes        = arForm.getReqCodes();

		String                 CHECKED    = "checked";
		ArrayList              listReq    = new ArrayList();
		PolicyRequirementsData data       = null;
		PolicyRequirements req;
		for (int i=0;i<reqId.length;i++) {
			if (index[i] != null) {
				if (index[i].equals(CHECKED)) {
					data = new PolicyRequirementsData();
					StatusData stat = new StatusData();
					data.setReferenceNumber(refNo);
					data.setRequirementCode(reqCodes[i]);
					if (statusCode[i] != null) {
						if (statusCode[i].trim().length() > 0) {
							stat.setStatusId(Long.parseLong(statusCode[i]));
						}
					}
					data.setStatus(stat);
					if (reqId[i] != null) {
						if (reqId[i].trim().length() > 0) {
							data.setRequirementId(Long.parseLong(reqId[i]));
						}
					}
					data.setUpdatedBy(userId);
					data.setUpdateDate(new Date());
					if (seq[i] != null) {
						if (seq[i].trim().length() > 0) {
							data.setSequenceNumber(Long.parseLong(seq[i]));
						}
					}
					data.setLevel(level[i]);
					listReq.add(data);
				}
			}
		}
		req  = new PolicyRequirements();

		AssessmentRequestData reqObj = new AssessmentRequestData();
		LOBData               lobObj = new LOBData();
		lobObj.setLOBCode(lob);
		reqObj.setLob(lobObj);
		SunLifeOfficeData     ofcObj = new SunLifeOfficeData();
		ofcObj.setOfficeId(branchId);
		reqObj.setBranch(ofcObj);
		UserProfileData       usrObj2 = new UserProfileData();
		usrObj2.setUserId(agentId);
		reqObj.setAgent(usrObj2);
		reqObj.setSourceSystem(sourceSystem);
		req.cancelRequirement(listReq, reqObj);
		
		LOGGER.info("cancelRequirement end");
	}//

	private void waiveRequirement(PolicyRequirementsForm arForm, String refNo, String userId, String lob, String branchId, String agentId, String sourceSystem) throws Exception {

		LOGGER.info("waiveRequirement start");
		String[] reqId           = arForm.getReqIds();
		String[] index           = arForm.getIndexTemp();
		String[] statusCode		 = arForm.getStatusCodes();
		
		String[] seq             = arForm.getSeqs();
		String[] level           = arForm.getReqLevels();
		String[] reqCodes        = arForm.getReqCodes();

		String                 CHECKED    = "checked";
		ArrayList              listReq    = new ArrayList();
		PolicyRequirementsData data       = null;
		PolicyRequirements req;
		for (int i=0;i<reqId.length;i++) {
			if (index[i] != null) {
				if (index[i].equals(CHECKED)) {
					data = new PolicyRequirementsData();
					StatusData stat = new StatusData();
					data.setReferenceNumber(refNo);
					data.setRequirementCode(reqCodes[i]);
					if (statusCode[i] != null) {
						if (statusCode[i].trim().length() > 0) {
							stat.setStatusId(Long.parseLong(statusCode[i]));
						}
					}
					data.setStatus(stat);
					if (reqId[i] != null) {
						if (reqId[i].trim().length() > 0) {
							data.setRequirementId(Long.parseLong(reqId[i]));
						}
					}
					data.setUpdatedBy(userId);
					data.setUpdateDate(new Date());
					if (seq[i] != null) {
						if (seq[i].trim().length() > 0) {
							data.setSequenceNumber(Long.parseLong(seq[i]));
						}
					}
					data.setLevel(level[i]);
					listReq.add(data);
				}
			}
		}
		req  = new PolicyRequirements();

		AssessmentRequestData reqObj = new AssessmentRequestData();
		LOBData               lobObj = new LOBData();
		lobObj.setLOBCode(lob);
		reqObj.setLob(lobObj);
		SunLifeOfficeData     ofcObj = new SunLifeOfficeData();
		ofcObj.setOfficeId(branchId);
		reqObj.setBranch(ofcObj);
		UserProfileData       usrObj2 = new UserProfileData();
		usrObj2.setUserId(agentId);
		reqObj.setAgent(usrObj2);
		reqObj.setSourceSystem(sourceSystem);
		req.waiveRequirement(listReq, reqObj);
		
		LOGGER.info("waiveRequirement end");
	}//

	private void rejectRequirement(PolicyRequirementsForm arForm, String refNo, String userId, String lob, String branchId, String sourceSystem) throws Exception {

		LOGGER.info("rejectRequirement start");
		String[] reqId           = arForm.getReqIds();
		String[] index           = arForm.getIndexTemp();
		String[] statusCode		 = arForm.getStatusCodes();
		
		String[] seq             = arForm.getSeqs();
		String[] level           = arForm.getReqLevels();
		String[] reqCodes        = arForm.getReqCodes();

		String                 CHECKED    = "checked";
		ArrayList              listReq    = new ArrayList();
		PolicyRequirementsData data       = null;
		PolicyRequirements req;
		for (int i=0;i<reqId.length;i++) {
			if (index[i] != null) {
				if (index[i].equals(CHECKED)) {
					data = new PolicyRequirementsData();
					StatusData stat = new StatusData();
					data.setReferenceNumber(refNo);
					data.setRequirementCode(reqCodes[i]);
					if (statusCode[i] != null) {
						if (statusCode[i].trim().length() > 0) {
							stat.setStatusId(Long.parseLong(statusCode[i]));
						}
					}
					data.setStatus(stat);
					if (reqId[i] != null) {
						if (reqId[i].trim().length() > 0) {
							data.setRequirementId(Long.parseLong(reqId[i]));
						}
					}
					data.setUpdatedBy(userId);
					data.setUpdateDate(new Date());
					if (seq[i] != null) {
						if (seq[i].trim().length() > 0) {
							data.setSequenceNumber(Long.parseLong(seq[i]));
						}
					}
					data.setLevel(level[i]);
					listReq.add(data);
				}
			}
		}
		req  = new PolicyRequirements();

		AssessmentRequestData reqObj = new AssessmentRequestData();
		LOBData               lobObj = new LOBData();
		lobObj.setLOBCode(lob);
		reqObj.setLob(lobObj);
		SunLifeOfficeData     ofcObj = new SunLifeOfficeData();
		ofcObj.setOfficeId(branchId);
		reqObj.setBranch(ofcObj);
		reqObj.setSourceSystem(sourceSystem);

		
		req.rejectRequirements(listReq, reqObj);
		LOGGER.info("rejectRequirement end");
	}

	private void genericChangeStatus(PolicyRequirementsForm arForm, String refNo, String userId, String lob, String branchId, String agentId, String toStatus, String sourceSystem) throws IUMException{

		LOGGER.info("genericChangeStatus start");
		String[] reqId           = arForm.getReqIds();
		String[] index           = arForm.getIndexTemp();
		String[] statusCode		 = arForm.getStatusCodes();
		
		String[] seq             = arForm.getSeqs();
		String[] level           = arForm.getReqLevels();
		String[] reqCodes        = arForm.getReqCodes();

		
		String                 CHECKED    = "checked";
		ArrayList              listReq    = new ArrayList();
		PolicyRequirementsData data       = null;
		PolicyRequirements req;
		for (int i=0;i<reqId.length;i++) {
			if (index[i] != null) {
				if (index[i].equals(CHECKED)) {
					data = new PolicyRequirementsData();
					StatusData stat = new StatusData();
					data.setReferenceNumber(refNo);
					data.setRequirementCode(reqCodes[i]);
					if (statusCode[i] != null) {
						if (statusCode[i].trim().length() > 0) {
							stat.setStatusId(Long.parseLong(statusCode[i]));
						}
					}
					data.setStatus(stat);
					if (reqId[i] != null) {
						if (reqId[i].trim().length() > 0) {
							data.setRequirementId(Long.parseLong(reqId[i]));
						}
					}
					data.setUpdatedBy(userId);
					data.setUpdateDate(new Date());
					if (seq[i] != null) {
						if (seq[i].trim().length() > 0) {
							data.setSequenceNumber(Long.parseLong(seq[i]));
						}
					}
					data.setLevel(level[i]);
					listReq.add(data);
				}
			}
		}
		req  = new PolicyRequirements();

		AssessmentRequestData reqObj = new AssessmentRequestData();
		LOBData               lobObj = new LOBData();
		lobObj.setLOBCode(lob);
		reqObj.setLob(lobObj);
		SunLifeOfficeData     ofcObj = new SunLifeOfficeData();
		ofcObj.setOfficeId(branchId);
		reqObj.setBranch(ofcObj);
		UserProfileData       usrObj2 = new UserProfileData();
		usrObj2.setUserId(agentId);
		reqObj.setAgent(usrObj2);

		reqObj.setReferenceNumber(refNo);
		reqObj.setSourceSystem(sourceSystem);

		long newStatus = Long.parseLong(toStatus);
		req.genericPRChangeStatus(listReq, reqObj, newStatus);
		LOGGER.info("genericChangeStatus end");
	}

}


