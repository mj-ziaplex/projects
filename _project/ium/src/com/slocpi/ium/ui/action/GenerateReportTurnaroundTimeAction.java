/*
 * Created on Mar 7, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.ui.action;

import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.slocpi.ium.data.LOBData;
import com.slocpi.ium.data.TurnAroundTimeParam;
import com.slocpi.ium.data.UserData;
import com.slocpi.ium.data.UserProfileData;
import com.slocpi.ium.reports.TurnaroundTimeReport;
import com.slocpi.ium.ui.form.ReportTurnaroundTimeForm;
import com.slocpi.ium.ui.util.Pagination;
import com.slocpi.ium.ui.util.StateHandler;
import com.slocpi.ium.util.CodeHelper;

/**
 * @author nic.decapia
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class GenerateReportTurnaroundTimeAction extends IUMAction {

	private static final Logger LOGGER = LoggerFactory.getLogger(GenerateReportTurnaroundTimeAction.class);
	/* (non-Javadoc)
	 * @see com.slocpi.ium.ui.action.IUMAction#handleAction(org.apache.struts.action.ActionMapping, org.apache.struts.action.ActionForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	public ActionForward handleAction(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response)
		throws Exception {
		
		LOGGER.info("handleAction start");
		String page = "reportsPage";
		
		try {
			page = "reportsPage";
			
			StateHandler sessionHandler = new StateHandler();
			UserData userData = sessionHandler.getUserData(request);
			UserProfileData userPref = userData.getProfile();
			
			ReportTurnaroundTimeForm frmReport = (ReportTurnaroundTimeForm) form;
			if (frmReport == null) {
				ActionErrors errors = new ActionErrors();
				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.system.exception", "Report Parameters are undefined."));
				saveErrors(request, errors);
				page = "errorPage";
			}

			
			TurnAroundTimeParam filter = extractFilter(frmReport);
			TurnaroundTimeReport rpt = new TurnaroundTimeReport();
			ArrayList rptContent = rpt.generate(filter);
			
			int recPerPage = (userPref.getRecordsPerView()!=0)?userPref.getRecordsPerView():10;
			Pagination pgn = new Pagination(rptContent,recPerPage);
			frmReport.setPage(pgn.getPage(frmReport.getPageNo()));
			request.setAttribute("reportFilter", frmReport);						
		}
		catch (Exception e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			ActionErrors errors = new ActionErrors();
			errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.system.exception", e.getMessage()));
			saveErrors(request, errors);
			page = "errorPage";       	
		} 

		LOGGER.info("handleAction end");
		LOGGER.debug("PAGE FORWARD-->" + page);
		return (mapping.findForward(page));
	}
	
	/**
	 * @param frmReport
	 * @return
	 */
	private TurnAroundTimeParam extractFilter(ReportTurnaroundTimeForm frm) {
	
		LOGGER.info("extractFilter start");
		TurnAroundTimeParam filter = new TurnAroundTimeParam();
		filter.setStartDate(frm.getStartDate());
		filter.setEndDate(frm.getEndDate());
		
		filter.setStartStatus(frm.getStartStatus());
		filter.setEndStatus(frm.getEndStatus());
		
		filter.setUnit(frm.getReportType());		
		
		String lobId = frm.getLineOfBusiness();
		LOBData lob = new LOBData();
		if (lobId.length() != 0) {
			lob.setLOBCode(lobId);
			filter.setLob(lob);
		}
		LOGGER.info("extractFilter end");
		return (filter);		
	}

}
