/*
 * Created on Feb 24, 2004
 * package name = com.slocpi.ium.ui.action
 * file name    = ResetPassword.java
 */
package com.slocpi.ium.ui.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.slocpi.ium.util.CodeHelper;

/**
 * TODO Class Description of ResetPassword.java
 * @author Engel
 * 
 */
public class ResetPasswordAction extends IUMAction {

	private static final Logger LOGGER = LoggerFactory.getLogger(ResetPasswordAction.class);
	/* (non-Javadoc)
	 * @see com.slocpi.ium.ui.action.IUMAction#handleAction(org.apache.struts.action.ActionMapping, org.apache.struts.action.ActionForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	 /**
	  * This method handles the processes of ResetPasswordAction class. It uses the resetPassword method of the UserManager.  
	  */
	public ActionForward handleAction(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response)
		throws Exception {
		
		LOGGER.info("handleAction start");
		String page="userProfileDetailPage";
		try{
		
//			UserManager usrMgr = new UserManager();
//			UserProfileForm upForm = (UserProfileForm) form;
//			UserProfileData upData = new UserProfileData();
//			
//			upData.setUserId(upForm.getUserId());
//			LOGGER.debug("b4 usrMgr.resetPassword(upData)");
//			usrMgr.resetPassword(upData);
//			LOGGER.debug("after usrMgr.resetPassword(upData)");
		}catch (Exception e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			ActionErrors errors = new ActionErrors();
			errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.system.exception", e.getMessage()));
			saveErrors(request, errors); 
			page= "errorPage";
		}
		LOGGER.info("handleAction end");
		LOGGER.debug("PAGE FORWARD-->" + page);
		return (mapping.findForward(page));
	}

}
