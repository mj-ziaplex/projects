/*
 * Created on Jan 12, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.ui.action;

import java.util.Date;
import java.util.HashMap;
import java.util.ResourceBundle;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.slocpi.ium.data.UserData;
import com.slocpi.ium.data.UserProfileData;
import com.slocpi.ium.ui.util.StateHandler;
import com.slocpi.ium.util.IUMConstants;
import com.slocpi.ium.util.UserManager;

/**
 * @author daguila
 * 
 *         To change the template for this generated type comment go to
 *         Window>Preferences>Java>Code Generation>Code and Comments
 */
public class LoginWMSAction extends Action {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(LoginWMSAction.class);
	/*
	 * (non-Javadoc)
	 * 
	 * @see org.apache.struts.action.Action#execute(org.apache.struts.action.
	 * ActionMapping, org.apache.struts.action.ActionForm,
	 * javax.servlet.http.HttpServletRequest,
	 * javax.servlet.http.HttpServletResponse)
	 */
	/**
	 * This method handles the processes of LoginAction class. It checks the
	 * validity of the user Id and password inputted by the user. It also checks
	 * the status of the newly logged-in user.
	 */
	public ActionForward execute(
			ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response)
			throws Exception {

		LOGGER.info("execute start");
		String page = "";
		
		final Date start = new Date();
		Date end;
		long elapse;
		long startTmp = System.currentTimeMillis();
		long endTmp;
		
		try {
			
			request.getSession().removeAttribute("policyNum");
			request.getSession().removeAttribute("policySuffix");
			request.removeAttribute("policyNum");
			request.removeAttribute("policySuffix");
			
	
			
			page = "loginPage";
			String id = cleanString(request.getParameter("id"));
			String tempPage = cleanString(request.getParameter("page"));
			
			StateHandler sh = new StateHandler();
			sh.removeUserData(request);
			sh.removeUserAccess(request);
			
			if (!"".equals(id)) {
				sh.removeNotificationID(request);
				sh.setNotificationID(request, id);
			} else {
				id = sh.getNotificationID(request);
			}
			
			
			if (!"".equals(tempPage)) {
				sh.removeNotificationPage(request);
				sh.setNotificationPage(request, tempPage);
			} else {
				tempPage = sh.getNotificationPage(request);
			}

			
			String task = cleanString(request.getParameter("task"));
			task = "login";

			if (task.equals("login")) {
				
				UserManager um = new UserManager();
				UserData user = null;

				String userId = cleanString(request.getParameter("agentCode"));
				String password = cleanString(request.getParameter("password"));

				String strNBStrucNotesSettleDate = request.getParameter("strNBStrucNotesSettleDate");
				request.getSession().setAttribute("strNBStrucNotesSettleDate", strNBStrucNotesSettleDate);

				String hasIUMRecord = cleanString(request.getParameter("hasIUMRecord"));
				String hasIUMRecordTmp = cleanString((String) request.getSession().getAttribute("hasIUMRecord"));

				if (!"".equals(hasIUMRecordTmp)) {
					hasIUMRecord = hasIUMRecordTmp;
				}
				request.getSession().setAttribute("hasIUMRecord", hasIUMRecord);
				
				end = new Date(); endTmp = System.currentTimeMillis();
				elapse = ((end.getTime() - start.getTime())%(60 * 1000)) / 1000;
				
				if (um.isValidUser(userId, password)) {
					end = new Date(); endTmp = System.currentTimeMillis();
					elapse = ((end.getTime() - start.getTime())%(60 * 1000)) / 1000;
					
					userId = userId.toUpperCase();
					if (request.getSession().getAttribute("userProfile_" + userId) != null) {
						
						user = (UserData) request.getSession().getAttribute("userProfile_" + userId);
					} else {
						
						user = um.retrieveUserDetails(userId, request);
						request.getSession().setAttribute("userProfile_" + userId, user);
						request.setAttribute("userProfile_" + userId, user);
					}
					end = new Date(); endTmp = System.currentTimeMillis();
					elapse = ((end.getTime() - start.getTime())%(60 * 1000)) / 1000;
					
					HashMap userAccess = null;
					UserProfileData usrProf = user.getProfile();
					request.getSession().setAttribute("loginInd", usrProf.getLogin_ind());
					
					if (usrProf.isLock() == true) {
						LOGGER.warn("User Id is locked");
						ActionErrors errors = new ActionErrors();
						errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.login.locked"));
						saveErrors(request, errors);
					} else if (usrProf.isActive() != true) {
						LOGGER.warn("User Id is NOT active");
						ActionErrors errors = new ActionErrors();
						errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.login.deactivated"));
						saveErrors(request, errors);
					} else {

						ResourceBundle rb = ResourceBundle.getBundle(IUMConstants.IUM_WMS_CONFIG);
						String contextPath = rb.getString("wms_context_path");

						if (request.getContextPath().equalsIgnoreCase(contextPath)) {

							userAccess = um.getUserPrivileges(userId, request.getParameter("access") + "");

							sh.setUserData(request, user);
							sh.setUserAccess(request, userAccess);

							page = "cdsDetailPage"; 

							String referenceNumber = cleanString(request.getParameter("policyNumber"));
							String refSuffix = "";
							
							String callRefresh = cleanString(request.getParameter("callRefresh"));
							if ("".equals(callRefresh)) {
								callRefresh = "false";
							}
							request.setAttribute("refreshCDS", callRefresh);
						
							if (referenceNumber.length() > 9) {
								refSuffix = referenceNumber.substring(referenceNumber.length() - 1);
								referenceNumber = referenceNumber.substring(0, referenceNumber.length() - 1);
							}

							request.setAttribute("policyNum", referenceNumber);
							request.setAttribute("policySuffix", refSuffix);
							
							LOGGER.info("policyNum-->" +  referenceNumber);
							LOGGER.info("policySuffix-->" +  refSuffix);
							LOGGER.info("refreshCDS-->" +  callRefresh);
							
							request.getSession().setAttribute("KOReqMedLabDetailForm_" + referenceNumber, null);
							request.getSession().setAttribute("CDSDetailForm_" + referenceNumber, null);
							request.getSession().setAttribute("requestData_" + referenceNumber, null);
						} else {
							page = "closeBrowser";
						}
					}
				} else {

					if (request.getParameter("userID") != null
							&& userId.equals(request.getParameter("userID"))
							&& request.getParameter("attempts") != null) {
						
						request.setAttribute("attempts",
								(new Integer(Integer.parseInt(request.getParameter("attempts")) + 1).toString()));
						
						if (Integer.parseInt((String) request.getAttribute("attempts")) == 3) {
							um.lockUser(userId);
						}
						
					} else {
						request.setAttribute("attempts", "1");
					}
					request.setAttribute("userID", userId);
					ActionErrors errors = new ActionErrors();
					errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.login"));
					saveErrors(request, errors);
					page = "closeBrowser";
				}
			}
			end = new Date(); endTmp = System.currentTimeMillis();
			elapse = ((end.getTime() - start.getTime())%(60 * 1000)) / 1000;

		}
		catch (Exception e) {
			e.printStackTrace();
			ActionErrors errors = new ActionErrors();
			errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.system.exception", e.getMessage()));
			saveErrors(request, errors);
			page = "closeBrowser";
		}
		
		LOGGER.info("execute end");
		LOGGER.debug("PAGE FORWARD-->" + page);
		return (mapping.findForward(page));
	}
	
	// ********************************** //
	// *** METHODS WITH STRING RETURN *** //
	// ********************************** //
		
	// Method to convert null to a blank String or trim if not null
	//
	// @param String str
	public String cleanString(final String str){
			
		
		String toRet = "";
		
		if(str == null){
			return toRet;
		} else {
			toRet = str.trim();
		}
		
		return toRet;
	}
}
