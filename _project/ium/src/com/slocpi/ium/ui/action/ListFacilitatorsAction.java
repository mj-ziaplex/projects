package com.slocpi.ium.ui.action;


import java.io.IOException;
import java.sql.Connection;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.slocpi.ium.data.UserAccessData;
import com.slocpi.ium.data.UserData;
import com.slocpi.ium.data.UserProfileData;
import com.slocpi.ium.data.dao.UserDAO;
import com.slocpi.ium.data.util.DataSourceProxy;
import com.slocpi.ium.ui.util.StateHandler;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.IUMConstants;


/**
 * @TODO Class Description ViewMedicalRecordListAction
 * @author: Ingrid Villanueva
 * @version: 1.0
 */
public class ListFacilitatorsAction extends IUMAction {
	private static final Logger LOGGER = LoggerFactory.getLogger(ListFacilitatorsAction.class);
  /**
   * @TODO method description for execute
   * @param mapping mapping of request to an instance of this class
   * @param form  object
   * @param request request object
   * @param response response object
   * @exception IOException if an input/output error occurs
   * @exception ServletException if a servlet exception occurs
   * @return ActionForward as defined in the mapping parameter.
   */
  public ActionForward handleAction(ActionMapping mapping, 
							   ActionForm form, 
							   HttpServletRequest request, 
							   HttpServletResponse response)
							   throws Exception {
	LOGGER.info("handleAction start");
	String page = "";

	try {
		StateHandler sh = new StateHandler();
		UserData ud = sh.getUserData(request);
		UserProfileData profile = ud.getProfile();
		String userId = profile.getUserId();
		String pageId = "";
		UserAccessData uad = extractAccessPriveleges(userId, pageId);
		
		if (uad.getAccessCode().equals("W")) {
			ArrayList users = new ArrayList();
			
            UserDAO user = new UserDAO();            
            users = user.selectUserProfileByRole(IUMConstants.ROLES_FACILITATOR);
            request.setAttribute("facilitators", users);
            page = "facilitatorsPage";
		}
		else {
			page = "errorPage";
		}
	} 
	catch (Exception e) {
		LOGGER.error(CodeHelper.getStackTrace(e));
	}
	
	LOGGER.info("handleAction end");
	LOGGER.debug("PAGE FORWARD-->" + page);
	return (mapping.findForward(page));
  }

	
    
}

