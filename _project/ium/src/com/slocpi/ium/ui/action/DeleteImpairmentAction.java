/*
 * Created on Feb 16, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.ui.action;

import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.slocpi.ium.data.UserData;
import com.slocpi.ium.data.UserProfileData;
import com.slocpi.ium.ui.form.ImpairmentForm;
import com.slocpi.ium.ui.util.StateHandler;
import com.slocpi.ium.underwriter.Impairment;

/**
 * @author Cris
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class DeleteImpairmentAction extends IUMAction{
	private static final Logger LOGGER = LoggerFactory.getLogger(DeleteImpairmentAction.class);
	
	public ActionForward handleAction(ActionMapping mapping, ActionForm form, HttpServletRequest request,
									  HttpServletResponse response){
		
		LOGGER.info("handleAction start");
		
		String session_id = request.getParameter("session_id").toString();
		request.setAttribute("session_id", session_id);
		
		String page="";
		String code = "";
		
		StateHandler sh = new StateHandler();
		UserData ud = sh.getUserData(request);
		UserProfileData profile = ud.getProfile();
		String userId = profile.getUserId();
		
		ArrayList failedImpairment = new ArrayList();
		ImpairmentForm impForm = (ImpairmentForm)form;
		String [] impairments = impForm.getIdSelected();
		
		Impairment imp = new Impairment();
		for (int i=0; i<impairments.length; i++){
			String impairmentId = (String) impairments[i];
			long id = Long.parseLong(impairmentId);
			try {
				code = imp.deleteImpairment(id, userId);
			} catch (Exception e){
				failedImpairment.add(e.getMessage());
			}
		}
		
		if (failedImpairment.size() >=1){
			StringBuffer errorMessage = new StringBuffer();
			for (int i=0; i < failedImpairment.size(); i++){
				String message = (String) failedImpairment.get(i);
				errorMessage.append(message);
			}
			constructError("error.submitRequest",errorMessage.toString(),request);
			page = "error";
		}
		else {
			page = "viewUWAssessmentDetail";
		}
		LOGGER.info("handleAction end");
		LOGGER.debug("PAGE FORWARD-->" + page);
		return mapping.findForward(page);
	}
	
	private String constructError (String key, String parameter, HttpServletRequest request){
		
		LOGGER.info("constructError start");
		String pageReturn = "requestDetailsPage";
		ActionErrors error = new ActionErrors();
		error.add(ActionErrors.GLOBAL_ERROR, new ActionError(key, parameter));
		saveErrors(request, error);
		LOGGER.info("constructError end");
		return pageReturn;
	}
	
}
