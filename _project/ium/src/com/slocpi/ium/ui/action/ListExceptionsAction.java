/*
 * Created on Mar 5, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.ui.action;

import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.slocpi.ium.IUMException;
import com.slocpi.ium.data.ExceptionData;
import com.slocpi.ium.data.ExceptionDetailsData;
import com.slocpi.ium.data.ExceptionFilterData;
import com.slocpi.ium.data.UserData;
import com.slocpi.ium.data.UserProfileData;
import com.slocpi.ium.ui.form.ExceptionFilterForm;
import com.slocpi.ium.ui.form.ExceptionForm;
import com.slocpi.ium.ui.util.IUMWebException;
import com.slocpi.ium.ui.util.Page;
import com.slocpi.ium.ui.util.Pagination;
import com.slocpi.ium.ui.util.StateHandler;
import com.slocpi.ium.underwriter.AdminExceptionLog;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.DateHelper;

/**
 * @author Cris
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class ListExceptionsAction extends IUMAction{
	private static final Logger LOGGER = LoggerFactory.getLogger(ListExceptionsAction.class);
	
	public ActionForward handleAction(ActionMapping mapping, ActionForm form,
									  HttpServletRequest request, 
									  HttpServletResponse response){
	
	  LOGGER.info("handleAction start");
	  String page = "";
	  
	  StateHandler sh = new StateHandler();
	  UserData ud = sh.getUserData(request);		
	  UserProfileData profile = ud.getProfile();
	  
	  ExceptionFilterForm filter = (ExceptionFilterForm)form;
	  ExceptionFilterData exFilterData = populateFilter(filter);
	  
	  String exceptionId = filter.getIdSelected();
	  String selectedView= request.getParameter("selectedViewDetails");
	
	  AdminExceptionLog ex = new AdminExceptionLog();
	  ArrayList list = new ArrayList();
	  ArrayList details = new ArrayList();
	  
	  String search = request.getParameter("search");
	  try {
			if  (exceptionId != null && selectedView.equals("1")){
				details = ex.retrieveExceptionDetails(Long.parseLong(exceptionId));
				ExceptionFilterForm exForm = new ExceptionFilterForm();
				exForm.setDetails(details);
				ExceptionDetailsData data = (ExceptionDetailsData) details.get(0);
			
				request.setAttribute("viewThisException", exForm);
				request.setAttribute("exceptionDetailData", data);
			}
		
			if (search != null && search.equals("1")){					
				request.setAttribute("searchIsTrue", "1");
				list = populateExceptionForm(ex.retrieveExceptions(exFilterData));
				
			}

			int recPerView = profile.getRecordsPerView();
			if (recPerView == 0){
				recPerView = 1;
			}
			int pageNo = 1;	
			String reqPageNum = request.getParameter("pageNo");
			if ((reqPageNum != null) && (!reqPageNum.equals("")))  {
				pageNo = Integer.parseInt(reqPageNum);
			}
		
			Pagination pgn = new Pagination(list, recPerView);
			Page pg = pgn.getPage(pageNo);
			
			ExceptionFilterForm exceptionFilterForm = new ExceptionFilterForm();			
			exceptionFilterForm.setExceptions(pg.getList());		
			request.setAttribute("exceptionForm", exceptionFilterForm);
			request.setAttribute("filterForm", filter);
			request.setAttribute("page", pg);
			request.setAttribute("pageNo", String.valueOf(pageNo));
	
		page = "exceptionPage";
	} catch (IUMException e) {
		LOGGER.error(CodeHelper.getStackTrace(e));
		page = constructError("error.system.exception", e.getMessage(),request);
	} catch (IUMWebException e) {
		LOGGER.error(CodeHelper.getStackTrace(e));
		page = constructError("error.system.exception", e.getMessage(),request);
	}
	  LOGGER.info("handleAction end");
		LOGGER.debug("PAGE FORWARD-->" + page);
	  return mapping.findForward(page);


					  	
  }
  
  private ExceptionFilterData populateFilter(ExceptionFilterForm form){
	  
	LOGGER.info("populateFilter start");
  	ExceptionFilterData filterData = new ExceptionFilterData();
  	filterData.setStartDate(DateHelper.parse(form.getStartDate(),"ddMMMyyyy"));
  	filterData.setEndDate(DateHelper.parse(form.getEndDate(),"ddMMMyyyy"));
  	
	filterData.setRecordType(form.getRecordType());
	filterData.setInterfaceType(form.getInterfaceSelected());
	LOGGER.info("populateFilter end");
	return filterData;
  }
 
 
  private ArrayList populateExceptionForm(ArrayList list){
	  
	LOGGER.info("populateExceptionForm start");
  	ArrayList results = new ArrayList();
  	for (int i=0; i<list.size(); i++){
  		ExceptionData exData = (ExceptionData) list.get(i);
  		ExceptionForm form = new ExceptionForm();
  		
  		form.setExceptionId(String.valueOf(exData.getExceptionId()));
  		form.setExceptionRefNum(exData.getReferenceNumber());
  		form.setExceptionInterface(exData.getExceptionInterface());
  		form.setExceptionDate(exData.getExceptionDate());
  		form.setRecordType(exData.getRecordType());
  		
  		results.add(form);
  	}
  	LOGGER.info("populateExceptionForm end");
  	return results;
  }
  
  private String constructError (String key, String parameter, HttpServletRequest request){
	  
	      LOGGER.info("constructError start");
		  String page = "errorPage";
		  ActionErrors error = new ActionErrors();
		  error.add(ActionErrors.GLOBAL_ERROR, new ActionError(key, parameter));
		  saveErrors(request, error);
		  LOGGER.info("constructError end");
		  return page;
  }

}
