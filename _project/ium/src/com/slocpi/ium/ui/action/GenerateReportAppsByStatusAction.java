/*
 * Created on Mar 7, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.ui.action;

import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.slocpi.ium.data.ApplicationsByStatusFilterData;
import com.slocpi.ium.data.LOBData;
import com.slocpi.ium.data.SunLifeOfficeData;
import com.slocpi.ium.data.UserData;
import com.slocpi.ium.data.UserProfileData;
import com.slocpi.ium.reports.ApplicationsByStatusReport;
import com.slocpi.ium.ui.form.ReportAppsByStatusForm;
import com.slocpi.ium.ui.util.Pagination;
import com.slocpi.ium.ui.util.StateHandler;
import com.slocpi.ium.util.CodeHelper;

/**
 * @author nic.decapia
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class GenerateReportAppsByStatusAction extends IUMAction {
	private static final Logger LOGGER = LoggerFactory.getLogger(GenerateReportAppsByStatusAction.class);
	
	/* (non-Javadoc)
	 * @see com.slocpi.ium.ui.action.IUMAction#handleAction(org.apache.struts.action.ActionMapping, org.apache.struts.action.ActionForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	public ActionForward handleAction(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response)
		throws Exception {
		
		LOGGER.info("handleAction start");
		String page = "reportsPage";
		
		try {
			page = "reportsPage";
			
			StateHandler sessionHandler = new StateHandler();
			UserData userData = sessionHandler.getUserData(request);
			UserProfileData userPref = userData.getProfile();
			
			ReportAppsByStatusForm frmReport = (ReportAppsByStatusForm) form;
			if (frmReport == null) {
				ActionErrors errors = new ActionErrors();
				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.system.exception", "Report Parameters are undefined."));
				saveErrors(request, errors);
				page = "errorPage";
			}

			
			ApplicationsByStatusFilterData filter = extractFilter(frmReport);
			
			ApplicationsByStatusReport rpt = new ApplicationsByStatusReport();
			ArrayList rptContent = rpt.generateReport(filter);
			
			int recPerPage = (userPref.getRecordsPerView()!=0)?userPref.getRecordsPerView():10;
			Pagination pgn = new Pagination(rptContent,recPerPage);
			frmReport.setPage(pgn.getPage(frmReport.getPageNo()));
			
			request.setAttribute("reportFilter", frmReport);						
						        	
		}
		catch (Exception e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			ActionErrors errors = new ActionErrors();
			errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.system.exception", e.getMessage()));
			saveErrors(request, errors);
			page = "errorPage";       	
		} 

		LOGGER.info("handleAction end");
		LOGGER.debug("PAGE FORWARD-->" + page);
		return (mapping.findForward(page));
	}

	/**
	 * @param frmReport
	 * @return
	 */
	private ApplicationsByStatusFilterData extractFilter(ReportAppsByStatusForm frm) {
		
		LOGGER.info("extractFilter start");
		ApplicationsByStatusFilterData filter = new ApplicationsByStatusFilterData();
		filter.setStartDate(frm.getStartDate());
		filter.setEndDate(frm.getEndDate());
		filter.setFilterType(Integer.valueOf(frm.getReportType()).intValue());		
				
		
		SunLifeOfficeData slo = new SunLifeOfficeData();
		String branchId = frm.getBranch();
		if (branchId.length() != 0) {
			slo.setOfficeId(branchId);
		}
		else {
			slo.setOfficeId(null);
		}
		filter.setBranch(slo);
				
		UserProfileData usr = new UserProfileData();
		String underwriterId = frm.getUnderwriter();
		if (underwriterId.length() != 0) {
			usr.setUserId(underwriterId);
		}
		else {
			usr.setUserId(null);
		}
		filter.setUnderwriter(usr);

		LOBData lob = new LOBData();
		String lobCode = frm.getLob();
		if (lobCode.length() != 0) {
			lob.setLOBCode(lobCode);		
		}
		else {
			lob.setLOBCode(null);
		}
		filter.setLob(lob);
				
		LOGGER.info("extractFilter end");
		return (filter);
	}

}
