
package com.slocpi.ium.ui.action;

import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.slocpi.ium.data.UserData;
import com.slocpi.ium.data.UserProfileData;
import com.slocpi.ium.ui.form.UserPreferenceForm;
import com.slocpi.ium.ui.util.StateHandler;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.workflow.ProcessConfigurator;




public class ConfigureUserPreferenceAction extends IUMAction {

	private static final Logger LOGGER = LoggerFactory.getLogger(ConfigureUserPreferenceAction.class);
	public ActionForward handleAction(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response)
		throws Exception {
		
		LOGGER.info("handleAction start");
		String page="viewUserPreference";
		try{		
			String actionType = request.getParameter("actionType");
			ProcessConfigurator procConf = new ProcessConfigurator();
			StateHandler sh = new StateHandler();		
			UserData ud = sh.getUserData(request);		
			UserProfileData profile = ud.getProfile();
			String userId = profile.getUserId();
			UserPreferenceForm userPrefForm  = (UserPreferenceForm) form;
			if (actionType != null) {
				if (actionType.equals("update")) {
					updateNotificationSettings(userPrefForm,userId);
					userPrefForm.setPreferences(procConf.getEvents(userId));								
					request.setAttribute("userPreference",userPrefForm);
				}			
			} else {
				userPrefForm = new UserPreferenceForm();
				userPrefForm.setPreferences(procConf.getEvents(userId));								
				request.setAttribute("userPreference",userPrefForm);
			}
		}catch (Exception e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			ActionErrors errors = new ActionErrors();
			errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.system.exception", e.getMessage()));
			saveErrors(request, errors); 
			page= "errorPage";
		}		
		LOGGER.info("handleAction end");
		LOGGER.debug("PAGE FORWARD-->" + page);
		return (mapping.findForward(page));
	}

	/**
	 * @param form
	 * @param userId
	 */
	private void updateNotificationSettings(UserPreferenceForm form, String userId) {
		
		LOGGER.info("updateNotificationSettings start");
		ProcessConfigurator procConf = new ProcessConfigurator();
		ArrayList eventList = getEventsList(form);
		procConf.updateUserProcessPreferences(eventList,userId);
		LOGGER.info("updateNotificationSettings end");
	}
	
	private ArrayList getEventsList(UserPreferenceForm userPrefForm) {
		
		LOGGER.info("getEventsList start");
		ArrayList eventsList = new ArrayList();
		String events[] = userPrefForm.getEvents();
		if (events != null && events.length > 0) {
			int len = events.length;
			for (int i=0;i<len;i++) {
				eventsList.add(events[i]);
			}
		}
		LOGGER.info("getEventsList end");
		return eventsList;		
	}
	
	

}
