/*
 * Created on Jan 7, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.ui.action;

import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.slocpi.ium.IUMException;
import com.slocpi.ium.data.UWAssessmentData;
import com.slocpi.ium.data.UserData;
import com.slocpi.ium.data.UserProfileData;
import com.slocpi.ium.ui.form.AssessmentRequestForm;
import com.slocpi.ium.ui.form.RequestDetailForm;
import com.slocpi.ium.ui.util.StateHandler;
import com.slocpi.ium.underwriter.AssessmentRequest;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.DateHelper;

/**
 * @author Cris
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class SaveUWAssessmentAction extends IUMAction{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(SaveUWAssessmentAction.class);
	private static final String YES = "Y";
	private static final String NO = "N";
	
	public ActionForward handleAction (ActionMapping mapping, 
									  ActionForm form, 
									  HttpServletRequest request,
									  HttpServletResponse response){
		
		LOGGER.info("handleAction start");
		String page = "";
		try{
			RequestDetailForm arForm = (RequestDetailForm) form;
			String session_id = request.getParameter("session_id").toString();
			request.setAttribute("session_id", session_id);
			
			String referenceNumber = arForm.getRefNo();
			String assignedTo = arForm.getAssignedTo();			
			String remarks = arForm.getRemarks();
			Date dateForwarded = DateHelper.sqlDate(DateHelper.parse(arForm.getDateForwarded(), "ddMMMyyyy"));
		
			UserData userData = new StateHandler().getUserData(request);
			UserProfileData userLogIn = userData.getProfile();
			String user = userLogIn.getUserId();
			
			String savedSection = (request.getParameter("savedSection") != null) ? request.getParameter("savedSection").trim(): "";
			
			try {
				HttpSession session = request.getSession();
				if (session == null) {
					LOGGER.debug("request.getSession() is null");
				} else {
					LOGGER.debug("request.getSession() is not null");
					session.setAttribute("savedSection", savedSection);
					String temp = (String) session.getAttribute("savedSection");
					String temp2 = (temp != null) ? temp.trim(): "session object savedSection is not saved" ;					
				}
			} catch (Exception e) {
				LOGGER.error(CodeHelper.getStackTrace(e));
			}
			
			saveRemarks(referenceNumber, remarks, user);
			String reqPage = request.getParameter("reqPage");
			
			if (reqPage != null) {
				saveUWAssessment(arForm, user);
				page = "cdsDetailPage";
			}

			request.setAttribute("policyNum", referenceNumber);
		
		} catch (Exception e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			constructError("error.system.exception", e.getMessage(), request);
			page = "errorPage";
		}
		LOGGER.info("handleAction end");
		LOGGER.debug("PAGE FORWARD-->" + page);
		return mapping.findForward(page);
	}
	
		
	private String constructError (String key, String parameter, HttpServletRequest request){
		
		LOGGER.info("constructError start");
		String result = "error";
		ActionErrors error = new ActionErrors();
		error.add(ActionErrors.GLOBAL_ERROR, new ActionError(key, parameter));
		saveErrors(request, error);
		LOGGER.info("constructError end");
		return result;
	}

	private void saveRemarks(String referenceNumber, String remarks, String updatedBy) throws IUMException {
		
		LOGGER.info("saveRemarks start");
		AssessmentRequest ar = new AssessmentRequest();
		ar.setRemarks(referenceNumber, remarks, updatedBy);
		LOGGER.info("saveRemarks end");
	}
	
	private void saveUWAssessment(AssessmentRequestForm arForm, String user) throws IUMException {	
		
		LOGGER.info("saveUWAssessment start");
		UWAssessmentData data = new UWAssessmentData();
		data.setReferenceNumber(arForm.getRefNo());
		data.setRemarks(arForm.getUWRemarks());
		data.setAnalysis(arForm.getUWAnalysis());
		data.setFinalDecision(arForm.getUWFinalDecision());
		
		AssessmentRequest ar = new AssessmentRequest();
		
		data.setUpdatedBy(user);
		data.setUpdateDate(new Date());
		
		data.setPostedBy(user);
		data.setPostedDate(new Date());
		ar.updateUWAssessmentDetails(data);
		
		LOGGER.info("saveUWAssessment end");
	}
	
	
}


