/*
 * Created on Apr 12, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.ui.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.slocpi.ium.IUMException;
import com.slocpi.ium.interfaces.IUMInterfaceException;
import com.slocpi.ium.service.MessageController;
import com.slocpi.ium.ui.form.RequestDetailForm;
import com.slocpi.ium.underwriter.AssessmentRequest;
import com.slocpi.ium.util.CodeHelper;

/**
 * @author Cris
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class AutoSettleAction extends IUMAction{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(AutoSettleAction.class);
	
	public ActionForward handleAction (ActionMapping mapping, ActionForm form, HttpServletRequest request, 
									   HttpServletResponse response){
									   	
		LOGGER.info("handleAction start");
		String page = "";
		RequestDetailForm arForm = (RequestDetailForm)form;
		String referenceNumber = arForm.getRefNo();
		
		MessageController mc = new MessageController();
		AssessmentRequest ar = new AssessmentRequest();
		try {
			mc.processAutoSettleAssessmentRequest(ar.getDetails(referenceNumber));
		} catch (IUMInterfaceException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			constructError("error.system.exception",e.getMessage(),request);
			page = "errorPage";	
		} catch (IUMException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			constructError("error.system.exception",e.getMessage(),request);
			page = "errorPage";	
		}
		page = "requestDetailsPage";
		LOGGER.info("handleAction end");
		LOGGER.debug("PAGE FORWARD-->" + page);
		return mapping.findForward(page);
	}
	

	
	private void constructError (String key, String parameter, HttpServletRequest request){
		
		LOGGER.info("constructError start");
		ActionErrors error = new ActionErrors();
		error.add(ActionErrors.GLOBAL_ERROR, new ActionError(key, parameter));
		saveErrors(request, error);
		LOGGER.info("constructError end");
	}



}
