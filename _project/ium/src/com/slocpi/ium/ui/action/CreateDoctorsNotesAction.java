package com.slocpi.ium.ui.action;


import java.io.IOException;
import java.sql.SQLException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.slocpi.ium.IUMException;
import com.slocpi.ium.data.AssessmentRequestData;
import com.slocpi.ium.data.MedicalNotes;
import com.slocpi.ium.data.UserData;
import com.slocpi.ium.data.UserProfileData;
import com.slocpi.ium.interfaces.notification.IUMNotificationException;
import com.slocpi.ium.ui.form.DiscussionForm;
import com.slocpi.ium.ui.util.Roles;
import com.slocpi.ium.ui.util.StateHandler;
import com.slocpi.ium.underwriter.AssessmentRequest;
import com.slocpi.ium.underwriter.DoctorsNotes;
import com.slocpi.ium.underwriter.UnderWriterException;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.IUMConstants;


/**
 * @TODO Class Description ViewDoctorsNotesAction
 * @author: Ma. Theresa Vicencio
 * @version: 1.0
 */
public class CreateDoctorsNotesAction extends IUMAction {
	private static final Logger LOGGER = LoggerFactory.getLogger(CreateDoctorsNotesAction.class);
  /**
   * @TODO method description for execute
   * @param mapping mapping of request to an instance of this class
   * @param form  object
   * @param request request object
   * @param response response object
   * @exception IOException if an input/output error occurs
   * @exception ServletException if a servlet exception occurs
   * @return ActionForward as defined in the mapping parameter.
   */
  public ActionForward handleAction(ActionMapping mapping, 
							   ActionForm form, 
							   HttpServletRequest request, 
							   HttpServletResponse response)
							   throws Exception {
	  
	LOGGER.info("handleAction start");
	String page = "";
	try {
		
		StateHandler sh = new StateHandler();
		UserData ud = sh.getUserData(request);
		UserProfileData profile = ud.getProfile();
		String userId = profile.getUserId();
		String pageId = "";
	
		DiscussionForm discussionForm = (DiscussionForm)form;

		Roles userRoles = new Roles(); 

		if ((userRoles.getRoles(ud.getRoles(),IUMConstants.ROLES_UNDERWRITER)) ||
			(userRoles.getRoles(ud.getRoles(),IUMConstants.ROLES_UNDERWRITER_SUPERVISOR))) {
			String sendTo = discussionForm.getDoctor();
			saveMedicalNote(discussionForm, userId, sendTo);
		}
		else if ((userRoles.getRoles(ud.getRoles(),IUMConstants.ROLES_MEDICAL_ADMIN)) ||
				 (userRoles.getRoles(ud.getRoles(),IUMConstants.ROLES_MEDICAL_SUPERVISOR)) ||
				 (userRoles.getRoles(ud.getRoles(),IUMConstants.ROLES_MEDICAL_CONSULTANT))) {			
			
			String refNo = discussionForm.getRefNo();
			AssessmentRequest ar = new AssessmentRequest();
			AssessmentRequestData arData = ar.getDetails(refNo);
			String sendTo = arData.getAssignedTo().getUserId();
			saveMedicalNote(discussionForm, userId, sendTo);
		}			
		
		page = "doctorsNotesDetailPage";
		
	} 
	catch (UnderWriterException e) {
		LOGGER.error(CodeHelper.getStackTrace(e));
		ActionErrors errors = new ActionErrors();
		errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.request.generic", e.getMessage()));
		saveErrors(request, errors);
		page = "doctorsNotesDetailPage";
	}
	catch (IUMNotificationException e) {
		LOGGER.error(CodeHelper.getStackTrace(e));
		ActionErrors errors = new ActionErrors();
		errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.system.exception", e.getMessage()));
		saveErrors(request, errors);
		page = "errorPage";
	}
	catch (IUMException e) {
		LOGGER.error(CodeHelper.getStackTrace(e));
		ActionErrors errors = new ActionErrors();
		errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.system.exception", e.getMessage()));
		saveErrors(request, errors);
		page = "errorPage";
	}
	LOGGER.info("handleAction end");
	LOGGER.debug("PAGE FORWARD-->" + page);
	return (mapping.findForward(page));
  }



	private void saveMedicalNote(DiscussionForm form, String postedBy, String sendTo) throws SQLException, IUMNotificationException, Exception{
		
		LOGGER.info("saveMedicalNote start");
		MedicalNotes note  = new MedicalNotes();
		note.setReferenceNumber(form.getRefNo());
		note.setNotes(form.getNotes());
		note.setPostDate(new java.util.Date());
		note.setPostedBy(postedBy);	
		DoctorsNotes docNotes = new DoctorsNotes();
		docNotes.insertDoctorsNote(note, sendTo);
		LOGGER.info("saveMedicalNote end");	
	}
  
}

