/*
 * Created on Mar 9, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.ui.action;

import java.sql.Timestamp;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.slocpi.ium.IUMException;
import com.slocpi.ium.data.JobScheduleData;
import com.slocpi.ium.interfaces.mib.MIBController;
import com.slocpi.ium.underwriter.Reference;
import com.slocpi.ium.util.CodeHelper;

/**
 * @author Cris
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class DisplayMIBScheduleAction extends IUMAction{
	private static final Logger LOGGER = LoggerFactory.getLogger(DisplayMIBScheduleAction.class);
	
	public ActionForward handleAction(ActionMapping mapping, ActionForm form,
									  HttpServletRequest request, HttpServletResponse response){
		
		LOGGER.info("handleAction start");
		String page = "";
		
		try {
			Reference ref = new Reference();
			JobScheduleData jobSched = ref.retrieveMIBSchedule();
			String schedule = null;
			String description = null;
			if (jobSched != null){
				Timestamp timeStamp = jobSched.getTime();
				if (timeStamp != null){
					schedule = getTime(timeStamp.getHours());
					String zero = "0";
					if (!schedule.startsWith("12")){
						schedule = zero.concat(schedule);
					}
				}
				description = jobSched.getDescription();
			}
			else{
				schedule = "";
				description = "";
			}
			
			String functionSelected = request.getParameter("functionSelected");
			if (functionSelected != null && functionSelected.equals("export")){
				
				MIBController mib = new MIBController();
				mib.export();
			}
						

			request.setAttribute("schedule",schedule);
			request.setAttribute("description", description);
			page = "mibSchedulePage";
		} catch (IUMException e){
			LOGGER.error(CodeHelper.getStackTrace(e));
			constructError("error.system.exception",e.getMessage(),request);
			page = "errorPage";
		}
		
		LOGGER.info("handleAction end");
		LOGGER.debug("PAGE FORWARD-->" + page);
		return mapping.findForward(page);
	}
	

	private void constructError (String key, String parameter, HttpServletRequest request){
		LOGGER.info("constructError start");
		ActionErrors error = new ActionErrors();
		error.add(ActionErrors.GLOBAL_ERROR, new ActionError(key, parameter));
		saveErrors(request, error);
		LOGGER.info("constructError end");
	}
	
	private String getTime(long input){
		
		LOGGER.info("getTime start");
		String day = "AM";
		long hour = input;
		if (input > 12){
			hour -= 12;
		}
		if (input >= 12){
			day = "PM";
		}
			
		if (input == 0){
			hour += 12;
		}
		String time = String.valueOf(hour).concat(":00 " + day);
		LOGGER.info("getTime end");
		return time;
	}	
	

}
