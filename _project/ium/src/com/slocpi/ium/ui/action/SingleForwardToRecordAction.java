/*
 * Created on Apr 20, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.ui.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.slocpi.ium.IUMException;
import com.slocpi.ium.data.UserData;
import com.slocpi.ium.data.UserProfileData;
import com.slocpi.ium.ui.form.RequestDetailForm;
import com.slocpi.ium.ui.util.StateHandler;
import com.slocpi.ium.underwriter.AssessmentRequest;
import com.slocpi.ium.util.CodeHelper;

/**
 * @author Cris
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class SingleForwardToRecordAction extends IUMAction{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(SingleForwardToRecordAction.class);
	public ActionForward handleAction (ActionMapping mapping, 
											  ActionForm form, 
											  HttpServletRequest request,
											  HttpServletResponse response){
		
		LOGGER.info("handleAction start");
		String page = "requestListPage";
		
		RequestDetailForm arForm = (RequestDetailForm) form;
		String referenceNumber = arForm.getRefNo();
		
		UserData userData = new StateHandler().getUserData(request);
		UserProfileData userLogIn = userData.getProfile();
		String user = userLogIn.getUserId(); 
				
		AssessmentRequest ar = new AssessmentRequest();
		try {
			ar.forwardToRecords(referenceNumber, user);
		} catch (IUMException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			constructError("error.submitRequest",e.getMessage(),request);
		}
		LOGGER.info("handleAction end");
		LOGGER.debug("PAGE FORWARD-->" + page);
		return mapping.findForward(page);
	}
	
	
	private String constructError (String key, String parameter, HttpServletRequest request){
		
		LOGGER.info("constructError start");
		String result = "error";
		ActionErrors error = new ActionErrors();
		error.add(ActionErrors.GLOBAL_ERROR, new ActionError(key, parameter));
		saveErrors(request, error);
		LOGGER.info("constructError end");
		return result;
	}

}
