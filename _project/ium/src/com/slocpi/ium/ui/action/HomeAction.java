/*
 * Created on Jan 13, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.ui.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.slocpi.ium.data.UserData;
import com.slocpi.ium.data.UserProfileData;
import com.slocpi.ium.ui.util.StateHandler;
import com.slocpi.ium.underwriter.AssessmentRequest;
import com.slocpi.ium.underwriter.User;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.UserManager;

/**
 * @author daguila
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class HomeAction extends IUMAction {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(HomeAction.class);
	/* (non-Javadoc)
	 * @see com.slocpi.ium.ui.action.IUMAction#handleAction(org.apache.struts.action.ActionMapping, org.apache.struts.action.ActionForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	 /**
	  * This method handles the processes of the HomeAction class. It displays the different attributes of the user. 
	  */
	public ActionForward handleAction(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response)
		throws Exception {
		
		LOGGER.info("handleAction start");
	  ActionForward af = mapping.findForward("homePage");			
      try {			
      
		StateHandler sh = new StateHandler();
		UserData ud = sh.getUserData(request);
		AssessmentRequest ar = new AssessmentRequest();
		UserManager userManager = new UserManager();
		String notificationValue = " ";
		String numRecords = " ";

		if(!(request.getParameter("notificationInd") == null)){
			notificationValue = request.getParameter("notificationInd");
			numRecords = request.getParameter("recordsPerView");
		}
	

		User user = new User();
		int noOfPendingAR = 0;
		int noOfPendingMR = 0;

		noOfPendingAR = user.getPendingStatAR(ud.getProfile());
		noOfPendingMR = user.getPendingStatMR(ud.getProfile());

		if(! (numRecords == " ")){
			((UserProfileData)ud.getProfile()).setRecordsPerView(Integer.parseInt(numRecords));
		}
		
		if(notificationValue.toLowerCase().compareTo("true") == 0){
			((UserProfileData)ud.getProfile()).setNotificationInd(true);
		}
		else if(notificationValue.toLowerCase().compareTo("false") == 0){
			((UserProfileData)ud.getProfile()).setNotificationInd(false);
		}
		
		userManager.updateUserProfileData(ud.getProfile());
		request.setAttribute("userProfile",ud.getProfile());
		request.setAttribute("noOfPendingAR", Integer.toString(noOfPendingAR));
		request.setAttribute("noOfPendingMR", Integer.toString(noOfPendingMR));		
      }
      catch (Exception e) {
    	 LOGGER.error(CodeHelper.getStackTrace(e));
        ActionErrors errors = new ActionErrors();
		errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.system.exception", e.getMessage()));
		saveErrors(request, errors); 
		LOGGER.info("handleAction end");
		LOGGER.debug("page forward-->errorPage");
		return (mapping.findForward("errorPage")); 
      }
     
      LOGGER.info("handleAction end");
      LOGGER.debug("page forward-->homePage");
	  return af;
	}

}
