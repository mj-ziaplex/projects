/*
 * Created on Feb 11, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.ui.action;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.slocpi.ium.data.AccessData;
import com.slocpi.ium.data.UserAccessData;
import com.slocpi.ium.data.UserData;
import com.slocpi.ium.data.UserProfileData;
import com.slocpi.ium.data.util.DataSourceProxy;
import com.slocpi.ium.ui.form.UserAccessForm;
import com.slocpi.ium.ui.util.StateHandler;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.DateHelper;
import com.slocpi.ium.util.UserManager;
/**
 * @author Abie
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class ViewUserAccessDetailAction extends IUMAction{
	private static final Logger LOGGER = LoggerFactory.getLogger(ViewUserAccessDetailAction.class);
	private Connection conn = new DataSourceProxy().getConnection();
		/**
		 * @see com.slocpi.ium.ui.action.IUMAction#handleAction(org.apache.struts.action.ActionMapping, org.apache.struts.action.ActionForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
		 */
		/**
		 * This method handles the processes of ViewUserAccessDetailAction class.
		 * It uses the setDisplayAttributes and getUserAccess methods and applyTemplate method of UserManager to display the user accesses.
		 * This also handles the saving of changes made on the user accesses thru the saveUserAccess method of UserManager.     
		 */
		public ActionForward handleAction(
			ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response)
			throws Exception {
			
			LOGGER.info("handleAction start");    
			StateHandler sh = new StateHandler();
			UserData ud = sh.getUserData(request);
			UserProfileData profile = ud.getProfile();
			String userId = profile.getUserId();
			String page = "";
			
			ArrayList list = new ArrayList();
			String check =(String)request.getParameter("check");
			UserManager userManager = new UserManager();
			UserData userData = new UserData();
			UserAccessForm userAccessForm = (UserAccessForm) form;
			String userCode = userAccessForm.getUserId();
			
			try {
				
				if(check == null)
					check = " ";
				if(check.equals("submit")){
					userAccessForm = (UserAccessForm) form;
					userData.setAccess(userManager.getUserAccess(userCode));
					list = getUserAccess(form, userData.getAccess(),userId, userCode);
					userManager.saveUserAccess(list);
						
				}

				UserAccessForm uAccessForm = (UserAccessForm) form;
				UserData uData = new UserData();

				if(uAccessForm.equals(null)){
					uAccessForm = new UserAccessForm();

				}

				if((!check.equals("submit") && request.getParameter("entry") != null))
					userManager.applyTemplate(userCode, new Long (uAccessForm.getTemplateId()).longValue(),userId);
																		
				uData.setAccess(userManager.getUserAccess(userCode));

				if(request.getParameter("entry")==null && uData.getAccess().size() >0){
					userAccessForm.setAccessCount(userManager.getAccess().size());
					String[] header = new String[userAccessForm.getAccessCount()];
					for(int i = 0;i<userAccessForm.getAccessCount();i++)
						header[i] = ((AccessData)userManager.getAccess().get(i)).getAccessDesc();
					userAccessForm.setPageHeader(header);
					userAccessForm = setDisplayAttributes(uData, userAccessForm);
					

				}
				else if(request.getParameter("entry")!= null){
				
					uAccessForm.setAccessCount(userManager.getAccess().size());
					String[] header = new String[uAccessForm.getAccessCount()];
					for(int i = 0;i<uAccessForm.getAccessCount();i++)
						header[i] = ((AccessData)userManager.getAccess().get(i)).getAccessDesc();
					uAccessForm.setPageHeader(header);
					userAccessForm = setDisplayAttributes(uData, uAccessForm);

				}
				
				request.setAttribute("userAccessForm", userAccessForm);	
  				request.setAttribute("userData", userData);
				page = "viewUserAccessDetailPage";
		  
			}
		
			catch (Exception e) {
				LOGGER.error(CodeHelper.getStackTrace(e));
				ActionErrors errors = new ActionErrors();
				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.system.exception", e.getMessage()));
				page = "errorPage"; 
			}
		
			
			LOGGER.info("handleAction end");
			LOGGER.debug("PAGE FORWARD-->" + page);
			return (mapping.findForward(page));
	  }// execute

	  
	  private UserAccessForm setDisplayAttributes(UserData uData, UserAccessForm uForm){
		
		LOGGER.info("setDisplayAttributes start");
		int rowSize = 0;
		ArrayList pageAccess = new ArrayList();
		int size = uData.getAccess().size();
		String pageDesc = null;
		long pageId = 0;
		long prevPageId = 0;
		long currPageId = 0;
		long accessId = 0;
		boolean access = false;
		char checkVal = ' ';
		boolean isLast = false;
		
		char tempChar[] = new char[uForm.getAccessCount()];
	
		for(int i=0;i<uForm.getAccessCount();i++)
			tempChar[i] = 'd';
				
		accessId = ((UserAccessData)uData.getAccess().get(0)).getAccessID();
		access = ((UserAccessData)uData.getAccess().get(0)).hasAccess();
		pageId = ((UserAccessData)uData.getAccess().get(0)).getPageID();
			
		prevPageId = pageId;
		currPageId = pageId;
		rowSize++;

		if(access)
			checkVal = 'y';
		else 
			checkVal = 'n';

		tempChar[new Long(accessId).intValue()-1] = checkVal;

		for(int i=1;i<size;i++){
									
			accessId = ((UserAccessData)uData.getAccess().get(i)).getAccessID();
			access = ((UserAccessData)uData.getAccess().get(i)).hasAccess();
			pageId = ((UserAccessData)uData.getAccess().get(i)).getPageID();
				
			prevPageId = currPageId;
			currPageId = pageId;
			
			if(access)
				checkVal = 'y';
			else 
				checkVal = 'n';
							
			if(prevPageId == currPageId){
				tempChar[new Long(accessId).intValue()-1] = checkVal;
				isLast = false;
			}
			else{
				String tempStr = new String(tempChar);
				pageAccess.add(tempStr);
				rowSize++;
				isLast = true;
			
				for(int k=0;k<uForm.getAccessCount();k++)
					tempChar[k] = 'd';
				
				tempChar[new Long(accessId).intValue()-1] = checkVal;
		
			}
			if(i+1 == size){
				String tempStr = new String(tempChar);
				pageAccess.add(tempStr);
			}
		}

		String[] pageName = new String[rowSize];
		String[] pageNameId = new String[rowSize];
		int index = 0;
	
			
		for(int i=0;i<size;i++){
			pageId = ((UserAccessData)uData.getAccess().get(i)).getPageID();
			pageDesc = ((UserAccessData)uData.getAccess().get(i)).getPageDesc();
				
			if(i==0){
				prevPageId = pageId;
				currPageId = pageId;
				pageName[index] = pageDesc;
				pageNameId[index] = new Long(pageId).toString();
	
			}
			else{
				prevPageId = currPageId;
				currPageId = pageId;
			
			}
			if(prevPageId != currPageId){
				index++;
				pageName[index] = pageDesc;
				pageNameId[index] = new Long(pageId).toString();
	
			}
		}

		uForm.setPageAccess((String[])(pageAccess.toArray(new String[pageAccess.size()])));
		uForm.setPageName(pageName);
		uForm.setRowSize(rowSize);
		uForm.setPageId(pageNameId);
		LOGGER.info("setDisplayAttributes end");
		
		return uForm;
	  }
  
  
	  private ArrayList getUserAccess(ActionForm form, ArrayList template, String userId, String userCode) throws NumberFormatException, Exception{
  	
		LOGGER.info("getUserAccess start");
		String[] resultStr = null;
		int size = 0;
		long tempPageId;
		long tempAccessId;
		long pId;
		long aId;
		char tempAccess = ' ';
		int ctr;
		UserAccessForm uForm = (UserAccessForm) form;
		boolean found = false;
	  	  	  	
		resultStr = uForm.getPageAccessResult();
		size = resultStr.length;
		for(int i = 0;i<size; i++ ){
				
			resultStr[i].trim();
							
			tempPageId = new Long (resultStr[i].substring(0,resultStr[i].indexOf('~'))).longValue();
			tempAccessId = new Long (resultStr[i].substring(resultStr[i].indexOf('~')+1,resultStr[i].indexOf('@'))).longValue();
			tempAccess = resultStr[i].charAt(resultStr[i].length()-1);

			ctr = 0;
			found = false;
			
			while(!found && ctr <template.size()){
	
				pId = ((UserAccessData)template.get(ctr)).getPageID();
				aId = ((UserAccessData)template.get(ctr)).getAccessID();
				
				if(pId == tempPageId && aId == tempAccessId){
					found = true;
		
					if(tempAccess == 'y')
						((UserAccessData)template.get(ctr)).setAccess(true);
					else 
						((UserAccessData)template.get(ctr)).setAccess(false);
					
					((UserAccessData)template.get(ctr)).setUpdatedDate(DateHelper.sqlTimestamp(new Date()));
					((UserAccessData)template.get(ctr)).setUpdatedBy(userId);
					((UserAccessData)template.get(ctr)).setUserID(userCode);
		
				}
						
				ctr++;
			}
		}
		LOGGER.info("getUserAccess end");
		return template;
 	
	  }

}

