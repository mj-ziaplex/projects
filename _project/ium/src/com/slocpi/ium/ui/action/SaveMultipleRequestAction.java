/*
 * Created on Jan 9, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.ui.action;

import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.slocpi.ium.data.UserData;
import com.slocpi.ium.data.UserProfileData;
import com.slocpi.ium.ui.form.RequestFilterForm;
import com.slocpi.ium.ui.form.UserLoadListForm;
import com.slocpi.ium.ui.util.StateHandler;
import com.slocpi.ium.underwriter.AssessmentRequest;
import com.slocpi.ium.underwriter.UnderWriterException;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.IUMConstants;

/**
 * @author Cris
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class SaveMultipleRequestAction extends IUMAction{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(SaveMultipleRequestAction.class);
	
	public ActionForward handleAction (ActionMapping mapping, 
								  ActionForm form, 
								  HttpServletRequest request,
								  HttpServletResponse response){
		
		LOGGER.info("handleAction start");
		String page = "";
		String assignedTo = "";
				
		RequestFilterForm rlForm = (RequestFilterForm) form;
	
		String [] requests = rlForm.getCheckedRefNum();
		long status = Long.parseLong(rlForm.getChangeStatusTo());
		int numberOfRequest = requests.length;
		assignedTo = rlForm.getChangeAssignTo();
		
		UserData userData = new StateHandler().getUserData(request);
		UserProfileData userLogIn = userData.getProfile();
		String user = userLogIn.getUserId();  
		try {
			page = submitRequest(status,numberOfRequest,requests,user,assignedTo,request);
		} catch (Exception e){
			constructError("error.system.exception", e.getMessage(), request);
			page = "errorPage";
		} finally {
			rlForm.setAssignedTo(user);
			ViewAssessmentRequestListAction vl = new ViewAssessmentRequestListAction();
			try {
				vl.handleAction(mapping, rlForm, request, response);
				
				// Added by BJ "Bujo" Taduran for requests.jsp
				ViewAssessmentRequestListAction uwLoad = new ViewAssessmentRequestListAction();
				UserLoadListForm userLoadListForm = uwLoad.ListUnderwriterLoad();
				request.setAttribute("userLoadListForm",userLoadListForm);
				
			} catch (Exception e1) {
				LOGGER.error(CodeHelper.getStackTrace(e1));
			}
			LOGGER.info("handleAction end");
			LOGGER.debug("PAGE FORWARD-->" + page);
		}
		return mapping.findForward(page);
	}
	
	private String submitRequest(long status, int numberOfRequest,String [] requests,String user, String assignedTo,HttpServletRequest request){
		
		LOGGER.info("submitRequest start");
		ArrayList failedRequest = new ArrayList();
		
		AssessmentRequest ar = new AssessmentRequest();
		
		String failedRequestList = new String();
		StringBuffer sb = new StringBuffer();
		
		String page="";
		
		if (status == IUMConstants.STATUS_FOR_ASSESSMENT){
			for (int i=0; i<numberOfRequest; i++){
				String referenceNumber = requests[i];
				try{
					ar.submitToUnderwriter(referenceNumber, user);
				} catch(Exception e){
					LOGGER.error(CodeHelper.getStackTrace(e));
					failedRequest.add(referenceNumber + ": " +  e.getMessage() + "<br>&nbsp;");
				}
			}
		} else if (status == IUMConstants.STATUS_FOR_TRANSMITTAL_USD){
			for (int i=0; i<numberOfRequest; i++){
				String referenceNumber = requests[i];
				try{
					ar.transmit(referenceNumber,assignedTo,user);
				} catch (Exception e){
					LOGGER.error(CodeHelper.getStackTrace(e));
					failedRequest.add(referenceNumber + ": " +  e.getMessage() + "<br>&nbsp;");
				}
			}
		} else if (status == IUMConstants.STATUS_FOR_FACILITATOR_ACTION){
			for (int i =0; i<numberOfRequest; i++){
				String referenceNumber = requests[i];
				try{
					if (assignedTo != null && !assignedTo.equals("")){
						ar.setToForFacilitatorAction(referenceNumber,user);
						ar.assignTo(referenceNumber,assignedTo,user);						
					} else {
						LOGGER.error("Assigned To is required.");
						throw new UnderWriterException("Assigned To is required.");
					}

				} catch(Exception e){
					LOGGER.error(CodeHelper.getStackTrace(e));
					failedRequest.add(referenceNumber + ": " +  e.getMessage() + "<br>&nbsp;");
				}
			}
		} else if (status == IUMConstants.STATUS_FOR_APPROVAL){
			for (int i=0; i<numberOfRequest; i++){
				String referenceNumber = requests[i];
				try{
					ar.submitRequestForApproval(referenceNumber,assignedTo,user);
				}catch(Exception e){
					LOGGER.error(CodeHelper.getStackTrace(e));
					failedRequest.add(referenceNumber + ": " +  e.getMessage() + "<br>&nbsp;");
				}
			}
		} else if (status == IUMConstants.STATUS_UNDERGOING_ASSESSMENT){
			if (numberOfRequest > 1){
				String errorMessage = "Multiple requests is not allowed for \"Undergoing Assessment\"";
				page = constructError("error.submitRequest",errorMessage,request);
			} else{
				for (int i=0; i<numberOfRequest; i++){
					String referenceNumber = requests[i];
					try{
						ar.assessApplication(referenceNumber,user);
					} catch(Exception e){
						LOGGER.error(CodeHelper.getStackTrace(e));
						failedRequest.add(referenceNumber + ": " + e.getMessage() + "<br>&nbsp;");
					}
				}
			}
		} else if (status == IUMConstants.STATUS_AR_FOR_REFERRAL){
			for (int i=0; i<numberOfRequest; i++){
				String referenceNumber = requests[i];
				try{
					ar.forReferral(referenceNumber,assignedTo,user);
				} catch(Exception e){
					LOGGER.error(CodeHelper.getStackTrace(e));
					failedRequest.add(referenceNumber + ": " + e.getMessage() + "<br>&nbsp;");
				}
			}
		} else if (status == IUMConstants.STATUS_AWAITING_MEDICAL){
			for (int i=0; i<numberOfRequest; i++){
				String referenceNumber = requests[i];
				try{
					ar.awaitingMedical(referenceNumber,assignedTo,user);
				}catch(Exception e){
					LOGGER.error(CodeHelper.getStackTrace(e));
					failedRequest.add(referenceNumber + ": " + e.getMessage() + "<br>&nbsp;");
				}
			}
		} else if (status == IUMConstants.STATUS_AWAITING_REQUIREMENTS){
			for (int i=0; i<numberOfRequest; i++){
				String referenceNumber = requests[i];
				try{
					ar.awaitingRequirements(referenceNumber,assignedTo,user);
				}catch(Exception e){
					LOGGER.error(CodeHelper.getStackTrace(e));
					failedRequest.add(referenceNumber + ": " + e.getMessage() + "<br>&nbsp;");
				}
			}
		} else if (status == IUMConstants.STATUS_NB_REVIEW_ACTION){
			for (int i=0; i<numberOfRequest; i++){
				String referenceNumber = requests[i];
				failedRequest.add(referenceNumber + ": " + "The request of the status is not valid." + "<br>&nbsp;");
			}
		} else {
			for (int i=0; i<numberOfRequest; i++){
				String referenceNumber = requests[i];
				try{
					ar.genericChangeARStatus(referenceNumber,status,assignedTo,user);
				} catch(Exception e){
					LOGGER.error(CodeHelper.getStackTrace(e));
					failedRequest.add(referenceNumber + ": " + e.getMessage() + "<br>&nbsp;");
				}
			}
		}
		if (failedRequest.size() >=1){
			StringBuffer errorMessage = new StringBuffer();
			for (int i=0; i < failedRequest.size(); i++){
				String message = (String) failedRequest.get(i);
				errorMessage.append(message);
			}
			constructError("error.submitRequest",errorMessage.toString(),request);
			page = "error";
		}
		else {
			page = "requestListPage";
		}
		LOGGER.debug("submitRequest page forward-->" + page);
		LOGGER.info("submitRequest end");
		return page;
	}

	private String constructError (String key, String parameter, HttpServletRequest request){
		LOGGER.info("constructError start");
		String page = "error";
		ActionErrors error = new ActionErrors();
		error.add(ActionErrors.GLOBAL_ERROR, new ActionError(key, parameter));
		saveErrors(request, error);
		LOGGER.info("constructError end");
		return page;
	}
	
}

