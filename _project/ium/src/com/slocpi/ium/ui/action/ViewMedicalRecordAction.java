package com.slocpi.ium.ui.action;

// struts package
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.slocpi.ium.data.MedicalRecordData;
import com.slocpi.ium.data.StatusData;
import com.slocpi.ium.data.util.DataSourceProxy;
import com.slocpi.ium.ui.form.MedicalRecordDetailForm;
import com.slocpi.ium.underwriter.AssessmentRequest;
import com.slocpi.ium.underwriter.MedicalLabRecord;
import com.slocpi.ium.util.CodeHelper;


/**
 * @TODO Class Description ViewMedicalRecordAction
 * @author: Ma. Theresa Vicencio
 * @version: 1.0
 */
public class ViewMedicalRecordAction extends IUMAction {
	private static final Logger LOGGER = LoggerFactory.getLogger(ViewMedicalRecordAction.class);
  /**
   * @TODO method description for execute
   * @param mapping mapping of request to an instance of this class
   * @param form  object
   * @param request request object
   * @param response response object
   * @exception IOException if an input/output error occurs
   * @exception ServletException if a servlet exception occurs
   * @return ActionForward as defined in the mapping parameter.
   */
	public ActionForward handleAction(ActionMapping mapping, 
							   ActionForm form, 
							   HttpServletRequest request, 
							   HttpServletResponse response)
							   throws Exception {
		
		LOGGER.info("handleAction start");
		String page = "";
		Connection conn = new DataSourceProxy().getConnection();
		try {
			
			final Date start = new Date();
			Date end;
			long elapse;
			
			ArrayList medicalExamDetailsList = new ArrayList(); 		
			MedicalLabRecord  medicalLabRecord = new  MedicalLabRecord();
			MedicalRecordData medicalRecordData = new MedicalRecordData();
			if (request.getAttribute("refNo") == null)
				medicalRecordData = medicalLabRecord.getMedicalRecord(Long.parseLong(request.getParameter("refNo")));
			else
				medicalRecordData = medicalLabRecord.getMedicalRecord(Long.parseLong((String)request.getAttribute("refNo")));
		
			medicalExamDetailsList.add(medicalRecordData);
		 
			MedicalRecordDetailForm medicalRecordDetailForm = (MedicalRecordDetailForm) form;
			if (null == medicalRecordDetailForm){
				medicalRecordDetailForm = new MedicalRecordDetailForm();
			}
			 if (request.getAttribute("actionType") == "save" || medicalRecordDetailForm.getActionType().equals("")){
			 	medicalRecordDetailForm.setActionType("view");
			 	medicalRecordDetailForm.setErr("");
			 }
			 else{
			 	if (request.getParameter("actionType") != null){
			 		medicalRecordDetailForm.setActionType(request.getParameter("actionType"));
			 	}
			 }
						
			medicalRecordDetailForm.setMedicalRecordData(medicalExamDetailsList);
		
			if (request.getParameter("status") == null){
				StatusData statusData =  medicalRecordData.getStatus();
				medicalRecordDetailForm.setStatus(Long.toString(statusData.getStatusId()));
				medicalRecordDetailForm.setPrevStatus(Long.toString(statusData.getStatusId()));
			}
			else{				
				medicalRecordDetailForm.setStatus(request.getParameter("status"));				
				if (request.getAttribute("status") != null){
					medicalRecordDetailForm.setStatus((String)request.getAttribute("status"));
					request.setAttribute("status", null);
				}
			}
			medicalRecordDetailForm.setLabTestInd(medicalRecordData.getLabTestInd());
			AssessmentRequest ar = new AssessmentRequest();
			String ar_lob = ar.getDetails(medicalRecordData.getReferenceNo()).getLob().getLOBCode();
			request.setAttribute("ar_lob", ar_lob);
			
			request.setAttribute("medicalRecordDetailForm", medicalRecordDetailForm);
			
			end = new Date();
			elapse = ((end.getTime() - start.getTime())%(60 * 1000)) / 1000;
			page = "medicalRecordsDetailPage"; 
		
			
		} 
		catch (Exception e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
	  		ActionErrors errors = new ActionErrors();
	  		errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.system.exception", e.getMessage()));
	  		saveErrors(request, errors);
 			page="errorPage";  
		}
		finally{
			closeConnection(conn);
		}
		
		LOGGER.info("handleAction end");
		LOGGER.debug("PAGE FORWARD-->" + page);
		return (mapping.findForward(page));
	}
	
	private void closeConnection(Connection conn) throws SQLException {
		
		
		if (!conn.isClosed()){
			conn.close();
		}		
		
	}
    
}

