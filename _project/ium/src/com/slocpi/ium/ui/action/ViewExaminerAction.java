package com.slocpi.ium.ui.action;

import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.slocpi.ium.IUMException;
import com.slocpi.ium.data.ExaminerData;
import com.slocpi.ium.data.UserData;
import com.slocpi.ium.data.UserProfileData;
import com.slocpi.ium.ui.form.ExaminerForm;
import com.slocpi.ium.ui.util.IUMWebException;
import com.slocpi.ium.ui.util.Page;
import com.slocpi.ium.ui.util.Pagination;
import com.slocpi.ium.ui.util.StateHandler;
import com.slocpi.ium.underwriter.Reference;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.DateHelper;

/**
 * ViewExaminerAction.java 
 * Description:
 *
 * @version $Revision: 1.1 $
 * @author $Author: pw12 $ $Date: 2012/08/01 07:07:17 $
 */
public class ViewExaminerAction extends IUMAction{
	private static final Logger LOGGER = LoggerFactory.getLogger(ViewExaminerAction.class);
	/**
	 * @see com.slocpi.ium.ui.action.IUMAction#handleAction(org.apache.struts.action.ActionMapping, org.apache.struts.action.ActionForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	public ActionForward handleAction(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response)
		throws Exception {
			
		LOGGER.info("handleAction start");
		String page = "";
		try{
			
			StateHandler sh = new StateHandler();
			UserData ud = sh.getUserData(request);
			UserProfileData profile = ud.getProfile();
			
			ExaminerForm eForm = (ExaminerForm) form;
			ExaminerData eData = this.createExaminerObject(eForm);
			this.populateExaminerForm(eForm,eData);
			request.setAttribute("examiner",eForm);
			
			ArrayList examinerList = this.populateExaminerList(); 
			Page pg = this.createPage(request, profile, examinerList); 
						
			page = "viewExaminerPage";
			
		}catch(Exception e){
			LOGGER.error(CodeHelper.getStackTrace(e));
			ActionErrors errors = new ActionErrors();
			errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.system.exception", e.getMessage()));
			saveErrors(request, errors);
			page = "errorPage";
		}
		
		LOGGER.info("handleAction end");
		LOGGER.debug("PAGE FORWARD-->" + page);
		return mapping.findForward(page);
	}
	
	private ExaminerData createExaminerObject(ExaminerForm examinerForm) throws Exception{

		LOGGER.info("createExaminerObject start");
		
		Reference mgr = new Reference();
		ExaminerData eData = mgr.getExaminer(Long.parseLong(examinerForm.getExaminerId())); 
		
		LOGGER.info("createExaminerObject end");
		return eData;
	}
	
	private void populateExaminerForm(ExaminerForm form, ExaminerData data){
		
		LOGGER.info("populateExaminerForm start");
		form.setStatus("M");
		form.setLastName(data.getLastName());
		form.setFirstName(data.getFirstName());
		form.setMiddleName(data.getMiddleName());
		form.setSalutation(data.getSalutation());
		form.setDateOfBirth((DateHelper.format(data.getDateOfBirth(),"ddMMMyyyy")).toUpperCase());
		form.setSex(data.getSex());
		if(data.getRank()!=null){	
			form.setRank(String.valueOf(data.getRank()));
		}
		form.setRankEffectiveDate((DateHelper.format(data.getRankEffectiveDate(),"ddMMMyyyy")).toUpperCase());
		if(data.getTIN()!=null){
			form.setTIN(this.parseTIN(String.valueOf(data.getTIN())));
		}
		form.setOfficeNumber(data.getContactNumber());
		form.setFaxNumber(data.getFaxNumber());
		form.setMobileNumber(data.getMobileNumber());
		form.setBusAddrLine1(data.getBusAddrLine1());
		form.setBusAddrLine2(data.getBusAddrLine2());
		form.setBusAddrLine3(data.getBusAddrLine3());
		form.setCity(data.getCity());
		form.setProvince(data.getProvince());
		form.setCountry(data.getCountry());
		if(data.getZipCode()!=null){
			form.setZipCode(String.valueOf(data.getZipCode()));
		}
		form.setMailToBusAddrInd(data.getMailToBusAddrInd());
		form.setPreEmploymentSrvcInd(data.getPreEmploymentSrvcInd());
		form.setAccreditationInd(data.getAccreditationInd());
		form.setClinicHrsFrom(DateHelper.format(data.getClinicHrsFrom(),"hh:mma"));
		form.setClinicHrsTo(DateHelper.format(data.getClinicHrsTo(),"hh:mma"));
		if(data.getExaminePlace() != null){
			form.setExaminePlace(String.valueOf(data.getExaminePlace()));
		}
		
		
		form.setSpecializationsList(data.getSpecializations());
		LOGGER.info("populateExaminerForm end");						
	}
	
	private ArrayList populateExaminerList() throws IUMException {
		
		LOGGER.info("populateExaminerList start");
		
		Reference mgr = new Reference();
		ArrayList examList = mgr.getExaminers();
		
		LOGGER.info("populateExaminerList end");
				
		return examList;
	}
	
	private Page createPage(HttpServletRequest request, UserProfileData profile, ArrayList examinerList) throws IUMWebException
	{
		
		LOGGER.info("createPage start");
		
		int recPerView = profile.getRecordsPerView();
		
		
		if (recPerView == 0) {
		  recPerView = 1;
		}
			
		int pageNo = 1;
		String reqPageNum = request.getParameter("pageNo");
		
		
		if ((reqPageNum != null) && !(reqPageNum.equals(""))) {
			try {
				pageNo = Integer.parseInt(reqPageNum);
			}
			catch (NumberFormatException e) {
				//pageNo will be the first page   
				LOGGER.error(CodeHelper.getStackTrace(e));
			}
		}	
						
		Pagination pgn = new Pagination(examinerList, recPerView);
		Page pg = pgn.getPage(pageNo);			
		request.setAttribute("page", pg);
		request.setAttribute("pageNo", String.valueOf(pageNo));
		
		LOGGER.info("createPage end");
				
		return pg;
	}
	
	private String parseTIN(String unparsed){
		
		LOGGER.info("parseTIN start");
		
		String parsed="";
		if(unparsed.length()==9){
			parsed = unparsed.substring(0,3)+"-"+unparsed.substring(3,6)+"-"+unparsed.substring(6,9);
		}else{
			parsed = unparsed;
		}
		
		LOGGER.info("parseTIN end");
		return parsed;
	}
		

}
