/*
 * Created on Jan 7, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.ui.action;

import java.sql.Connection;
import java.sql.SQLException;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.ResourceBundle;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import com.slocpi.ium.IUMException;
import com.slocpi.ium.data.AssessmentRequestData;
import com.slocpi.ium.data.ClientData;
import com.slocpi.ium.data.ClientDataSheetData;
import com.slocpi.ium.data.KickOutMessageData;
import com.slocpi.ium.data.LOBData;
import com.slocpi.ium.data.MedicalRecordData;
import com.slocpi.ium.data.PolicyRequirementsData;
import com.slocpi.ium.data.StatusData;
import com.slocpi.ium.data.SunLifeOfficeData;
import com.slocpi.ium.data.TestProfileData;
import com.slocpi.ium.data.UserAccessData;
import com.slocpi.ium.data.UserData;
import com.slocpi.ium.data.UserProfileData;
import com.slocpi.ium.data.dao.RequirementDAO;
import com.slocpi.ium.data.util.DataSourceProxy;
import com.slocpi.ium.interfaces.IUMInterfaceException;
import com.slocpi.ium.ui.form.CDSDetailForm;
import com.slocpi.ium.ui.form.KOReasonsForm;
import com.slocpi.ium.ui.form.MedLabRecordsForm;
import com.slocpi.ium.ui.form.RequirementForm;
import com.slocpi.ium.ui.util.StateHandler;
import com.slocpi.ium.underwriter.AssessmentRequest;
import com.slocpi.ium.underwriter.KickOutMessage;
import com.slocpi.ium.underwriter.PolicyRequirements;
import com.slocpi.ium.underwriter.UnderWriterException;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.DateHelper;
import com.slocpi.ium.util.IUMConstants;
import com.slocpi.ium.util.ValueConverter;

/**
 * @author Ingrid Villanueva
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class SaveRequirementAction extends IUMAction{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(SaveRequirementAction.class);
	
	public ActionForward handleAction (ActionMapping mapping,
									  ActionForm form,
									  HttpServletRequest request,
									  HttpServletResponse response)
									  throws Exception
									  {
		
		LOGGER.info("handleAction start");
		AssessmentRequest assessmentReq = new AssessmentRequest();
		String page = "cdsDetailPage";
		CDSDetailForm arForm = (CDSDetailForm) form;
		String   referenceNumber = arForm.getRefNo();
		String suffix = assessmentReq.getPolicySuffix(referenceNumber);
		
		String savedSection = (request.getParameter("savedSection") != null) ? request.getParameter("savedSection").trim(): "";
		
		try {
			HttpSession session = request.getSession();
			if (session == null) {
				LOGGER.debug("request.getSession() is null");
			} else {
				LOGGER.debug("request.getSession() is not null");
				session.setAttribute("savedSection", savedSection);
				String temp = (String) session.getAttribute("savedSection");
				String temp2 = (temp != null) ? temp.trim(): "session object savedSection is not saved" ;
				
			}
		} catch (Exception e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
		}
		
		arForm.setRequestStatus(request.getParameter("arStatus"));
		try {
			StateHandler sh = new StateHandler();
			UserData ud = sh.getUserData(request);
			UserProfileData profile = ud.getProfile();
			String userId = profile.getUserId();
		
			String pageId = IUMConstants.PAGE_CDS;
			UserAccessData uad = extractAccessPriveleges(userId, pageId);

			if (uad.getAccessCode().equals("W")) {
				
				String   errorMessage    = "";
        		String   statusTo        = arForm.getStatusTo();
				String   lob             = request.getParameter("lob");
				String   facilitator     = request.getParameter("facilitator");
				if (facilitator == null) facilitator = "";
				String   followUp        = request.getParameter("followUp");
				String   branchId        = request.getParameter("branchCode");
				String   agentId         = request.getParameter("agentCode");
				String   assignedTo      = request.getParameter("assignedTo");
				
				if (followUp == null) {
					if (statusTo.equals(Long.toString(IUMConstants.STATUS_ORDERED))) {
						
						orderRequirement(arForm, referenceNumber, userId, lob, branchId, agentId, arForm.getSourceSystem());
						page = "cdsDetailPage";
					}
					
					else if (statusTo.equals(Long.toString(IUMConstants.STATUS_RECEIVED_IN_SITE))) {
						receiveRequirement(arForm, referenceNumber, userId, lob, branchId, assignedTo, arForm.getSourceSystem());
					}
					else if (statusTo.equals(Long.toString(IUMConstants.STATUS_REVIEWED_AND_ACCEPTED))){
						acceptRequirement(arForm, referenceNumber, userId, lob, branchId, assignedTo, arForm.getSourceSystem());
					}
					else if (statusTo.equals(Long.toString(IUMConstants.STATUS_REQ_CANCELLED))){
						cancelRequirement(arForm, referenceNumber, userId, lob, branchId, agentId, arForm.getSourceSystem());
					}
					else if (statusTo.equals(Long.toString(IUMConstants.STATUS_WAIVED))){
						waiveRequirement(arForm, referenceNumber, userId, lob, branchId, agentId, arForm.getSourceSystem());
					}
					else if (statusTo.equals(Long.toString(IUMConstants.STATUS_REVIEWED_AND_REJECTED))){
						rejectRequirement(arForm, referenceNumber, userId, lob, branchId, arForm.getSourceSystem());
					}
					else{
						genericChangeStatus(arForm, referenceNumber, userId, lob, branchId, agentId, statusTo, arForm.getSourceSystem());
						
					}
				}
				else {
					followUp(arForm, referenceNumber, userId, lob, branchId, agentId);
				}

				HttpSession session = request.getSession();
				if (session.getAttribute("requirementForm") != null)
				    session.removeAttribute("requirementForm");

				arForm   = populateRequestDetails(arForm);
				ArrayList             reqLevelList    = getRequestLevel();
				ArrayList requirements = populateRequirements(referenceNumber);
				ArrayList             koReasons       = populateKOReasons(referenceNumber);
				/*ArrayList             medLabRecords   = populateMedLabRecords(referenceNumber);*/
				RequirementForm       requirementForm = populateRequirementDetails(referenceNumber, Long.parseLong(arForm.getReqId()[0]));

				arForm.setRequirements(requirements);
				arForm.setReqLevelList(reqLevelList);
				arForm.setKoReasons(koReasons);
				/*arForm.setMedLabRecords(medLabRecords);*/
				arForm.setPolicySuffix(suffix);
				session.setAttribute("requirementForm", requirementForm);
				session.setAttribute("detailForm", arForm);
				page = "cdsDetailPage";
				
			}
			else {
				page = "errorPage";
			}

			if(assessmentReq.isReqtCountExceeded(referenceNumber) && page.equals("cdsDetailPage")){
				ActionMessages warning = new ActionMessages();
				warning.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("message.reqtCountExceeded"));
				saveMessages(request, warning);
			}
		}
		catch(UnderWriterException e) {
			
			HttpSession session = request.getSession();
			if (session.getAttribute("requirementForm") != null)
			    session.removeAttribute("requirementForm");
			
			arForm   = populateRequestDetails(arForm);
			ArrayList             reqLevelList    = getRequestLevel();
			ArrayList requirements = populateRequirements(referenceNumber);
			ArrayList             koReasons       = populateKOReasons(referenceNumber);
			/*ArrayList             medLabRecords   = populateMedLabRecords(referenceNumber);*/
			RequirementForm       requirementForm = populateRequirementDetails(referenceNumber, Long.parseLong(arForm.getReqId()[0]));
			arForm.setRequirements(requirements);
			arForm.setReqLevelList(reqLevelList);
			arForm.setKoReasons(koReasons);
			/*arForm.setMedLabRecords(medLabRecords);*/
			arForm.setPolicySuffix(suffix);
			
			session.setAttribute("detailForm", arForm);
			session.setAttribute("requirementForm", requirementForm);

			page = "cdsDetailPage";
			ActionErrors errors = new ActionErrors();
			String       err    = e.getMessage();
			if (err == null) err = "";
			errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.invalidRequirement", err));
			saveErrors(request, errors);
		}
		catch(IUMException eIUM) {
			LOGGER.error(CodeHelper.getStackTrace(eIUM));
			ActionErrors errors = new ActionErrors();
			errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.system.exception", eIUM.getMessage()));
			saveErrors(request, errors);
			request.setAttribute("errors", eIUM.getMessage());
			page = "errorPage";
		}
		catch (IUMInterfaceException eInterface) {
			LOGGER.error(CodeHelper.getStackTrace(eInterface));
			ActionErrors errors = new ActionErrors();
			errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.system.exception", eInterface.getMessage()));
			saveErrors(request, errors);
			request.setAttribute("errors", eInterface.getMessage());
			page = "errorPage";
		}
		LOGGER.info("handleAction end");
		LOGGER.debug("PAGE FORWARD-->" + page);
		return mapping.findForward(page);
	}

	private void followUp(CDSDetailForm arForm, String refNo, String userId, String lob, String branchId, String agentId) throws Exception {

		LOGGER.info("followUp start");
        String   statusTo        = arForm.getStatusTo();
        String[] reqId           = arForm.getReqId();
        String[] index           = arForm.getIndexTemp();
		String[] statusCode		 = arForm.getStatusCode();
		String[] followUpNo      = arForm.getFollowUpNo();
		String[] followUpDate    = arForm.getFollowUpDate();
		String[] seq             = arForm.getSeq();
		String[] level           = arForm.getReqLevel();
		String[] reqCodes        = arForm.getReqCodes();
		String[] dateSent        = arForm.getDateSent();

		boolean                noErrors   = false;
		String                 pageReturn = "";
		String                 CHECKED    = "checked";
		ArrayList              listReq    = new ArrayList();
		PolicyRequirementsData data       = null;
		
		for (int i=0;i<reqId.length;i++) {
			if (index[i] != null) {
				if (index[i].equals(CHECKED)) {
					data = new PolicyRequirementsData();
					data.setReferenceNumber(refNo);
					data.setRequirementCode(reqCodes[i]);
					if (reqId[i] != null) {
						if (reqId[i].trim().length() > 0) {
							data.setRequirementId(Long.parseLong(reqId[i]));
						}
					}
                    if (followUpNo[i] != null) {
                    	if (followUpNo[i].trim().length() > 0) {
							data.setFollowUpNumber(Long.parseLong(followUpNo[i]));
                    	}
                    }
					data.setUpdatedBy(userId);
					data.setUpdateDate(new Date());
					data.setFollowUpDate(DateHelper.parse(followUpDate[i],"ddMMMyyyy"));
					if (seq[i] != null) {
						if (seq[i].trim().length() > 0) {
							data.setSequenceNumber(Long.parseLong(seq[i]));
						}
					}
					StatusData stat = new StatusData();
					if (statusCode[i] != null) {
						if (statusCode[i].trim().length() > 0) {
							stat.setStatusId(Long.parseLong(statusCode[i]));
						}
					}
					data.setStatus(stat);
					data.setLevel(level[i]);
					data.setDateSent(DateHelper.parse(dateSent[i],"ddMMMyyy"));
					listReq.add(data);
				}
			}
		}
		PolicyRequirements    req    = new PolicyRequirements();

		AssessmentRequestData reqObj = new AssessmentRequestData();
		LOBData               lobObj = new LOBData();
		lobObj.setLOBCode(lob);
		reqObj.setLob(lobObj);
		SunLifeOfficeData     ofcObj = new SunLifeOfficeData();
		ofcObj.setOfficeId(branchId);
		reqObj.setBranch(ofcObj);
		UserProfileData       usrObj2 = new UserProfileData();
		usrObj2.setUserId(agentId);
		reqObj.setAgent(usrObj2);

		req.sendFollowUpRequirementNotification(listReq, reqObj);
		LOGGER.info("followUp end");
	}	
	
	private void orderRequirement(CDSDetailForm arForm, String refNo, String userId, String lob,
                 String branchId, String agentId, String sourceSystem) throws Exception {	

		LOGGER.info("orderRequirement start");
        String   statusTo        = arForm.getStatusTo();
	    String[] reqId           = arForm.getReqId();
	    String[] index           = arForm.getIndexTemp();
	    String[] statusCode		 = arForm.getStatusCode();
	    String[] followUpNo      = arForm.getFollowUpNo();
	    String[] followUpDate    = arForm.getFollowUpDate();
	    String[] seq             = arForm.getSeq();
	    String[] level           = arForm.getReqLevel();
	    String[] reqCodes        = arForm.getReqCodes();
	    
	    boolean                noErrors   = false;
		String                 pageReturn = "";
		String                 CHECKED    = "checked";
		ArrayList              listReq    = new ArrayList();
		PolicyRequirementsData data       = null;

		PolicyRequirements req = new PolicyRequirements();

		for (int i=0;i<reqId.length;i++) {
			if (index[i] != null) {
				if (index[i].equals(CHECKED)) {
		    		data = new PolicyRequirementsData();
		    		data.setReferenceNumber(refNo);
		    		data.setRequirementCode(reqCodes[i]);
		    		if (reqId[i] != null) {
		    			if (reqId[i].trim().length() > 0){
							data.setRequirementId(Long.parseLong(reqId[i]));
		    			}
		    		}
		    		if (followUpNo[i] != null) {
		    			if (followUpNo[i].trim().length() > 0) {
                    		data.setFollowUpNumber(Long.parseLong(followUpNo[i]));
		    			}
		    		}
		    		data.setUpdatedBy(userId);
		    		data.setUpdateDate(new Date());
					StatusData stat = new StatusData();
					if (statusCode[i] != null) {
						if (statusCode[i].trim().length() > 0) {
							stat.setStatusId(Long.parseLong(statusCode[i]));
						}
					}
					data.setStatus(stat);
					if (seq[i] != null) {
						if (seq[i].trim().length() > 0) {
							data.setSequenceNumber(Long.parseLong(seq[i]));
						}
					}
					data.setLevel(level[i]);
		    		listReq.add(data);
				}
			}
		}


		AssessmentRequestData reqObj  = new AssessmentRequestData();
		LOBData               lobObj  = new LOBData();
		lobObj.setLOBCode(lob);
		reqObj.setLob(lobObj);
		SunLifeOfficeData     ofcObj  = new SunLifeOfficeData();
		ofcObj.setOfficeId(branchId);
		reqObj.setBranch(ofcObj);
		UserProfileData       usrObj2 = new UserProfileData();
		usrObj2.setUserId(agentId);
		reqObj.setAgent(usrObj2);
        StatusData stat = new StatusData();
        stat.setStatusId(Long.parseLong(arForm.getRequestStatus()));
        reqObj.setStatus(stat);
		UserProfileData       usrObj1 = new UserProfileData();
		
		reqObj.setSourceSystem(sourceSystem);	
        req.orderRequirement(listReq, reqObj, userId);
        LOGGER.info("orderRequirement end");
	}

	private void receiveRequirement(CDSDetailForm arForm, String refNo, String userId, String lob, String branchId, String assignedTo, String sourceSystem) throws Exception {

		LOGGER.info("receiveRequirement start");
        String   statusTo        = arForm.getStatusTo();
		String[] reqId           = arForm.getReqId();
		String[] index           = arForm.getIndexTemp();
		String[] statusCode		 = arForm.getStatusCode();
		String[] followUpNo      = arForm.getFollowUpNo();
		String[] followUpDate    = arForm.getFollowUpDate();
		String[] seq             = arForm.getSeq();
		String[] level           = arForm.getReqLevel();
		String[] reqCodes        = arForm.getReqCodes();

		boolean                noErrors   = false;
		String                 pageReturn = "";
		String                 CHECKED    = "checked";
		ArrayList              listReq    = new ArrayList();
		PolicyRequirementsData data       = null;
		PolicyRequirements req;
		for (int i=0;i<reqId.length;i++) {
			if (index[i] != null) {
				if (index[i].equals(CHECKED)) {
		    		data = new PolicyRequirementsData();
					data.setReferenceNumber(refNo);
					data.setRequirementCode(reqCodes[i]);
		    		StatusData stat = new StatusData();
		    		if (statusCode[i] != null) {
		    			if (statusCode[i].trim().length() > 0) {
							stat.setStatusId(Long.parseLong(statusCode[i]));
		    			}
		    		}
		    		data.setStatus(stat);
		    		if (reqId[i] != null) {
		    			if (reqId[i].trim().length() > 0) {
							data.setRequirementId(Long.parseLong(reqId[i]));
		    			}
		    		}

		    		data.setUpdatedBy(userId);
		    		data.setUpdateDate(new Date());
		    		if (seq[i] != null) {
		    			if (seq[i].trim().length() > 0) {
							data.setSequenceNumber(Long.parseLong(seq[i]));
		    			}
		    		}
					data.setLevel(level[i]);
		    		listReq.add(data);
				}
			}
		}
		req  = new PolicyRequirements();

		AssessmentRequestData reqObj = new AssessmentRequestData();
		LOBData               lobObj = new LOBData();
		lobObj.setLOBCode(lob);
		reqObj.setLob(lobObj);
		SunLifeOfficeData     ofcObj = new SunLifeOfficeData();
		ofcObj.setOfficeId(branchId);
		reqObj.setBranch(ofcObj);
		UserProfileData uData = new UserProfileData();
		uData.setUserId(assignedTo);
		reqObj.setAssignedTo(uData);
		reqObj.setSourceSystem(sourceSystem);
		req.receiveRequirement(listReq, reqObj);
		LOGGER.info("receiveRequirement end");
		
	}

	private void acceptRequirement(CDSDetailForm arForm, String refNo, String userId, String lob, String branchId, String assignedTo, String sourceSystem) throws Exception {

		LOGGER.info("acceptRequirement start");
		String   statusTo        = arForm.getStatusTo();
		String[] reqId           = arForm.getReqId();
		String[] index           = arForm.getIndexTemp();
		String[] statusCode		 = arForm.getStatusCode();
		String[] followUpNo      = arForm.getFollowUpNo();
		String[] followUpDate    = arForm.getFollowUpDate();
		String[] seq             = arForm.getSeq();
		String[] level           = arForm.getReqLevel();
		String[] reqCodes        = arForm.getReqCodes();

		boolean                noErrors   = false;
		String                 pageReturn = "";
		String                 CHECKED    = "checked";
		ArrayList              listReq    = new ArrayList();
		PolicyRequirementsData data       = null;
		PolicyRequirements req;
		for (int i=0;i<reqId.length;i++) {
			if (index[i] != null) {
				if (index[i].equals(CHECKED)) {
		    		data = new PolicyRequirementsData();
		    		StatusData stat = new StatusData();
					data.setReferenceNumber(refNo);
					data.setRequirementCode(reqCodes[i]);
					if (statusCode[i] != null) {
						if (statusCode[i].trim().length() > 0) {
							stat.setStatusId(Long.parseLong(statusCode[i]));
						}
					}
		    		data.setStatus(stat);
		    		if (reqId[i] != null) {
		    			if (reqId[i].trim().length() > 0) {
							data.setRequirementId(Long.parseLong(reqId[i]));
		    			}
		    		}
		    		data.setUpdatedBy(userId);
		    		data.setUpdateDate(new Date());
		    		if (seq[i] != null) {
		    			if (seq[i].trim().length() > 0) {
							data.setSequenceNumber(Long.parseLong(seq[i]));
		    			}
		    		}
					data.setLevel(level[i]);
		    		listReq.add(data);
				}
			}
		}
		req  = new PolicyRequirements();

		AssessmentRequestData reqObj = new AssessmentRequestData();
		UserProfileData uData = new UserProfileData();
		uData.setUserId(assignedTo);	
		reqObj.setAssignedTo(uData);		
		LOBData               lobObj = new LOBData();
		lobObj.setLOBCode(lob);
		reqObj.setLob(lobObj);
		SunLifeOfficeData     ofcObj = new SunLifeOfficeData();
		ofcObj.setOfficeId(branchId);
		reqObj.setBranch(ofcObj);
		reqObj.setSourceSystem(sourceSystem);
		req.acceptRequirement(listReq, reqObj);
		LOGGER.info("acceptRequirement end");
	}

	private void cancelRequirement(CDSDetailForm arForm, String refNo, String userId, String lob, String branchId, String agentId, String sourceSystem) throws Exception {

		LOGGER.info("cancelRequirement start");
		String   statusTo        = arForm.getStatusTo();
		String[] reqId           = arForm.getReqId();
		String[] index           = arForm.getIndexTemp();
		String[] statusCode		 = arForm.getStatusCode();
		String[] followUpNo      = arForm.getFollowUpNo();
		String[] followUpDate    = arForm.getFollowUpDate();
		String[] seq             = arForm.getSeq();
		String[] level           = arForm.getReqLevel();
		String[] reqCodes        = arForm.getReqCodes();

		boolean                noErrors   = false;
		String                 pageReturn = "";
		String                 CHECKED    = "checked";
		ArrayList              listReq    = new ArrayList();
		PolicyRequirementsData data       = null;
		PolicyRequirements req;
		for (int i=0;i<reqId.length;i++) {
			if (index[i] != null) {
				if (index[i].equals(CHECKED)) {
					data = new PolicyRequirementsData();
					StatusData stat = new StatusData();
					data.setReferenceNumber(refNo);
					data.setRequirementCode(reqCodes[i]);
					if (statusCode[i] != null) {
						if (statusCode[i].trim().length() > 0) {
							stat.setStatusId(Long.parseLong(statusCode[i]));
						}
					}
					data.setStatus(stat);
					if (reqId[i] != null) {
						if (reqId[i].trim().length() > 0) {
							data.setRequirementId(Long.parseLong(reqId[i]));
						}
					}
					data.setUpdatedBy(userId);
					data.setUpdateDate(new Date());
					if (seq[i] != null) {
						if (seq[i].trim().length() > 0) {
							data.setSequenceNumber(Long.parseLong(seq[i]));
						}
					}
					data.setLevel(level[i]);
					listReq.add(data);
				}
			}
		}
		req  = new PolicyRequirements();

		AssessmentRequestData reqObj = new AssessmentRequestData();
		LOBData               lobObj = new LOBData();
		lobObj.setLOBCode(lob);
		reqObj.setLob(lobObj);
		SunLifeOfficeData     ofcObj = new SunLifeOfficeData();
		ofcObj.setOfficeId(branchId);
		reqObj.setBranch(ofcObj);
		UserProfileData       usrObj2 = new UserProfileData();
		usrObj2.setUserId(agentId);
		reqObj.setAgent(usrObj2);
		reqObj.setSourceSystem(sourceSystem);
		req.cancelRequirement(listReq, reqObj);
		LOGGER.info("cancelRequirement end");
	}//

	private void waiveRequirement(CDSDetailForm arForm, String refNo, String userId, String lob, String branchId, String agentId, String sourceSystem) throws Exception {

		LOGGER.info("waiveRequirement start");
		String   statusTo        = arForm.getStatusTo();
		String[] reqId           = arForm.getReqId();
		String[] index           = arForm.getIndexTemp();
		String[] statusCode		 = arForm.getStatusCode();
		String[] followUpNo      = arForm.getFollowUpNo();
		String[] followUpDate    = arForm.getFollowUpDate();
		String[] seq             = arForm.getSeq();
		String[] level           = arForm.getReqLevel();
		String[] reqCodes        = arForm.getReqCodes();

		
		boolean                noErrors   = false;
		String                 pageReturn = "";
		String                 CHECKED    = "checked";
		ArrayList              listReq    = new ArrayList();
		PolicyRequirementsData data       = null;
		PolicyRequirements req;
		for (int i=0;i<reqId.length;i++) {
			if (index[i] != null) {
				if (index[i].equals(CHECKED)) {
					data = new PolicyRequirementsData();
					StatusData stat = new StatusData();
					data.setReferenceNumber(refNo);
					data.setRequirementCode(reqCodes[i]);
					if (statusCode[i] != null) {
						if (statusCode[i].trim().length() > 0) {
							stat.setStatusId(Long.parseLong(statusCode[i]));
						}
					}
					data.setStatus(stat);
					if (reqId[i] != null) {
						if (reqId[i].trim().length() > 0) {
							data.setRequirementId(Long.parseLong(reqId[i]));
						}
					}
					data.setUpdatedBy(userId);
					data.setUpdateDate(new Date());
					if (seq[i] != null) {
						if (seq[i].trim().length() > 0) {
							data.setSequenceNumber(Long.parseLong(seq[i]));
						}
					}
					data.setLevel(level[i]);
					listReq.add(data);
				}
			}
		}
		req  = new PolicyRequirements();

		AssessmentRequestData reqObj = new AssessmentRequestData();
		LOBData               lobObj = new LOBData();
		lobObj.setLOBCode(lob);
		reqObj.setLob(lobObj);
		SunLifeOfficeData     ofcObj = new SunLifeOfficeData();
		ofcObj.setOfficeId(branchId);
		reqObj.setBranch(ofcObj);
		UserProfileData       usrObj2 = new UserProfileData();
		usrObj2.setUserId(agentId);
		reqObj.setAgent(usrObj2);
		reqObj.setSourceSystem(sourceSystem);
		req.waiveRequirement(listReq, reqObj);
		LOGGER.info("waiveRequirement end");
	}//

	private void rejectRequirement(CDSDetailForm arForm, String refNo, String userId, String lob, String branchId, String sourceSystem) throws Exception {

		LOGGER.info("rejectRequirement start");
		String   statusTo        = arForm.getStatusTo();
		String[] reqId           = arForm.getReqId();
		String[] index           = arForm.getIndexTemp();
		String[] statusCode		 = arForm.getStatusCode();
		String[] followUpNo      = arForm.getFollowUpNo();
		String[] followUpDate    = arForm.getFollowUpDate();
		String[] seq             = arForm.getSeq();
		String[] level           = arForm.getReqLevel();
		String[] reqCodes        = arForm.getReqCodes();

		boolean                noErrors   = false;
		String                 pageReturn = "";
		String                 CHECKED    = "checked";
		ArrayList              listReq    = new ArrayList();
		PolicyRequirementsData data       = null;
		PolicyRequirements req;
		for (int i=0;i<reqId.length;i++) {
			if (index[i] != null) {
				if (index[i].equals(CHECKED)) {
					data = new PolicyRequirementsData();
					StatusData stat = new StatusData();
					data.setReferenceNumber(refNo);
					data.setRequirementCode(reqCodes[i]);
					if (statusCode[i] != null) {
						if (statusCode[i].trim().length() > 0) {
							stat.setStatusId(Long.parseLong(statusCode[i]));
						}
					}
					data.setStatus(stat);
					if (reqId[i] != null) {
						if (reqId[i].trim().length() > 0) {
							data.setRequirementId(Long.parseLong(reqId[i]));
						}
					}
					data.setUpdatedBy(userId);
					data.setUpdateDate(new Date());
					if (seq[i] != null) {
						if (seq[i].trim().length() > 0) {
							data.setSequenceNumber(Long.parseLong(seq[i]));
						}
					}
					data.setLevel(level[i]);
					listReq.add(data);
				}
			}
		}
		req  = new PolicyRequirements();

		AssessmentRequestData reqObj = new AssessmentRequestData();
		LOBData               lobObj = new LOBData();
		lobObj.setLOBCode(lob);
		reqObj.setLob(lobObj);
		SunLifeOfficeData     ofcObj = new SunLifeOfficeData();
		ofcObj.setOfficeId(branchId);
		reqObj.setBranch(ofcObj);
		reqObj.setSourceSystem(sourceSystem);
		req.rejectRequirements(listReq, reqObj);
		LOGGER.info("rejectRequirement end");
		
	}

	private ArrayList populateRequirements(String refNo) throws Exception {
		
	   LOGGER.info("populateRequirements start");
	  PolicyRequirements pr = new PolicyRequirements();
	  ArrayList reqList = pr.getRequirements(refNo);

	  int size = reqList.size();
	  ArrayList requirements = new ArrayList();
	  for (int i = 0; i < size; i++) {
		PolicyRequirementsData reqData = (PolicyRequirementsData)reqList.get(i);
		RequirementForm reqForm = new RequirementForm();
		reqForm.setRefNo(reqData.getReferenceNumber());
		reqForm.setReqId(String.valueOf(reqData.getRequirementId()));
		reqForm.setSeq(String.valueOf(reqData.getSequenceNumber()));
		reqForm.setReqCode(reqData.getRequirementCode());
		reqForm.setReqLevel(reqData.getLevel());
		StatusData stat = reqData.getStatus();
		reqForm.setReqStatusCode(String.valueOf(stat.getStatusId()));
		reqForm.setReqStatusDesc(stat.getStatusDesc());
		reqForm.setStatusDate((DateHelper.format(reqData.getStatusDate(), "ddMMMyyyy")).toUpperCase());
		reqForm.setComment(reqData.getComments());
		reqForm.setClientType(reqData.getClientType());
		reqForm.setFollowUpDate((DateHelper.format(reqData.getFollowUpDate(), "ddMMMyyyy")).toUpperCase());
		reqForm.setFollowUpNo(Long.toString(reqData.getFollowUpNumber()));
		reqForm.setDateSent((DateHelper.format(reqData.getDateSent(), "ddMMMyyyy")).toUpperCase());
		reqForm.setClientNo(reqData.getClientId());
		reqForm.setTestDate((DateHelper.format(reqData.getTestDate(), "ddMMMyyyy")).toUpperCase());
		requirements.add(reqForm);
		
		reqForm.setImageReferenceNum(reqData.getImageRef());
		  
		String fnURL = getWMSImageLink(reqData);
		reqForm.setWmsImageLink(fnURL);
	  }
	  LOGGER.info("populateRequirements end");
	  return (requirements);
	}

	private CDSDetailForm populateRequestDetails(CDSDetailForm reqForm) throws UnderWriterException, IUMException {
	 
	  LOGGER.info("populateRequestDetails start");
	  AssessmentRequest assessmentRequest = new AssessmentRequest();
	  AssessmentRequestData requestData = assessmentRequest.getDetails(reqForm.getRefNo());

	  LOBData lob = requestData.getLob();
	  ClientData insured = requestData.getInsured();
	  ClientData owner = requestData.getOwner();
	  SunLifeOfficeData branch = requestData.getBranch();
	  UserProfileData agent = requestData.getAgent();
	  UserProfileData assignedTo = requestData.getAssignedTo();
	  UserProfileData underwriter = requestData.getUnderwriter();
	  UserProfileData location = requestData.getFolderLocation();
	  ClientDataSheetData cds = requestData.getClientDataSheet();
	  StatusData statusData = requestData.getStatus();

	  NumberFormat nf = NumberFormat.getInstance();
	  nf.setMinimumFractionDigits(2);
	  nf.setMaximumFractionDigits(2);

	  reqForm.setTransmitIndicator(requestData.getTransmitIndicator());
	  reqForm.setRefNo(reqForm.getRefNo());
	  reqForm.setLob(lob.getLOBCode());
	  reqForm.setSourceSystem(requestData.getSourceSystem());
	  reqForm.setAmountCovered(nf.format(requestData.getAmountCovered()));
	  reqForm.setPremium(nf.format(requestData.getPremium()));
	  reqForm.setInsuredName(insured.getGivenName() + " " + insured.getLastName());
	  reqForm.setBranchCode(branch.getOfficeId());
	  reqForm.setBranchName(branch.getOfficeName());
	  reqForm.setAgentCode(agent.getUserId());
	  reqForm.setAgentName(agent.getFirstName() + " " + agent.getLastName());
	  reqForm.setRequestStatus(String.valueOf(statusData.getStatusId()));
	  reqForm.setAssignedTo(assignedTo.getUserId());
	  reqForm.setUnderwriter(underwriter.getUserId());
	  reqForm.setLocation(location.getUserId());

	  reqForm.setRemarks(requestData.getRemarks());
	  reqForm.setReceivedDate((DateHelper.format(requestData.getApplicationReceivedDate(), "ddMMMyyyy")).toUpperCase());
	  reqForm.setDateForwarded((DateHelper.format(requestData.getForwardedDate(), "ddMMMyyyy")).toUpperCase());
	  reqForm.setDateEdited((DateHelper.format(requestData.getUpdatedDate(), "ddMMMyyyy")).toUpperCase());
	  reqForm.setDateCreated((DateHelper.format(requestData.getCreatedDate(), "ddMMMyyyy")).toUpperCase());
	  reqForm.setStatusDate((DateHelper.format(requestData.getStatusDate(), "ddMMMyyyy")).toUpperCase());

	  reqForm.setInsuredClientType(cds.getClientType());
	  reqForm.setInsuredClientNo(insured.getClientId());
	  reqForm.setInsuredLastName(insured.getLastName());
	  reqForm.setInsuredFirstName(insured.getGivenName());
	  reqForm.setInsuredMiddleName(insured.getMiddleName());
	  reqForm.setInsuredTitle(insured.getTitle());
	  reqForm.setInsuredSuffix(insured.getSuffix());
	  reqForm.setInsuredAge(String.valueOf(insured.getAge()));
	  reqForm.setInsuredSex(insured.getSex());
	  reqForm.setInsuredBirthDate((DateHelper.format(insured.getBirthDate(), "ddMMMyyyy")).toUpperCase());

	  reqForm.setOwnerClientType(cds.getClientType());
	  reqForm.setOwnerClientNo(owner.getClientId());
	  reqForm.setOwnerLastName(owner.getLastName());
	  reqForm.setOwnerFirstName(owner.getGivenName());
	  reqForm.setOwnerMiddleName(owner.getMiddleName());
	  reqForm.setOwnerTitle(owner.getTitle());
	  reqForm.setOwnerSuffix(owner.getSuffix());
	  reqForm.setOwnerAge(String.valueOf(owner.getAge()));
	  reqForm.setOwnerSex(owner.getSex());
	  reqForm.setOwnerBirthDate((DateHelper.format(owner.getBirthDate(), "ddMMMyyyy")).toUpperCase());

	  LOGGER.info("populateRequestDetails end");
	  return (reqForm);
	}

	private ArrayList getRequestLevel() throws SQLException, IUMException {
		
	   LOGGER.info("getRequestLevel start");
	  
      try {
		  RequirementDAO  dao  = new RequirementDAO();
		  ArrayList       list = new ArrayList();
		  list = dao.retrieveRequirements();
		  if (list != null){
			  LOGGER.debug("size = " + list.size());
		  }else{
			  LOGGER.debug("list is null");
		  }
		  LOGGER.info("getRequestLevel end");  
		  return list;
      }
      catch (SQLException e) {
    	  LOGGER.error(CodeHelper.getStackTrace(e));
      	  throw new IUMException(e.getMessage());
      }
    }

	private ArrayList populateKOReasons(String refNo) throws UnderWriterException, IUMException {
		
	   LOGGER.info("populateKOReasons start");
	  KickOutMessage ko = new KickOutMessage();
	  ArrayList reqList = ko.getMessages(refNo);

	  int size = reqList.size();
	  ArrayList koReasons = new ArrayList();
	  for (int i = 0; i < size; i++) {
		KickOutMessageData koData = (KickOutMessageData)reqList.get(i);
		KOReasonsForm koForm = new KOReasonsForm();
		koForm.setSeq(String.valueOf(koData.getSequenceNumber()));
		koForm.setMessageText(koData.getMessageText());
		koForm.setClientNo(koData.getClientId());
		koForm.setFailResponse(koData.getFailResponse());
		koReasons.add(koForm);
	  }

	  LOGGER.info("populateKOReasons end");
	  return (koReasons);
	}

	/*private ArrayList populateMedLabRecords(String refNo) throws UnderWriterException, IUMException {
		
	  LOGGER.info("populateMedLabRecords start");
	  AssessmentRequest ar = new AssessmentRequest();
	  ArrayList medReqList = ar.getKOMedRecs(refNo);

	  int size = medReqList.size();
	  ArrayList medicalRecs = new ArrayList();

	  for (int i = 0; i < size; i++) {
		MedicalRecordData medData = (MedicalRecordData)medReqList.get(i);
		MedLabRecordsForm medRecForm = new MedLabRecordsForm();
		TestProfileData testData = medData.getTest();
		medRecForm.setCode(String.valueOf(testData.getTestId()));
		medRecForm.setTestDescription(testData.getTestDesc());

		String type = "";
		String labTestInd = medData.getLabTestInd();
		if (labTestInd != null) {
		  if (labTestInd.equals("Y")) {
			type = "LAB";
		  }
		  else if (labTestInd.equals("Y")) {
			type = "MED";
		  }
		}

		medRecForm.setType(type);
		medRecForm.setDateConducted((DateHelper.format(medData.getConductedDate(), "ddMMMyyyy")).toUpperCase());
		medRecForm.setDateReceived((DateHelper.format(medData.getReceivedDate(), "ddMMMyyyy")).toUpperCase());
		medicalRecs.add(medRecForm);
	  }
	  LOGGER.info("populateMedLabRecords end");
	  return (medicalRecs);
	}*/

	private RequirementForm populateRequirementDetails(String refNo, long reqId) throws Exception {

	  LOGGER.info("populateRequirementDetails start");
	  PolicyRequirements pr = new PolicyRequirements();

	  PolicyRequirementsData reqData = pr.getRequirement(reqId);

	  RequirementForm reqForm = new RequirementForm();
	  if (reqData != null) {
		  reqForm.setRefNo(reqData.getReferenceNumber());
		  reqForm.setReqId(Long.toString(reqId));
		  reqForm.setReqCode(reqData.getRequirementCode());
		  reqForm.setSeq(String.valueOf(reqData.getSequenceNumber()));
		  reqForm.setReqDesc(reqData.getReqDesc());
		  reqForm.setReqLevel(reqData.getLevel());
		  reqForm.setCreatedDate((DateHelper.format(reqData.getCreateDate(), "ddMMMyyyy")).toUpperCase());
		  reqForm.setCompleteReq(String.valueOf(reqData.isCompletedRequirementInd()));
		  
		  if (reqData.getStatus() != null) {
			  reqForm.setReqStatusCode(String.valueOf(reqData.getStatus().getStatusDesc()));
			  reqForm.setReqStatusValue(String.valueOf(reqData.getStatus().getStatusId()));
		  }
		  reqForm.setStatusDate((DateHelper.format(reqData.getStatusDate(), "ddMMMyyyy")).toUpperCase());
		  reqForm.setUpdatedBy(reqData.getUpdatedBy());
		  reqForm.setUpdatedDate((DateHelper.format(reqData.getUpdateDate(), "ddMMMyyyy")).toUpperCase());
		  reqForm.setClientType(reqData.getClientType());
		  reqForm.setDesignation(reqForm.getDesignation());
		  reqForm.setTestDate((DateHelper.format(reqData.getTestDate(), "ddMMMyyyy")).toUpperCase());
		  reqForm.setTestResult(reqData.getTestResultCode());
		  reqForm.setTestDesc(reqData.getTestResultDesc());
		  reqForm.setResolve(String.valueOf(reqData.isResolveInd()));
		 
		  reqForm.setFollowUpNo(String.valueOf(reqData.getFollowUpNumber()));
		  reqForm.setFollowUpDate((DateHelper.format(reqData.getFollowUpDate(), "ddMMMyyyy")).toUpperCase());
		  reqForm.setValidityDate((DateHelper.format(reqData.getValidityDate(), "ddMMMyyyy")).toUpperCase());
		  reqForm.setPaidInd(String.valueOf(reqData.isPaidInd()));
		  reqForm.setNewTest(ValueConverter.booleanToString(reqData.getNewTestOnly()));
		  reqForm.setOrderDate((DateHelper.format(reqData.getOrderDate(), "ddMMMyyyy")).toUpperCase());
		  reqForm.setDateSent((DateHelper.format(reqData.getDateSent(), "ddMMMyyyy")).toUpperCase());
		  reqForm.setCcasSuggest(ValueConverter.booleanToString(reqData.getCCASuggestInd()));
		  reqForm.setReceivedDate((DateHelper.format(reqData.getReceiveDate(), "ddMMMyyyy")).toUpperCase());
		  reqForm.setAutoOrder(ValueConverter.booleanToString(reqData.getAutoOrderInd()));
		  reqForm.setFldComm(ValueConverter.booleanToString(reqData.getFldCommentInd()));
		  reqForm.setComment(reqData.getComments());
		  reqForm.setClientNo(reqData.getClientId());
		  
		  reqForm.setImageReferenceNum(reqData.getImageRef());
		  
		  String fnURL = getWMSImageLink(reqData);
		  reqForm.setWmsImageLink(fnURL);
	  }
	  
	  LOGGER.info("populateRequirementDetails end");
	  return (reqForm);
	}
	
	private String getWMSImageLink(PolicyRequirementsData reqData) {
		
		
		ResourceBundle rb = ResourceBundle.getBundle(IUMConstants.IUM_WMS_CONFIG);
		String WMSServer = rb.getString("WMS_Server");
		String paramObjectStoreName = rb.getString("param_Object_Store_Name");
		String fnURL = null;
		int imageRef=0;
		if (reqData.getImageRef() != null){
			try{
				imageRef = Integer.parseInt(reqData.getImageRef().trim());
				if(imageRef!=0){
					fnURL = WMSServer + "/getContent?id=" + reqData.getImageRef() + "&objectStoreName=" + paramObjectStoreName + "&objectType=document";
				}
			} catch(NumberFormatException e){
				if(!reqData.getImageRef().trim().equals("Y")||!reqData.getImageRef().trim().equals("y")){
					fnURL = WMSServer + "/getContent?id=" + reqData.getImageRef() + "&objectStoreName=" + paramObjectStoreName + "&objectType=document";
				}
				LOGGER.error(CodeHelper.getStackTrace(e));
			}
		}
		
		return fnURL;
	}
	

	private void genericChangeStatus(CDSDetailForm arForm, String refNo, String userId, String lob, String branchId, String agentId, String toStatus, String sourceSystem) throws IUMException{

		LOGGER.info("genericChangeStatus start");
		String   statusTo        = arForm.getStatusTo();
		String[] reqId           = arForm.getReqId();
		String[] index           = arForm.getIndexTemp();
		String[] statusCode		 = arForm.getStatusCode();
		String[] followUpNo      = arForm.getFollowUpNo();
		String[] followUpDate    = arForm.getFollowUpDate();
		String[] seq             = arForm.getSeq();
		String[] level           = arForm.getReqLevel();
		String[] reqCodes        = arForm.getReqCodes();

		boolean                noErrors   = false;
		String                 pageReturn = "";
		String                 CHECKED    = "checked";
		ArrayList              listReq    = new ArrayList();
		PolicyRequirementsData data       = null;
		PolicyRequirements req;
		for (int i=0;i<reqId.length;i++) {
			if (index[i] != null) {
				if (index[i].equals(CHECKED)) {
					data = new PolicyRequirementsData();
					StatusData stat = new StatusData();
					data.setReferenceNumber(refNo);
					data.setRequirementCode(reqCodes[i]);
					if (statusCode[i] != null) {
						if (statusCode[i].trim().length() > 0) {
							stat.setStatusId(Long.parseLong(statusCode[i]));
						}
					}
					data.setStatus(stat);
					if (reqId[i] != null) {
						if (reqId[i].trim().length() > 0) {
							data.setRequirementId(Long.parseLong(reqId[i]));
						}
					}
					data.setUpdatedBy(userId);
					data.setUpdateDate(new Date());
					if (seq[i] != null) {
						if (seq[i].trim().length() > 0) {
							data.setSequenceNumber(Long.parseLong(seq[i]));
						}
					}
					data.setLevel(level[i]);
					listReq.add(data);
				}
			}
		}
		req  = new PolicyRequirements();

		AssessmentRequestData reqObj = new AssessmentRequestData();
		LOBData               lobObj = new LOBData();
		lobObj.setLOBCode(lob);
		reqObj.setLob(lobObj);
		SunLifeOfficeData     ofcObj = new SunLifeOfficeData();
		ofcObj.setOfficeId(branchId);
		reqObj.setBranch(ofcObj);
		UserProfileData       usrObj2 = new UserProfileData();
		usrObj2.setUserId(agentId);
		reqObj.setAgent(usrObj2);

		reqObj.setReferenceNumber(refNo);
		reqObj.setSourceSystem(sourceSystem);

		long newStatus = Long.parseLong(toStatus);
		req.genericPRChangeStatus(listReq, reqObj, newStatus);
		LOGGER.info("genericChangeStatus end");
	}

}


