package com.slocpi.ium.ui.action;

// struts package
import java.io.IOException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.ResourceBundle;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.slocpi.ium.IUMException;
import com.slocpi.ium.data.UserAccessData;
import com.slocpi.ium.ui.util.StateHandler;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.IUMConstants;
import com.slocpi.ium.util.SecuredHttpServletWrapper;
import com.slocpi.ium.util.UserManager;

/**
 * @TODO Class Description IUMAction
 * @author: Ma. Theresa Vicencio
 * @version: 1.0
 */
public abstract class IUMAction extends Action {
	private static final Logger BASE_LOGGER = LoggerFactory.getLogger(IUMAction.class);
	/**
	 * @TODO method description for execute
	 * @param mapping mapping of request to an instance of this class
	 * @param form object
	 * @param request request object
	 * @param response response object
	 * @exception IOException if an input/output error occurs
	 * @exception ServletException if a servlet exception occurs
	 * @return ActionForward as defined in the mapping parameter.
	 */
	public ActionForward execute(
			ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		BASE_LOGGER.info("execute start");
		String page = "";
		ActionForward forward = null;
		
		try {

			StateHandler sh = new StateHandler();
			SecuredHttpServletWrapper reqWrapper = new SecuredHttpServletWrapper(request);

			if (request.getQueryString() != null) {
				if (request.getMethod().equals("GET")) {
					Enumeration e = request.getParameterNames();
					while (e.hasMoreElements()) {
						String param = e.nextElement().toString();
						request.getParameterValues(param)[0] = 
								request.getParameter(param).replaceAll("<", "").replaceAll(">", "");
						try {
							request.getParameterValues(param)[0] = request.getParameterValues(param)[0].replaceAll("\"", "");
						} catch (Exception ex) {
							BASE_LOGGER.error(CodeHelper.getStackTrace(ex));
						}

						try {
							request.getParameterValues(param)[0] = 
									request.getParameterValues(param)[0].replaceAll("\\\\", "");
						} catch (Exception ex) {
							BASE_LOGGER.error("Error in getting parameter with double forward slash:");
							BASE_LOGGER.error(CodeHelper.getStackTrace(ex));
						}
					}
					forward = handleAction(mapping, form, request, response);
					

				} else {
					
					if (sh.getUserData(request) == null) {

						ResourceBundle rb = ResourceBundle.getBundle(IUMConstants.IUM_WMS_CONFIG);
						String wmsContext = rb.getString("wms_context_path");

						if (request.getContextPath().equalsIgnoreCase(wmsContext)) {
							forward = mapping.findForward("closeBrowser");
						} else {
							BASE_LOGGER.warn("*** session has expired ***");
							ActionErrors errors = new ActionErrors();
							errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("message.sessionexpire"));
							saveErrors(request, errors);
							forward = mapping.findForward("loginPage");
						}
					} else {
						forward = handleAction(mapping, form, request, response);
					}
				}
			} else {
				
				if (sh.getUserData(request) == null) {
					ResourceBundle rb = ResourceBundle.getBundle(IUMConstants.IUM_WMS_CONFIG);
					String wmsContext = rb.getString("wms_context_path");

					if (request.getContextPath().equalsIgnoreCase(wmsContext)) {
						forward = mapping.findForward("closeBrowser");
					} else {
						BASE_LOGGER.warn("*** session has expired ***");
						ActionErrors errors = new ActionErrors();
						errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("message.sessionexpire"));
						saveErrors(request, errors);
						forward = mapping.findForward("loginPage");
					}
				} else {
					forward = handleAction(mapping, form, request, response);
				}
			}
		}
		catch (Exception e) {
			BASE_LOGGER.error(CodeHelper.getStackTrace(e));
			request.setAttribute("error", e.getMessage());
		}
		
		BASE_LOGGER.info("execute end");
		return (forward);
	}// execute

	/**
	 * To be deleted
	 */
	protected UserAccessData extractAccessPriveleges(String userId, String pageId) throws IUMException {
		
		BASE_LOGGER.info("extractAccessPriveleges start");
		UserAccessData uad = new UserAccessData();
		uad.setAccessStatus("A");
		uad.setAccessCode("W");
		uad.setUserId(userId);
		BASE_LOGGER.info("extractAccessPriveleges end");
		
		return uad;
	}

	protected ArrayList getAccessPriveleges(String userId, String pageId) throws IUMException {
		
		BASE_LOGGER.info("getAccessPriveleges start");
		ArrayList userPageAccess = new ArrayList();
		UserManager userManager = new UserManager();
		
		try {
			userPageAccess = userManager.getUserPageAccess(userId, Long.parseLong(pageId));
		} catch (NumberFormatException e) {
			BASE_LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMException(e);
		} catch (IUMException e) {
			BASE_LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMException(e);
		}
		BASE_LOGGER.info("getAccessPriveleges end");
		return userPageAccess;
	}
	public abstract ActionForward handleAction(
			ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response) throws Exception;
}