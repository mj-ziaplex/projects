package com.slocpi.ium.ui.action;

import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.slocpi.ium.IUMException;
import com.slocpi.ium.data.LaboratoryData;
import com.slocpi.ium.data.UserData;
import com.slocpi.ium.data.UserProfileData;
import com.slocpi.ium.ui.form.LaboratoryForm;
import com.slocpi.ium.ui.util.StateHandler;
import com.slocpi.ium.underwriter.Reference;
import com.slocpi.ium.underwriter.UnderWriterException;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.DateHelper;

/**
 * @author ppe
 *
 * To change this generated comment edit the template variable "typecomment":
 * Window>Preferences>Java>Templates.
 * To enable and disable the creation of type comments go to
 * Window>Preferences>Java>Code Generation.
 */
public class CreateLaboratoryAction extends IUMAction {

	private static final Logger LOGGER = LoggerFactory.getLogger(CreateLaboratoryAction.class);
	/**
	 * @see com.slocpi.ium.ui.action.IUMAction#handleAction(org.apache.struts.action.ActionMapping, org.apache.struts.action.ActionForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	public ActionForward handleAction(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response)
		throws Exception {
		
		LOGGER.info("handleAction start");
		String page = "";
		page = "listLaboratoryPage";
		
		LaboratoryForm labForm = (LaboratoryForm)form;
		
		try
		{
			if(labForm.getActionType().equalsIgnoreCase("create"))
			{					
			}
			else if(labForm.getActionType().equalsIgnoreCase("save_create"))
			{			
				StateHandler sh = new StateHandler();
				UserData ud = sh.getUserData(request);
				UserProfileData profile = ud.getProfile();
			
				LaboratoryData labData = populateLaboratoryDetails(labForm, profile);
				Reference ref = new Reference();
				ref.createLaboratoryData(labData);
			}
						
			ListLaboratoryAction listLabAction = new ListLaboratoryAction();
			listLabAction.handleAction(mapping, form, request, response);
		} catch (UnderWriterException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
  			ActionErrors errors = new ActionErrors();
  			errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.admin.generic", e.getMessage()));
  			saveErrors(request, errors);  			
   			page = "listLaboratoryPage";
   			labForm.setActionType("create");
   			ListLaboratoryAction listLabAction = new ListLaboratoryAction();
			listLabAction.handleAction(mapping, labForm, request, response);
 		} 
 		catch (IUMException e) {
 			LOGGER.error(CodeHelper.getStackTrace(e));
  			ActionErrors errors = new ActionErrors();
  			errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.system.exception", e.getMessage()));
  			saveErrors(request, errors);
  			page = "errorPage";
 		}
 			catch (Exception e) {
 			LOGGER.error(CodeHelper.getStackTrace(e));
  			ActionErrors errors = new ActionErrors();
  			errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.system.exception", e.getMessage()));
  			saveErrors(request, errors);
  			page = "errorPage";  		
 		}	
				
		LOGGER.info("handleAction end");
		LOGGER.debug("PAGE FORWARD-->" + page);
		return (mapping.findForward(page));
	}
	
	private LaboratoryData populateLaboratoryDetails(LaboratoryForm labForm, UserProfileData profile)
	{
		LOGGER.info("populateLaboratoryDetails start");
		LaboratoryData labData = new LaboratoryData();
		
		labData.setLabId(Long.parseLong(labForm.getLabId()));
		labData.setLabName(labForm.getLabName());
		labData.setContactPerson(labForm.getContactPerson());
		labData.setBusinessAddrLine1(labForm.getBusAdd1());
		labData.setBusinessAddrLine2(labForm.getBusAdd2());
		labData.setBusinessAddrLine3(labForm.getBusAdd3());
		labData.setCity(labForm.getBusCity());
		labData.setCountry(labForm.getBusCountry());
		labData.setZipCode(labForm.getBusZipCode());
		labData.setOfficeNum(labForm.getOfcPhone1());
		labData.setOfficeNum2(labForm.getOfcPhone2());
		labData.setFaxNum(labForm.getFaxNumber());
		labData.setBranchName(labForm.getBranchName());
		labData.setBranchAddrLine1(labForm.getBranchAdd1());
		labData.setBranchAddrLine2(labForm.getBranchAdd2());
		labData.setBranchAddrLine3(labForm.getBranchAdd3());
		labData.setBranchCity(labForm.getBranchCity());
		labData.setBranchCountry(labForm.getBranchCountry());
		labData.setBranchZipCode(labForm.getBranchZipCode());						
		labData.setBranchPhone(labForm.getBranchPhone());
		labData.setBranchFax_num(labForm.getBranchFaxNumber());
		labData.setAccreditInd(labForm.getAccredited());
		
		if(!labForm.getAccreditedDate().equals(""))
		{
			labData.setAccreditDate(DateHelper.parse(labForm.getAccreditedDate(), "ddMMMyyyy"));
		}	
		
		labData.setCreatedDate(new Date(System.currentTimeMillis()));
		labData.setCreatedBy(profile);
		labData.setEmailAddress(labForm.getEmailAddress());
		LOGGER.info("populateLaboratoryDetails end");
		return labData;
	}
	
}
