/*
 * Created on Mar 1, 2004
 * package name = com.slocpi.ium.ui.action
 * file name    = RemoveRoleFromUserAction.java
 */
package com.slocpi.ium.ui.action;

import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.slocpi.ium.data.RolesData;
import com.slocpi.ium.data.UserData;
import com.slocpi.ium.data.UserProfileData;
import com.slocpi.ium.ui.form.UserProfileForm;
import com.slocpi.ium.ui.util.StateHandler;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.UserManager;

/**
 * TODO Class Description of RemoveRoleFromUserAction.java
 * @author Engel
 * 
 */
public class RemoveRoleFromUserAction extends IUMAction {
	private static final Logger LOGGER = LoggerFactory.getLogger(RemoveRoleFromUserAction.class);
	/* (non-Javadoc)
	 * @see com.slocpi.ium.ui.action.IUMAction#handleAction(org.apache.struts.action.ActionMapping, org.apache.struts.action.ActionForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	public ActionForward handleAction(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response)
		throws Exception {

		   LOGGER.info("handleAction start");
			String page="";
			try{		
				UserProfileForm upform = (UserProfileForm)form;
				StateHandler sh = new StateHandler();
				UserData userlogged = sh.getUserData(request);
				UserProfileData udata= userlogged.getProfile();
			
				UserManager usrmgr = new UserManager();
				ArrayList roles = listRoles(upform.getUsrRoles());
				usrmgr.removeRolesFromUser(upform.getUserId(),udata.getUserId(),roles);
				
				UserData userData = new UserData();
				userData = usrmgr.retrieveUserDetails(upform.getUserId());
				ArrayList updatedRoles = userData.getRoles();
				String defaultRole = userData.getProfile().getRole();
				int size = updatedRoles.size();
				switch(size){
					case 0:
						usrmgr.updateDefaultRole(null,upform.getUserId(),udata.getUserId() );  
						break;
					case 1:
						usrmgr.updateDefaultRole( ((RolesData)updatedRoles.get(0)).getRolesId(),upform.getUserId(), udata.getUserId() );  
						break;
					default:
						boolean hasDefaultRole=false;
						for(int i=0; i<size; i++){
							String role = ((RolesData)updatedRoles.get(i)).getRolesId();
							if(role.equalsIgnoreCase(defaultRole)){
								hasDefaultRole=true;
								break;															
							}
							
						}
						if(hasDefaultRole=false){
							usrmgr.updateDefaultRole(null,upform.getUserId(),udata.getUserId());
						}
						break;
					 
				}

				page="userProfileDetailPage";
				
			}catch (Exception e) {
				LOGGER.error(CodeHelper.getStackTrace(e));
				ActionErrors errors = new ActionErrors();
				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.system.exception", e.getMessage()));
				saveErrors(request, errors); 
				page= "errorPage";
			}		
			LOGGER.info("handleAction end");
			LOGGER.debug("PAGE FORWARD-->" + page);
			return (mapping.findForward(page));
	}

	private ArrayList listRoles(String roles[]){
	
	    LOGGER.info("listRoles start");
		ArrayList result = new ArrayList();
		
		for(int i=0;i<roles.length;i++){
			result.add(roles[i]); 
		}
		LOGGER.info("listRoles end");
		return result;
	}
	
}
