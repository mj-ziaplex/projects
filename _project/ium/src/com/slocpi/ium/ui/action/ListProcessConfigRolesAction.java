package com.slocpi.ium.ui.action;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.StringTokenizer;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.slocpi.ium.data.RolesData;
import com.slocpi.ium.ui.form.ProcessConfigRoleForm;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.workflow.ProcessConfigurator;

/**
 * ListRolesAction.java 
 * Description:
 *
 * @version $Revision: 1.1 $
 * @author $Author: pw12 $ $Date: 2012/08/01 07:07:19 $
 */
public class ListProcessConfigRolesAction extends IUMAction{
	private static final Logger LOGGER = LoggerFactory.getLogger(ListProcessConfigRolesAction.class);
	/**
	 * @see com.slocpi.ium.ui.action.IUMAction#handleAction(org.apache.struts.action.ActionMapping, org.apache.struts.action.ActionForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	public ActionForward handleAction(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response)
		throws Exception {
		
		LOGGER.info("handleAction start");
		String page = "";
		
		try{
			
			String roles = request.getParameter("roles");
			String flagRoles = request.getParameter("flagRoles");
			String lob = request.getParameter("lob");
			int eventId = 0;
			
			if((request.getParameter("eventId")) != null && !(request.getParameter("eventId").equals(""))){
				eventId = Integer.parseInt(request.getParameter("eventId"));
			}else{
				
			}
			
			ProcessConfigRoleForm pcrForm = (ProcessConfigRoleForm) form;
			if(flagRoles==null || flagRoles.equals("")){
				this.userHasNotSelected(pcrForm,lob,eventId);
			}else{
				this.userHasSelected(pcrForm,lob,eventId,roles);
			}
			
			request.setAttribute("processConfigRole",pcrForm);					
			
			page = "listProcessConfigRoles";
									
		}catch(Exception e){
			LOGGER.error(CodeHelper.getStackTrace(e));
			ActionErrors errors = new ActionErrors();
			errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.system.exception", e.getMessage()));
			saveErrors(request, errors);
			page = "errorPage";
		}
		LOGGER.info("handleAction end");
		LOGGER.debug("PAGE FORWARD-->" + page);
		return (mapping.findForward(page));
	}

	private void userHasNotSelected(ProcessConfigRoleForm pcrForm,String lob, int eventId) throws Exception{
		LOGGER.info("userHasNotSelected start");
		pcrForm.setAssignedRoles(this.getAssignedRoles(lob,eventId));
		pcrForm.setAvailableRoles(this.getAvailableRoles(lob,eventId));
		LOGGER.info("userHasNotSelected end");
	}
	
	private void userHasSelected(ProcessConfigRoleForm pcrForm,String lob, int eventId, String roles) throws Exception{
		
		LOGGER.info("userHasSelected start");
		ArrayList allRoles = new ArrayList();
		allRoles.addAll(this.getAssignedRoles(lob,eventId));
		allRoles.addAll(this.getAvailableRoles(lob,eventId));
		HashMap selectedRoleCodes = this.retrieveSelectedRoleCodes(roles);
		ArrayList fromUserAssigned = new ArrayList();
		ArrayList fromUserAvailable = new ArrayList();
		
		for(int i=0; i<allRoles.size(); i++){
			RolesData role = (RolesData) allRoles.get(i);
			if(selectedRoleCodes.containsKey(role.getRolesId())){
				fromUserAssigned.add(role);
			}else{
				fromUserAvailable.add(role);
			}
		}										
		pcrForm.setAssignedRoles(fromUserAssigned);
		pcrForm.setAvailableRoles(fromUserAvailable);
		LOGGER.info("userHasSelected end");
	}
	
		
	private HashMap retrieveSelectedRoleCodes(String roles){
		
		LOGGER.info("retrieveSelectedRoleCodes start");
		HashMap roleCodes = new HashMap();
		StringTokenizer token = new StringTokenizer(roles,"~");
		while(token.hasMoreTokens()){
			String code = token.nextToken();
			roleCodes.put(code,code);
		}
		LOGGER.info("retrieveSelectedRoleCodes end");
		return roleCodes;
	}
		
	private ArrayList getAvailableRoles(String lob, int eventId) throws Exception{
		
		LOGGER.info("getAvailableRoles start");
		ProcessConfigurator pc = new ProcessConfigurator();
		ArrayList list = pc.getAvailableRoles(lob,eventId);
		LOGGER.info("getAvailableRoles end");
		return list;
		
	}
	
	private ArrayList getAssignedRoles(String lob, int eventId) throws Exception{
		
		LOGGER.info("getAssignedRoles start");
		ProcessConfigurator pc = new ProcessConfigurator();
		ArrayList list = pc.getAssignedRoles(lob,eventId);
		LOGGER.info("getAssignedRoles end");
		return list;
		
	}
	
	
}
