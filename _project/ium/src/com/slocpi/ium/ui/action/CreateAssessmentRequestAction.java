/*
 * Created on Jan 12, 2004
 * package name = com.slocpi.ium.ui.action
 * file name    = CreateAssessmentRequestAction.java
 */
package com.slocpi.ium.ui.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.slocpi.ium.IUMException;
import com.slocpi.ium.data.ClientData;
import com.slocpi.ium.data.StatusData;
import com.slocpi.ium.data.UserData;
import com.slocpi.ium.data.UserProfileData;
import com.slocpi.ium.ui.form.AssessmentRequestForm;
import com.slocpi.ium.ui.util.StateHandler;
import com.slocpi.ium.underwriter.AssessmentRequest;
import com.slocpi.ium.underwriter.User;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.DateHelper;
import com.slocpi.ium.util.IUMConstants;
import com.slocpi.ium.workflow.NotificationBuilder;
import com.slocpi.ium.workflow.Workflow;
import com.slocpi.ium.workflow.WorkflowItem;

/**
 * TODO Class Description of CreateAssessmentRequestAction.java
 * 
 * @author Engel
 * 
 */
public class CreateAssessmentRequestAction extends IUMAction {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(CreateAssessmentRequestAction.class);

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.slocpi.ium.ui.action.IUMAction#handleAction(org.apache.struts.action
	 * .ActionMapping, org.apache.struts.action.ActionForm,
	 * javax.servlet.http.HttpServletRequest,
	 * javax.servlet.http.HttpServletResponse)
	 */
	public ActionForward handleAction(
			ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		LOGGER.info("handleAction start");
		
		sampleTestSMS();

		String page = "";
		try {
			
			page = "createAssessmentRequestsPage";
			StateHandler sessionHandler = new StateHandler();
			UserData userData = sessionHandler.getUserData(request);
			UserProfileData userPref = userData.getProfile();
			AssessmentRequestForm arf = (AssessmentRequestForm) form;

			if (arf == null
					|| (arf.getLocation() == null || arf.getLocation().equals(""))) {
				arf = setForm(arf, userPref.getUserId());
			}

			if (arf.getClientToSearch().equalsIgnoreCase(IUMConstants.CLIENT_TYPE_INSURED)) {
				retrieveInsuredClientData(arf);
			} else if (arf.getClientToSearch().equalsIgnoreCase(IUMConstants.CLIENT_TYPE_OWNER)) {
				retrieveOwnerClientData(arf);
			} else if (arf.getClientToSearch().equalsIgnoreCase(IUMConstants.CLIENT_TYPE_INSURED + IUMConstants.CLIENT_TYPE_OWNER)) {
				retrieveInsuredClientData(arf);
				retrieveOwnerClientData(arf);
			}

			User us = new User();
			arf.setAgentBranches(us.getAgentBranches());
			request.setAttribute("requestForm", arf);
			
		} catch (Exception e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			ActionErrors errors = new ActionErrors();
			errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.system.exception", e.getMessage()));
			saveErrors(request, errors);
			return (mapping.findForward("errorPage"));
		}
		LOGGER.info("handleAction end");
		LOGGER.debug("PAGE FORWARD-->" + page);
		return (mapping.findForward(page));
	}

	public void sampleTestSMS() {
		
		LOGGER.info("sampleTestSMS start");
		WorkflowItem wfi = new WorkflowItem();
		wfi.setType(IUMConstants.POLICY_REQUIREMENTS);
		wfi.setObjectID("124857");
		wfi.setLOB("IL");
		wfi.setPreviousStatus("24");
		wfi.setSender("JDACA");
		Workflow wf = new Workflow();
		
		try {
			
			Object reqObj;
			try {
				
				reqObj = wf.tmpGetObject(wfi);
				if (reqObj != null) {
					LOGGER.debug("reqObj is not null");
				} else {
					LOGGER.debug("reqObj is  null");
				}
				
				Class objClass = reqObj.getClass();

				if (wfi.getSpecialCase() == null) {
					
					StatusData statusData = (StatusData) objClass.getDeclaredMethod(IUMConstants.STATUS, null).invoke(reqObj, null);
					String currStatus = String.valueOf(statusData.getStatusId());
					wfi.setCurrentStatus(currStatus);
					
				} else if (wfi.getSpecialCase().equalsIgnoreCase(IUMConstants.RECEIVED_REQUIREMENT)) {
					wfi.setCurrentStatus(String.valueOf(IUMConstants.STATUS_RECEIVED_IN_SITE));
				} else {
					wfi.setCurrentStatus(String.valueOf(IUMConstants.STATUS_ORDERED));
				}
				
				String notificationInfo[] = wf.tmpGetNotificationDetails(wfi);
				if (notificationInfo == null) {
					LOGGER.debug("notificationInfo is null");
				} else {
					LOGGER.debug("notificationInfo is not  null" + notificationInfo.length);
				}
				notificationInfo[0] = "1";
				notificationInfo[1] = "7";
				
				if (notificationInfo != null
						&& notificationInfo[1] != null
						&& notificationInfo[0] != null
						&& notificationInfo[2] != null
						&& IUMConstants.YES.equalsIgnoreCase(notificationInfo[0])) {
					
					NotificationBuilder nb = new NotificationBuilder();
					nb.sendNotification(reqObj, wfi, notificationInfo[1], notificationInfo[2]);
				} else {
					LOGGER.debug("There are no Notification Info Retrieved");
				}

			} catch (Exception e) {
				LOGGER.error(CodeHelper.getStackTrace(e));
				e.printStackTrace();
			}

		} catch (Exception e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
		}

		LOGGER.info("sampleTestSMS end");
	}

	private AssessmentRequestForm setForm(
			AssessmentRequestForm arf,
			String userId) throws IUMException {
		
		LOGGER.info("setForm start");
		if (arf == null) {
			arf = new AssessmentRequestForm();
		}
		arf.setLocation(userId);
		arf.setRequestStatus(String.valueOf(IUMConstants.STATUS_NB_REVIEW_ACTION));
		arf.setUnderwriter(""); 
		LOGGER.info("setForm end");
		return arf;

	}

	private void retrieveInsuredClientData(AssessmentRequestForm arf) throws IUMException {
		
		LOGGER.info("retrieveInsuredClientData start");
		arf.setInsuredClientNo(arf.getInsuredClientNo().toUpperCase());
		AssessmentRequest ar = new AssessmentRequest();
		ClientData cdata = ar.getClientData(arf.getInsuredClientNo());
		if (cdata != null && cdata.getGivenName() != null
				&& !cdata.getGivenName().equals("")) {
			
			if (cdata.getAge() == 0) {
				arf.setInsuredAge("");
			} else {
				arf.setInsuredAge(String.valueOf(cdata.getAge()));
			}

			if (cdata.getBirthDate() != null) {
				arf.setInsuredBirthDate((DateHelper.format(cdata.getBirthDate(), "ddMMMyyyy")).toUpperCase());
			} else {
				arf.setInsuredBirthDate("");
			}
			
			if (cdata.getGivenName() != null) {
				arf.setInsuredFirstName(cdata.getGivenName());
			} else {
				arf.setInsuredFirstName("");
			}
			
			if (cdata.getLastName() != null) {
				arf.setInsuredLastName(cdata.getLastName());
			} else {
				arf.setInsuredLastName("");
			}
			
			if (cdata.getMiddleName() != null) {
				arf.setInsuredMiddleName(cdata.getMiddleName());
			} else {
				arf.setInsuredMiddleName("");
			}
			
			if (cdata.getTitle() != null) {
				arf.setInsuredTitle(cdata.getTitle());
			} else {
				arf.setInsuredTitle("");
			}
			
			if (cdata.getSex() != null) {
				arf.setInsuredSex(cdata.getSex());
			} else {
				arf.setInsuredSex("");
			}
			
			if (cdata.getSuffix() != null) {
				arf.setInsuredSuffix(cdata.getSuffix());
			} else {
				arf.setInsuredSuffix("");
			}
			
			arf.setInsuredOriginal(arf.getInsuredClientNo()
					+ arf.getInsuredFirstName() + arf.getInsuredLastName()
					+ arf.getInsuredMiddleName() + arf.getInsuredTitle()
					+ arf.getInsuredSuffix() + arf.getInsuredSex());
		} else {
			arf.setInsuredAge("");
			arf.setInsuredFirstName("");
			arf.setInsuredMiddleName("");
			arf.setInsuredLastName("");
			arf.setInsuredBirthDate("");
			arf.setInsuredSuffix("");
			arf.setInsuredTitle("");
			arf.setInsuredSex("");
			arf.setInsuredOriginal("");
		}
		LOGGER.info("retrieveInsuredClientData end");
	}

	private void retrieveOwnerClientData(AssessmentRequestForm arf) throws IUMException {
		
		LOGGER.info("retrieveOwnerClientData start");
		arf.setOwnerClientNo(arf.getOwnerClientNo().toUpperCase());
		AssessmentRequest ar = new AssessmentRequest();
		ClientData cdata = ar.getClientData(arf.getOwnerClientNo());
		
		if (cdata != null && cdata.getGivenName() != null
				&& !cdata.getGivenName().equals("")) {

			if (cdata.getAge() == 0) {
				arf.setOwnerAge("");
			} else {
				arf.setOwnerAge(String.valueOf(cdata.getAge()));
			}

			if (cdata.getBirthDate() != null) {
				arf.setOwnerBirthDate((DateHelper.format(cdata.getBirthDate(), "ddMMMyyyy")).toUpperCase());
			} else {
				arf.setOwnerBirthDate("");
			}
			
			if (cdata.getGivenName() != null) {
				arf.setOwnerFirstName(cdata.getGivenName());
			} else {
				arf.setOwnerFirstName("");
			}
			
			if (cdata.getLastName() != null) {
				arf.setOwnerLastName(cdata.getLastName());
			} else {
				arf.setOwnerLastName("");
			}
			
			if (cdata.getMiddleName() != null) {
				arf.setOwnerMiddleName(cdata.getMiddleName());
			} else {
				arf.setOwnerMiddleName("");
			}
			
			if (cdata.getTitle() != null) {
				arf.setOwnerTitle(cdata.getTitle());
			} else {
				arf.setOwnerTitle("");
			}
			
			if (cdata.getSex() != null) {
				arf.setOwnerSex(cdata.getSex());
			} else {
				arf.setOwnerSex("");
			}
			
			if (cdata.getSuffix() != null) {
				arf.setOwnerSuffix(cdata.getSuffix());
			} else {
				arf.setOwnerSuffix("");
			}
			
			arf.setOwnerOriginal(arf.getOwnerClientNo()
					+ arf.getOwnerFirstName() + arf.getOwnerLastName()
					+ arf.getOwnerMiddleName() + arf.getOwnerTitle()
					+ arf.getOwnerSuffix() + arf.getOwnerSex());
		} else {
			arf.setOwnerAge("");
			arf.setOwnerFirstName("");
			arf.setOwnerMiddleName("");
			arf.setOwnerLastName("");
			arf.setOwnerBirthDate("");
			arf.setOwnerSuffix("");
			arf.setOwnerTitle("");
			arf.setOwnerSex("");
			arf.setOwnerOriginal("");
		}
		LOGGER.info("retrieveOwnerClientData end");
	}

}
