/*
 * Created on Apr 5, 2006
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.ui.action;

import java.util.ArrayList;
import java.util.Iterator;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import com.slocpi.ium.IUMException;
import com.slocpi.ium.data.AssessmentRequestData;
import com.slocpi.ium.data.KickOutMessageData;
import com.slocpi.ium.data.MedicalRecordData;
import com.slocpi.ium.data.RequestFilterData;
import com.slocpi.ium.data.TestProfileData;
import com.slocpi.ium.data.UserData;
import com.slocpi.ium.ui.form.CDSDetailForm;
import com.slocpi.ium.ui.form.FormLoader;
import com.slocpi.ium.ui.form.KOReasonsForm;
import com.slocpi.ium.ui.form.KOReqMedLabDetailForm;
import com.slocpi.ium.ui.form.MedLabRecordsForm;
import com.slocpi.ium.ui.form.RequestFilterForm;
import com.slocpi.ium.ui.form.RequirementForm;
import com.slocpi.ium.underwriter.AssessmentRequest;
import com.slocpi.ium.underwriter.KickOutMessage;
import com.slocpi.ium.underwriter.UnderWriterException;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.DateHelper;
import com.slocpi.ium.util.UserManager;
import com.slocpi.ium.util.ValueConverter;

/**
 * @author rjuan
 * 
 *         To change the template for this generated type comment go to
 *         Window>Preferences>Java>Code Generation>Code and Comments CR#10150 -
 *         Generic Reassignment access
 * 
 */
public class ViewKOReqMedLabSpecialAction extends IUMAction {
	private static final Logger LOGGER = LoggerFactory.getLogger(ViewKOReqMedLabSpecialAction.class);
	
	
	private HttpServletRequest request;

	public HttpServletRequest getRequest() {
		return request;
	}

	public void setRequest(HttpServletRequest request) {
		this.request = request;
	}

	public ActionForward handleAction(
			ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		LOGGER.info("handleAction start");
		String page = "";

		try {

			HttpSession session = request.getSession();
			String session_id = "";
			if (request.getAttribute("session_id") != null) {
				session_id = request.getAttribute("session_id").toString();
			} else if (request.getParameter("session_id") != null
					|| !"".equals(request.getParameter("session_id"))) {				
				session_id = request.getParameter("session_id");
			}

			request.setAttribute("session_id", session_id);
			
			if (request.getAttribute("firstListReqRedirectFlag") == null) {

				if (session == null) {
					LOGGER.debug("request.getSession() is null");
				} else {
					LOGGER.debug("request.getSession() is not null");
					session.setAttribute("savedSection_" + session_id, "DetailsOfRequirementDetails");
				}
			}

			AssessmentRequest assessmentReq = new AssessmentRequest();
			RequestFilterForm reqFilterForm = (RequestFilterForm) form;
			RequestFilterData reqFilterData = new RequestFilterData();

			if (!reqFilterForm.getClickedRefNo().equals("none")) {
				reqFilterForm.setRefNo(reqFilterForm.getClickedRefNo());
			}

			reqFilterData = setRequestFilterData(reqFilterForm);

			ArrayList result = null;
			if (request.getSession().getAttribute("assessmentReqListResult_" + reqFilterForm.getRefNo()) != null) {
				result = (ArrayList) request.getSession().getAttribute("assessmentReqListResult_" + reqFilterForm.getRefNo());
			} else {
				result = assessmentReq.getRequestList(reqFilterData);
			}
			

			if (result != null && !result.isEmpty() && result.size() > 0)
				reqFilterForm.setRefNo(((AssessmentRequestData) result.get(0)).getReferenceNumber());

			reqFilterForm.setLob(((AssessmentRequestData) result.get(0)).getLob().toString());
			request.getSession().setAttribute("reqFilterForm", reqFilterForm);
			
			
			String refNo = reqFilterForm.getRefNo();
			String isMaintain = request.getParameter("isMaintain"); 

			KOReqMedLabDetailForm koReqMedLabDetailForm = new KOReqMedLabDetailForm();
			koReqMedLabDetailForm.setRefNo(reqFilterForm.getRefNo());
			koReqMedLabDetailForm.setLob(reqFilterForm.getLob());
			
			if ((KOReqMedLabDetailForm) request.getSession().getAttribute("KOReqMedLabDetailForm_" + refNo) == null) {

				koReqMedLabDetailForm = (KOReqMedLabDetailForm) FormLoader.loadAssessmentRequest(koReqMedLabDetailForm, request);
				setRequest(request);
			} else {
				koReqMedLabDetailForm = (KOReqMedLabDetailForm) request.getSession().getAttribute("KOReqMedLabDetailForm_" + refNo);
			}

			request.getSession().setAttribute("KOReqMedLabDetailForm_" + refNo, koReqMedLabDetailForm);
			request.setAttribute("branchName", koReqMedLabDetailForm.getBranchName());
			request.setAttribute("underwriterName", koReqMedLabDetailForm.getUnderwriterName());
			request.setAttribute("agentName", koReqMedLabDetailForm.getAgentName());
			request.setAttribute("lobDesc", koReqMedLabDetailForm.getLobDesc());

			if (request.getSession().getAttribute( "userProfile_" + koReqMedLabDetailForm.getAssignedTo()) == null) {
				
				UserData ud = new UserManager().retrieveUserDetails(koReqMedLabDetailForm.getUnderwriter());
				request.getSession().setAttribute("userProfile_" + koReqMedLabDetailForm.getAssignedTo(), ud);
				request.setAttribute("userProfile_" + koReqMedLabDetailForm.getAssignedTo(), ud);
			}
			
			ArrayList medLabRecords = null;
			if (request.getSession().getAttribute("medLabRecords_" + refNo) == null) {
				medLabRecords = populateMedLabRecords(koReqMedLabDetailForm.getInsuredClientNo());
			} else {
				medLabRecords = (ArrayList) request.getSession().getAttribute("medLabRecords_" + refNo);
			}

			request.getSession().setAttribute("medLabRecords_" + refNo, medLabRecords);
			
			ArrayList koReasons = null;
			if (request.getSession().getAttribute("koReasons_" + refNo) == null) {
				
				koReasons = populateKOReasons(refNo);
				
			} else {
				
				koReasons = (ArrayList) request.getSession().getAttribute("koReasons_" + refNo);
			}
			request.getSession().setAttribute("koReasons_" + refNo, koReasons);
			
			ArrayList requirements = new ArrayList(); 
			ArrayList reqLevelList = new ArrayList(); 
			
			koReqMedLabDetailForm.setRequirements(requirements);
			koReqMedLabDetailForm.setMedLabRecords(medLabRecords);
			koReqMedLabDetailForm.setKoReasons(koReasons);
			koReqMedLabDetailForm.setReqLevelList(reqLevelList);

			RequirementForm requirementForm = new RequirementForm(); 
			request.setAttribute("requirementForm", requirementForm);

			session.setAttribute("detailForm", koReqMedLabDetailForm);
			
			CDSDetailForm cdsDetailForm = new CDSDetailForm();
			cdsDetailForm.setRefNo(reqFilterForm.getRefNo());
			cdsDetailForm.setPolicySuffix(koReqMedLabDetailForm.getPolicySuffix());
			cdsDetailForm.setLob(koReqMedLabDetailForm.getLob());
			cdsDetailForm.setPolicySuffix(reqFilterForm.getLob());
			cdsDetailForm.setLob(reqFilterForm.getLob());

			session.setAttribute("cdsDetailForm_" + session_id, cdsDetailForm);

			page = "cdsDetailPage";

			if (assessmentReq.isKOCountExceeded(refNo)
					&& page.equals("cdsDetailPage")) {
				
				ActionMessages warning = new ActionMessages();
				warning.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("message.kOCountExceeded"));
				saveMessages(request, warning);
			}

			if (assessmentReq.isReqtCountExceeded(refNo)
					&& page.equals("koReqMedLabDetailPage")) {
				
				ActionMessages warning = new ActionMessages();
				warning.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("message.reqtCountExceeded"));
				saveMessages(request, warning);
			}
			
		} catch (UnderWriterException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			ActionErrors errors = new ActionErrors();
			errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.request.generic", e.getMessage()));
			saveErrors(request, errors);
			page = "koReqMedLabDetailPage";
		} catch (IUMException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			ActionErrors errors = new ActionErrors();
			errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.system.exception", e.getMessage()));
			saveErrors(request, errors);
			page = "errorPage";
		} catch (Exception e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			ActionErrors errors = new ActionErrors();
			errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.system.exception", e.getMessage()));
			saveErrors(request, errors);
			page = "errorPage";
		}

		LOGGER.info("handleAction end");
		LOGGER.debug("PAGE FORWARD-->" + page);
		return (mapping.findForward(page));
	}

	
	private RequestFilterData setRequestFilterData(
			RequestFilterForm reqFilterForm) throws Exception {

		LOGGER.info("setRequestFilterData start");
		RequestFilterData reqFilterData = new RequestFilterData();

		if (reqFilterForm.getRefNo() != null) {
			reqFilterData.setReferenceNumber(reqFilterForm.getRefNo()
					.toUpperCase());
		}

		reqFilterData.setAgent(reqFilterForm.getAgent());

		if (reqFilterForm.getClientNumber() != null) {
			reqFilterData.setClientNumber(reqFilterForm.getClientNumber().toUpperCase());
		}

		reqFilterData.setLob(reqFilterForm.getLob());
		reqFilterData.setStatus(reqFilterForm.getStatus());
		reqFilterData.setAssignedTo(reqFilterForm.getAssignedTo());
		reqFilterData.setRequirementCode(reqFilterForm.getRequirementCode());
		reqFilterData.setNameOfInsured(reqFilterForm.getNameOfInsured());
		reqFilterData.setBranch(reqFilterForm.getBranch());
		reqFilterData.setApplicationReceivedDate(DateHelper.parse(reqFilterForm.getApplicationReceivedDate(), "ddMMMyyyy"));
		reqFilterData.setLocation(reqFilterForm.getLocation());
		reqFilterData.setCoverageAmount(ValueConverter.convertCurrency(reqFilterForm.getCoverageAmount()));
		reqFilterData.setSortOrder(reqFilterForm.getSortOrder());

		reqFilterData.setColumns(reqFilterForm.getColumns());
		reqFilterData.setRequirementCode(reqFilterForm.getRequirementCode());
		LOGGER.info("setRequestFilterData end");
		return reqFilterData;
	}

	/**
	 * @throws Exception
	 * @deprecated use FormLoader.loadAssessmentRequest() method
	 */
	private ArrayList populateMedLabRecords(String clientNo) throws Exception {

		LOGGER.info("populateMedLabRecords start");
		ArrayList medicalRecs = new ArrayList();

		AssessmentRequest ar = new AssessmentRequest();
		ArrayList medReqList = ar.getKOMedRecs(clientNo);

		if (medReqList != null && !medReqList.isEmpty()) {
			
			Iterator it = medReqList.iterator();
			
			while (it.hasNext()) {
				
				MedicalRecordData medData = (MedicalRecordData) it.next();
				MedLabRecordsForm medRecForm = new MedLabRecordsForm();
				TestProfileData testData = medData.getTest();
				medRecForm.setCode(String.valueOf(testData.getTestId()));
				medRecForm.setTestDescription(testData.getTestDesc());

				String type = "";
				String labTestInd = medData.getLabTestInd();
				type = labTestInd;

				medRecForm.setType(type);
				medRecForm.setDateConducted((DateHelper.format(medData.getConductedDate(), "ddMMMyyyy")).toUpperCase());
				medRecForm.setDateReceived((DateHelper.format(medData.getReceivedDate(), "ddMMMyyyy")).toUpperCase());
				medicalRecs.add(medRecForm);
			}
		}
		
		LOGGER.info("populateMedLabRecords end");
		return (medicalRecs);
	}

	
	private ArrayList populateKOReasons(String refNo) throws Exception {
		
		LOGGER.info("populateKOReasons start");
		
		ArrayList koReasons = new ArrayList();

		KickOutMessage ko = new KickOutMessage();
		ArrayList reqList = ko.getMessages(refNo);

		if (reqList != null && !reqList.isEmpty()) {
			
			Iterator it = reqList.iterator();
			
			while (it.hasNext()) {

				KickOutMessageData koData = (KickOutMessageData) it.next();
				KOReasonsForm koForm = new KOReasonsForm();
				koForm.setSeq(String.valueOf(koData.getSequenceNumber()));
				koForm.setMessageText(koData.getMessageText());
				koForm.setClientNo(koData.getClientId());
				koForm.setFailResponse(koData.getFailResponse());
				koReasons.add(koForm);
			}
		}
		
		LOGGER.info("populateKOReasons end");
		return (koReasons);
	}

	


}
