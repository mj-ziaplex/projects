package com.slocpi.ium.ui.action;

// struts package
import java.io.IOException;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.slocpi.ium.IUMException;
import com.slocpi.ium.data.AssessmentRequestData;
import com.slocpi.ium.data.ClientData;
import com.slocpi.ium.data.ClientDataSheetData;
import com.slocpi.ium.data.LOBData;
import com.slocpi.ium.data.MedicalNotes;
import com.slocpi.ium.data.StatusData;
import com.slocpi.ium.data.SunLifeOfficeData;
import com.slocpi.ium.data.UWAssessmentData;
import com.slocpi.ium.data.UserData;
import com.slocpi.ium.data.UserProfileData;
import com.slocpi.ium.ui.form.DiscussionForm;
import com.slocpi.ium.ui.form.DoctorsNotesDetailForm;
import com.slocpi.ium.ui.form.FormLoader;
import com.slocpi.ium.ui.util.Page;
import com.slocpi.ium.ui.util.Pagination;
import com.slocpi.ium.ui.util.StateHandler;
import com.slocpi.ium.underwriter.AssessmentRequest;
import com.slocpi.ium.underwriter.DoctorsNotes;
import com.slocpi.ium.underwriter.UnderWriterException;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.DateHelper;
import com.slocpi.ium.util.IUMConstants;
import com.slocpi.ium.util.ValueConverter;


/**
 * @TODO Class Description ViewDoctorsNotesAction
 * @author: Ma. Theresa Vicencio
 * @version: 1.0
 */
public class ViewDoctorsNotesAction extends IUMAction {

	private static final Logger LOGGER = LoggerFactory.getLogger(ViewDoctorsNotesAction.class);
  /**
   * @TODO method description for execute
   * @param mapping mapping of request to an instance of this class
   * @param form  object
   * @param request request object
   * @param response response object
   * @exception IOException if an input/output error occurs
   * @exception ServletException if a servlet exception occurs
   * @return ActionForward as defined in the mapping parameter.
   */
  public ActionForward handleAction(ActionMapping mapping,
							   ActionForm form,
							   HttpServletRequest request,
							   HttpServletResponse response)
							   throws Exception {
	
	LOGGER.info("handleAction start");
	String page = "";
	try {
		
		final Date start = new Date();
		Date end;
		long elapse;
		
		StateHandler sh = new StateHandler();
		UserData ud = sh.getUserData(request);
		UserProfileData profile = ud.getProfile();
		String userId = profile.getUserId();
		String pageId = "";

		String role = profile.getRole();
		

		DoctorsNotesDetailForm doctorsNotesDetailForm = (DoctorsNotesDetailForm) form;

		if (doctorsNotesDetailForm == null) {
			doctorsNotesDetailForm = new DoctorsNotesDetailForm();
		}

		String refNo = doctorsNotesDetailForm.getRefNo();
		ArrayList discussionList = populateDiscussionList(refNo);


		int recPerView = profile.getRecordsPerView();
		
		if (recPerView == 0) {
		  recPerView = 1;
		}
		int pageNo = 1;
		String reqPageNum = request.getParameter("pageNo");
		
		if ((reqPageNum != null) && !(reqPageNum.equals(""))) {
			try {
				pageNo = Integer.parseInt(reqPageNum);
			}
			catch (NumberFormatException e) {
				//pageNo will be the first page
				LOGGER.error(CodeHelper.getStackTrace(e));
			}
		}

		Pagination pgn = new Pagination(discussionList, recPerView);
		Page pg = pgn.getPage(pageNo);

		String isMaintain = request.getParameter("isMaintain");
	
		if ((isMaintain == null) || (isMaintain.equals("false"))) {
			doctorsNotesDetailForm = (DoctorsNotesDetailForm) FormLoader.loadAssessmentRequest(doctorsNotesDetailForm);
		}
		else {
			String searchClientType = request.getParameter("searchClientType");
			if (searchClientType != null && !searchClientType.equals("")){
				if (searchClientType.equals(IUMConstants.CLIENT_TYPE_INSURED)){
					retrieveInsuredClientData(doctorsNotesDetailForm);
				} else if (searchClientType.equals(IUMConstants.CLIENT_TYPE_OWNER)){
					retrieveOwnerClientData(doctorsNotesDetailForm);
				}
				doctorsNotesDetailForm.setClientToSearch(searchClientType);
			}
			else{
				doctorsNotesDetailForm = (DoctorsNotesDetailForm) FormLoader.loadAssessmentRequest(doctorsNotesDetailForm);
			}
		}

		
		doctorsNotesDetailForm.setDiscussionList(pg.getList());
		request.setAttribute("detailForm", doctorsNotesDetailForm);
		request.setAttribute("page", pg);
		request.setAttribute("pageNo", String.valueOf(pageNo));
		
		
		end = new Date();
		elapse = ((end.getTime() - start.getTime())%(60 * 1000)) / 1000;
		
		page = "doctorsNotesDetailPage";
	
	}
	catch (UnderWriterException e) {
		LOGGER.error(CodeHelper.getStackTrace(e));
		ActionErrors errors = new ActionErrors();
		errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.request.generic", e.getMessage()));
		saveErrors(request, errors);
	  page = "doctorsNotesDetailPage";
	}
	catch (IUMException e) {
		LOGGER.error(CodeHelper.getStackTrace(e));
		ActionErrors errors = new ActionErrors();
		errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.system.exception", e.getMessage()));
		saveErrors(request, errors);
		page = "errorPage";
	}
	catch (Exception e) {
		LOGGER.error(CodeHelper.getStackTrace(e));
		ActionErrors errors = new ActionErrors();
		errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.system.exception", e.getMessage()));
		saveErrors(request, errors);
		page = "errorPage";
	}
	
	LOGGER.info("handleAction end");
	LOGGER.debug("PAGE FORWARD-->" + page);
	return (mapping.findForward(page));
  }

/**
 * @deprecated use FormLoader.loadAssessmentRequest() method
 * @param reqForm
 * @return
 * @throws UnderWriterException
 * @throws IUMException
 */
  private DoctorsNotesDetailForm populateRequestDetails(DoctorsNotesDetailForm reqForm) throws UnderWriterException, IUMException {
	
	LOGGER.info("populateRequestDetails start");
	AssessmentRequest assessmentRequest = new AssessmentRequest();
	AssessmentRequestData requestData = assessmentRequest.getDetails(reqForm.getRefNo());

	LOBData lob = requestData.getLob();
	ClientData insured = requestData.getInsured();
	ClientData owner = requestData.getOwner();
	SunLifeOfficeData branch = requestData.getBranch();
	UserProfileData agent = requestData.getAgent();
	UserProfileData assignedTo = requestData.getAssignedTo();
	UserProfileData underwriter = requestData.getUnderwriter();
	UserProfileData location = requestData.getFolderLocation();
	ClientDataSheetData cds = requestData.getClientDataSheet();
	StatusData statusData = requestData.getStatus();

	NumberFormat nf = NumberFormat.getInstance();
	nf.setMinimumFractionDigits(2);
	nf.setMaximumFractionDigits(2);

	reqForm.setAutoSettleIndicator(requestData.getAutoSettleIndicator());
	reqForm.setTransmitIndicator(requestData.getTransmitIndicator());
	reqForm.setRefNo(reqForm.getRefNo());
	reqForm.setLob(lob.getLOBCode());
	reqForm.setSourceSystem(requestData.getSourceSystem());
	reqForm.setAmountCovered(nf.format(requestData.getAmountCovered()));
	reqForm.setPremium(nf.format(requestData.getPremium()));
	reqForm.setInsuredName(insured.getGivenName() + " " + insured.getLastName());
	reqForm.setBranchCode(branch.getOfficeId());
	reqForm.setBranchName(branch.getOfficeName());
	reqForm.setAgentCode(agent.getUserId());
	reqForm.setAgentName(agent.getFirstName() + " " + agent.getLastName());
	reqForm.setRequestStatus(String.valueOf(statusData.getStatusId()));
	reqForm.setAssignedTo(assignedTo.getUserId());
	reqForm.setUnderwriter(underwriter.getUserId());
	reqForm.setLocation(location.getUserId());

	reqForm.setRemarks(requestData.getRemarks());
	reqForm.setReceivedDate((DateHelper.format(requestData.getApplicationReceivedDate(), "ddMMMyyyy")).toUpperCase());
	reqForm.setDateForwarded((DateHelper.format(requestData.getForwardedDate(), "ddMMMyyyy")).toUpperCase());
	reqForm.setDateEdited((DateHelper.format(requestData.getUpdatedDate(), "ddMMMyyyy")).toUpperCase());
	reqForm.setDateCreated((DateHelper.format(requestData.getCreatedDate(), "ddMMMyyyy")).toUpperCase());
	reqForm.setStatusDate((DateHelper.format(requestData.getStatusDate(), "ddMMMyyyy")).toUpperCase());

	reqForm.setInsuredClientType(cds.getClientType());
	reqForm.setInsuredClientNo(insured.getClientId());
	reqForm.setInsuredLastName(insured.getLastName());
	reqForm.setInsuredFirstName(insured.getGivenName());
	reqForm.setInsuredMiddleName(insured.getMiddleName());
	reqForm.setInsuredTitle(insured.getTitle());
	reqForm.setInsuredSuffix(insured.getSuffix());

	if (insured.getAge() == 0){
		reqForm.setInsuredAge("");
	}else {
		reqForm.setInsuredAge(String.valueOf(insured.getAge()));
	}

	reqForm.setInsuredSex(insured.getSex());
	reqForm.setInsuredBirthDate((DateHelper.format(insured.getBirthDate(), "ddMMMyyyy")).toUpperCase());

	reqForm.setOwnerClientType(cds.getClientType());
	reqForm.setOwnerClientNo(owner.getClientId());
	reqForm.setOwnerLastName(owner.getLastName());
	reqForm.setOwnerFirstName(owner.getGivenName());
	reqForm.setOwnerMiddleName(owner.getMiddleName());
	reqForm.setOwnerTitle(owner.getTitle());
	reqForm.setOwnerSuffix(owner.getSuffix());

	if (owner.getAge() == 0){
		reqForm.setOwnerAge("");
	}else {
		reqForm.setOwnerAge(String.valueOf(owner.getAge()));
	}

	reqForm.setOwnerSex(owner.getSex());
	reqForm.setOwnerBirthDate((DateHelper.format(owner.getBirthDate(), "ddMMMyyyy")).toUpperCase());

	UWAssessmentData uwData = assessmentRequest.getUWAssessmentDetails(reqForm.getRefNo());
	reqForm.setUWRemarks(uwData.getRemarks());

	LOGGER.info("populateRequestDetails end");
	return (reqForm);
  }

  private ArrayList populateDiscussionList(String refNum) throws UnderWriterException, IUMException {
	
    LOGGER.info("populateDiscussionList start");
	DoctorsNotes docNotes = new DoctorsNotes();
	ArrayList discussions = docNotes.getDoctorsNotesList(refNum);

	int size = discussions.size();
	ArrayList notes = new ArrayList();

	NumberFormat nf = NumberFormat.getInstance();
	nf.setMinimumFractionDigits(0);
	nf.setMaximumFractionDigits(2);

	for (int i = 0; i < size; i++) {
		MedicalNotes medNotes = (MedicalNotes)discussions.get(i);
		DiscussionForm discussionForm = new DiscussionForm();
		discussionForm.setPostedBy(medNotes.getPostedBy());
		discussionForm.setDatePosted((DateHelper.format(medNotes.getPostDate(), "ddMMMyyyy HH:mm")).toUpperCase());
		discussionForm.setNotes(ValueConverter.newLineToHTML(medNotes.getNotes()));
		discussionForm.setRecipient(medNotes.getRecipient());
		notes.add(discussionForm);
	}

	LOGGER.info("populateDiscussionList end");
	return (notes);
  }


private void retrieveInsuredClientData(DoctorsNotesDetailForm arf) throws IUMException{
	
		  LOGGER.info("retrieveInsuredClientData start");
		  arf.setInsuredClientNo(arf.getInsuredClientNo().toUpperCase());
		  AssessmentRequest ar = new AssessmentRequest();
		  ClientData cdata = ar.getClientData(arf.getInsuredClientNo());
		  if(cdata!=null&&cdata.getGivenName()!=null && !cdata.getGivenName().equals("")){
			  if (cdata.getAge() == 0){
			  	arf.setInsuredAge("");
			  }else {
				arf.setInsuredAge( String.valueOf(cdata.getAge()) );
			  }

			  if(cdata.getBirthDate()!=null){
				  arf.setInsuredBirthDate( (DateHelper.format( cdata.getBirthDate(),"ddMMMyyyy" ) ).toUpperCase());
			  }else{
				  arf.setInsuredBirthDate("");
			  }
			  if(cdata.getGivenName()!=null){
				  arf.setInsuredFirstName( cdata.getGivenName() );
			  }else{
				  arf.setInsuredFirstName( "" );
			  }
			  if(cdata.getLastName()!=null){
				  arf.setInsuredLastName( cdata.getLastName() );
			  }else{
				  arf.setInsuredLastName( "" );
			  }
			  if(cdata.getMiddleName()!=null){
				  arf.setInsuredMiddleName( cdata.getMiddleName() );
			  }else{
				  arf.setInsuredMiddleName( "" );
			  }
			  if(cdata.getTitle()!=null){
				  arf.setInsuredTitle( cdata.getTitle() );
			  }else{
				  arf.setInsuredTitle( "" );
			  }
			  if(cdata.getSex()!=null){
				  arf.setInsuredSex( cdata.getSex() );
			  }else{
				  arf.setInsuredSex( "" );
			  }
			  if(cdata.getSuffix()!=null){
				  arf.setInsuredSuffix( cdata.getSuffix() );
			  }else{
				  arf.setInsuredSuffix( "" );
			  }
			  arf.setInsuredOriginal( arf.getInsuredClientNo()+
									  arf.getInsuredFirstName()+
									  arf.getInsuredLastName()+
									  arf.getInsuredMiddleName()+
									  arf.getInsuredTitle()+
									  arf.getInsuredSuffix()+
									  arf.getInsuredSex());
		  }else{
			  arf.setInsuredAge("");
			  arf.setInsuredFirstName("");
			  arf.setInsuredMiddleName("");
			  arf.setInsuredLastName("");
			  arf.setInsuredBirthDate("");
			  arf.setInsuredSuffix("");
			  arf.setInsuredTitle("");
			  arf.setInsuredSex("");
			  arf.setInsuredOriginal("");
		  }
		  LOGGER.info("retrieveInsuredClientData end");
	  }

	private void retrieveOwnerClientData(DoctorsNotesDetailForm arf)throws IUMException{
		
		    LOGGER.info("retrieveOwnerClientData start");
			arf.setOwnerClientNo(arf.getOwnerClientNo().toUpperCase());
			AssessmentRequest ar = new AssessmentRequest();
			ClientData cdata = ar.getClientData(arf.getOwnerClientNo());
			if(cdata!=null&&cdata.getGivenName()!=null && !cdata.getGivenName().equals("")){
				if (cdata.getAge() == 0){
					arf.setOwnerAge("");
				}else {
					arf.setOwnerAge( String.valueOf(cdata.getAge()) );
				}

				if(cdata.getBirthDate()!=null){
					arf.setOwnerBirthDate( (DateHelper.format( cdata.getBirthDate(),"ddMMMyyyy" ) ).toUpperCase());
				}else{
					arf.setOwnerBirthDate("");
				}
				if(cdata.getGivenName()!=null){
					arf.setOwnerFirstName( cdata.getGivenName() );
				}else{
					arf.setOwnerFirstName( "" );
				}
				if(cdata.getLastName()!=null){
					arf.setOwnerLastName( cdata.getLastName() );
				}else{
					arf.setOwnerLastName( "" );
				}
				if(cdata.getMiddleName()!=null){
					arf.setOwnerMiddleName( cdata.getMiddleName() );
				}else{
					arf.setOwnerMiddleName( "" );
				}
				if(cdata.getTitle()!=null){
					arf.setOwnerTitle( cdata.getTitle() );
				}else{
					arf.setOwnerTitle( "" );
				}
				if(cdata.getSex()!=null){
					arf.setOwnerSex( cdata.getSex() );
				}else{
					arf.setOwnerSex( "" );
				}
				if(cdata.getSuffix()!=null){
					arf.setOwnerSuffix( cdata.getSuffix() );
				}else{
					arf.setOwnerSuffix( "" );
				}
				arf.setOwnerOriginal( arf.getOwnerClientNo()+
										arf.getOwnerFirstName()+
										arf.getOwnerLastName()+
										arf.getOwnerMiddleName()+
										arf.getOwnerTitle()+
										arf.getOwnerSuffix()+
										arf.getOwnerSex());

			}else{
				arf.setOwnerAge("");
				arf.setOwnerFirstName("");
				arf.setOwnerMiddleName("");
				arf.setOwnerLastName("");
				arf.setOwnerBirthDate("");
				arf.setOwnerSuffix("");
				arf.setOwnerTitle("");
				arf.setOwnerSex("");
				arf.setOwnerOriginal("");
			}
			LOGGER.info("retrieveOwnerClientData end");
		}

}

