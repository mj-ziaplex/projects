/*
 * Created on Jan 15, 2004
 * package name = com.slocpi.ium.ui.action
 * file name    = SaveAssessmentRequestAction.java
 */
package com.slocpi.ium.ui.action;

import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.slocpi.ium.data.AssessmentRequestData;
import com.slocpi.ium.data.ClientData;
import com.slocpi.ium.data.LOBData;
import com.slocpi.ium.data.StatusData;
import com.slocpi.ium.data.SunLifeOfficeData;
import com.slocpi.ium.data.UserProfileData;
import com.slocpi.ium.ui.form.AssessmentRequestForm;
import com.slocpi.ium.ui.form.RequestDetailForm;
import com.slocpi.ium.ui.util.StateHandler;
import com.slocpi.ium.underwriter.AssessmentRequest;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.DateHelper;
import com.slocpi.ium.util.IUMConstants;
import com.slocpi.ium.util.ValueConverter;

/**
 * TODO Class Description of SaveAssessmentRequestAction.java
 * @author Engel
 * 
 */
public class SaveAssessmentRequestAction extends IUMAction{
	private static final Logger LOGGER = LoggerFactory.getLogger(SaveAssessmentRequestAction.class);

	/* (non-Javadoc)
	 * @see com.slocpi.ium.ui.action.IUMAction#handleAction(org.apache.struts.action.ActionMapping, org.apache.struts.action.ActionForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	public ActionForward handleAction(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		LOGGER.info("handleAction start");
		String page="viewAssessmentRequestPage";
		
		try{
		
		AssessmentRequestForm aReqf = (AssessmentRequestForm)form;
		AssessmentRequest ar = new AssessmentRequest();
		
		AssessmentRequestData aReqD = changeToForm(aReqf);
		StateHandler sth = new StateHandler();
		UserProfileData usr = (sth.getUserData(request)).getProfile();
		aReqD.setUpdatedBy(usr);
		aReqD.setCreatedBy(usr);
		aReqD.setCurrentUser(usr.getUserId());
		ar.createAssessmentRequest(aReqD);
		form = null;
		RequestDetailForm rdf = new RequestDetailForm();
		aReqf.setRefNo(aReqf.getRefNo().toUpperCase());
		rdf.setRefNo(aReqf.getRefNo());
		request.setAttribute("requestDetailForm", rdf);
		
		}catch (Exception e) {
		  LOGGER.error(CodeHelper.getStackTrace(e));
		  ActionErrors errors = new ActionErrors();
		  errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.system.exception", e.getMessage()));
		  saveErrors(request, errors); 
		  return (mapping.findForward("createAssessmentRequestsPage")); 
		}
		LOGGER.info("handleAction end");
		LOGGER.debug("PAGE FORWARD-->" + page);
		return (mapping.findForward(page));
	}
	
	
	private AssessmentRequestData changeToForm(AssessmentRequestForm arF){
		
		LOGGER.info("changeToForm start");
		AssessmentRequestData arD = new AssessmentRequestData();
		
		arD.setReferenceNumber(arF.getRefNo());
			LOBData lob = new LOBData();
			lob.setLOBCode(arF.getLob());
		arD.setLob(lob);
		arD.setSourceSystem(arF.getSourceSystem());
		arD.setAmountCovered( ValueConverter.convertCurrency(arF.getAmountCovered()) );
		arD.setPremium( ValueConverter.convertCurrency(arF.getPremium() ) );
		if(arF.getInsuredClientNo()!=null && arF.getInsuredClientNo().length()>0){
				ClientData insured = new ClientData();
				insured.setClientId(arF.getInsuredClientNo());
				insured.setGivenName(arF.getInsuredFirstName());
				insured.setLastName(arF.getInsuredLastName());
				insured.setMiddleName(arF.getInsuredMiddleName());
				if( arF.getInsuredAge().equals("")){
					insured.setAge(0);
				}else{
					insured.setAge( Integer.parseInt( arF.getInsuredAge() ) );
				}
				insured.setSex( arF.getInsuredSex());
				insured.setSuffix( arF.getInsuredSuffix() );
				insured.setTitle( arF.getInsuredTitle() );
				if(arF.getInsuredBirthDate().equals("")){
					insured.setBirthDate( null );
				}else{
					insured.setBirthDate( DateHelper.parse(arF.getInsuredBirthDate(),"ddMMMyyyy") );
				}
			arD.setInsured(insured);
		}else{
			arD.setInsured(null);
		}
		if(arF.getOwnerClientNo()!=null && arF.getOwnerClientNo().length()>0){
			arF.setOwnerClientNo(arF.getOwnerClientNo().toUpperCase());
			ClientData owner = new ClientData();
			owner.setClientId(arF.getOwnerClientNo());
			owner.setGivenName(arF.getOwnerFirstName());
			owner.setLastName(arF.getOwnerLastName());
			owner.setMiddleName(arF.getOwnerMiddleName());
			if (arF.getOwnerAge().equals("")){
				owner.setAge(0);
			}else{
				owner.setAge( Integer.parseInt( arF.getOwnerAge() ) );
			}
			owner.setSex( arF.getOwnerSex());
			owner.setSuffix( arF.getOwnerSuffix() );
			owner.setTitle( arF.getOwnerTitle() );
			if(arF.getOwnerBirthDate().equals("")){
				owner.setBirthDate(null);
			}else{
				owner.setBirthDate( DateHelper.parse(arF.getOwnerBirthDate(),"ddMMMyyyy") );
			}
			
			arD.setOwner(owner);
		}else{
			arD.setOwner(null);
		}
			SunLifeOfficeData slo = new SunLifeOfficeData();
			slo.setOfficeId(arF.getBranchCode());
		arD.setBranch(slo);
			UserProfileData agnt = new UserProfileData();
			agnt.setUserId(arF.getAgentCode());		
		arD.setAgent(agnt);
			StatusData stat = new StatusData();
			stat.setStatusId(IUMConstants.STATUS_NB_REVIEW_ACTION);
		arD.setStatus(stat);
		Date today = new Date();
		arD.setStatusDate(today);
		
		arD.setUnderwriter(null);
			UserProfileData folder = new UserProfileData();
			folder.setUserId(arF.getLocation());
		arD.setFolderLocation(folder);
		arD.setRemarks(arF.getRemarks());
		arD.setApplicationReceivedDate( DateHelper.parse(arF.getReceivedDate(),"ddMMMyyyy") );
		arD.setForwardedDate( DateHelper.parse(arF.getDateForwarded(),"ddMMMyyyy") );
		
		arD.setCreatedDate(today);
		arD.setUpdatedDate(today);
			UserProfileData usr = new UserProfileData();
			usr.setUserId(arF.getAssignedTo());
		arD.setAssignedTo(usr);
		LOGGER.info("changeToForm end");
		return arD;
	}
}
