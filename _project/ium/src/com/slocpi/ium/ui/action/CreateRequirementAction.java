package com.slocpi.ium.ui.action;

// struts package
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.slocpi.ium.IUMException;
import com.slocpi.ium.data.AssessmentRequestData;
import com.slocpi.ium.data.ClientData;
import com.slocpi.ium.data.ClientDataSheetData;
import com.slocpi.ium.data.KickOutMessageData;
import com.slocpi.ium.data.LOBData;
import com.slocpi.ium.data.MedicalRecordData;
import com.slocpi.ium.data.PolicyRequirementsData;
import com.slocpi.ium.data.StatusData;
import com.slocpi.ium.data.SunLifeOfficeData;
import com.slocpi.ium.data.TestProfileData;
import com.slocpi.ium.data.UserAccessData;
import com.slocpi.ium.data.UserData;
import com.slocpi.ium.data.UserProfileData;
import com.slocpi.ium.data.dao.RequirementDAO;
import com.slocpi.ium.data.util.DataSourceProxy;
import com.slocpi.ium.interfaces.IUMInterfaceException;
import com.slocpi.ium.ui.form.CDSDetailForm;
import com.slocpi.ium.ui.form.KOReasonsForm;
import com.slocpi.ium.ui.form.MedLabRecordsForm;
import com.slocpi.ium.ui.form.RequirementForm;
import com.slocpi.ium.ui.util.StateHandler;
import com.slocpi.ium.underwriter.AssessmentRequest;
import com.slocpi.ium.underwriter.KickOutMessage;
import com.slocpi.ium.underwriter.PolicyRequirements;
import com.slocpi.ium.underwriter.UnderWriterException;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.DateHelper;
import com.slocpi.ium.util.IUMConstants;
import com.slocpi.ium.util.ValueConverter;

/**
 * @TODO Class Description CreateRequirementAction
 * @author: Ingrid Villanueva
 * @version: 1.0
 */
public class CreateRequirementAction extends IUMAction {

	private static final Logger LOGGER = LoggerFactory.getLogger(CreateRequirementAction.class);

	/**
	 * @TODO method description for execute
	 * @param mapping
	 *            mapping of request to an instance of this class
	 * @param form
	 *            object
	 * @param request
	 *            request object
	 * @param response
	 *            response object
	 * @exception IOException
	 *                if an input/output error occurs
	 * @exception ServletException
	 *                if a servlet exception occurs
	 * @return ActionForward as defined in the mapping parameter.
	 */
	public ActionForward handleAction(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		
		LOGGER.info("handleAction start");
		String page = "cdsDetailPage";

		String session_id = request.getParameter("session_id").toString();
		request.setAttribute("session_id", session_id);

		HttpSession session = request.getSession();
		if (session.getAttribute("requirementForm") != null)
			session.removeAttribute("requirementForm");

		CDSDetailForm arForm = (CDSDetailForm) session
				.getAttribute("detailForm_" + session_id);
		RequirementForm reqForm = (RequirementForm) form;

		String referenceNumber = arForm.getRefNo();
		String lob = arForm.getLob();
		String requestStatus = arForm.getRequestStatus();
		String mode = request.getParameter("mode");

		StateHandler sh = new StateHandler();
		UserData ud = sh.getUserData(request);
		UserProfileData profile = ud.getProfile();
		String userId = profile.getUserId();

		String savedSection = (request.getParameter("savedSectionReq") != null) ? request
				.getParameter("savedSectionReq").trim()
				: "";
		
		try {
			session = request.getSession();
			if (session == null) {
				LOGGER.debug("request.getSession() is null");
			} else {
				LOGGER.debug("request.getSession() is not null");
				session
						.setAttribute("savedSection_" + session_id,
								savedSection);
				String temp = (String) session.getAttribute("savedSection_"
						+ session_id);
				String temp2 = (temp != null) ? temp.trim()
						: "session object savedSection is not saved";
				LOGGER.debug("session.getAttribute('savedSection'): " + temp2);
			}
		} catch (Exception e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
		}

		try {
			
			String pageId = IUMConstants.PAGE_CDS;
			
			UserAccessData uad = extractAccessPriveleges(userId, pageId);
			if (uad.getAccessCode().equals("W")) {
				
				String clientId = getClientNo(arForm, reqForm.getClientType());
				reqForm.setClientNo(clientId);
				long reqId = getRequirementId(reqForm, mode, referenceNumber,
						userId, lob, requestStatus, arForm.getSourceSystem());
			/*	buildPage(request, arForm, referenceNumber, Long
						.toString(reqId));*/
			} else {
				page = "errorPage";
			}
		}
		catch (UnderWriterException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
		/*	buildPage(request, arForm, referenceNumber, "");*/

			ActionErrors errors = new ActionErrors();
			String err = e.getMessage();
			if (err == null)
				err = "";
			errors.add(ActionErrors.GLOBAL_ERROR, new ActionError(
					"error.invalidRequirement", err));
			saveErrors(request, errors);
			
			return (mapping.findForward(page));
		} catch (IUMException eIUM) {
			LOGGER.error(CodeHelper.getStackTrace(eIUM));
			ActionErrors errors = new ActionErrors();
			errors.add(ActionErrors.GLOBAL_ERROR, new ActionError(
					"error.system.exception", eIUM.getMessage()));
			saveErrors(request, errors);
			page = "errorPage";
		} catch (IUMInterfaceException eInterface) {
			LOGGER.error(CodeHelper.getStackTrace(eInterface));
			ActionErrors errors = new ActionErrors();
			errors.add(ActionErrors.GLOBAL_ERROR, new ActionError(
					"error.system.exception", eInterface.getMessage()));
			saveErrors(request, errors);
			page = "errorPage";
		}
		
		LOGGER.info("handleAction end");
		LOGGER.debug("PAGE FORWARD-->" + page);
		return (mapping.findForward(page));
	}// execute

/*private void buildPage(HttpServletRequest request, CDSDetailForm arForm,
			String referenceNumber, String reqIdStr) throws Exception {
		
		LOGGER.info("buildPage start");

		arForm = populateRequestDetails(arForm);
		ArrayList requirements = populateRequirements(referenceNumber);
		ArrayList reqLevelList = getRequestLevel();
		ArrayList koReasons = populateKOReasons(referenceNumber);
		ArrayList medLabRecords = populateMedLabRecords(referenceNumber);
		
		arForm.setRequirements(requirements);
		arForm.setKoReasons(koReasons);
		arForm.setMedLabRecords(medLabRecords);
		arForm.setReqLevelList(reqLevelList);
		
		HttpSession session = request.getSession();
		session.setAttribute("detailForm", arForm);
		
		long reqId = 0;
		
		if (reqIdStr.equals("")) {
			if (requirements == null)
				LOGGER.debug("null requirements");
			if (requirements != null) {
				int sizeArr = requirements.size();
				if (sizeArr > 0) {
					RequirementForm req = (RequirementForm) requirements.get(0);
					if (req != null) {
						reqId = Long.parseLong(req.getReqId());
					} else {
						LOGGER.debug("policy is null");
					}
				}
			}
		} else {
			reqId = Long.parseLong(reqIdStr);
		}
		RequirementForm requirementForm = populateRequirementDetails(
				referenceNumber, reqId);

		request.setAttribute("requirementForm", requirementForm);

		LOGGER.info("buildPage end");
	}*/

private long saveRequirementRecord(RequirementForm reqForm,
		String referenceNo, String userId, String lob,
		String requestStatus, String sourceSystem) throws Exception {
	
	LOGGER.info("saveRequirementRecord start");

	PolicyRequirementsData data = new PolicyRequirementsData();

	data.setReferenceNumber(referenceNo);

	if (reqForm.getSeq() != null) {
		if (reqForm.getSeq().trim().length() > 0) {
			data.setSequenceNumber(Long.parseLong(reqForm.getSeq()));
		}
	}

	data.setRequirementCode(reqForm.getReqCodeValue());
	data.setLevel(reqForm.getReqLevel());
	StatusData stat = new StatusData();
	if (reqForm.getReqStatusValue() != null) {
		if (reqForm.getReqStatusValue().trim().length() > 0) {
			stat.setStatusId(Long.parseLong(reqForm.getReqStatusValue()));
		}
	}
	data.setStatus(stat);
	

	if (!reqForm.getStatusDate().equals(""))
		data.setStatusDate(DateHelper.parse(reqForm.getStatusDate(),
				"ddMMMyyyy"));
	else
		data.setStatusDate(null);

	data.setCreateDate(new Date());
	data.setCreatedBy(userId);

	data.setUpdatedBy(userId);

	data.setUpdateDate(new Date());
	if (!reqForm.getReceivedDate().equals(""))
		data.setReceiveDate(DateHelper.parse(reqForm.getReceivedDate(),
				"ddMMMyyyy"));
	else
		data.setReceiveDate(null);
	
	if (reqForm.getCompleteReq() != null) {
		if (reqForm.getCompleteReq().equalsIgnoreCase("Y")) {
			data.setCompletedRequirementInd(true);
			data.setReceiveDate(new Date());
		} else
			data.setCompletedRequirementInd(false);
	}

	data.setCCASuggestInd(ValueConverter.stringToBoolean(reqForm
			.getCcasSuggest()));
	if (!reqForm.getTestDate().equals(""))
		data.setTestDate(DateHelper.parse(reqForm.getTestDate(),
				"ddMMMyyyy"));
	else
		data.setTestDate(null);

	data.setTestResultCode(reqForm.getTestResult());

	data.setTestResultDesc(reqForm.getTestDesc());

	data.setDesignation(reqForm.getDesignation());
	
	data.setClientType(reqForm.getClientType());
	
	data.setClientId(reqForm.getClientNo());
	if (reqForm.getResolve() != null) {
		if (reqForm.getResolve().equalsIgnoreCase("Y"))
			data.setResolveInd(true);
		else
			data.setResolveInd(false);
	}
	
	if (reqForm.getFollowUpNo() != null) {
		if (!reqForm.getFollowUpNo().equals("")) {
			data.setFollowUpNumber(Long.parseLong(reqForm.getFollowUpNo()));// 19
		}
	}
	

	if (!reqForm.getFollowUpDate().equals(""))
		data.setFollowUpDate(DateHelper.parse(reqForm.getFollowUpDate(),
				"ddMMMyyyy"));
	else

		data.setFollowUpDate(null);

	if (!reqForm.getValidityDate().equals(""))
		data.setValidityDate(DateHelper.parse(reqForm.getValidityDate(),
				"ddMMMyyyy"));
	else
		data.setValidityDate(null);
	if (reqForm.getPaidInd() != null) {
		if (reqForm.getPaidInd().equalsIgnoreCase("Y"))
			data.setPaidInd(true);
		else
			data.setPaidInd(false);
	}
	data.setNewTestOnly(ValueConverter
			.stringToBoolean(reqForm.getNewTest()));
	if (!reqForm.getOrderDate().equals(""))
		data.setOrderDate(DateHelper.parse(reqForm.getOrderDate(),
				"ddMMMyyyy"));
	else
		data.setOrderDate(null);

	
	if (!reqForm.getDateSent().equals("")) {
	

		data.setDateSent(DateHelper.parse(reqForm.getDateSent(),
				"ddMMMyyyy"));

	} else {
		
		data.setDateSent(null);
	}

	data.setAutoOrderInd(ValueConverter.stringToBoolean(reqForm
			.getAutoOrder()));
	data.setFldCommentInd(ValueConverter.stringToBoolean(reqForm
			.getFldComm()));
	data.setComments(reqForm.getComment());
	data.setABACUScreateDate(new Date());
	data.setABACUSUpdateDate(new Date());
	data.setABACUSUpdatedBy(userId);
	PolicyRequirements req = new PolicyRequirements();
	AssessmentRequestData reqObj = new AssessmentRequestData();
	LOBData lobObj = new LOBData();
	lobObj.setLOBCode(lob);
	reqObj.setLob(lobObj);
	reqObj.setSourceSystem(sourceSystem); 
	StatusData statusObj = new StatusData();
	statusObj.setStatusId(Long.parseLong(requestStatus));
	reqObj.setStatus(statusObj);
	
	LOGGER.info("saveRequirementRecord end");
	return req.createRequirement(data, reqObj, userId);
}

	private void updateRequirementRecord(RequirementForm reqForm,
			String referenceNo, String userId, String lob, String sourceSystem)
			throws Exception {

		LOGGER.info("updateRequirementRecord start");
		PolicyRequirementsData data = new PolicyRequirementsData();
		if (reqForm.getReqId() != null) {
			if (reqForm.getReqId().trim().length() > 0) {
				data.setRequirementId(Long.parseLong(reqForm.getReqId()));
			}
		}
		data.setReferenceNumber(referenceNo);
		
		if (reqForm.getSeq() != null) {
			if (reqForm.getSeq().trim().length() > 0) {
				data.setSequenceNumber(Long.parseLong(reqForm.getSeq()));
			}
		}
		data.setRequirementCode(reqForm.getReqCodeValue());
		data.setLevel(reqForm.getReqLevel());
		StatusData stat = new StatusData();
		if (reqForm.getReqStatusValue() != null) {
			if (reqForm.getReqStatusValue().trim().length() > 0) {
				stat.setStatusId(Long.parseLong(reqForm.getReqStatusValue()));
			}
		}
		data.setStatus(stat);
		if (!reqForm.getStatusDate().equals(""))
			data.setStatusDate(DateHelper.parse(reqForm.getStatusDate(),
					"ddMMMyyyy"));
		else
			data.setStatusDate(null);
		data.setUpdatedBy(userId); 
		data.setUpdateDate(new Date());

		if (!reqForm.getReceivedDate().equals(""))
			data.setReceiveDate(DateHelper.parse(reqForm.getReceivedDate(),
					"ddMMMyyyy"));
		else
			data.setReceiveDate(null);

		if (reqForm.getCompleteReq() != null) {
			if (reqForm.getCompleteReq().equalsIgnoreCase("Y")) {
				data.setCompletedRequirementInd(true);
			} else
				data.setCompletedRequirementInd(false);
		}

		data.setCCASuggestInd(ValueConverter.stringToBoolean(reqForm
				.getCcasSuggest()));
		if (!reqForm.getTestDate().equals(""))
			data.setTestDate(DateHelper.parse(reqForm.getTestDate(),
					"ddMMMyyyy"));
		else
			data.setTestDate(null);

		data.setTestResultCode(reqForm.getTestResult());

		data.setTestResultDesc(reqForm.getTestDesc());

		data.setDesignation(reqForm.getDesignation());
		data.setClientType(reqForm.getClientType());
		data.setClientId(reqForm.getClientNo());
		if (reqForm.getResolve() != null) {
			if (reqForm.getResolve().equalsIgnoreCase("Y"))
				data.setResolveInd(true);
			else
				data.setResolveInd(false);
		}
		if (reqForm.getFollowUpNo() != null) {
			if (!reqForm.getFollowUpNo().equals("")) {
				data.setFollowUpNumber(Long.parseLong(reqForm.getFollowUpNo()));
			}
		}
		if (!reqForm.getFollowUpDate().equals(""))
			data.setFollowUpDate(DateHelper.parse(reqForm.getFollowUpDate(),
					"ddMMMyyyy"));
		else
			data.setFollowUpDate(DateHelper.add(new Date(),
					Calendar.DAY_OF_MONTH, (int) data.getFollowUpNumber()));
		if (!reqForm.getValidityDate().equals(""))
			data.setValidityDate(DateHelper.parse(reqForm.getValidityDate(),
					"ddMMMyyyy"));
		else
			data.setValidityDate(null);
		if (reqForm.getPaidInd() != null) {
			if (reqForm.getPaidInd().equalsIgnoreCase("Y"))
				data.setPaidInd(true);
			else
				data.setPaidInd(false);
		}
		data.setNewTestOnly(ValueConverter
				.stringToBoolean(reqForm.getNewTest()));
		if (!reqForm.getOrderDate().equals(""))
			data.setOrderDate(DateHelper.parse(reqForm.getOrderDate(),
					"ddMMMyyyy"));
		else
			data.setOrderDate(null);
		if (!reqForm.getDateSent().equals(""))
			data.setDateSent(DateHelper.parse(reqForm.getDateSent(),
					"ddMMMyyyy"));
		else
			data.setDateSent(null);
		data.setAutoOrderInd(ValueConverter.stringToBoolean(reqForm
				.getAutoOrder()));
		data.setFldCommentInd(ValueConverter.stringToBoolean(reqForm
				.getFldComm()));
		data.setComments(reqForm.getComment());
		data.setABACUScreateDate(new Date());
		data.setABACUSUpdateDate(new Date());
		data.setABACUSUpdatedBy(userId);
		PolicyRequirements req = new PolicyRequirements();
		
		AssessmentRequestData reqObj = new AssessmentRequestData();
		LOBData lobObj = new LOBData();
		lobObj.setLOBCode(lob);
		reqObj.setLob(lobObj);
		reqObj.setSourceSystem(sourceSystem);
		req.maintainRequirement(data, reqObj);
		LOGGER.info("updateRequirementRecord end");
	}

	private ArrayList populateRequirements(String refNo) throws Exception {
		
		LOGGER.info("populateRequirements start");
		PolicyRequirements pr = new PolicyRequirements();
		ArrayList reqList = pr.getRequirements(refNo);

		int size = reqList.size();
		ArrayList requirements = new ArrayList();
		for (int i = 0; i < size; i++) {
			PolicyRequirementsData reqData = (PolicyRequirementsData) reqList
					.get(i);
			RequirementForm reqForm = new RequirementForm();
			reqForm.setRefNo(reqData.getReferenceNumber());
			reqForm.setReqId(String.valueOf(reqData.getRequirementId()));
			reqForm.setSeq(String.valueOf(reqData.getSequenceNumber()));
			reqForm.setReqCode(reqData.getRequirementCode());
			reqForm.setReqStatusCode(String.valueOf(reqData.getStatus()
					.getStatusId()));
			reqForm.setReqStatusDesc(reqData.getStatus().getStatusDesc());
			reqForm.setStatusDate((DateHelper.format(reqData.getStatusDate(),
					"ddMMMyyyy")).toUpperCase());
			reqForm.setComment(reqData.getComments());
			reqForm.setClientType(reqData.getClientType());
			reqForm.setReqLevel(reqData.getLevel());
			reqForm.setFollowUpDate((DateHelper.format(reqData
					.getFollowUpDate(), "ddMMMyyyy")).toUpperCase());
			reqForm.setDateSent((DateHelper.format(reqData.getDateSent(),
					"ddMMMyyyy")).toUpperCase());
			reqForm.setClientNo(reqData.getClientId());
			reqForm.setFollowUpNo(Long.toString(reqData.getFollowUpNumber()));
			reqForm.setTestDate((DateHelper.format(reqData.getTestDate(),
					"ddMMMyyyy")).toUpperCase());
			requirements.add(reqForm);
		}
		LOGGER.info("populateRequirements end");
		return (requirements);
	}

	private RequirementForm populateRequirementDetails(String refNo, long reqId)
			throws Exception {
		
		LOGGER.info("populateRequirementDetails start");
		PolicyRequirements pr = new PolicyRequirements();
		PolicyRequirementsData reqData = pr.getRequirement(reqId);
		RequirementForm reqForm = new RequirementForm();
		if (reqData != null) {
			reqForm.setRefNo(reqData.getReferenceNumber());
			reqForm.setReqId(Long.toString(reqId));
			reqForm.setReqCode(reqData.getRequirementCode());
			reqForm.setSeq(String.valueOf(reqData.getSequenceNumber()));
			reqForm.setReqDesc(reqData.getReqDesc());
			reqForm.setReqLevel(reqData.getLevel());
			reqForm.setCreatedDate((DateHelper.format(reqData.getCreateDate(),
					"ddMMMyyyy")).toUpperCase());
			reqForm.setCompleteReq(String.valueOf(reqData
					.isCompletedRequirementInd()));
		
			if (reqData.getStatus() != null) {
				reqForm.setReqStatusCode(String.valueOf(reqData.getStatus()
						.getStatusDesc()));
				reqForm.setReqStatusValue(String.valueOf(reqData.getStatus()
						.getStatusId()));
			}
			reqForm.setStatusDate((DateHelper.format(reqData.getStatusDate(),
					"ddMMMyyyy")).toUpperCase());
			reqForm.setUpdatedBy(reqData.getUpdatedBy());
			reqForm.setUpdatedDate((DateHelper.format(reqData.getUpdateDate(),
					"ddMMMyyyy")).toUpperCase());
			reqForm.setClientType(reqData.getClientType());
			reqForm.setDesignation(reqForm.getDesignation());
			reqForm.setTestDate((DateHelper.format(reqData.getTestDate(),
					"ddMMMyyyy")).toUpperCase());
			reqForm.setTestResult(reqData.getTestResultCode());
			reqForm.setTestDesc(reqData.getTestResultDesc());
			reqForm.setResolve(String.valueOf(reqData.isResolveInd()));
		
			reqForm.setFollowUpNo(String.valueOf(reqData.getFollowUpNumber()));
			reqForm.setFollowUpDate((DateHelper.format(reqData
					.getFollowUpDate(), "ddMMMyyyy")).toUpperCase());
			reqForm.setValidityDate((DateHelper.format(reqData
					.getValidityDate(), "ddMMMyyyy")).toUpperCase());
			reqForm.setPaidInd(String.valueOf(reqData.isPaidInd()));
			reqForm.setNewTest(ValueConverter.booleanToString(reqData
					.getNewTestOnly()));
			reqForm.setOrderDate((DateHelper.format(reqData.getOrderDate(),
					"ddMMMyyyy")).toUpperCase());
			reqForm.setDateSent((DateHelper.format(reqData.getDateSent(),
					"ddMMMyyyy")).toUpperCase());
			reqForm.setCcasSuggest(ValueConverter.booleanToString(reqData
					.getCCASuggestInd()));
			reqForm.setReceivedDate((DateHelper.format(
					reqData.getReceiveDate(), "ddMMMyyyy")).toUpperCase());
			reqForm.setAutoOrder(ValueConverter.booleanToString(reqData
					.getAutoOrderInd()));
			reqForm.setFldComm(ValueConverter.booleanToString(reqData
					.getFldCommentInd()));
			reqForm.setComment(reqData.getComments());
			reqForm.setClientNo(reqData.getClientId());
		}
		LOGGER.info("populateRequirementDetails end");
		return (reqForm);
	}// populateRequirementDetails

	private ArrayList getRequestLevel() throws SQLException, IUMException {
		
		LOGGER.info("getRequestLevel start");
		
		try {
			
			RequirementDAO dao = new RequirementDAO();
			ArrayList list = new ArrayList();
			list = dao.retrieveRequirements();
			if (list != null){
				LOGGER.debug("size = " + list.size());
			}
			else{
				LOGGER.debug("list is null");
			}
			
			LOGGER.info("getRequestLevel end");
			return list;
		} catch (SQLException e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new IUMException(e.getMessage());
		} 
	}

	

	/*private ArrayList populateMedLabRecords(String refNo)
			throws UnderWriterException, IUMException {
		
		LOGGER.info("populateMedLabRecords start");
		AssessmentRequest ar = new AssessmentRequest();
		ArrayList medReqList = ar.getKOMedRecs(refNo);

		int size = medReqList.size();
		ArrayList medicalRecs = new ArrayList();

		for (int i = 0; i < size; i++) {
			MedicalRecordData medData = (MedicalRecordData) medReqList.get(i);
			MedLabRecordsForm medRecForm = new MedLabRecordsForm();
			TestProfileData testData = medData.getTest();
			medRecForm.setCode(String.valueOf(testData.getTestId()));
			medRecForm.setTestDescription(testData.getTestDesc());

			String type = "";
			String labTestInd = medData.getLabTestInd();
			if (labTestInd != null) {
				if (labTestInd.equals("Y")) {
					type = "LAB";
				} else if (labTestInd.equals("Y")) {
					type = "MED";
				}
			}

			medRecForm.setType(type);
			medRecForm.setDateConducted((DateHelper.format(medData
					.getConductedDate(), "ddMMMyyyy")).toUpperCase());
			medRecForm.setDateReceived((DateHelper.format(medData
					.getReceivedDate(), "ddMMMyyyy")).toUpperCase());
			medicalRecs.add(medRecForm);
		}
		LOGGER.info("populateMedLabRecords end");
		return (medicalRecs);
	}// populateMedLabRecords
*/
	private ArrayList populateKOReasons(String refNo)
			throws UnderWriterException, IUMException {
		
		LOGGER.info("populateKOReasons start");
		KickOutMessage ko = new KickOutMessage();
		ArrayList reqList = ko.getMessages(refNo);

		int size = reqList.size();
		ArrayList koReasons = new ArrayList();
		for (int i = 0; i < size; i++) {
			KickOutMessageData koData = (KickOutMessageData) reqList.get(i);
			KOReasonsForm koForm = new KOReasonsForm();
			koForm.setSeq(String.valueOf(koData.getSequenceNumber()));
			koForm.setMessageText(koData.getMessageText());
			koForm.setClientNo(koData.getClientId());
			koForm.setFailResponse(koData.getFailResponse());
			koReasons.add(koForm);
		}

		LOGGER.info("populateKOReasons end");
		return (koReasons);
	}// populateKOReasons

	private String getClientNo(CDSDetailForm arForm, String clientType) {
		
		LOGGER.info("getClientNo start");
		String clientId = "";

		if (clientType.equals(IUMConstants.CLIENT_TYPE_INSURED)) {
			clientId = arForm.getInsuredClientNo();
		} else if (clientType.equals(IUMConstants.CLIENT_TYPE_OWNER)) {
			if (arForm.getOwnerClientNo() != null) {
				if (arForm.getOwnerClientNo().trim().length() > 0) {
					clientId = arForm.getOwnerClientNo();
				} else {
					clientId = arForm.getInsuredClientNo();
				}
			} else {
				clientId = arForm.getInsuredClientNo();
			}
		} else if (clientType.equals(IUMConstants.CLIENT_TYPE_MEMBER)) {
			clientId = arForm.getInsuredClientNo();
		} else if (clientType.equals(IUMConstants.CLIENT_TYPE_PLANHOLDER)) {
			clientId = arForm.getInsuredClientNo();
		}
		LOGGER.info("getClientNo end");
		return clientId;

	}

	private long getRequirementId(RequirementForm reqForm, String mode,
			String referenceNumber, String userId, String lob,
			String requestStatus, String sourceSystem) throws Exception {
		
		LOGGER.info("getRequirementId start");
		long reqId = 0;

		if (mode != null) {
			if (mode.trim().length() == 0) {
				reqId = saveRequirementRecord(reqForm, referenceNumber, userId,
						lob, requestStatus, sourceSystem);
			} else {
				updateRequirementRecord(reqForm, referenceNumber, userId, lob,
						sourceSystem);
				if (reqForm.getReqId() != null) {
					if (reqForm.getReqId().trim().length() > 0) {
						reqId = Long.parseLong(reqForm.getReqId());
					}
				}
			}
		} else {
			reqId = saveRequirementRecord(reqForm, referenceNumber, userId,
					lob, requestStatus, sourceSystem);
		}

		LOGGER.info("getRequirementId end");
		return reqId;

	}

	private CDSDetailForm populateRequestDetails(CDSDetailForm reqForm)
			throws UnderWriterException, IUMException {
		
		LOGGER.info("populateRequestDetails start");
		AssessmentRequest assessmentRequest = new AssessmentRequest();
		AssessmentRequestData requestData = assessmentRequest
				.getDetails(reqForm.getRefNo());

		LOBData lob = requestData.getLob();
		ClientData insured = requestData.getInsured();
		ClientData owner = requestData.getOwner();
		SunLifeOfficeData branch = requestData.getBranch();
		UserProfileData agent = requestData.getAgent();
		UserProfileData assignedTo = requestData.getAssignedTo();
		UserProfileData underwriter = requestData.getUnderwriter();
		UserProfileData location = requestData.getFolderLocation();
		ClientDataSheetData cds = requestData.getClientDataSheet();
		StatusData statusData = requestData.getStatus();

		NumberFormat nf = NumberFormat.getInstance();
		nf.setMinimumFractionDigits(2);
		nf.setMaximumFractionDigits(2);

		reqForm.setTransmitIndicator(requestData.getTransmitIndicator());
		reqForm.setRefNo(reqForm.getRefNo());
		reqForm.setLob(lob.getLOBCode());
		reqForm.setSourceSystem(requestData.getSourceSystem());
		reqForm.setAmountCovered(nf.format(requestData.getAmountCovered()));
		reqForm.setPremium(nf.format(requestData.getPremium()));
		reqForm.setInsuredName(insured.getGivenName() + " "
				+ insured.getLastName());
		reqForm.setBranchCode(branch.getOfficeId());
		reqForm.setBranchName(branch.getOfficeName());
		reqForm.setAgentCode(agent.getUserId());
		reqForm.setAgentName(agent.getFirstName() + " " + agent.getLastName());
		reqForm.setRequestStatus(String.valueOf(statusData.getStatusId()));
		reqForm.setAssignedTo(assignedTo.getUserId());
		reqForm.setUnderwriter(underwriter.getUserId());
		reqForm.setLocation(location.getUserId());

		reqForm.setRemarks(requestData.getRemarks());
		reqForm.setReceivedDate((DateHelper.format(requestData
				.getApplicationReceivedDate(), "ddMMMyyyy")).toUpperCase());
		reqForm.setDateForwarded((DateHelper.format(requestData
				.getForwardedDate(), "ddMMMyyyy")).toUpperCase());
		reqForm.setDateEdited((DateHelper.format(requestData.getUpdatedDate(),
				"ddMMMyyyy")).toUpperCase());
		reqForm.setDateCreated((DateHelper.format(requestData.getCreatedDate(),
				"ddMMMyyyy")).toUpperCase());
		reqForm.setStatusDate((DateHelper.format(requestData.getStatusDate(),
				"ddMMMyyyy")).toUpperCase());

		reqForm.setInsuredClientType(cds.getClientType());
		reqForm.setInsuredClientNo(insured.getClientId());
		reqForm.setInsuredLastName(insured.getLastName());
		reqForm.setInsuredFirstName(insured.getGivenName());
		reqForm.setInsuredMiddleName(insured.getMiddleName());
		reqForm.setInsuredTitle(insured.getTitle());
		reqForm.setInsuredSuffix(insured.getSuffix());
		reqForm.setInsuredAge(String.valueOf(insured.getAge()));
		reqForm.setInsuredSex(insured.getSex());
		reqForm.setInsuredBirthDate((DateHelper.format(insured.getBirthDate(),
				"ddMMMyyyy")).toUpperCase());

		reqForm.setOwnerClientType(cds.getClientType());
		reqForm.setOwnerClientNo(owner.getClientId());
		reqForm.setOwnerLastName(owner.getLastName());
		reqForm.setOwnerFirstName(owner.getGivenName());
		reqForm.setOwnerMiddleName(owner.getMiddleName());
		reqForm.setOwnerTitle(owner.getTitle());
		reqForm.setOwnerSuffix(owner.getSuffix());
		reqForm.setOwnerAge(String.valueOf(owner.getAge()));
		reqForm.setOwnerSex(owner.getSex());
		reqForm.setOwnerBirthDate((DateHelper.format(owner.getBirthDate(),
				"ddMMMyyyy")).toUpperCase());

		LOGGER.info("populateRequestDetails end");
		return (reqForm);
	}// populateRequestDetails

}
