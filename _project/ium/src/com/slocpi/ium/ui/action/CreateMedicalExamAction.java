package com.slocpi.ium.ui.action;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.slocpi.ium.data.AssessmentRequestData;
import com.slocpi.ium.data.ClientData;
import com.slocpi.ium.data.ExaminationPlaceData;
import com.slocpi.ium.data.ExaminerData;
import com.slocpi.ium.data.LaboratoryData;
import com.slocpi.ium.data.LaboratoryTestData;
import com.slocpi.ium.data.MedicalRecordData;
import com.slocpi.ium.data.PolicyMedicalRecordData;
import com.slocpi.ium.data.RankData;
import com.slocpi.ium.data.SectionData;
import com.slocpi.ium.data.StatusData;
import com.slocpi.ium.data.SunLifeDeptData;
import com.slocpi.ium.data.SunLifeOfficeData;
import com.slocpi.ium.data.TestProfileData;
import com.slocpi.ium.data.UserProfileData;
import com.slocpi.ium.data.dao.AssessmentRequestDAO;
import com.slocpi.ium.data.dao.ClientDAO;
import com.slocpi.ium.data.dao.LaboratoryDAO;
import com.slocpi.ium.data.dao.RanksDAO;
import com.slocpi.ium.data.dao.TestProfileDAO;
import com.slocpi.ium.data.dao.UserDAO;
import com.slocpi.ium.data.util.DataSourceProxy;
import com.slocpi.ium.ui.form.MedicalRecordDetailForm;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.DateHelper;
import com.slocpi.ium.util.IUMConstants;

/**
 * @author jcristobal
 *
 * To change this generated comment edit the template variable "typecomment":
 * Window>Preferences>Java>Templates.
 * To enable and disable the creation of type comments go to
 * Window>Preferences>Java>Code Generation.
 */
public class CreateMedicalExamAction extends IUMAction {
	private static final Logger LOGGER = LoggerFactory.getLogger(CreateMedicalExamAction.class);
	/**
	 * @see com.slocpi.ium.ui.action.IUMAction#handleAction(org.apache.struts.action.ActionMapping, org.apache.struts.action.ActionForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	public ActionForward handleAction(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response)
		throws Exception {
		
		LOGGER.info("handleAction start");
		String page = "";
		//Connection conn = new DataSourceProxy().getConnection();
		try {
			
			ArrayList medicalExamDetailsList = new ArrayList();			
			MedicalRecordDetailForm medicalRecordDetailForm = (MedicalRecordDetailForm)form;
							
			MedicalRecordData medicalRecordData = new MedicalRecordData();
			if (medicalRecordDetailForm.getActionType().equalsIgnoreCase("view") || medicalRecordDetailForm.getActionType().equalsIgnoreCase("")){												 		
				initializeMedicalRecordData(medicalExamDetailsList, request, medicalRecordData);
		 
				medicalRecordDetailForm = new MedicalRecordDetailForm();				
				medicalRecordDetailForm.setStatus(Long.toString(IUMConstants.STATUS_REQUESTED));					 						
			}
			else{								
				initializeMedicalRecordData(medicalExamDetailsList, request, medicalRecordData);
				getMedicalExamRecordDetails(medicalExamDetailsList, medicalRecordDetailForm, medicalRecordData, request);							
			}	
			
			medicalExamDetailsList.add(medicalRecordData);
			medicalRecordDetailForm.setMedicalRecordData(medicalExamDetailsList);			
			if (request.getParameter("actionType") != null){
				if (!request.getParameter("actionType").equals("view")){
					medicalRecordDetailForm.setActionType((String)request.getParameter("actionType"));				
					request.setAttribute("refNo", medicalRecordDetailForm.getMedicalRecordId());
				}
				else
					medicalRecordDetailForm.setActionType("create");
			}
			else
				medicalRecordDetailForm.setActionType("create");
			
			request.setAttribute("medicalRecordDetailForm", medicalRecordDetailForm);
						
			page = "createMedicalExamPage";
			
		} 
		catch (Exception e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
		  	ActionErrors errors = new ActionErrors();
		  	errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.system.exception", e.getMessage()));
		  	saveErrors(request, errors); 
	  	  	page = "errorPage";   
		}
		
		LOGGER.info("handleAction end");
		LOGGER.debug("PAGE FORWARD-->" + page);
		return (mapping.findForward(page));
	}
	
	private void initializeMedicalRecordData(ArrayList medicalExamDetailsList, HttpServletRequest request, MedicalRecordData medicalRecordData) throws SQLException{
	
		LOGGER.info("initializeMedicalRecordData start");			
		PolicyMedicalRecordData policyMedicalRecordData = new PolicyMedicalRecordData();
		
		medicalRecordData.setPolicyMedicalRecordData(policyMedicalRecordData);
		
		ClientData clientData = new ClientData();
		medicalRecordData.setClient(clientData);
		
		UserProfileData userProfileData = new UserProfileData();
		medicalRecordData.setAgent(userProfileData);
		
		SunLifeOfficeData sunLifeOfficeData = new SunLifeOfficeData();		
		medicalRecordData.setBranch(sunLifeOfficeData);
		
		medicalRecordData.setLabTestInd("L"); 
		medicalRecordData.setRemarks("");
		
		LaboratoryData labData = new LaboratoryData();
		medicalRecordData.setLaboratory(labData);
		
		TestProfileData testProfileData = new TestProfileData();
		medicalRecordData.setTest(testProfileData);
		
		ExaminationPlaceData examinationPlaceData = new ExaminationPlaceData();
		examinationPlaceData.setExaminationPlaceId(0);
		medicalRecordData.setExaminationPlace(examinationPlaceData);
						
		medicalRecordData.setRequestedDate(new Date());
		medicalRecordData.setConductedDate(null);
		medicalRecordData.setReceivedDate(null);				
		medicalRecordData.setFollowUpNumber(0);
		Date followUpDate = DateHelper.add(new Date(), Calendar.DAY_OF_MONTH, (int) medicalRecordData.getFollowUpNumber());		
		medicalRecordData.setFollowUpDate(followUpDate);
		medicalRecordData.setValidityDate(null);
		medicalRecordData.setAppointmentDateTime(null);
		medicalRecordData.setSevenDayMemoInd(true);
		
		StatusData statusData = new StatusData();
		statusData.setStatusId(IUMConstants.STATUS_NOT_PROCEEDED_WITH);		
		statusData.setStatusDesc(Long.toString(IUMConstants.STATUS_NOT_PROCEEDED_WITH)); 
		medicalRecordData.setApplicationStatus(statusData);
		
		medicalRecordData.setReasonForReqt("");
		Date date = new Date();		
		medicalRecordData.setSevenDayMemoDate(date);
		
		SectionData sectionData = new SectionData();		
		sectionData.setSectionId(IUMConstants.DEFAULT_SECTION_ID);
		medicalRecordData.setSection(sectionData);
		
		medicalRecordData.setRequestingParty("");
		medicalRecordData.setChargableTo("");
		
		SunLifeDeptData sunLifeDeptData = new SunLifeDeptData();
		medicalRecordData.setDepartment(sunLifeDeptData);
		
		medicalRecordData.setAmount(0);
		medicalRecordData.setPaidByAgentInd(false);
		
		ExaminerData examinerData = new ExaminerData();
		examinerData.setExaminerId(0);
		medicalRecordData.setExaminer(examinerData);
		
		medicalRecordData.setFacilitator("");
		medicalRecordData.setRequestForLOAInd(true);
		medicalRecordData.setReceivedResults(false);
				 
		LOGGER.info("initializeMedicalRecordData end");
	}
	
	private void getMedicalExamRecordDetails(ArrayList medicalExamDetailsList, MedicalRecordDetailForm medicalRecordDetailForm, MedicalRecordData medicalRecordData, HttpServletRequest request) throws Exception {
		
		LOGGER.info("getMedicalExamRecordDetails start");
		medicalRecordDetailForm.setErr("fromcreate");
		
		ActionErrors errors = new ActionErrors();
		AssessmentRequestData assessmentRequestData = new AssessmentRequestData();
		AssessmentRequestDAO assessmentRequestDAO = new AssessmentRequestDAO();
		if (!medicalRecordDetailForm.getRefNo().equals("")){			
			PolicyMedicalRecordData policyMedicalRecordData = new PolicyMedicalRecordData();
			policyMedicalRecordData.setReferenceNumber(medicalRecordDetailForm.getRefNo().toUpperCase());
			medicalRecordData.setPolicyMedicalRecordData(policyMedicalRecordData);
						
			assessmentRequestData = assessmentRequestDAO.retrieveRequestDetail(medicalRecordDetailForm.getRefNo().toUpperCase());			
			if (assessmentRequestData.getReferenceNumber() == null){					
				errors.add(ActionErrors.GLOBAL_MESSAGE, new ActionError("message.noexisting.singular", "record with reference number " + medicalRecordDetailForm.getRefNo().toUpperCase()));
				saveErrors(request, errors);
				medicalRecordDetailForm.setErr("refNo");
				medicalRecordDetailForm.setLob("");
			} 
			else{
				if (!(assessmentRequestData.getStatus().getStatusId() == IUMConstants.STATUS_AWAITING_REQUIREMENTS || assessmentRequestData.getStatus().getStatusId() == IUMConstants.STATUS_AWAITING_MEDICAL)){
					medicalRecordDetailForm.setErr("refNo");
				}
				medicalRecordDetailForm.setLob(assessmentRequestData.getLob().getLOBCode());				
			}
		}
		LOGGER.debug("Value of err outside : " + medicalRecordDetailForm.getErr()); 
		
		if (!medicalRecordDetailForm.getFacilitator().equals(""))
			medicalRecordData.setFacilitator(medicalRecordDetailForm.getFacilitator());
		else{
			UserProfileData assignedTo = assessmentRequestData.getAssignedTo();
			if (assignedTo != null)
				medicalRecordData.setFacilitator(assignedTo.getUserId());
		}
		
		if (medicalRecordDetailForm.getApplicationStatus().equals("") || (!medicalRecordDetailForm.getApplicationStatus().equals("") && Long.parseLong(medicalRecordDetailForm.getApplicationStatus()) == IUMConstants.STATUS_NOT_PROCEEDED_WITH)){
			if (!medicalRecordDetailForm.getRefNo().equals("")){ 
				if (assessmentRequestData.getStatus() != null)
					medicalRecordData.setApplicationStatus(assessmentRequestData.getStatus());
			}
			else{
				StatusData applicationStatus = new StatusData();
				applicationStatus.setStatusId(IUMConstants.STATUS_NOT_PROCEEDED_WITH);		
				applicationStatus.setStatusDesc(Long.toString(IUMConstants.STATUS_NOT_PROCEEDED_WITH));
				medicalRecordData.setApplicationStatus(applicationStatus);
			}
		}
		else{
			StatusData applicationStatus = new StatusData();
			applicationStatus.setStatusId(Long.parseLong(medicalRecordDetailForm.getApplicationStatus()));
			medicalRecordData.setApplicationStatus(applicationStatus);
		}
		
		if (!medicalRecordDetailForm.getBranch().equals("")){
			SunLifeOfficeData sunLifeOfficeData = new SunLifeOfficeData();
			sunLifeOfficeData.setOfficeId(medicalRecordDetailForm.getBranch());		
			medicalRecordData.setBranch(sunLifeOfficeData);
		}
		else
			if (assessmentRequestData.getBranch() != null)
				medicalRecordData.setBranch(assessmentRequestData.getBranch());

		if (!medicalRecordDetailForm.getAgent().equals("")){			
			UserProfileData userProfileData = new UserProfileData();			
			userProfileData.setUserId(medicalRecordDetailForm.getAgent());
			medicalRecordData.setAgent(userProfileData);
		}
		else
			if (assessmentRequestData.getAgent() != null)
				medicalRecordData.setAgent(assessmentRequestData.getAgent());
				
		if (request.getParameter("refInd") != null){
			
			if (request.getParameter("refInd").equals("1")){				
				UserProfileData userProfileData = new UserProfileData();
				UserDAO userDAO = new UserDAO();
				userProfileData = userDAO.selectUserProfile(medicalRecordDetailForm.getAgent());
				if (userProfileData != null){
					SunLifeOfficeData sunLifeOfficeData = new SunLifeOfficeData();
					sunLifeOfficeData.setOfficeId(userProfileData.getOfficeCode());		
					medicalRecordData.setBranch(sunLifeOfficeData);
				}
			}
			else if (request.getParameter("refInd").equals("2")){//branch
				
				if (!medicalRecordDetailForm.getAgent().equals("")){
					UserProfileData userProfileData = new UserProfileData();
					UserDAO userDAO = new UserDAO();
					userProfileData = userDAO.selectUserProfile(medicalRecordDetailForm.getAgent());
					if (userProfileData != null && medicalRecordDetailForm.getBranch().equals(userProfileData.getOfficeCode())){
						userProfileData.setUserId(medicalRecordDetailForm.getAgent());
						medicalRecordData.setAgent(userProfileData);
					}
					else{
						userProfileData = new UserProfileData();
						SunLifeOfficeData sunLifeOfficeData = new SunLifeOfficeData();
						sunLifeOfficeData.setOfficeId(medicalRecordDetailForm.getBranch());		
						medicalRecordData.setBranch(sunLifeOfficeData);
					}
				}
			}
		}
		
		if (!medicalRecordDetailForm.getClientNo().equals("")){
			ClientData clientData = new ClientData();			
			ClientDAO clientDAO = new ClientDAO();
			clientData = clientDAO.retrieveClient(medicalRecordDetailForm.getClientNo().toUpperCase());
			if (clientData != null){
				
				if (!medicalRecordDetailForm.getRefNo().equals("")){ 
					if (!assessmentRequestDAO.isClientExists(clientData.getClientId(), medicalRecordDetailForm.getRefNo().toUpperCase())){
						errors.add(ActionErrors.GLOBAL_MESSAGE, new ActionError("message.noexisting.singular", "client record with id " + medicalRecordDetailForm.getClientNo().toUpperCase() + " for reference number " + medicalRecordDetailForm.getRefNo().toUpperCase()));
						saveErrors(request, errors); 
					}						
				}
				
			}
			else{	
				clientData = new ClientData();
				clientData.setClientId(medicalRecordDetailForm.getClientNo());
				
				errors.add(ActionErrors.GLOBAL_MESSAGE, new ActionError("message.noexisting.singular", "client record with id " + medicalRecordDetailForm.getClientNo().toUpperCase()));
				saveErrors(request, errors); 
			
			}
			medicalRecordData.setClient(clientData);			
		}
		else{ 	
			ClientData clientData = new ClientData();
			ClientDAO clientDAO = new ClientDAO();
			clientData = clientDAO.retrieveClientData(medicalRecordDetailForm.getLastName(), medicalRecordDetailForm.getFirstName(), medicalRecordDetailForm.getBirthDate());
			if (clientData == null){
				clientData = new ClientData();
				clientData.setClientId(medicalRecordDetailForm.getClientNo().toUpperCase());
				clientData.setLastName(medicalRecordDetailForm.getLastName());
				clientData.setGivenName(medicalRecordDetailForm.getFirstName());
				clientData.setBirthDate(DateHelper.parse(medicalRecordDetailForm.getBirthDate(),"ddMMMyyyy"));
							
			}				
			medicalRecordData.setClient(clientData);			
		}	
				
		if (medicalRecordDetailForm.getType().equalsIgnoreCase("Y")){
			medicalRecordData.setLabTestInd("L"); 
		}
		else{
			medicalRecordData.setLabTestInd("M");
		}
		
		if (!medicalRecordDetailForm.getRemarks().equals("")){
			medicalRecordData.setRemarks(medicalRecordDetailForm.getRemarks());
		}
				
		if (medicalRecordDetailForm.getType().equalsIgnoreCase("Y")){
			LaboratoryData laboratoryData = new LaboratoryData();
			if (!medicalRecordDetailForm.getLabName().equals(""))						
				laboratoryData.setLabId(Long.parseLong(medicalRecordDetailForm.getLabName()));
			medicalRecordData.setLaboratory(laboratoryData);
		}
		else if (medicalRecordDetailForm.getType().equalsIgnoreCase("N")){
			ExaminerData examinerData = new ExaminerData();
			if (!medicalRecordDetailForm.getExaminer().equals(""))
				examinerData.setExaminerId(Long.parseLong(medicalRecordDetailForm.getExaminer()));			
			medicalRecordData.setExaminer(examinerData);	
		}
		
		if (!medicalRecordDetailForm.getTest().equals("")){					
			TestProfileData testProfileData = new TestProfileData();
			testProfileData.setTestId(Long.parseLong(medicalRecordDetailForm.getTest()));
			medicalRecordData.setTest(testProfileData);
		}
		
		if (!medicalRecordDetailForm.getFollowUpNumber().equals(""))
			medicalRecordData.setFollowUpNumber(Long.parseLong(medicalRecordDetailForm.getFollowUpNumber()));
					
		if (request.getParameter("refInd") != null){
			if (request.getParameter("refInd").equals("3")){
				TestProfileData testProfileData = new TestProfileData();				
				if (!medicalRecordDetailForm.getTest().equals("")){
					TestProfileDAO testProfileDAO = new TestProfileDAO();
					testProfileData = testProfileDAO.retrieveTestProfile(Long.parseLong(medicalRecordDetailForm.getTest()));
					if (testProfileData.getFollowUpNumber() > 0)
						medicalRecordData.setFollowUpNumber(testProfileData.getFollowUpNumber());
					else
						medicalRecordData.setFollowUpNumber(0);
				}
			}
		}			
		
		if (!medicalRecordDetailForm.getExamPlace().equals("")){
			ExaminationPlaceData examinationPlaceData = new ExaminationPlaceData();
			examinationPlaceData.setExaminationPlaceId(Long.parseLong(medicalRecordDetailForm.getExamPlace()));
			medicalRecordData.setExaminationPlace(examinationPlaceData);
		} else {
			medicalRecordData.setExaminationPlace(null);
		}
	
		if (!medicalRecordDetailForm.getDateRequested().equals(""))
			medicalRecordData.setRequestedDate(DateHelper.parse(medicalRecordDetailForm.getDateRequested(),"ddMMMyyyy"));
		if (!medicalRecordDetailForm.getDateConducted().equals(""))
			medicalRecordData.setConductedDate(DateHelper.parse(medicalRecordDetailForm.getDateConducted(),"ddMMMyyyy"));
		if (!medicalRecordDetailForm.getDateReceived().equals(""))
			medicalRecordData.setReceivedDate(DateHelper.parse(medicalRecordDetailForm.getDateReceived(),"ddMMMyyyy"));		
		if (!medicalRecordDetailForm.getFollowUpDate().equals(""))
			medicalRecordData.setFollowUpDate(DateHelper.parse(medicalRecordDetailForm.getFollowUpDate(),"ddMMMyyyy"));
		if (!medicalRecordDetailForm.getDateValidity().equals(""))
			medicalRecordData.setValidityDate(DateHelper.parse(medicalRecordDetailForm.getDateValidity(),"ddMMMyyyy"));
		
		if (!medicalRecordDetailForm.getAppointmentDate().equals("")){
			medicalRecordData.setAppointmentDateTime(DateHelper.parse(medicalRecordDetailForm.getAppointmentDate(),"ddMMMyyyy"));
			if (!medicalRecordDetailForm.getAppointmentTime().equals("")){
				medicalRecordData.setAppointmentDateTime(DateHelper.parse((medicalRecordDetailForm.getAppointmentDate() + " " +medicalRecordDetailForm.getAppointmentTime()), "ddMMMyyyy hh:mm aaa"));
			}
		}

		if (medicalRecordDetailForm.getSevenDayMemo().equalsIgnoreCase("Y")){
			medicalRecordData.setSevenDayMemoInd(true);
		}
		else if (medicalRecordDetailForm.getSevenDayMemo().equalsIgnoreCase("N")){
			medicalRecordData.setSevenDayMemoInd(false);
		}
		
		StatusData statusData = new StatusData(); 
		if (!medicalRecordDetailForm.getStatus().equals("")){			
			statusData.setStatusId(Long.parseLong(medicalRecordDetailForm.getStatus()));			
		}
		medicalRecordData.setStatus(statusData);
				
		if (!medicalRecordDetailForm.getReason().equals(""))
			medicalRecordData.setReasonForReqt(medicalRecordDetailForm.getReason());
			
		if (!medicalRecordDetailForm.getSevenDayMemoDate().equals(""))
			medicalRecordData.setSevenDayMemoDate(DateHelper.parse(medicalRecordDetailForm.getSevenDayMemoDate(),"ddMMMyyyy"));
		
		if (!medicalRecordDetailForm.getSection().equals("")){
			SectionData sectionData = new SectionData();
			sectionData.setSectionId(medicalRecordDetailForm.getSection());
			medicalRecordData.setSection(sectionData);
		}
		
		if (!medicalRecordDetailForm.getReqParty().equals(""))
			medicalRecordData.setRequestingParty(medicalRecordDetailForm.getReqParty());
			
		if (!medicalRecordDetailForm.getChargeTo().equals(""))
			medicalRecordData.setChargableTo(medicalRecordDetailForm.getChargeTo());
	
		if (!medicalRecordDetailForm.getDepartment().equals("")){			
			SunLifeDeptData sunLifeDeptData = new SunLifeDeptData();
			sunLifeDeptData.setDeptId(medicalRecordDetailForm.getDepartment());
			medicalRecordData.setDepartment(sunLifeDeptData);
		}
		
		if (medicalRecordDetailForm.getType().equalsIgnoreCase("Y")){
			if (!medicalRecordDetailForm.getLabName().equals("") && !medicalRecordDetailForm.getTest().equals("")){
				LaboratoryDAO laboratoryDAO = new LaboratoryDAO();
				LaboratoryTestData laboratoryTestData = laboratoryDAO.retrieveLaboratoryTestData(Long.parseLong(medicalRecordDetailForm.getTest()), Long.parseLong(medicalRecordDetailForm.getLabName()));
				medicalRecordData.setAmount(laboratoryTestData.getTestFee());
			}
		}
		else if (medicalRecordDetailForm.getType().equalsIgnoreCase("N")){
			if (!medicalRecordDetailForm.getExaminer().equals("")){
				RanksDAO ranksDAO = new RanksDAO();
				RankData rankData = ranksDAO.retrieveRankData(Long.parseLong(medicalRecordDetailForm.getExaminer()));
				if (rankData != null)
					medicalRecordData.setAmount(rankData.getRankFee());
				else
					medicalRecordData.setAmount(0);				
			}		
		}
				
		if (medicalRecordDetailForm.getPayAgent().equalsIgnoreCase("Y")){
			medicalRecordData.setPaidByAgentInd(true);
		}
		else if (medicalRecordDetailForm.getPayAgent().equalsIgnoreCase("N")){
			medicalRecordData.setPaidByAgentInd(false);
		}
		
		if (medicalRecordDetailForm.getRequestForLOAInd().equalsIgnoreCase("Y")){
			medicalRecordData.setRequestForLOAInd(true);
		}
		else if (medicalRecordDetailForm.getRequestForLOAInd().equalsIgnoreCase("N")){
			medicalRecordData.setRequestForLOAInd(false);
			
		}
		if (medicalRecordDetailForm.getResultsReceivedInd().equalsIgnoreCase("Y")){
			medicalRecordData.setReceivedResults(true);
			
		}
		else if (medicalRecordDetailForm.getResultsReceivedInd().equalsIgnoreCase("N")){ 
			medicalRecordData.setReceivedResults(false);
		}
		LOGGER.info("getMedicalExamRecordDetails end");	 
		
	}
	
	/*private void closeConnection(Connection conn) throws SQLException {
		
		
		if (!conn.isClosed())
			conn.close();
		
	}*/
	
	private Date add(String strDate, int noOfDays){
		
		LOGGER.info("add start");
		String [] months = {"JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC"};
	  	
	  	String dateConducted = strDate;
		String day = dateConducted.substring(0, 2);
      	String month = dateConducted.substring(2, 5);
      	String yr = dateConducted.substring(5,9);
      	
      	int mo = 0;
      	int i = 0;
      	for (i=0; i < months.length-1; i++) {
        	if (months[i].equals(month)) {
          		mo = i+1;        
          		break;
        	}
      	}      		
      
		Date validityDate = DateHelper.add(DateHelper.parse(mo + "/" + day + "/" + yr), Calendar.DAY_OF_MONTH,  (int) noOfDays);
		
		LOGGER.info("add end");
		return validityDate;		
	}
	
}
