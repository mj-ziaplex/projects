/*
 * Created on Mar 10, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.ui.action;

import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.slocpi.ium.IUMException;
import com.slocpi.ium.data.JobScheduleData;
import com.slocpi.ium.data.UserData;
import com.slocpi.ium.data.UserProfileData;
import com.slocpi.ium.ui.form.MIBScheduleForm;
import com.slocpi.ium.ui.util.StateHandler;
import com.slocpi.ium.underwriter.Reference;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.DateHelper;
import com.slocpi.ium.util.IUMConstants;

/**
 * @author Cris
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class SaveMIBScheduleAction extends IUMAction{
	private static final Logger LOGGER = LoggerFactory.getLogger(SaveMIBScheduleAction.class);
	
	public ActionForward handleAction(ActionMapping mapping, ActionForm form,
									  HttpServletRequest request, HttpServletResponse response){
		
		LOGGER.info("handleAction start");
		String page = "";
		
		StateHandler sh = new StateHandler();
		UserData ud = sh.getUserData(request);
		UserProfileData userProfile = ud.getProfile();
		String userId = userProfile.getUserId();

		MIBScheduleForm mibForm = (MIBScheduleForm) form;
		
		try {
			String function = mibForm.getFunctionSelected();
			if (function != null && !function.equals("")){
				if (function.equals("save")){
					Reference ref = new Reference();
					JobScheduleData sched = new JobScheduleData();
					sched.setType(IUMConstants.SCHEDULE_TYPE_MIB_EXPORT);
					
					Date date = DateHelper.parse(mibForm.getSchedule(), "h:mm a");
					
					sched.setTime(DateHelper.sqlTimestamp(date));
					sched.setDescription(mibForm.getDescription());
					sched.setCreatedBy(userId);
					sched.setCreatedDate(DateHelper.sqlTimestamp(new java.util.Date()));
					sched.setUpdatedBy(userId);
					sched.setUpdatedDate(DateHelper.sqlTimestamp(new java.util.Date()));

					ref.setupMIBSchedule(sched);

				} else if (function.equals("export")){
					//to do
				}
				
				page = "mibSchedulePage";
				
			}
			
		} catch (IUMException e){
			LOGGER.error(CodeHelper.getStackTrace(e));
			constructError("error.system.exception",e.getMessage(),request);
			page = "errorPage";
		}
		LOGGER.info("handleAction end");
		LOGGER.debug("PAGE FORWARD-->" + page);
		return mapping.findForward(page);
	}
	
	
	private void constructError (String key, String parameter, HttpServletRequest request){
		
		LOGGER.info("constructError start");
		ActionErrors error = new ActionErrors();
		error.add(ActionErrors.GLOBAL_ERROR, new ActionError(key, parameter));
		saveErrors(request, error);
		LOGGER.info("constructError end");
	}


}
