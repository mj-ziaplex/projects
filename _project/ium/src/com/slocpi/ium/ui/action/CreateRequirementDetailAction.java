/*
 * Created on Mar 4, 2004
 * package name = com.slocpi.ium.ui.action
 * file name    = CreateRequirementDetailAction.java
 */
package com.slocpi.ium.ui.action;

import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.slocpi.ium.data.RequirementData;
import com.slocpi.ium.data.UserData;
import com.slocpi.ium.data.UserProfileData;
import com.slocpi.ium.ui.form.RequirementForm;
import com.slocpi.ium.ui.util.Page;
import com.slocpi.ium.ui.util.Pagination;
import com.slocpi.ium.ui.util.StateHandler;
import com.slocpi.ium.underwriter.Reference;

/**
 * This class is responsible for displaying the IUM's list of requirements.
 * This class will display the list and allow the viewing of a specific requirement.
 * @author Engel
 * 
 */
public class CreateRequirementDetailAction extends IUMAction {
	private static final Logger LOGGER = LoggerFactory.getLogger(CreateRequirementDetailAction.class);
	/* (non-Javadoc)
	 * @see com.slocpi.ium.ui.action.IUMAction#handleAction(org.apache.struts.action.ActionMapping, org.apache.struts.action.ActionForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	public ActionForward handleAction(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response)
		throws Exception {
		
		LOGGER.info("handleAction start");
		String page="viewDetails";
		RequirementForm reqfrm = (RequirementForm)form;
		Reference refBO = new Reference();
		StateHandler sessionHandler = new StateHandler();
		UserData userData = sessionHandler.getUserData(request);
		UserProfileData userPref = userData.getProfile();
		
		String pageNumber = reqfrm.getPageNumber();
		
		if(pageNumber==null || pageNumber.equals("")){
			pageNumber="1";
		}
			
		
		if( reqfrm!=null&&reqfrm.getReqCode()!=null&&!reqfrm.getReqCode().equals("") ){
			RequirementData data = refBO.getRequirementDetail(reqfrm.getReqCode());
			if(data.getReqtCode()!=null){
				reqfrm = dataToForm(data);	
			}
		}
		
		
		ArrayList list = refBO.getRequirementDetails();
		ArrayList arrForm = new ArrayList();
		for(int i=0; i<list.size(); i++){
			RequirementData data = (RequirementData)list.get(i);
			RequirementForm frm = dataToForm(data);
			arrForm.add(frm); 
		}
		
		reqfrm.setPageNumber(pageNumber);
		
		int recPerPage = (userPref.getRecordsPerView()!=0)?userPref.getRecordsPerView():10;
		Pagination pgn = new Pagination(arrForm,recPerPage);
		Page pageObj = pgn.getPage(Integer.parseInt(pageNumber));
		
		request.setAttribute("pageRecord", pageObj);
		request.setAttribute("requirement",reqfrm);
		LOGGER.info("handleAction end");
		LOGGER.debug("PAGE FORWARD-->" + page);
		
		return (mapping.findForward(page));
	}

	private RequirementForm dataToForm(RequirementData data){
		
		LOGGER.info("dataToForm start");
		RequirementForm req = null;
		if(data!=null){
			req = new RequirementForm();
			req.setReqCode(   data.getReqtCode()   );
			req.setReqDesc(   data.getReqtDesc()   );
			req.setReqLevel(  data.getLevel()	   );
			req.setValidity(  String.valueOf(  data.getValidity()  )   );
			req.setFollowUpNo(  String.valueOf(data.getFollowUpNum() )  );
			req.setFormId(  String.valueOf(data.getFormID())  );
			
		 
			String testDateIndicator = data.getTestDateIndicator();
			if (testDateIndicator == null){
				testDateIndicator = "0";
			}
			req.setTestDateIndicator(testDateIndicator);
					
		}
		LOGGER.info("dataToForm end");
		return req;
	}

}
