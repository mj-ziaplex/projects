/*
 * Created on Mar 5, 2004
 * package name = com.slocpi.ium.ui.action
 * file name    = MaintainRequirementDetailAction.java
 */
package com.slocpi.ium.ui.action;

import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.slocpi.ium.IUMException;
import com.slocpi.ium.data.RequirementData;
import com.slocpi.ium.data.UserData;
import com.slocpi.ium.data.UserProfileData;
import com.slocpi.ium.ui.form.RequirementForm;
import com.slocpi.ium.ui.util.StateHandler;
import com.slocpi.ium.underwriter.Reference;
import com.slocpi.ium.underwriter.UnderWriterException;
import com.slocpi.ium.util.CodeHelper;

/**
 * TODO Class Description of MaintainRequirementDetailAction.java
 * @author Engel
 * 
 */
public class MaintainRequirementDetailAction extends IUMAction {

	private static final Logger LOGGER = LoggerFactory.getLogger(MaintainRequirementDetailAction.class);
	/* (non-Javadoc)
	 * @see com.slocpi.ium.ui.action.IUMAction#handleAction(org.apache.struts.action.ActionMapping, org.apache.struts.action.ActionForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	public ActionForward handleAction(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response)
		throws Exception {
		
		LOGGER.info("handleAction start");
		String page="reloadList";
		String mode = request.getParameter("mode");
		RequirementForm reqf = (RequirementForm)form;
		StateHandler sh = new StateHandler();
		UserData ud = sh.getUserData(request);
		UserProfileData profile = ud.getProfile();
		String userId = profile.getUserId();
		
		try{
			saveRequirement(reqf,mode,userId);		
		} catch(UnderWriterException e){
			LOGGER.error(CodeHelper.getStackTrace(e));
			ActionErrors errors = new ActionErrors();
			errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.duplicateRequirementCode", e.getMessage()));
			saveErrors(request, errors);
			page="viewDetails"; 
		}catch(IUMException e){
			LOGGER.error(CodeHelper.getStackTrace(e));
			ActionErrors errors = new ActionErrors();
			errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.system.exception", e.getMessage()));
			saveErrors(request, errors);
			page="errorPage"; 
		}
		
		LOGGER.info("handleAction end");
		LOGGER.debug("PAGE FORWARD-->" + page);
		return (mapping.findForward(page));
	}

	private void saveRequirement(RequirementForm reqf,String mode,String userId)throws UnderWriterException, IUMException{
		
		LOGGER.info("saveRequirement start");
		RequirementData data = new RequirementData();
		data.setTestDateIndicator(reqf.getTestDateIndicator());
		data.setReqtCode(reqf.getReqCode());
		data.setReqtDesc(reqf.getReqDesc());
		data.setLevel(reqf.getReqLevel());
		data.setValidity( Long.parseLong(reqf.getValidity())  );
		data.setFollowUpNum(Integer.parseInt(reqf.getFollowUpNo()));
		if(reqf==null||reqf.getFormId()==null||reqf.getFormId().equals(""))
		    data.setFormID(  0L  );
		else
			data.setFormID(  Long.parseLong(reqf.getFormId())  );
		if(data.getFormID()==0L)
			data.setFormIndicator("0");
		else
		    data.setFormIndicator("1");
		
		if (mode != null) {
			if (mode.equals("add")) {
			   data.setCreateDate(new Date());
		   	   data.setCreatedBy(userId);
		    }
		} else {
			data.setUpdateDate(new Date());
			data.setUpdatedBy(userId);       	
		}
		
		
		Reference ref = new Reference();
		
			if (mode != null) {
			   if (mode.equals("add")) {
				RequirementData checkExist = ref.getRequirementDetail(data.getReqtCode());
				if(checkExist!=null&&checkExist.getReqtCode()!=null&&!checkExist.getReqtCode().equals("")){
					LOGGER.warn("Duplicate Requirement Code Error.");
					throw new UnderWriterException("Duplicate Requirement Code Error.");
				}else{
					ref.createRequirement(data);
				}
			   }
			}else {
				ref.editRequirement(data);
			}		
	
			LOGGER.info("saveRequirement end");
	}
	
}