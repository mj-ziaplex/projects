package com.slocpi.ium.ui.util;

import java.util.ArrayList;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

/**
 * Page.java 
 * Description:
 *
 * @version $Revision: 1.1 $
 * @author $Author: pw12 $ $Date: 2012/08/01 07:08:06 $
 */
public class Page {
	private ArrayList list;
	private ArrayList pageNumbers;
	
	public String toString(){
		
		return ToStringBuilder.reflectionToString(this, ToStringStyle.DEFAULT_STYLE);
	}
	
	/**
	 * Returns the list.
	 * @return ArrayList
	 */
	public ArrayList getList() {
		return list;
	}

	/**
	 * Returns the pageNumbers.
	 * @return ArrayList
	 */
	public ArrayList getPageNumbers() {
		return pageNumbers;
	}

	/**
	 * Sets the list.
	 * @param list The list to set
	 */
	public void setList(ArrayList list) {
		this.list = list;
	}

	/**
	 * Sets the pageNumbers.
	 * @param pageNumbers The pageNumbers to set
	 */
	public void setPageNumbers(ArrayList pageNumbers) {
		this.pageNumbers = pageNumbers;
	}

}
