package com.slocpi.ium.ui.util;

import java.util.HashMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.slocpi.ium.IUMException;
/**
 * Access.java 
 * Description:
 *
 * @version $Revision: 1.1 $
 * @author $Author: pw12 $ $Date: 2012/08/01 07:08:06 $
 */
public class Access {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(Access.class);
	/**
	 * This method checks the access of a user. 
	 * @param usrAccess
	 * @param pageID
	 * @param accessID
	 * @return
	 * @throws IUMException
	 */
	 public boolean hasAccess(
			 final HashMap usrAccess,
			 final String pageID,
			 final String accessID) throws IUMException {
		 
		 
		boolean withAccess = false;
		
       HashMap pageAccess = (HashMap)  usrAccess.get(pageID);
       if (pageAccess !=null && !usrAccess.isEmpty()) {
    	   
       	  if (pageAccess.get(accessID) != null) {
       		  
          		withAccess = ((Boolean) pageAccess.get(accessID)).booleanValue();
       	  }
       }
       
       return withAccess;
   	}
  	 
}
