package com.slocpi.ium.ui.util;

import java.util.ArrayList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.slocpi.ium.IUMException;
import com.slocpi.ium.data.RolesData;

/**
 * Roles.java 
 * Description: Checks Roles contained in the arraylist
 *
 * @version $Revision: 1.1 $
 * @author $Author: pw12 $ $Date: 2012/08/01 07:08:06 $
 */
public class Roles {
	private static final Logger LOGGER = LoggerFactory.getLogger(Roles.class);
	
	 public boolean getRoles(ArrayList aryRoles, String usrRoles) throws IUMException {
		 
		LOGGER.info("getRoles start");
		boolean withRole = false;
		
		for(int i=0; i<aryRoles.size(); i++){
			RolesData rolesData = (RolesData)aryRoles.get(i);  
			if (usrRoles.equals(rolesData.getRolesId())){
				withRole = true;
			}
		}

		LOGGER.info("getRoles end");
		return withRole;
   	}
  	 

}
