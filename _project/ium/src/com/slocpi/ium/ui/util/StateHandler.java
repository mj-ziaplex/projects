/*
 * Created on Jan 5, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slocpi.ium.ui.util;

import java.util.HashMap;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import com.slocpi.ium.data.UserData;

/**
 * @author daguila
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class StateHandler {
	
	private String USER_KEY = "user";
	private String USERACCESS_KEY = "useraccess";
	private String ID_KEY = "notificationid";
	private String PAGE_KEY = "notificationpage";
	
	public String toString(){
		return ToStringBuilder.reflectionToString(this, ToStringStyle.DEFAULT_STYLE);
	}
	
	public void setUserData(HttpServletRequest request, UserData user) {
		request.getSession().setAttribute(USER_KEY,user);
	}
	
	public UserData getUserData(HttpServletRequest request) {
		return (UserData)request.getSession().getAttribute(USER_KEY);
	}
	
	public void removeUserData(HttpServletRequest request) {
		request.getSession().removeAttribute(USER_KEY);
	}


	public void setUserAccess(HttpServletRequest request, HashMap usrAccess) {
			request.getSession().setAttribute(USERACCESS_KEY,usrAccess);
	}
	
	public HashMap getUserAccess(HttpServletRequest request) {
		return (HashMap)request.getSession().getAttribute(USERACCESS_KEY);
	}
	
	public void removeUserAccess(HttpServletRequest request) {
		request.getSession().removeAttribute(USERACCESS_KEY);
	}
	
	public void setNotificationID(HttpServletRequest request, String id) {
			request.getSession().setAttribute(ID_KEY,id);
	}
	
	public String getNotificationID(HttpServletRequest request) {
			return (String)request.getSession().getAttribute(ID_KEY);
	}
	
	public void removeNotificationID(HttpServletRequest request) {
			request.getSession().removeAttribute(ID_KEY);
	}
	
	public void setNotificationPage(HttpServletRequest request, String page) {
			request.getSession().setAttribute(PAGE_KEY,page);
	}
	
	public String getNotificationPage(HttpServletRequest request) {
			return (String)request.getSession().getAttribute(PAGE_KEY);
	}
	
	public void removeNotificationPage(HttpServletRequest request) {
			request.getSession().removeAttribute(PAGE_KEY);
	}
	
}
