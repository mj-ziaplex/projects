package com.slocpi.ium.ui.util;

import java.util.ArrayList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Pagination.java 
 * Description:
 *
 * @version $Revision: 1.1 $
 * @author $Author: pw12 $ $Date: 2012/08/01 07:08:06 $
 */
public class Pagination {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(Pagination.class);
	private ArrayList dataObjects;
	private ArrayList pageNumbers;
	
	private int beginRowCount;
	private int endRowCount;
	private int noOfRecords; 
	private int noOfRowsPerPage;  
	
	public Pagination(ArrayList dataObjects, int noOfRowsPerPage) throws IUMWebException{
		
		LOGGER.info("Pagination start");
		if(dataObjects != null){
			this.dataObjects = dataObjects;
			this.noOfRowsPerPage = noOfRowsPerPage;
			this.noOfRecords = dataObjects.size();
			
		}else{
			LOGGER.warn("Object passed as List of data is null");
			throw new IUMWebException("Object passed as List of data is null");
		}
		LOGGER.info("Pagination end");
	}
	
	public Page getPage(int pageNo) throws IUMWebException{
		
		LOGGER.info("getPage start");
		Page page = new Page();
		ArrayList recs = new ArrayList();
		detBeginAndEndRowCount(pageNo);
		ArrayList pageNums = determinePageNumbers(pageNo);
		
		for(int i=beginRowCount-1;i<endRowCount;i++){
			recs.add(this.dataObjects.get(i));
		}
				
		page.setList(recs);
		page.setPageNumbers(pageNums);
		LOGGER.info("getPage end");
		return page;
	}
	
	
	
	/*
	 * Method determinePageNumbers.
	 * desc: populates collection of navLinks per DisplayPage. follows a 
	 *  rule building page navigation links
	 * @param pageNo
	 * @return Collection
	 * @throws PagedResultsException
	 */
	private ArrayList determinePageNumbers(int pageNo) throws IUMWebException{
		
		LOGGER.info("determinePageNumbers start");
		int noOfPages = totalNoOfPages();
		ArrayList navLinks = new ArrayList();

			if(noOfPages==pageNo){
				pageNo--;
			}

			int lowerBound = (pageNo/10)*10;
			int upperBound = (pageNo/10+1)*10;
			int pageNos = lowerBound+1;

			if(lowerBound==0 && noOfPages<14){
				for(int i=1;i<=noOfPages;i++) {
					navLinks.add(new Integer(i));
					pageNos++;
				}
			} else if(lowerBound>0 && noOfPages<=upperBound) {
				int startPage = 1;
				int midPoint = lowerBound/2;
				navLinks.add(new Integer(startPage));
				navLinks.add(new Integer(midPoint));
				for(int i=lowerBound;i<noOfPages+1;i++){
					navLinks.add(new Integer(i));
				}
			} else if(lowerBound==0 && noOfPages>=upperBound) {
				int oneThird = (((noOfPages-upperBound)/3+upperBound)/10)*10;
				int twoThirds = (((noOfPages-upperBound)*2/3+upperBound)/10)*10;
				for(int i=1;i<11;i++){
					navLinks.add(new Integer(i));
				}
					if(upperBound!=oneThird && oneThird!=twoThirds){
						navLinks.add(new Integer(oneThird));
						navLinks.add(new Integer(twoThirds));
						navLinks.add(new Integer(noOfPages));
					}else {
						if((noOfPages-upperBound)/3 >1){
							oneThird = Math.round((noOfPages-upperBound)/3+upperBound);
							twoThirds = Math.round((noOfPages-upperBound)*2/3+upperBound);
							navLinks.add(new Integer(oneThird));
							navLinks.add(new Integer(twoThirds));
							navLinks.add(new Integer(noOfPages));
						}else {
							for(int i=upperBound+1;i<=noOfPages;i++){
								navLinks.add(new Integer(i));
							}
						}
					}
			} else if(lowerBound>0 && noOfPages>upperBound) {
				int startPage = 1;
				int midPoint = ((lowerBound/2)/10)*10;
				int oneThird = (((noOfPages-upperBound)/3+upperBound)/10)*10;
				int twoThirds = (((noOfPages-upperBound)*2/3+upperBound)/10)*10;

				if(midPoint==0){
					midPoint = (lowerBound/2);
				}

				navLinks.add(new Integer(startPage));
				navLinks.add(new Integer(midPoint));

				for(int i=lowerBound;i<upperBound+1;i++){
					navLinks.add(new Integer(i));
				}

				if(upperBound!=oneThird && oneThird!=twoThirds){
					navLinks.add(new Integer(oneThird));
					navLinks.add(new Integer(twoThirds));
					navLinks.add(new Integer(noOfPages));
				}else {
					if((noOfPages-upperBound)/3 >1){
							oneThird = Math.round((noOfPages-upperBound)/3+upperBound);
							twoThirds = Math.round((noOfPages-upperBound)*2/3+upperBound);
							navLinks.add(new Integer(oneThird));
							navLinks.add(new Integer(twoThirds));
							navLinks.add(new Integer(noOfPages));
						}else {
							for(int i=upperBound+1;i<=noOfPages;i++){
								navLinks.add(new Integer(i));
							}
						}
				}
			}
			LOGGER.info("determinePageNumbers end");
		return navLinks;
	}	
	
	/*
	 * Method detBeginAndEndRowCount.
	 * determines the indices of the first and last record of the page to be displayed  
	 * @param int pageNo
	 * @throws PagedResultsException
	 */	
	private void detBeginAndEndRowCount(int pageNo) throws IUMWebException{
						
		LOGGER.info("detBeginAndEndRowCount start");
		this.beginRowCount = 0;
		this.endRowCount = 0;
		
			beginRowCount = (((pageNo-1) * noOfRowsPerPage) + 1);
			if(pageNo*noOfRowsPerPage > noOfRecords){
				endRowCount = noOfRecords;
			}else{
				endRowCount = pageNo * noOfRowsPerPage;
			}
					
			LOGGER.info("detBeginAndEndRowCount end");
	}
	
	/*
	 * Method totalNoOfPages.
	 * desc: a private method that determines the total number
	 *  of pages, given the number of records
	 *  and number of rows per page.
	 * @return int
	 * @throws PagedResultsException
	 */
	private int totalNoOfPages() throws IUMWebException{
		
		LOGGER.info("totalNoOfPages start");
		int noOfPages=0;
		try{
			if(noOfRecords%noOfRowsPerPage>0){
				noOfPages = noOfRecords/noOfRowsPerPage+1;
			}else {
				noOfPages = noOfRecords/noOfRowsPerPage;
			}
		}
		catch(ArithmeticException e){
			LOGGER.warn("no of rows per page is zero or is not a valid integer");
			throw new IUMWebException("no of rows per page is zero or is not a valid integer");
		}
		LOGGER.info("totalNoOfPages end");
		return noOfPages;
	}
	
}
