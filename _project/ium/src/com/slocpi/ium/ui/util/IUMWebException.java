package com.slocpi.ium.ui.util;

import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.Serializable;

/**
 * IUMWebException.java 
 * Description:
 *
 * @version $Revision: 1.1 $
 * @author $Author: pw12 $ $Date: 2012/08/01 07:08:06 $
 */
public class IUMWebException extends Exception
      implements Serializable{
	
	private Throwable cause = null;

    /**
     * Creates a new PagedResultsException object.
     *
     * @param message the message describing this Exception
     */
    public IUMWebException(String message) {
        super(message);
    }

    /**
     * Creates a new Ex object.
     *
     * @param message the message describing this Exception
     * @param cause a throwable cause, such as another exception
     */

    public IUMWebException( String   message,
               Throwable cause )
    {
        super( message );
        this.cause = cause;
    }

    /**
     * Returns the throwable cause of this Exception.
     *
     * @return the cause
     */
    public Throwable getCause() {
        return cause;
    }

    /**
     * Prints a stack trace of the throwable cause to System.err.
     */
    public void printStackTrace() {
        super.printStackTrace();
        if (cause != null) {
            System.err.println("Caused by: ");
            cause.printStackTrace();
        }
    }

    /**
     * Prints a stack trace of the throwable cause to the given PrintStream.
     *
     * @param ps the PrintStream to which the cause should be sent.
     */
    public void printStackTrace(PrintStream ps) {
        super.printStackTrace(ps);
        if (cause != null) {
            ps.println("Caused by: ");
            cause.printStackTrace(ps);
        }
    }

    /**
     * Prints a stack trace of the throwable cause to the given PrintWriter.
     *
     * @param pw the PrintWriter to which the cause should be sent.
     */
    public void printStackTrace(PrintWriter pw) {
        super.printStackTrace(pw);
        if (cause != null) {
            pw.println("Caused by: ");
            cause.printStackTrace(pw);
        }
    }
}
