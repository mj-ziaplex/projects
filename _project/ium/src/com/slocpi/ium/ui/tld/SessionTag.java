package com.slocpi.ium.ui.tld;

//struts package
import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspTagException;
import javax.servlet.jsp.tagext.TagSupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.slocpi.ium.ui.util.StateHandler;
import com.slocpi.ium.util.CodeHelper;


/**
 * Forwards the control to the login page if session has expired.
 * @author: Ma. Theresa Vicencio
 * @version: 1.0
 */

public final class SessionTag extends TagSupport {
	private static final Logger LOGGER = LoggerFactory.getLogger(SessionTag.class);
	private String page = "/expiredSession.do";
  
	/**
	 * Return the forward page.
	 */
	public String getPage() {
		return (page);
	}


	/**
	 * Set the forward page.
	 * @param page The new forward page
	 */
	public void setPage(String _page) {
		page = _page;
	}


	/**
	 * Implements the TagSupport method.
	 * @return int
	 */
	public int doStartTag() {
		return SKIP_BODY;
	}


	/**
	 * Perform our logged-in user check by looking for the existence of
	 * a session scope bean under the specified name.  If this bean is not
	 * present, control is forwarded to the specified logon page.
	 * @exception JspTagException if a JSP exception has occurred
	 */
   public int doEndTag() throws JspTagException {	
	   
    LOGGER.info("doEndTag start");
	StateHandler sh = new StateHandler();
	HttpServletRequest request = (HttpServletRequest)pageContext.getRequest();
	
	//check if user has logged in
	if (sh.getUserData(request) != null) {		
		return (EVAL_PAGE);
	}	
	else {
	  try {
		LOGGER.warn("*** session has expired ***");
		pageContext.forward(page);
	  }
	  catch (Exception e) {
		 LOGGER.error(CodeHelper.getStackTrace(e));
		throw new JspTagException(e.toString());
	  }
	  LOGGER.info("doEndTag end");
	  return (SKIP_PAGE);
	}		
  }


}
