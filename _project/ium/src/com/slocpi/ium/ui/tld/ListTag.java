package com.slocpi.ium.ui.tld;

//struts package
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import javax.servlet.jsp.JspTagException;
import javax.servlet.jsp.tagext.TagSupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.slocpi.ium.data.AccessTemplateData;
import com.slocpi.ium.data.ExaminationPlaceData;
import com.slocpi.ium.data.ExaminerData;
import com.slocpi.ium.data.LaboratoryData;
import com.slocpi.ium.data.RankData;
import com.slocpi.ium.data.RequirementFormData;
import com.slocpi.ium.data.SpecializationData;
import com.slocpi.ium.data.dao.DocumentRequirementsDAO;
import com.slocpi.ium.data.dao.RequirementFormDAO;
import com.slocpi.ium.data.dao.StatusCHDao;
import com.slocpi.ium.data.util.DataSourceProxy;
import com.slocpi.ium.underwriter.Reference;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.IUMConstants;
import com.slocpi.ium.util.NameValuePair;
import com.slocpi.ium.util.SortHelper;
import com.slocpi.ium.util.UserManager;



/**
 * Renders information as a listbox.
 * @author: Ma. Theresa Vicencio
 * @version: 1.0
 * 
 * -Replace all StringBuffer to StringBuilder,
 * 	this is to ensure to speed up page loading
 * 	when taglib is called
 * -Replace all String concatenation into StringBuilder append,
 * 	this is lessen load in Java and is faster in generating html element
 * @author: Cshellz Aries Sayan - 05192014
 * @version: 1.1 
 */
public class ListTag extends TagSupport {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ListTag.class);
	private String listBoxName = "";
	private String selectedItem = "";
	private String onChange = "";
	private String onFocus = "";
	private String type = "";
	private String className = "";
	private String styleName = "";
	private String disabled = "";
	private String filter = "";
	private String lob = "";
	private String objectType = "";
	private String size = "";


	/**
	 * Instantiates the object
	 */
	public ListTag() {
		super();
	}


	/**
	 * Sets the name that the listbox will be referred to in the JSP page.
	 * @param name listbox name
	 */
	public void setListBoxName(String name) {
		this.listBoxName = name;
	}


	/**
	 * Sets the default item that will appear selected in the list box.
	 * @param code item code to be selected
	 */
	public void setSelectedItem(String code) {
		this.selectedItem = code;
	}


	/**
	 * Sets the behavior of the list box or container page upon selection from the list.
	 * @param change a javascript call that dictates the behavior of the listbox upon selection
	 */
	public void setOnChange(String change) {
		this.onChange = change;
	}


	/**
	 * Sets the behavior of the list box or container page upon focus from the list.
	 * @param change a javascript call that dictates the behavior of the listbox upon focus
	 */
	public void setOnFocus(String focus) {
		this.onFocus = focus;
	}


	/**
	 * Sets the style to be used from the CSS
	 * @param style style from the CSS
	 */
	public void setClassName(String style) {
		this.className = style;
	}


	/**
	 * Sets the style to be used not from the CSS
	 * @param style style not from the CSS
	 */
	 public void setStyleName(String style) {
		this.styleName = style;
	 }


	/**
	 * Sets the disabled behavior of the list
	 * @param disable disabled value
	 */
	public void setDisabled(String disable) {
		this.disabled = disable;
	}


	/**
	 * Sets the line of business
	 * @param lob lob value
	 */
	public void setLob(String lob) {
		this.lob = lob;
	}


	/**
	 * Sets the object type (AR,MR,NM)
	 * @param lob lob value
	 */
	public void setObjectType(String objectType) {
		this.objectType = objectType;
	}


	/**
	 * Sets the size
	 * @param string
	 */
	public void setSize(String size) {
		this.size = size;
	}


	/**
	 * Indicates the kind of list that will be rendered
	 * @param type specifies the type of information to be retrieved
	 */
	public void setType(String type) {
		this.type = type;
	}


	/**
	 * Sets the filter
	 * @param filter
	 */
	public void setFilter(String filter) {
		this.filter = filter;
	}


	/**
	 * Implements the TagSupport method.
	 * @return int
	 */
	public int doStartTag() {
		return SKIP_BODY;
	}


	/**
	 * Implements the TagSupport method that retrieves the list of data and render in a listbox.
	 * @return int
	 */
	public int doEndTag() throws JspTagException {
		
		LOGGER.info("doEndTag start");
		try {
			if (type.equals(IUMConstants.LIST_LOB)) {
				listLOB();
			}
			else if (type.equals(IUMConstants.LIST_BRANCHES)) {
				listBranches();
			}
			else if (type.equals(IUMConstants.LIST_REQUEST_STATUS)) {
				listRequestStatus();
			}
			else if (type.equals(IUMConstants.LIST_REQUIREMENT_STATUS)) {
				listRequirementStatus();
			}
			else if (type.equals(IUMConstants.LIST_REQUIREMENT_CODE)) {
				listRequirementCode();
			}
			else if (type.equals(IUMConstants.LIST_MEDICAL_RECORD_STATUS)) {
				listMedicalRecordStatus();
			}
			else if (type.equals(IUMConstants.LIST_CLIENT_TYPE)) {
				listClientTypes();
			}
			else if (type.equals(IUMConstants.LIST_MIB_IMPAIRMENT_CODE)) {
				listMIBImpairmentCode();
			}
			else if (type.equals(IUMConstants.LIST_MIB_NUMBER_CODE)) {
				listMIBNumberCode();
			}
			else if (type.equals(IUMConstants.LIST_MIB_LETTER_CODE)) {
				listMIBLetterCode();
			}
			else if (type.equals(IUMConstants.LIST_MIB_ACTION_CODE)) {
				listMIBActionCode();
			}
			else if (type.equals(IUMConstants.LIST_AGENT)) {
				listAgent();
			}
			else if (type.equals(IUMConstants.LIST_BRANCH_AGENTS)) {
				listBranchAgents();
			}
			else if (type.equals(IUMConstants.LIST_LOCATION)) {
				listLocation();
			}
			else if (type.equals(IUMConstants.LIST_ASSIGN_TO)) {
				listAssignTo();
			}
			else if (type.equals(IUMConstants.LIST_DOCUMENT_TYPE)) {
				listDocumentType();
			}
			else if (type.equals(IUMConstants.LIST_DEPARTMENT)) {
				listDepartments();
			}
			else if (type.equals(IUMConstants.LIST_SECTION)) {
				listSections();
			}
			else if (type.equals(IUMConstants.LIST_USERS)) {
				listUsers();
			}
			else if (type.equals(IUMConstants.LIST_USER_TYPE)) {
				listUserType();
			}
			else if (type.equals(IUMConstants.LIST_ACCESS_LEVEL)) {
				listAccessLevel();
			}
			else if (type.equals(IUMConstants.LIST_EXAMINERS)) {
				listExaminers();
			}
			else if (type.equals(IUMConstants.LIST_LABORATORIES)) {
				listLaboratories();
			}
			else if (type.equals(IUMConstants.LIST_SCHEDULE)) {
				listSchedule();
			}
			else if (type.equals(IUMConstants.LIST_IMPAIRMENT_CONFIRMATION)) {
				listImpairmentConfirmation();
			}
			else if (type.equals(IUMConstants.LIST_TEMPLATES)){
				listTemplate();
			}
			else if (type.equals(IUMConstants.LIST_DOCTORS)){
				listDoctors();
			}
			else if (type.equals(IUMConstants.LIST_PC_STATUS)){
				listProcessConfigStatus(false);
			}
			else if (type.equals(IUMConstants.LIST_OBJ_STATUS)){
				listObjStatus();
			}
			else if (type.equals(IUMConstants.LIST_PC_STATUS_NULL)){
				listProcessConfigStatus(true);
			}
			else if (type.equals(IUMConstants.LIST_NOTIFICATION_TEMPLATES)){
				listNotificationTemplates();
			}
			else if (type.equals(IUMConstants.LIST_ROLES)){
				listRoles();
			}
			else if (type.equals(IUMConstants.LIST_REFERENCE_CODES)){
				listReferenceCodes();
			}
			else if (type.equals(IUMConstants.LIST_TRANSACTION_TYPE)){
				listTransactionTypes();
			}
			else if (type.equals(IUMConstants.LIST_TRANSACTION_RECORD)){
				listTables();
			}
			else if (type.equals(IUMConstants.LIST_TEST_PROFILES)){
				listTestProfile();
			}
			else if (type.equals(IUMConstants.LIST_LABORATORY_TEST_PROFILES)){
				listLaboratoryTestProfile();
			}
			else if (type.equals(IUMConstants.LIST_STATUS_TYPE)){
				listStatusType();
			}
			else if (type.equals(IUMConstants.LIST_FORM_TEMPLATE_FORMAT)){
				listFormTemplateFormat();
			}
			else if (type.equals(IUMConstants.LIST_LOB_DOCUMENT_TYPE)) {
				listLobDocumentType();
			}
			else if (type.equals(IUMConstants.LIST_LOB_REQUIREMENTS)) {
				listLobRequirements();
			}
			else if (type.equals(IUMConstants.LIST_HOLIDAY_YEAR)) {
				listHolidayYear();
			}
			else if (type.equals(IUMConstants.LIST_REQUIREMENT_FORMS)){
				listRequirementForms();
			}
			else if (type.equals(IUMConstants.LIST_AUTO_ASSIGN_CRITERIA)){
				listAutoAssignmentCriteria();
			}
			else if (type.equals(IUMConstants.LIST_AUTO_ASSIGN_CRITERIA_LOV)){
				listAutoAssignmentCriteriaLOV();
			}
			else if (type.equals(IUMConstants.LIST_RECORD_TYPES)){
				listRecordTypes();
			}
			else if (type.equals(IUMConstants.LIST_RANKS)){
				listRanks();
			}
			else if (type.equals(IUMConstants.LIST_SPECIALIZATIONS)){
				listSpecializations();
			}
			else if (type.equals(IUMConstants.LIST_EXAMINATION_PLACES)){
				listExaminationPlaces();
			}
			else if (type.equals(IUMConstants.LIST_PURGE_CRITERIA)) {
				listPurgeCriteria();
			}
			else if (type.equals(IUMConstants.LIST_CHANGE_MED_RECORD_STATUS)) {
				listChangeMedicalRecordStatus();
			}
			else if (type.equals(IUMConstants.LIST_FILTER_MED_RECORD_STATUS)) {
				listFilterMedicalRecordStatus();
			}
			else if (type.equals(IUMConstants.LIST_INTERFACE_TYPES)){
				listInterfaceTypes();
			}
			else {
				throw new Exception("Unidentified list type or none specified");
			}
		} catch (Exception e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new JspTagException (e.getMessage());
		}
		LOGGER.info("doEndTag end");
		return EVAL_PAGE;
	}


	/**
	 *
	 */
	private void listChangeMedicalRecordStatus() throws Exception {
		
		LOGGER.info("listChangeMedicalRecordStatus start 1");
				StringBuilder result = new StringBuilder();
				CodeHelper codeHelper = new CodeHelper();
				Collection list = codeHelper.getCodeValues("medicalRecordStatus", filter);

				if (list != null) {
					Iterator iterator = list.iterator();

					//parse list
					while (iterator.hasNext()) {
						NameValuePair nameValuePair = (NameValuePair)iterator.next();
						String code = nameValuePair.getValue();
						String desc = nameValuePair.getName();

						if (selectedItem != null && selectedItem.equals(code)) {
							result = result.append("<option value= '").append(code).append("' selected>").append(desc).append("</option>" );
						} else if (!code.equals(Long.toString(IUMConstants.STATUS_REQUESTED))) {
							result = result.append("<option value= '").append(code).append("'>").append(desc).append("</option>");
						}
					}
				}
				result = result.insert(0,"<option value=''>Select Medical Record Status</option>");
				if (disabled.equals("") || disabled.equals("false")) {
					result = result.insert(0,new StringBuilder("<select class='").append(className).append("' name= '").append(listBoxName).append("' style= '").append(styleName).append("' onChange= '").append(onChange).append("' onFocus= '").append(onFocus).append("'>"));
				}
				else {
					result = result.insert(0,new StringBuilder("<select class='").append(className).append("' name= '").append(listBoxName).append("' style= '").append(styleName).append("' disabled= '").append(disabled).append("' onChange= '").append(onChange).append("' onFocus= '").append(onFocus).append("'>"));
				}
				result = result.append( "</select>");
				pageContext.getOut().println(result);
				
				LOGGER.info("listChangeMedicalRecordStatus end 1");
	}

	private void listFilterMedicalRecordStatus() throws Exception {
		
				LOGGER.info("listFilterMedicalRecordStatus start 2");
				StringBuilder result = new StringBuilder();
				CodeHelper codeHelper = new CodeHelper();
				Collection list = codeHelper.getCodeValues("medicalRecordStatus", filter);

				if (list != null) {
					Iterator iterator = list.iterator();

					//parse list
					while (iterator.hasNext()) {
						NameValuePair nameValuePair = (NameValuePair)iterator.next();
						String code = nameValuePair.getValue();
						String desc = nameValuePair.getName();

						if (selectedItem != null && selectedItem.equals(code)) {
							result = result.append("<option value= '").append(code).append("' selected>").append(desc).append("</option>" );
						} else {
							result = result.append("<option value= '").append(code).append("'>").append(desc).append("</option>");
						}
					}
				}
				result = result.insert(0,"<option value=''>Select All</option>");
				if (disabled.equals("") || disabled.equals("false")) {
					result = result.insert(0,new StringBuilder("<select class='").append(className).append("' name= '").append(listBoxName).append("' style= '").append(styleName).append("' onChange= '").append(onChange).append("' onFocus= '").append(onFocus).append("'>"));
				}
				else {
					result = result.insert(0,new StringBuilder("<select class='").append(className).append("' name= '").append(listBoxName).append("' style= '").append(styleName).append("' disabled= '").append(disabled).append("' onChange= '").append(onChange).append("' onFocus= '").append(onFocus).append("'>"));
				}
				result = result.append( "</select>");
				pageContext.getOut().println(result);
				
				LOGGER.info("listFilterMedicalRecordStatus end 2");
	}

	private void listMedicalRecordStatus() throws Exception {
		
		LOGGER.info("listMedicalRecordStatus start");
			StringBuilder result = new StringBuilder();
			CodeHelper codeHelper = new CodeHelper();
			Collection list = codeHelper.getCodeValues("medicalRecordStatus", filter);

			if (list != null) {
				Iterator iterator = list.iterator();

				//parse list
				while (iterator.hasNext()) {
					NameValuePair nameValuePair = (NameValuePair)iterator.next();
					String code = nameValuePair.getValue();
					String desc = nameValuePair.getName();

					if (selectedItem != null && selectedItem.equals(code)) {
						result = result.append("<option value= '").append(code).append("' selected>").append(desc).append("</option>" );
					} else {
						result = result.append("<option value= '").append(code).append("'>").append(desc).append("</option>");
					}
				}
			}
			result = result.insert(0,"<option value=''>Select Medical Record Status</option>");
			if (disabled.equals("") || disabled.equals("false")) {
				result = result.insert(0,new StringBuilder("<select class='").append(className).append("' name= '").append(listBoxName).append("' style= '").append(styleName).append("' onChange= '").append(onChange).append("' onFocus= '").append(onFocus).append("'>"));
			}
			else {
				result = result.insert(0,new StringBuilder("<select class='").append(className).append("' name= '").append(listBoxName).append("' style= '").append(styleName).append("' disabled= '").append(disabled).append("' onChange= '").append(onChange).append("' onFocus= '").append(onFocus).append("'>"));
			}
			result = result.append( "</select>");
			pageContext.getOut().println(result);
			LOGGER.info("listMedicalRecordStatus end");
		}//listMedicalRecordStatus

	private void listLobRequirements() throws SQLException, IOException {
		
		LOGGER.info("listLobRequirements start");
		StringBuilder result = new StringBuilder();
		Connection conn = null;
		Collection list = null;
		try{
			conn = new DataSourceProxy().getConnection();
			DocumentRequirementsDAO docsReqDAO = new DocumentRequirementsDAO(conn);
			list = docsReqDAO.getCodeValuesReqs(lob);
		} finally{
			closeConnection(conn);
		}
		if (list != null) {
			Iterator iterator = list.iterator();

			//parse list
			while (iterator.hasNext()) {
				NameValuePair nameValuePair = (NameValuePair)iterator.next();
				String code = nameValuePair.getValue();
				String desc = nameValuePair.getName();

				if (selectedItem != null && selectedItem.equals(code)) {
					result = result.append("<option value= '").append(code).append("' selected>").append(desc).append("</option>" );
				}
				else {
					result = result.append("<option value= '").append(code).append("'>").append(code).append(" - "+desc).append("</option>");
				}
			}
		}
		result = result.insert(0,"<option value=''>Select Requirement</option>");
		if (disabled.equals("") || disabled.equals("false")) {
			result = result.insert(0,new StringBuilder("<select class='").append(className).append("' name= '").append(listBoxName).append("' style= '").append(styleName).append("' onChange= '").append(onChange).append("' onFocus= '").append(onFocus).append("'>"));
		}
		else {
			result = result.insert(0,new StringBuilder("<select class='").append(className).append("' name= '").append(listBoxName).append("' style= '").append(styleName).append("' disabled= '").append(disabled).append("' onChange= '").append(onChange).append("' onFocus= '").append(onFocus).append("'>"));
		}
		result = result.append( "</select>");
		pageContext.getOut().println(result);
		LOGGER.info("listLobRequirements end");
	}//listLobRequirements


	private void listLobDocumentType() throws Exception {
		
		LOGGER.info("listLobDocumentType start");
		StringBuilder result = new StringBuilder();
		Connection conn = null;
		Collection list =null;
		try{
			conn = new DataSourceProxy().getConnection();
			DocumentRequirementsDAO docsReqDAO = new DocumentRequirementsDAO(conn);
			list = docsReqDAO.getCodeValuesDocs(lob);
		} finally{
			closeConnection(conn);
		}

		if (list != null) {
			Iterator iterator = list.iterator();

			//parse list
			while (iterator.hasNext()) {
				NameValuePair nameValuePair = (NameValuePair)iterator.next();
				String code = nameValuePair.getValue();
				String desc = nameValuePair.getName();

				if (selectedItem != null && selectedItem.equals(code)) {
					result = result.append("<option value= '").append(code).append("' selected>").append(desc).append("</option>" );
				}
				else {
					result = result.append("<option value= '").append(code).append("'>").append(desc).append("</option>");
				}
			}
		}
		result = result.insert(0,"<option value=''>Select Document</option>");
		if (disabled.equals("") || disabled.equals("false")) {
			result = result.insert(0,new StringBuilder("<select class='").append(className).append("' name= '").append(listBoxName).append("' style= '").append(styleName).append("' onChange= '").append(onChange).append("' onFocus= '").append(onFocus).append("'>"));
		}
		else {
			result = result.insert(0,new StringBuilder("<select class='").append(className).append("' name= '").append(listBoxName).append("' style= '").append(styleName).append("' disabled= '").append(disabled).append("' onChange= '").append(onChange).append("' onFocus= '").append(onFocus).append("'>"));
		}
		result = result.append( "</select>");
		pageContext.getOut().println(result);
		LOGGER.info("listLobDocumentType end");
	}//listDocumentType


	private void listRoles() throws Exception {
		
		LOGGER.info("listRoles start");
		StringBuilder result = new StringBuilder();
		CodeHelper codeHelper = new CodeHelper();
		Collection roleList = codeHelper.getCodeValues("roles");

		if (roleList != null){
			Iterator iterator = roleList.iterator();

			//parse list
			while (iterator.hasNext()){
				NameValuePair nameValuePair = (NameValuePair)iterator.next();
				String code = nameValuePair.getValue();
				String desc = nameValuePair.getName();
				if (selectedItem != null && selectedItem.equals(String.valueOf(desc))){
					result = result.append("<option value= '").append(code).append("' selected>").append(desc).append("</option>");
				}
				else{
					result = result.append("<option value= '").append(code).append("'>").append(desc).append("</option>");
				}
			}
		}
		result = result.insert(0,"<option value=''>Select Role</option>");
		if (disabled.equals("") || disabled.equals("false")) {
			result = result.insert(0,new StringBuilder("<select class='").append(className).append("' name= '").append(listBoxName).append("' style= '").append(styleName).append("' onChange= '").append(onChange).append("' onFocus= '").append(onFocus).append("'>"));
		}
		else {
			result = result.insert(0,new StringBuilder("<select class='").append(className).append("' name= '").append(listBoxName).append("' style= '").append(styleName).append("' disabled= '").append(disabled).append("' onChange= '").append(onChange).append("' onFocus= '").append(onFocus).append("'>"));
		}
		result = result.append( "</select>");
		pageContext.getOut().println(result);
		LOGGER.info("listRoles end");
	}//listRoles



	private void listNotificationTemplates() throws Exception {
		
		LOGGER.info("listNotificationTemplates start");
		StringBuilder result = new StringBuilder();
		CodeHelper codeHelper = new CodeHelper();
		Collection list = codeHelper.getCodeValues("notificationTemplates");

		if (list != null) {
			Iterator iterator = list.iterator();

			//parse list
			while (iterator.hasNext()) {
				NameValuePair nameValuePair = (NameValuePair)iterator.next();
				String code = nameValuePair.getValue();
				String desc = nameValuePair.getName();

				if (selectedItem != null && selectedItem.equals(code)) {
					result = result.append("<option value= '").append(code).append("' selected>").append(desc).append("</option>" );
				}
				else {
					result = result.append("<option value= '").append(code).append("'>").append(desc).append("</option>");
				}
			}
		}
		result = result.insert(0,"<option value=''>Select Notification Template</option>");
		if (disabled.equals("") || disabled.equals("false")) {
			result = result.insert(0,new StringBuilder("<select class='").append(className).append("' name= '").append(listBoxName).append("' style= '").append(styleName).append("' onChange= '").append(onChange).append("' onFocus= '").append(onFocus).append("'>"));
		}
		else {
			result = result.insert(0,new StringBuilder("<select class='").append(className).append("' name= '").append(listBoxName).append("' style= '").append(styleName).append("' disabled= '").append(disabled).append("' onChange= '").append(onChange).append("' onFocus= '").append(onFocus).append("'>"));
		}
		result = result.append( "</select>");
		pageContext.getOut().println(result);
		LOGGER.info("listNotificationTemplates end");
	}//listNotificationTemplates


	private void listObjStatus() throws Exception {
		
		LOGGER.info("listObjStatus start");
		
		StringBuilder result = new StringBuilder();

		if (objectType != null && !objectType.equals("")) {
			Connection conn = null;
			Collection list = null;
			
			StatusCHDao statusDAO = new StatusCHDao();
			list = statusDAO.getCodeValue1(objectType,lob);
			
			Iterator it = list.iterator();
			while (it.hasNext()) {
				NameValuePair nameValuePair = (NameValuePair)it.next();
				String code = nameValuePair.getValue();
				String desc = nameValuePair.getName();

				if (selectedItem != null && selectedItem.equals(code)) {
					result = result.append("<option value= '").append(code).append("' selected>").append(desc).append("</option>" );
				}
				else {
					result = result.append("<option value= '").append(code).append("'>").append(code).append(" - ").append(desc).append("</option>");
				}
			}
		}
		result.insert(0,"<option value=''>Select a Status</option>");
		if (disabled.equals("") || disabled.equals("false")) {
			result = result.insert(0,new StringBuilder("<select class='").append(className).append("' name= '").append(listBoxName).append("' style= '").append(styleName).append("' onChange= '").append(onChange).append("' onFocus= '").append(onFocus).append("'>"));
		}
		else {
			result = result.insert(0,new StringBuilder("<select class='").append(className).append("' name= '").append(listBoxName).append("' style= '").append(styleName).append("' disabled= '").append(disabled).append("' onChange= '").append(onChange).append("' onFocus= '").append(onFocus).append("'>"));
		}
		result.append( "</select>");
		pageContext.getOut().println(result);
		LOGGER.info("listObjStatus end");
	}// listObjStatus


	private void listProcessConfigStatus(boolean withNull) throws Exception {
		
		LOGGER.info("listObjStatus start");
		StringBuilder result = new StringBuilder();

		if ((lob != null && !lob.equals("")) && (objectType != null && !objectType.equals(""))) {

			Connection conn = null;
			Collection list = null;
			
			StatusCHDao statusDAO = new StatusCHDao();
			list = statusDAO.getCodeValue(lob,objectType);
			
			Iterator it = list.iterator();
			while (it.hasNext()) {
				NameValuePair nameValuePair = (NameValuePair)it.next();
				String code = nameValuePair.getValue();
				String desc = nameValuePair.getName();

				if (selectedItem != null && selectedItem.equals(code)) {
					result = result.append("<option value= '").append(code).append("' selected>").append(desc).append("</option>" );
				}
				else {
					result = result.append("<option value= '").append(code).append("'>").append(desc).append("</option>");
				}
			}
		}
		if (size == null || size.equals("")) {
			result.insert(0,"<option value=''>Select a Status</option>");
		}
		if (disabled.equals("") || disabled.equals("false")) {
			result = result.insert(0,new StringBuilder("<select class='").append(className).append("' name= '").append(listBoxName).append("' style= '").append(styleName).append("' onChange= '").append(onChange).append("' onFocus= '").append(onFocus).append("'>"));
		}
		else {
			result = result.insert(0,new StringBuilder("<select class='").append(className).append("' name= '").append(listBoxName).append("' style= '").append(styleName).append("' disabled= '").append(disabled).append("' onChange= '").append(onChange).append("' onFocus= '").append(onFocus).append("'>"));
		}
		result.append( "</select>");
		pageContext.getOut().println(result);
		
		LOGGER.info("listObjStatus end");
	}//listProcessConfigStatus


	private void listTemplate() throws Exception {
		
		LOGGER.info("listTemplate start");
		StringBuilder result = new StringBuilder();
		UserManager u = new UserManager();
		Collection templateList = u.getAccessTemplates();

		if (templateList != null){
			Iterator iterator = templateList.iterator();

			//parse list
			while (iterator.hasNext()){
				AccessTemplateData accessTemplate = (AccessTemplateData) iterator.next();
				long code = accessTemplate.getTemplateID();
				String desc = accessTemplate.getTemplateDesc();

				if(selectedItem != null && selectedItem.equals(String.valueOf(code))){
					result = result.append("<option value= '").append(code).append("' selected>").append(desc).append("</option>");
				}
				else{
					result = result.append("<option value= '").append(code).append("'>").append(desc).append("</option>");
				}
			}
		}
		result = result.insert(0,"<option value=''>Select Template</option>");
		if (disabled.equals("") || disabled.equals("false")) {
			result = result.insert(0,new StringBuilder("<select class='").append(className).append("' name= '").append(listBoxName).append("' style= '").append(styleName).append("' onChange= '").append(onChange).append("' onFocus= '").append(onFocus).append("'>"));
		}
		else {
			result = result.insert(0,new StringBuilder("<select class='").append(className).append("' name= '").append(listBoxName).append("' style= '").append(styleName).append("' disabled= '").append(disabled).append("' onChange= '").append(onChange).append("' onFocus= '").append(onFocus).append("'>"));
		}
		result = result.append( "</select>");
		pageContext.getOut().println(result);
		LOGGER.info("listTemplate end");
	}//listTemplate


	private void listLOB() throws Exception {
		
		LOGGER.info("listLOB start");
		StringBuilder result = new StringBuilder();
		CodeHelper codeHelper = new CodeHelper();
		Collection list = codeHelper.getCodeValues("lob");

		if (list != null) {
			Iterator iterator = list.iterator();

			//parse list
			while (iterator.hasNext()) {
				NameValuePair nameValuePair = (NameValuePair)iterator.next();
				String code = nameValuePair.getValue();
				String desc = nameValuePair.getName();

				if (selectedItem != null && selectedItem.equals(code)) {
					result = result.append("<option value= '").append(code).append("' selected>").append(desc).append("</option>" );
				}
				else {
					result = result.append("<option value= '").append(code).append("'>").append(desc).append("</option>");
				}
			}
		}
		result = result.insert(0,"<option value=''>Select LOB</option>");
		if (disabled.equals("") || disabled.equals("false")) {
			result = result.insert(0,new StringBuilder("<select class='").append(className).append("' name= '").append(listBoxName).append("' style= '").append(styleName).append("' onChange= '").append(onChange).append("' onFocus= '").append(onFocus).append("'>"));
		}
		else {
			result = result.insert(0,new StringBuilder("<select class='").append(className).append("' name= '").append(listBoxName).append("' style= '").append(styleName).append("' disabled= '").append(disabled).append("' onChange= '").append(onChange).append("' onFocus= '").append(onFocus).append("'>"));
		}
		result = result.append( "</select>");
		pageContext.getOut().println(result);
		LOGGER.info("listLOB end");
	}//listLOB


	private void listRequestStatus(String _filter) throws Exception {
		filter = _filter;
		listRequestStatus();
	}//listRequestStatus


	private void listRequirementStatus(String _filter) throws Exception {
		filter = _filter;
		listRequirementStatus();
	}//listRequirementStatus


	private void listMedicalRecordStatus(String _filter) throws Exception {
		filter = _filter;
		listMedicalRecordStatus();
	}//listMedicalRecordStatus


	private void listRequestStatus() throws Exception {
		
		LOGGER.info("listRequestStatus start");
		
		StringBuilder result = new StringBuilder();
		CodeHelper codeHelper = new CodeHelper();
		Collection list = codeHelper.getCodeValues("requestStatus", filter);

		if (list != null) {
			Iterator iterator = list.iterator();

			//parse list
			while (iterator.hasNext()) {
				NameValuePair nameValuePair = (NameValuePair)iterator.next();
				String code = nameValuePair.getValue();
				String desc = nameValuePair.getName();

				if (selectedItem != null && selectedItem.equals(code)) {
					result = result.append("<option value= '").append(code).append("' selected>").append(desc).append("</option>" );
				}
				else {
					result = result.append("<option value= '").append(code).append("'>").append(desc).append("</option>");
				}
			}
		}
		result = result.insert(0,"<option value=''>Select Request Status</option>");
		if (disabled.equals("") || disabled.equals("false")) {
			result = result.insert(0,new StringBuilder("<select class='").append(className).append("' name= '").append(listBoxName).append("' style= '").append(styleName).append("' onChange= '").append(onChange).append("' onFocus= '").append(onFocus).append("'>"));
		}
		else {
			result = result.insert(0,new StringBuilder("<select class='").append(className).append("' name= '").append(listBoxName).append("' style= '").append(styleName).append("' disabled= '").append(disabled).append("' onChange= '").append(onChange).append("' onFocus= '").append(onFocus).append("'>"));
		}
		result = result.append( "</select>");
		pageContext.getOut().println(result);
		LOGGER.info("listRequestStatus end");
	}//listRequestStatus


	private void listRequirementStatus() throws Exception {
		
		LOGGER.info("listRequirementStatus start");
		StringBuilder result = new StringBuilder();
		CodeHelper codeHelper = new CodeHelper();
		Collection list = codeHelper.getCodeValues("requirementStatus", filter);

		// Create options tags
		if (list != null) {

			Iterator iterator = list.iterator();
			while (iterator.hasNext()) {
				
				NameValuePair nameValuePair = (NameValuePair)iterator.next();
				String code = nameValuePair.getValue();
				String desc = nameValuePair.getName();

				if (selectedItem != null && selectedItem.equals(code)) {
					result = result.append("<option value= '").append(code).append("' selected>").append(desc).append("</option>" );
				} else {
					result = result.append("<option value= '").append(code).append("'>").append(desc).append("</option>");
				}
			}
		}
		
		// Set Select tags
		result = result.insert(0,"<option value=''>Select Requirement Status</option>");
		if (disabled.equals("") || disabled.equals("false")) {
			result = result.insert(0, new StringBuilder("<select class= '").append(className).append("' name= '").append(listBoxName).append("' style= '").append(styleName).append("' onChange= '").append(onChange).append("' onFocus= '").append(onFocus).append("'>"));
		} else {
			result = result.insert(0, new StringBuilder("<select class= '").append(className).append("' name= '").append(listBoxName).append("' style= '").append(styleName).append("' disabled= '").append(disabled).append("' onChange= '").append(onChange).append("' onFocus= '").append(onFocus).append("'>"));
		}
		result = result.append( "</select>");
		
		// Display output
		pageContext.getOut().println(result);
		
		LOGGER.info("listRequirementStatus end");
	}//listRequirementStatus




	private void listBranches() throws Exception {
		
		LOGGER.info("listBranches start");
		StringBuilder result = new StringBuilder();
		CodeHelper codeHelper = new CodeHelper();
		Collection list = codeHelper.getCodeValues("branch");

		if (list != null) {
			Iterator iterator = list.iterator();

			//parse list
			while (iterator.hasNext()) {
				NameValuePair nameValuePair = (NameValuePair)iterator.next();
				String code = nameValuePair.getValue();
				String desc = nameValuePair.getName();

				if (selectedItem != null && selectedItem.equals(code)) {
					result = result.append("<option value= '").append(code).append("' selected>").append(desc).append("</option>" );
				}
				else {
					result = result.append("<option value= '").append(code).append("'>").append(code).append(" - ").append(desc).append("</option>");
				}
			}
		}
		result = result.insert(0,"<option value=''>Select Branch</option>");
		if (disabled.equals("") || disabled.equals("false")) {
			result = result.insert(0,new StringBuilder("<select class='").append(className).append("' name= '").append(listBoxName).append("' style= '").append(styleName).append("' onChange= '").append(onChange).append("' onFocus= '").append(onFocus).append("'>"));
		}
		else {
			result = result.insert(0,new StringBuilder("<select class='").append(className).append("' name= '").append(listBoxName).append("' style= '").append(styleName).append("' disabled= '").append(disabled).append("' onChange= '").append(onChange).append("' onFocus= '").append(onFocus).append("'>"));
		}
		result = result.append( "</select>");
		pageContext.getOut().println(result);
		LOGGER.info("listBranches end");
	}//listBranches()


	private void listClientTypes() throws Exception {
		
		LOGGER.info("listClientTypes start");
		StringBuilder result = new StringBuilder();
		CodeHelper codeHelper = new CodeHelper();
		Collection list = codeHelper.getCodeValues("clientType");

		if (list != null) {
			Iterator iterator = list.iterator();

			//parse list
			while (iterator.hasNext()) {
				if (filter.equals(IUMConstants.LOB_INDIVIDUAL_LIFE)) {
					NameValuePair nameValuePair = (NameValuePair)iterator.next();
					String code = nameValuePair.getValue();
					String desc = nameValuePair.getName();

					if (code.equals(IUMConstants.CLIENT_TYPE_INSURED) || code.equals(IUMConstants.CLIENT_TYPE_OWNER)) {
						if (selectedItem != null && selectedItem.equals(code)) {
							result = result.append("<option value= '").append(code).append("' selected>").append(desc).append("</option>" );
						}
						else {
							result = result.append("<option value= '").append(code).append("'>").append(desc).append("</option>");
						}
					}
				}
				else {
					NameValuePair nameValuePair = (NameValuePair)iterator.next();
					String code = nameValuePair.getValue();
					String desc = nameValuePair.getName();

					if (selectedItem != null && selectedItem.equals(code)) {
						result = result.append("<option value= '").append(code).append("' selected>").append(desc).append("</option>" );
					}
					else {
						result = result.append("<option value= '").append(code).append("'>").append(desc).append("</option>");
					}
				}
			}
		}
		result = result.insert(0,"<option value=''>Select Client Type</option>");
		if (disabled.equals("") || disabled.equals("false")) {
			result = result.insert(0,new StringBuilder("<select class='").append(className).append("' name= '").append(listBoxName).append("' style= '").append(styleName).append("' onChange= '").append(onChange).append("' onFocus= '").append(onFocus).append("'>"));
		}
		else {
			result = result.insert(0,new StringBuilder("<select class='").append(className).append("' name= '").append(listBoxName).append("' style= '").append(styleName).append("' disabled= '").append(disabled).append("' onChange= '").append(onChange).append("' onFocus= '").append(onFocus).append("'>"));
		}
		result = result.append( "</select>");
		pageContext.getOut().println(result);
		LOGGER.info("listClientTypes end");
	}//listClientTypes


	private void listMIBImpairmentCode() throws Exception {
		
		LOGGER.info("listMIBImpairmentCode start");
		StringBuilder result = new StringBuilder();
		CodeHelper codeHelper = new CodeHelper();
		Collection list = codeHelper.getCodeValues("mibImpairment");

		// Set options
		if (list != null) {
			
			Iterator iterator = list.iterator();
			while (iterator.hasNext()) {
				
				NameValuePair nameValuePair = (NameValuePair)iterator.next();
				String code = nameValuePair.getValue();
				String desc = nameValuePair.getName();

				if (selectedItem != null && selectedItem.equals(code)) {
					result = result.append("<option value= '").append(code).append("' selected>").append(code).append(" - ").append(desc).append("</option>" );
				}
				else {
					result = result.append("<option value= '").append(code).append("'>").append(code).append(" - ").append(desc).append("</option>");
				}
			}
		}
		
		// Set select
		result = result.insert(0,"<option value=''>Select Description</option>");
		if (disabled.equals("") || disabled.equals("false")) {
			result = result.insert(0,new StringBuilder("<select class='").append(className).append("' name= '").append(listBoxName).append("' style= '").append(styleName).append("' onChange= '").append(onChange).append("' onFocus= '").append(onFocus).append("'>"));
		} else {
			result = result.insert(0,new StringBuilder("<select class='").append(className).append("' name= '").append(listBoxName).append("' style= '").append(styleName).append("' disabled= '").append(disabled).append("' onChange= '").append(onChange).append("' onFocus= '").append(onFocus).append("'>"));
		}
		
		result = result.append( "</select>");
		// display
		pageContext.getOut().println(result);
		
		LOGGER.info("listMIBImpairmentCode end");
	}//listMIBImpairmentCode


	private void listMIBActionCode() throws Exception {
		
		LOGGER.info("listMIBActionCode start");
		StringBuilder result = new StringBuilder();
		CodeHelper codeHelper = new CodeHelper();
		Collection list = codeHelper.getCodeValues("mibAction");

		// Set options
		if (list != null) {
			
			Iterator iterator = list.iterator();
			while (iterator.hasNext()) {
				NameValuePair nameValuePair = (NameValuePair)iterator.next();
				String code = nameValuePair.getValue();
				String desc = nameValuePair.getName();
				
				if (selectedItem != null && selectedItem.equals(code)) {
					result = result.append("<option value= '").append(code).append("' selected>").append(code).append(" - ").append(desc).append("</option>" );
				} else {
					result = result.append("<option value= '").append(code).append("'>").append(code).append(" - ").append(desc).append("</option>");
				}
			}
		}
		
		// Set select
		result = result.insert(0,"<option value=''>Select Action Code</option>");
		if (disabled.equals("") || disabled.equals("false")) {
			result = result.insert(0,new StringBuilder("<select class='").append(className).append("' name= '").append(listBoxName).append("' style= '").append(styleName).append("' onChange= '").append(onChange).append("' onFocus= '").append(onFocus).append("'>"));
		} else {
			result = result.insert(0,new StringBuilder("<select class='").append(className).append("' name= '").append(listBoxName).append("' style= '").append(styleName).append("' disabled= '").append(disabled).append("' onChange= '").append(onChange).append("' onFocus= '").append(onFocus).append("'>"));
		}
		result = result.append( "</select>");
		
		// Display result
		pageContext.getOut().println(result);
		LOGGER.info("listMIBActionCode end");
	}//listMIBActionCode


	private void listMIBLetterCode() throws Exception {
		
		LOGGER.info("listMIBLetterCode start");
		StringBuilder result = new StringBuilder();
		CodeHelper codeHelper = new CodeHelper();
		Collection list = codeHelper.getCodeValues("mibLetter");

		if(list != null) {
			Iterator iterator = list.iterator();

			//parse list
			while (iterator.hasNext()) {
				NameValuePair nameValuePair = (NameValuePair)iterator.next();
				String code = nameValuePair.getValue();
				String desc = nameValuePair.getName();

				if (selectedItem != null && selectedItem.equals(code)) {
					result = result.append("<option value= '").append(code).append("' selected>").append(desc).append("</option>" );
				} else {
					result = result.append("<option value= '").append(code).append("'>").append(code).append(" - ").append(desc).append("</option>");
				}
			}
		}
		result = result.insert(0,"<option value=''>Select Impairment Origin</option>");
		if (disabled.equals("") || disabled.equals("false")) {
			result = result.insert(0,new StringBuilder("<select class='").append(className).append("' name= '").append(listBoxName).append("' style= '").append(styleName).append("' onChange= '").append(onChange).append("' onFocus= '").append(onFocus).append("'>"));
		}
		else {
			result = result.insert(0,new StringBuilder("<select class='").append(className).append("' name= '").append(listBoxName).append("' style= '").append(styleName).append("' disabled= '").append(disabled).append("' onChange= '").append(onChange).append("' onFocus= '").append(onFocus).append("'>"));
		}
		result = result.append( "</select>");
		pageContext.getOut().println(result);
		LOGGER.info("listMIBLetterCode end");
	}//listMIBLetterCode


	private void listMIBNumberCode() throws Exception {
		
		LOGGER.info("listMIBNumberCode start");
		StringBuilder result = new StringBuilder();
		CodeHelper codeHelper = new CodeHelper();
		Collection list = codeHelper.getCodeValues("mibNumber");

		// Set options
		if (list != null) {
			
			Iterator iterator = list.iterator();
			while (iterator.hasNext()) {
				NameValuePair nameValuePair = (NameValuePair)iterator.next();
				String code = nameValuePair.getValue();
				String desc = nameValuePair.getName();

				if (selectedItem != null && selectedItem.equals(code)) {
					result = result.append("<option value= '").append(code).append("' selected>").append(desc).append("</option>" );
				} else {
					result = result.append("<option value= '").append(code).append("'>").append(code).append(" - ").append(desc).append("</option>");
				}
			}
		}
		
		// Set select
		result = result.insert(0,"<option value=''>Select Relationship</option>");
		if (disabled.equals("") || disabled.equals("false")) {
			result = result.insert(0,new StringBuilder("<select class='").append(className).append("' name= '").append(listBoxName).append("' style= '").append(styleName).append("' onChange= '").append(onChange).append("' onFocus= '").append(onFocus).append("'>"));
		} else {
			result = result.insert(0,new StringBuilder("<select class='").append(className).append("' name= '").append(listBoxName).append("' style= '").append(styleName).append("' disabled= '").append(disabled).append("' onChange= '").append(onChange).append("' onFocus= '").append(onFocus).append("'>"));
		}
		result = result.append( "</select>");
		
		// Display
		pageContext.getOut().println(result);
		LOGGER.info("listMIBNumberCode end");
	}//listMIBNumberCode


	private void listAgent() throws Exception {
		listUsers();
	}//listAgent


	private void listLocation() throws Exception {
		listUsers();
	}//listLocation


	private void listAssignTo() throws Exception {
		listUsers();
	}//listAssignTo


	private void listDoctors() throws Exception {
		listUsers();
	}//listDoctors


	private void listDocumentType() throws Exception {
		
		LOGGER.info("listDocumentType start");
		StringBuilder result = new StringBuilder();
		CodeHelper codeHelper = new CodeHelper();
		Collection list = codeHelper.getCodeValues("document");

		if (list != null) {
			Iterator iterator = list.iterator();

			//parse list
			while (iterator.hasNext()) {
				NameValuePair nameValuePair = (NameValuePair)iterator.next();
				String code = nameValuePair.getValue();
				String desc = nameValuePair.getName();

				if (selectedItem != null && selectedItem.equals(code)) {
					result = result.append("<option value= '").append(code).append("' selected>").append(desc).append("</option>" );
				}
				else {
					result = result.append("<option value= '").append(code).append("'>").append(desc).append("</option>");
				}
			}
		}
		result = result.insert(0,"<option value=''>Select Document</option>");
		if (disabled.equals("") || disabled.equals("false")) {
			result = result.insert(0,new StringBuilder("<select class='").append(className).append("' name= '").append(listBoxName).append("' style= '").append(styleName).append("' onChange= '").append(onChange).append("' onFocus= '").append(onFocus).append("'>"));
		}
		else {
			result = result.insert(0,new StringBuilder("<select class='").append(className).append("' name= '").append(listBoxName).append("' style= '").append(styleName).append("' disabled= '").append(disabled).append("' onChange= '").append(onChange).append("' onFocus= '").append(onFocus).append("'>"));
		}
		result = result.append( "</select>");
		pageContext.getOut().println(result);
		LOGGER.info("listDocumentType end");
	}//listDocumentType


	private void listDepartments() throws Exception {
		
		LOGGER.info("listDepartments start");
		StringBuilder result = new StringBuilder();
		CodeHelper codeHelper = new CodeHelper();
		Collection list = codeHelper.getCodeValues("department");

		if(list != null) {
			Iterator iterator = list.iterator();

			//parse list
			while (iterator.hasNext()) {
				NameValuePair nameValuePair = (NameValuePair)iterator.next();
				String code = nameValuePair.getValue();
				String desc = nameValuePair.getName();

				if (selectedItem != null && selectedItem.equals(code)) {
					result = result.append("<option value= '").append(code).append("' selected>").append(desc).append("</option>" );
				}
				else {
					result = result.append("<option value= '").append(code).append("'>").append(desc).append("</option>");
				}
			}
		}
		result = result.insert(0,"<option value=''>Select Department</option>");
		if (disabled.equals("") || disabled.equals("false")) {
			result = result.insert(0,new StringBuilder("<select class='").append(className).append("' name= '").append(listBoxName).append("' style= '").append(styleName).append("' onChange= '").append(onChange).append("' onFocus= '").append(onFocus).append("'>"));
		}
		else {
			result = result.insert(0,new StringBuilder("<select class='").append(className).append("' name= '").append(listBoxName).append("' style= '").append(styleName).append("' disabled= '").append(disabled).append("' onChange= '").append(onChange).append("' onFocus= '").append(onFocus).append("'>"));
		}
		result = result.append( "</select>");
		pageContext.getOut().println(result);
		LOGGER.info("listDepartments end");
	}//listDepartments


	private void listSections() throws Exception {
		
		LOGGER.info("listSections start");
		StringBuilder result = new StringBuilder();

		//get list
		CodeHelper codeHelper = new CodeHelper();
		Collection list = codeHelper.getCodeValues("section");

		if (list != null) {
			Iterator iterator = list.iterator();

			//parse list
			while (iterator.hasNext()) {
				NameValuePair nameValuePair = (NameValuePair)iterator.next();
				String code = nameValuePair.getValue();
				String desc = nameValuePair.getName();

				if (selectedItem != null && selectedItem.equals(code)) {
					result = result.append("<option value= '").append(code).append("' selected>").append(desc).append("</option>" );
				}
				else {
					result = result.append("<option value= '").append(code).append("'>").append(desc).append("</option>");
				}
			}
		}
		result = result.insert(0,"<option value=''>Select Section</option>");
		if (disabled.equals("") || disabled.equals("false")) {
			result = result.insert(0,new StringBuilder("<select class='").append(className).append("' name= '").append(listBoxName).append("' style= '").append(styleName).append("' onChange= '").append(onChange).append("' onFocus= '").append(onFocus).append("'>"));
		}
		else {
			result = result.insert(0,new StringBuilder("<select class='").append(className).append("' name= '").append(listBoxName).append("' style= '").append(styleName).append("' disabled= '").append(disabled).append("' onChange= '").append(onChange).append("' onFocus= '").append(onFocus).append("'>"));
		}
		result = result.append( "</select>");
		pageContext.getOut().println(result);
		LOGGER.info("listSections end");
	}//listSections


	private void listUsers(String _filter) throws Exception {
		filter = _filter;
		listUsers();
	}//listUsers


	private void listUsers() throws Exception {
		
		LOGGER.info("listUsers start");
		StringBuilder result = new StringBuilder();
		CodeHelper codeHelper = new CodeHelper();
		Collection list = codeHelper.getCodeValues("users", filter);

		// Create option tags
		if (list != null) {
			
			Iterator iterator = list.iterator();
			while (iterator.hasNext()) {
				
				NameValuePair nameValuePair = (NameValuePair)iterator.next();
				String code = nameValuePair.getValue();
				String desc = nameValuePair.getName();

				if (selectedItem != null && selectedItem.equals(code)) {
					result.append("<option value='").append(code).append("' selected>").append(desc).append("</option>");
				} else {
					result.append("<option value='").append(code).append("'>").append(desc).append("</option>");
				}
			}
		}

		// Create Select Tag
		result = result.insert(0, "<option value=''>Select User</option>");
		if (disabled.equals("") || disabled.equals("false")) {
			result = result.insert(0, new StringBuilder("<select class='").append(className).append("' name='").append(listBoxName).append("' style='").append(styleName).append("' onChange='").append(onChange).append("' onFocus='").append(onFocus).append("'>"));
		} else {
			result = result.insert(0,new StringBuilder("<select class='").append(className).append("' name='").append(listBoxName).append("' style='").append(styleName).append("' disabled='").append(disabled).append("' onChange='").append(onChange).append("' onFocus='").append(onFocus).append("'>"));
		}
		result = result.append( "</select>");
		
		// Display output
		pageContext.getOut().println(result);
		LOGGER.info("listUsers end");
	}//listUsers


	private void listBranchAgents() throws Exception {
		
		LOGGER.info("listBranchAgents start");
		StringBuilder result = new StringBuilder();
		CodeHelper codeHelper = new CodeHelper();
		Collection list = codeHelper.getCodeValues("branchAgents", filter);

		if (list != null) {
			Iterator iterator = list.iterator();

			while (iterator.hasNext()) {
				NameValuePair nameValuePair = (NameValuePair)iterator.next();
				String code = nameValuePair.getValue();
				String desc = nameValuePair.getName();

				if (selectedItem != null && selectedItem.equals(code)) {
					result.append("<option value= '").append(code).append("' selected>").append(desc).append("</option>");
				}
				else {
					result.append("<option value= '").append(code).append("' >").append(desc).append("</option>");
				}
			}
		}

		result = result.insert(0,"<option value=''>Select User</option>");
		if (disabled.equals("") || disabled.equals("false")) {
			result = result.insert(0,new StringBuilder("<select class='").append(className).append("' name= '").append(listBoxName).append("' style= '").append(styleName).append("' onChange= '").append(onChange).append("' onFocus= '").append(onFocus).append("'>"));
		}
		else {
			result = result.insert(0,new StringBuilder("<select class='").append(className).append("' name= '").append(listBoxName).append("' style= '").append(styleName).append("' disabled= '").append(disabled).append("' onChange= '").append(onChange).append("' onFocus= '").append(onFocus).append("'>"));
		}
		result = result.append( "</select>");
		pageContext.getOut().println(result);
		LOGGER.info("listBranchAgents end");
	}//listBranchAgents


	private void listAccessLevel() throws Exception {
		
		LOGGER.info("listAccessLevel start");
		StringBuilder result = new StringBuilder();
		CodeHelper codeHelper = new CodeHelper();
		Collection list = new ArrayList();

		list.add(new NameValuePair("User","User"));
		list.add(new NameValuePair("Underwriter","Underwriter"));
		list.add(new NameValuePair("Manager","Manager"));
		list.add(new NameValuePair("Supervisor","Supervisor"));
		list.add(new NameValuePair("Administrator","Administrator"));

		if (list != null) {
			Iterator iterator = list.iterator();

			while (iterator.hasNext()) {
				NameValuePair nameValuePair = (NameValuePair)iterator.next();
				String code = nameValuePair.getValue();
				String desc = nameValuePair.getName();

				if (selectedItem != null && selectedItem.equals(code)) {
					result.append("<option value= '").append(code).append("' selected>").append(desc).append("</option>");
				}
				else {
					result.append("<option value= '").append(code).append("' >").append(desc).append("</option>");
				}
			}
		}
		result = result.insert(0,"<option value=''>Select Access Level</option>");
		if (disabled.equals("") || disabled.equals("false")) {
			result = result.insert(0,new StringBuilder("<select class='").append(className).append("' name= '").append(listBoxName).append("' style= '").append(styleName).append("' onChange= '").append(onChange).append("' onFocus= '").append(onFocus).append("'>"));
		}
		else {
			result = result.insert(0,new StringBuilder("<select class='").append(className).append("' name= '").append(listBoxName).append("' style= '").append(styleName).append("' disabled= '").append(disabled).append("' onChange= '").append(onChange).append("' onFocus= '").append(onFocus).append("'>"));
		}
		result = result.append( "</select>");
		pageContext.getOut().println(result);
		LOGGER.info("listAccessLevel end");
	}//listAccessLevel


	private void listUserType() throws Exception {
		
		LOGGER.info("listUserType start");
		StringBuilder result = new StringBuilder();
		CodeHelper codeHelper = new CodeHelper();
		Collection list = new ArrayList();

		list.add(new NameValuePair("Examiner",IUMConstants.USER_TYPE_EXAMINER));
		list.add(new NameValuePair("Laboratory",IUMConstants.USER_TYPE_LABORATORY));
		list.add(new NameValuePair("Agent", IUMConstants.USER_TYPE_AGENT));
		list.add(new NameValuePair("Sun Life", IUMConstants.USER_TYPE_SUNLIFE));
		if (list != null) {
			Iterator iterator = list.iterator();

			while (iterator.hasNext()) {
				NameValuePair nameValuePair = (NameValuePair)iterator.next();
				String code = nameValuePair.getValue();
				String desc = nameValuePair.getName();

				if (selectedItem != null && selectedItem.equals(code)) {
					result.append("<option value= '").append(code).append("' selected>").append(desc).append("</option>");
				}
				else {
					result.append("<option value= '").append(code).append("' >").append(desc).append("</option>");
				}
			}
		}

		result = result.insert(0,"<option value=''>Select User Type</option>");
		if (disabled.equals("") || disabled.equals("false")) {
			result = result.insert(0,new StringBuilder("<select class='").append(className).append("' name= '").append(listBoxName).append("' style= '").append(styleName).append("' onChange= '").append(onChange).append("' onFocus= '").append(onFocus).append("'>"));
		}
		else {
			result = result.insert(0,new StringBuilder("<select class='").append(className).append("' name= '").append(listBoxName).append("' style= '").append(styleName).append("' disabled= '").append(disabled).append("' onChange= '").append(onChange).append("' onFocus= '").append(onFocus).append("'>"));
		}
		result = result.append( "</select>");
		pageContext.getOut().println(result);
		LOGGER.info("listUserType end");
	}//listUserType


	private void listExaminers() throws Exception {
		
		LOGGER.info("listExaminers start");
		StringBuilder result = new StringBuilder();
		CodeHelper codeHelper = new CodeHelper();
		Collection examList = codeHelper.getExaminerData();

		if (examList != null) {
			Iterator iterator = examList.iterator();

			while (iterator.hasNext()) {
				ExaminerData nameValuePair = (ExaminerData)iterator.next();
				String code = Long.toString(nameValuePair.getExaminerId());
				String fname = nameValuePair.getFirstName();
				String mname = nameValuePair.getMiddleName();
				String lname = nameValuePair.getLastName();
				if (fname == null) fname = "";
				if (mname == null) mname = "";
				if (lname == null) lname = "";
			
				String desc = lname + ", " + fname;

				if (selectedItem != null && selectedItem.equals(code)) {
					result.append("<option value= '").append(code).append("' selected>").append(desc).append("</option>");
				}
				else {
					result.append("<option value= '").append(code).append("' >").append(desc).append("</option>");
				}
			}
		}

		result = result.insert(0,"<option value=''>Select Examiner</option>");
		if (disabled.equals("") || disabled.equals("false")) {
			result = result.insert(0,new StringBuilder("<select class='").append(className).append("' name= '").append(listBoxName).append("' style= '").append(styleName).append("' onChange= '").append(onChange).append("' onFocus= '").append(onFocus).append("'>"));
		}
		else {
			result = result.insert(0,new StringBuilder("<select class='").append(className).append("' name= '").append(listBoxName).append("' style= '").append(styleName).append("' disabled= '").append(disabled).append("' onChange= '").append(onChange).append("' onFocus= '").append(onFocus).append("'>"));
		}
		result = result.append( "</select>");
		pageContext.getOut().println(result);
		LOGGER.info("listExaminers end");
		
	}//listExaminers


	private void listLaboratories() throws Exception {
		
		LOGGER.info("listLaboratories start");
		StringBuilder result = new StringBuilder();
		CodeHelper codeHelper = new CodeHelper();
		Collection labList = codeHelper.getLaboratoryData();

		if (labList != null) {
			Iterator iterator = labList.iterator();

			while (iterator.hasNext()) {
				LaboratoryData nameValuePair = (LaboratoryData)iterator.next();
				LOGGER.debug("lab Id = " + nameValuePair.getLabId());
				String code = Long.toString(nameValuePair.getLabId());
				String desc = nameValuePair.getLabName();

				if (selectedItem != null && selectedItem.equals(code)) {
					result.append("<option value= '").append(code).append("' selected>").append(desc).append("</option>");
				}
				else {
					result.append("<option value= '").append(code).append("' >").append(desc).append("</option>");
				}
			}
		}

		result = result.insert(0,"<option value=''>Select Laboratory</option>");
		if (disabled.equals("") || disabled.equals("false")) {
			result = result.insert(0,new StringBuilder("<select class='").append(className).append("' name= '").append(listBoxName).append("' style= '").append(styleName).append("' onChange= '").append(onChange).append("' onFocus= '").append(onFocus).append("'>"));
		}
		else {
			result = result.insert(0,new StringBuilder("<select class='").append(className).append("' name= '").append(listBoxName).append("' style= '").append(styleName).append("' disabled= '").append(disabled).append("' onChange= '").append(onChange).append("' onFocus= '").append(onFocus).append("'>"));
		}
		result = result.append( "</select>");
		pageContext.getOut().println(result);
		LOGGER.info("listLaboratories end");
	}//listLaboratories


	private void listSchedule() throws Exception {
		
		LOGGER.info("listSchedule start");
		StringBuilder result = new StringBuilder();
		CodeHelper codeHelper = new CodeHelper();
		Collection schedList = new ArrayList();

		schedList.add(new NameValuePair("No Schedule","nosched"));
		schedList.add(new NameValuePair("12:00 AM","12:00 AM"));
		schedList.add(new NameValuePair("01:00 AM","01:00 AM"));
		schedList.add(new NameValuePair("02:00 AM","02:00 AM"));
		schedList.add(new NameValuePair("03:00 AM","03:00 AM"));
		schedList.add(new NameValuePair("04:00 AM","04:00 AM"));
		schedList.add(new NameValuePair("05:00 AM","05:00 AM"));
		schedList.add(new NameValuePair("06:00 AM","06:00 AM"));
		schedList.add(new NameValuePair("07:00 AM","07:00 AM"));
		schedList.add(new NameValuePair("08:00 AM","08:00 AM"));
		schedList.add(new NameValuePair("09:00 AM","09:00 AM"));
		schedList.add(new NameValuePair("10:00 AM","10:00 AM"));
		schedList.add(new NameValuePair("11:00 AM","11:00 AM"));
		schedList.add(new NameValuePair("12:00 PM","12:00 PM"));
		schedList.add(new NameValuePair("01:00 PM","01:00 PM"));
		schedList.add(new NameValuePair("02:00 PM","02:00 PM"));
		schedList.add(new NameValuePair("03:00 PM","03:00 PM"));
		schedList.add(new NameValuePair("04:00 PM","04:00 PM"));
		schedList.add(new NameValuePair("05:00 PM","05:00 PM"));
		schedList.add(new NameValuePair("06:00 PM","06:00 PM"));
		schedList.add(new NameValuePair("07:00 PM","07:00 PM"));
		schedList.add(new NameValuePair("08:00 PM","08:00 PM"));
		schedList.add(new NameValuePair("09:00 PM","09:00 PM"));
		schedList.add(new NameValuePair("10:00 PM","10:00 PM"));
		schedList.add(new NameValuePair("11:00 PM","11:00 PM"));

		if (schedList != null) {
			Iterator iterator = schedList.iterator();

			while (iterator.hasNext()) {
				NameValuePair nameValuePair = (NameValuePair)iterator.next();
				String code = nameValuePair.getValue();
				String desc = nameValuePair.getName();

				if (selectedItem != null && selectedItem.equals(code)) {
					result.append("<option value= '").append(code).append("' selected>").append(desc).append("</option>");
				}
				else {
					result.append("<option value= '").append(code).append("' >").append(desc).append("</option>");
				}
			}
		}

		result = result.insert(0,"<option value=''>Select Schedule</option>");
		if (disabled.equals("") || disabled.equals("false")) {
			result = result.insert(0,new StringBuilder("<select class='").append(className).append("' name= '").append(listBoxName).append("' style= '").append(styleName).append("' onChange= '").append(onChange).append("' onFocus= '").append(onFocus).append("'>"));
		}
		else {
			result = result.insert(0,new StringBuilder("<select class='").append(className).append("' name= '").append(listBoxName).append("' style= '").append(styleName).append("' disabled= '").append(disabled).append("' onChange= '").append(onChange).append("' onFocus= '").append(onFocus).append("'>"));
		}
		result = result.append( "</select>");
		pageContext.getOut().println(result);
		LOGGER.info("listSchedule end");
	}//listSchedule


	private void listImpairmentConfirmation() throws Exception {

		LOGGER.info("listImpairmentConfirmation start");
		StringBuilder result = new StringBuilder();
		CodeHelper codeHelper = new CodeHelper();
		Collection impConfirmList = new ArrayList();

		impConfirmList.add(new NameValuePair("R","R"));
		impConfirmList.add(new NameValuePair("V","V"));

		if(impConfirmList != null) {
			Iterator iterator = impConfirmList.iterator();

			while (iterator.hasNext()) {
				NameValuePair nameValuePair = (NameValuePair)iterator.next();
				String code = nameValuePair.getValue();
				String desc = nameValuePair.getName();

				if (selectedItem != null && selectedItem.equals(code)) {
					result.append("<option value= '").append(code).append("' selected>").append(desc).append("</option>");
				}
				else {
					result.append("<option value= '").append(code).append("' >").append(desc).append("</option>");
				}
			}
		}

		result = result.insert(0,"<option value=''>Select</option>");
		if (disabled.equals("") || disabled.equals("false")) {
			result = result.insert(0,new StringBuilder("<select class='").append(className).append("' name= '").append(listBoxName).append("' style= '").append(styleName).append("' onChange= '").append(onChange).append("' onFocus= '").append(onFocus).append("'>"));
		}
		else {
			result = result.insert(0,new StringBuilder("<select class='").append(className).append("' name= '").append(listBoxName).append("' style= '").append(styleName).append("' disabled= '").append(disabled).append("' onChange= '").append(onChange).append("' onFocus= '").append(onFocus).append("'>"));
		}
		result = result.append( "</select>");
		pageContext.getOut().println(result);
		LOGGER.info("listImpairmentConfirmation end");
	}//listImpairmentConfirmation


	private void listRequirementCode() throws Exception {
		
		LOGGER.info("listRequirementCode start");
		StringBuilder result = new StringBuilder();
		CodeHelper codeHelper = new CodeHelper();
		Collection list = codeHelper.getCodeValues("requirementCode");

		if (list != null) {
			Iterator iterator = list.iterator();

			//parse list
			while (iterator.hasNext()) {
				NameValuePair nameValuePair = (NameValuePair)iterator.next();
				String code = nameValuePair.getValue();
				String desc = nameValuePair.getName();

				if (selectedItem != null && selectedItem.equals(code)) {
					result = result.append("<option value= '").append(code).append("' selected>"+code+" - ").append(desc).append("</option>" );
				} else {
					result = result.append("<option value= '").append(code).append("'>").append(code).append(" - ").append(desc).append("</option>");
				}
			}
		}
		result = result.insert(0,"<option value=''>Select Requirement</option>");
		if (disabled.equals("") || disabled.equals("false")) {
			result = result.insert(0,new StringBuilder("<select class='").append(className).append("' name= '").append(listBoxName).append("' style= '").append(styleName).append("' onChange= '").append(onChange).append("' onFocus= '").append(onFocus).append("'>"));
		}
		else {
			result = result.insert(0,new StringBuilder("<select class='").append(className).append("' name= '").append(listBoxName).append("' style= '").append(styleName).append("' disabled= '").append(disabled).append("' onChange= '").append(onChange).append("' onFocus= '").append(onFocus).append("'>"));
		}
		result = result.append( "</select>");
		pageContext.getOut().println(result);
		LOGGER.info("listRequirementCode end");
	}//listRequirementCode


	private void listReferenceCodes() throws Exception {
		
		LOGGER.info("listReferenceCodes start");
		StringBuilder result = new StringBuilder();
		Collection list = new ArrayList();

		//populate reference code list
		list.add(new NameValuePair("Access", IUMConstants.REF_CODE_ACCESS));
		list.add(new NameValuePair("Access Template", IUMConstants.REF_CODE_ACCESS_TEMPLATE));
		list.add(new NameValuePair("Auto-Assignment", IUMConstants.REF_CODE_AUTO_ASSIGN));
		list.add(new NameValuePair("Auto-Assignment Criteria", IUMConstants.REF_CODE_AUTO_ASSIGN_CRITERIA));
		list.add(new NameValuePair("Client Type", IUMConstants.REF_CODE_CLIENT_TYPE));
		list.add(new NameValuePair("Department", IUMConstants.REF_CODE_DEPARTMENT));
		list.add(new NameValuePair("Document Type", IUMConstants.REF_CODE_DOCUMENT_TYPE));
		list.add(new NameValuePair("Examination Area", IUMConstants.REF_CODE_EXAM_AREA));
		list.add(new NameValuePair("Examination Place", IUMConstants.REF_CODE_EXAM_PLACE));
		list.add(new NameValuePair("Examiner Rank", IUMConstants.REF_CODE_EXAM_RANK));
		list.add(new NameValuePair("Examiner Specialization", IUMConstants.REF_CODE_EXAM_SPECIALIZATION));
		list.add(new NameValuePair("Holiday", IUMConstants.REF_CODE_HOLIDAY));
		list.add(new NameValuePair("LOB", IUMConstants.REF_CODE_LOB));
		list.add(new NameValuePair("MIB Action", IUMConstants.REF_CODE_MIB_ACTION));
		list.add(new NameValuePair("MIB Impairment", IUMConstants.REF_CODE_MIB_IMPAIRMENT));
		list.add(new NameValuePair("MIB Letter", IUMConstants.REF_CODE_MIB_LETTER));
		list.add(new NameValuePair("MIB Number", IUMConstants.REF_CODE_MIB_NUMBER));
		list.add(new NameValuePair("Page", IUMConstants.REF_CODE_PAGE));
		list.add(new NameValuePair("Requirement Forms", IUMConstants.REF_CODE_REQUIREMENT_FORMS));
		list.add(new NameValuePair("Roles", IUMConstants.REF_CODE_ROLES));
		list.add(new NameValuePair("Section", IUMConstants.REF_CODE_SECTION));
		list.add(new NameValuePair("Status", IUMConstants.REF_CODE_STATUS));
		list.add(new NameValuePair("Test Profile", IUMConstants.REF_CODE_TEST_PROFILE));

		if (list != null) {
			Iterator iterator = list.iterator();

			while (iterator.hasNext()) {
				NameValuePair nameValuePair = (NameValuePair)iterator.next();
				String code = nameValuePair.getValue();
				String desc = nameValuePair.getName();

				if (selectedItem != null && selectedItem.equals(code)) {
					result.append("<option value= '").append(code).append("' selected>").append(desc).append("</option>");
				}
				else {
					result.append("<option value= '").append(code).append("' >").append(desc).append("</option>");
				}
			}
		}

		result = result.insert(0,"<option value=''>Select Reference Code</option>");
		if (disabled.equals("") || disabled.equals("false")) {
			result = result.insert(0,new StringBuilder("<select class='").append(className).append("' name= '").append(listBoxName).append("' style= '").append(styleName).append("' onChange= '").append(onChange).append("' onFocus= '").append(onFocus).append("'>"));
		}
		else {
			result = result.insert(0,new StringBuilder("<select class='").append(className).append("' name= '").append(listBoxName).append("' style= '").append(styleName).append("' disabled= '").append(disabled).append("' onChange= '").append(onChange).append("' onFocus= '").append(onFocus).append("'>"));
		}
		result = result.append( "</select>");
		pageContext.getOut().println(result);
		LOGGER.info("listReferenceCodes end");
	}//listReferenceCodes


	private void listStatusType() throws Exception {
		
		LOGGER.info("listStatusType start");
		StringBuilder result = new StringBuilder();
		Collection list = new ArrayList();

		//populate status type list
		list.add(new NameValuePair(IUMConstants.STAT_TYPE_DESC_AR, IUMConstants.STAT_TYPE_AR));
		list.add(new NameValuePair(IUMConstants.STAT_TYPE_DESC_M, IUMConstants.STAT_TYPE_M));
		list.add(new NameValuePair(IUMConstants.STAT_TYPE_DESC_NM, IUMConstants.STAT_TYPE_NM));

		if (list != null) {
			Iterator iterator = list.iterator();

			while (iterator.hasNext()) {
				NameValuePair nameValuePair = (NameValuePair)iterator.next();
				String code = nameValuePair.getValue();
				String desc = nameValuePair.getName();

				if (selectedItem != null && selectedItem.equals(code)) {
					result.append("<option value= '").append(code).append("' selected>").append(desc).append("</option>");
				}
				else {
					result.append("<option value= '").append(code).append("' >").append(desc).append("</option>");
				}
			}
		}
		result = result.insert(0,"<option value=''>Select Status Type</option>");
		if (disabled.equals("") || disabled.equals("false")) {
			result = result.insert(0,new StringBuilder("<select class='").append(className).append("' name= '").append(listBoxName).append("' style= '").append(styleName).append("' onChange= '").append(onChange).append("' onFocus= '").append(onFocus).append("'>"));
		}
		else {
			result = result.insert(0,new StringBuilder("<select class='").append(className).append("' name= '").append(listBoxName).append("' style= '").append(styleName).append("' disabled= '").append(disabled).append("' onChange= '").append(onChange).append("' onFocus= '").append(onFocus).append("'>"));
		}
		result = result.append( "</select>");
		pageContext.getOut().println(result);
		LOGGER.info("listStatusType end");
	}//listStatusType


	private void listFormTemplateFormat() throws Exception {
		
		LOGGER.info("listFormTemplateFormat start");
		StringBuilder result = new StringBuilder();
		Collection list = new ArrayList();

		//populate template format list
		list.add(new NameValuePair("PDF", "PDF"));

		if (list != null) {
			Iterator iterator = list.iterator();

			while (iterator.hasNext()) {
				NameValuePair nameValuePair = (NameValuePair)iterator.next();
				String code = nameValuePair.getValue();
				String desc = nameValuePair.getName();

				if (selectedItem != null && selectedItem.equals(code)) {
					result.append("<option value= '").append(code).append("' selected>").append(desc).append("</option>");
				}
				else {
					result.append("<option value= '").append(code).append("' >").append(desc).append("</option>");
				}
			}
		}

		result = result.insert(0,"<option value=''>Select Template Format</option>");
		if (disabled.equals("") || disabled.equals("false")) {
			result = result.insert(0,new StringBuilder("<select class='").append(className).append("' name= '").append(listBoxName).append("' style= '").append(styleName).append("' onChange= '").append(onChange).append("' onFocus= '").append(onFocus).append("'>"));
		}
		else {
			result = result.insert(0,new StringBuilder("<select class='").append(className).append("' name= '").append(listBoxName).append("' style= '").append(styleName).append("' disabled= '").append(disabled).append("' onChange= '").append(onChange).append("' onFocus= '").append(onFocus).append("'>"));
		}
		result = result.append( "</select>");
		pageContext.getOut().println(result);
		LOGGER.info("listFormTemplateFormat end");
	}//listFormTemplateFormat


	private void listHolidayYear() throws Exception {
		
		LOGGER.info("listHolidayYear start");
		StringBuilder result = new StringBuilder();
		CodeHelper codeHelper = new CodeHelper();
		Collection list = codeHelper.getCodeValues("holidayYear");

		if (list != null) {
			Iterator iterator = list.iterator();

			//parse list
			while (iterator.hasNext()) {
				String year = (String)iterator.next();

				if (selectedItem != null && selectedItem.equals(year)) {
					result.append("<option value= '").append(year).append("' selected>").append(year).append("</option>");
				}
				else {
					result.append("<option value= '").append(year).append("' >").append(year).append("</option>");
				}
			}
		}
		result = result.insert(0,"<option value=''>Select Year</option>");
		if (disabled.equals("") || disabled.equals("false")) {
			result = result.insert(0,new StringBuilder("<select class='").append(className).append("' name= '").append(listBoxName).append("' style= '").append(styleName).append("' onChange= '").append(onChange).append("' onFocus= '").append(onFocus).append("'>"));
		}
		else {
			result = result.insert(0,new StringBuilder("<select class='").append(className).append("' name= '").append(listBoxName).append("' style= '").append(styleName).append("' disabled= '").append(disabled).append("' onChange= '").append(onChange).append("' onFocus= '").append(onFocus).append("'>"));
		}
		result = result.append( "</select>");
		pageContext.getOut().println(result);
		LOGGER.info("listHolidayYear end");
	}//listHolidayYear


	private void listAutoAssignmentCriteria() throws Exception {
		
		LOGGER.info("listAutoAssignmentCriteria start");
		StringBuilder result = new StringBuilder();
		CodeHelper codeHelper = new CodeHelper();
		Collection list = codeHelper.getCodeValues("autoAssignmentCriteria");

		if (list != null) {
			Iterator iterator = list.iterator();

			//parse list
			while (iterator.hasNext()) {
				NameValuePair nameValuePair = (NameValuePair)iterator.next();
				String code = nameValuePair.getValue();
				String desc = nameValuePair.getName();

				if (selectedItem != null && selectedItem.equals(code)) {
					result = result.append("<option value= '").append(code).append("' selected>").append(desc).append("</option>" );
				}
				else {
					result = result.append("<option value= '").append(code).append("'>").append(desc).append("</option>");
				}
			}
		}
		result = result.insert(0,"<option value=''>Select Auto-Assignment Criteria</option>");
		if (disabled.equals("") || disabled.equals("false")) {
			result = result.insert(0,new StringBuilder("<select class='").append(className).append("' name= '").append(listBoxName).append("' style= '").append(styleName).append("' onChange= '").append(onChange).append("' onFocus= '").append(onFocus).append("'>"));
		}
		else {
			result = result.insert(0,new StringBuilder("<select class='").append(className).append("' name= '").append(listBoxName).append("' style= '").append(styleName).append("' disabled= '").append(disabled).append("' onChange= '").append(onChange).append("' onFocus= '").append(onFocus).append("'>"));
		}
		result = result.append( "</select>");
		pageContext.getOut().println(result);
		LOGGER.info("listAutoAssignmentCriteria end");
	}//listAutoAssignmentCriteria


	private void listTransactionTypes() throws Exception {
		
		LOGGER.info("listTransactionTypes start");
		StringBuilder result = new StringBuilder();
		CodeHelper codeHelper = new CodeHelper();
		Collection transactionTypes = new ArrayList();

		transactionTypes.add(new NameValuePair("Add", "ADD"));
		transactionTypes.add(new NameValuePair("Update", "UPDATE"));
		transactionTypes.add(new NameValuePair("Delete", "DELETE"));

		if (transactionTypes != null){
			Iterator it = transactionTypes.iterator();

			while (it.hasNext()){
				NameValuePair nv = (NameValuePair) it.next();
				String code = nv.getValue();
				String desc = nv.getName();

				if (selectedItem != null && selectedItem.equalsIgnoreCase(code)){
					result.append("<option value= '").append(code).append("' selected>").append(desc).append("</option>");
				}
				else{
					result.append("<option value= '").append(code).append("'>").append(desc).append("</option>");
				}
			}
		}
		result = result.insert(0,"<option value=''></option>");
		if (disabled.equals("") || disabled.equals("false")) {
			result = result.insert(0,new StringBuilder("<select class='").append(className).append("' name= '").append(listBoxName).append("' style= '").append(styleName).append("' onChange= '").append(onChange).append("' onFocus= '").append(onFocus).append("'>"));
		}
		else {
			result = result.insert(0,new StringBuilder("<select class='").append(className).append("' name= '").append(listBoxName).append("' style= '").append(styleName).append("' disabled= '").append(disabled).append("' onChange= '").append(onChange).append("' onFocus= '").append(onFocus).append("'>"));
		}
		result = result.append( "</select>");
		pageContext.getOut().println(result);
		LOGGER.info("listTransactionTypes end");
	}//listTransactionTypes


	private void listTables() throws Exception {
		
		LOGGER.info("listTables start");
		StringBuilder result = new StringBuilder();
		CodeHelper codeHelper = new CodeHelper();
		Collection tables = new ArrayList();

		// populate tables in Audit Trail
		tables.add(new NameValuePair("Assessment Requests","1"));
		tables.add(new NameValuePair("Policy Requirements","2"));
		tables.add(new NameValuePair("Impairments","3"));
		tables.add(new NameValuePair("Medical Records","4"));
		tables.add(new NameValuePair("User Profiles","5"));
		tables.add(new NameValuePair("User Access","6"));
		tables.add(new NameValuePair("Requirements","7"));
		tables.add(new NameValuePair("Requirement Forms","8"));
		tables.add(new NameValuePair("Lines of Business","9"));
		tables.add(new NameValuePair("Departments","10"));
		tables.add(new NameValuePair("Sections","11"));
		tables.add(new NameValuePair("Specializations","12"));
		tables.add(new NameValuePair("Examination Places","13"));
		tables.add(new NameValuePair("Examination Areas","14"));
		tables.add(new NameValuePair("Document Types","15"));
		tables.add(new NameValuePair("Client Types","16"));
		tables.add(new NameValuePair("Laboratory Tests","17"));
		tables.add(new NameValuePair("Auto-assign Criteria","18"));
		tables.add(new NameValuePair("Status Codes","19"));
		tables.add(new NameValuePair("Laboratories","20"));
		tables.add(new NameValuePair("Test Profiles","21"));
		tables.add(new NameValuePair("Examiners","22"));
		tables.add(new NameValuePair("Ranks","23"));
		tables.add(new NameValuePair("Branches","24"));
		tables.add(new NameValuePair("Underwriting Auto-assign Criteria","25"));
		tables.add(new NameValuePair("Process Configurations","26"));
		tables.add(new NameValuePair("Holidays","27"));
		tables.add(new NameValuePair("Roles","28"));
		tables.add(new NameValuePair("Notification Templates","29"));
		tables.add(new NameValuePair("MIB Export","30"));
		tables.add(new NameValuePair("Auto-expire Medical Records","31"));
		tables.add(new NameValuePair("Archive Audit Trails","32"));
		tables.add(new NameValuePair("MIB Impairments","33"));
		tables.add(new NameValuePair("MIB Actions","34"));
		tables.add(new NameValuePair("MIB Numbers","35"));
		tables.add(new NameValuePair("MIB Letters","36"));
		tables.add(new NameValuePair("MIB Export Schedule","37"));
		tables.add(new NameValuePair("Auto-expire Schedule","38"));
		tables.add(new NameValuePair("Medical Bills","39"));
		tables.add(new NameValuePair("Accesses","40"));
		tables.add(new NameValuePair("Pages","41"));
		tables.add(new NameValuePair("Access Templates","42"));
		tables.add(new NameValuePair("Access Template Details","43"));
		tables.add(new NameValuePair("Examiner Specializations","44"));
		tables.add(new NameValuePair("Notification Recipients","45"));
		tables.add(new NameValuePair("LOB Requirements","46"));
		tables.add(new NameValuePair("LOB Documents","47"));
		tables.add(new NameValuePair("LOB Status","48"));



		if (tables != null){
			Iterator it = tables.iterator();

			while (it.hasNext()){
				NameValuePair nv = (NameValuePair) it.next();
				String code = nv.getValue();
				String desc = nv.getName();

				if (selectedItem != null && selectedItem.equalsIgnoreCase(code)){
					result.append("<option value= '").append(code).append("' selected>").append(desc).append("</option>");
				} else {
					result.append("<option value= '").append(code).append("'>").append(desc).append("</option>");
				}
			}
		}
		result = result.insert(0,"<option value=''></option>");
		if (disabled.equals("") || disabled.equals("false")) {
			result = result.insert(0,new StringBuilder("<select class='").append(className).append("' name= '").append(listBoxName).append("' style= '").append(styleName).append("' onChange= '").append(onChange).append("' onFocus= '").append(onFocus).append("'>"));
		}
		else {
			result = result.insert(0,new StringBuilder("<select class='").append(className).append("' name= '").append(listBoxName).append("' style= '").append(styleName).append("' disabled= '").append(disabled).append("' onChange= '").append(onChange).append("' onFocus= '").append(onFocus).append("'>"));
		}
		result = result.append( "</select>");
		pageContext.getOut().println(result);
		LOGGER.info("listTables end");
	}//listTables


	private void listRequirementForms() throws Exception {
		
		LOGGER.info("listRequirementForms start");
		StringBuilder result = new StringBuilder();
		Connection conn = null;
		ArrayList data = null;
		
		try{
			conn=new DataSourceProxy().getConnection();
			RequirementFormDAO rfrmDAO = new RequirementFormDAO(conn);
			SortHelper sort = new SortHelper(IUMConstants.TABLE_REQUIREMENT_FORMS);
			sort.setColumn(IUMConstants.SORT_BY_REF_FORM_NAME);
			sort.setSortOrder(IUMConstants.ASCENDING);
			data = rfrmDAO.retrieveRequirementForms(sort);
		}finally{
			closeConnection(conn);
		}

		if (data != null) {
			for (int i=0; i<data.size(); i++) {
				RequirementFormData reqf= (RequirementFormData)data.get(i);
				String code = String.valueOf(reqf.getFormId());
				String desc = reqf.getFormName();
				if (selectedItem != null && selectedItem.equals(String.valueOf(code))){
					result = result.append("<option value= '").append(code).append("' selected>").append(desc).append("</option>");
				}
				else{
					result = result.append("<option value= '").append(code).append("'>").append(desc).append("</option>");
				}
			}
		}
		result = result.insert(0,"<option value=''>Select Form</option>");
		if (disabled.equals("") || disabled.equals("false")) {
			result = result.insert(0,new StringBuilder("<select class='").append(className).append("' name= '").append(listBoxName).append("' style= '").append(styleName).append("' onChange= '").append(onChange).append("' onFocus= '").append(onFocus).append("'>"));
		}
		else {
			result = result.insert(0,new StringBuilder("<select class='").append(className).append("' name= '").append(listBoxName).append("' style= '").append(styleName).append("' disabled= '").append(disabled).append("' onChange= '").append(onChange).append("' onFocus= '").append(onFocus).append("'>"));
		}
		result = result.append( "</select>");
		pageContext.getOut().println(result);

		LOGGER.info("listRequirementForms end");
	}//listRequirementForms


	private void listTestProfile() throws Exception {
		
		LOGGER.info("listTestProfile start");
		StringBuilder result = new StringBuilder();
		CodeHelper codeHelper = new CodeHelper();
		Collection list = codeHelper.getCodeValues("testProfiles");

		if (list != null) {
			Iterator iterator = list.iterator();

			//parse list
			while (iterator.hasNext()) {
				NameValuePair nameValuePair = (NameValuePair)iterator.next();
				String code = nameValuePair.getValue();
				String desc = nameValuePair.getName();

				if (selectedItem != null && selectedItem.equals(code)) {
					result = result.append("<option value= '").append(code).append("' selected>"+ desc).append("</option>" );
				}
				else {
					result = result.append("<option value= '").append(code).append("'>").append(desc).append("</option>");
				}
			}
		}
		result = result.insert(0,"<option value=''>Select Test Code</option>");
		if (disabled.equals("") || disabled.equals("false")) {
			result = result.insert(0,new StringBuilder("<select class='").append(className).append("' name= '").append(listBoxName).append("' style= '").append(styleName).append("' onChange= '").append(onChange).append("' onFocus= '").append(onFocus).append("'>"));
		}
		else {
			result = result.insert(0,new StringBuilder("<select class='").append(className).append("' name= '").append(listBoxName).append("' style= '").append(styleName).append("' disabled= '").append(disabled).append("' onChange= '").append(onChange).append("' onFocus= '").append(onFocus).append("'>"));
		}
		result = result.append( "</select>");
		pageContext.getOut().println(result);
		LOGGER.info("listTestProfile end");
	}//listTestProfile

	//listLaboratoryTestProfile
	private void listLaboratoryTestProfile() throws Exception {
		
		LOGGER.info("listLaboratoryTestProfile start");
		StringBuilder result = new StringBuilder();
		CodeHelper codeHelper = new CodeHelper();
		Collection list = codeHelper.getCodeValues("laboratoryTestProfiles", filter);

		if (list != null) {
			Iterator iterator = list.iterator();

			//parse list
			while (iterator.hasNext()) {
				NameValuePair nameValuePair = (NameValuePair)iterator.next();
				String code = nameValuePair.getValue();
				String desc = nameValuePair.getName();

				if (selectedItem != null && selectedItem.equals(code)) {
					result = result.append("<option value= '").append(code).append("' selected>"+ desc).append("</option>" );
				}
				else {
					result = result.append("<option value= '").append(code).append("'>").append(desc).append("</option>");
				}
			}
		}
		result = result.insert(0,"<option value=''>Select Test Code</option>");
		if (disabled.equals("") || disabled.equals("false")) {
			result = result.insert(0,new StringBuilder("<select class='").append(className).append("' name= '").append(listBoxName).append("' style= '").append(styleName).append("' onChange= '").append(onChange).append("' onFocus= '").append(onFocus).append("'>"));
		}
		else {
			result = result.insert(0,new StringBuilder("<select class='").append(className).append("' name= '").append(listBoxName).append("' style= '").append(styleName).append("' disabled= '").append(disabled).append("' onChange= '").append(onChange).append("' onFocus= '").append(onFocus).append("'>"));
		}
		result = result.append( "</select>");
		pageContext.getOut().println(result);
		LOGGER.info("listLaboratoryTestProfile end");
	}//listLaboratoryTestProfile


	private void listAutoAssignmentCriteriaLOV() throws Exception {
		
		LOGGER.info("listAutoAssignmentCriteriaLOV start");
		StringBuilder result = new StringBuilder();
		Collection list = new ArrayList();

		//populate auto-asignment criteria LOV list
		list.add(new NameValuePair(IUMConstants.CRITERIA_AGENT, IUMConstants.CRITERIA_AGENT));
		list.add(new NameValuePair(IUMConstants.CRITERIA_ASSIGNED_TO, IUMConstants.CRITERIA_ASSIGNED_TO));
		list.add(new NameValuePair(IUMConstants.CRITERIA_BRANCH, IUMConstants.CRITERIA_BRANCH));
		list.add(new NameValuePair(IUMConstants.CRITERIA_LOB, IUMConstants.CRITERIA_LOB));

		if (list != null) {
			Iterator iterator = list.iterator();

			while (iterator.hasNext()) {
				NameValuePair nameValuePair = (NameValuePair)iterator.next();
				String code = nameValuePair.getValue();
				String desc = nameValuePair.getName();

				if (selectedItem != null && selectedItem.equals(code)) {
					result.append("<option value= '").append(code).append("' selected>").append(desc).append("</option>");
				}
				else {
					result.append("<option value= '").append(code).append("' >").append(desc).append("</option>");
				}
			}
		}
		result = result.insert(0,"<option value=''>Select Criteria Code</option>");
		if (disabled.equals("") || disabled.equals("false")) {
			result = result.insert(0,new StringBuilder("<select class='").append(className).append("' name= '").append(listBoxName).append("' style= '").append(styleName).append("' onChange= '").append(onChange).append("' onFocus= '").append(onFocus).append("'>"));
		}
		else {
			result = result.insert(0,new StringBuilder("<select class='").append(className).append("' name= '").append(listBoxName).append("' style= '").append(styleName).append("' disabled= '").append(disabled).append("' onChange= '").append(onChange).append("' onFocus= '").append(onFocus).append("'>"));
		}
		result = result.append( "</select>");
		pageContext.getOut().println(result);
		LOGGER.info("listAutoAssignmentCriteriaLOV end");
	}//listAutoAssignmentCriteriaLOV


	private void listRecordTypes() throws Exception {
		
		LOGGER.info("listRecordTypes start");
		StringBuilder result = new StringBuilder();
		CodeHelper codeHelper = new CodeHelper();
		Collection exceptionTypes = new ArrayList();

		exceptionTypes.add(new NameValuePair("Policy Queue", "QPQI"));
		exceptionTypes.add(new NameValuePair("Kick Out", "QCKO"));
		exceptionTypes.add(new NameValuePair("Client Data Sheet", "QCRI"));
		exceptionTypes.add(new NameValuePair("Requirement Maintenance", "QRQM"));
		exceptionTypes.add(new NameValuePair("Requirement Inquiry", "QRRI"));
		exceptionTypes.add(new NameValuePair("Auto Settle", "QPAS"));

		if (exceptionTypes!= null) {
			Iterator it = exceptionTypes.iterator();

			while (it.hasNext()){
				NameValuePair nv = (NameValuePair) it.next();
				String code = nv.getValue();
				String desc = nv.getName();

				if (selectedItem != null && selectedItem.equalsIgnoreCase(code)){
					result.append("<option value= '").append(code).append("' selected>").append(desc).append("</option>");
				}
				else{
					result.append("<option value= '").append(code).append("'>").append(desc).append("</option>");
				}
			}
		}
		result = result.insert(0,"<option value=''></option>");
		if (disabled.equals("") || disabled.equals("false")) {
			result = result.insert(0,new StringBuilder("<select class='").append(className).append("' name= '").append(listBoxName).append("' style= '").append(styleName).append("' onChange= '").append(onChange).append("' onFocus= '").append(onFocus).append("'>"));
		}
		else {
			result = result.insert(0,new StringBuilder("<select class='").append(className).append("' name= '").append(listBoxName).append("' style= '").append(styleName).append("' disabled= '").append(disabled).append("' onChange= '").append(onChange).append("' onFocus= '").append(onFocus).append("'>"));
		}
		result = result.append( "</select>");
		pageContext.getOut().println(result);
		LOGGER.info("listRecordTypes end");
	}//listRecordTypes


	private void listRanks() throws Exception {
		
		LOGGER.info("listRanks start");
		StringBuilder result = new StringBuilder();
		Reference ref = new Reference();
		ArrayList ranks = ref.getExamRanks();

		if (ranks != null){
			for(int i=0; i<ranks.size(); i++){
				RankData nameValuePair = (RankData) ranks.get(i);
				String code = String.valueOf(nameValuePair.getRankCode());
				String desc = nameValuePair.getRankDesc();

				if (desc == null) {
					desc = "";
				}

				if (selectedItem != null && selectedItem.equals(code)) {
					result.append("<option value= '").append(code).append("' selected>").append(desc).append("</option>");
				}
				else {
					result.append("<option value= '").append(code).append("' >").append(desc).append("</option>");
				}
			}
		}
		result = result.insert(0,"<option value=''>Select Rank</option>");
		if (disabled.equals("") || disabled.equals("false")) {
			result = result.insert(0,new StringBuilder("<select class='").append(className).append("' name= '").append(listBoxName).append("' style= '").append(styleName).append("' onChange= '").append(onChange).append("' onFocus= '").append(onFocus).append("'>"));
		}
		else {
			result = result.insert(0,new StringBuilder("<select class='").append(className).append("' name= '").append(listBoxName).append("' style= '").append(styleName).append("' disabled= '").append(disabled).append("' onChange= '").append(onChange).append("' onFocus= '").append(onFocus).append("'>"));
		}
		result = result.append( "</select>");
		pageContext.getOut().println(result);
		LOGGER.info("listRanks end");
	}//listRanks


	private void listSpecializations() throws Exception{
		
		LOGGER.info("listSpecializations start");
		StringBuilder result = new StringBuilder();
		Reference ref = new Reference();
		ArrayList specializations = ref.getSpecializations();

		if (specializations != null) {
			for(int i=0; i<specializations.size(); i++){
				SpecializationData nameValuePair = (SpecializationData) specializations.get(i);
				String id = String.valueOf(nameValuePair.getSpecializationId());
				String desc = nameValuePair.getSpecializationDesc();
				String code = id+"~"+desc;
				if(desc == null) desc = "";

				if (selectedItem != null && selectedItem.equals(code)) {
					result.append("<option value= '").append(code).append("' selected>").append(desc).append("</option>");
				}
				else {
					result.append("<option value= '").append(code).append("' >").append(desc).append("</option>");
				}
			}
		}
		result = result.insert(0,"<option value=''>Select Specialization</option>");
		if (disabled.equals("") || disabled.equals("false")) {
			result = result.insert(0,new StringBuilder("<select class='").append(className).append("' name= '").append(listBoxName).append("' style= '").append(styleName).append("' onChange= '").append(onChange).append("' onFocus= '").append(onFocus).append("'>"));
		}
		else {
			result = result.insert(0,new StringBuilder("<select class='").append(className).append("' name= '").append(listBoxName).append("' style= '").append(styleName).append("' disabled= '").append(disabled).append("' onChange= '").append(onChange).append("' onFocus= '").append(onFocus).append("'>"));
		}
		result = result.append( "</select>");
		pageContext.getOut().println(result);
		LOGGER.info("listSpecializations end");
	}//listSpecializations


	private void listExaminationPlaces() throws Exception{
		
		LOGGER.info("listExaminationPlaces start");
		StringBuilder result = new StringBuilder();
		Reference ref = new Reference();
		ArrayList examPlaces = ref.getExamPlaces();

		if (examPlaces!= null) {
			for(int i=0; i<examPlaces.size(); i++){
				ExaminationPlaceData nameValuePair = (ExaminationPlaceData) examPlaces.get(i);
				String code = String.valueOf(nameValuePair.getExaminationPlaceId());
				String desc = nameValuePair.getExaminationPlaceDesc();
				if(desc == null) desc = "";

				if (selectedItem != null && selectedItem.equals(code)) {
					result.append("<option value= '").append(code).append("' selected>").append(desc).append("</option>");
				}
				else {
					result.append("<option value= '").append(code).append("' >").append(desc).append("</option>");
				}
			}
		}
		result = result.insert(0,"<option value=''>Select Examination Place</option>");
		if (disabled.equals("") || disabled.equals("false")) {
			result = result.insert(0,new StringBuilder("<select class='").append(className).append("' name= '").append(listBoxName).append("' style= '").append(styleName).append("' onChange= '").append(onChange).append("' onFocus= '").append(onFocus).append("'>"));
		}
		else {
			result = result.insert(0,new StringBuilder("<select class='").append(className).append("' name= '").append(listBoxName).append("' style= '").append(styleName).append("' disabled= '").append(disabled).append("' onChange= '").append(onChange).append("' onFocus= '").append(onFocus).append("'>"));
		}
		result = result.append( "</select>");
		pageContext.getOut().println(result);
		LOGGER.info("listExaminationPlaces end");
	}//listExaminationPlaces


	private void listInterfaceTypes() throws Exception {
		
		LOGGER.info("listInterfaceTypes start");
		StringBuilder result = new StringBuilder();
		CodeHelper codeHelper = new CodeHelper();
		Collection interfaceTypes = new ArrayList();

		interfaceTypes.add(new NameValuePair("ABACUS", "ABACUS"));
		interfaceTypes.add(new NameValuePair("INGENIUM", "INGENIUM"));
		interfaceTypes.add(new NameValuePair("PRISM", "PRISM"));
		interfaceTypes.add(new NameValuePair("LWS", "LWS"));
		interfaceTypes.add(new NameValuePair("MIB", "MIB"));
		interfaceTypes.add(new NameValuePair("SMTP", "SMTP"));

		if (interfaceTypes != null) {
			Iterator it = interfaceTypes.iterator();

			while (it.hasNext()){
				NameValuePair nv = (NameValuePair) it.next();
				String code = nv.getValue();
				String desc = nv.getName();

				if (selectedItem != null && selectedItem.equalsIgnoreCase(code)){
					result.append("<option value= '").append(code).append("' selected>").append(desc).append("</option>");
				}
				else {
					result.append("<option value= '").append(code).append("'>").append(desc).append("</option>");
				}
			}
		}
		result = result.insert(0,"<option value=''></option>");
		if (disabled.equals("") || disabled.equals("false")) {
			result = result.insert(0,new StringBuilder("<select class='").append(className).append("' name= '").append(listBoxName).append("' style= '").append(styleName).append("' onChange= '").append(onChange).append("' onFocus= '").append(onFocus).append("'>"));
		}
		else {
			result = result.insert(0,new StringBuilder("<select class='").append(className).append("' name= '").append(listBoxName).append("' style= '").append(styleName).append("' disabled= '").append(disabled).append("' onChange= '").append(onChange).append("' onFocus= '").append(onFocus).append("'>"));
		}
		result = result.append( "</select>");
		pageContext.getOut().println(result);
		LOGGER.info("listInterfaceTypes end");
	}//listInterfaceTypes

	private void listPurgeCriteria() throws Exception {
		
		LOGGER.info("listPurgeCriteria start");
		StringBuilder result = new StringBuilder();

		Collection purgeCriteria = new ArrayList();
		NameValuePair pair = new NameValuePair();
		pair.setName(IUMConstants.PURGE_ASSESSMENT_REQUESTS);
		pair.setValue(IUMConstants.PURGE_ASSESSMENT_REQUESTS);
		purgeCriteria.add(pair);
		pair = new NameValuePair();
		pair.setName(IUMConstants.PURGE_MEDICAL_RECORDS);
		pair.setValue(IUMConstants.PURGE_MEDICAL_RECORDS);
		purgeCriteria.add(pair);
		pair = new NameValuePair();
		pair.setName(IUMConstants.PURGE_AUDIT_TRAIL_LOGS);
		pair.setValue(IUMConstants.PURGE_AUDIT_TRAIL_LOGS);
		purgeCriteria.add(pair);
		pair = new NameValuePair();
		pair.setName(IUMConstants.PURGE_ACTIVITY_LOGS);
		pair.setValue(IUMConstants.PURGE_ACTIVITY_LOGS);
		purgeCriteria.add(pair);

		Iterator it = purgeCriteria.iterator();
		while (it.hasNext()) {
			NameValuePair nameValuePair = (NameValuePair) it.next();
			String code = nameValuePair.getName();
			String desc = nameValuePair.getValue();
			if (selectedItem != null && selectedItem.equals(code)) {
				result = result.append("<option value= '").append(code).append("' selected>").append(desc).append("</option>" );
			}
			else {
				result = result.append("<option value= '").append(code).append("'>").append(desc).append("</option>");
			}
		}
		result = result.insert(0,"<option value=''>Select Criteria</option>");
		if (disabled.equals("") || disabled.equals("false")) {
			result = result.insert(0,new StringBuilder("<select class='").append(className).append("' name= '").append(listBoxName).append("' style= '").append(styleName).append("' onChange= '").append(onChange).append("' onFocus= '").append(onFocus).append("'>"));
		}
		else {
			result = result.insert(0,new StringBuilder("<select class='").append(className).append("' name= '").append(listBoxName).append("' style= '").append(styleName).append("' disabled= '").append(disabled).append("' onChange= '").append(onChange).append("' onFocus= '").append(onFocus).append("'>"));
		}
		result = result.append( "</select>");
		pageContext.getOut().println(result);
		LOGGER.info("listPurgeCriteria end");
	}//listDocumentType



	private void closeConnection(Connection conn){
		
		
		if(conn!=null){
			try{
				conn.close();
			}catch(SQLException e){
				e.printStackTrace();
			}
		}
		
	}
}