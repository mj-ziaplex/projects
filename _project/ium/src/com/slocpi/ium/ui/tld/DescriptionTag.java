package com.slocpi.ium.ui.tld;

//struts package
// java package
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import javax.servlet.jsp.JspTagException;
import javax.servlet.jsp.tagext.TagSupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.slocpi.ium.data.dao.StatusCHDao;
import com.slocpi.ium.data.util.DataSourceProxy;
import com.slocpi.ium.util.CodeHelper;
import com.slocpi.ium.util.IUMConstants;
import com.slocpi.ium.util.NameValuePair;


/**
 * Renders information as a listbox.
 * @author: Ma. Theresa Vicencio
 * @version: 1.0
 */
public class DescriptionTag extends TagSupport {
	private static final Logger LOGGER = LoggerFactory.getLogger(DescriptionTag.class);
	private String queryCode = "";
	private String type = "";


	/**
	 * Instantiates the object
	 */
	public DescriptionTag() {
		super();
	}


	/**
	 * Limits the selection of data specified.
	 * @param code country code
	 */
	public void setQueryCode(String code) {
		queryCode = code;
	}
	 

	/**
	 * Sets the type of description to be displayed
	 * @param code source type
	 */
	public void setType(String code) {
		type = code;
	}


	/**
	 * Implements the TagSupport method.
	 * @return int
	 */
	public int doStartTag() {
		return SKIP_BODY;
	}


	/**
	 * Implements the TagSupport method that retrieves the description of data and render in a the page.
	 * @return int
	 */
	public int doEndTag() throws JspTagException {
		
		LOGGER.info("doEndTag start");
		try {
			if (type.equals(IUMConstants.LIST_LOB)) {
				displayLOB();
			}
			else if (type.equals(IUMConstants.LIST_CLIENT_TYPE)) {
				displayClientType();
			}
			else if (type.equals(IUMConstants.LIST_SOURCE_SYSTEM)) {
				displaySourceSystem();
			}
			else if (type.equals(IUMConstants.LIST_USERS)) {
				displayName();
			}
			else if (type.equals(IUMConstants.LIST_BRANCHES)) {
				displayBranch();
			}
			else if (type.equals(IUMConstants.LIST_SEX)) {
			  displaySex();
			}
			else if (type.equals(IUMConstants.LIST_PC_STATUS)) {
			  displayStatus();
			}
			else if (type.equals(IUMConstants.LIST_NOTIFICATION_TEMPLATES)) {
			  displayTemplateName();
			}
			else if (type.equals(IUMConstants.LIST_TEST_PROFILES)) {
			  displayTestName();
			}
			else if (type.equals(IUMConstants.LIST_ROLES)) {
			  displayRole();
			}
			else if(type.equals(IUMConstants.LIST_AUTO_ASSIGN_CRITERIA)){
			  displayAutoAssignmentCriteria();
			}
			else if (type.equals(IUMConstants.LIST_TRANSACTION_RECORD)){
			  displayTransactionRecords();
			}
			else {
				LOGGER.warn("Unidentified list type or none specified");
				throw new Exception("Unidentified list type or none specified");
			}
		}
		catch (Exception e) {
			LOGGER.error(CodeHelper.getStackTrace(e));
			throw new JspTagException (e.getMessage());
		}
		LOGGER.info("doEndTag end");
		return EVAL_PAGE;
	}
	  
	  
	/**
	 * 
	 */
	private void displayTemplateName() throws IOException {	
		
		LOGGER.info("displayTemplateName start");
		String result = "";
		
		CodeHelper codeHelper = new CodeHelper();
		Collection statusList = codeHelper.getCodeValue("notificationTemplates", queryCode);
		if (statusList.size() > 0) {
			NameValuePair nameValuePair = (NameValuePair)((ArrayList) statusList).get(0);
			String desc = nameValuePair.getName();
			result = desc;		
		}
		
		
		pageContext.getOut().println(result); 
		LOGGER.info("displayTemplateName end");
	}
	
	private void displayTestName() throws IOException {		
		
		LOGGER.info("displayTestName start");
		String result = "";
		
		CodeHelper codeHelper = new CodeHelper();
		Collection statusList = codeHelper.getCodeValue("testProfiles", queryCode);
		if (statusList.size() > 0) {
			NameValuePair nameValuePair = (NameValuePair)((ArrayList) statusList).get(0);
			String desc = nameValuePair.getName();
			result = desc;		
		}
				
		pageContext.getOut().println(result); 
		LOGGER.info("displayTestName end");
	}


	/**
	 * 
	 */
	private void displayStatus() throws SQLException, IOException {		
		
		LOGGER.info("displayStatus start");
		String result = "";
		
		StatusCHDao statDAO = new StatusCHDao();
		result = statDAO.getDescription(queryCode);
		pageContext.getOut().println(result);
		
		LOGGER.info("displayStatus end");
	}


	private void displayLOB() throws Exception {
		
		LOGGER.info("displayLOB start");
		String result = "";
		
		CodeHelper codeHelper = new CodeHelper();
		Collection statusList = codeHelper.getCodeValue("lob", queryCode);
		if (statusList.size() > 0) {
			NameValuePair nameValuePair = (NameValuePair)((ArrayList) statusList).get(0);
			String desc = nameValuePair.getName();
			result = desc;		
		}
		
		pageContext.getOut().println(result);
		LOGGER.info("displayLOB end");
	}	  
	  

	private void displayClientType() throws Exception {
		
		LOGGER.info("displayClientType start");
		String result = "";
		
		CodeHelper codeHelper = new CodeHelper();
		Collection statusList = codeHelper.getCodeValue("clientType", queryCode);
		if (statusList.size() > 0) {
			NameValuePair nameValuePair = (NameValuePair)((ArrayList) statusList).get(0);
			String desc = nameValuePair.getName();
			result = desc;		
		}
		
		pageContext.getOut().println(result);
		LOGGER.info("displayClientType end");    	
	}
	
	  
	private void displaySourceSystem() throws Exception {
		
		LOGGER.info("displaySourceSystem start");
		String result = "";
		
		HashMap list = new HashMap();

		list.put(IUMConstants.SYSTEM_ABACUS, IUMConstants.SYSTEM_ABACUS_DESC);
		list.put(IUMConstants.SYSTEM_GLASS, IUMConstants.SYSTEM_GLASS_DESC);
		list.put(IUMConstants.SYSTEM_PRISM, IUMConstants.SYSTEM_PRISM_DESC);
		list.put(IUMConstants.SYSTEM_INGENIUM, IUMConstants.SYSTEM_INGENIUM_DESC);
		list.put("", "NONE");
		
		if (list.size() > 0) {			
			String desc = (String)list.get(queryCode);
			if (desc != null) {
				result = desc;	
			}
		}
		
		pageContext.getOut().println(result);    
		LOGGER.info("displaySourceSystem end");	
	}


	private void displayName() throws Exception {
		
		LOGGER.info("displayName start");
		String result = "";
		
		CodeHelper codeHelper = new CodeHelper();
		Collection statusList = codeHelper.getCodeValue("users", queryCode);

		if (statusList.size() > 0) {
			NameValuePair nameValuePair = (NameValuePair)((ArrayList) statusList).get(0);			
			String desc = nameValuePair.getName();
			result = desc;
		}
		
		pageContext.getOut().println(result);   
		LOGGER.info("displayName end");   	
	}


	private void displayBranch() throws Exception {
		
		LOGGER.info("displayBranch start");
		String result = "";
		
		CodeHelper codeHelper = new CodeHelper();
		Collection statusList = codeHelper.getCodeValue("branch", queryCode);
		if (statusList.size() > 0) {
			NameValuePair nameValuePair = (NameValuePair)((ArrayList) statusList).get(0);
			String desc = nameValuePair.getName();
			result = desc;
		}
		
		pageContext.getOut().println(result);   
		
		LOGGER.info("displayBranch end");
	}


	private void displaySex() throws Exception {
		
		LOGGER.info("displaySex start");
		String result = "";
		
		HashMap list = new HashMap();

		list.put("M", "Male");
		list.put("F", "Female");
		
		if (list.size() > 0) {			
			String desc = (String)list.get(queryCode);
			if (desc != null) {
				result = desc;	
			}
		}
		
		pageContext.getOut().println(result);
		LOGGER.info("displaySex end");  	
	}


	private void displayRole() throws Exception {
		
		LOGGER.info("displayRole start");
		String result = "";
		
		CodeHelper codeHelper = new CodeHelper();
		Collection roleList = codeHelper.getCodeValue("roles", queryCode);
		if (roleList.size() > 0) {
			NameValuePair nameValuePair = (NameValuePair)((ArrayList) roleList).get(0);
			String desc = nameValuePair.getName();
			result = desc;
		}
		
		pageContext.getOut().println(result);   
		LOGGER.info("displayRole end");
	}


	private void displayAutoAssignmentCriteria() throws Exception {
		
		LOGGER.info("displayAutoAssignmentCriteria start");
		
		String result = "";
		
		CodeHelper codeHelper = new CodeHelper();
		Collection statusList = codeHelper.getCodeValue("autoAssignmentCriteria", queryCode);
		if (statusList.size() > 0) {
			NameValuePair nameValuePair = (NameValuePair)((ArrayList) statusList).get(0);
			String desc = nameValuePair.getName();
			result = desc;		
		}
		
		pageContext.getOut().println(result);
		LOGGER.info("displayAutoAssignmentCriteria end"); 	
	}
		


	private void closeConnection(Connection conn)throws SQLException{
		
		
		if(conn!=null){
			conn.close();
		}
		
	}

	private void displayTransactionRecords() throws Exception {
		
		LOGGER.info("displayTransactionRecords start");
		String result = "";
		
		HashMap tableMapping = new HashMap();
		
		tableMapping.put("1","Assessment Requests");
		tableMapping.put("2","Policy Requirements");
		tableMapping.put("3","Impairments");
		tableMapping.put("4","Medical Records");
		tableMapping.put("5","User Profiles");
		tableMapping.put("6","User Access");
		tableMapping.put("7","Requirements");
		tableMapping.put("8","Requirement Forms");
		tableMapping.put("9","Lines of Business");
		tableMapping.put("10","Departments");
		tableMapping.put("11","Sections");
		tableMapping.put("12","Specializations");
		tableMapping.put("13","Examination Places");
		tableMapping.put("14","Examination Areas");
		tableMapping.put("15","Document Types");
		tableMapping.put("16","Client Types");
		tableMapping.put("17","Laboratory Tests");
		tableMapping.put("18","Auto-assign Criteria");
		tableMapping.put("19","Status Codes");
		tableMapping.put("20","Laboratories");
		tableMapping.put("21","Test Profiles");
		tableMapping.put("22","Examiners");
		tableMapping.put("23","Ranks");
		tableMapping.put("24","Branches");
		tableMapping.put("25","Underwriting Auto-assign Criteria");
		tableMapping.put("26","Process Configurations");
		tableMapping.put("27","Holidays");
		tableMapping.put("28","Roles");
		tableMapping.put("29","Notification Templates");
		tableMapping.put("30","MIB Export");
		tableMapping.put("31","Auto-expire Medical Records");
		tableMapping.put("32","Archive Audit Trails");
		tableMapping.put("33","MIB Impairments");
		tableMapping.put("34","MIB Actions");
		tableMapping.put("35","MIB Numbers");
		tableMapping.put("36","MIB Letters");
		tableMapping.put("37","MIB Export Schedule");
		tableMapping.put("38","Auto-expire Schedule");
		tableMapping.put("39","Medical Bills");
		tableMapping.put("40","Accesses");
		tableMapping.put("41","Pages");
		tableMapping.put("42","Access Templates");
		tableMapping.put("43","Access Template Details");
		tableMapping.put("44","Examiner Specializations");
		tableMapping.put("45","Notification Recipients");
		tableMapping.put("46","LOB Requirements");
		tableMapping.put("47","LOB Documents");
		tableMapping.put("48","LOB Status");

		if (tableMapping.size() > 0) {			
			String desc = (String)tableMapping.get(queryCode);
			if (desc != null) {
				result = desc;	
			}
		}
		
		pageContext.getOut().println(result);
		LOGGER.info("displayTransactionRecords end"); 	
	}
}