<%String contextPath = request.getContextPath();%>
<script language="Javascript">
   if (parent.document.forms[0]) {
       parent.window.location = "<%=contextPath%>/expiredSession.do";
   }
   if(window.opener != null && opener.document.forms[0] != null){
	   if (opener.document.forms[0]) {
    		opener.window.location.href = "<%=contextPath%>/expiredSession.do";
			opener.window.focus();
		    window.close();
		}
	}
</script>

<%@ taglib uri="/struts-html.tld" prefix="html" %>
<%@page language="java" import="java.util.*" %>
<%
/* Andre Ceasar Dacanay - css enhancement - start */
String iumCss = "";
String companyCode = "";
try {
	ResourceBundle rbWMSSP = ResourceBundle.getBundle("dbconfig");
	iumCss = rbWMSSP.getString("IUM_CSS_VERSION");
	companyCode = iumCss;
	iumCss = "_"+iumCss;
} catch (Exception e){}
if(iumCss.equals("_CP")){
	companyCode="";
}else if(iumCss.equals("_GF")){
	companyCode = companyCode+" ";
}

/* Andre Ceasar Dacanay - css enhancement - end */
%>
<html>
  <head>
    <title><%=companyCode%>Sun Life Financial - Philippines</title>
    <link href="<%=contextPath%>/css/pw_main.css" rel="stylesheet" type="text/css">
    <link href="<%=contextPath%>/css/main.css" rel="stylesheet" type="text/css">
    <script language="JavaScript" src="<%=contextPath%>/js/navigation.js"></script>
    <script language="JavaScript" src="<%=contextPath%>/js/validation.js"></script>
    <script language="JavaScript" src="<%=contextPath%>/js/login.js"></script>
  </head>
  
  <body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="document.frm.agentCode.focus();">
  
    <!--- START OF HEADER -->
    <table cellspacing="0" cellpadding="0" border="0" width="100%" background="<%=contextPath%>/images/filler.gif">
      <tr>
        <td><img src="<%=contextPath%>/images/head_logo.gif" border="0"></td>
        <td align="right" valign="top" colspan="6"><img src="<%=contextPath%>/images/head_clouds.jpg"></td>
      </tr>
    </table>
    <!--- END OF HEADER -->

    <table cellspacing="0" cellpadding="0" border="0" width="100%">
      <tr>
        <td width="147" height="800" background="<%=contextPath%>/images/bg_yellow.gif" valign="top" >
          <table cellspacing="0" cellpadding="0" border="0" width="147">
            <tr>
              <td colspan="2" class="labelmenu" height="8">&nbsp;</td>
            </tr>
            <tr>
              <td colspan="2"class="labelmenu" width="147" align="center">&nbsp;</td>
            </tr>
            <tr>
              <td colspan="2" height="8">&nbsp;</td>
            </tr>
            <tr>
              <td class="labelmenu" width="20" height="8">&nbsp;</td>
              <td class="labelmenu" width="127" height="8">&nbsp;</td>
            </tr>
            <tr>
              <td colspan="2" height="8">&nbsp;</td>
            </tr>
            <tr>
              <td class="labelmenu" width="20" height="8">&nbsp;</td>
              <td class="labelmenu" width="127" height="8">&nbsp;</td>
            </tr>
            <tr>
              <td colspan="2" height="8">&nbsp;</td>
            </tr>
            <tr>
              <td class="labelmenu" width="20" height="8">&nbsp;</td>
              <td class="labelmenu" width="127" height="8">&nbsp;</td>
            </tr>
          </table>
        </td>
        <td width="100%" valign="top">
        
          <!--- START OF SUB-HEADER -->
          <table cellspacing="0" cellpadding="0" border="0" width="100%">
            <tr>
              <td class="label2" background="<%=contextPath%>/images/GradientGray.jpg" height="27" width="100%" valign="top">&nbsp;&nbsp;
                <span class="main">Integrated Underwriting and Medical System</span>
              </td>
            </tr>
            <tr>
              <td><img src="<%=contextPath%>/images/spacer.gif" border="0" height="10"></td>
            </tr>
            <tr>
              <td class="title3"><b>&nbsp;Login Page</b></td>
            </tr>
            <tr>
              <td colspan="10" width="800"><hr NOSHADE></TD>
            </tr>
          </table>
          <!--- END  OF SUB-HEADER -->
			
          <!--- START OF BODY -->
          <form name="frm" method="post" action="login.do" focus="agentCode">
            <input type="hidden" name="task" value="login">
            <table cellspacing="0" cellpadding="0" border="0">
              <tr>
                <td class="label1"><img src="<%=contextPath%>/images/spacer.gif" border="0" width="10"></td>
                <td colspan="2"><html:errors/></td>
              </tr>
              <tr>
                <td colspan="2"><img src="<%=contextPath%>/images/spacer.gif" border="0" height="10"></td>
              </tr>
              <tr>
                <td colspan="2"><img src="<%=contextPath%>/images/spacer.gif" border="0" height="10"></td>
                <td rowspan="10">
                  <table cellspacing="0" cellpadding="0" border="0">
                    <tr>
                      <td align="right" width="10"><img src="<%=contextPath%>/images/border.jpg" border="0" height="180" width="30"></td>
                      <td><img src="<%=contextPath%>/images/agentfront.jpg" border="0" ></td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr>
                <td class="label1"><img src="<%=contextPath%>/images/spacer.gif" border="0" width="10"></td>
                <td class="label2"><b>User ID</b></td>
              </tr>
              <tr>
                <td class="label1"><img src="<%=contextPath%>/images/spacer.gif" border="0" width="10"></td>
                <td class="label1"><input type="text" class="inputtext1" name="agentCode" ></td>
              </tr>
              <tr>
                <td colspan="2"><img src="<%=contextPath%>/images/spacer.gif" border="0" height="10"></td>
              </tr>
              <tr>
                <td class="label1"><img src="<%=contextPath%>/images/spacer.gif" border="0" width="10"></td>
                <td class="label2"><b>Password</b></td>
              </tr>
              <tr>
                <td class="label1"><img src="<%=contextPath%>/images/spacer.gif" border="0" width="10"></td>
                <td class="label1"><input type="password" class="inputtext1" name="password" ></td>
              </tr>
              <tr>
                <td colspan="2"><img src="<%=contextPath%>/images/spacer.gif" border="0" height="10"></td>
              </tr>
              <tr>
                <td class="label1"><img src="<%=contextPath%>/images/spacer.gif" border="0" width="10"></td>
                <td class="label1"><input type="submit" class="button1" value="Login" onClick="return(isValid());"></td>
              </tr>
              <tr>
                <td colspan="3"><img src="<%=contextPath%>/images/spacer.gif" border="0" height="10"></td>
              </tr>
            </table>
            <input type="hidden" name="userID" value="<%=request.getAttribute("userID")%>">
            <input type="hidden" name="attempts" value="<%=request.getAttribute("attempts")==null?"0":(String)request.getAttribute("attempts")%>">            
          </form>
          <!--- END OF BODY -->
          
          <table>
            <tr>
              <td><img src="<%=contextPath%>/images/spacer.gif" border="0" height="10"></td>
            </tr>
            <tr>
              <td class="label2">Please note that User IDs and Passwords are case sensitive.</td>
            </tr>
            <tr>
              <td class="label2bold">Forgot your password? </td>
            </tr>
            <tr>
              <td class="label2">Call HELPDESK 8866188 loc. 4000 or 1-800-1888-4000 (toll-free)</td>
            </tr>
            <tr>
              <td><img src="<%=contextPath%>/images/spacer.gif" border="0" height="40"></td>
            </tr>
            <tr>
              <td colspan="10" width="800"><hr NOSHADE></TD>
            </tr>
            <tr>
              <td colspan="10" class="label5">� 2003 Sun Life Financial. All rights reserved.</td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
  </body>
</html>