<%@ page language="java" buffer="1024kb"%>
<!-- JAVA UTILS -->
<%@ page import="java.util.ResourceBundle"%>

<%
	String contextPath = request.getContextPath();
%>
<%
	/* Andre Ceasar Dacanay - css enhancement - start */
	String iumCss = "";
	String iumColorO = "";
	String companyCode = "";
	try {
		ResourceBundle rbWMSSP = ResourceBundle.getBundle("dbconfig");
		iumCss = rbWMSSP.getString("IUM_CSS_VERSION");
		companyCode = iumCss;
		iumCss = "_" + iumCss;
	} catch (Exception e) {
	}
	if (iumCss.equals("_CP")) {
		iumColorO = "#FFCB00";
		companyCode = "";
	} else if (iumCss.equals("_GF")) {
		iumColorO = "#303A77";
		companyCode = companyCode + " ";
	} else {
		iumColorO = "#FFCB00";
	}

	/* Andre Ceasar Dacanay - css enhancement - end */
%>
<%
	ResourceBundle rb1 = ResourceBundle.getBundle("wmsconfig");
	String wmsContextPath1 = rb1.getString("wms_context_path");

	String HEADER_WIDTH = "";
	String TABLE_WIDTH = "";
	String GRAY_WIDTH = "";

	if (contextPath.equalsIgnoreCase(wmsContextPath1)) {
		HEADER_WIDTH = "800";
		TABLE_WIDTH = "800";
		GRAY_WIDTH = "780";
	} else {
		HEADER_WIDTH = "100%";
		TABLE_WIDTH = "100%";
		GRAY_WIDTH = "100%";
	}
%>
<html>
<head>
	<title><%=companyCode%>Integrated Underwriting and Medical System</title>
	<link href="<%=contextPath%>/css/main.css" rel="stylesheet" type="text/css" />
	<script language="Javascript" src="<%=contextPath%>/js/navigation.js"></script>
</head>
<body leftmargin="0" topmargin="0">
	<table border="0" cellpadding="0" cellspacing="0" height="100%" width="<%=HEADER_WIDTH%>">
		<tr>
			<td width="100%" colspan="2"><jsp:include page="header.jsp" flush="true" /></td>
		</tr>
		<tr>
			<td rowspan="2" bgcolor="<%=iumColorO%>" height="100%" width="20">
				<img src="<%=contextPath%>/images/spacer.gif" width="20">
			</td>
			<td background="<%=contextPath%>/images/GradientGray.jpg" height="27" width="<%=GRAY_WIDTH%>" valign=top>
				&nbsp;&nbsp; <span class="main">Integrated Underwriting and Medical System</span>
			</td>
		</tr>
		<tr>
			<td width="<%=TABLE_WIDTH%>" height="100%" valign="top">
				<!-- BODY -->
				<table border="0" cellpadding="10" cellspacing="0" width="<%=TABLE_WIDTH%>">
					<tr>
						<td>
							<table border="0" cellpadding="10" cellspacing="0" width="<%=TABLE_WIDTH%>" height="500">
								<tr>
									<td valign="top">
										<table border="0" cellpadding="0" cellspacing="0" width="<%=TABLE_WIDTH%>">
											<tr>
												<td class="label1">Sorry, you do not have access to this page.</td>
											</tr>
											<tr>
												<td class="label5">&nbsp;</td>
											</tr>
											<tr>
												<td class="label5">&nbsp;</td>
											</tr>
											<tr>
												<td class="label5">
													<a href="javascript: history.back()">Back to previous page</a>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td colspan="10" class="label5">� 2003 Sun Life Financial. All rights reserved.</td>
					</tr>
				</table> <!-- BODY -->
			</td>
		</tr>
	</table>
</body>
</html>
