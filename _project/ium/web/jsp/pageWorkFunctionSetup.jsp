<%@ page language="java" import="java.util.*,com.slocpi.ium.data.*, com.slocpi.ium.ui.form.*, com.slocpi.ium.util.*, com.slocpi.ium.ui.util.*, java.text.* " buffer="12kb"%>
<%@ taglib uri="/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/struts-html.tld" prefix="html" %>
<%@ taglib uri="/ium.tld" prefix="ium" %>
<%String contextPath = request.getContextPath();%>
<%
/* Andre Ceasar Dacanay - css enhancement - start */
String iumCss = "";
String iumColorB = "";
String companyCode = "";
try {
	ResourceBundle rbWMSSP = ResourceBundle.getBundle("dbconfig");
	iumCss = rbWMSSP.getString("IUM_CSS_VERSION");
	companyCode = iumCss;
	iumCss = "_"+iumCss;
} catch (Exception e){}
if(iumCss.equals("_CP")){
	iumColorB="#2A4C7C";
	companyCode="";
}else if(iumCss.equals("_GF")){
	iumColorB="#303A77";
	companyCode = companyCode+" ";
}else{
	iumColorB="#2A4C7C";
}
/* Andre Ceasar Dacanay - css enhancement - end */
%>
<jsp:useBean id="workFunctionForm" scope="request" class="com.slocpi.ium.ui.form.WorkFunctionForm" />
<%
    // This routine queries the access rights of the users (RCRIVERA)
    //
    // --- START --- 
    
    StateHandler sessionHandler = new StateHandler();
    Access access = new Access();
	HashMap userAccess = sessionHandler.getUserAccess(request);
    if(!access.hasAccess(userAccess,IUMConstants.PAGE_WORK_FUNCTION,IUMConstants.ACC_INQUIRE) ){                                                                                                                    
    	response.sendRedirect("noAccessPage.jsp");   
    } 
    

    String MAINTAIN_ACCESS = "";
    
    if(!access.hasAccess(userAccess,IUMConstants.PAGE_WORK_FUNCTION,IUMConstants.ACC_MAINTAIN) ){                                                                                                                     
	  MAINTAIN_ACCESS = "DISABLED";
    }
    
    // --- END ---
 %>    


<html>
 <head>
    <title><%=companyCode%>Integrated Underwriting and Medical System</title>
    <link href="<%=contextPath%>/css/pw_main.css" rel="stylesheet" type="text/css">
    <link href="<%=contextPath%>/css/main<%=iumCss%>.css" rel="stylesheet" type="text/css">
    <link href="<%=contextPath%>/css/styleCommon.css" rel="stylesheet" type="text/css">
    <script language="JavaScript" src="<%=contextPath%>/js/dhtml_init.js"></script>
    <script language="JavaScript" src="<%=contextPath%>/js/navigation.js"></script>
    <script language="JavaScript" src="<%=contextPath%>/jsp/sl_left_nav.jsp"></script>

    <script language="Javascript">
    	  
    	function changeFlag(){
			var ctr = 0;
			while(ctr< document.workFunction.pageAccessCheckBox.length){
				if(document.workFunction.pageAccessCheckBox[ctr].checked){
    				document.workFunction.pageAccessResult[ctr].value = document.workFunction.pageAccessResult[ctr].value+"@y";
    			}
	    		else{
	    			document.workFunction.pageAccessResult[ctr].value = document.workFunction.pageAccessResult[ctr].value+"@n";
    			}
    			ctr++;
				
			}

    		document.workFunction.check.value = 'submit';

    		document.workFunction.submit();
	   	}
   		
       	function checkInquire(pageId,x,accessCount,i){

			var lastIndex = parseInt(i)*parseInt(accessCount)+parseInt(x);

	   		var index=0;
   			var flag = 0;
   			var curr_index = lastIndex;
   			index = parseInt(lastIndex) % parseInt(accessCount);
   			if(index == 0)
   				index = accessCount;
	   		lastIndex--;

   			if(x > <%=IUMConstants.ACC_INQUIRE%> && document.workFunction.pageAccessCheckBox[lastIndex].checked){
				while(index != <%=IUMConstants.ACC_INQUIRE%>){
					if(index != <%=IUMConstants.ACC_INQUIRE%>){
						index--;
						lastIndex--;

					}
				}
				if(!document.workFunction.pageAccessCheckBox[lastIndex].checked)			
					document.workFunction.pageAccessCheckBox[lastIndex].checked = true;


			}
			else if(x == <%=IUMConstants.ACC_INQUIRE%> && document.workFunction.pageAccessCheckBox[lastIndex].checked == false){
				index++;
				while(index<= accessCount){
					if(document.workFunction.pageAccessCheckBox[curr_index].checked){
						flag = 1;
					}
					curr_index++;
					index++;
					
				}
				if(flag==1){
					document.workFunction.pageAccessCheckBox[lastIndex].checked = true;					
					alert("Please uncheck the other access/es first before unchecking this.");
				}
			}

	   	}

    	function submitForm(){
    		document.workFunction.submit();
    	}
    	
    </script>
  </head>
  
  <body leftmargin="0" topmargin="0">
	<form name = "workFunction" method = "post" action = "<%=contextPath%>/viewWork.do">  
        <table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%">
        <tr>
          <td width="100%" colspan="2">
            <jsp:include page="header.jsp" flush="true"/>
            <table border="0" cellpadding="0" cellspacing="0" width="100%" height="100%">
              <tr valign="top">
                <td background="<%=contextPath%>/images/bg_yellow.gif" id="leftnav" valign="top" width="147">
                  <img height="9" src="<%=contextPath%>/images/sp.gif" width="147"><br>
                  <img height="30" src="<%=contextPath%>/images/sp.gif" width="7"> <br>
                  <img height="7" src="<%=contextPath%>/images/sp.gif" width="147"><br>
                  <div id="level0" style="position: absolute; visibility: hidden; width: 100%; z-index: 500"> 
                    <script language=javascript>
	                  <!--
                      document.write(writeMenu(''));
                      if (IE4) {
                        document.write(writeDiv());
                      };
                      //-->
                    </script>
                  </div>
                  <div id="NSFix" style="position: absolute; visibility: hidden; z-index: 999">&nbsp;</div>
                  <!-- Netscape needs a hard-coded div to write dynamic DIVs --> 
                  <script language=javascript>
                    <!--
                    if (NS4) {
                      document.write(writeDiv());
                    }
                    //-->
                  </script>
                  <script>
                    <!--
                    initMenu();
                    //-->
                  </script>
                </td>

                <td width="100%"> 
                  <div align="left"> 
                    <table border="0" cellpadding="0" width="100%" cellspacing="0">
                      <tr> 
                        <td width="100%"> 
                          <div align="left">
                            <table border="0" cellpadding="3" cellspacing="0" width="100%" height="27"  background="<%=contextPath%>/images/sectionbkgray.jpg">
                              <tr> 
                                <td valign="top"><span class="main">Integrated Underwriting and Medical System</span></td>
                              </tr>
                            </table>
                          </div>
                        </td>
                      </tr>
                    </table>
            
            <!-- BODY -->
            
			<table width ="100%" cellpadding="3" cellspacing="5" border="0">
			 <tr><td class="title2"><b>Work Functions</b></td></tr>
			 <tr>
<td colspan="4" align="left" >
	    		 <input type="hidden" value="display" name="check">
	    		 <input type="button" value="Save" class="button1" onclick="changeFlag()" <%=MAINTAIN_ACCESS%>>&nbsp;
			     <input type="button" value="Cancel" class="button1" onclick="gotoPage('workFunction','<%=contextPath%>/admin.do');">
			    </td>
		</tr>
			 
    		<tr valign="top">
    		    <td colspan="4">
			    	<table border="1" bordercolor="<%=iumColorB%>" cellpadding="10" cellspacing="0" bgcolor=""  width="100%"  height="400" class ="listtable1">
					  <tr><td colspan="4" valign="top">
				        <table border="1" width="100%" cellspacing="1" cellpadding="0"  >
				        <tr class="headerRow6">
				        <td class="headerRow6">PAGES</td>
				        <%for(int i=0;i<workFunctionForm.getAccessCount();i++){%>
				        <td class="headerRow6"><%out.print(workFunctionForm.getPageHeader()[i].toUpperCase());%></td>
				        <%}%>
				        </tr>				        	
				    
				    	
				    	<%for(int i=0;i< workFunctionForm.getRowSize();i++){

				    	%>

				    	<%
				    		if(i%2 == 0){
				    	%>
				    		<tr bgcolor="#CECECE" class="row5">
				    		<td><%out.print(workFunctionForm.getPageName()[i]);%></td>
				    	<%
				    		}
				    		else{
				    		
				    	%>	
				    		<tr bgcolor="#EDEFF0" class="row6">
							<td><%out.print(workFunctionForm.getPageName()[i]);%></td>
						<%
							}
							int x = 0;		
							for(int j=0; j< workFunctionForm.getAccessCount();j++){

								x = j+1;	
								if(workFunctionForm.getPageAccess()[i].charAt(j)== 'y'){
	               								

						%>
							<td valign="top" align="center" colspan="1">
               					<input type="hidden" name="pageAccessResult" value ="<%=workFunctionForm.getPageNameId()[i]+"~"+x%>">
								<input type="checkbox" class="label2" name="pageAccessCheckBox"  onclick="checkInquire('<%=workFunctionForm.getPageNameId()[i]%>','<%=x%>','<%=workFunctionForm.getAccessCount()%>','<%=i%>');" checked>

               				</td>
               			<%
               					}
              					else if (workFunctionForm.getPageAccess()[i].charAt(j)== 'n'){
               			%>
               				<td valign="top" align="center" colspan="1">
               					<input type="hidden" name="pageAccessResult" value ="<%=workFunctionForm.getPageNameId()[i]+"~"+x%>">
								<input type="checkbox" class="label2" name="pageAccessCheckBox" onclick="checkInquire('<%=workFunctionForm.getPageNameId()[i]%>','<%=x%>','<%=workFunctionForm.getAccessCount()%>','<%=i%>');">
               				</td>
               			<%
               					}
               					else{
               						               				
               			%>
               				<td valign="top" align="center" colspan="1">&nbsp;
               				</td>
               			<%
               					}
               					
               				}
               			%>
               			</tr>
               			<%
               			}
               			%>
               			 </table>
				       </td></tr>
			    	</table>
			
	    		</td>
	    	</tr> 
	    	<tr>
	    		<td colspan="4" align="left" >
	    		
	    		 <input type="button" value="Save" class="button1" onclick="changeFlag()" <%=MAINTAIN_ACCESS%>>&nbsp;
			     <input type="button" value="Cancel" class="button1" onclick="gotoPage('workFunction','<%=contextPath%>/admin.do');">
			    </td>
	    	</tr>
	    	
	    		

            <tr>
               <td colspan="10" class="label1"><font size="1">� 2003 Sun Life Financial. All rights reserved.</font></td>
            </tr>
            </table>
            <!-- BODY -->
            
            </div>
          </td>
        </tr>
        </td>
        </tr>
      </table>
    </form>
  </body>
</html>
    