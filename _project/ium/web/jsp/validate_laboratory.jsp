<%@ page language="java" import="com.slocpi.ium.util.*" buffer="12kb"%>
<%@ taglib uri="/struts-bean.tld" prefix="bean" %>

<!--
function putButton(numRecordsPerView, numRecords){
	var form = document.forms[0];
	if(document.all["create_btn"] != null){
		if(numRecords >= 10 && numRecordsPerView >= 10 ){
	        document.all["create_btn"].style.display = "block";
	    }
	    else{
    	   document.all["create_btn"].style.display = "none";
	    }
	}
	else{
		document.all["doc_cal"].style.display = "block";
	}
}
  	
function redirectForm(url, actionType)
{
	var form = document.forms[0];
	
	form.actionType.value = actionType;
	form.action = url;
	form.submit();

}

function validateMaintain(url, actionType) {
  var form = document.forms[0];
  
  if(form.updateLabId.length > 1)
  { 
		if(noChecked(form.updateLabId))
		{
			alert("<bean:message key="error.field.noselection" arg0="Laboratory" arg1="to update" />");
			return false;
		}
	}
	else
	{
		if(form.updateLabId.checked ==  false)
		{
			alert("<bean:message key="error.field.noselection" arg0="Laboratory" arg1="to update" />");
			return false;
		}
	}	
	redirectForm(url, actionType);
}

function validateAddTest(url, actionType)
{
  var form = document.forms[0];
	if(form.testCode.value == "")
	{
		alert("<bean:message key="error.field.noselection" arg0="Laboratory Test" arg1="to add" />");
		form.testCode.focus();		
		return false;
	}
	redirectForm(url, actionType);
}

function validateRemoveTest(url, actionType)
{
  var form = document.forms[0];
	if(noChecked(form.delTest))
	{
		alert("<bean:message key="error.field.noselection" arg0="Laboratory Test" arg1="to remove" />");
		return false;
	}
	inactivateTest(url, actionType);
}

function inactivateTest(url, actionType) {
	var form = document.forms[0];
	var len = form.delTest.length;
	var len2 = form.labTest.length;
	
	for(i=0; i<len; i++)
	{
		if(form.delTest[i].checked == true)
		{
			for(j=0; j<len2; j++)
			{
				if(form.delTest[i].value == form.labTest[j].value)
				{
					form.labTestStatus[j].value = "toInactive";
				}	
			}	
		}
	}
	redirectForm(url, actionType);
}	

function validateForm(url, actionType)
{
	var form = document.forms[0];
  var msgSpecial = "<bean:message key="error.field.specialcharacter" arg0="\\n Hypen [ - ] \\n Hash [ # ] \\n Parenthesis [ ( ) ] \\n Period [ . ] \\n Comma [ , ]" />"

  //required fields
	if(isEmpty(form.labId.value))
	{
		alert("<bean:message key="error.field.required" arg0="Laboratory ID" />");
		form.labId.focus();
		return false;
	}
	else if(!isNumeric(form.labId.value))
	{
		alert("<bean:message key="error.field.numeric" arg0="Laboratory ID" />");
		form.labId.focus();
		return false;
	}
	
	if(isEmpty(form.labName.value))
	{
		alert("<bean:message key="error.field.required" arg0="Laboratory Name" />");
		form.labName.focus();
		return false;
	}
	else if(!isAlphaNumericSpecial(form.labName.value))
	{
		alert("<bean:message key="error.field.alphanumeric" arg0="Laboratory Name" />" + "\n" + msgSpecial);
		form.labName.focus();
		return false;
	}
	
	if(isEmpty(form.contactPerson.value))
	{
		alert("<bean:message key="error.field.required" arg0="Contact Person" />");
		form.contactPerson.focus();
		return false;
	}
	else if(!isAlphaSpecial(form.contactPerson.value))
	{
		alert("<bean:message key="error.field.alpha" arg0="Contact Person" />"  + "\n" + msgSpecial);
		form.contactPerson.focus();
		return false;
	}
	
	if(!isEmpty(form.emailAddress.value))
	{
		if(!isEmail(form.emailAddress.value))
		{
			alert("<bean:message key="error.field.format" arg0="E-mail Address" arg1="" arg2="username@domainname" />");
			form.emailAddress.focus();
			return false;
		}
	}
	
	var hasAdd = 0;
	if(!isEmpty(form.busAdd1.value))
	{
		hasAdd = 1;
		if(!isAlphaNumericSpecial(form.busAdd1.value))
		{
			alert("<bean:message key="error.field.alphanumeric" arg0="Business Address Line 1" />"  + "\n" + msgSpecial);
			form.busAdd1.focus();
			return false;
		}
	}
	
	if(!isEmpty(form.busAdd2.value))
	{
		if(hasAdd != 1)
		{
			alert("<bean:message key="error.field.required" arg0="Business Address Line 1" />");
			form.busAdd1.focus();
			return false;
		}
	
		hasAdd = 2;			
		if(!isAlphaNumericSpecial(form.busAdd2.value))
		{
			alert("<bean:message key="error.field.alphanumeric" arg0="Business Address Line 2" />"  + "\n" + msgSpecial);
			form.busAdd2.focus();
			return false;
		}
	}
	
	if(!isEmpty(form.busAdd3.value))
	{
		if(hasAdd != 2)
		{
			alert("<bean:message key="error.field.required" arg0="Business Address Line 2" />");
			form.busAdd2.focus();
			return false;
		}
	
		hasAdd = 3;			
		if(!isAlphaNumericSpecial(form.busAdd3.value))
		{
			alert("<bean:message key="error.field.alphanumeric" arg0="Business Address Line 3" />"  + "\n" + msgSpecial);
			form.busAdd3.focus();
			return false;
		}
	}
	
	if(!isAlpha(form.busCity.value))
	{
		alert("<bean:message key="error.field.alpha" arg0="City" />");
		form.busCity.focus();
		return false;
	}
	
	if(!isAlpha(form.busCountry.value))
	{
		alert("<bean:message key="error.field.alpha" arg0="Country" />");
		form.busCountry.focus();
		return false;
	}
	
	if(!isNumeric(form.busZipCode.value))
	{
		alert("<bean:message key="error.field.numeric" arg0="Zip Code" />");
		form.busZipCode.focus();
		return false;
	}
	
	if(!isValidPhone(form.ofcPhone1.value))
	{
		alert("<bean:message key="error.field.format" arg0="Business Phone 1" arg1="" arg2="" />");
		form.ofcPhone1.focus();
		return false;
	}
	
	if(!isValidPhone(form.ofcPhone2.value))
	{
		alert("<bean:message key="error.field.format" arg0="Business Phone 2" arg1="" arg2="" />");
		form.ofcPhone2.focus();
		return false;
	}
	
	if(!isValidPhone(form.faxNumber.value))
	{
		alert("<bean:message key="error.field.format" arg0="Fax Number" arg1="" arg2="" />");
		form.faxNumber.focus();
		return false;
	}
	
	if(!isAlphaNumericSpecial(form.branchName.value))
	{
		alert("<bean:message key="error.field.alphanumeric" arg0="Branch Name" />"  + "\n" + msgSpecial);
		form.branchName.focus();
		return false;
	}
	
	hasAdd = 0;
	if(!isEmpty(form.branchAdd1.value))
	{
		hasAdd = 1;
		if(!isAlphaNumericSpecial(form.branchAdd1.value))
		{
			alert("<bean:message key="error.field.alphanumeric" arg0="Branch Address Line 1" />"  + "\n" + msgSpecial);
			form.branchAdd1.focus();
			return false;
		}
	}
	
	if(!isEmpty(form.branchAdd2.value))
	{
		if(hasAdd != 1)
		{
			alert("<bean:message key="error.field.required" arg0="Branch Address Line 1" />");
			form.branchAdd1.focus();
			return false;
		}
	
		hasAdd = 2;			
		if(!isAlphaNumericSpecial(form.branchAdd2.value))
		{
			alert("<bean:message key="error.field.alphanumeric" arg0="Branch Address Line 2" />"  + "\n" + msgSpecial);
			form.branchAdd2.focus();
			return false;
		}
	}
	
	if(!isEmpty(form.branchAdd3.value))
	{
		if(hasAdd != 2)
		{
			alert("<bean:message key="error.field.required" arg0="Branch Address Line 2" />");
			form.branchAdd2.focus();
			return false;
		}
	
		hasAdd = 3;			
		if(!isAlphaNumericSpecial(form.branchAdd3.value))
		{
			alert("<bean:message key="error.field.alphanumeric" arg0="Branch Address Line 3" />"  + "\n" + msgSpecial);
			form.branchAdd3.focus();
			return false;
		}
	}
	
	if(!isAlpha(form.branchCity.value))
	{
		alert("<bean:message key="error.field.alpha" arg0="Branch City" />");
		form.branchCity.focus();
		return false;
	}
	
	if(!isAlpha(form.branchCountry.value))
	{
		alert("<bean:message key="error.field.alpha" arg0="Branch Country" />");
		form.branchCountry.focus();
		return false;
	}
	
	if(!isNumeric(form.branchZipCode.value))
	{
		alert("<bean:message key="error.field.numeric" arg0="Branch Zip Code" />");
		form.branchZipCode.focus();
		return false;
	}
	
	if(!isValidPhone(form.branchPhone.value))
	{
		alert("<bean:message key="error.field.format" arg0="Branch Phone" arg1="" arg2="" />");
		form.branchPhone.focus();
		return false;
	}		
	
	if(!isValidPhone(form.branchFaxNumber.value))
	{
		alert("<bean:message key="error.field.format" arg0="Branch Fax Number" arg1="" arg2="" />");
		form.branchFaxNumber.focus();
		return false;
	}
	
	if(!isEmpty(form.accreditedDate.value))
	{
		if(!isValidDate(form.accreditedDate.value))
		{
			alert("<bean:message key="error.field.format" arg0="Accredited Date" arg1="date" arg2="DDMMMYYYY" />");
			form.accreditedDate.focus();
			return false;
		}
	}	
	
 if(form.labFee != null)
 {
 	if(form.labFee.length > 1)
 	{
 		for(i=0; i<form.labFee.length; i++)
 		{
 			var value = replace(form.labFee[i].value, ",", "");

	 		if(isNaN(value) || parseFloat(value) <= 0.0)
	 		{
		 		alert("<bean:message key="error.field.invalidValue" arg0="Lab Fee" />");
		 		form.labFee[i].focus();
	 			return false;
	 		}
	 		else if(parseFloat(value) > 9999999.99)
	 		{
		 		alert("<bean:message key="error.field.lessthanequalto" arg0="Lab Fee" arg1="9999999.99" />");
		 		form.labFee[i].focus();
	 			return false;
	 		}
	 	}	
 	}
 	else
 	{ 		
	 	var value = replace(form.labFee.value, ",", "");
	 	if(isNaN(value) || parseFloat(value) <= 0.0)
		{
	 		alert("<bean:message key="error.field.invalidValue" arg0="Lab Fee" />");
	 		form.labFee.focus();
			return false;
		}
		else if(parseFloat(value) > 9999999.99)
 		{
	 		alert("<bean:message key="error.field.lessthanequalto" arg0="Lab Fee" arg1="9999999.99" />");
	 		form.labFee.focus();
 			return false;
 		}
 	}
 }
	
	redirectForm(url, actionType);
}

function rePaginate (page, actionUrl) {
	form = document.forms[0];
	if (form.pageNo != null) {
		form.pageNo.value = page;	
	}
	
	redirectForm(actionUrl, 'display');
}

function formatFee(event)
{
	var form = document.forms[0];
	if(form.labFee != null)
	{
		if(form.labFee.length > 1)
 		{
	 		for(i=0; i<form.labFee.length; i++)
	 		{
 				getKeyAmount(event, form.labFee[i]);
 			}
	 	}
 		else
	 	{
		 	getKeyAmount(event, form.labFee);
	 	}	
	 }	
}

function initializePage(event)
{
	formatFee(event);
  formatDate();
}

function formatDate()
{
	var form = document.forms[0];
	if(form.accreditedDate != null)
	{
		if(!isEmpty(form.accreditedDate.value))
		{
			var d = form.accreditedDate.value;
			var dd   = d.substring(0,2);
			var mm  = d.substring(2,5).toUpperCase();
			var yyyy = d.substring(5,9); 	
			
			form.accreditedDate.value = dd + mm + yyyy;			
		}
	}
	
}

function makeUpperCase(field){
	var upper = eval("document.frm."+field+".value.toUpperCase()");
	eval("document.frm."+field+".value="+"'"+upper+"'");
}


	
//-->