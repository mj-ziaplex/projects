<%@ page language="java" buffer="24kb"%>
<!-- JAVA UTILS -->
<%@ page import="java.util.Date, java.util.ResourceBundle" %>
<!-- IUM FORMS -->
<%@ page import="com.slocpi.ium.ui.form.AssessmentRequestForm" %>
<!-- IUM UTILS -->
<%@ page import="com.slocpi.ium.util.DateHelper, com.slocpi.ium.util.IUMConstants" %>
<!-- TAGLIBS -->
<%@ taglib uri="/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/struts-html.tld" prefix="html" %>
<%@ taglib uri="/ium.tld" prefix="ium" %>

<%
String contextPath = request.getContextPath();
AssessmentRequestForm  requestForm= (AssessmentRequestForm)request.getAttribute("requestForm");
String insuredSexFemale="";
String insuredSexMale="";
// added this for change request - cris
if (!"".equals(requestForm.getInsuredSex())){
	if(requestForm.getInsuredSex().equals("F")){
		insuredSexFemale="checked"; 
	}else if((requestForm.getInsuredSex().equals("M")) ||(requestForm.getInsuredSex().equals(""))) {
		insuredSexMale="checked"; 
	}
}

String ownerSexFemale="";
String ownerSexMale="";
//added this for change request -- cris
if (!"".equals(requestForm.getOwnerSex())){
	if(requestForm.getOwnerSex().equals("F")){
		ownerSexFemale="checked"; 
	}else if((requestForm.getOwnerSex().equals("M")) || (requestForm.getOwnerSex().equals(""))) {
		ownerSexMale="checked"; 
	}
}



Date date = new Date();
String dateToday = DateHelper.format(date,"ddMMMyyyy").toUpperCase(); 
String filter=IUMConstants.ROLES_NB_REVIEWER +","+ IUMConstants.ROLES_NB_STAFF + "," + IUMConstants.ROLES_NB_ADMIN;
%>
<%
/* Andre Ceasar Dacanay - css enhancement - start */
String iumCss = "";
String iumColorO = "";
String companyCode = "";
try {
	ResourceBundle rbWMSSP = ResourceBundle.getBundle("dbconfig");
	iumCss = rbWMSSP.getString("IUM_CSS_VERSION");
	companyCode = iumCss;
	iumCss = "_"+iumCss;
} catch (Exception e){}
if(iumCss.equals("_CP")){
	iumColorO = "#FFCB00";
	companyCode="";
}else if(iumCss.equals("_GF")){
	iumColorO = "#303A77";
	companyCode = companyCode+" ";
}else{
	iumColorO = "#FFCB00";
}
/* Andre Ceasar Dacanay - css enhancement - end */
%>
<html>
  <head>
    <title><%=companyCode%>Integrated Underwriting and Medical System</title>
    <link href="<%=contextPath%>/css/main<%=iumCss%>.css" rel="stylesheet" type="text/css">
    <script language="Javascript" src="<%=contextPath%>/js/navigation.js"></script>
    <script language="Javascript" src="<%=contextPath%>/js/validation.js"></script>
    <script language="Javascript" src="<%=contextPath%>/jsp/val_createAssessmentRequest.jsp"></script>
    <script language="javascript">
	// empties field if not valid date!	
	function checkDate(obj,objAge1,objAge2){
		if (obj.value.length>0){
  			if(!isValidDate(obj.value)){
				   alert('<bean:message key="error.field.format" arg0="Birth Date" arg1="date" arg2="(ddMMMyyyy)"/>');
					 obj.focus();
 					 obj.select();
					 return false;
				}else if (isGreaterDate(obj.value,'<%=dateToday%>') ){
				alert('<bean:message key="error.field.beforecurrentdate" arg0="Birth Date" arg1="date" arg2="('<%=dateToday%>')"/>');
					obj.focus();
					obj.select();
				}else{ 
				   var age = computeAge(obj.value,'<%=dateToday%>');
				   if(age < 0){
				   	age = 0;
				   }
					 objAge1.value = age;
					 objAge2.value = age;
					  
					 return true;
				}
				
		}
		return false;
	}

	
	
	//Validation for required Fields in Insured Client Data
	function checkInsuredClientDetails(){
		var frm = document.requestForm;

		if(isEmpty(frm.insuredClientNo.value)){
			alert('<bean:message key="error.field.specificrequired" arg0="Client No"/>');
			frm.insuredClientNo.focus();
			return false;
		}
		if(isEmpty(frm.insuredLastName.value)){
			alert('<bean:message key="error.field.specificrequired" arg0="Last Name"/>');
			frm.insuredLastName.focus();
			return false;
		}
		if(isEmpty(frm.insuredFirstName.value)){
			alert('<bean:message key="error.field.specificrequired" arg0="First Name"/>');
			frm.insuredLastName.focus();
			return false;
		}
		/*if(isEmpty(frm.insuredMiddleName.value)){
			alert('<bean:message key="error.field.specificrequired" arg0="Middle Name"/>');
			frm.insuredMiddleName.focus();
			return false;
		}*/
		if(isEmpty(frm.insuredBirthDate.value)){
			/* commented out due to change request not requiring birth date
				alert('<bean:message key="error.field.specificrequired" arg0="Birth Date"/>');
				frm.insuredBirthDate.focus();
				return false;
 			*/
		} else{
			if( !isValidDate(frm.insuredBirthDate.value) ){
				alert('<bean:message key="error.field.format" arg0="Birth Date" arg1="date" arg2="(ddMMMyyyy)"/>');
				frm.insuredBirthDate.focus();
				frm.insuredBirthDate.select();
				return false;
		  }else{
				if (isGreaterDate(frm.insuredBirthDate.value,'<%=dateToday%>') ){
				alert('<bean:message key="error.field.beforecurrentdate" arg0="Birth Date"/>');
					frm.insuredBirthDate.focus();
					frm.insuredBirthDate.select();
					return false;
				}
			}	
		}
/* commented out due to change request not requiring gender
		if( frm.insuredSex[0].checked==false && frm.insuredSex[1].checked==false ){
			alert('<bean:message key="error.field.specificrequired" arg0="Gender"/>');
			frm.insuredSex[0].focus();
			return false;
		}
*/		
		if(frm.lob.value=="<%=IUMConstants.LOB_GROUP_LIFE%>"){
  		if( !isEmpty(frm.insuredAge.value) && frm.insuredAge.value<18 ){
			  alert('<bean:message key="error.field.greaterthanequalto" arg0="Age" arg1="18"/>');
  			//alert(frm.insuredClientType.options[frm.insuredClientType.selectedIndex].text+' must be at least 18 years of age.');
  			frm.insuredBirthDate.focus();
  			frm.insuredBirthDate.select();
  		  return false;
  		}
		}				
	
		return true;
	}	
	
	
	//Validation for required Fields in Owner Client Data
	function checkOwnerClientDetails(){
		var frm= document.requestForm;
		if(isEmpty(frm.ownerClientNo.value)){
			alert('<bean:message key="error.field.specificrequired" arg0="Client No"/>');
			frm.ownerClientNo.focus();
			return false;			
		}
		if(isEmpty(frm.ownerAge.value)){
			/* commented out due to change request not to require age
				alert('<bean:message key="error.field.specificrequired" arg0="Age"/>');
				frm.ownerAge.focus();
				return false; 
			*/			
		}else{
			if(!isNumeric(frm.ownerAge.value)){
				alert('<bean:message key="error.field.numeric" arg0="Age"/>')
				frm.ownerAge.focus();
				frm.ownerAge.select();
				return false;
			}
		}
		if(isEmpty(frm.ownerLastName.value)){
			alert('<bean:message key="error.field.specificrequired" arg0="Last Name"/>');
			frm.ownerLastName.focus();
			return false;
		}
		if(isEmpty(frm.ownerFirstName.value)){
			alert('<bean:message key="error.field.specificrequired" arg0="First Name"/>');
			frm.ownerFirstName.focus();
			return false;
		}
		/*if(isEmpty(frm.ownerMiddleName.value)){
			alert('<bean:message key="error.field.specificrequired" arg0="Middle Name"/>');
			frm.ownerMiddleName.focus();
			return false;
		}*/
		if(isEmpty(frm.ownerBirthDate.value)){
			/* commented out due to change request not requiring Birth Date
			alert('<bean:message key="error.field.specificrequired" arg0="Birth Date"/>');
			frm.ownerBirthDate.focus();
			return false;
			*/
		}else{
			if( !isValidDate(frm.ownerBirthDate.value) ){
				alert('<bean:message key="error.field.format" arg0="Birth Date" arg1="date" arg2="(ddMMMyyyy)"/>');
				frm.ownerBirthDate.focus();
				frm.ownerBirthDate.select();
				return false;
		    }else{
				if (isGreaterDate(frm.ownerBirthDate.value,'<%=dateToday%>') ){
				alert('<bean:message key="error.field.beforecurrentdate" arg0="Birth Date"/>');
					frm.ownerBirthDate.focus();
					frm.ownerBirthDate.select();
					return false;
				}
			}	
		}
/* commented out due to change request not requiring gender
		if( frm.ownerSex[0].checked==false && frm.ownerSex[1].checked==false ){
			alert('<bean:message key="error.field.specificrequired" arg0="Gender"/>');
			frm.ownerSex[0].focus();
			return false;
		}
*/
		if( !isEmpty(frm.ownerAge.value) && frm.ownerAge.value<18 ){
			if(frm.lob.value=="<%=IUMConstants.LOB_PRE_NEED%>"){
				  alert('<bean:message key="error.field.greaterthanequalto" arg0="Age" arg1="18"/>');
					//alert(frm.ownerClientType.options[frm.ownerClientType.selectedIndex].text+' must be at least 18 years of age.');
			}else{
	 			  //alert('Owner must be at least 18 years of age.');
				  alert('<bean:message key="error.field.greaterthanequalto" arg0="Age" arg1="18"/>');
			}
			frm.ownerBirthDate.focus();
			frm.ownerBirthDate.select();
		  return false;
		}
		return true;		
	}
	
	
	
	function validateClientData(){
		var pass=true;
		var frm= document.requestForm;
		var lob = frm.lob.value;
		if( lob=="<%=IUMConstants.LOB_PRE_NEED%>" ){
			pass = checkOwnerClientDetails();			
		}else if(lob=="<%=IUMConstants.LOB_INDIVIDUAL_LIFE%>" || lob=="<%=IUMConstants.LOB_GROUP_LIFE%>"){
			pass = checkInsuredClientDetails();
			if(frm.sameClientDataCB.checked==false && lob=='<%=IUMConstants.LOB_INDIVIDUAL_LIFE%>'){
				pass = checkOwnerClientDetails();
			}
			if(lob=="<%=IUMConstants.LOB_INDIVIDUAL_LIFE%>" && pass==true){
				var temppass = pass;
				var result= pass;
				if(!isEmpty(frm.ownerClientNo.value) ){
					result = checkOwnerClientDetails();
					if (temppass==true){
						pass=result;
					}
				}
			}
		}
		return pass;
	}
	
	
	

	function validateEntries(){
		
		var frm= document.requestForm;
		
		// check refNo.
		if(	isEmpty(frm.refNo.value)	){
			alert('<bean:message key="error.field.specificrequired" arg0="Reference number"/>');
			frm.refNo.focus();			
			return false;
		}
		
		// for agent, lob, branch, location, assignedTo		
		if(	noSelection(frm.agentCode.value)	){
			alert('<bean:message key="error.field.specificrequired" arg0="Agent ID"/>');
			frm.agentCode.focus();						
			return false;
		}
		
		if(	noSelection(frm.branchCode.value)	){
			alert('<bean:message key="error.field.specificrequired" arg0="Branch / Office ID"/>');
			frm.branchCode.focus();		
			return false;
		}
		
		if(	noSelection(frm.lob.value)	){
			alert('<bean:message key="error.field.specificrequired" arg0="LOB code"/>');
			frm.lob.focus();		
			return false;
		}
		
		//check premium and amountCovered		
		if(	isEmpty(frm.amountCovered.value)	){
			alert('<bean:message key="error.field.specificrequired" arg0="Amount Covered"/>');
			frm.amountCovered.focus();						
			return false;
		}else{
			if(!isInLimitCurrency(frm.amountCovered.value,'999999999.99')){
				alert('<bean:message key="error.field.lessthan" arg0="Amount Covered" arg1="999,999,999.99"/>');
				frm.amountCovered.focus();
				frm.amountCovered.selected();								
				return false;
			}
			if(isInLimitCurrency(frm.amountCovered.value,'0.00')){
				alert('<bean:message key="error.field.greaterthan" arg0="Amount Covered" arg1="0.00"/>');
				frm.amountCovered.focus();
				frm.amountCovered.select();				
				return false;
			}
		}
		if(	isEmpty(frm.premium.value)	){
			alert('<bean:message key="error.field.specificrequired" arg0="Premium"/>');
			frm.premium.focus();						
			return false;			
		}else{
			if(!isInLimitCurrency(frm.premium.value,'999999999.99')){
				alert('<bean:message key="error.field.lessthan" arg0="Premium" arg1="999,999,999.99"/>');
				frm.premium.focus();
				frm.premium.select();									
				return false;
			}
			if(isInLimitCurrency(frm.premium.value,'0.00')){
				alert('<bean:message key="error.field.greaterthan" arg0="Premium" arg1="0.00"/>');
			  frm.premium.focus();
				frm.premium.select();									
				return false;
			}
			
		}		
		
		//checks receivedDate and dateForwarded
		if (isEmpty(frm.receivedDate.value)){
			alert('<bean:message key="error.field.specificrequired" arg0="Application received date"/>');		
			frm.receivedDate.focus();					
			return false;
		}else{
			if (!isValidDate(frm.receivedDate.value)){
				alert('<bean:message key="error.field.format" arg0="Application received date" arg1="date" arg2="(ddMMMyyyy)"/>');
				frm.receivedDate.focus();
				frm.receivedDate.select();								
				return false;				
			}
			if (isGreaterDate(frm.receivedDate.value, '<%=dateToday%>')){
				alert('<bean:message key="error.field.beforecurrentdate" arg0="Application received date"/>');
				frm.receivedDate.focus();
				frm.receivedDate.select();				
				return false;				
			}
		}
		if (isEmpty(frm.dateForwarded.value)){
			alert('<bean:message key="error.field.specificrequired" arg0="Date forwarded"/>');
			frm.dateForwarded.focus();			
			return false;			
		}else{
			if (!isValidDate(frm.dateForwarded.value)){
				alert('<bean:message key="error.field.format" arg0="Date forwarded" arg1="date" arg2="(ddMMMyyyy)"/>');
				frm.dateForwarded.focus();
				frm.dateForwarded.select();				
				return false;				
			}
			if (isGreaterDate(frm.dateForwarded.value,'<%=dateToday%>')){
				alert('<bean:message key="error.field.beforecurrentdate" arg0="Date forwarded"/>');
				frm.dateForwarded.focus();
				frm.dateForwarded.select();				
				return false;				
			}
			if (isGreaterDate(frm.receivedDate.value,frm.dateForwarded.value) ){
				alert('<bean:message key="error.field.afterenddate" arg0="Date forwarded" arg1="Date received"/>');
					frm.dateForwarded.focus();
					frm.dateForwarded.select();
					return false;
			}			
			
		}

		if(	noSelection(frm.location.value)	){
			alert('<bean:message key="error.field.specificrequired" arg0="User (Folder Location)"/>');
			frm.location.focus();
			return false;
		}
		if(	noSelection(frm.assignedTo.value)	){
			alert('<bean:message key="error.field.specificrequired" arg0="Assigned To ID"/>');
			frm.assignedTo.focus();			
			return false;
		}
		
		
		if(frm.lob.value=='<%=IUMConstants.LOB_INDIVIDUAL_LIFE%>' && frm.insuredClientNo.value==frm.ownerClientNo.value && 
		   frm.sameClientDataCB.checked==false){
			alert('Insured Client No. is the same as Owner Client No.');	
			frm.sameClientDataCB.focus();    	
			return false;
	    }
		
		return true;
	}


   	function saveRequest(){
	    document.requestForm.action= 'saveAssessmentRequest.do';
	    if(validateEntries()==true){
	    	var frm= document.requestForm;
	    	if(frm.sameClientDataCB.checked==true&& frm.lob.value=='<%=IUMConstants.LOB_INDIVIDUAL_LIFE%>'){
	    		syncClientData();
	    	}
			if(validateClientData()==true){
				if(confirmUpdate()){
					document.requestForm.submit();
				}else{
					searchClient('<%=IUMConstants.CLIENT_TYPE_INSURED+IUMConstants.CLIENT_TYPE_OWNER%>');					
				}
			}
		}
	}

	function confirmUpdate(){
		var frm= document.requestForm;
		var pass = false;
		//alert(frm.insuredOriginal.value+","+frm.ownerOriginal.value);
		if (frm.insuredOriginal.value=='' && frm.ownerOriginal.value==''){
			pass = true;
		}else{
		   
			var valueInsured='';
		    if(frm.insuredOriginal.value!=''){
					valueInsured=frm.insuredClientNo.value+
				   		frm.insuredFirstName.value+
						frm.insuredLastName.value+
						frm.insuredMiddleName.value+
						frm.insuredTitle.value+
						frm.insuredSuffix.value;
						if(frm.insuredSex[0].checked==true){
						    valueInsured = valueInsured+frm.insuredSex[0].value;
						}else{
						    valueInsured = valueInsured+frm.insuredSex[1].value;
						}
			}
			var valueOwner='';
			if(frm.ownerOriginal.value!=''){
			        valueOwner=frm.ownerClientNo.value+
					   	frm.ownerFirstName.value+
						frm.ownerLastName.value+
						frm.ownerMiddleName.value+
						frm.ownerTitle.value+
						frm.ownerSuffix.value;
						if(frm.ownerSex[0].checked==true){
						    valueOwner = valueOwner+frm.ownerSex[0].value;
						}else{
						    valueOwner = valueOwner+frm.ownerSex[1].value;
						}
			}
			//alert(frm.insuredOriginal.value+","+frm.ownerOriginal.value);
			//alert(valueInsured+","+valueOwner);
			if(frm.insuredOriginal.value==valueInsured && frm.ownerOriginal.value==valueOwner){
				pass=true;	
			}else{				
				pass=confirm("Any update made on the client fields will be reflected in the client record.  "+  
							 "Select OK to proceed or Cancel to use original client details");
			}
		}
		return pass;
		
	}
			
	function cancelRequest() {
		window.location= 'listAssessmentRequests.do';
	}

  // create the arrays for the agentBranch javascript
    // declare the agents list
    aAgents = new Array ();
    <logic:iterate id="agentOffices" name="requestForm" property="agentBranches">
    aAgents.push("<bean:write name='agentOffices' property='userId' />");
    </logic:iterate>
	
    // declare the branches for the agents
    aBranches = new Array ();
    <logic:iterate id="agentOffices" name="requestForm" property="agentBranches">
    aBranches.push("<bean:write name='agentOffices' property='officeCode' />");	
    </logic:iterate>

    // declare the agent names array
    aAgentNames = new Array();
    <logic:iterate id="agentOffices" name="requestForm" property="agentBranches">
    aAgentNames.push("<bean:write name='agentOffices' property='lastName' />, <bean:write name='agentOffices' property='firstName' />");	
    </logic:iterate>
  </script>  
  <script language="Javascript" src="<%=contextPath%>/js/agentBranch.js"></script>
  
  </head>
  <body leftmargin="0" topmargin="0" onLoad="initializePage()">
  <form name="requestForm" method="post">
      <table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%">
        <tr>
          <td width="100%" colspan="2">    
            <jsp:include page="header.jsp" flush="true"/>
          </td>
        </tr>
        <tr>
          <td rowspan="2" bgcolor="<%=iumColorO%>" height="100%" width="20"><img src="<%=contextPath%>/images/spacer.gif" width="20"></td>
          <td background="<%=contextPath%>/images/GradientGray.jpg" height="27" width="100%" valign="top">&nbsp;&nbsp;
            <span class="main">Integrated Underwriting and Medical System</span>
          </td>
        </tr>
        <tr>
          <td width="100%" height="100%">
<!-- Status List START -->
            <table border="0" cellpadding="0" cellspacing="0" width="100%">
              <tr>
                <td align="left">&nbsp;
                  <ium:list className="label2" listBoxName="requestStatus" type="<%=IUMConstants.LIST_REQUEST_STATUS%>" styleName="width:200" selectedItem="<%=String.valueOf(IUMConstants.STATUS_NB_REVIEW_ACTION)%>" disabled="disable"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  <input class="button1" name="saveButton" type="button" value="Save" onClick="saveRequest()">&nbsp;
			<input class="button1" type="button" value="Cancel" onClick="cancelRequest()">&nbsp;                  
                </td>
              </tr>
            </table>
<!-- Status List END -->

&nbsp;<html:errors/>
					<input type="hidden" name="statusDate" value="<%=dateToday%>">
    		  <input type="hidden" name="lob2" value='<bean:write name="requestForm" property="lob"/>'>
					<input type="hidden" name="clientToSearch" value='<bean:write name="requestForm" property="clientToSearch"/>'>
          <input type="hidden" name="insuredOriginal" value='<bean:write name="requestForm" property="insuredOriginal"/>'>
          <input type="hidden" name="ownerOriginal" value='<bean:write name="requestForm" property="ownerOriginal"/>'>
					
			
            <table border="0" cellpadding="10" cellspacing="0" height="100%" width="100%">
              <tr>
                <td>
                  <table border="0" cellpadding="2" cellspacing="0" width="900">
                      <tr>
                        <td class="label2"><span class="required">*</span><B>Reference No.</B></td>
                        <td class="label2"><input type="text" name="refNo" class="label2" style="width:150" value='<bean:write name="requestForm" property="refNo"/>' maxlength="15" onBlur="this.value=this.value.toUpperCase();"/></td>
                        <td class="label2">&nbsp;</td>
                        <td class="label2"><B>&nbsp;&nbsp;Admin Sys Source</B></td>
                        <td class="label2">
                        	<input type="hidden" name="sourceSystem" class="label2" style="width:150" value='<bean:write name="requestForm" property="sourceSystem"/>'>
                        	<input type="text" name="adminSource" class="label2" style="width:150" value='None' disabled=true>
                        </td>
                        <td class="label2">&nbsp;</td>
                        <td class="label2"><B>&nbsp;&nbsp;Date Created</B></td>
                        <td class="label2">
							 <input type="text" name="dateCreated" class="label2" style="width:150" value="<%=dateToday%>" maxlength="9" disabled="disable">
						</td>
                      </tr>
                      <tr>
                        <td class="label2"><span class="required">*</span><B>Agent</B></td>
                        <td class="label2">               
	               <ium:list className="label2" listBoxName="agentCode" type="<%=IUMConstants.LIST_USERS%>" styleName="width:150" selectedItem="<%=requestForm.getAgentCode()%>"  filter="<%=IUMConstants.ROLES_AGENTS%>" onChange="assignBranch(this, document.requestForm.branchCode);"/>
                  		  </td>
                        <td class="label2">&nbsp;</td>
                        <td class="label2"><span class="required">*</span><B>Branch</B></td>
                        <td colspan="4" className="label2">
                  <ium:list  styleName="width:450" className="label2" listBoxName="branchCode" type="<%=IUMConstants.LIST_BRANCHES%>" selectedItem="<%=requestForm.getBranchCode()%>" onChange="filterAgentList(document.requestForm.agentCode, this);"/>
											  </td>                 
                      </tr>
										  <tr>
                        <td colspan="8" className="label2">&nbsp;</td>
                      </tr>
                      <tr>
                        <td class="label2"><span class="required">*</span><b>LOB</b></td>
                        <td class="label2"><ium:list className="label2" listBoxName="lob" type="<%=IUMConstants.LIST_LOB%>" styleName="width:150" selectedItem="<%=requestForm.getLob()%>" onChange="changeDefaultClientType()" />
												</td>
                        <td class="label2">&nbsp;</td>    
                        <td class="label2"><span class="required">*</span><B>Amount Covered</B></td>
                        <td class="label2">
                        <input type="text" name="amountCovered" class="label2" style="width:150" value='<bean:write name="requestForm" property="amountCovered"/>'  maxlength="14" onBlur="document.requestForm.amountCovered.value=formatCurrency(document.requestForm.amountCovered.value);" onKeyUp="getKeyAmount(event,this);">
                        </td>
                        <td class="label2">&nbsp;</td>
                        <td class="label2"><span class="required">*</span><B>Premium</B></td>
                        <td class="label2">
                        <input type="text" name="premium" class="label2" style="width:150" value='<bean:write name="requestForm" property="premium" />' maxlength="14" onBlur="document.requestForm.premium.value=formatCurrency(document.requestForm.premium.value);"  onKeyUp="getKeyAmount(event,this);" >
                        </td>
                      </tr>
                      <tr>
                        <td class="label2"><span class="required">*</span><B>Date Received</B></td>
                        <td class="label2"><input type="text" name="receivedDate" class="label2" style="width:150" value='<bean:write name="requestForm" property="receivedDate"/>' maxlength="9" onKeyUp="getKeyDate(event,this);" >&nbsp;
                        <a href="#" onClick="popUpWin('<%=contextPath%>/jsp/calendar.jsp?textbox=requestForm.receivedDate',290,155);"><img src="<%=contextPath%>/images/calendar.gif" height="17" width="19" border="0">
				        </a>
                        </td>
                        <td class="label2">&nbsp;</td>  
                        <td class="label2"><span class="required">*</span><B>Date Forwarded</B></td>
                        <td class="label2"><input type="text" name="dateForwarded" class="label2" style="width:150" value='<bean:write name="requestForm" property="dateForwarded"/>' maxlength="9" onKeyUp="getKeyDate(event,this);">&nbsp;
                        <a href="#" onClick="popUpWin('<%=contextPath%>/jsp/calendar.jsp?textbox=requestForm.dateForwarded',290,155);"><img src="<%=contextPath%>/images/calendar.gif" height="17" width="19" border="0">
				        </a>
                        </td>
                        <td class="label2">&nbsp;</td>
                        <td class="label2"><B>&nbsp;&nbsp;Date Edited</B></td>
                        <td class="label2"><input type="text" name="dateEdited" class="label2" style="width:150" value="<%=dateToday%>" maxlength="9" disabled="disable">
                        </td>
                      </tr>
                      <tr>
                        <td class="label2"><span class="required">*</span><B>Location</B></td>
                        <td class="label2">
	               <ium:list className="label2" listBoxName="location" type="<%=IUMConstants.LIST_USERS%>" styleName="width:150" selectedItem="<%=requestForm.getLocation()%>" filter="<%=filter%>"/>
                        </td>
                        <td class="label2">&nbsp;</td>
                        <td class="label2"><span class="required">*</span><B>Assigned to</B></td>
                        <td class="label2">
	               <ium:list className="label2" listBoxName="assignedTo" type="<%=IUMConstants.LIST_USERS%>" styleName="width:150" selectedItem="<%=requestForm.getAssignedTo()%>" filter="<%=filter%>"/>
                        </td>
                        <td class="label2">&nbsp;</td>
                        <td class="label2"><B>&nbsp;&nbsp;Underwriter</B></td>                        
                        <td class="label2">
				   <ium:list className="label2" listBoxName="underwriter" type="<%=IUMConstants.LIST_USERS%>" styleName="width:150" selectedItem="<%=requestForm.getUnderwriter()%>" disabled="disable" filter="<%= IUMConstants.ROLES_UNDERWRITER%>"/>                        
						</td>
                      </tr>
                      <tr>
                        <td colspan="8" className="label2">&nbsp;
                        </td>
                      </tr>
					
									
<!--START OF CLIENT (insured) DETAILS-->									
                      <tr id="INS1" class="hide">
                        <td class="label2"><b>&nbsp;&nbsp;Client Type</b></td>
                        <td class="label2">
						<ium:list className="label2" listBoxName="insuredClientType" type="<%=IUMConstants.LIST_CLIENT_TYPE%>" styleName="width:150" selectedItem="" disabled="disable"/>
						</td>
                        <td class="label2">&nbsp;</td>
                        <td class="label2"><span class="required">*</span><b>Client No.</b></td>
						<td class="label2">
						<input type="text" name="insuredClientNo" class="label2" style="width:150" value='<bean:write name="requestForm" property="insuredClientNo"/>' onKeyUp="getKeyInsured(event)" onBlur="this.value=this.value.toUpperCase();searchClient('<%=IUMConstants.CLIENT_TYPE_INSURED%>')" maxlength="10">
						</td>
						<td class="label2">&nbsp;</td>
						<td id="SAME1" class="hide" align="right" class="label2">
							<input type="checkbox" name="sameClientDataCB" value="Y" onClick="sameClientDataControl();">
							<input type="hidden" name="sameClientData" value ="<%=requestForm.getSameClientData()%>">
						</td>
						<td id="SAME2" class="hide"><div class="label2">same insured and owner</div></td>
                      </tr>
                      <tr id="INS2" class="hide">
                        <td class="label2"><span class="required">*</span><b>Last Name</b></td>
                        <td class="label2"><input type="text" name="insuredLastName" class="label2" style="width:150" value='<bean:write name="requestForm" property="insuredLastName"/>' maxlength="40"></td>
                        <td class="label2">&nbsp;</td>
                        <td class="label2"><span class="required">*</span><b>First Name</b></td>
                        <td class="label2"><input type="text" name="insuredFirstName" class="label2" style="width:150" value='<bean:write name="requestForm" property="insuredFirstName"/>' maxlength="25"></td>
                        <td class="label2">&nbsp;</td>
                        <td class="label2"><b>&nbsp;&nbsp;Middle Name</b></td>
                        <td class="label2"><input type="text" name="insuredMiddleName" class="label2" style="width:150" value='<bean:write name="requestForm" property="insuredMiddleName"/>' maxlength="25"></td>
                      </tr>
                      <tr id="INS3" class="hide">
                        <td class="label2"><b>&nbsp;&nbsp;Title</b></td>
                        <td class="label2"><input type="text" name="insuredTitle" class="label2" style="width:150" value='<bean:write name="requestForm" property="insuredTitle"/>' maxlength="15"></td> 
                        <td class="label2">&nbsp;</td>
                        <td class="label2"><b>&nbsp;&nbsp;Suffix</b></td>
                        <td class="label2"><input type="text" name="insuredSuffix" class="label2" style="width:150" value='<bean:write name="requestForm" property="insuredSuffix"/>' maxlength="10"></td>
                        <td class="label2">&nbsp;</td>
<!--                    <td class="label2"><span class="required">*</span><b>Birth Date</b></td>  original before removal due to change request-->
                        <td class="label2"><b>Birth Date</b></td>
                        <td class="label2"><input type="text" name="insuredBirthDate" class="label2" style="width:150" value='<bean:write name="requestForm" property="insuredBirthDate"/>' maxlength="9" onKeyUp="getKeyDate(event,this);" onBlur="checkDate(this,document.requestForm.insuredAge,document.requestForm.iage);">&nbsp;
                        <a href="#" onClick="popUpWin('<%=contextPath%>/jsp/calendar.jsp?textbox=requestForm.insuredBirthDate',290,155);"><img src="<%=contextPath%>/images/calendar.gif" height="17" width="19" border="0">
				        </a>
                        </td>
                      </tr>
                      <tr id="INS4" class="hide">                   
                        <td class="label2"><b>&nbsp;&nbsp;Age</b></td>
                        <td class="label2"><input type="hidden" name="insuredAge" class="label2" style="width:150" value='<bean:write name="requestForm" property="insuredAge"/>' maxlength="2">
<input type="text" name="iage" class="label2" style="width:150" value='<bean:write name="requestForm" property="insuredAge"/>' maxlength="2" disabled="true">
												</td>
                        <td class="label2">&nbsp;</td>
<!--                        <td class="label2"><span class="required">*</span><b>Gender</b></td>  commented out due to change request not requiring gender-->
						<td class="label2"><b>Gender</b></td>
                        <td class="label2">
                        <input type="radio" name="insuredSex" value="M" <%=insuredSexMale%> class="label2">Male</input>&nbsp;&nbsp;&nbsp;
                        <input type="radio" name="insuredSex" value="F" <%=insuredSexFemale%>  class="label2">Female</input>
                        </td>                        
                      </tr>

                      
<!--START OF CLIENT (owner) DETAILS-->
                      <tr id="OWN1" class="hide">
                        <td class="label2"><b>&nbsp;&nbsp;Client Type</b></td>
                        <td class="label2"><ium:list className="label2" listBoxName="ownerClientType" type="<%=IUMConstants.LIST_CLIENT_TYPE%>" styleName="width:150" selectedItem="" disabled="disable"/></td>
                        <td class="label2">&nbsp;</td>
                        <td class="label2"><span class="required">*</span><b>Client No.</b></td>
                        <td class="label2">
                        	<input type="text" name="ownerClientNo" class="label2" style="width:150" value='<bean:write name="requestForm" property="ownerClientNo"/>' onKeyUp="getKeyOwner(event)" onBlur="this.value=this.value.toUpperCase();searchClient('<%=IUMConstants.CLIENT_TYPE_OWNER%>')" maxlength="10">
						</td>
                      </tr>
                      <tr id="OWN2" class="hide">
                        <td class="label2"><span class="required">*</span><b>Last Name</b></td>
                        <td class="label2"><input type="text" name="ownerLastName" class="label2" style="width:150" value='<bean:write name="requestForm" property="ownerLastName"/>' maxlength="40"></td> 
                        <td class="label2">&nbsp;</td>
                        <td class="label2"><span class="required">*</span><b>First Name</b></td>
                        <td class="label2"><input type="text" name="ownerFirstName" class="label2" style="width:150" value='<bean:write name="requestForm" property="ownerFirstName"/>' maxlength="25"></td>
                        <td class="label2">&nbsp;</td>
                        <td class="label2"><b>&nbsp;&nbsp;Middle Name</b></td>
                        <td class="label2"><input type="text" name="ownerMiddleName" class="label2" style="width:150" value='<bean:write name="requestForm" property="ownerMiddleName"/>' maxlength="25"></td>
                      </tr>
                      <tr id="OWN3" class="hide">
                        <td class="label2"><b>&nbsp;&nbsp;Title</b></td>
                        <td class="label2"><input type="text" name="ownerTitle" class="label2" style="width:150" value='<bean:write name="requestForm" property="ownerTitle"/>' maxlength="15"></td> 
                        <td class="label2">&nbsp;</td>
                        <td class="label2"><b>&nbsp;&nbsp;Suffix</b></td>
                        <td class="label2"><input type="text" name="ownerSuffix" class="label2" style="width:150" value='<bean:write name="requestForm" property="ownerSuffix"/>' maxlength="10"></td>
                        <td class="label2">&nbsp;</td>
<!--                        <td class="label2"><span class="required">*</span><b>Birth Date</b></td> -->
                        <td class="label2"><b>Birth Date</b></td>
                        <td class="label2">
                        	<input type="text" name="ownerBirthDate" class="label2" style="width:150" value='<bean:write name="requestForm" property="ownerBirthDate"/>' maxlength="9" onKeyUp="getKeyDate(event,this);" onBlur="checkDate(this,document.requestForm.ownerAge,document.requestForm.oage);">&nbsp;
                        	<a href="#" onClick="popUpWin('<%=contextPath%>/jsp/calendar.jsp?textbox=requestForm.ownerBirthDate',290,155);"><img src="<%=contextPath%>/images/calendar.gif" height="17" width="19" border="0">
					        </a>
                        	</td>
                      </tr>
                      <tr id="OWN4" class="hide">                       
                        <td class="label2"><b>&nbsp;&nbsp;Age</b></td>
                        <td class="label2"><input type="hidden" name="ownerAge" class="label2" style="width:150" value='<bean:write name="requestForm" property="ownerAge"/>' maxlength="2">
<input type="text" name="oage" class="label2" style="width:150" value='<bean:write name="requestForm" property="ownerAge"/>' maxlength="2" disabled="true">
												</td>
                        <td class="label2">&nbsp;</td>
<!--                        <td class="label2"><span class="required">*</span><b>Gender</b></td> -->
                        <td class="label2"><b>Gender</b></td>
                        <td class="label2">
                            <input type="radio" name="ownerSex" value="M" <%=ownerSexMale%> class="label2">Male</input>&nbsp;&nbsp;&nbsp;
                            <input type="radio" name="ownerSex" value="F" <%=ownerSexFemale%> class="label2">Female</input>
                        </td>                        
                      </tr>
										
<!--END OF CLIENT DETAILS-->

                      <tr>
                        <td class="label2" colspan="8">&nbsp;</td>
                      </tr>							
                    </table>
                </td>
              </tr> 
              <tr>
                <td valign=top colspan="2" className="label2">
					<div valign="top" class="label2"><B>&nbsp;&nbsp;Remarks</B></div>
                    &nbsp;&nbsp;<textarea rows="7" cols="54" name="remarks"><bean:write name="requestForm" property="remarks"/></textarea>
                </td>
              </tr>
              <tr>
                <td colspan="10" class="label1"><font size="1">� 2003 Sun Life Financial. All rights reserved.</font></td>
              </tr>
              <tr>
                <td colspan="10" class="label1">&nbsp;</td>
              </tr>          
            </table>
          </td>
        </tr>
      </table>
  </form>
  </body>
</html>