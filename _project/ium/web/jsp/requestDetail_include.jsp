<%@ page language="java" import="java.util.*, com.slocpi.ium.ui.form.*, com.slocpi.ium.util.*, com.slocpi.ium.ui.util.*, com.slocpi.ium.data.*, com.slocpi.ium.underwriter.*, java.util.ResourceBundle" buffer="1024kb"%>
<%@ taglib uri="/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/struts-html.tld" prefix="html" %>
<%@ taglib uri="/ium.tld" prefix="ium" %>
<script language="Javascript" type="text/javascript">
function uwPopup(url){
	var uwPop = window.open(url, 'popup', "top=180 left=410 width=700 height=310 scrollbars=yes");
}
   	
</script>	
<%

CDSDetailForm actionForm = (CDSDetailForm) request.getAttribute("detailForm");

String contextPath = request.getContextPath();

String currentStatus = request.getParameter("status");
String selectedStatus = request.getParameter("selectedStatus");

//request details
String refNo = request.getParameter("refNo");
String policySuffix = request.getParameter("policySuffix");
String lob = request.getParameter("lob");
String sourceSystem = request.getParameter("sourceSystem");
String amountCovered = request.getParameter("amountCovered");
String premium = request.getParameter("premium");
String branchCode = request.getParameter("branchCode");
String agentCode = request.getParameter("agentCode");
String assignedTo = request.getParameter("assignedTo");
String underwriter = request.getParameter("underwriter");
String location = request.getParameter("location");
String remarks = request.getParameter("remarks");
String receivedDate = request.getParameter("receivedDate");
String dateForwarded = request.getParameter("dateForwarded");
String dateEdited = request.getParameter("dateEdited");
String dateCreated = request.getParameter("dateCreated");

//insured client data
String insuredClientType = request.getParameter("insuredClientType");
String insuredClientNo = request.getParameter("insuredClientNo");
String insuredLastName = request.getParameter("insuredLastName");
String insuredFirstName = request.getParameter("insuredFirstName");
String insuredMiddleName = request.getParameter("insuredMiddleName");
String insuredTitle = request.getParameter("insuredTitle");
String insuredSuffix = request.getParameter("insuredSuffix");
String insuredAge = request.getParameter("insuredAge");
String insuredSex = request.getParameter("insuredSex");
String insuredBirthDate = request.getParameter("insuredBirthDate");

//owner client data
String ownerClientType = request.getParameter("ownerClientType");
String ownerClientNo = request.getParameter("ownerClientNo");
String ownerLastName = request.getParameter("ownerLastName");
String ownerFirstName = request.getParameter("ownerFirstName");
String ownerMiddleName = request.getParameter("ownerMiddleName");
String ownerTitle = request.getParameter("ownerTitle");
String ownerSuffix = request.getParameter("ownerSuffix");
String ownerAge = request.getParameter("ownerAge");
String ownerSex = request.getParameter("ownerSex");
String ownerBirthDate = request.getParameter("ownerBirthDate");

//filter for assigned to
AssessmentRequest ar = new AssessmentRequest();			
String filter = ar.retrieveAssignToRolesFilter(currentStatus, selectedStatus, lob);
UserData ud = new UserData();
if (filter.equals("")) {
    filter = "NONE";
	ud = new UserManager().retrieveUserDetails(assignedTo);
}
else
{
	ud.setProfile(new UserProfileData());
}
request.setAttribute("userData", ud);

ResourceBundle rb = ResourceBundle.getBundle("wmsconfig");
String signatureVerificationURL = rb.getString("signatureVerificationURL");
String wmsContextPath = rb.getString("wms_context_path");
if(policySuffix==null){
	policySuffix = "";
}
String SVLink = signatureVerificationURL + "?PolicyNo=";
String policy = (String) (request.getAttribute("policyNum"));

if (policy == null){
	policy = refNo;

}

String UWPopupLink = contextPath + "/viewUWAssessmentPopup.do?refNo=" + policy;


String TABLE_WIDTH = "";
String LABEL_CLASS = "";
String TXTBOX1_WIDTH = "";
String TXTBOX2_WIDTH = "";
if(contextPath.equalsIgnoreCase(wmsContextPath)){
	TABLE_WIDTH = "400";
	LABEL_CLASS = "label7";
	TXTBOX1_WIDTH = "width:120";
	TXTBOX2_WIDTH = "width:375";
}else{
	TABLE_WIDTH = "100%";
	LABEL_CLASS = "label2";
	TXTBOX1_WIDTH = "width:150";
	TXTBOX2_WIDTH = "width:450";
}

String _lob = actionForm.getLob();
if (_lob == null) {
  _lob = "";
}

%>
<input type="hidden" name="searchClientType" value="">


<div style="border: solid;border-width:1;position:relative; top:-39; left:0.65; width: 700px">
                  <table border="0" cellpadding="3" cellspacing="0" width="700px">
                    
                    <tr bgcolor="#2A4C7C">
                        <% if (_lob.equals(IUMConstants.LOB_INDIVIDUAL_LIFE)) { %>
						<td class="<%=LABEL_CLASS%>" colspan="2" style="color: #FFFFFF" ><b>Individual Life Policy Information</b></td>
	                    <% } else if(_lob.equals(IUMConstants.LOB_PRE_NEED)){%>
	                    <td class="<%=LABEL_CLASS%>" colspan="2" style="color: #FFFFFF" ><b>Plan Information</b></td>
	                    <% } else if(_lob.equals(IUMConstants.LOB_MUTUAL_FUNDS)){%>
	                    <td class="<%=LABEL_CLASS%>" colspan="2" style="color: #FFFFFF" ><b>Mutual Funds Policy Information</b></td>
	                    <% } else if(_lob.equals(IUMConstants.LOB_GROUP_LIFE)){%>
	                    <td class="<%=LABEL_CLASS%>" colspan="2" style="color: #FFFFFF" ><b>Group Life Policy Information</b></td>
	                    <% } %>
	                    <td class="<%=LABEL_CLASS%>">&nbsp;</td>
						<td class="<%=LABEL_CLASS%>">&nbsp;</td>
						<td class="<%=LABEL_CLASS%>">&nbsp;</td>
						<td class="<%=LABEL_CLASS%>">&nbsp;</td>
					
                	</tr>  
                	
                	<tr>
						<td class="<%=LABEL_CLASS%>"><b>Reference No.</b></td>
	                    <td class="<%=LABEL_CLASS%>"><bean:write name="detailForm" property="refNo"/><bean:write name="detailForm" property="policySuffix"/>
						<input type="hidden" name="refNo" value="<bean:write name="detailForm" property="refNo"/>"></td>
	                   	
						<td class="<%=LABEL_CLASS%>"><b>Amount Covered</b></td>
	                    <td class="<%=LABEL_CLASS%>"><bean:write name="detailForm" property="amountCovered"/></td>
						
	                    <td class="<%=LABEL_CLASS%>"><b>Date Created</b></td>
						<td class="<%=LABEL_CLASS%>"><bean:write name="detailForm" property="dateCreated"/><input type="hidden" name="dateCreated" value="
						<bean:write name="detailForm" property="dateCreated"/>"></td>
	                    
                	</tr> 
                	
                	<tr>
						<td class="<%=LABEL_CLASS%>"><b>Premium</b></td>
	                    <td class="<%=LABEL_CLASS%>"><bean:write name="detailForm" property="premium"/><input type="hidden" name="premium" value="
						<bean:write name="detailForm" property="premium"/>"></td>
	                    <td class="<%=LABEL_CLASS%>"><b>Premium Mode</b></td>
					
						<td class="<%=LABEL_CLASS%>">&nbsp;</td>
	                    <td class="<%=LABEL_CLASS%>"><b>Date Edited</b></td>
	                    <td class="<%=LABEL_CLASS%>"><bean:write name="detailForm" property="dateEdited"/><input type="hidden" name="dateEdited" value="
						<bean:write name="detailForm" property="dateEdited"/>"></td>
               		 </tr>
               		 
               		 <tr>
						<td class="<%=LABEL_CLASS%>"><b>Supplemental Benefits</b></td>
						<td></td>
               		 </tr>
                	
				</table>                	

</div>

      <div style="border: none;border-width:1;position:relative; top:-25; left:0.65">
			
	<table border="0" cellpadding="3" cellspacing="0" width="80%">

  <%
                    //String sex = insuredSex;
  					String sex = "M";
                    String sexChecked_M = "checked";
                    String sexChecked_F = "";
                    //if (sex != null) {
                      if (sex.equals("M")) {
                        sexChecked_M = "checked";
                      }
                      else if (sex.equals("F")) {
                        sexChecked_F = "checked";
                      }
                    //}
  %>					
	
  
				
				
<%
                    String iSelected = "";
                    String oSelected = "";                    
                    if (lob != null) {
                      if (lob.equals(IUMConstants.LOB_INDIVIDUAL_LIFE)) {
                    	  iSelected = IUMConstants.CLIENT_TYPE_INSURED;
                          oSelected = IUMConstants.CLIENT_TYPE_OWNER;
                      }
                      else if (lob.equals(IUMConstants.LOB_GROUP_LIFE)) {
                        iSelected = IUMConstants.CLIENT_TYPE_MEMBER;
                      }
                      else if (lob.equals(IUMConstants.LOB_PRE_NEED)) { 
                        iSelected = IUMConstants.CLIENT_TYPE_PLANHOLDER;
                      }
                    }

                    String sameIndValue = "N";
                    String sameIndChecked = "";
                    if (insuredClientNo.equals(ownerClientNo)) {
                      sameIndValue = "Y";
                      sameIndChecked = "checked";
                    }
%>

          
				 <tr > 
					<% if (lob.equals(IUMConstants.LOB_INDIVIDUAL_LIFE)) { %>
					<td class="<%=LABEL_CLASS%>"  colspan="2"></td>
					<% //} else { %>
                    <td class="<%=LABEL_CLASS%>"  colspan="2"></td>
                    <% } %>
				</tr>
				 <tr > 
					<td>&nbsp;</td>
				</tr>
	</table>
</div>     

<div style="border: solid;border-width:1;position:absolute; top:180; left:30; width: 342px">
			
	<table border="0" cellpadding="3" cellspacing="0" width="342px">
           	    	
				<tr bgcolor="#2A4C7C">
					<td class="<%=LABEL_CLASS%>" colspan="2" style="color: #FFFFFF"><b>Insured Information</b></td>
                    <td class="<%=LABEL_CLASS%>">&nbsp;</td>
					<td class="<%=LABEL_CLASS%>">&nbsp;</td>
			    </tr>
			    
			    <tr > 
					<td class="<%=LABEL_CLASS%>" ><b>Client No.</b></td>
					<td class="<%=LABEL_CLASS%>"><bean:write name="detailForm" property="insuredClientNo"/><input type="hidden" name="insuredClientNo" value="<bean:write name="detailForm" property="insuredClientNo"/>" onBlur="searchClientData('<%=IUMConstants.CLIENT_TYPE_INSURED%>')" maxlength="10" disabled></td>
					<td class="<%=LABEL_CLASS%>">&nbsp;</td>
				</tr>
				
				<tr >
					<td class="<%=LABEL_CLASS%>"><b>Name</b></td>
                    <td class="<%=LABEL_CLASS%>" colspan="3"><bean:write name="detailForm" property="insuredTitle"/>
                    &nbsp;<bean:write name="detailForm" property="insuredFirstName"/>
                    &nbsp;<bean:write name="detailForm" property="insuredMiddleName"/>
                    &nbsp;<bean:write name="detailForm" property="insuredLastName"/>
                    &nbsp;<bean:write name="detailForm" property="insuredSuffix"/></td>
				</tr>
				
				<tr >     
					<td class="<%=LABEL_CLASS%>" ><b>Gender</b></td>
					<td class="<%=LABEL_CLASS%>" align="left" >  
					                         
						<logic:equal name="detailForm" property="insuredSex" value="M">
							<input type="radio" name="insuredSex" value="M" checked>Male</input>&nbsp;&nbsp;&nbsp;
							<input type="radio" name="insuredSex" value="F">Female</input>  
						</logic:equal>
						<logic:notEqual name="detailForm" property="insuredSex" value="M">
							<input type="radio" name="insuredSex" value="M" >Male</input>&nbsp;&nbsp;&nbsp;
							<input type="radio" name="insuredSex" value="F" checked>Female</input>  
						</logic:notEqual>
					<td class="<%=LABEL_CLASS%>">&nbsp;</td>	
				</tr>
				
				<tr >                   
					<td class="<%=LABEL_CLASS%>" ><b>Birth Date</b></td>
					<td class="<%=LABEL_CLASS%>"><bean:write name="detailForm" property="insuredBirthDate"/><input type="hidden" name="insuredBirthDate"  value="<bean:write name="detailForm" property="insuredBirthDate"/>" onKeyUp="getKeyDate(event,this);" disabled>&nbsp;<div id="req_cal1" style="display:none;position:absolute;"></div></td> 
					<td class="<%=LABEL_CLASS%>" ><b>Age</b></td>
					<td class="<%=LABEL_CLASS%>" ><bean:write name="detailForm" property="insuredAge"/><input type="hidden" name="insuredAge" value="<bean:write name="detailForm" property="insuredAge"/>"></td>
					
				</tr>
	
	
	</table>
</div>	

<div style="border: solid;border-width:1;position:absolute; top:180; left:382; width: 348px">
			
	<table border="0" cellpadding="3" cellspacing="0" width="348px">
           	    	
				<tr bgcolor="#2A4C7C">
					<td class="<%=LABEL_CLASS%>" colspan="2" style="color: #FFFFFF"><b>Owner Information</b></td>
                    <td class="<%=LABEL_CLASS%>">&nbsp;</td>
					<td class="<%=LABEL_CLASS%>">&nbsp;</td>	
				</tr>
				
				<tr > 
					<td class="<%=LABEL_CLASS%>" ><b>Client No.</b></td>
					<td class="<%=LABEL_CLASS%>" ><bean:write name="detailForm" property="ownerClientNo"/><input type="hidden" name="ownerClientNo" value="
					<bean:write name="detailForm" property="ownerClientNo"/>"></td>
					<td class="<%=LABEL_CLASS%>">&nbsp;</td>
				</tr>
				
				<tr >
					<td class="<%=LABEL_CLASS%>"><b>Name</b></td>
                    <td class="<%=LABEL_CLASS%>" colspan="3"><bean:write name="detailForm" property="ownerTitle"/>
                    &nbsp;<bean:write name="detailForm" property="ownerFirstName"/>
                    &nbsp;<bean:write name="detailForm" property="ownerMiddleName"/>
                    &nbsp;<bean:write name="detailForm" property="ownerLastName"/>
                    &nbsp;<bean:write name="detailForm" property="ownerSuffix"/></td>
				</tr>
				
				 <tr id="">     
					<td class="<%=LABEL_CLASS%>" ><b>Gender</b></td>
					<td class="<%=LABEL_CLASS%>" align="left" >                        
						<logic:equal name="detailForm" property="ownerSex" value="M">
							<input type="radio" name="ownerSex" value="M" checked>Male</input>&nbsp;&nbsp;&nbsp;
							<input type="radio" name="ownerSex" value="F">Female</input>  
						</logic:equal>
						<logic:notEqual name="detailForm" property="insuredSex" value="M">
							<input type="radio" name="ownerSex" value="M" >Male</input>&nbsp;&nbsp;&nbsp;
							<input type="radio" name="ownerSex" value="F" checked>Female</input>  
						</logic:notEqual>
					<td class="<%=LABEL_CLASS%>">&nbsp;</td>	 	
                </tr>
				
				<tr >                   
					<td class="<%=LABEL_CLASS%>" ><b>Birth Date</b></td>
                    <td class="<%=LABEL_CLASS%>" ><bean:write name="detailForm" property="ownerBirthDate"/><input type="hidden" name="ownerBirthDate" " value="<bean:write name="detailForm" property="ownerBirthDate"/>" disabled>&nbsp;<div id="req_cal2" style="display:none;position:absolute;"></div></td> 
					<td class="<%=LABEL_CLASS%>" ><b>Age</b></td>
					<td class="<%=LABEL_CLASS%>" ><bean:write name="detailForm" property="ownerAge"/><input type="hidden" name="ownerAge" value="<bean:write name="detailForm" property="ownerAge"/>"></td>
			    </tr> 
			
	</table>
	
</div>

<div style="border: solid;border-width:1;position:relative; top:49; left:0.65; width=700px;"  >
			
	<table border="0" cellpadding="3" cellspacing="0" width="700px">
           
                <tr>
                    <td class="<%=LABEL_CLASS%>" width="0"><b>Agent</b></td>
                    <td class="<%=LABEL_CLASS%>"><ium:description type="<%=IUMConstants.LIST_USERS%>" queryCode="<%=agentCode%>"/><input type="hidden" name="agentCode" value="<bean:write name="detailForm" property="agentCode"/>"></td>
				    
					<td class="<%=LABEL_CLASS%>"  width="0"  ><b>Branch</b></td>
					<td class="<%=LABEL_CLASS%>"><ium:description type="<%=IUMConstants.LIST_BRANCHES%>" queryCode="<%=branchCode%>"/><input type="hidden" name="branchCode" value="<bean:write name="detailForm" property="branchCode"/>"></td>
				</tr>
                
	</table>

</div>       
			     
 <div style="border: solid;border-width:1;position:absolute; top:200; left:30 ; visibility:hidden">  
 <input type="hidden" name="searchClientType" value="">
                  <table border="0" cellpadding="3" cellspacing="0" width="<%=TABLE_WIDTH%>">
                    <tr>
                      <td class="<%=LABEL_CLASS%>"><b>&nbsp;&nbsp;Reference No.</b></td>
                      <td class="<%=LABEL_CLASS%>"><bean:write name="detailForm" property="refNo"/><bean:write name="detailForm" property="policySuffix"/><input type="hidden" name="refNo" value="<bean:write name="detailForm" property="refNo"/>"></td>
                      <td class="<%=LABEL_CLASS%>">&nbsp;</td>
                      <td class="<%=LABEL_CLASS%>"><b>&nbsp;&nbsp;Admin Sys Source</b></td>
                      <td class="<%=LABEL_CLASS%>"><ium:description type="<%=IUMConstants.LIST_SOURCE_SYSTEM%>" queryCode="<%=sourceSystem%>"/><input type="hidden" name="sourceSystem" value="<bean:write name="detailForm" property="sourceSystem"/>"></td>
                      <td class="<%=LABEL_CLASS%>">&nbsp;</td>
                      <td class="<%=LABEL_CLASS%>"><b>&nbsp;&nbsp;Date Created</b></td>
                      <td class="<%=LABEL_CLASS%>"><bean:write name="detailForm" property="dateCreated"/><input type="hidden" name="dateCreated" value="<bean:write name="detailForm" property="dateCreated"/>"></td>
                    </tr>
                    <tr>
                      <td class="<%=LABEL_CLASS%>"><b>&nbsp;&nbsp;Agent</b></td>
                      <td class="<%=LABEL_CLASS%>"><ium:description type="<%=IUMConstants.LIST_USERS%>" queryCode="<%=agentCode%>"/><input type="hidden" name="agentCode" value="<bean:write name="detailForm" property="agentCode"/>"></td>
                      <td class="<%=LABEL_CLASS%>">&nbsp;</td>
                      <td class="<%=LABEL_CLASS%>"><b>&nbsp;&nbsp;Branch</b></td>
                      <td class="<%=LABEL_CLASS%>" colspan="4"><ium:description type="<%=IUMConstants.LIST_BRANCHES%>" queryCode="<%=branchCode%>"/><input type="hidden" name="branchCode" value="<bean:write name="detailForm" property="branchCode"/>"></td>
                    </tr>
                    <tr>
                      <td class="<%=LABEL_CLASS%>" colspan="8">&nbsp;</td>
                    </tr>
                    <tr>
                      <td class="<%=LABEL_CLASS%>"><b>&nbsp;&nbsp;LOB</b></td>
                      <td class="<%=LABEL_CLASS%>"><ium:description type="<%=IUMConstants.LIST_LOB%>" queryCode="<%=lob%>"/><input type="hidden" name="lob" value="<bean:write name="detailForm" property="lob"/>"></td>
                      <td class="<%=LABEL_CLASS%>">&nbsp;</td>    
                      <td class="<%=LABEL_CLASS%>"><b>&nbsp;&nbsp;Amount Covered</b></td>
                      <td class="<%=LABEL_CLASS%>"><bean:write name="detailForm" property="amountCovered"/><input type="hidden" name="amountCovered" value="<bean:write name="detailForm" property="amountCovered"/>"></td>
                      <td class="<%=LABEL_CLASS%>">&nbsp;</td>    
                      <td class="<%=LABEL_CLASS%>"><b>&nbsp;&nbsp;Premium</b></td>
                      <td class="<%=LABEL_CLASS%>"><bean:write name="detailForm" property="premium"/><input type="hidden" name="premium" value="<bean:write name="detailForm" property="premium"/>"></td>
                    </tr>                    
                    <tr>
                      <td class="<%=LABEL_CLASS%>"><b>&nbsp;&nbsp;Date Received</b></td>
                      <td class="<%=LABEL_CLASS%>"><bean:write name="detailForm" property="receivedDate"/><input type="hidden" name="receivedDate" value="<bean:write name="detailForm" property="receivedDate"/>"></td>
                      <td class="<%=LABEL_CLASS%>">&nbsp;</td>
                      <% if ((Long.parseLong(currentStatus) == IUMConstants.STATUS_FOR_TRANSMITTAL_USD) ||
                             (Long.parseLong(currentStatus) == IUMConstants.STATUS_AWAITING_REQUIREMENTS) || 
                             (Long.parseLong(currentStatus) == IUMConstants.STATUS_AWAITING_MEDICAL)) { %> 
                      <td class="<%=LABEL_CLASS%>"><span class="required">*</span><b>Date Forwarded</b></td>
                      <td class="<%=LABEL_CLASS%>" width="200"><input type="text" name="dateForwarded" class="<%=LABEL_CLASS%>" style="width:150" value="<bean:write name="detailForm" property="dateForwarded"/>" onKeyUp="getKeyDate(event,this);" disabled>&nbsp;<div id="req_cal3" style="display:none;position:absolute;"><a href="#" onClick="popUpWin('<%=contextPath%>/jsp/calendar.jsp?textbox=requestForm.dateForwarded',290,155);"><img src="<%=contextPath%>/images/calendar.gif" border="0"></a></div></td>
                      <% } else { %> 
		              <td class="<%=LABEL_CLASS%>"><b>&nbsp;&nbsp;Date Forwarded</b></td>
					  <td class="<%=LABEL_CLASS%>"><bean:write name="detailForm" property="dateForwarded"/><input type="hidden" name="dateForwarded" value="<bean:write name="detailForm" property="dateForwarded"/>"></td>
                      <% } %>
                      <td class="<%=LABEL_CLASS%>">&nbsp;</td>
                      <td class="<%=LABEL_CLASS%>"><b>&nbsp;&nbsp;Date Edited</b></td>
                      <td class="<%=LABEL_CLASS%>"><bean:write name="detailForm" property="dateEdited"/><input type="hidden" name="dateEdited" value="<bean:write name="detailForm" property="dateEdited"/>"></td>
                    </tr>
                    <tr>
                      <td class="<%=LABEL_CLASS%>"><b>&nbsp;&nbsp;Location</b></td>


                      <% if ((Long.parseLong(currentStatus) == IUMConstants.STATUS_APPROVED) ||  
                             (Long.parseLong(currentStatus) == IUMConstants.STATUS_AR_CANCELLED) ||
                             (Long.parseLong(currentStatus) == IUMConstants.STATUS_DECLINED) ||
                             (Long.parseLong(currentStatus) == IUMConstants.STATUS_NOT_PROCEEDED_WITH)) { %>
                      <!-- <td class="<%=LABEL_CLASS%>"><bean:message key="message.location"/></td> -->
					  <td class="<%=LABEL_CLASS%>"><ium:description type="<%=IUMConstants.LIST_USERS%>" queryCode="<%=location%>"/><input type="hidden" name="location" value="<bean:write name="detailForm" property="location"/>"></td>
                      <% } else { %>
                      <td class="<%=LABEL_CLASS%>"><ium:description type="<%=IUMConstants.LIST_USERS%>" queryCode="<%=location%>"/><input type="hidden" name="location" value="<bean:write name="detailForm" property="location"/>"></td>
                      <% } %>
                      <td class="<%=LABEL_CLASS%>">&nbsp;</td>
                      <td class="<%=LABEL_CLASS%>"><span class="required">*</span><b>Assigned to</b></td>
                      <td class="<%=LABEL_CLASS%>"><ium:list className="label2" listBoxName="assignedTo" type="<%=IUMConstants.LIST_ASSIGN_TO%>" styleName="width:150" selectedItem="<%=assignedTo%>" disabled="true" filter="<%=filter%>"/></td>
                      <td class="<%=LABEL_CLASS%>">&nbsp;</td>
                      <td class="<%=LABEL_CLASS%>"><b>&nbsp;&nbsp;Underwriter</b></td>
                      <td class="<%=LABEL_CLASS%>"><ium:description type="<%=IUMConstants.LIST_USERS%>" queryCode="<%=underwriter%>"/><input type="hidden" name="underwriter" value="<bean:write name="detailForm" property="underwriter"/>"></td>
                    </tr>           
                    <tr>
                      <td class="<%=LABEL_CLASS%>" colspan="8">&nbsp;</td>
                    </tr>
                    
                    <!-- ******************** INSURED ******************** -->                     
                    <tr id="insured1">
                      <td class="<%=LABEL_CLASS%>"><b>&nbsp;&nbsp;Client Type</b></td>
                      <% if (lob.equals(IUMConstants.LOB_INDIVIDUAL_LIFE)) { %>
                      <td class="<%=LABEL_CLASS%>"><ium:list className="label2" listBoxName="insuredClientType" type="<%=IUMConstants.LIST_CLIENT_TYPE%>" styleName="width:150" selectedItem="<%=iSelected%>" onChange="switchClient(this.value);" filter="<%=IUMConstants.LOB_INDIVIDUAL_LIFE%>"/></td>
                      <% } else { %>
                      <td class="<%=LABEL_CLASS%>"><ium:description type="<%=IUMConstants.LIST_CLIENT_TYPE%>" queryCode="<%=iSelected%>"/><input type="hidden" name="insuredClientType" value="<bean:write name="detailForm" property="insuredClientType"/>"></td>
                      <% } %>
                      <td class="<%=LABEL_CLASS%>">&nbsp;</td>
                      <td class="<%=LABEL_CLASS%>"><b>&nbsp;&nbsp;Client No.</b></td>
                      <td class="<%=LABEL_CLASS%>"><bean:write name="detailForm" property="insuredClientNo"/><input type="hidden" name="insuredClientNo" value="<bean:write name="detailForm" property="insuredClientNo"/>"></td>
                      <td class="<%=LABEL_CLASS%>">&nbsp;</td>
                      <% if (lob.equals(IUMConstants.LOB_INDIVIDUAL_LIFE)) { %>
                      <td class="<%=LABEL_CLASS%>" colspan="2">&nbsp;<input type="checkbox" name="sameClientDataInd" value="<%=sameIndValue%>" <%=sameIndChecked%> disabled> Same insured and owner</td>
                      <% } else { %>
                      <td class="<%=LABEL_CLASS%>" colspan="2">&nbsp;</td>
                      <% } %>
                    </tr>
                    <tr id="insured2">
                      <td class="<%=LABEL_CLASS%>"><b>&nbsp;&nbsp;Last Name</b></td>
                      <td class="<%=LABEL_CLASS%>"><bean:write name="detailForm" property="insuredLastName"/><input type="hidden" name="insuredLastName" value="<bean:write name="detailForm" property="insuredLastName"/>"></td> 
                      <td class="<%=LABEL_CLASS%>">&nbsp;</td>
                      <td class="<%=LABEL_CLASS%>"><b>&nbsp;&nbsp;First Name</b></td>
                      <td class="<%=LABEL_CLASS%>"><bean:write name="detailForm" property="insuredFirstName"/><input type="hidden" name="insuredFirstName" value="<bean:write name="detailForm" property="insuredFirstName"/>"></td>
                      <td class="<%=LABEL_CLASS%>">&nbsp;</td>
                      <td class="<%=LABEL_CLASS%>"><b>&nbsp;&nbsp;Middle Name</b></td>
                      <td class="<%=LABEL_CLASS%>"><bean:write name="detailForm" property="insuredMiddleName"/><input type="hidden" name="insuredMiddleName" value="<bean:write name="detailForm" property="insuredMiddleName"/>"></td>
                    </tr>
                    <tr id="insured3">
                      <td class="<%=LABEL_CLASS%>"><b>&nbsp;&nbsp;Title</b></td>
                      <td class="<%=LABEL_CLASS%>"><bean:write name="detailForm" property="insuredTitle"/><input type="hidden" name="insuredTitle" value="<bean:write name="detailForm" property="insuredTitle"/>"></td> 
                      <td class="<%=LABEL_CLASS%>">&nbsp;</td>
                      <td class="<%=LABEL_CLASS%>"><b>&nbsp;&nbsp;Suffix</b></td>
                      <td class="<%=LABEL_CLASS%>"><bean:write name="detailForm" property="insuredSuffix"/><input type="hidden" name="insuredSuffix" value="<bean:write name="detailForm" property="insuredSuffix"/>"></td>
                      <td class="<%=LABEL_CLASS%>">&nbsp;</td>
                      <td class="<%=LABEL_CLASS%>"><b>&nbsp;&nbsp;Birth Date</b></td>
                      <td class="<%=LABEL_CLASS%>"><bean:write name="detailForm" property="insuredBirthDate"/><input type="hidden" name="insuredBirthDate" value="<bean:write name="detailForm" property="insuredBirthDate"/>"></td>
                    </tr>
                   
                    <tr id="insured4">
                      <td class="<%=LABEL_CLASS%>"><b>&nbsp;&nbsp;Age</b></td>
                      <td class="<%=LABEL_CLASS%>"><bean:write name="detailForm" property="insuredAge"/><input type="hidden" name="insuredAge" value="<bean:write name="detailForm" property="insuredAge"/>"></td>
                      <td class="<%=LABEL_CLASS%>">&nbsp;</td>
                      <td class="<%=LABEL_CLASS%>"><b>&nbsp;&nbsp;Gender</b></td>
                      <td class="<%=LABEL_CLASS%>" align="left">                        
                        <input type="radio" name="insuredSex4" value="M" <%=sexChecked_M%> disabled>Male</input>&nbsp;&nbsp;&nbsp;
                        <input type="radio" name="insuredSex4" value="F" <%=sexChecked_F%> disabled>Female</input>                        
                      </td>
                      <td class="<%=LABEL_CLASS%>">&nbsp;</td>
                      <% 
                      if(contextPath.equalsIgnoreCase(wmsContextPath)){ %>
                      <td class="<%=LABEL_CLASS%>" colspan="2">&nbsp;&nbsp;<a href="<%= SVLink %><bean:write name="detailForm" property="refNo"/><bean:write name="detailForm" property="policySuffix"/>" target="_blank">Signature Verification</a></td> 
                      <% } %>                        
                    </tr>
                    <% if (lob.equals(IUMConstants.LOB_INDIVIDUAL_LIFE)) { %>
                    <!-- ******************** OWNER ******************** -->                     
                    <tr id="owner1" style="display:none">
                      <td class="<%=LABEL_CLASS%>"><b>&nbsp;&nbsp;Client Type</b></td>
                      <td class="<%=LABEL_CLASS%>"><ium:list className="label2" listBoxName="ownerClientType" type="<%=IUMConstants.LIST_CLIENT_TYPE%>" styleName="width:150" selectedItem="<%=oSelected%>" onChange="switchClient(this.value);" filter="<%=IUMConstants.LOB_INDIVIDUAL_LIFE%>"/></td>
                      <td class="<%=LABEL_CLASS%>">&nbsp;</td>
                      <td class="<%=LABEL_CLASS%>"><b>&nbsp;&nbsp;Client No.</b></td>
                      <td class="<%=LABEL_CLASS%>" colspan="4"><bean:write name="detailForm" property="ownerClientNo"/><input type="hidden" name="ownerClientNo" value="<bean:write name="detailForm" property="ownerClientNo"/>"></td>
                    </tr>
                    <tr id="owner2" style="display:none">
                      <td class="<%=LABEL_CLASS%>"><b>&nbsp;&nbsp;Last Name</b></td>
                      <td class="<%=LABEL_CLASS%>"><bean:write name="detailForm" property="ownerLastName"/><input type="hidden" name="ownerLastName" value="<bean:write name="detailForm" property="ownerLastName"/>"></td> 
                      <td class="<%=LABEL_CLASS%>">&nbsp;</td>
                      <td class="<%=LABEL_CLASS%>"><b>&nbsp;&nbsp;First Name</b></td>
                      <td class="<%=LABEL_CLASS%>"><bean:write name="detailForm" property="ownerFirstName"/><input type="hidden" name="ownerFirstName" value="<bean:write name="detailForm" property="ownerFirstName"/>"></td>
                      <td class="<%=LABEL_CLASS%>">&nbsp;</td>
                      <td class="<%=LABEL_CLASS%>"><b>&nbsp;&nbsp;Middle Name</b></td>
                      <td class="<%=LABEL_CLASS%>"><bean:write name="detailForm" property="ownerMiddleName"/><input type="hidden" name="ownerMiddleName" value="<bean:write name="detailForm" property="ownerMiddleName"/>"></td>
                    </tr>
                    <tr id="owner3" style="display:none">
                      <td class="<%=LABEL_CLASS%>"><b>&nbsp;&nbsp;Title</b></td>
                      <td class="<%=LABEL_CLASS%>"><bean:write name="detailForm" property="ownerTitle"/><input type="hidden" name="ownerTitle" value="<bean:write name="detailForm" property="ownerTitle"/>"></td> 
                      <td class="<%=LABEL_CLASS%>">&nbsp;</td>
                      <td class="<%=LABEL_CLASS%>"><b>&nbsp;&nbsp;Suffix</b></td>
                      <td class="<%=LABEL_CLASS%>"><bean:write name="detailForm" property="ownerSuffix"/><input type="hidden" name="ownerSuffix" value="<bean:write name="detailForm" property="ownerSuffix"/>"></td>
                      <td class="<%=LABEL_CLASS%>">&nbsp;</td>
                      <td class="<%=LABEL_CLASS%>"><b>&nbsp;&nbsp;Birth Date</b></td>
                      <td class="<%=LABEL_CLASS%>"><bean:write name="detailForm" property="ownerBirthDate"/><input type="hidden" name="ownerBirthDate" value="<bean:write name="detailForm" property="ownerBirthDate"/>"></td>
                    </tr>
                    <%
                    String sex2 = ownerSex;
                    String sexChecked_M2 = "";
                    String sexChecked_F2 = "";
                    if (sex2 != null) {
                      if (sex2.equals("M")) {
                        sexChecked_M2 = "checked";
                      }
                      else if (sex2.equals("F")) {
                        sexChecked_F2 = "checked";
                      }
                    }
                    %>
                    <tr id="owner4" style="display:none">
                      <td class="<%=LABEL_CLASS%>"><b>&nbsp;&nbsp;Age</b></td>
                      <td class="<%=LABEL_CLASS%>"><bean:write name="detailForm" property="ownerAge"/><input type="hidden" name="ownerAge" value="<bean:write name="detailForm" property="ownerAge"/>"></td>
                      <td class="<%=LABEL_CLASS%>">&nbsp;&nbsp;Sex&nbsp;<%=sex2%>&nbsp;<%=sexChecked_F2%></td>
                      <td class="<%=LABEL_CLASS%>"><b>&nbsp;&nbsp;Gender</b></td>
                      <td class="<%=LABEL_CLASS%>" align="left">                        
                        <input type="radio" name="ownerSex4" value="M" <%=sexChecked_M2%> disabled>Male</input>&nbsp;&nbsp;&nbsp;
                        <input type="radio" name="ownerSex4" value="F" <%=sexChecked_F2%> disabled>Female</input>                        
                      </td>
                     
                      <td class="<%=LABEL_CLASS%>">&nbsp;</td>
                      <% 
                      if(contextPath.equalsIgnoreCase(wmsContextPath)){ %>
                      <td class="<%=LABEL_CLASS%>" colspan="2">&nbsp;&nbsp;<a href="<%= SVLink %><bean:write name="detailForm" property="refNo"/><bean:write name="detailForm" property="policySuffix"/>" target="_blank">Signature Verification</a></td> 
                      <% } %>                        
                    </tr>
                    <% } %>
                    <tr>
						<td>&nbsp;</td><td width="150">&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td width="150">&nbsp;</td><td>&nbsp;</td>
                        <% if(contextPath.equalsIgnoreCase(wmsContextPath)){ %>
                        <td class="<%=LABEL_CLASS%>" colspan="8">&nbsp;&nbsp;<a href="javascript:uwPopup('<%=UWPopupLink%>');">UW Assessment Analysis</a></td>
                    	<% } %>
                    </tr>
                  </table>

</div>
  <bean:define id="userProfile" name="userData" property="profile" />
  <script language="Javascript" type="text/javascript">
   	frm = document.forms[0];
   	if (frm.assignedTo.length == 1) {  // check if assigned to has no value ("Select User" is the only option)
		frm.assignedTo.options.add(new Option("<bean:write name='userProfile' property='lastName' />, <bean:write name='userProfile' property='firstName' />", "<bean:write name='userProfile' property='userId' />"));
		frm.assignedTo.options.selectedIndex = 1; // force the selection of the assigned to user
	}
  </script>  
</head>