<%@ page language="java" import="java.util.*, com.slocpi.ium.ui.form.*, com.slocpi.ium.util.*, com.slocpi.ium.ui.util.*, com.slocpi.ium.data.*" buffer="1024kb"%>
<%@ taglib uri="/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/struts-html.tld" prefix="html" %>
<%@ taglib uri="/ium.tld" prefix="ium" %>
<%String contextPath = request.getContextPath();%>
<%
/* Andre Ceasar Dacanay - css enhancement - start */
String iumCss = "";
String iumColorB = "";
String companyCode = "";
try {
	ResourceBundle rbWMSSP = ResourceBundle.getBundle("dbconfig");
	iumCss = rbWMSSP.getString("IUM_CSS_VERSION");
	companyCode = iumCss;
	iumCss = "_"+iumCss;
} catch (Exception e){}
if(iumCss.equals("_CP")){
	iumColorB="#2A4C7C";
	companyCode="";
}else if(iumCss.equals("_GF")){
	iumColorB="#303A77";
	companyCode = companyCode+" ";
}else{
	iumColorB="#2A4C7C";
}
/* Andre Ceasar Dacanay - css enhancement - end */
%>
<% AuditTrailFilterForm filterForm = (AuditTrailFilterForm) request.getAttribute("filterForm");
   String clickOnSearch = (String)request.getAttribute("searchIsTrue");
   
   String dateToday = DateHelper.format(new java.util.Date(), "ddMMMyyyy");
   String startDate = filterForm.getStartDate();
   String endDate = filterForm.getEndDate();
   String tableSelected = filterForm.getTable();
   String typeSelected = filterForm.getTransactionType();	
   if (startDate == null || startDate.equals("")){
     startDate = dateToday;
   }
  
   if (endDate == null || endDate.equals("")){
     endDate = dateToday;
   }
	
%>
<%
    // This routine queries the access rights of the users (RCRIVERA)
    //
    // --- START --- 
    
    StateHandler sessionHandler = new StateHandler();
    Access access = new Access();
	HashMap userAccess = sessionHandler.getUserAccess(request);
    if(!access.hasAccess(userAccess,IUMConstants.PAGE_AUDIT_TRAIL,IUMConstants.ACC_INQUIRE) ){                                                                                                                    
    	response.sendRedirect("noAccessPage.jsp");   
    } 
    
    // --- END ----
%>
    

<html>
  <head>
    <title><%=companyCode%>Integrated Underwriting and Medical System</title>
    <link href="<%=contextPath%>/css/pw_main.css" rel="stylesheet" type="text/css">
    <link href="<%=contextPath%>/css/main<%=iumCss%>.css" rel="stylesheet" type="text/css">
    <link href="<%=contextPath%>/css/styleCommon.css" rel="stylesheet" type="text/css">
    <script language="JavaScript" src="<%=contextPath%>/js/dhtml_init.js"></script>
    <script language="JavaScript" src="<%=contextPath%>/js/navigation.js"></script>
    <script language="Javascript" src="<%=contextPath%>/js/validation.js"></script>
    <script language="JavaScript" src="<%=contextPath%>/jsp/sl_left_nav.jsp"></script>
	<script language="JavaScript" src="<%=contextPath%>/jsp/validate_auditTrail.jsp"></script>
  </head>

  <body leftMargin="0" topmargin="0" marginwidth="0" marginheight="0" onUnload="closePopUpWin();">
    <table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%">
      <tr>
        <td width="100%" colspan="2">
          <jsp:include page="header.jsp" flush="true"/>
          <table border="0" cellpadding="0" cellspacing="0" width="100%" height="100%">
            <tr valign="top">
              <td background="<%=contextPath%>/images/bg_yellow.gif" id="leftnav" valign="top" width="147">
                <img height="9" src="<%=contextPath%>/images/sp.gif" width="147"><br>
                <img height="30" src="<%=contextPath%>/images/sp.gif" width="7"> <br>
                <img height="7" src="<%=contextPath%>/images/sp.gif" width="147"><br>
                <div id="level0" style="position: absolute; visibility: hidden; width: 100%; z-index: 500"> 
                  <script language=javascript>
	                <!--
                    document.write(writeMenu(''));
                    if (IE4) {
                      document.write(writeDiv());
                    };
                    //-->
                  </script>
                </div>
                <div id="NSFix" style="position: absolute; visibility: hidden; z-index: 999">&nbsp;</div>
                <!-- Netscape needs a hard-coded div to write dynamic DIVs --> 
                <script language=javascript>
                  <!--
                  if (NS4) {
                    document.write(writeDiv());
                  }
                  //-->
                </script>
                <script>
                  <!--
                  initMenu();
                  //-->
                </script>
              </td>

              <form name="auditForm" method="post">      
              <td width="100%"> 
                <div align="left"> 
                <table border="0" cellpadding="0" width="100%" cellspacing="0">
                  <tr> 
                    <td width="100%"> 
                      <div align="left"> 
                        <table border="0" cellpadding="3" cellspacing="0" width="100%" height="27"  background="images/sectionbkgray.jpg">
                          <tr> 
                            <td valign="top"> 
                               <span class="main">&nbsp;Integrated Underwriting and Medical System </span>
                            </td>
                          </tr>
                        </table>
                      </div>
                    </td>
                  </tr>
                  <tr> 
                    <td width="100%" colspan=2>&nbsp;</td>
                  </tr>
                  <tr> 
                    <td width="100%" colspan=2 class="title2">&nbsp;&nbsp;Audit Trail Maintenance</td>
                  </tr>          
                </table>

                <table width="100%" cellpadding="3" cellspacing="5" border="0">
                  <tr valign="top">
                  <td>
                   <table border="1" bordercolor="<%=iumColorB%>" cellpadding=10 cellspacing=0 bgcolor="" width="700" class="listtable1">
                     <tr>
                       <td colspan="2" height="100%">
                          <table border="0" cellpadding="2" cellspacing="1" bgcolor=#FFFFFF style="border: 0px solid #D1D1D1;" class="listtable1">
                            <tr>
                              <td>
                                <!--IFRAME src="tempAdminDetail.html" width="665" height="100" scrolling="no" frameborder="0" >
                                </IFRAME-->
                  
                			    <table border="0" width="669">
		        		          <tr>
		        		          <!-- Added hidden field to store today's Date -->
		        		            <input type="hidden" name="today" value="<%=dateToday%>">
		        		            <td width="250" class="label2"><b>Transaction Date</b><td/>
		        		            <td width="300" class="label2"><b>From &nbsp</b>
		        		            	<input type="text" name="startDate" class="label2" value="<%=startDate%>">
										<a href="#" onClick="popUpWin('<%=contextPath%>/jsp/calendar.jsp?textbox=auditForm.startDate',290,155);">
							            <img src="<%=contextPath%>/images/calendar.gif" height="17" width="19" border="0">
								        </a>
									</td>
		        		            <td width="300" class="label2"><b>To &nbsp</b>
		        		            	<input type="text" name="endDate" class="label2" value="<%=endDate%>"> 
										<a href="#" onClick="popUpWin('<%=contextPath%>/jsp/calendar.jsp?textbox=auditForm.endDate',290,155);">
							            <img src="<%=contextPath%>/images/calendar.gif" height="17" width="19" border="0">
								        </a>
		        		            </td>
		        		            <td width="100">&nbsp;</td>
		        		            <td width="100">&nbsp;</td>
		        		          </tr>
		        		          <tr>
		        		            <td width="150" class="label2" valign="top"><b>Transaction Record</b><td/>
		        		            <td width="300"><ium:list className="label2" listBoxName="table" type="<%=IUMConstants.LIST_TRANSACTION_RECORD%>" styleName="width:200" selectedItem="<%=tableSelected%>" onChange="" filter=""/></td>
		        		            <td width="100">&nbsp;</td>
		        		            <td width="100">&nbsp;</td>
		        		          </tr>
		        		          <tr>
		        		            <td width="150" class="label2" valign="top"><b>Transaction Type</b><td/>
		        		            <td width="300"><ium:list className="label2" listBoxName="transactionType" type="<%=IUMConstants.LIST_TRANSACTION_TYPE%>" styleName="width:200" selectedItem="<%=typeSelected%>" onChange="" filter=""/></td>
		        		            <td width="100">&nbsp;</td>
		        		            <td width="100">&nbsp;</td>
		        		          </tr>
		        		        </table>                  
                              </td>
                            </tr>
                          </table>
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
                
                <tr valign="top">
                  <td colspan="2"> 
                    <!--- START OF BODY -->
                    <table border="0" width="700">
                    	<tr><td align="left">
	                    <input type="button" value="Search" class="button1" onclick="searchAuditTrail()">&nbsp;
	                    </td>
	                    </tr>
	                  </table>  

                    <% if (clickOnSearch != null && clickOnSearch.equals("1")){%>
                    <table border="1" bordercolor="<%=iumColorB%>" cellpadding=10 cellspacing=0 bgcolor="" width="700" class="listtable1">
                      <tr>
                        <td colspan="2" height="100%">
                          <table border="0" cellpadding="2" cellspacing="1" bgcolor=#FFFFFF style="border: 1px solid #D1D1D1;" class="listtable1">
                            <tr class="headerrow1">
                              <td width="150">DATE</td>
                              <td width="300">TRANSACTION RECORD</td>
							  <td width="200">RECORD ID</td>
                              <td width="150">TYPE</td>
                              <td width="200">FROM</td>
                              <td width="200">CHANGED TO</td>
                            </tr>

                            <%
                            int i = 0;
                            String tr_class;
                            %>
                            <logic:iterate id="auditLog" name="auditTrailLogForm" property="auditTrailLog">
                            <%
							String transactionRecord = ((AuditTrailForm)auditLog).getTable();
                            if (i%2 == 0) {
                              tr_class = "row1";
                            } else {
                              tr_class = "row2";
                            }
                            %>
                            <tr>
                              <td class="<%=tr_class%>" width="100"><bean:write name="auditLog" property="transactionDate"/></td>
                              <td class="<%=tr_class%>" width="300"><ium:description type="<%=IUMConstants.LIST_TRANSACTION_RECORD%>" queryCode="<%=transactionRecord%>"/></td>
							  <td class="<%=tr_class%>" width="100"><bean:write name="auditLog" property="recordId"/></td>
                              <td class="<%=tr_class%>" width="100"><bean:write name="auditLog" property="transactionType"/></td>
                              <td class="<%=tr_class%>"><bean:write name="auditLog" property="fromValue"/></td>
                              <td class="<%=tr_class%>"><bean:write name="auditLog" property="toValue"/></td>
                            </tr>
                            <%i++;%>
                            </logic:iterate>

                            <% if (i == 0) { %>
                            <tr>
                              <td class="label2" colspan="3"><bean:message key="message.noexisting" arg0="audit trail"/></td>                                                    
                            </tr>
                            <% } %>
      
                            <!-- START Number of pages -->		
                            <bean:size id="noOfPages" name="page" property="pageNumbers" />
                            <% if (noOfPages.intValue() > 1) { %>
                            <tr>
                              <td class="headerrow4" width="100%" height="10" class="label2" valign="bottom" colspan="7">
                                <%
                                int pageNumber = Integer.parseInt((String)request.getAttribute("pageNo"));
                                String viewPage = "listAuditTrail.do";
                                int currLink = 1;
                                int prevLink = 1;
                                int firstPage = 1;
                                int lastPage = noOfPages.intValue();
                                %>

                                <!-- don't display link for previous page if the current page is the first page -->
                                <% if (pageNumber > firstPage) { %>
                                  <a href="#" onClick="javascript:rePaginate(1,'<%=viewPage%>');"><img src="<%=contextPath%>/images/pagnavfirst_off.gif" name="pagnavfirst" border="0" width="18" height="13"/></a>&nbsp;					
                                  <a href="#" onClick="javascript:rePaginate('<%=pageNumber-1%>','<%=viewPage%>');"><img src="<%=contextPath%>/images/pagnavleft_off.gif" name="pagnavleft" border="0" width="18" height="13"/></a>
                                <% } else {%>
                                  <img src="<%=contextPath%>/images/pagnavfirst_on.gif" name="pagnavfirst" border="0" width="18" height="13"/>&nbsp;
                                  <img src="<%=contextPath%>/images/pagnavleft_on.gif" name="pagnavleft" border="0" width="18" height="13"/>
                                <% } %>

                                <logic:iterate id="navLinks" name="page" property="pageNumbers">                                                            
                                  <% currLink = ((Integer)navLinks).intValue();%>
                                  <% if (((Integer)navLinks).intValue() == pageNumber) { %>
                                    <b><bean:write name="navLinks"/></b>
                                  <% } else { %>
                                    <% if ((currLink > (prevLink+1))) { %>
                                    ...
                                    <% } %>
                                    <a href="#" onClick="rePaginate('<bean:write name="navLinks"/>','<%=viewPage%>');" class="links2"><bean:write name="navLinks"/></a>
                                  <% } %>
                                  <%prevLink = currLink;%>
                                </logic:iterate>

                                <!-- don't display link for next page if the current page is the last page -->
                                <% if(pageNumber < lastPage) { %>
                                  <a href="#" onClick="rePaginate('<%=pageNumber+1%>','<%=viewPage%>');"><img src="<%=contextPath%>/images/pagnavright_off.gif" name="pagnavright" border="0" width="18" height="13"/></a>&nbsp;
                                  <a href="#" onClick="rePaginate('<%=prevLink%>','<%=viewPage%>');"><img src="<%=contextPath%>/images/pagnavlast_off.gif" width="18" height="13" border="0"></a>
                                <% } else { %>
                                  <img src="<%=contextPath%>/images/pagnavright_on.gif" width="18" height="13" border="0">&nbsp;
                                  <img src="<%=contextPath%>/images/pagnavlast_on.gif" width="18" height="13" border="0">
                                <% } %>
                              </td>
                            </tr>
                            <% } %>
                            <input type="hidden" name="pageNo" value="">
                            <!-- END Number of pages -->
                          </table>
                        </td>
                      </tr>
                    </table>
                    <% } %>
                    <input type="hidden" name="search" value="">
                    </td>
                  </tr>
                </table>
              </form>
            <!--- END OF BODY -->
          </td>
        </tr>
      </table>
    </div>
  </body>
</html>