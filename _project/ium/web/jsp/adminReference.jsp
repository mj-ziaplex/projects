<%@ page language="java" import="java.util.*, com.slocpi.ium.ui.form.*, com.slocpi.ium.util.*, com.slocpi.ium.ui.util.*, com.slocpi.ium.data.*" buffer="1024kb"%>
<%@ taglib uri="/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/struts-html.tld" prefix="html" %>
<%@ taglib uri="/ium.tld" prefix="ium" %>
<%String contextPath = request.getContextPath();%>
<%
/* Andre Ceasar Dacanay - css enhancement - start */
String iumCss = "";
String companyCode = "";
try {
	ResourceBundle rbWMSSP = ResourceBundle.getBundle("dbconfig");
	iumCss = rbWMSSP.getString("IUM_CSS_VERSION");
	companyCode = iumCss;
	iumCss = "_"+iumCss;
} catch (Exception e){}
if(iumCss.equals("_CP")){
	companyCode="";
}else if(iumCss.equals("_GF")){
	companyCode = companyCode+" ";
}
/* Andre Ceasar Dacanay - css enhancement - end */
%>
<ium:validateSession />

<jsp:useBean id="refCodeForm" scope="request" class="com.slocpi.ium.ui.form.ReferenceCodeListForm"/>

<%
    // This routine queries the access rights of the users (RCRIVERA)
    //
    // --- START --- 

	StateHandler sessionHandler = new StateHandler();
	Access access = new Access();
    
	HashMap userAccess = sessionHandler.getUserAccess(request);
    if(!access.hasAccess(userAccess,IUMConstants.PAGE_REFERENCE_CODES,IUMConstants.ACC_INQUIRE) ){                                                                                                                    
    	response.sendRedirect("noAccessPage.jsp");   
    } 
    boolean CREATE_ACCESS = true;
    boolean MAINTAIN_ACCESS = true;
    
    
    if(!access.hasAccess(userAccess,IUMConstants.PAGE_REFERENCE_CODES,IUMConstants.ACC_CREATE) ){                                                                                                                     
	  CREATE_ACCESS = false; 	
    }
    if(!access.hasAccess(userAccess,IUMConstants.PAGE_REFERENCE_CODES,IUMConstants.ACC_MAINTAIN) ){                                                                                                                     
	  MAINTAIN_ACCESS = false;
    }
    // --- END ---
 %>    
 
<%
UserData userData = sessionHandler.getUserData(request); 
UserProfileData userPref = userData.getProfile();
%>
<html>
  <head>
    <title>Integrated Underwriting and Medical System</title>
    <link href="<%=contextPath%>/css/main<%=iumCss%>.css" rel="stylesheet" type="text/css">
    <link href="<%=contextPath%>/css/styleCommon.css" rel="stylesheet" type="text/css">
    <script language="JavaScript" src="<%=contextPath%>/js/dhtml_init.js"></script>
    <script language="JavaScript" src="<%=contextPath%>/js/navigation.js"></script>
    <script language="JavaScript" src="<%=contextPath%>/jsp/sl_left_nav.jsp"></script>
    <script language="JavaScript" src="<%=contextPath%>/js/validation.js"></script>
    <script language="Javascript" src="<%=contextPath%>/jsp/validate_adm_ReferenceCodes.jsp"></script> 
  </head>

  <% String reqRefCode = request.getParameter("referenceCode"); %> 
  <% String maintainFlag = request.getParameter("maintainFlag"); %> 
  <% String pageNo = request.getParameter("pageNo"); %>
  <% String idxCode = request.getParameter("idxCode"); %>
  <%
  String aaCriteriaCode = "";
  if ((reqRefCode != null) && (reqRefCode.equals(IUMConstants.REF_CODE_AUTO_ASSIGN))) {
    CodeHelper codeHelper = new CodeHelper();
    Collection list = codeHelper.getCodeValue("autoAssignmentCriteria", refCodeForm.getCode());
    if (list.size() > 0) {
      NameValuePair nameValuePair = (NameValuePair)((ArrayList) list).get(0);
      aaCriteriaCode = nameValuePair.getName();
    }
  }
  int recordsNum = 0;
  if((ArrayList)refCodeForm.getListDetails() != null){
	 recordsNum = ((ArrayList)refCodeForm.getListDetails()).size();
  }
  %>

  <body leftMargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="initialize('<%=reqRefCode%>', '<%=aaCriteriaCode%>','<%=userPref.getRecordsPerView()%>','<%=recordsNum%>');" onUnload="closePopUpWin();">
    <form name="adminForm" method="post">
      <input type="hidden" name="maintainFlag" value="<%=maintainFlag%>">
      <input type="hidden" name="pageNo" value="<%=pageNo%>">
      <% if ((maintainFlag != null) && (maintainFlag.equals(IUMConstants.YES))) { %>
      <input type="hidden" name="idxUpdateCode" value="<%=idxCode%>">
      <% } %>
      <input type="hidden" name="sortOrder" value="<bean:write name="refCodeForm" property="sortOrder"/>">
      <input type="hidden" name="sortBy" value="<bean:write name="refCodeForm" property="sortBy"/>">
      <table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%">
        <tr>
          <td width="100%" colspan="2">
            <jsp:include page="header.jsp" flush="true"/>
            <table border="0" cellpadding="0" cellspacing="0" width="100%" height="100%">
              <tr valign="top">
                <td background="<%=contextPath%>/images/bg_yellow.gif" id="leftnav" valign="top" width="147">
                  <img height="9" src="<%=contextPath%>/images/sp.gif" width="147"><br>
                  <img height="30" src="<%=contextPath%>/images/sp.gif" width="7"> <br>
                  <img height="7" src="<%=contextPath%>/images/sp.gif" width="147"><br>
                  <div id="level0" style="position: absolute; visibility: hidden; width: 100%; z-index: 500"> 
                    <script language=javascript>
	                  <!--
                      document.write(writeMenu(''));
                      if (IE4) {
                        document.write(writeDiv());
                      };
                      //-->
                    </script>
                  </div>
                  <div id="NSFix" style="position: absolute; visibility: hidden; z-index: 999">&nbsp;</div>
                  <!-- Netscape needs a hard-coded div to write dynamic DIVs --> 
                  <script language=javascript>
                    <!--
                    if (NS4) {
                      document.write(writeDiv());
                    }
                    //-->
                  </script>
                  <script>
                    <!--
                    initMenu();
                    //-->
                  </script>
                </td>

                <td width="100%"> 
                  <div align="left"> 
                    <table border="0" cellpadding="0" width="100%" cellspacing="0">
                      <tr> 
                        <td width="100%"> 
                          <div align="left">
                            <table border="0" cellpadding="3" cellspacing="0" width="100%" height="27"  background="<%=contextPath%>/images/sectionbkgray.jpg">
                              <tr> 
                                <td valign="top"><span class="main">Integrated Underwriting and Medical System</span></td>
                              </tr>
                            </table>
                          </div>
                        </td>
                      </tr>
                      <tr> 
                        <td width="100%" class="title2">&nbsp;</td>
                      </tr>
                      <tr>
                        <td width="100%" class="title2">&nbsp;&nbsp;Reference Code Maintenance</td>
                      </tr>
                    </table>

                    <!--- START OF BODY -->
                    <table cellpadding="3" cellspacing="5" border="0" width="700">
                      <tr> 
                        <td class="label2">
                          <b>Select Reference Code </b>&nbsp;
                          <ium:list className="label2" listBoxName="referenceCode" type="<%=IUMConstants.LIST_REFERENCE_CODES%>" selectedItem="<%=reqRefCode%>" onChange="displayList();"/>
                        </td>
                      </tr>
                      
                      <% if ((reqRefCode != null) && !(reqRefCode.equals(""))) { %>

                      <!--******************** START Reference Code Input Fields ********************-->
                      <tr valign="top" id="body1">
                        <td>
                          <table border="1" bordercolor="#2A4C7C" cellpadding="10" cellspacing="0" width="100%" class="listtable1">
                            <tr>
                              <td>
                                <table border="0" cellpadding="2" cellspacing="2" class="listtable1" width="675">
                                  <tr><td colspan="2"><html:errors/></td></tr>
                                  <% String maxLength = ""; %>
                                  <% if (reqRefCode.equals(IUMConstants.REF_CODE_REQUIREMENT_FORMS)) { %>
                                    <%
                                    String formNameDisplay1 = "none";
                                    String formNameDisplay2 = "block";                                    
                                    if ((maintainFlag != null) && (maintainFlag.equals(IUMConstants.YES))) {
                                      formNameDisplay1 = "block";
                                      formNameDisplay2 = "none";
                                    }
                                    %>
                                  <tr id="formName1" style="display:<%=formNameDisplay1%>" height="25">
                                    <td width="150" class="label2"><b>Name</b></td>
                                    <td class="label2"><bean:write name="refCodeForm" property="formName"/></td>
                                  </tr>
                                  <tr id="formName2" style="display:<%=formNameDisplay2%>">
                                    <td width="150" class="label2" valign="top"><span class="required">*</span><b>Name</b></td>
                                    <td class="label2"><textarea name="formName" class="label2" rows="4" cols="55" onkeypress="limitText(this,'110')"><bean:write name="refCodeForm" property="formName"/></textarea></td>
                                  </tr>
                                  <tr>
                                    <td width="150" class="label2"><span class="required">*</span><b>Template Format</b></td>
                                    <td class="label2"><ium:list className="label2" listBoxName="templateFormat" type="<%=IUMConstants.LIST_FORM_TEMPLATE_FORMAT%>" styleName="width:350px" selectedItem="<%=refCodeForm.getTemplateFormat()%>"/></td>
                                  </tr>
                                  <tr>
                                    <td width="150" class="label2"><span class="required">*</span><b>Template Name</b></td>
                                    <td class="label2"><input type="text" name="templateName" class="label2" maxlength="100" style="width:350px" value="<bean:write name="refCodeForm" property="templateName"/>"></td>
                                  </tr>
                                  <% } %>

                                  <% if (reqRefCode.equals(IUMConstants.REF_CODE_AUTO_ASSIGN)) { %>
                                    <%
                                    String autoAssignDisplay1 = "none";
                                    String autoAssignDisplay2 = "block";                                    
                                    if ((maintainFlag != null) && (maintainFlag.equals(IUMConstants.YES))) {
                                      autoAssignDisplay1 = "block";
                                      autoAssignDisplay2 = "none";
                                    }
                                    %>
                                  <tr id="criteriaCode1" style="display:<%=autoAssignDisplay1%>" height="25">
                                    <td width="150" class="label2"><b>Criteria Code</b></td>
                                    <td class="label2"><ium:description type="<%=IUMConstants.LIST_AUTO_ASSIGN_CRITERIA%>" queryCode="<%=refCodeForm.getCode()%>"/></td>
                                  </tr>
                                  <tr id="criteriaCode2" style="display:<%=autoAssignDisplay2%>">
                                    <td width="150" class="label2"><span class="required">*</span><b>Criteria Code</b></td>
                                    <td class="label2"><ium:list className="label2" listBoxName="code" type="<%=IUMConstants.LIST_AUTO_ASSIGN_CRITERIA%>" styleName="width:350px" selectedItem="<%=refCodeForm.getCode()%>" onChange="switchAutoAssignDescription();"/></td>
                                  </tr>
                                  <% String code = refCodeForm.getDescription(); %>
                                  <tr id="desc_branch" style="display:none">
                                    <td width="150" class="label2"><span class="required">*</span><b>Criteria Value</b></td>
                                    <td class="label2"><ium:list className="label2" listBoxName="descBranch" type="<%=IUMConstants.LIST_BRANCHES%>" styleName="width:350" selectedItem="<%=code%>" onChange="setAutoAssignDescription();"/></td>
                                  </tr>
                                  <tr id="desc_assigned_to" style="display:none">
                                    <td width="150" class="label2"><span class="required">*</span><b>Criteria Value</b></td>
                                    <td class="label2"><ium:list className="label2" listBoxName="descAssignedTo" type="<%=IUMConstants.LIST_ASSIGN_TO%>" styleName="width:350" selectedItem="<%=code%>" onChange="setAutoAssignDescription();"/></td>
                                  </tr>
                                  <tr id="desc_agent" style="display:none">
                                    <td width="150" class="label2"><span class="required">*</span><b>Criteria Value</b></td>
                                    <td class="label2"><ium:list className="label2" listBoxName="descAgent" type="<%=IUMConstants.LIST_AGENT%>" styleName="width:350" selectedItem="<%=code%>" filter="<%=IUMConstants.ROLES_AGENTS%>" onChange="setAutoAssignDescription();"/></td>
                                  </tr>
                                  <tr id="desc_lob" style="display:none">
                                    <td width="150" class="label2"><span class="required">*</span><b>Criteria Value</b></td>
                                    <td class="label2"><ium:list className="label2" listBoxName="descLob" type="<%=IUMConstants.LIST_LOB%>" styleName="width:350" selectedItem="<%=code%>" onChange="setAutoAssignDescription();"/></td>
                                  </tr>
                                  <tr id="desc" style="display:block">
                                    <td width="150" class="label2"><span class="required">*</span><b>Criteria Value</b></td>
                                    <td class="label2"><input type="text" name="description" class="label2"  maxlength="25" style="width:350px" value="<bean:write name="refCodeForm" property="description"/>"></td>
                                  </tr>
                                  <tr>
                                    <td width="150" class="label2"><span class="required">*</span><b>Underwriter</b></td>
                                    <% String UWFilter = IUMConstants.ROLES_UNDERWRITER + "," + IUMConstants.ROLES_UNDERWRITER_SUPERVISOR; %> 
                                    <td class="label2"><ium:list className="label2" listBoxName="underwriter" type="<%=IUMConstants.LIST_USERS%>" styleName="width:350px" selectedItem="<%=refCodeForm.getUnderwriter()%>" filter="<%=UWFilter%>"/></td>
                                  </tr>
                                  <% } %>

                                  <!-- HOLIDAY DATE -->
                                  <% if (reqRefCode.equals(IUMConstants.REF_CODE_HOLIDAY)) { %>
                                  <tr>
                                    <td width="150" class="label2"><span class="required">*</span><b>Date</b></td>
                                    <td class="label2"><input type="text" name="date" class="label2" style="width:350px" value="<bean:write name="refCodeForm" property="date"/>">&nbsp;<div id="holiday_cal" style="display:none;position:absolute;"><a href="#" onClick="popUpWin('<%=contextPath%>/jsp/calendar.jsp?textbox=adminForm.date',290,155);"><img src="<%=contextPath%>/images/calendar.gif" border="0"></a></div></td>
                                  </tr>
                                  <% } %>

                                  <!-- CODE -->
                                  <% if ((reqRefCode.equals(IUMConstants.REF_CODE_LOB)) ||
                                         (reqRefCode.equals(IUMConstants.REF_CODE_DEPARTMENT)) ||
                                         (reqRefCode.equals(IUMConstants.REF_CODE_SECTION)) ||
                                         (reqRefCode.equals(IUMConstants.REF_CODE_EXAM_SPECIALIZATION)) ||
                                         (reqRefCode.equals(IUMConstants.REF_CODE_EXAM_PLACE)) ||
                                         (reqRefCode.equals(IUMConstants.REF_CODE_EXAM_AREA)) ||
                                         (reqRefCode.equals(IUMConstants.REF_CODE_DOCUMENT_TYPE)) ||
                                         (reqRefCode.equals(IUMConstants.REF_CODE_CLIENT_TYPE)) ||
                                         (reqRefCode.equals(IUMConstants.REF_CODE_AUTO_ASSIGN_CRITERIA)) ||
                                         (reqRefCode.equals(IUMConstants.REF_CODE_STATUS)) ||
                                         (reqRefCode.equals(IUMConstants.REF_CODE_ROLES)) ||
                                         (reqRefCode.equals(IUMConstants.REF_CODE_TEST_PROFILE)) ||
                                         (reqRefCode.equals(IUMConstants.REF_CODE_EXAM_RANK)) ||
                                         (reqRefCode.equals(IUMConstants.REF_CODE_MIB_IMPAIRMENT)) ||
                                         (reqRefCode.equals(IUMConstants.REF_CODE_MIB_NUMBER)) ||
                                         (reqRefCode.equals(IUMConstants.REF_CODE_MIB_ACTION)) ||
                                         (reqRefCode.equals(IUMConstants.REF_CODE_MIB_LETTER)) ||
                                         (reqRefCode.equals(IUMConstants.REF_CODE_ACCESS))) { %>
                                    <%
                                    //******************** SET MAXLENGTH FOR CODE ********************
                                    maxLength = "";
                                    if ((reqRefCode.equals(IUMConstants.REF_CODE_CLIENT_TYPE)) ||
                                        (reqRefCode.equals(IUMConstants.REF_CODE_MIB_NUMBER)) ||
                                        (reqRefCode.equals(IUMConstants.REF_CODE_MIB_LETTER))) {
                                      maxLength = "1";
                                    }
                                    else if ((reqRefCode.equals(IUMConstants.REF_CODE_LOB)) ||
                                             (reqRefCode.equals(IUMConstants.REF_CODE_ACCESS))) {
                                      maxLength = "2";
                                    }
                                    else if ((reqRefCode.equals(IUMConstants.REF_CODE_DOCUMENT_TYPE)) ||
                                             (reqRefCode.equals(IUMConstants.REF_CODE_EXAM_SPECIALIZATION)) ||
                                             (reqRefCode.equals(IUMConstants.REF_CODE_EXAM_PLACE)) ||
                                             (reqRefCode.equals(IUMConstants.REF_CODE_EXAM_AREA)) ||
                                             (reqRefCode.equals(IUMConstants.REF_CODE_EXAM_RANK)) ||
                                             (reqRefCode.equals(IUMConstants.REF_CODE_MIB_ACTION)) ||
                                             (reqRefCode.equals(IUMConstants.REF_CODE_HOLIDAY))) {
                                      maxLength = "3";
                                    }
                                    else if (reqRefCode.equals(IUMConstants.REF_CODE_STATUS)) {
                                      maxLength = "4";
                                    }
                                    else if (reqRefCode.equals(IUMConstants.REF_CODE_TEST_PROFILE)) {
                                      maxLength = "5";
                                    }
                                    else if (reqRefCode.equals(IUMConstants.REF_CODE_MIB_IMPAIRMENT)) {
                                      maxLength = "6";
                                    }
                                    else if (reqRefCode.equals(IUMConstants.REF_CODE_DEPARTMENT)) {
                                      maxLength = "10";
                                    }
                                    else if ((reqRefCode.equals(IUMConstants.REF_CODE_SECTION)) ||
                                             (reqRefCode.equals(IUMConstants.REF_CODE_ROLES))) {
                                      maxLength = "15";
                                    }
                                    else if (reqRefCode.equals(IUMConstants.REF_CODE_AUTO_ASSIGN_CRITERIA)) {
                                      maxLength = "20";
                                    }

                                    String codeDisplay1 = "none";
                                    String codeDisplay2 = "block";                                    
                                    if ((maintainFlag != null) && (maintainFlag.equals(IUMConstants.YES))) {
                                      codeDisplay1 = "block";
                                      codeDisplay2 = "none";
                                    }
                                    %>
                                  <tr id="code1" style="display:<%=codeDisplay1%>" height="25">
                                    <td width="150" class="label2"><b>Code</b></td>
                                    <td class="label2"><bean:write name="refCodeForm" property="code"/></td>
                                  </tr>
                                  <% if (reqRefCode.equals(IUMConstants.REF_CODE_AUTO_ASSIGN_CRITERIA)) { %>
                                  <tr id="code2" style="display:<%=codeDisplay2%>">
                                    <td width="150" class="label2"><span class="required">*</span><b>Code</b></td>
                                    <td class="label2"><ium:list className="label2" listBoxName="code" type="<%=IUMConstants.LIST_AUTO_ASSIGN_CRITERIA_LOV%>" styleName="width:350px" selectedItem="<%=refCodeForm.getCode()%>"/></td>
                                  </tr>
                                  <% } else { %>
                                  <tr id="code2" style="display:<%=codeDisplay2%>">
                                    <td width="150" class="label2"><span class="required">*</span><b>Code</b></td>
                                    <td class="label2"><input type="text" name="code" class="label2" value="" maxlength="<%=maxLength%>" style="width:350px"></td>
                                  </tr>
                                  <% }//AUTO_ASIGN_CRITERIA %>
                                  <% } %>

                                  <!-- DESCRIPTION -->
                                  <% if ((reqRefCode.equals(IUMConstants.REF_CODE_LOB)) ||
                                         (reqRefCode.equals(IUMConstants.REF_CODE_DEPARTMENT)) ||
                                         (reqRefCode.equals(IUMConstants.REF_CODE_SECTION)) ||
                                         (reqRefCode.equals(IUMConstants.REF_CODE_EXAM_SPECIALIZATION)) ||
                                         (reqRefCode.equals(IUMConstants.REF_CODE_EXAM_PLACE)) ||
                                         (reqRefCode.equals(IUMConstants.REF_CODE_EXAM_AREA)) ||
                                         (reqRefCode.equals(IUMConstants.REF_CODE_DOCUMENT_TYPE)) ||
                                         (reqRefCode.equals(IUMConstants.REF_CODE_CLIENT_TYPE)) ||
                                         (reqRefCode.equals(IUMConstants.REF_CODE_AUTO_ASSIGN_CRITERIA)) ||
                                         (reqRefCode.equals(IUMConstants.REF_CODE_STATUS)) ||
                                         (reqRefCode.equals(IUMConstants.REF_CODE_ROLES)) ||
                                         (reqRefCode.equals(IUMConstants.REF_CODE_ACCESS_TEMPLATE)) ||
                                         (reqRefCode.equals(IUMConstants.REF_CODE_TEST_PROFILE)) ||
                                         (reqRefCode.equals(IUMConstants.REF_CODE_EXAM_RANK)) ||
                                         (reqRefCode.equals(IUMConstants.REF_CODE_MIB_IMPAIRMENT)) ||
                                         (reqRefCode.equals(IUMConstants.REF_CODE_MIB_NUMBER)) ||
                                         (reqRefCode.equals(IUMConstants.REF_CODE_MIB_ACTION)) ||
                                         (reqRefCode.equals(IUMConstants.REF_CODE_MIB_LETTER)) ||
                                         (reqRefCode.equals(IUMConstants.REF_CODE_HOLIDAY)) ||
                                         (reqRefCode.equals(IUMConstants.REF_CODE_ACCESS)) ||
                                         (reqRefCode.equals(IUMConstants.REF_CODE_PAGE))) { %>
                                    <%
                                    //******************** SET MAXLENGTH FOR DESCRIPTION ********************
                                    maxLength = "";
                                    if ((reqRefCode.equals(IUMConstants.REF_CODE_LOB)) ||
                                        (reqRefCode.equals(IUMConstants.REF_CODE_CLIENT_TYPE)) ||
                                        (reqRefCode.equals(IUMConstants.REF_CODE_EXAM_PLACE)) ||
                                        (reqRefCode.equals(IUMConstants.REF_CODE_EXAM_AREA)) ||
                                        (reqRefCode.equals(IUMConstants.REF_CODE_ACCESS_TEMPLATE)) ||
                                        (reqRefCode.equals(IUMConstants.REF_CODE_ACCESS)) ||
                                        (reqRefCode.equals(IUMConstants.REF_CODE_PAGE))) {
                                      maxLength = "25";
                                    }
                                    else if ((reqRefCode.equals(IUMConstants.REF_CODE_DOCUMENT_TYPE)) ||
                                             (reqRefCode.equals(IUMConstants.REF_CODE_AUTO_ASSIGN_CRITERIA)) ||
                                             (reqRefCode.equals(IUMConstants.REF_CODE_EXAM_SPECIALIZATION)) ||
                                             (reqRefCode.equals(IUMConstants.REF_CODE_STATUS)) ||
                                             (reqRefCode.equals(IUMConstants.REF_CODE_TEST_PROFILE)) ||
                                             (reqRefCode.equals(IUMConstants.REF_CODE_EXAM_RANK)) ||
                                             (reqRefCode.equals(IUMConstants.REF_CODE_ROLES)) ||
                                             (reqRefCode.equals(IUMConstants.REF_CODE_MIB_NUMBER)) ||
                                             (reqRefCode.equals(IUMConstants.REF_CODE_MIB_ACTION)) ||
                                             (reqRefCode.equals(IUMConstants.REF_CODE_MIB_LETTER)) ||
                                             (reqRefCode.equals(IUMConstants.REF_CODE_HOLIDAY))) {
                                      maxLength = "40";
                                    }
                                    else if ((reqRefCode.equals(IUMConstants.REF_CODE_DEPARTMENT)) ||
                                             (reqRefCode.equals(IUMConstants.REF_CODE_SECTION))) {
                                      maxLength = "50";
                                    }
                                    else if (reqRefCode.equals(IUMConstants.REF_CODE_MIB_IMPAIRMENT)) {
                                      maxLength = "104";
                                    }
                                    %>

                                  <% if ((reqRefCode.equals(IUMConstants.REF_CODE_ACCESS_TEMPLATE) || reqRefCode.equals(IUMConstants.REF_CODE_PAGE)) && 
                                         ((maintainFlag != null) && (maintainFlag.equals(IUMConstants.YES)))) { %>
                                  <tr id="code1" style="display:block" height="25">
                                    <td width="150" class="label2"><b>Code</b></td>
                                    <td class="label2"><bean:write name="refCodeForm" property="code"/></td>
                                  </tr>
                                  <% } %>

                                  <tr>
                                    <td width="150" class="label2" valign="top"><span class="required">*</span><b>Description</b></td>
                                    <td class="label2"><textarea name="description" class="label2" rows="4" cols="55" onkeypress="limitText(this,'<%=maxLength%>')"><bean:write name="refCodeForm" property="description"/></textarea></td>
                                  </tr>
                                  <% } %>

                                  <!-- STATUS -->
                                  <% if ((reqRefCode.equals(IUMConstants.REF_CODE_REQUIREMENT_FORMS)) || 
                                         (reqRefCode.equals(IUMConstants.REF_CODE_AUTO_ASSIGN_CRITERIA))) { %>
                                  <tr>
                                    <td width="150" class="label2"><span class="required">*</span><b>Active</b></td>
                                    <td class="label2">
                                      <% 
                                      String statusChecked_YES = "";
                                      String statusChecked_NO = "";
                                      if ((maintainFlag != null) && (maintainFlag.equals(IUMConstants.YES))) {
                                        String status = refCodeForm.getStatus();
                                        if ((status != null) && (status.equals(IUMConstants.YES))) {
                                          statusChecked_YES = "checked";
                                        }
                                        else if ((status != null) && (status.equals(IUMConstants.NO))) {
                                          statusChecked_NO = "checked";
                                        }
                                      }
                                      %>
                                      <input type="radio" name="status" value="true" <%=statusChecked_YES%>>Yes</input>&nbsp;&nbsp;&nbsp;
                                      <input type="radio" name="status" value="false" <%=statusChecked_NO%>>No</input>                                        
                                    </td>
                                  </tr>
                                  <% } %>

                                  <!-- TYPE for status -->
                                  <% if (reqRefCode.equals(IUMConstants.REF_CODE_STATUS)) { %>
                                  <tr>
                                    <td width="150" class="label2"><span class="required">*</span><b>Type</b></td>
                                    <td class="label2"><ium:list className="label2" listBoxName="type" type="<%=IUMConstants.LIST_STATUS_TYPE%>" styleName="width:350px" selectedItem="<%=refCodeForm.getType()%>"/></td>
                                  </tr>
                                  <% } %>

                                  <!-- TYPE for test profile -->
                                  <% if (reqRefCode.equals(IUMConstants.REF_CODE_TEST_PROFILE)) { %>
                                  <tr>
                                    <td width="150" class="label2"><span class="required">*</span><b>Type</b></td>
                                    <td class="label2">
                                      <% 
                                      String typeChecked_M = "";
                                      String typeChecked_L = "";
                                      if ((maintainFlag != null) && (maintainFlag.equals(IUMConstants.YES))) {
                                        String type = refCodeForm.getType();
                                        if ((type != null) && (type.equals(IUMConstants.TEST_TYPE_MEDICAL))) {
                                          typeChecked_M = "checked";
                                        }
                                        else if ((type != null) && (type.equals(IUMConstants.TEST_TYPE_LABORATORY))) {
                                          typeChecked_L = "checked";
                                        }
                                      }
                                      %>
                                      <input type="radio" name="type" value="M" <%=typeChecked_M%>>Medical</input>&nbsp;&nbsp;&nbsp;
                                      <input type="radio" name="type" value="L" <%=typeChecked_L%>>Laboratory</input>                                        
                                    </td>
                                  </tr>

                                  <!-- NO. OF DAYS VALID -->
                                  <tr>
                                    <td width="150" class="label2"><span class="required">*</span><b>No. of Days Valid</b></td>
                                    <td class="label2"><input type="text" name="daysValid" class="label2" maxlength="3" style="width:350px" value="<bean:write name="refCodeForm" property="daysValid"/>"></td>
                                  </tr>

                                  <!-- TAXABLE -->
                                  <tr>
                                    <td width="150" class="label2"><span class="required">*</span><b>Taxable</b></td>
                                    <td class="label2">
                                      <% 
                                      String taxChecked_YES = "";
                                      String taxChecked_NO = "";
                                      if ((maintainFlag != null) && (maintainFlag.equals(IUMConstants.YES))) {
                                        String taxable = refCodeForm.getTaxable();
                                        if ((taxable != null) && (taxable.equals(IUMConstants.YES))) {
                                          taxChecked_YES = "checked";
                                        }
                                        else if ((taxable != null) && (taxable.equals(IUMConstants.NO))) {
                                          taxChecked_NO = "checked";
                                        }
                                      }
                                      %>
                                      <input type="radio" name="taxable" value="true" <%=taxChecked_YES%>>Yes</input>&nbsp;&nbsp;&nbsp;
                                      <input type="radio" name="taxable" value="false" <%=taxChecked_NO%>>No</input>                                        
                                    </td>
                                  </tr>
                                  <!-- FOLLOW UP NO. -->
                                  <tr>
                                    <td width="150" class="label2"><span class="required">*</span><b>Follow Up No.</b></td>
                                    <td class="label2"><input type="text" name="followUpNo" class="label2" maxlength="3" style="width:350px" value="<bean:write name="refCodeForm" property="followUpNo"/>"></td>
                                  </tr>
                                  <% } %>

                                  <!-- FEE -->
                                  <% if (reqRefCode.equals(IUMConstants.REF_CODE_EXAM_RANK)) { %>
                                  <tr>
                                    <td width="150" class="label2"><span class="required">*</span><b>Fee</b></td>
                                    <td class="label2"><input type="text" name="fee" class="label2" maxLength="13"style="width:350px" onKeyUp="getKeyAmount(event,this);" onBlur="validateCurrency(this, 'Fee');" value="<bean:write name="refCodeForm" property="fee"/>"></td>
                                  </tr>
                                  <% } %>

                                  <tr id="saveCancel">
                                    <td colspan="2" class="label2" align="left">
                                      <input type="button" class="button1" name="save" value="Save" onClick="saveRefCode();">&nbsp;
                                      <input type="button" class="button1" name="cancel" value="Cancel" onClick="cancelRefCode();">
                                    </td>
                                  </tr>
                                </table>
                              </td>
                            </tr>
                          </table>
                        </td>
                      </tr>
                      <!--******************** END Reference Code Input Fields ********************-->
                      <bean:size id="noOfPages" name="page" property="pageNumbers"/>

                      <tr id="createMaintain1">
                        <td class="label2" align="left">                          
                          <input type="button" class="button1" name="create" value="<%if (reqRefCode.equals(IUMConstants.REF_CODE_AUTO_ASSIGN)) {%> Map Criteria to Underwriter <%}else{%> Create <%}%>" onClick="createRefCode();" <% if (! reqRefCode.equals(IUMConstants.REF_CODE_ACCESS_TEMPLATE) && CREATE_ACCESS ) {  } else {%> disabled <% }%>>&nbsp;
                          <input type="button" class="button1" name="maintain" value="<%if (reqRefCode.equals(IUMConstants.REF_CODE_AUTO_ASSIGN)) {%> Edit Mapping <%}else{%> Maintain <%}%>" onClick="maintainRefCode();" <% if ((noOfPages.intValue() > 0) && MAINTAIN_ACCESS ) {  } else {%> disabled <%}%>>
                        </td>
                      </tr>

                      <!--******************** START Reference Code List ********************-->
                      <tr valign="top" id="body2">
                        <td>
                          <table border="1" bordercolor="#2A4C7C" cellpadding="10" cellspacing="0" width="100%" class="listtable1">
                            <tr>
                              <td>
                                <table border="0" cellpadding="2" cellspacing="2" class="listtable1" width="675">
                                  
                                  <!-- YEAR -->
                                  <% if (reqRefCode.equals(IUMConstants.REF_CODE_HOLIDAY)) { %>
                                  <tr>
                                    <%
                                    String year = refCodeForm.getYear();
                                    if ((year == null) || (year.equals(""))) {
                                      year = String.valueOf(Calendar.getInstance().get(Calendar.YEAR));
                                    }
                                    %>
                                    <td class="label2" colspan="10"><b>Year : </b><ium:list className="label2" listBoxName="year" type="<%=IUMConstants.LIST_HOLIDAY_YEAR%>" styleName="width:100px" selectedItem="<%=year%>" onChange="displayHolidayList();"/></td>
                                    <input type="hidden" name="selectedYear" value="<%=request.getParameter("year")%>">
                                  </tr>
                                  <% } %>

                                  <!-- HEADER -->
                                  <% String sortOrder = refCodeForm.getSortOrder(); %>
                                  <% String sortBy = refCodeForm.getSortBy(); %>
                                  <% if (reqRefCode.equals(IUMConstants.REF_CODE_AUTO_ASSIGN)) { %>
                                  <tr>
                                    <td class="headerrow1">&nbsp;</td>
                                    <td class="headerrow1">
                                    <% if (sortOrder.equals(IUMConstants.ASCENDING) && sortBy.equals(String.valueOf(IUMConstants.SORT_BY_REF_CRITERIA_CODE))) { %>
	                                  <a href="javascript:resort('<%=IUMConstants.SORT_BY_REF_CRITERIA_CODE%>','<%=IUMConstants.DESCENDING%>');" class="links1"><b>CRITERIA CODE</b><img src="<%=contextPath%>/images/uarrow.gif" border="0"></a>
                                    <% } else { %>
	                                  <a href="javascript:resort('<%=IUMConstants.SORT_BY_REF_CRITERIA_CODE%>','<%=IUMConstants.ASCENDING%>');" class="links1"><b>CRITERIA CODE</b><img src="<%=contextPath%>/images/darrow.gif" border="0"></a>
                                    <% } %>
                                    </td>
                                    <td class="headerrow1"><b>CRITERIA FIELD VALUE</b></td>
                                    <td class="headerrow1">
                                    <% if (sortOrder.equals(IUMConstants.ASCENDING) && sortBy.equals(String.valueOf(IUMConstants.SORT_BY_REF_UNDERWRITER))) { %>
	                                  <a href="javascript:resort('<%=IUMConstants.SORT_BY_REF_UNDERWRITER%>','<%=IUMConstants.DESCENDING%>');" class="links1"><b>UNDERWRITER</b><img src="<%=contextPath%>/images/uarrow.gif" border="0"></a>
                                    <% } else { %>
	                                  <a href="javascript:resort('<%=IUMConstants.SORT_BY_REF_UNDERWRITER%>','<%=IUMConstants.ASCENDING%>');" class="links1"><b>UNDERWRITER</b><img src="<%=contextPath%>/images/darrow.gif" border="0"></a>
                                    <% } %>
                                    </td>
                                  </tr>
                                  <% } else if (reqRefCode.equals(IUMConstants.REF_CODE_REQUIREMENT_FORMS)) { %>
                                  <tr>
                                    <td class="headerrow1">&nbsp;</td>
                                    <td class="headerrow1">
                                    <% if (sortOrder.equals(IUMConstants.ASCENDING) && sortBy.equals(String.valueOf(IUMConstants.SORT_BY_REF_FORM_NAME))) { %>
	                                  <a href="javascript:resort('<%=IUMConstants.SORT_BY_REF_FORM_NAME%>','<%=IUMConstants.DESCENDING%>');" class="links1"><b>FORM NAME</b><img src="<%=contextPath%>/images/uarrow.gif" border="0"></a>
                                    <% } else { %>
	                                  <a href="javascript:resort('<%=IUMConstants.SORT_BY_REF_FORM_NAME%>','<%=IUMConstants.ASCENDING%>');" class="links1"><b>FORM NAME</b><img src="<%=contextPath%>/images/darrow.gif" border="0"></a>
                                    <% } %>
                                    </td>
                                    <td class="headerrow1">
                                    <% if (sortOrder.equals(IUMConstants.ASCENDING) && sortBy.equals(String.valueOf(IUMConstants.SORT_BY_REF_TEMPLATE_FORMAT))) { %>
	                                  <a href="javascript:resort('<%=IUMConstants.SORT_BY_REF_TEMPLATE_FORMAT%>','<%=IUMConstants.DESCENDING%>');" class="links1"><b>TEMPLATE FORMAT</b><img src="<%=contextPath%>/images/uarrow.gif" border="0"></a>
                                    <% } else { %>
	                                  <a href="javascript:resort('<%=IUMConstants.SORT_BY_REF_TEMPLATE_FORMAT%>','<%=IUMConstants.ASCENDING%>');" class="links1"><b>TEMPLATE FORMAT</b><img src="<%=contextPath%>/images/darrow.gif" border="0"></a>
                                    <% } %>
                                    </td>
                                    <td class="headerrow1">
                                    <% if (sortOrder.equals(IUMConstants.ASCENDING) && sortBy.equals(String.valueOf(IUMConstants.SORT_BY_REF_TEMPLATE_NAME))) { %>
	                                  <a href="javascript:resort('<%=IUMConstants.SORT_BY_REF_TEMPLATE_NAME%>','<%=IUMConstants.DESCENDING%>');" class="links1"><b>TEMPLATE NAME</b><img src="<%=contextPath%>/images/uarrow.gif" border="0"></a>
                                    <% } else { %>
	                                  <a href="javascript:resort('<%=IUMConstants.SORT_BY_REF_TEMPLATE_NAME%>','<%=IUMConstants.ASCENDING%>');" class="links1"><b>TEMPLATE NAME</b><img src="<%=contextPath%>/images/darrow.gif" border="0"></a>
                                    <% } %>
                                    </td>
                                    <td class="headerrow1">
                                    <% if (sortOrder.equals(IUMConstants.ASCENDING) && sortBy.equals(String.valueOf(IUMConstants.SORT_BY_REF_ACTIVE))) { %>
	                                  <a href="javascript:resort('<%=IUMConstants.SORT_BY_REF_ACTIVE%>','<%=IUMConstants.DESCENDING%>');" class="links1"><b>ACTIVE</b><img src="<%=contextPath%>/images/uarrow.gif" border="0"></a>
                                    <% } else { %>
	                                  <a href="javascript:resort('<%=IUMConstants.SORT_BY_REF_ACTIVE%>','<%=IUMConstants.ASCENDING%>');" class="links1"><b>ACTIVE</b><img src="<%=contextPath%>/images/darrow.gif" border="0"></a>
                                    <% } %>
                                    </td>
                                    <td class="headerrow1">&nbsp;</td>
                                  </tr>
                                  <% } else { %>
                                  <tr>
                                    <td class="headerrow1">&nbsp;</td>
                                    <logic:iterate id="refCode" name="refCodeForm" property="listHeaders">
                                    <td class="headerrow1"><b><bean:write name="refCode"/></b></td>
                                    </logic:iterate>
                                  </tr>
                                  <% } %>

                                  <% if (noOfPages.intValue() == 0) { %>
                                  <tr>
                                    <td class="label2" colspan="14"><bean:message key="message.noexisting" arg0="reference codes"/></td>
                                  </tr>
                                  <% } else { %>

                                  <!-- LIST -->
                                  <%
                                  int i=0;
                                  String tr_class;
                                  %>
                                  <logic:iterate id="refCode" name="refCodeForm" property="listDetails">
                                  <%
                                  if (i%2 == 0) {
                                    tr_class = "row1";
                                  } else {
                                    tr_class = "row2";
                                  }
                                  %>
                                  <% if (reqRefCode.equals(IUMConstants.REF_CODE_REQUIREMENT_FORMS)) { %>
                                  <tr>
                                    <td class="<%=tr_class%>" align="center" width="5"><input type="radio" name="idxCode" value="<bean:write name="refCode" property="formName"/>"></td>
                                    <td class="<%=tr_class%>" align="left" width="270"><bean:write name="refCode" property="formName"/></td>
                                    <td class="<%=tr_class%>" align="center" width="100"><bean:write name="refCode" property="templateFormat"/></td>
                                    <td class="<%=tr_class%>" align="left" width="220"><bean:write name="refCode" property="templateName"/></td>                                              
                                    <td class="<%=tr_class%>" align="center" width="100"><bean:write name="refCode" property="status"/></td>
                                    <td class="<%=tr_class%>" align="center" width="5"><a href="<bean:write name="refCode" property="urlFile"/>" target="_blank"><img border="0" src="<%=contextPath%>/images/viewdoc.gif"></a></td>
                                  </tr>
                                  <% } else if ((reqRefCode.equals(IUMConstants.REF_CODE_LOB)) ||
                                                (reqRefCode.equals(IUMConstants.REF_CODE_DEPARTMENT)) ||
                                                (reqRefCode.equals(IUMConstants.REF_CODE_SECTION)) ||
                                                (reqRefCode.equals(IUMConstants.REF_CODE_EXAM_SPECIALIZATION)) ||
                                                (reqRefCode.equals(IUMConstants.REF_CODE_EXAM_PLACE)) ||
                                                (reqRefCode.equals(IUMConstants.REF_CODE_EXAM_AREA)) ||
                                                (reqRefCode.equals(IUMConstants.REF_CODE_DOCUMENT_TYPE)) ||
                                                (reqRefCode.equals(IUMConstants.REF_CODE_CLIENT_TYPE)) ||
                                                (reqRefCode.equals(IUMConstants.REF_CODE_ROLES)) ||
                                                (reqRefCode.equals(IUMConstants.REF_CODE_MIB_IMPAIRMENT)) ||
                                                (reqRefCode.equals(IUMConstants.REF_CODE_MIB_NUMBER)) ||
                                                (reqRefCode.equals(IUMConstants.REF_CODE_MIB_ACTION)) ||
                                                (reqRefCode.equals(IUMConstants.REF_CODE_MIB_LETTER)) ||
                                                (reqRefCode.equals(IUMConstants.REF_CODE_ACCESS)) ||
                                                (reqRefCode.equals(IUMConstants.REF_CODE_PAGE))) { %>
                                  <tr>
                                    <td class="<%=tr_class%>" align="center" width="5"><input type="radio" name="idxCode" value="<bean:write name="refCode" property="code"/>"></td>
                                    <td class="<%=tr_class%>" align="left" width="250"><bean:write name="refCode" property="code"/></td>
                                    <td class="<%=tr_class%>" align="left" width="445"><bean:write name="refCode" property="description"/></td>
                                  </tr>  
                                  <% } else if (reqRefCode.equals(IUMConstants.REF_CODE_AUTO_ASSIGN_CRITERIA)) { %>
                                  <tr>
                                    <td class="<%=tr_class%>" align="center" width="5"><input type="radio" name="idxCode" value="<bean:write name="refCode" property="code"/>"></td>
                                    <td class="<%=tr_class%>" align="left" width="245"><bean:write name="refCode" property="code"/></td>
                                    <td class="<%=tr_class%>" align="left" width="350"><bean:write name="refCode" property="description"/></td>
                                    <td class="<%=tr_class%>" align="center" width="100"><bean:write name="refCode" property="status"/></td>
                                  </tr>  
                                  <% } else if (reqRefCode.equals(IUMConstants.REF_CODE_STATUS)) { %>
                                  <tr>
                                    <td class="<%=tr_class%>" align="center" width="5"><input type="radio" name="idxCode" value="<bean:write name="refCode" property="code"/>"></td>
                                    <td class="<%=tr_class%>" align="center" width="50"><bean:write name="refCode" property="code"/></td>
                                    <td class="<%=tr_class%>" align="left" width="450"><bean:write name="refCode" property="description"/></td>
                                    <td class="<%=tr_class%>" align="left" width="195"><bean:write name="refCode" property="type"/></td>
                                  </tr>
                                  <% } else if (reqRefCode.equals(IUMConstants.REF_CODE_TEST_PROFILE)) { %>
                                  <tr>
                                    <td class="<%=tr_class%>" align="center" width="5"><input type="radio" name="idxCode" value="<bean:write name="refCode" property="code"/>"></td>
                                    <td class="<%=tr_class%>" align="center" width="50"><bean:write name="refCode" property="code"/></td>
                                    <td class="<%=tr_class%>" align="left" width="345"><bean:write name="refCode" property="description"/></td>
                                    <td class="<%=tr_class%>" align="center" width="100"><bean:write name="refCode" property="type"/></td>
                                    <td class="<%=tr_class%>" align="center" width="100"><bean:write name="refCode" property="daysValid"/></td>
                                    <td class="<%=tr_class%>" align="center" width="100"><bean:write name="refCode" property="taxable"/></td>
                                    <td class="<%=tr_class%>" align="center" width="100"><bean:write name="refCode" property="followUpNo"/></td>
                                  </tr>  
                                  <% } else if (reqRefCode.equals(IUMConstants.REF_CODE_EXAM_RANK)) { %>
                                  <tr>
                                    <td class="<%=tr_class%>" align="center" width="5"><input type="radio" name="idxCode" value="<bean:write name="refCode" property="code"/>"></td>
                                    <td class="<%=tr_class%>" align="center" width="50"><bean:write name="refCode" property="code"/></td>
                                    <td class="<%=tr_class%>" align="left" width="500"><bean:write name="refCode" property="description"/></td>
                                    <td class="<%=tr_class%>" align="right" width="145"><bean:write name="refCode" property="fee"/></td>
                                  </tr>
                                  <% } else if (reqRefCode.equals(IUMConstants.REF_CODE_AUTO_ASSIGN)) { %>
                                  <tr>
                                    <%
                                    ArrayList details = refCodeForm.getListDetails();
                                    ReferenceCodeForm refForm = (ReferenceCodeForm) details.get(i);
                                    String criteriaCode = refForm.getCode();
                                    String criteriaFieldValue = refForm.getDescription();
                                    String underwriter = refForm.getUnderwriter();

                                    CodeHelper codeHelper = new CodeHelper();
                                    Collection list = codeHelper.getCodeValue("autoAssignmentCriteria", criteriaCode);
                                    String code = "";
                                    if (list.size() > 0) {
                                      NameValuePair nameValuePair = (NameValuePair)((ArrayList) list).get(0);
                                      code = nameValuePair.getName();
                                    }
                                    %>
                                    <td class="<%=tr_class%>" align="center" width="5"><input type="radio" name="idxCode" value="<bean:write name="refCode" property="id"/>"></td>
                                    <td class="<%=tr_class%>" align="left" width="100"><ium:description type="<%=IUMConstants.LIST_AUTO_ASSIGN_CRITERIA%>" queryCode="<%=criteriaCode%>"/></td>
                                    <% if (code.equals(IUMConstants.CRITERIA_BRANCH)) { %>
                                    <td class="<%=tr_class%>" align="left" width="300"><ium:description type="<%=IUMConstants.LIST_BRANCHES%>" queryCode="<%=criteriaFieldValue%>"/></td>
                                    <% } else if (code.equals(IUMConstants.CRITERIA_AGENT)) { %>
                                    <td class="<%=tr_class%>" align="left" width="295"><ium:description type="<%=IUMConstants.LIST_USERS%>" queryCode="<%=criteriaFieldValue%>"/></td>
                                    <% } else if (code.equals(IUMConstants.CRITERIA_ASSIGNED_TO)) { %>
                                    <td class="<%=tr_class%>" align="left" width="295"><ium:description type="<%=IUMConstants.LIST_USERS%>" queryCode="<%=criteriaFieldValue%>"/></td>
                                    <% } else if (code.equals(IUMConstants.CRITERIA_LOB)) { %>
                                    <td class="<%=tr_class%>" align="left" width="295"><ium:description type="<%=IUMConstants.LIST_LOB%>" queryCode="<%=criteriaFieldValue%>"/></td>
                                    <% } else { %>
                                    <td class="<%=tr_class%>" align="left" width="300"><bean:write name="refCode" property="description"/></td>
                                    <% } %>
                                    <td class="<%=tr_class%>" align="left" width="295"><ium:description type="<%=IUMConstants.LIST_USERS%>" queryCode="<%=underwriter%>"/></td>
                                  </tr>
                                  <% } else if (reqRefCode.equals(IUMConstants.REF_CODE_ACCESS_TEMPLATE)) { %>
                                  <tr>
                                    <td class="<%=tr_class%>" align="center" width="5"><input type="radio" name="idxCode" value="<bean:write name="refCode" property="code"/>"></td>
                                    <td class="<%=tr_class%>" align="left" width="245"><bean:write name="refCode" property="code"/></td>
                                    <td class="<%=tr_class%>" align="left" width="300"><bean:write name="refCode" property="description"/></td>
                                    <td class="<%=tr_class%>" align="left" width="50"><bean:write name="refCode" property="id"/></td>
                                  </tr>
                                  <% } else if (reqRefCode.equals(IUMConstants.REF_CODE_HOLIDAY)) { %>
                                  <tr>
                                    <td class="<%=tr_class%>" align="center" width="5"><input type="radio" name="idxCode" value="<bean:write name="refCode" property="code"/>"></td>
                                    <td class="<%=tr_class%>" align="left" width="50"><bean:write name="refCode" property="date"/></td>
                                    <td class="<%=tr_class%>" align="left" width="300"><bean:write name="refCode" property="description"/></td>
                                  </tr>
                                  <% } %>
                                  <%i++;%>
                                  </logic:iterate>

                                  <% if (noOfPages.intValue() > 1) { %>
                                  <!--******************** START Number of pages ********************-->
                                  <tr>
                                    <td class="headerrow4" width="100%" height="10" class="label2" valign="bottom" colspan="14">                                              
                                    <%
                                    int pageNumber = Integer.parseInt((String)request.getAttribute("pageNo"));
                                    String viewPage = "viewReferenceCode.do";
                                    int currLink = 1;
                                    int prevLink = 1;
                                    int firstPage = 1;
                                    //int lastPage = noOfPages.intValue();
                                    
                                    Page displayPage = (Page) request.getAttribute("page");
                                    int lastPage = 0;
                                    ArrayList pageNos = (ArrayList)displayPage.getPageNumbers();
                                    if (pageNos.size() > 0) {
                                      lastPage = ((Integer)pageNos.get(noOfPages.intValue()-1)).intValue(); 
                                    }
                                    %>

                                    <!-- don't display link for previous page if the current page is the first page -->
                                    <% if (pageNumber > firstPage) { %>
                                      <a href="#" onClick="javascript:rePaginate(1,'<%=viewPage%>');"><img src="<%=contextPath%>/images/pagnavfirst_off.gif" name="pagnavfirst" border="0" width="18" height="13"/></a>&nbsp;					
                                      <a href="#" onClick="javascript:rePaginate('<%=pageNumber-1%>','<%=viewPage%>');"><img src="<%=contextPath%>/images/pagnavleft_off.gif" name="pagnavleft" border="0" width="18" height="13"/></a>
                                    <% } else {%>
                                      <img src="<%=contextPath%>/images/pagnavfirst_on.gif" name="pagnavfirst" border="0" width="18" height="13"/>&nbsp;
                                      <img src="<%=contextPath%>/images/pagnavleft_on.gif" name="pagnavleft" border="0" width="18" height="13"/>
                                    <% } %>

                                    <logic:iterate id="navLinks" name="page" property="pageNumbers">                                                            
                                      <% currLink = ((Integer)navLinks).intValue();%>
                                      <% if (((Integer)navLinks).intValue() == pageNumber) { %>
                                        <b><bean:write name="navLinks"/></b>
                                      <% } else { %>
                                        <% if ((currLink > (prevLink+1))) { %>
                                        ...
                                        <% } %>
                                        <a href="#" onClick="rePaginate('<bean:write name="navLinks"/>','<%=viewPage%>');" class="links2"><bean:write name="navLinks"/></a>
                                      <% } %>
                                      <%prevLink = currLink;%>
                                    </logic:iterate>

                                    <!-- don't display link for next page if the current page is the last page -->
                                    <% if(pageNumber < lastPage) { %>
                                      <a href="#" onClick="rePaginate('<%=pageNumber+1%>','<%=viewPage%>');"><img src="<%=contextPath%>/images/pagnavright_off.gif" name="pagnavright" border="0" width="18" hei!ht="13"/></a>&nbsp;
                                      <a href="#" onClick="rePaginate('<%=prevLink%>','<%=viewPage%>');"><img src="<%=contextPath%>/images/pagnavlast_off.gif" width="18" height="13" border="0"></a>
                                    <% } else { %>
                                       <img src="<%=contextPath%>/images/pagnavright_on.gif" width="18" height="13" border="0">&nbsp;
                                       <img src="<%=contextPath%>/images/pagnavlast_on.gif" width="18" heikght="13" border="0">
                                    <% } %>
                                    </td>
                                  </tr>
                                  <% } %>
                                  <!--******************** END Number of pages ********************-->
                                  <% } %>                                  
                                </table>
                              </td>
                            </tr>
                          </table>
                        </td>
                      </tr>
                      <!--******************** END Reference Code List ********************-->

                      <tr id="createMaintain2">
                        <td class="label2" align="left">                          
                          <input type="button" class="button1" name="create" value="<%if (reqRefCode.equals(IUMConstants.REF_CODE_AUTO_ASSIGN)) {%> Map Criteria to Underwriter <%}else{%> Create <%}%>" onClick="createRefCode();" <% if (! reqRefCode.equals(IUMConstants.REF_CODE_ACCESS_TEMPLATE) && CREATE_ACCESS ) {  } else {%> disabled <% }%>>&nbsp;
                          <input type="button" class="button1" name="maintain" value="<%if (reqRefCode.equals(IUMConstants.REF_CODE_AUTO_ASSIGN)) {%> Edit Mapping <%}else{%> Maintain <%}%>" onClick="maintainRefCode();" <% if ((noOfPages.intValue() > 0) && MAINTAIN_ACCESS ) {  } else {%> disabled <%}%>>
                        </td>
                      </tr>

                      <% } %>
                    </table>
                  </div>
                </td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </form>
  </body>
</html>