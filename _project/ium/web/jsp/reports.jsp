<%@ page language="java" 
         import="java.util.*, com.slocpi.ium.ui.form.*, com.slocpi.ium.util.*, com.slocpi.ium.ui.util.*" 
         buffer="1024kb"%>
<%@ taglib uri="/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/struts-html.tld" prefix="html" %>
<%@ taglib uri="/ium.tld" prefix="ium" %>
<ium:validateSession />
<%String contextPath = request.getContextPath();%>
<%
/* Andre Ceasar Dacanay - css enhancement - start */
String iumCss = "";
String iumColorB = "";
String iumColorO = "";
String companyCode = "";
try {
	ResourceBundle rbWMSSP = ResourceBundle.getBundle("dbconfig");
	iumCss = rbWMSSP.getString("IUM_CSS_VERSION");
	companyCode = iumCss;
	iumCss = "_"+iumCss;
} catch (Exception e){}
if(iumCss.equals("_CP")){
	iumColorB="#2A4C7C";
	iumColorO = "#FFCB00";
	companyCode="";
}else if(iumCss.equals("_GF")){
	iumColorB="#303A77";
	iumColorO = "#303A77";
	companyCode = companyCode+" ";
}else{
	iumColorB="#2A4C7C";
	iumColorO = "#FFCB00";
}
/* Andre Ceasar Dacanay - css enhancement - end */
%>
<%
    // This routine queries the access rights of the users (RCRIVERA)
    //
    // --- START --- 
    
    StateHandler sessionHandler = new StateHandler();
    Access access = new Access();
	HashMap userAccess = sessionHandler.getUserAccess(request);
    if(!access.hasAccess(userAccess,IUMConstants.PAGE_REPORTS,IUMConstants.ACC_INQUIRE) ){                                                                                                                    
    	response.sendRedirect("noAccessPage.jsp");   
    } 
    
    // --- END ----
%>
<% 
   String REPORT_MSG_NO_RESULT = "Sorry, no record is found for the parameters you supplied.";
   String REPORT_MSG_TRY_AGAIN = "Enter a different set of report parameters then try again.";

   int typeReport = 0;
   String selectedReport = request.getParameter("reportName");
   if (selectedReport != null) {
     typeReport = (new Integer(selectedReport)).intValue();
   }

%>
<html:html locale="true">
<html>
 <head>
  <title><%=companyCode%>Integrated Underwriting and Medical System</title>
   <link href="<%=contextPath%>/css/main.css" rel="stylesheet" type="text/css">
   <script language="Javascript" src="<%=contextPath%>/js/navigation.js"></script>
   <script language="Javascript" src="<%=contextPath%>/js/validation.js"></script> 
<script language="javascript" type="text/javascript">
<!--
 printerFriendly = null;
 popupwin = null;
 
 function setReport(reportType) {
  reportForm.reportName.value = reportType;
  return true;
 }
-->
</script>
 </head>
 <body leftmargin="0" topmargin="0" onLoad="document.forms[1].startDate.focus();" onUnload="closePopUpWin();closePrinterFriendly();">
 <form name="reportForm" method="post">
  <input type="hidden" name="reportName" value="<%=typeReport%>"/>
 </form>
  <table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%">
  <!-- application header -->
   <tr>
    <td width="100%" colspan="2"><jsp:include page="header.jsp" flush="true"/></td>
   </tr>
   <tr>
    <td class="label2" rowspan="2" bgcolor="<%=iumColorO %>" height="100%" width="20">
     <img src="<%=contextPath%>/images/spacer.gif" width="20">
    </td>
    <td class="label2" background="<%=contextPath%>/images/GradientGray.jpg" height="27" width="100%" valign="top">
     &nbsp;&nbsp;<span class="main">Integrated Underwriting and Medical System</span>
    </td>
   </tr>
  <!-- module body -->
   <tr>
    <td width="100%" height="100%" valign="top">
     <table cellpadding="0" cellspacing="0" border="0" bordercolor="<%=iumColorB%>" width="100%" height="100%">
      <tr>
       <td>
        <table cellpadding="0" cellspacing="0" border="0" width="100%" height="100%">
         <tr>
          <td>&nbsp;</td>
          <td valign="top">
           <table cellpadding="0" cellspacing="0" border="0" width="100%">
            <!-- page title -->
            <tr><td class="label1"><img src="<%=contextPath%>/images/reports_title.jpg" border="0"></td></tr>
            <tr><td>&nbsp;</td></tr> 			<!-- separates title from menu -->
            <tr><td>            
<%
  
  String reportTitles[] = {"TAT\nReport",
                           "Total Number of\nRequirements Ordered", 
                           "Statement of\nMedical Billing",
                           "Summary of\nMedical Billing",
                           "Number of\nApplications Approved",
                           "Summary of\nApplications by Status",
                           "List of Applications\nwith Pending Requirements",
                           "List of Applications\nwith Pending Medical Results"
                          };
   
                        
   String C_HIGHLIGHT_ON = iumColorB;
   String C_HIGHLIGHT_OFF = "#BBBBBB";
   
   String curBGColor;
   String cellClass = "tabLink";

	out.println ("<table cellpadding='0' cellspacing='0'>");
	out.println ("<tr>");
	
    for (int i=0; i<reportTitles.length; i++)
    {
       curBGColor = C_HIGHLIGHT_OFF;
       if (typeReport == i)
       {
         curBGColor = C_HIGHLIGHT_ON;
         out.println ("<td bgcolor='" + curBGColor + "'>");
		 cellClass = "tabSelected";
       }
       else {
                
         out.println ("<td onmouseover=this.style.backgroundColor='" + C_HIGHLIGHT_ON + "' " 
                        + "onmouseout=this.style.backgroundColor='" + C_HIGHLIGHT_OFF + "' "
                        + "bgcolor='" + curBGColor + "' "
                        + "style='cursor:hand'>");
		 cellClass = "tabLink";                        
       }
       
       out.println ("<table border='0' cellpadding='3' cellspacing='0'>");
       out.println ("<tr>");

       if (typeReport != i) out.println ("<a href=\"javascript:setReport("+ i + "); gotoPage('reportForm', 'viewReports.do');\">");		
       out.println ("<td class='" + cellClass + "'>");
       out.println ("<b>" + reportTitles[i] + "</b>");
       out.println ("</td>");
       if (typeReport != i) out.println ("</a>");
       out.println ("</tr>");
       out.println ("</table>");       
       
        
       out.println ("</td>");

        
       out.println ("<td>&nbsp;</td>");
    }
    
    out.println ("</tr>");
    out.println ("</table>");
%>            
		        </td>
		    </tr> <!-- reports menu -->
            <tr><td bgcolor="<%=iumColorB%>">&nbsp;</td></tr>						   <!-- separates menu from report details -->
	 	    <tr>
 	 	     <td>
 	 	      <!-- report details start -->
<%  
 String reportPage[] = {
                        "reportTAT.jsp",
                        "reportReqtOrd.jsp",
                        "reportStmtMedBill.jsp",
                        "reportSummMedBill.jsp",
                        "reportNumberApprovedApps.jsp",          
                        "reportSummAppStat.jsp",
                        "reportFollowReqt.jsp",
                        "reportFollowMed.jsp"
                         };
%> 	 	     
              <jsp:include page="<%=reportPage[typeReport]%>" flush="true">
              	<jsp:param name="msgnoresult" value = "<%=REPORT_MSG_NO_RESULT%>" /> 
              	<jsp:param name="msgtryagain" value = "<%=REPORT_MSG_TRY_AGAIN%>" />
			  </jsp:include>
           	  <!-- report details end -->
             </td>
            </tr>
           </table>
          </td>
         </tr>
        </table>        
       </td>
      </tr>
	  <!-- application footer -->
	  <tr height="10%">
		<td class="label2"><br><br>&nbsp;� 2003 Sun Life Financial. All rights reserved.<br><br></td>
	  </tr>      
     </table>
    </td>
   </tr>
 </table>  
 </body>
</html>
</html:html>