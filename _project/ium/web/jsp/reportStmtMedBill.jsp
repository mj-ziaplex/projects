<%@ page language="java" 
         import="java.util.*, com.slocpi.ium.ui.form.*, com.slocpi.ium.util.*, com.slocpi.ium.ui.util.*, com.slocpi.ium.data.MedicalExamBillingFilter, com.slocpi.ium.data.MedicalExamBillingData, java.text.*" 
         buffer="1024kb"%>
<%@ taglib uri="/struts-bean.tld"  prefix="bean" %>
<%@ taglib uri="/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/struts-html.tld"  prefix="html" %>
<%@ taglib uri="/ium.tld"          prefix="ium" %>
<ium:validateSession />
<%String contextPath = request.getContextPath();%>
<%
    // This routine queries the access rights of the users (RCRIVERA)
    //
    // --- START --- 
    
    StateHandler sessionHandler = new StateHandler();
    Access access = new Access();
	HashMap userAccess = sessionHandler.getUserAccess(request);
    if(!access.hasAccess(userAccess,IUMConstants.PAGE_REPORTS,IUMConstants.ACC_INQUIRE) ){                                                                                                                    
    	response.sendRedirect("noAccessPage.jsp");   
    } 
    
    // --- END ----
%>
<%

// page processing
 
// get the form data
 ReportStmtMedBillForm reportForm = null;
 Page displayPage = null;
 
 reportForm = (ReportStmtMedBillForm) request.getAttribute("reportFilter");
 if (reportForm != null) { // report content populated
 	displayPage = reportForm.getPage();
	request.setAttribute("displayPage", displayPage);
 }
 else {
 // intialize the form object
    reportForm = new ReportStmtMedBillForm();
    reportForm.setReportType(String.valueOf(MedicalExamBillingFilter.REPORT_TYPE_AGENT));
 }
 request.setAttribute("reportForm", reportForm);
%>
<link href="<%=contextPath%>/css/pw_main.css" rel="stylesheet" type="text/css">
<link href="<%=contextPath%>/css/styleCommon.css" rel="stylesheet" type="text/css">
<link href="<%=contextPath%>/css/main.css" rel="stylesheet" type="text/css">
<script language="Javascript" src="<%=contextPath%>/js/navigation.js"></script>
<script language="Javascript" src="<%=contextPath%>/js/validation.js"></script>
<form name="formStmtMed" method="post">
<input type="hidden" name="reportName" value="<%=request.getParameter("reportName")%>"/>
<table cellpadding="1" cellspacing="0" border="1" width="100%" height="100%" bordercolor="#2A4C7C">
 <!-- report paramters -->
 <tr>
 	<td>
     <table cellpadding="1" cellspacing="0" border="0" width="100%" height="100%">
	  <tr><td colspan="2" class="label2"><b>Enter Report Parameters</b></td></tr>
      <tr>
       <td>
       <table class="listtable1" cellpadding="1" cellspacing="0" border="1" width="100%" height="100%" bordercolor="#2A4C7C">
        <tr>
         <td>
   	       <table cellpadding="1" cellspacing="0" border="0" width="100%" height="100%">
	        <tr>
	         <td>
	          <table cellpadding="1" cellspacing="0" border="0" width="100%" height="100%">
	           <tr><td class="label2"><b>Medical Exam Date Conducted</b></td></tr>
	           <tr>
	            <td>
	             <table class="listtable1" cellpadding="1" cellspacing="0" border="1" width="35%" bordercolor="#2A4C7C">
			      <tr>
			       <td>
  	  	            <!-- date period -->
	  	            <table cellpadding="2" cellspacing="0" border="0" width="100%">
	  	             <tr>
	  		          <td class="label2" width="36%"><b>Start Date</b></td>
  	  		          <td width="35%"><html:text styleClass="label2" name="reportForm" property="startDate" onkeyup="getKeyDate(event, this);" /></td>
	  		          <td width="29%">
						<a href="#" onClick="popUpWin('<%=contextPath%>/jsp/calendar.jsp?textbox=formStmtMed.startDate',290,155);">
				            <img src="<%=contextPath%>/images/calendar.gif" height="17" width="19" border="0">
				        </a>
	  		          </td>
	  		         </tr>
	  		         <tr>
	  		          <td class="label2"><b>End Date</b></td>
  	  		          <td><html:text styleClass="label2" name="reportForm" property="endDate" onkeyup="getKeyDate(event, this);" /></td>
	  		          <td>
						<a href="#" onClick="popUpWin('<%=contextPath%>/jsp/calendar.jsp?textbox=formStmtMed.endDate',290,155);">
				            <img src="<%=contextPath%>/images/calendar.gif" height="17" width="19" border="0">
				        </a>
	  		          </td>
	  		         </tr>
	  	            </table>
			       </td>
	              </tr>
	             </table>
	            </td>
	           </tr>
               <tr><td>&nbsp;</td></tr>
               <tr>
                <td>
                 <!-- Report Types -->
                 <table cellpadding="2" cellspacing="0" border="0" width="65%">
                  <tr>
               	   <td class="label2" width="15%"><b>Report Type</b></td>
            	   <td class="label2" width="5%">
            	   	<input type="radio" name="reportType" value="<%=MedicalExamBillingFilter.REPORT_TYPE_AGENT%>" onClick="manageFilters();"
            	   		<logic:equal name="reportForm" property="reportType" value="<%=MedicalExamBillingFilter.REPORT_TYPE_AGENT%>">checked</logic:equal>
           	   		/>
            	   </td>
            	   <td class="label2" width="15%"><b>Agent</b></td>
            	   <td class="label2" width="5%">
            	   	<input type="radio" name="reportType" value="<%=MedicalExamBillingFilter.REPORT_TYPE_EXAMINER%>" onClick="manageFilters();" 
            	   		<logic:equal name="reportForm" property="reportType" value="<%=MedicalExamBillingFilter.REPORT_TYPE_EXAMINER%>">checked</logic:equal>
            	   	/>
            	   </td>
            	   <td class="label2" width="15%"><b>Examiner</b></td>
            	   <td class="label2" width="5%">
            	   	<input type="radio" name="reportType" value="<%=MedicalExamBillingFilter.REPORT_TYPE_LABORATORY%>" onClick="manageFilters();"
            	   		<logic:equal name="reportForm" property="reportType" value="<%=MedicalExamBillingFilter.REPORT_TYPE_LABORATORY%>">checked</logic:equal>
            	   		 />
            	   	</td>
            	   <td class="label2" width="28%"><b>Laboratory</b></td>
                  </tr>
                 </table>          
                </td>
               </tr>
               <tr><td>&nbsp;</td></tr>
               <tr><td class="label2"><b>Filters</b></td></tr>
               <tr>
                <td>
				 <table class="listtable1" cellpadding="1" cellspacing="0" border="1" width="90%" bordercolor="#2A4C7C">
				  <tr>
				   <td>
	  		        <!-- Filters -->
	  		        <table cellpadding="2" cellspacing="0" border="0" width="100%" height="100%">
	  		         <tr>
	  		          <td class="label2" width="12%"><b>Agent</b></td>
  	  		          <td width="30%">
  	  		           <ium:list className="label2" listBoxName="agent" type="<%=IUMConstants.LIST_AGENT%>" selectedItem="<%=reportForm.getAgent()%>" styleName="width:200" filter="<%=IUMConstants.ROLES_AGENTS%>"/>
  	  		          </td>
	  		          <td class="label2" width="10%"><b>Examiner</b></td>
  	  		          <td width="48%">
  	  		           <ium:list className="label2" listBoxName="examiner" type="<%=IUMConstants.LIST_EXAMINERS%>" selectedItem="<%=reportForm.getExaminer()%>" styleName="width:200"/>
  	  		          </td>  	  		          
	  		         </tr>
	  		         <tr>
	  		          <td class="label2"><b>Laboratory</b></td>
  	  		          <td>
  	  		           <ium:list className="label2" listBoxName="laboratory" type="<%=IUMConstants.LIST_LABORATORIES%>" selectedItem="<%=reportForm.getLaboratory()%>" styleName="width:200"/>
  	  		          </td>
  	  		          <td>&nbsp;</td>  	  		          
	  		         </tr>
	  		         <tr>
	  		         </tr>
	  		        </table>	  				 
	  		       </td>
	  		      </tr>
	  		     </table>
                </td>
               </tr>	     
	          </table>
   	         </td>  
	        </tr>
	        <tr>
	         <td>
	          <table cellpadding="1" cellspacing="0" border="0" width="90%">
	           <tr>
	            <td>
		         	<input type="button" class="button1" value="Generate" style="width:100" onClick="generateReport();" />
		         	&nbsp;
	    	     	<input type="button" class="button1" value="Reset" style="width:100" onClick="gotoPage('reportForm', 'viewReports.do')"/>
	    	     </td>
	    	    </tr>
	    	   </table>
	         </td>
	        </tr>
	       </table> <!-- end of parameter grouping -->
   	      </td>
	     </tr>
	    </table> <!-- end of Report Parameters border -->
	   </td>
       <td width="12%">&nbsp;</td> <!-- minimizing horizontal scrolling -->
	  </tr>
     </table> <!-- end of Report Parameters container -->      
 	</td>
 </tr>
 <input type="hidden" name="pageNo" value="<%=reportForm.getPageNo()%>">
 <!-- report content --> 
<%
   int i=0;
   String tr_class;
   String td_bgcolor;
   String msgline = "&nbsp;";

   if (displayPage != null) {
   	
   	String HEADER_EXAMLAB = "Examiner/Laboratory";
   	String HEADER_AGENT = "Agent";

   	String colHeader = HEADER_EXAMLAB;
   	boolean isAgentType = false;
   	
   	if (Integer.valueOf(reportForm.getReportType()).intValue() == MedicalExamBillingFilter.REPORT_TYPE_AGENT) {
   		colHeader += "/" + HEADER_AGENT;
   		isAgentType = true;
   	}

%>
 <tr>
  <td>
   <table border="0" cellpadding="2" cellspacing="1" bgcolor="#FFFFFF" style="border: 1px solid #D1D1D1;width:88%;" class="listtable1">
<%
	if (displayPage.getList().size() > 0) {
	 int colspan = 12;
	 if (isAgentType) colspan++;
%>
    <tr>
    	<td colspan="<%=colspan%>" class="label2" align="right">
    		<a href="#" onClick="javascript:showPrinterFriendly();">Printer Friendly</a>
    	</td>
    </tr>
<%
	}
%>
    <tr class="headerrow1">
     <td><%=colHeader%></td>
	 <td>Sun Life Office/Branch</td>
     <td>Exam Date</td>
	 <td>Exam Type</td>
     <td>Application/Policy No.</td>
	 <td>Name of Insured</td>
     <td>LOB</td>
	 <td>Reason</td>
     <td>Place of Exam</td>
	 <td>Exam Fee</td>
     <td>Tax W/held</td>
	 <% if (isAgentType) { %><td>Chargeable Amount</td><% }%>
     <td>Payable Amount</td>
    </tr>
    <logic:iterate id="rpt" name="displayPage" property="list">
<%

	NumberFormat nf = NumberFormat.getNumberInstance(); 
    DecimalFormat df = (DecimalFormat)nf; 
    df.applyPattern("###,##0.00"); 
    
    String examFee = ((MedicalExamBillingData)rpt).getExamFee();
    String taxWithheld = ((MedicalExamBillingData)rpt).getTaxWithheld();
    String payableAmt = ((MedicalExamBillingData)rpt).getPayableAmt();
    
    examFee = df.format(Double.parseDouble(examFee == null?"0":examFee)); 
    taxWithheld = df.format(Double.parseDouble(taxWithheld==null?"0":taxWithheld)); 
    payableAmt = df.format(Double.parseDouble(payableAmt==null?"0":payableAmt)); 
   
   if (i%2 == 0) {
   	tr_class = "row1";
    td_bgcolor = "#CECECE";
   }
   else {
    tr_class = "row2";
    td_bgcolor = "#EDEFF0";
   }
%>
  <tr class=<%=tr_class%>>
   <td><bean:write name='rpt' property='name' /></td>
   <td><bean:write name="rpt" property="branch" /></td>
   <td><bean:write name='rpt' property='examDate' /></td>
   <td><bean:write name="rpt" property="examType" /></td>
   <td><bean:write name="rpt" property="policyNumber" /></td>
   <td><bean:write name='rpt' property='insured' /></td>
   <td><bean:write name="rpt" property="lob" /></td>
   <td><bean:write name='rpt' property='reason' /></td>
   <td><bean:write name="rpt" property="examPlace" /></td>
   <td align="right"><%=examFee%></td>
   <td align="right"><%=taxWithheld%></td>
   <% if (isAgentType) { 
	   String chargeableAmt = ((MedicalExamBillingData)rpt).getChargeableAmt();
	   chargeableAmt = df.format(Double.parseDouble(chargeableAmt==null?"0":chargeableAmt)); 
	%>
	<td align="right"><%=chargeableAmt%></td>
	<% }%>
   <td align="right"><%=payableAmt%></td>      
  </tr>                                       
<%i++;%>
  </logic:iterate>
  <tr>
<% 
	String colSpan = (isAgentType ? "13" : "12");
	if (i==0) {
	  msgline = request.getParameter("msgnoresult") + " ";
	  msgline += request.getParameter("msgtryagain"); 
%>
		<td class='label2' align='center' colspan="<%=colSpan%>"><%=msgline%></td>
<%	} 
	else {
			int pageNumber = reportForm.getPageNo();
		%>
       	<td class="headerrow4" colspan="<%=colSpan%>" width="100%" height="10" class="label2" valign="bottom" >
        <!-- don't display link for previous page if the current page is the first page -->					
        <% if (pageNumber > 1) { %>
        	<a href="#" onclick="javascript:rePaginate(1,'GenerateReportStmtMedBillAction.do');"><img src="images/pagnavfirst_off.gif" name="pagnavfirst" border="0" width="18" height="13"/></a>&nbsp;					
            <a href="#" onclick="javascript:rePaginate(<%=pageNumber-1%>,'GenerateReportStmtMedBillAction.do');"><img src="images/pagnavleft_off.gif" name="pagnavleft" border="0" width="18" height="13"/></a>
        <% } else {%>
        	<img src="images/pagnavfirst_on.gif" name="pagnavfirst" border="0" width="18" height="13"/>&nbsp;
            <img src="images/pagnavleft_on.gif" name="pagnavleft" border="0" width="18" height="13"/>
        <% } %>
        <% 
        int currLink = 1;
        int prevLink = 1;
        %>
        <logic:iterate id="navLinks" name="displayPage" property="pageNumbers">
	        <% currLink = ((Integer)navLinks).intValue(); %>
	        <% if (((Integer)navLinks).intValue() == pageNumber) { %>
	        	<b><bean:write name="navLinks"/></b>
	        <% } else { %>		
	        	<% if ((currLink > (prevLink+1))) { %>
	            	...
			<% } %>
	        	<a href="#" onclick="rePaginate('<bean:write name="navLinks"/>','GenerateReportStmtMedBillAction.do');" class="links2"><bean:write name="navLinks"/></a>
			<% } %>
        	<% prevLink = currLink; %>
        </logic:iterate>
		<!-- don't display link for next page if the current page is the last page -->
        <%  if(pageNumber < prevLink) {  %>
        	<a href="#" onClick="rePaginate('<%=pageNumber+1%>','GenerateReportStmtMedBillAction.do');" class=links1><img src="<%=contextPath%>/images/pagnavright_off.gif" name="pagnavright" border="0" width="18" height="13"/></a>&nbsp;
        	<a href="#" onClick="rePaginate('<%=prevLink%>','GenerateReportStmtMedBillAction.do');" class=links1><img src="<%=contextPath%>/images/pagnavlast_off.gif" width="18" height="13" border="0"></a>
        <%  } else {  %>
        	<img src="<%=contextPath%>/images/pagnavright_on.gif" width="18" height="13" border="0">&nbsp;
            <img src="<%=contextPath%>/images/pagnavlast_on.gif" width="18" height="13" border="0">
        <%  }  %>
		</td>
<%  }  %>		
	</tr>
   </table>
  </td>
 </tr>
<%  } %> 	
</table>
</form>
<script language="Javascript" type="text/javascript">
 <!--
 manageFilters();

 function showPrinterFriendly() {
	frm = document.formStmtMed;
	typeSelected = frm.reportType;
 	
 	parmReports = "?";
 	parmReports += "startDate=" + '<bean:write name="reportForm" property="startDate" />';
 	parmReports += "&endDate=" + '<bean:write name="reportForm" property="endDate" />';
 	parmReports += "&reportType=" + '<bean:write name="reportForm" property="reportType" />';
 	parmReports += "&agent=" + '<bean:write name="reportForm" property="agent" />';
 	parmReports += "&examiner=" + '<bean:write name="reportForm" property="examiner" />';
 	parmReports += "&laboratory=" + '<bean:write name="reportForm" property="laboratory" />';
 	
	windowParms = "width=750, height=650";
	printerFriendly = window.open('GenerateReportStmtMedBillPrintAction.do' + parmReports, "printFriendly", windowParms);
	printerFriendly.focus();
 }
 
 function resetPage() {
 	document.formStmtMed.reset();
 	manageFilters();
 }
 
 function manageFilters() {
  thisForm = document.formStmtMed;
  typeSelected = thisForm.reportType;

  thisForm.examiner.disabled = true;
  thisForm.agent.disabled = true;  
  thisForm.laboratory.disabled = true;

  for (i=0; i<typeSelected.length; i++) {
   if (typeSelected[i].checked) {
	switch (i) {
	 case 0: // agent type
	  thisForm.agent.disabled = false;
	  thisForm.laboratory.selectedIndex = 0;
	  thisForm.examiner.selectedIndex = 0;
	  break;
	 case 1: // examiner type
	  thisForm.examiner.disabled = false;
	  thisForm.agent.selectedIndex = 0;
	  thisForm.laboratory.selectedIndex = 0;
	  break;
	 case 2: // laboratory type
	  thisForm.agent.disabled = false;
	  thisForm.laboratory.disabled = false;
	  thisForm.examiner.selectedIndex = 0;
	  break;		  		  
	 }
   }
  }
  
  return true;		   
 }

 function rePaginate (page, actionUrl)
 {
	var frm = document.formStmtMed;
	if(frm.pageNo != null)
	{
		frm.pageNo.value = page;	
	}
	submitForm(actionUrl);
 }
 
 function submitForm(actionUrl)
 {	
	document.formStmtMed.action = actionUrl;
    document.formStmtMed.submit();
 }
 
 function generateReport() {
  frm = document.formStmtMed;
  if (!validateForm(frm)) return false; // validate the parameter values
  frm.pageNo.value = "1"; // reset the pagination
  gotoPage('formStmtMed','GenerateReportStmtMedBillAction.do'); // submit the form
 }
 
 function validateForm(frm) {

  // check for required dates and date formats
  if (isEmpty(frm.startDate.value)) {
  	alert ("Start Date is a required field.");
  	frm.startDate.focus();
  	return false;
  }
  
  if (!isValidDate(frm.startDate.value)) {
   alert('<bean:message key="error.field.format" arg0="Start Date" arg1="date" arg2="(ddMMMyyyy)"/>');
   frm.startDate.focus();
   return false;
  }
  
  if (isEmpty(frm.endDate.value)) {
  	alert ("End Date is a required field.");
  	frm.endDate.focus();
  	return false;
  }

  if (!isValidDate(frm.endDate.value)) {
   alert('<bean:message key="error.field.format" arg0="End Date" arg1="date" arg2="(ddMMMyyyy)"/>');
   frm.endDate.focus();
   return false;
  }
  
  // make sure the start date is earlier than the end date
  if (isGreaterDate(frm.startDate.value, frm.endDate.value)) {
   alert('<bean:message key="error.field.beforeenddate" arg0="Start Date" arg1="End Date" />');
   frm.startDate.focus();
   return false;
  }
  
  return true;
 
 }
 -->
 </script>