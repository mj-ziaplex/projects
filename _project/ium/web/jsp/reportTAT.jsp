<%@ page language="java" 
         import="java.util.*, com.slocpi.ium.ui.form.*, com.slocpi.ium.util.*, com.slocpi.ium.ui.util.*, com.slocpi.ium.data.TurnAroundTimeParam" 
         buffer="1024kb"%>
<%@ taglib uri="/struts-bean.tld"  prefix="bean" %>
<%@ taglib uri="/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/struts-html.tld"  prefix="html" %>
<%@ taglib uri="/ium.tld"          prefix="ium" %>
<ium:validateSession />
<%String contextPath = request.getContextPath();%>
<%
    // This routine queries the access rights of the users (RCRIVERA)
    //
    // --- START --- 
    
    StateHandler sessionHandler = new StateHandler();
    Access access = new Access();
	HashMap userAccess = sessionHandler.getUserAccess(request);
    if(!access.hasAccess(userAccess,IUMConstants.PAGE_REPORTS,IUMConstants.ACC_INQUIRE) ){                                                                                                                    
    	response.sendRedirect("noAccessPage.jsp");   
    } 
    
    // --- END ----
%>
<%
// page processing
 
// get the form data
 ReportTurnaroundTimeForm reportForm = null;
 Page displayPage = null;
 
 reportForm = (ReportTurnaroundTimeForm) request.getAttribute("reportFilter");
 if (reportForm != null) { // report content populated
 	displayPage = reportForm.getPage();
	request.setAttribute("displayPage", displayPage);
 }
 else {
 // intialize the form object
    reportForm = new ReportTurnaroundTimeForm();
    reportForm.setReportType(String.valueOf(TurnAroundTimeParam.TAT_IN_DAYS));
 }
 request.setAttribute("reportForm", reportForm);
 
%>
<link href="<%=contextPath%>/css/pw_main.css" rel="stylesheet" type="text/css">
<link href="<%=contextPath%>/css/styleCommon.css" rel="stylesheet" type="text/css">
<link href="<%=contextPath%>/css/main.css" rel="stylesheet" type="text/css">
<script language="Javascript" src="<%=contextPath%>/js/navigation.js"></script>
<script language="Javascript" src="<%=contextPath%>/js/validation.js"></script>
<form name="formTAT" method="post">
<input type="hidden" name="reportName" value="0"/>
<table cellpadding="1" cellspacing="0" border="1" width="100%" height="100%" bordercolor="#2A4C7C">
 <!-- report paramters -->
 <tr>
 	<td>
     <table cellpadding="1" cellspacing="0" border="0" width="100%" height="100%">
	  <tr><td colspan="2" class="label2"><b>Enter Report Parameters</b></td></tr>
      <tr>
       <td>
       <table class="listtable1" cellpadding="1" cellspacing="0" border="1" width="100%" height="100%" bordercolor="#2A4C7C">
        <tr>
         <td>
   	       <table cellpadding="1" cellspacing="0" border="0" width="80%">
	        <tr>
	         <td width="50%">
	          <table cellpadding="1" cellspacing="0" border="0" width="100%">
	           <tr><td class="label2"><b>Assessment Request Status Date</b></td></tr>
	           <tr>
	            <td>
	             <table class="listtable1" cellpadding="1" cellspacing="0" border="1" width="100%" bordercolor="#2A4C7C">
			      <tr>
			       <td>
  	  	            <!-- date period -->
	  	            <table cellpadding="2" cellspacing="0" border="0" width="100%">
	  	             <tr>
	  		          <td class="label2" width="30%"><b>Start Date</b></td>
  	  		          <td width="41%">
						<html:text styleClass="label2" name="reportForm" property="startDate" onkeyup="getKeyDate(event, this);" />  	  		          	
  	  		          </td>
	  		          <td width="29%">
						<a href="#" onClick="popUpWin('<%=contextPath%>/jsp/calendar.jsp?textbox=formTAT.startDate',290,155);">
				            <img src="<%=contextPath%>/images/calendar.gif" height="17" width="19" border="0">
				        </a>		  		          
	  		          </td>
	  		         </tr>
	  		         <tr>
	  		          <td class="label2"><b>End Date</b></td>
  	  		          <td>
  	  		          	<html:text styleClass="label2" name="reportForm" property="endDate" onkeyup="getKeyDate(event, this);" />
  	  		          </td>
	  		          <td>
						<a href="#" onClick="popUpWin('<%=contextPath%>/jsp/calendar.jsp?textbox=formTAT.endDate',290,155);">
				            <img src="<%=contextPath%>/images/calendar.gif" height="17" width="19" border="0">
				        </a>	  		          
	  		          </td>
	  		         </tr>
	  	            </table>
			       </td>
	              </tr>
	             </table>
	            </td>
	           </tr>
               <tr><td>&nbsp;</td></tr>
               <tr>
                <td>
                 <!-- Unit -->
                 <table cellpadding="2" cellspacing="0" border="0" width="100%" height="100%">
                  <tr>
            	   <td class="label2" width="30%"><b>Unit</b></td>
            	   <td width="5%">
            	   	<input type="radio" name="reportType" value="<%=TurnAroundTimeParam.TAT_IN_DAYS%>" 
					 	<logic:equal name="reportForm" property="reportType" value="<%=TurnAroundTimeParam.TAT_IN_DAYS%>" >CHECKED</logic:equal>
            	   	/> 
            	   </td>            	   	
            	   </td>
            	   <td class="label2" width="20%"><b>Days</b></td>
            	   <td width="5%">
            	   	<input type="radio" name="reportType" value="<%=TurnAroundTimeParam.TAT_IN_HOURS%>" 
					 	<logic:equal name="reportForm" property="reportType" value="<%=TurnAroundTimeParam.TAT_IN_HOURS%>" >CHECKED</logic:equal>
            	   	/> 
            	   </td>
            	   <td class="label2" width="40%"><b>Hours</b></td>
                  </tr>
                 </table>          
                </td>
               </tr>	     
	          </table>
   	         </td>  
   	         <td width="50%">
   	          <!-- start -->
	          <table cellpadding="1" cellspacing="0" border="0" width="100%">
	           <tr><td class="label2"><b>Assessment Request Status</b></td></tr>
	           <tr>
	            <td>
	             <table class="listtable1" cellpadding="1" cellspacing="0" border="1" width="100%" height="100%" bordercolor="#2A4C7C">
			      <tr>
			       <td>
  	  	            <!-- date period -->
	  	            <table cellpadding="2" cellspacing="0" border="0" width="100%">
	  	             <tr>
  				      <td class="label2" width="30%"><b>Status From</b></td>
  				      <td>
  	  		           <ium:list className="label2" 
  	  		                     listBoxName="startStatus" 
  	  		                     type="<%=IUMConstants.LIST_REQUEST_STATUS%>" 
  	  		                     styleName="width:160;height:20"
  	  		                     selectedItem="<%=reportForm.getStartStatus()%>" />
  	  		          </td>
	  		         </tr>
	  		         <tr>
	  		          <td class="label2"><b>Status To</b></td>
  	  		          <td>
  	  		           <ium:list className="label2" 
  	  		                     listBoxName="endStatus" 
  	  		                     type="<%=IUMConstants.LIST_REQUEST_STATUS%>" 
  	  		                     styleName="width:160;height:20"
  	  		                     selectedItem="<%=reportForm.getEndStatus()%>" />
  	  		          </td>
	  		         </tr>
	  	            </table>
			       </td>
	              </tr>
	             </table>
	            </td>
	           </tr>
               <tr><td>&nbsp;</td></tr>
               <tr>
                <td>
                 <!-- LOB -->
                 <table cellpadding="2" cellspacing="0" border="0" width="100%">
                  <tr>
            	   <td class="label2" width="30%"><b>Business Line</b></td>
            	   <td width="70%">
             	    <ium:list className="label2" 
             	    		  listBoxName="lineOfBusiness" 
             	    		  type="<%=IUMConstants.LIST_LOB%>" 
             	    		  styleName="width:160"
             	    		  selectedItem="<%=reportForm.getLineOfBusiness()%>" />
            	   </td>
                  </tr>
                 </table>
                </td>
               </tr>	     
	          </table>
	          <!-- end -->   	          
   	         </td>
	        </tr>
	        <tr>
	         <td colspan="2">
	          <table cellpadding="1" cellspacing="0" border="0" width="90%">
	           <tr>
	            <td>
		         	<input type="button" class="button1" value="Generate" style="width:100" onClick="generateReport();" />
		         	&nbsp;
	    	     	<input type="button" class="button1" value="Reset" style="width:100" onClick="gotoPage('reportForm', 'viewReports.do')" />
	    	     </td>
	    	    </tr>
	    	   </table>
	         </td>
	        </tr>
	       </table> <!-- end of parameter grouping -->
   	      </td>
	     </tr>
	    </table> <!-- end of Report Parameters border -->
	   </td>
       <td width="12%">&nbsp;</td> <!-- minimizing horizontal scrolling -->
	  </tr>
     </table> <!-- end of Report Parameters container -->      
 	</td>
 </tr>
 <input type="hidden" name="pageNo" value="<%=reportForm.getPageNo()%>">
 <!-- report content --> 
<%
   int i=0;
   String tr_class;
   String td_bgcolor;
   String msgline = "&nbsp;";

   if (displayPage != null) {
      	
    String timeTitle = "(Hours)";
    if (reportForm.getReportType().equals(String.valueOf(TurnAroundTimeParam.TAT_IN_DAYS))) {
    	timeTitle = "(Days)";
    }

%>
 <tr>
  <td>
   <table border="0" cellpadding="2" cellspacing="1" bgcolor="#FFFFFF" style="border: 1px solid #D1D1D1;width:88%;" class="listtable1">
<%
	if (displayPage.getList().size() > 0) {
%>
    <tr>
    	<td colspan="7" class="label2" align="right">
    		<a href="#" onClick="javascript:showPrinterFriendly();">Printer Friendly</a>
    	</td>
    </tr>
<%
	}
%>
    <tr class="headerrow1">
     <td rowspan="2">Business Line</td>
     <td rowspan="2">Request Reference No.</td>
     <td colspan="2">Request Status Period</td>
     <td rowspan="2">Elapsed Time <%=timeTitle%></td>
     <td rowspan="2">Requests Processed</td>
     <td rowspan="2">Average TAT <%=timeTitle%></td>
    </tr>
    <tr class="headerrow1">
     <td>Start</td>
     <td>End</td>
   </tr>
    <logic:iterate id="rpt" name="displayPage" property="list">
<%
   if (i%2 == 0) {
   	tr_class = "row1";
    td_bgcolor = "#CECECE";
   }
   else {
    tr_class = "row2";
    td_bgcolor = "#EDEFF0";
   }
%>
  <tr class=<%=tr_class%>>
   <td width="20%"><b><bean:write name='rpt' property='lineOfBusiness' /></b></td>
   <td width="20%"><bean:write name='rpt' property='referenceNumber' /></td>
   <td width="15%"><bean:write name='rpt' property='dateStart' /></td>
   <td width="15%"><bean:write name='rpt' property='dateEnd' /></td>
   <td align="right" width="10%"><bean:write name="rpt" property="elapsedTime" /></td>      
   <td align="right" width="10%"><bean:write name="rpt" property="processedCount" /></td>      
   <td align="right" width="10%"><bean:write name="rpt" property="averageTAT" /></td>      
  </tr>                                       
<%i++;%>
  </logic:iterate>
  <tr>
<% 
	if (i==0) {
	  msgline = request.getParameter("msgnoresult") + " ";
	  msgline += request.getParameter("msgtryagain");
%>
		<td class='label2' align='center' colspan="7"><%=msgline%></td>
<%	} 
	else {
			int pageNumber = reportForm.getPageNo();
		%>
       	<td class="headerrow4" colspan="11" width="100%" height="10" class="label2" valign="bottom" >
        <!-- don't display link for previous page if the current page is the first page -->					
        <% if (pageNumber > 1) { %>
        	<a href="#" onclick="javascript:rePaginate(1,'GenerateReportTurnAroundTimeAction.do');"><img src="images/pagnavfirst_off.gif" name="pagnavfirst" border="0" width="18" height="13"/></a>&nbsp;					
            <a href="#" onclick="javascript:rePaginate(<%=pageNumber-1%>,'GenerateReportTurnAroundTimeAction.do');"><img src="images/pagnavleft_off.gif" name="pagnavleft" border="0" width="18" height="13"/></a>
        <% } else {%>
        	<img src="images/pagnavfirst_on.gif" name="pagnavfirst" border="0" width="18" height="13"/>&nbsp;
            <img src="images/pagnavleft_on.gif" name="pagnavleft" border="0" width="18" height="13"/>
        <% } %>
        <% 
        int currLink = 1;
        int prevLink = 1;
        %>
        <logic:iterate id="navLinks" name="displayPage" property="pageNumbers">
	        <% currLink = ((Integer)navLinks).intValue(); %>
	        <% if (((Integer)navLinks).intValue() == pageNumber) { %>
	        	<b><bean:write name="navLinks"/></b>
	        <% } else { %>		
	        	<% if ((currLink > (prevLink+1))) { %>
	            	...
			<% } %>
	        	<a href="#" onclick="rePaginate('<bean:write name="navLinks"/>','GenerateReportTurnAroundTimeAction.do');" class="links2"><bean:write name="navLinks"/></a>
			<% } %>
        	<% prevLink = currLink; %>
        </logic:iterate>
		<!-- don't display link for next page if the current page is the last page -->
        <%  if(pageNumber < prevLink) {  %>
        	<a href="#" onClick="rePaginate('<%=pageNumber+1%>','GenerateReportTurnAroundTimeAction.do');" class=links1><img src="<%=contextPath%>/images/pagnavright_off.gif" name="pagnavright" border="0" width="18" height="13"/></a>&nbsp;
        	<a href="#" onClick="rePaginate('<%=prevLink%>','GenerateReportTurnAroundTimeAction.do');" class=links1><img src="<%=contextPath%>/images/pagnavlast_off.gif" width="18" height="13" border="0"></a>
        <%  } else {  %>
        	<img src="<%=contextPath%>/images/pagnavright_on.gif" width="18" height="13" border="0">&nbsp;
            <img src="<%=contextPath%>/images/pagnavlast_on.gif" width="18" height="13" border="0">
        <%  }  %>
		</td>
<%  }  %>		
	</tr>
   </table>
  </td>
 </tr>
<%  } %> 	
</table>
</form> 
<script language="Javascript" type="text/javascript">
 <!--
 function showPrinterFriendly() {
	frm = document.formTAT;
	typeSelected = frm.reportType;
 	
 	parmReports = "?";
 	parmReports += "startDate=" + '<bean:write name="reportForm" property="startDate" />';
 	parmReports += "&endDate=" + '<bean:write name="reportForm" property="endDate" />';
 	parmReports += "&reportType=" + '<bean:write name="reportForm" property="reportType" />';
 	parmReports += "&startStatus=" + '<bean:write name="reportForm" property="startStatus" />';
 	parmReports += "&endStatus=" + '<bean:write name="reportForm" property="endStatus" />';
 	parmReports += "&lob=" + '<bean:write name="reportForm" property="lineOfBusiness" />';
 	
	windowParms = "width=750, height=650";
	printerFriendly = window.open('GenerateReportTurnaroundTimePrintAction.do' + parmReports, "printFriendly", windowParms);
	printerFriendly.focus();
 }
  
 function rePaginate (page, actionUrl)
 {
	var frm = document.formTAT;
	if(frm.pageNo != null)
	{
		frm.pageNo.value = page;	
	}
	submitForm(actionUrl);
 }

 function submitForm(actionUrl)
 {	
	document.formTAT.action = actionUrl;
    document.formTAT.submit();
 }
   
 function resetPage()
 {
 	document.formTAT.reset();
 } 
 
 function generateReport() {
  if (!validateForm(document.formTAT)) return false; // validate the parameter values
  formTAT.pageNo.value = 1;
  gotoPage('formTAT','GenerateReportTurnAroundTimeAction.do'); // submit the form
 }
 
 function validateForm(frm) {

  // check for required dates and date formats
  if (isEmpty(frm.startDate.value)) {
  	alert ("Start Date is a required field.");
  	frm.startDate.focus();
  	return false;
  }
  
  if (!isValidDate(frm.startDate.value)) {
   alert('<bean:message key="error.field.format" arg0="Start Date" arg1="date" arg2="(ddMMMyyyy)"/>');
   frm.startDate.focus();
   return false;
  }
  
  if (isEmpty(frm.endDate.value)) {
  	alert ("End Date is a required field.");
  	frm.endDate.focus();
  	return false;
  }

  if (!isValidDate(frm.endDate.value)) {
   alert('<bean:message key="error.field.format" arg0="End Date" arg1="date" arg2="(ddMMMyyyy)"/>');
   frm.endDate.focus();
   return false;
  }
  
  // make sure the start date is earlier than the end date
  if (isGreaterDate(frm.startDate.value, frm.endDate.value)) {
   alert('<bean:message key="error.field.beforeenddate" arg0="Start Date" arg1="End Date" />');
   frm.startDate.focus();
   return false;
  }
  
  // check the status fields
  if (noSelection(frm.startStatus.value)) {
   alert('<bean:message key="error.field.requiredselection" arg0="Status From" />');
   frm.startStatus.focus();
   return false;
  }
  
  if (noSelection(frm.endStatus.value)) {
   alert('<bean:message key="error.field.requiredselection" arg0="Status To" />');
   frm.endStatus.focus();
   return false;
  }
  
  return true;
 
 }
 -->
</script>