<%@ taglib uri="/struts-html.tld" prefix="html" %>
<%@ taglib uri="/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/struts-logic.tld" prefix="logic" %>
<%@ page language="java" import="java.util.*"%>
<jsp:useBean id="workFunctionForm" scope="request" class="com.slocpi.ium.ui.form.WorkFunctionForm"/>
<%String contextPath = request.getContextPath(); %>
<%
/* Andre Ceasar Dacanay - css enhancement - start */
String iumCss = "";
String iumColorB = "";
String iumColorO = "";
String companyCode = "";
try {
	ResourceBundle rbWMSSP = ResourceBundle.getBundle("dbconfig");
	iumCss = rbWMSSP.getString("IUM_CSS_VERSION");
	companyCode = iumCss;
	iumCss = "_"+iumCss;
} catch (Exception e){}
if(iumCss.equals("_CP")){
	iumColorB="#2A4C7C";
	iumColorO = "#FFCB00";
	companyCode="";
}else if(iumCss.equals("_GF")){
	iumColorB="#303A77";
	iumColorO = "#303A77";
	companyCode = companyCode+" ";
}else{
	iumColorB="#2A4C7C";
	iumColorO = "#FFCB00";
}
/* Andre Ceasar Dacanay - css enhancement - end */
%>
<html>
	<body>
		<head>
			<title><%=companyCode%>Integrated Underwriting and Medical System</title>
			<link href="<%=contextPath%>/css/pw_main.css"     rel="stylesheet" type="text/css">
			<link href="<%=contextPath%>/css/main<%=iumCss%>.css" rel="stylesheet" type="text/css">
			<link href="<%=contextPath%>/css/styleCommon.css" rel="stylesheet" type="text/css">
			<script language="JavaScript" src="<%=contextPath%>/js/dhtml_init.js"></script>
			<script language="JavaScript" src="<%=contextPath%>/js/navigation.js"></script>
			<script language="JavaScript" src="<%=contextPath%>/jsp/sl_left_nav.jsp"></script>
		</head>

	<BODY leftMargin=0 topmargin=0 marginwidth=0 marginheight=0>
		<table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%">
			<tr>
				<td width="100%" colspan="2">
					<jsp:include page="header.jsp" flush="true"/>
				</td>
			</tr>
			<tr>
				  <td rowspan="2" bgcolor="<%=iumColorO%>" height="100%" width="20"><img src="<%=contextPath%>/images/spacer.gif" width="20"></td>
	              <td background="<%=contextPath%>/images/GradientGray.jpg" height="27" width="100%" valign="top">&nbsp;&nbsp;
               	  	  <span class="main">Integrated Underwriting and Medical System</span>
        		  </td>
	        </tr>
            <tr>
            	<td width="100%" height="100%" valign="top">
              		<!-- BODY -->
			        <table width="100%" cellpadding="3" cellspacing="5" border="0">
		            <tr> 
		            	<%if ((request.getParameter("actionType")).equalsIgnoreCase("create")){%>
				            <td class="label2"><b>Create Work Function</b>              
				        <%}else{%>
				        	<td class="label2"><b>Update Work Function</b>              
				        <%}%>
            			</td>
		            </tr>
		            <tr valign="top"> 
		            	<td> 
<!--- START OF BODY -->
							<form name="frm" method="post">
								<table border="1" bordercolor="<%=iumColorB%>" cellpadding=10 cellspacing=0 bgcolor="" width="500" class="listtable1">
							        <tr><td colspan="2" height="100%">
									    <table border="0" width="100%" >
									    	<%if ((request.getParameter("actionType")).equalsIgnoreCase("create")){%>
										        <tr>
											        <td class="label2"><b>Action Code</b><td/>
											        <td><input type="text" name = "templateCode" class="label2" value=""> </td>
										        </tr>
										        <tr>
											        <td class="label2" valign="top"><b>Description</b><td/>
											        <td><textarea cols="20" name = "templateDesc" rows="4" class="label2"></textarea></td>
										        </tr>        
									        <%}else{%>									        	
									        	<logic:iterate id="work" name="workFunctionForm" property="workFunctionData">        
        											<tr>
        												<td class="label2"><b>Action Code</b><td/>
        												<input type="hidden" name="templateCode" value = '<bean:write name="work" property="templateCode"/>'>
												        <td><input type="text" name = "templateCode" class="label2" value='<bean:write name="work" property="templateCode"/>' disabled> </td>
													</tr>
													<tr>
											        	<td class="label2" valign="top"><b>Description</b><td/>
											        	<td><textarea cols="20" name = "templateDesc" rows="4" class="label2"><bean:write name="work" property="templateDesc"/></textarea></td>
										        	</tr>
											    </logic:iterate>
									        <%}%>
									        <tr><td colspan="4" align="right">&nbsp;</td></tr>
									        <tr>
									        	<td colspan="6" align="right">
									        		<%if ((request.getParameter("actionType")).equalsIgnoreCase("create")){%>									        												        		
											        	<input type="button" value="Save" class="button1" onclick="gotoPage('frm', '<%=contextPath%>/createWorkFunction.do');">&nbsp;
											        <%}else{%>
											        	<input type="button" value="Save" class="button1" onclick="gotoPage('frm', '<%=contextPath%>/updateWorkFunction.do');">&nbsp;
											        <%}%>
											        <input type="button" value="Cancel" class="button1" onclick="gotoPage('frm', '<%=contextPath%>/listWorkFunction.do');">
										        </td>
										    </tr>
								        </table>
									</td>
								</tr>
							</table>
						</form>
<!--- END OF BODY -->
					</td>
				</tr>

			</table>
		</div>
	</body>
</html>