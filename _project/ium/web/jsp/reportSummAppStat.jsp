<%@ page language="java" 
         import="java.util.*, com.slocpi.ium.ui.form.*, com.slocpi.ium.util.*, com.slocpi.ium.ui.util.*, com.slocpi.ium.data.ApplicationsByStatusFilterData" 
         buffer="1024kb"%>
<%@ taglib uri="/struts-bean.tld"  prefix="bean" %>
<%@ taglib uri="/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/struts-html.tld"  prefix="html" %>
<%@ taglib uri="/ium.tld"          prefix="ium" %>
<ium:validateSession />
<%String contextPath = request.getContextPath();%>
<%
    // This routine queries the access rights of the users (RCRIVERA)
    //
    // --- START --- 
    
    StateHandler sessionHandler = new StateHandler();
    Access access = new Access();
	HashMap userAccess = sessionHandler.getUserAccess(request);
    if(!access.hasAccess(userAccess,IUMConstants.PAGE_REPORTS,IUMConstants.ACC_INQUIRE) ){                                                                                                                    
    	response.sendRedirect("noAccessPage.jsp");   
    } 

    // --- END ----
%>
<%
// page processing

 ReportAppsByStatusForm reportForm = null;
 Page displayPage = null;
 
 reportForm = (ReportAppsByStatusForm) request.getAttribute("reportFilter"); // extract the form attribute from the request object
 if (reportForm != null) { // data extracted
 	displayPage = reportForm.getPage();
 	request.setAttribute("displayPage", displayPage);	// contains the result set
 }
 else { // page loaded
 	reportForm = new ReportAppsByStatusForm();
 	reportForm.setReportType(String.valueOf(ApplicationsByStatusFilterData.FILTER_BY_BRANCH)); 
 }
 request.setAttribute("reportForm", reportForm);

%>
<link href="<%=contextPath%>/css/pw_main.css" rel="stylesheet" type="text/css">
<link href="<%=contextPath%>/css/styleCommon.css" rel="stylesheet" type="text/css">
<link href="<%=contextPath%>/css/main.css" rel="stylesheet" type="text/css">
<script language="Javascript" src="<%=contextPath%>/js/navigation.js"></script>
<script language="Javascript" src="<%=contextPath%>/js/validation.js"></script>
<form name="formSummAppStat" method="post">
<input type="hidden" name="reportName" value="<%=request.getParameter("reportName")%>"/>
<table cellpadding="1" cellspacing="0" border="1" width="100%" height="100%" bordercolor="#2A4C7C">
 <!-- report paramters -->
 <tr>
 	<td>
     <table cellpadding="1" cellspacing="0" border="0" width="100%" height="100%">
	  <tr><td colspan="2" class="label2"><b>Enter Report Parameters</b></td></tr>
      <tr>
       <td>
       <table class="listtable1" cellpadding="1" cellspacing="0" border="1" width="100%" height="100%" bordercolor="#2A4C7C">
        <tr>
         <td>
   	       <table cellpadding="1" cellspacing="0" border="0" width="100%" height="100%">
	        <tr>
	         <td>
	          <table cellpadding="1" cellspacing="0" border="0" width="100%" height="100%">
	           <tr><td class="label2"><b>Assessment Request Status Date</b></td></tr>
	           <tr>
	            <td>
	             <table class="listtable1" cellpadding="1" cellspacing="0" border="1" width="41.5%" bordercolor="#2A4C7C">
			      <tr>
			       <td>
  	  	            <!-- date period -->
	  	            <table cellpadding="2" cellspacing="0" border="0" width="100%">
	  	             <tr>
	  		          <td class="label2" width="36%"><b>Start Date</b></td>
  	  		          <td width="35%"><html:text styleClass="label2" name="reportForm" property="startDate" onkeyup="getKeyDate(event, this);" /></td>
	  		          <td width="29%">
						<a href="#" onClick="popUpWin('<%=contextPath%>/jsp/calendar.jsp?textbox=formSummAppStat.startDate',290,155);">
				            <img src="<%=contextPath%>/images/calendar.gif" height="17" width="19" border="0">
				        </a>		  		          
	  		          </td>
	  		         </tr>
	  		         <tr>
	  		          <td class="label2"><b>End Date</b></td>
  	  		          <td><html:text styleClass="label2" name="reportForm" property="endDate" onkeyup="getKeyDate(event, this);" /></td>
	  		          <td>
						<a href="#" onClick="popUpWin('<%=contextPath%>/jsp/calendar.jsp?textbox=formSummAppStat.endDate',290,155);">
				            <img src="<%=contextPath%>/images/calendar.gif" height="17" width="19" border="0">
				        </a>	  		          
	  		          </td>
	  		         </tr>
	  	            </table>
			       </td>
	              </tr>
	             </table>
	            </td>
	           </tr>
               <tr><td>&nbsp;</td></tr>
               <tr>
                <td>
                 <!-- Report Types -->
                 <table cellpadding="2" cellspacing="0" border="0" width="65%">
                  <tr>
               	   <td class="label2" width="21%"><b>Report Type</b></td>
            	   <td class="label2" width="5%">
            	   	<input type="radio" name="reportType" value="<%=ApplicationsByStatusFilterData.FILTER_BY_BRANCH%>" onClick="manageFilters();"
						<logic:equal name="reportForm" property="reportType" value="<%=ApplicationsByStatusFilterData.FILTER_BY_BRANCH%>" >CHECKED </logic:equal>  />
            	   </td>
            	   <td class="label2" ><b>Branch</b></td>
            	   <td class="label2" width="5%">
            	   	<input type="radio" name="reportType" value="<%=ApplicationsByStatusFilterData.FILTER_BY_LOB%>" onClick="manageFilters();"
            	   		<logic:equal name="reportForm" property="reportType" value="<%=ApplicationsByStatusFilterData.FILTER_BY_LOB%>" >CHECKED </logic:equal> />
            	   </td>
            	   <td class="label2" ><b>LOB</b></td>
            	   <td class="label2" width="5%">
            	   	<input type="radio" name="reportType" value="<%=ApplicationsByStatusFilterData.FILTER_BY_UNDERWRITER%>" onClick="manageFilters();"
						<logic:equal name="reportForm" property="reportType" value="<%=ApplicationsByStatusFilterData.FILTER_BY_UNDERWRITER%>" >CHECKED </logic:equal> />
            	   </td>
            	   <td class="label2" ><b>Underwriter</b></td>
                  </tr>
                 </table>          
                </td>
               </tr>
               <tr><td>&nbsp;</td></tr>
	           <tr><td class="label2"><b>Filters</b></td></tr>
               <tr>
                <td>
                 <table class="listtable1" cellpadding="1" cellspacing="0" border="1" width="90%" bordercolor="#2A4C7C">
                  <tr>
                   <td>
    		        <!-- Filters -->
	  		        <table cellpadding="2" cellspacing="0" border="0" width="100%" height="100%">
	  		         <tr>
	  		          <td class="label2" width="12%"><b>Branch</b></td>
  	  		          <td width="30%">
  	  		           <ium:list className="label2" listBoxName="branch" styleName="width:200"
  	  		           	type="<%=IUMConstants.LIST_BRANCHES%>" selectedItem="<%=reportForm.getBranch()%>" />
  	  		          </td>
	  		          <td class="label2" width="5%"><b>LOB</b></td>
  	  		          <td width="53%">
  	  		           <ium:list className="label2" listBoxName="lob" styleName="width:160" 
  	  		           	type="<%=IUMConstants.LIST_LOB%>" selectedItem="<%=reportForm.getLob()%>" />
  	  		          </td>  	  		          
	  		         </tr>
	  		         <tr>
	  		          <td class="label2"><b>Underwriter</b></td>
  	  		          <td>
  	  		           <ium:list className="label2" listBoxName="underwriter" styleName="width:160"
  	  		           type="<%=IUMConstants.LIST_USERS%>" selectedItem="<%=reportForm.getUnderwriter()%>" />
  	  		          </td>  	  		          
  	  		          <td colspan="2">&nbsp;</td>
	  		         </tr>
	  		        </table>	  				 
	  		       </td>
	  		      </tr>
	  		     </table>
                </td>
               </tr>	     
	          </table>
   	         </td>  
	        </tr>
	        <tr>
	         <td>
	          <table cellpadding="1" cellspacing="0" border="0" width="90%">
	           <tr>
	            <td>
		         	<input type="button" class="button1" value="Generate" style="width:100" onClick="generateReport();" />
		         	&nbsp;
	    	     	<input type="button" class="button1" value="Reset" style="width:100" onClick="gotoPage('reportForm', 'viewReports.do')"/>
	    	     </td>
	    	    </tr>
	    	   </table>
	         </td>
	        </tr>
	       </table> <!-- end of parameter grouping -->
   	      </td>
	     </tr>
	    </table> <!-- end of Report Parameters border -->
	   </td>
       <td width="12%">&nbsp;</td> <!-- minimizing horizontal scrolling -->
	  </tr>
     </table> <!-- end of Report Parameters container -->      
 	</td>
 </tr>
 <!-- report content --> 
 <input type="hidden" name="pageNo" value="<%=reportForm.getPageNo()%>">	
<%
   int i=0;
   String tr_class;
   String td_bgcolor;
   String msgline = "&nbsp;";

   if (displayPage != null) {
   	String colHeader1 = "";
   	String colValue1 = "";
   	
   	String HEADER_BRANCH = "Sun Life Office/Branch";
   	String HEADER_LOB = "LOB";
   	String HEADER_UDWTR = "Underwriter";

   	switch (Integer.valueOf(reportForm.getReportType()).intValue())
   	{
   		case ApplicationsByStatusFilterData.FILTER_BY_BRANCH:
	   		colHeader1 = HEADER_BRANCH;
   			break;
		case ApplicationsByStatusFilterData.FILTER_BY_LOB:
   			colHeader1 = HEADER_LOB;   	
			break;
		case ApplicationsByStatusFilterData.FILTER_BY_UNDERWRITER:
			colHeader1 = HEADER_UDWTR;
			break;
   	}

%>
 <tr>
  <td>
   <table border="0" cellpadding="2" cellspacing="1" bgcolor=#FFFFFF style="border: 1px solid #D1D1D1;width:88%" class="listtable1">
<%
	if (displayPage.getList().size() > 0) {
%>
    <tr>
    	<td colspan="5" class="label2" align="right">
    		<a href="#" onClick="javascript:showPrinterFriendly();">Printer Friendly</a>
    	</td>
    </tr>
<%
	}
%>
    <tr class="headerrow1">
     <td><%=colHeader1%></td>
     <td>Status</td>
     <td>Reference No.</td>
     <td>Date</td>         
     <td>Ordered</td>
    </tr>
    <logic:iterate id="rpt" name="displayPage" property="list">
<%
   if (i%2 == 0) {
   	tr_class = "row1";
    td_bgcolor = "#CECECE";
   }
   else {
    tr_class = "row2";
    td_bgcolor = "#EDEFF0";
   }
%>
  <tr class=<%=tr_class%>>
   <td width="40%"><b><bean:write name="rpt" property="groupName" /></b></td>
   <td width="32%"><bean:write name="rpt" property="statusDesc" /></td>   
   <td width="14%"><bean:write name="rpt" property="referenceNumber" /></td>      
   <td width="9%"><bean:write name="rpt" property="statusDate" /></td>   
   <td align="right" width="5%">
   	<logic:notEqual name="rpt" property="countAR" value="0">
   		<bean:write name="rpt" property="countAR" />
   	</logic:notEqual>
   </td>      
  </tr>                                       
<%i++;%>
 </logic:iterate>
<% 
	if (i==0) {
	  msgline = request.getParameter("msgnoresult") + " ";
	  msgline += request.getParameter("msgtryagain");
%>
	<td class='label2' align='center' colspan="7"><%=msgline%></td>
<%
	} 
	else {
			int pageNumber = reportForm.getPageNo();
%>
       	<td class="headerrow4" colspan="11" width="100%" height="10" class="label2" valign="bottom" >
        <!-- don't display link for previous page if the current page is the first page -->					
        <% if (pageNumber > 1) { %>
        	<a href="#" onclick="javascript:rePaginate(1,'GenerateReportAppsByStatusAction.do');"><img src="images/pagnavfirst_off.gif" name="pagnavfirst" border="0" width="18" height="13"/></a>&nbsp;					
            <a href="#" onclick="javascript:rePaginate(<%=pageNumber-1%>,'GenerateReportAppsByStatusAction.do');"><img src="images/pagnavleft_off.gif" name="pagnavleft" border="0" width="18" height="13"/></a>
        <% } else {%>
        	<img src="images/pagnavfirst_on.gif" name="pagnavfirst" border="0" width="18" height="13"/>&nbsp;
            <img src="images/pagnavleft_on.gif" name="pagnavleft" border="0" width="18" height="13"/>
        <% } %>
        <% 
        int currLink = 1;
        int prevLink = 1;
        %>
        <logic:iterate id="navLinks" name="displayPage" property="pageNumbers">
	        <% currLink = ((Integer)navLinks).intValue(); %>
	        <% if (((Integer)navLinks).intValue() == pageNumber) { %>
	        	<b><bean:write name="navLinks"/></b>
	        <% } else { %>		
	        	<% if ((currLink > (prevLink+1))) { %>
	            	...
			<% } %>
	        	<a href="#" onclick="rePaginate('<bean:write name="navLinks"/>','GenerateReportAppsByStatusAction.do');" class="links2"><bean:write name="navLinks"/></a>
			<% } %>
        	<% prevLink = currLink; %>
        </logic:iterate>
		<!-- don't display link for next page if the current page is the last page -->
        <%  if(pageNumber < prevLink) {  %>
        	<a href="#" onClick="rePaginate('<%=pageNumber+1%>','GenerateReportAppsByStatusAction.do');" class=links1><img src="<%=contextPath%>/images/pagnavright_off.gif" name="pagnavright" border="0" width="18" height="13"/></a>&nbsp;
        	<a href="#" onClick="rePaginate('<%=prevLink%>','GenerateReportAppsByStatusAction.do');" class=links1><img src="<%=contextPath%>/images/pagnavlast_off.gif" width="18" height="13" border="0"></a>
        <%  } else {  %>
        	<img src="<%=contextPath%>/images/pagnavright_on.gif" width="18" height="13" border="0">&nbsp;
            <img src="<%=contextPath%>/images/pagnavlast_on.gif" width="18" height="13" border="0">
        <%  }  %>
		</td>
<%  }  %>		
	</tr>
   </table>
  </td>
 </tr>
<%  } %>
</table>
<script language="Javascript" type="text/javascript">
<!--
 manageFilters();

 function showPrinterFriendly() {
	frm = document.formSummAppStat;
	typeSelected = frm.reportType;
 	
 	parmReports = "?";
 	parmReports += "startDate=" + '<bean:write name="reportForm" property="startDate" />';
 	parmReports += "&endDate=" + '<bean:write name="reportForm" property="endDate" />';
 	parmReports += "&reportType=" + '<bean:write name="reportForm" property="reportType" />';
 	parmReports += "&branch=" + '<bean:write name="reportForm" property="branch" />';
 	parmReports += "&lob=" + '<bean:write name="reportForm" property="lob" />';
 	parmReports += "&underwriter=" + '<bean:write name="reportForm" property="underwriter" />';
 	
	windowParms = "width=750, height=650";
	printerFriendly = window.open('GenerateReportAppsByStatusPrintAction.do' + parmReports, "printFriendly", windowParms);
	printerFriendly.focus();
 }
 
 function resetPage() {
	document.formSummAppStat.reset();
	manageFilters(); 
 }

 function rePaginate (page, actionUrl)
 {
	var frm = document.formSummAppStat;
	if(frm.pageNo != null)
	{
		frm.pageNo.value = page;	
	}
	submitForm(actionUrl);
 }

 function submitForm(actionUrl)
 {	
	document.formSummAppStat.action = actionUrl;
    document.formSummAppStat.submit();
 }
 
 
 function manageFilters() {
  thisForm = document.formSummAppStat;
  typeSelected = document.formSummAppStat.reportType;

  thisForm.branch.disabled = true;  
  thisForm.underwriter.disabled = true;  
  thisForm.lob.disabled = true;  

  for (i=0; i<typeSelected.length; i++) {
   if (typeSelected[i].checked) {
	switch (i) {
	 case 0: // branch type
	  thisForm.branch.disabled = false;
	  thisForm.lob.selectedIndex = 0;
	  thisForm.underwriter.selectedIndex = 0;
	  break;
	 case 1:
	  thisForm.lob.disabled = false;
	  thisForm.branch.selectedIndex = 0;
	  thisForm.underwriter.selectedIndex = 0;
	  break;
	 case 2: // underwriter type
	  thisForm.underwriter.disabled = false;
	  thisForm.branch.selectedIndex = 0;
  	  thisForm.lob.selectedIndex = 0;
	  break;		  		  
	 }
   }
  }
  
  return true;		   
 }

 function generateReport() {
  var frm = document.formSummAppStat;
  if (!validateForm(document.formSummAppStat)) return false; // validate the parameter values
  frm.pageNo.value = "1";	// reset the page number;
  gotoPage('formSummAppStat','GenerateReportAppsByStatusAction.do'); // submit the form
 }
 
 function validateForm(frm) {

  // check for required dates and date formats
  if (isEmpty(frm.startDate.value)) {
  	alert ("Start Date is a required field.");
  	frm.startDate.focus();
  	return false;
  }
  
  if (!isValidDate(frm.startDate.value)) {
   alert('<bean:message key="error.field.format" arg0="Start Date" arg1="date" arg2="(ddMMMyyyy)"/>');
   frm.startDate.focus();
   return false;
  }
  
  if (isEmpty(frm.endDate.value)) {
  	alert ("End Date is a required field.");
  	frm.endDate.focus();
  	return false;
  }

  if (!isValidDate(frm.endDate.value)) {
   alert('<bean:message key="error.field.format" arg0="End Date" arg1="date" arg2="(ddMMMyyyy)"/>');
   frm.endDate.focus();
   return false;
  }
  
  // make sure the start date is earlier than the end date
  if (isGreaterDate(frm.startDate.value, frm.endDate.value)) {
   alert('<bean:message key="error.field.beforeenddate" arg0="Start Date" arg1="End Date" />');
   frm.startDate.focus();
   return false;
  }

  return true;
 }
-->
</script>
</form>
