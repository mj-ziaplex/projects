<%@ page language="java" 
         import="java.util.*, com.slocpi.ium.ui.form.*, com.slocpi.ium.util.*, com.slocpi.ium.ui.util.*, com.slocpi.ium.data.ARWithPendingMedFilterData" 
         buffer="1024kb"%>
<%@ taglib uri="/struts-bean.tld"  prefix="bean" %>
<%@ taglib uri="/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/struts-html.tld"  prefix="html" %>
<%@ taglib uri="/ium.tld"          prefix="ium" %>
<ium:validateSession />
<%String contextPath = request.getContextPath();%>
<%
    // This routine queries the access rights of the users (RCRIVERA)
    //
    // --- START --- 
    
    StateHandler sessionHandler = new StateHandler();
    Access access = new Access();
	HashMap userAccess = sessionHandler.getUserAccess(request);
    if(!access.hasAccess(userAccess,IUMConstants.PAGE_REPORTS,IUMConstants.ACC_INQUIRE) ){                                                                                                                    
    	response.sendRedirect("noAccessPage.jsp");   
    } 
    
    // --- END ----
%>
<%
/* 
	ARWithPendingMedFilterData 
	BRANCH_REPORT_TYPE = 0;
	LABORATORY_REPORT_TYPE = 1;
	UNDERWRITER_REPORT_TYPE = 2;
	EXAMINER_REPORT_TYPE = 3;
*/

   ReportFollowMedForm reportForm = null;
   Page displayPage = null;

   reportForm = (ReportFollowMedForm) request.getAttribute("reportFilter");
   if (reportForm != null) { // gone through the Action class
		displayPage = reportForm.getPage();
		request.setAttribute("displayPage", displayPage);
   }
   else { // came from the menu
   		reportForm = new ReportFollowMedForm();  // initialize
   		reportForm.setReportType(String.valueOf(ARWithPendingMedFilterData.BRANCH_REPORT_TYPE));
   }
   request.setAttribute("reportForm", reportForm);
      
%>
<link href="<%=contextPath%>/css/pw_main.css" rel="stylesheet" type="text/css">
<link href="<%=contextPath%>/css/styleCommon.css" rel="stylesheet" type="text/css">
<link href="<%=contextPath%>/css/main.css" rel="stylesheet" type="text/css">
<script language="Javascript" src="<%=contextPath%>/js/navigation.js"></script>
<script language="Javascript" src="<%=contextPath%>/js/validation.js"></script>
<form name="formFollowMed" method="post">
<input type="hidden" name="reportName" value="<%=request.getParameter("reportName")%>"/>
<table cellpadding="1" cellspacing="0" border="1" width="100%" height="100%" bordercolor="#2A4C7C">
 <!-- report paramters -->
 <tr>
 	<td>
     <table cellpadding="1" cellspacing="0" border="0" width="100%" height="100%">
	  <tr><td colspan="2" class="label2"><b>Enter Report Parameters</b></td></tr>
      <tr>
       <td>
       <table class="listtable1" cellpadding="1" cellspacing="0" border="1" width="100%" height="100%" bordercolor="#2A4C7C">
        <tr>
         <td>
   	       <table cellpadding="1" cellspacing="0" border="0" width="100%" height="100%">
	        <tr>
	         <td>
	          <table cellpadding="1" cellspacing="0" border="0" width="100%" height="100%">
	           <tr><td class="label2"><b>Medical Request Date</b></td></tr>
	           <tr>
	            <td>
	             <table class="listtable1" cellpadding="1" cellspacing="0" border="1" width="38%" bordercolor="#2A4C7C">
			      <tr>
			       <td>
  	  	            <!-- date period -->
	  	            <table cellpadding="2" cellspacing="0" border="0" width="100%">
	  	             <tr>
	  		          <td class="label2" width="36%"><b>Start Date</b></td>
  	  		          <td width="35%"><html:text styleClass="label2" name="reportForm" property="startDate" onkeyup="getKeyDate(event, this);"/></td>
	  		          <td width="29%">
						<a href="#" onClick="popUpWin('<%=contextPath%>/jsp/calendar.jsp?textbox=formFollowMed.startDate',290,155);">
				            <img src="<%=contextPath%>/images/calendar.gif" height="17" width="19" border="0">
				        </a>	  		          
	  		          </td>
	  		         </tr>
	  		         <tr>
	  		          <td class="label2"><b>End Date</b></td>
  	  		          <td><html:text styleClass="label2" name="reportForm" property="endDate" onkeyup="getKeyDate(event, this);"/></td>
	  		          <td>
						<a href="#" onClick="popUpWin('<%=contextPath%>/jsp/calendar.jsp?textbox=formFollowMed.endDate',290,155);">
				            <img src="<%=contextPath%>/images/calendar.gif" height="17" width="19" border="0">
				        </a>	
	  		          </td>
	  		         </tr>
	  	            </table>
			       </td>
	              </tr>
	             </table>
	            </td>
	           </tr>
               <tr><td>&nbsp;</td></tr>
               <tr>
                <td>
                 <!-- Report Types -->
                 <table cellpadding="2" cellspacing="0" border="0" width="100%">
                  <tr>
               	   <td class="label2" width="12%"><b>Report Type</b></td>
            	   <td class="label2" width="3%">
            	   	<input type="radio" name="reportType" value="<%=ARWithPendingMedFilterData.BRANCH_REPORT_TYPE%>" onClick="manageFilters();" 
						<logic:equal name="reportForm" property="reportType" value="<%=ARWithPendingMedFilterData.BRANCH_REPORT_TYPE%>" >CHECKED</logic:equal>
	            	  />
            	   </td>
            	   <td class="label2" width="8%"><b>Branch</b></td>
            	   <td class="label2" width="3%">
            	   	<input type="radio" name="reportType" value="<%=ARWithPendingMedFilterData.UNDERWRITER_REPORT_TYPE%>" onClick="manageFilters();" 
						<logic:equal name="reportForm" property="reportType" value="<%=ARWithPendingMedFilterData.UNDERWRITER_REPORT_TYPE%>" >CHECKED</logic:equal>
            	   	/>
            	   </td>
            	   <td class="label2" width="18%"><b>Underwriter</b></td>
            	   <td class="label2" width="3%">
            	   	<input type="radio" name="reportType" value="<%=ARWithPendingMedFilterData.EXAMINER_REPORT_TYPE%>" onClick="manageFilters();" 
						<logic:equal name="reportForm" property="reportType" value="<%=ARWithPendingMedFilterData.EXAMINER_REPORT_TYPE%>" >CHECKED</logic:equal>
            	    />
            	   </td>
            	   <td class="label2" width="15%"><b>Examiner</b></td>
            	   <td class="label2" width="3%">
            	   	<input type="radio" name="reportType" value="<%=ARWithPendingMedFilterData.LABORATORY_REPORT_TYPE%>" onClick="manageFilters();" 
						<logic:equal name="reportForm" property="reportType" value="<%=ARWithPendingMedFilterData.LABORATORY_REPORT_TYPE%>" >CHECKED</logic:equal>	
            	   	/>
            	   </td>
             	   <td class="label2" width="33%"><b>Laboratory</b></td>
                  </tr>
                 </table>          
                </td>
               </tr>
               <tr><td>&nbsp;</td></tr>
               <tr><td class="label2"><b>Filters</b></td></tr>
               <tr>
                <td>
	             <table class="listtable1" cellpadding="2" cellspacing="0" border="1" width="75%" bordercolor="#2A4C7C">
	              <tr>
	               <td>
	  		        <!-- Filters -->
	  		        <table cellpadding="2" cellspacing="0" border="0" width="100%">
	  		         <tr>
	  		          <td class="label2" width="1%"><b>Branch</b></td>
  	  		          <td width="39%">
  	  		           <ium:list className="label2" listBoxName="branch" styleName="width:200"
  	  		           	type="<%=IUMConstants.LIST_BRANCHES%>" selectedItem="<%=reportForm.getBranch()%>" />
  	  		          </td>  	  		          
	  		          <td class="label2" width="22%"><b>Underwriter</b></td>
  	  		          <td width="38%">
  	  		           <ium:list className="label2" listBoxName="underwriter" styleName="width:200"
	 	  		      	type="<%=IUMConstants.LIST_USERS%>" selectedItem="<%=reportForm.getUnderwriter()%>" />
  	  		          </td>  	  		          
	  		         </tr>	  		         
	  		         <tr>
	  		          <td class="label2" width="20%"><b>Examiner</b></td>
  	  		          <td>
  	  		           <ium:list className="label2" listBoxName="examiner" styleName="width:200"
  	  		           	type="<%=IUMConstants.LIST_EXAMINERS%>" selectedItem="<%=reportForm.getExaminer()%>" />
  	  		          </td>  	  		          
	  		          <td class="label2"><b>Laboratory</b></td>
  	  		          <td>
  	  		           <ium:list className="label2" listBoxName="laboratory" styleName="width:200"
	  	  		       	type="<%=IUMConstants.LIST_LABORATORIES%>" selectedItem="<%=reportForm.getLaboratory()%>"/>
  	  		          </td>  	  		          
	  		         </tr>
	  		        </table>	  				 
	  		       </td>
	  		      </tr>
	  		     </table>
                </td>
               </tr>	     
	          </table>
   	         </td>  
	        </tr>
	        <tr>
	         <td>
	          <table cellpadding="1" cellspacing="0" border="0" width="75%">
	           <tr>
	            <td>
		         	<input type="button" class="button1" value="Generate" style="width:100" onClick="generateReport();" />
		         	&nbsp;
	    	     	<input type="button" class="button1" value="Reset" style="width:100" onClick="gotoPage('reportForm', 'viewReports.do')"/>
	    	     </td>
	    	    </tr>
	    	   </table>
	         </td>
	        </tr>
	       </table> <!-- end of parameter grouping -->
   	      </td>
	     </tr>
	    </table> <!-- end of Report Parameters border -->
	   </td>
       <td width="12%">&nbsp;</td> <!-- minimizing horizontal scrolling -->
	  </tr>
     </table> <!-- end of Report Parameters container -->      
 	</td>
 </tr>
  <input type="hidden" name="pageNo" value="<%=reportForm.getPageNo()%>">
 <!-- report content --> 	
<%
   int i=0;
   String tr_class;
   String td_bgcolor;
   String msgline = "&nbsp;";
   
   String colHeader1 = "";
   String colHeader2 = "";
   
   String COL_BRANCH = "Sun Life Office/Branch";
   String COL_EXMMED = "Examiner/Laboratory";
   String COL_EXAMINER = "Examiner";
   String COL_LABORATORY = "Laboratory";
   String COL_UNDERWRITER = "Underwriter";

   if (displayPage != null) {

	switch (Integer.parseInt(reportForm.getReportType())) {
		case ARWithPendingMedFilterData.BRANCH_REPORT_TYPE:
			colHeader1 = COL_BRANCH;
			colHeader2 = COL_EXMMED;
			break;
		case ARWithPendingMedFilterData.LABORATORY_REPORT_TYPE:
			colHeader1 = COL_LABORATORY;
			colHeader2 = COL_BRANCH;
			break;
		case ARWithPendingMedFilterData.UNDERWRITER_REPORT_TYPE:
			colHeader1 = COL_UNDERWRITER;
			colHeader2 = COL_EXMMED;
			break;
		case ARWithPendingMedFilterData.EXAMINER_REPORT_TYPE:
			colHeader1 = COL_EXAMINER;
			colHeader2 = COL_BRANCH;
		    break;
	}

%>
 <tr>
  <td>
   <table border="0" cellpadding="2" cellspacing="1" bgcolor=#FFFFFF style="border: 1px solid #D1D1D1;width:88%" class="listtable1">
<%
	if (displayPage.getList().size() > 0) {
%>
    <tr>
    	<td colspan="7" class="label2" align="right">
    		<a href="#" onClick="javascript:showPrinterFriendly();">Printer Friendly</a>
    	</td>
    </tr>
<%
	}
%>
    <tr class="headerrow1">
     <td><%=colHeader1%></td>
     <td><%=colHeader2%></td>
     <td>Reference No.</td>     
     <td>Test</td>
     <td>Date Requested</td>     
     <td>Target Date of Submission</td>     
     <td>Total Count</td>
    </tr>
    <logic:iterate id="rpt" name="displayPage" property="list">
<%
   if (i%2 == 0) {
   	tr_class = "row1";
    td_bgcolor = "#CECECE";
   }
   else {
    tr_class = "row2";
    td_bgcolor = "#EDEFF0";
   }
%>
  <tr class=<%=tr_class%>>
<%

	switch (Integer.parseInt(reportForm.getReportType())) {
		case ARWithPendingMedFilterData.BRANCH_REPORT_TYPE:
%>
			<td width="20%"><b><bean:write name="rpt" property="branchName" /></b></td>
			<td width="20%"><bean:write name="rpt" property="nameExaminerLab" /></td>
<%
			break;
		case ARWithPendingMedFilterData.LABORATORY_REPORT_TYPE:
%>
			<td width="20%"><b><bean:write name="rpt" property="nameExaminerLab" /></b></td>
			<td width="20%"><bean:write name="rpt" property="branchName" /></td>
<%
			break;
		case ARWithPendingMedFilterData.UNDERWRITER_REPORT_TYPE:
%>
			<td width="20%"><b>
				<bean:write name="rpt" property="underwriterLastName" /><logic:notEmpty name="rpt" property="underwriterGivenName">,&nbsp;</logic:notEmpty><bean:write name="rpt" property="underwriterGivenName" />
			</b></td>
			<td width="20%"><bean:write name="rpt" property="nameExaminerLab" /></td>
<%			break;
		case ARWithPendingMedFilterData.EXAMINER_REPORT_TYPE:
%>
			<td width="20%"><b><bean:write name="rpt" property="nameExaminerLab" /></b></td>
			<td width="20%"><bean:write name="rpt" property="branchName" /></td>
<%
		    break;
	}
%>
   <td width="14%"><bean:write name="rpt" property="referenceNumber" /></td>
   <td width="23%"><bean:write name="rpt" property="testDescription" /></td>
   <td width="9%"><bean:write name="rpt" property="dateRequested" /></td>
   <td width="9%"><bean:write name="rpt" property="dateSubmitted" /></td>
   <td align="right" width="5%">
   	<logic:notEqual name="rpt" property="totalCount" value="0">
   		<bean:write name="rpt" property="totalCount" />
   	</logic:notEqual>
   </td>   
  </tr>                                       
<%i++;%>
 </logic:iterate>
<% 
	if (i==0) {
	  msgline = request.getParameter("msgnoresult") + " ";
	  msgline += request.getParameter("msgtryagain");
%>
		<td class='label2' align='center' colspan="7"><%=msgline%></td>
<%	} 
	else {
		int pageNumber = reportForm.getPageNo();
%>
       	<td class="headerrow4" colspan="11" width="100%" height="10" class="label2" valign="bottom" >
        <!-- don't display link for previous page if the current page is the first page -->					
        <% if (pageNumber > 1) { %>
        	<a href="#" onclick="javascript:rePaginate(1,'GenerateReportFollowMedAction.do');"><img src="images/pagnavfirst_off.gif" name="pagnavfirst" border="0" width="18" height="13"/></a>&nbsp;					
            <a href="#" onclick="javascript:rePaginate(<%=pageNumber-1%>,'GenerateReportFollowMedAction.do');"><img src="images/pagnavleft_off.gif" name="pagnavleft" border="0" width="18" height="13"/></a>
        <% } else {%>
        	<img src="images/pagnavfirst_on.gif" name="pagnavfirst" border="0" width="18" height="13"/>&nbsp;
            <img src="images/pagnavleft_on.gif" name="pagnavleft" border="0" width="18" height="13"/>
        <% } %>
        <% 
        int currLink = 1;
        int prevLink = 1;
        %>
        <logic:iterate id="navLinks" name="displayPage" property="pageNumbers">
	        <% currLink = ((Integer)navLinks).intValue(); %>
	        <% if (((Integer)navLinks).intValue() == pageNumber) { %>
	        	<b><bean:write name="navLinks"/></b>
	        <% } else { %>		
	        	<% if ((currLink > (prevLink+1))) { %>
	            	...
			<% } %>
	        	<a href="#" onclick="rePaginate('<bean:write name="navLinks"/>','GenerateReportFollowMedAction.do');" class="links2"><bean:write name="navLinks"/></a>
			<% } %>
        	<% prevLink = currLink; %>
        </logic:iterate>
		<!-- don't display link for next page if the current page is the last page -->
        <%  if(pageNumber < prevLink) {  %>
        	<a href="#" onClick="rePaginate('<%=pageNumber+1%>','GenerateReportFollowMedAction.do');" class=links1><img src="<%=contextPath%>/images/pagnavright_off.gif" name="pagnavright" border="0" width="18" height="13"/></a>&nbsp;
        	<a href="#" onClick="rePaginate('<%=prevLink%>','GenerateReportFollowMedAction.do');" class=links1><img src="<%=contextPath%>/images/pagnavlast_off.gif" width="18" height="13" border="0"></a>
        <%  } else {  %>
        	<img src="<%=contextPath%>/images/pagnavright_on.gif" width="18" height="13" border="0">&nbsp;
            <img src="<%=contextPath%>/images/pagnavlast_on.gif" width="18" height="13" border="0">
        <%  }  %>
		</td>
<%  }  %>		
	</tr>
   </table>
  </td>
 </tr>
<%  } %>
</table>
</form>
<script language="javascript" type="text/javascript">
<!--
// Examiner Filter - Examiner, Branch and Underwriter Type
// Laboratory Filter Laboratory, Branch and Underwriter Type
 manageFilters();

 function showPrinterFriendly() {
	frm = document.formFollowMed;
	typeSelected = frm.reportType;
 	
 	parmReports = "?";
 	parmReports += "startDate=" + '<bean:write name="reportForm" property="startDate" />';
 	parmReports += "&endDate=" + '<bean:write name="reportForm" property="endDate" />';
 	parmReports += "&reportType=" + '<bean:write name="reportForm" property="reportType" />';
 	parmReports += "&branch=" + '<bean:write name="reportForm" property="branch" />';
 	parmReports += "&examiner=" + '<bean:write name="reportForm" property="examiner" />';
 	parmReports += "&laboratory=" + '<bean:write name="reportForm" property="laboratory" />';
 	parmReports += "&underwriter=" + '<bean:write name="reportForm" property="underwriter" />';
 	
	windowParms = "width=750, height=650";
	printerFriendly = window.open('GenerateReportFollowMedPrintAction.do' + parmReports, "printFriendly", windowParms);
	printerFriendly.focus();
 }


 function resetPage() {
   document.formFollowMed.reset();
   manageFilters();
 }

 function rePaginate (page, actionUrl)
 {
	var frm = document.formFollowMed;
	if(frm.pageNo != null)
	{
		frm.pageNo.value = page;	
	}
	submitForm(actionUrl);
 }
 function submitForm(actionUrl)
 {	
	document.formFollowMed.action = actionUrl;
    document.formFollowMed.submit();
 }
  
 function manageFilters() {
  thisForm = document.formFollowMed;
  typeSelected = document.formFollowMed.reportType;

  thisForm.examiner.disabled = false;
  thisForm.branch.disabled = false;  
  thisForm.laboratory.disabled = false;
  thisForm.underwriter.disabled = false;  

  for (i=0; i<typeSelected.length; i++) {
   if (typeSelected[i].checked) {
	switch (i) {
	 case 2: // examiner type
	  thisForm.laboratory.disabled = true;
	  thisForm.laboratory.selectedIndex = 0;
	  break;
	 case 3: // laboratory type
	  thisForm.examiner.disabled = true;
	  thisForm.examiner.selectedIndex = 0;
	  break;		  		  
	 }
   }
  }
  
  return true;		   
 }

 function generateReport() {
  var frm = document.formFollowMed;
  if (!validateForm(document.formFollowMed)) return false; // validate the parameter values
  frm.pageNo.value = "1"; // reset the pagination
  gotoPage('formFollowMed','GenerateReportFollowMedAction.do'); // submit the form
 }
 
 function validateForm(frm) {

  // check for required dates and date formats
  if (isEmpty(frm.startDate.value)) {
  	alert ("Start Date is a required field.");
  	frm.startDate.focus();
  	return false;
  }
  
  if (!isValidDate(frm.startDate.value)) {
   alert('<bean:message key="error.field.format" arg0="Start Date" arg1="date" arg2="(ddMMMyyyy)"/>');
   frm.startDate.focus();
   return false;
  }
  
  if (isEmpty(frm.endDate.value)) {
  	alert ("End Date is a required field.");
  	frm.endDate.focus();
  	return false;
  }

  if (!isValidDate(frm.endDate.value)) {
   alert('<bean:message key="error.field.format" arg0="End Date" arg1="date" arg2="(ddMMMyyyy)"/>');
   frm.endDate.focus();
   return false;
  }
  
  // make sure the start date is earlier than the end date
  if (isGreaterDate(frm.startDate.value, frm.endDate.value)) {
   alert('<bean:message key="error.field.beforeenddate" arg0="Start Date" arg1="End Date" />');
   frm.startDate.focus();
   return false;
  }

  return true;
 }
-->
</script>