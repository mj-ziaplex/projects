<%@ taglib uri="/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/struts-logic.tld" prefix="logic" %>
<%@ page language="java" import="java.util.*, com.slocpi.ium.ui.form.*, com.slocpi.ium.util.*, com.slocpi.ium.ui.util.*, com.slocpi.ium.data.*, com.slocpi.ium.underwriter.*" buffer="1024kb"%>
<%@ taglib uri="/struts-html.tld" prefix="html" %>
<%@ taglib uri="/ium.tld" prefix="ium" %>
<%String contextPath = request.getContextPath();%>
<html>
  <head>
    <title>Integrated Underwriting and Medical System</title>
    <link href="<%=contextPath%>/css/main.css" rel="stylesheet" type="text/css">
    <script language="Javascript" src="<%=contextPath%>/js/navigation.js"></script>
  </head>
  
  <body leftmargin="0" topmargin="0">
  
   <%  //WMS session expiration = close browser
		ResourceBundle rb1 = ResourceBundle.getBundle("wmsconfig");
		String wmsContextPath1 = rb1.getString("wms_context_path");
		String TABLE_WIDTH = "";
       	if(contextPath.equalsIgnoreCase(wmsContextPath1)){ 
       		TABLE_WIDTH = "660";
  		}
  		else{
  			TABLE_WIDTH = "750";
  		}%>
  
    <table border="0" width="<%=TABLE_WIDTH%>" cellpadding="2" cellspacing="1" bgcolor=#FFFFFF style="border: 1px solid #D1D1D1;" class="listtable1">
      <tr>
        <td class="headerrow6" width="10%">CODE</td>
        <td class="headerrow6" width="50%">TEST DESCRIPTION</td>
        <td class="headerrow6" width="10%">TYPE</td>
        <td class="headerrow6" width="15%">DATE CONDUCTED</td>
        <td class="headerrow6" width="15%">DATE RECEIVED</td>        
      </tr>
      <%
      int i=0;
      String tr_class;
      String td_bgcolor;
      %>
      <logic:iterate id="med" name="detailForm" property="medLabRecords" scope="session"> 
      <%
      if (i%2 == 0) {
        tr_class = "row1";
        td_bgcolor = "#CECECE";
      } else {
        tr_class = "row2";
        td_bgcolor = "#EDEFF0";
      }
      %>
      <tr>
        <td class="<%=tr_class%>" align="center"><bean:write name="med" property="code"/></td>
        <td class="<%=tr_class%>"><bean:write name="med" property="testDescription"/></td>
        <td class="<%=tr_class%>" align="center"><bean:write name="med" property="type"/></td>
        <td class="<%=tr_class%>"><bean:write name="med" property="dateConducted"/></td>
        <td class="<%=tr_class%>"><bean:write name="med" property="dateReceived"/></td>
      </tr>
        <%i++;%>
      </logic:iterate>
      <% if (i == 0) { %>
      <tr>
        <td class="label2" colspan="5"><bean:message key="message.noexisting" arg0="medical and lab records"/></td>                                                    
      </tr>
      <% } %>
    </table>
  </body>
</html>
