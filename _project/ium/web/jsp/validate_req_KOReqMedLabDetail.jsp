<%@ page language="java" import="com.slocpi.ium.util.IUMConstants" buffer="12kb"%>
<%@ taglib uri="/struts-bean.tld" prefix="bean" %>
<!--
var RequirementsClientType = 'I';

function createRequirement(lob) {
	var form = document.requirementForm;
	form.seqDisplay.value="";
	form.seq.value="";
	enable(form.reqCode);
	form.reqCode.options[0].selected = true;
	if (lob == "<%= IUMConstants.LOB_INDIVIDUAL_LIFE %>") {
		enable(form.clientTypeDisplay[0]);
		enable(form.clientTypeDisplay[1]);
	}
	else {
		disable(form.clientTypeDisplay);
	}
	if (lob != "<%= IUMConstants.LOB_INDIVIDUAL_LIFE %>") {
		enable(form.completeReq);
		if (form.completeReq.checked == true) {
		    form.completeReq.click();
		}
	}
	enable(form.designation);
	enable(form.testDate);
	enable(form.resolve[0]);
	enable(form.resolve[1]);
	enable(form.followUpNo);
	enable(form.validityDate);
	enable(form.paidInd[0]);
	enable(form.paidInd[1]);
	enable(form.newTest[0]);
	enable(form.newTest[1]);
	enable(form.autoOrder[0]);
	enable(form.autoOrder[1]);
	enable(form.fldComm[0]);
	enable(form.fldComm[1]);
	enable(form.comment);
	if (lob == "<%= IUMConstants.LOB_INDIVIDUAL_LIFE %>") {
		//form.clientTypeDisplay[0].checked = false;
		form.clientTypeDisplay[0].checked = true;
		form.clientTypeDisplay[1].checked = false;
	}
	//form.reqStatusCode.value  = "NTO";
	//form.reqStatusValue.value = "<%= IUMConstants.STATUS_NTO %>";
	form.reqStatusCode.value  = "ORD";
	form.reqStatusValue.value = "<%= IUMConstants.STATUS_ORDERED %>";
	form.reqDesc.value        = "";
	form.statusDate.value     = "";
	form.updatedBy.value      = userId;
	form.updatedDate.value    = getFormattedCurrentDate();
	form.designation.value    = "";
	form.testDate.value       = "";
	form.testResult.value     = "";
	form.testDesc.value       = "";
	form.followUpDate.value   = "";
	form.validityDate.value   = "";
	form.dateSent.value       = "";
	form.clientTypeDisplay[0].click();
	form.resolve[1].click();
	form.paidInd[1].click();
	form.newTest[1].click();
	form.autoOrder[1].click();
	form.fldComm[1].click();
	form.orderDate.value     = "";
	form.ccasSuggest.value   = "";
	form.receivedDate.value  = "";
	form.comment.value       = "";
	document.all["doc_cal1"].style.display = "block";
	document.all["doc_cal2"].style.display = "block";
	document.all["doc_cal3"].style.display = "none";
}


function maintainRequirement() {
	var form = document.requirementForm;
	enable(form.designation);
	enable(form.testDate);
	//enable(form.testResult);
	enable(form.resolve[0]);
	enable(form.resolve[1]);
	enable(form.followUpNo);
	enable(form.followUpDate);
	enable(form.validityDate);
	enable(form.paidInd[0]);
	enable(form.paidInd[1]);
	enable(form.newTest[0]);
	enable(form.newTest[1]);
	enable(form.autoOrder[0]);
	enable(form.autoOrder[1]);
	enable(form.fldComm[0]);
	enable(form.fldComm[1]);
	enable(form.comment);
	document.all["doc_cal1"].style.display = "block";
	document.all["doc_cal2"].style.display = "block";
	document.all["doc_cal3"].style.display = "block";
	form.updatedBy.value      = userId;
	form.updatedDate.value    = getFormattedCurrentDate();
}


function disableRequirementFields(lob) {
	var form = document.requirementForm;
	disable(form.seqDisplay);
	disable(form.reqCode);
	//disable(form.reqDesc);
	//disable(form.createdDate);
	if (lob == "<%= IUMConstants.LOB_INDIVIDUAL_LIFE %>") {
		disable(form.clientTypeDisplay[0]);
		disable(form.clientTypeDisplay[1]);
	}
	else {
		disable(form.clientTypeDisplay);
	}
	//disable(form.clientNo);
	//disable(form.reqStatusCode);
	//disable(form.statusDate);
	disable(form.completeReq);
	//disable(form.updatedBy);
	//disable(form.updatedDate);
	disable(form.designation);
	disable(form.testDate);
	//disable(form.testResult);
	disable(form.ccasSuggest);
	//disable(form.testDesc);
	disable(form.resolve[0]);
	disable(form.resolve[1]);
	disable(form.followUpNo);
	disable(form.followUpDate);
	disable(form.validityDate);
	disable(form.paidInd[0]);
	disable(form.paidInd[1]);
	disable(form.newTest[0]);
	disable(form.newTest[1]);
	//disable(form.orderDate);
	//disable(form.dateSent);
	//disable(form.receivedDate);
	disable(form.autoOrder[0]);
	disable(form.autoOrder[1]);
	disable(form.fldComm[0]);
	disable(form.fldComm[1]);
	//disable(form.reqLevel[0]);
	//disable(form.reqLevel[1]);
	//disable(form.reqLevelDisplay);
	disable(form.comment);
	document.all["doc_cal1"].style.display = "none";
	document.all["doc_cal2"].style.display = "none";
	document.all["doc_cal3"].style.display = "none";

}


function setReqParam(refNo, reqId, form, url) {
	
	document.requirementForm.reqId.value = reqId;
	document.requirementForm.refNo.value = refNo;
	gotoPage(form, url);
}




function orderRequirement(contextPath, facilitator, form) {
	//var form = document.forms[0];
	//alert("order requirements start");
	var lob      = form.lob.value;
	//alert("lob:"+lob);
	var branchId = form.branchCode.value;
	//alert("branchId:"+branchId);
	var agentId  = form.agentCode.value;
	//alert("agentId:"+agentId);
	var reqStat  = parent.requestForm.currentStatus.value;
	//alert("reqStat:"+reqStat);
	form.action  = contextPath + "/saveRequirementStatus.do?lob=" + lob + "&facilitator=" + facilitator + "&branchCode=" + branchId + "&agentCode=" + agentId + "&arStatus=" + reqStat;
	//alert("order requirements to submit");
	
		//try{
		//	showLayer();
		//	window.question.focus();
		//	setLayerPosition2();
		//}catch(e){
		//}
		
	form.submit();
	
}


function validateRequirement(contextPath,lob,session_id) {
	var form   = document.requirementForm;
	var selInd = form.reqCode.options.selectedIndex;
	form.reqCodeValue.value = form.reqCode.options[selInd].text;
	if (form.reqCode.options[selInd].value == "-") {
		alert("<bean:message key="error.field.requiredselection" arg0="Requirement code"/>");
		form.reqCode.focus();
		return;
	}
	if (lob == "<%= IUMConstants.LOB_INDIVIDUAL_LIFE %>") {
		if (!form.clientTypeDisplay[0].checked && !form.clientTypeDisplay[1].checked) {
			alert("<bean:message key="error.field.requiredselection" arg0="Client type"/>");
			form.clientTypeDisplay[0].focus();
			return;
		}
	}
	if (trim(form.testDate.value) != "") {
		if (!isValidDate(form.testDate.value)) {
		   alert("<bean:message key="error.field.date" arg0="Test Date"/>");
		   form.testDate.focus();
		   form.testDate.select();
		   return false;
		}
    }
	if (trim(form.validityDate.value) != "") {
		if (!isValidDate(form.validityDate.value)) {
		   alert("<bean:message key="error.field.date" arg0="Validity Date"/>");
		   form.validityDate.focus();
		   form.validityDate.select();
		   return false;
		}
	}
	if (trim(form.followUpNo.value) != "") {
	    if (!isNumeric(trim(form.followUpNo.value))) {
		    alert("<bean:message key="error.field.numeric" arg0="Followup No"/>");
		    form.followUpNo.focus();
		    form.followUpNo.select();
	        return;
	    }
	}
	else {
	    alert("<bean:message key="error.field.required" arg0="Followup No"/>");
	    form.followUpNo.value = "";
	    form.followUpNo.focus();
        return;
	}
	if (form.completeReq.checked == true) {
	    form.completeReq.value = "Y";
	}
	form.action = contextPath + "/createRequirement.do?session_id=" +session_id;
	
		//try{
		//	showLayer();
		//	window.question.focus();
		//	setLayerPosition2();
		//}catch(e){
		//}
			
	form.submit();

}

function changeLevelAndType(lob) {
    var form   = document.requirementForm;
    var selInd = form.reqCode.options.selectedIndex;
    var val    = form.reqCode.options[selInd].value
    var arr    = val.split("-");
    var level  = arr[0];
    var cType  = arr[1];
    var fNum   = arr[2];
    if (cType == "null") {
        cType = "";
    }
    if (fNum == "null") {
        fNum = "";
    }
    form.reqLevel.value          = level;
    form.followUpNo.value        = fNum;
    if (level == "<%= IUMConstants.LEVEL_POLICY %>") {
        form.reqLevelDisplay.value = "Policy";
		if (lob == "<%= IUMConstants.LOB_INDIVIDUAL_LIFE %>") {
		        form.clientTypeDisplay[0].click();
		}
    }
    if (level == "<%= IUMConstants.LEVEL_CLIENT %>") {
        form.reqLevelDisplay.value = "Client";
		if (lob == "<%= IUMConstants.LOB_INDIVIDUAL_LIFE %>") {
	        if (cType == "<%= IUMConstants.CLIENT_TYPE_INSURED %>") {
    	       form.clientTypeDisplay[0].click();
        	}
	        if (cType == "<%= IUMConstants.CLIENT_TYPE_OWNER %>") {
    	       form.clientTypeDisplay[1].click();
        	}
        }
    }
    if (cType != "") {
	    form.clientType.value = cType;
	}
	else {
		if (lob == "<%= IUMConstants.LOB_INDIVIDUAL_LIFE %>") {
		    if (level == "<%= IUMConstants.LEVEL_POLICY %>" || level == "<%= IUMConstants.LEVEL_CLIENT %>") {
			    if (form.clientTypeDisplay[0].checked == true) {
			        form.clientType.value = "<%= IUMConstants.CLIENT_TYPE_INSURED %>";
	        	}
			    if (form.clientTypeDisplay[1].checked == true) {
			        form.clientType.value = "<%= IUMConstants.CLIENT_TYPE_OWNER %>";
	    	    }
		    }
		}
	}
	
	changeReqCodeValue();
}

function changeType(lob) {
    var form   = document.requirementForm;
    var selInd = form.reqCode.options.selectedIndex;
    var val    = form.reqCode.options[selInd].value
    var arr    = val.split("-");
    var level  = arr[0];
    var cType  = arr[1];
    if (cType == "null") {
        cType = "";
    }
    form.reqLevel.value          = level;
    if (level == "<%= IUMConstants.LEVEL_POLICY %>") {
        form.reqLevelDisplay.value = "Policy";
		if (lob == "<%= IUMConstants.LOB_INDIVIDUAL_LIFE %>") {
		        form.clientTypeDisplay[0].click();
		}
    }
    if (level == "<%= IUMConstants.LEVEL_CLIENT %>") {
        form.reqLevelDisplay.value = "Client";
		if (lob == "<%= IUMConstants.LOB_INDIVIDUAL_LIFE %>") {
	        if (cType == "<%= IUMConstants.CLIENT_TYPE_INSURED %>") {
    	       form.clientTypeDisplay[0].click();
        	}
	        if (cType == "<%= IUMConstants.CLIENT_TYPE_OWNER %>") {
    	       form.clientTypeDisplay[1].click();
        	}
        }
    }
    if (cType != "") {
	    form.clientType.value = cType;
	}
	else {
		if (lob == "<%= IUMConstants.LOB_INDIVIDUAL_LIFE %>") {
		    if (level == "<%= IUMConstants.LEVEL_POLICY %>" || level == "<%= IUMConstants.LEVEL_CLIENT %>") {
			    if (form.clientTypeDisplay[0].checked == true) {
			        form.clientType.value = "<%= IUMConstants.CLIENT_TYPE_INSURED %>";
	        	}
			    if (form.clientTypeDisplay[1].checked == true) {
			        form.clientType.value = "<%= IUMConstants.CLIENT_TYPE_OWNER %>";
	    	    }
		    }
		}
	}

}

function changeClientType() {
    var form   = document.requirementForm;
    var cType  = "";
    if (form.clientTypeDisplay[0].checked == true) {
        cType = "<%= IUMConstants.CLIENT_TYPE_INSURED %>";
    }
    if (form.clientTypeDisplay[1].checked == true) {
        cType = "<%= IUMConstants.CLIENT_TYPE_OWNER %>";
    }
    
    RequirementsClientType = cType;
    changeReqCodeValue();
    form.clientType.value = cType;

}

function changeReqCodeValue() {
	// Change the value of reqCode entries
	var form   = document.requirementForm;
	var selInd = form.reqCode.options.selectedIndex;
	var reqCodeVal = form.reqCode.options[selInd].value;
	var arrReqCode = reqCodeVal.split('-');
	reqCodeVal = arrReqCode[0] + '-' + RequirementsClientType + '-' + arrReqCode[2];
	form.reqCode.options[selInd].value = reqCodeVal;
	form.clientType.value = RequirementsClientType;
}

function haveSelectedStatus(){
	var form = document.requestForm;
	var status = form.requestStatus.value;
	if (status == null || status ==""){
		alert("<bean:message key="error.field.requiredselection" arg0="status"/>");
		form.requestStatus.focus();
		return false;
	} else {
		return true;
	}
}



function validateForMaintainRequirement(contextPath,lob,session_id) {
	var form   = document.requirementForm;
	if (trim(form.testDate.value) != "") {
		if (!isValidDate(trim(form.testDate.value))) {
		   alert("<bean:message key="error.field.date" arg0="Test Date"/>");
		   form.testDate.focus();
		   form.testDate.select();
		   return false;
		}
    }

	if (trim(form.validityDate.value) != "") {
		if (!isValidDate(trim(form.validityDate.value))) {
		   alert("<bean:message key="error.field.date" arg0="Validity Date"/>");
		   form.validityDate.focus();
		   form.validityDate.select();
		   return false;
		}
	}

	if (trim(form.followUpNo.value) != "") {
	    if (!isNumeric(trim(form.followUpNo.value))) {
		    alert("<bean:message key="error.field.numeric" arg0="Followup No"/>");
		    form.followUpNo.focus();
		    form.followUpNo.select();
	        return;
	    }
	}
	if (trim(form.followUpNo.value) == "") {
	    alert("<bean:message key="error.field.required" arg0="Followup No"/>");
	    form.followUpNo.value = "";
       	return;
	}

	if (trim(form.followUpDate.value) != "") {
		if (!isValidDate(trim(form.followUpDate.value))) {
		   alert("<bean:message key="error.field.date" arg0="Followup Date"/>");
		   form.followUpDate.focus();
		   form.followUpDate.select();
		   return false;
		}
	}

	form.action = contextPath + "/createRequirement.do?mode=update&session_id="+session_id;
			//try{
			//	showLayer();
			//	window.question.focus();
			//	setLayerPosition2();
			//}catch(e){
			//}
	form.submit();
	
}

function countStatusOrdered(form) {
    //var form = document.forms[0];
    var cnt  = 0;
  
    
   if (form.index[0]) {
        for (var i=0;i<form.index.length;i++) {
           if (form.index[i].disabled == false) {
	         if (form.statusCode[i].value == "<%= IUMConstants.STATUS_ORDERED %>") {
	            cnt++;
	         }
	       }
	    }
   }
   else {
      if (form.index.disabled == false) {
       if (form.statusCode.value == "<%= IUMConstants.STATUS_ORDERED %>") {
          cnt++;
       }
      }
    }
    if (cnt > 0) {
        return "true";
    }
    else {
        
        return "false";
    }
}

    function disableCheckBox(form) {
        //var form = document.forms[0];
        form.selAll.disabled      = true;
        form.saveBtn.disabled     = true;
        form.statusTo.disabled    = true;
        form.followUp.disabled    = true;
        form.facilitator.disabled = true;
    }

    function countDisabledCheckBoxes(form) {
        //var form = document.forms[0];
        var cnt  = 0;
        if (form.index[0]) {
	        for (var i=0;i<form.index.length;i++) {
    	        if (form.index[i].disabled) {
        	        cnt++;
            	}
	        }
    	    if (cnt == form.index.length) {
        	    disableCheckBox(form);
	        }
	    }
    }

    function countDisabledCheckBox(form) {
        //var form = document.forms[0];
        var cnt  = 0;
        if (form.index) {
	        if (form.index.disabled) {
	            cnt++;
	        }
	        if (cnt > 0) {
	            disableCheckBox(form);
	        }
	    }
    }

function showCreateMaintain(){
	//document.all["createMaintain"].className = "show";
	document.all["createMaintain"].style.display = "block";
}

function showCreateMaintain2(){
	//document.all["createMaintain2"].className = "show";
	//document.all["createMaintain2"].style.display = "block";
}

function hideCreateMaintain(){
	//document.all["createMaintain"].className = "hide";
	document.all["createMaintain"].style.display = "none";
}

function hideCreateMaintain2(){
	//document.all["createMaintain2"].className = "hide";
	//document.all["createMaintain2"].style.display = "none";
}


function showSaveCancel(){
	//document.all["saveCancel"].className = "show";
	document.all["saveCancel"].style.display = "block";
}

function showSaveCancel2(){
	//document.all["saveCancel2"].className = "show";
	//document.all["saveCancel2"].style.display = "block";
}

function hideSaveCancel(){
	//document.all["saveCancel"].className = "hide";
	document.all["saveCancel"].style.display = "none";
}

function hideSaveCancel2(){
	//document.all["saveCancel2"].className = "hide";
	//document.all["saveCancel2"].style.display = "none";
}


function showMaintainCancel(){
	//document.all["maintainCancel"].className = "show";
	document.all["maintainCancel"].style.display = "block";
}

function showMaintainCancel2(){
	//document.all["maintainCancel2"].className = "show";
	//document.all["maintainCancel2"].style.display = "block";
}

function hideMaintainCancel(){
	//document.all["maintainCancel"].className = "hide";
	document.all["maintainCancel"].style.display = "none";
}

function hideMaintainCancel2(){
	//document.all["maintainCancel2"].className = "hide";
	//document.all["maintainCancel2"].style.display = "none";
}



function sendNotification(contextPath,savedSectionReq) {
	var form = document.requirementForm;
	
	form.action  = contextPath + "/sendNotificationOrderReqt.do?savedSectionReq="+ savedSectionReq;
	form.submit();
	
}
//new function from jonats 02/06
function enableComment(){
	document.all["comment"].readOnly = false;
}


//-->