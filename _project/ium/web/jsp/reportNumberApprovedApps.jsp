<%@ page language="java" 
         import="java.util.*, com.slocpi.ium.ui.form.*, com.slocpi.ium.util.*, com.slocpi.ium.ui.util.*, com.slocpi.ium.ui.form.ReportApprovedAppsForm"
		 buffer="1024kb"%>
<%@ taglib uri="/struts-bean.tld"  prefix="bean" %>
<%@ taglib uri="/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/struts-html.tld"  prefix="html" %>
<%@ taglib uri="/ium.tld"          prefix="ium" %>
<ium:validateSession />
<%String contextPath = request.getContextPath();%>
<%    // This routine queries the access rights of the users (RCRIVERA)
    //
    // --- START --- 
    
    StateHandler sessionHandler = new StateHandler();
    Access access = new Access();
	HashMap userAccess = sessionHandler.getUserAccess(request);
    if(!access.hasAccess(userAccess,IUMConstants.PAGE_REPORTS,IUMConstants.ACC_INQUIRE) ){                                                                                                                    
    	response.sendRedirect("noAccessPage.jsp");   
    } 

    // --- END ----
%>
<% // START OF REPORT PROCESSING
/**
 * form fields:
 *  startDate
 *  endDate
 *  requestor
 *  approver
 */
 ReportApprovedAppsForm reportForm = null; 
 Page displayPage = null;
 
 // extract the Form object
 reportForm =(ReportApprovedAppsForm) request.getAttribute("reportFilter");
 int countRecs = 0;
 if (reportForm != null)  { 
	displayPage = reportForm.getPage();
	// get record count
	for (int i=0; i<displayPage.getList().size(); i++) {
		AssessmentRequestForm ar = (AssessmentRequestForm) displayPage.getList().get(i);
		if (ar.getRefNo().length() != 0) countRecs++;
	}
	request.setAttribute("recCount", new Integer(countRecs));	
	request.setAttribute("displayPage", displayPage);
 }
 else {
 	// initialize the page
    reportForm = new ReportApprovedAppsForm();
 }
 request.setAttribute("reportForm", reportForm);
%>
<link href="<%=contextPath%>/css/pw_main.css" rel="stylesheet" type="text/css">
<link href="<%=contextPath%>/css/styleCommon.css" rel="stylesheet" type="text/css">
<link href="<%=contextPath%>/css/main.css" rel="stylesheet" type="text/css">
<script language="Javascript" src="<%=contextPath%>/js/navigation.js"></script>
<script language="Javascript" src="<%=contextPath%>/js/validation.js"></script>
<script language="javascript" type="text/javascript">
<!--
 function showPrinterFriendly() {
	frm = document.formApprovedApps;
 	
 	parmReports = "?";
 	parmReports += "startDate=" + '<bean:write name="reportForm" property="startDate" />';
 	parmReports += "&endDate=" + '<bean:write name="reportForm" property="endDate" />';
 	parmReports += "&requestor=" + '<bean:write name="reportForm" property="requestor" />';
 	parmReports += "&approver=" + '<bean:write name="reportForm" property="approver" />';
 	
	windowParms = "width=750, height=650";
	printerFriendly = window.open('GenerateReportApprovedAppsPrintAction.do' + parmReports, "printFriendly", windowParms);
	printerFriendly.focus();
 }
 
 function rePaginate (page, actionUrl)
 {
		var frm = document.formApprovedApps;
		if(frm.pageNo != null)
		{
			frm.pageNo.value = page;	
		}
		submitForm(actionUrl);
 }
 function submitForm(actionUrl)
 {	
	document.formApprovedApps.action = actionUrl;
    document.formApprovedApps.submit();
 }
 
 function generateReport() {
  if (!validateForm (document.formApprovedApps)) return false;   // validate the values
  gotoPage('formApprovedApps', 'GenerateReportApprovedAppsAction.do'); // submit the form
 }  
  
 function validateForm (frm) {
  // check for required dates and date formats
  if (isEmpty(frm.startDate.value)) {
  	alert ("Start Date is a required field.");
  	frm.startDate.focus();
  	return false;
  }
  
  if (!isValidDate(frm.startDate.value)) {
   alert('<bean:message key="error.field.format" arg0="Start Date" arg1="date" arg2="(ddMMMyyyy)"/>');
   frm.startDate.focus();
   return false;
  }
  
  if (isEmpty(frm.endDate.value)) {
  	alert ("End Date is a required field.");
  	frm.endDate.focus();
  	return false;
  }

  if (!isValidDate(frm.endDate.value)) {
   alert('<bean:message key="error.field.format" arg0="End Date" arg1="date" arg2="(ddMMMyyyy)"/>');
   frm.endDate.focus();
   return false;
  }
  
  // make sure the start date is earlier than the end date
  if (isGreaterDate(frm.startDate.value, frm.endDate.value)) {
   alert('<bean:message key="error.field.beforeenddate" arg0="Start Date" arg1="End Date" />');
   frm.startDate.focus();
   return false;
  }

  return true;  

 }
--> 
</script>
<form name="formApprovedApps" method="post">
<input type="hidden" name="reportName" value="<%=request.getParameter("reportName")%>"/>
<table cellpadding="1" cellspacing="0" border="1" width="100%" height="100%" bordercolor="#2A4C7C">
 <!-- report paramters -->
 <tr>
 	<td>
     <table cellpadding="1" cellspacing="0" border="0" width="100%" height="100%">
	  <tr><td colspan="2" class="label2"><b>Enter Report Parameters</b></td></tr>
      <tr>
       <td>
       <table class="listtable1" cellpadding="1" cellspacing="0" border="1" width="100%"  bordercolor="#2A4C7C">
        <tr>
         <td>
   	       <table cellpadding="1" cellspacing="0" border="0" width="100%" height="100%">
	        <tr>
	         <td>
	          <table cellpadding="1" cellspacing="0" border="0" width="100%" height="100%">
	           <tr><td class="label2"><b>Assessment Request Approval Date</b></td></tr>
	           <tr>
	            <td>
	             <table class="listtable1" cellpadding="1" cellspacing="0" border="1" width="50%" bordercolor="#2A4C7C">
			      <tr>
			       <td>
  	  	            <!-- date period -->
	  	            <table cellpadding="2" cellspacing="0" border="0" width="100%" height="100%">
	  	             <tr>
	  		          <td class="label2" width="32%"><b>Start Date</b></td>
  	  		          <td width="30%"><html:text styleClass="label2" name="reportForm" property="startDate" onkeyup="getKeyDate(event, this);" /></td>
	  		          <td width="38%">
						<a href="#" onClick="popUpWin('<%=contextPath%>/jsp/calendar.jsp?textbox=formApprovedApps.startDate',290,155);">
				            <img src="<%=contextPath%>/images/calendar.gif" height="17" width="19" border="0">
				        </a>
	  		          </td>
	  		         </tr>
	  		         <tr>
	  		          <td class="label2"><b>End Date</b></td>
  	  		          <td><html:text styleClass="label2" name="reportForm" property="endDate"  onkeyup="getKeyDate(event, this);"/></td>
	  		          <td>
						<a href="#" onClick="popUpWin('<%=contextPath%>/jsp/calendar.jsp?textbox=formApprovedApps.endDate',290,155);">
				            <img src="<%=contextPath%>/images/calendar.gif" height="17" width="19" border="0">
				        </a>	  		          
	  		          </td>
	  		         </tr>
	  	            </table>
			       </td>
	              </tr>
	             </table>
	            </td>
	           </tr>
               <tr><td>&nbsp;</td></tr>
	          </table>
   	         </td>  
	        </tr>
			<tr>
   	         <td>
	          <table cellpadding="1" cellspacing="0" border="0" width="100%" height="100%">
	           <tr><td class="label2"><b>Filters</b></td></tr>
	           <tr>
	            <td>
	             <table class="listtable1" cellpadding="1" cellspacing="0" border="1" width="50%" bordercolor="#2A4C7C">
			      <tr>
			       <td>
	  		        <!-- Requesting Party -->
	  		        <table cellpadding="2" cellspacing="0" border="0" width="100%" height="100%">
	  		         <tr>
	  		          <td class="label2" width="30%"><b>Requesting Party</b></td>
  	  		          <td width="70%">
  	  		           <ium:list className="label2" listBoxName="requestor" type="<%=IUMConstants.LIST_LOB%>" 
  	  		           	styleName="width:250" selectedItem="<%=reportForm.getRequestor()%>" />
  	  		          </td>
	  		         </tr>
	  		         <tr>
	  		          <td class="label2"><b>Approving Party</b></td>
  	  		          <td>
  	  		           <ium:list className="label2" listBoxName="approver" type="<%=IUMConstants.LIST_DEPARTMENT%>" 
  	  		           	styleName="width:250" selectedItem="<%=reportForm.getApprover()%>" />
  	  		          </td>  	  		          
	  		         </tr>
	  		        </table>	  				 
			       </td>
	              </tr>
	             </table>
	            </td>
	           </tr>
<!--               <tr><td>&nbsp;</td></tr>	     -->
	          </table>
   	         </td>
			</tr>
	        <tr>
	         <td colspan="2">
	          <table cellpadding="1" cellspacing="0" border="0" width="50%">
	           <tr>
	            <td>
		         	<input type="button" class="button1" value="Generate" style="width:100" onClick="generateReport();" />
		         	&nbsp;
	    	     	<input type="button" class="button1" value="Reset" style="width:100" onClick="gotoPage('reportForm', 'viewReports.do')"/>
	    	     </td>
	    	    </tr>
	    	   </table>
	         </td>
	        </tr>
	       </table> <!-- end of parameter grouping -->
   	      </td>
	     </tr>
	    </table> <!-- end of Report Parameters border -->
	   </td>
       <td width="15%">&nbsp;</td> <!-- minimizing horizontal scrolling -->
	  </tr>
     </table> <!-- end of Report Parameters container -->      
 	</td>
 </tr>
 <input type="hidden" name="pageNo" value="<%=reportForm.getPageNo()%>">
 <!-- report content --> 	
<%
   int i=0;
   String tr_class;
   String td_bgcolor;
   String msgline = "&nbsp;";

   if (displayPage != null) {
%>
 <tr>
  <td>
   <table border="0" cellpadding="2" cellspacing="1" bgcolor=#FFFFFF style="border: 1px solid #D1D1D1;width:85%;" class="listtable1">
	<tr class="label2">
		<td>Total No. of Applications Approved is <b><bean:write name="recCount" /></b></td>
<%
	if (displayPage.getList().size() > 0) {
%>
	   	<td colspan="2" class="label2" align="right">
    		<a href="#" onClick="javascript:showPrinterFriendly();">Printer Friendly</a>
    	</td>
<%
	}
%>
	</tr>
    <tr class="headerrow1">
		<td>Requesting Party</td>
	    <td>Approving Party</td>
    	<td>Assessment Request Number</td>
    </tr>
    <logic:iterate id="rpt" name="displayPage" property="list">  
<%
   if (i%2 == 0) {
   	tr_class = "row1";
    td_bgcolor = "#CECECE";
   }
   else {
    tr_class = "row2";
    td_bgcolor = "#EDEFF0";
   }
%>
  <tr class=<%=tr_class%>>
   <td width="43%"><b><bean:write name="rpt" property="lob" /></b></td>
   <td width="43%""><bean:write name="rpt" property="approvingDept" /></td>   
   <td width="15%"><bean:write name="rpt" property="refNo"/></td>
  </tr>                                       
<%i++;%>
  </logic:iterate>
  <tr>
<% 
	if (i==0) {
	  msgline = request.getParameter("msgnoresult") + " ";
	  msgline += request.getParameter("msgtryagain");
%>
		<td class='label2' align='center' colspan="3"><%=msgline%></td>
<%	} 
	else {
		int pageNumber = reportForm.getPageNo();
%>
       	<td class="headerrow4" colspan="11" width="100%" height="10" class="label2" valign="bottom" >
        <!-- don't display link for previous page if the current page is the first page -->					
        <% if (pageNumber > 1) { %>
        	<a href="#" onclick="javascript:rePaginate(1,'GenerateReportApprovedAppsAction.do');"><img src="images/pagnavfirst_off.gif" name="pagnavfirst" border="0" width="18" height="13"/></a>&nbsp;					
            <a href="#" onclick="javascript:rePaginate(<%=pageNumber-1%>,'GenerateReportApprovedAppsAction.do');"><img src="images/pagnavleft_off.gif" name="pagnavleft" border="0" width="18" height="13"/></a>
        <% } else {%>
        	<img src="images/pagnavfirst_on.gif" name="pagnavfirst" border="0" width="18" height="13"/>&nbsp;
            <img src="images/pagnavleft_on.gif" name="pagnavleft" border="0" width="18" height="13"/>
        <% } %>
        <% 
        int currLink = 1;
        int prevLink = 1;
        %>
        <logic:iterate id="navLinks" name="displayPage" property="pageNumbers">
	        <% currLink = ((Integer)navLinks).intValue(); %>
	        <% if (((Integer)navLinks).intValue() == pageNumber) { %>
	        	<b><bean:write name="navLinks"/></b>
	        <% } else { %>		
	        	<% if ((currLink > (prevLink+1))) { %>
	            	...
			<% } %>
	        	<a href="#" onclick="rePaginate('<bean:write name="navLinks"/>','GenerateReportApprovedAppsAction.do');" class="links2"><bean:write name="navLinks"/></a>
			<% } %>
        	<% prevLink = currLink; %>
        </logic:iterate>
		<!-- don't display link for next page if the current page is the last page -->
        <%  if(pageNumber < prevLink) {  %>
        	<a href="#" onClick="rePaginate('<%=pageNumber+1%>','GenerateReportApprovedAppsAction.do');" class=links1><img src="<%=contextPath%>/images/pagnavright_off.gif" name="pagnavright" border="0" width="18" height="13"/></a>&nbsp;
        	<a href="#" onClick="rePaginate('<%=prevLink%>','GenerateReportApprovedAppsAction.do');" class=links1><img src="<%=contextPath%>/images/pagnavlast_off.gif" width="18" height="13" border="0"></a>
        <%  } else {  %>
        	<img src="<%=contextPath%>/images/pagnavright_on.gif" width="18" height="13" border="0">&nbsp;
            <img src="<%=contextPath%>/images/pagnavlast_on.gif" width="18" height="13" border="0">
        <%  }  %>
        </td>
<%  }  %>
	</tr>
   </table>
  </td>
 </tr>
<%  } %>
</table>
</form>
	 	      
