<%@ page language="java" buffer="1024kb"%>
<!-- JAVA UTILS -->
<%@ page import="java.util.ResourceBundle"%>

<%-- <%@ taglib uri="/struts-bean.tld" prefix="bean" %> --%>
<%-- <%@ taglib uri="/struts-html.tld" prefix="html" %> --%>

<%String contextPath = request.getContextPath();%>
<script language="JavaScript">
if (parent.document.forms[0]) {
    //parent.window.location = "<%=contextPath%>/jsp/koReqMedLabDetail.jsp";
	parent.document.forms[0].action = 'viewKOReqMedLabDetail.do';
	parent.document.forms[0].submit();
}
</script>

<%
/* Andre Ceasar Dacanay - css enhancement - start */
String iumCss = "";
String iumColorB = "";
String iumColorO = "";
String companyCode = "";
try {
	ResourceBundle rbWMSSP = ResourceBundle.getBundle("dbconfig");
	iumCss = rbWMSSP.getString("IUM_CSS_VERSION");
	companyCode = iumCss;
	iumCss = "_"+iumCss;
} catch (Exception e){}

if(iumCss.equals("_CP")){
	iumColorB="#2A4C7C";
	iumColorO = "#FFCB00";
	companyCode="";
}else if(iumCss.equals("_GF")){
	iumColorB="#303A77";
	iumColorO = "#303A77";
	companyCode = companyCode+" ";
}else{
	iumColorB="#2A4C7C";
	iumColorO = "#FFCB00";
}

/* Andre Ceasar Dacanay - css enhancement - end */
%>

<%

  
  StateHandler sessionHandler = new StateHandler();
  UserData ud = sessionHandler.getUserData(request);
  UserProfileData profile = ud.getProfile();
  String userId = profile.getUserId();
  Roles userRoles = new Roles();
%>

<%
    // This routine queries the access rights of the users (RCRIVERA)
    // --- START ---
    Access access = new Access();

	HashMap userAccess = sessionHandler.getUserAccess(request);
    if(!access.hasAccess(userAccess,IUMConstants.PAGE_ASSESSMENT_DETAILS,IUMConstants.ACC_INQUIRE) ){
    	response.sendRedirect("noAccessPage.jsp");
    }

    String CREATE_ACCESS = "";
    String MAINTAIN_ACCESS = "";
    String DELETE_ACCESS = "";
    String EXECUTE_ACCESS = "";
    String CREATE_KO = "";
    String MAINTAIN_KO = "";
    String DELETE_KO = "";
    String EXECUTE_KO = "";

    boolean INQUIRE_REQUEST = true;
    boolean INQUIRE_CDS = true;
    boolean INQUIRE_KOREQRMT = true;
    boolean INQUIRE_UWASSESS= true;
    boolean INQUIRE_DOCTORNOTES = true;

    // CREATE ASSESSMENT REQUEST
    if (!access.hasAccess(userAccess,IUMConstants.PAGE_ASSESSMENT_DETAILS,IUMConstants.ACC_CREATE) ){
	  CREATE_ACCESS = "DISABLED";
    }
	// MAINTAIN ASSESSMENT REQUEST
    if (!access.hasAccess(userAccess,IUMConstants.PAGE_ASSESSMENT_DETAILS,IUMConstants.ACC_MAINTAIN) ){
	  MAINTAIN_ACCESS = "DISABLED";
    }
	// -- NOT APPLICABLE --
    if (!access.hasAccess(userAccess,IUMConstants.PAGE_ASSESSMENT_DETAILS,IUMConstants.ACC_DELETE) ){
	  DELETE_ACCESS = "DISABLED";
    }
	// UPDATE LOCATION
    if (!access.hasAccess(userAccess,IUMConstants.PAGE_ASSESSMENT_DETAILS,IUMConstants.ACC_EXECUTE) ){
	  EXECUTE_ACCESS = "DISABLED";
    }

	// CREATE REQUIREMENT
    if(!access.hasAccess(userAccess,IUMConstants.PAGE_REQUIREMENTS_KO,IUMConstants.ACC_CREATE) ){
	  CREATE_KO = "DISABLED";
    }
	// MAINTAIN REQUIREMENT
    if(!access.hasAccess(userAccess,IUMConstants.PAGE_REQUIREMENTS_KO,IUMConstants.ACC_MAINTAIN) ){
	  MAINTAIN_KO = "DISABLED";
    }
	// -- NOT APPLICABLE --
    if(!access.hasAccess(userAccess,IUMConstants.PAGE_REQUIREMENTS_KO,IUMConstants.ACC_DELETE) ){
	  DELETE_KO = "DISABLED";
    }
	// -- NOT APPLICABLE --
    if(!access.hasAccess(userAccess,IUMConstants.PAGE_REQUIREMENTS_KO,IUMConstants.ACC_EXECUTE) ){
	  EXECUTE_KO = "DISABLED";
    }

    if(!access.hasAccess(userAccess,IUMConstants.PAGE_REQUEST_DETAILS,IUMConstants.ACC_INQUIRE) ){
	  INQUIRE_REQUEST = false;
    }
    if(!access.hasAccess(userAccess,IUMConstants.PAGE_CDS,IUMConstants.ACC_INQUIRE) ){
	  INQUIRE_CDS = false;
    }
    if(!access.hasAccess(userAccess,IUMConstants.PAGE_REQUIREMENTS_KO,IUMConstants.ACC_INQUIRE) ){
	  INQUIRE_KOREQRMT = false;
    }
    if(!access.hasAccess(userAccess,IUMConstants.PAGE_UW_ASSESSMENT,IUMConstants.ACC_INQUIRE) ){
	  INQUIRE_UWASSESS = false;
    }
    if(!access.hasAccess(userAccess,IUMConstants.PAGE_DOCTORS_NOTES,IUMConstants.ACC_INQUIRE) ){
	  INQUIRE_DOCTORNOTES = false;
    }
    // --- END ---
 %>
<html>
  <head>
    <title><%=companyCode%>Sun Life Financial - Philippines</title>
    <link href="<%=contextPath%>/css/main<%=iumCss%>.css" rel="stylesheet" type="text/css">
    <script language="Javascript" src="<%=contextPath%>/js/navigation.js"></script>
    <script language="Javascript" src="<%=contextPath%>/js/validation.js"></script>
    <script language="Javascript" src="<%=contextPath%>/jsp/validate_req_RequestDetail.jsp"></script>
    <script language="Javascript" src="<%=contextPath%>/jsp/validate_req_KOReqMedLabDetail.jsp"></script>
    <script language="Javascript">
	var userId = "<%=userId%>";
    </script>
  </head>

  <%
  KOReqMedLabDetailForm actionForm = (KOReqMedLabDetailForm) session.getAttribute("detailForm");
  String lob = actionForm.getLob();
  String transmitInd = actionForm.getTransmitIndicator();
  String autoSettleInd = actionForm.getAutoSettleIndicator();
  String clientToSearch = actionForm.getClientToSearch();


  //current status
  String currentStatus = request.getParameter("currentStatus");
  if (currentStatus == null) {
    currentStatus = actionForm.getRequestStatus();
  }
  String isSuccessChangeStatus = request.getParameter("isSuccessChangeStatus");
  if ((isSuccessChangeStatus != null) && (isSuccessChangeStatus.equals("true"))) {
    currentStatus = actionForm.getRequestStatus();
  }

  //selected status
  String selectedStatus = request.getParameter("requestStatus");
  if (selectedStatus == null) {
    selectedStatus = actionForm.getRequestStatus();
  }

    //current assigned to
  String currentAssignedTo = actionForm.getAssignedTo();
  if (currentAssignedTo == null || (currentAssignedTo != null && currentAssignedTo.equals(""))) {
    currentAssignedTo = request.getParameter("currentAssignedTo");
  }

  //disable buttons (except create assessment request) if current status is an end status
  AssessmentRequest ar = new AssessmentRequest();
  String FORWARD_EXECUTE_ACCESS = EXECUTE_ACCESS;
  if (ar.isEndStatus(lob, Long.parseLong(currentStatus))) {
    MAINTAIN_ACCESS = "DISABLED";
    EXECUTE_ACCESS = "DISABLED";
    CREATE_KO = "DISABLED";
    MAINTAIN_KO = "DISABLED";
  }

  boolean isMaintain = false;
  String reqIsMaintain = request.getParameter("isMaintain");
  if ((reqIsMaintain != null) && (reqIsMaintain.equals("true"))) {
    isMaintain = true;
  }

  //filter for request status
  String lob_status = lob + "," + currentStatus;
  %>
  
  <body leftmargin="0" topmargin="0" onLoad="<% if (isMaintain) { %> enableRequestDetails(); enableTabDetails('ko'); setDateForwarded(); hideSaveCancel(); hideMaintainCancel(); showCreateMaintain();initializeClientData('<%=clientToSearch%>'); <% } else { %> disableRequestDetails(); disableTabDetails('ko'); disableRequirementFields('<%=lob%>'); hideSaveCancel(); hideMaintainCancel(); showCreateMaintain(); changeType('<%=lob%>'); <% } %> 
  	<%
  		//WMS session expiration = close browser
		ResourceBundle rb1 = ResourceBundle.getBundle("wmsconfig");
		String wmsContextPath1 = rb1.getString("wms_context_path");
		String HEADER_WIDTH = "";
		String TABLE_WIDTH = "";
		String TABLE_TD = "";
		String FRAME_WIDTH = "";
		String COMMENTBOX_WIDTH = "";
		String GRAY_WIDTH = "";
		String BUTTONSPACE_WIDTH = "";
		String DATE_MAX_WIDTH = "";
		String LABEL_CLASS = "";
		String DIV_WIDTH = "";
		
       	if(contextPath.equalsIgnoreCase(wmsContextPath1)){ 
       		HEADER_WIDTH = "740";
       		TABLE_WIDTH = "740";
       		FRAME_WIDTH ="740";
       		TABLE_TD = "0";
       		COMMENTBOX_WIDTH = "90";
       		GRAY_WIDTH = "780";
       		BUTTONSPACE_WIDTH = "180";
       		DATE_MAX_WIDTH = "width:70";
       	    LABEL_CLASS = "label7";
       	    DIV_WIDTH = "700";
  		}
  		else{
  			HEADER_WIDTH = "100%";
  			TABLE_WIDTH = "100%";
  			FRAME_WIDTH ="1000";
       		TABLE_TD = "15";
       		COMMENTBOX_WIDTH = "130";
       		GRAY_WIDTH = "100%";
       		BUTTONSPACE_WIDTH = "300";
       		DATE_MAX_WIDTH = "width:100";
       		LABEL_CLASS = "label2";
       		DIV_WIDTH = "975";
  		}%>" onUnload="closePopUpWin();">
    <table border="0" cellpadding="0" cellspacing="0" height="100%" width="<%=HEADER_WIDTH%>">
      <tr>
        <td width="<%=HEADER_WIDTH%>" colspan="2">
          <jsp:include page="header.jsp" flush="true"/>
        </td>
      </tr>
      <tr>
        <td class="label2" rowspan="2" bgcolor="<%iumColorO%>" height="100%" width="20"><img src="<%=contextPath%>/images/spacer.gif" width="20"></td>
        <td class="label2" background="<%=contextPath%>/images/GradientGray.jpg" height="27" width="<%=GRAY_WIDTH%>" valign="top">&nbsp;&nbsp;
          <span class="main"><%=companyCode%>Integrated Underwriting and Medical System</span>
        </td>
      </tr>
      <tr>
        <form name="requestForm" method="post">
          <td height="100%">
          &nbsp;&nbsp;<html:errors/>
          <!-- Status List START -->
          <table border="0" cellpadding="5" cellspacing="0"  width="<%=TABLE_WIDTH%>">
            <tr>
              <input type="hidden" name="updateType" value="<%=IUMConstants.PROCESS_TYPE_SINGLE%>">
                <% if ((actionForm.getSourceSystem() != null) && (transmitInd != null && transmitInd.equals(IUMConstants.INDICATOR_FAILED))) {%>
                <td class="label2" align="left"><input class="button1" type="button" name="btnRetrieveCDS" value="Retrieve CDS" onClick="retrieveCDS();" <%=MAINTAIN_ACCESS%>></td>
                <% } else {%>
               <td width="<%=BUTTONSPACE_WIDTH%>">&nbsp;</td>
                <% }%>
                
                <td width="<%=BUTTONSPACE_WIDTH%>">&nbsp;</td>
                
				<% if ((Long.parseLong(currentStatus) == IUMConstants.STATUS_APPROVED) ||
                       (Long.parseLong(currentStatus) == IUMConstants.STATUS_AR_CANCELLED) ||
                       (Long.parseLong(currentStatus) == IUMConstants.STATUS_DECLINED) ||
					   (Long.parseLong(currentStatus) == IUMConstants.STATUS_FOR_OFFER) ||
                       (Long.parseLong(currentStatus) == IUMConstants.STATUS_NOT_PROCEEDED_WITH)) { %>
                <td class="label2" align="left"><input class="button1" type="button" name="btnForward" value="Forward to Records" onClick="forwardToRecords();"  <%=FORWARD_EXECUTE_ACCESS%>></td>
                <% } else {%>
                <td width="<%=BUTTONSPACE_WIDTH%>">&nbsp;</td>
				<%}%>
            </tr>
            
            <% if (autoSettleInd != null) {
            	  String autoSettleMsg = "";
                  if (autoSettleInd.equals(IUMConstants.INDICATOR_SUCCESS)) {
					autoSettleMsg = "Auto-settle Successful";
                  } else if (autoSettleInd.equals(IUMConstants.INDICATOR_FAILED)){
                	autoSettleMsg = "Auto-settle Not Successful:";
                  }
            %>
            <tr><td class="error"><%=autoSettleMsg%>&nbsp;<bean:write name="detailForm" property="autoSettleRemarks"/></td></tr>
			<% } %>
          </table>
          <!-- Status List END -->

          <!-- hidden field to store current status -->
          <input type="hidden" name="currentStatus" value="<%=currentStatus%>">
          <!-- hidden field to store remarks, UW analysis, UW remarks -->
          <input type="hidden" name="remarks" value="<bean:write name="detailForm" property="remarks"/>">
          <input type="hidden" name="UWAnalysis" value="<bean:write name="detailForm" property="UWAnalysis"/>">
          <input type="hidden" name="UWRemarks" value="<bean:write name="detailForm" property="UWRemarks"/>">
          <!-- hidden field to store maintain flag for request details -->
          <input type="hidden" name="isMaintain" value="<%=isMaintain%>">
          <!-- hidden field to store success flag for change status in request details -->
          <input type="hidden" name="isSuccessChangeStatus" value="false">
          <!-- hidden field to store the currently assigned to user -->
          <input type="hidden" name="currentAssignedTo" value="<%=currentAssignedTo%>">
          <!-- hidden field to store the tab/page of the request detail -->
          <input type="hidden" name="reqPage" value="koReqMedLabDetail">

          <table border="0" cellpadding="10" cellspacing="0" height="100%" width="<%=TABLE_WIDTH%>">
            <tr>
              <td>
                <%
                String _sourceSystem = actionForm.getSourceSystem();
                if (_sourceSystem == null) {
                  _sourceSystem = "";
                }

                String _agentCode = actionForm.getAgentCode();
                if (_agentCode == null) {
                  _agentCode = "";
                }

                String _lob = actionForm.getLob();
                if (_lob == null) {
                  _lob = "";
                }

                String _branchCode = actionForm.getBranchCode();
                if (_branchCode == null) {
                  _branchCode = "";
                }

                String _assignedTo = currentAssignedTo;
                if (_assignedTo == null) {
                  _assignedTo = "";
                }

                String _location = actionForm.getLocation();
                if (_location == null) {
                  _location = "";
                }

                String _underwriter = actionForm.getUnderwriter();
                if (_underwriter == null) {
                  _underwriter = "";
                }

                String _insuredClientNo = actionForm.getInsuredClientNo();
                if (_insuredClientNo == null) {
                  _insuredClientNo = "";
                }

                String _insuredSex = actionForm.getInsuredSex();
                if (_insuredSex == null) {
                  _insuredSex = "";
                }

                String _ownerClientNo = actionForm.getOwnerClientNo();
                if (_ownerClientNo == null) {
                  _ownerClientNo = "";
                }

                String _ownerSex = actionForm.getOwnerSex();
                if (_ownerSex == null) {
                  _ownerSex = "";
                }

                String _status = currentStatus;
                if (_status == null) {
                  _status = "";
                }

                String _selectedStatus = selectedStatus;
                if (_selectedStatus == null) {
                  _selectedStatus = "";
                }
                %>
                
                <jsp:include page="requestDetail_include.jsp" flush="true">
                  <jsp:param name="sourceSystem" value = "<%=_sourceSystem%>"/>
                  <jsp:param name="agentCode" value = "<%=_agentCode%>"/>
                  <jsp:param name="lob" value = "<%=_lob%>"/>
                  <jsp:param name="branchCode" value = "<%=_branchCode%>"/>
                  <jsp:param name="assignedTo" value = "<%=_assignedTo%>"/>
                  <jsp:param name="location" value = "<%=_location%>"/>
                  <jsp:param name="underwriter" value = "<%=_underwriter%>"/>
                  <jsp:param name="insuredClientNo" value = "<%=_insuredClientNo%>"/>
                  <jsp:param name="insuredSex" value = "<%=_insuredSex%>"/>
                  <jsp:param name="ownerClientNo" value = "<%=_ownerClientNo%>"/>
                  <jsp:param name="ownerSex" value = "<%=_ownerSex%>"/>
                  <jsp:param name="status" value = "<%=_status%>"/>
                  <jsp:param name="selectedStatus" value = "<%=_selectedStatus%>"/>
                </jsp:include>
              </td>
            </tr>
        </form>
        <!-- end of request detail form -->
            <tr>
              <td width="100%" height="100%" valign="top">
                <table cellpadding="1" cellspacing="0">
                  <tr>
                 
                    <% if (INQUIRE_CDS) {%>
                    <td bgcolor="#BBBBBB" style="cursor:hand" onmouseover="this.style.backgroundColor = '<%=iumColorB%>'" onmouseout="this.style.backgroundColor = '#BBBBBB'">
                      <table border="0" cellpadding="3" cellspacing="0">
                        <tr><a href="javascript:gotoPage('requestForm', 'viewCDSDetail.do');"><td class="tabLink"><b>CDS Details-KO & Med/Lab and UW Assessment</b></td></a></tr>
                      </table>
                    </td>
                    <td></td>
                    <% }%>
                    <td bgcolor="<%=iumColorB%>">
                      <table border="0" cellpadding="3" cellspacing="0">
                        <tr><td class="tabSelected"><b>Requirements</b></td></tr>
                      </table>
                    </td>
                    <td></td>
                 
                    <% if (INQUIRE_DOCTORNOTES) {%>
                    <td bgcolor="#BBBBBB" style="cursor:hand" onmouseover="this.style.backgroundColor = '<%=iumColorB%>'" onmouseout="this.style.backgroundColor = '#BBBBBB'">
                      <table border="0" cellpadding="3" cellspacing="0">
                        <tr><a href="javascript:gotoPage('requestForm', 'viewDoctorsNotes.do');"><td class="tabLink"><b>Doctor's Notes</b></td></a></tr>
                      </table>
                    </td>
                    <td></td>
                    <%}%>
                  </tr>
                </table>
                <!-- Body -->
                <table cellpadding="0" cellspacing="0" border="1" bordercolor="<%=iumColorB%>" width="<%=TABLE_WIDTH%>">
                  <tr>
                    <td class="label2" bgcolor="<%=iumColorB%>" colspan="11">&nbsp;</td>
                  </tr>
                  <tr>
                    <td>
                      <table cellpadding="10" cellspacing="0" border="0" width="100%">
                        <tr>
                          <td class="label2" valign="top">
                            <b>LIST OF REQUIREMENTS</b><br>
                            <html:messages message="true" id="warning"><span class="error"><bean:write name="warning"/></span></html:messages><br>
							<div id=listOfRequirements style="width: <%=DIV_WIDTH%>px;height: 325px;">
								<jsp:include page="listOfRequirements.jsp" flush="true">
									<jsp:param name="lob" value = "<%=_lob%>"/>
									<jsp:param name="branchId" value = "<%=_branchCode%>"/>
									<jsp:param name="agentCode" value = "<%=_agentCode%>"/>
									<jsp:param name="assignedTo" value = "<%=_assignedTo%>"/>
									<jsp:param name="currentStatus" value = "<%=_status%>"/>
								</jsp:include>
							</div>
                          </td>
                        </tr>
                        <tr>
                          <td class="label2" valign="top">
                            <a name="requirementDetail"></a>
                            <form name="requirementForm" method="post">
                            <input type="hidden" name="refNo" value="<%=actionForm.getRefNo()%>">
                            <!-- for requirementForm -->
                            <%
                            RequirementForm req = (RequirementForm) request.getAttribute("requirementForm");
                            if (req == null) req = (RequirementForm) session.getAttribute("requirementForm");
                            %>
                            <input type="hidden" name="refNoReq" value="<%=req.getRefNo()%>">		<!-- for requirementForm -->
                            <table border="0" cellpadding="0" cellspacing="0" width="<%=TABLE_WIDTH%>">
                              <tr>
                                 <td class="label2"><b>DETAILS OF REQUIREMENT</b></td>
                              </tr>
                              <tr>
                            <input type="hidden" name="reqId" value="<%=req.getReqId() %>"> 		<!-- for requirementForm -->
                            <input type="hidden" name="mode">  						                <!-- for requirementForm -->
                                <td height="100%">
                                  <table border="1" bordercolor="<%=iumColorB%>" cellpadding="5" cellspacing="0" width="<%=TABLE_WIDTH%>">
                                    <tr>
                                      <td height="100%">
                                        <%
                                        // requirement level
                                        String level_Ins = "";
                                        String level_Own = "";
                                        String reqLevel = req.getReqLevel();
                                        String reqLevelDisplay = "";
                                        if (reqLevel != null) {
                                          if (reqLevel.equals("P")) {
                                            reqLevelDisplay = "Policy";
                                          }
                                          else if (reqLevel.equals("C")) {
                                            reqLevelDisplay = "Client";
                                          }
                                        }
                                        else {
                                          reqLevel = "";
                                        }
                                        if (req.getClientType() != null) {
                                           if (req.getClientType().equals("I")) {
                                               level_Ins = "checked";
                                           }
                                           else if (req.getClientType().equals("O")) {
                                               level_Own = "checked";
                                           }
                                        }
                                        // resolve
                                        String resolve = req.getResolve();
                                        String resolveChecked_Yes = "";
                                        String resolveChecked_No = "";
                                        if (resolve != null) {
                                          if (resolve.equals("true")) {
                                            resolveChecked_Yes = "checked";
                                          }
                                          else if (resolve.equals("false")) {
                                            resolveChecked_No = "checked";
                                          }
                                        }

                                        // paid indicator
                                        String paid = req.getPaidInd();
                                        String paidChecked_Yes = "";
                                        String paidChecked_No = "";
                                        if (paid != null) {
                                          if (paid.equals("true")) {
                                            paidChecked_Yes = "checked";
                                          }
                                          else if (paid.equals("false")) {
                                            paidChecked_No = "checked";
                                          }
                                        }

                                        // new test only
                                        String newTest = req.getNewTest();
                                        String newTestChecked_Yes = "";
                                        String newTestChecked_No = "";
                                        if (newTest != null) {
                                          if (newTest.equals("Y")) {
                                            newTestChecked_Yes = "checked";
                                          }
                                          else {
                                            newTestChecked_No = "checked";
                                          }
                                        }

                                        // auto order
                                        String autoOrder = req.getAutoOrder();
                                        String autoOrderChecked_Yes = "";
                                        String autoOrderChecked_No = "";
                                        if (autoOrder != null) {
                                          if (autoOrder.equals("Y")) {
                                            autoOrderChecked_Yes = "checked";
                                          }
                                          else if (autoOrder.equals("N")) {
                                            autoOrderChecked_No = "checked";
                                          }
                                        }

                                        // fld comm
                                        String fld = req.getFldComm();
                                        String fldChecked_Yes = "";
                                        String fldChecked_No = "";
                                        if (fld != null) {
                                          if (fld.equals("Y")) {
                                            fldChecked_Yes = "checked";
                                          }
                                          else if (fld.equals("N")) {
                                            fldChecked_No = "checked";
                                          }
                                        }

                                        // complete Requirement
                                        String completeReq = req.getCompleteReq();
                                        String compReqChecked = "";
                                        String compReqValue = "N";
                                        if (completeReq != null) {
                                          if (completeReq.equals("true")) {
                                            compReqChecked = "checked";
                                            compReqValue = "Y";
                                          }
                                        }

                                        %>
                                        <div id="divname" style="height:325;overflow:auto;">
                                        <table border="0" cellpadding="2" cellspacing="0" width="<%=TABLE_WIDTH%>">
                                          <tr>
                                            <td class="<%=LABEL_CLASS%>"><b>Seq</b></td>
                                            <td class="<%=LABEL_CLASS%>"><input type="text" name="seqDisplay" class="<%=LABEL_CLASS%>" style="width:100" value="<bean:write name="requirementForm" property="seq"/>"></td>
                                            <input type="hidden" name="seq" value="<bean:write name="requirementForm" property="seq"/>">
                                            <td class="<%=LABEL_CLASS%>" width="<%=TABLE_TD%>">&nbsp;</td>
                                            <td class="<%=LABEL_CLASS%>"><span class="required">*</span><b>Code</b></td>
                                            <td class="<%=LABEL_CLASS%>">
                                            <%
                                            ArrayList levelList = new ArrayList();
                                            levelList = actionForm.getReqLevelList();
                                            Iterator it = levelList.iterator();
                                            %>
                                            <select name="reqCode" onChange="changeLevelAndType();" class="<%=LABEL_CLASS%>">
                                            <%
                                            int cntCode=0;
                                            while (it.hasNext())
                                            {
                                            	RequirementData data = (RequirementData) it.next();
		                                        if (data != null)
                	                            {
                                            %>
                    	                    <%		if (data.getReqtCode()!=IUMConstants.REQ_CODE_RECD1)//if (!data.getReqtCode().equals(IUMConstants.REQ_CODE_RECD1))
                        	                    	{
                                           				if (req.getReqCode() != null)
                                            			{
                                                 			if (req.getReqCode().equals(data.getReqtCode()))
                                            				{
		                                    %>
                 				                            	<option value="<%=data.getLevel()%>-<%=data.getClientType()%>-<%=data.getFollowUpNum()%>" selected><%= data.getReqtCode()%></option>
                                            <%       		}
                                            				else
                                            				{
                                            %>
        			                                         	<option value="<%=data.getLevel()%>-<%=data.getClientType()%>-<%=data.getFollowUpNum()%>"><%= data.getReqtCode()%></option>
                                            <%      		} %>
                                            <% 			}
                                            			else
                                            			{ %>
                                            <%   			if (cntCode == 0)
                                            				{
                                            %>
                                            				 	<option value="-" selected>Select Code</option>
				                                            	<option value="<%=data.getLevel()%>-<%=data.getClientType()%>-<%=data.getFollowUpNum()%>"><%= data.getReqtCode()%></option>
                                            <%   			}
                                            				else
                                            				{
                                            %>
                                                              	<option value="<%=data.getLevel()%>-<%=data.getClientType()%>-<%=data.getFollowUpNum()%>"><%= data.getReqtCode()%></option>
                                            <%  			} %>
                                            <% 			} %>

                                            <%		}
                                            		else
                                            		{
                                            %>
                                               			<option value="<%=data.getLevel()%>-<%=data.getClientType()%>-<%=data.getFollowUpNum()%>" selected><%= data.getReqtCode()%></option>
                                            <%		}
                                                	cntCode++;
                                            	}
                                            }
                                            %>
                                            </select>
                                            </td>
                                            <input type="hidden" name="reqCodeValue" value="<%= req.getReqCode()%>">
                                            <td class="<%=LABEL_CLASS%>" width="<%=TABLE_TD%>">&nbsp;</td>
                                            <td class="<%=LABEL_CLASS%>" colspan="2"><b>Level</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                              <%--<input type="radio" name="reqLevel" value="P" <%=level_Policy%> onClick="changeClientType();">Policy
                                              <input type="radio" name="reqLevel" value="C" <%=level_Client%> onClick="changeClientType();">Client--%>
                                              <input type="text"   class="noTextbox" name="reqLevelDisplay" value="<%=reqLevelDisplay%>" size="6" maxLength="6" readonly>
                                              <input type="hidden" name="reqLevel" value="<%=reqLevel%>">
                                            </td>
                                          </tr>
                                          <tr>
                                            <td class="<%=LABEL_CLASS%>"><b>Description</b></td>
                                            <td class="<%=LABEL_CLASS%>" colspan="8"><input type="text" name="reqDesc" class="noTextbox" style="width:585" value="<bean:write name="requirementForm" property="reqDesc"/>" maxlength="30" readonly></td>
                                          </tr>
                                          <tr>
                                            <td class="<%=LABEL_CLASS%>"><b>Status</b></td>
                                            <td class="<%=LABEL_CLASS%>"><input type="text" name="reqStatusCode" value="<%=req.getReqStatusCode()%>" class="noTextbox" style="width=150px" readonly></td>
                                            <input type="hidden" name="reqStatusValue" value="<%=req.getReqStatusValue()%>"/>
                                            <td class="<%=LABEL_CLASS%>" width="<%=TABLE_TD%>">&nbsp;</td>
                                            <td class="<%=LABEL_CLASS%>"><span class="required">*</span><b>Client Type</b></td>
                                            <td class="<%=LABEL_CLASS%>">
                                            <% if (lob.equals(IUMConstants.LOB_INDIVIDUAL_LIFE)) { %>
                                            <input type="radio" name="clientTypeDisplay" id="clientTypeDisplay1"  value="<%= IUMConstants.CLIENT_TYPE_INSURED %>" <%=level_Ins%> onClick="changeClientType();">Insured
                                            <input type="radio" name="clientTypeDisplay" value="<%= IUMConstants.CLIENT_TYPE_OWNER %>" <%=level_Own%> onClick="changeClientType();">Owner
                                            <input type="hidden" name="clientType" value="">
                                            <% } else if (lob.equals(IUMConstants.LOB_PRE_NEED)) { %>
                                            <input type="text" name="clientTypeDisplay" class="<%=LABEL_CLASS%>" size="2" value="Planholder" style="width:100"></td>
                                            <input type="hidden" name="clientType" value="<%= IUMConstants.CLIENT_TYPE_PLANHOLDER %>">
                                            <% } else { %>
                                            <input type="text" name="clientTypeDisplay" class="<%=LABEL_CLASS%>" size="2" value="Member" style="width:100"></td>
                                            <input type="hidden" name="clientType" value="<%= IUMConstants.CLIENT_TYPE_MEMBER %>">
                                            <% } %>
                                            <td class="<%=LABEL_CLASS%>" width="<%=TABLE_TD%>">&nbsp;</td>
                                            <td class="<%=LABEL_CLASS%>"><b>Completed Requirement?</b></td>
                                            <td class="<%=LABEL_CLASS%>"><input type="checkbox" name="completeReq" class="<%=LABEL_CLASS%>" value="<%=compReqValue%>" <%=compReqChecked%>></td>
                                          </tr>
                                          <tr>
                                            <td class="<%=LABEL_CLASS%>"><b>Status Date</b></td>
                                            <td class="<%=LABEL_CLASS%>"><input type="text" name="statusDate" class="noTextbox" style="width:100" value="<bean:write name="requirementForm" property="statusDate"/>" maxlenght="9" readonly></td>
                                            <td class="<%=LABEL_CLASS%>" width="<%=TABLE_TD%>">&nbsp;</td>
                                            <td class="<%=LABEL_CLASS%>"><b>CCAS Suggest</b></td>
                                            <td class="<%=LABEL_CLASS%>"><input type="checkbox" name="ccasSuggest" class="<%=LABEL_CLASS%>" value=""></td>
                                            <td class="<%=LABEL_CLASS%>" width="<%=TABLE_TD%>">&nbsp;</td>
                                            <td class="<%=LABEL_CLASS%>"><b>Designation</b></td>
                                            <td class="<%=LABEL_CLASS%>"><input type="text" name="designation" class="<%=LABEL_CLASS%>" style="<%=DATE_MAX_WIDTH%>" value="<bean:write name="requirementForm" property="designation"/>" maxlength="10"></td>
                                          </tr>
                                          <tr>
                                            <td class="<%=LABEL_CLASS%>"><b>Updated By</b></td>
                                            <td class="<%=LABEL_CLASS%>"><input type="text" name="updatedBy" class="noTextbox" style="width:100" value="<bean:write name="requirementForm" property="updatedBy"/>" maxlength="10" readonly></td>
                                            <td class="<%=LABEL_CLASS%>" width="<%=TABLE_TD%>">&nbsp;</td>
                                            <td class="<%=LABEL_CLASS%>"><b>Updated Date</b></td>
                                            <td class="<%=LABEL_CLASS%>"><input type="text" name="updatedDate" class="noTextbox" style="width:100" value="<bean:write name="requirementForm" property="updatedDate"/>"  maxlength="9" readonly></td>
                                            <td class="<%=LABEL_CLASS%>" width="<%=TABLE_TD%>">&nbsp;</td>
                                            <td class="<%=LABEL_CLASS%>"><b>Validity Date</b></td>
                                            <td class="<%=LABEL_CLASS%>"><input type="text" name="validityDate" class="<%=LABEL_CLASS%>" style="<%=DATE_MAX_WIDTH%>" value="<bean:write name="requirementForm" property="validityDate"/>" onKeyUp="getKeyDate(event,this);" maxlength="9">&nbsp;
                                            <div id="doc_cal2" style="display:none;position:absolute;"><a href="#" onClick="popUpWin('<%=contextPath%>/jsp/calendar.jsp?textbox=requirementForm.validityDate',290,155);"><img src="<%=contextPath%>/images/calendar.gif" height="17" width="19" border="0">
											</a></div>
											</td>
                                          </tr>
                                          <tr>
                                            <td class="<%=LABEL_CLASS%>"><b>Test Date</b></td>
                                            <td class="<%=LABEL_CLASS%>"><input type="text" name="testDate" class="<%=LABEL_CLASS%>" style="<%=DATE_MAX_WIDTH%>" value="<bean:write name="requirementForm" property="testDate"/>" onKeyUp="getKeyDate(event,this);" maxlength="9" >&nbsp;
                                            <div id="doc_cal1" style="display:none;position:absolute;"><a href="#" onClick="popUpWin('<%=contextPath%>/jsp/calendar.jsp?textbox=requirementForm.testDate',290,155);"><img src="<%=contextPath%>/images/calendar.gif" height="17" width="19" border="0">
											</a></div>
                                            </td>
                                            <td class="<%=LABEL_CLASS%>" width="<%=TABLE_TD%>">&nbsp;</td>
                                            <td class="<%=LABEL_CLASS%>"><b>Test Result</b></td>
                                            <td class="<%=LABEL_CLASS%>"><input type="text" name="testResult" class="noTextbox" style="width:100" value="<bean:write name="requirementForm" property="testResult"/>" maxlength="4" readonly></td>
                                            <td class="<%=LABEL_CLASS%>" width="<%=TABLE_TD%>">&nbsp;</td>
                                            <td class="<%=LABEL_CLASS%>"><b>Order Date</b></td>
                                            <td class="<%=LABEL_CLASS%>"><input type="text" name="orderDate" class="noTextbox" style="width:100" value="<bean:write name="requirementForm" property="orderDate"/>" maxlength="9" readonly></td>
                                          </tr>
                                          <tr>
                                            <td class="<%=LABEL_CLASS%>"><b>Description</b></td>
                                            <td class="<%=LABEL_CLASS%>" colspan="8"><input type="text" class="noTextbox" name="testDesc" style="width:585" maxLength="40" value="<bean:write name="requirementForm" property="testDesc"/>" readonly></td>
                                          </tr>
                                          <tr>
                                            <td class="<%=LABEL_CLASS%>"><b>Resolve</b></td>
                                            <td class="<%=LABEL_CLASS%>">
                                              <input type="radio" name="resolve" class="<%=LABEL_CLASS%>" value="Y" <%=resolveChecked_Yes%>>Yes
                                              <input type="radio" name="resolve" class="<%=LABEL_CLASS%>" value="N" <%=resolveChecked_No%>>No
                                            </td>
                                            <td class="<%=LABEL_CLASS%>" width="<%=TABLE_TD%>">&nbsp;</td>
                                            <td class="<%=LABEL_CLASS%>"><span class="required">*</span><b>Follow Up No.</b></td>
                                            <td class="<%=LABEL_CLASS%>">
                                            <input type="text" name="followUpNo" class="<%=LABEL_CLASS%>" style="width:100" value="<bean:write name="requirementForm" property="followUpNo"/>" maxLength="1">
                                            </td>
                                            <td class="<%=LABEL_CLASS%>" width="<%=TABLE_TD%>">&nbsp;</td>
                                            <td class="<%=LABEL_CLASS%>"><b>Follow Up Date</b></td>
                                            <td class="<%=LABEL_CLASS%>"><input type="text" name="followUpDate" class="<%=LABEL_CLASS%>" style="<%=DATE_MAX_WIDTH%>" value="<bean:write name="requirementForm" property="followUpDate"/>" onKeyUp="getKeyDate(event,this);" maxlength="9">
                                            <div id="doc_cal3" style="display:none;position:absolute;"><a href="#" onClick="popUpWin('<%=contextPath%>/jsp/calendar.jsp?textbox=requirementForm.followUpDate',290,155);"><img src="<%=contextPath%>/images/calendar.gif" height="17" width="19" border="0">
											</a></div>
											</td>
                                          </tr>
                                          <tr>
                                            <td class="<%=LABEL_CLASS%>"><b>Date Sent</b></td>
                                            <td class="<%=LABEL_CLASS%>"><input type="text" name="dateSent" class="noTextbox" style="width:100" value="<bean:write name="requirementForm" property="dateSent"/>" onKeyUp="getKeyDate(event,this);" maxlength="9" readonly></td>
                                            <td class="<%=LABEL_CLASS%>" width="<%=TABLE_TD%>">&nbsp;</td>
                                            <td class="<%=LABEL_CLASS%>"><b>Paid Indicator</b></td>
                                            <td class="<%=LABEL_CLASS%>">
                                              <input type="radio" name="paidInd" class="<%=LABEL_CLASS%>" value="Y" <%=paidChecked_Yes%>>Yes
                                              <input type="radio" name="paidInd" class="<%=LABEL_CLASS%>" value="N" <%=paidChecked_No%>>No
                                            </td>
                                            <td class="<%=LABEL_CLASS%>" width="<%=TABLE_TD%>">&nbsp;</td>
                                            <td class="<%=LABEL_CLASS%>"><b>New Tests Only</b></td>
                                            <td class="<%=LABEL_CLASS%>">
                                              <input type="radio" name="newTest" class="<%=LABEL_CLASS%>" value="Y" <%=newTestChecked_Yes%>>Yes
                                              <input type="radio" name="newTest" class="<%=LABEL_CLASS%>" value="N" <%=newTestChecked_No%>>No
                                            </td>
                                          </tr>
                                          <tr>
                                            <td class="<%=LABEL_CLASS%>"><b>Received Date</b></td>
                                            <td class="<%=LABEL_CLASS%>"><input type="text" name="receivedDate" class="noTextbox" style="width:100" value="<bean:write name="requirementForm" property="receivedDate"/>" onKeyUp="getKeyDate(event,this);" maxlength="9" readonly></td>
                                            <td class="<%=LABEL_CLASS%>" width="<%=TABLE_TD%>">&nbsp;</td>
                                            <td class="<%=LABEL_CLASS%>"><b>Auto Order</b></td>
                                            <td class="<%=LABEL_CLASS%>">
                                              <input type="radio" name="autoOrder" class="<%=LABEL_CLASS%>" value="Y" <%=autoOrderChecked_Yes%>>Yes
                                              <input type="radio" name="autoOrder" class="<%=LABEL_CLASS%>" value="N" <%=autoOrderChecked_No%>>No
                                            </td>
                                            <td class="<%=LABEL_CLASS%>" width="<%=TABLE_TD%>">&nbsp;</td>
                                            <td class="<%=LABEL_CLASS%>"><b>FLD Comm</b></td>
                                            <td class="<%=LABEL_CLASS%>">
                                              <input type="radio" name="fldComm" class="<%=LABEL_CLASS%>" value="Y" <%=fldChecked_Yes%>>Yes
                                              <input type="radio" name="fldComm" class="<%=LABEL_CLASS%>" value="N" <%=fldChecked_No%>>No
                                            </td>
                                          </tr>
                                          <tr>
                                            <td class="<%=LABEL_CLASS%>" valign="top"><b>Comment</b></td>                                  	
                                        	<% String statValue = req.getReqStatusValue(); %>          	
                                  		<% if (Long.parseLong(statValue) == IUMConstants.STATUS_NTO || Long.parseLong(statValue) == IUMConstants.STATUS_SCC) { %>          
                                            		<td class="<%=LABEL_CLASS%>" colspan="8"><textarea class="<%=LABEL_CLASS%>" name="comment" cols="<%=COMMENTBOX_WIDTH%>" READONLY rows="2" ><bean:write name="requirementForm" property="comment"/></textarea></td>
										<% } else { %>
													<td class="<%=LABEL_CLASS%>" colspan="8"><textarea class="<%=LABEL_CLASS%>" name="comment" cols="<%=COMMENTBOX_WIDTH%>" rows="2" ><bean:write name="requirementForm" property="comment"/></textarea></td>
										<% } %>
                                          </tr>
                                        </table>
                                        </div>
                                      </td>
                                    </tr>
                                  </table>
                                </td>
                              </tr>
                              <tr>
                                <td class="<%=LABEL_CLASS%>" align="right" colspan="9">&nbsp;</td>
                              </tr>
                              <tr>
                                <%--<% if (! (Long.parseLong(currentStatus) == IUMConstants.STATUS_AR_CANCELLED)) { %>
                                   <% if ( (userRoles.getRoles(ud.getRoles(),IUMConstants.ROLES_NB_STAFF) && lob.equals(IUMConstants.LOB_INDIVIDUAL_LIFE)) ||
                                           ( (lob.equals(IUMConstants.LOB_INDIVIDUAL_LIFE)) &&
                                             ((Long.parseLong(currentStatus) != IUMConstants.STATUS_UNDERGOING_ASSESSMENT) &&
                                              (Long.parseLong(currentStatus) != IUMConstants.STATUS_AWAITING_REQUIREMENTS)) ) ||
                                           ( (!lob.equals(IUMConstants.LOB_INDIVIDUAL_LIFE)) &&
                                             ((Long.parseLong(currentStatus) != IUMConstants.STATUS_UNDERGOING_ASSESSMENT) &&
                                              (Long.parseLong(currentStatus) != IUMConstants.STATUS_AWAITING_REQUIREMENTS) &&
                                              (Long.parseLong(currentStatus) != IUMConstants.STATUS_NB_REVIEW_ACTION)) ) ) {
                                     %>
                                   <td class="label2" align="right" colspan="9" id="saveCancel">
                                  <input class="button1" type=button value="Save"     disabled      name="save1Btn">
                                  <input class="button1" type=button value="Cancel"   disabled      name="cancel1Btn">
                                </td>
                                <td class="label2" align="right" colspan="9" id="maintainCancel">
                                  <input class="button1" type=button value="Save"     disabled      name="save2Btn">
                                  <input class="button1" type=button value="Cancel"   disabled      name="cancel2Btn">
                                </td>
                                <td class="label2" align="right" colspan="9" id="createMaintain">
                                  <input class="button1" type=button value="Maintain" disabled      name="maintainBtn">
                                  <input class="button1" type=button value="Create"   disabled      name="createBtn">
                                </td>
                                   <% } else { %> --%>
                                <td class="<%=LABEL_CLASS%>" align="left" colspan="9" id="saveCancel">
                                  <input class="button1" type=button value="Save"     onclick="validateRequirement('<%=contextPath%>','<%=lob%>');"          name="save1Btn">
                                  <input class="button1" type=button value="Cancel"   onclick="document.requirementForm.reset();changeLevelAndType('<%=lob%>');disableRequirementFields('<%= lob %>');hideSaveCancel(); hideMaintainCancel();showCreateMaintain();"           name="cancel1Btn">
                                </td>
                                <td class="<%=LABEL_CLASS%>" align="left" colspan="9" id="maintainCancel">
                                  <input class="button1" type=button value="Save"     onclick=" validateForMaintainRequirement('<%=contextPath%>','<%=lob%>');"          name="save2Btn">
                                  <input class="button1" type=button value="Cancel"   onclick="document.requirementForm.reset();changeLevelAndType('<%=lob%>');disableRequirementFields('<%= lob %>');hideSaveCancel(); hideMaintainCancel();showCreateMaintain();"           name="cancel2Btn">
                                </td>
                                  
                                  <%--<% if (statValue == null) statValue = "0"; %>
                                  <% if (statValue.trim().equals("")) statValue = "0"; %>                   
                                  
                                      <% if (compReqValue.equals("Y")) { %>
                                         <td class="label2" align="right" colspan="9" id="createMaintain">
                                         <input class="button1" type=button value="Maintain" disabled      name="maintainBtn">
                                         <input class="button1" type=button value="Create"   onclick="disableRequirementFields('<%= lob %>');createRequirement('<%=lob%>');showSaveCancel(); hideCreateMaintain(); hideMaintainCancel();"   name="createBtn" <%= CREATE_KO%>>
                                         </td>
                                      <% } else { %>--%>
                                         <td class="<%=LABEL_CLASS%>" align="left" colspan="9" id="createMaintain">
                                         <input class="button1" type=button value="Maintain" onclick="disableRequirementFields('<%= lob %>');maintainRequirement();showMaintainCancel(); hideSaveCancel();hideCreateMaintain();" name="maintainBtn" <%= MAINTAIN_KO%>>
                                         <input class="button1" type=button value="Create"   onclick="disableRequirementFields('<%= lob %>');createRequirement('<%=lob%>');showSaveCancel(); hideCreateMaintain(); hideMaintainCancel(); enableComment();"   name="createBtn" <%= CREATE_KO%>>
                                         </td>
                                      <%--<% } %>
                                  <% } else {%>
                                <td class="label2" align="right" colspan="9" id="createMaintain">
                                  <input class="button1" type=button value="Maintain" disabled      name="maintainBtn">
                                  <input class="button1" type=button value="Create"   onclick="disableRequirementFields('<%= lob %>');createRequirement('<%=lob%>');showSaveCancel(); hideCreateMaintain(); hideMaintainCancel();"   name="createBtn" <%= CREATE_KO%>>
                                </td>
                                  <% } %>
                                <% } %>
                                <% } else if (Long.parseLong(currentStatus) == IUMConstants.STATUS_AR_CANCELLED) { %>
                                <td class="label2" align="right" colspan="9" id="saveCancel">
                                  <input class="button1" type=button value="Save"     disabled      name="save1Btn">
                                  <input class="button1" type=button value="Cancel"   disabled      name="cancel1Btn">
                                </td>
                                <td class="label2" align="right" colspan="9" id="maintainCancel">
                                  <input class="button1" type=button value="Save"     disabled      name="save2Btn">
                                  <input class="button1" type=button value="Cancel"   disabled      name="cancel2Btn">
                                </td>
                                <td class="label2" align="right" colspan="9" id="createMaintain">
                                  <input class="button1" type=button value="Maintain" disabled      name="maintainBtn">
                                  <input class="button1" type=button value="Create"   disabled      name="createBtn">
                                </td>
                                <% } %>       --%>
                              </tr>
                            </table>
							  <script language="JavaScript">
							    <% if (!actionForm.getRefNo().equals(req.getRefNo())) { %>
                                       disableRequirementFields('<%=lob%>');
                                       disable(document.requirementForm.maintainBtn);
							    <% } %>

							  </script>

                            </form>
                          </td>
                        </tr>
                      </table>
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
            <tr>
              <td colspan="10" class="label5">� 2003 Sun Life Financial. All rights reserved.</td>
            </tr>
            <tr>
              <td colspan="10" class="label5">&nbsp;</td>
            </tr>
          </table>
          <!-- Body -->
         </td>
      </tr>
    </table>
  </body>
</html>