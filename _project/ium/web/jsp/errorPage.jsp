<%@ page language="java" buffer="1024kb"%>
<%@ taglib uri="/struts-html.tld" prefix="html" %> 
<%@ page language="java" import="java.util.*" %>
<%String contextPath = request.getContextPath();%>
<script language="Javascript">
   if (parent.document.forms[0]) {
       parent.window.location = "<%=contextPath%>/exception.do?error=<%= (String) request.getAttribute("errors")%>";
   }
</script>
<%
/* Andre Ceasar Dacanay - css enhancement - start */
String iumCss = "";
String iumColorO = "";
String companyCode = "";
try {
	ResourceBundle rbWMSSP = ResourceBundle.getBundle("dbconfig");
	iumCss = rbWMSSP.getString("IUM_CSS_VERSION");
	companyCode = iumCss;
	iumCss = "_"+iumCss;
} catch (Exception e){}
if(iumCss.equals("_CP")){
	iumColorO = "#FFCB00";
	companyCode="";
}else if(iumCss.equals("_GF")){
	iumColorO = "#303A77";
	companyCode = companyCode+" ";
}else{
	iumColorO = "#FFCB00";
}
/* Andre Ceasar Dacanay - css enhancement - end */
%>
<html>
  <head>
    <title><%=companyCode%>Integrated Underwriting and Medical System</title>
    <link href="<%=contextPath%>/css/main.css" rel="stylesheet" type="text/css">
    <script language="Javascript" src="<%=contextPath%>/js/navigation.js"></script>
  </head>
  <body leftmargin="0" topmargin="0">
  <form>
      <table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%">
        <tr>
          <td width="100%" colspan="2">    
            <jsp:include page="header.jsp" flush="true"/>
          </td>
        </tr>
        <tr>
          <td rowspan="2" bgcolor="<%=iumColorO%>" height="100%" width="20"><img src="<%=contextPath%>/images/spacer.gif" width="20"></td>
          <td background="<%=contextPath%>/images/GradientGray.jpg" height="27" width="100%" valign=top>&nbsp;&nbsp;
            <span class="main">Integrated Underwriting and Medical System</span>
          </td>
        </tr>  
        <tr>
          <td width="100%" height="100%" valign="top">
            <!-- BODY -->
            <table border="0" cellpadding="10" cellspacing="0" width="100%">
              <tr>
                <td>
                  <table border="0" cellpadding="10" cellspacing="0" width="100%" height="500">
                    <tr>
                      <td valign="top">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                          <tr><td class="label1"><html:errors/></td></tr>
                          <tr><td class="label5">&nbsp;</td></tr>
                          <tr><td class="label5">&nbsp;</td></tr>
                          <tr><td class="label5"><a href="javascript: history.back()">Back to previous page</a></td></tr>
                        </table>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr>
                <td colspan="10" class="label5">� 2003 Sun Life Financial. All rights reserved.</td>
              </tr>
            </table>
            <!-- BODY -->
          </td>
        </tr>
      </table>
    </form>
  </body>
</html>
