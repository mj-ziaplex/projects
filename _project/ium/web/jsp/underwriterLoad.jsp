<%@ taglib uri="/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/struts-html.tld" prefix="html" %>
<%@ taglib uri="/ium.tld" prefix="ium" %>
<%@ page language="java" import="java.util.*, com.slocpi.ium.ui.form.*, com.slocpi.ium.util.*" buffer="12kb"%>
<%
	String contextPath = request.getContextPath();
%>
<%
/* Andre Ceasar Dacanay - css enhancement - start */
String iumCss = "";
String companyCode = "";
try {
	ResourceBundle rbWMSSP = ResourceBundle.getBundle("dbconfig");
	iumCss = rbWMSSP.getString("IUM_CSS_VERSION");
	companyCode = iumCss;
	iumCss = "_"+iumCss;
} catch (Exception e){}
if(iumCss.equals("_CP")){
	companyCode="";
}else if(iumCss.equals("_GF")){
	companyCode = companyCode+" ";
}
/* Andre Ceasar Dacanay - css enhancement - end */
%>
<html>
  <head>
    <title><%=companyCode%>Integrated Underwriting and Medical System</title>
    <link href="<%=contextPath%>/css/main<%=iumCss%>.css" rel="stylesheet" type="text/css">
    <script language="Javascript" src="<%=contextPath%>/js/navigation.js"></script>
    <script language="Javascript" src="<%=contextPath%>/jsp/validate_request.jsp"></script>
  </head>
  
  <body leftmargin="0" topmargin="0">
    
    <table border="0" width="450" cellpadding="2" cellspacing="1" bgcolor=#FFFFFF style="border: 1px solid #D1D1D1;" class="listtable1">
      <tr class="headerrow1">
        <td width="50%">UNDERWRITER</td>
        <td width="15%">FOR ASSESSMENT</td>
        <td width="15%">UNDERGOING ASSESSMENT</td>
        <td width="20%">TOTAL</td>
      </tr>
      
     <%
      int i=0;
      String tr_class;
      String td_bgcolor;
      %>

      <logic:iterate id="req" name="userLoadListForm" property="userLoadList" scope="request">	

      <%
      if (i%2 == 0) {
        tr_class = "row1";
        td_bgcolor = "#CECECE";
      } else {
        tr_class = "row2";
        td_bgcolor = "#EDEFF0";
      }
      
     
      %>
      <tr class="<%=tr_class%>" >
        <td style="cursor:hand" onclick="filterByUnderwriter('<bean:write name="req" property="user_id"/>','');" class=links1>  <div align="left"><bean:write name="req" property="last_name"/>, 
        <bean:write name="req" property="first_name"/><!--<bean:write name="req" property="middle_name"/>--></div> </td>

    
    <%  
    	ArrayList arr = ((UserLoadForm)req).getLoadByStatus();
    %>
    	<td style="cursor:hand" onclick="filterByUnderwriter('<bean:write name="req" property="user_id"/>','<%=IUMConstants.STATUS_FOR_ASSESSMENT%>');" class=links1>
    		<div><%=arr.get(0)%></div>
    	</td>
    	<td style="cursor:hand" onclick="filterByUnderwriter('<bean:write name="req" property="user_id"/>','<%=IUMConstants.STATUS_UNDERGOING_ASSESSMENT%>');" class=links1>
			<div><%=arr.get(1)%></div>
		</td>
		
        <td style="cursor:hand" onclick="filterByUnderwriter('<bean:write name="req" property="user_id"/>','');" class=links1><div><bean:write name="req" property="totalLoad"/></div></td>
      </tr>
      <%i++;%>
      </logic:iterate>
      
    </table>

  </body>
</html>
