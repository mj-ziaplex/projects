<%@ page language="java" import="java.util.*,com.slocpi.ium.data.*, com.slocpi.ium.ui.form.*, com.slocpi.ium.util.*, com.slocpi.ium.ui.util.*, java.text.* " buffer="12kb"%>
<%@ taglib uri="/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/struts-html.tld" prefix="html" %>
<%@ taglib uri="/ium.tld" prefix="ium" %>

<%String contextPath = request.getContextPath();%>
<%
/* Andre Ceasar Dacanay - css enhancement - start */
String iumCss = "";
String iumColorB = "";
String companyCode = "";
try {
	ResourceBundle rbWMSSP = ResourceBundle.getBundle("dbconfig");
	iumCss = rbWMSSP.getString("IUM_CSS_VERSION");
	companyCode = iumCss;
	iumCss = "_"+iumCss;
} catch (Exception e){}
if(iumCss.equals("_CP")){
	iumColorB="#2A4C7C";
	companyCode="";
}else if(iumCss.equals("_GF")){
	iumColorB="#303A77";
	companyCode = companyCode+" ";
}else{
	iumColorB="#2A4C7C";
}
/* Andre Ceasar Dacanay - css enhancement - end */
%>
<%  ExaminerForm eForm = (ExaminerForm) request.getAttribute("examiner"); %>
<%
	String sStatus = eForm.getStatus();
	String disabledFlag = "";
	if(sStatus.equals("L")){
		disabledFlag = "disable";
	}
%>


<%


    // This routine queries the access rights of the users (RCRIVERA)
    //
    // --- START --- 
    
    StateHandler sessionHandler = new StateHandler();
    Access access = new Access();
	HashMap userAccess = sessionHandler.getUserAccess(request);
    if(!access.hasAccess(userAccess,IUMConstants.PAGE_EXAMINERS,IUMConstants.ACC_INQUIRE) ){                                                                                                                    
    	response.sendRedirect("noAccessPage.jsp");   
    } 
    
    String CREATE_ACCESS = "";
    String MAINTAIN_ACCESS = "";
    String DELETE_ACCESS = "";
    String EXECUTE_ACCESS = "";  
    
    if(!access.hasAccess(userAccess,IUMConstants.PAGE_EXAMINERS,IUMConstants.ACC_CREATE) ){                                                                                                                     
	  CREATE_ACCESS = "DISABLED"; 	
    }
    if(!access.hasAccess(userAccess,IUMConstants.PAGE_EXAMINERS,IUMConstants.ACC_MAINTAIN) ){                                                                                                                     
	  MAINTAIN_ACCESS = "DISABLED";
    }
    if(!access.hasAccess(userAccess,IUMConstants.PAGE_EXAMINERS,IUMConstants.ACC_DELETE) ){                                                                                                                     
	  DELETE_ACCESS = "DISABLED";
    }
    if(!access.hasAccess(userAccess,IUMConstants.PAGE_EXAMINERS,IUMConstants.ACC_EXECUTE) ){                                                                                                                     
	  EXECUTE_ACCESS = "DISABLED"; 	
    }
    
    // --- END ---
    
    UserData userData = sessionHandler.getUserData(request); 
	UserProfileData userPref = userData.getProfile();
 %>    
<bean:size id="sizeList" name="page" property="list" />

<% int recordsNum = sizeList.intValue(); %>

<html>
<head>
<title><%=companyCode%>Integrated Underwriting and Medical System</title>
<link href="<%=contextPath%>/css/pw_main.css"     rel="stylesheet" type="text/css">
<link href="<%=contextPath%>/css/main<%=iumCss%>.css" rel="stylesheet" type="text/css">
<link href="<%=contextPath%>/css/styleCommon.css" rel="stylesheet" type="text/css">
<script language="JavaScript" src="<%=contextPath%>/js/dhtml_init.js"></script>
<script language="JavaScript" src="<%=contextPath%>/js/navigation.js"></script>
<script language="JavaScript" src="<%=contextPath%>/js/validation.js"></script>
<script language="JavaScript" src="<%=contextPath%>/jsp/sl_left_nav.jsp"></script>
<script language="JavaScript">

function putButton(numRecordsPerView, numRecords){
  		if(numRecords >= 10  && numRecordsPerView >= 10){
  	        document.all["create_btn"].style.display = "block";
  	    }
  	    else{
  	       document.all["create_btn"].style.display = "none";
  	    }
}
  	
function createExaminer()
{	
		//selectAllSpecializations();
		//if(validateExaminer()){
		//	document.examinerForm.status.value = 'C';
		//	document.examinerForm.action = 'createExaminer.do';
		//	document.examinerForm.submit();
		//}
		document.examinerForm.status.value = 'C';
		document.examinerForm.action = 'listExaminers.do';
		document.examinerForm.submit();
}

function validateExaminer(){
	var form = document.forms[0];
	var examId = form.examinerId.value;
	var lastName = form.lastName.value;
	var firstName = form.firstName.value;
	var middleName = form.middleName.value;
	var salutation = form.salutation.value;
	var rank = form.rank.value;
	var rankDate = form.rankEffectiveDate.value;
	var tin = form.TIN.value;
	var officeNo = form.officeNumber.value;
	var faxNo = form.faxNumber.value;
	var mobileNo = form.mobileNumber.value;
	var zip = form.zipCode.value;
	var clinicFrom = form.clinicHrsFrom.value;
	var clinicTo = form.clinicHrsTo.value;
	var examinePlace = form.examinePlace.value;
	var len = form.specializations.length;
							
	if( isExaminerId(examId,'examinerId') && hasLastName(lastName,'lastName') &&hasFirstName(firstName,'firstName') &&hasMiddleName(middleName,'middleName') && hasSalutation(salutation,'salutation') && hasRank(rank,'rank')
			&& isRankDateValid(rankDate,'rankEffectiveDate') && isTIN(tin,'TIN') && isOfficeNoValid(officeNo,'officeNumber') && isFaxNoValid(faxNo,'faxNumber') &&  isMobileNoValid(mobileNo,'mobileNumber') && isZipCodeValid(zip,'zipCode')
			&& isClinicHour(clinicFrom,'clinicHrsFrom') && isClinicHour(clinicTo,'clinicHrsTo') && checkTimeSpan() && hasExaminePlace(examinePlace,'examinePlace') 
			 && hasSpecializations(len,'specialization')  ){
		return true;
	}else{
		return false;
	}
	
}


function isExaminerId(examId,fieldName){
	if(isEmpty(examId)){
		alert('Examiner ID is a required field.');
		eval("document.forms[0]."+fieldName+".focus()");
		return false;
	}else{
		if(isNumeric(examId))
		{
			return true;
		}else{
			alert('Examiner Id must be numeric.');
			eval("document.forms[0]."+fieldName+".focus()");
			return false;
		}
	}
}

function hasLastName(lastName,fieldName){
	if(isEmpty(lastName)){
		alert('Last Name is a required field');
		eval("document.forms[0]."+fieldName+".focus()");
		return false;
	}else{
		return true;
	}
}

function hasFirstName(firstName,fieldName){
	if(isEmpty(firstName)){
		alert('First Name is a required field');
		eval("document.forms[0]."+fieldName+".focus()");
		return false;
	}else{
		return true;
	}
}

function hasMiddleName(middleName,fieldName){
	if(isEmpty(middleName)){
		alert('Middle Name is a required field');
		eval("document.forms[0]."+fieldName+".focus()");
		return false;
	}else{
		return true;
	}
}

function hasSalutation(salutation,fieldName){
	if(isEmpty(salutation)){
		alert('Salutation is a required field');
		eval("document.forms[0]."+fieldName+".focus()");
		return false;
	}else{
		return true;
	}
}

function hasRank(rank,fieldName){
	if(isEmpty(rank)){
		alert('Please select a rank.');
		eval("document.forms[0]."+fieldName+".focus()");
		return false;
	}else{
		return true;
	}
}

function hasExaminePlace(examinePlace,fieldName){
	if(isEmpty(examinePlace)){
		alert('Please select an Examination Place.');
		eval("document.forms[0]."+fieldName+".focus()");
		return false;
	}else{
		return true;
	}
}

function hasSpecializations(len,fieldName){
	if(len==0){
		alert("Please select at least one specialization.");
		eval("document.forms[0]."+fieldName+".focus()");
		return false;
	}else{
		return true;
	}
}

function isRankDateValid(rankDate,fieldName){
	if(isEmpty(rankDate)){
		alert('Rank Effective Date is a required field.')
		eval("document.forms[0]."+fieldName+".focus()");
		return false;
	}
	
	if(isValidDate(rankDate)){
		return true;
	}else{
		alert('rank date is not valid. Format should be DDMONYYYY');
		eval("document.forms[0]."+fieldName+".focus()");
		return false;
	}
}

function isOfficeNoValid(officeNo,fieldName){
	return isPhoneNo('Office ',officeNo,fieldName);
}

function isFaxNoValid(faxNo,fieldName){
	return isPhoneNo('Fax ',faxNo,fieldName);
}

function isMobileNoValid(mobileNo,fieldName){
	return isPhoneNo('Mobile  ',mobileNo,fieldName);
}

function isZipCodeValid(zip,fieldName){
	if(isEmpty(zip))
	{
		return true;
	}else{
		if(isNumeric(zip))
		{
			return true;
		}else{
			alert('Zip Code must be numeric.');
			eval("document.forms[0]."+fieldName+".focus()");
			return false;
		}
	}
}

function isTIN(tin,fieldName)
{
	if(isEmpty(tin))
	{
		alert('TIN is a required field');
		eval("document.forms[0]."+fieldName+".focus()");
		return false;
	}
	var re = /\d\d\d-\d\d\d-\d\d\d/;
	if(re.test(tin)){
		return true;
	}else{
		alert('TIN format should be nnn-nnn-nnn.');
		eval("document.forms[0]."+fieldName+".focus()");	
		return false;
	}
}

function isPhoneNo(kind,phone,fieldName){
	if(isEmpty(phone))
	{
		return true;
	}
	for (i = 0; i < phone.length; i++) {   
        var c = phone.charAt(i);	
		if (! (isDigit(c) || c == " " || c == "(" || c == ")" || c == "-")) {
			alert(kind+' is not a valid Phone format. It should only contain the following characters:  0-9,-,( and )  ');
			eval("document.forms[0]."+fieldName+".focus()");
            return false;
        }    
    }	
    return true;
}

function isClinicHour(value,fieldName){
		// Checks if time is in HH:MM:SS AM/PM format.
		// The seconds and AM/PM are optional.
		if(isEmpty(value))
		{
			alert('Clinic Hour is a required field.');
			eval("document.forms[0]."+fieldName+".focus()");
			return false;
		}
				
		var timePat = /^(\d{1,2}):(\d{2})((AM|am|PM|pm))?$/;
		var matchArray = value.match(timePat);
		
		if (matchArray == null) {
			alert(value+" is not a valid Time format.");
			eval("document.forms[0]."+fieldName+".focus()");
			return false;
		}
		
		hour = matchArray[1];
		minute = matchArray[2];
		ampm = matchArray[3];
		
		if (ampm=="") { ampm = null }
		
		if (hour < 0  || hour > 23) {
			alert(value+": Hour must be between 1 and 12. (or 0 and 23 for military time)");
			return false;
		}
		
		if (hour <= 12 && ampm == null) {
				alert("You must specify AM or PM.");
				eval("document.forms[0]."+fieldName+".focus()");
				return false;
		}
		
		if  (hour > 12 && ampm != null) {
			alert(value+": You can't specify AM or PM for military time.");
			eval("document.forms[0]."+fieldName+".focus()");
			return false;
		}
		
		if (minute<0 || minute > 59) {
			alert (value+": Minute must be between 0 and 59.");
			eval("document.forms[0]."+fieldName+".focus()");
			return false;
		}
			
		return true;

}

function checkTimeSpan(){
	var fromTime = document.forms[0].clinicHrsFrom.value;
	var toTime = document.forms[0].clinicHrsTo.value;

	var timePat = /^(\d{1,2}):(\d{2})((AM|am|PM|pm))?$/;
	var fromMatchArray = fromTime.match(timePat);
	var toMatchArray = toTime.match(timePat);
	
	fromHour = fromMatchArray[1];
	fromMinute = fromMatchArray[2];
	fromAmPm = fromMatchArray[3];
		
	toHour = toMatchArray[1];
	toMinute = toMatchArray[2];
	toAmPm = toMatchArray[3];
	
	if(fromAmPm==""){fromAmPm = null;}
	if(toAmPm=="") {toAmPm=null; }
	
	if(fromAmPm != null){
		if((fromAmPm == "PM") || (fromAmPm == "pm")){
			if(fromHour!="12"){
				fromHour = parseFloat(fromHour) + 12;			
			}
		}else if((fromAmPm == "AM") || (fromAmPm == "am")){
			if(fromHour=="12"){
				fromHour = parseFloat(fromHour) - 12;
			}
		}
	}
	
	if(toAmPm != null){
		if((toAmPm == "PM") || (toAmPm == "pm")){
			if(toHour != 12){
				toHour = parseFloat(toHour) + 12;
			}
		}else if((toAmPm == "AM") || (toAmPm == "am")){
			if(toHour=="12"){
				toHour = parseFloat(toHour) - 12;
			}
		}
	}

	var totalMinutesFrom = ((parseFloat(fromHour))*60) + parseFloat(fromMinute);
	var totalMinutesTo = ((parseFloat(toHour))*60) + parseFloat(toMinute);
	//alert("Total Minutes From: "+totalMinutesFrom);
	//alert("Total Minutes To: "+totalMinutesTo);
	
	if((totalMinutesFrom>totalMinutesTo) || (totalMinutesFrom==totalMinutesTo)){
		alert("Hours From should be earlier than Hours To.");
		document.forms[0].clinicHrsFrom.focus();
		return false;
	}
			
	return true;
}


function selectAllSpecializations()
{
	var form = document.forms[0];
	var len = form.specializations.length;	
	
	for(i=len-1; i>=0; i--)
	{
		form.specializations[i].selected = true;
	}
}

function viewExaminer()
{
	
	var form = document.forms[0];
	var len = form.selectedExaminer.length;
	
	var radio_choice = false;
	if(len==undefined){
		if(form.selectedExaminer.checked){
			radio_choice = true;
			form.examinerId.value = form.selectedExaminer.value;	
		}
	}else{
		for (counter = 0; counter < len; counter++)
		{
			if (form.selectedExaminer[counter].checked)
			{
				radio_choice = true; 
				form.examinerId.value = form.selectedExaminer[counter].value;
				break;
			}
		}
	}
	if (!radio_choice)
	{
		alert("Please select an examiner.")
		return (false);
	}else{
		var eid = form.examinerId.value;
		document.examinerForm.action = 'viewExaminer.do';
		document.examinerForm.submit();	
	}
}

function saveExaminer()
{
	selectAllSpecializations();
	if(validateExaminer()){
		var sStatus = document.examinerForm.status.value;
		if(sStatus == 'C'){
			selectAllSpecializations();
			document.examinerForm.action = 'createExaminer.do';
			document.examinerForm.status.value = 'L';
			document.examinerForm.submit();
		}else if(sStatus == 'M'){
			selectAllSpecializations();
			document.examinerForm.action = 'saveExaminer.do';
			document.examinerForm.status.value = 'L';
			document.examinerForm.submit();
		}
	}
}

function addSpecialization()
{
	var form = document.forms[0];						
	var len = form.specializations.length;	
	var specialization = form.specialization.value;
	var arrayOfStrings = splitMe(specialization);
					
	if(specialization == "")
	{
		return false;
	}
						
	form.specializations.value = "";
						
	for(i=0; i<len; i++)
	{
		if(arrayOfStrings[0] == form.specializations[i].value)
		{
			alert(arrayOfStrings[1] + " already exists on the list.");
			return false;
		}
	}
	
	form.specializations[len] = new Option(arrayOfStrings[1],arrayOfStrings[0]);
}

function splitMe(stringToSplit)
{
	var arrayOfStrings = stringToSplit.split("~");
	//alert('the id is..' + arrayOfStrings[0]);
	//alert('the desc is..' + arrayOfStrings[1]);
	return arrayOfStrings;
}

function removeSpecialization()
{
	var form = document.forms[0];
	var len = form.specializations.length;	
	
	for(i=len-1; i>=0; i--)
	{
		if(form.specializations[i].selected == true)
		{ 						
			form.specializations[i].selected = false;
		form.specializations[i] = null;
		}
	}
}

<!-- pagination -->
function rePaginate(page, actionUrl)
{
	var frm = document.forms[0];
	
	if(frm.pageNo != null)
	{
		frm.pageNo.value = page;
	}
	submitForm(actionUrl);
}
	
function submitForm(actionUrl)
{	
	var status = document.examinerForm.status.value
	document.examinerForm.action = actionUrl;
	//if(status=='M'){
	//	document.examinerForm.status.value = 'C';
	//}else{
	//	document.examinerForm.status.value = 'P';
	//}
	document.examinerForm.submit();
}

function cancelExaminer(){
	document.examinerForm.action = 'listExaminers.do';
	document.examinerForm.status.value = 'L';
	document.examinerForm.submit();
}

function makeUpperCase(field){
	var upper = eval("document.examinerForm."+field+".value.toUpperCase()");
	eval("document.examinerForm."+field+".value="+"'"+upper+"'");
}

function defaultCursor(){
		var flag = document.examinerForm.status.value;
		if(flag != 'M'){
			document.examinerForm.examinerId.focus();
		}else{
			document.examinerForm.lastName.focus();
		}
}

<!-- pagination -->
</script>
</head>

<BODY leftMargin=0 topmargin=0 marginwidth=0 marginheight=0 onUnload="closePopUpWin();" onload="defaultCursor(); putButton(<%=userPref.getRecordsPerView()%> , <%=recordsNum%>);" >
<form name="examinerForm"  method="post">   
<input type="hidden" name="status" value="<bean:write name="examiner" property="status" />"> 
<table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%">
<tr><td width="100%" colspan="2">
  <!-- HEADER -->
  <jsp:include page="header.jsp" flush="true"/>
  	<table border="0" cellpadding="0" cellspacing="0" width="100%" height="100%">
  			<tr vAlign=top> 
   				<TD background="<%=contextPath%>/images/bg_yellow.gif" id=leftnav vAlign=top width=147> 
     				<IMG height=9 src="<%=contextPath%>/images/sp.gif" width=147><BR>
     				<IMG height=30 src="<%=contextPath%>/images/sp.gif" width=7> <BR>
			      <IMG height=7 src="<%=contextPath%>/images/sp.gif" width=147><BR>
			      <DIV id=level0 style="POSITION: absolute; VISIBILITY: hidden; WIDTH: 100%; Z-INDEX: 500"> 
			       	<SCRIPT language=javascript>
								<!--
								document.write(writeMenu(''))
								if (IE4) { document.write(writeDiv()) };								
								//-->
							</SCRIPT>
				    </DIV>
      			<DIV id=NSFix style="POSITION: absolute; VISIBILITY: hidden; Z-INDEX: 999">
      				&nbsp;
      			</DIV>
      			<!-- Netscape needs a hard-coded DIV to write dynamic DIVs --> 
      			<SCRIPT language=javascript>
             		<!--
               	 	if (NS4) { document.write(writeDiv()) }
                 //-->
             </SCRIPT>
             <SCRIPT>
              <!--
                   	initMenu();
           		//-->
           	</SCRIPT>
      <!-- END MENU CODE --> 
 
    </td>      											
			    <td width="100%"> 
			     	<div align="left"> 
	    				<table border="0" cellpadding="0" width="100%" cellspacing="0">
      					<tr> 
       						<td width="100%"> 
							      <div align="left"> 
							        <table border="0" cellpadding="3" cellspacing="0" width="100%" height="27"  background="<%=contextPath%>/images/sectionbkgray.jpg">
							          <tr> 
							            <td valign="top"> 
							               <span class="main">&nbsp;Integrated Underwriting and Medical System </span>
							            </td>
							          </tr>
							        </table>
							      </div>
						       </td>
						     </tr>
				         <tr> 
				           <td width="100%" colspan=2>&nbsp;</td>
      				</tr>
			 	     	</table>				 	       	
   	 				<table width="100%" cellpadding="3" cellspacing="5" border="0">
      				<tr valign="top">
     						<td class="title2">Examiner Maintenance
         						<!--Start of form -->
      						<table border="1" bordercolor="<%=iumColorB%>" cellpadding=10 cellspacing=0 bgcolor="" width="700" class="listtable1">
       							<tr>
       								<td colspan="2" height="100%">
          							<table border="0" cellpadding="2" cellspacing="1" bgcolor=#FFFFFF style="border: 0px solid #D1D1D1;" class="listtable1">
           								<tr>
           									<td>
    	   										<html:errors/>  
							              		<!--IFRAME src="tempAdminDetail.html" width="665" height="100" scrolling="no" frameborder="0" >
							                	</IFRAME-->            
                  
        <table border="0" width="669" >
        <tr>
        <td class="label2" ><span class="required">*</span><b>Examiner &nbsp;&nbsp;ID</b></td>
        <td colspan="6" ><input type="text" name="examinerId" size="10" class="label2" value="<bean:write name="examiner" property="examinerId" />" maxlength="5"
        	<logic:notEqual name="examiner" property="status" value="C" >readonly</logic:notEqual>   > </td>
        </tr>

        <tr>
        <td class="label2" width="110"><span class="required">*</span><b>Last &nbsp;&nbsp;Name</b></td>
        <td width="110" colspan="2"><input type="text" name="lastName" class="label2" size="30" value="<bean:write name="examiner" property="lastName" />"  maxlength="40"
        				<logic:equal name="examiner" property="status" value="L" > disabled </logic:equal> onblur="makeUpperCase('lastName')" > </td>
        <td class="label2" width="110"><span class="required">*</span><b>First &nbsp;&nbsp;Name</b></td>
        <td width="110"><input type="text" name="firstName" class="label2" size="30" value="<bean:write name="examiner" property="firstName" />" maxlength="25"
        				<logic:equal name="examiner" property="status" value="L" > disabled </logic:equal> onblur="makeUpperCase('firstName')"  > </td>
        <td class="label2" width="110"><span class="required">*</span><b>Middle &nbsp;&nbsp;Name</b></td>
        <td width="110"><input type="text" name="middleName" class="label2" size="30" value="<bean:write name="examiner" property="middleName" />"  maxlength="25"
        				<logic:equal name="examiner" property="status" value="L" > disabled </logic:equal> onblur="makeUpperCase('middleName')"  > </td>
        </tr>

        <tr>
        <td class="label2" width="110"><span class="required">*</span><b>Salutation</b></td>
        <td><input type="text" name="salutation" size="5"class="label2" value="<bean:write name="examiner" property="salutation" />" maxlength="25"
  	      				<logic:equal name="examiner" property="status" value="L" > disabled </logic:equal> onblur="makeUpperCase('salutation')"  > </td>
        <td>&nbsp;</td> 				
        <td class="label2" width="110"><b>&nbsp;&nbsp;Date of &nbsp;&nbsp;Birth</b></td>
        <td width="110">
        	<table width="110">
        		<tr>
        		<td>
		        	<input type="text" name="dateOfBirth" class="label2" maxlength="9" value="<bean:write name="examiner" property="dateOfBirth" />" 
        				<logic:equal name="examiner" property="status" value="L" > disabled </logic:equal> onblur="makeUpperCase('dateOfBirth')"  >
        		</td>
        		<td>		
		        	<logic:notEqual name="examiner" property="status" value="L" >
        				<a href="#" onClick="popUpWin('<%=contextPath%>/jsp/calendar.jsp?textbox=examinerForm.dateOfBirth',290,155);">
		        	</logic:notEqual>
        		    <img src="<%=contextPath%>/images/calendar.gif" height="17" width="19" border="0">
			       	<logic:notEqual name="examiner" property="status" value="L" >
	    		  		</a>
			      	</logic:notEqual>
			      </td>	
			      </tr>
			</table>	      	
		</td>
		<td class="label2" width="110" valign="middle"><b>&nbsp;&nbsp;Gender</b></td>
        <td width="110" class="label2">
        			<input type="radio" name="sex" value="M" <logic:equal name="examiner" property="sex" value="M" >CHECKED </logic:equal>
        					 <logic:empty name="examiner" property="sex">CHECKED </logic:empty>
        					<logic:equal name="examiner" property="status" value="L" > disabled </logic:equal> >Male
        			&nbsp;<input type="radio" name="sex" value="F" <logic:equal name="examiner" property="sex" value="F" >CHECKED </logic:equal>
        					<logic:equal name="examiner" property="status" value="L" > disabled </logic:equal>	>Female</td>
        </tr>
        
        <tr>
        <td class="label2" width="110"><span class="required">*</span><b>Rank</b></td>
        <td width="110"  colspan="2">
        <ium:list className="label2" listBoxName="rank" type="<%=IUMConstants.LIST_RANKS%>" styleName="width:110"  selectedItem="<%=eForm.getRank()%>" disabled="<%=disabledFlag%>" />
        <!--
        <select name="rank" class="label2">
        <option SELECTED>1</option>1
        <option >Rank 2</option>
        <option >Rank 3</option>
        <option >Rank 4</option>
        <option >Rank 5</option>
        </select>    
        -->
        </td>
        <td class="label2" width="110"><span class="required">*</span><b>Rank &nbsp;&nbsp;Effective &nbsp;&nbsp;Date</b></td>
        <td width="110">
	        <table width="110">
        		<tr>
        		<td>
        			<input type="text" name="rankEffectiveDate"class="label2" maxlength="9" value="<bean:write name="examiner" property="rankEffectiveDate" />" 
        			<logic:equal name="examiner" property="status" value="L" > disabled </logic:equal> onblur="makeUpperCase('rankEffectiveDate')" >
        		</td>
        		<td>	
	        		<logic:notEqual name="examiner" property="status" value="L" >
    	    			<a href="#" onClick="popUpWin('<%=contextPath%>/jsp/calendar.jsp?textbox=examinerForm.rankEffectiveDate',290,155);">
    	    		</logic:notEqual>	
           			<img src="<%=contextPath%>/images/calendar.gif" height="17" width="19" border="0">
	        		<logic:notEqual name="examiner" property="status" value="L" >    	    		           			
		      			</a>
		      		</logic:notEqual>
		      	</td>
		      	</tr>
		      </table>	
        </td>
        </tr>

        <tr>
        <td class="label2"><span class="required">*</span><b>TIN</b></td>
        <td colspan="6"><input type="text" name="TIN"class="label2" maxlength="11" value="<bean:write name="examiner" property="TIN" />" 
        		<logic:equal name="examiner" property="status" value="L" > disabled </logic:equal> ></td>
        </tr>

        <tr>
        <td colspan="7" class="label2">&nbsp;</td>
        </tr>

        <tr>
        <td colspan="7" class="label2"><b>&nbsp;&nbsp;Contact Numbers</b></td>
        </tr>

        <tr>
        <td class="label2" width="110"><b>&nbsp;&nbsp;Office</b></td>
        <td width="110"  colspan="2"><input type="text" name="officeNumber"class="label2" maxlength="13" value="<bean:write name="examiner" property="officeNumber" />" 
        		<logic:equal name="examiner" property="status" value="L" > disabled </logic:equal> ></td> 
        <td class="label2" width="110"><b>&nbsp;&nbsp;Fax</b></td>
        <td width="110"><input type="text" name="faxNumber"class="label2" maxlength="13" value="<bean:write name="examiner" property="faxNumber" />" 
        		<logic:equal name="examiner" property="status" value="L" > disabled </logic:equal> > </td>
        <td class="label2" width="110"><b>&nbsp;&nbsp;Mobile</b></td>
        <td width="110" class="label2"><input type="text" name="mobileNumber"class="label2" maxlength="13" value="<bean:write name="examiner" property="mobileNumber" />" 
        		<logic:equal name="examiner" property="status" value="L" > disabled </logic:equal> > </td>
        </tr>

        <tr>
        <td colspan="7" class="label2">&nbsp;</td>
        </tr>

        <tr>
        <td colspan="7" class="label2"><b>&nbsp;&nbsp;Business Address</b></td>
        </tr>
        
        <tr>
		 <td class="label2" valign="top" colspan="2" ><b>&nbsp;&nbsp;Address Line 1</b>&nbsp;&nbsp;</td>
        <td colspan="3 "><input type="text" name="busAddrLine1" size="60" maxlength="60" class="label2" value="<bean:write name="examiner" property="busAddrLine1" />"
        		<logic:equal name="examiner" property="status" value="L" > disabled </logic:equal>  onblur="makeUpperCase('busAddrLine1')"  > </td>
        </tr>
		<tr>
		<td class="label2" valign="top"  colspan="2"><b>&nbsp;&nbsp;Address Line 2</b>&nbsp;&nbsp;</td>
        <td colspan="3"><input type="text" name="busAddrLine2" size="60" maxlength="60" class="label2" value="<bean:write name="examiner" property="busAddrLine2" />"
        		<logic:equal name="examiner" property="status" value="L" > disabled </logic:equal>   onblur="makeUpperCase('busAddrLine2')" > </td>
        </tr>
		<tr>
		<td class="label2" valign="top" colspan="2"><b>&nbsp;&nbsp;Address Line 3</b>&nbsp;&nbsp;</td>
        <td colspan="3"><input type="text" name="busAddrLine3" size="60" maxlength="60" class="label2" value="<bean:write name="examiner" property="busAddrLine3" />"
        		<logic:equal name="examiner" property="status" value="L" > disabled </logic:equal>   onblur="makeUpperCase('busAddrLine3')" > </td>
        </tr>
        <tr>
        <td class="label2" colspan="2"><b>&nbsp;&nbsp;City</b>&nbsp;&nbsp;</td>
        <td ><input type="text" name="city" class="label2" maxlength="25" value="<bean:write name="examiner" property="city" />" 
        		<logic:equal name="examiner" property="status" value="L" > disabled </logic:equal>   onblur="makeUpperCase('city')" > </td>
        <td class="label2" ><b>&nbsp;&nbsp;Province</b>&nbsp;&nbsp;</td>
        <td class="label2"  colspan="2"><input type="text" name="province"class="label2" maxlength="25" value="<bean:write name="examiner" property="province" />" 
        		<logic:equal name="examiner" property="status" value="L" > disabled </logic:equal>   onblur="makeUpperCase('province')" > </td>
        </tr>
        <tr>
        <td class="label2" colspan="2"><b>&nbsp;&nbsp;Country</b>&nbsp;&nbsp;</td>
        <td><input type="text" name="country"class="label2" maxlength="25" value="<bean:write name="examiner" property="country" />" 
        		<logic:equal name="examiner" property="status" value="L" > disabled </logic:equal>   onblur="makeUpperCase('country')" > </td>
        <td class="label2" ><b>&nbsp;&nbsp;Zip Code</b>&nbsp;&nbsp;</td>
        <td  class="label2" colspan="2"><input type="text" name="zipCode"class="label2" maxlength="5" value="<bean:write name="examiner" property="zipCode" />" 
        		<logic:equal name="examiner" property="status" value="L" > disabled </logic:equal> > </td>
        </tr>
        
        <tr>
        <td class="label2" colspan="2"><b>&nbsp;&nbsp;Mail to Address?</b>&nbsp;&nbsp;</td>
        <td width="110" colspan="3" class="label2"><input type="radio" name="mailToBusAddrInd" value="1" <logic:equal name="examiner" property="mailToBusAddrInd" value="1" > CHECKED </logic:equal> 
        					 <logic:empty name="examiner" property="mailToBusAddrInd"> CHECKED </logic:empty>
        					<logic:equal name="examiner" property="status" value="L" > disabled </logic:equal> >Yes&nbsp;
        	<input type="radio" name="mailToBusAddrInd" value="0" <logic:equal name="examiner" property="mailToBusAddrInd" value="0" > CHECKED</logic:equal>
        					<logic:equal name="examiner" property="status" value="L" > disabled </logic:equal> >No&nbsp; </td>
        </tr>
        
        <tr>
        <td colspan="6" class="label2">&nbsp;</td>
        </tr>
        

        <tr>
        <td class="label2" width="220" colspan="2"><b>&nbsp;&nbsp;Offers &nbsp;&nbsp;PreEmployment &nbsp;&nbsp;Service?</b></td>
        <td class="label2" colspan="4"><input type="radio" name="preEmploymentSrvcInd" value="1" <logic:equal name="examiner" property="preEmploymentSrvcInd" value="1">CHECKED</logic:equal>  
        				<logic:empty name="examiner" property="preEmploymentSrvcInd">CHECKED</logic:empty>   
        				<logic:equal name="examiner" property="status" value="L" > disabled </logic:equal> >Yes&nbsp;
        		<input type="radio" name="preEmploymentSrvcInd" value="0" <logic:equal name="examiner" property="preEmploymentSrvcInd" value="0">CHECKED</logic:equal> 
        				<logic:equal name="examiner" property="status" value="L" > disabled </logic:equal> >No&nbsp; </td>
        </tr>
        <tr>
        <td class="label2" width="220" colspan="2"><b>&nbsp;&nbsp;Sun Life &nbsp;&nbsp;Accredited?</b></td>
        <td class="label2" colspan="4"><input type="radio" name="accreditationInd" value="1" <logic:equal name="examiner" property="accreditationInd" value="1">CHECKED </logic:equal>
        				<logic:empty name="examiner" property="accreditationInd" >CHECKED </logic:empty> 
        				<logic:equal name="examiner" property="status" value="L" > disabled </logic:equal> >Yes&nbsp;
        		<input type="radio" name="accreditationInd" value="0"" <logic:equal name="examiner" property="accreditationInd" value="0">CHECKED </logic:equal> 
        				<logic:equal name="examiner" property="status" value="L" > disabled </logic:equal> >No&nbsp; </td>
        </tr>
        <tr>
        <td class="label2" width="220" colspan="2"><span class="required">*</span><b>Clinic Hours</b></td>
        <td class="label2" colspan="4">From &nbsp;&nbsp;<input type="text" name="clinicHrsFrom"size="10" class="label2" maxlength="7" value="<bean:write name="examiner" property="clinicHrsFrom" />" 
        					<logic:equal name="examiner" property="status" value="L" > disabled </logic:equal> onblur="makeUpperCase('clinicHrsFrom')" >&nbsp;&nbsp;To&nbsp;&nbsp;
        		<input type="text" name="clinicHrsTo"size="10" class="label2" maxlength="7" value="<bean:write name="examiner" property="clinicHrsTo" />" 
        					<logic:equal name="examiner" property="status" value="L" > disabled </logic:equal> onblur="makeUpperCase('clinicHrsTo')" > </td>
        </tr>
        <tr>
        <td colspan="6" class="label2">&nbsp;</td>
        </tr>
        
        
        

         <tr>
        <td colspan="6" class="label2">
        <table border="0" cellpadding="0" cellspacing="0">
        <tr>
        <td class="label2" width="220" valign="top" colspan="2"><span class="required">*</span><b>Examination Place</b></td>
        <td class"label2" colspan="4">
        <ium:list className="label2" listBoxName="examinePlace" type="<%=IUMConstants.LIST_EXAMINATION_PLACES%>" styleName="width:280"  selectedItem="<%= eForm.getExaminePlace()%>" disabled="<%=disabledFlag%>" />        
        </td>
             
        </tr>
        
        </table>
        </td>
		</tr>
       <tr>
        <td colspan="6" class="label2">&nbsp;</td>
        </tr>
         
        <tr>
        <td colspan="6" class="label2">
        <table border="0" cellpadding="0" cellspacing="0">
        <tr>
        <td class="label2" width="110" valign="top"><span class="required">*</span><b>Specialization</b></td>
        <td width="110">
        <ium:list className="label2" listBoxName="specialization" type="<%=IUMConstants.LIST_SPECIALIZATIONS%>" styleName="width:250"  selectedItem="" disabled="<%=disabledFlag%>"/>
        
        </td>
        <td>&nbsp;</td>
        <td valign="top">
        <input type="button" value="Add" class="button1"  <logic:equal name="examiner" property="status" value="L" > disabled </logic:equal> onclick="addSpecialization();">
		</td>
        </tr>

        <tr>
        <td class="label2" width="110" valign="top">&nbsp;</td>
        <td width="110">
        <select name="specializations" class="label2" size="5" multiple style="width:250px"  <logic:equal name="examiner" property="status" value="L" > disabled </logic:equal> > 
        	<logic:iterate id="specials" name="examiner" property="specializationsList">
        		<option value="<bean:write name="specials" property="specializationId" />"> <bean:write name="specials" property="specializationDesc" /> </option>
        	 </logic:iterate>
        </select> 
        </td>
        <td>&nbsp;</td>
        <td valign="top">
        <input type="button" value="Remove" class="button1"  <logic:equal name="examiner" property="status" value="L" > disabled </logic:equal> onclick=" removeSpecialization();">
		</td>
        </tr>
        </table>
        </td>
		</tr>        
        </table>
        </td></tr>
        
        <tr><td colspan="5" align="left">
        <logic:notEqual name="examiner" property="status" value="L" >
        <input type="button" value="Save" class="button1" onclick="saveExaminer();">&nbsp;
        <input type="button" value="Cancel" class="button1" onclick="cancelExaminer();">
        </logic:notEqual>

        </td></tr>

              </table>
           </td></tr>
           </table>     

          
          </td>
          </tr>
          <tr><td align="left"><div>
	<logic:equal name="examiner" property="status" value="L" >     
        <input type="button" value="Create" class="button1" onclick="createExaminer();" <%=CREATE_ACCESS%> >&nbsp;
        <input type="button" value="Maintain" class="button1" onclick="viewExaminer();" <%=MAINTAIN_ACCESS%> >
      </logic:equal>
      </div>
    </td></tr>    
          <tr valign="top"> 
            <td colspan="2">
                     
<!--- START OF BODY -->
<table border="1" bordercolor="<%=iumColorB%>" cellpadding=10 cellspacing=0 bgcolor="" width="800" class="listtable1">
        <tr><td colspan="2" height="100%">
        <table border="0" cellpadding="2" cellspacing="1" bgcolor=#FFFFFF style="border: 1px solid #D1D1D1;" class="listtable1">
        <tr class="headerrow1">
        <td width="10"></td>
        <td width="65">ID</td>
        <td width="150">EXAMINER NAME</td>
        <td width="75">ACCREDITED</td>        
        <td width="200">PLACE</td>        
        <td width="200">CLINIC HOURS</td>        
        </tr>

		<% int i = 0; 
			String tr_class;
            String td_bgcolor;
        %>
		<logic:iterate id="examiners" name="page" property="list">
		<%
		  if (i%2 == 0) {
			 tr_class = "row1";
		     td_bgcolor = "#CECECE";
		   }
		  else {
		     tr_class = "row2";
		     td_bgcolor = "#EDEFF0";
		   }
		 %>
		<tr class="<%=tr_class%>" >
		<td><input type="radio" name="selectedExaminer" value="<bean:write name="examiners" property="examinerId"/>" ></td>
		<td><bean:write name="examiners" property="examinerId"/></td>
		<td><bean:write name="examiners" property="lastName"/>&nbsp;<bean:write name="examiners" property="firstName"/>&nbsp;
				<bean:write name="examiners" property="middleName"/></td>
		<td><logic:equal name="examiners" property="accreditationInd" value="1" >Y</logic:equal>
			<logic:equal name="examiners" property="accreditationInd" value="0" >N</logic:equal></td>
		<td><bean:write name="examiners" property="examinePlace"/></td>
		<td><%=DateHelper.format(((ExaminerData)examiners).getClinicHrsFrom(),"hh:mma")%>-<%=DateHelper.format(((ExaminerData)examiners).getClinicHrsTo(),"hh:mma")%></td>
		</tr>
		<% i++; %>
	 </logic:iterate>

		
<!-- START Number of pages -->
		<input type="hidden" name="pageNo" value="<bean:write name="examiner" property="pageNo" />">
					<%		   int pageNumber = Integer.parseInt(eForm.getPageNo());
							       Page displayPage = (Page)request.getAttribute("page");
					%>
					<% if (displayPage == null) { %>
						<tr>
							<td class="headerrow4" colspan="11" align="left" width="100%"></td>
						</tr>
					<% } else { %>
					<tr>
                         <td class="headerrow4" colspan="11" width="100%" height="10" class="label2" valign="bottom" >
                         <bean:size id="noOfPages" name="page" property="pageNumbers" />
                          
                         <%
                                          String viewPage = "listExaminers.do";
                                          int currLink = 1;
                                          int prevLink = 1;
                                          int firstPage = 1;
                                          int lastPage = 1;
                                         if(((ArrayList)displayPage.getPageNumbers()).size() > 0){
                                         	lastPage = ((Integer)((ArrayList)displayPage.getPageNumbers()).get(noOfPages.intValue()-1)).intValue();
                                         }
                          %>
                                         													

                                          <!-- don't display link for previous page if the current page is the first page -->
                                          <% if (pageNumber > firstPage) { %>
                                            <a href="#" onClick="javascript:rePaginate(1,'<%=viewPage%>');"><img src="<%=contextPath%>/images/pagnavfirst_off.gif" name="pagnavfirst" border="0" width="18" height="13"/></a>&nbsp;					
                                            <a href="#" onClick="javascript:rePaginate('<%=pageNumber-1%>','<%=viewPage%>');"><img src="<%=contextPath%>/images/pagnavleft_off.gif" name="pagnavleft" border="0" width="18" height="13"/></a>
                                          <% } else {%>
                                            <img src="<%=contextPath%>/images/pagnavfirst_on.gif" name="pagnavfirst" border="0" width="18" height="13"/>&nbsp;
                                            <img src="<%=contextPath%>/images/pagnavleft_on.gif" name="pagnavleft" border="0" width="18" height="13"/>
                                          <% } %>

                                          <logic:iterate id="navLinks" name="page" property="pageNumbers">                                                            
                                            <% currLink = ((Integer)navLinks).intValue();%>
                                            <% if (((Integer)navLinks).intValue() == pageNumber) { %>
                                              <b><bean:write name="navLinks"/></b>
                                            <% } else { %>
                                              <% if ((currLink > (prevLink+1))) { %>
                                              ...
                                              <% } %>
                                              <a href="#" onClick="rePaginate('<bean:write name="navLinks"/>','<%=viewPage%>');" class="links2"><bean:write name="navLinks"/></a>
                                            <% } %>
                                            <%prevLink = currLink;%>
                                          </logic:iterate>

                                          <!-- don't display link for next page if the current page is the last page -->
                                          <% if(pageNumber < lastPage) { %>
                                            <a href="#" onClick="rePaginate('<%=pageNumber+1%>','<%=viewPage%>');"><img src="<%=contextPath%>/images/pagnavright_off.gif" name="pagnavright" border="0" width="18" height="13"/></a>&nbsp;
                                            <a href="#" onClick="rePaginate('<%=prevLink%>','<%=viewPage%>');"><img src="<%=contextPath%>/images/pagnavlast_off.gif" width="18" height="13" border="0"></a>
                                          <% } else { %>
                                            <img src="<%=contextPath%>/images/pagnavright_on.gif" width="18" height="13" border="0">&nbsp;
                                            <img src="<%=contextPath%>/images/pagnavlast_on.gif" width="18" height="13" border="0">
                                          <% } %>
                            </td>
                        </tr>
                       <% } %>
		
<!-- END Number of pages -->		   
</table>

   


<!--- END OF BODY -->
</td>
</tr>
</table>
<table width="800">
	<tr><td align="left"><div id="create_btn">
	<logic:equal name="examiner" property="status" value="L" >     
        <input type="button" value="Create" class="button1" onclick="createExaminer();" <%=CREATE_ACCESS%> >&nbsp;
        <input type="button" value="Maintain" class="button1" onclick="viewExaminer();" <%=MAINTAIN_ACCESS%> >
      </logic:equal>
      </div>
    </td></tr>    
</table>     
</div>
</form>
</body>
</html>

