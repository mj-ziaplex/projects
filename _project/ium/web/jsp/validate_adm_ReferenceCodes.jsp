<%@ page language="java" import="com.slocpi.ium.util.*, com.slocpi.ium.ui.util.*, com.slocpi.ium.data.*" buffer="12kb"%>
<%@ taglib uri="/struts-bean.tld" prefix="bean" %>

<!--
var numRecordsPerView;
var numRecords;

function isValidCodeLength() { 
	var form = document.adminForm;
	var referenceCode = form.referenceCode.options[form.referenceCode.selectedIndex].value;

	if ((referenceCode == "<%=IUMConstants.REF_CODE_CLIENT_TYPE%>") ||
		(referenceCode == "<%=IUMConstants.REF_CODE_MIB_NUMBER%>") ||
		(referenceCode == "<%=IUMConstants.REF_CODE_MIB_LETTER%>")) {
		if (form.code.value.length > 1) {
			alert("<bean:message key="error.field.equalto" arg0="Code" arg1="1 character"/>");
			form.code.select();
			form.code.focus();
			return false;
		}
	}
	else if ((referenceCode == "<%=IUMConstants.REF_CODE_LOB%>") ||
		 	 (referenceCode == "<%=IUMConstants.REF_CODE_ACCESS%>")) {
		if (form.code.value.length > 2) {
			alert("<bean:message key="error.field.lessthanequalto" arg0="Code" arg1="2 characters"/>");
			form.code.select();
			form.code.focus();
			return false;
		}
	}
	else if ((referenceCode == "<%=IUMConstants.REF_CODE_DOCUMENT_TYPE%>") ||
			 (referenceCode == "<%=IUMConstants.REF_CODE_EXAM_SPECIALIZATION%>") ||
			 (referenceCode == "<%=IUMConstants.REF_CODE_EXAM_PLACE%>") ||
			 (referenceCode == "<%=IUMConstants.REF_CODE_EXAM_AREA%>") ||
			 (referenceCode == "<%=IUMConstants.REF_CODE_EXAM_RANK%>") ||
			 (referenceCode == "<%=IUMConstants.REF_CODE_MIB_ACTION%>") ||
			 (referenceCode == "<%=IUMConstants.REF_CODE_HOLIDAY%>")) {
		if (form.code.value.length > 3) {
			alert("<bean:message key="error.field.lessthanequalto" arg0="Code" arg1="3 characters"/>");
			form.code.select();
			form.code.focus();
			return false;
		}
	}
	else if (referenceCode == "<%=IUMConstants.REF_CODE_STATUS%>") {
		if (form.code.value.length > 4) {
			alert("<bean:message key="error.field.lessthanequalto" arg0="Code" arg1="4 characters"/>");
			form.code.select();
			form.code.focus();
			return false;
		}  
	}
	else if (referenceCode == "<%=IUMConstants.REF_CODE_TEST_PROFILE%>") {
		if (form.code.value.length > 5) {
			alert("<bean:message key="error.field.lessthanequalto" arg0="Code" arg1="5 characters"/>");
			form.code.select();
			form.code.focus();
			return false;
		}  
	}
	else if (referenceCode == "<%=IUMConstants.REF_CODE_MIB_IMPAIRMENT%>") {
		if (form.code.value.length > 6) {
			alert("<bean:message key="error.field.lessthanequalto" arg0="Code" arg1="6 characters"/>");
			form.code.select();
			form.code.focus();
			return false;
		}  
	}
	else if (referenceCode == "<%=IUMConstants.REF_CODE_DEPARTMENT%>") {
		if (form.code.value.length > 10) {
			alert("<bean:message key="error.field.lessthanequalto" arg0="Code" arg1="10 characters"/>");
			form.code.select();
			form.code.focus();
			return false;
		} 
	}
	else if ((referenceCode == "<%=IUMConstants.REF_CODE_SECTION%>") ||
             (referenceCode == "<%=IUMConstants.REF_CODE_ROLES%>")) {
		if (form.code.value.length > 15) {
			alert("<bean:message key="error.field.lessthanequalto" arg0="Code" arg1="15 characters"/>");
			form.code.select();
			form.code.focus();
			return false;
		} 
	}
	else if (referenceCode == "<%=IUMConstants.REF_CODE_AUTO_ASSIGN_CRITERIA%>") {
		if (form.code.value.length > 20) {
			alert("<bean:message key="error.field.lessthanequalto" arg0="Code" arg1="20 characters"/>");
			form.code.select();
			form.code.focus();
			return false;
		} 
	}
    return true;

}


function isValidDescLength() {
	var form = document.adminForm;
	var referenceCode = form.referenceCode.options[form.referenceCode.selectedIndex].value;

    if ((referenceCode == "<%=IUMConstants.REF_CODE_LOB%>") ||
		(referenceCode == "<%=IUMConstants.REF_CODE_CLIENT_TYPE%>") ||
		(referenceCode == "<%=IUMConstants.REF_CODE_EXAM_PLACE%>") ||
		(referenceCode == "<%=IUMConstants.REF_CODE_EXAM_AREA%>") ||
		(referenceCode == "<%=IUMConstants.REF_CODE_ACCESS_TEMPLATE%>") ||
		(referenceCode == "<%=IUMConstants.REF_CODE_ACCESS%>") ||
		(referenceCode == "<%=IUMConstants.REF_CODE_PAGE%>")) {
		if (form.description.value.length > 25) {
			alert("<bean:message key="error.field.lessthanequalto" arg0="Description" arg1="25 characters"/>");
			form.description.select();
			form.description.focus();
			return false;
		}
	}
	else if ((referenceCode == "<%=IUMConstants.REF_CODE_DOCUMENT_TYPE%>") ||
             (referenceCode == "<%=IUMConstants.REF_CODE_AUTO_ASSIGN_CRITERIA%>") ||
             (referenceCode == "<%=IUMConstants.REF_CODE_EXAM_SPECIALIZATION%>") ||
             (referenceCode == "<%=IUMConstants.REF_CODE_STATUS%>") ||
             (referenceCode == "<%=IUMConstants.REF_CODE_TEST_PROFILE%>") ||
             (referenceCode == "<%=IUMConstants.REF_CODE_EXAM_RANK%>") ||
             (referenceCode == "<%=IUMConstants.REF_CODE_ROLES%>") ||
             (referenceCode == "<%=IUMConstants.REF_CODE_MIB_NUMBER%>") ||
             (referenceCode == "<%=IUMConstants.REF_CODE_MIB_ACTION%>") ||
             (referenceCode == "<%=IUMConstants.REF_CODE_MIB_LETTER%>") ||
             (referenceCode == "<%=IUMConstants.REF_CODE_HOLIDAY%>")) {
		if (form.description.value.length > 40) {
			alert("<bean:message key="error.field.lessthanequalto" arg0="Description" arg1="40 characters"/>");
			form.description.select();
			form.description.focus();
			return false;
		}
	}
	else if ((referenceCode == "<%=IUMConstants.REF_CODE_DEPARTMENT%>") ||
             (referenceCode == "<%=IUMConstants.REF_CODE_SECTION%>")) {
		if (form.description.value.length > 50) {
			alert("<bean:message key="error.field.lessthanequalto" arg0="Description" arg1="50 characters"/>");
			form.description.select();
			form.description.focus();
			return false;
		}
	}
	else if (referenceCode == "<%=IUMConstants.REF_CODE_MIB_IMPAIRMENT%>") {
		if (form.description.value.length > 104) {
			alert("<bean:message key="error.field.lessthanequalto" arg0="Description" arg1="104 characters"/>");
			form.description.select();
			form.description.focus();
			return false;
		}
	}
	return true;
}



function isValid() {
	var form = document.adminForm;
	var referenceCode = form.referenceCode.options[form.referenceCode.selectedIndex].value;
	var flag = form.maintainFlag.value;
	if (flag == "<%=IUMConstants.YES%>") {
		if (referenceCode == "<%=IUMConstants.REF_CODE_REQUIREMENT_FORMS%>") {		
				
			if (noSelection(form.templateFormat.value)) {
				alert("<bean:message key="error.field.required" arg0="Template format"/>");
				form.templateFormat.focus();
				return false;
			}
			if (isEmpty(form.templateName.value)) {
				alert("<bean:message key="error.field.required" arg0="Template name"/>");
				form.templateName.select();
				form.templateName.focus();
				return false;
			}
			if (form.templateName.value.length > 100) {
				alert("<bean:message key="error.field.lessthanequalto" arg0="Template name" arg1="100 characters"/>");
				form.templateName.select();
				form.templateName.focus();
				return false;
			}
		}
	
	}
	
	if (flag != "<%=IUMConstants.YES%>") {
		if (referenceCode == "<%=IUMConstants.REF_CODE_AUTO_ASSIGN%>") {
			if (noSelection(form.code.value)) {
				alert("<bean:message key="error.field.required" arg0="Criteria Code"/>");
				form.code.focus();
				return false;
			}
			if (isEmpty(form.description.value)) {
				alert("<bean:message key="error.field.required" arg0="Criteria Value"/>");
				form.description.select();
				form.description.focus();
				return false;
			}
			if (form.description.value.length > 25) {
				alert("<bean:message key="error.field.lessthanequalto" arg0="Criteria Value" arg1="25 characters"/>");
				form.description.select();
				form.description.focus();
				return false;
			}			
			if (noSelection(form.underwriter.value)) {
				alert("<bean:message key="error.field.required" arg0="Underwriter"/>");
				form.underwriter.focus();
				return false;
			}
		}
	
		if (referenceCode == "<%=IUMConstants.REF_CODE_REQUIREMENT_FORMS%>") {		
			if (isEmpty(form.formName.value)) {
				alert("<bean:message key="error.field.required" arg0="Form name"/>");
				form.formName.select();
				form.formName.focus();
				return false;
			}
			if (form.formName.value.length > 110) {
				alert("<bean:message key="error.field.lessthanequalto" arg0="Form name" arg1="110 characters"/>");
				form.formName.select();
				form.formName.focus();
				return false;
			}				
			if (noSelection(form.templateFormat.value)) {
				alert("<bean:message key="error.field.required" arg0="Template format"/>");
				form.templateFormat.focus();
				return false;
			}
			if (isEmpty(form.templateName.value)) {
				alert("<bean:message key="error.field.required" arg0="Template name"/>");
				form.templateName.select();
				form.templateName.focus();
				return false;
			}
			if (form.templateName.value.length > 100) {
				alert("<bean:message key="error.field.lessthanequalto" arg0="Template name" arg1="100 characters"/>");
				form.templateName.select();
				form.templateName.focus();
				return false;
			}
		}

		if (referenceCode == "<%=IUMConstants.REF_CODE_HOLIDAY%>") {		
			if (isEmpty(form.date.value)) {
				alert("<bean:message key="error.field.required" arg0="Date"/>");
				form.date.select();
				form.date.focus();
				return false;
			}
			if (!isValidDate(form.date.value)) {
				alert("<bean:message key="error.field.format" arg0="Holiday date" arg1="date" arg2=""/>");
				form.date.select();
				form.date.focus();
				return false;
			}	
		}

		if ((referenceCode == "<%=IUMConstants.REF_CODE_LOB%>") ||
			(referenceCode == "<%=IUMConstants.REF_CODE_DEPARTMENT%>") || 
			(referenceCode == "<%=IUMConstants.REF_CODE_SECTION%>") ||
			(referenceCode == "<%=IUMConstants.REF_CODE_EXAM_SPECIALIZATION%>") ||
			(referenceCode == "<%=IUMConstants.REF_CODE_EXAM_PLACE%>") ||
			(referenceCode == "<%=IUMConstants.REF_CODE_EXAM_AREA%>") ||						
			(referenceCode == "<%=IUMConstants.REF_CODE_DOCUMENT_TYPE%>") ||
			(referenceCode == "<%=IUMConstants.REF_CODE_CLIENT_TYPE%>") ||
			(referenceCode == "<%=IUMConstants.REF_CODE_AUTO_ASSIGN_CRITERIA%>") ||
			(referenceCode == "<%=IUMConstants.REF_CODE_STATUS%>") ||
			(referenceCode == "<%=IUMConstants.REF_CODE_ROLES%>") ||
			(referenceCode == "<%=IUMConstants.REF_CODE_TEST_PROFILE%>") ||
			(referenceCode == "<%=IUMConstants.REF_CODE_EXAM_RANK%>") ||
			(referenceCode == "<%=IUMConstants.REF_CODE_MIB_IMPAIRMENT%>") ||
			(referenceCode == "<%=IUMConstants.REF_CODE_MIB_NUMBER%>") ||
			(referenceCode == "<%=IUMConstants.REF_CODE_MIB_ACTION%>") ||
			(referenceCode == "<%=IUMConstants.REF_CODE_MIB_LETTER%>") ||
			(referenceCode == "<%=IUMConstants.REF_CODE_ACCESS%>")) {
			
			if (isEmpty(form.code.value)) {
				alert("<bean:message key="error.field.required" arg0="Code"/>");
				form.code.select();
				form.code.focus();
				return false;
			}
			if (! isValidCodeLength()) {
				form.code.select();
				form.code.focus();
				return false;			
			}

			if ((referenceCode == "<%=IUMConstants.REF_CODE_EXAM_SPECIALIZATION%>") ||
				(referenceCode == "<%=IUMConstants.REF_CODE_EXAM_PLACE%>") ||
				(referenceCode == "<%=IUMConstants.REF_CODE_EXAM_AREA%>") ||
				(referenceCode == "<%=IUMConstants.REF_CODE_STATUS%>") ||
				(referenceCode == "<%=IUMConstants.REF_CODE_TEST_PROFILE%>") ||
				(referenceCode == "<%=IUMConstants.REF_CODE_EXAM_RANK%>") ||
				(referenceCode == "<%=IUMConstants.REF_CODE_ACCESS%>")) {
				if (!isNumeric(form.code.value)) {
					alert("<bean:message key="error.field.numeric" arg0="Code"/>");
					form.code.select();
					form.code.focus();
					return false;
				}				
			}
		}
	}
	
	if ((referenceCode == "<%=IUMConstants.REF_CODE_LOB%>") ||
		(referenceCode == "<%=IUMConstants.REF_CODE_DEPARTMENT%>") || 
		(referenceCode == "<%=IUMConstants.REF_CODE_SECTION%>") ||
		(referenceCode == "<%=IUMConstants.REF_CODE_EXAM_SPECIALIZATION%>") ||
		(referenceCode == "<%=IUMConstants.REF_CODE_EXAM_PLACE%>") ||
		(referenceCode == "<%=IUMConstants.REF_CODE_EXAM_AREA%>") ||						
		(referenceCode == "<%=IUMConstants.REF_CODE_DOCUMENT_TYPE%>") ||
		(referenceCode == "<%=IUMConstants.REF_CODE_CLIENT_TYPE%>") ||
		(referenceCode == "<%=IUMConstants.REF_CODE_AUTO_ASSIGN_CRITERIA%>") ||
		(referenceCode == "<%=IUMConstants.REF_CODE_STATUS%>") ||
		(referenceCode == "<%=IUMConstants.REF_CODE_ROLES%>") ||
		(referenceCode == "<%=IUMConstants.REF_CODE_ACCESS_TEMPLATE%>") ||
		(referenceCode == "<%=IUMConstants.REF_CODE_TEST_PROFILE%>") ||
		(referenceCode == "<%=IUMConstants.REF_CODE_EXAM_RANK%>") ||
		(referenceCode == "<%=IUMConstants.REF_CODE_MIB_IMPAIRMENT%>") ||
		(referenceCode == "<%=IUMConstants.REF_CODE_MIB_NUMBER%>") ||
		(referenceCode == "<%=IUMConstants.REF_CODE_MIB_ACTION%>") ||
		(referenceCode == "<%=IUMConstants.REF_CODE_MIB_LETTER%>") ||
		(referenceCode == "<%=IUMConstants.REF_CODE_HOLIDAY%>") ||
		(referenceCode == "<%=IUMConstants.REF_CODE_ACCESS%>") ||
		(referenceCode == "<%=IUMConstants.REF_CODE_PAGE%>")) {		
		
		if (isEmpty(form.description.value)) {
			alert("<bean:message key="error.field.required" arg0="Description"/>");
			form.description.select();
			form.description.focus();
			return false;
		}
		if (! isValidDescLength()) {
			form.description.select();
			form.description.focus();
			return false;			
		}		
	}

	if ((referenceCode == "<%=IUMConstants.REF_CODE_REQUIREMENT_FORMS%>") ||
		(referenceCode == "<%=IUMConstants.REF_CODE_AUTO_ASSIGN_CRITERIA%>")) {
		
		if (noChecked(form.status)) {
			alert("<bean:message key="error.field.required" arg0="Active indicator"/>");
			form.status[0].select();
			form.status[0].focus();
			return false;
		}
	}

	if (referenceCode == "<%=IUMConstants.REF_CODE_STATUS%>") {
		if (noSelection(form.type.value)) {
			alert("<bean:message key="error.field.required" arg0="Status Type"/>");
			form.type.focus();
			return false;
		}
	}
	
	if (referenceCode == "<%=IUMConstants.REF_CODE_TEST_PROFILE%>") {
		
		if (noChecked(form.type)) {
			alert("<bean:message key="error.field.required" arg0="Type"/>");
			form.type[0].select();
			form.type[0].focus();
			return false;
		}
		if (isEmpty(form.daysValid.value)) {
			alert("<bean:message key="error.field.required" arg0="No. of days valid"/>");
			form.daysValid.select();
			form.daysValid.focus();
			return false;
		}
		if (!isNumeric(form.daysValid.value)) {
			alert("<bean:message key="error.field.numeric" arg0="No. of days valid"/>");
			form.daysValid.select();
			form.daysValid.focus();
			return false;
		}
		if (form.daysValid.value <= 0) {
			alert("<bean:message key="error.field.greaterthan" arg0="No. of days valid" arg1="zero(0)"/>");
			form.daysValid.select();
			form.daysValid.focus();
			return false;
		}
		if (isEmpty(form.followUpNo.value)) {
			alert("<bean:message key="error.field.required" arg0="Follow up no."/>");
			form.followUpNo.select();
			form.followUpNo.focus();
			return false;
		}
		if (!isNumeric(form.followUpNo.value)) {
			alert("<bean:message key="error.field.numeric" arg0="Follow up no."/>");
			form.followUpNo.select();
			form.followUpNo.focus();
			return false;
		}
		if (form.followUpNo.value <= 0) {
			alert("<bean:message key="error.field.greaterthan" arg0="No. of days valid" arg1="zero(0)"/>");
			form.followUpNo.select();
			form.followUpNo.focus();
			return false;
		}
		if (noChecked(form.taxable)) {
			alert("<bean:message key="error.field.required" arg0="Taxable"/>");
			form.taxable.select();
			form.taxable.focus();
			return false;
		}
	}

	if (referenceCode == "<%=IUMConstants.REF_CODE_EXAM_RANK%>") {
		if (isEmpty(form.fee.value)) {
			alert("<bean:message key="error.field.required" arg0="Fee"/>");
			form.fee.focus();
			return false;
		}
		if (parseFloat(form.fee.value) <= parseFloat(0.00)) {
			alert("<bean:message key="error.field.greaterthan" arg0="Fee" arg1="0"/>");
							
			form.fee.focus();
			return false;
		}
	}
	
	return true;
}


function displayList() {
	var form = document.adminForm;
	var referenceCode = form.referenceCode.options[form.referenceCode.selectedIndex].value;

	if (!noSelection(referenceCode)) {
		form.maintainFlag.value = "<%=IUMConstants.NO%>";
		form.pageNo.value = "";
		form.action = "viewReferenceCode.do";
		form.submit();
	}
	else {
		hideBody();
	}
}



function displayHolidayList() {
	var form = document.adminForm;
	var year = form.year.options[form.year.selectedIndex].value;
	var selectedYear = form.selectedYear.value;

	if (noSelection(year)) {
		alert("<bean:message key="error.field.required" arg0="Year"/>");
		form.year.focus();
		
		var size = form.year.length;
		var found = false;
		for (i=0; ((i<size) && (!found)); i++) {
			value = form.year.options[i].value;
			if (selectedYear == value) {
				form.year.selectedIndex = i;
				found = true;
			}
		}		
	}
	else {
		form.maintainFlag.value = "<%=IUMConstants.NO%>";
		form.pageNo.value = "";
		form.action = "viewReferenceCode.do";
		form.submit();
	}
}


function setDefaultFocus() {
	var form = document.adminForm;
	form.referenceCode.focus();
}


function rePaginate (page, actionUrl) {
	form = document.adminForm;
	if (form.pageNo != null) {
		form.pageNo.value = page;
	}
	form.maintainFlag.value = "<%=IUMConstants.NO%>";
	form.action = actionUrl;
	form.submit();
}


function showCreateMaintain(){
	document.all["createMaintain1"].style.display = "block";
	document.all["createMaintain2"].style.display = "block";
}


function hideCreateMaintain(){
	document.all["createMaintain1"].style.display = "none";
	document.all["createMaintain2"].style.display = "none";
}


function showSaveCancel(){ 
	document.all["saveCancel"].style.display = "block";
}


function hideSaveCancel(){ 
	document.all["saveCancel"].style.display = "none";
}


function showBody(){ 
	document.all["body1"].style.display = "block";
	document.all["body2"].style.display = "block";
	showCreateMaintain();
	hideSaveCancel();
}


function hideBody(){ 
	document.all["body1"].style.display = "none";
	document.all["body2"].style.display = "none";
	hideCreateMaintain();
	hideSaveCancel();	
}


function initialize(referenceCode, criteriaCode, numRecordsPerView, numRecords) {
	var form = document.adminForm;

	if ((!isEmpty(referenceCode)) && (referenceCode != "null")) {
		var flag = form.maintainFlag.value;

		if (flag == "<%=IUMConstants.YES%>") {
			hideCreateMaintain();
			showSaveCancel();
			if (referenceCode == "<%=IUMConstants.REF_CODE_AUTO_ASSIGN%>") {
				switchAutoAssignDescription(criteriaCode);
			}
			if (referenceCode == "<%=IUMConstants.REF_CODE_HOLIDAY%>") {
				document.all["holiday_cal"].style.display = "block";
			}		
		}
		else {
			showCreateMaintain();
			hideSaveCancel();
			disableTextBoxes();
			initializeFields();
			this.numRecordsPerView = numRecordsPerView;
			this.numRecords = numRecords;
			putButton(numRecordsPerView, numRecords);
		}
	}
	form.referenceCode.focus();
}


function createRefCode() {
	showSaveCancel();
	hideCreateMaintain();

	enableTextBoxes();
	initializeFields();

	var form = document.adminForm;	
	var referenceCode = form.referenceCode.options[form.referenceCode.selectedIndex].value;
	if (referenceCode == "<%=IUMConstants.REF_CODE_REQUIREMENT_FORMS%>") {
		form.formName.focus();
		form.status[0].checked = true;
	}
	if (referenceCode == "<%=IUMConstants.REF_CODE_TEST_PROFILE%>"){
		form.type[0].checked = true;
		form.taxable[0].checked = true;
	}
	
	if (referenceCode == "<%=IUMConstants.REF_CODE_HOLIDAY%>") {
		form.date.focus();
	}
	if ((referenceCode == "<%=IUMConstants.REF_CODE_LOB%>") ||
		(referenceCode == "<%=IUMConstants.REF_CODE_DEPARTMENT%>") || 
		(referenceCode == "<%=IUMConstants.REF_CODE_SECTION%>") ||
		(referenceCode == "<%=IUMConstants.REF_CODE_EXAM_SPECIALIZATION%>") ||
		(referenceCode == "<%=IUMConstants.REF_CODE_EXAM_PLACE%>") ||
		(referenceCode == "<%=IUMConstants.REF_CODE_EXAM_AREA%>") ||						
		(referenceCode == "<%=IUMConstants.REF_CODE_DOCUMENT_TYPE%>") ||
		(referenceCode == "<%=IUMConstants.REF_CODE_CLIENT_TYPE%>") ||
		(referenceCode == "<%=IUMConstants.REF_CODE_AUTO_ASSIGN_CRITERIA%>") ||		
		(referenceCode == "<%=IUMConstants.REF_CODE_STATUS%>") ||
		(referenceCode == "<%=IUMConstants.REF_CODE_ROLES%>") ||
		(referenceCode == "<%=IUMConstants.REF_CODE_TEST_PROFILE%>") ||
		(referenceCode == "<%=IUMConstants.REF_CODE_EXAM_RANK%>") ||
		(referenceCode == "<%=IUMConstants.REF_CODE_MIB_IMPAIRMENT%>") ||
		(referenceCode == "<%=IUMConstants.REF_CODE_MIB_NUMBER%>") ||
		(referenceCode == "<%=IUMConstants.REF_CODE_MIB_ACTION%>") ||
		(referenceCode == "<%=IUMConstants.REF_CODE_MIB_LETTER%>") ||
		(referenceCode == "<%=IUMConstants.REF_CODE_AUTO_ASSIGN%>") ||
		(referenceCode == "<%=IUMConstants.REF_CODE_ACCESS%>")) {
		form.code.focus();
	}
	else if ((referenceCode == "<%=IUMConstants.REF_CODE_ACCESS_TEMPLATE%>") ||
			 (referenceCode == "<%=IUMConstants.REF_CODE_PAGE%>")) {
		form.description.focus();
	}
}


function maintainRefCode() {
	var form = document.adminForm;
	var idxCode = form.idxCode;
	
	if (idxCode != null) {
		if (idxCode.length != null) {
			if (noChecked(form.idxCode)) {
				alert("<bean:message key="error.field.noselection" arg0="Reference code" arg1=""/>");
			}
			else {
				form.maintainFlag.value = "<%=IUMConstants.YES%>";
				form.action = "viewReferenceCode.do";
				form.submit();
			}
		}
		else {
			if (! form.idxCode.checked) {
				alert("<bean:message key="error.field.noselection" arg0="Reference code" arg1=""/>");
			}
			else {
				form.maintainFlag.value = "<%=IUMConstants.YES%>";
				form.action = "viewReferenceCode.do";
				form.submit();
			}		
		}
	}
}


function saveRefCode() {
	var form = document.adminForm;
	var flag = form.maintainFlag.value;
	
	if (flag == "<%=IUMConstants.YES%>") {
		if (isValid()) {
			form.maintainFlag.value = "<%=IUMConstants.NO%>";
			form.action = "editReferenceCode.do";
			form.submit();
		}
	}
	else {	
		if (isValid()) {
			form.action = "createReferenceCode.do";
			form.submit();
		}
	}
}


function cancelRefCode() {
	var form = document.adminForm;	
	form.maintainFlag.value = "<%=IUMConstants.NO%>";
	if (form.idxCode != null) {
		clearAll(form.idxCode);
	}
	showCreateMaintain();
	hideSaveCancel();
	disableTextBoxes();
	initializeFields();
	putButton(numRecordsPerView, numRecords);
	
	//hide read-only code
	if(document.all["code1"] != null){
		document.all["code1"].style.display = "none";
	}
	if(document.all["code2"] != null){
		document.all["code2"].style.display = "block";
	}
}


function disableTextBoxes() {
	var form = document.adminForm;
	var referenceCode = form.referenceCode.options[form.referenceCode.selectedIndex].value;

	if (referenceCode == "<%=IUMConstants.REF_CODE_REQUIREMENT_FORMS%>") {
		disable(form.formName);
		disable(form.templateFormat);
		disable(form.templateName);
	}
	if (referenceCode == "<%=IUMConstants.REF_CODE_HOLIDAY%>") {
		disable(form.date);
		document.all["holiday_cal"].style.display = "none";
	}
	if ((referenceCode == "<%=IUMConstants.REF_CODE_LOB%>") ||
		(referenceCode == "<%=IUMConstants.REF_CODE_DEPARTMENT%>") || 
		(referenceCode == "<%=IUMConstants.REF_CODE_SECTION%>") ||
		(referenceCode == "<%=IUMConstants.REF_CODE_EXAM_SPECIALIZATION%>") ||
		(referenceCode == "<%=IUMConstants.REF_CODE_EXAM_PLACE%>") ||
		(referenceCode == "<%=IUMConstants.REF_CODE_EXAM_AREA%>") ||						
		(referenceCode == "<%=IUMConstants.REF_CODE_DOCUMENT_TYPE%>") ||
		(referenceCode == "<%=IUMConstants.REF_CODE_CLIENT_TYPE%>") ||
		(referenceCode == "<%=IUMConstants.REF_CODE_AUTO_ASSIGN_CRITERIA%>") ||
		(referenceCode == "<%=IUMConstants.REF_CODE_STATUS%>") ||
		(referenceCode == "<%=IUMConstants.REF_CODE_ROLES%>") ||
		(referenceCode == "<%=IUMConstants.REF_CODE_TEST_PROFILE%>") ||
		(referenceCode == "<%=IUMConstants.REF_CODE_EXAM_RANK%>") ||
		(referenceCode == "<%=IUMConstants.REF_CODE_MIB_IMPAIRMENT%>") ||
		(referenceCode == "<%=IUMConstants.REF_CODE_MIB_NUMBER%>") ||
		(referenceCode == "<%=IUMConstants.REF_CODE_MIB_ACTION%>") ||
		(referenceCode == "<%=IUMConstants.REF_CODE_MIB_LETTER%>") ||
		(referenceCode == "<%=IUMConstants.REF_CODE_AUTO_ASSIGN%>") ||
		(referenceCode == "<%=IUMConstants.REF_CODE_ACCESS%>")) {
		disable(form.code);
	}
	if ((referenceCode == "<%=IUMConstants.REF_CODE_LOB%>") ||
		(referenceCode == "<%=IUMConstants.REF_CODE_DEPARTMENT%>") || 
		(referenceCode == "<%=IUMConstants.REF_CODE_SECTION%>") ||
		(referenceCode == "<%=IUMConstants.REF_CODE_EXAM_SPECIALIZATION%>") ||
		(referenceCode == "<%=IUMConstants.REF_CODE_EXAM_PLACE%>") ||
		(referenceCode == "<%=IUMConstants.REF_CODE_EXAM_AREA%>") ||						
		(referenceCode == "<%=IUMConstants.REF_CODE_DOCUMENT_TYPE%>") ||
		(referenceCode == "<%=IUMConstants.REF_CODE_CLIENT_TYPE%>") ||
		(referenceCode == "<%=IUMConstants.REF_CODE_AUTO_ASSIGN_CRITERIA%>") ||
		(referenceCode == "<%=IUMConstants.REF_CODE_STATUS%>") ||
		(referenceCode == "<%=IUMConstants.REF_CODE_ROLES%>") ||
		(referenceCode == "<%=IUMConstants.REF_CODE_ACCESS_TEMPLATE%>") ||		
		(referenceCode == "<%=IUMConstants.REF_CODE_TEST_PROFILE%>") ||
		(referenceCode == "<%=IUMConstants.REF_CODE_EXAM_RANK%>") ||
		(referenceCode == "<%=IUMConstants.REF_CODE_MIB_IMPAIRMENT%>") ||
		(referenceCode == "<%=IUMConstants.REF_CODE_MIB_NUMBER%>") ||
		(referenceCode == "<%=IUMConstants.REF_CODE_MIB_ACTION%>") ||
		(referenceCode == "<%=IUMConstants.REF_CODE_MIB_LETTER%>") ||
		(referenceCode == "<%=IUMConstants.REF_CODE_HOLIDAY%>") ||
		(referenceCode == "<%=IUMConstants.REF_CODE_AUTO_ASSIGN%>") ||
		(referenceCode == "<%=IUMConstants.REF_CODE_ACCESS%>") ||
		(referenceCode == "<%=IUMConstants.REF_CODE_PAGE%>")) {
		disable(form.description);
	}
	if ((referenceCode == "<%=IUMConstants.REF_CODE_AUTO_ASSIGN_CRITERIA%>") || 
		(referenceCode == "<%=IUMConstants.REF_CODE_REQUIREMENT_FORMS%>")) {
		disable(form.status[0]);
		disable(form.status[1]);
	}
	if (referenceCode == "<%=IUMConstants.REF_CODE_STATUS%>") {
		disable(form.type);
	}
	if (referenceCode == "<%=IUMConstants.REF_CODE_TEST_PROFILE%>") {
		disable(form.type[0]);
		disable(form.type[1]);
		disable(form.daysValid);
		disable(form.followUpNo);
		disable(form.taxable[0]);
		disable(form.taxable[1]);
	}
	if (referenceCode == "<%=IUMConstants.REF_CODE_EXAM_RANK%>") {
		disable(form.fee);
	}
	if (referenceCode == "<%=IUMConstants.REF_CODE_AUTO_ASSIGN%>") {
		disable(form.underwriter);
	}	
}


function enableTextBoxes() {
	var form = document.adminForm;
	var referenceCode = form.referenceCode.options[form.referenceCode.selectedIndex].value;

	if (referenceCode == "<%=IUMConstants.REF_CODE_REQUIREMENT_FORMS%>") {
		enable(form.formName);
		enable(form.templateFormat);
		enable(form.templateName);
	}
	if (referenceCode == "<%=IUMConstants.REF_CODE_HOLIDAY%>") {
		enable(form.date);
		document.all["holiday_cal"].style.display = "block";
	}
	if ((referenceCode == "<%=IUMConstants.REF_CODE_LOB%>") ||
		(referenceCode == "<%=IUMConstants.REF_CODE_DEPARTMENT%>") || 
		(referenceCode == "<%=IUMConstants.REF_CODE_SECTION%>") ||
		(referenceCode == "<%=IUMConstants.REF_CODE_EXAM_SPECIALIZATION%>") ||
		(referenceCode == "<%=IUMConstants.REF_CODE_EXAM_PLACE%>") ||
		(referenceCode == "<%=IUMConstants.REF_CODE_EXAM_AREA%>") ||						
		(referenceCode == "<%=IUMConstants.REF_CODE_DOCUMENT_TYPE%>") ||
		(referenceCode == "<%=IUMConstants.REF_CODE_CLIENT_TYPE%>") ||
		(referenceCode == "<%=IUMConstants.REF_CODE_AUTO_ASSIGN_CRITERIA%>") ||
		(referenceCode == "<%=IUMConstants.REF_CODE_STATUS%>") ||
		(referenceCode == "<%=IUMConstants.REF_CODE_ROLES%>") ||	
		(referenceCode == "<%=IUMConstants.REF_CODE_TEST_PROFILE%>") ||
		(referenceCode == "<%=IUMConstants.REF_CODE_EXAM_RANK%>") ||
		(referenceCode == "<%=IUMConstants.REF_CODE_MIB_IMPAIRMENT%>") ||
		(referenceCode == "<%=IUMConstants.REF_CODE_MIB_NUMBER%>") ||
		(referenceCode == "<%=IUMConstants.REF_CODE_MIB_ACTION%>") ||
		(referenceCode == "<%=IUMConstants.REF_CODE_MIB_LETTER%>") ||
		(referenceCode == "<%=IUMConstants.REF_CODE_AUTO_ASSIGN%>") ||
		(referenceCode == "<%=IUMConstants.REF_CODE_ACCESS%>")) {
		enable(form.code);
	}
	if ((referenceCode == "<%=IUMConstants.REF_CODE_LOB%>") ||
		(referenceCode == "<%=IUMConstants.REF_CODE_DEPARTMENT%>") || 
		(referenceCode == "<%=IUMConstants.REF_CODE_SECTION%>") ||
		(referenceCode == "<%=IUMConstants.REF_CODE_EXAM_SPECIALIZATION%>") ||
		(referenceCode == "<%=IUMConstants.REF_CODE_EXAM_PLACE%>") ||
		(referenceCode == "<%=IUMConstants.REF_CODE_EXAM_AREA%>") ||						
		(referenceCode == "<%=IUMConstants.REF_CODE_DOCUMENT_TYPE%>") ||
		(referenceCode == "<%=IUMConstants.REF_CODE_CLIENT_TYPE%>") ||
		(referenceCode == "<%=IUMConstants.REF_CODE_AUTO_ASSIGN_CRITERIA%>") ||
		(referenceCode == "<%=IUMConstants.REF_CODE_STATUS%>") ||
		(referenceCode == "<%=IUMConstants.REF_CODE_ROLES%>") ||
		(referenceCode == "<%=IUMConstants.REF_CODE_ACCESS_TEMPLATE%>") ||		
		(referenceCode == "<%=IUMConstants.REF_CODE_TEST_PROFILE%>") ||
		(referenceCode == "<%=IUMConstants.REF_CODE_EXAM_RANK%>") ||
		(referenceCode == "<%=IUMConstants.REF_CODE_MIB_IMPAIRMENT%>") ||
		(referenceCode == "<%=IUMConstants.REF_CODE_MIB_NUMBER%>") ||
		(referenceCode == "<%=IUMConstants.REF_CODE_MIB_ACTION%>") ||
		(referenceCode == "<%=IUMConstants.REF_CODE_MIB_LETTER%>") ||
		(referenceCode == "<%=IUMConstants.REF_CODE_HOLIDAY%>") ||
		(referenceCode == "<%=IUMConstants.REF_CODE_AUTO_ASSIGN%>") ||
		(referenceCode == "<%=IUMConstants.REF_CODE_ACCESS%>") ||
		(referenceCode == "<%=IUMConstants.REF_CODE_PAGE%>")) {
		enable(form.description);
	}
	if ((referenceCode == "<%=IUMConstants.REF_CODE_AUTO_ASSIGN_CRITERIA%>") || 
		(referenceCode == "<%=IUMConstants.REF_CODE_REQUIREMENT_FORMS%>")) {
		enable(form.status[0]);
		enable(form.status[1]);
	}
	if (referenceCode == "<%=IUMConstants.REF_CODE_STATUS%>") {
		enable(form.type);
	}
	if (referenceCode == "<%=IUMConstants.REF_CODE_TEST_PROFILE%>") {
		enable(form.type[0]);
		enable(form.type[1]);
		enable(form.daysValid);
		enable(form.followUpNo);		
		enable(form.taxable[0]);
		enable(form.taxable[1]);
	}
	if (referenceCode == "<%=IUMConstants.REF_CODE_EXAM_RANK%>") {
		enable(form.fee);
	}
	if (referenceCode == "<%=IUMConstants.REF_CODE_AUTO_ASSIGN%>") {
		enable(form.underwriter);
	}	
}


function initializeFields() {
	var form = document.adminForm;
	var referenceCode = form.referenceCode.options[form.referenceCode.selectedIndex].value;

	if (referenceCode == "<%=IUMConstants.REF_CODE_REQUIREMENT_FORMS%>") {
		form.formName.value = "";
		form.templateFormat.selectedIndex = 0;
		form.templateName.value = "";
		

		//hide read-only code
		document.all["formName1"].style.display = "none";
		document.all["formName2"].style.display = "block";
	}
	if (referenceCode == "<%=IUMConstants.REF_CODE_HOLIDAY%>") {
		form.date.value = "";
	}	
	
	if ((referenceCode == "<%=IUMConstants.REF_CODE_LOB%>") ||
		(referenceCode == "<%=IUMConstants.REF_CODE_DEPARTMENT%>") || 
		(referenceCode == "<%=IUMConstants.REF_CODE_SECTION%>") ||
		(referenceCode == "<%=IUMConstants.REF_CODE_EXAM_SPECIALIZATION%>") ||
		(referenceCode == "<%=IUMConstants.REF_CODE_EXAM_PLACE%>") ||
		(referenceCode == "<%=IUMConstants.REF_CODE_EXAM_AREA%>") ||						
		(referenceCode == "<%=IUMConstants.REF_CODE_DOCUMENT_TYPE%>") ||
		(referenceCode == "<%=IUMConstants.REF_CODE_CLIENT_TYPE%>") ||
		(referenceCode == "<%=IUMConstants.REF_CODE_AUTO_ASSIGN_CRITERIA%>") ||
		(referenceCode == "<%=IUMConstants.REF_CODE_STATUS%>") ||
		(referenceCode == "<%=IUMConstants.REF_CODE_ROLES%>") ||
		(referenceCode == "<%=IUMConstants.REF_CODE_TEST_PROFILE%>") ||
		(referenceCode == "<%=IUMConstants.REF_CODE_EXAM_RANK%>") ||
		(referenceCode == "<%=IUMConstants.REF_CODE_MIB_IMPAIRMENT%>") ||
		(referenceCode == "<%=IUMConstants.REF_CODE_MIB_NUMBER%>") ||
		(referenceCode == "<%=IUMConstants.REF_CODE_MIB_ACTION%>") ||
		(referenceCode == "<%=IUMConstants.REF_CODE_MIB_LETTER%>") ||
		(referenceCode == "<%=IUMConstants.REF_CODE_ACCESS%>")) {
		form.code.value = "";
		
		//hide read-only code
		document.all["code1"].style.display = "none";
		document.all["code2"].style.display = "block";
	}
	if ((referenceCode == "<%=IUMConstants.REF_CODE_LOB%>") ||
		(referenceCode == "<%=IUMConstants.REF_CODE_DEPARTMENT%>") || 
		(referenceCode == "<%=IUMConstants.REF_CODE_SECTION%>") ||
		(referenceCode == "<%=IUMConstants.REF_CODE_EXAM_SPECIALIZATION%>") ||
		(referenceCode == "<%=IUMConstants.REF_CODE_EXAM_PLACE%>") ||
		(referenceCode == "<%=IUMConstants.REF_CODE_EXAM_AREA%>") ||
		(referenceCode == "<%=IUMConstants.REF_CODE_DOCUMENT_TYPE%>") ||
		(referenceCode == "<%=IUMConstants.REF_CODE_CLIENT_TYPE%>") ||
		(referenceCode == "<%=IUMConstants.REF_CODE_AUTO_ASSIGN_CRITERIA%>") ||
		(referenceCode == "<%=IUMConstants.REF_CODE_STATUS%>") ||
		(referenceCode == "<%=IUMConstants.REF_CODE_ROLES%>") ||
		(referenceCode == "<%=IUMConstants.REF_CODE_ACCESS_TEMPLATE%>") ||		
		(referenceCode == "<%=IUMConstants.REF_CODE_TEST_PROFILE%>") ||		
		(referenceCode == "<%=IUMConstants.REF_CODE_EXAM_RANK%>") ||
		(referenceCode == "<%=IUMConstants.REF_CODE_MIB_IMPAIRMENT%>") ||
		(referenceCode == "<%=IUMConstants.REF_CODE_MIB_NUMBER%>") ||
		(referenceCode == "<%=IUMConstants.REF_CODE_MIB_ACTION%>") ||
		(referenceCode == "<%=IUMConstants.REF_CODE_MIB_LETTER%>") ||		
		(referenceCode == "<%=IUMConstants.REF_CODE_HOLIDAY%>") ||
		(referenceCode == "<%=IUMConstants.REF_CODE_ACCESS%>") ||
		(referenceCode == "<%=IUMConstants.REF_CODE_PAGE%>")) {
		form.description.value = "";

		//hide read-only code		
		var flag = form.maintainFlag.value;
		if (flag == "<%=IUMConstants.YES%>") {
			document.all["code1"].style.display = "none";
		}
	}
	if (referenceCode == "<%=IUMConstants.REF_CODE_AUTO_ASSIGN_CRITERIA%>") {
		form.id.value = "";
	}
	if ((referenceCode == "<%=IUMConstants.REF_CODE_AUTO_ASSIGN_CRITERIA%>") || 
		(referenceCode == "<%=IUMConstants.REF_CODE_REQUIREMENT_FORMS%>")) {
		form.status[0].checked = false;
		form.status[1].checked = false;
	}
	if (referenceCode == "<%=IUMConstants.REF_CODE_STATUS%>") {
		form.type.value = "";
	}
	if (referenceCode == "<%=IUMConstants.REF_CODE_TEST_PROFILE%>") {
		form.type[0].checked = false;
		form.type[1].checked = false;
		form.daysValid.value = "";
		form.followUpNo.value = "";
		form.taxable[0].checked = false;
		form.taxable[1].checked = false;
	}
	if (referenceCode == "<%=IUMConstants.REF_CODE_EXAM_RANK%>") {
		form.fee.value = "";
	}
	if (referenceCode == "<%=IUMConstants.REF_CODE_AUTO_ASSIGN%>") {
		form.code.selectedIndex = 0;
		form.description.value = "";
		form.underwriter.selectedIndex = 0;
		
		switchAutoAssignDescription("");
		
		//hide read-only code
		document.all["criteriaCode1"].style.display = "none";
		document.all["criteriaCode2"].style.display = "block";
	}
}


function limitText(fieldObj, maxChars) {
	var result = true;
	if (fieldObj.value.length >= maxChars) {
		result = false;
	}
	if (window.event) {
		window.event.returnValue = result;
	}
	return result;
}


function validateCurrency(object, fieldName) {
	var value = object.value;
	value = replace(value, ",", "");
	if (fieldName == "Fee") {
		if (parseFloat(value) > 99999999.99) {
			alert("<bean:message key="error.field.lessthan" arg0="Fee" arg1="100,000,000.00"/>");
			object.select();
			object.focus();
		}
		else if (!isCurrency(value)) {
			alert("<bean:message key="error.field.format" arg0="Fee" arg1="currency" arg2=""/>");
			object.select();
			object.focus();
		}
		else {
			object.value = formatCurrency(value);
		}
	}
}


function switchAutoAssignDescription(criteriaCode) {
	var form = document.adminForm;
	var flag = form.maintainFlag.value;
	if (flag == "<%=IUMConstants.YES%>") {	
		//var code = form.idxUpdateCode.value;
		var code = criteriaCode;
	} else {
		var code = form.code.options[form.code.selectedIndex].text;
	}

	if (code == "<%=IUMConstants.CRITERIA_BRANCH%>") {
		//display branch list; hide others
		document.all["desc"].style.display = "none";
		document.all["desc_branch"].style.display = "block";
		document.all["desc_assigned_to"].style.display = "none";
		document.all["desc_agent"].style.display = "none";
		document.all["desc_lob"].style.display = "none";	
	}
	else if (code == "<%=IUMConstants.CRITERIA_AGENT%>") {
		//display assigned to list; hide others
		document.all["desc"].style.display = "none";
		document.all["desc_branch"].style.display = "none";
		document.all["desc_assigned_to"].style.display = "none";
		document.all["desc_agent"].style.display = "block";
		document.all["desc_lob"].style.display = "none";	
	}
	else if (code == "<%=IUMConstants.CRITERIA_ASSIGNED_TO%>") {
		//display agent list; hide others
		document.all["desc"].style.display = "none";
		document.all["desc_branch"].style.display = "none";
		document.all["desc_assigned_to"].style.display = "block";
		document.all["desc_agent"].style.display = "none";
		document.all["desc_lob"].style.display = "none";	
	}
	else if (code == "<%=IUMConstants.CRITERIA_LOB%>") {
		//display lob list; hide others
		document.all["desc"].style.display = "none";
		document.all["desc_branch"].style.display = "none";
		document.all["desc_assigned_to"].style.display = "none";
		document.all["desc_agent"].style.display = "none";
		document.all["desc_lob"].style.display = "block";	
	}
	else {
		//display description textarea; hide others
		document.all["desc"].style.display = "block";
		document.all["desc_branch"].style.display = "none";
		document.all["desc_assigned_to"].style.display = "none";
		document.all["desc_agent"].style.display = "none";
		document.all["desc_lob"].style.display = "none";
	}
}


function setAutoAssignDescription() {
	var form = document.adminForm;
	var code = form.code.options[form.code.selectedIndex].text;

	if (code == "<%=IUMConstants.CRITERIA_BRANCH%>") {
		form.description.value = form.descBranch.options[form.descBranch.selectedIndex].value;
	}
	else if (code == "<%=IUMConstants.CRITERIA_AGENT%>") {
		form.description.value = form.descAgent.options[form.descAgent.selectedIndex].value;	
	}
	else if (code == "<%=IUMConstants.CRITERIA_ASSIGNED_TO%>") {
		form.description.value = form.descAssignedTo.options[form.descAssignedTo.selectedIndex].value;
	}
	else if (code == "<%=IUMConstants.CRITERIA_LOB%>") {
		form.description.value = form.descLob.options[form.descLob.selectedIndex].value;
	}
}


function sortBy(column,sortOrder) {
	var form = document.adminForm;
	resetFilterFields();
	form.sortOrder.value = sortOrder;
	form.columns.value = column;
	form.action = 'viewReferenceCode.do';
	form.submit();		 
}


function resort(sortBy, sortOrder) {
	var form = document.adminForm;
	form.sortBy.value = sortBy;
	form.sortOrder.value = sortOrder;
	form.action = "viewReferenceCode.do";
	form.submit();        
}

function putButton(numRecordsPerView, numRecords) {
  	if (numRecordsPerView >= 10 && numRecords >=10) {
  		document.all["createMaintain2"].style.display = "block";
  	}
  	else{
  	    document.all["createMaintain2"].style.display = "none";
  	}
}
	
//-->