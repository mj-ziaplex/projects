<%@ page language="java" buffer="2048kb"%>
<!-- JAVA UTILS -->
<%@ page import="java.util.ResourceBundle" %>
<!-- IUM UTILS -->
<%@ page import="com.slocpi.ium.util.IUMConstants, com.slocpi.ium.util.UserManager" %>
<!-- IUM POJO -->
<%@ page import="com.slocpi.ium.ui.form.CDSDetailForm" %>
<!-- IUM DATA -->
<%@ page import="com.slocpi.ium.data.UserData, com.slocpi.ium.data.UserProfileData" %>
<!-- IUM UNDERWRITER -->
<%@ page import="com.slocpi.ium.underwriter.AssessmentRequest" %>

<!-- IUM TAGLIBS-->
<%-- <%@ taglib uri="/struts-bean.tld" prefix="bean"%> --%>
<%@ taglib uri="/ium.tld" prefix="ium"%>

<%

/* Andre Ceasar Dacanay - css enhancement - start */
String iumCss = "";
String iumBgColor = "";

try {
	ResourceBundle rbWMSSP = ResourceBundle.getBundle("dbconfig");
	iumCss = rbWMSSP.getString("IUM_CSS_VERSION");
	iumCss = "_"+iumCss;
	
} catch (Exception e){}


if(iumCss.equals("_CP")){
	iumBgColor = "bgcolor=\"#2a4c7c\"";
}else if(iumCss.equals("_GF")){
	iumBgColor = "bgcolor=\"#303A77\"";
}else{
	iumBgColor = "bgcolor=\"#2a4c7c\";";
}
/* Andre Ceasar Dacanay - css enhancement - end */
%>
<%

CDSDetailForm actionForm = (CDSDetailForm) request.getAttribute("detailForm");

String contextPath = request.getContextPath();

// Get Params
String sourceSystem = request.getParameter("sourceSystem");
String lob = request.getParameter("lob");
String lobDesc = request.getParameter("lobDesc");
String agentCode = request.getParameter("agentCode");
String branchCode = request.getParameter("branchCode");
String assignedTo = request.getParameter("assignedTo");
String location = request.getParameter("location");
String underwriter = request.getParameter("underwriter");
String underwriterName = request.getParameter("underwriterName");
String currentStatus = request.getParameter("status");
String selectedStatus = request.getParameter("selectedStatus");

//request details
String refNo = actionForm.getRefNo() == null ? "" : actionForm.getRefNo();
String policySuffix = actionForm.getPolicySuffix() == null ? "" : actionForm.getPolicySuffix();
String amountCovered = actionForm.getAmountCovered() == null ? "" : actionForm.getAmountCovered();
String premium = actionForm.getPremium() == null ? "" : actionForm.getPremium();
String dateCreated = actionForm.getDateCreated() == null ? "" : actionForm.getDateCreated();
String receivedDate = actionForm.getReceivedDate() == null ? "" : actionForm.getReceivedDate();
String dateForwarded = actionForm.getDateForwarded() == null ? "" : actionForm.getDateForwarded();
String dateEdited = actionForm.getDateEdited() == null ? "" : actionForm.getDateEdited();
String agentName = actionForm.getAgentName() == null ? "" : actionForm.getAgentName();
String branchName = actionForm.getBranchName() == null ? "" : actionForm.getBranchName();

//insured client data
String insuredClientNo = request.getParameter("insuredClientNo");
String insuredSex = request.getParameter("insuredSex");

String insuredClientType = actionForm.getInsuredClientType() == null ? "" : actionForm.getInsuredClientType();
String insuredTitle = actionForm.getInsuredTitle() == null ? "" : actionForm.getInsuredTitle();
String insuredFirstName = actionForm.getInsuredFirstName() == null ? "" : actionForm.getInsuredFirstName();
String insuredMiddleName = actionForm.getInsuredMiddleName() == null ? "" : actionForm.getInsuredMiddleName();
String insuredLastName = actionForm.getInsuredLastName() == null ? "" : actionForm.getInsuredLastName();
String insuredSuffix = actionForm.getInsuredSuffix() == null ? "" : actionForm.getInsuredSuffix();
String insuredAge = actionForm.getInsuredAge() == null ? "" : actionForm.getInsuredAge();
String insuredBirthDate = actionForm.getInsuredBirthDate() == null ? "" : actionForm.getInsuredBirthDate();

//owner client data
String ownerClientNo = request.getParameter("ownerClientNo");
String ownerSex = request.getParameter("ownerSex");

String ownerTitle = actionForm.getOwnerTitle() == null ? "" : actionForm.getOwnerTitle();
String ownerFirstName = actionForm.getOwnerFirstName() == null ? "" : actionForm.getOwnerFirstName();
String ownerMiddleName = actionForm.getOwnerMiddleName() == null ? "" : actionForm.getOwnerMiddleName();
String ownerLastName = actionForm.getOwnerLastName() == null ? "" : actionForm.getOwnerLastName();
String ownerSuffix = actionForm.getOwnerSuffix() == null ? "" : actionForm.getOwnerSuffix();
String ownerAge = actionForm.getOwnerAge() == null ? "" : actionForm.getOwnerAge();
String ownerBirthDate = actionForm.getOwnerBirthDate() == null ? "" : actionForm.getOwnerBirthDate();

// Check sourceSystem
String sourceSystemDesc = "";
if(sourceSystem != null && "4".equals(sourceSystem)){
	sourceSystemDesc = "INGENIUM";
}


//filter for assigned to
AssessmentRequest ar = new AssessmentRequest();
String filter = "";
if(currentStatus!=null && selectedStatus!=null && selectedStatus.equals(currentStatus)){
}else{
	filter = ar.retrieveAssignToRolesFilter(currentStatus, selectedStatus, lob);
}

UserData ud = new UserData();
if (filter.equals("")) {
	
    filter = "NONE";
    if(request.getSession().getAttribute("userProfile_"+assignedTo)==null && request.getAttribute("userProfile_"+assignedTo)==null){
    	ud = new UserManager().retrieveUserDetails(assignedTo);
    }else{
    	if(request.getSession().getAttribute("userProfile_"+assignedTo)!=null){
    		ud = (UserData)request.getSession().getAttribute("userProfile_"+assignedTo);
			request.getSession().setAttribute("userProfile_"+assignedTo, ud);
			request.setAttribute("userProfile_"+assignedTo, ud);
    	}else{
    		ud = (UserData)request.getAttribute("userProfile_"+assignedTo);
    	}
    	
    }
} else {
	ud.setProfile(new UserProfileData());
}
request.setAttribute("userData", ud);

ResourceBundle rb = ResourceBundle.getBundle("wmsconfig");
String signatureVerificationURL = rb.getString("signatureVerificationURL");
String wmsContextPath = rb.getString("wms_context_path");
if(policySuffix==null){
	policySuffix = "";
}

String SVLink = signatureVerificationURL + "?PolicyNo=";
String policy = (String) (request.getAttribute("policyNum"));

if (policy == null){
	policy = refNo;

}

String UWPopupLink = contextPath + "/viewUWAssessmentPopup.do?refNo=" + policy;

String TABLE_WIDTH = "";
String LABEL_CLASS = "";
String TXTBOX1_WIDTH = "";
String TXTBOX2_WIDTH = "";

if(contextPath.equalsIgnoreCase(wmsContextPath)){
	TABLE_WIDTH = "400";
	LABEL_CLASS = "label7";
	TXTBOX1_WIDTH = "width:120";
	TXTBOX2_WIDTH = "width:375";
}else{
	TABLE_WIDTH = "100%";
	LABEL_CLASS = "label2";
	TXTBOX1_WIDTH = "width:150";
	TXTBOX2_WIDTH = "width:450";
}
%>

<input type="hidden" name="searchClientType" value="">
<div style="border: solid;border-width:1;position:relative; top: 4px; left:0; width: 100%">
	<table border="0" cellpadding="3" cellspacing="0" width="100%">
    	<tr <%=iumBgColor%>> 
        	<% if (lob.equals(IUMConstants.LOB_INDIVIDUAL_LIFE)) { %>
				<td class="<%=LABEL_CLASS%>" colspan="2" style="color: #FFFFFF" ><b>INDIVIDUAL LIFE POLICY INFORMATION</b></td>
	        <% } else if(lob.equals(IUMConstants.LOB_PRE_NEED)){%>
	        	<td class="<%=LABEL_CLASS%>" colspan="2" style="color: #FFFFFF" ><b>PLAN INFORMATION</b></td>
			<% } else if(lob.equals(IUMConstants.LOB_MUTUAL_FUNDS)){%>
	        	<td class="<%=LABEL_CLASS%>" colspan="2" style="color: #FFFFFF" ><b>MUTUAL FUNDS POLICY INFORMATION</b></td>
	        <% } else if(lob.equals(IUMConstants.LOB_GROUP_LIFE)){%>
	        	<td class="<%=LABEL_CLASS%>" colspan="2" style="color: #FFFFFF" ><b>GROUP LIFE POLICY INFORMATION</b></td>
	        <% } %>
	        
	        <td class="<%=LABEL_CLASS%>">&nbsp;</td>
			<td class="<%=LABEL_CLASS%>">&nbsp;</td>
			<td class="<%=LABEL_CLASS%>">&nbsp;</td>
			<td class="<%=LABEL_CLASS%>">&nbsp;</td>
		</tr>  
		<tr>
			<td class="<%=LABEL_CLASS%>"><b>Reference No.</b></td>
	        <td class="<%=LABEL_CLASS%>">
	        	<%=refNo%>
	        	<%=policySuffix%>
				<input type="hidden" name="refNo" value="<%=refNo%>">
			</td>
			<td class="<%=LABEL_CLASS%>"><b>Amount Covered</b></td>
	        <td class="<%=LABEL_CLASS%>"><%=amountCovered%></td>
	        <td class="<%=LABEL_CLASS%>"><b>Date Created</b></td>
			<td class="<%=LABEL_CLASS%>">
				<%=dateCreated%>
				<input type="hidden" name="dateCreated" value="<%=dateCreated%>">
			</td>
		</tr> 
		<tr>
			<td class="<%=LABEL_CLASS%>"><b>Premium</b></td>
	        <td class="<%=LABEL_CLASS%>">
	        	<%=premium%>
	        	<input type="hidden" name="premium" value="<%=premium%>">
	        </td>
	        <td class="<%=LABEL_CLASS%>"><b>Premium Mode</b></td>
			<td class="<%=LABEL_CLASS%>">&nbsp;</td>
	        <td class="<%=LABEL_CLASS%>"><b>Date Edited</b></td>
	        <td class="<%=LABEL_CLASS%>">
	        	<%=dateEdited %>
	        	<input type="hidden" name="dateEdited" value="<%=dateEdited %>"/>
			</td>
		</tr>
		<tr>
			<td class="<%=LABEL_CLASS%>"><b>Supplemental Benefits</b></td>
			<td></td>
		</tr>
	</table>                	
</div>
<br/>

<%
	String iSelectedDesc = "";
	String oSelectedDesc = "";
	String iSelected = "";
	String oSelected = "";
	if (lob != null) {
		if (lob.equals(IUMConstants.LOB_INDIVIDUAL_LIFE)) {
			iSelected = IUMConstants.CLIENT_TYPE_INSURED;
			oSelected = IUMConstants.CLIENT_TYPE_OWNER;
		} else if (lob.equals(IUMConstants.LOB_GROUP_LIFE)) {
			iSelected = IUMConstants.CLIENT_TYPE_MEMBER;
		} else if (lob.equals(IUMConstants.LOB_PRE_NEED)) {
			iSelected = IUMConstants.CLIENT_TYPE_PLANHOLDER;
		}
	}

	String sameIndValue = "N";
	String sameIndChecked = "";
	if (insuredClientNo.equals(ownerClientNo)) {
		sameIndValue = "Y";
		sameIndChecked = "checked";
	}

	if ("O".equals(iSelected)) {
		iSelectedDesc = "Owner";
	} else if ("I".equals(iSelected)) {
		iSelectedDesc = "Insured";
	} else if ("P".equals(iSelected)) {
		iSelectedDesc = "Planholder";
	} else if ("M".equals(iSelected)) {
		iSelectedDesc = "Member";
	}

	if ("O".equals(oSelected)) {
		oSelectedDesc = "Owner";
	} else if ("I".equals(oSelected)) {
		oSelectedDesc = "Insured";
	} else if ("P".equals(oSelected)) {
		oSelectedDesc = "Planholder";
	} else if ("M".equals(oSelected)) {
		oSelectedDesc = "Member";
	}
%>
<div style="position:relative; width: 100%; top: 0; float: left;">
	<div style="border: solid;border-width:1; float: left; width: 49%; ">
		<table border="0" cellpadding="3" cellspacing="0" width="100%" >
			<tr <%=iumBgColor%>> 
				<td class="<%=LABEL_CLASS%>" colspan="2" style="color: #FFFFFF"><b>Insured Information</b></td>
				<td class="<%=LABEL_CLASS%>">&nbsp;</td>
				<td class="<%=LABEL_CLASS%>">&nbsp;</td>
			</tr>
			<tr > 
				<td class="<%=LABEL_CLASS%>"><b>Client No.</b></td>
				<td class="<%=LABEL_CLASS%>">
					<%=insuredClientNo%>
					<input type="hidden" name="insuredClientNo" value="<%=insuredClientNo%>" onBlur="searchClientData('<%=IUMConstants.CLIENT_TYPE_INSURED%>')" maxlength="10" disabled>
				</td>
				<td class="<%=LABEL_CLASS%>">&nbsp;</td>
			</tr>
			<tr >
				<td class="<%=LABEL_CLASS%>"><b>Name</b></td>
				<td class="<%=LABEL_CLASS%>" colspan="3">
					<%=insuredTitle%>&nbsp;
					<%=insuredFirstName%>&nbsp;
					<%=insuredMiddleName%>&nbsp;
					<%=insuredLastName%>&nbsp;
					<%=insuredSuffix%>
				</td>
			</tr>
			<tr>
				<%if ("C".equals(insuredSex)) {%>
					<td class="<%=LABEL_CLASS%>" >&nbsp;</td>
		            <td class="<%=LABEL_CLASS%>" >&nbsp;</td> 
					<td class="<%=LABEL_CLASS%>" ><input type="radio" name="insuredSex" value="C" style="visibility: hidden;"/></td>
					<td class="<%=LABEL_CLASS%>" >&nbsp;</td>
				<%} else {%>
					<td class="<%=LABEL_CLASS%>" ><b>Gender</b></td>
					<td class="<%=LABEL_CLASS%>" align="left">
						<%if ("M".equals(insuredSex)) {%>
							<input type="radio" name="insuredSex" value="M" checked/>Male&nbsp;&nbsp;&nbsp;
							<input type="radio" name="insuredSex" value="F" disabled="disabled"/>Female
						<%} else {%>
							<input type="radio" name="insuredSex" value="M" disabled="disabled"/>Male&nbsp;&nbsp;&nbsp;
							<input type="radio" name="insuredSex" value="F" checked/>Female  
						<%}%>
						
						<%if ("C".equals(insuredSex)) {%>
							<input type="radio" name="insuredSex" value="C" checked/>Company&nbsp;&nbsp;&nbsp;
						<%}%>
					<td class="<%=LABEL_CLASS%>">&nbsp;</td>
				<%}%>
			</tr>
			<tr >
				<%if ("C".equals(insuredSex)) {%>
					<td class="<%=LABEL_CLASS%>" >&nbsp;</td>
	                <td class="<%=LABEL_CLASS%>" >&nbsp;</td> 
					<td class="<%=LABEL_CLASS%>" >&nbsp;</td>
					<td class="<%=LABEL_CLASS%>" >&nbsp;</td>
				<%} else {%>
					<td class="<%=LABEL_CLASS%>"><b>Birth Date</b></td>
					<td class="<%=LABEL_CLASS%>">
						<%=insuredBirthDate%>
						<input type="hidden" name="insuredBirthDate" value="<%=insuredBirthDate%>" onKeyUp="getKeyDate(event,this);" disabled>&nbsp;
						<div id="req_cal1" style="display:none;position:absolute;"></div>
					</td> 
					<td class="<%=LABEL_CLASS%>"><b>Age</b></td>
					<td class="<%=LABEL_CLASS%>">
						<%=insuredAge%>
						<input type="hidden" name="insuredAge" value="<%=insuredAge%>">
					</td>
				<%}%>
			</tr>
		</table>
	</div>

	<div style="border: solid;border-width:1; float: right; width: 50%; left: 10px;">
		<table border="0" cellpadding="3" cellspacing="0" width="100%">
			<tr <%=iumBgColor%>> 
				<td class="<%=LABEL_CLASS%>" colspan="2" style="color: #FFFFFF"><b>Owner Information</b></td>
	            <td class="<%=LABEL_CLASS%>">&nbsp;</td>
				<td class="<%=LABEL_CLASS%>">&nbsp;</td>	
			</tr>
			<tr > 
				<td class="<%=LABEL_CLASS%>" ><b>Client No.</b></td>
				<td class="<%=LABEL_CLASS%>" >
					<%=ownerClientNo%>
					<input type="hidden" name="ownerClientNo" value="<%=ownerClientNo%>">
				</td>
				<td class="<%=LABEL_CLASS%>">&nbsp;</td>
			</tr>
			<tr >
				<td class="<%=LABEL_CLASS%>"><b>Name</b></td>
	            <td class="<%=LABEL_CLASS%>" colspan="3">
	            	<%=ownerTitle%>&nbsp;
	                <%=ownerFirstName%>&nbsp;
	                <%=ownerMiddleName%>&nbsp;
	                <%=ownerLastName%>&nbsp;
	                <%=ownerSuffix%>
				</td>
			</tr>
			<tr>
				<%if ("C".equals(ownerSex)) {%>
					<td class="<%=LABEL_CLASS%>" >&nbsp;</td>
		            <td class="<%=LABEL_CLASS%>" >&nbsp;</td> 
					<td class="<%=LABEL_CLASS%>" ><input type="radio" name="ownerSex" value="C" style="visibility: hidden;"/></td>
					<td class="<%=LABEL_CLASS%>" >&nbsp;</td>
				<%} else {%>
					<td class="<%=LABEL_CLASS%>" ><b>Gender</b></td>
					<td class="<%=LABEL_CLASS%>" align="left" >
						<%if ("M".equals(ownerSex)) {%>
							<input type="radio" name="ownerSex" value="M" checked/>Male&nbsp;&nbsp;&nbsp;
							<input type="radio" name="ownerSex" value="F" disabled="disabled"/>Female
						<%} else {%>
							<input type="radio" name="ownerSex" value="M" disabled="disabled"/>Male&nbsp;&nbsp;&nbsp;
							<input type="radio" name="ownerSex" value="F" checked/>Female
						<%}%>
						
						<%if ("C".equals(ownerSex)) {%>
							<input type="radio" name="ownerSex" value="C" checked/>Company&nbsp;&nbsp;&nbsp;
						<%}%>
					</td>
					<td class="<%=LABEL_CLASS%>">&nbsp;</td>
				<%}%>
	        </tr>
			<tr>
				<%if ("C".equals(ownerSex)) {%>              
					<td class="<%=LABEL_CLASS%>" >&nbsp;</td>
	                <td class="<%=LABEL_CLASS%>" >&nbsp;</td> 
					<td class="<%=LABEL_CLASS%>" >&nbsp;</td>
					<td class="<%=LABEL_CLASS%>" >&nbsp;</td>
				<%} else {%>
					<td class="<%=LABEL_CLASS%>"><b>Birth Date</b></td>
	                <td class="<%=LABEL_CLASS%>">
	                	<%=ownerBirthDate%>
	                	<input type="hidden" name="ownerBirthDate" value="<%=ownerBirthDate%>" disabled>&nbsp;
	                	<div id="req_cal2" style="display:none;position:absolute;"></div>
	                </td> 
					<td class="<%=LABEL_CLASS%>"><b>Age</b></td>
					<td class="<%=LABEL_CLASS%>">
						<%=ownerAge%>
						<input type="hidden" name="ownerAge" value="<%=ownerAge%>">
					</td>
				<%}%>
			</tr> 
		</table>
	</div>
</div>
<br/>

<div style="border: solid;border-width:1;position:relative; top:10px; left:0; width: 100%; float: left;">
	<table border="0" cellpadding="3" cellspacing="0" width="100%">
		<tr>
			<td class="<%=LABEL_CLASS%>" width="10%"><b>Agent</b></td>
			<td class="<%=LABEL_CLASS%>" width="30%">
				<%=agentName%>
				<input type="hidden" name="agentCode" value="<%=agentCode%>">&nbsp;&nbsp;&nbsp;&nbsp;
			 	<input class="button1" type="button" name="btnUpdateAgent" value="sync" onclick="updateAgent();">
			</td>
			<td class="<%=LABEL_CLASS%>" width="10%"><b>Branch</b></td>
			<td class="<%=LABEL_CLASS%>" width="40%">
				<%=branchName%>
				<input type="hidden" name="branchCode" value="<%=branchCode%>">
			</td>
		</tr>
	</table>
</div>       

<div style="border: solid;border-width:1;position:absolute; top:200; left:30 ; visibility:hidden">  
 	<input type="hidden" name="searchClientType" value="">
    
    <table border="0" cellpadding="3" cellspacing="0" width="<%=TABLE_WIDTH%>">
		<tr>
        	<td class="<%=LABEL_CLASS%>"><b>&nbsp;&nbsp;Reference No.</b></td>
            <td class="<%=LABEL_CLASS%>">
            	<%=refNo%><%=policySuffix%>
            	<input type="hidden" name="refNo" value="<%=refNo%>">
            </td>
            <td class="<%=LABEL_CLASS%>">&nbsp;</td>
            <td class="<%=LABEL_CLASS%>"><b>&nbsp;&nbsp;Admin Sys Source</b></td>
            
            <%if(!"".equals(sourceSystemDesc)) {%>
            	<td class="<%=LABEL_CLASS%>">
            		<%=sourceSystemDesc%>
            		<input type="hidden" name="sourceSystem" value="<%=sourceSystem%>">
            	</td>
            <%} else {%>
            	<td class="<%=LABEL_CLASS%>">
            		<ium:description type="<%=IUMConstants.LIST_SOURCE_SYSTEM%>" queryCode="<%=sourceSystem%>"/>
            		<input type="hidden" name="sourceSystem" value="<%=sourceSystem%>">
            	</td>
            <%}%>
            
            <td class="<%=LABEL_CLASS%>">&nbsp;</td>
            <td class="<%=LABEL_CLASS%>"><b>&nbsp;&nbsp;Date Created</b></td>
            <td class="<%=LABEL_CLASS%>">
            	<%=dateCreated%>
            	<input type="hidden" name="dateCreated" value="<%=dateCreated%>">
            </td>
		</tr>
        <tr>
			<td class="<%=LABEL_CLASS%>"><b>&nbsp;&nbsp;Agent</b></td>
            <td class="<%=LABEL_CLASS%>">
            	<%=agentName%>
            	<input type="hidden" name="agentCode" value="<%=agentCode%>">
            </td>
            <td class="<%=LABEL_CLASS%>">&nbsp;</td>
            <td class="<%=LABEL_CLASS%>"><b>&nbsp;&nbsp;Branch</b></td>
			<td class="<%=LABEL_CLASS%>" colspan="4">
				<%=branchName%>
				<input type="hidden" name="branchCode" value="<%=branchCode%>">
			</td>
		</tr>
		<tr>
        	<td class="<%=LABEL_CLASS%>" colspan="8">&nbsp;</td>
        </tr>
		<tr>
			<td class="<%=LABEL_CLASS%>"><b>&nbsp;&nbsp;LOB</b></td>
            <td class="<%=LABEL_CLASS%>">
            	<%=lobDesc%>
            	<input type="hidden" name="lob" value="<%=lob%>">
            </td>
            <td class="<%=LABEL_CLASS%>">&nbsp;</td>    
            <td class="<%=LABEL_CLASS%>"><b>&nbsp;&nbsp;Amount Covered</b></td>
            <td class="<%=LABEL_CLASS%>">
            	<%=amountCovered%>
            	<input type="hidden" name="amountCovered" value="<%=amountCovered%>">
            </td>
            <td class="<%=LABEL_CLASS%>">&nbsp;</td>    
            <td class="<%=LABEL_CLASS%>"><b>&nbsp;&nbsp;Premium</b></td>
            <td class="<%=LABEL_CLASS%>">
            	<%=premium%>
            	<input type="hidden" name="premium" value="<%=premium%>">
            </td>
		</tr>                    
		<tr>
			<td class="<%=LABEL_CLASS%>"><b>&nbsp;&nbsp;Date Received</b></td>
            <td class="<%=LABEL_CLASS%>">
            	<%=receivedDate%>
            	<input type="hidden" name="receivedDate" value="<%=receivedDate%>">
            </td>
            <td class="<%=LABEL_CLASS%>">&nbsp;</td>
            
            <% if ((Long.parseLong(currentStatus) == IUMConstants.STATUS_FOR_TRANSMITTAL_USD) ||
            		(Long.parseLong(currentStatus) == IUMConstants.STATUS_AWAITING_REQUIREMENTS) || 
                    (Long.parseLong(currentStatus) == IUMConstants.STATUS_AWAITING_MEDICAL)) { %> 
				<td class="<%=LABEL_CLASS%>"><span class="required">*</span><b>Date Forwarded</b></td>
                <td class="<%=LABEL_CLASS%>" width="200">
                	<input type="text" name="dateForwarded" class="<%=LABEL_CLASS%>" style="width:150" value="<%=dateForwarded%>" onKeyUp="getKeyDate(event,this);" disabled/>&nbsp;
                	<div id="req_cal3" style="display:none;position:absolute;">
                		<a href="#" onClick="popUpWin('<%=contextPath%>/jsp/calendar.jsp?textbox=requestForm.dateForwarded',290,155);">
                			<img src="<%=contextPath%>/images/calendar.gif" border="0">
                		</a>
                	</div>
                </td>
            <% } else { %> 
		        <td class="<%=LABEL_CLASS%>"><b>&nbsp;&nbsp;Date Forwarded</b></td>
				<td class="<%=LABEL_CLASS%>">
					<%=dateForwarded%>
					<input type="hidden" name="dateForwarded" value="<%=dateForwarded%>">
				</td>
            <% } %>
            
			<td class="<%=LABEL_CLASS%>">&nbsp;</td>
            <td class="<%=LABEL_CLASS%>"><b>&nbsp;&nbsp;Date Edited</b></td>
            <td class="<%=LABEL_CLASS%>">
            	<%=dateEdited%>
            	<input type="hidden" name="dateEdited" value="<%=dateEdited%>">
            </td>
        </tr>
        <tr>
        	<td class="<%=LABEL_CLASS%>"><b>&nbsp;&nbsp;Location</b></td>

            <% if ((Long.parseLong(currentStatus) == IUMConstants.STATUS_APPROVED) ||
            		(Long.parseLong(currentStatus) == IUMConstants.STATUS_AR_CANCELLED) ||
                    (Long.parseLong(currentStatus) == IUMConstants.STATUS_DECLINED) ||
                    (Long.parseLong(currentStatus) == IUMConstants.STATUS_NOT_PROCEEDED_WITH)) { %>
				<td class="<%=LABEL_CLASS%>">
					<ium:description type="<%=IUMConstants.LIST_USERS%>" queryCode="<%=location%>"/>
					<input type="hidden" name="location" value="<%=location%>">
				</td>
            <% } %>
            
            <td class="<%=LABEL_CLASS%>">&nbsp;</td>
            <td class="<%=LABEL_CLASS%>"><span class="required">*</span><b>Assigned to</b></td>
            <td class="<%=LABEL_CLASS%>">
            	<ium:list className="label2" listBoxName="assignedTo" type="<%=IUMConstants.LIST_ASSIGN_TO%>" styleName="width:150" selectedItem="<%=assignedTo%>" disabled="true" filter="<%=filter%>"/>
            </td>
            <td class="<%=LABEL_CLASS%>">&nbsp;</td>
            <td class="<%=LABEL_CLASS%>"><b>&nbsp;&nbsp;Underwriter</b></td>
            <td class="<%=LABEL_CLASS%>">
            	<%=underwriterName%>
            	<input type="hidden" name="underwriter" value="<%=underwriter%>">
            </td>
        </tr>           
        <tr>
        	<td class="<%=LABEL_CLASS%>" colspan="8">&nbsp;</td>
        </tr>
        
        <!-- ************************************************* -->
        <!-- ******************** INSURED ******************** -->
        <!-- ************************************************* -->
        <tr id="insured1">
			<td class="<%=LABEL_CLASS%>"><b>&nbsp;&nbsp;Client Type</b></td>
            
			<%if (lob.equals(IUMConstants.LOB_INDIVIDUAL_LIFE)) {%>
            	<td class="<%=LABEL_CLASS%>">
            		<select class="label2" name="insuredClientType" style="width:150" onChange="switchClient(this.value);">
            			<option value="">Select Client Type</option>
            			<option value="O">Owner</option>
            			<option value="I" selected>Insured</option>
            		</select>
            	</td>
            <%} else {%>
            	<td class="<%=LABEL_CLASS%>"><%=iSelectedDesc%>
            		<input type="hidden" name="insuredClientType" value="<%=insuredClientType%>">
            	</td>
            <%}%>
            
            <td class="<%=LABEL_CLASS%>">&nbsp;</td>
            <td class="<%=LABEL_CLASS%>"><b>&nbsp;&nbsp;Client No.</b></td>
            <td class="<%=LABEL_CLASS%>">
            	<%=insuredClientNo%>
            	<input type="hidden" name="insuredClientNo" value="<%=insuredClientNo%>">
            	</td>
            <td class="<%=LABEL_CLASS%>">&nbsp;</td>
            
            <% if (lob.equals(IUMConstants.LOB_INDIVIDUAL_LIFE)) { %>
            	<td class="<%=LABEL_CLASS%>" colspan="2">&nbsp;<input type="checkbox" name="sameClientDataInd" value="<%=sameIndValue%>" <%=sameIndChecked%> disabled> Same insured and owner</td>
            <% } else { %>
            	<td class="<%=LABEL_CLASS%>" colspan="2">&nbsp;</td>
            <% } %>
		</tr>
        <tr id="insured2">
        	<td class="<%=LABEL_CLASS%>"><b>&nbsp;&nbsp;Last Name</b></td>
            <td class="<%=LABEL_CLASS%>">
            	<%=insuredLastName%>
            	<input type="hidden" name="insuredLastName" value="<%=insuredLastName%>">
            </td> 
            <td class="<%=LABEL_CLASS%>">&nbsp;</td>
            <td class="<%=LABEL_CLASS%>"><b>&nbsp;&nbsp;First Name</b></td>
            <td class="<%=LABEL_CLASS%>">
            	<%=insuredFirstName%>
            	<input type="hidden" name="insuredFirstName" value="<%=insuredFirstName%>">
            </td>
            <td class="<%=LABEL_CLASS%>">&nbsp;</td>
            <td class="<%=LABEL_CLASS%>"><b>&nbsp;&nbsp;Middle Name</b></td>
            <td class="<%=LABEL_CLASS%>">
            	<%=insuredMiddleName%>
            	<input type="hidden" name="insuredMiddleName" value="<%=insuredMiddleName%>">
            </td>
        </tr>
        <tr id="insured3">
        	<td class="<%=LABEL_CLASS%>"><b>&nbsp;&nbsp;Title</b></td>
            <td class="<%=LABEL_CLASS%>">
            	<%=insuredTitle%>
            	<input type="hidden" name="insuredTitle" value="<%=insuredTitle%>">
            </td> 
            <td class="<%=LABEL_CLASS%>">&nbsp;</td>
            <td class="<%=LABEL_CLASS%>"><b>&nbsp;&nbsp;Suffix</b></td>
            <td class="<%=LABEL_CLASS%>">
            	<%=insuredSuffix%>
            	<input type="hidden" name="insuredSuffix" value="<%=insuredSuffix%>">
            </td>
            <td class="<%=LABEL_CLASS%>">&nbsp;</td>
            <td class="<%=LABEL_CLASS%>"><b>&nbsp;&nbsp;Birth Date</b></td>
            <td class="<%=LABEL_CLASS%>">
            	<%=insuredBirthDate%>
            	<input type="hidden" name="insuredBirthDate" value="<%=insuredBirthDate%>">
            </td>
        </tr>
        <tr id="insured4">
        	<td class="<%=LABEL_CLASS%>"><b>&nbsp;&nbsp;Age</b></td>
            <td class="<%=LABEL_CLASS%>">
            	<%=insuredAge%>
            	<input type="hidden" name="insuredAge" value="<%=insuredAge%>">
            </td>
            <td class="<%=LABEL_CLASS%>">&nbsp;</td>
            <td class="<%=LABEL_CLASS%>"><b>&nbsp;&nbsp;Gender</b></td>
            <td class="<%=LABEL_CLASS%>" align="left">                        
            	<input type="radio" name="insuredSex4" value="M" disabled>Male</input>&nbsp;&nbsp;&nbsp;
                <input type="radio" name="insuredSex4" value="F" disabled>Female</input>                        
            </td>
            <td class="<%=LABEL_CLASS%>">&nbsp;</td>
            
			<% if(contextPath.equalsIgnoreCase(wmsContextPath)) { %>
            	<td class="<%=LABEL_CLASS%>" colspan="2">
            		&nbsp;&nbsp;
            		<a href="<%= SVLink %><%=refNo%><%=policySuffix%>" target="_blank">
            			Signature Verification
            		</a>
            	</td> 
			<% } %>                        
        </tr>
        
        <!-- *********************************************** -->
        <!-- ******************** OWNER ******************** -->
        <!-- *********************************************** -->
        <% if (lob.equals(IUMConstants.LOB_INDIVIDUAL_LIFE)) { %>
			<tr id="owner1" style="display:none">
				<td class="<%=LABEL_CLASS%>"><b>&nbsp;&nbsp;Client Type</b></td>
                <td class="<%=LABEL_CLASS%>">
                	<select class="label2" name="ownerClientType" style="width:150" onChange="switchClient(this.value);">
                		<option value="">Select Client Type</option>
                		<option value="O" selected>Owner</option>
                		<option value="I">Insured</option>
                	</select>
                </td>
                <td class="<%=LABEL_CLASS%>">&nbsp;</td>
                <td class="<%=LABEL_CLASS%>"><b>&nbsp;&nbsp;Client No.</b></td>
                <td class="<%=LABEL_CLASS%>" colspan="4">
                	<%=ownerClientNo%>
                	<input type="hidden" name="ownerClientNo" value="<%=ownerClientNo%>">
                </td>
            </tr>
            <tr id="owner2" style="display:none">
            	<td class="<%=LABEL_CLASS%>"><b>&nbsp;&nbsp;Last Name</b></td>
                <td class="<%=LABEL_CLASS%>">
                	<%=ownerLastName%>
                	<input type="hidden" name="ownerLastName" value="<%=ownerLastName%>">
                </td> 
                <td class="<%=LABEL_CLASS%>">&nbsp;</td>
                <td class="<%=LABEL_CLASS%>"><b>&nbsp;&nbsp;First Name</b></td>
                <td class="<%=LABEL_CLASS%>">
                	<%=ownerFirstName%>
                	<input type="hidden" name="ownerFirstName" value="<%=ownerFirstName%>">
                </td>
                <td class="<%=LABEL_CLASS%>">&nbsp;</td>
                <td class="<%=LABEL_CLASS%>"><b>&nbsp;&nbsp;Middle Name</b></td>
                <td class="<%=LABEL_CLASS%>">
                	<%=ownerMiddleName%>
                	<input type="hidden" name="ownerMiddleName" value="<%=ownerMiddleName%>">
                </td>
            </tr>
            <tr id="owner3" style="display:none">
            	<td class="<%=LABEL_CLASS%>"><b>&nbsp;&nbsp;Title</b></td>
                <td class="<%=LABEL_CLASS%>">
                	<%=ownerTitle%>
                	<input type="hidden" name="ownerTitle" value="<%=ownerTitle%>">
                </td> 
                <td class="<%=LABEL_CLASS%>">&nbsp;</td>
                <td class="<%=LABEL_CLASS%>"><b>&nbsp;&nbsp;Suffix</b></td>
                <td class="<%=LABEL_CLASS%>">
                	<%=ownerSuffix%>
                	<input type="hidden" name="ownerSuffix" value="<%=ownerSuffix%>">
                </td>
                <td class="<%=LABEL_CLASS%>">&nbsp;</td>
                <td class="<%=LABEL_CLASS%>"><b>&nbsp;&nbsp;Birth Date</b></td>
                <td class="<%=LABEL_CLASS%>">
                	<%=ownerBirthDate%>
                	<input type="hidden" name="ownerBirthDate" value="<%=ownerBirthDate%>">
                </td>
            </tr>
            
            <%
			String sex2 = ownerSex;
            String sexChecked_M2 = "";
            String sexChecked_F2 = "";

			if (sex2 != null) {
            	if (sex2.equals("M")) {
                	sexChecked_M2 = "checked";
                } else if (sex2.equals("F")) {
                	sexChecked_F2 = "checked";
                }
            }
            %>
            
            <tr id="owner4" style="display:none">
            	<td class="<%=LABEL_CLASS%>"><b>&nbsp;&nbsp;Age</b></td>
                <td class="<%=LABEL_CLASS%>">
                	<%=ownerAge%>
                	<input type="hidden" name="ownerAge" value="<%=ownerAge%>">
                </td>
                <td class="<%=LABEL_CLASS%>">&nbsp;&nbsp;Sex&nbsp;<%=sex2%>&nbsp;<%=sexChecked_F2%></td>
                <td class="<%=LABEL_CLASS%>"><b>&nbsp;&nbsp;Gender</b></td>
                <td class="<%=LABEL_CLASS%>" align="left">                        
                	<input type="radio" name="ownerSex4" value="M" <%=sexChecked_M2%> disabled>Male</input>&nbsp;&nbsp;&nbsp;
                    <input type="radio" name="ownerSex4" value="F" <%=sexChecked_F2%> disabled>Female</input>                        
                </td>
                <td class="<%=LABEL_CLASS%>">&nbsp;</td>
                
				<% if(contextPath.equalsIgnoreCase(wmsContextPath)){ %>
                	<td class="<%=LABEL_CLASS%>" colspan="2">
                		&nbsp;&nbsp;
                		<a href="<%= SVLink %><%=refNo%><%=policySuffix%>" target="_blank">
                			Signature Verification
                		</a>
                	</td> 
                <% } %>                        
            </tr>
		<% } %>
 		
 		<tr>
			<td>&nbsp;</td>
			<td width="150">&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td width="150">&nbsp;</td>
			<td>&nbsp;</td>
			
			<% if(contextPath.equalsIgnoreCase(wmsContextPath)){ %>
            	<td class="<%=LABEL_CLASS%>" colspan="8">
            		&nbsp;&nbsp;
            		<a href="javascript:uwPopup('<%=UWPopupLink%>');">UW Assessment Analysis</a>
            	</td>
			<% } %>
        </tr>
	</table>
</div>

<script language="Javascript" type="text/javascript">
function uwPopup(url){
	var uwPop = window.open(url, 'popup', "top=180 left=410 width=700 height=310 scrollbars=yes");
}
</script>

<!-- <bean:define id="userProfile" name="userData" property="profile" /> -->
<script language="Javascript" type="text/javascript">
// 	frm = document.forms[0];
	
//    	if (frm.assignedTo.length == 1) {  // check if assigned to has no value ("Select User" is the only option)
// 		frm.assignedTo.options.add(new Option("<bean:write name='userProfile' property='lastName' />, <bean:write name='userProfile' property='firstName' />", "<bean:write name='userProfile' property='userId' />"));
// 		frm.assignedTo.options.selectedIndex = 1; // force the selection of the assigned to user
// 	}
</script>
  