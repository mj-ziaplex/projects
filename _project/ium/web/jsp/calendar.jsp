<%String contextPath = request.getContextPath();%>
<html>
  <head>
    <title>Integrated Underwriting and Medical System</title>
    <link href="<%=contextPath%>/css/main.css" rel="stylesheet" type="text/css">
    <script language="Javascript" src="<%=contextPath%>/js/navigation.js"></script>
    <script language="Javascript" src="<%=contextPath%>/js/calendar.js"></script>
  </head>
  <% String obj = request.getParameter("textbox"); %>
  <body bgcolor="#EEEEEE" onLoad="showCalendar('cal', '<%=obj%>');">
    <div id="cal" style="position:absolute;visibility:visible"></div>
  </body>
</html>
