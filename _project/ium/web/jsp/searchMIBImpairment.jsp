<%@ page language="java" import="java.util.*,com.slocpi.ium.data.*, com.slocpi.ium.ui.form.*, com.slocpi.ium.underwriter.* , com.slocpi.ium.util.*, com.slocpi.ium.ui.util.*, java.text.* " buffer="12kb"%>
<%@ taglib uri="/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/struts-html.tld" prefix="html" %>
<%@ taglib uri="/ium.tld" prefix="ium" %>
<%
/* Andre Ceasar Dacanay - css enhancement - start */
String iumCss = "";
String iumColorB = "";
String companyCode = "";
try {
	ResourceBundle rbWMSSP = ResourceBundle.getBundle("dbconfig");
	iumCss = rbWMSSP.getString("IUM_CSS_VERSION");
	companyCode = iumCss;
	iumCss = "_"+iumCss;
} catch (Exception e){}
if(iumCss.equals("_CP")){
	iumColorB="#2A4C7C";
	companyCode="";
}else if(iumCss.equals("_GF")){
	iumColorB="#303A77";
	companyCode = companyCode+" ";
}else{
	iumColorB="#2A4C7C";
}
/* Andre Ceasar Dacanay - css enhancement - end */
%>
<%
SearchMIBImpairmentsForm requirement = request.getAttribute("requirement")!=null ? (SearchMIBImpairmentsForm)request.getAttribute("requirement") : new SearchMIBImpairmentsForm(); 
String contextPath = request.getContextPath();
String arFormRefNo = request.getParameter("arFormRefNo")!=null ? request.getParameter("arFormRefNo") :"";
String sourceSystem = request.getParameter("sourceSystem")!=null ? request.getParameter("sourceSystem") :"";
String policySuffix = request.getParameter("policySuffix")!=null ? request.getParameter("policySuffix") :"";
String code = request.getAttribute("code")!=null ? (String)request.getAttribute("code") :"";
String desc = request.getAttribute("desc")!=null ? (String)request.getAttribute("desc") :"";
int pageNumber = 1;

if(arFormRefNo.equals("")){
	arFormRefNo = (String)request.getAttribute("arFormRefNo");
}

 %>    
<html>

<head>
<title><%=companyCode%>IUM&nbsp;&nbsp;<%=arFormRefNo%>&nbsp;<%=policySuffix%>&nbsp;&nbsp;Search MIB Impairments</title>
<link href="<%=contextPath%>/css/pw_main.css"     rel="stylesheet" type="text/css">
<link href="<%=contextPath%>/css/main<%=iumCss%>.css" rel="stylesheet" type="text/css">
<link href="<%=contextPath%>/css/styleCommon.css" rel="stylesheet" type="text/css">
<script language="JavaScript" src="<%=contextPath%>/js/validation.js"></script>
<script language="JavaScript" src="<%=contextPath%>/js/dhtml_init.js"></script>
<script language="JavaScript" src="<%=contextPath%>/js/navigation.js"></script>
<script language="JavaScript" src="<%=contextPath%>/jsp/sl_left_nav.jsp"></script>
<script language="JavaScript">

<%if(arFormRefNo.equals("")){%>
	document.frm.refNo.value = opener.document.getElementById('arFormRefNo').value;
//alert("document.frm.refNo.value "+document.frm.refNo.value);
<%}%>

function rePaginate( pageNum, target){
    var frm = document.frm;
		frm.pageNumber.value = pageNum;
		frm.action=target;
		frm.submit();
}

function trimTextArea(){
   var frm= document.frm;
	 if(frm.reqDesc.value.length>70){
	    frm.reqDesc.value=frm.reqDesc.value.substring(0,70);
	 }

}




</script>
</head>
<BODY leftMargin="0" topmargin="0" marginwidth="0" marginheight="0" onUnload="closePopUpWindow()">
<script type="text/javascript">

function replaceStr(str){
	str = str.replace(/&#39;/g,"\'");
	return str;
}

function searchMIBImpairment(){
	var form = document.frm;
	var code = form.code.value;
	var desc = form.desc.value;
	var pageNumber = 1;
	//pageNumber = form.pageNumber.value;
	form.action  = "<%=contextPath%>/searchMIBImpairment.do?mode=search&code="+code+"&desc="+desc+"&pageNumber="+pageNumber;
	form.submit();
}

function clearFields(){
	document.frm.code.value="";
	document.frm.desc.value="";
}

function setMIBImpairmentToOpener(code,val){
	window.opener.document.requestForm.code.value=code;
	window.close();
}
</script>
<form name="frm"  method="post">    
<table border="0" cellpadding="0" cellspacing="0" >
	<tr>								
	    <td width="100%"> 
	     	<div align="left"> 	 	       	
   	 				<table width="100%" cellpadding="10" cellspacing="2" border="0">
	      				<tr valign="top">
     						<td class="title2">Search MIB Impairments
							<!--Start of form -->
      						<table border="1" bordercolor="<%=iumColorB%>" cellpadding="5" cellspacing="0" bgcolor="" width="500" class="listtable1">
       							<tr>
       								<td colspan="2" height="100%">
					   				    <table border="0" width="500" cellpadding="2" cellspacing="2" class="listtable1">
									        <tr>
											  <html:errors/>									        
									          <td class="label2" width="20%" align="left"><b>Code</b></td>
									          <td class="label2" width="80%" align="left">
									          		<input name="code" type="text" value="<%=code %>" onkeydown="if(event.keyCode == 13){searchMIBImpairment();}"/>
									          </td>
									        </tr>
									        <tr>
											  <td class="label2" width="20%" align="left"><b>Description</b></td>
									          <td class="label2" width="80%" align="left">
									          		<input name="desc" type="text" value="<%=desc %>" onkeydown="if(event.keyCode == 13){searchMIBImpairment();}"/>
									          </td>
									        </tr>
									    </table>
									</td>
								</tr>
							</table>
						</td>
					 </tr>
				</table>
		<table>     
		<tr>
			<td align="left" colspan="7">&nbsp;&nbsp;
		        <input type="button" class="button1" name="searchBtn" value="Search" onclick="searchMIBImpairment()"> 
		        <input type="button" class="button1" name="clearBtn" value="Clear" onclick="clearFields()"> 
		        <input type="button" class="button1" name="closeBtn" value="Close"  onClick="window.close()">
			</td>
		</tr>
		<tr>
		<td>&nbsp;</td>
        <td>
        
<!--- START OF BODY -->
<table border="1" bordercolor="<%=iumColorB%>" cellpadding="10" cellspacing="0" bgcolor="" width="700" class="listtable1">
<tr><td>

	<table border="0" cellpadding="2" cellspacing="2" class="listtable1" width="700">
        <tr class="headerrow1">
	        <td class="headerrow1" width="15%" >Code</td>
	        <td class="headerrow1" >Description</td>
        </tr>
        

		<input type="hidden" name="pageNumber" value='<bean:write name="requirement" property="pageNumber"/>'>
		<input type="hidden" name="refNo" value='<%=arFormRefNo%>'>
		<input type="hidden" name="arFormRefNo" value='<%=arFormRefNo%>'>
		<input type="hidden" name="sourceSystem" value="<%=sourceSystem%>">

		
		
<%
	int i = 0; 
	String tr_class;
	String td_bgcolor;
	String pagingColSpan = "2";
%>
 <logic:iterate id="rec" name="pageRecord" property="list">				
<%
	if (i%2 == 0){
		tr_class = "row1";
		td_bgcolor = "#CECECE";
	}else{
		tr_class = "row2";
		td_bgcolor = "#EDEFF0";
	}	

	
%>
	<tr class="<%=tr_class%>" >
	   <td valign="top" style="cursor:hand" onclick="setMIBImpairmentToOpener('<bean:write name="rec" property="code"/>', '')"><bean:write name="rec" property="code"/></td>
	   <td valign="top" style="cursor:hand" onclick="setMIBImpairmentToOpener('<bean:write name="rec" property="code"/>', '')"> <script>document.writeln(replaceStr('<bean:write name="rec" property="desc"/>'))</script></td>
	</tr>
<% i++; %>
</logic:iterate>
<%   

	String reqPageNumber = requirement.getPageNumber()!=null && !requirement.getPageNumber().equals("") ? requirement.getPageNumber() : "1";
	pageNumber = Integer.parseInt(reqPageNumber);  
	
	Page pageRecord = (Page)request.getAttribute("pageRecord");
	if (pageRecord==null){ %>		
				 	<tr>
					 <td class="headerrow4" colspan="<%=pagingColSpan%>" align="left" width="100%">No record found.</td>
					</tr>
	<%}else{%>
                    <tr>
                      <td class="headerrow4" colspan="<%=pagingColSpan%>" width="100%" height="10" class="label2" valign="bottom" >
                                                <!-- don't display link for previous page if the current page is the first page -->					
                                               <% if (pageNumber > 1) { %>
                      <a href="#" onclick="javascript:rePaginate(1,'searchMIBImpairment.do?arFormRefNo=<%=arFormRefNo%>&policySuffix=<%=policySuffix%>');"><img src="images/pagnavfirst_off.gif" name="pagnavfirst" border="0" width="18" height="13"/></a>&nbsp;					
                      <a href="#" onclick="javascript:rePaginate(<%=pageNumber-1%>,'searchMIBImpairment.do?arFormRefNo=<%=arFormRefNo%>&policySuffix=<%=policySuffix%>');"><img src="images/pagnavleft_off.gif" name="pagnavleft" border="0" width="18" height="13"/></a>
                                                <% } else {%>
                      <img src="images/pagnavfirst_on.gif" name="pagnavfirst" border="0" width="18" height="13"/>&nbsp;
                      <img src="images/pagnavleft_on.gif" name="pagnavleft" border="0" width="18" height="13"/>
                                                <% } %>
                                                <% 
                                                int currLink = 1;
                                                int prevLink = 1;
                                                %>
                           <logic:iterate id="navLinks" name="pageRecord" property="pageNumbers">
                                                  <% currLink = ((Integer)navLinks).intValue(); %>
                                                  <% if (((Integer)navLinks).intValue() == pageNumber) { %>
                                   <b><bean:write name="navLinks"/></b>
                                                  <% } else { %>		
                                                    <% if ((currLink > (prevLink+1))) { %>
                                                      ...
                                                    <% } %>
                      <a href="#" onclick="rePaginate('<bean:write name="navLinks"/>','searchMIBImpairment.do?arFormRefNo=<%=arFormRefNo%>&policySuffix=<%=policySuffix%>');" class="links2"><bean:write name="navLinks"/></a>
                                                  <% } %>
                                                  <% prevLink = currLink; %>
                                                </logic:iterate>
									
                                                <!-- don't display link for next page if the current page is the last page -->
                                                
                                                   <%  if(pageNumber < prevLink) {  %>
                      <a href="#" onClick="rePaginate('<%=pageNumber+1%>','searchMIBImpairment.do?arFormRefNo=<%=arFormRefNo%>&policySuffix=<%=policySuffix%>');" class="links1"><img src="<%=contextPath%>/images/pagnavright_off.gif" name="pagnavright" border="0" width="18" height="13"/></a>&nbsp;
                      <a href="#" onClick="rePaginate('<%=prevLink%>','searchMIBImpairment.do?arFormRefNo=<%=arFormRefNo%>&policySuffix=<%=policySuffix%>');" class=links1><img src="<%=contextPath%>/images/pagnavlast_off.gif" width="18" height="13" border="0"></a>
                                                <%  } else {  %>
                      <img src="<%=contextPath%>/images/pagnavright_on.gif" width="18" height="13" border="0">&nbsp;
                      <img src="<%=contextPath%>/images/pagnavlast_on.gif" width="18" height="13" border="0">
                                                <%  }  %>
                      </td>
                    </tr>
                                            <% } %>
          <% if (i == 0) { %>
          <tr>
            <td class="label2" colspan="11" nowrap><bean:message key="message.noexisting" arg0="requirements"/></td>
          </tr>
           <% } %>
	</table>
</td></tr>
</table>

</td>
</tr>
<!--- END OF BODY -->
</table>
</div>
</form>
</body>
</html>