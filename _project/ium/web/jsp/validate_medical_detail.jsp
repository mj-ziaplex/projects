<%@ page language="java" import="com.slocpi.ium.util.*" buffer="12kb"%>
<%@ taglib uri="/struts-bean.tld" prefix="bean" %>

<!--
	//START INITIALIZE	
	function initFields() {				
		if (document.frm.actionType.value == "create")
			initCreate();			
    	else if  (document.frm.actionType.value == "view")
    		initView();
    	else if (document.frm.actionType.value == "maintain")
    		initMaintain();
    	else if (document.frm.actionType.value == "changestatus")
    		changeStatusAction();

   		if (isEmpty(document.frm.refNo.value) || document.frm.err.value == "refNo"){
			document.frm.facilitator.disabled = true;
			document.frm.facilitator.value = "";
		}
    }                
		
    function initView(){
		//alert("view");
		disableFields();      
		showAllLayers();
		
		//By Abbie Start
	   	document.all["doc_cal1"].style.display = "none";
	   	document.all["doc_cal2"].style.display = "none";
	   	document.all["doc_cal3"].style.display = "none";
	   	document.all["doc_cal4"].style.display = "none";	   		   	
	   	//document.all["doc_cal5"].style.display = "none";
	   	document.all["doc_cal6"].style.display = "none";
	   	document.all["doc_cal7"].style.display = "none";
	   	document.all["doc_cal8"].style.display = "none";	   		   		   		
   		//by abbie end
   		
    	if (document.frm.labTestInd.value == "L")  //if (document.frm.labTestInd.value == "1") 
    		enableLab();
    	else if (document.frm.labTestInd.value == "M")
    		enableExaminer();
    }    
    
    function initMaintain(){
		//alert("maintain");
		document.frm.save.style.visibility = "visible";
		document.frm.cancel.style.visibility = "visible";
		document.frm.create.style.visibility = "hidden";
		document.frm.maintain.style.visibility = "hidden";

		if (document.frm.labTestInd.value == "L")  //if (document.frm.labTestInd.value == "1") 
    		enableLab();
   		else
   			enableExaminer();
   		document.frm.status.focus();
		//document.frm.status.select();					
	   		
		if (document.frm.status.value == "18"){ //requested
			//alert("requested");
			//by abbie Start
	   		document.all["doc_cal1"].style.display = "block";
	   		document.all["doc_cal2"].style.display = "block";
	   		document.all["doc_cal3"].style.display = "none";
	   		document.all["doc_cal4"].style.display = "none";	   		   	
	   		//document.all["doc_cal5"].style.display = "block";
	   		document.all["doc_cal6"].style.display = "block";
	   		document.all["doc_cal7"].style.display = "block";
	   		document.all["doc_cal8"].style.display = "block";	   		   		   		
	   		//by abbie end
	   			   	  		
			showAllLayers();
			enableFields();			
			
			document.frm.dateConducted.disabled = true;
	  		//if (!isEmpty(document.frm.dateConducted.value))	  			
	  		//	document.frm.dateConducted.value = "";
	  		document.frm.dateReceived.disabled = true;
	  		//if (!isEmpty(document.frm.dateReceived.value))	  			
	  		//	document.frm.dateReceived.value = "";
	  		document.frm.dateValidity.disabled = true;
	  		//if (!isEmpty(document.frm.dateValidity.value))	  			
	  		//	document.frm.dateValidity.value = "";
	   	
			document.frm.requestForLOAInd[0].disabled = true;
		    document.frm.requestForLOAInd[1].disabled = true;
      		document.frm.resultsReceivedInd[0].disabled = true;
      		document.frm.resultsReceivedInd[1].disabled = true;      		
      		if (document.frm.sevenDayMemo[1].checked){
				document.frm.sevenDayMemoDate.disabled = true;
  				document.frm.applicationStatus.disabled = true;
  				document.frm.reason.disabled = true;
  				document.frm.section.disabled = true;
  				document.all["doc_cal8"].style.display = "none";	   		   		 
			}
		}
		else if (document.frm.status.value == "20"){ //else if (document.frm.status.value == "19" || document.frm.status.value == "20"){ //confirm and valid/receive
			//alert("confirm");			
			disableFields();
			
			enableClientFields();
      
			document.frm.status.disabled = false;
			document.frm.status.focus();
			document.frm.remarks.disabled = false;
			//document.frm.remarks.focus();
			//document.frm.remarks.select();
		}
		else if (document.frm.status.value == "19"){
			disableFields();
			
			document.frm.status.disabled = false;
			document.frm.status.focus();
			document.frm.remarks.disabled = false;
		}
		else{
			//alert("else");
			disableFields();
			document.frm.status.disabled = false;
			//document.frm.remarks.disabled = false;
			//document.frm.remarks.focus();
			//document.frm.remarks.select();
			//add validation for remarks
		}    	
    }
    
    function initCreate(){
		document.frm.save.style.visibility = "visible";
		document.frm.cancel.style.visibility = "visible";
		document.frm.create.style.visibility = "hidden";
		document.frm.maintain.style.visibility = "hidden";
		
		//by Abbie Start	
		document.all["doc_cal1"].style.display = "block";
	   	document.all["doc_cal2"].style.display = "block";
	   	document.all["doc_cal3"].style.display = "block";
	   	document.all["doc_cal4"].style.display = "block";	   		   	
	   	//document.all["doc_cal5"].style.display = "block";
	   	document.all["doc_cal6"].style.display = "block";
	   	document.all["doc_cal7"].style.display = "block";
	   	document.all["doc_cal8"].style.display = "block";	   		   		   		
   		//by abbie end
   		
   		//document.all["reqAgent"].style.display = "block";
	   	
		showAllLayers();//?
   		enableFields();

  		if (document.frm.type[0].checked) {
      		document.frm.type[0].disabled = false;
      		enableLab();
      	}
  		else{
  			document.frm.type[1].disabled = false;      
  			enableExaminer();
  		}

		if (document.frm.sevenDayMemo[1].checked){
			//document.frm.sevenDayMemoDate.disabled = true;
  			//document.frm.applicationStatus.disabled = true;
  			//document.frm.reason.disabled = true;
  			//document.frm.section.disabled = true;
  			document.frm.sevenDayMemo[0].disabled = false;
	    	document.frm.sevenDayMemo[1].disabled = false;    	
    		disable7DayMemoFields();
    		document.all["doc_cal8"].style.display = "none";	   		   		   		
		}
		else{
			document.frm.sevenDayMemo[0].disabled = false;
    		document.frm.sevenDayMemo[1].disabled = false;
			enable7DayMemoFields();
		}
		
		if (document.frm.status.value == "18"){ //requested
			//resetConfirmFields();
			//resetValidFields();
			
			if (!(isEmpty(document.frm.refNo.value) && isEmpty(document.frm.clientNo.value))){
				document.all["doc_cal3"].style.display = "none";
	   			document.all["doc_cal4"].style.display = "none";	   		   	
	   	
	  			//document.frm.dateRequested.value = getCurrentDate();
	  			document.frm.dateConducted.disabled = true;
	  			if (!isEmpty(document.frm.dateConducted.value))	  			
	  				document.frm.dateConducted.value = "";
	  			document.frm.dateReceived.disabled = true;
	  			if (!isEmpty(document.frm.dateReceived.value))	  			
	  				document.frm.dateReceived.value = "";
	  			document.frm.dateValidity.disabled = true;
	  			if (!isEmpty(document.frm.dateValidity.value))	  			
	  				document.frm.dateValidity.value = "";
	  		}

	  		document.frm.chargeTo.disabled = true;
	  		document.frm.amount.disabled = true;
		}
		else if (document.frm.status.value == "19"){ //confirm
	   		document.all["doc_cal8"].style.display = "none";
	   			  		
	  		if (!(isEmpty(document.frm.refNo.value) && isEmpty(document.frm.clientNo.value))){
				document.all["doc_cal3"].style.display = "none";
	   			document.all["doc_cal4"].style.display = "none";	   		   	
	   	
	  			//document.frm.dateRequested.value = getCurrentDate();
	  			document.frm.dateConducted.disabled = true;
	  			if (!isEmpty(document.frm.dateConducted.value))	  			
	  				document.frm.dateConducted.value = "";
	  			document.frm.dateReceived.disabled = true;
	  			if (!isEmpty(document.frm.dateReceived.value))	  			
	  				document.frm.dateReceived.value = "";
	  			document.frm.dateValidity.disabled = true;
	  			if (!isEmpty(document.frm.dateValidity.value))	  			
	  				document.frm.dateValidity.value = "";
	  		}
	  			  			
	  		document.frm.chargeTo.disabled = true;
	  		document.frm.amount.disabled = true;

   			document.frm.sevenDayMemo[0].disabled = false;
	    	document.frm.sevenDayMemo[1].disabled = false;    	
    		disable7DayMemoFields();    		

			document.frm.resultsReceivedInd[1].checked = true;
			document.frm.resultsReceivedInd[0].disabled = true;
      		document.frm.resultsReceivedInd[1].disabled = true;      		
      		
			document.frm.requestForLOAInd[1].checked = true;
    		document.frm.requestForLOAInd[0].disabled = true;
	    	document.frm.requestForLOAInd[1].disabled = true;
		}
		else if (document.frm.status.value == "20"){ //valid
			//alert("valid");
			//resetConfirmFields();
			
	   		document.all["doc_cal7"].style.display = "none";
	   		document.all["doc_cal8"].style.display = "none";
			
	  		document.frm.resultsReceivedInd[0].checked = true;

			document.frm.dateValidity.disabled = true;
	    	document.frm.dateConducted.disabled = false;
   			document.frm.dateReceived.disabled = false;   			
   			document.frm.appointmentDate.disabled = true;
   			document.frm.appointmentTime.disabled = true;
   			
   			if (!isEmpty(document.frm.appointmentDate.value))	   			
	   			document.frm.appointmentDate.value = "";	   		
	   		if (!isEmpty(document.frm.appointmentTime.value))	   			
	   			document.frm.appointmentTime.value = "";
   		
 			disable7DayMemoFields();
    		document.frm.sevenDayMemo[0].disabled = true;
    		document.frm.sevenDayMemo[1].disabled = true;

    		document.frm.resultsReceivedInd[0].checked = true;
    		document.frm.resultsReceivedInd[0].disabled = true;
	    	document.frm.resultsReceivedInd[1].disabled = true;	    	
	    	
    		document.frm.requestForLOAInd[0].checked = true;
    		document.frm.requestForLOAInd[0].disabled = true;
	    	document.frm.requestForLOAInd[1].disabled = true;	    	
		}		
		focusOnNextField();
    }//function initCreate(){


     function resetTest(){
    	document.frm.test.value = "";
    	document.frm.followUpNumber.value = "0";
    }
    
    function resetConfirmFields(){
  		document.frm.appointmentDate.value = "";
		document.frm.appointmentTime.value = "";
    }
    
    function resetValidFields(){
    	document.frm.dateConducted.value = "";
		document.frm.dateReceived.value = "";
		document.frm.remarks.value = "";
    }
    
    function reset7DayMemoFields(){    	
    	document.frm.sevenDayMemo[1].checked = true;
    	document.frm.sevenDayMemoDate.value = "";
      	document.frm.applicationStatus.value = "12";
      	document.frm.reason.value = "";
      	document.frm.section.value = "POLICYISUE";
    }
    
    	function focusOnNextField(){
    	if (noSelection(document.frm.facilitator.value)) {
			document.frm.facilitator.focus();
		}    	
		//else if (isEmpty(document.frm.refNo.value)){
		//	document.frm.refNo.focus();
		//	document.frm.refNo.select();
		//}
		else if (noSelection(document.frm.agent.value)) {			
			document.frm.agent.focus();
		}
		else if (noSelection(document.frm.branch.value)) {
			document.frm.branch.focus();
		}
		else if (isEmpty(document.frm.clientNo.value)){
			if (isEmpty(document.frm.lastName.value)) {
				document.frm.lastName.focus();
				document.frm.lastName.select();
			}		
			else if (isEmpty(document.frm.birthDate.value)) {
				document.frm.birthDate.focus();
				document.frm.birthDate.select();
		    }
			else if (isEmpty(document.frm.firstName.value)) {
				document.frm.firstName.focus();
				document.frm.firstName.select();
			}				
		}    					
		else if (document.frm.type[0].checked) {
			if (noSelection(document.frm.labName.value)) {
				document.frm.labName.focus();
			}
		}			
		else if (document.frm.type[1].checked) {
			if (noSelection(document.frm.examiner.value)) {
				document.frm.examiner.focus();
			}		    			    	    	
		}
    	else if (document.frm.resultsReceivedInd[0].checked) {
			if (isEmpty(document.frm.remarks.value)) {
				document.frm.remarks.focus();
				document.frm.remarks.select();
			}
    	}
		else if (noSelection(document.frm.test.value)) {
			document.frm.test.focus();
		}
    	else if (document.frm.sevenDayMemo[0].checked){
    		if (isEmpty(document.frm.sevenDayMemoDate.value)) {
				document.frm.sevenDayMemoDate.focus();
				document.frm.sevenDayMemoDate.select();
			}
			else if (noSelection(document.frm.applicationStatus.value)) {
				document.frm.applicationStatus.focus();
			}
			else if (isEmpty(document.frm.reason.value)) {
				document.frm.reason.focus();
				document.frm.reason.select();
			}
			else if (noSelection(document.frm.section.value)) {
				document.frm.section.focus();
			}
    	}    	    	
		else if (noSelection(document.frm.reqParty.value)) {
			document.frm.reqParty.focus();
		}    	
    	else if (noSelection(document.frm.department.value)) { 
			document.frm.department.focus(); 
		}
		else{ //in case all the required fields have value
			document.frm.status.focus();
		}
	}

    //END INITIALIZE
    
	//START VISIBILITY	
	function disableFields(){
      document.frm.refNo.disabled = true;
      document.frm.agent.disabled = true;
      document.frm.clientNo.disabled = true;
      document.frm.branch.disabled = true;
      document.frm.lastName.disabled = true;
      document.frm.birthDate.disabled = true;
      document.frm.firstName.disabled = true;
      
      //alert(document.frm.status.value);
      document.frm.status.disabled = true;
      document.frm.remarks.disabled = true;
      document.frm.type[0].disabled = true;
      document.frm.type[1].disabled = true;      
      document.frm.labName.disabled = true;
      document.frm.examiner.disabled = true;
      document.frm.test.disabled = true;
      document.frm.examPlace.disabled = true;
      document.frm.dateRequested.disabled = true;
      document.frm.dateConducted.disabled = true;
      document.frm.dateReceived.disabled = true;
      document.frm.dateValidity.disabled = true;
      document.frm.followUpNumber.disabled = true;      
      document.frm.followUpDate.disabled = true;
      document.frm.appointmentDate.disabled = true;
      document.frm.appointmentTime.disabled = true;
            
      document.frm.sevenDayMemo[0].disabled = true;
      document.frm.sevenDayMemo[1].disabled = true;
      document.frm.sevenDayMemoDate.disabled = true;
      document.frm.applicationStatus.disabled = true;
      document.frm.reason.disabled = true;
      document.frm.section.disabled = true;      
      
      document.frm.reqParty.disabled = true;
      document.frm.department.disabled = true;
      document.frm.chargeTo.disabled = true;
      document.frm.amount.disabled = true;
      document.frm.payAgent[0].disabled = true;
      document.frm.payAgent[1].disabled = true;
      
      document.frm.requestForLOAInd[0].disabled = true;
	  document.frm.requestForLOAInd[1].disabled = true;
      document.frm.resultsReceivedInd[0].disabled = true;
      document.frm.resultsReceivedInd[1].disabled = true;      		
      document.frm.facilitator.disabled = true;      		
      //document.frm.receivedDate.disabled = true;
    }
    
    function enableFields(){
      document.frm.refNo.disabled = false;
      document.frm.agent.disabled = false;
      document.frm.clientNo.disabled = false;
      document.frm.branch.disabled = false;
      document.frm.lastName.disabled = false;
      document.frm.birthDate.disabled = false;
      document.frm.firstName.disabled = false;
      
      //alert(document.frm.status.value);
      document.frm.status.disabled = false;
      document.frm.remarks.disabled = false;
      document.frm.type[0].disabled = false;
      document.frm.type[1].disabled = false;      
      document.frm.labName.disabled = false;
      document.frm.examiner.disabled = false;
      document.frm.test.disabled = false;
      document.frm.examPlace.disabled = false;
      document.frm.dateRequested.disabled = false;
      document.frm.dateConducted.disabled = false;
      document.frm.dateReceived.disabled = false;
      document.frm.dateValidity.disabled = false;
      document.frm.followUpNumber.disabled = false;      
      document.frm.followUpDate.disabled = false;
      document.frm.appointmentDate.disabled = false;
      document.frm.appointmentTime.disabled = false;
            
      document.frm.sevenDayMemo[0].disabled = false;
      document.frm.sevenDayMemo[1].disabled = false;
      document.frm.sevenDayMemoDate.disabled = false;
      document.frm.applicationStatus.disabled = false;
      document.frm.reason.disabled = false;
      document.frm.section.disabled = false;      
      
      document.frm.reqParty.disabled = false;
      document.frm.department.disabled = false;
      document.frm.chargeTo.disabled = false;
      document.frm.amount.disabled = false;
      document.frm.payAgent[0].disabled = false;
      document.frm.payAgent[1].disabled = false;
      
      document.frm.requestForLOAInd[0].disabled = false;
	  document.frm.requestForLOAInd[1].disabled = false;
      document.frm.resultsReceivedInd[0].disabled = false;
      document.frm.resultsReceivedInd[1].disabled = false;      		
      //document.frm.receivedDate.disabled = false;
      document.frm.facilitator.disabled = false;      		
    }
    
    function showAllLayers(){
    	//document.getElementById("facilitatorList").style.visibility="visible";
    	//document.getElementById("facilitatorLabel").style.visibility="visible";		    	
		document.getElementById("btnGrp1").style.visibility="visible";  
		document.getElementById("btnGrp2").style.visibility="visible";
    }
    
    function enableLab(){
    	document.getElementById("labNameDiv").style.visibility="visible";  
		document.getElementById("examinerNameDiv").style.visibility="hidden";
		//reset
		document.frm.examiner.value = "";
        //document.frm.followUpNumber.value = "0";
    }
    
    function  enableExaminer(){
    	document.getElementById("labNameDiv").style.visibility="hidden";  
		document.getElementById("examinerNameDiv").style.visibility="visible";
		//reset
		document.frm.labName.value = "";
		//document.frm.followUpNumber.value = "0";
    }
    
        function enable7DayMemoFields(){
    	//document.frm.sevenDayMemo[0].disabled = false;
    	//document.frm.sevenDayMemo[1].disabled = false;
    	document.all["doc_cal8"].style.display = "block";	   		   		   		
      	document.frm.sevenDayMemoDate.disabled = false;
      	document.frm.sevenDayMemoDate.value = getCurrentDate();
      	document.frm.applicationStatus.disabled = false;
      	document.frm.reason.disabled = false;
      	document.frm.section.disabled = false;  
      	document.frm.applicationStatus.focus();
    }
    
    function disable7DayMemoFields(){
    	//document.frm.sevenDayMemo[0].disabled = true;
    	//document.frm.sevenDayMemo[1].disabled = true;
    	document.all["doc_cal8"].style.display = "none";	   		   		   		
      	document.frm.sevenDayMemoDate.disabled = true;
      	document.frm.applicationStatus.disabled = true;
      	document.frm.reason.disabled = true;
      	document.frm.section.disabled = true;        	
      	reset7DayMemoFields();
    }
    
    function enableResultsReceived(){
    	//like this for now
    	if (document.frm.status.value != "20"){
    		alert("Please set the status to Valid");
    		document.frm.resultsReceivedInd[1].checked = true;
   		};
    	
    	//alert("You won't be able to edit the seven-day memo fields and Request LOA.");
    	
    	//document.frm.dateConducted.disabled = false;
   		//document.frm.dateReceived.disabled = false;
   		
 		//disable7DayMemoFields();
    	//document.frm.sevenDayMemo[0].disabled = true;
    	//document.frm.sevenDayMemo[1].disabled = true;
    	////reset7DayMemoFields();    	
    	//document.frm.requestForLOAInd[1].checked = true;
    	//document.frm.requestForLOAInd[0].disabled = true;
	    //document.frm.requestForLOAInd[1].disabled = true;
    }
    
    function disableResultsReceived(){
    	//if (document.frm.status.value == "20"){
		//	alert("You can't set Results Received to No when the Status is Valid.");
		//	document.frm.resultsReceivedInd[0].checked = true;
		//}
    	//else{
    	//	document.frm.dateConducted.disabled = true;
	   	//	document.frm.dateReceived.disabled = true;
   		
   		//	document.frm.sevenDayMemo[0].disabled = false;
	    //	document.frm.sevenDayMemo[1].disabled = false;    	
	    //	//if (document.frm.sevenDayMemo[0].checked){
	    //	//	enable7DayMemoFields();
	    //	//}
	    //	//else{
	    //	disable7DayMemoFields();
	    //	//}
	    	
	    //	//document.frm.requestForLOAInd[0].checked = true;
	    //	document.frm.requestForLOAInd[0].disabled = false;
		// document.frm.requestForLOAInd[1].disabled = false;
	    //}
    }
    
    function enableRequestForLOA(){
    
    }
    
    function disableRequestForLOA(){
    	if (document.frm.status.value != "19"){
    		alert("Please set the status to Confirmed");
    		document.frm.requestForLOAInd[0].checked = true;
   		};
    }
    
    function enableClientFields(){
    	if (isEmpty(document.frm.clientNo.value) || isEmpty(document.frm.refNo.value) || document.frm.err.value == "refNo" || document.frm.err.value == "fromcreate"){
			document.frm.refNo.disabled = false;
    		document.frm.agent.disabled = false;
	    	document.frm.clientNo.disabled = false;
    		document.frm.branch.disabled = false;
    		document.frm.lastName.disabled = false;
    		document.frm.birthDate.disabled = false;
    		document.frm.firstName.disabled = false;
    		//document.frm.facilitator.disabled = false;
      	}
    }
	//END VISIBILITY
	    
    //START COMPUTATION
    function getCurrentDate() {
      var date = new Date();
      var month = new Array("JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC");
      
      var day = date.getDate();
      if (day < 10) {
        day = "0" + day;
      }
      
      var currDate = day + month[date.getMonth()] + date.getFullYear();
      return currDate;
    }
    
    function setFollowUpDate(){
    	var oneMinute = 60 * 1000;  // milliseconds in a minute
		var oneHour = oneMinute * 60;
		var oneDay = oneHour * 24;
		var oneWeek = oneDay * 7;

		//Once this is done you can calculate a future date based on today's date using these variables. For example, here is how you would find the date 5 weeks from now. 

		//var today = new Date()
 		//var dateInMS = today.getTime() + oneWeek * 5
 		//var targetDate = new Date(dateInMS)
 		
 		var today = new Date();
		var dateInMS = today.getTime() + (oneDay * document.frm.followUpNumber.value);
 		var targetDate = new Date(dateInMS);
 		
 		//format date
 		var month = new Array("JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC"); 		
 		var day = targetDate.getDate();
 		if (day < 10) {
        	day = "0" + day;
      	}
      	//alert(document.frm.followUpNumber.value);
 		document.frm.followUpDate.value = day + month[targetDate.getMonth()] + targetDate.getFullYear();
    }    
    //END COMPUTATION
    
    //START ACTION
    function onChangeStatus(){
		var url;
		if (document.frm.actionType.value == "maintain"){
			url = 'viewMedicalDetail.do?actionType=changestatus&refNo=' + document.frm.medicalRecordId.value + '&status=' + document.frm.status.value;
			//alert(url);
			gotoPage('frm', url);
		}
		else if (document.frm.actionType.value == "changestatus"){
			url = 'viewMedicalDetail.do?actionType=changestatus&refNo=' + document.frm.medicalRecordId.value + '&status=' + document.frm.status.value;
			gotoPage('frm', url);
		}
		else if (document.frm.actionType.value == "create"){
			callCreateAction();	
		}
	}
	
	function callCreateAction() {
		enableFields();
		if (document.frm.actionType.value == "create") //just added this
    		gotoPage('frm', 'createMedicalExam.do');
    	else
    		gotoPage('frm', 'createMedicalExam.do?actionType=' + document.frm.actionType.value);
	}

	function refreshAgentOrBranch(ind) {
		enableFields();
		if (document.frm.actionType.value == "create") //just added this
    		gotoPage('frm', 'createMedicalExam.do?refInd=' + ind);
    	else
    		gotoPage('frm', 'createMedicalExam.do?actionType=' + document.frm.actionType.value + '&refInd=' + ind);
	}

	function refreshAgentOrBranch(ind) {
		enableFields();
		if (document.frm.actionType.value == "create") //just added this
    		gotoPage('frm', 'createMedicalExam.do?refInd=' + ind);
    	else
    		gotoPage('frm', 'createMedicalExam.do?actionType=' + document.frm.actionType.value + '&refInd=' + ind);
	}
	
	
	/*function viewMedicalRecordAction(url) {			
		document.frm.save.style.visibility = "hidden";
		document.frm.cancel.style.visibility = "hidden";
		document.frm.create.style.visibility = "visible";
		document.frm.maintain.style.visibility = "visible";
    	window.location = url;
	}*/
	
	function saveMedicalExam(){
    	//alert(document.frm.actionType.value + " " + document.frm.status.value);
    	var saveExam = true;
    	//alert("Start " + saveExam);
    	if ((document.frm.actionType.value == "create" && document.frm.status.value == "18") || ((document.frm.actionType.value == "maintain" || document.frm.actionType.value == "changestatus") && document.frm.status.value == "18")){
    		//alert("request");
    		saveExam = validateCreate();
    	}
    	else if (document.frm.actionType.value == "create" && document.frm.status.value == "19"){
    		//alert("confirm");
    		saveExam = validateConfirm();
    	}
    	else if ((document.frm.actionType.value == "create" && document.frm.status.value == "20") || (document.frm.actionType.value == "changestatus" && document.frm.status.value == "20")){
    		//alert("valid");
    		saveExam = validateReceive();
    	}    	    	    	    	
		else if ((document.frm.actionType.value == "maintain" || document.frm.actionType.value == "changestatus") && document.frm.status.value == "19"){
			//alert("confirm maintain");
			saveExam = validateConfirm();
			//alert(document.frm.refNo.value);
			//document.frm.actionType.referenceNum = document.frm.refNo.value;			
			//alert(document.frm.actionType.referenceNum);
		}
		else{//not yet tested
			//alert("else");
			if (noSelection(document.frm.status.value)) {
    			alert("<bean:message key="error.field.requiredselection" arg0="status"/>");	//alert("Facilitator is a required field.");
				document.frm.status.focus();
				saveExam = false;
			}
			else
				saveExam = true;
		}
		
		if (saveExam){
			enableFields();
			//alert(document.frm.status.value);
			if (document.frm.status.value == "22")
				gotoPage('frm', 'saveMedicalExam.do?followUpDate='+document.frm.followUpDate.value+'&referenceNum='+document.frm.refNo.value); //alert(document.frm.actionType.value + " 22");
			else if (document.frm.status.value == "18")
				gotoPage('frm', 'saveMedicalExam.do?amount='+document.frm.amount.value); //alert(document.frm.actionType.value + " 18"); //
			else if (document.frm.status.value == "20")
				gotoPage('frm', 'saveMedicalExam.do?test='+document.frm.test.value+'&referenceNum='+document.frm.refNo.value); //alert(document.frm.actionType.value + " 18"); //
			else 
				gotoPage('frm', 'saveMedicalExam.do?referenceNum='+document.frm.refNo.value);	 //alert(document.frm.actionType.value); //		
		}				
    }
    //END ACTION
    
    //START BUTTON ACTIONS
   	function maintainAction(){
		//alert(document.frm.status.value);
		document.frm.save.style.visibility = "visible";
		document.frm.cancel.style.visibility = "visible";
		document.frm.create.style.visibility = "hidden";
		document.frm.maintain.style.visibility = "hidden";
		if (document.frm.status.value == "18"){ //requested
			showAllLayers();
			enableFields();
			document.frm.requestForLOAInd[0].disabled = true;
		    document.frm.requestForLOAInd[1].disabled = true;
      		document.frm.resultsReceivedInd[0].disabled = true;
      		document.frm.resultsReceivedInd[1].disabled = true;      		
      		document.frm.status.focus();
			//document.frm.status.select();			
		}
		else if (document.frm.status.value == "20"){ //confirm //else if (document.frm.status.value == "19" || document.frm.status.value == "20"){ //confirm
			disableFields();
			
			enableClientFields();
			
			document.frm.status.disabled = false;
			document.frm.remarks.disabled = false;
			document.frm.status.focus();			
			//document.frm.remarks.focus();
			//document.frm.remarks.select();
		}
		else if (document.frm.status.value == "19"){
			disableFields();
			
			document.frm.status.disabled = false;
			document.frm.remarks.disabled = false;
			document.frm.status.focus();			
			//document.frm.remarks.focus();
			//document.frm.remarks.select();
		}
		else{
			disableFields();
			//alert("else");
			document.frm.remarks.disabled = false;
			document.frm.remarks.focus();
			document.frm.remarks.select();
			//add validation for remarks
		}
	}
	
	function changeStatusAction(){
		document.frm.save.style.visibility = "visible";
		document.frm.cancel.style.visibility = "visible";
		document.frm.create.style.visibility = "hidden";		
		document.frm.maintain.style.visibility = "hidden";
		
		document.all["doc_cal1"].style.display = "none";
	   	document.all["doc_cal2"].style.display = "none";
	   	document.all["doc_cal3"].style.display = "none";
	   	document.all["doc_cal4"].style.display = "none";	   		   	
	   	//document.all["doc_cal5"].style.display = "block";
	   	document.all["doc_cal6"].style.display = "none";
	   	document.all["doc_cal7"].style.display = "none";
	   	document.all["doc_cal8"].style.display = "none";	   		   		   		
	   	
	   	enableFields();			
	   	
		if (document.frm.status.value == "18"){ //requested
			//showAllLayers();
			//enableFields();
			//document.frm.requestForLOAInd[0].disabled = true;
		    //document.frm.requestForLOAInd[1].disabled = true;
			//document.frm.requestForLOAInd[0].checked = true;
			
		    //document.frm.resultsReceivedInd[1].checked = true;
      		//document.frm.resultsReceivedInd[0].disabled = true;
      		//document.frm.resultsReceivedInd[1].disabled = true;      		
      		//document.frm.status.focus();
			////document.frm.status.select();		
				
			//alert("requested");
			//by abbie Start
	   		document.all["doc_cal1"].style.display = "block";
	   		document.all["doc_cal6"].style.display = "block";
	   		document.all["doc_cal7"].style.display = "block";
	   		document.all["doc_cal8"].style.display = "block";	   		   		   		
	   		//by abbie end
	   			   	  		
			showAllLayers();
			enableFields();			
			
			document.frm.dateConducted.disabled = true;
	  		//if (!isEmpty(document.frm.dateConducted.value))	  			
	  		//	document.frm.dateConducted.value = "";
	  		document.frm.dateReceived.disabled = true;
	  		//if (!isEmpty(document.frm.dateReceived.value))	  			
	  		//	document.frm.dateReceived.value = "";
	  		document.frm.dateValidity.disabled = true;
	  		//if (!isEmpty(document.frm.dateValidity.value))	  			
	  		//	document.frm.dateValidity.value = "";
	   	
			document.frm.requestForLOAInd[0].disabled = true;
		    document.frm.requestForLOAInd[1].disabled = true;
      		document.frm.resultsReceivedInd[0].disabled = true;
      		document.frm.resultsReceivedInd[1].disabled = true;      		
      		if (document.frm.sevenDayMemo[1].checked){
				document.frm.sevenDayMemoDate.disabled = true;
  				document.frm.applicationStatus.disabled = true;
  				document.frm.reason.disabled = true;
  				document.frm.section.disabled = true;
  				document.all["doc_cal8"].style.display = "none";	   		   		 
			}
			if (document.frm.labTestInd.value == "L")  //if (document.frm.labTestInd.value == "1") 
	    		enableLab();
    		else
    			enableExaminer();
      		document.frm.status.focus();
			//document.frm.status.select();			
		}
		else if (document.frm.status.value == "19"){ //confirm
			document.all["doc_cal7"].style.display = "block";
			
			disableFields();
			
			//enableClientFields();
      		
			document.frm.requestForLOAInd[1].checked = true;
			document.frm.resultsReceivedInd[1].checked = true;
			
			document.frm.status.disabled = false;
			document.frm.appointmentDate.disabled = false;
      		document.frm.appointmentTime.disabled = false;
      		document.frm.remarks.disabled = false;
      		
      		if (isEmpty(document.frm.appointmentDate.value)) {// || isEmpty(document.frm.appointmentTime.value))
				 alert("<bean:message key="error.field.required" arg0="Appointment date"/>");
				document.frm.appointmentDate.focus();
				document.frm.appointmentDate.select();
			}
			else if (isEmpty(document.frm.appointmentTime.value)) {
				 alert("<bean:message key="error.field.required" arg0="Appointment time"/>");
				document.frm.appointmentTime.focus();
				document.frm.appointmentTime.select();
			}
		}		
		else if (document.frm.status.value == "20"){ //valid (receive) 
			document.all["doc_cal3"].style.display = "block";
			document.all["doc_cal4"].style.display = "block";
			
			disableFields();
			
			enableClientFields();
      		
			document.frm.status.disabled = false;						
			document.frm.dateReceived.disabled = false;			
			document.frm.dateReceived.value = getCurrentDate(); //change to current date										
			document.frm.remarks.disabled = false;
			document.frm.dateConducted.disabled = false;			
			
			if (isEmpty(document.frm.remarks.value)){ //prompt user
				alert("<bean:message key="error.field.required" arg0="Remarks"/>");
				document.frm.remarks.focus();
				document.frm.remarks.select();
			}			
			else if (isEmpty(document.frm.dateConducted.value)){
				alert("<bean:message key="error.field.required" arg0="Date conducted"/>");
				document.frm.dateConducted.focus();
				document.frm.dateConducted.select();
			}
						
			//document.frm.resultsReceivedInd[0].disabled = false;			
			document.frm.requestForLOAInd[0].checked = true;
			document.frm.resultsReceivedInd[0].checked = true;
		}
		else if (document.frm.status.value == "22"){ //not submitted
			//document.frm.fUpDate.value = document.frm.followUpDate.value;
			disableFields();
			//alert(document.frm.fUpDate.value);
			document.frm.status.disabled = false;						
			if (isEmpty(document.frm.followUpDate.value))
				alert("<bean:message key="error.field.required" arg0="Follow-up date"/>");
			else{
				//check if follow-up date >= current date
				//getCurrentDate();
				//
			}
		}
		else{//else if (document.frm.status.value == "23"){ //cancelled
			disableFields();
			document.frm.status.disabled = false;						
		}
	}
	
	function cancelAction(url){
		window.location = url;
	}
	//END BUTTON ACTIONS
    
    //START VALIDATION
    function validateCreate(){
    	/////////////
		if (isEmpty(document.frm.refNo.value)  && (document.frm.status.value != "20")){
  			alert("<bean:message key="error.field.requiredselection" arg0="reference number"/>");
			document.frm.refNo.focus();
			return false;
  		}
  		if (noSelection(document.frm.agent.value)) {			
			alert("Please select an agent."); //alert("<bean:message key="error.field.requiredselection" arg0="agent"/>"); //alert("Agent Name is a required field.");
			document.frm.agent.focus();
			//document.frm.agent.select();
			return false;
		}    	
    	if (isEmpty(document.frm.clientNo.value) && (document.frm.status.value != "20")){
    		alert("<bean:message key="error.field.requiredselection" arg0="client number"/>");
			document.frm.clientNo.focus();
			return false;
    	}
    	if (noSelection(document.frm.branch.value)) {
			alert("<bean:message key="error.field.requiredselection" arg0="branch"/>"); //alert("Branch Name is a required field.");
			document.frm.branch.focus();
			//document.frm.branch.select();
			return false;
		}
		if (!isEmpty(document.frm.clientNo.value)){
			if (isEmpty(document.frm.lastName.value)) {
				alert("<bean:message key="error.field.required" arg0="Last name"/>"); //alert("Last Name is a required field.");
				document.frm.lastName.focus();
				document.frm.lastName.select();
				return false;
			}		
			if (isEmpty(document.frm.birthDate.value)) {
				alert("<bean:message key="error.field.required" arg0="Birth date"/>"); //alert("Birth Date is a required field.");
				document.frm.birthDate.focus();
				document.frm.birthDate.select();
				return false;
		    }
			else if (!isValidDate(document.frm.birthDate.value)) {
				alert("<bean:message key="error.field.date" arg0="Birth date"/>");			   			   
				document.frm.birthDate.focus();
				document.frm.birthDate.select();
				return false;
			}
			if (isEmpty(document.frm.firstName.value)) {
				alert("<bean:message key="error.field.required" arg0="First name"/>"); //alert("First Name is a required field.");
				document.frm.firstName.focus();
				document.frm.firstName.select();
				return false;
			}				
		}
    	
		if (isEmpty(document.frm.refNo.value) && isEmpty(document.frm.clientNo.value)){
	    	if (isEmpty(document.frm.dateConducted.value) && (document.frm.status.value == "20")) { 
				alert("<bean:message key="error.field.required" arg0="Date conducted"/>"); //alert("Date conducted is a required field.");
				document.frm.dateConducted.focus();
				document.frm.dateConducted.select();
				return false;
			}
			else{
			 	if (!isValidDate(document.frm.dateConducted.value)){
					alert("<bean:message key="error.field.date" arg0="Date conducted"/>");
			    	//document.frm.dateConducted.focus();
			    	//document.frm.dateConducted.select();
		   			return false;
		   		}
		   		else{
		   			if (isGreaterThanCurrentDate(document.frm.dateConducted.value)){
		   				//error.field.beforeenddate = {0} should be earlier than or equal to the {1}.
		   				alert("<bean:message key="error.field.beforeenddate" arg0="Conducted date"  arg1="current date"/>");
    					document.frm.dateConducted.focus();
   						document.frm.dateConducted.select();
   						return false;
		   			}
		   			else{	
			   			if (!isGreaterThanEqualToDate(document.frm.dateConducted.value, document.frm.dateRequested.value)){
			   				alert("<bean:message key="error.field.afterenddate" arg0="Conducted date"  arg1="requested date"/>");
	    					document.frm.dateConducted.focus();
   							document.frm.dateConducted.select();
	   						return false;
		   				}
		   			}
		   		}
			}
			if (isEmpty(document.frm.dateReceived.value) && (document.frm.status.value == "20")) {
				alert("<bean:message key="error.field.required" arg0="Date received"/>"); //alert("Date Received is a required field.");
				//document.frm.dateReceived.focus();
				//document.frm.dateReceived.select();
				return false;
			}
			else if (!isValidDate(document.frm.dateReceived.value)){
				alert("<bean:message key="error.field.date" arg0="Date received"/>");
			    //document.frm.dateReceived.focus();
			    //document.frm.dateReceived.select();
		   		return false;
			}
			if (isEmpty(document.frm.remarks.value)) {
				alert("<bean:message key="error.field.required" arg0="Remarks"/>"); //alert("Remarks is a required field.");
				document.frm.remarks.focus();
				document.frm.remarks.select();
				return false;
			}
    	}
    	/////////////
    	if (noSelection(document.frm.status.value)) {
    		alert("<bean:message key="error.field.requiredselection" arg0="status"/>");	//alert("Facilitator is a required field.");
			document.frm.status.focus();
			//document.frm.facilitator.select();
			return false;
		}    	
		if (document.frm.facilitator.disabled == false){
    		if (noSelection(document.frm.facilitator.value)) {
    			alert("<bean:message key="error.field.requiredselection" arg0="facilitator"/>");	//alert("Facilitator is a required field.");
				document.frm.facilitator.focus();
				//document.frm.facilitator.select();
				return false;
			}    	
		}
		
					
		//else if (!isValidDate(document.frm.dateRequested.value)) {
		//  alert("Date Requested is an invalid date."); //alert("<bean:message key="error.field.date" arg0="Date Requested"/>");
		//   document.frm.dateRequested.focus();
		//}
    	if (document.frm.resultsReceivedInd[0].checked) {
    		//Start - these field dates are disabled
    		if (isEmpty(document.frm.dateConducted.value)) { 
				//alert("<bean:message key="error.field.required" arg0="Date conducted"/>"); //alert("Date conducted is a required field.");
				//document.frm.dateConducted.focus();
				//document.frm.dateConducted.select();
				//return false;
			}
			else if (!isValidDate(document.frm.dateConducted.value)){
				//alert("<bean:message key="error.field.date" arg0="Date conducted"/>");
			    //document.frm.dateConducted.focus();
			    //document.frm.dateConducted.select();
		   		//return false;
			}
			if (isEmpty(document.frm.dateReceived.value)) {
				//alert("<bean:message key="error.field.required" arg0="Date received"/>"); //alert("Date Received is a required field.");
				//document.frm.dateReceived.focus();
				//document.frm.dateReceived.select();
				//return false;
			}
			else if (!isValidDate(document.frm.dateReceived.value)){
				//alert("<bean:message key="error.field.date" arg0="Date received"/>");
			    //document.frm.dateReceived.focus();
			    //document.frm.dateReceived.select();
		   		//return false;
			}
			//End
			if (isEmpty(document.frm.remarks.value)) {
				alert("<bean:message key="error.field.required" arg0="Remarks"/>"); //alert("Remarks is a required field.");
				document.frm.remarks.focus();
				document.frm.remarks.select();
				return false;
			}
    	}
    	//if (isEmpty(document.frm.followUpDate.value)) {
    	//	alert("<bean:message key="error.field.required" arg0="Follow-up date"/>"); //alert("Follow-Up Date is a required field.");
		//    document.frm.followUpDate.focus();
		//    document.frm.followUpDate.select();
		//	return false;
    	//}
    	//else if (!isValidDate(document.frm.followUpDate.value)) {
		//   alert("<bean:message key="error.field.date" arg0="Follow-up date"/>"); //alert("Follow-Up Date is an invalid date.");
		//   document.frm.followUpDate.focus();
		//   document.frm.followUpDate.select();
		//   return false;
		//}	
		if (!isEmpty(document.frm.followUpDate.value)) {
			if (!isValidDate(document.frm.followUpDate.value)) {
		    	alert("<bean:message key="error.field.date" arg0="Follow-up date"/>"); //alert("Follow-Up Date is an invalid date.");
		   		document.frm.followUpDate.focus();
		   		document.frm.followUpDate.select();
		   		return false;
			}
    	}
    	if (document.frm.type[0].checked) {
			if (noSelection(document.frm.labName.value)) {
				alert("<bean:message key="error.field.requiredselection" arg0="laboratory"/>"); //alert("Laboratory Name is a required field.");
				document.frm.labName.focus();
				//document.frm.labName.select();
				return false;
			}
		}			
		if (document.frm.type[1].checked) {
			if (noSelection(document.frm.examiner.value)) {
				alert("<bean:message key="error.field.requiredselection" arg0="branch"/>"); //alert("Examiner Name is a required field.");
				document.frm.examiner.focus();
				//document.frm.examiner.select();
				return false;
			}		    			    	    	
		}
		if (noSelection(document.frm.test.value)) {
			alert("<bean:message key="error.field.requiredselection" arg0="test"/>"); //alert("Test is a required field.");
			document.frm.test.focus();
			//document.frm.test.select();
			return false;
		}
    	if (document.frm.requestForLOAInd[1].checked){
    		if (isEmpty(document.frm.appointmentDate.value)) {
    			alert("<bean:message key="error.field.required" arg0="Appointment date"/>"); //alert("Appointment Date is a required field."); 
    			document.frm.appointmentDate.focus();
    			document.frm.appointmentDate.select();
				return false;
    		}
    		else {
    			if (!isValidDate(document.frm.appointmentDate.value)) {    			
		   			alert("<bean:message key="error.field.date" arg0="Appointment date"/>");
		    		document.frm.appointmentDate.focus();
		   			document.frm.appointmentDate.select();
		   			return false;
		   		}
				else{
		   			//date format is ddMMMyyyy; date1 >= date2 returns true
					if (!isGreaterThanEqualToDate(document.frm.appointmentDate.value, getCurrentDate())){
						//error.field.afterenddate = {0} should be later than or equal to the {1}.
						alert("<bean:message key="error.field.afterenddate" arg0="Appointment date"  arg1="current date"/>");
		    			document.frm.appointmentDate.focus();
		   				document.frm.appointmentDate.select();
		   				return false;
					}					
		   		}
			}	
    		if (isEmpty(document.frm.appointmentTime.value)) {
    			alert("<bean:message key="error.field.required" arg0="Appointment time"/>");
    			document.frm.appointmentTime.focus();
    			document.frm.appointmentTime.select();
				return false;
    		}
    		else if (!isValidTime(document.frm.appointmentTime.value)) {
		   		alert("<bean:message key="error.field.time" arg0="Appointment time"/>");
		    	document.frm.appointmentTime.focus();
		   		document.frm.appointmentTime.select();
		   		return false;
			}
    	}    	
    	if (document.frm.sevenDayMemo[0].checked){
    		if (isEmpty(document.frm.sevenDayMemoDate.value)) {
    			alert("<bean:message key="error.field.required" arg0="Seven day memo date"/>");
				document.frm.sevenDayMemoDate.focus();
				document.frm.sevenDayMemoDate.select();
				return false;
			}
			else if (!isValidDate(document.frm.sevenDayMemoDate.value)) {
		   		alert("<bean:message key="error.field.date" arg0="Seven day memo date"/>");
		   		document.frm.sevenDayMemoDate.focus();
		   		document.frm.sevenDayMemoDate.select();
				return false;
			}	
			if (noSelection(document.frm.applicationStatus.value)) {
				alert("<bean:message key="error.field.requiredselection" arg0="application status"/>"); //alert("Application Status is a required field.");
				document.frm.applicationStatus.focus();
				//document.frm.applicationStatus.select();
				return false;
			}
			if (isEmpty(document.frm.reason.value)) {
				alert("<bean:message key="error.field.required" arg0="Reason"/>");
				document.frm.reason.focus();
				document.frm.reason.select();
				return false;
			}
			if (noSelection(document.frm.section.value)) {
				alert("<bean:message key="error.field.requiredselection" arg0="section"/>"); //alert("Section is a required field.");
				document.frm.section.focus();
				//document.frm.section.select();
				return false;
			}
    	}    	    	
		if (noSelection(document.frm.reqParty.value)) {
			alert("<bean:message key="error.field.requiredselection" arg0="requesting party"/>"); //alert("Requesting Party is a required field.");
			document.frm.reqParty.focus();
			//document.frm.reqParty.select();
			return false;
		}    	
    	if (noSelection(document.frm.department.value)) { 
			alert("<bean:message key="error.field.requiredselection" arg0="department"/>"); //alert("Department is a required field.");
			document.frm.department.focus();
			//document.frm.department.select();
			return false;
		}
		//if (isNumeric(document.frm.amount.value)) { //drop-down?
		//	alert("<bean:message key="error.field.numeric" arg0="Amount"/>"); 
		//	document.frm.amount.focus();
		//	document.frm.amount.select();
		//	return false;
		//}		    	
		return true;
    }
    
    function validateConfirm(){
    	if (document.frm.actionType.value == "create"){
    		//////////////////
    		if (isEmpty(document.frm.clientNo.value) && !isEmpty(document.frm.refNo.value)){
	    		alert("<bean:message key="error.field.requiredselection" arg0="client number"/>");
				document.frm.clientNo.focus();
				return false;
	    	}
	    	if (isEmpty(document.frm.refNo.value) && !isEmpty(document.frm.clientNo.value)){
	    		alert("<bean:message key="error.field.requiredselection" arg0="reference number"/>");
				document.frm.refNo.focus();
				return false;
	    	}
			if (isEmpty(document.frm.refNo.value) && isEmpty(document.frm.clientNo.value)){
	    		if (isEmpty(document.frm.dateConducted.value)) { 
					alert("<bean:message key="error.field.required" arg0="Date conducted"/>"); //alert("Date conducted is a required field.");
					document.frm.dateConducted.focus();
					document.frm.dateConducted.select();
					return false;
				}
				else{
				 	if (!isValidDate(document.frm.dateConducted.value)){
						alert("<bean:message key="error.field.date" arg0="Date conducted"/>");
				    	//document.frm.dateConducted.focus();
				    	//document.frm.dateConducted.select();
			   			return false;
			   		}
			   		else{
			   			if (isGreaterThanCurrentDate(document.frm.dateConducted.value)){
			   				//error.field.beforeenddate = {0} should be earlier than or equal to the {1}.
			   				alert("<bean:message key="error.field.beforeenddate" arg0="Conducted date"  arg1="current date"/>");
	    					document.frm.dateConducted.focus();
	   						document.frm.dateConducted.select();
	   						return false;
			   			}
			   			else{	
				   			if (!isGreaterThanEqualToDate(document.frm.dateConducted.value, document.frm.dateRequested.value)){
				   				alert("<bean:message key="error.field.afterenddate" arg0="Conducted date"  arg1="requested date"/>");
		    					document.frm.dateConducted.focus();
	   							document.frm.dateConducted.select();
		   						return false;
			   				}
			   			}
			   		}
				}
				if (isEmpty(document.frm.dateReceived.value)) {
					alert("<bean:message key="error.field.required" arg0="Date received"/>"); //alert("Date Received is a required field.");
					//document.frm.dateReceived.focus();
					//document.frm.dateReceived.select();
					return false;
				}
				else if (!isValidDate(document.frm.dateReceived.value)){
					alert("<bean:message key="error.field.date" arg0="Date received"/>");
				    //document.frm.dateReceived.focus();
				    //document.frm.dateReceived.select();
			   		return false;
				}
				if (isEmpty(document.frm.remarks.value)) {
					alert("<bean:message key="error.field.required" arg0="Remarks"/>"); //alert("Remarks is a required field.");
					document.frm.remarks.focus();
					document.frm.remarks.select();
					return false;
				}
	    	}
    		//////////
    		if (noSelection(document.frm.status.value)) {
	    		alert("<bean:message key="error.field.requiredselection" arg0="status"/>");	//alert("Facilitator is a required field.");
				document.frm.status.focus();
				//document.frm.facilitator.select();
				return false;
			}    	
			if (document.frm.facilitator.disabled == false){
	    		if (noSelection(document.frm.facilitator.value)) {
	    			alert("<bean:message key="error.field.requiredselection" arg0="facilitator"/>");	//alert("Facilitator is a required field.");
					document.frm.facilitator.focus();
					//document.frm.facilitator.select();
					return false;
				}    	
			}
			if (noSelection(document.frm.agent.value)) {			
				alert("Please select an agent."); //alert("<bean:message key="error.field.requiredselection" arg0="agent"/>"); //alert("Agent Name is a required field.");
				document.frm.agent.focus();
				//document.frm.agent.select();
				return false;
			}    	
			if (noSelection(document.frm.branch.value)) {
				alert("<bean:message key="error.field.requiredselection" arg0="branch"/>"); //alert("Branch Name is a required field.");
				document.frm.branch.focus();
				//document.frm.branch.select();
				return false;
			}
			if (isEmpty(document.frm.clientNo.value)){
				if (isEmpty(document.frm.lastName.value)) {
					alert("<bean:message key="error.field.required" arg0="Last name"/>"); //alert("Last Name is a required field.");
					document.frm.lastName.focus();
					document.frm.lastName.select();
					return false;
				}		
				if (isEmpty(document.frm.birthDate.value)) {
					alert("<bean:message key="error.field.required" arg0="Birth date"/>"); //alert("Birth Date is a required field.");
					document.frm.birthDate.focus();
					document.frm.birthDate.select();
					return false;
			    }
				else if (!isValidDate(document.frm.birthDate.value)) {
					alert("<bean:message key="error.field.date" arg0="Birth date"/>");			   			   
					document.frm.birthDate.focus();
					document.frm.birthDate.select();
					return false;
				}
				if (isEmpty(document.frm.firstName.value)) {
					alert("<bean:message key="error.field.required" arg0="First name"/>"); //alert("First Name is a required field.");
					document.frm.firstName.focus();
					document.frm.firstName.select();
					return false;
				}				
			}			
			if (!isEmpty(document.frm.followUpDate.value)) {
				if (!isValidDate(document.frm.followUpDate.value)) {
			    	alert("<bean:message key="error.field.date" arg0="Follow-up date"/>"); //alert("Follow-Up Date is an invalid date.");
			   		document.frm.followUpDate.focus();
			   		document.frm.followUpDate.select();
			   		return false;
				}
	    	}
	    	if (document.frm.type[0].checked) {
				if (noSelection(document.frm.labName.value)) {
					alert("<bean:message key="error.field.requiredselection" arg0="laboratory"/>"); //alert("Laboratory Name is a required field.");
					document.frm.labName.focus();
					//document.frm.labName.select();
					return false;
				}
			}			
			if (document.frm.type[1].checked) {
				if (noSelection(document.frm.examiner.value)) {
					alert("<bean:message key="error.field.requiredselection" arg0="branch"/>"); //alert("Examiner Name is a required field.");
					document.frm.examiner.focus();
					//document.frm.examiner.select();
					return false;
				}		    			    	    	
			}
			if (noSelection(document.frm.test.value)) {
				alert("<bean:message key="error.field.requiredselection" arg0="test"/>"); //alert("Test is a required field.");
				document.frm.test.focus();
				//document.frm.test.select();
				return false;
			}
	    	if (document.frm.sevenDayMemo[0].checked){
	    		if (isEmpty(document.frm.sevenDayMemoDate.value)) {
	    			alert("<bean:message key="error.field.required" arg0="Seven day memo date"/>");
					document.frm.sevenDayMemoDate.focus();
					document.frm.sevenDayMemoDate.select();
					return false;
				}
				else if (!isValidDate(document.frm.sevenDayMemoDate.value)) {
			   		alert("<bean:message key="error.field.date" arg0="Seven day memo date"/>");
			   		document.frm.sevenDayMemoDate.focus();
			   		document.frm.sevenDayMemoDate.select();
					return false;
				}	
				if (noSelection(document.frm.applicationStatus.value)) {
					alert("<bean:message key="error.field.requiredselection" arg0="application status"/>"); //alert("Application Status is a required field.");
					document.frm.applicationStatus.focus();
					//document.frm.applicationStatus.select();
					return false;
				}
				if (isEmpty(document.frm.reason.value)) {
					alert("<bean:message key="error.field.required" arg0="Reason"/>");
					document.frm.reason.focus();
					document.frm.reason.select();
					return false;
				}
				if (noSelection(document.frm.section.value)) {
					alert("<bean:message key="error.field.requiredselection" arg0="section"/>"); //alert("Section is a required field.");
					document.frm.section.focus();
					//document.frm.section.select();
					return false;
				}
	    	}    	    	
			if (noSelection(document.frm.reqParty.value)) {
				alert("<bean:message key="error.field.requiredselection" arg0="requesting party"/>"); //alert("Requesting Party is a required field.");
				document.frm.reqParty.focus();
				//document.frm.reqParty.select();
				return false;
			}    	
	    	if (noSelection(document.frm.department.value)) { 
				alert("<bean:message key="error.field.requiredselection" arg0="department"/>"); //alert("Department is a required field.");
				document.frm.department.focus();
				//document.frm.department.select();
				return false;
			}
    	}//if (document.frm.actionType.value == "create")

		if (isEmpty(document.frm.appointmentDate.value)) {
			alert("<bean:message key="error.field.required" arg0="Appointment date"/>"); //alert("Appointment Date is a required field."); 
			document.frm.appointmentDate.focus();
			document.frm.appointmentDate.select();
			return false;
		}
		else {
			if (!isValidDate(document.frm.appointmentDate.value)) {
	   			alert("<bean:message key="error.field.date" arg0="Appointment date"/>");
	    		document.frm.appointmentDate.focus();
	   			document.frm.appointmentDate.select();
	   			return false;
	   		}
	   		else{
	   			//date format is ddMMMyyyy; date1 >= date2 returns true
				if (!isGreaterThanCurrentDate(document.frm.appointmentDate.value)){
					if (!isGreaterThanEqualToDate(document.frm.appointmentDate.value, document.frm.dateRequested.value)){
						//error.field.afterenddate = {0} should be later than or equal to the {1}.
						alert("<bean:message key="error.field.afterenddate" arg0="Appointment date" arg1="requested date"/>");
	    				document.frm.appointmentDate.focus();
	   					document.frm.appointmentDate.select();
	   					return false;
	   				}
				}					
	   		}
		}	
		if (isEmpty(document.frm.appointmentTime.value)) {
			alert("<bean:message key="error.field.required" arg0="Appointment time"/>");
			document.frm.appointmentTime.focus();
			document.frm.appointmentTime.select();
			return false;
		}
		else if (!isValidTime(document.frm.appointmentTime.value)) {
	   		alert("<bean:message key="error.field.time" arg0="Appointment time"/>");
	    	document.frm.appointmentTime.focus();
	   		document.frm.appointmentTime.select();
	   		return false;
		}

		return true;
    }
    
    function validateReceive(){
    	if (document.frm.actionType.value == "create"){
	    	if (noSelection(document.frm.status.value)) {
	    		alert("<bean:message key="error.field.requiredselection" arg0="status"/>");	//alert("Facilitator is a required field.");
				document.frm.status.focus();
				//document.frm.facilitator.select();
				return false;
			}
			if (document.frm.facilitator.disabled == false){    	
		    	if (noSelection(document.frm.facilitator.value)) {
		    		alert("<bean:message key="error.field.requiredselection" arg0="facilitator"/>");	//alert("Facilitator is a required field.");
					document.frm.facilitator.focus();
					//document.frm.facilitator.select();
					return false;
				}    	
			}
			if (noSelection(document.frm.agent.value)) {			
				alert("Please select an agent."); //alert("<bean:message key="error.field.requiredselection" arg0="agent"/>"); //alert("Agent Name is a required field.");
				document.frm.agent.focus();
				//document.frm.agent.select();
				return false;
			}    	
			if (noSelection(document.frm.branch.value)) {
				alert("<bean:message key="error.field.requiredselection" arg0="branch"/>"); //alert("Branch Name is a required field.");
				document.frm.branch.focus();
				//document.frm.branch.select();
				return false;
			}
			if (isEmpty(document.frm.clientNo.value)){
				if (isEmpty(document.frm.lastName.value)) {
					alert("<bean:message key="error.field.required" arg0="Last name"/>"); //alert("Last Name is a required field.");
					document.frm.lastName.focus();
					document.frm.lastName.select();
					return false;
				}		
				if (isEmpty(document.frm.birthDate.value)) {
					alert("<bean:message key="error.field.required" arg0="Birth date"/>"); //alert("Birth Date is a required field.");
					document.frm.birthDate.focus();
					document.frm.birthDate.select();
					return false;
			    }
				else if (!isValidDate(document.frm.birthDate.value)) {
					alert("<bean:message key="error.field.date" arg0="Birth date"/>");			   			   
					document.frm.birthDate.focus();
					document.frm.birthDate.select();
					return false;
				}
				if (isEmpty(document.frm.firstName.value)) {
					alert("<bean:message key="error.field.required" arg0="First name"/>"); //alert("First Name is a required field.");
					document.frm.firstName.focus();
					document.frm.firstName.select();
					return false;
				}				
			}			
			if (isEmpty(document.frm.remarks.value)) {
				alert("<bean:message key="error.field.required" arg0="Remarks"/>"); //alert("Remarks is a required field.");
				document.frm.remarks.focus();
				document.frm.remarks.select();
				return false;
			}
			if (!isEmpty(document.frm.followUpDate.value)) {
				if (!isValidDate(document.frm.followUpDate.value)) {
			    	alert("<bean:message key="error.field.date" arg0="Follow-up date"/>"); //alert("Follow-Up Date is an invalid date.");
			   		document.frm.followUpDate.focus();
			   		document.frm.followUpDate.select();
			   		return false;
				}
	    	}
	    	if (document.frm.type[0].checked) {
				if (noSelection(document.frm.labName.value)) {
					alert("<bean:message key="error.field.requiredselection" arg0="laboratory"/>"); //alert("Laboratory Name is a required field.");
					document.frm.labName.focus();
					//document.frm.labName.select();
					return false;
				}
			}			
			if (document.frm.type[1].checked) {
				if (noSelection(document.frm.examiner.value)) {
					alert("<bean:message key="error.field.requiredselection" arg0="branch"/>"); //alert("Examiner Name is a required field.");
					document.frm.examiner.focus();
					//document.frm.examiner.select();
					return false;
				}		    			    	    	
			}
			if (noSelection(document.frm.test.value)) {
				alert("<bean:message key="error.field.requiredselection" arg0="test"/>"); //alert("Test is a required field.");
				document.frm.test.focus();
				//document.frm.test.select();
				return false;
			}
			if (isEmpty(document.frm.dateConducted.value)) { 
				alert("<bean:message key="error.field.required" arg0="Date conducted"/>"); //alert("Date conducted is a required field.");
				document.frm.dateConducted.focus();
				document.frm.dateConducted.select();
				return false;
			}
			else{
			 	if (!isValidDate(document.frm.dateConducted.value)){
					alert("<bean:message key="error.field.date" arg0="Date conducted"/>");
			    	document.frm.dateConducted.focus();
			    	document.frm.dateConducted.select();
		   			return false;
		   		}
		   		else{
		   			if (isGreaterThanCurrentDate(document.frm.dateConducted.value)){
		   				//error.field.beforeenddate = {0} should be earlier than or equal to the {1}.
		   				alert("<bean:message key="error.field.beforeenddate" arg0="Conducted date"  arg1="current date"/>");
    					document.frm.dateConducted.focus();
   						document.frm.dateConducted.select();
   						return false;
		   			}
		   			else{	
			   			if (!isGreaterThanEqualToDate(document.frm.dateConducted.value, document.frm.dateRequested.value)){
			   				alert("<bean:message key="error.field.afterenddate" arg0="Conducted date"  arg1="requested date"/>");
	    					document.frm.dateConducted.focus();
   							document.frm.dateConducted.select();
	   						return false;
		   				}
		   			}
		   		}
			}
			if (isEmpty(document.frm.dateReceived.value)) {
				alert("<bean:message key="error.field.required" arg0="Date received"/>"); //alert("Date Received is a required field.");
				document.frm.dateReceived.focus();
				document.frm.dateReceived.select();
				return false;
			}
			else if (!isValidDate(document.frm.dateReceived.value)){
				alert("<bean:message key="error.field.date" arg0="Date received"/>");
			    document.frm.dateReceived.focus();
			    document.frm.dateReceived.select();
		   		return false;
			}
			if (noSelection(document.frm.reqParty.value)) {
				alert("<bean:message key="error.field.requiredselection" arg0="requesting party"/>"); //alert("Requesting Party is a required field.");
				document.frm.reqParty.focus();
				//document.frm.reqParty.select();
				return false;
			}    	
	    	if (noSelection(document.frm.department.value)) { 
				alert("<bean:message key="error.field.requiredselection" arg0="department"/>"); //alert("Department is a required field.");
				document.frm.department.focus();
				//document.frm.department.select();
				return false;
			}
    	}//if (document.frm.actionType.value == "create")
    	
    	if (document.frm.resultsReceivedInd[0].checked) {
    		if (isEmpty(document.frm.dateConducted.value)) { 
				alert("<bean:message key="error.field.required" arg0="Date conducted"/>"); //alert("Date conducted is a required field.");
				document.frm.dateConducted.focus();
				document.frm.dateConducted.select();
				return false;
			}
			else{
			 	if (!isValidDate(document.frm.dateConducted.value)){
					alert("<bean:message key="error.field.date" arg0="Date conducted"/>");
			    	//document.frm.dateConducted.focus();
			    	//document.frm.dateConducted.select();
		   			return false;
		   		}
		   		else{
		   			if (isGreaterThanCurrentDate(document.frm.dateConducted.value)){
		   				//error.field.beforeenddate = {0} should be earlier than or equal to the {1}.
		   				alert("<bean:message key="error.field.beforeenddate" arg0="Conducted date"  arg1="current date"/>");
    					document.frm.dateConducted.focus();
   						document.frm.dateConducted.select();
   						return false;
		   			}
		   			else{	
			   			if (!isGreaterThanEqualToDate(document.frm.dateConducted.value, document.frm.dateRequested.value)){
			   				alert("<bean:message key="error.field.afterenddate" arg0="Conducted date"  arg1="requested date"/>");
	    					document.frm.dateConducted.focus();
   							document.frm.dateConducted.select();
	   						return false;
		   				}
		   			}
		   		}
			}
			if (isEmpty(document.frm.dateReceived.value)) {
				alert("<bean:message key="error.field.required" arg0="Date received"/>"); //alert("Date Received is a required field.");
				//document.frm.dateReceived.focus();
				//document.frm.dateReceived.select();
				return false;
			}
			else if (!isValidDate(document.frm.dateReceived.value)){
				alert("<bean:message key="error.field.date" arg0="Date received"/>");
			    //document.frm.dateReceived.focus();
			    //document.frm.dateReceived.select();
		   		return false;
			}
			if (isEmpty(document.frm.remarks.value)) {
				alert("<bean:message key="error.field.required" arg0="Remarks"/>"); //alert("Remarks is a required field.");
				document.frm.remarks.focus();
				document.frm.remarks.select();
				return false;
			}
    	}
				    	
    	return true;
    }
    //END VALIDATION

//-->