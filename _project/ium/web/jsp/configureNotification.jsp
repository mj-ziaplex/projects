<%@ page language="java" import="java.util.*,com.slocpi.ium.data.*, com.slocpi.ium.ui.form.*, com.slocpi.ium.util.*, com.slocpi.ium.ui.util.*, java.text.* " buffer="12kb"%>
<jsp:useBean id="notificationTempForm" scope="request" class="com.slocpi.ium.ui.form.NotificationTempForm"/>
<%@ taglib uri="/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/struts-html.tld" prefix="html" %>
<%@ taglib uri="/ium.tld" prefix="ium" %>
<%String contextPath = request.getContextPath();%>
<%
/* Andre Ceasar Dacanay - css enhancement - start */
String iumCss = "";
String iumColorB = "";
String companyCode = "";
try {
	ResourceBundle rbWMSSP = ResourceBundle.getBundle("dbconfig");
	iumCss = rbWMSSP.getString("IUM_CSS_VERSION");
	companyCode = iumCss;
	iumCss = "_"+iumCss;
} catch (Exception e){}
if(iumCss.equals("_CP")){
	iumColorB="#2A4C7C";
	companyCode="";
}else if(iumCss.equals("_GF")){
	iumColorB="#303A77";
	companyCode = companyCode+" ";
}else{
	iumColorB="#2A4C7C";
}
/* Andre Ceasar Dacanay - css enhancement - end */
%>
<%
	StateHandler sessionHandler = new StateHandler();
    Access access = new Access();
    
    // This routine queries the access rights of the users (RCRIVERA)
    //
    // --- START --- 
    
	HashMap userAccess = sessionHandler.getUserAccess(request);
    if(!access.hasAccess(userAccess,IUMConstants.PAGE_NOTIFICATION_STATEMENT,IUMConstants.ACC_INQUIRE) ){                                                                                                                    
    	response.sendRedirect("noAccessPage.jsp");   
    } 
    
    String CREATE_ACCESS = "";
    String MAINTAIN_ACCESS = "";
    
        
    if(!access.hasAccess(userAccess,IUMConstants.PAGE_NOTIFICATION_STATEMENT,IUMConstants.ACC_CREATE)  ){                                                                                                                     
	  CREATE_ACCESS = "DISABLED"; 	
    }
    if(!access.hasAccess(userAccess,IUMConstants.PAGE_NOTIFICATION_STATEMENT,IUMConstants.ACC_MAINTAIN) ){                                                                                                                     
	  MAINTAIN_ACCESS = "DISABLED";
    }
   
    
    // --- END ---
 %>    
<%
UserData userData = sessionHandler.getUserData(request); 
UserProfileData userPref = userData.getProfile();
%>
<html>

<head>
<title><%=companyCode%>Integrated Underwriting and Medical System</title>
<link href="<%=contextPath%>/css/pw_main.css"     rel="stylesheet" type="text/css">
<link href="<%=contextPath%>/css/main<%=iumCss%>.css" rel="stylesheet" type="text/css">
<link href="<%=contextPath%>/css/styleCommon.css" rel="stylesheet" type="text/css">
<script language="JavaScript" src="<%=contextPath%>/js/dhtml_init.js"></script>
<script language="JavaScript" src="<%=contextPath%>/js/navigation.js"></script>
<script language="JavaScript" src="<%=contextPath%>/jsp/sl_left_nav.jsp"></script>
<script language="JavaScript" src="<%=contextPath%>/js/lookupjs.js"></script>
<script language="Javascript">
	
</script>
</head>

<BODY leftMargin=0 topmargin=0 marginwidth=0 marginheight=0 onLoad="javascript:consolidateList(); putButton(<%=userPref.getRecordsPerView()%>,<%=((ArrayList)notificationTempForm.getNotificationTemplates()).size()%>);">
<form name="frm" action="<%=contextPath%>/confNotificationTemp.do" method="post">   
<table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%">
<tr><td width="100%" colspan="2">
  <jsp:include page="header.jsp" flush="true"/>
<table border="0" cellpadding="0" cellspacing="0" width="100%" height="100%">
  <tr vAlign=top> 
    <TD background="<%=contextPath%>/images/bg_yellow.gif" id=leftnav vAlign=top width=147> 
      <IMG height=9 src="<%=contextPath%>/images/sp.gif" width=147><BR>
      <IMG height=30 src="<%=contextPath%>/images/sp.gif" width=7> <BR>
      <IMG height=7 src="<%=contextPath%>/images/sp.gif" width=147><BR>
      <DIV id=level0 style="POSITION: absolute; VISIBILITY: hidden; WIDTH: 100%; Z-INDEX: 500"> 
        <SCRIPT language=javascript>
			<!--
				document.write(writeMenu(''))
				if (IE4) { document.write(writeDiv()) };
				
			//-->
			</SCRIPT>
      </DIV>
      <DIV id=NSFix style="POSITION: absolute; VISIBILITY: hidden; Z-INDEX: 999">&nbsp;</DIV>
      <!-- Netscape needs a hard-coded DIV to write dynamic DIVs --> 
      <SCRIPT language=javascript>
                        <!--
                                if (NS4) { document.write(writeDiv()) }
                        //-->
                        </SCRIPT>
                        <SCRIPT>
                        <!--
                                initMenu();
                        //-->
                        </SCRIPT>
      <!-- END MENU CODE --> </td>

<input type="hidden" name="pageNo" value="<bean:write name='notificationTempForm' property='pageNo'/>">   
<input type="hidden" name="actionType" value="<bean:write name='notificationTempForm' property='actionType'/>">
<input type="hidden" name="mobileCounter">   
<input type="hidden" name="descCounter">
    <td width="100%"> 
      <div align="left"> 
        <table border="0" cellpadding="0" width="100%" cellspacing="0">
          <tr> 
            <td width="100%"> 
              <div align="left"> 
                <table border="0" cellpadding="3" cellspacing="0" width="100%" height="27"  background="<%=contextPath%>/images/sectionbkgray.jpg">
                  <tr> 
                    <td valign="top"> 
                       <span class="main">&nbsp;Integrated Underwriting and Medical System </span>
                    </td>
                  </tr>
                </table>
              </div>
            </td>
          </tr>
          <tr> 
            <td width="100%" colspan=2>&nbsp;</td>
          </tr>
        </table>
        <table width="100%" cellpadding="3" cellspacing="5" border="0">
          <tr valign="top">
         <td class="title2">Notification Template
         <table border="1" bordercolor="<%=iumColorB%>" cellpadding=10 cellspacing=0 bgcolor="" width="700" class="listtable1">
           <tr><td colspan="2" height="100%">
              <table border="0" cellpadding="2" cellspacing="1" bgcolor=#FFFFFF style="border: 0px solid #D1D1D1;" class="listtable1">
                <tr ><td>
                  <!--IFRAME src="tempAdminDetail.html" width="665" height="100" scrolling="no" frameborder="0" >
                  </IFRAME-->
        <%String disabledStatus = "true";%>
        <%String disabledStatus2 = "disabled";%>
        <%if (!"".equals(notificationTempForm.getActionType()) && ("maintain".equals(notificationTempForm.getActionType()) || "create".equals(notificationTempForm.getActionType()))) {%> 
        	<%disabledStatus = "false";
        	  disabledStatus2 = "";
        	%> 
        <%}%>        
        <table border="0" width="669" >
        <tr class="label2">
        <td class="label2"><b>&nbsp;&nbsp;Notification ID</b></td>
        <td><bean:write name="notificationTempForm" property="notificationId"/></td>
        <input type="hidden" name="notificationId" value="<%=notificationTempForm.getNotificationId()%>">
        </tr>
        <tr>
        <td width="150" class="label2" valign="top"><span class="required">*</span><b>Description</b></td>
        <td width="300"><input type="text" name="notificationDesc" maxlength="40" value="<bean:write name="notificationTempForm" property="notificationDesc"/>" size="30" class="label2" <%=disabledStatus2%>></td>
        <td width="100">&nbsp;</td>
        <td width="100">&nbsp;</td>
        </tr>        

        <tr>
        <td colspan="2" class="label2"><b>&nbsp;&nbsp;Email</b>&nbsp;</td>
        </tr>
        
        <tr>
        <td colspan="2" class="label2">
        <table border="0" cellpadding="2" cellspacing="1" bgcolor=#FFFFFF style="border: 1px solid #D1D1D1;" class="listtable1" width="500">
        <tr>
        <td class="label2" colspan="3 width="100" height="2"></td>
        </tr>
	  <tr>
	  <td class="label2" width="100" valign="top"><b>&nbsp;&nbsp;Subject</b></td>
        <td class="label2" colspan="2"><input name="subject" type="text" maxlength="100" size="30" class="label2" value="<bean:write name="notificationTempForm" property="subject"/>" <%=disabledStatus2%>/></td>	
	  </tr>
        <tr>
        <td class="label2" width="100" valign="top"><b>&nbsp;&nbsp;Content</b></td>
        <td class="label2" width="250" colspan="2"><textarea class="label2" rows="5" cols="50" name="emailContent" <%=disabledStatus2%>><bean:write name="notificationTempForm" property="emailContent"/></textarea></td>
        </tr>
        
        
        <tr>
        <td class="label2" colspan="3" width="100" height="2"></td>
        </tr>
        
        </table>
        </td>
        </tr>

		<%String checkedIndYes = "";
		  String checkedIndNo  = "checked";
		  String disableContent = "disabled";	
			if (notificationTempForm.getMobileInd() != null && IUMConstants.YES.equals(notificationTempForm.getMobileInd())) {
				checkedIndYes="checked";
				checkedIndNo = "";
				if (disabledStatus2.equals("")) {
					disableContent = "";
				}
			}
		%>
        <tr>
        <td class="label2"><b>&nbsp;&nbsp;Notify Mobile ?</b></td>
        <td class="label2"><input type="radio" name="mobileInd" value="<%=IUMConstants.YES%>" onClick="javascript:toggleMobileContent();" <%=disabledStatus2%> <%=checkedIndYes%>>Yes&nbsp;<input type="radio" name="mobileInd" value="" onClick="javascript:toggleMobileContent();" <%=disabledStatus2%> <%=checkedIndNo%>>No&nbsp; </td>
        </tr>

        <tr>
        <td colspan="2" class="label2"><b>&nbsp;&nbsp;Mobile</b>&nbsp;</td>
        </tr>

        <tr>
        <td colspan="2" class="label2">
        <table border="0" cellpadding="2" cellspacing="1" bgcolor=#FFFFFF style="border: 1px solid #D1D1D1;" class="listtable1" width="500">
        <tr>
        <td class="label2" colspan="3 width="100" height="2"></td>
        </tr>
        <tr>
        <td class="label2" width="100" valign="top"><b>&nbsp;&nbsp;Content</b></td>
        <td class="label2" width="250" colspan="2"><textarea name="mobileContent" class="label2" rows="5" cols="50" <%=disableContent%> onkeyup="textCounter(this,this.form.mobileCounter,320);"><bean:write name="notificationTempForm" property="mobileContent"/></textarea></td>
        </tr>
        
        
        
        <tr>
        <td class="label2" colspan="3" width="100" height="2"></td>
        </tr>
        
        </table>
        </td>
        </tr>
        
        
        <tr>
        <td colspan="2" class="label2">&nbsp;</td>
        </tr>
		
		<tr><td colspan="2" class="label2">
			<table border="0" cellpadding="2" cellspacing="1" bgcolor=#FFFFFF style="border: 1px solid #D1D1D1;" class="listtable1" width="600">
			<tr>
	        <td class="label2" width="200" valign="top"><b>&nbsp;&nbsp;Recipient<br>&nbsp;&nbsp;(Individual Life)</b></td>
	        <td class="label2" width="100" >
	        <ium:list className="label2" listBoxName="roleIL1" type="<%=IUMConstants.LIST_ROLES%>" disabled="<%=disabledStatus%>"/>             
	        </td>
	        <td valign="top" width="300">
	        <input type="button" name="addRoleIL" value="Add" class="button1" onClick="javascript:add(document.forms[0].roleIL1,document.forms[0].roleIL2);" <%=disabledStatus2%>>
	        </td>
	        </tr>
	        <tr>
	        <td class="label2" width="100" valign="top"><b>&nbsp;</b></td>
	        <td class="label2" width="100" >
	        <select class="label2" size="5" name="roleIL2" <%=disabledStatus2%> multiple>	     
	        <logic:present name="notificationTempForm" property="roleIL2">
	        <logic:iterate id="roleIL" name="notificationTempForm" property="roleIL2">
	        	<option value="<bean:write name="roleIL"/>"></option>
	        </logic:iterate>
	        </logic:present>
	        </select>                
	        </td>
	        <td valign="top" width="300">
	        <input type="button" name="removeRoleIL" value="Remove" class="button1" onClick="javascript:remove(document.forms[0].roleIL1,document.forms[0].roleIL2);" <%=disabledStatus2%>>
	        </td>
	        </tr>
	        <tr>
	        <td class="label2" width="200" valign="top"><b>&nbsp;&nbsp;Recipient<br>&nbsp;&nbsp;(Group Life)</b></td>
	        <td class="label2" width="100" >
	        <ium:list className="label2" listBoxName="roleGL1" type="<%=IUMConstants.LIST_ROLES%>" disabled="<%=disabledStatus%>"/>             
	        </td>
	        <td valign="top" width="300">
	        <input type="button" name="addRoleGL" value="Add" class="button1" onClick="javascript:add(document.forms[0].roleGL1,document.forms[0].roleGL2);" <%=disabledStatus2%>>
	        </td>
	        </tr>
	        <tr>
	        <td class="label2" width="100" valign="top"><b>&nbsp;</b></td>
	        <td class="label2" width="100" >
	        <select class="label2" size="5" name="roleGL2" <%=disabledStatus2%> multiple>	       
	        <logic:present name="notificationTempForm" property="roleGL2">
	        <logic:iterate id="roleGL" name="notificationTempForm" property="roleGL2">
	        	<option value="<bean:write name="roleGL"/>"></option>
	        </logic:iterate>
	        </logic:present>
	        </select>                
	        </td>
	        <td valign="top" width="300">
	        <input type="button" name="removeRoleGL" value="Remove" class="button1" onClick="javascript:remove(document.forms[0].roleGL1,document.forms[0].roleGL2);" <%=disabledStatus2%>>
	        </td>
	        </tr>
	        <tr>
	        <td class="label2" width="200" valign="top"><b>&nbsp;&nbsp;Recipient<br>&nbsp;&nbsp;(Pre-Need)</b></td>
	        <td class="label2" width="100" >
	        <ium:list className="label2" listBoxName="rolePN1" type="<%=IUMConstants.LIST_ROLES%>" disabled="<%=disabledStatus%>"/>             
	        </td>
	        <td valign="top" width="300">
	        <input type="button" name="addRolePN" value="Add" class="button1" onClick="javascript:add(document.forms[0].rolePN1,document.forms[0].rolePN2);" <%=disabledStatus2%>>
	        </td>
	        </tr>
	        <tr>
	        <td class="label2" width="100" valign="top"><b>&nbsp;</b></td>
	        <td class="label2" width="100" >
	        <select class="label2" size="5" name="rolePN2" <%=disabledStatus2%> multiple>	        
	        <logic:present name="notificationTempForm" property="rolePN2">
	        <logic:iterate id="rolePN" name="notificationTempForm" property="rolePN2">
	        	<option value="<bean:write name="rolePN"/>"></option>
	        </logic:iterate>
	        </logic:present>
	        </select>                
	        </td>
	        <td valign="top" width="300">
	        <input type="button" name="removeRolePN" value="Remove" class="button1" onClick="javascript:remove(document.forms[0].rolePN1,document.forms[0].rolePN2);" <%=disabledStatus2%>>
	        </td>
	        </tr>
			</table>
		</td>	
		</tr>
	  </table>
        </td></tr>	
        <tr><td colspan="5" align="left">
        <input type="submit" name="save" value="Save" class="button1" onClick="javascript:return(create2());" <%=disabledStatus2%>>&nbsp;
        <input type="button" name="cancel" value="Cancel" class="button1" onClick="javascript:rePaginate('<%=notificationTempForm.getPageNo()%>');" <%=disabledStatus2%>>

        </td></tr>

              </table>
           </td></tr>
           </table>     

          
          </td>
          </tr>
          <tr>
          <td colspan="10">
			<div  align="left">
		<%if (disabledStatus.equals("true")) {%>
		<input type="button" name="create" value="Create" class="button1" onClick="javascript:enableEdit();" <%=CREATE_ACCESS%>>&nbsp;
		<logic:notEmpty name="notificationTempForm" property="notificationTemplates">
        <input type="submit" name="maintain1" value="Maintain" class="button1" onClick="javascript:return(maintain());" <%=MAINTAIN_ACCESS%>>
        </logic:notEmpty>
        <logic:empty name="notificationTempForm" property="notificationTemplates">
        <input type="submit" name="maintain1" value="Maintain" class="button1" onClick="javascript:return(maintain());" <%=MAINTAIN_ACCESS%> disabled>
        </logic:empty>
        <%}%>
		    </div>
		 </td>
   
	     </tr>
          <tr valign="top"> 
          	          
            <td colspan="2"> 
        

            
<!--- START OF BODY -->
<table border="1" bordercolor="<%=iumColorB%>" cellpadding=10 cellspacing=0 bgcolor="" width="700" class="listtable1">
        <tr><td colspan="2" height="100%">
        <table border="0" cellpadding="2" cellspacing="1" bgcolor=#FFFFFF style="border: 1px solid #D1D1D1;" class="listtable1">
        <tr class="headerrow1">
	  <td width="20">&nbsp;</td>	
        <td width="250">NOTIFICATION ID</td>
        <td width="500">DESCRIPTION</td>
        </tr>
        <%int i = 0;%>
        <logic:present name="notificationTempForm" property="notificationTemplates">
        	<logic:iterate id="notTemplate" name="notificationTempForm" property="notificationTemplates" type="com.slocpi.ium.util.NameValuePair">
        		<%if (i%2 == 0) {%>
        		<tr class="row1">
        		<%}else {%>
        		<tr class="row2">
        		<%}%>
				  <td><input type="radio" name="notId" value="<%=notTemplate.getValue()%>"/></td>
				  <td><bean:write name="notTemplate" property="value"/></td>
				  <td><bean:write name="notTemplate" property="name"/></td>		
				</tr>
				<%i++;%>
        	</logic:iterate>
        </logic:present>        
       
<!-- START Number of pages -->	
<logic:present name="notificationTempForm" property="pages">	
  <tr>
  	<td class="headerrow4" colspan="10" align="left" width="100%" >
          <table border="0" cellpadding="0" cellspacing="0"  width="100%" >
              <tr>
              <td class="headerrow4" width="100%" height="10" class="label2" valign="bottom" colspan="14"> 
              <bean:size id="noOfPages" name="notificationTempForm" property="pages" />                                             
              <%
              int pageNumber = Integer.parseInt(notificationTempForm.getPageNo());             
              int currLink = 1;
              int prevLink = 1;
              int firstPage = 1;
              int lastPage = noOfPages.intValue();
              %>

              <!-- don't display link for previous page if the current page is the first page -->
              <% if (pageNumber > firstPage) { %>
                <a href="#" onClick="javascript:rePaginate(1);"><img src="<%=contextPath%>/images/pagnavfirst_off.gif" name="pagnavfirst" border="0" width="18" height="13"/></a>&nbsp;					
                <a href="#" onClick="javascript:rePaginate('<%=pageNumber-1%>');"><img src="<%=contextPath%>/images/pagnavleft_off.gif" name="pagnavleft" border="0" width="18" height="13"/></a>
              <% } else {%>
                <img src="<%=contextPath%>/images/pagnavfirst_on.gif" name="pagnavfirst" border="0" width="18" height="13"/>&nbsp;
                <img src="<%=contextPath%>/images/pagnavleft_on.gif" name="pagnavleft" border="0" width="18" height="13"/>
              <% } %>

              <logic:iterate id="navLinks" name="notificationTempForm" property="pages">                                                            
                <% currLink = ((Integer)navLinks).intValue();%>
                <% if (((Integer)navLinks).intValue() == pageNumber) { %>
                  <b><bean:write name="navLinks"/></b>
                <% } else { %>
                  <% if ((currLink > (prevLink+1))) { %>
                  ...
                  <% } %>
                  <a href="#" onClick="rePaginate('<bean:write name="navLinks"/>');" class="links2"><bean:write name="navLinks"/></a>
                <% } %>
                <%prevLink = currLink;%>
              </logic:iterate>

              <!-- don't display link for next page if the current page is the last page -->
              <% if(pageNumber < lastPage) { %>
                <a href="#" onClick="rePaginate('<%=pageNumber+1%>');"><img src="<%=contextPath%>/images/pagnavright_off.gif" name="pagnavright" border="0" width="18" height="13"/></a>&nbsp;
                <a href="#" onClick="rePaginate('<%=prevLink%>');"><img src="<%=contextPath%>/images/pagnavlast_off.gif" width="18" height="13" border="0"></a>
              <% } else { %>
                <img src="<%=contextPath%>/images/pagnavright_on.gif" width="18" height="13" border="0">&nbsp;
                <img src="<%=contextPath%>/images/pagnavlast_on.gif" width="18" height="13" border="0">
              <% } %>
              </td>
            </tr>
          </table>
      </td>
   </tr>   	
</logic:present>    
<!-- END Number of pages -->		   
</table>

<!--- END OF BODY -->
</td>
</tr>
</table>
<table>
<tr>
	<td colspan="10" align="left">
	<div id = "create_btn" align="left">
		<%if (disabledStatus.equals("true")) {%>
		<input type="button" name="create" value="Create" class="button1" onClick="javascript:enableEdit();" <%=CREATE_ACCESS%>>&nbsp;
		<logic:notEmpty name="notificationTempForm" property="notificationTemplates">
        <input type="submit" name="maintain1" value="Maintain" class="button1" onClick="javascript:return(maintain());" <%=MAINTAIN_ACCESS%>>
        </logic:notEmpty>
        <logic:empty name="notificationTempForm" property="notificationTemplates">
        <input type="submit" name="maintain1" value="Maintain" class="button1" onClick="javascript:return(maintain());" <%=MAINTAIN_ACCESS%> disabled>
        </logic:empty>
        <%}%>
    </div>
	</td>
</tr>
</table>
</div>
</form>
</body>
</html>