<%@ page language="java" import="com.slocpi.ium.util.*" buffer="12kb"%>
<%@ taglib uri="/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/ium.tld" prefix="ium" %>
<%@page language="java" import="java.util.*" %>
<%String contextPath = request.getContextPath();%>
<jsp:useBean id="purgeRecordsForm" scope="request" class="com.slocpi.ium.ui.form.PurgeRecordsForm"/>
<html>
<head>
<%
/* Andre Ceasar Dacanay - css enhancement - start */
String iumCss = "";
String iumColorB = "";
String companyCode = "";
try {
	ResourceBundle rbWMSSP = ResourceBundle.getBundle("dbconfig");
	iumCss = rbWMSSP.getString("IUM_CSS_VERSION");
	companyCode = iumCss;
	iumCss = "_"+iumCss;
} catch (Exception e){}
if(iumCss.equals("_CP")){
	iumColorB="#2A4C7C";
	companyCode="";
}else if(iumCss.equals("_GF")){
	iumColorB="#303A77";
	companyCode = companyCode+" ";
}else{
	iumColorB="#2A4C7C";
}
/* Andre Ceasar Dacanay - css enhancement - end */
%>
<title><%=companyCode%>Integrated Underwriting and Medical System</title>
<link href="<%=contextPath%>/css/pw_main.css"     rel="stylesheet" type="text/css">
<link href="<%=contextPath%>/css/main<%=iumCss%>.css"        rel="stylesheet" type="text/css">
<link href="<%=contextPath%>/css/styleCommon.css" rel="stylesheet" type="text/css">
<script language="JavaScript" src="<%=contextPath%>/js/dhtml_init.js"></script>
<script language="JavaScript" src="<%=contextPath%>/js/navigation.js"></script>
<script language="JavaScript" src="<%=contextPath%>/js/validation.js"></script>
<script language="JavaScript" src="<%=contextPath%>/jsp/sl_left_nav.jsp"></script>
<script language="JavaScript">
	function purgeOldData(contextPath) {	  
	    var form = document.forms[0];
	    if (validateFields(form)) {
		    form.action = contextPath + "/purgeRecords.do";
		    form.submit();
		}
		else {
		    return;
		}
	}
	
	function validateFields(form) {
		if (trim(form.startDate.value) != "") {
			if (!isValidDate(form.startDate.value)) {
			   alert("<bean:message key="error.field.date" arg0="Start Date"/>");
			   form.startDate.focus();
			   form.startDate.select();
			   return false;
			}
	    }
		else {
		    alert("<bean:message key="error.field.required" arg0="Start Date"/>");	    
		    form.startDate.value = "";
		    form.startDate.focus();
	        return false;
		}
	    
		if (trim(form.endDate.value) != "") {
			if (!isValidDate(form.endDate.value)) {
			   alert("<bean:message key="error.field.date" arg0="End Date"/>");
			   form.endDate.focus();
			   form.endDate.select();
			   return false;
			}
	    }
		else {
		    alert("<bean:message key="error.field.required" arg0="End Date"/>");	    
		    form.endDate.value = "";
		    form.endDate.focus();
	        return false;
		}
	
	    if (isGreaterDate(form.startDate.value, form.endDate.value)) {
		    alert("<bean:message key="error.field.earlierthanequalto" arg0="Start Date" arg1="End Date"/>");	    
		    form.startDate.focus();
		    form.startDate.select();
	        return false;    
	    }
	    
	    if (trim(form.criteria.value) == "") {
		    alert("<bean:message key="error.field.required" arg0="Criteria"/>");	    
		    form.criteria.focus();
	        return false;	    
	    }	
	    return true;
		
	}
	
	function onLoadPage() {
	    <% if (request.getAttribute("mode") == null) { %>
	        hideStatistics();
	    <% } else { %>
	        showStatistics();
	    <% } %>
	}
	
	function showStatistics() {
        document.all["statisticsLogTitle"].style.display = "block";		
        document.all["statisticsSection"].style.display  = "block";	
	}

	function hideStatistics(){
        document.all["statisticsLogTitle"].style.display = "none";			
		document.all["statisticsSection"].style.display  = "none";
	}
</script>
</head>

<BODY leftMargin=0 topmargin=0 marginwidth=0 marginheight=0 onLoad="onLoadPage();">
<table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%">
<tr><td width="100%" colspan="2">
<jsp:include page="header.jsp" flush="true"/>

          <table border="0" cellpadding="0" cellspacing="0" width="100%" height="100%">
            <tr valign="top">
              <td background="<%=contextPath%>/images/bg_yellow.gif" id="leftnav" valign="top" width="147">
                <img height="9" src="<%=contextPath%>/images/sp.gif" width="147"><br>
                <img height="30" src="<%=contextPath%>/images/sp.gif" width="7"> <br>
                <img height="7" src="<%=contextPath%>/images/sp.gif" width="147"><br>
                <div id="level0" style="position: absolute; visibility: hidden; width: 100%; z-index: 500"> 
        <SCRIPT language=javascript>
			<!--
				document.write(writeMenu(''))
				if (IE4) { document.write(writeDiv()) };
				
			//-->
			</SCRIPT>
      </DIV>
      <DIV id=NSFix style="POSITION: absolute; VISIBILITY: hidden; Z-INDEX: 999">&nbsp;</DIV>
      <!-- Netscape needs a hard-coded DIV to write dynamic DIVs --> 
      <SCRIPT language=javascript>
                        <!--
                                if (NS4) { document.write(writeDiv()) }
                        //-->
                        </SCRIPT>
                        <SCRIPT>
                        <!--
                                initMenu();
                        //-->
                        </SCRIPT>
      <!-- END MENU CODE --> </td>
<form name="frm" method="post">
    <td width="100%"> 
      <div align="left"> 
        <table border="0" cellpadding="0" width="100%" cellspacing="0">
          <tr> 
            <td width="100%"> 
              <div align="left"> 
                <table border="0" cellpadding="3" cellspacing="0" width="100%" height="27"  background="<%=contextPath%>/images/sectionbkgray.jpg">
                  <tr> 
                    <td valign="top"> 
                       <span class="main">&nbsp;Integrated Underwriting and Medical System </span>
                    </td>
                  </tr>
                </table>
              </div>
            </td>
          </tr>
          <tr> 
            <td width="100%" colspan=2>&nbsp;</td>
          </tr>
          <tr> 
            <td width="100%" colspan=2 class="title2">Purge Old Data</td>
          </tr>
        </table>
        <table width="100%" cellpadding="3" cellspacing="5" border="0">
          <tr valign="top"> 
            <td> 
		<table border="1" bordercolor="<%=iumColorB%>" cellpadding=10 cellspacing=0 bgcolor="" width="500" class="listtable1">
        <tr><td colspan="2" height="100%">
        <table border="0" cellpadding="2" cellspacing="1" bgcolor=#FFFFFF style="border: 0px solid #D1D1D1;" class="listtable1">
        <!-- Content of Filter -->
        <tr>
        <td class="label2"><b>Start Date</b><td/>
        <td>
            <input type="text" class="inputtext1" name="startDate" maxLength="9" onKeyUp="getKeyDate(event,this);"  value="<bean:write name="purgeRecordsForm" property="startDate"/>">
            <a href="#" onClick="popUpWin('<%=contextPath%>/jsp/calendar.jsp?textbox=frm.startDate',290,155);"><img src="<%=contextPath%>/images/calendar.gif" height="17" width="19" border="0"></a>            
        </td>
        <td>&nbsp;</td>
        </tr>
        <tr>
        <td class="label2" valign="top"><b>End Date</b><td/>
        <td>
            <input type="text" class="inputtext1" name="endDate"  maxLength="9" onKeyUp="getKeyDate(event,this);"  value="<bean:write name="purgeRecordsForm" property="endDate"/>">
            <a href="#" onClick="popUpWin('<%=contextPath%>/jsp/calendar.jsp?textbox=frm.endDate',290,155);"><img src="<%=contextPath%>/images/calendar.gif" height="17" width="19" border="0"></a>            
        </td>
        <td>&nbsp;</td>        
        </tr>
        <tr>
        <td class="label2" valign="top"><b>Criteria</b><td/>
        <td><ium:list className="label2" listBoxName="criteria" type="<%=IUMConstants.LIST_PURGE_CRITERIA%>" styleName="width:200" selectedItem="<%=purgeRecordsForm.getCriteria()%>" onChange=""/>
        </td>
        <td>&nbsp;</td>        
        </tr>        
        <tr><td colspan="3" align="right">&nbsp;</td></tr>
        <tr><td colspan="3" align="left">
        <input type="button" value="Purge"      class="button1" name="purgeBtn" onclick="purgeOldData('<%=contextPath%>');">&nbsp;
        <input type="button" value="Cancel"     class="button1" name="cancelBtn" onclick="gotoPage('frm','<%=contextPath%>/admin.do');">
        </td></tr> 
        <!-- End of Content Filter-->
        </table>
        </td>
        </tr>
    </table>
</form>
</td>    
</tr>
</table>
<br>
<br>

<table>
<tr id="statisticsLogTitle">
<td colspan="4" class="label2"><b>Statistics Log</b></td>
</tr>
<tr id="statisticsSection">
<td>
<table width="500" class="listtable1">
        <tr>
        <td>
        <table width="500">
        <tr class="label2">
        <td width="100"><b>Purge Date &nbsp;</b><td/>
        <td width="1"><b>:</b>
        <td width="170"><bean:write name="purgeRecordsForm" property="purgeDate"/></td>
        <td colspan="4">&nbsp;</td>
        </tr>
        <tr class="label2">
        <td valign="top" width="100"><b>Start Date &nbsp;</b><td/>
        <td width="1"><b>:</b>        
        <td width="170"><bean:write name="purgeRecordsForm" property="startDate"/></td>        
        <td width="18">&nbsp;</td>
        <td valign="top" width="130"><b>End Date &nbsp;</b><td/>
        <td width="1"><b>:</b>                
        <td width="100"><bean:write name="purgeRecordsForm" property="endDate"/></td>
        </tr>        
        <tr class="label2">
        <td valign="top" width="100"><b>Criteria &nbsp;</b><td/>
        <td width="1"><b>:</b>        
        <td width="170"><bean:write name="purgeRecordsForm" property="criteria"/></td>
        <td width="18">&nbsp;</td>        
        <td valign="top" width="110"><b>No. of Records &nbsp;</b><td/>
        <td width="1"><b>:</b>                
        <td width="100"><bean:write name="purgeRecordsForm" property="numOfRec"/></td>        
        </tr>                
        <tr class="label2">
        <td width="100"><b>Result &nbsp;</b><td/>
        <td width="1"><b>:</b>        
        <td width="170"><bean:write name="purgeRecordsForm" property="result"/></td>
        <td colspan="4">&nbsp;</td>
        </tr>        
        <table>        
        </td>
        </tr>
</table>        
</td></tr>
<!--- END OF BODY -->
</table>
</body>
</html>