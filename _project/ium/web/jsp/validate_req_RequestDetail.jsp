<%@ page language="java" import="com.slocpi.ium.util.IUMConstants, com.slocpi.ium.util.DateHelper" buffer="12kb"%>
<%@ taglib uri="/struts-bean.tld" prefix="bean" %>

<!--
var assignToForm = "";

function enableDocumentDetails() {
	var form = document.requestForm;
	enable(form.docCode);
	enable(form.dateSubmit);
	enable(form.dateRequested);
	document.all["doc_cal"].style.display = "inline";
	document.all["doc_cal_req"].style.display = "inline";
	
	form.dateSubmit.value = "<%=DateHelper.getCurrentDate()%>";
	form.dateRequested.value = "<%=DateHelper.getCurrentDate()%>";

	showSaveCancelDoc();
	hideAddDocument();
}


function disableDocumentDetails() {
	var form = document.requestForm;
	disable(form.docCode);
	disable(form.dateSubmit);
	disable(form.dateRequested);
	document.all["doc_cal"].style.display = "none";
	document.all["doc_cal_req"].style.display = "none";
	
	form.docCode.selectedIndex = 0;
	form.dateSubmit.value = "";
	form.dateRequested.value = "";

	hideSaveCancelDoc();
	showAddDocument();
}


function showSaveCancelDoc() {
	document.all["saveCancelDoc"].style.display = "block";
}

	
function hideSaveCancelDoc() {
	document.all["saveCancelDoc"].style.display = "none";
}



function showAddDocument() {
	document.all["addDocument"].style.display = "block";
}

	
function hideAddDocument() {
	document.all["addDocument"].style.display = "none";
}


function saveDocument() {
	var form = document.requestForm;

	if (noSelection(form.docCode.value)) {
		alert("<bean:message key="error.field.specificrequired" arg0="Document"/>");
		form.docCode.focus();
	}
	else if (isEmpty(form.dateRequested.value)) {
		alert("<bean:message key="error.field.specificrequired" arg0="Date requested"/>");
		form.dateRequested.select();
		form.dateRequested.focus();
	}
	else if (!isValidDate(form.dateRequested.value)) {
		alert("<bean:message key="error.field.format" arg0="Date requested" arg1="date" arg2=""/>");
		form.dateRequested.select();
		form.dateRequested.focus();
	}
	else if (isGreaterDate(form.dateRequested.value, "<%=DateHelper.getCurrentDate()%>")) {
		alert("<bean:message key="error.field.beforecurrentdate" arg0="Date requested"/>");
		form.dateRequested.select();
		form.dateRequested.focus();
	}
	else if (isEmpty(form.dateSubmit.value)) {
		alert("<bean:message key="error.field.specificrequired" arg0="Date submitted"/>");
		form.dateSubmit.select();
		form.dateSubmit.focus();
	}
	else if (!isValidDate(form.dateSubmit.value)) {
		alert("<bean:message key="error.field.format" arg0="Date submitted" arg1="date" arg2=""/>");
		form.dateSubmit.select();
		form.dateSubmit.focus();
	}
	else if (isGreaterDate(form.dateSubmit.value, "<%=DateHelper.getCurrentDate()%>")) {
		alert("<bean:message key="error.field.beforecurrentdate" arg0="Date submitted"/>");
		form.dateSubmit.select();
		form.dateSubmit.focus();
	}
	else {
		form.docDesc.value = form.docCode.options[form.docCode.selectedIndex].text;
		gotoPage("requestForm", "updateDocumentList.do");
	}
}


function enableRequestDetails() {
	var form = document.requestForm;
	var currentStatus = form.currentStatus.value;
	var status = form.requestStatus.options[form.requestStatus.selectedIndex].value;
	
	if (currentStatus == "<%=IUMConstants.STATUS_NB_REVIEW_ACTION%>") {
		enable(form.agentCode);
		enable(form.branchCode);	
		enable(form.insuredClientNo);
		if (form.lob.value == "<%=IUMConstants.LOB_INDIVIDUAL_LIFE%>") {
			enable(form.sameClientDataInd);		
			//enable(form.insuredClientType);
		}	
		enable(form.insuredLastName);
		enable(form.insuredFirstName);
		enable(form.insuredMiddleName);
		enable(form.insuredTitle);
		enable(form.insuredSuffix);
		enable(form.insuredBirthDate);
		enable(form.insuredSex[0]);
		enable(form.insuredSex[1]);
		document.all[""].style.display = "block";
		document.all[""].style.display = "block";
		
		if (form.lob.value == "<%=IUMConstants.LOB_INDIVIDUAL_LIFE%>") {
			//enable(form.ownerClientType);
			if (! form.sameClientDataInd.checked) {
				enable(form.ownerClientNo);			
				enable(form.ownerLastName);
				enable(form.ownerFirstName);
				enable(form.ownerMiddleName);
				enable(form.ownerTitle);
				enable(form.ownerSuffix);
				enable(form.ownerBirthDate);
				enable(form.ownerSex[0]);
				enable(form.ownerSex[1]);
				document.all["req_cal2"].style.display = "block";
			}
		}
		enable(form.amountCovered);
		enable(form.premium);
		enable(form.receivedDate);
	}
	else {
		if ((currentStatus == "<%=IUMConstants.STATUS_FOR_TRANSMITTAL_USD%>") || 
			(currentStatus == "<%=IUMConstants.STATUS_AWAITING_REQUIREMENTS%>") ||
			(currentStatus == "<%=IUMConstants.STATUS_AWAITING_MEDICAL%>")) {
			enable(form.dateForwarded);
			document.all["req_cal3"].style.display = "block";
		}
		
		if (form.lob.value == "<%=IUMConstants.LOB_INDIVIDUAL_LIFE%>") {
			//enable(form.insuredClientType);
			//enable(form.ownerClientType);
		}
	}

	enable(form.requestStatus);
	enable(form.assignedTo);
}

      
function disableRequestDetails() {
	var form = document.requestForm;
	var currentStatus = form.currentStatus.value;
	var status = form.requestStatus.options[form.requestStatus.selectedIndex].value;	
	
	if (currentStatus == "<%=IUMConstants.STATUS_NB_REVIEW_ACTION%>") {
		disable(form.agentCode);
		disable(form.branchCode);	
		disable(form.insuredClientNo);
		if (form.lob.value == "<%=IUMConstants.LOB_INDIVIDUAL_LIFE%>") {
			disable(form.sameClientDataInd);
			//disable(form.insuredClientType);
		}
		disable(form.insuredLastName);
		disable(form.insuredFirstName);
		disable(form.insuredMiddleName);
		disable(form.insuredTitle);
		disable(form.insuredSuffix);
		disable(form.insuredBirthDate);
		disable(form.insuredSex[0]);
		disable(form.insuredSex[1]);
		document.all["req_cal1"].style.display = "none";

		if (form.lob.value == "<%=IUMConstants.LOB_INDIVIDUAL_LIFE%>") {
			//disable(form.ownerClientType);		
			disable(form.ownerClientNo);
			disable(form.ownerLastName);
			disable(form.ownerFirstName);
			disable(form.ownerMiddleName);
			disable(form.ownerTitle);
			disable(form.ownerSuffix);
			disable(form.ownerBirthDate);
			disable(form.ownerSex[0]);
			disable(form.ownerSex[1]);
			document.all["req_cal2"].style.display = "none";
		}
		disable(form.amountCovered);
		disable(form.premium);
		disable(form.receivedDate);
	}
	else {
		if ((currentStatus == "<%=IUMConstants.STATUS_FOR_TRANSMITTAL_USD%>") || 
			(currentStatus == "<%=IUMConstants.STATUS_AWAITING_REQUIREMENTS%>") ||
			(currentStatus == "<%=IUMConstants.STATUS_AWAITING_MEDICAL%>")) {
			disable(form.dateForwarded);
		}
		
		if (form.lob.value == "<%=IUMConstants.LOB_INDIVIDUAL_LIFE%>") {
			//disable(form.insuredClientType);
			//disable(form.ownerClientType);
		}
	}

	disable(form.requestStatus);
	disable(form.assignedTo);

	if (form.lob.value == "<%=IUMConstants.LOB_INDIVIDUAL_LIFE%>") {
		showInsuredDetails();	
		hideOwnerDetails();	
	}
	else if ((form.lob.value == "<%=IUMConstants.LOB_GROUP_LIFE%>") || (form.lob.value == "<%=IUMConstants.LOB_PRE_NEED%>")) {
		showInsuredDetails();	
		//hideOwnerDetails();
	}
}


function enableOwnerDetails() {
	var form = document.requestForm;
	if (form.sameClientDataInd.checked) {
		disable(form.ownerClientNo);
		disable(form.ownerLastName);
		disable(form.ownerFirstName);
		disable(form.ownerMiddleName);
		disable(form.ownerTitle);
		disable(form.ownerSuffix);
		disable(form.ownerBirthDate);
		disable(form.ownerSex[0]);
		disable(form.ownerSex[1]);

		document.all["req_cal2"].style.display = "none";
		form.sameClientData.value = "Y";
	}
	else {
		enable(form.ownerClientNo);
		enable(form.ownerLastName);
		enable(form.ownerFirstName);
		enable(form.ownerMiddleName);
		enable(form.ownerTitle);
		enable(form.ownerSuffix);
		enable(form.ownerBirthDate);
		enable(form.ownerSex[0]);
		enable(form.ownerSex[1]);
		
		document.all["req_cal2"].style.display = "block";
		form.sameClientData.value = "N";
	}
}


function enableTabDetails(tab) {
	var form = document.requestForm;
	if (tab == "request") {
		enable(form.remarks);
	}
	else if (tab == "uw") {
		enable(form.UWAnalysis);
		enable(form.UWRemarks);
	}	
	document.all["save_button"].style.display = "block";
	document.all["cancel_button"].style.display = "block";
	document.all["maintain_button"].style.display = "none";
	document.all["create_button"].style.display = "none";
}


function disableTabDetails(tab) {
	var form = document.requestForm;
	if (tab == "request") {
		disable(form.remarks);
	}
	else if (tab == "uw") {
		disable(form.UWAnalysis);
		disable(form.UWRemarks);
		
	}
	document.all["save_button"].style.display = "none";
	document.all["cancel_button"].style.display = "none";
	document.all["maintain_button"].style.display = "block";
	document.all["create_button"].style.display = "block";
}


function showInsuredDetails() {
	document.all["insured1"].style.display = "block";
	document.all["insured2"].style.display = "block";	
	document.all["insured3"].style.display = "block";	
	document.all["insured4"].style.display = "block";	
}

	
function hideInsuredDetails() {
	document.all["insured1"].style.display = "none";
	document.all["insured2"].style.display = "none";	
	document.all["insured3"].style.display = "none";	
	document.all["insured4"].style.display = "none";
}

	
function showOwnerDetails() {
	document.all["owner1"].style.display = "block";
	document.all["owner2"].style.display = "block";	
	document.all["owner3"].style.display = "block";	
	document.all["owner4"].style.display = "block";
}

	
function hideOwnerDetails() {
	document.all["owner1"].style.display = "none";
	document.all["owner2"].style.display = "none";	
	document.all["owner3"].style.display = "none";	
	document.all["owner4"].style.display = "none";	
} 


function switchClient(value) {
	var form = document.requestForm;
	if (form.lob.value == "<%=IUMConstants.LOB_INDIVIDUAL_LIFE%>") {
		if (value.toUpperCase() == "<%=IUMConstants.CLIENT_TYPE_OWNER%>") {
			hideInsuredDetails();	
			showOwnerDetails();

			// set client type with correct chosen value
			form.ownerClientType.selectedIndex = form.insuredClientType.selectedIndex;			
		}
		else if (value.toUpperCase() == "<%=IUMConstants.CLIENT_TYPE_INSURED%>") {
			showInsuredDetails();	
			hideOwnerDetails();

			// set client type with correct chosen value
			form.insuredClientType.selectedIndex = form.ownerClientType.selectedIndex;			
		}
		else if (value.toUpperCase() == "<%=IUMConstants.CLIENT_TYPE_MEMBER%>") {
			alert("<bean:message key="error.field.applicableto" arg0="This client type" arg1="Group Life LOB" />");
		}
		else if (value.toUpperCase() == "<%=IUMConstants.CLIENT_TYPE_PLANHOLDER%>") {
			alert("<bean:message key="error.field.applicableto" arg0="This client type" arg1="Pre-Need LOB" />");
		}				
	}
	else if ((form.lob.value == "<%=IUMConstants.LOB_GROUP_LIFE%>") || (form.lob.value == "<%=IUMConstants.LOB_PRE_NEED%>")) {
		showInsuredDetails();	
		//hideOwnerDetails();

		// set client type with correct chosen value
		form.insuredClientType.selectedIndex = form.ownerClientType.selectedIndex;		
	}
}


function isValidStatus() {
	var form = document.requestForm;
	var currentStatus = form.currentStatus.value;

	if (noSelection(form.requestStatus.value)) {
		alert("<bean:message key="error.field.required" arg0="Request Status"/>");
		
		var size = form.requestStatus.length;
		var found = false;
		for (i=0; ((i<size) && (!found)); i++) {
			value = form.requestStatus.options[i].value;
			if (currentStatus == value) {
				form.requestStatus.selectedIndex = i;
				found = true;
			}
		}
		
		form.requestStatus.focus();
		return false;
	}
	return true;
}


function isValidRequestDetails() {
	var form = document.requestForm;
	var currentStatus = form.currentStatus.value;
	var lob = form.lob.value;
	
	if (noSelection(form.agentCode.value)) {
		alert("<bean:message key="error.field.required" arg0="Agent"/>");
		form.agentCode.focus();
		return false;
	}
	if (noSelection(form.branchCode.value)) {
		alert("<bean:message key="error.field.required" arg0="Branch"/>");
		form.branchCode.focus();
		return false;	
	}
	if (isEmpty(form.amountCovered.value)) {
		alert("<bean:message key="error.field.required" arg0="Amount covered"/>");
		form.amountCovered.select();
		form.amountCovered.focus();
		return false;		
	}
	if (isEmpty(form.premium.value)) {
		alert("<bean:message key="error.field.required" arg0="Premium"/>");
		form.premium.select();
		form.premium.focus();
		return false;		
	}
	if (isEmpty(form.receivedDate.value)) {
		alert("<bean:message key="error.field.required" arg0="Date received"/>");
		form.receivedDate.select();
		form.receivedDate.focus();
		return false;
	}
	if (!isValidDate(form.receivedDate.value)) {
		alert("<bean:message key="error.field.format" arg0="Received date" arg1="date" arg2=""/>");
		form.receivedDate.select();
		form.receivedDate.focus();
		return false;		
	}
	if (isEmpty(form.assignedTo.value)) {
		alert("<bean:message key="error.field.required" arg0="Assigned To"/>");
		//form.assignedTo.select();
		form.assignedTo.focus();
		return false;	
	}
	if (currentStatus == "<%=IUMConstants.STATUS_NB_REVIEW_ACTION%>") {
		if (lob == "<%=IUMConstants.LOB_INDIVIDUAL_LIFE%>") {
			if (isEmpty(form.insuredClientType.value)) {
				alert("<bean:message key="error.field.required" arg0="Client type"/>");
				form.insuredClientType.focus();
				return false;
			}
		}
		if (isEmpty(form.insuredClientNo.value)) {
			alert("<bean:message key="error.field.required" arg0="Client no."/>");
			form.insuredClientNo.select();
			form.insuredClientNo.focus();
			return false;
		}
		if (isEmpty(form.insuredLastName.value)) {
			alert("<bean:message key="error.field.required" arg0="Last name"/>");
			form.insuredLastName.select();
			form.insuredLastName.focus();
			return false;
		}
		if (isEmpty(form.insuredFirstName.value)) {
			alert("<bean:message key="error.field.required" arg0="First name"/>");
			form.insuredFirstName.select();
			form.insuredFirstName.focus();
			return false;
		}
		/*if (isEmpty(form.insuredMiddleName.value)) {
			alert("<bean:message key="error.field.required" arg0="Middle name"/>");
			form.insuredMiddleName.select();
			form.insuredMiddleName.focus();
			return false;
		}*/

		/*if (isEmpty(form.insuredBirthDate.value)) {
			alert("<bean:message key="error.field.required" arg0="Birth date"/>");
			form.insuredBirthDate.select();
			form.insuredBirthDate.focus();
			return false;
		}*/
		/*
		if (noChecked(form.insuredSex)) {
			alert("<bean:message key="error.field.required" arg0="Gender"/>");
			form.insuredSex[0].select();
			form.insuredSex[0].focus();
			return false;
		}
		*/
	}
	return true;
}

function checkFields(){
	var form = document.requestForm;
	var currentStatus = form.currentStatus.value;
	
	if (isEmpty(form.assignedTo.value)) {
		alert("<bean:message key="error.field.required" arg0="Assigned To"/>");
		//form.assignedTo.select();
		form.assignedTo.focus();
		return false;	
	}
	if((currentStatus == "<%=IUMConstants.STATUS_FOR_TRANSMITTAL_USD%>") || (currentStatus == "<%=IUMConstants.STATUS_AWAITING_REQUIREMENTS%>")
			|| (currentStatus == "<%=IUMConstants.STATUS_AWAITING_MEDICAL%>")){
			if(isEmpty(form.dateForwarded.value)){
				alert("<bean:message key="error.field.required" arg0="Date forwarded"/>");
				form.dateForwarded.select();
				form.dateForwarded.focus();
				return false;
			}
			if (isGreaterDate(form.receivedDate.value,form.dateForwarded.value) ){
				alert('<bean:message key="error.field.afterenddate" arg0="Date forwarded" arg1="Date received"/>');
				form.dateForwarded.focus();
				form.dateForwarded.select();
				return false;
			}		
	}
	saveRequest();
	
}
function saveRequest(){
	var form = document.requestForm;
	var currentStatus = form.currentStatus.value;	
	if (isValidStatus()) {
		form.isMaintain.value = false;
		
			form.isSuccessChangeStatus.value="true";
			gotoPage("requestForm", "saveRequest.do");		
		
	}
}

function saveUWAssessment(session_id){
	var form = document.requestForm;
	
	if (isEmpty(form.UWAnalysis.value) && isEmpty(form.UWRemarks.value) && isEmpty(form.UWFinalDecision.value)){
		alert("<bean:message key="error.field.required" arg0="Analysis, Final Decision, or Remarks"/>");
		return false;
	}else{
	
		//try{
		//	showLayer();
		//	window.question.focus();
		//	setLayerPosition2();
		//}catch(e){
		//}
		gotoPage("requestForm", "saveUWAssessment.do?session_id=" +session_id);
	}

}

function validateCurrency(object, fieldName) {
	var value = object.value;
	value = replace(value, ",", "");

	if (fieldName == "Premium") {
		if (parseFloat(value) > 9999999999.99) {
			alert("<bean:message key="error.field.lessthan" arg0="Premium" arg1="100,000,000.00"/>");
			object.select();
			object.focus();
		}
		else if (!isCurrency(value)) {
			alert("<bean:message key="error.field.format" arg0="Premium" arg1="currency" arg2=""/>");
			object.select();
			object.focus();
		}
		else {
			object.value = formatCurrency(value);
		}
	}
	else if (fieldName == "Amount Covered") {
		if (parseFloat(value) > 9999999999.99) {
			alert("<bean:message key="error.field.lessthan" arg0="Amount Covered" arg1="100,000,000.00"/>");
			object.select();
			object.focus();
		}
		else if (!isCurrency(value)) {
			alert("<bean:message key="error.field.format" arg0="Amount Covered" arg1="currency" arg2=""/>");
			object.select();
			object.focus();
		}
		else {
			object.value = formatCurrency(value);
		}
	}
}


function haveSelectedStatus(){
	var form = document.requestForm;
	var status = form.requestStatus.value;
	if (status == null || status ==""){
		alert("<bean:message key="error.field.requiredselection" arg0="status"/>");
		form.requestStatus.focus();
		return false;
	} else {
		return true;
	}
}


function setDefaultFocus() {
	var form = document.requestForm;
	form.docCode.focus();
}


function retrieveCDS(){
	var form = document.requestForm;
	form.action = "retrieveCDS.do";
	form.submit();
}

function autoSettle(){
	var form = document.requestForm;
	form.action = "autoSettle.do";
	form.submit();
}

function setDateForwarded() {
	var form = document.requestForm;
	var currentStatus = form.currentStatus.value;

	if (currentStatus == "<%=IUMConstants.STATUS_FOR_TRANSMITTAL_USD%>") {
		form.dateForwarded.value = "<%=DateHelper.getCurrentDate()%>";
	}
	/*
	else if ((currentStatus == "<%=IUMConstants.STATUS_AWAITING_REQUIREMENTS%>") ||
			 (currentStatus == "<%=IUMConstants.STATUS_AWAITING_MEDICAL%>")) {
		form.dateForwarded.value = form.orig_dateForwarded.value;
	}
	*/	
}


function cancelRequest(tab) {
	var form = document.requestForm;
	form.isMaintain.value = false;

	disableRequestDetails(); 
	disableTabDetails(tab);
		
	var page = "viewRequestDetail.do";
	if (form.reqPage.value == "requestDetail") {
		page = "viewRequestDetail.do";
	}                          
	else if (form.reqPage.value == "cdsDetail") {
		page = "viewCDSDetail.do";
	}
	else if (form.reqPage.value == "koReqMedLabDetail") {
		page = "viewKOReqMedLabDetail.do";
	}
	else if (form.reqPage.value == "uwAssessmentDetail") {
		page = "viewUWAssessmentDetail.do";
	}
	else if (form.reqPage.value == "doctorsNotesDetail") {
		page = "viewDoctorsNotes.do";
	}
	gotoPage("requestForm", page);
}


function maintainRequest(tab) {
	var form = document.requestForm;
	form.isMaintain.value = true;

	enableRequestDetails(); 
	enableTabDetails(tab); 
	setDateForwarded();
}


function setAssignTo() {
	var form = document.requestForm;
	form.isMaintain.value = true;
		
	var page = "viewRequestDetail.do";
	if (form.reqPage.value == "requestDetail") {
		page = "viewRequestDetail.do";
	}                          
	else if (form.reqPage.value == "cdsDetail") {
		page = "viewCDSDetail.do";
	}
	else if (form.reqPage.value == "koReqMedLabDetail") {
		page = "viewKOReqMedLabDetail.do";
	}
	else if (form.reqPage.value == "uwAssessmentDetail") {
		page = "viewUWAssessmentDetail.do";
	}
	else if (form.reqPage.value == "doctorsNotesDetail") {
		page = "viewDoctorsNotes.do";
	}
	gotoPage("requestForm", page);
}

function forwardToRecords(){
	var form = document.requestForm;
	form.action = "singleForwardToRecords.do";
	form.submit();
}

function searchClientData(client){
	var form = document.requestForm;
	form.searchClientType.value = client;
	
	if (isEmpty(form.insuredClientNo.value)) {
		alert("<bean:message key="error.field.required" arg0="Client no."/>");
		form.insuredClientNo.select();
		form.insuredClientNo.focus();
		return false;
	}
	
	var page = "viewRequestDetail.do";
	if (form.reqPage.value == "requestDetail") {
		page = "viewRequestDetail.do";
	}                          
	else if (form.reqPage.value == "cdsDetail") {
		page = "viewCDSDetail.do";
	}
	else if (form.reqPage.value == "koReqMedLabDetail") {
		page = "viewKOReqMedLabDetail.do";
	}
	else if (form.reqPage.value == "uwAssessmentDetail") {
		page = "viewUWAssessmentDetail.do";
	}
	else if (form.reqPage.value == "doctorsNotesDetail") {
		page = "viewDoctorsNotes.do";
	}
	gotoPage("requestForm", page);
}

function initializeClientData(value){
	// reuse switchClient() code 
	var form = document.requestForm;
	
	if(value=="<%=IUMConstants.CLIENT_TYPE_INSURED%>"){
		if(isEmpty(form.insuredLastName.value)){
			alert("There is no existing client with this number - " + form.insuredClientNo.value);
		}
				
	} else if(value=="<%=IUMConstants.CLIENT_TYPE_OWNER%>"){
		if(isEmpty(form.ownerLastName.value)){
			alert("There is no existing client with this number - " + form.ownerClientNo.value);
		}
	}
	
	if (form.lob.value == "<%=IUMConstants.LOB_INDIVIDUAL_LIFE%>") {
		if (value.toUpperCase() == "<%=IUMConstants.CLIENT_TYPE_OWNER%>") {
			hideInsuredDetails();	
			showOwnerDetails();
		}
		else if (value.toUpperCase() == "<%=IUMConstants.CLIENT_TYPE_INSURED%>") {
			showInsuredDetails();	
			hideOwnerDetails();
		}
		else if (value.toUpperCase() == "<%=IUMConstants.CLIENT_TYPE_MEMBER%>") {
			alert("<bean:message key="error.field.applicableto" arg0="This client type" arg1="Group Life LOB" />");
		}
		else if (value.toUpperCase() == "<%=IUMConstants.CLIENT_TYPE_PLANHOLDER%>") {
			alert("<bean:message key="error.field.applicableto" arg0="This client type" arg1="Pre-Need LOB" />");
		}				
	}
	else if ((form.lob.value == "<%=IUMConstants.LOB_GROUP_LIFE%>") || (form.lob.value == "<%=IUMConstants.LOB_PRE_NEED%>")) {
		showInsuredDetails();	
		//hideOwnerDetails();
	}
}
//-->