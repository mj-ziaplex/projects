<%@ page language="java" import="java.util.*" %>
<%
/* Andre Ceasar Dacanay - css enhancement - start */
String iumCss = "";
String companyCode = "";
try {
	ResourceBundle rbWMSSP = ResourceBundle.getBundle("dbconfig");
	iumCss = rbWMSSP.getString("IUM_CSS_VERSION");
	companyCode = iumCss;
	iumCss = "_"+iumCss;
} catch (Exception e){}
if(iumCss.equals("_CP")){
	companyCode="";
}else if(iumCss.equals("_GF")){
	companyCode = companyCode+" ";
}

/* Andre Ceasar Dacanay - css enhancement - end */
%>
<html>
  <head>
    <title><%=companyCode%>Integrated Underwriting and Medical System</title>
    <script language="JavaScript" type="text/javascript">
	<!--
	function closeW()
	{
		window.opener = self;
		window.close();
	}
	// -->
	</script> 
  </head>
  
  <body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="closeW();">
  </body>
  
</html>