<%@ taglib uri="/struts-html.tld" prefix="html" %>
<%@ taglib uri="/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/ium.tld" prefix="ium" %>
<%@ page language="java" import="java.util.*,com.slocpi.ium.data.*, com.slocpi.ium.ui.form.*, com.slocpi.ium.util.*, com.slocpi.ium.ui.util.*, java.text.* " buffer="12kb"%>
<jsp:useBean id="medicalRecordsForm" scope="request" class="com.slocpi.ium.ui.form.MedicalRecordsForm"/>
<jsp:useBean id="displayPage" scope="request" class="com.slocpi.ium.ui.util.Page"/>
<html:html locale="true">
<%String contextPath = request.getContextPath();
StateHandler sessionHandler = new StateHandler();
Access access = new Access();
UserData userData = sessionHandler.getUserData(request); 
UserProfileData userPref = userData.getProfile();
%>
<%

/* Andre Ceasar Dacanay - css enhancement - start */
String iumCss = "";
String iumBorderColor = "";
String iumColorW = "";
String iumColorB = "";
String iumColorO = "";
String companyCode = "";
try {
	ResourceBundle rbWMSSP = ResourceBundle.getBundle("dbconfig");
	iumCss = rbWMSSP.getString("IUM_CSS_VERSION");
	companyCode = iumCss;
	iumCss = "_"+iumCss;
} catch (Exception e){}

if(iumCss.equals("_CP")){
	iumBorderColor = "bordercolor=\"#2a4c7c\"";
	iumColorB = "#2a4c7c"; 
	iumColorO = "#FFCB00";
	companyCode="";
}else if(iumCss.equals("_GF")){
	iumBorderColor = "bordercolor=\"#303A77\"";
	iumColorW = "style=\"color:white;\"";
	iumColorB = "#303A77";
	iumColorO = "#303A77";
	companyCode = companyCode+" ";
}else{
	iumBorderColor = "bordercolor=\"#2a4c7c\";";
	iumColorB = "#2a4c7c"; 
	iumColorO = "#FFCB00";
}
/* Andre Ceasar Dacanay - css enhancement - end */
%>
<%
    // This routine queries the access rights of the users (RCRIVERA)
    //
    // --- START --- 
    
	HashMap userAccess = sessionHandler.getUserAccess(request);
    if(!access.hasAccess(userAccess,IUMConstants.PAGE_MEDRECORD_LIST,IUMConstants.ACC_INQUIRE) ){                                                                                                                    
    	response.sendRedirect("noAccessPage.jsp");   
    } 
    
    //String EXECUTE_ACCESS = "";      
    String CREATE_ACCESS = "";
    String MAINTAIN_ACCESS = "";  
    
    if(!access.hasAccess(userAccess,IUMConstants.PAGE_MEDRECORD_LIST,IUMConstants.ACC_CREATE) ){                                                                                                                     
	  CREATE_ACCESS = "DISABLED"; 	
    }

    //if(!access.hasAccess(userAccess,IUMConstants.PAGE_MEDRECORD_LIST,IUMConstants.ACC_EXECUTE) ){                                                                                                                     
	// EXECUTE_ACCESS = "DISABLED"; 	
    //}
    
    if(!access.hasAccess(userAccess,IUMConstants.PAGE_MEDRECORD_LIST,IUMConstants.ACC_MAINTAIN) ){                                                                                                                     
	 MAINTAIN_ACCESS = "DISABLED"; 	
    }
    
    
    // --- END ---
 %>    
<html>
  <head>
    <title><%=companyCode%>Integrated Underwriting and Medical System</title>
    <link href="<%=contextPath%>/css/main<%=iumCss%>.css" rel="stylesheet" type="text/css">
    <script language="Javascript" src="<%=contextPath%>/js/navigation.js"></script>
    <script language="Javascript" src="<%=contextPath%>/js/validation.js"></script>    
    <script language="Javascript" src="<%=contextPath%>/jsp/validate_medicalRecords.jsp"></script>    
  </head>
  
<logic:notEmpty name="medicalRecordsForm" property="medicalRecordData">  
  <body leftmargin="0" topmargin="0" onKeyUp="checkEnter(event,'search');" onLoad="disableMedicalStatusFields(); checkMedicalStatus(); putCreateBtn(<%=userPref.getRecordsPerView()%>, <%=((ArrayList)medicalRecordsForm.getMedicalRecordData()).size()%>);" onUnload="closePopUpWin();">
</logic:notEmpty>
<logic:empty name="medicalRecordsForm" property="medicalRecordData">  
  <body leftmargin="0" topmargin="0" onKeyUp="checkEnter(event,'search');" onLoad="disableChangeStatusFields(); checkMedicalStatus(); putCreateBtn(<%=userPref.getRecordsPerView()%>, <%=((ArrayList)medicalRecordsForm.getMedicalRecordData()).size()%>);" onUnload="closePopUpWin();">
</logic:empty>

    <form name="frm" method="post">
      <input type="hidden" name="sortOrder" value='<bean:write name="medicalRecordsForm" property="sortOrder"/>'>    
      <input type="hidden" name="sortBy"    value='<bean:write name="medicalRecordsForm" property="sortBy"/>'>          
      <font face="verdana">
        <table border="0" cellpadding="0" cellspacing="0" height="100%" width="1200">
          <tr>
            <td width="100%" colspan="2">    
              <jsp:include page="header.jsp" flush="true"/>
            </td>
          </tr>
          <tr>
            <td rowspan="2" bgcolor="<%=iumColorO%>" height="100%" width="20"><img src="<%=contextPath%>/images/spacer.gif" width="20"></td>
            <td background="<%=contextPath%>/images/GradientGray.jpg" height="27" width="100%" valign="top">&nbsp;&nbsp;
              <span class="main">Integrated Underwriting and Medical System</span>
            </td>
          </tr>
          <tr>
            <td width="1200" height="100%" valign="top">
              <!-- BODY -->
              <table cellpadding="0" cellspacing="0" border="0" bordercolor="<%=iumColorB%>" width="1200">
                <tr>
                  <td>
                    <table cellpadding="10" cellspacing="0" border="0" width="1200">
                      <tr>
                        <td>
                          <table border="0" cellpadding="0" cellspacing="0" width="1200">
                            <tr><td><img src="<%=contextPath%>/images/medicalrecords_title.jpg" border="0"></td></tr>
                            <tr><td>&nbsp;</td></tr>                            
                            <tr>
                              <td>
                                <table border="0" bordercolor="<%=iumColorB%>" cellpadding="0" cellspacing="0" bgcolor="" width="1200">
                                  <tr><td></td></tr>
									<tr>
									<td>                                    
									<table>
									<tr>
									<td colspan="2" class="error">
									<html:errors/>
									</td>
									</tr>
									</table>
									</td>
									</tr>                                                                                                        
                                  <tr>
                                    <td>
                                      <table border="1" bordercolor="<%=iumColorB%>" cellpadding="10" cellspacing="0" bgcolor="" width="800" class="listtable1">
                                        <tr>
                                          <td>
                                            <table border="0" cellpadding="2" cellspacing="0">
                                              <tr>
                                                <td class="label2"><b>Reference No.</b></td>
                                                <td class="label2"><input type="textbox" class="label2" name="referenceNo" maxlength="15" value='<bean:write name="medicalRecordsForm" property="referenceNo"/>'></td>
                                                <td class="label2">&nbsp;</td>
                                                <td class="label2" width="20"><b>Client No.</b></td>
                                                <td class="label2"><input type="textbox" class="label2" name="clientNo" maxlength="10" value='<bean:write name="medicalRecordsForm" property="clientNo"/>'></td>
                                                <td class="label2">&nbsp;</td>
                                                <td class="label2"><b>7-day Memo Date</b></td>
                                                <td class="label2"><input type="textbox" class="label2" name="sevenDayMemoDate" value='<bean:write name="medicalRecordsForm" property="sevenDayMemoDate"/>' onKeyUp="getKeyDate(event,this);" maxlength="9" >&nbsp;
                                                <a href="#" onClick="popUpWin('<%=contextPath%>/jsp/calendar.jsp?textbox=frm.sevenDayMemoDate',290,155);"><img src="<%=contextPath%>/images/calendar.gif" height="17" width="19" border="0">
										        </a>
												</td>
                                              </tr>
                                              <tr>
                                                <td class="label2"><b>Last Name</b></td>
                                                <td class="label2"><input type="textbox" class="label2" name="lastName" maxlength="40" value='<bean:write name="medicalRecordsForm" property="lastName"/>'></td>
                                                <td class="label2">&nbsp;</td>
                                                <td class="label2"><b>First Name</b></td>
                                                <td class="label2"><input type="textbox" class="label2" name="firstName" maxlength="25" value='<bean:write name="medicalRecordsForm" property="firstName"/>'></td>
                                                <td class="label2">&nbsp;</td>
                                                <td class="label2"><b>Date Requested</b></td>
                                                <td class="label2"><input type="textbox" class="label2" name="dateRequested" value='<bean:write name="medicalRecordsForm" property="dateRequested"/>' onKeyUp="getKeyDate(event,this);" maxlength="9" >&nbsp;
                                                 <a href="#" onClick="popUpWin('<%=contextPath%>/jsp/calendar.jsp?textbox=frm.dateRequested',290,155);"><img src="<%=contextPath%>/images/calendar.gif" height="17" width="19" border="0">
										        </a>
                                                </td>
                                              </tr>
                                              <tr>
                                                <td class="label2"><b>Status</b></td>
                                                <td class="label2"><font size="1">
                                                 <ium:list className="label2" listBoxName="status" type="<%=IUMConstants.LIST_FILTER_MED_RECORD_STATUS%>" styleName="width:200" selectedItem="<%=medicalRecordsForm.getStatus()%>" onChange="putValueChStatus();"/>                                                
                                                 <input type="hidden" name="chStatus" value='<%=((String)request.getAttribute("chStatus"))%>'>
                                                </font>
                                                </td>
                                                <td class="label2">&nbsp;</td>
                                                <td class="label2"><b>Follow Up No.</b></td>
                                                <td class="label2"><input type="textbox" class="label2" maxLength="2" name="followUpNo" onKeyUp="getKeyNum(event, this);" value='<bean:write name="medicalRecordsForm" property="followUpNo"/>'></td>
                                                <td class="label2">&nbsp;</td>
                                                <td class="label2"><b>Follow Up Start Date</b></td>
                                                <td class="label2"><input type="textbox" class="label2" name="followUpStartDate" value='<bean:write name="medicalRecordsForm" property="followUpStartDate"/>' onKeyUp="getKeyDate(event,this);" maxlength="9" >&nbsp;
                                                <a href="#" onClick="popUpWin('<%=contextPath%>/jsp/calendar.jsp?textbox=frm.followUpStartDate',290,155);"><img src="<%=contextPath%>/images/calendar.gif" height="17" width="19" border="0">
										        </a>
                                                </td>
                                              </tr>
                                              <tr> 
                                                <td class="label2"><b>Examiner</b></td>                                                
                                                <td class="label2">
                                                <ium:list className="label2" listBoxName="examiner" type="<%=IUMConstants.LIST_EXAMINERS%>" styleName="width:200" selectedItem="<%=medicalRecordsForm.getExaminer()%>"/>                                                
                                                </td>
                                                <td class="label2">&nbsp;</td>
                                                <td class="label2"><b>Laboratory</b></td>
                                                <td class="label2">
                                                 <ium:list className="label2" listBoxName="lab" type="<%=IUMConstants.LIST_LABORATORIES%>" styleName="width:200" selectedItem="<%=medicalRecordsForm.getLab()%>"/>                                                
                                                </td>
                                                <td class="label2">&nbsp;</td>
                                                <td class="label2"><b>Follow Up End Date</b></td>
                                                <td class="label2"><input type="textbox" class="label2" name="followUpEndDate" value='<bean:write name="medicalRecordsForm" property="followUpEndDate"/>' onKeyUp="getKeyDate(event,this);" maxlength="9" >&nbsp;
                                                <a href="#" onClick="popUpWin('<%=contextPath%>/jsp/calendar.jsp?textbox=frm.followUpEndDate',290,155);"><img src="<%=contextPath%>/images/calendar.gif" height="17" width="19" border="0">
										        </a>
                                                </td>
                                              </tr>
                                              <tr>
                                                <td class="label2"><b>Unassigned</b></td>
                                                <td colspan="8"><input type="checkbox" name="unassigned" class="label2"></td>
                                              </tr>
                                              <tr>
                                                <td colspan="9" align="left">
                                                <input type="button" class="button1" name="search" value="Search" onClick="searchMedicalRecord('<%=contextPath%>');">
                                                <input type="button" class="button1" value="Reset"  onClick="resetMedicalFields();">
                                                </td>
                                              </tr>
                                            </table>
                                          </td>
                                        </tr>
                                      </table>
                                    </td>
                                  </tr>
                                  <tr><td></td></tr>
                                  <tr><td></td></tr>
                                  <tr><td>&nbsp;</td></tr>
                                  <tr><table><tr><td><div align="left"><input class="button1" type="button" value="Create" onclick="gotoPage('frm','createMedicalExam.do');" <%=CREATE_ACCESS%> ></div></td></tr></table></tr>
                                  
                                  <tr>
                                    <td colspan="2" height="100%">
                                      <table border="1" bordercolor="<%=iumColorB%>" cellpadding="10" cellspacing="0" width="1200" class="listtable1">
                                        <tr>
                                          <td colspan="2" height="100%">
                                            <table border="0" cellpadding="2" cellspacing="1" bgcolor="#FFFFFF" style="border: 1px solid #D1D1D1;" class="listtable1">
                                              <tr class="headerrow1">
                                                <td width="10">
                                                <input type="checkbox" name="selAll" class="label2" onClick="selectAll();enableClientNoAll();">
                                                </td>
                                                <td width="80">
                                              <logic:notEmpty name="displayPage" property="list">
                                                 <% if (medicalRecordsForm.getSortOrder().equals("") && medicalRecordsForm.getSortBy().equals("0")) {%>
                                                 <a <%=iumColorW%> href="javascript:resort('<%=contextPath%>','1','d');">DATE CONDUCTED 
                                                 <img src="<%=contextPath%>/images/uarrow.gif" border="0"><a>                                                                                   
                                                 <% } else { %>
                                                  	<% if (medicalRecordsForm.getSortOrder().equals("") && medicalRecordsForm.getSortBy().equals("1")) {%>
                                                		<a <%=iumColorW%> href="javascript:resort('<%=contextPath%>','1','d');">DATE CONDUCTED 
                                                 		<img src="<%=contextPath%>/images/uarrow.gif" border="0"><a>                                                                                   
                                                 	<% } else { %>
                                                 		<a <%=iumColorW%> href="javascript:resort('<%=contextPath%>','1','a');">DATE CONDUCTED 
                                                 		<img src="<%=contextPath%>/images/darrow.gif" border="0"><a>                                                                                   
                                                	<% } %>                                                
                                                 <% } %>                                                 
                                               </logic:notEmpty>
                                               <logic:empty name="displayPage" property="list">DATE CONDUCTED</logic:empty>
                                                 </td>
                                                <td width="80">
                                              <logic:notEmpty name="displayPage" property="list">                                                
                                                <% if (medicalRecordsForm.getSortOrder().equals("") && medicalRecordsForm.getSortBy().equals("2")) {%>
                                                 <a <%=iumColorW%> href="javascript:resort('<%=contextPath%>','2','d');">MEDICAL RECORD ID 
                                                 <img src="<%=contextPath%>/images/uarrow.gif" border="0"><a>                                  
                                                <% } else { %>
                                                 <a <%=iumColorW%> href="javascript:resort('<%=contextPath%>','2','a');">MEDICAL RECORD ID 
                                                 <img src="<%=contextPath%>/images/darrow.gif" border="0"><a>                                                                                  
                                                <% } %>
                                               </logic:notEmpty>
                                               <logic:empty name="displayPage" property="list">MEDICAL RECORD ID</logic:empty>                                               
                                                 </td>                                                 
                                                <td width="130">
                                              <logic:notEmpty name="displayPage" property="list">                                                
                                                <% if (medicalRecordsForm.getSortOrder().equals("") && medicalRecordsForm.getSortBy().equals("3")) {%>                                                
                                                <a <%=iumColorW%> href="javascript:resort('<%=contextPath%>','3','d');">REFERENCE NO.
                                                 <img src="<%=contextPath%>/images/uarrow.gif" border="0"><a>                                  
                                                <% } else { %>
                                                 <a <%=iumColorW%> href="javascript:resort('<%=contextPath%>','3','a');">REFERENCE NO. 
                                                 <img src="<%=contextPath%>/images/darrow.gif" border="0"><a>                                                                                  
                                                <% } %>                                          
                                               </logic:notEmpty>       
                                               <logic:empty name="displayPage" property="list">REFERENCE NO.</logic:empty>                                               
                                                </td>
                                                <td width="100">
                                              <logic:notEmpty name="displayPage" property="list">                                                
                                                <% if (medicalRecordsForm.getSortOrder().equals("") && medicalRecordsForm.getSortBy().equals("4")) {%>                                                                                                
                                                 <a <%=iumColorW%> href="javascript:resort('<%=contextPath%>','4','d');">CLIENT NO.
                                                 <img src="<%=contextPath%>/images/uarrow.gif" border="0"><a>                                                                                  
                                                <% } else { %>
                                                 <a <%=iumColorW%> href="javascript:resort('<%=contextPath%>','4','a');">CLIENT NO. 
                                                 <img src="<%=contextPath%>/images/darrow.gif" border="0"><a>                                                                                  
                                                <% } %>
                                              </logic:notEmpty>                                                      
                                               <logic:empty name="displayPage" property="list">CLIENT NO.</logic:empty>                                                                                                
                                                </td>
                                                <td width="80">
                                              <logic:notEmpty name="displayPage" property="list">
                                                <% if (medicalRecordsForm.getSortOrder().equals("") && medicalRecordsForm.getSortBy().equals("5")) {%>                                                                                                                                                
                                                <a <%=iumColorW%> href="javascript:resort('<%=contextPath%>','5','d');">LAST NAME
                                                 <img src="<%=contextPath%>/images/uarrow.gif" border="0"><a>                                                                                  
                                                <% } else { %>
                                                 <a <%=iumColorW%> href="javascript:resort('<%=contextPath%>','5','a');">LAST NAME 
                                                 <img src="<%=contextPath%>/images/darrow.gif" border="0"><a>                                                                                  
                                                <% } %>                                                                                                                                                  
                                                </logic:notEmpty>
                                               <logic:empty name="displayPage" property="list">LAST NAME</logic:empty>                                                
                                                </td>
                                                <td width="140">
                                              <logic:notEmpty name="displayPage" property="list">                                                
                                                <% if (medicalRecordsForm.getSortOrder().equals("") && medicalRecordsForm.getSortBy().equals("6")) {%>                                                                                                                                                                                                
                                                <a <%=iumColorW%> href="javascript:resort('<%=contextPath%>','6','d');">FIRST NAME
                                                 <img src="<%=contextPath%>/images/uarrow.gif" border="0"><a>                                                                                  
                                                <% } else { %>
                                                 <a <%=iumColorW%> href="javascript:resort('<%=contextPath%>','6','a');">FIRST NAME 
                                                 <img src="<%=contextPath%>/images/darrow.gif" border="0"><a>                                                                                  
                                                <% } %>                                          
                                                </logic:notEmpty>                                                 
                                               <logic:empty name="displayPage" property="list">FIRST NAME</logic:empty>                                                                                                                                                                                                       
                                                </td>
                                                <td width="140">
                                              <logic:notEmpty name="displayPage" property="list">                                                
                                                <% if (medicalRecordsForm.getSortOrder().equals("") && medicalRecordsForm.getSortBy().equals("7")) {%>                                                                                                                                                                                                                                                
                                                <a <%=iumColorW%> href="javascript:resort('<%=contextPath%>','7','d');">EXAMINER/LAB NAME
                                                 <img src="<%=contextPath%>/images/uarrow.gif" border="0"><a>                                                                                  
                                                <% } else { %>
                                                 <a <%=iumColorW%> href="javascript:resort('<%=contextPath%>','7','a');">EXAMINER/LAB NAME
                                                 <img src="<%=contextPath%>/images/darrow.gif" border="0"><a>                                                                                  
                                                <% } %>                                          
                                               </logic:notEmpty>                                                 
                                               <logic:empty name="displayPage" property="list">EXAMINER/LAB NAME</logic:empty>                                                                                                                                                                                                                                                      
                                                </td>
                                                <td width="20">
                                              <logic:notEmpty name="displayPage" property="list">                                                
                                                <% if (medicalRecordsForm.getSortOrder().equals("") && medicalRecordsForm.getSortBy().equals("8")) {%>                                                                                                                                                                                                                                                
                                                <a <%=iumColorW%> href="javascript:resort('<%=contextPath%>','8','d');">TYPE
                                                 <img src="<%=contextPath%>/images/uarrow.gif" border="0"><a>                                                                                  
                                                <% } else { %>
                                                 <a <%=iumColorW%> href="javascript:resort('<%=contextPath%>','8','a');">TYPE 
                                                 <img src="<%=contextPath%>/images/darrow.gif" border="0"><a>                                                                                  
                                                <% } %>                                          
                                                </logic:notEmpty>                                                        
                                               <logic:empty name="displayPage" property="list">TYPE</logic:empty>                                                                                                                                                                                                                                                
                                                </td>
                                                <td width="140">
                                              <logic:notEmpty name="displayPage" property="list">                                                
                                                <% if (medicalRecordsForm.getSortOrder().equals("") && medicalRecordsForm.getSortBy().equals("9")) {%>                                                                                                                                                                                                                                                
                                                <a <%=iumColorW%> href="javascript:resort('<%=contextPath%>','9','d');">OFFICE/BRANCH
                                                 <img src="<%=contextPath%>/images/uarrow.gif" border="0"><a>                                                                                  
                                                <% } else { %>
                                                 <a <%=iumColorW%> href="javascript:resort('<%=contextPath%>','9','a');">OFFICE/BRANCH 
                                                 <img src="<%=contextPath%>/images/darrow.gif" border="0"><a>                                                                                  
                                                <% } %>                                          
                                                </logic:notEmpty>                                           
                                               <logic:empty name="displayPage" property="list">OFFICE/BRANCH</logic:empty>                                                                                                                                                                                                                                                             
                                                </td>
                                                <td width="100">
                                              <logic:notEmpty name="displayPage" property="list">                                                
                                                <% if (medicalRecordsForm.getSortOrder().equals("") && medicalRecordsForm.getSortBy().equals("10")) {%>                                                                                                                                                                                                                                                
                                                <a <%=iumColorW%> href="javascript:resort('<%=contextPath%>','10','d');">TEST
                                                 <img src="<%=contextPath%>/images/uarrow.gif" border="0"><a>                                                                                  
                                                <% } else { %>
                                                 <a <%=iumColorW%> href="javascript:resort('<%=contextPath%>','10','a');">TEST 
                                                 <img src="<%=contextPath%>/images/darrow.gif" border="0"><a>                                                                                  
                                                <% } %>                                          
                                                </logic:notEmpty>                                                                                                                                                                                                                                                                                                        
                                               <logic:empty name="displayPage" property="list">TEST</logic:empty>                                                
                                                </td>
                                                <td width="100">
                                              <logic:notEmpty name="displayPage" property="list">                                                
                                                <% if (medicalRecordsForm.getSortOrder().equals("") && medicalRecordsForm.getSortBy().equals("11")) {%>                                                                                                                                                                                                                                                
                                                <a <%=iumColorW%> href="javascript:resort('<%=contextPath%>','11','d');">STATUS
                                                 <img src="<%=contextPath%>/images/uarrow.gif" border="0"><a>                                                                                  
                                                <% } else { %>
                                                 <a <%=iumColorW%> href="javascript:resort('<%=contextPath%>','11','a');">STATUS 
                                                 <img src="<%=contextPath%>/images/darrow.gif" border="0"><a>                                                                                  
                                                <% } %>                                          
                                                </logic:notEmpty>                                           
                                               <logic:empty name="displayPage" property="list">STATUS</logic:empty>                                                                                                                                                                                                                                                             
                                                </td>
                                                <td width="80">
                                              <logic:notEmpty name="displayPage" property="list">                                                
                                                <% if (medicalRecordsForm.getSortOrder().equals("") && medicalRecordsForm.getSortBy().equals("12")) {%>                                                                                                                                                                                                                                                
                                                <a <%=iumColorW%> href="javascript:resort('<%=contextPath%>','12','d');">DATE RECEIVED
                                                 <img src="<%=contextPath%>/images/uarrow.gif" border="0"><a>                                                                                  
                                                <% } else { %>
                                                 <a <%=iumColorW%> href="javascript:resort('<%=contextPath%>','12','a');">DATE RECEIVED 
                                                 <img src="<%=contextPath%>/images/darrow.gif" border="0"><a>                                                                                  
                                                <% } %>                                          
                                                </logic:notEmpty>                                             
                                               <logic:empty name="displayPage" property="list">DATE RECEIVED</logic:empty>                                                                                                                                                                                                                                                           
                                                </td>
                                              </tr>
                                              <logic:empty name="displayPage" property="list">
                                              <tr class="row1">                                              
                                              <td class="label2" colspan="13" align="center">
                                              <bean:message key="message.noexisting" arg0="medical records"/>
                                              </td>
                                              </tr>
                                              </logic:empty>
                                           <% if (displayPage != null) { %>                                              
                                              <logic:notEmpty name="displayPage" property="list">
                                              <%int i=0;%>
 											  <%--<logic:iterate id="med" name="medicalRecordsForm" property="medicalRecordData">--%>
                                              <logic:iterate id="med" name="displayPage" property="list"> 											  
 											  <% if (i%2 == 0) { %>
                                              <tr class="row1">
                                              <% } else { %>
                                              <tr class="row2">                                              
                                              <% } %>
                                                  <td><input type="checkbox" name="index" class="label2" onClick="enableClientNo('<%=i%>');"></td>
                                                  <td style="cursor:hand" onclick="viewMedicalRecord('<%=contextPath%>/viewMedicalDetail.do?refNo=<bean:write name="med" property="medicalRecordId"/>');"><bean:write name="med" property="dateConducted"/>&nbsp;</td>
                                                  <td style="cursor:hand" onclick="viewMedicalRecord('<%=contextPath%>/viewMedicalDetail.do?refNo=<bean:write name="med" property="medicalRecordId"/>');"><bean:write name="med" property="medicalRecordId"/>&nbsp;</td>                                                  
                                                  <td style="cursor:hand" onclick="viewMedicalRecord('<%=contextPath%>/viewMedicalDetail.do?refNo=<bean:write name="med" property="medicalRecordId"/>');"><bean:write name="med" property="refNo"/>&nbsp;</td>
                                                  <td style="cursor:hand" onclick="viewMedicalRecord('<%=contextPath%>/viewMedicalDetail.do?refNo=<bean:write name="med" property="medicalRecordId"/>');"><bean:write name="med" property="clientNo"/>&nbsp;</td>
                                                  <td style="cursor:hand" onclick="viewMedicalRecord('<%=contextPath%>/viewMedicalDetail.do?refNo=<bean:write name="med" property="medicalRecordId"/>');"><bean:write name="med" property="lastName"/>&nbsp;</td>
                                                  <td style="cursor:hand" onclick="viewMedicalRecord('<%=contextPath%>/viewMedicalDetail.do?refNo=<bean:write name="med" property="medicalRecordId"/>');"><bean:write name="med" property="firstName"/>&nbsp;</td>
                                                  <td style="cursor:hand" onclick="viewMedicalRecord('<%=contextPath%>/viewMedicalDetail.do?refNo=<bean:write name="med" property="medicalRecordId"/>');"><bean:write name="med" property="labName"/>&nbsp;</td>
                                                  <td style="cursor:hand" onclick="viewMedicalRecord('<%=contextPath%>/viewMedicalDetail.do?refNo=<bean:write name="med" property="medicalRecordId"/>');"><div align="center"><bean:write name="med" property="labTestInd"/>&nbsp;</div></td>
                                                  <td style="cursor:hand" onclick="viewMedicalRecord('<%=contextPath%>/viewMedicalDetail.do?refNo=<bean:write name="med" property="medicalRecordId"/>');"><bean:write name="med" property="branch"/>&nbsp;</td>
                                                  <td style="cursor:hand" onclick="viewMedicalRecord('<%=contextPath%>/viewMedicalDetail.do?refNo=<bean:write name="med" property="medicalRecordId"/>');"><bean:write name="med" property="test"/>&nbsp;</td>
                                                  <td style="cursor:hand" onclick="viewMedicalRecord('<%=contextPath%>/viewMedicalDetail.do?refNo=<bean:write name="med" property="medicalRecordId"/>');"><bean:write name="med" property="status"/>&nbsp;</td>
                                                  <td style="cursor:hand" onclick="viewMedicalRecord('<%=contextPath%>/viewMedicalDetail.do?refNo=<bean:write name="med" property="medicalRecordId"/>');"><bean:write name="med" property="dateReceived"/>&nbsp;</td>
                                              </tr>
                                              <input type="hidden" name="indexTemp">
                                              <input type="hidden" name="medicalStatus"   value='<bean:write name="med" property="statusId"/>'>                                              
                                              <input type="hidden" name="medicalRecordId" value='<bean:write name="med" property="medicalRecordId"/>'>                                              
                                              <input type="hidden" name="testId"          value='<bean:write name="med" property="testId"/>'>
                                              <input type="hidden" name="followUpDate"    value='<bean:write name="med" property="followUpDate"/>'>                                                                                           
                                              <input type="hidden" name="clientId"        value='<bean:write name="med" property="clientNo"/>'>                                                                                                                                         
                                              <input type="hidden" name="lastNameArr"     value='<bean:write name="med" property="lastName"/>'>                                                                                                                                         
                                              <input type="hidden" name="firstNameArr"    value='<bean:write name="med" property="firstName"/>'>                                                                                                                                                                                                                                     
                                              <input type="hidden" name="refNo"           value='<bean:write name="med" property="refNo"/>'>                                                                                                                                                                                                                                                                                   
                                              <input type="hidden" name="branches"        value='<bean:write name="med" property="branch"/>'>
                                              <input type="hidden" name="createdDate"     value='<bean:write name="med" property="createdDate"/>'>
                                              <%i++;%>
                                              </logic:iterate>
                                              
                                              
                                              </logic:notEmpty>
                                             <%} %>
                                           <!-- page navigation -->
											<input type="hidden" name="pageNo" value="<%=medicalRecordsForm.getPageNo()%>">
											<%
		          								int pageNumber = medicalRecordsForm.getPageNo();
		         							%>
                                           
                                           <% if (displayPage == null) { %>
												<tr>
												  <td class="headerrow4" colspan="13" align="left" width="100%"></td>
												</tr>
												<% } else { %>
													<tr>
														<td class="headerrow4" colspan="13" width="100%" height="10" class="label2" valign="bottom" >
															<!-- don't display link for previous page if the current page is the first page -->					
													        <% if (pageNumber > 1) { %>
																<a <%=iumColorW%> href="#" onclick="javascript:rePaginate(1,'<%=contextPath%>/listMedicalRecords.do');"><img src="images/pagnavfirst_off.gif" name="pagnavfirst" border="0" width="18" height="13"/></a>&nbsp;					
																<a <%=iumColorW%> href="#" onclick="javascript:rePaginate(<%=pageNumber-1%>,'<%=contextPath%>/listMedicalRecords.do');"><img src="images/pagnavleft_off.gif" name="pagnavleft" border="0" width="18" height="13"/></a>
												  			<% } else {%>
																<img src="images/pagnavfirst_on.gif" name="pagnavfirst" border="0" width="18" height="13"/>&nbsp;
																<img src="images/pagnavleft_on.gif" name="pagnavleft" border="0" width="18" height="13"/>
															 <%}%>
												             <% 
									          				  int currLink = 1;
													          int prevLink = 1;
									  			             %>
    								  			             <logic:iterate id="navLinks" name="displayPage" property="pageNumbers">
												            	<%currLink = ((Integer)navLinks).intValue();%>				
												            	<%if (((Integer)navLinks).intValue() == pageNumber){%>
									            					<b><bean:write name="navLinks"/></b>
											        	    	<%}else{%>		
									           						<%if ((currLink > (prevLink+1))) {%>
													        	    	...
												               		<%}%>
												               			<a <%=iumColorW%> href="#" onclick="rePaginate('<bean:write name="navLinks"/>','<%=contextPath%>/listMedicalRecords.do');" class="links2"><bean:write name="navLinks"/></a>
													           	<%}%>
									            	       	   	<%prevLink = currLink;%>
											               	</logic:iterate>
									
											        		<!-- don't display link for next page if the current page is the last page -->
									         		 		<%if(pageNumber < prevLink){%>
											         			<a <%=iumColorW%> href="#" onclick="rePaginate('<%=pageNumber+1%>','<%=contextPath%>/listMedicalRecords.do');"><img src="images/pagnavright_off.gif" name="pagnavright" border="0" width="18" height="13"/></a>&nbsp;
									          					<a <%=iumColorW%> href="#" onclick="rePaginate('<%=prevLink%>','<%=contextPath%>/listMedicalRecords.do');"><img src="images/pagnavlast_off.gif" width="18" height="13" border="0"></a>
									          				<%} else { %>
									          					<img src="images/pagnavright_on.gif" width="18" height="13" border="0">&nbsp;
									          					<img src="images/pagnavlast_on.gif" width="18" height="13" border="0">
										          			<%}%>        		
													   </td>
												</tr>
												<% } %> 
                                                      <tr><td></td></tr>
                                                    </table>
                                                  </td>
                                                </tr>
                                              </table>
                                              <table><tr><td>
                                              <div align="left" id="create_btn" ><input class="button1" type="button" value="Create" onclick="gotoPage('frm','createMedicalExam.do');" <%=CREATE_ACCESS%> ></div>
                                              </td></tr></table>
                                            </td>
                                          </tr>
                                        </table>
                                      </td>
                                    </tr>
                                    
                                    <tr>
                                      <td>
                                        <table border="1" bordercolor="<%=iumColorB%>" cellpadding=10 cellspacing=0 bgcolor="" width="510" class="listtable1">
                                          <tr>
                                            <td height="100%" valign="top">
                                              <table border="0" cellpadding="2" cellspacing="0">
                                                <tr>
                                                  <td valign="top">
                                                    <table border="0" cellpadding="2" cellspacing="0">
                                                      <tr>
                                                        <td><font size="1"><b>Assign Client No.</b></font></td>
                                                        <td><input type="textbox" class="label2" name="assignedClientNo"></td>
                                                        <td><font size="1"></font></td>
                                                      </tr>
                                                      <tr>
                                                        <td><font size="1"><b>Change Status to</b></font></td>
                                                        <td>
                                                        <ium:list className="label2" listBoxName="statusTo" type="<%=IUMConstants.LIST_CHANGE_MED_RECORD_STATUS%>" styleName="width:200" selectedItem="" onChange="enableRequiredFields();"/>                                                        </td>
                                                        <td><font size="1"></font></td>
                                                      </tr>
                                                      <tr>
                                                        <td><font size="1"><b>Date Conducted</b></font></td>
                                                        <td><input type="textbox" maxLength="9" class="label2" name="conductedDate" onKeyUp="getKeyDate(event,this);" >&nbsp;
                                                        <a href="#" onClick="popUpWin('<%=contextPath%>/jsp/calendar.jsp?textbox=frm.conductedDate',290,155);"><img src="<%=contextPath%>/images/calendar.gif" height="17" width="19" border="0">
												        </a>
                                                        </td>
                                                        <td><font size="1"></font></td>
                                                      </tr>
                                                      <tr>
                                                        <td><font size="1"><b>Appointment Date</b></font></td>
                                                        <td><input type="textbox" maxLength="9" class="label2" name="appointmentDate" onKeyUp="getKeyDate(event,this);">&nbsp;
                                                        <a href="#" onClick="popUpWin('<%=contextPath%>/jsp/calendar.jsp?textbox=frm.appointmentDate',290,155);"><img src="<%=contextPath%>/images/calendar.gif" height="17" width="19" border="0">
										        		</a>
                                                        </td>
                                                        <td><font size="1"></font></td>
                                                      </tr>
                                                      <tr>
                                                        <td><font size="1"><b>Appointment Time</b></font></td>
                                                        <td><input type="textbox" maxLength="8" class="label2" name="appointmentTime"></td>
                                                        <td><font size="1"></font></td>
                                                      </tr>
                                                      <tr>
                                                        <td colspan="3" align="left"><font size="1"><b><input type="button" class="button1" name="saveBtn" value="Save" <%=MAINTAIN_ACCESS%> onClick="saveStatus('<%=contextPath%>');"></b></font></td>
                                                      </tr>
                                                    </table>
                                                  </td>
                                                </tr>   
                                              </table> 
                                            </td>
                                          </tr>
                                        </table>
                                        <!-- BODY -->
                                      </td>
                                    </tr>
                                  </table>
                                </td>
                              </tr>
                                                      <tr>				
                          <td colspan="10" class="label1">&nbsp;</td>				
                        </tr>				
                        <tr>
                          <td colspan="10" class="label1"><font size="1">� 2003 Sun Life Financial. All rights reserved.</font></td>
                        </tr>
	         			 <tr><td class="label2">&nbsp;</td></tr> 
                              
                            </table>
                          </td>
                        </tr>
		            </table>
                  </td>
                </tr>                
              </table>
            </td>
          </tr>
        </table>
      </font>
    </form>
  </body>
</html>
</html:html>