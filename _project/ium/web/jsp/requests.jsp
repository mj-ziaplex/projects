<%@ taglib uri="/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/struts-html.tld" prefix="html" %>
<%@ taglib uri="/ium.tld" prefix="ium" %>
<%@ taglib uri="/oscache.tld" prefix="cache" %>

<%@ page language="java" import="java.util.*,com.slocpi.ium.data.*, com.slocpi.ium.ui.form.*, com.slocpi.ium.util.*, com.slocpi.ium.ui.util.*, java.text.*, com.slocpi.ium.underwriter.*" buffer="24kb"%>


<html:html locale="true">
<%
/* Andre Ceasar Dacanay - css enhancement - start */
String iumCss = "";
String iumBorderColor = "";
String iumColorW = "";
String iumColorO = "";
String companyCode = "";
try {
	ResourceBundle rbWMSSP = ResourceBundle.getBundle("dbconfig");
	iumCss = rbWMSSP.getString("IUM_CSS_VERSION");
	companyCode = iumCss;
	iumCss = "_"+iumCss;
} catch (Exception e){}

if(iumCss.equals("_CP")){
	iumBorderColor = "bordercolor=\"#2a4c7c\"";
	iumColorO = "#FFCB00";
	companyCode="";
}else if(iumCss.equals("_GF")){
	iumBorderColor = "bordercolor=\"#303A77\"";
	iumColorO = "#303A77";
	iumColorW = "style=\"color:white;\"";
	companyCode = companyCode+" ";
}else{
	iumBorderColor = "bordercolor=\"#2a4c7c\";";
	iumColorO = "#FFCB00";
}
/* Andre Ceasar Dacanay - css enhancement - end */
%>
<% String contextPath = request.getContextPath(); %>
<% RequestFilterForm reqFilter = (RequestFilterForm)request.getAttribute("requestFilter"); 
   String [] requests = reqFilter.getCheckedRefNum();
   ArrayList requestCheck = new ArrayList();
   if (requests != null) {
   	 for (int i=0; i<requests.length; i++){
	   requestCheck.add(requests[i]);
   	 }
   }
%>
<% Page displayPage = reqFilter.getPage(); 
   request.setAttribute("displayPage",displayPage);
   String roles = IUMConstants.ROLES_NB_STAFF+","+IUMConstants.ROLES_NB_REVIEWER+","+IUMConstants.ROLES_NB_ADMIN+","+IUMConstants.ROLES_FACILITATOR+","+IUMConstants.ROLES_UNDERWRITER;
    StateHandler sessionHandler = new StateHandler();
    Access access = new Access();
    
	UserData userData = sessionHandler.getUserData(request); 
    Roles userRoles = new Roles(); // Class that checks Roles of user contained in the array list

	// -Removed by Randall replaced by code written above
	// UserProfileData userPref = userData.getProfile();
	// String usrRole = userPref.getRole();
	
    

    
	
	
%>
  
<%
    // This routine queries the access rights of the users (RCRIVERA)
    //
    // --- START --- 
    
	HashMap userAccess = sessionHandler.getUserAccess(request);
    if(!access.hasAccess(userAccess,IUMConstants.PAGE_ASSESSMENT_REQUEST,IUMConstants.ACC_INQUIRE) ){                                                                                                                    
    	response.sendRedirect("noAccessPage.jsp");   
    } 
    
    String CREATE_ACCESS = "";
    String MAINTAIN_ACCESS = "";
    String DELETE_ACCESS = "";
    String EXECUTE_ACCESS = "";  
    String REASSIGNTO_ACCESS = "";
        
     
    if(!access.hasAccess(userAccess,IUMConstants.PAGE_ASSESSMENT_DETAILS,IUMConstants.ACC_CREATE)){
    // || !userRoles.getRoles(userData.getRoles(),IUMConstants.ROLES_NB_STAFF) ){                                                                                                                     
	  CREATE_ACCESS = "DISABLED"; 	
    }
    if(!access.hasAccess(userAccess,IUMConstants.PAGE_ASSESSMENT_REQUEST,IUMConstants.ACC_MAINTAIN) ){                                                                                                                     
	  MAINTAIN_ACCESS = "DISABLED";
    }
    if(!access.hasAccess(userAccess,IUMConstants.PAGE_ASSESSMENT_REQUEST,IUMConstants.ACC_DELETE) ){                                                                                                                     
	  DELETE_ACCESS = "DISABLED";
    }
    if(!access.hasAccess(userAccess,IUMConstants.PAGE_ASSESSMENT_REQUEST,IUMConstants.ACC_EXECUTE) ){                                                                                                                     
	  EXECUTE_ACCESS = "DISABLED"; 	
    }
    if(!access.hasAccess(userAccess,IUMConstants.PAGE_ASSESSMENT_REQUEST,IUMConstants.ACC_EXECUTE) ){  //|| !userRoles.getRoles(userData.getRoles(),IUMConstants.ROLES_UNDERWRITER_SUPERVISOR)  ){ 
	  REASSIGNTO_ACCESS = "DISABLED"; 	
    }
    
    // --- END ---
 %>    

<html>
  <head>
    <title><%=companyCode%>Integrated Underwriting and Medical System</title>
    <link href="<%=contextPath%>/css/main<%=iumCss%>.css" rel="stylesheet" type="text/css">
    <script language="Javascript" src="<%=contextPath%>/js/navigation.js"></script>
    <script language="Javascript" src="<%=contextPath%>/js/validation.js"></script>
    <script language="Javascript" src="<%=contextPath%>/jsp/validate_request.jsp"></script>

    <script language="javascript">
	<!-- kristian -->
	function rePaginate (page, actionUrl)
	{
		var frm = document.forms[0];
		if(frm.pageNo != null)
		{
			frm.pageNo.value = page;	
		}
		resetFilterFields();
		submitForm(actionUrl);
	}
	
	function submitForm(actionUrl)
	{	
		document.requestFilterForm.action = actionUrl;
		document.requestFilterForm.submit();
	}
	<!-- kristian -->
	
	function createAssessmentRequest(){
		window.location= 'createAssessmentRequest.do';
	}
	
	
	function viewDetails(refNo, url) {
		document.requestFilterForm.reset();
		var frm = document.requestFilterForm;
		frm.clickedRefNo.value = refNo;
		frm.isMaintain.value = false;
   	    gotoPage('requestFilterForm', url);
	}

	
	function displayFormattedCurrency(value){
		var data = formatCurrency(value);
    	document.write(data);
    }
    
    
    function uwLoadReload(){
    	parent.location='listAssessmentRequests.do';
    }
    </script>  
    
 

  </head>

  <%
  boolean isMaintain = false;
  String reqIsMaintain = request.getParameter("isMaintain");
  if ((reqIsMaintain != null) && (reqIsMaintain.equals("true"))) {
    isMaintain = true;
  }
  %>

  <body leftmargin="0" topmargin="0" onLoad="<% if (isMaintain) { %> enableAssignTo(); <% } else { %> disableAssignTo(); <% } %>" onKeyUp="checkEnter(event,'search');" onUnload="closePopUpWin();">

    <form name="requestFilterForm" method="post">
      <!-- hidden field to store maintain flag for request details -->
      <input type="hidden" name="clickedRefNo" value="<%=reqFilter .getClickedRefNo()%>">      
      <input type="hidden" name="isMaintain" value="<%=isMaintain%>">

      <table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%">
        <tr>
          <td width="100%" colspan="2">    
            <jsp:include page="header.jsp" flush="true"/>
          </td>
        </tr>
        <tr>
          <td rowspan="2" bgcolor="<%=iumColorO%>" height="100%" width="20"><img src="<%=contextPath%>/images/spacer.gif" width="20"></td>
          <td background="<%=contextPath%>/images/GradientGray.jpg" height="27" width="100%" valign="top">&nbsp;&nbsp;
            <span class="main">Integrated Underwriting and Medical System</span>
          </td>
        </tr>  
        <tr>
          <td width="100%" height="100%" valign="top">
            <!-- BODY -->
            <table cellpadding="0" cellspacing="0" border="0" <%=iumBorderColor%>width="100%">
              <tr>
                <td>
                  <table cellpadding="10" cellspacing="0" border="0" width="100%">
                    <tr>
                      <td>
                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                          <tr>
                            <td colspan="2"><img src="<%=contextPath%>/images/assessmentrequest_title.jpg" border="0"></td>
                          </tr>
                          <tr>
                            <td colspan="2">&nbsp;</td>
                          </tr>
                          <tr>
                            <td>
                              <!-- START Enter Search Criteria -->
                              <table border="0" <%=iumBorderColor%> cellpadding="0" cellspacing="0" bgcolor="" width="100%"> 
                                <tr><td colspan="1" class="label2"><b>Enter Search Criteria</b></td></tr>
                                <tr>
                                  <td>
                                    <table border="1" <%=iumBorderColor%> cellpadding="6" cellspacing="0" bgcolor="" width="600" class="listtable1">
                                      <tr>
                                        <td>                                        
                                          <input type="hidden" name="tmp_referenceNumber" value='<bean:write name="requestFilter" property="refNo"/>' >
                                          <input type="hidden" name="tmp_lob" value='<%=reqFilter .getLob()%>'>
                                          <input type="hidden" name="tmp_branch" value='<%=reqFilter .getBranch()%>'>
                                          <input type="hidden" name="tmp_status" value='<%=reqFilter .getStatus()%>'>
                                          <input type="hidden" name="tmp_agent" value='<%=reqFilter.getAgent()%>'>
                                          <input type="hidden" name="tmp_location" value='<%=reqFilter.getLocation()%>'>
                                          <input type="hidden" name="tmp_clientNumber" value='<bean:write name="requestFilter" property="clientNumber"/>'>
                                          <input type="hidden" name="tmp_assignedTo" value='<%=reqFilter.getAssignedTo()%>'>
                                          <input type="hidden" name="tmp_nameOfInsured" value='<bean:write name="requestFilter" property="nameOfInsured"/>'>
                                          <input type="hidden" name="tmp_applicationReceivedDate" value='<bean:write name="requestFilter" property="applicationReceivedDate"/>'>
                                          <input type="hidden" name="tmp_coverageAmount" value='<bean:write name="requestFilter" property="coverageAmount"/>'>
                                          <input type="hidden" name="tmp_requirementCode" value='<bean:write name="requestFilter" property="requirementCode"/>'>
                                          <input type="hidden" name="sortOrder" value='<bean:write name="requestFilter" property="sortOrder"/>'>
                                          <input type="hidden" name="columns" value='<bean:write name="requestFilter" property="columns"/>'>
										  
										  <cache:cache key="search" time="-1" groups="searchGroup">
                                          <table border="0" cellpadding=2 cellspacing=0 >
                                            <tr>
                                              <td class="label2"><b>Reference No.</b></td>
                                              <td class="label2"><input type="textbox" class="label2" style="width:140" name="refNo" maxlength="15" value="<bean:write name="requestFilter" property="refNo"/>"></td>
                                              <td class="label2" width="20">&nbsp;</td>
                                              <td class="label2"><b>Branch</b></td>
                                              <td class="label2"><ium:list className="label2" listBoxName="branch" type="<%=IUMConstants.LIST_BRANCHES%>" styleName="width:280"  selectedItem="<%=reqFilter.getBranch()%>"/></td>
                                            </tr>
                                            <tr></tr>
                                            <tr>
                                              <td class="label2"><b>LOB</b></td>
                                              <td class="label2"><ium:list className="label2" listBoxName="lob" type="<%=IUMConstants.LIST_LOB%>" styleName="width:140"  selectedItem="<%=reqFilter .getLob()%>"/></td>
                                              <td class="label2" width="20">&nbsp;</td>
                                              <td class="label2"><b>Requirement Code</b></td>
                                              <td class="label2"><ium:list className="label2" listBoxName="requirementCode" type="<%=IUMConstants.LIST_REQUIREMENT_CODE%>" styleName="width:280"  selectedItem="<%=reqFilter.getRequirementCode()%>"  filter=""/></td>																						
                                            </tr>
                                            <tr>
                                              <td class="label2"><b>Status</b></td>
                                              <td class="label2"><ium:list className="label2" listBoxName="status" type="<%=IUMConstants.LIST_REQUEST_STATUS%>" styleName="width:140"  selectedItem="<%=reqFilter.getStatus()%>"/></td>																					
                                              <td class="label2">&nbsp;</td>
                                              <td class="label2"><b>Location</b></td>
                                              <td class="label2"><ium:list className="label2" listBoxName="location" type="<%=IUMConstants.LIST_USERS%>" styleName="width:280"  selectedItem="<%=reqFilter.getLocation()%>" filter="<%= roles%>"/></td>
                                            </tr>
                                            <tr>
                                             <td class="label2"><b>Client No.</b></td>
                                             <td class="label2"><input type="textbox" class="label2" style="width:140" name="clientNumber" maxlength="10" value="<bean:write name="requestFilter" property="clientNumber"/>"></td>
                                             <td class="label2">&nbsp;</td>
                                             <td class="label2"><b>Assigned to</b></td>
                                             <td class="label2"><ium:list className="label2" listBoxName="assignedTo" type="<%=IUMConstants.LIST_USERS%>" styleName="width:280"  selectedItem="<%=reqFilter.getAssignedTo()%>" filter="<%= roles%>"/></td>
                                            </tr>
                                            <tr>
                                              <td class="label2"><b>Application Received Date</b></td>
                                              <td class="label2"><input type="textbox" class="label2" style="width:140" name="applicationReceivedDate" onKeyUp="getKeyDate(event,this);" maxlength="9" onBlur="checkDate(this);" value="<bean:write name="requestFilter" property="applicationReceivedDate"/>">&nbsp;<a href="#" onUnload="closePopUpWin();" onClick="popUpWin('<%=contextPath%>/jsp/calendar.jsp?textbox=requestFilterForm.applicationReceivedDate',290,155);"><img src="<%=contextPath%>/images/calendar.gif" height="17" width="19" border="0"></a></td>                                              
											  <td class="label2">&nbsp;</td>
                                              <td class="label2"><b>Agent</b></td>
                                              <td class="label2"><ium:list className="label2" listBoxName="agent" type="<%=IUMConstants.LIST_AGENT%>" styleName="width:280"  selectedItem="<%=reqFilter.getAgent()%>" filter="<%=IUMConstants.ROLES_AGENTS%>"/></td>
                                            </tr> 
                                            <tr>
                                              <td class="label2"><b>Amount Covered</b></td>
                                              <td class="label2"><input type="textbox" class="label2" style="width:140" name="coverageAmount" maxlength="14" onBlur="document.requestFilterForm.coverageAmount.value=formatCurrency(document.requestFilterForm.coverageAmount.value);" onKeyUp="getKeyAmount(event,this);" value="<bean:write name="requestFilter" property="coverageAmount"/>"></td>
                                              <td class="label2">&nbsp;</td>
                                              <td class="label2"><b>Name of Insured</b></td>
                                              <td class="label2"><input type="textbox" class="label2" style="width:280" name="nameOfInsured" maxlength="40" value="<bean:write name="requestFilter" property="nameOfInsured"/>"></td>                                          
                                            </tr>
                                            <tr>
                                              <td colspan="5" align="left">
                                              	<input type="button" class="button1" name ="search" value=" Search " onClick="validateSearchCriteria()">                          
                                              	<input type="hidden" name="startSearch">
                                              	<input type="button" class="button1" value="  Reset  " onClick="resetButton()">
                                              </td>
                                            </tr>
                                          </table>
                                          </cache:cache>
                                        </td>
                                      </tr>
                                    </table>
                                  </td>
                                </tr>
                              </table>
                              
                              <!-- END Enter Search Criteria -->

                            </td>
                            <td valign="top" align="left">
								<%if( userRoles.getRoles(userData.getRoles(),IUMConstants.ROLES_UNDERWRITER_SUPERVISOR) ){%> 
								<div style="visibility:visible">
								<%}else{%> 
								<div class='hide'>
								<%}%>
                                <div class=label2><b>Underwriter Load</b></div>
                                <table border="1" <%=iumBorderColor%> cellpadding=10 cellspacing=0 bgcolor="" class="listtable1" width="300">
                                   <tr>
                                    <td colspan="2" height="100%" class="label2">
                                    
                                      <!-- This jsp:include tag replaced the iFrame below as requested March 14, 2007  -->
									    <div style="OVERFLOW:auto; WIDTH: 450px; HEIGHT: 180px;">
											  <jsp:include page="underwriterLoad.jsp" flush="true"/>
                                        <br>
                                        </div>
                                        <span align="right"><input type="button" class="button1" value="Refresh" onClick="uwLoadReload();" ></span>
                                      <!-- <IFRAME name="uwload" src="<%=contextPath%>/underwriterLoad.do" width="450" height="165" scrolling="auto" frameborder="0">
                                      </IFRAME><br>  
                                      <span align="right"><input type="button" class="button1" value="Refresh" onClick="iframeReload();" ></span>
                                      -->
                                      
                                    </td>
                                  </tr>
                                </table>
                              </div>
                            </td>
                          </tr>        
                          <tr><td>&nbsp;</td></tr>
                          <tr>
                            <td colspan="3"><html:errors/>
                              <table border="0" <%=iumBorderColor%> cellpadding="0" cellspacing="0" bgcolor="" width="100%">
                                <tr>
                                  <td colspan="2">
                                    <a href="listAssessmentRequests.do" class=links1><% if(reqFilter.getStatus() == null){%><img src="<%=contextPath%>/images/folderOpen.gif" border="0"><%} else {%> <img src="<%=contextPath%>/images/folderClosed.gif" border="0"> <%}%> My Pending </a> &nbsp;
				                    <a href="javascript:filterByStatus('<%=IUMConstants.STATUS_NB_REVIEW_ACTION%>');" class=links1> <% if(reqFilter.getStatus() != null && reqFilter.getStatus().equals(String.valueOf(IUMConstants.STATUS_NB_REVIEW_ACTION))) {%><img src="<%=contextPath%>/images/folderOpen.gif" border="0"> <%} else { %><img src="<%=contextPath%>/images/folderClosed.gif" border="0"> <%}%> NB Review </a> &nbsp;
                                    <a href="javascript:filterByStatus('<%=IUMConstants.STATUS_FOR_TRANSMITTAL_USD%>')" class=links1><% if(reqFilter.getStatus() != null && reqFilter.getStatus().equals(String.valueOf(IUMConstants.STATUS_FOR_TRANSMITTAL_USD))) {%><img src="<%=contextPath%>/images/folderOpen.gif" border="0"><%} else { %><img src="<%=contextPath%>/images/folderClosed.gif" border="0"> <%}%>Transmittal to USD </a> &nbsp;
                                    <a href="javascript:filterByStatus('<%=IUMConstants.STATUS_FOR_FACILITATOR_ACTION%>')" class=links1><% if(reqFilter.getStatus() != null && reqFilter.getStatus().equals(String.valueOf(IUMConstants.STATUS_FOR_FACILITATOR_ACTION))) {%><img src="<%=contextPath%>/images/folderOpen.gif" border="0"><%} else { %><img src="<%=contextPath%>/images/folderClosed.gif" border="0"> <%}%>Facilitator's Action </a> &nbsp;
                                    <a href="javascript:filterByStatus('<%=IUMConstants.STATUS_FOR_ASSESSMENT%>')" class=links1><% if(reqFilter.getStatus() != null && reqFilter.getStatus().equals(String.valueOf(IUMConstants.STATUS_FOR_ASSESSMENT))) {%><img src="<%=contextPath%>/images/folderOpen.gif" border="0"><%} else { %><img src="<%=contextPath%>/images/folderClosed.gif" border="0"> <%}%>For Assessment </a> &nbsp;
                                    <a href="javascript:filterByStatus('<%=IUMConstants.STATUS_UNDERGOING_ASSESSMENT%>')" class=links1><% if(reqFilter.getStatus() != null && reqFilter.getStatus().equals(String.valueOf(IUMConstants.STATUS_UNDERGOING_ASSESSMENT))) {%><img src="<%=contextPath%>/images/folderOpen.gif" border="0"><%} else { %><img src="<%=contextPath%>/images/folderClosed.gif" border="0"><%}%> Undergoing Assessment </a> &nbsp;
                                    <a href="javascript:filterByStatus('<%=IUMConstants.STATUS_APPROVED%>')" class=links1><% if(reqFilter.getStatus() != null && reqFilter.getStatus().equals(String.valueOf(IUMConstants.STATUS_APPROVED))) {%><img src="<%=contextPath%>/images/folderOpen.gif" border="0"><%} else { %><img src="<%=contextPath%>/images/folderClosed.gif" border="0"> <%}%>Approved </a> &nbsp;
                                    <a href="javascript:filterByStatus('<%=IUMConstants.STATUS_DECLINED%>')" class=links1><% if(reqFilter.getStatus() != null && reqFilter.getStatus().equals(String.valueOf(IUMConstants.STATUS_DECLINED))) {%><img src="<%=contextPath%>/images/folderOpen.gif" border="0"><%} else { %><img src="<%=contextPath%>/images/folderClosed.gif" border="0"> <%}%>Declined </a> &nbsp;
                                    <a href="javascript:filterByStatus('<%=IUMConstants.STATUS_NOT_PROCEEDED_WITH%>')" class=links1><% if(reqFilter.getStatus() != null && reqFilter.getStatus().equals(String.valueOf(IUMConstants.STATUS_NOT_PROCEEDED_WITH))) {%><img src="<%=contextPath%>/images/folderOpen.gif" border="0"><%} else { %><img src="<%=contextPath%>/images/folderClosed.gif" border="0"> <%}%> NPW </a>
								  </td>
                                </tr>
                                <tr>
                                  <td colspan="3" height="100%">
                                    <table border="1" <%=iumBorderColor%> cellpadding="10" cellspacing="0" bgcolor="" width="100%" class="listtable1">
                                      <tr>
                                        <td colspan="3" height="100%" class="label2" valign="top">
                                          <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                            <tr>
                                              <td align="left">
                                                <table border="1" <%=iumBorderColor%> cellpadding="1" cellspacing="0" class="listtable1">
                                                  <tr>
                                                    <td>
                                                    <%
                                                    //selected status
                                                    String selectedStatus = request.getParameter("changeStatusTo");
                                                    if (selectedStatus == null) {
                                                      selectedStatus = reqFilter.getStatus();
                                                    }

                                                    //filter for assigned to
                                                    AssessmentRequest ar = new AssessmentRequest();			
                                                    String filter = ar.retrieveAssignToRolesFilter(null, selectedStatus, null);
                                                    %>
                                                      <table width="100%">
                                                        <tr>
                                                          <td class="label2"><b>Change Status to </b></td>
                                                          <td class="label2">
                                                            <ium:list className="label2" listBoxName="changeStatusTo" type="<%=IUMConstants.LIST_REQUEST_STATUS%>" styleName="width:200"  selectedItem="<%=selectedStatus%>" onChange="setAssignTo();"/>&nbsp;
                                                            <input type="button" class="button1" value="Save" onClick="saveMultipleRequest()" <%=EXECUTE_ACCESS%>>
                                                          </td>
                                                        </tr>
                                                        <tr>
                                                          <td class="label2"><b>Assign to</b></td>
                                                          <td class="label2"><ium:list className="label2" listBoxName="changeAssignTo" type="<%=IUMConstants.LIST_ASSIGN_TO%>" styleName="width:200" selectedItem="" filter="<%=filter%>"/></td>
                                                        </tr>
                                                      </table>       
                                                    </td>
                                                  </tr>
                                                </table>                                           
                                              </td>
                                              <td align="right">
                                                <!-- place condition here -->
                                                <table border="0" cellpadding="0" cellspacing="0">
                                                  <tr>
                                                    <td>
                                                       <table width="100%" border="0">
                                                        <tr>
                                                          <td > &nbsp </td>
                                                        </tr>
													   </table>    
                                          <!--//       <table width="100%" border="0">
                                                        <tr>
                                                          <td class="label2"><b>Reassign to </b></td>
                                                          <td align="right">
                                                            <ium:list className="label2" listBoxName="reassignTo" type="<%=IUMConstants.LIST_USERS%>" styleName="width:200"  selectedItem="<%=IUMConstants.ROLES_UNDERWRITER%>" filter="<%=IUMConstants.ROLES_UNDERWRITER%>"/>
                                                            <input type="button" class="button1" value="Reassign" onClick="saveMultipleReassign()" <%= REASSIGNTO_ACCESS %> >
                                                            <input type="hidden" name="updateType" value="<%=IUMConstants.PROCESS_TYPE_MULTIPLE%>">
                                                          </td>
                                                        </tr>
													   </table>   -->
													   <table>
                                                        <tr>
                                                          <td>
                                                            <input type="button" class="button1" value="Create" onClick="createAssessmentRequest();" <%= CREATE_ACCESS %>>
                                                            <input type="button" class="button1" value="Update Location" onClick="updateLocation();" <%= EXECUTE_ACCESS %>>
															<input type="button" class="button1" value="Forward to Records" onClick="mutilpleForwardToRecords();" <%= EXECUTE_ACCESS %>>
															<!-- Added the hidden field for since this is needed by UpdateLocationAction -->
	 													    <input type="hidden" name="updateType" value="<%=IUMConstants.PROCESS_TYPE_MULTIPLE%>">
                                                          </td>
                                                        </tr>
                                                      </table>    
                                                    </td>
                                                  </tr>
                                                </table>
                                              </td>
                                            </tr>
                                          </table>                                          
                                          <table border="0" cellpadding="2" cellspacing="1" bgcolor=#FFFFFF style="border: 1px solid #D1D1D1;" class="listtable1">
                                            <tr class="headerrow1">
                                              <td width="10">

                                              <input type="checkbox" class="label1" name="refcheck" onClick="toggleAll(document.requestFilterForm.checkedRefNum)">
                                              
                                              </td>
                                              <td width="100">
                                              <%if(reqFilter.getSortOrder().equals(IUMConstants.ASCENDING) && reqFilter.getColumns()==IUMConstants.SORT_BY_REFERENCE_NUM ){ %>
	                                              <a <%=iumColorW%> href="javascript:sortBy('<%=IUMConstants.SORT_BY_REFERENCE_NUM%>','<%=IUMConstants.DESCENDING%>');"  >
	                                              REFERENCE NO.
	                                              <img src="<%=contextPath%>/images/uarrow.gif" border="0">
	                                              </a>
	                                          <%} else if(reqFilter.getSortOrder().equals(IUMConstants.ASCENDING)&& reqFilter.getColumns()!=IUMConstants.SORT_BY_REFERENCE_NUM ) {%>
                                                  <a <%=iumColorW%> href="javascript:sortBy('<%=IUMConstants.SORT_BY_REFERENCE_NUM%>','<%=IUMConstants.DESCENDING%>');"  >
	                                              REFERENCE NO.
	                                              <img src="<%=contextPath%>/images/uarrow.gif" border="0">
	                                              </a>
                                              <%} else {%>
	                                              <a <%=iumColorW%> href="javascript:sortBy('<%=IUMConstants.SORT_BY_REFERENCE_NUM%>','<%=IUMConstants.ASCENDING%>');"  >
	                                              REFERENCE NO.
	                                              <img src="<%=contextPath%>/images/darrow.gif" border="0">
	                                              </a>
                                              <%}%>
                                              </td>
                                              <td width="30">
                                              <%if(reqFilter.getSortOrder().equals(IUMConstants.ASCENDING) && reqFilter.getColumns()==IUMConstants.SORT_BY_LOB ){ %>
	                                              <a <%=iumColorW%> href="javascript:sortBy('<%=IUMConstants.SORT_BY_LOB%>','<%=IUMConstants.DESCENDING%>');"  >
	                                              LOB
	                                              <img src="<%=contextPath%>/images/uarrow.gif" border="0">
	                                              </a>
                                              <%} else {%>
	                                              <a <%=iumColorW%> href="javascript:sortBy('<%=IUMConstants.SORT_BY_LOB%>','<%=IUMConstants.ASCENDING%>');"  >
	                                              LOB
	                                              <img src="<%=contextPath%>/images/darrow.gif" border="0">
	                                              </a>
                                              <%}%>
                                              </td>
                                              <td width="150">
                                              <%if(reqFilter.getSortOrder().equals(IUMConstants.ASCENDING) && reqFilter.getColumns()== IUMConstants.SORT_BY_STATUS){ %>
	                                              <a <%=iumColorW%> href="javascript:sortBy('<%=IUMConstants.SORT_BY_STATUS%>','<%=IUMConstants.DESCENDING%>');"  >
	                                              STATUS
	                                              <img src="<%=contextPath%>/images/uarrow.gif" border="0">
	                                              </a>
                                              <%} else {%>
	                                              <a <%=iumColorW%> href="javascript:sortBy('<%=IUMConstants.SORT_BY_STATUS%>','<%=IUMConstants.ASCENDING%>');"  >
	                                              STATUS
	                                              <img src="<%=contextPath%>/images/darrow.gif" border="0">
	                                              </a>
                                              <%}%>
                                              </td>
                                              <td width="170">
                                              <%if(reqFilter.getSortOrder().equals(IUMConstants.ASCENDING) && reqFilter.getColumns()==IUMConstants.SORT_BY_NAME_OF_INSURED){ %>
	                                              <a <%=iumColorW%> href="javascript:sortBy('<%=IUMConstants.SORT_BY_NAME_OF_INSURED%>','<%=IUMConstants.DESCENDING%>');"  >
	                                              NAME OF INSURED
	                                              <img src="<%=contextPath%>/images/uarrow.gif" border="0">
	                                              </a>
                                              <%} else {%>
	                                              <a <%=iumColorW%> href="javascript:sortBy('<%=IUMConstants.SORT_BY_NAME_OF_INSURED%>','<%=IUMConstants.ASCENDING%>');"  >
	                                              NAME OF INSURED
	                                              <img src="<%=contextPath%>/images/darrow.gif" border="0">
	                                              </a>
                                              <%}%>
                                              </td>
                                              <td width="130">
											  <%if(reqFilter.getSortOrder().equals(IUMConstants.ASCENDING) && reqFilter.getColumns()==IUMConstants.SORT_BY_AMOUNT_COVERED){ %>
	                                              <a <%=iumColorW%> href="javascript:sortBy('<%=IUMConstants.SORT_BY_AMOUNT_COVERED%>','<%=IUMConstants.DESCENDING%>');"  >
	                                              AMOUNT COVERED
	                                              <img src="<%=contextPath%>/images/uarrow.gif" border="0">
	                                              </a>
                                              <%} else {%>
	                                              <a <%=iumColorW%> href="javascript:sortBy('<%=IUMConstants.SORT_BY_AMOUNT_COVERED%>','<%=IUMConstants.ASCENDING%>');"  >
	                                              AMOUNT COVERED
	                                              <img src="<%=contextPath%>/images/darrow.gif" border="0">
	                                              </a>
                                              <%}%>                                              
                                              </td>
                                              <td width="150">
											  <%if(reqFilter.getSortOrder().equals(IUMConstants.ASCENDING) && reqFilter.getColumns()==IUMConstants.SORT_BY_ASSIGNED_TO){ %>
	                                              <a <%=iumColorW%> href="javascript:sortBy('<%=IUMConstants.SORT_BY_ASSIGNED_TO%>','<%=IUMConstants.DESCENDING%>');"  >
	                                              ASSIGNED TO
	                                              <img src="<%=contextPath%>/images/uarrow.gif" border="0">
	                                              </a>
                                              <%} else {%>
	                                              <a <%=iumColorW%> href="javascript:sortBy('<%=IUMConstants.SORT_BY_ASSIGNED_TO%>','<%=IUMConstants.ASCENDING%>');"  >
	                                              ASSIGNED TO
	                                              <img src="<%=contextPath%>/images/darrow.gif" border="0">
	                                              </a>
                                              <%}%>                                              
                                              </td>
                                              <td width="130">
											  <%if(reqFilter.getSortOrder().equals(IUMConstants.ASCENDING) && reqFilter.getColumns()==IUMConstants.SORT_BY_BRANCH){ %>
	                                              <a <%=iumColorW%> href="javascript:sortBy('<%=IUMConstants.SORT_BY_BRANCH%>','<%=IUMConstants.DESCENDING%>');"  >
	                                              BRANCH
	                                              <img src="<%=contextPath%>/images/uarrow.gif" border="0">
	                                              </a>
                                              <%} else {%>
	                                              <a <%=iumColorW%> href="javascript:sortBy('<%=IUMConstants.SORT_BY_BRANCH%>','<%=IUMConstants.ASCENDING%>');"  >
	                                              BRANCH
	                                              <img src="<%=contextPath%>/images/darrow.gif" border="0">
	                                              </a>
                                              <%}%>                                              
                                              </td>
                                              <td width="140">
											  <%if(reqFilter.getSortOrder().equals(IUMConstants.ASCENDING) && reqFilter.getColumns()==IUMConstants.SORT_BY_AGENT){ %>
	                                              <a <%=iumColorW%> href="javascript:sortBy('<%=IUMConstants.SORT_BY_AGENT%>','<%=IUMConstants.DESCENDING%>');"  >
	                                              AGENT
	                                              <img src="<%=contextPath%>/images/uarrow.gif" border="0">
	                                              </a>
                                              <%} else {%>
	                                              <a <%=iumColorW%> href="javascript:sortBy('<%=IUMConstants.SORT_BY_AGENT%>','<%=IUMConstants.ASCENDING%>');"  >
	                                              AGENT
	                                              <img src="<%=contextPath%>/images/darrow.gif" border="0">
	                                              </a>
                                              <%}%>                                              
                                              </td>
                                              <td width="100">
											  <%if(reqFilter.getSortOrder().equals(IUMConstants.ASCENDING) && reqFilter.getColumns()==IUMConstants.SORT_BY_APP_RECEIVED_DATE){ %>
	                                              <a <%=iumColorW%> href="javascript:sortBy('<%=IUMConstants.SORT_BY_APP_RECEIVED_DATE%>','<%=IUMConstants.DESCENDING%>');"  >
	                                              APP RECD DATE
	                                              <img src="<%=contextPath%>/images/uarrow.gif" border="0">
	                                              </a>
                                              <%} else {%>
	                                              <a <%=iumColorW%> href="javascript:sortBy('<%=IUMConstants.SORT_BY_APP_RECEIVED_DATE%>','<%=IUMConstants.ASCENDING%>');"  >
	                                              APP RECD DATE
	                                              <img src="<%=contextPath%>/images/darrow.gif" border="0">
	                                              </a>
                                              <%}%>                                                  
                                              </td>
                                              <td width="125">
											  <%if(reqFilter.getSortOrder().equals(IUMConstants.ASCENDING) && reqFilter.getColumns()==IUMConstants.SORT_BY_LOCATION){ %>
	                                              <a <%=iumColorW%> href="javascript:sortBy('<%=IUMConstants.SORT_BY_LOCATION%>','<%=IUMConstants.DESCENDING%>');"  >
	                                              LOCATION
	                                              <img src="<%=contextPath%>/images/uarrow.gif" border="0">
	                                              </a>
                                              <%} else {%>
	                                              <a <%=iumColorW%> href="javascript:sortBy('<%=IUMConstants.SORT_BY_LOCATION%>','<%=IUMConstants.ASCENDING%>');"  >
	                                              LOCATION
	                                              <img src="<%=contextPath%>/images/darrow.gif" border="0">
	                                              </a>
                                              <%}%>                                                  
                                              </td>
                                            </tr>


                                            <%
                                            int i=0;
                                            String tr_class;
                                            String td_bgcolor;
                                            %>
<%--
                                            <logic:iterate id="req" name="assessmentRequestList" property="assessmentRequestList" scope="request">	
--%>
                                            <logic:iterate id="req" name="displayPage" property="list">
                                            <%
                                            String amountCovered = ((AssessmentRequestForm)req).getAmountCovered();
											String refNumber = ((AssessmentRequestForm)req).getRefNo();

                                            NumberFormat nf = NumberFormat.getNumberInstance(); 
                                            DecimalFormat df = (DecimalFormat)nf; 
                                            df.applyPattern("###,##0.00"); 
                                            amountCovered = df.format(Double.parseDouble(amountCovered)); 
      
                                            if (i%2 == 0) {
                                              tr_class = "row1";
                                              td_bgcolor = "#CECECE";
                                            }
                                            else {
                                              tr_class = "row2";
                                              td_bgcolor = "#EDEFF0";
                                            }
                                            %>

                                            <tr class="<%=tr_class%>">
                                              <td><input type="checkbox" class="label1" name="checkedRefNum" value='<bean:write name="req" property="refNo"/>' <%=requestCheck.contains(refNumber)?"checked":""%>></td>
                                              <td style="cursor:hand" onclick="viewDetails('<bean:write name="req" property="refNo"/>', 'viewKOReqMedLabSpecial.do');"><bean:write name="req" property="refNo"/><bean:write name="req" property="policySuffix"/></td>
                                              <td style="cursor:hand" onclick="viewDetails('<bean:write name="req" property="refNo"/>', 'viewKOReqMedLabSpecial.do');"><bean:write name="req" property="lob"/></td>
                                              <td style="cursor:hand" onclick="viewDetails('<bean:write name="req" property="refNo"/>', 'viewKOReqMedLabSpecial.do');"><bean:write name="req" property="requestStatus"/></td>
                                              <td style="cursor:hand" onclick="viewDetails('<bean:write name="req" property="refNo"/>', 'viewKOReqMedLabSpecial.do');"><bean:write name="req" property="insuredName"/></td>
                                              <td style="cursor:hand" onclick="viewDetails('<bean:write name="req" property="refNo"/>', 'viewKOReqMedLabSpecial.do');"><div align="right"><%=amountCovered%></div></td>
                                              <td style="cursor:hand" onclick="viewDetails('<bean:write name="req" property="refNo"/>', 'viewKOReqMedLabSpecial.do');"><bean:write name="req" property="assignedTo"/></td>
                                              <td style="cursor:hand" onclick="viewDetails('<bean:write name="req" property="refNo"/>', 'viewKOReqMedLabSpecial.do');"><bean:write name="req" property="branchName"/></td>
                                              <td style="cursor:hand" onclick="viewDetails('<bean:write name="req" property="refNo"/>', 'viewKOReqMedLabSpecial.do');"><bean:write name="req" property="agentName"/></td>
                                              <td style="cursor:hand" onclick="viewDetails('<bean:write name="req" property="refNo"/>', 'viewKOReqMedLabSpecial.do');"><bean:write name="req" property="receivedDate"/></td>
                                              <td style="cursor:hand" onclick="viewDetails('<bean:write name="req" property="refNo"/>', 'viewKOReqMedLabSpecial.do');"><bean:write name="req" property="location"/></td>	
                                            </tr>
                                            <%i++;%>
                                            </logic:iterate>
                                            <%if (i==0){%>
                                            	<tr class="row1"><td class="label2" colspan="11" align="center">There are no existing pending requests.</td></tr>
                                            <%}%>

                                            <!-- kristian ORIGINAL CODE
                                            <tr>
                                              <td class="headerrow4" colspan="11" align="left" width="100%">
                                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                  <tr height="1"><td></td></tr>
                                                  <tr>
                                                    <td class="headerrow4" width="100%" height="10" class="label2" valign="bottom" >
                                                      <a href="#" onmouseover="imageOver('pagnavfirst')" onmouseout="imageOut('pagnavfirst')">
                                                      <img src="images/pagnavfirst_off.gif" name="pagnavfirst" border="0" width="18" height="13"/></a>
                                                      <a href="#" onmouseover="imageOver('pagnavleft')" onmouseout="imageOut('pagnavleft')">
                                                      <img src="images/pagnavleft_off.gif" name="pagnavleft" border="0" width="18" height="13"/></a>
                                                      1,2,3,4,5,7,8,9,10...20...30...40
                                                      <a href="#" onmouseover="imageOver('pagnavright')" onmouseout="imageOut('pagnavright')">
                                                      <img src="images/pagnavright_off.gif" name="pagnavright" border="0" width="18" height="13"/></a>
                                                      <a href="#" onmouseover="imageOver('pagnavlast')" onmouseout="imageOut('pagnavlast')">
                                                      <img src="images/pagnavlast_off.gif" name="pagnavlast" border="0" width="18" height="13"/></a>&nbsp;&nbsp; 
                                                    </td>
                                                  </tr>
                                                  <tr><td></td></tr>
                                                </table>
                                              </td>
                                            </tr>
                                            kristian ORIGINAL CODE -->
                                            <!--------------  kristian TESTING --------->
                                            <!-- page navigation -->
											<input type="hidden" name="pageNo" value="<%=reqFilter.getPageNo()%>">
											<%
		          								int pageNumber = reqFilter.getPageNo();
		         							%>
                                           
                                            <% if (displayPage == null) { %>
                                            <tr>
                                              <td class="headerrow4" colspan="11" align="left" width="100%"></td>
                                            </tr>
                                            <% } else { %>
                                            <tr>
                                              <td class="headerrow4" colspan="11" width="100%" height="10" class="label2" valign="bottom" >
                                                <!-- don't display link for previous page if the current page is the first page -->					
                                                <% if (pageNumber > 1) { %>
                                                  <a href="#" onclick="javascript:rePaginate(1,'listAssessmentRequests.do');"><img src="images/pagnavfirst_off.gif" name="pagnavfirst" border="0" width="18" height="13"/></a>&nbsp;					
                                                  <a href="#" onclick="javascript:rePaginate(<%=pageNumber-1%>,'listAssessmentRequests.do');"><img src="images/pagnavleft_off.gif" name="pagnavleft" border="0" width="18" height="13"/></a>
                                                <% } else {%>
                                                  <img src="images/pagnavfirst_on.gif" name="pagnavfirst" border="0" width="18" height="13"/>&nbsp;
                                                  <img src="images/pagnavleft_on.gif" name="pagnavleft" border="0" width="18" height="13"/>
                                                <% } %>
                                                <% 
                                                int currLink = 1;
                                                int prevLink = 1;
                                                %>
                                                <logic:iterate id="navLinks" name="displayPage" property="pageNumbers">
                                                  <% currLink = ((Integer)navLinks).intValue(); %>
                                                  <% if (((Integer)navLinks).intValue() == pageNumber) { %>
                                                    <b><bean:write name="navLinks"/></b>
                                                  <% } else { %>		
                                                    <% if ((currLink > (prevLink+1))) { %>
                                                      ...
                                                    <% } %>
                                                    <a href="#" onclick="rePaginate('<bean:write name="navLinks"/>','listAssessmentRequests.do');" class="links2"><bean:write name="navLinks"/></a>
                                                  <% } %>
                                                  <% prevLink = currLink; %>
                                                </logic:iterate>
									
                                                <!-- don't display link for next page if the current page is the last page -->
                                                
                                                   <%  if(pageNumber < prevLink) {  %>
                                                  <a href="#" onClick="rePaginate('<%=pageNumber+1%>','listAssessmentRequests.do');" class=links1><img src="<%=contextPath%>/images/pagnavright_off.gif" name="pagnavright" border="0" width="18" height="13"/></a>&nbsp;
                                                  <a href="#" onClick="rePaginate('<%=prevLink%>','listAssessmentRequests.do');" class=links1><img src="<%=contextPath%>/images/pagnavlast_off.gif" width="18" height="13" border="0"></a>
                                                <%  } else {  %>
                                                  <img src="<%=contextPath%>/images/pagnavright_on.gif" width="18" height="13" border="0">&nbsp;
                                                  <img src="<%=contextPath%>/images/pagnavlast_on.gif" width="18" height="13" border="0">
                                                <%  }  %>

                                              </td>
                                            </tr>
                                            <% } %>
                                            <!------------- kristian TESTING -------------->                                            
                                          </table>
                                        </td>
                                      </tr>
                                    </table>
                                  </td>
                                </tr>  
                              </table>
                            </td>
                          </tr>
                        </table>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr>
                <td colspan="10" class="label1">
                  <br><br>
                  <font size="1">� 2003 Sun Life Financial. All rights reserved.</font>
                  <br><br>
                </td>
              </tr>
            </table>
            <!-- BODY -->
          </td>  
        </tr>
      </table>
    </form>
  </body>

</html>
</html:html>

