<%@ taglib uri="/struts-html.tld" prefix="html" %>
<%@ taglib uri="/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/ium.tld" prefix="ium" %>
<%@ page language="java" import="java.util.*,com.slocpi.ium.data.*, com.slocpi.ium.ui.form.*, com.slocpi.ium.util.*, com.slocpi.ium.ui.util.*, java.text.* " buffer="12kb"%>
<jsp:useBean id="userProfileListForm" scope="request" class="com.slocpi.ium.ui.form.UserProfileListForm"/>
<jsp:useBean id="displayPage" scope="request" class="com.slocpi.ium.ui.util.Page"/>
<%
/* Andre Ceasar Dacanay - css enhancement - start */
String iumCss = "";
String iumColorB = "";
String iumColorO = "";
String companyCode = "";
try {
	ResourceBundle rbWMSSP = ResourceBundle.getBundle("dbconfig");
	iumCss = rbWMSSP.getString("IUM_CSS_VERSION");
	companyCode = iumCss;
	iumCss = "_"+iumCss;
} catch (Exception e){}
if(iumCss.equals("_CP")){
	iumColorB="#2A4C7C";
	iumColorO = "#FFCB00";
	companyCode="";
}else if(iumCss.equals("_GF")){
	iumColorB="#303A77";
	iumColorO = "#303A77";
	companyCode = companyCode+" ";
}else{
	iumColorB="#2A4C7C";
	iumColorO = "#FFCB00";
}
/* Andre Ceasar Dacanay - css enhancement - end */
%>
<% String contextPath = request.getContextPath();
StateHandler sessionHandler = new StateHandler();
Access access = new Access();
UserData userData = sessionHandler.getUserData(request); 
HashMap userAccess = sessionHandler.getUserAccess(request);

    if(!access.hasAccess(userAccess,IUMConstants.PAGE_USER_ACCESS,IUMConstants.ACC_INQUIRE) ){                                                                                                                    
    	response.sendRedirect("noAccessPage.jsp");   
    } 
    
    String CREATE_ACCESS = "";
    String MAINTAIN_ACCESS = "";
    
    if(!access.hasAccess(userAccess,IUMConstants.PAGE_USER_ACCESS,IUMConstants.ACC_CREATE)){
	  CREATE_ACCESS = "DISABLED"; 	
    }
    if(!access.hasAccess(userAccess,IUMConstants.PAGE_USER_ACCESS,IUMConstants.ACC_MAINTAIN) ){                                                                                                                     
	  MAINTAIN_ACCESS = "DISABLED";
    }
UserProfileData userPref = userData.getProfile();
%>
<html>

<head>
<title><%=companyCode%>Integrated Underwriting and Medical System</title>
<link href="<%=contextPath%>/css/pw_main.css"     rel="stylesheet" type="text/css">
<link href="<%=contextPath%>/css/main<%=iumCss%>.css" rel="stylesheet" type="text/css">
<link href="<%=contextPath%>/css/styleCommon.css" rel="stylesheet" type="text/css">
<script language="JavaScript" src="<%=contextPath%>/js/dhtml_init.js"></script>
<script language="JavaScript" src="<%=contextPath%>/js/navigation.js"></script>
<script language="JavaScript" src="<%=contextPath%>/js/validation.js"></script>
<script language="JavaScript" src="<%=contextPath%>/jsp/sl_left_nav.jsp"></script>
<script language="JavaScript">
	
	function putButton(numRecordsPerView, numRecords){
  		if(numRecords >= 10 && numRecordsPerView >= 10){
  	        document.all["create_btn"].style.display = "block";
  	    }
  	    else{
  	       document.all["create_btn"].style.display = "none";
  	    }
  	}
	function rePaginate (page, actionUrl)
	{
		form = document.forms[0];
		if(form.pageNo != null)
		{
			form.pageNo.value = page;	
		}
		submitForm(actionUrl);
	}
	
	function validateSearch(actionUrl)
	{
		var form = document.forms[0];
		if(form.userIdFilter.value == "")
		{
			if(form.lastNameFilter.value == "")
			{
				if(form.acf2IdFilter.value == "")
				{
					if(form.branchNameFilter.value == "")
					{
						if(form.departmentFilter.value == "")
						{
							var con = confirm('<bean:message key="message.nofilter"/>');
							if(!con)
							{
								return false;
							}
						}
					}
				}
			}
		}
		submitForm(actionUrl);
	}
	
	function submitForm(actionUrl)
	{	
		document.forms[0].action = actionUrl;
		document.forms[0].submit();
	}    


function clickMaintain(){
	 var frm = document.frm;
	 var value = checkHasReqPick();
	 if(value!=''){
			frm.action='viewUserAdminDetail.do'+'?userId='+value+'&view=maintain';
			frm.submit();	
	 }else{
		   alert('<bean:message key="error.field.requiredselection" arg0="User"/>');
	 }
}
	
function checkHasReqPick(){
   var pick = document.frm.userPick;
	 var result='';
	 if(pick.value==null){
		 for(i=0;i<pick.length;i++){
		   if(pick[i].checked==true){
			   result=pick[i].value;
			 }
		 }
	 }else{
	    if(pick.checked==true){
			   result=pick.value;											 			
			}
	 }
	 return result;
}
</script>
</head>

<BODY leftMargin=0 topmargin=0 marginwidth=0 marginheight=0 onKeyUp="checkEnter(event,'search');" onLoad = "putButton(<%=userPref.getRecordsPerView()%>,<%=((ArrayList)userProfileListForm.getUserProfileData()).size()%>);" >
<form name="frm" method="post">
<table border="0" cellpadding="0" cellspacing="0" height="100%" width="1100">
<tr><td colspan="2">
<jsp:include page="header.jsp" flush="true"/>
</td>
</tr>
<tr>
  <td rowspan="2" bgcolor="<%=iumColorO%>" height="100%" width="20"><img src="<%=contextPath%>/images/spacer.gif" width="20"></td>
            <td background="<%=contextPath%>/images/GradientGray.jpg" height="27" valign="top" width="100%">&nbsp;&nbsp;
              <span class="main" width="100%">Integrated Underwriting and Medical System</span>
            </td>
          </tr>
          <tr>
            <td height="100%" valign="top">
              <!-- BODY -->
        <table width="1100" cellpadding="3" cellspacing="5" border="0">
          <tr valign="top"> 
            <td> 


<!--- START OF BODY -->

              <table cellpadding="0" cellspacing="0" border="0" bordercolor="<%=iumColorB%>" width="1100">
                <tr>
                  <td>
                    <table cellpadding="10" cellspacing="0" border="0" width="1100">
                      <tr>
                        <td>
                          <table border="0" cellpadding="0" cellspacing="0" width="1100">
                            <tr><td class="title2"><b>User Profiles</b></td></tr>
                            <tr><td>&nbsp;</td></tr>
                            <tr>
                              <td>
                                <table border="0" bordercolor="<%=iumColorB%>" cellpadding="0" cellspacing="0" bgcolor="" width="1000">
                                  <tr><td></td></tr>
                                  <tr>
                                    <td>
                                      <table border="1" bordercolor="<%=iumColorB%>" cellpadding="10" cellspacing="0" bgcolor="" width="1000" class="listtable1">
                                        <tr>
                                          <td>
                                            <table border="0" cellpadding="2" cellspacing="0">
                                              <tr>
                                                <td class="label2" width="120"><b>User ID</b></td>
                                                <td class="label2" width="200"><input type="textbox" class="label2" name="userIdFilter" value='<bean:write name="userProfileListForm" property="userIdFilter"/>' maxLength="10"></td>
                                                <td class="label2" width="10">&nbsp;</td>
                                                <td class="label2" width="200"><b>Last Name</b></td>
                                                <td class="label2" width="240"><input type="textbox" class="label2" name="lastNameFilter" value='<bean:write name="userProfileListForm" property="lastNameFilter"/>' maxLength="40"></td>
                                                <td class="label2" colspan="3">&nbsp;</td>                                                
                                              </tr>
                                              <tr>
                                                <td class="label2" width="120"><b>ACF2 ID</b></td>
                                                <td class="label2" width="200"><input type="textbox" class="label2" name="acf2IdFilter" value='<bean:write name="userProfileListForm" property="acf2IdFilter"/>' maxLength="8"></td>
                                                <td class="label2" width="10">&nbsp;</td>
                                                <td class="label2" width="240"><b>Sun Life Office / Branch Code / Description</b></td>
                                                <td class="label2" colspan="4"><ium:list className="label2" listBoxName="branchNameFilter" type="<%=IUMConstants.LIST_BRANCHES%>" selectedItem="<%=userProfileListForm.getBranchNameFilter()%>"/></td>
                                              </tr>                                              
                                              <tr>
                                                <td class="label2" width="120"><b>Department</b></td>
                                                <td class="label2" width="240"><ium:list className="label2" listBoxName="departmentFilter" type="<%=IUMConstants.LIST_DEPARTMENT%>" styleName="width:250" selectedItem="<%=userProfileListForm.getDepartmentFilter()%>"/></td>
                                                <td class="label2" colspan="6">&nbsp;</td>
                                              </tr>
                                              <tr>
                                                <td colspan="9" align="left"><input type="button" name="search" class="button1" value="Search" onClick="validateSearch('<%=contextPath%>/listUserProfile.do');"></td>
                                              </tr>                                              
                                            </table>
                                          </td>
                                        </tr>
                                      </table>
                                    </td>
                                  </tr>
                                  <tr><td></td></tr>
                                  <tr><td></td></tr>
                                  <tr><td>&nbsp;</td></tr>
                                  <tr>
                                    <td colspan="2" height="100%">
<table>
 <tr>
	
		<td>
			<div align="left">
 			 <input type="button" class="button1" value="Create" onclick="gotoPage('frm','<%=contextPath%>/jsp/userAdminDetail.jsp');" <%=CREATE_ACCESS%> >&nbsp;&nbsp;
 			 <input type="button" class="button1" value="Maintain" onclick="clickMaintain();" <%=MAINTAIN_ACCESS%> >
			</div>
		</td>
	
</tr>
</table>

<table border="1" bordercolor="<%=iumColorB%>" cellpadding=10 cellspacing=0 bgcolor="" width="1000" class="listtable1">
        <tr><td colspan="2" height="100%">
        <table border="0" cellpadding="2" cellspacing="1" bgcolor=#FFFFFF style="border: 1px solid #D1D1D1;" class="listtable1" width="1000">
        <tr class="headerrow1">
		<td width="10">&nbsp;</td>	 			
        <td width="90">USER ID</td>
        <td width="90">ACF2 ID</td>        
        <td width="300">NAME</td>
        <td width="200">EMAIL</td>        
        <td width="80">CONTACT NO</td>                
        <td width="80">SUN LIFE OFFICE/BRANCH</td>
        <td width="80">DEPARTMENT</td>
        <td width="80">SECTION</td>
        </tr>
       <%int row=0; %>
      <logic:empty name="displayPage" property="list">
      <tr class="row1">                                              
      <td class="label2" colspan="9" align="center">
      <bean:message key="message.noexisting" arg0="user profiles"/>
      </td>
     </tr>
     </logic:empty>
     <logic:notEmpty name="displayPage" property="list">       
	  <%--<logic:iterate id="user" name="userProfileForm" property="userProfileData">--%>
      <logic:iterate id="user" name="displayPage" property="list">	  
	  <% if (row%2 == 0) { %>
        <tr class="row1">
      <% } else { %>
        <tr class="row2">
      <% } %>	  
		  <td><input type="radio" name="userPick" value='<bean:write name="user" property="userId"/>'></td>
		  <td style="cursor:hand" onclick="gotoPage('frm','<%=contextPath%>/viewUserAdminDetail.do?userId=<bean:write name="user" property="userId"/>');"><bean:write name="user" property="userId"/>&nbsp;</td>
		  <td style="cursor:hand" onclick="gotoPage('frm','<%=contextPath%>/viewUserAdminDetail.do?userId=<bean:write name="user" property="userId"/>');"><bean:write name="user" property="ACF2ID"/>&nbsp;</td>		  
		  <td style="cursor:hand" onclick="gotoPage('frm','<%=contextPath%>/viewUserAdminDetail.do?userId=<bean:write name="user" property="userId"/>');"><bean:write name="user" property="lastName"/>, <bean:write name="user" property="firstName"/> <bean:write name="user" property="middleName"/>&nbsp;</td>
		  <td style="cursor:hand" onclick="gotoPage('frm','<%=contextPath%>/viewUserAdminDetail.do?userId=<bean:write name="user" property="userId"/>');"><bean:write name="user" property="emailAddr"/>&nbsp;</td>		  
		  <td style="cursor:hand" onclick="gotoPage('frm','<%=contextPath%>/viewUserAdminDetail.do?userId=<bean:write name="user" property="userId"/>');"><bean:write name="user" property="mobileNumber"/>&nbsp;</td>		  
		  <td style="cursor:hand" onclick="gotoPage('frm','<%=contextPath%>/viewUserAdminDetail.do?userId=<bean:write name="user" property="userId"/>');"><bean:write name="user" property="officeCode"/>&nbsp;</td>
		  <td style="cursor:hand" onclick="gotoPage('frm','<%=contextPath%>/viewUserAdminDetail.do?userId=<bean:write name="user" property="userId"/>');"><bean:write name="user" property="deptCode"/>&nbsp;</td>
		  <td style="cursor:hand" onclick="gotoPage('frm','<%=contextPath%>/viewUserAdminDetail.do?userId=<bean:write name="user" property="userId"/>');"><bean:write name="user" property="sectionCode"/>&nbsp;</td>
		</tr>
		<%row++;%>
       </logic:iterate>
      </logic:notEmpty>
                                           <!-- page navigation -->
											<input type="hidden" name="pageNo" value="<%=userProfileListForm.getPageNo()%>">
											<%
		          								int pageNumber = userProfileListForm.getPageNo();
		         							%>
                                           
                                           <% if (displayPage == null) { %>
												<tr>
												  <td class="headerrow4" colspan="8" align="left"></td>
												</tr>
												<% } else {  %>
													<tr>
														<td class="headerrow4" colspan="9" height="10" class="label2" valign="bottom" >
															<!-- don't display link for previous page if the current page is the first page -->					
													        <% if (pageNumber > 1) { %>
																<a href="#" onclick="javascript:rePaginate(1,'<%=contextPath%>/listUserProfile.do');"><img src="images/pagnavfirst_off.gif" name="pagnavfirst" border="0" width="18" height="13"/></a>&nbsp;					
																<a href="#" onclick="javascript:rePaginate(<%=pageNumber-1%>,'<%=contextPath%>/listUserProfile.do');"><img src="images/pagnavleft_off.gif" name="pagnavleft" border="0" width="18" height="13"/></a>
												  			<% } else {%>
																<img src="images/pagnavfirst_on.gif" name="pagnavfirst" border="0" width="18" height="13"/>&nbsp;
																<img src="images/pagnavleft_on.gif" name="pagnavleft" border="0" width="18" height="13"/>
															 <%}%>
												             <% 
									          				  int currLink = 1;
													          int prevLink = 1;
									  			             %>
    								  			             <logic:iterate id="navLinks" name="displayPage" property="pageNumbers">
												            	<%currLink = ((Integer)navLinks).intValue();%>				
												            	<%if (((Integer)navLinks).intValue() == pageNumber){%>
									            					<b><bean:write name="navLinks"/></b>
											        	    	<%}else{%>		
									           						<%if ((currLink > (prevLink+1))) {%>
													        	    	...
												               		<%}%>
												               			<a href="#" onclick="rePaginate('<bean:write name="navLinks"/>','<%=contextPath%>/listUserProfile.do');" class="links2"><bean:write name="navLinks"/></a>
													           	<%}%>
									            	       	   	<%prevLink = currLink;%>
											               	</logic:iterate>
									
											        		<!-- don't display link for next page if the current page is the last page -->
									         		 		<%if(pageNumber < prevLink){%>
											         			<a href="#" onclick="rePaginate('<%=pageNumber+1%>','<%=contextPath%>/listUserProfile.do');"><img src="images/pagnavright_off.gif" name="pagnavright" border="0" width="18" height="13"/></a>&nbsp;
									          					<a href="#" onclick="rePaginate('<%=prevLink%>','<%=contextPath%>/listUserProfile.do');"><img src="images/pagnavlast_off.gif" width="18" height="13" border="0"></a>
									          				<%} else { %>
									          					<img src="images/pagnavright_on.gif" width="18" height="13" border="0">&nbsp;
									          					<img src="images/pagnavlast_on.gif" width="18" height="13" border="0">
										          			<%}%>        		
													   </td> 
												</tr>
												<% } %> 
            

</table>

<!--- END OF BODY -->
</td>
</tr>
</table>

</td>
</tr>
</table>
</td>
</tr>
<tr>
	<table>
	<tr>
		<td>
			<div align="left" id="create_btn" >
 			 <input type="button" class="button1" value="Create" onclick="gotoPage('frm','<%=contextPath%>/jsp/userAdminDetail.jsp');" <%=CREATE_ACCESS%>>&nbsp;&nbsp;
 			 <input type="button" class="button1" value="Maintain" onclick="clickMaintain();" <%=MAINTAIN_ACCESS%> >
			</div>
		</td>
	</tr>
	</table>
</tr>

</table>
</td>
</tr>
<tr>
     <td colspan="10" class="label1">
     <br><br>
        <font size="1">� 2003 Sun Life Financial. All rights reserved.</font>
     <br><br>
     </td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>

</table>
</form>
</body>
</html>