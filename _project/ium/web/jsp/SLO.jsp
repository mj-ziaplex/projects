<%@ page language="java" import="java.util.*, com.slocpi.ium.ui.form.*, com.slocpi.ium.util.*, com.slocpi.ium.ui.util.*, com.slocpi.ium.data.*" buffer="1024kb"%>
<%@ taglib uri="/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/struts-html.tld" prefix="html" %>
<%@ taglib uri="/ium.tld" prefix="ium" %>
<%
/* Andre Ceasar Dacanay - css enhancement - start */
String iumCss = "";
String iumColorB = "";
String companyCode = "";
try {
	ResourceBundle rbWMSSP = ResourceBundle.getBundle("dbconfig");
	iumCss = rbWMSSP.getString("IUM_CSS_VERSION");
	companyCode = iumCss;
	iumCss = "_"+iumCss;
} catch (Exception e){}
if(iumCss.equals("_CP")){
	iumColorB="#2A4C7C";
	companyCode="";
}else if(iumCss.equals("_GF")){
	iumColorB="#303A77";
	companyCode = companyCode+" ";
}else{
	iumColorB="#2A4C7C";
}
/* Andre Ceasar Dacanay - css enhancement - end */
%>
<%SLOForm sloform = (SLOForm)request.getAttribute("sloform"); 

%>
<%String contextPath = request.getContextPath();%>



<%
	StateHandler sessionHandler = new StateHandler();
    Access access = new Access();
    
    // This routine queries the access rights of the users (RCRIVERA)
    //
    // --- START --- 
    
	HashMap userAccess = sessionHandler.getUserAccess(request);
    if(!access.hasAccess(userAccess,IUMConstants.PAGE_SUNLIFE_OFFICES,IUMConstants.ACC_INQUIRE) ){                                                                                                                    
    	response.sendRedirect("noAccessPage.jsp");   
    } 
    
    String CREATE_ACCESS = "";
    String MAINTAIN_ACCESS = "";
        
    if(!access.hasAccess(userAccess,IUMConstants.PAGE_SUNLIFE_OFFICES,IUMConstants.ACC_CREATE)  ){                                                                                                                     
	  CREATE_ACCESS = "DISABLED"; 	
    }
    if(!access.hasAccess(userAccess,IUMConstants.PAGE_SUNLIFE_OFFICES,IUMConstants.ACC_MAINTAIN) ){                                                                                                                     
	  MAINTAIN_ACCESS = "DISABLED";
    }
   
    
    // --- END ---
 %>    
<%
UserData userData = sessionHandler.getUserData(request); 
UserProfileData userPref = userData.getProfile();
%>
<html>
<head>
<title><%=companyCode%>Integrated Underwriting and Medical System</title>
<link href="<%=contextPath%>/css/pw_main.css"     rel="stylesheet" type="text/css">
<link href="<%=contextPath%>/css/main<%=iumCss%>.css" rel="stylesheet" type="text/css">
<link href="<%=contextPath%>/css/styleCommon.css" rel="stylesheet" type="text/css">
<script language="JavaScript" src="<%=contextPath%>/js/validation.js"></script>
<script language="JavaScript" src="<%=contextPath%>/js/dhtml_init.js"></script>
<script language="JavaScript" src="<%=contextPath%>/js/navigation.js"></script>
<script language="JavaScript" src="<%=contextPath%>/jsp/sl_left_nav.jsp"></script>
<script language="JavaScript">

function putButton(numRecordsPerView, numRecords){
  		if(numRecords >= 10 && numRecordsPerView >= 10){
  	        document.all["create_btn"].style.display = "block";
  	    }
  	    else{
  	       document.all["create_btn"].style.display = "none";
  	    }
  	}

function clickSave(){
   var frm = document.frm; 
   if(validateFormEntry()==true){
				
	 <%if(sloform==null||sloform.getOfficeId()==null||sloform.getOfficeId().equals("")){%>
	      frm.action="maintainOffices.do?mode=add";
				frm.submit();
	 <%}else{%>
	      frm.action="maintainOffices.do";
				frm.officeId.disabled=false;
				frm.submit();
	 <%}%>
	 }
}

function validateFormEntry(){
   var result=true;
	 var frm = document.frm;
	 if(isEmpty(frm.officeId.value)){
	    alert('<bean:message key="error.field.required" arg0="Sun Life Office Id"/>');
			frm.officeId.focus();
			return false;																
	 }
	 if(isEmpty(frm.officeName.value)){
	    alert('<bean:message key="error.field.required" arg0="Office/Branch Name"/>');
			frm.officeName.focus();
			return false;																
	 }
/*	 if(isEmpty(frm.addr1.value)){
	    alert('<bean:message key="error.field.required" arg0="Address Line 1"/>');
			frm.addr1.focus();
			return false;																
	 }
	 if(isEmpty(frm.addr2.value)){
	    alert('<bean:message key="error.field.required" arg0="Address Line 2"/>');
			frm.addr2.focus();
			return false;																
	 }	 
	 if(isEmpty(frm.addr3.value)){
	    alert('<bean:message key="error.field.required" arg0="Address Line 3"/>');
			frm.addr3.focus();
			return false;																
	 }
	 if(isEmpty(frm.city.value)){
	    alert('<bean:message key="error.field.required" arg0="City"/>');
			frm.city.focus();
			return false;																
	 }
	 //Province not required
	 /*if(isEmpty(frm.province.value)){
	    alert('<bean:message key="error.field.required" arg0="Province"/>');
			frm.province.focus();
			return false;																
	 }

	 if(isEmpty(frm.country.value)){
	    alert('<bean:message key="error.field.required" arg0="Country"/>');
			frm.country.focus();
			return false;																
	 }
	 if(isEmpty(frm.zipCode.value)){
	    alert('<bean:message key="error.field.required" arg0="Zip Code"/>');
			frm.zipCode.focus();
			return false;																
	 }else{
	 	  if(!isNumeric(frm.zipCode.value)){
  	    alert('<bean:message key="error.field.numeric" arg0="Zip Code"/>');
  			frm.zipCode.focus();
				frm.zipCode.select();
  			return false;																
	 			}		 
	 }//end if
*/	 
	 return result;
}

function rePaginate( pageNum, target){
    var frm = document.frm;
		frm.pageNumber.value = pageNum;
		//clickCreate();
		frm.action=target;
		frm.submit();
}


function clickCreate(){
	 enableFields();
	 var frm = document.frm;
	 frm.officeId.value='';
	 frm.officeName.value='';
	 frm.officeType[2].selected = true;
	 frm.addr1.value='';
	 frm.addr2.value='';
	 frm.addr3.value='';
	 frm.city.value='';
	 frm.province.value='';
 	 frm.country.value='';
 	 frm.zipCode.value='';
	 frm.contactNumber.value='';
	 frm.faxNumber.value='';
	 frm.officeId.focus();
	 
}

function clickMaintain(){
	 var frm = document.frm;
	 var value = checkHasReqPick();
	 if(value!=''){
	    enableFields();
			frm.officeId.value = value;
			frm.action='viewOffices.do';
			frm.submit();	
	 }else{
	    alert('<bean:message key="error.field.requiredselection" arg0="branch/office"/>');
	 }
}

function checkHasReqPick(){
   var pick = document.frm.sloPick;
	 var result='';
	 if(pick.value==null){
		 for(i=0;i<pick.length;i++){
		   if(pick[i].checked==true){
			   result=pick[i].value;
			 }
		 }
	 }else{
	    if(pick.checked==true){
			   result=pick.value;											 			
			}
	 }
	 return result;
}


function disableFields(){
   var frm = document.frm;
   	 frm.officeId.value='';
     frm.officeName.value='';
	 frm.officeType.value='';
	 frm.addr1.value='';
	 frm.addr2.value='';
	 frm.addr3.value='';
	 frm.city.value='';
	 frm.province.value='';
	 frm.country.value='';
 	 frm.zipCode.value='';
	 frm.contactNumber.value='';
	 frm.faxNumber.value='';
   	 
	 frm.officeId.disabled=true;
	 frm.officeName.disabled=true;
	 frm.officeType.disabled=true;
	 frm.addr1.disabled=true;
	 frm.addr2.disabled=true;
	 frm.addr3.disabled=true;
	 frm.city.disabled=true;
	 frm.province.disabled=true;
	 frm.country.disabled=true;
 	 frm.zipCode.disabled=true;
	 frm.contactNumber.disabled=true;
	 frm.faxNumber.disabled=true;
 	 document.all['save_cancel'].className = 'hide';
   	 document.all['create_cancel'].className = 'show';
  	 document.all['create_cancel2'].className = 'show';
}


function enableFields(){
   var frm = document.frm;
	 frm.officeId.disabled=false;
	 frm.officeName.disabled=false;
	 frm.officeType.disabled=false;
	 frm.addr1.disabled=false;
	 frm.addr2.disabled=false;
	 frm.addr3.disabled=false;
	 frm.city.disabled=false;
	 frm.province.disabled=false;
	 frm.country.disabled=false;
 	 frm.zipCode.disabled=false;
	 frm.contactNumber.disabled=false;
	 frm.faxNumber.disabled=false;
	 document.all['create_cancel'].className = 'hide';
 	 document.all['create_cancel2'].className = 'hide';
	 document.all['save_cancel'].className = 'show'; 	 				 				 
}

function onLoadPage(){
	 <%if(sloform==null||sloform.getOfficeId()==null||sloform.getOfficeId().equals("")){%>
	 		  disableFields();																																																				     	 																																																						
	 <%}else{%>
	 			enableFields();
				selectDefaultType();
				setSelectedRadioButton();
				document.frm.officeName.focus();
				document.frm.officeId.disabled=true;								 
	 <%}%>
}

function selectDefaultType(){
   var frm = document.frm;
	 var selectedType = '<%=sloform.getOfficeType()%>';
	 for(i=0; i<frm.officeType.length;i++){
	     if(frm.officeType[i].value == selectedType){
			     frm.officeType[i].selected = true; 				
			 } 
	 }
}

function setSelectedRadioButton(){
	 var frm = document.frm;
   var pick = '<%=sloform.getOfficeId()%>';
	 var pickradio = document.frm.sloPick;
	 if(pickradio.value==null){
		 for(i=0;i<pickradio.length;i++){
		   if(pickradio[i].value==pick){
			     pickradio[i].checked=true;															
			 }
		 }
	 }else{
	    if(pick.value==pick){
			   frm.checked=true;   											 			
			}
	 }



}
</script>
</head>

<BODY leftMargin=0 topmargin=0 marginwidth=0 marginheight=0 onLoad="onLoadPage(); putButton(<%=userPref.getRecordsPerView()%>, <%=((Page)request.getAttribute("pageRecord")).getList().size()%>);">
<form name="frm"  method="post">    
<table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%">
<tr><td width="100%" colspan="2">
  <!-- HEADER -->
  <jsp:include page="header.jsp" flush="true"/>
  	<table border="0" cellpadding="0" cellspacing="0" width="100%" height="100%">
  			<tr vAlign=top> 
   				<TD background="<%=contextPath%>/images/bg_yellow.gif" id=leftnav vAlign=top width=147> 
     				<IMG height=9 src="<%=contextPath%>/images/sp.gif" width=147><BR>
     				<IMG height=30 src="<%=contextPath%>/images/sp.gif" width=7> <BR>
			      <IMG height=7 src="<%=contextPath%>/images/sp.gif" width=147><BR>
			      <DIV id=level0 style="POSITION: absolute; VISIBILITY: hidden; WIDTH: 100%; Z-INDEX: 500"> 
			       	<SCRIPT language=javascript>
								<!--
								document.write(writeMenu(''))
								if (IE4) { document.write(writeDiv()) };								
								//-->
							</SCRIPT>
				    </DIV>
      			<DIV id=NSFix style="POSITION: absolute; VISIBILITY: hidden; Z-INDEX: 999">
      				&nbsp;
      			</DIV>
      			<!-- Netscape needs a hard-coded DIV to write dynamic DIVs --> 
      			<SCRIPT language=javascript>
             		<!--
               	 	if (NS4) { document.write(writeDiv()) }
                 //-->
             </SCRIPT>
             <SCRIPT>
              <!--
                   	initMenu();
           		//-->
           	</SCRIPT>
      <!-- END MENU CODE --> 
 <input type="hidden" name="pageNumber" value='<bean:write name="sloform" property="pageNumber"/>'>
 
 
    </td>      											
			    <td width="100%"> 
			     	<div align="left"> 
	    				<table border="0" cellpadding="0" width="100%" cellspacing="0">
      					<tr> 
       						<td width="100%"> 
							      <div align="left"> 
							        <table border="0" cellpadding="3" cellspacing="0" width="100%" height="27"  background="<%=contextPath%>/images/sectionbkgray.jpg">
							          <tr> 
							            <td valign="top"> 
							               <span class="main">&nbsp;Integrated Underwriting and Medical System </span>
							            </td>
							          </tr>
							        </table>
							      </div>
						       </td>
						     </tr>
				         <tr> 
				           <td width="100%" colspan=2>&nbsp;</td>
	      				</tr>
			 	     	</table>				 	       	

<table width="100%" cellpadding="3" cellspacing="5" border="0">
	<tr valign="top">
    	<td class="title2">Sun Life Offices
         						<!--Start of form -->
			<table border="1" bordercolor="<%=iumColorB%>" cellpadding=5 cellspacing=0 bgcolor="" width="700" class="listtable1">
				<tr>
					<td colspan="2" height="100%">

						<table border="0" width="690" cellpadding="2" cellspacing="5" >
						<tr>
<html:errors/>
				          <td class="label2"><b>Sun Life Office ID</b></td>
				   	      <td class="label2" width="200"><input type="text" name="officeId" class="label2" maxlength="15" onBlur="this.value=trim(this.value);"  value='<bean:write name="sloform" property="officeId"/>'></td>
					      <td class="label2" >&nbsp;</td>
					      <td class="label2" width="110"><b>Office Type</b></td>
					      <td><select class="label2" name="officeType" sytle="width:100">
							<option value='M'>Main Office</option>
							<option value='S'>Site Office</option>
							<option value='B'>Branch Office</option>
							</select>
						  </td>
				        </tr>
				        <tr valign="top">
				          <td class="label2"><b>Office/Branch Name</b></td>
				          <td class="label2" colspan="4"><input type="text" name="officeName" class="label2" onBlur="this.value=trim(this.value);" value='<bean:write name="sloform" property="officeName"/>' size="52" maxlength="50"></td>
					    </tr>

				        <tr>
				          <td class="label2"><b>Address Line 1</b></td>
				          <td class="label2" colspan="4"><input type="text" name="addr1" class="label2" value='<bean:write name="sloform" property="addr1"/>' size="60" maxlength="60"></td>
				        </tr>
				        <tr>
				          <td class="label2"><b>Address Line 2</b></td>
					      <td class="label2" colspan="4"><input type="text" name="addr2" class="label2" value='<bean:write name="sloform" property="addr2"/>' size="60"  maxlength="60"></td>
				        </tr>
				        <tr>
				          <td class="label2"><b>Address Line 3</b></td>
				          <td class="label2" colspan="4"><input type="text" name="addr3" class="label2" value='<bean:write name="sloform" property="addr3"/>' size="60"  maxlength="60"></td>
				        </tr>

				        <tr>
					      <td class="label2"><b>City</b></td>
				          <td class="label2"><input type="text" name="city" class="label2" value='<bean:write name="sloform" property="city"/>' maxlength="25"></td>
			     	      <td> &nbsp;</td>
				          <td class="label2"><b>Province</b></td>
				          <td class="label2"><input type="text" name="province" class="label2" value='<bean:write name="sloform" property="province"/>' maxlength="25"></td>
				        </tr>
				        <tr>
				          <td class="label2"><b>Country</b></td>
				          <td class="label2"><input type="text" name="country" class="label2" value='<bean:write name="sloform" property="country"/>' maxlength="25"></td>
		      		      <td> &nbsp;</td>
				          <td class="label2"><b>Zip Code</b></td>
				          <td class="label2"><input type="text" name="zipCode" class="label2" value='<bean:write name="sloform" property="zipCode" />' onKeyUp="getKeyNum(event, this)" maxlength="6"></td>
				        </tr>
				
				        <tr>
				          <td class="label2" colspan="5"><b>Contact Numbers</b></td>
						</tr>				
				        <tr>
				          <td class="label2"><b>Office</b></td>
				          <td class="label2"><input type="text" name="contactNumber" class="label2" value='<bean:write name="sloform" property="contactNumber"/>' maxlength="13"></td>
			              <td> &nbsp;</td>
				          <td class="label2"><b>Fax</b></td>
				          <td class="label2"><input type="text" name="faxNumber" class="label2" value='<bean:write name="sloform" property="faxNumber"/>' maxlength="13"></td>
					     </tr>

				
				        <tr id="save_cancel" class="hide">
							<td align="left" colspan="5">
						        <input type="button" value=" Save " class="button1" onClick="clickSave();">&nbsp;
						        <input type="button" value="Cancel" class="button1" onClick="disableFields();">
					        </td>
				  		</tr>
			            </table>
				     </td>
				   </tr>
		     </table>
			 <table>     
				   <tr valign="top">
			       <tr><td>&nbsp;</td> 
				   </tr> 
				   <tr id="create_cancel" class="show">
			      <td align="left" colspan="4">                          
			      
					    <input type="button" class="button1" name="create" value=" Create " onClick="clickCreate();" <%=CREATE_ACCESS%>> &nbsp;
					    <input type="button" class="button1" name="maintain" value="Maintain" onClick="clickMaintain();" <%=MAINTAIN_ACCESS%>>
					
    				  </td>
    			</tr>
			       <tr>
					 <td>
            
<!--- START OF BODY -->
			<table border="1" bordercolor="<%=iumColorB%>" cellpadding=5 cellspacing=0 bgcolor="" width="700" class="listtable1">
			<tr>
				<td>
						<table border="0" cellpadding="2" cellspacing="2" class="listtable1" width="700">

			        <tr class="headerrow1">
							<td width="15">&nbsp;</td>
					        <td class="headerrow1" ><b>OFFICE ID</b></td>
					        <td class="headerrow1" >OFFICE/BRANCH NAME</td>
					        <td class="headerrow1" >TYPE</td>        
				        </tr>

				
		<% int i = 0; 
			String tr_class;
      String td_bgcolor;
     %>
	 
		 <logic:iterate id="rec" name="pageRecord" property="list">				

		<%
		  if (i%2 == 0) {
			 tr_class = "row1";
		     td_bgcolor = "#CECECE";
		   }
		  else {
		     tr_class = "row2";
		     td_bgcolor = "#EDEFF0";
		   }
			 
			 
		 %>
					<tr class="<%=tr_class%>" >
						<td><input type="radio" name="sloPick" value='<bean:write name="rec" property="officeId"/>'></td>
						<td><bean:write name="rec" property="officeId"/></td>
						<td><bean:write name="rec" property="officeName"/></td>
						<td><bean:write name="rec" property="officeType"/></td>
					</tr>
		<% i++; %>
		
		</logic:iterate>
<%   int pageNumber = Integer.parseInt(sloform.getPageNumber());  
		 Page pageRecord = (Page)request.getAttribute("pageRecord");
%>

<%   if (pageRecord==null){ %>		
				 	<tr>
					 <td class="headerrow4" colspan="4" align="left" width="100%"><bean:message key="message.noexisting" arg0="Offices/Branches"/></td>
					</tr>
 <%}else{%>
                    <tr>
                      <td class="headerrow4" colspan="4" width="100%" height="10" class="label2" valign="bottom" >
                                                <!-- don't display link for previous page if the current page is the first page -->					
                                               <% if (pageNumber > 1) { %>
                      <a href="#" onclick="javascript:rePaginate(1,'viewOffices.do');"><img src="images/pagnavfirst_off.gif" name="pagnavfirst" border="0" width="18" height="13"/></a>&nbsp;					
                      <a href="#" onclick="javascript:rePaginate(<%=pageNumber-1%>,'viewOffices.do');"><img src="images/pagnavleft_off.gif" name="pagnavleft" border="0" width="18" height="13"/></a>
                                                <% } else {%>
                      <img src="images/pagnavfirst_on.gif" name="pagnavfirst" border="0" width="18" height="13"/>&nbsp;
                      <img src="images/pagnavleft_on.gif" name="pagnavleft" border="0" width="18" height="13"/>
                                                <% } %>
                                                <% 
                                                int currLink = 1;
                                                int prevLink = 1;
                                                %>
                           <logic:iterate id="navLinks" name="pageRecord" property="pageNumbers">
                                                  <% currLink = ((Integer)navLinks).intValue(); %>
                                                  <% if (((Integer)navLinks).intValue() == pageNumber) { %>
                                   <b><bean:write name="navLinks"/></b>
                                                  <% } else { %>		
                                                    <% if ((currLink > (prevLink+1))) { %>
                                                      ...
                                                    <% } %>
                      <a href="#" onclick="rePaginate('<bean:write name="navLinks"/>','viewOffices.do');" class="links2"><bean:write name="navLinks"/></a>
                                                  <% } %>
                                                  <% prevLink = currLink; %>
                                                </logic:iterate>
									
                                                <!-- don't display link for next page if the current page is the last page -->
                                                
                                                   <%  if(pageNumber < prevLink) {  %>
                      <a href="#" onClick="rePaginate('<%=pageNumber+1%>','viewOffices.do');" class=links1><img src="<%=contextPath%>/images/pagnavright_off.gif" name="pagnavright" border="0" width="18" height="13"/></a>&nbsp;
                      <a href="#" onClick="rePaginate('<%=prevLink%>','viewOffices.do');" class=links1><img src="<%=contextPath%>/images/pagnavlast_off.gif" width="18" height="13" border="0"></a>
                                                <%  } else {  %>
                      <img src="<%=contextPath%>/images/pagnavright_on.gif" width="18" height="13" border="0">&nbsp;
                      <img src="<%=contextPath%>/images/pagnavlast_on.gif" width="18" height="13" border="0">
                                                <%  }  %>
                      </td>
                    </tr>
                                            <% } %>

					
					</table>
				</td>
				</tr>
				</table>
				</td>
			</tr>
			<tr id="create_cancel2" class="show">
					  <td align="left" colspan="4"><div id="create_btn">                          
					    <input type="button" class="button1" name="create" value=" Create " onClick="clickCreate();" <%=CREATE_ACCESS%>> &nbsp;
					    <input type="button" class="button1" name="maintain" value="Maintain" onClick="clickMaintain();" <%=MAINTAIN_ACCESS%>>
						</div>
    				  </td>
    		</tr>
		</table>
</form>
<!--- END OF BODY -->
</table>
</div>
</body>
</html>