<%@ page language="java" import="java.util.*,com.slocpi.ium.data.*, com.slocpi.ium.ui.form.*, com.slocpi.ium.util.*, com.slocpi.ium.ui.util.*, java.text.* " buffer="12kb"%>
<%@ taglib uri="/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/struts-html.tld" prefix="html" %>
<%@ taglib uri="/ium.tld" prefix="ium" %>
<%

ArrayList requirementList = (ArrayList)request.getAttribute("requirementList"); 
PolicyRequirementsForm requirement = request.getAttribute("requirement")!=null ? (PolicyRequirementsForm)request.getAttribute("requirement") : new PolicyRequirementsForm(); 
String contextPath = request.getContextPath();
StateHandler sessionHandler = new StateHandler();
UserData userData = sessionHandler.getUserData(request); 
UserProfileData userPref = userData.getProfile();
String arFormRefNo = request.getParameter("arFormRefNo")!=null ? request.getParameter("arFormRefNo") :"";
String arFormOwnerClientNo = request.getParameter("arFormOwnerClientNo")!=null ? request.getParameter("arFormOwnerClientNo") :"";
String arFormRequestStatus = request.getParameter("arFormRequestStatus")!=null ? request.getParameter("arFormRequestStatus") :"";
String lob = request.getParameter("lob")!=null ? request.getParameter("lob") :"";
String sessionId = request.getParameter("sessionId")!=null ? request.getParameter("sessionId") :"";
String detailForm = "detailForm_" +sessionId;
String sourceSystem = request.getParameter("sourceSystem")!=null ? request.getParameter("sourceSystem") :"";
String agentCode = request.getParameter("agentCode")!=null ? request.getParameter("agentCode") :"";
String branchCode = request.getParameter("branchCode")!=null ? request.getParameter("branchCode") :"";
String assignedTo = request.getParameter("assignedTo")!=null ? request.getParameter("assignedTo") :"";
String EXECUTE_KO = request.getParameter("EXECUTE_KO")!=null ? request.getParameter("EXECUTE_KO") :"";
String sendButtonControl = request.getParameter("sendButtonControl")!=null ? request.getParameter("sendButtonControl") :"";
int pageNumber = 1;



if(arFormRefNo.equals("")){
	arFormRefNo = (String)request.getAttribute("arFormRefNo");
}
if(arFormOwnerClientNo.equals("")){
	arFormOwnerClientNo = (String)request.getAttribute("arFormOwnerClientNo");
}
if(arFormRequestStatus.equals("")){
	arFormRequestStatus = (String)request.getAttribute("arFormRequestStatus");
}
if(lob.equals("")){
	lob = (String)request.getAttribute("lob");
}
if(sessionId.equals("")){
	sessionId = (String)request.getAttribute("sessionId");
}
if(sourceSystem.equals("")){
	sourceSystem = (String)request.getAttribute("sourceSystem");
}
if(agentCode.equals("")){
	agentCode = (String)request.getAttribute("agentCode");
}
if(branchCode.equals("")){
	branchCode = (String)request.getAttribute("branchCode");
}
if(assignedTo.equals("")){
	assignedTo = (String)request.getAttribute("assignedTo");
}

if(EXECUTE_KO.equals("")){
	EXECUTE_KO = (String)request.getAttribute("EXECUTE_KO");
}

if(sendButtonControl.equals("")){
	sendButtonControl = (String)request.getAttribute("sendButtonControl");
}

 %>    
<html>

<head>
<title>Integrated Underwriting and Medical System</title>
<link href="<%=contextPath%>/css/pw_main.css"     rel="stylesheet" type="text/css">
<link href="<%=contextPath%>/css/main.css" rel="stylesheet" type="text/css">
<link href="<%=contextPath%>/css/styleCommon.css" rel="stylesheet" type="text/css">
<script language="JavaScript" src="<%=contextPath%>/js/validation.js"></script>
<script language="JavaScript" src="<%=contextPath%>/js/dhtml_init.js"></script>
<script language="JavaScript" src="<%=contextPath%>/js/navigation.js"></script>
<script language="JavaScript" src="<%=contextPath%>/jsp/sl_left_nav.jsp"></script>
<script language="JavaScript">

<%if(arFormRefNo.equals("")){%>
document.frm.refNo.value = opener.document.getElementById('arFormRefNo').value;
//alert("document.frm.refNo.value "+document.frm.refNo.value);
<%}%>


function putButton(numRecordsPerView, numRecords){
	if(numRecords >= 10 && numRecordsPerView >= 10){
        //document.all["create_btn"].style.display = "block";
    }
    else{
      // document.all["create_btn"].style.display = "none";
    }
}

function rePaginate( pageNum, target){
    var frm = document.frm;
		frm.pageNumber.value = pageNum;
		frm.action=target;
		frm.submit();
}

function trimTextArea(){
   var frm= document.frm;
	 if(frm.reqDesc.value.length>70){
	    frm.reqDesc.value=frm.reqDesc.value.substring(0,70);
	 }

}

function clickMaintain(){
	 var form = document.frm;
	 var value = form.reqId.value;
	 if(value!=''){
	 	//alert('clickMaintain value '+value);
	    enableFields();
			//frm.reqCode.value = value;
			//frm.action='viewPolicyRequirements.do?arFormRefNo=<%=arFormRefNo%>&arFormOwnerClientNo=<%=arFormOwnerClientNo%>';
			//frm.submit();	
	 }else{
	    //alert('<bean:message key="error.field.requiredselection" arg0="requirement"/>');
	 }
}

function clickSave(){
   var frm = document.frm; 
   if(validateFormEntry()==true){
				
	 <%if(requirement==null||requirement.getReqCode()==null||requirement.getReqCode().equals("")||request.getParameter("mode")!=null){%>
	      frm.action="maintainAdminRequirementCode.do?mode=add";
				frm.submit();
	 <%}else{%>
	      frm.action="maintainAdminRequirementCode.do";
				frm.reqCode.disabled=false;
				frm.submit();
	 <%}%>
	 }
}

function validateFormEntry(){
   var result=true;
	 var frm = document.frm;
	 if(isEmpty(frm.reqCode.value)){
	    alert('<bean:message key="error.field.required" arg0="Code"/>');
			frm.reqCode.focus();
			return false;																
	 }
	 if(isEmpty(frm.reqDesc.value)){
	    alert('<bean:message key="error.field.required" arg0="Description"/>');
			frm.reqDesc.focus();
			return false;																
	 }
	 if(frm.reqLevel[0].checked==false && frm.reqLevel[1].checked==false){
	    alert('<bean:message key="error.field.singleselection" arg0="Level"/>');
			frm.reqLevel[0].focus();
			return false;																
	 }
	 if(isEmpty(frm.validity.value)){
	    alert('<bean:message key="error.field.required" arg0="Validity"/>');
			frm.validity.focus();
			return false;																
	 }else{
	 	  if(!isNumeric(frm.validity.value)){
	       alert('<bean:message key="error.field.numeric" arg0="Validity"/>');
			   frm.validity.focus();
			   return false;																
	 		}
	 }
	 
	 if(isEmpty(frm.followUpNo.value)){
	    alert('<bean:message key="error.field.required" arg0="Follow Up"/>');
			frm.followUpNo.focus();
			return false;																
	 }else{
	 	  if(!isNumeric(frm.followUpNo.value)){
	       alert('<bean:message key="error.field.numeric" arg0="Follow Up"/>');
			   frm.followUpNo.focus();
			   return false;																
	 		}
	 }

	 if(frm.withForm[0].checked==false && frm.withForm[1].checked==false){
	    alert('<bean:message key="error.field.singleselection" arg0="with Form"/>');
			frm.withForm[0].focus();
			return false;																
	 }
	 
	 if(frm.withForm[0].checked==true && noSelection(frm.formId.value)){
      alert('<bean:message key="error.field.required" arg0="Form"/>');
			frm.formId.focus();
			return false;
				 																	
	 }
	 return result;
}

function checkHasReqPick(){
   var pick = document.frm.reqPick;
	 var result='';
	 if(pick.value==null){
		 for(i=0;i<pick.length;i++){
		   if(pick[i].checked==true){
			   result=pick[i].value;
			 }
		 }
	 }else{
	    if(pick.checked==true){
			   result=pick.value;											 			
			}
	 }
	 return result;
}

function disableFields(){
   var frm = document.frm;
	 //frm.reqCode.disabled=true;
	 //frm.reqDesc.disabled=true;
	 //frm.reqLevel[0].disabled=true;
	 //frm.reqLevel[1].disabled=true;
	 //frm.validity.disabled=true;
	 //frm.followUpNo.disabled=true;
	 //frm.withForm[0].disabled=true;
	 //frm.withForm[1].disabled=true;	
	 //frm.formId.disabled=true;
	 //frm.testDateIndicator.disabled=true;	 	
 	 document.all['save_cancel'].className = 'hide';
 	 document.all['create_cancel'].className = 'show';
 	 document.all['create_cancel2'].className = 'show'; 	 
}

function enableFields(){
   var frm = document.frm;
	 //frm.reqCode.disabled=false;
	 //frm.reqDesc.disabled=false;
	 //frm.reqLevel[0].disabled=false;
	 //frm.reqLevel[1].disabled=false;
	 //frm.validity.disabled=false;
	 //frm.followUpNo.disabled=false;
	 //frm.withForm[0].disabled=false;
	 //frm.withForm[1].disabled=false;
	 //frm.testDateIndicator.disabled=false;	 		 	
	 document.all['save_cancel'].className = 'show'; 	 				 				 
 	 document.all['create_cancel'].className = 'hide';
 	 document.all['create_cancel2'].className = 'hide'; 	 
}

function clickCreate(){
	 enableFields();
	 var frm = document.frm;
	 emptyFields();
	 frm.reqLevel[0].checked=true;
	 frm.withForm[1].checked=true;
	 frm.reqCode.focus();
}

function emptyFields(){
	 var frm = document.frm;
	 //frm.reqCode.value='';
	 //frm.reqDesc.value='';
	 //frm.reqLevel[0].checked=false;
	 //frm.reqLevel[1].checked=false;
	 //frm.validity.value='';
	 //frm.followUpNo.value='';
	 //frm.withForm[0].checked=false;
	 //frm.withForm[1].checked=false;
	 //frm.formId.options[0].selected=true;
	 //frm.formId.disabled=true
}

function controlFormIdField(){
   var frm = document.frm;
	 if(frm.withForm[0].checked==true){
	    frm.formId.disabled=false;
	 }else{
	    frm.formId.disabled=true;
	 }				 
}

function getCheckedValue(radioObj) {
	if(!radioObj)
		return "";
	var radioLength = radioObj.length;
	if(radioLength == undefined)
		if(radioObj.checked)
			return radioObj.value;
		else
			return "";
	for(var i = 0; i < radioLength; i++) {
		if(radioObj[i].checked) {
			return radioObj[i].value;
		}
	}
	return "";
}


function clickSaveUpdate(){
   	var form = document.frm;
   	//alert(form.reqId.value+' '+form.reqCode.value);

	form.resolve.value = getCheckedValue(form.rdResolve);
	form.paidInd.value = getCheckedValue(form.rdPaidInd);
	form.newTest.value = getCheckedValue(form.rdNewTest);
   	form.autoOrder.value = getCheckedValue(form.rdAutoOrder);
   	form.fldComm.value = getCheckedValue(form.rdFldComm);

	form.action = "<%=contextPath%>/viewPolicyRequirements.do?mode=saveUpdate&session_id=<%=sessionId%>";
			//try{
			//	showLayer();
			//	window.question.focus();
			//	setLayerPosition2();
			//}catch(e){
			//}
	form.submit();	 
}

function clickSaveCreate(){
   	var form = document.frm;
   	//alert(form.reqId.value+' '+form.reqCode.value);

	//form.resolve.value = getCheckedValue(form.rdResolve);
	//form.paidInd.value = getCheckedValue(form.rdPaidInd);
	//form.newTest.value = getCheckedValue(form.rdNewTest);
   	//form.autoOrder.value = getCheckedValue(form.rdAutoOrder);
   	//form.fldComm.value = getCheckedValue(form.rdFldComm);

	form.action = "<%=contextPath%>/viewPolicyRequirements.do?mode=saveCreate&session_id=<%=sessionId%>&sessionId=<%=sessionId%>&arFormRefNo=<%=arFormRefNo%>&arFormOwnerClientNo=<%=arFormOwnerClientNo%>&lob=<%=lob%>&arFormRequestStatus=<%=arFormRequestStatus%>&sourceSystem=<%=sourceSystem%>&agentCode=<%=agentCode%>&branchCode=<%=branchCode%>&assignedTo=<%=assignedTo%>&sendButtonControl=<%=sendButtonControl%>&EXECUTE_KO=<%=EXECUTE_KO%>';";
			//try{
			//	showLayer();
			//	window.question.focus();
			//	setLayerPosition2();
			//}catch(e){
			//}
	form.submit();	 
}

function onLoadPage(){
	 <%if(requirement==null||requirement.getReqCode()==null||requirement.getReqCode().equals("")){%>
	 		  disableFields();																																																				     	 																																																						
	 <%}else{%>
	 			//enableFields();
				controlFormIdField();
				<%if(request.getParameter("mode")==null){%>
				   document.frm.reqCode.disabled=true;								 
			    <%}%>				   
	 <%}%>
}

function changeLevelAndFollowUpNo(obj, index){
	var form = document.frm;
	
	var selValue = obj.value
	
	//alert(selValue);
	//form.reqCodeArr[index].value = obj.innerText;
	//form.reqCodeValueArr[index].value = obj.innerText;
	
	//alert(index+" "+form.reqCodeArr[index].value);
	//alert(index+" "+form.reqCodeValueArr[index].value);		
	
	
	var reqLevel = "";
	var reqLevelDesc = "";		
	var followUpNo = "";
	var reqCodeDesc = "";	
	var reqCode = "";			

	reqLevel = selValue.substring(selValue.indexOf("+@1@")+4, selValue.indexOf("-@1@"));
	//alert("reqLevel "+reqLevel);
	form.reqCodeArr[index].value = reqLevel;
	
	followUpNo = selValue.substring(selValue.indexOf("+@2@")+4, selValue.indexOf("-@2@"));
	//alert("followUpNo "+followUpNo);

	reqCodeDesc = selValue.substring(selValue.indexOf("+@3@")+4, selValue.indexOf("-@3@"));
	//alert("reqCodeDesc "+reqCodeDesc);
	
	reqCode = selValue.substring(selValue.indexOf("+@4@")+4, selValue.indexOf("-@4@"));
	//alert("reqCode "+reqCode);

	form.reqCodeArr[index].value = reqCode;
	form.reqCodeValueArr[index].value = reqCode;
	form.reqLevelArr[index].value = reqLevel;
	form.followUpNoArr[index].value = followUpNo;
	
	//alert('index reqCode '+index+ ' '+reqCode);
	
	if(reqLevel == 'P'){
		reqLevelDesc = 'Policy Level';
	}else if(reqLevel == 'C'){
		reqLevelDesc = 'Client Level';
	}
	
	if(reqLevelDesc != ''){
		form.tdReqCodeDescArr[index].value = reqCodeDesc + ' |  '+reqLevelDesc;
	}else{
		form.tdReqCodeDescArr[index].value = reqCodeDesc;
	}
	
	
	//alert(form.selReqCode.index[0]);
	//if(){
	
	//}
	//alert(form.selReqCode[0].value);
	//alert(form.selReqCode[0].value);
	//alert(form.selReqCode[1].value);
	//alert(form.selReqCode[2].value);
	//alert(form.selReqCode[3].value);
	if(form.selReqCode[0].value=='' && form.selReqCode[1].value=='' && form.selReqCode[2].value=='' && form.selReqCode[3].value==''){
		form.create.disabled = "true";
	}else{
		form.create.disabled = "";
	}
	
	
	
	//form.getElementById('tdReqLevelDesc0').innerHTML = reqLevel;
	

}

function expandReq()
{
	if(document.getElementById('reqDiv').style.display == ""){
		document.getElementById('reqDiv').style.display = "none"
	} else {
		document.getElementById('reqDiv').style.display = ""
	}
}

function updateRequirement(reqId){
	//alert('reqId : '+reqId);
	var form = document.frm;
	var pageNumber = 1;
	form.reqId.value = reqId;
	//form.mode.value = "update";
	//form.pageNumber.value = </%=pageNumber%>;
	pageNumber = form.pageNumber.value;
	//alert('pageNumber '+pageNumber);
	
	form.action  = "<%=contextPath%>/viewPolicyRequirements.do?arFormRefNo=<%=arFormRefNo%>&arFormOwnerClientNo=<%=arFormOwnerClientNo%>&mode=update&pageNumber="+pageNumber;
	form.submit();
}

function sendNotification(contextPath,savedSectionReq) {
	var form = document.frm;
	form.action  = contextPath + "/sendNotificationOrderPolReqt.do?savedSectionReq="+ savedSectionReq;
	form.submit();
}

function setReqSavedSection(val) {
	document.frm.savedSectionReq.value = val;
}
								
function orderRequirement(contextPath, facilitator, form) {
	var lob = form.lob.value;
	var branchId = form.branchCode.value;
	var agentId = form.agentCode.value;
	var reqStat = form.currentStatus.value;
	form.action = contextPath + "/savePolicyRequirementStatus.do?lob=" + lob + "&facilitator=" + facilitator + "&branchCode=" + branchId + "&agentCode=" + agentId + "&arStatus=" + reqStat;
	form.submit();
}


function saveStatus(contextPath, followup) {
	var form = document.frm;
	var lob = form.lob.value;
	var branchId = form.branchCode.value;
	var assignedTo = form.assignedTo.value;
	var reqStat = form.currentStatus.value;

	if (followup == "no") {
		if (form.statusTo.value == "") {
			alert("<bean:message key="error.field.requiredselection" arg0="Requirement status"/>");
			form.statusTo.focus();
			return;
		}
	}

	var cnt    = 0;
	var ord    = 0;
	var rec    = 0;
	var waive  = 0;
	var acc    = 0;
	var rej    = 0;
	var canc   = 0;
	var others = 0;
	if (form.index[0]) {
		//alert('form.index[0]');
		for (var i=0;i<form.index.length;i++) {
			if (form.index[i].checked) {
				//alert('form.index[i].checked '+i);
				form.indexTemp[i].value = "checked";
				if (form.statusTo.value == "<%= IUMConstants.STATUS_ORDERED %>") {
					if (form.statusCodes[i].value == "<%= IUMConstants.STATUS_ORDERED %>") {
					    ord++;
					}
					else {
					    others++;
					}
			    }
				if (form.statusTo.value == "<%= IUMConstants.STATUS_RECEIVED_IN_SITE %>") {
					if (form.statusCodes[i].value == "<%= IUMConstants.STATUS_RECEIVED_IN_SITE %>") {
					    rec++;
					}
					else {
					    others++;
					}
			    }
				if (form.statusTo.value == "<%= IUMConstants.STATUS_WAIVED %>") {
					if (form.statusCodes[i].value == "<%= IUMConstants.STATUS_WAIVED %>") {
					    waive++;
					}
					else {
					    others++;
					}
			    }
				if (form.statusTo.value == "<%= IUMConstants.STATUS_REVIEWED_AND_REJECTED %>") {
					if (form.statusCodes[i].value == "<%= IUMConstants.STATUS_REVIEWED_AND_REJECTED %>") {
					    rej++;
					}
					else {
					    others++;
					}
			    }
				if (form.statusTo.value == "<%= IUMConstants.STATUS_REVIEWED_AND_ACCEPTED %>") {
					if (form.statusCodes[i].value == "<%= IUMConstants.STATUS_REVIEWED_AND_ACCEPTED %>") {
					    acc++;
					}
					else {
					    others++;
					}
			    }
				if (form.statusTo.value == "<%= IUMConstants.STATUS_REQ_CANCELLED %>") {
					if (form.statusCodes[i].value == "<%= IUMConstants.STATUS_REQ_CANCELLED %>") {
					    canc++;
					}
					else {
					    others++;
					}
			    }

				cnt++;
			}
		}
	}
	else {
		if (form.index.checked) {
			//alert('form.index.checked ');
			form.indexTemp.value = "checked";
			if (form.statusTo.value == "<%= IUMConstants.STATUS_ORDERED %>") {
				if (form.statusCodes.value == "<%= IUMConstants.STATUS_ORDERED %>") {
				    ord++;
				}
			}
			if (form.statusTo.value == "<%= IUMConstants.STATUS_RECEIVED_IN_SITE %>") {
				if (form.statusCodes.value == "<%= IUMConstants.STATUS_RECEIVED_IN_SITE %>") {
				    rec++;
				}
			}
			if (form.statusTo.value == "<%= IUMConstants.STATUS_WAIVED %>") {
				if (form.statusCodes.value == "<%= IUMConstants.STATUS_WAIVED %>") {
				    waive++;
				}
			}
			if (form.statusTo.value == "<%= IUMConstants.STATUS_REVIEWED_AND_REJECTED %>") {
				if (form.statusCodes.value == "<%= IUMConstants.STATUS_REVIEWED_AND_REJECTED %>") {
				    rej++;
				}
			}
			if (form.statusTo.value == "<%= IUMConstants.STATUS_REVIEWED_AND_ACCEPTED %>") {
				if (form.statusCodes.value == "<%= IUMConstants.STATUS_REVIEWED_AND_ACCEPTED %>") {
				    acc++;
				}
			}
			if (form.statusTo.value == "<%= IUMConstants.STATUS_REQ_CANCELLED %>") {
				if (form.statusCodes.value == "<%= IUMConstants.STATUS_REQ_CANCELLED %>") {
				    canc++;
				}
			}
			cnt++;
		}
	}

	if (cnt == 0) {
		alert("<bean:message key="error.field.noselection" arg0="" arg1="to update"/>");
		return;
	}
	if (ord == 1 && others == 0) {
		alert("<bean:message key="error.sameStatusRequirement" arg0="ordered"/>");
		return;
	}
	if (rec == 1 && others == 0) {
		alert("<bean:message key="error.sameStatusRequirement" arg0="received"/>");
		return;
	}
	if (waive == 1 && others == 0) {
		alert("<bean:message key="error.sameStatusRequirement" arg0="waived"/>.");
		return;
	}
	if (rej == 1 && others == 0) {
		alert("<bean:message key="error.sameStatusRequirement" arg0="rejected"/>");
		return;
	}
	if (acc == 1 && others == 0) {
		alert("<bean:message key="error.sameStatusRequirement" arg0="accepted"/>");
		return;
	}
	if (canc == 1 && others == 0) {
		alert("<bean:message key="error.sameStatusRequirement" arg0="cancelled"/>");
		return;
	}

	var status = form.statusTo.value;
	
	if (status == "<%= IUMConstants.STATUS_ORDERED %>" && lob == "<%= IUMConstants.LOB_INDIVIDUAL_LIFE %>") {
		orderRequirement(contextPath, form.facilitator.value, document.frm);
	}
	
	if (status == "<%= IUMConstants.STATUS_ORDERED %>" && (lob == "<%= IUMConstants.LOB_PRE_NEED %>" || lob == "<%= IUMConstants.LOB_GROUP_LIFE %>")) {
	       if (reqStat == "<%= IUMConstants.STATUS_NB_REVIEW_ACTION %>" || reqStat == "<%= IUMConstants.STATUS_AWAITING_REQUIREMENTS %>") {
	           orderRequirement(contextPath, "", document.frm);
	       } else {
 	           alert("<bean:message key="error.requirement.invalid" arg0="ordering"/>");
 	           return;
	       }
	       if (reqStat == "<%= IUMConstants.STATUS_UNDERGOING_ASSESSMENT %>" || reqStat == "<%= IUMConstants.STATUS_AWAITING_REQUIREMENTS %>") {
               if (countStatusOrdered(form) == "false") {
		           if (form.facilitator.value == "") {
		              if (form.facilitator.disabled) {
		                  form.facilitator.disabled = false;
		              }
				      alert("<bean:message key="error.field.requiredselection" arg0="Facilitator"/>");
				      form.facilitator.focus();
				      return;
	        	   }
	           }
	           orderRequirement(contextPath, form.facilitator.value, document.frm);
           } else {
 	           alert("<bean:message key="error.requirement.invalid" arg0="ordering"/>");
 	           return;
           }
	}

    if (followup == "no" && status != "<%= IUMConstants.STATUS_ORDERED %>") {
	   form.action = contextPath + "/savePolicyRequirementStatus.do?lob=" + lob + "&branchCode=" + branchId + "&arStatus=" + reqStat + "&assignedTo=" + assignedTo;
	   form.submit();
    }
    if (followup == "yes") {
	   form.action = contextPath + "/savePolicyRequirementStatus.do?lob=" + lob + "&branchCode=" + branchId + "&followUp=yes&arStatus=" + reqStat;
  	   form.submit();
	}
}

function countStatusOrdered(form) {
    var form = document.frm;
    var cnt  = 0;
    
   if (form.index[0]) {
        for (var i=0;i<form.index.length;i++) {
           if (form.index[i].disabled == false) {
	         if (form.statusCodes[i].value == "<%= IUMConstants.STATUS_ORDERED %>") {
	            cnt++;
	         }
	       }
	    }
   }
   else {
      if (form.index.disabled == false) {
       if (form.statusCodes.value == "<%= IUMConstants.STATUS_ORDERED %>") {
          cnt++;
       }
      }
    }
    if (cnt > 0) {
        return "true";
    }
    else {
        
        return "false";
    }
}

function onChangeOfStatus() {
    var form = document.frm; 
	var lob = form.lob.value;
	var reqStat = form.currentStatus.value;
	var status  = form.statusTo.value;
	
	if (status != "<%= IUMConstants.STATUS_ORDERED %>") {
       form.facilitator.disabled = true;
	}
	if (status == "<%= IUMConstants.STATUS_ORDERED %>" && lob == "<%= IUMConstants.LOB_INDIVIDUAL_LIFE %>") {
  	      if (countStatusOrdered(form) == "false") {
	         form.facilitator.disabled = false;
	      } else {
	         form.facilitator.disabled = true;
	      }
    }
	if (status == "<%= IUMConstants.STATUS_ORDERED %>") {
	    if (lob == "<%= IUMConstants.LOB_PRE_NEED %>" || lob == "<%= IUMConstants.LOB_GROUP_LIFE %>") {
	       if (reqStat == "<%= IUMConstants.STATUS_NB_REVIEW_ACTION %>" || reqStat == "<%= IUMConstants.STATUS_AWAITING_REQUIREMENTS %>") {
	           if (!form.facilitator.disabled) {
	    	        form.facilitator.disabled = true;
	    	   }
       	   }
	       if (reqStat == "<%= IUMConstants.STATUS_UNDERGOING_ASSESSMENT %>" || reqStat == "<%= IUMConstants.STATUS_AWAITING_REQUIREMENTS %>") {
	           if (form.facilitator.disabled) {
	  	       	   if (countStatusOrdered(form) == "false") {
    	 	          form.facilitator.disabled = false;
    	 	       }
    	       }
       	   }
	    }
	}
}
</script>
</head>

<%
String reqId = requirement.getReqId()!=null && !requirement.getReqId().equals("") ? requirement.getReqId() : "";
String reqCode = requirement.getReqCode()!=null && !requirement.getReqCode().equals("") ? requirement.getReqCode() : "-";
String reqCodeValue = requirement.getReqCodeValue()!=null && !requirement.getReqCodeValue().equals("") ? requirement.getReqCodeValue() : "-";
String reqDesc = requirement.getReqDesc()!=null && !requirement.getReqDesc().equals("") ? requirement.getReqDesc() : "-";
String clientNo = requirement.getClientNo()!=null && !requirement.getClientNo().equals("") ? requirement.getClientNo() : "-";
String clientType = requirement.getClientType()!=null && !requirement.getClientType().equals("") ? requirement.getClientType() : "-";
String clientTypeDesc = "";
if(clientType.equals("I")){
	clientTypeDesc = "Insured";
}else if(clientType.equals("O")){
	clientTypeDesc = "Owner";
}else{
	clientTypeDesc = "-";
}
String followUpDate = requirement.getFollowUpDate()!=null && !requirement.getFollowUpDate().equals("") ? requirement.getFollowUpDate() : "";
String comment = requirement.getComment()!=null && !requirement.getComment().equals("") ? requirement.getComment() : "";
String seq = requirement.getSeq()!=null && !requirement.getSeq().equals("") ? requirement.getSeq() : "";
String reqStatusCode = requirement.getReqStatusCode()!=null && !requirement.getReqStatusCode().equals("") ? requirement.getReqStatusCode() : "";
String reqStatusValue = requirement.getReqStatusValue()!=null && !requirement.getReqStatusValue().equals("") ? requirement.getReqStatusValue() : "";
String completeReq = requirement.getCompleteReq()!=null && !requirement.getCompleteReq().equals("") ? requirement.getCompleteReq() : "";
String completeReqDesc = "";
if(completeReq.equals("1")){
	completeReqDesc = "Yes";
}else if(completeReq.equals("0")){
	completeReqDesc = "No";
}else{
	completeReqDesc = "No";
}
String statusDate = requirement.getStatusDate()!=null && !requirement.getStatusDate().equals("") ? requirement.getStatusDate() : "";
String ccasSuggest = requirement.getCcasSuggest()!=null && !requirement.getCcasSuggest().equals("") ? requirement.getCcasSuggest() : "";
String ccasSuggestDesc = "";
if(ccasSuggest.equals("1")){
	ccasSuggestDesc = "Yes";
}else if(ccasSuggest.equals("0")){
	ccasSuggestDesc = "No";
}else{
	ccasSuggestDesc = "No";
}
String designation = requirement.getDesignation()!=null && !requirement.getDesignation().equals("") ? requirement.getDesignation() : "";
String updatedBy = requirement.getUpdatedBy()!=null && !requirement.getUpdatedBy().equals("") ? requirement.getUpdatedBy() : "";
String updatedDate = requirement.getUpdatedDate()!=null && !requirement.getUpdatedDate().equals("") ? requirement.getUpdatedDate() : "";
String validityDate = requirement.getValidityDate()!=null && !requirement.getValidityDate().equals("") ? requirement.getValidityDate() : "";
String testDate = requirement.getTestDate()!=null && !requirement.getTestDate().equals("") ? requirement.getTestDate() : "";
String testResult = requirement.getTestResult()!=null && !requirement.getTestResult().equals("") ? requirement.getTestResult() : "";
String orderDate = requirement.getOrderDate()!=null && !requirement.getOrderDate().equals("") ? requirement.getOrderDate() : "";
String testDesc = requirement.getTestDesc()!=null && !requirement.getTestDesc().equals("") ? requirement.getTestDesc() : "";


String resolve = requirement.getResolve()!=null && !requirement.getResolve().equals("") ? requirement.getResolve() : "";
String resolveYes = "";
String resolveNo = "";
if (resolve.equals("true")) {
	resolveYes = "checked";
}else if (resolve.equals("false")){
	resolveNo = "checked";
}
String followUpNo = requirement.getFollowUpNo()!=null && !requirement.getFollowUpNo().equals("") ? requirement.getFollowUpNo() : "";
String reqLevel = requirement.getReqLevel()!=null && !requirement.getReqLevel().equals("") ? requirement.getReqLevel() : "";
String reqLevelDesc = "";
if(reqLevel.equals("C")){
	reqLevelDesc = "Client";
}else if(reqLevel.equals("P")){
	reqLevelDesc = "Policy";
}else{
	reqLevelDesc = "-";
}
String dateSent = requirement.getDateSent()!=null && !requirement.getDateSent().equals("") ? requirement.getDateSent() : "";
String paidInd = requirement.getPaidInd()!=null && !requirement.getPaidInd().equals("") ? requirement.getPaidInd() : "";
String paidIndYes = "";
String paidIndNo = "";
if (paidInd.equals("true")) {
	paidIndYes = "checked";
}else if (paidInd.equals("false")){
	paidIndNo = "checked";
}else{
	paidIndNo = "checked";
}
String newTest = requirement.getNewTest()!=null && !requirement.getNewTest().equals("") ? requirement.getNewTest() : "";

String newTestYes = "";
String newTestNo = "";
if (newTest.equals("Y")) {
	newTestYes = "checked";
}else if (newTest.equals("N")){
	newTestNo = "checked";
}else{
	newTestNo = "checked";
}
String receivedDate = requirement.getReceivedDate()!=null && !requirement.getReceivedDate().equals("") ? requirement.getReceivedDate() : "";
String autoOrder = requirement.getAutoOrder()!=null && !requirement.getAutoOrder().equals("") ? requirement.getAutoOrder() : "";
String autoOrderYes = "";
String autoOrderNo = "";
if (autoOrder.equals("Y")) {
	autoOrderYes = "checked";
}else if (autoOrder.equals("N")){
	autoOrderNo = "checked";
}else{
	autoOrderNo = "checked";
}
String fldComm = requirement.getFldComm()!=null && !requirement.getFldComm().equals("") ? requirement.getFldComm() : "";
String fldCommYes = "";
String fldCommNo = "";
if (fldComm.equals("Y")) {
	fldCommYes = "checked";
}else if (fldComm.equals("N")){
	fldCommNo = "checked";
}else{
	fldCommNo = "checked";
}
%>
<!-- <BODY leftMargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="onLoadPage(); putButton(</%=userPref.getRecordsPerView()%>, </%=((Page)request.getAttribute("pageRecord")).getList().size()%>); "> -->
<BODY leftMargin="0" topmargin="0" marginwidth="0" marginheight="0" >
<form name="frm"  method="post">    
<table width="100%" cellpadding="3" cellspacing="5" border="0">
	<tr valign="top">
		<td class="title2">Create Requirements
		
		<input type="hidden" name="lob" value='<%=lob%>'>
		<input type="hidden" name="currentStatus" value='<%=arFormRequestStatus%>'>
		<input type="hidden" name="refNo" value='<%=arFormRefNo%>'>
		<input type="hidden" name="arFormRefNo" value='<%=arFormRefNo%>'>
		<input type="hidden" name="arFormOwnerClientNo" value='<%=arFormOwnerClientNo%>'>
		<input type="hidden" name="sessionId" value='<%=sessionId%>'>
		<input type="hidden" name="branchCode" value="<%=branchCode%>">
		<input type="hidden" name="agentCode" value="<%=agentCode%>">
		<input type="hidden" name="assignedTo" value="<%=assignedTo%>">
		<input type="hidden" name="sourceSystem" value="<%=sourceSystem%>">
		
			<table border="1" bordercolor="#2A4C7C" cellpadding="5" cellspacing="0" bgcolor="" width="800" class="listtable1">
				<tr>
					<td height="100%">
						<table border="0" width="790" cellpadding="1" cellspacing="1" class="listtable1">
							<tr>
								<html:errors/>
								<td>
								<table border="0" width="790" cellpadding="1" cellspacing="1">
								<tr>
									<td class="label2" align="left" style="width: 75px"><span class="required">*</span><b>Code</b></td>
								</tr>
								<tr>
									<td class="label2" align="left">
										<input type="hidden" name="reqCodeValue" value=""/>
										<input type="hidden" name="reqCodeArr" value=""/>
										<input type="hidden" name="reqCodeValueArr" value=""/>
										<input type="hidden" name="reqLevelArr" value=""/>
							          		<!-- <select name="selReqCode" onChange="changeLevelAndType();" class="label2"> -->
							          		<select name="selReqCode" class="label2" onChange="changeLevelAndFollowUpNo(this, 0);">
											<%
											Iterator it = requirementList.iterator();
											int ctr = 0;
											while (it.hasNext()) {
												RequirementData data = (RequirementData) it.next();	
												
												if(ctr==0){
											%>
												<option value="">-</option>
											<%	
												}
												ctr++;
											%>
											<option value="+@1@<%=data.getLevel()%>-@1@+@2@<%=data.getFollowUpNum()%>-@2@+@3@<%=data.getReqtDesc()%>-@3@+@4@<%=data.getReqtCode()%>-@4@"><%=data.getReqtCode()%> - <%=data.getReqtDesc()%></option>
											<%}%>
											</select>
							          </td>
								</tr>
								</table>
								</td>
	   				    	</tr>
	   				    	
	   				    	<tr>
								<td>
								<table border="0" width="790" cellpadding="1" cellspacing="1">
								<tr>
									<td class="label2" align="left" style="width: 90px"><span class="required">*</span><b>Client Type</b></td>
									<td class="label2" align="left" style="width: 25px"><b>Seq</b></td>
		   				    		<td class="label2" align="left" style="width: 90px"><b>Designation</b></td>
		   				    		<td class="label2" align="left" style="width: 105px"><b>Validity Date</b></td>
		   				    		<td class="label2" align="left" style="width: 105px"><b>Test Date</b></td>
		   				    		<td class="label2" align="left" style="width: 50px"><b>Follow Up No</b></td>
									<td class="label2" align="left" style="width: 50px"><b>Resolve</b></td>
		   				    		<td class="label2" align="left" style="width: 50px"><b>Paid Ind</b></td>
		   				    		<td class="label2" align="left" style="width: 50px"><b>New Test</b></td>
		   				    		<td class="label2" align="left" style="width: 50px"><b>Auto Order</b></td>
		   				    		<td class="label2" align="left" style="width: 50px"><b>Fld Comm</b></td>
								 </tr>
								 <tr>
								 	<td class="label2" align="left">
							          	<select name="clientTypeArr" id="clientTypeArr" class="label2">
							          		<option value="I">Insured</option>
							          		<option value="O">Owner</option>
							          	</select>
							          </td>
							          <td class="label2" align="left">
							          		<input type="text" name="seqArr" class="label2" value="" maxLength="10" style="width: 25px"/>
							          </td>
							          <td class="label2" align="left">
							          		<input type="text" name="designationArr" class="label2" value="" maxlength="10" style="width: 90px"/>
							          </td>
							          <td class="label2"><input type="text" name="validityDateArr" class="label2" value="<%=validityDate%>" onKeyUp="getKeyDate(event,this);" maxlength="9" style="width: 75px">&nbsp;
										<div id="doc_cal2" style="position:absolute;">
											<a href="#" onClick="popUpWindow('<%=contextPath%>/jsp/calendar.jsp?textbox=frm.validityDateArr[0]',290,155,'yes','yes','yes','no','no','no','no','3');">
											<img src="<%=contextPath%>/images/calendar.gif" height="17" width="19" border="0"> 
											</a>
										</div>
									  </td>
									  <td class="label2"><input type="text" name="testDateArr" class="label2" value="<%=testDate%>" onKeyUp="getKeyDate(event,this);" maxlength="9" style="width: 75px">&nbsp;
										<div id="doc_cal1" style="position:absolute;">
											<a href="#" onClick="popUpWindow('<%=contextPath%>/jsp/calendar.jsp?textbox=frm.testDateArr[0]',290,155,'yes','yes','yes','no','no','no','no','3');">
												<img src="<%=contextPath%>/images/calendar.gif" height="17" width="19" border="0"> 
											</a>
										</div>
									  </td>
							          <td class="label2" align="left">
							          	<input type="text" name="followUpNoArr" class="label2" value="" maxlength="1" style="width: 25px"/>
							          </td>
								 	<td class="label2" align="left">
							          	<select name="resolveArr" id="resolveArr" class="label2">
							          		<option value="N">No</option>
							          		<option value="Y">Yes</option>
							          	</select>
							          </td>
							          <td class="label2" align="left">
							          	<select name="paidIndArr" id="paidIndArr" class="label2">
							          		<option value="N">No</option>
							          		<option value="Y">Yes</option>
							          	</select>
							          </td>      
							          <td class="label2" align="left">
							          	<select name="newTestArr" id="newTestArr" class="label2">
							          		<option value="N">No</option>
							          		<option value="Y">Yes</option>
							          	</select>
							          </td>
							          <td class="label2" align="left">
							          	<select name="autoOrderArr" id="autoOrderArr" class="label2">
							          		<option value="N">No</option>
							          		<option value="Y">Yes</option>
							          	</select>
							          </td>
							          <td class="label2" align="left">
							          	<select name="fldCommArr" id="fldCommArr" class="label2">
							          		<option value="N">No</option>
							          		<option value="Y">Yes</option>
							          	</select>
							          </td>
								 </tr>
								</table>
								</td>
					    	</tr>
					    	
					    	<tr>
								<td>
								<table border="0" width="790" cellpadding="1" cellspacing="1">
								<tr>
		   				    		<td class="label2" align="left"><b>Comment</b></td>
								 </tr>
								 <tr>
										<td class="label2" align="left"><textarea class="label2" name="commentArr" cols="77" rows="2"></textarea>
										<input type="hidden" name="tdReqCodeDescArr" id="tdReqCodeDescArr" value="" /></td>
								 </tr>
								</table>
								</td>
					    	</tr>
					</table>
						
					</td>
				</tr>
				
				
				
				<tr>
					<td height="100%">
						<table border="0" width="790" cellpadding="1" cellspacing="1" class="listtable1">
							<tr>
								<html:errors/>
								<td>
								<table border="0" width="790" cellpadding="1" cellspacing="1">
								<tr>
									<td class="label2" align="left" style="width: 75px"><span class="required">*</span><b>Code</b></td>
								</tr>
								<tr>
									<td class="label2" align="left">
										<input type="hidden" name="reqCodeValue" value=""/>
										<input type="hidden" name="reqCodeArr" value=""/>
										<input type="hidden" name="reqCodeValueArr" value=""/>
										<input type="hidden" name="reqLevelArr" value=""/>
							          		<!-- <select name="selReqCode" onChange="changeLevelAndType();" class="label2"> -->
							          		<select name="selReqCode" class="label2" onChange="changeLevelAndFollowUpNo(this, 1);">
											<%
											it = requirementList.iterator();
											ctr = 0;
											while (it.hasNext()) {
												RequirementData data = (RequirementData) it.next();	
												
												if(ctr==0){
											%>
												<option value="">-</option>
											<%	
												}
												ctr++;
											%>
											<option value="+@1@<%=data.getLevel()%>-@1@+@2@<%=data.getFollowUpNum()%>-@2@+@3@<%=data.getReqtDesc()%>-@3@+@4@<%=data.getReqtCode()%>-@4@"><%=data.getReqtCode()%> - <%=data.getReqtDesc()%></option>
											<%}%>
											</select>
							          </td>
								</tr>
								</table>
								</td>
	   				    	</tr>
	   				    	
	   				    	<tr>
								<td>
								<table border="0" width="790" cellpadding="1" cellspacing="1">
								<tr>
									<td class="label2" align="left" style="width: 90px"><span class="required">*</span><b>Client Type</b></td>
									<td class="label2" align="left" style="width: 25px"><b>Seq</b></td>
		   				    		<td class="label2" align="left" style="width: 90px"><b>Designation</b></td>
		   				    		<td class="label2" align="left" style="width: 105px"><b>Validity Date</b></td>
		   				    		<td class="label2" align="left" style="width: 105px"><b>Test Date</b></td>
		   				    		<td class="label2" align="left" style="width: 50px"><b>Follow Up No</b></td>
									<td class="label2" align="left" style="width: 50px"><b>Resolve</b></td>
		   				    		<td class="label2" align="left" style="width: 50px"><b>Paid Ind</b></td>
		   				    		<td class="label2" align="left" style="width: 50px"><b>New Test</b></td>
		   				    		<td class="label2" align="left" style="width: 50px"><b>Auto Order</b></td>
		   				    		<td class="label2" align="left" style="width: 50px"><b>Fld Comm</b></td>
								 </tr>
								 <tr>
								 	<td class="label2" align="left">
							          	<select name="clientTypeArr" id="clientTypeArr" class="label2">
							          		<option value="I">Insured</option>
							          		<option value="O">Owner</option>
							          	</select>
							          </td>
							          <td class="label2" align="left">
							          		<input type="text" name="seqArr" class="label2" value="" maxLength="10" style="width: 25px"/>
							          </td>
							          <td class="label2" align="left">
							          		<input type="text" name="designationArr" class="label2" value="" maxlength="10" style="width: 90px"/>
							          </td>
							          <td class="label2"><input type="text" name="validityDateArr" class="label2" value="<%=validityDate%>" onKeyUp="getKeyDate(event,this);" maxlength="9" style="width: 75px">&nbsp;
										<div id="doc_cal2" style="position:absolute;">
											<a href="#" onClick="popUpWindow('<%=contextPath%>/jsp/calendar.jsp?textbox=frm.validityDateArr[1]',290,155,'yes','yes','yes','no','no','no','no','3');">
											<img src="<%=contextPath%>/images/calendar.gif" height="17" width="19" border="0"> 
											</a>
										</div>
									  </td>
									  <td class="label2"><input type="text" name="testDateArr" class="label2" value="<%=testDate%>" onKeyUp="getKeyDate(event,this);" maxlength="9" style="width: 75px">&nbsp;
										<div id="doc_cal1" style="position:absolute;">
											<a href="#" onClick="popUpWindow('<%=contextPath%>/jsp/calendar.jsp?textbox=frm.testDateArr[1]',290,155,'yes','yes','yes','no','no','no','no','3');">
												<img src="<%=contextPath%>/images/calendar.gif" height="17" width="19" border="0"> 
											</a>
										</div>
									  </td>
							          <td class="label2" align="left">
							          	<input type="text" name="followUpNoArr" class="label2" value="" maxlength="1" style="width: 25px"/>
							          </td>
								 	<td class="label2" align="left">
							          	<select name="resolveArr" id="resolveArr" class="label2">
							          		<option value="N">No</option>
							          		<option value="Y">Yes</option>
							          	</select>
							          </td>
							          <td class="label2" align="left">
							          	<select name="paidIndArr" id="paidIndArr" class="label2">
							          		<option value="N">No</option>
							          		<option value="Y">Yes</option>
							          	</select>
							          </td>      
							          <td class="label2" align="left">
							          	<select name="newTestArr" id="newTestArr" class="label2">
							          		<option value="N">No</option>
							          		<option value="Y">Yes</option>
							          	</select>
							          </td>
							          <td class="label2" align="left">
							          	<select name="autoOrderArr" id="autoOrderArr" class="label2">
							          		<option value="N">No</option>
							          		<option value="Y">Yes</option>
							          	</select>
							          </td>
							          <td class="label2" align="left">
							          	<select name="fldCommArr" id="fldCommArr" class="label2">
							          		<option value="N">No</option>
							          		<option value="Y">Yes</option>
							          	</select>
							          </td>
								 </tr>
								</table>
								</td>
					    	</tr>
					    	
					    	<tr>
								<td>
								<table border="0" width="790" cellpadding="1" cellspacing="1">
								<tr>
		   				    		<td class="label2" align="left"><b>Comment</b></td>
								 </tr>
								 <tr>
										<td class="label2" align="left"><textarea class="label2" name="commentArr" cols="77" rows="2"></textarea>
										<input type="hidden" name="tdReqCodeDescArr" id="tdReqCodeDescArr" value="" /></td>
								 </tr>
								</table>
								</td>
					    	</tr>
					</table>
						
					</td>
				</tr>
				

				<tr>
					<td height="100%">
						<table border="0" width="790" cellpadding="1" cellspacing="1" class="listtable1">
							<tr>
								<html:errors/>
								<td>
								<table border="0" width="790" cellpadding="1" cellspacing="1">
								<tr>
									<td class="label2" align="left" style="width: 75px"><span class="required">*</span><b>Code</b></td>
								</tr>
								<tr>
									<td class="label2" align="left">
										<input type="hidden" name="reqCodeValue" value=""/>
										<input type="hidden" name="reqCodeArr" value=""/>
										<input type="hidden" name="reqCodeValueArr" value=""/>
										<input type="hidden" name="reqLevelArr" value=""/>
							          		<!-- <select name="selReqCode" onChange="changeLevelAndType();" class="label2"> -->
							          		<select name="selReqCode" class="label2" onChange="changeLevelAndFollowUpNo(this, 2);">
											<%
											it = requirementList.iterator();
											ctr = 0;
											while (it.hasNext()) {
												RequirementData data = (RequirementData) it.next();	
												
												if(ctr==0){
											%>
												<option value="">-</option>
											<%	
												}
												ctr++;
											%>
											<option value="+@1@<%=data.getLevel()%>-@1@+@2@<%=data.getFollowUpNum()%>-@2@+@3@<%=data.getReqtDesc()%>-@3@+@4@<%=data.getReqtCode()%>-@4@"><%=data.getReqtCode()%> - <%=data.getReqtDesc()%></option>
											<%}%>
											</select>
							          </td>
								</tr>
								</table>
								</td>
	   				    	</tr>
	   				    	
	   				    	<tr>
								<td>
								<table border="0" width="790" cellpadding="1" cellspacing="1">
								<tr>
									<td class="label2" align="left" style="width: 90px"><span class="required">*</span><b>Client Type</b></td>
									<td class="label2" align="left" style="width: 25px"><b>Seq</b></td>
		   				    		<td class="label2" align="left" style="width: 90px"><b>Designation</b></td>
		   				    		<td class="label2" align="left" style="width: 105px"><b>Validity Date</b></td>
		   				    		<td class="label2" align="left" style="width: 105px"><b>Test Date</b></td>
		   				    		<td class="label2" align="left" style="width: 50px"><b>Follow Up No</b></td>
									<td class="label2" align="left" style="width: 50px"><b>Resolve</b></td>
		   				    		<td class="label2" align="left" style="width: 50px"><b>Paid Ind</b></td>
		   				    		<td class="label2" align="left" style="width: 50px"><b>New Test</b></td>
		   				    		<td class="label2" align="left" style="width: 50px"><b>Auto Order</b></td>
		   				    		<td class="label2" align="left" style="width: 50px"><b>Fld Comm</b></td>
								 </tr>
								 <tr>
								 	<td class="label2" align="left">
							          	<select name="clientTypeArr" id="clientTypeArr" class="label2">
							          		<option value="I">Insured</option>
							          		<option value="O">Owner</option>
							          	</select>
							          </td>
							          <td class="label2" align="left">
							          		<input type="text" name="seqArr" class="label2" value="" maxLength="10" style="width: 25px"/>
							          </td>
							          <td class="label2" align="left">
							          		<input type="text" name="designationArr" class="label2" value="" maxlength="10" style="width: 90px"/>
							          </td>
							          <td class="label2"><input type="text" name="validityDateArr" class="label2" value="<%=validityDate%>" onKeyUp="getKeyDate(event,this);" maxlength="9" style="width: 75px">&nbsp;
										<div id="doc_cal2" style="position:absolute;">
											<a href="#" onClick="popUpWindow('<%=contextPath%>/jsp/calendar.jsp?textbox=frm.validityDateArr[2]',290,155,'yes','yes','yes','no','no','no','no','3');">
											<img src="<%=contextPath%>/images/calendar.gif" height="17" width="19" border="0"> 
											</a>
										</div>
									  </td>
									  <td class="label2"><input type="text" name="testDateArr" class="label2" value="<%=testDate%>" onKeyUp="getKeyDate(event,this);" maxlength="9" style="width: 75px">&nbsp;
										<div id="doc_cal1" style="position:absolute;">
											<a href="#" onClick="popUpWindow('<%=contextPath%>/jsp/calendar.jsp?textbox=frm.testDateArr[2]',290,155,'yes','yes','yes','no','no','no','no','3');">
												<img src="<%=contextPath%>/images/calendar.gif" height="17" width="19" border="0"> 
											</a>
										</div>
									  </td>
							          <td class="label2" align="left">
							          	<input type="text" name="followUpNoArr" class="label2" value="" maxlength="1" style="width: 25px"/>
							          </td>
								 	<td class="label2" align="left">
							          	<select name="resolveArr" id="resolveArr" class="label2">
							          		<option value="N">No</option>
							          		<option value="Y">Yes</option>
							          	</select>
							          </td>
							          <td class="label2" align="left">
							          	<select name="paidIndArr" id="paidIndArr" class="label2">
							          		<option value="N">No</option>
							          		<option value="Y">Yes</option>
							          	</select>
							          </td>      
							          <td class="label2" align="left">
							          	<select name="newTestArr" id="newTestArr" class="label2">
							          		<option value="N">No</option>
							          		<option value="Y">Yes</option>
							          	</select>
							          </td>
							          <td class="label2" align="left">
							          	<select name="autoOrderArr" id="autoOrderArr" class="label2">
							          		<option value="N">No</option>
							          		<option value="Y">Yes</option>
							          	</select>
							          </td>
							          <td class="label2" align="left">
							          	<select name="fldCommArr" id="fldCommArr" class="label2">
							          		<option value="N">No</option>
							          		<option value="Y">Yes</option>
							          	</select>
							          </td>
								 </tr>
								</table>
								</td>
					    	</tr>
					    	
					    	<tr>
								<td>
								<table border="0" width="790" cellpadding="1" cellspacing="1">
								<tr>
		   				    		<td class="label2" align="left"><b>Comment</b></td>
								 </tr>
								 <tr>
										<td class="label2" align="left"><textarea class="label2" name="commentArr" cols="77" rows="2"></textarea>
										<input type="hidden" name="tdReqCodeDescArr" id="tdReqCodeDescArr" value="" /></td>
								 </tr>
								</table>
								</td>
					    	</tr>
					</table>
						
					</td>
				</tr>
				
				

				<tr>
					<td height="100%">
						<table border="0" width="790" cellpadding="1" cellspacing="1" class="listtable1">
							<tr>
								<html:errors/>
								<td>
								<table border="0" width="790" cellpadding="1" cellspacing="1">
								<tr>
									<td class="label2" align="left" style="width: 75px"><span class="required">*</span><b>Code</b></td>
								</tr>
								<tr>
									<td class="label2" align="left">
										<input type="hidden" name="reqCodeValue" value=""/>
										<input type="hidden" name="reqCodeArr" value=""/>
										<input type="hidden" name="reqCodeValueArr" value=""/>
										<input type="hidden" name="reqLevelArr" value=""/>
							          		<!-- <select name="selReqCode" onChange="changeLevelAndType();" class="label2"> -->
							          		<select name="selReqCode" class="label2" onChange="changeLevelAndFollowUpNo(this, 3);">
											<%
											it = requirementList.iterator();
											ctr = 0;
											while (it.hasNext()) {
												RequirementData data = (RequirementData) it.next();	
												
												if(ctr==0){
											%>
												<option value="">-</option>
											<%	
												}
												ctr++;
											%>
											<option value="+@1@<%=data.getLevel()%>-@1@+@2@<%=data.getFollowUpNum()%>-@2@+@3@<%=data.getReqtDesc()%>-@3@+@4@<%=data.getReqtCode()%>-@4@"><%=data.getReqtCode()%> - <%=data.getReqtDesc()%></option>
											<%}%>
											</select>
							          </td>
								</tr>
								</table>
								</td>
	   				    	</tr>
	   				    	
	   				    	<tr>
								<td>
								<table border="0" width="790" cellpadding="1" cellspacing="1">
								<tr>
									<td class="label2" align="left" style="width: 90px"><span class="required">*</span><b>Client Type</b></td>
									<td class="label2" align="left" style="width: 25px"><b>Seq</b></td>
		   				    		<td class="label2" align="left" style="width: 90px"><b>Designation</b></td>
		   				    		<td class="label2" align="left" style="width: 105px"><b>Validity Date</b></td>
		   				    		<td class="label2" align="left" style="width: 105px"><b>Test Date</b></td>
		   				    		<td class="label2" align="left" style="width: 50px"><b>Follow Up No</b></td>
									<td class="label2" align="left" style="width: 50px"><b>Resolve</b></td>
		   				    		<td class="label2" align="left" style="width: 50px"><b>Paid Ind</b></td>
		   				    		<td class="label2" align="left" style="width: 50px"><b>New Test</b></td>
		   				    		<td class="label2" align="left" style="width: 50px"><b>Auto Order</b></td>
		   				    		<td class="label2" align="left" style="width: 50px"><b>Fld Comm</b></td>
								 </tr>
								 <tr>
								 	<td class="label2" align="left">
							          	<select name="clientTypeArr" id="clientTypeArr" class="label2">
							          		<option value="I">Insured</option>
							          		<option value="O">Owner</option>
							          	</select>
							          </td>
							          <td class="label2" align="left">
							          		<input type="text" name="seqArr" class="label2" value="" maxLength="10" style="width: 25px"/>
							          </td>
							          <td class="label2" align="left">
							          		<input type="text" name="designationArr" class="label2" value="" maxlength="10" style="width: 90px"/>
							          </td>
							          <td class="label2"><input type="text" name="validityDateArr" class="label2" value="<%=validityDate%>" onKeyUp="getKeyDate(event,this);" maxlength="9" style="width: 75px">&nbsp;
										<div id="doc_cal2" style="position:absolute;">
											<a href="#" onClick="popUpWindow('<%=contextPath%>/jsp/calendar.jsp?textbox=frm.validityDateArr[3]',290,155,'yes','yes','yes','no','no','no','no','3');">
											<img src="<%=contextPath%>/images/calendar.gif" height="17" width="19" border="0"> 
											</a>
										</div>
									  </td>
									  <td class="label2"><input type="text" name="testDateArr" class="label2" value="<%=testDate%>" onKeyUp="getKeyDate(event,this);" maxlength="9" style="width: 75px">&nbsp;
										<div id="doc_cal1" style="position:absolute;">
											<a href="#" onClick="popUpWindow('<%=contextPath%>/jsp/calendar.jsp?textbox=frm.testDateArr[3]',290,155,'yes','yes','yes','no','no','no','no','3');">
												<img src="<%=contextPath%>/images/calendar.gif" height="17" width="19" border="0"> 
											</a>
										</div>
									  </td>
							          <td class="label2" align="left">
							          	<input type="text" name="followUpNoArr" class="label2" value="" maxlength="1" style="width: 25px"/>
							          </td>
								 	<td class="label2" align="left">
							          	<select name="resolveArr" id="resolveArr" class="label2">
							          		<option value="N">No</option>
							          		<option value="Y">Yes</option>
							          	</select>
							          </td>
							          <td class="label2" align="left">
							          	<select name="paidIndArr" id="paidIndArr" class="label2">
							          		<option value="N">No</option>
							          		<option value="Y">Yes</option>
							          	</select>
							          </td>      
							          <td class="label2" align="left">
							          	<select name="newTestArr" id="newTestArr" class="label2">
							          		<option value="N">No</option>
							          		<option value="Y">Yes</option>
							          	</select>
							          </td>
							          <td class="label2" align="left">
							          	<select name="autoOrderArr" id="autoOrderArr" class="label2">
							          		<option value="N">No</option>
							          		<option value="Y">Yes</option>
							          	</select>
							          </td>
							          <td class="label2" align="left">
							          	<select name="fldCommArr" id="fldCommArr" class="label2">
							          		<option value="N">No</option>
							          		<option value="Y">Yes</option>
							          	</select>
							          </td>
								 </tr>
								</table>
								</td>
					    	</tr>
					    	
					    	<tr>
								<td>
								<table border="0" width="790" cellpadding="1" cellspacing="1">
								<tr>
		   				    		<td class="label2" align="left"><b>Comment</b></td>
								 </tr>
								 <tr>
										<td class="label2" align="left"><textarea class="label2" name="commentArr" cols="77" rows="2"></textarea>
										<input type="hidden" name="tdReqCodeDescArr" id="tdReqCodeDescArr" value="" /></td>
								 </tr>
								</table>
								</td>
					    	</tr>
					</table>
						
					</td>
				</tr>
				
			</table>
		</td>
	</tr>
</table>

			<table>     
				<tr valign="top">
					<td></td> 
				</tr> 
				<tr id="create_cancel" class="show">
					<td align="left" colspan="12">&nbsp;&nbsp;
					<input type="button" class="button1" name="create" id="create" value=" Save " onClick="clickSaveCreate();" disabled>&nbsp;
					<input type="button" class="button1" name="cancel" value="Cancel" onClick="window.close()">
	  				</td>
				</tr>
			</table>

</form>
</body>
</html>