<%@ page language="java" import="java.util.*, com.slocpi.ium.ui.form.*, com.slocpi.ium.util.*, com.slocpi.ium.ui.util.*, com.slocpi.ium.data.*" buffer="1024kb"%>
<%@ taglib uri="/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/ium.tld" prefix="ium" %>
<%@ taglib uri="/struts-html.tld" prefix="html" %>
<%
/* Andre Ceasar Dacanay - css enhancement - start */
String iumCss = "";
String companyCode = "";
try {
	ResourceBundle rbWMSSP = ResourceBundle.getBundle("dbconfig");
	iumCss = rbWMSSP.getString("IUM_CSS_VERSION");
	companyCode = iumCss;
	iumCss = "_"+iumCss;
} catch (Exception e){}
if(iumCss.equals("_CP")){
	companyCode="";
}else if(iumCss.equals("_GF")){
	companyCode = companyCode+" ";
}
/* Andre Ceasar Dacanay - css enhancement - end */
%>
<%String contextPath = request.getContextPath();
String frPage = (String)request.getParameter("frPage");
if(frPage==null) frPage = " ";
StateHandler sessionHandler = new StateHandler();
Access access = new Access();

%>
<jsp:useBean id="userProfileForm" scope="request" class="com.slocpi.ium.ui.form.UserProfileForm" />

<html>
  <head>
    <title><%=companyCode%>Integrated Underwriting and Medical System</title>
    <link href="<%=contextPath%>/css/pw_main.css" rel="stylesheet" type="text/css">
    <link href="<%=contextPath%>/css/main<%=iumCss%>.css" rel="stylesheet" type="text/css">
    <link href="<%=contextPath%>/css/styleCommon.css" rel="stylesheet" type="text/css">
    <script language="JavaScript" src="<%=contextPath%>/js/dhtml_init.js"></script>
    <script language="JavaScript" src="<%=contextPath%>/js/navigation.js"></script>
	<script language="JavaScript" src="<%=contextPath%>/js/validation.js"></script>
  </head>

  <body leftMargin="0" topmargin="0" marginwidth="0" marginheight="0" onload="<%if(userProfileForm.getOldPassword().equals("")){ %> document.frm.oldPassword.focus();
		<%} else { if(userProfileForm.getNewPassword().equals("")) {%>  document.frm.newPassword.focus();
		<%} else { %> document.frm.confirmPassword.focus(); <% } } %>"  onKeyUp="checkEnter(event,'save');" >
    <table border="0" cellpadding="0" cellspacing="0" width="100%" height="100%">
      <tr vAlign="top"> 
            <table width="100%" cellpadding="3" cellspacing="5" border="0">
 
             <%if(userProfileForm.getOPassword() == " "){%>
              <tr>
              	<td class="label2"><b>You have successfully changed your password.</b></td>
              </tr>
              <%}%>
              <tr>
              <td colspan="2" class="error">
				<html:errors/>
			  </td>
			  </tr>					
              <tr>
                <td class="label2"><b>Change User Password</b>              
                </td>
              </tr>
              <tr valign="top"> 
                <td> 
                  <!--- START OF BODY -->
                  <form name="frm" action="<%=contextPath%>/changePassword.do" method="post">
                    <table border="0" cellpadding=10 cellspacing=0 bgcolor="" width="500">
                      <tr>
                        <td colspan="2" height="100%">
                          <table border="0" width="100%">
                            <tr>
                              <td class="label2"><b>User ID </b><td/>
                              <td class="label2"><%=userProfileForm.getUserId()%>
                              <input type="hidden" value="<%=userProfileForm.getUserId()%>" name="userId">
                              </td>
                            </tr>
                            <tr>

                              <td class="label2"><b>Old Password </b><td/>
                              <td><input type="password" class="label2" value="<%=userProfileForm.getOldPassword()%>" name="oldPassword" maxlength="16"></td>
                   				<input type="hidden" value="<%=userProfileForm.getPassword()%>" name="password">
                   				<input type="hidden" value="<%=userProfileForm.getOPassword()%>" name ="oPassword">
                 				<input type="hidden" value="<%=frPage%>" name ="frPage">
                 				
                 				<input type="hidden" value="<%=userProfileForm.getPassword1()%>" name ="password1">
                 				<input type="hidden" value="<%=userProfileForm.getPassword2()%>" name ="password2">
                 				<input type="hidden" value="<%=userProfileForm.getPassword3()%>" name ="password3">
                 				<input type="hidden" value="<%=userProfileForm.getPassword4()%>" name ="password4">
                 				<input type="hidden" value="<%=userProfileForm.getPassword5()%>" name ="password5">
                            </tr>
                            <tr>
                              <td class="label2"><b>New Password </b><td/>
                              <td><input type="password" class="label2" value="<%=userProfileForm.getNewPassword()%>" name="newPassword" maxlength="16"></td>
                            </tr>
                            <tr>
                              <td class="label2"><b>Confirm New Password </b><td/>
                              <td><input type="password" class="label2" value="<%=userProfileForm.getConfirmPassword()%>" name="confirmPassword" maxlength="16"></td>
                            </tr>
                            <tr><td colspan="4" align="right">&nbsp;</td></tr>
                            <tr>
                              <td colspan="6" align="right">
                              	<input type="hidden" value="submit" name="check">
                                <input type="button" value="Save" class="button1" name="save" onclick='document.frm.submit();'>&nbsp;
                                <input type="button" value="Cancel" class="button1"  onclick='window.close();'>
		                       </td>
                            </tr>
                          </table>
                        </td>
                      </tr>
                    </table>
                  </form>
                  <!--- END OF BODY -->
                </td>
              </tr>
            </table>
      </tr>
    </table>    
  
  </body>
  
</html>
