<%@ page language="java" import="java.util.*,com.slocpi.ium.data.*, com.slocpi.ium.ui.form.*, com.slocpi.ium.util.*, com.slocpi.ium.ui.util.*, java.text.* " buffer="12kb"%>
<%@ taglib uri="/struts-bean.tld" prefix="bean" %>

<!--
function rePaginate (page, actionUrl) {
	form = document.auditForm;
	if (form.pageNo != null) {
		form.pageNo.value = page;
	}
	form.search.value=1;
	form.action = actionUrl;
	form.submit();
}


function searchAuditTrail(){
	var form = document.auditForm;
	if (checkValidity() && checkDates()){
		form.search.value=1;
		form.action="listAuditTrail.do";
		form.submit();
	}else {
		return;
	}

}

function checkValidity(){
	var form = document.auditForm;
	
	if (form.startDate.value == null || form.startDate.value == ""){
		alert("<bean:message key="error.field.required" arg0="From Date" />");
		return false;
	}
	
	if (form.endDate.value == null || form.endDate.value == ""){
		alert("<bean:message key="error.field.required" arg0="To Date" />");
		return false;
	}
	
	if (!isValidDate(form.startDate.value)){
		alert("<bean:message key="error.field.format" arg0="From Date" arg1="date" arg2=""/>");
		form.startDate.select();
		form.startDate.focus();
		return false;
	}
	
	if (!isValidDate(form.endDate.value)){
		alert("<bean:message key="error.field.format" arg0="To Date" arg1="date" arg2=""/>");
		form.endDate.select();
		form.endDate.focus();
		return false;
	}
	
	return true;
}	

function checkDates(){
	var form = document.auditForm;
	
	if (isGreaterDate(form.endDate.value, form.startDate.value) || isDateEqual(form.endDate.value, form.startDate.value)){
		if (isGreaterDate(form.endDate.value,form.today.value)){
			alert("<bean:message key="error.field.earlierthanequalto" arg0="To Date" arg1="Date today" />");
			return false;
		} else {
			return true;
		}
	} else {
		alert("<bean:message key="error.field.laterthan" arg0="To Date" arg1="From Date" />");
		return false;
	} 
}

//date format is ddMMMyyyy; date1 == date2 returns true
function isDateEqual(d1, d2)	{
	var dd1   = d1.substring(0,2);
	var mm1   = getMonthInt(d1.substring(2,5).toUpperCase());
	var yyyy1 = parseInt(d1.substring(5,9)); 	
	
	var dd2   = parseInt(d2.substring(0,2));
	var mm2   = getMonthInt(d2.substring(2,5).toUpperCase());
	var yyyy2 = parseInt(d2.substring(5,9)); 	
	
	var date1 = new Date();
	date1.setDate(dd1);
	date1.setMonth(mm1);
	date1.setYear(yyyy1);
	
	var date2 = new Date();
	date2.setDate(dd2);
	date2.setMonth(mm2);
	date2.setYear(yyyy2);
	
    if ((date1-date2)==0) {
        return true;
    }
    else {
        return false;
    } 
}

//-->