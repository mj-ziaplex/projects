<%@ taglib uri="/struts-html.tld" prefix="html" %>
<%@ taglib uri="/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/struts-logic.tld" prefix="logic" %>
<%@page language="java" import="java.util.*" %>
<jsp:useBean id="workFunctionForm" scope="request" class="com.slocpi.ium.ui.form.WorkFunctionForm"/>
<%String contextPath = request.getContextPath(); %>
<%
/* Andre Ceasar Dacanay - css enhancement - start */
String iumCss = "";
String iumColorB = "";
String iumColorO = "";
String companyCode = "";
try {
	ResourceBundle rbWMSSP = ResourceBundle.getBundle("dbconfig");
	iumCss = rbWMSSP.getString("IUM_CSS_VERSION");
	companyCode = iumCss;
	iumCss = "_"+iumCss;
} catch (Exception e){}
if(iumCss.equals("_CP")){
	iumColorB="#2A4C7C";
	iumColorO = "#FFCB00";
	companyCode="";
}else if(iumCss.equals("_GF")){
	iumColorB="#303A77";
	iumColorO = "#303A77";
	companyCode = companyCode+" ";
}else{
	iumColorB="#2A4C7C";
	iumColorO = "#FFCB00";
}
/* Andre Ceasar Dacanay - css enhancement - end */
%>
<html>
	<head>
		<title><%=companyCode%>Integrated Underwriting and Medical System</title>
		<link href="<%=contextPath%>/css/pw_main.css"     rel="stylesheet" type="text/css">
		<link href="<%=contextPath%>/css/main<%=iumCss%>.css" rel="stylesheet" type="text/css">
		<link href="<%=contextPath%>/css/styleCommon.css" rel="stylesheet" type="text/css">
		<script language="JavaScript" src="<%=contextPath%>/js/dhtml_init.js"></script>
		<script language="JavaScript" src="<%=contextPath%>/js/navigation.js"></script>
		<script language="JavaScript" src="<%=contextPath%>/jsp/sl_left_nav.jsp"></script>
			
		<script language="javascript">
			function viewDetails(tC){
				var form = document.forms[0];
				form.templateCode.value=tC;
				form.actionType.value="update";
				gotoPage('frm', 'viewWorkFunctionDetail.do');
			}
			function create(){
				var form = document.forms[0];
				form.actionType.value="create";
				gotoPage('frm', '<%=contextPath%>/jsp/workFunctionDetail.jsp');
			}
		</script>
	</head>

	<BODY leftMargin=0 topmargin=0 marginwidth=0 marginheight=0>
		<table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%">
			<tr>
				<td width="100%" colspan="2">
					<jsp:include page="header.jsp" flush="true"/>
				</td>
			</tr>
			<tr>
				<td rowspan="2" bgcolor="<%=iumColorO%>" height="100%" width="20"><img src="<%=contextPath%>/images/spacer.gif" width="20"></td>
	            <td background="<%=contextPath%>/images/GradientGray.jpg" height="27" width="100%" valign="top">&nbsp;&nbsp;
       		    	<span class="main">Integrated Underwriting and Medical System</span>
	            </td>
       		</tr>
       		<tr>
           		<td width="100%" height="100%" valign="top">
           			<!-- BODY -->
			        <table width="100%" cellpadding="3" cellspacing="5" border="0">
			          	<tr> 
				            <td class="label2"><b>Work Functions</b></td>
				        </tr>
 			          	<tr valign="top"> 
			          		<td> 
								<!--- START OF BODY -->
								<form name="frm" method="post">
									<input type="hidden" name="actionType">
									<input type="hidden" name="templateCode">
									<table border="1" bordercolor="<%=iumColorB%>" cellpadding=10 cellspacing=0 bgcolor="" width="500" class="listtable1">
							        	<tr>
							        		<td colspan="2" height="100%">
												<table border="0" cellpadding="2" cellspacing="1" bgcolor=#FFFFFF style="border: 1px solid #D1D1D1;" class="listtable1">
										        	<tr class="headerrow1">
											        	<td width="20%">ACTION CODE</td>
											        	<td width="45%">DESCRIPTION</td>
										        	</tr>
												    <logic:iterate id="work" name="workFunctionForm" property="workFunctionData">        											    	
	        											<tr class="row1" style="cursor:hand" onclick="viewDetails('<bean:write name="work" property="templateCode"/>');">        											
													  		<td><bean:write name="work" property="templateCode"/>&nbsp;</td>
													  	    <td><bean:write name="work" property="templateDesc"/>&nbsp;</td>
														</tr>
												    </logic:iterate>
													<tr>
									    				<td class="headerrow4" colspan="10" align="left" width=100% >
											            	<table border="0" cellpadding="0" cellspacing="0"  width="100%" >
											            		<tr height="1"><td></td></tr>
											                	<tr>
																	<td class="headerrow4" width="100%" height="10" class="label2" valign="bottom" >
																		<a href="#" onmouseover="imageOver('pagnavfirst')" onmouseout="imageOut('pagnavfirst')">
																			<img src="<%=contextPath%>/images/pagnavfirst_off.gif" name="pagnavfirst" border="0" width="18" height="13"/></a>
																		<a href="#" onmouseover="imageOver('pagnavleft')" onmouseout="imageOut('pagnavleft')">
																			<img src="<%=contextPath%>/images/pagnavleft_off.gif" name="pagnavleft" border="0" width="18" height="13"/></a>
																			1,2,3,4,5,7,8,9,10...20...30...40
																		<a href="#" onmouseover="imageOver('pagnavright')" onmouseout="imageOut('pagnavright')">
																			<img src="<%=contextPath%>/images/pagnavright_off.gif" name="pagnavright" border="0" width="18" height="13"/></a>
																		<a href="#" onmouseover="imageOver('pagnavlast')" onmouseout="imageOut('pagnavlast')">
																			<img src="<%=contextPath%>/images/pagnavlast_off.gif" name="pagnavlast" border="0" width="18" height="13"/></a>&nbsp;&nbsp; 
																	</td>
										                    	</tr>
 									               			</table>
                										</td>
             										</tr>   	
             										<tr>
             											<td align="right" colspan="8">
												 			<input type="button" class="button1" value="Create" onclick="create();">
														</td>
													</tr>
												</table>
											</form>
											<!--- END OF BODY -->
										</td>
									</tr>
								</table>
	</body>
</html>