<%@ page language="java" import="java.util.*,com.slocpi.ium.data.*, com.slocpi.ium.ui.form.*, com.slocpi.ium.util.*, com.slocpi.ium.ui.util.*, java.text.* " buffer="12kb"%>
<%@ taglib uri="/struts-bean.tld" prefix="bean" %>

<!--

function putButton(numRecordsPerView, numRecords) {
  	if(document.all["buttons2"] != null){
	  	if (numRecordsPerView >= 10 && numRecords >=10) {
  			document.all["buttons2"].style.display = "block";
	  	}
	  	else{
  		    document.all["buttons2"].style.display = "none";
	  	}
	 }
}

function rePaginate (page, actionUrl) {
	form = document.exceptionForm;
	if (form.pageNo != null) {
		form.pageNo.value = page;
	}
	form.search.value=1;
	form.action = actionUrl;
	form.submit();
}


function searchException(){
	var form = document.exceptionForm;
	
	if ((form.startDate.value == null || form.startDate.value == "") && (form.endDate.value == null || form.endDate.value == "")){
		var ok = confirm("This will display the whole database and may take a long time. Continue with search?");
		if (ok){
			executeSearch();
		} else {
			return;
		}
	} else if (checkValidity() && checkDates()){
		executeSearch();
	}else {
		return;
	}
}

function executeSearch(){
	var form = document.exceptionForm;
	form.search.value=1;
	form.action="listException.do";
	form.submit();
}

function viewExceptionDetail(){
	var form = document.exceptionForm;
	var selected = countChecked();
	
	if (selected <= 0){
		alert("Select an exception to view");
	} else if (selected == 1){
		form.search.value=1;
		form.selectedViewDetails.value = 1;
		form.action = "listException.do";
		form.submit();
	} else {
		alert("Select one exception to view details");
	}
	
}


function countChecked(){
	var frm = document.exceptionForm;
	var selected =0;
	if (frm.idSelected == null){
		return selected;
	} else {
		var length = frm.idSelected.length;
		if (length==null){
			if (frm.idSelected.checked==true){
				selected++;
			} 
		}
		else {
			for (var i=0; i<length; i++){
				if (frm.idSelected[i].checked==true){
					selected++;
				}
			}
		}
	}
	return selected;
}

function checkValidity(){
	var form = document.exceptionForm;
	
	if (form.startDate.value == null || form.startDate.value == ""){
		alert("Please enter start date");
		return false;
	}
	
	if (form.endDate.value == null || form.endDate.value == ""){
		alert("Please enter end date");
		return false;
	}
	
	if (!isValidDate(form.startDate.value)){
		alert("<bean:message key="error.field.format" arg0="From Date" arg1="date" arg2=""/>");
		form.startDate.select();
		form.startDate.focus();
		return false;
	}
	
	if (!isValidDate(form.endDate.value)){
		alert("<bean:message key="error.field.format" arg0="To Date" arg1="date" arg2=""/>");
		form.endDate.select();
		form.endDate.focus();
		return false;
	}
	
	return true;
}	

function checkDates(){
	var form = document.exceptionForm;
	
	if (isGreaterDate(form.endDate.value, form.startDate.value) || isDateEqual(form.endDate.value, form.startDate.value)){
		if (isGreaterDate(form.endDate.value,form.today.value)){
			alert("<bean:message key="error.field.earlierthanequalto" arg0="To Date" arg1="Date today" />");
			return false;
		} else {
			return true;
		}
	} else {
		alert("<bean:message key="error.field.laterthan" arg0="To Date" arg1="From Date" />");
		return false;
	} 
}

//date format is ddMMMyyyy; date1 == date2 returns true
function isDateEqual(d1, d2)	{
	var dd1   = d1.substring(0,2);
	var mm1   = getMonthInt(d1.substring(2,5).toUpperCase());
	var yyyy1 = parseInt(d1.substring(5,9)); 	
	
	var dd2   = parseInt(d2.substring(0,2));
	var mm2   = getMonthInt(d2.substring(2,5).toUpperCase());
	var yyyy2 = parseInt(d2.substring(5,9)); 	
	
	var date1 = new Date();
	date1.setDate(dd1);
	date1.setMonth(mm1);
	date1.setYear(yyyy1);
	
	var date2 = new Date();
	date2.setDate(dd2);
	date2.setMonth(mm2);
	date2.setYear(yyyy2);
	
    if ((date1-date2)==0) {
        return true;
    }
    else {
        return false;
    } 
}

//-->