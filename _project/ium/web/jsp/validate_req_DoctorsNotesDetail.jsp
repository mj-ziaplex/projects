<%@ page language="java" import="com.slocpi.ium.util.*, com.slocpi.ium.ui.util.*, com.slocpi.ium.data.*" buffer="12kb"%>
<%@ taglib uri="/struts-bean.tld" prefix="bean" %>

<!--
function haveSelectedStatus(){
	var form = document.requestForm;
	var status = form.requestStatus.value;
	if (status == null || status ==""){
		alert("<bean:message key="error.field.requiredselection" arg0="status"/>");
		form.requestStatus.focus();
		return false;
	} else {
		return true;
	}
}

function saveMedicalNote(){
	var form = document.requestForm;
	if (isEmpty(form.notes.value)){
		alert("<bean:message key="error.field.required" arg0="Inputs and Discussions"/>");
	}
	<%
	StateHandler sh = new StateHandler();
	UserData ud = sh.getUserData(request);
	UserProfileData profile = ud.getProfile();
	String role = profile.getRole();
	Roles userRoles = new Roles();
	%>
	<% //if (role.equals(IUMConstants.ROLES_UNDERWRITER)) { %>
	<% if (userRoles.getRoles(ud.getRoles(),IUMConstants.ROLES_UNDERWRITER) || userRoles.getRoles(ud.getRoles(),IUMConstants.ROLES_UNDERWRITER_SUPERVISOR)) { %>

	else if (noSelection(form.doctor.value)){
		alert("<bean:message key="error.field.required" arg0="The doctor to notify"/>");
		form.doctor.focus();
	}    
	<% } %>
	else {
		form.action = "createDoctorsNotes.do";
		form.submit();
	}
	return;
}


function setDefaultFocus() {
	var form = document.requestForm;
	//form.notes.focus();
}


function rePaginate (page, actionUrl) {
	form = document.requestForm;
	if (form.pageNo != null) {
		form.pageNo.value = page;	
	}
	gotoPage("requestForm", actionUrl);
}

//-->