<%@ page language="java" buffer="2048kb"%>
<!-- JAVA UTILS -->
<%@ page import="java.util.ArrayList, java.util.HashMap, java.util.ResourceBundle" %>
<!-- IUM UTILS -->
<%@ page import="com.slocpi.ium.util.IUMConstants" %>
<%@ page import="com.slocpi.ium.ui.util.Access, com.slocpi.ium.ui.util.Page, com.slocpi.ium.ui.util.Roles, com.slocpi.ium.ui.util.StateHandler" %>
<!-- IUM POJO -->
<%@ page import="com.slocpi.ium.ui.form.CDSDetailForm, com.slocpi.ium.ui.form.CDSMortalityRatingForm, com.slocpi.ium.ui.form.ImpairmentForm, com.slocpi.ium.ui.form.UWAnalysisForm, com.slocpi.ium.ui.form.PolicyCoverageForm, com.slocpi.ium.ui.form.CDSSummaryForm" %>
<!-- IUM DATA -->
<%@ page import="com.slocpi.ium.data.UserData, com.slocpi.ium.data.UserProfileData" %>
<!-- IUM UNDERWRITER -->
<%@ page import="com.slocpi.ium.underwriter.AssessmentRequest, com.slocpi.ium.underwriter.PolicyRequirements" %>

<!-- IUM TAGLIBS -->
<%@ taglib uri="/ium.tld" prefix="ium"%>

<%
//WMS session expiration = close browser
ResourceBundle rb1 = ResourceBundle.getBundle("wmsconfig");
String wmsContextPath1 = rb1.getString("wms_context_path");
String titlePrefix = "";

long start = System.currentTimeMillis();
long end;
long elapse;

end = System.currentTimeMillis();
elapse = (end - start);


	/* Andre Ceasar Dacanay - css enhancement - start */
	String iumCss = "";
	String iumBgColor = "";
	String iumColor = "";
	String iumColorO = "";
	String companyCode = "";
	try {
		ResourceBundle rbWMSSP = ResourceBundle.getBundle("dbconfig");
		iumCss = rbWMSSP.getString("IUM_CSS_VERSION");
		companyCode = iumCss;
		iumCss = "_" + iumCss;
		
	} catch (Exception e) {}
	
	if(iumCss.equals("_CP")){
		iumBgColor = "bgcolor=\"#2a4c7c\"";
		iumColorO = "#FFCB00";
		companyCode="";
	}else if(iumCss.equals("_GF")){
		iumBgColor = "bgcolor=\"#303A77\"";
		iumColor = "style=\"color:#303A77;\"";
		iumColorO = "#303A77";
		companyCode = companyCode+" ";
	}else{
		iumBgColor = "bgcolor=\"#2a4c7c\";";
		iumColorO = "#FFCB00";
	}
	/* Andre Ceasar Dacanay - css enhancement - end */
	
	String session_id = request.getAttribute("session_id").toString();
	String contextPath = request.getContextPath();
	String savedSection = (request.getSession().getAttribute("savedSection_" + session_id) != null) ? (String) request.getSession().getAttribute("savedSection_" + session_id): "";
	String LABEL_CLASS = "";
	String detailForm = "detailForm_" + session_id;
	
	CDSDetailForm arForm = (CDSDetailForm) session.getAttribute("detailForm_" + session_id);
	String arFormRefNo = arForm != null && arForm.getRefNo() != null ? arForm.getRefNo(): "";
	String arFormOwnerClientNo = arForm != null	&& arForm.getOwnerClientNo() != null ? arForm.getOwnerClientNo() : "";
	String arFormRequestStatus = arForm != null	&& arForm.getRequestStatus() != null ? arForm.getRequestStatus() : "";
	String arFormInsuredClientNo = arForm != null	&& arForm.getInsuredClientNo() != null ? arForm.getInsuredClientNo() : "";

	if(contextPath.equalsIgnoreCase(wmsContextPath1)){if(iumCss.equals("_GF")){titlePrefix = "GF ";}}
	
	if(titlePrefix.equals("")){	if(iumCss.equals("_GF")){titlePrefix = companyCode+" ";}}
	
	end = System.currentTimeMillis();
	elapse = (end - start);
	
%>

<style type="text/css">
	.opaqueLayer{
		display:none;
		position:absolute;
		top:0px;left:0px;
		opacity:0.6;
		filter:alpha(opacity=60);
		background-color: #000000;
        z-Index:1000;
    }
	
	.questionLayer {
		position:absolute;
        top:0px;
        left:0px;
        width:350px;
        height:200px;
        display:none;
        z-Index:1001;
        border:2px solid black;
        background-color:#FFFFFF;
        text-align:center;
        vertical-align:middle;
        padding:10px;
    }
</style>

<script type="text/javascript">
	
            function getBrowserHeight() {
                var intH = 0;
                var intW = 0;
               
                if(typeof window.innerWidth  == 'number' ) {
                   intH = window.innerHeight;
                   intW = window.innerWidth;
                   //alert('number '+intH+' '+intW);
                } 
                else if(document.documentElement && (document.documentElement.clientWidth || document.documentElement.clientHeight)) {
                    intH = document.documentElement.clientHeight;
                    intW = document.documentElement.clientWidth;
                    //alert('documentElement clientWidth '+intH+' '+intW);
                }
                else if(document.body && (document.body.clientWidth || document.body.clientHeight)) {
                    intH = document.body.clientHeight;
                    intW = document.body.clientWidth;
                    //alert('body clientWidth '+intH+' '+intW);
                }

				try{
                   //alert('number '+document.body.scrollHeight );
                   intH = document.body.scrollHeight;
                   intW = document.body.scrollWidth;
				}catch(e){
					try{
						intH = document.body.clientHeight;
                    	intW = document.body.clientWidth;
                    }catch(e){}
				}
				
                return { width: parseInt(intW), height: parseInt(intH) };
            }  

            function setLayerPosition() {
                var shadow = document.getElementById("shadow");
                var question = document.getElementById("question");

                var bws = getBrowserHeight();
                shadow.style.width = bws.width + "px";
                shadow.style.height = bws.height + "px";

                question.style.left = parseInt((bws.width - 350) / 2);
                question.style.top = parseInt((bws.height - 200) / 2);

                shadow = null;
                question = null;
            }
            
            function setLayerPosition2() {
                var shadow = document.getElementById("shadow");
                var question = document.getElementById("question");

                var bws = getBrowserHeight();
                shadow.style.width = bws.width + "px";
                shadow.style.height = bws.height + "px";

                question.style.left = parseInt((bws.width - 350) / 2);
                question.style.top = parseInt((bws.height - 200) / 2) + 250;

                shadow = null;
                question = null;
            }
            function showLayer() {
                setLayerPosition();

                var shadow = document.getElementById("shadow");
                var question = document.getElementById("question");
 
                shadow.style.display = "block"; 
                question.style.display = "block";

                shadow = null;
                question = null;
                
                var codespan = document.getElementById("codespan");
                codespan.style.display ="none";
                
                var relationshipspan = document.getElementById("relationshipspan");
                relationshipspan.style.display ="none";
                
                var impairmentOriginspan = document.getElementById("impairmentOriginspan");
                impairmentOriginspan.style.display ="none";

                var actionCodespan = document.getElementById("actionCodespan");
                actionCodespan.style.display ="none";
                
                var confirmspan = document.getElementById("confirmspan");
                confirmspan.style.display ="none";
                
                var statusTospan = document.getElementById("statusTospan");
                statusTospan.style.display ="none";
                
                var reqCodespan = document.getElementById("reqCodespan");
                reqCodespan.style.display ="none";
            }
            
            function hideLayer() {
                var shadow = document.getElementById("shadow");
                var question = document.getElementById("question");
 
                shadow.style.display = "none"; 
                question.style.display = "none";

                shadow = null;
                question = null; 
                
                var codespan = document.getElementById("codespan");
                codespan.style.display ="";
                
                var relationshipspan = document.getElementById("relationshipspan");
                relationshipspan.style.display ="";
                
                var impairmentOriginspan = document.getElementById("impairmentOriginspan");
                impairmentOriginspan.style.display ="";

                var actionCodespan = document.getElementById("actionCodespan");
                actionCodespan.style.display ="";
                
                var confirmspan = document.getElementById("confirmspan");
                confirmspan.style.display ="";
                
                var statusTospan = document.getElementById("statusTospan");
                statusTospan.style.display ="none";
                
                var reqCodespan = document.getElementById("reqCodespan");
                reqCodespan.style.display ="";
            }

            window.onresize = setLayerPosition;

if (parent.document.forms[0]) {
    //parent.window.location = "<%=contextPath%>/jsp/koReqMedLabDetail.jsp";
	parent.document.forms[0].action = 'viewKOReqMedLabDetail.do';
	parent.document.forms[0].submit();
}
function expandReq()
{
	if(document.getElementById('reqDiv').style.display == ""){
		document.getElementById('reqDiv').style.display = "none"
	} else {
		document.getElementById('reqDiv').style.display = ""
	}
}

function findPosX(obj) {
      var curleft = 0;
      if (obj.offsetParent) {
            while (obj.offsetParent) {
                  curleft += obj.offsetLeft
                  obj = obj.offsetParent;
            }
      }
      else if (obj.x)
            curleft += obj.x;
      return curleft - 100;
}

function findPosY(obj) {
      var curtop = 0;
      if (obj.offsetParent) {
            while (obj.offsetParent) {
                  curtop += obj.offsetTop
                  obj = obj.offsetParent;
            }
      }
      else if (obj.y)
            curtop += obj.y;
      return curtop;
}

function goto(objID) {
      window.scrollTo(findPosX(document.getElementById(objID)),findPosY(document.getElementById(objID)));
}

function isArray(obj) {

   var bool=obj instanceof Array;
   return bool;
   
}
// Update Agent Details from Ingenium to IUM - Andre Ceasar Dacanay - start
function updateAgent(){
	var arFormRefNo = document.getElementById('arFormRefNo').value;
	var polSuffix = document.getElementById('polSuffix').value;
	var agentCode = document.getElementById('agentCode').value;
	var branchCode = document.getElementById('branchCode').value;
	var userId = document.getElementById('userId').value;
	
	var url = '<%=contextPath%>/updateAgentINGToIUM.do?sessionId=<%=session_id%>&arFormRefNo='+arFormRefNo+'&polSuffix='+polSuffix+'&agentCode='+agentCode+'&branchCode='+branchCode+'&userId='+userId;
	
	popUpWindow(url,375,150,'no','no','no','no','no','no','no','0','400','150');
}
// Update Agent Details from Ingenium to IUM - Andre Ceasar Dacanay - end
function viewCDSDetail()
{

for (var i=0; i < document.forms[0].idSelected.length; i++)
   {
   

   if (document.forms[0].idSelected[i].checked)
      {
      document.forms[0].selectedId.value= document.forms[0].idSelected[i].value;
       
      }
   }
   
    if(document.forms[0].selectedId.value=="")
      document.forms[0].selectedId.value=document.forms[0].idSelected.value;
 
   window.location = 'viewCDSDetail.do?idSelected=' + document.forms[0].selectedId.value+"&savedSection="+ document.requestForm.savedSection.value +"&session_id="+ '<%=session_id%>';
}

	function createImpairments(){
		try{
			showSaveCancel(); 
		}catch(e){}
		try{
			hideCreateMaintain(); 
		}catch(e){}
		try{
			enableImpairmentFields(); 
		}catch(e){
			showCreateMaintain();
			hideSaveCancel();
		}
		try{
			setDefaultFocus();
		}catch(e){}
	}
</script>

<%
	// This routine queries the access rights of the users (RCRIVERA)
	//
	// --- START ---
	end = System.currentTimeMillis();
	elapse = (end - start);


	Access access = new Access();
	StateHandler sessionHandler = new StateHandler();
	UserData ud = sessionHandler.getUserData(request);
	UserProfileData profile = ud.getProfile();
	String userId = profile.getUserId();

	HashMap userAccess = sessionHandler.getUserAccess(request);
	if (!access.hasAccess(userAccess, IUMConstants.PAGE_ASSESSMENT_DETAILS, IUMConstants.ACC_INQUIRE)) {
		response.sendRedirect("noAccessPage.jsp");
	}

	//req
	String CREATE_KO = "";
	String MAINTAIN_KO = "";
	String DELETE_KO = "";
	String EXECUTE_KO = "";

	if (!access.hasAccess(userAccess, IUMConstants.PAGE_REQUIREMENTS_KO, IUMConstants.ACC_CREATE)) {
		CREATE_KO = "DISABLED";
	}
	
	if (!access.hasAccess(userAccess, IUMConstants.PAGE_REQUIREMENTS_KO, IUMConstants.ACC_MAINTAIN)) {
		MAINTAIN_KO = "DISABLED";
	}
	
	if (!access.hasAccess(userAccess, IUMConstants.PAGE_REQUIREMENTS_KO, IUMConstants.ACC_DELETE)) {
		DELETE_KO = "DISABLED";
	}
	
	if (!access.hasAccess(userAccess, IUMConstants.PAGE_REQUIREMENTS_KO, IUMConstants.ACC_EXECUTE)) {
		EXECUTE_KO = "DISABLED";
	}

	//req
	String CREATE_ACCESS = "";
	String MAINTAIN_ACCESS = "";
	String DELETE_ACCESS = "";
	String EXECUTE_ACCESS = "";
	String CREATE_REQUEST = "";
	String CREATE_UW = "";
	String MAINTAIN_UW = "";
	String DELETE_UW = "";
	String EXECUTE_UW = "";

	// CREATE ASSESSMENT REQUEST
	if (!access.hasAccess(userAccess, IUMConstants.PAGE_ASSESSMENT_DETAILS, IUMConstants.ACC_CREATE)) {
		CREATE_ACCESS = "DISABLED";
	}
	// MAINTAIN ASSESSMENT REQUEST
	if (!access.hasAccess(userAccess, IUMConstants.PAGE_ASSESSMENT_DETAILS, IUMConstants.ACC_MAINTAIN)) {
		MAINTAIN_ACCESS = "DISABLED";
	}
	// -- NOT APPLICABLE --
	if (!access.hasAccess(userAccess, IUMConstants.PAGE_ASSESSMENT_DETAILS, IUMConstants.ACC_DELETE)) {
		DELETE_ACCESS = "DISABLED";
	}
	// UPDATE LOCATION
	if (!access.hasAccess(userAccess, IUMConstants.PAGE_ASSESSMENT_DETAILS, IUMConstants.ACC_EXECUTE)) {
		EXECUTE_ACCESS = "DISABLED";
	}
	// CREATE IMPAIRMENT
	if (!access.hasAccess(userAccess, IUMConstants.PAGE_UW_ASSESSMENT, IUMConstants.ACC_CREATE)) {
		CREATE_UW = "DISABLED";
	}
	// MAINTAIN IMPAIRMENT
	if (!access.hasAccess(userAccess, IUMConstants.PAGE_UW_ASSESSMENT, IUMConstants.ACC_MAINTAIN)) {
		MAINTAIN_UW = "DISABLED";
	}
	// DELETE IMPAIRMENT
	if (!access.hasAccess(userAccess, IUMConstants.PAGE_UW_ASSESSMENT, IUMConstants.ACC_DELETE)) {
		DELETE_UW = "DISABLED";
	}
	// -- NOT APPLICABLE --
	if (!access.hasAccess(userAccess, IUMConstants.PAGE_UW_ASSESSMENT, IUMConstants.ACC_EXECUTE)) {
		EXECUTE_UW = "DISABLED";
	}

	// req
	boolean INQUIRE_REQUEST = true;
	boolean INQUIRE_CDS = true;
	boolean INQUIRE_KOREQRMT = true;
	boolean INQUIRE_UWASSESS = true;
	boolean INQUIRE_DOCTORNOTES = true;
	
	if (!access.hasAccess(userAccess, IUMConstants.PAGE_REQUEST_DETAILS, IUMConstants.ACC_INQUIRE)) {
		INQUIRE_REQUEST = false;
	}
	if (!access.hasAccess(userAccess, IUMConstants.PAGE_CDS, IUMConstants.ACC_INQUIRE)) {
		INQUIRE_CDS = false;
	}
	if (!access.hasAccess(userAccess, IUMConstants.PAGE_REQUIREMENTS_KO, IUMConstants.ACC_INQUIRE)) {
		INQUIRE_KOREQRMT = false;
	}
	if (!access.hasAccess(userAccess, IUMConstants.PAGE_UW_ASSESSMENT, IUMConstants.ACC_INQUIRE)) {
		INQUIRE_UWASSESS = false;
	}
	if (!access.hasAccess(userAccess, IUMConstants.PAGE_DOCTORS_NOTES, IUMConstants.ACC_INQUIRE)) {
		INQUIRE_DOCTORNOTES = false;
	}
	// --- END ---
	
	// Get MAIN CDS DETAIL OBJECT
	CDSDetailForm actionForm = (CDSDetailForm) request.getAttribute("detailForm");
%>
<html>
<head>
<title>
	<%=titlePrefix%>IUM&nbsp;&nbsp;
	<%=actionForm.getRefNo()%>
	<%=actionForm.getPolicySuffix()%>&nbsp;&nbsp;
	<%=actionForm.getInsuredTitle()%>&nbsp;
	<%=actionForm.getInsuredFirstName()%>&nbsp;
	<%=actionForm.getInsuredMiddleName()%>&nbsp;
	<%=actionForm.getInsuredLastName()%>&nbsp;
	<%=actionForm.getInsuredSuffix()%>
</title>
<!-- CSS FILES -->
<link href="<%=contextPath%>/css/main<%=iumCss%>.css" rel="stylesheet" type="text/css">
<link href="<%=contextPath%>/css/datePicker.css" rel="stylesheet"type="text/css">
<!-- JS FILES -->
<script type="text/javascript" src="<%=contextPath%>/js/navigation-min.js"></script>
<script type="text/javascript" src="<%=contextPath%>/js/validation-min.js"></script>
<script type="text/javascript" src="<%=contextPath%>/js/datePicker.js"></script>
<script type="text/javascript" src="<%=contextPath%>/jsp/validate_req_RequestDetail.jsp"></script>
<script type="text/javascript" src="<%=contextPath%>/jsp/validate_req_UWAssessmentDetail.jsp"></script>
<script type="text/javascript" src="<%=contextPath%>/jsp/validate_req_KOReqMedLabDetail.jsp"></script>

<script type="text/javascript">var userId = "<%=userId%>";</script>
</head>

<%

end = System.currentTimeMillis();
elapse = (end - start);


	String branchName = (String)request.getAttribute("branchName")!=null? (String)request.getAttribute("branchName") : "";
	String agentName = (String)request.getAttribute("agentName")!=null? (String)request.getAttribute("agentName") : "";
	String lobDesc = (String)request.getAttribute("lobDesc")!=null? (String)request.getAttribute("lobDesc") : "";
	String underwriterName = request.getAttribute("underwriterName")!=null? (String)request.getAttribute("underwriterName") : "";
	
	String lob = actionForm.getLob();
	String transmitInd = actionForm.getTransmitIndicator();
	String autoSettleInd = actionForm.getAutoSettleIndicator();
	String clientToSearch = actionForm.getClientToSearch();

	//current status
	String currentStatus = request.getParameter("currentStatus");
	if (currentStatus == null) {
		currentStatus = actionForm.getRequestStatus();
	}
	
	String isSuccessChangeStatus = request.getParameter("isSuccessChangeStatus");
	if (isSuccessChangeStatus != null
			&& isSuccessChangeStatus.equals("true")) {
		currentStatus = actionForm.getRequestStatus();
	}
	
	//selected status
	String selectedStatus = request.getParameter("requestStatus");
	if (selectedStatus == null) {
		selectedStatus = actionForm.getRequestStatus();
	}
	
	//current assigned to
	String currentAssignedTo = actionForm.getAssignedTo();
	if (currentAssignedTo == null
			|| (currentAssignedTo != null && currentAssignedTo.equals(""))) {
		currentAssignedTo = request.getParameter("currentAssignedTo");
	}
	
	ImpairmentForm impForm = (ImpairmentForm) request.getAttribute("maintainThisImpairment");
	String role = (String) request.getAttribute("role");

	boolean impairmentMaintain = false;
	int isRefresh = 0;
	String impCode = "";
	String impRelationship = "";
	String impOrigin = "";
	String impDate = "";
	String impHeight = "";
	String impWeight = "";
	String impBP = "";
	String impConfirmationCode = "";
	String impairmentSelected = "";
	String impAction = "";
	
	if (impForm != null) {
		
		isRefresh = 1;
		impCode = impForm.getCode();
		impRelationship = impForm.getRelationship();
		impOrigin = impForm.getImpairmentOrigin();
		impDate = impForm.getImpairmentDate();
		impAction = impForm.getActionCode();
		
		if (!impForm.getHeightInFeet().equals("")) {
			impHeight = impForm.getHeightInFeet() + "'"+ impForm.getHeightInInches() + "&quot";
		}
		
		impWeight = impForm.getWeight();
		impBP = impForm.getBloodPressure();
		impConfirmationCode = impForm.getConfirm();
		impairmentSelected = impForm.getImpairmentId();
		impairmentMaintain = true;
	}

	Roles userRoles = new Roles(); // Class that checks Roles of user contained in the array list
	boolean hasAccess = false;
	
	//disable impairment buttons if role is not underwriter
	if ((userRoles.getRoles(ud.getRoles(), IUMConstants.ROLES_UNDERWRITER))
			|| (userRoles.getRoles(ud.getRoles(), IUMConstants.ROLES_UNDERWRITER_SUPERVISOR))) {
		
		hasAccess = true;
	} else {
		
		CREATE_UW = "DISABLED";
		MAINTAIN_UW = "DISABLED";
		DELETE_UW = "DISABLED";
	}
	
end = System.currentTimeMillis();
elapse = (end - start);

	
	//disable buttons (except create) if current status is an end status
	AssessmentRequest ar = new AssessmentRequest();
	
	if (ar.isEndStatus(lob, Long.parseLong(currentStatus))) {
		
		MAINTAIN_ACCESS = "DISABLED";
		EXECUTE_ACCESS = "DISABLED";
		CREATE_UW = "DISABLED";
		MAINTAIN_UW = "DISABLED";
		DELETE_UW = "DISABLED";
		//req
		CREATE_KO = "DISABLED";
		MAINTAIN_KO = "DISABLED";
		DELETE_KO = "DISABLED";
		EXECUTE_KO = "DISABLED";
		//req
	}

end = System.currentTimeMillis();
elapse = (end - start);

	
	
	boolean isMaintain = false;
	String reqIsMaintain = request.getParameter("isMaintain");

	if ((reqIsMaintain != null) && (reqIsMaintain.equals("true"))) {
		isMaintain = true;
	}
	
	//filter for request status
	String lob_status = lob + "," + currentStatus;

	//WMS resize
	ResourceBundle rb = ResourceBundle.getBundle("wmsconfig");
	String wmsContextPath = rb.getString("wms_context_path");

	String TABLE_WIDTH = "";
	String TEXTAREA_COL = "";
	String GRAY_WIDTH = "";
	String SUB_TABLE_WIDTH = "";
	String DIV_WIDTH = "";
	String TABLE_TD = "";
	String FRAME_WIDTH = "";
	String COMMENTBOX_WIDTH = "";
	String DATE_MAX_WIDTH = "";
	
	if (contextPath.equalsIgnoreCase(wmsContextPath)) {
		
		TABLE_WIDTH = "500";
		TEXTAREA_COL = "50";
		GRAY_WIDTH = "500";
		LABEL_CLASS = "label7";
		SUB_TABLE_WIDTH = "500";
		DIV_WIDTH = "width: 500px;overflow: auto;";
		FRAME_WIDTH = "500";
		TABLE_TD = "0";
		COMMENTBOX_WIDTH = "100";
		DATE_MAX_WIDTH = "width:70";
	} else {
		
		TABLE_WIDTH = "80%";
		TEXTAREA_COL = "50";
		GRAY_WIDTH = "100%";
		LABEL_CLASS = "label2";
		SUB_TABLE_WIDTH = "50%";
		DIV_WIDTH = "width: 400px;";
		FRAME_WIDTH = "400";
		TABLE_TD = "15";
		COMMENTBOX_WIDTH = "130";
		DATE_MAX_WIDTH = "width:100";
	}

	if (LABEL_CLASS.equals("")) {
		LABEL_CLASS = "label2";
	}
	
end = System.currentTimeMillis();
elapse = (end - start);


	PolicyRequirements polReq = new PolicyRequirements();
	boolean hasOrderReqt = polReq.hasOrderRequirements(actionForm.getRefNo());
	String sendButtonControl = "DISABLED";

	if (hasOrderReqt) {
		sendButtonControl = "ENABLED";
	}
		
end = System.currentTimeMillis();
elapse = (end - start);

	
%>

<body leftmargin="0" topmargin="0"
	onLoad="hideSaveCancel(); hideSaveCancel2(); hideMaintainCancel2(); showCreateMaintain2();
	<%if (isMaintain) {%> 
		enableRequestDetails(); enableTabDetails('uw'); setDateForwarded(); initializeClientData('<%=clientToSearch%>');  
	<%} else {%> 
		disableTabDetails('uw'); 
	<%}%>
	
	<%if(!contextPath.equalsIgnoreCase(wmsContextPath)){
	  	if (Long.parseLong(currentStatus) == IUMConstants.STATUS_UNDERGOING_ASSESSMENT) {%>
			showCreateMaintain(); disableImpairmentFields(); clearImpairmentFields(); 
		<%} else {%> 
			hideCreateMaintain(); 
		<%}%>
	<%} else {%>
		enableTabDetails('uw'); hideSaveCancel(); showCreateMaintain(); disableImpairmentFields(); clearImpairmentFields(); showCreateMaintain2(); 
	<%}%>
	
	<%if (!savedSection.equals("")) {%>
		goto('<%=savedSection%>');
	<%	
		request.getSession().setAttribute("savedSection_"+session_id,"");
	}%>
	
	<%if(impairmentMaintain){%>
	    hideCreateMaintain();
	    showSaveCancel();	
	<%} else {%>
	   showCreateMaintain();
	   hideSaveCancel();	
	<%}%>"
	onUnload="closePopUpWin(); closePopUpWindow();">

<form name="requestForm" method="post">

<div id="shadow" class="opaqueLayer"></div>
<div id="question" class="questionLayer" onclick="hideLayer();"><br />
	<br />
	<br />
	Please wait... <br />
	<br />
	<br />
</div>
<input type="hidden" name="polSuffix" id="polSuffix" value="<%=actionForm.getPolicySuffix()%>" />

<div><jsp:include page="header.jsp" flush="true" /></div>

<div style="width: 926px;">
	<table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%">
		<tr>
			<td class="label2" rowspan="2" bgcolor="<%=iumColorO%>" height="100%" width="1%">
				<img src="<%=contextPath%>/images/spacer.gif" width="20">
			</td>
			<td class="label2" background="<%=contextPath%>/images/GradientGray.jpg" height="27" width="99%" valign="top">
				&nbsp;&nbsp;
				<span class="main">
					<a NAME="back">Integrated Underwriting and Medical System</a>
				</span>
			</td>
		</tr>
	</table>

	<table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%">
		<tr>
			<td class="label2" rowspan="2" bgcolor="<%=iumColorO%>" height="100%"
				width="1%">
				<img src="<%=contextPath%>/images/spacer.gif" width="20">
			</td>
			<td>&nbsp;&nbsp;&nbsp;</td>
			<td class="label2" width="99%" valign="top" style="padding-left: 0.65;">
				&nbsp;&nbsp;
				<!-- *************************************************** -->
				<!-- *************** MAIN CONTENT ********************** -->
				<!-- *************************************************** -->
				<div id="MainContent" style="left:0.65;">
					<div id="IndLifePolInf" style="position:relative; top:0; width: 700px">
						<!-- hidden field to store current status -->
						<input type="hidden" name="currentStatus" value="<%=currentStatus%>"> 
						<input type="hidden" name="remarks" value="<%=actionForm.getRemarks()%>"> 
						<input type="hidden" name="isMaintain" value="<%=isMaintain%>">
						<input type="hidden" name="isSuccessChangeStatus" value="false">
						<input type="hidden" name="currentAssignedTo" value="<%=currentAssignedTo%>">
						<input type="hidden" name="impairmentToBeEdited" value="<%=impairmentSelected%>">
						<input type="hidden" name="selectedMaintain" value="0">
						<input type="hidden" name='selectedId'>
						<input type="hidden" name="refreshFlag" value="<%=isRefresh%>">
						<input type="hidden" name="reqPage" value="uwAssessmentDetail">
						<input type="hidden" name="reqPage" value="koReqMedLabDetail">
						<%
							String _sourceSystem = actionForm.getSourceSystem();
							if (_sourceSystem == null) {
								_sourceSystem = "";
							}
				
							String _agentCode = actionForm.getAgentCode();
							if (_agentCode == null) {
								_agentCode = "";
							}
				
							String _lob = actionForm.getLob();
							if (_lob == null) {
								_lob = "";
							}
				
							String _branchCode = actionForm.getBranchCode();
							if (_branchCode == null) {
								_branchCode = "";
							}
				
							String _assignedTo = currentAssignedTo; //actionForm.getAssignedTo();
							if (_assignedTo == null) {
								_assignedTo = "";
							}
				
							String _location = actionForm.getLocation();
							if (_location == null) {
								_location = "";
							}
				
							String _underwriter = actionForm.getUnderwriter();
							if (_underwriter == null) {
								_underwriter = "";
							}
				
							String _insuredClientNo = actionForm.getInsuredClientNo();
							if (_insuredClientNo == null) {
								_insuredClientNo = "";
							}
				
							String _insuredSex = actionForm.getInsuredSex();
							if (_insuredSex == null) {
								_insuredSex = "";
							}
				
							String _ownerClientNo = actionForm.getOwnerClientNo();
							if (_ownerClientNo == null) {
								_ownerClientNo = "";
							}
				
							String _ownerSex = actionForm.getOwnerSex();
							if (_ownerSex == null) {
								_ownerSex = "";
							}
				
							String _status = currentStatus; //actionForm.getRequestStatus();
							if (_status == null) {
								_status = "";
							}
				
							String _selectedStatus = selectedStatus;
							if (_selectedStatus == null) {
								_selectedStatus = "";
							}
						%>
			
						<jsp:include page="requestDetail_include_top.jsp" flush="true">
							<jsp:param name="sourceSystem" value="<%=_sourceSystem%>" />
							<jsp:param name="lob" value="<%=_lob%>" />
							<jsp:param name="lobDesc" value="<%=lobDesc%>" />
							<jsp:param name="agentCode" value="<%=_agentCode%>" />
							<jsp:param name="agentName" value="<%=agentName%>" />
							<jsp:param name="branchCode" value="<%=_branchCode%>" />
							<jsp:param name="branchName" value="<%=branchName%>" />
							<jsp:param name="assignedTo" value="<%=_assignedTo%>" />
							<jsp:param name="location" value="<%=_location%>" />
							<jsp:param name="underwriter" value="<%=_underwriter%>" />
							<jsp:param name="underwriterName" value="<%=underwriterName%>" />
							<jsp:param name="status" value="<%=_status%>" />
							<jsp:param name="selectedStatus" value="<%=_selectedStatus%>" />
							<jsp:param name="insuredClientNo" value="<%=_insuredClientNo%>" />
							<jsp:param name="insuredSex" value="<%=_insuredSex%>" />
							<jsp:param name="ownerClientNo" value="<%=_ownerClientNo%>" />
							<jsp:param name="ownerSex" value="<%=_ownerSex%>" />
						</jsp:include>
					</div>
					<!-- *************************************************** -->
					<!-- *************** END MAIN CONTENT ********************** -->
					<!-- *************************************************** -->
					
					<br />
					<div id="IndLifePolInfLinks" style="position:relative; top:0; left:0.65; width: 700px">
						<table border="0" cellpadding="3" cellspacing="0" width="100%">
							<tr>
								<td class="<%=LABEL_CLASS%>" style="text-align:center ;font-size: 12px">
									<a HREF="#cds"> CDS Details</a> &nbsp;&nbsp;&nbsp;&nbsp;
									<a HREF="#ko">KO Details</a> &nbsp;&nbsp;&nbsp;&nbsp;
									<a HREF="#uw">UW Assessment</a> &nbsp;&nbsp;&nbsp;&nbsp;
									<!-- <a HREF="#req">Requirements</a> &nbsp;&nbsp;&nbsp;&nbsp;  -->
									<a HREF="#" onclick="showPolicyRequirementsPage();">Requirements</a>
								</td>
							</tr>
						</table>
					</div>
					<br />
					<br />
					
					<!-- *************************************************** -->
					<!-- *************** CDS CONTENT ********************** -->
					<!-- *************************************************** -->
					<div id="CDSSection" style="position:relative; top:0; left:0.65; width: 700px">
						<h3><a name="cds" <%=iumColor%>>CDS Section</a></h3>
					</div>
					<br />
						
					<div id="Summary" style="border: solid;border-width:1;position:relative; top:0; left:0.65; width: 700px">
						<div id="SummaryId">
							<table border="0" cellpadding="3" cellspacing="0" width="100%">
								<!-- <tr bgcolor="#2A4C7C"> -->
								<tr <%=iumBgColor%>>
									<td class="<%=LABEL_CLASS%>" colspan="2" style="color: #FFFFFF"><b>SUMMARY</b></td>
								</tr>
							</table>
						</div>
					
						<div id="SummarySubTitle">
							<table border="0" cellpadding="3" cellspacing="0" width="100%">
								<!-- <tr bgcolor="#2A4C7C"> -->
								<tr <%=iumBgColor%>>
									<td class="<%=LABEL_CLASS%>" colspan="6" style="color: #FFFFFF">
									TOTAL EXISTING (IN FORCE), PENDING/SIMULTANEOUS, APPLIED FOR
									</td>
								</tr>
							</table>
						</div>
							
						<%
						CDSSummaryForm summary = (CDSSummaryForm) request.getAttribute("cdsSummaryForm");
						%>
						<div id="SummaryTable1" style="float: left; width: 70%; padding: 10px;">
							<table border="0" cellpadding="2" cellspacing="1" bgcolor=#FFFFFF style="border: 1px solid #D1D1D1;" width="100%">
								<tr>
									<td class="headerrow1" width="60%">&nbsp;</td>
									<td class="headerrow1" width="20%"><b>BASIC + RTB</b></td>
									<td class="headerrow1" width="20%"><b>ADB/ADDD</b></td>
								</tr>
								<tr>
									<td class="row1"><b>APPLIED FOR</b></td>
									<td class="row1" align="right"><%=summary.getAppliedForBr()%></td>
									<td class="row1" align="right"><%=summary.getAppliedForAd()%></td>
								</tr>
								<tr>
									<td class="row2"><b>PENDING / SIMULTANEOUS SUN LIFE</b></td>
									<td class="row2" align="right"><%=summary.getPendingAmountBr()%></td>
									<td class="row2" align="right"><%=summary.getPendingAmountAd()%></td>
								</tr>
								<tr>
									<td class="row1" colspan="3"><b>EXISTING SUN LIFE</b></td>
								</tr>
								<tr>
									<td class="row2"><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IN FORCE</b></td>
									<td class="row2" align="right"><%=summary.getExistingInForceBr()%></td>
									<td class="row2" align="right"><%=summary.getExistingInForceAd()%></td>
								</tr>
								<tr>
									<td class="row1"><b>TOTAL SUN LIFE</b></td>
									<td class="row1" align="right"><%=summary.getTotalSunLifeBr()%></td>
									<td class="row1" align="right"><%=summary.getTotalSunLifeAd()%></td>
								</tr>
								<tr>
									<td class="row2"><b>OTHER COMPANIES</b></td>
									<td class="row2" align="right"><%=summary.getOtherCompaniesBr()%></td>
									<td class="row2" align="right"><%=summary.getOtherCompaniesAd()%></td>
								</tr>
								<tr>
									<td class="row1"><b>TOTAL SUN LIFE AND OTHER COMPANIES</b></td>
									<td class="row1" align="right"><%=summary.getTotalSunLifeOtherCompaniesBr()%></td>
									<td class="row1" align="right"><%=summary.getTotalSunLifeOtherCompaniesAd()%></td>
								</tr>
							</table>
						</div>
							
						<div id="SummaryTable2" style="float: left; width: 30%; padding: 10px; padding-left: 0px">
							<table border="0" cellpadding="2" cellspacing="1" bgcolor=#FFFFFF style="border: 1px solid #D1D1D1;" width="100%">
								<tr>
									<td class="headerrow1" colspan="2">
										<b>EXISTING IN FORCE RIDERS</b>
									</td>
								</tr>
								<tr>
									<td class="row1" width="40%"><b>CCR</b></td>
									<td class="row1" width="60%" align="right"><%=summary.getTotalCCRCoverage()%></td>
								</tr>
								<tr>
									<td class="row2"><b>APDB</b></td>
									<td class="row2" align="right"><%=summary.getTotalAPDBCoverage()%></td>
								</tr>
								<tr>
									<td class="row1"><b>HIB</b></td>
									<td class="row1" align="right"><%=summary.getTotalHIBCoverage()%></td>
								</tr>
								<tr>
									<td class="row2"><b>FB/F&MB</b></td>
									<td class="row2" align="right"><%=summary.getTotalFBBMBCoverage()%></td>
								</tr>
							</table>
						</div>
				
						<div id="SummaryTotalReinsuredAmount" style="padding-bottom: 10px">
							<table border="0" cellpadding="3" cellspacing="0" width="100%">
								<tr>
									<td class="<%=LABEL_CLASS%>" colspan="6">
										&nbsp;&nbsp;&nbsp;
										<b>
										TOTAL REINSURED AMOUNT (IN FORCE): 
										<%=summary.getTotalReinsuredAmount()%>
										</b>
									</td>
								</tr>
								<tr>
									<td class="<%=LABEL_CLASS%>" colspan="6">
										&nbsp;&nbsp;&nbsp;
										<b>TOTAL ADB/ADD REINSURED AMOUNT:
										<%=summary.getTotalAdbReinsuredAmount()%>
										</b>
									</td>
								</tr>
							</table>
						</div>
					</div>
					<br />
					<!-- *************************************************** -->
					<!-- *************** END CDS CONTENT ********************** -->
					<!-- *************************************************** -->
				
						
					<!-- *************************************************** -->
					<!-- *************** MORTALITY CONTENT ********************** -->
					<!-- *************************************************** -->
					<div id="MortalityRating" style="border: solid;border-width:1;position:relative; top:0; left:0.65; width: 700px">
						<div id="MortalityRatingId">
							<table border="0" cellpadding="3" cellspacing="0" width="100%">
								<!-- <tr bgcolor="#2A4C7C"> -->
								<tr <%=iumBgColor%>>
									<td class="<%=LABEL_CLASS%>" colspan="2" style="color: #FFFFFF">
										<b>MORTALITY RATING</b>
									</td>
								</tr>
							</table>
						</div>
						
						<%
						int j = 0;
						String align;
						
						// ViewCDSDetailAction set the attribute detailForm
						CDSDetailForm cdf = (CDSDetailForm) request.getAttribute("detailForm");
						ArrayList al = cdf.getMortalityRatingList();
				
						// Mortality rating variables for the Insured
						CDSMortalityRatingForm cdsmrform1 = (CDSMortalityRatingForm) al.get(0);
						String iClientType = (cdsmrform1.getClientType() != null) ? cdsmrform1.getClientType().trim() : "";
						String iHeight = (cdsmrform1.getHeight() != null) ? cdsmrform1.getHeight().trim() : "";
						String iWeight = (cdsmrform1.getWeight() != null) ? cdsmrform1.getWeight().trim() : "";
						String iBasic = (cdsmrform1.getBasic() != null) ? cdsmrform1.getBasic().trim() : "";
						String iCCR = (cdsmrform1.getCCR() != null) ? cdsmrform1.getCCR().trim() : "";
						String iAPDB = (cdsmrform1.getAPDB() != null) ? cdsmrform1.getAPDB().trim() : "";
						String iSmokingBasic = (cdsmrform1.getSmokingBasic() != null) ? cdsmrform1.getSmokingBasic().trim() : "";
						String iSmokingCCR = (cdsmrform1.getSmokingCCR() != null) ? cdsmrform1.getSmokingCCR().trim() : "";
						String iSmokingAPDB = (cdsmrform1.getSmokingAPDB() != null) ? cdsmrform1.getSmokingAPDB().trim() : "";
						String iFamilyHistoryBasic = (cdsmrform1.getFamilyHistoryBasic() != null) ? cdsmrform1.getFamilyHistoryBasic().trim() : "";
						String iFamilyHistoryCCR = (cdsmrform1.getFamilyHistoryCCR() != null) ? cdsmrform1.getFamilyHistoryCCR().trim() : "";
						String iFamilyHistoryAPDB = (cdsmrform1.getFamilyHistoryAPDB() != null) ? cdsmrform1.getFamilyHistoryAPDB().trim() : "";
						String iTotalAssestMortBasic = (cdsmrform1.getTotalAssestMortBasic() != null) ? cdsmrform1.getTotalAssestMortBasic().trim() : "";
						String iTotalAssestMortCCR = (cdsmrform1.getTotalAssestMortCCR() != null) ? cdsmrform1.getTotalAssestMortCCR().trim() : "";
						String iTotalAssestMortAPDB = (cdsmrform1.getTotalAssestMortAPDB() != null) ? cdsmrform1.getTotalAssestMortAPDB().trim() : "";
				
						String oClientType = "";
						String oHeight = "";
						String oWeight = "";
						String oBasic = "";
						String oCCR = "";
						String oAPDB = "";
						String oSmokingBasic = "";
						String oSmokingCCR = "";
						String oSmokingAPDB = "";
						String oFamilyHistoryBasic = "";
						String oFamilyHistoryCCR = "";
						String oFamilyHistoryAPDB = "";
						String oTotalAssestMortBasic = "";
						String oTotalAssestMortCCR = "";
						String oTotalAssestMortAPDB = "";
				
						// Mortality rating variables for the Owner
						if (al.size() < 2) {
								
							oClientType = iClientType;
							oHeight = iHeight;
							oWeight = iWeight;
							oBasic = iBasic;
							oCCR = iCCR;
							oAPDB = iAPDB;
							oSmokingBasic = iSmokingBasic;
							oSmokingCCR = iSmokingCCR;
							oSmokingAPDB = iSmokingAPDB;
							oFamilyHistoryBasic = iFamilyHistoryBasic;
							oFamilyHistoryCCR = iFamilyHistoryCCR;
							oFamilyHistoryAPDB = iFamilyHistoryAPDB;
							oTotalAssestMortBasic = iTotalAssestMortBasic;
							oTotalAssestMortCCR = iTotalAssestMortCCR;
							oTotalAssestMortAPDB = iTotalAssestMortAPDB;
						} else {
								
							CDSMortalityRatingForm cdsmrform2 = (CDSMortalityRatingForm) al.get(1);
							oClientType = (cdsmrform2.getClientType() != null) ? cdsmrform2.getClientType().trim() : "";
							oHeight = (cdsmrform2.getHeight() != null) ? cdsmrform2.getHeight().trim() : "";
							oWeight = (cdsmrform2.getWeight() != null) ? cdsmrform2.getWeight().trim() : "";
							oBasic = (cdsmrform2.getBasic() != null) ? cdsmrform2.getBasic().trim() : "";
							oCCR = (cdsmrform2.getCCR() != null) ? cdsmrform2.getCCR().trim() : "";
							oAPDB = (cdsmrform2.getAPDB() != null) ? cdsmrform2.getAPDB().trim() : "";
							oSmokingBasic = (cdsmrform2.getSmokingBasic() != null) ? cdsmrform2.getSmokingBasic().trim() : "";
							oSmokingCCR = (cdsmrform2.getSmokingCCR() != null) ? cdsmrform2.getSmokingCCR().trim() : "";
							oSmokingAPDB = (cdsmrform2.getSmokingAPDB() != null) ? cdsmrform2.getSmokingAPDB().trim() : "";
							oFamilyHistoryBasic = (cdsmrform2.getFamilyHistoryBasic() != null) ? cdsmrform2.getFamilyHistoryBasic().trim() : "";
							oFamilyHistoryCCR = (cdsmrform2.getFamilyHistoryCCR() != null) ? cdsmrform2.getFamilyHistoryCCR().trim() : "";
							oFamilyHistoryAPDB = (cdsmrform2.getFamilyHistoryAPDB() != null) ? cdsmrform2.getFamilyHistoryAPDB().trim() : "";
							oTotalAssestMortBasic = (cdsmrform2.getTotalAssestMortBasic() != null) ? cdsmrform2.getTotalAssestMortBasic().trim() : "";
							oTotalAssestMortCCR = (cdsmrform2.getTotalAssestMortCCR() != null) ? cdsmrform2.getTotalAssestMortCCR().trim() : "";
							oTotalAssestMortAPDB = (cdsmrform2.getTotalAssestMortAPDB() != null) ? cdsmrform2.getTotalAssestMortAPDB().trim() : "";
						}
						%>
						
						<div id="MortalityRatingInsured" style="float: left; width: 50%; padding: 10px;">
							<table border="0" cellpadding="2" cellspacing="1" bgcolor=#FFFFFF style="border: 1px solid #D1D1D1;" width="100%" id="<%=iClientType%>">
								<tr>
									<td class="headerrow1"><b><%=iClientType%></b></td>
									<td class="headerrow1"><b>BASIC</b></td>
									<td class="headerrow1"><b>CCR</b></td>
									<td class="headerrow1"><b>ADPB</b></td>
								</tr>
								<tr>
									<td class="row1" width="55%"><b> HEIGHT: <%=iHeight%>&nbsp;&nbsp;<br /> WEIGHT: <%=iWeight%> lbs.</b></td>
									<td class="row1" width="15%"><%=iBasic%></td>
									<td class="row1" width="15%"><%=iCCR%></td>
									<td class="row1" width="15%"><%=iAPDB%></td>
								</tr>
								<tr>
									<td class="row2"><b>SMOKING</b></td>
									<td class="row2"><%=iSmokingBasic%></td>
									<td class="row2"><%=iSmokingCCR%></td>
									<td class="row2"><%=iSmokingAPDB%></td>
								</tr>
								<tr>
									<td class="row1"><b>FAMILY HISTORY</b></td>
									<td class="row1"><%=iFamilyHistoryBasic%></td>
									<td class="row1"><%=iFamilyHistoryCCR%></td>
									<td class="row1"><%=iFamilyHistoryAPDB%></td>
								</tr>
								<tr>
									<td class="row2" width="300"><b>TOTAL ASSESSED-MORT%</b></td>
									<td class="row2"><%=iTotalAssestMortBasic%></td>
									<td class="row2"><%=iTotalAssestMortCCR%></td>
									<td class="row2"><%=iTotalAssestMortAPDB%></td>
								</tr>
							</table>
						</div>
		
						<div id="MortalityRatingOwner" style="float: left; width: 50%; padding: 10px; padding-left: 0px">
							<table border="0" cellpadding="2" cellspacing="1" bgcolor=#FFFFFF style="border: 1px solid #D1D1D1;" width="100%" id="<%=oClientType%>">
								<tr>
									<td class="headerrow1"><b><%=oClientType%></b></td>
									<td class="headerrow1"><b>BASIC</b></td>
									<td class="headerrow1"><b>CCR</b></td>
									<td class="headerrow1"><b>ADPB</b></td>
								</tr>
								<tr>
									<td class="row1" width="55%"><b> HEIGHT: <%=oHeight%>&nbsp;&nbsp;<br />
									WEIGHT: <%=oWeight%> lbs.</b></td>
									<td class="row1" width="15%"><%=oBasic%></td>
									<td class="row1" width="15%"><%=oCCR%></td>
									<td class="row1" width="15%"><%=oAPDB%></td>
								</tr>
								<tr>
									<td class="row2"><b>SMOKING</b></td>
									<td class="row2"><%=oSmokingBasic%></td>
									<td class="row2"><%=oSmokingCCR%></td>
									<td class="row2"><%=oSmokingAPDB%></td>
								</tr>
								<tr>
									<td class="row1"><b>FAMILY HISTORY</b></td>
									<td class="row1"><%=oFamilyHistoryBasic%></td>
									<td class="row1"><%=oFamilyHistoryCCR%></td>
									<td class="row1"><%=oFamilyHistoryAPDB%></td>
								</tr>
								<tr>
									<td class="row2" width="300"><b>TOTAL ASSESSED-MORT%</b></td>
									<td class="row2"><%=oTotalAssestMortBasic%></td>
									<td class="row2"><%=oTotalAssestMortCCR%></td>
									<td class="row2"><%=oTotalAssestMortAPDB%></td>
								</tr>
							</table>
						</div>
						<% j++; %>
					</div>
					<br />
					<!-- *************************************************** -->
					<!-- *************** END MORTALITY CONTENT ********************** -->
					<!-- *************************************************** -->
						
						
					<!-- *************************************************** -->
					<!-- *************** POLICY CONTENT ********************** -->
					<!-- *************************************************** -->
					<div id="PolicyCoverageInformation" style="border: solid;border-width:1;position:relative; top:0; left:0.65; width: 100%">
						<div id="PolicyCoverageInformationId">
							<table border="0" cellpadding="3" cellspacing="0" width="100%">
								<!-- <tr bgcolor="#2A4C7C"> -->
								<tr <%=iumBgColor%>>
									<td class="<%=LABEL_CLASS%>" colspan="2" style="color: #FFFFFF">
										<b>POLICY COVERAGE INFORMATION</b>
									</td>
								</tr>
							</table>
						</div>
				
						<%
						ArrayList policyCoverageList = actionForm.getPolicyCoverageList();
						String divHeight = "100%";
							
						if (policyCoverageList != null) {
							divHeight = (policyCoverageList.size() <= 20) ? divHeight : "414px";
						}
						%>
				
						<div id="PolicyCoverageInformationDetails" style="padding: 10px; width: 100%; overflow:auto; height: <%=divHeight%>;">
							<table border="0" cellpadding="2" cellspacing="1" bgcolor=#FFFFFF style="border: 1px solid #D1D1D1;" width="100%" id="policyCoverage">
								<tr>
									<td class="headerrow1">POLICY#</td>
									<td class="headerrow1">COV</td>
									<td class="headerrow1">REL CD</td>
									<td class="headerrow1">ISSUE DATE</td>
									<td class="headerrow1">COV STAT</td>
									<td class="headerrow1">SMKR CD</td>
									<td class="headerrow1">PLAN</td>
									<td class="headerrow1">MED IND</td>
									<td class="headerrow1">FACE AMT</td>
									<td class="headerrow1">DEC</td>
									<td class="headerrow1">BASIC+RTB REINSRD AMT</td>
									<td class="headerrow1">ADB/ADDD REINSURED AMOUNT</td>
									<td class="headerrow1">REINS TYPE</td>
									<td class="headerrow1">MORT MORB RATING</td>
									<td class="headerrow1">TEMP FLAT EXTRA RATING</td>
									<td class="headerrow1">PERM FLAT EXTRA RATING</td>
									<td class="headerrow1">REINSTMT DATE</td>
								</tr>
				
								<%
								int i = 0;
								String tr_class = "";
								
								PolicyCoverageForm pol;
								
								for (int xx = 0; xx < policyCoverageList.size(); xx++) {
									if (xx % 2 == 0) {
										tr_class = "row1";
									} else {
										tr_class = "row2";
									}
									
									pol = (PolicyCoverageForm) policyCoverageList.get(xx);
								%>
									<tr>
										<td class="<%=tr_class%>" align="left">
											<%=pol.getPolicyNo()%>
											<%=pol.getPolicySuffix()%>
										</td>
										<td class="<%=tr_class%>" align="center"><%=pol.getCoverageNo()%></td>
										<td class="<%=tr_class%>" align="center"><%=pol.getRelationship()%></td>
										<td class="<%=tr_class%>" align="left"><%=pol.getIssueDate()%></td>
										<td class="<%=tr_class%>" align="center"><%=pol.getCoverageStatus()%></td>
										<td class="<%=tr_class%>" align="center"><%=pol.getSmokerInd()%></td>
										<td class="<%=tr_class%>" align="left"><%=pol.getPlanCode()%></td>
										<td class="<%=tr_class%>" align="center"><%=pol.getMedicalInd()%></td>
										<td class="<%=tr_class%>" align="right"><%=pol.getFaceAmount()%></td>
										<td class="<%=tr_class%>" align="center"><%=pol.getDecision()%></td>
										<td class="<%=tr_class%>" align="right"><%=pol.getReinsuredAmount()%></td>
										<td class="<%=tr_class%>" align="center"><%=pol.getAdbReinsuredAmount()%></td>
										<td class="<%=tr_class%>" align="center"><%=pol.getReinsuranceType()%></td>
										<td class="<%=tr_class%>" align="center"><%=pol.getRelCvgMeFct()%></td>
										<td class="<%=tr_class%>" align="center"><%=pol.getRelCvgFeUpremAmt()%></td>
										<td class="<%=tr_class%>" align="center"><%=pol.getRelCvgFePermAmt()%></td>
										<td class="<%=tr_class%>" align="center"><%=pol.getReinstatementDate()%></td>
									</tr>
									<% i++; %>
								<%}%>
								
								<%if (i == 0) {%>
								<tr>
									<td class="label2" colspan="14"></td>
								</tr>
								<%}%>
							</table>
						</div>
					</div>
					<br />
					<!-- *************************************************** -->
					<!-- *************** END POLICY CONTENT ********************** -->
					<!-- *************************************************** -->
						
						
					<!-- *************************************************** -->
					<!-- *************** LINKS ********************** -->
					<!-- *************************************************** -->
					<div id="CDSSectionLinks" style="position:relative; top:0; left:0.65; width: 700px">
						<table border="0" cellpadding="3" cellspacing="0" width="100%">
							<tr>
								<td class="<%=LABEL_CLASS%>" style="text-align: center; font-size: 12px">
								<a HREF="#ko">KO Details</a> &nbsp;&nbsp;
								<a HREF="#uw">UW Assessment</a> &nbsp;&nbsp;
								<a HREF="#" onclick="showPolicyRequirementsPage();">Requirements</a> &nbsp;&nbsp;
								<a HREF="#back">Back to Top</a></td>
							</tr>
						</table>
					</div>
					<br />
					<br />
					<!-- *************************************************** -->
					<!-- *************** END LINKS ********************** -->
					<!-- *************************************************** -->
						
						
					<!-- *************************************************** -->
					<!-- *************** KO CONTENT ********************** -->
					<!-- *************************************************** -->
					<div id="KOSection" style="position:relative; top:0; left:0.65; width: 700px">
						<h3><a name="ko" <%=iumColor%>>KO Section</a></h3>
					</div>
					
					<div id="KickOutReasons" style="border: solid;border-width:1;position:relative; top:0; left:0.65; width: 700px">
						<div id="KickOutReasonsId">
							<table border="0" cellpadding="3" cellspacing="0" width="100%">
								<!-- <tr bgcolor="#2A4C7C"> -->
								<tr <%=iumBgColor%>>
									<td class="<%=LABEL_CLASS%>" colspan="2" style="color: #FFFFFF">
										<b>CLEAR CASE KICK-OUT REASONS</b>
									</td>
								</tr>
							</table>
						</div>
							
						<div id="KickOutReasonsDetails" style="padding: 10px; width: 100%">
							<jsp:include page="kickOutReasons.jsp" flush="true">
								<jsp:param name="koReasons" value="<%=actionForm%>" />
							</jsp:include>
						</div>
					</div>
					<br />
					<!-- *************************************************** -->
					<!-- *************** END KO CONTENT ********************** -->
					<!-- *************************************************** -->
						
						
					<!-- *************************************************** -->
					<!-- *************** LINKS ********************** -->
					<!-- *************************************************** -->
					<div id="KOSectionLinks" style="position:relative; top:0; left:0.65; width: 700px">
						<table border="0" cellpadding="3" cellspacing="0" width="100%">
							<tr>
								<td class="<%=LABEL_CLASS%>" style="text-align: center; font-size: 12px">
								<a HREF="#cds">CDS Details</a> &nbsp;&nbsp; <a HREF="#uw">UW Assessment</a> &nbsp;&nbsp;
								<a HREF="#" onclick="showPolicyRequirementsPage();">Requirements</a> &nbsp;&nbsp;
								<a HREF="#back">Back to Top</a></td>
							</tr>
						</table>
					</div>
					<br />
					<br />
					<!-- *************************************************** -->
					<!-- *************** END LINKS ********************** -->
					<!-- *************************************************** -->
						
						
					<!-- *************************************************** -->
					<!-- *************** UWASSESSMENT CONTENT ********************** -->
					<!-- *************************************************** -->
					<div id="UWAssessmentSection" style="position:relative; top:0; left:0.65; width: 700px">
						<h3><a name="uw" <%=iumColor%>>UW Assessment Section</a></h3>
					</div>
				
					<div id="UWAssessment" style="border: solid;border-width:1;position:relative; top:0; left:0; width: 680px">
						<div id="UWAssessmentPostedInfo" style="padding: 10px;">
							<table border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" style="border: 1px solid #D1D1D1;" class="listtable1" ID="Table1" width="100%">
								<%
								ArrayList tmp = actionForm.getListOfAnalysis();
								UWAnalysisForm analysis;
								
								for (int xx = 0; xx < tmp.size(); xx++) {
									
									analysis = (UWAnalysisForm) tmp.get(xx);
								%>
									<tr>
										<td class="headerrow1" width="55%">
											<div align="left">Posted By: <%=analysis.getPostedBy()%></div>
										</td>
										<td class="headerrow1">
											<div align="right">Posted On: <%=analysis.getPostedDate()%></div>
										</td>
									</tr>
									<tr>
										<td class="row1" colspan="2"><%=analysis.getAnalysis()%></td>
									</tr>
								<%}%>
							</table>
						</div>
							
						<!-- UWAssessmentPostedInfo -->
						<div id="UWAssessmentAnalysisRemarks" style="padding-left: 10px; padding-top: 10px;">
							<div style="float: left; width: 99%; ">
								<table border="0" cellpadding="2" cellspacing="1" bgcolor=#FFFFFF width="100%">
									<tr>
										<td class="<%=LABEL_CLASS%>"><b>Analysis</b>&nbsp;<%=impOrigin%>&nbsp;<%=impAction%>&nbsp;</td>
									</tr>
									<tr>
										<td class="<%=LABEL_CLASS%>">
											<textarea rows="10" cols="110" class="<%=LABEL_CLASS%>" name="UWAnalysis"></textarea>
										</td>
									</tr>
								</table>
								
								<table border="0" cellpadding="2" cellspacing="1" bgcolor=#FFFFFF width="100%">
									<tr>
										<td class="<%=LABEL_CLASS%>"><b>Final Decision<b></td>
										<td class="<%=LABEL_CLASS%>"><b>Remarks</b></td>
									</tr>
									<tr>
										<td class="<%=LABEL_CLASS%>">
											<textarea rows="10" onchange="flagChangeFD();" cols="53" class="<%=LABEL_CLASS%>" name="UWFinalDecision"><%=actionForm.getUWFinalDecision()%></textarea>
										</td>
										<td class="<%=LABEL_CLASS%>">
											<textarea rows="10" cols="53" class="<%=LABEL_CLASS%>" name="UWRemarks"><%=actionForm.getUWRemarks()%></textarea>
										</td>
									</tr>
								</table>
							</div>
						</div>
						
						<!-- UWAssessmentAnalysisRemarks -->
						<script type="text/javascript">
							var changedFD = 0;
											
							function flagChangeFD() {
								changedFD = 1;
							}
											
							function checkNewFD() {
												
								if(changedFD == 1) {
									//appendFinalDecisionToAnalysis()
									changedFD = 0;
								}
							}
											
							function appendFinalDecisionToAnalysis() {
								
								var temp1 = document.requestForm.UWAnalysis;
								var temp2 = document.requestForm.UWFinalDecision;
								temp1.value = temp1.value + temp2.value;
							}
											
							function setSavedSection(val) {
								document.requestForm.savedSection.value = val;
							}
						</script>
						
						<div style="padding-left: 10px; padding-top: 13px;">
							<!-- save uw assessment here -->
							<input class="button1" type="button" name="btnSave" value="Save" onClick="setSavedSection('uw'); checkNewFD(); saveUWAssessment('<%=session_id%>');">
							<input type="hidden" name="savedSection" value="" />
							
							<table style="visibility: hidden">
								<tr>
									<td class="<%=LABEL_CLASS%>" align="left" id="maintain_button">
										<input class="button1" type="button" name="btnMaintain" value="Maintain" onClick="maintainRequest('uw');"
										<%=MAINTAIN_ACCESS%>>
									</td>
									<td class="<%=LABEL_CLASS%>" align="left" id="create_button">
										<input class="button1" type="button" name="btnCreate" value="Create" onClick="window.location = 'createAssessmentRequest.do';"
										<%=CREATE_ACCESS%>>
									</td>
									<td class="<%=LABEL_CLASS%>" align="left" id="save_button">
										<input class="button1" type="button" name="btnSave" value="Save" onClick="saveRequest();"
										<%=MAINTAIN_ACCESS%>>
									</td>
									<td class="<%=LABEL_CLASS%>" align="left" id="cancel_button">
										<input class="button1" type="button" name="btnCancel" value="Cancel" onClick="window.location = 'viewCDSDetail.do';"
										<%=MAINTAIN_ACCESS%>>
									</td>
									<td class="<%=LABEL_CLASS%>" align="left" style="visibility: hidden">
										<input class="button1" type="button" name="btnUpdateLocation" value="Update Location" onClick="gotoPage('requestForm', 'updateLocation.do');"
										<%=EXECUTE_ACCESS%>>
									</td>
								</tr>
							</table>
						</div>
					</div>
					<br />
					<!-- *************************************************** -->
					<!-- *************** END UWASSESSMENT CONTENT ********************** -->
					<!-- *************************************************** -->
				
				
					<!-- *************************************************** -->
					<!-- *************** IMPAIRMENTS CONTENT ********************** -->
					<!-- *************************************************** -->
					<div id="UWAssessmentImpairments" style="border: solid;border-width:1;position:relative; top:0; left:0; width: 700px;">
						<div id="UWAssessmentImpairmentsId">
							<table border="0" cellpadding="3" cellspacing="0" width="142%">
								<!-- <tr bgcolor="#2A4C7C"> -->
								<tr <%=iumBgColor%>>
									<td class="<%=LABEL_CLASS%>" style="color: #FFFFFF"><b>IMPAIRMENTS</b></td>
								</tr>
							</table>
						</div>
						
						<!-- UWAssessmentImpairmentsId -->
						<div id="UWAssessmentImpairmentsDetails" style="padding: 10px; width: 131%">
							<%
							String reqClientType = request.getParameter("clientType");
							String clientType = "";
					
							if (reqClientType == null) {
								clientType = IUMConstants.CLIENT_TYPE_INSURED;
							} else {
								clientType = reqClientType;
							}
					
							String clientTypeChecked_O = "";
							String clientTypeChecked_I = "";
					
							if (clientType.equals(IUMConstants.CLIENT_TYPE_OWNER)) {
								clientTypeChecked_O = "checked";
							} else if (clientType.equals(IUMConstants.CLIENT_TYPE_INSURED)) {
								clientTypeChecked_I = "checked";
							}
							%>
							<%if (_lob.equals(IUMConstants.LOB_INDIVIDUAL_LIFE)) {%>
								<b>Client Type </b>&nbsp;
								<input type="radio" checked name="impClientType" value="<%=IUMConstants.CLIENT_TYPE_INSURED%>" <%=clientTypeChecked_I%> onClick="">Insured&nbsp;
								<input type="radio" name="impClientType" value="<%=IUMConstants.CLIENT_TYPE_OWNER%>" <%=clientTypeChecked_O%> onClick="">Owner&nbsp;
								<br>
							<%}%>
							
							<table border="0" cellpadding="2" cellspacing="1" width="108%" bgcolor="#FFFFFF" style="border: 1px solid #D1D1D1;" class="listtable1">
								<%if (contextPath.equalsIgnoreCase(wmsContextPath)) {%>
								<tr>
									<td class="headerrow7" width="5" align="center">&nbsp;</td>
									<td class="headerrow7" width="300" align="center"><span class="required">*</span>DESCRIPTION</td>
									<td class="headerrow7" width="150" align="center"><span class="required">*</span>RELATIONSHIP</td>
									<td class="headerrow7" width="150" align="center"><span class="required">*</span>IMPAIRMENT ORIGIN</td>
									<td class="headerrow7" width="100" align="center"><span class="required">*</span>IMPAIRMENT DATE</td>
									<td class="headerrow7" width="110" align="center"><span class="required">*</span>ACTION CODE</td>
									<td class="headerrow7" width="90" align="center">STAT (HT/WT/BP)</td>
									<td class="headerrow7" width="20" align="center">CONFIRM</td>
								</tr>
								
								<%} else {
									if (Long.parseLong(currentStatus) == IUMConstants.STATUS_UNDERGOING_ASSESSMENT) {
								%>
								<tr>
									<td class="headerrow6" width="5" align="center">&nbsp;</td>
									<td class="headerrow6" width="150" align="center"><span class="required">*</span>DESCRIPTION</td>
									<td class="headerrow6" width="115" align="center"><span class="required">*</span>RELATIONSHIP</td>
									<td class="headerrow6" width="115" align="center"><span class="required">*</span>IMPAIRMENT ORIGIN</td>
									<td class="headerrow6" width="30" align="center"><span class="required">*</span>IMPAIRMENT DATE</td>
									<td class="headerrow7" width="20" align="center"><span class="required">*</span>ACTION CODE</td>
									<td class="headerrow7" width="90" align="center">STAT (HT/WT/BP)</td>
									<td class="headerrow7" width="20" align="center">CONFIRM</td>
								</tr>
								<%	} else { %>
								<tr>
									<td class="headerrow6" width="5" align="center">&nbsp;</td>
									<td class="headerrow6" width="150" align="center">DESCRIPTION</td>
									<td class="headerrow6" width="115" align="center">RELATIONSHIP</td>
									<td class="headerrow6" width="115" align="center">IMPAIRMENT ORIGIN</td>
									<td class="headerrow6" width="30" align="center">IMPAIRMENT DATE</td>
									<td class="headerrow7" width="20" align="center">ACTION CODE</td>
									<td class="headerrow7" width="90" align="center">STAT (HT/WT/BP)</td>
									<td class="headerrow7" width="20" align="center">CONFIRM</td>
								</tr>
								<%
									}
								}
								%>
								
								<%
								i = 0;
								
								ArrayList tmp2 = actionForm.getImpairments();
								ImpairmentForm impairment;
								
								for (int xx = 0; xx < tmp2.size(); xx++) {
									
									impairment = (ImpairmentForm) tmp2.get(xx);
	
									String height = impairment.getHeightInFeet();
									String weight = impairment.getWeight();
									String bloodPressure = impairment.getBloodPressure();
									String impExportDate = impairment.getExportDate();
									String impairmentCheckBoxAccess = "";
									
									if (impExportDate != null && !impExportDate.equals("")) {
										impairmentCheckBoxAccess = "disabled";
									}
									
									if (contextPath.equalsIgnoreCase(wmsContextPath)) {
										if (xx % 2 == 0) {
											tr_class = "row7";
										} else {
											tr_class = "row8";
										}
									} else {
										if (xx % 2 == 0) {
											tr_class = "row1";
										} else {
											tr_class = "row2";
										}
									}
									%>
									
									<%if (contextPath.equalsIgnoreCase(wmsContextPath)) {%>
									<tr>
										<td class="<%=tr_class%>" width="20">
											<input type="radio" name="idSelected" value='<%=impairment.getImpairmentId()%>' <%=impairmentCheckBoxAccess%>>
										</td>
										<td class="<%=tr_class%>" width="150"><%=impairment.getDescription()%></td>
										<td class="<%=tr_class%>" width="115"><%=impairment.getRelationship()%></td>
										<td class="<%=tr_class%>" width="115"><%=impairment.getImpairmentOrigin()%></td>
										<td class="<%=tr_class%>" width="50"><%=impairment.getImpairmentDate()%></td>
										<td class="<%=tr_class%>" width="50"><%=impairment.getActionCode()%></td>
										<td class="<%=tr_class%>" width="50">
										
											<%if (height != null && height.equals("")) {%>
												&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
											<% } else { %>
												<%=impairment.getHeightInFeet()%>'<%=impairment.getHeightInInches()%>" /
											<%}%>
											 
											<%if (weight != null && weight.equals("")) {%>
												&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
											<%} else {%>
												<%=impairment.getWeight()%> /
											<%}%>
											 
											<%if (bloodPressure != null && bloodPressure.equals("")) {%>
												&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
											<%} else {%>
												<%=impairment.getBloodPressure()%>
											<%}%>
										</td>
										<td class="<%=tr_class%>" width="60" align="center"><%=impairment.getConfirm()%></td>
									</tr>
									<%
									} else {
									%>
									<tr>
										<td class="<%=tr_class%>" width="5">
											<input type="radio" name="idSelected" value='<%=impairment.getImpairmentId()%>' <%=impairmentCheckBoxAccess%>>
										</td>
										<td class="<%=tr_class%>" width="150"><%=impairment.getDescription()%></td>
										<td class="<%=tr_class%>" width="115"><%=impairment.getRelationship()%></td>
										<td class="<%=tr_class%>" width="115"><%=impairment.getImpairmentOrigin()%></td>
										<td class="<%=tr_class%>" width="50"><%=impairment.getImpairmentDate()%></td>
										<td class="<%=tr_class%>" width="50"><%=impairment.getAgentCode()%></td>
										<td class="<%=tr_class%>" width="50">
										
										<%if (height != null && height.equals("")) {%>
											&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										<%} else {%>
										 	<%=impairment.getHeightInFeet()%>'<%=impairment.getHeightInInches()%>" / 
										<%}%>
										 
										<%if (weight != null && weight.equals("")) {%>
										 	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										 <%} else {%>
										 	<%=impairment.getWeight()%> /
										 <%}%>
										 		
										 <%if (bloodPressure != null && bloodPressure.equals("")) {%>
										 		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
										 <%} else {%>
										 	<%=impairment.getBloodPressure()%>
										 <%}%>
										</td>
										<td class="<%=tr_class%>" width="50" align="center"><%=impairment.getConfirm()%></td>
									</tr>
									<%}%>
									<%i++;%>
								<%}%>
								
								<% if (i == 0) { %>
								<tr>
									<%if (contextPath.equalsIgnoreCase(wmsContextPath)) { %>
										<td class="row8" width="5">&nbsp;</td>
										<td class="row8" colspan="7"></td>
									<% } else { %>
										<td class="row2" width="5">&nbsp;</td>
										<td class="row2" colspan="7"></td>
									<% } %>
								</tr>
								<%i++;%>
								<%}%>
								
								<%if (contextPath.equalsIgnoreCase(wmsContextPath)) {
									if (i % 2 == 0) {
										tr_class = "row7";
									} else {
										tr_class = "row8";
									}
								} else {
									if (i % 2 == 0) {
										tr_class = "row1";
									} else {
										tr_class = "row2";
									}
								}
								%>
								
								<%if (contextPath.equalsIgnoreCase(wmsContextPath)) {%>
								<tr>
									<td class="<%=tr_class%>" width="5">&nbsp;</td>
									<td class="<%=tr_class%>" width="250" id="codetd">
										<div id="codespan">
											<ium:list className="label2" styleName="width:225" disabled="true" listBoxName="code" type="<%=IUMConstants.LIST_MIB_IMPAIRMENT_CODE%>" selectedItem="<%=impCode%>" />
											<input class="label2" type="button" name="impCodeBut" value="..." onclick="showSearchImpairmentsCodeDescPage();">
										</div>
									</td>
									<td class="<%=tr_class%>" width="140" id="relationshiptd">
										<div id="relationshipspan">
											<select class="label2" id="relationship"  name="relationship" style="width:135" disabled="true">
												<option value="">Select Relationship</option>
												<option value="0">0 - Insured or life to be insured</option>
												<option value="1">1 - Father of the insured</option>
												<option value="2">2 - Mother of the insured</option>
												<option value="3">3 - Brother of the insured</option>
												<option value="4">4 - Sister of the insured</option>
												<option value="5">5 - Spouse of the insured</option>
												<option value="6">6 - Other persons living w/ insured</option>
											</select>
										</div>
									</td>
									<td class="<%=tr_class%>" width="140" id="impairmentOrigintd">
										<div id="impairmentOriginspan">
											<select class="label2" id="impairmentOrigin" name="impairmentOrigin" style="width:135" disabled="true">
												<option value="">Select Impairment Origin</option>
												<option value="m">m - Death</option>
												<option value="w">w - Discovery of the Impairment</option>
												<option value="x">x - Operation</option>
												<option value="y">y - Treatment</option>
												<option value="z">z - Recovery from Impairment</option>
											</select>
										</div>
									</td>
									<td class="<%=tr_class%>" width="100">
										<input type="text" style="width:70" maxlength="9" disabled="true" name="impairmentDate" class="label2" value="<%=impDate%>">&nbsp;
										<div id="imp_cal" style="display:none;position:absolute;">
											<img src="<%=contextPath%>/images/calendar.gif" border="0" onClick="displayDatePicker('impairmentDate', false, 'ddMMMyyyy', '');">
										</div>
									</td>
									<td class="<%=tr_class%>" width="100" id="actionCodetd">
										<div id="actionCodespan">
											<select class="label2" id="actionCode" name="actionCode" style="width:115" disabled="true">
												<option value="">Select Action Code</option>
												<option value="A">A - Application is still under process</option>
												<option value="B">B - BDR</option>
												<option value="C">C - Cancelled</option>
												<option value="D">D - Declined</option>
												<option value="DR">DR - Declined Rider</option>
												<option value="I">I - Incomplete</option>
												<option value="P">P - Postponed</option>
												<option value="R">R - Rated</option>
												<option value="RB">RB - Rated Basic</option>
												<option value="Rec">Rec - Reconsidered</option>
												<option value="RR">RR - Rated Rider</option>
												<option value="S">S - Standard</option>
											</select>
										</div>
									</td>
									<td class="<%=tr_class%>" width="100">
										<div style="width:100">
											<input type="text" style="width:30" disabled="true" maxlength="6" name="height" value="<%=impHeight%>" class="label2">
											<input type="text" style="width:30" disabled="true" maxlength="3" name="weight" value="<%=impWeight%>" class="label2">
											<input type="text" style="width:30" disabled="true" maxlength="7" name="bloodPressure" value="<%=impBP%>" class="label2">
										</div>
									</td>
									<td class="<%=tr_class%>" width="60" align="center" id="confirmtd">
										<div id="confirmspan">
											<select class="label2" id="confirm" name="confirm" style="width:60" disabled="true">
												<option value="">Select</option>
												<option value="R" >R</option>
												<option value="V" >V</option>
											</select>
										</div>
									</td>
								</tr>
					
								<%} else {
									if (Long.parseLong(currentStatus) == IUMConstants.STATUS_UNDERGOING_ASSESSMENT) {
								%>
								
								<!-- impairments input here -->
								<tr>
									<td class="<%=tr_class%>" width="20">&nbsp;</td>
									<td class="<%=tr_class%>" width="150">
										<ium:list className="label2" styleName="width:200" disabled="true" listBoxName="code" type="<%=IUMConstants.LIST_MIB_IMPAIRMENT_CODE%>" selectedItem="<%=impCode%>" />
									</td>
									<td class="<%=tr_class%>" width="115">
										<ium:list className="label2" styleName="width:115" disabled="true" listBoxName="relationship" type="<%=IUMConstants.LIST_MIB_NUMBER_CODE%>" selectedItem="<%=impRelationship%>" />
									</td>
									<td class="<%=tr_class%>" width="115">
										<ium:list className="label2" styleName="width:115" disabled="true" listBoxName="impairmentOrigin" type="<%=IUMConstants.LIST_MIB_LETTER_CODE%>" selectedItem="<%=impOrigin%>" />
									</td>
									<td class="<%=tr_class%>" width="50">
										<input type="text" style="width:20" maxlength="9" disabled="true" name="impairmentDate" class="label2" value="<%=impDate%>">&nbsp;
										<div id="imp_cal" style="display:none;position:absolute;">
											<img src="<%=contextPath%>/images/calendar.gif" border="0" onClick="displayDatePicker('impairmentDate', false, 'ddMMMyyyy', '-');">
										</div>
									</td>
									<td class="<%=tr_class%>" width="50">
										<ium:list className="label2" styleName="width:115" disabled="true" listBoxName="actionCode" type="<%=IUMConstants.LIST_MIB_ACTION_CODE%>" selectedItem="<%=impAction%>" />
									</td>
									<td class="<%=tr_class%>" width="50">
										<div style="width:100">
											<input type="text" style="width:30" disabled="true" maxlength="6" name="height" value="<%=impHeight%>" class="label2">
											<input type="text" style="width:30" disabled="true" maxlength="3" name="weight" value="<%=impWeight%>" class="label2">
											<input type="text" style="width:30" disabled="true" maxlength="7" name="bloodPressure" value="<%=impBP%>" class="label2">
										</div>
									</td>
									<td class="<%=tr_class%>" width="60" align="center" id="confirmtd">
										<div id="confirmspan">
											<select class="label2" id="confirm" name="confirm" style="width:60" disabled="true" onChange="" onFocus="">
												<option value="">Select</option>
												<option value="R" >R</option>
												<option value="V" >V</option>
											</select>
										</div>
									</td>
								</tr>
								<%
									}
								}
								%>
							<%//Page pg = (Page) request.getAttribute("page");%>
							<%//=pg.getPageNumbers()%>
						</table>
							
						<table>
							<tr id="createMaintain">
								<td class="<%=LABEL_CLASS%>" align="left">
									<input type="button" class="button1" name="create" value="Create" onClick="createImpairments();">&nbsp;
								</td>
							</tr>
							<!-- save impairment here -->
							<tr id="saveCancel">
								<td class="<%=LABEL_CLASS%>" align="left">
									<input type="button" class="button1" name="save" id="saveImpairmentBtn" value="Save" onClick="setSavedSection('UWAssessmentImpairments'); saveImpairment('<%=session_id%>');">&nbsp;
									<input type="button" class="button1" name="cancel" id="cancelImpairmentBtn" value="Cancel" onClick="cancelImpairment();">
								</td>
							</tr>
						</table>
						</div>
					</div>
					<br />
					<!-- *************************************************** -->
					<!-- *************** END IMPAIRMENTS CONTENT ********************** -->
					<!-- *************************************************** -->
						
					<!-- *************************************************** -->
					<!-- *************** LINKS WITH HIDDEN********************** -->
					<!-- *************************************************** -->
					<div id="UWAssessmentLinks" style="position:relative; top:0; left:0.65; width: 700px">
						<table border="0" cellpadding="3" cellspacing="0" width="100%">
							<tr>
								<td class="<%=LABEL_CLASS%>" style="text-align: center;font-size: 12px">
								<a HREF="#cds">CDS Details</a>&nbsp;&nbsp;
								<a HREF="#ko">KO Details</a>&nbsp;&nbsp;
								<a HREF="#" onclick="showPolicyRequirementsPage();">Requirements</a>&nbsp;&nbsp;
								<a HREF="#back">Back to Top</a>&nbsp;&nbsp;</td>
							</tr>
						</table>
							
						<!-- ******* HIDDEN VALS ******* -->
						<input type="hidden" name="heightInFeet" value=""> 
						<input type="hidden" name="heightInInches" value="">
						<input type="hidden" name="clientType" value="<%=request.getParameter("clientType")%>"> 
						<input type="hidden" name="arFormRefNo" id="arFormRefNo"value="<%=arFormRefNo %>"> 
						<input type="hidden" name="arFormOwnerClientNo" id="arFormOwnerClientNo" value="<%=arFormOwnerClientNo%>">
						<input type="hidden" name="arFormInsuredClientNo" id="arFormInsuredClientNo" value="<%=arFormInsuredClientNo%>">						
						<input type="hidden" name="sendButtonControl" id="sendButtonControl" value="<%=sendButtonControl%>"> 
						<input type="hidden" name="EXECUTE_KO" id="EXECUTE_KO" value="<%=EXECUTE_KO%>"> 
						<input type="hidden" name="userId" id="userId" value="<%=userId%>">
					</div>
					<br />
					<br />
					<!-- *************************************************** -->
					<!-- *************** END LINKS WITH HIDDEN********************** -->
					<!-- *************************************************** -->
						
		
					<!-- *************************************************** -->
					<!-- *************** SLFCopyright********************** -->
					<!-- *************************************************** -->
					<div id="SLFCopyright">
						<table>
							<tr>
								<td colspan="10" class="label5">
									� 2008 Sun Life Financial. All rights reserved.
								</td>
							</tr>
						</table>
					</div>
				</div>
			</td>
		</tr>
	</table>
</div>
</form>

<%
end = System.currentTimeMillis();
elapse = (end - start);

%>

<script type="text/javascript">
	function showPolicyRequirementsPage(){
		
		var policySuffix = document.getElementById('polSuffix').value;
		var url = '<%=contextPath%>/viewPolicyRequirements.do?sessionId=<%=session_id%>&arFormRefNo=<%=arFormRefNo%>&arFormOwnerClientNo=<%=arFormOwnerClientNo%>&arFormInsuredClientNo=<%=arFormInsuredClientNo%>&lob=<%=_lob%>&arFormRequestStatus=<%=arFormRequestStatus%>&sourceSystem=<%=_sourceSystem%>&agentCode=<%=_agentCode%>&branchCode=<%=_branchCode%>&assignedTo=<%=_assignedTo%>&sendButtonControl=<%=sendButtonControl%>&EXECUTE_KO=<%=EXECUTE_KO%>&policySuffix='+policySuffix;
		popUpWindow(url,775,700,'yes','yes','yes','no','no','no','no','0','810','30');
	}
	
	function showSearchImpairmentsCodeDescPage(){
		var policySuffix = document.getElementById('polSuffix').value;
		var url = '<%=contextPath%>/searchMIBImpairment.do?sessionId=<%=session_id%>&arFormRefNo=<%=arFormRefNo%>&arFormOwnerClientNo=<%=arFormOwnerClientNo%>&arFormInsuredClientNo=<%=arFormInsuredClientNo%>&lob=<%=_lob%>&arFormRequestStatus=<%=arFormRequestStatus%>&sourceSystem=<%=_sourceSystem%>&agentCode=<%=_agentCode%>&branchCode=<%=_branchCode%>&assignedTo=<%=_assignedTo%>&sendButtonControl=<%=sendButtonControl%>&EXECUTE_KO=<%=EXECUTE_KO%>&policySuffix='+policySuffix;
		popUpWindow(url,770,453,'yes','yes','yes','no','no','no','no','0','810','30');
	}
	
	try{
		
		var impConfirmCd ="<%=impConfirmationCode%>";
		var impActionCd ="<%=impAction%>";
		var impOriginCd ="<%=impOrigin%>";
		var relationshipCd ="<%=impRelationship%>";
	
		document.getElementById("confirm").value=impConfirmCd;
		document.getElementById("actionCode").value=impActionCd;
		document.getElementById("impairmentOrigin").value=impOriginCd;
		document.getElementById("impairmentOrigin").value=relationshipCd;
	} catch(e) {
	}
</script>

</body>
</html>
