 
<%@ page language="java" import="java.util.*, com.slocpi.ium.ui.form.*, com.slocpi.ium.util.*, com.slocpi.ium.ui.util.*, com.slocpi.ium.data.*, com.slocpi.ium.underwriter.*" buffer="24kb"%>
<%@ taglib uri="/struts-bean.tld"  prefix="bean" %>
<%@ taglib uri="/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/struts-html.tld"  prefix="html" %>
<%@ taglib uri="/ium.tld"          prefix="ium" %>

<%String contextPath  = request.getContextPath();%>


<%  String session_id = request.getAttribute("session_id").toString();
    

    String detailForm="detailForm_" + session_id;
   CDSDetailForm arForm = (CDSDetailForm) session.getAttribute("detailForm_" + session_id); %>
<% String currentStat = arForm.getRequestStatus(); %>
<% if (currentStat == null) currentStat = "0"; %>
<% if (currentStat.equals("")) currentStat = "0"; %>
<%
    // This routine queries the access rights of the users (RCRIVERA)
    //
    // --- START ---

    StateHandler sh = new StateHandler();
	UserData userData = sh.getUserData(request);
    Roles userRoles = new Roles(); // Class that checks Roles of user contained in the array list
    Access access = new Access();

	HashMap userAccess = sh.getUserAccess(request);

    String CREATE_KO = "";
    String MAINTAIN_KO = "";
    String DELETE_KO = "";
    String EXECUTE_KO = "";


    if(!access.hasAccess(userAccess,IUMConstants.PAGE_REQUIREMENTS_KO,IUMConstants.ACC_CREATE) ){
	  CREATE_KO = "DISABLED";
    }
    if(!access.hasAccess(userAccess,IUMConstants.PAGE_REQUIREMENTS_KO,IUMConstants.ACC_MAINTAIN) ){
	  MAINTAIN_KO = "DISABLED";
    }
    if(!access.hasAccess(userAccess,IUMConstants.PAGE_REQUIREMENTS_KO,IUMConstants.ACC_DELETE) ){
	  DELETE_KO = "DISABLED";
    }
    if(!access.hasAccess(userAccess,IUMConstants.PAGE_REQUIREMENTS_KO,IUMConstants.ACC_EXECUTE) ){
	  EXECUTE_KO = "DISABLED";
    }

    // --- END ---

	//disable buttons if current status is an end status
	AssessmentRequest ar = new AssessmentRequest();
	String currentStatus = arForm.getRequestStatus(); //request.getParameter("currentStatus");
    String lob = request.getParameter("lob");
	if (ar.isEndStatus(lob, Long.parseLong(currentStatus))) {
		CREATE_KO = "DISABLED";
		MAINTAIN_KO = "DISABLED";
		DELETE_KO = "DISABLED";
		EXECUTE_KO = "DISABLED";
	}

	PolicyRequirements polReq = new PolicyRequirements();
	boolean hasOrderReqt = polReq.hasOrderRequirements(arForm.getRefNo());
	String sendButtonControl = "DISABLED";
	String s="";
	if (hasOrderReqt){
		sendButtonControl = "ENABLED";
		s="e";
	}
	
 %>

<%-- <html>
  <head> 
    <title>Sun Life Financial - Philippines</title> 
    <link href="<%=contextPath%>/css/main.css" rel="stylesheet" type="text/css">
    <script language="Javascript" src="<%=contextPath%>/js/navigation.js"></script>
    <script language="Javascript" src="<%=contextPath%>/jsp/validate_req_CDSDetail.jsp"></script>--%>
    
    <script language="Javascript">
    
	
function saveStatus(contextPath, followup) {

	var form       = document.prequirementForm; //document.forms[0];
	var lob        = form.lob.value;
	var branchId   = form.branchCode.value;
	var assignedTo = form.assignedTo.value;
	var reqStat    = requestForm.currentStatus.value;
	//var role       = parent.requestForm.userRole.value;

	if (followup == "no") {
		if (form.statusTo.value == "") {
			alert("<bean:message key="error.field.requiredselection" arg0="Requirement status"/>");
			form.statusTo.focus();
			return;
		}
	}

	var cnt    = 0;
	var ord    = 0;
	var rec    = 0;
	var waive  = 0;
	var acc    = 0;
	var rej    = 0;
	var canc   = 0;
	var others = 0;
	if (form.index[0]) {
		for (var i=0;i<form.index.length;i++) {
			if (form.index[i].checked) {
				form.indexTemp[i].value = "checked";
				if (form.statusTo.value == "<%= IUMConstants.STATUS_ORDERED %>") {
					if (form.statusCode[i].value == "<%= IUMConstants.STATUS_ORDERED %>") {
					    ord++;
					}
					else {
					    others++;
					}
			    }
				if (form.statusTo.value == "<%= IUMConstants.STATUS_RECEIVED_IN_SITE %>") {
					if (form.statusCode[i].value == "<%= IUMConstants.STATUS_RECEIVED_IN_SITE %>") {
					    rec++;
					}
					else {
					    others++;
					}
			    }
				if (form.statusTo.value == "<%= IUMConstants.STATUS_WAIVED %>") {
					if (form.statusCode[i].value == "<%= IUMConstants.STATUS_WAIVED %>") {
					    waive++;
					}
					else {
					    others++;
					}
			    }
				if (form.statusTo.value == "<%= IUMConstants.STATUS_REVIEWED_AND_REJECTED %>") {
					if (form.statusCode[i].value == "<%= IUMConstants.STATUS_REVIEWED_AND_REJECTED %>") {
					    rej++;
					}
					else {
					    others++;
					}
			    }
				if (form.statusTo.value == "<%= IUMConstants.STATUS_REVIEWED_AND_ACCEPTED %>") {
					if (form.statusCode[i].value == "<%= IUMConstants.STATUS_REVIEWED_AND_ACCEPTED %>") {
					    acc++;
					}
					else {
					    others++;
					}
			    }
				if (form.statusTo.value == "<%= IUMConstants.STATUS_REQ_CANCELLED %>") {
					if (form.statusCode[i].value == "<%= IUMConstants.STATUS_REQ_CANCELLED %>") {
					    canc++;
					}
					else {
					    others++;
					}
			    }

				cnt++;
			}
		}
	}
	else {
		if (form.index.checked) {
			form.indexTemp.value = "checked";
			if (form.statusTo.value == "<%= IUMConstants.STATUS_ORDERED %>") {
				if (form.statusCode.value == "<%= IUMConstants.STATUS_ORDERED %>") {
				    ord++;
				}
			}
			if (form.statusTo.value == "<%= IUMConstants.STATUS_RECEIVED_IN_SITE %>") {
				if (form.statusCode.value == "<%= IUMConstants.STATUS_RECEIVED_IN_SITE %>") {
				    rec++;
				}
			}
			if (form.statusTo.value == "<%= IUMConstants.STATUS_WAIVED %>") {
				if (form.statusCode.value == "<%= IUMConstants.STATUS_WAIVED %>") {
				    waive++;
				}
			}
			if (form.statusTo.value == "<%= IUMConstants.STATUS_REVIEWED_AND_REJECTED %>") {
				if (form.statusCode.value == "<%= IUMConstants.STATUS_REVIEWED_AND_REJECTED %>") {
				    rej++;
				}
			}
			if (form.statusTo.value == "<%= IUMConstants.STATUS_REVIEWED_AND_ACCEPTED %>") {
				if (form.statusCode.value == "<%= IUMConstants.STATUS_REVIEWED_AND_ACCEPTED %>") {
				    acc++;
				}
			}
			if (form.statusTo.value == "<%= IUMConstants.STATUS_REQ_CANCELLED %>") {
				if (form.statusCode.value == "<%= IUMConstants.STATUS_REQ_CANCELLED %>") {
				    canc++;
				}
			}
			cnt++;
		}
	}

	if (cnt == 0) {
		alert("<bean:message key="error.field.noselection" arg0="" arg1="to update"/>");
		return;
	}

	if (ord == 1 && others == 0) {
		alert("<bean:message key="error.sameStatusRequirement" arg0="ordered"/>");
		return;
	}
	if (rec == 1 && others == 0) {
		alert("<bean:message key="error.sameStatusRequirement" arg0="received"/>");
		return;
	}
	if (waive == 1 && others == 0) {
		alert("<bean:message key="error.sameStatusRequirement" arg0="waived"/>.");
		return;
	}
	if (rej == 1 && others == 0) {
		alert("<bean:message key="error.sameStatusRequirement" arg0="rejected"/>");
		return;
	}
	if (acc == 1 && others == 0) {
		alert("<bean:message key="error.sameStatusRequirement" arg0="accepted"/>");
		return;
	}
	if (canc == 1 && others == 0) {
		alert("<bean:message key="error.sameStatusRequirement" arg0="cancelled"/>");
		return;
	}

	var status = form.statusTo.value;
	
	if (status == "<%= IUMConstants.STATUS_ORDERED %>" && lob == "<%= IUMConstants.LOB_INDIVIDUAL_LIFE %>") {
       <% //if (!userRoles.getRoles(userData.getRoles(),IUMConstants.ROLES_NB_STAFF) && !userRoles.getRoles(userData.getRoles(),IUMConstants.ROLES_UNDERWRITER) && !userRoles.getRoles(userData.getRoles(),IUMConstants.ROLES_UNDERWRITER_SUPERVISOR)) { 
       //if (!userRoles.getRoles(userData.getRoles(),IUMConstants.ROLES_NB_STAFF) && !userRoles.getRoles(userData.getRoles(),IUMConstants.ROLES_UNDERWRITER) && !userRoles.getRoles(userData.getRoles(),IUMConstants.ROLES_UNDERWRITER_SUPERVISOR)) { 
       %>
         
           <%--alert("<bean:message key="error.accessRequirement" arg0="order"/>");
           return;--%>
       <%//} else {%>
          
          //alert("countStatusOrdered:"+countStatusOrdered(document.prequirementForm));
          
          <%--
          if (countStatusOrdered(document.prequirementForm) == "false") {
		    
		    // delete facilitator validation 
		   if (form.facilitator.value == "") {
                if (form.facilitator.disabled) {
		            form.facilitator.disabled = false;
		        }
				alert("<bean:message key="error.field.requiredselection" arg0="Facilitator"/>");
				form.facilitator.focus(); 
				return;
	    	}
	    	
	      }
	      --%>
	      //alert("order requirements");
	      //orderRequirement(contextPath, "", document.prequirementForm);
	      orderRequirement(contextPath, form.facilitator.value, document.prequirementForm);
	      //alert("order requirements submitted");
		  //window.open(contextPath + "/listFacilitators.do","window","location=no,scrollbars=yes,menubar=no,toolbar=no,resizable=yes,width=300,height=300");
	  		
	  
	   <%//}%>
	}
	
	if (status == "<%= IUMConstants.STATUS_ORDERED %>" && (lob == "<%= IUMConstants.LOB_PRE_NEED %>" || lob == "<%= IUMConstants.LOB_GROUP_LIFE %>")) {
        <%// if (!userRoles.getRoles(userData.getRoles(),IUMConstants.ROLES_NB_STAFF) && !userRoles.getRoles(userData.getRoles(),IUMConstants.ROLES_UNDERWRITER) && !userRoles.getRoles(userData.getRoles(),IUMConstants.ROLES_UNDERWRITER_SUPERVISOR)) { %>
           <%--alert("<bean:message key="error.accessRequirement" arg0="order"/>");
           return;--%>
	    <%//}%>
       <% //if (userRoles.getRoles(userData.getRoles(),IUMConstants.ROLES_NB_STAFF)) { %>
	       if (reqStat == "<%= IUMConstants.STATUS_NB_REVIEW_ACTION %>" || reqStat == "<%= IUMConstants.STATUS_AWAITING_REQUIREMENTS %>") {
	           orderRequirement(contextPath, "", document.prequirementForm);
	       }
	       else {
	           alert("JONATHAN1");
 	           alert("<bean:message key="error.requirement.invalid" arg0="ordering"/>");
 	           alert("JONATHAN2");
 	           return;
	       }
	    <%//}	%>
        <% //if (userRoles.getRoles(userData.getRoles(),IUMConstants.ROLES_UNDERWRITER)) { %>
	       if (reqStat == "<%= IUMConstants.STATUS_UNDERGOING_ASSESSMENT %>" || reqStat == "<%= IUMConstants.STATUS_AWAITING_REQUIREMENTS %>") {
               if (countStatusOrdered(form) == "false") {
		           
		          alert("countStatusOrdered(form)=false"); 
		          // delete facilitator validation
		           if (form.facilitator.value == "") {
		              if (form.facilitator.disabled) {
		                  form.facilitator.disabled = false;
		              }
				      alert("<bean:message key="error.field.requiredselection" arg0="Facilitator"/>");
				      form.facilitator.focus();
				      return;
	        	   }
	        	   
	           }
	           //orderRequirement(contextPath, "", document.prequirementForm);
	           
	           orderRequirement(contextPath, form.facilitator.value, document.prequirementForm);
               //window.open(contextPath + "/listFacilitators.do","window","location=no,scrollbars=yes,menubar=no,toolbar=no,resizable=yes,width=300,height=300");
           }
           else {
 	           alert("<bean:message key="error.requirement.invalid" arg0="ordering"/>");
 	           return;
           }
	    <%//}%>
	}
	if (status == "<%= IUMConstants.STATUS_REQ_CANCELLED %>") {
	   if (lob == "<%= IUMConstants.LOB_INDIVIDUAL_LIFE %>") {
           <%// if (!userRoles.getRoles(userData.getRoles(),IUMConstants.ROLES_UNDERWRITER)) { %>
    	       <%--alert("<bean:message key="error.accessRequirement" arg0="cancel"/>");
        	   return;--%>
	       <%//}%>
	   }
       else {
        <% //if (!userRoles.getRoles(userData.getRoles(),IUMConstants.ROLES_NB_STAFF) && !userRoles.getRoles(userData.getRoles(),IUMConstants.ROLES_UNDERWRITER)) { %>
	           <%--alert("<bean:message key="error.accessRequirement" arg0="cancel"/>");
    	       return;--%>
		<%//}%>
       }

	}
	if (status == "<%= IUMConstants.STATUS_WAIVED %>") {
	   if (lob == "<%= IUMConstants.LOB_INDIVIDUAL_LIFE %>") {
	       <% //if (!userRoles.getRoles(userData.getRoles(),IUMConstants.ROLES_UNDERWRITER)) { %>
    	       <%--alert("<bean:message key="error.accessRequirement" arg0="waive"/>");
        	   return;--%>
	       <%//}%>
	   }
       else {
        <%//if (!userRoles.getRoles(userData.getRoles(),IUMConstants.ROLES_NB_STAFF) && !userRoles.getRoles(userData.getRoles(),IUMConstants.ROLES_UNDERWRITER)) { %>
	          <%-- alert("<bean:message key="error.accessRequirement" arg0="waive"/>");
    	       return;--%>
		<%//}%>
       }

	}
    if (followup == "no" && status != "<%= IUMConstants.STATUS_ORDERED %>") {
       if (status == "<%=IUMConstants.STATUS_REVIEWED_AND_ACCEPTED%>") {
        <%//if (!userRoles.getRoles(userData.getRoles(),IUMConstants.ROLES_NB_STAFF) && !userRoles.getRoles(userData.getRoles(),IUMConstants.ROLES_UNDERWRITER) && !userRoles.getRoles(userData.getRoles(),IUMConstants.ROLES_FACILITATOR)) { %>
               <%--alert("<bean:message key="error.accessRequirement" arg0="accept"/>");
               return;--%>
        <%//}%>
       }
       
	   form.action = contextPath + "/saveRequirementStatus.do?lob=" + lob + "&branchCode=" + branchId + "&arStatus=" + reqStat + "&assignedTo=" + assignedTo;
	   
	   	//try{
		//	showLayer();
		//	window.question.focus();
		//	setLayerPosition2();
		//}catch(e){
		//}
		
	   form.submit();
	 
    }
    if (followup == "yes") {
      
	   form.action = contextPath + "/saveRequirementStatus.do?lob=" + lob + "&branchCode=" + branchId + "&followUp=yes&arStatus=" + reqStat;
	   
	   	//try{
		//	showLayer();
		//	window.question.focus();
		//	setLayerPosition2();
		//}catch(e){
		//}
	   
  	   form.submit();
  	  
	}
	
}

function onChangeOfStatus() {

    var form    = document.prequirementForm; //document.forms[0];
	var lob     = form.lob.value;
	var reqStat = requestForm.currentStatus.value;
	//var role    = parent.requestForm.userRole.value;
	var status  = form.statusTo.value;
	
	if (status != "<%= IUMConstants.STATUS_ORDERED %>") {
       form.facilitator.disabled = true;
	}
	if (status == "<%= IUMConstants.STATUS_ORDERED %>" && lob == "<%= IUMConstants.LOB_INDIVIDUAL_LIFE %>") {
       
       <%//if (!userRoles.getRoles(userData.getRoles(),IUMConstants.ROLES_NB_STAFF) && !userRoles.getRoles(userData.getRoles(),IUMConstants.ROLES_UNDERWRITER) && !userRoles.getRoles(userData.getRoles(),IUMConstants.ROLES_UNDERWRITER_SUPERVISOR)) { %>
            <%--alert("<bean:message key="error.accessRequirement" arg0="order"/>");
  	        form.facilitator.disabled = true;
            return;--%>
       <%//}else { %>
  	      if (countStatusOrdered(form) == "false") {
	         form.facilitator.disabled = false;
	      }
	      else {
	         form.facilitator.disabled = true;
	      }
	   <%//}%>
	   
    }
	if (status == "<%= IUMConstants.STATUS_ORDERED %>") {
	
	    if (lob == "<%= IUMConstants.LOB_PRE_NEED %>" || lob == "<%= IUMConstants.LOB_GROUP_LIFE %>") {
            <% //if (!userRoles.getRoles(userData.getRoles(),IUMConstants.ROLES_NB_STAFF) && !userRoles.getRoles(userData.getRoles(),IUMConstants.ROLES_UNDERWRITER) && !userRoles.getRoles(userData.getRoles(),IUMConstants.ROLES_UNDERWRITER_SUPERVISOR)) { %>
              <%--alert("<bean:message key="error.accessRequirement" arg0="order"/>");
              return;--%>
	        <%//}%>
            <%//if (userRoles.getRoles(userData.getRoles(),IUMConstants.ROLES_NB_STAFF)) { %>
		       if (reqStat == "<%= IUMConstants.STATUS_NB_REVIEW_ACTION %>" || reqStat == "<%= IUMConstants.STATUS_AWAITING_REQUIREMENTS %>") {
		           if (!form.facilitator.disabled) {
 	    	           form.facilitator.disabled = true;
 	    	       }
	       	   }
	    	<%//}%>
   	       <%//if (userRoles.getRoles(userData.getRoles(),IUMConstants.ROLES_UNDERWRITER)) { %>
		       if (reqStat == "<%= IUMConstants.STATUS_UNDERGOING_ASSESSMENT %>" || reqStat == "<%= IUMConstants.STATUS_AWAITING_REQUIREMENTS %>") {
		           if (form.facilitator.disabled) {
			  	       if (countStatusOrdered(form) == "false") {
 	    	 	          form.facilitator.disabled = false;
 	    	 	       }
 	    	       }
	       	   }
	       <%//}%>
	    }
	
	}

	if (status == "<%= IUMConstants.STATUS_REQ_CANCELLED %>") {
	   if (lob == "<%= IUMConstants.LOB_INDIVIDUAL_LIFE %>") {
   	       <%//if (!userRoles.getRoles(userData.getRoles(),IUMConstants.ROLES_UNDERWRITER)) { %>
    	       <%--alert("<bean:message key="error.accessRequirement" arg0="cancel"/>");
        	   return;--%>
	       <%//}%>
	   }
       else {
           <%// if (!userRoles.getRoles(userData.getRoles(),IUMConstants.ROLES_NB_STAFF) && !userRoles.getRoles(userData.getRoles(),IUMConstants.ROLES_UNDERWRITER)) { %>
	           <%--alert("<bean:message key="error.accessRequirement" arg0="cancel"/>");
    	       return;--%>
		   <%//}%>
       }

	}
	if (status == "<%= IUMConstants.STATUS_WAIVED %>") {
	   if (lob == "<%= IUMConstants.LOB_INDIVIDUAL_LIFE %>") {
           <%//if (!userRoles.getRoles(userData.getRoles(),IUMConstants.ROLES_UNDERWRITER)) { %>
    	       <%--alert("<bean:message key="error.accessRequirement" arg0="waive"/>");
        	   return;--%>
	       <%//}%>
	   }
       else {
           <% //if (!userRoles.getRoles(userData.getRoles(),IUMConstants.ROLES_NB_STAFF) && !userRoles.getRoles(userData.getRoles(),IUMConstants.ROLES_UNDERWRITER)) { %>
	           <%--alert("<bean:message key="error.accessRequirement" arg0="waive"/>");
    	       return;--%>
		   <%//}%>
       }

	}
    if (status == "<%=IUMConstants.STATUS_REVIEWED_AND_ACCEPTED%>") {
           <% //if (!userRoles.getRoles(userData.getRoles(),IUMConstants.ROLES_NB_STAFF) && !userRoles.getRoles(userData.getRoles(),IUMConstants.ROLES_UNDERWRITER) && !userRoles.getRoles(userData.getRoles(),IUMConstants.ROLES_FACILITATOR)) { %>
               <%--alert("<bean:message key="error.accessRequirement" arg0="accept"/>");
               return;--%>
          <%//}%>
    }

}


function viewDocument(fnUrl){
		
		//var scr_w = screen.availWidth;
		//var scr_h = screen.availHeight;
		
		//window.resizeTo((scr_w*.75),scr_h);
		//window.moveTo(0,0);
		window.open(fnUrl,'WMS Image Viewer','toolbar=no,menubar=no,resizable=yes');
		//var handler = window.open(fnUrl,"WMS Image Viewer","toolbar=no,menubar=no,resizable=yes");
		//handler.resizeTo((scr_w*.50),scr_h);		
}
		
	
</script>
 <!--  </head>-->

  <logic:empty name="<%=detailForm%>"  property="requirements" scope="session">
  <body leftmargin="0" topmargin="0" onLoad="disableCheckBox(document.prequirementForm);">
  </logic:empty>
  <logic:notEmpty name="<%=detailForm%>"  property="requirements" scope="session">
  <body leftmargin="0" topmargin="0">
  </logic:notEmpty>

    <form name="prequirementForm" method="post">
     <input type="hidden" name="session_id"     value='<%=session_id%>'>
      <%--<table border="0" cellpadding="0" cellspacing="0">
        <tr>
          <td class="label1" colspan="3"><html:errors/>&nbsp;</td>
        </tr>
      </table>--%>

      <table>
        <tr>
          <td class="label2"><b>Change Status To &nbsp;&nbsp; </b></td>
       <%--<% if (!( (Long.parseLong(currentStat) == IUMConstants.STATUS_UNDERGOING_ASSESSMENT)  ||
                 (Long.parseLong(currentStat) == IUMConstants.STATUS_AWAITING_REQUIREMENTS)  ||
                 (Long.parseLong(currentStat) == IUMConstants.STATUS_FOR_FACILITATOR_ACTION) ||
                 ((Long.parseLong(currentStat) == IUMConstants.STATUS_NB_REVIEW_ACTION) &&
                  (lob.equals(IUMConstants.LOB_PRE_NEED) || lob.equals(IUMConstants.LOB_GROUP_LIFE)) ) ) &&
                ( (Long.parseLong(currentStat) == IUMConstants.STATUS_AR_CANCELLED) ||
                 (EXECUTE_KO.equals("DISABLED"))) ) { %>
          <td>
            <ium:list className="label2" listBoxName="statusTo" type="<%=IUMConstants.LIST_REQUIREMENT_STATUS%>" styleName="width:200" selectedItem="" onChange="onChangeOfStatus();" disabled="true"/>
          </td>
          <td>&nbsp;&nbsp;<input class="button1" type="button" name="saveBtn" value="Save" disabled></td>
          <td>&nbsp;&nbsp;<input type="button" class="button1" name="followUp" value="Follow-up" disabled></td>
       <% } else { %>          --%>
          <td>
            <%
            //filter for request status
            String lob_status = lob + ",null";
            %>
            <div id="statusTospan"><ium:list className="label2" listBoxName="statusTo" type="<%=IUMConstants.LIST_REQUIREMENT_STATUS%>" styleName="width:200" selectedItem="" onChange="onChangeOfStatus();" filter="<%=lob_status%>"/></div>
          </td>
          <input type="hidden" name="savedSection" value="req" />
          <td>&nbsp;&nbsp;<input class="button1" type="button" name="saveBtn" value="Save" onClick="saveStatus('<%=contextPath%>','no');" <%= EXECUTE_KO%>></td>
          <td>&nbsp;&nbsp;<input type="button" class="button1" name="followUp" value="Follow-up" onClick="setReqSavedSection('req'); saveStatus('<%=contextPath%>','yes');" <%= EXECUTE_KO%>></td>
		  <% if (sendButtonControl.equals("ENABLED")){%>
	          <td>&nbsp;&nbsp;<input type="button" class="button1" name="sendBtn" value="Send Notification" onClick="setReqSavedSection('req'); sendNotification('<%=contextPath%>','req');" <%= EXECUTE_KO%>></td>
		  <%}%>

       <%--<% } %>--%>
        </tr>
        <tr></tr>
       <tr style="visibility:hidden">
          <td class="label2"><b>Facilitator </b></td>
          <td>
             <%--Facilitator List START --%>
            <ium:list className="label2" listBoxName="facilitator" type="<%=IUMConstants.LIST_USERS%>" styleName="width:200" selectedItem="" filter="<%= IUMConstants.ROLES_FACILITATOR %>" disabled="true"/>
             <%--Facilitator List END --%>
          </td>
        </tr>
              </table>

      <div id="divname" style="">
      	 <% //WMS session expiration = close browser
			ResourceBundle rb = ResourceBundle.getBundle("wmsconfig");
			String wmsContextPath = rb.getString("wms_context_path");
			String LIST_TABLE_WIDTH = "";
			String TD_WIDTH = "";
            if(contextPath.equalsIgnoreCase(wmsContextPath)){
            	LIST_TABLE_WIDTH = "560";
            	TD_WIDTH = "8";
            }
            else {
            	LIST_TABLE_WIDTH = "560";
                TD_WIDTH = "15";
            	}%>
        <table border="0" width="" cellpadding="2" cellspacing="1" bgcolor=#FFFFFF style="border: 1px solid #D1D1D1;" class="listtable1">
          <tr>
            <td class="headerrow6"><input type="checkbox" name="selAll" onClick="javascript:selectAllEnabled(document.prequirementForm);"></td>
            <td class="headerrow6">SEQ</td>
            <td class="headerrow6">CODE</td>
            <% 
			//WMS session expiration = close browser			
            if(contextPath.equalsIgnoreCase(wmsContextPath)){ %>
            <td class="headerrow6" width="15" align="center">&nbsp;&nbsp;IMAGE</td>
            <%}%>
            <td class="headerrow6" width="50">ST</td>
            <td class="headerrow6" width="75">STATUS DATE</td>
            <td class="headerrow6">CLIENT NO.</td>
            <td class="headerrow6" width="">COMMENT</td>
            <td class="headerrow6">I/O</td>
            <td class="headerrow6">P/C</td>
            <td class="headerrow6" width="70">FOLLOW-UP DATE</td>
            <td class="headerrow6" width="70">DATE SENT</td>
            <td class="headerrow6" width="">TEST DATE</td>
            
          </tr>
          <%
          int    i=0;
          String tr_class;
          %>
          <input type="hidden" name="refNo"        value='<bean:write name="<%=detailForm%>" property="refNo"/>'>
          <input type="hidden" name="lob"          value="<%=request.getParameter("lob")%>">
          <input type="hidden" name="branchCode"   value="<%=request.getParameter("branchId")%>">
          <input type="hidden" name="agentCode"    value="<%=request.getParameter("agentCode")%>">
          <input type="hidden" name="assignedTo"   value="<%=request.getParameter("assignedTo")%>">
          <input type="hidden" name="sourceSystem"   value="<%=arForm.getSourceSystem()%>">
          <%
          ResourceBundle rbPN = ResourceBundle.getBundle("clientnumbers");
          String suppressedPolicies = rbPN.getString("suppressedclientnumbers");
          if (arForm.getOwnerClientNo() == null || 
        		  suppressedPolicies.indexOf(arForm.getOwnerClientNo().trim()) == -1) {
          %>
          <logic:iterate id="req" name="<%=detailForm%>" property="requirements" scope="session">
            <%
            if (i%2 == 0) {
              tr_class = "row1";
            } else {
              tr_class = "row2";
            }
            %>
            <tr>
              <td class="<%=tr_class%>" valign="top">
              <% if (arForm.getRefNo().equals(((RequirementForm)req).getRefNo())) { %>
	              <input type="checkbox" name="index">
	          <% } else { %>
	              <input type="checkbox" name="index" disabled>
	          <% } %>
              </td>
              <td class="<%=tr_class%>" valign="top" style="cursor:hand" onclick="setReqParam('<bean:write name="detailForm" property="refNo"/>', '<bean:write name="req" property="reqId"/>', 'requirementForm', '<%=contextPath%>/viewKOReqMedLabSpecial.do#requirementDetail');"><bean:write name="req" property="seq"/></td>
              <td class="<%=tr_class%>" valign="top" style="cursor:hand" onclick="setReqParam('<bean:write name="detailForm" property="refNo"/>', '<bean:write name="req" property="reqId"/>', 'requirementForm', '<%=contextPath%>/viewKOReqMedLabSpecial.do#requirementDetail');"><bean:write name="req" property="reqCode"/></td>
               <%
				String fnUrl = ((RequirementForm)req).getWmsImageLink(); 
            	if(contextPath.equalsIgnoreCase(wmsContextPath)){
			   %>
	
            <% if(fnUrl!= null && !fnUrl.equals("") ){ %>
              	<td class="<%=tr_class%>" valign="top" style="cursor:hand" align="center"><center><a href="<%=fnUrl%>" target="_blank">View Document</a></center></td>
            <% }else{%>
            	<td class="<%=tr_class%>" valign="top" style="cursor:hand" align="center"><center></center></td>
            <%} }%>
              <td class="<%=tr_class%>" valign="top" style="cursor:hand" onclick="setReqParam('<bean:write name="detailForm" property="refNo"/>', '<bean:write name="req" property="reqId"/>', 'requirementForm', '<%=contextPath%>/viewKOReqMedLabSpecial.do#requirementDetail');"><bean:write name="req" property="reqStatusDesc"/></td>
              <td class="<%=tr_class%>" valign="top" style="cursor:hand" onclick="setReqParam('<bean:write name="<%=detailForm%>" property="refNo"/>', '<bean:write name="req" property="reqId"/>', 'requirementForm', '<%=contextPath%>/viewKOReqMedLabSpecial.do#requirementDetail');"><bean:write name="req" property="statusDate"/></td>
              <td class="<%=tr_class%>" valign="top" style="cursor:hand" onclick="setReqParam('<bean:write name="<%=detailForm%>" property="refNo"/>', '<bean:write name="req" property="reqId"/>', 'requirementForm', '<%=contextPath%>/viewKOReqMedLabSpecial.do#requirementDetail');"><bean:write name="req" property="clientNo"/></td>
              <td class="<%=tr_class%>" valign="top" style="cursor:hand" onclick="setReqParam('<bean:write name="<%=detailForm%>" property="refNo"/>', '<bean:write name="req" property="reqId"/>', 'requirementForm', '<%=contextPath%>/viewKOReqMedLabSpecial.do#requirementDetail');"><bean:write name="req" property="comment"/></td>
              <td class="<%=tr_class%>" valign="top" align="center" style="cursor:hand" onclick="setReqParam('<bean:write name="<%=detailForm%>" property="refNo"/>', '<bean:write name="req" property="reqId"/>', 'requirementForm', '<%=contextPath%>/viewKOReqMedLabSpecial.do#requirementDetail');"><bean:write name="req" property="clientType"/></td>
              <td class="<%=tr_class%>" valign="top" align="center" style="cursor:hand" onclick="setReqParam('<bean:write name="<%=detailForm%>" property="refNo"/>', '<bean:write name="req" property="reqId"/>', 'requirementForm', '<%=contextPath%>/viewKOReqMedLabSpecial.do#requirementDetail');"><bean:write name="req" property="reqLevel"/></td>
              <td class="<%=tr_class%>" valign="top" style="cursor:hand" onclick="setReqParam('<bean:write name="<%=detailForm%>" property="refNo"/>', '<bean:write name="req" property="reqId"/>', 'requirementForm', '<%=contextPath%>/viewKOReqMedLabSpecial.do#requirementDetail');"><bean:write name="req" property="followUpDate"/></td>
              <td class="<%=tr_class%>" valign="top" style="cursor:hand" onclick="setReqParam('<bean:write name="<%=detailForm%>" property="refNo"/>', '<bean:write name="req" property="reqId"/>', 'requirementForm', '<%=contextPath%>/viewKOReqMedLabSpecial.do#requirementDetail');"><bean:write name="req" property="dateSent"/></td>
              <td class="<%=tr_class%>" valign="top" style="cursor:hand" onclick="setReqParam('<bean:write name="<%=detailForm%>" property="refNo"/>', '<bean:write name="req" property="reqId"/>', 'requirementForm', '<%=contextPath%>/viewKOReqMedLabSpecial.do#requirementDetail');"><bean:write name="req" property="testDate"/></td>
                        
            </tr>
            <%i++;%>
            <input type="hidden" name="indexTemp"    value="">
            <input type="hidden" name="reqId"        value='<bean:write name="req" property="reqId"/>'>
            <input type="hidden" name="statusCode"   value='<bean:write name="req" property="reqStatusCode"/>'>
            <input type="hidden" name="followUpNo"   value='<bean:write name="req" property="followUpNo"/>'>
            <input type="hidden" name="seq"          value='<bean:write name="req" property="seq"/>'>
            <input type="hidden" name="followUpDate" value='<bean:write name="req" property="followUpDate"/>'>
            <input type="hidden" name="reqLevel"     value='<bean:write name="req" property="reqLevel"/>'>
            <input type="hidden" name="reqCodes"     value='<bean:write name="req" property="reqCode"/>'>
            <input type="hidden" name="reqRefNo"     value='<bean:write name="req" property="refNo"/>'>
            <input type="hidden" name="dateSent"     value='<bean:write name="req" property="dateSent"/>'>
            <input type="hidden" name="testDate"     value='<bean:write name="req" property="testDate"/>'>
           
          </logic:iterate>
          <%
          } else {
        	  out.println("Contents have been suppressed.");
          }
          %>
          <% if (i == 0) { %>
          <tr>
            <td class="label2" colspan="11" nowrap><bean:message key="message.noexisting" arg0="requirements"/></td>
          </tr>
           <% } %>
        </table>
      </div>
      <script language="Javascript">
       var form = document.prequirementForm; //document.forms[0];
       <% if (!( (Long.parseLong(currentStat) == IUMConstants.STATUS_UNDERGOING_ASSESSMENT)  ||
                 (Long.parseLong(currentStat) == IUMConstants.STATUS_AWAITING_REQUIREMENTS)  ||
                 (Long.parseLong(currentStat) == IUMConstants.STATUS_FOR_FACILITATOR_ACTION) ||
                 ((Long.parseLong(currentStat) == IUMConstants.STATUS_NB_REVIEW_ACTION) &&
                  (lob.equals(IUMConstants.LOB_PRE_NEED) || lob.equals(IUMConstants.LOB_GROUP_LIFE)) ) ) &&
                ( (Long.parseLong(currentStat) == IUMConstants.STATUS_AR_CANCELLED) ||
                 (EXECUTE_KO.equals("DISABLED"))) ) {%>

           form.selAll.disabled = true;
           <% if (i > 0 && i > 1) { %>
           if (form.index[0]) {
	           for (var i=0;i<form.index.length;i++) {
                   form.index[i].disabled = true;
    	       }
    	   }
    	  <% } else if (i == 1) { %>
    	   if (form.index) {
    	     form.index.disabled = true;
    	   }
   	   <%  } %>
       <% } %>
       <% if (i>0 && i>1) { %>
           countDisabledCheckBoxes(document.prequirementForm);
       <% } else if (i == 1) { %>
           countDisabledCheckBox(document.prequirementForm);
       <% } %>
      </script>
    </form>
  </body>
<%-- </html> --%>
