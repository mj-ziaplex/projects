<%@ page language="java" import="java.util.*,com.slocpi.ium.data.*, com.slocpi.ium.ui.form.*, com.slocpi.ium.util.*, com.slocpi.ium.ui.util.*, java.text.* " buffer="12kb"%>
<%@ taglib uri="/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/struts-html.tld" prefix="html" %>
<%@ taglib uri="/ium.tld" prefix="ium" %>

<%String contextPath = request.getContextPath();%>
<%
/* Andre Ceasar Dacanay - css enhancement - start */
String iumCss = "";
String companyCode = "";
try {
	ResourceBundle rbWMSSP = ResourceBundle.getBundle("dbconfig");
	iumCss = rbWMSSP.getString("IUM_CSS_VERSION");
	companyCode = iumCss;
	iumCss = "_"+iumCss;
} catch (Exception e){}
if(iumCss.equals("_CP")){
	companyCode="";
}else if(iumCss.equals("_GF")){
	companyCode = companyCode+" ";
}
/* Andre Ceasar Dacanay - css enhancement - end */
%>
<html>
<head>
<title><%=companyCode%>Integrated Underwriting and Medical System - Roles Selection</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<%=contextPath%>/css/pw_main.css"     rel="stylesheet" type="text/css">
<link href="<%=contextPath%>/css/main<%=iumCss%>.css" rel="stylesheet"  type="text/css">
<link href="<%=contextPath%>/css/styleCommon.css" rel="stylesheet" type="text/css">
<script language="JavaScript" src="<%=contextPath%>/js/roles.js"></script>
</head>

<body>
<html:errors/>  
<form name="newRoles"  >
<table width="400" border="0" align="center" cellpadding="5" cellspacing="0" class="dataTablejj">
 <tr>
  <td class="label2" valign="top">&nbsp;Available Roles</td>
  <td valign="top">&nbsp;</td>
  <td class="label2" valign="top">&nbsp;Selected Roles</td>
 </tr>
 <tr>
  <td valign="top">
  <select name="selectRemaining" size="12" multiple class="smallselect"  style="width:160px">
  <logic:iterate id="availableRoles" name="processConfigRole" property="availableRoles">
   <option value="<bean:write name="availableRoles" property="rolesId" />" ><bean:write name="availableRoles" property="rolesDesc" /></option>
   </logic:iterate>
  </select></td>
  <td align="center" valign="top"> 
   <table width="100%" border="0" cellspacing="5" cellpadding="0">
    <tr>
     <td align="center" valign="top"><input name="button1" type="button" class="menuselect" value=" Include &gt;&gt; " onclick="addListItem()" ></td>
    </tr>
    <tr>
     <td align="center" valign="top"><input name="button2" type="button" class="menuselect" value="&lt;&lt; Remove " onClick="removeListItem()"></td>
    </tr>
    <tr>
     <td align="center" valign="top">&nbsp;</td>
    </tr>
    <tr>
     <td align="center" valign="top">&nbsp;</td>
    </tr>
   </table>
  </td>
  <td valign="top"><select name="selectUserDefined" size="12" multiple class="smallselect" style="width:160px">
        <logic:iterate id="assignedRoles" name="processConfigRole" property="assignedRoles">
   		<option value="<bean:write name="assignedRoles" property="rolesId" />" ><bean:write name="assignedRoles" property="rolesDesc" /></option>
   		</logic:iterate>
   	 	</select></td>
 </tr>
 <tr>
  <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
   </table></td>
 </tr>
 <tr align="center">
  <td valign="top"></td>
  <td valign="top"><input name="button5" type="button" class="menuselect" value="  Ok  " onclick="save()" >&nbsp;&nbsp;&nbsp;
  <input name="button6" type="button" class="menuselect" value="Cancel" onclick="window.close()" ></td>
  <td valign="top"></td>
 </tr>
</table>
</form>
</body>
</html>
