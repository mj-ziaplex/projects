<%@ page language="java" import="java.util.*,com.slocpi.ium.data.*, com.slocpi.ium.ui.form.*, com.slocpi.ium.util.*, com.slocpi.ium.ui.util.*, java.text.* " buffer="12kb"%>
<jsp:useBean id="processConfigForm" scope="request" class="com.slocpi.ium.ui.form.ProcessConfigForm"/>
<%@ taglib uri="/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/struts-html.tld" prefix="html" %>
<%@ taglib uri="/ium.tld" prefix="ium" %>
<%String contextPath = request.getContextPath();
StateHandler sessionHandler = new StateHandler();
Access access = new Access();
%>
<%
/* Andre Ceasar Dacanay - css enhancement - start */
String iumCss = "";
String iumBorderColor = "";
String iumBgColor = "";
String iumBgColorMOut = "";
String iumBgColorMOv = "";
String companyCode = "";
try {
	ResourceBundle rbWMSSP = ResourceBundle.getBundle("dbconfig");
	iumCss = rbWMSSP.getString("IUM_CSS_VERSION");
	companyCode = iumCss;
	iumCss = "_"+iumCss;
} catch (Exception e){}

if(iumCss.equals("_CP")){
	iumBgColorMOv="#2A4C7C";
	iumBgColorMOut="#BBBBBB";
	iumBorderColor="bordercolor=\"#2A4C7C\"";
	iumBgColor="bgcolor=\"#2A4C7C\"";
	companyCode="";
}else if(iumCss.equals("_GF")){
	iumBgColorMOv="#303A77";
	iumBgColorMOut="#BBBBBB";
	iumBorderColor="bordercolor=\"#303A77\"";
	iumBgColor="bgcolor=\"#303A77\"";
	companyCode = companyCode+" ";
}else{
	iumBgColorMOv="#2A4C7C";
	iumBgColorMOut="#BBBBBB";
	iumBorderColor="bordercolor=\"#2A4C7C\"";
	iumBgColor="bgcolor=\"#2A4C7C\"";
}
/* Andre Ceasar Dacanay - css enhancement - end */
%>
<%
    // This routine queries the access rights of the users (RCRIVERA)
    //
    // --- START --- 
    
	HashMap userAccess = sessionHandler.getUserAccess(request);
    if(!access.hasAccess(userAccess,IUMConstants.PAGE_PROCESS_CONFIG,IUMConstants.ACC_INQUIRE) ){                                                                                                                    
    	response.sendRedirect("noAccessPage.jsp");   
    } 
    
    String CREATE_ACCESS = "";
    String MAINTAIN_ACCESS = "";
       
    if(!access.hasAccess(userAccess,IUMConstants.PAGE_PROCESS_CONFIG,IUMConstants.ACC_CREATE) ){                                                                                                                     
	  CREATE_ACCESS = "DISABLED"; 	
    }
    if(!access.hasAccess(userAccess,IUMConstants.PAGE_PROCESS_CONFIG,IUMConstants.ACC_MAINTAIN )){                                                                                                                     
	  MAINTAIN_ACCESS = "DISABLED"; 	
    }
   
    
    
    // --- END ---
    UserData userData = sessionHandler.getUserData(request); 
	UserProfileData userPref = userData.getProfile();
 %>    

<html>

<head>
<title><%=companyCode%>Integrated Underwriting and Medical System</title>

<link href="<%=contextPath%>/css/pw_main.css" rel="stylesheet" type="text/css">
<link href="<%=contextPath%>/css/main<%=iumCss%>.css" rel="stylesheet" type="text/css">

<script language="JavaScript" src="<%=contextPath%>/js/dhtml_init.js"></script>
<script language="JavaScript" src="<%=contextPath%>/js/navigation.js"></script>
<script language="JavaScript" src="<%=contextPath%>/jsp/sl_left_nav.jsp"></script>
<script language="Javascript">
  	<!--
  	function lobChange() {
  		var form = document.forms[0];
  		form.actionType.value = "refresh";
  		form.submit();
  	}
  	
  	function enableButtons() {
  		var form = document.forms[0];
  		form.saveButton.disabled = false;
  		form.cancelButton.disabled = false;
  	}
  	function enableCreate() {
  		var form = document.forms[0];
  		form.eventName.disabled = false;
  		form.eventName.focus();
  		form.fromStatus.disabled = false;
  		form.toStatus.disabled = false;
  		form.notificationTemplate.disabled = false;
  		form.activeInd.disabled = false;
  		document.getElementById('defaultButtons').innerHTML="<input class='button1' type='submit' value='Create' id=create name=create onClick='javascript:return(validateCreate());'>" +
 													 "<input class='button1' type='button' value='Cancel' id=cancel name=cancel onClick='javascript:lobChange()'>";
		document.getElementById('defaultButtons2').innerHTML="<input class='button1' type='submit' value='Create' id=create name=create onClick='javascript:return(validateCreate());'>" +
 													 "<input class='button1' type='button' value='Cancel' id=cancel name=cancel onClick='javascript:lobChange()'>";
 		document.getElementById('roles').href = "javascript:roleSetting()"
 		
  	}
  	function validate() {
  		var form = document.forms[0];
  		var toStatus = form.toStatus.value;
  		var description  = form.eventName.value;
  		if (description == "") {
  			alert('<bean:message key="error.field.required" arg0="Description"/>');
  			form.eventName.focus();
  			return false;
  		}
  		if (toStatus == "") {
  			alert('<bean:message key="error.field.requiredselection" arg0="TO status"/>');
  			form.toStatus.focus();
  			return false;
  		}
  		return true;
  	}
  	function validateCreate() {
  		var form = document.forms[0];  		
  		form.actionType.value = "create";
  		if (validate()) {
  			return true;
  		} else {
  			return false;
  		}
  	}
  	function validateUpdate() {
  		var form = document.forms[0];  		
  		form.actionType.value = "update";
  		if (validate()) {
  			return true;
  		} else {
  			return false;
  		}
  	}  	
  	function maintainEvent() {
  		var form = document.forms[0];
  		form.actionType.value = "maintain";  		
  		var len = 0;
  		if (form.eventId1[0]) {
  			len = form.eventId1.length;
  		}
  		var isNotChecked = true;
  		if (len == 0) {
  			if (form.eventId1.checked == true) {
  				form.eventId.value = form.eventId1.value;  		
  				isNotChecked = false;
  			}  			
  		}else {  		
	  		for (var i=0;i<len;i++) {
	  			if (form.eventId1[i].checked == true) {
	  				form.eventId.value = form.eventId1[i].value;  		
	  				isNotChecked = false;
	  			}
	  		}
  		}
  		if (isNotChecked) {
  			alert('<bean:message key="error.field.noselection" arg0="process configuration" arg1="to update"/>');
  			return false;
  		}
  		form.submit();
  	}
  	function addNewStatus() {
  		var form = document.forms[0];
  		var status1 = form.status1.value;
  		if (status1 == "") {
  			alert('<bean:message key="error.field.requiredselection" arg0="status"/>');
  			form.status1.focus();
  			return false;
  		} else {
  			form.actionType.value = "addNewStatus";
  			form.submit();
  		}
  		
  	}  	
  	function goToPage(objType) {
  		var form = document.forms[0];
  		if (objType == '<%=IUMConstants.ASSESSMENT_REQUEST%>') {
  			form.objectType.value = '<%=IUMConstants.ASSESSMENT_REQUEST%>';
  			form.actionType.value = "refresh";
  		}else 
  		if (objType == '<%=IUMConstants.POLICY_REQUIREMENTS%>') {
  			form.objectType.value = '<%=IUMConstants.POLICY_REQUIREMENTS%>';
  			form.actionType.value = "refresh";
  		} else
  		if (objType == '<%=IUMConstants.MEDICAL_RECORDS%>') {
  			form.objectType.value = '<%=IUMConstants.MEDICAL_RECORDS%>';
  			form.actionType.value = "refresh";
  		} else
  		if (objType == 'DocsReq') {
  			form.action = '<%=contextPath%>/confDocsRequirements.do';  			
  			form.actionType.value = "refresh";
  		}  		
  		form.submit();
  	}
  	
  	function roleSetting(){
  		//ksantos 03262004
  		var eventId = document.forms[0].eventId.value;
  		var lob = document.forms[0].lob.value;
  		var roles = document.forms[0].roles.value;
  		var flagRoles = document.forms[0].flagRoles.value;
  		
  		//if(roles == ""){
  			displayWindow('listRoles.do?lob='+lob+'&eventId='+eventId+'&roles='+roles+'&flagRoles='+flagRoles,'Settings','490','270','');
  		//}else{
  			//alert("roles has value");
  		//}
  	}
  	
  	function displayWindow(theURL,winName,width,height,features) { //v3.1
	// Made by Eddie Traversa modified from Macromedia Code
	// http://nirvana.media3.net/
    	var window_width = width;
	    var window_height = height;
	    var newfeatures= features;
	    var window_top = (screen.height-window_height)/2;
	    var window_left = (screen.width-window_width)/2;
	    popupwin = window.open(''+ theURL + '',''+ winName + '','width=' + window_width + ',height=' + window_height + ',top=' + window_top + ',left=' + window_left + ',features=' + newfeatures + '');
	    popupwin.focus();
	}

	function putButton(numRecordsPerView){
		var form = document.forms[0];
		
		if(numRecordsPerView == null){
			numRecordsPerView = 0;
		}
		if(document.all["defaultButtons2"] != null){
			if(numRecordsPerView >= 10 ){
		        document.all["defaultButtons2"].style.display = "block";
		    }
		    else{
    		   document.all["defaultButtons2"].style.display = "none";
		    }
		}
		//if(form.actionType.value == "create" || form.actionType.value == "maintain"){ removed this code causing javascript error
		if(form.actionType.value == "maintain"){
			form.eventName.focus();
		}
		
	}
  	//-->
  </script>
</head>


<BODY leftMargin=0 topmargin=0 marginwidth=0 marginheight=0 onload="putButton('<%=processConfigForm.getProcessConfigEntries()!=null?processConfigForm.getProcessConfigEntries().size():0%>')" onUnload="closePopUpWin();">
<table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%">
<tr><td width="100%" colspan="2">
 	<!-- HEADER -->
      <jsp:include page="header.jsp" flush="true"/>
    <!-- HEADER -->
<table border="0" cellpadding="0" cellspacing="0" width="100%" height="100%">
  <tr vAlign=top> 
    <TD background="<%=contextPath%>/images/bg_yellow.gif" id=leftnav vAlign=top width=147> 
      <IMG height=9 src="<%=contextPath%>/images/sp.gif" width=147><BR>
      <IMG height=30 src="<%=contextPath%>/images/sp.gif" width=7> <BR>
      <IMG height=7 src="<%=contextPath%>/images/sp.gif" width=147><BR>
      <DIV id=level0 style="POSITION: absolute; VISIBILITY: hidden; WIDTH: 100%; Z-INDEX: 500"> 
      <link href="<%=contextPath%>/css/styleCommon.css" rel="stylesheet" type="text/css">
        <SCRIPT language=javascript>
			<!--
				document.write(writeMenu(''))
				if (IE4) { document.write(writeDiv()) };
				
			//-->
			</SCRIPT>
      </DIV>
      <DIV id=NSFix style="POSITION: absolute; VISIBILITY: hidden; Z-INDEX: 999">&nbsp;</DIV>
      <!-- Netscape needs a hard-coded DIV to write dynamic DIVs --> 
      <SCRIPT language=javascript>
                        <!--
                                if (NS4) { document.write(writeDiv()) }
                        //-->
                        </SCRIPT>
                        <SCRIPT>
                        <!--
                                initMenu();
                        //-->
                        </SCRIPT>
      <!-- END MENU CODE --> </td>
<form name="frm" action="<%=contextPath%>/configureWorkflow.do" method="post">      
	<input type="hidden" name="actionType" value="<%=processConfigForm.getActionType()%>">
	<input type="hidden" name="objectType" value="<%=IUMConstants.ASSESSMENT_REQUEST%>">
    <td width="100%"> 
      <div align="left"> 
        <table border="0" cellpadding="0" width="100%" cellspacing="0">
          <tr> 
            <td width="100%"> 
              <div align="left"> 
                <table border="0" cellpadding="3" cellspacing="0" width="100%" height="27"  background="<%=contextPath%>/images/sectionbkgray.jpg">
                  <tr> 
                    <td valign="top"> 
                       <span class="main">&nbsp;Integrated Underwriting and Medical System </span>
                    </td>
                  </tr>
                </table>
              </div>
            </td>
          </tr>
          <tr> 
            <td width="100%" colspan=2>&nbsp;</td>
          </tr>
        </table>
        <table width="100%" cellpadding="3" cellspacing="5" border="0">
          <tr valign="top">
         <td class="title2">Process Configuration Maintenance
          <table border="1" <%=iumBorderColor%> cellpadding=10 cellspacing=0 bgcolor="" width="900" class="listtable1">
           <tr><td colspan="2" height="100%">
              <table border="0" cellpadding="2" cellspacing="1" bgcolor=#FFFFFF style="border: 0px solid #D1D1D1;" class="listtable1">
                <tr ><td>

			<table border="0" cellpadding="10" cellspacing="0" height="100%" width="100%">						
				<tr>
     <td>
      <table border="0" cellpadding="2" cellspacing="0" width="900">
					  <tr>
        <td class="label2"><b>Line of Business</b></td>															
								<td width="750">
									<ium:list className="label2" listBoxName="lob" type="<%=IUMConstants.LIST_LOB%>" selectedItem='<%=processConfigForm.getLob()%>' onChange="javascript:lobChange();"/>						
								</td>
							</tr>
							<tr>
								<td colspan="2">&nbsp;</td>
								<html:errors/>
							</tr>
						</table>
		   </td>
		  </tr> 
				<tr>					
					<td width="100%" height="100%" valign="top">
					<!--tabs navigation-->			
						<table cellpadding="1" cellspacing="0">
							<tr>
								<td <%=iumBgColor%>>
									<table border="0" cellpadding="3" cellspacing="0">
		        <tr>
		         <td class="tabSelected"><b>Assessment Request</b></td>
		        </tr>
		       </table>
		      </td>
		      <td></td>
		      <td bgcolor="#BBBBBB" style="cursor:hand" onmouseover="this.style.backgroundColor = '<%=iumBgColorMOv %>'" onmouseout="this.style.backgroundColor = '<%=iumBgColorMOut %>'">
		       <table border="0" cellpadding="3" cellspacing="0">
		        <tr><a href="#" onClick="javascript:goToPage('<%=IUMConstants.POLICY_REQUIREMENTS%>');"><td class="tabLink"><b>Requirement Management</b></td></a></tr>
		       </table>
		      </td>
		      <td></td>
		      <td bgcolor="#BBBBBB" style="cursor:hand" onmouseover="this.style.backgroundColor = '<%=iumBgColorMOv %>'" onmouseout="this.style.backgroundColor = '<%=iumBgColorMOut %>'">
		       <table border="0" cellpadding="3" cellspacing="0">
		        <tr><a href="#" onClick="javascript:goToPage('<%=IUMConstants.MEDICAL_RECORDS%>');"><td class="tabLink"><b>Medical Records</b></td></a></tr>
		       </table>
		      </td>
		      <td></td>
		      <td bgcolor="#BBBBBB" style="cursor:hand" onmouseover="this.style.backgroundColor = '<%=iumBgColorMOv %>'" onmouseout="this.style.backgroundColor = '<%=iumBgColorMOut %>'">
		       <table border="0" cellpadding="3" cellspacing="0">
		        <tr><a href="#" onClick="javascript:goToPage('DocsReq');"><td class="tabLink"><b>Documents and Requirements</b></td></a></tr>
		       </table>
		      </td>
		      <td></td>       
		     </tr>
		    </table>
		    <!-- end of navigation tabs -->
		    <table cellpadding="0" cellspacing="0" border="1" <%=iumBorderColor%> width="100%">
							<tr>
								<td <%=iumBgColor%> colspan="11">&nbsp;</td>
							</tr>
							<tr>							
								<td>
									<table cellpadding="10" cellspacing="0" border="0" width="100%">
										<tr>
											<td valign="top"  width="100%">
												<!-- start of STATUS GROUP -->
												<table border="0" <%=iumBorderColor%> cellpadding="0" cellspacing="0" bgcolor="" width="100%">
													<tr>
														<td class="label2">
															<b>STATUS</b>
														</td>
													</tr>  
													<tr>
														<td valign="top">
															<table border="0" cellpadding="2" cellspacing="0" width="250">
																<tr>
																	<td valign="top">
		               	<table class="listTable1" width="100%">
																			<tr class="headerrow1">
																				<td width="250" align="center">
																					STATUS
																				</td>
																			</tr>
																			<%int i=0;%>
																			<logic:present name="processConfigForm" property="availableStatuses">																				
																				<logic:iterate id="status" name="processConfigForm" property="availableStatuses">																		
																				<%if (i%2 == 0) {%>
																					<tr class="row1">
																				<%}else {%>
																					<tr class="row2">
																				<%}%>
																					<td width="250">																						
																						<bean:write name="status" property="name"/>
																					</td>
																				</tr>
																				<%i++;%>
																				</logic:iterate>
																			</logic:present>
																			
																			<%if (i%2 == 0) {%>
																				<tr class="row1">
																			<%}else {%>
																				<tr class="row2">
																			<%}%>
																				<%String disableStatusList = "true";%>
																				<logic:notEmpty name="processConfigForm" property="lob">
																					<%disableStatusList = "false";%>
																					<logic:equal name="processConfigForm" property="actionType" value="maintain">
																					<%disableStatusList = "true";%>
																					</logic:equal>
																				</logic:notEmpty>
																				<td width="250">
																					<ium:list className="label2" listBoxName="status1" type="<%=IUMConstants.LIST_OBJ_STATUS%>" objectType="<%=IUMConstants.ASSESSMENT_REQUEST%>" lob="<%=processConfigForm.getLob()%>" disabled="<%=disableStatusList%>"/>
																				</td>
																			</tr>
																		</table>	
		               </td>
		              </tr>
		              <tr>  
																	<td align="left">
																		<logic:notEmpty name="processConfigForm" property="lob">
																		<logic:notEqual name="processConfigForm" property="actionType" value="maintain">
																		<input class="button1" type="button" value="Add Status" id=add name=add <%=CREATE_ACCESS%> onClick="javascript:return(addNewStatus());"/>
																		</logic:notEqual>
																		</logic:notEmpty>
																	</td>    
		              </tr>
		             </table>
		            </td>
		           </tr>                                
											 </table>
											 <!-- end of STATUS group -->
										 </td>																					 
									 </tr>	
									 <tr>											 
										 <td>
												<!-- start of EVENTS group -->
							     <table border="0" <%=iumBorderColor%> cellpadding="0" cellspacing="0" bgcolor="" width="100%">
		           <tr>
													 <td class="label2">
														 <b>EVENTS</b>
														</td>
													</tr>  
													<tr><td>&nbsp;</td></tr>
											
													<tr><td align="left">
																		<div id="defaultButtons2" style="display:inline">
																		<logic:notEmpty name="processConfigForm" property="lob">	
																		 <%if ("maintain".equals(processConfigForm.getActionType())) {%>
																		 	<input class='button1' type='submit' value='Save' id=update name=update onClick='javascript:return(validateUpdate());'>
																		 	<input class='button1' type='button' value='Cancel' id=cancel name=cancel onClick='javascript:lobChange()'>
																		 <%}else {%>				
																		<input class="button1" type="button" value="Create" id=add name=add <%=CREATE_ACCESS%> onClick="javascript:enableCreate();">
																		<logic:notEmpty name="processConfigForm" property="processConfigEntries">
 																	    	<input class="button1" type="button" value="Maintain" id=maintain name=maintain onClick="javascript:maintainEvent();" <%=MAINTAIN_ACCESS%> >
 																	    </logic:notEmpty>
 																	    <%}%>	
 																	    </logic:notEmpty>
																		</div>
														</td>
														</tr>
													
		           <tr>
		            <td valign="top">
		             <table border="0" cellpadding="2" cellspacing="0" width="100%">
		              <tr>
		               <td valign="top">                       																				
																		<table class="listTable1" width="100%">
																			<tr class="headerrow1">
																				<td width="20">
																					&nbsp;
																				</td>
																				<td width="200">
																					DESCRIPTION
																				</td>

																		 	<td width="150">
																			 	FROM
																			 </td>
																				<td width="150">
																					TO
																				</td>
																				<td width="300">
																					NOTIFICATION
																				</td>
																				<td width="50">
																					ACTIVE
																				</td>
																			</tr>
																			<%int k = 0;%>
																			<logic:present name="processConfigForm" property="processConfigEntries">
																				<logic:iterate id="processConfig" name="processConfigForm" property="processConfigEntries">
																					<%if (k%2 == 0) {%>
																						<tr class="row1">
																					<%}else {%>
																						<tr class="row2">
																					<%}%>
																					 <td>
																							<input name="eventId1" value="<%=((ProcessConfigData)processConfig).getEventId()%>" type="radio" class="label2"/>
																						</td>
		
																					 <td>
																							<bean:write name="processConfig" property="eventName"/>
																						</td>
		
																					 <td>
																							<ium:description type="<%=IUMConstants.LIST_PC_STATUS%>" queryCode="<%=((ProcessConfigData)processConfig).getFromStatus()%>"/>
																						</td>
																						<td>
																							<ium:description type="<%=IUMConstants.LIST_PC_STATUS%>" queryCode="<%=((ProcessConfigData)processConfig).getToStatus()%>"/>
																						</td>
																						<td>
																							<ium:description type="<%=IUMConstants.LIST_NOTIFICATION_TEMPLATES%>" queryCode="<%=((ProcessConfigData)processConfig).getNotification()%>"/>
																						</td>
																						<td align="center">
																							<%if (((ProcessConfigData)processConfig).isNotificationInd()) {%>
																								Y
																							<%}else {%>
																								N
																							<%}%>		
																						</td>
																					</tr>
																					<%k++;%>
																				</logic:iterate>
																			</logic:present>
																			
																			<%String actionType = processConfigForm.getActionType();
																			  String disabled = "true";
																			  String disabled2 = "disabled"	;	
																				if (actionType != null && !"".equals(actionType)) {
																					if ("maintain".equals(actionType)) {
																						disabled = "false";
																						disabled2 = "";																						
																					}	
																				}
																			%>
																			<%if (k%2 == 0) {%>
																				<tr class="row1">
																			<%}else {%>
																				<tr class="row2">
																			<%}%>
																				<td>
																					<%-- ksantos 03262004 --%>
																					<a id="roles"  <logic:equal name="processConfigForm" property="actionType" value="maintain">href="javascript:roleSetting()"</logic:equal> >
																								<img src=<%=contextPath%>/images/roles.gif alt="ROLES" border=1 width=15 height=15></a>
																					<input type="hidden" name="eventId" value="<%=processConfigForm.getEventId()%>"/>
																					<input type="hidden" name="roles" value="" />
																					<input type="hidden" name="flagRoles" value="" />
																					<%--ksantos 03262004 --%>
																				</td>
																			 	<td>
																					<span class="required">*</span><input name="eventName" type="text" value='<bean:write name="processConfigForm" property="eventName"/>' maxlength="25" size="30" class="label2" <%=disabled2%>/>
																				</td>
																				
																				<td>
																					<ium:list className="label2" listBoxName="fromStatus" type="<%=IUMConstants.LIST_PC_STATUS%>" lob='<%=processConfigForm.getLob()%>' objectType="<%=IUMConstants.ASSESSMENT_REQUEST%>" selectedItem="<%=processConfigForm.getFromStatus()%>" disabled="<%=disabled%>"/>						
																				</td>
																				<td>
																					<span class="required">*</span><ium:list className="label2" listBoxName="toStatus" type="<%=IUMConstants.LIST_PC_STATUS%>" lob='<%=processConfigForm.getLob()%>' objectType="<%=IUMConstants.ASSESSMENT_REQUEST%>" selectedItem="<%=processConfigForm.getToStatus()%>" disabled="<%=disabled%>"/>						
																				</td>
																				<td align="center">
																					<ium:list className="label2" listBoxName="notificationTemplate" type="<%=IUMConstants.LIST_NOTIFICATION_TEMPLATES%>" onChange="" selectedItem="<%=processConfigForm.getNotificationTemplate()%>" disabled="<%=disabled%>"/>						
																				</td>
																				<td align="center">
																					<%String mode = "";%>
																					<%if (IUMConstants.YES.equals(processConfigForm.getActiveInd())) {%>
																						<%mode = "checked";%>
																					<%}%>
																					<input type="checkbox" value="<%=IUMConstants.YES%>" id=checkbox1 name="activeInd" <%=mode%> <%=disabled2%>/>
																				</td>
																			</tr>
		                </table>
																	</td>
		              </tr>
		              <tr>  
																	<td align="left">
																		<div id="defaultButtons" style="display:inline">
																		<logic:notEmpty name="processConfigForm" property="lob">	
																		 <%if ("maintain".equals(processConfigForm.getActionType())) {%>
																		 	<input class='button1' type='submit' value='Save' id=update name=update onClick='javascript:return(validateUpdate());'>
																		 	<input class='button1' type='button' value='Cancel' id=cancel name=cancel onClick='javascript:lobChange()'>
																		 <%}else {%>				
																		<input class="button1" type="button" value="Create" id=add name=add <%=CREATE_ACCESS%> onClick="javascript:enableCreate();">
																		<logic:notEmpty name="processConfigForm" property="processConfigEntries">
 																	    	<input class="button1" type="button" value="Maintain" id=maintain name=maintain onClick="javascript:maintainEvent();" <%=MAINTAIN_ACCESS%> >
 																	    </logic:notEmpty>
 																	    <%}%>	
 																	    </logic:notEmpty>
																		</div>
																	</td>    
		              </tr>
		             </table>
		            </td>
		           </tr>             
		          </table>
		          <!-- end of EVENTS group -->
											</td>  
										</tr> 
									</table>
								</td>								
							</tr>
						</table>			
						<!-- end of navigation tabs -->																															
					</td>
				</tr>
			</table>
			<!-- end of body -->                
                  
<!--- END OF BODY -->
</form>
</td>
</tr>
</table>
</div>
</body>
</html>