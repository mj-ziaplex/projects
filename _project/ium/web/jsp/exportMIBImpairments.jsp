<%@ taglib uri="/struts-html.tld" prefix="html" %>
<%@ taglib uri="/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/struts-logic.tld" prefix="logic" %>
<%@page language="java" import="java.util.*" %>
<jsp:useBean id="mibImpairmentsForm" scope="request" class="com.slocpi.ium.ui.form.MIBImpairmentsForm"/>
<% String contextPath = request.getContextPath(); %>
<%
/* Andre Ceasar Dacanay - css enhancement - start */
String iumCss = "";
String iumColorB = "";
String iumColorO = "";
String companyCode = "";
try {
	ResourceBundle rbWMSSP = ResourceBundle.getBundle("dbconfig");
	iumCss = rbWMSSP.getString("IUM_CSS_VERSION");
	companyCode = iumCss;
	iumCss = "_"+iumCss;
} catch (Exception e){}
if(iumCss.equals("_CP")){
	iumColorB="#2A4C7C";
	iumColorO="#FFCB00";
	companyCode="";
}else if(iumCss.equals("_GF")){
	iumColorB="#303A77";
	iumColorO="#303A77";
	companyCode = companyCode+" ";
}else{
	iumColorB="#2A4C7C";
	iumColorO="#FFCB00";
}
/* Andre Ceasar Dacanay - css enhancement - end */
%>
<html>
<body>
<head>
<title><%=companyCode%>Integrated Underwriting and Medical System</title>
<link href="<%=contextPath%>/css/main<%=iumCss%>.css"        rel="stylesheet" type="text/css">
<link href="<%=contextPath%>/css/pw_main.css"     rel="stylesheet" type="text/css">
<link href="<%=contextPath%>/css/styleCommon.css" rel="stylesheet" type="text/css">
<script language="JavaScript" src="<%=contextPath%>/js/navigation.js"></script>
<script language="JavaScript" src="<%=contextPath%>/js/validation.js"></script>
<script language="JavaScript">
function loadDefaultDate() {
    var form = document.forms[0];
    form.startDate.value = getFormattedCurrentDate();
    form.endDate.value   = getFormattedCurrentDate();
}

function exportMIB(contextPath) {
   var form = document.forms[0];
   if (trim(form.startDate.value) == "") {
       alert("<bean:message key="error.field.required" arg0="Start Date"/>");	        
       return;
   }
   else {
	   if (!isValidDate(form.startDate.value)) {
    	  alert("<bean:message key="error.field.date" arg0="Start Date"/>");
	      return;   
	   }
   }
   if (trim(form.endDate.value) == "") {
       alert("<bean:message key="error.field.required" arg0="End Date"/>");	        
       return;
   }
   else {
      if (!isValidDate(form.endDate.value)) {
   		   alert("<bean:message key="error.field.date" arg0="End Date"/>");   
	       return;   
	  }   
   }
   if (isGreaterDate(form.startDate.value, form.endDate.value)) {
      alert("<bean:message key="error.field.greaterthanequalto" arg0="End Date" arg1="Start Date"/>");   
      return;
   }
   form.action = contextPath + "/exportMIBImpairments.do?mode=export";
   form.submit();
}
</script>
</head>

<BODY leftMargin=0 topmargin=0 marginwidth=0 marginheight=0 onLoad="loadDefaultDate();">
<table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%">
<tr><td width="100%" colspan="2">
<jsp:include page="header.jsp" flush="true"/>
</td>
</tr>
<tr>
  <td rowspan="2" bgcolor="<%=iumColorO%>" height="100%" width="20"><img src="<%=contextPath%>/images/spacer.gif" width="20"></td>
            <td background="<%=contextPath%>/images/GradientGray.jpg" height="27" width="100%" valign="top">&nbsp;&nbsp;
              <span class="main">Integrated Underwriting and Medical System</span>
            </td>
          </tr>
          <tr>
            <td width="100%" height="100%" valign="top">
              <!-- BODY -->
        <table width="100%" cellpadding="3" cellspacing="5" border="0">
          <tr> 
            <td class="label2"><b>Export MIB Impairments</b>              
            </td>
          </tr>
          <tr valign="top"> 
            <td> 

<!--- START OF BODY -->
<form name="frm" method="post">
<table border="1" bordercolor="<%=iumColorB%>" cellpadding=10 cellspacing=0 bgcolor="" width="500" class="listtable1">
        <tr><td colspan="2" height="100%">
        <table>
        <tr>
        <td class="label2" colspan="2"><b>Start Date</b><td/>
        <td colspan="2">
	<input type="text" maxLength="9" size="20" name="startDate" value="" class="label2">
        </td>
        </tr>
        <tr>
        <td class="label2" valign="top" colspan="2"><b>End Date</b><td/>
        <td colspan="2">
	<input type="text" maxLength="9" size="20" name="endDate" value=""  class="label2">
        </td>
        </tr>
        <tr><td colspan="4" align="right">&nbsp;</td></tr>

        <tr><td colspan="4" align="right">
        <input type="button" value="Export" class="button1" onclick="exportMIB('<%=contextPath%>');">&nbsp;
		<input type="button" value="Save" class="button1" onclick="gotoPage('#');">&nbsp;
        <input type="button" value="Cancel" class="button1" onclick="gotoPage('frm','<%=contextPath%>/jsp/admin.jsp');">

        </td></tr>
        
        
        </table>
		</td></tr>

</table>
</form>
<!--- END OF BODY -->
</td>
</tr>

</table>
</div>
</body>
</html>