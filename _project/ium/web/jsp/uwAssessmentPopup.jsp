<%@ page language="java" import="java.util.*, com.slocpi.ium.ui.form.*, com.slocpi.ium.util.*, com.slocpi.ium.ui.util.*, com.slocpi.ium.data.*, com.slocpi.ium.underwriter.*" buffer="1024kb"%>
<%@ taglib uri="/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/struts-html.tld" prefix="html" %>
<%String contextPath = request.getContextPath();%>
<%
/* Andre Ceasar Dacanay - css enhancement - start */
String iumCss = "";
try {
	ResourceBundle rbWMSSP = ResourceBundle.getBundle("dbconfig");
	iumCss = rbWMSSP.getString("IUM_CSS_VERSION");
	iumCss = "_"+iumCss;
} catch (Exception e){}
/* Andre Ceasar Dacanay - css enhancement - end */
%>
<html>
<head>
<link href="<%=contextPath%>/css/main<%=iumCss%>.css" rel="stylesheet" type="text/css">
</head>
<body>
<table border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" style="border: 1px solid #D1D1D1;" class="listtable1" ID="Table1" width="700">
	
	<logic:empty name="detailForm" property="listOfAnalysis">
  		<font align="center"><h3> No analysis to be displayed for this policy. </h3></font>
  	</logic:empty>
  	
  	<logic:notEmpty name="detailForm" property="listOfAnalysis">
  		
		<logic:iterate id="analysis" name="detailForm" property="listOfAnalysis">
		<%String analysisDesc = ((UWAnalysisForm)analysis).getAnalysis();%>
		<tr>
			<td class="headerrow1" width="55%"><div align="left">Posted By: <bean:write name="analysis" property="postedBy"/></div></td>
        	<td class="headerrow1"><div align="right">Posted On: <bean:write name="analysis" property="postedDate"/></div></td>
	    </tr>
    	<tr>
        	 <td class="row1" colspan="2"><%=analysisDesc%></td>
	     </tr>
		</logic:iterate>
	
	</logic:notEmpty>
</table>
</body>



</html>