<%@ page language="java" import="com.slocpi.ium.util.*" buffer="12kb"%>
<%@ taglib uri="/struts-bean.tld" prefix="bean" %>

<!--
	function emptyInsuredDetails(){
		var form = document.requestForm;
		form.insuredClientType.value='';
		form.insuredClientNo.value='';
		form.insuredAge.value='';
		form.insuredLastName.value='';
		form.insuredFirstName.value='';
		form.insuredMiddleName.value='';
		form.insuredTitle.value='';
		form.insuredSuffix.value='';
		form.insuredBirthDate.value='';
		form.insuredSex[0].checked=false;
		form.insuredSex[1].checked=false;
		form.sameClientDataCB.checked=false;
		form.sameClientData.value='';
		form.insuredOriginal.value='';
		
	}
	
	function emptyOwnerDetails(){
		var form = document.requestForm;
		form.ownerClientType.value='';
		form.ownerClientNo.value='';
		form.ownerAge.value='';
		form.ownerLastName.value='';
		form.ownerFirstName.value='';
		form.ownerMiddleName.value='';
		form.ownerTitle.value='';
		form.ownerSuffix.value='';
		form.ownerBirthDate.value='';
		form.ownerSex[0].checked=false;
		form.ownerSex[1].checked=false;
		form.ownerOriginal.value='';
		
		
	}




	function disableInsured(){
		var form = document.requestForm;
		if (form.lob.value=="<%=IUMConstants.LOB_INDIVIDUAL_LIFE%>"){
			form.sameClientDataCB.checked==true;
			form.sameClientData.value="Y";
			form.sameClientDataCB.disabled=true;
		}
		form.insuredClientType.disabled=true;
		form.insuredLastName.disabled=true;
		form.insuredFirstName.disabled=true;
		form.insuredMiddleName.disabled=true;
		form.insuredTitle.disabled=true;
		form.insuredSuffix.disabled=true;
		form.insuredBirthDate.disabled=true;
		form.insuredAge.disabled=true;
		form.insuredSex[0].disabled=true;
		form.insuredSex[1].disabled=true;
	
	}
		
	function disableOwner(){
		var form = document.requestForm;
		form.ownerClientType.disabled=true;		
		form.ownerLastName.disabled=true;
		form.ownerFirstName.disabled=true;
		form.ownerMiddleName.disabled=true;
		form.ownerTitle.disabled=true;
		form.ownerSuffix.disabled=true;
		form.ownerBirthDate.disabled=true;
		form.ownerAge.disabled=true;
		form.ownerSex[0].disabled=true;
		form.ownerSex[1].disabled=true;
	
	}	

	function enableInsured(){
		var form = document.requestForm;
		if (form.lob.value=="<%=IUMConstants.LOB_INDIVIDUAL_LIFE%>"){
			form.sameClientDataCB.disabled=false;
		}
		form.insuredLastName.disabled=false;
		form.insuredFirstName.disabled=false;
		form.insuredMiddleName.disabled=false;
		form.insuredTitle.disabled=false;
		form.insuredSuffix.disabled=false;
		form.insuredBirthDate.disabled=false;
		form.insuredAge.disabled=false;
		form.insuredSex[0].disabled=false;
		form.insuredSex[1].disabled=false;
	
	}
		
	function enableOwner(){
		var form = document.requestForm;
		form.ownerLastName.disabled=false;
		form.ownerFirstName.disabled=false;
		form.ownerMiddleName.disabled=false;
		form.ownerTitle.disabled=false;
		form.ownerSuffix.disabled=false;
		form.ownerBirthDate.disabled=false;
		form.ownerAge.disabled=false;
		form.ownerSex[0].disabled=false;
		form.ownerSex[1].disabled=false;
	
	}	


	function showInsuredDetails(){
	    document.all['INS1'].className = 'show';
		document.all['INS2'].className = 'show';
		document.all['INS3'].className = 'show';
		document.all['INS4'].className = 'show';		
		var form = document.requestForm;
		if(form.lob.value=="<%=IUMConstants.LOB_INDIVIDUAL_LIFE%>"){
			document.all['SAME1'].className='show';
			document.all['SAME2'].className='show';
		}
	}
	
	function hideInsuredDetails(){
	    document.all['INS1'].className = 'hide';
		document.all['INS2'].className = 'hide';
		document.all['INS3'].className = 'hide';
		document.all['INS4'].className = 'hide';		
		document.all['SAME1'].className = 'hide';
		document.all['SAME2'].className = 'hide';		
		
	}
	
	function showOwnerDetails(){
		var form = document.requestForm;
		if( (form.lob.value=="<%=IUMConstants.LOB_INDIVIDUAL_LIFE%>" && form.sameClientDataCB.checked==false )||
			form.lob.value=="<%=IUMConstants.LOB_PRE_NEED%>")		
		{		
		    document.all['OWN1'].className = 'show';
			document.all['OWN2'].className = 'show';
			document.all['OWN3'].className = 'show';
	  	    document.all['OWN4'].className = 'show';
	  	}
	}
	
	
	function hideOwnerDetails(){
	    document.all['OWN1'].className = 'hide';
		document.all['OWN2'].className = 'hide';
		document.all['OWN3'].className = 'hide';
		document.all['OWN4'].className = 'hide';		
	} 




	function changeDefaultClientType(){
		/*  Business Rules:
				1.  GL (Group Life) -- clientType="<%=IUMConstants.CLIENT_TYPE_MEMBER%>" (member)--> insured 
				2.  PN (Pre-need)		-- clientType="<%=IUMConstants.CLIENT_TYPE_PLANHOLDER%>" (planholder)--> owner
				3.  IL 				-- clientType="<%=IUMConstants.CLIENT_TYPE_INSURED%>" (default) may also enter Owner.
		*/
		var form = document.requestForm;
		var pass = false;
		if(isEmpty(form.lob2.value)){
			pass=true;
		}else{
			pass=confirm("Changing LOB will reset the fields for Client Details");
		}
		if (pass==true){
   			emptyOwnerDetails();
			emptyInsuredDetails();
			disableOwner();
			disableInsured();
			form.lob2.value = form.lob.value;
			clientTypeControl();
		}else{
			if(isEmpty(form.lob2.value)){
		    	form.lob.value='';
			}else{
			  	form.lob.value = form.lob2.value;
			}
		}
	}
 
	
	
	function clientTypeControl(){
		var form = document.requestForm;
		if (form.lob.value=="<%=IUMConstants.LOB_GROUP_LIFE%>"){
			showInsuredDetails();
			if ( isEmpty(form.ownerClientNo.value) ){
				hideOwnerDetails();
			}else{
				showOwnerDetails();
			}
			form.insuredClientType.value = "<%=IUMConstants.CLIENT_TYPE_MEMBER%>";
			form.refNo.maxLength=15;			
		}else if (form.lob.value=="<%=IUMConstants.LOB_PRE_NEED%>"){
			showOwnerDetails();
			hideInsuredDetails();
			form.ownerClientType.value = "<%=IUMConstants.CLIENT_TYPE_PLANHOLDER%>";
			form.refNo.maxLength=15;			
		}else if (form.lob.value=="<%=IUMConstants.LOB_INDIVIDUAL_LIFE%>") {
			showInsuredDetails();
			form.insuredClientType.value = "<%=IUMConstants.CLIENT_TYPE_INSURED%>";
			if( !isEmpty(form.ownerClientNo.value) && (form.ownerClientNo.value!=form.insuredClientNo.value) ){
				showOwnerDetails();
				form.ownerClientType.value = "<%=IUMConstants.CLIENT_TYPE_OWNER%>";					
			}else{
				hideOwnerDetails();
				form.sameClientDataCB.checked=true;
				emptyOwnerDetails();				
			}
			form.refNo.maxLength=10;
			form.refNo.value = form.refNo.value.substring(0,10);			
		}else{
			hideInsuredDetails();
			hideOwnerDetails();
		}
	}

	
	function initializePage(){
		var form = document.requestForm;
		if( isEmpty(form.insuredClientNo.value) ){
			disableInsured();
			emptyInsuredDetails();
			form.refNo.focus();
		}else{
			enableInsured();
		}
		if( isEmpty(form.ownerClientNo.value) ){
			disableOwner();
			emptyOwnerDetails();
		}else{
			enableOwner();
		}
		if (form.lob.value=="<%=IUMConstants.LOB_INDIVIDUAL_LIFE%>") {
			if (form.sameClientData.value=="Y"){
				form.sameClientDataCB.checked=true;
				hideOwnerDetails();
			}
		}
		clientTypeControl();
		//Alert Msg for no client Data retrieved.
		if(!isEmpty(form.clientToSearch.value)){
			if(form.clientToSearch.value=="<%=IUMConstants.CLIENT_TYPE_INSURED%>"){
				if(isEmpty(form.insuredFirstName.value)){
					alert("There is no existing client with this number - " + form.insuredClientNo.value);
					form.sameClientDataCB.select();
				}
				form.insuredLastName.focus();		
				
			}else if(form.clientToSearch.value=="<%=IUMConstants.CLIENT_TYPE_OWNER%>"){
			  if(form.ownerClientNo.value!='' && form.ownerClientNo.value!=form.insuredClientNo.value){
  				if(isEmpty(form.ownerFirstName.value)){
  					alert("There is no existing client with this number - " + form.ownerClientNo.value);
  					form.sameClientDataCB.select();
  				}
					form.ownerLastName.focus();
					
				}
			}
		}
	}

	
    function sameClientDataControl(){
		var form = document.requestForm;
    	if (form.sameClientDataCB.checked==true){
    		form.sameClientData.value="Y";
    		emptyOwnerDetails();				
    		hideOwnerDetails();

    	}else{
    		form.sameClientData.value="N";
				emptyOwnerDetails();
    		showOwnerDetails();
				form.ownerClientType.value = "<%=IUMConstants.CLIENT_TYPE_OWNER%>";
    		
    	}
    }
   function syncClientData(){
		var form = document.requestForm;
		form.ownerClientNo.value = form.insuredClientNo.value;
		form.ownerAge.value=form.insuredAge.value;
		form.ownerLastName.value=form.insuredLastName.value;
		form.ownerFirstName.value=form.insuredFirstName.value;
		form.ownerMiddleName.value=form.insuredMiddleName.value;
		form.ownerTitle.value=form.insuredTitle.value;
		form.ownerSuffix.value=form.insuredSuffix.value;
		form.ownerBirthDate.value=form.insuredBirthDate.value;
		if (form.insuredSex[0].checked==true){
			form.ownerSex[0].checked=true;
			form.ownerSex[1].checked=false;
		}else{
			form.ownerSex[0].checked=false;
			form.ownerSex[1].checked=true;
		}
    }
    
    //submit form to retrieve clientData
    function searchClient(client){
		var form = document.requestForm;
		form.clientToSearch.value=client;
		form.action = "createAssessmentRequest.do";
		form.submit();
    }
    
    //capture keyStroke: enter value=13 [INSURED]
	function getKeyInsured(keyStroke) {
	   var keyCode = (document.layers) ? keyStroke.which : event.keyCode;
   	   if(keyCode==13){
   	     searchClient("<%=IUMConstants.CLIENT_TYPE_INSURED%>");
   	   }
	}
		
	//capture keyStroke: enter value=13 [OWNER]
	function getKeyOwner(keyStroke) {
	   var keyCode = (document.layers) ? keyStroke.which : event.keyCode;
   	   if(keyCode==13){
   	     searchClient("<%=IUMConstants.CLIENT_TYPE_OWNER%>");
   	   }
	}
    
    
-->