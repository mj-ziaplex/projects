<%@ page language="java" buffer="12kb"%>
<!-- JAVA UTILS -->
<%@ page import="java.util.ResourceBundle, java.util.ArrayList" %>
<!-- IUM UTILS -->
<%@ page import="com.slocpi.ium.util.IUMConstants" %>
<%@ page import="com.slocpi.ium.ui.util.Page, com.slocpi.ium.ui.util.StateHandler" %>
<!-- IUM POJO -->
<%@ page import="com.slocpi.ium.ui.form.PolicyRequirementsForm" %>
<!-- IUM DATA -->
<%@ page import="com.slocpi.ium.data.UserData, com.slocpi.ium.data.UserProfileData" %>

<!-- IUM TAGLIBS -->
<%@ taglib uri="/ium.tld" prefix="ium"%>

<%
	/* Andre Ceasar Dacanay - css enhancement - start */
	String iumCss = "";
	String iumBgColor = "";
	String iumColor = "";
	String iumColorB = "";
	String companyCode = "";
	
	try {
		
		ResourceBundle rbWMSSP = ResourceBundle.getBundle("dbconfig");
		iumCss = rbWMSSP.getString("IUM_CSS_VERSION");
		
		companyCode = iumCss;
		iumCss = "_"+iumCss;
	} catch (Exception e){}
	
	if(iumCss.equals("_CP")){
		iumBgColor = "bgcolor=\"#2a4c7c\"";
		iumColorB = "#2A4C7C";
		companyCode="";
	}else if(iumCss.equals("_GF")){
		iumBgColor = "bgcolor=\"#303A77\"";
		iumColor = "style=\"color:#303A77;\"";
		iumColorB = "#303A77";
		companyCode = companyCode+" ";
	}else{
		iumBgColor = "bgcolor=\"#2a4c7c\";";
		iumColorB = "#2A4C7C";
	}
	/* Andre Ceasar Dacanay - css enhancement - end */
%>


<%
	
	PolicyRequirementsForm requirement = (PolicyRequirementsForm) request.getAttribute("requirement");
	
	if(requirement != null) { 
		requirement = (PolicyRequirementsForm) request.getAttribute("requirement");
	} else {
		requirement = new PolicyRequirementsForm(); 
	}
	
	
	String contextPath = request.getContextPath();
	StateHandler sessionHandler = new StateHandler();
	UserData userData = sessionHandler.getUserData(request); 
	UserProfileData userPref = userData.getProfile();
	
	String sessionId = request.getParameter("sessionId")!=null ? request.getParameter("sessionId") :"";
	String sourceSystem = request.getParameter("sourceSystem")!=null ? request.getParameter("sourceSystem") :"";
	
	String mode = request.getParameter("mode")!=null ? request.getParameter("mode") :"";
	String arFormRefNo = request.getParameter("arFormRefNo")!=null ? request.getParameter("arFormRefNo") :"";
	String policySuffix = request.getParameter("policySuffix")!=null ? request.getParameter("policySuffix") :"";
	
	String lob = request.getParameter("lob")!=null ? request.getParameter("lob") :"";
	String arFormOwnerClientNo = request.getParameter("arFormOwnerClientNo")!=null ? request.getParameter("arFormOwnerClientNo") :"";
	String arFormInsuredClientNo = request.getParameter("arFormInsuredClientNo")!=null ? request.getParameter("arFormInsuredClientNo") :"";
	String agentCode = request.getParameter("agentCode")!=null ? request.getParameter("agentCode") :"";
	String branchCode = request.getParameter("branchCode")!=null ? request.getParameter("branchCode") :"";
	String assignedTo = request.getParameter("assignedTo")!=null ? request.getParameter("assignedTo") :"";
	String arFormRequestStatus = request.getParameter("arFormRequestStatus")!=null ? request.getParameter("arFormRequestStatus") :"";
	
	String EXECUTE_KO = request.getParameter("EXECUTE_KO")!=null ? request.getParameter("EXECUTE_KO") :"";
	String sendButtonControlSend = request.getParameter("sendButtonControl")!=null ? request.getParameter("sendButtonControl") :"";
	String dateToday = request.getAttribute("systemDate") != null ? request.getAttribute("systemDate").toString() : "";
	
	// MR-ID-13-00002, MR-GU-13-00002 Structured Notes End Date - MMANG 31Jan2013 - Start
	String strNBStrucNotesSettleDate = request.getSession().getAttribute("strNBStrucNotesSettleDate")!=null ? 
		request.getSession().getAttribute("strNBStrucNotesSettleDate").toString(): "";
	// MR-ID-13-00002, MR-GU-13-00002 Structured Notes End Date - MMANG 31Jan2013 - End
		
	String detailForm = "detailForm_" + sessionId;
	
	String hasOrderReqt = request.getAttribute("hasOrderReqt")!=null ? (String)request.getAttribute("hasOrderReqt") :"";
	
	int pageNumber = 1;
	
	
	String sendButtonControl = "DISABLED";
	
	if(requirement!=null && requirement.getOrderRequirementsTag()!=null && requirement.getOrderRequirementsTag().equals("YES")){
		sendButtonControl = "ENABLED";
	}else{
		sendButtonControl = "DISABLED";
	}
	
	if(sendButtonControlSend.equals("SEND")){
		sendButtonControl = "DISABLED";
	}
	
	
	if(arFormRefNo.equals("")){
		arFormRefNo = (String)request.getAttribute("arFormRefNo");
	}
	
	if(arFormOwnerClientNo.equals("")){
		arFormOwnerClientNo = (String)request.getAttribute("arFormOwnerClientNo");
	}
	
	if(arFormInsuredClientNo.equals("")){
		arFormInsuredClientNo = (String)request.getAttribute("arFormInsuredClientNo");
	}
	
	if(arFormRequestStatus.equals("")){
		arFormRequestStatus = (String)request.getAttribute("arFormRequestStatus");
	}
	
	if(lob.equals("")){
		lob = (String)request.getAttribute("lob");
	}
	
	if(sessionId.equals("")){
		sessionId = (String)request.getAttribute("sessionId");
	}
	
	if(sourceSystem.equals("")){
		sourceSystem = (String)request.getAttribute("sourceSystem");
	}
	
	if(agentCode.equals("")){
		agentCode = (String)request.getAttribute("agentCode");
	}
	
	if(branchCode.equals("")){
		branchCode = (String)request.getAttribute("branchCode");
	}
	
	if(assignedTo.equals("")){
		assignedTo = (String)request.getAttribute("assignedTo");
	}
	
	if(EXECUTE_KO.equals("")){
		EXECUTE_KO = (String)request.getAttribute("EXECUTE_KO");
	}
	
	
 %>
 
 <%
	String reqId = requirement.getReqId()!=null && !requirement.getReqId().equals("") ? requirement.getReqId() : "";
	String reqCode = requirement.getReqCode()!=null && !requirement.getReqCode().equals("") ? requirement.getReqCode() : "-";
	String reqCodeValue = requirement.getReqCodeValue()!=null && !requirement.getReqCodeValue().equals("") ? requirement.getReqCodeValue() : "-";
	String reqDesc = requirement.getReqDesc()!=null && !requirement.getReqDesc().equals("") ? requirement.getReqDesc() : "-";
	String clientNo = requirement.getClientNo()!=null && !requirement.getClientNo().equals("") ? requirement.getClientNo() : "-";
	String clientType = requirement.getClientType()!=null && !requirement.getClientType().equals("") ? requirement.getClientType() : "-";
	
	String clientTypeDesc = "";
	if(clientType.equals("I")){
		clientTypeDesc = "Insured";
	}else if(clientType.equals("O")){
		clientTypeDesc = "Owner";
	}else{
		clientTypeDesc = "-";
	}
	
	String followUpDate = requirement.getFollowUpDate()!=null && !requirement.getFollowUpDate().equals("") ? requirement.getFollowUpDate() : "";
	String comment = requirement.getComment()!=null && !requirement.getComment().equals("") ? requirement.getComment() : "";
	String seq = requirement.getSeq()!=null && !requirement.getSeq().equals("") ? requirement.getSeq() : "";
	String reqStatusCode = requirement.getReqStatusCode()!=null && !requirement.getReqStatusCode().equals("") ? requirement.getReqStatusCode() : "";
	String reqStatusValue = requirement.getReqStatusValue()!=null && !requirement.getReqStatusValue().equals("") ? requirement.getReqStatusValue() : "";
	String completeReq = requirement.getCompleteReq()!=null && !requirement.getCompleteReq().equals("") ? requirement.getCompleteReq() : "";
	
	
	String completeReqDesc = "";
	String completeReqYes = "";
	String completeReqNo = "";
	if(completeReq.equals("true") || completeReq.equals("Y") || completeReq.equals("1")){
		completeReqDesc = "Yes";
		completeReqYes= "checked";
	}else if(completeReq.equals("false") || completeReq.equals("N") || completeReq.equals("0")){
		completeReqDesc = "No";
		completeReqNo = "checked";
	}else{
		completeReqDesc = "No";
		completeReqNo = "checked";
	}
	
	String statusDate = requirement.getStatusDate()!=null && !requirement.getStatusDate().equals("") ? requirement.getStatusDate() : "";
	String ccasSuggest = requirement.getCcasSuggest()!=null && !requirement.getCcasSuggest().equals("") ? requirement.getCcasSuggest() : "";
	
	
	String ccasSuggestDesc = "";
	String ccasSuggestYes = "";
	String ccasSuggestNo = "";
	if(ccasSuggest.equals("true") || ccasSuggest.equals("Y") || ccasSuggest.equals("1")){
		ccasSuggestDesc = "Yes";
		ccasSuggestYes = "checked";
	}else if(ccasSuggest.equals("false") || ccasSuggest.equals("N") || ccasSuggest.equals("0")){
		ccasSuggestDesc = "No";
		ccasSuggestNo = "checked";
	}else{
		ccasSuggestDesc = "No";
		ccasSuggestNo = "checked";
	}
	
	String designation = requirement.getDesignation()!=null && !requirement.getDesignation().equals("") ? requirement.getDesignation() : "";
	String updatedBy = requirement.getUpdatedBy()!=null && !requirement.getUpdatedBy().equals("") ? requirement.getUpdatedBy() : "";
	String updatedDate = requirement.getUpdatedDate()!=null && !requirement.getUpdatedDate().equals("") ? requirement.getUpdatedDate() : "";
	String validityDate = requirement.getValidityDate()!=null && !requirement.getValidityDate().equals("") ? requirement.getValidityDate() : "";
	String testDate = requirement.getTestDate()!=null && !requirement.getTestDate().equals("") ? requirement.getTestDate() : "";
	String testResult = requirement.getTestResult()!=null && !requirement.getTestResult().equals("") ? requirement.getTestResult() : "";
	String orderDate = requirement.getOrderDate()!=null && !requirement.getOrderDate().equals("") ? requirement.getOrderDate() : "";
	String testDesc = requirement.getTestDesc()!=null && !requirement.getTestDesc().equals("") ? requirement.getTestDesc() : "";
	
	String resolve = requirement.getResolve()!=null && !requirement.getResolve().equals("") ? requirement.getResolve() : "";
	String resolveYes = "";
	String resolveNo = "";
	if (resolve.equals("true")) {
		resolveYes = "checked";
	}else if (resolve.equals("false")){
		resolveNo = "checked";
	}else{
		resolveNo = "checked";
	}
	
	String followUpNo = requirement.getFollowUpNo()!=null && !requirement.getFollowUpNo().equals("") ? requirement.getFollowUpNo() : "";
	String reqLevel = requirement.getReqLevel()!=null && !requirement.getReqLevel().equals("") ? requirement.getReqLevel() : "";
	String reqLevelDesc = "";
	if(reqLevel.equals("C")){
		reqLevelDesc = "Client";
	}else if(reqLevel.equals("P")){
		reqLevelDesc = "Policy";
	}else{
		reqLevelDesc = "-";
	}
	
	String dateSent = requirement.getDateSent()!=null && !requirement.getDateSent().equals("") ? requirement.getDateSent() : "";
	String paidInd = requirement.getPaidInd()!=null && !requirement.getPaidInd().equals("") ? requirement.getPaidInd() : "";
	String paidIndYes = "";
	String paidIndNo = "";
	if (paidInd.equals("true")) {
		paidIndYes = "checked";
	}else if (paidInd.equals("false")){
		paidIndNo = "checked";
	}else{
		paidIndNo = "checked";
	}
	
	String newTest = requirement.getNewTest()!=null && !requirement.getNewTest().equals("") ? requirement.getNewTest() : "";
	
	String newTestYes = "";
	String newTestNo = "";
	if (newTest.equals("Y")) {
		newTestYes = "checked";
	}else if (newTest.equals("N")){
		newTestNo = "checked";
	}else{
		newTestNo = "checked";
	}
	
	String receivedDate = requirement.getReceivedDate()!=null && !requirement.getReceivedDate().equals("") ? requirement.getReceivedDate() : "";
	String autoOrder = requirement.getAutoOrder()!=null && !requirement.getAutoOrder().equals("") ? requirement.getAutoOrder() : "";
	String autoOrderYes = "";
	String autoOrderNo = "";
	if (autoOrder.equals("Y")) {
		autoOrderYes = "checked";
	}else if (autoOrder.equals("N")){
		autoOrderNo = "checked";
	}else{
		autoOrderNo = "checked";
	}
	
	String fldComm = requirement.getFldComm()!=null && !requirement.getFldComm().equals("") ? requirement.getFldComm() : "";
	String fldCommYes = "";
	String fldCommNo = "";
	if (fldComm.equals("Y")) {
		fldCommYes = "checked";
	}else if (fldComm.equals("N")){
		fldCommNo = "checked";
	}else{
		fldCommNo = "checked";
	}
%>
 
<html>

	<head>
	<title><%=companyCode%>IUM&nbsp;&nbsp;<%=arFormRefNo%>&nbsp;<%=policySuffix%>&nbsp;&nbsp;Requirements</title>
	<link href="<%=contextPath%>/css/pw_main.css" rel="stylesheet" type="text/css">
	<link href="<%=contextPath%>/css/main<%=iumCss%>.css" rel="stylesheet" type="text/css">
	<link href="<%=contextPath%>/css/styleCommon.css" rel="stylesheet" type="text/css">
	
	<script type="text/javascript" src="<%=contextPath%>/js/validation.js"></script>
	<script type="text/javascript" src="<%=contextPath%>/js/dhtml_init.js"></script>
	<script type="text/javascript" src="<%=contextPath%>/js/navigation.js"></script>
	<script type="text/javascript" src="<%=contextPath%>/jsp/sl_left_nav.jsp"></script>
	
	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
	<script type="text/javascript" src="<%=contextPath%>/js/commonFunctions.js"></script>
	<!-- policyRequirements Script -->
	<script type="text/javascript">
	</script>
	</head>
	
	<body leftMargin="0" topmargin="0" marginwidth="0" marginheight="0" onUnload="closePopUpWindow();">
		<form name="frm"  method="post">    
			<table border="0" cellpadding="0" cellspacing="0" >
				<tr>								
					<td width="100%"> 
						<div align="left"> 	 	       	
							<table width="100%" cellpadding="10" cellspacing="2" border="0">
								<tr valign="top">
									<td class="title2">Requirements
										<!--Start of form -->
								      	<table border="1" bordercolor="<%=iumColorB%>" cellpadding="5" cellspacing="0" bgcolor="" width="700" class="listtable1">
								       		<tr>
								       			<td colspan="2" height="100%">
													<table border="0" width="690" cellpadding="2" cellspacing="2" class="listtable1">
									    				<tr>
									          				<td class="label2" width="20%" align="left"><span class="required">*</span><b>Code</b></td>
									          				<td class="label2" width="30%" align="left"><%=reqCode%>
									          					<input type="hidden" name="reqCode" value="<%=reqCode%>"/>
									          					<input type="hidden" name="reqCodeValue" value="<%=reqCode%>"/>
									          				</td>
							  								<td class="label2" width="20%" align="left">&nbsp;<b>Description</b></td>
									          				<td class="label2" width="30%" align="left"><%=reqDesc%>
									          					<input type="hidden" name="reqDesc" value="<%=reqDesc%>"/>
									          				</td>
									        			</tr>
									        			<tr>        
									        				<td class="label2" align="left"><span class="required">*</span><b>Client Type</b></td>
									          				<td class="label2" align="left"><%=clientTypeDesc%>
									          					<input type="hidden" name="clientNo" value="<%=clientNo%>"/>
									          					<input type="hidden" name="clientType" value="<%=clientType%>"/>
									          				</td>
									          				<td class="label2" align="left">&nbsp;<b>Follow Up Date</b></td>
									          				<td class="label2" align="left"><input type="text" class="label2" style="width:45%" name="followUpDate" value="<%=followUpDate%>" onBlur="validateFollowUpDate(this);" maxlength="9">
																<input type="hidden" name="dateToday" value="<%=dateToday%>">
																<div id="divFollowUpDate" style="position:absolute; width:40%">
																	<a href="#" onClick="popUpWindow('<%=contextPath%>/jsp/calendar.jsp?textbox=frm.followUpDate',290,155,'yes','yes','yes','no','no','no','no','1');">
																		<img src="<%=contextPath%>/images/calendar.gif" height="17" width="19" border="0">
																	</a>
																</div>
															</td>
									        			</tr>
												        <tr>        
												          	<td class="label2" align="left">&nbsp;<b>Comments</b></td>
												          	<td class="label2" align="left" colspan="3"><textarea class="label2" id="commentArr" name="comment" cols="88" rows="2" maxlength="4000"><%=comment%></textarea></td>
												        </tr>
												        <tr>
														  	<td align="left" colspan="4" class="label2">
																<div id="Expand">
																	<a <%=iumColor%> href="javascript:expandReq();" style="cursor: hand;">more details</a>
																</div> 
															</td>
														</tr>
														<tr>
															<td colspan="8">
																<!-- ExpandBox -->
																<div id="reqDiv" style="display: none;">
																	<table bordercolor="<%=iumColorB%>" cellpadding="2" cellspacing="2" bgcolor="" width="700" class="listtable1">
																		<tr>
																			<td class="label2" width="20%" align="left"><b>Seq</b></td>
																			<td class="label2" width="30%" align="left"><%=seq%><input type="hidden" name="seq" value="<%=seq%>"/></td>
																			<td class="label2" width="20%" align="left"><b>Status</b></td>
																			<td class="label2" width="30%" align="left"><%=reqStatusCode%><input type="hidden" name="reqStatusValue" value="<%=reqStatusValue%>"/></td>
																		</tr>
																		<tr>
																			<td class="label2" align="left"><b>Completed Requirement?</b></td>
																			<td class="label2" align="left"> 
																				<%=completeReqDesc%>  
																			  	<input type="hidden" name="completeReq" value="<%=completeReq%>"/>
																		 	</td>
																			<td class="label2" align="left"><b>Status Date</b></td>
																			<td class="label2" align="left"><%=statusDate%>
																				<input type="hidden" name="statusDate" value="<%=statusDate%>"/>
																			</td>
																		</tr>
																		<tr>
																			<td class="label2" align="left"><b>CCAS Suggest?</b></td>
																			<td class="label2" align="left">
																				<%=ccasSuggestDesc%>
																				<input type="hidden" name="ccasSuggest" value="<%=ccasSuggest%>"/>
																			</td>
																			<td class="label2" align="left"><b>Designation</b></td>
																			<td class="label2" align="left"><input type="text" name="designation" class="label2" value="<%=designation%>" maxlength="10" style="width:45%"></td>
																		</tr>
																		<tr>
																			<td class="label2"><b>Updated By</b></td>
																			<td class="label2"><%=updatedBy%></td>
																			<td class="label2" style="width:10%"><b>Updated Date</b></td>
																			<td class="label2" align="left"><%=updatedDate%></td>
																		</tr>
																		<tr>
																			<td class="label2"><b>Validity Date</b></td>
																			<td class="label2"><input type="text" name="validityDate" class="label2" style="width:45%" value="<%=validityDate%>" onKeyUp="getKeyDate(event,this);" maxlength="9">&nbsp;
																				<div id="doc_cal2" style="position:absolute;">
																					<a href="#" onClick="popUpWindow('<%=contextPath%>/jsp/calendar.jsp?textbox=frm.validityDate',290,155,'yes','yes','yes','no','no','no','no','1');">
																						<img src="<%=contextPath%>/images/calendar.gif" height="17" width="19" border="0"> 
																					</a>
																				</div>
																			</td>
																			<td class="label2"><b>Test Date</b></td>
																			<td class="label2"><input type="text" name="testDate" class="label2" style="width:45%" value="<%=testDate%>" onKeyUp="getKeyDate(event,this);" maxlength="9">&nbsp;
																				<div id="doc_cal1" style="position:absolute;">
																					<a href="#" onClick="popUpWindow('<%=contextPath%>/jsp/calendar.jsp?textbox=frm.testDate',290,155,'yes','yes','yes','no','no','no','no','1');">
																						<img src="<%=contextPath%>/images/calendar.gif" height="17" width="19" border="0"> 
																					</a>
																				</div>
																			</td>
																		</tr>
																		<tr>
																			<td class="label2"><b>Test Result</b></td>
																			<td class="label2"><%=testResult%>
																				<input type="hidden" name="testResult" value="<%=testResult%>"/>	
																			</td>
																			<td class="label2"><b>Order Date</b></td>
																			<td class="label2"><%=orderDate%>
																				<input type="hidden" name="orderDate" value="<%=orderDate%>"/>	
																			</td>
																		</tr>
																		<tr>
																			<td class="label2"><b>Description</b></td>
																			<td class="label2" colspan="5"><%=testDesc%>
																				<input type="hidden" name="testDesc" value="<%=testDesc%>"/>	
																			</td>
																		</tr>
																		<tr>
																			<td class="label2"><b>Resolve</b></td>
																			<td class="label2">
																				<input type="radio" name="rdResolve" class="label2" value="Y" <%=resolveYes%>>Yes 
																				<input type="radio" name="rdResolve" class="label2" value="N" <%=resolveNo%>>No
																				<input type="hidden" name="resolve" value="<%=resolve%>"/>
																			</td>
																			<td class="label2">
																				<span class="required">*</span><b>Follow Up No</b></td>
																			<td class="label2"><input type="text" name="followUpNo" class="label2" value="<%=followUpNo%>" maxLength="1" style="width:15%"></td>
																		</tr>
																		<tr>
																			<td class="label2"><b>Level</b></td>
																			<td class="label2"><%=reqLevelDesc%>
																				<input type="text" class="noTextbox" name="reqLevelDisplay" value="" size="6" maxLength="6" readonly> 
																				<input type="hidden" name="reqLevel" value="<%=reqLevel%>">
																			</td>
																			<td class="label2"><b>Date Sent</b></td>
																			<td class="label2"><%=dateSent%>
																				<input type="hidden" name="dateSent" value="<%=dateSent%>"/>
																			</td>
																		</tr>
																		<tr>
																			<td class="label2"><b>Paid Indicator</b></td>
																			<td class="label2">
																				<input type="radio" name="rdPaidInd" class="label2" value="Y" <%=paidIndYes%>>Yes 
																				<input type="radio" name="rdPaidInd" class="label2" value="N" <%=paidIndNo%>>No
																				<input type="hidden" name="paidInd" value="<%=paidInd%>"/>
																			</td>
																			<td class="label2"><b>New Tests Only</b></td>
																			<td class="label2">
																				<input type="radio" name="rdNewTest" class="label2" value="Y" <%=newTestYes%>>Yes 
																				<input type="radio" name="rdNewTest" class="label2" value="N" <%=newTestNo%>>No
																				<input type="hidden" name="newTest" value="<%=newTest%>"/>
																			</td>
																		</tr>
																		<tr>
																			<td class="label2"><b>Received Date</b></td>
																			<td class="label2"><%=receivedDate%>
																				<input type="hidden" name="receivedDate" value="<%=receivedDate%>"/>
																			</td>
																			<td class="label2"><b>Auto Order</b></td>
																			<td class="label2">
																				<input type="radio" name="rdAutoOrder" class="label2" value="Y" <%=autoOrderYes%>>Yes 
																				<input type="radio" name="rdAutoOrder" class="label2" value="N" <%=autoOrderNo%>>No
																				<input type="hidden" name="autoOrder" value="<%=autoOrder%>"/>
																			</td>
																		</tr>
																		<tr>
																			<td class="label2"><b>FLD Comm</b></td>
																			<td class="label2">
																				<input type="radio" name="rdFldComm" class="label2" value="Y" <%=fldCommYes%>>Yes 
																				<input type="radio" name="rdFldComm" class="label2" value="N" <%=fldCommNo%>>No
																				<input type="hidden" name="fldComm" value="<%=fldComm%>"/>
																				</td>
																		</tr>
																	</table>
																</div>
																<!-- ExpandBox -->
															</td>
														</tr>
												        <tr id="save_cancel" class="hide">
														  	<td align="left" colspan="2">
													        	<input type="button" class="button1" name="saveUpdateRequirement" value="Save" onclick="clickSaveUpdate()"> 
													        	<input type="button" value="Cancel" class="button1" onClick="emptyFields();disableFields();">
												          	</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
										<!-- END OF TABLE -->
									</td>
								</tr>
							</table>
						</div>
					</td>
				</tr>
			</table>
					
			<!-- ************************************ -->
			<!-- ******* START OF POLICY LIST ******* -->
			<!-- ************************************ -->
			<table>     
				<tr valign="top">
					<td></td> 
				</tr> 
				<tr id="create_cancel" class="show">
					<td align="left" colspan="7">&nbsp;&nbsp;
						<input type="button" class="button1" name="create" value=" Create " onClick="clickCreate();">
						<input type="button" class="button1" name="close" value=" Close " onClick="window.close()">
					</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>
						<!--- START OF BODY -->
						<%
							ResourceBundle rbPN = ResourceBundle.getBundle("clientnumbers");
							ResourceBundle rb = ResourceBundle.getBundle("wmsconfig");
							
							String suppressedPolicies = rbPN.getString("suppressedclientnumbers");
							String wmsContextPath = rb.getString("wms_context_path");
						%>
						<table border="1" bordercolor="<%=iumColorB%>" cellpadding="10" cellspacing="0" bgcolor="" width="700" class="listtable1">
							<tr>
								<td>
									<!-- *********************************** -->
									<!-- ******* CHANGE STATUS TABLE ******* -->
									<!-- *********************************** -->
									<table border="0" cellpadding="0" cellspacing="0" width="700">
										<tr>
											<td class="label2"><b>Change Status To</b></td>
											<td>
									           <%
									           //filter for request status
									           String lob_status = lob + ",null";
									           %>
									            <div id="statusTospan">
									            	<ium:list className="label2" listBoxName="statusTo" type="<%=IUMConstants.LIST_REQUIREMENT_STATUS%>" styleName="width:200" selectedItem="" onChange="onChangeOfStatus(); expandCommentTospan();" filter="<%=lob_status%>"/>
									            </div>
									            <input type="hidden" name="savedSection" value="req" />
									        </td>
									        <td>
									        	<input class="button1" type="button" name="saveBtn" value="Save" onClick="saveStatus('<%=contextPath%>','no');" <%= EXECUTE_KO%>>
									        	<input type="button" class="button1" name="followUp" value="Follow-up" onClick="setReqSavedSection('req'); saveStatus('<%=contextPath%>','yes');" <%= EXECUTE_KO%>>
											  	<% if (sendButtonControl.equals("ENABLED")){%>
										        	<input type="button" class="button1" name="sendBtn" value="Send Notification" onClick="setReqSavedSection('req'); sendNotification('<%=contextPath%>','req');" <%= EXECUTE_KO%>>
											  	<%}%>
											</td>
										</tr>
										<tr>
											<td colspan="3">
												<div id="commentTospan" style="display: none;">
													<table border="0" cellpadding="2" cellspacing="2">
														<tr>
															<td class="label2"><b>Comment</b></td>
															<td> &nbsp;</td>
															<td>
													           <textarea class="label2" name="commentTo" cols="88" rows="2"></textarea>
													        </td>
														</tr>
													</table>
												</div>
											</td>
										</tr>
										<tr>
											<td class="label2"><b>Change Follow-Up Date To</b></td>
											<td class="label2"><input type="text" name="followUpDateTo" class="label2" style="width:45%" value="" onBlur="validateFollowUpDate(this);" maxlength="9">&nbsp;
												<div id="doc_cal2" style="position:absolute;">
													<a href="#" onClick="popUpWindow('<%=contextPath%>/jsp/calendar.jsp?textbox=frm.followUpDateTo',290,155,'yes','yes','yes','no','no','no','no','1');">
														<img src="<%=contextPath%>/images/calendar.gif" height="17" width="19" border="0"> 
													</a>
												</div>
											</td>
									        <td></td>
										</tr>
										<tr style="visibility:hidden">
											<td class="label2"><b>Facilitator </b></td>
										   	<td>
										    	<%--Facilitator List START --%>
										     	<ium:list className="label2" listBoxName="facilitator" type="<%=IUMConstants.LIST_USERS%>" styleName="width:200" selectedItem="" filter="<%= IUMConstants.ROLES_FACILITATOR %>" disabled="true"/>
										      	<%--Facilitator List END --%>
										   	</td>
										 </tr>
									</table>
						
									<!-- ***************************** -->
									<!-- ******* START OF LIST ******* -->
									<!-- ***************************** -->
									<table border="0" cellpadding="2" cellspacing="2" class="listtable1" width="700">
								        <tr class="headerrow1">
											<td class="headerrow1">
												<input type="checkbox" name="selAll" onClick="javascript:selectAllEnabled(document.frm);">
												
												<!-- *** INCLUDE HIDDEN VALUES *** -->
												<input type="hidden" name="reqId" value='<%=reqId%>'>
										        <input type="hidden" name="reqStatusCode" value='<%=reqStatusCode%>'>
												<input type="hidden" name="pageNumber" value='<%=requirement.getPageNumber()%>'>
												<input type="hidden" name="lob" value='<%=lob%>'>
												<input type="hidden" name="currentStatus" value='<%=arFormRequestStatus%>'>
												<input type="hidden" name="refNo" value='<%=arFormRefNo%>'>
												<input type="hidden" name="arFormRefNo" value='<%=arFormRefNo%>'>
												<input type="hidden" name="arFormOwnerClientNo" value='<%=arFormOwnerClientNo%>'>
												<input type="hidden" name="arFormInsuredClientNo" value='<%=arFormInsuredClientNo%>'>
												<input type="hidden" name="sessionId" value='<%=sessionId%>'>
												<input type="hidden" name="branchCode" value="<%=branchCode%>">
												<input type="hidden" name="agentCode" value="<%=agentCode%>">
												<input type="hidden" name="assignedTo" value="<%=assignedTo%>">
												<input type="hidden" name="sourceSystem" value="<%=sourceSystem%>">
												<input type="hidden" name="savedSectionReq" value=""> 
												<input type="hidden" name="EXECUTE_KO" value="<%=EXECUTE_KO%>">
											</td>
									        <td class="headerrow1" >SEQ</td>
									        <td class="headerrow1" >CODE</td>
									        
									        <%if(contextPath.equalsIgnoreCase(wmsContextPath)){%>
									            <td class="headerrow6" width="15" align="center">&nbsp;&nbsp;IMAGE</td>
									        <%}%>
									        
									        <td class="headerrow1" width="50">ST</td>
									        <td class="headerrow1" width="75">STATUS DATE</td>
									        <td class="headerrow1">CLIENT NO</td>
									        <td class="headerrow1" width="200">COMMENT</td>
									        <td class="headerrow1">I/O</td>
									        <td class="headerrow1">P/C</td>
									        <td class="headerrow1" width="70">FOLLOW-UP DATE</td>
									        <td class="headerrow1" width="70">DATE SENT</td>
									        <td class="headerrow1" width="">TEST DATE</td>
								        </tr>
								        
										<%
										Page pageRecord = (Page)request.getAttribute("pageRecord");
										
										int i = 0; 
										String tr_class;
										String td_bgcolor;
										String pagingColSpan = "12";

										ArrayList pgTmp = pageRecord.getList();
										for(int xx = 0; xx < pgTmp.size(); xx++){
											
											PolicyRequirementsForm tmp = (PolicyRequirementsForm) pgTmp.get(xx);
											
											if (xx % 2 == 0){
												tr_class = "row1";
												td_bgcolor = "#CECECE";
											}else{
												tr_class = "row2";
												td_bgcolor = "#EDEFF0";
											}
										%>
											<tr class="<%=tr_class%>" >
												<td>
													<% if (arFormRefNo.equals(tmp.getRefNo())) { %>
											        	<input type="checkbox" name="index">
											        <% } else { %>
											        	<input type="checkbox" name="index" disabled>
											        <% } %>
											        
											        <!-- *** INCLUDE HIDDEN VALS *** -->
											        <input type="hidden" name="indexTemp" value="">
													<input type="hidden" name="reqIds" value='<%=tmp.getReqId()%>'>
											        <input type="hidden" name="statusCodes" value='<%=tmp.getReqStatusCode()%>'>
											        <input type="hidden" name="followUpNos" value='<%=tmp.getFollowUpNo()%>'>
											        <input type="hidden" name="seqs" value='<%=tmp.getSeq()%>'>
											        <input type="hidden" name="followUpDates" value='<%=tmp.getFollowUpDate()%>'>
											        <input type="hidden" name="reqLevels" value='<%=tmp.getReqLevel()%>'>
											        <input type="hidden" name="reqCodes" value='<%=tmp.getReqCode()%>'>
											        <input type="hidden" name="reqRefNos" value='<%=tmp.getRefNo()%>'>
											        <input type="hidden" name="dateSents" value='<%=tmp.getDateSent()%>'>
											        <input type="hidden" name="testDates" value='<%=tmp.getTestDate()%>'>
											    </td>
												<td valign="top" style="cursor:hand" onclick="updateRequirement('<%=tmp.getReqId()%>', '<%=tmp.getReqStatusDesc()%>')"><%=tmp.getSeq()%></td>
												<td valign="top" style="cursor:hand" onclick="updateRequirement('<%=tmp.getReqId()%>', '<%=tmp.getReqStatusDesc()%>')"><%=tmp.getReqCode()%></td>
												
												<%
												String fnUrl = tmp.getWmsImageLink();
												if(contextPath.equalsIgnoreCase(wmsContextPath)){
													
													pagingColSpan = "13";
													if(fnUrl!= null && !fnUrl.equals("") ){ %>
											              	<td><center><a href="<%=fnUrl%>" target="_blank">View Document</a></center></td>
											        <%}else{%>
											            	<td><center>&nbsp;</center></td>
													<%}%>
												<%}%>
												<td valign="top" style="cursor:hand" onclick="updateRequirement('<%=tmp.getReqId()%>', '<%=tmp.getReqStatusDesc()%>')"><%=tmp.getReqStatusDesc()%></td>
												<td valign="top" style="cursor:hand" onclick="updateRequirement('<%=tmp.getReqId()%>', '<%=tmp.getReqStatusDesc()%>')"><%=tmp.getStatusDate()%></td>
												<td valign="top" style="cursor:hand" onclick="updateRequirement('<%=tmp.getReqId()%>', '<%=tmp.getReqStatusDesc()%>')"><%=tmp.getClientNo()%></td>
												<td valign="top" style="cursor:hand" onclick="updateRequirement('<%=tmp.getReqId()%>', '<%=tmp.getReqStatusDesc()%>')"><%=tmp.getComment() == null ? "" : tmp.getComment()%></td>
												<td valign="top" style="cursor:hand" onclick="updateRequirement('<%=tmp.getReqId()%>', '<%=tmp.getReqStatusDesc()%>')"><%=tmp.getClientType() == null ? "" : tmp.getClientType()%></td>
												<td valign="top" style="cursor:hand" onclick="updateRequirement('<%=tmp.getReqId()%>', '<%=tmp.getReqStatusDesc()%>')"><%=tmp.getReqLevel()%></td>
												<td valign="top" style="cursor:hand" onclick="updateRequirement('<%=tmp.getReqId()%>', '<%=tmp.getReqStatusDesc()%>')"><%=tmp.getFollowUpDate()%></td>
												<td valign="top" style="cursor:hand" onclick="updateRequirement('<%=tmp.getReqId()%>', '<%=tmp.getReqStatusDesc()%>')"><%=tmp.getDateSent()%></td>
												<td valign="top" style="cursor:hand" onclick="updateRequirement('<%=tmp.getReqId()%>', '<%=tmp.getReqStatusDesc()%>')"><%=tmp.getTestDate()%></td>
											</tr>
											<%i++;%>
										<%}%>
										
										<%
										String reqPageNumber = requirement.getPageNumber()!=null && !requirement.getPageNumber().equals("") ? requirement.getPageNumber() : "1";
										pageNumber = Integer.parseInt(reqPageNumber);  
										
										
										if (pageRecord==null){ %>		
											<tr>
												<td class="headerrow4" colspan="<%=pagingColSpan%>" align="left" width="100%">No record found.</td>
											</tr>
										<%}else{%>
								        	<tr>
												<td class="headerrow4" colspan="<%=pagingColSpan%>" width="100%" height="10" class="label2" valign="bottom" >
									            	<!-- don't display link for previous page if the current page is the first page -->					
									                <% if (pageNumber > 1) { %>
									                	<a <%=iumColor%> href="#" onclick="javascript:rePaginate(1,'viewPolicyRequirements.do?arFormRefNo=<%=arFormRefNo%>&policySuffix=<%=policySuffix%>&arFormOwnerClientNo=<%=arFormOwnerClientNo%>&arFormInsuredClientNo=<%=arFormInsuredClientNo%>');">
									                		<img src="images/pagnavfirst_off.gif" name="pagnavfirst" border="0" width="18" height="13"/>
									                	</a>&nbsp;					
									                    <a <%=iumColor%> href="#" onclick="javascript:rePaginate(<%=pageNumber-1%>,'viewPolicyRequirements.do?arFormRefNo=<%=arFormRefNo%>&policySuffix=<%=policySuffix%>&arFormOwnerClientNo=<%=arFormOwnerClientNo%>&arFormInsuredClientNo=<%=arFormInsuredClientNo%>');">
									                    	<img src="images/pagnavleft_off.gif" name="pagnavleft" border="0" width="18" height="13"/>
									                    </a>
									                <% } else {%>
									                	<img src="images/pagnavfirst_on.gif" name="pagnavfirst" border="0" width="18" height="13"/>&nbsp;
									                    <img src="images/pagnavleft_on.gif" name="pagnavleft" border="0" width="18" height="13"/>
									                <% } %>
									                
									                <% 
									                int currLink = 1;
									                int prevLink = 1;
									                
									                ArrayList navlinks = pageRecord.getPageNumbers();
									                for(int xx = 0; xx < navlinks.size(); xx++){
									                	
									                	int tmp = ((Integer) navlinks.get(xx)).intValue();
									                	currLink = tmp;
									                %>
									                    <% if (tmp == pageNumber) { %>
									                    	<b><%=navlinks.get(xx)%></b>
									                    <% } else { %>		
									                    	<% if (currLink > (prevLink+1)) { %>
									                        	...
									                        <% } %>
									                      	<a <%=iumColor%> href="#" onclick="rePaginate('<%=tmp%>','viewPolicyRequirements.do?arFormRefNo=<%=arFormRefNo%>&policySuffix=<%=policySuffix%>&arFormOwnerClientNo=<%=arFormOwnerClientNo%>&arFormInsuredClientNo=<%=arFormInsuredClientNo%>');" class="links2"><%=tmp%></a>
									                    <% } %>
									                    
									                    <% prevLink = currLink; %>
									                <%}%>
																		
									                <!-- don't display link for next page if the current page is the last page -->
									                <%if(pageNumber < prevLink) {  %>
									                	<a <%=iumColor%> href="#" onClick="rePaginate('<%=pageNumber+1%>','viewPolicyRequirements.do?arFormRefNo=<%=arFormRefNo%>&policySuffix=<%=policySuffix%>&arFormOwnerClientNo=<%=arFormOwnerClientNo%>&arFormInsuredClientNo=<%=arFormInsuredClientNo%>');" class="links1">
									                		<img src="<%=contextPath%>/images/pagnavright_off.gif" name="pagnavright" border="0" width="18" height="13"/>
									                	</a>&nbsp;
									                    <a <%=iumColor%> href="#" onClick="rePaginate('<%=prevLink%>','viewPolicyRequirements.do?arFormRefNo=<%=arFormRefNo%>&policySuffix=<%=policySuffix%>&arFormOwnerClientNo=<%=arFormOwnerClientNo%>&arFormInsuredClientNo=<%=arFormInsuredClientNo%>');" class="links1">
									                    	<img src="<%=contextPath%>/images/pagnavlast_off.gif" width="18" height="13" border="0">
									                    </a>
									                <%} else {%>
									                    <img src="<%=contextPath%>/images/pagnavright_on.gif" width="18" height="13" border="0">&nbsp;
									                    <img src="<%=contextPath%>/images/pagnavlast_on.gif" width="18" height="13" border="0">
									                <%}%>
												</td>
								            </tr>
								      	<%}%>
	
								        <% if (i == 0) { %>
									        <tr>
									        	<td class="label2" colspan="11" nowrap>No requirements record found.</td>
									    	</tr>
										<% } %>
									</table>
								</td>
							</tr>
						</table>
					</td>
					<!--- END OF BODY -->
				</tr>
			</table>
		</form>
	
		<!--- START OF FUNCTIONS -->
		<script type="text/javascript">
			window.onLoad = onLoadPage();
			
			function onLoadPage(){
				<%if(arFormRefNo.equals("")){%>
					document.frm.refNo.value = opener.document.getElementById('arFormRefNo').value;
				<%}%>
			
				<%if(requirement == null
						|| requirement.getReqCode() == null
						|| requirement.getReqCode().equals("")) {%>
						
					disableFields();     	 		
				<%} else {%>
					enableFields();
					
					<%if(request.getParameter("mode") == null) {%>
						document.frm.reqCode.disabled=true;								 
					<%}%>				   
				<%}%>
			}
		
			function disableFields(){
			 	document.getElementById('save_cancel').style.display = 'none';
// 			 	document.getElementById('create_cancel').className = 'show';
			}
		
			function enableFields(){
				try{
					if(frm.reqStatusCode.value == "ORD"){
						document.getElementById('save_cancel').style.display = 'inline'; 
					}else{
						document.getElementById('save_cancel').style.display = 'none'; 	 
					}
				}catch(e){
					alert("Encountered Error enabling save button for requirements edit.");
				}
			}
		
			function rePaginate(pageNum, target){
				
			    var frm = document.frm;
				frm.pageNumber.value = pageNum;
				frm.action=target;
				frm.submit();
			}
		
			function trimTextArea(){
				
				var frm= document.frm;
				if(frm.reqDesc.value.length>70){
				   frm.reqDesc.value=frm.reqDesc.value.substring(0,70);
				}
			}
		
			function clickMaintain(){
				
				var form = document.frm;
				var value = form.reqId.value;
				
				if(value!=''){
				 	
					enableFields();
				 }
			}
		
			function clickSave(){
				
				var frm = document.frm; 
			   	if(validateFormEntry()==true){
				
					<%if(requirement==null||requirement.getReqCode()==null||requirement.getReqCode().equals("")||request.getParameter("mode")!=null){%>
				    	frm.action="maintainAdminRequirementCode.do?mode=add";
						frm.submit();
				 	<%}else{%>
				    	frm.action="maintainAdminRequirementCode.do";
						frm.reqCode.disabled=false;
						frm.submit();
				 	<%}%>
				}
			}
		
			function validateFormEntry(){
				
				var result=true;
				var frm = document.frm;
				
				if(isEmpty(frm.reqCode.value)){
					alert("<bean:message key='error.field.required' arg0='Code'/>");
					frm.reqCode.focus();
					return false;			
				}
				
				if(isEmpty(frm.reqDesc.value)){
					alert("<bean:message key='error.field.required' arg0='Description'/>");
					frm.reqDesc.focus();
					return false;			
				}
				
				if(frm.reqLevel[0].checked==false && frm.reqLevel[1].checked==false){
					alert("<bean:message key='error.field.singleselection' arg0='Level'/>");
					frm.reqLevel[0].focus();
					return false;			
				}
				
				if(isEmpty(frm.validity.value)){
					alert("<bean:message key='error.field.required' arg0='Validity'/>");
					frm.validity.focus();
					return false;			
				}else{
				 	
					if(!isNumeric(frm.validity.value)){
				    	alert("<bean:message key='error.field.numeric' arg0='Validity'/>");
						frm.validity.focus();
						return false;			
				 	}
				}
				 
				if(isEmpty(frm.followUpNo.value)){
					alert("<bean:message key='error.field.required' arg0='Follow Up'/>");
					frm.followUpNo.focus();
					return false;			
				}else{
				
					if(!isNumeric(frm.followUpNo.value)){
				    	alert("<bean:message key='error.field.numeric' arg0='Follow Up'/>");
						frm.followUpNo.focus();
						return false;
				 	}
				}
			
				if(frm.withForm[0].checked==false && frm.withForm[1].checked==false){
					alert("<bean:message key='error.field.singleselection' arg0='with Form'/>");
					frm.withForm[0].focus();
					return false;			
				}
				 
				if(frm.withForm[0].checked==true && noSelection(frm.formId.value)){
			    	alert("<bean:message key='error.field.required' arg0='Form'/>");
					frm.formId.focus();
					return false;
				}
				
				return result;
			}
		
			function checkHasReqPick(){
				
				var pick = document.frm.reqPick;
				var result='';
				
				if(pick.value==null){
					for(i=0;i<pick.length;i++){
						if(pick[i].checked==true){
							result=pick[i].value;
						}
					}
				}else{
					
					if(pick.checked==true){
						result=pick.value;											 			
					}
				}
				
				return result;
			}
		
			function clickCreate(){
				
				var frm = document.frm;
				var url = '<%=contextPath%>/viewPolicyRequirements.do?mode=create&sessionId=<%=sessionId%>&arFormRefNo=<%=arFormRefNo%>&arFormOwnerClientNo=<%=arFormOwnerClientNo%>&arFormInsuredClientNo=<%=arFormInsuredClientNo%>&lob=<%=lob%>&arFormRequestStatus=<%=arFormRequestStatus%>&sourceSystem=<%=sourceSystem%>&agentCode=<%=agentCode%>&branchCode=<%=branchCode%>&assignedTo=<%=assignedTo%>&sendButtonControl=<%=sendButtonControl%>&EXECUTE_KO=<%=EXECUTE_KO%>&policySuffix=<%=policySuffix%>';
				popUpWindow(url,845,750,'yes','yes','yes','no','no','no','no','2','810','60');
			}
		
			function emptyFields(){
				 var frm = document.frm;
			}
		
			function controlFormIdField(){
				
			   	var frm = document.frm;
				if(frm.withForm[0].checked==true){
					frm.formId.disabled=false;
				}else{
					frm.formId.disabled=true;
				}				 
			}
		
			function getCheckedValue(radioObj) {
				
				if(!radioObj){
					return "";
				}
					
				var radioLength = radioObj.length;
				if(radioLength == undefined){
					if(radioObj.checked) {
						return radioObj.value;
					}else{
						return "";
					}
				}
				
				for(var i = 0; i < radioLength; i++) {
					if(radioObj[i].checked) {
						return radioObj[i].value;
					}
				}
				
				return "";
			}
		
		
			function clickSaveUpdate(){
				
			   	var form = document.frm;
				form.resolve.value = getCheckedValue(form.rdResolve);
				form.paidInd.value = getCheckedValue(form.rdPaidInd);
				form.newTest.value = getCheckedValue(form.rdNewTest);
			   	form.autoOrder.value = getCheckedValue(form.rdAutoOrder);
			   	form.fldComm.value = getCheckedValue(form.rdFldComm);
			
				form.action = "<%=contextPath%>/viewPolicyRequirements.do?mode=saveUpdate&session_id=<%=sessionId%>&policySuffix=<%=policySuffix%>";
				form.submit();	 
			}
		
			function expandReq(){
			
				if(document.getElementById('reqDiv').style.display == ""){
					document.getElementById('reqDiv').style.display = "none";
				} else {
					document.getElementById('reqDiv').style.display = "";
				}
			}
		
			function expandCommentTospan(){
				
				try{
					if(document.getElementById('statusTo').value==13){
							document.getElementById('commentTospan').style.display = "";
					}else{
						document.getElementById('commentTospan').style.display = "none";
					}
				}catch(e){}
			}
		
			function updateRequirement(reqId, reqStatusDesc){
	
				var pageNumber = 1;
				var form = document.frm;
				form.reqId.value = reqId;
				form.reqStatusCode.value = reqStatusDesc;
				
				pageNumber = form.pageNumber.value;
				form.action  = "<%=contextPath%>/viewPolicyRequirements.do?arFormRefNo=<%=arFormRefNo%>&policySuffix=<%=policySuffix%>&arFormOwnerClientNo=<%=arFormOwnerClientNo%>&arFormInsuredClientNo=<%=arFormInsuredClientNo%>&mode=update&pageNumber=" + pageNumber;
				form.submit();
			}
		
			function sendNotification(contextPath,savedSectionReq) {
				var form = document.frm;
				form.action  = contextPath + "/sendNotificationOrderPolReqt.do?savedSectionReq="+ savedSectionReq+"&sendButtonControl=SEND&policySuffix=<%=policySuffix%>";
				form.submit();
			}
		
			function setReqSavedSection(val) {
				document.frm.savedSectionReq.value = val;
			}
										
			function orderRequirement(contextPath, facilitator, form) {
				var lob = form.lob.value;
				var branchId = form.branchCode.value;
				var agentId = form.agentCode.value;
				var reqStat = form.currentStatus.value;
				form.action = contextPath + "/savePolicyRequirementStatus.do?lob=" + lob + "&facilitator=" + facilitator + "&branchCode=" + branchId + "&agentCode=" + agentId + "&arStatus=" + reqStat;
				form.submit();
			}
		
			// MR-ID-13-00002, MR-GU-13-00002 Structured Notes End Date - MMANG 31Jan2013 - Start
			function customDateParse(str) {
				var months = ['JAN','FEB','MAR','APR','MAY','JUN','JUL','AUG','SEP','OCT','NOV','DEC'],
				n = months.length, re = /(\d{2})([a-z]{3})(\d{4})/i, matches;
			
				while(n--) { months[months[n]]=n; } // map month names to their index :)
			
				matches = str.match(re); // extract date parts from string
			
				return new Date(matches[3], months[matches[2]], matches[1]);
			}
			// MR-ID-13-00002, MR-GU-13-00002 Structured Notes End Date - MMANG 31Jan2013 - End
		
			function saveStatus(contextPath, followup) {
				
				var form = document.frm;
				var lob = form.lob.value;
				var branchId = form.branchCode.value;
				var assignedTo = form.assignedTo.value;
				var reqStat = form.currentStatus.value;
				var followUpDateTo = form.followUpDateTo.value;
				var strNBStrucNotesSettleDate = "<%=strNBStrucNotesSettleDate%>"; 
			
				if (followup == "no") {
					if (form.statusTo.value == "" && followUpDateTo == "") {
						alert("Please select a value for Requirement Status or Follow Up Date.");
						form.statusTo.focus();
						return;
					}
				}
				
				if (form.statusTo.value == "" && followUpDateTo == "") {
					alert("Please select a value for Requirement Status or Follow Up Date.");
					form.statusTo.focus();
					return;
				}
				
				// MR-ID-13-00002, MR-GU-13-00002 Structured Notes End Date - MMANG 31Jan2013 - Start
				if (followUpDateTo != "" && strNBStrucNotesSettleDate != "") {
					
					var dateFormDate = new Date();
					var dateSNDate = new Date();
					try {
						
						dateFormDate = customDateParse(followUpDateTo);
						dateSNDate = customDateParse(strNBStrucNotesSettleDate);
						
						if (dateSNDate < dateFormDate) {
							alert("This plan is Structured Notes. Please select a date on or before the settlement date: <%=strNBStrucNotesSettleDate%>");
							form.statusTo.focus();
							return;
						}
					} catch (ex) {}
				}
				// MR-ID-13-00002, MR-GU-13-00002 Structured Notes End Date - MMANG 31Jan2013 - End	
			
				var cnt    = 0;
				var ord    = 0;
				var rec    = 0;
				var waive  = 0;
				var acc    = 0;
				var rej    = 0;
				var canc   = 0;
				var others = 0;
				
				if (form.index[0]) {
					
					for (var i=0;i<form.index.length;i++) {
						
						if (form.index[i].checked) {
							
							form.indexTemp[i].value = "checked";
							if (form.statusTo.value == "<%= IUMConstants.STATUS_ORDERED %>") {
								if (form.statusCodes[i].value == "<%= IUMConstants.STATUS_ORDERED %>") {
								    ord++;
								} else {
								    others++;
								}
						    }
							
							if (form.statusTo.value == "<%= IUMConstants.STATUS_RECEIVED_IN_SITE %>") {
								if (form.statusCodes[i].value == "<%= IUMConstants.STATUS_RECEIVED_IN_SITE %>") {
								    rec++;
								} else {
								    others++;
								}
						    }
							
							if (form.statusTo.value == "<%= IUMConstants.STATUS_WAIVED %>") {
								if (form.statusCodes[i].value == "<%= IUMConstants.STATUS_WAIVED %>") {
								    waive++;
								} else {
								    others++;
								}
						    }
							
							if (form.statusTo.value == "<%= IUMConstants.STATUS_REVIEWED_AND_REJECTED %>") {
								if (form.statusCodes[i].value == "<%= IUMConstants.STATUS_REVIEWED_AND_REJECTED %>") {
								    rej++;
								} else {
								    others++;
								}
						    }
							
							if (form.statusTo.value == "<%= IUMConstants.STATUS_REVIEWED_AND_ACCEPTED %>") {
								if (form.statusCodes[i].value == "<%= IUMConstants.STATUS_REVIEWED_AND_ACCEPTED %>") {
								    acc++;
								} else {
								    others++;
								}
						    }
							
							if (form.statusTo.value == "<%= IUMConstants.STATUS_REQ_CANCELLED %>") {
								if (form.statusCodes[i].value == "<%= IUMConstants.STATUS_REQ_CANCELLED %>") {
								    canc++;
								} else {
								    others++;
								}
						    }
			
							cnt++;
						}
					}
				} else {
					
					if (form.index.checked) {
						
						form.indexTemp.value = "checked";
						if (form.statusTo.value == "<%= IUMConstants.STATUS_ORDERED %>") {
							if (form.statusCodes.value == "<%= IUMConstants.STATUS_ORDERED %>") {
							    ord++;
							}
						}
						
						if (form.statusTo.value == "<%= IUMConstants.STATUS_RECEIVED_IN_SITE %>") {
							if (form.statusCodes.value == "<%= IUMConstants.STATUS_RECEIVED_IN_SITE %>") {
							    rec++;
							}
						}
						
						if (form.statusTo.value == "<%= IUMConstants.STATUS_WAIVED %>") {
							if (form.statusCodes.value == "<%= IUMConstants.STATUS_WAIVED %>") {
							    waive++;
							}
						}
						
						if (form.statusTo.value == "<%= IUMConstants.STATUS_REVIEWED_AND_REJECTED %>") {
							if (form.statusCodes.value == "<%= IUMConstants.STATUS_REVIEWED_AND_REJECTED %>") {
							    rej++;
							}
						}
						
						if (form.statusTo.value == "<%= IUMConstants.STATUS_REVIEWED_AND_ACCEPTED %>") {
							if (form.statusCodes.value == "<%= IUMConstants.STATUS_REVIEWED_AND_ACCEPTED %>") {
							    acc++;
							}
						}
						
						if (form.statusTo.value == "<%= IUMConstants.STATUS_REQ_CANCELLED %>") {
							if (form.statusCodes.value == "<%= IUMConstants.STATUS_REQ_CANCELLED %>") {
							    canc++;
							}
						}
						cnt++;
					}
				}
			
				if (cnt == 0) {
					alert("<bean:message key='error.field.noselection' arg0=' arg1='to update'/>");
					return;
				}
				
				if (ord == 1 && others == 0) {
					alert("<bean:message key='error.sameStatusRequirement' arg0='ordered'/>");
					return;
				}
				
				if (rec == 1 && others == 0) {
					alert("<bean:message key='error.sameStatusRequirement' arg0='received'/>");
					return;
				}
				
				if (waive == 1 && others == 0) {
					alert("<bean:message key='error.sameStatusRequirement' arg0='waived'/>.");
					return;
				}
				
				if (rej == 1 && others == 0) {
					alert("<bean:message key='error.sameStatusRequirement' arg0='rejected'/>");
					return;
				}
				
				if (acc == 1 && others == 0) {
					alert("<bean:message key='error.sameStatusRequirement' arg0='accepted'/>");
					return;
				}
				
				if (canc == 1 && others == 0) {
					alert("<bean:message key='error.sameStatusRequirement' arg0='cancelled'/>");
					return;
				}
			
				var status = form.statusTo.value;
				if (status == "<%= IUMConstants.STATUS_ORDERED %>" && lob == "<%= IUMConstants.LOB_INDIVIDUAL_LIFE %>") {
					orderRequirement(contextPath, form.facilitator.value, document.frm);
				}
				
				if (status == "<%= IUMConstants.STATUS_ORDERED %>"
						&& (lob == "<%= IUMConstants.LOB_PRE_NEED %>"
						|| lob == "<%= IUMConstants.LOB_GROUP_LIFE %>")) {
					
				       if (reqStat == "<%= IUMConstants.STATUS_NB_REVIEW_ACTION %>"
				    		   || reqStat == "<%= IUMConstants.STATUS_AWAITING_REQUIREMENTS %>") {
				    	   
				           orderRequirement(contextPath, "", document.frm);
				       } else {
			 	           alert("<bean:message key='error.requirement.invalid' arg0='ordering'/>");
			 	           return;
				       }
				       
				       if (reqStat == "<%= IUMConstants.STATUS_UNDERGOING_ASSESSMENT %>"
				    		   || reqStat == "<%= IUMConstants.STATUS_AWAITING_REQUIREMENTS %>") {
				    	   
			               if (countStatusOrdered(form) == "false") {
					           if (form.facilitator.value == "") {
					              if (form.facilitator.disabled) {
					                  form.facilitator.disabled = false;
					              }
							      alert("<bean:message key='error.field.requiredselection' arg0='Facilitator'/>");
							      form.facilitator.focus();
							      return;
				        	   }
				           }
				           orderRequirement(contextPath, form.facilitator.value, document.frm);
			           } else {
			 	           alert("<bean:message key='error.requirement.invalid' arg0='ordering'/>");
			 	           return;
			           }
				}
			
			    if (followup == "no" && status != "<%= IUMConstants.STATUS_ORDERED %>") {
				   form.action = contextPath + "/savePolicyRequirementStatus.do?lob=" + lob + "&branchCode=" + branchId + "&arStatus=" + reqStat + "&assignedTo=" + assignedTo;
				   form.submit();
			    }
			    
			    if (followup == "yes") {
				   form.action = contextPath + "/savePolicyRequirementStatus.do?lob=" + lob + "&branchCode=" + branchId + "&followUp=yes&arStatus=" + reqStat;
			  	   form.submit();
				}
			}
		
			function countStatusOrdered(form) {
				
			   var form = document.frm;
			   var cnt  = 0;
			    
			   if (form.index[0]) {
			        for (var i=0;i<form.index.length;i++) {
			           if (form.index[i].disabled == false
			        		   && form.statusCodes[i].value == "<%= IUMConstants.STATUS_ORDERED %>") {
				            
			        	   cnt++;
				       }
				    }
			   } else {
			      if (form.index.disabled == false
			    		  && form.statusCodes.value == "<%= IUMConstants.STATUS_ORDERED %>") {
			          cnt++;
			      }
			    }
			   
			    if (cnt > 0) {
			        return "true";
			    } else {
			        
			        return "false";
			    }
			}
		
			function onChangeOfStatus() {
				
			    var form = document.frm; 
				var lob = form.lob.value;
				var reqStat = form.currentStatus.value;
				var status  = form.statusTo.value;
				
				if (status != "<%= IUMConstants.STATUS_ORDERED %>") {
			       form.facilitator.disabled = true;
				}
				
				if (status == "<%= IUMConstants.STATUS_ORDERED %>" && lob == "<%= IUMConstants.LOB_INDIVIDUAL_LIFE %>") {
			  	      if (countStatusOrdered(form) == "false") {
				         form.facilitator.disabled = false;
				      } else {
				         form.facilitator.disabled = true;
				      }
			    }
				
				if (status == "<%= IUMConstants.STATUS_ORDERED %>") {
					
				    if (lob == "<%= IUMConstants.LOB_PRE_NEED %>"
				    		|| lob == "<%= IUMConstants.LOB_GROUP_LIFE %>") {
				    	
				       if (reqStat == "<%= IUMConstants.STATUS_NB_REVIEW_ACTION %>"
				    		   || reqStat == "<%= IUMConstants.STATUS_AWAITING_REQUIREMENTS %>") {
				    	   
				           if (!form.facilitator.disabled) {
				    	        form.facilitator.disabled = true;
				    	   }
			       	   }
				       
				       if (reqStat == "<%= IUMConstants.STATUS_UNDERGOING_ASSESSMENT %>"
				    		   || reqStat == "<%= IUMConstants.STATUS_AWAITING_REQUIREMENTS %>") {
				    	   
				           if (form.facilitator.disabled) {
				  	       	   if (countStatusOrdered(form) == "false") {
			    	 	          form.facilitator.disabled = false;
			    	 	       }
			    	       }
			       	   }
				    }
				}
			}
			
			function validateFollowUpDate(inputFollowUpDate) {
				
				var dateToday = document.getElementById("dateToday");
				var months = new Array("JAN","FEB","MAR","APR","MAY","JUN","JUL","AUG","SEP","OCT","NOV","DEC");
				
				if((dateToday != null && dateToday != "") && (inputFollowUpDate != null && inputFollowUpDate != "")){
					var currentDate = dateToday.value;
					var followUpDate = inputFollowUpDate.value;
					
					var errorMessage;
					
					var dateToday_day = currentDate.substring(0,2);
					var dateToday_month = currentDate.substring(2,5).toUpperCase();
					var dateToday_year = currentDate.substring(5,9);
					
					if(!isValidDate(followUpDate)){
						alert("Invalid Input Date : "+followUpDate)
						return false;
					}
					
					var followUpDate_day = followUpDate.substring(0,2);
					var followUpDate_month = followUpDate.substring(2,5).toUpperCase();
					var followUpDate_year = followUpDate.substring(5,9);
					
					if(followUpDate_year == dateToday_year){
						var followUpDate_month_index, dateToday_month_index;
						
						followUpDate_month_index = getIndexForLookUpValue(followUpDate_month,months);
						dateToday_month_index = getIndexForLookUpValue(dateToday_month, months);
						
						if(followUpDate_month_index == dateToday_month_index){
							if(followUpDate_day < dateToday_day){
								errorMessage = "Invalid Date: The inputed day is lesser than current day("+currentDate+")";
								inputFollowUpDate.value = "";
							}else if(followUpDate_day == dateToday_day){
								errorMessage = "Invalid Date: The inputed day is equal current day("+currentDate+")";
								inputFollowUpDate.value = "";
							}
						}else if(followUpDate_month_index < dateToday_month_index){
							errorMessage = "Invalid Date: The inputed month is lesser than current month("+currentDate+")";
							inputFollowUpDate.value = "";
						}
						
					}else if(followUpDate_year < dateToday_year){
						errorMessage = "Invalid Date: The inputed year is lesser than current date("+currentDate+")";
						inputFollowUpDate.value = "";
					}
					
					if(errorMessage != null && errorMessage != ""){
						alert(errorMessage);
						return false;
					}
				}
			}
			
			function getIndexForLookUpValue(lookUpValue,toLookUpCollection){
				
				if(lookUpValue != null && toLookUpCollection != null){
					for(var index = 0; index < toLookUpCollection.length; index++){
						if(lookUpValue == toLookUpCollection[index]){
							return index;
						}
					}
				}
			}
		</script>
	</body>
</html>