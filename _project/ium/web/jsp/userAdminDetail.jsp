<%@ taglib uri="/struts-html.tld" prefix="html" %>
<%@ taglib uri="/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/ium.tld" prefix="ium" %>
<%@page language="java" import="java.util.*,com.slocpi.ium.util.*, com.slocpi.ium.ui.util.*, com.slocpi.ium.ui.form.UserProfileForm,java.text.*, com.slocpi.ium.ui.form.UserRoleForm" buffer="12kb"%>
<jsp:useBean id="userProfileForm" scope="request" class="com.slocpi.ium.ui.form.UserProfileForm"/>
<%String contextPath = request.getContextPath(); %>
<%
/* Andre Ceasar Dacanay - css enhancement - start */
String iumCss = "";
String iumColorB = "";
String iumColorO = "";
String companyCode = "";
try {
	ResourceBundle rbWMSSP = ResourceBundle.getBundle("dbconfig");
	iumCss = rbWMSSP.getString("IUM_CSS_VERSION");
	companyCode = iumCss;
	iumCss = "_"+iumCss;
} catch (Exception e){}
if(iumCss.equals("_CP")){
	iumColorB="#2A4C7C";
	iumColorO = "#FFCB00";
	companyCode="";
}else if(iumCss.equals("_GF")){
	iumColorB="#303A77";
	iumColorO = "#303A77";
	companyCode = companyCode+" ";
}else{
	iumColorB="#2A4C7C";
	iumColorO = "#FFCB00";
}
/* Andre Ceasar Dacanay - css enhancement - end */
%>
<%String mode        = "add"; %>

<%if (request.getParameter("mode") != null && !request.getParameter("mode").equals("add") ) { 
    mode = "edit"; 
  } 
%>
<%
	StateHandler sessionHandler = new StateHandler();
    Access access = new Access();
    
   HashMap userAccess = sessionHandler.getUserAccess(request);
%>
<% String view="";
   if(request.getParameter("view")!=null){
	    view = request.getParameter("view");
	 }
%>


<%String filter      = IUMConstants.LIST_USERS; %>
<%if (request.getAttribute("filter") != null ) { %>
<%    filter = (String) request.getAttribute("filter"); } 
  else if(userProfileForm.getUserType() !="" && userProfileForm.getUserType() != null){
  	filter = userProfileForm.getUserType();
  }
%>
<%    if (filter.equals("E")) filter = IUMConstants.LIST_EXAMINERS; %>
<%    if (filter.equals("L")) filter = IUMConstants.LIST_LABORATORIES; %>
<%	  if (filter.equals("A") || filter.equals("S")){
		filter = IUMConstants.LIST_USERS;
		
	  }
    
%>

<html>
<body>
<head>
<title><%=companyCode%>Integrated Underwriting and Medical System</title>
<link href="<%=contextPath%>/css/pw_main.css"     rel="stylesheet" type="text/css">
<link href="<%=contextPath%>/css/main<%=iumCss%>.css" rel="stylesheet" type="text/css">
<link href="<%=contextPath%>/css/styleCommon.css" rel="stylesheet" type="text/css">
<script language="JavaScript" src="<%=contextPath%>/js/dhtml_init.js"></script>
<script language="JavaScript" src="<%=contextPath%>/js/navigation.js"></script>
<script language="JavaScript" src="<%=contextPath%>/js/validation.js"></script>
<script language="JavaScript" src="<%=contextPath%>/jsp/sl_left_nav.jsp"></script>
<%--<script language="Javascript" src="<%=contextPath%>/jsp/validate_user_profile.jsp"></script>--%>
<script language="JavaScript">

function checkSpace(str) {
    var hasSpace = "false";
    var len = str.length;
    var cnt = 0;
    for (var i=0;i<len;i++) {
        var strtemp = str.substr(i,1);
        if (trim(strtemp) == "") {
           if (i>0 && i<(len-1)) {
	           cnt++;
	       }
        }
    }
    if (cnt > 0) {
       hasSpace = "true";
    }
    return hasSpace;
}

function validateFields(contextPath, mode) {
    var form   = document.forms[0];
    
    <% if (request.getParameter("mode") == null || request.getParameter("mode").equals("add")) { %>
    var userId = trim(form.userId.value);    
    form.userId.value = userId;
    if (userId == "") {
        alert("<bean:message key="error.field.required" arg0="User ID"/>");	            
        form.userId.value = "";
        form.userId.focus();
        return;
    }
    else {
        form.userId.value = userId.toUpperCase();
    }
    if (form.userType.value == "") {
        alert("<bean:message key="error.field.required" arg0="User Type"/>");	            
        return;
    }    
    if (checkSpace(userId) == "true") {
        alert("<bean:message key="error.field.invalidValue" arg0="User ID"/>");	            
        form.userId.focus();
        form.userId.select();
        return;
    }
	<%}%>    
	
	var acf2id = trim(form.acf2Id.value);
	form.acf2Id.value = acf2id;
	if (acf2id != "") {
	    form.acf2Id.value = acf2id.toUpperCase();
	}
	

	if(form.activated[0].checked==false && form.activated[1].checked==false){
	    alert('<bean:message key="error.field.singleselection" arg0="Activated"/>');
			frm.activated[0].focus();
			return false;						
	}else{
				if(form.activated[0].checked==true && checkDefaultRole()==false){
    	    alert('Default role is required to activate user.');
					frm.activated[0].checked=false;
					frm.activated[1].checked=true;
    			frm.activated[1].focus();
    			return false;						
				}
	
	}
	
	if(form.locked[0].checked==false && form.locked[1].checked==false){
	    alert('<bean:message key="error.field.singleselection" arg0="Locked"/>');
			frm.locked[1].focus();
			return false;						
	}
	
	
	
	var lastName = trim(form.lastName.value);
	form.lastName.value = lastName;
    if (lastName == "") {
        alert("<bean:message key="error.field.required" arg0="Last Name"/>");	            
        form.lastName.value = "";
        form.lastName.focus();
        return;
    }
    else {
        form.lastName.value = lastName.toUpperCase();
    }
    
    var firstName = trim(form.firstName.value);
    form.firstName.value = firstName;
    if (firstName == "") {
        alert("<bean:message key="error.field.required" arg0="First Name"/>");	            
        form.firstName.value = "";
        form.firstName.focus();
        return;
    }
    else {
        form.firstName.value = firstName.toUpperCase();
    }
    
    var emailAddr = trim(form.emailAddr.value);
    form.emailAddr.value = emailAddr;
    if (emailAddr != "") {
        var email = emailAddr;
        var eArr  = email.split("@");
        if (eArr.length < 2) {
           alert("<bean:message key="error.field.length" arg0="Email Address"/>");
           form.emailAddr.focus();
           form.emailAddr.select();
           return;
        }
        else {
           var domain = eArr[1];
           var dArr = domain.split(".");
           if (dArr.length < 2) {
	           alert("<bean:message key="error.field.length" arg0="Email Address"/>");
	           form.emailAddr.focus();
	           form.emailAddr.select();
	           return;           
           }
        }
    }
    
    var contactNo = trim(form.contactNo.value);
    form.contactNo.value = contactNo;
    if (contactNo != "") {
       if (!isNumeric(contactNo)) {
           alert("<bean:message key="error.field.numeric" arg0="Contact Number"/>");
           form.contactNo.focus();
           form.contactNo.select();
           return;
       }
       if (contactNo.length < 7) {
           alert("<bean:message key="error.field.length" arg0="Contact Number"/>");
           form.contactNo.focus();
           form.contactNo.select();
           return;       
       }
    }
    
    var mobileNo = trim(form.mobileNo.value);
    form.mobileNo.value = mobileNo;
    if (mobileNo != "") {
       if (!isNumeric(mobileNo)) {
           alert("<bean:message key="error.field.numeric" arg0="Mobile Number"/>");
           form.mobileNo.focus();
           form.mobileNo.select();
           return;
       }
       if (mobileNo.length < 11) {
           alert("<bean:message key="error.field.length" arg0="Mobile Number"/>");
           form.mobileNo.focus();
           form.mobileNo.select();
           return;       
       }
       
    }
	if(document.forms[0].userType.value == "A" ){
	   	var branch = trim(form.branchName.value);
    	if(branch == ""){
	    	alert("<bean:message key="error.field.required" arg0="Sun Life Office / Branch"/>");	            
	        form.branchName.focus();
    	    return;
    	}
    }
    var addr1 = trim(form.addr1.value);
    form.addr1.value = addr1;
    if (addr1 != "") {
        form.addr1.value = addr1.toUpperCase();
    }
    
    var addr2 = trim(form.addr2.value);
    form.addr2.value = addr2;
    if (addr2 != "") {
        form.addr2.value = addr2.toUpperCase();
    }
    
    var addr3 = trim(form.addr3.value);
    form.addr3.value = addr3;
    if (addr3 != "") {
        form.addr3.value = addr3.toUpperCase();
    }
    
    var city = trim(form.city.value);
    form.city.value = city;
    if (city != "") {
        form.city.value = city.toUpperCase();
    }
    
    var country = trim(form.country.value);
    form.country.value = country;
    if (country != "") {
	    form.country.value = country.toUpperCase();
    }
    
    var middleName = trim(form.middleName.value);
    form.middleName.value = middleName;
    if (middleName != "") {
        form.middleName.value = middleName.toUpperCase();
    }
    
    var zipcode = trim(form.zipCode.value);
    form.zipCode.value = zipcode;
    if (zipcode != "") {
        form.zipCode.value = zipcode.toUpperCase();
    }
		

		
    if (mode == "add") {
	    form.action = contextPath + "/createUserProfile.do?mode=add";
	}
	else {
			 if(checkDefaultRole()==true){
		  	     form.action = contextPath + "/createUserProfile.do";	
  	    		 document.frm.userId.disabled=false;
			 }
			 else {
		         alert("<bean:message key="error.field.requiredselection" arg0="Default Role"/>");	            
			     return;
			 }
	
	}	
    form.submit();
   
}

function checkDefaultRole(){
   var frm = document.frm;
	 var pass = true;
	 if(frm.defaultRole==null){
	    pass=false;
	 }else{
	    var length =frm.defaultRole.length;
			if(length==null){
			   if(frm.defaultRole.checked==false){
				    pass=false;
				 }
			}else{
			   var count=0;
			   for(i=0; i<frm.defaultRole.length;i++){
				    if(frm.defaultRole[i].checked==true){
						   count++;
						}
				 }
				 if (count==0){
				   pass=false;
				 }
			}
	 }
	 
	 return pass;
}

function selectUsersAdd(contextPath, mode) {
    var form = document.forms[0];
     if(form.userType.value != ""){
	    form.action = "<%=contextPath%>/listUsers.do?filter=" + form.userType.value;
	}
	else{
		form.action = "<%=contextPath%>/listUsers.do";
	}
    form.submit();
}

function selectUsersView() {
    var form = document.forms[0];
    form.action = "<%=contextPath%>/listUsers.do?mode=view&filter=" + form.userType.value;	
    form.submit();
}

function getSexValue(idx) {
   var form = document.forms[0];
   if (idx == "1") {
       form.sex.value = "M";
   }
   else {
       form.sex.value = "F";
   }
}

function focusOnFirstField() {
<% if (request.getParameter("mode") != null && !request.getParameter("mode").equals("add")) { %>
<%	    if(view.equals("maintain")){%>
            document.forms[0].acf2Id.focus();
            if (trim(document.forms[0].userType.value) == "" || document.forms[0].userType.value == "A" || document.forms[0].userType.value == "S") {
               document.forms[0].userCode.disabled = true;   
              
            }
<%		}else{ %>
				  	disableAll();
<%		}%>
<% } else { %>
	document.forms[0].userId.focus();
    if (trim(document.forms[0].userType.value) == "") {
        document.forms[0].userCode.disabled = true;          
        document.all["usrCode1"].style.display = "none";
    	document.all["usrCode2"].style.display = "block";
        
    }	
    else if(document.forms[0].userType.value == "<%=IUMConstants.USER_TYPE_EXAMINER%>" || document.forms[0].userType.value == "<%=IUMConstants.USER_TYPE_LABORATORY%>" ){
    	document.all["usrCode1"].style.display = "block";
    	document.all["usrCode2"].style.display = "none";
    }
    else if(document.forms[0].userType.value == "<%=IUMConstants.USER_TYPE_AGENT%>"){
    	document.all["branch1"].style.display = "block";
    	document.all["branch2"].style.display = "none";
    }
<% } %>
}

function addRole(){
   var frm = document.frm;
	 if (noSelection(frm.roleToAdd.value)){
       alert("<bean:message key="error.field.requiredselection" arg0="user role"/>");
	 }else{
	   var pass=true;
	 
		if(frm.usrRoles!=null){ 
			 var length =frm.usrRoles.length; 
			 if( length==null ){
		  	   	 if(frm.usrRoles.value==frm.roleToAdd.value){
		  			     pass=false;
		  		 }			 	  
			 }else{
	    	  	for(i=0; i<length; i++){
	    	   		if(frm.usrRoles[i].value==frm.roleToAdd.value){
	    			     pass=false;
	    					 break;
	    			}			 	  
				}
			 }
		 }
		 if(pass==true){
	 	    frm.action="addUserRole.do";
			frm.userId.disabled=false;
			frm.submit();					
	 	 }else{
	        alert("<bean:message key="error.field.alreadyexisting" arg0="role"/>");
	     }
	
		 			 
	}
}

function countChecked(){
	var frm = document.frm;
	var selected =0;
	if(frm.usrRoles!=null){
	    var length =frm.usrRoles.length; 
		
		if (length==null){
			if (frm.usrRoles.checked==true){
				selected++;
			} 
		 }else {
			for (var i=0; i<length; i++){
				if (frm.usrRoles[i].checked==true){
					selected++;
				}
			}
		 }
	 }
	 return selected;
}

function removeRole(){
	var frm = document.frm;
	if(countChecked()>0){
    var length  = frm.usrRoles.length;
		if(length==null){
		   length=1;
		}
		if(length>countChecked()){
		    frm.action="removeUserRole.do";
				frm.userId.disabled=false;
				frm.submit();
		}else{
				var pass=confirm("Removing all User Roles will deactivate this user.");
				if(pass==true){	
  		    frm.action="removeUserRole.do";
  				frm.userId.disabled=false;
  				frm.submit();
				}
		}
		
	}else{
		alert('<bean:message key="error.field.requiredselection" arg0="role"/>');
	}
	
}

/*function manipulateButtons(){
   document.all['MaintainButton'].className = 'hide';
 	 document.all['SaveButton'].className = 'show';
 	 document.all['CancelButton'].className = 'show'; 	
	 enableAll();
	 document.frm.acf2Id.focus(); 
}*/

function disableAll(){
	var frm = document.frm;
	frm.userId.disabled=true;
	frm.acf2Id.disabled=true;
	frm.userType.disabled=true;
	frm.userCode.disabled=true;
	frm.activated[0].disabled=true;
	frm.activated[1].disabled=true;
	frm.locked[0].disabled=true;
	frm.locked[1].disabled=true;
	frm.lastName.disabled=true;
	frm.firstName.disabled=true;
	frm.middleName.disabled=true;		
	frm.gender[0].disabled=true;
	frm.gender[1].disabled=true;
	frm.emailAddr.disabled=true;
	frm.contactNo.disabled=true;
	frm.mobileNo.disabled=true;
	frm.branchName.disabled=true;
	frm.department.disabled=true;
	frm.section.disabled=true;	
	frm.roleToAdd.disabled=true;
	document.all['Add Role'].disabled=true;
	document.all['Remove Role'].disabled=true;
	document.all['User Access'].disabled=true;
//	document.all['Reset Password'].disabled=true;
	frm.allRoles.disabled=true;
	if(frm.usrRoles.value==null){
	  for(i=0; i<frm.usrRoles.length; i++){
		   frm.usrRoles[i].disabled=true;
			 frm.defaultRole[i].disabled=true;
		}
	}else{
			frm.usrRoles.disabled=true;	
		  frm.defaultRole.disabled=true;
	}	 
	
	frm.addr1.disabled=true;
	frm.addr2.disabled=true;
	frm.addr3.disabled=true;
	frm.city.disabled=true;
	frm.country.disabled=true;
	frm.zipCode.disabled=true;
					
}

function enableAll(){
	var frm = document.frm;
	//frm.userId.disabled=false;
	frm.acf2Id.disabled=false;
	//frm.userType.disabled=false;
	//frm.userCode.disabled=false;
	frm.activated[0].disabled=false;
	frm.activated[1].disabled=false;
	frm.locked[0].disabled=false;
	frm.locked[1].disabled=false;
	frm.lastName.disabled=false;
	frm.firstName.disabled=false;
	frm.middleName.disabled=false;		
	frm.gender[0].disabled=false;
	frm.gender[1].disabled=false;
	frm.emailAddr.disabled=false;
	frm.contactNo.disabled=false;
	frm.mobileNo.disabled=false;
	frm.branchName.disabled=false;
	frm.department.disabled=false;
	frm.section.disabled=false;	
	frm.roleToAdd.disabled=false;
	document.all['Add Role'].disabled=false;
<%	if(userProfileForm.getRoles()!=null&&userProfileForm.getRoles().size()>0){%>
	document.all['Remove Role'].disabled=false;
	document.all['User Access'].disabled=false;
<%	}%>

//	document.all['Reset Password'].disabled=false;
	frm.allRoles.disabled=false;
	if(frm.usrRoles.value==null){
	  alert('');
	  for(i=0; i<frm.usrRoles.length; i++){
		   frm.usrRoles[i].disabled=false;
			 frm.defaultRole[i].disabled=false;
		}
	}else{
			frm.usrRoles.disabled=false;	
		  frm.defaultRole.disabled=false;
	}	 
	
	frm.addr1.disabled=false;
	frm.addr2.disabled=false;
	frm.addr3.disabled=false;
	frm.city.disabled=false;
	frm.country.disabled=false;
	frm.zipCode.disabled=false;
					
}
</script>
</head>

<BODY leftMargin=0 topmargin=0 marginwidth=0 marginheight=0 onLoad="focusOnFirstField();">
<table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%">
<tr><td width="100%" colspan="2">
<jsp:include page="header.jsp" flush="true"/>
</td>
</tr>
<tr>
  <td rowspan="2" bgcolor="<%=iumColorO%>" height="100%" width="20"><img src="<%=contextPath%>/images/spacer.gif" width="20"></td>
            <td background="<%=contextPath%>/images/GradientGray.jpg" height="27" width="100%" valign="top">&nbsp;&nbsp;
              <span class="main">Integrated Underwriting and Medical System</span>
            </td>
          </tr>
          <tr>
            <td width="100%" height="100%" valign="top">
              <!-- BODY -->
        <table width="100%" cellpadding="3" cellspacing="5" border="0">
          <tr> 
          <% if (request.getParameter("mode") != null && !request.getParameter("mode").equals("add") ) { %>
            <td class="title2"><b>Edit User Profile</b>              
            </td>
          <% } else { %>          
            <td class="title2"><b>Create User Profile</b>              
            </td>
          <% } %>
          </tr>
          <tr valign="top"> 
            <td> 
<!--- START OF BODY -->
<form name="frm" action="main.html" method="post">
<table>
<tr>
<td colspan="2" class="error">
<html:errors/>
</td>
</tr>
</table>
</tr>
</td>
<tr>
<td>
<input type="hidden" name="fullName" value='<bean:write name="userProfileForm" property="firstName"/> <bean:write name="userProfileForm" property="lastName"/>'>
<table border="1" bordercolor="<%=iumColorB%>" cellpadding=10 cellspacing=0 bgcolor="" width="500" class="listtable1">
        <tr><td colspan="2" height="100%">
        <table border="0" width="100%" >
        <tr>
        <% if (mode.equals("add")) { %>		
        <td  class="label2"><span class="required">*</span><b>User ID </b></td>        
        <% } else { %>
        <td  class="label2"><b>&nbsp;&nbsp;User ID </b></td>        
        <% } %>        
        <td>
        <% if (mode.equals("add")) { %>				        
        <input type="text" class="label2" value='<bean:write name="userProfileForm" property="userId"/>' maxLength="10" name="userId"/>
        <% } else { %>
        <input type="text" class="label2" value='<bean:write name="userProfileForm" property="userId"/>' maxLength="10" name="userId" disabled/>        
        <% } %>
         </td>
		<td>&nbsp;</td>				
        <td class="label2"><b>&nbsp;&nbsp;ACF2 ID</b></td>
        <td><input type="text" class="label2" value='<bean:write name="userProfileForm" property="acf2Id"/>' maxLength="8" name="acf2Id"> </td>
        </tr>
        <tr>
        <% if (mode.equals("add")) { %>				   
        <td class="label2"><span class="required">*</span><b>User Type</b></td>
        <% } else { %>
        <td class="label2"><b>&nbsp;&nbsp;User Type</b></td>
        <% } %>
        <td>
        <% if (mode.equals("add")) { %>
          <ium:list className="label2" listBoxName="userType" type="<%=IUMConstants.LIST_USER_TYPE%>" styleName="width:200" selectedItem="<%=userProfileForm.getUserType()%>" onChange="selectUsersAdd();"/>
        <% } else { %>
          <ium:list className="label2" listBoxName="userType" type="<%=IUMConstants.LIST_USER_TYPE%>" styleName="width:200" selectedItem="<%=userProfileForm.getUserType()%>" onChange="selectUsersView();" disabled="disable"/>
        <% } %>        
        </td>
		<td>&nbsp;</td>				        
		
        <td class="label2" id="usrCode1" style="display:none"><span class="required">*</span><b>User &nbsp;&nbsp;Code</b></td>        
        <td class="label2" id="usrCode2"><b>&nbsp;&nbsp;User &nbsp;&nbsp;Code</b></td>
        <td class="label2" colspan="4">
   
        <% 
		
        if (mode.equals("add") && !(filter == IUMConstants.LIST_USERS)) { %>
        <ium:list className="label2" listBoxName="userCode" type="<%=filter%>" styleName="width:300" selectedItem="<%=userProfileForm.getUserCode()%>"/></td>
        <% } else { %>
        <ium:list className="label2" listBoxName="userCode" type="<%=filter%>" styleName="width:300" selectedItem="<%=userProfileForm.getUserCode()%>" disabled="true"/></td>
        <% } %>
		</tr>        
		<%
		
		String activeY = userProfileForm.getActivated().equalsIgnoreCase("Y")?"checked":"";
		String activeX = userProfileForm.getActivated().equalsIgnoreCase("N")?"checked":"";
		
		if(mode.equals("add"))	{
			 activeY = "";
			 activeX = "checked";
		}

		String lockY = userProfileForm.getLocked().equalsIgnoreCase("Y")?"checked":"";
		String lockX = userProfileForm.getLocked().equalsIgnoreCase("N")?"checked":"";

		if(mode.equals("add"))	{
			 lockY = "";
			 lockX = "checked";
		}
		
		%>
        <tr>
        <% if (mode.equals("add") || view.equals("maintain")) { %>
        <td class="label2"><span class="required">*</span><b>Activated?</b></td>
        <%} else {%>
        <td class="label2"><b>&nbsp;&nbsp;Activated?</b></td>
        <%}%> 
        <% if (mode.equals("add"))	{%>
        <td class="label2"><input type="radio" name="activated" value="Y" <%=activeY%> disabled >Yes&nbsp;<input type="radio" name="activated" value="N"  <%=activeX%> disabled>No&nbsp; </td>
        <%} else {%>
        <td class="label2"><input type="radio" name="activated" value="Y" <%=activeY%> >Yes&nbsp;<input type="radio" name="activated" value="N"  <%=activeX%>>No&nbsp; </td>
        <%}%>
				<td>&nbsp;</td>
	    <% if (mode.equals("add") || view.equals("maintain")) { %>				
        <td class="label2"><span class="required">*</span><b>Locked?</b></td>
        <%} else {%>
        <td class="label2"><b>&nbsp;&nbsp;Locked?</b></td>
        <%}%> 
        <td class="label2"><input type="radio" name="locked" value="Y" <%=lockY%>>Yes&nbsp;<input type="radio" name="locked" value="N"  <%=lockX%>>No&nbsp; </td>			
        </tr>

        <tr>
        <% if (mode.equals("add") || view.equals("maintain")) { %>	
        <td class="label2"><span class="required">*</span><b>Last Name </b></td>
        <%} else {%>
        <td class="label2"><b>&nbsp;&nbsp;Last Name </b></td>
        <%}%>
        <td><input type="text" class="label2" size ="30" value='<bean:write name="userProfileForm" property="lastName"/>' maxLength="40" name="lastName"> </td>
				<td>&nbsp;</td>
		<% if (mode.equals("add") || view.equals("maintain")) { %>				
        <td class="label2"><span class="required">*</span><b>First &nbsp;&nbsp;Name </b></td>
        <%} else {%>
        <td class="label2"><b>&nbsp;&nbsp;First &nbsp;&nbsp;Name </b></td>
        <%}%>
        <td><input type="text" class="label2" size ="30" value='<bean:write name="userProfileForm" property="firstName"/>' maxLength="25" name="firstName"> </td>
				<td>&nbsp;</td>
        <td class="label2"><b>&nbsp;&nbsp;Middle &nbsp;&nbsp;Name </b></td>
        <td><input type="text" class="label2" size ="30" value='<bean:write name="userProfileForm" property="middleName"/>' maxLength="25" name="middleName"> </td>
        </tr>
        <tr>
        <td class="label2"><b>&nbsp;&nbsp;Gender</b></td>
        <td class="label2">
        <%String radio_M = "";
          String radio_F = "";
          
          if (userProfileForm.getSex() != null) {
          	if (!userProfileForm.getSex().equals("")){
	             if (userProfileForm.getSex().equals("M")) {
	                radio_M = "checked";
	             }
	             else if (userProfileForm.getSex().equals("F")) {
	                radio_F = "checked";
	             }
	         }
	         else
				radio_M = "checked";
          }          
        %>
        <input type="radio" name="gender" value="M" <%=radio_M%> onClick="getSexValue('1');">Male &nbsp;
        <input type="radio" name="gender" value="F" <%=radio_F%> onClick="getSexValue('2');">Female &nbsp;
        <input type="hidden" name="sex" value='<bean:write name="userProfileForm" property="sex"/>'>
        </td>
        </tr>
				<tr><td colspan="8">&nbsp;</td></tr>
								
        <tr>
        <td class="label2"><b>&nbsp;&nbsp;Email &nbsp;&nbsp;Address	 </b></td>
        <td  colspan="7"><input type="text" class="label2" size ="25" value='<bean:write name="userProfileForm" property="emailAddr"/>' maxLength="50" name="emailAddr"> </td>
        </tr>
        <tr>
        <td class="label2"><b>&nbsp;&nbsp;Contact &nbsp;&nbsp;Number	 </b></td>
        <td><input type="text" class="label2" value='<bean:write name="userProfileForm" property="contactNo"/>' size="20" maxLength="15" name="contactNo"> </td>
				<td>&nbsp;</td>
        <td class="label2"><b>&nbsp;&nbsp;Mobile &nbsp;&nbsp;Number	 </b></td>
        <td colspan="4"><input type="text" class="label2" value='<bean:write name="userProfileForm" property="mobileNo"/>' size="20" maxLength="13" name="mobileNo"> </td>
        </tr>
				
				<tr><td colspan="8">&nbsp;</td></tr>				
        <tr>
        <td class="label2" id="branch1" style="display:none"><span class="required">*</span><b>Sun Life &nbsp;&nbsp;Office/Branch</b></td>
        <td class="label2" id="branch2"><b>&nbsp;&nbsp;Sun Life &nbsp;&nbsp;Office/Branch</b></td>
        <td colspan="7">
         <ium:list className="label2" listBoxName="branchName" type="<%=IUMConstants.LIST_BRANCHES%>" selectedItem="<%=userProfileForm.getBranchName()%>"/>
				 </td>
        </tr>
				<tr>
        <td class="label2"><b>&nbsp;&nbsp;Department</b></td>
        <td><ium:list className="label2" listBoxName="department" type="<%=IUMConstants.LIST_DEPARTMENT%>" selectedItem="<%=userProfileForm.getDepartment()%>"  styleName="width:250"/></td>
				<td>&nbsp;</td>
        <td class="label2"><b>&nbsp;&nbsp;Section</b>&nbsp;</td>
        <td colspan="4"><ium:list className="label2" listBoxName="section" type="<%=IUMConstants.LIST_SECTION%>" selectedItem="<%=userProfileForm.getSection()%>" styleName="width:300"/></td>
        </tr>
				<tr><td colspan="8">&nbsp;</td></tr>


				<tr>
        <% 
				String disabled="";
				String haveRecord="";
				String haveRecord2="";
				if (mode.equals("add")) {
					 disabled="disabled"; 
				}
				if(userProfileForm.getRoles()==null||userProfileForm.getRoles().size()==0)
				{
					 haveRecord="disabled";																																				 
				}
				if(userProfileForm.getRoles()==null||userProfileForm.getRoles().size()==0
						|| (!access.hasAccess(userAccess,IUMConstants.PAGE_USER_ACCESS,IUMConstants.ACC_MAINTAIN)))
				{
					 haveRecord2="disabled";																																				 
				}
				%>
				
        <td class="label2" valign="TOP"><b>&nbsp;&nbsp;User Roles</b></td>
				<td class="label2" valign="top" colspan="3">
						
								<%if(disabled.equals("")){%>
								<ium:list className="label2" listBoxName="roleToAdd" type="<%=IUMConstants.LIST_ROLES%>" selectedItem="" styleName="width:300"/>
								<%}else {%>
								<ium:list className="label2" listBoxName="roleToAdd" type="<%=IUMConstants.LIST_ROLES%>" selectedItem="" styleName="width:300" disabled="disable"/>
								<%}%>								
				</td>
				<td colspan="4">
						<input type="button" name="Add Role"  class="button1" style="width:150" value="Add Role..." onClick="addRole();" <%=disabled%> ><br>
				</td>
				<tr>
				<td>&nbsp;</td>
				<td class="label2" valign="top" colspan="3">				
						<table width="100%" >
						<tr>
								<td class="headerrow1" width="5%"><input type="checkbox" class="label1" name="allRoles" onclick="markCheckBox(document.forms[0].allRoles, document.forms[0].usrRoles)" <%=haveRecord%> ></td>
								<td class="headerrow1" align="center" width="80%">Roles</td>
								<td class="headerrow1" width="15%">Default</td>
						</tr>

			
						
						<%if(!haveRecord.equals("")){%>				
						<tr class="row1">
								<td colspan="3"  class="label2" align="center"><B>NONE</B></td>
						</tr>
						<%}else{

              int i=0;
              String tr_class;
              String td_bgcolor;
              %>						
             <logic:iterate id="role" name="userProfileForm" property="roles"  scope="request">
            	<%
              if (i%2 == 0) {
                    tr_class = "row1";
                    td_bgcolor = "#CECECE";
              } else {
                    tr_class = "row2";
                    td_bgcolor = "#EDEFF0";
             	}
            	String checked = "";
            	UserRoleForm usf = (UserRoleForm)role;
            	if(usf.getUserRoleCode().equals(userProfileForm.getDefaultRole()) ){
            		checked="checked";
            	}
              %>
						
                  <tr class="<%=tr_class%>" >
            				<td><input type="checkbox" class="label1" name="usrRoles" value='<bean:write name="role" property="userRoleCode"/>'></td>
            				<td class="label2" align="left"><bean:write name="role" property="description"/> </td>
            				<td class="label2" align="center"><input type="radio" name="defaultRole" value='<bean:write name="role" property="userRoleCode"/>' <%=checked%> ></td>
            			</tr>
        			<%i++;%>
         </logic:iterate>	
				 		<%}%>						
						</table>
				</td>
				<td colspan="4">
						<input type="button" name="Remove Role"   class="button1" style="width:150" value="Remove Role..." <%=haveRecord%> onClick="removeRole();"><br>
						<input type="button" name="User Access" class="button1"  style="width:150" value="User Access..." <%=haveRecord2%> onClick="document.frm.userId.disabled=false;gotoPage('frm','<%=contextPath%>/viewAccess.do');"><br>
						<!-- comment out for ldap impl
						<input type="button" name="Reset Password" class="button1"  style="width:150" value="Reset Password..." onClick="document.frm.userId.disabled=false;gotoPage('frm','<%=contextPath%>/resetPassword.do');" <%=haveRecord2%> >
						-->
				</td>
				
				</tr>				

				<tr><td colspan="8">&nbsp;</td></tr>
        <tr>
        <td class="label2"><b>&nbsp;&nbsp;Address 1	 </b></td>
        <td  colspan="7"><input type="text" class="label2" size ="50" value='<bean:write name="userProfileForm" property="addr1"/>' maxLength="80" name="addr1"> </td>
        </tr>
        <tr>
        <td class="label2"><b>&nbsp;&nbsp;Address 2</b></td>
        <td   colspan="7"><input type="text" class="label2" size ="50" value='<bean:write name="userProfileForm" property="addr2"/>' maxLength="80" name="addr2"> </td>
        </tr>
        <tr>
        <td class="label2"><b>&nbsp;&nbsp;Address 3</b></td>
        <td   colspan="7"><input type="text" class="label2" size ="50" value='<bean:write name="userProfileForm" property="addr3"/>' maxLength="80" name="addr3"> </td>
        </tr>

        <tr>
        <td class="label2"><b>&nbsp;&nbsp;City	 </b></td>
        <td><input type="text" class="label2" value='<bean:write name="userProfileForm" property="city"/>' maxLength="20" name="city"> </td>
				<td>&nbsp;</td>
        <td class="label2"><b>&nbsp;&nbsp;Country	 </b></td>
        <td><input type="text" class="label2" value='<bean:write name="userProfileForm" property="country"/>' maxLength="20" name="country"> </td>
				<td>&nbsp;</td>
        <td class="label2"><b>&nbsp;&nbsp;Zip Code	 </b></td>
        <td><input type="text" class="label2" value='<bean:write name="userProfileForm" property="zipCode"/>' size="5" maxLength="5" name="zipCode"> </td>
        </tr>


        
        <tr><td colspan="8" align="right">&nbsp;</td></tr>

        <tr><td colspan="8" align="left">
<%				if (mode.equals("add") || view.equals("maintain") ) {%>				
        <input type="button" value="  Save  " class="button1" onclick="validateFields('<%=contextPath%>','<%=mode%>');" id="SaveButton">&nbsp;
        <input type="button" value=" Cancel " class="button1" onclick="gotoPage('frm','<%=contextPath%>/listUserProfile.do');" id="CancelButton">				
<%			 } else { %>
        <input type="button" value=" Back " class="button1" onclick="gotoPage('frm','<%=contextPath%>/listUserProfile.do');" id="CancelButton">
<%			 }        %>				
        
        </td></tr>
        
        
        </table>
		</td></tr>

</table>
</form>
<!--- END OF BODY -->
</td>
</tr>

</table>
</body>
</html>