<%@ page language="java" import="com.slocpi.ium.util.*" buffer="12kb"%>
<%@ taglib uri="/struts-bean.tld" prefix="bean" %>

<!--
function initializeCDS() {
	var form = document.requestForm;
	
	showSummary();
	showPolicyCoverage();
	showMortalityRateInsured();
	if (form.hasOwner.value == "true") {
		hideMortalityRateOwner();
	}
}


function showPolicyCoverage() {	
	//document.all["policyCoverage"].className = "show";
	document.all["policyCoverage"].style.display = "block";
}

	
function hidePolicyCoverage() {
	//document.all["policyCoverage"].className = "hide";
	document.all["policyCoverage"].style.display = "none";
}


function showMortalityRateOwner() {	
	//document.all["OWNER"].className = "show";
	document.all["OWNER"].style.display = "block";
}

	
function hideMortalityRateOwner() {
	//document.all["OWNER"].className = "hide";
	document.all["OWNER"].style.display = "none";
}


function showMortalityRateInsured() {	
	//document.all["INSURED"].className = "show";
	document.all["INSURED"].style.display = "block";
}

	
function hideMortalityRateInsured() {
	//document.all["INSURED"].className = "hide";
	document.all["INSURED"].className = "none";
}


function showSummary() {	
	//document.all["summary"].className = "show";
	document.all["summary"].style.display = "block";
}

	
function hideSummary() {
	//document.all["summary"].className = "hide";
	document.all["summary"].style.display = "none";
}


function switchClient(value) {
	var form = document.requestForm;
	var lob = form.lob.value;
	if (lob == "<%=IUMConstants.LOB_INDIVIDUAL_LIFE%>") {
		if (value.toUpperCase() == "<%=IUMConstants.CLIENT_TYPE_OWNER%>") {
			hideInsuredDetails();	
			showOwnerDetails();

			// set client type with correct chosen value
			form.ownerClientType.selectedIndex = form.insuredClientType.selectedIndex;

			if (form.hasOwner.value != "null") {
				if (form.hasOwner.value == "true") {
					hideSummary();
					hidePolicyCoverage();

					showMortalityRateOwner();
					showMortalityRateInsured();
				}
			}
		}
		else if (value.toUpperCase() == "<%=IUMConstants.CLIENT_TYPE_INSURED%>") {
			showInsuredDetails();	
			hideOwnerDetails();

			// set client type with correct chosen value
			form.insuredClientType.selectedIndex = form.ownerClientType.selectedIndex;
			
			if (form.hasOwner.value != "null") {
				showSummary();
				showPolicyCoverage();
			
				if (form.hasOwner.value == "true") {
					hideMortalityRateOwner();
				}
				showMortalityRateInsured();
			}
		}
		else if (value.toUpperCase() == "<%=IUMConstants.CLIENT_TYPE_MEMBER%>") {
			alert("<bean:message key="error.field.applicableto" arg0="This client type" arg1="Group Life LOB" />");
		}
		else if (value.toUpperCase() == "<%=IUMConstants.CLIENT_TYPE_PLANHOLDER%>") {
			alert("<bean:message key="error.field.applicableto" arg0="This client type" arg1="Pre-Need LOB" />");
		}				
	}
	else if ((lob == "<%=IUMConstants.LOB_GROUP_LIFE%>") || (lob == "<%=IUMConstants.LOB_PRE_NEED%>")){	
		showInsuredDetails();	
		//hideOwnerDetails();
			
		// set client type with correct chosen value
		form.insuredClientType.selectedIndex = form.ownerClientType.selectedIndex;
	
		if (form.hasOwner.value != "null") {
		
			showSummary();
			showPolicyCoverage();
		
			if (form.hasOwner.value == "true") {
				hideMortalityRateOwner();
			}
			showMortalityRateInsured();
		}
	}	
}


function haveSelectedStatus(){
	var form = document.requestForm;
	var status = form.requestStatus.value;
	if (status == null || status ==""){
		alert("<bean:message key="error.field.requiredselection" arg0="status"/>");
		form.requestStatus.focus();
		return false;
	} else {
		return true;
	}
}


function rePaginate (page, actionUrl) {
	form = document.requestForm;
	if (form.pageNo != null) {
		form.pageNo.value = page;	
	}
	gotoPage("requestForm", actionUrl);
}


function initializeClientData(value) {
	var form = document.requestForm;
	var lob = form.lob.value;
	
	if(value=="<%=IUMConstants.CLIENT_TYPE_INSURED%>"){
		if(isEmpty(form.insuredLastName.value)){
			alert("There is no existing client with this number - " + form.insuredClientNo.value);
		}
				
	} else if(value=="<%=IUMConstants.CLIENT_TYPE_OWNER%>"){
		if(isEmpty(form.ownerLastName.value)){
			alert("There is no existing client with this number - " + form.ownerClientNo.value);
		}
	}
	
	if (lob == "<%=IUMConstants.LOB_INDIVIDUAL_LIFE%>") {
		if (value.toUpperCase() == "<%=IUMConstants.CLIENT_TYPE_OWNER%>") {
			hideInsuredDetails();	
			showOwnerDetails();


			if (form.hasOwner.value != "null") {
				if (form.hasOwner.value == "true") {
					hideSummary();
					hidePolicyCoverage();

					showMortalityRateOwner();
					showMortalityRateInsured();
				}
			}
		}
		else if (value.toUpperCase() == "<%=IUMConstants.CLIENT_TYPE_INSURED%>") {
			showInsuredDetails();	
			hideOwnerDetails();

			
			if (form.hasOwner.value != "null") {
				showSummary();
				showPolicyCoverage();
			
				if (form.hasOwner.value == "true") {
					hideMortalityRateOwner();
				}
				showMortalityRateInsured();
			}
		}
		else if (value.toUpperCase() == "<%=IUMConstants.CLIENT_TYPE_MEMBER%>") {
			alert("<bean:message key="error.field.applicableto" arg0="This client type" arg1="Group Life LOB" />");
		}
		else if (value.toUpperCase() == "<%=IUMConstants.CLIENT_TYPE_PLANHOLDER%>") {
			alert("<bean:message key="error.field.applicableto" arg0="This client type" arg1="Pre-Need LOB" />");
		}				
	}
	else if ((lob == "<%=IUMConstants.LOB_GROUP_LIFE%>") || (lob == "<%=IUMConstants.LOB_PRE_NEED%>")){	
		showInsuredDetails();	
		//hideOwnerDetails();
			
	
		if (form.hasOwner.value != "null") {
		
			showSummary();
			showPolicyCoverage();
		
			if (form.hasOwner.value == "true") {
				hideMortalityRateOwner();
			}
			showMortalityRateInsured();
		}
	}	
}


//-->