<%@ page language="java" import="java.util.*, com.slocpi.ium.ui.form.*, com.slocpi.ium.util.*, com.slocpi.ium.ui.util.*, com.slocpi.ium.data.*" buffer="1024kb"%>
<%@ taglib uri="/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/struts-html.tld" prefix="html" %>
<%@ taglib uri="/ium.tld" prefix="ium" %>
<%String contextPath = request.getContextPath();%>
<%
/* Andre Ceasar Dacanay - css enhancement - start */
String iumCss = "";
String iumColorB = "";
String companyCode = "";
try {
	ResourceBundle rbWMSSP = ResourceBundle.getBundle("dbconfig");
	iumCss = rbWMSSP.getString("IUM_CSS_VERSION");
	companyCode = iumCss;
	iumCss = "_"+iumCss;
} catch (Exception e){}
if(iumCss.equals("_CP")){
	iumColorB="#2A4C7C";
	companyCode="";
}else if(iumCss.equals("_GF")){
	iumColorB="#303A77";
	companyCode = companyCode+" ";
}else{
	iumColorB="#2A4C7C";
}
/* Andre Ceasar Dacanay - css enhancement - end */
%>
<%
	String time = (String) request.getAttribute("schedule");
	String description = (String) request.getAttribute("description");
	
	if (time == null){
		time = "nosched";
	}
	if (description == null){
		description = "";
	}
%>


<%
    // This routine queries the access rights of the users (RCRIVERA)
    //
    // --- START --- 
    
    StateHandler sessionHandler = new StateHandler();
    Access access = new Access();
	HashMap userAccess = sessionHandler.getUserAccess(request);
    if(!access.hasAccess(userAccess,IUMConstants.PAGE_MIB_EXPORT,IUMConstants.ACC_INQUIRE) ){                                                                                                                    
    	response.sendRedirect("noAccessPage.jsp");   
    }
    
    
    String MAINTAIN_ACCESS = "";
    String EXECUTE_ACCESS = "";
    
    if(!access.hasAccess(userAccess,IUMConstants.PAGE_MIB_EXPORT,IUMConstants.ACC_MAINTAIN) ){                                                                                                                     
	  MAINTAIN_ACCESS = "DISABLED";
    }
    if(!access.hasAccess(userAccess,IUMConstants.PAGE_MIB_EXPORT,IUMConstants.ACC_EXECUTE) ){                                                                                                                     
	  EXECUTE_ACCESS = "DISABLED";
    }
    
    
    // --- END ---
 %>    

<html>
<body>
<head>
<title><%=companyCode%>Integrated Underwriting and Medical System</title>
    <link href="<%=contextPath%>/css/pw_main.css" rel="stylesheet" type="text/css">
    <link href="<%=contextPath%>/css/main<%=iumCss%>.css" rel="stylesheet" type="text/css">
    <link href="<%=contextPath%>/css/styleCommon.css" rel="stylesheet" type="text/css">
    <script language="JavaScript" src="<%=contextPath%>/js/dhtml_init.js"></script>
    <script language="JavaScript" src="<%=contextPath%>/js/navigation.js"></script>
    <script language="Javascript" src="<%=contextPath%>/js/validation.js"></script>
    <script language="JavaScript" src="<%=contextPath%>/jsp/sl_left_nav.jsp"></script>
	<script language="JavaScript" src="<%=contextPath%>/jsp/validate_auditTrail.jsp"></script>
	<script>
		function saveSchedule(){
			var form = document.schedule;
			form.functionSelected.value="save";
			form.action = "saveMIBSchedule.do" ;
			form.submit();
		}
		function exportSchedule(){
			var form = document.schedule;
			form.functionSelected.value="export";
			form.action = "displayMIBSchedule.do" ;
			form.submit();
		}
	</script>
</head>

<BODY leftMargin=0 topmargin=0 marginwidth=0 marginheight=0>
    <table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%">
      <tr>
        <td width="100%" colspan="2">
          <jsp:include page="header.jsp" flush="true"/>
          <table border="0" cellpadding="0" cellspacing="0" width="100%" height="100%">
            <tr valign="top">
              <td background="<%=contextPath%>/images/bg_yellow.gif" id="leftnav" valign="top" width="147">
                <img height="9" src="<%=contextPath%>/images/sp.gif" width="147"><br>
                <img height="30" src="<%=contextPath%>/images/sp.gif" width="7"> <br>
                <img height="7" src="<%=contextPath%>/images/sp.gif" width="147"><br>
                <div id="level0" style="position: absolute; visibility: hidden; width: 100%; z-index: 500"> 
        <SCRIPT language=javascript>
			<!--
				document.write(writeMenu(''))
				if (IE4) { document.write(writeDiv()) };
				
			//-->
			</SCRIPT>
      </DIV>
      <DIV id=NSFix style="POSITION: absolute; VISIBILITY: hidden; Z-INDEX: 999">&nbsp;</DIV>
      <!-- Netscape needs a hard-coded DIV to write dynamic DIVs --> 
      <SCRIPT language=javascript>
                        <!--
                                if (NS4) { document.write(writeDiv()) }
                        //-->
                        </SCRIPT>
                        <SCRIPT>
                        <!--
                                initMenu();
                        //-->
                        </SCRIPT>
      <!-- END MENU CODE --> </td>
    <td width="100%"> 
      <div align="left"> 
        <table border="0" cellpadding="0" width="100%" cellspacing="0">
          <tr> 
            <td width="100%"> 
              <div align="left"> 
                <table border="0" cellpadding="3" cellspacing="0" width="100%" height="27"  background="images/sectionbkgray.jpg">
                  <tr> 
                    <td valign="top"> 
                       <span class="main">&nbsp;Integrated Underwriting and Medical System </span>
                    </td>
                  </tr>
                </table>
              </div>
            </td>
          </tr>
          <tr> 
            <td width="100%" colspan=2>&nbsp;</td>
          </tr>
        </table>
        <table width="100%" cellpadding="3" cellspacing="5" border="0">
          <tr> 
            <td class="label2"><b>Setup MIB Export Schedule</b>              
            </td>
          </tr>
          <tr valign="top"> 
            <td> 
<!--- START OF BODY -->
<form name="schedule" method="post">
<table border="1" bordercolor="<%=iumColorB%>" cellpadding=10 cellspacing=0 bgcolor="" width="500" class="listtable1">
        <tr><td colspan="2" height="100%">
        <table>
        <tr>
        <td class="label2" width="120"><b>Export Schedule</b><td/>
        <td>
		<ium:list className="label2" listBoxName="schedule" type="<%=IUMConstants.LIST_SCHEDULE%>" styleName="width:200" selectedItem="<%= time%>" onChange="" filter=""/>
        </td>
        </tr>
        <tr>
        <td class="label2" valign="top"><b>Notes</b><td/>
        <td><textarea class="label2" name="description" rows="5" cols="53"><%= description%></textarea></td>
        </tr>
        <tr><td colspan="4" align="right">&nbsp;</td></tr>

        <tr><td colspan="4" align="left">
        <input type="button" value="Export" class="button1" onclick="exportSchedule()" <%=EXECUTE_ACCESS%>>&nbsp;
		<input type="button" value="Save" class="button1" onclick="saveSchedule();"<%=MAINTAIN_ACCESS%>>&nbsp;
        <input type="button" value="Cancel" class="button1" onclick="gotoPage('admin.html');">
		<!-- Added hidden to store if save and export button was clicked-->
		<input type="hidden" name="functionSelected" value="0">

        </td></tr>
        
        
        </table>
		</td></tr>

</table>
</form>
<!--- END OF BODY -->
</td>
</tr>

</table>
</div>
</body>
</html>