<%@ page language="java" import="java.util.*,com.slocpi.ium.data.*, com.slocpi.ium.ui.form.*, com.slocpi.ium.util.*, com.slocpi.ium.ui.util.*, java.text.* " buffer="12kb"%>
<%@ taglib uri="/struts-bean.tld" prefix="bean" %>

<!--
	var assignToForm = "";

	function saveMultipleRequest() {
		var form = document.requestFilterForm;
		var selected = countChecked();
		if (selected <=0){
			alert('<bean:message key="message.noARSelection"/>');
		} else {
			if (haveSelectedStatus()){
				form.action = "saveMultipleRequest.do";
				form.submit();
			}
		}
		return;
	}
	
	
	function filterResults() {
		var form = document.requestFilterForm;
		form.pageNo.value='1';
		form.action = "listAssessmentRequests.do";
		form.submit();
	}
	
	
	function validateSearchCriteria() {
		var form = document.forms[0];
		var noSearch = true;
		
		if(form.refNo.value != "")
		{
			noSearch = false;
		}
				
		if(form.branch.value != "")
		{
			noSearch = false;
		}
		
		if(form.lob.value != "")
		{
			noSearch = false;
		}
		
		if(form.requirementCode.value != "")
		{
			noSearch = false;
		}
		
		if(form.status.value != "")
		{
			noSearch = false;
		}
		
		if(form.location.value != "")
		{
			noSearch = false;
		}
		
		if(form.clientNumber.value != "")
		{
			noSearch = false;
		}
		
		if(form.assignedTo.value != "")
		{
			noSearch = false;
		}
		
		if(form.applicationReceivedDate.value != "")
		{
			noSearch = false;
		}
		
		if(form.agent.value != "")
		{
			noSearch = false;
		}
		
		if(form.coverageAmount.value != "")
		{
			noSearch = false;
		}
		
		if(form.nameOfInsured.value != "")
		{
			noSearch = false;
		}		
				
		if(noSearch == true)
		{
			var con = confirm('<bean:message key="message.nofilter" />');			
			if(!con)
			{
				return false;
			}
		}		
		
		form.clickedRefNo.value = "none";
		form.startSearch.value="<%=IUMConstants.YES%>"
		filterResults();
	}
	
	
	function saveMultipleReassign() {
		var form = document.requestFilterForm;
		var selected = countChecked();
		if (selected <=0){
			alert('<bean:message key="message.noARSelection"/>');
		} else {
			if (noSelection(form.reassignTo.value)==true){
				alert('Select one Underwriter from the Reassign To field.');
			}else{
				resetFilterFields();
				form.action = "saveMultipleReassign.do";
				form.submit();
			}
		}
		return;
	}	
	
	
	function countChecked() {
		var frm = document.requestFilterForm;
		var selected =0;
		if (frm.checkedRefNum == null){
			return selected;
		} else {
			var length = frm.checkedRefNum.length;
			if (length==null){
				if (frm.checkedRefNum.checked==true){
					selected++;
				} 
			}
			else {
				for (var i=0; i<length; i++){
					if (frm.checkedRefNum[i].checked==true){
						selected++;
					}
				}
			}
		}
		return selected;
	}
	
	
	function haveSelectedStatus() {
		var frm = document.requestFilterForm;
		var status = frm.changeStatusTo.value;
		if (status == null || status ==""){
			alert("<bean:message key="error.field.required" arg0="Request Status"/>");
			return false;
		}
		else {
			return true;
		}
	}
	

	function disableAssignTo() {
		var form = document.requestFilterForm;
		disable(form.changeAssignTo);
	}
	

	function enableAssignTo() {
		var form = document.requestFilterForm;
		enable(form.changeAssignTo);
	}


	function setAssignTo() {
		var form = document.requestFilterForm;
		var status = form.changeStatusTo.options[form.changeStatusTo.selectedIndex].value;
		if ((status == <%=IUMConstants.STATUS_FOR_APPROVAL%>) || 
			(status == <%=IUMConstants.STATUS_FOR_TRANSMITTAL_USD%>) ||
			(status == <%=IUMConstants.STATUS_AR_FOR_REFERRAL%>)) {
			form.isMaintain.value = true;
			gotoPage("requestFilterForm", "listAssessmentRequests.do");			
		} else if ((status == <%=IUMConstants.STATUS_NB_REVIEW_ACTION%>) ||
//				   (status == <%=IUMConstants.STATUS_FOR_FACILITATOR_ACTION%>) ||   // 0920 enabled dropdown list of users as part of CR
				   (status == <%=IUMConstants.STATUS_FOR_ASSESSMENT%>) ||
				   (status == <%=IUMConstants.STATUS_UNDERGOING_ASSESSMENT%>) ||
				   (status == <%=IUMConstants.STATUS_APPROVED%>) ||
				   (status == <%=IUMConstants.STATUS_DECLINED%>) ||
				   (status == <%=IUMConstants.STATUS_FOR_OFFER%>) ||
				   (status == <%=IUMConstants.STATUS_AWAITING_REQUIREMENTS%>) ||
				   (status == <%=IUMConstants.STATUS_AWAITING_MEDICAL%>) ||
				   (status == <%=IUMConstants.STATUS_NOT_PROCEEDED_WITH%>) ||
				   (status == <%=IUMConstants.STATUS_AR_CANCELLED%>)){
				   
				 		form.changeAssignTo.selectedIndex = 0;
						form.isMaintain.value = true;
						disableAssignTo();
		}
		else {
			form.isMaintain.value = true;
			gotoPage("requestFilterForm", "listAssessmentRequests.do");			
		}
	}


	function resetFilterFields(){
		var frm = document.requestFilterForm;
		emptyFilterFields();
		frm.refNo.value 	= frm.tmp_referenceNumber.value;
		if(frm.tmp_lob.value!='')
	       frm.lob.value = frm.tmp_lob.value;
	    if(frm.tmp_branch.value!='')
		   frm.branch.value 			= frm.tmp_branch.value;
		if(frm.tmp_status.value!='')	 
		   frm.status.value 			= frm.tmp_status.value;
		if(frm.tmp_agent.value!='')	 
		   frm.agent.value 			= frm.tmp_agent.value;
		if(frm.tmp_location.value!='')	 
		   frm.location.value 		= frm.tmp_location.value;
		frm.clientNumber.value 	= frm.tmp_clientNumber.value;
		if(frm.tmp_assignedTo.value!='')
		   frm.assignedTo.value 		= frm.tmp_assignedTo.value;
		if(frm.tmp_requirementCode.value!='')
		   frm.requirementCode.value 		= frm.tmp_requirementCode.value;
		frm.nameOfInsured.value 	= frm.tmp_nameOfInsured.value;
		frm.applicationReceivedDate.value = frm.tmp_applicationReceivedDate.value;
		frm.coverageAmount.value 	= frm.tmp_coverageAmount.value;
	}
	

	function updateLocation(){
		var form = document.requestFilterForm;
		var selected = countChecked();
		if (selected <=0) {
			alert('<bean:message key="message.noARSelection"/>');
		} else {
			form.action = "updateLocation.do";
			form.submit();
		}
		return;
	}	
	
	
	function emptyFilterFields() {
		var frm = document.requestFilterForm;
		frm.refNo.value 			= '';
		frm.lob.value 				= '';
		frm.branch.value 			= '';
		frm.status.value 			= '';
		frm.agent.value 			= '';
		frm.location.value 			= '';
		frm.clientNumber.value 		= '';
		frm.assignedTo.value 		= '';
		frm.nameOfInsured.value 	= '';
		frm.applicationReceivedDate.value = '';
		frm.coverageAmount.value 	= '';
		frm.requirementCode.value 	= '';
	}
	
	
	function filterByStatus(status) {
	   emptyFilterFields();
	   document.requestFilterForm.status.value = status;
		document.requestFilterForm.pageNo.value = "1";
		 document.forms[0].action = 'listAssessmentRequests.do';
		 document.forms[0].submit();		 
	}
	
	
	function sortBy(column,sortOrder) {
	   resetFilterFields();
	   document.requestFilterForm.sortOrder.value = sortOrder;
       document.requestFilterForm.columns.value = column;
  	   document.forms[0].action = 'listAssessmentRequests.do';
	   document.forms[0].submit();		 
	}
	
	
	function checkDate(obj) {
		if (obj.value.length>0){
  			if(!isValidDate(obj.value)){
  				alert('<bean:message key="error.field.format" arg0="Application Received Date" arg1="date" arg2="(ddMMMyyyy)"/>');
  				obj.value = '';
		   	}
		}
	}
	
	
	function filterByUnderwriter(underwriter,status) {
		var frm = parent.requestFilterForm;
		frm.refNo.value = '';
		frm.lob.value = '';
		frm.branch.value = '';
		frm.status.value = status;
		frm.agent.value = '';
		frm.location.value = '';
		frm.clientNumber.value 	= '';
		frm.assignedTo.value = underwriter;;
		frm.nameOfInsured.value = '';
		frm.applicationReceivedDate.value = '';
		frm.coverageAmount.value = '';
		frm.requirementCode.value 	= '';
  	    frm.action = 'listAssessmentRequests.do';
	    frm.submit();		 
		
	}
	
	
	function iframeReload() {
 		<% if(session != null) { %>
	    	document.uwload.location='underwriterLoad.do';
		<% } else { %>
	    	parent.location='listAssessmentRequests.do';
		<% } %>
	}	


	function resetButton(){
		var frm = document.requestFilterForm;
		emptyFilterFields();
		<%
			StateHandler sessionHandler = new StateHandler();
			UserData userData = sessionHandler.getUserData(request);
			UserProfileData userPref = userData.getProfile();
			String usrId = userPref.getUserId();
			String role = userPref.getRole();
					
			
			if (role.equals(IUMConstants.ROLES_UNDERWRITER_SUPERVISOR)) {
		%>
		    frm.status.value = '<%=IUMConstants.STATUS_FOR_ASSESSMENT%>';
		<% } else if (role.equals(IUMConstants.ROLES_NB_SUPERVISOR)) { %>
		    frm.status.value = '<%=IUMConstants.STATUS_NB_REVIEW_ACTION%>';
		<% } else { %>
		    frm.assignedTo.value = '<%=usrId%>';
		<% } %>
	}
	
	function mutilpleForwardToRecords() {
		var form = document.requestFilterForm;
		var selected = countChecked();
		if (selected <=0){
			alert('<bean:message key="message.noARSelection"/>');
		} else {
			form.action = "multipleForwardToRecords.do";
			form.submit();
		}
		return;
	}

//-->