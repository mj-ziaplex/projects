<%@ page language="java" import="com.slocpi.ium.util.*" buffer="12kb"%>
<%@ taglib uri="/struts-bean.tld" prefix="bean" %>
<!--
    function saveStatus(contextPath) {    
        var form   = document.forms[0];
        var cnt    = 0;
        var conf   = 0;
        var valid  = 0;
        var canc   = 0;
        var others = 0;
        var notSub = 0;
        if (form.index[0]) {
           for (var i=0;i<form.index.length;i++) { 
               if (form.index[i].checked) {
                   form.indexTemp[i].value = "checked";
				   if (form.statusTo.value == "<%= IUMConstants.STATUS_CONFIRMED %>") {
					  if (form.medicalStatus[i].value == "<%= IUMConstants.STATUS_CONFIRMED %>") {
					      conf++;
					  }
					  else {
					      others++;
					  }
			       }                   
				   if (form.statusTo.value == "<%= IUMConstants.STATUS_VALID %>") {
					  if (form.medicalStatus[i].value == "<%= IUMConstants.STATUS_VALID %>") {
					      valid++;
					  }
					  else {
					      others++;
					  }
			       }                   			       
				   if (form.statusTo.value == "<%= IUMConstants.STATUS_CANCELLED %>") {
					  if (form.medicalStatus[i].value == "<%= IUMConstants.STATUS_CANCELLED %>") {
					      canc++;
					  }
					  else {
					      others++;
					  }
			       }                   			       
				   if (form.statusTo.value == "<%= IUMConstants.STATUS_NOT_SUBMITTED %>") {
					  if (form.medicalStatus[i].value == "<%= IUMConstants.STATUS_NOT_SUBMITTED %>") {
					      notSub++;
					  }
					  else {
					      others++;
					  }
			       }                   			       
			       
                   cnt++;
               }
           }
        }
        else {
           if (form.index.checked) {
               form.indexTemp.value = "checked";               
			   if (form.statusTo.value == "<%= IUMConstants.STATUS_CONFIRMED %>") {
				  if (form.medicalStatus.value == "<%= IUMConstants.STATUS_CONFIRMED %>") {
				      conf++;
				  }
				  else {
				      others++;
				  }
		       }                                  
			   if (form.statusTo.value == "<%= IUMConstants.STATUS_VALID %>") {
				  if (form.medicalStatus.value == "<%= IUMConstants.STATUS_VALID %>") {
				      valid++;
				  }
				  else {
				      others++;
				  }
		       }                                  		       
			   if (form.statusTo.value == "<%= IUMConstants.STATUS_CANCELLED %>") {
				  if (form.medicalStatus.value == "<%= IUMConstants.STATUS_CANCELLED %>") {
				      canc++;
				  }
				  else {
				      others++;
				  }
		       }                                  		       		       
			   if (form.statusTo.value == "<%= IUMConstants.STATUS_NOT_SUBMITTED %>") {
				  if (form.medicalStatus.value == "<%= IUMConstants.STATUS_NOT_SUBMITTED %>") {
				      notSub++;
				  }
				  else {
				      others++;
				  }
		       }                                  		       		       		       
               cnt++;
           }
        }
	    if (cnt == 0) {
		   alert("<bean:message key="error.field.noselection" arg0="" arg1="to update"/>");
    	   return;
  	    }

		if (conf == 1 && others == 0) {
			alert("<bean:message key="error.sameStatusMedical" arg0="confirmed"/>");
			return;	
		}
		if (valid == 1 && others == 0) {
			alert("<bean:message key="error.sameStatusMedical" arg0="received"/>");
			return;	
		}
		if (canc == 1 && others == 0) {
			alert("<bean:message key="error.sameStatusMedical" arg0="cancelled"/>");
			return;	
		}
		if (notSub == 1 && others == 0) {
			alert("<bean:message key="error.invalidStatus"/>");
			return;	
		}

        var selInd = form.statusTo.options.selectedIndex;
        if (form.statusTo.options[selInd].value == "") {        
            alert("<bean:message key="error.field.requiredselection" arg0="status"/>");	 
            form.statusTo.focus();
            return;
        }
        if (form.statusTo.options[selInd].value == "<%=IUMConstants.STATUS_CONFIRMED %>") {
	        if (trim(form.appointmentDate.value) == "") {
  		       alert("<bean:message key="error.field.required" arg0="Appointment Date"/>");	 
  		       form.appointmentDate.focus();       
  		       return;
	        }
	        else {	        
	           if (!isValidDate(trim(form.appointmentDate.value))) {
    		       alert("<bean:message key="error.field.date" arg0="Appointment Date"/>");	        
    		       form.appointmentDate.focus();
    		       form.appointmentDate.select();
    		       return;
	           }
	           <%--else {
	               if (!isGreaterEqualToCurrentDate(trim(form.appointmentDate.value))) {
    		       	   alert("<bean:message key="error.field.laterthanequalto" arg0="Appointment Date" arg1="current date"/>");	        
	    		       form.appointmentDate.focus();
    			       form.appointmentDate.select();
    			       return;	               
	               }
	           }--%>
	        }       
	        if (trim(form.appointmentTime.value) == "") {
  		       alert("<bean:message key="error.field.required" arg0="Appointment Time"/>");	        	        
  		       form.appointmentTime.focus();
	           return;
	        }
	        else {
	           if (!isValidTime(trim(form.appointmentTime.value))) {
    		       alert("<bean:message key="error.field.time" arg0="Appointment Time"/>");	        
    		       form.appointmentTime.focus();
    		       form.appointmentTime.select();
    		       return;
	           }
	        }       	        
        }
        if (form.statusTo.options[selInd].value == "<%=IUMConstants.STATUS_VALID %>") {
	        if (trim(form.conductedDate.value) == "") {
  		       alert("<bean:message key="error.field.required" arg0="Conducted Date"/>");	        	        
  		       form.conductedDate.focus();
	           return;	        
	        }
	        else {
	           if (!isValidDate(trim(form.conductedDate.value))) {
    		       alert("<bean:message key="error.field.date" arg0="Conducted Date"/>");	        
    		       form.conductedDate.focus();
    		       form.conductedDate.select();
    		       return;
	           }
	           <%--else {
	               if (isGreaterThanCurrentDate(trim(form.conductedDate.value))) {
    		       	   alert("<bean:message key="error.field.earlierthanequalto" arg0="Conducted Date" arg1="current date"/>");	        
	    		       form.conductedDate.focus();
    			       form.conductedDate.select();
    			       return;	               
	               }	           
	           }--%>
	        }       	        
        }                
        var sortBy    = form.sortBy.value;
        var sortOrder = form.sortOrder.value;       
        form.action = contextPath + "/saveMedicalStatusBatch.do?sortBy=" + sortBy + "&sortOrder=" + sortOrder;
        form.submit();    
    
    }
    
    function viewMedicalRecord(url) {
        window.location = url;
    }
    
    function disableChangeStatusFields() {
        var form = document.forms[0];
        
        form.assignedClientNo.disabled = true;
        form.appointmentDate.disabled  = true;
        form.appointmentTime.disabled  = true;
        form.statusTo.disabled         = true;
        form.conductedDate.disabled    = true;
        form.saveBtn.disabled          = true;
        form.selAll.disabled           = true;    
    }    

    function disableMedicalStatusFields() {
        var form = document.forms[0];
        
        form.assignedClientNo.disabled = true;
        form.appointmentDate.disabled  = true;
        form.appointmentTime.disabled  = true;
        form.conductedDate.disabled    = true;

    }    
    
    function searchMedicalRecord(contextPath) {    
       var form = document.forms[0];
       
       if ((trim(form.referenceNo.value) == "") &&
           (trim(form.clientNo.value) == "") &&
           (trim(form.sevenDayMemoDate.value) == "") &&
           (trim(form.lastName.value) == "") &&
           (trim(form.firstName.value) == "") &&
           (trim(form.dateRequested.value) == "") &&
           (trim(form.status.value) == "") && 
           (trim(form.followUpNo.value) == "") &&
           (trim(form.followUpStartDate.value) == "") &&
           (trim(form.examiner.value) == "") &&
           (trim(form.lab.value) == "") &&
           (trim(form.followUpEndDate.value) == "") &&
           (!form.unassigned.checked)) {
	           if (!confirm("<bean:message key="message.nofilter"/>")) {
    	           return;
        	   }
       }
           
       if (form.unassigned.checked) {
           form.unassigned.value = "Y";
       }
       if (trim(form.sevenDayMemoDate.value) != "") {
	           if (!isValidDate(trim(form.sevenDayMemoDate.value))) {
    		       alert("<bean:message key="error.field.date" arg0="Seven Day Memo Date"/>");	        
    		       form.sevenDayMemoDate.focus();
    		       form.sevenDayMemoDate.select();
    		       return;
	           }
	   }
       if (trim(form.dateRequested.value) != "") {
	           if (!isValidDate(trim(form.dateRequested.value))) {
    		       alert("<bean:message key="error.field.date" arg0="Date Requested"/>");	        
    		       form.dateRequested.focus();
    		       form.dateRequested.select();
    		       return;
	           }
	   }
       if (trim(form.followUpStartDate.value) != "") {
	           if (!isValidDate(trim(form.followUpStartDate.value))) {
    		       alert("<bean:message key="error.field.date" arg0="Followup Start Date"/>");	        
    		       form.followUpStartDate.focus();
    		       form.followUpStartDate.select();
    		       return;
	           }
	   }
       if (trim(form.followUpEndDate.value) != "") {
	           if (!isValidDate(trim(form.followUpEndDate.value))) {
    		       alert("<bean:message key="error.field.date" arg0="Followup End Date"/>");	        
    		       form.followUpEndDate.focus();
    		       form.followUpEndDate.select();
    		       return;
	           }
	   }

       
       form.action = contextPath + "/listMedicalRecords.do";
       form.submit();
    }
    
    function resort(contextPath, sortBy, sortOrder) {
       var form = document.forms[0];
       if (sortOrder == "a") {
	       form.action = contextPath + "/listMedicalRecords.do?sortBy=" + sortBy + "&sortOrder=a";
	   } else {
	       form.action = contextPath + "/listMedicalRecords.do?sortBy=" + sortBy + "&sortOrder=d";	   
	   }
       form.submit();        
    }
    
    function enableRequiredFields() {
        var form   = document.forms[0];
        
        var selInd = form.statusTo.options.selectedIndex;
        if (form.statusTo.options[selInd].value == "") {        
	        //form.assignedClientNo.value    = "";
	        form.appointmentDate.value     = "";
	        form.appointmentTime.value     = "";
	        form.conductedDate.value       = "";                
	        //form.assignedClientNo.disabled = true;
	        form.appointmentDate.disabled  = true;
	        form.appointmentTime.disabled  = true;
	        form.conductedDate.disabled    = true;        
        }
        if (form.statusTo.options[selInd].value == "<%=IUMConstants.STATUS_CONFIRMED %>") {
	        //form.assignedClientNo.value    = "";
	        form.appointmentDate.value     = "";
	        form.appointmentTime.value     = "";
	        form.conductedDate.value       = "";                        
	        //form.assignedClientNo.disabled = true;                
	        form.conductedDate.disabled    = true;        
	        form.appointmentDate.disabled  = false;
	        form.appointmentTime.disabled  = false;	        
        }
        if (form.statusTo.options[selInd].value == "<%=IUMConstants.STATUS_VALID %>") {
	        //form.assignedClientNo.value    = "";
	        form.appointmentDate.value     = "";
	        form.appointmentTime.value     = "";
	        form.conductedDate.value       = "";                        
	        //form.assignedClientNo.disabled = true;        
	        form.appointmentDate.disabled  = true;
	        form.appointmentTime.disabled  = true;        
	        form.conductedDate.disabled    = false;	        
        }        
        if (form.statusTo.options[selInd].value == "<%=IUMConstants.STATUS_NOT_SUBMITTED %>") {
	        //form.assignedClientNo.value    = "";
	        form.appointmentDate.value     = "";
	        form.appointmentTime.value     = "";
	        form.conductedDate.value       = "";                        
	        //form.assignedClientNo.disabled = true;
	        form.appointmentDate.disabled  = true;
	        form.appointmentTime.disabled  = true;
	        form.conductedDate.disabled    = true;
	    }
        if (form.statusTo.options[selInd].value == "<%=IUMConstants.STATUS_CANCELLED %>") {
	        //form.assignedClientNo.value    = "";
	        form.appointmentDate.value     = "";
	        form.appointmentTime.value     = "";
	        form.conductedDate.value       = "";                        
	        //form.assignedClientNo.disabled = true;
	        form.appointmentDate.disabled  = true;
	        form.appointmentTime.disabled  = true;
	        form.conductedDate.disabled    = true;
	    }
	    
    }
    
	function rePaginate (page, actionUrl)
	{
		var form = document.forms[0];
		if(form.pageNo != null)
		{
			form.pageNo.value = page;	
		}
		submitForm(actionUrl);
	}
	
	function submitForm(actionUrl)
	{	
		document.forms[0].action = actionUrl;
		document.forms[0].submit();
	}    
	
	function enableClientNo(idx) {
		var form = document.forms[0];
		if (form.clientId[0]) {
		    if (form.clientId[idx].value == "") {
		      if (form.index[idx].checked) {
		        form.assignedClientNo.disabled = false;
		      }
		      else {
		        form.assignedClientNo.value    = "";
 		        form.assignedClientNo.disabled = true;
		      }
	    	}
		    else {
		        form.assignedClientNo.value    = "";		    
		        form.assignedClientNo.disabled = true;		    
		    }
	   }
	   else {
	       if (form.clientId.value == "") {
	         if (form.index.checked) {
	           form.assignedClientNo.disabled = false;
	         }
	         else {
		        form.assignedClientNo.value    = "";	         
	           form.assignedClientNo.disabled = true;
	         }
	       }
	       else {
 	           form.assignedClientNo.value    = "";	       
	           form.assignedClientNo.disabled = true;
	       }
	   }
	}
	
	function enableClientNoAll() {
	    var form = document.forms[0];
	    var cnt  = 0;
	    if (form.clientId[0]) {
	        if (form.selAll.checked) {
		        for (var i=0;i<form.indexTemp.length;i++) {
		            if (form.clientId[i].value == "") {
	    	            cnt++;
	        	    }
	        	}
	        	if (cnt > 0) {
	        	    form.assignedClientNo.disabled = false;
	        	}
	        	else {
 		            form.assignedClientNo.value    = "";	        	
	        	    form.assignedClientNo.disabled = true;
	        	}
	        }	 
	        else {
		        form.assignedClientNo.value    = "";	        
	            form.assignedClientNo.disabled = true;
	        }   
	    }
	    else {
	       if (form.clientId.value == "") {
	         if (form.selAll.checked) {
	           form.assignedClientNo.disabled = false;
	         }
	         else {
		       form.assignedClientNo.value    = "";	         
	           form.assignedClientNo.disabled = true;
	         }
	       }
	       else {
	           form.assignedClientNo.value    = "";	       
	           form.assignedClientNo.disabled = true;
	       }	    
	    }
	}
	
	function resetMedicalFields() {
	    var form = document.forms[0];
	    form.referenceNo.value       = "";
	    form.clientNo.value          = "";
	    form.lastName.value          = "";
	    form.firstName.value         = "";
	    form.sevenDayMemoDate.value  = "";
	    form.dateRequested.value     = "";
	    form.followUpNo.value        = "";
	    form.followUpStartDate.value = "";
	    form.followUpEndDate.value   = "";
	    if (form.unassigned.checked) {
 	        form.unassigned.click();
 	    }
	    form.examiner.options[0].selected = true;
	    form.lab.options[0].selected      = true; 	    
 	    form.status.options[0].selected   = true;
	}
	
	function checkMedicalStatus(){
		var form = document.forms[0];
    	if( form.chStatus.value != 'true' ){
    		for (var i=0;i<form.status.options.length;i++) {
        		if (form.status.options[i].value == '<%=IUMConstants.STATUS_REQUESTED%>') {
            	        form.status.options[i].selected = true;
                	}
	
            	}
 	  	}
  	}
  	
  	
  	function putValueChStatus(){
  		var form = document.forms[0];
  		
  		form.chStatus.value = 'true';
  	}
  	
  	function putCreateBtn(numRecordsPerView, numRecords){
  		if(numRecords >= 10 && numRecordsPerView >= 10){
  	        document.all["create_btn"].style.display = "block";
  	    }
  	    else{
  	       document.all["create_btn"].style.display = "none";
  	    }
  	}
	
//-->