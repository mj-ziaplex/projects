<%@ page language="java" import="java.util.*,com.slocpi.ium.data.*, com.slocpi.ium.ui.form.*, com.slocpi.ium.util.*, com.slocpi.ium.ui.util.*, java.text.* " buffer="12kb"%>
<%@ taglib uri="/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/struts-html.tld" prefix="html" %>
<%@ taglib uri="/ium.tld" prefix="ium" %>
<%RequirementForm requirementList = (RequirementForm)request.getAttribute("requirement"); %>

<%String contextPath = request.getContextPath();%>
<%
/* Andre Ceasar Dacanay - css enhancement - start */
String iumCss = "";
String iumColorB = "";
String companyCode = "";
try {
	ResourceBundle rbWMSSP = ResourceBundle.getBundle("dbconfig");
	iumCss = rbWMSSP.getString("IUM_CSS_VERSION");
	companyCode = iumCss;
	iumCss = "_"+iumCss;
} catch (Exception e){}
if(iumCss.equals("_CP")){
	iumColorB="#2A4C7C";
	companyCode="";
}else if(iumCss.equals("_GF")){
	iumColorB="#303A77";
	companyCode = companyCode+" ";
}else{
	iumColorB="#2A4C7C";
}
/* Andre Ceasar Dacanay - css enhancement - end */
%>
<%
    // This routine queries the access rights of the users (RCRIVERA)
    //
    // --- START --- 
    
    StateHandler sessionHandler = new StateHandler();
    Access access = new Access();
	HashMap userAccess = sessionHandler.getUserAccess(request);
    if(!access.hasAccess(userAccess,IUMConstants.PAGE_REQUIREMENTS,IUMConstants.ACC_INQUIRE) ){                                                                                                                    
    	response.sendRedirect("noAccessPage.jsp");   
    } 
    
    String CREATE_ACCESS = "";
    String MAINTAIN_ACCESS = "";
    String DELETE_ACCESS = "";
    String EXECUTE_ACCESS = "";  
    
        
    if(!access.hasAccess(userAccess,IUMConstants.PAGE_REQUIREMENTS,IUMConstants.ACC_CREATE) ){                                                                                                                     
	  CREATE_ACCESS = "DISABLED"; 	
    }
    if(!access.hasAccess(userAccess,IUMConstants.PAGE_REQUIREMENTS,IUMConstants.ACC_MAINTAIN) ){                                                                                                                     
	  MAINTAIN_ACCESS = "DISABLED";
    }
    if(!access.hasAccess(userAccess,IUMConstants.PAGE_REQUIREMENTS,IUMConstants.ACC_DELETE) ){                                                                                                                     
	  DELETE_ACCESS = "DISABLED";
    }
    if(!access.hasAccess(userAccess,IUMConstants.PAGE_REQUIREMENTS,IUMConstants.ACC_EXECUTE) ){                                                                                                                     
	  EXECUTE_ACCESS = "DISABLED"; 	
    }
    
    // --- END ---

 %>    
<html>

<head>
<title><%=companyCode%>Integrated Underwriting and Medical System</title>
<link href="<%=contextPath%>/css/pw_main.css"     rel="stylesheet" type="text/css">
<link href="<%=contextPath%>/css/main<%=iumCss%>.css" rel="stylesheet" type="text/css">
<link href="<%=contextPath%>/css/styleCommon.css" rel="stylesheet" type="text/css">
<script language="JavaScript" src="<%=contextPath%>/js/validation.js"></script>
<script language="JavaScript" src="<%=contextPath%>/js/dhtml_init.js"></script>
<script language="JavaScript" src="<%=contextPath%>/js/navigation.js"></script>
<script language="JavaScript" src="<%=contextPath%>/jsp/sl_left_nav.jsp"></script>
<script language="JavaScript">
function putButton(numRecordsPerView, numRecords){
  		if(numRecords >= 10 && numRecordsPerView >= 10){
  	        document.all["create_btn"].style.display = "block";
  	    }
  	    else{
  	       document.all["create_btn"].style.display = "none";
  	    }
}
function rePaginate( pageNum, target){
    var frm = document.frm;
		frm.pageNumber.value = pageNum;
		frm.action=target;
		frm.submit();
}

function trimTextArea(){
   var frm= document.frm;
	 if(frm.reqDesc.value.length>70){
	    frm.reqDesc.value=frm.reqDesc.value.substring(0,70);
	 }

}

function clickMaintain(){
	 var frm = document.frm;
	 var value = checkHasReqPick();
	 if(value!=''){
	    enableFields();
			frm.reqCode.value = value;
			frm.action='viewAdminRequirementCode.do';
			frm.submit();	
	 }else{
	    alert('<bean:message key="error.field.requiredselection" arg0="requirement"/>');
	 }
}

function clickSave(){
   var frm = document.frm; 
   if(validateFormEntry()==true){
				
	 <%if(requirementList==null||requirementList.getReqCode()==null||requirementList.getReqCode().equals("")||request.getParameter("mode")!=null){%>
	      frm.action="maintainAdminRequirementCode.do?mode=add";
				frm.submit();
	 <%}else{%>
	      frm.action="maintainAdminRequirementCode.do";
				frm.reqCode.disabled=false;
				frm.submit();
	 <%}%>
	 }
}

function validateFormEntry(){
   var result=true;
	 var frm = document.frm;
	 if(isEmpty(frm.reqCode.value)){
	    alert('<bean:message key="error.field.required" arg0="Code"/>');
			frm.reqCode.focus();
			return false;																
	 }
	 if(isEmpty(frm.reqDesc.value)){
	    alert('<bean:message key="error.field.required" arg0="Description"/>');
			frm.reqDesc.focus();
			return false;																
	 }
	 if(frm.reqLevel[0].checked==false && frm.reqLevel[1].checked==false){
	    alert('<bean:message key="error.field.singleselection" arg0="Level"/>');
			frm.reqLevel[0].focus();
			return false;																
	 }
	 if(isEmpty(frm.validity.value)){
	    alert('<bean:message key="error.field.required" arg0="Validity"/>');
			frm.validity.focus();
			return false;																
	 }else{
	 	  if(!isNumeric(frm.validity.value)){
	       alert('<bean:message key="error.field.numeric" arg0="Validity"/>');
			   frm.validity.focus();
			   return false;																
	 		}
	 }
	 
	 if(isEmpty(frm.followUpNo.value)){
	    alert('<bean:message key="error.field.required" arg0="Follow Up"/>');
			frm.followUpNo.focus();
			return false;																
	 }else{
	 	  if(!isNumeric(frm.followUpNo.value)){
	       alert('<bean:message key="error.field.numeric" arg0="Follow Up"/>');
			   frm.followUpNo.focus();
			   return false;																
	 		}
	 }

	 if(frm.withForm[0].checked==false && frm.withForm[1].checked==false){
	    alert('<bean:message key="error.field.singleselection" arg0="with Form"/>');
			frm.withForm[0].focus();
			return false;																
	 }
	 
	 if(frm.withForm[0].checked==true && noSelection(frm.formId.value)){
      alert('<bean:message key="error.field.required" arg0="Form"/>');
			frm.formId.focus();
			return false;
				 																	
	 }
	 
	 
	 
	 
	 
   
	 return result;
}

function checkHasReqPick(){
   var pick = document.frm.reqPick;
	 var result='';
	 if(pick.value==null){
		 for(i=0;i<pick.length;i++){
		   if(pick[i].checked==true){
			   result=pick[i].value;
			 }
		 }
	 }else{
	    if(pick.checked==true){
			   result=pick.value;											 			
			}
	 }
	 return result;
}

function disableFields(){
   var frm = document.frm;
	 frm.reqCode.disabled=true;
	 frm.reqDesc.disabled=true;
	 frm.reqLevel[0].disabled=true;
	 frm.reqLevel[1].disabled=true;
	 frm.validity.disabled=true;
	 frm.followUpNo.disabled=true;
	 frm.withForm[0].disabled=true;
	 frm.withForm[1].disabled=true;	
	 frm.formId.disabled=true;
	 frm.testDateIndicator.disabled=true;	 	
 	 document.all['save_cancel'].className = 'hide';
 	 document.all['create_cancel'].className = 'show';
 	 document.all['create_cancel2'].className = 'show'; 	 
}


function enableFields(){
   var frm = document.frm;
	 frm.reqCode.disabled=false;
	 frm.reqDesc.disabled=false;
	 frm.reqLevel[0].disabled=false;
	 frm.reqLevel[1].disabled=false;
	 frm.validity.disabled=false;
	 frm.followUpNo.disabled=false;
	 frm.withForm[0].disabled=false;
	 frm.withForm[1].disabled=false;
	 frm.testDateIndicator.disabled=false;	 		 	
	 document.all['save_cancel'].className = 'show'; 	 				 				 
 	 document.all['create_cancel'].className = 'hide';
 	 document.all['create_cancel2'].className = 'hide'; 	 
}

function clickCreate(){
	 enableFields();
	 var frm = document.frm;
	 emptyFields();
	 frm.reqLevel[0].checked=true;
	 frm.withForm[1].checked=true;
	 frm.reqCode.focus();
}

function emptyFields(){
	 var frm = document.frm;
	 frm.reqCode.value='';
	 frm.reqDesc.value='';
	 frm.reqLevel[0].checked=false;
	 frm.reqLevel[1].checked=false;
	 frm.validity.value='';
	 frm.followUpNo.value='';
	 frm.withForm[0].checked=false;
	 frm.withForm[1].checked=false;
	 frm.formId.options[0].selected=true;
	 frm.formId.disabled=true
}

function controlFormIdField(){
   var frm = document.frm;
	 if(frm.withForm[0].checked==true){
	    frm.formId.disabled=false;
	 }else{
	    frm.formId.disabled=true;
	 }				 
}

function onLoadPage(){
	 <%if(requirementList==null||requirementList.getReqCode()==null||requirementList.getReqCode().equals("")){%>
	 		  disableFields();																																																				     	 																																																						
	 <%}else{%>
	 			enableFields();
				controlFormIdField();
				<%if(request.getParameter("mode")==null){%>
				   document.frm.reqCode.disabled=true;								 
			    <%}%>				   
	 <%}%>
}

</script>
</head>

<BODY leftMargin=0 topmargin=0 marginwidth=0 marginheight=0 onLoad="onLoadPage(); putButton(10, <%=((Page)request.getAttribute("pageRecord")).getList().size()%>); ">
<form name="frm"  method="post">    
<table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%">
	<tr><td width="100%" colspan="2">
<!-- HEADER -->
  <jsp:include page="header.jsp" flush="true"/>
  		<table border="0" cellpadding="0" cellspacing="0" width="100%" height="100%">
  			<tr vAlign=top> 
   				<TD background="<%=contextPath%>/images/bg_yellow.gif" id=leftnav vAlign=top width=147> 
     				<IMG height=9 src="<%=contextPath%>/images/sp.gif" width=147><BR>
     				<IMG height=30 src="<%=contextPath%>/images/sp.gif" width=7> <BR>
			      	<IMG height=7 src="<%=contextPath%>/images/sp.gif" width=147><BR>
				      <DIV id=level0 style="POSITION: absolute; VISIBILITY: hidden; WIDTH: 100%; Z-INDEX: 500"> 
				       	<SCRIPT language=javascript>
								<!--
								document.write(writeMenu(''))
								if (IE4) { document.write(writeDiv()) };								
								//-->
						</SCRIPT>
				    </DIV>
      				<DIV id=NSFix style="POSITION: absolute; VISIBILITY: hidden; Z-INDEX: 999">
      				&nbsp;
      				</DIV>
      			<!-- Netscape needs a hard-coded DIV to write dynamic DIVs --> 
      					<SCRIPT language=javascript>
             		<!--
               	 	if (NS4) { document.write(writeDiv()) }
                 //-->
			             </SCRIPT>
            			 <SCRIPT>
              <!--
                   	initMenu();
           		//-->
			           	</SCRIPT>
      <!-- END MENU CODE --> 
 
			    </td>      											
			    <td width="100%"> 
			     	<div align="left"> 
	    				<table border="0" cellpadding="0" width="100%" cellspacing="0">
      					<tr> 
       						<td width="100%"> 
								<div align="left"> 
							        <table border="0" cellpadding="3" cellspacing="0" width="100%" height="27"  background="<%=contextPath%>/images/sectionbkgray.jpg">
							          <tr> 
							            <td valign="top"> 
							               <span class="main">&nbsp;Integrated Underwriting and Medical System </span>
							            </td>
							          </tr>
							        </table>
							   	</div>
						    </td>
						 </tr>
				         <tr> 
				         	<td width="100%" colspan=2>&nbsp;</td>
	      				</tr>
			 	     	</table>			

 <input type="hidden" name="pageNumber" value='<bean:write name="requirement" property="pageNumber"/>'>			 	     		 	       	
   	 				<table width="100%" cellpadding="3" cellspacing="5" border="0">
	      				<tr valign="top">
     						<td class="title2">Requirement Details
<!--Start of form -->
      						<table border="1" bordercolor="<%=iumColorB%>" cellpadding=5 cellspacing=0 bgcolor="" width="700" class="listtable1">
       							<tr>
       								<td colspan="2" height="100%">
					   				    <table border="0" width="690" cellpadding="2" cellspacing="5" class="listtable1">
									        <tr>
<html:errors/>									        
									          <td class="label2" width="110"><span class="required">*</span><b>Code</b></td>
									          <td><input type="text" name="reqCode" class="label2" maxlength="5" value='<bean:write name="requirement" property="reqCode"/>' onBlur="this.value=trim(this.value.toUpperCase());"></td>
									        </tr>
									        <tr valign="top">
									          <td class="label2"><span class="required">*</span><b>Description</b></td>
									          <td><textarea name="reqDesc" class="label2" rows="2" cols="40" onKeyUp="trimTextArea();" onBlur="this.value=trim(this.value.toUpperCase());"><bean:write name="requirement" property="reqDesc"/></textarea> </td>
									        </tr>

<%
				String policy="";
				String client=""; 
				if(requirementList.getReqLevel().equals(IUMConstants.LEVEL_POLICY)){
  			   policy="checked";																																	
				}
				if(requirementList.getReqLevel().equals(IUMConstants.LEVEL_CLIENT)){
  			   client="checked";																																	
				}
				%>				
									        <tr>
									          <td class="label2"><b>&nbsp;&nbsp;Level</b></td>
									          <td class="label2"><input type="radio" name="reqLevel" value="<%=IUMConstants.LEVEL_POLICY%>" <%=policy%>>Policy &nbsp;&nbsp; &nbsp;<input type="radio" name="reqLevel" value="<%=IUMConstants.LEVEL_CLIENT%>" <%=client%>> Client </td>
									        </tr>
									        <tr>
									          <td class="label2"><span class="required">*</span><b>Validity</b></td>
									          <td class="label2"><input type="text" name="validity" class="label2" maxlength="3" value='<bean:write name="requirement" property="validity"/>'>&nbsp;<b>Mos</b> </td>
									        </tr>
									        <tr>
									          <td class="label2"><span class="required">*</span><b>Follow-up</b></td>
									          <td class="label2"><input type="text" name="followUpNo" class="label2" maxlength="1" value='<bean:write name="requirement" property="followUpNo"/>'>&nbsp;<b>Days</b> </td>
									        </tr>
				<%
				String withFormY="";
				String withFormN=""; 
				String formDisabled="";
				if(requirementList.getFormId()==null||requirementList.getFormId().equals("")||requirementList.getFormId().equals("0")){
  			   withFormN="checked";
					 formDisabled="disabled";					 																																	
				}else{
				   withFormY="checked";
				}
				%>
									        <tr>
									          <td class="label2"><b>&nbsp;&nbsp;w/ Form</b></td>
									          <td class="label2">
												<input type="radio" name="withForm" value="Y" <%=withFormY%> onClick="controlFormIdField();">Yes &nbsp;&nbsp; &nbsp; <input type="radio" name="withForm" value="N" <%=withFormN%> onClick="controlFormIdField();">No
							        		  </td>
											</tr>
											<tr>
											  <td class="label2"><b>&nbsp;&nbsp;Form</b></td>
						    				  <td class="label2">
					<ium:list className="label2" listBoxName="formId" type="<%=IUMConstants.LIST_REQUIREMENT_FORMS%>" styleName="width:400"  selectedItem="<%=requirementList.getFormId()%>" disabled="<%=formDisabled%>"/>
											  </td>
									        </tr>
											<tr>
									        	<td class="label2"><b>&nbsp;&nbsp;Required Test Date</b></td>								        		
									        	<td class="label2"><input type="checkbox" name="testDateIndicator" value="1" <%=requirementList.getTestDateIndicator().equals("1")?"checked":""%>></td>
									        </tr>
	
									        <tr id="save_cancel" class="hide">
											  <td align="left" colspan="2">
										        <input type="button" value="Save" class="button1" onClick="clickSave();">&nbsp;
										        <input type="button" value="Cancel" class="button1" onClick="emptyFields();disableFields();">
									          </td>
											</tr>
									    </table>
									</td>
								</tr>
							</table>
						</td>
					 </tr>
				</table>
				<table>     
					<tr valign="top">
						<td></td> 
				    </tr> 
		<tr id="create_cancel" class="show">		    <td align="left" colspan="7">&nbsp;&nbsp;                          

    <input type="button" class="button1" name="create" value=" Create " onClick="clickCreate();" <%=CREATE_ACCESS%>> &nbsp;
    <input type="button" class="button1" name="maintain" value="Maintain" onClick="clickMaintain();" <%= MAINTAIN_ACCESS%>>
   
  </td>
	</tr>
			<tr>
						<td>&nbsp;</td>
            			<td>
<!--- START OF BODY -->
<table border="1" bordercolor="<%=iumColorB%>" cellpadding=10 cellspacing=0 bgcolor="" width="700" class="listtable1">
<tr><td>
<table border="0" cellpadding="2" cellspacing="2" class="listtable1" width="700">

        <tr class="headerrow1">
				<td width="15">&nbsp;</td>
        <td class="headerrow1" ><b>REQUIREMENT CODE</b></td>
        <td class="headerrow1" >DESCRIPTION</td>
        <td class="headerrow1" >LEVEL</td>        
        <td class="headerrow1" >VALIDITY</td>        
        <td class="headerrow1" >FOLLOW-UP</td>
		<td class="headerrow1" >W/ FORM</td>  
		<td class="headerrow1" >TEST DATE</td>         
        </tr>

				
		<% int i = 0; 
			String tr_class;
      String td_bgcolor;
     %>


 <logic:iterate id="rec" name="pageRecord" property="list">				

		<%
  		  String tdIndicator = ((RequirementForm)rec).getTestDateIndicator();
		  if (i%2 == 0) {
			 tr_class = "row1";
		     td_bgcolor = "#CECECE";
		   }
		  else {
		     tr_class = "row2";
		     td_bgcolor = "#EDEFF0";
		   }
			 
			 
			String selectedPick= "";	
		 %>
		<tr class="<%=tr_class%>" >
		<td><input type="radio" name="reqPick" value='<bean:write name="rec" property="reqCode"/>' <%=selectedPick%>></td>
		<td><bean:write name="rec" property="reqCode"/></td>
		<td><bean:write name="rec" property="reqDesc"/></td>
		<td><bean:write name="rec" property="reqLevel"/></td>
		<td><bean:write name="rec" property="validity"/></td>
		<td><bean:write name="rec" property="followUpNo"/></td>
		<td><bean:write name="rec" property="formId"/></td>
		<td align="center"><input type="checkbox" name="displayTDIndicator" disabled <%=tdIndicator.equals("1")?"checked":""%>></td>
		</tr>
		<% i++; %>
		
		</logic:iterate>
<%   

int pageNumber = Integer.parseInt(requirementList.getPageNumber());  
		 Page pageRecord = (Page)request.getAttribute("pageRecord");
%>

<%   if (pageRecord==null){ %>		
				 	<tr>
					 <td class="headerrow4" colspan="8" align="left" width="100%"><bean:message key="message.noexisting" arg0="Offices/Branches"/></td>
					</tr>
 <%}else{%>
                    <tr>
                      <td class="headerrow4" colspan="8" width="100%" height="10" class="label2" valign="bottom" >
                                                <!-- don't display link for previous page if the current page is the first page -->					
                                               <% if (pageNumber > 1) { %>
                      <a href="#" onclick="javascript:rePaginate(1,'viewAdminRequirementCode.do');"><img src="images/pagnavfirst_off.gif" name="pagnavfirst" border="0" width="18" height="13"/></a>&nbsp;					
                      <a href="#" onclick="javascript:rePaginate(<%=pageNumber-1%>,'viewAdminRequirementCode.do');"><img src="images/pagnavleft_off.gif" name="pagnavleft" border="0" width="18" height="13"/></a>
                                                <% } else {%>
                      <img src="images/pagnavfirst_on.gif" name="pagnavfirst" border="0" width="18" height="13"/>&nbsp;
                      <img src="images/pagnavleft_on.gif" name="pagnavleft" border="0" width="18" height="13"/>
                                                <% } %>
                                                <% 
                                                int currLink = 1;
                                                int prevLink = 1;
                                                %>
                           <logic:iterate id="navLinks" name="pageRecord" property="pageNumbers">
                                                  <% currLink = ((Integer)navLinks).intValue(); %>
                                                  <% if (((Integer)navLinks).intValue() == pageNumber) { %>
                                   <b><bean:write name="navLinks"/></b>
                                                  <% } else { %>		
                                                    <% if ((currLink > (prevLink+1))) { %>
                                                      ...
                                                    <% } %>
                      <a href="#" onclick="rePaginate('<bean:write name="navLinks"/>','viewAdminRequirementCode.do');" class="links2"><bean:write name="navLinks"/></a>
                                                  <% } %>
                                                  <% prevLink = currLink; %>
                                                </logic:iterate>
									
                                                <!-- don't display link for next page if the current page is the last page -->
                                                
                                                   <%  if(pageNumber < prevLink) {  %>
                      <a href="#" onClick="rePaginate('<%=pageNumber+1%>','viewAdminRequirementCode.do');" class=links1><img src="<%=contextPath%>/images/pagnavright_off.gif" name="pagnavright" border="0" width="18" height="13"/></a>&nbsp;
                      <a href="#" onClick="rePaginate('<%=prevLink%>','viewAdminRequirementCode.do');" class=links1><img src="<%=contextPath%>/images/pagnavlast_off.gif" width="18" height="13" border="0"></a>
                                                <%  } else {  %>
                      <img src="<%=contextPath%>/images/pagnavright_on.gif" width="18" height="13" border="0">&nbsp;
                      <img src="<%=contextPath%>/images/pagnavlast_on.gif" width="18" height="13" border="0">
                                                <%  }  %>
                      </td>
                    </tr>
                                            <% } %>

		
</table>
</td>
</tr>
</table>

</td>

</tr>
<tr id="create_cancel2" class="show"><td>&nbsp;</td>

<td class="label2" align="left" colspan="7">&nbsp;&nbsp;<div id="create_btn">                          
    <input type="button" class="button1" name="create" value=" Create " onClick="clickCreate();" <%=CREATE_ACCESS%>> &nbsp;
    <input type="button" class="button1" name="maintain" value="Maintain" onClick="clickMaintain();" <%= MAINTAIN_ACCESS%>>
    </div>
  </td>
</tr>

<!--- END OF BODY -->
</table>
</div>
</form>
</body>
</html>