<%@ page language="java" import="java.util.*,com.slocpi.ium.data.*, com.slocpi.ium.ui.form.*, com.slocpi.ium.util.*, com.slocpi.ium.ui.util.*, java.text.* " buffer="12kb"%>
<%@ taglib uri="/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/struts-html.tld" prefix="html" %>
<%@ taglib uri="/ium.tld" prefix="ium" %>
<%String contextPath = request.getContextPath();%>
<%
/* Andre Ceasar Dacanay - css enhancement - start */
String iumCss = "";
String iumColorB = "";
String companyCode = "";
try {
	ResourceBundle rbWMSSP = ResourceBundle.getBundle("dbconfig");
	iumCss = rbWMSSP.getString("IUM_CSS_VERSION");
	companyCode = iumCss;
	iumCss = "_"+iumCss;
} catch (Exception e){}
if(iumCss.equals("_CP")){
	iumColorB="#2A4C7C";
	companyCode="";
}else if(iumCss.equals("_GF")){
	iumColorB="#303A77";
	companyCode = companyCode+" ";
}else{
	iumColorB="#2A4C7C";
}
/* Andre Ceasar Dacanay - css enhancement - end */
%>
<jsp:useBean id="laboratoryForm" scope="request" class="com.slocpi.ium.ui.form.LaboratoryForm" />

<%
    // This routine queries the access rights of the users (RCRIVERA)
    //
    // --- START --- 
    
    StateHandler sessionHandler = new StateHandler();
    Access access = new Access();
	HashMap userAccess = sessionHandler.getUserAccess(request);
    if(!access.hasAccess(userAccess,IUMConstants.PAGE_LABORATORIES,IUMConstants.ACC_INQUIRE) ){                                                                                                                    
    	response.sendRedirect("noAccessPage.jsp");   
    } 
    
    String CREATE_ACCESS = "";
    String MAINTAIN_ACCESS = "";
    String DELETE_ACCESS = "";
    String EXECUTE_ACCESS = "";  
    
    if(!access.hasAccess(userAccess,IUMConstants.PAGE_LABORATORIES,IUMConstants.ACC_CREATE) ){                                                                                                                     
	  CREATE_ACCESS = "DISABLED"; 	
    }
    if(!access.hasAccess(userAccess,IUMConstants.PAGE_LABORATORIES,IUMConstants.ACC_MAINTAIN) ){                                                                                                                     
	  MAINTAIN_ACCESS = "DISABLED";
    }
    if(!access.hasAccess(userAccess,IUMConstants.PAGE_LABORATORIES,IUMConstants.ACC_DELETE) ){                                                                                                                     
	  DELETE_ACCESS = "DISABLED";
    }
    if(!access.hasAccess(userAccess,IUMConstants.PAGE_LABORATORIES,IUMConstants.ACC_EXECUTE) ){                                                                                                                     
	  EXECUTE_ACCESS = "DISABLED"; 	
    }
    
    // --- END ---
    UserData userData = sessionHandler.getUserData(request); 
	UserProfileData userPref = userData.getProfile();
 %>    

<html>
<head>
<title><%=companyCode%>Integrated Underwriting and Medical System</title>
<link href="<%=contextPath%>/css/pw_main.css"     rel="stylesheet" type="text/css">
<link href="<%=contextPath%>/css/main<%=iumCss %>.css" rel="stylesheet" type="text/css">
<link href="<%=contextPath%>/css/styleCommon.css" rel="stylesheet" type="text/css">
<script language="JavaScript" src="<%=contextPath%>/js/dhtml_init.js"></script>
    <script language="JavaScript" src="<%=contextPath%>/js/navigation.js"></script>
    <script language="JavaScript" src="<%=contextPath%>/jsp/sl_left_nav.jsp"></script>
    <script language="JavaScript" src="<%=contextPath%>/js/validation.js"></script>
    <script language="Javascript" src="<%=contextPath%>/jsp/validate_laboratory.jsp"></script>
</head>

<BODY leftMargin=0 topmargin=0 marginwidth=0 marginheight=0 onload="initializePage(event); putButton(<%=userPref.getRecordsPerView()%>, <%=((ArrayList)laboratoryForm.getLaboratoryList()).size()%>);">

<form name="frm" action="listLaboratory.do" method="post">      
<table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%">
	<tr>
		<td width="100%" colspan="2">
			<!-- HEADER -->
 			<jsp:include page="header.jsp" flush="true"/>
			<table border="0" cellpadding="0" cellspacing="0" width="100%" height="100%">
  			<tr vAlign=top> 
   				<TD background="<%=contextPath%>/images/bg_yellow.gif" id=leftnav vAlign=top width=147> 
     				<IMG height=9 src="<%=contextPath%>/images/sp.gif" width=147><BR>
     				<IMG height=30 src="<%=contextPath%>/images/sp.gif" width=7> <BR>
			      <IMG height=7 src="<%=contextPath%>/images/sp.gif" width=147><BR>
			      <DIV id=level0 style="POSITION: absolute; VISIBILITY: hidden; WIDTH: 100%; Z-INDEX: 500"> 
			       	<SCRIPT language=javascript>
								<!--
								document.write(writeMenu(''))
								if (IE4) { document.write(writeDiv()) };								
								//-->
							</SCRIPT>
				    </DIV>
      			<DIV id=NSFix style="POSITION: absolute; VISIBILITY: hidden; Z-INDEX: 999">
      				&nbsp;
      			</DIV>
      			<!-- Netscape needs a hard-coded DIV to write dynamic DIVs --> 
      			<SCRIPT language=javascript>
             		<!--
               	 	if (NS4) { document.write(writeDiv()) }
                 //-->
             </SCRIPT>
             <SCRIPT>
              <!--
                   	initMenu();
           		//-->
           	</SCRIPT>
  					<!-- END MENU CODE --> 
  				</td>      											
			    <td width="100%"> 
			     	<div align="left"> 
	    				<table border="0" cellpadding="0" width="100%" cellspacing="0">
      					<tr> 
       						<td width="100%"> 
							      <div align="left"> 
							        <table border="0" cellpadding="3" cellspacing="0" width="100%" height="27"  background="<%=contextPath%>/images/sectionbkgray.jpg">
							          <tr> 
							            <td valign="top"> 
							               <span class="main">&nbsp;Integrated Underwriting and Medical System </span>
							            </td>
							          </tr>
							        </table>
							      </div>
						       </td>
						     </tr>
				         <tr> 
				           <td width="100%" colspan=2>				           
				           	&nbsp;
				           </td>
      				</tr>
			 	    </table>				 	      
			 	    <%
			 	    	String disable = "disabled";
			 	    	if (laboratoryForm.getActionType().equalsIgnoreCase("update") || laboratoryForm.getActionType().equalsIgnoreCase("create"))
			 	    	{
			 	    		disable = "";
			 	    	}
			 	    %> 	
   	 				<table width="100%" cellpadding="3" cellspacing="5" border="0">
      				<tr valign="top">
     						<td class="title2">Laboratory Maintenance
         						<!--Start of form -->
      						<table border="1" bordercolor="<%=iumColorB%>" cellpadding=10 cellspacing=0 bgcolor="" width="700" class="listtable1">
       							<tr>
       								<td colspan="2" height="100%">
          							<table border="0" cellpadding="2" cellspacing="1" bgcolor=#FFFFFF style="border: 0px solid #D1D1D1;" class="listtable1">
           								<tr>
           									<td>
           											<html:errors/>        
							              		<!--IFRAME src="tempAdminDetail.html" width="665" height="100" scrolling="no" frameborder="0" >
							                	</IFRAME-->                  
										        		<table border="0" width="669" >
										        			<tr>
										        				<td class="label2" width="130"><span class="required">*</span><b>Laboratory ID</b></td>
													        	<td  width="549" colspan="4" class="label2">
													        	<%
																			if (!laboratoryForm.getActionType().equalsIgnoreCase("update"))
																			{
																		%>														        
														        		<input type="text" maxlength="5" size="10" class="label2" name="labId" onBlur="makeUpperCase('labId');" value="<%=laboratoryForm.getLabId()!=null?laboratoryForm.getLabId():""%>" <%=disable%>> 
														        <%
														        	} else {
														        %>		
													        		<input type="hidden" size="10" class="label2" name="labId" value="<%=laboratoryForm.getLabId()!=null?laboratoryForm.getLabId():""%>"> 
															        <%=laboratoryForm.getLabId()%>
														        <%
															        }
															      %> 
													        	</td>
													        </tr>																
													        <tr>
													        	<td class="label2" width="130"><span class="required">*</span><b>Name</b></td>
													        	<td  width="549" colspan="4"><input type="text" maxlength="40" size="40" class="label2" name="labName" onBlur="makeUpperCase('labName');" value="<%=laboratoryForm.getLabName()!=null?laboratoryForm.getLabName():""%>" <%=disable%>> </td>
													        </tr>
													        <script language="javascript">
													        <%
																		if (laboratoryForm.getActionType().equalsIgnoreCase("create"))
																		{
																	%>																						
														        		document.forms[0].labId.focus();
													        <%
													        	}
													        	else if (laboratoryForm.getActionType().equalsIgnoreCase("update"))
													        	{
													        %>
													        		 document.forms[0].labName.focus();
													        <%	
													        	}
													        %>
												        	</script>
													        <tr>
													        	<td class="label2" width="130"><span class="required">*</span><b>Contact Person</b></td>
													        	<td  width="549" colspan="4"><input type="text" maxlength="60" size="40" class="label2" name="contactPerson" value="<%=laboratoryForm.getContactPerson()!=null?laboratoryForm.getContactPerson():""%>" <%=disable%>> </td>
													        </tr>
													        <tr>
													        	<td class="label2" width="130"><b>&nbsp;&nbsp;Email Address</b></td>
													        	<td  width="549" colspan="4"><input type="text" class="label2" maxlength="40"  size="40" name="emailAddress" value="<%=laboratoryForm.getEmailAddress()!=null?laboratoryForm.getEmailAddress():""%>" <%=disable%>> </td>
													        </tr>													        
													         <tr>
													        	<td colspan="5" class="label2" width="130">&nbsp;</td>
													        </tr>			
													        <tr>
													        	<td colspan="5" class="label2" width="130"><b>&nbsp;&nbsp;Business Address</b></td>
													        </tr>													                
																	<tr>
														        <td class="label2" valign="top"><b>&nbsp;&nbsp;Address Line 1</b></td>
														        <td colspan="4"><input type="text" size="60" maxlength="60" class="label2" name="busAdd1" value="<%=laboratoryForm.getBusAdd1()!=null?laboratoryForm.getBusAdd1():""%>" <%=disable%>></td>
														      </tr>
																	<tr>
														        <td class="label2" valign="top"><b>&nbsp;&nbsp;Address Line 2</b></td>
														        <td colspan="4"><input type="text" size="60" class="label2" maxlength="60" name="busAdd2" value="<%=laboratoryForm.getBusAdd2()!=null?laboratoryForm.getBusAdd2():""%>" <%=disable%>></td>
														      </tr>
																	<tr>
														        <td class="label2" valign="top"><b>&nbsp;&nbsp;Address Line 3</b></td>
														        <td colspan="4"><input type="text" size="60" class="label2" name="busAdd3" maxlength="60"  value="<%=laboratoryForm.getBusAdd3()!=null?laboratoryForm.getBusAdd3():""%>" <%=disable%>></td>
														      </tr>				
														      <tr>
													        	<td class="label2" width="130"><b>&nbsp;&nbsp;City</b></td>
													        	<td width="190"><input type="text" maxlength="20" class="label2" size ="20" name="busCity"  value="<%=laboratoryForm.getBusCity()!=null?laboratoryForm.getBusCity():""%>" <%=disable%>> </td>
																		<td width="49">&nbsp;</td>
													        	<td class="label2" width="130"><b>Country</b></td>
													        	<td width="190"><input type="text" class="label2" maxlength="20" size="20" name="busCountry" value="<%=laboratoryForm.getBusCountry()!=null?laboratoryForm.getBusCountry():""%>" <%=disable%>> </td>
													        </tr>				
																	<tr>
														        <td class="label2" valign="top"><b>&nbsp;&nbsp;Zip Code</b></td>
														        <td colspan="4"><input type="text" size="10" maxlength="5" class="label2" name="busZipCode" value="<%=laboratoryForm.getBusZipCode()!=null?laboratoryForm.getBusZipCode():""%>" <%=disable%>></td>
														      </tr>																	        						      														      
													        <tr>
													        	<td colspan="5" class="label2" width="130"><b>&nbsp;&nbsp;Contact Numbers</b></td>
													        </tr>
													        <tr>
													        	<td class="label2" width="130"><b>&nbsp;&nbsp;Office Phone 1</b></td>
													        	<td width="190"><input type="text" class="label2" maxlength="13" size ="20" name="ofcPhone1"  value="<%=laboratoryForm.getOfcPhone1()!=null?laboratoryForm.getOfcPhone1():""%>" <%=disable%>> </td>
																		<td width="49">&nbsp;</td>
													        	<td class="label2" width="130"><b>Office Phone 2</b></td>
													        	<td width="190"><input type="text" class="label2" maxlength="13" size="20" name="ofcPhone2" value="<%=laboratoryForm.getOfcPhone2()!=null?laboratoryForm.getOfcPhone2():""%>" <%=disable%>> </td>
													        </tr>
													        <tr>
													        	<td class="label2" width="130"><b>&nbsp;&nbsp;Fax Number</b></td>
													        	<td width="549" colspan="4" ><input type="text" class="label2" maxlength="13" name="faxNumber" value="<%=laboratoryForm.getFaxNumber()!=null?laboratoryForm.getFaxNumber():""%>" <%=disable%>> </td>
													        </tr>
													         <tr>
													        	<td colspan="5" class="label2" width="130">&nbsp;</td>
													        </tr>																        
 																	<tr>
														        <td class="label2" valign="top"><b>&nbsp;&nbsp;Branch Name</b></td>
														        <td colspan="4"><input type="text" size="40" class="label2" maxlength="40" name="branchName" value="<%=laboratoryForm.getBranchName()!=null?laboratoryForm.getBranchName():""%>" <%=disable%>></td>
														      </tr>
													        <tr>
													        	<td colspan="5" class="label2" width="130"><b>&nbsp;&nbsp;Branch Address</b></td>
													        </tr>													                
																	<tr>
														        <td class="label2" valign="top"><b>&nbsp;&nbsp;Address Line 1</b></td>
														        <td colspan="4"><input type="text" size="60" maxlength="60" class="label2" name="branchAdd1" value="<%=laboratoryForm.getBranchAdd1()!=null?laboratoryForm.getBranchAdd1():""%>" <%=disable%>></td>
														      </tr>
																	<tr>
														        <td class="label2" valign="top"><b>&nbsp;&nbsp;Address Line 2</b></td>
														        <td colspan="4"><input type="text" size="60" maxlength="60" class="label2" name="branchAdd2" value="<%=laboratoryForm.getBranchAdd2()!=null?laboratoryForm.getBranchAdd2():""%>" <%=disable%>></td>
														      </tr>
																	<tr>
														        <td class="label2" valign="top"><b>&nbsp;&nbsp;Address Line 3</b></td>
														        <td colspan="4"><input type="text" size="60" maxlength="60" class="label2" name="branchAdd3" value="<%=laboratoryForm.getBranchAdd3()!=null?laboratoryForm.getBranchAdd3():""%>" <%=disable%>></td>
														      </tr>											
														      <tr>
													        	<td class="label2" width="130"><b>&nbsp;&nbsp;City</b></td>
													        	<td width="190"><input type="text" class="label2" maxlength="20" size ="20" name="branchCity"  value="<%=laboratoryForm.getBranchCity()!=null?laboratoryForm.getBranchCity():""%>" <%=disable%>> </td>
																		<td width="49">&nbsp;</td>
													        	<td class="label2" width="130"><b>Country</b></td>
													        	<td width="190"><input type="text" class="label2" maxlength="20" size="20" name="branchCountry" value="<%=laboratoryForm.getBranchCountry()!=null?laboratoryForm.getBranchCountry():""%>" <%=disable%>> </td>
													        </tr>				
																	<tr>
														        <td class="label2" valign="top"><b>&nbsp;&nbsp;Zip Code</b></td>
														        <td colspan="4"><input type="text" size="10" maxlength="5" class="label2" name="branchZipCode" value="<%=laboratoryForm.getBranchZipCode()!=null?laboratoryForm.getBranchZipCode():""%>" <%=disable%>></td>
														      </tr>																	        						      														      														      
														      <tr>
													        	<td colspan="5" class="label2"><b>&nbsp;&nbsp;Branch Contact Numbers</b></td>
													        </tr>
													        <tr>
													        	<td class="label2" width="130"><b>&nbsp;&nbsp;Branch Phone</b></td>
													        	<td width="190"><input type="text" class="label2" size ="20" maxlength="13" name="branchPhone"  value="<%=laboratoryForm.getBranchPhone()!=null?laboratoryForm.getBranchPhone():""%>" <%=disable%>> </td>
																		<td width="49">&nbsp;</td>
													        	<td class="label2" width="130"><b>Branch Fax Number</b></td>
													        	<td width="190"><input type="text" class="label2" size="20" maxlength="13" name="branchFaxNumber" value="<%=laboratoryForm.getBranchFaxNumber()!=null?laboratoryForm.getBranchFaxNumber():""%>" <%=disable%>> </td>
													        </tr>
													         <tr>
													        	<td colspan="5" class="label2" width="130">&nbsp;</td>
													        </tr>																        
														      <tr>       
														        <td class="label2" width="130"><b>&nbsp;&nbsp;Sun Life &nbsp;&nbsp;Accredited ?</b></td>
														        <td width="190" class="label2">
														        	<%if(!laboratoryForm.getAccredited().equals("0")) {%>
															        	<input type="radio" name="accredited" value="1" CHECKED <%=disable%>>Yes&nbsp;<input type="radio" name="accredited" value="0" <%=disable%>>No&nbsp; </td>
															        <%} else {%>	
																        <input type="radio" name="accredited" value="1" <%=disable%>>Yes&nbsp;<input type="radio" name="accredited" value="0" CHECKED <%=disable%>>No&nbsp; </td>
																      <%}%>  
																		<td width="49">&nbsp;</td>
														        <td class="label2" width="130"><b>Accreditation Date</b></td>
													        	<td width="190"><input type="text" class="label2" name="accreditedDate" value="<%=laboratoryForm.getAccreditedDate()!=null?laboratoryForm.getAccreditedDate():""%>" <%=disable%> onblur="formatDate()"> &nbsp;
													        	<div id="doc_cal" style="display:none;position:absolute;"><a href="#" onClick="popUpWin('<%=contextPath%>/jsp/calendar.jsp?textbox=frm.accreditedDate',290,155);"><img src="<%=contextPath%>/images/calendar.gif" border="0"></a></div>
													        	</td>
													        </tr>        											        															        
													        <tr>
													        	<td colspan="5" class="label2" width="130">&nbsp;</td>
													        </tr>		
													       	<%
																		if (laboratoryForm.getActionType().equalsIgnoreCase("update"))
																		{
																	%>														        
													        <!-- Start of LIST TESTS -->
													        <tr>
															    	<td class="label2" valign="top" width="100"><b>Lab Test/s Offered</b></td>
															      <td width="100">
															      	<ium:list className="label2" listBoxName="testCode" type="<%=IUMConstants.LIST_TEST_PROFILES%>" />     
															     	</td>
															      <td colspan="3" valign="top">
															      	<input type="button" name="testAdd" value="Add" class="button1" onclick="validateAddTest('updateLaboratory.do', 'add_test');"> 
															      </td>
															    </tr>
															 		<tr>
														  	    <td class="label2" valign="top" width="100"><b>&nbsp;</b></td>
														    	  <td width="100">
																			<!-- START OF LAB TEST -->					
																			<div style="position:relative; height:200; background-color:#888B9E; layer-background-color:#FFFFFF; overflow-y: auto;">							        
														        	<table border="1" bordercolor="<%=iumColorB%>" cellpadding=1 cellspacing=0 bgcolor="" width="200" class="listtable1">
														        		<tr>
															        		<td height="100%">
															        			<table border="0" cellpadding="2" cellspacing="1" bgcolor=#FFFFFF style="border: 0px solid #D1D1D1;" class="listtable1">
															        				<tr class="headerrow1">
															        					<td width="10">&nbsp;</td>
															        					<td width="70" class="Label2">Lab/Tests</td>
																			        	<td width="200" class="Label2">Fee</td>      
																			        </tr>
																			        <%
																			        	int testLen = laboratoryForm.getLabTest().length;
																			        	int cnt1 = 0;
																			        	boolean hasRecord = false;
																			        	for(int cnt=0; cnt<testLen; cnt++)
																			        	{
																		        	%>																		        	
																				        	<input type="hidden" name="labTest" value="<%=laboratoryForm.getLabTest()[cnt]%>">
																			       			<input type="hidden" name="labTestStatus" value="<%=laboratoryForm.getLabTestStatus()[cnt]%>">																				       																					       			
																		        	<%
																		        			if(laboratoryForm.getLabTestStatus()[cnt].equalsIgnoreCase("active"))
																		        			{																		        
																		        				hasRecord = true;			
																				        		if((cnt1%2) == 0)
																				        		{
																			        %>
																							        <tr class="row1">
																			        <%
																				        		} else {																		        		
																			        %>
		    	    																        <tr class="row2">
																		    	    <%
																			      	  		}
																			      	  		cnt1++;
																		        	%>
																				        	<td width="10">
																				        		<input type="checkbox" name="delTest" value="<%=laboratoryForm.getLabTest()[cnt]%>">																			        		
																				        	</td>
																					        <td width="70" class="Label2">
																					        	<ium:description type="<%=IUMConstants.LIST_TEST_PROFILES %>" queryCode="<%=laboratoryForm.getLabTest()[cnt]%>"/>
																									</td>
																        					<td width="200" class="Label2"><input type="text" size="12" class="label2" name="labFee" value="<%=laboratoryForm.getLabFee()[cnt]%>" onblur="getKeyAmount(event, this)"></td>   
																        					<script language="javascript">
																        						var form = document.forms[0];
																        						var len = form.testCode.length;
															  	      						for(i=0; i<len; i++)
															    	    						{
															      	  							if(form.testCode[i].value == "<%=laboratoryForm.getLabTest()[cnt]%>" )
															        								{
															        									form.testCode[i] = null;
															        									break;
															        								}
															        							}
															        						</script>   
																			        <%		
																			        		}
																			        	}
																			        	
																			        	if(!hasRecord)
																			        	{
																			        %>														   
																			        		<tr>
																			        			<td colspan="3"  class="Label2">
																			        				<bean:message key="message.noexisting" arg0="laboratory Tests" />
																			        			</td>
																			        		</tr>
																			        <%
																			        	}
																			        %>     				        
																		      	</table>
																		      </td>
																		   	</tr>
																		  </table>
																		  </div>
															        <!-- END OF LAB TEST -->																			   	
														        </td>
														        <td colspan="3" valign="top">
														        	<input type="button" name="testRemove" value="Remove" class="button1" onclick="inactivateTest('updateLaboratory.do', 'delete_test');"> 
														        	<input type="hidden" name="remLab">
													  	      </td>
														  	 	</tr>                         
														  	 	<%}%>
													     	</table>
													  	</td>
													 	</tr>   
										        <tr>
										        	<td colspan="5" align="left">
										        		<% if (laboratoryForm.getActionType().equalsIgnoreCase("create")) { %>
											        		<input type="button" value="Save" class="button1" onclick="validateForm('createLaboratory.do', 'save_create');">&nbsp;
											        		<input type="button" value="Cancel" class="button1" onclick="redirectForm('listLaboratory.do', 'cancel');">
											        	<%} else if( laboratoryForm.getActionType().equalsIgnoreCase("update")) {%>
											        		<input type="button" value="Save" class="button1" onclick="validateForm('updateLaboratory.do', 'save_update');">&nbsp;
											        		<input type="button" value="Cancel" class="button1" onclick="redirectForm('listLaboratory.do', 'cancel');">
										        		<%
										        					}
										        		%>
										        	</td>
										      	</tr>
											    </table>
												</td>
											</tr>
						       	</table>               
						       	<!-- END OF FORM -->
				         	</td>
				       	</tr>
				<% if (!laboratoryForm.getActionType().equalsIgnoreCase("create") && !laboratoryForm.getActionType().equalsIgnoreCase("update")) { %>																			
												
														<td align="left"><div>
												     	<input type="button" value="Create" class="button1" onclick="redirectForm('createLaboratory.do', 'create');" <%= CREATE_ACCESS%>>&nbsp;
									       			<input type="button" value="Maintain" class="button1" onclick="validateMaintain('updateLaboratory.do', 'update');" <%= MAINTAIN_ACCESS%>>            
									       		</div></td>
												
							       			<%}%>       	
			         	<tr valign="top"> 
			         	
				    	    <td colspan="2"> 	
				    	    			       			
										<!--- START OF List -->
										<table border="1" bordercolor="<%=iumColorB%>" cellpadding=10 cellspacing=0 bgcolor="" width="700" class="listtable1">
							      	<tr>
							        	<td colspan="2" height="100%">
							        		<table border="0" cellpadding="2" cellspacing="1" bgcolor=#FFFFFF style="border: 1px solid #D1D1D1;" class="listtable1">
							        			<tr class="headerrow1">
							        			<%
																if (!laboratoryForm.getActionType().equalsIgnoreCase("update") && !laboratoryForm.getActionType().equalsIgnoreCase("create"))
																{
														%>			
								        			<td width="10">&nbsp;</td>
														<%}%>								        			
							        				<td width="100">LABORATORY ID</td>
							        				<td width="250">NAME</td>
							        				<td width="190">CONTACT PERSON</td>        
							        				<td width="200">OFFICE NUMBER</td>        
							        			</tr>
							        			<%
							        				int len = laboratoryForm.getLaboratoryList().size();
							        				for(int i=0; i<len; i++)
							        				{
							        					LaboratoryData labData = (LaboratoryData)laboratoryForm.getLaboratoryList().get(i);
							        					if((i%2) == 0)
							        					{
							        			%>
									        				<tr class="row1">									        				
							        			<%	
							        					} else {
							        			%>
										        			<tr class="row2">
							        			<%	
							        					}
							        			%>
										        			<%
																			if (!laboratoryForm.getActionType().equalsIgnoreCase("update") && !laboratoryForm.getActionType().equalsIgnoreCase("create"))
																			{
																		%>			
							        							<td>
							        								<input type="radio" name="updateLabId" value="<%=labData.getLabId()%>">
							        							</td>
							        							<%}%>
							        							<td>
							        								<%=labData.getLabId()%>
							        							</td>
							        							<td>
							        								<%=labData.getLabName()!=null?labData.getLabName():""%>
							        							</td>
							        							<td>
							        								<%=labData.getContactPerson()!=null?labData.getContactPerson():""%>
							        							</td>
							        							<td>
							        								<%=labData.getOfficeNum()!=null?labData.getOfficeNum():labData.getOfficeNum2()!=null?labData.getOfficeNum2():""%>
							        							</td>							        														        														        							
							        						</tr>
							        			<%		
							        				}
							        			%>							        			
														<!-- START Number of pages -->		
													  <tr>
													  	<td class="headerrow4" colspan="10" align="left" width="100%" >
													  	  <table border="0" cellpadding="0" cellspacing="0"  width="100%" >
													    	  <tr>
																		<td class="headerrow4" width="100%" height="10" class="label2" valign="bottom" >
																			<bean:size id="noOfPages" name="page" property="pageNumbers" />
																			 <%
                                          int pageNumber = Integer.parseInt((String)request.getAttribute("pageNo"));                               
                                          String viewPage = "listLaboratory.do";
                                          int currLink = 1;
                                          int prevLink = 1;
                                          int firstPage = 1;
                                          int lastPage = noOfPages.intValue();
                                          %>
                                          
     																			<input type="hidden" name="pageNo" value = "1">

                                          <!-- don't display link for previous page if the current page is the first page -->
                                          <% if (pageNumber > firstPage) { %>
                                            <a href="#" onClick="javascript:rePaginate(1,'<%=viewPage%>');"><img src="<%=contextPath%>/images/pagnavfirst_off.gif" name="pagnavfirst" border="0" width="18" height="13"/></a>&nbsp;					
                                            <a href="#" onClick="javascript:rePaginate('<%=pageNumber-1%>','<%=viewPage%>');"><img src="<%=contextPath%>/images/pagnavleft_off.gif" name="pagnavleft" border="0" width="18" height="13"/></a>
                                          <% } else {%>
                                            <img src="<%=contextPath%>/images/pagnavfirst_on.gif" name="pagnavfirst" border="0" width="18" height="13"/>&nbsp;
                                            <img src="<%=contextPath%>/images/pagnavleft_on.gif" name="pagnavleft" border="0" width="18" height="13"/>
                                          <% } %>

                                          <logic:iterate id="navLinks" name="page" property="pageNumbers">                                                            
                                            <% currLink = ((Integer)navLinks).intValue();%>
                                            <% if (((Integer)navLinks).intValue() == pageNumber) { %>
                                              <b><bean:write name="navLinks"/></b>
                                            <% } else { %>
                                              <% if ((currLink > (prevLink+1))) { %>
                                              ...
                                              <% } %>
                                              <a href="#" onClick="rePaginate('<bean:write name="navLinks"/>','<%=viewPage%>');" class="links2"><bean:write name="navLinks"/></a>
                                            <% } %>
                                            <%prevLink = currLink;%>
                                          </logic:iterate>

                                          <!-- don't display link for next page if the current page is the last page -->
                                          <% if(pageNumber < lastPage) { %>
                                            <a href="#" onClick="rePaginate('<%=pageNumber+1%>','<%=viewPage%>');"><img src="<%=contextPath%>/images/pagnavright_off.gif" name="pagnavright" border="0" width="18" height="13"/></a>&nbsp;
                                            <a href="#" onClick="rePaginate('<%=prevLink%>','<%=viewPage%>');"><img src="<%=contextPath%>/images/pagnavlast_off.gif" width="18" height="13" border="0"></a>
                                          <% } else { %>
                                            <img src="<%=contextPath%>/images/pagnavright_on.gif" width="18" height="13" border="0">&nbsp;
                                            <img src="<%=contextPath%>/images/pagnavlast_on.gif" width="18" height="13" border="0">
                                          <% } %>
																		</td>
													      	</tr>
													     	</table>
													  	</td>
													 	</tr>   	
														<!-- END Number of pages -->		   
													</table>				
													
													<input type="hidden" name="actionType">
												</td>
											</tr>
										</table>
										<% if (!laboratoryForm.getActionType().equalsIgnoreCase("create") && !laboratoryForm.getActionType().equalsIgnoreCase("update")) { %>																			
													<table width="700">
														<tr><td align="left"><div id="create_btn">
												     	<input type="button" value="Create" class="button1" onclick="redirectForm('createLaboratory.do', 'create');" <%= CREATE_ACCESS%>>&nbsp;
									       			<input type="button" value="Maintain" class="button1" onclick="validateMaintain('updateLaboratory.do', 'update');" <%= MAINTAIN_ACCESS%>>            
									       			</div>
									       		</td></tr>
									       	</table>		
							       			<%}%>
										<!--- END OF List -->
									</td>
								</tr>
							</table>			
							
						</div>
					</td>
				</tr>
			</table>	
		</td>
	</tr>
</table>				
</form>
</body>
</html>