<%@ page language="java" buffer="1024kb"%>
<!-- JAVA UTILS -->
<%@ page import="java.util.ResourceBundle" %>

<%

String contextPath = request.getContextPath();%>

<%
//WMS session expiration = close browser
ResourceBundle rb1 = ResourceBundle.getBundle("wmsconfig");
String wmsContextPath1 = rb1.getString("wms_context_path");

if(contextPath.equalsIgnoreCase(wmsContextPath1)) { %>
<!-- ************ -->
<!-- HEADER START -->
<!-- ************ -->
	<table border="0" cellspacing="0" cellpadding="0" bgcolor="#00334C" width="800">
		<tr>
			<td rowspan="2"><img src="<%=contextPath%>/images/header2.GIF"></td>
			<td align="right" colspan="12"><img src="<%=contextPath%>/images/head_clouds.gif"></td>
		</tr>
		<tr>
			<td><img name="menu_home" src="<%=contextPath%>/images/menu_home_off.gif" border="0"></td>
			<td><img src="<%=contextPath%>/images/menu_sep.gif" border="0"></td>
			<td><img name="menu_request" src="<%=contextPath%>/images/menu_request_off.gif" border="0"></td>
			<td><img src="<%=contextPath%>/images/menu_sep.gif" border="0"></td>
			<td><img name="menu_med_records" src="<%=contextPath%>/images/menu_med_records_off.gif" border="0"></td>
			<td><img src="<%=contextPath%>/images/menu_sep.gif" border="0"></td>
			<td><img name="menu_reports" src="<%=contextPath%>/images/menu_reports_off.gif" border="0"></td>
			<td><img src="<%=contextPath%>/images/menu_sep.gif" border="0"></td>
			<td><img name="menu_admin" src="<%=contextPath%>/images/menu_admin_off.gif" border="0"></td>
			<td><img src="<%=contextPath%>/images/menu_sep.gif" border="0"></td>
			<td>
				<a href="<%=contextPath%>/login.do?task=logout"
					onmouseover="imageOver('<%=contextPath%>','menu_logout')"
					onmouseout="imageOut('<%=contextPath%>','menu_logout')">
					<img name="menu_logout" src="<%=contextPath%>/images/menu_logout_off.gif" border="0">
				</a>
			</td>
		</tr>
	</table>
<%} else {%>

	<table border="0" cellspacing="0" cellpadding="0" bgcolor="#00334C" width="100%">
		<tr>
			<td rowspan="2"><img src="<%=contextPath%>/images/header.gif"></td>
			<td rowspan="2" width="86">&nbsp;</td>
			<td align="right" colspan="12"><img src="<%=contextPath%>/images/head_clouds.gif"></td>
			<td rowspan="2" width="20">&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td width="48">
				<a href="<%=contextPath%>/home.do"
					onmouseover="imageOver('<%=contextPath%>','menu_home')"
					onmouseout="imageOut('<%=contextPath%>','menu_home')">
					<img name="menu_home" src="<%=contextPath%>/images/menu_home_off.gif" border="0">
				</a>
			</td>
			<td width="18"><img src="<%=contextPath%>/images/menu_sep.gif" border="0"></td>
			<td width="73">
				<a href="<%=contextPath%>/listAssessmentRequests.do"
					onmouseover="imageOver('<%=contextPath%>','menu_request')"
					onmouseout="imageOut('<%=contextPath%>','menu_request')">
					<img name="menu_request" src="<%=contextPath%>/images/menu_request_off.gif" border="0">
				</a>
			</td>
			<td width="18"><img src="<%=contextPath%>/images/menu_sep.gif" border="0"></td>
			<td width="150">
				<a href="<%=contextPath%>/listMedicalRecords.do"
					onmouseover="imageOver('<%=contextPath%>','menu_med_records')"
					onmouseout="imageOut('<%=contextPath%>','menu_med_records')">
					<img name="menu_med_records" src="<%=contextPath%>/images/menu_med_records_off.gif" border="0">
				</a>
			</td>
			<td width="18"><img src="<%=contextPath%>/images/menu_sep.gif" border="0"></td>
			<td width="71">
				<a href="<%=contextPath%>/viewReports.do"
					onmouseover="imageOver('<%=contextPath%>','menu_reports')"
					onmouseout="imageOut('<%=contextPath%>','menu_reports')">
					<img name="menu_reports" src="<%=contextPath%>/images/menu_reports_off.gif" border="0">
				</a></td>
			<td width="18"><img src="<%=contextPath%>/images/menu_sep.gif" border="0"></td>
			<td width="131">
				<a href="<%=contextPath%>/admin.do"
					onmouseover="imageOver('<%=contextPath%>','menu_admin')"
					onmouseout="imageOut('<%=contextPath%>','menu_admin')">
					<img name="menu_admin" src="<%=contextPath%>/images/menu_admin_off.gif" border="0">
				</a>
			</td>
			<td width="18"><img src="<%=contextPath%>/images/menu_sep.gif" border="0"></td>
			<td width="63">
				<a href="<%=contextPath%>/login.do?task=logout"
					onmouseover="imageOver('<%=contextPath%>','menu_logout')"
					onmouseout="imageOut('<%=contextPath%>','menu_logout')">
					<img name="menu_logout" src="<%=contextPath%>/images/menu_logout_off.gif" border="0">
				</a>
			</td>
		</tr>
	</table>
<%}%>
<!-- ********** -->
<!-- HEADER END -->
<!-- ********** -->
