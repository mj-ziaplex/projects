<%@ page language="java" import="java.util.*, com.slocpi.ium.ui.form.*, com.slocpi.ium.util.*, com.slocpi.ium.ui.util.*, com.slocpi.ium.data.*, com.slocpi.ium.underwriter.*" buffer="1024kb"%>
<%@ taglib uri="/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/struts-html.tld" prefix="html" %>
<%@ taglib uri="/ium.tld" prefix="ium" %>
<%String contextPath = request.getContextPath();%>

<%
    // This routine queries the access rights of the users (RCRIVERA)
    //
    // --- START ---

    StateHandler sessionHandler = new StateHandler();
    Access access = new Access();

	HashMap userAccess = sessionHandler.getUserAccess(request);
    if(!access.hasAccess(userAccess,IUMConstants.PAGE_ASSESSMENT_DETAILS,IUMConstants.ACC_INQUIRE) ){
    	response.sendRedirect("noAccessPage.jsp");
    }

    String CREATE_ACCESS = "";
    String MAINTAIN_ACCESS = "";
    String DELETE_ACCESS = "";
    String EXECUTE_ACCESS = "";
    String CREATE_REQUEST = "";
    String CREATE_UW = "";
    String MAINTAIN_UW = "";
    String DELETE_UW = "";
    String EXECUTE_UW = "";

    boolean INQUIRE_REQUEST = true;
    boolean INQUIRE_CDS = true;
    boolean INQUIRE_KOREQRMT = true;
    boolean INQUIRE_UWASSESS= true;
    boolean INQUIRE_DOCTORNOTES = true;

    // CREATE ASSESSMENT REQUEST
    if (!access.hasAccess(userAccess,IUMConstants.PAGE_ASSESSMENT_DETAILS,IUMConstants.ACC_CREATE) ){
	  CREATE_ACCESS = "DISABLED";
    }
	// MAINTAIN ASSESSMENT REQUEST
    if (!access.hasAccess(userAccess,IUMConstants.PAGE_ASSESSMENT_DETAILS,IUMConstants.ACC_MAINTAIN) ){
	  MAINTAIN_ACCESS = "DISABLED";
    }
	// -- NOT APPLICABLE --
    if (!access.hasAccess(userAccess,IUMConstants.PAGE_ASSESSMENT_DETAILS,IUMConstants.ACC_DELETE) ){
	  DELETE_ACCESS = "DISABLED";
    }
	// UPDATE LOCATION
    if (!access.hasAccess(userAccess,IUMConstants.PAGE_ASSESSMENT_DETAILS,IUMConstants.ACC_EXECUTE) ){
	  EXECUTE_ACCESS = "DISABLED";
    }

	// CREATE IMPAIRMENT
    if(!access.hasAccess(userAccess,IUMConstants.PAGE_UW_ASSESSMENT,IUMConstants.ACC_CREATE) ){
	  CREATE_UW = "DISABLED";
    }
	// MAINTAIN IMPAIRMENT
    if(!access.hasAccess(userAccess,IUMConstants.PAGE_UW_ASSESSMENT,IUMConstants.ACC_MAINTAIN) ){
	  MAINTAIN_UW = "DISABLED";
    }
	// DELETE IMPAIRMENT
    if(!access.hasAccess(userAccess,IUMConstants.PAGE_UW_ASSESSMENT,IUMConstants.ACC_DELETE) ){
	  DELETE_UW = "DISABLED";
    }
	// -- NOT APPLICABLE --
    if(!access.hasAccess(userAccess,IUMConstants.PAGE_UW_ASSESSMENT,IUMConstants.ACC_EXECUTE) ){
	  EXECUTE_UW = "DISABLED";
    }

    if(!access.hasAccess(userAccess,IUMConstants.PAGE_REQUEST_DETAILS,IUMConstants.ACC_INQUIRE) ){
	  INQUIRE_REQUEST = false;
    }
    if(!access.hasAccess(userAccess,IUMConstants.PAGE_CDS,IUMConstants.ACC_INQUIRE) ){
	  INQUIRE_CDS = false;
    }
    if(!access.hasAccess(userAccess,IUMConstants.PAGE_REQUIREMENTS_KO,IUMConstants.ACC_INQUIRE) ){
	  INQUIRE_KOREQRMT = false;
    }
    if(!access.hasAccess(userAccess,IUMConstants.PAGE_UW_ASSESSMENT,IUMConstants.ACC_INQUIRE) ){
	  INQUIRE_UWASSESS = false;
    }
    if(!access.hasAccess(userAccess,IUMConstants.PAGE_DOCTORS_NOTES,IUMConstants.ACC_INQUIRE) ){
	  INQUIRE_DOCTORNOTES = false;
    }

    // --- END ---
 %>
<html>
  <head>
    <title>Sun Life Financial - Philippines</title>
    <link href="<%=contextPath%>/css/main.css" rel="stylesheet" type="text/css">
    <script language="Javascript" src="<%=contextPath%>/js/navigation.js"></script>
    <script language="Javascript" src="<%=contextPath%>/js/validation.js"></script>
    <script language="Javascript" src="<%=contextPath%>/jsp/validate_req_RequestDetail.jsp"></script>
    <script language="Javascript" src="<%=contextPath%>/jsp/validate_req_UWAssessmentDetail.jsp"></script>
  </head>

  <%
  CDSDetailForm actionForm = (CDSDetailForm) request.getAttribute("detailForm");
  String lob = actionForm.getLob();
  String transmitInd = actionForm.getTransmitIndicator();
  String autoSettleInd = actionForm.getAutoSettleIndicator();
  String clientToSearch = actionForm.getClientToSearch();

  //current status
  String currentStatus = request.getParameter("currentStatus");
  if (currentStatus == null) {
    currentStatus = actionForm.getRequestStatus();
  }
  String isSuccessChangeStatus = request.getParameter("isSuccessChangeStatus");
  if ((isSuccessChangeStatus != null) && (isSuccessChangeStatus.equals("true"))) {
    currentStatus = actionForm.getRequestStatus();
  }

  //selected status
  String selectedStatus = request.getParameter("requestStatus");
  if (selectedStatus == null) {
    selectedStatus = actionForm.getRequestStatus();
  }

   //current assigned to
  String currentAssignedTo = actionForm.getAssignedTo();
  if (currentAssignedTo == null || (currentAssignedTo != null && currentAssignedTo.equals(""))) {
    currentAssignedTo = request.getParameter("currentAssignedTo");
  }

  ImpairmentForm impForm = (ImpairmentForm)request.getAttribute("maintainThisImpairment");
  String role = (String) request.getAttribute("role");

  String impCode = "";
  String impRelationship = "";
  String impOrigin = "";
  String impDate = "";
  String impHeight = "";
  String impWeight = "";
  String impBP = "";
  String impConfirmationCode = "";
  String impairmentSelected = "";

  int isRefresh = 0;
  if (impForm != null) {
    isRefresh = 1;
    impCode = impForm.getCode();
    impRelationship = impForm.getRelationship();
    impOrigin = impForm.getImpairmentOrigin();
    impDate = impForm.getImpairmentDate();

    if (!impForm.getHeightInFeet().equals("")){
    	impHeight = impForm.getHeightInFeet() + "'" + impForm.getHeightInInches() +  "&quot";
	}

    impWeight = impForm.getWeight();
    impBP = impForm.getBloodPressure();
    impConfirmationCode = impForm.getConfirm();
    impairmentSelected = impForm.getImpairmentId();
  }

  UserData ud = sessionHandler.getUserData(request);
  UserProfileData profile = ud.getProfile();
  String userId = profile.getUserId();
  Roles userRoles = new Roles(); 

  boolean hasAccess = false;
 
  if ((userRoles.getRoles(ud.getRoles(),IUMConstants.ROLES_UNDERWRITER)) ||
      (userRoles.getRoles(ud.getRoles(),IUMConstants.ROLES_UNDERWRITER_SUPERVISOR))) {
    hasAccess = true;
  }
  else {
    CREATE_UW = "DISABLED";
    MAINTAIN_UW = "DISABLED";
    DELETE_UW = "DISABLED";
  }

  //disable buttons (except create) if current status is an end status
  AssessmentRequest ar = new AssessmentRequest();
  String FORWARD_EXECUTE_ACCESS = EXECUTE_ACCESS;
  if (ar.isEndStatus(lob, Long.parseLong(currentStatus))) {
  	MAINTAIN_ACCESS = "DISABLED";
    EXECUTE_ACCESS = "DISABLED";
    CREATE_UW = "DISABLED";
    MAINTAIN_UW = "DISABLED";
    DELETE_UW = "DISABLED";
  }

  boolean isMaintain = false;
  String reqIsMaintain = request.getParameter("isMaintain");
  
  if ((reqIsMaintain != null) && (reqIsMaintain.equals("true"))) {
    	isMaintain = true;
  }
	
	
  //filter for request status
  String lob_status = lob + "," + currentStatus;
  
  
  //WMS resize
  ResourceBundle rb = ResourceBundle.getBundle("wmsconfig");
  String wmsContextPath = rb.getString("wms_context_path");
  
  String TABLE_WIDTH = "";
  String TEXTAREA_COL = "";
  String GRAY_WIDTH = "";
  String LABEL_CLASS = "";
  String SUB_TABLE_WIDTH = "";
  String DIV_WIDTH = "";
  if(contextPath.equalsIgnoreCase(wmsContextPath)){
  	TABLE_WIDTH = "700";
  	TEXTAREA_COL = "60";
  	GRAY_WIDTH = "780";
  	LABEL_CLASS = "label7";
  	SUB_TABLE_WIDTH ="680";
	DIV_WIDTH = "width: 680px;height: 325px;overflow: auto;";
  }else{
  	TABLE_WIDTH = "100%";
  	TEXTAREA_COL = "80";
  	GRAY_WIDTH = "100%";
  	LABEL_CLASS = "label2";
  	SUB_TABLE_WIDTH ="70%";
	DIV_WIDTH = "width: 800px;height: 325px;overflow: auto;";
  }
  
  %>
  <body leftmargin="0" topmargin="0" onLoad="<% if (isMaintain) { %> enableRequestDetails(); enableTabDetails('uw'); setDateForwarded();initializeClientData('<%=clientToSearch%>'); <% } else { %> disableRequestDetails(); disableTabDetails('uw'); <% } %> 
  <% 
  if(!contextPath.equalsIgnoreCase(wmsContextPath)){
  	if (Long.parseLong(currentStatus) == IUMConstants.STATUS_UNDERGOING_ASSESSMENT) { %>hideSaveCancel(); showCreateMaintain(); disableImpairmentFields(); clearImpairmentFields(); 
 <% }else{ %> hideSaveCancel(); hideCreateMaintain();
  <% }}else{%>enableTabDetails('uw');hideSaveCancel(); showCreateMaintain(); disableImpairmentFields(); clearImpairmentFields();<%} %>" onUnload="closePopUpWin();">
    <form name="requestForm" method="post">
      <table border="0" cellpadding="0" cellspacing="0" height="100%" width="<%=TABLE_WIDTH%>">
        <tr>
          <td width="<%=TABLE_WIDTH%>" colspan="2">
            <jsp:include page="header.jsp" flush="true"/>
          </td>
        </tr>
        <tr>
          <td class="label2" rowspan="2" bgcolor="#FFCB00" height="100%" width="20"><img src="<%=contextPath%>/images/spacer.gif" width="20"></td>
          <td class="label2" background="<%=contextPath%>/images/GradientGray.jpg" height="27" width="<%=GRAY_WIDTH%>" valign="top">&nbsp;&nbsp;
            <span class="main">Integrated Underwriting and Medical System</span>
          </td>
        </tr>
        <tr>
          <td width="<%=TABLE_WIDTH%>" height="100%">
            <html:errors/>
            <!-- Status List START -->
            <table border="0" cellpadding="5" cellspacing="0">
              <tr>
              	 
                <td class="<%=LABEL_CLASS%>" align="left"><ium:list className="label2" listBoxName="requestStatus" type="<%=IUMConstants.LIST_REQUEST_STATUS%>" styleName="width:200" selectedItem="<%=selectedStatus%>" onChange="setAssignTo();" filter="<%=lob_status%>"/></td>
                <td class="<%=LABEL_CLASS%>" align="left" id="maintain_button"><input class="button1" type="button" name="btnMaintain" value="Maintain" onClick="maintainRequest('uw');" <%=MAINTAIN_ACCESS%>></td>
                <td class="<%=LABEL_CLASS%>" align="left" id="create_button"><input class="button1" type="button" name="btnCreate" value="Create" onClick="window.location = 'createAssessmentRequest.do';" <%=CREATE_ACCESS%>></td>
                <td class="<%=LABEL_CLASS%>" align="left" id="save_button"><input class="button1" type="button" name="btnSave" value="Save" onClick="saveRequest();" <%=MAINTAIN_ACCESS%>></td>
                <td class="<%=LABEL_CLASS%>" align="left" id="cancel_button"><input class="button1" type="button" name="btnCancel" value="Cancel" onClick="cancelRequest('uw');" <%=MAINTAIN_ACCESS%>></td>
                <td class="<%=LABEL_CLASS%>" align="left"><input class="button1" type="button" name="btnUpdateLocation" value="Update Location" onClick="gotoPage('requestForm', 'updateLocation.do');" <%=EXECUTE_ACCESS%>></td>
                <input type="hidden" name="updateType" value="<%=IUMConstants.PROCESS_TYPE_SINGLE%>">
                <% if ((actionForm.getSourceSystem() != null) && (transmitInd != null && transmitInd.equals(IUMConstants.INDICATOR_FAILED))) {%>
                <td class="label2" align="left"><input class="button1" type="button" name="btnRetrieveCDS" value="Retrieve CDS" onClick="retrieveCDS();" <%=MAINTAIN_ACCESS%>></td>
                <% }%>
                <%-- if (autoSettleInd != null && autoSettleInd.equals(IUMConstants.INDICATOR_FAILED)) {--%>
                <% if (autoSettleInd != null) {
                	  String autoSettleButton = "";
                      if (autoSettleInd.equals(IUMConstants.INDICATOR_SUCCESS)) {
							autoSettleButton = "DISABLED";
                      }
                %>
                <td class="label2" align="left"><input class="button1" type="button" name="btnAutoSettle" value="Auto-Settle" onClick="autoSettle();" <%=autoSettleButton%>></td>
                <% }%>
				<% if ((Long.parseLong(currentStatus) == IUMConstants.STATUS_APPROVED) ||
                       (Long.parseLong(currentStatus) == IUMConstants.STATUS_AR_CANCELLED) ||
                       (Long.parseLong(currentStatus) == IUMConstants.STATUS_DECLINED) ||
					   (Long.parseLong(currentStatus) == IUMConstants.STATUS_FOR_OFFER) ||
                       (Long.parseLong(currentStatus) == IUMConstants.STATUS_NOT_PROCEEDED_WITH)) { %>
                <td class="label2" align="left"><input class="button1" type="button" name="btnForward" value="Forward to Records" onClick="forwardToRecords();"  <%=FORWARD_EXECUTE_ACCESS%>></td>
				<%}%>
              </tr>
            <% if (autoSettleInd != null) {
            	  String autoSettleMsg = "";
                  if (autoSettleInd.equals(IUMConstants.INDICATOR_SUCCESS)) {
					autoSettleMsg = "Auto-settle Successful";
                  } else if (autoSettleInd.equals(IUMConstants.INDICATOR_FAILED)){
                	autoSettleMsg = "Auto-settle Not Successful:";
                  }
            %>
            <tr><td class="error"><%=autoSettleMsg%>&nbsp;<bean:write name="detailForm" property="autoSettleRemarks"/></td></tr>
			<% } %>

            </table>
            <!-- Status List END -->

            <!-- hidden field to store current status -->
            <input type="hidden" name="currentStatus" value="<%=currentStatus%>">

            <!-- hidden field to store remarks -->
            <input type="hidden" name="remarks" value="<bean:write name="detailForm" property="remarks"/>">

            <!-- hidden field to store maintain flag for request details -->
            <input type="hidden" name="isMaintain" value="<%=isMaintain%>">

            <!-- hidden field to store success flag for change status in request details -->
            <input type="hidden" name="isSuccessChangeStatus" value="false">

            <!-- hidden field to store the currently assigned to user -->
            <input type="hidden" name="currentAssignedTo" value="<%=currentAssignedTo%>">

            <!-- hidden field to store the id to be edited and flag if maintenance is clicked -->
            <input type="hidden" name="impairmentToBeEdited" value="<%=impairmentSelected%>">
            <input type="hidden" name="selectedMaintain" value="0">

            <!-- hidden field to check if the page is for refresh -->
            <input type="hidden" name="refreshFlag" value="<%=isRefresh%>">

            <!-- hidden field to store the tab/page of the request detail -->
            <input type="hidden" name="reqPage" value="uwAssessmentDetail">

            <table border="0" cellpadding="10" cellspacing="0" height="100%" width="<%=TABLE_WIDTH%>">
              <tr>
                <td>
                  <%
                  String _sourceSystem = actionForm.getSourceSystem();
                  if (_sourceSystem == null) {
                    _sourceSystem = "";
                  }

                  String _agentCode = actionForm.getAgentCode();
                  if (_agentCode == null) {
                    _agentCode = "";
                  }

                  String _lob = actionForm.getLob();
                  if (_lob == null) {
                    _lob = "";
                  }

                  String _branchCode = actionForm.getBranchCode();
                  if (_branchCode == null) {
                    _branchCode = "";
                  }

                  String _assignedTo = currentAssignedTo; //actionForm.getAssignedTo();
                  if (_assignedTo == null) {
                    _assignedTo = "";
                  }

                  String _location = actionForm.getLocation();
                  if (_location == null) {
                    _location = "";
                  }

                  String _underwriter = actionForm.getUnderwriter();
                  if (_underwriter == null) {
                    _underwriter = "";
                  }

                  String _insuredClientNo = actionForm.getInsuredClientNo();
                  if (_insuredClientNo == null) {
                    _insuredClientNo = "";
                  }

                  String _insuredSex = actionForm.getInsuredSex();
                  if (_insuredSex == null) {
                    _insuredSex = "";
                  }

                  String _ownerClientNo = actionForm.getOwnerClientNo();
                  if (_ownerClientNo == null) {
                    _ownerClientNo = "";
                  }

                  String _ownerSex = actionForm.getOwnerSex();
                  if (_ownerSex == null) {
                    _ownerSex = "";
                  }

                  String _status = currentStatus; //actionForm.getRequestStatus();
                  if (_status == null) {
                    _status = "";
                  }

                  String _selectedStatus = selectedStatus;
                  if (_selectedStatus == null) {
                    _selectedStatus = "";
                  }
                  %>
                  <jsp:include page="requestDetail_include.jsp" flush="true">
                    <jsp:param name="sourceSystem" value = "<%=_sourceSystem%>"/>
                    <jsp:param name="agentCode" value = "<%=_agentCode%>"/>
                    <jsp:param name="lob" value = "<%=_lob%>"/>
                    <jsp:param name="branchCode" value = "<%=_branchCode%>"/>
                    <jsp:param name="assignedTo" value = "<%=_assignedTo%>"/>
                    <jsp:param name="location" value = "<%=_location%>"/>
                    <jsp:param name="underwriter" value = "<%=_underwriter%>"/>
                    <jsp:param name="insuredClientNo" value = "<%=_insuredClientNo%>"/>
                    <jsp:param name="insuredSex" value = "<%=_insuredSex%>"/>
                    <jsp:param name="ownerClientNo" value = "<%=_ownerClientNo%>"/>
                    <jsp:param name="ownerSex" value = "<%=_ownerSex%>"/>
                    <jsp:param name="status" value = "<%=_status%>"/>
                    <jsp:param name="selectedStatus" value = "<%=_selectedStatus%>"/>
                  </jsp:include>
                </td>
              </tr>
              <tr>
                <td width="<%=TABLE_WIDTH%>" height="100%" valign="top">
                  <table cellpadding="1" cellspacing="0">
                    <tr>
                      <td bgcolor="#2A4C7C">
                        <table border="0" cellpadding="3" cellspacing="0">
                          <tr><td class="tabSelected"><b><b>CDS Details-KO & Med/Lab, UW Assessment and Requirements</b></td></tr>
                        </table>
                      </td>
                      <td></td>
                      <% if (INQUIRE_DOCTORNOTES) {%>
                      <td bgcolor="#BBBBBB" style="cursor:hand" onmouseover="this.style.backgroundColor = '#2A4C7C'" onmouseout="this.style.backgroundColor = '#BBBBBB'">
                        <table border="0" cellpadding="3" cellspacing="0">
                          <tr><a href="javascript:gotoPage('requestForm', 'viewDoctorsNotes.do');"><td class="tabLink"><b>Doctor's Notes</b></td></a></tr>
                        </table>
                      </td>
                      <td></td>
                      <% }%>
                    </tr>
                 </table>
                  <!-- START OF CONTENT -->
                  <table cellpadding="0" cellspacing="0" border="1" bordercolor="#2A4C7C" width="<%=TABLE_WIDTH%>">
                    <tr>
                      <td class="<%=LABEL_CLASS%>" bgcolor="#2A4C7C" colspan="11">&nbsp;</td>
                    </tr>
                    <tr>
                      <td>
                        <table cellpadding="10" cellspacing="0" border="0" width="<%=TABLE_WIDTH%>">
                          <tr>
                            <td>
                              <table border="0" cellpadding="1" cellspacing="0" width="<%=TABLE_WIDTH%>">
                                <tr><td class="<%=LABEL_CLASS%>" colspan="2">&nbsp;</tr>
                                <tr>
                                	<td class="<%=LABEL_CLASS%>">Analysis</td>
                                	<td class="<%=LABEL_CLASS%>">Remarks</td>
                                </tr>
                                <tr>
                                	<td class="<%=LABEL_CLASS%>"><textarea rows="10" cols="<%=TEXTAREA_COL%>" class="<%=LABEL_CLASS%>" name="UWAnalysis"></textarea></td>
                                	<td class="<%=LABEL_CLASS%>"><textarea rows="10" cols="<%=TEXTAREA_COL%>" class="<%=LABEL_CLASS%>" name="UWRemarks"><bean:write name="detailForm" property="UWRemarks"/></textarea></td>
                                </tr>
                                <%  
                                if(contextPath.equalsIgnoreCase(wmsContextPath)){%>
                                	<tr><td class="<%=LABEL_CLASS%>" align="left"><input class="button1" type="button" name="btnSave" value="Save" onClick="saveUWAssessment();"></td></tr>
                                <%}%>
                                
                                <tr><td class="<%=LABEL_CLASS%>" colspan="2">&nbsp;</td></tr>
                    
								<tr>
									<td colspan="2">
										<table border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" style="border: 1px solid #D1D1D1;" class="listtable1" ID="Table1" width="<%=TABLE_WIDTH%>">
										<logic:iterate id="analysis" name="detailForm" property="listOfAnalysis">
										<%String analysisDesc = ((UWAnalysisForm)analysis).getAnalysis();%>
										<tr>
											<td class="headerrow1" width="55%"><div align="left">Posted By: <bean:write name="analysis" property="postedBy"/></div></td>
                    	                    <td class="headerrow1"><div align="right">Posted On: <bean:write name="analysis" property="postedDate"/></div></td>
                        	            </tr>
                            	        <tr>
                                	          <td class="row1" colspan="2"><%=analysisDesc%></td>
                                    	</tr>
										</logic:iterate>
										</table>
									</td>
								</tr>
								<tr><td colspan="2">&nbsp;</td></tr>
                                <tr><td colspan="2">&nbsp;</td></tr>
                                <tr>
								   <td class="<%=LABEL_CLASS%>" colspan="2">
                                    <table border="0" bordercolor="#2A4C7C" cellpadding="0" cellspacing="0" bgcolor="" width="<%=TABLE_WIDTH%>">
                                      <tr><td class="<%=LABEL_CLASS%>"><b>Impairments</b></td></tr>
                                      <tr>
                                        <td class="<%=LABEL_CLASS%>" height="100%">
                                          <%
                                          String reqClientType = request.getParameter("clientType");
                                          String clientType = "";

                                          if (reqClientType == null) {
                                            clientType = IUMConstants.CLIENT_TYPE_INSURED;
                                          }
                                          else {
                                            clientType = reqClientType;
                                          }

                                          String clientTypeChecked_O = "";
                                          String clientTypeChecked_I = "";

                                          if (clientType.equals(IUMConstants.CLIENT_TYPE_OWNER)) {
                                            clientTypeChecked_O = "checked";
                                          }
                                          else if (clientType.equals(IUMConstants.CLIENT_TYPE_INSURED)) {
                                            clientTypeChecked_I = "checked";
                                          }
                                          %>
                                          <a name="impairmentList"></a>
                                          <table border="1" bordercolor="#2A4C7C" cellpadding="10" cellspacing="0" bgcolor="" width="<%=TABLE_WIDTH%>">
                                            <tr>
                                              <td class="label2" height="100%">
                                                <% if (_lob.equals(IUMConstants.LOB_INDIVIDUAL_LIFE)) { %>
                                                <b>Client Type </b>&nbsp;
                                                <input type="radio" name="impClientType" value="<%=IUMConstants.CLIENT_TYPE_INSURED%>" <%=clientTypeChecked_I%> onClick="resetImpairment('<%=clientType%>', this.value)">Insured&nbsp;
                                                <input type="radio" name="impClientType" value="<%=IUMConstants.CLIENT_TYPE_OWNER%>" <%=clientTypeChecked_O%> onClick="resetImpairment('<%=clientType%>', this.value)">Owner&nbsp;
                                                <br>
                                                <% } %>
                                                <table border="0" cellpadding="2" cellspacing="1" width="<%=TABLE_WIDTH%>" bgcolor="#FFFFFF" style="border: 1px solid #D1D1D1;" class="listtable1">
                                                  <% //if (Long.parseLong(currentStatus) == IUMConstants.STATUS_UNDERGOING_ASSESSMENT) { %>
                                                  
                                                  	<% if(contextPath.equalsIgnoreCase(wmsContextPath)){ %>
                                                  		<tr>
                                                    	<td class="headerrow7" width="20" align="center">&nbsp;</td>
                                                    	<td class="headerrow7" width="200" align="center"><span class="required">*</span>DESCRIPTION</td>
                                                    	<td class="headerrow7" width="115" align="center"><span class="required">*</span>RELATIONSHIP</td>
                                                    	<td class="headerrow7" width="115" align="center"><span class="required">*</span>IMPAIRMENT ORIGIN</td>
                                                    	<td class="headerrow7" width="70" align="center"><span class="required">*</span>IMPAIRMENT DATE</td>
                                                    	<td class="headerrow7" width="100" align="center">STAT (HT/WT/BP)</td>
                                                    	<td class="headerrow7" width="40" align="center">CONFIRM</td>
                                                  		</tr>
                                                  	<% }else{ %>
                                                  		<% if (Long.parseLong(currentStatus) == IUMConstants.STATUS_UNDERGOING_ASSESSMENT) { %>
                                                  		<tr>
                                                    	<td class="headerrow6" width="5" align="center">&nbsp;</td>
                                                    	<td class="headerrow6" width="300" align="center"><span class="required">*</span>DESCRIPTION</td>
                                                    	<td class="headerrow6" width="175" align="center"><span class="required">*</span>RELATIONSHIP</td>
                                                    	<td class="headerrow6" width="175" align="center"><span class="required">*</span>IMPAIRMENT ORIGIN</td>
                                                    	<td class="headerrow6" width="150" align="center"><span class="required">*</span>IMPAIRMENT DATE</td>
                                                    	<td class="headerrow6" width="150" align="center">STAT (HT/WT/BP)</td>
                                                    	<td class="headerrow6" width="50" align="center">CONFIRM</td>
                                                  		</tr>
                                                  		<%}else{%>
                                                  			<tr>
                                                    		<td class="headerrow6" width="5" align="center">&nbsp;</td>
                                                    		<td class="headerrow6" width="300" align="center">DESCRIPTION</td>
                                                    		<td class="headerrow6" width="175" align="center">RELATIONSHIP</td>
                                                    		<td class="headerrow6" width="175" align="center">IMPAIRMENT ORIGIN</td>
                                                    		<td class="headerrow6" width="150" align="center">IMPAIRMENT DATE</td>
                                                    		<td class="headerrow6" width="150" align="center">STAT (HT/WT/BP)</td>
                                                    		<td class="headerrow6" width="50" align="center">CONFIRM</td>
                                                  			</tr>
                                                  	<%}%>
                                                  <%}%>
                                                 <%--  <% } else { %>
                                                  
                                                  		<% if(contextPath.equalsIgnoreCase(wmsContextPath)){ %>
                                                  			<tr>
                                                    		<td class="headerRow7" width="20" align="center">&nbsp;</td>
                                                    		<td class="headerRow7" width="200" align="center">DESCRIPTION</td>
                                                    		<td class="headerRow7" width="115" align="center">RELATIONSHIP</td>
                                                    		<td class="headerRow7" width="115" align="center">IMPAIRMENT ORIGIN</td>
                                                    		<td class="headerRow7" width="70" align="center">IMPAIRMENT DATE</td>
                                                    		<td class="headerRow7" width="100" align="center">STAT (HT/WT/BP)</td>
                                                    		<td class="headerRow7" width="40" align="center">CONFIRM</td>
                                                  			</tr>
                                                  	   <%}else{%>
                                                  			<tr>
                                                    		<td class="headerrow6" width="5" align="center">&nbsp;</td>
                                                    		<td class="headerrow6" width="300" align="center">DESCRIPTION</td>
                                                    		<td class="headerrow6" width="175" align="center">RELATIONSHIP</td>
                                                    		<td class="headerrow6" width="175" align="center">IMPAIRMENT ORIGIN</td>
                                                    		<td class="headerrow6" width="150" align="center">IMPAIRMENT DATE</td>
                                                    		<td class="headerrow6" width="150" align="center">STAT (HT/WT/BP)</td>
                                                    		<td class="headerrow6" width="50" align="center">CONFIRM</td>
                                                  			</tr>
                                                  		<%}%>  
                                                  <% } %> --%>

                                                  <%
                                                  int i = 0;
                                                  String tr_class;
                                                  %>
                                                  <logic:iterate id="impairment" name="detailForm" property="impairments">
                                                  <%
												  String height = ((ImpairmentForm)impairment).getHeightInFeet();
												  String weight = ((ImpairmentForm)impairment).getWeight();
												  String bloodPressure = ((ImpairmentForm)impairment).getBloodPressure();
												  String impExportDate = ((ImpairmentForm)impairment).getExportDate();
												  String impairmentCheckBoxAccess = "";
												  if (impExportDate != null && !impExportDate.equals("")){
													impairmentCheckBoxAccess = "disabled";
												  }
												  if(contextPath.equalsIgnoreCase(wmsContextPath)){
												  	if (i%2 == 0) {
                                                    	tr_class = "row7";
                                                  	} else {
                                                    	tr_class = "row8";
                                                  	}
												  } else {
													 if (i%2 == 0) {
	                                                    tr_class = "row1";
	                                                 } else {
	                                                    tr_class = "row2";
	                                                 }
												  }
                                                  %>
                                                  
                                                  <% if(contextPath.equalsIgnoreCase(wmsContextPath)){ %>
                                                  	<tr>
                                                    <td class="<%=tr_class%>" width="20"><input type="checkbox" class="label2" name="idSelected" value='<bean:write name="impairment" property="impairmentId"/>' <%=impairmentCheckBoxAccess%>></td>
                                                    <td class="<%=tr_class%>" width="200"><bean:write name="impairment" property="description"/></td>
                                                    <td class="<%=tr_class%>" width="115"><bean:write name="impairment" property="relationship"/></td>
                                                    <td class="<%=tr_class%>" width="115"><bean:write name="impairment" property="impairmentOrigin"/></td>
                                                    <td class="<%=tr_class%>" width="110"><bean:write name="impairment" property="impairmentDate"/></td>
                                                    <td class="<%=tr_class%>" width="100">
													  <%if (height != null && height.equals("")){%>
														  &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp/
													  <%} else {%>
	                                                      <bean:write name="impairment" property="heightInFeet"/>'<bean:write name="impairment" property="heightInInches"/>" /
													  <%}%>
													  <%if (weight != null && weight.equals("")){%>
													  	  &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp/
    	                                              <%} else {%>
													  	<bean:write name="impairment" property="weight"/> /
													  <%}%>
													  <%if (bloodPressure != null && bloodPressure.equals("")){%>
													  	  &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
        	                                          <%} else {%>
        	                                          	  <bean:write name="impairment" property="bloodPressure"/>
        	                                          <%}%>
                                                    </td>
                                                    <td class="<%=tr_class%>" width="60" align="center"><bean:write name="impairment" property="confirm"/></td>
                                                  	</tr>
                                                  <%}else{%>
                                                  <tr>
                                                    <td class="<%=tr_class%>" width="5"><input type="checkbox" class="label2" name="idSelected" value='<bean:write name="impairment" property="impairmentId"/>' <%=impairmentCheckBoxAccess%>></td>
                                                    <td class="<%=tr_class%>" width="300"><bean:write name="impairment" property="description"/></td>
                                                    <td class="<%=tr_class%>" width="175"><bean:write name="impairment" property="relationship"/></td>
                                                    <td class="<%=tr_class%>" width="175"><bean:write name="impairment" property="impairmentOrigin"/></td>
                                                    <td class="<%=tr_class%>" width="150"><bean:write name="impairment" property="impairmentDate"/></td>
                                                    <td class="<%=tr_class%>" width="150">
													  <%if (height != null && height.equals("")){%>
														  &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp/
													  <%} else {%>
	                                                      <bean:write name="impairment" property="heightInFeet"/>'<bean:write name="impairment" property="heightInInches"/>" /
													  <%}%>
													  <%if (weight != null && weight.equals("")){%>
													  	  &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp/
    	                                              <%} else {%>
													  	<bean:write name="impairment" property="weight"/> /
													  <%}%>
													  <%if (bloodPressure != null && bloodPressure.equals("")){%>
													  	  &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
        	                                          <%} else {%>
        	                                          	  <bean:write name="impairment" property="bloodPressure"/>
        	                                          <%}%>
                                                    </td>
                                                    <td class="<%=tr_class%>" width="75" align="center"><bean:write name="impairment" property="confirm"/></td>
                                                  </tr>
                                                  <%}%>
                                                  <%i++;%>
                                                  </logic:iterate>
                                                  <% if (i == 0) { %>
                                                  <tr>
                                                  <%
                                                  if(contextPath.equalsIgnoreCase(wmsContextPath)){ %>
                                                    <td class="row8" width="5">&nbsp;</td>
                                                    <td class="row8" colspan="6"><bean:message key="message.noexisting" arg0="impairments"/></td>
                                                  <%} else {%>
                                                  	<td class="row2" width="5">&nbsp;</td>
                                                    <td class="row2" colspan="6"><bean:message key="message.noexisting" arg0="impairments"/></td>
                                                  <%}%>  
                                                  </tr>
                                                  <%
                                                  i++;
                                                  }
                                                  %>
                                                  <% //if (Long.parseLong(currentStatus) == IUMConstants.STATUS_UNDERGOING_ASSESSMENT) { %>
                                                  <%
                                                  if(contextPath.equalsIgnoreCase(wmsContextPath)){
  												  	if (i%2 == 0) {
                                                      	tr_class = "row7";
                                                    	} else {
                                                      	tr_class = "row8";
                                                    	}
  												  } else {
  													 if (i%2 == 0) {
  	                                                    tr_class = "row1";
  	                                                 } else {
  	                                                    tr_class = "row2";
  	                                                 }
  												  }
                                                  %>
                                                  
                                                   <% if(contextPath.equalsIgnoreCase(wmsContextPath)){ %>
                                                   	<tr>
                                                    <td class="<%=tr_class%>" width="20">&nbsp;</td>
                                                    <td class="<%=tr_class%>" width="200"><ium:list className="<%=LABEL_CLASS%>" styleName="width:200" disabled="true" listBoxName="code" type="<%=IUMConstants.LIST_MIB_IMPAIRMENT_CODE%>" selectedItem="<%=impCode%>"/></td>
                                                    <td class="<%=tr_class%>" width="115"><ium:list className="<%=LABEL_CLASS%>" styleName="width:115" disabled="true" listBoxName="relationship" type="<%=IUMConstants.LIST_MIB_NUMBER_CODE%>" selectedItem="<%=impRelationship%>"/></td>
                                                    <td class="<%=tr_class%>" width="115"><ium:list className="<%=LABEL_CLASS%>" styleName="width:115" disabled="true" listBoxName="impairmentOrigin" type="<%=IUMConstants.LIST_MIB_LETTER_CODE%>" selectedItem="<%=impOrigin%>"/></td>
                                                    <td class="<%=tr_class%>" width="70"><input type="text" style="width:40" maxlength="9" disabled="true" name="impairmentDate" class="label2" value="<%=impDate%>">&nbsp;<div id="imp_cal" style="display:none;position:absolute;"><a href="#" onClick="popUpWin('<%=contextPath%>/jsp/calendar.jsp?textbox=requestForm.impairmentDate',290,155);"><img src="<%=contextPath%>/images/calendar.gif" border="0" disabled></a></div></td>
                                                    <td class="<%=tr_class%>" width="100">
                                                      <div style="width:100">
                                                        <input type="text" style="width:30" disabled="true" maxlength="6" name="height" value="<%=impHeight%>" class="label2">
                                                        <input type="text" style="width:30" disabled="true" maxlength="3" name="weight" value="<%=impWeight%>" class="label2">
                                                        <input type="text" style="width:30" disabled="true" maxlength="7" name="bloodPressure" value="<%=impBP%>" class="label2">
                                                      </div>
                                                    </td>
                                                    <td class="<%=tr_class%>" width="60" align="center"><ium:list className="label2" styleName="width:60" disabled="true" listBoxName="confirm" type="<%=IUMConstants.LIST_IMPAIRMENT_CONFIRMATION%>" selectedItem="<%=impConfirmationCode%>"/></td>
                                                  	</tr>
                                                  <%}else{%>
                                                  	<% if (Long.parseLong(currentStatus) == IUMConstants.STATUS_UNDERGOING_ASSESSMENT) { %>
                                                  <% //if(contextPath.equalsIgnoreCase(wmsContextPath)){ %>
                                                  	<tr>
                                                    <td class="<%=tr_class%>" width="20">&nbsp;</td>
                                                    <td class="<%=tr_class%>" width="200"><ium:list className="label2" styleName="width:200" disabled="true" listBoxName="code" type="<%=IUMConstants.LIST_MIB_IMPAIRMENT_CODE%>" selectedItem="<%=impCode%>"/></td>
                                                    <td class="<%=tr_class%>" width="115"><ium:list className="label2" styleName="width:115" disabled="true" listBoxName="relationship" type="<%=IUMConstants.LIST_MIB_NUMBER_CODE%>" selectedItem="<%=impRelationship%>"/></td>
                                                    <td class="<%=tr_class%>" width="115"><ium:list className="label2" styleName="width:115" disabled="true" listBoxName="impairmentOrigin" type="<%=IUMConstants.LIST_MIB_LETTER_CODE%>" selectedItem="<%=impOrigin%>"/></td>
                                                    <td class="<%=tr_class%>" width="70"><input type="text" style="width:20" maxlength="9" disabled="true" name="impairmentDate" class="label2" value="<%=impDate%>">&nbsp;<div id="imp_cal" style="display:none;position:absolute;"><a href="#" onClick="popUpWin('<%=contextPath%>/jsp/calendar.jsp?textbox=requestForm.impairmentDate',290,155);"><img src="<%=contextPath%>/images/calendar.gif" border="0" disabled></a></div></td>
                                                    <td class="<%=tr_class%>" width="100">
                                                      <div style="width:100">
                                                        <input type="text" style="width:30" disabled="true" maxlength="6" name="height" value="<%=impHeight%>" class="label2">
                                                        <input type="text" style="width:30" disabled="true" maxlength="3" name="weight" value="<%=impWeight%>" class="label2">
                                                        <input type="text" style="width:30" disabled="true" maxlength="7" name="bloodPressure" value="<%=impBP%>" class="label2">
                                                      </div>
                                                    </td>
                                                    <td class="<%=tr_class%>" width="60" align="center"><ium:list className="label2" styleName="width:60" disabled="true" listBoxName="confirm" type="<%=IUMConstants.LIST_IMPAIRMENT_CONFIRMATION%>" selectedItem="<%=impConfirmationCode%>"/></td>
                                                  	</tr>
                                                <%--  <%}else{%>
                                                  <tr>
                                                    <td class="<%=tr_class%>" width="5">&nbsp;</td>
                                                    <td class="<%=tr_class%>" width="300"><ium:list className="label2" styleName="width:300" disabled="true" listBoxName="code" type="<%=IUMConstants.LIST_MIB_IMPAIRMENT_CODE%>" selectedItem="<%=impCode%>"/></td>
                                                    <td class="<%=tr_class%>" width="175"><ium:list className="label2" styleName="width:175" disabled="true" listBoxName="relationship" type="<%=IUMConstants.LIST_MIB_NUMBER_CODE%>" selectedItem="<%=impRelationship%>"/></td>
                                                    <td class="<%=tr_class%>" width="175"><ium:list className="label2" styleName="width:175" disabled="true" listBoxName="impairmentOrigin" type="<%=IUMConstants.LIST_MIB_LETTER_CODE%>" selectedItem="<%=impOrigin%>"/></td>
                                                    <td class="<%=tr_class%>" width="150"><input type="text" style="width:75" maxlength="9" disabled="true" name="impairmentDate" class="label2" value="<%=impDate%>">&nbsp;<div id="imp_cal" style="display:none;position:absolute;"><a href="#" onClick="popUpWin('<%=contextPath%>/jsp/calendar.jsp?textbox=requestForm.impairmentDate',290,155);"><img src="<%=contextPath%>/images/calendar.gif" border="0" disabled></a></div></td>
                                                    <td class="<%=tr_class%>" width="150">
                                                      <div style="width:160">
                                                        <input type="text" style="width:50" disabled="true" maxlength="6" name="height" value="<%=impHeight%>" class="label2">
                                                        <input type="text" style="width:50" disabled="true" maxlength="3" name="weight" value="<%=impWeight%>" class="label2">
                                                        <input type="text" style="width:51" disabled="true" maxlength="7" name="bloodPressure" value="<%=impBP%>" class="label2">
                                                      </div>
                                                    </td>
                                                    <td class="<%=tr_class%>" width="75" align="center"><ium:list className="label2" styleName="width:75" disabled="true" listBoxName="confirm" type="<%=IUMConstants.LIST_IMPAIRMENT_CONFIRMATION%>" selectedItem="<%=impConfirmationCode%>"/></td>
                                                  </tr>
                                                  <%}%> --%>
                                                  <%}%>
                                                  <% } %>
                                                  <bean:size id="noOfPages" name="page" property="pageNumbers" />
                                                  <% if (noOfPages.intValue() > 1) { %>
                                                  <tr>
                                                    <td class="headerrow4" width="<%=TABLE_WIDTH%>" height="10" class="label2" valign="bottom" colspan="7">
                                                    <%
                                                    int pageNumber = Integer.parseInt((String)request.getAttribute("pageNo"));
                                                    String viewPage = "viewUWAssessmentDetail.do";
                                                    int currLink = 1;
                                                    int prevLink = 1;
                                                    int firstPage = 1;
                                                    //int lastPage = noOfPages.intValue();

                                                    Page displayPage = (Page) request.getAttribute("page");
                                                    int lastPage = 0;
                                                    ArrayList pageNos = (ArrayList)displayPage.getPageNumbers();
                                                    if (pageNos.size() > 0) {
                                                      lastPage = ((Integer)pageNos.get(noOfPages.intValue()-1)).intValue();
                                                    }
                                                    %>

                                                    <!-- don't display link for previous page if the current page is the first page -->
                                                    <% if (pageNumber > firstPage) { %>
                                                      <a href="#" onClick="javascript:rePaginate(1,'<%=viewPage%>');"><img src="<%=contextPath%>/images/pagnavfirst_off.gif" name="pagnavfirst" border="0" width="18" height="13"/></a>&nbsp;
                                                      <a href="#" onClick="javascript:rePaginate('<%=pageNumber-1%>','<%=viewPage%>');"><img src="<%=contextPath%>/images/pagnavleft_off.gif" name="pagnavleft" border="0" width="18" height="13"/></a>
                                                    <% } else {%>
                                                      <img src="<%=contextPath%>/images/pagnavfirst_on.gif" name="pagnavfirst" border="0" width="18" height="13"/>&nbsp;
                                                      <img src="<%=contextPath%>/images/pagnavleft_on.gif" name="pagnavleft" border="0" width="18" height="13"/>
                                                    <% } %>

                                                    <logic:iterate id="navLinks" name="page" property="pageNumbers">
                                                      <% currLink = ((Integer)navLinks).intValue();%>
                                                      <% if (((Integer)navLinks).intValue() == pageNumber) { %>
                                                        <b><bean:write name="navLinks"/></b>
                                                      <% } else { %>
                                                        <% if ((currLink > (prevLink+1))) { %>
                                                        ...
                                                        <% } %>
                                                        <a href="#" onClick="rePaginate('<bean:write name="navLinks"/>','<%=viewPage%>');" class="links2"><bean:write name="navLinks"/></a>
                                                      <% } %>
                                                      <%prevLink = currLink;%>
                                                    </logic:iterate>

                                                    <!-- don't display link for next page if the current page is the last page -->
                                                    <% if(pageNumber < lastPage) { %>
                                                      <a href="#" onClick="rePaginate('<%=pageNumber+1%>','<%=viewPage%>');"><img src="<%=contextPath%>/images/pagnavright_off.gif" name="pagnavright" border="0" width="18" height="13"/></a>&nbsp;
                                                      <a href="#" onClick="rePaginate('<%=prevLink%>','<%=viewPage%>');"><img src="<%=contextPath%>/images/pagnavlast_off.gif" width="18" height="13" border="0"></a>
                                                    <% } else { %>
                                                      <img src="<%=contextPath%>/images/pagnavright_on.gif" width="18" height="13" border="0">&nbsp;
                                                      <img src="<%=contextPath%>/images/pagnavlast_on.gif" width="18" height="13" border="0">
                                                    <% } %>
                                                    </td>
                                                  </tr>
                                                  <% } %>

                                                </table>
                                              </td>
                                            </tr>
                
                                          </table>
                                        </td>
                                      </tr>
                                    </table>
                                  </td>
                                </tr>
                                <% //if (Long.parseLong(currentStatus) == IUMConstants.STATUS_UNDERGOING_ASSESSMENT) {%>
                                <tr><td class="label2">&nbsp;</td></tr>
                                <tr id="createMaintain">
                                  <td class="<%=LABEL_CLASS%>" align="left">
                                    <input type="button" class="button1" name="create" value="Create" onClick="showSaveCancel(); hideCreateMaintain(); enableImpairmentFields(); setDefaultFocus();" <%=CREATE_UW%>>&nbsp;
                                    <input type="button" class="button1" name="maintain" value="Maintain" onClick="maintainImpairment()" <%=MAINTAIN_UW%>>
									<input type="button" class="button1" name="delete" value="Delete" onClick="deleteImpairment()" <%=DELETE_UW%>>
                                  </td>
                                </tr>
                                <tr id="saveCancel">
                                  <td class="label2" align="left">
                                    <input type="button" class="button1" name="save" value="Save" onClick="saveImpairment();">&nbsp;
                                    <input type="button" class="button1" name="cancel" value="Cancel" onClick="cancelImpairment();">
                                  </td>
                                </tr>
                                <% //} %>
                              </table>
                            </td>
                          </tr>
                        </table>
                      </td>
                    </tr>
                  </table>
                  <!-- END OF CONTENT -->
                </td>
              </tr>
              <tr>
                <td colspan="10" class="label5">� 2003 Sun Life Financial. All rights reserved.</td>
              </tr>
              <tr>
                <td colspan="10" class="label5">&nbsp;</td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
      <input type="hidden" name="clientType" value="<%=request.getParameter("clientType")%>">
      <input type="hidden" name="actionType" value="">
      <input type="hidden" name="heightInFeet" value="">
      <input type="hidden" name="heightInInches" value="">
      <input type="hidden" name="pageNo" value="1">
    </form>
  </body>
</html>
