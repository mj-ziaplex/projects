<%String contextPath = request.getContextPath();%>

// ---------------------------------------------------------
//    Menu Variables
// ---------------------
//    Adjust the variables in this section to make the menu
// ---------------------------------------------------------
var mArray = new Array('LinkID','LinkTxt','HREF','SubMenu') // do not edit
mArray[0]  = new Array ('a1','a1b1','a1b2','a1b3','a1b4','a1b5','a2','a2b1','a2b2','a3','a3b1','a3b2','a3b3','a4','a4b1','a4b2','a5','a5b1','a5b2','a6')

if (IE4) {
     //mArray[1]  = new Array ('List of Values', 'Reference Codes', 'Sunlife Offices','Examiners','Laboratories', 'Workflow','Process Config',' Underwriting Auto-assign','Notification Statement','Security','Users','Work Functions','User Access','Admin Logs','Audit Trail','Exceptions','Batch Processes','MIB Reports','MIB Schedule','Medical Expiration')	

	mArray[1] = new Array ;
	mArray[1][0] = 'List of Values';
	mArray[1][1] = 'Reference Codes';
	mArray[1][2] = 'Sun Life Offices';
	mArray[1][3] = 'Examiners';
	mArray[1][4] = 'Laboratories';
	mArray[1][5] = 'Requirements';

	mArray[1][6] = 'Workflow';
	mArray[1][7] = 'Process Configuration';
	mArray[1][8] = 'Notification Templates';

	mArray[1][9] = 'Security';
	mArray[1][10] = 'Users';
	mArray[1][11] = 'Work Functions';
	mArray[1][12] = 'Access Templates';

	mArray[1][13] = 'Admin Logs';
	mArray[1][14] = 'Audit Trail';
	mArray[1][15] = 'Exceptions';

	mArray[1][16] = 'Batch Processes';
	mArray[1][17] = 'MIB Export';
	mArray[1][18] = 'Medical Expiration';

	mArray[1][19] = 'Purging';
	
}

//mArray[2]  = new Array ('#','tempAdmin.html','tempSunlifeOffices.html','tempExaminer.html','tempLaboratory.html','#','tempProcessConfig.html','tempAutoAssignment.html','tempNotification.html','#','<%=contextPath%>/listUserProfile.do','tempWorkFunctions.html','<%=contextPath%>/viewTemplate.do','#','tempAuditTrail.html','tempExceptions.html','#','#','#','#')

mArray[2] = new Array ;
mArray[2][0] = '#';														//--- List of Values ---
mArray[2][1] = '<%=contextPath%>/adminReference.do';					//link for Reference Codes
mArray[2][2] = '<%=contextPath%>/viewOffices.do';						//link for Sunlife Offices
mArray[2][3] = '<%=contextPath%>/listExaminers.do';						//link for Examiners
mArray[2][4] = '<%=contextPath%>/listLaboratory.do';					//link for Laboratories
mArray[2][5] = '<%=contextPath%>/viewAdminRequirementCode.do';			//--- link for requirements ---
mArray[2][6] = '#';														//--- WorkFlow ---
mArray[2][7] = '<%=contextPath%>/configureWorkflow.do';					//link for Process Config
mArray[2][8] = '<%=contextPath%>/confNotificationTemp.do';				//link for Notification Statement
mArray[2][9] = '#';														//--- Security ---
mArray[2][10] = '<%=contextPath%>/listUserProfile.do';					//link for Users
mArray[2][11] = '<%=contextPath%>/viewWork.do';							//link for Work Functions
mArray[2][12] = '<%=contextPath%>/viewTemplate.do';						//link for User Access Templates

mArray[2][13] = '#';													//--- Admin Logs ---
mArray[2][14] = '<%=contextPath%>/listAuditTrail.do';					//link for Audit Trails
mArray[2][15] = '<%=contextPath%>/listException.do';					//link for Exceptions
mArray[2][16] = '#';													//link for BatchReports
mArray[2][17] = '<%=contextPath%>/displayMIBSchedule.do';  				//link for MIB Schedule
mArray[2][18] = '<%=contextPath%>/showExpireSchedule.do?mode=view';	//link for Medical Record Expiration
mArray[2][19] = '<%=contextPath%>/jsp/purgeOldData.jsp';    	        //link for Purging Old data
//mArray[3]  = new Array (1,0,0,0,0,1,0,0,0,1,0,0,0,1,0,0,1,0,0,0)

mArray[3] = new Array ;
mArray[3][0] = 1;
mArray[3][1] = 0;
mArray[3][2] = 0;
mArray[3][3] = 0;
mArray[3][4] = 0;
mArray[3][5] = 0;
mArray[3][6] = 1;
mArray[3][7] = 0;
mArray[3][8] = 0;
mArray[3][9] = 1;
mArray[3][10] = 0;
mArray[3][11] = 0;
mArray[3][12] = 0;
mArray[3][13] = 1;
mArray[3][14]= 0;
mArray[3][15] = 0;
mArray[3][16] = 1;
mArray[3][17] = 0;
mArray[3][18] = 0;
mArray[3][19] = 0;
var maxSub = 1 //this is the maximum submenu depth (count from 0) this could become a calculation


// ---------------------------------------------------------
//    Immediate Scripts
// ---------------------------------------------------------

// set up the hilight Array
var hilightArray = new Array()
for (var i=0; i<=maxSub; i++) {
	hilightArray[i] = null
}
// openItem is initially set to '' - to set the open Item,
// set this variable equal to the item ID in the HTML file,
// after this script file is called
var openItem = ''


// ---------------------------------------------------------
//    Menu Equivalency Statements
// ---------------------------------------------------------

// writeMenu globals
mStyle      = (NS4) ? 'CLASS="nsDx"' : '';
subImg      = '/images/arrowrtwht2.gif'
subImgOn    = '/images/arrowrtwht2_on.gif'
spImg       = '/images/sp.gif'
nsFirst     = ' CLASS="nsFirst"'
nsSecond    = ' CLASS="nsOrange"'
isOpenStart = (NS4) ? '<TR><TD>' : '';
isOpenEnd   = (NS4) ? '</TD></TR>' : '';

// doHilight image Path
subBkgd   = (NS4) ? '/images/bg_yellow.gif' : 'url("/images/bg_yellow.gif")'


// ---------------------------------------------------------
//    Menu Functions
// ---------------------------------------------------------

// shows menu
function showMenu (mID) {
	clearTimeout(timeID)
	mL = mID.length
	isOpenItem = (openItem.substr(0,mL)==mID)
	isOpenTree = (mL>2) ? (openItem.substr(0,mL-2)==mID.substr(0,mL-2)) : isOpenItem ;
	mLevel = ((openItem!=null)&&(!isOpenTree))||(mL<=2) ? (mL/2) - 1 : (mL/2) - 2
	//objID is the item that is rolled over
	objID = (NS4) ? 'level' + mLevel + '.document.' : ''
	theObjRef = gDivRef + objID + mID
	theObj = eval(theObjRef)

	
	curObjRef = hilightArray[mLevel]
	// the following will force the submenu to turn off if on the base level
	isSubOnMain = 0
	if ((openItem == mID) && (mL>2)) { isSubOnMain = 1 }
	
	// turn off the current highlight
	if (curObjRef != null) {
		curObj = eval(curObjRef)
		doHilight (curObj, 0, mLevel)
		closeLevels(mLevel)
	}
	// set the new item in the array
	if (!isOpenItem || isSubOnMain) { 
		hilightArray[mLevel] = theObj
		doHilight (theObj, 1, mLevel, isOpenTree)
	}
		
if (!isOpenItem) {
	// Detect if there is a subMenu
	for (x in mArray[0]) {
		if (mArray[0][x] == mID && mArray[3][x] == 1) { // there is a sub-menu
			theDocRef = gDivRef + 'level' + (mLevel+1) // these are at the doc root
			theDocObj = eval(theDocRef)
			
			if (NS4) {
				theDocObj.document.open('text/html');
				txt = writeMenu(mID)
				theDocObj.document.write(txt);
				theDocObj.document.close();
				theDocObj.bgColor="#FFCC00"
				
				// page coordinates
				y = theObj.pageY
				x = theObj.pageX 
				w = theObj.clip.width
						
			} else {
          
				theDocObj.innerHTML = '';
				theDocObj.innerHTML = writeMenu(mID)

				// coordinates local to previous level div
				y = eval(theObjRef + '.offsetTop')// original placement
				if ((mLevel > 0)) {
					y += eval(theObjRef + '.offsetParent.offsetTop')   
				}
				x = theObj.offsetParent.offsetLeft
				w = theObj.offsetWidth
			}
			
			// Position the Popup
			rPos = (NS4) ? x + w + 4 : x + w + 4 // add 8 for NS table padding
			eval(theDocRef + gStyle + '.left = ' + rPos);
			eval(theDocRef + gStyle + '.top = ' + y);
			eval(theDocRef + gStyle + '.visibility="' + gVisible + '"');
			
			break
		}
	}
}
}

// ---------------------------------------------------------
// hides menu

function hideMenu() {
	timeID = setTimeout('closeLevels(0)', 1000)
}


function doHilight (theObj, state, mLevel, isOpenTree) {
	if (isOpenTree) { // sub menu on base level
		// always level 0
		hil = state ? '#FAEEBB' : trsp	
		if (NS4) {
			theObj.bgColor = hil
		} else {
			theObj.style.backgroundColor = hil
		}

	} else if (mLevel == 0) { // base menu
		hil = state ? '#FFCC00' : trsp
		if (NS4) {
			theObj.bgColor = hil
		} else {
			theObj.style.backgroundColor = hil
		}
	} else { // submenu
		if (NS4) {
			hil = state ? subBkgd : null
			theObj.background.src = hil
		} else {
			hil = state ? subBkgd : 'none'
			theObj.style.backgroundImage = hil
		}
	}
}


function closeLevels (startLevel) {
	// turn off the current highlight
	curObjRef = hilightArray[startLevel]
	if (curObjRef != null) {
		curObj = eval(curObjRef)
		doHilight (curObj, 0, startLevel)
		hilightArray[startLevel] = null
		//closeLevels(startLevel)
		for (var i = startLevel+1; i<= maxSub; i++) {
			theDocRef = gDivRef + 'level' + i // these are at the doc root
			eval(theDocRef + gStyle + '.visibility="' + gHidden + '"');
			hilightArray[i] = null
		}
	}
}
		
function writeMenu(cID) {
	var txt  = ""
	var mLinkNum = 0
	var mLevel = (cID.length/2)
	var m = String.fromCharCode(97+mLevel)
	for (x in mArray[0]) {
		if (doLink(cID, mArray[0][x], m)) {
			var isOpen = (mArray[0][x]==openItem.substr(0,mLevel+2))
			mLinkNum++
			var mID = cID + m + mLinkNum
			var mDiv = '<DIV ID="' + mID + '" ' + mStyle
			var mLink  = '<A HREF="' + mArray[2][x] + '"'
			var mJS  = ' onMouseOver="showMenu(\'' + mID + '\')" onMouseOut="hideMenu()"'
			var cImg = (mArray[3][x]) ? ((isOpen) ? subImgOn : subImg) : spImg;
			var mImg = '<IMG SRC="' + cImg + '" ALIGN="baseline" VSPACE="0" BORDER="0" WIDTH="10" HEIGHT="7">'
			var mLinkText = mImg + mArray[1][x]

			if (NS4) {
				txtStyle = (mLevel > 0) ? nsSecond : nsFirst
				// NETSCAPE : DIV + LINK + JS + SPACER
				txt += '<TR><TD>' + mDiv + '>' + mLink + mJS + txtStyle + '>'
				txt += '<IMG SRC="/images/sp.gif" BORDER="0" WIDTH="10" HEIGHT="1">' + mLinkText
				txt += '<\/A>'
				txt += '<BR><IMG SRC="/images/sp.gif" BORDER="0" WIDTH="160" HEIGHT="3">' //NS width only works with this at the end
				txt += '<\/DIV></TD></TR>'
	
			} else {
				mClass = (mLevel > 0) ? 'jorange' : 'jfirst'
				// IE : LINK + DIV + JS
				txt += mLink + '>' + mDiv + mJS + ' CLASS="' + mClass + '">' + mLinkText
				txt += '<\/DIV><\/A>'
			}
			//if link is "on", write the menu below the link.
			if (isOpen) {
				txt += isOpenStart + writeMenu(mArray[0][x]) + isOpenEnd
			}
			
		}
	}
	if (NS4) {
		
		txt = '<TABLE BORDER="0" WIDTH="160" CELLPADDING="0" CELLSPACING="0">' + txt + '</TABLE>'
	}
	return txt
}

function doLink(cID, dbItem, m) {
	// detect if the items in the array start with the current Item
	if ((dbItem.indexOf(cID + m) != -1) && (dbItem.length-2 == cID.length)) {
		return true
	}
	return false
}

function writeDiv() {
	txt = ''
	for (var i = 1; i <= maxSub; i++) {
		txt += '<DIV ID="level' + i + '" STYLE="position:absolute; visibility:hidden;'
		if (IE4) {
			txt += ' z-index=' + (500+i) + '; background-color:#FFCC00; width:147'
		}
		txt += '"><\/DIV>\n'
	}
	return txt
}


function initMenu() {
	if (NS4) {
		document.level0.top = 126 // 71(table)+9(image)+46(stock ticker)
	}
	subMenu=0
	for (var i=2; i<=openItem.length; i++) {
		
		objID = (NS4) ? 'level0.document.' : ''
		theObjRef = gDivRef + objID + openItem.substr(0,i)
		theObj = eval(theObjRef)
		doHilight (theObj, 1, 0, subMenu)
		subMenu=1 // after the first time we are into submenus
		i++ // increment by 2 each loop
	}
	eval(gDivRef + 'level0' + gStyle + '.visibility = "' + gVisible + '"')
}



function MM_openBrWindow(theURL,winName,features) { //v2.0
window.open(theURL,winName,features);
}
	
function investorinfo()
{
MM_openBrWindow('http:/' + '/' + 'http://www.sunlife.com/slcorp/genericpage/0,3324,bGFuZy1lbmdsaXNoX3NpdGUtc2xjb3JwX2Vudi1saXZlX3B6bi1nZW5lcmljX3NlYy0xOF9zdGF0LV9lZC1fbmF2LTMzMzU0,00.html','investor','')
}

function corporateinfo()
{
MM_openBrWindow('http:/' + '/' + 'http://www.sunlife.com/slcorp/genericpage/0,3324,bGFuZy1lbmdsaXNoX3NpdGUtc2xjb3JwX2Vudi1saXZlX3B6bi1nZW5lcmljX3NlYy0xOF9zdGF0LV9lZC1fbmF2LTMzMzE5,00.html','corporate','')
}

function worldwide()
{
MM_openBrWindow('http:/' + '/' + 'http://www.sunlife.com/slcorp/genericpage/0,3324,bGFuZy1lbmdsaXNoX3NpdGUtc2xjb3JwX2Vudi1saXZlX3B6bi1nZW5lcmljX3NlYy0xOF9zdGF0LV9lZC1fbmF2LTMzMzgy,00.html','worldwide','')
}

function sunlink()
{
MM_openBrWindow('/services/sunlinkredirect.asp','sunlink','width=798,height=480,top=25,left=0,scrollbars=yes,status=yes,resizable=yes,toolbar=no,location=no')
}

function groupPortal()
{
MM_openBrWindow('/group/gripredirect.asp','b2b','')
}

