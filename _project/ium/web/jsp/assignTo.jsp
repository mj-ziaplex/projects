<%String contextPath = request.getContextPath();%>
<html>
  <head>
    <title>Integrated Underwriting and Medical System</title>
    <link href="<%=contextPath%>/css/main.css" rel="stylesheet" type="text/css">
    <script language="Javascript" src="<%=contextPath%>/js/navigation.js"></script>
  </head>
  <body>
    <table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" align="center">
      <tr>
        <td class="label2" align="center"><b>Assigned To</b></td> 
        <td align="center">
          <select class="label2">
            <option SELECTED></option>
            <option>Gonzalvo, Christine</option>
            <option>Jimenez, Jean</option>
            <option>Magnaye, Randy</option>
            <option>Repuyan, Grace</option>
            <option>Villareal, Mae</option>
          </select>
        </td> 
        <td align="center"><input type="button" class="button1" value="Ok" style="width:50px;" onClick="opener.location.href='koReqMedLab.htm';window.close();"></td>
      </tr>
    </table>
  </body>
</html>
