<jsp:useBean id="userProfile" scope="request" class="com.slocpi.ium.data.UserProfileData"/>
<jsp:useBean id="noOfPendingAR" scope="request" class="java.lang.String"/>
<jsp:useBean id="noOfPendingMR" scope="request" class="java.lang.String"/>
<%@ taglib uri="/ium.tld" prefix="ium" %>
<%@ taglib uri="/struts-bean.tld" prefix="bean" %>
<%@ page language="java" import="java.util.*,com.slocpi.ium.data.*, com.slocpi.ium.ui.form.*, com.slocpi.ium.util.*, com.slocpi.ium.ui.util.*, java.text.* " buffer="12kb"%>

<%
/* Andre Ceasar Dacanay - css enhancement - start */
String iumCss = "";
String iumColorO = "";
String companyCode = "";
try {
	ResourceBundle rbWMSSP = ResourceBundle.getBundle("dbconfig");
	iumCss = rbWMSSP.getString("IUM_CSS_VERSION");
	companyCode = iumCss;
	iumCss = "_"+iumCss;
	
} catch (Exception e){}
if(iumCss.equals("_CP")){
	iumColorO = "#FFCB00";
	companyCode="";
}else if(iumCss.equals("_GF")){
	iumColorO = "#303A77";
	companyCode = companyCode+" ";
}else{
	iumColorO = "#FFCB00";
}
/* Andre Ceasar Dacanay - css enhancement - end */
%>
<%String contextPath = request.getContextPath();%>

<%
    // This routine queries the access rights of the users (RCRIVERA)
    //
    // --- START --- 
    
    StateHandler sessionHandler = new StateHandler();
    Access access = new Access();
	HashMap userAccess = sessionHandler.getUserAccess(request);
    if(!access.hasAccess(userAccess,IUMConstants.PAGE_HOME,IUMConstants.ACC_INQUIRE) ){                                                                                                                    
    	response.sendRedirect("noAccessPage.jsp");   
    } 
    
    // --- END ----
%>
    

<html>
  <head>
    <title><%=companyCode%>Integrated Underwriting and Medical System</title>
    <link href="<%=contextPath%>/css/pw_main.css" rel="stylesheet" type="text/css">
    <link href="<%=contextPath%>/css/main<%=iumCss%>.css" rel="stylesheet" type="text/css">
    <link href="<%=contextPath%>/css/styleCommon.css" rel="stylesheet" type="text/css">
    <script language="Javascript" src="<%=contextPath%>/js/navigation.js"></script>
    <script language="Javascript" src="<%=contextPath%>/js/validation.js"></script>
    <script language="Javascript">
    <!--
    popupwin = null;
    var contextPath = '<%=contextPath%>';	    
    function openWindowAdvanceSetting () {
    	var form1 = document.forms[0];
    	popupwin = window.open(contextPath+'/configureUserPreference.do', 'Notification Settings','dialogWidth:845px;dialogHeight:400px;dialogTop:100;dialogLeft:100;resizable:no;scroll:yes;status:no;help:no');    
    }
    //-->
    </script>
  </head>
  
  <body leftmargin="0" topmargin="0" onUnload="closePopUpWin();">
    <form name="frm" method="post">
      <table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%">
        <tr>
          <td width="100%" colspan="2">    
            <jsp:include page="header.jsp" flush="true"/>
          </td>
        </tr>
        <tr>
          <td rowspan="2" bgcolor="<%=iumColorO%>" height="100%" width="20"><img src="<%=contextPath%>/images/spacer.gif" width="20"></td>
          <td background="<%=contextPath%>/images/GradientGray.jpg" height="27" width="100%" valign="top">&nbsp;&nbsp;
            <span class="main">Integrated Underwriting and Medical System</span>
          </td>
        </tr>
        <tr>
          <td width="100%" height="100%" valign="top">
            
            <!-- BODY Start -->
            <table cellpadding="0" cellspacing="0" border="0" bordercolor="#2A4C7C" width="100%">
              <tr>
                <td>
                  <table cellpadding="10" cellspacing="0" border="0" width="100%">
                    <tr>
                      <td>
                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                          <tr><td class="label1"><img src="<%=contextPath%>/images/myhomepage_title.jpg" border="0"></td></tr>
                          <tr><td class="label1">&nbsp;</td></tr>
                          <tr><td class="label1"><b>Welcome <%= userProfile.getFirstName() %> <%= userProfile.getLastName() %> </b></td></tr>
                          <tr><td class="label1"><b>Today is <%= DateHelper.format(new java.util.Date(), "EEEEE, MMMMM d, yyyy") %></b></td></tr>
                          <tr><td class="label1">&nbsp;</td></tr>
                          <tr>
                            <td class="label1">
                              <table border="0" cellpadding="0" cellspacing="0">
                                <tr><td class="title2">Reminder</td></tr>
                                <tr><td class="label2">You have <a href="listAssessmentRequests.do" class="links2"><b><u><%= noOfPendingAR %></u></b></a> Pending Assessment Requests.</td></tr>
                                <tr><td class="label2">You have <a href="listMedicalRecords.do" class="links2"><b><u><%= noOfPendingMR %></u></b></a> Pending Medical Records.</td></tr>
                                <tr><td class="label2">&nbsp;</td></tr>
                                <tr><td class="title2">Announcements</td></tr>
                                <tr><td class="label2">There will be a raffle draw conducted to all Sunlife Employees. So visit the SLOCPI website now.</td></tr>
                                <input type="hidden" value="home" name="frPage">
                                <tr><td class="label2">&nbsp;</td></tr>
                                <tr><td class="title2">Need Help?</td></tr>
                                <tr><td class="label2">If you are encountering problems using the IUM System, please send </td></tr>
                                <tr><td class="label2">an <!--a href="emailto:iumhelpdesk@sunlife.com.ph" class="links2"><b-->EMAIL<!--/b></a--> or call HELPDESK at 8866186 loc 4000 or 1-800-1888-4000(FREE)</td></tr>
                                <tr><td class="label2">&nbsp;</td></tr>
	                            <tr><td class="title2">My Preferences</td></tr>
                                <tr>
                                	<td>
	 	                           	   <table border="1" bordercolor="#2A4C7C"  cellspacing=0 bgcolor="" width="380" class="listTable1">
	 	                           	   	<tr>
	 	                           	   		<td>
	 	                           	   			<table border="0">
	 	                           	   			<tr><td><input type="hidden" name="notificationInd" value ="" ></td></tr>
	 	                           	   			<%if(userProfile.hasNotification() == true) {%>
    		        		                    <tr><td class="label2">&nbsp; Receive Notification <input name="notification" type="radio" value="true" checked onClick="changeRadioValue();">Yes</input>&nbsp;<input name="notification" type="radio" onClick="changeRadioValue();" >No</input></td><td align="left" class="links1"><a href="#" onClick="popUpWinScroll('<%=contextPath%>/configureUserPreference.do',600,350);">Advanced Settings</a></td></tr>
    		        		                    <%} else{%>
    		        		                    <tr><td class="label2">&nbsp; Receive Notification <input name="notification" type="radio" onClick="changeRadioValue();">Yes</input>&nbsp;<input name="notification" type="radio" value="false" checked onClick="changeRadioValue();">No</input></td><td align="left" class="links1"><a href="#" onClick="popUpWinScroll('<%=contextPath%>/configureUserPreference.do',600,350);">Advanced Settings</a></td></tr>
		    	        	                    <%}%>
	    	        	                         <tr><td class="label2">&nbsp; Records per View :&nbsp;<input type="textbox" name="recordsPerView" class="label2" maxlength="2" value='<bean:write name="userProfile" property="recordsPerView"/>' onKeyUp="getKeyNum(event, this);" onblur="checkNumVal(this);"></td>
   			        	                        <tr>
   			        	                           <td>&nbsp;<input type="button" class="button1" value=" Save " onClick="gotoPage('frm','<%=contextPath%>/home.do');" ></td>   			        	                        
		    	        	                    </tr>

		    	        	                    </table>
		    	        	                </td>
		    	        	             </tr>		    	        	             
            	                    </table>
            	                    </td>
            	                </tr>
                                <tr><td class="label2">&nbsp;</td></tr>
                                <tr><td class="label2">&nbsp;</td></tr>
                                <tr><td class="label2">&nbsp;</td></tr>
                                <tr><td class="label2">&nbsp;</td></tr>
                                <tr><td class="label2">&nbsp;</td></tr>
                                <tr><td class="label2">&nbsp;</td></tr>
                                <tr><td class="label2">&nbsp;</td></tr>
                                <tr><td class="label2">&nbsp;</td></tr>
                                <tr><td class="label2">&nbsp;</td></tr>
                              </table>
                            </td>
                          </tr>
                        </table>
                      </td>
                      <tr><td class="label5" height="100%">� 2003 Sun Life Financial. All rights reserved.</td></tr>
                    </tr>
                  </table>
                </td>
              </tr>              
            </table>
            <!-- BODY End -->            
          </td>
        </tr>
      </table>
    </form>
  </body>
</html>
