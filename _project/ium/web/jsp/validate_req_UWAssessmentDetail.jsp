<%@ page language="java" import="com.slocpi.ium.util.IUMConstants, com.slocpi.ium.util.DateHelper" buffer="12kb"%>
<%@ taglib uri="/struts-bean.tld" prefix="bean" %>

<!--
function isValid() {
	var form = document.requestForm;
	
	// split height details to height in feet and height in inches
	var height = form.height.value;
	
	if ((height.match("'") != null) && (height.match("\"") != null)) {
		var h1 = trim(height.substr(0, height.length-1));
	
		if (!isEmpty(h1)) {
			var h2 = h1.split("'");
	
			var hFt = trim(h2[0]);
				var hIn = trim(h2[1]);
	
		}
	}

	// split blood pressure
	var bloodPressure = form.bloodPressure.value;
	

	if (bloodPressure.match("-") != null) {
		var bp = bloodPressure.split("-");
		var bp1 = trim(bp[0]);

		var bp2 = trim(bp[1]);

	}

	

	// field validations
	if (noSelection(form.code.value)) {
		alert("<bean:message key="error.field.specificrequired" arg0="Description"/>");
		form.code.focus();
		return false;
	}
	if (noSelection(form.relationship.value)) {
		alert("<bean:message key="error.field.specificrequired" arg0="Relationship"/>");
		form.relationship.focus();
		return false;
	}
	if (noSelection(form.impairmentOrigin.value)) {
		alert("<bean:message key="error.field.specificrequired" arg0="Impairment origin"/>");
		form.impairmentOrigin.focus();
		return false;
	}
	if (isEmpty(form.impairmentDate.value)) {
		alert("<bean:message key="error.field.specificrequired" arg0="Impairment date"/>");
		form.impairmentDate.select();
		form.impairmentDate.focus();
		return false;
	}
	if (isEmpty(form.actionCode.value)) {
		alert("<bean:message key="error.field.specificrequired" arg0="Action code"/>");
		form.actionCode.focus();
		return false;
	}
	if (!isValidDate(form.impairmentDate.value)) {
		alert("<bean:message key="error.field.format" arg0="Impairment date" arg1="date" arg2=""/>");
		form.impairmentDate.select();
		form.impairmentDate.focus();
		return false;
	}
	else if (isGreaterDate(form.impairmentDate.value, "<%=DateHelper.getCurrentDate()%>")) {
		alert("<bean:message key="error.field.beforecurrentdate" arg0="Impairment date"/>");
		form.impairmentDate.select();
		form.impairmentDate.focus();
		return false;
	}
	
/* remove validation of height due to change request
	if (isEmpty(form.height.value)) {
		alert("<bean:message key="error.field.specificrequired" arg0="Height"/>");
		form.height.select();
		form.height.focus();
		return false;
	}
	if ((form.height.value.match("'") == null) || (form.height.value.match("\"") == null)) {
		msg = "Format should be: XX' YY\", where\n";
		msg += "     XX is height in feet and\n";
		msg += "     YY is height in inches.";
		alert("<bean:message key="error.field.format" arg0="Height" arg1="" arg2="\\n"/>" +msg);
		form.height.select();
		form.height.focus();
		return false;
	}
	if (isEmpty(hFt)) {
		alert("<bean:message key="error.field.specificrequired" arg0="Height in feet"/>");
		form.height.select();
		form.height.focus();
		return false;
	}
	if (!isNumeric(hFt)) {
		alert("<bean:message key="error.field.numeric" arg0="Height in feet"/>");
		form.height.select();
		form.height.focus();
		return false;
	}
	if (isEmpty(hIn)) {
		alert("<bean:message key="error.field.specificrequired" arg0="Height in inches"/>");
		form.height.select();
		form.height.focus();
		return false;
	}
	if (!isNumeric(hIn)) {
		alert("<bean:message key="error.field.numeric" arg0="Height in inches"/>");
		form.height.select();
		form.height.focus();
		return false;
	}
*/

// new validation for height 
	if (!isEmpty(form.height.value)) {
		if ((form.height.value.match("'") == null) || (form.height.value.match("\"") == null)) {
			msg = "Format should be: XX' YY\", where\n";
			msg += "     XX is height in feet and\n";
			msg += "     YY is height in inches.";
			alert("<bean:message key="error.field.format" arg0="Height" arg1="" arg2="\\n"/>" +msg);
			form.height.select();
			form.height.focus();
			return false;
		}
		
		if (isEmpty(hFt)) {
			alert("<bean:message key="error.field.specificrequired" arg0="Height in feet"/>");
			form.height.select();
			form.height.focus();
			return false;
		}

		if (!isNumeric(hFt)) {
			alert("<bean:message key="error.field.numeric" arg0="Height in feet"/>");
			form.height.select();
			form.height.focus();
			return false;
		}

		if (isEmpty(hIn)) {
			alert("<bean:message key="error.field.specificrequired" arg0="Height in inches"/>");
			form.height.select();
			form.height.focus();
			return false;
		}

		if (!isNumeric(hIn)) {
			alert("<bean:message key="error.field.numeric" arg0="Height in inches"/>");
			form.height.select();
			form.height.focus();
			return false;
		}
	
	} 

/* remove validation due to change request
	if (isEmpty(form.weight.value)) {
		alert("<bean:message key="error.field.specificrequired" arg0="Weight"/>");
		form.weight.select();
		form.weight.focus();
		return false;
	}
	if (!isNumeric(form.weight.value)) {
		alert("<bean:message key="error.field.numeric" arg0="Weight"/>");
		form.weight.select();
		form.weight.focus();
		return false;
	}
*/

// new validation for weight
	if (!isEmpty(form.weight.value)) {
		if (!isNumeric(form.weight.value)) {
			alert("<bean:message key="error.field.numeric" arg0="Weight"/>");
			form.weight.select();
			form.weight.focus();
			return false;
		}
	}

/* remove validation due to change request
	if (isEmpty(form.bloodPressure.value)) {
		alert("<bean:message key="error.field.specificrequired" arg0="Blood pressure"/>");
		form.bloodPressure.select();
		form.bloodPressure.focus();
		return false;
	}
	if (form.bloodPressure.value.match("-") == null) {
		msg = "Format should be: XXX-YYY\".\n";
		msg += "     XXX and YYY should be numeric";
		alert("<bean:message key="error.field.format" arg0="Blood pressure" arg1="" arg2="\\n"/>" +msg);
		form.bloodPressure.select();
		form.bloodPressure.focus();
		return false;
	}
*/	

// new validation for weight
	if (!isEmpty(form.bloodPressure.value)) {
		if (form.bloodPressure.value.match("-") == null) {
			msg = "Format should be: XXX-YYY\".\n";
			msg += "     XXX and YYY should be numeric";
			alert("<bean:message key="error.field.format" arg0="Blood pressure" arg1="" arg2="\\n"/>" +msg);
			form.bloodPressure.select();
			form.bloodPressure.focus();
			return false;
		}
		
		if (isEmpty(bp1) || isEmpty(bp2) || !isNumeric(bp1) || !isNumeric(bp2)) {
			msg = "Format should be: XXX-YYY\".\n";
			msg += "     XXX and YYY should be numeric";
			alert("<bean:message key="error.field.format" arg0="Blood pressure" arg1="" arg2="\\n"/>" +msg);
			form.bloodPressure.select();
			form.bloodPressure.focus();
			return false;
		}
	}

/*	Remove validation for confirmation due to change request
	if (noSelection(form.confirm.value)) {
		alert("<bean:message key="error.field.specificrequired" arg0="Confirmation"/>");
		form.confirm.focus();
		return false;
	}	
*/

	form.heightInFeet.value = hFt;

	form.heightInInches.value = hIn;

	return true;
}


function saveImpairment(session_id) {	
	var form = document.requestForm;
	var flag = form.refreshFlag.value;
	var valid = null;

	
	if (flag == 1){
		if(isValid()){
			var form = document.requestForm;
			form.action = "maintainImpairment.do?session_id=" + session_id;
			form.submit();
		}
	}else{
		valid = isValid();


		if (valid) {
			//try{
			//	showLayer();
			//	window.question.focus();
			//	setLayerPosition2();
			//}catch(e){
			//}
			gotoPage("requestForm", "createImpairment.do?session_id=" + session_id);
		}
	}
}  


function resetImpairment(origClientType, destClientType) {
	var form = document.requestForm;
	if (origClientType != destClientType) {
		if (destClientType.toUpperCase() == "<%=IUMConstants.CLIENT_TYPE_OWNER%>") {
			form.clientType.value = "<%=IUMConstants.CLIENT_TYPE_OWNER%>";
		}
		else if (destClientType.toUpperCase() == "<%=IUMConstants.CLIENT_TYPE_INSURED%>") {
			form.clientType.value = "<%=IUMConstants.CLIENT_TYPE_INSURED%>";
		}
		gotoPage("requestForm", "viewUWAssessmentDetail.do#impairmentList");
	}
}


function showCreateMaintain(){ 
	var form = document.requestForm;
	var flag = form.refreshFlag.value;
	if(flag == 1){
		hideCreateMaintain();
	} else {
		document.all["createMaintain"].style.display = "block";
	}
}


function hideCreateMaintain(){
	document.all["createMaintain"].style.display = "none";
}


function showSaveCancel(){ 
	document.all["saveCancel"].style.display = "block";
}


function hideSaveCancel(){

	var form = document.requestForm;
	var flag = form.refreshFlag.value;
	if (flag == 1){
		showSaveCancel();
	}
	else {
		document.all["saveCancel"].style.display = "none";
	}
} 


function enableImpairmentFields() {
	var form = document.requestForm;
	enable(form.code);
	enable(form.relationship);
	enable(form.impairmentOrigin);
	enable(form.impairmentDate);
	enable(form.actionCode);
	enable(form.height);
	enable(form.weight);
	enable(form.bloodPressure);
	enable(form.confirm);
	enable(form.impCodeBut);
	document.all["imp_cal"].style.display = "block";
}


function disableImpairmentFields() {
	var form = document.requestForm;
	var flag = form.refreshFlag.value;
	if (flag == 1){
		enableImpairmentFields();
	}
	else {
		disable(form.code);
		disable(form.relationship);
		disable(form.impairmentOrigin);
		disable(form.actionCode);
		disable(form.impairmentDate);
		disable(form.height);
		disable(form.weight);
		disable(form.bloodPressure);
		disable(form.confirm);
		disable(form.impCodeBut);
		document.all["imp_cal"].style.display = "none";
	}
}

function clearImpairmentFields() {
	var form = document.requestForm;
	var flag = form.refreshFlag.value;
	if (flag != 1){

		form.relationship.selectedIndex = 0;
		form.impairmentOrigin.selectedIndex = 0;
		form.impairmentDate.value = "";
		form.height.value = "";
		form.weight.value = "";
		form.bloodPressure.value = "";
		form.confirm.value = "";
	}
}

function cancelImpairment() {
	var form = document.requestForm;
	form.code.selectedIndex = 0;
	form.relationship.selectedIndex = 0;
	form.impairmentOrigin.selectedIndex = 0;
	form.actionCode.selectedIndex = 0;
	form.impairmentDate.value = "";
	form.height.value = "";
	form.weight.value = "";
	form.bloodPressure.value = "";
	form.confirm.value = "";
	form.refreshFlag.value=0;
	hideSaveCancel(); 
	showCreateMaintain(); 
	disableImpairmentFields(); 
}


function haveSelectedStatus(){
	var form = document.requestForm;
	var status = form.requestStatus.value;
	if (status == null || status ==""){
		alert("<bean:message key="error.field.requiredselection" arg0="status"/>");
		form.requestStatus.focus();
		return false;
	}
	else {
		return true;
	}
}


function rePaginate (page, actionUrl) {
	form = document.requestForm;
	if (form.pageNo != null) {
		form.pageNo.value = page;	
	}
	gotoPage("requestForm", actionUrl);
}


function setDefaultFocus() {
	var form = document.requestForm;
	form.code.focus();
}

function deleteImpairment(session_id){
	var form = document.requestForm;
	var selected = countChecked();
	if (selected <= 0){
		alert("Select one or more impairments to delete.");
	}
	else {
		var ok = confirm("Do you want to delete this record(s)?");
		if(ok){
			form.action = "deleteImpairment.do?session_id=" + session_id;
			form.submit();
		}else {
			return;
		}
	}
}

function countChecked(){
	var frm = document.requestForm;
	var selected =0;
	if (frm.idSelected == null){
		return selected;
	}
	else {
		var length = frm.idSelected.length;
		if (length==null){
			if (frm.idSelected.checked==true){
				selected++;
			} 
		}
		else {
			for (var i=0; i<length; i++){
				if (frm.idSelected[i].checked==true){
					selected++;
				}
			}
		}
	}
	return selected;
}

function maintainImpairment(){
	var form = document.requestForm;
	var selected = countChecked();
	var lobCode = form.lob.value;
	
	//alert("lobCode: "+ lobCode)
	if (lobCode == "<%= IUMConstants.LOB_INDIVIDUAL_LIFE%>"){
		var length = form.impClientType.length;
		//alert("lobCode: "+ lobCode+"; length: "+length)
		for(var i=0; i<length; i++){
			if (form.impClientType[i].checked){
				var clientType = form.impClientType[i].value;
			}
		}
	}
	else {
		var clientType = "<%=IUMConstants.CLIENT_TYPE_INSURED%>";
	}
	
	if (selected <= 0){
		alert("Select an impairment to edit.");
	}
	else if(selected == 1){
		if (clientType == "<%=IUMConstants.CLIENT_TYPE_INSURED%>"){
			form.clientType.value = "<%=IUMConstants.CLIENT_TYPE_INSURED%>";
		} else {
			form.clientType.value = "<%=IUMConstants.CLIENT_TYPE_OWNER%>";
		}
		
		form.selectedMaintain.value = 1;
		//alert("form.selectedMaintain.value = 1");
		form.action = "maintainImpairment.do";
		//alert("form.action = maintainImpairment.do;");
		form.submit();
	}
	else {
		alert("Select only one impairment to edit.");
	}
}

function saveAnalysis(){
	var form = document.requestForm;
	
	if (form.UWAnalysis.value == null || form.UWAnalysis.value == ""){
		alert("<bean:message key="error.field.required" arg0="Analysis"/>");
	} else {
		saveRequest();
	}

}
//-->