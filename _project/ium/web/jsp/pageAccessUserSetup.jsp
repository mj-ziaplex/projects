<%@ page language="java" import="java.util.*, com.slocpi.ium.ui.form.*, com.slocpi.ium.util.*, com.slocpi.ium.ui.util.*, com.slocpi.ium.data.*" buffer="1024kb"%>
<%@ taglib uri="/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/ium.tld" prefix="ium" %>
<%
/* Andre Ceasar Dacanay - css enhancement - start */
String iumCss = "";
String iumColorO = "";
String companyCode = "";
try {
	ResourceBundle rbWMSSP = ResourceBundle.getBundle("dbconfig");
	iumCss = rbWMSSP.getString("IUM_CSS_VERSION");
	companyCode = iumCss;
	iumCss = "_"+iumCss;
} catch (Exception e){}
if(iumCss.equals("_CP")){
	iumColorO = "#FFCB00";
	companyCode="";
}else if(iumCss.equals("_GF")){
	iumColorO = "#303A77";
	companyCode = companyCode+" ";
}else{
	iumColorO = "#FFCB00";
}

/* Andre Ceasar Dacanay - css enhancement - end */
%>
<%String contextPath = request.getContextPath();
String fullName = (String)request.getParameter("fullName");
StateHandler sessionHandler = new StateHandler();
Access access = new Access();
%>
<jsp:useBean id="userData" scope="request" class="com.slocpi.ium.data.UserData" />
<jsp:useBean id="userAccessForm" scope="request" class="com.slocpi.ium.ui.form.UserAccessForm" />

<%
    // This routine queries the access rights of the users (RCRIVERA)
    //
    // --- START --- 
    
	HashMap userAccess = sessionHandler.getUserAccess(request);
    if(!access.hasAccess(userAccess,IUMConstants.PAGE_USER_ACCESS,IUMConstants.ACC_INQUIRE) ){                                                                                                                    
    	response.sendRedirect("noAccessPage.jsp");   
    } 
    String MAINTAIN_ACCESS = "";
    
    if(!access.hasAccess(userAccess,IUMConstants.PAGE_USER_ACCESS,IUMConstants.ACC_MAINTAIN) ){                                                                                                                     
	  MAINTAIN_ACCESS = "DISABLED";
    }
    
    
    // --- END ---
 %>    
<html>
   <head>
    <title><%=companyCode%>Integrated Underwriting and Medical System</title>
    <link href="<%=contextPath%>/css/pw_main.css" rel="stylesheet" type="text/css">
    <link href="<%=contextPath%>/css/main.css" rel="stylesheet" type="text/css">
    <link href="<%=contextPath%>/css/styleCommon.css" rel="stylesheet" type="text/css">
    <script language="JavaScript" src="<%=contextPath%>/js/dhtml_init.js"></script>
    <script language="JavaScript" src="<%=contextPath%>/js/navigation.js"></script>
    <script language="JavaScript" src="<%=contextPath%>/jsp/sl_left_nav.jsp"></script>

    <script language="Javascript">
    	  
    	function changeFlag(){
			var ctr = 0;
			while(ctr< document.userAccess.pageAccessCheckBox.length){
				if(document.userAccess.pageAccessCheckBox[ctr].checked){
    				document.userAccess.pageAccessResult[ctr].value = document.userAccess.pageAccessResult[ctr].value+"@y";
    			}
	    		else{
	    			document.userAccess.pageAccessResult[ctr].value = document.userAccess.pageAccessResult[ctr].value+"@n";
    			}
    			ctr++;
				
			}

    		document.userAccess.check.value = 'submit';

    		document.userAccess.submit();
	   	}
	 	function checkInquire(pageId,x,accessCount,i,count,disableCnt){

			var lastIndex = count-1;
			var temp = 0;
			var index=0;
			var numAccess = 1;
			var flag = 0;
			var y = x-1;
			var curr_index = lastIndex;

			index = (document.userAccess.pageAccessResult[curr_index].value).indexOf("~");

  			if(index == 0)
   				index = accessCount;

			if(x > <%=IUMConstants.ACC_INQUIRE%> && document.userAccess.pageAccessCheckBox[lastIndex].checked){
				while(y!=<%=IUMConstants.ACC_INQUIRE%>){
					numAccess++;
					y--;
				}

				parseInt(numAccess);
				parseInt(curr_index);
				parseInt(disableCnt);
				temp = parseInt(curr_index)-parseInt(numAccess);
				temp = parseInt(temp)+parseInt(disableCnt);

				if(!document.userAccess.pageAccessCheckBox[temp].checked){
   					document.userAccess.pageAccessCheckBox[temp].checked = true;
   				
   				}
		   	}
			else if(x == <%=IUMConstants.ACC_INQUIRE%> && 
			  document.userAccess.pageAccessCheckBox[lastIndex].checked == false){
				while(flag == 0 ){
					curr_index++;
					index = (document.userAccess.pageAccessResult[curr_index].value).indexOf("~");			
					if((document.userAccess.pageAccessResult[curr_index].value).substring(0,index) == pageId){
						if(document.userAccess.pageAccessCheckBox[curr_index].checked == true){
							flag = 1;
							temp = 1;
						}
					}
					else
						flag = 1;
				}
				if(temp == 1){
					document.userAccess.pageAccessCheckBox[lastIndex].checked = true;					
					alert("Please uncheck the other access/es first before unchecking this.");
				}
			}


	   	}	   	
  
      
    	
    </script>
  </head>
  
  <body leftmargin="0" topmargin="0">
	<form name = "userAccess" method = "post" action = "<%=contextPath%>/viewAccess.do">  
     <table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%">
	  <tr>
        <td width="100%" colspan="2">    
          <jsp:include page="header.jsp" flush="true"/>
        </td>
      </tr>
      <tr>
        <td class="label2" rowspan="2" bgcolor="<%=iumColorO%>" height="100%" width="20"><img src="<%=contextPath%>/images/spacer.gif" width="20"></td>
        <td class="label2" background="<%=contextPath%>/images/GradientGray.jpg" height="27" width="100%" valign="top">&nbsp;&nbsp;
          <span class="main">Integrated Underwriting and Medical System</span>
        </td>
      </tr> 
	  <tr>
        <td>
            <!-- BODY -->
            
			<table width ="100%" cellpadding="3" cellspacing="5" border="0">
  		
    		<tr>
    				<td class = "label2"><b>User ID </b>&nbsp;&nbsp;<%out.print(userAccessForm.getUserId());%>
    				<input type="hidden" name ="userId" value="<%=userAccessForm.getUserId()%>">
    				&nbsp;&nbsp;&nbsp;&nbsp;<b>Name</b> &nbsp;&nbsp;<%out.print(fullName);%>
    				<input type="hidden" name="fullName" value="<%=fullName%>">

    				</td>
    		</tr> 

	<%if (userAccessForm.getPageAccess() != null){%> 
    	<tr>
	    		<td colspan="4" align="left" >
	    		 <input type="hidden" value="display" name="check">
	    		 <input type="button" value="Save" class="button1" onclick="changeFlag()" <%= MAINTAIN_ACCESS %> >&nbsp;
			     <input type="button" value="Cancel" class="button1" onclick="gotoPage('userAccess','<%=contextPath%>/viewUserAdminDetail.do?userId=<%=userAccessForm.getUserId()%>&view=maintain');">
			    </td>
	    	</tr>
    		<tr valign="top">
    		    <td colspan="4">
			    	<table border="1" bordercolor="#2A4C7C" cellpadding="10" cellspacing="0" bgcolor=""  width="100%"  height="400" class ="listtable1">
					  <tr><td colspan="4" valign="top">
				        <table border="1" width="100%" cellspacing="1" cellpadding="0"  >
				        <tr class="headerRow6">
   				        <td class="headerRow6">PAGES</td>
				        <%for(int i=0;i<userAccessForm.getAccessCount();i++){%>
				        <td class="headerRow6"><%out.print(userAccessForm.getPageHeader()[i].toUpperCase());%></td>
				        <%}%>
				        </tr>				        	
				    
				    	
				    	<%int ctr = 0;
				    	for(int i=0;i< userAccessForm.getRowSize();i++){
				    	%>

				    	<%
				    		if(i%2 == 0){
				    	%>
				    		<tr bgcolor="#CECECE" class="row5">
				    		<td><%out.print(userAccessForm.getPageName()[i]);%></td>
				    	<%
				    		}
				    		else{
				    		
				    	%>	
				    		<tr bgcolor="#EDEFF0" class="row6">
							<td><%out.print(userAccessForm.getPageName()[i]);%></td>
						<%
							}
							int x = 0;		
							int disableCnt = 0;
							for(int j=0; j<userAccessForm.getAccessCount();j++){

								x = j+1;	
								if(userAccessForm.getPageAccess()[i].charAt(j)== 'y'){
									ctr++;

						%>
							<td valign="top" align="center" colspan="1">
               					<input type="hidden" name="pageAccessResult" value ="<%=userAccessForm.getPageId()[i]+"~"+x%>">
								<input type="checkbox" class="label2" name="pageAccessCheckBox" onclick="checkInquire('<%=userAccessForm.getPageId()[i]%>','<%=x%>','<%=userAccessForm.getAccessCount()%>','<%=i%>','<%=ctr%>','<%=disableCnt%>');" checked>
               				</td>
               			<%
               					}
               					else if (userAccessForm.getPageAccess()[i].charAt(j)== 'n'){
	               					ctr++;
               			%>
               				<td valign="top" align="center" colspan="1">
               					<input type="hidden" name="pageAccessResult" value ="<%=userAccessForm.getPageId()[i]+"~"+x%>">
								<input type="checkbox" class="label2" name="pageAccessCheckBox" onclick="checkInquire('<%=userAccessForm.getPageId()[i]%>','<%=x%>','<%=userAccessForm.getAccessCount()%>','<%=i%>','<%=ctr%>','<%=disableCnt%>');">
               				</td>
               				
               			<%
               					}
               					else{
		               				disableCnt++;
               			%>
               				<td valign="top" align="center" colspan="1">&nbsp;
               				</td>
               			<%
               					}
               				}
               			%>
               			</tr>
               			<%
               			}
               			%>
               			 </table>
				       </td></tr>
			    	</table>
			
	    		</td>
	    	</tr> 
	    	<tr>
	    		<td colspan="4" align="left" >
	 
	    		 <input type="button" value="Save" class="button1" onclick="changeFlag()" <%= MAINTAIN_ACCESS %> >&nbsp;
			     <input type="button" value="Cancel" class="button1" onclick="gotoPage('userAccess','<%=contextPath%>/viewUserAdminDetail.do?userId=<%=userAccessForm.getUserId()%>&view=maintain');">
			    </td>
	    	</tr>
	    	<%}
	    	else{%>
	    		<table height="350">
	    			<tr>
	    				<td>&nbsp;</td>
	    			</tr>
	    		</table>
	    	<%}%>
            <tr>
               <td colspan="10" class="label1"><font size="1">� 2003 Sun Life Financial. All rights reserved.</font></td>
            </tr>
            </table>
            <!-- BODY -->
		
          </td>
        </tr>
      </table>
    </form>
  </body>
</html>