<%@ taglib uri="/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/struts-html.tld" prefix="html" %>
<%@ taglib uri="/ium.tld" prefix="ium" %>
<jsp:useBean id="userProfile" scope="request" class="com.slocpi.ium.data.UserProfileData"/>

<%@ page language="java" import="java.util.*,com.slocpi.ium.data.*, com.slocpi.ium.ui.form.*, com.slocpi.ium.util.*, com.slocpi.ium.ui.util.*, java.text.* " buffer="12kb"%>
<%String contextPath = request.getContextPath();
StateHandler sessionHandler = new StateHandler();
Access access = new Access();
%>
<%
/* Andre Ceasar Dacanay - css enhancement - start */
String iumCss = "";
String iumColorB = "";
String companyCode = "";
try {
	ResourceBundle rbWMSSP = ResourceBundle.getBundle("dbconfig");
	iumCss = rbWMSSP.getString("IUM_CSS_VERSION");
	companyCode = iumCss;
	iumCss = "_"+iumCss;
} catch (Exception e){}
if(iumCss.equals("_CP")){
	iumColorB="#2A4C7C";
	companyCode="";
}else if(iumCss.equals("_GF")){
	iumColorB="#303A77";
	companyCode = companyCode+" ";
}else{
	iumColorB="#2A4C7C";
}
/* Andre Ceasar Dacanay - css enhancement - end */
%>
<html>
  <head>
    <title><%=companyCode%>Integrated Underwriting and Medical System</title>
    <link href="<%=contextPath%>/css/pw_main.css" rel="stylesheet" type="text/css">
    <link href="<%=contextPath%>/css/main.css" rel="stylesheet" type="text/css">
    <link href="<%=contextPath%>/css/styleCommon.css" rel="stylesheet" type="text/css">
    <script language="JavaScript" src="<%=contextPath%>/js/dhtml_init.js"></script>
    <script language="JavaScript" src="<%=contextPath%>/js/navigation.js"></script>
    <script language="JavaScript" src="<%=contextPath%>/jsp/sl_left_nav.jsp"></script>
  </head>
<%
    // This routine queries the access rights of the users (RCRIVERA)
    //
    // --- START --- 
    
	HashMap userAccess = sessionHandler.getUserAccess(request);
    if(!access.hasAccess(userAccess,IUMConstants.PAGE_ADMIN,IUMConstants.ACC_INQUIRE) ){                                                                                                                    
    	response.sendRedirect("noAccessPage.jsp");   
    } 
    // --- END ---
 %>    

  <body leftMargin="0" topmargin="0" marginwidth="0" marginheight="0">
    <form name="frm" method="post">      
      <table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%">
        <tr>
          <td width="100%" colspan="2">
            <jsp:include page="header.jsp" flush="true"/>
            <table border="0" cellpadding="0" cellspacing="0" width="100%" height="100%">
              <tr valign="top">
                <td background="<%=contextPath%>/images/bg_yellow.gif" id="leftnav" valign="top" width="147">
                  <img height="9" src="<%=contextPath%>/images/sp.gif" width="147"><br>
                  <img height="30" src="<%=contextPath%>/images/sp.gif" width="7"> <br>
                  <img height="7" src="<%=contextPath%>/images/sp.gif" width="147"><br>
                  <div id="level0" style="position: absolute; visibility: hidden; width: 100%; z-index: 500"> 
                    <script language=javascript>
	                  <!--
                      document.write(writeMenu(''));
                      if (IE4) {
                        document.write(writeDiv());
                      };
                      //-->
                    </script>
                  </div>
                  <div id="NSFix" style="position: absolute; visibility: hidden; z-index: 999">&nbsp;</div>
                  <!-- Netscape needs a hard-coded div to write dynamic DIVs --> 
                  <script language=javascript>
                    <!--
                    if (NS4) {
                      document.write(writeDiv());
                    }
                    //-->
                  </script>
                  <script>
                    <!--
                    initMenu();
                    //-->
                  </script>
                </td>

                <td width="100%"> 
                  <div align="left"> 
                    <table border="0" cellpadding="0" width="100%" cellspacing="0">
                      <tr> 
                        <td width="100%"> 
                          <div align="left">
                            <table border="0" cellpadding="3" cellspacing="0" width="100%" height="27"  background="<%=contextPath%>/images/sectionbkgray.jpg">
                              <tr> 
                                <td valign="top"><span class="main">Integrated Underwriting and Medical System</span></td>
                              </tr>
                            </table>
                          </div>
                        </td>
                      </tr>
                      <tr> 
                        <td width="100%" class="title2">&nbsp;</td>
                      </tr>
                      <tr>
                        <td width="100%" class="title2">&nbsp;&nbsp;Administration Page</td>
                      </tr>
                    </table>

                    <!--- START OF BODY -->
                    <table width="100%" cellpadding="3" cellspacing="5" border="0">
                      <tr valign="top">
                        <td>
                          <table border="1" bordercolor="<%=iumColorB%>" cellpadding="10" cellspacing="0" bgcolor="" width="700" class="listtable1">
                            <tr>
                              <td colspan="2" height="100%">
                                <table border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" style="border: 0px solid #D1D1D1;" class="listtable1">
                                  <tr>
                                    <td>
                                      <table border="0" cellpadding="0" cellspacing="0" width="100%">
			                          	<tr><td class="label1"><b>Welcome <%=userProfile.getFirstName()%> <%=userProfile.getLastName()%> </b></td></tr>
                         				<tr><td class="label1"><b>Today is <%=DateHelper.format(new java.util.Date(), "EEEEE, MMMMM d, yyyy") %></b></td></tr>
                                        <tr><td class="label2">&nbsp;</td></tr>
                                        <tr><td class="title2">Statistics</td></tr>
                                        <tr><td class="label2">Handshake Exceptions</td></tr>
                                        <tr><td class="label2"><a href="<%=contextPath%>/listException.do" class="label1"><li><b>ABACUS</b></li></a></td></tr>
                                        <tr><td class="label2"><a href="<%=contextPath%>/listException.do" class="label1"><li><b>PRISM</b></li></a></td></tr>
                                        <tr><td class="label2"><a href="<%=contextPath%>/listException.do" class="label1"><li><b>LWS</b></li></a></td></tr>
                                        <tr><td class="label2"><a href="<%=contextPath%>/listException.do" class="label1"><li><b>MIB</b></li></a></td></tr>
                                        <tr><td class="label2"><a href="<%=contextPath%>/listException.do" class="label1"><li><b>SMTP</b></li></a></td></tr>
                                        <tr><td class="label2">&nbsp;</td></tr>
                                        <tr><td class="title2">Pending</td></tr>
                                        <tr><td class="label2"><b><a href="<%=contextPath%>/displayMIBSchedule.do" class="label1"><bean:write name="adminForm" property="mibForExport"/></a></b> MIB Records for export.</td></tr>
                                        <tr><td class="label2"><b><a href="<%=contextPath%>/showExpireSchedule.do?mode=view" class="label1"><bean:write name="adminForm" property="medRecForExp"/></a></b> Medical Records for expiration.</td></tr>
                                        <tr><td class="label2"><b><a href="<%=contextPath%>/listUserProfile.do" class="label1"><bean:write name="adminForm" property="pwdRequired"/></a></b> Locked Users.</td></tr>
                                        <tr><td class="label2">&nbsp;</td></tr>
                                        <tr><td class="label2">&nbsp;</td></tr>
                                        <tr><td class="label2">&nbsp;</td></tr>
                                      </table>
                                    </td>
                                  </tr>
                                </table>
                              </td>
                            </tr>
                          </table>
                        </td>
                      </tr>
                    </table>
                    <!--- END OF BODY -->
                  </div>
                </td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </form>
  </body>
</html>