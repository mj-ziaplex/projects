<%@ page language="java" import="java.util.*,com.slocpi.ium.data.*, com.slocpi.ium.ui.form.*, com.slocpi.ium.util.*, com.slocpi.ium.ui.util.*, java.text.* " buffer="12kb"%>
<jsp:useBean id="docsRequirementsForm" scope="request" class="com.slocpi.ium.ui.form.DocumentsRequirementsForm"/>
<%@ taglib uri="/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/struts-html.tld" prefix="html" %>
<%@ taglib uri="/ium.tld" prefix="ium" %>
<%String contextPath = request.getContextPath();
StateHandler sessionHandler = new StateHandler();
Access access = new Access();
%>
<%
/* Andre Ceasar Dacanay - css enhancement - start */
String iumCss = "";
String iumBorderColor = "";
String iumBgColor = "";
String iumBgColorMOut = "";
String iumBgColorMOv = "";
String companyCode = "";
try {
	ResourceBundle rbWMSSP = ResourceBundle.getBundle("dbconfig");
	iumCss = rbWMSSP.getString("IUM_CSS_VERSION");
	companyCode = iumCss;
	iumCss = "_"+iumCss;
} catch (Exception e){}

if(iumCss.equals("_CP")){
	iumBgColorMOv="#2A4C7C";
	iumBgColorMOut="#BBBBBB";
	iumBorderColor="bordercolor=\"#2A4C7C\"";
	iumBgColor="bgcolor=\"#2A4C7C\"";
	companyCode="";
}else if(iumCss.equals("_GF")){
	iumBgColorMOv="#303A77";
	iumBgColorMOut="#BBBBBB";
	iumBorderColor="bordercolor=\"#303A77\"";
	iumBgColor="bgcolor=\"#303A77\"";
	companyCode = companyCode+" ";
}else{
	iumBgColorMOv="#2A4C7C";
	iumBgColorMOut="#BBBBBB";
	iumBorderColor="bordercolor=\"#2A4C7C\"";
	iumBgColor="bgcolor=\"#2A4C7C\"";
}
/* Andre Ceasar Dacanay - css enhancement - end */
%>
<%
    // This routine queries the access rights of the users (RCRIVERA)
    //
    // --- START --- 
    
	HashMap userAccess = sessionHandler.getUserAccess(request);
    if(!access.hasAccess(userAccess,IUMConstants.PAGE_PROCESS_CONFIG,IUMConstants.ACC_INQUIRE) ){                                                                                                                    
    	response.sendRedirect("noAccessPage.jsp");   
    } 
    
    String CREATE_ACCESS = "";
    String MAINTAIN_ACCESS = "";
       
    if(!access.hasAccess(userAccess,IUMConstants.PAGE_PROCESS_CONFIG,IUMConstants.ACC_CREATE) ){                                                                                                                     
	  CREATE_ACCESS = "DISABLED"; 	
    }
    if(!access.hasAccess(userAccess,IUMConstants.PAGE_PROCESS_CONFIG,IUMConstants.ACC_MAINTAIN )){                                                                                                                     
	  MAINTAIN_ACCESS = "DISABLED"; 	
    }
   
    
    
    // --- END ---
 %>    

<html>
<body>
<head>
<title><%=companyCode%>Integrated Underwriting and Medical System</title>

<link href="<%=contextPath%>/css/pw_main.css"     rel="stylesheet" type="text/css">
<link href="<%=contextPath%>/css/main<%=iumCss%>.css" rel="stylesheet" type="text/css">

<script language="JavaScript" src="<%=contextPath%>/js/dhtml_init.js"></script>
<script language="JavaScript" src="<%=contextPath%>/js/navigation.js"></script>
<script language="JavaScript" src="<%=contextPath%>/jsp/sl_left_nav.jsp"></script>
<script language="Javascript">
  	function lobChange() {
  		var form = document.forms[0];
  		form.actionType.value = "refresh";
  		form.submit();
  	}
  	function addDocType () {
  		var form = document.forms[0];
  		form.actionType.value = "addDocType";
  		var docType = form.documentType.value;
  		if (docType == "") {
  			alert('<bean:message key="error.field.requiredselection" arg0="document type"/>');
  			form.documentType.focus();
  			return false;
  		}
  		return true;
  	}
  	function addRequirement () {
  		var form = document.forms[0];
  		form.actionType.value = "addRequirement";
  		var requirement = form.requirement.value;
  		if (requirement == "") {
  			alert('<bean:message key="error.field.requiredselection" arg0="requirement"/>');
  			form.requirement.focus();
  			return false;
  		}
  		return true;
  	}
  	function goToPage(objType) {
  		var form = document.forms[0];
  		if (objType == '<%=IUMConstants.ASSESSMENT_REQUEST%>') {
  			form.action = '<%=contextPath%>/configureWorkflow.do';
  			form.objectType.value = '<%=IUMConstants.ASSESSMENT_REQUEST%>';
  			form.actionType.value = "refresh";
  		}else 
  		if (objType == '<%=IUMConstants.POLICY_REQUIREMENTS%>') {
  			form.action = '<%=contextPath%>/configureWorkflow.do';
  			form.objectType.value = '<%=IUMConstants.POLICY_REQUIREMENTS%>';
  			form.actionType.value = "refresh";
  		} else
  		if (objType == '<%=IUMConstants.MEDICAL_RECORDS%>') {
  			form.action = '<%=contextPath%>/configureWorkflow.do';
  			form.objectType.value = '<%=IUMConstants.MEDICAL_RECORDS%>';
  			form.actionType.value = "refresh";
  		} else
  		if (objType == 'DocsReq') {  			  			
  			form.actionType.value = "refresh";
  		}  		
  		form.submit();
  	}
</script>  	
</head>

<BODY leftMargin=0 topmargin=0 marginwidth=0 marginheight=0>
<table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%">
<tr><td width="100%" colspan="2">
  <!-- HEADER -->
      <jsp:include page="header.jsp" flush="true"/>
  <!-- HEADER -->
<table border="0" cellpadding="0" cellspacing="0" width="100%" height="100%">
  <tr vAlign=top> 
    <TD background="<%=contextPath%>/images/bg_yellow.gif" id=leftnav vAlign=top width=147> 
      <IMG height=9 src="<%=contextPath%>/images/sp.gif" width=147><BR>
      <IMG height=30 src="<%=contextPath%>/images/sp.gif" width=7> <BR>
      <IMG height=7 src="<%=contextPath%>/images/sp.gif" width=147><BR>
      <DIV id=level0 style="POSITION: absolute; VISIBILITY: hidden; WIDTH: 100%; Z-INDEX: 500"> 
      <link href="<%=contextPath%>/css/styleCommon.css" rel="stylesheet" type="text/css">
        <SCRIPT language=javascript>
			<!--
				document.write(writeMenu(''))
				if (IE4) { document.write(writeDiv()) };
				
			//-->
			</SCRIPT>
      </DIV>
      <DIV id=NSFix style="POSITION: absolute; VISIBILITY: hidden; Z-INDEX: 999">&nbsp;</DIV>
      <!-- Netscape needs a hard-coded DIV to write dynamic DIVs --> 
      <SCRIPT language=javascript>
                        <!--
                                if (NS4) { document.write(writeDiv()) }
                        //-->
                        </SCRIPT>
                        <SCRIPT>
                        <!--
                                initMenu();
                        //-->
                        </SCRIPT>
      <!-- END MENU CODE --> </td>
<form name="frm" action="<%=contextPath%>/confDocsRequirements.do" method="post">      
<input type="hidden" name="actionType">
<input type="hidden" name="objectType">
    <td width="100%"> 
      <div align="left"> 
        <table border="0" cellpadding="0" width="100%" cellspacing="0">
          <tr> 
            <td width="100%"> 
              <div align="left"> 
                <table border="0" cellpadding="3" cellspacing="0" width="100%" height="27"  background="<%=contextPath%>/images/sectionbkgray.jpg">
                  <tr> 
                    <td valign="top"> 
                       <span class="main">&nbsp;Integrated Underwriting and Medical System </span>
                    </td>
                  </tr>
                </table>
              </div>
            </td>
          </tr>
          <tr> 
            <td width="100%" colspan=2>&nbsp;</td>
          </tr>
        </table>
        <table width="100%" cellpadding="3" cellspacing="5" border="0">
          <tr valign="top">
         <td class="title2">Process Configuration Maintenance
          <table border="1" <%=iumBorderColor%> cellpadding=10 cellspacing=0 bgcolor="" width="900" class="listtable1">
           <tr><td colspan="2" height="100%">
              <table border="0" cellpadding="2" cellspacing="1" bgcolor=#FFFFFF style="border: 0px solid #D1D1D1;" class="listtable1">
                <tr ><td>

			<table border="0" cellpadding="10" cellspacing="0" height="100%" width="100%">						
				<tr>
     <td>
      <table border="0" cellpadding="2" cellspacing="0" width="900">
					  <tr>
        <td class="label2"><b>Line of Business</b></td>															
								<td width="750">
									<ium:list className="label2" listBoxName="lob" type="<%=IUMConstants.LIST_LOB%>" selectedItem='<%=docsRequirementsForm.getLob()%>' onChange="javascript:lobChange();"/>
								</td>
							</tr>
							<tr>
								<td colspan="2">&nbsp;</td>
							</tr>
						</table>
		   </td>
		  </tr> 
				<tr>					
				<td width="100%" height="100%" valign="top">
					<!--tabs navigation-->			
		    	<table cellpadding="1" cellspacing="0" border="0">

				<tr>
		      <td bgcolor="#BBBBBB" style="cursor:hand" onmouseover="this.style.backgroundColor = '<%=iumBgColorMOv%>'" onmouseout="this.style.backgroundColor = '<%=iumBgColorMOut%>'">
		       <table border="0" cellpadding="3" cellspacing="0">
		        <tr><a href="#" onClick="javascript:goToPage('<%=IUMConstants.ASSESSMENT_REQUEST%>');"><td class="tabLink"><b>Assessment Request </b></td></a></tr>
		       </table>
		      </td>
		      
		      <td></td>
		      <td bgcolor="#BBBBBB" style="cursor:hand" onmouseover="this.style.backgroundColor = '<%=iumBgColorMOv%>'" onmouseout="this.style.backgroundColor = '<%=iumBgColorMOut%>'">
		       <table border="0" cellpadding="3" cellspacing="0">
		        <tr><a href="#" onClick="javascript:goToPage('<%=IUMConstants.POLICY_REQUIREMENTS%>');"><td class="tabLink"><b>Requirement Management</b></td></a></tr>
		       </table>
		      </td>
		      
		      <td></td>
	      <td bgcolor="#BBBBBB" style="cursor:hand" onmouseover="this.style.backgroundColor = '<%=iumBgColorMOv%>'" onmouseout="this.style.backgroundColor = '<%=iumBgColorMOut%>'">
		       <table border="0" cellpadding="3" cellspacing="0">
		        <tr><a href="#" onClick="javascript:goToPage('<%=IUMConstants.MEDICAL_RECORDS%>');"><td class="tabLink"><b>Medical Records</b></td></a></tr>
		       </table>
		      </td>		      <td <%=iumBgColor%>>
				<table border="0" cellpadding="3" cellspacing="0">
		        <tr>
		         <td class="tabSelected"><b>Documents and Requirements</b></td>
		        </tr>
		       </table>
		      </td>		 		      
		      <td></td>       
		     </tr>
		    </table>
		    <!-- end of navigation tabs -->
		    <table cellpadding="0" cellspacing="0" border="1" <%=iumBorderColor%> width="100%">
							<tr>
								<td <%=iumBgColor%> colspan="11">&nbsp;</td>
							</tr>
							<tr>							
								<td>
									<table cellpadding="10" cellspacing="0" border="0" width="100%">
										<tr>
											<td valign="top"  width="100%">
												<!-- start of STATUS GROUP -->
												<table border="0" <%=iumBorderColor%> cellpadding="0" cellspacing="0" bgcolor="" width="100%">
													<tr>
														<td class="label2">
															<b>TYPE</b>
														</td>
													</tr>  
													<tr>
														<td valign="top">
															<table border="0" cellpadding="2" cellspacing="0" width="250">
																<tr>
																	<td valign="top">
		               	<table class="listTable1" width="100%">
																			<tr class="headerrow1">
																				<td width="250" align="center">
																					TYPE
																				</td>
																			</tr>
																			<%int i = 0;%>
																			<logic:present name="docsRequirementsForm" property="docTypes">
																				<logic:iterate id="docTypeEntry" name="docsRequirementsForm" property="docTypes">
																				<%if (i%2 == 0) {%>
																				<tr class="row1">
																				<%} else {%>
																				<tr class="row2">
																				<%}%>																				
																				<td width="250">
																					<bean:write name="docTypeEntry"/>
																				</td>
																				</tr>
																				<%i++;%>
																				</logic:iterate>
																			</logic:present>
																			
																			<%if (i%2 == 0) {%>
																			<tr class="row1">
																			<%} else {%>
																			<tr class="row2">
																			<%}%>
																			
																				<%String disableStatusList = "true";%>
																				<logic:notEmpty name="docsRequirementsForm" property="lob">
																				<%disableStatusList = "false";%>																				
																				</logic:notEmpty>	
																					
																				<td width="250">
																					<ium:list className="label2" listBoxName="documentType" type="<%=IUMConstants.LIST_LOB_DOCUMENT_TYPE%>" lob="<%=docsRequirementsForm.getLob()%>" disabled="<%=disableStatusList%>"/>																					
																				</td>
																			</tr>
																		</table>	
		               </td>
		              </tr>
		              <tr>  
																	<td align="left">
																		<logic:notEmpty name="docsRequirementsForm" property="lob">
																		<input class="button1" type="submit" value="Add Document Type" id=add name=add onClick="javascript:return(addDocType());" <%=CREATE_ACCESS%> >
																		</logic:notEmpty>
																	</td>    
		              </tr>
		             </table>
		            </td>
		           </tr>                                
											 </table>
											 <!-- end of STATUS group -->
										 </td>																					 
									 </tr>	
									 <tr>											 
										 <td>
												<!-- start of EVENTS group -->
											<table border="0" <%=iumBorderColor%> cellpadding="0" cellspacing="0" bgcolor="" width="100%">
													<tr>
														<td class="label2">
															<b>REQUIREMENTS</b>
														</td>
													</tr>  
													<tr>
														<td valign="top">
															<table border="0" cellpadding="2" cellspacing="0" width="100%">
																<tr>
																	<td valign="top">
		               	<table class="listTable1" width="100%">
																			<tr class="headerrow1">
																				<td width="40%">
																					REQUIREMENTS
																				</td>
																				<td width="60%">
																					DESCRIPTION
																				</td>
																				
																			</tr>
																			<%int k = 0;%>
																			<logic:present name="docsRequirementsForm" property="requirements">
																				<logic:iterate id="requirement" name="docsRequirementsForm" property="requirements">
																				<%if (k%2 == 0) {%>
																				<tr class="row1">
																				<%} else {%>
																				<tr class="row2">
																				<%}%>			
																				<td>
																					<bean:write name="requirement" property="value"/>
																				</td>
																				<td>
																					<bean:write name="requirement" property="name"/>
																				</td>																				
																				</tr>
																				<%k++;%>
																				</logic:iterate>
																			</logic:present>
																				
																			<%if (k%2 == 0) {%>
																			<tr class="row1">
																			<%} else {%>
																			<tr class="row2">
																			<%}%>
																			
																			<%String disableStatusList1 = "true";%>
																			<logic:notEmpty name="docsRequirementsForm" property="lob">
																			<%disableStatusList1= "false";%>																				
																			</logic:notEmpty>	
																				<td>
																					<ium:list className="label2" listBoxName="requirement" type="<%=IUMConstants.LIST_LOB_REQUIREMENTS%>" lob="<%=docsRequirementsForm.getLob()%>" disabled="<%=disableStatusList1%>"/>
																				</td>
																				<td>
																					&nbsp;
																				</td>
																				
																			</tr>
																		</table>	
		               </td>
		              </tr>
		              <tr>  
																	<td align="left">
																		<logic:notEmpty name="docsRequirementsForm" property="lob">
																		<input class="button1" type="submit" value="Add Requirement" id=add name=add onClick="javascript:return(addRequirement());" <%=CREATE_ACCESS%> >
																		</logic:notEmpty>
																	</td>    
		              </tr>
		             </table>
		            </td>
		           </tr>                                
											 </table>
		          </table>
		          <!-- end of EVENTS group -->
											</td>  
										</tr> 
									</table>
								</td>								
							</tr>
						</table>			
						<!-- end of navigation tabs -->																															
					</td>
				</tr>
			</table>
			<!-- end of body -->                
                  
<!--- END OF BODY -->
</form>
</td>
</tr>
</table>
</div>
</body>
</html>