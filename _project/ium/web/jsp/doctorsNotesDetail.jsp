<%@ page language="java" import="java.util.*, com.slocpi.ium.ui.form.*, com.slocpi.ium.util.*, com.slocpi.ium.ui.util.*, com.slocpi.ium.data.*, com.slocpi.ium.underwriter.*" buffer="1024kb"%>
<%@ taglib uri="/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/struts-html.tld" prefix="html" %>
<%@ taglib uri="/ium.tld" prefix="ium" %>
<%String contextPath = request.getContextPath();%>
<%
/* Andre Ceasar Dacanay - css enhancement - start */
String iumCss = "";
String iumColorB = "";
String iumColorO = "";
String companyCode = "";
try {
	ResourceBundle rbWMSSP = ResourceBundle.getBundle("dbconfig");
	iumCss = rbWMSSP.getString("IUM_CSS_VERSION");
	companyCode = iumCss;
	iumCss = "_"+iumCss;
} catch (Exception e){}

if(iumCss.equals("_CP")){
	iumColorB="#2A4C7C";
	iumColorO = "#FFCB00";
	companyCode="";
}else if(iumCss.equals("_GF")){
	iumColorB="#303A77";
	iumColorO = "#303A77";
	companyCode = companyCode+" ";
}else{
	iumColorB="#2A4C7C";
	iumColorO = "#FFCB00";
}

/* Andre Ceasar Dacanay - css enhancement - end */
%>
<%
    // This routine queries the access rights of the users (RCRIVERA)
    //
    // --- START ---

    StateHandler sessionHandler = new StateHandler();
    Access access = new Access();

	HashMap userAccess = sessionHandler.getUserAccess(request);
    if(!access.hasAccess(userAccess,IUMConstants.PAGE_ASSESSMENT_DETAILS,IUMConstants.ACC_INQUIRE) ){
    	response.sendRedirect("noAccessPage.jsp");
    }

    String CREATE_ACCESS = "";
    String MAINTAIN_ACCESS = "";
    String DELETE_ACCESS = "";
    String EXECUTE_ACCESS = "";
    String CREATE_REQUEST = "";
    String CREATE_DOCTOR = "";
    String MAINTAIN_DOCTOR = "";
    String DELETE_DOCTOR = "";
    String EXECUTE_DOCTOR = "";

    boolean INQUIRE_REQUEST = true;
    boolean INQUIRE_CDS = true;
    boolean INQUIRE_KOREQRMT = true;
    boolean INQUIRE_UWASSESS= true;
    boolean INQUIRE_DOCTORNOTES = true;

    // CREATE ASSESSMENT REQUEST
    if (!access.hasAccess(userAccess,IUMConstants.PAGE_ASSESSMENT_DETAILS,IUMConstants.ACC_CREATE) ){
	  CREATE_ACCESS = "DISABLED";
    }
	// MAINTAIN ASSESSMENT REQUEST
    if (!access.hasAccess(userAccess,IUMConstants.PAGE_ASSESSMENT_DETAILS,IUMConstants.ACC_MAINTAIN) ){
	  MAINTAIN_ACCESS = "DISABLED";
    }
	// -- NOT APPLICABLE --
    if (!access.hasAccess(userAccess,IUMConstants.PAGE_ASSESSMENT_DETAILS,IUMConstants.ACC_DELETE) ){
	  DELETE_ACCESS = "DISABLED";
    }
	// UPDATE LOCATION
    if (!access.hasAccess(userAccess,IUMConstants.PAGE_ASSESSMENT_DETAILS,IUMConstants.ACC_EXECUTE) ){
	  EXECUTE_ACCESS = "DISABLED";
    }

    // ENTER DOCTOR'S NOTES
    if(!access.hasAccess(userAccess,IUMConstants.PAGE_DOCTORS_NOTES,IUMConstants.ACC_CREATE) ){
	  CREATE_DOCTOR = "DISABLED";
    }
    
   
    // -- NOT APPLICABLE --
    if(!access.hasAccess(userAccess,IUMConstants.PAGE_DOCTORS_NOTES,IUMConstants.ACC_MAINTAIN) ){
	  MAINTAIN_DOCTOR = "DISABLED";
    }
    
    // -- NOT APPLICABLE --
    if(!access.hasAccess(userAccess,IUMConstants.PAGE_DOCTORS_NOTES,IUMConstants.ACC_DELETE) ){
	  DELETE_DOCTOR = "DISABLED";
    }
    // -- NOT APPLICABLE --
    if(!access.hasAccess(userAccess,IUMConstants.PAGE_DOCTORS_NOTES,IUMConstants.ACC_EXECUTE) ){
	  EXECUTE_DOCTOR = "DISABLED";
    }

    if(!access.hasAccess(userAccess,IUMConstants.PAGE_REQUEST_DETAILS,IUMConstants.ACC_INQUIRE) ){
	  INQUIRE_REQUEST = false;
    }
    if(!access.hasAccess(userAccess,IUMConstants.PAGE_CDS,IUMConstants.ACC_INQUIRE) ){
	  INQUIRE_CDS = false;
    }
    if(!access.hasAccess(userAccess,IUMConstants.PAGE_REQUIREMENTS_KO,IUMConstants.ACC_INQUIRE) ){
	  INQUIRE_KOREQRMT = false;
    }
    if(!access.hasAccess(userAccess,IUMConstants.PAGE_UW_ASSESSMENT,IUMConstants.ACC_INQUIRE) ){
	  INQUIRE_UWASSESS = false;
    }
    if(!access.hasAccess(userAccess,IUMConstants.PAGE_DOCTORS_NOTES,IUMConstants.ACC_INQUIRE) ){
	  INQUIRE_DOCTORNOTES = false;
    }

    // --- END ---
    
 %>
<html>
  <head>
    <title><%=companyCode%>Integrated Underwriting and Medical System</title>
    <link href="<%=contextPath%>/css/main<%=iumCss%>.css" rel="stylesheet" type="text/css">
    <script language="Javascript" src="<%=contextPath%>/js/navigation.js"></script>
    <script language="Javascript" src="<%=contextPath%>/js/validation.js"></script>
    <script language="Javascript" src="<%=contextPath%>/jsp/validate_req_RequestDetail.jsp"></script>
    <script language="Javascript" src="<%=contextPath%>/jsp/validate_req_DoctorsNotesDetail.jsp"></script>
  </head>

  <%
  DoctorsNotesDetailForm actionForm = (DoctorsNotesDetailForm) request.getAttribute("detailForm");
  String lob = actionForm.getLob();
  String transmitInd = actionForm.getTransmitIndicator();
  String autoSettleInd = actionForm.getAutoSettleIndicator();
  String clientToSearch = actionForm.getClientToSearch();

  //current status
  String currentStatus = request.getParameter("currentStatus");
  if (currentStatus == null) {
    currentStatus = actionForm.getRequestStatus();
  }
  String isSuccessChangeStatus = request.getParameter("isSuccessChangeStatus");
  if ((isSuccessChangeStatus != null) && (isSuccessChangeStatus.equals("true"))) {
    currentStatus = actionForm.getRequestStatus();
  }

  //selected status
  String selectedStatus = request.getParameter("requestStatus");
  if (selectedStatus == null) {
    selectedStatus = actionForm.getRequestStatus();
  }

  //current assigned to
  String currentAssignedTo = actionForm.getAssignedTo();
  if (currentAssignedTo == null || (currentAssignedTo != null && currentAssignedTo.equals(""))) {
    currentAssignedTo = request.getParameter("currentAssignedTo");
  }

/*  Original current assigned to
  //current assigned to
  String currentAssignedTo = request.getParameter("currentAssignedTo");
  if (currentAssignedTo == null) {
    currentAssignedTo = actionForm.getAssignedTo();
  }
*/


  UserData ud = sessionHandler.getUserData(request);
  UserProfileData profile = ud.getProfile();
  String role = profile.getRole();
  String userId = profile.getUserId();
/*
  boolean isValid = false;
  if (userId.equals(currentAssignedTo)) {
    isValid = true;
  }
*/
  Roles userRoles = new Roles(); // Class that checks Roles of user contained in the array list

  boolean hasAccess = false;
  //disable save doctor's note button if role is not underwriter or medical staff
  if ((userRoles.getRoles(ud.getRoles(),IUMConstants.ROLES_MEDICAL_ADMIN)) ||
      (userRoles.getRoles(ud.getRoles(),IUMConstants.ROLES_MEDICAL_SUPERVISOR)) ||
      (userRoles.getRoles(ud.getRoles(),IUMConstants.ROLES_MEDICAL_CONSULTANT)) ||
      (userRoles.getRoles(ud.getRoles(),IUMConstants.ROLES_UNDERWRITER)) ||
      (userRoles.getRoles(ud.getRoles(),IUMConstants.ROLES_UNDERWRITER_SUPERVISOR))) {
    hasAccess = true;
  }
  else {
    CREATE_DOCTOR = "DISABLED";
  }

  //disable buttons (except create assessment request) if current status is an end status
  boolean isEndStatus = false;
  AssessmentRequest ar = new AssessmentRequest();
  String FORWARD_EXECUTE_ACCESS = EXECUTE_ACCESS;
  if (ar.isEndStatus(lob, Long.parseLong(currentStatus))) {
    
    MAINTAIN_ACCESS = "DISABLED";
    EXECUTE_ACCESS = "DISABLED";
    CREATE_DOCTOR = "DISABLED";
    isEndStatus = true;
  }
	// WMS resize
  	ResourceBundle rb = ResourceBundle.getBundle("wmsconfig");
  	String wmsContextPath = rb.getString("wms_context_path");
  
  	if(!contextPath.equalsIgnoreCase(wmsContextPath)){
  		if(Long.parseLong(currentStatus) != IUMConstants.STATUS_UNDERGOING_ASSESSMENT){
  			CREATE_DOCTOR = "DISABLED";
 		}
 	}
	
  boolean isMaintain = false;
  String reqIsMaintain = request.getParameter("isMaintain");
  if ((reqIsMaintain != null) && (reqIsMaintain.equals("true"))) {
    isMaintain = true;
  }

  //filter for request status
  String lob_status = lob + "," + currentStatus;
  
  
  
  
  String TABLE_WIDTH = "";
  String GRAY_WIDTH = "";
  if(contextPath.equalsIgnoreCase(wmsContextPath)){
  	TABLE_WIDTH = "700";
  	GRAY_WIDTH = "780";
  }else{
  	TABLE_WIDTH = "100%";
  	GRAY_WIDTH = "100%";
  }
  %>
  <body leftmargin="0" topmargin="0" onLoad="<% if (isMaintain) { %> enableRequestDetails(); enableTabDetails('cds'); setDateForwarded();initializeClientData('<%=clientToSearch%>'); <% } else { %> disableRequestDetails(); disableTabDetails('cds'); <% } %> <% if (!isEndStatus && hasAccess) { %> setDefaultFocus(); <% } %>" onUnload="closePopUpWin();">
    <form name="requestForm" method="post">
      <table border="0" cellpadding="0" cellspacing="0" height="100%" width="<%=TABLE_WIDTH%>">
        <tr>
          <td width="<%=TABLE_WIDTH%>" colspan="2">
            <jsp:include page="header.jsp" flush="true"/>
          </td>
        </tr>
        <tr>
          <td class="label2" rowspan="2" bgcolor="<%=iumColorO%>" height="100%" width="20"><img src="<%=contextPath%>/images/spacer.gif" width="20"></td>
          <td class="label2" background="<%=contextPath%>/images/GradientGray.jpg" height="27" width="<%=GRAY_WIDTH%>" valign="top">&nbsp;&nbsp;
            <span class="main">Integrated Underwriting and Medical System</span>
          </td>
        </tr>
        <tr>
          <td width="<%=TABLE_WIDTH%>" height="100%">
              <html:errors/>
              <!-- Status List START -->
              <table border="0" cellpadding="5" cellspacing="0">
                <tr>
                  <!--  <td class="label2" align="left"><ium:list className="label2" listBoxName="requestStatus" type="<%=IUMConstants.LIST_REQUEST_STATUS%>" styleName="width:200" selectedItem="<%=selectedStatus%>" onChange="setAssignTo();" filter="<%=lob_status%>"/></td>-->
                  <td>&nbsp;</td>
                  <td class="label2" align="left" id="maintain_button"><input class="button1" type="button" name="btnMaintain" value="Maintain" onClick="maintainRequest('doctor');" <%= MAINTAIN_ACCESS%>></td>
                  <td class="label2" align="left" id="create_button"><input class="button1" type="button" name="btnCreate" value="Create" onClick="window.location = 'createAssessmentRequest.do';" <%= CREATE_ACCESS%>></td>
                  <td class="label2" align="left" id="save_button"><input class="button1" type="button" name="btnSave" value="Save" onClick="saveRequest();" <%=MAINTAIN_ACCESS%>></td>
                  <td class="label2" align="left" id="cancel_button"><input class="button1" type="button" name="btnCancel" value="Cancel" onClick="cancelRequest('doctor');" <%=MAINTAIN_ACCESS%>></td>
                  <td class="label2" align="left"><input class="button1" type="button" name="btnUpdateLocation" value="Update Location" onClick="gotoPage('requestForm', 'updateLocation.do');" <%=EXECUTE_ACCESS%>></td>
            
                  <input type="hidden" name="updateType" value="<%=IUMConstants.PROCESS_TYPE_SINGLE%>">
                <% if ((actionForm.getSourceSystem() != null) && (transmitInd != null && transmitInd.equals(IUMConstants.INDICATOR_FAILED))) {%>
                <td class="label2" align="left"><input class="button1" type="button" name="btnRetrieveCDS" value="Retrieve CDS" onClick="retrieveCDS();" <%=MAINTAIN_ACCESS%>></td>
                <% }%>
                <%-- if (autoSettleInd != null && autoSettleInd.equals(IUMConstants.INDICATOR_FAILED)) {--%>
                <% if (autoSettleInd != null) {
                	  String autoSettleButton = "";
                      if (autoSettleInd.equals(IUMConstants.INDICATOR_SUCCESS)) {
							autoSettleButton = "DISABLED";
                      }
                %>
                <td class="label2" align="left"><input class="button1" type="button" name="btnAutoSettle" value="Auto-Settle" onClick="autoSettle();" <%=autoSettleButton%>></td>
                <% }%>
				<% if ((Long.parseLong(currentStatus) == IUMConstants.STATUS_APPROVED) ||
                       (Long.parseLong(currentStatus) == IUMConstants.STATUS_AR_CANCELLED) ||
                       (Long.parseLong(currentStatus) == IUMConstants.STATUS_DECLINED) ||
					   (Long.parseLong(currentStatus) == IUMConstants.STATUS_FOR_OFFER) ||
                       (Long.parseLong(currentStatus) == IUMConstants.STATUS_NOT_PROCEEDED_WITH)) { %>
                <td class="label2" align="left"><input class="button1" type="button" name="btnForward" value="Forward to Records" onClick="forwardToRecords();"  <%=FORWARD_EXECUTE_ACCESS%>></td>
				<%}%>
                </tr>
            <% if (autoSettleInd != null) {
            	  String autoSettleMsg = "";
                  if (autoSettleInd.equals(IUMConstants.INDICATOR_SUCCESS)) {
					autoSettleMsg = "Auto-settle Successful";
                  } else if (autoSettleInd.equals(IUMConstants.INDICATOR_FAILED)){
                	autoSettleMsg = "Auto-settle Not Successful:";
                  }
            %>
            <tr><td class="error"><%=autoSettleMsg%>&nbsp;<bean:write name="detailForm" property="autoSettleRemarks"/></td></tr>
			<% } %>
              </table>
              <!-- Status List END -->

              <!-- hidden field to store current status -->
              <input type="hidden" name="currentStatus" value="<%=currentStatus%>">

              <!-- hidden field to store remarks, UW analysis, UW remarks -->
              <input type="hidden" name="remarks" value="<bean:write name="detailForm" property="remarks"/>">
              <input type="hidden" name="UWAnalysis" value="<bean:write name="detailForm" property="UWAnalysis"/>">
              <input type="hidden" name="UWRemarks" value="<bean:write name="detailForm" property="UWRemarks"/>">

              <!-- hidden field to store maintain flag for request details -->
              <input type="hidden" name="isMaintain" value="<%=isMaintain%>">

              <!-- hidden field to store success flag for change status in request details -->
              <input type="hidden" name="isSuccessChangeStatus" value="false">

              <!-- hidden field to store the currently assigned to user -->
              <input type="hidden" name="currentAssignedTo" value="<%=currentAssignedTo%>">

              <!-- hidden field to store the tab/page of the request detail -->
              <input type="hidden" name="reqPage" value="doctorsNotesDetail">

              <table border="0" cellpadding="10" cellspacing="0" height="100%" width="100%">
                <tr>
                  <td>
                    <%
                    String _sourceSystem = actionForm.getSourceSystem();
                    if (_sourceSystem == null) {
                      _sourceSystem = "";
                    }

                    String _agentCode = actionForm.getAgentCode();
                    if (_agentCode == null) {
                      _agentCode = "";
                    }

                    String _lob = actionForm.getLob();
                    if (_lob == null) {
                      _lob = "";
                    }

                    String _branchCode = actionForm.getBranchCode();
                    if (_branchCode == null) {
                      _branchCode = "";
                    }

                    String _assignedTo = currentAssignedTo; //actionForm.getAssignedTo();
                    if (_assignedTo == null) {
                      _assignedTo = "";
                    }

                    String _location = actionForm.getLocation();
                    if (_location == null) {
                      _location = "";
                    }

                    String _underwriter = actionForm.getUnderwriter();
                    if (_underwriter == null) {
                      _underwriter = "";
                    }

                    String _insuredClientNo = actionForm.getInsuredClientNo();
                    if (_insuredClientNo == null) {
                      _insuredClientNo = "";
                    }

                    String _insuredSex = actionForm.getInsuredSex();
                    if (_insuredSex == null) {
                      _insuredSex = "";
                    }

                    String _ownerClientNo = actionForm.getOwnerClientNo();
                    if (_ownerClientNo == null) {
                      _ownerClientNo = "";
                    }

                    String _ownerSex = actionForm.getOwnerSex();
                    if (_ownerSex == null) {
                      _ownerSex = "";
                    }

                    String _status = currentStatus; //actionForm.getRequestStatus();
                    if (_status == null) {
                      _status = "";
                    }

                    String _selectedStatus = selectedStatus;
                    if (_selectedStatus == null) {
                      _selectedStatus = "";
                    }
                    %>
                    <jsp:include page="requestDetail_include.jsp" flush="true">
                      <jsp:param name="sourceSystem" value = "<%=_sourceSystem%>"/>
                      <jsp:param name="agentCode" value = "<%=_agentCode%>"/>
                      <jsp:param name="lob" value = "<%=_lob%>"/>
                      <jsp:param name="branchCode" value = "<%=_branchCode%>"/>
                      <jsp:param name="assignedTo" value = "<%=_assignedTo%>"/>
                      <jsp:param name="location" value = "<%=_location%>"/>
                      <jsp:param name="underwriter" value = "<%=_underwriter%>"/>
                      <jsp:param name="insuredClientNo" value = "<%=_insuredClientNo%>"/>
                      <jsp:param name="insuredSex" value = "<%=_insuredSex%>"/>
                      <jsp:param name="ownerClientNo" value = "<%=_ownerClientNo%>"/>
                      <jsp:param name="ownerSex" value = "<%=_ownerSex%>"/>
                      <jsp:param name="status" value = "<%=_status%>"/>
                      <jsp:param name="selectedStatus" value = "<%=_selectedStatus%>"/>
                    </jsp:include>
                  </td>
                </tr>
                <tr>
                  <td width="100%" height="100%" valign="top" width="<%=TABLE_WIDTH%>">
                    <table cellpadding="1" cellspacing="0">
                      <tr>
                      <!--<% if (INQUIRE_REQUEST) {%>
                        <td bgcolor="#BBBBBB" style="cursor:hand" onmouseover="this.style.backgroundColor = '#2A4C7C'" onmouseout="this.style.backgroundColor = '#BBBBBB'">
                          <table border="0" cellpadding="3" cellspacing="0">
                            <tr>
                              <tr><a href="javascript:gotoPage('requestForm', 'viewRequestDetail.do');"><td class="tabLink"><b>Request Details</b></td></a></tr>
                            </tr>
                          </table>
                        </td>
                        <td></td>
                        <% } %>-->
                        <% if (INQUIRE_CDS) {%>
                        <td bgcolor="#BBBBBB" style="cursor:hand" onmouseover="this.style.backgroundColor = '<%=iumColorB%>'" onmouseout="this.style.backgroundColor = '#BBBBBB'">
                          <table border="0" cellpadding="3" cellspacing="0">
                            <tr><a href="javascript:gotoPage('requestForm', 'viewCDSDetail.do');"><td class="tabLink"><b>CDS Details-KO & Med/Lab, UW Assessment and Requirements</b></td></a></tr>
                          </table>
                        </td>
                        <td></td>
                        <% } %>
                        <!--  <% if (INQUIRE_KOREQRMT) {%>
                        <td bgcolor="#BBBBBB" style="cursor:hand" onmouseover="this.style.backgroundColor = '#2A4C7C'" onmouseout="this.style.backgroundColor = '#BBBBBB'">
                          <table border="0" cellpadding="3" cellspacing="0">
                            <tr><a href="javascript:gotoPage('requestForm', 'viewKOReqMedLabDetail.do');"><td class="tabLink"><b>Requirements</b></td></a></tr>
                          </table>
                        </td>
                        <td></td>
                        <% } %>
                        <% if (INQUIRE_UWASSESS) {%>
                        <td bgcolor="#BBBBBB" style="cursor:hand" onmouseover="this.style.backgroundColor = '#2A4C7C'" onmouseout="this.style.backgroundColor = '#BBBBBB'">
                          <table border="0" cellpadding="3" cellspacing="0">
                            <tr><a href="javascript:gotoPage('requestForm', 'viewUWAssessmentDetail.do');"><td class="tabLink"><b>U/W Assessment</b></td></a></tr>
                          </table>
                        </td>
                        <td></td>
                        <% } %>-->

                        <td bgcolor="<%=iumColorB%>">
                          <table border="0" cellpadding="3" cellspacing="0">
                            <tr><td class="tabSelected"><b>Doctor's Notes</b></td></tr>
                          </table>
                        </td>
                        <td></td>
                      </tr>
                    </table>
                    <!-- START OF CONTENT -->
                    <table cellpadding="0" cellspacing="0" border="1" bordercolor="<%=iumColorB%>" width="<%=TABLE_WIDTH%>">
                      <tr>
                        <td class="label2" bgcolor="<%=iumColorB%>" colspan="11">&nbsp;</td>
                      </tr>
                      <tr>
                        <td>
                          <table cellpadding="10" cellspacing="0" border="0" width="<%=TABLE_WIDTH%>">
                            <tr>
                              <td>
                                <table border="0" cellpadding="2" cellspacing="0" width="<%=TABLE_WIDTH%>">
                                  <tr><td class="label2"><span class="required">*</span><b>Inputs and Discussions:</b></td></tr>
                                  <tr><td class="label2">&nbsp;</td></tr>
                                  <tr><td class="label2"><textarea rows="8" cols="80" name="notes" <%=CREATE_DOCTOR%> onkeyup="this.value=this.value.slice(0, 255);"></textarea></td></tr>
                                  <tr><td class="label2">&nbsp;</td></tr>
                                  <tr><td class="label2" align="left"><input class="button1" type="button" name="save" value="Save Note" onClick="saveMedicalNote()" <%=CREATE_DOCTOR%>></td></tr>
                                  <tr><td class="label2">&nbsp;</td></tr>
                                  <tr>
                                    <td class="label2">
                                      <%
                                      if (((userRoles.getRoles(ud.getRoles(),IUMConstants.ROLES_UNDERWRITER)) ||
                                           (userRoles.getRoles(ud.getRoles(),IUMConstants.ROLES_UNDERWRITER_SUPERVISOR))) &&
                                          !isEndStatus && hasAccess) {
                                        String docFilter = IUMConstants.ROLES_MEDICAL_ADMIN + "," + IUMConstants.ROLES_MEDICAL_SUPERVISOR + "," + IUMConstants.ROLES_MEDICAL_CONSULTANT;
                                      %>
                                      <span class="required">*</span><b>Medical examiner to notify : </b><ium:list className="label2" listBoxName="doctor" type="<%=IUMConstants.LIST_ASSIGN_TO%>" filter="<%=docFilter%>" />
                                      <% } %>
                                    </td>
                                  </tr>
                                  <tr>
                                    <td class="label2">
                                      <table border="0" cellpadding="3" cellspacing="0" bgcolor="#FFFFFF" style="border: 1px solid #D1D1D1;" class="listtable1" ID="Table1" width="<%=TABLE_WIDTH%>">
                                        <% int i=0; %>
                                        <logic:iterate id="medicalNote" name="detailForm" property="discussionList">
                                        <tr>
                                          <td class="headerrow1" width="35%"><div align="left">Posted By: <bean:write name="medicalNote" property="postedBy"/></div></td>
										  <td class="headerrow1" width="35%"><div align="left">Addressed To: <bean:write name="medicalNote" property="recipient"/></div></td>
                                          <td class="headerrow1"><div align="right">Posted On: <bean:write name="medicalNote" property="datePosted"/></div></td>
                                        </tr>
                                        <tr>
                                          <td class="row1" colspan="3"><bean:write name="medicalNote" property="notes" filter="false"/></td>
                                        </tr>
                                        <% i++; %>
                                        </logic:iterate>

                                        <bean:size id="noOfPages" name="page" property="pageNumbers" />
                                        <% if (i == 0) { %>
                                        <tr>
                                          <td class="label2" colspan="3"><bean:message key="message.noexisting" arg0="doctor's notes"/></td>
                                        </tr>

                                        <% } else if (noOfPages.intValue() > 1) { %>
                                        <tr>
                                          <td class="headerrow4" width="100%" height="10" class="label2" valign="bottom" colspan="14">
                                          <%
                                          int pageNumber = Integer.parseInt((String)request.getAttribute("pageNo"));
                                          String viewPage = "viewDoctorsNotes.do";
                                          int currLink = 1;
                                          int prevLink = 1;
                                          int firstPage = 1;
                                          //int lastPage = noOfPages.intValue();

                                          Page displayPage = (Page) request.getAttribute("page");
                                          int lastPage = 0;
                                          ArrayList pageNos = (ArrayList)displayPage.getPageNumbers();
                                          if (pageNos.size() > 0) {
                                            lastPage = ((Integer)pageNos.get(noOfPages.intValue()-1)).intValue();
                                          }
                                          %>

                                          <!-- don't display link for previous page if the current page is the first page -->
                                          <% if (pageNumber > firstPage) { %>
                                            <a href="#" onClick="javascript:rePaginate(1,'<%=viewPage%>');"><img src="<%=contextPath%>/images/pagnavfirst_off.gif" name="pagnavfirst" border="0" width="18" height="13"/></a>&nbsp;
                                            <a href="#" onClick="javascript:rePaginate('<%=pageNumber-1%>','<%=viewPage%>');"><img src="<%=contextPath%>/images/pagnavleft_off.gif" name="pagnavleft" border="0" width="18" height="13"/></a>
                                          <% } else {%>
                                            <img src="<%=contextPath%>/images/pagnavfirst_on.gif" name="pagnavfirst" border="0" width="18" height="13"/>&nbsp;
                                            <img src="<%=contextPath%>/images/pagnavleft_on.gif" name="pagnavleft" border="0" width="18" height="13"/>
                                          <% } %>

                                          <logic:iterate id="navLinks" name="page" property="pageNumbers">
                                            <% currLink = ((Integer)navLinks).intValue();%>
                                            <% if (((Integer)navLinks).intValue() == pageNumber) { %>
                                              <b><bean:write name="navLinks"/></b>
                                            <% } else { %>
                                              <% if ((currLink > (prevLink+1))) { %>
                                              ...
                                              <% } %>
                                              <a href="#" onClick="rePaginate('<bean:write name="navLinks"/>','<%=viewPage%>');" class="links2"><bean:write name="navLinks"/></a>
                                            <% } %>
                                            <%prevLink = currLink;%>
                                          </logic:iterate>

                                          <!-- don't display link for next page if the current page is the last page -->
                                          <% if(pageNumber < lastPage) { %>
                                            <a href="#" onClick="rePaginate('<%=pageNumber+1%>','<%=viewPage%>');"><img src="<%=contextPath%>/images/pagnavright_off.gif" name="pagnavright" border="0" width="18" height="13"/></a>&nbsp;
                                            <a href="#" onClick="rePaginate('<%=prevLink%>','<%=viewPage%>');"><img src="<%=contextPath%>/images/pagnavlast_off.gif" width="18" height="13" border="0"></a>
                                          <% } else { %>
                                            <img src="<%=contextPath%>/images/pagnavright_on.gif" width="18" height="13" border="0">&nbsp;
                                            <img src="<%=contextPath%>/images/pagnavlast_on.gif" width="18" height="13" border="0">
                                          <% } %>
                                          </td>
                                        </tr>
                                        <% } %>
                                      </table>
                                    </td>
                                  </tr>
                                </table>
                              </td>
                            </tr>
                          </table>
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
                <tr>
                  <td colspan="10" class="label5">� 2003 Sun Life Financial. All rights reserved.</td>
                </tr>
                <tr>
                  <td colspan="10" class="label5">&nbsp;</td>
                </tr>
              </table>
              <!-- START OF CONTENT -->
            </td>
          </tr>
        </table>
      <input type="hidden" name="refNo" value="">
      <input type="hidden" name="pageNo" value="">
    </form>
  </body>
</html>