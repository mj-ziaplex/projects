<%@ page language="java" buffer="1024kb"%>
<!-- JAVA UTILS -->
<%@ page import="java.util.ResourceBundle, java.util.List, java.util.ArrayList"%>
<!-- IUM POJO -->
<%@ page import="com.slocpi.ium.ui.form.KOReasonsForm, com.slocpi.ium.ui.form.CDSDetailForm"%>

<%
	
	String contextPath = request.getContextPath();
%>
<%
	/* Andre Ceasar Dacanay - css enhancement - start */
	String iumCss = "";
	String iumBgColor = "";

	String companyCode = "";
	try {
		ResourceBundle rbWMSSP = ResourceBundle.getBundle("dbconfig");
		iumCss = rbWMSSP.getString("IUM_CSS_VERSION");
		companyCode = iumCss;
		iumCss = "_" + iumCss;
	} catch (Exception e) {
	}

	if (iumCss.equals("_CP")) {
		iumBgColor = "bgcolor=\"#2a4c7c\"";
		companyCode = "";
	} else if (iumCss.equals("_GF")) {
		iumBgColor = "bgcolor=\"#303A77\"";;
		companyCode = companyCode + " ";
	} else {
		iumBgColor = "bgcolor=\"#2a4c7c\";";
	}
	/* Andre Ceasar Dacanay - css enhancement - end */
%>

<%
	//WMS session expiration = close browser
	ResourceBundle rb1 = ResourceBundle.getBundle("wmsconfig");
	String wmsContextPath1 = rb1.getString("wms_context_path");
	String TABLE_WIDTH = "";
	
	if (contextPath.equalsIgnoreCase(wmsContextPath1)) {
		TABLE_WIDTH = "570";
	} else {
		TABLE_WIDTH = "570";
	}
%>
<table border="0" width="<%=TABLE_WIDTH%>" cellpadding="2" cellspacing="1" bgcolor=#FFFFFF style="border: 1px solid #D1D1D1;" class="listtable1">
	<tr>
		<td class="headerrow6" width="5%">SEQ</td>
		<td class="headerrow6" width="22%">MESSAGE TEXT-POLICY ID-COVERAGE NUMBER</td>
		<td class="headerrow6" width="5%">CLIENT NO</td>
		<td class="headerrow6" width="10%">FAIL RESPONSE</td>
	</tr>
	
	<%
	int i = 0;
	String tr_class;
	
	// Used for iteration in ko reasons
	CDSDetailForm cdsForm = (CDSDetailForm) request.getAttribute("detailForm");
	ArrayList koReasons = cdsForm.getKoReasons();
	KOReasonsForm tmp;
	
	for (int x = 0; x < koReasons.size(); x++) {

		if (x % 2 == 0) {
			tr_class = "row1";
		} else {
			tr_class = "row2";
		}
		
		tmp = (KOReasonsForm) koReasons.get(x);
		%>
		<tr>
			<td class="<%=tr_class%>"><%=tmp.getSeq()%></td>
			<td class="<%=tr_class%>"><%=tmp.getMessageText()%></td>
			<td class="<%=tr_class%>"><%=tmp.getClientNo()%></td>
			<td class="<%=tr_class%>"><%=tmp.getFailResponse() == null ? "" : tmp.getFailResponse()%></td>
		</tr>
		<%i++;%>
	<%}%>
	
	<% if (i == 0) { %>
	<tr>
		<td class="label2" colspan="4">No Records Found.</td>
	</tr>
	<% } %>
</table>
<!--   </body> -->
<!-- </html> -->
