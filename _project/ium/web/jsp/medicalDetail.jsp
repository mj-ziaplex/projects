<jsp:useBean id="medicalRecordDetailForm" scope="request" class="com.slocpi.ium.ui.form.MedicalRecordDetailForm"/>
<%@ page language="java" import="java.util.*,com.slocpi.ium.data.*, com.slocpi.ium.ui.form.*, com.slocpi.ium.util.*, com.slocpi.ium.ui.util.*, com.slocpi.ium.underwriter.*, java.text.* " buffer="12kb"%>
<%@ taglib uri="/struts-html.tld" prefix="html" %>
<%@ taglib uri="/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/ium.tld" prefix="ium" %>
<%@page import="com.slocpi.ium.util.DateHelper"%>
<%@page import="com.slocpi.ium.util.IUMConstants"%>
<%@ page import="java.util.*" %>
<%@ page import="com.slocpi.ium.data.*" %>
<%String contextPath = request.getContextPath();
StateHandler sessionHandler = new StateHandler();
Access access = new Access();
%>
<%
/* Andre Ceasar Dacanay - css enhancement - start */
String iumCss = "";
String iumColorB = "";
String iumColorO = "";
String companyCode = "";
try {
	ResourceBundle rbWMSSP = ResourceBundle.getBundle("dbconfig");
	iumCss = rbWMSSP.getString("IUM_CSS_VERSION");
	companyCode = iumCss;
	iumCss = "_"+iumCss;
} catch (Exception e){}
if(iumCss.equals("_CP")){
	iumColorB="#2A4C7C";
	iumColorO = "#FFCB00";
	companyCode="";
}else if(iumCss.equals("_GF")){
	iumColorB="#303A77";
	iumColorO = "#303A77";
	companyCode = companyCode+" ";
}else{
	iumColorB="#2A4C7C";
	iumColorO = "#FFCB00";
}
/* Andre Ceasar Dacanay - css enhancement - end */
%>
<%
    // This routine queries the access rights of the users (RCRIVERA)
    //
    // --- START --- 
    
	HashMap userAccess = sessionHandler.getUserAccess(request);
    if(!access.hasAccess(userAccess,IUMConstants.PAGE_MEDRECORD_DETAIL,IUMConstants.ACC_INQUIRE) ){                                                                                                                    
    	response.sendRedirect("noAccessPage.jsp");   
    } 
    
    String CREATE_ACCESS = "";
    String MAINTAIN_ACCESS = "";
   
    
    if(!access.hasAccess(userAccess,IUMConstants.PAGE_MEDRECORD_DETAIL,IUMConstants.ACC_CREATE) ){                                                                                                                     
	  CREATE_ACCESS = "DISABLED"; 	
    }
    if(!access.hasAccess(userAccess,IUMConstants.PAGE_MEDRECORD_DETAIL,IUMConstants.ACC_MAINTAIN) ){                                                                                                                     
	  MAINTAIN_ACCESS = "DISABLED";
    }


// will disable the maintain button if status of the medical record is an end status
	String currentStatus = medicalRecordDetailForm.getStatus();
	String ar_lob = (String)request.getAttribute("ar_lob");
	MedicalLabRecord medRecord = new MedicalLabRecord();
	if (medRecord.isEndStatus(ar_lob, Long.parseLong(currentStatus))) {
    	MAINTAIN_ACCESS = "DISABLED";
	}
    
    
    // --- END ---
 %>    
<html>
	<head>
    	<title><%=companyCode%>Integrated Underwriting and Medical System</title>
    	<link href="<%=contextPath%>/css/main<%=iumCss%>.css" rel="stylesheet" type="text/css">
    	<script language="Javascript" src="js/navigation.js"></script>
    	<script language="Javascript" src="js/validation.js"></script>
    	<script language="Javascript" src="jsp/validate_medical_detail.jsp">    </script>
  	</head>
  	<body leftmargin="0" topmargin="0" onload="initFields()" onUnload="closePopUpWin();">
    	<form name="frm" method="post">
    		<input type="hidden" name="actionType" value="<%=medicalRecordDetailForm.getActionType()%>">
    		<input type="hidden" name="labTestInd" value="<%=medicalRecordDetailForm.getLabTestInd()%>">
    		<%if (request.getAttribute("refNo") == null){%>
    			<input type="hidden" name="medicalRecordId" value="<%=request.getParameter("refNo")%>">    
    		<%}else{%>
    			<input type="hidden" name="medicalRecordId" value="<%=(String)request.getAttribute("refNo")%>">    
    		<%}%>
    		<input type="hidden" name="prevStatus" value="<%=medicalRecordDetailForm.getPrevStatus()%>">    
    		<input type="hidden" name="referenceNum" value="<%=medicalRecordDetailForm.getRefNo()%>">
    		<input type="hidden" name="err" value="<%=medicalRecordDetailForm.getErr()%>">
    		<input type="hidden" name="lob" value="<%=medicalRecordDetailForm.getLob()%>">
    
      		<font face="verdana">
        		<table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%">
          			<tr>
            			<td width="100%" colspan="2">
              				<jsp:include page="header.jsp" flush="true"/>
            			</td>
          			</tr>
          			<tr>
            			<td rowspan="2" bgcolor="<%=iumColorO%>" height="100%" width="20"><img src="images/spacer.gif" width="20"></td>
            			<td background="images/GradientGray.jpg" height="27" width="100%" valign="top">&nbsp;&nbsp;
              				<span class="main">Integrated Underwriting and Medical System</span>
            			</td>
          			</tr> 
          			<tr>
            			<td width="100%" height="100%" valign="top">
				            <%
				          	ArrayList list = (ArrayList)medicalRecordDetailForm.getMedicalRecordData();
				          	MedicalRecordData medicalRecordData = (MedicalRecordData)list.get(0);
				          	%>

			              	<!-- BODY -->
	            		  	<table  cellpadding="10" cellspacing="0" border="0" width="700">
	            				<tr>
                					<td colspan="2" class="error"><html:errors/></td>
              					</tr>
	            				<tr>
	            					<td  width="35%" valign="bottom" class=label2><b>Status</b></td>
	            					<td  width="35%" valign="bottom" class=label2><b>Facilitator</b></td>
	            					<td  width="30%" valign="bottom" class=label2></td>
	            				</tr>
			        			<%
			        			String refNo ="";
				    	        if (request.getAttribute("refNo") == null)
									refNo = request.getParameter("refNo");
							 	else
									refNo = (String)request.getAttribute("refNo");%>
									
								<%								
								String lob = "null";
								String stat = "null";								
								if (!medicalRecordDetailForm.getLob().equals(""))								
									lob = medicalRecordDetailForm.getLob();
								if (!medicalRecordDetailForm.getPrevStatus().equals(""))
									stat = medicalRecordDetailForm.getPrevStatus();
								String lob_status = lob+","+stat;
								%>
								<input type="hidden" name="lobStatus" value="<%=lob_status%>">
	            				<tr>
									<td  width="35%" valign="top" class=label2>							
										<%if (medicalRecordDetailForm.getActionType().equalsIgnoreCase("create")){%>
					    					<ium:list className="label2" listBoxName="status" type="<%=IUMConstants.LIST_MEDICAL_RECORD_STATUS%>" styleName="width:200" selectedItem="<%=medicalRecordDetailForm.getStatus()%>" onChange="onChangeStatus()"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					    				<%}else{%>
					    					<ium:list className="label2" listBoxName="status" type="<%=IUMConstants.LIST_MEDICAL_RECORD_STATUS%>" styleName="width:200" selectedItem="<%=medicalRecordDetailForm.getStatus()%>" onChange="onChangeStatus()" filter="<%=lob_status%>"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;		    		    
					    				<%}%>
					    			</td>
					    			<td  width="35%" valign="top" class=label2>							
								    	<%
								    	String facilitator = "";
								    	if (medicalRecordData.getFacilitator() != null)
								    	    facilitator = medicalRecordData.getFacilitator();%>
					           			<ium:list className="label2" listBoxName="facilitator" type="<%=IUMConstants.LIST_USERS%>" filter="<%=IUMConstants.ROLES_FACILITATOR%>" styleName="width:200" selectedItem="<%=facilitator%>"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				    				</td>
				    				<td  width="30%" valign="top" class=label2 >
			    	        			<div id="btnGrp1" style="visibility:visible; position:absolute;">                        
				              				<input type="button" class="button1" name="maintain" value="Maintain" <%=MAINTAIN_ACCESS%> onClick="gotoPage('frm', 'viewMedicalDetail.do?actionType=maintain&refNo=<%=refNo%>');">
				              				<input type="button" class="button1" name="create" value="Create" <%=CREATE_ACCESS%> onClick="gotoPage('frm', 'createMedicalExam.do');">&nbsp;
				            			</div>
				            			<div id="btnGrp2" style="visibility:visible; position:absolute;">
				   		    				<input type="button" class="button1" name="save" value="Save" style="visibility:hidden" onClick="saveMedicalExam();">&nbsp;
				                			<input type="button" class="button1" name="cancel" value="Cancel" style="visibility:hidden" onClick="cancelAction('<%=contextPath%>/listMedicalRecords.do');"> 
				            			</div>
				        			</td>
								</tr>
			  				</table>
			  	
              				<table border="0" cellpadding="10" cellspacing="0" height="100%" width="100%">
                				<tr>
                  					<td width="100%" height="100%" valign="top" class=label2 ><b>Medical Record Detail</b>
                    					<table cellpadding="0" cellspacing="0" border="1" bordercolor="<%=iumColorB%>" width="700">
                      						<tr>
                        						<td>
                          							<table cellpadding="10" cellspacing="0" border="0" width="650">

                          								<logic:iterate id="medicalData" name="medicalRecordDetailForm" property="medicalRecordData">

                            							<tr>
                              								<td valign="top">
                                								<table border="0" bordercolor="<%=iumColorB%>" cellpadding="0" cellspacing="0" bgcolor="" width="100%">
                                  									<tr>
                                    									<td height="100%" valign="top" align="left">
                                      										<table border="1" bordercolor="<%=iumColorB%>" cellpadding="10" cellspacing="0" bgcolor="" width="700">
                                        										<tr>
                                          											<td height="100%" valign="top">
                                            											<table border="0" cellpadding="2" cellspacing="0">
                                              												<tr>
                                                												<td valign="top">
                                                  													<table border="0" cellpadding="2" cellspacing="0">
                                                    													<tr>
                                                      														<td class="label2"><b>&nbsp;&nbsp;Reference No.</b></td>
                                                      														<td>&nbsp;&nbsp;</td>
                                                      														<%
														                                                    PolicyMedicalRecordData policyMedicalRecordData = medicalRecordData.getPolicyMedicalRecordData();
													                                                    	if (policyMedicalRecordData == null){
													                                                      		policyMedicalRecordData = new PolicyMedicalRecordData();
													                                                      		policyMedicalRecordData.setReferenceNumber("");
													                                                      	}else{
													                                                      		if (policyMedicalRecordData.getReferenceNumber() == null)
													                                                      			policyMedicalRecordData.setReferenceNumber("");
													                                                      	}%>
                                                      														<td><input type="text" name="refNo" maxlength=15 class="label2" value='<%=policyMedicalRecordData.getReferenceNumber()%>' onblur="callCreateAction();"></td>
                                                      														<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                                      														<td class="label2"><span class="required">*</span><b>Agent</b></td>
                                                      														<td>&nbsp;&nbsp;</td>
																											<%
															                                                SunLifeOfficeData sunLifeOfficeData = medicalRecordData.getBranch();
															                                                if (sunLifeOfficeData == null){
															                                                	sunLifeOfficeData = new SunLifeOfficeData();
															                                                    sunLifeOfficeData.setOfficeId("");
															                                                }else{
															                                                	if (sunLifeOfficeData.getOfficeId() == null)
															                                                    	sunLifeOfficeData.setOfficeId("");
															                                                }%>

														                                                    <%
														                                                    UserProfileData userProfileData = medicalRecordData.getAgent();
														                                                    if (userProfileData == null){
														                                                    	userProfileData = new UserProfileData();
														                                                      	userProfileData.setUserId("");
														                                                    }else{
														                                                    	if (userProfileData.getUserId() == null)
														                                                      		userProfileData.setUserId("");
														                                                    };
																											if (userProfileData.getUserId().equals("") && medicalRecordDetailForm.getActionType().equalsIgnoreCase("view")){%>
																											  	<td><font size="1"><input type="text" name="agent" class="label2" value=''></font></td> 														                                                     
																											<%}else{%>
														                                                   	  	<td><font size="1"><ium:list className="label2" listBoxName="agent" type="<%=IUMConstants.LIST_BRANCH_AGENTS%>" filter="<%=sunLifeOfficeData.getOfficeId()%>" styleName="width:200" selectedItem="<%=userProfileData.getUserId()%>"  onChange="refreshAgentOrBranch(1);"/></font></td> 														                                                     
														                                                   	<%}%>
                                                    													</tr>
                                                    													<tr>
                                                      														<td class="label2"><b>&nbsp;&nbsp;Client No.</b></td>
														                                                    <td>&nbsp;</td>
														                                                    <%
														                                                    ClientData clientData = medicalRecordData.getClient();
														                                                    if (clientData == null){
														                                                    	clientData = new ClientData();
														                                                      	clientData.setClientId("");
														                                                      	clientData.setLastName("");
														                                                      	clientData.setGivenName("");
														                                                    }else{
														                                                      	if (clientData.getClientId() == null)
														                                                      		clientData.setClientId("");
														                                                      	if (clientData.getLastName() ==  null)
														                                                      		clientData.setLastName("");
														                                                      	if (clientData.getGivenName() == null)
														                                                      		clientData.setGivenName("");
														                                                    };
														                                                    if (clientData.getClientId().equals("") && medicalRecordDetailForm.getActionType().equalsIgnoreCase("view")){%>                                                                                                          
														                                                      	<td><font size="1"><input type="text" name="clientNo" maxlength=10 class="label2" value=''></font></td>
														                                                    <%}else{%>
														                                                    	<td><font size="1"><input type="text" name="clientNo" maxlength=10 class="label2" value='<%=clientData.getClientId()%>' onblur="callCreateAction();"></font></td>
															                                                <%}%>
														                                                    <td>&nbsp;</td>
														                                                    <td class="label2"><span class="required">*</span><b>Branch</b></td>
														                                                    <td>&nbsp;</td>
															                                                <%if (sunLifeOfficeData.getOfficeId().equals("") && medicalRecordDetailForm.getActionType().equalsIgnoreCase("view")){%>
															                                                	<td><font size="1"><input type="text" name="branch" class="label2" value=''></font></td>
															                                                <%}else{%>
														                                                    	<td><font size="1"><ium:list className="label2" listBoxName="branch" type="<%=IUMConstants.LIST_BRANCHES%>" styleName="width:200" selectedItem="<%=sunLifeOfficeData.getOfficeId()%>"  onChange="refreshAgentOrBranch(2);"/></font></td>
														                                                    <%}%>
														                                               	</tr>
													                                                    <tr>
													                                                     	<td class="label2"><span class="required">*</span><b>Last Name</b></td>
													                                                      	<td>&nbsp;</td>
													                                                      	<td><font size="1"><input type="text" name="lastName" maxlength=40 class="label2" value='<%=clientData.getLastName()%>'></font></td>
													                                                      	<td>&nbsp;</td>
													                                                      	<td class="label2"><span class="required">*</span><b>Birth Date</b></td>
													                                                      	<td>&nbsp;</td>       
													                                                      	<%if (clientData == null || clientData.getBirthDate() == null){%>                                                      
														                                                     	<td><font size="1"><input type="text" name="birthDate" class="label2" value='' onKeyUp="getKeyDate(event,this);"></font>
														                                                     	<div id="doc_cal1" style="display:none;position:absolute;"><a href="#" onClick="popUpWin('<%=contextPath%>/jsp/calendar.jsp?textbox=frm.birthDate',290,155);"><img src="<%=contextPath%>/images/calendar.gif" height="17" width="19" border="0">
																										        </a></div>
														                                                     	</td>
														                                                  	<%}else{%>	                                                  
													                                                      		<td><font size="1"><input type="text" name="birthDate" class="label2" value='<%=(DateHelper.format(clientData.getBirthDate(),"ddMMMyyyy")).toUpperCase()%>' onKeyUp="getKeyDate(event,this);"></font>
													                                                      		<div id="doc_cal1" style="display:none;position:absolute;"><a href="#" onClick="popUpWin('<%=contextPath%>/jsp/calendar.jsp?textbox=frm.birthDate',290,155);"><img src="<%=contextPath%>/images/calendar.gif" height="17" width="19" border="0">
																										        </a></div>
													                                                      		</td>
													                                                      	<%}%>
													                                                    </tr>
													                                                    <tr>
													                                                    	<td class="label2"><span class="required">*</span><b>First Name</b></td>
													                                                      	<td>&nbsp;</td>
													                                                      	<td><font size="1"><input type="text" name="firstName" maxlength=25 class="label2" value='<%=clientData.getGivenName()%>'"></font></td>
													                                                      	<td>&nbsp;</td>
													                                                      	<td><font size="1"><b>&nbsp;</b></font></td>
													                                                      	<td>&nbsp;</td>
													                                                      	<td><font size="1">&nbsp;</font></td>
													                                                    </tr>
                                                  													</table>
                                                												</td>
                                              												</tr>
                                            											</table>
                                          											</td>
                                        										</tr>
                                      										</table>
                                    									</td>
                                  									</tr>
                                								</table>
                              								</td>
                            							</tr>
                            
							                            <tr>
							                            	<td>
							                                	<table border="1" bordercolor="<%=iumColorB%>" cellpadding="10" cellspacing="0" bgcolor="" width="700">
							                                  		<tr>
							                                    		<td height="100%">
							                                      			<table border="0" cellpadding="2" cellspacing="0">
							                                        			<tr>
							                                          				<td valign="top">
							                                            				<table border="0" cellpadding="2" cellspacing="0">
							                                              					<tr>
							                                                					<td class="label2"><b>&nbsp;&nbsp;Laboratory</b></td>
							                                                					<td>&nbsp;&nbsp;</td>
							                                                					<td class="label2">
								                                                					<%if (medicalRecordData.getLabTestInd()==null){%>
								                                                						<input type="radio" name="type" value="Y" onclick="resetTest();enableLab()">Yes
																										<input type="radio" name="type" value="N" CHECKED onclick="resetTest();enableExaminer()">No
								                                                					<%}else{
									                                                					if((medicalRecordData.getLabTestInd()).equalsIgnoreCase("L")) {%>
																											<input type="radio" name="type" value="Y" CHECKED onclick="resetTest();enableLab()">Yes
																											<input type="radio" name="type" value="N" onclick="resetTest();enableExaminer()">No
																										<%}else{%>	 
																											<input type="radio" name="type" value="Y" onclick="resetTest();enableLab()">Yes
																											<input type="radio" name="type" value="N" CHECKED onclick="resetTest();enableExaminer()">No
																										<%}
																									}%>
							                                                					</td>
							                                                					<td>&nbsp;</td>
							                                                					<td class="label2"><b>&nbsp;&nbsp;Remarks</b></td>
							                                                					<td>&nbsp;</td>
							                                                					<td rowspan="2" valign="top">
							                                                  						<font size="1"><textarea name="remarks" class="label2" cols="26" rows="4"><bean:write name="medicalData" property="remarks"/></textarea></font>
							                                                					</td>
							                                              					</tr>
							                                              
							                                              					<tr>                                              
							                                                					<td class="label2" ><span class="required">*</span><b>Examiner/<br>&nbsp;&nbsp;Laboratory Name</b></td>
							                                                					<td>&nbsp;</td>
							                                                					<td valign="top" class=label2 >
												                                                	<div id="labNameDiv" style="visibility:visible; position:absolute;">                                                                        	
												                                                		<%
												                                                		LaboratoryData labData = medicalRecordData.getLaboratory();
														                                                if (labData == null){
														                                                	labData = new LaboratoryData();
														                                                    labData.setLabId(0);
														                                                };
												                                                		if (labData.getLabId()==0 && medicalRecordDetailForm.getActionType().equalsIgnoreCase("view")){%>
													                                                    	<font size="1"><input type="text" name="labName" class="label2" value=''></font>
													                                                    <%}else{%>
													                                                		<font size="1"><ium:list className="label2" listBoxName="labName" type="<%=IUMConstants.LIST_LABORATORIES%>" styleName="width:150" selectedItem="<%=labData.getLabId()%>"  onChange="callCreateAction();"/></font>
																                          				<%}%>
																            						</div>
																            						<div id="examinerNameDiv" style="visibility:visible; position:absolute;">
																            						    <%ExaminerData examiner = medicalRecordData.getExaminer();
																            								if (examiner == null){
																            							    examiner = new ExaminerData();
												                          						 			examiner.setExaminerId(0);
													                          						 	}
																            							if (examiner.getExaminerId() == 0 && medicalRecordDetailForm.getActionType().equalsIgnoreCase("view")){%>				            							
																            								<font size="1"><input type="text" name="examiner" class="label2" value=''></font>
																            							<%}else{%>
																	            							<font size="1"><ium:list className="label2" listBoxName="examiner" type="<%=IUMConstants.LIST_EXAMINERS%>" styleName="width:150" selectedItem="<%=examiner.getExaminerId()%>" onChange="callCreateAction();"/></font>
																                          				<%}%>
																            						</div>
							                                                					</td>
												                                                <td>&nbsp;</td>
												                                                <td>&nbsp;</td>
												                                                <td>&nbsp;</td>
							                                              					</tr>
							                                              					
							                                                				<tr>
							                                                					<td class="label2"><span class="required">*</span><b>Test</b></td>
							                                                					<td>&nbsp;</td>
												                                                <%
												                                                TestProfileData testProfileData = (TestProfileData) medicalRecordData.getTest();
												                                                if (testProfileData == null){
												                                                    testProfileData = new TestProfileData();
												                                                    testProfileData.setTestId(0);
												                                                };		                                                
												                                                if (testProfileData.getTestId() == 0 && medicalRecordDetailForm.getActionType().equalsIgnoreCase("view")){%>
												                                                	<td><font size="1"><input type="text" name="test" class="label2" value=''></font></td>
												                                                <%}else{%>
												                                                	<%if((medicalRecordData.getLabTestInd()).equalsIgnoreCase("L")) {%>
													                                                	<td><font size="1"><ium:list className="label2" listBoxName="test" type="<%=IUMConstants.LIST_LABORATORY_TEST_PROFILES%>" styleName="width:150" selectedItem="<%=testProfileData.getTestId()%>" onChange="refreshAgentOrBranch(3);" filter="<%=labData.getLabId()%>"/></font></td>													                                                
													                                                <%}else{%>
													                                                	<td><font size="1"><ium:list className="label2" listBoxName="test" type="<%=IUMConstants.LIST_TEST_PROFILES%>" styleName="width:150" selectedItem="<%=testProfileData.getTestId()%>" onChange="refreshAgentOrBranch(3);"/></font></td>													                                                
													                                                <%}%>
													                                            <%}%>
												                                                <td>&nbsp;</td>
												                                                <td class="label2"><b>&nbsp;&nbsp;Examination Place</b></td>
												                                                <td>&nbsp;</td>
												                                                <%
												                                                ExaminationPlaceData examPlace = (ExaminationPlaceData) medicalRecordData.getExaminationPlace();
												   					                            if (examPlace == null){
																									examPlace = new ExaminationPlaceData();
													                                                examPlace.setExaminationPlaceId(0);
																		                        };
												                                                if (examPlace.getExaminationPlaceId() == 0 &&  medicalRecordDetailForm.getActionType().equalsIgnoreCase("view")){%>
												                                                	<td><font size="1"><input type="text" name="examPlace" class="label2" value=''></font></td>
												                                                <%}else{%>
													                                                <td><font size="1"><ium:list className="label2" listBoxName="examPlace" type="<%=IUMConstants.LIST_EXAMINATION_PLACES%>" styleName="width:175" selectedItem="<%=examPlace.getExaminationPlaceId()%>"/></font></td>
														                          				<%}%>
							                                              					</tr>
							                                             					<tr>
												                                                <td class="label2"><b>&nbsp;&nbsp;Date Requested</b></td>
												                                                <td>&nbsp;</td>
												                                                <%if (medicalRecordData.getRequestedDate() == null){%>
												                                                	<td><font size="1"><input type="text" name="dateRequested" class="label2" value='' onKeyUp="getKeyDate(event,this);"></font>
												                                                	<div id="doc_cal2" style="display:none;position:absolute;"><a href="#" onClick="popUpWin('<%=contextPath%>/jsp/calendar.jsp?textbox=frm.dateRequested',290,155);"><img src="<%=contextPath%>/images/calendar.gif" height="17" width="19" border="0">
																									</a></div>
												                                                	</td>
												                                                <%}else{%>
													                                                <td><font size="1"><input type="text" name="dateRequested" class="label2" value='<%=(DateHelper.format(medicalRecordData.getRequestedDate(),"ddMMMyyyy")).toUpperCase()%>' onKeyUp="getKeyDate(event,this);"></font>
  												                                                	<div id="doc_cal2" style="display:none;position:absolute;"><a href="#" onClick="popUpWin('<%=contextPath%>/jsp/calendar.jsp?textbox=frm.dateRequested',290,155);"><img src="<%=contextPath%>/images/calendar.gif" height="17" width="19" border="0">
																									</a></div>
													                                                </td>
													                                            <%}%>
												                                                <td>&nbsp;&nbsp;&nbsp;</td>
												                                                <td class="label2"><b>&nbsp;&nbsp;Date Conducted</b></td>
												                                                <td>&nbsp;</td>
												                                                <%if (medicalRecordData.getConductedDate() == null){%>
												                                                	<td><font size="1"><input type="text" name="dateConducted" class="label2" value='' onKeyUp="getKeyDate(event,this);"></font>
												                                                	<div id="doc_cal3" style="display:none;position:absolute;"><a href="#" onClick="popUpWin('<%=contextPath%>/jsp/calendar.jsp?textbox=frm.dateConducted',290,155);"><img src="<%=contextPath%>/images/calendar.gif" height="17" width="19" border="0">
																									</a></div>
										                                                			</td>												                                                	
												                                                <%}else{%>
													                                                <td><font size="1"><input type="text" name="dateConducted" class="label2" value='<%=(DateHelper.format(medicalRecordData.getConductedDate(),"ddMMMyyyy")).toUpperCase()%>' onKeyUp="getKeyDate(event,this);"></font>
												                                                	<div id="doc_cal3" style="display:none;position:absolute;"><a href="#" onClick="popUpWin('<%=contextPath%>/jsp/calendar.jsp?textbox=frm.dateConducted',290,155);"><img src="<%=contextPath%>/images/calendar.gif" height="17" width="19" border="0">
																									</a></div>
		                                                											</td>													                                                
													                                            <%}%>
											                                              	</tr>                                             
											                                              	<tr>
												                                                <td class="label2"><b>&nbsp;&nbsp;Date Received</b></td>
												                                                <td>&nbsp;</td>
												                                                <%if (medicalRecordData.getReceivedDate() == null){%>
												                                                	<td><font size="1"><input type="text" name="dateReceived" class="label2" value='' onKeyUp="getKeyDate(event,this);" ></font>
												                                                	<div id="doc_cal4" style="display:none;position:absolute;"><a href="#" onClick="popUpWin('<%=contextPath%>/jsp/calendar.jsp?textbox=frm.dateReceived',290,155);"><img src="<%=contextPath%>/images/calendar.gif" height="17" width="19" border="0">
																									</a></div>
												                                                	</td>
												                                                <%}else{%>
													                                                <td><font size="1"><input type="text" name="dateReceived" class="label2" value='<%=(DateHelper.format(medicalRecordData.getReceivedDate(),"ddMMMyyyy")).toUpperCase()%>' onKeyUp="getKeyDate(event,this);"></font>
												                                                	<div id="doc_cal4" style="display:none;position:absolute;"><a href="#" onClick="popUpWin('<%=contextPath%>/jsp/calendar.jsp?textbox=frm.dateReceived',290,155);"><img src="<%=contextPath%>/images/calendar.gif" height="17" width="19" border="0">
																									</a></div>
													                                                </td>
												                                              	<%}%>
												                                                <td>&nbsp;&nbsp;&nbsp;</td>
												                                                <td  class="label2"><b>&nbsp;&nbsp;Valid Until</b></td>
												                                                <td>&nbsp;</td>
												                                                <%if (medicalRecordData.getValidityDate() == null){%>
												                                                	<td><font size="1"><input type="text" name="dateValidity" class="label2" value='' onKeyUp="getKeyDate(event,this);" ></font>
												                                                	<div id="doc_cal5" style="display:none;position:absolute;"><a href="#" onClick="popUpWin('<%=contextPath%>/jsp/calendar.jsp?textbox=frm.dateValidity',290,155);"><img src="<%=contextPath%>/images/calendar.gif" height="17" width="19" border="0">
																									</a></div>
												                                                	</td>
												                                                <%}else{%>	
												                                                	<td><font size="1"><input type="text" name="dateValidity" class="label2" value='<%=(DateHelper.format(medicalRecordData.getValidityDate(),"ddMMMyyyy")).toUpperCase()%>' onKeyUp="getKeyDate(event,this);"></font>
												                                                	<div id="doc_cal5" style="display:none;position:absolute;"><a href="#" onClick="popUpWin('<%=contextPath%>/jsp/calendar.jsp?textbox=frm.dateValidity',290,155);"><img src="<%=contextPath%>/images/calendar.gif" height="17" width="19" border="0">
																									</a></div>
												                                                	</td>
												                                                <%}%>
											                                              	</tr>                                              
											                                              	<tr>
												                                                <td class="label2"><b>&nbsp;&nbsp;Follow Up No.</b></td>
												                                                <td>&nbsp;</td>
													                                                <td><font size="1"><input type="text" name="followUpNumber" maxlength=2 class="label2" value='<bean:write name="medicalData" property="followUpNumber"/>' onblur="setFollowUpDate();"></font></td>
												                                                <td>&nbsp;</td>
												                                                <td class="label2"><b>&nbsp;&nbsp;Follow Up Date</b></td>
												                                                <td>&nbsp;</td>
												                                                <%if (medicalRecordData.getFollowUpDate() == null){%>
												                                                	<td><font size="1"><input type="text" name="followUpDate" class="label2" value='' onKeyUp="getKeyDate(event,this);"></font>
												                                                	<div id="doc_cal6" style="display:none;position:absolute;"><a href="#" onClick="popUpWin('<%=contextPath%>/jsp/calendar.jsp?textbox=frm.followUpDate',290,155);"><img src="<%=contextPath%>/images/calendar.gif" height="17" width="19" border="0">
																									</a></div>
												                                                	</td>
												                                                <%}else{%>	
												                                                	<td><font size="1"><input type="text" name="followUpDate" class="label2" value='<%=(DateHelper.format(medicalRecordData.getFollowUpDate(),"ddMMMyyyy")).toUpperCase()%>' onKeyUp="getKeyDate(event,this);"></font>
												                                                	<div id="doc_cal6" style="display:none;position:absolute;"><a href="#" onClick="popUpWin('<%=contextPath%>/jsp/calendar.jsp?textbox=frm.followUpDate',290,155);"><img src="<%=contextPath%>/images/calendar.gif" height="17" width="19" border="0">
																									</a></div>
												                                                	</td>
												                                                <%}%>
											                                              	</tr>
											                                              	<tr>
												                                                <td class="label2"><b>&nbsp;&nbsp;Appointment Date</b></td>
												                                                <td>&nbsp;</td>
												                                                <%if (medicalRecordData.getAppointmentDateTime() == null){%>
												                                                	<td><font size="1"><input type="text" name="appointmentDate" class="label2" value='' onKeyUp="getKeyDate(event,this);"></font>
												                                                	<div id="doc_cal7" style="display:none;position:absolute;"><a href="#" onClick="popUpWin('<%=contextPath%>/jsp/calendar.jsp?textbox=frm.appointmentDate',290,155);"><img src="<%=contextPath%>/images/calendar.gif" height="17" width="19" border="0">
																									</a></div>
												                                                	</td>
												                                                <%}else{%>
												                                                	<td><font size="1"><input type="text" name="appointmentDate" class="label2" value='<%=(DateHelper.format(medicalRecordData.getAppointmentDateTime(),"ddMMMyyyy")).toUpperCase()%>' onKeyUp="getKeyDate(event,this);"></font>
												                                                	<div id="doc_cal7" style="display:none;position:absolute;"><a href="#" onClick="popUpWin('<%=contextPath%>/jsp/calendar.jsp?textbox=frm.appointmentDate',290,155);"><img src="<%=contextPath%>/images/calendar.gif" height="17" width="19" border="0">
																									</a></div>
												                                                	</td>
												                                                <%}%>
												                                                <td>&nbsp;</td>
												                                                <td class="label2"><b>&nbsp;&nbsp;Appointment Time</b></td>
												                                                <td>&nbsp;</td>
												                                                <%if (medicalRecordData.getAppointmentDateTime() == null){%>
												                                                	<td><font size="1"><input type="text" name="appointmentTime" class="label2" value='' ></font></td>                                                
												                                                <%}else{%>
												                                                	<td><font size="1"><input type="text" name="appointmentTime" class="label2" value='<%=DateHelper.format(medicalRecordData.getAppointmentDateTime(),"hh:mm a")%>'></font></td>                                                
												                                                <%}%>
											                                              	</tr>                                              
							                                            				</table>
							                                          				</td>
							                                        			</tr>
							                                      			</table>
							                                    		</td>
							                                  		</tr>
							                                	</table>
							                              	</td>
							                            </tr>
                                                        
                            							<tr>
                            	 							<td height="100%" valign="top" align="left">
                            	 								<table border="1" bordercolor="<%=iumColorB%>" cellpadding="10" cellspacing="0" bgcolor="" width="700">
																	<tr>
																		<td height="100%" valign="top">
																			<table border="0" cellpadding="2" cellspacing="0">
																				<tr>
																					<td valign="top">
				                                            							<table border="0" cellpadding="2" cellspacing="0">
				                                            								<tr>
											                                            		<td width="100" class="label2"><b>&nbsp;&nbsp;7-Day Memo?</b></td>
											                                            		<td>&nbsp;&nbsp;</td>
											                                            		<td class="label2" width="160">
													                                                <%if(medicalRecordData.isSevenDayMemoInd() == true) {%>
																										<input type="radio" name="sevenDayMemo" value="Y" CHECKED onclick ="enable7DayMemoFields()">Yes
									                                					                <input type="radio" name="sevenDayMemo" value="N" onclick ="disable7DayMemoFields()">No
																									<%}else{%>	 
																										<input type="radio" name="sevenDayMemo" value="Y" onclick ="enable7DayMemoFields()">Yes
									                                					                <input type="radio" name="sevenDayMemo" value="N" CHECKED onclick ="disable7DayMemoFields()">No
																									<%}%>                                                  
							            	        					                         </td>
							            	        					                         <td>&nbsp;&nbsp;</td>
							                	    					                         <td width="100" class="label2"><b>&nbsp;&nbsp;Application <br>&nbsp; Status</b></td>
										        			                                     <td>&nbsp;</td>
										        			                                     <%
										        			                                     StatusData applicationStatusData = medicalRecordData.getApplicationStatus();
													                                             if (applicationStatusData == null){						                                             
							                                                   					   	applicationStatusData = new StatusData();
							                                                   					   	applicationStatusData.setStatusId(0);
												                                                }
												                                                if (applicationStatusData.getStatusId()==0 &&  medicalRecordDetailForm.getActionType().equalsIgnoreCase("view")){%>
																									<td width="160"><font size="1"><input type="text" name="applicationStatus" class="label2" value=''></font></td>
																								<%}else{%>
                                						             								<td width="160"><font size="1"><ium:list className="label2" listBoxName="applicationStatus" type="<%=IUMConstants.LIST_REQUEST_STATUS%>" styleName="width:160" selectedItem="<%=Long.toString(applicationStatusData.getStatusId())%>"/></font></td>
                                						             							<%}%>
				                                            								</tr>
											                                            	<tr>
												                                                <td><font size="1"><b>&nbsp;</b></font></td>
												                                                <td>&nbsp;</td>
												                                                <td><font size="1">&nbsp;</font></td>
												                                                <td>&nbsp;</td>												                                                
												                                                <td class="label2"><span class="required">*</span><b>Reason for Requirement</b></td>
												                                                <td>&nbsp;</td>
												                                                <td><font size="1"><textarea name="reason" class="label2" rows="2" cols="23"><bean:write name="medicalData" property="reasonForReqt"/></textarea></font></td>					                                                
							                                              					</tr>
							                                              					<tr>
							                                            						<td class="label2"><b>&nbsp;&nbsp;Date Generated</b></td>
												                                                <td>&nbsp;</td>
							                    					                            <td><font size="1"><input type="text" name="sevenDayMemoDate" class="label2" value='<%=(DateHelper.format(medicalRecordData.getSevenDayMemoDate(),"ddMMMyyyy")).toUpperCase()%>' onKeyUp="getKeyDate(event,this);" ></font>
												                                                <div id="doc_cal8" style="display:none;position:absolute;"><a href="#" onClick="popUpWin('<%=contextPath%>/jsp/calendar.jsp?textbox=frm.sevenDayMemoDate',290,155);"><img src="<%=contextPath%>/images/calendar.gif" height="17" width="19" border="0">
																								</a></div>
							                    					                            </td>
																								<td>&nbsp;</td>
							                                        					        <td class="label2"><b>&nbsp;&nbsp;Section</b></td>
												                                                <td>&nbsp;</td>                    					                            
							                    					                            <%
								                    					                        SectionData sectionData = (SectionData) medicalRecordData.getSection();
							                    					                        	if (sectionData == null){
							                                                   					   	sectionData = new SectionData();
							                                                   					   	sectionData.setSectionId("");
												                                                }else{
													                                              	if (sectionData.getSectionId() == null)
													                                               		sectionData.setSectionId("");
																								}
												                                               if (sectionData.getSectionId().equals("") &&  medicalRecordDetailForm.getActionType().equalsIgnoreCase("view")){%>
																							   		<td><font size="1"><input type="text" name="section" class="label2" value=''></font></td>
																							   <%}else{%>
						 																			<td><font size="1"><ium:list className="label2" listBoxName="section" type="<%=IUMConstants.LIST_SECTION%>" styleName="width:160" selectedItem="<%=sectionData.getSectionId()%>"/></font></td>
																							   <%}%>
							                                              					</tr>
				                                            							</table>
				                                        							</td>
																				</tr>
																			</table>
																		</td>
																	</tr>
                            	 								</table>
                            	 							</td>
                            							</tr>
                            
							                            <tr>
							                            	<td height="100%" valign="top" align="left">
							                            		<table border="1" bordercolor="<%=iumColorB%>" cellpadding=10 cellspacing=0 bgcolor="" width="700">
							                            			<tr>
							                            				<td height="100%" valign="top">
							                            					<table border="0" cellpadding="2" cellspacing="0">
							                            						<tr>
							                            							<td valign="top">
							                            								<table border="0" cellpadding="2" cellspacing="0">
							                            									<tr>
							                                                					<td class="label2"><span class="required">*</span><b>Requesting Party</b></td>                                                						                                                
							                                                					<td>&nbsp;&nbsp;</td>
							                                                					<td><font size="1">
							                                                					<%if (medicalRecordData.getRequestingParty() == null){
																									medicalRecordData.setRequestingParty("");
																								}																	
							                                                					if (medicalRecordData.getRequestingParty().equals("") && medicalRecordDetailForm.getActionType().equalsIgnoreCase("view")){%>
																									<input type="text" name="reqParty" class="label2" value=''>
																								<%}else{%>
							                                                					<select name="reqParty" class="label2"  styleName="width:160">
								                                                					<%if (medicalRecordData.getRequestingParty().equals("")){%>
									                                                					<option value="">Select Requesting Party</option>
									                                                					<option value="Agent">Agent</option>
										                                                				<option value="GL">GL</option>
										                                                				<option value="IL">IL</option>
									    	                                            				<option value="PN">PN</option>
									                                                				<%}else if (medicalRecordData.getRequestingParty().equalsIgnoreCase("Agent")){%>
										                                                				<option value="">Select Requesting Party</option>
										                                                				<option value="Agent" selected>Agent</option>
										                                                				<option value="GL">GL</option>
										                                                				<option value="IL">IL</option>
										                                                				<option value="PN">PN</option>
										                                                			<%}else if (medicalRecordData.getRequestingParty().equalsIgnoreCase("GL")){%>
										                                                				<option value="">Select Requesting Party</option>
										                                                				<option value="Agent">Agent</option>
										                                                				<option value="GL" selected>GL</option>
										                                                				<option value="IL">IL</option>
										                                                				<option value="PN">PN</option>
										                                                			<%}else if (medicalRecordData.getRequestingParty().equalsIgnoreCase("IL")){%>
										                                                				<option value="">Select Requesting Party</option>
										                                                				<option value="Agent">Agent</option>
										                                                				<option value="GL">GL</option>
										                                                				<option value="IL" selected>IL</option>
										                                                				<option value="PN">PN</option>
										                                                			<%}else{%>
										                                                				<option value="">Select Requesting Party</option>
										                                                				<option value="Agent">Agent</option>
										                                                				<option value="GL">GL</option>
										                                                				<option value="IL">IL</option>
										                                                				<option value="PN" selected>PN</option>
										                                                			<%}%>			                                                		
								                                                				</select>
								                                                				<%}%>
								                                                				</font></td>
								                                                				<td>&nbsp;&nbsp;</td>
												                                                <td class="label2"><b>&nbsp;&nbsp;Chargeable To</b></td>
							                    					                            <td>&nbsp;</td>
							                                        					        <td><font size="1"><input type="text" name="chargeTo" class="label2" value='<bean:write name="medicalData" property="chargableTo"/>'></font></td>
												                                            </tr>
												                                            <tr>
												                                            	<td class="label2"><span class="required">*</span><b>Department</b></td>
							                    					                            <td>&nbsp;</td>
							                    					                            <%
								                    					                            SunLifeDeptData sunLifeDeptData = (SunLifeDeptData) medicalRecordData.getDepartment();
								                    					                            if (sunLifeDeptData == null){
								                                                   					   	sunLifeDeptData = new SunLifeDeptData();
								                                                   					   	sunLifeDeptData.setDeptId("");
													                                                }else{
													                                                	if (sunLifeDeptData.getDeptId() == null)
													                                                		sunLifeDeptData.setDeptId("");
													                                                }
																									if (sunLifeDeptData.getDeptId().equals("") && medicalRecordDetailForm.getActionType().equalsIgnoreCase("view")){%>
													                                                	<td><font size="1"><input type="text" name="department" class="label2" value=''></font></td>
													                                                <%}else{%>
														                                            	<td><font size="1"><ium:list className="label2" listBoxName="department" type="<%=IUMConstants.LIST_DEPARTMENT%>" styleName="width:150" selectedItem="<%=sunLifeDeptData.getDeptId()%>"/></font></td>
														                                            <%}%>
														                                            <td>&nbsp;&nbsp;</td>
							                                                					<td class="label2"><b>&nbsp;&nbsp;Amount</b></td>
							                                                					<td>&nbsp;&nbsp;</td>
							                                               						<td><font size="1"><input type="text" name="amount" class="label2" value='<bean:write name="medicalData" property="amount"/>'></font></td>                                                					                                                					
												                                            </tr>
																							<tr>
												                                                <td><font size="1">&nbsp;</font></td>
							                    					                            <td>&nbsp;</td>
							                                        					        <td><font size="1">&nbsp;</td>
							                                        					        <td>&nbsp;&nbsp;</td>
												                                                <td class="label2"><b>&nbsp;&nbsp;Paid by Agent?</b></td>
							                    					                            <td>&nbsp;</td>
							                                        					        <td class="label2">
												                                                	<%if(medicalRecordData.isPaidByAgentInd() == true) {%>
																										<input type="radio" name="payAgent" value="Y" CHECKED>Yes
																										<input type="radio" name="payAgent" value="N">No
																									<%}else{%>	 
																										<input type="radio" name="payAgent" value="Y">Yes
												                                                  		<input type="radio" name="payAgent" value="N" CHECKED>No
																									<%}%>                                                                                                    
												                                                </td>
							                    				                            </tr>					                                            
							                            								</table>
							                            							</td>
							                            						</tr>
							                            					</table>
							                            				</td>
							                            			</tr>
							                            		</table>
							                            	</td>
														</tr>
							
														<tr>
							                            	<td height="100%" valign="top" align="left">
							                            		<table border="1" bordercolor="<%=iumColorB%>" cellpadding=10 cellspacing=0 bgcolor="" width="700">
							                            			<tr>
							                            				<td height="100%" valign="top">
							                            					<table border="0" cellpadding="2" cellspacing="0">
							                            						<tr>
							                            							<td valign="top">
							                            								<table border="0" cellpadding="2" cellspacing="0">
							                            									<tr>
							                                                					<td class="label2"><b>&nbsp;&nbsp;Results Received?</b></td>
							                                                					<td>&nbsp;</td>
							                                                					<td class="label2">
												                                                	<%
											                                                		if(medicalRecordData.isReceivedResults() == true) {%>
																										<input type="radio" name="resultsReceivedInd" value="Y" CHECKED onclick ="enableResultsReceived()">Yes
																										<input type="radio" name="resultsReceivedInd" value="N" onclick="disableResultsReceived()">No
																									<%}else{%>	 
																										<input type="radio" name="resultsReceivedInd" value="Y" onclick ="enableResultsReceived()">Yes
											                                                  			<input type="radio" name="resultsReceivedInd" value="N" CHECKED onclick="disableResultsReceived()">No
																									<%}%>                                                                                                    
												                                                </td>
												                                                <td><font size="1">&nbsp;</font></td>
												                                                <td><font size="1">&nbsp;</font></td>
												                                                <td><font size="1">&nbsp;</font></td>
												                                                <td><font size="1">&nbsp;</font></td>
												                                                <td><font size="1">&nbsp;</font></td>
												                                                <td>&nbsp;&nbsp;</td>
												                                                <td class="label2"><b>&nbsp;&nbsp;Request LOA?</b></td>
							                    					                            <td>&nbsp;</td>
							                                        					        <td class="label2">
												                                                	<%
												                                                	if(medicalRecordData.isRequestForLOAInd() == true) {%>
																										<input type="radio" name="requestForLOAInd" value="Y" CHECKED onclick="enableRequestForLOA()">Yes
																										<input type="radio" name="requestForLOAInd" value="N" onclick="disableRequestForLOA()">No
																									<%}else{%>	 
																										<input type="radio" name="requestForLOAInd" value="Y" onclick="enableRequestForLOA()">Yes
												                                                  		<input type="radio" name="requestForLOAInd" value="N" CHECKED onclick="disableRequestForLOA()">No
																									<%}%>                                                                                                    
												                                                </td>
												                                            </tr>
							                            								</table>
							                            							</td>
							                            						</tr>
							                            					</table>
							                            				</td>
							                            			</tr>
							                            		</table>
							                            	</td>
														</tr>

														</logic:iterate>
                          							</table>
                        						</td>
                      						</tr>
                    					</table>
                  					</td>
                				</tr>
                				
                				<tr>
                  					<td class="label1"><font size="1">� 2003 Sun Life Financial. All rights reserved.</font></td>
                				</tr>
                				<tr>
                  					<td class="label1">&nbsp;</td>
                				</tr>
              				</table>
            			</td>
          			</tr>
        		</table>
      		</font>
    	</form>
	</body>
</html>
