<%@ taglib uri="/struts-html.tld" prefix="html" %>
<%@ taglib uri="/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/struts-logic.tld" prefix="logic" %>
<% String contextPath = request.getContextPath(); %>
<%@page import="com.slocpi.ium.data.UserProfileData"%>
<%@page import="java.util.*"%>
<%
/* Andre Ceasar Dacanay - css enhancement - start */
String iumCss = "";
String iumColorB = "";
String companyCode = "";
try {
	ResourceBundle rbWMSSP = ResourceBundle.getBundle("dbconfig");
	iumCss = rbWMSSP.getString("IUM_CSS_VERSION");
	iumCss = "_"+iumCss;
} catch (Exception e){}
if(iumCss.equals("_CP")){
	iumColorB="#2A4C7C";
	companyCode="";
}else if(iumCss.equals("_GF")){
	iumColorB="#303A77";
	companyCode = companyCode+" ";
}else{
	iumColorB="#2A4C7C";
}
/* Andre Ceasar Dacanay - css enhancement - end */
%>
<html>
<body>
<head>
<title><%=companyCode%>Integrated Underwriting and Medical System</title>
<link href="<%=contextPath%>/css/pw_main.css"     rel="stylesheet" type="text/css">
<link href="<%=contextPath%>/css/main<%=iumCss%>.css" rel="stylesheet" type="text/css">
<link href="<%=contextPath%>/css/styleCommon.css" rel="stylesheet" type="text/css">
<script language="JavaScript" src="<%=contextPath%>/js/dhtml_init.js"></script>
<script language="JavaScript" src="<%=contextPath%>/js/navigation.js"></script>
<script language="JavaScript" src="<%=contextPath%>/jsp/sl_left_nav.jsp"></script>
<script language="JavaScript">
function saveFacilitator(contextPath, facilitator) {
    opener.orderRequirement(contextPath, facilitator, document.frm);
    window.close();
}
</script>
</head>

<BODY leftMargin=0 topmargin=0 marginwidth=0 marginheight=0>
<table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%">
          <tr>
            <td width="100%" height="100%" valign="top">
              <!-- BODY -->
        <table width="100%" cellpadding="3" cellspacing="5" border="0">
          <tr> 
            <td class="label2"><b>List of Facilitators</b>              
            </td>
          </tr>
          <tr valign="top"> 
            <td> 


<!--- START OF BODY -->
<form name="frm" method="post">
<table border="1" bordercolor="<%=iumColorB%>" cellpadding=10 cellspacing=0 bgcolor="" width="100%" class="listtable1">
        <tr><td colspan="2" height="100%">
        <table border="0" cellpadding="2" cellspacing="1" bgcolor=#FFFFFF style="border: 1px solid #D1D1D1;" class="listtable1">
        <tr class="headerrow1">
        <td width="12%">USER ID</td>
        </tr>
      <% ArrayList users = (ArrayList) request.getAttribute("facilitators"); %>      
      <% Iterator  it    = users.iterator(); %>
        <tr>      
        <% String firstName = ""; %>
        <% String lastName  = ""; %>
      <% while (it.hasNext()) { %>
        <% UserProfileData user = (UserProfileData) it.next(); %>
        <% firstName = user.getFirstName(); %>
        <% lastName  = user.getLastName(); %>
        <% if (firstName == null) firstName = ""; %>
        <% if (lastName == null) lastName = ""; %>
		  <td class="row1" style="cursor:hand" onclick="saveFacilitator('<%= contextPath %>','<%= user.getUserId() %>');"><%= firstName %> <%= lastName %>&nbsp;</td>
      <% } %>		  
		</tr>


</table>
</form>
<!--- END OF BODY -->
</td>
</tr>
</table>
</td>
</tr>
</table>
</body>
</html>