<%@ page language="java" import="java.util.*,com.slocpi.ium.data.*, com.slocpi.ium.ui.form.*, com.slocpi.ium.util.*, com.slocpi.ium.ui.util.*, java.text.* " buffer="12kb"%>
<%@ taglib uri="/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/struts-html.tld" prefix="html" %>
<%@ taglib uri="/ium.tld" prefix="ium" %>
<jsp:useBean id="exceptionDetailData" scope="request" class="com.slocpi.ium.data.ExceptionDetailsData"/>
<%String contextPath = request.getContextPath();%>
<%
/* Andre Ceasar Dacanay - css enhancement - start */
String iumCss = "";
String iumColorB = "";
String companyCode = "";
try {
	ResourceBundle rbWMSSP = ResourceBundle.getBundle("dbconfig");
	iumCss = rbWMSSP.getString("IUM_CSS_VERSION");
	companyCode = iumCss;
	iumCss = "_"+iumCss;
} catch (Exception e){}
if(iumCss.equals("_CP")){
	iumColorB="#2A4C7C";
	companyCode="";
}else if(iumCss.equals("_GF")){
	iumColorB="#303A77";
	companyCode = companyCode+" ";
}else{
	iumColorB="#2A4C7C";
}
/* Andre Ceasar Dacanay - css enhancement - end */
%>
<% ExceptionFilterForm filterForm = (ExceptionFilterForm) request.getAttribute("filterForm");
   ExceptionFilterForm exceptionForm = (ExceptionFilterForm) request.getAttribute("viewThisException");   
   String clickOnSearch = (String)request.getAttribute("searchIsTrue");

   int pageNum = Integer.parseInt((String)request.getAttribute("pageNo"));
   
   String dateToday = DateHelper.format(new java.util.Date(), "ddMMMyyyy");
   String startDate = filterForm.getStartDate();
   String endDate = filterForm.getEndDate();
   String typeSelected = filterForm.getRecordType();
   String interfaceSelected = filterForm.getInterfaceSelected();
	
   if (startDate == null || startDate.equals("")){
     startDate = "";
   }
  
   if (endDate == null || endDate.equals("")){
     endDate = "";
   }
	
%>
<%
    // This routine queries the access rights of the users (RCRIVERA)
    //
    // --- START --- 
    
    StateHandler sessionHandler = new StateHandler();
    Access access = new Access();
	HashMap userAccess = sessionHandler.getUserAccess(request);
    if(!access.hasAccess(userAccess,IUMConstants.PAGE_EXCEPTION_REPORTS,IUMConstants.ACC_INQUIRE) ){                                                                                                                    
    	response.sendRedirect("noAccessPage.jsp");   
    } 
    
    String CREATE_ACCESS = "";
    String MAINTAIN_ACCESS = "";
    String DELETE_ACCESS = "";
    String EXECUTE_ACCESS = "";  
    
    if(!access.hasAccess(userAccess,IUMConstants.PAGE_EXCEPTION_REPORTS,IUMConstants.ACC_CREATE) ){                                                                                                                     
	  CREATE_ACCESS = "DISABLED"; 	
    }
    if(!access.hasAccess(userAccess,IUMConstants.PAGE_EXCEPTION_REPORTS,IUMConstants.ACC_MAINTAIN) ){                                                                                                                     
	  MAINTAIN_ACCESS = "DISABLED";
    }
    if(!access.hasAccess(userAccess,IUMConstants.PAGE_EXCEPTION_REPORTS,IUMConstants.ACC_DELETE) ){                                                                                                                     
	  DELETE_ACCESS = "DISABLED";
    }
    if(!access.hasAccess(userAccess,IUMConstants.PAGE_EXCEPTION_REPORTS,IUMConstants.ACC_EXECUTE) ){                                                                                                                     
	  EXECUTE_ACCESS = "DISABLED"; 	
    }
    
    // --- END ---
    UserData userData = sessionHandler.getUserData(request); 
	UserProfileData userPref = userData.getProfile();
	
	
 %>    
<bean:size id="sizeList" name="page" property="list" />

<% int recordsNum = sizeList.intValue(); %>

<html>
  <head>
    <title><%=companyCode%>Integrated Underwriting and Medical System</title>
    <link href="<%=contextPath%>/css/pw_main.css" rel="stylesheet" type="text/css">
    <link href="<%=contextPath%>/css/main<%=iumCss%>.css" rel="stylesheet" type="text/css">
    <link href="<%=contextPath%>/css/styleCommon.css" rel="stylesheet" type="text/css">
    <script language="JavaScript" src="<%=contextPath%>/js/dhtml_init.js"></script>
    <script language="JavaScript" src="<%=contextPath%>/js/navigation.js"></script>
    <script language="Javascript" src="<%=contextPath%>/js/validation.js"></script>
    <script language="JavaScript" src="<%=contextPath%>/jsp/sl_left_nav.jsp"></script>
	<script language="JavaScript" src="<%=contextPath%>/jsp/validate_exception.jsp"></script>
  </head>

  <body leftMargin="0" topmargin="0" marginwidth="0" marginheight="0" onload="putButton(<%=userPref.getRecordsPerView()%>,<%=recordsNum%>);" onUnload="closePopUpWin();">
    <table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%">
      <tr>
        <td width="100%" colspan="2">
          <jsp:include page="header.jsp" flush="true"/>
          <table border="0" cellpadding="0" cellspacing="0" width="100%" height="100%">
            <tr valign="top">
              <td background="<%=contextPath%>/images/bg_yellow.gif" id="leftnav" valign="top" width="147">
                <img height="9" src="<%=contextPath%>/images/sp.gif" width="147"><br>
                <img height="30" src="<%=contextPath%>/images/sp.gif" width="7"> <br>
                <img height="7" src="<%=contextPath%>/images/sp.gif" width="147"><br>
                <div id="level0" style="position: absolute; visibility: hidden; width: 100%; z-index: 500"> 
                  <script language=javascript>
	                <!--
                    document.write(writeMenu(''));
                    if (IE4) {
                      document.write(writeDiv());
                    };
                    //-->
                  </script>
                </div>
                <div id="NSFix" style="position: absolute; visibility: hidden; z-index: 999">&nbsp;</div>
                <!-- Netscape needs a hard-coded div to write dynamic DIVs --> 
                <script language=javascript>
                  <!--
                  if (NS4) {
                    document.write(writeDiv());
                  }
                  //-->
                </script>
                <script>
                  <!--
                  initMenu();
                  //-->
                </script>
              </td>

              <form name="exceptionForm" method="post">      
              <td width="100%"> 
                <div align="left"> 
                <table border="0" cellpadding="0" width="100%" cellspacing="0">
                  <tr> 
                    <td width="100%"> 
                      <div align="left"> 
                        <table border="0" cellpadding="3" cellspacing="0" width="100%" height="27"  background="images/sectionbkgray.jpg">
                          <tr> 
                            <td valign="top"> 
                               <span class="main">&nbsp;Integrated Underwriting and Medical System </span>
                            </td>
                          </tr>
                        </table>
                      </div>
                    </td>
                  </tr>
                  <tr> 
                    <td width="100%" colspan=2>&nbsp;</td>
                  </tr>
                  <tr> 
                    <td width="100%" colspan=2 class="title2">&nbsp;&nbsp;Exception Log Maintenance</td>
                  </tr>          
                </table>

                <table width="100%" cellpadding="3" cellspacing="5" border="0">
                  <tr valign="top">
                  <td>
                   <table border="1" bordercolor="<%=iumColorB%>" cellpadding=10 cellspacing=0 bgcolor="" width="700" class="listtable1">
                     <tr>
                       <td colspan="2" height="100%">
                          <table border="0" cellpadding="2" cellspacing="1" bgcolor=#FFFFFF style="border: 0px solid #D1D1D1;" class="listtable1">
                            <tr>
                              <td>
                                <!--IFRAME src="tempAdminDetail.html" width="665" height="100" scrolling="no" frameborder="0" >
                                </IFRAME-->
                  
                			    <table border="0" width="669">
		        		          <tr>
		        		          <!-- Added hidden field to store today's Date -->
		        		            <input type="hidden" name="today" value="<%=dateToday%>">
		        		            <td width="250" class="label2"><b>Exception Date</b><td/>
		        		            <td width="300" class="label2"><b>From &nbsp</b>
		        		            	<input type="text" name="startDate" class="label2" value="<%=startDate%>"> 
										<a href="#" onClick="popUpWin('<%=contextPath%>/jsp/calendar.jsp?textbox=exceptionForm.startDate',290,155);">
							            <img src="<%=contextPath%>/images/calendar.gif" height="17" width="19" border="0">
								        </a>
		        		           	</td>
		        		            <td width="300" class="label2"><b>To &nbsp</b>
		        		            	<input type="text" name="endDate" class="label2" value="<%=endDate%>"> 
										<a href="#" onClick="popUpWin('<%=contextPath%>/jsp/calendar.jsp?textbox=exceptionForm.endDate',290,155);">
							            <img src="<%=contextPath%>/images/calendar.gif" height="17" width="19" border="0">
								        </a>
		        		            </td>
		        		            <td width="100">&nbsp;</td>
		        		            <td width="100">&nbsp;</td>
		        		          </tr>
		        		          <tr>
		        		            <td width="150" class="label2" valign="top"><b>Record Type</b><td/>
		        		            <td width="300"><ium:list className="label2" listBoxName="recordType" type="<%=IUMConstants.LIST_RECORD_TYPES%>" styleName="width:200" selectedItem="<%=typeSelected%>" onChange="" filter=""/></td>
		        		            <td width="100">&nbsp;</td>
		        		            <td width="100">&nbsp;</td>
		        		          </tr>
		        		          <tr>
		        		            <td width="150" class="label2" valign="top"><b>Interface</b><td/>
		        		            <td width="300"><ium:list className="label2" listBoxName="interfaceSelected" type="<%=IUMConstants.LIST_INTERFACE_TYPES%>" styleName="width:200" selectedItem="<%=interfaceSelected%>" onChange="" filter=""/></td>
		        		            <td width="100">&nbsp;</td>
		        		            <td width="100">&nbsp;</td>
		        		          </tr>
		        		        </table>                  
                              </td>
                            </tr>
                          </table>
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
                
                <tr valign="top">
                  <td colspan="2"> 
                    <!--- START OF BODY -->
                    <table width="700">
                    <tr>
                    <td align="left">
                    <input type="button" value="Search" class="button1" onclick="searchException()">&nbsp;
					<input type="button" value="View Details" class="button1" onclick="viewExceptionDetail()">&nbsp;
					</td>
					</tr>
					</table>
					<!-- Added hidden to store the if selected an exception to view the details -->
                    <input type="hidden" name="search" value="">
                    <input type="hidden" name="selectedViewDetails" value="0">
                    <% if (clickOnSearch != null && clickOnSearch.equals("1")){%>
					<table border="1" bordercolor="<%=iumColorB%>" cellpadding=10 cellspacing=0 bgcolor="" width="700" class="listtable1">
                      <tr>
                        <td colspan="2" height="100%">
                          <table border="0" cellpadding="2" cellspacing="1" bgcolor=#FFFFFF style="border: 1px solid #D1D1D1;" class="listtable1">
                            <tr class="headerrow1">
							  <td width="5">&nbsp</td>
                              <td width="150">EXCEPTION ID</td>
                              <td width="150">DATE</td>
                              <td width="150">RECORD TYPE</td>
                              <td width="200">INTERFACE</td>
                            </tr>

                            <%
                            int i = 0;
                            String tr_class;
                            %>
                            <logic:iterate id="exceptionLog" name="exceptionForm" property="exceptions">
                            <%
                            if (i%2 == 0) {
                              tr_class = "row1";
                            } else {
                              tr_class = "row2";
                            }
                            %>
                            <tr>
							  <td class="<%=tr_class%>" width="5"><input type="radio" class="label2" name="idSelected" value='<bean:write name="exceptionLog" property="exceptionId"/>'></td>
                              <td class="<%=tr_class%>" width="100"><bean:write name="exceptionLog" property="exceptionId"/></td>
                              <td class="<%=tr_class%>" width="100"><bean:write name="exceptionLog" property="exceptionDate"/></td>
                              <td class="<%=tr_class%>" width="100"><bean:write name="exceptionLog" property="recordType"/></td>
                              <td class="<%=tr_class%>" width="100"><bean:write name="exceptionLog" property="exceptionInterface"/></td>
                            </tr>
                            <%i++;%>
                            </logic:iterate>

                            <% if (i == 0) { %>
                            <tr>
                              <td class="label2" colspan="3"><bean:message key="message.noexisting" arg0="exceptions"/></td>                                                    
                            </tr>
                            <% } %>
      
                            <!-- START Number of pages -->		
                            <bean:size id="noOfPages" name="page" property="pageNumbers" />
                            <% if (noOfPages.intValue() > 1) { %>
                            <tr>
                              <td class="headerrow4" width="100%" height="10" class="label2" valign="bottom" colspan="7">
                                <%
                                int pageNumber = Integer.parseInt((String)request.getAttribute("pageNo"));
                                String viewPage = "listException.do";
                                int currLink = 1;
                                int prevLink = 1;
                                int firstPage = 1;
                                int lastPage = noOfPages.intValue();
                                %>

                                <!-- don't display link for previous page if the current page is the first page -->
                                <% if (pageNumber > firstPage) { %>
                                  <a href="#" onClick="javascript:rePaginate(1,'<%=viewPage%>');"><img src="<%=contextPath%>/images/pagnavfirst_off.gif" name="pagnavfirst" border="0" width="18" height="13"/></a>&nbsp;					
                                  <a href="#" onClick="javascript:rePaginate('<%=pageNumber-1%>','<%=viewPage%>');"><img src="<%=contextPath%>/images/pagnavleft_off.gif" name="pagnavleft" border="0" width="18" height="13"/></a>
                                <% } else {%>
                                  <img src="<%=contextPath%>/images/pagnavfirst_on.gif" name="pagnavfirst" border="0" width="18" height="13"/>&nbsp;
                                  <img src="<%=contextPath%>/images/pagnavleft_on.gif" name="pagnavleft" border="0" width="18" height="13"/>
                                <% } %>

                                <logic:iterate id="navLinks" name="page" property="pageNumbers">                                                            
                                  <% currLink = ((Integer)navLinks).intValue();%>
                                  <% if (((Integer)navLinks).intValue() == pageNumber) { %>
                                    <b><bean:write name="navLinks"/></b>
                                  <% } else { %>
                                    <% if ((currLink > (prevLink+1))) { %>
                                    ...
                                    <% } %>
                                    <a href="#" onClick="rePaginate('<bean:write name="navLinks"/>','<%=viewPage%>');" class="links2"><bean:write name="navLinks"/></a>
                                  <% } %>
                                  <%prevLink = currLink;%>
                                </logic:iterate>

                                <!-- don't display link for next page if the current page is the last page -->
                                <% if(pageNumber < lastPage) { %>
                                  <a href="#" onClick="rePaginate('<%=pageNumber+1%>','<%=viewPage%>');"><img src="<%=contextPath%>/images/pagnavright_off.gif" name="pagnavright" border="0" width="18" height="13"/></a>&nbsp;
                                  <a href="#" onClick="rePaginate('<%=prevLink%>','<%=viewPage%>');"><img src="<%=contextPath%>/images/pagnavlast_off.gif" width="18" height="13" border="0"></a>
                                <% } else { %>
                                  <img src="<%=contextPath%>/images/pagnavright_on.gif" width="18" height="13" border="0">&nbsp;
                                  <img src="<%=contextPath%>/images/pagnavlast_on.gif" width="18" height="13" border="0">
                                <% } %>
                              </td>
                            </tr>
                            <% } %>
                            <input type="hidden" name="pageNo" value="<%= pageNum%>">
                            <!-- END Number of pages -->
                          </table>
                        </td>
                       </tr>
                      
                    </table>
                    <table width="700">
					 <tr>
                         <td align="left"><div id="buttons2" style="display:none;position:absolute;" >
		                    <input type="button" value="Search" class="button1" onclick="searchException()">&nbsp;
							<input type="button" value="View Details" class="button1" onclick="viewExceptionDetail()">&nbsp;
		  				</div>
						</td>
					   </tr>
					   <tr>
					   	<td>&nbsp;</td>
					   	</tr>
					 </table>
						<!-- added this table to display the details of the exception -->
							<% if (exceptionForm != null) {%>
							<p class="title2">Exception Details</p>
							<table border="1" bordercolor="<%=iumColorB%>" cellpadding=10 cellspacing=0 bgcolor="" class="listtable1">
								<tr class="headerrow1">
									<td width="50">ID</td>
									<td>MESSAGE</td>
								<!--	removed <td>XML</td> -->
								</tr>
	                        	    <%
	    	                        int index = 0;
        	    	                %>
								<logic:iterate id="exceptionDetails" name="viewThisException" property="details">
	                    	        <%
    	    			            if (index%2 == 0) {
        	                	    tr_class = "row1";
            	                	} else {
	                	              tr_class = "row2";
    	                	        }
        	                	    %>
            	                	<tr>
    	            	              <td class="<%=tr_class%>"><bean:write name="exceptionDetails" property="id"/></td>
        	            	          <td class="<%=tr_class%>" width="350"><bean:write name="exceptionDetails" property="message"/></td>
	        	            	      <!--  <td class="<%=tr_class%>"><bean:write name="exceptionDetails" property="xmlRec"/></td> -->
        	        	            </tr>
		                   	        <%index++;%>
								</logic:iterate>
							</table>
							<br>
								<table border="1" bordercolor="<%=iumColorB%>" cellpadding=10 cellspacing=0 bgcolor="" width="700" class="listtable1">
									<tr class="headerrow1"><td>XML</td></tr>
									<% if (exceptionDetailData.getXmlRec() != null){%>
										<tr><td class="row2" width="700"><bean:write name="exceptionDetailData" property="xmlRec"/></td></tr>
									<%} else {%>
								        <tr>
                              				<td class="label2" colspan="3"><bean:message key="message.noexisting" arg0="xml"/></td>                                                    
                            			</tr>
									<% } %>
								</table>
							<% } %>
						<!-- end -->
                    <% } %>
                    </td>
                  </tr>
                </table>
              </form>
            <!--- END OF BODY -->
          </td>
        </tr>
      </table>
    </div>
  </body>
</html>