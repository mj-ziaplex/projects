<%@ page language="java" import="java.util.*, com.slocpi.ium.ui.form.*, com.slocpi.ium.util.*, com.slocpi.ium.ui.util.*, com.slocpi.ium.data.*, com.slocpi.ium.underwriter.*" buffer="1024kb"%>
<%@ taglib uri="/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/struts-html.tld" prefix="html" %>
<%@ taglib uri="/ium.tld" prefix="ium" %>
<%String contextPath = request.getContextPath();%>
<%
/* Andre Ceasar Dacanay - css enhancement - start */
String iumCss = "";
String iumColorB = "";
String iumColorO = "";
String companyCode = "";
try {
	ResourceBundle rbWMSSP = ResourceBundle.getBundle("dbconfig");
	iumCss = rbWMSSP.getString("IUM_CSS_VERSION");
	companyCode = iumCss;
	iumCss = "_"+iumCss;
} catch (Exception e){}
if(iumCss.equals("_CP")){
	iumColorB="#2A4C7C";
	iumColorO = "#FFCB00";
	companyCode="";
}else if(iumCss.equals("_GF")){
	iumColorB="#303A77";
	iumColorO = "#303A77";
	companyCode = companyCode+" ";
}else{
	iumColorB="#2A4C7C";
	iumColorO = "#FFCB00";
}
/* Andre Ceasar Dacanay - css enhancement - end */
%>
<%
    // This routine queries the access rights of the users (RCRIVERA)
    //
    // --- START ---

    StateHandler sessionHandler = new StateHandler();
    Access access = new Access();

	HashMap userAccess = sessionHandler.getUserAccess(request);
    if(!access.hasAccess(userAccess,IUMConstants.PAGE_ASSESSMENT_DETAILS,IUMConstants.ACC_INQUIRE) ){
    	response.sendRedirect("noAccessPage.jsp");
    }

    String CREATE_ACCESS = "";
    String MAINTAIN_ACCESS = "";
    String DELETE_ACCESS = "";
    String EXECUTE_ACCESS = "";
    String CREATE_REQUEST = "";

    boolean INQUIRE_REQUEST = true;
    boolean INQUIRE_CDS = true;
    boolean INQUIRE_KOREQRMT = true;
    boolean INQUIRE_UWASSESS= true;
    boolean INQUIRE_DOCTORNOTES = true;

    // CREATE ASSESSMENT REQUEST
    if (!access.hasAccess(userAccess,IUMConstants.PAGE_ASSESSMENT_DETAILS,IUMConstants.ACC_CREATE) ){
	  CREATE_ACCESS = "DISABLED";
    }
	// MAINTAIN ASSESSMENT REQUEST
    if (!access.hasAccess(userAccess,IUMConstants.PAGE_ASSESSMENT_DETAILS,IUMConstants.ACC_MAINTAIN) ){
	  MAINTAIN_ACCESS = "DISABLED";
    }
	// -- NOT APPLICABLE --
    if (!access.hasAccess(userAccess,IUMConstants.PAGE_ASSESSMENT_DETAILS,IUMConstants.ACC_DELETE) ){
	  DELETE_ACCESS = "DISABLED";
    }
	// UPDATE LOCATION
    if (!access.hasAccess(userAccess,IUMConstants.PAGE_ASSESSMENT_DETAILS,IUMConstants.ACC_EXECUTE) ){
	  EXECUTE_ACCESS = "DISABLED";
    }
	// ADD DOCUMENT
    if (!access.hasAccess(userAccess,IUMConstants.PAGE_REQUEST_DETAILS,IUMConstants.ACC_CREATE) ){
	  CREATE_REQUEST = "DISABLED";
    }

    if (!access.hasAccess(userAccess,IUMConstants.PAGE_REQUEST_DETAILS,IUMConstants.ACC_INQUIRE) ){
	  INQUIRE_REQUEST = false;
    }
    if (!access.hasAccess(userAccess,IUMConstants.PAGE_CDS,IUMConstants.ACC_INQUIRE) ){
	  INQUIRE_CDS = false;
    }
    if (!access.hasAccess(userAccess,IUMConstants.PAGE_REQUIREMENTS_KO,IUMConstants.ACC_INQUIRE) ){
	  INQUIRE_KOREQRMT = false;
    }
    if (!access.hasAccess(userAccess,IUMConstants.PAGE_UW_ASSESSMENT,IUMConstants.ACC_INQUIRE) ){
	  INQUIRE_UWASSESS = false;
    }
    if (!access.hasAccess(userAccess,IUMConstants.PAGE_DOCTORS_NOTES,IUMConstants.ACC_INQUIRE) ){
	  INQUIRE_DOCTORNOTES = false;
    }

    // --- END ---

 %>

<html>
  <head>
    <title><%=companyCode%>Integrated Underwriting and Medical System</title>
    <link href="<%=contextPath%>/css/main<%=iumCss%>.css" rel="stylesheet" type="text/css">
    <script language="Javascript" src="<%=contextPath%>/js/navigation.js"></script>
    <script language="Javascript" src="<%=contextPath%>/js/validation.js"></script>
    <script language="Javascript" src="<%=contextPath%>/jsp/validate_req_RequestDetail.jsp"></script>
  </head>

  <%
  RequestDetailForm actionForm = (RequestDetailForm) request.getAttribute("detailForm");
  String lob = actionForm.getLob();
  String transmitInd = actionForm.getTransmitIndicator();
  String autoSettleInd = actionForm.getAutoSettleIndicator();
  String clientToSearch = actionForm.getClientToSearch();

  //current status
  String currentStatus = request.getParameter("currentStatus");
  if (currentStatus == null) {
    currentStatus = actionForm.getRequestStatus();
  }
  String isSuccessChangeStatus = request.getParameter("isSuccessChangeStatus");
  if ((isSuccessChangeStatus != null) && (isSuccessChangeStatus.equals("true"))) {
    currentStatus = actionForm.getRequestStatus();
  }

  //selected status
  String selectedStatus = request.getParameter("requestStatus");
  if (selectedStatus == null) {
    selectedStatus = actionForm.getRequestStatus();
  }

  //current assigned to
  String currentAssignedTo = actionForm.getAssignedTo();
  if (currentAssignedTo == null || (currentAssignedTo != null && currentAssignedTo.equals(""))) {
    currentAssignedTo = request.getParameter("currentAssignedTo");
  }

/*  Original current assigned to
  //current assigned to
  String currentAssignedTo = request.getParameter("currentAssignedTo");
  if (currentAssignedTo == null) {
    currentAssignedTo = actionForm.getAssignedTo();
  }
*/

  String newAssignedTo = request.getParameter("assignedTo");
  if (newAssignedTo == null) {
    newAssignedTo = currentAssignedTo;
  }

  UserData ud = sessionHandler.getUserData(request);
  UserProfileData profile = ud.getProfile();
  String userId = profile.getUserId();

/*  boolean isValid = false;
  if (userId.equals(currentAssignedTo)) {
    isValid = true;
  }

  // CREATE ASSESSMENT REQUEST
  if (CREATE_ACCESS.equals("") && !isValid) {
    CREATE_ACCESS = "DISABLED";
  }
  // MAINTAIN ASSESSMENT REQUEST
  if (MAINTAIN_ACCESS.equals("") && !isValid) {
    MAINTAIN_ACCESS = "DISABLED";
  }
  // UPDATE LOCATION
  if (EXECUTE_ACCESS.equals("") && !isValid) {
    EXECUTE_ACCESS = "DISABLED";
  }
  // ADD DOCUMENT
  if (CREATE_REQUEST.equals("") && !isValid) {
    CREATE_REQUEST = "DISABLED";
  }
*/
  //disable buttons (except create assessment request) if current status is an end status
  AssessmentRequest ar = new AssessmentRequest();
  String FORWARD_EXECUTE_ACCESS = EXECUTE_ACCESS;
  if (ar.isEndStatus(lob, Long.parseLong(currentStatus))) {
    MAINTAIN_ACCESS = "DISABLED";
    EXECUTE_ACCESS = "DISABLED";
    CREATE_REQUEST = "DISABLED";
  }

  boolean isMaintain = false;
  String reqIsMaintain = request.getParameter("isMaintain");
  if ((reqIsMaintain != null) && (reqIsMaintain.equals("true"))) {
    isMaintain = true;
  }

  //filter for request status
  String lob_status = lob + "," + currentStatus;
  %>
  <script language="javascript" type="text/javascript">
  // create the arrays for the agentBranch javascript
    // declare the agents list
    aAgents = new Array ();
    <logic:iterate id="agentOffices" name="detailForm" property="agentBranches">
    aAgents.push("<bean:write name='agentOffices' property='userId' />");
    </logic:iterate>

    // declare the branches for the agents
    aBranches = new Array ();
    <logic:iterate id="agentOffices" name="detailForm" property="agentBranches">
    aBranches.push("<bean:write name='agentOffices' property='officeCode' />");
    </logic:iterate>

    // declare the agent names array
    aAgentNames = new Array();
    <logic:iterate id="agentOffices" name="detailForm" property="agentBranches">
    aAgentNames.push("<bean:write name='agentOffices' property='lastName' />, <bean:write name='agentOffices' property='firstName' />");
    </logic:iterate>
  </script>
  <script language="Javascript" src="<%=contextPath%>/js/agentBranch.js"></script>

  <body leftmargin="0" topmargin="0"  onLoad="<% if (isMaintain) { %> enableRequestDetails(); enableTabDetails('request'); setDateForwarded();initializeClientData('<%=clientToSearch%>'); <% } else { %> disableRequestDetails(); disableTabDetails('request'); <% } %> hideSaveCancelDoc(); showAddDocument();"  onUnload="closePopUpWin();">  
    <%
  		//WMS session expiration = close browser
		ResourceBundle rb1 = ResourceBundle.getBundle("wmsconfig");
		String wmsContextPath1 = rb1.getString("wms_context_path");
		String TABLE_WIDTH = "";
		String HEADER_WIDTH = "";
		String GRAY_WIDTH = "";
		String BUTTONSPACE_WIDTH = "";
       	if(contextPath.equalsIgnoreCase(wmsContextPath1)){ 
       		TABLE_WIDTH = "700";
       		HEADER_WIDTH = "700";
       		GRAY_WIDTH = "780";
       		BUTTONSPACE_WIDTH = "190";
       	}
  		else{
  			TABLE_WIDTH = "950";
  			HEADER_WIDTH = "100%";
  			GRAY_WIDTH = "100%";
       		BUTTONSPACE_WIDTH = "300";
  		}%>
    
    <form name="requestForm" method="post">
      <table border="0" cellpadding="0" cellspacing="0" height="100%" width="<%=HEADER_WIDTH%>">
        <tr>
          <td width="<%=TABLE_WIDTH%>" colspan="2">
            <jsp:include page="header.jsp" flush="true"/>
          </td>
        </tr>
        <tr>
          <td class="label2" rowspan="2" bgcolor="<%=iumColorO%>" height="100%" width="20"><img src="<%=contextPath%>/images/spacer.gif" width="20"></td>
          <td class="label2" background="<%=contextPath%>/images/GradientGray.jpg" height="27" width="<%=GRAY_WIDTH%>" valign="top">&nbsp;&nbsp;
            <span class="main">Integrated Underwriting and Medical System</span>
          </td>
        </tr>
        <tr>
          <td height="100%">
            <html:errors/>
            <!-- Status List START -->
            <table border="0" cellpadding="5" cellspacing="0" width="<%=TABLE_WIDTH%>">
              <tr>
                <td class="label2" width="200" align="left"><ium:list className="label2" listBoxName="requestStatus" type="<%=IUMConstants.LIST_REQUEST_STATUS%>" styleName="width:200" selectedItem="<%=selectedStatus%>" onChange="setAssignTo();" filter="<%=lob_status%>"/></td>
                <td class="label2" align="left" id="maintain_button"><input class="button1" type="button" name="btnMaintain" value="Maintain" onClick="maintainRequest('request');" <%=MAINTAIN_ACCESS%>></td>
                <td class="label2" align="left" id="create_button"><input class="button1" type="button" name="btnCreate" value="Create" onClick="window.location = 'createAssessmentRequest.do';" <%=CREATE_ACCESS%> ></td>
                <td class="label2" align="left" id="save_button"><input class="button1" type="button" name="btnSave" value="Save" onClick="checkFields();" <%=MAINTAIN_ACCESS%>></td>
                <td class="label2" align="left" id="cancel_button"><input class="button1" type="button" name="btnCancel" value="Cancel" onClick="cancelRequest('request');" <%=MAINTAIN_ACCESS%>></td>
                <td class="label2" align="left"><input class="button1" type="button" name="btnUpdateLocation" value="Update Location" onClick="gotoPage('requestForm', 'updateLocation.do');" <%=EXECUTE_ACCESS%>></td>
                <input type="hidden" name="updateType" value="<%=IUMConstants.PROCESS_TYPE_SINGLE%>">
                <% if ((actionForm.getSourceSystem() != null) && (transmitInd != null && transmitInd.equals(IUMConstants.INDICATOR_FAILED))) {%>
                <td class="label2" align="left"><input class="button1" type="button" name="btnRetrieveCDS" value="Retrieve CDS" onClick="retrieveCDS();" <%=MAINTAIN_ACCESS%>></td>
                <% } else {%>
                <td width="<%=BUTTONSPACE_WIDTH%>">&nbsp;</td>
                <% }%>
                <%-- if (autoSettleInd != null && autoSettleInd.equals(IUMConstants.INDICATOR_FAILED)) {--%>
                <% if (autoSettleInd != null) {
                	  String autoSettleButton = "";
                      if (autoSettleInd.equals(IUMConstants.INDICATOR_SUCCESS)) {
							autoSettleButton = "DISABLED";
                      }
                %>
                <td class="label2" align="left"><input class="button1" type="button" name="btnAutoSettle" value="Auto-Settle" onClick="autoSettle();" <%=autoSettleButton%>></td>
                <% } else {%>
                <td width="<%=BUTTONSPACE_WIDTH%>">&nbsp;</td>                
                <% }%>
				<% if ((Long.parseLong(currentStatus) == IUMConstants.STATUS_APPROVED) ||
                       (Long.parseLong(currentStatus) == IUMConstants.STATUS_AR_CANCELLED) ||
                       (Long.parseLong(currentStatus) == IUMConstants.STATUS_DECLINED) ||
					   (Long.parseLong(currentStatus) == IUMConstants.STATUS_FOR_OFFER) ||
                       (Long.parseLong(currentStatus) == IUMConstants.STATUS_NOT_PROCEEDED_WITH)) { %>
                <td class="label2" align="left"><input class="button1" type="button" name="btnForward" value="Forward to Records" onClick="forwardToRecords();"  <%=FORWARD_EXECUTE_ACCESS%>></td>
                <% } else {%>
                <td width="<%=BUTTONSPACE_WIDTH%>">&nbsp;</td>                
				<%}%>
              </tr>
            <% if (autoSettleInd != null) {
            	  String autoSettleMsg = "";
                  if (autoSettleInd.equals(IUMConstants.INDICATOR_SUCCESS)) {
					autoSettleMsg = "Auto-settle Successful";
                  } else if (autoSettleInd.equals(IUMConstants.INDICATOR_FAILED)){
                	autoSettleMsg = "Auto-settle Not Successful:";
                  }
            %>
            <tr><td class="error"><%=autoSettleMsg%>&nbsp;<bean:write name="detailForm" property="autoSettleRemarks"/></td></tr>
			<% } %>
            </table>
            <!-- Status List END -->

            <!-- hidden field to store current status -->
            <input type="hidden" name="currentStatus" value="<%=currentStatus%>">
            <input type="hidden" name="statusDate" value="<%=actionForm.getStatusDate()%>">

            <!-- hidden field to store UW analysis, UW remarks -->
            <input type="hidden" name="UWAnalysis" value="<bean:write name="detailForm" property="UWAnalysis"/>">
            <input type="hidden" name="UWRemarks" value="<bean:write name="detailForm" property="UWRemarks"/>">

            <!-- hidden field to store maintain flag for request details -->
            <input type="hidden" name="isMaintain" value="<%=isMaintain%>">

            <!-- hidden field to store success flag for change status in request details -->
            <input type="hidden" name="isSuccessChangeStatus" value="false">

            <!-- hidden field to store the currently assigned to user -->
            <input type="hidden" name="currentAssignedTo" value="<%=newAssignedTo%>">

            <!-- hidden field to store the tab/page of the request detail -->
            <input type="hidden" name="reqPage" value="requestDetail">

            <table border="0" cellpadding="10" cellspacing="0" height="100%" width="100%">
              <tr>
                <td>
                  <%
                  String _sourceSystem = actionForm.getSourceSystem();
                  if (_sourceSystem == null) {
                    _sourceSystem = "";
                  }

                  String _agentCode = actionForm.getAgentCode();
                  if (_agentCode == null) {
                    _agentCode = "";
                  }

                  String _lob = actionForm.getLob();
                  if (_lob == null) {
                    _lob = "";
                  }

                  String _branchCode = actionForm.getBranchCode();
                  if (_branchCode == null) {
                    _branchCode = "";
                  }

                  String _assignedTo = newAssignedTo; //actionForm.getAssignedTo();
                  if (_assignedTo == null) {
                    _assignedTo = "";
                  }

                  String _location = actionForm.getLocation();
                  if (_location == null) {
                    _location = "";
                  }

                  String _underwriter = actionForm.getUnderwriter();
                  if (_underwriter == null) {
                    _underwriter = "";
                  }

                  String _insuredClientNo = actionForm.getInsuredClientNo();
                  if (_insuredClientNo == null) {
                    _insuredClientNo = "";
                  }

                  String _insuredSex = actionForm.getInsuredSex();
                  if (_insuredSex == null) {
                    _insuredSex = "";
                  }

                  String _ownerClientNo = actionForm.getOwnerClientNo();
                  if (_ownerClientNo == null) {
                    _ownerClientNo = "";
                  }

                  String _ownerSex = actionForm.getOwnerSex();
                  if (_ownerSex == null) {
                    _ownerSex = "";
                  }

                  String _status = currentStatus; //actionForm.getRequestStatus();
                  if (_status == null) {
                    _status = "";
                  }

                  String _selectedStatus = selectedStatus;
                  if (_selectedStatus == null) {
                    _selectedStatus = "";
                  }
                  %>
                  <jsp:include page="requestDetail_include.jsp" flush="true">
                    <jsp:param name="sourceSystem" value = "<%=_sourceSystem%>"/>
                    <jsp:param name="agentCode" value = "<%=_agentCode%>"/>
                    <jsp:param name="lob" value = "<%=_lob%>"/>
                    <jsp:param name="branchCode" value = "<%=_branchCode%>"/>
                    <jsp:param name="assignedTo" value = "<%=_assignedTo%>"/>
                    <jsp:param name="location" value = "<%=_location%>"/>
                    <jsp:param name="underwriter" value = "<%=_underwriter%>"/>
                    <jsp:param name="insuredClientNo" value = "<%=_insuredClientNo%>"/>
                    <jsp:param name="insuredSex" value = "<%=_insuredSex%>"/>
                    <jsp:param name="ownerClientNo" value = "<%=_ownerClientNo%>"/>
                    <jsp:param name="ownerSex" value = "<%=_ownerSex%>"/>
                    <jsp:param name="status" value = "<%=_status%>"/>
                    <jsp:param name="selectedStatus" value = "<%=_selectedStatus%>"/>
                  </jsp:include>
                </td>
              </tr>
             <% if ((INQUIRE_REQUEST || INQUIRE_CDS || INQUIRE_KOREQRMT || INQUIRE_UWASSESS || INQUIRE_DOCTORNOTES)) {%>
              <tr valign="top">
                <td width="100%" height="100%" valign="top">
                  <table cellpadding="1" cellspacing="0">
                    <tr>
                      <td bgcolor="<%=iumColorB%>">
                        <table border="0" cellpadding="3" cellspacing="0">
                          <tr><td class="tabSelected"><b>Request Details</b></td></tr>
                        </table>
                      </td>
                      <td></td>
                      <% if (INQUIRE_CDS) {%>
                      <td bgcolor="#BBBBBB" style="cursor:hand" onmouseover="this.style.backgroundColor = '<%=iumColorB%>'" onmouseout="this.style.backgroundColor = '#BBBBBB'">
                        <table border="0" cellpadding="3" cellspacing="0">
                          <tr><a href="javascript:gotoPage('requestForm', 'viewCDSDetail.do');"><td class="tabLink"><b>CDS Details, KO & Med/Lab</b></td></a></tr>
                        </table>
                      </td>
                      <td></td>
                      <%}%>
                      <%if (INQUIRE_KOREQRMT) {%>
                      <td bgcolor="#BBBBBB" style="cursor:hand" onmouseover="this.style.backgroundColor = '<%=iumColorB%>'" onmouseout="this.style.backgroundColor = '#BBBBBB'">
                        <table border="0" cellpadding="3" cellspacing="0">
                          <tr><a href="javascript:gotoPage('requestForm', 'viewKOReqMedLabDetail.do');"><td class="tabLink"><b>Requirements</b></td></a></tr>
                        </table>
                      </td>
                      <td></td>
                      <%}%>
                      <%if (INQUIRE_UWASSESS) {%>
                      <td bgcolor="#BBBBBB" style="cursor:hand" onmouseover="this.style.backgroundColor = '<%=iumColorB%>'" onmouseout="this.style.backgroundColor = '#BBBBBB'">
                        <table border="0" cellpadding="3" cellspacing="0">
                          <tr><a href="javascript:gotoPage('requestForm', 'viewUWAssessmentDetail.do');"><td class="tabLink"><b>U/W Assessment</b></td></a></tr>
                        </table>
                      </td>
                      <td></td>
                      <%//}%>
                      <%if (INQUIRE_DOCTORNOTES) {%>
                      <td bgcolor="#BBBBBB" style="cursor:hand" onmouseover="this.style.backgroundColor = '<%=iumColorB%>'" onmouseout="this.style.backgroundColor = '#BBBBBB'">
                        <table border="0" cellpadding="3" cellspacing="0">
                          <tr><a href="javascript:gotoPage('requestForm', 'viewDoctorsNotes.do');"><td class="tabLink"><b>Doctor's Notes</b></td></a></tr>
                        </table>
                      </td>
                      <td></td>
                      <%}%>
                    </tr>
                  </table>
                  <table cellpadding="0" cellspacing="0" border="1" bordercolor="<%=iumColorB%>" width="<%=HEADER_WIDTH%>">
                    <tr>
                      <td class="label2" bgcolor="<%=iumColorB%>" colspan="11">&nbsp;</td>
                    </tr>
                    <tr>
                      <td>
                        <table cellpadding="10" cellspacing="0" border="0" width="100%">
                          <tr>
                            <td>
                              <table border="0" bordercolor="<%=iumColorB%>" cellpadding="0" cellspacing="0" bgcolor="" width="<%=TABLE_WIDTH%>">
                                <tr>
                                  <td height="100%">
                                    <table border="0" cellpadding="2" cellspacing="0">
                                      <tr>
                                        <td valign="top">
                                          <table border="0" cellpadding="2" cellspacing="0">
                                            <tr>
                                              <td class="label2" valign="top">
                                                <b>DOCUMENTS</b><input type="hidden" name="docDesc">
                                                <table border="0" cellpadding="2" cellspacing="2" width="550" style="border: 1px solid #D1D1D1;" class="listtable1">
                                                  <tr>
                                                    <td class="headerRow6" width="300"><span class="required">*</span>DOCUMENT</td>
                                                    <td class="headerrow6" width="150"><span class="required">*</span>DATE REQUESTED</td>
                                                    <td class="headerrow6" width="150"><span class="required">*</span>DATE SUBMITTED</td>
                                                  </tr>
                                                  <%
                                                  int i=0;
                                                  String tr_class = "row1";
                                                  %>
                                                  <logic:iterate id="doc" name="detailForm" property="documents">
                                                  <%
                                                  if (i%2 == 0) {
                                                    tr_class = "row1";
                                                  } else {
                                                    tr_class = "row2";
                                                  }
                                                  %>
                                                  <tr>
                                                    <td class="<%=tr_class%>"><bean:write name="doc" property="docDesc"/></td>
                                                    <td class="<%=tr_class%>" align="center"><bean:write name="doc" property="dateRequested"/></td>
                                                    <td class="<%=tr_class%>" align="center"><bean:write name="doc" property="dateSubmit"/></td>
                                                  </tr>
                                                  <%i++;%>
                                                  </logic:iterate>
                                                  <% if (i == 0) { %>
                                                  <tr>
                                                    <td class="label2" colspan="2"><bean:message key="message.noexisting" arg0="documents"/></td>
                                                  </tr>
                                                  <%
                                                  i++;
                                                  }

                                                  if (i%2 == 0) {
                                                    tr_class = "row1";
                                                  } else {
                                                    tr_class = "row2";
                                                  }
                                                  %>
                                                  <tr>
                                                    <td class="<%=tr_class%>"><ium:list className="label2" styleName="width:295" listBoxName="docCode" type="<%=IUMConstants.LIST_DOCUMENT_TYPE%>" selectedItem="" disabled="true"/></td>
                                                    <td class="<%=tr_class%>"><input type="text" style="width:100" name="dateRequested" class="label2" disabled>&nbsp;<div id="doc_cal_req" style="display:none;position:relative; width:24px; height:22px"><a href="#" onClick="popUpWin('<%=contextPath%>/jsp/calendar.jsp?textbox=requestForm.dateRequested',290,155);"><img src="<%=contextPath%>/images/calendar.gif" border="0"></a></div></td>
                                                    <td class="<%=tr_class%>"><input type="text" style="width:100" name="dateSubmit" class="label2" disabled>&nbsp;<div id="doc_cal" style="display:none;position:relative; width:24px; height:22px"><a href="#" onClick="popUpWin('<%=contextPath%>/jsp/calendar.jsp?textbox=requestForm.dateSubmit',290,155);"><img src="<%=contextPath%>/images/calendar.gif" border="0"></a></div></td>
                                                  </tr>
                                                </table>
                                                <br>
                                                <table border="0" cellpadding="0" cellspacing="0" width="450">
                                                  <tr id="addDocument">
                                                    <td align="left" colspan="2"><input class="button1" type="button" value="Add Document" onClick="enableDocumentDetails(); setDefaultFocus();" <%=CREATE_REQUEST%>></td>
                                                  </tr>
                                                  <tr id="saveCancelDoc">
                                                    <td align="left">
                                                      <input class="button1" type="button" value="Save" onClick="saveDocument();">&nbsp;
                                                      <input class="button1" type="button" value="Cancel" onClick="disableDocumentDetails();">
                                                    </td>
                                                  </tr>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td class="label2">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td class="label2">
                                                <b>ACTIVITY LOG</b>
                                                <table border="0" cellpadding="2" cellspacing="2"  width="<%=TABLE_WIDTH%>" style="border: 1px solid #D1D1D1;" class="listtable1">
                                                  <tr>
                                                    <td class="headerrow6">DATE</td>
                                                    <td class="headerrow6">ACTIVITY</td>
													<td class="headerrow6">ELAPSED DAYS</td>
													<td class="headerrow6">ASSIGNED TO</td>
                                                    <td class="headerrow6">BY</td>
                                                  </tr>
                                                  <%
                                                  i=0;
                                                  %>
                                                  <logic:iterate id="act" name="detailForm" property="activities">
                                                  <%
                                                  if (i%2 == 0) {
                                                    tr_class = "row1";
                                                  } else {
                                                    tr_class = "row2";
                                                  }
                                                  %>
                                                  <tr>
                                                    <td class="<%=tr_class%>"><bean:write name="act" property="activityDate"/></td>
                                                    <td class="<%=tr_class%>"><bean:write name="act" property="activityName"/></td>
                                                    <td class="<%=tr_class%>" style="align:right"><bean:write name="act" property="lapseTime"/></td>
                                                    <td class="<%=tr_class%>"><bean:write name="act" property="assignedTo"/></td>
                                                    <td class="<%=tr_class%>"><bean:write name="act" property="userName"/></td>
                                                  </tr>
                                                  <%i++;%>
                                                  </logic:iterate>
                                                  <% if (i == 0) { %>
                                                  <tr>
                                                    <td class="label2" colspan="3"><bean:message key="message.noexisting" arg0="activity logs"/></td>
                                                  </tr>
                                                  <% } %>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td class="label2">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td class="label2" valign="top" colspan="2">
                                                <div valign="top" class="label2"><b>Remarks</b></div>
                                                <textarea rows="7" cols="54" class="label2" name="remarks" onkeyup="this.value=this.value.slice(0, 255);"><bean:write name="detailForm" property="remarks"/></textarea>
                                              </td>
                                            </tr>
                                          </table>
                                        </td>
                                      </tr>
                                    </table>
                                  </td>
                                </tr>
                              </table>
                            </td>
                          </tr>
                        </table>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
              <%} else {%>
               <tr valign="top">
                <td width="100%" height="100%" valign="top">
                  <table border="0" cellpadding="1" height="100" cellspacing="0">
                    <tr>
                      <td>&nbsp;</td>
                    </tr>
                  </table>
               </td>
               </tr>

              <% } %>
              <% if (!INQUIRE_REQUEST ) {
                    if (INQUIRE_CDS) { %>
                     <script language="Javascript">
                      gotoPage('requestForm', 'viewCDSDetail.do')
                     </script>
                    <%} else if (INQUIRE_KOREQRMT) { %>
                     <script language="Javascript">
                      gotoPage('requestForm', 'viewKOReqMedLabDetail.do')
                     </script>
                    <%} else if (INQUIRE_UWASSESS) { %>
                     <script language="Javascript">
                      gotoPage('requestForm', 'viewUWAssessmentDetail.do')
                     </script>
                    <%} else if (INQUIRE_DOCTORNOTES) { %>
                     <script language="Javascript">
                      gotoPage('requestForm', 'viewDoctorsNotes.do')
                     </script>
                    <%} else { %>
                      &nbsp;
                    <%}%>
              <%}%>
              <tr>
                <td colspan="10" class="label5">� 2003 Sun Life Financial. All rights reserved.</td>
              </tr>
              <tr>
                <td colspan="10" class="label5">&nbsp;</td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </form>
  </body>
</html>