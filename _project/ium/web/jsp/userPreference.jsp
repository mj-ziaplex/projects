<%@ page language="java" import="java.util.*, com.slocpi.ium.ui.form.*, com.slocpi.ium.util.*, com.slocpi.ium.ui.util.*, com.slocpi.ium.data.*" buffer="1024kb"%>
<%@ taglib uri="/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/ium.tld" prefix="ium" %>
<%@ taglib uri="/struts-html.tld" prefix="html" %>
<%@ taglib uri="/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/ium.tld" prefix="ium" %>
<%
/* Andre Ceasar Dacanay - css enhancement - start */
String iumCss = "";
String iumColorB = "";
String companyCode = "";
try {
	ResourceBundle rbWMSSP = ResourceBundle.getBundle("dbconfig");
	iumCss = rbWMSSP.getString("IUM_CSS_VERSION");
	companyCode = iumCss;
	iumCss = "_"+iumCss;
} catch (Exception e){}
if(iumCss.equals("_CP")){
	iumColorB="#2A4C7C";
	companyCode="";
}else if(iumCss.equals("_GF")){
	iumColorB="#303A77";
	companyCode = companyCode+" ";
}else{
	iumColorB="#2A4C7C";
}
/* Andre Ceasar Dacanay - css enhancement - end */
%>
<%String contextPath = request.getContextPath();

String frPage = (String)request.getParameter("frPage");
if(frPage==null) frPage = " ";
StateHandler sessionHandler = new StateHandler();
Access access = new Access();
%>
<jsp:useBean id="userPreference" scope="request" class="com.slocpi.ium.ui.form.UserPreferenceForm" />


<html>
  <head>
    <title><%=companyCode%>Integrated Underwriting and Medical System</title>
    <link href="<%=contextPath%>/css/pw_main.css" rel="stylesheet" type="text/css">
    <link href="<%=contextPath%>/css/main<%=iumCss%>.css" rel="stylesheet" type="text/css">
    <link href="<%=contextPath%>/css/styleCommon.css" rel="stylesheet" type="text/css">
    <script language="JavaScript" src="<%=contextPath%>/js/dhtml_init.js"></script>
    <script language="JavaScript" src="<%=contextPath%>/js/navigation.js"></script>
    <script language="Javascript">
	<!--
    	function updateSettings() {
    		var form = document.forms[0];
    		form.actionType.value = "update";
    	}
    	//-->
    </script>

  </head>

  <body leftMargin="0" topmargin="0" marginwidth="0" marginheight="0" onload = "">
    <table border="0" cellpadding="0" cellspacing="0" width="100%" height="100%">
      <tr vAlign="top"> 
            <table width="100%" cellpadding="3" cellspacing="5" border="0">
 
             
              <tr>
              <td colspan="2" class="error">
				<html:errors/>
			  </td>
			  </tr>					
              <tr>
                <td class="label2"><b>Notification Settings</b>              
                </td>
              </tr>
              <tr valign="top"> 
                <td> 
                  <!--- START OF BODY -->
                  <form name="frm" action="<%=contextPath%>/configureUserPreference.do" method="post">
                  	<input type="hidden" name="actionType" value=""/>
                    <table border="0" cellpadding=10 cellspacing=0 bgcolor="" width="600">
                      <tr>
                        <td colspan="2" height="100%">
                          <table border="0" width="600">
                            <tr>                              
                              <logic:present name="userPreference" property="preferences">
                              <logic:iterate id="lobEvents" name="userPreference" property="preferences">
                              <table border="1" bordercolor="<%=iumColorB%>"  cellspacing=0 bgcolor="" width="600" class="listTable1">
                              <tr class="label2"><b><ium:description type="<%=IUMConstants.LIST_LOB%>" queryCode="<%=((Map.Entry)lobEvents).getKey()%>"/></b><br>
                              <td class="label2">
			      				<table width="600"><tr class="label2">
			      
						      <%int i=0;%>
						      <%String checked = "";%>
						      <logic:iterate id="prefData" name="lobEvents" property="value">
						      <%if (i%3==0) {%>
								<tr class="label2">
							  <%}%>			      									      	
			                  <td width="200">
						      <%if (IUMConstants.YES.equals(((NotificationSettingData)prefData).getNotificationInd())) {%>
								<%checked="checked";%>
							   <%}%>				
						      <input type="checkbox" name="events" value="<bean:write name="prefData" property='eventId'/>" <%=checked%>><bean:write name="prefData" property="eventName"/></td>
						      <%checked="";%>                                                          
						      <%if (i%3==2) {%>
								<tr class="label2">
							  <%}%>
						      <%i++;%>				
			                  </logic:iterate>
						      
						      </table>
						      </tr>	
			                  </td>
						      </tr>
                              </table><br>
                              </logic:iterate>
                              </logic:present>
				<logic:empty name="userPreference" property="preferences">
					<td class="label2">There are no notification events mapped for this user.</td>
				</logic:empty>
                            </tr>                                                        
                            <tr>
                              <td colspan="6" align="right">                              	
				<logic:notEmpty name="userPreference" property="preferences">
                                <input type="submit" value="Save" class="button1" onclick='javascript:updateSettings();'>&nbsp;
                                <input type="button" value="Cancel" class="button1"  onclick='window.close();'>
				</logic:notEmpty>
				
		                       </td>
                            </tr>
                          </table>
                        </td>
                      </tr>
                    </table>
                  </form>
                  <!--- END OF BODY -->
                </td>
              </tr>
            </table>
      </tr>
    </table>    
  </body>
</html>