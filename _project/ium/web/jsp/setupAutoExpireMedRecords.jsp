<%@ page language="java" import="com.slocpi.ium.util.*" buffer="12kb"%>
<%@ taglib uri="/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/ium.tld" prefix="ium" %>
<%@page language="java" import="java.util.*" %>
<%String contextPath = request.getContextPath();%>
<%
/* Andre Ceasar Dacanay - css enhancement - start */
String iumCss = "";
String iumColorB = "";
String companyCode = "";
try {
	ResourceBundle rbWMSSP = ResourceBundle.getBundle("dbconfig");
	iumCss = rbWMSSP.getString("IUM_CSS_VERSION");
	companyCode = iumCss;
	iumCss = "_"+iumCss;
} catch (Exception e){}
if(iumCss.equals("_CP")){
	iumColorB="#2A4C7C";
	companyCode="";
}else if(iumCss.equals("_GF")){
	iumColorB="#303A77";
	companyCode = companyCode+" ";
}else{
	iumColorB="#2A4C7C";
}
/* Andre Ceasar Dacanay - css enhancement - end */
%>
<jsp:useBean id="autoExpireForm" scope="request" class="com.slocpi.ium.ui.form.AutoExpireForm"/>
<html>
<body>
<head>
<title><%=companyCode%>Integrated Underwriting and Medical System</title>
<link href="<%=contextPath%>/css/pw_main.css"     rel="stylesheet" type="text/css">
<link href="<%=contextPath%>/css/main<%=iumCss%>.css"        rel="stylesheet" type="text/css">
<link href="<%=contextPath%>/css/styleCommon.css" rel="stylesheet" type="text/css">
<script language="JavaScript" src="<%=contextPath%>/js/dhtml_init.js"></script>
<script language="JavaScript" src="<%=contextPath%>/js/navigation.js"></script>
<script language="JavaScript" src="<%=contextPath%>/jsp/sl_left_nav.jsp"></script>
<script language="JavaScript">
function autoExpire(contextPath) {
    var form = document.forms[0];
    if (form.schedule.value == "") {
       alert("<bean:message key="error.field.requiredselection" arg0="Schedule"/>");
       form.schedule.focus();
       return;
    }
    form.action = contextPath + "/autoExpireMedRecords.do";
    form.submit();
}

function expireMedicalRecords(contextPath) {
    var form = document.forms[0];
    form.action = contextPath + "/expireMedRecords.do?mode=expire";
    form.submit();
}

function disableExpire() {
    var form     = document.forms[0];
    var selIndex = form.schedule.options.selectedIndex;
    if (form.schedule.options[selIndex].value == "nosched") {
        //form.expireBtn.disabled = true;
        //form.saveBtn.disabled   = true;
    }
    else {
        form.expireBtn.disabled = false;
        form.saveBtn.disabled   = false;    
    }
}
</script>
</head>

<BODY leftMargin=0 topmargin=0 marginwidth=0 marginheight=0>
<table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%">
<tr><td width="100%" colspan="2">
<jsp:include page="header.jsp" flush="true"/>

          <table border="0" cellpadding="0" cellspacing="0" width="100%" height="100%">
            <tr valign="top">
              <td background="<%=contextPath%>/images/bg_yellow.gif" id="leftnav" valign="top" width="147">
                <img height="9" src="<%=contextPath%>/images/sp.gif" width="147"><br>
                <img height="30" src="<%=contextPath%>/images/sp.gif" width="7"> <br>
                <img height="7" src="<%=contextPath%>/images/sp.gif" width="147"><br>
                <div id="level0" style="position: absolute; visibility: hidden; width: 100%; z-index: 500"> 
        <SCRIPT language=javascript>
			<!--
				document.write(writeMenu(''))
				if (IE4) { document.write(writeDiv()) };
				
			//-->
			</SCRIPT>
      </DIV>
      <DIV id=NSFix style="POSITION: absolute; VISIBILITY: hidden; Z-INDEX: 999">&nbsp;</DIV>
      <!-- Netscape needs a hard-coded DIV to write dynamic DIVs --> 
      <SCRIPT language=javascript>
                        <!--
                                if (NS4) { document.write(writeDiv()) }
                        //-->
                        </SCRIPT>
                        <SCRIPT>
                        <!--
                                initMenu();
                        //-->
                        </SCRIPT>
      <!-- END MENU CODE --> </td>

    <td width="100%"> 
      <div align="left"> 
        <table border="0" cellpadding="0" width="100%" cellspacing="0">
          <tr> 
            <td width="100%"> 
              <div align="left"> 
                <table border="0" cellpadding="3" cellspacing="0" width="100%" height="27"  background="images/sectionbkgray.jpg">
                  <tr> 
                    <td valign="top"> 
                       <span class="main">&nbsp;Integrated Underwriting and Medical System </span>
                    </td>
                  </tr>
                </table>
              </div>
            </td>
          </tr>
          <tr> 
            <td width="100%" colspan=2>&nbsp;</td>
          </tr>
        </table>
        <table width="100%" cellpadding="3" cellspacing="5" border="0">
          <tr> 
            <td class="label2"><b>Setup Expire Medical Records</b>              
            </td>
          </tr>
          <tr valign="top"> 
            <td> 
<!--- START OF BODY -->
<form name="frm" method="post">
<table border="1" bordercolor="<%=iumColorB%>" cellpadding=10 cellspacing=0 bgcolor="" width="500" class="listtable1">
        <tr><td colspan="2" height="100%">
        <table>
        <tr>
        <td class="label2"><b>Expiration Schedule</b><td/>
        <td>
        
            <ium:list className="label2" listBoxName="schedule" type="<%=IUMConstants.LIST_SCHEDULE%>" styleName="width:200" selectedItem="<%= autoExpireForm.getSchedule() %>" onChange="disableExpire();"/>
        </td>
        </tr>
        <tr>
        <td class="label2" valign="top"><b>Notes</b><td/>
        <td><textarea class="label2" rows="5" cols="51" name="notes"><bean:write name="autoExpireForm" property="notes"/></textarea></td>
        </tr>
        <tr><td colspan="4" align="right">&nbsp;</td></tr>

        <tr><td colspan="4" align="left">
        <input type="button" value="Expire"      class="button1" name="expireBtn" onclick="expireMedicalRecords('<%=contextPath%>');">&nbsp;
		<input type="button" value="Save"        class="button1" name="saveBtn"   onclick="autoExpire('<%= contextPath %>');">&nbsp;
        <input type="button" value="Cancel"      class="button1" name="cancelBtn" onclick="gotoPage('frm','<%=contextPath%>/admin.do');">

        </td></tr>
        
        
        </table>
</form>
</td></tr>
<!--- END OF BODY -->
</table>
</body>
</html>