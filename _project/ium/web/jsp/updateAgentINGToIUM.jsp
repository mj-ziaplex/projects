<!-- Update Agent Details from Ingenium to IUM - Andre Ceasar Dacanay -->
<html>

<head>
<title>Integrated Underwriting and Medical System</title>
<%
String message = request.getAttribute("message")!=null ? (String) request.getAttribute("message") : "";
String message2 = request.getAttribute("message2")!=null ? (String) request.getAttribute("message2") : "";
String updateTag = request.getAttribute("updateTag")!=null ? (String) request.getAttribute("updateTag") : "";
%>
<script language="JavaScript">
function reloadOpener(){
<%if (updateTag.equals("")){%>
	window.close();
<%}else{%>
	window.opener.document.location.reload(true);
<%}%>
}
</script>
</head>
<body leftMargin="0" topmargin="0" marginwidth="0" marginheight="0" >
<table width="375" height="150">
<tr>
<td colspan="2">
Message Details
</td>
</tr>
<tr>
<td>
<%=message%>
</td>
</tr>
<tr>
<td>
<%=message2%>
</td>
</tr>
<tr>
<td>
&nbsp;
</td>
</tr>
<tr>
<td align="center" colspan="2">
<input class="button1" type="button" name="btnReload" value="close" onclick="reloadOpener();">
</td>
</tr>
</table>
</body>
</html>