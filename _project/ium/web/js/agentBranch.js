/*
	 This include file assumes that three arrays have been created:
	 1. Agent User IDs - aAgents
	 2. Agent Branch IDs - aBranches
	 3. Agent User Names - aAgentNames
	 
	 These arrays are used in automatically assigning a branch
	 given an agent or filtering the agent list given a branch.
	 
	 The indexes of these arrays correspond to the element
	 of the each other's arrays' with the same index.
		
*/
 // assign default value for branch
 function assignBranch(agt, off) {
 /*
  	agt = SELECT object that lists Agents
 	off = SELECT object that lists Branches
 */	
  // change branch to current agent's branch
  for (i=0; i<aAgents.length; i++) {
   if (aAgents[i] == agt.options[agt.selectedIndex].value) {
    if (aBranches[i] != "") {
	 off.value = aBranches[i];
	}
   }
  }
  return true;
 }
	 
 // assign default value for branch
 function filterAgentList(agt, off) {
 /*
  	agt = SELECT object that lists Agents
 	off = SELECT object that lists Branches
 */	
  curAgent = agt.value;

  // clear the agent list
  for (i=agt.length; i>0; i--) {
   agt.remove(i);
  }

  // don't filter anything if no branch is selected
  if (off.selectedIndex == 0) { 
	  for (i=0; i<aBranches.length; i++) {
		agt.options.add(new Option(aAgentNames[i], aAgents[i]));
	  }  	
  }
  else {
	  // filter the list of agents	 
	  for (i=0; i<aBranches.length; i++) {
	   if (aBranches[i] == off.options[off.selectedIndex].value) {
		agt.options.add(new Option(aAgentNames[i], aAgents[i]));
	   }
	  }
  }
  
  agt.selectedIndex = 0;		
		
  return true;
 }