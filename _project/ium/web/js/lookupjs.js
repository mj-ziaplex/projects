    var arr = new Array();

    function sortLists(fList,tList) {
		var form       = document.forms[0];    
		if (form.fList != null) {
            sort(form.fList); 
        }
        if (form.tList != null) {
            sort(form.tList);
        }
    }
        
	
	
	function add(fList,tList) {
	    var form = document.forms[0];
	    var len  = fList.options.length;
	    if (len == 0) {
            alertAddError();
	    }
	    else {
	        addList(fList,tList);
 	    }
 	    
	}
	
	
	
	function addList(fList,tList) {
	    var form     = document.forms[0];	    	
        var selected = isCodeSelected(fList);
        if (!selected) {
            alert("Please select a code to add.");	 
            fList.focus();               
        }
	    else {
   	        move(fList, tList);	   	        
   	        sort(tList);   	        
    	}
	}
	
	function removeList(fList,tList) {
	    var form = document.forms[0];	    	
        var selected = isCodeSelected(tList);
        if (!selected) {
            alert("Please select a role to remove.");
            tList.focus();
        }
        else {
            move(tList,fList);	            
            sort(fList);
        }	
	}
	
	function remove(fList,tList) {
	    var form = document.forms[0];
	    var len  = tList.options.length;
	    if (len == 0) {
           alertRemoveError();
	    }
	    else {	    
	        removeList(fList,tList);
    	}
        //enableButtons(); 	
	}

    function isCodeSelected(objList) {
	    var form     = document.forms[0];	    	        
        var selected = false;
        var len      = objList.options.length;
        var i        = 0;
        
  	    while (i < len) {
	        if (objList.options[i].selected == true) {
	            selected = true;	            
	        }	        
	        i++;
  	    }
  	    return selected;
    }

   
    	
	function alertAddError() {
       alert("There is no code to add.");	
	}
	
	function alertRemoveError() {
       alert("There is nothing to remove.");	
	}
	
	function alertInvalidCodeMessage(message) {
	   if ((message != "null") && (message != "")) {	       
          alert("Some of the entered codes are invalid and have been removed \n from the selected list.");
	   }
	}
	
	
	
    function trimString (str) {
       while (str.charAt(0) == ' ')
         str = str.substring(1);       
       while (str.charAt(str.length - 1) == ' ')
         str = str.substring(0, str.length - 1);       
       return str;
    }
    
    

    function move(fbox, tbox) {
       var arrFbox = new Array();
       var arrTbox = new Array();
       var arrLookup = new Array();
       var i;
       for (i = 0; i < tbox.options.length; i++) {
           arrLookup[tbox.options[i].text] = tbox.options[i].value;
           arrTbox[i] = tbox.options[i].text;
       }
       var fLength = 0;
       var tLength = arrTbox.length;
       for(i = 0; i < fbox.options.length; i++) {
          arrLookup[fbox.options[i].text] = fbox.options[i].value;
          if (fbox.options[i].selected && fbox.options[i].value != "") {
             arrTbox[tLength] = fbox.options[i].text;
             tLength++;
          }
          else {
             arrFbox[fLength] = fbox.options[i].text;
             fLength++;
         }
       }
       //arrFbox.sort();
       //arrTbox.sort();
       fbox.length = 0;
       tbox.length = 0;
       var c;
       for(c = 0; c < arrFbox.length; c++) {
          var no = new Option();
          no.value = arrLookup[arrFbox[c]];
          no.text = arrFbox[c];
          fbox[c] = no;
       }
       for(c = 0; c < arrTbox.length; c++) {
          var no = new Option();
          no.value = arrLookup[arrTbox[c]];
          no.text = arrTbox[c];
          tbox[c] = no;
       }
    }
    
    function move2(fbox, tbox) {      
      var flen = fbox.options.length;
      var tlen = tbox.options.length;
      var fvalue;
      for (var i=0;i<flen;i++) {
      	fvalue = fbox.options[i].value;
      	for (var k=0;k<tlen;k++) {
      		if (fvalue == tbox.options[k].value) {
      			fbox.options[i].text = tbox.options[k].text;
      			tbox.options.remove(k);
      			break;
      		}
      	}
      }	   
    }
    
    function consolidateList () {
    	var form = document.forms[0];
    	if (form.actionType.value == "maintain") {
    		move2(form.roleIL2, form.roleIL1);
    		move2(form.roleGL2, form.roleGL1);
    		move2(form.rolePN2, form.rolePN1);
    	}    	
    }
    
    function sort(cBox)
    {
	   var arrCboxValue = new Array();
	   var arrCboxText = new Array();
       
       for(i=0; i<cBox.options.length; i++)
       {
       	 arrCboxValue[i] = cBox.options[i].value;  
       }
       
       arrCboxValue.sort();
       
       for(i=0; i<cBox.options.length; i++)
       {
       	 for(j=0; j<arrCboxValue.length; j++)	
       	 {      
       	 	if(arrCboxValue[j] == cBox.options[i].value)
       	 	{
       	 		arrCboxText[j] = cBox.options[i].text;       	 		
       	 		break;
       	 	}  
       	 }	
       }
       
       //return to selectbox
       cBox.length = 0;     
       var c;
       for(c = 0; c < arrCboxValue.length; c++) {
          var no = new Option();
          no.value = arrCboxValue[c];
          no.text = arrCboxText[c];
          cBox[c] = no;
       }              
    }
        
    
    function whenCodeSelected() {
       alert("error");
    }
    