$(document).ready(function() {
	$("textarea[maxlength]").bind('input propertychange', function(event) {  
	    var maxLength = $(this).attr('maxlength');
	    var length = $(this).val().length;
	    if (length >= maxLength) { 
	        var removeWhitespace = $(this).val().substring(0, maxLength);
	        removeWhitespace = removeWhitespace.replace(/\n/g, ' ');
	        $(this).val(removeWhitespace);
	    }  
    })  
});

$(document).ready(function() {
	$("#commentArr").keypress(function(event) {  		
		if ($('#commentArr').val().length == 4000) {
			event.preventDefault();			
		}
	})
});