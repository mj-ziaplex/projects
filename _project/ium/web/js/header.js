
headLinkImage = new Array();
headLinkImage['worldwide'] = new Array(2);
headLinkImage['worldwide'][0] = new Image();
headLinkImage['worldwide'][1] = new Image();
headLinkImage['worldwide'][0].src = 'images/head_wrldwide.gif';
headLinkImage['worldwide'][1].src = 'images/head_wrldwide_over.gif';

headLinkImage['sitemap'] = new Array(2);
headLinkImage['sitemap'][0] = new Image();
headLinkImage['sitemap'][1] = new Image();
headLinkImage['sitemap'][0].src = 'images/head_sitemap.gif';
headLinkImage['sitemap'][1].src = 'images/head_sitemap_over.gif';

headLinkImage['search'] = new Array(2);
headLinkImage['search'][0] = new Image();
headLinkImage['search'][1] = new Image();
headLinkImage['search'][0].src = 'images/head_search.gif';
headLinkImage['search'][1].src = 'images/head_search_over.gif';

headLinkImage['contactus'] = new Array(2);
headLinkImage['contactus'][0] = new Image();
headLinkImage['contactus'][1] = new Image();
headLinkImage['contactus'][0].src = 'images/head_contact.gif';
headLinkImage['contactus'][1].src = 'images/head_contact_over.gif';

headLinkImage['legal'] = new Array(2);
headLinkImage['legal'][0] = new Image();
headLinkImage['legal'][1] = new Image();
headLinkImage['legal'][0].src = 'images/head_legal.gif';
headLinkImage['legal'][1].src = 'images/head_legal_over.gif';

function showHeadLinkImage (imageName,state) {
  document.images[imageName].src = headLinkImage[imageName][state].src;
}


function showHeader () {
  tempText = "<TABLE background='images/bg_blue.gif' border=0 cellPadding=0 cellSpacing=0 width=781 height=5>"
           + "<TBODY>"
           + "<TR vAlign=top>"
           + "<TD><IMG border=0 height=70 src='images/head_logo.gif' width=351></TD>"
           + "<TD><IMG height=1 src='images/sp.gif' width=1></TD>"
           + "<TD align=right noWrap>"
           + "<IMG border=0 height=42 src='images/head_clouds.jpg' width=364><BR>"
           + "<A href='http://www.sunlife.com/' onmouseout=showHeadLinkImage('worldwide',0) onmouseover=showHeadLinkImage('worldwide',1) target=_blank>"
           + "<IMG border=0 height=29 name=worldwide src='images/head_wrldwide.gif' width=77></A>"
           + "<IMG border=0 height=29 src='images/head_menu_div.gif' width=25>"
           + "<A href='/search.asp' onmouseout=showHeadLinkImage('search',0) onmouseover=showHeadLinkImage('search',1)>"
           + "<IMG border=0 height=29 name=search src='images/head_search.gif' width=48></A>"
           + "<IMG border=0 height=29 src='images/head_menu_div.gif' width=25>"
           + "<A href='/sitemap.asp' onmouseout=showHeadLinkImage('sitemap',0) onmouseover=showHeadLinkImage('sitemap',1)>"
           + "<IMG border=0 height=29 name=sitemap src='images/head_sitemap.gif'></A>"
           + "<IMG border=0 height=29 src='images/head_menu_div.gif' width=25>"
           + "<A href='/contactus.asp' onmouseout=showHeadLinkImage('contactus',0) onmouseover=showHeadLinkImage('contactus',1)>"
           + "<IMG border=0 height=29 name=contactus src='images/head_contact.gif' width=80></A>"
           + "<IMG border=0 height=29 src='images/head_menu_div.gif' width=25>"
           + "<A href='/legal.asp' onmouseout=showHeadLinkImage('legal',0) onmouseover=showHeadLinkImage('legal',1)>"
           + "<IMG border=0 height=29 name=legal src='images/head_legal.gif' width=39></A>"
           + "<IMG border=0 height=1 src='images/sp.gif' width=25>"
           + "</TD>"
           + "</TR>"
           + "</TBODY>"
           + "</TABLE>";

  document.write(tempText);
}

showHeader();
