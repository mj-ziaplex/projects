// ---------------------------------------------------------
// check for DHTML browsers
var NS4, IE4
if (parseInt(navigator.appVersion) >= 4) {
	if (navigator.appName == "Netscape") {
		NS4 = true
		gBrowser = "NS"
	}
	else {
		IE4 = true
		gBrowser = "IE"
	}
} else {
	alert("browser not supported");
}

// ---------------------------------------------------------
// equivalence vars for all javascripts
var gHidden  = (NS4) ? "hide" : "hidden";
var gVisible = (NS4) ? "show" : "visible";
var gTopINT  = (NS4) ? ".top" : ".pixelTop";
var gWidth   = (NS4) ? ".clip.width" : ".clientWidth"
var gHeight  = (NS4) ? ".clip.height" : ".clientHeight";
var gDivRef  = (NS4) ? "document." : "document.all.";
var gStyle   = (NS4) ? "" : ".style";
var trsp     = (NS4) ? null : 'transparent'
var timeID   = 0

// ---------------------------------------------------------
// browser reload functions
if (NS4) { // trap original browser size for later...
	origWidth = innerWidth;
	origHeight = innerHeight;
}
function handleResize() {
	if (NS4) { // NS - only if browser was REALLY resized!!!!
		if (innerWidth != origWidth || innerHeight != origHeight) location.reload();
	} else { // IE - much better browser
		location.reload();
		return false;
	}
}
if (NS4) onresize = handleResize;
