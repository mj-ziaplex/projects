//variables used for the dates
var mm = "";
var dd = "";
var yyyy = "";
var dow = "";
var currYear = "";
var currDay = "";
var currMonth = "";
var mDays = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

//layer id
var layerID = "";

//textbox where the date will be rendered
var txtObject = "";

/*********************************************
 ********** FOR USER CONFIGURATIONS **********
 *********************************************/
 
//Month name use for display
var monthname = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

//Complete month name use for display
var monthnamecomp = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
 
//days of the week use for display
var weekname = ["S", "M", "T", "W", "T", "F", "S"];
 
//input javascripts to be used when clicking a date
var dayScript = "returnDate();";
 
//input style for the days with link
var dayClass = "links1";
 
//input style for the days without link
var dayClassNoLink = "calendar_text";
 
//input style for the title/header
var titleClass = "title4";

//input style for the title/header
var listClass = "label2";
 
//input how many years after the current to be 
//displayed in the selection box
var yearNext = 1;
 
//input how many years before the current to be 
//displayed in the selection box
var yearPrev = 50;

 
/*********************************************
 *********************************************/
 
 
//this will be called by the script inputting a parameter containing the layerId
function showCalendar(id, obj) {
	layerID = id;
	txtObject = obj;
	parseDate(); 
	var txt = createCalendar();  
	printCalendar(layerID, txt);
}
 

//this is a customized function in choosing which dates will be disabled.
function isDisabled(day) {
/*
	if(yyyy < currYear) {    
		return false;
	}
	else if (yyyy == currYear) {
		if (mm < currMonth) {
			return false;
		} else if (mm == currMonth) {   
			if(day <= currDay) {
				return false;
			}
		}
	}
	return true;
*/
	return false;
}
 
 
function parseDate() {
	var d = "";
 
	if (document.forms[0] != null) {
		d = new Date(document.forms[0].year.value, document.forms[0].month.value, 1);  
	}
	else {
		d = new Date();
		mm = d.getMonth();  
		yyyy = d.getFullYear(); 
		d = new Date(yyyy, mm, 1);  
	}
 
	mm = d.getMonth();
	dd = d.getDate();
	yyyy = d.getFullYear();  
	dow = d.getDay();
 
	if(isLeapYear(yyyy)) {
		mDays[1] = 29;
	}
	else {
		mDays[1] = 28;
	}
 
	d = new Date();
	currYear = d.getFullYear();  
	currMonth = d.getMonth();
	currDay = d.getDate(); 
}


//prints calendar to the page
function printCalendar(layerID,txt){
	if (document.getElementById) {
		document.getElementById(layerID).innerHTML = txt;
	}
	else if (document.all) {
		document.all["calendarDiv"].innerHTML = txt;
	}
	else if (document.layers) {
		with(document.layers[layerID].document) {
			open();
			write(txt);
			close();
		}
	}
}
 
 
function createCalendar() {
	var header = createHeader();
	var body = createBody();
	var footer = createFooter();
 
	return header + body + footer;
}
 
 
function createHeader() {
	var txt = "";
  
	txt = txt + "<form method=\"POST\" name=\"frmCalendar\">"; 
	txt = txt + "<TABLE BGCOLOR=\"#EEEEEE\" BORDER=0><TR><TD ALIGN=CENTER width=\"250\">"
	txt = txt + "<FONT class=\""+titleClass+"\">" + monthnamecomp[mm] + " " + yyyy + "</FONT>"; 
	txt = txt + "&nbsp;&nbsp;";
	txt = txt + "<select name=\"month\" class=\""+listClass+"\" onchange=\"showCalendar(\'"+layerID+"\', \'"+txtObject+"\')\">"; 
	for (i=0; i<monthname.length; i++) {  
		if(i == mm){
			txt = txt + "<option value=\""+i+"\" selected>" + monthname[i]; 
		}
		else {
			txt = txt + "<option value=\""+i+"\">" + monthname[i];
		}
	}
	txt = txt + "</select>";
	txt = txt + "&nbsp;&nbsp;";
	txt = txt + "<select name=\"year\" class=\""+listClass+"\" onchange=\"showCalendar(\'"+layerID+"\', \'"+txtObject+"\')\">";
 
	// prev year 
	for (i=(currYear-yearPrev); i<currYear; i++) {  
		if(i == yyyy) {
			txt = txt + "<option value=\""+i+"\" selected>" + i; 
		}
		else {
			txt = txt + "<option value=\""+i+"\">" + i; 
		}
	}
 
	//current till next years
	for (i=currYear; i<=(currYear+yearNext); i++) {  
		if (i == yyyy) {
			txt = txt + "<option value=\""+i+"\" selected>" + i; 
		}
		else {
			txt = txt + "<option value=\""+i+"\">" + i; 
		}
	}
 
	txt = txt + "</select>";
	txt = txt + "</td></tr><TR><TD ALIGN=CENTER >";
	txt = txt + "<TABLE CELLSPACING=0 CELLPADDING=0 BORDER=1 BORDERCOLORDARK=\"#FFFFFF\" BORDERCOLORLIGHT=\"#C0C0C0\">";
	txt = txt + "<TR>";
	for(i=0; i<weekname.length; i++) {
		txt = txt + "<TD WIDTH=20 ALIGN=CENTER VALIGN=MIDDLE><FONT SIZE=2 COLOR=\"#000000\" FACE=\"ARIAL\"><B>";
		txt = txt + weekname[i];
		txt = txt + "</B></FONT></TD>";
	}
	txt = txt + "</tr>";
 
	return txt;
}
 
 
function createFooter() {
	var txt = "";
 
	txt = txt + "</table>";
	txt = txt + "</td></tr></table>";
	txt = txt + "</form>";
 
	return txt;
}

 
function createBody() {
	var txt = "";  
 
	pDow = 0;
	cnt = 0;
    
	for(i=1; i<=mDays[mm]; i++) {
		if (cnt == 0) {
			txt = txt + "<tr>";
		}
		while(pDow != dow) {
			txt = txt + "<TD WIDTH=\"20\" HEIGHT=\"5\" class=\""+dayClass+"\">&nbsp;</td>";   
			pDow++;
			cnt++;
		}
		txt = txt + "<TD WIDTH=\"20\" HEIGHT=\"5\" ALIGN=\"CENTER\" VALIGN=\"MIDDLE\">";

		var d = new Date();
		var currDay = d.getDate();
		var currMo = d.getMonth();
		var currYr = d.getFullYear();
   
		if(isDisabled(i)) {
			if ((currDay == i) && (currMo == mm) && (currYr == yyyy)) {
				txt = txt + "<FONT class=\""+dayClassNoLink+"\"><b>";
				txt = txt + i;
				txt = txt + "</b></FONT>";
			}
			else {
				txt = txt + "<FONT class=\""+dayClassNoLink+"\">";
				txt = txt + i;
				txt = txt + "</FONT>";
			}
		}
		else {
			if ((currDay == i) && (currMo == mm) && (currYr == yyyy)) {
				txt = txt + "<A HREF=\"#\" onclick=\"dd="+i+ "; " + dayScript + "\" class=\""+ dayClass + "\"><b>";
				txt = txt + i;    
				txt = txt + "</b></A>";
			}
			else {
				txt = txt + "<A HREF=\"#\" onclick=\"dd="+i+ "; " + dayScript + "\" class=\""+ dayClass + "\">";
				txt = txt + i;    
				txt = txt + "</A>";
			}
		}
   
		txt = txt + "</TD>";   
		cnt++;
		if(cnt >= 7) {
			cnt = 0;
			txt = txt + "</tr>";
		}
	}
 
	if(cnt > 0) {
		for(i=cnt; i<7; i++) {
			txt = txt + "<TD WIDTH=\"20\" HEIGHT=\"5\" class=\""+dayClass+"\">&nbsp;</td>";   
		}
		txt = txt + "</tr>";
	}
  
	return txt;
}

 
function isLeapYear(yr) {
	if (yr % 100 == 0) {
		if (yr % 400 == 0) {  
			return true; 
		}
	} else {
		if ((yr % 4) == 0) {
			return true;
		}
	}
	return false;
}


function returnDate() {
	var mo = monthname[mm];
	var day = dd;
	if (day < 10) {
		day = "0" + day; 
	}
	var yr = yyyy;
	var parentObj = eval("self.opener.document."+txtObject);
	parentObj.value = day + mo.toUpperCase() + yr;
	window.close();
	parentObj.focus();
}