var popupwin = null;
var popupwindow = null;
var popupwindow1 = null;
var popupwindow2 = null;
var popupwindow3 = null;
var popupwindow4 = null;

// open a new window
function popUpWin(url,w,h) {
	if (! window.focus) {
		return true; 
	}
	var scrw = screen.availWidth; 
	var scrh = screen.availHeight;
	var lefPos = (scrw-w)/2; 
	var topPos = (scrh-h)/2;
	var options = "width=" + w + ",height=" + h + ",";
	options += "top=" + topPos + ",left=" + lefPos + ",";
	options += "resizable=no,";
	options += "scrollbars=no,";
	options += "status=no,";
	options += "menubar=no,";
	options += "toolbar=no,";
	options += "location=no,";
	options += "directories=no";
	popupwin = window.open(url, 'ium_window', options);
	popupwin.focus();
}



function popUpWinScroll(url,w,h) {
	if (! window.focus) {
		return true; 
	}
	var scrw = screen.availWidth; 
	var scrh = screen.availHeight;
	var lefPos = (scrw-w)/2; 
	var topPos = (scrh-h)/2;
	var options = "width=" + w + ",height=" + h + ",";
	options += "top=" + topPos + ",left=" + lefPos + ",";
	options += "resizable=yes,";
	options += "scrollbars=yes,";
	options += "status=no,";
	options += "menubar=no,";
	options += "toolbar=no,";
	options += "location=no,";
	options += "directories=no";
	popupwin = window.open(url, 'ium_window', options);
	popupwin.focus();
}

function popUpWindow(url,w,h,resizable,scrollbars,status,menubar,toolbar,location,directories,windowNum,lefPosition,topPosition) {
	if (!window.focus) {
		return true; 
	}
	var scrw = screen.availWidth; 
	var scrh = screen.availHeight;
	
	var lefPos = (scrw-w)/2; 
	var topPos = (scrh-h)/2;
	
	if(lefPosition!=null && lefPosition!=''){
		//alert('lefPosition');
		lefPos = lefPosition;
	}
	
	if(topPosition!=null && topPosition!=''){
		//alert('topPosition');
		topPos = topPosition;
	}
	
	var options = "width=" + w + ",height=" + h + ",";
	options += "top=" + topPos + ",left=" + lefPos + ",";
	options += "resizable="+resizable+",";
	options += "scrollbars="+scrollbars+",";
	options += "status="+status+",";
	options += "menubar="+menubar+",";
	options += "toolbar="+toolbar+",";
	options += "location="+location+",";
	options += "directories="+directories+"";
	
	if(windowNum == '0'){
		popupwindow = window.open(url, 'popupwindow', options);
		popupwindow.focus();
	}else if(windowNum == '1'){
		popupwindow1 = window.open(url, 'popupwindow1', options);
		popupwindow1.focus();
	}else if(windowNum == '2'){
		popupwindow2 = window.open(url, 'popupwindow2', options);
		popupwindow2.focus();
	}else if(windowNum == '3'){
		popupwindow3 = window.open(url, 'popupwindow3', options);
		popupwindow3.focus();
	}else if(windowNum == '4'){
		popupwindow4 = window.open(url, 'popupwindow4', options);
		popupwindow4.focus();
	}else{
		popupwin = window.open(url, 'popupwin', options);
		popupwin.focus();
	}

}

// close window
function closePopUpWin() {
	try{
    if (popupwin && !popupwin.closed) {
    	popupwin.close();
    }
    }catch(e){}
    
try{ popupwin.close(); }catch(e){}	
try{ popupwindow.close(); }catch(e){}	
try{  popupwindow1.close(); }catch(e){}	
try{  popupwindow2.close(); }catch(e){}	
try{  popupwindow3.close(); }catch(e){}	
try{  popupwindow4.close(); }catch(e){}	
}

// close printerfriendly page
function closePrinterFriendly() {
	if (printerFriendly && !printerFriendly.closed) {
		printerFriendly.close();	
	}
}


function toggleAll(field) {
     if (document.requestFilterForm.refcheck.checked) {
         checkAll(field);
     } else {
          clearAll(field);
     }
} 


function selectAll() {

    var form = document.forms[0];
    if (form.selAll.checked) {
       if (form.index[0]) {        
          for (var i=0;i<form.index.length;i++) {
              form.index[i].checked = true;
          }
       }
       else {
          form.index.checked = true;
       }
    }
    else {
       if (form.index[0]) {        
          for (var i=0;i<form.index.length;i++) {
              form.index[i].checked = false;
          }
       }
       else {
          form.index.checked = false;
       }       
    }
}


function selectAllEnabled(form) {

    //var form = document.forms[0];
   
    if (form.selAll.checked) {
       if (form.index[0]) {        
          for (var i=0;i<form.index.length;i++) {
            if (form.index[i].disabled == false) {
              form.index[i].checked = true;
            }
          }
       }
       else {
          if (form.index.disabled == false) {
	          form.index.checked = true;
	      }
       }
    }
    else {
       if (form.index[0]) {        
          for (var i=0;i<form.index.length;i++) {
              form.index[i].checked = false;
          }
       }
       else {
          form.index.checked = false;
       }       
    }
}


function enable(object) {
     object.disabled = false;
} 
    
    
function disable(object) {
     object.disabled = true;
}    


function show(object) {
     object.style.visibility = "visible";
} 
    
    
function hide(object) {
     object.style.visibility = "hidden";
}


function gotoPage(form, URL) {
     var form = document.forms[form];
     form.action = URL;
     form.submit();
}

function imageOver(contextPath, imgname)
{
    document[imgname].src = contextPath + "/images/" + imgname + "_on.gif";   
}
 
function imageOut(contextPath, imgname)
{
    document[imgname].src = contextPath + "/images/" +  imgname+ "_off.gif";
}

function checkAll(field) {
	if (field.length == 0 || field.length == null){
		field.checked = true;
	} else {
		for (i = 0; i < field.length; i++) {
			if (field[i].disabled != true){
		    	field[i].checked = true ;
		    }
		}
	}
}


function clearAll(field) {
    if (field.length == 0 || field.length == null){
		field.checked = false;
	} else {
		for (i = 0; i < field.length; i++) {
	    	field[i].checked = false ;
		}
	}
}





function gotoScreen() {
   var ind    =  document.form1.selView.selectedIndex;
   var view = eval("document.form1.selView.options[" + ind + "].value");
    if (view == "jobid") {
        //document.getElementById("listFrame").src = "listTicketByJobId.html";
        window.location.replace("listTicketByJobId.html");
    }   
    if (view == "status") {
        //document.getElementById("listFrame").src = "listTicketByStatus.html";
        window.location.replace("listTicketByStatus.html");
    }
    if (view == "system") {
        //document.getElementById("listFrame").src = "listTicketBySystem.html";
        window.location.replace("listTicketBySystem.html");       
    }
    if (view == "client") {
        //document.getElementById("listFrame").src = "listTicketByClient.html";
        window.location.replace("listTicketByClient.html");
    }
    if (view == "causecode") {
        //document.getElementById("listFrame").src = "listTicketByCauseCode.html";
        window.location.replace("listTicketByCauseCode.html");
    }
    if (view == "date") {
        //document.getElementById("listFrame").src = "listTicketByDateOccurred.html";
        window.location.replace("listTicketByDateOccurred.html");
    }    
  }
  
    function createNewTicket() {
        window.location.replace("problemTicketForm.html");
    }
    

function move(fbox, tbox) {
var arrFbox = new Array();
var arrTbox = new Array();
var arrLookup = new Array();
var i;
for (i = 0; i < tbox.options.length; i++) {
arrLookup[tbox.options[i].text] = tbox.options[i].value;
arrTbox[i] = tbox.options[i].text;
}
var fLength = 0;
var tLength = arrTbox.length;
for(i = 0; i < fbox.options.length; i++) {
arrLookup[fbox.options[i].text] = fbox.options[i].value;
if (fbox.options[i].selected && fbox.options[i].value != "") {
arrTbox[tLength] = fbox.options[i].text;
tLength++;
}
else {
arrFbox[fLength] = fbox.options[i].text;
fLength++;
   }
}
arrFbox.sort();
arrTbox.sort();
fbox.length = 0;
tbox.length = 0;
var c;
for(c = 0; c < arrFbox.length; c++) {
var no = new Option();
no.value = arrLookup[arrFbox[c]];
no.text = arrFbox[c];
fbox[c] = no;
}
for(c = 0; c < arrTbox.length; c++) {
var no = new Option();
no.value = arrLookup[arrTbox[c]];
no.text = arrTbox[c];
tbox[c] = no;
   }
}


function getCurrentDate() {
var date = new Date()
var year = date.getYear()
var month = date.getMonth()
var day = date.getDate()
var hour = date.getHours()
var minute = date.getMinutes()
var second = date.getSeconds()
var months = new Array("", "JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC")

var monthname = months[month]

return month + "/" + day + "/" + year

}

function getFormattedCurrentDate() {
var date = new Date()
var year = date.getYear()
var month = date.getMonth()
var day = date.getDate()
var strday = "" + day;
if (strday.length < 2) {
    strday = "";
    strday = "0" + day;
}
else {
    strday = "";
    strday = day;
}

var months = new Array("JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC")
var monthname = months[month]

var strDate = strday + monthname + year
return strDate

}
function getStartDate() {
var date = new Date()
var year = date.getYear()
var month = date.getMonth() - 3
var day = date.getDate()
var hour = date.getHours()
var minute = date.getMinutes()
var second = date.getSeconds()
return month + "/" + day + "/" + year

}

function getEndDate() {
var date = new Date()
var year = date.getYear()
var month = date.getMonth()
var day = date.getDate() - 1
var hour = date.getHours()
var minute = date.getMinutes()
var second = date.getSeconds()

return month + "/" + day + "/" + year

}

function closePopUpWindow(){
try{ popupwin.close(); }catch(e){}	
try{ popupwindow.close(); }catch(e){}	
try{  popupwindow1.close(); }catch(e){}	
try{  popupwindow2.close(); }catch(e){}	
try{  popupwindow3.close(); }catch(e){}	
try{  popupwindow4.close(); }catch(e){}	
}
