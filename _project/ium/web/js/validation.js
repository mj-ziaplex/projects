<!--
//check/uncheck checkboxes
function markCheckBox(chkA, chkB)
{
	var len = chkB.length;

	if(len > 1)
	{
		for(i=0; i<len; i++)
		{
			chkB[i].checked = chkA.checked;
		}		
	}	
	else
	{
		chkB.checked = chkA.checked;
	}
}

//trim leading spaces 
function ltrim(str) { 
  while (str.substring(0,1) == ' ') {
    str = str.substring(1, str.length);
  }
  return (str);
} 


//trim trailing spaces 
function rtrim(str) { 
  while (str.substring(str.length-1,str.length) == ' ') {
    str = str.substring(0, str.length-1);
  }
  return (str);
}


//trim leading and trailing spaces
function trim (str) {
  str = rtrim(ltrim(str));
  return (str);
}


//for textboxes; value.length == 0 returns true
function isEmpty(v){
	if (trim(v).length == 0) {
        return true;
    } else {
        return false; 
    }
}


//for dropdown menus; value == "" returns true
function noSelection(v) {
    if (v == "") {
        return true;
    } else {
        return false;
    }
}


//for radio buttons; returns true if none is checked
function noChecked(obj) {	
	var size = obj.length;
	for (i = 0; i < size; i++) {
		if (obj[i].checked) {
			return false;	
		}
	}
	return true;
}


//date format is yyyy-mm-dd; date1 > date2 returns true
function isGreater(d1,d2){
    var regstr = "-";
	var regexp = new RegExp(regstr, "g");
	var date1 = d1.replace(regexp,"");
    var date2 = d2.replace(regexp,"");
    if (date1 > date2) {
        return true;
    } else {
        return false;
    }
}

//formats the value to currency: XXX,XXX,XXX.XX
function formatCurrency(num) {
	num = num.toString().replace(/\$|\,/g,'');
	if(isNaN(num))
	num = "0";
	sign = (num == (num = Math.abs(num)));
	num = Math.floor(num*100);
	cents = num%100;
	num = Math.floor(num/100).toString();
	if(cents<10)
	cents = "0" + cents;
	for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++)
	num = num.substring(0,num.length-(4*i+3))+','+
	num.substring(num.length-(4*i+3));
	return (((sign)?'':'-') + num + '.' + cents);
}


//returns true if the value does not exceed the limit
function isInLimitCurrency(num, maxVal) {
	num = num.toString().replace(/\$|\,/g,'');
	maxVal = maxVal.toString().replace(/\$|\,/g,'');
	if(isNaN(num))
		num = "0";
	if (parseFloat(num)>parseFloat(maxVal)) {
		return false;
	}
	else {
		return true;
	}
}


//date format is yyyy-mm-dd; date1 == date2 returns true
function isEqual(d1,d2){
    var regstr = "-";
	var regexp = new RegExp(regstr, "g");
	var date1 = d1.replace(regexp,"");
    var date2 = d2.replace(regexp,"");
    if (date1 == date2) {
        return true;
    } else {
        return false;
    }
}


//date format is yyyy-mm-dd; (d1-d2) > yrs returns true
function isGreaterBy(d1,d2,yrs){
    var regstr = "-";
	var regexp = new RegExp(regstr, "g");
	d1 = d1.replace(regexp,"/");
    d2 = d2.replace(regexp,"/");	
    var date1 = new Date(d1);
    var date2 = new Date(d2);
    if ((date2.getYear() + yrs) > 100) {
        date2.setYear(date2.getYear() + yrs + 1900);		
    } else {
        date2.setYear(date2.getYear() + yrs);
    }	
    if (date1.getTime() >= date2.getTime()) {
        return true;
    } else {
        return false;
    }
}


//returns true if value is a valid email format.
function isEmail(v){
    var chk1 = v.search(/\b\w+@\w+[.]\w+/);
    var chk2 = v.search(/[.,@]$/);
    if (chk1<0) {
        return false;
    }
    if (chk2>=0) {
        return false;
    }
    return true;
}


//returns true if value consists of digits only. Useful for checking phone number and zip entries.
function isNumeric(v){
    var re = /\D+/;
    var chk = v.search(re);
    if (chk < 0) {
        return true
    } else {
        return false;
    }
}

//return true if character c is a special character ("-", "(", ")", "#", ".", ",")
function isSpecialCharacter (c) {
    return ( ((c == "-") || (c == "(") || (c == ")") || (c == "#") || (c == ",") || (c == ".") ) );
}
     

//returns true if character c is a letter (a .. z) or (A .. Z)
function isLetter (c) {
    return ( ((c >= "a") && (c <= "z")) || ((c >= "A") && (c <= "Z")) );
}


//returns true if character c is a digit (0 .. 9)
function isDigit (c) {
    return ((c >= "0") && (c <= "9"));
}


//returns true if value consists of digits and leters only.function isAlphanumeric (s)
function isAlphaNumeric(s) {
    var i;
    
    for (i = 0; i < s.length; i++) {   
        var c = s.charAt(i);
        if (! (isLetter(c) || isDigit(c) || c == " ")) {
            return false;
        }
    }  
    return true;
}

//returns true if value consists of digits, leters and special characters only.function isAlphanumericSpecial (s)
function isAlphaNumericSpecial(s) {
    var i;
    
    for (i = 0; i < s.length; i++) {   
        var c = s.charAt(i);
        if (! (isLetter(c) || isDigit(c) || isSpecialCharacter(c) || c == " ")) {
            return false;
        }
    }  
    return true;
}

//returns true if value consists of digits and leters only.function isAlphanumeric (s)
function isAlpha(s) {
    var i;
    
    for (i = 0; i < s.length; i++) {   
        var c = s.charAt(i);
        if (! (isLetter(c) || c == " ")) {
            return false;
        }
    }  
    return true;
}

//returns true if value consists of letters and special characters only.function isAlphaSpecial (s)
function isAlphaSpecial(s) {
    var i;
    
    for (i = 0; i < s.length; i++) {   
        var c = s.charAt(i);
        if (! (isLetter(c) || isSpecialCharacter(c) || c == " ") ) {
            return false;
        }
    }  
    return true;
}


//checks if the date format is "yyyy-mm-dd"
function isValidDateFormat(date) {
    var re = /\d\d\d\d-\d\d-\d\d/;
    if (re.test(date)) {
        return true;
    } else {
        return false;
    }
}


//returns true if 'yr' is a leap year
function isLeapYear(yr) {
    if (yr % 100 == 0) {
        if (yr % 400 == 0) {  
            return true; 
        }
    } else {
        if ((yr % 4) == 0) {
            return true;
        }
    }
    return false;
}


//returns true if 'strDate' is a valid date
function isValidDate(strDate)	{
	MONTH = new Array(12);
	MONTH[0] = "JAN";
	MONTH[1] = "FEB";
	MONTH[2] = "MAR";
	MONTH[3] = "APR";
	MONTH[4] = "MAY";
	MONTH[5] = "JUN";
	MONTH[6] = "JUL";
	MONTH[7] = "AUG";
	MONTH[8] = "SEP";
	MONTH[9] = "OCT";
	MONTH[10]= "NOV";
	MONTH[11]= "DEC";
		
	if (strDate.length != 9) {
		return false;
	} 
	else {
		dd = strDate.substring(0,2);
		MMM = strDate.substring(2,5).toUpperCase();
		yyyy = strDate.substring(5,9); 	
	}
	
    // invalid day
    if ((dd < 1) || (dd > 31)) {
        return false;	
    }

    // invalid month
    if ((MMM != MONTH[0]) && (MMM != MONTH[1]) && (MMM != MONTH[2]) && 
    	(MMM != MONTH[3]) && (MMM != MONTH[4]) && (MMM != MONTH[5]) && 
    	(MMM != MONTH[6]) && (MMM != MONTH[7]) && (MMM != MONTH[8]) && 
    	(MMM != MONTH[9]) && (MMM != MONTH[10]) && (MMM != MONTH[11])) {
        return false;	
    }
    // invalid year
    if (yyyy < 1000) {
        return false;
    }

    // validate day
    if ((dd >= 1) && (dd <= 31)) {
        // check for months with days until 31	
        if (dd == 31 && ((MMM==MONTH[1]) || (MMM==MONTH[3]) || (MMM==MONTH[5]) || (MMM==MONTH[8]) ||(MMM==MONTH[10])) ) {
            return false;
        }
        else if (dd == 30 && (MMM==MONTH[1]) ) {
            return false;
        }  	
        else if (((dd == 29) && (MMM==MONTH[1]) ) && (isLeapYear(yyyy) == false)) {
            return false;
        }
    }
  
    return true;       

}


//date format is ddMMMyyyy; date1 > date2 returns true
function isGreaterDate(d1, d2)	{
	var dd1   = d1.substring(0,2);
	var mm1   = getMonthInt(d1.substring(2,5).toUpperCase());
	var yyyy1 = parseInt(d1.substring(5,9)); 	
	
	var dd2   = parseInt(d2.substring(0,2));
	var mm2   = getMonthInt(d2.substring(2,5).toUpperCase());
	var yyyy2 = parseInt(d2.substring(5,9)); 	
	
	var date1 = new Date();
	date1.setDate(dd1);
	date1.setMonth(mm1);
	date1.setYear(yyyy1);
	
	var date2 = new Date();
	date2.setDate(dd2);
	date2.setMonth(mm2);
	date2.setYear(yyyy2);
	
    if (date1 > date2) {
        return true;
    }
    else {
        return false;
    } 
}

//date format is ddMMMyyyy; date1 >= date2 returns true
function isGreaterThanEqualToDate(d1, d2)	{
	var dd1   = d1.substring(0,2);
	var mm1   = getMonthInt(d1.substring(2,5).toUpperCase());
	var yyyy1 = parseInt(d1.substring(5,9)); 	
	
	var dd2   = parseInt(d2.substring(0,2));
	var mm2   = getMonthInt(d2.substring(2,5).toUpperCase());
	var yyyy2 = parseInt(d2.substring(5,9)); 	
	
	var date1 = new Date();
	date1.setDate(dd1);
	date1.setMonth(mm1);
	date1.setYear(yyyy1);
	
	var date2 = new Date();
	date2.setDate(dd2);
	date2.setMonth(mm2);
	date2.setYear(yyyy2);
	
    if (date1 >= date2) {
        return true;
    }
    else {
        return false;
    } 
}

//date format is ddMMMyyyy; date1 > currentDate returns true
function isGreaterThanCurrentDate(d1)	{
	var dd1   = d1.substring(0,2);
	var mm1   = getMonthInt(d1.substring(2,5).toUpperCase());
	var yyyy1 = parseInt(d1.substring(5,9)); 	
	
	var date1 = new Date();
	date1.setDate(dd1);
	date1.setMonth(mm1);
	date1.setYear(yyyy1);
	
	var date2 = new Date();
	
    if (date1 > date2) {
        return true;
    }
    else {
        return false;
    } 
}

//date format is ddMMMyyyy; date1 > currentDate returns true
function isGreaterEqualToCurrentDate(d1)	{
	var dd1   = d1.substring(0,2);
	var mm1   = getMonthInt(d1.substring(2,5).toUpperCase());
	var yyyy1 = parseInt(d1.substring(5,9)); 	
	
	var date1 = new Date();
	date1.setDate(dd1);
	date1.setMonth(mm1);
	date1.setYear(yyyy1);
	
	var date2 = new Date();
	
    if (date1 >= date2) {
        return true;
    }
    else {
        return false;
    } 
}

//date format is ddMMMyyyy; date1 == date2 returns true
function isEqualDate(d1,d2){
	var dd1   = d1.substring(0,2);
	var mm1   = getMonthInt(d1.substring(2,5).toUpperCase());
	var yyyy1 = parseInt(d1.substring(5,9)); 	
	
	var dd2   = parseInt(d2.substring(0,2));
	var mm2   = getMonthInt(d2.substring(2,5).toUpperCase());
	var yyyy2 = parseInt(d2.substring(5,9)); 	
	
	var date1 = new Date();
	date1.setDate(dd1);
	date1.setMonth(mm1);
	date1.setYear(yyyy1);
	
	var date2 = new Date();
	date2.setDate(dd2);
	date2.setMonth(mm2);
	date2.setYear(yyyy2);
	
    if (date1 == date2) {
        return true;
    } else {
        return false;
    }
}


//returns true if 'strTime' is a valid time
function isValidTime(strTime) {

    if (strTime.length < 8) {
       return false;
    }
    var arr = strTime.split(":");
    if (arr.length < 2) {
        return false;
    }
    else {
	    var h   = arr[0];
	    var m   = arr[1];
	    if (h > 12) {
	       return false;
	    }
	    var mArr = m.split(" ");
	    if (mArr.length < 2) {
	        return false;
	    }
	    else {
	        var min = mArr[0];	        
	        var mer = mArr[1];
	        if (min > 59) {
	            return false;
	        }
	        if (mer != "am" && mer != "AM" && mer != "pm" && mer != "PM") {
	            return false;
	        }
	    }
    }
    return true;
}


//returns the number value of the month MMM
function getMonthInt(MMM) {
	MONTH = new Array(12);
	MONTH[0] = "JAN";
	MONTH[1] = "FEB";
	MONTH[2] = "MAR";
	MONTH[3] = "APR";
	MONTH[4] = "MAY";
	MONTH[5] = "JUN";
	MONTH[6] = "JUL";
	MONTH[7] = "AUG";
	MONTH[8] = "SEP";
	MONTH[9] = "OCT";
	MONTH[10]= "NOV";
	MONTH[11]= "DEC";
		
    if (MONTH[0] == MMM) {
        return 0;
    }   
    if (MONTH[1] == MMM) {
        return 1;
    }   
    if (MONTH[2] == MMM) {
        return 2;
    }   
    if (MONTH[3] == MMM) {
        return 3;
    }   
    if (MONTH[4] == MMM) {
        return 4;
    }   
    if (MONTH[5] == MMM) {
        return 5;
    }   
    if (MONTH[6] == MMM) {
        return 6;
    }   
    if (MONTH[7] == MMM) {
        return 7;
    }   
    if (MONTH[8] == MMM) {
        return 8;
    }   
    if (MONTH[9] == MMM) {
        return 9;
    }   
    if (MONTH[10] == MMM) {
        return 10;
    }   
    if (MONTH[11] == MMM) {
        return 11;
    }   
    
}

function getKeyNum(keyStroke, obj){
	if(!isNumeric(obj.value)){
		obj.value = '';
	}
}

function getKeyAmount(keyStroke,obj){
	var temp = obj.value;
	var keyCode = (document.layers) ? keyStroke.which : event.keyCode;
	if( (keyCode>=48 && keyCode<=57)||keyCode==190||keyCode==188 || (keyCode>=96 &&keyCode<=105)||keyCode==8||keyCode==110){
 		temp = temp.toString().replace(/\$|\,/g,'');
	  	if(isNaN(temp)){
	 	 	obj.value = temp.substring(0,temp.length-1);
  	 	}
	
		temp = obj.value;
		var arr = temp.split(".");
		var wholenum = arr[0].toString().replace(/\$|\,/g,'');
		var decimal = arr[1];

		if (parseFloat(wholenum) > parseFloat('999999999')) {
			var newkey = "";
		 	newkey = wholenum.substring(9,wholenum.length);
       		wholenum = temp.substring(0,9);
    	    if(arr.length == 2){
    	    	obj.value =  wholenum + '.' + newkey + decimal;
    	    }
    	    else {
    		    obj.value = wholenum+ '.'+ newkey;
    	    }
   		   	obj.value = formatCurrency(obj.value); 
			if(obj.value == '1,000,000,000.00')
				obj.value = '999.999.999.99';  
		}
	}
	else {
		if (temp.length > 0) {
			var x = temp.substring(temp.length-1,temp.length);
			if (isNumeric(x) == false)
				obj.value = temp.substring(0,temp.length-1);
			obj.value=formatCurrency(obj.value);
		}
	}
}


function getKeyDate(keyStroke,obj){
	var temp = obj.value;
	var keyCode = (document.layers) ? keyStroke.which : event.keyCode;
	var dd = '';
	var mmm = '';
	var yyyy = '';
	
	if (temp.length > 1){
		dd = temp.substring(0,2);
	}	else {
		dd = temp.substring(0,1);
	}
	
	if (temp.length > 5 ){
		mmm = temp.substring(2,5);
		yyyy = temp.substring(5,9);
	}	else {
		mmm = temp.substring(2,temp.length);
	}
	
	if (!isNumeric(dd)) {
		dd = '';
	}else if (parseInt(dd)>31){
		dd = temp.substring(0,1);
	}
	
	if (!isAlpha(mmm)) {
		mmm = '';
	}	else {
		if (mmm.length == 3) {
			var x = getMonthInt(mmm.toUpperCase());
			if (!(x>=0 && x<=11)) {
				mmm = '';
			}	else {
				mmm = mmm.toUpperCase();
			}
		}
	}
	/*
	if (!isNumeric(yyyy)) {
		yyyy = '';
	}else {
		if (yyyy.length == 4) {
			if (!isValidDate(obj.value)) {
				alert('Invalid date.');
				dd = '';
			}
		}
	}
	if (dd == '') {
		mmm = '';
		yyyy='';
	}
	if (mmm == '') 
		yyyy = '';
	*/
	obj.value = dd + mmm + yyyy;
}


/** 
 * returns a new string resulting from replacing all occurrences of _old in value with _new
 * value = the string to be edited
 * _old = the value to be replaced
 * _new = the value that will replace the old value
 */
function replace(value, _old, _new) {
	while (value.indexOf(_old) >= 0) {
		value = value.replace(_old, _new);
	}
	return value;
}


// validate the value for currency format XXX,XXX,XXX.XX
function isCurrency(value) {
	var value = replace(value, ",", "");
	if (isNaN(value)) {
		return false;
	}
	return true;	
}

function computeAge(bday,today){
	var dd1   = bday.substring(0,5);
	var yyyy1 = parseInt(bday.substring(5,9)); 	
	
	var dd2   = today.substring(0,5);
	var yyyy2 = parseInt(today.substring(5,9));
	var age = yyyy2-yyyy1

	var newDate1 = dd1+yyyy2
	var newDate2 = dd2+yyyy2

	if( isGreaterDate( newDate2, newDate1)   || newDate1 == newDate2) {
		return age;
	}	else{
		return age-1;
	}
}

function isValidPhone(phone) {
	if(isEmpty(phone))
	{
		return true;
	}
	for (i = 0; i < phone.length; i++) {   
        var c = phone.charAt(i);	
		if (! (isDigit(c) || c == " " || c == "(" || c == ")" || c == "-")) {
            return false;
        }    
    }	
    return true;
}

function changeRadioValue(){
	var form = document.forms[0];
	if(form.notification[0].checked)
   		form.notificationInd.value = "true";
   	else	
		form.notificationInd.value = "false";
}

function checkNumVal(obj){
	if(obj.value == 0){
		alert('Please enter a value greater than 0.');
		obj.value ='';
		document.forms[0].recordsPerView.focus();
	}
}
function checkEnter(keystroke,buttonName){
	var keyCode = (document.layers) ? keyStroke.which : event.keyCode;
	var form = document.forms[0];
	
	if(keyCode == 13){
		form.elements[buttonName].click();
		
	}
	
}
//-->

