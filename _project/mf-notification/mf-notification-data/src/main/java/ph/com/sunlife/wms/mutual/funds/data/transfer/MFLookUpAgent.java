package ph.com.sunlife.wms.mutual.funds.data.transfer;

import java.io.Serializable;

public class MFLookUpAgent implements Serializable {

  private String cardNumber;
  private String lastName;
  private String firstName;
  private String middlename;
  private String mobileNumber;
  private String email;
  private String branchId;
  private String branch;
  private String agreementTypeCode;
  private String companyCode;

  public String getCardNumber() {
    return cardNumber;
  }

  public void setCardNumber(String cardNumber) {
    this.cardNumber = cardNumber;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getMiddlename() {
    return middlename;
  }

  public void setMiddlename(String middlename) {
    this.middlename = middlename;
  }

  public String getMobileNumber() {
    return mobileNumber;
  }

  public void setMobileNumber(String mobileNumber) {
    this.mobileNumber = mobileNumber;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getBranchId() {
    return branchId;
  }

  public void setBranchId(String branchId) {
    this.branchId = branchId;
  }

  public String getBranch() {
    return branch;
  }

  public void setBranch(String branch) {
    this.branch = branch;
  }

  public String getAgreementTypeCode() {
    return agreementTypeCode;
  }

  public void setAgreementTypeCode(String agreementTypeCode) {
    this.agreementTypeCode = agreementTypeCode;
  }

  public String getCompanyCode() {
    return companyCode;
  }

  public void setCompanyCode(String companyCode) {
    this.companyCode = companyCode;
  }
}
