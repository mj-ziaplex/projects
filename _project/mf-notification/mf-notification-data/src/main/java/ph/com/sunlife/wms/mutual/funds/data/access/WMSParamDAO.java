package ph.com.sunlife.wms.mutual.funds.data.access;

import com.sunlife.ascp.connectivity.DatabaseConnector;
import com.sunlife.ascp.data.dbutils.DbUtilsRepository;
import org.apache.commons.dbutils.handlers.ColumnListHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.List;
import java.util.Properties;

@Repository
public class WMSParamDAO extends DbUtilsRepository {

  private static final Logger LOGGER =
      LoggerFactory.getLogger(WMSParamDAO.class);
  private static final String SELECT_VALUE_BY_PARAMFIELD =
      "SELECT WMSP_Value FROM WMSParam WHERE WMSP_ParamField = ?";


  public String getParamFieldValue(final String field) throws SQLException {
    return this.query(SELECT_VALUE_BY_PARAMFIELD,
                      new ColumnListHandler<String>(),
                      field)
               .get(0);
  }

  public List<String> findParamFieldValue(final String field) throws SQLException {
    return this.query(SELECT_VALUE_BY_PARAMFIELD,
                      new ColumnListHandler<String>(),
                      field);
  }


  public WMSParamDAO(DataSource dataSource) {
    super(dataSource);
  }

  public WMSParamDAO(Properties properties) {
    super(properties);
  }

  public WMSParamDAO(DatabaseConnector connector) {
    super(connector);
  }
}
