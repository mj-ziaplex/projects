package ph.com.sunlife.wms.mutual.funds.data.access;

import com.sunlife.ascp.connectivity.DatabaseConnector;
import com.sunlife.ascp.data.dbutils.DbUtilsRepository;
import org.apache.commons.dbutils.handlers.ColumnListHandler;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.Properties;

@Repository
public class DocumentTypeDAO extends DbUtilsRepository {

  private static final String SELECT_NAME_BY_ID =
      "SELECT DT_Name FROM DocumentType WHERE DT_ID = ?";


  public String getName(final String id) throws SQLException {
    if (StringUtils.isBlank(id)) {
      throw new IllegalArgumentException("DocumentType IDs must neither be null, empty, nor blank.");
    }
    return this.query(SELECT_NAME_BY_ID, new ColumnListHandler<String>(), id)
               .get(0);
  }


  public DocumentTypeDAO(DataSource dataSource) {
    super(dataSource);
  }

  public DocumentTypeDAO(Properties properties) {
    super(properties);
  }

  public DocumentTypeDAO(DatabaseConnector connector) {
    super(connector);
  }
}
