package ph.com.sunlife.wms.mutual.funds.data.access;

import com.sunlife.ascp.connectivity.DatabaseConnector;
import com.sunlife.ascp.data.dbutils.DbUtilsRepository;
import org.apache.commons.dbutils.BasicRowProcessor;
import org.apache.commons.dbutils.BeanProcessor;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.springframework.stereotype.Repository;
import ph.com.sunlife.wms.mutual.funds.data.transfer.WMSEmailTemplate;

import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

@Repository
public class WMSEmailTemplateDAO extends DbUtilsRepository {

  public static final String SELECT_ALL =
      "SELECT ID, EN_CODE_ID, EN_CODE_TYPE, EN_DESCRIPTION, EN_COMPANY_CODE, EN_DATA FROM WMS_Email_Template";


  public List<WMSEmailTemplate> getAll() throws SQLException {
    return this.query(SELECT_ALL,
                      new BeanListHandler<WMSEmailTemplate>(WMSEmailTemplate.class,
                                                            new BasicRowProcessor(new BeanProcessor(getMappings()))));
  }


  public static Map<String, String> getMappings() {
    Map<String, String> columnsToFieldsMap;
    columnsToFieldsMap = new HashMap<String, String>();
    columnsToFieldsMap.put("ID", "id");
    columnsToFieldsMap.put("codeId", "EN_CODE_ID");
    columnsToFieldsMap.put("EN_CODE_TYPE", "codeType");
    columnsToFieldsMap.put("EN_DESCRIPTION", "description");
    columnsToFieldsMap.put("EN_COMPANY_CODE", "companyCode");
    columnsToFieldsMap.put("EN_DATA", "data");
    return columnsToFieldsMap;
  }


  public WMSEmailTemplateDAO(DataSource dataSource) {
    super(dataSource);
  }

  public WMSEmailTemplateDAO(Properties properties) {
    super(properties);
  }

  public WMSEmailTemplateDAO(DatabaseConnector connector) {
    super(connector);
  }
}
