package ph.com.sunlife.wms.mutual.funds.data.transfer;

import java.io.Serializable;

public class MFApplication implements Serializable {

  private String orderTicketNumber;
  private String agentId;
  private String clientNumber;
  private String clientLastName;
  private String clientFirstName;
  private String clientMiddlename;
  private String policyNumber;

  public String getOrderTicketNumber() {
    return orderTicketNumber;
  }

  public void setOrderTicketNumber(String orderTicketNumber) {
    this.orderTicketNumber = orderTicketNumber;
  }

  public String getAgentId() {
    return agentId;
  }

  public void setAgentId(String agentId) {
    this.agentId = agentId;
  }

  public String getClientNumber() {
    return clientNumber;
  }

  public void setClientNumber(String clientNumber) {
    this.clientNumber = clientNumber;
  }

  public String getClientLastName() {
    return clientLastName;
  }

  public void setClientLastName(String lastName) {
    this.clientLastName = lastName;
  }

  public String getClientFirstName() {
    return clientFirstName;
  }

  public void setClientFirstName(String firstName) {
    this.clientFirstName = firstName;
  }

  public String getClientMiddlename() {
    return clientMiddlename;
  }

  public void setClientMiddlename(String middlename) {
    this.clientMiddlename = middlename;
  }

  public String getPolicyNumber() {
    return policyNumber;
  }

  public void setPolicyNumber(String policyNumber) {
    this.policyNumber = policyNumber;
  }
}
