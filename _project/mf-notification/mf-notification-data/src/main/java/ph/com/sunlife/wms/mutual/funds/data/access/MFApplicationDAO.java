package ph.com.sunlife.wms.mutual.funds.data.access;

import com.sunlife.ascp.connectivity.DatabaseConnector;
import com.sunlife.ascp.data.dbutils.DbUtilsRepository;
import org.apache.commons.dbutils.BasicRowProcessor;
import org.apache.commons.dbutils.BeanProcessor;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Repository;
import ph.com.sunlife.wms.mutual.funds.data.transfer.MFApplication;

import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

@Repository
public class MFApplicationDAO extends DbUtilsRepository {

  public static final String SELECT_ALL =
      "SELECT MFP_OrderTicketNumber, MFP_AgentID, MFP_ClientNo, MFP_ClientLName, MFP_ClientFName, MFP_ClientMName, MFP_PolicyNo FROM MFApplications";
  public static final String SELECT_BY_ORDER_TICKET_NUMBER =
      SELECT_ALL + " WHERE MFP_OrderTicketNumber = ?";


  public List<MFApplication> getAll() throws SQLException {
    return this.query(SELECT_ALL,
                      new BeanListHandler<MFApplication>(MFApplication.class,
                                                         new BasicRowProcessor(new BeanProcessor(getMappings()))));
  }

 public List<MFApplication> find(String ticketNumber) throws SQLException {
   if (StringUtils.isBlank(ticketNumber)) {
     throw new IllegalArgumentException("MFLookUpAgent card number must neither be null, empty, nor blank.");
   }
   return this.query(SELECT_BY_ORDER_TICKET_NUMBER,
                     new BeanListHandler<MFApplication>(MFApplication.class,
                                                        new BasicRowProcessor(new BeanProcessor(getMappings()))),
                     ticketNumber);
  }


  private static final Map<String, String> getMappings() {
    Map<String, String> columnsToFieldsMap = new HashMap<String, String>();
    columnsToFieldsMap.put("MFP_OrderTicketNumber", "orderTicketNumber");
    columnsToFieldsMap.put("MFP_AgentID", "agentId");
    columnsToFieldsMap.put("MFP_ClientNo", "clientNumber");
    columnsToFieldsMap.put("MFP_ClientLName", "clientLastName");
    columnsToFieldsMap.put("MFP_ClientFName", "clientFirstName");
    columnsToFieldsMap.put("MFP_ClientMName", "clientMiddlename");
    columnsToFieldsMap.put("MFP_PolicyNo", "policyNumber");
    return columnsToFieldsMap;
  }


  public MFApplicationDAO(DataSource dataSource) {
    super(dataSource);
  }

  public MFApplicationDAO(Properties properties) {
    super(properties);
  }

  public MFApplicationDAO(DatabaseConnector connector) {
    super(connector);
  }
}
