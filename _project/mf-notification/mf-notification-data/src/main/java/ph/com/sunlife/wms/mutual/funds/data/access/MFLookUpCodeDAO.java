package ph.com.sunlife.wms.mutual.funds.data.access;

import com.sunlife.ascp.connectivity.DatabaseConnector;
import com.sunlife.ascp.data.dbutils.DbUtilsRepository;
import org.apache.commons.dbutils.BasicRowProcessor;
import org.apache.commons.dbutils.BeanProcessor;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import ph.com.sunlife.wms.mutual.funds.data.transfer.MFLookUpCode;

import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

@Repository
public final class MFLookUpCodeDAO extends DbUtilsRepository {

  private static final Logger LOGGER =
      LoggerFactory.getLogger(MFLookUpCodeDAO.class);
  private static final String LOOKUP_CODE_SELECT_BY_ID =
      "SELECT CODE_ID, CODE_TYPE, CODE_VALUE, CODE_DESC FROM MF_LOOKUP_CODE WHERE CODE_ID = ?";


  public MFLookUpCode get(final String id) throws SQLException {
    if (StringUtils.isBlank(id)) {
      throw new IllegalArgumentException("MFLookUpCodes' IDs must neither be null, empty, nor blank.");
    }
    return this.query(LOOKUP_CODE_SELECT_BY_ID,
                      new BeanHandler<MFLookUpCode>(MFLookUpCode.class),
                      id);
  }

  public List<MFLookUpCode> find(final String id) throws SQLException {
    if (StringUtils.isBlank(id)) {
      throw new IllegalArgumentException("MFLookUpCodes' IDs must neither be null, empty, nor blank.");
    }
    return this.query(LOOKUP_CODE_SELECT_BY_ID,
                      new BeanListHandler<MFLookUpCode>(MFLookUpCode.class,
                                                        new BasicRowProcessor(new BeanProcessor(getMappings()))),
                      id);
  }


  public static final Map<String, String> getMappings() {
    Map<String, String> columnsToFieldsMap;
    columnsToFieldsMap = new HashMap<String, String>();
    columnsToFieldsMap.put("CODE_ID", "id");
    columnsToFieldsMap.put("CODE_TYPE", "type");
    columnsToFieldsMap.put("CODE_VALUE", "value");
    columnsToFieldsMap.put("CODE_DESC", "description");
    return columnsToFieldsMap;
  }


  public MFLookUpCodeDAO(DataSource dataSource) {
    super(dataSource);
  }

  public MFLookUpCodeDAO(Properties properties) {
    super(properties);
  }

  public MFLookUpCodeDAO(DatabaseConnector connector) {
    super(connector);
  }
}
