package ph.com.sunlife.wms.mutual.funds.data.access;

import com.sunlife.ascp.connectivity.DatabaseConnector;
import com.sunlife.ascp.data.dbutils.DbUtilsRepository;
import org.apache.commons.dbutils.BasicRowProcessor;
import org.apache.commons.dbutils.BeanProcessor;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Repository;
import ph.com.sunlife.wms.mutual.funds.data.transfer.MFLookUpAgent;

import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

@Repository
public class MFLookUpAgentDAO extends DbUtilsRepository {

  private static final String SELECT_ALL =
      "SELECT CardNo, LastName, FirstName, MiddleName, MobileNo, Email, Branch, BranchId, AgreementTypeCode, CompanyCode FROM MF_LOOKUP_AGENT";
  private static final String SELECT_BY_CARD_NUMBER =
      SELECT_ALL + " WHERE CardNo = ?";
  private static final String SELECT_BY_CARD_NUMBER_AND_COMPANY_CODE =
      SELECT_BY_CARD_NUMBER + " AND CompanyCode = ?";
  private static final String SELECT_BY_CARD_NUMBER_AND_FILTER_COMPANY_CODE_AND_MOBILE_NUMBER =
      SELECT_BY_CARD_NUMBER + " AND (CompanyCode IS NULL OR CompanyCode <> 'GF') AND (MobileNo IS NOT NULL AND MobileNo <> '')";


  public MFLookUpAgent get(final String cardNumber) throws SQLException {
    if (StringUtils.isBlank(cardNumber)) {
      throw new IllegalArgumentException("MFLookUpAgent card number must neither be null, empty, nor blank.");
    }
    return this.query(SELECT_BY_CARD_NUMBER,
                      new BeanHandler<MFLookUpAgent>(MFLookUpAgent.class),
                      cardNumber);
  }

  public MFLookUpAgent get(final String cardNumber, final String companyCode) throws SQLException {
    if (StringUtils.isBlank(cardNumber) || StringUtils.isBlank(companyCode)) {
      throw new IllegalArgumentException("MFLookUpAgent card number and/or company code must neither be null, empty, nor blank.");
    }
    return this.query(SELECT_BY_CARD_NUMBER_AND_COMPANY_CODE,
                      new BeanHandler<MFLookUpAgent>(MFLookUpAgent.class),
                      cardNumber, companyCode);
  }

  public List<MFLookUpAgent> find(final String cardNumber) throws SQLException {
    if (StringUtils.isBlank(cardNumber)) {
      throw new IllegalArgumentException("MFLookUpAgent card number must neither be null, empty, nor blank.");
    }
    return this.query(SELECT_BY_CARD_NUMBER,
                      new BeanListHandler<MFLookUpAgent>(MFLookUpAgent.class,
                                                         new BasicRowProcessor(new BeanProcessor(getMappings()))),
                      cardNumber);
  }

  public List<MFLookUpAgent> find(final String cardNumber, final String companyCode) throws SQLException {
    if (StringUtils.isBlank(cardNumber) || StringUtils.isBlank(companyCode)) {
      throw new IllegalArgumentException("MFLookUpAgent card number and/or company code must neither be null, empty, nor blank.");
    }
    return this.query(SELECT_BY_CARD_NUMBER_AND_COMPANY_CODE,
                      new BeanListHandler<MFLookUpAgent>(MFLookUpAgent.class,
                                                         new BasicRowProcessor(new BeanProcessor(getMappings()))),
                      cardNumber, companyCode);
  }

  public List<MFLookUpAgent> findFilteredByCardNumberAndMobileNumber(final String cardNumber) throws SQLException {
    if (StringUtils.isBlank(cardNumber)) {
      throw new IllegalArgumentException("MFLookUpAgent card number must neither be null, empty, nor blank.");
    }
    return this.query(SELECT_BY_CARD_NUMBER_AND_FILTER_COMPANY_CODE_AND_MOBILE_NUMBER,
                      new BeanListHandler<MFLookUpAgent>(MFLookUpAgent.class,
                                                         new BasicRowProcessor(new BeanProcessor(getMappings()))),
                      cardNumber);
  }


  private static final Map<String, String> getMappings() {
    Map<String, String> columnsToFieldsMap;
    columnsToFieldsMap = new HashMap<String, String>();
    columnsToFieldsMap.put("CardNo", "cardNumber");
    columnsToFieldsMap.put("LastName", "lastName");
    columnsToFieldsMap.put("FirstName", "firstName");
    columnsToFieldsMap.put("MiddleName", "middlename");
    columnsToFieldsMap.put("MobileNo", "mobileNumber");
    columnsToFieldsMap.put("Email", "email");
    columnsToFieldsMap.put("Branch", "branch");
    columnsToFieldsMap.put("BranchId", "branchId");
    columnsToFieldsMap.put("AgreementTypeCode", "agreementTypeCode");
    columnsToFieldsMap.put("CompanyCode", "companyCode");
    return columnsToFieldsMap;
  }

  public MFLookUpAgentDAO(DataSource dataSource) {
    super(dataSource);
  }

  public MFLookUpAgentDAO(Properties properties) {
    super(properties);
  }

  public MFLookUpAgentDAO(DatabaseConnector connector) {
    super(connector);
  }
}
