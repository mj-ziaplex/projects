package com.zetcode;

import com.zetcode.service.WordService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@ComponentScan(basePackages="com.zetcode")
public class Application {

  private static final Logger logger = LoggerFactory.getLogger(Application.class);

  @Autowired
  private WordService wordService;

  public static void main(String[] args) {
    ApplicationContext ctx = new AnnotationConfigApplicationContext(Application.class);
    Application bean = ctx.getBean(Application.class);
    bean.run();
//    ctx.close();
  }

  public void run() {
    logger.info("{}", wordService.randomWord());
    logger.info("{}", wordService.randomWord());

    List<String> words = wordService.all();

//    words.stream().forEach(word -> logger.info("{}", word));
    for (String word : words) {
      logger.info("{}", word);
    }
  }
}
