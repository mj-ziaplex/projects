package ph.com.sunlife.wms.configuration;

import com.sunlife.ascp.connectivity.DatabaseConnector;
import com.sunlife.ascp.connectivity.DatabaseConnectorFactory;
import com.sunlife.ascp.data.DatabaseConnectorDataSource;
import com.sunlife.ascp.data.RelationalDatabase;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.*;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;

import javax.sql.DataSource;

@Configuration
@PropertySources({
  @PropertySource(value="D:/WMS/Properties/database.properties", ignoreResourceNotFound=true),
  @PropertySource(value="D:/WMS/WorkFlowComponents/Properties/database.properties", ignoreResourceNotFound=true),
  @PropertySource("classpath:database.properties")
})
public class DatabaseConfiguration {

  @Value("${db.software}")
  private String databaseSoftware;
  @Value("${db.hostname}")
  private String hostname;
  @Value("${db.port}")
  private int port;
  @Value("${db.name}")
  private String databaseName;
  @Value("${db.username}")
  private String username;
  @Value("${db.password}")
  private String password;


  @Bean
  public DataSource dataSource() {
    DatabaseConnector connector =
        DatabaseConnectorFactory.getConnector(RelationalDatabase.fromString(databaseSoftware));
    connector.setHost(hostname);
    connector.setPort(port);
    connector.setSchema(databaseName);
    connector.setUser(username);
    connector.setPassword(password);
    return new DatabaseConnectorDataSource(connector).getDataSource();
  }

  @Bean
  public String databaseSoftware() {
    return databaseSoftware;
  }


  @Bean
  public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
    return new PropertySourcesPlaceholderConfigurer();
  }
}
