package ph.com.sunlife.wms.mutual.funds.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;

@Component
@ComponentScan(basePackages = "ph.com.sunlife.wms")
public class Test {

  @Autowired
  private DataSource ds;
  @Autowired
  private TestInjection ti;

  public static void main(String[] args) {
    ti.setDataSource(ds);
    System.out.println(new TestInjection().getDataSource());
  }

  public void run() {

  }
}
