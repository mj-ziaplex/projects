package ph.com.sunlife.wms.dao;

import java.util.List;

import ph.com.sunlife.wms.dao.domain.DiscrepancyReportEntry;
import ph.com.sunlife.wms.dao.domain.StartEndMonth;
import ph.com.sunlife.wms.dao.exceptions.WMSDaoException;

/**
 * The data access object responsible for generating Collection Discrepancy
 * Report.
 * 
 * @author Zainal Limpao
 * 
 */
public interface CollectionDiscrepancyReportDao {

	/**
	 * Get Timeliness Discrepancy Report Entries.
	 * 
	 * @param param
	 * @return
	 * @throws WMSDaoException
	 */
	List<DiscrepancyReportEntry> getTimelinessDiscrepancyReports(
			StartEndMonth param) throws WMSDaoException;

	/**
	 * Get Accuracy Report Entries.
	 * 
	 * @param param
	 * @return
	 * @throws WMSDaoException
	 */
	List<DiscrepancyReportEntry> getAccuracyDiscrepancyReports(
			StartEndMonth param) throws WMSDaoException;
}
