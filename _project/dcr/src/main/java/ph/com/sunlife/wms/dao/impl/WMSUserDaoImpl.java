package ph.com.sunlife.wms.dao.impl;

import java.util.List;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import ph.com.sunlife.wms.dao.WMSUserDao;
import ph.com.sunlife.wms.dao.domain.WMSUser;
import ph.com.sunlife.wms.dao.exceptions.WMSDaoException;

public class WMSUserDaoImpl extends SqlMapClientDaoSupport implements WMSUserDao{
	
	private static final String NAMESPACE = "WMSUser";

	@Override
	public boolean createWMSUser(WMSUser user) throws WMSDaoException {
		boolean success = false;
		try {
			getSqlMapClientTemplate().queryForObject(NAMESPACE + ".createWMSUser", user);
			success = true;
		} catch (Exception ex) {
			throw new WMSDaoException("Error in " + WMSUserDaoImpl.class.getSimpleName() + ": "+ ex);
		}
		return success;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<WMSUser> getAllWMSUser() throws WMSDaoException {
		try {
			List<WMSUser> list = 
					(List<WMSUser>) getSqlMapClientTemplate().queryForList(NAMESPACE + ".getAllWMSUser");
			return list;
		} catch (Exception ex) {
			throw new WMSDaoException("Error in " + WMSUserDaoImpl.class.getSimpleName() + ": "+ ex);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<WMSUser> getWMSUserById(String userId) throws WMSDaoException {
		try {
			List<WMSUser> list = 
					(List<WMSUser>) getSqlMapClientTemplate().queryForList(NAMESPACE + ".getWMSUserById", userId);
			return list;
		} catch (Exception ex) {
			throw new WMSDaoException("Error in " + WMSUserDaoImpl.class.getSimpleName() + ": "+ ex);
		}
	}

	@Override
	public boolean updateWMSUser(WMSUser user) throws WMSDaoException {
		try {
			int updatedRows = getSqlMapClientTemplate().update(NAMESPACE + ".updateWMSUser", user);
			return updatedRows > 0;
		} catch (Exception ex) {
			throw new WMSDaoException("Error in " + WMSUserDaoImpl.class.getSimpleName() + ": "+ ex);
		}
	}

	@Override
	public boolean deleteWMSUser(String userId) throws WMSDaoException {
		try {
			int deletedRows = getSqlMapClientTemplate().delete(NAMESPACE + ".deleteWMSUser", userId);
			return deletedRows > 0;
		} catch (Exception ex) {
			throw new WMSDaoException("Error in " + WMSUserDaoImpl.class.getSimpleName() + ": "+ ex);
		}
	}

//	@Override
//	public boolean updateWMSUserLogin(WMSUser user) throws WMSDaoException {
//		try {
//			int updatedRows = getSqlMapClientTemplate().update(NAMESPACE + ".updateWMSUserLogin", user);
//			return updatedRows > 0;
//		} catch (Exception ex) {
//			throw new WMSDaoException("Error in " + WMSUserDaoImpl.class.getSimpleName() + ": "+ ex);
//		}
//	}
//
//	@Override
//	public boolean updateWMSUserLogout(WMSUser user) throws WMSDaoException {
//		try {
//			int updatedRows = getSqlMapClientTemplate().update(NAMESPACE + ".updateWMSUserLogout", user);
//			return updatedRows > 0;
//		} catch (Exception ex) {
//			throw new WMSDaoException("Error in " + WMSUserDaoImpl.class.getSimpleName() + ": "+ ex);
//		}
//	}

}
