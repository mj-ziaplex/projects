package ph.com.sunlife.wms.services.bo;

import java.util.Date;

import ph.com.sunlife.wms.dao.domain.CheckType;
import ph.com.sunlife.wms.dao.domain.Company;
import ph.com.sunlife.wms.dao.domain.Currency;
import ph.com.sunlife.wms.dao.domain.ProductType;
import ph.com.sunlife.wms.util.IpacUtil;
import ph.com.sunlife.wms.util.WMSDateUtil;

public class DCRChequeDepositSlipBO {

	private Long id;

	private DCRCashierBO dcrCashier = new DCRCashierBO();
	
	private long dcrCashierId;
	
	private BankBO depositoryBank = new BankBO();
	
	private String depositoryBankId;
	
	private Long companyId;

	private Company company;
	
	private String companyStr;

	private String accountNumber;
	
	private String customerCenter;
	
	private String customerCenterName;
	
	private Long currencyId;

	private Currency currency;
	
	private String prodCode;

	private double totalCheckAmount;

	private double requiredTotalAmount;
	
	private Date depositDate;
	
	private String depositDateStr;
	
	private Date dcrDate;
	
	private String dcrDateStr;
	
	private ProductType productType;

	private CheckType checkType;
	
	private long checkTypeId;

	private byte[] deposlipFile;
	
	private String dcrDepoSlipVersionSerialId;
	
	private String userId;
	
	private String acf2id;
	
	private String cashierName;
	
	private String barcodeStr;
	
	private double slfpiTotal;
	
	private double slocpiTotal;
	
	private double hiddenSlfpiTotal;
	
	private double hiddenSlocpiTotal;
	
	private double hiddenCollectionTotal;
	
	private double hiddenMixedChequeTotal;
	
	private double tradVulCollectionTotal;
	
	private double grpLifeCollectionTotal;
	
	private double hiddenTradVulCollectionTotal;
	
	private double hiddenGrpLifeCollectionTotal;

	public String getAcf2id() {
		return acf2id;
	}

	public void setAcf2id(String acf2id) {
		this.acf2id = acf2id;
	}

	public void setDcrCashier(DCRCashierBO dcrCashier) {
		this.dcrCashier = dcrCashier;
	}

	public void setDcrCashier(Long dcrCashierId) {
		if (dcrCashier == null) {
			dcrCashier = new DCRCashierBO();
		}
		dcrCashier.setId(dcrCashierId);
	}
	
	public void setDcrCashierId(long dcrCashierId) {
		this.dcrCashierId = dcrCashierId;
		if (dcrCashier == null) {
			dcrCashier = new DCRCashierBO();
		}
		dcrCashier.setId(dcrCashierId);
	}
	
	public long getDcrCashierId() {
		return dcrCashierId;
	}

	public DCRCashierBO getDcrCashier() {
		return dcrCashier;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	

	public BankBO getDepositoryBank() {
		return depositoryBank;
	}
	
	public void setDepositoryBank(BankBO depositoryBank) {
		this.depositoryBank = depositoryBank;
		this.depositoryBankId = depositoryBank.getId();
	}

	public String getDepositoryBankId() {
		return depositoryBankId;
	}

	public void setDepositoryBankId(String depositoryBankId) {
		this.depositoryBankId = depositoryBankId;
		BankBO bankBO = new BankBO();
		bankBO.setId(depositoryBankId);
		this.depositoryBank = bankBO;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public void setCustomerCenter(String customerCenter) {
		this.customerCenter = customerCenter;
	}

	public String getCustomerCenter() {
		return customerCenter;
	}

	public String getCustomerCenterName() {
		return customerCenterName;
	}

	public void setCustomerCenterName(String customerCenterName) {
		this.customerCenterName = customerCenterName;
	}

	public double getTotalCheckAmount() {
		return totalCheckAmount;
	}

	public void setTotalCheckAmount(double totalCheckAmount) {
		this.totalCheckAmount = totalCheckAmount;
	}

	public double getRequiredTotalAmount() {
		return requiredTotalAmount;
	}

	public void setRequiredTotalAmount(double requiredTotalAmount) {
		this.requiredTotalAmount = requiredTotalAmount;
	}

	public CheckType getCheckType() {
		return checkType;
	}

	public void setCheckType(CheckType checkType) {
		this.checkType = checkType;
	}
	
	public void setCheckType(Long checkTypeId) {
		this.checkType = CheckType.getById(checkTypeId);
		this.checkType = CheckType.getById(checkTypeId);
	}

	public Currency getCurrency() {
		return currency;
	}

	public void setCurrency(Currency currency) {
		this.currency = currency;
		this.currencyId = currency.getId();
	}

	public byte[] getDeposlipFile() {
		return deposlipFile;
	}

	public void setDeposlipFile(byte[] deposlipFile) {
		this.deposlipFile = deposlipFile;
	}
	
	public ProductType getProductType() {
		return productType;
	}

	public void setProductType(ProductType productType) {
		this.productType = productType;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DCRChequeDepositSlipBO other = (DCRChequeDepositSlipBO) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public Long getCompanyId() {
		return companyId;
	}

	public void setCompany(Company company) {
		this.company = company;
		this.companyId = company.getId();
		this.companyStr = IpacUtil.toIpacComCode(company.getId());
	}

	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
		this.company = Company.getCompany(companyId);
		this.companyStr = IpacUtil.toIpacComCode(companyId);
	}

	public Company getCompany() {
		return company;
	}

	public Long getCurrencyId() {
		return currencyId;
	}

	public void setCurrencyId(Long currencyId) {
		this.currencyId = currencyId;
	}

	public String getDcrDepoSlipVersionSerialId() {
		return dcrDepoSlipVersionSerialId;
	}

	public void setDcrDepoSlipVersionSerialId(String dcrDepoSlipVersionSerialId) {
		this.dcrDepoSlipVersionSerialId = dcrDepoSlipVersionSerialId;
	}

	public void setDepositDate(Date depositDate) {
		this.depositDate = depositDate;
		this.depositDateStr = WMSDateUtil.toFormattedDateStr(depositDate);
	}

	public Date getDepositDate() {
		return depositDate;
	}

	public void setDepositDateStr(String depositDateStr) {
		this.depositDateStr = depositDateStr;
		this.depositDate = WMSDateUtil.toDate(depositDateStr);
	}
	
	public String getDepositDateStr() {
		return depositDateStr;
	}
	
	public String getDcrDateStr() {
		return dcrDateStr;
	}

	public void setDcrDateStr(String dcrDateStr) {
		this.dcrDateStr = dcrDateStr;
		this.dcrDate = WMSDateUtil.toDate(dcrDateStr);
	}
	
	public Date getDcrDate() {
		return dcrDate;
	}

	public void setDcrDate(Date dcrDate) {
		this.dcrDate = dcrDate;
		this.dcrDateStr = WMSDateUtil.toFormattedDateStr(dcrDate);
	}

	public void setCompanyStr(String companyStr) {
		this.companyStr = companyStr;
	}

	public String getCompanyStr() {
		return companyStr;
	}

	public void setCheckTypeId(long checkTypeId) {
		this.checkTypeId = checkTypeId;
		this.checkType = CheckType.getById(checkTypeId);
	}

	public long getCheckTypeId() {
		return checkTypeId;
	}

	public void setProdCode(String prodCode) {
		this.prodCode = prodCode;
	}

	public String getProdCode() {
		return prodCode;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getCashierName() {
		return cashierName;
	}

	public void setCashierName(String cashierName) {
		this.cashierName = cashierName;
	}

	public String getBarcodeStr() {
		return barcodeStr;
	}

	public void setBarcodeStr(String barcodeStr) {
		this.barcodeStr = barcodeStr;
	}

	public double getSlfpiTotal() {
		return slfpiTotal;
	}

	public void setSlfpiTotal(double slfpiTotal) {
		this.slfpiTotal = slfpiTotal;
	}

	public double getSlocpiTotal() {
		return slocpiTotal;
	}

	public void setSlocpiTotal(double slocpiTotal) {
		this.slocpiTotal = slocpiTotal;
	}

	public double getHiddenSlfpiTotal() {
		return hiddenSlfpiTotal;
	}

	public void setHiddenSlfpiTotal(double hiddenSlfpiTotal) {
		this.hiddenSlfpiTotal = hiddenSlfpiTotal;
	}

	public double getHiddenSlocpiTotal() {
		return hiddenSlocpiTotal;
	}

	public void setHiddenSlocpiTotal(double hiddenSlocpiTotal) {
		this.hiddenSlocpiTotal = hiddenSlocpiTotal;
	}

	public double getHiddenCollectionTotal() {
		return hiddenCollectionTotal;
	}

	public void setHiddenCollectionTotal(double hiddenCollectionTotal) {
		this.hiddenCollectionTotal = hiddenCollectionTotal;
	}

	public double getHiddenMixedChequeTotal() {
		return hiddenMixedChequeTotal;
	}

	public void setHiddenMixedChequeTotal(double hiddenMixedChequeTotal) {
		this.hiddenMixedChequeTotal = hiddenMixedChequeTotal;
	}

	public double getHiddenTradVulCollectionTotal() {
		return hiddenTradVulCollectionTotal;
	}

	public void setHiddenTradVulCollectionTotal(double hiddenTradVulCollectionTotal) {
		this.hiddenTradVulCollectionTotal = hiddenTradVulCollectionTotal;
	}

	public double getHiddenGrpLifeCollectionTotal() {
		return hiddenGrpLifeCollectionTotal;
	}

	public void setHiddenGrpLifeCollectionTotal(double hiddenGrpLifeCollectionTotal) {
		this.hiddenGrpLifeCollectionTotal = hiddenGrpLifeCollectionTotal;
	}

	public double getTradVulCollectionTotal() {
		return tradVulCollectionTotal;
	}

	public void setTradVulCollectionTotal(double tradVulCollectionTotal) {
		this.tradVulCollectionTotal = tradVulCollectionTotal;
	}

	public double getGrpLifeCollectionTotal() {
		return grpLifeCollectionTotal;
	}

	public void setGrpLifeCollectionTotal(double grpLifeCollectionTotal) {
		this.grpLifeCollectionTotal = grpLifeCollectionTotal;
	}

}
