package ph.com.sunlife.wms.web.filter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

import org.apache.commons.lang.StringUtils;

/**
 * An instance of {@link HttpServletRequestWrapper} that appends the current
 * time millis into the request parameters via the query string.
 * 
 * @author Zainal Limpao
 * 
 */
public class AggressiveCachingStopperServletRequest extends
		HttpServletRequestWrapper {

	/**
	 * The Constructor
	 * 
	 * @param request
	 */
	public AggressiveCachingStopperServletRequest(HttpServletRequest request) {
		super(request);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.servlet.http.HttpServletRequestWrapper#getQueryString()
	 */
	@Override
	public String getQueryString() {
		final String timeMillisParam = "tm=" + System.currentTimeMillis();

		String queryString = super.getQueryString();

		if (!this.hasTimeMillisParameter()) {
			if (StringUtils.isNotEmpty(queryString)) {
				queryString += "&" + timeMillisParam;
			} else {
				queryString = timeMillisParam;
			}
		}

		return queryString.trim();
	}

	/**
	 * Ensures that the <b>tm</b> parameter is not apppended twice.
	 * 
	 * @return <code>true</code> or <code>false</code>
	 */
	private boolean hasTimeMillisParameter() {
		return StringUtils.isNotEmpty(super.getParameter("tm"));
	}

}
