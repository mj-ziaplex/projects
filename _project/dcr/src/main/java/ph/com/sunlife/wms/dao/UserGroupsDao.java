package ph.com.sunlife.wms.dao;

import java.util.List;

import ph.com.sunlife.wms.dao.domain.HubCC;
import ph.com.sunlife.wms.dao.domain.HubCCNBO;
import ph.com.sunlife.wms.dao.domain.HubCCNBOUserGroup;
import ph.com.sunlife.wms.dao.domain.UserGroup;
import ph.com.sunlife.wms.dao.domain.UserGroupHub;
import ph.com.sunlife.wms.dao.exceptions.WMSDaoException;

/**
 * This is the Data Access Object responsible for associating Users and Groups.
 * 
 * @author Zainal Limpao
 * 
 */
public interface UserGroupsDao {

	/**
	 * Retrieves all groups and HUB associated with the given user.
	 * 
	 * @param userId
	 * @return
	 * @throws WMSDaoException
	 */
	List<UserGroupHub> getUserGroupHubList(String userId)
			throws WMSDaoException;

	List<String> getHubForCustomerCenter(String siteCode)
			throws WMSDaoException;

	List<String> getGroupNames(List<String> groupIds) throws WMSDaoException;
	
	List<UserGroupHub> getAllUserGroupHubList() throws WMSDaoException;
	
	List<HubCCNBOUserGroup> getUserGroupHubByUserId(HubCCNBOUserGroup hub) throws WMSDaoException;
	
	boolean createUserGroupHub(HubCCNBOUserGroup hub) throws WMSDaoException;
	
	boolean updateUserGroupHub(HubCCNBOUserGroup hub) throws WMSDaoException;
	
	boolean createUserGroup(UserGroup group) throws WMSDaoException;
	
	boolean deleteUserGroup(UserGroup group) throws WMSDaoException;
	
	List<HubCCNBO> getHubCCNBO(HubCCNBO nbo) throws WMSDaoException;	
	
	boolean createHubCCNBO(HubCCNBO nbo) throws WMSDaoException;
	
	boolean createHubCC(HubCC cc) throws WMSDaoException;
	
	boolean deleteUserGroupHub(HubCCNBOUserGroup hub) throws WMSDaoException;
	
	boolean updateHubCCNBO(HubCCNBO nbo) throws WMSDaoException;
	
	boolean deleteHubCCNBO(HubCCNBO nbo) throws WMSDaoException;
	
	boolean updateHubCC(HubCC cc) throws WMSDaoException;
	
	boolean deleteHubCC(HubCC cc) throws WMSDaoException;
}
