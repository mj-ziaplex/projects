package ph.com.sunlife.wms.dao;

import java.util.List;

import ph.com.sunlife.wms.dao.domain.DCR;
import ph.com.sunlife.wms.dao.domain.DCRCenterCoinCollection;
import ph.com.sunlife.wms.dao.domain.DCRDollarCoinCollection;
import ph.com.sunlife.wms.dao.domain.DCRDollarCoinExchange;
import ph.com.sunlife.wms.dao.domain.DCRVDIL;
import ph.com.sunlife.wms.dao.domain.DCRVDILISOCollection;
import ph.com.sunlife.wms.dao.domain.DCRVDILPRSeriesNumber;
import ph.com.sunlife.wms.dao.exceptions.WMSDaoException;

public interface DCRVDILDao extends WMSDao<DCRVDIL> {

	DCRVDIL getByDCRCashierId(Long dcrCashierId) throws WMSDaoException;

	DCRVDILISOCollection insertDcrVdilIsoCollection(DCRVDILISOCollection entity)
			throws WMSDaoException;

	List<DCRVDILISOCollection> getDcrVdilIsoCollections(Long dcrVdilId)
			throws WMSDaoException;

	DCRVDILPRSeriesNumber insertDcrVdilPrSeriesNumber(
			DCRVDILPRSeriesNumber entity) throws WMSDaoException;

	List<DCRVDILPRSeriesNumber> getDcrVdilPrSeriesNumbers(Long dcrVdilId)
			throws WMSDaoException;

	boolean updateVdil(DCRVDIL dcrVdil) throws WMSDaoException;

	boolean updateDcrVdilPrSeriesNumber(DCRVDILPRSeriesNumber entity)
			throws WMSDaoException;

	boolean updateDcrVdilIsoCollection(DCRVDILISOCollection entity)
			throws WMSDaoException;

	// -----------------------------------------------------

	DCRDollarCoinCollection saveDCRDollarCoinCollection(
			DCRDollarCoinCollection coll) throws WMSDaoException;

	boolean updateNetDCRDollarCoinCollection(DCRDollarCoinCollection coll)
			throws WMSDaoException;

	DCRDollarCoinCollection getDollarCoinCollectionByDcrId(Long dcrId)
			throws WMSDaoException;

	Long getPreviousDayDcrId(DCR entity) throws Exception;

	DCRDollarCoinCollection getDollarCoinCollectionByDcrCashierId(
			Long dcrCashierId) throws WMSDaoException;

	// ----------------------------------------------------

	DCRDollarCoinExchange createDCRDollarCoinExchange(
			DCRDollarCoinExchange entity) throws WMSDaoException;

	boolean updateDCRDollarCoinExchange(DCRDollarCoinExchange entity)
			throws WMSDaoException;

	DCRDollarCoinExchange getDollarCoinExchangeByCashierId(Long dcrCashierId)
			throws WMSDaoException;

	DCRCenterCoinCollection getCurrentCenterCoinCollection(String ccId)
			throws WMSDaoException;

	DCRCenterCoinCollection createCurrentCenterCoinCollection(String ccId)
			throws WMSDaoException;

	boolean updateCurrentCenterCoinCollection(
			DCRCenterCoinCollection thisCollection) throws WMSDaoException;

}
