///*
// * $Id: AbstractLog4JProfiler.java,v 1.1 2012/10/16 08:17:59 PV70 Exp $
// *
// * Copyright (c) 2010 HEB
// * All rights reserved.
// *
// * This software is the confidential and proprietary information
// * of HEB.
// * 
// * COMMENTED AS NOT BEING USED
// */
//package ph.com.sunlife.wms.logging.aop;
//
//import org.apache.log4j.Logger;
//
//public abstract class AbstractLog4JProfiler extends AbstractProfiler {
//
//	protected Logger logger;
//
//	public Logger getLogger() {
//		return this.logger;
//	}
//
//	public void setLogger(Logger logger) {
//		this.logger = logger;
//	}
//
//}
