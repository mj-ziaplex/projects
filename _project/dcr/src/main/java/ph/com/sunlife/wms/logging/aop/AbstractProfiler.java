///*
// * $Id: AbstractProfiler.java,v 1.1 2012/10/16 08:17:59 PV70 Exp $
// *
// * Copyright (c) 2010 HEB
// * All rights reserved.
// *
// * This software is the confidential and proprietary information
// * of HEB.
// * 
// * COMMENTED AS NOT BEING USED
// */
//package ph.com.sunlife.wms.logging.aop;
//
//import java.util.Arrays;
//import org.aspectj.lang.ProceedingJoinPoint;
//import org.springframework.util.StopWatch;
//
//public abstract class AbstractProfiler {
//
//    /**
//     * Profile.
//     *
//     * @param call the call
//     * @return the object
//     * @throws Throwable the throwable
//     */
////	public Object profile(ProceedingJoinPoint call) throws Throwable {
////
////		Object retVal = null;
////		Throwable throwable = null;
////
////		// ProceedingJoinPoint.getSignature returns the method signature
////		StopWatch clock = new StopWatch("Profiling for '" +
////				call.getSignature() + "'");
////
////		try {
////			clock.start(call.toShortString());
////
////			retVal = call.proceed();
////		} catch (Throwable t) {
////			throwable = t;
////			throw throwable;
////		} finally {
////
////			clock.stop();
////			/* you can also access the arguments passed to the method */
////			String message = this.createMessage(call, retVal, clock);
////
////			if (throwable == null) {
////				this.log(message);
////			} else {
////				this.log(message, throwable);
////			}
////		}
////		return retVal;
////	}
//    /**
//     * Log.
//     *
//     * @param message the message
//     */
//    protected abstract void log(String message);
//
//    /**
//     * Log.
//     *
//     * @param message the message
//     * @param t the t
//     */
//    protected abstract void log(String message, Throwable t);
//
//    /**
//     * Creates the message.
//     *
//     * @param call the call
//     * @param retVal the ret val
//     * @param clock the clock
//     * @return the string
//     */
//    protected String createMessage(ProceedingJoinPoint call, Object retVal, StopWatch clock) {
//        return call.getSignature().getDeclaringTypeName() + "."
//                + call.getSignature().getName() + "(), args="
//                + Arrays.asList(call.getArgs()) + ", retVal=" + retVal
//                + ", time=" + clock.getTotalTimeMillis();
//    }
//}
