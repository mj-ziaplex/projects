package ph.com.sunlife.wms.dao;

import java.util.List;

import ph.com.sunlife.wms.dao.domain.DCR;
import ph.com.sunlife.wms.dao.domain.DCRCashier;
import ph.com.sunlife.wms.dao.domain.DCRCashierNoCollection;
import ph.com.sunlife.wms.dao.exceptions.WMSDaoException;

/**
 * The Data Access Object for {@link DCRCashier}.
 * 
 * @author Zainal Limpao
 * 
 */
public interface DCRCashierDao extends WMSDao<DCRCashier> {

	/**
	 * Returns a list of {@link DCRCashier} associated with the given CASHIER.
	 * This is regardless of what {@link DCR}.
	 * 
	 * @param sample
	 * @return
	 * @throws WMSDaoException
	 */
	List<DCRCashier> getDCRCashierListForCashier(DCRCashier sample)
			throws WMSDaoException;

	boolean changeStatusToPending(Long dCRCashierId) throws WMSDaoException;

	List<DCRCashier> getDCRCashierListByDcrId(final Long dcrId)
			throws WMSDaoException;

	List<DCRCashier> getDCRCashierListByDcrList(List<Long> dcrIds)
			throws WMSDaoException;

	boolean insertNoCollection(DCRCashierNoCollection dcrCashierNoCollection)
			throws WMSDaoException;

	List<DCRCashierNoCollection> getCashierNoCollections(String acf2id)
			throws WMSDaoException;

	boolean deleteCashierNoCollections(Long dcrId) throws WMSDaoException;
	
}
