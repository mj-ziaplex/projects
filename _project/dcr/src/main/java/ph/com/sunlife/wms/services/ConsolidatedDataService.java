package ph.com.sunlife.wms.services;

import java.util.List;

import ph.com.sunlife.wms.dao.domain.DCR;
import ph.com.sunlife.wms.dao.domain.DCRAuditLog;
import ph.com.sunlife.wms.dao.exceptions.WMSDaoException;
import ph.com.sunlife.wms.services.bo.AuditLog;
import ph.com.sunlife.wms.services.bo.ConsolidatedData;
import ph.com.sunlife.wms.services.bo.DCRBO;
import ph.com.sunlife.wms.services.bo.DCRCommentBO;
import ph.com.sunlife.wms.services.bo.DCRStepProcessorNoteBO;
import ph.com.sunlife.wms.services.exception.ServiceException;

/**
 * Service responsible for displaying data on the Consolidated View. This also
 * includes methods responsible for saving inline notes, add comments, and
 * approving a step in the step processor.
 * 
 * @author Zainal Limpao
 * 
 */
public interface ConsolidatedDataService {

	/**
	 * The method that assembles everything within the DCR Step Processor.
	 * 
	 * @param dcrId
	 * @return
	 * @throws ServiceException
	 */
	ConsolidatedData getConsolidatedData(Long dcrId) throws ServiceException;

	ConsolidatedData getConsolidatedData(Long dcrId, String userId)
			throws ServiceException;

	ConsolidatedData getConsolidatedData(Long dcrId, String openedBy,
			boolean isPPA, boolean isMgr) throws ServiceException;

	DCRBO getDcr(Long dcrId) throws ServiceException;

	/**
	 * Parses the {@link ConsolidatedData} and formats it to UI-readable JSON
	 * objects in the form of {@link String} list.
	 * 
	 * @param con
	 * @return
	 * @throws ServiceException
	 */
	List<String> formatConsolidatedData(ConsolidatedData con)
			throws ServiceException;

	/**
	 * This is when CCM approves the Consolidated DCR. It will then be submitted
	 * to PPA for reconciliation.
	 * 
	 * @param userId
	 * @param dcrId
	 * @param notes
	 * @throws ServiceException
	 */
	void approveForReconcilation(String userId, Long dcrId,
			List<DCRStepProcessorNoteBO> notes) throws ServiceException;

	/**
	 * This is when CCM needed response from the cashier hence he/she sets it to
	 * "awaiting response".
	 * 
	 * @param userId
	 * @param dcrId
	 * @param notes
	 * @throws ServiceException
	 */
	void awaitingFeedback(String userId, Long dcrId,
			List<DCRStepProcessorNoteBO> notes) throws ServiceException;

	/**
	 * This is the final step in which PPA personnel agrees that every line item
	 * in the Consolidated DCR is reconciled.
	 * 
	 * @param userId
	 * @param dcrId
	 * @param reconciledRowNums
	 * @param notes
	 * @throws ServiceException
	 */
	void ppaReconciled(String userId, Long dcrId,
			List<Integer> reconciledRowNums, List<DCRStepProcessorNoteBO> notes)
			throws ServiceException;

	/**
	 * This is the step in which Cashier Consolidator agrees with the values
	 * presented in the DCR and then submits them to CCM for review and
	 * approval.
	 * 
	 * @param userId
	 * @param dcrId
	 * @param notes
	 * @throws ServiceException
	 */
	void forReviewAndApproval(String userId, Long dcrId,
			List<DCRStepProcessorNoteBO> notes) throws ServiceException;

	void forVerification(String userId, Long dcrId,
			List<Integer> reconciledRowNums, List<DCRStepProcessorNoteBO> notes)
			throws ServiceException;

	void forVerificationPPA(String userId, Long dcrId,
			List<Integer> reconciledRowNums, List<DCRStepProcessorNoteBO> notes)
			throws ServiceException;

	void awaitingRequirements(String userId, Long dcrId,
			List<Integer> reconciledRowNums, List<DCRStepProcessorNoteBO> notes)
			throws ServiceException;

	void reqtsNotSubmittedPPA(String userId, Long dcrId,
			List<Integer> reconciledRowNums, List<DCRStepProcessorNoteBO> notes)
			throws ServiceException;

	/**
	 * This saves all the in-line {@link DCRStepProcessorNoteBO} notes.
	 * 
	 * @param dcrId
	 * @param notes
	 * @return
	 * @throws ServiceException
	 */
	List<DCRStepProcessorNoteBO> saveInlineNotes(String userId, Long dcrId,
			List<DCRStepProcessorNoteBO> notes) throws ServiceException;

	/**
	 * This is called when PPA personnel clicks the "save" button. All in-line
	 * {@link DCRStepProcessorNoteBO} notes will be saved as well as all the
	 * checked reconciled rows.
	 * 
	 * @param dcrId
	 * @param reconciledRowNums
	 * @param notes
	 * @throws ServiceException
	 */
	void savePPAStepProcessor(String userId, Long dcrId,
			List<Integer> reconciledRowNums, List<DCRStepProcessorNoteBO> notes)
			throws ServiceException;

	/**
	 * The method responsible for adding new {@link DCRCommentBO}.
	 * 
	 * @param dcrComment
	 * @throws ServiceException
	 */
	void addDcrComment(DCRCommentBO dcrComment) throws ServiceException;

	/**
	 * The method that retrieves a list of {@link AuditLog}.
	 * 
	 * @param dcrId
	 * @return
	 * @throws ServiceException
	 */
	List<AuditLog> getAuditLog(Long dcrId) throws ServiceException;

	/**
	 * Method called by the automated batch run that checks if there are
	 * unverified DCR workitems beyond 3 days.
	 * 
	 * @throws ServiceException
	 */
	void checkForUnverifiedDCR() throws ServiceException;

	/**
	 * Method called by the automated batch run that that moves DCR to
	 * "Not Verified" beyond 3 working days back to DCRPPARecon queue.
	 * 
	 * @param dcrId
	 * @throws ServiceException
	 * @see {@link #checkForUnverifiedDCR()}
	 */
	void notVerifiedPPA(Long dcrId) throws ServiceException;

	void notVerifiedPPA(String userId, Long dcrId,
			List<Integer> reconciledRowNums, List<DCRStepProcessorNoteBO> notes)
			throws ServiceException;

	/**
	 * This method is executed if PPA User selects <code>Verified</code> in the
	 * Step Processor Drop-down response list.
	 * 
	 * @param userId
	 * @param dcrId
	 * @param notes
	 * @throws ServiceException
	 */
	void verifiedPPA(String userId, Long dcrId,
			List<Integer> reconciledRowNums, List<DCRStepProcessorNoteBO> notes)
			throws ServiceException;

	/**
	 * Method called by the automated batch run that that moves DCR to
	 * "Reqts Submitted" beyond 3 working days back to DCRPPARecon.
	 * 
	 * @param dcrId
	 * @throws ServiceException
	 * @see {@link #checkForUnverifiedDCR()}
	 */
	void reqtsSubmittedPPA(Long dcrId) throws ServiceException;

	/**
	 * Method automatically called when a user attaches a file during which the
	 * workitem is set to "Awaiting Requirements".
	 * 
	 * @param userId
	 * @param dcrId
	 * @throws ServiceException
	 */
	void reqtsSubmittedPPA(String userId, Long dcrId) throws ServiceException;

	/**
	 * This method is executed if PPA User selects <code>Reqts Submitted</code>
	 * in the Step Processor Drop-down response list.
	 * 
	 * @param userId
	 * @param dcrId
	 * @param notes
	 * @throws ServiceException
	 */
	void reqtsSubmittedPPA(String userId, Long dcrId,
			List<Integer> reconciledRowNums, List<DCRStepProcessorNoteBO> notes)
			throws ServiceException;

	/**
	 * Method called by the automated batch run that checks if there are DCR
	 * workitems that have not had any attachment beyond 5 days.
	 * 
	 * @throws ServiceException
	 */
	void checkForNonSubmissionOfRequirements() throws ServiceException;

	/**
	 * The main method used by the WMS DCR PPA batch run which uses current time
	 * as its basis.
	 * 
	 * @throws ServiceException
	 */
	void ppaAutoRun() throws ServiceException;
	
	void checkAgeingDcrWorkItems() throws ServiceException;
	
	void processIPACAndIngeniumData(Long dcrId, boolean isMgr) throws ServiceException;
	
	ConsolidatedData getConsolidatedDataAttachment(Long dcrId) throws ServiceException;
	
	List<DCR> getAllDcrForStatusUpdate() throws ServiceException;
	
	boolean updateDCRStatus(DCR dcr, DCRAuditLog dcrAuditLog) throws ServiceException;
	
	List<DCR> getAllDcrForStatusUpdateFromSearch(DCR dcr) throws ServiceException;

    /*
     * Added for MR-WF-16-00034 - Random sampling for QA
     */
    public void qaReviewed(Long dcrId, String userId, String stepProcResponse) throws ServiceException, WMSDaoException;

}
