package ph.com.sunlife.wms.dao.domain;

/**
 * Custom enumeration for Currency used by DCR.
 * 
 * @author Zainal Limpao
 * 
 */
public enum Currency {

	/**
	 * The Philippine PESO Currency.
	 */
	PHP(1L, "PHP", "Philippine Peso", "PESO"),

	/**
	 * The US DOLLAR Currency.
	 */
	USD(2L, "USD", "US Dollar", "US DOLLAR");

	private Long id;

	private String name;

	private String description;

	private String ipacDescription;

	private Currency(Long id, String name, String description,
			String ipacDescription) {
		this.id = id;
		this.name = name;
		this.description = description;
		this.ipacDescription = ipacDescription;
	}

	public String getName() {
		return name;
	}

	public String getDescription() {
		return description;
	}

	public Long getId() {
		return id;
	}

	public static Currency getCurrency(Long id) {
		for (Currency cur : Currency.values()) {
			if (cur.getId().equals(id)) {
				return cur;
			}
		}

		return null;
	}

	public static Currency getCurrency(String name) {
		for (Currency cur : Currency.values()) {
			if (cur.getName().equals(name)) {
				return cur;
			}
		}

		return null;
	}

	public String getIpacDescription() {
		return ipacDescription;
	}

}
