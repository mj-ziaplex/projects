package ph.com.sunlife.wms.web.controller;

import ph.com.sunlife.wms.dao.domain.Company;
import ph.com.sunlife.wms.dao.domain.ProductType;

public class SLFPIController extends AbstractBalancingToolController {
	
	private String displayViewName = "slfpiBalancingToolView";
	
	private String pageTitle;
	
	@Override
	protected Company getCompany() {
		return Company.SLFPI;
	}
	
	@Override
	protected String getDisplayViewName() {
		return this.displayViewName;
	}

	public void setDisplayViewName(String displayViewName) {
		this.displayViewName = displayViewName;
	}
	
	
	@Override
	protected ProductType[] productTypesToDisplay() {
		
		return new ProductType[] { ProductType.SLIFPI_PN, ProductType.SLIFPI_GAF};
	}

	@Override
	protected String getPageTitle() {
		return pageTitle;
	}
	
	public void setPageTitle(String pageTitle) {
		this.pageTitle = pageTitle;
	}

}
