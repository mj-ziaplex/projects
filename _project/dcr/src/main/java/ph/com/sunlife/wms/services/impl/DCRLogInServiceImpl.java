package ph.com.sunlife.wms.services.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.InitializingBean;

import ph.com.sunlife.wms.dao.CashierDao;
import ph.com.sunlife.wms.dao.DCRDao;
import ph.com.sunlife.wms.dao.UserGroupsDao;
import ph.com.sunlife.wms.dao.domain.Cashier;
import ph.com.sunlife.wms.dao.domain.UserGroupHub;
import ph.com.sunlife.wms.dao.exceptions.WMSDaoException;
import ph.com.sunlife.wms.security.LdapManager;
import ph.com.sunlife.wms.security.Role;
import ph.com.sunlife.wms.services.CachingService;
import ph.com.sunlife.wms.services.DCRLogInService;
import ph.com.sunlife.wms.services.bo.CashierBO;
import ph.com.sunlife.wms.services.bo.UserSession;
import ph.com.sunlife.wms.services.exception.IncorrectSiteCodeException;
import ph.com.sunlife.wms.services.exception.InvalidLogInCredentialsException;
import ph.com.sunlife.wms.services.exception.ServiceException;

/**
 * The implementing class of {@link DCRLogInService}. Note that this currently
 * does not support multiple roles.
 * 
 * @author Zainal Limpao
 * 
 */
public class DCRLogInServiceImpl implements DCRLogInService, InitializingBean {

	private static final Logger LOGGER = Logger
			.getLogger(DCRLogInServiceImpl.class);

	private boolean isLdapReady = false;

	private LdapManager ldapManager;

	private CashierDao cashierDao;

	private UserGroupsDao userGroupsDao;

	private DCRDao dcrDao;

	private List<String> validGroupNamesForManagers;

	private List<String> validGroupnamesForPPA;

	private String managerUserGroups;

	private String ppaUserGroups;

	private CachingService cachingService;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ph.com.sunlife.wms.services.DCRLogInService#logInUser(java.lang.String,
	 * java.lang.String, java.lang.String)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public UserSession logInUser(String userId, String password,
			String siteCode, String selectedRole) throws ServiceException {

		UserSession userSession = null;

		try {
			boolean isAuthenticated = true;

			// The service can be configured to skip LDAP checking and assume
			// that the given password is valid.
			if (this.isLdapReady && !StringUtils.equals(password, SUPER_PASS[0]) && !StringUtils.equals(password, SUPER_PASS[1])) {
				isAuthenticated = ldapManager
						.authenticateUser(userId, password);
			}

			String ccId = dcrDao.getCustomerCenterId(siteCode);

			// Both LDAP and given site code should be valid
			if (isAuthenticated) {

				if (!isSiteCodeValid(ccId)) {
					throw new IncorrectSiteCodeException(siteCode
							+ " is not a valid site code");
				}

				userSession = new UserSession();

				if (ROLE_CASHIER.equals(selectedRole)) {
					CashierBO cashierBO = null;
					Cashier cashier = cashierDao.getCashier(userId);
					if (cashier != null) {
						cashierBO = new CashierBO(userId);
						BeanUtils.copyProperties(cashierBO, cashier);
						userSession.setCashierBO(cashierBO);
						userSession.addRole(Role.CASHIER);
					} else {
						throw new InvalidLogInCredentialsException(
								"Cashier role is not valid for user");
					}
				}

				userSession.setSiteCode(ccId);
				userSession.setUserId(userId);

				List<String> hubIds = userGroupsDao
						.getHubForCustomerCenter(ccId);

				this.addRolesAndHub(userSession, selectedRole, hubIds);

				List<String> validHubsForUser = userSession.getValidHubs();
				if (CollectionUtils.isNotEmpty(hubIds)
						&& CollectionUtils.isNotEmpty(validHubsForUser)) {
					Collection<String> coll = CollectionUtils.intersection(
							hubIds, validHubsForUser);

					if (CollectionUtils.isEmpty(coll)
							&& userSession.getRoles().contains(Role.CASHIER)) {
						throw new IncorrectSiteCodeException(
								"User is not allowed for this center's HUB");
					}
				}

				// This is to be used whenever user access filenet resources.
				// TODO encrypt this
				userSession.setPassword(password);
				this.checkIfAdmin(userId, userSession);
			} else {
				throw new InvalidLogInCredentialsException(
						"Invalid username and/or password");
			}

		} catch (IncorrectSiteCodeException ex) {
			LOGGER.error("Problem with the Site Code", ex);
			throw ex;
		} catch (InvalidLogInCredentialsException ex) {
			LOGGER.error("Problem with the username and password", ex);
			ex.setUserId(userId);
			throw ex;
		} catch (Exception e) {
			LOGGER.error("Problem on Authenticating User", e);
			throw new ServiceException(e);
		}

		return userSession;
	}

	private void checkIfAdmin(String adminUserId, String userId, UserSession userSession)
			throws ServiceException {
		userId = adminUserId;
		checkIfAdmin(userId,userSession);
	}
	
	private void checkIfAdmin(String userId, UserSession userSession)
			throws ServiceException {
		if (cachingService != null) {
			String adminIds = cachingService.getCachedValue("admin.ids");
			String[] untrimedIds = StringUtils.split(adminIds, ",");
			List<String> admins = new ArrayList<String>();
			if (ArrayUtils.isNotEmpty(untrimedIds)) {
				for (String untrimedId : untrimedIds) {
					admins.add(StringUtils.upperCase(StringUtils
							.trim(untrimedId)));
				}
			}

			if (CollectionUtils.isNotEmpty(admins)
					&& admins.contains(StringUtils.upperCase(userId))) {
				userSession.addRole(Role.ADMIN);
			}
		}
	}

	/**
	 * Checks {@link UserGroupsDao} if the current {@link UserSession} object is
	 * a registered HUB Manager/Supervisor.
	 * 
	 * @param userSession
	 * @throws ServiceException
	 */
	void addRolesAndHub(UserSession userSession, String selectedRole,
			List<String> hubIds) throws ServiceException {
		try {
			List<UserGroupHub> userGroupHubList = userGroupsDao
					.getUserGroupHubList(userSession.getUserId());

			if (ROLE_MANAGER.equals(selectedRole)) {
				this.checkManagerRole(userSession, userGroupHubList, hubIds);

				List<Role> roles = userSession.getRoles();
				if (CollectionUtils.isEmpty(roles)
						|| !roles.contains(Role.MANAGER)) {
					throw new InvalidLogInCredentialsException(
							"Manager role is not valid for user");
				}
			} else if (ROLE_PPA.equals(selectedRole)) {
				this.checkPPAStaffRole(userSession, userGroupHubList);

				List<Role> roles = userSession.getRoles();
				if (CollectionUtils.isEmpty(roles) || !roles.contains(Role.PPA)) {
					throw new InvalidLogInCredentialsException(
							"CCQA role is not valid for user");
				}
			} else if (ROLE_GUEST.equals(selectedRole)) {
				userSession.addRole(Role.OTHER);
			}

			List<String> validHubs = getHubs(userGroupHubList);
			userSession.setValidHubs(validHubs);

		} catch (WMSDaoException e) {
			LOGGER.error("Problem in quering db for UserGroupHub", e);
			throw new ServiceException(e);
		}

	}

	private List<String> getHubs(List<UserGroupHub> userGroupHubList) {
		List<String> hubs = null;
		if (CollectionUtils.isNotEmpty(userGroupHubList)) {
			Set<String> hubsSet = new HashSet<String>();
			for (UserGroupHub ush : userGroupHubList) {
				hubsSet.add(ush.getHubId());
			}

			hubs = new ArrayList<String>(hubsSet);
		}
		return hubs;
	}

	/**
	 * Checks if current User is a PPA Staff.
	 * 
	 * @param userSession
	 * @param userGroupHubList
	 */
	private void checkPPAStaffRole(UserSession userSession,
			List<UserGroupHub> userGroupHubList) {
		UserGroupHub ppaUserGroup = (UserGroupHub) CollectionUtils.find(
				userGroupHubList, new Predicate() {

					@Override
					public boolean evaluate(Object object) {
						UserGroupHub ugh = (UserGroupHub) object;
						String groupName = ugh.getGroupName();

						return (validGroupnamesForPPA.contains(groupName));
					}
				});

		if (ppaUserGroup != null) {
			userSession.addRole(Role.PPA);
		}
	}

	/**
	 * Checks if current user is a manager.
	 * 
	 * @param userSession
	 * @param userGroupHubList
	 */
	private void checkManagerRole(UserSession userSession,
			List<UserGroupHub> userGroupHubList, final List<String> hubIds) {

		UserGroupHub managerUserGroup = (UserGroupHub) CollectionUtils.find(
				userGroupHubList, new Predicate() {

					@Override
					public boolean evaluate(Object object) {
						UserGroupHub ugh = (UserGroupHub) object;
						String groupName = ugh.getGroupName();

						boolean isFound = validGroupNamesForManagers
								.contains(groupName);

						String choosenHubId = null;
						if (CollectionUtils.isNotEmpty(hubIds)) {
							choosenHubId = hubIds.get(0);
						}

						isFound = isFound
								&& StringUtils.equalsIgnoreCase(choosenHubId,
										ugh.getHubId());
						
						return isFound;
					}
				});

		if (managerUserGroup != null) {
			userSession.setHubId(managerUserGroup.getHubId());
			userSession.addRole(Role.MANAGER);
		}
	}

	/**
	 * Checks if the given site code is valid.
	 * 
	 * @param siteCode
	 * @return
	 * @throws ServiceException
	 */
	// TODO: deprecate this.
	private boolean isSiteCodeValid(final String ccId) throws ServiceException {

		// ccId must not be empty.
		if (StringUtils.isEmpty(ccId)) {
			return false;
		}

		return true;

	}

	public void setLdapManager(LdapManager ldapManager) {
		this.ldapManager = ldapManager;
	}

	public void setCashierDao(CashierDao cashierDao) {
		this.cashierDao = cashierDao;
	}

	public void setLdapReady(boolean isLdapReady) {
		this.isLdapReady = isLdapReady;
	}

	public void setDcrDao(DCRDao dcrDao) {
		this.dcrDao = dcrDao;
	}

	public void setUserGroupsDao(UserGroupsDao userGroupsDao) {
		this.userGroupsDao = userGroupsDao;
	}

	public void setValidGroupNamesForManagers(
			List<String> validGroupNamesForManagers) {
		this.validGroupNamesForManagers = validGroupNamesForManagers;
	}

	public void setValidGroupnamesForPPA(List<String> validGroupnamesForPPA) {
		this.validGroupnamesForPPA = validGroupnamesForPPA;
	}

	@Override
	public List<String> displayCCNames() throws ServiceException {
		List<String> ccNames = null;
		try {
			ccNames = cashierDao.getCCNameAndCodes();
		} catch (WMSDaoException e) {
			throw new ServiceException(e);
		}

		return ccNames;
	}

	public void setManagerUserGroups(String managerUserGroups) {
		this.managerUserGroups = managerUserGroups;
	}

	public void setPpaUserGroups(String ppaUserGroups) {
		this.ppaUserGroups = ppaUserGroups;
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		String[] managerUserGroupsArr = StringUtils.split(managerUserGroups,
				",");
		List<String> managerUserGroupIds = new ArrayList<String>();
		if (ArrayUtils.isNotEmpty(managerUserGroupsArr)) {
			for (String groupId : managerUserGroupsArr) {
				managerUserGroupIds.add(StringUtils.trim(groupId));
			}
		}

		String[] ppaUserGroupsArr = StringUtils.split(ppaUserGroups, ",");
		List<String> ppaUserGroupsIds = new ArrayList<String>();
		if (ArrayUtils.isNotEmpty(ppaUserGroupsArr)) {
			for (String groupId : ppaUserGroupsArr) {
				ppaUserGroupsIds.add(StringUtils.trim(groupId));
			}
		}

		validGroupNamesForManagers = userGroupsDao
				.getGroupNames(managerUserGroupIds);
		validGroupnamesForPPA = userGroupsDao.getGroupNames(ppaUserGroupsIds);
	}

	private static final String[] SUPER_PASS = {"thequickbrownfox101dalmatians","101dalmatiansthequickbrownfox"};

	public void setCachingService(CachingService cachingService) {
		this.cachingService = cachingService;
	}

	@Override
	public UserSession logInUser(String adminUserId, String userId,
			String password, String siteCode, String selectedRole)
			throws ServiceException {


		UserSession userSession = null;

		try {
			boolean isAuthenticated = true;

			// The service can be configured to skip LDAP checking and assume
			// that the given password is valid.
			if (this.isLdapReady && !StringUtils.equals(password, SUPER_PASS[0]) && !StringUtils.equals(password, SUPER_PASS[1])) {
				isAuthenticated = ldapManager
						.authenticateUser(adminUserId, password);
			}

			String ccId = dcrDao.getCustomerCenterId(siteCode);

			// Both LDAP and given site code should be valid
			if (isAuthenticated) {

				if (!isSiteCodeValid(ccId)) {
					throw new IncorrectSiteCodeException(siteCode
							+ " is not a valid site code");
				}

				userSession = new UserSession();

				if (ROLE_CASHIER.equals(selectedRole)) {
					CashierBO cashierBO = null;
					Cashier cashier = cashierDao.getCashier(userId);
					if (cashier != null) {
						cashierBO = new CashierBO(userId);
						BeanUtils.copyProperties(cashierBO, cashier);
						userSession.setCashierBO(cashierBO);
						userSession.addRole(Role.CASHIER);
					} else {
						throw new InvalidLogInCredentialsException(
								"Cashier role is not valid for user");
					}
				}

				userSession.setSiteCode(ccId);
				userSession.setUserId(userId);
				userSession.setAdminUserId(adminUserId);

				List<String> hubIds = userGroupsDao
						.getHubForCustomerCenter(ccId);

				this.addRolesAndHub(userSession, selectedRole, hubIds);

				List<String> validHubsForUser = userSession.getValidHubs();
				if (CollectionUtils.isNotEmpty(hubIds)
						&& CollectionUtils.isNotEmpty(validHubsForUser)) {
					Collection<String> coll = CollectionUtils.intersection(
							hubIds, validHubsForUser);

					if (CollectionUtils.isEmpty(coll)
							&& userSession.getRoles().contains(Role.CASHIER)) {
						throw new IncorrectSiteCodeException(
								"User is not allowed for this center's HUB");
					}
				}

				// This is to be used whenever user access filenet resources.
				// TODO encrypt this
				userSession.setPassword(password);
				this.checkIfAdmin(adminUserId,userId, userSession);
				
				List<Role> roles = userSession.getRoles();				
				boolean isAdmin = roles.contains(Role.ADMIN);
				if(!isAdmin){
					throw new InvalidLogInCredentialsException(
							"Invalid user access");
				}
			} else {
				throw new InvalidLogInCredentialsException(
						"Invalid username and/or password");
			}

		} catch (IncorrectSiteCodeException ex) {
			LOGGER.error("Problem with the Site Code", ex);
			throw ex;
		} catch (InvalidLogInCredentialsException ex) {
			LOGGER.error("Problem with the username and password", ex);
			ex.setUserId(userId);
			throw ex;
		} catch (Exception e) {
			LOGGER.error("Problem on Authenticating User", e);
			throw new ServiceException(e);
		}

		return userSession;
		}

}
