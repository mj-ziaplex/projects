package ph.com.sunlife.wms.web.controller.form;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.FactoryUtils;
import org.apache.commons.collections.ListUtils;

public class PPASPSaveForm extends PPASPForm {
	
	@SuppressWarnings("unchecked")
	private List<NoteForm> noteForms = ListUtils.lazyList(new ArrayList<NoteForm>(),FactoryUtils.instantiateFactory(NoteForm.class));
	
	private String reconRownums;
        
	public List<NoteForm> getNoteForms() {
		return noteForms;
	}

	public void setNoteForms(List<NoteForm> noteForms) {
		this.noteForms = noteForms;
	}

	public String getReconRownums() {
		return reconRownums;
	}

	public void setReconRownums(String reconRownums) {
		this.reconRownums = reconRownums;
	}

    }
