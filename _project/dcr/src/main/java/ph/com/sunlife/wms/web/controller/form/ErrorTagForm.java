package ph.com.sunlife.wms.web.controller.form;

import javax.servlet.http.HttpSession;

import org.springframework.validation.Errors;

import ph.com.sunlife.wms.web.controller.binder.BinderAware;
import ph.com.sunlife.wms.web.controller.binder.SessionAware;

// Change according to Healthcheck
// *Problem was declared as public/protected in BinderAware
// *Change BinderAware interface class to abstract class and extends it rather than implements
public class ErrorTagForm extends BinderAware implements SessionAware {

	private HttpSession session;

	private String dcrId;
	
	private String dcrStatus;
	
	private String dcrDate;
	
	private String concatCashierNameList;
	
	private String ccName;
	
	private boolean fromPPA;
	
	public String getDcrId() {
		return dcrId;
	}

	public void setDcrId(String dcrId) {
		this.dcrId = dcrId;
	}

    // Change according to Healthcheck
    // *Problem was declared as public/protected in BinderAware
    // *Change use BinderAware getter instead of direct access
    public String[] getAllowedFields() {
        return super.getAllowedFields();
    }

    // Change according to Healthcheck
    // *Problem was declared as public/protected in BinderAware
    // *Change use BinderAware getter instead of direct access
    public String[] getRequiredFields() {
        return super.getRequiredFields();
    }

    // Change according to Healthcheck
    // *Problem was declared as public/protected in BinderAware
    // *Change use BinderAware getter instead of direct access
    public String[] getUncheckedFields() {
        return super.getUncheckedFields();
    }

	@Override
	public void afterBind() {
	}

	@Override
	public boolean validate(Errors errors) {
		return false;
	}

	public HttpSession getSession() {
		return session;
	}

	public void setSession(HttpSession session) {
		this.session = session;
	}

	public String getSecretKey() {
		return null;
	}

	public String getDcrStatus() {
		return dcrStatus;
	}

	public void setDcrStatus(String dcrStatus) {
		this.dcrStatus = dcrStatus;
	}

	public String getConcatCashierNameList() {
		return concatCashierNameList;
	}

	public void setConcatCashierNameList(String concatCashierNameList) {
		this.concatCashierNameList = concatCashierNameList;
	}

	public String getDcrDate() {
		return dcrDate;
	}

	public void setDcrDate(String dcrDate) {
		this.dcrDate = dcrDate;
	}

	public String getCcName() {
		return ccName;
	}

	public void setCcName(String ccName) {
		this.ccName = ccName;
	}

	public boolean isFromPPA() {
		return fromPPA;
	}

	public void setFromPPA(boolean fromPPA) {
		this.fromPPA = fromPPA;
	}

}
