package ph.com.sunlife.wms.dao.domain;

public class Excemption {

	private Long dcrId;
	
	private String acf2id;
	
	private double worksiteAmount;
	
	private Long productTypeId;
	
	private Double reversalAmount = 0.0;
	
	public Double getReversalAmount() {
		return reversalAmount;
	}

	public void setReversalAmount(Double reversalAmount) {
		this.reversalAmount = reversalAmount;
	}

	public Long getDcrId() {
		return dcrId;
	}

	public void setDcrId(Long dcrId) {
		this.dcrId = dcrId;
	}

	public String getAcf2id() {
		return acf2id;
	}

	public void setAcf2id(String acf2id) {
		this.acf2id = acf2id;
	}

	public double getWorksiteAmount() {
		return worksiteAmount;
	}

	public void setWorksiteAmount(double worksiteAmount) {
		this.worksiteAmount = worksiteAmount;
	}

	public Long getProductTypeId() {
		return productTypeId;
	}

	public void setProductTypeId(Long productTypeId) {
		this.productTypeId = productTypeId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((acf2id == null) ? 0 : acf2id.hashCode());
		result = prime * result + ((dcrId == null) ? 0 : dcrId.hashCode());
		result = prime * result
				+ ((productTypeId == null) ? 0 : productTypeId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Excemption other = (Excemption) obj;
		if (acf2id == null) {
			if (other.acf2id != null)
				return false;
		} else if (!acf2id.equals(other.acf2id))
			return false;
		if (dcrId == null) {
			if (other.dcrId != null)
				return false;
		} else if (!dcrId.equals(other.dcrId))
			return false;
		if (productTypeId == null) {
			if (other.productTypeId != null)
				return false;
		} else if (!productTypeId.equals(other.productTypeId))
			return false;
		return true;
	}
	
}
