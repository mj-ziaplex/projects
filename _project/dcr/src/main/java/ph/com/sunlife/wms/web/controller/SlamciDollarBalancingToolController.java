package ph.com.sunlife.wms.web.controller;

import ph.com.sunlife.wms.dao.domain.Company;

public class SlamciDollarBalancingToolController extends
		AbstractBalancingToolController {

	private String displayViewName = "slamciBalancingToolView";

	@Override
	protected Company getCompany() {
		return Company.SLAMCID;
	}

	@Override
	protected String getDisplayViewName() {
		return this.displayViewName;
	}

	public void setDisplayViewName(String displayViewName) {
		this.displayViewName = displayViewName;
	}

	@Override
	protected String getPageTitle() {
		return "SLAMCI Dollar Balancing Tool";
	}

}
