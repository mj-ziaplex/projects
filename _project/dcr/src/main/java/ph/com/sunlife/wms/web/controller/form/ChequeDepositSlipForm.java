package ph.com.sunlife.wms.web.controller.form;

import java.util.Date;

import org.apache.commons.lang.StringUtils;

import ph.com.sunlife.wms.dao.domain.CheckType;
import ph.com.sunlife.wms.dao.domain.Company;
import ph.com.sunlife.wms.dao.domain.Currency;
import ph.com.sunlife.wms.dao.domain.ProductType;
import ph.com.sunlife.wms.util.IpacUtil;
import ph.com.sunlife.wms.util.WMSDateUtil;

public class ChequeDepositSlipForm  extends CashierWorkItemForm{


	private String depositoryBankId;

	private Long companyId;

	private Company company;
	
	private String companyStr;
	
	private String accountNumber;
	
	private String customerCenter;
	
	private String customerCenterName;
	
	private String prodCode;

	private double hiddenAmountChequeOnUs;
	
	private double hiddenAmountChequeLocal;
	
	private double hiddenAmountChequeRegional;
	
	private double hiddenAmountChequeOt;
	
	private double hiddenAmountBankOTC;
	
	private double hiddenAmountDollarCheck;
	
	private double hiddenAmountUSCheckInManila;
	
	private double hiddenAmountUSCheckOutsidePH;
	
	private double totalCheckAmount;
	
	private double requiredTotalAmount;

	private Date depositDate;
	
	private String depositDateStr;
	
	private Date dcrDate;
	
	private String dcrDateStr;
	
	private ProductType productType;

	private CheckType checkType;
	
	private long checkTypeId;

	private Currency currency;

	private Long currencyId;
	
	private String acf2id;
	
	private String cashierName;
	
	private String barcodeStr;
	
	private double hiddenSlfpiTotal;
	
	private double hiddenSlocpiTotal;
	
	private double hiddenCollectionTotal;
	
	private double hiddenMixedChequeTotal;

	private String chequeType;
	
	private double hiddenAmountCheque;
	
	private double hiddenTradVulCollectionTotal;
	
	private double hiddenGrpLifeCollectionTotal;
	
	public String getDepositoryBankId() {
		return depositoryBankId;
	}

	public void setDepositoryBankId(String depositoryBankId) {
		this.depositoryBankId = depositoryBankId;
	}

	public void setCustomerCenter(String customerCenter) {
		this.customerCenter = customerCenter;
	}

	public String getCustomerCenter() {
		return customerCenter;
	}

	public void setHiddenAmountChequeLocal(double hiddenAmountChequeLocal) {
		this.hiddenAmountChequeLocal = hiddenAmountChequeLocal;
	}

	public double getHiddenAmountChequeLocal() {
		return hiddenAmountChequeLocal;
	}

	public void setHiddenAmountChequeRegional(double hiddenAmountChequeRegional) {
		this.hiddenAmountChequeRegional = hiddenAmountChequeRegional;
	}

	public double getHiddenAmountChequeRegional() {
		return hiddenAmountChequeRegional;
	}

	public void setHiddenAmountChequeOnUs(double hiddenAmountChequeOnUs) {
		this.hiddenAmountChequeOnUs = hiddenAmountChequeOnUs;
	}

	public double getHiddenAmountChequeOnUs() {
		return hiddenAmountChequeOnUs;
	}



	public double getHiddenAmountChequeOt() {
		return hiddenAmountChequeOt;
	}

	public void setHiddenAmountChequeOt(double hiddenAmountChequeOt) {
		this.hiddenAmountChequeOt = hiddenAmountChequeOt;
	}

	public double getHiddenAmountBankOTC() {
		return hiddenAmountBankOTC;
	}

	public void setHiddenAmountBankOTC(double hiddenAmountBankOTC) {
		this.hiddenAmountBankOTC = hiddenAmountBankOTC;
	}

	public double getHiddenAmountDollarCheck() {
		return hiddenAmountDollarCheck;
	}

	public void setHiddenAmountDollarCheck(double hiddenAmountDollarCheck) {
		this.hiddenAmountDollarCheck = hiddenAmountDollarCheck;
	}

	public double getHiddenAmountUSCheckInManila() {
		return hiddenAmountUSCheckInManila;
	}

	public void setHiddenAmountUSCheckInManila(double hiddenAmountUSCheckInManila) {
		this.hiddenAmountUSCheckInManila = hiddenAmountUSCheckInManila;
	}

	public double getHiddenAmountUSCheckOutsidePH() {
		return hiddenAmountUSCheckOutsidePH;
	}

	public void setHiddenAmountUSCheckOutsidePH(double hiddenAmountUSCheckOutsidePH) {
		this.hiddenAmountUSCheckOutsidePH = hiddenAmountUSCheckOutsidePH;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public double getTotalCheckAmount() {
		return totalCheckAmount;
	}

	public void setTotalCheckAmount(double totalCheckAmount) {
		this.totalCheckAmount = totalCheckAmount;
	}

	public double getRequiredTotalAmount() {
		return requiredTotalAmount;
	}

	public void setRequiredTotalAmount(double requiredTotalAmount) {
		this.requiredTotalAmount = requiredTotalAmount;
	}

	public Date getDepositDate() {
		return depositDate;
	}

	public void setDepositDate(Date depositDate) {
		this.depositDate = depositDate;
		this.depositDateStr = StringUtils.upperCase(WMSDateUtil
				.toFormattedDateStr(depositDate));
	}

	public ProductType getProductType() {
		return productType;
	}

	public void setProductType(ProductType productType) {
		this.productType = productType;
	}

	public CheckType getCheckType() {
		return checkType;
	}

	public void setCheckType(CheckType checkType) {
		this.checkType = checkType;
		this.checkTypeId = checkType.getId();
	}
	
	public void setCheckTypeId(long checkTypeId) {
		this.checkTypeId = checkTypeId;
	}

	public long getCheckTypeId() {
		return checkTypeId;
	}


	public Currency getCurrency() {
		return currency;
	}

	public void setCurrency(Currency currency) {
		this.currency = currency;
		this.setCurrencyId(currency.getId());
	}

	public void setDepositDateStr(String depositDateStr) {
		this.depositDateStr = StringUtils.upperCase(depositDateStr);
		this.depositDate = WMSDateUtil.toDate(depositDateStr);
	}

	public String getDepositDateStr() {
		return StringUtils.upperCase(depositDateStr);
	}
	
	public String getDcrDateStr() {
		return StringUtils.upperCase(dcrDateStr);
	}

	public void setDcrDateStr(String dcrDateStr) {
		this.dcrDateStr = StringUtils.upperCase(dcrDateStr);
		this.dcrDate = WMSDateUtil.toDate(dcrDateStr);
	}
	
	public Date getDcrDate() {
		return dcrDate;
	}

	public void setDcrDate(Date dcrDate) {
		this.dcrDate = dcrDate;
		this.dcrDateStr = StringUtils.upperCase(WMSDateUtil
				.toFormattedDateStr(dcrDate));
	}

	public Long getCompanyId() {
		return companyId;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
		this.companyId = company.getId();
		this.companyStr = IpacUtil.toIpacComCode(company.getId());
	}

	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
		this.company = Company.getCompany(companyId);
		this.companyStr = IpacUtil.toIpacComCode(companyId);
	}

	public void setCompanyStr(String companyStr) {
		this.companyStr = companyStr;
	}

	public String getCompanyStr() {
		return companyStr;
	}

	public void setCurrencyId(Long currencyId) {
		this.currencyId = currencyId;
		this.currency = Currency.getCurrency(currencyId);
	}

	public Long getCurrencyId() {
		return currencyId;
	}

	public void setProdCode(String prodCode) {
		this.prodCode = prodCode;
	}

	public String getProdCode() {
		return prodCode;
	}

	public String getCustomerCenterName() {
		return customerCenterName;
	}

	public void setCustomerCenterName(String customerCenterName) {
		this.customerCenterName = customerCenterName;
	}

	public String getCashierName() {
		return cashierName;
	}

	public void setCashierName(String cashierName) {
		this.cashierName = cashierName;
	}

	public String getBarcodeStr() {
		return barcodeStr;
	}

	public void setBarcodeStr(String barcodeStr) {
		this.barcodeStr = barcodeStr;
	}

	public double getHiddenSlfpiTotal() {
		return hiddenSlfpiTotal;
	}

	public void setHiddenSlfpiTotal(double hiddenSlfpiTotal) {
		this.hiddenSlfpiTotal = hiddenSlfpiTotal;
	}

	public double getHiddenSlocpiTotal() {
		return hiddenSlocpiTotal;
	}

	public void setHiddenSlocpiTotal(double hiddenSlocpiTotal) {
		this.hiddenSlocpiTotal = hiddenSlocpiTotal;
	}

	public double getHiddenCollectionTotal() {
		return hiddenCollectionTotal;
	}

	public void setHiddenCollectionTotal(double hiddenCollectionTotal) {
		this.hiddenCollectionTotal = hiddenCollectionTotal;
	}

	public double getHiddenMixedChequeTotal() {
		return hiddenMixedChequeTotal;
	}

	public void setHiddenMixedChequeTotal(double hiddenMixedChequeTotal) {
		this.hiddenMixedChequeTotal = hiddenMixedChequeTotal;
	}

	public String getChequeType() {
		return chequeType;
	}

	public void setChequeType(String chequeType) {
		this.chequeType = chequeType;
	}

	public double getHiddenAmountCheque() {
		return hiddenAmountCheque;
	}

	public void setHiddenAmountCheque(double hiddenAmountCheque) {
		this.hiddenAmountCheque = hiddenAmountCheque;
	}

	public double getHiddenTradVulCollectionTotal() {
		return hiddenTradVulCollectionTotal;
	}

	public void setHiddenTradVulCollectionTotal(double hiddenTradVulCollectionTotal) {
		this.hiddenTradVulCollectionTotal = hiddenTradVulCollectionTotal;
	}

	public double getHiddenGrpLifeCollectionTotal() {
		return hiddenGrpLifeCollectionTotal;
	}

	public void setHiddenGrpLifeCollectionTotal(double hiddenGrpLifeCollectionTotal) {
		this.hiddenGrpLifeCollectionTotal = hiddenGrpLifeCollectionTotal;
	}

	public String getAcf2id() {
		return acf2id;
	}

	public void setAcf2id(String acf2id) {
		this.acf2id = acf2id;
	}




}
