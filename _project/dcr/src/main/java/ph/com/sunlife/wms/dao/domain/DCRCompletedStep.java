package ph.com.sunlife.wms.dao.domain;

import java.util.List;


public class DCRCompletedStep {
//	private String stepId;
//	private String stepName;
	private DCRStep dcrStep;
	private String stepCompletorUserId;
	private List<DCRError> dcrErrors;





	public String getStepCompletorUserId() {
		return stepCompletorUserId;
	}

	public void setStepCompletorUserId(String stepCompletorUserId) {
		this.stepCompletorUserId = stepCompletorUserId;
	}

	public DCRStep getDcrStep() {
		return dcrStep;
	}

	public void setDcrStep(DCRStep dcrStep) {
		this.dcrStep = dcrStep;
	}


	public List<DCRError> getDcrErrors() {
		return dcrErrors;
	}

	public void setDcrErrors(List<DCRError> dcrErrors) {
		this.dcrErrors = dcrErrors;
	}
	

}
