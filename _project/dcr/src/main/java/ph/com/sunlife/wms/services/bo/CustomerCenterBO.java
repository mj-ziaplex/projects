package ph.com.sunlife.wms.services.bo;

public class CustomerCenterBO implements Comparable<CustomerCenterBO> {

	private String ccId;

	private String ccName;

	private String ccCode;

	public String getCcId() {
		return ccId;
	}

	public void setCcId(String ccId) {
		this.ccId = ccId;
	}

	public String getCcName() {
		return ccName;
	}

	public void setCcName(String ccName) {
		this.ccName = ccName;
	}

	public String getCcCode() {
		return ccCode;
	}

	public void setCcCode(String ccCode) {
		this.ccCode = ccCode;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((ccId == null) ? 0 : ccId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CustomerCenterBO other = (CustomerCenterBO) obj;
		if (ccId == null) {
			if (other.ccId != null)
				return false;
		} else if (!ccId.equals(other.ccId))
			return false;
		return true;
	}

	@Override
	public int compareTo(CustomerCenterBO other) {
		return this.getCcName().compareTo(other.getCcName());
	}

}
