package ph.com.sunlife.wms.dao.domain;

import java.util.Date;


public class DCRErrorTagLog extends Entity {
	
	private Long dcrId;
	
	private Date datePosted;
	
	private Date dateUpdated;
	
	private String reason;
	
	private String postedUpdatedById;
	
	private String reasonDeleted;
	
	private String stepId;
	
	private String stepCompletorUserId;
	
	public Long getDcrId() {
		return dcrId;
	}

	public void setDcrId(Long dcrId) {
		this.dcrId = dcrId;
	}

	public Date getDatePosted() {
		return datePosted;
	}

	public void setDatePosted(Date datePosted) {
		this.datePosted = datePosted;
	}

	public Date getDateUpdated() {
		return dateUpdated;
	}

	public void setDateUpdated(Date dateUpdated) {
		this.dateUpdated = dateUpdated;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getPostedUpdatedById() {
		return postedUpdatedById;
	}

	public void setPostedUpdatedById(String postedUpdatedById) {
		this.postedUpdatedById = postedUpdatedById;
	}

	public String getReasonDeleted() {
		return reasonDeleted;
	}

	public void setReasonDeleted(String reasonDeleted) {
		this.reasonDeleted = reasonDeleted;
	}

	public String getStepId() {
		return stepId;
	}

	public void setStepId(String stepId) {
		this.stepId = stepId;
	}

	public String getStepCompletorUserId() {
		return stepCompletorUserId;
	}

	public void setStepCompletorUserId(String stepCompletorUserId) {
		this.stepCompletorUserId = stepCompletorUserId;
	}
	
}
