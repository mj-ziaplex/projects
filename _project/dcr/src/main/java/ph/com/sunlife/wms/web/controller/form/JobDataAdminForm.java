package ph.com.sunlife.wms.web.controller.form;

import java.util.Date;

import javax.servlet.http.HttpSession;

import org.springframework.validation.Errors;

import ph.com.sunlife.wms.web.controller.binder.BinderAware;
import ph.com.sunlife.wms.web.controller.binder.SessionAware;

// Change according to Healthcheck
// *Problem was declared as public/protected in BinderAware
// *Change BinderAware interface class to abstract class and extends it rather than implements
public class JobDataAdminForm extends BinderAware implements SessionAware {

	private Date dcrDate;
	
	private Long jobDataId;
	
	private HttpSession session;
	
	public Date getDcrDate() {
		return dcrDate;
	}

	public void setDcrDate(Date dcrDate) {
		this.dcrDate = dcrDate;
	}

	public Long getJobDataId() {
		return jobDataId;
	}

	public void setJobDataId(Long jobDataId) {
		this.jobDataId = jobDataId;
	}

	@Override
	public HttpSession getSession() {
		return session;
	}

	@Override
	public void setSession(HttpSession session) {
		this.session = session;
	}

	@Override
	public String getSecretKey() {
		return null;
	}

    // Change according to Healthcheck
    // *Problem was declared as public/protected in BinderAware
    // *Change use BinderAware getter instead of direct access
    public String[] getAllowedFields() {
        return super.getAllowedFields();
    }

    // Change according to Healthcheck
    // *Problem was declared as public/protected in BinderAware
    // *Change use BinderAware getter instead of direct access
    public String[] getRequiredFields() {
        return super.getRequiredFields();
    }

    // Change according to Healthcheck
    // *Problem was declared as public/protected in BinderAware
    // *Change use BinderAware getter instead of direct access
    public String[] getUncheckedFields() {
        return super.getUncheckedFields();
    }

	@Override
	public void afterBind() {
	}

	@Override
	public boolean validate(Errors errors) {
		return true;
	}

}
