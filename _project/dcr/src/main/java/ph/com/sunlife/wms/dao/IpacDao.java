/**
 * @author i176
 * ****************************************************************************
 * @author: ia23 - sasay
 * @date: Mar2016
 * @desc: Removed methods that will not be used due to MR-WF-15-00089 - DCR
 * Redesign
 */
package ph.com.sunlife.wms.dao;

import java.util.Date;
import java.util.List;

import ph.com.sunlife.wms.dao.domain.Bank;
import ph.com.sunlife.wms.dao.domain.Currency;
import ph.com.sunlife.wms.dao.domain.DCRCashDepositSlip;
import ph.com.sunlife.wms.dao.domain.DCRChequeDepositSlip;
import ph.com.sunlife.wms.dao.exceptions.WMSDaoException;

public interface IpacDao {

    boolean hasCollections(String centerCode, Date processDate, String acf2id) throws WMSDaoException;

    DCRCashDepositSlip populateBankAccount(String centerCode, DCRCashDepositSlip entity) throws WMSDaoException;

    DCRChequeDepositSlip populateBankAccount(String centerCode, DCRChequeDepositSlip entity) throws WMSDaoException;

    int getCurrentPdcCount(String customerCenterId) throws WMSDaoException;

    int getCurrentTradVulPdcCount(String customerCenterId) throws WMSDaoException;

    int getCurrentPNPdcCount(String customerCenterId) throws WMSDaoException;

    List<Bank> getAllDraweeBanks() throws WMSDaoException;

    int getSalesSlipCount(String siteCode, String prodCode, Date processDate, Currency currency, String userId) throws WMSDaoException;
}
