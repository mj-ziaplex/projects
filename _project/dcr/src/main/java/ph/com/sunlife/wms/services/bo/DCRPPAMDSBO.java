package ph.com.sunlife.wms.services.bo;

import static ph.com.sunlife.wms.util.ParameterMaskUtil.toMaskedString;

public class DCRPPAMDSBO {
	
	private Long id;
	
	private Long dcrId;
	
	private String salesSlipNumber;
	
	private String accountNumber;
	
	private String approvalNumber;
	
	private String reconciled;
	
	private String productCode;
	
	private String userId;
	
	private String reconBy;
	
	private String ppaFindings;
	
	private String ppaNotes;
	
	public String getReconBy() {
		return reconBy;
	}

	public void setReconBy(String reconBy) {
		this.reconBy = reconBy;
	}

	public String getPpaFindings() {
		return ppaFindings;
	}

	public void setPpaFindings(String ppaFindings) {
		this.ppaFindings = ppaFindings;
	}

	public String getPpaNotes() {
		return ppaNotes;
	}

	public void setPpaNotes(String ppaNotes) {
		this.ppaNotes = ppaNotes;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getDcrId() {
		return dcrId;
	}

	public void setDcrId(Long dcrId) {
		this.dcrId = dcrId;
	}

	public String getSalesSlipNumber() {
		return salesSlipNumber;
	}

	public void setSalesSlipNumber(String salesSlipNumber) {
		this.salesSlipNumber = salesSlipNumber;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = toMaskedString(accountNumber);
	}

	public String getApprovalNumber() {
		return approvalNumber;
	}

	public void setApprovalNumber(String approvalNumber) {
		this.approvalNumber = approvalNumber;
	}

	public String getReconciled() {
		return reconciled;
	}

	public void setReconciled(String reconciled) {
		this.reconciled = reconciled;
	}
	
	
}
