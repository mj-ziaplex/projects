package ph.com.sunlife.wms.dao.domain;


public class DCRErrorTagLookupDisplay extends Entity {
	private Long dcrId;
	private String stepId;
	private String stepName;
	private String userName;
	private String errId;
	private String errName;
	public Long getDcrId() {
		return dcrId;
	}
	public void setDcrId(Long dcrId) {
		this.dcrId = dcrId;
	}
	public String getStepId() {
		return stepId;
	}
	public void setStepId(String stepId) {
		this.stepId = stepId;
	}
	public String getStepName() {
		return stepName;
	}
	public void setStepName(String stepName) {
		this.stepName = stepName;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getErrId() {
		return errId;
	}
	public void setErrId(String errId) {
		this.errId = errId;
	}
	public String getErrName() {
		return errName;
	}
	public void setErrName(String errName) {
		this.errName = errName;
	}
	
	
}
