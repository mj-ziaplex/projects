package ph.com.sunlife.wms.dao.domain;

/**
 * The enumeration for all companies supported by DCR.
 * 
 * <p>
 * Note that SLAMCI is divided into two (PESO and Dollar), as it is not possible
 * to combine them in a single balancing tool.
 * </p>
 * 
 * @author Zainal Limpao
 * 
 */
public enum Company {

	/**
	 * The SLAMCI Peso.
	 */
	SLAMCIP(1L),

	/**
	 * The SLAMCI Dollar.
	 */
	SLAMCID(2L),

	/**
	 * The SLOCPI.
	 */
	SLOCPI(3L),

	/**
	 * The SLGFI.
	 */
	SLGFI(4L),

	/**
	 * The SLFPI.
	 */
	SLFPI(5L),

	/**
	 * The OTHER or Collection on Behalf.
	 */
	OTHER(6L);

	private Long id;

	private Company(Long id) {
		this.id = id;
	}

	public Long getId() {
		return this.id;
	}

	public static Company getCompany(Long id) {
		for (Company comp : Company.values()) {
			if (comp.getId().equals(id)) {
				return comp;
			}
		}

		return null;
	}

	public String getName() {
		return name();
	}

}
