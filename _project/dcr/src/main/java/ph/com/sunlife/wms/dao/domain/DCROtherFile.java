package ph.com.sunlife.wms.dao.domain;

import java.util.Date;

public class DCROtherFile extends Entity {

	private Long dcrFileSize;

	private byte[] dcrFile;

	private Date dcrFileUploadDate;

	private String dcrFileDescription;
	
	private DCRCashier dcrCashier = new DCRCashier();
	
	private String userId;
	
	private String versionId;
	
	private String dcrFileType;
	
	public String getDcrFileType() {
		return dcrFileType;
	}

	public void setDcrFileType(String dcrFileType) {
		this.dcrFileType = dcrFileType;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getVersionId() {
		return versionId;
	}

	public void setVersionId(String versionId) {
		this.versionId = versionId;
	}

	public Long getDcrFileSize() {
		return dcrFileSize;
	}

	public void setDcrFileSize(Long dcrFileSize) {
		this.dcrFileSize = dcrFileSize;
	}

	public byte[] getDcrFile() {
		return dcrFile;
	}

	public void setDcrFile(byte[] dcrFile) {
		this.dcrFile = dcrFile;
	}

	public Date getDcrFileUploadDate() {
		return dcrFileUploadDate;
	}

	public void setDcrFileUploadDate(Date dcrFileUploadDate) {
		this.dcrFileUploadDate = dcrFileUploadDate;
	}

	public String getDcrFileDescription() {
		return dcrFileDescription;
	}

	public void setDcrFileDescription(String dcrFileDescription) {
		this.dcrFileDescription = dcrFileDescription;
	}

	public DCRCashier getDcrCashier() {
		return dcrCashier;
	}

	public void setDcrCashier(DCRCashier dcrCashier) {
		this.dcrCashier = dcrCashier;
	}
	
	public void setDcrCashier(Long dcrCashierId) {
		DCRCashier dcrCashier = new DCRCashier();
		dcrCashier.setId(dcrCashierId);
		this.setDcrCashier(dcrCashier);
	}

}
