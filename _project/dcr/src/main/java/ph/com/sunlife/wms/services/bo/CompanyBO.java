package ph.com.sunlife.wms.services.bo;

public enum CompanyBO {

	SLAMCIP(1L), SLAMCID(2L), SLOCPI(3L), SLGFI(4L), SLFPI(5L), OTHER(6L);

	private Long id;
	
	private CompanyBO(Long id) {
		this.id = id;
	}
	
	public Long getId() {
		return this.id;
	}
	
	
	public static CompanyBO getCompany(Long id) {
		for (CompanyBO comp : CompanyBO.values()) {
			if (comp.getId().equals(id)) {
				return comp;
			}
		}
		
		return null;
	}
	
	public String getName() {
		return name();
	}
	
}
