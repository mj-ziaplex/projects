package ph.com.sunlife.wms.util;

import java.util.ArrayList;
import java.util.List;


/**
 * The Class PaginationUtil.
 * 
 * @author Zainal Limpao
 */
public class PaginationUtil {

    /**
     * Gets the page links.
     *
     * @param pageNumber the page number
     * @param itemsPerPage the items per page
     * @param totalItems the total items
     * @return the page links
     */
    public static List<Long> getPageLinks(Long pageNumber, Long itemsPerPage, Long totalItems){

        List<Long> pageList = new ArrayList<Long>();
        List<Long> tmpList = new ArrayList<Long>();
        List<List<Long>> allPages = new ArrayList<List<Long>>();

        if(totalItems <= itemsPerPage){
            pageList.add(1L);
        } else {
            int count = (int)Math.ceil(((double)totalItems)/itemsPerPage);
            for(int i=1; i<=count; i++){
                tmpList.add((long)i);
            }
            allPages = PaginationUtil.sortPerPage(tmpList);
        }

        for (List<Long> pageGrp : allPages) {
            for (Long page : pageGrp) {
                if(page.equals(pageNumber)){
                    pageList = pageGrp;
                    break;
                }
            }
        }

        return pageList;
    }

    /**
     * Determines and sorts pages to be displayed.
     *
     * @param thePages Complete list of pages
     * @return List of pages
     */
    private static List<List<Long>> sortPerPage(List<Long> thePages){
        List<List<Long>> pages = new ArrayList<List<Long>>();

        final int pageLinkCnt = 4;
        final int addedPagesCnt = 3;

        //trim out per page
        Long indexOf1stEntry = 0L;
        int indexOfNextList = 0;
        int lastIndex = pageLinkCnt;

        if(thePages.size() < pageLinkCnt){
            pages.add(thePages.subList(0, thePages.size()));
        }else {
            while(lastIndex <= thePages.size()) {

                if(indexOf1stEntry == 0){
                    pages.add(thePages.subList(0, lastIndex));
                }else{
                    pages.add(thePages.subList(indexOfNextList, lastIndex));
                }
                indexOfNextList = indexOfNextList + addedPagesCnt;
                lastIndex = lastIndex + addedPagesCnt;
                indexOf1stEntry++;
            }
            int pageSize = thePages.size();

            int lastIndexOfPages = pages.size() - 1;
            List<Long> theLastList = pages.get(lastIndexOfPages);
            int lastIndexOfList = pages.get(lastIndexOfPages).size() - 1;
            long lastElementOfList = theLastList.get(lastIndexOfList);

            if(lastElementOfList != pageSize){
                pages.add(thePages.subList(indexOfNextList, pageSize));
            }
        }
        return pages;
    }
}