package ph.com.sunlife.wms.web.view;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.springframework.web.servlet.view.AbstractView;

import ph.com.sunlife.wms.util.WMSMimeUtil;

public class WMSDownloadFileView extends AbstractView {

	private static final String FILE_NAME_KEY = "fileName";
	
	private static final String FILE_DATA_KEY = "fileData";

	public WMSDownloadFileView() {
		setContentType(WMSMimeUtil.DOWNLOAD_CONTENT_TYPE);
	}

	@SuppressWarnings("rawtypes")
	@Override
	protected void renderMergedOutputModel(Map model,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		final String fileName = (String) model.get(FILE_NAME_KEY);
		final byte[] fileData = (byte[]) model.get(FILE_DATA_KEY);

		response.setHeader("Content-Disposition", "attachment; filename="
				+ fileName);
		IOUtils.write(fileData, response.getOutputStream());
	}

}
