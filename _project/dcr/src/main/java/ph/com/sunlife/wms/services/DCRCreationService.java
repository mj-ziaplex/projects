package ph.com.sunlife.wms.services;

import java.util.Date;
import java.util.List;

import ph.com.sunlife.wms.dao.domain.DCR;
import ph.com.sunlife.wms.dao.exceptions.WMSDaoException;
import ph.com.sunlife.wms.integration.exceptions.IntegrationException;
import ph.com.sunlife.wms.services.bo.DCRBO;
import ph.com.sunlife.wms.services.bo.DCRBalancingToolBO;
import ph.com.sunlife.wms.services.bo.DCRBalancingToolProductBO;
import ph.com.sunlife.wms.services.bo.DCRCashierBO;
import ph.com.sunlife.wms.services.bo.DCRIpacValueBO;
import ph.com.sunlife.wms.services.exception.ServiceException;

/**
 * The service object responsible for creation of {@link DCRBO} work items, the
 * corresponding work item for each Cashier and displaying the necessary
 * information on the balancing tool pages.
 * 
 * @author Zainal Limpao
 * 
 */
public interface DCRCreationService {

	/**
	 * Creates DCR Workitems for all defined Customer Centers given current
	 * date.
	 * 
	 * @throws ServiceException
	 */
	boolean createDCRWorkItemsNow() throws ServiceException;

	boolean createDCRWorkItems(Date date) throws ServiceException;

	/**
	 * Creates DCR Work Item for each valid Customer Center within SunLife
	 * Philippines.
	 * 
	 * @param date
	 * @param boolean
	 * @return List of {@link DCRBO}
	 * @throws ServiceException
	 * @throws IntegrationException
	 */
	List<DCRBO> initiateDCRWorkItems(Date date, boolean isManualBatchRun) throws ServiceException,
			IntegrationException;

	/**
	 * By default, everytime a Cashier logs in to WMS-DCR, it will load the the
	 * Cashier's inbox which contains Daily Collections Reports. This process
	 * will do the following sub-processes in sequential order:
	 * <ol>
	 * <li>Before loading the {@link DCRCashierBO} objects, the system has to
	 * check if this particular cashier has a valid collection for the given
	 * customer center from IPAC.</li>
	 * <li>Create a Non-Filenet Cashier Collection Work Item represented by
	 * {@link DCRCashierBO} and join this object with existing
	 * {@link DCRCashierBO} associated with the cashier.</li>
	 * <li>The joined list will then be returned to the Controller for display
	 * to the Cashier Home Page screen.</li>
	 * </ol>
	 * 
	 * @param ccId
	 *            - customer center code
	 * @param acf2id
	 *            - username of the Cashier
	 * @return List of {@link DCRCashierBO}
	 * @throws ServiceException
	 */
	List<DCRCashierBO> createAndLoadDcrWorkItems(String ccId, String acf2id)
			throws ServiceException;

	/**
	 * Extract {@link DCRBO} objects associated with the list of
	 * {@link DCRCashierBO}
	 * 
	 * @param dcrCashierList
	 * @return
	 */
	List<DCRBO> extractDCRFromDCRCashierList(List<DCRCashierBO> dcrCashierList)
			throws ServiceException;

	/**
	 * Loads all the {@link DCRBalancingToolBO} objects associated with the
	 * given {@link DCRCashierBO} object. This checks if it is available on the
	 * database, and creates the necessary records including the children
	 * records if they are not yet found in the database.
	 * 
	 * @param dcrCashierBO
	 * @return
	 * @throws ServiceException
	 */
	List<DCRBalancingToolBO> createAndDisplayDCRBalancingTools(
			DCRCashierBO dcrCashierBO) throws ServiceException;

	/**
	 * Given the id and the username of the cashier, this loads the necessary
	 * {@link DCRCashierBO} object.
	 * 
	 * @param dcrCashierId
	 * @param acf2id
	 * @return
	 * @throws ServiceException
	 */
	DCRCashierBO getDCRCashierBO(Long dcrCashierId, String acf2id)
			throws ServiceException;

	/**
	 * Builds a list of {@link DCRIpacValueBO} given {@link DCRCashierBO}.
	 * Essentially, the first parameter which is the list of
	 * {@link DCRIpacValueBO} will be checked first if it's empty before doing
	 * an equal IPAC query.
	 * 
	 * @param values
	 * @param dcrCashier
	 * @return
	 * @throws ServiceException
	 */
	List<DCRIpacValueBO> buildIpacValues(List<DCRIpacValueBO> values,
			DCRCashierBO dcrCashier) throws ServiceException;

	/**
	 * This traverses the list of {@link DCRIpacValueBO} and associate each one
	 * according to the given {@link DCRBalancingToolProductBO}. This also
	 * retrieves all associated {@link DCReversalBO} entries and
	 * {@link DCROtherService} for the COB Services.
	 * 
	 * @param values
	 * @param products
	 * @return
	 * @throws ServiceException
	 */
	List<DCRBalancingToolProductBO> buildBalancingToolProducts(
			String centerCode, Date processDate, String acf2id,
			List<DCRIpacValueBO> values,
			List<DCRBalancingToolProductBO> products) throws ServiceException;

	/**
	 * Confirms that the current {@link DCRBalancingToolBO} page has values
	 * consistent and balanced with the amoung of cash and check of the cashier.
	 * 
	 * @param drcBalancingToolBO
	 * @return
	 */
	boolean confirmDCRBalancingTool(DCRBalancingToolBO drcBalancingToolBO,
			boolean isDayOne) throws ServiceException;

	/**
	 * Saves worksite amount into the database.
	 * 
	 * @param products
	 * @return
	 * @throws ServiceException
	 */
	boolean saveWorkSiteAmount(List<DCRBalancingToolProductBO> products)
			throws ServiceException;

	/**
	 * Returns a list of @{link DCRBO} given the customer center id.
	 * 
	 * @param ccId
	 * @return
	 * @throws ServiceException
	 */
	List<DCRBO> getAllDcrWorkItems(String ccId) throws ServiceException;

	// TODO: remove exposure of DCR object in the interface.
	List<DCR> saveDCRList(List<DCR> dcrList, Date date, boolean isManualBatchRun) throws WMSDaoException,
			IntegrationException;

	DCRBO getDcrById(Long id) throws ServiceException;

}
