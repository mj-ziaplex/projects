package ph.com.sunlife.wms.dao.impl;

import java.util.List;

import ph.com.sunlife.wms.dao.AbstractWMSSqlMapClientDaoSupport;
import ph.com.sunlife.wms.dao.DCRMixedChequePayeeDao;
import ph.com.sunlife.wms.dao.domain.Bank;
import ph.com.sunlife.wms.dao.domain.DCRMixedChequePayee;
import ph.com.sunlife.wms.dao.exceptions.WMSDaoException;

public class DCRMixedChequePayeeDaoImpl extends
		AbstractWMSSqlMapClientDaoSupport<DCRMixedChequePayee> implements
		DCRMixedChequePayeeDao {

	private static final String NAMESPACE = "DCRMixedChequePayee";

	@SuppressWarnings("unchecked")
	@Override
	public List<DCRMixedChequePayee> getAllByDcrCashier(Long dcrCashierId)
			throws WMSDaoException {
		List<DCRMixedChequePayee> list = null;

		try {
			list = getSqlMapClientTemplate().queryForList(
					getNamespace() + ".getAllByDcrCashier", dcrCashierId);
		} catch (Exception ex) {
			throw new WMSDaoException(ex);
		}

		return list;
	}

	@Override
	public boolean deleteById(Long id) throws WMSDaoException {
		int rowsDeleted = 0;

		try {
			rowsDeleted = getSqlMapClientTemplate().delete(
					getNamespace() + ".deleteById", id);
		} catch (Exception ex) {
			throw new WMSDaoException(ex);
		}
		return (rowsDeleted != 0);
	}

	@Override
	public boolean updateMixedChequePayee(DCRMixedChequePayee entity)
			throws WMSDaoException {
		int rowsUpdated = 0;

		try {
			rowsUpdated = getSqlMapClientTemplate().delete(
					getNamespace() + ".updateMixedChequePayee", entity);
		} catch (Exception ex) {
			throw new WMSDaoException(ex);
		}
		
		return (rowsUpdated != 0);
	}	
	

	@SuppressWarnings("unchecked")
	@Override
	public List<Bank> getAllBanks() throws WMSDaoException {
		List<Bank> resultList = null;
		try{
			resultList = getSqlMapClientTemplate().queryForList(
					getNamespace() + ".getAllBanks");
		} catch(Exception e){
			throw new WMSDaoException(e);
		}
		return resultList;
	}

	@Override
	protected String getNamespace() {
		return NAMESPACE;
	}

}
