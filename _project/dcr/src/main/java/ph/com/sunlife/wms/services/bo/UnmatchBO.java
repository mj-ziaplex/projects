package ph.com.sunlife.wms.services.bo;

public class UnmatchBO {
	
	private String filenetId;
	
	private Long recordId;
	
	private String recordLocation;
	
	private String dcrDate;
	
	private String ccId;

	public String getFilenetId() {
		return filenetId;
	}

	public void setFilenetId(String filenetId) {
		this.filenetId = filenetId;
	}

	public Long getRecordId() {
		return recordId;
	}

	public void setRecordId(Long recordId) {
		this.recordId = recordId;
	}

	public String getRecordLocation() {
		return recordLocation;
	}

	public void setRecordLocation(String recordLocation) {
		this.recordLocation = recordLocation;
	}

	public String getDcrDate() {
		return dcrDate;
	}

	public void setDcrDate(String dcrDate) {
		this.dcrDate = dcrDate;
	}

	public String getCcId() {
		return ccId;
	}

	public void setCcId(String ccId) {
		this.ccId = ccId;
	}
}
