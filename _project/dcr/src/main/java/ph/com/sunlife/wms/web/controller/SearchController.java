package ph.com.sunlife.wms.web.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.web.servlet.ModelAndView;

import ph.com.sunlife.wms.security.Role;
import ph.com.sunlife.wms.services.DCRCreationService;
import ph.com.sunlife.wms.services.DCRSearchService;
import ph.com.sunlife.wms.services.bo.CustomerCenterBO;
import ph.com.sunlife.wms.services.bo.DCRBO;
import ph.com.sunlife.wms.services.bo.HubBO;
import ph.com.sunlife.wms.services.bo.UserSession;
import ph.com.sunlife.wms.services.exception.ServiceException;
import ph.com.sunlife.wms.web.controller.form.SearchPageForm;
import ph.com.sunlife.wms.web.exception.ApplicationException;

public class SearchController extends AbstractSecuredMultiActionController {

	private static final String SEARCHPAGE_VIEW = "searchViewPage";

	private static final String CUSTOMER_CENTER_KEY = "customerCenters";

	private static final String HUB_KEY = "hubs";

	private static final String ALL_KEY = "ALL";
	
	private static final String TITLE_KEY = "title";
	
	private static final String TITLE_VALUE = "Search DCR";

	private DCRCreationService dcrCreationService;

	private DCRSearchService dcrSearchService;

	private static final Logger LOGGER = Logger
			.getLogger(SearchController.class);

	private static final String DCR_KEY = "dcrObjects";

	public ModelAndView doShowHomePage(HttpServletRequest request,
			HttpServletResponse response, SearchPageForm form)
			throws ApplicationException {

		LOGGER.debug("Initializing home page...");

		ModelAndView modelAndView = new ModelAndView();
		List<CustomerCenterBO> customerCenters = new ArrayList<CustomerCenterBO>();
		List<HubBO> hubBO = new ArrayList<HubBO>();
		try {
			UserSession userSession = this.getUserSession(request);		
			hubBO = dcrSearchService.getCCHubs();
			if(userSession.getRoles().contains(Role.OTHER) || userSession.getRoles().contains(Role.PPA)){
				for (HubBO h : hubBO) {
					customerCenters.addAll(dcrSearchService.getCCByHub(h.getHubId()));
					Collections.sort(customerCenters);
				}
			} else{
				customerCenters = dcrSearchService.getCCByHubUserCC(userSession.getSiteCode());
			}

		} catch (ServiceException e) {
			LOGGER.error(e);
			throw new ApplicationException(e);
		}

		modelAndView.addObject(HUB_KEY, hubBO);
		modelAndView.addObject(CUSTOMER_CENTER_KEY, customerCenters);
		modelAndView.addObject(TITLE_KEY, TITLE_VALUE);
		modelAndView.setViewName(SEARCHPAGE_VIEW);
		return modelAndView;
	}

	@SuppressWarnings("unchecked")
	public ModelAndView doFindDCRWorkItems(HttpServletRequest request,
			HttpServletResponse response, SearchPageForm form)
			throws ApplicationException {

		ModelAndView modelAndView = this
				.doShowHomePage(request, response, form);
		List<DCRBO> dailyCollections = new ArrayList<DCRBO>();
		try {
			DCRBO paramBO = new DCRBO();
			List<String> ccIdSet = new ArrayList<String>();
			if (form.getCcId() != null
					&& form.getCcId().equalsIgnoreCase(ALL_KEY)) {
				List<CustomerCenterBO> customerCenters = (List<CustomerCenterBO>) modelAndView
						.getModel().get(CUSTOMER_CENTER_KEY);
				if (CollectionUtils.isNotEmpty(customerCenters)) {
					for (CustomerCenterBO ccBO : customerCenters) {
						ccIdSet.add(ccBO.getCcId());
					}
				}
			} else {
				ccIdSet.add(form.getCcId());
			}
			paramBO.setCcIdSet(ccIdSet);
			paramBO.setHubId(form.getHubId());
			paramBO.setDcrEndDate(form.getDcrEndDate());
			paramBO.setDcrStartDate(form.getDcrStartDate());
			
			if (form.getSearchByCC()) {
				dailyCollections = dcrSearchService.getDCRByCCAndDate(paramBO);
			} else if (form.getSearchByHub()) {
				dailyCollections = dcrSearchService.getDCRByHubAndDate(paramBO);
			}
		} catch (ServiceException e) {
			LOGGER.error(e);
			throw new ApplicationException(e);
		}

		// Added for Audit finding for CCQA notes
		UserSession userSession = this.getUserSession(request);
		List<Role> roles = userSession.getRoles();
		boolean isPpa = roles.contains(Role.PPA);
		// Added for Audit finding for CCQA notes
		modelAndView.addObject("isPpa", isPpa);
                
		modelAndView.addObject(DCR_KEY, dailyCollections);
		modelAndView.addObject(TITLE_KEY, TITLE_VALUE);
		return modelAndView;
	}

    @Override
    protected Role[] allowedRoles() {
        // Change according to Healthcheck
        // Problem was due to changes in AbstractSecuredMultiActionController
        // Change was use parent getter method instead of inherited values
        return super.getALL_ROLES();
    }

	public DCRCreationService getDcrCreationService() {
		return dcrCreationService;
	}

	public void setDcrCreationService(DCRCreationService dcrCreationService) {
		this.dcrCreationService = dcrCreationService;
	}

	public DCRSearchService getDcrSearchService() {
		return dcrSearchService;
	}

	public void setDcrSearchService(DCRSearchService dcrSearchService) {
		this.dcrSearchService = dcrSearchService;
	}

}
