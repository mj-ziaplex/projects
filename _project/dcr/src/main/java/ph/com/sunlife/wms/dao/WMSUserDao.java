package ph.com.sunlife.wms.dao;

import java.util.List;

import ph.com.sunlife.wms.dao.domain.WMSUser;
import ph.com.sunlife.wms.dao.exceptions.WMSDaoException;

public interface WMSUserDao {

	boolean createWMSUser(WMSUser user) throws WMSDaoException;

	List<WMSUser> getAllWMSUser() throws WMSDaoException;

	List<WMSUser> getWMSUserById(String userId) throws WMSDaoException;

	boolean updateWMSUser(WMSUser user) throws WMSDaoException;

	boolean deleteWMSUser(String userId) throws WMSDaoException;
	
//	boolean updateWMSUserLogin(WMSUser user) throws WMSDaoException;
//
//	boolean updateWMSUserLogout(WMSUser user) throws WMSDaoException;
	
}
