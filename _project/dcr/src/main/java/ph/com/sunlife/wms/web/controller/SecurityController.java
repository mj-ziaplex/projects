package ph.com.sunlife.wms.web.controller;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.View;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;
import org.springframework.web.servlet.view.RedirectView;

import ph.com.sunlife.wms.dao.domain.WMSUser;
import ph.com.sunlife.wms.security.Role;
import ph.com.sunlife.wms.services.DCRLogInService;
import ph.com.sunlife.wms.services.WMSUserService;
import ph.com.sunlife.wms.services.bo.UserSession;
import ph.com.sunlife.wms.services.exception.IncorrectSiteCodeException;
import ph.com.sunlife.wms.services.exception.InvalidLogInCredentialsException;
import ph.com.sunlife.wms.services.exception.ServiceException;
import ph.com.sunlife.wms.util.WMSConstants;
import ph.com.sunlife.wms.web.controller.form.LoginForm;
import ph.com.sunlife.wms.web.exception.ApplicationException;

/**
 * The Controller responsible for authenticating and managing roles of users
 * into WMS-Web
 * 
 * @author Zainal Limpao
 * 
 */
public class SecurityController extends AbstractWMSMultiActionController {

	protected static final String LOGIN_VIEW = "loginView";

	protected static final String ADMIN_LOGIN_VIEW = "adminLoginView";

	protected static final String FAILED_ADMIN_LOGIN = "failedAdminLogin";

	protected static final String IS_ADMIN = "isAdmin";

	private static final Logger LOGGER = Logger.getLogger(SecurityController.class);

	private DCRLogInService dcrLogInService;

	private WMSUserService wmsUserService;

	@Override
	public ModelAndView handleBindingError(
			final HttpServletRequest request,
			final HttpServletResponse response,
			final ServletRequestBindingException cause) throws Exception {
		ModelAndView modelAndView = super.handleBindingError(request, response, cause);
		this.pushCCNamesToModel(modelAndView);
		return modelAndView;
	}

	/**
	 * The default {@link MultiActionController} invoked named method everytime
	 * a user accesses the system before logging in.
	 * 
	 * @param request
	 * @param response
	 * @return {@link ModelAndView} object
	 */
	public ModelAndView doShowLoginPage(
			final HttpServletRequest request,
			final HttpServletResponse response) throws Exception {
		LOGGER.info("Accessing login screen of WMS-DCR...");

		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName(LOGIN_VIEW);
		this.pushCCNamesToModel(modelAndView);

		HttpSession session = request.getSession(false);
		if (session != null) {
			UserSession user = (UserSession) session.getAttribute(WMSConstants.USER_SESSION);

			if (user == null) {
				modelAndView.addObject("errorMsg", "&nbsp;");
				return modelAndView;
			} else {
				View view = new RedirectView("/home/index.html", true);
				modelAndView.setView(view);
			}
		}

		return modelAndView;
	}

	private void pushCCNamesToModel(final ModelAndView modelAndView) throws Exception {
		List<String> ccNames = null;

		try {
			ccNames = dcrLogInService.displayCCNames();
		} catch (ServiceException e) {
			LOGGER.error("Error in pushCCNamesToMode: " + e);
			throw new ApplicationException(e);
		}

		modelAndView.addObject("ccNames", ccNames);
	}

	/**
	 * This is invoked whenever user wishes to log out of the system. The
	 * current {@link HttpSession} is invalidated releasing all associated
	 * objects including {@link UserSession}.
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	public ModelAndView doLogOut(
			final HttpServletRequest request,
			final HttpServletResponse response) throws Exception {
		
		ModelAndView modelAndView = new ModelAndView();
		if (new Boolean(String.valueOf(request.getSession().getAttribute(IS_ADMIN))).booleanValue()) {
			modelAndView.setViewName(ADMIN_LOGIN_VIEW);
		} else {
			modelAndView.setViewName(LOGIN_VIEW);
		}

		try {
			UserSession userSession = (UserSession) request.getSession().getAttribute(WMSConstants.USER_SESSION);
			if (null != userSession.getUserId()) {
				WMSUser wu = new WMSUser();
				wu.setWmsuId(userSession.getUserId());
				wu.setWmsdcrActive(false);
				wu.setWmsdcrLastlogout(new Date());
				//wmsUserService.updateWMSUserLogStatus(wu, false); No Docu; temp disable as no column in UAT and PR
			}
		} catch (Exception e) {
			LOGGER.error("ERROR in doLogOut: " + e);
		}

		HttpSession session = request.getSession(false);
		if (session != null) {
			session.invalidate();
			modelAndView.addObject("errorMsg", getMessage("action.logout.successful"));
		}
		this.pushCCNamesToModel(modelAndView);
		return modelAndView;
	}

	public ModelAndView handleIncorrectSiteCodeException(
			final HttpServletRequest request,
			final HttpServletResponse response,
			final IncorrectSiteCodeException ex) throws Exception {
		
		ModelAndView modelAndView = new ModelAndView();
		if ("Y".equals(String.valueOf(request.getSession().getAttribute(FAILED_ADMIN_LOGIN)))) {
			modelAndView.setViewName(ADMIN_LOGIN_VIEW);
		} else {
			modelAndView.setViewName(LOGIN_VIEW);
		}
		modelAndView.addObject("errorMsg", ex.getMessage());
		this.pushCCNamesToModel(modelAndView);
		return modelAndView;
	}

	public ModelAndView handleInvalidLogInCredentialsException(
			final HttpServletRequest request,
			final HttpServletResponse response,
			final InvalidLogInCredentialsException ex) throws Exception {

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug(ex.getUserId() + " has logged into WMS-DCR at " + new Date());
		}
		ModelAndView modelAndView = new ModelAndView();
		if ("Y".equals(String.valueOf(request.getSession().getAttribute(FAILED_ADMIN_LOGIN)))) {
			modelAndView.setViewName(ADMIN_LOGIN_VIEW);
		} else {
			modelAndView.setViewName(LOGIN_VIEW);
		}
		modelAndView.addObject("errorMsg", ex.getMessage());
		this.pushCCNamesToModel(modelAndView);
		return modelAndView;
	}

	/**
	 * The method used when user attempts to sign into the system using his
	 * crendentials via the {@link LoginForm}.
	 * 
	 * @param request
	 * @param response
	 * @param form
	 * @return {@link ModelAndView} object
	 */
	public ModelAndView doLogIn(
			final HttpServletRequest request,
			final HttpServletResponse response,
			final LoginForm form) throws Exception {

		String userId = form.getUserId();
		String password = form.getPassword();
		String siteCode = form.getSiteCode();
		String selectedRole = form.getSelectedRole();
		String adminUserId = form.getAdminUserId();

		ModelAndView modelAndView = new ModelAndView();

		UserSession userSession = null;
		try {

			if (null != adminUserId) {
				userSession = 
						dcrLogInService.logInUser(adminUserId, userId, password, siteCode, selectedRole);
			} else {
				userSession = 
						dcrLogInService.logInUser(userId, password, siteCode, selectedRole);
			}

			if (null != userSession) {
				WMSUser wu = new WMSUser();
				wu.setWmsuId(userId);
				wu.setWmsdcrActive(true);
				wu.setWmsdcrLastLogin(new Date());
				//wmsUserService.updateWMSUserLogStatus(wu, true); No Docu; temp disable as no column in UAT and PR
			}

			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug(userId + " has logged into WMS-DCR at " + new Date());
			}

			View view = new RedirectView("/home/index.html", true);

			modelAndView.setView(view);

			HttpSession session = request.getSession(true);
			if (null != adminUserId) {
				session.setAttribute(FAILED_ADMIN_LOGIN, "Y");
			}
			session.setAttribute(WMSConstants.USER_SESSION, userSession);
			session.setAttribute(IS_ADMIN, userSession.getRoles().contains(Role.ADMIN));
		} catch (ServiceException e) {
			LOGGER.error("Error in doLogIn problem in logging in: ", e);
			throw new ApplicationException(e);
		}

		return modelAndView;
	}

	/**
	 * Injection setter for {@link DCRLogInService}.
	 * 
	 * @param dcrLogInService
	 */
	public void setDcrLogInService(DCRLogInService dcrLogInService) {
		this.dcrLogInService = dcrLogInService;
	}

	public void setWmsUserService(WMSUserService wmsUserService) {
		this.wmsUserService = wmsUserService;
	}

	public ModelAndView doShowAdminLoginPage(
			final HttpServletRequest request,
			final HttpServletResponse response) throws Exception {
		LOGGER.info("Accessing admin login screen of WMS-DCR...");

		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName(ADMIN_LOGIN_VIEW);
		this.pushCCNamesToModel(modelAndView);

		HttpSession session = request.getSession(false);
		if (session != null) {
			UserSession user = (UserSession) session.getAttribute(WMSConstants.USER_SESSION);

			if (user == null) {
				modelAndView.addObject("errorMsg", "&nbsp;");
				return modelAndView;
			} else {
				View view = new RedirectView("/home/index.html", true);
				modelAndView.setView(view);
			}
		}

		return modelAndView;
	}

}
