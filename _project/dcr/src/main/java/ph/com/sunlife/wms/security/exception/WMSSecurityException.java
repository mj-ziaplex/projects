package ph.com.sunlife.wms.security.exception;

public class WMSSecurityException extends Exception {

    private static final long serialVersionUID = 1L;

    public WMSSecurityException(String msg) {
        super(msg);
    }

    public WMSSecurityException(String msg, Throwable throwable) {
        super(msg, throwable);
    }

    public WMSSecurityException(Throwable throwable) {
        super(throwable);
    }
}
