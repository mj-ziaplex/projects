package ph.com.sunlife.wms.dao.domain;

import java.util.Date;

public class DCRCachedProperty {

	private Long id;
	
	private String propKey;
	
	private String propValue;
	
	private String dcrCreUser;
	
	private Date dcrCreDate;
	
	private String dcrUpdUser;
	
	private Date dcrUpdDate;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPropKey() {
		return propKey;
	}

	public void setPropKey(String propKey) {
		this.propKey = propKey;
	}

	public String getPropValue() {
		return propValue;
	}

	public void setPropValue(String propValue) {
		this.propValue = propValue;
	}

	public String getDcrCreUser() {
		return dcrCreUser;
	}

	public void setDcrCreUser(String dcrCreUser) {
		this.dcrCreUser = dcrCreUser;
	}

	public Date getDcrCreDate() {
		return dcrCreDate;
	}

	public void setDcrCreDate(Date dcrCreDate) {
		this.dcrCreDate = dcrCreDate;
	}

	public String getDcrUpdUser() {
		return dcrUpdUser;
	}

	public void setDcrUpdUser(String dcrUpdUser) {
		this.dcrUpdUser = dcrUpdUser;
	}

	public Date getDcrUpdDate() {
		return dcrUpdDate;
	}

	public void setDcrUpdDate(Date dcrUpdDate) {
		this.dcrUpdDate = dcrUpdDate;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((propKey == null) ? 0 : propKey.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DCRCachedProperty other = (DCRCachedProperty) obj;
		if (propKey == null) {
			if (other.propKey != null)
				return false;
		} else if (!propKey.equals(other.propKey))
			return false;
		return true;
	}

}
