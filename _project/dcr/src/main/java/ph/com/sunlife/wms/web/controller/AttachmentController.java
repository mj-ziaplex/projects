package ph.com.sunlife.wms.web.controller;

import static ph.com.sunlife.wms.util.WMSConstants.STATUS_AWAITING_REQUIREMENTS;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;

import org.springframework.web.servlet.ModelAndView;

import ph.com.sunlife.wms.security.Role;
import ph.com.sunlife.wms.services.AttachmentService;
import ph.com.sunlife.wms.services.ConsolidatedDataService;
import ph.com.sunlife.wms.services.bo.DCRBO;
import ph.com.sunlife.wms.services.bo.UserSession;
import ph.com.sunlife.wms.services.exception.InvalidFileTypeException;
import ph.com.sunlife.wms.services.exception.ServiceException;
import ph.com.sunlife.wms.web.controller.form.AttachmentSPForm;
import ph.com.sunlife.wms.web.exception.ApplicationException;

public class AttachmentController extends AbstractSecuredMultiActionController {
    
    private static final Logger LOGGER = Logger.getLogger(AttachmentController.class);

	private static final String VIEW_NAME = "attachScreenView";

	private static final String PAGE_TITLE = "Attach Files";

	private static final String RELOAD_SP_KEY = "reloadSP";
	
	private static final String RELOAD_MAIN_WINDOW_KEY = "reloadMainWindow";

	private static final String TITLE_KEY = "title";

	private ConsolidatedDataService consolidatedDataService;

	private AttachmentService attachmentService;

	public ModelAndView showAttachmentsPage(HttpServletRequest request,
			HttpServletResponse response, AttachmentSPForm form) {

		ModelAndView modelAndView = new ModelAndView();

		modelAndView.setViewName(VIEW_NAME);
		modelAndView.addObject("dcrId", form.getDcrId());
		modelAndView.addObject(RELOAD_SP_KEY, form.isReloadSP());
		modelAndView.addObject(TITLE_KEY, "Attach File");
		modelAndView.addObject("errorMessage", "&nbsp;");

		return modelAndView;
	}

    public ModelAndView attachFile(
            HttpServletRequest request,
            HttpServletResponse response,
            AttachmentSPForm form) throws ApplicationException {
        
        try {
            UserSession userSession = getUserSession(request);
            String uploaderId = userSession.getUserId();
            Role role = userSession.getRoles().get(0);

            Long dcrId = form.getDcrId();
            DCRBO dcr = consolidatedDataService.getDcr(dcrId);
            String status = dcr.getStatus();

            attachmentService.uploadNonCashierAttachment(dcrId, uploaderId, form.getDescription(), role, form.getFile());
            ModelAndView mv = showAttachmentsPage(request, response, form);
            if (STATUS_AWAITING_REQUIREMENTS.equals(status)) {
                consolidatedDataService.reqtsSubmittedPPA(uploaderId, dcrId);
                mv.addObject(RELOAD_MAIN_WINDOW_KEY, true);
                // TODO: submit to Form an indicator to close the SP
            }

            mv.addObject(RELOAD_SP_KEY, true);
            return mv;
        } catch (ServiceException e) {
            // Change according to Healthcheck
            // *Problem was identified as inconsistent logging
            // *Change was added logging with object exception
            LOGGER.error("Error in attaching file: ", e);
            throw new ApplicationException(e);
        } catch (InvalidFileTypeException e) {
            // Change according to Healthcheck
            // *Problem was identified as inconsistent logging
            // *Change was added logging with object exception
            LOGGER.error("Error in attaching file: ", e);
            ModelAndView mv = showAttachmentsPage(request, response, form);
            mv.addObject("errorMessage", e.getMessage());
            return mv;
        }
    }

	public void setAttachmentService(AttachmentService attachmentService) {
		this.attachmentService = attachmentService;
	}

	public void setConsolidatedDataService(
			ConsolidatedDataService consolidatedDataService) {
		this.consolidatedDataService = consolidatedDataService;
	}

	@Override
	protected Role[] allowedRoles() {
		return new Role[] { Role.CASHIER, Role.MANAGER, Role.PPA };
	}

}
