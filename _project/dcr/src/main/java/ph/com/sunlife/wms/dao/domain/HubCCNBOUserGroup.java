package ph.com.sunlife.wms.dao.domain;

import java.io.Serializable;
import java.util.Date;

public class HubCCNBOUserGroup implements Serializable {

	private static final long serialVersionUID = -7299593852170456844L;
	
	private String hccnugHUBID;
	private String hccnugCCID;
	private String hccnugNBOID;
	private String hccnugWMSUID;
	private String hccnugGRPID;
	private String hccnugReceiveWork;
	private String hccnugActive;
	private String hccnugCREUser;
	private Date hccnugCREDate;
	private String hccnugUPDUser;
	private Date hccnugUPDDate;
	private String hccnugCompanyCode;
	private String hccnugReceiveWorkGF;
	
	public String getHccnugHUBID() {
		return hccnugHUBID;
	}
	public void setHccnugHUBID(String hccnugHUBID) {
		this.hccnugHUBID = hccnugHUBID;
	}
	public String getHccnugCCID() {
		return hccnugCCID;
	}
	public void setHccnugCCID(String hccnugCCID) {
		this.hccnugCCID = hccnugCCID;
	}
	public String getHccnugNBOID() {
		return hccnugNBOID;
	}
	public void setHccnugNBOID(String hccnugNBOID) {
		this.hccnugNBOID = hccnugNBOID;
	}
	public String getHccnugWMSUID() {
		return hccnugWMSUID;
	}
	public void setHccnugWMSUID(String hccnugWMSUID) {
		this.hccnugWMSUID = hccnugWMSUID;
	}
	public String getHccnugGRPID() {
		return hccnugGRPID;
	}
	public void setHccnugGRPID(String hccnugGRPID) {
		this.hccnugGRPID = hccnugGRPID;
	}
	public String getHccnugReceiveWork() {
		return hccnugReceiveWork;
	}
	public void setHccnugReceiveWork(String hccnugReceiveWork) {
		this.hccnugReceiveWork = hccnugReceiveWork;
	}
	public String getHccnugActive() {
		return hccnugActive;
	}
	public void setHccnugActive(String hccnugActive) {
		this.hccnugActive = hccnugActive;
	}
	public String getHccnugCREUser() {
		return hccnugCREUser;
	}
	public void setHccnugCREUser(String hccnugCREUser) {
		this.hccnugCREUser = hccnugCREUser;
	}
	public Date getHccnugCREDate() {
		return hccnugCREDate;
	}
	public void setHccnugCREDate(Date hccnugCREDate) {
		this.hccnugCREDate = hccnugCREDate;
	}
	public String getHccnugUPDUser() {
		return hccnugUPDUser;
	}
	public void setHccnugUPDUser(String hccnugUPDUser) {
		this.hccnugUPDUser = hccnugUPDUser;
	}
	public Date getHccnugUPDDate() {
		return hccnugUPDDate;
	}
	public void setHccnugUPDDate(Date hccnugUPDDate) {
		this.hccnugUPDDate = hccnugUPDDate;
	}
	public String getHccnugCompanyCode() {
		return hccnugCompanyCode;
	}
	public void setHccnugCompanyCode(String hccnugCompanyCode) {
		this.hccnugCompanyCode = hccnugCompanyCode;
	}
	public String getHccnugReceiveWorkGF() {
		return hccnugReceiveWorkGF;
	}
	public void setHccnugReceiveWorkGF(String hccnugReceiveWorkGF) {
		this.hccnugReceiveWorkGF = hccnugReceiveWorkGF;
	}
	@Override
	public String toString() {
		return "HubCCNBOUserGroup [hccnugHUBID=" + hccnugHUBID
				+ ", hccnugCCID=" + hccnugCCID + ", hccnugNBOID=" + hccnugNBOID
				+ ", hccnugWMSUID=" + hccnugWMSUID + ", hccnugGRPID="
				+ hccnugGRPID + ", hccnugReceiveWork=" + hccnugReceiveWork
				+ ", hccnugActive=" + hccnugActive + ", hccnugCREUser="
				+ hccnugCREUser + ", hccnugCREDate=" + hccnugCREDate
				+ ", hccnugUPDUser=" + hccnugUPDUser + ", hccnugUPDDate="
				+ hccnugUPDDate + ", hccnugCompanyCode=" + hccnugCompanyCode
				+ ", hccnugReceiveWorkGF=" + hccnugReceiveWorkGF + "]";
	}
}
