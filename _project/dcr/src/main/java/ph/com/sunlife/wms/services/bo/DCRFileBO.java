package ph.com.sunlife.wms.services.bo;

import java.io.Serializable;

public class DCRFileBO implements Serializable {

	private static final long serialVersionUID = 1677588322600033942L;

	private String fileName;

	private String fileId;

	private byte[] fileData;

	private String contentType;

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFileId() {
		return fileId;
	}

	public void setFileId(String fileId) {
		this.fileId = fileId;
	}

	public byte[] getFileData() {
		return fileData;
	}

	public void setFileData(byte[] fileData) {
		this.fileData = fileData;
	}

}