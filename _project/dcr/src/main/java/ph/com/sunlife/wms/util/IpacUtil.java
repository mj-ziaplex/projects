package ph.com.sunlife.wms.util;

/**
 * This is a simple translator of various IPAC terms to WMS terms and vice
 * versa.
 *
 * @author Crisseljess Mendoza
 * 
 * ** Date: Feb 2016
 * ** Dev: Cshells Sayan
 * ** Desc: Added new product ADA(MF22) to be able to filter and display in DCR 
 * 
 * ** Date: May 2017
 * ** Dev: Cshells Sayan
 * ** Desc: Added values for SR-WF-16-00016 - SLAMC - New USD Fund (WMS-DCR)
 */
public final class IpacUtil {

    final static String DCR_SLOCPI_KEY = "SLOCPI";
    final static String DCR_SLFPI_KEY = "SLFPI";
    final static String DCR_SLGFI_KEY = "SLGFI";
    final static String DCR_MF1_KEY = "SLAMCI_MF1";
    final static String DCR_MF2_KEY = "SLAMCI_MF2";
    final static String DCR_MF3_KEY = "SLAMCI_MF3";
    final static String DCR_MF4_KEY = "SLAMCI_MF4";
    final static String DCR_MF5_KEY = "SLAMCI_MF5";
    final static String DCR_MF6_KEY = "SLAMCI_MF6";
    final static String DCR_MF7_KEY = "SLAMCI_MF7";
    final static String DCR_MF8_KEY = "SLAMCI_MF8";
    final static String DCR_MF9_KEY = "SLAMCI_MF9";
    final static String DCR_MF20_KEY = "SLAMCI_MF20";
    final static String DCR_MF21_KEY = "SLAMCI_MF21";
    final static String DCR_MF10_KEY = "SLAMCI_MF10";
    final static String DCR_MF11_KEY = "SLAMCI_MF11";
    final static String DCR_PCO_KEY = "SLAMCI_PCO"; // Added for PCO
    final static String DCR_MF22_KEY = "SLAMCI_MF22"; // Added for ADA
    final static String DCR_MF12_KEY = "SLAMCI_MF12"; // Added for SR-WF-16-00016
    final static String IPAC_SLOCP_KEY = "SLOCP";
    final static String IPAC_SLAMC_KEY = "SLAMC";
    final static String IPAC_SLFPI_KEY = "SLFPI";
    final static String IPAC_SLGFI_KEY = "SLGFI";
    final static String IPAC_MF1_KEY = "SLAMC1";
    final static String IPAC_MF2_KEY = "SLAMC2";
    final static String IPAC_MF3_KEY = "SLAMC3";
    final static String IPAC_MF4_KEY = "SLAMC4";
    final static String IPAC_MF5_KEY = "SLAMC5";
    final static String IPAC_MF6_KEY = "SLAMC6";
    final static String IPAC_MF7_KEY = "SLAMC7";
    final static String IPAC_MF8_KEY = "SLAMC8";
    final static String IPAC_MF9_KEY = "SLAMC9";
    final static String IPAC_MF20_KEY = "SLAMC20";
    final static String IPAC_MF21_KEY = "SLAMC21";
    final static String IPAC_MF10_KEY = "SLAMC10";
    final static String IPAC_MF11_KEY = "SLAMC11";
    final static String IPAC_PCO_KEY = "SLAMCPCO"; // Added for PCO
    final static String IPAC_MF22_KEY = "SLAMC22"; // Added for ADA
    final static String IPAC_MF12_KEY = "SLAMC12"; // Added for SR-WF-16-00016
    final static String IPAC_FULL_SLAMC1_KEY = "Sun Life of Canada (Philippines) Prosperity Bond Fund, Inc.";
    final static String IPAC_FULL_SLAMC2_KEY = "Sun Life of Canada (Philippines) Prosperity Balanced Fund, Inc.";
    final static String IPAC_FULL_SLAMC3_KEY = "Sun Life of Canada (Philippines) Prosperity Equity Fund, Inc.";
    final static String IPAC_FULL_SLAMC4_KEY = "Sun Life of Canada (Philippines) Prosperity Dollar Advantage Fund, Inc.";
    final static String IPAC_FULL_SLAMC5_KEY = "Sun Life of Canada (Philippines) Prosperity Money Market Fund, Inc.";
    final static String IPAC_FULL_SLAMC6_KEY = "Sun Life of Canada (Philippines) Prosperity Dollar Abundance Fund, Inc.";
    final static String IPAC_FULL_SLAMC7_KEY = "Sun Life of Canada (Philippines) Prosperity GS Fund, Inc.";
    //final static String IPAC_FULL_SLAMC8_KEY = "Sun Life of Canada (Philippines) Prosperity GS Fund, Inc.";
    //final static String IPAC_FULL_SLAMC9_KEY = "Sun Life of Canada (Philippines) Prosperity GS Fund, Inc.";
    final static String IPAC_FULL_SLAMC20_KEY = "Sun Life of Canada (Philippines) Group Fund Peso";
    final static String IPAC_FULL_SLAMC21_KEY = "Sun Life of Canada (Philippines) Group Fund Dollar";
    final static String IPAC_FULL_SLFPI_KEY = "Sun Life Financial Plans, Inc.";
    final static String IPAC_FULL_SLOCP_KEY = "Sun Life of Canada (Philippines), Inc.";
    final static String IPAC_FULL_SLGFI_KEY = "Sun Life Grepa Financial, Inc.";
    final static String IPAC_FULL_SLAMCI_KEY = "Sun Life Asset Management Company, Inc.";
    final static String DCR_SLAMCIP = "SLAMCIP";
    final static String DCR_SLAMCID = "SLAMCID";
    final static String DCR_SLOCPI = "SLOCPI";
    final static String DCR_SLGFI = "SLGFI";
    final static String DCR_SLFPI = "SLFPI";
    final static String DCR_OTHER = "OTHER";
    // Change according to Healthcheck
    // Problem was identified as security risk; and public array
    // Change was set as private and generated getter method to be able to fetch content
    private static final String[] SLAMCI_PHP_COM_CODES = {"SLAMC1", "SLAMC2", "SLAMC3", "SLAMC5", "SLAMC7", "SLAMC8", "SLAMC9", "SLAMC20", "SLAMCPCO", "SLAMC22"};
    private static final String[] SLAMCI_USD_COM_CODES = {"SLAMC4", "SLAMC6", "SLAMC21", "SLAMC10", "SLAMC11", "SLAMC12"};
    private static final String[] SLOCPI_COM_CODES = {IPAC_SLOCP_KEY};

    /**
     * Change according to Healthcheck
     * *Problem was identified as security risk; and public array
     * *Change was set as private and generated getter method to be able to fetch content
     * 
     * @return the SLAMCI_PHP_COM_CODES
     */
    public static String[] getSLAMCI_PHP_COM_CODES() {
        return SLAMCI_PHP_COM_CODES;
    }

    /**
     * Change according to Healthcheck
     * *Problem was identified as security risk; and public array
     * *Change was set as private and generated getter method to be able to fetch content
     * 
     * @return the SLAMCI_USD_COM_CODES
     */
    public static String[] getSLAMCI_USD_COM_CODES() {
        return SLAMCI_USD_COM_CODES;
    }

    /**
     * Change according to Healthcheck
     * *Problem was identified as security risk; and public array
     * *Change was set as private and generated getter method to be able to fetch content
     * 
     * @return the SLOCPI_COM_CODES
     */
    public static String[] getSLOCPI_COM_CODES() {
        return SLOCPI_COM_CODES;
    }

    private IpacUtil() {
    }

    public static String convertToIpacCode(String dcrCode) {

        String convertedCode = null;
        if (DCR_SLOCPI_KEY.equalsIgnoreCase(dcrCode)) {
            convertedCode = IPAC_SLOCP_KEY;
        } else if (DCR_SLFPI_KEY.equalsIgnoreCase(dcrCode)) {
            convertedCode = IPAC_SLFPI_KEY;
        } else if (DCR_SLGFI_KEY.equalsIgnoreCase(dcrCode)) {
            convertedCode = IPAC_SLGFI_KEY;
        } else if (DCR_MF1_KEY.equalsIgnoreCase(dcrCode)) {
            convertedCode = IPAC_MF1_KEY;
        } else if (DCR_MF2_KEY.equalsIgnoreCase(dcrCode)) {
            convertedCode = IPAC_MF2_KEY;
        } else if (DCR_MF3_KEY.equalsIgnoreCase(dcrCode)) {
            convertedCode = IPAC_MF3_KEY;
        } else if (DCR_MF4_KEY.equalsIgnoreCase(dcrCode)) {
            convertedCode = IPAC_MF4_KEY;
        } else if (DCR_MF5_KEY.equalsIgnoreCase(dcrCode)) {
            convertedCode = IPAC_MF5_KEY;
        } else if (DCR_MF6_KEY.equalsIgnoreCase(dcrCode)) {
            convertedCode = IPAC_MF6_KEY;
        } else if (DCR_MF7_KEY.equalsIgnoreCase(dcrCode)) {
            convertedCode = IPAC_MF7_KEY;
        } else if (DCR_MF8_KEY.equalsIgnoreCase(dcrCode)) {
            convertedCode = IPAC_MF8_KEY;
        } else if (DCR_MF9_KEY.equalsIgnoreCase(dcrCode)) {
            convertedCode = IPAC_MF9_KEY;
        } else if (DCR_MF20_KEY.equalsIgnoreCase(dcrCode)) {
            convertedCode = IPAC_MF20_KEY;
        } else if (DCR_MF21_KEY.equalsIgnoreCase(dcrCode)) {
            convertedCode = IPAC_MF21_KEY;
        } else if (DCR_SLAMCID.equalsIgnoreCase(dcrCode)) {
            convertedCode = IPAC_SLAMC_KEY;
        } else if (DCR_SLAMCIP.equalsIgnoreCase(dcrCode)) {
            convertedCode = IPAC_SLAMC_KEY;
        } else if (DCR_MF10_KEY.equalsIgnoreCase(dcrCode)) {
            convertedCode = IPAC_MF10_KEY;
        } else if (DCR_MF11_KEY.equalsIgnoreCase(dcrCode)) {
            convertedCode = IPAC_MF11_KEY;
        } else if (DCR_PCO_KEY.equalsIgnoreCase(dcrCode)) { // Added for PCO
            convertedCode = IPAC_PCO_KEY;
        } else if (DCR_MF22_KEY.equalsIgnoreCase(dcrCode)) { // Added for ADA
            convertedCode = IPAC_MF22_KEY;
        } else if (DCR_MF12_KEY.equalsIgnoreCase(dcrCode)) { // Added for SR-WF-16-00016
            convertedCode = IPAC_MF12_KEY;
        } else {
            // Change according to Healthcheck
            // *Problem method is being used by IpacDao,
            //  but erros due to null on OTHER company
            // *Change add a default value of empty string;
            convertedCode = "";
        }

        return convertedCode;
    }

    public static String convertToDCRCode(String ipacCode) {

        String convertedCode = null;

        if (IPAC_SLOCP_KEY.equalsIgnoreCase(ipacCode)) {
            convertedCode = DCR_SLOCPI_KEY;
        } else if (IPAC_MF1_KEY.equalsIgnoreCase(ipacCode)) {
            convertedCode = DCR_MF1_KEY;
        } else if (IPAC_MF2_KEY.equalsIgnoreCase(ipacCode)) {
            convertedCode = DCR_MF2_KEY;
        } else if (IPAC_MF3_KEY.equalsIgnoreCase(ipacCode)) {
            convertedCode = DCR_MF3_KEY;
        } else if (IPAC_MF4_KEY.equalsIgnoreCase(ipacCode)) {
            convertedCode = DCR_MF4_KEY;
        } else if (IPAC_MF5_KEY.equalsIgnoreCase(ipacCode)) {
            convertedCode = DCR_MF5_KEY;
        } else if (IPAC_MF6_KEY.equalsIgnoreCase(ipacCode)) {
            convertedCode = DCR_MF6_KEY;
        } else if (IPAC_MF7_KEY.equalsIgnoreCase(ipacCode)) {
            convertedCode = DCR_MF7_KEY;
        } else if (IPAC_MF8_KEY.equalsIgnoreCase(ipacCode)) {
            convertedCode = DCR_MF8_KEY;
        } else if (IPAC_MF9_KEY.equalsIgnoreCase(ipacCode)) {
            convertedCode = DCR_MF9_KEY;
        } else if (IPAC_MF20_KEY.equalsIgnoreCase(ipacCode)) {
            convertedCode = DCR_MF20_KEY;
        } else if (IPAC_MF21_KEY.equalsIgnoreCase(ipacCode)) {
            convertedCode = DCR_MF21_KEY;
        } else if (IPAC_MF10_KEY.equalsIgnoreCase(ipacCode)) {
            convertedCode = DCR_MF10_KEY;
        } else if (IPAC_MF11_KEY.equalsIgnoreCase(ipacCode)) {
            convertedCode = DCR_MF11_KEY;
        } else if (IPAC_PCO_KEY.equalsIgnoreCase(ipacCode)) { // Added for PCO
            convertedCode = DCR_PCO_KEY;
        } else if (IPAC_MF22_KEY.equalsIgnoreCase(ipacCode)) { // Added for ADA
            convertedCode = DCR_MF22_KEY;
        } else if (IPAC_MF12_KEY.equalsIgnoreCase(ipacCode)) { // Added for SR-WF-16-00016
            convertedCode = DCR_MF12_KEY;
        }

        return convertedCode;
    }

    public static String toIpacFullCompanyName(String dcrCode) {

        if (dcrCode.equalsIgnoreCase(DCR_SLOCPI)) {
            dcrCode = IPAC_FULL_SLOCP_KEY;
        } else if (dcrCode.equalsIgnoreCase(DCR_SLFPI)) {
            dcrCode = IPAC_FULL_SLFPI_KEY;
        } else if (dcrCode.equalsIgnoreCase(DCR_SLGFI)) {
            dcrCode = IPAC_FULL_SLGFI_KEY;
        } else if (dcrCode.equalsIgnoreCase(DCR_SLAMCID) || dcrCode.equalsIgnoreCase(DCR_SLAMCIP)) {
            dcrCode = IPAC_FULL_SLAMCI_KEY;
        } else if (dcrCode.equalsIgnoreCase(DCR_OTHER)) {
            dcrCode = IPAC_FULL_SLOCP_KEY;
        }

        return dcrCode;
    }

    public static String toIpacComCode(Long companyId) {
        String comCode = null;
        if (companyId == 1L || companyId == 2L) {
            comCode = IPAC_SLAMC_KEY;
        } else if (companyId == 3) {
            comCode = IPAC_SLOCP_KEY;
        } else if (companyId == 4) {
            comCode = IPAC_SLGFI_KEY;
        } else if (companyId == 5) {
            comCode = IPAC_SLFPI_KEY;
        }
        return comCode;
    }

    public static Long toDcrComId(String ipacCode, Long currencyId) {
        Long comCode = null;
        if (IPAC_SLAMC_KEY.equalsIgnoreCase(ipacCode) && currencyId == 1L) {
            comCode = 1L;
        } else if (IPAC_SLAMC_KEY.equalsIgnoreCase(ipacCode) && currencyId == 2L) {
            comCode = 2L;
        } else if (IPAC_SLOCP_KEY.equalsIgnoreCase(ipacCode)) {
            comCode = 3L;
        } else if (IPAC_SLGFI_KEY.equalsIgnoreCase(ipacCode)) {
            comCode = 4L;
        } else if (IPAC_SLFPI_KEY.equalsIgnoreCase(ipacCode)) {
            comCode = 5L;
        }
        return comCode;
    }
}
