package ph.com.sunlife.wms.dao.domain;

import java.util.Date;

import ph.com.sunlife.wms.util.IpacUtil;
import ph.com.sunlife.wms.util.WMSDateUtil;


public class DCRChequeDepositSlip extends Entity {

	private DCRCashier dcrCashier = new DCRCashier();

	private Bank depositoryBank = new Bank();

	private Long companyId;
	
	private Long checkTypeId;
	
	private CheckType checkType;

	private Company company;
	
	private String companyStr;

	private String accountNumber;
	
	private String customerCenter;
	
	private String customerCenterName;

	private Long currencyId;

	private Currency currency;
	
	private String prodCode;

	private double totalChequeAmount;

	private double requiredTotalAmount;
	
	private Date depositDate;
	
	private String depositDateStr;
	
	private Date dcrDate;
	
	private String dcrDateStr;
	
	private String depositDateFormatted;
	
	private String acf2id;
	
	private String cashierName;
	
	public String getAcf2id() {
		return acf2id;
	}

	public void setAcf2id(String acf2id) {
		this.acf2id = acf2id;
	}

	private byte[] deposlipFile;
	
	private String dcrDepoSlipVersionSerialId;
	
	


	public DCRCashier getDcrCashier() {
		return dcrCashier;
	}

	public void setDcrCashier(Long dcrCashierId) {
		if (dcrCashier == null) {
			dcrCashier = new DCRCashier();
		}
		dcrCashier.setId(dcrCashierId);
	}

	public Bank getDepositoryBank() {
		return depositoryBank;
	}

	public void setDepositoryBank(Bank depositoryBank) {
		this.depositoryBank = depositoryBank;
	}
	
	public void setDepositoryBank(String bankId) {
		if (depositoryBank == null) {
			depositoryBank = new Bank();
		}
		depositoryBank.setId(bankId);
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
		this.companyId = company.getId();
		this.companyStr = IpacUtil.toIpacComCode(company.getId());
	}

	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
		this.company = Company.getCompany(companyId);
		this.companyStr = IpacUtil.toIpacComCode(companyId);
	}

	public Long getCompanyId() {
		return companyId;
	}

	public void setCustomerCenter(String customerCenter) {
		this.customerCenter = customerCenter;
	}

	public String getCustomerCenter() {
		return customerCenter;
	}

	public String getCustomerCenterName() {
		return customerCenterName;
	}

	public void setCustomerCenterName(String customerCenterName) {
		this.customerCenterName = customerCenterName;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public Long getCurrencyId() {
		return currencyId;
	}

	public void setCurrencyId(Long currencyId) {
		this.currencyId = currencyId;
	}

	public Currency getCurrency() {
		if (currencyId != null) {
			this.currency = Currency.getCurrency(currencyId);
		}

		return currency;
	}

	public void setCurrency(Currency currency) {
		this.currency = currency;
		this.currencyId = currency.getId();
	}

	public double getTotalChequeAmount() {
		return totalChequeAmount;
	}

	public void setTotalChequeAmount(double totalChequeAmount) {
		this.totalChequeAmount = totalChequeAmount;
	}

	public double getRequiredTotalAmount() {
		return requiredTotalAmount;
	}

	public void setRequiredTotalAmount(double requiredTotalAmount) {
		this.requiredTotalAmount = requiredTotalAmount;
	}

	public Date getDepositDate() {
		return depositDate;
	}

	public void setDepositDate(Date depositDate) {
		this.depositDate = depositDate;
		this.depositDateStr = WMSDateUtil.toFormattedDateStr(depositDate);
	}
	
	public void setDepositDateStr(String depositDateStr) {
		this.depositDateStr = depositDateStr;
		this.depositDate = WMSDateUtil.toDate(depositDateStr);
	}

	public String getDepositDateStr() {
		return depositDateStr;
	}
	
	public String getDcrDateStr() {
		return dcrDateStr;
	}

	public void setDcrDateStr(String dcrDateStr) {
		this.dcrDateStr = dcrDateStr;
		this.dcrDate = WMSDateUtil.toDate(dcrDateStr);
	}
	
	public Date getDcrDate() {
		return dcrDate;
	}

	public void setDcrDate(Date dcrDate) {
		this.dcrDate = dcrDate;
		this.dcrDateStr = WMSDateUtil.toFormattedDateStr(dcrDate);
	}

	public String getDepositDateFormatted() {
		return depositDateFormatted;
	}

	public void setDepositDateFormatted(String depositDateFormatted) {
		this.depositDateFormatted = depositDateFormatted;
	}

	public byte[] getDeposlipFile() {
		return deposlipFile;
	}

	public void setDeposlipFile(byte[] deposlipFile) {
		this.deposlipFile = deposlipFile;
	}

	public String getDcrDepoSlipVersionSerialId() {
		return dcrDepoSlipVersionSerialId;
	}

	public void setDcrDepoSlipVersionSerialId(String dcrDepoSlipVersionSerialId) {
		this.dcrDepoSlipVersionSerialId = dcrDepoSlipVersionSerialId;
	}

	public CheckType getCheckType() {
		return checkType;
	}

	public void setCheckType(CheckType checkType) {
		this.checkType = checkType;
	}
	
	public void setCheckType(Long checkTypeid) {
		this.checkType = CheckType.getById(checkTypeid);
		this.checkTypeId = checkTypeid;
	}

	public void setDcrCashier(DCRCashier dcrCashier) {
		this.dcrCashier = dcrCashier;
	}

	public void setCompanyStr(String companyStr) {
		this.companyStr = companyStr;
	}

	public String getCompanyStr() {
		return companyStr;
	}

	public void setCheckTypeId(Long checkTypeId) {
		this.checkTypeId = checkTypeId;
		this.checkType = CheckType.getById(checkTypeId);
	}

	public Long getCheckTypeId() {
		return checkTypeId;
	}

	public void setProdCode(String prodCode) {
		this.prodCode = prodCode;
	}

	public String getProdCode() {
		return prodCode;
	}

	public String getCashierName() {
		return cashierName;
	}

	public void setCashierName(String cashierName) {
		this.cashierName = cashierName;
	}



}
