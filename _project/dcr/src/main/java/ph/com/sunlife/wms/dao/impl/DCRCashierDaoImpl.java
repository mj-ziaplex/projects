package ph.com.sunlife.wms.dao.impl;

import java.sql.SQLException;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.orm.ibatis.SqlMapClientCallback;

import ph.com.sunlife.wms.dao.AbstractWMSSqlMapClientDaoSupport;
import ph.com.sunlife.wms.dao.DCRCashierDao;
import ph.com.sunlife.wms.dao.domain.DCR;
import ph.com.sunlife.wms.dao.domain.DCRCashier;
import ph.com.sunlife.wms.dao.domain.DCRCashierNoCollection;
import ph.com.sunlife.wms.dao.exceptions.WMSDaoException;

import com.ibatis.sqlmap.client.SqlMapExecutor;

/**
 * The implementing class for {@link DCRCashierDao}.
 * 
 * @author Zainal Limpao
 * 
 */
public class DCRCashierDaoImpl extends
		AbstractWMSSqlMapClientDaoSupport<DCRCashier> implements DCRCashierDao {

	private static final Logger LOGGER = Logger
			.getLogger(DCRCashierDaoImpl.class);

	private static final String NAMESPACE = "DCRCashier";

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ph.com.sunlife.wms.dao.AbstractWMSSqlMapClientDaoSupport#getNamespace()
	 */
	@Override
	protected String getNamespace() {
		return NAMESPACE;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ph.com.sunlife.wms.dao.AbstractWMSSqlMapClientDaoSupport#save(java.lang
	 * .Object)
	 */
	@Override
	public DCRCashier save(DCRCashier entity) throws WMSDaoException {
		DCR dcr = entity.getDcr();
		entity = super.save(entity);
		entity.setDcr(dcr);
		return entity;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<DCRCashier> getDCRCashierListForCashier(final DCRCashier sample)
			throws WMSDaoException {
		List<DCRCashier> execute = null;

		try {
			execute = (List<DCRCashier>) getSqlMapClientTemplate().execute(
					new SqlMapClientCallback() {

						@Override
						public Object doInSqlMapClient(SqlMapExecutor executor)
								throws SQLException {
							return executor.queryForList(getNamespace()
									+ ".getDCRCashierListForCashier", sample);
						}
					});
		} catch (Exception e) {
			throw new WMSDaoException(e);
		}
		return execute;
	}

	@SuppressWarnings("unchecked")
	public List<DCRCashier> getDCRCashierListByDcrId(final Long dcrId)
			throws WMSDaoException {
		List<DCRCashier> results = null;

		try {
			results = (List<DCRCashier>) getSqlMapClientTemplate().execute(
					new SqlMapClientCallback() {

						@Override
						public Object doInSqlMapClient(SqlMapExecutor executor)
								throws SQLException {
							return executor.queryForList(getNamespace()
									+ ".getDCRCashierListByDcrId", dcrId);
						}
					});
		} catch (Exception e) {
			throw new WMSDaoException(e);
		}
		return results;
	}

	@Override
	public boolean changeStatusToPending(Long dcrCashierId)
			throws WMSDaoException {
		boolean isUpdated = false;

		try {
			int rowsUpdated = getSqlMapClientTemplate().update(
					getNamespace() + ".changeStatusToPending", dcrCashierId);

			isUpdated = rowsUpdated != 0;
		} catch (Exception ex) {
			throw new WMSDaoException(ex);
		}

		return isUpdated;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<DCRCashier> getDCRCashierListByDcrList(final List<Long> dcrIds)
			throws WMSDaoException {
		List<DCRCashier> results = null;

		try {
			results = (List<DCRCashier>) getSqlMapClientTemplate().execute(
					new SqlMapClientCallback() {

						@Override
						public Object doInSqlMapClient(SqlMapExecutor executor)
								throws SQLException {
							return executor.queryForList(getNamespace()
									+ ".getDCRCashierListByDcrList", dcrIds);
						}
					});
		} catch (Exception e) {
			throw new WMSDaoException(e);
		}
		return results;
	}

	@Override
	public boolean insertNoCollection(
			final DCRCashierNoCollection dcrCashierNoCollection)
			throws WMSDaoException {

		boolean successful = false;

		try {
			getSqlMapClientTemplate().execute(new SqlMapClientCallback() {

				@Override
				public Object doInSqlMapClient(SqlMapExecutor executor)
						throws SQLException {
					return executor.insert(getNamespace()
							+ ".insertNoCollection", dcrCashierNoCollection);
				}
			});

			successful = true;
		} catch (Exception ex) {
			LOGGER.error(
					"Unable to insert record in DCRCashierNoCollection Table",
					ex);
		}

		return successful;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<DCRCashierNoCollection> getCashierNoCollections(
			final String acf2id) throws WMSDaoException {
		List<DCRCashierNoCollection> result = null;

		try {
			result = getSqlMapClientTemplate().executeWithListResult(
					new SqlMapClientCallback() {

						@Override
						public Object doInSqlMapClient(SqlMapExecutor executor)
								throws SQLException {

							return executor.queryForList(getNamespace()
									+ ".getCashierNoCollections", acf2id);
						}
					});
		} catch (Exception ex) {
			throw new WMSDaoException(ex);
		}

		return result;
	}

	@Override
	public boolean deleteCashierNoCollections(final Long dcrId)
			throws WMSDaoException {

		boolean success = false;

		try {
			success = (Boolean) getSqlMapClientTemplate().execute(
					new SqlMapClientCallback() {

						@Override
						public Object doInSqlMapClient(SqlMapExecutor executor)
								throws SQLException {
							int rowsDeleted = executor.delete(getNamespace()
									+ ".deleteCashierNoCollections", dcrId);
							return (rowsDeleted > 0);
						}
					});
		} catch (Exception ex) {
			LOGGER.error(ex);
		}

		return success;
	}
}
