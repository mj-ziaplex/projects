package ph.com.sunlife.wms.web.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.web.servlet.ModelAndView;

import ph.com.sunlife.wms.security.Role;
import ph.com.sunlife.wms.services.DCRCreationService;
import ph.com.sunlife.wms.services.PPAMDSService;
import ph.com.sunlife.wms.services.bo.DCRBO;
import ph.com.sunlife.wms.services.bo.DCRPPAMDSBO;
import ph.com.sunlife.wms.services.bo.PPAMDSBO;
import ph.com.sunlife.wms.services.bo.UserSession;
import ph.com.sunlife.wms.services.exception.ServiceException;
import ph.com.sunlife.wms.web.controller.form.MDSForm;
import ph.com.sunlife.wms.web.controller.form.PPAMDSForm;
import ph.com.sunlife.wms.web.exception.ApplicationException;

public class PPAMDSController extends AbstractSecuredMultiActionController {

    private static final String PPAMDS_PAGE_VIEW = "ppamdsViewPage";
    private static final String PPAMDS_KEY = "ppaMDSObjects";
    private static final String TITLE_KEY = "title";
    private PPAMDSService ppaMDSService;
    private DCRCreationService dcrCreationService;

    public PPAMDSService getPpaMDSService() {
        return ppaMDSService;
    }

    public void setPpaMDSService(PPAMDSService ppaMDSService) {
        this.ppaMDSService = ppaMDSService;
    }

    public DCRCreationService getDcrCreationService() {
        return dcrCreationService;
    }

    public void setDcrCreationService(DCRCreationService dcrCreationService) {
        this.dcrCreationService = dcrCreationService;
    }
    private static final Logger LOGGER = Logger.getLogger(PPAMDSController.class);

    public ModelAndView doShowHomePage(
            HttpServletRequest request,
            HttpServletResponse response,
            PPAMDSForm form) throws ApplicationException {

        ModelAndView modelAndView = new ModelAndView();

        try {
            String dcrIdStr = form.getDcrId();
            String ccId = form.getCcId();
            String dcrDateStr = form.getDcrDateStr();
            Long dcrId = Long.valueOf(dcrIdStr);
            // Added for MR-WF-16-00034 - Random sampling for QA
            boolean isPpaQa = form.isIsPpaQa();

            DCRBO thisDcr = dcrCreationService.getDcrById(dcrId);
            List<PPAMDSBO> ppaMdsList = new ArrayList<PPAMDSBO>();

            if (thisDcr != null) {
                Date dateTo = thisDcr.getDcrDate();
                Date dateFrom = thisDcr.getDcrDate();
                ppaMdsList = ppaMDSService.getMDSTransactions(ccId, dateFrom, dateTo, dcrId);
            }

            Set<PPAMDSBO> ppaMdsSet = new HashSet<PPAMDSBO>(ppaMdsList);

            modelAndView.addObject(PPAMDS_KEY, new ArrayList<PPAMDSBO>(ppaMdsSet));
            modelAndView.addObject("dcrId", dcrIdStr);
            modelAndView.addObject("ccId", ccId);
            modelAndView.addObject("dcrDateStr", dcrDateStr);
            modelAndView.addObject("savePPAMDSMapping", "/ppamds/save.html");
            modelAndView.addObject(TITLE_KEY, "PPA MDS Screen");
            // Added for MR-WF-16-00034 - Random sampling for QA
            modelAndView.addObject("isPpaQa", isPpaQa);
        } catch (ServiceException e) {
            LOGGER.error("Error in PPA MDS Controller: ", e);
            throw new ApplicationException(e);
        }

        modelAndView.setViewName(PPAMDS_PAGE_VIEW);
        return modelAndView;
    }

    public ModelAndView doSave(
            HttpServletRequest request,
            HttpServletResponse response,
            MDSForm form) throws ApplicationException {
        ModelAndView modelAndView;

        List<DCRPPAMDSBO> reconciledEntries = form.getPpamdsRecons();
        List<DCRPPAMDSBO> unreconciledEntries = form.getPpamdsUnrecons();

        try {
            UserSession userSession = getUserSession(request);
            ppaMDSService.reconcilePPAMDSList(reconciledEntries, StringUtils.lowerCase(userSession.getUserId()));

            List<DCRPPAMDSBO> entries = new ArrayList<DCRPPAMDSBO>();
            if (CollectionUtils.isNotEmpty(reconciledEntries)) {
                entries.addAll(reconciledEntries);
            }

            if (CollectionUtils.isNotEmpty(unreconciledEntries)) {
                entries.addAll(unreconciledEntries);
            }

            String dcrIdStr = form.getDcrId();
            Long dcrId = Long.valueOf(dcrIdStr);
            ppaMDSService.savePPAMDSNotes(dcrId, entries);

        } catch (ServiceException e) {
            LOGGER.error("Error in PPA MDS Controller: ", e);
            throw new ApplicationException(e);
        }
        
        modelAndView = this.doShowHomePage(request, response, form);
        modelAndView.setViewName(PPAMDS_PAGE_VIEW);
        return modelAndView;
    }

    @Override
    protected Role[] allowedRoles() {
        // Change according to Healthcheck
        // Problem was due to changes in AbstractSecuredMultiActionController
        // Change was use parent getter method instead of inherited values
        return super.getALL_ROLES();
    }
}
