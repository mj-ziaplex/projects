/*
 * ** Date: May 2017
 * ** Dev: Cshells Sayan
 * ** Desc: Added for a more dynamic approach; SR-WF-16-00016 - SLAMC - New USD Fund (WMS-DCR)
 */
package ph.com.sunlife.wms.dao.impl;

import org.apache.log4j.Logger;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;
import ph.com.sunlife.wms.dao.domain.Company;
import ph.com.sunlife.wms.dao.domain.Currency;

import ph.com.sunlife.wms.dao.domain.ProductType;
import ph.com.sunlife.wms.dao.domain.ProductTypeLookUp;
import ph.com.sunlife.wms.dao.exceptions.WMSDaoException;

/**
 * Added for SR-WF-16-00016 - SLAMC - New USD Fund (WMS-DCR)
 */
public class ProductTypeDaoImpl extends SqlMapClientDaoSupport {

    private static final Logger LOGGER = Logger.getLogger(ProductTypeDaoImpl.class);
    private static final String NAMESPACE = "ProductType";
    
    /*
     * (non-Javadoc)
     */
    protected String getNamespace() {
        return NAMESPACE;
    }
    
    /*
     * Method will run after application has been initialized to populate productTypes enum class based on table content
     */
    public void init() {       
        for(ProductType tmp : ProductType.values()) {
            try {
                ProductTypeLookUp ptLookUp = getProductType(tmp.getId());                

                if (ptLookUp != null) {
                	tmp.setCompany(Company.getCompany(ptLookUp.getCompany()));
                    tmp.setCurrency(Currency.getCurrency(ptLookUp.getCurrency()));
                    tmp.setProductName(ptLookUp.getProductName());
                    tmp.setProductCode(ptLookUp.getProductCode());
                    tmp.setFltProductCode(ptLookUp.getFltProductCode());
                    tmp.setProductPaySubCurr(ptLookUp.getProductPaySubCurr());	
                }
            } catch (WMSDaoException ex) {
                LOGGER.error("Error in getting productType details during init - (init): ", ex);
            }
        }
    }

    
    /*
     * Fetch product type details in Database by productTypeId
     */
    public ProductTypeLookUp getProductType(final Long productId) throws WMSDaoException {
        
        ProductTypeLookUp ptLookUp = null;
        try {
            ptLookUp = (ProductTypeLookUp) getSqlMapClientTemplate().queryForObject(NAMESPACE + ".getProductType", productId);
        } catch (Exception e) {
            LOGGER.error("Error in getting product type list - (getProductTypeList): ", e);
            throw new WMSDaoException(e);
        }

        return ptLookUp;
    }
}
