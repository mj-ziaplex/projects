package ph.com.sunlife.wms.services;

import java.util.List;
import ph.com.sunlife.wms.dao.domain.DatacapScanner;
import ph.com.sunlife.wms.services.exception.ServiceException;

public  interface DatacapScannerService
{
   List<DatacapScanner> getAllScanner()
    throws ServiceException;
  
   DatacapScanner getScannerById(String param)
    throws ServiceException;
  
   boolean deleteScanner(String param)
    throws ServiceException;
}

