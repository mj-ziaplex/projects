package ph.com.sunlife.wms.services.bo;

import java.util.List;

import javax.servlet.http.HttpSession;

public class AdminCenterCodeBO {

	private String ccId;
	private String ccName;
	private String ccDesc;
	private String ccAutoIdx;
	private String ccCanEncode;
	private String ccViewImage;
	private String ccZoomZone;
	private String ccActive;
	private String ccAbbrevName;
	private String ccIsIso;
	private HttpSession session;
	private String userId;
	private String userHub;
	private List<String> userGroups;
	private String userNBO;
	private String cId;
	
	public String getCcId() {
		return ccId;
	}
	public void setCcId(String ccId) {
		this.ccId = ccId;
	}
	public String getCcName() {
		return ccName;
	}
	public void setCcName(String ccName) {
		this.ccName = ccName;
	}
	public String getCcDesc() {
		return ccDesc;
	}
	public void setCcDesc(String ccDesc) {
		this.ccDesc = ccDesc;
	}
	public String getCcAutoIdx() {
		return ccAutoIdx;
	}
	public void setCcAutoIdx(String ccAutoIdx) {
		this.ccAutoIdx = ccAutoIdx;
	}
	public String getCcCanEncode() {
		return ccCanEncode;
	}
	public void setCcCanEncode(String ccCanEncode) {
		this.ccCanEncode = ccCanEncode;
	}
	public String getCcViewImage() {
		return ccViewImage;
	}
	public void setCcViewImage(String ccViewImage) {
		this.ccViewImage = ccViewImage;
	}
	public String getCcZoomZone() {
		return ccZoomZone;
	}
	public void setCcZoomZone(String ccZoomZone) {
		this.ccZoomZone = ccZoomZone;
	}
	public String getCcActive() {
		return ccActive;
	}
	public void setCcActive(String ccActive) {
		this.ccActive = ccActive;
	}
	public String getCcAbbrevName() {
		return ccAbbrevName;
	}
	public void setCcAbbrevName(String ccAbbrevName) {
		this.ccAbbrevName = ccAbbrevName;
	}
	public String getCcIsIso() {
		return ccIsIso;
	}
	public void setCcIsIso(String ccIsIso) {
		this.ccIsIso = ccIsIso;
	}
	public HttpSession getSession() {
		return session;
	}
	public void setSession(HttpSession session) {
		this.session = session;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getUserHub() {
		return userHub;
	}
	public void setUserHub(String userHub) {
		this.userHub = userHub;
	}
	public List<String> getUserGroups() {
		return userGroups;
	}
	public void setUserGroups(List<String> userGroups) {
		this.userGroups = userGroups;
	}
	public String getUserNBO() {
		return userNBO;
	}
	public void setUserNBO(String userNBO) {
		this.userNBO = userNBO;
	}
	public String getcId() {
		return cId;
	}
	public void setcId(String cId) {
		this.cId = cId;
	}
	
}
