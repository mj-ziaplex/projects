package ph.com.sunlife.wms.dao.domain;

import java.util.Date;

public class DCRComment extends Entity {
	
	private DCRCashier dcrCashier = new DCRCashier();

	private String acf2id;

	private String remarks;

	private Date datePosted;
	
	private Long dcrId;

	public DCRCashier getDcrCashier() {
		return dcrCashier;
	}

	public void setDcrCashier(DCRCashier dcrCashier) {
		this.dcrCashier = dcrCashier;
	}
	
	public void setDcrCashier(Long dcrCashierId) {
		DCRCashier dcrCashier = new DCRCashier();
		dcrCashier.setId(dcrCashierId);
		this.dcrCashier = dcrCashier;
	}

	public String getAcf2id() {
		return acf2id;
	}

	public void setAcf2id(String acf2id) {
		this.acf2id = acf2id;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public Date getDatePosted() {
		return datePosted;
	}

	public void setDatePosted(Date datePosted) {
		this.datePosted = datePosted;
	}

	public Long getDcrId() {
		return dcrId;
	}

	public void setDcrId(Long dcrId) {
		this.dcrId = dcrId;
	}

}
