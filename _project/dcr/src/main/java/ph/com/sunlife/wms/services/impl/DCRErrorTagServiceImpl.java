package ph.com.sunlife.wms.services.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.apache.log4j.Logger;

import ph.com.sunlife.wms.dao.DCRErrorTagDao;
import ph.com.sunlife.wms.dao.domain.DCRError;
import ph.com.sunlife.wms.dao.domain.DCRErrorTagLog;
import ph.com.sunlife.wms.dao.domain.DCRErrorTagLogDisplay;
import ph.com.sunlife.wms.dao.domain.DCRErrorTagLogLookup;
import ph.com.sunlife.wms.dao.domain.DCRErrorTagLookupDisplay;
import ph.com.sunlife.wms.dao.exceptions.WMSDaoException;
import ph.com.sunlife.wms.security.Role;
import ph.com.sunlife.wms.services.DCRErrorTagService;
import ph.com.sunlife.wms.services.bo.DCRCompletedStepBO;
import ph.com.sunlife.wms.services.bo.DCRErrorBO;
import ph.com.sunlife.wms.services.bo.DCRErrorTagLogBO;
import ph.com.sunlife.wms.services.bo.DCRErrorTagLogConsolidatedDisplayBO;
import ph.com.sunlife.wms.services.bo.DCRErrorTagLookupBO;
import ph.com.sunlife.wms.services.exception.ServiceException;
import ph.com.sunlife.wms.util.WMSCollectionUtils;

/**
 * The implementing class for {@link DCRErrorTagService}.
 * 
 * @author Josephus Sardan
 * 
 */
public class DCRErrorTagServiceImpl implements DCRErrorTagService {

	private static final Logger LOGGER = Logger
			.getLogger(DCRErrorTagServiceImpl.class);

	private DCRErrorTagDao dcrErrorTagDao;

	public void setDcrErrorTagDao(DCRErrorTagDao dcrErrorTagDao) {
		this.dcrErrorTagDao = dcrErrorTagDao;
	}

//	@Override
//	public DCRErrorTagLookupBO getDCRErrorTagLookupDisplay(final Long dcrId)
//			throws ServiceException {
//		DCRErrorTagLookupBO businessObject = new DCRErrorTagLookupBO();
//		try {
//			List<DCRErrorTagLookupDisplay> lookupDisplays = dcrErrorTagDao
//					.getDCRErrorTagLookupDisplay(dcrId);
//
//			businessObject = convertToBusinesObject1(lookupDisplays);
//
//		} catch (WMSDaoException e) {
//			LOGGER.error(e);
//			throw new ServiceException(e);
//		}
//		return businessObject;
//	}

	// THE MAKE OR BREAK METHOD
//	private DCRErrorTagLookupBO convertToBusinesObject1(
//			List<DCRErrorTagLookupDisplay> lookupDisplays) {
//		DCRErrorTagLookupBO businessObject = new DCRErrorTagLookupBO();
//		if (CollectionUtils.isNotEmpty(lookupDisplays)) {
//
//			Set<DCRErrorBO> err = new HashSet<DCRErrorBO>();
//			Set<DCRCompletedStepBO> step = new HashSet<DCRCompletedStepBO>();
//			Set<DCRErrorTagLookupBO> lookup = new HashSet<DCRErrorTagLookupBO>();
//
//			// int ctr0 = 0;
//			for (DCRErrorTagLookupDisplay lookupDisplay : lookupDisplays) {
//				// System.out.println("lookuplist ctr0: "+ctr0++);
//				DCRErrorBO e = new DCRErrorBO();
//				e.setErrorId(lookupDisplay.getErrId());
//				e.setErrorName(lookupDisplay.getErrName());
//				err.add(e);
//
//				DCRCompletedStepBO s = new DCRCompletedStepBO();
//				s.setStepId(lookupDisplay.getStepId());
//				s.setStepName(lookupDisplay.getStepName());
//				s.setUserName(lookupDisplay.getUserName());
//				// s.setDcrErrors(dcrErrors) SA BABA
//				step.add(s);
//
//				DCRErrorTagLookupBO look = new DCRErrorTagLookupBO();
//				look.setDcrId(lookupDisplay.getDcrId());
//				// b.setDcrCompletedSteps(dcrCompletedSteps) SA BABA
//				lookup.add(look);
//
//				// bat pala sa huli ito?
//				e.setCompletedStep(s);
//				s.setErrorTagLookup(look);
//			}
//
//			// System.out.println("set err size: "+err.size());
//			// System.out.println("set step size: "+step.size());
//			// System.out.println("set lookup size: "+lookup.size());
//
//			List<DCRErrorBO> errList = new ArrayList<DCRErrorBO>(err);
//			List<DCRCompletedStepBO> stepList = new ArrayList<DCRCompletedStepBO>(
//					step);
//			List<DCRErrorTagLookupBO> lookupList = new ArrayList<DCRErrorTagLookupBO>(
//					lookup);
//
//			// System.out.println("list err size: "+errList.size());
//			// System.out.println("list step size: "+stepList.size());
//			// System.out.println("list lookup size: "+lookupList.size());
//
//			// int ctr1 = 0;
//			for (DCRCompletedStepBO ss : stepList) {
//				// System.out.println("lookuplist ctr1: "+ctr1++);
//				final DCRCompletedStepBO finalss = ss;
//				List<DCRErrorBO> filteredErrList = WMSCollectionUtils
//						.findMatchedElements(errList, new Predicate() {
//							@Override
//							public boolean evaluate(Object object) {
//								DCRErrorBO ee = (DCRErrorBO) object;
//								return ee.getCompletedStep().equals(finalss);
//							}
//						});
//				// System.out.println("filtered err list size: "+filteredErrList.size());
//				Collections.sort(filteredErrList);
//				ss.setDcrErrors(filteredErrList);
//			}
//
//			// int ctr2 = 0;
//			for (DCRErrorTagLookupBO ll : lookupList) {
//				// System.out.println("lookuplist ctr2: "+ctr2++);
//				final DCRErrorTagLookupBO finalll = ll;
//				List<DCRCompletedStepBO> filteredStepList = WMSCollectionUtils
//						.findMatchedElements(stepList, new Predicate() {
//							@Override
//							public boolean evaluate(Object object) {
//								DCRCompletedStepBO ss = (DCRCompletedStepBO) object;
//								return ss.getErrorTagLookup().equals(finalll);
//							}
//						});
//				// System.out.println("filtered step list size: "+filteredStepList.size());
//				Collections.sort(filteredStepList);
//				ll.setDcrCompletedSteps(filteredStepList);
//			}
//			// dapat gumana to
//			businessObject = lookupList.get(0);
//		}
//		return businessObject;
//	}

	@Override
	public DCRErrorTagLogBO addDCRErrorTagLog(DCRErrorTagLogBO bo)
			throws ServiceException {
		DCRErrorTagLog entity1 = new DCRErrorTagLog();
		List<DCRErrorTagLogLookup> entityList = new ArrayList<DCRErrorTagLogLookup>();

		try {
			// TODO: still to test!
			this.convertToEntity1(bo, entity1);
			this.convertToEntity2(bo, entityList);
			// from 1 DCRErrorTagLogBO
			// to 1 DCRErrorTagLog and many DCRErrorTagLogLookup objects

			entity1 = dcrErrorTagDao.addDCRErrorTagLog(entity1);
			Long logId = entity1.getId();
			bo.setId(logId);
			for (DCRErrorTagLogLookup entity2 : entityList) {
				entity2.setLogId(logId);
				entity2 = dcrErrorTagDao.addDCRErrorTagLoglookup(entity2);
			}

		} catch (WMSDaoException e) {
			LOGGER.error(e);
			throw new ServiceException(e);
		}
		return bo;
	}

	private void convertToEntity1(DCRErrorTagLogBO bo, DCRErrorTagLog entity) {
		entity.setDcrId(bo.getDcrId());
		entity.setDatePosted(bo.getDatePosted());
		entity.setDateUpdated(bo.getDateUpdated());
		entity.setReason(bo.getReason());
		entity.setPostedUpdatedById(bo.getPostedUpdatedById());
		entity.setReasonDeleted(bo.getReasonDeleted());
		entity.setStepId(bo.getDcrCompletedStep().getStepId());
		entity.setStepCompletorUserId(bo.getDcrCompletedStep().getUserName());
	}

	private void convertToEntity2(DCRErrorTagLogBO bo,
			List<DCRErrorTagLogLookup> entityList) {
		Long dcrId = bo.getDcrId();
		String stepId = bo.getDcrCompletedStep().getStepId();
		List<DCRErrorBO> errs = bo.getDcrCompletedStep().getDcrErrors();

		for (DCRErrorBO err : errs) {
			DCRErrorTagLogLookup entity2 = new DCRErrorTagLogLookup();
			entity2.setDcrId(dcrId);
			entity2.setStepId(stepId);
			entity2.setErrId(err.getErrorId());
			entityList.add(entity2);
		}
	}

	@Override
	public List<DCRErrorTagLogConsolidatedDisplayBO> getDCRErrorTagLogDisplay(
			final Long dcrId) throws ServiceException {
		// List<DCRErrorTagLogDisplayBO> boList = new
		// ArrayList<DCRErrorTagLogDisplayBO>();
		List<DCRErrorTagLogDisplay> entityList = new ArrayList<DCRErrorTagLogDisplay>();
		List<DCRErrorTagLogConsolidatedDisplayBO> aggregatedBOs = new ArrayList<DCRErrorTagLogConsolidatedDisplayBO>();
		try {
			entityList = dcrErrorTagDao.getDCRErrorTagLogDisplay(dcrId);
			// this.convertToBusinessObject2(entityList, boList);
			aggregatedBOs = convertToBusinessObject3(entityList);
			// ^return this
		} catch (WMSDaoException e) {
			LOGGER.error(e);
			throw new ServiceException(e);
		}
		return aggregatedBOs;
	}

	private List<DCRErrorTagLogConsolidatedDisplayBO> convertToBusinessObject3(
			List<DCRErrorTagLogDisplay> entityList) {
		List<DCRErrorTagLogConsolidatedDisplayBO> aggregatedBOs = new ArrayList<DCRErrorTagLogConsolidatedDisplayBO>();
		if (CollectionUtils.isNotEmpty(entityList)) {

			Set<DCRErrorBO> err = new HashSet<DCRErrorBO>();
			Set<DCRErrorTagLogConsolidatedDisplayBO> disp = new HashSet<DCRErrorTagLogConsolidatedDisplayBO>();

			for (DCRErrorTagLogDisplay entity : entityList) {
				DCRErrorBO e = new DCRErrorBO();
				e.setErrorId(entity.getErrorId());
				String errName = entity.getErrorName();
				//System.out.println("JSARD errName: " + errName);
				if ("Other".equalsIgnoreCase(errName)) {
					errName = entity.getReason();// NOT WORKING?
				}
				e.setErrorName(errName);

				DCRErrorTagLogConsolidatedDisplayBO c = new DCRErrorTagLogConsolidatedDisplayBO();
				c.setId(entity.getId());// NEW
				c.setDcrId(entity.getDcrId());
				c.setDatePosted(entity.getDatePosted());
				c.setDateUpdated(entity.getDateUpdated());
				c.setStepId(entity.getStepId());
				c.setStepName(entity.getStepName());
				c.setStepCompletorUserId(entity.getStepCompletorUserId());
				c.setReason(entity.getReason());
				c.setPostedUpdatedById(entity.getPostedUpdatedById());
				c.setReasonDeleted(entity.getReasonDeleted());// ?
				disp.add(c);

				e.setConsolidatedDisplay(c);
				err.add(e);
			}

			List<DCRErrorBO> errList = new ArrayList<DCRErrorBO>(err);
			List<DCRErrorTagLogConsolidatedDisplayBO> dispList = new ArrayList<DCRErrorTagLogConsolidatedDisplayBO>(
					disp);

			for (DCRErrorTagLogConsolidatedDisplayBO cc : dispList) {
				final DCRErrorTagLogConsolidatedDisplayBO finalcc = cc;
				List<DCRErrorBO> filteredErrList = WMSCollectionUtils
						.findMatchedElements(errList, new Predicate() {
							@Override
							public boolean evaluate(Object object) {
								DCRErrorBO ee = (DCRErrorBO) object;
								return ee.getConsolidatedDisplay().equals(
										finalcc);
							}
						});
				Collections.sort(filteredErrList);
				cc.setErrors(filteredErrList);
			}
			aggregatedBOs = dispList;
		}
		return aggregatedBOs;
	}
	
	//not used anymore
	@Override
	public void editDCRErrorLogAndLookup(DCRErrorTagLogBO bo)
			throws ServiceException {

		// DCRErrorTagLog entity1 = new DCRErrorTagLog();
		List<DCRErrorTagLogLookup> entityList = new ArrayList<DCRErrorTagLogLookup>();

		String stepId = bo.getDcrCompletedStep().getStepId();
		Long dcrId = bo.getDcrId();
		String reason = bo.getReason();
		Date dateUpdated = bo.getDateUpdated();
		String postedUpdatedById = bo.getPostedUpdatedById();

		try {
			// TODO: mga needed to edit
			// A.update 'date updated' and 'reason'
			dcrErrorTagDao.updateErrorLogByDcrIdAndStepId(reason, dateUpdated,
					dcrId, stepId, postedUpdatedById);
			// B.wipe all associated errs via 'dcrId' and 'stepId'
			dcrErrorTagDao.deleteErrorLogLookupByDcrIdAndStepId(dcrId, stepId);//only delete records that are displayed in one row, don't include 'deleted' red rows

			this.convertToEntity2(bo, entityList);
			// entity1 = dcrErrorTagDao.addDCRErrorTagLog(entity1);
			// Long logId = entity1.getId();
			// bo.setId(logId);

			// C.loop insert 'dcr errs'
			for (DCRErrorTagLogLookup entity2 : entityList) {
				// entity2.setLogId(logId);
				entity2.setLogId(bo.getId());// NEW
				entity2 = dcrErrorTagDao.addDCRErrorTagLoglookup(entity2);
			}

		} catch (WMSDaoException e) {
			LOGGER.error(e);
			throw new ServiceException(e);
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ph.com.sunlife.wms.services.DCRErrorTagService#tagErrorLogAsDeleted(java
	 * .lang.String, java.lang.Long, java.lang.String)
	 */
	@Override
	public void tagErrorLogAsDeleted(String reasonDeleted, Long dcrId,
			String stepId, String userId,String tagger) throws ServiceException {
		try {
			dcrErrorTagDao.deleteErrorLogByDcrIdAndStepId(reasonDeleted, dcrId,
					stepId, userId, tagger);
		} catch (WMSDaoException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public List<DCRErrorBO> getDCRErrorByStatus(String dcrStatus)
			throws ServiceException {
		
		List<DCRError> entityList = new ArrayList<DCRError>();
		List<DCRErrorBO> errListBO =  new ArrayList<DCRErrorBO>();
		
		try{
			entityList = dcrErrorTagDao.getDCRErrorByStatus(dcrStatus);
			errListBO = convertToBusinessObject4(entityList);
		}catch(WMSDaoException e){
			LOGGER.error(e);
			throw new ServiceException(e);
		}

		return errListBO;
	}

	private List<DCRErrorBO> convertToBusinessObject4(List<DCRError> entityList) {
		List<DCRErrorBO> errListBO =  new ArrayList<DCRErrorBO>();
		if (CollectionUtils.isNotEmpty(entityList)) {
			for (DCRError entity : entityList) {
				DCRErrorBO err = new DCRErrorBO();
				err.setErrorId(entity.getErrorId());
				err.setErrorName(entity.getErrorName());
				errListBO.add(err);
			}
		}
		return errListBO;
	}

	@Override
	public String getStepIdByStatus(String dcrStatus) throws ServiceException {
		String stepId = null;
		
		try{
			stepId = dcrErrorTagDao.getStepIdByStatus(dcrStatus);
		}catch(WMSDaoException e){
			LOGGER.error(e);
			throw new ServiceException(e);
		}

		return stepId;
	}

	@Override
	public String getManagerByDcrId(Long dcrId) throws ServiceException {
		String managerId = null;
		
		try{
			managerId = dcrErrorTagDao.getManagerByDcrId(dcrId);
		}catch(WMSDaoException e){
			LOGGER.error(e);
			throw new ServiceException(e);
		}

		return managerId;
	}

	// private void convertToBusinessObject2(
	// List<DCRErrorTagLogDisplay> entityList,
	// List<DCRErrorTagLogDisplayBO> boList) {
	// for(DCRErrorTagLogDisplay entity : entityList){
	// DCRErrorTagLogDisplayBO bo = new DCRErrorTagLogDisplayBO();
	// bo.setDcrId(entity.getDcrId());
	// bo.setDatePosted(entity.getDatePosted());
	// bo.setDateUpdated(entity.getDateUpdated());
	// bo.setStepId(entity.getStepId());
	// bo.setStepName(entity.getStepName());
	// bo.setStepCompletorUserId(entity.getStepCompletorUserId());
	// bo.setErrorId(entity.getErrorId());
	// bo.setErrorName(entity.getErrorName());
	// bo.setReason(entity.getReason());
	// bo.setPostedUpdatedById(entity.getPostedUpdatedById());
	// bo.setReasonDeleted(entity.getReasonDeleted());
	// boList.add(bo);
	// }
	// }

	@Override
	public List<String> getUsersFromAuditLogByDcrId(String dcrId)
			throws ServiceException {
	List<String> userId = new ArrayList<String>();
		
		try{
			userId = dcrErrorTagDao.getUsersFromAuditLogByDcrId(Long.valueOf(dcrId));
		}catch(WMSDaoException e){
			LOGGER.error(e);
			throw new ServiceException(e);
		}
		
		return userId;
	}

}
