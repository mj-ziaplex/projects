package ph.com.sunlife.wms.services;

import java.util.Date;
import java.util.List;

import ph.com.sunlife.wms.security.Role;
import ph.com.sunlife.wms.services.bo.AttachmentBO;
import ph.com.sunlife.wms.services.bo.DCRCashDepositSlipBO;
import ph.com.sunlife.wms.services.bo.DCRChequeDepositSlipBO;
import ph.com.sunlife.wms.services.bo.DCRFileBO;
import ph.com.sunlife.wms.services.bo.MatchBO;
import ph.com.sunlife.wms.services.exception.InvalidFileTypeException;
import ph.com.sunlife.wms.services.exception.ServiceException;

/**
 * Service object responsible for attaching, retrieving and manipulating files
 * unto the DCR Workitems.
 * 
 * @author Zainal Limpao
 * @author Edgardo Cunanan
 */
public interface AttachmentService {

	/**
	 * Gets the list of {@link AttachmentBO} given the id of the Cashier Work
	 * Item.
	 * 
	 * @param dcrCashierId
	 * @return
	 * @throws ServiceException
	 */
	List<AttachmentBO> getAttachmentsByDcrCashierId(Long dcrCashierId)
			throws ServiceException;

	/**
	 * Gets the list of {@link AttachmentBO} given the DCR Workitem ID.
	 * 
	 * @param dcrId
	 * @return
	 * @throws ServiceException
	 */
	List<AttachmentBO> getAttachmentsByDcr(Long dcrId) throws ServiceException;

	/**
	 * The Cashier Work Item wizard contains a page that allows Cashiers to
	 * upload custom files. This method is responsible for persisting such to
	 * both the WMS database (for reference purposes) and to WMS Filenet.
	 * 
	 * @param dcrCashierId
	 * @param description
	 * @param fileType
	 * @param file
	 * @return
	 * @throws ServiceException
	 * @throws InvalidFileTypeException
	 *             - this denotes that the file being uploaded is not a valid
	 *             WMS Attachment.
	 */
	AttachmentBO uploadOtherFileAttachment(Long dcrCashierId,
			String description, String fileType, byte[] file)
			throws ServiceException, InvalidFileTypeException;

	/**
	 * Retrieves {@link DCRFileBO} given the Document ID.
	 * 
	 * @param filenetId
	 * @return
	 * @throws ServiceException
	 */
	DCRFileBO getFileFromFilenet(String filenetId) throws ServiceException;

	/**
	 * Extracts DCR Custom Content Type (MIME Type).
	 * 
	 * @param arr
	 * @return
	 * @throws InvalidFileTypeException
	 */
	String getDcrContentType(byte[] arr) throws InvalidFileTypeException;

	/**
	 * This method is being called via the DCR step processor's "Attach File"
	 * functionality. Note that this is not associated particularly to a Cashier
	 * Workitem, but to the entire DCR itself.
	 * 
	 * @param dcrId
	 * @param uploaderId
	 * @param description
	 * @param role
	 * @param file
	 * @return
	 * @throws ServiceException
	 * @throws InvalidFileTypeException
	 */
	AttachmentBO uploadNonCashierAttachment(Long dcrId, String uploaderId,
			String description, Role role, byte[] file)
			throws ServiceException, InvalidFileTypeException;

	/**
	 * This method is being called via the DCR step processor's "Create Note"
	 * functionality. Note that this is not associated particularly to a Cashier
	 * Workitem, but to the entire DCR itself.
	 * 
	 * @param dcrId
	 * @param uploaderId
	 * @param description
	 * @param role
	 * @param file
	 * @return
	 * @throws ServiceException
	 * @throws InvalidFileTypeException
	 */
	AttachmentBO uploadNotes(Long dcrId, String uploaderId, String description,
			Role role, byte[] file) throws ServiceException,
			InvalidFileTypeException;

	/**
	 * This method is being called via the DCR step processor's "Email"
	 * functionality. Emails are sent to the user and at the same time saved
	 * into the filenet in .EML format. Note that this is not associated
	 * particularly to a Cashier Workitem, but to the entire DCR itself.
	 * 
	 * @param dcrId
	 * @param uploaderId
	 * @param description
	 * @param role
	 * @param file
	 * @return
	 * @throws ServiceException
	 * @throws InvalidFileTypeException
	 */
	AttachmentBO uploadEmail(Long dcrId, String uploaderId, String description,
			Role role, byte[] file) throws ServiceException,
			InvalidFileTypeException;

	void unmatchAttachment(Long recordId, String recordLocation, Date dcrDate,
			String docCapsiteId, String documentId) throws ServiceException;

	void matchAttachment(MatchBO matchBo, String uploaderId, Long dcrId)
			throws ServiceException;

	List<AttachmentBO> getCandidateMatchAttachmentsByDCR(Long dcrId)
			throws ServiceException;

	List<DCRCashDepositSlipBO> getCashDepositSlips(Long dcrCashierId)
			throws ServiceException;

	List<DCRChequeDepositSlipBO> getCheckDepositSlips(Long dcrCashierId)
			throws ServiceException;

}
