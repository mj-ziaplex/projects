package ph.com.sunlife.wms.web.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;

import ph.com.sunlife.wms.dao.domain.Company;
import ph.com.sunlife.wms.dao.domain.ProductType;
import ph.com.sunlife.wms.services.DCROtherService;
import ph.com.sunlife.wms.services.bo.DCRBalancingToolProductBO;
import ph.com.sunlife.wms.services.bo.DCROtherBO;
import ph.com.sunlife.wms.services.exception.ServiceException;
import ph.com.sunlife.wms.web.controller.form.OtherForm;
import ph.com.sunlife.wms.web.exception.ApplicationException;

public class OtherController extends AbstractBalancingToolController {

	private String displayViewName = "otherBalancingToolView";

	private String pageTitle;

	private DCROtherService dcrOtherService;

	private static Logger LOGGER = Logger.getLogger(OtherController.class);

	public DCROtherService getDcrOtherService() {
		return dcrOtherService;
	}

	public void setDcrOtherService(DCROtherService dcrOtherService) {
		this.dcrOtherService = dcrOtherService;
	}

	@Override
	protected Company getCompany() {
		return Company.OTHER;
	}

	@Override
	protected String getDisplayViewName() {
		return this.displayViewName;
	}

	public void setDisplayViewName(String displayViewName) {
		this.displayViewName = displayViewName;
	}

	@Override
	protected ProductType[] productTypesToDisplay() {

		return new ProductType[] { ProductType.OTHER_DOLLAR,
				ProductType.OTHER_PESO };
	}

	@Override
	protected String getPageTitle() {
		return pageTitle;
	}

	public void setPageTitle(String pageTitle) {
		this.pageTitle = pageTitle;
	}

	@SuppressWarnings("unchecked")
	public ModelAndView confirmOthersBalancingToolPage(
			HttpServletRequest request, HttpServletResponse response,
			OtherForm form) throws ApplicationException {

		ModelAndView modelAndView = super.showBalancingToolPage(request,
				response, form);

		try {

			List<DCRBalancingToolProductBO> productsToDisplay = (List<DCRBalancingToolProductBO>) modelAndView
					.getModel().get(PRODUCTS_TO_DISPLAY_KEY);

			for (DCRBalancingToolProductBO ptd : productsToDisplay) {
				DCROtherBO dcrOtherBO = new DCROtherBO();
				DCROtherBO ptdBO = ptd.getDcrOther();
				dcrOtherBO.setDcrBalancingToolProductId(ptdBO
						.getDcrBalancingToolProductId());

				if (LOGGER.isDebugEnabled()) {
					LOGGER.debug("DBT Prod Id: "
							+ ptdBO.getDcrBalancingToolProductId());
				}

				if (ptd.getProductType().getProductCode()
						.equalsIgnoreCase("COBD")) {
					dcrOtherBO.setChequeNonCounter(form
							.getDtotalCheckNonCounter());
					dcrOtherBO.setTotalDollarCheque(form
							.getDtotalDollarCheque());
					dcrOtherBO.setTotalUsCheckInManila(form
							.getDtotalUsCheckInManila());
					dcrOtherBO.setTotalUsCheckOutPh(form
							.getDtotalUsCheckOutPh());
					dcrOtherBO.setCashCounterAmt(form.getDtotalCashCounter());
					dcrOtherBO
							.setCashNonCounter(form.getDtotalCashNonCounter());
				} else if (ptd.getProductType().getProductCode()
						.equalsIgnoreCase("COBP")) {
					dcrOtherBO.setCashCounterAmt(form.getPtotalCashCounter());
					dcrOtherBO
							.setCashNonCounter(form.getPtotalCashNonCounter());
					dcrOtherBO.setChequeNonCounter(form
							.getPtotalCheckNonCounter());
					dcrOtherBO.setChequeOnUs(form.getPtotalCheckOnUs());
					dcrOtherBO.setChequeRegional(form.getPtotalCheckRegional());
					dcrOtherBO.setChequeLocal(form.getPtotalChequeLocal());
				}
				// dcrOtherService.update(dcrOtherBO);
				ptd.setDcrOther(dcrOtherBO);
			}
			
			for (DCRBalancingToolProductBO ptd : productsToDisplay) {
				DCROtherBO dcrOtherBO = ptd.getDcrOther();
				dcrOtherService.update(dcrOtherBO);
			}

			if (form.isConfirm()) {
				modelAndView = super.confirmBalancingToolPage(request, response, form);
			} else {
				modelAndView.addObject(PRODUCTS_TO_DISPLAY_KEY, productsToDisplay);
			}

		} catch (ServiceException e) {
			LOGGER.error(e);
			// Swallow Exception
			modelAndView
					.addObject("alertMessage",
							"A problem occured while trying to save the Balancing Tool");
		}

		return modelAndView;
	}

	@Override
	protected void initBinder(HttpServletRequest request,
			ServletRequestDataBinder binder) throws Exception {
		super.initBinder(request, binder);

		binder.registerCustomEditor(Double.class, createMoneyPropertyEditor());
	}

}
