package ph.com.sunlife.wms.services.exception;

public class InvalidLogInCredentialsException extends RuntimeException {

	private static final long serialVersionUID = -1594529230759827375L;

	private String userId;

	public InvalidLogInCredentialsException(String message) {
		super(message);
	}

	public InvalidLogInCredentialsException(String message, Throwable throwable) {
		super(message, throwable);
	}

	public InvalidLogInCredentialsException(Throwable throwable) {
		super(throwable);
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
}
