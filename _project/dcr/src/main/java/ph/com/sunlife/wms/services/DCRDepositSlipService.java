package ph.com.sunlife.wms.services;

import java.util.List;
import java.util.Map;

import ph.com.sunlife.wms.dao.domain.CheckType;
import ph.com.sunlife.wms.services.bo.DCRBalancingToolProductBO;
import ph.com.sunlife.wms.services.bo.DCRCashDepositSlipBO;
import ph.com.sunlife.wms.services.bo.DCRChequeDepositSlipBO;
import ph.com.sunlife.wms.services.exception.ServiceException;
import ph.com.sunlife.wms.util.ParameterMap;

/**
 * Service responsible for the generating and saving of deposit slips.
 * 
 * @author Crisseljess Mendoza
 *
 */

public interface DCRDepositSlipService {
	
	
	/**
	 * this method saves the deposit slip in to the database and calls DCRFilenetIntegration Service for saving on ObjectStore
	 * @param dcrCashDepositSlip
	 * @throws ServiceException
	 */
	void saveCashDepositSlip(DCRCashDepositSlipBO dcrCashDepositSlip)
			throws ServiceException;
	/**
	 * this method converts the map object to a compatible Business object.
	 * @param dcrCashDepositSlip
	 * @throws ServiceException
	 */
	void saveCashDepositSlip(Map<String, Object> map) throws ServiceException;

	DCRCashDepositSlipBO getCashDepositSlip(Long dcrCashierId, Long currencyId,
			Long companyId,String prodCode) throws ServiceException;

	void saveChequeDepositSlip(DCRChequeDepositSlipBO businessObject)
			throws ServiceException;

	DCRChequeDepositSlipBO getChequeDepositSlip(Long dcrCashierId,
			Long currencyId, Long companyId, CheckType checkType, String prodCode)
			throws ServiceException;

	ParameterMap buildMapForFundBreakdownsSlamciPesoCash(
			List<DCRBalancingToolProductBO> products, ParameterMap map);

	ParameterMap buildMapForFundBreakdownsSlamciPesoCheque(
			List<DCRBalancingToolProductBO> products, ParameterMap map);

	ParameterMap buildMapForFundBreakdownsSlamciDollarCash(
			List<DCRBalancingToolProductBO> products, ParameterMap map);

	ParameterMap buildMapForFundBreakdownsSlamciDollarCheque(
			List<DCRBalancingToolProductBO> products, ParameterMap map);

	void saveChequeDepositSlip(Map<String, Object> map) throws ServiceException;

	DCRCashDepositSlipBO getIpacValuesForDisplay(String centerCode,
			DCRCashDepositSlipBO bo) throws ServiceException;

	DCRChequeDepositSlipBO getIpacValuesForDisplay(String centerCode,
			DCRChequeDepositSlipBO bo) throws ServiceException;
	
	String getCCNameById(String siteCode) throws ServiceException; 
	
	String getCashierNameByAcf2id(String acf2id) throws ServiceException;
	
	DCRChequeDepositSlipBO getSlocpiBankDetails(String centerCode,
			DCRChequeDepositSlipBO bo) throws ServiceException;
	
	DCRCashDepositSlipBO getSlocpiBankDetails(String centerCode,
			DCRCashDepositSlipBO bo) throws ServiceException;
}
