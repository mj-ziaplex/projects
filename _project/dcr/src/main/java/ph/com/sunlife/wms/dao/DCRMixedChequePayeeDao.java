package ph.com.sunlife.wms.dao;

import java.util.List;

import ph.com.sunlife.wms.dao.domain.Bank;
import ph.com.sunlife.wms.dao.domain.DCRMixedChequePayee;
import ph.com.sunlife.wms.dao.exceptions.WMSDaoException;

public interface DCRMixedChequePayeeDao extends WMSDao<DCRMixedChequePayee> {

	List<DCRMixedChequePayee> getAllByDcrCashier(Long dcrCashierId)
			throws WMSDaoException;

	boolean deleteById(Long id) throws WMSDaoException;

	boolean updateMixedChequePayee(DCRMixedChequePayee entity)
			throws WMSDaoException;
	
	List<Bank> getAllBanks() throws WMSDaoException;

}
