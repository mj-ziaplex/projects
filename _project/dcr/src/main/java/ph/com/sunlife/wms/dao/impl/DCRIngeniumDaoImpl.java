/**
 * 
 */
package ph.com.sunlife.wms.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.transaction.annotation.Transactional;

import ph.com.sunlife.wms.dao.DCRIngeniumDao;
import ph.com.sunlife.wms.dao.domain.SessionTotal;
import ph.com.sunlife.wms.dao.exceptions.WMSDaoException;

/**
 * @author i176
 *
 */
public class DCRIngeniumDaoImpl extends JdbcDaoSupport implements DCRIngeniumDao {

	private static final Logger LOGGER = Logger.getLogger(DCRIngeniumDaoImpl.class);
	
	@Override
	@Transactional
	public int insertDCRIngeniumSessionTotal(SessionTotal sessTotal)
			throws WMSDaoException {
		int rec = 0;
		String sql = "INSERT INTO [DCRIngeniumSessionTotal] "
				+ "(company,currency,amount,acf2id,process_date,center_code) "
				+ " VALUES (?, ?, ?, ?, ?, ?)";

		rec = getJdbcTemplate().update(
				sql,
				new Object[] { sessTotal.getCompany(), sessTotal.getCurrency(),
						sessTotal.getCollectionTotal(), sessTotal.getUserId(),
						sessTotal.getProcessDate(), sessTotal.getCenterCode() });
		try {
			if(rec > 0){
				getJdbcTemplate().getDataSource().getConnection().commit();
			}
		} catch (SQLException e) {
			LOGGER.error(e);
		}
		return rec;

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SessionTotal> getDCRIngeniumSessionTotal(SessionTotal sessTotal)
			throws WMSDaoException {
	       String sql = "SELECT * FROM DCRIngeniumSessionTotal where company='"+sessTotal.getCompany()+ "' and process_date='"+(new java.sql.Date(sessTotal.getProcessDate().getTime()))+"' and acf2id='"+sessTotal.getUserId()+"' and currency='"+sessTotal.getCurrency()+"' and center_code='"+sessTotal.getCenterCode()+"' ";
	        return getJdbcTemplate().query(sql, new DCRIngeniumSessionTotalRowMapper()); 
	}
	
	private static final class DCRIngeniumSessionTotalRowMapper implements RowMapper {
		@Override
		public SessionTotal mapRow(ResultSet resultSet, int rowNumber)
				throws SQLException {
			SessionTotal value = new SessionTotal();
			int idx = 1;
			value.setCompany(resultSet.getString(++idx));
			value.setCurrency(resultSet.getString(++idx));
			value.setCollectionTotal(resultSet.getDouble(++idx));
			value.setUserId(resultSet.getString(++idx));
			value.setProcessDate(resultSet.getDate(++idx));
			value.setCenterCode(resultSet.getString(++idx));
			return value;
		}
	}

	@Override
	@Transactional
	public int deleteDCRIngeniumSessionTotal(String ccId, Date processDate)
			throws WMSDaoException {
		int rec = 0;
		String sql = "DELETE FROM [DCRIngeniumSessionTotal] where center_code=? and process_date=?";
		rec = getJdbcTemplate().update(
				sql,
				new Object[] { ccId,  (new java.sql.Date(processDate.getTime()))});
		
		try {
			if(rec > 0){
				getJdbcTemplate().getDataSource().getConnection().commit();
			}
		} catch (SQLException e) {
			LOGGER.error(e);
		}
		
		return rec;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SessionTotal> getDCRIngeniumSessionTotal(String centerCode,
			Date processDate) throws WMSDaoException {
	       String sql = "SELECT * FROM DCRIngeniumSessionTotal where process_date='"+(new java.sql.Date(processDate.getTime()))+"' and center_code='"+centerCode+"' ";
	        return getJdbcTemplate().query(sql, new DCRIngeniumSessionTotalRowMapper());
	}

}
