package ph.com.sunlife.wms.web.controller.form;

/**
 * The From object for reversal entries. This can be reused for Adding, Updating
 * and Deleting reversals.
 * 
 * @author Zainal Limpao
 * 
 */
// Change according to Healthcheck
// *Problem was declared as public/protected in BinderAware
// *Change BinderAware interface class to abstract class and extends it rather than implements
public class ReversalForm extends CashierWorkItemForm {

	private Long dcrBalancingToolProductId;

	private double amount;

	private String remarks;

	private Long currencyId;

	private Long dcrReversalId;

	public Long getDcrReversalId() {
		return dcrReversalId;
	}

	public void setDcrReversalId(Long dcrReversalId) {
		this.dcrReversalId = dcrReversalId;
	}

	public Long getDcrBalancingToolProductId() {
		return dcrBalancingToolProductId;
	}

	public double getAmount() {
		return amount;
	}

	public String getRemarks() {
		return remarks;
	}

	public Long getCurrencyId() {
		return currencyId;
	}

	public void setCurrencyId(Long currencyId) {
		this.currencyId = currencyId;
	}

	public void setDcrBalancingToolProductId(Long dcrBalancingToolProductId) {
		this.dcrBalancingToolProductId = dcrBalancingToolProductId;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

    // Change according to Healthcheck
    // *Problem was declared as public/protected in BinderAware
    // *Change use BinderAware getter instead of direct access
    public String[] getRequiredFields() {
        return super.getRequiredFields();
    }
}
