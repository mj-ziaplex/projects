package ph.com.sunlife.wms.dao.exceptions;

/**
 * The encapsulated {@linK Exception} specific for Data-Access-Object related
 * problems.
 * 
 * @author Zainal Limpao
 * 
 */
public class WMSDaoException extends Exception {

	private static final long serialVersionUID = -6034671855805440607L;

	/**
	 * Null constructor.
	 */
	public WMSDaoException() {
	}

	/**
	 * Constructor with message.
	 * 
	 * @param message
	 */
	public WMSDaoException(String message) {
		super(message);
	}

	/**
	 * Constructor with throwable object.
	 * F
	 * @param throwable
	 */
	public WMSDaoException(Throwable throwable) {
		super(throwable);
	}

	/**
	 * Constructor with message and throwable object.
	 * 
	 * @param message
	 * @param throwable
	 */
	public WMSDaoException(String message, Throwable throwable) {
		super(message, throwable);
	}
}
