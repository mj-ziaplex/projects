package ph.com.sunlife.wms.dao.impl;

import java.sql.SQLException;
import java.util.List;

import org.springframework.orm.ibatis.SqlMapClientCallback;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.ibatis.sqlmap.client.SqlMapExecutor;


import ph.com.sunlife.wms.dao.DCRSearchDao;
import ph.com.sunlife.wms.dao.domain.CustomerCenter;
import ph.com.sunlife.wms.dao.domain.Hub;
import ph.com.sunlife.wms.dao.exceptions.WMSDaoException;

public class DCRSearchDaoImpl extends
	SqlMapClientDaoSupport implements DCRSearchDao {
	
	private static final String NAMESPACE = "DCRSearch";

	@SuppressWarnings("unchecked")
	@Override
	public List<CustomerCenter> getCCByHubUserCC(final String ccId) throws WMSDaoException {
		List<CustomerCenter> list = null;

		try {
			list = (List<CustomerCenter>) getSqlMapClientTemplate().execute(
					new SqlMapClientCallback() {

						@Override
						public Object doInSqlMapClient(SqlMapExecutor executor)
								throws SQLException {
							return executor.queryForList(NAMESPACE
									+ ".getCCByHubUserCC", ccId);
						}
					});
		} catch (Exception e) {
			throw new WMSDaoException(e);
		}
		return list;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Hub> getCCHubs() throws WMSDaoException {
		List<Hub> list = null;
		
		try{
			list = (List<Hub>) getSqlMapClientTemplate().execute(
					new SqlMapClientCallback() {

						@Override
						public Object doInSqlMapClient(SqlMapExecutor executor)
								throws SQLException {
							return executor.queryForList(NAMESPACE
									+ ".getCCHubs");
						}
					});
		} catch (Exception e){
			throw new WMSDaoException(e);
		}
		return list;
	}

    @SuppressWarnings("unchecked")
    @Override
    public List<CustomerCenter> getCCByHub(final String hubId) throws WMSDaoException {
        List<CustomerCenter> list = null;

        try {
            list = (List<CustomerCenter>) getSqlMapClientTemplate().execute(new SqlMapClientCallback() {
                @Override
                public Object doInSqlMapClient(SqlMapExecutor executor) throws SQLException {
                    return executor.queryForList(NAMESPACE + ".getCCByHub", hubId);
                }
            });
        } catch (Exception e) {
            throw new WMSDaoException(e);
        }
        return list;
    }
	
	

}
