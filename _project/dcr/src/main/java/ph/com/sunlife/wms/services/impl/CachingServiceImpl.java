package ph.com.sunlife.wms.services.impl;

import java.util.Calendar;
import java.util.Date;
import java.util.Map;

import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;

import ph.com.sunlife.wms.dao.DCRCachedPropertyDao;
import ph.com.sunlife.wms.dao.domain.DCRCachedProperty;
import ph.com.sunlife.wms.dao.exceptions.WMSDaoException;
import ph.com.sunlife.wms.services.CachingService;
import ph.com.sunlife.wms.services.exception.ServiceException;
import ph.com.sunlife.wms.util.WMSDateUtil;

/**
 * The Implementing Class for {@link CachingService}.
 * 
 * @author Zainal Limpao
 * 
 */
public class CachingServiceImpl implements CachingService {

	private static final Logger LOGGER = Logger
			.getLogger(CachingServiceImpl.class);

	private String wmsDcrSystemDateKey;

	private DCRCachedPropertyDao dcrCachedPropertyDao;

	private boolean useSystemDate = false;

	/*
	 * (non-Javadoc)
	 * 
	 * @see ph.com.sunlife.wms.services.CachingService#getWmsDcrSystemDate()
	 */
	@Override
	public Date getWmsDcrSystemDate() {
		Date defaultSystemDate = new Date();

		if (!useSystemDate) {
			try {
				LOGGER.info("Retrieving WMS DCR System Date from Cache...");

				Map<String, Object> results = dcrCachedPropertyDao
						.getAllCachedProperties();

				DCRCachedProperty wmsDcrSystemDateCachedProp = (DCRCachedProperty) results
						.get(wmsDcrSystemDateKey);
				String dateStr = wmsDcrSystemDateCachedProp.getPropValue();

				if (LOGGER.isDebugEnabled()) {
					LOGGER.debug("WMS DCR System Date is: " + dateStr);
				}
				
				Calendar cal = Calendar.getInstance();
				cal.setTime(defaultSystemDate);
				
				int hour = cal.get(Calendar.HOUR_OF_DAY);
				int minute = cal.get(Calendar.MINUTE);
				int second = cal.get(Calendar.SECOND);
				
				Date cachedSystemDate = WMSDateUtil.toDate(dateStr);
				cachedSystemDate = DateUtils.addHours(cachedSystemDate, hour);
				cachedSystemDate = DateUtils.addMinutes(cachedSystemDate, minute);
				cachedSystemDate = DateUtils.addSeconds(cachedSystemDate, second);
				
				return cachedSystemDate;
			} catch (WMSDaoException ex) {
				LOGGER.error(ex);
				LOGGER.debug("Returning default System Date");
				return defaultSystemDate;
			} catch (Exception ex) {
				LOGGER.error(ex);
				LOGGER.debug("Returning default System Date");
				return defaultSystemDate;
			}
		}

		LOGGER.info("Retrieving WMS DCR System Date from Actual Server Date...");
		return defaultSystemDate;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ph.com.sunlife.wms.services.CachingService#incrementWmsDcrSystemDate()
	 */
	@Override
	public boolean incrementWmsDcrSystemDate() throws ServiceException {
		// Date currentDcrSystemDate = this.getWmsDcrSystemDate();
		Date currentDcrSystemDate = new Date();
		try {
			boolean isIncrementedSuccessfully = dcrCachedPropertyDao
					.incrementWmsDcrSystemDate(wmsDcrSystemDateKey,
							currentDcrSystemDate);

			if (isIncrementedSuccessfully) {
				LOGGER.info("WMS DCR System Date has been incremented successfully");
			} else {
				LOGGER.info("Failed to increment WMS DCR System Date");
			}

			return isIncrementedSuccessfully;
		} catch (WMSDaoException ex) {
			LOGGER.error(ex);
			throw new ServiceException(ex);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ph.com.sunlife.wms.services.CachingService#resetCachedValues()
	 */
	@Override
	public void resetCachedValues() throws ServiceException {
		LOGGER.info("Values of Cache is being refreshed...");
		dcrCachedPropertyDao.reset();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ph.com.sunlife.wms.services.CachingService#getCachedValue(java.lang.String
	 * )
	 */
	@Override
	public String getCachedValue(String propertyKey) throws ServiceException {
		try {
			LOGGER.info("Retrieving a Value from Cache...");

			Map<String, Object> results = dcrCachedPropertyDao
					.getAllCachedProperties();

			DCRCachedProperty property = (DCRCachedProperty) results
					.get(propertyKey);

			String propValue = null;
			if (property != null) {
				propValue = property.getPropValue();
				return (String) propValue;
			}

			return null;
		} catch (WMSDaoException ex) {
			LOGGER.error(ex);
			throw new ServiceException(ex);
		} catch (Exception ex) {
			LOGGER.error(ex);
			throw new ServiceException(ex);
		}
	}
	@Override
	public boolean updateProperty(final String prop_key,final String prop_value, final String lastUpdatedUser, final Long lastUpdatedDate) throws ServiceException {
		boolean isUpdated = false;
		try {
			LOGGER.info("Update a Value from Cache...");

			isUpdated = dcrCachedPropertyDao.updateProperty(prop_key, prop_value, lastUpdatedUser, lastUpdatedDate);

		} catch (WMSDaoException ex) {
			LOGGER.error(ex);
			throw new ServiceException(ex);
		} 
		return isUpdated;
	}

	public void setDcrCachedPropertyDao(
			DCRCachedPropertyDao dcrCachedPropertyDao) {
		this.dcrCachedPropertyDao = dcrCachedPropertyDao;
	}

	public void setWmsDcrSystemDateKey(String wmsDcrSystemDateKey) {
		this.wmsDcrSystemDateKey = wmsDcrSystemDateKey;
	}

	public void setUseSystemDate(boolean useSystemDate) {
		this.useSystemDate = useSystemDate;
	}

}
