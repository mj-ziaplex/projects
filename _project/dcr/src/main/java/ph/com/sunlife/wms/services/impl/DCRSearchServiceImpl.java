package ph.com.sunlife.wms.services.impl;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;

import ph.com.sunlife.wms.dao.CashierDao;
import ph.com.sunlife.wms.dao.DCRDao;
import ph.com.sunlife.wms.dao.DCRSearchDao;
import ph.com.sunlife.wms.dao.domain.CustomerCenter;
import ph.com.sunlife.wms.dao.domain.DCR;
import ph.com.sunlife.wms.dao.domain.Hub;
import ph.com.sunlife.wms.dao.exceptions.WMSDaoException;
import ph.com.sunlife.wms.services.DCRSearchService;
import ph.com.sunlife.wms.services.bo.CustomerCenterBO;
import ph.com.sunlife.wms.services.bo.DCRBO;
import ph.com.sunlife.wms.services.bo.HubBO;
import ph.com.sunlife.wms.services.exception.ServiceException;

import static ph.com.sunlife.wms.util.WMSDateUtil.getRCD;

public class DCRSearchServiceImpl implements DCRSearchService {

	private DCRSearchDao dcrSearchDao;

	private DCRDao dcrDao;

	private CashierDao cashierDao;

	private static final String STANDARD_DATE_FORMAT_STR = "ddMMMyyyy";

	private static final DateFormat STANDARD_DATE_FORMAT = new SimpleDateFormat(
			STANDARD_DATE_FORMAT_STR);

	private static final Logger LOGGER = Logger
			.getLogger(DCRSearchServiceImpl.class);

	@Override
	public List<CustomerCenterBO> getCCByHubUserCC(String ccId)
			throws ServiceException {
		Set<CustomerCenterBO> resultSet = new HashSet<CustomerCenterBO>();

		try {
			List<CustomerCenter> customerCenters = dcrSearchDao
					.getCCByHubUserCC(ccId);
			if (CollectionUtils.isNotEmpty(customerCenters)) {
				for (CustomerCenter cc : customerCenters) {
					CustomerCenterBO ccBo = new CustomerCenterBO();
					this.convertCCToBusinessObject(ccBo, cc);
					resultSet.add(ccBo);
				}
			}
		} catch (WMSDaoException e) {
			throw new ServiceException(e);
		}

		if (CollectionUtils.isNotEmpty(resultSet)) {
			List<CustomerCenterBO> resultList = new ArrayList<CustomerCenterBO>(
					resultSet);
			Collections.sort(resultList);
			return resultList;
		}

		return null;
	}

	private void convertCCToBusinessObject(CustomerCenterBO customerCenterBO,
			CustomerCenter customerCenter) {
		customerCenterBO.setCcId(customerCenter.getCcId());
		customerCenterBO.setCcCode(customerCenter.getCcCode());
		customerCenterBO.setCcName(customerCenter.getCcName());
	}

	@Override
	public List<HubBO> getCCHubs() throws ServiceException {
		List<HubBO> resultList = new ArrayList<HubBO>();

		try {
			List<Hub> hubs = dcrSearchDao.getCCHubs();
			if (CollectionUtils.isNotEmpty(hubs)) {
				for (Hub h : hubs) {
					HubBO hubBO = new HubBO();
					this.convertHubToBusinessObject(hubBO, h);
					resultList.add(hubBO);
				}
			}
		} catch (WMSDaoException e) {
			throw new ServiceException(e);
		}
		return resultList;
	}

	private void convertHubToBusinessObject(HubBO hubBO, Hub hub) {
		hubBO.setHubId(hub.getHubId());
		hubBO.setHubName(hub.getHubName());
	}

	public DCRSearchDao getDcrSearchDao() {
		return dcrSearchDao;
	}

	public void setDcrSearchDao(DCRSearchDao dcrSearchDao) {
		this.dcrSearchDao = dcrSearchDao;
	}

	@Override
	public List<DCRBO> getDCRByCCAndDate(DCRBO dcrBO) throws ServiceException {
		List<DCRBO> resultList = new ArrayList<DCRBO>();

		try {
			DCR dcr = new DCR();
			dcr.setCcId(dcrBO.getCcId());
			dcr.setCcIdSet(dcrBO.getCcIdSet());
			dcr.setDcrStartDate(dcrBO.getDcrStartDate());
			dcr.setDcrEndDate(dcrBO.getDcrEndDate());
			List<DCR> dcrs = dcrDao.getDCRByCCAndDate(dcr);
			if (CollectionUtils.isNotEmpty(dcrs)) {
				for (DCR d : dcrs) {
					DCRBO resultDCRBO = new DCRBO();
					this.convertDCRToBusinessObject(resultDCRBO, d, false);
					resultList.add(resultDCRBO);
				}
			}
		} catch (WMSDaoException e) {
			throw new ServiceException(e);
		}
		return resultList;
	}

	public String getCCNameById(String siteCode) throws ServiceException {
		try {
			return cashierDao.getCCNameById(siteCode);
		} catch (WMSDaoException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public List<DCRBO> getDCRByHubAndDate(DCRBO dcrBO) throws ServiceException {
		List<DCRBO> resultList = new ArrayList<DCRBO>();

		try {
			DCR dcr = new DCR();
			dcr.setHubId(dcrBO.getHubId());
			dcr.setDcrStartDate(dcrBO.getDcrStartDate());
			dcr.setDcrEndDate(dcrBO.getDcrEndDate());
			List<DCR> dcrs = dcrDao.getDCRByHubAndDate(dcr);
			if (CollectionUtils.isNotEmpty(dcrs)) {
				for (DCR d : dcrs) {
					DCRBO resultDCRBO = new DCRBO();
					this.convertDCRToBusinessObject(resultDCRBO, d, false);
					resultList.add(resultDCRBO);
				}
			}
		} catch (WMSDaoException e) {
			throw new ServiceException(e);
		}
		return resultList;
	}

	@Override
	public List<DCRBO> getAllDCRForManager(String hubId)
			throws ServiceException {
		List<DCRBO> resultList = new ArrayList<DCRBO>();

		try {
			List<DCR> dcrs = dcrDao.getAllDCRForManager(hubId);
			if (CollectionUtils.isNotEmpty(dcrs)) {
				for (DCR d : dcrs) {
					DCRBO resultDCRBO = new DCRBO();
					this.convertDCRToBusinessObject(resultDCRBO, d, false);
					resultList.add(resultDCRBO);
				}
			}
		} catch (WMSDaoException e) {
			throw new ServiceException(e);
		}
		return resultList;
	}

	@Override
	public List<DCRBO> getDCRManagerByCCAndDate(DCRBO dcrBO, boolean isPpa)
			throws ServiceException {
		List<DCRBO> resultList = new ArrayList<DCRBO>();

		try {
			DCR dcr = new DCR();
			dcr.setCcId(dcrBO.getCcId());
			dcr.setCcIdSet(dcrBO.getCcIdSet());
			dcr.setDcrStartDate(dcrBO.getDcrStartDate());
			dcr.setDcrEndDate(dcrBO.getDcrEndDate());
			List<DCR> dcrs = dcrDao.getDCRManagerByCCAndDate(dcr);
			if (CollectionUtils.isNotEmpty(dcrs)) {
				for (DCR d : dcrs) {
					DCRBO resultDCRBO = new DCRBO();
					this.convertDCRToBusinessObject(resultDCRBO, d , isPpa);
					resultList.add(resultDCRBO);
				}
			}
		} catch (WMSDaoException e) {
			throw new ServiceException(e);
		}
		return resultList;
	}

	@Override
	public List<DCRBO> getDCRManagerByCC(List<String> ccIds)
			throws ServiceException {
		List<DCRBO> resultList = new ArrayList<DCRBO>();

		try {
			List<DCR> dcrs = dcrDao.getDCRManagerByCC(ccIds);
			if (CollectionUtils.isNotEmpty(dcrs)) {
				for (DCR d : dcrs) {
					DCRBO resultDCRBO = new DCRBO();
					this.convertDCRToBusinessObject(resultDCRBO, d, false);
					resultList.add(resultDCRBO);
				}
			}
		} catch (WMSDaoException e) {
			throw new ServiceException(e);
		}
		return resultList;
	}

	public void setDcrDao(DCRDao dcrDao) {
		this.dcrDao = dcrDao;
	}
	
	private void convertDCRToBusinessObject(DCRBO dcrBO, DCR dcr, boolean isPpa)
			throws ServiceException {
		Date requiredCompletionDate = dcr.getRequiredCompletionDate();
		Date dcrDate = dcr.getDcrDate();

		dcrBO.setRequiredCompletionDate(requiredCompletionDate);
		dcrBO.setCcId(dcr.getCcId());
		dcrBO.setCcName(dcr.getCcName());
		dcrBO.setCenterCode(dcr.getCenterCode());
		dcrBO.setStatus(dcr.getStatus());
		String formattedDateStr = StringUtils.upperCase(STANDARD_DATE_FORMAT
				.format(dcrDate));
		dcrBO.setFormatedDateStr(formattedDateStr);
		dcrBO.setId(dcr.getId());
		dcrBO.setUpdateDate(dcr.getUpdateDate());
		dcrBO.setUpdateUserId(dcr.getUpdateUserId());
		dcrBO.setCreateDate(dcr.getCreateDate());
		dcrBO.setCreateUserId(dcr.getCreateUserId());
		//Added for and MR-WF-16-00112
		if(isPpa){
			dcrBO.setUpdateDate(dcr.getCcqaSubmittedDate());
			dcrBO.setRequiredCompletionDate(dcr.getCcqaRequiredCompletionDate());
		}
	}

    @Override
    public List<CustomerCenterBO> getCCByHub(String hubId) throws ServiceException {
        Set<CustomerCenterBO> resultSet = new HashSet<CustomerCenterBO>();

        try {
            List<CustomerCenter> customerCenters = dcrSearchDao.getCCByHub(hubId);
            if (CollectionUtils.isNotEmpty(customerCenters)) {
                for (CustomerCenter cc : customerCenters) {
                    CustomerCenterBO ccBo = new CustomerCenterBO();
                    this.convertCCToBusinessObject(ccBo, cc);
                    resultSet.add(ccBo);
                }
            }

            if (CollectionUtils.isNotEmpty(resultSet)) {
                List<CustomerCenterBO> resultList = new ArrayList<CustomerCenterBO>(resultSet);
                Collections.sort(resultList);
                return resultList;
            }
        } catch (WMSDaoException e) {
            LOGGER.error(e);
            throw new ServiceException(e);
        }

        return null;
    }

	public CashierDao getCashierDao() {
		return cashierDao;
	}

    public void setCashierDao(CashierDao cashierDao) {
        this.cashierDao = cashierDao;
    }
    
    /*
     * Added for MR-WF-16-00034 - Random sampling for QA
     */
    @Override
    public List<DCRBO> getDCRQaByCC(List<String> ccIds) throws ServiceException {
        LOGGER.info("Start getting QA WI's by CC only...");
        
        List<DCRBO> resultList = new ArrayList<DCRBO>();
        try {
            List<DCR> dcrs = dcrDao.getDCRQaByCC(ccIds);
            
            if (CollectionUtils.isNotEmpty(dcrs)) {
                for (DCR d : dcrs) {
                    Date tmp = getRCD(d.getRequiredCompletionDate());
                    d.setRequiredCompletionDate(tmp);
                    
                    DCRBO resultDCRBO = new DCRBO();
                    this.convertDCRToBusinessObject(resultDCRBO, d , false);
                    resultList.add(resultDCRBO);
                }
            }
        } catch (WMSDaoException e) {
            LOGGER.error("Error encountered in getDCRQaByCC Service: ", e);
            throw new ServiceException(e);
        }
        
        LOGGER.info("End getting QA WI's by CC only...");
        return resultList;
    }

    /*
     * Added for MR-WF-16-00034 - Random sampling for QA
     */
    public List<DCRBO> getDCRQa(DCRBO dcrBO) throws ServiceException {
        LOGGER.info("Start getting QA WI's with params...");
        
        List<DCRBO> resultList = new ArrayList<DCRBO>();
        try {
            DCR dcr = new DCR();
            dcr.setCcId(dcrBO.getCcId());
            dcr.setCcIdSet(dcrBO.getCcIdSet());
            dcr.setDcrStartDate(dcrBO.getDcrStartDate());
            dcr.setDcrEndDate(dcrBO.getDcrEndDate());
            List<DCR> dcrs = dcrDao.getDCPpaQa(dcr);

            if (CollectionUtils.isNotEmpty(dcrs)) {
                for (DCR d : dcrs) {
                    Date tmp = getRCD(d.getRequiredCompletionDate());
                    d.setRequiredCompletionDate(tmp);
                    
                    DCRBO resultDCRBO = new DCRBO();
                    this.convertDCRToBusinessObject(resultDCRBO, d, false);
                    resultList.add(resultDCRBO);
                }
            }
        } catch (WMSDaoException e) {
            LOGGER.error("Error encountered in getDCRQa Service: ", e);
            throw new ServiceException(e);
        }

        LOGGER.info("End getting QA WI's with params...");
        return resultList;
    }
}
