package ph.com.sunlife.wms.services.bo;

import java.util.Date;
import java.util.List;

public class DCRBalancingToolBO {

	private Long id;
	
	private DCRCashierBO dcrCashier = new DCRCashierBO();
	
	private Date dateConfirmed;
	
	private Date dateReConfirmed;
	
	private byte[] snapShotDay1;
	
	private byte[] snapShotDay2;
	
	private Date lastRetrievedSessionTotal;
	
	private String htmlCode;
	
	private CompanyBO company;
	
	private List<DCRBalancingToolProductBO> products;
	
	private String ccId;
	
	private DCROtherBO dcrOtherBO;
	
	public DCROtherBO getDcrOtherBO() {
		return dcrOtherBO;
	}

	public void setDcrOtherBO(DCROtherBO dcrOtherBO) {
		this.dcrOtherBO = dcrOtherBO;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public DCRCashierBO getDcrCashier() {
		return dcrCashier;
	}

	public void setDcrCashier(DCRCashierBO dcrCashier) {
		this.dcrCashier = dcrCashier;
	}
	
	public void setDcrCashier(Long dcrCashierId) {
		DCRCashierBO dcrCashier = new DCRCashierBO();
		dcrCashier.setId(dcrCashierId);
		this.setDcrCashier(dcrCashier);
	}

	public Date getDateConfirmed() {
		return dateConfirmed;
	}

	public void setDateConfirmed(Date dateConfirmed) {
		this.dateConfirmed = dateConfirmed;
	}

	public Date getDateReConfirmed() {
		return dateReConfirmed;
	}

	public void setDateReConfirmed(Date dateReConfirmed) {
		this.dateReConfirmed = dateReConfirmed;
	}

	public byte[] getSnapShotDay1() {
		return snapShotDay1;
	}

	public void setSnapShotDay1(byte[] snapShotDay1) {
		this.snapShotDay1 = snapShotDay1;
	}

	public byte[] getSnapShotDay2() {
		return snapShotDay2;
	}

	public void setSnapShotDay2(byte[] snapShotDay2) {
		this.snapShotDay2 = snapShotDay2;
	}

	public Date getLastRetrievedSessionTotal() {
		return lastRetrievedSessionTotal;
	}

	public void setLastRetrievedSessionTotal(Date lastRetrievedSessionTotal) {
		this.lastRetrievedSessionTotal = lastRetrievedSessionTotal;
	}

	public CompanyBO getCompany() {
		return company;
	}

	public void setCompany(CompanyBO company) {
		this.company = company;
	}
	
	public void setCompany(Long companyId) {
		this.setCompany(CompanyBO.getCompany(companyId));
	}

	public List<DCRBalancingToolProductBO> getProducts() {
		return products;
	}

	public void setProducts(List<DCRBalancingToolProductBO> products) {
		this.products = products;
	}
	

	public String getHtmlCode() {
		return htmlCode;
	}

	public void setHtmlCode(String htmlCode) {
		this.htmlCode = htmlCode;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DCRBalancingToolBO other = (DCRBalancingToolBO) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public String getCcId() {
		return ccId;
	}

	public void setCcId(String ccId) {
		this.ccId = ccId;
	}

}
