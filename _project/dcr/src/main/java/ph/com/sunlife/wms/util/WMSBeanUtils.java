package ph.com.sunlife.wms.util;

import java.beans.PropertyDescriptor;
import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

/**
 * The custom WMS Bean Util class.
 *
 * @author Zainal Limpao
 *
 */
public final class WMSBeanUtils {

    private static final Logger LOGGER = Logger.getLogger(WMSBeanUtils.class);

    private WMSBeanUtils() {
    }

    /**
     * Copies fieldname-fieldvalue of an object source to a map as key-value
     * entries.
     *
     * @param objectSource
     * @param map
     */
    public static void copyToMap(Object objectSource, Map<String, Object> map) {
        List<String> names;
        names = listNestedPropertyName(objectSource);
        for (String name : names) {
            Object value = null;
            try {
                value = PropertyUtils.getNestedProperty(objectSource, name);
            } catch (Exception e) {
                // Change according to Healthcheck
                // *Problem was identified as no message on catch block
                // *Change was to add log message on catch block
                LOGGER.error("Error encountered trying to copy: ", e);
            }

            if (value != null) {
                map.put(name, value);
            }
        }
    }

    public static List<String> listNestedPropertyName(Object objectSource) {
        List<String> nodeNameList = new ArrayList<String>();

        if (Serializable.class.isAssignableFrom(objectSource.getClass())) {
            nodeNameList.add(objectSource.toString());
            return nodeNameList;
        }

        PropertyDescriptor[] propertyDescriptors =
                PropertyUtils.getPropertyDescriptors(objectSource.getClass());

        for (PropertyDescriptor propertyDescriptor : propertyDescriptors) {
            
            Method method = propertyDescriptor.getReadMethod();
            if (propertyDescriptor.getReadMethod() == null) {
                continue;
            }

            if (method.getGenericParameterTypes().length > 0) {
                continue;
            }

            // Change according to Healthcheck
            // *Problem was identified as value being set as null in catch block
            // *Change was to initialize object as null and just log error on catch block
            Object value = null;
            try {
                value = method.invoke(objectSource);
            } catch (IllegalAccessException e) {
                LOGGER.error("Error encountered getting property name: ", e);
            } catch (IllegalArgumentException e) {
                LOGGER.error("Error encountered getting property name: ", e);
            } catch (InvocationTargetException e) {
                LOGGER.error("Error encountered getting property name: ", e);
            }

            if (value == null) {
                continue;
            }
            
            String name = propertyDescriptor.getName();
            if (Map.class.isAssignableFrom(value.getClass())) { // Mapped name
                Map map = ((Map) value);
                name = StringUtils.substringBeforeLast(name, "Map");

                for (Object key : map.keySet()) {
                    String mappedName = name + "(" + key.toString() + ")";
                    List<String> nestedNames = listNestedPropertyName(map.get(key));

                    for (String nestedName : nestedNames) {
                        nodeNameList.add(mappedName + "." + nestedName);
                    }
                }

            } else if (List.class.isAssignableFrom(value.getClass())) { // Indexed name
                List list = ((List) value);
                name = StringUtils.substringBeforeLast(name, "List");

                for (int i = 0; i < list.size(); i++) {
                    String indexedName = name + "[" + i + "]";
                    List<String> nestedNames = listNestedPropertyName(list.get(i));

                    for (String nestedName : nestedNames) {
                        nodeNameList.add(indexedName + "." + nestedName);
                    }
                }
            } else if (Serializable.class.isAssignableFrom(value.getClass())) { // Simple Value
                nodeNameList.add(name);
            } else { // Nested Value
                List<String> nestedNames = listNestedPropertyName(value);

                for (String nestedName : nestedNames) {
                    nodeNameList.add(name + "." + nestedName);
                }
            }
        }

        return nodeNameList;
    }
}
