package ph.com.sunlife.wms.services.bo;

public class DCRVDILPRSeriesNumberBO {

	private Long id;
	
	private DCRVDILBO dcrvdil;
	
	private Long companyId;
	
	private int from;
	
	private int to;
	
	private String reason;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public DCRVDILBO getDcrvdil() {
		return dcrvdil;
	}

	public void setDcrvdil(DCRVDILBO dcrvdil) {
		this.dcrvdil = dcrvdil;
	}
	
	public void setDcrvdil(Long dcrvdilId) {
		DCRVDILBO dcrvdil = new DCRVDILBO();
		dcrvdil.setId(dcrvdilId);
		this.setDcrvdil(dcrvdil);
	}

	public int getFrom() {
		return from;
	}

	public void setFrom(int from) {
		this.from = from;
	}

	public int getTo() {
		return to;
	}

	public void setTo(int to) {
		this.to = to;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}

}
