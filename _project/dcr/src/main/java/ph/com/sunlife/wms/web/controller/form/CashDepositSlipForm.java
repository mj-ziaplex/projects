package ph.com.sunlife.wms.web.controller.form;

import java.util.Date;

import org.apache.commons.lang.StringUtils;

import ph.com.sunlife.wms.dao.domain.Company;
import ph.com.sunlife.wms.dao.domain.Currency;
import ph.com.sunlife.wms.util.IpacUtil;
import ph.com.sunlife.wms.util.WMSDateUtil;

/**
 * The Form Object for Cash Deposit Slip.
 * 
 * @author Jess Mendoza
 * 
 */
public class CashDepositSlipForm extends CashierWorkItemForm {

	private String userId;

	private String depositoryBankId;

	private String customerCenter;
	
	private String customerCenterName;

	private String accountNumber;

	private Long currencyId;

	private Currency currency;

	private Long companyId;

	private Company company;

	private String companyStr;

	private String prodCode;

	private double requiredTotalAmount;

	private Date depositDate;

	private String depositDateStr;

	private Date dcrDate;

	private String dcrDateStr;

	private int denom1000;

	private int denom500;

	private int denom200;

	private int denom100;

	private int denom50;

	private int denom20;

	private int denom10;

	private int denom5;

	private int denom2;

	private int denom1;

	private int denom1Coin;

	private int denom50c;

	private int denom25c;

	private int denom10c;

	private int denom5c;

	private int denom1c;

	private double hiddenCashAmount1000;

	private double hiddenCashAmount500;

	private double hiddenCashAmount200;

	private double hiddenCashAmount100;

	private double hiddenCashAmount50;

	private double hiddenCashAmount20;

	private double hiddenCashAmount10;

	private double hiddenCashAmount5;

	private double hiddenCashAmount2;

	private double hiddenCashAmount1;

	private double hiddenCashAmount1Coin;

	private double hiddenCashAmount50c;

	private double hiddenCashAmount25c;

	private double hiddenCashAmount10c;

	private double hiddenCashAmount5c;

	private double hiddenCashAmount1c;

	private double hiddenTotalAmount;

	private double hiddenTotalAmountBills;
	
	private String acf2id;
	
	private String cashierName;
	
	private String barcodeStr;
	
	private double hiddenTradVulCollectionTotal;
	
	private double hiddenGrpLifeCollectionTotal;
	

	public String getDepositoryBankId() {
		return depositoryBankId;
	}

	public void setDepositoryBankId(String depositoryBankId) {
		this.depositoryBankId = depositoryBankId;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public double getRequiredTotalAmount() {
		return requiredTotalAmount;
	}

	public void setRequiredTotalAmount(double requiredTotalAmount) {
		this.requiredTotalAmount = requiredTotalAmount;
	}

	public int getDenom1000() {
		return denom1000;
	}

	public void setDenom1000(int denom1000) {
		this.denom1000 = denom1000;
	}

	public int getDenom500() {
		return denom500;
	}

	public void setDenom500(int denom500) {
		this.denom500 = denom500;
	}

	public int getDenom200() {
		return denom200;
	}

	public void setDenom200(int denom200) {
		this.denom200 = denom200;
	}

	public int getDenom100() {
		return denom100;
	}

	public void setDenom100(int denom100) {
		this.denom100 = denom100;
	}

	public int getDenom50() {
		return denom50;
	}

	public void setDenom50(int denom50) {
		this.denom50 = denom50;
	}

	public int getDenom20() {
		return denom20;
	}

	public void setDenom20(int denom20) {
		this.denom20 = denom20;
	}

	public int getDenom10() {
		return denom10;
	}

	public void setDenom10(int denom10) {
		this.denom10 = denom10;
	}

	public int getDenom5() {
		return denom5;
	}

	public void setDenom5(int denom5) {
		this.denom5 = denom5;
	}

	public void setDenom2(int denom2) {
		this.denom2 = denom2;
	}

	public int getDenom2() {
		return denom2;
	}

	public int getDenom1() {
		return denom1;
	}

	public void setDenom1(int denom1) {
		this.denom1 = denom1;
	}

	public void setDenom1Coin(int denom1Coin) {
		this.denom1Coin = denom1Coin;
	}

	public int getDenom1Coin() {
		return denom1Coin;
	}

	public void setDenom50c(int denom50c) {
		this.denom50c = denom50c;
	}

	public int getDenom50c() {
		return denom50c;
	}

	public int getDenom25c() {
		return denom25c;
	}

	public void setDenom25c(int denom25c) {
		this.denom25c = denom25c;
	}

	public int getDenom10c() {
		return denom10c;
	}

	public void setDenom10c(int denom10c) {
		this.denom10c = denom10c;
	}

	public int getDenom5c() {
		return denom5c;
	}

	public void setDenom5c(int denom5c) {
		this.denom5c = denom5c;
	}

	public Date getDepositDate() {
		return depositDate;
	}

	public void setDepositDate(Date depositDate) {
		this.depositDate = depositDate;
		this.depositDateStr = StringUtils.upperCase(WMSDateUtil
				.toFormattedDateStr(depositDate));
	}

	public void setDepositDateStr(String depositDateStr) {
		this.depositDateStr = StringUtils.upperCase(depositDateStr);
		this.depositDate = WMSDateUtil.toDate(depositDateStr);
	}

	public String getDepositDateStr() {
		return StringUtils.upperCase(depositDateStr);
	}

	public String getDcrDateStr() {
		return StringUtils.upperCase(dcrDateStr);
	}

	public void setDcrDateStr(String dcrDateStr) {
		this.dcrDateStr = StringUtils.upperCase(dcrDateStr);
		this.dcrDate = WMSDateUtil.toDate(dcrDateStr);
	}

	public Date getDcrDate() {
		return dcrDate;
	}

	public void setDcrDate(Date dcrDate) {
		this.dcrDate = dcrDate;
		this.dcrDateStr = StringUtils.upperCase(WMSDateUtil
				.toFormattedDateStr(dcrDate));
	}

	public int getDenom1c() {
		return denom1c;
	}

	public void setDenom1c(int denom1c) {
		this.denom1c = denom1c;
	}

	public double getHiddenCashAmount1000() {
		return hiddenCashAmount1000;
	}

	public void setHiddenCashAmount1000(double hiddenCashAmount1000) {
		this.hiddenCashAmount1000 = hiddenCashAmount1000;
	}

	public double getHiddenCashAmount500() {
		return hiddenCashAmount500;
	}

	public void setHiddenCashAmount500(double hiddenCashAmount500) {
		this.hiddenCashAmount500 = hiddenCashAmount500;
	}

	public double getHiddenCashAmount200() {
		return hiddenCashAmount200;
	}

	public void setHiddenCashAmount200(double hiddenCashAmount200) {
		this.hiddenCashAmount200 = hiddenCashAmount200;
	}

	public double getHiddenCashAmount100() {
		return hiddenCashAmount100;
	}

	public void setHiddenCashAmount100(double hiddenCashAmount100) {
		this.hiddenCashAmount100 = hiddenCashAmount100;
	}

	public double getHiddenCashAmount50() {
		return hiddenCashAmount50;
	}

	public void setHiddenCashAmount50(double hiddenCashAmount50) {
		this.hiddenCashAmount50 = hiddenCashAmount50;
	}

	public double getHiddenCashAmount20() {
		return hiddenCashAmount20;
	}

	public void setHiddenCashAmount20(double hiddenCashAmount20) {
		this.hiddenCashAmount20 = hiddenCashAmount20;
	}

	public double getHiddenCashAmount10() {
		return hiddenCashAmount10;
	}

	public void setHiddenCashAmount10(double hiddenCashAmount10) {
		this.hiddenCashAmount10 = hiddenCashAmount10;
	}

	public double getHiddenCashAmount5() {
		return hiddenCashAmount5;
	}

	public void setHiddenCashAmount5(double hiddenCashAmount5) {
		this.hiddenCashAmount5 = hiddenCashAmount5;
	}

	public void setHiddenCashAmount2(double hiddenCashAmount2) {
		this.hiddenCashAmount2 = hiddenCashAmount2;
	}

	public double getHiddenCashAmount2() {
		return hiddenCashAmount2;
	}

	public double getHiddenCashAmount1() {
		return hiddenCashAmount1;
	}

	public void setHiddenCashAmount1(double hiddenCashAmount1) {
		this.hiddenCashAmount1 = hiddenCashAmount1;
	}

	public void setHiddenCashAmount1Coin(double hiddenCashAmount1Coin) {
		this.hiddenCashAmount1Coin = hiddenCashAmount1Coin;
	}

	public double getHiddenCashAmount1Coin() {
		return hiddenCashAmount1Coin;
	}

	public void setHiddenCashAmount50c(double hiddenCashAmount50c) {
		this.hiddenCashAmount50c = hiddenCashAmount50c;
	}

	public double getHiddenCashAmount50c() {
		return hiddenCashAmount50c;
	}

	public double getHiddenCashAmount25c() {
		return hiddenCashAmount25c;
	}

	public void setHiddenCashAmount25c(double hiddenCashAmount25c) {
		this.hiddenCashAmount25c = hiddenCashAmount25c;
	}

	public double getHiddenCashAmount10c() {
		return hiddenCashAmount10c;
	}

	public void setHiddenCashAmount10c(double hiddenCashAmount10c) {
		this.hiddenCashAmount10c = hiddenCashAmount10c;
	}

	public double getHiddenCashAmount5c() {
		return hiddenCashAmount5c;
	}

	public void setHiddenCashAmount5c(double hiddenCashAmount5c) {
		this.hiddenCashAmount5c = hiddenCashAmount5c;
	}

	public void setHiddenCashAmount1c(double hiddenCashAmount1c) {
		this.hiddenCashAmount1c = hiddenCashAmount1c;
	}

	public double getHiddenCashAmount1c() {
		return hiddenCashAmount1c;
	}

	public double getHiddenTotalAmount() {
		return hiddenTotalAmount;
	}

	public void setHiddenTotalAmount(double hiddenTotalAmount) {
		this.hiddenTotalAmount = hiddenTotalAmount;
	}

	public void setCustomerCenter(String customerCenter) {
		this.customerCenter = customerCenter;
	}

	public String getCustomerCenter() {
		return customerCenter;
	}

	public void setHiddenTotalAmountBills(double hiddenTotalAmountBills) {
		this.hiddenTotalAmountBills = hiddenTotalAmountBills;
	}

	public double getHiddenTotalAmountBills() {
		return hiddenTotalAmountBills;
	}

	public void setCurrencyId(Long currencyId) {
		this.currencyId = currencyId;
	}

	public Long getCurrencyId() {
		return currencyId;
	}

	public void setCurrency(Currency currency) {
		this.currency = currency;
	}

	public Currency getCurrency() {
		return currency;
	}

	public Long getCompanyId() {
		return companyId;
	}

	public void setCompany(Company company) {
		this.company = company;
		this.companyId = company.getId();
		this.companyStr = IpacUtil.toIpacComCode(company.getId());

	}

	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
		this.company = Company.getCompany(companyId);
		this.companyStr = IpacUtil.toIpacComCode(companyId);

	}

	public Company getCompany() {
		return company;
	}

	public void setCompanyStr(String companyStr) {
		this.companyStr = companyStr;
	}

	public String getCompanyStr() {
		return companyStr;
	}

	public void setProdCode(String prodCode) {
		this.prodCode = prodCode;
	}

	public String getProdCode() {
		return prodCode;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getCustomerCenterName() {
		return customerCenterName;
	}

	public void setCustomerCenterName(String customerCenterName) {
		this.customerCenterName = customerCenterName;
	}

	public String getCashierName() {
		return cashierName;
	}

	public void setCashierName(String cashierName) {
		this.cashierName = cashierName;
	}

	public String getBarcodeStr() {
		return barcodeStr;
	}

	public void setBarcodeStr(String barcodeStr) {
		this.barcodeStr = barcodeStr;
	}

	public double getHiddenTradVulCollectionTotal() {
		return hiddenTradVulCollectionTotal;
	}

	public void setHiddenTradVulCollectionTotal(double hiddenTradVulCollectionTotal) {
		this.hiddenTradVulCollectionTotal = hiddenTradVulCollectionTotal;
	}

	public double getHiddenGrpLifeCollectionTotal() {
		return hiddenGrpLifeCollectionTotal;
	}

	public void setHiddenGrpLifeCollectionTotal(double hiddenGrpLifeCollectionTotal) {
		this.hiddenGrpLifeCollectionTotal = hiddenGrpLifeCollectionTotal;
	}

	public String getAcf2id() {
		return acf2id;
	}

	public void setAcf2id(String acf2id) {
		this.acf2id = acf2id;
	}

}
