package ph.com.sunlife.wms.services;

import java.util.List;

import ph.com.sunlife.wms.dao.domain.DCRError;
import ph.com.sunlife.wms.dao.exceptions.WMSDaoException;
import ph.com.sunlife.wms.services.bo.DCRErrorBO;
import ph.com.sunlife.wms.services.bo.DCRErrorTagLogBO;
import ph.com.sunlife.wms.services.bo.DCRErrorTagLogConsolidatedDisplayBO;
import ph.com.sunlife.wms.services.bo.DCRErrorTagLookupBO;
import ph.com.sunlife.wms.services.exception.ServiceException;

/**
 * The Interface responsible for manipulating Error Tags on the DCR Step
 * Processor.
 * 
 * @author Josephus Sardan
 * 
 */
public interface DCRErrorTagService {

	// display possible inputs
//	DCRErrorTagLookupBO getDCRErrorTagLookupDisplay(final Long dcrId)
//			throws ServiceException;

	// input bo to dao, insert into two tables
	DCRErrorTagLogBO addDCRErrorTagLog(DCRErrorTagLogBO bo)
			throws ServiceException;

	// display tagged errors
	// dapat isang object na lang i-return nito, but with
	// List<DCRCompletedStepBO>
	// List<DCRErrorTagLogDisplayBO> getDCRErrorTagLogDisplay(final Long dcrId)
	// throws ServiceException;

	List<DCRErrorTagLogConsolidatedDisplayBO> getDCRErrorTagLogDisplay(
			final Long dcrId) throws ServiceException;

	// TODO delete
	void tagErrorLogAsDeleted(String reasonDeleted, final Long dcrId,
			String stepId, String userId, String tagger) throws ServiceException;

	// TODO edit
	void editDCRErrorLogAndLookup(DCRErrorTagLogBO bo) throws ServiceException;
	
	List<DCRErrorBO> getDCRErrorByStatus(final String dcrStatus) throws ServiceException;
	
	String getStepIdByStatus(final String dcrStatus) throws ServiceException;
	
	String getManagerByDcrId(final Long dcrId) throws ServiceException;

	List<String> getUsersFromAuditLogByDcrId(String dcrId) throws ServiceException;
}
