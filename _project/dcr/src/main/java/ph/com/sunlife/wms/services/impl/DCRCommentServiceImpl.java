package ph.com.sunlife.wms.services.impl;

import static ph.com.sunlife.wms.util.WMSConstants.LINE_SEPARATOR;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import ph.com.sunlife.wms.dao.DCRCommentDao;
import ph.com.sunlife.wms.dao.domain.DCRComment;
import ph.com.sunlife.wms.dao.exceptions.WMSDaoException;
import ph.com.sunlife.wms.services.DCRCommentService;
import ph.com.sunlife.wms.services.bo.DCRCommentBO;
import ph.com.sunlife.wms.services.exception.ServiceException;

/**
 * The implementing class for {@link DCRCommentService}.
 * 
 * @author Josephus Sardan
 * @author Zainal Limpao
 * 
 */
public class DCRCommentServiceImpl implements DCRCommentService {

	private static final Logger LOGGER = Logger
			.getLogger(DCRCommentServiceImpl.class);

	private static final String DATE_FORMAT_STR = "ddMMMyyyy hh:mm a";

	private static final DateFormat DATE_FORMAT = new SimpleDateFormat(
			DATE_FORMAT_STR);

	private DCRCommentDao dcrCommentDao;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ph.com.sunlife.wms.services.DCRCommentService#save(ph.com.sunlife.wms
	 * .services.bo.DCRCommentBO)
	 */
	@Override
	public DCRCommentBO save(DCRCommentBO dcrComment) throws ServiceException {

		DCRComment entity = new DCRComment();
		this.convertToEntity(dcrComment, entity);

		try {
			entity = dcrCommentDao.save(entity);
			dcrComment.setId(entity.getId());
		} catch (WMSDaoException e) {
			throw new ServiceException(e);
		}

		return dcrComment;
	}

	/**
	 * Converts {@link DCRCommentBO} to {@link DCRComment} entity.
	 * 
	 * @param dcrComment
	 * @param entity
	 */
	private void convertToEntity(DCRCommentBO dcrComment, DCRComment entity) {
		entity.setAcf2id(dcrComment.getAcf2id());
		entity.setDatePosted(dcrComment.getDatePosted());
		entity.setDcrCashier(dcrComment.getDcrCashier().getId());
		entity.setDcrId(dcrComment.getDcrId());

		String remarksWithoutLineBreaks = dcrComment.getRemarks().replace(
				LINE_SEPARATOR, " ");

		if (remarksWithoutLineBreaks.length() > 350) {
			remarksWithoutLineBreaks = remarksWithoutLineBreaks.substring(0,
					350);
		}

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Remarks field is changed into: "
					+ remarksWithoutLineBreaks);
		}

		entity.setRemarks(remarksWithoutLineBreaks);
	}

	/**
	 * Converts {@link DCRComment} entity to {@link DCRCommentBO}.
	 * 
	 * @param dcrComment
	 * @param entity
	 */
	private void convertToBusinessObject(DCRCommentBO dcrComment,
			DCRComment entity) {
		dcrComment.setAcf2id(entity.getAcf2id());
		dcrComment.setDatePosted(entity.getDatePosted());
		dcrComment.setDcrCashier(entity.getDcrCashier().getId());
		dcrComment.setDcrId(entity.getDcrId());

		String remarksWithoutLineBreaks = entity.getRemarks().replace(
				LINE_SEPARATOR, " ");

		remarksWithoutLineBreaks = remarksWithoutLineBreaks.replace("\"",
				"\\\"");

		dcrComment.setRemarks(remarksWithoutLineBreaks);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ph.com.sunlife.wms.services.DCRCommentService#getById(java.lang.Long)
	 */
	@Override
	public DCRCommentBO getById(Long id) throws ServiceException {
		// Currently has no implementation
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ph.com.sunlife.wms.services.DCRCommentService#getAllComments(java.lang
	 * .Long)
	 */
	@Override
	public List<DCRCommentBO> getAllComments(Long dcrCashierId)
			throws ServiceException {
		List<DCRCommentBO> resultList = new ArrayList<DCRCommentBO>();

		try {
			List<DCRComment> comments = dcrCommentDao
					.getAllComments(dcrCashierId);

			if (CollectionUtils.isNotEmpty(comments)) {
				for (DCRComment comment : comments) {
					DCRCommentBO result = new DCRCommentBO();
					this.convertToBusinessObject(result, comment);
					result.setId(comment.getId());

					String datePostedFormatted = DATE_FORMAT.format(comment
							.getDatePosted());

					result.setDatePostedFormatted(StringUtils
							.upperCase(datePostedFormatted));

					resultList.add(result);
				}

			}

		} catch (WMSDaoException e) {
			throw new ServiceException(e);
		}

		return resultList;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ph.com.sunlife.wms.services.DCRCommentService#getAllCommentsDcrId(java
	 * .lang.Long)
	 */
	@Override
	public List<DCRCommentBO> getAllCommentsDcrId(Long dcrId)
			throws ServiceException {
		List<DCRCommentBO> resultList = new ArrayList<DCRCommentBO>();

		try {
			List<DCRComment> comments = dcrCommentDao
					.getAllCommentsByDcrId(dcrId);

			if (CollectionUtils.isNotEmpty(comments)) {
				for (DCRComment comment : comments) {
					DCRCommentBO result = new DCRCommentBO();
					this.convertToBusinessObject(result, comment);
					result.setId(comment.getId());

					String datePostedFormatted = DATE_FORMAT.format(comment
							.getDatePosted());

					result.setDatePostedFormatted(StringUtils
							.upperCase(datePostedFormatted));

					resultList.add(result);
				}

			}

		} catch (WMSDaoException e) {
			LOGGER.error(e);
			throw new ServiceException(e);
		}

		return resultList;
	}

	/**
	 * Injection Setter for {@link DCRCommentDao}.
	 * 
	 * @param dcrCommentDao
	 */
	public void setDcrCommentDao(DCRCommentDao dcrCommentDao) {
		this.dcrCommentDao = dcrCommentDao;
	}
}
