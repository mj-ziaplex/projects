package ph.com.sunlife.wms.web.controller.form;

import javax.servlet.http.HttpSession;

import org.springframework.validation.Errors;

import ph.com.sunlife.wms.web.controller.binder.BinderAware;
import ph.com.sunlife.wms.web.controller.binder.SessionAware;

// Change according to Healthcheck
// *Problem was declared as public/protected in BinderAware
// *Change BinderAware interface class to abstract class and extends it rather than implements
public class AddNoteSPForm extends BinderAware implements SessionAware {

	private HttpSession session;
	
	private String content;
	
	private String title;
	
	private Long dcrId;
	
	private boolean reloadSP;
	
	public boolean isReloadSP() {
		return reloadSP;
	}

	public void setReloadSP(boolean reloadSP) {
		this.reloadSP = reloadSP;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	@Override
	public HttpSession getSession() {
		return session;
	}

	@Override
	public void setSession(HttpSession session) {
		this.session = session;
	}

	@Override
	public String getSecretKey() {
		// TODO Auto-generated method stub
		return null;
	}

    // Change according to Healthcheck
    // *Problem was declared as public/protected in BinderAware
    // *Change use BinderAware getter instead of direct access
    public String[] getAllowedFields() {
        return super.getAllowedFields();
    }

    // Change according to Healthcheck
    // *Problem was declared as public/protected in BinderAware
    // *Change use BinderAware getter instead of direct access
    public String[] getRequiredFields() {
        return super.getRequiredFields();
    }

    // Change according to Healthcheck
    // *Problem was declared as public/protected in BinderAware
    // *Change use BinderAware getter instead of direct access
    public String[] getUncheckedFields() {
        return super.getUncheckedFields();
    }

	@Override
	public void afterBind() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean validate(Errors errors) {
		// TODO Auto-generated method stub
		return false;
	}

	public Long getDcrId() {
		return dcrId;
	}

	public void setDcrId(Long dcrId) {
		this.dcrId = dcrId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

}
