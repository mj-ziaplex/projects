package ph.com.sunlife.wms.services;

import java.util.List;

import ph.com.sunlife.wms.services.bo.BankBO;
import ph.com.sunlife.wms.services.bo.CustomerCenterBO;
import ph.com.sunlife.wms.services.bo.MixedChequePayeeBO;
import ph.com.sunlife.wms.services.exception.ServiceException;
import ph.com.sunlife.wms.util.ParameterMap;

public interface DCRMixedChequePayeeService {

	MixedChequePayeeBO createNewMixedChequePayee(
			MixedChequePayeeBO businessObject) throws ServiceException;

	boolean deleteMixedChequePayee(Long mixedChequePayeeId)
			throws ServiceException;

	boolean updateMixedChequePayee(MixedChequePayeeBO businessObject)
			throws ServiceException;

	List<MixedChequePayeeBO> getAllMixedChequePayees(Long dcrCashierId)
			throws ServiceException;

	List<BankBO> getAllBanks() throws ServiceException;

	CustomerCenterBO getDCRCC(String ccId) throws ServiceException;

	List<MixedChequePayeeBO> getMixedChequePayeeBOsForPrinting(
			Long dcrCashierId, final ParameterMap form) throws ServiceException;
}
