package ph.com.sunlife.wms.services;

import java.util.List;

import ph.com.sunlife.wms.services.bo.DCRReversalBO;
import ph.com.sunlife.wms.services.exception.ServiceException;

/**
 * The Service Layer responsible for persisting Reversal Entries.
 * 
 * @author Zainal Limpao
 */
public interface DCRReversalService {

	/**
	 * Retrieves a {@link List} of {@link DCRReversalBO} entries given the
	 * product id.
	 * 
	 * @param dcrBalancingToolProductId
	 * @return
	 * @throws ServiceException
	 */
	List<DCRReversalBO> getAllByBalancingToolProduct(
			Long dcrBalancingToolProductId) throws ServiceException;

	/**
	 * Deletes the Reversal Entry given the corresponding id.
	 * 
	 * @param id
	 * @return
	 * @throws ServiceException
	 */
	boolean deleteById(Long id) throws ServiceException;

	/**
	 * Updates the {@link DCRReversalBO} values. Note that only remarks and
	 * amount are updatable.
	 * 
	 * @param dcrReversal
	 * @throws ServiceException
	 */
	void updateReversal(DCRReversalBO dcrReversal) throws ServiceException;

	/**
	 * Saves the {@link DCRReversalBO} values into the database.
	 * 
	 * @param dcrReversal
	 * @return
	 * @throws ServiceException
	 */
	DCRReversalBO save(DCRReversalBO dcrReversal) throws ServiceException;

}
