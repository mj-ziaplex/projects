package ph.com.sunlife.wms.dao.impl;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.CallableStatementCallback;
import org.springframework.jdbc.core.CallableStatementCreator;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

import ph.com.sunlife.wms.dao.DCRCandidateMatchDao;
import ph.com.sunlife.wms.dao.exceptions.WMSDaoException;

public class DCRCandidateMatchDaoImpl extends JdbcDaoSupport implements DCRCandidateMatchDao {

	@Override
	public void deleteAttachment(final Long recordId, String recordLocation)
			throws WMSDaoException {
		
		StringBuilder sb = new StringBuilder();
		sb.append("delete from ");
		sb.append(recordLocation);
		sb.append(" where id = ?;");
		
		final String sql = sb.toString();
		
		try {
			getJdbcTemplate().execute(
					new CallableStatementCreator() {
						public CallableStatement createCallableStatement(
								Connection con) throws SQLException {
							CallableStatement cs = con.prepareCall(sql);
							cs.setLong(1, recordId);
							return cs;
						}
					}, new CallableStatementCallback() {

						@Override
						public Object doInCallableStatement(CallableStatement cs)
								throws SQLException, DataAccessException {
							cs.execute();							
							return null;
						}
					});
		} catch (Exception ex) {
			throw new WMSDaoException(ex);
		}

	}

}
