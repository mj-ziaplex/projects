package ph.com.sunlife.wms.dao.impl;

import java.sql.SQLException;
import java.util.List;

import org.springframework.orm.ibatis.SqlMapClientCallback;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import ph.com.sunlife.wms.dao.ContactDao;
import ph.com.sunlife.wms.dao.domain.Contact;
import ph.com.sunlife.wms.dao.exceptions.WMSDaoException;

import com.ibatis.sqlmap.client.SqlMapExecutor;

public class ContactDaoImpl extends SqlMapClientDaoSupport implements
		ContactDao {

	private static final String NAMESPACE = "Contact";

	@SuppressWarnings("unchecked")
	public List<Contact> getAllWMSUsers() throws WMSDaoException {

		List<Contact> list = null;
		try {
			list = (List<Contact>) getSqlMapClientTemplate().execute(
					new SqlMapClientCallback() {

						@Override
						public Object doInSqlMapClient(SqlMapExecutor executor)
								throws SQLException {
							return executor.queryForList(NAMESPACE
									+ ".getAllWMSUsers");
						}
					});
			return list;
		} catch (Exception e) {
			throw new WMSDaoException(e);
		}
	}

}
