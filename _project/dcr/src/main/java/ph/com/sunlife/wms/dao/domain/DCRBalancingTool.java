package ph.com.sunlife.wms.dao.domain;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class DCRBalancingTool extends Entity {

	private DCRCashier dcrCashier = new DCRCashier();

	private Date dateConfirmed;

	private Date dateReConfirmed;

	private String snapShotDay1Id;

	private String snapShotDay2Id;

	private byte[] snapShotDay1;

	private byte[] snapShotDay2;

	private Date lastRetrievedSessionTotal;

	private Company company;

	private Long companyId;

	private List<DCRBalancingToolProduct> products;

	public DCRCashier getDcrCashier() {
		return dcrCashier;
	}

	public void setDcrCashier(DCRCashier dcrCashier) {
		this.dcrCashier = dcrCashier;
	}

	public void setDcrCashier(Long dcrCashierId) {
		this.dcrCashier.setId(dcrCashierId);
		this.setDcrCashier(dcrCashier);
	}

	public Date getDateConfirmed() {
		return dateConfirmed;
	}

	public void setDateConfirmed(Date dateConfirmed) {
		this.dateConfirmed = dateConfirmed;
	}

	public Date getDateReConfirmed() {
		return dateReConfirmed;
	}

	public void setDateReConfirmed(Date dateReConfirmed) {
		this.dateReConfirmed = dateReConfirmed;
	}

	public byte[] getSnapShotDay1() {
		return snapShotDay1;
	}

	public void setSnapShotDay1(byte[] snapShotDay1) {
		this.snapShotDay1 = snapShotDay1;
	}

	public byte[] getSnapShotDay2() {
		return snapShotDay2;
	}

	public void setSnapShotDay2(byte[] snapShotDay2) {
		this.snapShotDay2 = snapShotDay2;
	}

	public Date getLastRetrievedSessionTotal() {
		return lastRetrievedSessionTotal;
	}

	public void setLastRetrievedSessionTotal(Date lastRetrievedSessionTotal) {
		this.lastRetrievedSessionTotal = lastRetrievedSessionTotal;
	}

	public Company getCompany() {
		if (this.companyId != null) {
			return Company.getCompany(companyId);
		}

		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public Long getCompanyId() {
		if (company != null) {
			return company.getId();
		}

		return companyId;
	}

	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
		this.company = Company.getCompany(companyId);
	}

	public String getSnapShotDay1Id() {
		return snapShotDay1Id;
	}

	public void setSnapShotDay1Id(String snapShotDay1Id) {
		this.snapShotDay1Id = snapShotDay1Id;
	}

	public String getSnapShotDay2Id() {
		return snapShotDay2Id;
	}

	public void setSnapShotDay2Id(String snapShotDay2Id) {
		this.snapShotDay2Id = snapShotDay2Id;
	}

	public List<DCRBalancingToolProduct> getProducts() {
		return products;
	}

	public void setProducts(List<DCRBalancingToolProduct> products) {
		this.products = products;
	}

	public void addProduct(DCRBalancingToolProduct product) {
		if (this.products == null) {
			products = new ArrayList<DCRBalancingToolProduct>();
		}
		this.products.add(product);
	}

}
