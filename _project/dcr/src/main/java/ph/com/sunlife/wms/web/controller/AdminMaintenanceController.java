package ph.com.sunlife.wms.web.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.web.servlet.ModelAndView;

import ph.com.sunlife.wms.dao.domain.CenterCode;
import ph.com.sunlife.wms.dao.domain.DCR;
import ph.com.sunlife.wms.dao.domain.DCRAuditLog;
import ph.com.sunlife.wms.dao.domain.DatacapScanner;
import ph.com.sunlife.wms.dao.domain.HubCCNBO;
import ph.com.sunlife.wms.dao.domain.HubCCNBOUserGroup;
import ph.com.sunlife.wms.dao.domain.UserGroupHub;
import ph.com.sunlife.wms.dao.domain.WMSUser;
import ph.com.sunlife.wms.security.Role;
import ph.com.sunlife.wms.services.CenterCodeService;
import ph.com.sunlife.wms.services.ConsolidatedDataService;
import ph.com.sunlife.wms.services.DatacapScannerService;
import ph.com.sunlife.wms.services.WMSUserService;
import ph.com.sunlife.wms.services.bo.AdminCenterCodeBO;
import ph.com.sunlife.wms.services.bo.AdminWmsUserBO;
import ph.com.sunlife.wms.services.bo.UserSession;
import ph.com.sunlife.wms.util.WMSConstants;
import ph.com.sunlife.wms.util.WMSDateUtil;
import ph.com.sunlife.wms.util.WMSStringUtil;
import ph.com.sunlife.wms.web.controller.form.AdminCenterCodeForm;
import ph.com.sunlife.wms.web.controller.form.AdminDCRStatusForm;
import ph.com.sunlife.wms.web.controller.form.AdminWmsUserForm;
import ph.com.sunlife.wms.web.controller.form.SearchDCRStatusForm;

public class AdminMaintenanceController extends AbstractSecuredMultiActionController {

    private static final Logger LOGGER = Logger
            .getLogger(AdminMaintenanceController.class);
    private static final String SearchDCRStatusForm = "searchDCRStatusForm";
    private WMSUserService wmsUserService;
    private CenterCodeService centerCodeService;
    private ConsolidatedDataService consolidatedDataService;
    private DatacapScannerService datacapScannerService;

    public void setDatacapScannerService(DatacapScannerService datacapScannerService) {
		this.datacapScannerService = datacapScannerService;
	}

    public void setWmsUserService(WMSUserService wmsUserService) {
        this.wmsUserService = wmsUserService;
    }

    public void setCenterCodeService(CenterCodeService centerCodeService) {
        this.centerCodeService = centerCodeService;
    }

    public void setConsolidatedDataService(
            ConsolidatedDataService consolidatedDataService) {
        this.consolidatedDataService = consolidatedDataService;
    }

    @Override
    protected Role[] allowedRoles() {
        return new Role[]{Role.ADMIN};
    }

    public ModelAndView doShowAdminMaintenancePage(HttpServletRequest request, HttpServletResponse response) throws Exception {
        LOGGER.info("Admin maintenance page");
        ModelAndView modelAndView = new ModelAndView("adminView");
        return modelAndView;
    }

    public ModelAndView doShowAdminCenterCodePage(HttpServletRequest request, HttpServletResponse response) throws Exception {
        LOGGER.info("Admin center code page");
        ModelAndView modelAndView = new ModelAndView("adminCenterCodeView");
        modelAndView.addObject("ccList", centerCodeService.getAllCenterCode());
        return modelAndView;
    }

    public ModelAndView doShowAdminUserPage(
            HttpServletRequest request,
            HttpServletResponse response) throws Exception {
        
        LOGGER.info("Admin wms user page");
        ModelAndView modelAndView = new ModelAndView("adminUserView");
        String param = request.getParameter("searchUserID");
        List<WMSUser> userList = null;
        
        if (null != param && !"".equals(param.trim())) {
            userList = new ArrayList<WMSUser>();
            try {
                // Change according to Healthcheck
                // *Problem was identified as inconsistent logging
                // *Change accordingly:
                //   -remove setting of value of variable in catch
                //   -create and if-else condition to handle value
                //   -added new variable as container for checking in if-else
                WMSUser user = wmsUserService.getWMSUserById(param);
                if(null != user){
                    userList.add(user);
                } else {
                    userList = wmsUserService.getAllWMSUser();
                }
            } catch (Exception e) {
                // Change according to Healthcheck
                // *Problem was identified as inconsistent logging
                // *Change was to removed setting value of variable and add logging
                LOGGER.error("Problem in getting user list: ", e);
            }
        } else {
            userList = wmsUserService.getAllWMSUser();
        }

        modelAndView.addObject("userList", userList);
        return modelAndView;
    }
    
   
    public ModelAndView doShowAdminScannerPage(HttpServletRequest request, HttpServletResponse response) throws Exception {
        LOGGER.info("Admin datacap scanner page");
        ModelAndView modelAndView = new ModelAndView("adminScannerView");
        String param = request.getParameter("searchScannerID");
        List<DatacapScanner> scannerList = datacapScannerService.getAllScanner();
        if (StringUtils.isNotBlank(param)) {
        	scannerList = new ArrayList<DatacapScanner>(); 
            try {
                DatacapScanner scanner = datacapScannerService.getScannerById(param);
                if(null != scanner){
                	scannerList.add(scanner);
                } 
            } catch (Exception e) {
                LOGGER.error("Problem in getting scanner list: ", e);
            }
        } 

        modelAndView.addObject("scannerList", scannerList);
        return modelAndView;
    }

    public ModelAndView doShowAdminUserForm(HttpServletRequest request, HttpServletResponse response) throws Exception {
        LOGGER.info("Admin wms user form");
        ModelAndView modelAndView = new ModelAndView("adminUserView");

        boolean isNew = true;

        String userId = request.getParameter("userId");

        if (null != userId) {
            isNew = false;
        }

        List<String> activeOption = new ArrayList<String>();
        activeOption.add("N");
        activeOption.add("Y");

        Set<String> userHubSet = new HashSet<String>();
        Set<String> userGroupStrList = new HashSet<String>();
        List<UserGroupHub> userGroupList = new ArrayList<UserGroupHub>();
        List<UserGroupHub> groupList = wmsUserService.getAllUserGroupHubList();
        for (UserGroupHub ugh : groupList) {
            if (userGroupStrList.add(ugh.getGroupId() + "_" + ugh.getGroupName())) {
                userGroupList.add(ugh);
            }
            userHubSet.add(ugh.getHubId());
        }

        List<String> userHubList = new ArrayList<String>();
        userHubList.addAll(userHubSet);
        Collections.sort(userHubList);

        Map<String, String> comCodes = new HashMap<String, String>();
        comCodes.put("CP", "SLOCP");
        comCodes.put("GF", "SLGFI");

        modelAndView.addObject("ccList", centerCodeService.getAllCenterCode());
        modelAndView.addObject("userHubList", userHubList);
        modelAndView.addObject("userGroupList", userGroupList);
        modelAndView.addObject("activeOption", activeOption);
        modelAndView.addObject("comCodes", comCodes);

        AdminWmsUserForm adForm = new AdminWmsUserForm();

        if (isNew) {
            modelAndView.addObject("action", "new");
            adForm.setWmsuActiveOpt("Y");
        } else {
            modelAndView.addObject("action", "update");
            WMSUser user = wmsUserService.getWMSUserById(userId);
            adForm.setWmsuId(user.getWmsuId());
            adForm.setWmsuName(user.getWmsuName());
            adForm.setWmsuActive(user.getWmsuActive());
            adForm.setWmsuActiveOpt(user.getWmsuActive() ? "Y" : "N");
            groupList = wmsUserService.getUserGroupHubList(userId);
            adForm.setCompanyCode(groupList.size() == 0 ? "-" : groupList.get(0).getComCode());
            List<String> userGroups = new ArrayList<String>();
            List<String> userHubs = new ArrayList<String>();
            List<String> centerCodes = new ArrayList<String>();
            for (UserGroupHub ugh : groupList) {
                userGroups.add(ugh.getGroupId());
                userHubs.add(ugh.getHubId());
                centerCodes.add(ugh.getCcId());
            }
            adForm.setUserGroups(userGroups);
            adForm.setUserHubs(userHubs);
            adForm.setCenterCodes(centerCodes);
        }

        modelAndView.addObject("adminWmsUserForm", adForm);
        return modelAndView;
    }

    public ModelAndView doShowAdminDCRStatusPage(HttpServletRequest request, HttpServletResponse response, SearchDCRStatusForm form) throws Exception {
        LOGGER.info("Admin dcr status page");
        ModelAndView modelAndView = new ModelAndView("adminDCRStatusView");
        List<String> statusList = new ArrayList<String>();
        populateStatusList(statusList);

        List<CenterCode> ccList = centerCodeService.getAllCenterCode();
        List<DCR> dcrList = null;
        SearchDCRStatusForm searchDCRStatusForm = new SearchDCRStatusForm();
        if (null != form) {
            DCR dcr = new DCR();
            dcr.setStatus(form.getStatus());
            dcr.setCcId(WMSStringUtil.replaceEscChar(form.getCcId()));
            dcr.setDcrStartDate(form.getDcrStartDate());
            dcr.setDcrEndDate(form.getDcrEndDate());

            searchDCRStatusForm.setStatus(form.getStatus());
            searchDCRStatusForm.setCcId(WMSStringUtil.replaceEscChar(form
                    .getCcId()));
            searchDCRStatusForm.setDcrStartDate(form.getDcrStartDate());
            searchDCRStatusForm.setDcrEndDate(form.getDcrEndDate());

            if (null != dcr.getDcrStartDate() || null != dcr.getDcrEndDate()
                    || null != dcr.getStatus() || null != dcr.getCcId()) {
                dcrList = consolidatedDataService
                        .getAllDcrForStatusUpdateFromSearch(dcr);
                request.getSession().setAttribute(SearchDCRStatusForm,
                        searchDCRStatusForm);
            } else {
                dcrList = consolidatedDataService.getAllDcrForStatusUpdate();
                request.getSession().removeAttribute(SearchDCRStatusForm);
            }
        }

        if (null == dcrList) {
            dcrList = consolidatedDataService.getAllDcrForStatusUpdate();
            request.getSession().removeAttribute(SearchDCRStatusForm);
        }

        if (null != request.getSession().getAttribute(SearchDCRStatusForm)) {
            form = (SearchDCRStatusForm) request.getSession().getAttribute(SearchDCRStatusForm);
        }

        modelAndView.addObject(SearchDCRStatusForm, searchDCRStatusForm);
        modelAndView.addObject("dcrList", dcrList);
        modelAndView.addObject("statusList", statusList);
        modelAndView.addObject("ccList", ccList);
        modelAndView.addObject("action", "view");
        return modelAndView;
    }

    public ModelAndView doShowAdminDCRStatusForm(HttpServletRequest request, HttpServletResponse response) throws Exception {
        LOGGER.info("Admin dcr status form");
        ModelAndView modelAndView = new ModelAndView("adminDCRStatusView");

        String dcrId = request.getParameter("dcrId");

        List<String> downloadOption = new ArrayList<String>();
        downloadOption.add("N");
        downloadOption.add("Y");

        List<String> statusList = new ArrayList<String>();
        populateStatusList(statusList);

        List<WMSUser> users = wmsUserService.getAllWMSUser();
        List<String> userList = new ArrayList<String>();
        for (WMSUser user : users) {
            userList.add(user.getWmsuId());
        }
        Collections.sort(userList);
        modelAndView.addObject("userList", userList);
        modelAndView.addObject("downloadOption", downloadOption);
        modelAndView.addObject("statusList", statusList);
        modelAndView.addObject("action", "update");

        List<DCR> dcrList = consolidatedDataService.getAllDcrForStatusUpdate();
        DCR dcrTmp = null;
        for (DCR dcr : dcrList) {
            if (dcr.getId().toString().equals(dcrId)) {
                dcrTmp = dcr;
                break;
            }
        }

        AdminDCRStatusForm adForm = new AdminDCRStatusForm();
        adForm.setCenterCode(dcrTmp.getCenterCode());
        adForm.setDcrDate(dcrTmp.getDcrDate());
        adForm.setStatus(dcrTmp.getStatus());
        adForm.setHasDownload(dcrTmp.getHasDownloaded());

        modelAndView.addObject("adminDCRStatusForm", adForm);
        return modelAndView;
    }

    public ModelAndView doAdminDCRStatusDoUpdate(HttpServletRequest request, HttpServletResponse response, AdminDCRStatusForm form) throws Exception {
        LOGGER.info("Admin dcr status form update");

        DCR dcr = new DCR();
        dcr.setId(form.getDcrId());
        dcr.setStatus(form.getStatus());
        dcr.setUpdateDate(WMSDateUtil.toDateWithCompleteTime(form.getLogDate()));
        dcr.setUpdateUserId(form.getUpdateUserId().toLowerCase());
        dcr.setHasDownloaded(form.getHasDownload());

        DCRAuditLog audit = new DCRAuditLog();
        audit.setDcrId(form.getDcrId());
        audit.setLogDate(WMSDateUtil.toDateWithCompleteTime(form.getLogDate()));
        audit.setProcessStatus(form.getStatus());
        audit.setUserId(form.getUpdateUserId().toLowerCase());
        audit.setActionDetails(WMSStringUtil.replaceEscChar(form.getRemark()));

        consolidatedDataService.updateDCRStatus(dcr, audit);

        ModelAndView modelAndView = this.doShowAdminDCRStatusPage(request, response, (SearchDCRStatusForm) request.getSession().getAttribute(SearchDCRStatusForm));
        return modelAndView;
    }

    public ModelAndView doAdminUserFormDoCreate(HttpServletRequest request, HttpServletResponse response, AdminWmsUserForm form) throws Exception {
        LOGGER.info("Admin wms user form create");
        ModelAndView modelAndView = this.doShowAdminUserPage(request, response);
        String sessUser = ((UserSession) request.getSession().getAttribute(WMSConstants.USER_SESSION)).getUserId();

        AdminWmsUserBO wmsUserBO = new AdminWmsUserBO();
        BeanUtils.copyProperties(wmsUserBO, form);
        wmsUserService.doWMSUserCreate(wmsUserBO, sessUser);

        return modelAndView;
    }

    public ModelAndView doAdminUserFormDoUpdate(HttpServletRequest request, HttpServletResponse response, AdminWmsUserForm form) throws Exception {
        LOGGER.info("Admin wms user form update");
        ModelAndView modelAndView = this.doShowAdminUserPage(request, response);
        String sessUser = ((UserSession) request.getSession().getAttribute(WMSConstants.USER_SESSION)).getUserId();

        AdminWmsUserBO wmsUserBO = new AdminWmsUserBO();
        BeanUtils.copyProperties(wmsUserBO, form);
        wmsUserService.doWMSUserUpdate(wmsUserBO, sessUser);

        return modelAndView;
    }

    public ModelAndView doAdminUserFormDoRemove(HttpServletRequest request, HttpServletResponse response) throws Exception {
        LOGGER.info("Admin wms user form remove");
        ModelAndView modelAndView = this.doShowAdminUserPage(request, response);

        String[] userIds = StringUtils.split(request.getParameter("userIds"), ",");
        for (String userId : userIds) {
            wmsUserService.deleteWMSUser(userId);
        }

        return modelAndView;
    }
    
    public ModelAndView doAdminScannerFormDoRemove(HttpServletRequest request, HttpServletResponse response) throws Exception {
        LOGGER.info("Admin datacap scanner form remove");
        ModelAndView modelAndView = this.doShowAdminScannerPage(request, response);
        
        String[] pcNames = StringUtils.split(request.getParameter("pcNames"), ",");
        for (String pcName : pcNames) {
            datacapScannerService.deleteScanner(pcName);
        }
        modelAndView.addObject("scannerList", datacapScannerService.getAllScanner());
        return modelAndView;
    }

    public ModelAndView doShowAdminCenterCodeForm(HttpServletRequest request, HttpServletResponse response) throws Exception {
        LOGGER.info("Admin center code form");
        ModelAndView modelAndView = new ModelAndView("adminCenterCodeView");

        boolean isNew = true;

        String ccId = request.getParameter("ccId");

        if (null != ccId) {
            isNew = false;
        }

        List<String> yesNoOption = new ArrayList<String>();
        yesNoOption.add("N");
        yesNoOption.add("Y");

        Set<String> userHubSet = new HashSet<String>();
        Set<String> userGroupStrList = new HashSet<String>();
        List<UserGroupHub> userGroupList = new ArrayList<UserGroupHub>();
        List<UserGroupHub> groupList = wmsUserService.getAllUserGroupHubList();
        for (UserGroupHub ugh : groupList) {
            if (userGroupStrList.add(ugh.getGroupId() + "_" + ugh.getGroupName())) {
                userGroupList.add(ugh);
            }
            userHubSet.add(ugh.getHubId());
        }

        List<String> userHubList = new ArrayList<String>();
        userHubList.addAll(userHubSet);
        Collections.sort(userHubList);

        List<HubCCNBO> userNBOList = wmsUserService.getHubCCNBO(null);

        modelAndView.addObject("ccList", centerCodeService.getAllCenterCode());
        modelAndView.addObject("userHubList", userHubList);
        modelAndView.addObject("userGroupList", userGroupList);
        modelAndView.addObject("yesNoOption", yesNoOption);
        modelAndView.addObject("userList", wmsUserService.getAllWMSUser());


        AdminCenterCodeForm adForm = new AdminCenterCodeForm();

        if (isNew) {
            modelAndView.addObject("action", "new");
            adForm.setCcActive("Y");
            adForm.setCcAutoIdx("Y");
            adForm.setCcCanEncode("Y");
            adForm.setCcIsIso("Y");
            adForm.setCcViewImage("Y");
            adForm.setCcZoomZone("Y");
            adForm.setUserNBO("(ALL)");
        } else {
            modelAndView.addObject("action", "update");
            CenterCode cc = centerCodeService.getCenterCodeById(WMSStringUtil.replaceEscChar(ccId)).get(0);
            if (null != cc) {
                adForm.setCcId(cc.getCcId());
                adForm.setCcAbbrevName(cc.getCcAbbrevName());
                adForm.setCcDesc(cc.getCcDesc());
                adForm.setCcName(cc.getCcName());

                HubCCNBOUserGroup hub = new HubCCNBOUserGroup();
                hub.setHccnugCCID(cc.getCcId());

                HubCCNBO nbo = new HubCCNBO();
                nbo.setHccnCCID(cc.getCcId());
                List<HubCCNBO> nboList = wmsUserService.getHubCCNBO(nbo);
                if (CollectionUtils.isNotEmpty(nboList)) {
                    adForm.setUserNBO(nboList.get(0).getHccnNBOID());
                    adForm.setUserHub(nboList.get(0).getHccnHUBID());
                }
                hub.setHccnugHUBID(adForm.getUserHub());
                hub.setHccnugActive(cc.getCcActive() ? "1" : "0");
                List<HubCCNBOUserGroup> hubList = wmsUserService.getUserGroupHubByUserId(hub);
                if (CollectionUtils.isNotEmpty(hubList)) {
                    adForm.setUserId(hubList.get(0).getHccnugWMSUID());
                    List<String> grpList = new ArrayList<String>();
                    for (HubCCNBOUserGroup hcc : hubList) {
                        grpList.add(hcc.getHccnugGRPID());
                    }
                    adForm.setUserGroups(grpList);
                }

                adForm.setCcActive(cc.getCcActive() ? "Y" : "N");
                adForm.setCcAutoIdx(cc.getCcAutoIdx() ? "Y" : "N");
                adForm.setCcCanEncode(cc.getCcCanEncode() ? "Y" : "N");
                adForm.setCcIsIso(cc.getCcIsIso() ? "Y" : "N");
                adForm.setCcViewImage(cc.getCcViewImage() ? "Y" : "N");
                adForm.setCcZoomZone(cc.getCcZoomZone() ? "Y" : "N");
            }
        }

        modelAndView.addObject("adminCenterCodeForm", adForm);
        return modelAndView;
    }

    public ModelAndView doAdminCenterCodeFormDoCreate(HttpServletRequest request, HttpServletResponse response, AdminCenterCodeForm form) throws Exception {
        LOGGER.info("Admin center code form create");
        ModelAndView modelAndView = this.doShowAdminCenterCodePage(request, response);
        String sessUser = ((UserSession) request.getSession().getAttribute(WMSConstants.USER_SESSION)).getUserId();

        AdminCenterCodeBO adminCenterCodeBO = new AdminCenterCodeBO();
        BeanUtils.copyProperties(adminCenterCodeBO, form);
        String ccIdCachedValue = cachingService
                .getCachedValue("dcr.rollout.ccid");
        centerCodeService.doCenterCodeCreate(adminCenterCodeBO, sessUser, ccIdCachedValue);

        return modelAndView;
    }

    public ModelAndView doAdminCenterCodeFormDoUpdate(HttpServletRequest request, HttpServletResponse response, AdminCenterCodeForm form) throws Exception {
        LOGGER.info("Admin center code form update");
        ModelAndView modelAndView = this.doShowAdminCenterCodePage(request, response);
        String sessUser = ((UserSession) request.getSession().getAttribute(WMSConstants.USER_SESSION)).getUserId();

        AdminCenterCodeBO adminCenterCodeBO = new AdminCenterCodeBO();
        BeanUtils.copyProperties(adminCenterCodeBO, form);
        String ccIdCachedValue = cachingService
                .getCachedValue("dcr.rollout.ccid");
        centerCodeService.doCenterCodeUpdate(adminCenterCodeBO, sessUser, ccIdCachedValue);

        return modelAndView;
    }

    public ModelAndView doAdminCenterCodeFormDoRemove(HttpServletRequest request, HttpServletResponse response) throws Exception {
        LOGGER.info("Admin center code form remove");
        ModelAndView modelAndView = this.doShowAdminCenterCodePage(request, response);
        String sessUser = ((UserSession) request.getSession().getAttribute(WMSConstants.USER_SESSION)).getUserId();
        String ccIdCachedValue = cachingService
                .getCachedValue("dcr.rollout.ccid");
        String ccIdCachedValueTmp = "";
        String[] ccIds = StringUtils.split(request.getParameter("ccIds"), ",");
        for (String ccId : ccIds) {
            if (centerCodeService.deleteCenterCode(ccId)) {
                if (ccIdCachedValue.contains(ccId)) {
                    ccIdCachedValueTmp += "," + ccId;
                }
            }
        }

        if (!"".equals(ccIdCachedValueTmp)) {
            cachingService.updateProperty("dcr.rollout.ccid", ccIdCachedValue.replaceAll(ccIdCachedValueTmp, "").trim(), sessUser, new Date().getTime());
        }

        return modelAndView;
    }

    public ModelAndView doShowAdminAnnouncementPage(HttpServletRequest request, HttpServletResponse response) throws Exception {
        LOGGER.info("Admin dcr status page");
        ModelAndView modelAndView = new ModelAndView("adminAnnouncementView");
        return modelAndView;
    }

    void populateStatusList(List<String> statusList) {
        String statusArray[] = {"Reconciled", "Awaiting Reqts", "For Review and Approval", "PENDING", "Approved for Recon", "NEW", "Awaiting Feedback"};
        for (String status : statusArray) {
            statusList.add(status);
        }
        Collections.sort(statusList);
    }
}
