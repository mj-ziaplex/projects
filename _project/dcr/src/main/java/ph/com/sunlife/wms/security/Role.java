package ph.com.sunlife.wms.security;

public enum Role {

    CASHIER, MANAGER, PPA, OTHER, ADMIN;
}
