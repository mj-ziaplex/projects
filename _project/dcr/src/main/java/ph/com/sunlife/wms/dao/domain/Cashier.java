package ph.com.sunlife.wms.dao.domain;

/**
 * This represents the table PS_LOOKUP_CASHIER, which is a table pre-populated
 * by a batch job with entries representing a valid IPAC Cashier.
 * 
 * @author Zainal Limpao
 * 
 */
public class Cashier {

	private String acf2id;

	private String firstname;

	private String middlename;

	private String lastname;

	private String emailaddress;

	public Cashier() {
	}

	public Cashier(String acf2id) {
		this.acf2id = acf2id;
	}

	/**
	 * The ACF2ID column (USER ID) with varchar(6).
	 * 
	 * @return
	 */
	public String getAcf2id() {
		return acf2id;
	}

	public void setAcf2id(String acf2id) {
		this.acf2id = acf2id;
	}

	/**
	 * The FIRST_NAME column with varchar(50).
	 * 
	 * @return
	 */
	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	/**
	 * The MIDDLE_NAME column with varchar(50).
	 * 
	 * @return
	 */
	public String getMiddlename() {
		return middlename;
	}

	public void setMiddlename(String middlename) {
		this.middlename = middlename;
	}

	/**
	 * The LAST_NAME column with varchar(50).
	 * 
	 * @return
	 */
	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	/**
	 * The EMAIL_ADDRESS column with varchar(50).
	 * 
	 * @return
	 */
	public String getEmailaddress() {
		return emailaddress;
	}

	public void setEmailaddress(String emailaddress) {
		this.emailaddress = emailaddress;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((acf2id == null) ? 0 : acf2id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Cashier other = (Cashier) obj;
		if (acf2id == null) {
			if (other.acf2id != null)
				return false;
		} else if (!acf2id.equals(other.acf2id))
			return false;
		return true;
	}

}
