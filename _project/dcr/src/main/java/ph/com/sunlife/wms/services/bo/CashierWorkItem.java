package ph.com.sunlife.wms.services.bo;

public class CashierWorkItem {

	private Long dcrId;
	
	private Long dcrCashierId;
	
	private String acf2id;

	public Long getDcrId() {
		return dcrId;
	}

	public void setDcrId(Long dcrId) {
		this.dcrId = dcrId;
	}

	public Long getDcrCashierId() {
		return dcrCashierId;
	}

	public void setDcrCashierId(Long dcrCashierId) {
		this.dcrCashierId = dcrCashierId;
	}

	public String getAcf2id() {
		return acf2id;
	}

	public void setAcf2id(String acf2id) {
		this.acf2id = acf2id;
	}
	
}
