package ph.com.sunlife.wms.dao.domain;

public class DCRAddingMachineSheet extends Entity {

	private DCRAddingMachine dcrAddingMachine;

	private Company company;

	private double totalAmount;

	public DCRAddingMachine getDcrAddingMachine() {
		return dcrAddingMachine;
	}

	public void setDcrAddingMachine(DCRAddingMachine dcrAddingMachine) {
		this.dcrAddingMachine = dcrAddingMachine;
	}
	
	public void setDcrAddingMachine(Long dcrAddingMachineId) {
		DCRAddingMachine dcrAddingMachine = new DCRAddingMachine();
		dcrAddingMachine.setId(dcrAddingMachineId);
		this.dcrAddingMachine = dcrAddingMachine;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}
	
	public void setCompany(Long companyId) {
		this.setCompany(Company.getCompany(companyId));
	}

	public double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}

}
