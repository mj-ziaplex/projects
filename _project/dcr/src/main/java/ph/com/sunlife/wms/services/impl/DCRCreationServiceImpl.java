/**
 * @author i176
 * ****************************************************************************
 * @author: ia23 - sasay
 * @date: Mar2016
 * @desc: Removed methods and parameters that will not be used due to 
 * MR-WF-15-00089 - DCR Redesign
 */
package ph.com.sunlife.wms.services.impl;

import static ph.com.sunlife.wms.util.WMSConstants.STATUS_NEW;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import net.sf.jasperreports.engine.util.ObjectUtils;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;

import ph.com.sunlife.wms.dao.DCRBalancingToolDao;
import ph.com.sunlife.wms.dao.DCRCashierDao;
import ph.com.sunlife.wms.dao.DCRDao;
import ph.com.sunlife.wms.dao.DCROtherDao;
import ph.com.sunlife.wms.dao.DCRReversalDao;
import ph.com.sunlife.wms.dao.IpacDao;
import ph.com.sunlife.wms.dao.JobDataDao;
import ph.com.sunlife.wms.dao.domain.Company;
import ph.com.sunlife.wms.dao.domain.Currency;
import ph.com.sunlife.wms.dao.domain.DCR;
import ph.com.sunlife.wms.dao.domain.DCRAuditLog;
import ph.com.sunlife.wms.dao.domain.DCRBalancingTool;
import ph.com.sunlife.wms.dao.domain.DCRBalancingToolProduct;
import ph.com.sunlife.wms.dao.domain.DCRCashier;
import ph.com.sunlife.wms.dao.domain.DCRCashierNoCollection;
import ph.com.sunlife.wms.dao.domain.DCRIpacValue;
import ph.com.sunlife.wms.dao.domain.DCROther;
import ph.com.sunlife.wms.dao.domain.DCRReversal;
import ph.com.sunlife.wms.dao.domain.GAFValue;
import ph.com.sunlife.wms.dao.domain.JobData;
import ph.com.sunlife.wms.dao.domain.JobDataErrorLog;
import ph.com.sunlife.wms.dao.domain.NonPostedTransaction;
import ph.com.sunlife.wms.dao.domain.ProductType;
import ph.com.sunlife.wms.dao.exceptions.WMSDaoException;
import ph.com.sunlife.wms.integration.DCRFilenetIntegrationService;
import ph.com.sunlife.wms.integration.client.UserCollectionsTotalUtil;
import ph.com.sunlife.wms.integration.exceptions.IntegrationException;
import ph.com.sunlife.wms.services.CachingService;
import ph.com.sunlife.wms.services.DCRCreationService;
import ph.com.sunlife.wms.services.bo.CompanyBO;
import ph.com.sunlife.wms.services.bo.DCRBO;
import ph.com.sunlife.wms.services.bo.DCRBalancingToolBO;
import ph.com.sunlife.wms.services.bo.DCRBalancingToolProductBO;
import ph.com.sunlife.wms.services.bo.DCRCashierBO;
import ph.com.sunlife.wms.services.bo.DCRIpacValueBO;
import ph.com.sunlife.wms.services.bo.DCROtherBO;
import ph.com.sunlife.wms.services.bo.DCRReversalBO;
import ph.com.sunlife.wms.services.exception.ServiceException;
import ph.com.sunlife.wms.util.ExceptionUtil;
import ph.com.sunlife.wms.util.HtmlSnapShotUtil;
import ph.com.sunlife.wms.util.WMSCollectionUtils;
import ph.com.sunlife.wms.util.WMSConstants;
import ph.com.sunlife.wms.util.WMSDateUtil;
import ph.com.sunlife.wms.dao.DCRIPACDao; // Added for DCR redesign

import com.sunlife.ingeniumutil.container.UserCollectionsInfo;

/**
 * The implementing class of {@link DCRCreationService}.
 *
 * @author Zainal Limpao
 *
 */
public class DCRCreationServiceImpl implements DCRCreationService {

    private static final int ONE_CALENDAR_WEEK = 7;
    private static final String INGENIUM_USERID_SUFFIX = "i";
    private static final long DCR_CREATION_JOB_ID = 1L;
    private static final Logger LOGGER = Logger.getLogger(DCRCreationServiceImpl.class);
    private static final String INGENIUM_CURRENCY_DOLLAR = "US";
    private static final String INGENIUM_CURRENCY_PESO = "PS";
    private static final String STANDARD_DATE_FORMAT_STR = "ddMMMyyyy";
    private static final DateFormat STANDARD_DATE_FORMAT = new SimpleDateFormat(STANDARD_DATE_FORMAT_STR);
    private DCRDao dcrDao;
    private IpacDao ipacDao;
    private DCRCashierDao dcrCashierDao;
    private DCRBalancingToolDao dcrBalancingToolDao;
    private DCRReversalDao dcrReversalDao;
    private DCRFilenetIntegrationService dcrFilenetIntegrationService;
    private UserCollectionsTotalUtil slocpiUserCollectionsTotalUtil;
    private UserCollectionsTotalUtil slgfiUserCollectionsTotalUtil;
    private DCROtherDao dcrOtherDao;
    private JobDataDao jobDataDao;
    private CachingService cachingService;
    private DCRIPACDao dcrIpacDao;

    /*
     * (non-Javadoc)
     * 
     * @see
     * ph.com.sunlife.wms.services.DCRCreationService#initiateDCRWorkItems(java
     * .util.Date)
     */
    @Override
    public List<DCRBO> initiateDCRWorkItems(Date date, boolean isManualBatchRun) throws ServiceException,
            IntegrationException {
        LOGGER.info("Initializing DCR Creation...");

        Date startOfTheDay = WMSDateUtil.startOfTheDay(date);
        List<DCRBO> list = null;
        List<DCR> savedDCRList = null;
        try {
            boolean isHoliday = dcrDao.checkIfHoliday(date);

            if (!isHoliday) {
                savedDCRList = dcrDao.getAllDCRForTheDay(startOfTheDay);

                // if (CollectionUtils.isEmpty(savedDCRList)) {
                List<DCR> dcrList = this.createDCRPerCustomerCenter(
                        startOfTheDay, savedDCRList);

                if (isManualBatchRun) {
                    List<DCR> reprocessedDCRList = new ArrayList<DCR>();
                    for (DCR dcr : savedDCRList) {
                        if (null == dcr.getWorkObjectNumber()) {
                            reprocessedDCRList.add(dcr);
                        }
                    }
                    if (CollectionUtils.isNotEmpty(reprocessedDCRList)) {
                        dcrList = reprocessedDCRList;
                    }
                }

                if (CollectionUtils.isNotEmpty(dcrList)) {
                    dcrList = this.saveDCRList(dcrList, startOfTheDay, isManualBatchRun);
                } else {
                    LOGGER.info("Selected Creation Date already has corresponding DCR Workitems.");
                    throw new WMSDaoException(
                            "Unable to overwrite existing DCR Work Items");
                }

                Collections.sort(dcrList);
                list = this.convertDCRToDCRBO(dcrList);
            } else {
                LOGGER.info("Selected Creation Date is either a Weekend or a Holiday");
                throw new WMSDaoException(
                        "Unable to create DCR Work Items on weekends and holidays");
            }
        } catch (WMSDaoException e) {
            LOGGER.error(e);
            throw new ServiceException(e);
        }
        return list;
    }

    /**
     * The List of {@link DCR} objects will be converted to {@link DCRBO}. This
     * is to ensure that Domain objects from the Data Access Object layer will
     * not reach the controller, as one of the loose coupling best practices.
     *
     * @param dcrList
     * @return
     * @throws ServiceException
     */
    private List<DCRBO> convertDCRToDCRBO(List<DCR> dcrList)
            throws ServiceException {
        List<DCRBO> refreshedDCRList = new ArrayList<DCRBO>();
        for (DCR dcr : dcrList) {
            DCRBO dcrBO = new DCRBO();
            // BeanUtils.copyProperties(dcrBo, dcr);
            dcrBO.setCcId(dcr.getCcId());
            dcrBO.setCcName(dcr.getCcName());
            dcrBO.setCenterCode(dcr.getCenterCode());
            dcrBO.setId(dcr.getId());
            dcrBO.setStatus(dcr.getStatus());

            String formatedDateStr = StringUtils.upperCase(STANDARD_DATE_FORMAT.format(dcr.getDcrDate()));
            dcrBO.setFormatedDateStr(formatedDateStr);
            dcrBO.setFormatedRCDStr(StringUtils.upperCase(WMSDateUtil.toFormattedDateStr(dcr.getRequiredCompletionDate())));
            dcrBO.setDcrDate(dcr.getDcrDate());
            dcrBO.setUpdateDate(dcr.getUpdateDate());
            dcrBO.setUpdateUserId(dcr.getUpdateUserId());
            dcrBO.setCreateDate(dcr.getCreateDate());
            dcrBO.setCreateUserId(dcr.getCreateUserId());

            refreshedDCRList.add(dcrBO);
        }
        Collections.sort(refreshedDCRList);
        return refreshedDCRList;
    }

    /**
     * The List of {@link DCR} will be persisted by the Data Access Object into
     * the Database.
     *
     */
    public List<DCR> saveDCRList(List<DCR> dcrList, Date date, boolean isManualBatchRun)
            throws WMSDaoException, IntegrationException {
        List<DCR> savedDCRList = null;
        savedDCRList = new ArrayList<DCR>();

        for (DCR dcr : dcrList) {
            if (isManualBatchRun) {
                DCR dcrTmp = dcrDao.refresh(dcr);
                if (null == dcrTmp) {
                    dcr.setCreateUserId("auto");
                    dcr = dcrDao.save(dcr);
                    savedDCRList.add(dcr);
                } else {
                    if (null == dcrTmp.getWorkObjectNumber()) {
                        savedDCRList.add(dcr);
                    }
                }
            } else {
                dcr.setCreateUserId("auto");
                dcr = dcrDao.save(dcr);
                savedDCRList.add(dcr);
            }
        }

        for (DCR dcr : savedDCRList) {
            LOGGER.debug("Creating DCR Workitem for " + dcr.getCcId());

            if (this.dcrFilenetIntegrationService != null) {
                String workObjectNumber = this.dcrFilenetIntegrationService.createDcrOnFilenet(date, dcr.getId(), dcr.getCcId());
                if (StringUtils.isNotEmpty(workObjectNumber)) {
                    dcr.setWorkObjectNumber(workObjectNumber);
                    this.dcrDao.updateWorkObjectNumber(dcr);
                }
            }

            DCRAuditLog auditLog = new DCRAuditLog();

            auditLog.setActionDetails("WF Action: Created");
            auditLog.setDcrId(dcr.getId());
            auditLog.setLogDate(cachingService.getWmsDcrSystemDate());
            auditLog.setProcessStatus("New");
            auditLog.setUserId("auto");
            dcrDao.createLog(auditLog);
        }

        return savedDCRList;
    }

    /**
     * Injection setter for {@link DCRDao}
     *
     * @param dcrDao
     */
    public void setDcrDao(DCRDao dcrDao) {
        this.dcrDao = dcrDao;
    }

    /**
     * Injection setter for {@link DCRCashierDao}
     *
     * @param dcrCashierDao
     */
    public void setDcrCashierDao(DCRCashierDao dcrCashierDao) {
        this.dcrCashierDao = dcrCashierDao;
    }

    /**
     * Each extracted qualified Customer Center Code, one {@link DCR} will be
     * created. This is a started process in the application that has to occur
     * every business day.
     *
     * @param date
     * @return
     * @throws ServiceException
     */
    private List<DCR> createDCRPerCustomerCenter(Date date,
            List<DCR> savedDCRList) throws ServiceException {
        // List<String> centerCodes = null;

        String ccIdCachedValue = cachingService.getCachedValue("dcr.rollout.ccid");
        String[] ccIdArray = ccIdCachedValue.split(",");

        Set<String> ccIds = new HashSet<String>();
        for (String ccId : ccIdArray) {
            StringUtils.trim(ccId);
        }

        if (ArrayUtils.isNotEmpty(ccIdArray)) {
            for (String ccIdStr : ccIdArray) {
                String ccId = StringUtils.trim(ccIdStr);
                if (StringUtils.isNotEmpty(ccId)) {
                    ccIds.add(ccId);
                }
            }
        }

        Date requiredCompletionDate = this.generateRequiredCompletionDate(date);

        List<DCR> dcrList = new ArrayList<DCR>();
        if (CollectionUtils.isNotEmpty(ccIds)) {
            for (String centerCode : ccIds) {
                DCR dcr = new DCR();
                dcr.setCcId(centerCode);
                dcr.setDcrDate(date);
                dcr.setStatus(STATUS_NEW);
                dcr.setRequiredCompletionDate(requiredCompletionDate);

                final String thisCenterCode = centerCode;
                DCR thisDCR = (DCR) CollectionUtils.find(savedDCRList,
                        new Predicate() {
                    @Override
                    public boolean evaluate(Object object) {
                        DCR thisDCR = (DCR) object;
                        return StringUtils.equalsIgnoreCase(
                                thisCenterCode, thisDCR.getCcId());
                    }
                });

                if (thisDCR == null) {
                    dcrList.add(dcr);
                }
            }
        }

        return dcrList;
    }

    private Date generateRequiredCompletionDate(Date date)
            throws ServiceException {
        int numberOfHolidays = 0;
        try {
            // centerCodes = dcrDao.getAllCenterIds();
            numberOfHolidays = dcrDao.countHolidays(date);

            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("There are " + numberOfHolidays + " holidays within the week");
            }
        } catch (WMSDaoException e) {
            LOGGER.error(e);
            throw new ServiceException(e);
        }

        Date requiredCompletionDate = DateUtils.addDays(date, numberOfHolidays + ONE_CALENDAR_WEEK);
        return requiredCompletionDate;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * ph.com.sunlife.wms.services.DCRCreationService#createAndLoadDcrWorkItems
     * (java.lang.String, java.lang.String)
     */
    @Override
    public List<DCRCashierBO> createAndLoadDcrWorkItems(String ccId,
            String acf2id) throws ServiceException {

        int ipacDbHits = 0;

        List<DCR> dcrWorkItems = null;
        try {
            dcrWorkItems = dcrDao.getDCRWorkItemsForCCCashier(ccId);

            List<Long> dcrIds = new ArrayList<Long>();
            if (CollectionUtils.isNotEmpty(dcrWorkItems)) {
                for (DCR dcr : dcrWorkItems) {
                    dcrIds.add(dcr.getId());
                }
            }

            List<DCRCashier> existingCashierWorkItems = dcrCashierDao.getDCRCashierListByDcrList(dcrIds);

            List<DCRCashierNoCollection> noCollections = dcrCashierDao.getCashierNoCollections(acf2id);

            List<DCRCashier> cashierWorkItems = new ArrayList<DCRCashier>();

            Date today = new Date();

            for (DCR dcr : dcrWorkItems) {
                Date date = dcr.getDcrDate();

                final Long dcrId = dcr.getId();
                final String username = acf2id;
                DCRCashier existingCashierWorkitem = this
                        .searchForExistingCashierWorkItem(
                        existingCashierWorkItems, dcrId, username);

                boolean hasExistingCashierWorkItem = existingCashierWorkitem != null;

                DCRCashierNoCollection noCollection = this.findNoCollection(noCollections, dcrId);

                if (!hasExistingCashierWorkItem) {

                    if (noCollection == null) {

                        boolean hasCollections = this.checkIpacForCollections(date, acf2id, ccId);

                        ipacDbHits++;

                        if (hasCollections) {
                            DCRCashier cashierWorkItem = new DCRCashier();
                            cashierWorkItem.setCashier(acf2id);
                            cashierWorkItem.setDcr(dcr);
                            cashierWorkItem.setLastUpdateDate(cachingService.getWmsDcrSystemDate());
                            cashierWorkItem.setStatus(WMSConstants.STATUS_NEW);
                            cashierWorkItem = dcrCashierDao.save(cashierWorkItem);

                            cashierWorkItems.add(cashierWorkItem);
                        } else {
                            if (!DateUtils.isSameDay(dcr.getDcrDate(), today)) {
                                DCRCashierNoCollection thisNoCollection = new DCRCashierNoCollection();
                                thisNoCollection.setDcrId(dcrId);
                                thisNoCollection.setAcf2id(acf2id);
                                dcrCashierDao.insertNoCollection(thisNoCollection);
                            }
                        }

                    }
                } else {
                    existingCashierWorkitem.setDcr(dcr);
                    cashierWorkItems.add(existingCashierWorkitem);
                }
            }

            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("The number of IPAC DB Hits: " + ipacDbHits);
            }

            return this.convertCashierWorkItemsToBusinessObjects(cashierWorkItems);

        } catch (WMSDaoException e) {
            LOGGER.error(e);
            throw new ServiceException(e);
        }

    }

    private DCRCashierNoCollection findNoCollection(
            List<DCRCashierNoCollection> noCollections, final Long dcrId) {

        DCRCashierNoCollection noCollection = null;

        if (CollectionUtils.isNotEmpty(noCollections)) {
            noCollection = (DCRCashierNoCollection) CollectionUtils.find(
                    noCollections, new Predicate() {
                @Override
                public boolean evaluate(Object object) {
                    DCRCashierNoCollection noCollection = (DCRCashierNoCollection) object;
                    return ObjectUtils.equals(noCollection.getDcrId(), dcrId);
                }
            });
        }

        return noCollection;
    }

    private DCRCashier searchForExistingCashierWorkItem(
            List<DCRCashier> existingDcrCashierList, final Long dcrId,
            final String username) {
        DCRCashier existingDcrCashier = (DCRCashier) CollectionUtils.find(
                existingDcrCashierList, new Predicate() {
            @Override
            public boolean evaluate(Object object) {
                DCRCashier thisDcrCashier = (DCRCashier) object;

                DCR thisDcr = thisDcrCashier.getDcr();
                String thisUsername = thisDcrCashier.getCashier().getAcf2id();
                return thisDcr.getId().equals(dcrId)
                        && StringUtils.equalsIgnoreCase(thisUsername, username);
            }
        });
        return existingDcrCashier;
    }

    /**
     * Again this is to prevent tight coupling of dependencies between layers.
     *
     * @param dcrCashierList
     * @return
     */
    private List<DCRCashierBO> convertCashierWorkItemsToBusinessObjects(
            List<DCRCashier> dcrCashierList) {
        List<DCRCashierBO> businessObjects = new ArrayList<DCRCashierBO>();

        for (DCRCashier dcrCashier : dcrCashierList) {
            DCR dcr = dcrCashier.getDcr();
            DCRBO dcrBO = new DCRBO();
            dcrBO.setCcId(dcr.getCcId());
            dcrBO.setDcrDate(dcr.getDcrDate());
            dcrBO.setRequiredCompletionDate(dcr.getRequiredCompletionDate());
            dcrBO.setId(dcr.getId());
            dcrBO.setStatus(dcr.getStatus());
            dcrBO.setWorkObjectNumber(dcr.getWorkObjectNumber());

            String formatedDateStr = StringUtils.upperCase(STANDARD_DATE_FORMAT.format(dcr.getDcrDate()));
            dcrBO.setFormatedDateStr(formatedDateStr);

            String formatedRCDStr = StringUtils.upperCase(WMSDateUtil
                    .toFormattedDateStr(WMSDateUtil.getRCD(dcr.getDcrDate())));
            dcrBO.setFormatedRCDStr(formatedRCDStr);

            dcrBO.setCcName(dcr.getCcName());
            dcrBO.setCenterCode(dcr.getCenterCode());

            DCRCashierBO businessObj = new DCRCashierBO();
            businessObj.setDcr(dcrBO);
            businessObj.setCashier(dcrCashier.getCashier().getAcf2id());
            businessObj.setId(dcrCashier.getId());
            businessObj.setStatus(dcrCashier.getStatus());
            businessObj.setLastUpdateDate(dcrCashier.getLastUpdateDate());

            businessObj.setFormatedDateStr(formatedRCDStr);
            businessObj.setFormatedRCDStr(formatedRCDStr);

            businessObjects.add(businessObj);
        }

        Collections.sort(businessObjects);
        return businessObjects;
    }

    /**
     * This is a checker in which IPAC will be called if a collection has
     * already been made for the given cashier, in that particular customer
     * center, on this given day.
     *
     * @param date
     * @param acf2id
     * @param ccId
     * @return <code>true</code> if there is a collection, otherwise
     * <code>false</code>
     */
    boolean checkIpacForCollections(
            final Date date,
            final String acf2id,
            final String ccId) throws ServiceException {
        try {
            boolean hasCollections = ipacDao.hasCollections(ccId, date, acf2id);
            return hasCollections;
        } catch (WMSDaoException ex) {
            LOGGER.error(ex);
            throw new ServiceException(ex);
        }
        // return true;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * ph.com.sunlife.wms.services.DCRCreationService#getAllDcrWorkItems(java
     * .lang.String)
     */
    public List<DCRBO> getAllDcrWorkItems(String ccId) throws ServiceException {
        try {
            List<DCR> list = dcrDao.getDCRWorkItemsForCCCashier(ccId);
            return this.convertDCRToDCRBO(list);
        } catch (WMSDaoException e) {
            LOGGER.error(e);
            throw new ServiceException(e);
        }
    }

    @Override
    public DCRBO getDcrById(Long id) throws ServiceException {
        try {
            DCR dcr = dcrDao.getById(id);

            if (dcr != null) {
                List<DCR> dcrList = new ArrayList<DCR>();
                dcrList.add(dcr);
                List<DCRBO> businessObjects = this.convertDCRToDCRBO(dcrList);

                if (CollectionUtils.isNotEmpty(businessObjects)) {
                    return businessObjects.get(0);
                }
            }

            return null;
        } catch (WMSDaoException e) {
            LOGGER.error(e);
            throw new ServiceException(e);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * ph.com.sunlife.wms.services.DCRCreationService#extractDCRFromDCRCashierList
     * (java.util.List)
     */
    @Override
    public List<DCRBO> extractDCRFromDCRCashierList(
            List<DCRCashierBO> dcrCashierList) throws ServiceException {
        Set<DCRBO> dcrBOSet = new HashSet<DCRBO>();

        String ccId = null;
        for (DCRCashierBO dcb : dcrCashierList) {
            DCRBO dcrBO = dcb.getDcr();

            if (dcrBO != null && dcrBO.getId() != null) {
                dcrBOSet.add(dcrBO);

                ccId = dcrBO.getCcId();
            }
        }

        if (StringUtils.isNotEmpty(ccId)) {
            try {
                List<DCR> newAndPendingDCRList = dcrDao.getAllPendingDCRForCC(ccId);

                List<DCRBO> newAndPendingDCRBOList = convertDCRToDCRBO(newAndPendingDCRList);

                dcrBOSet.addAll(newAndPendingDCRBOList);
            } catch (WMSDaoException e) {
                LOGGER.error(e);
                throw new ServiceException(e);
            }
        }

        List<DCRBO> dcrBOList = new ArrayList<DCRBO>(dcrBOSet);
        Collections.sort(dcrBOList);
        return dcrBOList;
    }

    /*
     * (non-Javadoc)
     * 
     * @see ph.com.sunlife.wms.services.DCRCreationService#
     * createAndDisplayDCRBalancingTools
     * (ph.com.sunlife.wms.services.bo.DCRCashierBO)
     */
    @Override
    public List<DCRBalancingToolBO> createAndDisplayDCRBalancingTools(
            final DCRCashierBO dcrCashierBO) throws ServiceException {

        Long dcrCashierId = dcrCashierBO.getId();
        List<DCRBalancingTool> list = null;

        List<DCRBalancingToolBO> createdDCRBalancingTools = null;

        try {
            // Checks if balancing tool records are already saved in the db
            list = dcrBalancingToolDao.getBalancingTools(dcrCashierId);

            // If balancing tool records are not found, it will create them
            if (CollectionUtils.isEmpty(list)) {

                list = new ArrayList<DCRBalancingTool>();
                for (Company company : Company.values()) {
                    DCRBalancingTool obj = new DCRBalancingTool();
                    obj.setCompany(company);
                    obj.setDcrCashier(dcrCashierId);

                    // Creates 1 Balancing Tool record per Company
                    list.add(dcrBalancingToolDao.save(obj));
                }

                if (CollectionUtils.isNotEmpty(list)) {
                    this.saveProductsForBalancingTools(list);
                }
            }

            createdDCRBalancingTools = this.convertToBusinessObjects(list);

        } catch (WMSDaoException e) {
            LOGGER.error(e);
            throw new ServiceException(e);
        }

        return createdDCRBalancingTools;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * ph.com.sunlife.wms.services.DCRCreationService#confirmDCRBalancingTool
     * (ph.com.sunlife.wms.services.bo.DCRBalancingToolBO)
     */
    public boolean confirmDCRBalancingTool(DCRBalancingToolBO busObj,
            boolean isDayOne) throws ServiceException {
        boolean result = true;
        DCRBalancingTool entity = new DCRBalancingTool();
        entity.setId(busObj.getId());
        entity.setDateConfirmed(busObj.getDateConfirmed());
        entity.setDateReConfirmed(busObj.getDateReConfirmed());

        DCRCashierBO dcrCashier = busObj.getDcrCashier();
        try {
            // Filenet
            this.createAndAssociateSnapshot(busObj, entity, isDayOne);

            // WMS Database
            this.updateCOB(busObj);
            result = dcrBalancingToolDao.confirmBalancingTool(entity, isDayOne);
            Long dcrCashierId = dcrCashier.getId();
            dcrCashierDao.changeStatusToPending(dcrCashierId);

        } catch (WMSDaoException e) {
            LOGGER.error(e);
            throw new ServiceException(e);
        } catch (IntegrationException e) {
            LOGGER.error(e);
            throw new ServiceException(e);
        } catch (IOException e) {
            LOGGER.error(e);
            throw new ServiceException(e);
        }

        return result;
    }

    private void updateCOB(DCRBalancingToolBO bt) throws WMSDaoException {
        if (Company.OTHER.getId().equals(bt.getCompany().getId())) {
            List<DCRBalancingToolProductBO> products = bt.getProducts();
            if (CollectionUtils.isNotEmpty(products)) {
                for (DCRBalancingToolProductBO prod : products) {
                    DCROtherBO bo = prod.getDcrOther();

                    if (bo != null) {
                        DCROther entity = new DCROther();
                        this.convertToEntity(bo, entity);
                        dcrOtherDao.updateOther(entity);
                    }
                }
            }
        }

    }

    private void convertToEntity(DCROtherBO dcrOther, DCROther entity) {
        entity.setDcrBalancingToolProductId(dcrOther.getDcrBalancingToolProductId());
        entity.setCashCounterAmt(dcrOther.getCashCounterAmt());
        entity.setCashNonCounter(dcrOther.getCashNonCounter());
        entity.setChequeNonCounter(dcrOther.getChequeNonCounter());
        entity.setChequeOnUs(dcrOther.getChequeOnUs());
        entity.setChequeRegional(dcrOther.getChequeRegional());
        entity.setTotalDollarCheque(dcrOther.getTotalDollarCheque());
        entity.setTotalUsCheckInManila(dcrOther.getTotalUsCheckInManila());
        entity.setTotalUsCheckOutPh(dcrOther.getTotalUsCheckOutPh());
        entity.setId(dcrOther.getId());
        entity.setChequeLocal(dcrOther.getChequeLocal());
    }

    private void createAndAssociateSnapshot(
            final DCRBalancingToolBO busObj,
            final DCRBalancingTool entity,
            final boolean isDayOne) throws IntegrationException, IOException {

        String htmlCode = busObj.getHtmlCode();

        if (StringUtils.isNotEmpty(htmlCode)
                && !StringUtils.contains(htmlCode, "<html>")) {
            htmlCode = "<html>" + htmlCode + "</html>";

            htmlCode = HtmlSnapShotUtil.toSnapshotHtml(htmlCode);
            byte[] arr = null;
            if (StringUtils.isNotEmpty(htmlCode)) {
                arr = htmlCode.getBytes();
            }

            DCRCashierBO dcrCashier = busObj.getDcrCashier();
            Date dcrDate = dcrCashier.getDcr().getDcrDate();
            String docCapsiteId = busObj.getCcId();

            String snapshotStr = "";

            CompanyBO company = busObj.getCompany();
            // boolean isDayOne = busObj.getDateReConfirmed() == null;
            if (CompanyBO.SLGFI.equals(company)
                    || CompanyBO.SLOCPI.equals(company)) {
                snapshotStr = isDayOne ? "_DAY1_SNAPSHOT_" : "_DAY2_SNAPSHOT_";
            } else {
                snapshotStr = "_SNAPSHOT_";
            }

            String customFileName = StringUtils.upperCase(company.getName()
                    + snapshotStr + dcrCashier.getCashier().getAcf2id())
                    + ".html";

            String docId = dcrFilenetIntegrationService.attachFileToWorkItem(
                    dcrDate, docCapsiteId, customFileName, arr);

            if (isDayOne) {
                entity.setSnapShotDay1Id(docId);
            } else {
                entity.setSnapShotDay2Id(docId);
            }
        }

    }

    /**
     * Converts the domain objects (from DAO module) to business objects (from
     * Service module).
     *
     * @param list
     * @return
     * @throws WMSDaoException
     * @throws ServiceException
     */
    private List<DCRBalancingToolBO> convertToBusinessObjects(
            final List<DCRBalancingTool> list) throws WMSDaoException {

        List<DCRBalancingToolBO> createdDCRBalancingTools = new ArrayList<DCRBalancingToolBO>();
        for (DCRBalancingTool tool : list) {
            DCRBalancingToolBO busObj = new DCRBalancingToolBO();
            busObj.setId(tool.getId());
            busObj.setDcrCashier(tool.getDcrCashier().getId());
            busObj.setCompany(tool.getCompanyId());
            busObj.setDateConfirmed(tool.getDateConfirmed());
            busObj.setDateReConfirmed(tool.getDateReConfirmed());

            List<DCRBalancingToolProductBO> children = new ArrayList<DCRBalancingToolProductBO>();
            List<DCRBalancingToolProduct> products = dcrBalancingToolDao.getAllBalancingToolProducts(tool.getId());

            if (CollectionUtils.isNotEmpty(products)) {
                for (DCRBalancingToolProduct product : products) {
                    DCRBalancingToolProductBO subBO = new DCRBalancingToolProductBO();
                    subBO.setId(product.getId());
                    subBO.setProductType(product.getProductType());
                    subBO.setWorksiteAmount(product.getWorksiteAmount());
                    subBO.setDcrBalancingTool(busObj);

                    children.add(subBO);
                }
            }

            busObj.setProducts(children);

            createdDCRBalancingTools.add(busObj);
        }
        return createdDCRBalancingTools;
    }

    /**
     * Saves the corresponding {@link DCRBalancingToolProduct} objects
     * associated with the given list of {@link DCRBalancingTool}.
     *
     * @param list
     * @throws WMSDaoException
     * @throws ServiceException
     */
    private void saveProductsForBalancingTools(final List<DCRBalancingTool> list) throws WMSDaoException {
        for (DCRBalancingTool tool : list) {
            Company company = tool.getCompany();

            for (ProductType type : ProductType.values()) {
                if (company.equals(type.getCompany())) {
                    DCRBalancingToolProduct prod = new DCRBalancingToolProduct();
                    prod.setDcrBalancingTool(tool);
                    prod.setProductType(type);
                    prod.setWorksiteAmount(0.00);

                    prod = dcrBalancingToolDao.saveBalancingToolProduct(prod);

                    if (prod.getId() == null) {
                        throw new WMSDaoException("Unable to save DCRBalancingToolProduct");
                    }

                    if (ProductType.OTHER_DOLLAR.equals(type)
                            || ProductType.OTHER_PESO.equals(type)) {
                        DCROther other = new DCROther();
                        other.setDcrBalancingToolProductId(prod.getId());
                        other.setChequeLocal(0.0);
                        other.setCashCounterAmt(0.0);
                        other.setCashNonCounter(0.0);
                        other.setChequeNonCounter(0.0);
                        other.setChequeOnUs(0.0);
                        other.setChequeRegional(0.0);

                        other = dcrBalancingToolDao.saveOther(other);
                    }
                    // tool.addProduct(prod);
                }
            }
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * ph.com.sunlife.wms.services.DCRCreationService#getDCRCashierBO(java.lang
     * .Long, java.lang.String)
     */
    @Override
    public DCRCashierBO getDCRCashierBO(final Long dcrCashierId, String acf2id) throws ServiceException {
        DCRCashierBO businessObject = null;
        try {
            DCRCashier entity = dcrCashierDao.getById(dcrCashierId);

            if (entity != null
                    && entity.getCashier() != null
                    && StringUtils.equalsIgnoreCase(entity.getCashier()
                    .getAcf2id(), acf2id)) {
                businessObject = new DCRCashierBO();
                businessObject.setCashier(acf2id);
                businessObject.setId(entity.getId());
                businessObject.setStatus(entity.getStatus());

                DCR dcrEntity = entity.getDcr();

                DCRBO dcr = new DCRBO();
                dcr.setId(dcrEntity.getId());
                dcr.setDcrDate(dcrEntity.getDcrDate());
                dcr.setCcId(dcrEntity.getCcId());
                dcr.setCcName(dcrEntity.getCcName());
                businessObject.setDcr(dcr);
            }
        } catch (WMSDaoException e) {
            LOGGER.error(e);
            throw new ServiceException(e);
        }
        return businessObject;
    }

    /**
     * This is the injection setter for {@link DCRBalancingToolDao}.
     *
     * @param dcrBalancingToolDao
     */
    public void setDcrBalancingToolDao(DCRBalancingToolDao dcrBalancingToolDao) {
        this.dcrBalancingToolDao = dcrBalancingToolDao;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * ph.com.sunlife.wms.services.DCRCreationService#buildIpacValues(java.util
     * .List, ph.com.sunlife.wms.services.bo.DCRCashierBO)
     */
    @Override
    public List<DCRIpacValueBO> buildIpacValues(
            List<DCRIpacValueBO> values,
            final DCRCashierBO busObj) throws ServiceException {

        if (CollectionUtils.isEmpty(values)
                || !this.isSameDCRCashier(values, busObj)) {

            values = new ArrayList<DCRIpacValueBO>();
            try {
                DCR dcr = dcrDao.getById(busObj.getDcr().getId());
                DCRCashier dcrCashier = new DCRCashier();
                dcrCashier.setId(busObj.getId());
                dcrCashier.setDcr(dcr);
                dcrCashier.setCashier(busObj.getCashier().getAcf2id());

                List<DCRIpacValue> list = dcrIpacDao.getDCRIPACDailyCollectionSummary(
                        dcr.getCcId(), dcr.getDcrDate(), busObj.getCashier().getAcf2id()); // Added for DCR redesign

                for (DCRIpacValue val : list) {
                    DCRIpacValueBO value = new DCRIpacValueBO();
                    value.setAcf2id(val.getAcf2id());
                    value.setAmount(val.getAmount());
                    value.setPayCode(val.getPayCode());
                    value.setPaySubCode(val.getPaySubCode());
                    value.setPaySubDesc(val.getPaySubDesc());
                    value.setPaySubCurr(val.getPaySubCurr());
                    value.setId(val.getId());
                    value.setProdCode(val.getProdCode());
                    value.setDcrCashierId(busObj.getId());
                    value.setCurrency(val.getCurrency());

                    values.add(value);
                }

            } catch (WMSDaoException e) {
                LOGGER.error(e);
                throw new ServiceException(e);
            }
        }

        return values;
    }

    /**
     * A checker if the given values belong to the {@link DCRCashierBO}.
     *
     * @param values
     * @param busObj
     * @return
     */
    private boolean isSameDCRCashier(
            final List<DCRIpacValueBO> values,
            final DCRCashierBO busObj) {

        boolean isSameDCRCashier = false;

        if (CollectionUtils.isNotEmpty(values)) {

            DCRIpacValueBO bo = (DCRIpacValueBO) CollectionUtils.find(values,
                    new Predicate() {
                @Override
                public boolean evaluate(Object object) {
                    DCRIpacValueBO bo = (DCRIpacValueBO) object;
                    Long dcrCashierId = busObj.getId();
                    return bo.getDcrCashierId().equals(dcrCashierId);
                }
            });

            isSameDCRCashier = (bo != null);
        }

        return isSameDCRCashier;
    }

    private boolean hasIngeniumIntegration(
            final List<DCRBalancingToolProductBO> products) {
        if (CollectionUtils.isNotEmpty(products)) {
            for (DCRBalancingToolProductBO product : products) {
                ProductType type = product.getProductType();
                if (Company.SLOCPI.equals(type.getCompany())
                        || Company.SLGFI.equals(type.getCompany())) {
                    return true;
                }
            }
        }

        return false;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * ph.com.sunlife.wms.services.DCRCreationService#buildBalancingToolProducts
     * (java.util.List, java.util.List)
     */
    @Override
    public List<DCRBalancingToolProductBO> buildBalancingToolProducts(
            final String centerCode,
            final Date processDate,
            final String acf2id,
            final List<DCRIpacValueBO> values,
            final List<DCRBalancingToolProductBO> products) throws ServiceException {
        LOGGER.info("Building values for Balancing Tool...");

        if (values != null) {
            try {
                List<NonPostedTransaction> newBusinessIndividualDTR = null;
                List<NonPostedTransaction> renewalIndividualDTR = null;
                List<NonPostedTransaction> newBusinessVariableDTR = null;
                List<NonPostedTransaction> renewalVariableDTR = null;
                List<NonPostedTransaction> slgfiNewBusinessIndividualDTR = null;
                List<NonPostedTransaction> slgfiRenewalIndividualDTR = null;
                List<NonPostedTransaction> slgfiNewBusinessVariableDTR = null;
                List<NonPostedTransaction> slgfiRenewalVariableDTR = null;
                List<NonPostedTransaction> worksiteNonPostedDTR = null;
                List<NonPostedTransaction> nonPolicyDTR = null;

                if (hasIngeniumIntegration(products)) {
                    LOGGER.debug("This balancing tool has integration to Ingenium");

                    List<NonPostedTransaction> dtrList =
                            dcrIpacDao.getDCRIPACDTR(centerCode, processDate, acf2id); // Added for DCR redesign

                    newBusinessIndividualDTR = getNewBusinessIndividualDTR(dtrList);
                    renewalIndividualDTR = getRenewalIndividualDTR(dtrList);
                    newBusinessVariableDTR = getNewBusinessVariableDTR(dtrList);
                    renewalVariableDTR = getRenewalVariableDTR(dtrList);

                    slgfiNewBusinessIndividualDTR = getSlgfiNewBusinessIndividualDTR(dtrList);
                    slgfiRenewalIndividualDTR = getSlgfiRenewalIndividualDTR(dtrList);
                    slgfiNewBusinessVariableDTR = getSlgfiNewBusinessVariableDTR(dtrList);
                    slgfiRenewalVariableDTR = getSlgfiRenewalVariableDTR(dtrList);

                    worksiteNonPostedDTR = getWorksiteNonPostedDTR(dtrList);
                    nonPolicyDTR = getNonPolicyDTR(dtrList);
                }

                // Map IPAC Values to corresponding Product Totals
                this.mapIpacValuesToProductTotals(products, values);

                List<Long> productIds = new ArrayList<Long>();

                for (DCRBalancingToolProductBO product : products) {
                    ProductType thisProductType = product.getProductType();

                    if (ProductType.OTHER_DOLLAR.equals(thisProductType)
                            || ProductType.OTHER_PESO.equals(thisProductType)) {
                        productIds.add(product.getId());
                    } else if (ProductType.SLIFPI_GAF.equals(thisProductType)) {
                        List<GAFValue> gafValues =
                                dcrIpacDao.getDCRIPACGAF(centerCode, processDate, acf2id); //  Added for DCR redesign
                        this.mapGAFValuesToProduct(product, gafValues);
                    } else if (ProductType.SLOCPI_D_TRADVUL.equals(thisProductType)
                            || ProductType.SLOCPI_P_TRADVUL.equals(thisProductType)) {
                        double sessionTotal = getSessionTotal(processDate,
                                acf2id, product, Company.SLOCPI);
                        product.setSessionTotal(sessionTotal);
                    } else if (ProductType.GREPA_D_TRADVUL.equals(thisProductType)
                            || ProductType.GREPA_P_TRADVUL.equals(thisProductType)) {
                        double sessionTotal = getSessionTotal(processDate, acf2id, product, Company.SLGFI);
                        product.setSessionTotal(sessionTotal);
                    }

                    List<DCRReversal> dcrReversals = dcrReversalDao
                            .getAllByBalancingToolProduct(product.getId());
                    this.mapReversalsToProductTotals(product, dcrReversals);

                    this.populateSlocpiDTRValuesForProductPHP(
                            newBusinessIndividualDTR, renewalIndividualDTR,
                            newBusinessVariableDTR, renewalVariableDTR, product);

                    this.populateSlocpiDTRValuesForProductUSD(
                            newBusinessIndividualDTR, renewalIndividualDTR,
                            newBusinessVariableDTR, renewalVariableDTR, product);

                    this.populateSlgfiDTRValuesForProductPHP(
                            slgfiNewBusinessIndividualDTR,
                            slgfiRenewalIndividualDTR,
                            slgfiNewBusinessVariableDTR,
                            slgfiRenewalVariableDTR, product);

                    this.populateSlgfiDTRValuesForProductUSD(
                            slgfiNewBusinessIndividualDTR,
                            slgfiRenewalIndividualDTR,
                            slgfiNewBusinessVariableDTR,
                            slgfiRenewalVariableDTR, product);

                    if (ProductType.SLOCPI_D_TRADVUL.equals(thisProductType)) {

                        if (CollectionUtils.isNotEmpty(worksiteNonPostedDTR)) {
                            for (NonPostedTransaction trnx : worksiteNonPostedDTR) {
                                if ("USD".equalsIgnoreCase(trnx.getPaymentCurrencyStr())
                                        && StringUtils.equalsIgnoreCase("SLOCP", trnx.getCompanyCode())) {
                                    product.addTotalWorksiteNonPosted(trnx.getAmount());
                                }
                            }
                        }

                        if (CollectionUtils.isNotEmpty(nonPolicyDTR)) {
                            for (NonPostedTransaction trnx : nonPolicyDTR) {
                                if ("USD".equalsIgnoreCase(trnx.getPaymentCurrencyStr())
                                        && StringUtils.equalsIgnoreCase("SLOCP", trnx.getCompanyCode())) {
                                    if (trnx.getPaymentPostStatus() != 1) {
                                        product.addTotalNonPolicy(trnx.getAmount());
                                    } else {
                                        product.setTotalPostedNonPolicy(product.getTotalPostedNonPolicy() + trnx.getAmount());
                                    }
                                }
                            }
                        }
                    }

                    if (ProductType.SLOCPI_P_TRADVUL.equals(thisProductType)) {
                        if (CollectionUtils.isNotEmpty(worksiteNonPostedDTR)) {
                            for (NonPostedTransaction trnx : worksiteNonPostedDTR) {
                                if ("PHP".equalsIgnoreCase(trnx.getPaymentCurrencyStr())
                                        && StringUtils.equalsIgnoreCase("SLOCP", trnx.getCompanyCode())) {
                                    product.addTotalWorksiteNonPosted(trnx.getAmount());
                                }
                            }
                        }

                        if (CollectionUtils.isNotEmpty(nonPolicyDTR)) {
                            for (NonPostedTransaction trnx : nonPolicyDTR) {
                                if ("PHP".equalsIgnoreCase(trnx.getPaymentCurrencyStr())
                                        && StringUtils.equalsIgnoreCase("SLOCP", trnx.getCompanyCode())) {
                                    if (trnx.getPaymentPostStatus() != 1) {
                                        product.addTotalNonPolicy(trnx.getAmount());
                                    } else {
                                        product.setTotalPostedNonPolicy(product.getTotalPostedNonPolicy() + trnx.getAmount());
                                    }
                                }
                            }
                        }

                    }

                    if (ProductType.GREPA_D_TRADVUL.equals(thisProductType)) {

                        if (CollectionUtils.isNotEmpty(worksiteNonPostedDTR)) {
                            for (NonPostedTransaction trnx : worksiteNonPostedDTR) {
                                if ("USD".equalsIgnoreCase(trnx
                                        .getPaymentCurrencyStr())
                                        && StringUtils.equalsIgnoreCase(
                                        "SLGFI", trnx.getCompanyCode())) {
                                    product.addTotalWorksiteNonPosted(trnx
                                            .getAmount());
                                }
                            }
                        }

                        if (CollectionUtils.isNotEmpty(nonPolicyDTR)) {
                            for (NonPostedTransaction trnx : nonPolicyDTR) {
                                if ("USD".equalsIgnoreCase(trnx
                                        .getPaymentCurrencyStr())
                                        && StringUtils.equalsIgnoreCase(
                                        "SLGFI", trnx.getCompanyCode())) {
                                    if (trnx.getPaymentPostStatus() != 1) {
                                        product.addTotalNonPolicy(trnx
                                                .getAmount());
                                    } else {
                                        product.setTotalPostedNonPolicy(product
                                                .getTotalPostedNonPolicy()
                                                + trnx.getAmount());
                                    }
                                }
                            }
                        }
                    }

                    if (ProductType.GREPA_P_TRADVUL.equals(thisProductType)) {
                        if (CollectionUtils.isNotEmpty(worksiteNonPostedDTR)) {
                            for (NonPostedTransaction trnx : worksiteNonPostedDTR) {
                                if ("PHP".equalsIgnoreCase(trnx
                                        .getPaymentCurrencyStr())
                                        && StringUtils.equalsIgnoreCase(
                                        "SLGFI", trnx.getCompanyCode())) {
                                    product.addTotalWorksiteNonPosted(trnx
                                            .getAmount());
                                }
                            }
                        }

                        if (CollectionUtils.isNotEmpty(nonPolicyDTR)) {
                            for (NonPostedTransaction trnx : nonPolicyDTR) {
                                if ("PHP".equalsIgnoreCase(trnx
                                        .getPaymentCurrencyStr())
                                        && StringUtils.equalsIgnoreCase(
                                        "SLGFI", trnx.getCompanyCode())) {
                                    if (trnx.getPaymentPostStatus() != 1) {
                                        product.addTotalNonPolicy(trnx
                                                .getAmount());
                                    } else {
                                        product.setTotalPostedNonPolicy(product
                                                .getTotalPostedNonPolicy()
                                                + trnx.getAmount());
                                    }
                                }
                            }
                        }
                    }
                }

                List<DCROther> dcrOthers = dcrBalancingToolDao
                        .getDcrOthers(productIds);

                if (CollectionUtils.isNotEmpty(dcrOthers)) {
                    for (DCRBalancingToolProductBO product : products) {
                        Long productId = product.getId();
                        for (DCROther other : dcrOthers) {
                            this.mapOtherValuesToProductTotals(product, productId, other);
                        }
                    }
                }

            } catch (WMSDaoException e) {
                throw new ServiceException(e);
            } catch (Exception e) {
                throw new ServiceException(e);
            }
        }
        return products;
    }

    private double getSessionTotal(Date processDate, String userId,
            DCRBalancingToolProductBO product, Company company) {
        double sessionTotal = 0.0;

        if (processDate != null && StringUtils.isNotEmpty(userId)) {

            String userIdWithSuffix = StringUtils.upperCase(userId + INGENIUM_USERID_SUFFIX);
            if (LOGGER.isDebugEnabled() && company != null) {
                LOGGER.debug("Querying Session Total in Ingenium for " + company.getName() + " for user " + userIdWithSuffix);
            }

            double sessionTotalWithSuffix = queryForSessionTotal(processDate,
                    userIdWithSuffix, product, company);

            if (LOGGER.isDebugEnabled() && company != null) {
                LOGGER.debug("Querying Session Total in Ingenium for " + company.getName() + " for user " + userId);
            }

            double sessionTotalWithoutSuffix = queryForSessionTotal(
                    processDate, StringUtils.upperCase(userId), product, company);

            if (sessionTotalWithSuffix > sessionTotalWithoutSuffix) {
                sessionTotal = sessionTotalWithSuffix;
            } else {
                sessionTotal = sessionTotalWithoutSuffix;
            }
        }

        return sessionTotal;
    }

    private double queryForSessionTotal(Date processDate, String userId,
            DCRBalancingToolProductBO product, Company company) {
        UserCollectionsInfo info = new UserCollectionsInfo();
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String processDateStr = df.format(processDate);

        info.setProcessDate(processDateStr);
        info.setUserId(userId);

        ProductType productType = product.getProductType();
        Currency currency = productType.getCurrency();

        if (Currency.PHP.equals(currency)) {
            info.setCurrencyId(INGENIUM_CURRENCY_PESO);
        } else if (Currency.USD.equals(currency)) {
            info.setCurrencyId(INGENIUM_CURRENCY_DOLLAR);
        }

        List<String> outMessages = new ArrayList<String>();
        if (Company.SLOCPI.equals(company)) {
            info = slocpiUserCollectionsTotalUtil.userCollectionsInquiry(info,
                    outMessages);
        } else if (Company.SLGFI.equals(company)) {
            info = slgfiUserCollectionsTotalUtil.userCollectionsInquiry(info,
                    outMessages);
        }

        if (StringUtils.isNotEmpty(info.getErrorMessage())) {
            product.setStotProblem(info.getErrorMessage());
        }

        return info.getTotalCollectionAmount();
    }

    private void mapGAFValuesToProduct(DCRBalancingToolProductBO product,
            List<GAFValue> gafValues) {
        if (CollectionUtils.isNotEmpty(gafValues)) {
            final String cashPayType = "1";
            final String checkPayType = "2";
            final String cardPayType = "3";
            final String nonCashPayType = "4";

            for (GAFValue val : gafValues) {
                String payType = StringUtils.trim(val.getPayType());
                // Double amount = val.getTotalAmount();

                if (StringUtils.equals(cashPayType, payType)) {
                    product.addTotalCashCounter(val.getCashAmount());
                } else if (StringUtils.equals(checkPayType, payType)) {
                    product.setTotalCheckGaf(product.getTotalCheckGaf()
                            + val.getCheckAmount());
                } else if (StringUtils.equals(cardPayType, payType)) {
                    product.setTotalCardGaf(product.getTotalCardGaf()
                            + val.getCardAmount());
                } else if (StringUtils.equals(nonCashPayType, payType)) {
                    product.addTotalNonCash(val.getNcashAmount());
                }

            }
        }
    }

    private void populateSlgfiDTRValuesForProductPHP(
            List<NonPostedTransaction> newBusinessIndividualDTR,
            List<NonPostedTransaction> renewalIndividualDTR,
            List<NonPostedTransaction> newBusinessVariableDTR,
            List<NonPostedTransaction> renewalVariableDTR,
            DCRBalancingToolProductBO product) {

        if (ProductType.GREPA_P_TRADVUL.equals(product.getProductType())) {

            final String phpCurrencyString = Currency.PHP.getName();

            if (CollectionUtils.isNotEmpty(newBusinessIndividualDTR)) {
                for (NonPostedTransaction trnx : newBusinessIndividualDTR) {
                    if (phpCurrencyString.equalsIgnoreCase(trnx
                            .getPaymentCurrencyStr())) {
                        product.addTotalNewBusinessIndividual(trnx.getAmount());
                    }
                }
            }

            if (CollectionUtils.isNotEmpty(renewalIndividualDTR)) {
                for (NonPostedTransaction trnx : renewalIndividualDTR) {
                    if (phpCurrencyString.equalsIgnoreCase(trnx
                            .getPaymentCurrencyStr())) {
                        product.addTotalIndividualRenewal(trnx.getAmount());
                    }
                }
            }

            if (CollectionUtils.isNotEmpty(newBusinessVariableDTR)) {
                for (NonPostedTransaction trnx : newBusinessVariableDTR) {
                    if (phpCurrencyString.equalsIgnoreCase(trnx
                            .getPaymentCurrencyStr())) {
                        product.addTotalNewBusinessVariable(trnx.getAmount());
                    }
                }
            }

            if (CollectionUtils.isNotEmpty(renewalVariableDTR)) {
                for (NonPostedTransaction trnx : renewalVariableDTR) {
                    if (phpCurrencyString.equalsIgnoreCase(trnx
                            .getPaymentCurrencyStr())) {
                        product.addTotalVariableRenewal(trnx.getAmount());
                    }
                }
            }

        }
    }

    private void populateSlocpiDTRValuesForProductPHP(
            List<NonPostedTransaction> newBusinessIndividualDTR,
            List<NonPostedTransaction> renewalIndividualDTR,
            List<NonPostedTransaction> newBusinessVariableDTR,
            List<NonPostedTransaction> renewalVariableDTR,
            DCRBalancingToolProductBO product) {

        if (ProductType.SLOCPI_P_TRADVUL.equals(product.getProductType())) {

            final String phpCurrencyString = Currency.PHP.getName();

            if (CollectionUtils.isNotEmpty(newBusinessIndividualDTR)) {
                for (NonPostedTransaction trnx : newBusinessIndividualDTR) {
                    if (phpCurrencyString.equalsIgnoreCase(trnx
                            .getPaymentCurrencyStr())) {
                        product.addTotalNewBusinessIndividual(trnx.getAmount());
                    }
                }
            }

            if (CollectionUtils.isNotEmpty(renewalIndividualDTR)) {
                for (NonPostedTransaction trnx : renewalIndividualDTR) {
                    if (phpCurrencyString.equalsIgnoreCase(trnx
                            .getPaymentCurrencyStr())) {
                        product.addTotalIndividualRenewal(trnx.getAmount());
                    }
                }
            }

            if (CollectionUtils.isNotEmpty(newBusinessVariableDTR)) {
                for (NonPostedTransaction trnx : newBusinessVariableDTR) {
                    if (phpCurrencyString.equalsIgnoreCase(trnx
                            .getPaymentCurrencyStr())) {
                        product.addTotalNewBusinessVariable(trnx.getAmount());
                    }
                }
            }

            if (CollectionUtils.isNotEmpty(renewalVariableDTR)) {
                for (NonPostedTransaction trnx : renewalVariableDTR) {
                    if (phpCurrencyString.equalsIgnoreCase(trnx
                            .getPaymentCurrencyStr())) {
                        product.addTotalVariableRenewal(trnx.getAmount());
                    }
                }
            }

        }
    }

    private void populateSlgfiDTRValuesForProductUSD(
            List<NonPostedTransaction> newBusinessIndividualDTR,
            List<NonPostedTransaction> renewalIndividualDTR,
            List<NonPostedTransaction> newBusinessVariableDTR,
            List<NonPostedTransaction> renewalVariableDTR,
            DCRBalancingToolProductBO product) {

        if (ProductType.GREPA_D_TRADVUL.equals(product.getProductType())) {

            final String usdCurrencyStr = Currency.USD.getName();

            if (CollectionUtils.isNotEmpty(newBusinessIndividualDTR)) {
                for (NonPostedTransaction trnx : newBusinessIndividualDTR) {
                    if (usdCurrencyStr.equalsIgnoreCase(trnx
                            .getPaymentCurrencyStr())) {
                        product.addTotalNewBusinessIndividual(trnx.getAmount());
                    }
                }
            }

            if (CollectionUtils.isNotEmpty(renewalIndividualDTR)) {
                for (NonPostedTransaction trnx : renewalIndividualDTR) {
                    if (usdCurrencyStr.equalsIgnoreCase(trnx
                            .getPaymentCurrencyStr())) {
                        product.addTotalIndividualRenewal(trnx.getAmount());
                    }
                }
            }

            if (CollectionUtils.isNotEmpty(newBusinessVariableDTR)) {
                for (NonPostedTransaction trnx : newBusinessVariableDTR) {
                    if (usdCurrencyStr.equalsIgnoreCase(trnx
                            .getPaymentCurrencyStr())) {
                        product.addTotalNewBusinessVariable(trnx.getAmount());
                    }
                }
            }

            if (CollectionUtils.isNotEmpty(renewalVariableDTR)) {
                for (NonPostedTransaction trnx : renewalVariableDTR) {
                    if (usdCurrencyStr.equalsIgnoreCase(trnx
                            .getPaymentCurrencyStr())) {
                        product.addTotalVariableRenewal(trnx.getAmount());
                    }
                }
            }

        }
    }

    private void populateSlocpiDTRValuesForProductUSD(
            List<NonPostedTransaction> newBusinessIndividualDTR,
            List<NonPostedTransaction> renewalIndividualDTR,
            List<NonPostedTransaction> newBusinessVariableDTR,
            List<NonPostedTransaction> renewalVariableDTR,
            DCRBalancingToolProductBO product) {

        if (ProductType.SLOCPI_D_TRADVUL.equals(product.getProductType())) {

            final String usdCurrencyStr = Currency.USD.getName();

            if (CollectionUtils.isNotEmpty(newBusinessIndividualDTR)) {
                for (NonPostedTransaction trnx : newBusinessIndividualDTR) {
                    if (usdCurrencyStr.equalsIgnoreCase(trnx
                            .getPaymentCurrencyStr())) {
                        product.addTotalNewBusinessIndividual(trnx.getAmount());
                    }
                }
            }

            if (CollectionUtils.isNotEmpty(renewalIndividualDTR)) {
                for (NonPostedTransaction trnx : renewalIndividualDTR) {
                    if (usdCurrencyStr.equalsIgnoreCase(trnx
                            .getPaymentCurrencyStr())) {
                        product.addTotalIndividualRenewal(trnx.getAmount());
                    }
                }
            }

            if (CollectionUtils.isNotEmpty(newBusinessVariableDTR)) {
                for (NonPostedTransaction trnx : newBusinessVariableDTR) {
                    if (usdCurrencyStr.equalsIgnoreCase(trnx
                            .getPaymentCurrencyStr())) {
                        product.addTotalNewBusinessVariable(trnx.getAmount());
                    }
                }
            }

            if (CollectionUtils.isNotEmpty(renewalVariableDTR)) {
                for (NonPostedTransaction trnx : renewalVariableDTR) {
                    if (usdCurrencyStr.equalsIgnoreCase(trnx
                            .getPaymentCurrencyStr())) {
                        product.addTotalVariableRenewal(trnx.getAmount());
                    }
                }
            }

        }
    }

    private List<NonPostedTransaction> getNewBusinessIndividualDTR(
            List<NonPostedTransaction> dtrList) {

        List<NonPostedTransaction> matchedElements = WMSCollectionUtils
                .findMatchedElements(dtrList, new Predicate() {
            @Override
            public boolean evaluate(Object object) {
                NonPostedTransaction trnx = (NonPostedTransaction) object;

                // boolean isMatch = (trnx.getPaymentPostStatus() == 0);
                boolean isMatch = "IL".equals(trnx.getProductCode())
                        && ("I".equals(trnx.getTransactionType())
                        || "A".equals(trnx.getTransactionType())
                        || "C".equals(trnx.getTransactionType()));

                return isMatch;
            }
        });
        return matchedElements;
    }

    private List<NonPostedTransaction> getRenewalIndividualDTR(
            List<NonPostedTransaction> dtrList) {

        List<NonPostedTransaction> matchedElements = WMSCollectionUtils
                .findMatchedElements(dtrList, new Predicate() {
            @Override
            public boolean evaluate(Object object) {
                NonPostedTransaction trnx = (NonPostedTransaction) object;

                // boolean isMatch = (trnx.getPaymentPostStatus() == 0);
                boolean isMatch = "IL".equals(trnx.getProductCode())
                        && "R".equals(trnx.getTransactionType());

                return isMatch;
            }
        });
        return matchedElements;
    }

    private List<NonPostedTransaction> getNewBusinessVariableDTR(
            List<NonPostedTransaction> dtrList) {

        List<NonPostedTransaction> matchedElements = WMSCollectionUtils
                .findMatchedElements(dtrList, new Predicate() {
            @Override
            public boolean evaluate(Object object) {
                NonPostedTransaction trnx = (NonPostedTransaction) object;

                boolean isMatch = "VL".equals(trnx.getProductCode())
                        && "VI".equals(trnx.getTransactionType());

                return isMatch;
            }
        });
        return matchedElements;
    }

    private List<NonPostedTransaction> getRenewalVariableDTR(
            List<NonPostedTransaction> dtrList) {

        List<NonPostedTransaction> matchedElements = WMSCollectionUtils
                .findMatchedElements(dtrList, new Predicate() {
            @Override
            public boolean evaluate(Object object) {
                NonPostedTransaction trnx = (NonPostedTransaction) object;

                // boolean isMatch = (trnx.getPaymentPostStatus() == 0);
                boolean isMatch = "VL".equals(trnx.getProductCode())
                        && "VR".equals(trnx.getTransactionType());

                return isMatch;
            }
        });
        return matchedElements;
    }

    private List<NonPostedTransaction> getSlgfiNewBusinessIndividualDTR(
            List<NonPostedTransaction> dtrList) {

        List<NonPostedTransaction> matchedElements = WMSCollectionUtils
                .findMatchedElements(dtrList, new Predicate() {
            @Override
            public boolean evaluate(Object object) {
                NonPostedTransaction trnx = (NonPostedTransaction) object;

                // boolean isMatch = (trnx.getPaymentPostStatus() == 0);
                boolean isMatch = "RL".equals(trnx.getProductCode())
                        && "RI".equals(trnx.getTransactionType());

                return isMatch;
            }
        });
        return matchedElements;
    }

    private List<NonPostedTransaction> getSlgfiRenewalIndividualDTR(
            List<NonPostedTransaction> dtrList) {

        List<NonPostedTransaction> matchedElements = WMSCollectionUtils
                .findMatchedElements(dtrList, new Predicate() {
            @Override
            public boolean evaluate(Object object) {
                NonPostedTransaction trnx = (NonPostedTransaction) object;

                boolean isMatch = "RL".equals(trnx.getProductCode())
                        && "RR".equals(trnx.getTransactionType());

                return isMatch;
            }
        });
        return matchedElements;
    }

    private List<NonPostedTransaction> getSlgfiNewBusinessVariableDTR(
            List<NonPostedTransaction> dtrList) {

        List<NonPostedTransaction> matchedElements = WMSCollectionUtils
                .findMatchedElements(dtrList, new Predicate() {
            @Override
            public boolean evaluate(Object object) {
                NonPostedTransaction trnx = (NonPostedTransaction) object;

                // boolean isMatch = (trnx.getPaymentPostStatus() == 0);
                boolean isMatch = "RV".equals(trnx.getProductCode())
                        && "VB".equals(trnx.getTransactionType());

                return isMatch;
            }
        });
        return matchedElements;
    }

    private List<NonPostedTransaction> getSlgfiRenewalVariableDTR(
            List<NonPostedTransaction> dtrList) {

        List<NonPostedTransaction> matchedElements = WMSCollectionUtils
                .findMatchedElements(dtrList, new Predicate() {
            @Override
            public boolean evaluate(Object object) {
                NonPostedTransaction trnx = (NonPostedTransaction) object;

                // boolean isMatch = (trnx.getPaymentPostStatus() == 0);
                boolean isMatch = "RV".equals(trnx.getProductCode())
                        && "VV".equals(trnx.getTransactionType());

                return isMatch;
            }
        });
        return matchedElements;
    }

    private List<NonPostedTransaction> getNonPolicyDTR(
            List<NonPostedTransaction> dtrList) {

        List<NonPostedTransaction> matchedElements = WMSCollectionUtils
                .findMatchedElements(dtrList, new Predicate() {
            @Override
            public boolean evaluate(Object object) {
                NonPostedTransaction trnx = (NonPostedTransaction) object;
                return "N".equals(trnx.getTransactionType());

            }
        });
        return matchedElements;
    }

    private List<NonPostedTransaction> getWorksiteNonPostedDTR(
            List<NonPostedTransaction> dtrList) {

        List<NonPostedTransaction> matchedElements = WMSCollectionUtils
                .findMatchedElements(dtrList, new Predicate() {
            @Override
            public boolean evaluate(Object object) {
                NonPostedTransaction trnx = (NonPostedTransaction) object;
                // boolean isMatch = (trnx.getPaymentPostStatus() == 0);
                boolean isMatch = "W".equals(trnx.getTransactionType());

                return isMatch;
            }
        });
        return matchedElements;
    }

    /**
     * Associates corresponding {@link DCRReversalBO} entries into the right
     * {@link DCRBalancingToolProductBO}.
     *
     * @param product
     * @param dcrReversals
     * @throws IllegalAccessException
     * @throws InvocationTargetException
     */
    private void mapReversalsToProductTotals(DCRBalancingToolProductBO product,
            List<DCRReversal> dcrReversals) throws IllegalAccessException,
            InvocationTargetException {
        List<DCRReversalBO> dcrReversalBusObjs = new ArrayList<DCRReversalBO>();

        double totalReversalAmt = 0.0;
        if (CollectionUtils.isNotEmpty(dcrReversals)) {
            for (DCRReversal dcrReversal : dcrReversals) {
                DCRReversalBO dcrReversalBO = new DCRReversalBO();
                BeanUtils.copyProperties(dcrReversalBO, dcrReversal);

                String remarks = dcrReversal.getRemarks();
                String remarksWithoutLineBreaks = StringUtils.replace(remarks, "& #39;", "'");
                dcrReversalBO.setRemarks(remarksWithoutLineBreaks);

                dcrReversalBusObjs.add(dcrReversalBO);

                totalReversalAmt += dcrReversal.getAmount();
            }
        }

        product.setTotalReversalAmt(totalReversalAmt);
        product.setDcrReversals(dcrReversalBusObjs);
    }

    /**
     * This attaches the {@link DCROtherBO} object into
     * {@link DCRBalancingToolProductBO} which is only applicable to OTHER
     * {@link Company}.
     *
     * @param product
     * @param productId
     * @param other
     * @throws IllegalAccessException
     * @throws InvocationTargetException
     */
    private void mapOtherValuesToProductTotals(
            DCRBalancingToolProductBO product, Long productId, DCROther other)
            throws IllegalAccessException, InvocationTargetException {
        if (other.getDcrBalancingToolProductId().equals(productId)) {
            DCROtherBO dcrOtherBO = new DCROtherBO();
            BeanUtils.copyProperties(dcrOtherBO, other);

            product.setTotalCashCounter(dcrOtherBO.getCashCounterAmt());
            product.setTotalCashNonCounter(dcrOtherBO.getCashNonCounter());
            product.setTotalCheckLocal(dcrOtherBO.getChequeLocal());
            product.setTotalCheckOnUs(dcrOtherBO.getChequeOnUs());
            product.setTotalCheckRegional(dcrOtherBO.getChequeRegional());
            product.setTotalCheckNonCounter(dcrOtherBO.getChequeNonCounter());
            product.setTotalDollarCheque(dcrOtherBO.getTotalDollarCheque());
            product.setTotalUsCheckInManila(dcrOtherBO.getTotalUsCheckInManila());
            product.setTotalUsCheckOutPh(dcrOtherBO.getTotalUsCheckOutPh());
            product.setDcrOther(dcrOtherBO);
        }
    }

    private void mapIpacValuesToProductTotals(
            List<DCRBalancingToolProductBO> productTotals,
            List<DCRIpacValueBO> ipacValues) {
        LOGGER.info("Mapping IPAC Values to Product Columns...");

        if (CollectionUtils.isNotEmpty(ipacValues)) {
            for (DCRIpacValueBO value : ipacValues) {
                String paySubCurr = value.getPaySubCurr();
                String prodCode = value.getProdCode();
                String payCode = value.getPayCode();
                double amount = value.getAmount();

                ProductType type = ProductType.prodCodeCurrencyToProductType(prodCode, paySubCurr);

                DCRBalancingToolProductBO mappedPT = null;
                if (type != null) {
                    for (DCRBalancingToolProductBO pt : productTotals) {
                        if (type.equals(pt.getProductType())) {
                            mappedPT = pt;
                        }
                    }
                }

                if (mappedPT != null) {
                    if ("NCSH".equalsIgnoreCase(payCode)) {
                        mappedPT.setTotalNonCash(mappedPT.getTotalNonCash() + amount);
                    } else if ("CSH".equalsIgnoreCase(payCode)) {
                        this.mapCashValueToTotals(mappedPT, value);
                    } else if ("CHK".equalsIgnoreCase(payCode)) {
                        this.mapCheckValueToTotals(mappedPT, value);
                    } else if ("CRD".equalsIgnoreCase(payCode)) {
                        this.mapCardValueToTotals(mappedPT, value);
                    }
                }
            }
        }
    }

    private void mapCheckValueToTotals(
            DCRBalancingToolProductBO mappedPT,
            DCRIpacValueBO value) {
        
        String prodCode = value.getProdCode(); // Added for PCO - Not include PCO
        String paySubCode = value.getPaySubCode();
        double amount = value.getAmount();

        if ("R/C".equalsIgnoreCase(paySubCode) && !"MF".equals(prodCode)) { // Added for PCO - Not include PCO
            mappedPT.setTotalCheckRegional(mappedPT.getTotalCheckRegional()+ amount);
        } else if ("L/C".equalsIgnoreCase(paySubCode) && !"MF".equals(prodCode)) { // Added for PCO - Not include PCO
            mappedPT.setTotalCheckLocal(mappedPT.getTotalCheckLocal() + amount);
        } else if ("O/C".equalsIgnoreCase(paySubCode) && !"MF".equals(prodCode)) { // Added for PCO - Not include PCO
            mappedPT.setTotalCheckOT(mappedPT.getTotalCheckOT() + amount);
        } else if ("PMO".equalsIgnoreCase(paySubCode) && !"MF".equals(prodCode)) { // Added for PCO - Not include PCO
            ProductType productType = mappedPT.getProductType();
            if (productType != null
                    && (Company.SLAMCID.equals(productType.getCompany()) || Company.SLAMCIP.equals(productType.getCompany()))) {
                mappedPT.setTotalCheckNonCounter(mappedPT.getTotalCheckNonCounter() + amount);
            } else {
                mappedPT.setTotalPmo(mappedPT.getTotalPmo() + amount);
            }
        } else if ("OUC".equalsIgnoreCase(paySubCode) && !"MF".equals(prodCode)) { // Added for PCO - Not include PCO
            mappedPT.setTotalCheckOnUs(mappedPT.getTotalCheckOnUs() + amount);
        } else if ("CDM".equalsIgnoreCase(paySubCode) && !"MF".equals(prodCode)) { // Added for PCO - Not include PCO
            mappedPT.setTotalUsCheckInManila(mappedPT.getTotalUsCheckInManila() + amount);
        } else if ("COP".equalsIgnoreCase(paySubCode) && !"MF".equals(prodCode)) { // Added for PCO - Not include PCO
            mappedPT.setTotalUsCheckOutPh(mappedPT.getTotalUsCheckOutPh() + amount);
        } else if ("DCK".equalsIgnoreCase(paySubCode) && !"MF".equals(prodCode)) { // Added for PCO - Not include PCO
            mappedPT.setTotalDollarCheque(mappedPT.getTotalDollarCheque() + amount);
        } else if (("BCP".equalsIgnoreCase(paySubCode)
                || "BCU".equalsIgnoreCase(paySubCode)) && !"MF".equals(prodCode)) { // Added for PCO - Not include PCO
            mappedPT.setTotalCheckNonCounter(mappedPT.getTotalCheckNonCounter() + amount);
        } else if (("CGD".equalsIgnoreCase(paySubCode)
                || "CGL".equalsIgnoreCase(paySubCode)
                || "GYR".equalsIgnoreCase(paySubCode)) && !"MF".equals(prodCode)) { // Added for PCO - Not include PCO
            mappedPT.setPaymentFromInsurer(mappedPT.getPaymentFromInsurer() + amount);
        } else if ("MF".equalsIgnoreCase(paySubCode)) { // Added for PCO
            mappedPT.setTotalMf(mappedPT.getTotalMf() + amount);
        } else if ("SPC".equalsIgnoreCase(paySubCode)) { // Added for PCO Invest
            mappedPT.setTotalPcoInvest(mappedPT.getTotalPcoInvest() + amount);
        } else {
            mappedPT.setTotalCheckNonCounter(mappedPT.getTotalCheckNonCounter() + amount);
        }
    }

    private void mapCashValueToTotals(
            DCRBalancingToolProductBO mappedPT,
            DCRIpacValueBO value) {
        String paySubCode = value.getPaySubCode();
        double amount = value.getAmount();

        if ("CPH".equalsIgnoreCase(paySubCode)
                || "UCH".equalsIgnoreCase(paySubCode)) {
            mappedPT.setTotalCashCounter(mappedPT.getTotalCashCounter() + amount);
        } else if ("EPS".equalsIgnoreCase(paySubCode)) {
            mappedPT.setTotalCashEpsBpi(mappedPT.getTotalCashEpsBpi() + amount);
        } else if ("MF".equalsIgnoreCase(paySubCode)) { // Added for PCO
            mappedPT.setTotalMf(mappedPT.getTotalMf() + amount);
        } else if ("SPC".equalsIgnoreCase(paySubCode)) { // Added for PCO Invest
            mappedPT.setTotalPcoInvest(mappedPT.getTotalPcoInvest()+ amount);
        } else {
            mappedPT.setTotalCashNonCounter(mappedPT.getTotalCashNonCounter() + amount);
        }
    }

    private void mapCardValueToTotals(
            DCRBalancingToolProductBO mappedPT,
            DCRIpacValueBO value) {
        
        String prodCode = value.getProdCode(); // Added for PCO - Not include PCO
        String paySubCode = value.getPaySubCode();
        double amount = value.getAmount();

        if (("BPP".equalsIgnoreCase(paySubCode)
                || "BPU".equalsIgnoreCase(paySubCode)) && !"MF".equals(prodCode)) {  // Added for PCO - Not include PCO
            mappedPT.setTotalPosBpi(mappedPT.getTotalPosBpi() + amount);
        } else if (("CPP".equalsIgnoreCase(paySubCode)
                || "CPU".equalsIgnoreCase(paySubCode)) && !"MF".equals(prodCode)) { // Added for PCO - Not include PCO
            mappedPT.setTotalPosCtb(mappedPT.getTotalPosCtb() + amount);
        } else if (("EPP".equalsIgnoreCase(paySubCode)
                || "EPU".equalsIgnoreCase(paySubCode)) && !"MF".equals(prodCode)) { // Added for PCO - Not include PCO
            mappedPT.setTotalPosBdo(mappedPT.getTotalPosBdo() + amount);
        } else if (("HPP".equalsIgnoreCase(paySubCode)
                || "HPU".equalsIgnoreCase(paySubCode)) && !"MF".equals(prodCode)) { // Added for PCO - Not include PCO
            mappedPT.setTotalPosHsbc(mappedPT.getTotalPosHsbc() + amount);
        } else if (("SPP".equalsIgnoreCase(paySubCode)
                || "SPU".equalsIgnoreCase(paySubCode)) && !"MF".equals(prodCode)) { // Added for PCO - Not include PCO
            mappedPT.setTotalPosScb(mappedPT.getTotalPosScb() + amount);
        } else if (("RPP".equalsIgnoreCase(paySubCode)
                || "RPU".equalsIgnoreCase(paySubCode)) && !"MF".equals(prodCode)) { // Added for PCO - Not include PCO
            mappedPT.setTotalPosRcbc(mappedPT.getTotalPosRcbc() + amount);
        } else if (("BMP".equalsIgnoreCase(paySubCode)
                || "BMU".equalsIgnoreCase(paySubCode)) && !"MF".equals(prodCode)) { // Added for PCO - Not include PCO
            mappedPT.setTotalMdsBpi(mappedPT.getTotalMdsBpi() + amount);
        } else if (("CMP".equalsIgnoreCase(paySubCode)
                || "CMU".equalsIgnoreCase(paySubCode)) && !"MF".equals(prodCode)) { // Added for PCO - Not include PCO
            mappedPT.setTotalMdsCtb(mappedPT.getTotalMdsCtb() + amount);
        } else if (("EMP".equalsIgnoreCase(paySubCode)
                || "EMU".equalsIgnoreCase(paySubCode)) && !"MF".equals(prodCode)) { // Added for PCO - Not include PCO
            mappedPT.setTotalMdsBdo(mappedPT.getTotalMdsBdo() + amount);
        } else if (("HMP".equalsIgnoreCase(paySubCode)
                || "HMU".equalsIgnoreCase(paySubCode)) && !"MF".equals(prodCode)) { // Added for PCO - Not include PCO
            mappedPT.setTotalMdsHsbc(mappedPT.getTotalMdsHsbc() + amount);
        } else if (("SMP".equalsIgnoreCase(paySubCode)
                || "SMU".equalsIgnoreCase(paySubCode)) && !"MF".equals(prodCode)) { // Added for PCO - Not include PCO
            mappedPT.setTotalMdsSbc(mappedPT.getTotalMdsSbc() + amount);
        } else if (("RMP".equalsIgnoreCase(paySubCode)
                || "RMU".equalsIgnoreCase(paySubCode)) && !"MF".equals(prodCode)) { // Added for PCO - Not include PCO
            mappedPT.setTotalMdsRcbc(mappedPT.getTotalMdsRcbc() + amount);
        } else if ("CMM".equalsIgnoreCase(paySubCode) && !"MF".equals(prodCode)) { // Added for PCO - Not include PCO
            mappedPT.setTotalAutoCtb(mappedPT.getTotalAutoCtb() + amount);
        } else if ("SMM".equalsIgnoreCase(paySubCode) && !"MF".equals(prodCode)) { // Added for PCO - Not include PCO
            mappedPT.setTotalAutoScb(mappedPT.getTotalAutoScb() + amount);
        } else if (("RAU".equalsIgnoreCase(paySubCode)
                || "RMM".equalsIgnoreCase(paySubCode)) && !"MF".equals(prodCode)) { // Added for PCO - Not include PCO
            mappedPT.setTotalAutoRcbc(mappedPT.getTotalAutoRcbc() + amount);
        } else if ("ICT".equalsIgnoreCase(paySubCode) && !"MF".equals(prodCode)) { // Added for PCO - Not include PCO
            mappedPT.setTotalSunlinkOnline(mappedPT.getTotalSunlinkOnline() + amount);
        } else if ("HSP".equalsIgnoreCase(paySubCode) && !"MF".equals(prodCode)) { // Added for PCO - Not include PCO
            mappedPT.setTotalSunlinkHsbcPeso(mappedPT.getTotalSunlinkHsbcPeso() + amount);
        } else if ("HSU".equalsIgnoreCase(paySubCode) && !"MF".equals(prodCode)) { // Added for PCO - Not include PCO
            mappedPT.setTotalSunlinkHsbcDollar(mappedPT.getTotalSunlinkHsbcDollar() + amount);
        } else if ("EPS".equalsIgnoreCase(paySubCode) && !"MF".equals(prodCode)) { // Added for PCO - Not include PCO
            mappedPT.setTotalEpsBpi(mappedPT.getTotalEpsBpi() + amount);
        } else if ("MF".equalsIgnoreCase(paySubCode)) { // Added for PCO
            mappedPT.setTotalMf(mappedPT.getTotalMf() + amount);
        } else if ("SPC".equalsIgnoreCase(paySubCode)) { // Added for PCO Invest
            mappedPT.setTotalPcoInvest(mappedPT.getTotalPcoInvest() + amount);
        }
    }

    /**
     * Injection setter for {@link DCRReversalDao}.
     *
     * @param dcrReversalDao
     */
    public void setDcrReversalDao(DCRReversalDao dcrReversalDao) {
        this.dcrReversalDao = dcrReversalDao;
    }

    @Override
    public boolean saveWorkSiteAmount(List<DCRBalancingToolProductBO> products)
            throws ServiceException {
        boolean isSavedSuccessfully = true;
        for (DCRBalancingToolProductBO product : products) {
            DCRBalancingToolProduct entity = new DCRBalancingToolProduct();
            entity.setId(product.getId());
            entity.setWorksiteAmount(product.getWorksiteAmount());
            try {
                isSavedSuccessfully = isSavedSuccessfully
                        && dcrBalancingToolDao.saveWorkSite(entity);
            } catch (WMSDaoException e) {
                LOGGER.error(e);
                throw new ServiceException(e);
            }
        }

        return isSavedSuccessfully;
    }

    public void setIpacDao(IpacDao ipacDao) {
        this.ipacDao = ipacDao;
    }

    public void setDcrFilenetIntegrationService(
            DCRFilenetIntegrationService dcrFilenetIntegrationService) {
        this.dcrFilenetIntegrationService = dcrFilenetIntegrationService;
    }

    @Override
    public boolean createDCRWorkItemsNow() throws ServiceException {
        Date today = cachingService.getWmsDcrSystemDate();
        return this.createDCRWorkItems(today);
    }

    @Override
    public boolean createDCRWorkItems(Date date) throws ServiceException {
        String formattedDateStr = WMSDateUtil.toFormattedDateStr(date);

        System.out.println("Creation Date: " + formattedDateStr);
        LOGGER.info("Creation Date: " + formattedDateStr);

        boolean isSuccessful = false;
        try {
            this.initiateDCRWorkItems(WMSDateUtil.startOfTheDay(date), false);
            this.successfulJobRun(date);
            isSuccessful = true;
        } catch (ServiceException e) {
            // Change according to Healthcheck
            // *Problem was identified as inconsistent logging
            // *Change was remove setting of value of variable and add logging
            LOGGER.error("Problem in creating DCR workitems: ", e);
        } catch (IntegrationException e) {
            LOGGER.error("Problem in creating DCR workitems: ", e);
            ServiceException ex = new ServiceException(e);
            this.failedJobRun(date, ex);
        }

        return isSuccessful;
    }

    private void failedJobRun(Date today, ServiceException e)
            throws ServiceException {
        try {
            LOGGER.info("Job Run has failed to run this time");
            String stackTrace = ExceptionUtil.getStackTrace(e, 1000);
            JobDataErrorLog log = new JobDataErrorLog();
            log.setDateTime(today);
            log.setErrorMessage(stackTrace);
            jobDataDao.createErrorLog(log);

            JobData jobData = new JobData();
            jobData.setId(DCR_CREATION_JOB_ID);
            jobData.setLastRun(today);
            jobData.setSuccessful(false);
            jobDataDao.updateJobData(jobData);

        } catch (WMSDaoException ex1) {
            LOGGER.error(ex1);
            throw new ServiceException(ex1);
        }
    }

    private void successfulJobRun(Date today) throws ServiceException {
        try {
            LOGGER.info("Job Run has succeeded to run this time");

            JobData jobData = new JobData();
            jobData.setId(DCR_CREATION_JOB_ID);
            jobData.setLastRun(today);
            jobData.setSuccessful(true);
            jobDataDao.updateJobData(jobData);
        } catch (WMSDaoException ex1) {
            LOGGER.error(ex1);
            throw new ServiceException(ex1);
        }
    }

    public void setSlocpiUserCollectionsTotalUtil(
            UserCollectionsTotalUtil slocpiUserCollectionsTotalUtil) {
        this.slocpiUserCollectionsTotalUtil = slocpiUserCollectionsTotalUtil;
    }

    public void setSlgfiUserCollectionsTotalUtil(
            UserCollectionsTotalUtil slgfiUserCollectionsTotalUtil) {
        this.slgfiUserCollectionsTotalUtil = slgfiUserCollectionsTotalUtil;
    }

    public void setDcrOtherDao(DCROtherDao dcrOtherDao) {
        this.dcrOtherDao = dcrOtherDao;
    }

    public void setJobDataDao(JobDataDao jobDataDao) {
        this.jobDataDao = jobDataDao;
    }

    public void setCachingService(CachingService cachingService) {
        this.cachingService = cachingService;
    }

    /**
     * Added for DCR redesign
     * @return the dcrIpacDao - will be returned when implementing DCR redesign
     */
    public DCRIPACDao getDcrIpacDao() {
        return dcrIpacDao;
    }

    /**
     * Added for DCR redesign
     * @param dcrIpacDao the dcrIpacDao to set - will be returned when implementing DCR redesign
     */
    public void setDcrIpacDao(DCRIPACDao dcrIpacDao) {
        this.dcrIpacDao = dcrIpacDao;
    }
}
