package ph.com.sunlife.wms.web.controller.binder;

/*
 * $Id: BinderAware.java,v 1.2 2012/10/31 06:00:19 PV70 Exp $
 *
 * Copyright (c) 2012 Sun Life of Canada (Philippines) Inc.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of SLOCPI.
 */
import org.springframework.validation.Errors;

/**
 * Marks objects that could control how they're binded. Also {@link BinderAware}
 * commands can validate themselves.
 *
 * @author Zainal Limpao
 */
// Change according to Healthcheck
// *Problem was declared as public/protected
// *Change accordingly - but this may be remove in part 2 as it seems not being used:
//   -change to abstract class to be able to change to private
//   -set variables to private and set getters
public abstract class BinderAware {

    /**
     * The ALL_FIELDS_ARE_ALLOWED String[].
     */
    private static final String[] ALL_FIELDS_ARE_ALLOWED = new String[0];
    /**
     * The NO_REQUIRED_FIELDS String[].
     */
    private static final String[] NO_REQUIRED_FIELDS = new String[0];
    /**
     * The No Unfiltered Fields String[].
     */
    private static final String[] NO_UNCHECKED_FIELDS = new String[0];
    /**
     * The Disallowed Charaters.
     */
    private static final char[] DISALLOWED_CHARACTERS = new char[]{'|', ';', '$', '%', '@', '\'', '"', ',', '+', '\\', '<', '>', '(', ')'};

    /**
     * Return list of allowed fields for this command.
     *
     * @return list of allowed fields (properties)
     */
    public String[] getAllowedFields() {
        return ALL_FIELDS_ARE_ALLOWED;
    }

    /**
     * Return list of command fields which are mandatory. Validation errors will
     * be raised when some property is missing.
     *
     * @return list of fields (properties) which are required
     */
    public String[] getRequiredFields() {
        return NO_REQUIRED_FIELDS;
    }

    /**
     * Returns list of command fields which should not be filtered.
     *
     * @return list of fields (properties) which should not be filtered.
     */
    public String[] getUncheckedFields() {
        return NO_UNCHECKED_FIELDS;
    }

    /**
     * Callback for post-initializing command after successful binding.
     * Controllers which use {@link BinderAware} should guarantee that this
     * method won't be invoked when binding fails.
     */
    public void afterBind() {
    }

    /**
     * Whether this command object is currently in valid state. This performs
     * simple UI Form validation then normally delegates to
     * {@link org.springframework.validation.Errors#hasErrors()}.
     *
     * @param errors the errors
     * @return <code>true</code> if this command object is currently in valid
     * state, <code>false</code> otherwise
     */
    public boolean validate(Errors errors) {
        if (null == errors) {
            return true;
        } else {
            return false;
        }
    }
}
