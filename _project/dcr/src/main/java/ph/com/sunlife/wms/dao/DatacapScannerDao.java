package ph.com.sunlife.wms.dao;

import java.util.List;
import ph.com.sunlife.wms.dao.domain.DatacapScanner;
import ph.com.sunlife.wms.dao.exceptions.WMSDaoException;

public interface DatacapScannerDao
{
  List<DatacapScanner> getAllScanner()
    throws WMSDaoException;
  
  List<DatacapScanner> getScannerById(String pcName)
    throws WMSDaoException;
  
  boolean deleteScanner(String pcName)
    throws WMSDaoException;
}