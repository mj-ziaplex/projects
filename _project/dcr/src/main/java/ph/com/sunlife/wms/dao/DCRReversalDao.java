package ph.com.sunlife.wms.dao;

import java.util.List;

import ph.com.sunlife.wms.dao.domain.DCRBalancingToolProduct;
import ph.com.sunlife.wms.dao.domain.DCRReversal;
import ph.com.sunlife.wms.dao.exceptions.WMSDaoException;

/**
 * The Data Access Object responsible for maintenance of {@link DCRReversal}
 * entries.
 * 
 * @author Zainal Limpao
 * 
 */
public interface DCRReversalDao extends WMSDao<DCRReversal> {

	/**
	 * Retrieves all {@link DCRReversal} entries associated with the balancing
	 * tool product ID.
	 * 
	 * @param dcrBalancingToolProductId
	 * @return
	 * @throws WMSDaoException
	 */
	List<DCRReversal> getAllByBalancingToolProduct(
			Long dcrBalancingToolProductId) throws WMSDaoException;

	/**
	 * Deletes the given {@link DCRReversal} entry via
	 * {@link DCRReversal#getId()}.
	 * 
	 * 
	 * @param id
	 * @return <code>true</code> if successful, otherwise <code>false</code>
	 * @throws WMSDaoException
	 */
	boolean deleteById(Long id) throws WMSDaoException;

	/**
	 * Updates the {@link DCRReversal}. The only fields that can be updated are
	 * {@link DCRReversal#getAmount()} and {@link DCRReversal#getRemarks()}.
	 * Others are set by the system upon creation.
	 * 
	 * @param dcrReversal
	 * @throws WMSDaoException if a problem has been encountered.
	 */
	void updateReversal(DCRReversal dcrReversal) throws WMSDaoException;
	
	DCRBalancingToolProduct getBalancingToolProductById(Long id) throws WMSDaoException;

}
