package ph.com.sunlife.wms.dao.domain;

public class DCRIpacValue extends Entity {

	private String prodCode;
	
	private String payCode;
	
	private String paySubCode;
	
	private String paySubCurr;
	
	private Currency currency;
	
	private String paySubDesc;
	
	private String acf2id;
	
	private Cashier cashier;
	
	private Long dcrCashierId;
	
	private double amount;

	public String getProdCode() {
		return prodCode;
	}

	public void setProdCode(String prodCode) {
		this.prodCode = prodCode;
	}

	public String getPayCode() {
		return payCode;
	}

	public void setPayCode(String payCode) {
		this.payCode = payCode;
	}

	public String getPaySubCode() {
		return paySubCode;
	}

	public void setPaySubCode(String paySubCode) {
		this.paySubCode = paySubCode;
	}

	public String getPaySubCurr() {
		return paySubCurr;
	}

	public void setPaySubCurr(String paySubCurr) {
		this.paySubCurr = paySubCurr;
	}

	public Currency getCurrency() {
		if (this.paySubCurr != null) {
			this.currency = Currency.getCurrency(this.paySubCurr);
		}
		
		return this.currency;
	}

	public void setCurrency(Currency currency) {
		this.currency = currency;
	}

	public String getPaySubDesc() {
		return paySubDesc;
	}

	public void setPaySubDesc(String paySubDesc) {
		this.paySubDesc = paySubDesc;
	}

	public String getAcf2id() {
		return acf2id;
	}

	public void setAcf2id(String acf2id) {
		this.acf2id = acf2id;
	}

	public Cashier getCashier() {
		if (acf2id != null) {
			this.cashier = new Cashier(acf2id);
		}
		
		return this.cashier;
	}

	public void setCashier(Cashier cashier) {
		this.cashier = cashier;
	}

	public Long getDcrCashierId() {
		return dcrCashierId;
	}

	public void setDcrCashierId(Long dcrCashierId) {
		this.dcrCashierId = dcrCashierId;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}
	
	@Override
	public String toString() {
		return "DCRIpacValue [prodCode=" + prodCode + ", payCode=" + payCode
				+ ", paySubCode=" + paySubCode + ", paySubCurr=" + paySubCurr
				+ ", currency=" + currency + ", paySubDesc=" + paySubDesc
				+ ", acf2id=" + acf2id + ", cashier=" + cashier
				+ ", dcrCashierId=" + dcrCashierId + ", amount=" + amount + "]";
	}
	
}
