package ph.com.sunlife.wms.dao.domain;

import java.io.Serializable;
import java.util.Date;

public class DatacapScanner
  implements Serializable
{
  private static final long serialVersionUID = -6600543466891040286L;
  private String wsuName;
  private String wsuContactperson;
  private String wsuDeptbrnchname;
  private Date wsuCredate;
  
 
  public String getWsuName() {
	return wsuName;
}


public void setWsuName(String wsuName) {
	this.wsuName = wsuName;
}


public String getWsuContactperson() {
	return wsuContactperson;
}


public void setWsuContactperson(String wsuContactperson) {
	this.wsuContactperson = wsuContactperson;
}


public String getWsuDeptbrnchname() {
	return wsuDeptbrnchname;
}


public void setWsuDeptbrnchname(String wsuDeptbrnchname) {
	this.wsuDeptbrnchname = wsuDeptbrnchname;
}


public Date getWsuCredate() {
	return wsuCredate;
}


public void setWsuCredate(Date wsuCredate) {
	this.wsuCredate = wsuCredate;
}


public static long getSerialversionuid() {
	return serialVersionUID;
}


public String toString()
  {
    return "DatacapScanner [wsuName=" + this.wsuName + ", wsuContactperson=" + this.wsuContactperson + ", wsuDeptbrnchname=" + this.wsuDeptbrnchname + ", wsuCredate=" + this.wsuCredate + "]";
  }
}

