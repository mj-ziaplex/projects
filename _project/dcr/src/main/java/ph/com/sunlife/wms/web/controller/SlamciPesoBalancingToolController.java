package ph.com.sunlife.wms.web.controller;

import ph.com.sunlife.wms.dao.domain.Company;

public class SlamciPesoBalancingToolController extends
		AbstractBalancingToolController {

	private String displayViewName = "slamciBalancingToolView";

	private String pageTitle;
	
	@Override
	protected Company getCompany() {
		return Company.SLAMCIP;
	}

	@Override
	protected String getDisplayViewName() {
		return this.displayViewName;
	}

	public void setDisplayViewName(String displayViewName) {
		this.displayViewName = displayViewName;
	}

	@Override
	protected String getPageTitle() {
		return pageTitle;
	}
	
	public void setPageTitle(String pageTitle) {
		this.pageTitle = pageTitle;
	}

}
