package ph.com.sunlife.wms.services;

import java.util.Map;

import ph.com.sunlife.wms.services.bo.DCRAddingMachineBO;
import ph.com.sunlife.wms.services.exception.InvalidFileTypeException;
import ph.com.sunlife.wms.services.exception.ServiceException;

/**
 * The Service Interface for Adding Machine Upload.
 * 
 * @author Zainal Limpao
 * 
 */
public interface DCRAddingMachineService {

	/**
	 * Uploads the Adding Machine Spreadsheet. Make sure that the byte array
	 * represents a valid Spreadsheet file.
	 * 
	 * @param dcrCashierId
	 * @param file
	 * @return
	 * @throws ServiceException
	 * @throws InvalidFileTypeException
	 */
	DCRAddingMachineBO uploadDCRAddingMachine(Long dcrCashierId, byte[] file)
			throws ServiceException, InvalidFileTypeException;

	/**
	 * An overloaded method of {@link #uploadDCRAddingMachine(Long, byte[])}.
	 * 
	 * @param map
	 * @return
	 * @throws ServiceException
	 * @throws InvalidFileTypeException
	 */
	DCRAddingMachineBO uploadDCRAddingMachine(Map<String, Object> map)
			throws ServiceException, InvalidFileTypeException;

	/**
	 * Getter for uploaded Adding Machine.
	 * 
	 * @param dcrCashierId
	 * @return
	 * @throws ServiceException
	 */
	DCRAddingMachineBO getAddingMachine(Long dcrCashierId)
			throws ServiceException;

}
