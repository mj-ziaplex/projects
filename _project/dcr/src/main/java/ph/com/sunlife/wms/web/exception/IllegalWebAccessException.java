package ph.com.sunlife.wms.web.exception;

import ph.com.sunlife.wms.web.controller.AbstractSecuredMultiActionController;

/**
 * The custom exception that will be thrown if user has not logged in to the
 * system and/or is not authorized for such.
 * 
 * @author Zainal Limpao
 * 
 * @see AbstractSecuredMultiActionController#handleIllegalWebAccessException(javax.servlet.http.HttpServletRequest,
 *      javax.servlet.http.HttpServletResponse, IllegalWebAccessException)
 */
public class IllegalWebAccessException extends RuntimeException {

	private static final long serialVersionUID = 1592607458454576795L;

	public IllegalWebAccessException(String message) {
		super(message);
	}

	public IllegalWebAccessException(Throwable throwable) {
		super(throwable);
	}

	public IllegalWebAccessException(String message, Throwable throwable) {
		super(message, throwable);
	}
}
