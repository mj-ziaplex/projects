package ph.com.sunlife.wms.web.controller;

import static ph.com.sunlife.wms.util.WMSMimeUtil.GIF_CONTENT_TYPE;
import static ph.com.sunlife.wms.util.WMSMimeUtil.HTML_CONTENT_TYPE;
import static ph.com.sunlife.wms.util.WMSMimeUtil.JPEG_CONTENT_TYPE;
import static ph.com.sunlife.wms.util.WMSMimeUtil.PDF_CONTENT_TYPE;
import static ph.com.sunlife.wms.util.WMSMimeUtil.PNG_CONTENT_TYPE;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.View;

import ph.com.sunlife.wms.services.AttachmentService;
import ph.com.sunlife.wms.services.bo.DCRFileBO;
import ph.com.sunlife.wms.services.exception.ServiceException;
import ph.com.sunlife.wms.util.WMSMimeUtil;
import ph.com.sunlife.wms.web.controller.form.AttachmentForm;
import ph.com.sunlife.wms.web.exception.ApplicationException;
import ph.com.sunlife.wms.web.view.WMSDownloadFileView;
import ph.com.sunlife.wms.web.view.WMSImageView;
import ph.com.sunlife.wms.web.view.WMSPdfView;

/**
 * The Controller that will display the attached file via Filenet.
 * 
 * @author Zainal Limpao
 * 
 */
public class ViewFileController extends AbstractWMSMultiActionController {

	private static final Logger LOGGER = Logger.getLogger(ViewFileController.class);
	
	private static final String FILE_NAME_KEY = "fileName";

	private static final String FILE_DATA_KEY = "fileData";

	private AttachmentService attachmentService;

	/**
	 * Method to display the File.
	 * 
	 * @param request
	 * @param response
	 * @param form
	 * @return
	 * @throws ApplicationException
	 */
	public ModelAndView viewFile(HttpServletRequest request,
			HttpServletResponse response, AttachmentForm form)
			throws ApplicationException {
		LOGGER.info("Attempting to retrieve selected file on Filenet...");
		
		String filenetId = form.getFileId();
		Map<String, Object> model = new HashMap<String, Object>();
		try {
			DCRFileBO dcrFile = attachmentService.getFileFromFilenet(filenetId);
			this.assertDcrFile(dcrFile);

			byte[] fileData = dcrFile.getFileData();
			String contentType = dcrFile.getContentType();
			String fileExtension = WMSMimeUtil.getFileExtension(contentType);

			model.put(FILE_DATA_KEY, fileData);

			View view = null;
			if (PDF_CONTENT_TYPE.equals(contentType)) {
				view = new WMSPdfView();
			} else if (HTML_CONTENT_TYPE.equals(contentType)
					|| JPEG_CONTENT_TYPE.equals(contentType)
					|| GIF_CONTENT_TYPE.equals(contentType)
					|| PNG_CONTENT_TYPE.equals(contentType)) {
				view = new WMSImageView(contentType);
			} else {
				view = new WMSDownloadFileView();
			}
			
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("File Properties");
				LOGGER.debug("Content Type: " + contentType);
				LOGGER.debug("File Extension: " + fileExtension);
				if (fileData != null) {
					LOGGER.debug("File Size: " + fileData.length);
				}
			}

			model.put(FILE_NAME_KEY, "download" + fileExtension);
			return new ModelAndView(view, model);

		} catch (ServiceException e) {
			throw new ApplicationException(e);
		}

	}

	/**
	 * 
	 * @param dcrFile
	 * @throws ServiceException
	 */
	private void assertDcrFile(DCRFileBO dcrFile) throws ServiceException {
		if (dcrFile == null) {
			throw new ServiceException("File is not found");
		}
	}

	public void setAttachmentService(AttachmentService attachmentService) {
		this.attachmentService = attachmentService;
	}

}
