package ph.com.sunlife.wms.services;

import java.util.List;

import ph.com.sunlife.wms.services.bo.DCRCommentBO;
import ph.com.sunlife.wms.services.exception.ServiceException;

/**
 * The Service Layer responsible for persisting {@link DCRCommentBO} items.
 * 
 * @author <a href=\"http://facebook.com/jsard\"><b>Josephus "The Ladies' Man"
 *         Sardan</b></a>
 * 
 */
public interface DCRCommentService {

	/**
	 * Saves the {@link DCRCommentBO} entry into the database.
	 * 
	 * @param dcrComment
	 * @return
	 * @throws ServiceException
	 */
	DCRCommentBO save(DCRCommentBO dcrComment) throws ServiceException;

	/**
	 * Retrieves the {@link DCRCommentBO} with the given ID.
	 * 
	 * @param id
	 * @return
	 * @throws ServiceException
	 */
	DCRCommentBO getById(Long id) throws ServiceException;

	/**
	 * Get all comments for associated with the given ID of a DCR Cashier Work
	 * Item.
	 * 
	 * @param dcrCashierId
	 * @return
	 * @throws ServiceException
	 */
	List<DCRCommentBO> getAllComments(Long dcrCashierId)
			throws ServiceException;

	/**
	 * Get all comments for associated with the given ID of a DCR Work Item.
	 * 
	 * @param dcrId
	 * @return
	 * @throws ServiceException
	 */
	List<DCRCommentBO> getAllCommentsDcrId(Long dcrId) throws ServiceException;
}
