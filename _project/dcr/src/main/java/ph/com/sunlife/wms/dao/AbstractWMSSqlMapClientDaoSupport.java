package ph.com.sunlife.wms.dao;

import java.sql.SQLException;

import org.springframework.orm.ibatis.SqlMapClientCallback;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import ph.com.sunlife.wms.dao.domain.Entity;
import ph.com.sunlife.wms.dao.exceptions.WMSDaoException;

import com.ibatis.sqlmap.client.SqlMapExecutor;

/**
 * This is the abstracted implementation of {@link SqlMapClientDaoSupport} of
 * Spring but with ready-made basic CRUD operation methods.
 * 
 * @author Zainal Limpao
 * 
 * @param <E>
 */
@SuppressWarnings("unchecked")
public abstract class AbstractWMSSqlMapClientDaoSupport<E extends Entity>
		extends SqlMapClientDaoSupport implements WMSDao<E> {

	/**
	 * The namespace (from IBATIS Config File) of this DAO.
	 * 
	 * @return
	 */
	abstract protected String getNamespace();

	@Override
	public E save(final E entity)
			throws WMSDaoException {
		try {
			Long id = (Long) getSqlMapClientTemplate().execute(
					new SqlMapClientCallback() {

						@Override
						public Object doInSqlMapClient(SqlMapExecutor executor)
								throws SQLException {
							Long id = (Long) executor.queryForObject(
									getNamespace() + ".insert", entity);
							return id;
						}
					});
			
			entity.setId(id);
		} catch (Exception e) {
			throw new WMSDaoException(e);
		}
		return entity;
	}

	@Override
	public E getById(final Long id) throws WMSDaoException {
		E entity = null;

		try {
			entity = (E) getSqlMapClientTemplate().execute(
					new SqlMapClientCallback() {

						@Override
						public Object doInSqlMapClient(SqlMapExecutor executor) throws SQLException {
							return executor.queryForObject(getNamespace() + ".getById", id);
						}
					});
		} catch (Exception e) {
			throw new WMSDaoException(e);
		}
		return entity;
	}

	@Override
	public E refresh(final E entity) throws WMSDaoException {
		E entityWithId = null;

		try {
			entityWithId = (E) getSqlMapClientTemplate().execute(
					new SqlMapClientCallback() {

						@Override
						public Object doInSqlMapClient(SqlMapExecutor executor) throws SQLException {
							return executor.queryForObject(getNamespace() + ".refresh", entity);
						}
					});
		} catch (Exception e) {
			throw new WMSDaoException(e);
		}
		return entityWithId;
	}

}
