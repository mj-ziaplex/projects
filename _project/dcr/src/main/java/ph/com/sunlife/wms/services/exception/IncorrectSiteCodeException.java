package ph.com.sunlife.wms.services.exception;

public class IncorrectSiteCodeException extends RuntimeException {

	private static final long serialVersionUID = 4887522079160306872L;

	public IncorrectSiteCodeException(String message) {
		super(message);
	}

	public IncorrectSiteCodeException(String message, Throwable throwable) {
		super(message, throwable);
	}

	public IncorrectSiteCodeException(Throwable throwable) {
		super(throwable);
	}
}
