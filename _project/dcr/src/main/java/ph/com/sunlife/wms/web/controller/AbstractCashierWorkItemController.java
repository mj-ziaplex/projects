package ph.com.sunlife.wms.web.controller;

import static ph.com.sunlife.wms.util.WMSConstants.HAS_CASHIER_ADHOC_MENU;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import ph.com.sunlife.wms.dao.domain.CheckType;
import ph.com.sunlife.wms.services.AttachmentService;
import ph.com.sunlife.wms.services.DCRCreationService;
import ph.com.sunlife.wms.services.bo.CompanyBO;
import ph.com.sunlife.wms.services.bo.DCRBO;
import ph.com.sunlife.wms.services.bo.DCRBalancingToolBO;
import ph.com.sunlife.wms.services.bo.DCRCashDepositSlipBO;
import ph.com.sunlife.wms.services.bo.DCRCashierBO;
import ph.com.sunlife.wms.services.bo.DCRChequeDepositSlipBO;
import ph.com.sunlife.wms.services.bo.DCRIpacValueBO;
import ph.com.sunlife.wms.services.bo.UserSession;
import ph.com.sunlife.wms.services.exception.ServiceException;
import ph.com.sunlife.wms.util.WMSDateUtil;
import ph.com.sunlife.wms.web.exception.IllegalWebAccessException;
import org.apache.log4j.Logger;

/**
 * This is the generic {@link Controller} for all pages associated with the
 * Cashier Work Item. Note that the term "work item" is not related to Filenet
 * Components directly.
 * 
 * @author Zainal Limpao
 * 
 */
public abstract class AbstractCashierWorkItemController extends
		AbstractSecuredMultiActionController {
    
    private static final Logger LOGGER = Logger
			.getLogger(AbstractBalancingToolController.class);

	protected static final String PAGE_SEQUENCE_KEY = "pageSequence";

	protected static final String BALANCING_TOOLS_KEY = "balancingTools";

	protected static final String DCR_CASHIER_BO_KEY = "dcrCashierBO";

	protected static final String TITLE_KEY = "title";

	protected DCRCreationService dcrCreationService;

	protected AttachmentService attachmentService;

	/**
	 * Page Title, which will then be required to all
	 * {@link AbstractSecuredMultiActionController}.
	 * 
	 * @return
	 */
	abstract protected String getPageTitle();

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ph.com.sunlife.wms.web.controller.AbstractSecuredMultiActionController
	 * #afterHandleRequest(org.springframework.web.servlet.ModelAndView,
	 * javax.servlet.http.HttpServletRequest,
	 * javax.servlet.http.HttpServletResponse)
	 */
	@SuppressWarnings({ "unchecked" })
	@Override
	protected void afterHandleRequest(ModelAndView modelAndView,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		super.afterHandleRequest(modelAndView, request, response);

		// Checks if there is no exception.
		Exception exceptionFromModel = pullExceptionFromModelAndView(modelAndView);
		if (exceptionFromModel == null) {

			modelAndView.addObject(HAS_CASHIER_ADHOC_MENU, Boolean.TRUE);

			DCRCashierBO dc = (DCRCashierBO) modelAndView.getModel().get(
					DCR_CASHIER_BO_KEY);

			UserSession userSession = null;

			String pageTitle = this.getPageTitle();
			if (dc != null) {
				DCRBO dcr = dc.getDcr();
				if (dcr != null) {
					String dateStr = WMSDateUtil.toFormattedDateStr(dcr
							.getDcrDate());
					String ccName = dcr.getCcName();

					if (StringUtils.isNotEmpty(dateStr)
							&& StringUtils.isNotEmpty(ccName)) {
						pageTitle += " (" + ccName + " "
								+ StringUtils.upperCase(dateStr) + " DCR)";
					}
				}
			}

			modelAndView.addObject(TITLE_KEY, pageTitle);
			// This will generate markers for the adhoc menu if a
			// particular Balancing Tool has been confirmed or not
			// by the user.
			Map<String, Object> model = modelAndView.getModel();
			List<DCRBalancingToolBO> balancingTools = (List<DCRBalancingToolBO>) model
					.get(BALANCING_TOOLS_KEY);

			if (CollectionUtils.isEmpty(balancingTools)) {
				userSession = getUserSession(request);
				Long dcrCashierId = this.getDcrCashierId(request);

				DCRCashierBO dcrCashierBO = getDCRCashierBO(dcrCashierId,
						userSession, modelAndView);

				balancingTools = this.dcrCreationService
						.createAndDisplayDCRBalancingTools(dcrCashierBO);

				modelAndView.addObject(BALANCING_TOOLS_KEY, balancingTools);
			}

			this.pushIfBalancingToolsAreConfirmed(modelAndView);
			this.pushForCreatedDepositSlipCheckMarks(request, modelAndView,
					userSession);
		}
	}

	private void pushForCreatedDepositSlipCheckMarks(
			HttpServletRequest request, ModelAndView modelAndView,
			UserSession userSession) throws Exception {
		Map<String, Object> model = modelAndView.getModel();
		DCRCashierBO dcrCashierBo = (DCRCashierBO) model
				.get(DCR_CASHIER_BO_KEY);

		if (dcrCashierBo != null) {
			Long dcrCashierId = dcrCashierBo.getId();
			List<DCRCashDepositSlipBO> cashDepositSlips = attachmentService
					.getCashDepositSlips(dcrCashierId);
			List<DCRChequeDepositSlipBO> checkDepositSlips = attachmentService
					.getCheckDepositSlips(dcrCashierId);

			if (CollectionUtils.isNotEmpty(cashDepositSlips)) {
				for (DCRCashDepositSlipBO bo : cashDepositSlips) {
					String prodCode = bo.getProdCode();
					Long companyId = bo.getCompanyId();
					Long currencyId = bo.getCurrencyId();

					if (companyId == 1L && currencyId == 1L) {
						modelAndView.addObject("depositSlip01", true);
					}

					if (companyId == 2L && currencyId == 2L) {
						modelAndView.addObject("depositSlip05", true);
					}

					if (companyId == 3L && currencyId == 1L) {
						modelAndView.addObject("depositSlip07", true);
					}

					if (companyId == 3L && currencyId == 2L) {
						modelAndView.addObject("depositSlip11", true);
					}
					// TRAD/VUL
					// UNCOVERTED
					// GROUP
					if (companyId == 4L
							&& currencyId == 1L
							&& StringUtils.equalsIgnoreCase("TRAD/VUL",
									prodCode)) {
						modelAndView.addObject("depositSlip15", true);
					}

					if (companyId == 4L
							&& currencyId == 1L
							&& StringUtils.equalsIgnoreCase("UNCOVERTED",
									prodCode)) {
						modelAndView.addObject("depositSlip19", true);
					}

					if (companyId == 4L && currencyId == 1L
							&& StringUtils.equalsIgnoreCase("GROUP", prodCode)) {
						modelAndView.addObject("depositSlip23", true);
					}

					if (companyId == 4L
							&& currencyId == 2L
							&& StringUtils.equalsIgnoreCase("TRAD/VUL",
									prodCode)) {
						modelAndView.addObject("depositSlip27", true);
					}

					if (companyId == 4L
							&& currencyId == 2L
							&& StringUtils.equalsIgnoreCase("UNCOVERTED",
									prodCode)) {
						modelAndView.addObject("depositSlip31", true);
					}

					if (companyId == 4L && currencyId == 2L
							&& StringUtils.equalsIgnoreCase("GROUP", prodCode)) {
						modelAndView.addObject("depositSlip35", true);
					}

					if (companyId == 5L
							&& StringUtils
									.equalsIgnoreCase("PRENEED", prodCode)) {
						modelAndView.addObject("depositSlip39", true);
					}

					if (companyId == 5L
							&& StringUtils.equalsIgnoreCase("GAF", prodCode)) {
						modelAndView.addObject("depositSlip43", true);
					}

					if (companyId == 6L && currencyId == 1L) {
						modelAndView.addObject("depositSlip45", true);
					}

					if (companyId == 6L && currencyId == 2L) {
						modelAndView.addObject("depositSlip49", true);
					}
				}
			}

			if (CollectionUtils.isNotEmpty(checkDepositSlips)) {
				for (DCRChequeDepositSlipBO bo : checkDepositSlips) {
					String prodCode = bo.getProdCode();
					Long companyId = bo.getCompanyId();
					Long currencyId = bo.getCurrencyId();
					CheckType ct = CheckType.getById(bo.getCheckTypeId());

					if (companyId == 1L && CheckType.ON_US.equals(ct)) {
						modelAndView.addObject("depositSlip02", true);
					}

					if (companyId == 1L && CheckType.LOCAL.equals(ct)) {
						modelAndView.addObject("depositSlip03", true);
					}

					if (companyId == 1L && CheckType.REGIONAL.equals(ct)) {
						modelAndView.addObject("depositSlip04", true);
					}

					if (companyId == 2L && CheckType.DOLLARCHECK.equals(ct)) {
						modelAndView.addObject("depositSlip06", true);
					}

					if (companyId == 3L && currencyId == 1L
							&& CheckType.ON_US.equals(ct)) {
						modelAndView.addObject("depositSlip08", true);
					}

					if (companyId == 3L && currencyId == 1L
							&& CheckType.LOCAL.equals(ct)) {
						modelAndView.addObject("depositSlip09", true);
					}

					if (companyId == 3L && currencyId == 1L
							&& CheckType.REGIONAL.equals(ct)) {
						modelAndView.addObject("depositSlip10", true);
					}

					if (companyId == 3L && currencyId == 2L
							&& CheckType.DOLLARCHECK.equals(ct)) {
						modelAndView.addObject("depositSlip12", true);
					}

					if (companyId == 3L && currencyId == 2L
							&& CheckType.USCD_MANILA.equals(ct)) {
						modelAndView.addObject("depositSlip13", true);
					}

					if (companyId == 3L && currencyId == 2L
							&& CheckType.USCD_OUTSIDEPH.equals(ct)) {
						modelAndView.addObject("depositSlip14", true);
					}

					if (companyId == 4L
							&& currencyId == 1L
							&& StringUtils.equalsIgnoreCase("TRAD/VUL",
									prodCode) && CheckType.ON_US.equals(ct)) {
						modelAndView.addObject("depositSlip16", true);
					}

					if (companyId == 4L
							&& currencyId == 1L
							&& StringUtils.equalsIgnoreCase("TRAD/VUL",
									prodCode) && CheckType.LOCAL.equals(ct)) {
						modelAndView.addObject("depositSlip17", true);
					}

					if (companyId == 4L
							&& currencyId == 1L
							&& StringUtils.equalsIgnoreCase("TRAD/VUL",
									prodCode) && CheckType.REGIONAL.equals(ct)) {
						modelAndView.addObject("depositSlip18", true);
					}

					if (companyId == 4L
							&& currencyId == 1L
							&& StringUtils.equalsIgnoreCase("UNCOVERTED",
									prodCode) && CheckType.ON_US.equals(ct)) {
						modelAndView.addObject("depositSlip20", true);
					}

					if (companyId == 4L
							&& currencyId == 1L
							&& StringUtils.equalsIgnoreCase("UNCOVERTED",
									prodCode) && CheckType.LOCAL.equals(ct)) {
						modelAndView.addObject("depositSlip21", true);
					}

					if (companyId == 4L
							&& currencyId == 1L
							&& StringUtils.equalsIgnoreCase("UNCOVERTED",
									prodCode) && CheckType.REGIONAL.equals(ct)) {
						modelAndView.addObject("depositSlip22", true);
					}

					if (companyId == 4L && currencyId == 1L
							&& StringUtils.equalsIgnoreCase("GROUP", prodCode)
							&& CheckType.ON_US.equals(ct)) {
						modelAndView.addObject("depositSlip24", true);
					}

					if (companyId == 4L && currencyId == 1L
							&& StringUtils.equalsIgnoreCase("GROUP", prodCode)
							&& CheckType.LOCAL.equals(ct)) {
						modelAndView.addObject("depositSlip25", true);
					}

					if (companyId == 4L && currencyId == 1L
							&& StringUtils.equalsIgnoreCase("GROUP", prodCode)
							&& CheckType.REGIONAL.equals(ct)) {
						modelAndView.addObject("depositSlip26", true);
					}

					if (companyId == 4L
							&& currencyId == 2L
							&& StringUtils.equalsIgnoreCase("TRAD/VUL",
									prodCode)
							&& CheckType.DOLLARCHECK.equals(ct)) {
						modelAndView.addObject("depositSlip28", true);
					}

					if (companyId == 4L
							&& currencyId == 2L
							&& StringUtils.equalsIgnoreCase("TRAD/VUL",
									prodCode)
							&& CheckType.USCD_MANILA.equals(ct)) {
						modelAndView.addObject("depositSlip29", true);
					}

					if (companyId == 4L
							&& currencyId == 2L
							&& StringUtils.equalsIgnoreCase("TRAD/VUL",
									prodCode)
							&& CheckType.USCD_OUTSIDEPH.equals(ct)) {
						modelAndView.addObject("depositSlip30", true);
					}

					if (companyId == 4L
							&& currencyId == 2L
							&& StringUtils.equalsIgnoreCase("UNCOVERTED",
									prodCode)
							&& CheckType.DOLLARCHECK.equals(ct)) {
						modelAndView.addObject("depositSlip32", true);
					}

					if (companyId == 4L
							&& currencyId == 2L
							&& StringUtils.equalsIgnoreCase("UNCOVERTED",
									prodCode)
							&& CheckType.USCD_MANILA.equals(ct)) {
						modelAndView.addObject("depositSlip33", true);
					}

					if (companyId == 4L
							&& currencyId == 2L
							&& StringUtils.equalsIgnoreCase("UNCOVERTED",
									prodCode)
							&& CheckType.USCD_OUTSIDEPH.equals(ct)) {
						modelAndView.addObject("depositSlip34", true);
					}

					if (companyId == 4L && currencyId == 2L
							&& StringUtils.equalsIgnoreCase("GROUP", prodCode)
							&& CheckType.DOLLARCHECK.equals(ct)) {
						modelAndView.addObject("depositSlip36", true);
					}

					if (companyId == 4L && currencyId == 2L
							&& StringUtils.equalsIgnoreCase("GROUP", prodCode)
							&& CheckType.USCD_MANILA.equals(ct)) {
						modelAndView.addObject("depositSlip37", true);
					}

					if (companyId == 4L && currencyId == 2L
							&& StringUtils.equalsIgnoreCase("GROUP", prodCode)
							&& CheckType.USCD_OUTSIDEPH.equals(ct)) {
						modelAndView.addObject("depositSlip38", true);
					}

					if (companyId == 5L
							&& StringUtils
									.equalsIgnoreCase("PRENEED", prodCode)
							&& CheckType.ON_US.equals(ct)) {
						modelAndView.addObject("depositSlip40", true);
					}

					if (companyId == 5L
							&& StringUtils
									.equalsIgnoreCase("PRENEED", prodCode)
							&& CheckType.LOCAL.equals(ct)) {
						modelAndView.addObject("depositSlip41", true);
					}

					if (companyId == 5L
							&& StringUtils
									.equalsIgnoreCase("PRENEED", prodCode)
							&& CheckType.REGIONAL.equals(ct)) {
						modelAndView.addObject("depositSlip42", true);
					}

					if (companyId == 5L
							&& StringUtils.equalsIgnoreCase("GAF", prodCode)
							&& CheckType.LOCAL.equals(ct)) {
						modelAndView.addObject("depositSlip44", true);
					}

					if (companyId == 6L && CheckType.ON_US.equals(ct)) {
						modelAndView.addObject("depositSlip46", true);
					}

					if (companyId == 6L && CheckType.LOCAL.equals(ct)) {
						modelAndView.addObject("depositSlip47", true);
					}

					if (companyId == 6L && CheckType.REGIONAL.equals(ct)) {
						modelAndView.addObject("depositSlip48", true);
					}

					if (companyId == 6L && CheckType.DOLLARCHECK.equals(ct)) {
						modelAndView.addObject("depositSlip50", true);
					}

					if (companyId == 6L && CheckType.USCD_MANILA.equals(ct)) {
						modelAndView.addObject("depositSlip51", true);
					}

					if (companyId == 6L && CheckType.USCD_OUTSIDEPH.equals(ct)) {
						modelAndView.addObject("depositSlip52", true);
					}
				}
			}

			List<DCRIpacValueBO> ipacValues = getIpacValues(request, dcrCashierBo);

			if (CollectionUtils.isNotEmpty(ipacValues)) {
			}

			for (int i = 1; i <= 52; i++) {
				String marker = "depositSlip";
				if (i < 10) {
					marker += "0" + i;
				} else {
					marker += i;
				}

				Boolean depositSlipVal = (Boolean) model.get(marker);

				if (depositSlipVal == null) {

					if (StringUtils.equalsIgnoreCase("depositSlip01", marker)) {
						// TODO:
					} else if (StringUtils.equalsIgnoreCase("depositSlip02",
							marker)) {
						// TODO:
					} else if (StringUtils.equalsIgnoreCase("depositSlip03",
							marker)) {
						// TODO:
					} else if (StringUtils.equalsIgnoreCase("depositSlip04",
							marker)) {
						// TODO:
					} else if (StringUtils.equalsIgnoreCase("depositSlip05",
							marker)) {
						// TODO:
					} else if (StringUtils.equalsIgnoreCase("depositSlip06",
							marker)) {
						// TODO:
					} else if (StringUtils.equalsIgnoreCase("depositSlip07",
							marker)) {
						// TODO:
					} else if (StringUtils.equalsIgnoreCase("depositSlip08",
							marker)) {
						// TODO:
					} else if (StringUtils.equalsIgnoreCase("depositSlip09",
							marker)) {
						// TODO:
					} else if (StringUtils.equalsIgnoreCase("depositSlip10",
							marker)) {
						// TODO:
					} else if (StringUtils.equalsIgnoreCase("depositSlip11",
							marker)) {
						// TODO:
					} else if (StringUtils.equalsIgnoreCase("depositSlip12",
							marker)) {
						// TODO:
					} else if (StringUtils.equalsIgnoreCase("depositSlip13",
							marker)) {
						// TODO:
					} else if (StringUtils.equalsIgnoreCase("depositSlip14",
							marker)) {
						// TODO:
					} else if (StringUtils.equalsIgnoreCase("depositSlip15",
							marker)) {
						// TODO:
					} else if (StringUtils.equalsIgnoreCase("depositSlip16",
							marker)) {
						// TODO:
					} else if (StringUtils.equalsIgnoreCase("depositSlip17",
							marker)) {
						// TODO:
					} else if (StringUtils.equalsIgnoreCase("depositSlip18",
							marker)) {
						// TODO:
					} else if (StringUtils.equalsIgnoreCase("depositSlip19",
							marker)) {
						// TODO:
					} else if (StringUtils.equalsIgnoreCase("depositSlip20",
							marker)) {
						// TODO:
					} else if (StringUtils.equalsIgnoreCase("depositSlip21",
							marker)) {
						// TODO:
					} else if (StringUtils.equalsIgnoreCase("depositSlip22",
							marker)) {
						// TODO:
					} else if (StringUtils.equalsIgnoreCase("depositSlip23",
							marker)) {
						// TODO:
					} else if (StringUtils.equalsIgnoreCase("depositSlip24",
							marker)) {
						// TODO:
					} else if (StringUtils.equalsIgnoreCase("depositSlip25",
							marker)) {
						// TODO:
					} else if (StringUtils.equalsIgnoreCase("depositSlip26",
							marker)) {
						// TODO:
					} else if (StringUtils.equalsIgnoreCase("depositSlip27",
							marker)) {
						// TODO:
					} else if (StringUtils.equalsIgnoreCase("depositSlip28",
							marker)) {
						// TODO:
					} else if (StringUtils.equalsIgnoreCase("depositSlip29",
							marker)) {
						// TODO:
					} else if (StringUtils.equalsIgnoreCase("depositSlip30",
							marker)) {
						// TODO:
					} else if (StringUtils.equalsIgnoreCase("depositSlip31",
							marker)) {
						// TODO:
					} else if (StringUtils.equalsIgnoreCase("depositSlip32",
							marker)) {
						// TODO:
					} else if (StringUtils.equalsIgnoreCase("depositSlip33",
							marker)) {
						// TODO:
					} else if (StringUtils.equalsIgnoreCase("depositSlip34",
							marker)) {
						// TODO:
					} else if (StringUtils.equalsIgnoreCase("depositSlip35",
							marker)) {
						// TODO:
					} else if (StringUtils.equalsIgnoreCase("depositSlip36",
							marker)) {
						// TODO:
					} else if (StringUtils.equalsIgnoreCase("depositSlip37",
							marker)) {
						// TODO:
					} else if (StringUtils.equalsIgnoreCase("depositSlip38",
							marker)) {
						// TODO:
					} else if (StringUtils.equalsIgnoreCase("depositSlip39",
							marker)) {
						// TODO:
					} else if (StringUtils.equalsIgnoreCase("depositSlip40",
							marker)) {
						// TODO:
					} else if (StringUtils.equalsIgnoreCase("depositSlip41",
							marker)) {
						// TODO:
					} else if (StringUtils.equalsIgnoreCase("depositSlip42",
							marker)) {
						// TODO:
					} else if (StringUtils.equalsIgnoreCase("depositSlip43",
							marker)) {
						// TODO:
					} else if (StringUtils.equalsIgnoreCase("depositSlip44",
							marker)) {
						// TODO:
					} else if (StringUtils.equalsIgnoreCase("depositSlip45",
							marker)) {
						// TODO:
					} else if (StringUtils.equalsIgnoreCase("depositSlip46",
							marker)) {
						// TODO:
					} else if (StringUtils.equalsIgnoreCase("depositSlip47",
							marker)) {
						// TODO:
					} else if (StringUtils.equalsIgnoreCase("depositSlip48",
							marker)) {
						// TODO:
					} else if (StringUtils.equalsIgnoreCase("depositSlip49",
							marker)) {
						// TODO:
					} else if (StringUtils.equalsIgnoreCase("depositSlip50",
							marker)) {
						// TODO:
					} else if (StringUtils.equalsIgnoreCase("depositSlip51",
							marker)) {
						// TODO:
					} else if (StringUtils.equalsIgnoreCase("depositSlip52",
							marker)) {
						// TODO:
					}

					modelAndView.addObject(marker, false);
				}
			}

		}
	}

	private boolean findInIpacCollection(final String prodCode,
			final String payCode, final String paySubCode,
			final String paySubCurr, List<DCRIpacValueBO> ipacValues) {

		DCRIpacValueBO thisVal = (DCRIpacValueBO) CollectionUtils.find(
				ipacValues, new Predicate() {

					@Override
					public boolean evaluate(Object object) {
						DCRIpacValueBO thisVal = (DCRIpacValueBO) object;

						boolean isFound = true;

						isFound = isFound
								&& StringUtils.equalsIgnoreCase(prodCode,
										thisVal.getProdCode());

						isFound = isFound
								&& StringUtils.equalsIgnoreCase(payCode,
										thisVal.getPayCode());

						isFound = isFound
								&& StringUtils.equalsIgnoreCase(paySubCode,
										thisVal.getPaySubCode());

						isFound = isFound
								&& StringUtils.equalsIgnoreCase(paySubCurr,
										thisVal.getPaySubCurr());

						return isFound;
					}
				});

		return (thisVal != null);
	}

	/**
	 * Retrieves the {@link DCRCashierBO} from the DB given the dcrCashierId.
	 * There will be no DB-hit if {@link DCRCashierBO} is already present in the
	 * {@link ModelAndView}.
	 * 
	 * @param dcrCashierId
	 * @param userSession
	 * @param modelAndView
	 * @return
	 * @throws ServiceException
	 */
	protected DCRCashierBO getDCRCashierBO(Long dcrCashierId,
			UserSession userSession, ModelAndView modelAndView)
			throws ServiceException {
		DCRCashierBO dcrCashierBO = (DCRCashierBO) modelAndView.getModel().get(
				DCR_CASHIER_BO_KEY);

		if (dcrCashierBO == null) {
			dcrCashierBO = this.dcrCreationService.getDCRCashierBO(
					dcrCashierId, userSession.getUserId());

			if (dcrCashierBO != null) {
				populateDcrCompletely(dcrCashierBO);
			}

			modelAndView.addObject(DCR_CASHIER_BO_KEY, dcrCashierBO);
		} else {
			populateDcrCompletely(dcrCashierBO);
		}

		return dcrCashierBO;
	}

	private void populateDcrCompletely(DCRCashierBO dcrCashierBO)
			throws ServiceException {
		DCRBO dcr = dcrCashierBO.getDcr();
		if (dcr != null) {
			Long dcrId = dcr.getId();
			String ccName = dcr.getCcName();

			if (StringUtils.isEmpty(ccName)) {
				dcr = dcrCreationService.getDcrById(dcrId);
				dcrCashierBO.setDcr(dcr);
			}
		}
	}

	/**
	 * Places markers into the {@link ModelAndView} for the balancing tools that
	 * aren't confirmed. The check mark on the left side menu indicates that the
	 * balancing tool for that has been confirmed.
	 * 
	 * @param modelAndView
	 */
	@SuppressWarnings("unchecked")
	private void pushIfBalancingToolsAreConfirmed(ModelAndView modelAndView) {

		Map<String, Object> model = modelAndView.getModel();
		List<DCRBalancingToolBO> balancingTools = (List<DCRBalancingToolBO>) model
				.get(BALANCING_TOOLS_KEY);

		DCRCashierBO dcrCashierBo = (DCRCashierBO) model
				.get(DCR_CASHIER_BO_KEY);
		Date dcrDate = dcrCashierBo.getDcr().getDcrDate();

		if (CollectionUtils.isNotEmpty(balancingTools)) {
			for (DCRBalancingToolBO tool : balancingTools) {

				Date dateConfirmed = tool.getDateConfirmed();
				Date dateReconfirmed = tool.getDateReConfirmed();
				boolean isConfirmed = (dateConfirmed != null);
				Date today = cachingService.getWmsDcrSystemDate();

				String isConfirmedVal = isConfirmed ? "confirmed"
						: "unconfirmed";

				if (CompanyBO.SLAMCIP.equals(tool.getCompany())) {
					modelAndView
							.addObject("isSlamcipConfirmed", isConfirmedVal);
				} else if (CompanyBO.SLAMCID.equals(tool.getCompany())) {
					modelAndView
							.addObject("isSlamcidConfirmed", isConfirmedVal);
				} else if (CompanyBO.SLOCPI.equals(tool.getCompany())) {
					if (DateUtils.isSameDay(dcrDate, today)) {
						modelAndView.addObject("isSlocpiConfirmed",
								isConfirmedVal);
					} else {
						isConfirmedVal = (dateReconfirmed != null) ? "confirmed"
								: "unconfirmed";
						modelAndView.addObject("isSlocpiConfirmed",
								isConfirmedVal);
					}
				} else if (CompanyBO.SLGFI.equals(tool.getCompany())) {
					if (DateUtils.isSameDay(dcrDate, today)) {
						modelAndView.addObject("isSlgfiConfirmed",
								isConfirmedVal);
					} else {
						isConfirmedVal = (dateReconfirmed != null) ? "confirmed"
								: "unconfirmed";
						modelAndView.addObject("isSlgfiConfirmed",
								isConfirmedVal);
					}
				} else if (CompanyBO.SLFPI.equals(tool.getCompany())) {
					modelAndView.addObject("isSlfpiConfirmed", isConfirmedVal);
				} else {
					modelAndView.addObject("isOthersConfirmed", isConfirmedVal);
				}
			}
		}
	}

	/**
	 * This attempts to extract the <code>dcrCashierId</code> from the
	 * {@link HttpServletRequest}. There are many better ways to do this such as
	 * using the {@link #bind(HttpServletRequest, Object)} method but will have
	 * to be dealt with later.
	 * 
	 * @param request
	 * @return {@link Long} value.
	 */
	private Long getDcrCashierId(HttpServletRequest request) {
		String dcrCashierIdStr = request.getParameter("dcrCashierId");
		Long dcrCashierId = null;
		try {
			dcrCashierId = Long.valueOf(dcrCashierIdStr);
		} catch (Exception ex) {
			throw new IllegalWebAccessException(ex);
		}
		return dcrCashierId;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ph.com.sunlife.wms.web.controller.AbstractWMSMultiActionController#
	 * handleBindingError(javax.servlet.http.HttpServletRequest,
	 * javax.servlet.http.HttpServletResponse,
	 * org.springframework.web.bind.ServletRequestBindingException)
	 */
	@Override
	public ModelAndView handleBindingError(HttpServletRequest request,
			HttpServletResponse response, ServletRequestBindingException ex)
			throws Exception {

		BindException errors = (BindException) ex.getCause();

		if (errors.hasFieldErrors("dcrCashierId")
				|| errors.hasFieldErrors("session")) {
			return this.handleIllegalWebAccessException(request, response,
					new IllegalWebAccessException(
							"You do not have access rights for that page", ex));
		}

		ModelAndView modelAndView = super.handleBindingError(request, response,
				ex);

		Set<String> fieldsWithErrors = getFieldsWithErrors(errors);
		modelAndView.addObject("errors", fieldsWithErrors);

		modelAndView.addObject("errorMsg", "Invalid action");
		modelAndView
				.addObject("thisDate", cachingService.getWmsDcrSystemDate());
		modelAndView.addObject("thisURL", request.getRequestURL());
		modelAndView.addObject("hasCashierAdhocMenu", false);

		return modelAndView;
	}

    /**
     * Retrieves IPAC Values from {@link HttpSession}.
     *
     * @param request
     * @return
     */
    protected List<DCRIpacValueBO> getIpacValues(
            HttpServletRequest request,
            DCRCashierBO dcrCashier) throws ServiceException {
        
        List<DCRIpacValueBO> values = null;
        UserSession userSession = this.getUserSession(request);

//        if (userSession != null) { // To be removed in DCR redesign as it causes an error, data is not being refreshed
//            LOGGER.info("userSession is not empty setting IpacValues...");
//            values = (List<DCRIpacValueBO>) userSession.getIpacValues();
//        } else {
            LOGGER.info("userSession is not empty setting IpacValues...");
            values = dcrCreationService.buildIpacValues(values, dcrCashier);
//        }
        return values;
    }

	/**
	 * Injection setter for {@link DCRCreationService}.
	 * 
	 * @param dcrCreationService
	 */
	public void setDcrCreationService(DCRCreationService dcrCreationService) {
		this.dcrCreationService = dcrCreationService;
	}

	public void setAttachmentService(AttachmentService attachmentService) {
		this.attachmentService = attachmentService;
	}

}
