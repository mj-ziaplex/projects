package ph.com.sunlife.wms.dao.domain;



public class DCRErrorTagLogLookup extends Entity {
	private Long dcrId;
	private String stepId;
	private String errId;
	private Long logId;
	public Long getDcrId() {
		return dcrId;
	}
	public void setDcrId(Long dcrId) {
		this.dcrId = dcrId;
	}
	public String getStepId() {
		return stepId;
	}
	public void setStepId(String stepId) {
		this.stepId = stepId;
	}
	public String getErrId() {
		return errId;
	}
	public void setErrId(String errId) {
		this.errId = errId;
	}
	public Long getLogId() {
		return logId;
	}
	public void setLogId(Long logId) {
		this.logId = logId;
	}
	
}
