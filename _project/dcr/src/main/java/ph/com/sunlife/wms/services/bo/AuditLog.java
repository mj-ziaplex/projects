package ph.com.sunlife.wms.services.bo;

import java.util.Date;

public class AuditLog {

	private Long id;
	
	private Long dcrId;

	private Date logDate;
	
	private String dcrDateStr;
	
	private String docCapsiteId;
	
	private String docCapsiteName;

	private String processStatus;

	private String actionDetails;

	private String userId;

	public String getDocCapsiteName() {
		return docCapsiteName;
	}

	public void setDocCapsiteName(String docCapsiteName) {
		this.docCapsiteName = docCapsiteName;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getDcrId() {
		return dcrId;
	}

	public void setDcrId(Long dcrId) {
		this.dcrId = dcrId;
	}

	public Date getLogDate() {
		return logDate;
	}

	public void setLogDate(Date logDate) {
		this.logDate = logDate;
	}

	public String getProcessStatus() {
		return processStatus;
	}

	public void setProcessStatus(String processStatus) {
		this.processStatus = processStatus;
	}

	public String getActionDetails() {
		return actionDetails;
	}

	public void setActionDetails(String actionDetails) {
		this.actionDetails = actionDetails;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getDcrDateStr() {
		return dcrDateStr;
	}

	public void setDcrDateStr(String dcrDateStr) {
		this.dcrDateStr = dcrDateStr;
	}

	public String getDocCapsiteId() {
		return docCapsiteId;
	}

	public void setDocCapsiteId(String docCapsiteId) {
		this.docCapsiteId = docCapsiteId;
	}

}
