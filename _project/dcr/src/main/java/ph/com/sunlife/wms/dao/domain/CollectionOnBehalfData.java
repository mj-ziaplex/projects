package ph.com.sunlife.wms.dao.domain;

/**
 * This represents the table <b>DCROther</b> otherwise known as <b>Collection on
 * Behalf</b>. This denotes the amount collected in behalf of a payment for a
 * product outside of Sun Life of Canada Philippines, Inc. (such as that of Sun
 * Life Hong Kong, Indonesia, Malaysia, etc).
 * 
 * <p>
 * It has a <b>(Zero or One) to One</b> relationship with
 * {@link DCRBalancingToolProduct} table.
 * </p>
 * 
 * @author Zainal Limpao
 * 
 */
public class CollectionOnBehalfData {

	private String acf2id;

	private Long productTypeId;

	private double cashCounterAmount;

	private double cashNonCounterAmount;

	private double chequeOnUs;

	private double chequeRegional;

	private double chequeLocal;

	private double chequeNonCounter;

	private double chequeDollar;

	private double chequeUsDrawnMla;

	private double chequeUsDownOut;

	public String getAcf2id() {
		return acf2id;
	}

	public void setAcf2id(String acf2id) {
		this.acf2id = acf2id;
	}

	/**
	 * The <b>dcr_balancing_tool_product_id</b> column with bigint type. This
	 * represents the {@link DCRBalancingToolProduct#getId()}.
	 * 
	 * @return
	 */
	public Long getProductTypeId() {
		return productTypeId;
	}

	public void setProductTypeId(Long productTypeId) {
		this.productTypeId = productTypeId;
	}

	/**
	 * The <b>cash_counter_amt</b> column with decimal type.
	 * 
	 * @return
	 */
	public double getCashCounterAmount() {
		return cashCounterAmount;
	}

	public void setCashCounterAmount(double cashCounterAmount) {
		this.cashCounterAmount = cashCounterAmount;
	}

	/**
	 * The <b>cash_non_counter_amt</b> column with decimal type.
	 * 
	 * @return
	 */
	public double getCashNonCounterAmount() {
		return cashNonCounterAmount;
	}

	public void setCashNonCounterAmount(double cashNonCounterAmount) {
		this.cashNonCounterAmount = cashNonCounterAmount;
	}

	/**
	 * The <b>cheque_on_us</b> column with decimal type.
	 * 
	 * @return
	 */
	public double getChequeOnUs() {
		return chequeOnUs;
	}

	public void setChequeOnUs(double chequeOnUs) {
		this.chequeOnUs = chequeOnUs;
	}

	/**
	 * The <b>cheque_regional</b> column with decimal type.
	 * 
	 * @return
	 */
	public double getChequeRegional() {
		return chequeRegional;
	}

	public void setChequeRegional(double chequeRegional) {
		this.chequeRegional = chequeRegional;
	}

	/**
	 * The <b>cheque_local</b> column with decimal type.
	 * 
	 * @return
	 */
	public double getChequeLocal() {
		return chequeLocal;
	}

	public void setChequeLocal(double chequeLocal) {
		this.chequeLocal = chequeLocal;
	}

	/**
	 * The <b>cheque_non_counter</b> column with decimal type.
	 * 
	 * @return
	 */
	public double getChequeNonCounter() {
		return chequeNonCounter;
	}

	public void setChequeNonCounter(double chequeNonCounter) {
		this.chequeNonCounter = chequeNonCounter;
	}

	/**
	 * The <b>cheque_dollar</b> column with decimal type.
	 * 
	 * @return
	 */
	public double getChequeDollar() {
		return chequeDollar;
	}

	public void setChequeDollar(double chequeDollar) {
		this.chequeDollar = chequeDollar;
	}

	/**
	 * The <b>cheque_us_drawn_mla</b> column with decimal type.
	 * 
	 * @return
	 */
	public double getChequeUsDrawnMla() {
		return chequeUsDrawnMla;
	}

	public void setChequeUsDrawnMla(double chequeUsDrawnMla) {
		this.chequeUsDrawnMla = chequeUsDrawnMla;
	}

	/**
	 * The <b>cheque_us_drawn_out</b> column with decimal type.
	 * 
	 * @return
	 */
	public double getChequeUsDownOut() {
		return chequeUsDownOut;
	}

	public void setChequeUsDownOut(double chequeUsDownOut) {
		this.chequeUsDownOut = chequeUsDownOut;
	}

}
