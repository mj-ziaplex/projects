package ph.com.sunlife.wms.web.controller.form;

public class AddingMachineForm extends CashierWorkItemForm {

	private byte[] data;

	public byte[] getData() {
		return data;
	}

	public void setData(byte[] data) {
		this.data = data;
	}
	
}
