package ph.com.sunlife.wms.dao.domain;

public class DCRIpacLookup extends Entity {

	private String prodCode;

	private String payCode;

	private String paySubCode;

	private String paySubCurr;

	private Currency currency;

	private String paySubDesc;

	private Long productTypeId;

	private ProductType productType;
	
	private Long dcrIpacMappingId;
	
	private String group;
	
	private String subGroup;
	
	private String subGroupDescription;

	public String getProdCode() {
		return prodCode;
	}

	public void setProdCode(String prodCode) {
		this.prodCode = prodCode;
	}

	public String getPayCode() {
		return payCode;
	}

	public void setPayCode(String payCode) {
		this.payCode = payCode;
	}

	public String getPaySubCode() {
		return paySubCode;
	}

	public void setPaySubCode(String paySubCode) {
		this.paySubCode = paySubCode;
	}

	public String getPaySubCurr() {
		return paySubCurr;
	}

	public void setPaySubCurr(String paySubCurr) {
		this.paySubCurr = paySubCurr;
	}

	public Currency getCurrency() {
		if (this.paySubCurr != null) {
			this.currency = Currency.getCurrency(this.paySubCurr);
		}
		return currency;
	}

	public void setCurrency(Currency currency) {
		this.currency = currency;
	}

	public String getPaySubDesc() {
		return paySubDesc;
	}

	public void setPaySubDesc(String paySubDesc) {
		this.paySubDesc = paySubDesc;
	}

	public Long getProductTypeId() {
		return productTypeId;
	}

	public void setProductTypeId(Long productTypeId) {
		this.productTypeId = productTypeId;
	}

	public ProductType getProductType() {
		if (this.productTypeId != null) {
			this.productType = ProductType.getById(this.productTypeId);
		}

		return productType;
	}

	public void setProductType(ProductType productType) {
		this.productType = productType;
	}
	
	public Long getDcrIpacMappingId() {
		return dcrIpacMappingId;
	}

	public void setDcrIpacMappingId(Long dcrIpacMappingId) {
		this.dcrIpacMappingId = dcrIpacMappingId;
	}
	
	public String getGroup() {
		return group;
	}

	public void setGroup(String group) {
		this.group = group;
	}

	public String getSubGroup() {
		return subGroup;
	}

	public void setSubGroup(String subGroup) {
		this.subGroup = subGroup;
	}

	public String getSubGroupDescription() {
		return subGroupDescription;
	}

	public void setSubGroupDescription(String subGroupDescription) {
		this.subGroupDescription = subGroupDescription;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((payCode == null) ? 0 : payCode.hashCode());
		result = prime * result
				+ ((paySubCode == null) ? 0 : paySubCode.hashCode());
		result = prime * result
				+ ((paySubCurr == null) ? 0 : paySubCurr.hashCode());
		result = prime * result
				+ ((prodCode == null) ? 0 : prodCode.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DCRIpacLookup other = (DCRIpacLookup) obj;
		if (payCode == null) {
			if (other.payCode != null)
				return false;
		} else if (!payCode.equals(other.payCode))
			return false;
		if (paySubCode == null) {
			if (other.paySubCode != null)
				return false;
		} else if (!paySubCode.equals(other.paySubCode))
			return false;
		if (paySubCurr == null) {
			if (other.paySubCurr != null)
				return false;
		} else if (!paySubCurr.equals(other.paySubCurr))
			return false;
		if (prodCode == null) {
			if (other.prodCode != null)
				return false;
		} else if (!prodCode.equals(other.prodCode))
			return false;
		return true;
	}

	
}
