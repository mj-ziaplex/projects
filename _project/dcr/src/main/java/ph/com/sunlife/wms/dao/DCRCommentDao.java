package ph.com.sunlife.wms.dao;

import java.util.List;

import ph.com.sunlife.wms.dao.domain.DCRComment;
import ph.com.sunlife.wms.dao.exceptions.WMSDaoException;

/**
 * The Data Access Object for performing CRUD operations on {@link DCRComment}.
 * 
 * 
 * @author Zainal Limpao
 * @see WMSDao
 */
public interface DCRCommentDao extends WMSDao<DCRComment> {

	/**
	 * Gets a {@link List} of {@link DCRComment} associated with the given
	 * {@link DCRCashier}.
	 * 
	 * @param dcrCashierId
	 * @return
	 * @throws WMSDaoException
	 */
	List<DCRComment> getAllComments(Long dcrCashierId) throws WMSDaoException;

	List<DCRComment> getAllCommentsByDcrId(final Long dcrId)
			throws WMSDaoException;
}
