package ph.com.sunlife.wms.services.bo;

import java.util.Arrays;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.apache.commons.lang.StringUtils;

import ph.com.sunlife.wms.dao.domain.Company;
import ph.com.sunlife.wms.dao.domain.Currency;

public enum DSPath {

	SLAMCIP_CASH(1L, 1L, "Cash", "slamcip.cash.ds.jrxml.path"),

	SLAMCIP_CHEQUE(1L, 1L, "Cheque", "slamcip.check.ds.jrxml.path"),

	SLAMCID_CASH(2L, 2L, "Cash", "slamcid.cash.ds.jrxml.path"),

	SLAMCID_CHEQUE(2L, 2L, "Cheque", "slamcid.check.ds.jrxml.path"),
	
	SLOCPIP_CASH(3L, 1L, "Cash", "slocpi.peso.cash.ds.jrxml.path"),
	
	SLOCPIP_CHEQUE(3L, 1L, "Cheque", "slocpi.peso.check.ds.jrxml.path"),
	
	SLOCPID_CASH(3L, 2L, "Cash", "slocpi.dollar.cash.ds.jrxml.path"),
	
	SLOCPID_CHEQUE(3L, 2L, "Cheque", "slocpi.dollar.check.ds.jrxml.path"),

	PESO_CASH(null, 1L, "Cash", "peso.cash.ds.jrxml.path"),

	PESO_CHEQUE(null, 1L, "Cheque", "peso.cheque.ds.jrxml.path"),

	DOLLAR_CASH(null, 2L, "Cash", "dollar.cash.ds.jrxml.path"),

	DOLLAR_CHEQUE(null, 2L, "Cheque", "dollar.cheque.ds.jrxml.path");

	private DSPath(Long companyId, Long currencyId, String type, String path) {
		this.companyId = companyId;
		this.currencyId = currencyId;
		this.path = path;
		this.type = type;
	}

	private String type;

	private String path;

	private Long companyId;

	private Long currencyId;

	public String getPath() {
		return path;
	}

	public Long getCompanyId() {
		return companyId;
	}

	public Long getCurrencyId() {
		return currencyId;
	}

	public String getType() {
		return type;
	}

	public static DSPath getDSPath(final Long companyId, final Long currencyId,
			final String type) {

		DSPath thisDSPath = null;
		if (Company.SLAMCID.getId().equals(companyId)
				|| Company.SLAMCIP.getId().equals(companyId)) {
			List<DSPath> dspaths = Arrays.asList(DSPath.values());

			thisDSPath = (DSPath) CollectionUtils.find(dspaths,
					new Predicate() {

						@Override
						public boolean evaluate(Object object) {
							DSPath dsPath = (DSPath) object;

							Long thisCompanyId = dsPath.getCompanyId();
							Long thisCurrencyId = dsPath.getCurrencyId();
							String thisType = dsPath.getType();

							if (thisCompanyId != null
									&& thisCompanyId.equals(companyId)
									&& thisCurrencyId.equals(currencyId)
									&& thisType.equals(type)) {
								return true;
							}
							return false;
						}
					});
		}else if(Company.SLOCPI.getId().equals(companyId)){
			List<DSPath> dspaths = Arrays.asList(DSPath.values());
			
			thisDSPath = (DSPath) CollectionUtils.find(dspaths,
					new Predicate() {

						@Override
						public boolean evaluate(Object object) {
							DSPath dsPath = (DSPath) object;

							Long thisCompanyId = dsPath.getCompanyId();
							Long thisCurrencyId = dsPath.getCurrencyId();
							String thisType = dsPath.getType();

							if (thisCompanyId != null
									&& thisCompanyId.equals(companyId)
									&& thisCurrencyId.equals(currencyId)
									&& thisType.equals(type)) {
								return true;
							}
							return false;
						}
					});
		
		}else {
			if (Currency.PHP.getId().equals(currencyId)) {
				if (StringUtils.equalsIgnoreCase("Cash", type)) {
					thisDSPath = DSPath.PESO_CASH;
				} else if (StringUtils.equalsIgnoreCase("Cheque", type)) {
					thisDSPath = DSPath.PESO_CHEQUE;
				}
			} else if (Currency.USD.getId().equals(currencyId)) {
				if (StringUtils.equalsIgnoreCase("Cash", type)) {
					thisDSPath = DSPath.DOLLAR_CASH;
				} else if (StringUtils.equalsIgnoreCase("Cheque", type)) {
					thisDSPath = DSPath.DOLLAR_CHEQUE;
				}
			}

		}
		return thisDSPath;
	}
}
