package ph.com.sunlife.wms.web.controller;

import static ph.com.sunlife.wms.web.view.WMSJasperPdfView.JASPER_PRINT_KEY;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import ph.com.sunlife.wms.dao.domain.CheckType;
import ph.com.sunlife.wms.dao.domain.Company;
import ph.com.sunlife.wms.dao.domain.Currency;
import ph.com.sunlife.wms.dao.domain.ProductType;
import ph.com.sunlife.wms.security.Role;
import ph.com.sunlife.wms.services.DCRDepositSlipService;
import ph.com.sunlife.wms.services.DCRMixedChequePayeeService;
import ph.com.sunlife.wms.services.bo.CompanyBO;
import ph.com.sunlife.wms.services.bo.DCRBalancingToolBO;
import ph.com.sunlife.wms.services.bo.DCRBalancingToolProductBO;
import ph.com.sunlife.wms.services.bo.DCRCashDepositSlipBO;
import ph.com.sunlife.wms.services.bo.DCRChequeDepositSlipBO;
import ph.com.sunlife.wms.services.bo.DSPath;
import ph.com.sunlife.wms.services.bo.MixedChequePayeeBO;
import ph.com.sunlife.wms.services.bo.UserSession;
import ph.com.sunlife.wms.services.exception.ServiceException;
import ph.com.sunlife.wms.util.IpacUtil;
import ph.com.sunlife.wms.util.ParameterMap;
import ph.com.sunlife.wms.util.WMSDateUtil;
import ph.com.sunlife.wms.web.controller.form.CashDepositSlipForm;
import ph.com.sunlife.wms.web.controller.form.ChequeDepositSlipForm;
import ph.com.sunlife.wms.web.exception.ApplicationException;
import ph.com.sunlife.wms.web.view.WMSJasperPdfView;

/**
 * The {@link Controller} responsible for Deposit Slip Creation.
 * 
 * @author Jess Mendoza
 * 
 */
public class CreateDepositSlipController extends
		AbstractBalancingToolController {

	private static final Logger LOGGER = Logger
			.getLogger(CreateDepositSlipController.class);

	final static String COMPANY_KEY_SLAMCIP = "SLAMCI";
	final static String COMPANY_KEY_SLAMCID = "SLAMCI";
	final static String COMPANY_KEY_SLOCPI = "SLOCPI";
	final static String COMPANY_KEY_SLGFI = "SLGFI";
	final static String COMPANY_KEY_SLFPI = "SLFPI";
	final static String COMPANY_KEY_OTHER = "OTHER";

	final static String BC_SLAMCI = "bc.slamci";
	final static String BC_SLOCPI = "bc.slocpi";
	final static String BC_SLFPI = "bc.slfpi";
	final static String BC_SLGFI = "bc.slgfi";
	final static String BC_OTHER = "bc.other";
	final static String BC_PESO = "bc.peso";
	final static String BC_DOLLAR = "bc.dollar";
	final static String BC_CASH = "bc.cash";
	final static String BC_COU = "bc.cou";
	final static String BC_CLC = "bc.clc";
	final static String BC_CRC = "bc.crc";
	final static String BC_COT = "bc.cot";
	final static String BC_DC = "bc.dc";
	final static String BC_UCIM = "bc.ucim";
	final static String BC_UCOP = "bc.ucop";
	final static String BC_OTC = "bc.otc";
	final static String BC_VDS = "bc.vds";
	final static String BC_VDIL = "bc.vdil";
	final static String BC_TRADVUL = "bc.tradvul";
	final static String BC_GROUP = "bc.group";
	final static String BC_UNCONVERTED = "bc.unconverted";
	final static String BC_PRENEED = "bc.preneed";
	final static String BC_GAF = "bc.gaf";

	final static String PROD_CASH = "prod.cash";
	final static String PROD_CHEQUE = "prod.cheque";
	final static String PROD_TRADVUL = "prod.tradvul";
	final static String PROD_GROUP = "prod.group";
	final static String PROD_UNCONVERTED = "prod.unc";
	final static String PROD_PRENEED = "prod.pn";
	final static String PROD_GAF = "prod.gaf";
        
        // Added for PCO
        private String removeFromTotalProduct = "";

	private DCRDepositSlipService dcrDepositSlipService;
	private DCRMixedChequePayeeService dcrMixedChequePayeeService;

	private String pageTitle;

	public void generateBarCodeStrCash(ParameterMap map) {
		LOGGER.info("generateBarCodeStrCash ..");
		
		String barCodeStr = null;

		Date dcrDate = (Date) map.get("dcrDate");

		String dcrDateStr = StringUtils.upperCase(WMSDateUtil
				.toFormattedDateStr(dcrDate));

		String siteCode = (String) map.get("customerCenter");

		Company company = (Company) map.get("company");

		Long currency = (Long) map.get("currencyId");

		String paymentType = getMessage(BC_CASH);

		String documentType = getMessage(BC_VDS);

		String companyStr = getCompanyCodeForBarcode(company);

		String prodCodeStr = getProdCodeForBarcode(map);
		
		String acf2id = (String) map.get("acf2id");

		barCodeStr = "*" + dcrDateStr + siteCode + companyStr
				+ currency.toString() + paymentType + documentType
				+ prodCodeStr + acf2id + "*";

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Generated Bar Code for this is " + barCodeStr);
		}
		
		map.put("barCodeStr", barCodeStr);
		
		
	}

	private String getCompanyCodeForBarcode(Company company) {
		String companyStr = null;
		if (Company.SLAMCID.equals(company) || Company.SLAMCIP.equals(company)) {
			companyStr = getMessage(BC_SLAMCI);
		} else if (Company.SLOCPI.equals(company)) {
			companyStr = getMessage(BC_SLOCPI);
		} else if (Company.SLFPI.equals(company)) {
			companyStr = getMessage(BC_SLFPI);
		} else if (Company.SLGFI.equals(company)) {
			companyStr = getMessage(BC_SLGFI);
		} else if (Company.OTHER.equals(company)) {
			companyStr = getMessage(BC_OTHER);
		}
		return companyStr;
	}

	public void generateBarCodeStrCheque(ParameterMap map) {
		LOGGER.info("generateBarCodeStrCheque ..");
		String barCodeStr = null;

		Date dcrDate = (Date) map.get("dcrDate");

		String dcrDateStr = StringUtils.upperCase(WMSDateUtil
				.toFormattedDateStr(dcrDate));

		String siteCode = (String) map.get("customerCenter");

		Company company = (Company) map.get("company");

		Long currency = (Long) map.get("currencyId");

		String paymentType = getCheckTypeForBarcode(map);

		String documentType = getMessage(BC_VDS);

		String companyStr = getCompanyCodeForBarcode(company);

		String prodCodeStr = getProdCodeForBarcode(map);

		String currencyStr = currency != null ? String.valueOf(currency) : null;

		String acf2id = (String) map.get("acf2id");
		
		barCodeStr = "*" + dcrDateStr + siteCode + companyStr + currencyStr
				+ paymentType + documentType + prodCodeStr + acf2id +"*";

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Generated Bar Code for this is " + barCodeStr);
		}
		map.put("barCodeStr", barCodeStr);
	}

	private String getCheckTypeForBarcode(ParameterMap map) {
		CheckType checkType = (CheckType) map.get("checkType");
		String paymentType = null;
		if (CheckType.ON_US.equals(checkType)) {
			paymentType = getMessage(BC_COU);
		} else if (CheckType.LOCAL.equals(checkType)) {
			paymentType = getMessage(BC_CLC);
		} else if (CheckType.REGIONAL.equals(checkType)) {
			paymentType = getMessage(BC_CRC);
		} else if (CheckType.OT.equals(checkType)) {
			paymentType = getMessage(BC_COT);
		} else if (CheckType.DOLLARCHECK.equals(checkType)) {
			paymentType = getMessage(BC_DC);
		} else if (CheckType.USCD_MANILA.equals(checkType)) {
			paymentType = getMessage(BC_UCIM);
		} else if (CheckType.USCD_OUTSIDEPH.equals(checkType)) {
			paymentType = getMessage(BC_UCOP);
		} else if (CheckType.BANK_OTC.equals(checkType)) {
			paymentType = getMessage(BC_OTC);
		}
		return paymentType;
	}

	private String getProdCodeForBarcode(ParameterMap map) {
		String prodCode = (String) map.get("prodCode");
		String prodCodeStr = null;

		String cash = getMessage(PROD_CASH);
		String cheque = getMessage(PROD_CHEQUE);
		String tradvul = getMessage(PROD_TRADVUL);
		String group = getMessage(PROD_GROUP);
		String unconverted = getMessage(PROD_UNCONVERTED);
		String preneed = getMessage(PROD_PRENEED);
		String gaf = getMessage(PROD_GAF);
		if (cash.equalsIgnoreCase(prodCode)
				|| cheque.equalsIgnoreCase(prodCode)) {
			prodCodeStr = "0";
		} else if (tradvul.equalsIgnoreCase(prodCode)) {
			prodCodeStr = getMessage(BC_TRADVUL);
		} else if (group.equalsIgnoreCase(prodCode)) {
			prodCodeStr = getMessage(BC_GROUP);
		} else if (unconverted.equalsIgnoreCase(prodCode)) {
			prodCodeStr = getMessage(BC_UNCONVERTED);
		} else if (preneed.equalsIgnoreCase(prodCode)) {
			prodCodeStr = getMessage(BC_PRENEED);
		} else if (gaf.equalsIgnoreCase(prodCode)) {
			prodCodeStr = getMessage(BC_GAF);
		}
		return prodCodeStr;
	}

	@SuppressWarnings("unchecked")
	private void saveCashDepositSlip(HttpServletRequest request,
			HttpServletResponse response, CashDepositSlipForm form,
			final Long currencyId, final Long companyId)
			throws ApplicationException {
		LOGGER.info("Saving this Cash Deposit Slip...");
		
		Map<String, Object> map = new ParameterMap(form);
		map.put("currencyId", currencyId);
		map.put("companyId", companyId);

		try {
			byte[] data = null;
			UserSession userSession = getUserSession(request);
			String userId = userSession.getUserId();
			DSPath dsPath = DSPath.getDSPath(companyId, currencyId, "Cash");

			Map<String, Object> previewModel = previewCash(request, response,
					form, companyId, dsPath.getPath());
			JasperPrint jp = (JasperPrint) previewModel.get(JASPER_PRINT_KEY);
			data = JasperExportManager.exportReportToPdf(jp);
			map.put("deposlipFile", data);
			map.put("userId", userId);

			dcrDepositSlipService.saveCashDepositSlip(map);
			LOGGER.info("Save Successful");
		} catch (Exception e) {
			throw new ApplicationException(e);
		}
	}

	@SuppressWarnings("unchecked")
	private void saveCheckDepositSlip(HttpServletRequest request,
			HttpServletResponse response, ChequeDepositSlipForm form,
			final Long currencyId, final Long companyId,
			final CheckType checkType) throws ApplicationException {
		LOGGER.info("Saving this Check Deposit Slip...");
		
		Map<String, Object> map = new ParameterMap(form);
		map.put("currencyId", currencyId);
		map.put("companyId", companyId);
		map.put("checkType", checkType);

		try {
			byte[] data = null;
			UserSession userSession = getUserSession(request);
			String userId = userSession.getUserId();

			DSPath dsPath = DSPath.getDSPath(companyId, currencyId, "Cheque");

			Map<String, Object> previewModel = previewCheque(request, response,
					form, companyId, dsPath.getPath());
			JasperPrint jp = (JasperPrint) previewModel.get(JASPER_PRINT_KEY);
			data = JasperExportManager.exportReportToPdf(jp);
			map.put("deposlipFile", data);
			map.put("userId", userId);

			dcrDepositSlipService.saveChequeDepositSlip(map);
			LOGGER.info("Save Successful");
		} catch (Exception e) {
			LOGGER.error(e);
			throw new ApplicationException(e);
		}
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private Map previewCash(HttpServletRequest request,
			HttpServletResponse response, CashDepositSlipForm form,
			Long companyId, String path) throws ApplicationException {
		LOGGER.info("Generating PDF file for this Cash Deposit Slip...");

		JasperDesign jasperDesign;
		try {
			LOGGER.info("Attempting to load Jasper Design...");
			jasperDesign = JRXmlLoader.load(getServletContext().getRealPath(
					getMessage(path)));

			if (jasperDesign != null) {
				LOGGER.info("Jasper Design has been loaded successfully");
			}

			LOGGER.info("Attempting to load Compiled Jasper Report...");
			JasperReport jasperReport = JasperCompileManager
					.compileReport(jasperDesign);

			if (jasperReport != null) {
				LOGGER.info("Jasper Report has been loaded successfully");
			}

			ParameterMap map = new ParameterMap(form);
			String acf2id = (String) map.get("cashierName");
			generateBarCodeStrCash(map);
			form.setCompanyId(companyId);

			ModelAndView modelAndView = showBalancingToolPage(request,
					response, form);
			Map model = modelAndView.getModel();

			List<DCRBalancingToolProductBO> products = (List<DCRBalancingToolProductBO>) model
					.get(PRODUCTS_TO_DISPLAY_KEY);

			if (companyId == Company.SLAMCIP.getId()) {
				map = dcrDepositSlipService
						.buildMapForFundBreakdownsSlamciPesoCash(products, map);
			} else if (companyId == Company.SLAMCID.getId()) {
				map = dcrDepositSlipService
						.buildMapForFundBreakdownsSlamciDollarCash(products,
								map);
			}

			LOGGER.info("Attempting to create Jasper Print...");
			JasperPrint jasperPrint = JasperFillManager.fillReport(
					jasperReport, map);

			if (jasperPrint != null) {
				LOGGER.info("Jasper Print has been generated successfully...");
			}

			model.put(JASPER_PRINT_KEY, jasperPrint);

			LOGGER.info("Creating Model and View Object...");
			return model;

		} catch (Exception e) {
			throw new ApplicationException(e);
		}
	}

    @SuppressWarnings({"unchecked", "rawtypes"})
    private Map previewCheque(HttpServletRequest request,
            HttpServletResponse response, final ChequeDepositSlipForm form,
            Long companyId, String path) throws ApplicationException {
        LOGGER.info("Generating PDF file for this Check Deposit Slip...");

        JasperDesign jasperDesign;
        try {
            LOGGER.info("Attempting to load Jasper Design...");
            jasperDesign = JRXmlLoader.load(getServletContext().getRealPath(getMessage(path)));

            if (jasperDesign != null) {
                LOGGER.info("Jasper Design has been loaded successfully");
            }

            LOGGER.info("Attempting to load Compiled Jasper Report...");
            JasperReport jasperReport = JasperCompileManager.compileReport(jasperDesign);

            if (jasperReport != null) {
                LOGGER.info("Jasper Report has been loaded successfully");
            }

            ParameterMap map = new ParameterMap(form);

            generateBarCodeStrCheque(map);
            form.setCompanyId(companyId);
            Map model = new HashMap();

            if (companyId == Company.SLAMCIP.getId()) {
                ModelAndView modelAndView = showBalancingToolPage(request, response, form);
                model = modelAndView.getModel();
                List<DCRBalancingToolProductBO> products = (List<DCRBalancingToolProductBO>) model.get(PRODUCTS_TO_DISPLAY_KEY);
                map = dcrDepositSlipService.buildMapForFundBreakdownsSlamciPesoCheque(products,map);
            } else if (companyId == Company.SLAMCID.getId()) {
                ModelAndView modelAndView = showBalancingToolPage(request, response, form);
                model = modelAndView.getModel();
                List<DCRBalancingToolProductBO> products = (List<DCRBalancingToolProductBO>) model.get(PRODUCTS_TO_DISPLAY_KEY);
                map = dcrDepositSlipService.buildMapForFundBreakdownsSlamciDollarCheque(products, map);
            }
            
            List<MixedChequePayeeBO> filteredBOs = dcrMixedChequePayeeService
                    .getMixedChequePayeeBOsForPrinting(form.getDcrCashierId(), map);

            LOGGER.info("Attempting to create Jasper Print...");
            JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, map, new JRBeanCollectionDataSource(filteredBOs));

            if (jasperPrint != null) {
                LOGGER.info("Jasper Print has been generated successfully...");
            }

            model.put(JASPER_PRINT_KEY, jasperPrint);

            LOGGER.info("Creating Model and View Object...");
            return model;

        } catch (Exception e) {
            LOGGER.error("Error encountered generating DS preview for cheque: ", e);
            throw new ApplicationException(e);
        }
    }

	private double getSlocpiTotal(List<MixedChequePayeeBO> list,
			Currency currency) {
		double mixedSlfpiTotal = 0.0;
		if (!CollectionUtils.isEmpty(list)) {
			for (MixedChequePayeeBO mixedChequePayeeBO : list) {
				if (currency.equals(mixedChequePayeeBO.getCurrency())) {
					mixedSlfpiTotal += mixedChequePayeeBO.getSlocpi();
				}
			}
		}
		return mixedSlfpiTotal;

	}

	private double getSlfpiTotal(List<MixedChequePayeeBO> list,
			Currency currency) {
		double mixedSlfpiTotal = 0.0;
		if (!CollectionUtils.isEmpty(list)) {
			for (MixedChequePayeeBO mixedChequePayeeBO : list) {
				if (currency.equals(mixedChequePayeeBO.getCurrency())) {
					mixedSlfpiTotal += mixedChequePayeeBO.getSlfpi();
				}
			}
		}
		return mixedSlfpiTotal;

	}

	private double getTotalCashCollection(
			List<DCRBalancingToolProductBO> products, Currency currency) {
		double totalCashCollection = 0.0;
		for (DCRBalancingToolProductBO product : products) {
			if (currency.equals(product.getProductType().getCurrency())) {
				totalCashCollection += product.getTotalCashCounter();
			}
		}

		return totalCashCollection;
	}

    private double getTotalPcoPurchaseCollection(
            List<DCRBalancingToolProductBO> products,
            Currency currency) {
        
        List<String> productList = Arrays.asList(StringUtils.split(removeFromTotalProduct));
        LOGGER.info("Compute PCO related transaction to be cancelled in DS...");
        LOGGER.info("EXCLUDE PRODUCT LIST: " + productList.toString());
        double totalPcoPurchaseCollection = 0.0;
        for (DCRBalancingToolProductBO product : products) {
            if (currency.equals(product.getProductType().getCurrency())) {
                if(productList.contains(product.getProductType().getProductCode())) {
                    LOGGER.info("PCO record found.....");
                }
                if ("MF".equalsIgnoreCase(product.getProductType().getProductCode())) {
                    totalPcoPurchaseCollection += product.getTotalCashCounter();
                }
            }
        }

        return totalPcoPurchaseCollection;
    }

    private double getTotalChequeCollection(
            List<DCRBalancingToolProductBO> products,
            CheckType checkType) {

        double total = 0.00;
        if (CheckType.ON_US.equals(checkType)) {
            total = getTotalCheckOnUs(products);
        } else if (CheckType.LOCAL.equals(checkType)) {
            total = getTotalCheckLocal(products);
        } else if (CheckType.REGIONAL.equals(checkType)) {
            total = getTotalCheckRegional(products);
        } else if (CheckType.OT.equals(checkType)) {
            total = getTotalCheckOT(products);
        } else if (CheckType.DOLLARCHECK.equals(checkType)) {
            total = getTotalDollarCheck(products);
        } else if (CheckType.USCD_MANILA.equals(checkType)) {
            total = getTotalUSCDManila(products);
        } else if (CheckType.USCD_OUTSIDEPH.equals(checkType)) {
            total = getTotalUSCDOutPH(products);
        } else if (CheckType.BANK_OTC.equals(checkType)) {
            total = getTotalBankOTC(products);
        }
        return total;
    }

	private double getTotalCheckOnUs(List<DCRBalancingToolProductBO> products) {
		double totalCheckOnUs = 0.0;
		for (DCRBalancingToolProductBO product : products) {
			totalCheckOnUs += product.getTotalCheckOnUs();
		}

		return totalCheckOnUs;
	}

	private double getTotalCheckLocal(List<DCRBalancingToolProductBO> products) {
		double totalCheckLocal = 0.0;
		for (DCRBalancingToolProductBO product : products) {
			if (ProductType.SLIFPI_GAF.equals(product.getProductType())) {
				totalCheckLocal += product.getTotalCheckGaf();
			} else {
				totalCheckLocal += product.getTotalCheckLocal();
			}

		}
		return totalCheckLocal;
	}

	private double getTotalCheckRegional(
			List<DCRBalancingToolProductBO> products) {
		double totalCheckRegional = 0.0;
		for (DCRBalancingToolProductBO product : products) {
			totalCheckRegional += product.getTotalCheckRegional();
		}
		return totalCheckRegional;
	}

	private double getTotalCheckOT(List<DCRBalancingToolProductBO> products) {
		double totalCheckOT = 0.0;
		for (DCRBalancingToolProductBO product : products) {
			totalCheckOT += product.getTotalCheckOT();
		}
		return totalCheckOT;
	}

	private double getTotalDollarCheck(List<DCRBalancingToolProductBO> products) {
		double totalDollarCheck = 0.0;
		for (DCRBalancingToolProductBO product : products) {
			totalDollarCheck += product.getTotalDollarCheque();
		}
		return totalDollarCheck;
	}

	private double getTotalUSCDManila(List<DCRBalancingToolProductBO> products) {
		double totalUSCDManila = 0.0;
		for (DCRBalancingToolProductBO product : products) {
			totalUSCDManila += product.getTotalUsCheckInManila();
		}
		return totalUSCDManila;
	}

	private double getTotalUSCDOutPH(List<DCRBalancingToolProductBO> products) {
		double totalUSCDOutPH = 0.0;
		for (DCRBalancingToolProductBO product : products) {
			totalUSCDOutPH += product.getTotalUsCheckOutPh();
		}
		return totalUSCDOutPH;
	}

	private double getTotalBankOTC(List<DCRBalancingToolProductBO> products) {
		double totalBankOTC = 0.0;
		for (DCRBalancingToolProductBO product : products) {
			totalBankOTC += product.getTotalBankOTCCheckPayment();
		}
		return totalBankOTC;
	}

	private void filterProductsByCompany(final Company company,
			List<DCRBalancingToolProductBO> products) {
		CollectionUtils.filter(products, new Predicate() {

			@Override
			public boolean evaluate(Object object) {
				DCRBalancingToolProductBO product = (DCRBalancingToolProductBO) object;
				ProductType productType = product.getProductType();
				return (company.equals(productType.getCompany()));
			}
		});
	}

	private void filterProductsBySpecificProduct(final ProductType prodType,
			List<DCRBalancingToolProductBO> products) {
		if ((prodType != null)
			&& (!Company.SLAMCID.equals(prodType.getCompany()) && !Company.SLAMCIP.equals(prodType.getCompany()))
                        && !Company.SLOCPI.equals(prodType.getCompany())) {

			CollectionUtils.filter(products, new Predicate() {

				@Override
				public boolean evaluate(Object object) {
					DCRBalancingToolProductBO product = (DCRBalancingToolProductBO) object;
					ProductType productType = product.getProductType();
					return (productType.equals(prodType));
				}
			});
		}
	}
	
	private double getTotalCashForSpecificProductSlocpi(final ProductType prodType,
			List<DCRBalancingToolProductBO> products,Currency currency){
		double total = 0.00;
		List<DCRBalancingToolProductBO> tempProducts = new ArrayList<DCRBalancingToolProductBO>(products);
			if ((prodType != null) && Company.SLOCPI.equals(prodType.getCompany())) {
				CollectionUtils.filter(tempProducts, new Predicate() {
					@Override
					public boolean evaluate(Object object) {
						DCRBalancingToolProductBO product = (DCRBalancingToolProductBO) object;
						ProductType productType = product.getProductType();
						return (productType.equals(prodType));
					}
				});
					
				}
			
			total = getTotalCashCollection(tempProducts, currency);
		return total;
	}
	
	private double getTotalChequeForSpecificProductSlocpi(final ProductType prodType,
			List<DCRBalancingToolProductBO> products,CheckType checkType){
		double total = 0.00;
		List<DCRBalancingToolProductBO> tempProducts = new ArrayList<DCRBalancingToolProductBO>(products);
			if ((prodType != null) && Company.SLOCPI.equals(prodType.getCompany())) {
				CollectionUtils.filter(tempProducts, new Predicate() {
					@Override
					public boolean evaluate(Object object) {
						DCRBalancingToolProductBO product = (DCRBalancingToolProductBO) object;
						ProductType productType = product.getProductType();
						return (productType.equals(prodType));
					}
				});
					
				}
			
			total = getTotalChequeCollection(tempProducts, checkType);
		return total;
	}
			
			
		

	@SuppressWarnings("unchecked")
	private ModelAndView doShowCDSPageCash(HttpServletRequest request,
			HttpServletResponse response, CashDepositSlipForm form,
			Company company, Currency currency, String viewNameKey,
			String saveMappingKey, String previewMappingKey, long pageSequence,
			String prodCode, ProductType prodType) throws Exception,
			ApplicationException {

		LOGGER.info("CDS Controller v 1.38 4/11/2013");
		
		UserSession userSession = getUserSession(request);
		String centerCode = userSession.getSiteCode();

		
		form.setCompanyId(company.getId());

		ModelAndView modelAndView = showBalancingToolPage(request, response,
				form);

		Map<String, Object> model = modelAndView.getModel();
		List<DCRBalancingToolProductBO> products = (List<DCRBalancingToolProductBO>) model
				.get(PRODUCTS_TO_DISPLAY_KEY);
		DCRBalancingToolBO balancingTool = (DCRBalancingToolBO) model
				.get(SPECIFIC_BALANCING_TOOL_KEY);
		boolean isConfirmed = (balancingTool.getDateConfirmed() != null)
				|| (balancingTool.getDateReConfirmed() != null);

		this.filterProductsByCompany(company, products);
		this.filterProductsBySpecificProduct(prodType, products);
		double totalCashCollection = this.getTotalCashCollection(products, currency);
                // Added for PCO purchase - to be removed in DS
                double totalPcoPurchaseCollection = this.getTotalPcoPurchaseCollection(products, currency);
                
		boolean isSaveDisabled = true;

		DCRCashDepositSlipBO bo = null;
		try {
			String message = getMessage(prodCode);
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("Querying Deposit Slip for the following parameters:");
				LOGGER.debug("Company: " + company.getId());
				LOGGER.debug("Currency: " + currency.getId());
				LOGGER.debug("DCR Cashier WorkItem ID: " + form.getDcrCashierId());
				LOGGER.debug("Message: " + message);
			}
			
			bo = dcrDepositSlipService.getCashDepositSlip(
					form.getDcrCashierId(), currency.getId(), company.getId(),
					message);

			if (bo == null) {
				LOGGER.debug("Deposit Slip is New");
				
				bo = new DCRCashDepositSlipBO();
				bo.setCompany(company);
				bo.setCurrency(currency);

				bo = dcrDepositSlipService.getIpacValuesForDisplay(centerCode,
						bo);
				if (bo.getDepositoryBank().getId() == null) {
					LOGGER.debug("No Bank for this DepoSlip, will be using SLOCPI Info");
					
					bo = dcrDepositSlipService.getSlocpiBankDetails(centerCode, bo);
				}
				bo.setRequiredTotalAmount(totalCashCollection);
                                // Added for PCO purchase - to be removed in DS
                                bo.setPcoRequiredTotal(totalPcoPurchaseCollection);
				bo.setProdCode(message);
				isSaveDisabled = false;
			}

			bo.setCustomerCenter(centerCode);
			bo.setCustomerCenterName(dcrDepositSlipService.getCCNameById(centerCode));
			String acf2id = userSession.getUserId();
			bo.setAcf2id(acf2id);
			bo.setCashierName(dcrDepositSlipService.getCashierNameByAcf2id(acf2id));
			bo.setCompanyStr(IpacUtil.toIpacFullCompanyName(company.getName()));
			if(bo.getCompany().equals(Company.SLOCPI)){
				bo.setTradVulCollectionTotal(this.getTotalCashForSpecificProductSlocpi(ProductType.SLOCPI_P_TRADVUL, products, currency));
				bo.setGrpLifeCollectionTotal(this.getTotalCashForSpecificProductSlocpi(ProductType.SLOCPI_GROUPLIFE, products, currency));
			}
			
			modelAndView.addObject("bo", bo);
			modelAndView.addObject("isSavedDisabled", isSaveDisabled);
			modelAndView.setViewName(getMessage(viewNameKey));
			modelAndView.addObject("saveMapping", getMessage(saveMappingKey));
			modelAndView.addObject("previewMapping",
					getMessage(previewMappingKey));
			modelAndView.addObject(PAGE_SEQUENCE_KEY, pageSequence);
			modelAndView.addObject("isConfirmed", isConfirmed);
			this.setPageTitle(bo.getCompanyStr() + " Cash Deposit Slip");

		} catch (ServiceException e) {
			LOGGER.error(e);
			throw new ApplicationException(e);
		}

		return modelAndView;
	}

    @SuppressWarnings("unchecked")
    private ModelAndView doShowCDSPageCheque(HttpServletRequest request,
            HttpServletResponse response, ChequeDepositSlipForm form,
            Company company, Currency currency, String viewNameKey,
            String saveMappingKey, String previewMappingKey, long pageSequence,
            CheckType checkType, String prodCode, boolean hasMixCheque,
            ProductType prodType) throws Exception, ApplicationException {

        LOGGER.info("CDS Controller v 1.38 4/11/2013");
        LOGGER.info("Creating Check Deposit Slip Input Page...");

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Creating Deposit Slip for: " + company.name());
        }

        UserSession userSession = getUserSession(request);
        String centerCode = userSession.getSiteCode();

        form.setCompanyId(company.getId());

        ModelAndView modelAndView = showBalancingToolPage(request, response, form);

        Map<String, Object> model = modelAndView.getModel();
        List<DCRBalancingToolProductBO> products =
                (List<DCRBalancingToolProductBO>) model.get(PRODUCTS_TO_DISPLAY_KEY);
        DCRBalancingToolBO balancingTool =
                (DCRBalancingToolBO) model.get(SPECIFIC_BALANCING_TOOL_KEY);

        boolean isConfirmed = (balancingTool.getDateConfirmed() != null) || (balancingTool.getDateReConfirmed() != null);

        this.filterProductsByCompany(company, products);
        this.filterProductsBySpecificProduct(prodType, products);
        double totalChequeCollection = this.getTotalChequeCollection(products, checkType);

        boolean isSaveDisabled = true;

        form.setCheckType(checkType);
        form.setCompany(company);
        form.setCurrency(currency);

        DCRChequeDepositSlipBO bo = null;
        try {
            bo = dcrDepositSlipService.getChequeDepositSlip(
                    form.getDcrCashierId(), currency.getId(), company.getId(),
                    checkType, getMessage(prodCode));

            if (bo == null) {
                bo = new DCRChequeDepositSlipBO();
                bo.setCompany(company);
                bo.setCurrency(currency);
                bo.setCheckType(checkType);
                bo = dcrDepositSlipService.getIpacValuesForDisplay(centerCode, bo);
                bo.setCustomerCenter(centerCode);
                bo.setRequiredTotalAmount(totalChequeCollection);
                bo.setTotalCheckAmount(totalChequeCollection);
                bo.setProdCode(getMessage(prodCode));
                isSaveDisabled = false;

                if (bo.getDepositoryBank().getId() == null) {
                    bo = dcrDepositSlipService.getSlocpiBankDetails(centerCode, bo);
                }
            } else {
                bo.setTotalCheckAmount(totalChequeCollection);
            }

            if (hasMixCheque) {
                ParameterMap map = new ParameterMap(form);
                List<MixedChequePayeeBO> filteredBOs = 
                        dcrMixedChequePayeeService.getMixedChequePayeeBOsForPrinting(form.getDcrCashierId(), map);
                
                double slocpiTotal = getSlocpiTotal(filteredBOs, currency);
                double slfpiTotal = getSlfpiTotal(filteredBOs, currency);
                bo.setSlfpiTotal(slfpiTotal);
                bo.setSlocpiTotal(slocpiTotal);
                if (CollectionUtils.isNotEmpty(filteredBOs)) {
                    if (LOGGER.isDebugEnabled()) {
                        LOGGER.debug("Number of mixed checks " + filteredBOs.size());
                    }

                    String companyName = company.name();

                    for (MixedChequePayeeBO mc : filteredBOs) {
                        CompanyBO c = mc.getPayee();
                        String mixedChequePayee = c.name();

                        if (LOGGER.isDebugEnabled()) {
                            LOGGER.debug("Payee: " + mixedChequePayee);
                            LOGGER.debug("Total Amount: " + mc.getCheckAmount());
                            LOGGER.debug("SLFPI: " + mc.getSlfpi());
                            LOGGER.debug("SLOCPI: " + mc.getSlocpi());
                        }

                        if (StringUtils.equalsIgnoreCase(companyName, "SLFPI")) {
                            if (StringUtils.equalsIgnoreCase(companyName, mixedChequePayee)) {
                                bo.setTotalCheckAmount(bo.getTotalCheckAmount() + mc.getSlocpi());
                            } else {
                                bo.setTotalCheckAmount(bo.getTotalCheckAmount() - mc.getSlfpi());
                            }
                        } else if (StringUtils.equalsIgnoreCase(companyName, "SLOCPI")) {
                            if (StringUtils.equalsIgnoreCase(companyName, mixedChequePayee)) {
                                bo.setTotalCheckAmount(bo.getTotalCheckAmount() + mc.getSlfpi());
                            } else {
                                bo.setTotalCheckAmount(bo.getTotalCheckAmount() - mc.getSlocpi());
                            }
                        }
                    }
                }
            }
            
            if (bo.getCompany().equals(Company.SLOCPI)) {
                bo.setTradVulCollectionTotal(this.getTotalChequeForSpecificProductSlocpi(ProductType.SLOCPI_P_TRADVUL, products, checkType));
                bo.setGrpLifeCollectionTotal(this.getTotalChequeForSpecificProductSlocpi(ProductType.SLOCPI_GROUPLIFE, products, checkType));
            }

            bo.setCustomerCenter(centerCode);
            bo.setCustomerCenterName(dcrDepositSlipService.getCCNameById(centerCode));
            String acf2id = userSession.getUserId();
            bo.setAcf2id(acf2id);
            bo.setCashierName(dcrDepositSlipService.getCashierNameByAcf2id(acf2id));
            bo.setCompanyStr(IpacUtil.toIpacFullCompanyName(company.getName()));
            
            // Set data to display 
            modelAndView.addObject("bo", bo);
            modelAndView.addObject("hasMixCheque", hasMixCheque);
            modelAndView.addObject("isSavedDisabled", isSaveDisabled);
            modelAndView.setViewName(getMessage(viewNameKey));
            modelAndView.addObject("saveMapping", getMessage(saveMappingKey));
            modelAndView.addObject("previewMapping", getMessage(previewMappingKey));
            modelAndView.addObject(PAGE_SEQUENCE_KEY, pageSequence);
            modelAndView.addObject("isConfirmed", isConfirmed);
            this.setPageTitle(bo.getCompanyStr() + " Cheque Deposit Slip");
        } catch (ServiceException e) {
            LOGGER.error("Error in generating DS cheque data: ", e);
            throw new ApplicationException(e);
        }

        return modelAndView;

    }

	public final ModelAndView doShowCDSPageSLAMCIPesoCash(
			HttpServletRequest request, HttpServletResponse response,
			CashDepositSlipForm form) throws Exception {

		return doShowCDSPageCash(request, response, form, Company.SLAMCIP,
				Currency.PHP, "cdspage.cash.peso", "slamcip.cash.save",
				"slamcip.cash.preview", 9L, "prod.cash", null);

	}

	public final ModelAndView doShowCDSPageSLAMCIPesoChequeOnUs(
			HttpServletRequest request, HttpServletResponse response,
			ChequeDepositSlipForm form) throws Exception, ApplicationException {

		return doShowCDSPageCheque(request, response, form, Company.SLAMCIP,
				Currency.PHP, "cdspage.cou.peso", "slamcip.cou.save",
				"slamcip.cou.preview", 10L, CheckType.ON_US, "prod.cheque",
				false, null);

	}

	public final ModelAndView doShowCDSPageSLAMCIPesoChequeLocalClearing(
			HttpServletRequest request, HttpServletResponse response,
			ChequeDepositSlipForm form) throws Exception, ApplicationException {

		return doShowCDSPageCheque(request, response, form, Company.SLAMCIP,
				Currency.PHP, "cdspage.clc.peso", "slamcip.clc.save",
				"slamcip.clc.preview", 11L, CheckType.LOCAL, "prod.cheque",
				false, null);

	}

	public final ModelAndView doShowCDSPageSLAMCIPesoChequeRegionalClearing(
			HttpServletRequest request, HttpServletResponse response,
			ChequeDepositSlipForm form) throws Exception, ApplicationException {

		return doShowCDSPageCheque(request, response, form, Company.SLAMCIP,
				Currency.PHP, "cdspage.crc.peso", "slamcip.crc.save",
				"slamcip.crc.preview", 12L, CheckType.REGIONAL, "prod.cheque",
				false, null);

	}

	public final ModelAndView doShowCDSPageSLAMCIPesoChequeOT(
			HttpServletRequest request, HttpServletResponse response,
			ChequeDepositSlipForm form) throws Exception, ApplicationException {

		return doShowCDSPageCheque(request, response, form, Company.SLAMCIP,
				Currency.PHP, "cdspage.cot.peso", "slamcip.cot.save",
				"slamcip.cot.preview", 13L, CheckType.OT, "prod.cheque", false,
				null);

	}

	public ModelAndView saveSLAMCIPesoCashDepositSlip(
			HttpServletRequest request, HttpServletResponse response,
			CashDepositSlipForm form) throws Exception {

		saveCashDepositSlip(request, response, form, Currency.PHP.getId(),
				Company.SLAMCIP.getId());
		return this.doShowCDSPageSLAMCIPesoCash(request, response, form);
	}

	public ModelAndView saveSLAMCIPesoChequeOnUsDepositSlip(
			HttpServletRequest request, HttpServletResponse response,
			ChequeDepositSlipForm form) throws Exception, ApplicationException {

		saveCheckDepositSlip(request, response, form, Currency.PHP.getId(),
				Company.SLAMCIP.getId(), CheckType.ON_US);

		return doShowCDSPageSLAMCIPesoChequeOnUs(request, response, form);
	}

	public ModelAndView saveSLAMCIPesoChequeLocalDepositSlip(
			HttpServletRequest request, HttpServletResponse response,
			ChequeDepositSlipForm form) throws Exception, ApplicationException {

		saveCheckDepositSlip(request, response, form, Currency.PHP.getId(),
				Company.SLAMCIP.getId(), CheckType.LOCAL);

		return doShowCDSPageSLAMCIPesoChequeLocalClearing(request, response,
				form);
	}

	public ModelAndView saveSLAMCIPesoChequeRegionalDepositSlip(
			HttpServletRequest request, HttpServletResponse response,
			ChequeDepositSlipForm form) throws Exception, ApplicationException {

		saveCheckDepositSlip(request, response, form, Currency.PHP.getId(),
				Company.SLAMCIP.getId(), CheckType.REGIONAL);

		return doShowCDSPageSLAMCIPesoChequeRegionalClearing(request, response,
				form);
	}

	public ModelAndView saveSLAMCIPesoChequeOTDepositSlip(
			HttpServletRequest request, HttpServletResponse response,
			ChequeDepositSlipForm form) throws Exception, ApplicationException {

		saveCheckDepositSlip(request, response, form, Currency.PHP.getId(),
				Company.SLAMCIP.getId(), CheckType.OT);

		return doShowCDSPageSLAMCIPesoChequeOT(request, response, form);
	}

	@SuppressWarnings({ "rawtypes" })
	public ModelAndView previewSlamciPesoCashDepositSlip(
			HttpServletRequest request, HttpServletResponse response,
			CashDepositSlipForm form) throws ApplicationException {

		Map model = previewCash(request, response, form,
				Company.SLAMCIP.getId(), "slamcip.cash.ds.jrxml.path");

		return new ModelAndView(new WMSJasperPdfView(), model);
	}

	@SuppressWarnings({ "rawtypes" })
	public ModelAndView previewSlamciPesoChequeDepositSlip(
			HttpServletRequest request, HttpServletResponse response,
			ChequeDepositSlipForm form) throws ApplicationException {

		Map model = previewCheque(request, response, form,
				Company.SLAMCIP.getId(), "slamcip.check.ds.jrxml.path");

		return new ModelAndView(new WMSJasperPdfView(), model);

	}

	/*
	 * @SuppressWarnings({ "rawtypes" }) public ModelAndView
	 * previewSLAMCIPesoChequeLocalDepositSlip( HttpServletRequest request,
	 * HttpServletResponse response, ChequeDepositSlipForm form) throws
	 * ApplicationException {
	 * 
	 * Map model = previewCheque(request, response, form,
	 * Company.SLAMCIP.getId(), "slamcip.check.ds.jrxml.path");
	 * 
	 * return new ModelAndView(new WMSJasperPdfView(), model);
	 * 
	 * }
	 * 
	 * @SuppressWarnings({ "rawtypes" }) public ModelAndView
	 * previewSLAMCIPesoChequeRegionalDepositSlip( HttpServletRequest request,
	 * HttpServletResponse response, ChequeDepositSlipForm form) throws
	 * ApplicationException {
	 * 
	 * Map model = previewCheque(request, response, form,
	 * Company.SLAMCIP.getId(), "slamcip.check.ds.jrxml.path");
	 * 
	 * return new ModelAndView(new WMSJasperPdfView(), model);
	 * 
	 * }
	 * 
	 * @SuppressWarnings({ "rawtypes" }) public ModelAndView
	 * previewSLAMCIPesoChequeOTDepositSlip( HttpServletRequest request,
	 * HttpServletResponse response, ChequeDepositSlipForm form) throws
	 * ApplicationException {
	 * 
	 * Map model = previewCheque(request, response, form,
	 * Company.SLAMCIP.getId(), "slamcip.check.ds.jrxml.path");
	 * 
	 * return new ModelAndView(new WMSJasperPdfView(), model);
	 * 
	 * }
	 */
	public final ModelAndView doShowCDSPageSLAMCIDollarCash(
			HttpServletRequest request, HttpServletResponse response,
			CashDepositSlipForm form) throws Exception, ApplicationException {

		return doShowCDSPageCash(request, response, form, Company.SLAMCID,
				Currency.USD, "cdspage.cash.dollar", "slamcid.cash.save",
				"slamcid.cash.preview", 14L, "prod.cash", null);

	}

	public final ModelAndView doShowCDSPageSLAMCIDollarCheck(
			HttpServletRequest request, HttpServletResponse response,
			ChequeDepositSlipForm form) throws Exception, ApplicationException {

		return doShowCDSPageCheque(request, response, form, Company.SLAMCID,
				Currency.USD, "cdspage.dc", "slamcid.dc.save",
				"slamcid.dc.preview", 15L, CheckType.DOLLARCHECK,
				"prod.cheque", false, null);

	}

	public final ModelAndView doShowCDSPageSLAMCIDollarUSCheckInManila(
			HttpServletRequest request, HttpServletResponse response,
			ChequeDepositSlipForm form) throws Exception, ApplicationException {

		return doShowCDSPageCheque(request, response, form, Company.SLAMCID,
				Currency.USD, "cdspage.ucim", "slamcid.ucim.save",
				"slamcid.ucim.preview", 16L, CheckType.USCD_MANILA,
				"prod.cheque", false, null);

	}

	public final ModelAndView doShowCDSPageSLAMCIDollarUSCheckOutsidePH(
			HttpServletRequest request, HttpServletResponse response,
			ChequeDepositSlipForm form) throws Exception, ApplicationException {

		return doShowCDSPageCheque(request, response, form, Company.SLAMCID,
				Currency.USD, "cdspage.ucop", "slamcid.ucop.save",
				"slamcid.ucop.preview", 17L, CheckType.USCD_OUTSIDEPH,
				"prod.cheque", false, null);

	}

	public final ModelAndView doShowCDSPageSLAMCIDollarBankOTC(
			HttpServletRequest request, HttpServletResponse response,
			ChequeDepositSlipForm form) throws Exception, ApplicationException {

		return doShowCDSPageCheque(request, response, form, Company.SLAMCID,
				Currency.USD, "cdspage.otc", "slamcid.otc.save",
				"slamcid.otc.preview", 18L, CheckType.BANK_OTC, "prod.cheque",
				false, null);

	}

	public ModelAndView saveSLAMCIDollarCashDepositSlip(
			HttpServletRequest request, HttpServletResponse response,
			CashDepositSlipForm form) throws Exception, ApplicationException {

		saveCashDepositSlip(request, response, form, Currency.USD.getId(),
				Company.SLAMCID.getId());
		return this.doShowCDSPageSLAMCIDollarCash(request, response, form);
	}

	public ModelAndView saveSLAMCIDollarCheck(HttpServletRequest request,
			HttpServletResponse response, ChequeDepositSlipForm form)
			throws Exception, ApplicationException {

		saveCheckDepositSlip(request, response, form, Currency.USD.getId(),
				Company.SLAMCID.getId(), CheckType.DOLLARCHECK);

		return doShowCDSPageSLAMCIDollarCheck(request, response, form);
	}

	public ModelAndView saveSLAMCIDollarUSCheckInManila(
			HttpServletRequest request, HttpServletResponse response,
			ChequeDepositSlipForm form) throws Exception, ApplicationException {

		saveCheckDepositSlip(request, response, form, Currency.USD.getId(),
				Company.SLAMCID.getId(), CheckType.USCD_MANILA);

		return doShowCDSPageSLAMCIDollarUSCheckInManila(request, response, form);
	}

	public ModelAndView saveSLAMCIDollarUSCheckOutsidePH(
			HttpServletRequest request, HttpServletResponse response,
			ChequeDepositSlipForm form) throws Exception, ApplicationException {

		saveCheckDepositSlip(request, response, form, Currency.USD.getId(),
				Company.SLAMCID.getId(), CheckType.USCD_OUTSIDEPH);

		return doShowCDSPageSLAMCIDollarUSCheckOutsidePH(request, response,
				form);
	}

	public ModelAndView saveSLAMCIDollarBankOTC(HttpServletRequest request,
			HttpServletResponse response, ChequeDepositSlipForm form)
			throws Exception, ApplicationException {

		saveCheckDepositSlip(request, response, form, Currency.USD.getId(),
				Company.SLAMCID.getId(), CheckType.BANK_OTC);

		return doShowCDSPageSLAMCIDollarBankOTC(request, response, form);
	}

	@SuppressWarnings({ "rawtypes" })
	public ModelAndView previewSlamciDollarCashDepositSlip(
			HttpServletRequest request, HttpServletResponse response,
			CashDepositSlipForm form) throws ApplicationException {

		Map model = previewCash(request, response, form,
				Company.SLAMCID.getId(), "slamcid.cash.ds.jrxml.path");
		return new ModelAndView(new WMSJasperPdfView(), model);
	}

	@SuppressWarnings({ "rawtypes" })
	public ModelAndView previewSLAMCIDollarChequeDepositSlip(
			HttpServletRequest request, HttpServletResponse response,
			ChequeDepositSlipForm form) throws ApplicationException {

		Map model = previewCheque(request, response, form,
				Company.SLAMCID.getId(), "slamcid.check.ds.jrxml.path");
		return new ModelAndView(new WMSJasperPdfView(), model);
	}

	/*
	 * @SuppressWarnings({ "rawtypes" }) public ModelAndView
	 * previewSLAMCIDollarUSCheckInManila( HttpServletRequest request,
	 * HttpServletResponse response, CashDepositSlipForm form) throws
	 * ApplicationException {
	 * 
	 * Map model = preview(request, response, form, Company.SLAMCID.getId(),
	 * "slamcid.check.ds.jrxml.path"); return new ModelAndView(new
	 * WMSJasperPdfView(), model); }
	 * 
	 * @SuppressWarnings({ "rawtypes" }) public ModelAndView
	 * previewSLAMCIDollarUSCheckOutsidePH( HttpServletRequest request,
	 * HttpServletResponse response, CashDepositSlipForm form) throws
	 * ApplicationException {
	 * 
	 * Map model = preview(request, response, form, Company.SLAMCID.getId(),
	 * "slamcid.check.ds.jrxml.path"); return new ModelAndView(new
	 * WMSJasperPdfView(), model); }
	 * 
	 * @SuppressWarnings({ "rawtypes" }) public ModelAndView
	 * previewSLAMCIDollarBankOTC(HttpServletRequest request,
	 * HttpServletResponse response, CashDepositSlipForm form) throws
	 * ApplicationException {
	 * 
	 * Map model = preview(request, response, form, Company.SLAMCID.getId(),
	 * "slamcid.check.ds.jrxml.path"); return new ModelAndView(new
	 * WMSJasperPdfView(), model); }
	 */
	public final ModelAndView doShowCDSPageSLOCPIPesoCash(
			HttpServletRequest request, HttpServletResponse response,
			CashDepositSlipForm form) throws Exception {

		return doShowCDSPageCash(request, response, form, Company.SLOCPI,
				Currency.PHP, "cdspage.cash.peso", "slocpip.cash.save",
				"slocpip.cash.preview", 19L, "prod.tradvul",
				ProductType.SLOCPI_P_TRADVUL);

	}

	public final ModelAndView doShowCDSPageSLOCPIPesoChequeOnUs(
			HttpServletRequest request, HttpServletResponse response,
			ChequeDepositSlipForm form) throws Exception {

		return doShowCDSPageCheque(request, response, form, Company.SLOCPI,
				Currency.PHP, "cdspage.cou.peso", "slocpip.cou.save",
				"slocpip.cou.preview", 20L, CheckType.ON_US, "prod.tradvul",
				true, ProductType.SLOCPI_P_TRADVUL);
	}

	public final ModelAndView doShowCDSPageSLOCPIPesoChequeLocalClearing(
			HttpServletRequest request, HttpServletResponse response,
			ChequeDepositSlipForm form) throws Exception {

		return doShowCDSPageCheque(request, response, form, Company.SLOCPI,
				Currency.PHP, "cdspage.clc.peso", "slocpip.clc.save",
				"slocpip.clc.preview", 21L, CheckType.LOCAL, "prod.tradvul",
				true, ProductType.SLOCPI_P_TRADVUL);
	}

	public final ModelAndView doShowCDSPageSLOCPIPesoChequeRegionalClearing(
			HttpServletRequest request, HttpServletResponse response,
			ChequeDepositSlipForm form) throws Exception {

		return doShowCDSPageCheque(request, response, form, Company.SLOCPI,
				Currency.PHP, "cdspage.crc.peso", "slocpip.crc.save",
				"slocpip.crc.preview", 22L, CheckType.REGIONAL, "prod.tradvul",
				true, ProductType.SLOCPI_P_TRADVUL);
	}

	public final ModelAndView doShowCDSPageSLOCPIPesoChequeOT(
			HttpServletRequest request, HttpServletResponse response,
			ChequeDepositSlipForm form) throws Exception {

		return doShowCDSPageCheque(request, response, form, Company.SLOCPI,
				Currency.PHP, "cdspage.cot.peso", "slocpip.cot.save",
				"slocpip.cot.preview", 23L, CheckType.OT, "prod.tradvul", true,
				ProductType.SLOCPI_P_TRADVUL);
	}

	public ModelAndView saveSLOCPIPesoCashDepositSlip(
			HttpServletRequest request, HttpServletResponse response,
			CashDepositSlipForm form) throws Exception {

		saveCashDepositSlip(request, response, form, Currency.PHP.getId(),
				Company.SLOCPI.getId());
		return this.doShowCDSPageSLOCPIPesoCash(request, response, form);
	}

	public ModelAndView saveSLOCPIPesoChequeOnUsDepositSlip(
			HttpServletRequest request, HttpServletResponse response,
			ChequeDepositSlipForm form) throws Exception, ApplicationException {

		saveCheckDepositSlip(request, response, form, Currency.PHP.getId(),
				Company.SLOCPI.getId(), CheckType.ON_US);

		return doShowCDSPageSLOCPIPesoChequeOnUs(request, response, form);
	}

	public ModelAndView saveSLOCPIPesoChequeLocalDepositSlip(
			HttpServletRequest request, HttpServletResponse response,
			ChequeDepositSlipForm form) throws Exception, ApplicationException {

		saveCheckDepositSlip(request, response, form, Currency.PHP.getId(),
				Company.SLOCPI.getId(), CheckType.LOCAL);

		return doShowCDSPageSLOCPIPesoChequeLocalClearing(request, response,
				form);
	}

	public ModelAndView saveSLOCPIPesoChequeRegionalDepositSlip(
			HttpServletRequest request, HttpServletResponse response,
			ChequeDepositSlipForm form) throws Exception, ApplicationException {

		saveCheckDepositSlip(request, response, form, Currency.PHP.getId(),
				Company.SLOCPI.getId(), CheckType.REGIONAL);

		return doShowCDSPageSLOCPIPesoChequeRegionalClearing(request, response,
				form);
	}

	public ModelAndView saveSLOCPIPesoChequeOTDepositSlip(
			HttpServletRequest request, HttpServletResponse response,
			ChequeDepositSlipForm form) throws Exception, ApplicationException {

		saveCheckDepositSlip(request, response, form, Currency.PHP.getId(),
				Company.SLOCPI.getId(), CheckType.OT);

		return doShowCDSPageSLOCPIPesoChequeOT(request, response, form);
	}

	@SuppressWarnings({ "rawtypes" })
	public ModelAndView previewSLOCPIPesoCashDepositSlip(
			HttpServletRequest request, HttpServletResponse response,
			CashDepositSlipForm form) throws ApplicationException {

		Map model = previewCash(request, response, form,
				Company.SLOCPI.getId(), "slocpi.peso.cash.ds.jrxml.path");
		return new ModelAndView(new WMSJasperPdfView(), model);
	}

	@SuppressWarnings({ "rawtypes" })
	public ModelAndView previewSLOCPIPesoChequeDepositSlip(
			HttpServletRequest request, HttpServletResponse response,
			ChequeDepositSlipForm form) throws ApplicationException {

		Map model = previewCheque(request, response, form,
				Company.SLOCPI.getId(), "slocpi.peso.check.ds.jrxml.path");
		return new ModelAndView(new WMSJasperPdfView(), model);
	}

	// TODO separation of SLOCPI TO SLOCPI GROUP
	public final ModelAndView doShowCDSPageSLOCPIPesoGrpCash(
			HttpServletRequest request, HttpServletResponse response,
			CashDepositSlipForm form) throws Exception {

		return doShowCDSPageCash(request, response, form, Company.SLOCPI,
				Currency.PHP, "cdspage.cash.peso", "slocpip.grp.cash.save",
				"slocpip.grp.cash.preview", 24L, "prod.group",
				ProductType.SLOCPI_GROUPLIFE);

	}

	public final ModelAndView doShowCDSPageSLOCPIPesoGrpChequeOnUs(
			HttpServletRequest request, HttpServletResponse response,
			ChequeDepositSlipForm form) throws Exception {

		return doShowCDSPageCheque(request, response, form, Company.SLOCPI,
				Currency.PHP, "cdspage.cou.peso", "slocpip.grp.cou.save",
				"slocpip.grp.cou.preview", 25L, CheckType.ON_US, "prod.group",
				false, ProductType.SLOCPI_GROUPLIFE);
	}

	public final ModelAndView doShowCDSPageSLOCPIPesoGrpChequeLocalClearing(
			HttpServletRequest request, HttpServletResponse response,
			ChequeDepositSlipForm form) throws Exception {

		return doShowCDSPageCheque(request, response, form, Company.SLOCPI,
				Currency.PHP, "cdspage.clc.peso", "slocpip.grp.clc.save",
				"slocpip.grp.clc.preview", 26L, CheckType.LOCAL, "prod.group",
				false, ProductType.SLOCPI_GROUPLIFE);
	}

	public final ModelAndView doShowCDSPageSLOCPIPesoGrpChequeRegionalClearing(
			HttpServletRequest request, HttpServletResponse response,
			ChequeDepositSlipForm form) throws Exception {

		return doShowCDSPageCheque(request, response, form, Company.SLOCPI,
				Currency.PHP, "cdspage.crc.peso", "slocpip.grp.crc.save",
				"slocpip.grp.crc.preview", 27L, CheckType.REGIONAL,
				"prod.group", false, ProductType.SLOCPI_GROUPLIFE);
	}

	public final ModelAndView doShowCDSPageSLOCPIGrpPesoChequeOT(
			HttpServletRequest request, HttpServletResponse response,
			ChequeDepositSlipForm form) throws Exception {

		return doShowCDSPageCheque(request, response, form, Company.SLOCPI,
				Currency.PHP, "cdspage.cot.peso", "slocpip.grp.cot.save",
				"slocpip.grp.cot.preview", 28L, CheckType.OT, "prod.group",
				false, ProductType.SLOCPI_GROUPLIFE);
	}

	public ModelAndView saveSLOCPIPesoGrpCashDepositSlip(
			HttpServletRequest request, HttpServletResponse response,
			CashDepositSlipForm form) throws Exception {

		saveCashDepositSlip(request, response, form, Currency.PHP.getId(),
				Company.SLOCPI.getId());
		return this.doShowCDSPageSLOCPIPesoGrpCash(request, response, form);
	}

	public ModelAndView saveSLOCPIPesoGrpChequeOnUsDepositSlip(
			HttpServletRequest request, HttpServletResponse response,
			ChequeDepositSlipForm form) throws Exception, ApplicationException {

		saveCheckDepositSlip(request, response, form, Currency.PHP.getId(),
				Company.SLOCPI.getId(), CheckType.ON_US);

		return doShowCDSPageSLOCPIPesoGrpChequeOnUs(request, response, form);
	}

	public ModelAndView saveSLOCPIPesoGrpChequeLocalDepositSlip(
			HttpServletRequest request, HttpServletResponse response,
			ChequeDepositSlipForm form) throws Exception, ApplicationException {

		saveCheckDepositSlip(request, response, form, Currency.PHP.getId(),
				Company.SLOCPI.getId(), CheckType.LOCAL);

		return doShowCDSPageSLOCPIPesoGrpChequeLocalClearing(request, response,
				form);
	}

	public ModelAndView saveSLOCPIPesoGrpChequeRegionalDepositSlip(
			HttpServletRequest request, HttpServletResponse response,
			ChequeDepositSlipForm form) throws Exception, ApplicationException {

		saveCheckDepositSlip(request, response, form, Currency.PHP.getId(),
				Company.SLOCPI.getId(), CheckType.REGIONAL);

		return doShowCDSPageSLOCPIPesoGrpChequeRegionalClearing(request,
				response, form);
	}

	public ModelAndView saveSLOCPIPesoGrpChequeOTDepositSlip(
			HttpServletRequest request, HttpServletResponse response,
			ChequeDepositSlipForm form) throws Exception, ApplicationException {

		saveCheckDepositSlip(request, response, form, Currency.PHP.getId(),
				Company.SLOCPI.getId(), CheckType.OT);

		return doShowCDSPageSLOCPIGrpPesoChequeOT(request, response, form);
	}

	@SuppressWarnings({ "rawtypes" })
	public ModelAndView previewSLOCPIPesoGrpCashDepositSlip(
			HttpServletRequest request, HttpServletResponse response,
			CashDepositSlipForm form) throws ApplicationException {

		Map model = previewCash(request, response, form,
				Company.SLOCPI.getId(), "peso.cash.ds.jrxml.path");
		return new ModelAndView(new WMSJasperPdfView(), model);
	}

	@SuppressWarnings({ "rawtypes" })
	public ModelAndView previewSLOCPIPesoGrpChequeDepositSlip(
			HttpServletRequest request, HttpServletResponse response,
			ChequeDepositSlipForm form) throws ApplicationException {

		Map model = previewCheque(request, response, form,
				Company.SLOCPI.getId(), "peso.cheque.ds.jrxml.path");
		return new ModelAndView(new WMSJasperPdfView(), model);
	}

	public final ModelAndView doShowCDSPageSLOCPIDollarCash(
			HttpServletRequest request, HttpServletResponse response,
			CashDepositSlipForm form) throws Exception {

		return doShowCDSPageCash(request, response, form, Company.SLOCPI,
				Currency.USD, "cdspage.cash.dollar", "slocpid.cash.save",
				"slocpid.cash.preview", 29L, "prod.tradvul",
				ProductType.SLOCPI_D_TRADVUL);

	}

	public final ModelAndView doShowCDSPageSLOCPIDollarCheck(
			HttpServletRequest request, HttpServletResponse response,
			ChequeDepositSlipForm form) throws Exception {

		return doShowCDSPageCheque(request, response, form, Company.SLOCPI,
				Currency.USD, "cdspage.dc", "slocpid.dc.save",
				"slocpid.dc.preview", 30L, CheckType.DOLLARCHECK,
				"prod.tradvul", true, ProductType.SLOCPI_D_TRADVUL);
	}

	public final ModelAndView doShowCDSPageSLOCPIDollarUSCheckInManila(
			HttpServletRequest request, HttpServletResponse response,
			ChequeDepositSlipForm form) throws Exception {

		return doShowCDSPageCheque(request, response, form, Company.SLOCPI,
				Currency.USD, "cdspage.ucim", "slocpid.ucim.save",
				"slocpid.ucim.preview", 31L, CheckType.USCD_MANILA,
				"prod.tradvul", true, ProductType.SLOCPI_D_TRADVUL);
	}

	public final ModelAndView doShowCDSPageSLOCPIDollarUSCheckOutsidePH(
			HttpServletRequest request, HttpServletResponse response,
			ChequeDepositSlipForm form) throws Exception {

		return doShowCDSPageCheque(request, response, form, Company.SLOCPI,
				Currency.USD, "cdspage.ucop", "slocpid.ucop.save",
				"slocpid.ucop.preview", 32L, CheckType.USCD_OUTSIDEPH,
				"prod.tradvul", true, ProductType.SLOCPI_D_TRADVUL);
	}

	public final ModelAndView doShowCDSPageSLOCPIDollarBankOTC(
			HttpServletRequest request, HttpServletResponse response,
			ChequeDepositSlipForm form) throws Exception {

		return doShowCDSPageCheque(request, response, form, Company.SLOCPI,
				Currency.USD, "cdspage.otc", "slocpid.otc.save",
				"slocpid.otc.preview", 33L, CheckType.BANK_OTC, "prod.tradvul",
				true, ProductType.SLOCPI_D_TRADVUL);
	}

	public ModelAndView saveSLOCPIDollarCashDepositSlip(
			HttpServletRequest request, HttpServletResponse response,
			CashDepositSlipForm form) throws Exception {

		saveCashDepositSlip(request, response, form, Currency.USD.getId(),
				Company.SLOCPI.getId());
		return this.doShowCDSPageSLOCPIDollarCash(request, response, form);
	}

	public ModelAndView saveSLOCPIDollarCheck(HttpServletRequest request,
			HttpServletResponse response, ChequeDepositSlipForm form)
			throws Exception, ApplicationException {

		saveCheckDepositSlip(request, response, form, Currency.USD.getId(),
				Company.SLOCPI.getId(), CheckType.DOLLARCHECK);

		return doShowCDSPageSLOCPIDollarCheck(request, response, form);
	}

	public ModelAndView saveSLOCPIDollarUSCheckInManila(
			HttpServletRequest request, HttpServletResponse response,
			ChequeDepositSlipForm form) throws Exception, ApplicationException {

		saveCheckDepositSlip(request, response, form, Currency.USD.getId(),
				Company.SLOCPI.getId(), CheckType.USCD_MANILA);

		return doShowCDSPageSLOCPIDollarUSCheckInManila(request, response, form);
	}

	public ModelAndView saveSLOCPIDollarUSCheckOutsidePH(
			HttpServletRequest request, HttpServletResponse response,
			ChequeDepositSlipForm form) throws Exception, ApplicationException {

		saveCheckDepositSlip(request, response, form, Currency.USD.getId(),
				Company.SLOCPI.getId(), CheckType.USCD_OUTSIDEPH);

		return doShowCDSPageSLOCPIDollarUSCheckOutsidePH(request, response,
				form);
	}

	public ModelAndView saveSLOCPIDollarBankOTC(HttpServletRequest request,
			HttpServletResponse response, ChequeDepositSlipForm form)
			throws Exception, ApplicationException {

		saveCheckDepositSlip(request, response, form, Currency.USD.getId(),
				Company.SLOCPI.getId(), CheckType.BANK_OTC);

		return doShowCDSPageSLOCPIDollarBankOTC(request, response, form);
	}

	@SuppressWarnings({ "rawtypes" })
	public ModelAndView previewSLOCPIDollarCashDepositSlip(
			HttpServletRequest request, HttpServletResponse response,
			CashDepositSlipForm form) throws ApplicationException {

		Map model = previewCash(request, response, form,
				Company.SLOCPI.getId(), "dollar.cash.ds.jrxml.path");
		return new ModelAndView(new WMSJasperPdfView(), model);
	}

	@SuppressWarnings({ "rawtypes" })
	public ModelAndView previewSLOCPIDollarChequeDepositSlip(
			HttpServletRequest request, HttpServletResponse response,
			ChequeDepositSlipForm form) throws ApplicationException {

		Map model = previewCheque(request, response, form,
				Company.SLOCPI.getId(), "dollar.cheque.ds.jrxml.path");
		return new ModelAndView(new WMSJasperPdfView(), model);
	}

	public final ModelAndView doShowCDSPageSLOCPIDollarGrpCash(
			HttpServletRequest request, HttpServletResponse response,
			CashDepositSlipForm form) throws Exception {

		return doShowCDSPageCash(request, response, form, Company.SLOCPI,
				Currency.USD, "cdspage.cash.dollar", "slocpid.grp.cash.save",
				"slocpid.grp.cash.preview", 34L, "prod.group",
				ProductType.SLOCPI_GROUPLIFE);

	}

	public final ModelAndView doShowCDSPageSLOCPIGrpDollarCheck(
			HttpServletRequest request, HttpServletResponse response,
			ChequeDepositSlipForm form) throws Exception {

		return doShowCDSPageCheque(request, response, form, Company.SLOCPI,
				Currency.USD, "cdspage.dc", "slocpid.grp.dc.save",
				"slocpid.grp.dc.preview", 35L, CheckType.DOLLARCHECK,
				"prod.group", false, ProductType.SLOCPI_GROUPLIFE);
	}

	public final ModelAndView doShowCDSPageSLOCPIGrpDollarUSCheckInManila(
			HttpServletRequest request, HttpServletResponse response,
			ChequeDepositSlipForm form) throws Exception {

		return doShowCDSPageCheque(request, response, form, Company.SLOCPI,
				Currency.USD, "cdspage.ucim", "slocpid.grp.ucim.save",
				"slocpid.grp.ucim.preview", 36L, CheckType.USCD_MANILA,
				"prod.group", false, ProductType.SLOCPI_GROUPLIFE);
	}

	public final ModelAndView doShowCDSPageSLOCPIGrpDollarUSCheckOutsidePH(
			HttpServletRequest request, HttpServletResponse response,
			ChequeDepositSlipForm form) throws Exception {

		return doShowCDSPageCheque(request, response, form, Company.SLOCPI,
				Currency.USD, "cdspage.ucop", "slocpid.grp.ucop.save",
				"slocpid.grp.ucop.preview", 37L, CheckType.USCD_OUTSIDEPH,
				"prod.group", false, ProductType.SLOCPI_GROUPLIFE);
	}

	public final ModelAndView doShowCDSPageSLOCPIGrpDollarBankOTC(
			HttpServletRequest request, HttpServletResponse response,
			ChequeDepositSlipForm form) throws Exception {

		return doShowCDSPageCheque(request, response, form, Company.SLOCPI,
				Currency.USD, "cdspage.otc", "slocpid.grp.otc.save",
				"slocpid.grp.otc.preview", 38L, CheckType.BANK_OTC,
				"prod.group", false, ProductType.SLOCPI_GROUPLIFE);
	}

	public ModelAndView saveSLOCPIDollarGrpCashDepositSlip(
			HttpServletRequest request, HttpServletResponse response,
			CashDepositSlipForm form) throws Exception {

		saveCashDepositSlip(request, response, form, Currency.USD.getId(),
				Company.SLOCPI.getId());
		return this.doShowCDSPageSLOCPIDollarGrpCash(request, response, form);
	}

	public ModelAndView saveSLOCPIGrpDollarCheck(HttpServletRequest request,
			HttpServletResponse response, ChequeDepositSlipForm form)
			throws Exception, ApplicationException {

		saveCheckDepositSlip(request, response, form, Currency.USD.getId(),
				Company.SLOCPI.getId(), CheckType.DOLLARCHECK);

		return doShowCDSPageSLOCPIGrpDollarCheck(request, response, form);
	}

	public ModelAndView saveSLOCPIDollarGrpUSCheckInManila(
			HttpServletRequest request, HttpServletResponse response,
			ChequeDepositSlipForm form) throws Exception, ApplicationException {

		saveCheckDepositSlip(request, response, form, Currency.USD.getId(),
				Company.SLOCPI.getId(), CheckType.USCD_MANILA);

		return doShowCDSPageSLOCPIGrpDollarUSCheckInManila(request, response,
				form);
	}

	public ModelAndView saveSLOCPIDollarGrpUSCheckOutsidePH(
			HttpServletRequest request, HttpServletResponse response,
			ChequeDepositSlipForm form) throws Exception, ApplicationException {

		saveCheckDepositSlip(request, response, form, Currency.USD.getId(),
				Company.SLOCPI.getId(), CheckType.USCD_OUTSIDEPH);

		return doShowCDSPageSLOCPIGrpDollarUSCheckOutsidePH(request, response,
				form);
	}

	public ModelAndView saveSLOCPIGrpDollarBankOTC(HttpServletRequest request,
			HttpServletResponse response, ChequeDepositSlipForm form)
			throws Exception, ApplicationException {

		saveCheckDepositSlip(request, response, form, Currency.USD.getId(),
				Company.SLOCPI.getId(), CheckType.BANK_OTC);

		return doShowCDSPageSLOCPIGrpDollarBankOTC(request, response, form);
	}

	@SuppressWarnings({ "rawtypes" })
	public ModelAndView previewSLOCPIDollarGrpCashDepositSlip(
			HttpServletRequest request, HttpServletResponse response,
			CashDepositSlipForm form) throws ApplicationException {

		Map model = previewCash(request, response, form,
				Company.SLOCPI.getId(), "dollar.cash.ds.jrxml.path");
		return new ModelAndView(new WMSJasperPdfView(), model);
	}

	@SuppressWarnings({ "rawtypes" })
	public ModelAndView previewSLOCPIDollarGrpChequeDepositSlip(
			HttpServletRequest request, HttpServletResponse response,
			ChequeDepositSlipForm form) throws ApplicationException {

		Map model = previewCheque(request, response, form,
				Company.SLOCPI.getId(), "dollar.cheque.ds.jrxml.path");
		return new ModelAndView(new WMSJasperPdfView(), model);
	}

	public final ModelAndView doShowCDSPageSLGFIPesoCash(
			HttpServletRequest request, HttpServletResponse response,
			CashDepositSlipForm form) throws Exception {

		return doShowCDSPageCash(request, response, form, Company.SLGFI,
				Currency.PHP, "cdspage.cash.peso", "slgfip.cash.save",
				"slgfip.cash.preview", 39L, "prod.tradvul",
				ProductType.GREPA_P_TRADVUL);

	}

	public final ModelAndView doShowCDSPageSLGFIPesoChequeOnUs(
			HttpServletRequest request, HttpServletResponse response,
			ChequeDepositSlipForm form) throws Exception {

		return doShowCDSPageCheque(request, response, form, Company.SLGFI,
				Currency.PHP, "cdspage.cou.peso", "slgfip.cou.save",
				"slgfip.cou.preview", 40L, CheckType.ON_US, "prod.tradvul",
				false, ProductType.GREPA_P_TRADVUL);
	}

	public final ModelAndView doShowCDSPageSLGFIPesoChequeLocalClearing(
			HttpServletRequest request, HttpServletResponse response,
			ChequeDepositSlipForm form) throws Exception {

		return doShowCDSPageCheque(request, response, form, Company.SLGFI,
				Currency.PHP, "cdspage.clc.peso", "slgfip.clc.save",
				"slgfip.clc.preview", 41L, CheckType.LOCAL, "prod.tradvul",
				false, ProductType.GREPA_P_TRADVUL);
	}

	public final ModelAndView doShowCDSPageSLGFIPesoChequeRegionalClearing(
			HttpServletRequest request, HttpServletResponse response,
			ChequeDepositSlipForm form) throws Exception {

		return doShowCDSPageCheque(request, response, form, Company.SLGFI,
				Currency.PHP, "cdspage.crc.peso", "slgfip.crc.save",
				"slgfip.crc.preview", 42L, CheckType.REGIONAL, "prod.tradvul",
				false, ProductType.GREPA_P_TRADVUL);
	}

	public final ModelAndView doShowCDSPageSLGFIPesoChequeOT(
			HttpServletRequest request, HttpServletResponse response,
			ChequeDepositSlipForm form) throws Exception {

		return doShowCDSPageCheque(request, response, form, Company.SLGFI,
				Currency.PHP, "cdspage.cot.peso", "slgfip.cot.save",
				"slgfip.cot.preview", 43L, CheckType.OT, "prod.tradvul", false,
				ProductType.GREPA_P_TRADVUL);
	}

	public ModelAndView saveSLGFIPesoCashDepositSlip(
			HttpServletRequest request, HttpServletResponse response,
			CashDepositSlipForm form) throws Exception {

		saveCashDepositSlip(request, response, form, Currency.PHP.getId(),
				Company.SLGFI.getId());
		return this.doShowCDSPageSLGFIPesoCash(request, response, form);
	}

	public ModelAndView saveSLGFIPesoChequeOnUsDepositSlip(
			HttpServletRequest request, HttpServletResponse response,
			ChequeDepositSlipForm form) throws Exception, ApplicationException {

		saveCheckDepositSlip(request, response, form, Currency.PHP.getId(),
				Company.SLGFI.getId(), CheckType.ON_US);

		return doShowCDSPageSLGFIPesoChequeOnUs(request, response, form);
	}

	public ModelAndView saveSLGFIPesoChequeLocalDepositSlip(
			HttpServletRequest request, HttpServletResponse response,
			ChequeDepositSlipForm form) throws Exception, ApplicationException {

		saveCheckDepositSlip(request, response, form, Currency.PHP.getId(),
				Company.SLGFI.getId(), CheckType.LOCAL);

		return doShowCDSPageSLGFIPesoChequeLocalClearing(request, response,
				form);
	}

	public ModelAndView saveSLGFIPesoChequeRegionalDepositSlip(
			HttpServletRequest request, HttpServletResponse response,
			ChequeDepositSlipForm form) throws Exception, ApplicationException {

		saveCheckDepositSlip(request, response, form, Currency.PHP.getId(),
				Company.SLGFI.getId(), CheckType.REGIONAL);

		return doShowCDSPageSLGFIPesoChequeRegionalClearing(request, response,
				form);
	}

	public ModelAndView saveSLGFIPesoChequeOTDepositSlip(
			HttpServletRequest request, HttpServletResponse response,
			ChequeDepositSlipForm form) throws Exception, ApplicationException {

		saveCheckDepositSlip(request, response, form, Currency.PHP.getId(),
				Company.SLGFI.getId(), CheckType.OT);

		return doShowCDSPageSLGFIPesoChequeOT(request, response, form);
	}

	@SuppressWarnings({ "rawtypes" })
	public ModelAndView previewSLGFIPesoCashDepositSlip(
			HttpServletRequest request, HttpServletResponse response,
			CashDepositSlipForm form) throws ApplicationException {

		Map model = previewCash(request, response, form, Company.SLGFI.getId(),
				"peso.cash.ds.jrxml.path");
		return new ModelAndView(new WMSJasperPdfView(), model);
	}

	@SuppressWarnings({ "rawtypes" })
	public ModelAndView previewSLGFIPesoChequeDepositSlip(
			HttpServletRequest request, HttpServletResponse response,
			ChequeDepositSlipForm form) throws ApplicationException {

		Map model = previewCheque(request, response, form,
				Company.SLGFI.getId(), "peso.cheque.ds.jrxml.path");
		return new ModelAndView(new WMSJasperPdfView(), model);
	}

	public final ModelAndView doShowCDSPageSLGFIPesoUncCash(
			HttpServletRequest request, HttpServletResponse response,
			CashDepositSlipForm form) throws Exception {

		return doShowCDSPageCash(request, response, form, Company.SLGFI,
				Currency.PHP, "cdspage.cash.peso", "slgfip.unc.cash.save",
				"slgfip.unc.cash.preview", 44L, "prod.unc",
				ProductType.GREPA_P_UNCONVERTED);

	}

	public final ModelAndView doShowCDSPageSLGFIPesoUncChequeOnUs(
			HttpServletRequest request, HttpServletResponse response,
			ChequeDepositSlipForm form) throws Exception {

		return doShowCDSPageCheque(request, response, form, Company.SLGFI,
				Currency.PHP, "cdspage.cou.peso", "slgfip.unc.cou.save",
				"slgfip.unc.cou.preview", 45L, CheckType.ON_US, "prod.unc",
				false, ProductType.GREPA_P_UNCONVERTED);
	}

	public final ModelAndView doShowCDSPageSLGFIPesoUncChequeLocalClearing(
			HttpServletRequest request, HttpServletResponse response,
			ChequeDepositSlipForm form) throws Exception {

		return doShowCDSPageCheque(request, response, form, Company.SLGFI,
				Currency.PHP, "cdspage.clc.peso", "slgfip.unc.clc.save",
				"slgfip.unc.clc.preview", 46L, CheckType.LOCAL, "prod.unc",
				false, ProductType.GREPA_P_UNCONVERTED);
	}

	public final ModelAndView doShowCDSPageSLGFIPesoUncChequeRegionalClearing(
			HttpServletRequest request, HttpServletResponse response,
			ChequeDepositSlipForm form) throws Exception {

		return doShowCDSPageCheque(request, response, form, Company.SLGFI,
				Currency.PHP, "cdspage.crc.peso", "slgfip.unc.crc.save",
				"slgfip.unc.crc.preview", 47L, CheckType.REGIONAL, "prod.unc",
				false, ProductType.GREPA_P_UNCONVERTED);
	}

	public final ModelAndView doShowCDSPageSLGFIPesoUncChequeOT(
			HttpServletRequest request, HttpServletResponse response,
			ChequeDepositSlipForm form) throws Exception {

		return doShowCDSPageCheque(request, response, form, Company.SLGFI,
				Currency.PHP, "cdspage.cot.peso", "slgfip.unc.cot.save",
				"slgfip.unc.cot.preview", 48L, CheckType.OT, "prod.unc", false,
				ProductType.GREPA_P_UNCONVERTED);
	}

	public ModelAndView saveSLGFIPesoUncCashDepositSlip(
			HttpServletRequest request, HttpServletResponse response,
			CashDepositSlipForm form) throws Exception {

		saveCashDepositSlip(request, response, form, Currency.PHP.getId(),
				Company.SLGFI.getId());
		return this.doShowCDSPageSLGFIPesoUncCash(request, response, form);
	}

	public ModelAndView saveSLGFIPesoUncChequeOnUsDepositSlip(
			HttpServletRequest request, HttpServletResponse response,
			ChequeDepositSlipForm form) throws Exception, ApplicationException {

		saveCheckDepositSlip(request, response, form, Currency.PHP.getId(),
				Company.SLGFI.getId(), CheckType.ON_US);

		return doShowCDSPageSLGFIPesoUncChequeOnUs(request, response, form);
	}

	public ModelAndView saveSLGFIPesoUncChequeLocalDepositSlip(
			HttpServletRequest request, HttpServletResponse response,
			ChequeDepositSlipForm form) throws Exception, ApplicationException {

		saveCheckDepositSlip(request, response, form, Currency.PHP.getId(),
				Company.SLGFI.getId(), CheckType.LOCAL);

		return doShowCDSPageSLGFIPesoUncChequeLocalClearing(request, response,
				form);
	}

	public ModelAndView saveSLGFIPesoUncChequeRegionalDepositSlip(
			HttpServletRequest request, HttpServletResponse response,
			ChequeDepositSlipForm form) throws Exception, ApplicationException {

		saveCheckDepositSlip(request, response, form, Currency.PHP.getId(),
				Company.SLGFI.getId(), CheckType.REGIONAL);

		return doShowCDSPageSLGFIPesoUncChequeRegionalClearing(request,
				response, form);
	}

	public ModelAndView saveSLGFIPesoUncChequeOTDepositSlip(
			HttpServletRequest request, HttpServletResponse response,
			ChequeDepositSlipForm form) throws Exception, ApplicationException {

		saveCheckDepositSlip(request, response, form, Currency.PHP.getId(),
				Company.SLGFI.getId(), CheckType.OT);

		return doShowCDSPageSLGFIPesoUncChequeOT(request, response, form);
	}

	@SuppressWarnings({ "rawtypes" })
	public ModelAndView previewSLGFIPesoUncCashDepositSlip(
			HttpServletRequest request, HttpServletResponse response,
			CashDepositSlipForm form) throws ApplicationException {

		Map model = previewCash(request, response, form, Company.SLGFI.getId(),
				"peso.cash.ds.jrxml.path");
		return new ModelAndView(new WMSJasperPdfView(), model);
	}

	@SuppressWarnings({ "rawtypes" })
	public ModelAndView previewSLGFIPesoUncChequeDepositSlip(
			HttpServletRequest request, HttpServletResponse response,
			ChequeDepositSlipForm form) throws ApplicationException {

		Map model = previewCheque(request, response, form,
				Company.SLGFI.getId(), "peso.cheque.ds.jrxml.path");
		return new ModelAndView(new WMSJasperPdfView(), model);
	}

	public final ModelAndView doShowCDSPageSLGFIDollarCash(
			HttpServletRequest request, HttpServletResponse response,
			CashDepositSlipForm form) throws Exception {

		return doShowCDSPageCash(request, response, form, Company.SLGFI,
				Currency.USD, "cdspage.cash.dollar", "slgfid.cash.save",
				"slgfid.cash.preview", 49L, "prod.tradvul",
				ProductType.GREPA_D_TRADVUL);

	}

	public final ModelAndView doShowCDSPageSLGFIDollarCheck(
			HttpServletRequest request, HttpServletResponse response,
			ChequeDepositSlipForm form) throws Exception {

		return doShowCDSPageCheque(request, response, form, Company.SLGFI,
				Currency.USD, "cdspage.dc", "slgfid.dc.save",
				"slgfid.dc.preview", 50L, CheckType.DOLLARCHECK,
				"prod.tradvul", false, ProductType.GREPA_D_TRADVUL);
	}

	public final ModelAndView doShowCDSPageSLGFIDollarUSCheckInManila(
			HttpServletRequest request, HttpServletResponse response,
			ChequeDepositSlipForm form) throws Exception {

		return doShowCDSPageCheque(request, response, form, Company.SLGFI,
				Currency.USD, "cdspage.ucim", "slgfid.ucim.save",
				"slgfid.ucim.preview", 51L, CheckType.USCD_MANILA,
				"prod.tradvul", false, ProductType.GREPA_D_TRADVUL);
	}

	public final ModelAndView doShowCDSPageSLGFIDollarUSCheckOutsidePH(
			HttpServletRequest request, HttpServletResponse response,
			ChequeDepositSlipForm form) throws Exception {

		return doShowCDSPageCheque(request, response, form, Company.SLGFI,
				Currency.USD, "cdspage.ucop", "slgfid.ucop.save",
				"slgfid.ucop.preview", 52L, CheckType.USCD_OUTSIDEPH,
				"prod.tradvul", false, ProductType.GREPA_D_TRADVUL);
	}

	public final ModelAndView doShowCDSPageSLGFIDollarBankOTC(
			HttpServletRequest request, HttpServletResponse response,
			ChequeDepositSlipForm form) throws Exception {

		return doShowCDSPageCheque(request, response, form, Company.SLGFI,
				Currency.USD, "cdspage.otc", "slgfid.otc.save",
				"slgfid.otc.preview", 53L, CheckType.BANK_OTC, "prod.tradvul",
				false, ProductType.GREPA_D_TRADVUL);
	}

	public ModelAndView saveSLGFIDollarCashDepositSlip(
			HttpServletRequest request, HttpServletResponse response,
			CashDepositSlipForm form) throws Exception {

		saveCashDepositSlip(request, response, form, Currency.USD.getId(),
				Company.SLGFI.getId());
		return this.doShowCDSPageSLGFIDollarCash(request, response, form);
	}

	public ModelAndView saveSLGFIDollarCheck(HttpServletRequest request,
			HttpServletResponse response, ChequeDepositSlipForm form)
			throws Exception, ApplicationException {

		saveCheckDepositSlip(request, response, form, Currency.USD.getId(),
				Company.SLGFI.getId(), CheckType.DOLLARCHECK);

		return doShowCDSPageSLGFIDollarCheck(request, response, form);
	}

	public ModelAndView saveSLGFIDollarUSCheckInManila(
			HttpServletRequest request, HttpServletResponse response,
			ChequeDepositSlipForm form) throws Exception, ApplicationException {

		saveCheckDepositSlip(request, response, form, Currency.USD.getId(),
				Company.SLGFI.getId(), CheckType.USCD_MANILA);

		return doShowCDSPageSLGFIDollarUSCheckInManila(request, response, form);
	}

	public ModelAndView saveSLGFIDollarUSCheckOutsidePH(
			HttpServletRequest request, HttpServletResponse response,
			ChequeDepositSlipForm form) throws Exception, ApplicationException {

		saveCheckDepositSlip(request, response, form, Currency.USD.getId(),
				Company.SLGFI.getId(), CheckType.USCD_OUTSIDEPH);

		return doShowCDSPageSLGFIDollarUSCheckOutsidePH(request, response, form);
	}

	public ModelAndView saveSLGFIDollarBankOTC(HttpServletRequest request,
			HttpServletResponse response, ChequeDepositSlipForm form)
			throws Exception, ApplicationException {

		saveCheckDepositSlip(request, response, form, Currency.USD.getId(),
				Company.SLGFI.getId(), CheckType.BANK_OTC);

		return doShowCDSPageSLGFIDollarBankOTC(request, response, form);
	}

	@SuppressWarnings({ "rawtypes" })
	public ModelAndView previewSLGFIDollarCashDepositSlip(
			HttpServletRequest request, HttpServletResponse response,
			CashDepositSlipForm form) throws ApplicationException {

		Map model = previewCash(request, response, form, Company.SLGFI.getId(),
				"dollar.cash.ds.jrxml.path");
		return new ModelAndView(new WMSJasperPdfView(), model);
	}

	@SuppressWarnings({ "rawtypes" })
	public ModelAndView previewSLGFIDollarChequeDepositSlip(
			HttpServletRequest request, HttpServletResponse response,
			ChequeDepositSlipForm form) throws ApplicationException {

		Map model = previewCheque(request, response, form,
				Company.SLGFI.getId(), "dollar.cheque.ds.jrxml.path");
		return new ModelAndView(new WMSJasperPdfView(), model);
	}

	public final ModelAndView doShowCDSPageSLGFIDollarUncCash(
			HttpServletRequest request, HttpServletResponse response,
			CashDepositSlipForm form) throws Exception {

		return doShowCDSPageCash(request, response, form, Company.SLGFI,
				Currency.USD, "cdspage.cash.dollar", "slgfid.unc.cash.save",
				"slgfid.unc.cash.preview", 54L, "prod.unc",
				ProductType.GREPA_D_UNCONVERTED);

	}

	public final ModelAndView doShowCDSPageSLGFIUncDollarCheck(
			HttpServletRequest request, HttpServletResponse response,
			ChequeDepositSlipForm form) throws Exception {

		return doShowCDSPageCheque(request, response, form, Company.SLGFI,
				Currency.USD, "cdspage.dc", "slgfid.unc.dc.save",
				"slgfid.unc.dc.preview", 55L, CheckType.DOLLARCHECK,
				"prod.unc", false, ProductType.GREPA_D_UNCONVERTED);
	}

	public final ModelAndView doShowCDSPageSLGFIUncDollarUSCheckInManila(
			HttpServletRequest request, HttpServletResponse response,
			ChequeDepositSlipForm form) throws Exception {

		return doShowCDSPageCheque(request, response, form, Company.SLGFI,
				Currency.USD, "cdspage.ucim", "slgfid.unc.ucim.save",
				"slgfid.unc.ucim.preview", 56L, CheckType.USCD_MANILA,
				"prod.unc", false, ProductType.GREPA_D_UNCONVERTED);
	}

	public final ModelAndView doShowCDSPageSLGFIUncDollarUSCheckOutsidePH(
			HttpServletRequest request, HttpServletResponse response,
			ChequeDepositSlipForm form) throws Exception {

		return doShowCDSPageCheque(request, response, form, Company.SLGFI,
				Currency.USD, "cdspage.ucop", "slgfid.unc.ucop.save",
				"slgfid.unc.ucop.preview", 57L, CheckType.USCD_OUTSIDEPH,
				"prod.unc", false, ProductType.GREPA_D_UNCONVERTED);
	}

	public final ModelAndView doShowCDSPageSLGFIUncDollarBankOTC(
			HttpServletRequest request, HttpServletResponse response,
			ChequeDepositSlipForm form) throws Exception {

		return doShowCDSPageCheque(request, response, form, Company.SLGFI,
				Currency.USD, "cdspage.otc", "slgfid.unc.otc.save",
				"slgfid.unc.otc.preview", 58L, CheckType.BANK_OTC, "prod.unc",
				false, ProductType.GREPA_D_UNCONVERTED);
	}

	public ModelAndView saveSLGFIUncDollarCashDepositSlip(
			HttpServletRequest request, HttpServletResponse response,
			CashDepositSlipForm form) throws Exception {

		saveCashDepositSlip(request, response, form, Currency.USD.getId(),
				Company.SLGFI.getId());
		return this.doShowCDSPageSLGFIDollarUncCash(request, response, form);
	}

	public ModelAndView saveSLGFIUncDollarCheck(HttpServletRequest request,
			HttpServletResponse response, ChequeDepositSlipForm form)
			throws Exception, ApplicationException {

		saveCheckDepositSlip(request, response, form, Currency.USD.getId(),
				Company.SLGFI.getId(), CheckType.DOLLARCHECK);

		return doShowCDSPageSLGFIUncDollarCheck(request, response, form);
	}

	public ModelAndView saveSLGFIUncDollarUSCheckInManila(
			HttpServletRequest request, HttpServletResponse response,
			ChequeDepositSlipForm form) throws Exception, ApplicationException {

		saveCheckDepositSlip(request, response, form, Currency.USD.getId(),
				Company.SLGFI.getId(), CheckType.USCD_MANILA);

		return doShowCDSPageSLGFIUncDollarUSCheckInManila(request, response,
				form);
	}

	public ModelAndView saveSLGFIUncDollarUSCheckOutsidePH(
			HttpServletRequest request, HttpServletResponse response,
			ChequeDepositSlipForm form) throws Exception, ApplicationException {

		saveCheckDepositSlip(request, response, form, Currency.USD.getId(),
				Company.SLGFI.getId(), CheckType.USCD_OUTSIDEPH);

		return doShowCDSPageSLGFIUncDollarUSCheckOutsidePH(request, response,
				form);
	}

	public ModelAndView saveSLGFIUncDollarBankOTC(HttpServletRequest request,
			HttpServletResponse response, ChequeDepositSlipForm form)
			throws Exception, ApplicationException {

		saveCheckDepositSlip(request, response, form, Currency.USD.getId(),
				Company.SLGFI.getId(), CheckType.BANK_OTC);

		return doShowCDSPageSLGFIUncDollarBankOTC(request, response, form);
	}

	@SuppressWarnings({ "rawtypes" })
	public ModelAndView previewSLGFIUncDollarCashDepositSlip(
			HttpServletRequest request, HttpServletResponse response,
			CashDepositSlipForm form) throws ApplicationException {

		Map model = previewCash(request, response, form, Company.SLGFI.getId(),
				"dollar.cash.ds.jrxml.path");
		return new ModelAndView(new WMSJasperPdfView(), model);
	}

	@SuppressWarnings({ "rawtypes" })
	public ModelAndView previewSLGFIUncDollarChequeDepositSlip(
			HttpServletRequest request, HttpServletResponse response,
			ChequeDepositSlipForm form) throws ApplicationException {

		Map model = previewCheque(request, response, form,
				Company.SLGFI.getId(), "dollar.cheque.ds.jrxml.path");
		return new ModelAndView(new WMSJasperPdfView(), model);
	}

	public final ModelAndView doShowCDSPageSLGFIGrpPesoCash(
			HttpServletRequest request, HttpServletResponse response,
			CashDepositSlipForm form) throws Exception {

		return doShowCDSPageCash(request, response, form, Company.SLGFI,
				Currency.PHP, "cdspage.cash.peso", "slgfip.grp.cash.save",
				"slgfip.grp.cash.preview", 80L, "prod.group",
				ProductType.GREPA_GP_GROUPLIFE);

	}

	public final ModelAndView doShowCDSPageSLGFIGrpPesoChequeOnUs(
			HttpServletRequest request, HttpServletResponse response,
			ChequeDepositSlipForm form) throws Exception {

		return doShowCDSPageCheque(request, response, form, Company.SLGFI,
				Currency.PHP, "cdspage.cou.peso", "slgfip.grp.cou.save",
				"slgfip.grp.cou.preview", 81L, CheckType.ON_US, "prod.group",
				false, ProductType.GREPA_GP_GROUPLIFE);
	}

	public final ModelAndView doShowCDSPageSLGFIGrpPesoChequeLocalClearing(
			HttpServletRequest request, HttpServletResponse response,
			ChequeDepositSlipForm form) throws Exception {

		return doShowCDSPageCheque(request, response, form, Company.SLGFI,
				Currency.PHP, "cdspage.clc.peso", "slgfip.grp.clc.save",
				"slgfip.grp.clc.preview", 82L, CheckType.LOCAL, "prod.group",
				false, ProductType.GREPA_GP_GROUPLIFE);
	}

	public final ModelAndView doShowCDSPageSLGFIGrpPesoChequeRegionalClearing(
			HttpServletRequest request, HttpServletResponse response,
			ChequeDepositSlipForm form) throws Exception {

		return doShowCDSPageCheque(request, response, form, Company.SLGFI,
				Currency.PHP, "cdspage.crc.peso", "slgfip.grp.crc.save",
				"slgfip.grp.crc.preview", 83L, CheckType.REGIONAL,
				"prod.group", false, ProductType.GREPA_GP_GROUPLIFE);
	}

	public final ModelAndView doShowCDSPageSLGFIGrpPesoChequeOT(
			HttpServletRequest request, HttpServletResponse response,
			ChequeDepositSlipForm form) throws Exception {

		return doShowCDSPageCheque(request, response, form, Company.SLGFI,
				Currency.PHP, "cdspage.cot.peso", "slgfip.grp.cot.save",
				"slgfip.grp.cot.preview", 84L, CheckType.OT, "prod.group",
				false, ProductType.GREPA_GP_GROUPLIFE);
	}

	public ModelAndView saveSLGFIGrpPesoCash(HttpServletRequest request,
			HttpServletResponse response, CashDepositSlipForm form)
			throws Exception {

		saveCashDepositSlip(request, response, form, Currency.PHP.getId(),
				Company.SLGFI.getId());
		return this.doShowCDSPageSLGFIGrpPesoCash(request, response, form);
	}

	public ModelAndView saveSLGFIGrpPesoChequeOnUs(HttpServletRequest request,
			HttpServletResponse response, ChequeDepositSlipForm form)
			throws Exception, ApplicationException {

		saveCheckDepositSlip(request, response, form, Currency.PHP.getId(),
				Company.SLGFI.getId(), CheckType.ON_US);

		return doShowCDSPageSLGFIGrpPesoChequeOnUs(request, response, form);
	}

	public ModelAndView saveSLGFIGrpPesoChequeLocalClearing(
			HttpServletRequest request, HttpServletResponse response,
			ChequeDepositSlipForm form) throws Exception, ApplicationException {

		saveCheckDepositSlip(request, response, form, Currency.PHP.getId(),
				Company.SLGFI.getId(), CheckType.LOCAL);

		return doShowCDSPageSLGFIGrpPesoChequeLocalClearing(request, response,
				form);
	}

	public ModelAndView saveSLGFIGrpPesoChequeRegionalClearing(
			HttpServletRequest request, HttpServletResponse response,
			ChequeDepositSlipForm form) throws Exception, ApplicationException {

		saveCheckDepositSlip(request, response, form, Currency.PHP.getId(),
				Company.SLGFI.getId(), CheckType.REGIONAL);

		return doShowCDSPageSLGFIGrpPesoChequeRegionalClearing(request,
				response, form);
	}

	public ModelAndView saveSLGFIGrpPesoChequeOT(HttpServletRequest request,
			HttpServletResponse response, ChequeDepositSlipForm form)
			throws Exception, ApplicationException {

		saveCheckDepositSlip(request, response, form, Currency.PHP.getId(),
				Company.SLGFI.getId(), CheckType.OT);

		return doShowCDSPageSLGFIGrpPesoChequeOT(request, response, form);
	}

	@SuppressWarnings({ "rawtypes" })
	public ModelAndView previewSLGFIGrpPesoCashDepositSlip(
			HttpServletRequest request, HttpServletResponse response,
			CashDepositSlipForm form) throws ApplicationException {

		Map model = previewCash(request, response, form, Company.SLGFI.getId(),
				"peso.cash.ds.jrxml.path");
		return new ModelAndView(new WMSJasperPdfView(), model);
	}

	@SuppressWarnings({ "rawtypes" })
	public ModelAndView previewSLGFIGrpPesoChequeDepositSlip(
			HttpServletRequest request, HttpServletResponse response,
			ChequeDepositSlipForm form) throws ApplicationException {

		Map model = previewCheque(request, response, form,
				Company.SLGFI.getId(), "peso.cheque.ds.jrxml.path");
		return new ModelAndView(new WMSJasperPdfView(), model);
	}

	public final ModelAndView doShowCDSPageSLGFIGrpDollarCash(
			HttpServletRequest request, HttpServletResponse response,
			CashDepositSlipForm form) throws Exception {

		return doShowCDSPageCash(request, response, form, Company.SLGFI,
				Currency.USD, "cdspage.cash.dollar", "slgfid.grp.cash.save",
				"slgfid.grp.cash.preview", 85L, "prod.group",
				ProductType.GREPA_GD_GROUPLIFE);

	}

	public final ModelAndView doShowCDSPageSLGFIGrpDollarCheck(
			HttpServletRequest request, HttpServletResponse response,
			ChequeDepositSlipForm form) throws Exception {

		return doShowCDSPageCheque(request, response, form, Company.SLGFI,
				Currency.USD, "cdspage.dc", "slgfid.grp.dc.save",
				"slgfid.grp.dc.preview", 86L, CheckType.DOLLARCHECK,
				"prod.group", false, ProductType.GREPA_GD_GROUPLIFE);
	}

	public final ModelAndView doShowCDSPageSLGFIGrpDollarUSCheckInManila(
			HttpServletRequest request, HttpServletResponse response,
			ChequeDepositSlipForm form) throws Exception {

		return doShowCDSPageCheque(request, response, form, Company.SLGFI,
				Currency.USD, "cdspage.ucim", "slgfid.grp.ucim.save",
				"slgfid.grp.ucim.preview", 87L, CheckType.USCD_MANILA,
				"prod.group", false, ProductType.GREPA_GD_GROUPLIFE);
	}

	public final ModelAndView doShowCDSPageSLGFIGrpDollarUSCheckOutsidePH(
			HttpServletRequest request, HttpServletResponse response,
			ChequeDepositSlipForm form) throws Exception {

		return doShowCDSPageCheque(request, response, form, Company.SLGFI,
				Currency.USD, "cdspage.ucop", "slgfid.grp.ucop.save",
				"slgfid.grp.ucop.preview", 88L, CheckType.USCD_OUTSIDEPH,
				"prod.group", false, ProductType.GREPA_GD_GROUPLIFE);
	}

	public final ModelAndView doShowCDSPageSLGFIGrpDollarBankOTC(
			HttpServletRequest request, HttpServletResponse response,
			ChequeDepositSlipForm form) throws Exception {

		return doShowCDSPageCheque(request, response, form, Company.SLGFI,
				Currency.USD, "cdspage.otc", "slgfid.unc.otc.save",
				"slgfid.unc.otc.preview", 89L, CheckType.BANK_OTC,
				"prod.group", false, ProductType.GREPA_GD_GROUPLIFE);
	}

	public ModelAndView saveSLGFIGrpDollarCash(HttpServletRequest request,
			HttpServletResponse response, CashDepositSlipForm form)
			throws Exception {

		saveCashDepositSlip(request, response, form, Currency.USD.getId(),
				Company.SLGFI.getId());
		return this.doShowCDSPageSLGFIGrpDollarCash(request, response, form);
	}

	public ModelAndView saveSLGFIGrpDollarCheck(HttpServletRequest request,
			HttpServletResponse response, ChequeDepositSlipForm form)
			throws Exception, ApplicationException {

		saveCheckDepositSlip(request, response, form, Currency.USD.getId(),
				Company.SLGFI.getId(), CheckType.DOLLARCHECK);

		return doShowCDSPageSLGFIGrpDollarCheck(request, response, form);
	}

	public ModelAndView saveSLGFIGrpDollarUSCheckInManila(
			HttpServletRequest request, HttpServletResponse response,
			ChequeDepositSlipForm form) throws Exception, ApplicationException {

		saveCheckDepositSlip(request, response, form, Currency.USD.getId(),
				Company.SLGFI.getId(), CheckType.USCD_MANILA);

		return doShowCDSPageSLGFIGrpDollarUSCheckInManila(request, response,
				form);
	}

	public ModelAndView saveSLGFIGrpDollarUSCheckOutsidePH(
			HttpServletRequest request, HttpServletResponse response,
			ChequeDepositSlipForm form) throws Exception, ApplicationException {

		saveCheckDepositSlip(request, response, form, Currency.USD.getId(),
				Company.SLGFI.getId(), CheckType.USCD_OUTSIDEPH);

		return doShowCDSPageSLGFIGrpDollarUSCheckOutsidePH(request, response,
				form);
	}

	public ModelAndView saveSLGFIGrpDollarBankOTC(HttpServletRequest request,
			HttpServletResponse response, ChequeDepositSlipForm form)
			throws Exception, ApplicationException {

		saveCheckDepositSlip(request, response, form, Currency.USD.getId(),
				Company.SLGFI.getId(), CheckType.BANK_OTC);

		return doShowCDSPageSLGFIUncDollarBankOTC(request, response, form);
	}

	@SuppressWarnings({ "rawtypes" })
	public ModelAndView previewSLGFIGrpDollarCash(HttpServletRequest request,
			HttpServletResponse response, CashDepositSlipForm form)
			throws ApplicationException {

		Map model = previewCash(request, response, form, Company.SLGFI.getId(),
				"dollar.cash.ds.jrxml.path");
		return new ModelAndView(new WMSJasperPdfView(), model);
	}

	@SuppressWarnings({ "rawtypes" })
	public ModelAndView previewSLGFIGrpDollarCheque(HttpServletRequest request,
			HttpServletResponse response, ChequeDepositSlipForm form)
			throws ApplicationException {

		Map model = previewCheque(request, response, form,
				Company.SLGFI.getId(), "dollar.cheque.ds.jrxml.path");
		return new ModelAndView(new WMSJasperPdfView(), model);
	}

	public final ModelAndView doShowCDSPageSLFPIPesoCash(
			HttpServletRequest request, HttpServletResponse response,
			CashDepositSlipForm form) throws Exception {

		return doShowCDSPageCash(request, response, form, Company.SLFPI,
				Currency.PHP, "cdspage.cash.peso", "slfpip.cash.save",
				"slfpip.cash.preview", 59L, "prod.pn", ProductType.SLIFPI_PN);

	}

	public final ModelAndView doShowCDSPageSLFPIPesoChequeOnUs(
			HttpServletRequest request, HttpServletResponse response,
			ChequeDepositSlipForm form) throws Exception {

		return doShowCDSPageCheque(request, response, form, Company.SLFPI,
				Currency.PHP, "cdspage.cou.peso", "slfpip.cou.save",
				"slfpip.cou.preview", 60L, CheckType.ON_US, "prod.pn", true,
				ProductType.SLIFPI_PN);
	}

	public final ModelAndView doShowCDSPageSLFPIPesoChequeLocalClearing(
			HttpServletRequest request, HttpServletResponse response,
			ChequeDepositSlipForm form) throws Exception {

		return doShowCDSPageCheque(request, response, form, Company.SLFPI,
				Currency.PHP, "cdspage.clc.peso", "slfpip.clc.save",
				"slfpip.clc.preview", 61L, CheckType.LOCAL, "prod.pn", true,
				ProductType.SLIFPI_PN);
	}

	public final ModelAndView doShowCDSPageSLFPIPesoChequeRegionalClearing(
			HttpServletRequest request, HttpServletResponse response,
			ChequeDepositSlipForm form) throws Exception {

		return doShowCDSPageCheque(request, response, form, Company.SLFPI,
				Currency.PHP, "cdspage.crc.peso", "slfpip.crc.save",
				"slfpip.crc.preview", 62L, CheckType.REGIONAL, "prod.pn", true,
				ProductType.SLIFPI_PN);
	}

	public final ModelAndView doShowCDSPageSLFPIPesoChequeOT(
			HttpServletRequest request, HttpServletResponse response,
			ChequeDepositSlipForm form) throws Exception {

		return doShowCDSPageCheque(request, response, form, Company.SLFPI,
				Currency.PHP, "cdspage.cot.peso", "slfpip.cot.save",
				"slfpip.cot.preview", 63L, CheckType.OT, "prod.pn", true,
				ProductType.SLIFPI_PN);
	}

	public ModelAndView saveSLFPIPesoCashDepositSlip(
			HttpServletRequest request, HttpServletResponse response,
			CashDepositSlipForm form) throws Exception {

		saveCashDepositSlip(request, response, form, Currency.PHP.getId(),
				Company.SLFPI.getId());
		return this.doShowCDSPageSLFPIPesoCash(request, response, form);
	}

	public ModelAndView saveSLFPIPesoChequeOnUsDepositSlip(
			HttpServletRequest request, HttpServletResponse response,
			ChequeDepositSlipForm form) throws Exception, ApplicationException {

		saveCheckDepositSlip(request, response, form, Currency.PHP.getId(),
				Company.SLFPI.getId(), CheckType.ON_US);

		return doShowCDSPageSLFPIPesoChequeOnUs(request, response, form);
	}

	public ModelAndView saveSLFPIPesoChequeLocalDepositSlip(
			HttpServletRequest request, HttpServletResponse response,
			ChequeDepositSlipForm form) throws Exception, ApplicationException {

		saveCheckDepositSlip(request, response, form, Currency.PHP.getId(),
				Company.SLFPI.getId(), CheckType.LOCAL);

		return doShowCDSPageSLFPIPesoChequeLocalClearing(request, response,
				form);
	}

	public ModelAndView saveSLFPIPesoChequeRegionalDepositSlip(
			HttpServletRequest request, HttpServletResponse response,
			ChequeDepositSlipForm form) throws Exception, ApplicationException {

		saveCheckDepositSlip(request, response, form, Currency.PHP.getId(),
				Company.SLFPI.getId(), CheckType.REGIONAL);

		return doShowCDSPageSLFPIPesoChequeRegionalClearing(request, response,
				form);
	}

	public ModelAndView saveSLFPIPesoChequeOTDepositSlip(
			HttpServletRequest request, HttpServletResponse response,
			ChequeDepositSlipForm form) throws Exception, ApplicationException {

		saveCheckDepositSlip(request, response, form, Currency.PHP.getId(),
				Company.SLFPI.getId(), CheckType.OT);

		return doShowCDSPageSLFPIPesoChequeOT(request, response, form);
	}

	@SuppressWarnings({ "rawtypes" })
	public ModelAndView previewSLFPIPesoCashDepositSlip(
			HttpServletRequest request, HttpServletResponse response,
			CashDepositSlipForm form) throws ApplicationException {

		Map model = previewCash(request, response, form, Company.SLFPI.getId(),
				"peso.cash.ds.jrxml.path");
		return new ModelAndView(new WMSJasperPdfView(), model);
	}

	@SuppressWarnings({ "rawtypes" })
	public ModelAndView previewSLFPIPesoChequeDepositSlip(
			HttpServletRequest request, HttpServletResponse response,
			ChequeDepositSlipForm form) throws ApplicationException {

		Map model = previewCheque(request, response, form,
				Company.SLFPI.getId(), "peso.cheque.ds.jrxml.path");
		return new ModelAndView(new WMSJasperPdfView(), model);
	}

	public final ModelAndView doShowCDSPageSLFPIPesoGafCash(
			HttpServletRequest request, HttpServletResponse response,
			CashDepositSlipForm form) throws Exception {

		return doShowCDSPageCash(request, response, form, Company.SLFPI,
				Currency.PHP, "cdspage.cash.peso", "slfpip.gaf.cash.save",
				"slfpip.gaf.cash.preview", 64L, "prod.gaf",
				ProductType.SLIFPI_GAF);

	}

	public final ModelAndView doShowCDSPageSLFPIPesoGafChequeOnUs(
			HttpServletRequest request, HttpServletResponse response,
			ChequeDepositSlipForm form) throws Exception {

		return doShowCDSPageCheque(request, response, form, Company.SLFPI,
				Currency.PHP, "cdspage.cou.peso", "slfpip.gaf.cou.save",
				"slfpip.gaf.cou.preview", 65L, CheckType.ON_US, "prod.gaf",
				false, ProductType.SLIFPI_GAF);
	}

	public final ModelAndView doShowCDSPageSLFPIPesoGafChequeLocalClearing(
			HttpServletRequest request, HttpServletResponse response,
			ChequeDepositSlipForm form) throws Exception {

		return doShowCDSPageCheque(request, response, form, Company.SLFPI,
				Currency.PHP, "cdspage.clc.peso", "slfpip.gaf.clc.save",
				"slfpip.gaf.clc.preview", 66L, CheckType.LOCAL, "prod.gaf",
				false, ProductType.SLIFPI_GAF);
	}

	public final ModelAndView doShowCDSPageSLFPIPesoGafChequeRegionalClearing(
			HttpServletRequest request, HttpServletResponse response,
			ChequeDepositSlipForm form) throws Exception {

		return doShowCDSPageCheque(request, response, form, Company.SLFPI,
				Currency.PHP, "cdspage.crc.peso", "slfpip.gaf.crc.save",
				"slfpip.gaf.crc.preview", 67L, CheckType.REGIONAL, "prod.gaf",
				false, ProductType.SLIFPI_GAF);
	}

	public final ModelAndView doShowCDSPageSSLFPIPesoGafChequeOT(
			HttpServletRequest request, HttpServletResponse response,
			ChequeDepositSlipForm form) throws Exception {

		return doShowCDSPageCheque(request, response, form, Company.SLFPI,
				Currency.PHP, "cdspage.cot.peso", "slfpip.gaf.cot.save",
				"slfpip.gaf.cot.preview", 68L, CheckType.OT, "prod.gaf", false,
				ProductType.SLIFPI_GAF);
	}

	public ModelAndView saveSLFPIPesoGafCashDepositSlip(
			HttpServletRequest request, HttpServletResponse response,
			CashDepositSlipForm form) throws Exception {

		saveCashDepositSlip(request, response, form, Currency.PHP.getId(),
				Company.SLFPI.getId());
		return this.doShowCDSPageSLFPIPesoGafCash(request, response, form);
	}

	public ModelAndView saveSLFPIPesoGafChequeOnUsDepositSlip(
			HttpServletRequest request, HttpServletResponse response,
			ChequeDepositSlipForm form) throws Exception, ApplicationException {

		saveCheckDepositSlip(request, response, form, Currency.PHP.getId(),
				Company.SLFPI.getId(), CheckType.ON_US);

		return doShowCDSPageSLFPIPesoGafChequeOnUs(request, response, form);
	}

	public ModelAndView saveSLFPIPesoGafChequeLocalDepositSlip(
			HttpServletRequest request, HttpServletResponse response,
			ChequeDepositSlipForm form) throws Exception, ApplicationException {

		saveCheckDepositSlip(request, response, form, Currency.PHP.getId(),
				Company.SLFPI.getId(), CheckType.LOCAL);

		return doShowCDSPageSLFPIPesoGafChequeLocalClearing(request, response,
				form);
	}

	public ModelAndView saveSLFPIPesoGafChequeRegionalDepositSlip(
			HttpServletRequest request, HttpServletResponse response,
			ChequeDepositSlipForm form) throws Exception, ApplicationException {

		saveCheckDepositSlip(request, response, form, Currency.PHP.getId(),
				Company.SLFPI.getId(), CheckType.REGIONAL);

		return doShowCDSPageSLFPIPesoGafChequeRegionalClearing(request,
				response, form);
	}

	public ModelAndView saveSLFPIPesoGafChequeOTDepositSlip(
			HttpServletRequest request, HttpServletResponse response,
			ChequeDepositSlipForm form) throws Exception, ApplicationException {

		saveCheckDepositSlip(request, response, form, Currency.PHP.getId(),
				Company.SLFPI.getId(), CheckType.OT);

		return doShowCDSPageSSLFPIPesoGafChequeOT(request, response, form);
	}

	@SuppressWarnings({ "rawtypes" })
	public ModelAndView previewSLFPIPesoGafCashDepositSlip(
			HttpServletRequest request, HttpServletResponse response,
			CashDepositSlipForm form) throws ApplicationException {

		Map model = previewCash(request, response, form, Company.SLFPI.getId(),
				"peso.cash.ds.jrxml.path");
		return new ModelAndView(new WMSJasperPdfView(), model);
	}

	@SuppressWarnings({ "rawtypes" })
	public ModelAndView previewSLFPIPesoGafChequeDepositSlip(
			HttpServletRequest request, HttpServletResponse response,
			ChequeDepositSlipForm form) throws ApplicationException {

		Map model = previewCheque(request, response, form,
				Company.SLFPI.getId(), "peso.cheque.ds.jrxml.path");
		return new ModelAndView(new WMSJasperPdfView(), model);
	}

	public final ModelAndView doShowCDSPageOTHERPesoCash(
			HttpServletRequest request, HttpServletResponse response,
			CashDepositSlipForm form) throws Exception {

		return doShowCDSPageCash(request, response, form, Company.OTHER,
				Currency.PHP, "cdspage.cash.peso", "otherp.cash.save",
				"otherp.cash.preview", 70L, "prod.cash", ProductType.OTHER_PESO);

	}

	public final ModelAndView doShowCDSPageOTHERPesoChequeOnUs(
			HttpServletRequest request, HttpServletResponse response,
			ChequeDepositSlipForm form) throws Exception {

		return doShowCDSPageCheque(request, response, form, Company.OTHER,
				Currency.PHP, "cdspage.cou.peso", "otherp.cou.save",
				"otherp.cou.preview", 71L, CheckType.ON_US, "prod.cheque",
				false, ProductType.OTHER_PESO);
	}

	public final ModelAndView doShowCDSPageOTHERPesoChequeLocalClearing(
			HttpServletRequest request, HttpServletResponse response,
			ChequeDepositSlipForm form) throws Exception {

		return doShowCDSPageCheque(request, response, form, Company.OTHER,
				Currency.PHP, "cdspage.clc.peso", "otherp.clc.save",
				"otherp.clc.preview", 72L, CheckType.LOCAL, "prod.cheque",
				false, ProductType.OTHER_PESO);
	}

	public final ModelAndView doShowCDSPageOTHERPesoChequeRegionalClearing(
			HttpServletRequest request, HttpServletResponse response,
			ChequeDepositSlipForm form) throws Exception {

		return doShowCDSPageCheque(request, response, form, Company.OTHER,
				Currency.PHP, "cdspage.crc.peso", "otherp.crc.save",
				"otherp.crc.preview", 73L, CheckType.REGIONAL, "prod.cheque",
				false, ProductType.OTHER_PESO);
	}

	public final ModelAndView doShowCDSPageOTHERPesoChequeOT(
			HttpServletRequest request, HttpServletResponse response,
			ChequeDepositSlipForm form) throws Exception {

		return doShowCDSPageCheque(request, response, form, Company.OTHER,
				Currency.PHP, "cdspage.cot.peso", "otherp.cot.save",
				"otherp.cot.preview", 74L, CheckType.OT, "prod.cheque", false,
				ProductType.OTHER_PESO);
	}

	public ModelAndView saveOTHERPesoCashDepositSlip(
			HttpServletRequest request, HttpServletResponse response,
			CashDepositSlipForm form) throws Exception {

		saveCashDepositSlip(request, response, form, Currency.PHP.getId(),
				Company.OTHER.getId());
		return this.doShowCDSPageOTHERPesoCash(request, response, form);
	}

	public ModelAndView saveOTHERPesoChequeOnUsDepositSlip(
			HttpServletRequest request, HttpServletResponse response,
			ChequeDepositSlipForm form) throws Exception, ApplicationException {

		saveCheckDepositSlip(request, response, form, Currency.PHP.getId(),
				Company.OTHER.getId(), CheckType.ON_US);

		return doShowCDSPageOTHERPesoChequeOnUs(request, response, form);
	}

	public ModelAndView saveOTHERPesoChequeLocalDepositSlip(
			HttpServletRequest request, HttpServletResponse response,
			ChequeDepositSlipForm form) throws Exception, ApplicationException {

		saveCheckDepositSlip(request, response, form, Currency.PHP.getId(),
				Company.OTHER.getId(), CheckType.LOCAL);

		return doShowCDSPageOTHERPesoChequeLocalClearing(request, response,
				form);
	}

	public ModelAndView saveOTHERPesoChequeRegionalDepositSlip(
			HttpServletRequest request, HttpServletResponse response,
			ChequeDepositSlipForm form) throws Exception, ApplicationException {

		saveCheckDepositSlip(request, response, form, Currency.PHP.getId(),
				Company.OTHER.getId(), CheckType.REGIONAL);

		return doShowCDSPageOTHERPesoChequeRegionalClearing(request, response,
				form);
	}

	public ModelAndView saveOTHERPesoChequeOTDepositSlip(
			HttpServletRequest request, HttpServletResponse response,
			ChequeDepositSlipForm form) throws Exception, ApplicationException {

		saveCheckDepositSlip(request, response, form, Currency.PHP.getId(),
				Company.OTHER.getId(), CheckType.OT);

		return doShowCDSPageOTHERPesoChequeOT(request, response, form);
	}

	@SuppressWarnings({ "rawtypes" })
	public ModelAndView previewOTHERPesoCashDepositSlip(
			HttpServletRequest request, HttpServletResponse response,
			CashDepositSlipForm form) throws ApplicationException {

		Map model = previewCash(request, response, form, Company.OTHER.getId(),
				"peso.cash.ds.jrxml.path");
		return new ModelAndView(new WMSJasperPdfView(), model);
	}

	@SuppressWarnings({ "rawtypes" })
	public ModelAndView previewOTHERPesoChequeDepositSlip(
			HttpServletRequest request, HttpServletResponse response,
			ChequeDepositSlipForm form) throws ApplicationException {

		Map model = previewCheque(request, response, form,
				Company.OTHER.getId(), "peso.cheque.ds.jrxml.path");
		return new ModelAndView(new WMSJasperPdfView(), model);
	}

	public final ModelAndView doShowCDSPageOTHERDollarCash(
			HttpServletRequest request, HttpServletResponse response,
			CashDepositSlipForm form) throws Exception {

		return doShowCDSPageCash(request, response, form, Company.OTHER,
				Currency.USD, "cdspage.cash.dollar", "otherd.cash.save",
				"otherd.cash.preview", 75L, "prod.cash",
				ProductType.OTHER_DOLLAR);

	}

	public final ModelAndView doShowCDSPageOTHERDollarCheck(
			HttpServletRequest request, HttpServletResponse response,
			ChequeDepositSlipForm form) throws Exception {

		return doShowCDSPageCheque(request, response, form, Company.OTHER,
				Currency.USD, "cdspage.dc", "otherd.dc.save",
				"otherd.dc.preview", 76L, CheckType.DOLLARCHECK, "prod.cheque",
				false, ProductType.OTHER_DOLLAR);
	}

	public final ModelAndView doShowCDSPageOTHERDollarUSCheckInManila(
			HttpServletRequest request, HttpServletResponse response,
			ChequeDepositSlipForm form) throws Exception {

		return doShowCDSPageCheque(request, response, form, Company.OTHER,
				Currency.USD, "cdspage.ucim", "otherd.ucim.save",
				"otherd.ucim.preview", 77L, CheckType.USCD_MANILA,
				"prod.cheque", false, ProductType.OTHER_DOLLAR);
	}

	public final ModelAndView doShowCDSPageOTHERDollarUSCheckOutsidePH(
			HttpServletRequest request, HttpServletResponse response,
			ChequeDepositSlipForm form) throws Exception {

		return doShowCDSPageCheque(request, response, form, Company.OTHER,
				Currency.USD, "cdspage.ucop", "otherd.ucop.save",
				"otherd.ucop.preview", 78L, CheckType.USCD_OUTSIDEPH,
				"prod.cheque", false, ProductType.OTHER_DOLLAR);
	}

	public final ModelAndView doShowCDSPageOTHERDollarBankOTC(
			HttpServletRequest request, HttpServletResponse response,
			ChequeDepositSlipForm form) throws Exception {

		return doShowCDSPageCheque(request, response, form, Company.OTHER,
				Currency.USD, "cdspage.otc", "otherd.otc.save",
				"otherd.otc.preview", 79L, CheckType.BANK_OTC, "prod.cheque",
				false, ProductType.OTHER_DOLLAR);
	}

	public ModelAndView saveOTHERDollarCashDepositSlip(
			HttpServletRequest request, HttpServletResponse response,
			CashDepositSlipForm form) throws Exception {

		saveCashDepositSlip(request, response, form, Currency.USD.getId(),
				Company.OTHER.getId());
		return this.doShowCDSPageOTHERDollarCash(request, response, form);
	}

	public ModelAndView saveOTHERDollarCheck(HttpServletRequest request,
			HttpServletResponse response, ChequeDepositSlipForm form)
			throws Exception, ApplicationException {

		saveCheckDepositSlip(request, response, form, Currency.USD.getId(),
				Company.OTHER.getId(), CheckType.DOLLARCHECK);

		return doShowCDSPageOTHERDollarCheck(request, response, form);
	}

	public ModelAndView saveOTHERDollarUSCheckInManila(
			HttpServletRequest request, HttpServletResponse response,
			ChequeDepositSlipForm form) throws Exception, ApplicationException {

		saveCheckDepositSlip(request, response, form, Currency.USD.getId(),
				Company.OTHER.getId(), CheckType.USCD_MANILA);

		return doShowCDSPageOTHERDollarUSCheckInManila(request, response, form);
	}

	public ModelAndView saveOTHERDollarUSCheckOutsidePH(
			HttpServletRequest request, HttpServletResponse response,
			ChequeDepositSlipForm form) throws Exception, ApplicationException {

		saveCheckDepositSlip(request, response, form, Currency.USD.getId(),
				Company.OTHER.getId(), CheckType.USCD_OUTSIDEPH);

		return doShowCDSPageOTHERDollarUSCheckOutsidePH(request, response, form);
	}

	public ModelAndView saveOTHERDollarBankOTC(HttpServletRequest request,
			HttpServletResponse response, ChequeDepositSlipForm form)
			throws Exception, ApplicationException {

		saveCheckDepositSlip(request, response, form, Currency.USD.getId(),
				Company.OTHER.getId(), CheckType.BANK_OTC);

		return doShowCDSPageOTHERDollarBankOTC(request, response, form);
	}

	@SuppressWarnings({ "rawtypes" })
	public ModelAndView previewOTHERDollarCashDepositSlip(
			HttpServletRequest request, HttpServletResponse response,
			CashDepositSlipForm form) throws ApplicationException {

		Map model = previewCash(request, response, form, Company.OTHER.getId(),
				"dollar.cash.ds.jrxml.path");
		return new ModelAndView(new WMSJasperPdfView(), model);
	}

	@SuppressWarnings({ "rawtypes" })
	public ModelAndView previewOTHERDollarChequeDepositSlip(
			HttpServletRequest request, HttpServletResponse response,
			ChequeDepositSlipForm form) throws ApplicationException {

		Map model = previewCheque(request, response, form,
				Company.OTHER.getId(), "dollar.cheque.ds.jrxml.path");
		return new ModelAndView(new WMSJasperPdfView(), model);
	}

	@Override
	protected Role[] allowedRoles() {
		return new Role[] { Role.CASHIER };
	}

	@Override
	protected String getPageTitle() {
		return pageTitle;
	}

	public void setPageTitle(String pageTitle) {
		this.pageTitle = pageTitle;
	}

	public void setDcrDepositSlipService(
			DCRDepositSlipService dcrDepositSlipService) {
		this.dcrDepositSlipService = dcrDepositSlipService;
	}

	@Override
	protected Company getCompany() {
		return null;
	}

	@Override
	protected String getDisplayViewName() {
		return null;
	}

	public void setDcrMixedChequePayeeService(
			DCRMixedChequePayeeService dcrMixedChequePayeeService) {
		this.dcrMixedChequePayeeService = dcrMixedChequePayeeService;
	}

    // Added for PCO
    /**
     * @return the removeFromTotalProduct
     */
    public String getRemoveFromTotalProduct() {
        return removeFromTotalProduct;
    }

    /**
     * @param removeFromTotalProduct the removeFromTotalProduct to set
     */
    public void setRemoveFromTotalProduct(String removeFromTotalProduct) {
        this.removeFromTotalProduct = removeFromTotalProduct;
    }

}
