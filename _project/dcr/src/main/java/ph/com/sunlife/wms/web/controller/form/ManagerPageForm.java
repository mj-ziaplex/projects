package ph.com.sunlife.wms.web.controller.form;

import java.util.Calendar;
import java.util.Date;

import ph.com.sunlife.wms.util.WMSDateUtil;

public class ManagerPageForm extends CashierHomePageForm {

	private static final String ALL_MANAGER_DCR_KEY = "allManagerDcr";

	private Date dcrStartDate;

	private Date dcrEndDate;

	private String dcrDateStr;

	private String filterSelect;
	
	private String status;
	
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getFilterSelect() {
		return filterSelect;
	}

	public void setFilterSelect(String filterSelect) {
		this.filterSelect = filterSelect;
	}

	public Date getDcrStartDate() {
		return dcrStartDate;
	}

	public void setDcrStartDate(Date dcrStartDate) {
		if (dcrStartDate != null) {
			this.dcrStartDate = dcrStartDate;
		} else {
//			this.dcrStartDate = WMSDateUtil.startOfMonth(WMSDateUtil
//					.toFormattedDateStr(new Date()));
			this.dcrStartDate = WMSDateUtil.toDate("01JAN1970");
		}
		// this.dcrDateStr = WMSDateUtil.toFormattedDateStr(dcrStartDate);
	}

	public Date getDcrEndDate() {
		return dcrEndDate;
	}

	public void setDcrEndDate(Date dcrEndDate) {
		if (dcrEndDate != null) {
			this.dcrEndDate = dcrEndDate;
		} else {
			this.dcrEndDate = WMSDateUtil.endOfMonth(WMSDateUtil
					.toFormattedDateStr(new Date()));
		}
	}

	public String getDcrDateStr() {
		return dcrDateStr;
	}

	public void setDcrDateStr(String dcrDateStr) {
		this.dcrDateStr = dcrDateStr;
		// Calendar c = Calendar.getInstance();
		// if(!dcrDateStr.equalsIgnoreCase(ALL_MANAGER_DCR_KEY)){
		// this.dcrStartDate =
		// WMSDateUtil.startOfTheDay(WMSDateUtil.toDate(dcrDateStr));
		// c.setTime(WMSDateUtil.toDate(dcrDateStr));
		// c.add(Calendar.DATE, 1);
		// this.dcrEndDate = WMSDateUtil.startOfTheDay(c.getTime());
		// } else{
		// this.dcrStartDate =
		// WMSDateUtil.startOfMonth(WMSDateUtil.toFormattedDateStr(new Date()));
		// c.setTime(WMSDateUtil.endOfMonth(WMSDateUtil.toFormattedDateStr(new
		// Date())));
		// c.add(Calendar.DATE, 1);
		// this.dcrEndDate = WMSDateUtil.startOfTheDay(c.getTime());
		// }
	}

}
