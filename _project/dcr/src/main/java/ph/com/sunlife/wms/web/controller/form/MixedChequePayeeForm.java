package ph.com.sunlife.wms.web.controller.form;

import java.util.Date;

import ph.com.sunlife.wms.util.WMSDateUtil;

public class MixedChequePayeeForm extends CashierWorkItemForm {
	
	public Long mcpId;
	
	public String bankDD;
	
	public String checkNumberTB;
	
	public String searchDatePicker;
	
	public Double checkAmountTB;
	
	public Long typeOfCheckDD;
	
	public Long payeeDD;
	
	public Long currencyDD;
	
	public Double slamciTB;
	
	public Double slocpiTB;
	
	public Double slgfiTB;
	
	public Double slfpiTB;
	
	public Double otherTB;
	
	public Date checkDate;

	public Long getMcpId() {
		return mcpId;
	}

	public void setMcpId(Long mcpId) {
		this.mcpId = mcpId;
	}

	public Date getCheckDate() {
		return checkDate;
	}

	public void setCheckDate(Date checkDate) {
		this.checkDate = checkDate;
		this.searchDatePicker = WMSDateUtil.toFormattedDateStr(checkDate);
	}

	public String getBankDD() {
		return bankDD;
	}

	public void setBankDD(String bankDD) {
		this.bankDD = bankDD;
	}

	public String getCheckNumberTB() {
		return checkNumberTB;
	}

	public void setCheckNumberTB(String checkNumberTB) {
		this.checkNumberTB = checkNumberTB;
	}

	public String getSearchDatePicker() {
		return searchDatePicker;
	}

	public void setSearchDatePicker(String searchDatePicker) {
		this.searchDatePicker = searchDatePicker;
		this.checkDate = WMSDateUtil.toDate(searchDatePicker);
	}

	public Double getCheckAmountTB() {
		return checkAmountTB;
	}

	public void setCheckAmountTB(Double checkAmountTB) {
		this.checkAmountTB = checkAmountTB;
	}

	public Long getTypeOfCheckDD() {
		return typeOfCheckDD;
	}

	public void setTypeOfCheckDD(Long typeOfCheckDD) {
		this.typeOfCheckDD = typeOfCheckDD;
	}

	public Long getPayeeDD() {
		return payeeDD;
	}

	public void setPayeeDD(Long payeeDD) {
		this.payeeDD = payeeDD;
	}

	public Long getCurrencyDD() {
		return currencyDD;
	}

	public void setCurrencyDD(Long currencyDD) {
		this.currencyDD = currencyDD;
	}

	public Double getSlamciTB() {
		return slamciTB;
	}

	public void setSlamciTB(Double slamciTB) {
		this.slamciTB = slamciTB;
	}

	public Double getSlocpiTB() {
		return slocpiTB;
	}

	public void setSlocpiTB(Double slocpiTB) {
		this.slocpiTB = slocpiTB;
	}

	public Double getSlgfiTB() {
		return slgfiTB;
	}

	public void setSlgfiTB(Double slgfiTB) {
		this.slgfiTB = slgfiTB;
	}

	public Double getSlfpiTB() {
		return slfpiTB;
	}

	public void setSlfpiTB(Double slfpiTB) {
		this.slfpiTB = slfpiTB;
	}

	public Double getOtherTB() {
		return otherTB;
	}

	public void setOtherTB(Double otherTB) {
		this.otherTB = otherTB;
	}

}
