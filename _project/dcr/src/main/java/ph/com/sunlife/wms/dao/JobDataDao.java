package ph.com.sunlife.wms.dao;

import java.util.List;

import ph.com.sunlife.wms.dao.domain.JobData;
import ph.com.sunlife.wms.dao.domain.JobDataErrorLog;
import ph.com.sunlife.wms.dao.exceptions.WMSDaoException;

public interface JobDataDao {

	Long createErrorLog(JobDataErrorLog jobDataErrorLog) throws WMSDaoException;

	List<JobData> getAllJobData() throws WMSDaoException;

	List<JobDataErrorLog> getJobDataErrorLog(Long jobId) throws WMSDaoException;

	boolean updateJobData(JobData jobData) throws WMSDaoException;

}
