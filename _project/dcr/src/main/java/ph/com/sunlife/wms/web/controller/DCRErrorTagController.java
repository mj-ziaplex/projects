package ph.com.sunlife.wms.web.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.web.servlet.ModelAndView;

import ph.com.sunlife.wms.security.Role;
import ph.com.sunlife.wms.services.DCRErrorTagService;
import ph.com.sunlife.wms.services.bo.DCRCompletedStepBO;
import ph.com.sunlife.wms.services.bo.DCRErrorBO;
import ph.com.sunlife.wms.services.bo.DCRErrorTagLogBO;
import ph.com.sunlife.wms.services.bo.DCRErrorTagLogConsolidatedDisplayBO;
import ph.com.sunlife.wms.services.bo.DCRErrorTagLookupBO;
import ph.com.sunlife.wms.services.bo.UserSession;
import ph.com.sunlife.wms.services.exception.ServiceException;
import ph.com.sunlife.wms.web.controller.form.ErrorTagForm;
import ph.com.sunlife.wms.web.controller.form.ErrorTagSaveForm;
import ph.com.sunlife.wms.web.exception.ApplicationException;

public class DCRErrorTagController extends AbstractSecuredMultiActionController {
	
	//private static final String ERROR_LOOKUP = "errLookup";
	
	private static final String ERROR_LIST = "errorList";
	
	private static final String ERROR_LOG = "errLog";
	
	private static final String TITLE_KEY = "title";
	
	private DCRErrorTagService dcrErrorTagService;
	
	private String pageTitle;
	
	private String displayViewName;
	
	public DCRErrorTagService getDcrErrorTagService() {
		return dcrErrorTagService;
	}

	public void setDcrErrorTagService(DCRErrorTagService dcrErrorTagService) {
		this.dcrErrorTagService = dcrErrorTagService;
	}

	public String getPageTitle() {
		return pageTitle;
	}

	public void setPageTitle(String pageTitle) {
		this.pageTitle = pageTitle;
	}

	public String getDisplayViewName() {
		return displayViewName;
	}

	public void setDisplayViewName(String displayViewName) {
		this.displayViewName = displayViewName;
	}

	private static final Logger LOGGER = Logger.getLogger(DCRErrorTagController.class);
	
	//onClick of launchBtn, pass dcrId
	public ModelAndView doShowErrorTagScreen(HttpServletRequest request, 
			HttpServletResponse response, ErrorTagForm form) throws ApplicationException{
		ModelAndView modelAndView = new ModelAndView();
		try{
			//call 2 'get' svc methods
			//DCRErrorTagLookupBO lookup = dcrErrorTagService.getDCRErrorTagLookupDisplay(Long.valueOf(form.getDcrId()));
			//List<DCRErrorTagLogDisplayBO> log = dcrErrorTagService.getDCRErrorTagLogDisplay(Long.valueOf(form.getDcrId()));
			//modelAndView.addObject(ERROR_LOOKUP, lookup);
			
			List<DCRErrorBO> errList = dcrErrorTagService.getDCRErrorByStatus(form.getDcrStatus());
			List<DCRErrorTagLogConsolidatedDisplayBO> log = dcrErrorTagService.getDCRErrorTagLogDisplay(Long.valueOf(form.getDcrId()));
			
			modelAndView.addObject(ERROR_LIST, errList);
			modelAndView.addObject(ERROR_LOG, log);
			
			modelAndView.addObject("errorTagDcrId", form.getDcrId());
			modelAndView.addObject("stepIdByStatus", dcrErrorTagService.getStepIdByStatus(form.getDcrStatus()));//get step id because
			modelAndView.addObject("stepNameAsStatus", form.getDcrStatus());//dcr status is equal to step name
			List<String> cashierList = convertCSVToList(form.getConcatCashierNameList());
			modelAndView.addObject("concatCashierNameListFromSP", form.getConcatCashierNameList());
			modelAndView.addObject("dcrStatus",form.getDcrStatus());
			modelAndView.addObject("dcrDate",form.getDcrDate());
			modelAndView.addObject("ccName",form.getCcName());
			
			modelAndView.addObject("saveErrorTagMapping", "/errorTag/save.html");
			modelAndView.addObject("editErrorTagMapping", "/errorTag/edit.html");
			modelAndView.addObject("deleteErrorTagMapping", "/errorTag/delete.html");
			
			modelAndView.addObject(TITLE_KEY, "Error Tagging Utility");
			
			//if dcrStatus is 'For Review and Approval', cashierList lang...but if 'Approved for Recon', add CCM id to this dropdown
			String managerId = null;
			if(form.getDcrStatus().equalsIgnoreCase("Approved for Recon")
				|| form.getDcrStatus().equalsIgnoreCase("Verified")
				|| form.getDcrStatus().equalsIgnoreCase("Not Verified")
				|| form.getDcrStatus().equalsIgnoreCase("Reqts Submitted")
				|| form.getDcrStatus().equalsIgnoreCase("Reqts Not Submitted")
				|| form.getDcrStatus().equalsIgnoreCase("For Verification")
				|| form.getDcrStatus().equalsIgnoreCase("Awaiting Reqts")){
				managerId = dcrErrorTagService.getManagerByDcrId(Long.valueOf(form.getDcrId()));
				List<Role> session = this.getUserSession(request).getRoles();
				if(session.contains(Role.PPA)){
					List<String> allUsers = dcrErrorTagService.getUsersFromAuditLogByDcrId(form.getDcrId());
					Set<String> cashierSet = new HashSet<String>(cashierList);
					cashierSet.addAll(allUsers);
					cashierList = new ArrayList<String>(cashierSet);
				}
			}
			modelAndView.addObject("cashierIDs", cashierList);
			modelAndView.addObject("managerId", managerId);
			UserSession us= getUserSession(request);
			String loggedInUser = us.getUserId();
			Role role = us.getRoles().get(0);
			boolean fromPPA =false;
			if(Role.PPA.equals(role)){
			 fromPPA =true;
			}
			modelAndView.addObject("fromPPA",fromPPA);
			modelAndView.addObject("loggedInUser",loggedInUser);
			
		}catch(ServiceException e){
			LOGGER.error(e);
			throw new ApplicationException(e);
		}
		modelAndView.setViewName("errorTagScreen");
		return modelAndView;
	}
	
	private List<String> convertCSVToList(String concatCashierNameList) {
		List<String> list = new ArrayList<String>();
		String[] splitted = StringUtils.split(concatCashierNameList, "~");
		if(splitted != null){
			for(String s : splitted){
				list.add(s);
			}
		}
		return list;
	}

	//use a more detailed form, but extend ErrorTagForm
	public ModelAndView saveErrorTag(HttpServletRequest request, 
			HttpServletResponse response, ErrorTagSaveForm form) throws ApplicationException{
		//ModelAndView modelAndView = new ModelAndView();
		DCRErrorTagLogBO bo = new DCRErrorTagLogBO();
		UserSession us= getUserSession(request);
		String loggedInUser = us.getUserId();
		bo.setPostedUpdatedById(us.getUserId());
		
		  
		Role role = us.getRoles().get(0);
		
		convertToBO(form,bo,"Add");
		try {
			bo = dcrErrorTagService.addDCRErrorTagLog(bo);
		} catch (ServiceException e) {
			LOGGER.error(e);
			throw new ApplicationException(e);
		}
		//modelAndView.setViewName("errorTagScreen");
		ModelAndView modelAndView = doShowErrorTagScreen(request, response, form);
		
		boolean fromPPA =false;
		if(Role.PPA.equals(role)){
		 fromPPA =true;
		}
		modelAndView.addObject("fromPPA",fromPPA);
		modelAndView.addObject("loggedInUser",loggedInUser);
		return modelAndView;
	}
	
	//not used anymore
	public ModelAndView editErrorTag(HttpServletRequest request, 
			HttpServletResponse response, ErrorTagSaveForm form) throws ApplicationException{
		
		DCRErrorTagLogBO bo = new DCRErrorTagLogBO();
		UserSession us= getUserSession(request);
		bo.setPostedUpdatedById(us.getUserId());
		convertToBO(form,bo,"Edit");
		Role role = us.getRoles().get(0);
		try{
			dcrErrorTagService.editDCRErrorLogAndLookup(bo);
		}catch(ServiceException e){
			LOGGER.error(e);
			throw new ApplicationException(e);
		}
		
		//mga needed to edit:
		//A.update 'date updated' and 'reason' 
		//B.wipe all associated errs via 'dcrId' and 'stepId'
		//C.loop insert 'dcr errs'
		
		ModelAndView modelAndView = doShowErrorTagScreen(request, response, form);
		boolean fromPPA =false;
		if(Role.PPA.equals(role)){
		 fromPPA =true;
		}
		modelAndView.addObject("fromPPA",fromPPA);
		return modelAndView;
	}
	
	private void convertToBO(ErrorTagSaveForm form, DCRErrorTagLogBO bo, String addOrEdit) {
		if(addOrEdit=="Edit"){
			bo.setId(Long.valueOf(form.getLogIdentity()));
		}
		bo.setDcrId(Long.valueOf(form.getDcrId()));
		Date now = new Date();
		bo.setDatePosted(now);
		bo.setDateUpdated(now);
		bo.setReason(form.getReason());
		DCRCompletedStepBO step = new DCRCompletedStepBO();
		step.setStepId(form.getStepId());
		step.setStepName(form.getStepName());
		step.setUserName(form.getUserName());
		List<DCRErrorBO> list = form.getTaggedErrors();
		List<DCRErrorBO> newList = new ArrayList<DCRErrorBO>();
		for(DCRErrorBO e : list){
			if(e!=null){
				newList.add(e);
			}
		}
		step.setDcrErrors(newList);
		bo.setDcrCompletedStep(step);
	}

	//dcrId AND stepId lang ipasa sa form for this
	public ModelAndView deleteErrorTag(HttpServletRequest request, 
			HttpServletResponse response, ErrorTagSaveForm form) throws ApplicationException{
		
		String reasonDeleted = form.getReasonDeleted();
		Long dcrId = Long.valueOf(form.getDcrId());
		String stepId = form.getStepId();
		String userId = form.getUserId();
		String tagger = form.getTagger();
		UserSession us= getUserSession(request);
		String loggedInUser = us.getUserId();
		Role role  = us.getRoles().get(0);
		try{
			//TODO: FIX TAGGER
			dcrErrorTagService.tagErrorLogAsDeleted(reasonDeleted, dcrId, stepId, userId, tagger);
		}catch(ServiceException e){
			LOGGER.error(e);
			throw new ApplicationException(e);
		}
		
		ModelAndView modelAndView = doShowErrorTagScreen(request, response, form);
		boolean fromPPA =false;
		if(Role.PPA.equals(role)){
		 fromPPA =true;
		}
		modelAndView.addObject("fromPPA",fromPPA);
		modelAndView.addObject("loggedInUser",loggedInUser);
		return modelAndView;		
	}
	
//	public ModelAndView doShowHomePage(HttpServletRequest request,
//			HttpServletResponse response, PPAMDSForm form) throws ApplicationException {
//		
//		ModelAndView modelAndView = new ModelAndView();
//		
//		try{
//			UserSession userSession = getUserSession(request);
//			Long cashierId = form.getDcrCashierId();
//			String userId = userSession.getUserId();
//			DCRCashierBO dcrCashierBO = dcrCreationService.getDCRCashierBO(cashierId, userId);
//			String ccId = dcrCashierBO.getDcr().getCcId();
//			Date dateTo = dcrCashierBO.getDcr().getDcrDate(); 
//			Calendar c = Calendar.getInstance();
//			c.setTime(dateTo);
//			c.add(Calendar.DAY_OF_MONTH, -1);
//			Date dateFrom = c.getTime();
			
//			String dcrId = form.getDcrId();
//			System.out.println("dcrId: "+dcrId);
//			String ccId = form.getCcId();
//			String dcrDateStr = form.getDcrDateStr();
//			
//			Date dateTo = WMSDateUtil.toDateWithTime(dcrDateStr); //pass dcrId, get DCRBO, then get ccId and dcrDate
//			
//			Calendar c = Calendar.getInstance();
//			c.setTime(dateTo);
//			c.add(Calendar.DAY_OF_MONTH, -1);
//			Date dateFrom = c.getTime();
//			
//			List<PPAMDSBO> ppaMDSBO = ppaMDSService.getMDSTransactions(ccId, dateFrom, dateTo, Long.valueOf(dcrId));
//			modelAndView.addObject(PPAMDS_KEY, ppaMDSBO);
//			modelAndView.addObject("dcrId", dcrId);
//			modelAndView.addObject("ccId", ccId);
//			modelAndView.addObject("dcrDateStr", dcrDateStr);
//			modelAndView.addObject("savePPAMDSMapping", "/ppamds/save.html");
//		}catch(ServiceException e){
//			LOGGER.error(e);
//			throw new ApplicationException(e);
//		}
//		
//		
//		return modelAndView;
//	}

//	public ModelAndView doSave(HttpServletRequest request,
//			HttpServletResponse response, MDSForm form) throws ApplicationException {
//		ModelAndView modelAndView = new ModelAndView();
//		
//		List<DCRPPAMDSBO> reconciledEntries = form.getPpamdsRecons();
//		List<DCRPPAMDSBO> unreconciledEntries = form.getPpamdsUnrecons();
//		
//		try {
//			ppaMDSService.reconcilePPAMDSList(reconciledEntries);
//			ppaMDSService.unreconcilePPAMDSList(unreconciledEntries);//dapat matawag ito
//		} catch (ServiceException e) {
//			LOGGER.error(e);
//			throw new ApplicationException(e);
//		}
//		modelAndView = this.doShowHomePage(request, response, form);
//		modelAndView.setViewName(PPAMDS_PAGE_VIEW);
//		
//		return modelAndView;
//	}
	
	@Override
	protected Role[] allowedRoles() {
		Role[] ppa = {Role.PPA, Role.MANAGER};
		return ppa;
	}

}
