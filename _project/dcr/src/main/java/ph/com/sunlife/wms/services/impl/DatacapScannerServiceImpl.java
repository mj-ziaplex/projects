package ph.com.sunlife.wms.services.impl;

import java.util.List;

import org.apache.log4j.Logger;

import ph.com.sunlife.wms.dao.DatacapScannerDao;
import ph.com.sunlife.wms.dao.domain.DatacapScanner;
import ph.com.sunlife.wms.dao.exceptions.WMSDaoException;
import ph.com.sunlife.wms.services.DatacapScannerService;
import ph.com.sunlife.wms.services.exception.ServiceException;

public class DatacapScannerServiceImpl
  implements DatacapScannerService
{
  private static final Logger LOGGER = Logger.getLogger(DatacapScannerServiceImpl.class);
  private DatacapScannerDao datacapScannerDao;
  
  public void setDatacapScannerDao(DatacapScannerDao datacapScannerDao)
  {
    this.datacapScannerDao = datacapScannerDao;
  }
  
  @Override
  public List<DatacapScanner> getAllScanner()
    throws ServiceException
  {
    List<DatacapScanner> scannerList = null;
    try
    {
      scannerList = datacapScannerDao.getAllScanner();
    }
    catch (WMSDaoException e)
    {
    	LOGGER.error("Error in " + DatacapScannerServiceImpl.class.getSimpleName() + ": " + e);
    }
    return scannerList;
  }
  
  @Override
  public DatacapScanner getScannerById(String pcName)
    throws ServiceException
  {
    List<DatacapScanner> scannerList = null;
    try
    {
      scannerList = datacapScannerDao.getScannerById(pcName);
    }
    catch (WMSDaoException e)
    {
      LOGGER.error("Error in " + DatacapScannerServiceImpl.class.getSimpleName() + ": " + e);
    }
    return scannerList.get(0);
  }
  
  @Override
  public boolean deleteScanner(String pcName)
    throws ServiceException
  {
    boolean isDeleted = false;
    try
    {
      isDeleted = datacapScannerDao.deleteScanner(pcName);
    }
    catch (WMSDaoException e)
    {
      LOGGER.error("Error in " + DatacapScannerServiceImpl.class.getSimpleName() + ": " + e);
    }
    return isDeleted;
  }
}
