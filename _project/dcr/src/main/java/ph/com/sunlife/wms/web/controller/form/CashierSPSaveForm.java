package ph.com.sunlife.wms.web.controller.form;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.FactoryUtils;
import org.apache.commons.collections.ListUtils;

public class CashierSPSaveForm extends CashierSPForm {

	@SuppressWarnings("unchecked")
	private List<NoteForm> noteList = ListUtils.lazyList(
			new ArrayList<NoteForm>(),
			FactoryUtils.instantiateFactory(NoteForm.class));

	public List<NoteForm> getNoteList() {
		return noteList;
	}

	public void setNoteList(List<NoteForm> noteList) {
		this.noteList = noteList;
	}

}
