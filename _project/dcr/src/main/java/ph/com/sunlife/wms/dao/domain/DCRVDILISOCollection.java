package ph.com.sunlife.wms.dao.domain;

import java.util.Date;

public class DCRVDILISOCollection extends Entity {

	private DCRVDIL dcrvdil = new DCRVDIL();
	
	private Date dcrDate;
	
	private Date pickupDate;
	
	private Long currencyId;
	
	private Long typeId;
	
	private double amount;

	public DCRVDIL getDcrvdil() {
		return dcrvdil;
	}

	public void setDcrvdil(DCRVDIL dcrvdil) {
		this.dcrvdil = dcrvdil;
	}
	
	public void setDcrvdil(Long dcrvdilId) {
		DCRVDIL dcrvdil = new DCRVDIL();
		dcrvdil.setId(dcrvdilId);
		this.setDcrvdil(dcrvdil);
	}

	public Date getDcrDate() {
		return dcrDate;
	}

	public void setDcrDate(Date dcrDate) {
		this.dcrDate = dcrDate;
	}

	public Date getPickupDate() {
		return pickupDate;
	}

	public void setPickupDate(Date pickupDate) {
		this.pickupDate = pickupDate;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public Long getCurrencyId() {
		return currencyId;
	}

	public void setCurrencyId(Long currencyId) {
		this.currencyId = currencyId;
	}

	public Long getTypeId() {
		return typeId;
	}

	public void setTypeId(Long typeId) {
		this.typeId = typeId;
	}

}
