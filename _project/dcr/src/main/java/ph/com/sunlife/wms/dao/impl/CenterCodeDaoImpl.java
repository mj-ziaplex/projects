package ph.com.sunlife.wms.dao.impl;

import java.util.List;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import ph.com.sunlife.wms.dao.CenterCodeDao;
import ph.com.sunlife.wms.dao.domain.CenterCode;
import ph.com.sunlife.wms.dao.exceptions.WMSDaoException;

public class CenterCodeDaoImpl extends SqlMapClientDaoSupport implements CenterCodeDao{

	private static final String NAMESPACE = "CenterCode";

	@Override
	public boolean createCenterCode(CenterCode cc) throws WMSDaoException {
		boolean success = false;
		try {
			getSqlMapClientTemplate().queryForObject(
					NAMESPACE + ".createCenterCode", cc);
			success = true;
		} catch (Exception ex) {
			throw new WMSDaoException(ex);
		}
		return success;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CenterCode> getAllCenterCode() throws WMSDaoException {
		try {
			List<CenterCode> list = (List<CenterCode>) getSqlMapClientTemplate()
					.queryForList(NAMESPACE + ".getAllCenterCode");
			return list;
		} catch (Exception ex) {
			throw new WMSDaoException(ex);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CenterCode> getCenterCodeById(String ccId) throws WMSDaoException {
		try {
			List<CenterCode> list = (List<CenterCode>) getSqlMapClientTemplate()
					.queryForList(NAMESPACE + ".getCenterCodeById", ccId);
			return list;
		} catch (Exception ex) {
			throw new WMSDaoException(ex);
		}
	}

	@Override
	public boolean updateCenterCode(CenterCode cc) throws WMSDaoException {
		try {
			int updatedRows = getSqlMapClientTemplate().update(
					NAMESPACE + ".updateCenterCode", cc);
			return updatedRows > 0;
		} catch (Exception ex) {
			throw new WMSDaoException(ex);
		}
	}

	@Override
	public boolean deleteCenterCode(String ccId) throws WMSDaoException {
		try {
			int deletedRows = getSqlMapClientTemplate().delete(
					NAMESPACE + ".deleteCenterCode", ccId);
			return deletedRows > 0;
		} catch (Exception ex) {
			throw new WMSDaoException(ex);
		}
	}

}
