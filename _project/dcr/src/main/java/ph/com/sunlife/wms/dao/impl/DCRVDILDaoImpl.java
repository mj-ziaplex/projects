package ph.com.sunlife.wms.dao.impl;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.time.DateUtils;
import org.springframework.orm.ibatis.SqlMapClientCallback;

import ph.com.sunlife.wms.dao.AbstractWMSSqlMapClientDaoSupport;
import ph.com.sunlife.wms.dao.DCRVDILDao;
import ph.com.sunlife.wms.dao.domain.DCR;
import ph.com.sunlife.wms.dao.domain.DCRCenterCoinCollection;
import ph.com.sunlife.wms.dao.domain.DCRDollarCoinCollection;
import ph.com.sunlife.wms.dao.domain.DCRDollarCoinExchange;
import ph.com.sunlife.wms.dao.domain.DCRVDIL;
import ph.com.sunlife.wms.dao.domain.DCRVDILISOCollection;
import ph.com.sunlife.wms.dao.domain.DCRVDILPRSeriesNumber;
import ph.com.sunlife.wms.dao.domain.Entity;
import ph.com.sunlife.wms.dao.exceptions.WMSDaoException;

import com.ibatis.sqlmap.client.SqlMapExecutor;
import org.apache.log4j.Logger;

public class DCRVDILDaoImpl extends AbstractWMSSqlMapClientDaoSupport<DCRVDIL> implements DCRVDILDao {

	private static final String NAMESPACE = "DCRVDIL";
        private static final Logger LOGGER = Logger.getLogger(DCRReversalDaoImpl.class);

	@SuppressWarnings("unchecked")
	@Override
	public DCRVDIL getByDCRCashierId(Long dcrCashierId) throws WMSDaoException {

		try {
			List<DCRVDIL> list = getSqlMapClientTemplate().queryForList(
					getNamespace() + ".getByDCRCashierId", dcrCashierId);

			if (CollectionUtils.isNotEmpty(list)) {
				return list.get(0);
			}
		} catch (Exception ex) {
			throw new WMSDaoException(ex);
		}

		return null;
	}

	@Override
	public DCRVDILISOCollection insertDcrVdilIsoCollection(
			DCRVDILISOCollection entity) throws WMSDaoException {
		try {
			Long id = (Long) getSqlMapClientTemplate().queryForObject(
					getNamespace() + ".insertDcrVdilIsoCollection", entity);
			entity.setId(id);
		} catch (Exception ex) {
			throw new WMSDaoException(ex);
		}
		return entity;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<DCRVDILISOCollection> getDcrVdilIsoCollections(Long dcrVdilId)
			throws WMSDaoException {

		List<DCRVDILISOCollection> list = null;
		try {
			list = getSqlMapClientTemplate().queryForList(
					getNamespace() + ".getDcrVdilIsoCollections", dcrVdilId);
		} catch (Exception ex) {
			throw new WMSDaoException(ex);
		}
		return list;
	}

	@Override
	public DCRVDILPRSeriesNumber insertDcrVdilPrSeriesNumber(
			DCRVDILPRSeriesNumber entity) throws WMSDaoException {
		try {
			Long id = (Long) getSqlMapClientTemplate().queryForObject(
					getNamespace() + ".insertDcrVdilPrSeriesNumber", entity);
			entity.setId(id);
		} catch (Exception ex) {
			throw new WMSDaoException(ex);
		}
		return entity;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<DCRVDILPRSeriesNumber> getDcrVdilPrSeriesNumbers(Long dcrVdilId)
			throws WMSDaoException {
		List<DCRVDILPRSeriesNumber> list = null;
		try {
			list = getSqlMapClientTemplate().queryForList(
					getNamespace() + ".getDcrVdilPrSeriesNumbers", dcrVdilId);
		} catch (Exception ex) {
			throw new WMSDaoException(ex);
		}
		return list;
	}

	@Override
	public boolean updateVdil(DCRVDIL entity) throws WMSDaoException {
		return this.update(entity, "updateVdil");
	}

	@Override
	public boolean updateDcrVdilPrSeriesNumber(DCRVDILPRSeriesNumber entity)
			throws WMSDaoException {
		return this.update(entity, "updateDcrVdilPrSeriesNumber");
	}

	@Override
	public boolean updateDcrVdilIsoCollection(DCRVDILISOCollection entity)
			throws WMSDaoException {
		return this.update(entity, "updateDcrVdilIsoCollection");
	}

	@Override
	protected String getNamespace() {
		return NAMESPACE;
	}

	private boolean update(Entity entity, String sqlMapId)
			throws WMSDaoException {
		boolean isSuccessful = false;
		try {
			getSqlMapClientTemplate().update(getNamespace() + "." + sqlMapId,
					entity);

			isSuccessful = true;
		} catch (Exception ex) {
			throw new WMSDaoException(ex);
		}
		return isSuccessful;
	}

	private int checkPreviousBusinessDayDifference(Date date, int marker)
			throws Exception {
		int count = (Integer) getSqlMapClientTemplate().queryForObject(
				getNamespace() + ".checkIfHoliday",
				DateUtils.addDays(date, marker));

		return count;

	}

    @SuppressWarnings("unchecked")
    public DCRDollarCoinCollection getDollarCoinCollectionByDcrId(Long dcrId) throws WMSDaoException {
        
        // Change according to Healthcheck
        // *Made variable global to have a return
        DCRDollarCoinCollection collection = null;
        try {
            if (dcrId != null) {
                List<DCRDollarCoinCollection> list = (List<DCRDollarCoinCollection>) getSqlMapClientTemplate()
                        .queryForList(getNamespace() + ".getPreviousDollarCoinCollectionByDcrCashierId", dcrId);

                if (CollectionUtils.isNotEmpty(list)) {
                    collection = new DCRDollarCoinCollection();
                    for (DCRDollarCoinCollection coll : list) {

                        if (collection.getDenom1() < coll.getDenom1()) {
                            collection.setDenom1(coll.getDenom1());
                        }

                        if (collection.getDenom10c() < coll.getDenom10c()) {
                            collection.setDenom10c(coll.getDenom10c());
                        }

                        if (collection.getDenom1c() < coll.getDenom1c()) {
                            collection.setDenom1c(coll.getDenom1c());
                        }

                        if (collection.getDenom25c() < coll.getDenom25c()) {
                            collection.setDenom25c(coll.getDenom25c());
                        }

                        if (collection.getDenom50c() < coll.getDenom50c()) {
                            collection.setDenom50c(coll.getDenom50c());
                        }

                        if (collection.getDenom5c() < coll.getDenom5c()) {
                            collection.setDenom5c(coll.getDenom5c());
                        }
                    }
                }
            }
        } catch (Exception ex) {
            // Change according to Healthcheck
            // *Problem was identified no logging in log4j
            // *Change was add log4j
            LOGGER.error("Error in getting dollor coin collection by dcr id: ", ex);
        }
        return collection;
    }

	@SuppressWarnings("unchecked")
	public Long getPreviousDayDcrId(DCR entity) throws Exception {
		Date dcrDate = entity.getDcrDate();

		int marker = -1;
		int count = 1;
		while (count == 1) {
			count = checkPreviousBusinessDayDifference(dcrDate, marker);

			if (count == 0) {
				break;
			}

			marker--;
		}
		entity.setMarker(marker);

		List<Long> dcrIds = (List<Long>) getSqlMapClientTemplate()
				.queryForList(getNamespace() + ".getPreviousDayDcrId", entity);

		Long dcrId = null;

		if (CollectionUtils.isNotEmpty(dcrIds)) {
			dcrId = dcrIds.get(0);
		}
		return dcrId;
	}

	public DCRDollarCoinCollection saveDCRDollarCoinCollection(
			final DCRDollarCoinCollection coll) throws WMSDaoException {
		try {
			Long id = (Long) getSqlMapClientTemplate().execute(
					new SqlMapClientCallback() {

						@Override
						public Object doInSqlMapClient(SqlMapExecutor executor)
								throws SQLException {
							Long id = (Long) executor.queryForObject(
									getNamespace()
											+ ".insertDollarCoinCollection",
									coll);
							return id;
						}
					});

			coll.setId(id);

			return coll;
		} catch (Exception e) {
			throw new WMSDaoException(e);
		}
	}

	@Override
	public boolean updateNetDCRDollarCoinCollection(
			final DCRDollarCoinCollection coll) throws WMSDaoException {
		boolean isUpdated = false;
		try {
			getSqlMapClientTemplate().update(
					getNamespace() + ".updateDollarCoinCollection", coll);

			isUpdated = true;
		} catch (Exception e) {
			throw new WMSDaoException(e);
		}

		return isUpdated;
	}

	@SuppressWarnings("unchecked")
	@Override
	public DCRDollarCoinCollection getDollarCoinCollectionByDcrCashierId(
			Long dcrCashierId) throws WMSDaoException {
		DCRDollarCoinCollection coll = null;

		try {
			List<DCRDollarCoinCollection> list = (List<DCRDollarCoinCollection>) getSqlMapClientTemplate()
					.queryForList(
							getNamespace()
									+ ".getDollarCoinCollectionByDcrCashierId",
							dcrCashierId);

			if (CollectionUtils.isNotEmpty(list)) {
				coll = list.get(0);
			}
		} catch (Exception ex) {
			throw new WMSDaoException(ex);
		}

		return coll;
	}

	@Override
	public DCRDollarCoinExchange createDCRDollarCoinExchange(
			DCRDollarCoinExchange entity) throws WMSDaoException {

		Long dcrVdilId = entity.getDcrvdil().getId();
		try {
			Long id = (Long) getSqlMapClientTemplate().queryForObject(
					getNamespace() + ".saveDollarCoinExchange", dcrVdilId);
			entity.setId(id);
			return entity;
		} catch (Exception ex) {
			throw new WMSDaoException(ex);
		}
	}

	@Override
	public boolean updateDCRDollarCoinExchange(DCRDollarCoinExchange entity)
			throws WMSDaoException {
		try {
			int rowsUpdated = getSqlMapClientTemplate().update(
					getNamespace() + ".updateDollarCoinExchange", entity);
			return (rowsUpdated != 0);
		} catch (Exception ex) {
			throw new WMSDaoException(ex);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public DCRDollarCoinExchange getDollarCoinExchangeByCashierId(
			Long dcrCashierId) throws WMSDaoException {
		try {
			List<DCRDollarCoinExchange> list = (List<DCRDollarCoinExchange>) getSqlMapClientTemplate()
					.queryForList(
							getNamespace()
									+ ".getDollarCoinExchangeByCashierId",
							dcrCashierId);

			if (CollectionUtils.isNotEmpty(list)) {
				return (DCRDollarCoinExchange) CollectionUtils.get(list, 0);
			}
		} catch (Exception ex) {
			throw new WMSDaoException(ex);
		}

		return null;
	}

	@SuppressWarnings("unchecked")
	public DCRCenterCoinCollection getCurrentCenterCoinCollection(String ccId)
			throws WMSDaoException {

		DCRCenterCoinCollection thisCollection = null;
		try {
			List<DCRCenterCoinCollection> list = (List<DCRCenterCoinCollection>) getSqlMapClientTemplate()
					.queryForList(
							getNamespace() + ".getCurrentCenterCoinCollection",
							ccId);

			if (CollectionUtils.isNotEmpty(list)) {
				thisCollection = list.get(0);
			}
		} catch (Exception ex) {
			throw new WMSDaoException(ex);
		}
		return thisCollection;
	}

	@Override
	public DCRCenterCoinCollection createCurrentCenterCoinCollection(String ccId)
			throws WMSDaoException {

		DCRCenterCoinCollection thisCollection = null;
		try {
			Long id = (Long) getSqlMapClientTemplate()
					.queryForObject(
							getNamespace()
									+ ".createCurrentCenterCoinCollection",
							ccId);

			if (id != null) {
				thisCollection = new DCRCenterCoinCollection();
				thisCollection.setId(id);
				thisCollection.setDateUpdated(new Date());
				thisCollection.setCcId(ccId);
			}
			return thisCollection;
		} catch (Exception ex) {
			throw new WMSDaoException(ex);
		}
	}

	@Override
	public boolean updateCurrentCenterCoinCollection(
			DCRCenterCoinCollection thisCollection) throws WMSDaoException {

		boolean isSuccessful = false;

		try {
			int rowsUpdated = getSqlMapClientTemplate().update(
					getNamespace() + ".updateCurrentCenterCoinCollection",
					thisCollection);

			isSuccessful = (rowsUpdated != 0);
			return isSuccessful;
		} catch (Exception ex) {
			throw new WMSDaoException(ex);
		}

	}
}
