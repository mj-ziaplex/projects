package ph.com.sunlife.wms.dao.domain;

/**
 * This is the enum representing all valid check types. These data come from
 * IPAC.
 * 
 * @author Zainal Limpao
 * 
 */
public enum CheckType {

	/**
	 * The On_Us Check.
	 */
	ON_US(1L, "On Us", "OUC"),

	/**
	 * The Local Check.
	 */
	LOCAL(2L, "Local", "L/C"),

	/**
	 * The Regional Check.
	 */
	REGIONAL(3L, "Regional", "R/C"),

	/**
	 * The Dollar Check.
	 */
	DOLLARCHECK(4L, "Dollar Cheque", "DCK"),

	/**
	 * The US Check Drawn in Manila.
	 */
	USCD_MANILA(5L, "US Check Drawn in Manila", "CDM"),

	/**
	 * The US Check Drawn Outside of Philippines.
	 */
	USCD_OUTSIDEPH(6L, "US CHECK DRAWN OUTSIDE PH", "COP"),

	/**
	 * The Over the Counter Check.
	 */
	BANK_OTC(7L, "BANK OTC CHECK PAYMENT (DOLLAR)", "BCU"),

	/**
	 * The Out of Town Check.
	 */
	OT(8L, "OUT-OF-TOWN", "O/C");

	private Long id;

	private String checkTypeName;

	private String paySubCode;

	private CheckType(Long id, String name, String paySubCode) {
		this.checkTypeName = name;
		this.id = id;
		this.paySubCode = paySubCode;
	}

	public String getCheckTypeName() {
		return checkTypeName;
	}

	public Long getId() {
		return id;
	}

	public String getPaySubCode() {
		return paySubCode;
	}

	public static CheckType getById(Long id) {
		for (CheckType checkType : CheckType.values()) {
			if (checkType.getId().equals(id)) {
				return checkType;
			}
		}
		return null;
	}

	public String getName() {
		return name();
	}

}
