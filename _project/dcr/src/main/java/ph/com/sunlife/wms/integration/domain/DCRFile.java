package ph.com.sunlife.wms.integration.domain;

import java.io.Serializable;
import java.util.Date;

public class DCRFile implements Serializable {

	private static final long serialVersionUID = 1677588322600033942L;

	private String docTypeId;
	
	private String fileName;
	
	private String fileId;
	
	private byte[] fileData;
	
	private String contentType;
	
	private Date dateCreated;
	
	// ReportTitle: DCR SLOCP
	private String reportTitle;
	
	// SourceofFunds: DCR Currency Peso
	private String sourceOfFunds;
	
	// CorrespondenceType: DCR Type Cheque Local
	private String correspondenceType;
	
	// TemplateID: DCR Preneed
	private String templateID;
	
	// LastModifier: WC01
	private String lastModifier;
	
	// AssignedUser: wc01
	private String assignedUser;
	
	private int fileSize;
	
	public int getFileSize() {
		return fileSize;
	}

	public void setFileSize(int fileSize) {
		this.fileSize = fileSize;
	}

	public String getAssignedUser() {
		return assignedUser;
	}

	public void setAssignedUser(String assignedUser) {
		this.assignedUser = assignedUser;
	}

	public String getLastModifier() {
		return lastModifier;
	}

	public void setLastModifier(String lastModifier) {
		this.lastModifier = lastModifier;
	}

	public String getReportTitle() {
		return reportTitle;
	}

	public void setReportTitle(String reportTitle) {
		this.reportTitle = reportTitle;
	}

	public String getSourceOfFunds() {
		return sourceOfFunds;
	}

	public void setSourceOfFunds(String sourceOfFunds) {
		this.sourceOfFunds = sourceOfFunds;
	}

	public String getCorrespondenceType() {
		return correspondenceType;
	}

	public void setCorrespondenceType(String correspondenceType) {
		this.correspondenceType = correspondenceType;
	}

	public String getTemplateID() {
		return templateID;
	}

	public void setTemplateID(String templateID) {
		this.templateID = templateID;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFileId() {
		return fileId;
	}

	public void setFileId(String fileId) {
		this.fileId = fileId;
	}

	public byte[] getFileData() {
		return fileData;
	}

	public void setFileData(byte[] fileData) {
		this.fileData = fileData;
	}

	public String getDocTypeId() {
		return docTypeId;
	}

	public void setDocTypeId(String docTypeId) {
		this.docTypeId = docTypeId;
	}

}
