package ph.com.sunlife.wms.integration.client;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.sunlife.ingeniumutil.base.IngeniumBaseUtil;
import com.sunlife.ingeniumutil.container.UserCollectionsInfo;
import com.sunlife.ingeniumutil.exception.IngeniumException;
import com.sunlife.ingeniumutil.request.TXLifeReq;
import com.sunlife.ingeniumutil.request.UserCollectionsTotalData;
import com.sunlife.ingeniumutil.response.TXLifeRes;
import com.sunlife.ingeniumutil.txlife.jaxws.IngeniumClient;

public class UserCollectionsTotalUtil extends IngeniumBaseUtil {

	private static final Logger LOGGER = Logger
			.getLogger(UserCollectionsTotalUtil.class);

	private static final String INQUIRY_USER_COLLECTIONS = "UserCollectionsTotal";

	public UserCollectionsTotalUtil(String userLoginName, String userCoId,
			String location, String wsdlLocation) {
		super(userLoginName, userCoId, location, wsdlLocation);
	}

	public UserCollectionsTotalUtil(String userLoginName, String userCoId,
			String crypType, String location, String wsdlLocation) {
		super(userLoginName, userCoId, crypType, location, wsdlLocation);
	}

	public UserCollectionsInfo userCollectionsInquiry(UserCollectionsInfo info,
			List<String> outMessages) {
		TXLifeReq txLifeReq = createTXLifeReq(INQUIRY_USER_COLLECTIONS);
		UserCollectionsTotalData userCollectionsTotalData = txLifeReq
				.getTXLifeRequest().getOLifE().getUserCollectionsTotalData();

		userCollectionsTotalData.getMirCommonFields().setMirProcDt(
				info.getProcessDate());
		userCollectionsTotalData.getMirCommonFields().setMirCrcyCd(
				info.getCurrencyId());
		userCollectionsTotalData.getMirCommonFields().setMirUserId(
				info.getUserId());

		TXLifeRes txtLifeRes = null;
		try {
			txtLifeRes = IngeniumClient.callTXLife(txLifeReq, outMessages,
					getWsdlLocation());
			com.sunlife.ingeniumutil.response.UserCollectionsTotalData respData = txtLifeRes
					.getTXLifeResponse().getOLifE()
					.getUserCollectionsTotalData();
			info.setTotalCollectionAmount(Double.valueOf(respData
					.getMirDvTtlCollAmt()));
		} catch (IngeniumException e) {
			LOGGER.error(e);
			info.setErrorMessage(e.getMessage());
		} catch (Exception e) {
			LOGGER.error(e);
			info.setErrorMessage(e.getMessage());
		}

		return info;
	}

	@Override
	public void setPswd(String pswd) {
		if (StringUtils.isNotEmpty(pswd)) {
			super.setPswd(pswd);
		}
	}
	
	
}