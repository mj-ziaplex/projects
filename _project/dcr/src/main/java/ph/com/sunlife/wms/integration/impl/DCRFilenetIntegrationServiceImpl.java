package ph.com.sunlife.wms.integration.impl;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.InitializingBean;

import ph.com.sunlife.wms.integration.DCRFilenetIntegrationService;
import ph.com.sunlife.wms.integration.domain.DCRFile;
import ph.com.sunlife.wms.integration.exceptions.IntegrationException;
import ph.com.sunlife.wms.util.WMSDateUtil;

import com.filenet.wcm.api.BadReferenceException;
import com.filenet.wcm.api.BaseObject;
import com.filenet.wcm.api.BaseObjects;
import com.filenet.wcm.api.ClassDescription;
import com.filenet.wcm.api.Document;
import com.filenet.wcm.api.Folder;
import com.filenet.wcm.api.ObjectFactory;
import com.filenet.wcm.api.ObjectStore;
import com.filenet.wcm.api.Properties;
import com.filenet.wcm.api.Property;
import com.filenet.wcm.api.PropertyNotFoundException;
import com.filenet.wcm.api.Session;
import com.filenet.wcm.api.TransportInputStream;
import com.filenet.wcm.api.Value;
import com.filenet.wcm.api.Values;

import filenet.vw.api.VWException;
import filenet.vw.api.VWFetchType;
import filenet.vw.api.VWQueue;
import filenet.vw.api.VWQueueQuery;
import filenet.vw.api.VWSession;
import filenet.vw.api.VWWorkObject;

/**
 * The implementing class for {@link DCRFilenetIntegrationService}.
 *
 * @author Zainal Limpao
 * @author Edgardo Cunanan
 * @author Crisseljess Mendoza
 */
public class DCRFilenetIntegrationServiceImpl implements DCRFilenetIntegrationService, InitializingBean {

    private Logger LOGGER = Logger.getLogger(DCRFilenetIntegrationServiceImpl.class);
    private static final String PROPERTY_WF_ACTION = "WFAction";
    private static final String PROPERTY_Q_NAME = "QName";
    private static final String PROPERTY_Q_DATE = "QDate";
    private static final String PROPERTY_PROCESS_STATUS = "ProcessStatus";
    private static final String PROPERTY_DCR_NUMBER = "DCRNumber";
    private static final String PROPERTY_DCR_DATE = "DCRDate";
    private static final String PROPERTY_DOC_CAPSITE_ID = "DocCapsiteId";
    private static final String PROPERTY_AUDIT_TAG = "AuditTag";
    private static final String FILENET_CLASS_NAME = "DCRFolder";
    private String routerURL = "WMSST2_PE01";
    private String rootFolder = "/TestDCR";
    // Change according to Healthcheck
    // *Problem was identified due to hard coded values credentials
    // *Change was to set as empty string for initial value
    private String userName;
    private String password;
    private String filenetUrl;
    private String objectStoreName;
    private boolean isTest = false;

    public void setRootFolder(String rootFolder) {
        this.rootFolder = rootFolder;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setFilenetUrl(String filenetUrl) {
        this.filenetUrl = filenetUrl;
    }

    public void setTest(boolean isTest) {
        this.isTest = isTest;
    }

    public List<DCRFile> getValidatedDepositSlipFiles(String dcrFolderPath) throws IntegrationException {
        try {
            Folder folder = (Folder) getFNObjectByIdOrPath(BaseObject.TYPE_FOLDER, this.rootFolder + "/" + dcrFolderPath);

            BaseObjects contents = folder.getContainees();

            if (CollectionUtils.isNotEmpty(contents)) {
                List<DCRFile> validatedDepositSlips = new ArrayList<DCRFile>();
                for (Object obj : contents) {
                    if (obj instanceof Document) {
                        Document doc = (Document) obj;

                        String docTypeIdPropertyValue = null;
                        try {
                            docTypeIdPropertyValue = (String) doc.getPropertyValue("DocTypeID");
                        } catch (PropertyNotFoundException ex) {
                            // Change according to Healthcheck
                            // *Problem was identified as inconsistent logging
                            // *Change was to set logging as error instead of debug
                            LOGGER.error("This file has no DocTypeId property: ", ex);
                        }

                        if (StringUtils.containsIgnoreCase(docTypeIdPropertyValue, "VDS")
                                || StringUtils.containsIgnoreCase(docTypeIdPropertyValue, "Vault")
                                || StringUtils.containsIgnoreCase(docTypeIdPropertyValue, "VDIL")) {
                            DCRFile vds = new DCRFile();
                            vds.setDocTypeId(docTypeIdPropertyValue);
                            vds.setContentType((String) doc.getPropertyValue("MimeType"));
                            vds.setFileId(doc.getId());

                            vds.setCorrespondenceType((String) doc.getPropertyValue("CorrespondenceType"));
                            vds.setFileName((String) doc.getPropertyValue("DocumentTitle"));
                            vds.setLastModifier((String) doc.getPropertyValue("LastModifier"));
                            vds.setReportTitle((String) doc.getPropertyValue("ReportTitle"));
                            vds.setSourceOfFunds((String) doc.getPropertyValue("SourceOfFunds"));
                            vds.setTemplateID((String) doc.getPropertyValue("TemplateID"));
                            vds.setAssignedUser((String) doc.getPropertyValue("AssignedUser"));

                            try {
                                Date dateCreated = (Date) doc.getPropertyValue("DateCreated");
                                vds.setDateCreated(dateCreated);
                            } catch (Exception ex) {
                                // Change according to Healthcheck
                                // *Problem was identified as inconsistent logging
                                // *Change was add exception messsage on log
                                LOGGER.error("DateCreated field is not a valid Date Object: ", ex);
                            }
                            
                            validatedDepositSlips.add(vds);
                        }
                    }
                }

                return validatedDepositSlips;
            }
        } catch (Exception e) {
            throw new IntegrationException(e);
        }

        return null;
    }

    /*
     * (non-Javadoc)
     * 
     * @see ph.com.sunlife.wms.integration.DCRFilenetIntegrationService#
     * createDcrOnFilenet(java.util.Date, java.lang.Long, java.lang.String)
     */
    @Override
    public String createDcrOnFilenet(
            Date dcrDate,
            Long dcrId,
            String docCapsiteId) throws IntegrationException {

        Folder rootFilenetFolder = null;
        Folder dcrFolder = null;

        Map<String, Object> folderProps = new HashMap<String, Object>();
        folderProps.put(PROPERTY_AUDIT_TAG, false);
        folderProps.put(PROPERTY_DOC_CAPSITE_ID, docCapsiteId);
        folderProps.put(PROPERTY_DCR_DATE, dcrDate);
        folderProps.put(PROPERTY_DCR_NUMBER, String.valueOf(dcrId));
        folderProps.put(PROPERTY_PROCESS_STATUS, "NEW");
        folderProps.put(PROPERTY_Q_DATE, dcrDate);
        folderProps.put(PROPERTY_Q_NAME, "DCRCashierRecon");
        folderProps.put(PROPERTY_WF_ACTION, "1");

        String dcrDateStr = StringUtils.upperCase(WMSDateUtil.toFormattedDateStr(dcrDate));

        String workObjectNumber = null;
        if (!isTest) {
            try {
                StringBuilder path = new StringBuilder();
                path.append("/").append(FILENET_CLASS_NAME).append("/").append(dcrDateStr).append(docCapsiteId);

                rootFilenetFolder = (Folder) getFNObjectByIdOrPath(BaseObject.TYPE_FOLDER, rootFolder);
                dcrFolder = (Folder) getFNObjectByIdOrPath(BaseObject.TYPE_FOLDER, path.toString());
                dcrFolder.getName();
            } catch (BadReferenceException e) {
                // Change according to Healthcheck
                // *Problem was no logging on catch and a value was set
                // *Change was add exception logging
                LOGGER.error("Error on create DCR folder not found: ", e);
                dcrFolder = this.createNewFolder(dcrDateStr, docCapsiteId, rootFilenetFolder, dcrFolder, folderProps);
            } catch (Exception e1) {
                LOGGER.error("Error on create DCR folder not found: ", e1);
                throw new IntegrationException(e1);
            }

            workObjectNumber = dcrFolder.getId();
        }

        return workObjectNumber;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * ph.com.sunlife.wms.integration.DCRFilenetIntegrationService#approveStep
     * (java.lang.String, java.lang.String, java.lang.String)
     */
    public void approveStep(String response, String wObName, String queueName)
            throws IntegrationException {
        VWSession vwSession;
        try {
            vwSession = getVWSession();

            VWQueue queue = vwSession.getQueue(queueName);
            String filterStr = "F_Subject = '" + wObName + "'";
            VWQueueQuery query = getVWQueryForWO(filterStr, queue);
            VWWorkObject vwObject;
            if (query.hasNext()) {

                vwObject = (VWWorkObject) query.next();

                vwObject.doLock(true);
                if (!response.isEmpty() && response != null) {
                    vwObject.setSelectedResponse(response);
                }
                vwObject.doDispatch();
                vwSession.logoff();
            }

        } catch (VWException e) {
            throw new IntegrationException(e);
        }

    }

    private VWQueueQuery getVWQueryForWO(String filterStr, VWQueue queue)
            throws VWException {
        VWQueueQuery query = queue.createQuery(null, null, null,
                VWQueue.QUERY_READ_LOCKED, filterStr, null,
                VWFetchType.FETCH_TYPE_WORKOBJECT);
        return query;
    }

    private VWSession getVWSession() throws VWException {
        System.setProperty(
                "java.security.auth.login.config",
                "C:\\workspace-test\\WMS-Main\\WMS-Integration\\src\\main\\resources\\jaas.conf.WSI");
        VWSession vwSession = new VWSession();
        vwSession.setBootstrapCEURI(filenetUrl);
        vwSession.logon(userName, password, routerURL);
        return vwSession;
    }

    @SuppressWarnings("rawtypes")
    private Folder createNewFolder(String dateStr, String docCapsiteId,
            Folder rootFilenetFolder, Folder dcrFolder,
            Map<String, Object> folderProps) throws IntegrationException {
        try {
            dcrFolder = rootFilenetFolder.addSubFolder(dateStr + docCapsiteId,
                    FILENET_CLASS_NAME,
                    createProperties((HashMap) folderProps), null);
        } catch (Exception e1) {
            throw new IntegrationException(e1);
        }
        return dcrFolder;
    }

    private void attachFileToFolder(Folder dcrFolder, Document document,
            TransportInputStream t) {
        document.setContent(t, true, true);
        document.file(dcrFolder, true);
    }

    /*
     * (non-Javadoc)
     * 
     * @see ph.com.sunlife.wms.integration.DCRFilenetIntegrationService#
     * attachFileToWorkItem(java.util.Date, java.lang.String, java.lang.String,
     * byte[])
     */
    public String attachFileToWorkItem(Date dcrDate, String docCapsiteId,
            String customFileName, byte[] file) throws IntegrationException {

        String folderName = WMSDateUtil.toFormattedDateStr(dcrDate)
                + docCapsiteId;

        String documentId = null;
        InputStream is = null;
        try {
            Folder folder = (Folder) getFNObjectByIdOrPath(
                    BaseObject.TYPE_FOLDER, rootFolder + "/" + folderName);
            ObjectStore os = getObjectStore(objectStoreName,
                    createSession(userName, password));
            Document doc = createFileNetDoc(os, customFileName);

            is = new ByteArrayInputStream(file);
            TransportInputStream tis = new TransportInputStream(is);
            tis.setFilename(customFileName);

            this.attachFileToFolder(folder, doc, tis);

            documentId = doc.getId();
        } catch (Exception e) {
            throw new IntegrationException(e);
        } finally {
            IOUtils.closeQuietly(is);
        }

        return documentId;
    }

    @Override
    public void removeFileFromWorkItem(Date dcrDate, String docCapsiteId,
            String documentId) throws IntegrationException {

        String dcrFolderPath = rootFolder + "/"
                + WMSDateUtil.toFormattedDateStr(dcrDate) + docCapsiteId;

        try {
            Folder dcrFolder = (Folder) getFNObjectByIdOrPath(
                    BaseObject.TYPE_FOLDER, dcrFolderPath);

            Document document = (Document) getFNObjectByIdOrPath(
                    BaseObject.TYPE_DOCUMENT, documentId);

            document.unfile(dcrFolder);
        } catch (Exception e) {
            throw new IntegrationException(e);
        }

    }

    public DCRFile getFileFromFilenet(String id) throws IntegrationException {
        DCRFile dcrFile = null;
        try {
            Document retrievedDoc = (Document) getFNObjectByIdOrPath(BaseObject.TYPE_DOCUMENT, id);
            if (retrievedDoc != null) {
                byte[] fileData = IOUtils.toByteArray(retrievedDoc.getContent());

                String fileName = retrievedDoc.getName();
                Date dateCreated = retrievedDoc.getPropertyDateValue(Property.DATE_CREATED);
                String fileId = retrievedDoc.getId();
                String contentType = retrievedDoc.getPropertyStringValue("MimeType");

                dcrFile = new DCRFile();
                dcrFile.setDateCreated(dateCreated);
                dcrFile.setFileData(fileData);
                dcrFile.setFileId(fileId);
                dcrFile.setFileName(fileName);
                dcrFile.setContentType(contentType);
                dcrFile.setFileSize(fileData.length);
            }

        } catch (Exception e) {
            // Change according to Healthcheck
            // *Problem was identified as inconsistent logging
            // *Change was add exception messsage on log
            LOGGER.error("Error in getting file in CE Server: ", e);
        }
        return dcrFile;
    }

    @SuppressWarnings("unchecked")
    private Document createFileNetDoc(ObjectStore os, String filenetDisplayName) {
        Properties docProps = ObjectFactory.getProperties();
        Property docName = ObjectFactory.getProperty(Property.DOCUMENT_TITLE);
        docName.setValue(filenetDisplayName);
        docProps.add(docName);

        Document document = (Document) os.createObject(
                ClassDescription.DOCUMENT, docProps, null);
        return document;
    }

    @Override
    public String copyFileToWorkItem(Date dcrDate, String docCapsiteId,
            String documentId) throws IntegrationException {
        try {
            Document doc = (Document) getFNObjectByIdOrPath(
                    BaseObject.TYPE_DOCUMENT, documentId);
            String dcrFolderPath = rootFolder + "/"
                    + WMSDateUtil.toFormattedDateStr(dcrDate) + docCapsiteId;
            Folder folder = (Folder) getFNObjectByIdOrPath(
                    BaseObject.TYPE_FOLDER, dcrFolderPath);

            doc.file(folder, true);
            String docId = doc.getId();
            return docId;
        } catch (Exception e) {
            throw new IntegrationException(e);
        }
    }

    private ObjectStore getObjectStore(String name, Session session) {
        System.setProperty("java.security.auth.login.config", "jaas.conf.WSI");
        return ObjectFactory.getObjectStore(name, session);
    }

    private Session createSession(String userName, String password) {
        Session s = null;
        s = ObjectFactory.getSession("MyApp", Session.DEFAULT, userName,
                password);
        s.setRemoteServerUrl(filenetUrl);
        s.setRemoteServerUploadUrl(filenetUrl);
        s.setRemoteServerDownloadUrl(filenetUrl);
        return s;
    }

    public BaseObject getFNObjectByIdOrPath(int objType, String idOrPath)
            throws Exception {
        ObjectStore objectStore = ObjectFactory.getObjectStore(objectStoreName,
                createSession(userName, password));
        BaseObject bos = (BaseObject) objectStore.getObject(objType, idOrPath);
        return bos;
    }

    @SuppressWarnings({"rawtypes", "unused", "unchecked"})
    private Properties createProperties(HashMap hm) throws Exception {
        Properties propProperties = ObjectFactory.getProperties();
        Value v1 = null;
        Iterator it = hm.keySet().iterator();

        if (hm == null) {
            throw new Exception("Missing Parameter: HashMap Object");
        }

        while (it.hasNext()) {
            String key = (String) it.next();
            Property propProperty = ObjectFactory.getProperty(key);
            if (hm.get(key) instanceof String) {
                String strPropValue = (String) hm.get(key);
                propProperty.setValue(strPropValue);
                propProperties.add(propProperty);
            } else if (hm.get(key) instanceof Integer) {
                Integer intPropValue = (Integer) hm.get(key);
                propProperty.setValue(intPropValue);
                propProperties.add(propProperty);
            } else if (hm.get(key) instanceof Double) {
                Double dblPropValue = (Double) hm.get(key);
                propProperty.setValue(dblPropValue);
                propProperties.add(propProperty);
            } else if (hm.get(key) instanceof Boolean) {
                Boolean blnPropValue = (Boolean) hm.get(key);
                propProperty.setValue(blnPropValue);
                propProperties.add(propProperty);
            } else if (hm.get(key) instanceof Date) {
                Date dtPropValue = (Date) hm.get(key);
                propProperty.setValue(dtPropValue);
                propProperties.add(propProperty);
            } else if (hm.get(key) instanceof String[]) {
                String[] strPropValue = (String[]) hm.get(key);
                Values vs = ObjectFactory.getValues();
                for (int i = 0; i < strPropValue.length; i++) {
                    v1 = ObjectFactory.getValue();
                    v1.setValue(strPropValue[i]);
                    vs.add(v1);
                }
                propProperty.setValue(vs);
                propProperties.add(propProperty);
            } else if (hm.get(key) instanceof int[]) {
                int[] intPropValue = (int[]) hm.get(key);
                Values vs = ObjectFactory.getValues();
                for (int i = 0; i < intPropValue.length; i++) {
                    v1 = ObjectFactory.getValue();
                    v1.setValue(intPropValue[i]);
                    vs.add(v1);
                }
                propProperty.setValue(vs);
                propProperties.add(propProperty);
            } else if (hm.get(key) instanceof float[]) {
                float[] fltPropValue = (float[]) hm.get(key);
                Values vs = ObjectFactory.getValues();

                for (int i = 0; i < fltPropValue.length; i++) {
                    v1 = ObjectFactory.getValue();
                    v1.setValue(fltPropValue[i]);
                    vs.add(v1);
                }
                propProperty.setValue(vs);
                propProperties.add(propProperty);
            } else if (hm.get(key) instanceof boolean[]) {
                boolean[] blnPropValue = (boolean[]) hm.get(key);
                Values vs = ObjectFactory.getValues();
                for (int i = 0; i < blnPropValue.length; i++) {
                    v1 = ObjectFactory.getValue();
                    v1.setValue(blnPropValue[i]);
                    vs.add(v1);
                }
                propProperty.setValue(vs);
                propProperties.add(propProperty);
            } else if (hm.get(key) instanceof Date[]) {
                Date[] dtPropValue = (Date[]) hm.get(key);
                Values vs = ObjectFactory.getValues();
                for (int i = 0; i < dtPropValue.length; i++) {
                    v1 = ObjectFactory.getValue();
                    v1.setValue(dtPropValue[i]);
                    vs.add(v1);
                }
                propProperty.setValue(vs);
                propProperties.add(propProperty);
            }
        }
        return propProperties;
    }

    public void setRouterURL(String routerURL) {
        this.routerURL = routerURL;
    }

    public void setObjectStoreName(String objectStoreName) {
        this.objectStoreName = objectStoreName;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        if (StringUtils.containsIgnoreCase(routerURL, "cemp:")) {
            setRouterURL(StringUtils.remove(routerURL, "cemp:"));
        }
    }

    public DCRFile getFileFromFilenetInfo(String id) throws IntegrationException {
        DCRFile dcrFile = null;
        try {
            
            Document retrievedDoc = (Document) getFNObjectByIdOrPath(BaseObject.TYPE_DOCUMENT, id);
            if (retrievedDoc != null) {
                String fileName = retrievedDoc.getName();
                Date dateCreated = retrievedDoc.getPropertyDateValue(Property.DATE_CREATED);
                String fileId = retrievedDoc.getId();
                String contentType = retrievedDoc.getPropertyStringValue("MimeType");

                dcrFile = new DCRFile();
                dcrFile.setDateCreated(dateCreated);
                dcrFile.setFileId(fileId);
                dcrFile.setFileName(fileName);
                dcrFile.setContentType(contentType);
            }

        } catch (Exception e) {
            // Change according to Healthcheck
            // *Problem was identified as inconsistent logging
            // *Change was add exception messsage on log
            LOGGER.error("Error in getting file info: ", e);
        }
        return dcrFile;
    }
}
