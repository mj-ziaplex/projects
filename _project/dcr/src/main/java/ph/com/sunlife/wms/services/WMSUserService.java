package ph.com.sunlife.wms.services;

import java.util.List;

import ph.com.sunlife.wms.dao.domain.HubCCNBO;
import ph.com.sunlife.wms.dao.domain.HubCCNBOUserGroup;
import ph.com.sunlife.wms.dao.domain.UserGroup;
import ph.com.sunlife.wms.dao.domain.UserGroupHub;
import ph.com.sunlife.wms.dao.domain.WMSUser;
import ph.com.sunlife.wms.services.bo.AdminWmsUserBO;
import ph.com.sunlife.wms.services.exception.ServiceException;

public interface WMSUserService {
	
	boolean createWMSUser(WMSUser user) throws ServiceException;

	List<WMSUser> getAllWMSUser() throws ServiceException;

	WMSUser getWMSUserById(String userId) throws ServiceException;

	boolean updateWMSUser(WMSUser user) throws ServiceException;

	boolean deleteWMSUser(String userId) throws ServiceException;
	
//	boolean updateWMSUserLogStatus(WMSUser user, boolean isLogged) throws ServiceException;

	List<UserGroupHub> getAllUserGroupHubList() throws ServiceException;
	
	List<UserGroupHub> getUserGroupHubList(String userId) throws ServiceException;
	
	List<HubCCNBOUserGroup> getUserGroupHubByUserId(HubCCNBOUserGroup hub) throws ServiceException;
	
	boolean createUserGroupHub(HubCCNBOUserGroup hub) throws ServiceException;
	
	boolean updateUserGroupHub(HubCCNBOUserGroup hub) throws ServiceException;
	
	boolean createUserGroup(UserGroup group) throws ServiceException;
	
	boolean deleteUserGroup(UserGroup group) throws ServiceException;
	
	List<HubCCNBO> getHubCCNBO(HubCCNBO nbo) throws ServiceException;
	
	boolean deleteUserGroupHub(HubCCNBOUserGroup hub) throws ServiceException;
	
	void doWMSUserCreate(AdminWmsUserBO adminWmsUserBO, String sessUser) throws ServiceException;

	void doWMSUserUpdate(AdminWmsUserBO adminWmsUserBO, String sessUser) throws ServiceException;
}
