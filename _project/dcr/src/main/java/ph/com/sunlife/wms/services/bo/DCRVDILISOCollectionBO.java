package ph.com.sunlife.wms.services.bo;

import java.util.Date;

import ph.com.sunlife.wms.util.WMSDateUtil;

public class DCRVDILISOCollectionBO {

	private Long id;
	
	private DCRVDILBO dcrvdil;
	
	private Date dcrDate;
	
	private Date pickupDate;
	
	private Long currencyId;
	
	private Long typeId;
	
	private double amount;

	public String getDcrDateStr(){
		if (dcrDate != null) {
			return WMSDateUtil.toFormattedDateStr(dcrDate);
		}

		return null;
	}
	
	public String getPickupDateStr(){
		if (pickupDate != null) {
			return WMSDateUtil.toFormattedDateStr(pickupDate);
		}

		return null;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public DCRVDILBO getDcrvdil() {
		return dcrvdil;
	}

	public void setDcrvdil(DCRVDILBO dcrvdil) {
		this.dcrvdil = dcrvdil;
	}
	
	public void setDcrvdil(Long dcrvdilId) {
		DCRVDILBO dcrvdil = new DCRVDILBO();
		dcrvdil.setId(dcrvdilId);
		this.setDcrvdil(dcrvdil);
	}

	public Date getDcrDate() {
		return dcrDate;
	}

	public void setDcrDate(Date dcrDate) {
		this.dcrDate = dcrDate;
	}

	public Date getPickupDate() {
		return pickupDate;
	}

	public void setPickupDate(Date pickupDate) {
		this.pickupDate = pickupDate;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public Long getCurrencyId() {
		return currencyId;
	}

	public void setCurrencyId(Long currencyId) {
		this.currencyId = currencyId;
	}

	public Long getTypeId() {
		return typeId;
	}

	public void setTypeId(Long typeId) {
		this.typeId = typeId;
	}

}
