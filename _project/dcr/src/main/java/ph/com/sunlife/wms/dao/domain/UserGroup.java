package ph.com.sunlife.wms.dao.domain;

import java.io.Serializable;
import java.util.Date;

public class UserGroup implements Serializable {

	private static final long serialVersionUID = 1657584721152397641L;

	private String ugWMSUID;
	private String ugGRPID;
	private String ugReceiveWork;
	private String ugPrimaryGroup;
	private String ugActive;
	private String ugCREUser;
	private Date ugCREDate;
	private String ugUPDUser;
	private Date ugUPDDate;
	
	public String getUgWMSUID() {
		return ugWMSUID;
	}
	public void setUgWMSUID(String ugWMSUID) {
		this.ugWMSUID = ugWMSUID;
	}
	public String getUgGRPID() {
		return ugGRPID;
	}
	public void setUgGRPID(String ugGRPID) {
		this.ugGRPID = ugGRPID;
	}
	public String getUgReceiveWork() {
		return ugReceiveWork;
	}
	public void setUgReceiveWork(String ugReceiveWork) {
		this.ugReceiveWork = ugReceiveWork;
	}
	public String getUgPrimaryGroup() {
		return ugPrimaryGroup;
	}
	public void setUgPrimaryGroup(String ugPrimaryGroup) {
		this.ugPrimaryGroup = ugPrimaryGroup;
	}
	public String getUgActive() {
		return ugActive;
	}
	public void setUgActive(String ugActive) {
		this.ugActive = ugActive;
	}
	public String getUgCREUser() {
		return ugCREUser;
	}
	public void setUgCREUser(String ugCREUser) {
		this.ugCREUser = ugCREUser;
	}
	public Date getUgCREDate() {
		return ugCREDate;
	}
	public void setUgCREDate(Date ugCREDate) {
		this.ugCREDate = ugCREDate;
	}
	public String getUgUPDUser() {
		return ugUPDUser;
	}
	public void setUgUPDUser(String ugUPDUser) {
		this.ugUPDUser = ugUPDUser;
	}
	public Date getUgUPDDate() {
		return ugUPDDate;
	}
	public void setUgUPDDate(Date ugUPDDate) {
		this.ugUPDDate = ugUPDDate;
	}
	@Override
	public String toString() {
		return "UserGroup [ugWMSUID=" + ugWMSUID + ", ugGRPID=" + ugGRPID
				+ ", ugReceiveWork=" + ugReceiveWork + ", ugPrimaryGroup="
				+ ugPrimaryGroup + ", ugActive=" + ugActive + ", ugCREUser="
				+ ugCREUser + ", ugCREDate=" + ugCREDate + ", ugUPDUser="
				+ ugUPDUser + ", ugUPDDate=" + ugUPDDate + "]";
	}
	
}
