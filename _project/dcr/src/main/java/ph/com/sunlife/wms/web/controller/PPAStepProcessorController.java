package ph.com.sunlife.wms.web.controller;

import static ph.com.sunlife.wms.util.WMSConstants.STATUS_AWAITING_REQUIREMENTS;
import static ph.com.sunlife.wms.util.WMSConstants.STATUS_FOR_VERIFICATION;
import static ph.com.sunlife.wms.util.WMSConstants.STATUS_NOT_VERIFIED;
import static ph.com.sunlife.wms.util.WMSConstants.STATUS_RECONCILED;
import static ph.com.sunlife.wms.util.WMSConstants.STATUS_REQUIREMENTS_NOT_SUBMITTED;
import static ph.com.sunlife.wms.util.WMSConstants.STATUS_REQUIREMENTS_SUBMITTED;
import static ph.com.sunlife.wms.util.WMSConstants.STATUS_VERIFIED;
import static ph.com.sunlife.wms.util.WMSConstants.STATUS_QA_REVIEWED;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.web.servlet.ModelAndView;

import ph.com.sunlife.wms.security.Role;
import ph.com.sunlife.wms.services.ConsolidatedDataService;
import ph.com.sunlife.wms.services.DCRCommentService;
import ph.com.sunlife.wms.services.DCRCreationService;
import ph.com.sunlife.wms.services.DCRSearchService;
import ph.com.sunlife.wms.services.bo.ConsolidatedData;
import ph.com.sunlife.wms.services.bo.DCRCommentBO;
import ph.com.sunlife.wms.services.bo.DCRStepProcessorNoteBO;
import ph.com.sunlife.wms.services.bo.UserSession;
import ph.com.sunlife.wms.services.exception.ServiceException;
import ph.com.sunlife.wms.util.WMSDateUtil;
import ph.com.sunlife.wms.web.controller.form.AddCommentForm;
import ph.com.sunlife.wms.web.controller.form.NoteForm;
import ph.com.sunlife.wms.web.controller.form.PPASPForm;
import ph.com.sunlife.wms.web.controller.form.PPASPSaveForm;
import ph.com.sunlife.wms.web.exception.ApplicationException;

public class PPAStepProcessorController extends
		AbstractSecuredMultiActionController {

	private static final String DCR_DATE_STR = "dcrDateStr";

	private static final String SAVE_PPA_STEP_PROCESSOR_MAPPING = "savePPAStepProcessorMapping";

	private static final String PPA_SP_KEY = "ppaSPPage";

	private static final String COMMENTS_SP_KEY = "spCommentPage";

	private static final String CONSOLIDATED_DATA_KEY = "consolidatedData";

	private static final String ATTACHMENT_KEY = "attachments";

	private static final String FORMATTED_CONSOLIDATED_DATA_KEY = "formattedConsolidatedData";

	private static final String COMMENTS_LIST_KEY = "commentList";

	private static final String USER_ROLE_KEY = "userRoles";

	private static final String FROM_SEARCH_KEY = "fromSearch";

	private static final String CLOSE_KEY = "closeWindow";

	private DCRCreationService dcrCreationService;

	private DCRSearchService dcrSearchService;

	private ConsolidatedDataService consolidatedDataService;

	private DCRCommentService dcrCommentService;

	private static final Logger LOGGER = Logger
			.getLogger(PPAStepProcessorController.class);

	public ModelAndView doShowHomePage(
			HttpServletRequest request,
			HttpServletResponse response,
            PPASPForm form) throws ApplicationException {

		LOGGER.info("Start doShowHomePage method for PPAStepProcessorController...");

		ModelAndView modelAndView = new ModelAndView();
		try {

			UserSession userSession = this.getUserSession(request);
			List<Role> roles = userSession.getRoles();
			Long dcrId = form.getDcrId();
			
			ConsolidatedData consolidatedData = consolidatedDataService.getConsolidatedData(dcrId, null, true, false);		
			
			Date dcrDate = consolidatedData.getDcrDate();
			String dcrDateStr = request.getParameter("dcrDt");
                        
			List<String> formattedConsolidatedData = consolidatedDataService.formatConsolidatedData(consolidatedData);

			// Added for MR-WF-16-00034 - Random sampling for QA
			boolean isPpaQa = Boolean.parseBoolean(request.getParameter("isPpaQa"));

			// Set objects for view
			modelAndView.addObject(CONSOLIDATED_DATA_KEY, consolidatedData);

			modelAndView.addObject(FORMATTED_CONSOLIDATED_DATA_KEY, formattedConsolidatedData);

			modelAndView.addObject(USER_ROLE_KEY, roles);

			modelAndView.addObject(SAVE_PPA_STEP_PROCESSOR_MAPPING, "/ppaSP/save.html");

			modelAndView.addObject(ATTACHMENT_KEY, consolidatedData.getUserToAttachmentsMap());
			
			modelAndView.addObject(DCR_DATE_STR, (null != dcrDate ? WMSDateUtil.toFormattedDateStrWithTime(dcrDate) : (null != dcrDateStr ? dcrDateStr : "")));
			
			modelAndView.addObject(FROM_SEARCH_KEY, form.isFromSearch());

			modelAndView.addObject("title", "PPA Step Processor");
                        
			// Added for MR-WF-16-00034 - Random sampling for QA
			modelAndView.addObject("isPpaQa", isPpaQa);

		} catch (ServiceException e) {
			LOGGER.error(e);
			throw new ApplicationException(e);
		}

		modelAndView.setViewName(PPA_SP_KEY);
		modelAndView.addObject(CLOSE_KEY, false);

		LOGGER.info("Start doShowHomePage method for PPAStepProcessorController...");
		return modelAndView;
	}

	public ModelAndView savePPAStepProcessor(HttpServletRequest request,
			HttpServletResponse response, PPASPSaveForm form)
			throws ApplicationException {

		List<NoteForm> noteForms = form.getNoteForms();

		Long dcrId = form.getDcrId();

		String userId = getUserSession(request).getUserId();

		List<DCRStepProcessorNoteBO> notes = new ArrayList<DCRStepProcessorNoteBO>();
		if (CollectionUtils.isNotEmpty(noteForms)) {
			for (NoteForm noteForm : noteForms) {
				if (noteForm != null) {
					DCRStepProcessorNoteBO note = new DCRStepProcessorNoteBO();
					note.setAcf2id(userId);
					note.setDcrId(dcrId);
					note.setNoteContent(noteForm.getNoteContent());
					note.setNoteType(noteForm.getNoteType());
					note.setRowNum(noteForm.getRowNum());
					notes.add(note);
				}
			}
		}

		try {
			LOGGER.info("Trying to save notes and checked reconciled rows...");
			consolidatedDataService.savePPAStepProcessor(userId, dcrId,
					getReconciledRowNums(form.getReconRownums()), notes);
		} catch (ServiceException e) {
			LOGGER.error(e);
			throw new ApplicationException(e);
		}

		return this.doShowHomePage(request, response, form);
	}

	private List<Integer> getReconciledRowNums(String reconRowNums) {
		List<Integer> rowNumbers = null;
		if (StringUtils.isNotEmpty(reconRowNums)) {
			rowNumbers = new ArrayList<Integer>();
			String[] rowNums = StringUtils.split(reconRowNums, ",");
			for (String rowNum : rowNums) {
				rowNumbers.add(Integer.valueOf(StringUtils.trim(rowNum)));
			}
		}

		return rowNumbers;
	}

	public DCRCreationService getDcrCreationService() {
		return dcrCreationService;
	}

	public void setDcrCreationService(DCRCreationService dcrCreationService) {
		this.dcrCreationService = dcrCreationService;
	}

	public DCRSearchService getDcrSearchService() {
		return dcrSearchService;
	}

	public void setDcrSearchService(DCRSearchService dcrSearchService) {
		this.dcrSearchService = dcrSearchService;
	}

	public ConsolidatedDataService getConsolidatedDataService() {
		return consolidatedDataService;
	}

	public void setConsolidatedDataService(
			ConsolidatedDataService consolidatedDataService) {
		this.consolidatedDataService = consolidatedDataService;
	}

	public DCRCommentService getDcrCommentService() {
		return dcrCommentService;
	}

	public void setDcrCommentService(DCRCommentService dcrCommentService) {
		this.dcrCommentService = dcrCommentService;
	}

	@Override
	protected Role[] allowedRoles() {
		Role[] ppa = { Role.PPA };
		return ppa;
	}

	public ModelAndView doShowComments(HttpServletRequest request,
			HttpServletResponse response, PPASPForm form)
			throws ApplicationException {

		ModelAndView modelAndView = new ModelAndView();

		try {
			List<DCRCommentBO> dcrCommentBO = dcrCommentService
					.getAllCommentsDcrId(form.getDcrId());

			modelAndView.addObject(COMMENTS_LIST_KEY, dcrCommentBO);

		} catch (ServiceException e) {
			LOGGER.error(e);
			throw new ApplicationException(e);
		}

		modelAndView.setViewName(COMMENTS_SP_KEY);

		return modelAndView;

	}

	public ModelAndView doSaveComment(HttpServletRequest request,
			HttpServletResponse response, AddCommentForm form) throws Exception {

		Long dcrCashierId = form.getDcrCashierId();
		Long dcrId = form.getDcrId();
		UserSession userSession = getUserSession(request);
		String acf2id = userSession.getUserId();

		DCRCommentBO dcrComment = new DCRCommentBO();
		dcrComment.setAcf2id(acf2id);
		dcrComment.setDatePosted(new Date());
		dcrComment.setDcrCashier(dcrCashierId);
		dcrComment.setRemarks(form.getRemarks());
		dcrComment.setDcrId(dcrId);

		ModelAndView modelAndView = new ModelAndView();
		List<DCRCommentBO> dcrComments = null;

		try {
			dcrCommentService.save(dcrComment);
			dcrComments = dcrCommentService
					.getAllCommentsDcrId(form.getDcrId());
		} catch (ServiceException e) {
			LOGGER.error(e);
			throw new ApplicationException(
					"Something went wrong after trying to add new comment", e);
		}

		modelAndView.addObject(COMMENTS_LIST_KEY, dcrComments);
		modelAndView.setViewName(COMMENTS_SP_KEY);

		return modelAndView;
	}

    public ModelAndView doCompleteWork(
            HttpServletRequest request,
            HttpServletResponse response,
            PPASPSaveForm form) throws ApplicationException {

        LOGGER.info("Start doCompleteWork method for PPAStepProcessorController...");

        UserSession userSession = getUserSession(request);
        String userId = userSession.getUserId();

        ModelAndView modelAndView = this.doShowHomePage(request, response, form);
        try {
            List<DCRStepProcessorNoteBO> dcrNoteList = new ArrayList<DCRStepProcessorNoteBO>();
            for (NoteForm nl : form.getNoteForms()) {
                DCRStepProcessorNoteBO tempNoteBO = new DCRStepProcessorNoteBO();
                tempNoteBO.setNoteContent(nl.getNoteContent());
                tempNoteBO.setNoteType(nl.getNoteType());
                tempNoteBO.setRowNum(nl.getRowNum());
                tempNoteBO.setDcrId(form.getDcrId());
                dcrNoteList.add(tempNoteBO);
            }
            Long dcrId = form.getDcrId();
            String reconRowNums = form.getReconRownums();

            String stepProcResponse = form.getStepProcResponse();
            
            if (reconRowNums == null) {
                reconRowNums = (request.getParameter("rowNums") == null) ? null : request.getParameter("rowNums");
            }

            if (STATUS_RECONCILED.equals(stepProcResponse)) {
                consolidatedDataService.ppaReconciled(userId, dcrId,
                        getReconciledRowNums(reconRowNums), dcrNoteList);
            } else if (STATUS_FOR_VERIFICATION.equals(stepProcResponse)) {
                consolidatedDataService.forVerificationPPA(userId, dcrId,
                        getReconciledRowNums(reconRowNums), dcrNoteList);
            } else if (STATUS_AWAITING_REQUIREMENTS.equals(stepProcResponse)) {
                consolidatedDataService.awaitingRequirements(userId, dcrId,
                        getReconciledRowNums(reconRowNums), dcrNoteList);
            } else if (STATUS_VERIFIED.equals(stepProcResponse)) {
                consolidatedDataService.verifiedPPA(userId, dcrId,
                        getReconciledRowNums(reconRowNums), dcrNoteList);
            } else if (STATUS_REQUIREMENTS_SUBMITTED.equals(stepProcResponse)) {
                consolidatedDataService.reqtsSubmittedPPA(userId, dcrId,
                        getReconciledRowNums(reconRowNums), dcrNoteList);
            } else if (STATUS_REQUIREMENTS_NOT_SUBMITTED.equals(stepProcResponse)) {
                consolidatedDataService.reqtsNotSubmittedPPA(userId, dcrId,
                        getReconciledRowNums(reconRowNums), dcrNoteList);
            } else if (STATUS_NOT_VERIFIED.equals(stepProcResponse)) {
                consolidatedDataService.notVerifiedPPA(userId, dcrId,
                        getReconciledRowNums(reconRowNums), dcrNoteList);
            } // Added for MR-WF-16-00034 - Random sampling for QA
            else if (STATUS_QA_REVIEWED.equals(stepProcResponse)) {
                consolidatedDataService.qaReviewed(dcrId, userId, stepProcResponse);
            }

        } catch (Exception e) {
            LOGGER.error(e);
            throw new ApplicationException(e);
        }

        modelAndView.addObject(CLOSE_KEY, true);

        LOGGER.info("End doCompleteWork method for PPAStepProcessorController...");
        return modelAndView;
    }
}
