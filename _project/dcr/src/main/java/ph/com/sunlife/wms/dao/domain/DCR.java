package ph.com.sunlife.wms.dao.domain;

import java.util.Date;
import java.util.List;

/**
 * The heart and soul of the entire WMS DCR application. This represents the
 * table <b>DCR</b> which denote a workitem for the center for the day.
 * 
 * @author Zainal Limpao
 * 
 */
public class DCR extends Entity implements Comparable<DCR> {

	private String ccId;

	private List<String> ccIdSet;

	private Date dcrDate;

	private Date dcrStartDate;

	private Date dcrEndDate;

	private Date requiredCompletionDate;

	private String status;

	private List<DCRCashier> dcrCashiers;

	private String workObjectNumber;

	private String ccName;

	private String centerCode;

	private String hubId;

	private String createUserId;

	private Date createDate;

	private String updateUserId;

	private Date updateDate;

	private int marker;
	
	private String hasDownloaded;
	
	private Date downloadedDate;
	
	//Added for MR-WF-16-00112
	private int ccqaTAT;
	
	private Date ccqaRequiredCompletionDate;
	
	private Date ccqaSubmittedDate;
	
	public String getHasDownloaded() {
		return hasDownloaded;
	}

	public void setHasDownloaded(String hasDownloaded) {
		this.hasDownloaded = hasDownloaded;
	}

	public Date getDownloadedDate() {
		return downloadedDate;
	}

	public void setDownloadedDate(Date downloadedDate) {
		this.downloadedDate = downloadedDate;
	}

	/**
	 * The <b>dcr_cre_user</b> column with type varchar(6).
	 * 
	 * @return
	 */
	public String getCreateUserId() {
		return createUserId;
	}

	public void setCreateUserId(String createUserId) {
		this.createUserId = createUserId;
	}

	/**
	 * The <b>dcr_cre_date</b> column with type datetime.
	 * 
	 * @return
	 */
	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	/**
	 * The <b>dcr_upd_user</b> column with type varchar(6).
	 * 
	 * @return
	 */
	public String getUpdateUserId() {
		return updateUserId;
	}

	public void setUpdateUserId(String updateUserId) {
		this.updateUserId = updateUserId;
	}

	/**
	 * The <b>dcr_upd_date</b> column with type datetime.
	 * 
	 * @return
	 */
	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public List<String> getCcIdSet() {
		return ccIdSet;
	}

	public void setCcIdSet(List<String> ccIdSet) {
		this.ccIdSet = ccIdSet;
	}

	/**
	 * The <b>HubCCNBOs.HCCN_HUB_ID</b> associated with <b>DCR.cc_id</b>
	 * 
	 * @return
	 */
	public String getHubId() {
		return hubId;
	}

	public void setHubId(String hubId) {
		this.hubId = hubId;
	}

	/**
	 * This does not represent a column, but only used as parameter whenever DCR
	 * object is passed on.
	 * 
	 * @return
	 */
	public Date getDcrStartDate() {
		return dcrStartDate;
	}

	public void setDcrStartDate(Date dcrStartDate) {
		this.dcrStartDate = dcrStartDate;
	}

	/**
	 * This does not represent a column, but only used as parameter whenever DCR
	 * object is passed on.
	 * 
	 * @return
	 */
	public Date getDcrEndDate() {
		return dcrEndDate;
	}

	public void setDcrEndDate(Date dcrEndDate) {
		this.dcrEndDate = dcrEndDate;
	}

	public String getCenterCode() {
		return centerCode;
	}

	public void setCenterCode(String centerCode) {
		this.centerCode = centerCode;
	}

	public String getCcName() {
		return ccName;
	}

	public void setCcName(String ccName) {
		this.ccName = ccName;
	}

	public String getCcId() {
		return ccId;
	}

	public void setCcId(String ccId) {
		this.ccId = ccId;
	}

	/**
	 * The <b>dcr_dt</b> column with type datetime.
	 * 
	 * @return
	 */
	public Date getDcrDate() {
		return dcrDate;
	}

	public void setDcrDate(Date dcrDate) {
		this.dcrDate = dcrDate;
	}

	/**
	 * The <b>required_cmp_dt</b> column with type datetime.
	 * 
	 * @return
	 */
	public Date getRequiredCompletionDate() {
		return requiredCompletionDate;
	}

	public void setRequiredCompletionDate(Date requiredCompletionDate) {
		this.requiredCompletionDate = requiredCompletionDate;
	}

	/**
	 * The <b>status</b> column with type varchar(60).
	 * 
	 * @return
	 */
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * Transient property.
	 * 
	 * @return
	 */
	public List<DCRCashier> getDcrCashiers() {
		return dcrCashiers;
	}

	public void setDcrCashiers(List<DCRCashier> dcrCashiers) {
		this.dcrCashiers = dcrCashiers;
	}

	/**
	 * The <b>F_WobNum</b> column with varchar(40).
	 * 
	 * @return
	 */
	public String getWorkObjectNumber() {
		return workObjectNumber;
	}

	public void setWorkObjectNumber(String workObjectNumber) {
		this.workObjectNumber = workObjectNumber;
	}

	@Override
	public int compareTo(DCR other) {
		int compareTo = 0;
		if (other.getId() != null && this.getId() != null) {
			compareTo = other.getId().compareTo(this.getId());
		}
		return compareTo;
	}

	public int getMarker() {
		return marker;
	}

	public void setMarker(int marker) {
		this.marker = marker;
	}
	
	//Added for MR-WF-16-00112
	public int getCcqaTAT() {
		return ccqaTAT;
	}

	public void setCcqaTAT(int ccqaTAT) {
		this.ccqaTAT = ccqaTAT;
	}
	
	public Date getCcqaRequiredCompletionDate() {
		return ccqaRequiredCompletionDate;
	}

	public void setCcqaRequiredCompletionDate(Date ccqaRequiredCompletionDate) {
		this.ccqaRequiredCompletionDate = ccqaRequiredCompletionDate;
	}

	public Date getCcqaSubmittedDate() {
		return ccqaSubmittedDate;
	}

	public void setCcqaSubmittedDate(Date ccqaSubmittedDate) {
		this.ccqaSubmittedDate = ccqaSubmittedDate;
	}

	@Override
	public String toString() {
		return "DCR [ccId=" + ccId + ", ccIdSet=" + ccIdSet + ", dcrDate="
				+ dcrDate + ", dcrStartDate=" + dcrStartDate + ", dcrEndDate="
				+ dcrEndDate + ", requiredCompletionDate="
				+ requiredCompletionDate + ", status=" + status
				+ ", dcrCashiers=" + dcrCashiers + ", workObjectNumber="
				+ workObjectNumber + ", ccName=" + ccName + ", centerCode="
				+ centerCode + ", hubId=" + hubId + ", createUserId="
				+ createUserId + ", createDate=" + createDate
				+ ", updateUserId=" + updateUserId + ", updateDate="
				+ updateDate + ", marker=" + marker + ", hasDownloaded="
				+ hasDownloaded + ", downloadedDate=" + downloadedDate + "]";
	}

}
