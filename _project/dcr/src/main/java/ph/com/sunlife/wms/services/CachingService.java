package ph.com.sunlife.wms.services;

import java.util.Date;

import ph.com.sunlife.wms.services.exception.ServiceException;

/**
 * The Caching Service Mechanism for WMS DCR OE Upload. All values are stored in
 * the table <b>dbo.DCRCachedProperty</b> in the WMS Database. Values are being
 * cached into the server's memory and are only refreshed ideally every hour.
 * 
 * @author Zainal Limpao
 * 
 */
public interface CachingService {

	/**
	 * The DB configurable WMS DCR System Date.
	 * 
	 * @return
	 */
	Date getWmsDcrSystemDate();

	/**
	 * The call that increases the WMS DCR System Date by one (1) day. This is
	 * triggered daily.
	 * 
	 * @return
	 * @throws ServiceException
	 */
	boolean incrementWmsDcrSystemDate() throws ServiceException;

	/**
	 * The method that resets all values stored in the memory.
	 * 
	 * @throws ServiceException
	 */
	void resetCachedValues() throws ServiceException;

	/**
	 * The getter method for the Cached Property.
	 * 
	 * @param propertyKey
	 * @return
	 * @throws ServiceException
	 */
	String getCachedValue(String propertyKey) throws ServiceException;
	
	boolean updateProperty(final String prop_key,final String prop_value, final String lastUpdatedUser, final Long lastUpdatedDate) throws ServiceException;
}
