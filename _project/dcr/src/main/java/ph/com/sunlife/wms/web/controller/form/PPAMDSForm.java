package ph.com.sunlife.wms.web.controller.form;

import javax.servlet.http.HttpSession;

import org.springframework.validation.Errors;

import ph.com.sunlife.wms.web.controller.binder.BinderAware;
import ph.com.sunlife.wms.web.controller.binder.SessionAware;

// Change according to Healthcheck
// *Problem was declared as public/protected in BinderAware
// *Change BinderAware interface class to abstract class and extends it rather than implements
public class PPAMDSForm extends BinderAware implements SessionAware {

	private HttpSession session;

	private String dcrId;
	
	private String ccId;
	
	private String dcrDateStr;
        
	// Added for MR-WF-16-00034 - Random sampling for QA
	private boolean isPpaQa;
	
	public String getDcrId() {
		return dcrId;
	}

	public void setDcrId(String dcrId) {
		this.dcrId = dcrId;
	}

	public String getDcrDateStr() {
		return dcrDateStr;
	}

	public void setDcrDateStr(String dcrDateStr) {
		this.dcrDateStr = dcrDateStr;
	}

	public String getCcId() {
		return ccId;
	}

	public void setCcId(String ccId) {
		this.ccId = ccId;
	}

    // Change according to Healthcheck
    // *Problem was declared as public/protected in BinderAware
    // *Change use BinderAware getter instead of direct access
    public String[] getAllowedFields() {
        return super.getAllowedFields();
    }

    // Change according to Healthcheck
    // *Problem was declared as public/protected in BinderAware
    // *Change use BinderAware getter instead of direct access
    public String[] getRequiredFields() {
        return super.getRequiredFields();
    }

    // Change according to Healthcheck
    // *Problem was declared as public/protected in BinderAware
    // *Change use BinderAware getter instead of direct access
    public String[] getUncheckedFields() {
        return super.getUncheckedFields();
    }

	@Override
	public void afterBind() {
	}

	@Override
	public boolean validate(Errors errors) {
		return false;
	}

	public HttpSession getSession() {
		return session;
	}

	public void setSession(HttpSession session) {
		this.session = session;
	}

	public String getSecretKey() {
		return null;
	}

    /**
     * Added for MR-WF-16-00034 - Random sampling for QA
     * @return the isPpaQa
     */
    public boolean isIsPpaQa() {
        return isPpaQa;
    }

    /**
     * Added for MR-WF-16-00034 - Random sampling for QA
     * @param isPpaQa the isPpaQa to set
     */
    public void setIsPpaQa(boolean isPpaQa) {
        this.isPpaQa = isPpaQa;
    }

}
