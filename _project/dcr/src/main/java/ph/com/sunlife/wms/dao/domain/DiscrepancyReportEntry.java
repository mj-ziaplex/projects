package ph.com.sunlife.wms.dao.domain;

import java.util.Date;

public class DiscrepancyReportEntry {

	private Long dcrId;
	
	private String ccId;
	
	private String ccName;
	
	private Date dcrDate;
	
	private Date requiredCompletionDate;
	
	private String status;
	
	private Date lastUpdateDate;
	
	private String lastUpdateUser;
	
	private int numberOfFindings = 0;
	
	private String findings;
	
	public String getFindings() {
		return findings;
	}

	public void setFindings(String findings) {
		this.findings = findings;
	}

	public int getNumberOfFindings() {
		return numberOfFindings;
	}

	public void setNumberOfFindings(int numberOfFindings) {
		this.numberOfFindings = numberOfFindings;
	}

	public Long getDcrId() {
		return dcrId;
	}

	public void setDcrId(Long dcrId) {
		this.dcrId = dcrId;
	}

	public String getCcId() {
		return ccId;
	}

	public void setCcId(String ccId) {
		this.ccId = ccId;
	}

	public String getCcName() {
		return ccName;
	}

	public void setCcName(String ccName) {
		this.ccName = ccName;
	}

	public Date getDcrDate() {
		return dcrDate;
	}

	public void setDcrDate(Date dcrDate) {
		this.dcrDate = dcrDate;
	}

	public Date getRequiredCompletionDate() {
		return requiredCompletionDate;
	}

	public void setRequiredCompletionDate(Date requiredCompletionDate) {
		this.requiredCompletionDate = requiredCompletionDate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getLastUpdateDate() {
		return lastUpdateDate;
	}

	public void setLastUpdateDate(Date lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
	}

	public String getLastUpdateUser() {
		return lastUpdateUser;
	}

	public void setLastUpdateUser(String lastUpdateUser) {
		this.lastUpdateUser = lastUpdateUser;
	}
	
}
