package ph.com.sunlife.wms.dao.domain;

import java.io.Serializable;
import java.util.Date;

public class HubCC implements Serializable {

	private static final long serialVersionUID = -4428238276841973345L;
	
	private String hccHUBID;
	private String hccCCID;
	private String hccActive;
	private String hccCREUser;
	private Date hccCREDate;
	private String hccUPDUser;
	private Date hccUPDDate;
	
	public String getHccHUBID() {
		return hccHUBID;
	}
	public void setHccHUBID(String hccHUBID) {
		this.hccHUBID = hccHUBID;
	}
	public String getHccCCID() {
		return hccCCID;
	}
	public void setHccCCID(String hccCCID) {
		this.hccCCID = hccCCID;
	}
	public String getHccActive() {
		return hccActive;
	}
	public void setHccActive(String hccActive) {
		this.hccActive = hccActive;
	}
	public String getHccCREUser() {
		return hccCREUser;
	}
	public void setHccCREUser(String hccCREUser) {
		this.hccCREUser = hccCREUser;
	}
	public Date getHccCREDate() {
		return hccCREDate;
	}
	public void setHccCREDate(Date hccCREDate) {
		this.hccCREDate = hccCREDate;
	}
	public String getHccUPDUser() {
		return hccUPDUser;
	}
	public void setHccUPDUser(String hccUPDUser) {
		this.hccUPDUser = hccUPDUser;
	}
	public Date getHccUPDDate() {
		return hccUPDDate;
	}
	public void setHccUPDDate(Date hccUPDDate) {
		this.hccUPDDate = hccUPDDate;
	}
	@Override
	public String toString() {
		return "HubCCNBO [hccHUBID=" + hccHUBID + ", hccCCID=" + hccCCID
				+ ", hccActive=" + hccActive + ", hccCREUser=" + hccCREUser
				+ ", hccCREDate=" + hccCREDate + ", hccUPDUser=" + hccUPDUser
				+ ", hccUPDDate=" + hccUPDDate + "]";
	}
}
