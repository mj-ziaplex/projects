package ph.com.sunlife.wms.dao.domain;

public class DCRSearchParameter {

	private String userId;
	private String HubId;
	
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getHubId() {
		return HubId;
	}
	public void setHubId(String hubId) {
		HubId = hubId;
	}	
	
}
