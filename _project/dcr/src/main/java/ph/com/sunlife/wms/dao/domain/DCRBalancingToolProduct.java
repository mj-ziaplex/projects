package ph.com.sunlife.wms.dao.domain;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.util.CollectionUtils;

public class DCRBalancingToolProduct extends Entity {

	private DCRBalancingTool dcrBalancingTool = new DCRBalancingTool();

	private ProductType productType;

	private Long productTypeId;

	private double worksiteAmount;
	
	private DCROther dcrOther;
	
	private List<DCRIpacValue> dcrIpacValues;
	
	private List<DCRReversal> reversals;
	
	public DCRBalancingTool getDcrBalancingTool() {
		return dcrBalancingTool;
	}

	public void setDcrBalancingTool(DCRBalancingTool dcrBalancingTool) {
		this.dcrBalancingTool = dcrBalancingTool;
	}

	public ProductType getProductType() {
		if (productType == null) {
			this.productType = ProductType.getById(this.productTypeId);
		}
		return productType;
	}

	public void setProductType(ProductType productType) {
		this.productType = productType;
	}

	public double getWorksiteAmount() {
		return worksiteAmount;
	}

	public void setWorksiteAmount(double worksiteAmount) {
		this.worksiteAmount = worksiteAmount;
	}

	public List<DCRReversal> getReversals() {
		return reversals;
	}

	public void setReversals(List<DCRReversal> reverals) {
		this.reversals = reverals;
	}

	public void setReversals(DCRReversal reversal) {
		List<DCRReversal> reversals = new ArrayList<DCRReversal>(
				Collections.singleton(reversal));
		this.setReversals(reversals);
	}

	public Long getProductTypeId() {
		if (productTypeId == null) {
			this.productTypeId = productType.getId();
		}
		return productTypeId;
	}

	public void setProductTypeId(Long productTypeId) {
		this.productType = ProductType.getById(productTypeId);
		this.productTypeId = productTypeId;
	}

	public void addReversal(DCRReversal reversal) {
		if (CollectionUtils.isEmpty(this.reversals)) {
			List<DCRReversal> reversals = new ArrayList<DCRReversal>(
					Collections.singleton(reversal));
			this.setReversals(reversals);
		} else {
			this.reversals.add(reversal);
		}
	}

	public List<DCRIpacValue> getDcrIpacValues() {
		return dcrIpacValues;
	}

	public void setDcrIpacValues(List<DCRIpacValue> dcrIpacValues) {
		this.dcrIpacValues = dcrIpacValues;
	}

	public DCROther getDcrOther() {
		return dcrOther;
	}

	public void setDcrOther(DCROther dcrOther) {
		this.dcrOther = dcrOther;
	}

}
