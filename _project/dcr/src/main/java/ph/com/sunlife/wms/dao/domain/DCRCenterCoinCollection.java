package ph.com.sunlife.wms.dao.domain;

import java.util.Date;

public class DCRCenterCoinCollection {

	private Long id;

	private String ccId;

	private Date dateUpdated;

	private int denom1;

	private int denom50c;

	private int denom25c;

	private int denom10c;

	private int denom5c;

	private int denom1c;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCcId() {
		return ccId;
	}

	public void setCcId(String ccId) {
		this.ccId = ccId;
	}

	public Date getDateUpdated() {
		return dateUpdated;
	}

	public void setDateUpdated(Date dateUpdated) {
		this.dateUpdated = dateUpdated;
	}

	public int getDenom1() {
		return denom1;
	}

	public void setDenom1(int denom1) {
		this.denom1 = denom1;
	}

	public int getDenom50c() {
		return denom50c;
	}

	public void setDenom50c(int denom50c) {
		this.denom50c = denom50c;
	}

	public int getDenom25c() {
		return denom25c;
	}

	public void setDenom25c(int denom25c) {
		this.denom25c = denom25c;
	}

	public int getDenom10c() {
		return denom10c;
	}

	public void setDenom10c(int denom10c) {
		this.denom10c = denom10c;
	}

	public int getDenom5c() {
		return denom5c;
	}

	public void setDenom5c(int denom5c) {
		this.denom5c = denom5c;
	}

	public int getDenom1c() {
		return denom1c;
	}

	public void setDenom1c(int denom1c) {
		this.denom1c = denom1c;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((ccId == null) ? 0 : ccId.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DCRCenterCoinCollection other = (DCRCenterCoinCollection) obj;
		if (ccId == null) {
			if (other.ccId != null)
				return false;
		} else if (!ccId.equals(other.ccId))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
