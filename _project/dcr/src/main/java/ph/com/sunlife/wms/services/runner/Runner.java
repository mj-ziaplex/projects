package ph.com.sunlife.wms.services.runner;

import static ph.com.sunlife.wms.util.WMSConstants.STATUS_NEW;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import ph.com.sunlife.wms.dao.domain.DCR;
import ph.com.sunlife.wms.services.DCRCreationService;
import ph.com.sunlife.wms.services.bo.DCRIpacValueBO;
import ph.com.sunlife.wms.util.WMSDateUtil;

public class Runner {

//    private static final ApplicationContext APPLICATION_CONTEXT =
//            new ClassPathXmlApplicationContext("ph/com/sunlife/wms/service/spring/stand-alone-config.xml");

    public static void main(String args[]) throws Exception {
//		System.out.println("Initializing all system properties...");
//		System.out.println("Preparing DCR Creation...");
//		
//        DCRCreationService dcrCreationService = (DCRCreationService) APPLICATION_CONTEXT.getBean("dcrCreationService");
////		
//        Date date = WMSDateUtil.toDate("12DEC2012");
//
//        DCR dcr = new DCR();
//        dcr.setCcId("H4");
//        dcr.setDcrDate(date);
//        dcr.setStatus(STATUS_NEW);
//        dcr.setRequiredCompletionDate(WMSDateUtil.getRCD(date));
//
//        List<DCR> dcrList = new ArrayList<DCR>();
//        dcrList.add(dcr);
//
//        dcrCreationService.saveDCRList(dcrList, date, false);

//		dcrCreationService.initiateDCRWorkItems(date);
//		
//		System.out.println("DCR Workitems Creation for "
//				+ WMSDateUtil.toFormattedDateStr(date) + " completed");
        
        DCRIpacValueBO tmp = new DCRIpacValueBO();
        tmp.setProdCode("MF3");
        tmp.setPaySubCode("L/C");
        
        if(!"MF".equals(tmp.getProdCode())){
            System.out.println("/n");
            System.out.println("/n");
            System.out.println("/n");
            System.out.println("/n");
        }
        
        if ("L/C".equalsIgnoreCase(tmp.getPaySubCode()) && !"MF".equals(tmp.getProdCode())) { // Added for PCO - Not include PCO
            System.out.println("/n");
            System.out.println("/n");
            System.out.println("PASOK SA CONDITION");
            System.out.println("/n");
            System.out.println("/n");
        } else { // Added for PCO - Not include PCO
            System.out.println("/n");
            System.out.println("/n");
            System.out.println("HND PASOK SA CONDITION");
            System.out.println("/n");
            System.out.println("/n");
        }
    }
}
