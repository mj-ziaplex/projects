package ph.com.sunlife.wms.dao.impl;

import java.sql.SQLException;
import java.util.List;

import org.springframework.orm.ibatis.SqlMapClientCallback;

import com.ibatis.sqlmap.client.SqlMapExecutor;

import ph.com.sunlife.wms.dao.AbstractWMSSqlMapClientDaoSupport;
import ph.com.sunlife.wms.dao.DCRCommentDao;
import ph.com.sunlife.wms.dao.domain.DCRComment;
import ph.com.sunlife.wms.dao.exceptions.WMSDaoException;

/**
 * The implementing class for the {@link DCRCommentDao} interface.
 * 
 * @author Zainal Limpao
 * 
 */
public class DCRCommentDaoImpl extends
		AbstractWMSSqlMapClientDaoSupport<DCRComment> implements DCRCommentDao {

	private static final String NAMESPACE = "DCRComment";

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ph.com.sunlife.wms.dao.AbstractWMSSqlMapClientDaoSupport#getNamespace()
	 */
	@Override
	protected String getNamespace() {
		return NAMESPACE;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ph.com.sunlife.wms.dao.DCRCommentDao#getAllComments(java.lang.Long)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<DCRComment> getAllComments(final Long dcrCashierId)
			throws WMSDaoException {
		List<DCRComment> list = null;

		try {
			list = (List<DCRComment>) getSqlMapClientTemplate().execute(
					new SqlMapClientCallback() {

						@Override
						public Object doInSqlMapClient(SqlMapExecutor executor)
								throws SQLException {
							return executor.queryForList(getNamespace()
									+ ".getAllComments", dcrCashierId);
						}
					});
		} catch (Exception e) {
			throw new WMSDaoException(e);
		}
		return list;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<DCRComment> getAllCommentsByDcrId(final Long dcrId)
			throws WMSDaoException {
		List<DCRComment> list = null;

		try {
			list = (List<DCRComment>) getSqlMapClientTemplate().execute(
					new SqlMapClientCallback() {

						@Override
						public Object doInSqlMapClient(SqlMapExecutor executor)
								throws SQLException {
							return executor.queryForList(getNamespace()
									+ ".getAllCommentsByDcrId", dcrId);
						}
					});
		} catch (Exception e) {
			throw new WMSDaoException(e);
		}
		return list;
	}

}
