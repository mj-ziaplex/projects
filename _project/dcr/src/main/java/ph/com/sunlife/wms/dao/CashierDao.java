package ph.com.sunlife.wms.dao;

import java.util.List;

import ph.com.sunlife.wms.dao.domain.Cashier;
import ph.com.sunlife.wms.dao.exceptions.WMSDaoException;

/**
 * The Data Access Object responsible for retrieving Cashier-specific
 * information.
 * 
 * @author Zainal Limpao
 * 
 */
public interface CashierDao {

	/**
	 * Creates {@link Cashier} object given the acf2id.
	 * 
	 * @param acf2id
	 * @return
	 * @throws WMSDaoException
	 */
	Cashier getCashier(String acf2id) throws WMSDaoException;

	List<String> getCCNameAndCodes() throws WMSDaoException;

	String getCCNameById(String siteCode) throws WMSDaoException;

	List<Cashier> getMultipleCashierInfo(List<String> cashierUserIds)
			throws WMSDaoException;
}
