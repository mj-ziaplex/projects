package ph.com.sunlife.wms.dao.impl;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;

import ph.com.sunlife.wms.dao.AbstractWMSSqlMapClientDaoSupport;
import ph.com.sunlife.wms.dao.DCRCashDepositSlipDao;
import ph.com.sunlife.wms.dao.domain.DCRCashDepositSlip;
import ph.com.sunlife.wms.dao.exceptions.WMSDaoException;

public class DCRCashDepositSlipDaoImpl extends
		AbstractWMSSqlMapClientDaoSupport<DCRCashDepositSlip> implements
		DCRCashDepositSlipDao {

	private static final String NAMESPACE = "DCRCashDepositSlip";

	@Override
	protected String getNamespace() {
		return NAMESPACE;
	}

	@SuppressWarnings("unchecked")
	@Override
	public DCRCashDepositSlip getCashDepositSlipByExample(
			DCRCashDepositSlip example) throws WMSDaoException {
		List<DCRCashDepositSlip> list = getSqlMapClientTemplate().queryForList(
				getNamespace() + ".getCashDepositSlip", example);

		if (CollectionUtils.isNotEmpty(list)) {
			return list.get(0);
		}

		return null;
	}

	public DCRCashDepositSlip saveCashDepositSlip(
			DCRCashDepositSlip dcrCashDepositSlip) throws WMSDaoException {
		Long id = (Long) getSqlMapClientTemplate().queryForObject(
				getNamespace() + ".insert", dcrCashDepositSlip);
		dcrCashDepositSlip.setId(id);
		return dcrCashDepositSlip;
	}

	@SuppressWarnings("unchecked")
	public List<DCRCashDepositSlip> getAllCashDepositSlipsByDcrId(Long dcrId)
			throws WMSDaoException {
		List<DCRCashDepositSlip> results = null;

		try {
			results = getSqlMapClientTemplate().queryForList(
					getNamespace() + ".getAllCashDepositSlipsByDcrId", dcrId);
		} catch (Exception e) {
			throw new WMSDaoException(e);
		}

		return results;
	}

	@SuppressWarnings("unchecked")
	public List<DCRCashDepositSlip> getAllCashDepositSlipsByDcrCashierId(
			Long dcrCashierId) throws WMSDaoException {
		List<DCRCashDepositSlip> results = null;

		try {
			results = getSqlMapClientTemplate().queryForList(
					getNamespace() + ".getAllCashDepositSlipsByDcrCashierId",
					dcrCashierId);
		} catch (Exception e) {
			throw new WMSDaoException(e);
		}

		return results;
	}
}
