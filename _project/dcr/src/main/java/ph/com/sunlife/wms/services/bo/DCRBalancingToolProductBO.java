package ph.com.sunlife.wms.services.bo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;

import ph.com.sunlife.wms.dao.domain.ProductType;

public class DCRBalancingToolProductBO {

	private Long id;

	private DCRBalancingToolBO dcrBalancingTool;

	private ProductType productType;

	private DCROtherBO dcrOther;

	private double worksiteAmount;

	private List<DCRIpacValueBO> dcrIpacValues;

	private double totalCashCounter;

	private double totalCashNonCounter;

	private double totalCheckOnUs;

	private double totalCheckLocal;

	private double totalCheckRegional;

	private double totalCheckOT;

	private double totalCheckNonCounter;

	private double totalNonCash;

	private double totalPosBpi;

	private double totalPosCtb;

	private double totalPosHsbc;

	private double totalPosScb;

	private double totalPosRcbc;

	private double totalPosBdo;

	private double totalNBIndividual;

	private double totalNBVariable;

	private double totalVULRenewal;

	private double totalNonCashFromDTR;

	private double totalNonPolicy;

	private double totalWorksiteNonPosted;

	// TODO: include this in the output jsp - ${column.totalCreditMemo}
	private double totalCreditMemo;

	private double exemptionsReversal;

	private double exemptionsWorksite;

	private List<DCRReversalBO> dcrReversals;

	private double totalReversalAmt;

	private double totalNewBusinessIndividual;

	private double totalNewBusinessVariable;

	private double totalIndividualRenewal;

	private double totalVariableRenewal;

	// TODO: include this in the output jsp - ${column.totalUsCheckInManila}
	private double totalUsCheckInManila;

	// TODO: include this in the output jsp - ${column.totalUsCheckOutPh}
	private double totalUsCheckOutPh;

	// TODO: include this in the output jsp - ${column.totalDollarCheque}
	private double totalDollarCheque;

	// TODO: include this in the output jsp - ${column.totalBankOTCCheckPayment}
	private double totalBankOTCCheckPayment;

	private double totalMdsBpi;

	private double totalMdsBdo;

	private double totalMdsRcbc;

	private double totalMdsSbc;

	private double totalMdsCtb;

	private double totalMdsHsbc;
	
	private double totalAutoCtb;
	
	private double totalAutoScb;
	
	private double totalAutoRcbc;
	
	private double totalSunlinkOnline;
	
	private double totalSunlinkHsbcDollar;
	
	private double totalSunlinkHsbcPeso;
	
	private double totalEpsBpi;
	
	// EPS-BPI
	private double totalCashEpsBpi;
	
	// Postal Money Order (under check)
	private double totalPmo;
	
	private double totalCheckGaf;
	
	private double totalCardGaf;
	
	private double sessionTotal;
	
	private double paymentFromInsurer;
	
	private String stotProblem;
	
	private double totalPostedNonPolicy;
        
        private double totalMf; // Added for PCO
        
        private double totalPcoInvest; // Added for PCO
	
	public String getStotProblem() {
		return stotProblem;
	}
	
	public double getPaymentFromInsurer() {
		return paymentFromInsurer;
	}

	public void setPaymentFromInsurer(double paymentFromInsurer) {
		this.paymentFromInsurer = paymentFromInsurer;
	}



	public void setStotProblem(String stotProblem) {
		this.stotProblem = stotProblem;
	}

	public double getTotalCheckGaf() {
		return totalCheckGaf;
	}

	public void setTotalCheckGaf(double totalCheckGaf) {
		this.totalCheckGaf = totalCheckGaf;
	}

	public double getTotalCardGaf() {
		return totalCardGaf;
	}

	public void setTotalCardGaf(double totalCardGaf) {
		this.totalCardGaf = totalCardGaf;
	}

	public double getTotalPmo() {
		return totalPmo;
	}

	public void setTotalPmo(double totalPmo) {
		this.totalPmo = totalPmo;
	}

	public double getTotalCashEpsBpi() {
		return totalCashEpsBpi;
	}

	public void setTotalCashEpsBpi(double totalCashEpsBpi) {
		this.totalCashEpsBpi = totalCashEpsBpi;
	}

	public double getTotalSunlinkHsbcDollar() {
		return totalSunlinkHsbcDollar;
	}

	public void setTotalSunlinkHsbcDollar(double totalSunlinkHsbcDollar) {
		this.totalSunlinkHsbcDollar = totalSunlinkHsbcDollar;
	}

	public double getTotalSunlinkHsbcPeso() {
		return totalSunlinkHsbcPeso;
	}

	public void setTotalSunlinkHsbcPeso(double totalSunlinkHsbcPeso) {
		this.totalSunlinkHsbcPeso = totalSunlinkHsbcPeso;
	}

	public double getTotalEpsBpi() {
		return totalEpsBpi;
	}

	public void setTotalEpsBpi(double totalEpsBpi) {
		this.totalEpsBpi = totalEpsBpi;
	}

	public double getTotalMdsBpi() {
		return totalMdsBpi;
	}

	public void setTotalMdsBpi(double totalMdsBpi) {
		this.totalMdsBpi = totalMdsBpi;
	}

	public double getTotalMdsBdo() {
		return totalMdsBdo;
	}

	public void setTotalMdsBdo(double totalMdsBdo) {
		this.totalMdsBdo = totalMdsBdo;
	}

	public double getTotalMdsRcbc() {
		return totalMdsRcbc;
	}

	public void setTotalMdsRcbc(double totalMdsRcbc) {
		this.totalMdsRcbc = totalMdsRcbc;
	}

	public double getTotalMdsSbc() {
		return totalMdsSbc;
	}

	public void setTotalMdsSbc(double totalMdsSbc) {
		this.totalMdsSbc = totalMdsSbc;
	}

	public double getTotalMdsCtb() {
		return totalMdsCtb;
	}

	public void setTotalMdsCtb(double totalMdsCtb) {
		this.totalMdsCtb = totalMdsCtb;
	}

	public double getTotalMdsHsbc() {
		return totalMdsHsbc;
	}

	public void setTotalMdsHsbc(double totalMdsHsbc) {
		this.totalMdsHsbc = totalMdsHsbc;
	}

	public void addDcrIpacValues(DCRIpacValueBO ipacValue) {
		if (dcrIpacValues == null) {
			dcrIpacValues = new ArrayList<DCRIpacValueBO>();
		}
		dcrIpacValues.add(ipacValue);
	}

	public List<DCRIpacValueBO> getDcrIpacValues() {
		if (CollectionUtils.isEmpty(dcrIpacValues)) {
			this.dcrIpacValues = new ArrayList<DCRIpacValueBO>();
		}
		return dcrIpacValues;
	}

	public void setDcrIpacValues(List<DCRIpacValueBO> dcrIpacValues) {
		this.dcrIpacValues = dcrIpacValues;
	}

	private List<ReversalBO> reversals;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public DCRBalancingToolBO getDcrBalancingTool() {
		return dcrBalancingTool;
	}

	public void setDcrBalancingTool(DCRBalancingToolBO dcrBalancingTool) {
		this.dcrBalancingTool = dcrBalancingTool;
	}

	public ProductType getProductType() {
		return productType;
	}

	public void setProductType(ProductType productType) {
		this.productType = productType;
	}

	public double getWorksiteAmount() {
		return worksiteAmount;
	}

	public void setWorksiteAmount(double worksiteAmount) {
		this.worksiteAmount = worksiteAmount;
	}

	public List<ReversalBO> getReversals() {
		return reversals;
	}

	public void setReversals(List<ReversalBO> reverals) {
		this.reversals = reverals;
	}

	public void setReversals(ReversalBO reversal) {
		List<ReversalBO> reversals = new ArrayList<ReversalBO>(
				Collections.singleton(reversal));
		this.setReversals(reversals);
	}

	public void addReversal(ReversalBO reversal) {
		if (CollectionUtils.isEmpty(this.reversals)) {
			List<ReversalBO> reversals = new ArrayList<ReversalBO>(
					Collections.singleton(reversal));
			this.setReversals(reversals);
		} else {
			this.reversals.add(reversal);
		}
	}

	public double getTotalCashCounter() {
		return totalCashCounter;
	}

	public void setTotalCashCounter(double totalCashCounter) {
		this.totalCashCounter = totalCashCounter;
	}

	public void addTotalCashCounter(double amount) {
		this.totalCashCounter = this.totalCashCounter + amount;
	}

	public double getTotalCashNonCounter() {
		return totalCashNonCounter;
	}

	public void setTotalCashNonCounter(double totalCashNonCounter) {
		this.totalCashNonCounter = totalCashNonCounter;
	}

	public void addTotalCashNonCounter(double amount) {
		this.totalCashNonCounter = this.totalCashNonCounter + amount;
	}

	public double getTotalCheckOnUs() {
		return totalCheckOnUs;
	}

	public void setTotalCheckOnUs(double totalCheckOnUs) {
		this.totalCheckOnUs = totalCheckOnUs;
	}

	public void addTotalCheckOnUs(double amount) {
		this.totalCheckOnUs = this.totalCheckOnUs + amount;
	}

	public double getTotalCheckLocal() {
		return totalCheckLocal;
	}

	public void setTotalCheckLocal(double totalCheckLocal) {
		this.totalCheckLocal = totalCheckLocal;
	}

	public void addTotalCheckLocal(double amount) {
		this.totalCheckLocal = this.totalCheckLocal + amount;
	}

	public double getTotalCheckRegional() {
		return totalCheckRegional;
	}

	public void setTotalCheckRegional(double totalCheckRegional) {
		this.totalCheckRegional = totalCheckRegional;
	}

	public void addTotalCheckRegional(double amount) {
		this.totalCheckRegional = this.totalCheckRegional + amount;
	}

	public double getTotalCheckOT() {
		return totalCheckOT;
	}

	public void setTotalCheckOT(double totalCheckOT) {
		this.totalCheckOT = totalCheckOT;
	}

	public void addTotalCheckOT(double amount) {
		this.totalCheckOT = this.totalCheckOT + amount;
	}

	public double getTotalCheckNonCounter() {
		return totalCheckNonCounter;
	}

	public void setTotalCheckNonCounter(double totalCheckNonCounter) {
		this.totalCheckNonCounter = totalCheckNonCounter;
	}

	public void addTotalCheckNonCounter(double amount) {
		this.totalCheckNonCounter = this.totalCheckNonCounter + amount;
	}

	public double getTotalNonCash() {
		return totalNonCash;
	}

	public void setTotalNonCash(double totalNonCash) {
		this.totalNonCash = totalNonCash;
	}

	public void addTotalNonCash(double amount) {
		this.totalNonCash = this.totalNonCash + amount;
	}

	public double getTotalPosBpi() {
		return totalPosBpi;
	}

	public void setTotalPosBpi(double totalPosBpi) {
		this.totalPosBpi = totalPosBpi;
	}

	public void addTotalPosBpi(double amount) {
		this.totalPosBpi = this.totalPosBpi + amount;
	}

	public double getTotalPosCtb() {
		return totalPosCtb;
	}

	public void setTotalPosCtb(double totalPosCtb) {
		this.totalPosCtb = totalPosCtb;
	}

	public void addTotalPosCtb(double amount) {
		this.totalPosCtb = this.totalPosCtb + amount;
	}

	public double getTotalPosHsbc() {
		return totalPosHsbc;
	}

	public void setTotalPosHsbc(double totalPosHsbc) {
		this.totalPosHsbc = totalPosHsbc;
	}

	public void addTotalPosHsbc(double amount) {
		this.totalPosHsbc = this.totalPosHsbc + amount;
	}

	public double getTotalPosScb() {
		return totalPosScb;
	}

	public void setTotalPosScb(double totalPosScb) {
		this.totalPosScb = totalPosScb;
	}

	public void addTotalPosScb(double amount) {
		this.totalPosScb = this.totalPosScb + amount;
	}

	public double getTotalPosRcbc() {
		return totalPosRcbc;
	}

	public void setTotalPosRcbc(double totalPosRcbc) {
		this.totalPosRcbc = totalPosRcbc;
	}

	public void addTotalPosRcbc(double amount) {
		this.totalPosRcbc = this.totalPosRcbc + amount;
	}

	public double getTotalPosBdo() {
		return totalPosBdo;
	}

	public void setTotalPosBdo(double totalPosBdo) {
		this.totalPosBdo = totalPosBdo;
	}

	public void addTotalPosBdo(double amount) {
		this.totalPosBdo = this.totalPosBdo + amount;
	}

	public double getTotalNBIndividual() {
		return totalNBIndividual;
	}

	public void setTotalNBIndividual(double totalNBIndividual) {
		this.totalNBIndividual = totalNBIndividual;
	}

	public void addTotalNBIndividual(double amount) {
		this.totalNBIndividual = this.totalNBIndividual + amount;
	}

	public double getTotalNBVariable() {
		return totalNBVariable;
	}

	public void setTotalNBVariable(double totalNBVariable) {
		this.totalNBVariable = totalNBVariable;
	}

	public void addTotalNBVariable(double amount) {
		this.totalNBVariable = this.totalNBVariable + amount;
	}

	public double getTotalVULRenewal() {
		return totalVULRenewal;
	}

	public void setTotalVULRenewal(double totalVULRenewal) {
		this.totalVULRenewal = totalVULRenewal;
	}

	public void addTotalVULRenewal(double amount) {
		this.totalVULRenewal = this.totalVULRenewal + amount;
	}

	public double getTotalNonCashFromDTR() {
		return totalNonCashFromDTR;
	}

	public void setTotalNonCashFromDTR(double totalNonCashFromDTR) {
		this.totalNonCashFromDTR = totalNonCashFromDTR;
	}

	public void addTotalNonCashFromDTR(double amount) {
		this.totalNonCashFromDTR = this.totalNonCashFromDTR + amount;
	}

	public double getTotalNonPolicy() {
		return totalNonPolicy;
	}

	public void setTotalNonPolicy(double totalNonPolicy) {
		this.totalNonPolicy = totalNonPolicy;
	}

	public void addTotalNonPolicy(double amount) {
		this.totalNonPolicy = this.totalNonPolicy + amount;
	}

	public double getTotalWorksiteNonPosted() {
		return totalWorksiteNonPosted;
	}

	public void setTotalWorksiteNonPosted(double totalWorksiteNonPosted) {
		this.totalWorksiteNonPosted = totalWorksiteNonPosted;
	}

	public void addTotalWorksiteNonPosted(double amount) {
		this.totalWorksiteNonPosted = this.totalWorksiteNonPosted + amount;
	}

	public double getExemptionsReversal() {
		return exemptionsReversal;
	}

	public void setExemptionsReversal(double exemptionsReversal) {
		this.exemptionsReversal = exemptionsReversal;
	}

	public void addExemptionsReversal(double amount) {
		this.exemptionsReversal = this.exemptionsReversal + amount;
	}

	public double getExemptionsWorksite() {
		return exemptionsWorksite;
	}

	public void setExemptionsWorksite(double exemptionsWorksite) {
		this.exemptionsWorksite = exemptionsWorksite;
	}

	public void addExemptionsWorksite(double amount) {
		this.exemptionsWorksite = this.exemptionsWorksite + amount;
	}

	public DCROtherBO getDcrOther() {
		return dcrOther;
	}

	public void setDcrOther(DCROtherBO dcrOther) {
		this.dcrOther = dcrOther;
	}

	public List<DCRReversalBO> getDcrReversals() {
		return dcrReversals;
	}

	public void setDcrReversals(List<DCRReversalBO> dcrReversals) {
		this.dcrReversals = dcrReversals;
	}

	public double getTotalReversalAmt() {
		return totalReversalAmt;
	}

	public void setTotalReversalAmt(double totalReversalAmt) {
		this.totalReversalAmt = totalReversalAmt;
	}

	public double getTotalCreditMemo() {
		return totalCreditMemo;
	}

	public void setTotalCreditMemo(double totalCreditMemo) {
		this.totalCreditMemo = totalCreditMemo;
	}

	public void addTotalCreditMemo(double amount) {
		this.totalCreditMemo = this.totalCreditMemo + amount;
	}

	public double getTotalNewBusinessIndividual() {
		return totalNewBusinessIndividual;
	}

	public void setTotalNewBusinessIndividual(double totalNewBusinessIndividual) {
		this.totalNewBusinessIndividual = totalNewBusinessIndividual;
	}

	public void addTotalNewBusinessIndividual(double amount) {
		this.totalNewBusinessIndividual = this.totalNewBusinessIndividual
				+ amount;
	}

	public double getTotalNewBusinessVariable() {
		return totalNewBusinessVariable;
	}

	public void setTotalNewBusinessVariable(double totalNewBusinessVariable) {
		this.totalNewBusinessVariable = totalNewBusinessVariable;
	}

	public void addTotalNewBusinessVariable(double amount) {
		this.totalNewBusinessVariable = this.totalNewBusinessVariable + amount;
	}

	public double getTotalVariableRenewal() {
		return totalVariableRenewal;
	}

	public void setTotalVariableRenewal(double totalVariableRenewal) {
		this.totalVariableRenewal = totalVariableRenewal;
	}

	public void addTotalVariableRenewal(double amount) {
		this.totalVariableRenewal = this.totalVariableRenewal + amount;
	}

	public double getTotalIndividualRenewal() {
		return totalIndividualRenewal;
	}

	public void setTotalIndividualRenewal(double totalIndividualRenewal) {
		this.totalIndividualRenewal = totalIndividualRenewal;
	}

	public void addTotalIndividualRenewal(double amount) {
		this.totalIndividualRenewal = this.totalIndividualRenewal + amount;
	}

	public double getTotalUsCheckInManila() {
		return totalUsCheckInManila;
	}

	public void setTotalUsCheckInManila(double totalUsCheckInManila) {
		this.totalUsCheckInManila = totalUsCheckInManila;
	}

	public void addTotalUsCheckInManila(double amount) {
		this.totalUsCheckInManila = this.totalUsCheckInManila + amount;
	}

	public double getTotalUsCheckOutPh() {
		return totalUsCheckOutPh;
	}

	public void setTotalUsCheckOutPh(double totalUsCheckOutPh) {
		this.totalUsCheckOutPh = totalUsCheckOutPh;
	}

	public void addTotalUsCheckOutPh(double amount) {
		this.totalUsCheckOutPh = this.totalUsCheckOutPh + amount;
	}

	public double getTotalDollarCheque() {
		return totalDollarCheque;
	}

	public void setTotalDollarCheque(double totalDollarCheque) {
		this.totalDollarCheque = totalDollarCheque;
	}

	public void addTotalDollarCheque(double amount) {
		this.totalDollarCheque = this.totalDollarCheque + amount;
	}

	public double getTotalBankOTCCheckPayment() {
		return totalBankOTCCheckPayment;
	}

	public void setTotalBankOTCCheckPayment(double totalBankOTCCheckPayment) {
		this.totalBankOTCCheckPayment = totalBankOTCCheckPayment;
	}

	public void addTotalBankOTCCheckPayment(double amount) {
		this.totalBankOTCCheckPayment = this.totalBankOTCCheckPayment + amount;
	}

	public double getTotalAutoCtb() {
		return totalAutoCtb;
	}

	public void setTotalAutoCtb(double totalAutoCtb) {
		this.totalAutoCtb = totalAutoCtb;
	}

	public double getTotalAutoScb() {
		return totalAutoScb;
	}

	public void setTotalAutoScb(double totalAutoScb) {
		this.totalAutoScb = totalAutoScb;
	}

	public double getTotalAutoRcbc() {
		return totalAutoRcbc;
	}

	public void setTotalAutoRcbc(double totalAutoRcbc) {
		this.totalAutoRcbc = totalAutoRcbc;
	}

	public double getTotalSunlinkOnline() {
		return totalSunlinkOnline;
	}

	public void setTotalSunlinkOnline(double totalSunlinkOnline) {
		this.totalSunlinkOnline = totalSunlinkOnline;
	}

	public double getSessionTotal() {
		return sessionTotal;
	}

	public void setSessionTotal(double sessionTotal) {
		this.sessionTotal = sessionTotal;
	}

	public double getTotalPostedNonPolicy() {
		return totalPostedNonPolicy;
	}

	public void setTotalPostedNonPolicy(double totalPostedNonPolicy) {
		this.totalPostedNonPolicy = totalPostedNonPolicy;
	}

    /**
     * @return the totalMf
     */
    public double getTotalMf() {
        return totalMf;
    }

    /**
     * @param totalMf the totalMf to set
     */
    public void setTotalMf(double totalMf) {
        this.totalMf = totalMf;
    }

    /**
     * @return the totalPcoInvest
     */
    public double getTotalPcoInvest() {
        return totalPcoInvest;
    }

    /**
     * @param totalPcoInvest the totalPcoInvest to set
     */
    public void setTotalPcoInvest(double totalPcoInvest) {
        this.totalPcoInvest = totalPcoInvest;
    }

}
