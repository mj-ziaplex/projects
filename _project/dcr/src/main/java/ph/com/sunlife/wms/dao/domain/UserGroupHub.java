package ph.com.sunlife.wms.dao.domain;

public class UserGroupHub {

	private String hubId;
	
	private String acf2id;
	
	private String groupName;
	
	private String groupId;

	private String ccId;
	
	private String comCode;

	public String getHubId() {
		return hubId;
	}

	public void setHubId(String hubId) {
		this.hubId = hubId;
	}

	public String getAcf2id() {
		return acf2id;
	}

	public void setAcf2id(String acf2id) {
		this.acf2id = acf2id;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getGroupId() {
		return groupId;
	}

	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

	public String getCcId() {
		return ccId;
	}

	public void setCcId(String ccId) {
		this.ccId = ccId;
	}

	public String getComCode() {
		return comCode;
	}

	public void setComCode(String comCode) {
		this.comCode = comCode;
	}

	@Override
	public String toString() {
		return "UserGroupHub [hubId=" + hubId + ", acf2id=" + acf2id
				+ ", groupName=" + groupName + ", groupId=" + groupId
				+ ", ccId=" + ccId + ", comCode=" + comCode + "]";
	}	
}
