package ph.com.sunlife.wms.web.controller.form;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.FactoryUtils;
import org.apache.commons.collections.ListUtils;

import ph.com.sunlife.wms.services.bo.DCRErrorBO;

public class ErrorTagSaveForm extends ErrorTagForm {
	//private String dcrId;//Long?
	private String reason;
	private String stepId;
	private String stepName;
	private String userId;
	private String userName;
	private String tagger;
	
	//NEW
	private String logIdentity;//for edit
	private String reasonDeleted;
	
	@SuppressWarnings("unchecked")
	private List<DCRErrorBO> taggedErrors = ListUtils.lazyList(
			new ArrayList<DCRErrorBO>(),
			FactoryUtils.instantiateFactory(DCRErrorBO.class));

//	public String getDcrId() {
//		return dcrId;
//	}
//
//	public void setDcrId(String dcrId) {
//		this.dcrId = dcrId;
//	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getStepId() {
		return stepId;
	}

	public void setStepId(String stepId) {
		this.stepId = stepId;
	}

	public String getStepName() {
		return stepName;
	}

	public void setStepName(String stepName) {
		this.stepName = stepName;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public List<DCRErrorBO> getTaggedErrors() {
		return taggedErrors;
	}

	public void setTaggedErrors(List<DCRErrorBO> taggedErrors) {
		this.taggedErrors = taggedErrors;
	}

	public String getLogIdentity() {
		return logIdentity;
	}

	public void setLogIdentity(String logIdentity) {
		this.logIdentity = logIdentity;
	}

	public String getReasonDeleted() {
		return reasonDeleted;
	}

	public void setReasonDeleted(String reasonDeleted) {
		this.reasonDeleted = reasonDeleted;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getTagger() {
		return tagger;
	}

	public void setTagger(String tagger) {
		this.tagger = tagger;
	}
	
	
}
