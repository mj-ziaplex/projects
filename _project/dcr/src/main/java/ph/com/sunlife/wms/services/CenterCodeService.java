package ph.com.sunlife.wms.services;

import java.util.List;

import ph.com.sunlife.wms.dao.domain.CenterCode;
import ph.com.sunlife.wms.dao.domain.HubCC;
import ph.com.sunlife.wms.dao.domain.HubCCNBO;
import ph.com.sunlife.wms.services.bo.AdminCenterCodeBO;
import ph.com.sunlife.wms.services.exception.ServiceException;

public interface CenterCodeService {

	boolean createCenterCode(CenterCode cc) throws ServiceException;

	List<CenterCode> getAllCenterCode() throws ServiceException;

	List<CenterCode> getCenterCodeById(String ccId) throws ServiceException;

	boolean updateCenterCode(CenterCode cc) throws ServiceException;

	boolean deleteCenterCode(String ccId) throws ServiceException;
	
	boolean createHubCCNBO(HubCCNBO nbo) throws ServiceException;
	
	boolean createHubCC(HubCC cc) throws ServiceException;
	
	boolean updateHubCCNBO(HubCCNBO nbo) throws ServiceException;

	boolean updateHubCC(HubCC cc) throws ServiceException;
	
	boolean deleteHubCCNBO(HubCCNBO nbo) throws ServiceException;

	boolean deleteHubCC(HubCC cc) throws ServiceException;
	
	void doCenterCodeCreate(AdminCenterCodeBO adminCenterCodeBO, String sessUser, String ccIdCachedValue) throws ServiceException;

	void doCenterCodeUpdate(AdminCenterCodeBO adminCenterCodeBO, String sessUser, String ccIdCachedValue) throws ServiceException;

}
