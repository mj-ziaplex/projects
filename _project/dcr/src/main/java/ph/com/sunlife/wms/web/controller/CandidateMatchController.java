package ph.com.sunlife.wms.web.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.web.servlet.ModelAndView;

import ph.com.sunlife.wms.security.Role;
import ph.com.sunlife.wms.services.AttachmentService;
import ph.com.sunlife.wms.services.DCRSearchService;
import ph.com.sunlife.wms.services.bo.AttachmentBO;
import ph.com.sunlife.wms.services.bo.CustomerCenterBO;
import ph.com.sunlife.wms.services.bo.DCRBO;
import ph.com.sunlife.wms.services.bo.HubBO;
import ph.com.sunlife.wms.services.bo.MatchBO;
import ph.com.sunlife.wms.services.bo.UnmatchBO;
import ph.com.sunlife.wms.services.bo.UserSession;
import ph.com.sunlife.wms.services.exception.ServiceException;
import ph.com.sunlife.wms.util.WMSDateUtil;
import ph.com.sunlife.wms.web.controller.form.CandidateMatchForm;
import ph.com.sunlife.wms.web.controller.form.CandidateMatchSearchForm;
import ph.com.sunlife.wms.web.controller.form.MatchUnmatchForm;
import ph.com.sunlife.wms.web.exception.ApplicationException;

public class CandidateMatchController extends
		AbstractSecuredMultiActionController {
	
	private static final String CANDIDATE_MATCH_KEY = "candidateMatchView";
	
	private static final String TITLE_KEY = "title";
	
	private static final String CC_NAME_KEY = "ccName";
	
	private static final String CC_ID_KEY = "ccId";
	
	private static final String DCR_DATE_KEY = "dcrDateStr";
	
	private static final String CC_LIST_KEY = "ccList";
	
	private static final String ATTACHMENT_KEY = "attachments";
	
	private static final String DCR_ID_KEY = "dcrId";
	
	private static final String DCR_DATE_STR_KEY = "dcrDate";
	
	private static final String SEARCH_RESULTS_KEY = "searchResults";
	
	private static final String CC_PARAM_KEY = "searchCC";
	
	private static final String DCR_DATE_PARAM_KEY = "searchDate";
	
	private static final String FROM_SEARCH_KEY = "fromSearch";
	
	private static final String RELOAD_SP = "reloadSP";
	
	private static final String FORM_KEY = "form";
	
	private static final String MATCH_COUNT_KEY = "matchCount";
	
	private static final String UNMATCH_COUNT_KEY = "unmatchCount";
	
	private DCRSearchService dcrSearchService;
	
	private AttachmentService attachmentService;
	
	public AttachmentService getAttachmentService() {
		return attachmentService;
	}

	public void setAttachmentService(AttachmentService attachmentService) {
		this.attachmentService = attachmentService;
	}

	public DCRSearchService getDcrSearchService() {
		return dcrSearchService;
	}

	public void setDcrSearchService(DCRSearchService dcrSearchService) {
		this.dcrSearchService = dcrSearchService;
	}

	public ModelAndView doShowCandidateMatchPage(HttpServletRequest request,
			HttpServletResponse response, CandidateMatchForm form) throws ApplicationException {

		ModelAndView modelAndView = new ModelAndView();

		modelAndView.setViewName(CANDIDATE_MATCH_KEY);
		modelAndView.addObject(TITLE_KEY, "Candidate Match");
		modelAndView.addObject(CC_NAME_KEY, form.getCcName());
		modelAndView.addObject(DCR_DATE_KEY, form.getDcrDateStr());
		modelAndView.addObject(DCR_ID_KEY, form.getDcrId());
		modelAndView.addObject(CC_ID_KEY, form.getCcId());
		modelAndView.addObject(DCR_DATE_STR_KEY, form.getDcrDate());
		modelAndView.addObject(FROM_SEARCH_KEY, false);
		modelAndView.addObject(RELOAD_SP, false);
		List<CustomerCenterBO> ccList = new ArrayList<CustomerCenterBO>();
		Set<CustomerCenterBO> ccSet = new HashSet<CustomerCenterBO>();
		List<AttachmentBO> attachmentList = new ArrayList<AttachmentBO>();
		List<CustomerCenterBO> resultList =  null;
		try{
			List<HubBO> hubList = dcrSearchService.getCCHubs();
			for(HubBO h: hubList){
				ccList.addAll(dcrSearchService.getCCByHub(h.getHubId()));
				ccSet.addAll(ccList);
			}
			if (CollectionUtils.isNotEmpty(ccSet)) {
				resultList = new ArrayList<CustomerCenterBO>(
						ccSet);
				Collections.sort(resultList);
			}
			modelAndView.addObject(CC_LIST_KEY, resultList);
			
			attachmentList = attachmentService.getCandidateMatchAttachmentsByDCR(form.getDcrId());
			modelAndView.addObject(ATTACHMENT_KEY, attachmentList);
		} catch(ServiceException e){
			throw new ApplicationException(e);
		}

		return modelAndView;

	}
	
	public ModelAndView doUnmatch(HttpServletRequest request,
			HttpServletResponse response, MatchUnmatchForm form) throws ApplicationException {

		ModelAndView modelAndView = new ModelAndView();
		try{
			for(UnmatchBO ub: form.getUnMatchList()){				
				attachmentService.unmatchAttachment(ub.getRecordId(), ub.getRecordLocation(), WMSDateUtil.toDate(ub.getDcrDate()), ub.getCcId(), ub.getFilenetId());
			}
			modelAndView = this.doShowCandidateMatchPage(request, response, form);	
			modelAndView.addObject(RELOAD_SP, true);
			modelAndView.addObject(FROM_SEARCH_KEY, false);
			modelAndView.addObject(UNMATCH_COUNT_KEY, form.getUnMatchList().size());
		} catch(ServiceException e){
			throw new ApplicationException(e);
		}

		return modelAndView;

	}
	
	public ModelAndView doMatch(HttpServletRequest request,
			HttpServletResponse response, MatchUnmatchForm form) throws ApplicationException {
		ModelAndView modelAndView = new ModelAndView();
		
		try{
			UserSession userSession = getUserSession(request);
			for(MatchBO ub: form.getMatchList()){				
				attachmentService.matchAttachment(ub, userSession.getUserId(), form.getDcrId());
			}		
		} catch(ServiceException e){
			throw new ApplicationException(e);
		}
		
		modelAndView = this.doShowCandidateMatchPage(request, response, form);	
		modelAndView.addObject(RELOAD_SP, true);
		modelAndView.addObject(FROM_SEARCH_KEY, false);
		modelAndView.addObject(MATCH_COUNT_KEY, form.getMatchList().size());
		return modelAndView;
	}
	
	public ModelAndView doFindCandidateMatch(HttpServletRequest request,
			HttpServletResponse response, CandidateMatchSearchForm form) throws ApplicationException {
		
		ModelAndView modelAndView = new ModelAndView();
		try{
			DCRBO tempDcrBo = new DCRBO();
			List<String> ccIdSet = new ArrayList<String>();
			ccIdSet.add(form.getSearchCcId());
			tempDcrBo.setCcIdSet(ccIdSet);
			tempDcrBo.setDcrStartDate(WMSDateUtil.startOfTheDay(form.getSearchDcrDate()));
			tempDcrBo.setDcrEndDate(WMSDateUtil.endOfTheDay(form.getSearchDcrDate()));
			List<DCRBO> dcrBo = dcrSearchService.getDCRByCCAndDate(tempDcrBo);
			List<AttachmentBO> results = new ArrayList<AttachmentBO>();
			for(DCRBO d: dcrBo){
				if(!d.getId().equals(form.getDcrId())){
					results.addAll(attachmentService.getCandidateMatchAttachmentsByDCR(d.getId()));
				}
			}			
			modelAndView = this.doShowCandidateMatchPage(request, response, form);
			modelAndView.addObject(SEARCH_RESULTS_KEY, results);
			modelAndView.addObject(CC_PARAM_KEY, dcrSearchService.getCCNameById(form.getSearchCcId()));
			modelAndView.addObject(DCR_DATE_PARAM_KEY, form.getSearchDcrDateStr());
			modelAndView.addObject(FROM_SEARCH_KEY, true);
			modelAndView.addObject(FORM_KEY, form);
		}catch(ServiceException e){
			throw new ApplicationException(e);
		}
		return modelAndView;
	}

    @Override
    protected Role[] allowedRoles() {
        // Change according to Healthcheck
        // Problem was due to changes in AbstractSecuredMultiActionController
        // Change was use parent getter method instead of inherited values
        return super.getALL_ROLES();
    }
}
