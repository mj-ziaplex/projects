package ph.com.sunlife.wms.services.exception;

public class InvalidFileTypeException extends Exception {

	private static final long serialVersionUID = -8749909964345351580L;

	public InvalidFileTypeException(String message) {
		super(message);
	}

	public InvalidFileTypeException(String message, Throwable throwable) {
		super(message, throwable);
	}

	public InvalidFileTypeException(Throwable throwable) {
		super(throwable);
	}
}
