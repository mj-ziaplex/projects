///*
// * $Id: WarnLog4JProfiler.java,v 1.1 2012/10/16 08:17:58 PV70 Exp $
// *
// * Copyright (c) 2010 HEB
// * All rights reserved.
// *
// * This software is the confidential and proprietary information
// * of HEB.
// * 
// * COMMENTED AS NOT BEING USED
// */
//package ph.com.sunlife.wms.logging.aop;
//
//public class WarnLog4JProfiler extends AbstractLog4JProfiler {
//
//	/* (non-Javadoc)
//	 * @see com.heb.wtw.aop.logging.AbstractProfiler#log(java.lang.String)
//	 */
//	@Override
//	protected void log(String message) {
//		logger.warn(message);
//	}
//
//	/* (non-Javadoc)
//	 * @see com.heb.wtw.aop.logging.AbstractProfiler#log(java.lang.String, java.lang.Throwable)
//	 */
//	@Override
//	protected void log(String message, Throwable t) {
//		logger.warn(message, t);
//	}
//
//}
