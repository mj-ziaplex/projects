package ph.com.sunlife.wms.dao.domain;

import java.io.Serializable;

public class DCRCashierNoCollection implements Serializable {
	
	private static final long serialVersionUID = -1889536561180669136L;

	private Long dcrId;
	
	private String acf2id;

	public Long getDcrId() {
		return dcrId;
	}

	public void setDcrId(Long dcrId) {
		this.dcrId = dcrId;
	}

	public String getAcf2id() {
		return acf2id;
	}

	public void setAcf2id(String acf2id) {
		this.acf2id = acf2id;
	}

}
