package ph.com.sunlife.wms.services;

import java.util.Map;

import com.ibatis.sqlmap.engine.mapping.parameter.ParameterMap;

import ph.com.sunlife.wms.services.bo.DCRVDILBO;
import ph.com.sunlife.wms.services.bo.VDILCollectionTotalsBO;
import ph.com.sunlife.wms.services.exception.ServiceException;

/**
 * The service interface for VDIL Creation.
 * 
 * @author Zainal Limpao
 * @author Josephus Sardan
 * 
 */
public interface DCRVDILService {

	/**
	 * Creates a new VDIL instance.
	 * 
	 * @param dcrCashierId
	 * @return
	 * @throws ServiceException
	 */
	DCRVDILBO createNewVdil(Long dcrCashierId) throws ServiceException;

	/**
	 * Retrieves VDIL instance given cashier workitem id and user id.
	 * 
	 * @param dcrCashierId
	 * @param acf2id
	 * @return
	 * @throws ServiceException
	 */
	DCRVDILBO getVdil(Long dcrCashierId, String acf2id) throws ServiceException;

	/**
	 * Updates values of {@link DCRVDILBO}.
	 * 
	 * @param businessObject
	 * @return
	 * @throws ServiceException
	 */
	DCRVDILBO updateVdil(DCRVDILBO businessObject) throws ServiceException;

	/**
	 * Updates values of {@link DCRVDILBO} given a {@link ParameterMap}.
	 * 
	 * @param map
	 * @return
	 * @throws ServiceException
	 * @see #getVdil(Long, String)
	 */
	DCRVDILBO updateVdil(Map<String, Object> map) throws ServiceException;

	/**
	 * Generates Collection Totals from PPA given various specific product types
	 * and payments.
	 * 
	 * @param vdil
	 * @return
	 * @throws ServiceException
	 */
	VDILCollectionTotalsBO generateCollectionTotals(DCRVDILBO vdil)
			throws ServiceException;

}
