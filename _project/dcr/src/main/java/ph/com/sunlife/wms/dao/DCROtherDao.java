package ph.com.sunlife.wms.dao;

import ph.com.sunlife.wms.dao.domain.DCROther;
import ph.com.sunlife.wms.dao.exceptions.WMSDaoException;

public interface DCROtherDao extends WMSDao<DCROther>{

	/**
	 * Updates database entry given DCROther entity and returns it
	 * 
	 * @param dcrCashierId
	 * @return
	 * @throws WMSDaoException
	 */
	void updateOther(DCROther dcrOther) throws WMSDaoException;

}
