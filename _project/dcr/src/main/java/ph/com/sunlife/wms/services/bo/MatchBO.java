package ph.com.sunlife.wms.services.bo;

public class MatchBO {
	
	private String filenetId;
	
	private Long recordId;
	
	private String recordLocation;
	
	private String dcrDate;
	
	private String ccId;
	
	private String description;
	
	private String fileType;
	
	private Integer filesize;

	public Integer getFilesize() {
		return filesize;
	}

	public void setFilesize(Integer filesize) {
		this.filesize = filesize;
	}

	public String getFilenetId() {
		return filenetId;
	}

	public void setFilenetId(String filenetId) {
		this.filenetId = filenetId;
	}

	public Long getRecordId() {
		return recordId;
	}

	public void setRecordId(Long recordId) {
		this.recordId = recordId;
	}

	public String getRecordLocation() {
		return recordLocation;
	}

	public void setRecordLocation(String recordLocation) {
		this.recordLocation = recordLocation;
	}

	public String getDcrDate() {
		return dcrDate;
	}

	public void setDcrDate(String dcrDate) {
		this.dcrDate = dcrDate;
	}

	public String getCcId() {
		return ccId;
	}

	public void setCcId(String ccId) {
		this.ccId = ccId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getFileType() {
		return fileType;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}
	
	

}
