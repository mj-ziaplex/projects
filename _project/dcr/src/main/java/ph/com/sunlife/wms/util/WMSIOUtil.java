package ph.com.sunlife.wms.util;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Multipurpose Utility Class for Java I/O.
 * 
 * @author Zainal Limpao
 * 
 */
public class WMSIOUtil {

	/**
	 * To byte array.
	 * 
	 * @param input
	 *            the input
	 * @return the byte[]
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static byte[] toByteArray(InputStream input) throws IOException {
		BufferedInputStream bufferedInputStream = new BufferedInputStream(input);
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		int ch;
		while ((ch = bufferedInputStream.read()) != -1) {
			outputStream.write(ch);
		}
		return outputStream.toByteArray();
	}
}
