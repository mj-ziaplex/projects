/*
 * ** Date: Feb 2016
 * ** Dev: Cshells Sayan
 * ** Desc: Added new product ADA(MF22) to be able to filter and display in DCR
 * 
 * ** Date: May 2017
 * ** Dev: Cshells Sayan
 * ** Desc: Modified methods for a more dynamic approach; SR-WF-16-00016 - SLAMC - New USD Fund (WMS-DCR)
 */
package ph.com.sunlife.wms.dao.domain;

public enum ProductType {

    // TODO: fill this with actual product types across all companies
	// Modified for SR-WF-16-00016 - SLAMC - New USD Fund (WMS-DCR)
	SLAMCI_MF1(1L),
    SLAMCI_MF2(2L),
    SLAMCI_MF3(3L),
    SLAMCI_MF4(4L),
    SLAMCI_MF5(5L),
    SLAMCI_MF6(6L),
    SLAMCI_MF7(7L),
    SLAMCI_MF20(8L),
    SLAMCI_MF21(9L),
    SLIFPI_GAF(11L),
    SLIFPI_PN(10L),
    GREPA_P_TRADVUL(12L),
    GREPA_D_TRADVUL(13L),
    GREPA_P_UNCONVERTED(14L),
    GREPA_D_UNCONVERTED(15L),
    SLOCPI_P_TRADVUL(16L),
    SLOCPI_D_TRADVUL(17L),
    SLOCPI_GROUPLIFE(18L),
    OTHER_PESO(19L),
    OTHER_DOLLAR(20L),
    GREPA_GP_GROUPLIFE(21L),
    GREPA_GD_GROUPLIFE(22L),
    SLAMCI_MF8(23L),
    SLAMCI_MF9(24L),
    SLAMCI_MF10(25L),
    SLAMCI_MF11(26L),
    SLAMCI_PCO(27L), // Added for PCO
    SLAMCI_ADA(28L), // Added for ADA
    SLAMCI_MF12(29L); // Added for SR-WF-16-00016 - SLAMC - New USD Fund (WMS-DCR)
    
    private Long id;
    private Company company;
    private Currency currency;
    private String productName;
    private String productCode;
    
    // Added for SR-WF-16-00016 - SLAMC - New USD Fund (WMS-DCR)
    private String fltProductCode;
    private String productPaySubCurr;
    
    ProductType(final Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public Company getCompany() {
        return company;
    }

    public String getProductName() {
        return productName;
    }

    public String getProductCode() {
        return productCode;
    }

    public Currency getCurrency() {
        return currency;
    }

    public static ProductType getById(Long id) {
        for (ProductType type : ProductType.values()) {
            if (type.getId().equals(id)) {
                return type;
            }
        }
        return null;
    }

    /*
     * Return ProductType Object based on product code and product sub-currency
     * Modified for SR-WF-16-00016 - SLAMC - New USD Fund (WMS-DCR)
     */
    public static ProductType prodCodeCurrencyToProductType(
            String prodCode,
            String paySubCur) {

    	if ("VL".equals(prodCode)) {
           prodCode = "IL";
        } else if("RV".equals(prodCode)) {
           prodCode = "RL";
    	}
        
        ProductType pTtype = null;
        for (ProductType type : ProductType.values()) {
            
            if (type.getFltProductCode().equals(prodCode) && type.getProductPaySubCurr().equals(paySubCur)) {
                pTtype = type;
                break;
            }
        }

        return pTtype;
    }
    
    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @param company the company to set
     */
    public void setCompany(Company company) {
        this.company = company;
    }

    /**
     * @param currency the currency to set
     */
    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    /**
     * @param productName the productName to set
     */
    public void setProductName(String productName) {
        this.productName = productName;
    }
    

    /**
     * @param productCode the productCode to set
     */
    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }
    
    /**
     * Added for SR-WF-16-00016 - SLAMC - New USD Fund (WMS-DCR)
     * @return the fltProductCode
     */
    public String getFltProductCode() {
        return fltProductCode;
    }

    /**
     * Added for SR-WF-16-00016 - SLAMC - New USD Fund (WMS-DCR)
     * @param fltProductCode the fltProductCode to set
     */
    public void setFltProductCode(String fltProductCode) {
        this.fltProductCode = fltProductCode;
    }

    /**
     * Added for SR-WF-16-00016 - SLAMC - New USD Fund (WMS-DCR)
     * @return the productPaySubCurr
     */
    public String getProductPaySubCurr() {
        return productPaySubCurr;
    }

    /**
     * Added for SR-WF-16-00016 - SLAMC - New USD Fund (WMS-DCR)
     * @param productPaySubCurr the productPaySubCurr to set
     */
    public void setProductPaySubCurr(String productPaySubCurr) {
        this.productPaySubCurr = productPaySubCurr;
    }
}
