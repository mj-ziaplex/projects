package ph.com.sunlife.wms.util;

import java.util.TreeMap;

/**
 * A custom Map with String and Object as Key-Value parameters. Object that
 * serve as the constructor parameter is translated to a map with the field
 * names as key and the file values as the map's value.
 *
 * @author Zainal Limpao
 *
 */
public class ParameterMap extends TreeMap<String, Object> {

    // Change according to Healthcheck
    // *Problem was identified due to being hard coded
    // *Change was comment code block as not being used
    //private static final long serialVersionUID = 7666322395592345892L;
    private Object objectSource;

    /**
     * Null constructor.
     */
    public ParameterMap() {
    }

    /**
     * Constructor with an object.
     *
     * @param objectSource
     */
    public ParameterMap(Object objectSource) {
        WMSBeanUtils.copyToMap(objectSource, this);
        this.objectSource = objectSource;
    }

    public Object getObjectSource() {
        return objectSource;
    }

    public void setObjectSource(Object objectSource) {
        super.clear();

        WMSBeanUtils.copyToMap(objectSource, this);
        this.objectSource = objectSource;
    }
}
