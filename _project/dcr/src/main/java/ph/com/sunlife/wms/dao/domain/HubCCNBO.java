package ph.com.sunlife.wms.dao.domain;

import java.io.Serializable;
import java.util.Date;

public class HubCCNBO implements Serializable {

	private static final long serialVersionUID = -428750941975043925L;

	private String hccnHUBID;
	private String hccnCCID;
	private String hccnNBOID;	
	private String hccnActive;
	private String hccnCREUser;
	private Date hccnCREDate;
	private String hccnUPDUser;
	private Date hccnUPDDate;

	public String getHccnHUBID() {
		return hccnHUBID;
	}
	public void setHccnHUBID(String hccnHUBID) {
		this.hccnHUBID = hccnHUBID;
	}
	public String getHccnCCID() {
		return hccnCCID;
	}
	public void setHccnCCID(String hccnCCID) {
		this.hccnCCID = hccnCCID;
	}
	public String getHccnNBOID() {
		return hccnNBOID;
	}
	public void setHccnNBOID(String hccnNBOID) {
		this.hccnNBOID = hccnNBOID;
	}
	public String getHccnActive() {
		return hccnActive;
	}
	public void setHccnActive(String hccnActive) {
		this.hccnActive = hccnActive;
	}
	public String getHccnCREUser() {
		return hccnCREUser;
	}
	public void setHccnCREUser(String hccnCREUser) {
		this.hccnCREUser = hccnCREUser;
	}
	public Date getHccnCREDate() {
		return hccnCREDate;
	}
	public void setHccnCREDate(Date hccnCREDate) {
		this.hccnCREDate = hccnCREDate;
	}
	public String getHccnUPDUser() {
		return hccnUPDUser;
	}
	public void setHccnUPDUser(String hccnUPDUser) {
		this.hccnUPDUser = hccnUPDUser;
	}
	public Date getHccnUPDDate() {
		return hccnUPDDate;
	}
	public void setHccnUPDDate(Date hccnUPDDate) {
		this.hccnUPDDate = hccnUPDDate;
	}
	@Override
	public String toString() {
		return "HubCCNBO [hccnHUBID=" + hccnHUBID + ", hccnCCID=" + hccnCCID
				+ ", hccnNBOID=" + hccnNBOID + ", hccnActive=" + hccnActive
				+ ", hccnCREUser=" + hccnCREUser + ", hccnCREDate="
				+ hccnCREDate + ", hccnUPDUser=" + hccnUPDUser
				+ ", hccnUPDDate=" + hccnUPDDate + "]";
	}	
}
