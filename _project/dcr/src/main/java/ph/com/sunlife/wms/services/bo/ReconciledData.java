package ph.com.sunlife.wms.services.bo;

import java.util.Date;
import java.util.List;

public class ReconciledData {

	private Long dcrId;
	
	private String userId;
	
	private List<Integer> reconciledRows;
	
	private Date dateTime;

	public Long getDcrId() {
		return dcrId;
	}

	public void setDcrId(Long dcrId) {
		this.dcrId = dcrId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public List<Integer> getReconciledRows() {
		return reconciledRows;
	}

	public void setReconciledRows(List<Integer> reconciledRows) {
		this.reconciledRows = reconciledRows;
	}

	public Date getDateTime() {
		return dateTime;
	}

	public void setDateTime(Date dateTime) {
		this.dateTime = dateTime;
	}
	
}
