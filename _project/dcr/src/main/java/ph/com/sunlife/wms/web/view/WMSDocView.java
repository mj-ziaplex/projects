package ph.com.sunlife.wms.web.view;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.springframework.web.servlet.view.AbstractView;

import ph.com.sunlife.wms.util.WMSConstants;

public class WMSDocView extends AbstractView {

	private static final String FILE_DATA_KEY = "fileData";

	public WMSDocView(String docType) {
		String contentType = null;

		if (WMSConstants.FILE_TYPE_DOC.equals(docType)) {
			contentType = "application/msword";
		} else if (WMSConstants.FILE_TYPE_DOCX.equals(docType)) {
			contentType = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
		}

		setContentType(contentType);
	}

	@SuppressWarnings("rawtypes")
	@Override
	protected void renderMergedOutputModel(Map model,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		final byte[] fileData = (byte[]) model.get(FILE_DATA_KEY);
		IOUtils.write(fileData, response.getOutputStream());
	}
}
