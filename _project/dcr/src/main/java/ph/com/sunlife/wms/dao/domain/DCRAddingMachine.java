package ph.com.sunlife.wms.dao.domain;

public class DCRAddingMachine extends Entity {

	private DCRCashier dcrCashier = new DCRCashier();

	private byte[] addingMachineFile;
	
	private String dcrAddingMachineVrsSerId;

	public DCRCashier getDcrCashier() {
		return dcrCashier;
	}

	public void setDcrCashier(DCRCashier dcrCashier) {
		this.dcrCashier = dcrCashier;
	}

	public void setDcrCashier(Long dcrCashierId) {
		DCRCashier dcrCashier = new DCRCashier();
		dcrCashier.setId(dcrCashierId);
		this.dcrCashier = dcrCashier;
	}

	public byte[] getAddingMachineFile() {
		return addingMachineFile;
	}

	public void setAddingMachineFile(byte[] addingMachineFile) {
		this.addingMachineFile = addingMachineFile;
	}

	public String getDcrAddingMachineVrsSerId() {
		return dcrAddingMachineVrsSerId;
	}

	public void setDcrAddingMachineVrsSerId(String dcrAddingMachineVrsSerId) {
		this.dcrAddingMachineVrsSerId = dcrAddingMachineVrsSerId;
	}
	
}
