package ph.com.sunlife.wms.dao.impl;

import java.sql.SQLException;
import java.util.List;

import org.springframework.orm.ibatis.SqlMapClientCallback;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.ibatis.sqlmap.client.SqlMapExecutor;

import ph.com.sunlife.wms.dao.CashierDao;
import ph.com.sunlife.wms.dao.domain.Cashier;
import ph.com.sunlife.wms.dao.exceptions.WMSDaoException;

/**
 * The implementing class of {@link CashierDao}.
 * 
 * @author Zainal LImpao
 * 
 */
public class CashierDaoImpl extends SqlMapClientDaoSupport implements
		CashierDao {

	private static final String NAMESPACE = "Cashier";

	/*
	 * (non-Javadoc)
	 * 
	 * @see ph.com.sunlife.wms.dao.CashierDao#getCashier(java.lang.String)
	 */
	@Override
	public Cashier getCashier(final String acf2id) throws WMSDaoException {
		Cashier cashier = null;
		try {
			cashier = (Cashier) getSqlMapClientTemplate().execute(
					new SqlMapClientCallback() {

						@Override
						public Object doInSqlMapClient(SqlMapExecutor executor)
								throws SQLException {
							return executor.queryForObject(NAMESPACE
									+ ".getCashier", acf2id);
						}
					});
		} catch (Exception e) {
			throw new WMSDaoException(e);
		}
		return cashier;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<String> getCCNameAndCodes() throws WMSDaoException {
		return (List<String>) getSqlMapClientTemplate().queryForList(
				NAMESPACE + ".getCCNameAndCodes");
	}

	public String getCCNameById(String siteCode) throws WMSDaoException {

		return (String) getSqlMapClientTemplate().queryForObject(
				NAMESPACE + ".getCCNameById", siteCode);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Cashier> getMultipleCashierInfo(
			final List<String> cashierUserIds) throws WMSDaoException {
		try {
			return getSqlMapClientTemplate().executeWithListResult(
					new SqlMapClientCallback() {

						@Override
						public Object doInSqlMapClient(SqlMapExecutor executor)
								throws SQLException {
							return executor
									.queryForList(NAMESPACE
											+ ".getMultipleCashierInfo",
											cashierUserIds);
						}
					});
		} catch (Exception e) {
			throw new WMSDaoException(e);
		}
	}

}
