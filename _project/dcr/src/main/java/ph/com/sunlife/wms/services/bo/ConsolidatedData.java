package ph.com.sunlife.wms.services.bo;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ph.com.sunlife.wms.util.WMSDateUtil;

public class ConsolidatedData {

	private Long dcrId;

	private String customerCenterName;

	private String ccId;

	private String status;

	private Date dcrDate;

	private String formattedDcrDate;
	
	private String dcrDateStr;

	private List<ConsolidatedCashierTotals> cashierTotals;

	private List<DCRStepProcessorNoteBO> notes;

	// Do not use this anymore
	private List<Integer> reconciledRowNums;

	private List<DCRCommentBO> commentBusinessObjects;

	private List<CashierWorkItem> cashierWorkItems;

	private Map<String, Object> userToAttachmentsMap;

	private List<DCROtherBO> dcrOthers;

	private List<CashierBO> inactiveCashiers;
	
	private List<DCRCashDepositSlipBO> dcrCashDepositSlipBO;
	
	private List<DCRChequeDepositSlipBO> dcrChequeDepositSlipBO;
	
	private List<DCRCashDepositSlipBO> validatedCashDepositSlips;
	
	private List<DCRChequeDepositSlipBO> validatedChequeDepositSlips;

	private Map<String, List<CashierConfirmationBO>> cashierConfirmationsMap = new HashMap<String, List<CashierConfirmationBO>>();
	
	private Map<String, List<AttachmentBO>> snapshotAttachments;
	
	private List<ReconciledData> reconciledData;
	
	private boolean mdsReconciled;
	
	private boolean nonDay1;
	
	private Map<String, String> userIdShortNameMap;
	
	// ${reconciledData.userId} ${reconciledData.reconciledRows}
	
	public boolean isNonDay1() {
		return nonDay1;
	}

	public void setNonDay1(boolean nonDay1) {
		this.nonDay1 = nonDay1;
	}

	public List<ReconciledData> getReconciledData() {
		return reconciledData;
	}

	public void setReconciledData(List<ReconciledData> reconciledData) {
		this.reconciledData = reconciledData;
	}

	public String getDcrDateStr() {
		return dcrDateStr;
	}

	public void setDcrDateStr(String dcrDateStr) {
		this.dcrDateStr = dcrDateStr;
	}

	public List<DCRChequeDepositSlipBO> getDcrChequeDepositSlipBO() {
		return dcrChequeDepositSlipBO;
	}

	public void setDcrChequeDepositSlipBO(
			List<DCRChequeDepositSlipBO> dcrChequeDepositSlipBO) {
		this.dcrChequeDepositSlipBO = dcrChequeDepositSlipBO;
	}

	public List<DCRCashDepositSlipBO> getDcrCashDepositSlipBO() {
		return dcrCashDepositSlipBO;
	}

	public void setDcrCashDepositSlipBO(
			List<DCRCashDepositSlipBO> dcrCashDepositSlipBO) {
		this.dcrCashDepositSlipBO = dcrCashDepositSlipBO;
	}

	public List<CashierBO> getInactiveCashiers() {
		return inactiveCashiers;
	}

	public void setInactiveCashiers(List<CashierBO> inactiveCashiers) {
		this.inactiveCashiers = inactiveCashiers;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public List<DCRCommentBO> getCommentBusinessObjects() {
		return commentBusinessObjects;
	}

	public void setCommentBusinessObjects(
			List<DCRCommentBO> commentBusinessObjects) {
		this.commentBusinessObjects = commentBusinessObjects;
	}

	public Long getDcrId() {
		return dcrId;
	}

	public void setDcrId(Long dcrId) {
		this.dcrId = dcrId;
	}

	public List<ConsolidatedCashierTotals> getCashierTotals() {
		return cashierTotals;
	}

	public void setCashierTotals(List<ConsolidatedCashierTotals> cashierTotals) {
		this.cashierTotals = cashierTotals;
	}

	public String getCustomerCenterName() {
		return customerCenterName;
	}

	public void setCustomerCenterName(String customerCenterName) {
		this.customerCenterName = customerCenterName;
	}

	public String getCcId() {
		return ccId;
	}

	public void setCcId(String ccId) {
		this.ccId = ccId;
	}

	public Date getDcrDate() {
		return dcrDate;
	}

	public void setDcrDate(Date dcrDate) {
		this.dcrDate = dcrDate;
		this.formattedDcrDate = WMSDateUtil.toFormatMMMMddyyyy(dcrDate);
		this.dcrDateStr = WMSDateUtil.toFormattedDateStr(dcrDate);
	}

	public String getFormattedDcrDate() {
		return formattedDcrDate;
	}

	public void setFormattedDcrDate(String formattedDcrDate) {
		this.formattedDcrDate = formattedDcrDate;
	}

	public List<DCRStepProcessorNoteBO> getNotes() {
		return notes;
	}

	public void setNotes(List<DCRStepProcessorNoteBO> notes) {
		this.notes = notes;
	}

	public List<Integer> getReconciledRowNums() {
		return reconciledRowNums;
	}

	public void setReconciledRowNums(List<Integer> reconciledRowNums) {
		this.reconciledRowNums = reconciledRowNums;
	}

	public List<CashierWorkItem> getCashierWorkItems() {
		return cashierWorkItems;
	}

	public void setCashierWorkItems(List<CashierWorkItem> cashierWorkItems) {
		this.cashierWorkItems = cashierWorkItems;
	}

	public Map<String, Object> getUserToAttachmentsMap() {
		return userToAttachmentsMap;
	}

	public void setUserToAttachmentsMap(Map<String, Object> userToAttachmentsMap) {
		this.userToAttachmentsMap = userToAttachmentsMap;
	}

	public List<DCROtherBO> getDcrOthers() {
		return dcrOthers;
	}

	public void setDcrOthers(List<DCROtherBO> dcrOthers) {
		this.dcrOthers = dcrOthers;
	}

	public Map<String, List<CashierConfirmationBO>> getCashierConfirmationsMap() {
		return cashierConfirmationsMap;
	}

	public void setCashierConfirmationsMap(
			Map<String, List<CashierConfirmationBO>> cashierConfirmationsMap) {
		this.cashierConfirmationsMap = cashierConfirmationsMap;
	}

	public Map<String, List<AttachmentBO>> getSnapshotAttachments() {
		return snapshotAttachments;
	}

	public void setSnapshotAttachments(
			Map<String, List<AttachmentBO>> snapshotAttachments) {
		this.snapshotAttachments = snapshotAttachments;
	}

	public List<DCRCashDepositSlipBO> getValidatedCashDepositSlips() {
		return validatedCashDepositSlips;
	}

	public void setValidatedCashDepositSlips(
			List<DCRCashDepositSlipBO> validatedCashDepositSlips) {
		this.validatedCashDepositSlips = validatedCashDepositSlips;
	}

	public List<DCRChequeDepositSlipBO> getValidatedChequeDepositSlips() {
		return validatedChequeDepositSlips;
	}

	public void setValidatedChequeDepositSlips(
			List<DCRChequeDepositSlipBO> validatedChequeDepositSlips) {
		this.validatedChequeDepositSlips = validatedChequeDepositSlips;
	}

	public boolean isMdsReconciled() {
		return mdsReconciled;
	}

	public void setMdsReconciled(boolean mdsReconciled) {
		this.mdsReconciled = mdsReconciled;
	}

	public Map<String, String> getUserIdShortNameMap() {
		return userIdShortNameMap;
	}

	public void setUserIdShortNameMap(Map<String, String> userIdShortNameMap) {
		this.userIdShortNameMap = userIdShortNameMap;
	}

}
