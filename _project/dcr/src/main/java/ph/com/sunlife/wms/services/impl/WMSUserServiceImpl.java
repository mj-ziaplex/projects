package ph.com.sunlife.wms.services.impl;

import java.util.Date;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;

import ph.com.sunlife.wms.dao.UserGroupsDao;
import ph.com.sunlife.wms.dao.WMSUserDao;
import ph.com.sunlife.wms.dao.domain.HubCCNBO;
import ph.com.sunlife.wms.dao.domain.HubCCNBOUserGroup;
import ph.com.sunlife.wms.dao.domain.UserGroup;
import ph.com.sunlife.wms.dao.domain.UserGroupHub;
import ph.com.sunlife.wms.dao.domain.WMSUser;
import ph.com.sunlife.wms.dao.exceptions.WMSDaoException;
import ph.com.sunlife.wms.services.WMSUserService;
import ph.com.sunlife.wms.services.bo.AdminWmsUserBO;
import ph.com.sunlife.wms.services.exception.ServiceException;
import ph.com.sunlife.wms.util.WMSStringUtil;

//import com.sunlife.common.util.DateUtil;
public class WMSUserServiceImpl implements WMSUserService {

    private static final Logger LOGGER = Logger.getLogger(WMSUserServiceImpl.class);
    private WMSUserDao wmsUserDao;
    private UserGroupsDao userGroupsDao;

    public void setWmsUserDao(WMSUserDao wmsUserDao) {
        this.wmsUserDao = wmsUserDao;
    }

    public void setUserGroupsDao(UserGroupsDao userGroupsDao) {
        this.userGroupsDao = userGroupsDao;
    }

    @Override
    public boolean createWMSUser(WMSUser user) throws ServiceException {
        boolean isCreated = false;
        try {
            isCreated = wmsUserDao.createWMSUser(user);
        } catch (WMSDaoException e) {
            LOGGER.error("Error in " + WMSUserServiceImpl.class.getSimpleName() + ": " + e);
        }
        return isCreated;
    }

    @Override
    public List<WMSUser> getAllWMSUser() throws ServiceException {
        List<WMSUser> userList = null;
        try {
            userList = wmsUserDao.getAllWMSUser();
        } catch (WMSDaoException e) {
            LOGGER.error("Error in " + WMSUserServiceImpl.class.getSimpleName() + ": " + e);
        }
        return userList;
    }

    @Override
    public WMSUser getWMSUserById(String userId) throws ServiceException {
        List<WMSUser> userList = null;
        try {
            userList = wmsUserDao.getWMSUserById(userId);
        } catch (WMSDaoException e) {
            LOGGER.error("Error in " + WMSUserServiceImpl.class.getSimpleName() + ": " + e);
        }
        return userList.get(0);
    }

    @Override
    public boolean updateWMSUser(WMSUser user) throws ServiceException {
        boolean isUpdated = false;
        try {
            isUpdated = wmsUserDao.updateWMSUser(user);
        } catch (WMSDaoException e) {
            LOGGER.error("Error in " + WMSUserServiceImpl.class.getSimpleName() + ": " + e);
        }
        return isUpdated;
    }

    @Override
    public boolean deleteWMSUser(String userId) throws ServiceException {
        boolean isDeleted = false;
        try {
            isDeleted = wmsUserDao.deleteWMSUser(userId);
        } catch (WMSDaoException e) {
            LOGGER.error("Error in " + WMSUserServiceImpl.class.getSimpleName() + ": " + e);
        }
        return isDeleted;
    }

//	@Override
//	public boolean updateWMSUserLogStatus(WMSUser user, boolean isLogged) throws ServiceException {
//		boolean isUpdated = false;
//		try {
//			if (isLogged) {
//				isUpdated = wmsUserDao.updateWMSUserLogin(user);
//			} else {
//				isUpdated = wmsUserDao.updateWMSUserLogout(user);
//			}
//		} catch (WMSDaoException e) {
//			LOGGER.error("Error in " + WMSUserServiceImpl.class.getSimpleName() + ": "+  e);
//		}
//		return isUpdated;
//	}
    public List<UserGroupHub> getAllUserGroupHubList() throws ServiceException {
        List<UserGroupHub> groupHubList = null;
        try {
            groupHubList = userGroupsDao.getAllUserGroupHubList();
        } catch (WMSDaoException e) {
            LOGGER.error("Error in " + WMSUserServiceImpl.class.getSimpleName() + ": " + e);
        }
        return groupHubList;
    }

    public List<UserGroupHub> getUserGroupHubList(String userId)
            throws ServiceException {
        List<UserGroupHub> groupHubList = null;
        try {
            groupHubList = userGroupsDao.getUserGroupHubList(userId);
        } catch (WMSDaoException e) {
            LOGGER.error("Error in " + WMSUserServiceImpl.class.getSimpleName() + ": " + e);
        }
        return groupHubList;
    }

    public List<HubCCNBOUserGroup> getUserGroupHubByUserId(HubCCNBOUserGroup hub)
            throws ServiceException {
        List<HubCCNBOUserGroup> groupHubList = null;
        try {
            groupHubList = userGroupsDao.getUserGroupHubByUserId(hub);
        } catch (WMSDaoException e) {
            LOGGER.error("Error in " + WMSUserServiceImpl.class.getSimpleName() + ": " + e);
        }
        return groupHubList;
    }

    @Override
    public boolean createUserGroupHub(HubCCNBOUserGroup hub)
            throws ServiceException {
        boolean isCreated = false;
        try {
            isCreated = userGroupsDao.createUserGroupHub(hub);
        } catch (WMSDaoException e) {
            LOGGER.error("Error in " + WMSUserServiceImpl.class.getSimpleName() + ": " + e);
        }
        return isCreated;
    }

    @Override
    public boolean updateUserGroupHub(HubCCNBOUserGroup hub)
            throws ServiceException {
        boolean isUpdated = false;
        try {
            isUpdated = userGroupsDao.updateUserGroupHub(hub);
        } catch (WMSDaoException e) {
            LOGGER.error("Error in " + WMSUserServiceImpl.class.getSimpleName() + ": " + e);
        }
        return isUpdated;
    }

    @Override
    public boolean deleteUserGroupHub(HubCCNBOUserGroup hub)
            throws ServiceException {
        boolean isDeleted = false;
        try {
            isDeleted = userGroupsDao.deleteUserGroupHub(hub);
        } catch (WMSDaoException e) {
            LOGGER.error("Error in " + WMSUserServiceImpl.class.getSimpleName() + ": " + e);
        }
        return isDeleted;
    }

    @Override
    public boolean createUserGroup(UserGroup group) throws ServiceException {
        boolean isCreated = false;
        try {
            isCreated = userGroupsDao.createUserGroup(group);
        } catch (WMSDaoException e) {
            LOGGER.error("Error in " + WMSUserServiceImpl.class.getSimpleName() + ": " + e);
        }
        return isCreated;
    }

    @Override
    public boolean deleteUserGroup(UserGroup group) throws ServiceException {
        boolean isDeleted = false;
        try {
            isDeleted = userGroupsDao.deleteUserGroup(group);
        } catch (WMSDaoException e) {
            LOGGER.error("Error in " + WMSUserServiceImpl.class.getSimpleName() + ": " + e);
        }
        return isDeleted;
    }

    @Override
    public List<HubCCNBO> getHubCCNBO(HubCCNBO nbo) throws ServiceException {
        List<HubCCNBO> nboList = null;
        try {
            nboList = userGroupsDao.getHubCCNBO(nbo);
        } catch (WMSDaoException e) {
            LOGGER.error("Error in " + WMSUserServiceImpl.class.getSimpleName() + ": " + e);
        }
        return nboList;
    }

    @Override
    public void doWMSUserCreate(AdminWmsUserBO adminWmsUserBO, String sessUser) throws ServiceException {
        if (null != adminWmsUserBO) {
            WMSUser user = new WMSUser();
            user.setWmsuId(adminWmsUserBO.getWmsuId());
            user.setWmsuActive("Y".equalsIgnoreCase(adminWmsUserBO.getWmsuActiveOpt()));
//            user.setWmsuExpPwdDate(DateUtil.DateAddDays(new Date(), 30)); // Commented to remove usage of CommonUtils
            user.setWmsuName(adminWmsUserBO.getWmsuName());
            user.setWmsuPwd(" ");
            user.setWmsuCreUser(sessUser);
            user.setWmsuCreDate(new Date());
            user.setWmsuLongName(adminWmsUserBO.getWmsuName());
            user.setWmsuShortName(adminWmsUserBO.getWmsuId());
            if (createWMSUser(user)) {
                List<String> userGroups = adminWmsUserBO.getUserGroups();
                List<String> userHubs = adminWmsUserBO.getUserHubs();
                List<String> centerCodes = adminWmsUserBO.getCenterCodes();

                for (String ccId : centerCodes) {
                    ccId = WMSStringUtil.replaceEscChar(ccId);
                    for (String hub : userHubs) {
                        hub = WMSStringUtil.replaceEscChar(hub);
                        for (String group : userGroups) {
                            HubCCNBOUserGroup cuu = new HubCCNBOUserGroup();
                            cuu.setHccnugCCID(ccId);
                            cuu.setHccnugGRPID(group);
                            cuu.setHccnugHUBID(hub);
                            cuu.setHccnugWMSUID(user.getWmsuId());
                            List<HubCCNBOUserGroup> cuuResultList = getUserGroupHubByUserId(cuu);
                            if (CollectionUtils.isNotEmpty(cuuResultList)) {
                                cuu.setHccnugActive("1");
                                updateUserGroupHub(cuu);
                            } else {
                                cuu.setHccnugWMSUID(null);
                                cuu.setHccnugActive(null);
                                cuuResultList = getUserGroupHubByUserId(cuu);
                                for (HubCCNBOUserGroup cuuResults : cuuResultList) {
                                    if (null != cuuResults) {
                                        cuu.setHccnugWMSUID(cuuResults.getHccnugWMSUID());
                                        cuu.setHccnugGRPID(null);
                                        List<HubCCNBOUserGroup> cuuResults1 = getUserGroupHubByUserId(cuu);
                                        for (HubCCNBOUserGroup cuuResult : cuuResults1) {
                                            HubCCNBOUserGroup cuuNew = new HubCCNBOUserGroup();
                                            cuuNew.setHccnugActive("1");
                                            cuuNew.setHccnugCCID(cuuResult.getHccnugCCID());
                                            cuuNew.setHccnugGRPID(cuuResult.getHccnugGRPID());
                                            cuuNew.setHccnugHUBID(cuuResult.getHccnugHUBID());
                                            cuuNew.setHccnugWMSUID(user.getWmsuId());
                                            cuuNew.setHccnugCREUser(sessUser);
                                            cuuNew.setHccnugCREDate(new Date());
                                            cuuNew.setHccnugReceiveWork(cuuResult.getHccnugReceiveWork());
                                            cuuNew.setHccnugReceiveWorkGF(cuuResult.getHccnugReceiveWorkGF());

                                            HubCCNBO nbo = new HubCCNBO();
                                            nbo.setHccnCCID(cuuResult.getHccnugCCID());
                                            nbo.setHccnHUBID(cuuResult.getHccnugHUBID());
                                            cuuNew.setHccnugNBOID(getHubCCNBO(nbo).size() == 0 ? "(ALL)" : getHubCCNBO(nbo).get(0).getHccnNBOID());
                                            cuuNew.setHccnugCompanyCode("-".equals(adminWmsUserBO.getCompanyCode()) ? null : adminWmsUserBO.getCompanyCode());

                                            List<HubCCNBOUserGroup> cuuResultExist = getUserGroupHubByUserId(cuuNew);
                                            if (CollectionUtils.isEmpty(cuuResultExist)) {
                                                if (group.equalsIgnoreCase(cuuNew.getHccnugGRPID())) {
                                                    if (createUserGroupHub(cuuNew)) {
                                                        UserGroup userGroup = new UserGroup();
                                                        userGroup.setUgActive("1");
                                                        userGroup.setUgCREDate(new Date());
                                                        userGroup.setUgCREUser(sessUser);
                                                        userGroup.setUgPrimaryGroup("0");
                                                        userGroup.setUgGRPID(cuuResult.getHccnugGRPID());
                                                        userGroup.setUgWMSUID(user.getWmsuId());
                                                        userGroup.setUgReceiveWork("1");
                                                        createUserGroup(userGroup);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    @Override
    public void doWMSUserUpdate(AdminWmsUserBO adminWmsUserBO, String sessUser)
            throws ServiceException {
        if (null != adminWmsUserBO) {
            WMSUser userEntity = getWMSUserById(adminWmsUserBO.getWmsuId());
            WMSUser user = new WMSUser();
            user.setWmsuId(adminWmsUserBO.getWmsuId());
            user.setWmsuActive("Y".equalsIgnoreCase(adminWmsUserBO
                    .getWmsuActiveOpt()));
            user.setWmsuExpPwdDate(userEntity.getWmsuExpPwdDate());
            user.setWmsuName(adminWmsUserBO.getWmsuName());
            user.setWmsuPwd(userEntity.getWmsuPwd());
            user.setWmsuCreUser(userEntity.getWmsuCreUser());
            user.setWmsuCreDate(userEntity.getWmsuCreDate());
            user.setWmsuUpdUser(sessUser);
            user.setWmsuUpdDate(new Date());
            user.setWmsuLongName(adminWmsUserBO.getWmsuName());
            user.setWmsuShortName(adminWmsUserBO.getWmsuId());
            if (updateWMSUser(user)) {
                List<String> userGroups = adminWmsUserBO.getUserGroups();
                List<String> userHubs = adminWmsUserBO.getUserHubs();
                List<String> centerCodes = adminWmsUserBO.getCenterCodes();

                List<UserGroupHub> groupList = getUserGroupHubList(user
                        .getWmsuId());
                for (UserGroupHub ugh : groupList) {
                    String group = ugh.getGroupId();
                    HubCCNBOUserGroup cuu = new HubCCNBOUserGroup();
                    cuu.setHccnugActive("0");
                    cuu.setHccnugWMSUID(user.getWmsuId());
                    cuu.setHccnugGRPID(group);
                    if (deleteUserGroupHub(cuu)) {
                        UserGroup userGroup = new UserGroup();
                        userGroup.setUgGRPID(group);
                        userGroup.setUgWMSUID(user.getWmsuId());
                        deleteUserGroup(userGroup);
                    }
                }

                for (String ccId : centerCodes) {
                    ccId = WMSStringUtil.replaceEscChar(ccId);
                    for (String hub : userHubs) {
                        hub = WMSStringUtil.replaceEscChar(hub);
                        for (String group : userGroups) {
                            HubCCNBOUserGroup cuu = new HubCCNBOUserGroup();
                            // cuu.setHccnugActive("1");
                            cuu.setHccnugCCID(ccId);
                            cuu.setHccnugGRPID(group);
                            cuu.setHccnugHUBID(hub);
                            cuu.setHccnugWMSUID(user.getWmsuId());
                            List<HubCCNBOUserGroup> cuuResultList = getUserGroupHubByUserId(cuu);
                            if (CollectionUtils.isNotEmpty(cuuResultList)) {
                                cuu.setHccnugActive("1");
                                cuu.setHccnugCompanyCode("-"
                                        .equals(adminWmsUserBO.getCompanyCode()) ? null
                                        : adminWmsUserBO.getCompanyCode());
                                for (HubCCNBOUserGroup cuuResult : cuuResultList) {
                                    if (updateUserGroupHub(cuu)) {
                                        UserGroup userGroup = new UserGroup();
                                        userGroup.setUgActive("1");
                                        userGroup.setUgCREDate(new Date());
                                        userGroup.setUgCREUser(sessUser);
                                        userGroup.setUgPrimaryGroup("0");
                                        userGroup.setUgGRPID(cuuResult
                                                .getHccnugGRPID());
                                        userGroup.setUgWMSUID(user.getWmsuId());
                                        userGroup.setUgReceiveWork("1");
                                        createUserGroup(userGroup);
                                    }
                                }
                            } else {
                                cuu.setHccnugWMSUID(null);
                                cuu.setHccnugActive(null);
                                cuuResultList = getUserGroupHubByUserId(cuu);
                                for (HubCCNBOUserGroup cuuResults : cuuResultList) {
                                    if (null != cuuResults) {
                                        cuu.setHccnugWMSUID(cuuResults
                                                .getHccnugWMSUID());
                                        cuu.setHccnugGRPID(null);
                                        List<HubCCNBOUserGroup> cuuResults1 = getUserGroupHubByUserId(cuu);
                                        for (HubCCNBOUserGroup cuuResult : cuuResults1) {
                                            HubCCNBOUserGroup cuuNew = new HubCCNBOUserGroup();
                                            cuuNew.setHccnugActive("1");
                                            cuuNew.setHccnugCCID(cuuResult
                                                    .getHccnugCCID());
                                            cuuNew.setHccnugGRPID(cuuResult
                                                    .getHccnugGRPID());
                                            cuuNew.setHccnugHUBID(cuuResult
                                                    .getHccnugHUBID());
                                            cuuNew.setHccnugWMSUID(user
                                                    .getWmsuId());
                                            cuuNew.setHccnugCREUser(sessUser);
                                            cuuNew.setHccnugCREDate(new Date());
                                            cuuNew.setHccnugReceiveWork(cuuResult
                                                    .getHccnugReceiveWork());
                                            cuuNew.setHccnugReceiveWorkGF(cuuResult
                                                    .getHccnugReceiveWorkGF());
                                            HubCCNBO nbo = new HubCCNBO();
                                            nbo.setHccnCCID(cuuResult
                                                    .getHccnugCCID());
                                            nbo.setHccnHUBID(cuuResult
                                                    .getHccnugHUBID());
                                            cuuNew.setHccnugNBOID(getHubCCNBO(
                                                    nbo).size() == 0 ? "(ALL)"
                                                    : getHubCCNBO(nbo).get(0)
                                                    .getHccnNBOID());
                                            cuuNew.setHccnugCompanyCode("-"
                                                    .equals(adminWmsUserBO
                                                    .getCompanyCode()) ? null
                                                    : adminWmsUserBO
                                                    .getCompanyCode());
                                            List<HubCCNBOUserGroup> cuuResultExist = getUserGroupHubByUserId(cuuNew);
                                            if (CollectionUtils
                                                    .isEmpty(cuuResultExist)) {
                                                if (group
                                                        .equalsIgnoreCase(cuuNew
                                                        .getHccnugGRPID())) {
                                                    if (createUserGroupHub(cuuNew)) {
                                                        UserGroup userGroup = new UserGroup();
                                                        userGroup
                                                                .setUgActive("1");
                                                        userGroup
                                                                .setUgCREDate(new Date());
                                                        userGroup
                                                                .setUgCREUser(sessUser);
                                                        userGroup
                                                                .setUgPrimaryGroup("0");
                                                        userGroup
                                                                .setUgGRPID(cuuResult
                                                                .getHccnugGRPID());
                                                        userGroup
                                                                .setUgWMSUID(user
                                                                .getWmsuId());
                                                        userGroup
                                                                .setUgReceiveWork("1");
                                                        createUserGroup(userGroup);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

            }

        }
    }
}
