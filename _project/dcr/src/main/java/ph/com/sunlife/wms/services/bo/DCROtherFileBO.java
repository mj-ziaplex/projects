package ph.com.sunlife.wms.services.bo;

import java.util.Date;

public class DCROtherFileBO {

	private Long id;

	private Long dcrFileSize;

	private byte[] dcrFile;

	private Date dcrFileUploadDate;

	private String dcrFileDescription;
	
	private DCRCashierBO dcrCashier;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getDcrFileSize() {
		return dcrFileSize;
	}

	public void setDcrFileSize(Long dcrFileSize) {
		this.dcrFileSize = dcrFileSize;
	}

	public byte[] getDcrFile() {
		return dcrFile;
	}

	public void setDcrFile(byte[] dcrFile) {
		this.dcrFile = dcrFile;
	}

	public Date getDcrFileUploadDate() {
		return dcrFileUploadDate;
	}

	public void setDcrFileUploadDate(Date dcrFileUploadDate) {
		this.dcrFileUploadDate = dcrFileUploadDate;
	}

	public String getDcrFileDescription() {
		return dcrFileDescription;
	}

	public void setDcrFileDescription(String dcrFileDescription) {
		this.dcrFileDescription = dcrFileDescription;
	}

	public DCRCashierBO getDcrCashier() {
		return dcrCashier;
	}

	public void setDcrCashier(DCRCashierBO dcrCashier) {
		this.dcrCashier = dcrCashier;
	}
	
	public void setDcrCashier(Long dcrCashierId) {
		DCRCashierBO dcrCashier = new DCRCashierBO();
		dcrCashier.setId(dcrCashierId);
		this.setDcrCashier(dcrCashier);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DCROtherFileBO other = (DCROtherFileBO) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}	
	
}
