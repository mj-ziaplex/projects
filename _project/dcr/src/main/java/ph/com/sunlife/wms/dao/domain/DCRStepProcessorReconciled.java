package ph.com.sunlife.wms.dao.domain;

import java.util.Date;

public class DCRStepProcessorReconciled extends Entity {

	private Long dcrId;
	
	private String reconciledRowNums;
	
	private String userId;
	
	private Date dateTime;

	public Long getDcrId() {
		return dcrId;
	}

	public void setDcrId(Long dcrId) {
		this.dcrId = dcrId;
	}

	public String getReconciledRowNums() {
		return reconciledRowNums;
	}

	public void setReconciledRowNums(String reconciledRowNums) {
		this.reconciledRowNums = reconciledRowNums;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Date getDateTime() {
		return dateTime;
	}

	public void setDateTime(Date dateTime) {
		this.dateTime = dateTime;
	}
	
}
