package ph.com.sunlife.wms.services.bo;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import ph.com.sunlife.wms.util.WMSDateUtil;

public class DCRBO implements Comparable<DCRBO> {

	private Long id;

	private String ccId;

	private List<String> ccIdSet;

	private Date dcrDate;

	private Date dcrStartDate;

	private Date dcrEndDate;

	private Date requiredCompletionDate;

	private String status;

	private List<DCRCashierBO> dcrCashiers;

	private String workObjectNumber;

	private String formatedDateStr;

	private String ccName;

	private String centerCode;

	private String hubId;
	
	private String formatedRCDStr;
	
	private String createUserId;
	
	private Date createDate;
	
	private String updateUserId;
	
	private Date updateDate;
	
	private String formattedUpdateDate;
	
	private String uploaderName;
	
	public String getUploaderName() {
		return uploaderName;
	}

	public void setUploaderName(String uploaderName) {
		this.uploaderName = uploaderName;
	}

	public String getFormattedUpdateDate() {
		return formattedUpdateDate;
	}

	public void setFormattedUpdateDate(String formattedUpdateDate) {
		this.formattedUpdateDate = formattedUpdateDate;
	}

	public String getFormatedRCDStr() {
		return formatedRCDStr;
	}

	public void setFormatedRCDStr(String formatedRCDStr) {
		this.formatedRCDStr = formatedRCDStr;
	}

	public List<String> getCcIdSet() {
		return ccIdSet;
	}

	public void setCcIdSet(List<String> ccIdSet) {
		this.ccIdSet = ccIdSet;
	}

	public String getHubId() {
		return hubId;
	}

	public void setHubId(String hubId) {
		this.hubId = hubId;
	}

	public Date getDcrStartDate() {
		return dcrStartDate;
	}

	public void setDcrStartDate(Date dcrStartDate) {
		this.dcrStartDate = dcrStartDate;
	}

	public Date getDcrEndDate() {
		return dcrEndDate;
	}

	public void setDcrEndDate(Date dcrEndDate) {
		this.dcrEndDate = dcrEndDate;
	}

	public String getCcName() {
		return ccName;
	}

	public void setCcName(String ccName) {
		this.ccName = ccName;
	}

	public String getFormatedDateStr() {
		return formatedDateStr;
	}

	public void setFormatedDateStr(String formatedDateStr) {
		this.formatedDateStr = formatedDateStr;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCcId() {
		return ccId;
	}

	public void setCcId(String ccId) {
		this.ccId = ccId;
	}

	public Date getDcrDate() {
		return dcrDate;
	}

	public void setDcrDate(Date dcrDate) {
		this.dcrDate = dcrDate;
		this.formatedDateStr = StringUtils.upperCase(WMSDateUtil
				.toFormattedDateStr(dcrDate));
	}

	public Date getRequiredCompletionDate() {
		return requiredCompletionDate;
	}

	//Added for MR-WF-15-00112 
	public void setRequiredCompletionDate(Date requiredCompletionDate) {
		this.requiredCompletionDate = requiredCompletionDate;
		this.formatedRCDStr = StringUtils.upperCase(WMSDateUtil.toFormattedDateStrWithCompleteTime(requiredCompletionDate));
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public List<DCRCashierBO> getDcrCashiers() {
		return dcrCashiers;
	}

	public void setDcrCashiers(List<DCRCashierBO> dcrCashiers) {
		this.dcrCashiers = dcrCashiers;
	}

	public String getWorkObjectNumber() {
		return workObjectNumber;
	}

	public void setWorkObjectNumber(String workObjectNumber) {
		this.workObjectNumber = workObjectNumber;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DCRBO other = (DCRBO) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public int compareTo(DCRBO other) {
		int compareTo = 0;
		if (other.getDcrDate() != null && this.getDcrDate() != null) {
			compareTo = other.getDcrDate().compareTo(this.getDcrDate());
		}
		return compareTo;
	}

	public String getCenterCode() {
		return centerCode;
	}

	public void setCenterCode(String centerCode) {
		this.centerCode = centerCode;
	}

	public String getCreateUserId() {
		return createUserId;
	}

	public void setCreateUserId(String createUserId) {
		this.createUserId = createUserId;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getUpdateUserId() {
		return updateUserId;
	}

	public void setUpdateUserId(String updateUserId) {
		this.updateUserId = updateUserId;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
		this.formattedUpdateDate = WMSDateUtil.toFormattedDateStr(updateDate);
	}
	
}
