package ph.com.sunlife.wms.web.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.web.servlet.ModelAndView;

import ph.com.sunlife.wms.security.Role;
import ph.com.sunlife.wms.services.EmailService;
import ph.com.sunlife.wms.services.bo.ContactBO;
import ph.com.sunlife.wms.services.bo.UserSession;
import ph.com.sunlife.wms.services.bo.WMSEmail;
import ph.com.sunlife.wms.services.exception.ServiceException;
import ph.com.sunlife.wms.web.controller.form.EmailForm;
import ph.com.sunlife.wms.web.exception.ApplicationException;

/**
 * The Controller for the Email Common Tool launched via the DCR Step Processor.
 * 
 * @author Crisseljess Mendoza
 * @author Zainal Limpao
 * 
 */
public class EmailController extends AbstractSecuredMultiActionController {

	private static final Logger LOGGER = Logger
			.getLogger(EmailController.class);

	private static final String DCR_ID_KEY = "dcrId";

	private static final String IS_SENT_KEY = "isSent";

	private static final String ADDRESS_BOOK_KEY = "addressBook";

	private static final String VIEW_NAME = "emailScreenView";

	private static final String PAGE_TITLE = "Email";

	private static final String RELOAD_SP_KEY = "reloadSP";

	private static final String TITLE_KEY = "title";

	private EmailService emailService;

	/**
	 * The default method invoked if "Send Email" launch button has been
	 * selected.
	 * 
	 * @param request
	 * @param response
	 * @param form
	 * @return
	 * @throws ApplicationException
	 */
	public ModelAndView showEmailPage(HttpServletRequest request,
			HttpServletResponse response, EmailForm form)
			throws ApplicationException {
		LOGGER.info("Loading the Send Email Page...");

		try {
			Long dcrId = form.getDcrId();
			List<ContactBO> addressBook = emailService.getAddressBook();

			ModelAndView modelAndView = new ModelAndView();
			modelAndView.setViewName(VIEW_NAME);
			modelAndView.addObject(DCR_ID_KEY, dcrId);
			modelAndView.addObject(ADDRESS_BOOK_KEY, addressBook);
			modelAndView.addObject(IS_SENT_KEY, form.isSent());
			modelAndView.addObject(RELOAD_SP_KEY, form.isReloadSP());
			modelAndView.addObject(TITLE_KEY, "Create Email");
			modelAndView.addObject("contentFromErrorTag",convertCSVToList(form.getConcatErrorNameList()));//still to loop in jstl
			modelAndView.addObject("toFromErrorTag",form.getTo());
			modelAndView.addObject("ccFromErrorTag",form.getCc());
			modelAndView.addObject("subjectFromErrorTag",form.getSubject());
			modelAndView.addObject("fromErrorTag",form.isFromErrorTag());
			return modelAndView;
		} catch (ServiceException e) {
			throw new ApplicationException(e);
		}

	}

	private List<String> convertCSVToList(String concatErrorNameList) {
		List<String> list = new ArrayList<String>();
		String[] splitted = StringUtils.split(concatErrorNameList, "~");
		if(splitted != null){
			for(String s : splitted){
				list.add(s);
			}
		}
		return list;
	}

    /**
     * The actual method responsible for sending email.
     *
     * @param request
     * @param response
     * @param form
     * @return
     * @throws ApplicationException
     */
    public ModelAndView sendEmail(
            HttpServletRequest request,
            HttpServletResponse response,
            EmailForm form) throws ApplicationException {
        
        LOGGER.info("Email is being sent...");
        try {
            UserSession userSession = getUserSession(request);
            String from = userSession.getUserId();

            WMSEmail email = new WMSEmail();

            email.setTo(form.getTo());
            email.setCc(form.getCc());
            email.setFrom(from + "@sunlife.com");
            email.setSubject(form.getSubject());
            email.setText(form.getContent());
            email.setDcrId(form.getDcrId());
            email.setUserId(from);

            boolean isSent = emailService.sendEmail(email);

            form.setSent(isSent);
            form.setReloadSP(true);

            LOGGER.info("Email has been sent successfully...");
            return showEmailPage(request, response, form);
        } catch (ServiceException e) {
            // Change according to Healthcheck
            // *Problem was identified as inconsistent logging
            // *Change was added logging with object exception
            LOGGER.error("Error in sending email: ", e);
            throw new ApplicationException("Email sending failed. Please use valid email address.");
        }
    }

	@Override
	protected Role[] allowedRoles() {
		return new Role[] { Role.CASHIER, Role.MANAGER, Role.PPA };
	}

	public void setEmailService(EmailService emailService) {
		this.emailService = emailService;
	}

	protected String getPageTitle() {
		return PAGE_TITLE;
	}

}
