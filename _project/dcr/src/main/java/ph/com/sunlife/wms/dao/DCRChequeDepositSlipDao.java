package ph.com.sunlife.wms.dao;

import java.util.List;

import ph.com.sunlife.wms.dao.domain.DCRChequeDepositSlip;
import ph.com.sunlife.wms.dao.exceptions.WMSDaoException;

/**
 * This is the interface responsible for doing CRUD functionalities for
 * {@link DCRCashDepositSlip}.
 * 
 * <p>
 * Note that this interface does not have implementation for
 * {@link WMSDao#refresh(Object)}.
 * </p>
 * 
 * @author Zainal Limpao
 * @see WMSDao
 */
public interface DCRChequeDepositSlipDao extends WMSDao<DCRChequeDepositSlip> {

	DCRChequeDepositSlip getChequeDepositSlip(DCRChequeDepositSlip example)
			throws WMSDaoException;

	List<DCRChequeDepositSlip> getAllChequeDepositSlipsByDcrId(Long dcrId)
			throws WMSDaoException;

	List<DCRChequeDepositSlip> getAllChequeDepositSlipsByDcrCashierId(
			Long dcrCashierId) throws WMSDaoException;

}
