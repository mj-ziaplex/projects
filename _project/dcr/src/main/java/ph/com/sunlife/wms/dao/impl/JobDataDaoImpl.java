package ph.com.sunlife.wms.dao.impl;

import java.util.List;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import ph.com.sunlife.wms.dao.JobDataDao;
import ph.com.sunlife.wms.dao.domain.JobData;
import ph.com.sunlife.wms.dao.domain.JobDataErrorLog;
import ph.com.sunlife.wms.dao.exceptions.WMSDaoException;

public class JobDataDaoImpl extends SqlMapClientDaoSupport implements
		JobDataDao {

	private static final String NAMESPACE = "JobData";

	@Override
	public Long createErrorLog(JobDataErrorLog jobDataErrorLog)
			throws WMSDaoException {
		try {
			Long id = (Long) getSqlMapClientTemplate().queryForObject(
					NAMESPACE + ".createErrorLog", jobDataErrorLog);
			return id;
		} catch (Exception ex) {
			throw new WMSDaoException(ex);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<JobData> getAllJobData() throws WMSDaoException {
		try {
			List<JobData> list = (List<JobData>) getSqlMapClientTemplate()
					.queryForList(NAMESPACE + ".getAllJobData");
			return list;
		} catch (Exception ex) {
			throw new WMSDaoException(ex);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<JobDataErrorLog> getJobDataErrorLog(Long jobId)
			throws WMSDaoException {
		try {
			List<JobDataErrorLog> list = (List<JobDataErrorLog>) getSqlMapClientTemplate()
					.queryForList(NAMESPACE + ".getJobDataErrorLog", jobId);
			return list;
		} catch (Exception ex) {
			throw new WMSDaoException(ex);
		}
	}

	@Override
	public boolean updateJobData(JobData jobData) throws WMSDaoException {
		try {
			int updatedRows = getSqlMapClientTemplate().update(
					NAMESPACE + ".updateJobData", jobData);
			return updatedRows != 0 ? true : false;
		} catch (Exception ex) {
			throw new WMSDaoException(ex);
		}
	}

}
