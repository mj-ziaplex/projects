package ph.com.sunlife.wms.web.controller;

import java.beans.PropertyEditor;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;

import ph.com.sunlife.wms.dao.domain.CheckType;
import ph.com.sunlife.wms.dao.domain.Currency;
import ph.com.sunlife.wms.security.Role;
import ph.com.sunlife.wms.services.DCRMixedChequePayeeService;
import ph.com.sunlife.wms.services.bo.BankBO;
import ph.com.sunlife.wms.services.bo.CompanyBO;
import ph.com.sunlife.wms.services.bo.CustomerCenterBO;
import ph.com.sunlife.wms.services.bo.MixedChequePayeeBO;
import ph.com.sunlife.wms.services.bo.UserSession;
import ph.com.sunlife.wms.services.exception.ServiceException;
import ph.com.sunlife.wms.web.controller.form.MixedChequePayeeForm;
import ph.com.sunlife.wms.web.exception.ApplicationException;

public class MixedChequePayeeController extends AbstractCashierWorkItemController {
	
	private String displayViewName = "mixedChequePayeeBalancingToolView";
	
	private String pageTitle;	

	private static Logger LOGGER = Logger.getLogger(MixedChequePayeeController.class);
	
	private static final String MIXED_CHEQUE_PAYEE_LIST = "mixedChequePayeeList";
	
	private static final String BANK_KEY = "banks";

	private static final String PAYEE_KEY = "payees";
	
	private static final String CURRENCY_KEY = "currencies";
	
	private static final String CHECK_TYPE_KEY = "checkTypes";
	
	private static final String CUSTOMER_CENTER_KEY = "customerCenter";
	
	private DCRMixedChequePayeeService dcrMixedChequePayeeService;

	public DCRMixedChequePayeeService getDcrMixedChequePayeeService() {
		return dcrMixedChequePayeeService;
	}

	public void setDcrMixedChequePayeeService(
			DCRMixedChequePayeeService dcrMixedChequePayeeService) {
		this.dcrMixedChequePayeeService = dcrMixedChequePayeeService;
	}
	
	public String getDisplayViewName() {
		return displayViewName;
	}

	public void setDisplayViewName(String displayViewName) {
		this.displayViewName = displayViewName;
	}

	@Override
	protected String getPageTitle() {
		return pageTitle;
	}
	
	public void setPageTitle(String pageTitle) {
		this.pageTitle = pageTitle;
	}

    @Override
    protected Role[] allowedRoles() {
        // Change according to Healthcheck
        // Problem was due to changes in AbstractSecuredMultiActionController
        // Change was use parent getter method instead of inherited values
        return super.getALL_ROLES();
    }
	
	public ModelAndView doShowHomePage(HttpServletRequest request, HttpServletResponse response,
			MixedChequePayeeForm form){
		
		ModelAndView modelAndView = new ModelAndView();		
		modelAndView.setViewName(displayViewName);
		UserSession userSession = getUserSession(request);
		
		
		try{
			List<MixedChequePayeeBO> mixedChequePayeeBO = dcrMixedChequePayeeService.getAllMixedChequePayees(form.getDcrCashierId());
			List<BankBO> banks = dcrMixedChequePayeeService.getAllBanks();
			List<CompanyBO> companyBO = new ArrayList<CompanyBO>();			
		//	companyBO.add(CompanyBO.OTHER);
		//	companyBO.add(CompanyBO.SLAMCID);
		//	companyBO.add(CompanyBO.SLAMCIP);
			companyBO.add(CompanyBO.SLFPI);
		//	companyBO.add(CompanyBO.SLGFI);
			companyBO.add(CompanyBO.SLOCPI);
			List<Currency> currencies = new ArrayList<Currency>();
			currencies.add(Currency.PHP);
			currencies.add(Currency.USD);
			List<CheckType> checkTypes = new ArrayList<CheckType>();
			checkTypes.add(CheckType.LOCAL);
			checkTypes.add(CheckType.ON_US);
			checkTypes.add(CheckType.REGIONAL);
			checkTypes.add(CheckType.OT);
			checkTypes.add(CheckType.DOLLARCHECK);
			checkTypes.add(CheckType.USCD_MANILA);
			checkTypes.add(CheckType.USCD_OUTSIDEPH);
			checkTypes.add(CheckType.BANK_OTC);
			modelAndView.addObject(MIXED_CHEQUE_PAYEE_LIST,mixedChequePayeeBO);
			modelAndView.addObject(BANK_KEY,banks);
			modelAndView.addObject(PAYEE_KEY, companyBO);
			modelAndView.addObject(CURRENCY_KEY,currencies);
			modelAndView.addObject(CHECK_TYPE_KEY,checkTypes);
			
			CustomerCenterBO customerCenterBO = dcrMixedChequePayeeService.getDCRCC(userSession.getSiteCode());
			modelAndView.addObject(CUSTOMER_CENTER_KEY,customerCenterBO);
		}catch(ServiceException e){
			LOGGER.error(e);
		}
		
		return modelAndView;
	}
	
	public ModelAndView createNewCheck (HttpServletRequest request, HttpServletResponse response,
			MixedChequePayeeForm form){
		
		ModelAndView modelAndView = null;
		try{
			MixedChequePayeeBO mixedChequePayeeBO = new MixedChequePayeeBO();
			CheckType checkType = CheckType.getById(form.getTypeOfCheckDD());
			CompanyBO companyBO = CompanyBO.getCompany(form.getPayeeDD());
			Currency currency = Currency.getCurrency(form.getCurrencyDD());
			mixedChequePayeeBO.setBank(form.getBankDD());
			mixedChequePayeeBO.setCheckAmount(form.getCheckAmountTB());
			mixedChequePayeeBO.setCheckDate(form.getCheckDate());
			mixedChequePayeeBO.setCheckNumber(form.getCheckNumberTB());
			mixedChequePayeeBO.setCheckType(checkType);
			mixedChequePayeeBO.setCurrency(currency);
			mixedChequePayeeBO.setPayee(companyBO);
			// mixedChequePayeeBO.setSlamci(form.getSlamciTB());
			mixedChequePayeeBO.setSlfpi(form.getSlfpiTB());
			// mixedChequePayeeBO.setSlgfi(form.getSlgfiTB());
			mixedChequePayeeBO.setSlocpi(form.getSlocpiTB());
			// mixedChequePayeeBO.setOthers(form.getOtherTB());
			mixedChequePayeeBO.setDcrCashier(form.getDcrCashierId());
			
			
			mixedChequePayeeBO = dcrMixedChequePayeeService.createNewMixedChequePayee(mixedChequePayeeBO);
			
					
		}catch(ServiceException e){
			LOGGER.error(e);
		}		
		
		modelAndView = this.doShowHomePage(request, response, form);	
		
		return modelAndView;
	
	}
	
	public ModelAndView deleteCheck (HttpServletRequest request, HttpServletResponse response,
			MixedChequePayeeForm form){
		
		ModelAndView modelAndView = null;
		try{
			dcrMixedChequePayeeService.deleteMixedChequePayee(form.getMcpId());
		}catch(ServiceException e){
			LOGGER.error(e);
		}
		
		modelAndView = this.doShowHomePage(request, response, form);	
		
		return modelAndView;
	
	}
	
	public ModelAndView updateCheck (HttpServletRequest request, HttpServletResponse response,
			MixedChequePayeeForm form) throws ApplicationException{
		
		ModelAndView modelAndView = null;
		
		try{
			
			MixedChequePayeeBO mixedChequePayeeBO = new MixedChequePayeeBO();
			CheckType checkType = CheckType.getById(form.getTypeOfCheckDD());
			CompanyBO companyBO = CompanyBO.getCompany(form.getPayeeDD());
			Currency currency = Currency.getCurrency(form.getCurrencyDD());
			mixedChequePayeeBO.setBank(form.getBankDD());
			mixedChequePayeeBO.setCheckAmount(form.getCheckAmountTB());
			mixedChequePayeeBO.setCheckDate(form.getCheckDate());
			mixedChequePayeeBO.setCheckNumber(form.getCheckNumberTB());
			mixedChequePayeeBO.setCheckType(checkType);
			mixedChequePayeeBO.setCurrency(currency);
			mixedChequePayeeBO.setPayee(companyBO);
			mixedChequePayeeBO.setSlamci(form.getSlamciTB());
			mixedChequePayeeBO.setSlfpi(form.getSlfpiTB());
			mixedChequePayeeBO.setSlgfi(form.getSlgfiTB());
			mixedChequePayeeBO.setSlocpi(form.getSlocpiTB());
			mixedChequePayeeBO.setOthers(form.getOtherTB());
			mixedChequePayeeBO.setDcrCashier(form.getDcrCashierId());
			mixedChequePayeeBO.setId(form.getMcpId());
			
			dcrMixedChequePayeeService.updateMixedChequePayee(mixedChequePayeeBO);
		}catch(ServiceException e){
			LOGGER.error(e);
			throw new ApplicationException(e);
		}
		
		
		modelAndView = this.doShowHomePage(request, response, form);	
		
		return modelAndView;
	}

	@Override
	protected void registerAdditionalPropertyEditors(
			ServletRequestDataBinder binder) throws Exception {

		PropertyEditor moneyEditor = createMoneyPropertyEditor();
		binder.registerCustomEditor(Double.class,moneyEditor);
	}
	
	

}
