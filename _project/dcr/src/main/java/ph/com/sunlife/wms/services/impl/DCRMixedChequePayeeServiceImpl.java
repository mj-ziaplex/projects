package ph.com.sunlife.wms.services.impl;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.apache.commons.collections.PredicateUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import ph.com.sunlife.wms.dao.DCRDao;
import ph.com.sunlife.wms.dao.DCRMixedChequePayeeDao;
import ph.com.sunlife.wms.dao.IpacDao;
import ph.com.sunlife.wms.dao.domain.Bank;
import ph.com.sunlife.wms.dao.domain.Company;
import ph.com.sunlife.wms.dao.domain.CustomerCenter;
import ph.com.sunlife.wms.dao.domain.DCRMixedChequePayee;
import ph.com.sunlife.wms.dao.exceptions.WMSDaoException;
import ph.com.sunlife.wms.services.DCRMixedChequePayeeService;
import ph.com.sunlife.wms.services.bo.BankBO;
import ph.com.sunlife.wms.services.bo.CompanyBO;
import ph.com.sunlife.wms.services.bo.CustomerCenterBO;
import ph.com.sunlife.wms.services.bo.MixedChequePayeeBO;
import ph.com.sunlife.wms.services.exception.ServiceException;
import ph.com.sunlife.wms.util.ParameterMap;

/**
 * The implementing class of {@link DCRMixedChequePayeeService}.
 * 
 * @author Zainal Limpao
 * @author Edgardo Cunanan
 * @author Crisseljess Mendoza
 * 
 */
public class DCRMixedChequePayeeServiceImpl implements
		DCRMixedChequePayeeService {

	private static final Logger LOGGER = Logger
			.getLogger(DCRMixedChequePayeeServiceImpl.class);

	private static final String STANDARD_DATE_FORMAT_STR = "ddMMMyyyy";

	private static final DateFormat STANDARD_DATE_FORMAT = new SimpleDateFormat(
			STANDARD_DATE_FORMAT_STR);

	private DCRMixedChequePayeeDao dcrMixedChequePayeeDao;

	private DCRDao dcrDao;

	private IpacDao ipacDao;

	private DCRMixedChequePayee convertToEntity(
			MixedChequePayeeBO businessObject) {
		DCRMixedChequePayee entity = new DCRMixedChequePayee();

		entity.setBank(businessObject.getBank().getId());
		entity.setCheckAmount(businessObject.getCheckAmount());
		entity.setCheckDate(businessObject.getCheckDate());
		entity.setCheckNumber(businessObject.getCheckNumber());
		entity.setCheckTypeId(businessObject.getCheckType().getId());
		entity.setCurrency(businessObject.getCurrency());
		entity.setDcrCashier(businessObject.getDcrCashier().getId());
		entity.setOthers(businessObject.getOthers());
		entity.setPayeeId(businessObject.getPayee().getId());
		entity.setSlamci(businessObject.getSlamci());
		entity.setSlgfi(businessObject.getSlgfi());
		entity.setSlocpi(businessObject.getSlocpi());
		entity.setSlfpi(businessObject.getSlfpi());
		entity.setId(businessObject.getId());

		return entity;

	}

	private MixedChequePayeeBO convertToBusinessObject(
			DCRMixedChequePayee entity) {
		MixedChequePayeeBO businessObject = new MixedChequePayeeBO();

		businessObject.setBank(entity.getBank().getId());
		businessObject.setCheckAmount(entity.getCheckAmount());
		businessObject.setCheckDate(entity.getCheckDate());
		businessObject.setCheckNumber(entity.getCheckNumber());
		businessObject.setCheckType(entity.getCheckType());
		businessObject.setCurrency(entity.getCurrency());
		businessObject.setDcrCashier(entity.getDcrCashier().getId());
		businessObject.setOthers(entity.getOthers());
		businessObject
				.setPayee(CompanyBO.getCompany(entity.getPayee().getId()));
		businessObject.setSlamci(entity.getSlamci());
		businessObject.setSlgfi(entity.getSlgfi());
		businessObject.setSlocpi(entity.getSlocpi());
		businessObject.setSlfpi(entity.getSlfpi());
		businessObject.setId(entity.getId());
		businessObject.setCheckDateStr(StringUtils
				.upperCase(STANDARD_DATE_FORMAT.format(entity.getCheckDate())));

		return businessObject;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ph.com.sunlife.wms.services.DCRMixedChequePayeeService#
	 * createNewMixedChequePayee
	 * (ph.com.sunlife.wms.services.bo.MixedChequePayeeBO)
	 */
	@Override
	public MixedChequePayeeBO createNewMixedChequePayee(
			MixedChequePayeeBO businessObject) throws ServiceException {

		DCRMixedChequePayee entity = convertToEntity(businessObject);
		try {
			entity = dcrMixedChequePayeeDao.save(entity);

			Long id = entity.getId();
			if (id != null) {
				businessObject.setId(id);
			}

		} catch (WMSDaoException e) {
			LOGGER.error(e);
			throw new ServiceException(e);
		}

		return businessObject;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ph.com.sunlife.wms.services.DCRMixedChequePayeeService#deleteMixedChequePayee
	 * (java.lang.Long)
	 */
	@Override
	public boolean deleteMixedChequePayee(Long mixedChequePayeeId)
			throws ServiceException {
		boolean isDeleteSuccessful = false;
		try {
			isDeleteSuccessful = dcrMixedChequePayeeDao
					.deleteById(mixedChequePayeeId);
		} catch (WMSDaoException e) {
			LOGGER.error(e);
			throw new ServiceException(e);
		}

		return isDeleteSuccessful;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ph.com.sunlife.wms.services.DCRMixedChequePayeeService#updateMixedChequePayee
	 * (ph.com.sunlife.wms.services.bo.MixedChequePayeeBO)
	 */
	@Override
	public boolean updateMixedChequePayee(MixedChequePayeeBO businessObject)
			throws ServiceException {

		boolean isUpdatedSuccessfully = false;
		try {
			DCRMixedChequePayee entity = convertToEntity(businessObject);
			isUpdatedSuccessfully = dcrMixedChequePayeeDao
					.updateMixedChequePayee(entity);
		} catch (WMSDaoException e) {
			LOGGER.error(e);
			throw new ServiceException(e);
		}
		return isUpdatedSuccessfully;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ph.com.sunlife.wms.services.DCRMixedChequePayeeService#
	 * getAllMixedChequePayees(java.lang.Long)
	 */
	@Override
	public List<MixedChequePayeeBO> getAllMixedChequePayees(Long dcrCashierId)
			throws ServiceException {

		List<MixedChequePayeeBO> businessObjects = null;
		try {
			List<DCRMixedChequePayee> list = dcrMixedChequePayeeDao
					.getAllByDcrCashier(dcrCashierId);

			if (CollectionUtils.isNotEmpty(list)) {
				businessObjects = new ArrayList<MixedChequePayeeBO>();

				for (DCRMixedChequePayee entity : list) {
					MixedChequePayeeBO businessObject = convertToBusinessObject(entity);
					businessObjects.add(businessObject);
				}

			}
		} catch (WMSDaoException e) {
			LOGGER.error(e);
			throw new ServiceException(e);
		}

		return businessObjects;
	}

    /*
     * (non-Javadoc)
     * 
     * @see ph.com.sunlife.wms.services.DCRMixedChequePayeeService#getAllBanks()
     */
    @Override
    public List<BankBO> getAllBanks() throws ServiceException {
        LOGGER.info("Getting all drawee banks from IPAC DB...");

        List<BankBO> resultList = new ArrayList<BankBO>();
        try {
            List<Bank> list = ipacDao.getAllDraweeBanks();
            if (CollectionUtils.isNotEmpty(list)) {
                for (Bank entity : list) {
                    BankBO businessObject = new BankBO();
                    convertBankToBusinessObject(businessObject, entity);
                    resultList.add(businessObject);
                }
            }
            // Change according to Healthcheck
            // *Problem was identified as inconsistent logging
            // *Change was moved into if-else condition and moved out in catch block
            else {
                list = dcrMixedChequePayeeDao.getAllBanks();
                if (CollectionUtils.isNotEmpty(list)) {
                    for (Bank entity : list) {
                        BankBO businessObject = new BankBO();
                        convertBankToBusinessObject(businessObject, entity);
                        resultList.add(businessObject);
                    }
                }
            }
        } catch (WMSDaoException e) {
            // Change according to Healthcheck
            // *Problem was identified as inconsistent logging
            // *Change was remove setting of value of variable and exception in logging
            LOGGER.debug("IPAC_WMS_DRAWEE is not yet available in this DB", e);
        }

        if (LOGGER.isDebugEnabled() && CollectionUtils.isNotEmpty(resultList)) {
            LOGGER.debug("Found " + resultList.size() + " banks");
        }
        return resultList;
    }

	private BankBO convertBankToBusinessObject(BankBO bankBO, Bank bank) {
		// TODO: why not use BeanUtils?
		bankBO.setId(bank.getId());
		bankBO.setName(bank.getName());

		return bankBO;
	}

	public void setDcrMixedChequePayeeDao(
			DCRMixedChequePayeeDao dcrMixedChequePayeeDao) {
		this.dcrMixedChequePayeeDao = dcrMixedChequePayeeDao;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ph.com.sunlife.wms.services.DCRMixedChequePayeeService#getDCRCC(java.
	 * lang.String)
	 */
	@Override
	public CustomerCenterBO getDCRCC(String ccId) throws ServiceException {

		try {
			List<String> ccs = new ArrayList<String>();
			ccs.add(ccId);
			List<CustomerCenter> cc = dcrDao.getCustomerCenters(ccs);

			if (CollectionUtils.isNotEmpty(cc)) {
				CustomerCenter customerCenter = cc.get(0);

				CustomerCenterBO customerCenterBO = new CustomerCenterBO();
				customerCenterBO.setCcId(customerCenter.getCcId());
				customerCenterBO.setCcCode(customerCenter.getCcCode());
				customerCenterBO.setCcName(customerCenter.getCcName());

				return customerCenterBO;
			}
		} catch (WMSDaoException e) {
			LOGGER.error(e);
			throw new ServiceException(e);
		}

		return null;
	}

	public void setDcrDao(DCRDao dcrDao) {
		this.dcrDao = dcrDao;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ph.com.sunlife.wms.services.DCRMixedChequePayeeService#
	 * getMixedChequePayeeBOsForPrinting(java.lang.Long,
	 * ph.com.sunlife.wms.util.ParameterMap)
	 */
	@SuppressWarnings("unchecked")
	public List<MixedChequePayeeBO> getMixedChequePayeeBOsForPrinting(
			Long dcrCashierId, final ParameterMap form) throws ServiceException {

		List<MixedChequePayeeBO> mixedChequePayeeBOs = getAllMixedChequePayees(dcrCashierId);

		List<Predicate> predicates = new ArrayList<Predicate>();
		Predicate predicateCurrencyCheckType = new Predicate() {

			@Override
			public boolean evaluate(Object object) {
				return ((MixedChequePayeeBO) object).getCheckType().equals(
						form.get("checkType"))
						&& ((MixedChequePayeeBO) object).getCurrency().getId() == form
								.get("currencyId");

			}
		};
		Predicate predicateCompany = new Predicate() {

			@Override
			public boolean evaluate(Object object) {
				boolean include = false;
				if (Company.SLAMCID.getId() == form.get("companyId")
						|| Company.SLAMCIP.getId() == form.get("companyId")) {
					include = ((MixedChequePayeeBO) object).getSlamci() > 0;
				} else if (Company.SLFPI.getId() == form.get("companyId")) {
					include = ((MixedChequePayeeBO) object).getSlfpi() > 0;
				} else if (Company.SLGFI.getId() == form.get("companyId")) {
					include = ((MixedChequePayeeBO) object).getSlgfi() > 0;
				} else if (Company.SLOCPI.getId() == form.get("companyId")) {
					include = ((MixedChequePayeeBO) object).getSlocpi() > 0;
				}

				return include;
			}

		};
		predicates.add(predicateCurrencyCheckType);
		predicates.add(predicateCompany);
		Predicate filter = PredicateUtils.allPredicate(predicates);
		List<MixedChequePayeeBO> filtered = null;
		if (CollectionUtils.isNotEmpty(mixedChequePayeeBOs)) {
			filtered = (List<MixedChequePayeeBO>) CollectionUtils.select(
					mixedChequePayeeBOs, filter);
		}
		return filtered;

	}

	public void setIpacDao(IpacDao ipacDao) {
		this.ipacDao = ipacDao;
	}

}
