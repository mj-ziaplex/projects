package ph.com.sunlife.wms.web.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.web.servlet.ModelAndView;

import ph.com.sunlife.wms.security.Role;
import ph.com.sunlife.wms.services.ConsolidatedDataService;
import ph.com.sunlife.wms.services.DCRCommentService;
import ph.com.sunlife.wms.services.DCRCreationService;
import ph.com.sunlife.wms.services.DCRSearchService;
import ph.com.sunlife.wms.services.bo.ConsolidatedData;
import ph.com.sunlife.wms.services.bo.DCRCommentBO;
import ph.com.sunlife.wms.services.bo.DCRStepProcessorNoteBO;
import ph.com.sunlife.wms.services.bo.UserSession;
import ph.com.sunlife.wms.services.exception.ServiceException;
import ph.com.sunlife.wms.web.controller.form.AddCommentForm;
import ph.com.sunlife.wms.web.controller.form.CashierSPForm;
import ph.com.sunlife.wms.web.controller.form.CashierSPSaveForm;
import ph.com.sunlife.wms.web.controller.form.NoteForm;
import ph.com.sunlife.wms.web.exception.ApplicationException;

public class CashierStepProcessorController extends
		AbstractSecuredMultiActionController {
	
	private static final String CASHIER_SP_KEY = "cashierSPPage";
	
	private static final String COMMENTS_SP_KEY = "spCommentPage";
	
	private static final String CONSOLIDATED_DATA_KEY = "consolidatedData";
	
	private static final String ATTACHMENT_KEY = "attachments";
	
	private static final String FORMATTED_CONSOLIDATED_DATA_KEY = "formattedConsolidatedData";
	
	private static final String COMMENTS_LIST_KEY = "commentList";
	
	private static final String USER_ROLE_KEY = "userRoles";
	
	private static final String FROM_SEARCH_KEY = "fromSearch";
	
	private static final String CLOSE_KEY = "closeWindow";
	
	private DCRCreationService dcrCreationService;
	
	private DCRSearchService dcrSearchService;
	
	private ConsolidatedDataService consolidatedDataService;
	
	private DCRCommentService dcrCommentService;
	
	private static final Logger LOGGER = Logger.getLogger(CashierStepProcessorController.class);
	
	public ModelAndView doShowHomePage(
			final HttpServletRequest request,
			final HttpServletResponse response,
			final CashierSPForm form) throws ApplicationException {
		
		LOGGER.debug("Initializing home page...");

		ModelAndView modelAndView = new ModelAndView();

		try{

			UserSession userSession = this.getUserSession(request);
			String userId = userSession.getUserId();
			List<Role> roles = userSession.getRoles();
			List<String> formattedConsolidatedData = new ArrayList<String>();
			
			boolean isMgr = roles.contains(Role.MANAGER);
			Long dcrId = form.getDcrId();
			
			ConsolidatedData consolidatedData = consolidatedDataService.getConsolidatedData(dcrId, userId, false, isMgr);		
			
			modelAndView.addObject(CONSOLIDATED_DATA_KEY, consolidatedData);
			
			formattedConsolidatedData = consolidatedDataService.formatConsolidatedData(consolidatedData);
			
			modelAndView.addObject(FORMATTED_CONSOLIDATED_DATA_KEY, formattedConsolidatedData);
			
			modelAndView.addObject(USER_ROLE_KEY, roles);
			
			modelAndView.addObject("saveInlineNotesMapping", "/cashierSP/save.html");
	
			modelAndView.addObject(ATTACHMENT_KEY, consolidatedData.getUserToAttachmentsMap());
			
			modelAndView.addObject(FROM_SEARCH_KEY, form.isFromSearch());
			
			modelAndView.addObject("title", "DCR Step Processor");
		
		} catch(ServiceException e){
			LOGGER.error("Error encountered in showing home page - doShowHomePage: ", e);
			throw new ApplicationException(e);
		}
		
		modelAndView.setViewName(CASHIER_SP_KEY);
		modelAndView.addObject(CLOSE_KEY, false);

		return modelAndView;
	}

	public DCRCreationService getDcrCreationService() {
		return dcrCreationService;
	}

	public void setDcrCreationService(DCRCreationService dcrCreationService) {
		this.dcrCreationService = dcrCreationService;
	}

	public DCRSearchService getDcrSearchService() {
		return dcrSearchService;
	}

	public void setDcrSearchService(DCRSearchService dcrSearchService) {
		this.dcrSearchService = dcrSearchService;
	}	

	public void setDcrCommentService(DCRCommentService dcrCommentService) {
		this.dcrCommentService = dcrCommentService;
	}

    @Override
    protected Role[] allowedRoles() {
        // Change according to Healthcheck
        // Problem was due to changes in AbstractSecuredMultiActionController
        // Change was use parent getter method instead of inherited values
        return super.getALL_ROLES();
    }

	public ConsolidatedDataService getConsolidatedDataService() {
		return consolidatedDataService;
	}

	public void setConsolidatedDataService(
			ConsolidatedDataService consolidatedDataService) {
		this.consolidatedDataService = consolidatedDataService;
	}
	
	public ModelAndView doShowComments(HttpServletRequest request,
			HttpServletResponse response, CashierSPForm form) throws ApplicationException {
		
		ModelAndView modelAndView = new ModelAndView();
		
		try{
			List<DCRCommentBO> dcrCommentBO = dcrCommentService.getAllCommentsDcrId(form.getDcrId());
			
			modelAndView.addObject(COMMENTS_LIST_KEY, dcrCommentBO);
			
		}catch(ServiceException e){
			LOGGER.error(e);
			throw new ApplicationException(e);
		}
		
		modelAndView.setViewName(COMMENTS_SP_KEY);
			
		return modelAndView;
		
	}
	
	public ModelAndView doCompleteWork(
			final HttpServletRequest request,
			final HttpServletResponse response,
			final CashierSPSaveForm form) throws ApplicationException{
		
		LOGGER.info("Start complete of WI - doCompleteWork ...");
		
		UserSession userSession = getUserSession(request);
		String userId = userSession.getUserId();
		
		try{
			List<DCRStepProcessorNoteBO> dcrNoteList = new ArrayList<DCRStepProcessorNoteBO>();
			for(NoteForm nl : form.getNoteList()){
				DCRStepProcessorNoteBO tempNoteBO = new DCRStepProcessorNoteBO();
				tempNoteBO.setNoteContent(nl.getNoteContent());
				tempNoteBO.setNoteType(nl.getNoteType());
				tempNoteBO.setRowNum(nl.getRowNum());
				tempNoteBO.setDcrId(form.getDcrId());
				dcrNoteList.add(tempNoteBO);
			}
			
			if("For Review and Approval".equals(form.getStepProcResponse().trim())){
				consolidatedDataService.forReviewAndApproval(userId, form.getDcrId(), dcrNoteList);
			} else if("Approve For Reconciliation".equals(form.getStepProcResponse())){
				consolidatedDataService.approveForReconcilation(userId, form.getDcrId(), dcrNoteList);
			}  else if("Awaiting Feedback".equals(form.getStepProcResponse())){
				consolidatedDataService.awaitingFeedback(userId, form.getDcrId(), dcrNoteList);
			}
		} catch(Exception e){
			LOGGER.error("Error encountered in completing WI - doCompleteWork: ", e);
			throw new ApplicationException(e);
		}
		
		LOGGER.info("End complete of WI - doCompleteWork ...");
		
		ModelAndView modelAndView = this.doShowHomePage(request, response, form);
		modelAndView.addObject(CLOSE_KEY, true);
		
		return modelAndView;
	}
	
	public ModelAndView doSaveComment(HttpServletRequest request,
			HttpServletResponse response, AddCommentForm form) throws Exception {

		Long dcrCashierId = form.getDcrCashierId();
		Long dcrId = form.getDcrId();
		UserSession userSession = getUserSession(request);
		String acf2id = userSession.getUserId();

		DCRCommentBO dcrComment = new DCRCommentBO();
		dcrComment.setAcf2id(acf2id);
		dcrComment.setDatePosted(new Date());
		dcrComment.setDcrCashier(dcrCashierId);
		dcrComment.setRemarks(form.getRemarks());
		dcrComment.setDcrId(dcrId);
		
		ModelAndView modelAndView = new ModelAndView();
		List<DCRCommentBO> dcrComments = null;

		try {
			dcrCommentService.save(dcrComment);
			dcrComments = dcrCommentService.getAllCommentsDcrId(form.getDcrId());
		} catch (ServiceException e) {
			throw new ApplicationException(
					"Something went wrong after trying to add new comment", e);
		}
		
		modelAndView.addObject(COMMENTS_LIST_KEY, dcrComments);
		modelAndView.setViewName(COMMENTS_SP_KEY);

		return modelAndView;
	}
	
	public ModelAndView saveInLineNotes(HttpServletRequest request,
			HttpServletResponse response, CashierSPSaveForm form)
			throws ApplicationException {

		List<NoteForm> noteForms = form.getNoteList();

		Long dcrId = form.getDcrId();

		String userId = getUserSession(request).getUserId();

		if (CollectionUtils.isNotEmpty(noteForms)) {
			List<DCRStepProcessorNoteBO> notes = new ArrayList<DCRStepProcessorNoteBO>();
			for (NoteForm noteForm : noteForms) {
				if (noteForm != null) {
					DCRStepProcessorNoteBO note = new DCRStepProcessorNoteBO();
					note.setAcf2id(userId);
					note.setDcrId(dcrId);
					note.setNoteContent(noteForm.getNoteContent());
					note.setNoteType(noteForm.getNoteType());
					note.setRowNum(noteForm.getRowNum());
					notes.add(note);
				}
			}

			try {
				consolidatedDataService.saveInlineNotes(userId, dcrId, notes);
			} catch (ServiceException e) {
				throw new ApplicationException(e);
			}
		}
		return this.doShowHomePage(request, response, form);
	}
	
	public ModelAndView doProcessConsolidatedData(HttpServletRequest request,
			HttpServletResponse response, CashierSPForm form) throws ApplicationException {
		LOGGER.info("start doProcessConsolidatedData..");
		ModelAndView modelAndView = new ModelAndView();
		
		try{
			UserSession userSession = this.getUserSession(request);
			List<Role> roles = userSession.getRoles();
			boolean isMgr = roles.contains(Role.MANAGER);

			consolidatedDataService.processIPACAndIngeniumData(form.getDcrId(), isMgr);
			
		}catch(ServiceException e){
			LOGGER.error(e);
			throw new ApplicationException(e);
		}
		
		modelAndView.setViewName(CASHIER_SP_KEY);
		modelAndView.addObject(CLOSE_KEY, false);

		return modelAndView;
		
	}
}
