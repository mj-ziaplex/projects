package ph.com.sunlife.wms.util;

import java.text.DecimalFormat;

/**
 * Custom Math Util Class.
 * 
 * @author Zainal Limpao
 * 
 */
public class MathUtil {

	/**
	 * Rounds the given value to the nearest monetary value.
	 * 
	 * @param value
	 * @return
	 */
	public static double round(double value) {
		DecimalFormat dtime = new DecimalFormat("#.##");
		return Double.valueOf(dtime.format(value));
	}
}
