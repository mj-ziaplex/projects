package ph.com.sunlife.wms.util;

/**
 * Multipurpose Utility Class for Parameter Masking.
 * 
 * @author Cshellz Sayan
 * 
 */
public class ParameterMaskUtil {

	/**
	 * To byte array.
	 * 
	 * @param str parameter to be masked
	 * @return String masked number
	 */
	public static String toMaskedString(final String str) {

		String toRet = "";
		
		if(str == null) {
			return toRet;
		} else {
			
			// Remove spaces
			toRet = str.trim();
			toRet = toRet.replaceAll(" ", "");
			
			// If valid length(16) then masked
			// else return trimmed value
			if(toRet.length() <= 12) {
				toRet = "XXXX-XXXX-XXXX";
			} else {
				toRet = "XXXX-XXXX-XXXX-" + toRet.substring(12, toRet.length());
			}
		}
		
		return toRet;
	}
}
