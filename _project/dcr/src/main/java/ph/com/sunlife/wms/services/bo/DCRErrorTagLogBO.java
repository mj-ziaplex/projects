package ph.com.sunlife.wms.services.bo;

import java.util.Date;


public class DCRErrorTagLogBO {
	
	private Long id;
	
	private Long dcrId;
	
	private Date datePosted;
	
	private Date dateUpdated;
	
	private String reason;
	
	private String postedUpdatedById;
	
	private String reasonDeleted;
	
	private DCRCompletedStepBO dcrCompletedStep;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getDcrId() {
		return dcrId;
	}

	public void setDcrId(Long dcrId) {
		this.dcrId = dcrId;
	}

	public Date getDatePosted() {
		return datePosted;
	}

	public void setDatePosted(Date datePosted) {
		this.datePosted = datePosted;
	}

	public Date getDateUpdated() {
		return dateUpdated;
	}

	public void setDateUpdated(Date dateUpdated) {
		this.dateUpdated = dateUpdated;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getPostedUpdatedById() {
		return postedUpdatedById;
	}

	public void setPostedUpdatedById(String postedUpdatedById) {
		this.postedUpdatedById = postedUpdatedById;
	}

	public String getReasonDeleted() {
		return reasonDeleted;
	}

	public void setReasonDeleted(String reasonDeleted) {
		this.reasonDeleted = reasonDeleted;
	}

	public DCRCompletedStepBO getDcrCompletedStep() {
		return dcrCompletedStep;
	}

	public void setDcrCompletedStep(DCRCompletedStepBO dcrCompletedStep) {
		this.dcrCompletedStep = dcrCompletedStep;
	}

	
}
