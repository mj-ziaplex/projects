package ph.com.sunlife.wms.services.bo;

import java.util.List;

public class DCRErrorTagLookupBO {
	private Long dcrId;
	private List<DCRCompletedStepBO> dcrCompletedSteps;

	public Long getDcrId() {
		return dcrId;
	}

	public void setDcrId(Long dcrId) {
		this.dcrId = dcrId;
	}

	public List<DCRCompletedStepBO> getDcrCompletedSteps() {
		return dcrCompletedSteps;
	}

	public void setDcrCompletedSteps(List<DCRCompletedStepBO> dcrCompletedSteps) {
		this.dcrCompletedSteps = dcrCompletedSteps;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((dcrId == null) ? 0 : dcrId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DCRErrorTagLookupBO other = (DCRErrorTagLookupBO) obj;
		if (dcrId == null) {
			if (other.dcrId != null)
				return false;
		} else if (!dcrId.equals(other.dcrId))
			return false;
		return true;
	}
	
}
