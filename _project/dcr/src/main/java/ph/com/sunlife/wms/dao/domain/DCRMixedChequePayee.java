package ph.com.sunlife.wms.dao.domain;

import java.util.Date;

public class DCRMixedChequePayee extends Entity {

	private DCRCashier dcrCashier = new DCRCashier();

	private Bank bank = new Bank();

	private String checkNumber;

	private Date checkDate;

	private double checkAmount;

	private CheckType checkType;
	
	private Long checkTypeId;

	private Company payee;
	
	private Long payeeId;

	private double slamci;

	private double slocpi;

	private double slgfi;

	private double slfpi;

	private double others;
	
	private Currency currency;
	
	private Long currencyId;

	public Long getCurrencyId() {
		return currencyId;
	}

	public void setCurrencyId(Long currencyId) {
		this.currencyId = currencyId;
		this.currency = Currency.getCurrency(currencyId);
	}

	public DCRCashier getDcrCashier() {
		return dcrCashier;
	}

	public void setDcrCashier(DCRCashier dcrCashier) {
		this.dcrCashier = dcrCashier;
	}
	
	public void setDcrCashier(Long dcrCashierId) {
		DCRCashier dcrCashier = new DCRCashier();
		dcrCashier.setId(dcrCashierId);
		this.setDcrCashier(dcrCashier);
	}	

	public Bank getBank() {
		return bank;
	}

	public void setBank(Bank bank) {
		this.bank = bank;
	}
	
	public void setBank(String id) {
		Bank bank = new Bank();
		bank.setId(id);
		this.setBank(bank);
	}

	public String getCheckNumber() {
		return checkNumber;
	}

	public void setCheckNumber(String checkNumber) {
		this.checkNumber = checkNumber;
	}

	public Date getCheckDate() {
		return checkDate;
	}

	public void setCheckDate(Date checkDate) {
		this.checkDate = checkDate;
	}

	public double getCheckAmount() {
		return checkAmount;
	}

	public void setCheckAmount(double checkAmount) {
		this.checkAmount = checkAmount;
	}

	public CheckType getCheckType() {
		return checkType;
	}

	public void setCheckType(CheckType checkType) {
		this.checkType = checkType;
		this.checkTypeId = checkType.getId();
	}
	
	public Long getCheckTypeId() {
		return checkTypeId;
	}

	public void setCheckTypeId(Long checkTypeId) {
		this.checkTypeId = checkTypeId;
		this.checkType = CheckType.getById(checkTypeId);
	}

	public Company getPayee() {
		return payee;
	}

	public void setPayee(Company payee) {
		this.payee = payee;
		this.payeeId = payee.getId();
	}
	
	public Long getPayeeId() {
		return payeeId;
	}

	public void setPayeeId(Long payeeId) {
		this.payeeId = payeeId;
		this.payee = Company.getCompany(payeeId);
	}

	public double getSlamci() {
		return slamci;
	}

	public void setSlamci(double slamci) {
		this.slamci = slamci;
	}

	public double getSlocpi() {
		return slocpi;
	}

	public void setSlocpi(double slocpi) {
		this.slocpi = slocpi;
	}

	public double getSlgfi() {
		return slgfi;
	}

	public void setSlgfi(double slgfi) {
		this.slgfi = slgfi;
	}

	public double getSlfpi() {
		return slfpi;
	}

	public void setSlfpi(double slfpi) {
		this.slfpi = slfpi;
	}

	public double getOthers() {
		return others;
	}

	public void setOthers(double others) {
		this.others = others;
	}

	public Currency getCurrency() {
		return currency;
	}

	public void setCurrency(Currency currency) {
		this.currency = currency;
	}

}
