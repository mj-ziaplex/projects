package ph.com.sunlife.wms.dao.domain;

import java.util.Date;

public class DCRReversal extends Entity {

	private Long dcrBalancingToolProductId;

	private double amount;

	private String remarks;

	private Currency currency;

	private Long currencyId;

	private Date dateTime;

	public Long getDcrBalancingToolProductId() {
		return dcrBalancingToolProductId;
	}

	public void setDcrBalancingToolProductId(Long dcrBalancingToolProductId) {
		this.dcrBalancingToolProductId = dcrBalancingToolProductId;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public Currency getCurrency() {
		return currency;
	}

	public void setCurrency(Currency currency) {
		this.currency = currency;
		this.currencyId = currency.getId();
	}

	public Long getCurrencyId() {
		return currencyId;
	}

	public void setCurrencyId(Long currencyId) {
		this.currencyId = currencyId;
		this.currency = Currency.getCurrency(currencyId);
	}

	public Date getDateTime() {
		return dateTime;
	}

	public void setDateTime(Date dateTime) {
		this.dateTime = dateTime;
	}

}
