/**
 * @author i176
 * ****************************************************************************
 * @author: ia23 - sasay
 * @date: Mar2016
 * @desc: Removed methods that will not be used due to MR-WF-15-00089 - DCR
 * Redesign
 */
package ph.com.sunlife.wms.dao;

import java.util.Date;
import java.util.List;

import ph.com.sunlife.wms.dao.domain.DCR;
import ph.com.sunlife.wms.dao.domain.DCRIpacValue;
import ph.com.sunlife.wms.dao.domain.GAFValue;
import ph.com.sunlife.wms.dao.domain.NonPostedTransaction;
import ph.com.sunlife.wms.dao.domain.PPAMDS;
import ph.com.sunlife.wms.dao.exceptions.WMSDaoException;

/**
 * Interface for DCRIPACDaoImpl
 */
public interface DCRIPACDao {

    int updateDCRIPACDownloadFlag(String flag, Date date, Long dcrId) throws WMSDaoException;

    List<DCRIpacValue> getDCRIPACDailyCollectionSummary(String center_code, Date process_date) throws WMSDaoException;

    List<NonPostedTransaction> getDCRIPACDTR(String center_code, Date process_date) throws WMSDaoException;

    List<GAFValue> getDCRIPACGAF(String center_code, Date process_date) throws WMSDaoException;

    List<PPAMDS> getDCRIPACMDSTransaction(String company, String center_code, Date from_date, Date to_date) throws WMSDaoException;

    List<PPAMDS> getDCRIPACMDSTransaction(String center_code, Date process_date) throws WMSDaoException;

    List<DCRIpacValue> getDCRIPACDailyCollectionSummary(String center_code, Date process_date, DCRIpacValue value) throws WMSDaoException;

    List<NonPostedTransaction> getDCRIPACDTR(String center_code, Date process_date, NonPostedTransaction value) throws WMSDaoException;

    List<GAFValue> getDCRIPACGAF(String center_code, Date process_date, GAFValue value) throws WMSDaoException;

    List<PPAMDS> getDCRIPACMDSTransaction(String company, String center_code, Date from_date, Date to_date, PPAMDS value) throws WMSDaoException;

    DCR getDCRDownloadData(Long dcrId) throws WMSDaoException;

    List<DCRIpacValue> getDCRIPACDailyCollectionSummary(String centerCode, Date processDate, String acf2id) throws WMSDaoException;

    List<NonPostedTransaction> getDCRIPACDTR(String centerCode, Date processDate, String acf2id) throws WMSDaoException;

    List<GAFValue> getDCRIPACGAF(String centerCode, Date processDate, String acf2id) throws WMSDaoException;

    List<PPAMDS> getDCRIPACMDSTransaction(String company, String centerCode, Date fromDate, Date toDate, String currency) throws WMSDaoException;
}
