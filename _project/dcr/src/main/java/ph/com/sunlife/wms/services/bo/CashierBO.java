package ph.com.sunlife.wms.services.bo;

public class CashierBO {

	private String acf2id;

	private String firstname;

	private String middlename;

	private String lastname;

	private String emailaddress;
	
	public CashierBO(String acf2id) {
		this.acf2id = acf2id;
	}

	public String getAcf2id() {
		return acf2id;
	}

	public void setAcf2id(String acf2id) {
		this.acf2id = acf2id;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getMiddlename() {
		return middlename;
	}

	public void setMiddlename(String middlename) {
		this.middlename = middlename;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getEmailaddress() {
		return emailaddress;
	}

	public void setEmailaddress(String emailaddress) {
		this.emailaddress = emailaddress;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((acf2id == null) ? 0 : acf2id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CashierBO other = (CashierBO) obj;
		if (acf2id == null) {
			if (other.acf2id != null)
				return false;
		} else if (!acf2id.equals(other.acf2id))
			return false;
		return true;
	}

}
