package ph.com.sunlife.wms.dao.impl;

import java.sql.SQLException;

import org.springframework.orm.ibatis.SqlMapClientCallback;

import com.ibatis.sqlmap.client.SqlMapExecutor;

import ph.com.sunlife.wms.dao.AbstractWMSSqlMapClientDaoSupport;
import ph.com.sunlife.wms.dao.DCRCashDepositSlipDao;
import ph.com.sunlife.wms.dao.DCROtherDao;
import ph.com.sunlife.wms.dao.domain.DCRCashDepositSlip;
import ph.com.sunlife.wms.dao.domain.DCRComment;
import ph.com.sunlife.wms.dao.domain.DCROther;
import ph.com.sunlife.wms.dao.exceptions.WMSDaoException;

public class DCROtherDaoImpl extends AbstractWMSSqlMapClientDaoSupport<DCROther> 
										implements DCROtherDao{
	
	private static final String NAMESPACE = "DCROther";

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ph.com.sunlife.wms.dao.AbstractWMSSqlMapClientDaoSupport#getNamespace()
	 */
	@Override
	protected String getNamespace() {
		return NAMESPACE;
	}

	@Override
	public void updateOther(final DCROther dcrOther) throws WMSDaoException {
		try {
			getSqlMapClientTemplate().execute(
					new SqlMapClientCallback() {

						@Override
						public Object doInSqlMapClient(SqlMapExecutor executor)
								throws SQLException {
							executor.queryForObject(
									getNamespace() + ".update", dcrOther);
							return true;
						}
					});
		} catch (Exception e) {
			throw new WMSDaoException(e);
		}
	}

}
