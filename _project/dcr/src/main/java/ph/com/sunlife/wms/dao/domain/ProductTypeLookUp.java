/*
 * ** Date: May 2017
 * ** Dev: Cshells Sayan
 * ** Desc: Modified methods for a more dynamic approach; SR-WF-16-00016 - SLAMC - New USD Fund (WMS-DCR)
 */
package ph.com.sunlife.wms.dao.domain;

/**
 * Added for SR-WF-16-00016 - SLAMC - New USD Fund (WMS-DCR)
 * @author ia23
 */
public class ProductTypeLookUp {
    private Long id;
    private Long product_id;
    private Long company;
    private Long currency;
    private String productName;
    private String productCode;
    private String fltProductCode;
    private String productPaySubCurr;

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the product_id
     */
    public Long getProduct_id() {
        return product_id;
    }

    /**
     * @param product_id the product_id to set
     */
    public void setProduct_id(Long product_id) {
        this.product_id = product_id;
    }

    /**
     * @return the company
     */
    public Long getCompany() {
        return company;
    }

    /**
     * @param company the company to set
     */
    public void setCompany(Long company) {
        this.company = company;
    }

    /**
     * @return the currency
     */
    public Long getCurrency() {
        return currency;
    }

    /**
     * @param currency the currency to set
     */
    public void setCurrency(Long currency) {
        this.currency = currency;
    }

    /**
     * @return the productName
     */
    public String getProductName() {
        return productName;
    }

    /**
     * @param productName the productName to set
     */
    public void setProductName(String productName) {
        this.productName = productName;
    }

    /**
     * @return the productCode
     */
    public String getProductCode() {
        return productCode;
    }

    /**
     * @param productCode the productCode to set
     */
    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    /**
     * @return the fltProductCode
     */
    public String getFltProductCode() {
        return fltProductCode;
    }

    /**
     * @param fltProductCode the fltProductCode to set
     */
    public void setFltProductCode(String fltProductCode) {
        this.fltProductCode = fltProductCode;
    }

    /**
     * @return the productPaySubCurr
     */
    public String getProductPaySubCurr() {
        return productPaySubCurr;
    }

    /**
     * @param productPaySubCurr the productPaySubCurr to set
     */
    public void setProductPaySubCurr(String productPaySubCurr) {
        this.productPaySubCurr = productPaySubCurr;
    }

}
