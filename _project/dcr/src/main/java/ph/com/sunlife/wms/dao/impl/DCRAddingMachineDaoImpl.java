package ph.com.sunlife.wms.dao.impl;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;

import ph.com.sunlife.wms.dao.AbstractWMSSqlMapClientDaoSupport;
import ph.com.sunlife.wms.dao.DCRAddingMachineDao;
import ph.com.sunlife.wms.dao.domain.DCRAddingMachine;
import ph.com.sunlife.wms.dao.exceptions.WMSDaoException;

public class DCRAddingMachineDaoImpl extends
		AbstractWMSSqlMapClientDaoSupport<DCRAddingMachine> implements
		DCRAddingMachineDao {

	private static final String NAMESPACE = "DCRAddingMachine";

	@Override
	protected String getNamespace() {
		return NAMESPACE;
	}

	@SuppressWarnings("unchecked")
	@Override
	public DCRAddingMachine getByDcrCashierId(Long dcrCashierId)
			throws WMSDaoException {
		DCRAddingMachine entity = null;

		try {
			List<DCRAddingMachine> list = getSqlMapClientTemplate()
					.queryForList(getNamespace() + ".getByDcrCashierId",
							dcrCashierId);

			if (CollectionUtils.isNotEmpty(list)) {
				entity = list.get(0);
			}

		} catch (Exception e) {
			throw new WMSDaoException(e);
		}
		return entity;
	}

}
