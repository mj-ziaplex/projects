package ph.com.sunlife.wms.dao;

import java.util.List;

import ph.com.sunlife.wms.dao.domain.Contact;
import ph.com.sunlife.wms.dao.exceptions.WMSDaoException;

/**
 * The address book.
 * 
 * @author Crisseljess Mendoza
 * 
 */
public interface ContactDao {

	/**
	 * get all WMS Users registered in the database.
	 * 
	 * @return
	 * @throws WMSDaoException
	 */
	List<Contact> getAllWMSUsers() throws WMSDaoException;
}
