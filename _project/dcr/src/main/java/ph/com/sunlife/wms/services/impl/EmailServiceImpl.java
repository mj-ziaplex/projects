package ph.com.sunlife.wms.services.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.axis.utils.ByteArrayOutputStream;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.mail.javamail.JavaMailSender;

import ph.com.sunlife.wms.dao.ContactDao;
import ph.com.sunlife.wms.dao.DCRDao;
import ph.com.sunlife.wms.dao.domain.Contact;
import ph.com.sunlife.wms.dao.domain.DCR;
import ph.com.sunlife.wms.dao.domain.DCRNonCashierFile;
import ph.com.sunlife.wms.dao.exceptions.WMSDaoException;
import ph.com.sunlife.wms.integration.DCRFilenetIntegrationService;
import ph.com.sunlife.wms.integration.exceptions.IntegrationException;
import ph.com.sunlife.wms.services.EmailService;
import ph.com.sunlife.wms.services.bo.ContactBO;
import ph.com.sunlife.wms.services.bo.WMSEmail;
import ph.com.sunlife.wms.services.exception.ServiceException;
import ph.com.sunlife.wms.util.WMSDateUtil;
import ph.com.sunlife.wms.util.WMSStringUtil;

/**
 * The impelementing class of {@link EmailService}.
 * 
 * @author Crisseljess Mendoza
 * @author Zainal Limpao
 */
public class EmailServiceImpl implements EmailService {

	private static final Logger LOGGER = Logger
			.getLogger(EmailServiceImpl.class);

	private static final String EMAIL_SUFFIX_KEY = "@Sunlife.com";

	private JavaMailSender mailSender;

	private DCRFilenetIntegrationService dcrFilenetIntegrationService;

	private DCRDao dcrDao;

	private ContactDao contactDao;

	public void setDcrDao(DCRDao dcrDao) {
		this.dcrDao = dcrDao;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ph.com.sunlife.wms.services.EmailService#getAddressBook()
	 */
	@Override
	public List<ContactBO> getAddressBook() throws ServiceException {

		try {
			List<Contact> list = contactDao.getAllWMSUsers();

			List<ContactBO> bos = new ArrayList<ContactBO>();
			this.copyToBO(list, bos);

			return bos;
		} catch (WMSDaoException e) {
			LOGGER.error(e);
			throw new ServiceException(e);
		}

	}

	private void copyToBO(List<Contact> list, List<ContactBO> bos) {
		for (Contact contact : list) {
			ContactBO bo = new ContactBO();
			bo.setUserId(contact.getUserId());
			bo.setFullName(contact.getFullName());
			bo.setShortName(contact.getShortName());
			bo.setEmailAddress(bo.getShortName() + EMAIL_SUFFIX_KEY);

			bos.add(bo);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ph.com.sunlife.wms.services.EmailService#sendEmail(ph.com.sunlife.wms
	 * .services.bo.WMSEmail)
	 */
	@Override
	public boolean sendEmail(WMSEmail email) throws ServiceException {

		MimeMessage message = mailSender.createMimeMessage();

		try {
			DCR dcr = dcrDao.getById(email.getDcrId());

			message.addRecipients(Message.RecipientType.TO,
					InternetAddress.parse(replaceSeparator(email.getTo())));
			if (email.getCc() != null) {
				message.addRecipients(Message.RecipientType.CC,
						InternetAddress.parse(replaceSeparator(email.getCc())));
			}
			message.setFrom(new InternetAddress(email.getFrom()));
			message.setText(WMSStringUtil.replaceEscChar(email.getText()));
			message.setSubject(WMSStringUtil.replaceEscChar(email.getSubject()));
			
			mailSender.send(message);
			LOGGER.debug("Email was sent successfully...");
			ByteArrayOutputStream os = new ByteArrayOutputStream();
			message.writeTo(os);

			byte[] emlData = os.toByteArray();

			Date today = new Date();
			String formattedDateStrWithTime = WMSDateUtil
					.toFormattedDateStrWithTime(today);
			String customFileName = "EMAIL_"
					+ StringUtils.upperCase(StringUtils.replace(
							formattedDateStrWithTime, " ", "_")) + ".eml";

			String docId = dcrFilenetIntegrationService.attachFileToWorkItem(
					dcr.getDcrDate(), dcr.getCcId(), customFileName, emlData);

			DCRNonCashierFile df = new DCRNonCashierFile();
			df.setDcrFileDescription("Email sent on "
					+ formattedDateStrWithTime);
			df.setDcrFileType("eml");
			df.setDcrFileUploadDate(today);
			df.setFileSize((long) emlData.length);
			df.setDcrId(dcr.getId());
			df.setUploaderId(email.getUserId());
			df.setVersionId(docId);

			Long dfId = dcrDao.saveNonCashierFile(df);
			df.setId(dfId);
		} catch (AddressException e) {
			LOGGER.error(e);
			throw new ServiceException(e);
		} catch (MessagingException e) {
			LOGGER.error(e);
			throw new ServiceException(e);
		} catch (IOException e) {
			LOGGER.error(e);
			throw new ServiceException(e);
		} catch (IntegrationException e) {
			LOGGER.error(e);
			throw new ServiceException(e);
		} catch (WMSDaoException e) {
			LOGGER.error(e);
			throw new ServiceException(e);
		}

		return true;
	}

	private String replaceSeparator(String sendingList) {
		if (StringUtils.contains(sendingList, ";")) {
			sendingList = StringUtils.replace(sendingList, ";", ",");
		}
		if (StringUtils.contains(sendingList, ":")) {
			sendingList = StringUtils.replace(sendingList, ":", ",");
		}
		return sendingList;
	}

	public void setMailSender(JavaMailSender mailSender) {
		this.mailSender = mailSender;
	}

	public void setDcrFilenetIntegrationService(
			DCRFilenetIntegrationService dcrFilenetIntegrationService) {
		this.dcrFilenetIntegrationService = dcrFilenetIntegrationService;
	}

	public void setContactDao(ContactDao contactDao) {
		this.contactDao = contactDao;
	}

	@Override
	public boolean sendSimpleMessage(WMSEmail email) throws ServiceException {
		MimeMessage message = mailSender.createMimeMessage();

		try {
			message.addRecipients(Message.RecipientType.TO,
					InternetAddress.parse(replaceSeparator(email.getTo())));

			message.setFrom(new InternetAddress(email.getFrom()));
			message.setText(email.getText());
			message.setSubject(email.getSubject());
			
			mailSender.send(message);
		} catch (AddressException e) {
			throw new ServiceException(e);
		} catch (MessagingException e) {
			throw new ServiceException(e);
		}

		return true;
	}

}
