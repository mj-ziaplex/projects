package ph.com.sunlife.wms.dao.domain;

public class GAFValue {

	private String companyName;
	
	private String prodCode;
	
	private String paymentCurrency;
	
	private String paymentPostStatus;
	
	private String payType;
	
	private Double totalAmount;
	
	private String userId;
	
	private double cashAmount;
	
	private double cardAmount;
	
	private double checkAmount;
	
	private double ncashAmount;
	
	public double getCashAmount() {
		return cashAmount;
	}

	public void setCashAmount(double cashAmount) {
		this.cashAmount = cashAmount;
	}

	public double getCardAmount() {
		return cardAmount;
	}

	public void setCardAmount(double cardAmount) {
		this.cardAmount = cardAmount;
	}

	public double getCheckAmount() {
		return checkAmount;
	}

	public void setCheckAmount(double checkAmount) {
		this.checkAmount = checkAmount;
	}

	public double getNcashAmount() {
		return ncashAmount;
	}

	public void setNcashAmount(double ncashAmount) {
		this.ncashAmount = ncashAmount;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getProdCode() {
		return prodCode;
	}

	public void setProdCode(String prodCode) {
		this.prodCode = prodCode;
	}

	public String getPaymentCurrency() {
		return paymentCurrency;
	}

	public void setPaymentCurrency(String paymentCurrency) {
		this.paymentCurrency = paymentCurrency;
	}

	public String getPaymentPostStatus() {
		return paymentPostStatus;
	}

	public void setPaymentPostStatus(String paymentPostStatus) {
		this.paymentPostStatus = paymentPostStatus;
	}

	public String getPayType() {
		return payType;
	}

	public void setPayType(String payType) {
		this.payType = payType;
	}

	public Double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(Double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	@Override
	public String toString() {
		return "GAFValue [companyName=" + companyName + ", prodCode="
				+ prodCode + ", paymentCurrency=" + paymentCurrency
				+ ", paymentPostStatus=" + paymentPostStatus + ", payType="
				+ payType + ", totalAmount=" + totalAmount + ", userId="
				+ userId + ", cashAmount=" + cashAmount + ", cardAmount="
				+ cardAmount + ", checkAmount=" + checkAmount
				+ ", ncashAmount=" + ncashAmount + "]";
	}
	
}
