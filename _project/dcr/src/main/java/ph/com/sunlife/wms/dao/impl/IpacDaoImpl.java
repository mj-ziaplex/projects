/**
 * @author i176
 * ****************************************************************************
 * @author: ia23 - sasay
 * @date: Mar2016
 * @desc: Removed methods that will not be used due to MR-WF-15-00089 - DCR
 * Redesign
 */
package ph.com.sunlife.wms.dao.impl;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import oracle.jdbc.OracleTypes;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.CallableStatementCallback;
import org.springframework.jdbc.core.CallableStatementCreator;
import org.springframework.jdbc.core.PreparedStatementCallback;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

import ph.com.sunlife.wms.dao.IpacDao;
import ph.com.sunlife.wms.dao.domain.Bank;
import ph.com.sunlife.wms.dao.domain.Currency;
import ph.com.sunlife.wms.dao.domain.DCRCashDepositSlip;
import ph.com.sunlife.wms.dao.domain.DCRChequeDepositSlip;
import ph.com.sunlife.wms.dao.exceptions.WMSDaoException;
import ph.com.sunlife.wms.util.IpacUtil;
import ph.com.sunlife.wms.util.WMSDateUtil;

public class IpacDaoImpl extends JdbcDaoSupport implements IpacDao {

    private static final String CHINATRUST_ID = "CTB";
    private static final Logger LOGGER = Logger.getLogger(IpacDaoImpl.class);
    private static final String SQL_DRAWEE_BANK = "SELECT bank_code, bank_desc "
            + "FROM IPAC_WMS_DRAWEE ORDER BY bank_desc";
    private static final String SQL_COMBINED_GET_BANK_DETAILS_SQL = "SELECT bank_initial, bank_account, bank_desc FROM IPAC_WMS_BANKACCOUNT_VW "
            + "LEFT OUTER JOIN (IPAC_WMS_DRAWEE)"
            + "ON (IPAC_WMS_DRAWEE.bank_code = IPAC_WMS_BANKACCOUNT_VW.bank_initial)"
            + "WHERE site_code = ? "
            + "AND com_code LIKE ? "
            + "AND pay_sub_code = ?";
    private static final String GET_BANK_DETAILS_SQL = "SELECT bank_initial, bank_account "
            + "FROM IPAC_WMS_BANKACCOUNT_VW "
            + "WHERE site_code = ? "
            + "AND com_code LIKE ? "
            + "AND pay_sub_code = ?";
    private static final int INDEX_CENTER_CODE = 1;
    private static final int INDEX_PROCESS_DATE = 2;
    private static final int INDEX_USER_ID = 3;
    private static final int INDEX_RESULT = 4;

    @Override
    public boolean hasCollections(final String centerCode,
            final Date processDate, final String acf2id) throws WMSDaoException {
        LOGGER.info("Invoking IPAC SP: wms_has_transaction()...");

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("site_code: " + centerCode);
            LOGGER.debug("process_date: " + processDate);
            LOGGER.debug("user_id: " + acf2id);
        }

        final String dateStr = WMSDateUtil.toFormattedDateStr(processDate);

        StringBuilder sb = new StringBuilder();
        sb.append("declare ");
        sb.append("a    boolean := false; ");
        sb.append("begin ");
        sb.append("a := ipac_report.wms_has_transaction(?,?,?); ");
        sb.append("? := CASE WHEN a THEN 'TRUE' ELSE 'FALSE' END; ");
        sb.append("end; ");
        final String sql = sb.toString();

        String result = null;

        try {
            result = (String) getJdbcTemplate().execute(
                    new CallableStatementCreator() {
                public CallableStatement createCallableStatement(
                        Connection con) throws SQLException {
                    CallableStatement cs = con.prepareCall(sql);
                    cs.setString(INDEX_CENTER_CODE,
                            StringUtils.upperCase(centerCode));
                    cs.setString(INDEX_PROCESS_DATE,
                            StringUtils.upperCase(dateStr));
                    cs.setString(INDEX_USER_ID,
                            StringUtils.upperCase(acf2id));
                    cs.registerOutParameter(INDEX_RESULT,
                            OracleTypes.VARCHAR);
                    return cs;
                }
            }, new CallableStatementCallback() {

                @Override
                public Object doInCallableStatement(CallableStatement cs)
                        throws SQLException, DataAccessException {
                    cs.execute();
                    String result = cs.getString(4);
                    return result;
                }
            });
        } catch (Exception ex) {
            throw new WMSDaoException(ex);
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Result is " + result);
        }

        return StringUtils.equalsIgnoreCase("TRUE", result);
    }

    // Modify to handle special MF transactions
    //  - Change is made to handle MF(PCO)
    @Override
    public DCRCashDepositSlip populateBankAccount(
            String centerCode,
            DCRCashDepositSlip entity) throws WMSDaoException {

        final String paySubCode;
        if (Currency.USD.equals(entity.getCurrency())) {
            paySubCode = "UCH";
        } else {
            paySubCode = "CPH";
        }

        String comCode = IpacUtil.convertToIpacCode(entity.getCompany().getName());
        DCRCashDepositSlip depositSlip = null;
        try {
            // Change according to Healthcheck
            // *Problem was identified as value is being set on catch block
            // *Change was to add If-Else condition to house desposit slip values
            depositSlip = (DCRCashDepositSlip) getJdbcTemplate().execute(
                    createPreparedStatement(centerCode, paySubCode, comCode, SQL_COMBINED_GET_BANK_DETAILS_SQL),
                    createCashDepoSlipCallback(entity, true));

            if (null == depositSlip) {
                depositSlip = (DCRCashDepositSlip) getJdbcTemplate().execute(
                        createPreparedStatement(centerCode, paySubCode, comCode, GET_BANK_DETAILS_SQL),
                        createCashDepoSlipCallback(entity, false));
            }
        } catch (Exception ex) {
            // Change according to Healthcheck
            // *Problem was identified as value is being set on catch block
            // *Change remove setting of value and just log error encountered
            LOGGER.error("Problem in getting bank accounts: ", ex);
        }
        return depositSlip;
    }

    // Modify to handle special MF transactions
    //  - Change is made to handle MF(PCO)
    @Override
    public DCRChequeDepositSlip populateBankAccount(
            final String centerCode,
            final DCRChequeDepositSlip entity) throws WMSDaoException {

        String paySubCode = entity.getCheckType().getPaySubCode();
        String comCode = IpacUtil.convertToIpacCode(entity.getCompany().getName());

        DCRChequeDepositSlip depositSlip = null;
        try {
            // Change according to Healthcheck
            // *Problem was identified as value is being set on catch block
            // *Change was to add If-Else condition to house desposit slip values
            depositSlip = (DCRChequeDepositSlip) getJdbcTemplate().execute(
                    createPreparedStatement(centerCode, paySubCode, comCode, SQL_COMBINED_GET_BANK_DETAILS_SQL),
                    createChequeDepoSlipCallback(entity, true));
            if (null == depositSlip) {
                depositSlip = (DCRChequeDepositSlip) getJdbcTemplate().execute(
                        createPreparedStatement(centerCode, paySubCode, comCode, GET_BANK_DETAILS_SQL),
                        createChequeDepoSlipCallback(entity, false));
            }
        } catch (Exception ex) {
            // Change according to Healthcheck
            // *Problem was identified as value is being set on catch block
            // *Change remove setting of value and just log error encountered
            LOGGER.error("Problem in getting bank accounts: ", ex);
        }
        return depositSlip;
    }

    private PreparedStatementCallback createChequeDepoSlipCallback(
            final DCRChequeDepositSlip entity, final boolean withBankDesc) {
        return new PreparedStatementCallback() {

            @Override
            public Object doInPreparedStatement(PreparedStatement ps)
                    throws SQLException, DataAccessException {
                ResultSet rs = ps.executeQuery();

                while (rs.next()) {
                    String accountNumber = rs.getString("bank_account");
                    Bank bank = createBank(rs, withBankDesc);
                    entity.setAccountNumber(accountNumber);
                    entity.setDepositoryBank(bank);

                    if (StringUtils.isNotEmpty(bank.getName())) {
                        break;
                    }
                }
                return entity;
            }
        };
    }

    private Bank createBank(ResultSet rs, boolean withBankDesc)
            throws SQLException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Retrieving Bank Information from IPAC DB...");
            LOGGER.debug("Has Bank Description: " + withBankDesc);
        }

        Bank bank = new Bank();

        if (withBankDesc) {
            String bankInitial = rs.getString("bank_initial");
            String bankDesc = rs.getString("bank_desc");

            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("bank_initial: " + bankInitial);
                LOGGER.debug("bank_desc: " + bankDesc);
            }

            bank.setId(bankInitial);
            bank.setName(bankDesc);
        } else {
            String bankInitial = rs.getString("bank_initial");

            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("bank_initial: " + bankInitial);
            }

            bank.setId(bankInitial);
        }

        return bank;
    }

    private PreparedStatementCallback createCashDepoSlipCallback(
            final DCRCashDepositSlip entity,
            final boolean withBankDesc) {
        
        return new PreparedStatementCallback() {
            @Override
            public Object doInPreparedStatement(PreparedStatement ps) throws SQLException, DataAccessException {
                ResultSet rs = ps.executeQuery();

                while (rs.next()) {
                    String accountNumber = rs.getString("bank_account");
                    Bank bank = createBank(rs, withBankDesc);

                    if (!StringUtils.equalsIgnoreCase(bank.getId(), CHINATRUST_ID)) {
                        entity.setAccountNumber(accountNumber);
                        entity.setDepositoryBank(bank);
                    }

                    if (StringUtils.isNotEmpty(bank.getName())) {
                        break;
                    }
                }
                return entity;
            }
        };
    }

    // Modify to handle special MF transactions
    //  - Change is made to handle MF(PCO)
    private PreparedStatementCreator createPreparedStatement(
            final String centerCode,
            final String paySubCode,
            final String comCode,
            final String sql) {

        return new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(Connection con) throws SQLException {

                // Added to not include PCO bank account
                StringBuilder sqlTmp = new StringBuilder();
                sqlTmp.append(sql);
                if(comCode.startsWith("MF") || comCode.startsWith("SLAMC")) { // Added contition to not get PCO
                    sqlTmp.append(" AND com_code NOT IN ('SLAMCI')");
                }
                LOGGER.debug("FINAL SQL: " + sqlTmp);
                
                PreparedStatement ps = con.prepareStatement(sqlTmp.toString());
                String centerCodeUp = StringUtils.upperCase(centerCode);
                String comCodeUp = StringUtils.upperCase(comCode + "%");
                String paySubCodeUp = StringUtils.upperCase(paySubCode);

                if (LOGGER.isDebugEnabled()) {
                    LOGGER.debug("Parameter 1: " + centerCodeUp);
                    LOGGER.debug("Parameter 2: " + comCodeUp);
                    LOGGER.debug("Parameter 3: " + paySubCodeUp);
                }

                ps.setString(1, centerCodeUp);
                ps.setString(2, comCodeUp);
                ps.setString(3, paySubCodeUp);
                return ps;
            }
        };
    }

    @Override
    public int getCurrentPdcCount(final String customerCenterId)
            throws WMSDaoException {

        LOGGER.info("Invoking IPAC SP: get_pdc_count()...");

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("site_code: " + customerCenterId);
        }

        int count = 0;

        StringBuilder sb = new StringBuilder();
        sb.append("declare ");
        sb.append("cc_id varchar(2); ");
        sb.append("pdc_count number; ");
        sb.append("begin ");
        sb.append("cc_id := ?; ");
        sb.append("pdc_count := ipac_report.get_pdc_count(cc_id); ");
        sb.append("? := pdc_count; ");
        sb.append("end; ");

        final String sql = sb.toString();

        try {
            count = (Integer) getJdbcTemplate().execute(
                    new CallableStatementCreator() {
                public CallableStatement createCallableStatement(
                        Connection con) throws SQLException {
                    CallableStatement cs = con.prepareCall(sql);
                    cs.setString(1,
                            StringUtils.upperCase(customerCenterId));
                    cs.registerOutParameter(2, OracleTypes.NUMBER);
                    return cs;
                }
            }, new CallableStatementCallback() {

                @Override
                public Object doInCallableStatement(CallableStatement cs)
                        throws SQLException, DataAccessException {
                    cs.execute();
                    int result = cs.getInt(2);
                    return result;
                }
            });
        } catch (Exception ex) {
            throw new WMSDaoException(ex);
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("PDC Count is " + count);
        }

        return count;
    }

    @Override
    public int getCurrentPNPdcCount(final String customerCenterId)
            throws WMSDaoException {
        return getProdPdcCount(customerCenterId, "PN");
    }

    @Override
    public int getCurrentTradVulPdcCount(final String customerCenterId)
            throws WMSDaoException {
        int tradPdcCount = getProdPdcCount(customerCenterId, "IL");
        int vulPdcCount = getProdPdcCount(customerCenterId, "VL");
        return tradPdcCount + vulPdcCount;
    }

    int getProdPdcCount(final String customerCenterId, final String prodCode) {
        LOGGER.info("Invoking IPAC SP: get_prod_pdc_count()...");

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("site_code: " + customerCenterId);
            LOGGER.debug("prod_code: " + prodCode);
        }

        final StringBuilder sb = new StringBuilder();
        sb.append("declare ");
        sb.append("cc_id varchar(2); ");
        sb.append("prod_code varchar(4); ");
        sb.append("pdc_count number; ");
        sb.append("begin ");
        sb.append("cc_id := ?; ");
        sb.append("prod_code := ?; ");
        sb.append("pdc_count := ipac_report.get_prod_pdc_count(cc_id, prod_code); ");
        sb.append("? := pdc_count; ");
        sb.append("end; ");

        int count;
        count = (Integer) getJdbcTemplate().execute(
                new CallableStatementCreator() {
            public CallableStatement createCallableStatement(
                    Connection con) throws SQLException {
                CallableStatement cs = con.prepareCall(sb.toString());
                cs.setString(1, StringUtils.upperCase(customerCenterId));
                cs.setString(2, prodCode);
                cs.registerOutParameter(3, OracleTypes.NUMBER);
                return cs;
            }
        }, new CallableStatementCallback() {

            @Override
            public Object doInCallableStatement(CallableStatement cs)
                    throws SQLException, DataAccessException {
                cs.execute();
                int result = cs.getInt(3);
                return result;
            }
        });

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("PDC Count is " + count);
        }

        return count;
    }

    public int getSalesSlipCount(String siteCode, String prodCode,
            Date processDate, Currency currency, String userId)
            throws WMSDaoException {
        String sql = "SELECT IPAC_REPORT.get_card_sales_slip_count(?, ?, ?, ?, ?) from dual";

        int result = 0;
        try {
            SalesSlipCountAction action = new SalesSlipCountAction(siteCode,
                    prodCode, processDate, currency, userId);
            result = (Integer) getJdbcTemplate().execute(sql, action);
        } catch (Exception ex) {
            LOGGER.error(ex);
            // Swallow this exception
        }
        return result;
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Bank> getAllDraweeBanks() throws WMSDaoException {
        List<Bank> draweeBanks = null;
        try {
            GetAllDraweeBanksCallback action = createAllDraweeBanksAction();
            draweeBanks = (List<Bank>) getJdbcTemplate().execute(
                    SQL_DRAWEE_BANK, action);
            return draweeBanks;
        } catch (Exception ex) {
            LOGGER.error(ex);
            throw new WMSDaoException(ex);
        }
    }

    GetAllDraweeBanksCallback createAllDraweeBanksAction() {
        return new GetAllDraweeBanksCallback();
    }

    static class GetAllDraweeBanksCallback implements PreparedStatementCallback {

        @Override
        public Object doInPreparedStatement(PreparedStatement ps)
                throws SQLException, DataAccessException {
            ResultSet rs = ps.executeQuery();

            List<Bank> draweeBanks = new ArrayList<Bank>();
            while (rs.next()) {
                Bank bank = new Bank();
                String name = rs.getString("bank_desc");
                String id = rs.getString("bank_code");

                if (StringUtils.isNotEmpty(name) && StringUtils.isNotEmpty(id)) {
                    bank.setId(id);
                    bank.setName(name);
                    draweeBanks.add(bank);
                }
            }

            return draweeBanks;
        }

    }

    static class SalesSlipCountAction implements PreparedStatementCallback {

        private String siteCode;

        private Date processDate;

        private String prodCode;

        private String userId;

        private String currencyCode;

        public SalesSlipCountAction(String siteCode, String prodCode,
                Date processDate, Currency currency, String userId) {
            this.siteCode = siteCode;
            this.processDate = processDate;
            this.prodCode = prodCode;
            this.userId = userId;
            this.currencyCode = currency.getName();
        }

        @Override
        public Object doInPreparedStatement(PreparedStatement ps)
                throws SQLException, DataAccessException {
            ps.setString(1, siteCode);
            ps.setString(2, prodCode);
            ps.setDate(3, new java.sql.Date(processDate.getTime()));
            ps.setString(4, currencyCode);
            ps.setString(5, userId);

            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                return rs.getInt(1);
            }

            return 0;
        }

    }
}
