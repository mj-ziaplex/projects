package ph.com.sunlife.wms.services.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;

import ph.com.sunlife.wms.dao.JobDataDao;
import ph.com.sunlife.wms.dao.domain.JobData;
import ph.com.sunlife.wms.dao.domain.JobDataErrorLog;
import ph.com.sunlife.wms.dao.exceptions.WMSDaoException;
import ph.com.sunlife.wms.services.JobDataService;
import ph.com.sunlife.wms.services.bo.JobDataBO;
import ph.com.sunlife.wms.services.bo.JobDataErrorLogBO;
import ph.com.sunlife.wms.services.exception.ServiceException;
import ph.com.sunlife.wms.util.ExceptionUtil;

public class JobDataServiceImpl implements JobDataService {

	private static final Logger LOGGER = Logger
			.getLogger(JobDataServiceImpl.class);

	private JobDataDao jobDataDao;

	@Override
	public List<JobDataBO> getAllJobData() throws ServiceException {
		try {
			List<JobData> list = jobDataDao.getAllJobData();

			if (CollectionUtils.isNotEmpty(list)) {
				List<JobDataBO> resultList = new ArrayList<JobDataBO>();
				for (JobData job : list) {
					JobDataBO bo = new JobDataBO();

					boolean isLastRunNull = job.getLastRun() == null;
					if (isLastRunNull) {
						job.setLastRun(new Date());
					}
					BeanUtils.copyProperties(bo, job);
					
					if (isLastRunNull) {
						bo.setLastRun(null);
						bo.setSuccessful(false);
					}
					
					resultList.add(bo);
				}
				return resultList;
			}

			return null;
		} catch (WMSDaoException e) {
			LOGGER.error(e);
			throw new ServiceException(e);
		} catch (IllegalAccessException e) {
			LOGGER.error(e);
			throw new ServiceException(e);
		} catch (InvocationTargetException e) {
			LOGGER.error(e);
			throw new ServiceException(e);
		}

	}

	@Override
	public List<JobDataErrorLogBO> getJobDataErrorLog(Long jobId)
			throws ServiceException {
		try {
			List<JobDataErrorLog> list = jobDataDao.getJobDataErrorLog(jobId);

			if (CollectionUtils.isNotEmpty(list)) {
				List<JobDataErrorLogBO> resultList = new ArrayList<JobDataErrorLogBO>();
				for (JobDataErrorLog errorLog : list) {
					JobDataErrorLogBO bo = new JobDataErrorLogBO();
					BeanUtils.copyProperties(bo, errorLog);
					resultList.add(bo);
				}
				return resultList;
			}

			return null;
		} catch (WMSDaoException e) {
			LOGGER.error(e);
			throw new ServiceException(e);
		} catch (IllegalAccessException e) {
			LOGGER.error(e);
			throw new ServiceException(e);
		} catch (InvocationTargetException e) {
			LOGGER.error(e);
			throw new ServiceException(e);
		}
	}

	@Override
	public boolean updateJobData(JobDataBO jobDataBO, Exception ex)
			throws ServiceException {
		try {
			JobData jobData = new JobData();
			BeanUtils.copyProperties(jobData, jobDataBO);

			if (ex != null) {
				JobDataErrorLog log = new JobDataErrorLog();
				log.setJobId(jobData.getId());
				log.setDateTime(new Date());
				log.setErrorMessage(ExceptionUtil.getStackTrace(ex, 1000));
				jobDataDao.createErrorLog(log);
			}
			return jobDataDao.updateJobData(jobData);
		} catch (IllegalAccessException e) {
			throw new ServiceException(e);
		} catch (InvocationTargetException e) {
			throw new ServiceException(e);
		} catch (WMSDaoException e) {
			throw new ServiceException(e);
		}
	}

	public void setJobDataDao(JobDataDao jobDataDao) {
		this.jobDataDao = jobDataDao;
	}

}
