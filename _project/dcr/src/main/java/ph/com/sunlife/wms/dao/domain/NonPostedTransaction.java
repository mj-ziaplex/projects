package ph.com.sunlife.wms.dao.domain;

public class NonPostedTransaction {

	private String companyCode;
	
	private String productCode;
	
	private String transactionType;
	
	private String paymentCurrencyStr;
	
	private double amount;
	
	private String userId;
	
	private int paymentPostStatus = -1;

	public String getCompanyCode() {
		return companyCode;
	}

	public void setCompanyCode(String companyCode) {
		this.companyCode = companyCode;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}

	public String getPaymentCurrencyStr() {
		return paymentCurrencyStr;
	}

	public void setPaymentCurrencyStr(String paymentCurrencyStr) {
		this.paymentCurrencyStr = paymentCurrencyStr;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public int getPaymentPostStatus() {
		return paymentPostStatus;
	}

	public void setPaymentPostStatus(int paymentPostStatus) {
		this.paymentPostStatus = paymentPostStatus;
	}

	@Override
	public String toString() {
		return "NonPostedTransaction [companyCode=" + companyCode
				+ ", productCode=" + productCode + ", transactionType="
				+ transactionType + ", paymentCurrencyStr="
				+ paymentCurrencyStr + ", amount=" + amount + ", userId="
				+ userId + ", paymentPostStatus=" + paymentPostStatus + "]";
	}

}
