package ph.com.sunlife.wms.services.bo;

import java.util.Date;

public class DCRDepositSlipBO {

	private Long id;

	private CompanyBO company;

	private DCRCashierBO dcrCashier;

	private Date dateTimeCreated;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public CompanyBO getCompany() {
		return company;
	}

	public void setCompany(CompanyBO company) {
		this.company = company;
	}
	
	public void setCompany(Long companyId) {
		this.setCompany(CompanyBO.getCompany(companyId));
	}

	public DCRCashierBO getDcrCashier() {
		return dcrCashier;
	}

	public void setDcrCashier(DCRCashierBO dcrCashier) {
		this.dcrCashier = dcrCashier;
	}
	
	public void setDcrCashier(Long dcrCashierId) {
		DCRCashierBO dcrCashier = new DCRCashierBO();
		dcrCashier.setId(dcrCashierId);
		this.setDcrCashier(dcrCashier);
	}	

	public Date getDateTimeCreated() {
		return dateTimeCreated;
	}

	public void setDateTimeCreated(Date dateTimeCreated) {
		this.dateTimeCreated = dateTimeCreated;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DCRDepositSlipBO other = (DCRDepositSlipBO) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
}
