package ph.com.sunlife.wms.security;

/**
 * A simple JNDI-LDAP.
 *
 * @author Zainal Limpao
 *
 */
public interface LdapManager {

    /**
     * Checks if both username and password are valid.
     *
     * @param username
     * @param password
     * @return
     * @throws Exception
     */
    boolean authenticateUser(String username, String password) throws Exception;
}
