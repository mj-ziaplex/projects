package ph.com.sunlife.wms.dao.domain;

import java.util.Date;

public class DCRNonCashierFile extends Entity {

	private Long dcrId;
	
	private Date dcrFileUploadDate;

	private String dcrFileDescription;
	
	private String versionId;
	
	private String dcrFileType;
	
	private Long fileSize;
	
	private String uploaderId;
	
	private String shortName;
	
	public String getUploaderId() {
		return uploaderId;
	}

	public void setUploaderId(String uploaderId) {
		this.uploaderId = uploaderId;
	}

	public Long getFileSize() {
		return fileSize;
	}

	public void setFileSize(Long fileSize) {
		this.fileSize = fileSize;
	}

	public Long getDcrId() {
		return dcrId;
	}

	public void setDcrId(Long dcrId) {
		this.dcrId = dcrId;
	}

	public Date getDcrFileUploadDate() {
		return dcrFileUploadDate;
	}

	public void setDcrFileUploadDate(Date dcrFileUploadDate) {
		this.dcrFileUploadDate = dcrFileUploadDate;
	}

	public String getDcrFileDescription() {
		return dcrFileDescription;
	}

	public void setDcrFileDescription(String dcrFileDescription) {
		this.dcrFileDescription = dcrFileDescription;
	}

	public String getVersionId() {
		return versionId;
	}

	public void setVersionId(String versionId) {
		this.versionId = versionId;
	}

	public String getDcrFileType() {
		return dcrFileType;
	}

	public void setDcrFileType(String dcrFileType) {
		this.dcrFileType = dcrFileType;
	}

	public String getShortName() {
		return shortName;
	}

	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

}
