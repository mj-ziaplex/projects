package ph.com.sunlife.wms.web.controller.validator;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import ph.com.sunlife.wms.services.DCRCreationService;
import ph.com.sunlife.wms.services.bo.DCRCashierBO;
import ph.com.sunlife.wms.services.bo.UserSession;
import ph.com.sunlife.wms.services.exception.ServiceException;
import ph.com.sunlife.wms.util.WMSConstants;
import ph.com.sunlife.wms.web.controller.form.CashierWorkItemForm;

/**
 * The {@link Validator} object for {@link CashierWorkItemForm}.
 * 
 * @author Zainal Limpao
 * 
 */
public class CashierWorkItemFormValidator implements Validator {

	private static final Logger LOGGER = Logger
			.getLogger(CashierWorkItemFormValidator.class);

	private DCRCreationService dcrCreationService;

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.validation.Validator#supports(java.lang.Class)
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public boolean supports(Class clazz) {
		return CashierWorkItemForm.class.isAssignableFrom(clazz);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.validation.Validator#validate(java.lang.Object,
	 * org.springframework.validation.Errors)
	 */
	@Override
	public void validate(Object target, Errors errors) {
		LOGGER.debug("Validating balancing tool form...");

		CashierWorkItemForm form = (CashierWorkItemForm) target;

		String acf2id = this.validateAndExtractUserId(errors, form);

		Long dcrCashierId = form.getDcrCashierId();
		this.checkUserForDCRWorkItem(errors, dcrCashierId, acf2id);

	}

	/**
	 * Validates if {@link UserSession#getUserId()} is present within the
	 * {@link CashierWorkItemForm}.
	 * 
	 * @param errors
	 * @param form
	 * @return
	 */
	private String validateAndExtractUserId(Errors errors,
			CashierWorkItemForm form) {
		HttpSession session = form.getSession();
		String acf2id = null;
		if (session == null) {
			// TODO: create error messages property file
			errors.rejectValue("session", "some.error.code");
		} else {
			UserSession userSession = (UserSession) session
					.getAttribute(WMSConstants.USER_SESSION);

			if (userSession == null) {
				// TODO: create error messages property file
				errors.rejectValue("session", "some.error.code");
			} else {
				acf2id = userSession.getUserId();
			}
		}
		return acf2id;
	}

	/**
	 * Check if the given user is alloed for that {@link DCRCashierBO}.
	 * 
	 * @param errors
	 * @param dcrCashierId
	 * @param acf2id
	 */
	private void checkUserForDCRWorkItem(Errors errors, Long dcrCashierId,
			String acf2id) {
		try {
			DCRCashierBO bo = dcrCreationService.getDCRCashierBO(dcrCashierId,
					acf2id);

			if (bo == null) {
				// TODO: create error messages property file
				errors.rejectValue("dcrCashierId", "some.error.code");
			}
		} catch (ServiceException e) {
			LOGGER.error(
					"A problem occured while retrieving DCRCashier object from DB",
					e);
		}
	}

	/**
	 * The injection setter for {@link DCRCreationService}.
	 * 
	 * @param dcrCreationService
	 */
	public void setDcrCreationService(DCRCreationService dcrCreationService) {
		this.dcrCreationService = dcrCreationService;
	}

}
