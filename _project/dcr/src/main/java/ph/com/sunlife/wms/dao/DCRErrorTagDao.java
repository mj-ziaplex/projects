package ph.com.sunlife.wms.dao;

import java.util.Date;
import java.util.List;

import ph.com.sunlife.wms.dao.domain.DCRError;
import ph.com.sunlife.wms.dao.domain.DCRErrorTagLog;
import ph.com.sunlife.wms.dao.domain.DCRErrorTagLogDisplay;
import ph.com.sunlife.wms.dao.domain.DCRErrorTagLogLookup;
import ph.com.sunlife.wms.dao.domain.DCRErrorTagLookupDisplay;
import ph.com.sunlife.wms.dao.exceptions.WMSDaoException;

public interface DCRErrorTagDao extends WMSDao<DCRErrorTagLog> {
//	List<DCRErrorTagLookupDisplay> getDCRErrorTagLookupDisplay(final Long dcrId)
//		throws WMSDaoException;
	
	DCRErrorTagLog addDCRErrorTagLog(DCRErrorTagLog entity)
			throws WMSDaoException;

	DCRErrorTagLogLookup addDCRErrorTagLoglookup(DCRErrorTagLogLookup entity)
			throws WMSDaoException;

	List<DCRErrorTagLogDisplay> getDCRErrorTagLogDisplay(final Long dcrId)
			throws WMSDaoException;
	
	boolean deleteErrorLogLookupByDcrIdAndStepId(final Long dcrId, String stepId) throws WMSDaoException;
	
	boolean updateErrorLogByDcrIdAndStepId(String reason, Date dateUpdated, final Long dcrId, String stepId, String postedUpdatedById) throws WMSDaoException;
	
	boolean deleteErrorLogByDcrIdAndStepId(String reasonDeleted, final Long dcrId, String stepId, String userId,String tagger) throws WMSDaoException;
	
	List<DCRError> getDCRErrorByStatus(final String dcrStatus) throws WMSDaoException;
	
	String getStepIdByStatus(final String dcrStatus) throws WMSDaoException;
	
	String getManagerByDcrId(final Long dcrId) throws WMSDaoException;
	
	List<String> getUsersFromAuditLogByDcrId(final Long dcrId) throws WMSDaoException;
}
