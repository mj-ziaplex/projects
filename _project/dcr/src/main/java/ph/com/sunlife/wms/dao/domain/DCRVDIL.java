package ph.com.sunlife.wms.dao.domain;

import java.util.Date;
import java.util.List;

public class DCRVDIL extends Entity {

	private DCRCashier dcrCashier = new DCRCashier();
	
	private double pettyCash;
	
	private double photocopy;
	
	private double workingFund;
	
	private double unclaimedChange;
	
	private double overages;
	
	private int vultradPDCRPendingWarehouse;
	
	private int pnPDCPendingWarehouse;
	
	private double afterCutoffCashPeso;
	
	private double afterCutoffCashDollar;
	
	private int afterCutoffCheck;
	
	private int afterCutoffCard;
	
	private String forSafeKeeping;
	
	private double totalDollarCoins;
	
	private List<DCRVDILPRSeriesNumber> prSeriesNumbers;
	
	private List<DCRVDILISOCollection> isoCollections;
	
	private DCRDollarCoinExchange dcrDollarCoinExchange;
	
	private DCRDollarCoinCollection currentCoinCollection;
	
	private DCRDollarCoinCollection previousCoinCollection;
	
	private Date dateSaved;
	
	private String vdilVersionSerialId;
	
	private int marker;
	
	public int getMarker() {
		return marker;
	}

	public void setMarker(int marker) {
		this.marker = marker;
	}

	public String getVdilVersionSerialId() {
		return vdilVersionSerialId;
	}

	public void setVdilVersionSerialId(String vdilVersionSerialId) {
		this.vdilVersionSerialId = vdilVersionSerialId;
	}

	public DCRCashier getDcrCashier() {
		return dcrCashier;
	}

	public void setDcrCashier(DCRCashier dcrCashier) {
		this.dcrCashier = dcrCashier;
	}
	
	public void setDcrCashier(Long dcrCashierId) {
		DCRCashier dcrCashier = new DCRCashier();
		dcrCashier.setId(dcrCashierId);
		this.setDcrCashier(dcrCashier);
	}
	
	public double getPettyCash() {
		return pettyCash;
	}

	public void setPettyCash(double pettyCash) {
		this.pettyCash = pettyCash;
	}

	public double getPhotocopy() {
		return photocopy;
	}

	public void setPhotocopy(double photocopy) {
		this.photocopy = photocopy;
	}

	public double getWorkingFund() {
		return workingFund;
	}

	public void setWorkingFund(double workingFund) {
		this.workingFund = workingFund;
	}

	public double getUnclaimedChange() {
		return unclaimedChange;
	}

	public void setUnclaimedChange(double unclaimedChange) {
		this.unclaimedChange = unclaimedChange;
	}

	public double getOverages() {
		return overages;
	}

	public void setOverages(double overages) {
		this.overages = overages;
	}

	public int getVultradPDCRPendingWarehouse() {
		return vultradPDCRPendingWarehouse;
	}

	public void setVultradPDCRPendingWarehouse(int vultradPDCRPendingWarehouse) {
		this.vultradPDCRPendingWarehouse = vultradPDCRPendingWarehouse;
	}

	public int getPnPDCPendingWarehouse() {
		return pnPDCPendingWarehouse;
	}

	public void setPnPDCPendingWarehouse(int pnPDCPendingWarehouse) {
		this.pnPDCPendingWarehouse = pnPDCPendingWarehouse;
	}

	public double getAfterCutoffCashPeso() {
		return afterCutoffCashPeso;
	}

	public void setAfterCutoffCashPeso(double afterCutoffCashPeso) {
		this.afterCutoffCashPeso = afterCutoffCashPeso;
	}

	public double getAfterCutoffCashDollar() {
		return afterCutoffCashDollar;
	}

	public void setAfterCutoffCashDollar(double afterCutoffCashDollar) {
		this.afterCutoffCashDollar = afterCutoffCashDollar;
	}

	public int getAfterCutoffCheck() {
		return afterCutoffCheck;
	}

	public void setAfterCutoffCheck(int afterCutoffCheck) {
		this.afterCutoffCheck = afterCutoffCheck;
	}

	public int getAfterCutoffCard() {
		return afterCutoffCard;
	}

	public void setAfterCutoffCard(int afterCutoffCard) {
		this.afterCutoffCard = afterCutoffCard;
	}

	public String getForSafeKeeping() {
		return forSafeKeeping;
	}

	public void setForSafeKeeping(String forSafeKeeping) {
		this.forSafeKeeping = forSafeKeeping;
	}

	public double getTotalDollarCoins() {
		return totalDollarCoins;
	}

	public void setTotalDollarCoins(double totalDollarCoins) {
		this.totalDollarCoins = totalDollarCoins;
	}

	public List<DCRVDILPRSeriesNumber> getPrSeriesNumbers() {
		return prSeriesNumbers;
	}

	public void setPrSeriesNumbers(List<DCRVDILPRSeriesNumber> prSeriesNumbers) {
		this.prSeriesNumbers = prSeriesNumbers;
	}

	public List<DCRVDILISOCollection> getIsoCollections() {
		return isoCollections;
	}

	public void setIsoCollections(List<DCRVDILISOCollection> isoCollections) {
		this.isoCollections = isoCollections;
	}

	public DCRDollarCoinExchange getDcrDollarCoinExchange() {
		return dcrDollarCoinExchange;
	}

	public void setDcrDollarCoinExchange(DCRDollarCoinExchange dcrDollarCoinExchange) {
		this.dcrDollarCoinExchange = dcrDollarCoinExchange;
	}

	public DCRDollarCoinCollection getCurrentCoinCollection() {
		return currentCoinCollection;
	}

	public void setCurrentCoinCollection(
			DCRDollarCoinCollection currentCoinCollection) {
		this.currentCoinCollection = currentCoinCollection;
	}

	public DCRDollarCoinCollection getPreviousCoinCollection() {
		return previousCoinCollection;
	}

	public void setPreviousCoinCollection(
			DCRDollarCoinCollection previousCoinCollection) {
		this.previousCoinCollection = previousCoinCollection;
	}

	public Date getDateSaved() {
		return dateSaved;
	}

	public void setDateSaved(Date dateSaved) {
		this.dateSaved = dateSaved;
	}

}
