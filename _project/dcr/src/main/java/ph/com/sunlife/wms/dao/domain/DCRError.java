package ph.com.sunlife.wms.dao.domain;


public class DCRError {
	private String errorId;
	private String errorName;
	
	public String getErrorId() {
		return errorId;
	}

	public void setErrorId(String errorId) {
		this.errorId = errorId;
	}

	public String getErrorName() {
		return errorName;
	}

	public void setErrorName(String errorName) {
		this.errorName = errorName;
	}



}
