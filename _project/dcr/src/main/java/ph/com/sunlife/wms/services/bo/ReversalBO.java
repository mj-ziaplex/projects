package ph.com.sunlife.wms.services.bo;

import java.util.Date;

import ph.com.sunlife.wms.dao.domain.Currency;

public class ReversalBO {

	private Long id;
	
	private DCRBalancingToolProductBO dcrBalancingToolProduct;
	
	private double amount;

	private String remarks;

	private Currency currency;

	private Date dateTime;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public DCRBalancingToolProductBO getDcrBalancingToolProduct() {
		return dcrBalancingToolProduct;
	}

	public void setDcrBalancingToolProduct(
			DCRBalancingToolProductBO dcrBalancingToolProduct) {
		this.dcrBalancingToolProduct = dcrBalancingToolProduct;
	}

	public void setDcrBalancingToolProduct(
			Long dcrBalancingToolProductId) {
		DCRBalancingToolProductBO dcrBalancingToolProduct = new DCRBalancingToolProductBO();
		dcrBalancingToolProduct.setId(dcrBalancingToolProductId);
		this.setDcrBalancingToolProduct(dcrBalancingToolProduct);
	}
	
	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public Currency getCurrency() {
		return currency;
	}

	public void setCurrency(Currency currency) {
		this.currency = currency;
	}

	public Date getDateTime() {
		return dateTime;
	}

	public void setDateTime(Date dateTime) {
		this.dateTime = dateTime;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ReversalBO other = (ReversalBO) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
}
