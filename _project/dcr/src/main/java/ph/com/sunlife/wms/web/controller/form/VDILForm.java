package ph.com.sunlife.wms.web.controller.form;

import java.util.Date;

import ph.com.sunlife.wms.util.WMSDateUtil;



public class VDILForm extends CashierWorkItemForm {
	
	private Long vdilId;
	
	private Double pettyCash;

	private Double photocopy;
	
	private Double workingFund;
	
	private Double unclaimedChange;
	
	private Double overages;
	
	//for US DOLLAR COIN COLLECTION
	private Double rcvdPrevDay;//new
	private Double rcvdToday;//new
	private Double lessChangeForTheDay;//new
	private Double totalDollarCoins;
	
	private Integer vultradPDCRPendingWarehouse;
	
	private Integer pnPDCPendingWarehouse;
	
	//does warehousetotal to be inserted/added to DCRVDILBO? but why is totalDollarCoins a column?
	//private double warehouseTotal;
	
	private Double afterCutoffCashPeso;
	
	private Double afterCutoffCashDollar;
	
	private Integer afterCutoffCheck;
	
	private Integer afterCutoffCard;
	
	private String forSafeKeeping;
	
	//DCRVDILISOCollectionBO
	private Long isoCollectionId0;
	private String dcrDate0;
	private String pickupDate0;
	private Double amount0;
	
	private Long isoCollectionId1;
	private String dcrDate1;
	private String pickupDate1;
	private Double amount1;
	
	private Long isoCollectionId2;
	private String dcrDate2;
	private String pickupDate2;
	private Double amount2;
	
	private Long isoCollectionId3;
	private String dcrDate3;
	private String pickupDate3;
	private Double amount3;
	
	private Date dcrDate0date;
	private Date dcrDate1date;
	private Date dcrDate2date;
	private Date dcrDate3date;
	private Date pickupDate0date;
	private Date pickupDate1date;
	private Date pickupDate2date;
	private Date pickupDate3date;
	
	//DCRVDILPRSeriesNumberBO
	private Long prSeriesNumberId0;
	private Long companyId0;
	private Integer from0;
	private Integer to0;
	private String reason0;
	
	private Long prSeriesNumberId1;
	private Long companyId1;
	private Integer from1;
	private Integer to1;
	private String reason1;
	
	private Long prSeriesNumberId2;
	private Long companyId2;
	private Integer from2;
	private Integer to2;
	private String reason2;
	
	private Long prSeriesNumberId3;
	private Long companyId3;
	private Integer from3;
	private Integer to3;
	private String reason3;
	
	private String companyName0;
	private String companyName1;
	private String companyName2;
	private String companyName3;
	
	private Integer in_denom1;
	private Integer in_denom50c;
	private Integer in_denom25c;
	private Integer in_denom10c;
	private Integer in_denom5c;
	private Integer in_denom1c;
	private Integer out_denom1;
	private Integer out_denom50c;
	private Integer out_denom25c;
	private Integer out_denom10c;
	private Integer out_denom5c;
	private Integer out_denom1c;
	
	public Long getVdilId() {
		return vdilId;
	}
	public void setVdilId(Long vdilId) {
		this.vdilId = vdilId;
	}
	public Double getPettyCash() {
		return pettyCash;
	}
	public void setPettyCash(Double pettyCash) {
		this.pettyCash = pettyCash;
	}
	public Double getPhotocopy() {
		return photocopy;
	}
	public void setPhotocopy(Double photocopy) {
		this.photocopy = photocopy;
	}
	public Double getWorkingFund() {
		return workingFund;
	}
	public void setWorkingFund(Double workingFund) {
		this.workingFund = workingFund;
	}
	public Double getUnclaimedChange() {
		return unclaimedChange;
	}
	public void setUnclaimedChange(Double unclaimedChange) {
		this.unclaimedChange = unclaimedChange;
	}
	public Double getOverages() {
		return overages;
	}
	public void setOverages(Double overages) {
		this.overages = overages;
	}
	public Double getTotalDollarCoins() {
		return totalDollarCoins;
	}
	public void setTotalDollarCoins(Double totalDollarCoins) {
		this.totalDollarCoins = totalDollarCoins;
	}
	public Integer getVultradPDCRPendingWarehouse() {
		return vultradPDCRPendingWarehouse;
	}
	public void setVultradPDCRPendingWarehouse(Integer vultradPDCRPendingWarehouse) {
		this.vultradPDCRPendingWarehouse = vultradPDCRPendingWarehouse;
	}
	public Integer getPnPDCPendingWarehouse() {
		return pnPDCPendingWarehouse;
	}
	public void setPnPDCPendingWarehouse(Integer pnPDCPendingWarehouse) {
		this.pnPDCPendingWarehouse = pnPDCPendingWarehouse;
	}
	public Double getAfterCutoffCashPeso() {
		return afterCutoffCashPeso;
	}
	public void setAfterCutoffCashPeso(Double afterCutoffCashPeso) {
		this.afterCutoffCashPeso = afterCutoffCashPeso;
	}
	public Double getAfterCutoffCashDollar() {
		return afterCutoffCashDollar;
	}
	public void setAfterCutoffCashDollar(Double afterCutoffCashDollar) {
		this.afterCutoffCashDollar = afterCutoffCashDollar;
	}
	public Integer getAfterCutoffCheck() {
		return afterCutoffCheck;
	}
	public void setAfterCutoffCheck(Integer afterCutoffCheck) {
		this.afterCutoffCheck = afterCutoffCheck;
	}
	public Integer getAfterCutoffCard() {
		return afterCutoffCard;
	}
	public void setAfterCutoffCard(Integer afterCutoffCard) {
		this.afterCutoffCard = afterCutoffCard;
	}
	public String getForSafeKeeping() {
		return forSafeKeeping;
	}
	public void setForSafeKeeping(String forSafeKeeping) {
		this.forSafeKeeping = forSafeKeeping;
	}
	public Long getIsoCollectionId0() {
		return isoCollectionId0;
	}
	public void setIsoCollectionId0(Long isoCollectionId0) {
		this.isoCollectionId0 = isoCollectionId0;
	}
	public String getDcrDate0() {
		return dcrDate0;
	}
	public void setDcrDate0(String dcrDate0) {
		this.dcrDate0 = dcrDate0;
		this.dcrDate0date = WMSDateUtil.toDate(dcrDate0);
	}
	public String getPickupDate0() {
		return pickupDate0;
	}
	public void setPickupDate0(String pickupDate0) {
		this.pickupDate0 = pickupDate0;
		this.pickupDate0date = WMSDateUtil.toDate(pickupDate0);
	}
	public Double getAmount0() {
		return amount0;
	}
	public void setAmount0(Double amount0) {
		this.amount0 = amount0;
	}
	public Long getIsoCollectionId1() {
		return isoCollectionId1;
	}
	public void setIsoCollectionId1(Long isoCollectionId1) {
		this.isoCollectionId1 = isoCollectionId1;
	}
	public String getDcrDate1() {
		return dcrDate1;
	}
	public void setDcrDate1(String dcrDate1) {
		this.dcrDate1 = dcrDate1;
		this.dcrDate1date = WMSDateUtil.toDate(dcrDate1);
	}
	public String getPickupDate1() {
		return pickupDate1;
	}
	public void setPickupDate1(String pickupDate1) {
		this.pickupDate1 = pickupDate1;
		this.pickupDate1date = WMSDateUtil.toDate(pickupDate1);
	}
	public Double getAmount1() {
		return amount1;
	}
	public void setAmount1(Double amount1) {
		this.amount1 = amount1;
	}
	public Long getIsoCollectionId2() {
		return isoCollectionId2;
	}
	public void setIsoCollectionId2(Long isoCollectionId2) {
		this.isoCollectionId2 = isoCollectionId2;
	}
	public String getDcrDate2() {
		return dcrDate2;
	}
	public void setDcrDate2(String dcrDate2) {
		this.dcrDate2 = dcrDate2;
		this.dcrDate2date = WMSDateUtil.toDate(dcrDate2);
	}
	public String getPickupDate2() {
		return pickupDate2;
	}
	public void setPickupDate2(String pickupDate2) {
		this.pickupDate2 = pickupDate2;
		this.pickupDate2date = WMSDateUtil.toDate(pickupDate2);
	}
	public Double getAmount2() {
		return amount2;
	}
	public void setAmount2(Double amount2) {
		this.amount2 = amount2;
	}
	public Long getIsoCollectionId3() {
		return isoCollectionId3;
	}
	public void setIsoCollectionId3(Long isoCollectionId3) {
		this.isoCollectionId3 = isoCollectionId3;
	}
	public String getDcrDate3() {
		return dcrDate3;
	}
	public void setDcrDate3(String dcrDate3) {
		this.dcrDate3 = dcrDate3;
		this.dcrDate3date = WMSDateUtil.toDate(dcrDate3);
	}
	public String getPickupDate3() {
		return pickupDate3;
	}
	public void setPickupDate3(String pickupDate3) {
		this.pickupDate3 = pickupDate3;
		this.pickupDate3date = WMSDateUtil.toDate(pickupDate3);
	}
	public Double getAmount3() {
		return amount3;
	}
	public void setAmount3(Double amount3) {
		this.amount3 = amount3;
	}
	public Long getPrSeriesNumberId0() {
		return prSeriesNumberId0;
	}
	public void setPrSeriesNumberId0(Long prSeriesNumberId0) {
		this.prSeriesNumberId0 = prSeriesNumberId0;
	}
	public Long getCompanyId0() {
		return companyId0;
	}
	public void setCompanyId0(Long companyId0) {
		this.companyId0 = companyId0 == 0L ? null : companyId0;
		this.companyName0 = getCompanyNameById(companyId0);
	}
	public Integer getFrom0() {
		return from0;
	}
	public void setFrom0(Integer from0) {
		this.from0 = from0;
	}
	public Integer getTo0() {
		return to0;
	}
	public void setTo0(Integer to0) {
		this.to0 = to0;
	}
	public String getReason0() {
		return reason0;
	}
	public void setReason0(String reason0) {
		this.reason0 = reason0 == "" ? null : reason0;
	}
	public Long getPrSeriesNumberId1() {
		return prSeriesNumberId1;
	}
	public void setPrSeriesNumberId1(Long prSeriesNumberId1) {
		this.prSeriesNumberId1 = prSeriesNumberId1;
	}
	public Long getCompanyId1() {
		return companyId1;
	}
	public void setCompanyId1(Long companyId1) {
		this.companyId1 = companyId1 == 0L ? null : companyId1;
		this.companyName1 = getCompanyNameById(companyId1);
	}
	public Integer getFrom1() {
		return from1;
	}
	public void setFrom1(Integer from1) {
		this.from1 = from1;
	}
	public Integer getTo1() {
		return to1;
	}
	public void setTo1(Integer to1) {
		this.to1 = to1;
	}
	public String getReason1() {
		return reason1;
	}
	public void setReason1(String reason1) {
		this.reason1 = reason1 == "" ? null : reason1;
	}
	public Long getPrSeriesNumberId2() {
		return prSeriesNumberId2;
	}
	public void setPrSeriesNumberId2(Long prSeriesNumberId2) {
		this.prSeriesNumberId2 = prSeriesNumberId2;
	}
	public Long getCompanyId2() {
		return companyId2;
	}
	public void setCompanyId2(Long companyId2) {
		this.companyId2 = companyId2 == 0L ? null : companyId2;
		this.companyName2 = getCompanyNameById(companyId2);
	}
	public Integer getFrom2() {
		return from2;
	}
	public void setFrom2(Integer from2) {
		this.from2 = from2;
	}
	public Integer getTo2() {
		return to2;
	}
	public void setTo2(Integer to2) {
		this.to2 = to2;
	}
	public String getReason2() {
		return reason2;
	}
	public void setReason2(String reason2) {
		this.reason2 = reason2 == "" ? null : reason2;
	}
	public Long getPrSeriesNumberId3() {
		return prSeriesNumberId3;
	}
	public void setPrSeriesNumberId3(Long prSeriesNumberId3) {
		this.prSeriesNumberId3 = prSeriesNumberId3;
	}
	public Long getCompanyId3() {
		return companyId3;
	}
	public void setCompanyId3(Long companyId3) {
		this.companyId3 = companyId3 == 0L ? null : companyId3;
		this.companyName3 = getCompanyNameById(companyId3);
	}
	public Integer getFrom3() {
		return from3;
	}
	public void setFrom3(Integer from3) {
		this.from3 = from3;
	}
	public Integer getTo3() {
		return to3;
	}
	public void setTo3(Integer to3) {
		this.to3 = to3;
	}
	public String getReason3() {
		return reason3;
	}
	public void setReason3(String reason3) {
		this.reason3 = reason3 == "" ? null : reason3;
	}
	
	//try only, to pass values to pdf
	public Double getRcvdPrevDay() {
		return rcvdPrevDay;
	}
	public void setRcvdPrevDay(Double rcvdPrevDay) {
		this.rcvdPrevDay = rcvdPrevDay;
	}
	public Double getRcvdToday() {
		return rcvdToday;
	}
	public void setRcvdToday(Double rcvdToday) {
		this.rcvdToday = rcvdToday;
	}
	public Double getLessChangeForTheDay() {
		return lessChangeForTheDay;
	}
	public void setLessChangeForTheDay(Double lessChangeForTheDay) {
		this.lessChangeForTheDay = lessChangeForTheDay;
	}
	
	//for jrxml, edit setter of date
	public Date getDcrDate0date() {
		return dcrDate0date;
	}
	public void setDcrDate0date(Date dcrDate0date) {
		this.dcrDate0date = dcrDate0date;
		this.dcrDate0 = WMSDateUtil.toFormattedDateStr(dcrDate0date);
	}
	public Date getDcrDate1date() {
		return dcrDate1date;
	}
	public void setDcrDate1date(Date dcrDate1date) {
		this.dcrDate1date = dcrDate1date;
		this.dcrDate1 = WMSDateUtil.toFormattedDateStr(dcrDate1date);
	}
	public Date getDcrDate2date() {
		return dcrDate2date;
	}
	public void setDcrDate2date(Date dcrDate2date) {
		this.dcrDate2date = dcrDate2date;
		this.dcrDate2 = WMSDateUtil.toFormattedDateStr(dcrDate2date);
	}
	public Date getDcrDate3date() {
		return dcrDate3date;
	}
	public void setDcrDate3date(Date dcrDate3date) {
		this.dcrDate3date = dcrDate3date;
		this.dcrDate3 = WMSDateUtil.toFormattedDateStr(dcrDate3date);
	}
	public Date getPickupDate0date() {
		return pickupDate0date;
	}
	public void setPickupDate0date(Date pickupDate0date) {
		this.pickupDate0date = pickupDate0date;
		this.pickupDate0 = WMSDateUtil.toFormattedDateStr(pickupDate0date);
	}
	public Date getPickupDate1date() {
		return pickupDate1date;
	}
	public void setPickupDate1date(Date pickupDate1date) {
		this.pickupDate1date = pickupDate1date;
		this.pickupDate1 = WMSDateUtil.toFormattedDateStr(pickupDate1date);
	}
	public Date getPickupDate2date() {
		return pickupDate2date;
	}
	public void setPickupDate2date(Date pickupDate2date) {
		this.pickupDate2date = pickupDate2date;
		this.pickupDate2 = WMSDateUtil.toFormattedDateStr(pickupDate2date);
	}
	public Date getPickupDate3date() {
		return pickupDate3date;
	}
	public void setPickupDate3date(Date pickupDate3date) {
		this.pickupDate3date = pickupDate3date;
		this.pickupDate3 = WMSDateUtil.toFormattedDateStr(pickupDate3date);
	}
	public String getCompanyName0() {
		return companyName0;
	}
	public void setCompanyName0(String companyName0) {
		this.companyName0 = companyName0;
	}
	public String getCompanyName1() {
		return companyName1;
	}
	public void setCompanyName1(String companyName1) {
		this.companyName1 = companyName1;
	}
	public String getCompanyName2() {
		return companyName2;
	}
	public void setCompanyName2(String companyName2) {
		this.companyName2 = companyName2;
	}
	public String getCompanyName3() {
		return companyName3;
	}
	public void setCompanyName3(String companyName3) {
		this.companyName3 = companyName3;
	}
	private static String getCompanyNameById(Long companyId) {
		if(companyId == 1L){
			return "SLAMCI PESO";
		}else if(companyId == 2L){
			return "SLAMCI DOLLAR";
		}else if(companyId == 3L){
			return "SLOCPI";
		}else if(companyId == 4L){
			return "SLGFI";
		}else if(companyId == 5L){
			return "SLFPI";
		}else if(companyId == 6L){
			return "OTHERS";
		}
		return null;
	}
	
	public Integer getIn_denom1() {
		return in_denom1;
	}
	public void setIn_denom1(Integer in_denom1) {
		this.in_denom1 = in_denom1;
	}
	public Integer getIn_denom50c() {
		return in_denom50c;
	}
	public void setIn_denom50c(Integer in_denom50c) {
		this.in_denom50c = in_denom50c;
	}
	public Integer getIn_denom25c() {
		return in_denom25c;
	}
	public void setIn_denom25c(Integer in_denom25c) {
		this.in_denom25c = in_denom25c;
	}
	public Integer getIn_denom10c() {
		return in_denom10c;
	}
	public void setIn_denom10c(Integer in_denom10c) {
		this.in_denom10c = in_denom10c;
	}
	public Integer getIn_denom5c() {
		return in_denom5c;
	}
	public void setIn_denom5c(Integer in_denom5c) {
		this.in_denom5c = in_denom5c;
	}
	public Integer getIn_denom1c() {
		return in_denom1c;
	}
	public void setIn_denom1c(Integer in_denom1c) {
		this.in_denom1c = in_denom1c;
	}
	public Integer getOut_denom1() {
		return out_denom1;
	}
	public void setOut_denom1(Integer out_denom1) {
		this.out_denom1 = out_denom1;
	}
	public Integer getOut_denom50c() {
		return out_denom50c;
	}
	public void setOut_denom50c(Integer out_denom50c) {
		this.out_denom50c = out_denom50c;
	}
	public Integer getOut_denom25c() {
		return out_denom25c;
	}
	public void setOut_denom25c(Integer out_denom25c) {
		this.out_denom25c = out_denom25c;
	}
	public Integer getOut_denom10c() {
		return out_denom10c;
	}
	public void setOut_denom10c(Integer out_denom10c) {
		this.out_denom10c = out_denom10c;
	}
	public Integer getOut_denom5c() {
		return out_denom5c;
	}
	public void setOut_denom5c(Integer out_denom5c) {
		this.out_denom5c = out_denom5c;
	}
	public Integer getOut_denom1c() {
		return out_denom1c;
	}
	public void setOut_denom1c(Integer out_denom1c) {
		this.out_denom1c = out_denom1c;
	}
	
}
