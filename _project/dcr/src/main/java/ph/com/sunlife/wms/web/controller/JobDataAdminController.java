package ph.com.sunlife.wms.web.controller;

import static ph.com.sunlife.wms.util.WMSConstants.CHECK_EXPIRING_DCR;
import static ph.com.sunlife.wms.util.WMSConstants.COLLECTION_DISCREPANCY_JOB_ID;
import static ph.com.sunlife.wms.util.WMSConstants.DCR_CREATION_JOB_ID;
import static ph.com.sunlife.wms.util.WMSConstants.POPULATE_IPAC_TO_DCR;
import static ph.com.sunlife.wms.util.WMSConstants.PPA_ABEYANCE_JOB_ID;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.apache.log4j.Logger;
import org.springframework.web.servlet.ModelAndView;

import ph.com.sunlife.wms.integration.exceptions.IntegrationException;
import ph.com.sunlife.wms.security.Role;
import ph.com.sunlife.wms.services.CollectionDiscrepancyReportService;
import ph.com.sunlife.wms.services.ConsolidatedDataService;
import ph.com.sunlife.wms.services.DCRCreationService;
import ph.com.sunlife.wms.services.DCRIPACService;
import ph.com.sunlife.wms.services.JobDataService;
import ph.com.sunlife.wms.services.bo.JobDataBO;
import ph.com.sunlife.wms.services.bo.JobDataErrorLogBO;
import ph.com.sunlife.wms.services.exception.ServiceException;
import ph.com.sunlife.wms.web.controller.form.JobDataAdminForm;
import ph.com.sunlife.wms.web.exception.ApplicationException;

public class JobDataAdminController extends AbstractSecuredMultiActionController {

	private static final Logger LOGGER = Logger
			.getLogger(JobDataAdminController.class);

	private DCRCreationService dcrCreationService;

	private CollectionDiscrepancyReportService collectionDiscrepancyReportService;

	private JobDataService jobDataService;

	private ConsolidatedDataService consolidatedDataService;

	private DCRIPACService dcrIPACService;
	
	public ModelAndView doShowJobDataAdminPage(HttpServletRequest request,
			HttpServletResponse response, JobDataAdminForm form)
			throws ApplicationException {

		ModelAndView mv = new ModelAndView("jobDataView");
		try {
			List<JobDataBO> jobs = jobDataService.getAllJobData();

			if (CollectionUtils.isNotEmpty(jobs)) {
				for (JobDataBO job : jobs) {
					if (!job.isSuccessful()) {
						List<JobDataErrorLogBO> errorLogs = jobDataService
								.getJobDataErrorLog(job.getId());
						if (CollectionUtils.isNotEmpty(errorLogs)) {
							JobDataErrorLogBO latestErrorLog = errorLogs.get(0);
							job.setLatestErrorLog(latestErrorLog);
						}
					}
				}
			}

			mv.addObject("jobs", jobs);
			return mv;
		} catch (ServiceException e) {
			throw new ApplicationException(e);
		}
	}
	
	public ModelAndView openDcrCreationPopup(HttpServletRequest request,
			HttpServletResponse response, JobDataAdminForm form) {
		ModelAndView modelAndView = new ModelAndView("dcrCreationView");
		modelAndView.addObject("isDcrCreationExecuted", false);
		return modelAndView;
	}

	@SuppressWarnings("unchecked")
	public ModelAndView executeJobManually(HttpServletRequest request,
			HttpServletResponse response, JobDataAdminForm form)
			throws ApplicationException {

		ModelAndView modelAndView = doShowJobDataAdminPage(request, response,
				form);

		List<JobDataBO> jobs = (List<JobDataBO>) modelAndView.getModel().get(
				"jobs");

		final Long jobDataId = form.getJobDataId();

		JobDataBO selectedJob = (JobDataBO) CollectionUtils.find(jobs,
				new Predicate() {
					@Override
					public boolean evaluate(Object object) {
						JobDataBO job = (JobDataBO) object;
						return job.getId().equals(jobDataId);
					}
				});

		selectedJob.setLastRun(new Date());
		Exception ex = null;

		modelAndView.addObject("isDcrCreationExecuted", false);
		if (DCR_CREATION_JOB_ID.equals(jobDataId)) {
			Date dcrDate = form.getDcrDate();
			try {
				dcrCreationService.initiateDCRWorkItems(dcrDate,true);
				selectedJob.setSuccessful(true);
			} catch (ServiceException e) {
				LOGGER.error(e);
				ex = e;
				selectedJob.setSuccessful(false);
			} catch (IntegrationException e) {
				LOGGER.error(e);
				ex = e;
				selectedJob.setSuccessful(false);
			}
			updateJobData(selectedJob, ex);
			modelAndView = openDcrCreationPopup(request, response, form);
			modelAndView.addObject("isDcrCreationExecuted", true);
		} else if (COLLECTION_DISCREPANCY_JOB_ID.equals(jobDataId)) {
			try {
				collectionDiscrepancyReportService
						.generateDiscrepancyReportNow();
				selectedJob.setSuccessful(true);
			} catch (ServiceException e) {
				LOGGER.error(e);
				ex = e;
				selectedJob.setSuccessful(false);
			}
			updateJobData(selectedJob, ex);

		} else if (PPA_ABEYANCE_JOB_ID.equals(jobDataId)) {
			try {
				consolidatedDataService.ppaAutoRun();
				selectedJob.setSuccessful(true);
			} catch (ServiceException e) {
				LOGGER.error(e);
				ex = e;
				selectedJob.setSuccessful(false);
			}
			updateJobData(selectedJob, ex);
		} else if (CHECK_EXPIRING_DCR.equals(jobDataId)) {
			try {
				consolidatedDataService.checkAgeingDcrWorkItems();
				selectedJob.setSuccessful(true);
			} catch (ServiceException e) {
				LOGGER.error(e);
				ex = e;
				selectedJob.setSuccessful(false);
			}
			updateJobData(selectedJob, ex);
			
		} else if (POPULATE_IPAC_TO_DCR.equals(jobDataId)) {
			try {
				dcrIPACService.runIPACToDCRJob();
				selectedJob.setSuccessful(true);
			} catch (ServiceException e) {
				LOGGER.error(e);
				ex = e;
				selectedJob.setSuccessful(false);
			}
			updateJobData(selectedJob, ex);
			
		}
		return modelAndView;

	}

	private void updateJobData(JobDataBO selectedJob, Exception ex) {
		try {
			jobDataService.updateJobData(selectedJob, ex);
		} catch (ServiceException e1) {
			LOGGER.error(e1);
		}
	}

	protected Role[] allowedRoles() {
		return new Role[] { Role.ADMIN };
	}

	public void setJobDataService(JobDataService jobDataService) {
		this.jobDataService = jobDataService;
	}

	public void setDcrCreationService(DCRCreationService dcrCreationService) {
		this.dcrCreationService = dcrCreationService;
	}

	public void setCollectionDiscrepancyReportService(
			CollectionDiscrepancyReportService collectionDiscrepancyReportService) {
		this.collectionDiscrepancyReportService = collectionDiscrepancyReportService;
	}

	public void setConsolidatedDataService(
			ConsolidatedDataService consolidatedDataService) {
		this.consolidatedDataService = consolidatedDataService;
	}
	
	public void setDcrIPACService(DCRIPACService dcrIPACService) {
		this.dcrIPACService = dcrIPACService;
	}


}
