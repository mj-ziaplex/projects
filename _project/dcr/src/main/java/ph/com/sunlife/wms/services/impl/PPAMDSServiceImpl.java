package ph.com.sunlife.wms.services.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.apache.commons.lang.ObjectUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import ph.com.sunlife.wms.dao.DCRPPAMDSDao;
import ph.com.sunlife.wms.dao.DCRIPACDao; // TO be added DCR redesign
import ph.com.sunlife.wms.dao.domain.Company;
import ph.com.sunlife.wms.dao.domain.Currency;
import ph.com.sunlife.wms.dao.domain.DCRPPAMDS;
import ph.com.sunlife.wms.dao.domain.DCRPPAMDSNote;
import ph.com.sunlife.wms.dao.domain.PPAMDS;
import ph.com.sunlife.wms.dao.exceptions.WMSDaoException;
import ph.com.sunlife.wms.services.PPAMDSService;
import ph.com.sunlife.wms.services.bo.DCRPPAMDSBO;
import ph.com.sunlife.wms.services.bo.PPAMDSBO;
import ph.com.sunlife.wms.services.exception.ServiceException;

/**
 * The implementing class for {@link PPAMDSService}.
 *
 * @author Josephus Sardan
 * @author Edgardo Cunanan
 *
 */
public class PPAMDSServiceImpl implements PPAMDSService {

    private static final Logger LOGGER = Logger.getLogger(PPAMDSServiceImpl.class);

    private DCRPPAMDSDao dcrPPAMDSDao;
    
    private DCRIPACDao dcrIpacDao; // Added for DCR redesign

    public boolean isMdsReconciled(String customerCenter, Date from, Date to,
            List<PPAMDS> ipacMdsTransactions) throws ServiceException {

        Set<String> salesSlipNumbers = new HashSet<String>();
        try {
            if (CollectionUtils.isNotEmpty(ipacMdsTransactions)) {
                for (PPAMDS p : ipacMdsTransactions) {

                    String salesSlipNumber = p.getSalesSlipNumber();
                    if (StringUtils.isNotEmpty(salesSlipNumber)) {
                        salesSlipNumbers.add(salesSlipNumber);
                    }
                }
            }

            boolean isReconciled = true;

            if (CollectionUtils.isNotEmpty(salesSlipNumbers)) {
                List<DCRPPAMDS> mdsTransactions = dcrPPAMDSDao.getWmsMdsTransactions(new ArrayList<String>(salesSlipNumbers));

                DCRPPAMDS dcrPPAMds = null;
                boolean hasMatchedSSN = false;
                for (String thisSalesSlipNumber : salesSlipNumbers) {

                    for (DCRPPAMDS dcrPPAMdsItr : mdsTransactions) {
                        if (StringUtils.equalsIgnoreCase(
                                dcrPPAMdsItr.getSalesSlipNumber(),
                                thisSalesSlipNumber)) {
                            dcrPPAMds = dcrPPAMdsItr;
                            hasMatchedSSN = true;
                            break;
                        }
                    }

                    if (hasMatchedSSN) {
                        break;
                    }

                }

                if (dcrPPAMds == null) {
                    return false;
                } else {
                    if (!StringUtils.equalsIgnoreCase("Y",
                            dcrPPAMds.getReconciled())) {
                        return false;
                    }
                }
            }

            return isReconciled;
        } catch (WMSDaoException e) {
            LOGGER.error("Error in PPA MDS Service Impl: ", e);
            throw new ServiceException(e);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * ph.com.sunlife.wms.services.PPAMDSService#getMDSTransactions(java.lang
     * .String, java.util.Date, java.util.Date, java.lang.Long)
     */
    @Override
    public List<PPAMDSBO> getMDSTransactions(String customerCenter, Date from,
            Date to, Long dcrId) throws ServiceException {
        List<PPAMDSBO> list = new ArrayList<PPAMDSBO>();

        try {
            for (Company company : Company.values()) {
                for (Currency currency : Currency.values()) {
                    List<PPAMDS> ppaMDS = dcrIpacDao.getDCRIPACMDSTransaction(
                            company.name(), customerCenter, from, to, currency.getName()); // Added for DCR redesign

                    if (CollectionUtils.isNotEmpty(ppaMDS)) {
                        for (PPAMDS p : ppaMDS) {
                            PPAMDSBO ppaMDSBO = new PPAMDSBO();
                            this.convertMDSToBusinessObject(ppaMDSBO, p, currency);
                            DCRPPAMDSBO bo = new DCRPPAMDSBO();
                            bo.setDcrId(dcrId);
                            bo.setSalesSlipNumber(p.getSalesSlipNumber());
                            bo.setUserId(p.getUserId());
                            bo.setProductCode(p.getProdCode());

                            String isReconciledStr = "N";

                            DCRPPAMDS entity = new DCRPPAMDS();
                            convertBOToEntity(bo, entity);
                            entity = dcrPPAMDSDao.getIdByDcrIdAndSalesSlipNumber(entity);

                            if (isReconciled(entity)) {
                                isReconciledStr = "Y";
                            }

                            if (entity != null) {
                                String reconBy = entity.getReconBy();
                                String ppaNotes = entity.getPpaNotes();
                                String ppaFindings = entity.getPpaFindings();
                                Date reconDate = entity.getReconDate();

                                if (StringUtils.isNotEmpty(reconBy)) {
                                    ppaMDSBO.setReconBy(reconBy);
                                }

                                if (StringUtils.isNotEmpty(ppaNotes)) {
                                    ppaMDSBO.setPpaFindings(ppaFindings);
                                }

                                if (StringUtils.isNotEmpty(ppaFindings)) {
                                    ppaMDSBO.setPpaNotes(ppaNotes);
                                }

                                if (reconDate != null) {
                                    ppaMDSBO.setReconDate(reconDate);
                                }
                            }

                            ppaMDSBO.setIsReconciled(isReconciledStr);
                            list.add(ppaMDSBO);
                        }
                    }
                }
            }

            if (CollectionUtils.isNotEmpty(list)) {
                List<DCRPPAMDSNote> notes = dcrPPAMDSDao.getDCRPPAMDSNote(dcrId);

                if (CollectionUtils.isNotEmpty(notes)) {
                    for (DCRPPAMDSNote note : notes) {
                        final PPAMDSBO mockMds = new PPAMDSBO();
                        mockMds.setAccountNumber(note.getAccountNumber());
                        mockMds.setApprovalNumber(note.getApprovalNumber());
                        mockMds.setSalesSlipNumber(note.getSalesSlipNumber());

                        PPAMDSBO thisObj = (PPAMDSBO) CollectionUtils.find(list, new Predicate() {
                            @Override
                            public boolean evaluate(Object object) {
                                PPAMDSBO thisObj = (PPAMDSBO) object;
                                return ObjectUtils.equals(thisObj, mockMds);
                            }
                        });

                        thisObj.setPpaFindings(note.getPpaFindings());
                        thisObj.setPpaNotes(note.getPpaNotes());
                    }
                }
            }

            return list;
        } catch (Exception e) {
            LOGGER.error("Error in getting MDS transaction: ", e);
            throw new ServiceException(e);
        }
    }

    private void convertMDSToBusinessObject(PPAMDSBO ppaMDSBO, PPAMDS ppaMDS,
            Currency currency) {
        ppaMDSBO.setApplicationSerialNumber(ppaMDS.getApplicationSerialNumber());
        ppaMDSBO.setCardType(ppaMDS.getCardType());
        ppaMDSBO.setComCode(ppaMDS.getComCode());
        ppaMDSBO.setCompany(ppaMDS.getCompany());
        ppaMDSBO.setCurrency(currency.getName());
        ppaMDSBO.setCustomerCenter(ppaMDS.getCustomerCenter());
        ppaMDSBO.setDateRangeFrom(ppaMDS.getDateRangeFrom());
        ppaMDSBO.setDateRangeTo(ppaMDS.getDateRangeTo());
        ppaMDSBO.setDocumentId(ppaMDS.getDocumentId());
        ppaMDSBO.setPaySubDesc(ppaMDS.getPaySubDesc());
        ppaMDSBO.setPolicyPlanClientNumber(ppaMDS.getPolicyPlanClientNumber());
        ppaMDSBO.setProdCode(ppaMDS.getProdCode());
        ppaMDSBO.setProduct(ppaMDS.getProduct());
        ppaMDSBO.setPymtCurrency(ppaMDS.getPymtCurrency());
        ppaMDSBO.setSiteCode(ppaMDS.getSiteCode());
        ppaMDSBO.setTrxnDateProduct(ppaMDS.getTrxnDateProduct());
        ppaMDSBO.setUserId(ppaMDS.getUserId());
        ppaMDSBO.setAccountNumber(ppaMDS.getAccountNumber());
        ppaMDSBO.setApprovalDate(ppaMDS.getApprovalDate());
        ppaMDSBO.setApprovalNumber(ppaMDS.getApprovalNumber());
        ppaMDSBO.setCardAmount(ppaMDS.getCardAmount());
        ppaMDSBO.setSalesSlipNumber(ppaMDS.getSalesSlipNumber());
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * ph.com.sunlife.wms.services.PPAMDSService#isReconciled(ph.com.sunlife
     * .wms.services.bo.DCRPPAMDSBO)
     */
    public boolean isReconciled(DCRPPAMDS entity) throws ServiceException {
        Long id = null;
        // DCRPPAMDS entity = new DCRPPAMDS();
        // convertBOToEntity(bo, entity);
        // try {
        // entity = dcrPPAMDSDao.getIdByDcrIdAndSalesSlipNumber(entity);

        if (entity != null) {
            id = entity.getId();
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("JSARD dcrppamds id: " + id);
        }

        if (entity == null) {
            return false;
        }
        String reconciled = entity.getReconciled();

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("JSARD entity.getReconciled(): " + reconciled);
            LOGGER.debug("JSARD the return value " + "Y".equals(reconciled));
        }

        return "Y".equals(reconciled);

        // } catch (WMSDaoException e) {
        // LOGGER.error(e);
        // throw new ServiceException(e);
        // }
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * ph.com.sunlife.wms.services.PPAMDSService#reconcilePPAMDSList(java.util
     * .List)
     */
    @Override
    public void reconcilePPAMDSList(List<DCRPPAMDSBO> list, String reconciler)
            throws ServiceException {
        DCRPPAMDS entity = new DCRPPAMDS();
        for (DCRPPAMDSBO bo : list) {
            Long id = null;
            if (bo != null) {// because of lazylist
                convertBOToEntity(bo, entity);
                try {
                    entity = dcrPPAMDSDao
                            .getIdByDcrIdAndSalesSlipNumber(entity);

                    if (entity != null) {
                        id = entity.getId();
                    } else {
                        entity = new DCRPPAMDS();
                        convertBOToEntity(bo, entity);
                    }

                    if (id != null) {
                        // if
                        // (!StringUtils.equalsIgnoreCase(entity.getReconBy(),
                        // reconciler)) {
                        // entity.setReconBy(reconciler);
                        // }
                        if (StringUtils.equalsIgnoreCase("N",
                                entity.getReconciled())) {
                            dcrPPAMDSDao.reconcilePPAMDS(entity);// naka-check,
                            // uncheck,
                        }
                    } else {
                        entity.setReconBy(reconciler);
                        dcrPPAMDSDao.insertDCRPPAMDS(entity);// naka-uncheck,
                    }
                } catch (WMSDaoException e) {
                    LOGGER.error("Error in PPA MDS Service Impl: ", e);
                    throw new ServiceException(e);
                }
            }
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * ph.com.sunlife.wms.services.PPAMDSService#unreconcilePPAMDSList(java.
     * util.List)
     */
    @Override
    public void unreconcilePPAMDSList(List<DCRPPAMDSBO> list)
            throws ServiceException {
        Long id = null;
        DCRPPAMDS entity = new DCRPPAMDS();
        for (DCRPPAMDSBO bo : list) {
            if (bo != null) {// because of lazylist
                convertBOToEntity(bo, entity);
                try {
                    entity = dcrPPAMDSDao
                            .getIdByDcrIdAndSalesSlipNumber(entity);

                    if (entity != null) {
                        id = entity.getId();
                    }
                    if (id != null) {
                        dcrPPAMDSDao.unreconcilePPAMDS(id);// naka-check,
                        // uncheck
                    } else {
                        // TODO: translate this to English
                        LOGGER.debug("Dapat di pumasok dito, MDS to unreconcile not existing");// naka-uncheck,
                        // check,
                        // uncheck
                    }
                } catch (WMSDaoException e) {
                    LOGGER.error("Error in PPA MDS Service Impl: ", e);
                    throw new ServiceException(e);
                }
            }
        }
    }

    private void convertBOToEntity(DCRPPAMDSBO bo, DCRPPAMDS entity) {
        // entity.setId(bo.getId());
        entity.setDcrId(bo.getDcrId());
        // entity.setDcrId(dcrId);//pass as param in svc
        entity.setSalesSlipNumber(bo.getSalesSlipNumber());
        entity.setAccountNumber(bo.getAccountNumber());
        entity.setApprovalNumber(bo.getApprovalNumber());
        // entity.setReconciled(bo.getReconciled());
    }

    public void savePPAMDSNotes(Long dcrId, List<DCRPPAMDSBO> entries)
            throws ServiceException {
        try {
            if (CollectionUtils.isNotEmpty(entries)) {

                List<DCRPPAMDSNote> notes = dcrPPAMDSDao
                        .getDCRPPAMDSNote(dcrId);

                for (DCRPPAMDSBO entry : entries) {
                    if (entry != null) {
                        String ppaFindings = entry.getPpaFindings();
                        String ppaNotes = entry.getPpaNotes();

                        if (StringUtils.isNotEmpty(ppaFindings)
                                || StringUtils.isNotEmpty(ppaNotes)) {
                            final DCRPPAMDSNote mockNote = new DCRPPAMDSNote();
                            mockNote.setAccountNumber(entry.getAccountNumber());
                            mockNote.setApprovalNumber(entry
                                    .getApprovalNumber());
                            mockNote.setSalesSlipNumber(entry
                                    .getSalesSlipNumber());
                            mockNote.setPpaFindings(ppaFindings);
                            mockNote.setPpaNotes(ppaNotes);
                            mockNote.setDcrId(dcrId);

                            DCRPPAMDSNote thisObj = (DCRPPAMDSNote) CollectionUtils
                                    .find(notes, new Predicate() {

                                @Override
                                public boolean evaluate(Object object) {
                                    DCRPPAMDSNote thisObj = (DCRPPAMDSNote) object;
                                    return StringUtils.equals(
                                            thisObj.getAccountNumber(),
                                            mockNote.getAccountNumber())
                                            && StringUtils.equals(
                                            thisObj.getApprovalNumber(),
                                            mockNote.getApprovalNumber())
                                            && StringUtils.equals(
                                            thisObj.getSalesSlipNumber(),
                                            mockNote.getSalesSlipNumber());
                                }
                            });

                            if (thisObj == null) {
                                dcrPPAMDSDao.savePPAMDSNote(mockNote);
                            } else {
                                if (!(StringUtils.equals(
                                        thisObj.getPpaFindings(), ppaFindings) && StringUtils
                                        .equals(thisObj.getPpaNotes(), ppaNotes))) {
                                    dcrPPAMDSDao.updatePPAMDSNote(mockNote);
                                }
                            }
                        }
                    }
                }
            }
        } catch (WMSDaoException ex) {
            throw new ServiceException(ex);
        }

    }

    public void setDcrPPAMDSDao(DCRPPAMDSDao dcrPPAMDSDao) {
        this.dcrPPAMDSDao = dcrPPAMDSDao;
    }

    /**
     * Added for DCR redesign
     * @return the dcrIpacDao
     */
    public DCRIPACDao getDcrIpacDao() {
        return dcrIpacDao;
    }

    /**
     * Added for DCR redesign
     * @param dcrIpacDao the dcrIpacDao to set
     */
    public void setDcrIpacDao(DCRIPACDao dcrIpacDao) {
        this.dcrIpacDao = dcrIpacDao;
    }
}
