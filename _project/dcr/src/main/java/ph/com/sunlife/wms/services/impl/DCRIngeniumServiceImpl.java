/**
 * 
 */
package ph.com.sunlife.wms.services.impl;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import ph.com.sunlife.wms.dao.CashierDao;
import ph.com.sunlife.wms.dao.DCRCashierDao;
import ph.com.sunlife.wms.dao.DCRIngeniumDao;
import ph.com.sunlife.wms.dao.domain.Cashier;
import ph.com.sunlife.wms.dao.domain.Company;
import ph.com.sunlife.wms.dao.domain.Currency;
import ph.com.sunlife.wms.dao.domain.DCRCashier;
import ph.com.sunlife.wms.dao.domain.DCRIpacValue;
import ph.com.sunlife.wms.dao.domain.ProductType;
import ph.com.sunlife.wms.dao.domain.SessionTotal;
import ph.com.sunlife.wms.dao.exceptions.WMSDaoException;
import ph.com.sunlife.wms.integration.client.UserCollectionsTotalUtil;
import ph.com.sunlife.wms.services.DCRIngeniumService;
import ph.com.sunlife.wms.services.bo.ConsolidatedCashierProductTotals;
import ph.com.sunlife.wms.services.bo.ConsolidatedCashierTotals;
import ph.com.sunlife.wms.services.bo.ConsolidatedData;
import ph.com.sunlife.wms.services.exception.ServiceException;
import ph.com.sunlife.wms.util.WMSCollectionUtils;

import com.sunlife.ingeniumutil.container.UserCollectionsInfo;

/**
 * @author i176
 *
 */
public class DCRIngeniumServiceImpl implements DCRIngeniumService {
	
	private static final Logger LOGGER = Logger
			.getLogger(DCRIngeniumServiceImpl.class);
	
	private static final String INGENIUM_USERID_SUFFIX = "i";
	
	private DCRIngeniumDao dcrIngeniumDao;
	
	private DCRCashierDao dcrCashierDao;
	
	private UserCollectionsTotalUtil slocpiUserCollectionsTotalUtil;

	private UserCollectionsTotalUtil slgfiUserCollectionsTotalUtil;

	private CashierDao cashierDao;
	

	public DCRCashierDao getDcrCashierDao() {
		return dcrCashierDao;
	}

	public void setDcrCashierDao(DCRCashierDao dcrCashierDao) {
		this.dcrCashierDao = dcrCashierDao;
	}

	public UserCollectionsTotalUtil getSlocpiUserCollectionsTotalUtil() {
		return slocpiUserCollectionsTotalUtil;
	}

	public void setSlocpiUserCollectionsTotalUtil(
			UserCollectionsTotalUtil slocpiUserCollectionsTotalUtil) {
		this.slocpiUserCollectionsTotalUtil = slocpiUserCollectionsTotalUtil;
	}

	public UserCollectionsTotalUtil getSlgfiUserCollectionsTotalUtil() {
		return slgfiUserCollectionsTotalUtil;
	}

	public void setSlgfiUserCollectionsTotalUtil(
			UserCollectionsTotalUtil slgfiUserCollectionsTotalUtil) {
		this.slgfiUserCollectionsTotalUtil = slgfiUserCollectionsTotalUtil;
	}

	public CashierDao getCashierDao() {
		return cashierDao;
	}

	public void setCashierDao(CashierDao cashierDao) {
		this.cashierDao = cashierDao;
	}

	public DCRIngeniumDao getDcrIngeniumDao() {
		return dcrIngeniumDao;
	}

	public void setDcrIngeniumDao(DCRIngeniumDao dcrIngeniumDao) {
		this.dcrIngeniumDao = dcrIngeniumDao;
	}

	@Override
	public boolean populateDCRSessionTotal(Long dcrId, Date dcrDate,
			String ccId, List<DCRIpacValue> ipacValues) throws ServiceException {
		boolean success = true;
		try {

			List<DCRCashier> cashierItems = dcrCashierDao
					.getDCRCashierListByDcrId(dcrId);

			List<String> cashierUserIds = getCashierUserIds(cashierItems);

			ConsolidatedData consolidatedData = new ConsolidatedData();
			consolidatedData.setDcrId(dcrId);
			consolidatedData.setDcrDate(dcrDate);
			consolidatedData.setCcId(ccId);

			if (CollectionUtils.isNotEmpty(cashierUserIds)) {

				List<Cashier> cashierList = cashierDao
						.getMultipleCashierInfo(cashierUserIds);

				for (String userId : cashierUserIds) {
					LOGGER.info("cashierTotal for " + userId);

					ConsolidatedCashierTotals cashierTotal = new ConsolidatedCashierTotals();
					cashierTotal.setAcf2id(userId);

					// Cashier cashier = cashierDao.getCashier(userId);
					Cashier cashier = getCashierInfo(userId, cashierList);

					String cashierFullName = null;
					if (cashier != null) {
						String firstName = cashier.getFirstname();
						String lastName = cashier.getLastname();
						String middleName = cashier.getMiddlename();

						cashierFullName = lastName + ", " + firstName + " "
								+ middleName;
					} else {
						cashierFullName = userId + ", " + userId + " " + userId;
					}

					cashierTotal.setCashierFullName(cashierFullName);

					cashierTotal.setParent(consolidatedData);

					this.mapIpacValuesToCorrespondingCashier(cashierTotal,
							ipacValues);
				}

			}
		} catch (WMSDaoException ex) {
			LOGGER.error(ex);
			success = false;
			throw new ServiceException(ex);
		}
		return success;
	}

	@Override
	public SessionTotal retrieveDCRSessionTotal(UserCollectionsInfo info, String company) throws ServiceException {
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		List<SessionTotal> sessTotalList = null;

		try {
		SessionTotal sessTotal = new SessionTotal();
		sessTotal.setCompany(company);
		sessTotal.setCurrency(info.getCurrencyId());
		sessTotal.setProcessDate(df.parse(info.getProcessDate()));
		sessTotal.setUserId(info.getUserId());
		sessTotal.setCenterCode(info.getCustomerCenterId());
		sessTotalList = dcrIngeniumDao.getDCRIngeniumSessionTotal(sessTotal);
		} catch (WMSDaoException e) {
			LOGGER.error(e);
		} catch (ParseException e) {
			LOGGER.error(e);
		}
		return sessTotalList.size() > 0 ? sessTotalList.get(0) : null;
	}
	
	@Override
	public List<SessionTotal> retrieveDCRSessionTotal(UserCollectionsInfo info) throws ServiceException {
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		List<SessionTotal> sessTotalList = null;

		try {
		Date processDate = df.parse(info.getProcessDate());
		String centerCode = info.getCustomerCenterId();
		sessTotalList = dcrIngeniumDao.getDCRIngeniumSessionTotal(centerCode, processDate);
		} catch (WMSDaoException e) {
			LOGGER.error(e);
		} catch (ParseException e) {
			LOGGER.error(e);
		}
		return sessTotalList;
	}

	
	private List<String> getCashierUserIds(List<DCRCashier> cashierItems) {
		List<String> userIds = null;

		if (CollectionUtils.isNotEmpty(cashierItems)) {
			userIds = new ArrayList<String>();
			for (DCRCashier cashierItem : cashierItems) {
				String acf2id = cashierItem.getCashier().getAcf2id();
				userIds.add(acf2id);
			}
		}
		return userIds;
	}
	
	private Cashier getCashierInfo(String userId, List<Cashier> cashierList) {
		if (CollectionUtils.isNotEmpty(cashierList)) {
			for (Cashier cashierInfo : cashierList) {
				String acf2id = cashierInfo.getAcf2id();
				if (StringUtils.equalsIgnoreCase(userId, acf2id)) {
					return cashierInfo;
				}
			}
		}
		return null;
	}
	
	private void mapIpacValuesToCorrespondingCashier(
			ConsolidatedCashierTotals cashierTotal,
			List<DCRIpacValue> ipacValues) {

		final String acf2id = cashierTotal.getAcf2id();

		List<ConsolidatedCashierProductTotals> productTotals = new ArrayList<ConsolidatedCashierProductTotals>();
		for (ProductType type : ProductType.values()) {
			ConsolidatedCashierProductTotals productTotal = new ConsolidatedCashierProductTotals();
			productTotal.setProductType(type);
			productTotals.add(productTotal);
		}

		List<DCRIpacValue> valuesForCashier = WMSCollectionUtils
				.findMatchedElements(ipacValues, new Predicate() {

					@Override
					public boolean evaluate(Object object) {
						DCRIpacValue value = (DCRIpacValue) object;
						return StringUtils.equalsIgnoreCase(acf2id,
								value.getAcf2id());
					}
				});
		this.mapIpacValuesToProductTotals(productTotals, valuesForCashier);
		try {
			this.mapSessionTotalsToProductTotals(cashierTotal, acf2id,
					productTotals);
		} catch (ParseException e) {
			LOGGER.error(e);
		} catch (WMSDaoException e) {
			LOGGER.error(e);
		}
		cashierTotal.setProductTotals(productTotals);
	}

	private void mapSessionTotalsToProductTotals(
			ConsolidatedCashierTotals cashierTotal, final String userId,
			List<ConsolidatedCashierProductTotals> productTotals) throws ParseException, WMSDaoException {

		
		List<ConsolidatedCashierProductTotals> slocpiProductTotals = WMSCollectionUtils
				.findMatchedElements(productTotals, new Predicate() {

					@Override
					public boolean evaluate(Object object) {
						ConsolidatedCashierProductTotals pt = (ConsolidatedCashierProductTotals) object;
						return (ProductType.SLOCPI_D_TRADVUL.equals(pt
								.getProductType()) || ProductType.SLOCPI_P_TRADVUL
								.equals(pt.getProductType()));
					}
				});

		LOGGER.info("retrieving slocpiProductTotals from Ingenium Webservice");
		for (ConsolidatedCashierProductTotals slocpiProductTotal : slocpiProductTotals) {
			double sessionTotal = 0.0;

			ProductType productType = slocpiProductTotal.getProductType();
			String userIdWithSuffix = StringUtils.upperCase(userId
					+ INGENIUM_USERID_SUFFIX);

			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("Querying Session Total in Ingenium for SLOCPI for user "
						+ userIdWithSuffix);
			}

			double sessionTotalWithSuffix = queryForSlocpiSessionTotal(
					cashierTotal, userIdWithSuffix, productType.getCurrency());

			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("Querying Session Total in Ingenium for SLOCPI for user "
						+ StringUtils.upperCase(userId));
			}

			double sessionTotalWithoutSuffix = queryForSlocpiSessionTotal(
					cashierTotal, StringUtils.upperCase(userId),
					productType.getCurrency());

			if (sessionTotalWithSuffix > sessionTotalWithoutSuffix) {
				sessionTotal = sessionTotalWithSuffix;
			} else {
				sessionTotal = sessionTotalWithoutSuffix;
			}

			slocpiProductTotal.setSessionTotal(sessionTotal);
		}

		List<ConsolidatedCashierProductTotals> slgfiProductTotals = WMSCollectionUtils
				.findMatchedElements(productTotals, new Predicate() {

					@Override
					public boolean evaluate(Object object) {
						ConsolidatedCashierProductTotals pt = (ConsolidatedCashierProductTotals) object;
						return (ProductType.GREPA_D_TRADVUL.equals(pt
								.getProductType()) || ProductType.GREPA_P_TRADVUL
								.equals(pt.getProductType()));
					}
				});
		LOGGER.info("retrieving slgfiProductTotals from Ingenium Webservice");
		for (ConsolidatedCashierProductTotals slgfiProductTotal : slgfiProductTotals) {
			double sessionTotal = 0.0;

			ProductType productType = slgfiProductTotal.getProductType();
			String userIdWithSuffix = StringUtils.upperCase(userId
					+ INGENIUM_USERID_SUFFIX);

			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("Querying Session Total in Ingenium for SLGFI for user "
						+ userIdWithSuffix);
			}

			double sessionTotalWithSuffix = queryForSlgfiSessionTotal(
					cashierTotal, userIdWithSuffix, productType.getCurrency());

			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("Querying Session Total in Ingenium for SLGFI for user "
						+ StringUtils.upperCase(userId));
			}

			double sessionTotalWithoutSuffix = queryForSlgfiSessionTotal(
					cashierTotal, StringUtils.upperCase(userId),
					productType.getCurrency());

			if (sessionTotalWithSuffix > sessionTotalWithoutSuffix) {
				sessionTotal = sessionTotalWithSuffix;
			} else {
				sessionTotal = sessionTotalWithoutSuffix;
			}

			slgfiProductTotal.setSessionTotal(sessionTotal);
		}

	}

	private double queryForSlgfiSessionTotal(
			ConsolidatedCashierTotals cashierTotal, final String userId,
			Currency currency) throws WMSDaoException, ParseException {
		UserCollectionsInfo info = new UserCollectionsInfo();
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		String processDateStr = df
				.format(cashierTotal.getParent().getDcrDate());

		info.setProcessDate(processDateStr);
		info.setUserId(userId);

		if (Currency.PHP.equals(currency)) {
			info.setCurrencyId("PS");
		} else if (Currency.USD.equals(currency)) {
			info.setCurrencyId("US");
		}

		List<String> outMessages = new ArrayList<String>();
		
		//DateTime startDateTime = null;
		//startDateTime = DateTime.now();

		
		info = slgfiUserCollectionsTotalUtil.userCollectionsInquiry(info,
				outMessages);

		double sessionTotal = info.getTotalCollectionAmount();
		//LOGGER.info("queryForSlgfiSessionTotal:"+(DateTime.now().getMillis() - startDateTime.getMillis()));

		SessionTotal sessTotal = new SessionTotal();
		sessTotal.setCollectionTotal(sessionTotal);
		sessTotal.setCompany("SLGFI");
		sessTotal.setCurrency(info.getCurrencyId());
		sessTotal.setProcessDate(df.parse(info.getProcessDate()));
		sessTotal.setUserId(info.getUserId());
		sessTotal.setCenterCode(cashierTotal.getParent().getCcId());
		info.setCustomerCenterId(sessTotal.getCenterCode());
		try {
			if(null == retrieveDCRSessionTotal(info, sessTotal.getCompany())){
				if(dcrIngeniumDao.insertDCRIngeniumSessionTotal(sessTotal) > 0){
					LOGGER.info("Successfully inserted :"+ sessTotal.toString());
				}else{
					LOGGER.info("Failed to insert :"+ sessTotal.toString());
				}		
			}else{
				LOGGER.info("SessionTotal record already exists. "+sessTotal.toString());
			}
		} catch (ServiceException e) {
			LOGGER.error(e);
		}
		
		return sessionTotal;
	}

	private double queryForSlocpiSessionTotal(
			ConsolidatedCashierTotals cashierTotal, final String acf2id,
			Currency currency) throws ParseException, WMSDaoException {
		UserCollectionsInfo info = new UserCollectionsInfo();
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		String processDateStr = df
				.format(cashierTotal.getParent().getDcrDate());

		info.setProcessDate(processDateStr);
		info.setUserId(acf2id);

		if (Currency.PHP.equals(currency)) {
			info.setCurrencyId("PS");
		} else if (Currency.USD.equals(currency)) {
			info.setCurrencyId("US");
		}

		List<String> outMessages = new ArrayList<String>();
		
		//DateTime startDateTime = null;
		//startDateTime = DateTime.now();

		info = slocpiUserCollectionsTotalUtil.userCollectionsInquiry(info,
				outMessages);

		double sessionTotal = info.getTotalCollectionAmount();
		//LOGGER.info("queryForSlocpiSessionTotal:"+(DateTime.now().getMillis() - startDateTime.getMillis()));

		SessionTotal sessTotal = new SessionTotal();
		sessTotal.setCollectionTotal(sessionTotal);
		sessTotal.setCompany("SLOCP");
		sessTotal.setCurrency(info.getCurrencyId());
		sessTotal.setProcessDate(df.parse(info.getProcessDate()));
		sessTotal.setUserId(info.getUserId());
		sessTotal.setCenterCode(cashierTotal.getParent().getCcId());
		info.setCustomerCenterId(sessTotal.getCenterCode());
		try {
			if(null == retrieveDCRSessionTotal(info, sessTotal.getCompany())){
				if(dcrIngeniumDao.insertDCRIngeniumSessionTotal(sessTotal) > 0){
					LOGGER.info("Successfully inserted :"+ sessTotal.toString());
				}else{
					LOGGER.info("Failed to insert :"+ sessTotal.toString());
				}		
			}
		} catch (ServiceException e) {
			LOGGER.error(e);
		}
		
		return sessionTotal;
	}

	private void mapIpacValuesToProductTotals(
			List<ConsolidatedCashierProductTotals> productTotals,
			List<DCRIpacValue> valuesForCashier) {

		for (DCRIpacValue value : valuesForCashier) {
			String paySubCurr = value.getPaySubCurr();
			String prodCode = value.getProdCode();
			String payCode = value.getPayCode();
			// String paySubCode = value.getPaySubCode();
			double amount = value.getAmount();

			ProductType type = ProductType.prodCodeCurrencyToProductType(
					prodCode, paySubCurr);

			ConsolidatedCashierProductTotals mappedPT = null;
			if (type != null) {
				for (ConsolidatedCashierProductTotals pt : productTotals) {
					if (type.equals(pt.getProductType())) {
						mappedPT = pt;
					}
				}
			}

			if (mappedPT != null) {
				if ("NCSH".equalsIgnoreCase(payCode)) {
					double totalNonCashAmount = mappedPT.getTotalNonCash()
							+ amount;
					mappedPT.setTotalNonCash(totalNonCashAmount);

					if (ProductType.GREPA_D_TRADVUL.equals(type)
							|| ProductType.GREPA_P_TRADVUL.equals(type)
							|| ProductType.SLOCPI_D_TRADVUL.equals(type)
							|| ProductType.SLOCPI_P_TRADVUL.equals(type)) {
						mappedPT.setTotalNonCashFromDTR(totalNonCashAmount);
					}

				} else if ("CSH".equalsIgnoreCase(payCode)) {
					this.mapCashValueToTotals(mappedPT, value);
				} else if ("CHK".equalsIgnoreCase(payCode)) {
					mapCheckValueToTotals(mappedPT, value);
				} else if ("CRD".equalsIgnoreCase(payCode)) {
					this.mapCardValueToTotals(mappedPT, value);
				}
			}
		}
	}
	
	private void mapCheckValueToTotals(
			ConsolidatedCashierProductTotals mappedPT, DCRIpacValue value) {
		String paySubCode = value.getPaySubCode();
		double amount = value.getAmount();

		if ("R/C".equalsIgnoreCase(paySubCode)) {
			mappedPT.setTotalCheckRegional(mappedPT.getTotalCheckRegional()
					+ amount);
		} else if ("L/C".equalsIgnoreCase(paySubCode)) {
			mappedPT.setTotalCheckLocal(mappedPT.getTotalCheckLocal() + amount);
		} else if ("O/C".equalsIgnoreCase(paySubCode)) {
			mappedPT.setTotalCheckOT(mappedPT.getTotalCheckOT() + amount);
		} else if ("PMO".equalsIgnoreCase(paySubCode)) {
			ProductType productType = mappedPT.getProductType();
			if (productType != null
					&& (Company.SLAMCID.equals(productType.getCompany()) || Company.SLAMCIP
							.equals(productType.getCompany()))) {
				mappedPT.setTotalCheckNonCounter(mappedPT
						.getTotalCheckNonCounter() + amount);
			} else {
				mappedPT.setTotalPmo(mappedPT.getTotalPmo() + amount);
			}
		} else if ("OUC".equalsIgnoreCase(paySubCode)) {
			mappedPT.setTotalCheckOnUs(mappedPT.getTotalCheckOnUs() + amount);
		} else if ("CDM".equalsIgnoreCase(paySubCode)) {
			mappedPT.setTotalUsCheckInManila(mappedPT.getTotalUsCheckInManila()
					+ amount);
		} else if ("COP".equalsIgnoreCase(paySubCode)) {
			mappedPT.setTotalUsCheckOutPh(mappedPT.getTotalUsCheckOutPh()
					+ amount);
		} else if ("DCK".equalsIgnoreCase(paySubCode)) {
			mappedPT.setTotalDollarCheque(mappedPT.getTotalDollarCheque()
					+ amount);
		} else if ("BCP".equalsIgnoreCase(paySubCode)
				|| "BCU".equalsIgnoreCase(paySubCode)) {
			mappedPT.setTotalCheckNonCounter(mappedPT.getTotalCheckNonCounter()
					+ amount);
		} else if ("CGD".equalsIgnoreCase(paySubCode)
				|| "CGL".equalsIgnoreCase(paySubCode)
				|| "GYR".equalsIgnoreCase(paySubCode)) {
			mappedPT.setPaymentFromInsurer(mappedPT.getPaymentFromInsurer()
					+ amount);
		} else {
			mappedPT.setTotalCheckNonCounter(mappedPT.getTotalCheckNonCounter()
					+ amount);
		}
	}

	private void mapCashValueToTotals(
			ConsolidatedCashierProductTotals mappedPT, DCRIpacValue value) {
		String paySubCode = value.getPaySubCode();
		double amount = value.getAmount();

		if ("CPH".equalsIgnoreCase(paySubCode)
				|| "UCH".equalsIgnoreCase(paySubCode)) {
			mappedPT.setTotalCashCounter(mappedPT.getTotalCashCounter()
					+ amount);
		} else if ("EPS".equalsIgnoreCase(paySubCode)) {
			mappedPT.setTotalCashEpsBpi(mappedPT.getTotalCashEpsBpi() + amount);
		} else {
			mappedPT.setTotalCashNonCounter(mappedPT.getTotalCashNonCounter()
					+ amount);
		}
	}

	private void mapCardValueToTotals(
			ConsolidatedCashierProductTotals mappedPT, DCRIpacValue value) {
		String paySubCode = value.getPaySubCode();
		double amount = value.getAmount();

		if ("BPP".equalsIgnoreCase(paySubCode)
				|| "BPU".equalsIgnoreCase(paySubCode)) {
			mappedPT.setTotalPosBpi(mappedPT.getTotalPosBpi() + amount);
		} else if ("CPP".equalsIgnoreCase(paySubCode)
				|| "CPU".equalsIgnoreCase(paySubCode)) {
			mappedPT.setTotalPosCtb(mappedPT.getTotalPosCtb() + amount);
		} else if ("EPP".equalsIgnoreCase(paySubCode)
				|| "EPU".equalsIgnoreCase(paySubCode)) {
			mappedPT.setTotalPosBdo(mappedPT.getTotalPosBdo() + amount);
		} else if ("HPP".equalsIgnoreCase(paySubCode)
				|| "HPU".equalsIgnoreCase(paySubCode)) {
			mappedPT.setTotalPosHsbc(mappedPT.getTotalPosHsbc() + amount);
		} else if ("SPP".equalsIgnoreCase(paySubCode)
				|| "SPU".equalsIgnoreCase(paySubCode)) {
			mappedPT.setTotalPosScb(mappedPT.getTotalPosScb() + amount);
		} else if ("RPP".equalsIgnoreCase(paySubCode)
				|| "RPU".equalsIgnoreCase(paySubCode)) {
			mappedPT.setTotalPosRcbc(mappedPT.getTotalPosRcbc() + amount);
		} else if ("BMP".equalsIgnoreCase(paySubCode)
				|| "BMU".equalsIgnoreCase(paySubCode)) {
			mappedPT.setTotalMdsBpi(mappedPT.getTotalMdsBpi() + amount);
		} else if ("CMP".equalsIgnoreCase(paySubCode)
				|| "CMU".equalsIgnoreCase(paySubCode)) {
			mappedPT.setTotalMdsCtb(mappedPT.getTotalMdsCtb() + amount);
		} else if ("EMP".equalsIgnoreCase(paySubCode)
				|| "EMU".equalsIgnoreCase(paySubCode)) {
			mappedPT.setTotalMdsBdo(mappedPT.getTotalMdsBdo() + amount);
		} else if ("HMP".equalsIgnoreCase(paySubCode)
				|| "HMU".equalsIgnoreCase(paySubCode)) {
			mappedPT.setTotalMdsHsbc(mappedPT.getTotalMdsHsbc() + amount);
		} else if ("SMP".equalsIgnoreCase(paySubCode)
				|| "SMU".equalsIgnoreCase(paySubCode)) {
			mappedPT.setTotalMdsSbc(mappedPT.getTotalMdsSbc() + amount);
		} else if ("RMP".equalsIgnoreCase(paySubCode)
				|| "RMU".equalsIgnoreCase(paySubCode)) {
			mappedPT.setTotalMdsRcbc(mappedPT.getTotalMdsRcbc() + amount);
		} else if ("CMM".equalsIgnoreCase(paySubCode)) {
			mappedPT.setTotalAutoCtb(mappedPT.getTotalAutoCtb() + amount);
		} else if ("SMM".equalsIgnoreCase(paySubCode)) {
			mappedPT.setTotalAutoScb(mappedPT.getTotalAutoScb() + amount);
		} else if ("RAU".equalsIgnoreCase(paySubCode)
				|| "RMM".equalsIgnoreCase(paySubCode)) {
			mappedPT.setTotalAutoRcbc(mappedPT.getTotalAutoRcbc() + amount);
		} else if ("ICT".equalsIgnoreCase(paySubCode)) {
			mappedPT.setTotalSunlinkOnline(mappedPT.getTotalSunlinkOnline()
					+ amount);
		} else if ("HSP".equalsIgnoreCase(paySubCode)) {
			mappedPT.setTotalSunlinkHsbcPeso(mappedPT.getTotalSunlinkHsbcPeso()
					+ amount);
		} else if ("HSU".equalsIgnoreCase(paySubCode)) {
			mappedPT.setTotalSunlinkHsbcDollar(mappedPT
					.getTotalSunlinkHsbcDollar() + amount);
		} else if ("EPS".equalsIgnoreCase(paySubCode)) {
			mappedPT.setTotalEpsBpi(mappedPT.getTotalEpsBpi() + amount);
		}

	}

	@Override
	public void removeDCRIngeniumSessionTotal(String ccId, Date processDate)
			throws ServiceException {
		try {
			dcrIngeniumDao.deleteDCRIngeniumSessionTotal(ccId, processDate);
		} catch (WMSDaoException e) {
			LOGGER.error(e);
			throw new ServiceException(e);
		}
	}
}
