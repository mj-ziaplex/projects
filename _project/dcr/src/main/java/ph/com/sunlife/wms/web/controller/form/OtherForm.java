package ph.com.sunlife.wms.web.controller.form;

import org.apache.commons.lang.ArrayUtils;

public class OtherForm extends CashierWorkItemForm {
	
	private Double ptotalCashCounter;
	private Double ptotalCashNonCounter;
	private Double ptotalChequeLocal;
	private Double ptotalCheckOnUs;
	private Double ptotalCheckRegional;
	private Double ptotalCheckNonCounter;
	private Double dtotalCashCounter;
	private Double dtotalCashNonCounter;
	private Double dtotalCheckOnUs;
	private Double dtotalCheckRegional;
	private Double dtotalCheckNonCounter;
	private Double dtotalDollarCheque;
	private Double dtotalUsCheckOutPh;
	private Double dtotalUsCheckInManila;
	private boolean confirm;
	
	private String fieldList[] = {"ptotalCashCounter","ptotalCashNonCounter","ptotalChequeLocal","ptotalCheckOnUs","ptotalCheckRegional","ptotalCheckNonCounter",
	                             "dtotalCashCounter","dtotalCashNonCounter","dtotalCheckOnUs","dtotalCheckRegional","dtotalCheckNonCounter",
	                             "dtotalUsCheckInManila","dtotalUsCheckOutPh","dtotalDollarCheque"};	
	
	@Override
	public String[] getRequiredFields() {
		String fields[] = super.getRequiredFields();
		for(int a=0; a < fieldList.length; a++){
			ArrayUtils.add(fields,fieldList[a]);
		}
		return fields;
	}	
	
	
	public boolean isConfirm() {
		return confirm;
	}



	public void setConfirm(boolean confirm) {
		this.confirm = confirm;
	}



	public Double getDtotalDollarCheque() {
		return dtotalDollarCheque;
	}



	public void setDtotalDollarCheque(Double dtotalDollarCheque) {
		this.dtotalDollarCheque = dtotalDollarCheque;
	}



	public Double getDtotalUsCheckOutPh() {
		return dtotalUsCheckOutPh;
	}



	public void setDtotalUsCheckOutPh(Double dtotalUsCheckOutPh) {
		this.dtotalUsCheckOutPh = dtotalUsCheckOutPh;
	}



	public Double getDtotalUsCheckInManila() {
		return dtotalUsCheckInManila;
	}



	public void setDtotalUsCheckInManila(Double dtotalUsCheckInManila) {
		this.dtotalUsCheckInManila = dtotalUsCheckInManila;
	}



	public Double getPtotalCashCounter() {
		return ptotalCashCounter;
	}



	public void setPtotalCashCounter(Double ptotalCashCounter) {
		this.ptotalCashCounter = ptotalCashCounter;
	}



	public Double getPtotalCashNonCounter() {
		return ptotalCashNonCounter;
	}



	public void setPtotalCashNonCounter(Double ptotalCashNonCounter) {
		this.ptotalCashNonCounter = ptotalCashNonCounter;
	}



	public Double getPtotalCheckOnUs() {
		return ptotalCheckOnUs;
	}



	public void setPtotalCheckOnUs(Double ptotalCheckOnUs) {
		this.ptotalCheckOnUs = ptotalCheckOnUs;
	}



	public Double getPtotalCheckRegional() {
		return ptotalCheckRegional;
	}



	public void setPtotalCheckRegional(Double ptotalCheckRegional) {
		this.ptotalCheckRegional = ptotalCheckRegional;
	}



	public Double getPtotalCheckNonCounter() {
		return ptotalCheckNonCounter;
	}



	public void setPtotalCheckNonCounter(Double ptotalCheckNonCounter) {
		this.ptotalCheckNonCounter = ptotalCheckNonCounter;
	}



	public Double getDtotalCashCounter() {
		return dtotalCashCounter;
	}



	public void setDtotalCashCounter(Double dtotalCashCounter) {
		this.dtotalCashCounter = dtotalCashCounter;
	}



	public Double getDtotalCashNonCounter() {
		return dtotalCashNonCounter;
	}



	public void setDtotalCashNonCounter(Double dtotalCashNonCounter) {
		this.dtotalCashNonCounter = dtotalCashNonCounter;
	}



	public Double getDtotalCheckOnUs() {
		return dtotalCheckOnUs;
	}



	public void setDtotalCheckOnUs(Double dtotalCheckOnUs) {
		this.dtotalCheckOnUs = dtotalCheckOnUs;
	}



	public Double getDtotalCheckRegional() {
		return dtotalCheckRegional;
	}



	public void setDtotalCheckRegional(Double dtotalCheckRegional) {
		this.dtotalCheckRegional = dtotalCheckRegional;
	}



	public Double getDtotalCheckNonCounter() {
		return dtotalCheckNonCounter;
	}



	public void setDtotalCheckNonCounter(Double dtotalCheckNonCounter) {
		this.dtotalCheckNonCounter = dtotalCheckNonCounter;
	}


	public Double getPtotalChequeLocal() {
		return ptotalChequeLocal;
	}


	public void setPtotalChequeLocal(Double ptotalChequeLocal) {
		this.ptotalChequeLocal = ptotalChequeLocal;
	}
	

}
