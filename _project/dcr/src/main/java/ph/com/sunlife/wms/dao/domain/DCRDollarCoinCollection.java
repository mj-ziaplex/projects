package ph.com.sunlife.wms.dao.domain;

import java.util.Date;

public class DCRDollarCoinCollection extends Entity {

	private DCRVDIL dcrvdil = new DCRVDIL();
	
	private Date dateCreated;
	
	private int denom1;
	
	private int denom50c;
	
	private int denom25c;
	
	private int denom10c;
	
	private int denom5c;
	
	private int denom1c;

	public DCRVDIL getDcrvdil() {
		return dcrvdil;
	}

	public void setDcrvdil(DCRVDIL dcrvdil) {
		this.dcrvdil = dcrvdil;
	}
	
	public void setDcrvdil(Long dcrvdilId) {
		DCRVDIL dcrvdil = new DCRVDIL();
		dcrvdil.setId(dcrvdilId);
		this.setDcrvdil(dcrvdil);
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public int getDenom1() {
		return denom1;
	}

	public void setDenom1(int denom1) {
		this.denom1 = denom1;
	}

	public int getDenom50c() {
		return denom50c;
	}

	public void setDenom50c(int denom50c) {
		this.denom50c = denom50c;
	}

	public int getDenom25c() {
		return denom25c;
	}

	public void setDenom25c(int denom25c) {
		this.denom25c = denom25c;
	}

	public int getDenom10c() {
		return denom10c;
	}

	public void setDenom10c(int denom10c) {
		this.denom10c = denom10c;
	}

	public int getDenom5c() {
		return denom5c;
	}

	public void setDenom5c(int denom5c) {
		this.denom5c = denom5c;
	}

	public int getDenom1c() {
		return denom1c;
	}

	public void setDenom1c(int denom1c) {
		this.denom1c = denom1c;
	}
	
	
}
