package ph.com.sunlife.wms.util;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 * Utility class responsible for created snapshot-friendly html out of a regular
 * html code. This removes all href links, and disables all buttons and form
 * submissions.
 * 
 * @author Zainal Limpao
 * 
 */
public class HtmlSnapShotUtil {

	/**
	 * Get {@link HtmlLink} from the htmlCode.
	 * 
	 * @param htmlCode
	 * @return
	 */
	public static List<HtmlLink> grabHtmlLinks(final String htmlCode) {
		if (StringUtils.isEmpty(htmlCode)) {
			return null;
		}

		List<HtmlLink> result = new ArrayList<HtmlLink>();

		Document document = Jsoup.parse(htmlCode, "UTF-8");
		Elements links = document.getElementsByTag("a");
		for (Element link : links) {

			String linkHref = link.attr("href");
			String linkText = link.text();

			HtmlLink htmlLink = new HtmlLink();
			htmlLink.setLink(linkHref);
			htmlLink.setLinkText(linkText);

			result.add(htmlLink);
		}

		return result;
	}

	/**
	 * Converts Html Code to a Snapshot-friendly Html.
	 * 
	 * @param htmlCode
	 * @return
	 */
	public static String toSnapshotHtml(String htmlCode) {
		Document document = Jsoup.parse(htmlCode, "UTF-8");

		replaceLinksWithDeadLinks(document);
		disableButtons(document);
		disableInputFields(document);
		removeButtonNodes(document);

		return document.html();
	}

	private static void disableInputFields(Document document) {
		Elements inputElements = document.getElementsByTag("input");
		for (Element element : inputElements) {
			String value = element.attr("type");
			if (StringUtils.equalsIgnoreCase("text", value)) {
				element.attr("disabled", "disabled");
			}
		}
	}

	private static void removeButtonNodes(Document document) {
		Element confirmButtonDiv = document.getElementById("confirmButtonDiv");
		if (confirmButtonDiv != null) {
			confirmButtonDiv.remove();
		}

		Element addCommentNode = document.getElementById("addCommentNode");
		if (addCommentNode != null) {
			addCommentNode.remove();
		}

		Element navigationNode = document.getElementById("navigationNode");
		if (navigationNode != null) {
			navigationNode.remove();
		}
	}

	private static void replaceLinksWithDeadLinks(Document document) {
		Elements links = document.getElementsByTag("a");
		for (Element link : links) {
			link.attr("href", "#");
		}
	}

	private static void disableButtons(Document document) {
		Elements inputElements = document.getElementsByTag("input");
		for (Element element : inputElements) {
			String value = element.attr("type");
			if (StringUtils.equalsIgnoreCase("button", value)
					|| StringUtils.equalsIgnoreCase("submit", value)) {
				element.attr("disabled", "disabled");
			}
		}

		Elements buttonElements = document.getElementsByTag("button");
		for (Element element : buttonElements) {
			element.attr("disabled", "disabled");
		}

	}

}
