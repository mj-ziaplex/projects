package ph.com.sunlife.wms.services.impl;

import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;

import ph.com.sunlife.wms.dao.CenterCodeDao;
import ph.com.sunlife.wms.dao.DCRCachedPropertyDao;
import ph.com.sunlife.wms.dao.UserGroupsDao;
import ph.com.sunlife.wms.dao.domain.CenterCode;
import ph.com.sunlife.wms.dao.domain.HubCC;
import ph.com.sunlife.wms.dao.domain.HubCCNBO;
import ph.com.sunlife.wms.dao.domain.HubCCNBOUserGroup;
import ph.com.sunlife.wms.dao.exceptions.WMSDaoException;
import ph.com.sunlife.wms.services.CenterCodeService;
import ph.com.sunlife.wms.services.bo.AdminCenterCodeBO;
import ph.com.sunlife.wms.services.exception.ServiceException;
import ph.com.sunlife.wms.util.WMSStringUtil;

public class CenterCodeServiceImpl implements CenterCodeService {
	
	private static final Logger LOGGER = Logger
			.getLogger(CenterCodeServiceImpl.class);
	
	private CenterCodeDao centerCodeDao;
	
	private UserGroupsDao userGroupsDao;
	
	private DCRCachedPropertyDao dcrCachedPropertyDao;
	
	public void setDcrCachedPropertyDao(DCRCachedPropertyDao dcrCachedPropertyDao) {
		this.dcrCachedPropertyDao = dcrCachedPropertyDao;
	}

	public void setUserGroupsDao(UserGroupsDao userGroupsDao) {
		this.userGroupsDao = userGroupsDao;
	}

	public void setCenterCodeDao(CenterCodeDao centerCodeDao) {
		this.centerCodeDao = centerCodeDao;
	}

	@Override
	public boolean createCenterCode(CenterCode cc) throws ServiceException {
		boolean isCreated = false;
		try {
			isCreated = centerCodeDao.createCenterCode(cc);
		} catch (WMSDaoException e) {
			LOGGER.error(e);
		}
		return isCreated;
	}

	@Override
	public List<CenterCode> getAllCenterCode() throws ServiceException {
		List<CenterCode> ccList = null;
		try {
			ccList = centerCodeDao.getAllCenterCode();
		} catch (WMSDaoException e) {
			LOGGER.error(e);
		}
		return ccList;
	}

	@Override
	public List<CenterCode> getCenterCodeById(String ccId)
			throws ServiceException {
		List<CenterCode> ccList = null;
		try {
			ccList = centerCodeDao.getCenterCodeById(ccId);
		} catch (WMSDaoException e) {
			LOGGER.error(e);
		}
		return ccList;
	}

	@Override
	public boolean updateCenterCode(CenterCode cc) throws ServiceException {
		boolean isUpdated = false;
		try {
			isUpdated = centerCodeDao.updateCenterCode(cc);
		} catch (WMSDaoException e) {
			LOGGER.error(e);
		}
		return isUpdated;
	}

	@Override
	public boolean deleteCenterCode(String ccId) throws ServiceException {
		boolean isDeleted = false;
		try {
			isDeleted = centerCodeDao.deleteCenterCode(ccId);
		} catch (WMSDaoException e) {
			LOGGER.error(e);
		}
		return isDeleted;
	}
	
	@Override
	public boolean createHubCCNBO(HubCCNBO nbo) throws ServiceException {
		boolean isCreated = false;
		try {
			isCreated = userGroupsDao.createHubCCNBO(nbo);
		} catch (WMSDaoException e) {
			LOGGER.error(e);
		}
		return isCreated;
	}

	@Override
	public boolean createHubCC(HubCC cc) throws ServiceException {
		boolean isCreated = false;
		try {
			isCreated = userGroupsDao.createHubCC(cc);
		} catch (WMSDaoException e) {
			LOGGER.error(e);
		}
		return isCreated;
	}

	
	@Override
	public boolean updateHubCCNBO(HubCCNBO nbo) throws ServiceException {
		boolean isUpdated = false;
		try {
			isUpdated = userGroupsDao.updateHubCCNBO(nbo);
		} catch (WMSDaoException e) {
			LOGGER.error(e);
		}
		return isUpdated;
	}

	@Override
	public boolean updateHubCC(HubCC cc) throws ServiceException {
		boolean isUpdated = false;
		try {
			isUpdated = userGroupsDao.updateHubCC(cc);
		} catch (WMSDaoException e) {
			LOGGER.error(e);
		}
		return isUpdated;
	}
	
	@Override
	public boolean deleteHubCCNBO(HubCCNBO nbo) throws ServiceException {
		boolean isDeleted = false;
		try {
			isDeleted = userGroupsDao.deleteHubCCNBO(nbo);
		} catch (WMSDaoException e) {
			LOGGER.error(e);
		}
		return isDeleted;
	}

	@Override
	public boolean deleteHubCC(HubCC cc) throws ServiceException {
		boolean isDeleted = false;
		try {
			isDeleted = userGroupsDao.deleteHubCC(cc);
		} catch (WMSDaoException e) {
			LOGGER.error(e);
		}
		return isDeleted;
	}

	@Override
	public void doCenterCodeCreate(AdminCenterCodeBO adminCenterCodeBO,
			String sessUser, String ccIdCachedValue) throws ServiceException {
		if(null != adminCenterCodeBO){
			CenterCode cc = new CenterCode();
			String ccId = adminCenterCodeBO.getCcId().replaceAll(",", "").trim();
			String nboStr = WMSStringUtil.replaceEscChar(adminCenterCodeBO.getUserNBO());
			String hubStr = WMSStringUtil.replaceEscChar(adminCenterCodeBO.getUserHub());
			cc.setCcId(ccId);
			cc.setCcName(adminCenterCodeBO.getCcName());
			cc.setCcAbbrevName(adminCenterCodeBO.getCcAbbrevName());
			cc.setCcDesc(adminCenterCodeBO.getCcDesc());
			cc.setCcActive("Y".equalsIgnoreCase(adminCenterCodeBO.getCcActive()));
			cc.setCcAutoIdx("Y".equalsIgnoreCase(adminCenterCodeBO.getCcAutoIdx()));
			cc.setCcCanEncode("Y".equalsIgnoreCase(adminCenterCodeBO.getCcCanEncode()));
			cc.setCcIsIso("Y".equalsIgnoreCase(adminCenterCodeBO.getCcIsIso()));
			cc.setCcViewImage("Y".equalsIgnoreCase(adminCenterCodeBO.getCcViewImage()));
			cc.setCcZoomZone("Y".equalsIgnoreCase(adminCenterCodeBO.getCcZoomZone()));
			cc.setCcCreUser(sessUser);
			cc.setCcCreDate(new Date());
			if(createCenterCode(cc)){
				HubCC hcc = new HubCC();
				hcc.setHccActive(cc.getCcActive() ? "1" : "0");
				hcc.setHccCCID(ccId);
				hcc.setHccHUBID(hubStr);
				hcc.setHccCREUser(sessUser);
				hcc.setHccCREDate(new Date());
				if(createHubCC(hcc)){
					HubCCNBO nbo = new HubCCNBO();
					nbo.setHccnActive(hcc.getHccActive());
					nbo.setHccnCCID(ccId);
					nbo.setHccnHUBID(hubStr);
					nbo.setHccnNBOID(nboStr);
					nbo.setHccnCREUser(sessUser);
					nbo.setHccnCREDate(new Date());
					if(createHubCCNBO(nbo)){
						for(String group : adminCenterCodeBO.getUserGroups()){
							HubCCNBOUserGroup hub = new HubCCNBOUserGroup();
							hub.setHccnugActive(hcc.getHccActive());
							hub.setHccnugCCID(ccId);
							hub.setHccnugGRPID(group);
							hub.setHccnugHUBID(hubStr);
							hub.setHccnugNBOID(nboStr);
							hub.setHccnugReceiveWorkGF("1");
							hub.setHccnugReceiveWork("1");
							hub.setHccnugWMSUID(adminCenterCodeBO.getUserId());
							hub.setHccnugCREUser(sessUser);
							hub.setHccnugCREDate(new Date());
							try {
								userGroupsDao.createUserGroupHub(hub);
							} catch (WMSDaoException e) {
								LOGGER.error(e);
							}
						}
					}
				}
				try {
					if(!ccIdCachedValue.contains(ccId)){
					dcrCachedPropertyDao.updateProperty("dcr.rollout.ccid", ccIdCachedValue+=","+ccId, sessUser, new Date().getTime());
					}
				} catch (WMSDaoException e) {
					LOGGER.error(e);
				}
			}
		}		
	}

	@Override
	public void doCenterCodeUpdate(AdminCenterCodeBO adminCenterCodeBO,
			String sessUser, String ccIdCachedValue) throws ServiceException {
		if(null != adminCenterCodeBO){
			String ccId = adminCenterCodeBO.getcId().replaceAll(",", "").trim();
			CenterCode ccEntity = getCenterCodeById(WMSStringUtil.replaceEscChar(ccId)).get(0);
			CenterCode cc = new CenterCode();
			String nboStr = WMSStringUtil.replaceEscChar(adminCenterCodeBO.getUserNBO());
			String hubStr = WMSStringUtil.replaceEscChar(adminCenterCodeBO.getUserHub());
			
			cc.setCcId(ccId);
			cc.setCcName(adminCenterCodeBO.getCcName());
			cc.setCcAbbrevName(adminCenterCodeBO.getCcAbbrevName());
			cc.setCcDesc(adminCenterCodeBO.getCcDesc());
			cc.setCcActive("Y".equalsIgnoreCase(adminCenterCodeBO.getCcActive()));
			cc.setCcAutoIdx("Y".equalsIgnoreCase(adminCenterCodeBO.getCcAutoIdx()));
			cc.setCcCanEncode("Y".equalsIgnoreCase(adminCenterCodeBO.getCcCanEncode()));
			cc.setCcIsIso("Y".equalsIgnoreCase(adminCenterCodeBO.getCcIsIso()));
			cc.setCcViewImage("Y".equalsIgnoreCase(adminCenterCodeBO.getCcViewImage()));
			cc.setCcZoomZone("Y".equalsIgnoreCase(adminCenterCodeBO.getCcZoomZone()));
			cc.setCcCreUser(ccEntity.getCcCreUser());
			cc.setCcCreDate(ccEntity.getCcCreDate());
			cc.setCcUpdUser(sessUser);
			cc.setCcUpdDate(new Date());
			if(updateCenterCode(cc)){
				HubCC hcc = new HubCC();
				hcc.setHccActive(cc.getCcActive() ? "1" : "0");
				hcc.setHccCCID(ccId);
				hcc.setHccHUBID(hubStr);
				hcc.setHccCREUser(ccEntity.getCcCreUser());
				hcc.setHccCREDate(ccEntity.getCcCreDate());
				hcc.setHccUPDUser(sessUser);
				hcc.setHccUPDDate(new Date());
				if(updateHubCC(hcc)){
					HubCCNBO nbo = new HubCCNBO();
					nbo.setHccnActive(hcc.getHccActive());
					nbo.setHccnCCID(ccId);
					nbo.setHccnHUBID(hubStr);
					nbo.setHccnNBOID(nboStr);
					nbo.setHccnCREUser(ccEntity.getCcCreUser());
					nbo.setHccnCREDate(ccEntity.getCcCreDate());					
					nbo.setHccnUPDUser(sessUser);
					nbo.setHccnUPDDate(new Date());
					if(updateHubCCNBO(nbo)){
						for(String group : adminCenterCodeBO.getUserGroups()){
							HubCCNBOUserGroup hub = new HubCCNBOUserGroup();
							hub.setHccnugActive(hcc.getHccActive());
							hub.setHccnugCCID(ccId);
							hub.setHccnugGRPID(group);
							hub.setHccnugHUBID(hubStr);
							hub.setHccnugNBOID(nboStr);
							hub.setHccnugReceiveWorkGF("1");
							hub.setHccnugReceiveWork("1");
							hub.setHccnugWMSUID(adminCenterCodeBO.getUserId());
							hub.setHccnugCREUser(sessUser);
							hub.setHccnugCREDate(new Date());
							try {
								if(userGroupsDao.deleteUserGroupHub(hub)){
									userGroupsDao.createUserGroupHub(hub);
								}
							} catch (WMSDaoException e) {
								LOGGER.error(e);
							}
						}
					}
				}
				if(!ccIdCachedValue.contains(ccId)){
					try {
						dcrCachedPropertyDao.updateProperty("dcr.rollout.ccid", ccIdCachedValue+=","+ccId, sessUser, new Date().getTime());
					} catch (WMSDaoException e) {
						LOGGER.error(e);
					}
				}
			}
		}		
	}
	
	
}
