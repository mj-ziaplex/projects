package ph.com.sunlife.wms.dao;

import java.util.List;

import ph.com.sunlife.wms.dao.domain.BalancingToolSnapshot;
import ph.com.sunlife.wms.dao.domain.CollectionOnBehalfData;
import ph.com.sunlife.wms.dao.domain.DCRBalancingTool;
import ph.com.sunlife.wms.dao.domain.DCRBalancingToolProduct;
import ph.com.sunlife.wms.dao.domain.DCRCashier;
import ph.com.sunlife.wms.dao.domain.DCRIpacLookup;
import ph.com.sunlife.wms.dao.domain.DCRIpacValue;
import ph.com.sunlife.wms.dao.domain.DCROther;
import ph.com.sunlife.wms.dao.domain.Excemption;
import ph.com.sunlife.wms.dao.exceptions.WMSDaoException;

/**
 * This is Data Access Object with various CRUD operations specific for the
 * Balancing Tool Pages.
 * 
 * @author Zainal Limpao
 * 
 */
public interface DCRBalancingToolDao extends WMSDao<DCRBalancingTool> {

	/**
	 * Given the Cashier Work Item (Non-Filenet), this will load all the
	 * associated {@link DCRBalancingTool} objects. Naturally there will be 5
	 * for each, as each represents a Sun Life Company.
	 * 
	 * @param dcrCashierId
	 * @return
	 * @throws WMSDaoException
	 */
	List<DCRBalancingTool> getBalancingTools(Long dcrCashierId)
			throws WMSDaoException;

	/**
	 * Saves the {@link DCRBalancingToolProduct} into the database.
	 * 
	 * @param dcrBalancingToolProduct
	 * @return
	 */
	DCRBalancingToolProduct saveBalancingToolProduct(
			DCRBalancingToolProduct dcrBalancingToolProduct);

	/**
	 * Given the specific ID of the {@link DCRBalancingTool}, it will load the
	 * necessary balancing tool product columns for it.
	 * 
	 * @param dcrBalancingToolId
	 * @return
	 * @throws WMSDaoException
	 */
	List<DCRBalancingToolProduct> getAllBalancingToolProducts(
			Long dcrBalancingToolId) throws WMSDaoException;

	List<DCRBalancingToolProduct> getAllBalancingToolProducts(
			List<Long> dcrBalancingToolIds) throws WMSDaoException;

	/**
	 * This is the retrieval operation using DB Linked Servers going to IPAC.
	 * 
	 * @param dcrCashier
	 * @return
	 * @throws WMSDaoException
	 */
	List<DCRIpacValue> getIpacValues(DCRCashier dcrCashier)
			throws WMSDaoException;

	// /**
	// * This loads a table of {@link DCRIpacLookup} values, to be used to map
	// * {@link DCRIpacValue} to their corresponding
	// * {@link DCRBalancingToolProduct}.
	// *
	// * <p>
	// * <i>Note that this is a singleton call, in which will populate a
	// singleton
	// * list as values do not change overtime. </i>
	// * </p>
	// *
	// * @return
	// * @throws WMSDaoException
	// */
	// List<DCRIpacLookup> getIpacLookupMapping() throws WMSDaoException;

	/**
	 * Confirms the {@link DCRBalancingTool}.
	 * 
	 * @param entity
	 * @return
	 * @throws WMSDaoException
	 */
	boolean confirmBalancingTool(DCRBalancingTool entity, boolean isDayOne)
			throws WMSDaoException;

	DCROther saveOther(DCROther dcrOther) throws WMSDaoException;

	List<DCROther> getDcrOthers(List<Long> productIds) throws WMSDaoException;

	boolean saveWorkSite(DCRBalancingToolProduct entity) throws WMSDaoException;

	List<Excemption> getExcemptions(Long dcrId) throws WMSDaoException;

	List<CollectionOnBehalfData> getCollectionOnBehalfData(Long dcrId)
			throws WMSDaoException;

	List<DCROther> getDcrOthersByDcrId(Long dcrId) throws WMSDaoException;

	List<BalancingToolSnapshot> getSnapshotsByDcrId(Long dcrId)
			throws WMSDaoException;

	List<DCRBalancingTool> getMultipleBalancingTools(
			List<Long> cashierWorkItemIds) throws WMSDaoException;
}
