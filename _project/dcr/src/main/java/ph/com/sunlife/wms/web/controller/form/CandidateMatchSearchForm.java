package ph.com.sunlife.wms.web.controller.form;

import java.util.Date;

import ph.com.sunlife.wms.util.WMSDateUtil;

public class CandidateMatchSearchForm extends CandidateMatchForm {
	
	private String searchDcrDateStr;
	
	private String searchCcId;
	
	private Date searchDcrDate;

	public String getSearchDcrDateStr() {
		return searchDcrDateStr;
	}

	public void setSearchDcrDateStr(String searchDcrDateStr) {
		this.searchDcrDateStr = searchDcrDateStr;
		this.searchDcrDate = WMSDateUtil.toDate(searchDcrDateStr);
	}

	public String getSearchCcId() {
		return searchCcId;
	}

	public void setSearchCcId(String searchCcId) {
		this.searchCcId = searchCcId;
	}

	public Date getSearchDcrDate() {
		return searchDcrDate;
	}

	public void setSearchDcrDate(Date searchDcrDate) {
		this.searchDcrDate = searchDcrDate;
	}
	
	

}
