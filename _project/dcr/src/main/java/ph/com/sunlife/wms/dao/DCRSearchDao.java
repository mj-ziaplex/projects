package ph.com.sunlife.wms.dao;

import java.util.List;

import ph.com.sunlife.wms.dao.domain.CustomerCenter;
import ph.com.sunlife.wms.dao.domain.Hub;
import ph.com.sunlife.wms.dao.exceptions.WMSDaoException;

public interface DCRSearchDao {
	
	List<CustomerCenter> getCCByHubUserCC(String ccId) throws WMSDaoException;
	
	List<CustomerCenter> getCCByHub(String hubId) throws WMSDaoException;
	
	List<Hub> getCCHubs() throws WMSDaoException;

}
