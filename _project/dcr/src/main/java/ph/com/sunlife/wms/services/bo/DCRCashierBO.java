package ph.com.sunlife.wms.services.bo;

import java.util.Date;

public class DCRCashierBO implements Comparable<DCRCashierBO> {

	private Long id;

	private CashierBO cashier;

	private DCRBO dcr;

	private String status;

	private Date lastUpdateDate;

	private byte[] dcrCashierFile;

	private String dcrCashierFileDescription;

	private DCRVDILBO dcrVDIL;
	
	private String formatedDateStr;
	
	private String formatedRCDStr;
	
	private String shortName;
	
	public String getFormatedRCDStr() {
		return formatedRCDStr;
	}

	public void setFormatedRCDStr(String formatedRCDStr) {
		this.formatedRCDStr = formatedRCDStr;
	}

	public String getFormatedDateStr() {
		return formatedDateStr;
	}

	public void setFormatedDateStr(String formatedDateStr) {
		this.formatedDateStr = formatedDateStr;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public CashierBO getCashier() {
		return cashier;
	}

	public void setCashier(CashierBO cashier) {
		this.cashier = cashier;
	}

	public void setCashier(String cashierId) {
		CashierBO cashier = new CashierBO(cashierId);
		this.setCashier(cashier);
	}

	public DCRBO getDcr() {
		return dcr;
	}

	public void setDcr(DCRBO dcr) {
		this.dcr = dcr;
	}

	public void setDcr(Long dcrId) {
		DCRBO dcr = new DCRBO();
		dcr.setId(dcrId);
		this.setDcr(dcr);
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getLastUpdateDate() {
		return lastUpdateDate;
	}

	public void setLastUpdateDate(Date lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
	}

	public byte[] getDcrCashierFile() {
		return dcrCashierFile;
	}

	public void setDcrCashierFile(byte[] dcrCashierFile) {
		this.dcrCashierFile = dcrCashierFile;
	}

	public String getDcrCashierFileDescription() {
		return dcrCashierFileDescription;
	}

	public void setDcrCashierFileDescription(String dcrCashierFileDescription) {
		this.dcrCashierFileDescription = dcrCashierFileDescription;
	}

	public DCRVDILBO getDcrVDIL() {
		return dcrVDIL;
	}

	public void setDcrVDIL(DCRVDILBO dcrVDIL) {
		this.dcrVDIL = dcrVDIL;
	}

	public void setDcrVDIL(Long dcrVDILId) {
		DCRVDILBO dcrVDIL = new DCRVDILBO();
		dcrVDIL.setId(dcrVDILId);
		this.setDcrVDIL(dcrVDIL);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DCRCashierBO other = (DCRCashierBO) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public int compareTo(DCRCashierBO other) {
		int compareTo = 0;
		if (this.getDcr() != null && this.getDcr().getDcrDate() != null
				&& other.getDcr() != null
				&& other.getDcr().getDcrDate() != null) {
			compareTo = other.getDcr().getDcrDate()
					.compareTo(this.getDcr().getDcrDate());
		}
		return compareTo;
	}

	public String getShortName() {
		return shortName;
	}

	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

}
