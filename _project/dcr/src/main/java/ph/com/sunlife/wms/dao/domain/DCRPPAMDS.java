package ph.com.sunlife.wms.dao.domain;

import java.util.Date;

import static ph.com.sunlife.wms.util.ParameterMaskUtil.toMaskedString;


public class DCRPPAMDS extends Entity {
	
	private Long dcrId;
	
	private String salesSlipNumber;
	
	private String accountNumber;
	
	private String approvalNumber;
	
	private String reconciled;
	
	private String reconBy;
	
	private String ppaFindings;
	
	private String ppaNotes;
	
	private Date reconDate;
	
	public Date getReconDate() {
		return reconDate;
	}

	public void setReconDate(Date reconDate) {
		this.reconDate = reconDate;
	}

	public String getReconBy() {
		return reconBy;
	}

	public void setReconBy(String reconBy) {
		this.reconBy = reconBy;
	}

	public String getPpaFindings() {
		return ppaFindings;
	}

	public void setPpaFindings(String ppaFindings) {
		this.ppaFindings = ppaFindings;
	}

	public String getPpaNotes() {
		return ppaNotes;
	}

	public void setPpaNotes(String ppaNotes) {
		this.ppaNotes = ppaNotes;
	}

	public Long getDcrId() {
		return dcrId;
	}

	public void setDcrId(Long dcrId) {
		this.dcrId = dcrId;
	}

	public String getSalesSlipNumber() {
		return salesSlipNumber;
	}

	public void setSalesSlipNumber(String salesSlipNumber) {
		this.salesSlipNumber = salesSlipNumber;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = toMaskedString(accountNumber);
	}

	public String getApprovalNumber() {
		return approvalNumber;
	}

	public void setApprovalNumber(String approvalNumber) {
		this.approvalNumber = approvalNumber;
	}

	public String getReconciled() {
		return reconciled;
	}

	public void setReconciled(String reconciled) {
		this.reconciled = reconciled;
	}
	
	
}
