package ph.com.sunlife.wms.web.controller.form;

import javax.servlet.http.HttpSession;

import org.springframework.validation.Errors;

import ph.com.sunlife.wms.web.controller.binder.BinderAware;
import ph.com.sunlife.wms.web.controller.binder.SessionAware;

// Change according to Healthcheck
// *Problem was declared as public/protected in BinderAware
// *Change BinderAware interface class to abstract class and extends it rather than implements
public class EmailForm extends BinderAware implements SessionAware {

	private HttpSession session;
	
	private String to;
	
	private String from;
	
	private String cc;
	
	private String bcc;
	
	private String subject;
	
	private Long dcrId;
	
	private String content;
	
	private boolean isSent;
	
	private boolean reloadSP;
	
	private String concatErrorNameList;
	
	private boolean fromErrorTag;
	
	public boolean isReloadSP() {
		return reloadSP;
	}

	public void setReloadSP(boolean reloadSP) {
		this.reloadSP = reloadSP;
	}

	public Long getDcrId() {
		return dcrId;
	}

	public void setDcrId(Long dcrId) {
		this.dcrId = dcrId;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getCc() {
		return cc;
	}

	public void setCc(String cc) {
		this.cc = cc;
	}

	public String getBcc() {
		return bcc;
	}

	public void setBcc(String bcc) {
		this.bcc = bcc;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

    // Change according to Healthcheck
    // *Problem was declared as public/protected in BinderAware
    // *Change use BinderAware getter instead of direct access
    public String[] getAllowedFields() {
        return super.getAllowedFields();
    }

    // Change according to Healthcheck
    // *Problem was declared as public/protected in BinderAware
    // *Change use BinderAware getter instead of direct access
    public String[] getRequiredFields() {
        return super.getRequiredFields();
    }

    // Change according to Healthcheck
    // *Problem was declared as public/protected in BinderAware
    // *Change use BinderAware getter instead of direct access
    public String[] getUncheckedFields() {
        return super.getUncheckedFields();
    }

	@Override
	public void afterBind() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean validate(Errors errors) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public HttpSession getSession() {
		return session;
	}

	@Override
	public void setSession(HttpSession session) {
		this.session = session;
	}

	@Override
	public String getSecretKey() {
		return null;
	}

	public boolean isSent() {
		return isSent;
	}

	public void setSent(boolean isSent) {
		this.isSent = isSent;
	}

	public String getConcatErrorNameList() {
		return concatErrorNameList;
	}

	public void setConcatErrorNameList(String concatErrorNameList) {
		this.concatErrorNameList = concatErrorNameList;
	}

	public boolean isFromErrorTag() {
		return fromErrorTag;
	}

	public void setFromErrorTag(boolean fromErrorTag) {
		this.fromErrorTag = fromErrorTag;
	}

	
	
}
