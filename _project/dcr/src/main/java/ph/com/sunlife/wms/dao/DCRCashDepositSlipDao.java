package ph.com.sunlife.wms.dao;

import java.util.List;

import ph.com.sunlife.wms.dao.domain.DCRCashDepositSlip;
import ph.com.sunlife.wms.dao.exceptions.WMSDaoException;

/**
 * This is the interface responsible for doing CRUD functionalities for
 * {@link DCRCashDepositSlip}.
 * 
 * <p>
 * Note that this interface does not have implementation for
 * {@link WMSDao#refresh(Object)}.
 * </p>
 * 
 * @author Zainal Limpao
 * @see WMSDao
 */
public interface DCRCashDepositSlipDao extends WMSDao<DCRCashDepositSlip> {

	DCRCashDepositSlip getCashDepositSlipByExample(DCRCashDepositSlip example)
			throws WMSDaoException;

	DCRCashDepositSlip saveCashDepositSlip(DCRCashDepositSlip dcrCashDepositSlip)
			throws WMSDaoException;

	List<DCRCashDepositSlip> getAllCashDepositSlipsByDcrId(Long dcrId)
			throws WMSDaoException;
	
	List<DCRCashDepositSlip> getAllCashDepositSlipsByDcrCashierId(
			Long dcrCashierId) throws WMSDaoException;
}
