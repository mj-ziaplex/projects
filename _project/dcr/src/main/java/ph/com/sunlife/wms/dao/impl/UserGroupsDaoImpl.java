package ph.com.sunlife.wms.dao.impl;

import java.sql.SQLException;
import java.util.List;

import org.springframework.orm.ibatis.SqlMapClientCallback;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import ph.com.sunlife.wms.dao.UserGroupsDao;
import ph.com.sunlife.wms.dao.domain.HubCC;
import ph.com.sunlife.wms.dao.domain.HubCCNBO;
import ph.com.sunlife.wms.dao.domain.HubCCNBOUserGroup;
import ph.com.sunlife.wms.dao.domain.UserGroup;
import ph.com.sunlife.wms.dao.domain.UserGroupHub;
import ph.com.sunlife.wms.dao.exceptions.WMSDaoException;

import com.ibatis.sqlmap.client.SqlMapExecutor;

/**
 * The implementing clas of {@link UserGroupsDao}.
 * 
 * @author Zainal Limpao
 * 
 */
public class UserGroupsDaoImpl extends SqlMapClientDaoSupport implements
		UserGroupsDao {

	private static final String NAMESPACE = "UserGroups";

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ph.com.sunlife.wms.dao.UserGroupsDao#getUserGroupHubList(java.lang.String
	 * )
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<UserGroupHub> getUserGroupHubList(final String userId)
			throws WMSDaoException {
		List<UserGroupHub> list = null;

		try {
			list = (List<UserGroupHub>) getSqlMapClientTemplate().execute(
					new SqlMapClientCallback() {

						@Override
						public Object doInSqlMapClient(SqlMapExecutor executor)
								throws SQLException {
							return executor.queryForList(NAMESPACE
									+ ".getUserGroupHubList", userId);
						}
					});
		} catch (Exception e) {
			throw new WMSDaoException(e);
		}
		return list;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<String> getHubForCustomerCenter(String siteCode)
			throws WMSDaoException {
		List<String> list = null;

		try {
			list = (List<String>) getSqlMapClientTemplate().queryForList(
					NAMESPACE + ".getHubForCustomerCenter", siteCode);
		} catch (Exception e) {
			throw new WMSDaoException(e);
		}
		return list;

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<String> getGroupNames(List<String> groupIds)
			throws WMSDaoException {
		try {
			List<String> groupNames = (List<String>) getSqlMapClientTemplate()
					.queryForList(NAMESPACE + ".getGroupNames", groupIds);
			return groupNames;
		} catch (Exception e) {
			throw new WMSDaoException(e);
		}
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<UserGroupHub> getAllUserGroupHubList()
			throws WMSDaoException {
		List<UserGroupHub> list = null;

		try {
			list = (List<UserGroupHub>) getSqlMapClientTemplate().execute(
					new SqlMapClientCallback() {

						@Override
						public Object doInSqlMapClient(SqlMapExecutor executor)
								throws SQLException {
							return executor.queryForList(NAMESPACE
									+ ".getAllUserGroupHubList");
						}
					});
		} catch (Exception e) {
			throw new WMSDaoException(e);
		}
		return list;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<HubCCNBOUserGroup> getUserGroupHubByUserId(HubCCNBOUserGroup hub) throws WMSDaoException {
		try {
			List<HubCCNBOUserGroup> list = (List<HubCCNBOUserGroup>) getSqlMapClientTemplate()
					.queryForList(NAMESPACE + ".getUserGroupHubByUserId", hub);
			return list;
		} catch (Exception ex) {
			throw new WMSDaoException(ex);
		}
	}
	
	@Override
	public boolean createUserGroupHub(HubCCNBOUserGroup hub) throws WMSDaoException {
		boolean success = false;
		try {
			getSqlMapClientTemplate().queryForObject(
					NAMESPACE + ".createUserGroupHub", hub);
			success = true;
		} catch (Exception ex) {
			throw new WMSDaoException(ex);
		}
		return success;
	}
	
	@Override
	public boolean updateUserGroupHub(HubCCNBOUserGroup hub) throws WMSDaoException {
		try {
			int updatedRows = getSqlMapClientTemplate().update(
					NAMESPACE + ".updateUserGroupHub", hub);
			return updatedRows > 0;
		} catch (Exception ex) {
			throw new WMSDaoException(ex);
		}
	}
	
	@Override
	public boolean deleteUserGroupHub(HubCCNBOUserGroup hub) throws WMSDaoException {
		int rowsDeleted = 0;
		try {
			rowsDeleted = getSqlMapClientTemplate().delete(
					NAMESPACE + ".deleteUserGroupHub", hub);
		} catch (Exception e) {
			throw new WMSDaoException(e);
		}
		return (rowsDeleted != 0);
	}
	
	@Override
	public boolean createUserGroup(UserGroup group) throws WMSDaoException {
		boolean success = false;
		try {
			getSqlMapClientTemplate().queryForObject(
					NAMESPACE + ".createUserGroup", group);
			success = true;
		} catch (Exception ex) {
			throw new WMSDaoException(ex);
		}
		return success;
	}
	
	@Override
	public boolean deleteUserGroup(UserGroup group) throws WMSDaoException {
		int rowsDeleted = 0;
		try {
			rowsDeleted = getSqlMapClientTemplate().delete(
					NAMESPACE + ".deleteUserGroup", group);
		} catch (Exception e) {
			throw new WMSDaoException(e);
		}
		return (rowsDeleted != 0);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<HubCCNBO> getHubCCNBO(HubCCNBO nbo) throws WMSDaoException {
		try {
			List<HubCCNBO> list = (List<HubCCNBO>) getSqlMapClientTemplate()
					.queryForList(NAMESPACE + ".getHubCCNBO", nbo);
			return list;
		} catch (Exception ex) {
			throw new WMSDaoException(ex);
		}
	}
	
	@Override
	public boolean createHubCCNBO(HubCCNBO nbo) throws WMSDaoException {
		boolean success = false;
		try {
			getSqlMapClientTemplate().queryForObject(
					NAMESPACE + ".createHubCCNBO", nbo);
			success = true;
		} catch (Exception ex) {
			throw new WMSDaoException(ex);
		}
		return success;
	}
	
	@Override
	public boolean createHubCC(HubCC cc) throws WMSDaoException {
		boolean success = false;
		try {
			getSqlMapClientTemplate().queryForObject(
					NAMESPACE + ".createHubCC", cc);
			success = true;
		} catch (Exception ex) {
			throw new WMSDaoException(ex);
		}
		return success;
	}
	
	@Override
	public boolean updateHubCCNBO(HubCCNBO nbo) throws WMSDaoException {
		try {
			int updatedRows = getSqlMapClientTemplate().update(
					NAMESPACE + ".updateHubCCNBO", nbo);
			return updatedRows > 0;
		} catch (Exception ex) {
			throw new WMSDaoException(ex);
		}
	}
	
	@Override
	public boolean deleteHubCCNBO(HubCCNBO nbo) throws WMSDaoException {
		int rowsDeleted = 0;
		try {
			rowsDeleted = getSqlMapClientTemplate().delete(
					NAMESPACE + ".deleteHubCCNBO", nbo);
		} catch (Exception e) {
			throw new WMSDaoException(e);
		}
		return (rowsDeleted != 0);
	}
	
	@Override
	public boolean updateHubCC(HubCC cc) throws WMSDaoException {
		try {
			int updatedRows = getSqlMapClientTemplate().update(
					NAMESPACE + ".updateHubCC", cc);
			return updatedRows > 0;
		} catch (Exception ex) {
			throw new WMSDaoException(ex);
		}
	}
	
	@Override
	public boolean deleteHubCC(HubCC cc) throws WMSDaoException {
		int rowsDeleted = 0;
		try {
			rowsDeleted = getSqlMapClientTemplate().delete(
					NAMESPACE + ".deleteHubCC", cc);
		} catch (Exception e) {
			throw new WMSDaoException(e);
		}
		return (rowsDeleted != 0);
	}
}
