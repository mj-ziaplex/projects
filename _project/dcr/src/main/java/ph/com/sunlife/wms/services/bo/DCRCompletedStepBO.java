package ph.com.sunlife.wms.services.bo;

import java.util.List;

public class DCRCompletedStepBO implements Comparable<DCRCompletedStepBO>{
	private String stepId;
	private String stepName;
	private String userName;
	private List<DCRErrorBO> dcrErrors;
	private DCRErrorTagLookupBO errorTagLookup;

	public String getStepId() {
		return stepId;
	}

	public void setStepId(String stepId) {
		this.stepId = stepId;
	}

	public String getStepName() {
		return stepName;
	}

	public void setStepName(String stepName) {
		this.stepName = stepName;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public List<DCRErrorBO> getDcrErrors() {
		return dcrErrors;
	}

	public void setDcrErrors(List<DCRErrorBO> dcrErrors) {
		this.dcrErrors = dcrErrors;
	}

	public DCRErrorTagLookupBO getErrorTagLookup() {
		return errorTagLookup;
	}

	public void setErrorTagLookup(DCRErrorTagLookupBO errorTagLookup) {
		this.errorTagLookup = errorTagLookup;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((stepId == null) ? 0 : stepId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DCRCompletedStepBO other = (DCRCompletedStepBO) obj;
		if (stepId == null) {
			if (other.stepId != null)
				return false;
		} else if (!stepId.equals(other.stepId))
			return false;
		return true;
	}

	@Override
	public int compareTo(DCRCompletedStepBO other) {
		int compareTo = 0;
		if(other.getStepId() != null && this.getStepId() != null){
			compareTo = this.getStepId().compareTo(other.getStepId());
		}
		return compareTo;
	}
	
	
}
