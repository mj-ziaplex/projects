package ph.com.sunlife.wms.util;

public class WMSStringUtil {
	
	public static String replaceEscChar(String str){
		if(null != str){
			str = str.replaceAll("& #39;", "\\'");
			str = str.replaceAll("& #40;", "(");
			str = str.replaceAll("& #41;", ")");
			str = str.replaceAll("& lt;", "<");
			str = str.replaceAll("& gt;", ">");
		}
		return str;
	}

}
