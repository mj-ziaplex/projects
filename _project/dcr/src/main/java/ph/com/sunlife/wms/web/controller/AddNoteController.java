package ph.com.sunlife.wms.web.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.web.servlet.ModelAndView;

import ph.com.sunlife.wms.security.Role;
import ph.com.sunlife.wms.services.AttachmentService;
import ph.com.sunlife.wms.services.bo.UserSession;
import ph.com.sunlife.wms.services.exception.InvalidFileTypeException;
import ph.com.sunlife.wms.services.exception.ServiceException;
import ph.com.sunlife.wms.web.controller.form.AddNoteSPForm;
import ph.com.sunlife.wms.web.exception.ApplicationException;

/**
 * The Controller used when adding a note within the step processor.
 * 
 * @author Crisseljess Mendoza
 * 
 */
public class AddNoteController extends AbstractSecuredMultiActionController {

	private static final String VIEW_NAME = "addNoteScreenView";

	private static final String RELOAD_SP_KEY = "reloadSP";

	private static final String TITLE_KEY = "title";

	private AttachmentService attachmentService;

	public ModelAndView showAddNoteScreen(HttpServletRequest request,
			HttpServletResponse response, AddNoteSPForm form) {

		ModelAndView modelAndView = new ModelAndView();

		modelAndView.setViewName(VIEW_NAME);
		modelAndView.addObject("dcrId", form.getDcrId());
		modelAndView.addObject(RELOAD_SP_KEY, form.isReloadSP());
		modelAndView.addObject(TITLE_KEY, "Add Note");
		return modelAndView;
	}

	public ModelAndView saveNote(HttpServletRequest request,
			HttpServletResponse response, AddNoteSPForm form)
			throws ApplicationException {

		UserSession userSession = getUserSession(request);
		String uploaderId = userSession.getUserId();
		Long dcrId = form.getDcrId();
		Role role = userSession.getRoles().get(0);
		String content = form.getContent();
		String description = form.getTitle();

		if (!StringUtils.contains(content, "<html>")) {
			content = "<html>" + content + "</html>";
			StringUtils.replace(content, "<", "&lt;");
			StringUtils.replace(content, ">", "&gt;");
		}

		byte[] file = content.getBytes();

		try {
			attachmentService.uploadNonCashierAttachment(dcrId, uploaderId,
					description, role, file);
			form.setReloadSP(true);
		} catch (ServiceException e) {
			throw new ApplicationException(e);
		} catch (InvalidFileTypeException e) {
			throw new ApplicationException(e);
		}

		return showAddNoteScreen(request, response, form);
	}

	@Override
	protected Role[] allowedRoles() {
		return new Role[] { Role.CASHIER, Role.MANAGER, Role.PPA };
	}

	public void setAttachmentService(AttachmentService attachmentService) {
		this.attachmentService = attachmentService;
	}

}
