package ph.com.sunlife.wms.web.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;
import org.springframework.web.servlet.ModelAndView;

import ph.com.sunlife.wms.dao.domain.Company;
import ph.com.sunlife.wms.dao.domain.Currency;
import ph.com.sunlife.wms.dao.domain.ProductType;
import ph.com.sunlife.wms.security.Role;
import ph.com.sunlife.wms.services.DCRCommentService;
import ph.com.sunlife.wms.services.DCRReversalService;
import ph.com.sunlife.wms.services.bo.CompanyBO;
import ph.com.sunlife.wms.services.bo.DCRBalancingToolBO;
import ph.com.sunlife.wms.services.bo.DCRBalancingToolProductBO;
import ph.com.sunlife.wms.services.bo.DCRCashierBO;
import ph.com.sunlife.wms.services.bo.DCRCommentBO;
import ph.com.sunlife.wms.services.bo.DCRIpacValueBO;
import ph.com.sunlife.wms.services.bo.DCRReversalBO;
import ph.com.sunlife.wms.services.bo.UserSession;
import ph.com.sunlife.wms.services.exception.ServiceException;
import ph.com.sunlife.wms.web.controller.form.AddCommentForm;
import ph.com.sunlife.wms.web.controller.form.CashierWorkItemForm;
import ph.com.sunlife.wms.web.controller.form.ReversalForm;
import ph.com.sunlife.wms.web.exception.ApplicationException;

/**
 * The abstraction for {@link MultiActionController} that is common to all
 * Balancing Tool Pages.
 * 
 * @author Zainal Limpao
 * 
 */
public abstract class AbstractBalancingToolController extends
		AbstractCashierWorkItemController {

	private static final String IS_DAY_ONE_KEY = "isDayOne";

	private static final Logger LOGGER = Logger
			.getLogger(AbstractBalancingToolController.class);

	private static final String COMPANY_KEY = "company";

	private static final String IS_CONFIRMED_KEY = "isConfirmed";

	private static final String DCR_COMMENTS = "dcrComments";

	protected static final String PRODUCTS_TO_DISPLAY_KEY = "productsToDisplay";

	protected static final String SPECIFIC_BALANCING_TOOL_KEY = "balancingTool";

	protected static final String IPAC_VALUES_KEY = "ipacValues";

    // Change according to Healthcheck
    // *Problem was declared as public/protected
    // *Change was declare as private
    private static final ProductType[] DISPLAY_ALL_PRODUCT_TYPES = ProductType.values();

	private DCRCommentService dcrCommentService;

	private DCRReversalService dcrReversalService;

	/**
	 * A marker to determine the corresponding {@link Company} of the
	 * implementing controller.
	 * 
	 * @return
	 */
	protected abstract Company getCompany();

	/**
	 * Lists down the appropriate {@link ProductType} for this controller. By
	 * the default, all product types are considered.
	 * 
	 * @return
	 */
	protected ProductType[] productTypesToDisplay() {
		return DISPLAY_ALL_PRODUCT_TYPES;
	}

	/**
	 * This is a required field that can be configured on Spring
	 * 
	 * @return
	 */
	protected abstract String getDisplayViewName();

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ph.com.sunlife.wms.web.controller.AbstractSecuredMultiActionController
	 * #allowedRoles()
	 */
	@Override
	protected Role[] allowedRoles() {
		// Only CASHIER personnel are expected to access this controller.
		return new Role[] { Role.CASHIER };
	}

    /**
     * An optional callback method if implementing class decides to add more
     * objects into the request attribute.
     *
     * @param model
     * @param request
     * @param response
     * @param form
     * @return
     */
    protected Map<String, Object> createCustomObjects(
            Map<String, Object> model,
            HttpServletRequest request,
            HttpServletResponse response,
            CashierWorkItemForm form) {
        return null;
    }

	/**
	 * This is when the confirm button is selected. Note that behavior is
	 * different for SLOCPI and SLGFI balancing tool pages.
	 * 
	 * @param request
	 * @param response
	 * @param form
	 * @return {@link ModelAndView} object of Spring MVC.
	 * @throws Exception
	 */
	@SuppressWarnings("rawtypes")
	public final ModelAndView confirmBalancingToolPage(
			HttpServletRequest request, HttpServletResponse response,
			CashierWorkItemForm form) throws ApplicationException {
		LOGGER.info("This balancing tool is being confirmed by user...");

		ModelAndView modelAndView = this.showBalancingToolPage(request,
				response, form);

		Map model = modelAndView.getModel();
		DCRBalancingToolBO balancingTool = (DCRBalancingToolBO) model
				.get(SPECIFIC_BALANCING_TOOL_KEY);
		CompanyBO company = balancingTool.getCompany();

		UserSession userSession = getUserSession(request);
		String centerCode = userSession.getSiteCode();

		DCRCashierBO dcrCashier = (DCRCashierBO) model.get(DCR_CASHIER_BO_KEY);

		String htmlCode = StringEscapeUtils.unescapeHtml(form.getHtmlCode());

		Date dcrDate = dcrCashier.getDcr().getDcrDate();
		Date today = cachingService.getWmsDcrSystemDate();

		boolean isToConfirm = false;
		boolean isDayOne = true;

		if (CompanyBO.SLOCPI.equals(company) || CompanyBO.SLGFI.equals(company)) {
			isDayOne = DateUtils.isSameDay(dcrDate, today);
		}
		
		if (isDayOne) {
			LOGGER.info("Confirming Day 1...");
			if (balancingTool.getDateConfirmed() == null) {
				balancingTool.setDateConfirmed(today);
				isToConfirm = true;
			}
		} else {
			LOGGER.info("Confirming Day 2...");
			//if (CompanyBO.SLOCPI.equals(company)
			//		|| CompanyBO.SLGFI.equals(company)) {
				if (balancingTool.getDateReConfirmed() == null) {
					if (balancingTool.getDateConfirmed() == null) {
						balancingTool.setDateConfirmed(today);
					}
					balancingTool.setDateReConfirmed(today);
					isToConfirm = true;
				}
			//} else {
			//	if (balancingTool.getDateConfirmed() == null) {
			//		balancingTool.setDateConfirmed(today);
			//		balancingTool.setDateReConfirmed(today);
			//		isToConfirm = true;
			//	}
			//}
		}

		if (isToConfirm) {
			LOGGER.info("Commencing Confirmation Process...");
			balancingTool.setHtmlCode(htmlCode);
			balancingTool.setDcrCashier(dcrCashier);
			balancingTool.setCcId(centerCode);

			try {
				dcrCreationService.confirmDCRBalancingTool(balancingTool,
						isDayOne);
				modelAndView.addObject(IS_CONFIRMED_KEY, true);
				LOGGER.info("Confirmation successful...");
			} catch (ServiceException ex) {
				LOGGER.error(ex);
				modelAndView
						.addObject("alertMessage",
								"A problem occured while trying to confirm the Balancing Tool");
				modelAndView.addObject(IS_CONFIRMED_KEY, false);
				balancingTool.setDateConfirmed(null);
				balancingTool.setDateReConfirmed(null);
			}
		} else {
			LOGGER.info("Unable to confirm an already confirmed balancing tool");
		}
		return modelAndView;
	}

	/**
	 * The handler method whenever user attempts to add a new comment.
	 * 
	 * @param request
	 * @param response
	 * @param form
	 * @return
	 * @throws Exception
	 */
	public final ModelAndView addNewComment(HttpServletRequest request,
			HttpServletResponse response, AddCommentForm form)
			throws ApplicationException {
		LOGGER.info("New Comment is being processed...");

		Long dcrCashierId = form.getDcrCashierId();
		UserSession userSession = getUserSession(request);
		String acf2id = userSession.getUserId();

		DCRCommentBO dcrComment = new DCRCommentBO();
		dcrComment.setAcf2id(acf2id);
		dcrComment.setDatePosted(cachingService.getWmsDcrSystemDate());
		dcrComment.setDcrCashier(dcrCashierId);
		dcrComment.setRemarks(form.getRemarks());

		try {
			DCRCashierBO dcrCashier = dcrCreationService.getDCRCashierBO(
					dcrCashierId, acf2id);

			Long dcrId = dcrCashier.getDcr().getId();
			dcrComment.setDcrId(dcrId);

			dcrCommentService.save(dcrComment);

			ModelAndView modelAndView = this.showBalancingToolPage(request,
					response, form);

			return modelAndView;
		} catch (ServiceException e) {
			throw new ApplicationException(
					"Something went wrong after trying to add new comment", e);
		}

	}

    /**
     * The handler method for adding new reversal entry.
     *
     * @param request
     * @param response
     * @param form
     * @return
     * @throws Exception
     */
    public final ModelAndView addNewReversalEntry(
            HttpServletRequest request,
            HttpServletResponse response,
            ReversalForm form) throws Exception {
        
        LOGGER.info("New Reversal Entry is being processed...");
        Long dcrBalancingToolProductId = form.getDcrBalancingToolProductId();
        double amount = form.getAmount();
        String remarks = form.getRemarks();
        Long currencyId = form.getCurrencyId();

        try {
            // Change according to Healthcheck
            // *Problem was using == on floating point is imprecise
            // *Change was to use Double.compare as it uses doubleToLongBits
            if (Double.compare(amount, 0.0) != 0) {
                DCRReversalBO entry = new DCRReversalBO();
                entry.setDcrBalancingToolProductId(dcrBalancingToolProductId);
                entry.setAmount(amount);
                entry.setRemarks(remarks);
                entry.setDateTime(cachingService.getWmsDcrSystemDate());
                entry.setCurrency(Currency.getCurrency(currencyId));
                dcrReversalService.save(entry);
            }
        } catch (ServiceException e) {
            LOGGER.error(e);
            throw new ApplicationException(e);
        }

        return this.showBalancingToolPage(request, response, form);
    }

	/**
	 * The handler method for deleting reversal.
	 * 
	 * @param request
	 * @param response
	 * @param form
	 * @return
	 * @throws Exception
	 */
	public final ModelAndView deleteReversalEntry(HttpServletRequest request,
			HttpServletResponse response, ReversalForm form) throws Exception {
		LOGGER.info("A Reversal Entry is being deleted...");

		Long dcrReversalId = form.getDcrReversalId();

		try {
			boolean isDeleted = dcrReversalService.deleteById(dcrReversalId);

			System.out.println(isDeleted);
		} catch (ServiceException e) {
			LOGGER.error(e);
			throw new ApplicationException(e);
		}

		return this.showBalancingToolPage(request, response, form);
	}

	/**
	 * The handler method for updating existing Reversal Entries.
	 * 
	 * @param request
	 * @param response
	 * @param form
	 * @return
	 * @throws Exception
	 */
	public final ModelAndView updateReversalEntry(HttpServletRequest request,
			HttpServletResponse response, ReversalForm form) throws Exception {
		LOGGER.info("A Reversal Entry is being processed...");

		Long dcrBalancingToolProductId = form.getDcrBalancingToolProductId();
		double amount = form.getAmount();
		String remarks = form.getRemarks();
		Long dcrReversalId = form.getDcrReversalId();

		try {
			DCRReversalBO businessObject = new DCRReversalBO();
			businessObject.setAmount(amount);
			businessObject.setRemarks(remarks);
			businessObject
					.setDcrBalancingToolProductId(dcrBalancingToolProductId);
			businessObject.setDateTime(cachingService.getWmsDcrSystemDate());
			businessObject.setId(dcrReversalId);

			dcrReversalService.updateReversal(businessObject);
		} catch (ServiceException e) {
			LOGGER.error(e);
			throw new ApplicationException(e);
		}

		return this.showBalancingToolPage(request, response, form);
	}

    /**
     * The default method "showBalancingToolpage" for all Balancing Tool
     * Controllers.
     *
     * @param request
     * @param response
     * @param form
     * @return
     * @throws Exception
     */
    @SuppressWarnings("unchecked")
    public final ModelAndView showBalancingToolPage(
            HttpServletRequest request,
            HttpServletResponse response,
            CashierWorkItemForm form) throws ApplicationException {

        LOGGER.info("Initializing Balancing Tool Page...");

        long startTime = System.currentTimeMillis();

        ModelAndView modelAndView = new ModelAndView();

        Long companyId = null;
        if (getCompany() != null) {
            companyId = getCompany().getId();
        } else {
            companyId = form.getCompanyId();
        }

        Long dcrCashierId = form.getDcrCashierId();

        UserSession userSession = this.getUserSession(request);
        String acf2id = userSession.getUserId();

        DCRCashierBO dcrCashierBO = null;
        DCRBalancingToolBO balancingTool = null;

        List<DCRBalancingToolProductBO> productsToDisplay = null;
        List<DCRIpacValueBO> ipacValues = null;
        boolean isConfirmed = false;

        List<DCRBalancingToolBO> balancingTools = null;
        List<DCRCommentBO> dcrComments = null;
        boolean isDayOne = false;

        try {
            dcrCashierBO = this.dcrCreationService.getDCRCashierBO(dcrCashierId, acf2id);

            Date processDate = dcrCashierBO.getDcr().getDcrDate();
            String centerCode = userSession.getSiteCode();

            Date today = cachingService.getWmsDcrSystemDate();
            isDayOne = DateUtils.isSameDay(processDate, today);

            balancingTools = this.dcrCreationService.createAndDisplayDCRBalancingTools(dcrCashierBO);

            for (DCRBalancingToolBO bo : balancingTools) {
                if (companyId.equals(bo.getCompany().getId())) {
                    balancingTool = bo;
                    productsToDisplay = this.filterProductTypes(bo.getProducts());
                    isConfirmed = checkIfConfirmed(balancingTool);
                }
            }

            // Stores values retrieved from IPAC given the cashier, customer
            // center code and date, which are associated normally to
            // DCRCashierBO object
            if (dcrCashierBO != null) {
                LOGGER.info("DCRCashierBO is not empty...");
                this.pushIpacValuesToSession(request, dcrCashierBO);
                ipacValues = this.getIpacValues(request, dcrCashierBO);

                productsToDisplay = this.dcrCreationService
                        .buildBalancingToolProducts(centerCode, processDate,
                        acf2id, ipacValues, productsToDisplay);
                balancingTool.setProducts(productsToDisplay);
            }

            dcrComments = dcrCommentService.getAllComments(dcrCashierId);
        } catch (ServiceException ex) {
            LOGGER.error(ex);
            throw new ApplicationException(ex);
        }

        modelAndView.addObject(IS_DAY_ONE_KEY, isDayOne);
        modelAndView.addObject(DCR_CASHIER_BO_KEY, dcrCashierBO);
        modelAndView.addObject(SPECIFIC_BALANCING_TOOL_KEY, balancingTool);
        modelAndView.addObject(PRODUCTS_TO_DISPLAY_KEY, productsToDisplay);
        modelAndView.addObject(IS_CONFIRMED_KEY, isConfirmed);

        modelAndView.addObject(COMPANY_KEY, Company.getCompany(companyId));
        modelAndView.addObject(PAGE_SEQUENCE_KEY, companyId);

        modelAndView.addObject(BALANCING_TOOLS_KEY, balancingTools);
        modelAndView.addObject(DCR_COMMENTS, dcrComments);

        modelAndView.addObject("alertMessage", "");

        if (CollectionUtils.isNotEmpty(ipacValues)) {
            modelAndView.addObject(IPAC_VALUES_KEY, ipacValues);
        }

        Map<String, Object> customObjects = this.createCustomObjects(
                modelAndView.getModel(), request, response, form);

        if (customObjects != null) {
            modelAndView.addAllObjects(customObjects);
        }

        modelAndView.setViewName(this.getDisplayViewName());

        long endTime = System.currentTimeMillis();
        long timeElapsed = endTime - startTime;

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("It took " + timeElapsed + "ms to load all the necessary data for this balancing tool");
        }

        LOGGER.info("Completed loading data for this balancing tool");
        return modelAndView;
    }

	private boolean checkIfConfirmed(DCRBalancingToolBO balancingTool) {

		Date today = cachingService.getWmsDcrSystemDate();
		Date dateConfirmed = balancingTool.getDateConfirmed();
		Date dateReconfirmed = balancingTool.getDateReConfirmed();

		boolean isConfirmed = false;
		Long btCompanyId = balancingTool.getCompany().getId();
		if (Company.SLOCPI.getId().equals(btCompanyId)
				|| Company.SLGFI.getId().equals(btCompanyId)) {
			if ((dateConfirmed != null && DateUtils.isSameDay(today, dateConfirmed)) || dateReconfirmed != null) {
				isConfirmed = true;
			}
		} else {
			if(dateConfirmed != null){
				isConfirmed = true;
			}
		}

		return isConfirmed;
	}

	/**
	 * Retrieves values from IPAC given the cashier ID, center code, and date of
	 * the DCR represented by {@link DCRCashierBO} object.
	 * 
	 * @param request
	 * @param dcrCashier
	 * @throws ServiceException
	 */
	private void pushIpacValuesToSession(HttpServletRequest request,
			DCRCashierBO dcrCashier) throws ServiceException {
		// Retrieves value either from DB or session1
		List<DCRIpacValueBO> values = this.getIpacValues(request, dcrCashier);
		values = dcrCreationService.buildIpacValues(values, dcrCashier);

		UserSession userSession = getUserSession(request);
		if (userSession != null) {
                    LOGGER.info("UserSession object is not empty...");
                    userSession.setIpacValues(values);
                    this.replaceUserSession(request, userSession);
		}
	}

	/**
	 * Filters the list of {@link DCRBalancingToolProductBO} object to be
	 * displayed on the screen given all the products.
	 * 
	 * @param products
	 * @return
	 */
	private List<DCRBalancingToolProductBO> filterProductTypes(
			List<DCRBalancingToolProductBO> products) {
		List<DCRBalancingToolProductBO> filteredProducts = new ArrayList<DCRBalancingToolProductBO>();

		List<ProductType> validProductTypes = new ArrayList<ProductType>();
		CollectionUtils.addAll(validProductTypes, this.productTypesToDisplay());

		for (DCRBalancingToolProductBO product : products) {
			if (validProductTypes.contains(product.getProductType())) {
				filteredProducts.add(product);
			}
		}
		return filteredProducts;
	}

	public void setDcrCommentService(DCRCommentService dcrCommentService) {
		this.dcrCommentService = dcrCommentService;
	}

	public void setDcrReversalService(DCRReversalService dcrReversalService) {
		this.dcrReversalService = dcrReversalService;
	}

}
