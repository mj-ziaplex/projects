package ph.com.sunlife.wms.web.exception;

/**
 * This exception is expected to be thrown on unrecoverable fatal errors such as
 * network connectivity, DB server downtime and the likes.
 * 
 * @author Zainal Limpao
 * 
 */
public class ApplicationException extends Exception {

	private static final long serialVersionUID = 413102437359899797L;

	/**
	 * Constructor featuring a {@link String} message.
	 * 
	 * @param message
	 */
	public ApplicationException(String message) {
		super(message);
	}

	/**
	 * Constructor featuring a {@link String} message and {@link Throwable}
	 * cause.
	 * 
	 * @param message
	 * @param cause
	 */
	public ApplicationException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * Constructor featuring a {@link Throwable}
	 * 
	 * @param cause
	 */
	public ApplicationException(Throwable cause) {
		super(cause);
	}
}