package ph.com.sunlife.wms.web.view;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.springframework.web.servlet.view.AbstractView;

public class WMSEmlView extends AbstractView {

	private static final String FILE_DATA_KEY = "fileData";

	public WMSEmlView() {
		setContentType("message/rfc822");
	}

	@SuppressWarnings("rawtypes")
	@Override
	protected void renderMergedOutputModel(Map model,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		final byte[] fileData = (byte[]) model.get(FILE_DATA_KEY);
		IOUtils.write(fileData, response.getOutputStream());
	}

}
