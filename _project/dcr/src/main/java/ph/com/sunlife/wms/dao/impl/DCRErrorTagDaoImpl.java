package ph.com.sunlife.wms.dao.impl;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.orm.ibatis.SqlMapClientCallback;

import ph.com.sunlife.wms.dao.AbstractWMSSqlMapClientDaoSupport;
import ph.com.sunlife.wms.dao.DCRErrorTagDao;
import ph.com.sunlife.wms.dao.domain.DCRError;
import ph.com.sunlife.wms.dao.domain.DCRErrorTagLog;
import ph.com.sunlife.wms.dao.domain.DCRErrorTagLogDisplay;
import ph.com.sunlife.wms.dao.domain.DCRErrorTagLogLookup;
//import ph.com.sunlife.wms.dao.domain.DCRErrorTagLookupDisplay;
import ph.com.sunlife.wms.dao.exceptions.WMSDaoException;

import com.ibatis.sqlmap.client.SqlMapExecutor;

public class DCRErrorTagDaoImpl extends
		AbstractWMSSqlMapClientDaoSupport<DCRErrorTagLog> implements
		DCRErrorTagDao {

	private static final String NAMESPACE = "DCRErrorTag";

	@Override
	protected String getNamespace() {
		return NAMESPACE;
	}

	@Override
	public DCRErrorTagLog addDCRErrorTagLog(DCRErrorTagLog entity)
			throws WMSDaoException {
		try {
			Long id = (Long) getSqlMapClientTemplate().queryForObject(
					getNamespace() + ".insertDCRErrorTagLog", entity);
			entity.setId(id);
		} catch (Exception ex) {
			throw new WMSDaoException(ex);
		}
		return entity;
	}

	@Override
	public DCRErrorTagLogLookup addDCRErrorTagLoglookup(
			DCRErrorTagLogLookup entity) throws WMSDaoException {
		try {
			Long id = (Long) getSqlMapClientTemplate().queryForObject(
					getNamespace() + ".insertDCRErrorTagLoglookup", entity);
			entity.setId(id);
		} catch (Exception ex) {
			throw new WMSDaoException(ex);
		}
		return entity;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<DCRErrorTagLogDisplay> getDCRErrorTagLogDisplay(final Long dcrId)
			throws WMSDaoException {
		List<DCRErrorTagLogDisplay> results = null;

		try {
			results = (List<DCRErrorTagLogDisplay>) getSqlMapClientTemplate()
					.execute(new SqlMapClientCallback() {

						@Override
						public Object doInSqlMapClient(SqlMapExecutor executor)
								throws SQLException {
							return executor.queryForList(getNamespace()
									+ ".selectDCRErrorTagLogDisplay", dcrId);
						}
					});
		} catch (Exception e) {
			throw new WMSDaoException(e);
		}
		return results;
	}

	// @SuppressWarnings("unchecked")
	// @Override
	// public List<DCRErrorTagLookupDisplay> getDCRErrorTagLookupDisplay(final
	// Long dcrId)
	// throws WMSDaoException {
	// List<DCRErrorTagLookupDisplay> results = null;
	//
	// try {
	// results = (List<DCRErrorTagLookupDisplay>) getSqlMapClientTemplate()
	// .execute(new SqlMapClientCallback() {
	//
	// @Override
	// public Object doInSqlMapClient(SqlMapExecutor executor)
	// throws SQLException {
	// return executor.queryForList(getNamespace()
	// + ".selectDCRErrorTagLookupDisplay", dcrId);
	// }
	// });
	// } catch (Exception e) {
	// throw new WMSDaoException(e);
	// }
	// return results;
	// }

	@Override
	public boolean deleteErrorLogLookupByDcrIdAndStepId(final Long dcrId,
			String stepId) throws WMSDaoException {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("dcrId", dcrId);
		map.put("stepId", stepId);

		int rowsDeleted = 0;
		try {
			rowsDeleted = getSqlMapClientTemplate().delete(
					getNamespace() + ".deleteErrorLogLookupByDcrIdAndStepId",
					map);
		} catch (Exception e) {
			throw new WMSDaoException(e);
		}
		return (rowsDeleted != 0);
	}

	@Override
	public boolean updateErrorLogByDcrIdAndStepId(String reason,
			Date dateUpdated, final Long dcrId, String stepId,
			String postedUpdatedById) throws WMSDaoException {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("reason", reason);
		map.put("dateUpdated", dateUpdated);
		map.put("dcrId", dcrId);
		map.put("stepId", stepId);
		map.put("postedUpdatedById", postedUpdatedById);

		int rowsUpdated = 0;
		try {
			rowsUpdated = getSqlMapClientTemplate().update(
					getNamespace() + ".updateErrorLogByDcrIdAndStepId", map);
		} catch (Exception ex) {
			throw new WMSDaoException(ex);
		}
		return (rowsUpdated != 0);
	}

	@Override
	public boolean deleteErrorLogByDcrIdAndStepId(String reasonDeleted,
			final Long dcrId, String stepId, String userId, String tagger)
			throws WMSDaoException {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("reasonDeleted", reasonDeleted);
		map.put("dcrId", dcrId);
		map.put("stepId", stepId);
		map.put("userId", userId);
		map.put("tagger", tagger);

		int rowsUpdated = 0;
		try {
			rowsUpdated = getSqlMapClientTemplate().update(
					getNamespace() + ".deleteErrorLogByDcrIdAndStepId", map);
		} catch (Exception ex) {
			throw new WMSDaoException(ex);
		}
		return (rowsUpdated != 0);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<DCRError> getDCRErrorByStatus(final String dcrStatus)
			throws WMSDaoException {
		List<DCRError> results = null;

		try {
			results = (List<DCRError>) getSqlMapClientTemplate().execute(
					new SqlMapClientCallback() {

						@Override
						public Object doInSqlMapClient(SqlMapExecutor executor)
								throws SQLException {
							return executor.queryForList(getNamespace()
									+ ".selectDCRErrorByStatus", dcrStatus);
						}
					});
		} catch (Exception e) {
			throw new WMSDaoException(e);
		}
		return results;
	}

	@Override
	public String getStepIdByStatus(final String dcrStatus)
			throws WMSDaoException {
		String stepId = null;
		try {
			stepId = (String) getSqlMapClientTemplate().execute(
					new SqlMapClientCallback() {

						@Override
						public Object doInSqlMapClient(SqlMapExecutor executor)
								throws SQLException {
							return executor.queryForObject(getNamespace()
									+ ".selectStepIdByStatus", dcrStatus);
						}
					});
		} catch (Exception e) {
			throw new WMSDaoException(e);
		}
		return stepId;
	}

	@Override
	public String getManagerByDcrId(final Long dcrId) throws WMSDaoException {
		try {
			String managerId = (String) getSqlMapClientTemplate().execute(
					new SqlMapClientCallback() {

						@SuppressWarnings("unchecked")
						@Override
						public Object doInSqlMapClient(SqlMapExecutor executor)
								throws SQLException {
							List<String> managerIds = executor.queryForList(
									getNamespace() + ".selectManagerByDcrId",
									dcrId);

							if (CollectionUtils.isNotEmpty(managerIds)) {
								return managerIds.get(0);
							}

							return null;
						}
					});
			return managerId;
		} catch (Exception e) {
			throw new WMSDaoException(e);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<String> getUsersFromAuditLogByDcrId(final Long dcrId)
			throws WMSDaoException {
		List<String> userId = new ArrayList<String>();
		try {
			userId = (List<String>) getSqlMapClientTemplate().execute(
					new SqlMapClientCallback() {

						@Override
						public Object doInSqlMapClient(SqlMapExecutor executor)
								throws SQLException {
							return executor.queryForList(getNamespace()
									+ ".getUsersFromAuditLogByDcrId", dcrId);
						}
					});
		} catch (Exception e) {
			throw new WMSDaoException(e);
		}
		return userId;
	}

}
