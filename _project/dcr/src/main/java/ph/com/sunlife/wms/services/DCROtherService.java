package ph.com.sunlife.wms.services;

import ph.com.sunlife.wms.services.bo.DCROtherBO;
import ph.com.sunlife.wms.services.exception.ServiceException;

/**
 * Service responsible for manipulating records of {@link DCROtherBO}.
 * 
 * @author Edgardo Cunanan
 * 
 */
public interface DCROtherService {

	/**
	 * Updates {@link DCROtherBO}.
	 * 
	 * @param dcrOther
	 * @return
	 * @throws ServiceException
	 */
	DCROtherBO update(DCROtherBO dcrOther) throws ServiceException;

}
