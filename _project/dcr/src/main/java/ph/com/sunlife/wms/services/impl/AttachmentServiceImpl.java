package ph.com.sunlife.wms.services.impl;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.util.IOUtils;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.springframework.util.Assert;

import ph.com.sunlife.wms.dao.DCRAddingMachineDao;
import ph.com.sunlife.wms.dao.DCRBalancingToolDao;
import ph.com.sunlife.wms.dao.DCRCandidateMatchDao;
import ph.com.sunlife.wms.dao.DCRCashDepositSlipDao;
import ph.com.sunlife.wms.dao.DCRCashierDao;
import ph.com.sunlife.wms.dao.DCRChequeDepositSlipDao;
import ph.com.sunlife.wms.dao.DCRDao;
import ph.com.sunlife.wms.dao.DCROtherFileDao;
import ph.com.sunlife.wms.dao.DCRVDILDao;
import ph.com.sunlife.wms.dao.domain.BalancingToolSnapshot;
import ph.com.sunlife.wms.dao.domain.Company;
import ph.com.sunlife.wms.dao.domain.Currency;
import ph.com.sunlife.wms.dao.domain.DCR;
import ph.com.sunlife.wms.dao.domain.DCRAddingMachine;
import ph.com.sunlife.wms.dao.domain.DCRCashDepositSlip;
import ph.com.sunlife.wms.dao.domain.DCRCashier;
import ph.com.sunlife.wms.dao.domain.DCRChequeDepositSlip;
import ph.com.sunlife.wms.dao.domain.DCRNonCashierFile;
import ph.com.sunlife.wms.dao.domain.DCROtherFile;
import ph.com.sunlife.wms.dao.domain.DCRVDIL;
import ph.com.sunlife.wms.dao.exceptions.WMSDaoException;
import ph.com.sunlife.wms.integration.DCRFilenetIntegrationService;
import ph.com.sunlife.wms.integration.domain.DCRFile;
import ph.com.sunlife.wms.integration.exceptions.IntegrationException;
import ph.com.sunlife.wms.security.Role;
import ph.com.sunlife.wms.services.AttachmentService;
import ph.com.sunlife.wms.services.bo.AttachmentBO;
import ph.com.sunlife.wms.services.bo.CompanyBO;
import ph.com.sunlife.wms.services.bo.DCRCashDepositSlipBO;
import ph.com.sunlife.wms.services.bo.DCRChequeDepositSlipBO;
import ph.com.sunlife.wms.services.bo.DCRFileBO;
import ph.com.sunlife.wms.services.bo.MatchBO;
import ph.com.sunlife.wms.services.exception.InvalidFileTypeException;
import ph.com.sunlife.wms.services.exception.ServiceException;
import ph.com.sunlife.wms.util.WMSCollectionUtils;
import ph.com.sunlife.wms.util.WMSConstants;
import ph.com.sunlife.wms.util.WMSDateUtil;
import ph.com.sunlife.wms.util.WMSMimeUtil;

/**
 * The implementing class for {@link AttachmentService}.
 * 
 * @author Zainal Limpao
 * @author Edgardo Cunanan
 * 
 */
public class AttachmentServiceImpl implements AttachmentService {

	private static final Logger LOGGER = Logger
			.getLogger(AttachmentServiceImpl.class);

	private DCRDao dcrDao;

	private DCRCashDepositSlipDao dcrCashDepositSlipDao;

	private DCRChequeDepositSlipDao dcrChequeDepositSlipDao;

	private DCRCashierDao dcrCashierDao;

	private DCRFilenetIntegrationService dcrFilenetIntegrationService;

	private DCROtherFileDao dcrOtherFileDao;

	private DCRAddingMachineDao dcrAddingMachineDao;

	private DCRVDILDao dcrVdilDao;

	private DCRBalancingToolDao dcrBalancingToolDao;

	private DCRCandidateMatchDao dcrCandidateMatchDao;

	private List<String> validFileTypes;

	public DCRCandidateMatchDao getDcrCandidateMatchDao() {
		return dcrCandidateMatchDao;
	}

	public void setDcrCandidateMatchDao(
			DCRCandidateMatchDao dcrCandidateMatchDao) {
		this.dcrCandidateMatchDao = dcrCandidateMatchDao;
	}

	public void setDcrBalancingToolDao(DCRBalancingToolDao dcrBalancingToolDao) {
		this.dcrBalancingToolDao = dcrBalancingToolDao;
	}

	public void setDcrDao(DCRDao dcrDao) {
		this.dcrDao = dcrDao;
	}

	public void setDcrVdilDao(DCRVDILDao dcrVdilDao) {
		this.dcrVdilDao = dcrVdilDao;
	}

	public void setDcrChequeDepositSlipDao(
			DCRChequeDepositSlipDao dcrChequeDepositSlipDao) {
		this.dcrChequeDepositSlipDao = dcrChequeDepositSlipDao;
	}

	public void setValidFileTypes(List<String> validFileTypes) {
		this.validFileTypes = validFileTypes;
	}

	public void setDcrAddingMachineDao(DCRAddingMachineDao dcrAddingMachineDao) {
		this.dcrAddingMachineDao = dcrAddingMachineDao;
	}

	public void setDcrOtherFileDao(DCROtherFileDao dcrOtherFileDao) {
		this.dcrOtherFileDao = dcrOtherFileDao;
	}

	public void setDcrCashierDao(DCRCashierDao dcrCashierDao) {
		this.dcrCashierDao = dcrCashierDao;
	}

	public void setDcrCashDepositSlipDao(
			DCRCashDepositSlipDao dcrCashDepositSlipDao) {
		this.dcrCashDepositSlipDao = dcrCashDepositSlipDao;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ph.com.sunlife.wms.services.AttachmentService#getAttachmentsByDcrCashierId
	 * (java.lang.Long)
	 */
	@Override
	public List<AttachmentBO> getAttachmentsByDcrCashierId(Long dcrCashierId)
			throws ServiceException {
		try {
			DCRCashier dc = dcrCashierDao.getById(dcrCashierId);

			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("Retrieving attachments for "
						+ dc.getCashier().getAcf2id() + "...");
			}

			List<AttachmentBO> list = retrieveAttachmentsForCashier(dc);
			this.fillWithSnapshotAttachments(dc, list);

			if (CollectionUtils.isNotEmpty(list) && LOGGER.isDebugEnabled()) {
				LOGGER.debug("Found " + list.size() + " attachments.");
			}

			return list;
		} catch (WMSDaoException e) {
			throw new ServiceException(e);
		}
	}

	private void fillWithSnapshotAttachments(DCRCashier dc,
			List<AttachmentBO> list) throws WMSDaoException {
		final String userId = dc.getCashier().getAcf2id();
		List<BalancingToolSnapshot> snapshots = dcrBalancingToolDao
				.getSnapshotsByDcrId(dc.getDcr().getId());
		snapshots = WMSCollectionUtils.findMatchedElements(snapshots,
				new Predicate() {

					@Override
					public boolean evaluate(Object object) {
						BalancingToolSnapshot bts = (BalancingToolSnapshot) object;
						return (StringUtils.equalsIgnoreCase(userId,
								bts.getAcf2id()));
					}
				});
		if (CollectionUtils.isNotEmpty(snapshots)) {
			for (BalancingToolSnapshot snapshot : snapshots) {
				CompanyBO company = CompanyBO.getCompany(snapshot
						.getCompanyId());
				String acf2id = snapshot.getAcf2id();

				String docId1 = snapshot.getDay1DocId();
				if (StringUtils.isNotEmpty(docId1)) {
					AttachmentBO attachment = new AttachmentBO();
					attachment.setDocId(docId1);

					String snapshotStr = "";
					if (CompanyBO.SLGFI.equals(company)
							|| CompanyBO.SLOCPI.equals(company)) {
						snapshotStr = "_DAY1_SNAPSHOT_";
					} else {
						snapshotStr = "_SNAPSHOT_";
					}

					String description = "Data Snapshot for " + company.name()
							+ " by " + StringUtils.upperCase(acf2id);
					String customFileName = StringUtils.upperCase(company
							.getName() + snapshotStr + acf2id)
							+ ".html";
					attachment.setFilename(customFileName);
					attachment.setDescription(description);

					attachment.setUploaderId(acf2id);
					attachment.setFileUrl("viewfile/view.html?fileId=" + docId1
							+ "&fileType=html");
					attachment.setFileType("html");

					attachment.setUploadDate(snapshot.getDateConfirmed());
					list.add(attachment);
				}

				String docId2 = snapshot.getDay2DocId();
				if (StringUtils.isNotEmpty(docId2)) {
					AttachmentBO attachment = new AttachmentBO();
					attachment.setDocId(docId2);

					String snapshotStr = "";
					if (CompanyBO.SLGFI.equals(company)
							|| CompanyBO.SLOCPI.equals(company)) {
						snapshotStr = "_DAY2_SNAPSHOT_";
					} else {
						snapshotStr = "_SNAPSHOT_";
					}

					String customFileName = StringUtils.upperCase(company
							.getName() + snapshotStr + acf2id)
							+ ".html";
					attachment.setFilename(customFileName);

					attachment.setFileUrl("viewfile/view.html?fileId=" + docId2
							+ "&fileType=html");
					attachment.setUploaderId(acf2id);

					String description = "Data Snapshot for " + company.name()
							+ " by " + StringUtils.upperCase(acf2id);
					attachment.setDescription(description);
					attachment.setFileType("html");

					attachment.setUploadDate(snapshot.getDateReconfirmed());

					list.add(attachment);
				}

			}
		}
	}

	private void fillWithNonCashierAttachments(Long dcrId,
			List<AttachmentBO> attachments) throws ServiceException {
		try {
			//DateTime startDateTime = null;
			//startDateTime = DateTime.now();

			List<DCRNonCashierFile> dcrNoNCashierFiles = dcrDao
					.getAllNonCashierFile(dcrId);
			
			//LOGGER.info("fillWithNonCashierAttachments dcrDao.getAllNonCashierFile:"+(DateTime.now().getMillis() - startDateTime.getMillis()));

			if (CollectionUtils.isNotEmpty(dcrNoNCashierFiles)) {
				for (DCRNonCashierFile dcrNoNCashierFile : dcrNoNCashierFiles) {
					AttachmentBO attachment = new AttachmentBO();

					//startDateTime = DateTime.now();
					DCRFile dF = dcrFilenetIntegrationService.getFileFromFilenetInfo(dcrNoNCashierFile.getVersionId());
					//LOGGER.info("fillWithNonCashierAttachments dcrFilenetIntegrationService.getFileFromFilenet:"+(DateTime.now().getMillis() - startDateTime.getMillis()));

					if (dF != null) {
						String filename = dF.getFileName();
						String contentType = dF.getContentType();
						String fileType = WMSMimeUtil.getFileType(contentType);
						int fileSize = dF.getFileSize();

						attachment = new AttachmentBO();
						attachment.setFilename(filename);
						//attachment.setFilesize(dF.getFileData().length);
						attachment.setFilesize(dcrNoNCashierFile.getFileSize().intValue());
						attachment.setFileUrl("viewfile/view.html?fileId="
								+ dcrNoNCashierFile.getVersionId()
								+ "&fileType=" + fileType);
						attachment.setUploaderId(dcrNoNCashierFile
								.getUploaderId());
						attachment.setFileType(fileType);
						attachment.setDescription(dcrNoNCashierFile
								.getDcrFileDescription());
						attachment.setUploadDate(dF.getDateCreated());

						attachment.setRecordId(dcrNoNCashierFile.getId());
						attachment.setRecordLocation("DCRNonCashierFile");
						attachment.setFilenetId(dcrNoNCashierFile
								.getVersionId());
						attachment.setShortName(dcrNoNCashierFile.getShortName());

						attachments.add(attachment);
					}
				}
			}
		} catch (WMSDaoException e) {
			LOGGER.error(e);
			throw new ServiceException(e);
		} catch (IntegrationException e) {
			LOGGER.error(e);
			throw new ServiceException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ph.com.sunlife.wms.services.AttachmentService#getAttachmentsByDcr(java
	 * .lang.Long)
	 */
	@Override
	public List<AttachmentBO> getAttachmentsByDcr(Long dcrId)
			throws ServiceException {
		LOGGER.debug("Retrieving all attachments for this DCR Workitem... ");

		//DateTime startDateTime = null;
		
		List<AttachmentBO> attachments = new ArrayList<AttachmentBO>();
		try {
			List<DCRCashier> list = dcrCashierDao
					.getDCRCashierListByDcrId(dcrId);
			if (CollectionUtils.isNotEmpty(list)) {
				for (DCRCashier dc : list) {
					//startDateTime = DateTime.now();
					attachments.addAll(retrieveAttachmentsForCashier(dc));
					//LOGGER.info("getAttachmentsByDcr retrieveAttachmentsForCashier:"+(DateTime.now().getMillis() - startDateTime.getMillis()));
				}
			}

			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("Found " + attachments.size() + " attachment(s)");
			}
		} catch (WMSDaoException e) {
			LOGGER.error(e);
			throw new ServiceException(e);
		}

		//startDateTime = DateTime.now();
		this.fillWithNonCashierAttachments(dcrId, attachments);
		//LOGGER.info("getAttachmentsByDcr fillWithNonCashierAttachments:"+(DateTime.now().getMillis() - startDateTime.getMillis()));

		return attachments;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ph.com.sunlife.wms.services.AttachmentService#
	 * getCandidateMatchAttachmentsByDCR(java.lang.Long)
	 */
	@Override
	public List<AttachmentBO> getCandidateMatchAttachmentsByDCR(Long dcrId)
			throws ServiceException {
		LOGGER.debug("retrieving all candidate match attachments for this DCR Workitem... ");

		List<AttachmentBO> attachments = new ArrayList<AttachmentBO>();
		try {
			List<DCRCashier> list = dcrCashierDao
					.getDCRCashierListByDcrId(dcrId);
			if (CollectionUtils.isNotEmpty(list)) {
				for (DCRCashier dc : list) {
					attachments
							.addAll(retrieveCandidateMatchAttachmentsForCashier(dc));
				}
			}

			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("Found " + attachments.size() + " attachments(s)");
			}
		} catch (WMSDaoException e) {
			LOGGER.error(e);
			throw new ServiceException(e);
		}

		this.fillWithNonCashierAttachments(dcrId, attachments);

		return attachments;
	}

	private List<AttachmentBO> retrieveCandidateMatchAttachmentsForCashier(
			DCRCashier dc) throws ServiceException {
		List<AttachmentBO> attachments = new ArrayList<AttachmentBO>();

		try {
			String uploaderId = dc.getCashier().getAcf2id();
			this.fillWithOtherAttachments(dc, attachments);
			if (CollectionUtils.isNotEmpty(attachments)) {
				for (AttachmentBO attachment : attachments) {
					attachment.setUploaderId(uploaderId);
				}
			}

		} catch (WMSDaoException ex) {
			throw new ServiceException(ex);
		}
		Collections.sort(attachments);
		return attachments;
	}

	/**
	 * Creates a list of {@link AttachmentBO} representing all attached files
	 * given {@link DCRCashier} work item object. Note that these
	 * {@link AttachmentBO} don't have the physical file but just references to
	 * the actual FILENET documents.
	 * 
	 * @param dc
	 * @return
	 * @throws ServiceException
	 */
	private List<AttachmentBO> retrieveAttachmentsForCashier(DCRCashier dc)
			throws ServiceException {

		List<AttachmentBO> attachments = new ArrayList<AttachmentBO>();

		try {
			String uploaderId = dc.getCashier().getAcf2id();			

			// Get all cash deposit slip PDFs
			this.fillWithCashDepositSlipAttachments(dc, attachments);

			// Get all check deposit slip PDFs
			this.fillWithChequeDepositSlipAttachments(dc, attachments);

			// Get the VDIL PDF
			this.fillWithVDIL(dc, attachments);

			// Get manually uploaded files via the Upload Files screen.
			this.fillWithOtherAttachments(dc, attachments);

			// Get the uploaded adding machine.
			this.fillWithAddingMachine(dc, attachments);

			// Associate all files to the uploader.
			if (CollectionUtils.isNotEmpty(attachments)) {
				for (AttachmentBO attachment : attachments) {
					attachment.setUploaderId(uploaderId);
				}
			}

		} catch (WMSDaoException ex) {
			throw new ServiceException(ex);
		} catch (IntegrationException ex) {
			throw new ServiceException(ex);
		}
		Collections.sort(attachments);
		return attachments;
	}

	private void fillWithAddingMachine(DCRCashier dc,
			List<AttachmentBO> attachments) throws WMSDaoException,
			IntegrationException {
		Long dcrCashierId = dc.getId();

		DCRAddingMachine entity = dcrAddingMachineDao
				.getByDcrCashierId(dcrCashierId);

		if (entity != null && dc != null) {

			DCRFile dcrFile = dcrFilenetIntegrationService
					.getFileFromFilenetInfo(entity.getDcrAddingMachineVrsSerId());

			if (dcrFile != null) {
				AttachmentBO attachment = new AttachmentBO();
				attachment.setDescription("Adding Machine");
				String customFilename = dcrFile.getFileName();
				String contentType = dcrFile.getContentType();
				//byte[] byteArr = dcrFile.getFileData();
				//int fileSize = dcrFile.getFileSize();
				attachment.setFilename(customFilename);
				attachment.setFileType(WMSMimeUtil.getFileType(contentType));
				//attachment.setFilesize(byteArr.length);
				attachment.setFilesize(null != entity.getAddingMachineFile() ? entity.getAddingMachineFile().length : 0);
				attachment.setFileUrl("viewfile/view.html?fileId="
						+ entity.getDcrAddingMachineVrsSerId() + "&fileType="
						+ WMSMimeUtil.getFileType(contentType));
				attachment.setUploadDate(dcrFile.getDateCreated());

				attachment.setRecordId(entity.getId());
				attachment.setRecordLocation("DCRAddingMachine");
				attachments.add(attachment);
			}
		}
	}

	private void fillWithOtherAttachments(DCRCashier dc,
			List<AttachmentBO> attachments) throws WMSDaoException {
		Long dcrCashierId = dc.getId();

		List<DCROtherFile> otherFiles = dcrOtherFileDao
				.getOtherFilesByDcrCashierId(dcrCashierId);

		if (CollectionUtils.isNotEmpty(otherFiles) && dc != null) {
			for (DCROtherFile otherFile : otherFiles) {
				AttachmentBO attachment = new AttachmentBO();
				String uploaderId = StringUtils.upperCase(dc.getCashier()
						.getAcf2id());
				String customFileName = createCustomFileName(otherFile,
						uploaderId) + "." + otherFile.getDcrFileType();
				Date uploadDate = otherFile.getDcrFileUploadDate();

				attachment.setFilename(customFileName);
				attachment
						.setFilesize(safeLongToInt(otherFile.getDcrFileSize()));
				attachment.setFileUrl("viewfile/view.html?fileId="
						+ otherFile.getVersionId() + "&fileType="
						+ otherFile.getDcrFileType());
				attachment.setUploaderId(uploaderId);

				String dcrFileDescription = otherFile.getDcrFileDescription();
				dcrFileDescription = StringUtils.replace(dcrFileDescription,
						WMSConstants.LINE_SEPARATOR, " ");
				dcrFileDescription = StringUtils.trim(dcrFileDescription);
				attachment.setDescription(dcrFileDescription);

				attachment.setFileType(otherFile.getDcrFileType());
				attachment.setUploadDate(uploadDate);

				attachment.setRecordId(otherFile.getId());
				attachment.setRecordLocation("DCROtherFile");
				attachment.setFilenetId(otherFile.getVersionId());

				attachments.add(attachment);
			}
		}
	}

	private String createCustomFileName(DCROtherFile otherFile,
			String uploaderId) {
		return StringUtils.upperCase(StringUtils.replace(
				"OTHER_FILE_"
						+ uploaderId
						+ "_"
						+ WMSDateUtil.toFormattedDateStrWithTime(otherFile
								.getDcrFileUploadDate()), " ", "_"));
	}

	private void fillWithVDIL(DCRCashier dc, List<AttachmentBO> attachments)
			throws WMSDaoException {

		String uploaderId = dc.getCashier().getAcf2id();
		DCRVDIL vdil = dcrVdilDao.getByDCRCashierId(dc.getId());

		if (vdil != null
				&& StringUtils.isNotEmpty(vdil.getVdilVersionSerialId())) {
			AttachmentBO attachment = new AttachmentBO();
			attachment.setDescription("Vault Daily Inventory List");
			attachment.setFilename("VDIL_" + StringUtils.upperCase(uploaderId)
					+ "." + WMSConstants.FILE_TYPE_PDF);
			attachment.setFileType("pdf");
			attachment.setUploaderId(uploaderId);
			attachment.setFileUrl("viewfile/view.html?fileId="
					+ vdil.getVdilVersionSerialId());
			attachment.setUploadDate(vdil.getDateSaved());

			attachment.setRecordId(vdil.getId());
			attachment.setRecordLocation("DCRVDIL");

			attachments.add(attachment);
		}
	}

	private void fillWithChequeDepositSlipAttachments(DCRCashier dc,
			List<AttachmentBO> attachments) throws WMSDaoException {
		Long dcrCashierId = dc.getId();

		List<DCRChequeDepositSlip> checkDepositSlips = dcrChequeDepositSlipDao
				.getAllChequeDepositSlipsByDcrCashierId(dcrCashierId);

		if (CollectionUtils.isNotEmpty(checkDepositSlips)) {
			for (DCRChequeDepositSlip ds : checkDepositSlips) {
				Company company = Company.getCompany(ds.getCompanyId());
				Currency currency = Currency.getCurrency(ds.getCurrencyId());
				String userId = ds.getAcf2id();
				String filenetId = ds.getDcrDepoSlipVersionSerialId();
				Date dcrDate = ds.getDcrDate();

				AttachmentBO attachment = new AttachmentBO();

				String prodCode = "";
				if (Company.SLAMCID != company && Company.SLAMCIP != company) {
					prodCode = "_" + ds.getProdCode();
				}
				String checkTypeName = ds.getCheckType().getCheckTypeName();
				String customFileName = company.getName() + "_" + checkTypeName
						+ prodCode + "_ChequeDepositSlip_"
						+ StringUtils.upperCase(userId) + ".pdf";

				attachment.setFilename(customFileName);
				attachment.setFilenetId(filenetId);
				attachment.setUploaderId(userId);
				attachment.setFileType("pdf");
				attachment.setDescription(company.getName() + "_"
						+ currency.getName() + " " + checkTypeName
						+ " Check Deposit Slip");

				attachment.setFileUrl("viewfile/view.html?fileId=" + filenetId);
				attachment.setUploadDate(dcrDate);

				attachment.setRecordId(ds.getId());
				attachment.setRecordLocation("DCRCheckDepositSlip");

				attachments.add(attachment);
			}
		}
	}

	public List<DCRCashDepositSlipBO> getCashDepositSlips(Long dcrCashierId)
			throws ServiceException {

		try {
			List<DCRCashDepositSlip> cashDepositSlips = dcrCashDepositSlipDao
					.getAllCashDepositSlipsByDcrCashierId(dcrCashierId);

			if (CollectionUtils.isNotEmpty(cashDepositSlips)) {
				List<DCRCashDepositSlipBO> cashDepositSlipBos = new ArrayList<DCRCashDepositSlipBO>();
				for (DCRCashDepositSlip cds : cashDepositSlips) {
					DCRCashDepositSlipBO cdsBo = new DCRCashDepositSlipBO();
					cdsBo.setCompanyId(cds.getCompanyId());
					cdsBo.setProdCode(cds.getProdCode());
					cdsBo.setCurrencyId(cds.getCurrencyId());

					cashDepositSlipBos.add(cdsBo);
				}

				return cashDepositSlipBos;
			}
		} catch (WMSDaoException ex) {
			LOGGER.error(ex);
			throw new ServiceException(ex);
		}

		return null;
	}

	public List<DCRChequeDepositSlipBO> getCheckDepositSlips(Long dcrCashierId)
			throws ServiceException {

		try {
			List<DCRChequeDepositSlip> checkDepositSips = dcrChequeDepositSlipDao
					.getAllChequeDepositSlipsByDcrCashierId(dcrCashierId);

			if (CollectionUtils.isNotEmpty(checkDepositSips)) {
				List<DCRChequeDepositSlipBO> checkDepositSlipBos = new ArrayList<DCRChequeDepositSlipBO>();
				for (DCRChequeDepositSlip cds : checkDepositSips) {
					DCRChequeDepositSlipBO cdsBo = new DCRChequeDepositSlipBO();
					cdsBo.setCompanyId(cds.getCompanyId());
					cdsBo.setProdCode(cds.getProdCode());
					cdsBo.setCurrencyId(cds.getCurrencyId());
					cdsBo.setCheckTypeId(cds.getCheckTypeId());

					checkDepositSlipBos.add(cdsBo);
				}

				return checkDepositSlipBos;
			}
		} catch (WMSDaoException ex) {
			LOGGER.error(ex);
			throw new ServiceException(ex);
		}

		return null;
	}

	private void fillWithCashDepositSlipAttachments(DCRCashier dc,
			List<AttachmentBO> attachments) throws WMSDaoException {
		// Cash Deposit Slip
		Long dcrCashierId = dc.getId();

		List<DCRCashDepositSlip> cashDepositSlips = dcrCashDepositSlipDao
				.getAllCashDepositSlipsByDcrCashierId(dcrCashierId);

		if (CollectionUtils.isNotEmpty(cashDepositSlips)) {
			for (DCRCashDepositSlip ds : cashDepositSlips) {
				Company company = Company.getCompany(ds.getCompanyId());
				Currency currency = Currency.getCurrency(ds.getCurrencyId());
				String userId = ds.getAcf2id();
				String filenetId = ds.getDcrDepoSlipVersionSerialId();
				Date dcrDate = ds.getDcrDate();

				AttachmentBO attachment = new AttachmentBO();
				String prodCode = "";
				if (Company.SLAMCID != company && Company.SLAMCIP != company) {
					prodCode = "_" + ds.getProdCode();
				}
				String fileName = company.getName() + "_" + currency.getName()
						+ prodCode + "_CashDepositSlip_"
						+ StringUtils.upperCase(userId) + ".pdf";

				attachment.setFilename(fileName);
				attachment.setFilenetId(filenetId);
				attachment.setUploaderId(userId);
				attachment.setFileType("pdf");
				attachment.setDescription(company.getName() + "_"
						+ currency.getName() + " Cash Deposit Slip");

				attachment.setFileUrl("viewfile/view.html?fileId=" + filenetId);
				attachment.setUploadDate(dcrDate);

				attachment.setRecordLocation("DCRCashDepositSlip");
				attachment.setRecordId(ds.getId());

				attachments.add(attachment);
			}
		}
	}

	// TODO: move this to a more appropriate Util Class.
	private int safeLongToInt(long l) {
		if (l < Integer.MIN_VALUE || l > Integer.MAX_VALUE) {
			throw new IllegalArgumentException(l
					+ " cannot be cast to int without changing its value.");
		}
		return (int) l;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ph.com.sunlife.wms.services.AttachmentService#getDcrContentType(byte[])
	 */
	@Override
	public String getDcrContentType(byte[] arr) throws InvalidFileTypeException {
		LOGGER.debug("Attempting to retrieve Mime type of the given byte array file...");

		String validContentType = null;
		String contentType = WMSMimeUtil.getMimeType(arr);

		if (WMSMimeUtil.MSWORD_CONTENT_TYPE.equals(contentType)) {
			validContentType = checkIfExcel(arr);

			if (StringUtils.isEmpty(validContentType)) {
				validContentType = contentType;
			}
		}
                // Change according to Healthcheck
                // *Problem was identified as security risk; and a public array
                // *Change was set as private and generated getter method to be able to fetch content
                else if (ArrayUtils.contains(WMSMimeUtil.getZIP_CONTENT_TYPE(), contentType)) {
			if (StringUtils.isEmpty(validContentType)) {
				validContentType = checkIfExcel(arr);
			}

			if (StringUtils.isEmpty(validContentType)) {
				validContentType = checkIfWord2007(arr);
			}

			// Accept ZIP
			if (StringUtils.isEmpty(validContentType)) {
				validContentType = contentType;
			}
		} else if (validFileTypes.contains(contentType)) {
			validContentType = contentType;
		} else {
			String string = new String(arr);
			String trimmedStr = StringUtils.trim(string);
			if (StringUtils.isNotEmpty(trimmedStr)) {
				arr = trimmedStr.getBytes();
			}
			contentType = WMSMimeUtil.getMimeType(arr);

			if (validFileTypes.contains(contentType)) {
				validContentType = contentType;
			}
		}

		if (StringUtils.isEmpty(validContentType)) {
			throw new InvalidFileTypeException("File is not a valid DCR Attachment");
		}

		return validContentType;
	}

	private String checkIfWord2007(byte[] arr) {
		ByteArrayInputStream bais = null;
		try {
			bais = new ByteArrayInputStream(arr);
			XWPFDocument docx = new XWPFDocument(OPCPackage.open(bais));
			if (docx != null) {
				return WMSMimeUtil.MSWORD2007_CONTENT_TYPE;
			}
		} catch (InvalidFormatException e) {
			LOGGER.error(e);
			// swallow
		} catch (IOException e) {
			LOGGER.error(e);
			// swallow
		} catch (Exception e) {
			LOGGER.error(e);
			// swallow
		} finally {
			IOUtils.closeQuietly(bais);
		}
		return null;
	}

    private String checkIfExcel(byte[] arr) {
        
        ByteArrayInputStream bais = null;
        try {
            bais = new ByteArrayInputStream(arr);
            Workbook wb = WorkbookFactory.create(bais);

            if (wb instanceof XSSFWorkbook) {
                return WMSMimeUtil.MSEXCEL2007_CONTENT_TYPE;
            } else if (wb instanceof HSSFWorkbook) {
                return WMSMimeUtil.MSEXCEL_CONTENT_TYPE;
            }
        } catch (InvalidFormatException e) {
            // Change according to Healthcheck
            // *Problem was identified as inconsistent logging
            // *Change add exceptionobject in logging
            LOGGER.error("File is not a Valid Excel Spreadsheet", e);
        } catch (IOException e) {
            // Change according to Healthcheck
            // *Problem was identified as inconsistent logging
            // *Change add exceptionobject in logging
            LOGGER.error("Problem occured on opening the file", e);
        } catch (Exception e) {
            // Change according to Healthcheck
            // *Problem was identified as inconsistent logging
            // *Change add exceptionobject in logging
            LOGGER.error("Problem occured on opening the file", e);
        } finally {
            IOUtils.closeQuietly(bais);
        }

        return null;
    }

	@Override
	public AttachmentBO uploadOtherFileAttachment(Long dcrCashierId,
			String description, String fileType, byte[] file)
			throws ServiceException, InvalidFileTypeException {
		LOGGER.debug("Begin attaching file to filenet...");

		AttachmentBO attachment = null;
		try {
			if (ArrayUtils.isEmpty(file)) {
				LOGGER.error("User must select a file to upload");
				throw new InvalidFileTypeException(
						"User must select a file to upload");
			}

			String contentType = getDcrContentType(file);
			DCRCashier dc = dcrCashierDao.getById(dcrCashierId);

			DCROtherFile dcrOtherFile = new DCROtherFile();
			dcrOtherFile.setDcrCashier(dc);

			description = StringUtils.replace(description,
					WMSConstants.LINE_SEPARATOR, " ");
			description = StringUtils.trim(description);
			dcrOtherFile.setDcrFileDescription(description);

			dcrOtherFile.setDcrFileSize(Long.valueOf(file.length));
			dcrOtherFile.setDcrFileType(WMSMimeUtil.getFileType(contentType));
			dcrOtherFile.setDcrFileUploadDate(new Date());
			// dcrOtherFile.setUserId(dc.get)

			Date dcrDate = dc.getDcr().getDcrDate();
			String docCapsiteId = dc.getDcr().getCcId();
			String uploaderId = StringUtils.upperCase(dc.getCashier()
					.getAcf2id());
			String customFileName = createCustomFileName(dcrOtherFile,
					uploaderId) + WMSMimeUtil.getFileExtension(contentType);

			String docId = dcrFilenetIntegrationService.attachFileToWorkItem(
					dcrDate, docCapsiteId, customFileName, file);

			dcrOtherFile.setVersionId(docId);

			Long fileId = dcrOtherFileDao.saveOtherFile(dcrOtherFile);
			Assert.notNull(fileId);
			LOGGER.debug("File was successfully attached");

			attachment = new AttachmentBO();
			attachment.setFilename(customFileName);
			attachment
					.setFilesize(safeLongToInt(dcrOtherFile.getDcrFileSize()));
			attachment.setFileUrl("viewfile/view.html?fileId=" + docId
					+ "&fileType=" + fileType);
			attachment.setUploaderId(uploaderId);
			attachment.setFileType(dcrOtherFile.getDcrFileType());
			attachment.setDescription(dcrOtherFile.getDcrFileDescription());
			attachment.setDocId(docId);
		} catch (IntegrationException e) {
			LOGGER.error(e);
			throw new ServiceException(e);
		} catch (WMSDaoException e) {
			LOGGER.error(e);
			throw new ServiceException(e);
		}

		return attachment;
	}

	public void setDcrFilenetIntegrationService(
			DCRFilenetIntegrationService dcrFilenetIntegrationService) {
		this.dcrFilenetIntegrationService = dcrFilenetIntegrationService;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ph.com.sunlife.wms.services.AttachmentService#getFileFromFilenet(java
	 * .lang.String)
	 */
	@Override
	public DCRFileBO getFileFromFilenet(String filenetId)
			throws ServiceException {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Retrieving file from filenet with Doc ID: "
					+ filenetId);
		}

		try {
			DCRFile filenetFile = dcrFilenetIntegrationService
					.getFileFromFilenet(filenetId);
			if (filenetFile != null) {
				LOGGER.debug("File found");

				DCRFileBO dcrFile = new DCRFileBO();
				BeanUtils.copyProperties(dcrFile, filenetFile);
				return dcrFile;
			}

			LOGGER.debug("File not found");
			return null;
		} catch (Exception e) {
			LOGGER.error(e);
			throw new ServiceException(e);
		}
	}

	private AttachmentBO uploadSpecificNonCashierAttachment(String type,
			Long dcrId, String uploaderId, String description, Role role,
			byte[] file) throws ServiceException, InvalidFileTypeException {
		LOGGER.debug("Begin attaching file to filenet...");

		AttachmentBO attachment = null;
		try {
			if (ArrayUtils.isEmpty(file)) {
				LOGGER.error("User must select a file to upload");
				throw new InvalidFileTypeException(
						"User must select a file to upload");
			}

			String contentType = getDcrContentType(file);
			String fileType = WMSMimeUtil.getFileType(contentType);
			String fileExtension = WMSMimeUtil.getFileExtension(contentType);
			Date today = new Date();

			DCR dcr = dcrDao.getById(dcrId);

			if (dcr != null) {
				int filesize = file.length;

				DCRNonCashierFile dcrNonCashierFile = new DCRNonCashierFile();

				description = StringUtils.replace(description,
						WMSConstants.LINE_SEPARATOR, " ");
				description = StringUtils.trim(description);
				dcrNonCashierFile.setDcrFileDescription(description);

				dcrNonCashierFile.setDcrFileType(fileType);
				dcrNonCashierFile.setDcrId(dcrId);
				dcrNonCashierFile.setFileSize((long) filesize);
				dcrNonCashierFile.setUploaderId(uploaderId);
				dcrNonCashierFile.setDcrFileUploadDate(today);

				Date dcrDate = dcr.getDcrDate();
				String docCapsiteId = dcr.getCcId();

				String roleName = role.name();

				String formattedDateStrWithTime = WMSDateUtil
						.toFormattedDateStrWithTime(today);

				String customFileName = StringUtils.upperCase(StringUtils
						.replace(roleName + "_" + uploaderId + "_"
								+ formattedDateStrWithTime, " ", "_"))
						+ fileExtension;

				if (StringUtils.isNotEmpty(type)) {
					customFileName = StringUtils.upperCase(type)
							+ customFileName;
				}

				String docId = dcrFilenetIntegrationService
						.attachFileToWorkItem(dcrDate, docCapsiteId,
								customFileName, file);

				if (LOGGER.isDebugEnabled()) {
					LOGGER.debug("Filename: " + customFileName);
					LOGGER.debug("Doc ID: " + docId);
					LOGGER.debug("Filesize: " + filesize);
				}

				dcrNonCashierFile.setVersionId(docId);
				dcrDao.saveNonCashierFile(dcrNonCashierFile);

				attachment = new AttachmentBO();
				attachment.setFilename(customFileName);
				attachment.setFilesize(filesize);
				attachment.setFileUrl("viewfile/view.html?fileId=" + docId
						+ "&fileType=" + fileType);
				attachment.setUploaderId(uploaderId);
				attachment.setFileType(fileType);
				attachment.setDescription(description);
			}
		} catch (IntegrationException e) {
			LOGGER.error(e);
			throw new ServiceException(e);
		} catch (WMSDaoException e) {
			LOGGER.error(e);
			throw new ServiceException(e);
		}

		return attachment;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ph.com.sunlife.wms.services.AttachmentService#matchAttachment(ph.com.
	 * sunlife.wms.services.bo.MatchBO, java.lang.String, java.lang.Long)
	 */
	@Override
	public void matchAttachment(MatchBO matchBo, String uploaderId, Long dcrId)
			throws ServiceException {
		try {
			String docId = dcrFilenetIntegrationService.copyFileToWorkItem(
					WMSDateUtil.toDate(matchBo.getDcrDate()),
					matchBo.getCcId(), matchBo.getFilenetId());
			DCRNonCashierFile dcrNonCashierFile = new DCRNonCashierFile();
			dcrNonCashierFile.setDcrFileDescription(matchBo.getDescription());
			dcrNonCashierFile.setDcrFileType(matchBo.getFileType());
			dcrNonCashierFile.setDcrId(dcrId);
			dcrNonCashierFile.setFileSize((long) matchBo.getFilesize());
			dcrNonCashierFile.setUploaderId(uploaderId);
			dcrNonCashierFile.setDcrFileUploadDate(new Date());
			dcrNonCashierFile.setVersionId(docId);
			dcrDao.saveNonCashierFile(dcrNonCashierFile);

		} catch (IntegrationException e) {
			LOGGER.error(e);
			throw new ServiceException(e);
		} catch (WMSDaoException e) {
			LOGGER.error(e);
			throw new ServiceException(e);
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ph.com.sunlife.wms.services.AttachmentService#uploadNonCashierAttachment
	 * (java.lang.Long, java.lang.String, java.lang.String,
	 * ph.com.sunlife.wms.security.Role, byte[])
	 */
	@Override
	public AttachmentBO uploadNonCashierAttachment(Long dcrId,
			String uploaderId, String description, Role role, byte[] file)
			throws ServiceException, InvalidFileTypeException {
		LOGGER.debug("Uploading file via the attach file popup...");
		return this.uploadSpecificNonCashierAttachment(null, dcrId, uploaderId,
				description, role, file);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ph.com.sunlife.wms.services.AttachmentService#uploadNotes(java.lang.Long,
	 * java.lang.String, java.lang.String, ph.com.sunlife.wms.security.Role,
	 * byte[])
	 */
	@Override
	public AttachmentBO uploadNotes(Long dcrId, String uploaderId,
			String description, Role role, byte[] file)
			throws ServiceException, InvalidFileTypeException {
		LOGGER.debug("Attaching newly created notes to the workitem...");
		return this.uploadSpecificNonCashierAttachment("notes", dcrId,
				uploaderId, description, role, file);
	}

	@Override
	public AttachmentBO uploadEmail(Long dcrId, String uploaderId,
			String description, Role role, byte[] file)
			throws ServiceException, InvalidFileTypeException {
		LOGGER.debug("Attaching newly created email to workitem...");
		return this.uploadSpecificNonCashierAttachment("email", dcrId,
				uploaderId, description, role, file);
	}

	@Override
	public void unmatchAttachment(Long recordId, String recordLocation,
			Date dcrDate, String docCapsiteId, String documentId)
			throws ServiceException {
		try {
			dcrFilenetIntegrationService.removeFileFromWorkItem(dcrDate,
					docCapsiteId, documentId);
			dcrCandidateMatchDao.deleteAttachment(recordId, recordLocation);
		} catch (WMSDaoException e) {
			LOGGER.error(e);
			throw new ServiceException(e);
		} catch (IntegrationException e) {
			LOGGER.error(e);
			throw new ServiceException(e);
		}

	}

}
