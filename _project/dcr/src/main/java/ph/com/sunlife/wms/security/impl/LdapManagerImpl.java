package ph.com.sunlife.wms.security.impl;

import java.util.Hashtable;

import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import ph.com.sunlife.wms.security.LdapManager;
import ph.com.sunlife.wms.security.exception.WMSSecurityException;

/**
 * The implementing class of {@link LdapManager}.
 *
 * @author Zainal Limpao
 *
 */
public class LdapManagerImpl implements LdapManager {

    public static final String LDAP_CONTEXT = "com.sun.jndi.ldap.LdapCtxFactory";
    private static final Logger LOGGER = Logger.getLogger(LdapManagerImpl.class);
    private String url;
    private String authentication;
    private boolean includeDescriptionLookup = false;

    /**
     * Authenticate user credentials against LDAP server
     *
     * @param username
     * @param password
     * @return
     * @throws ph.com.sunlife.wms.security.exception.WMSSecurityException
     */
    public boolean authenticateUser(String username, String password) throws WMSSecurityException {
        LOGGER.info("Attempting to authenticate user...");
        if (StringUtils.isEmpty(username)) {
            LOGGER.error("username can not be empty...");
            throw new WMSSecurityException("username can not be empty");
        }

        if (StringUtils.isEmpty(password)) {
            LOGGER.error("password can not be empty...");
            throw new WMSSecurityException("password can not be empty");
        }

        DirContext localCtx = null;
        boolean retval;

        try {
            Hashtable<String, String> env = new Hashtable<String, String>(11);
            env.put(Context.INITIAL_CONTEXT_FACTORY, LDAP_CONTEXT);
            env.put(Context.PROVIDER_URL, url);
            env.put(Context.SECURITY_AUTHENTICATION, authentication);
            env.put(Context.SECURITY_PRINCIPAL, username);
            env.put(Context.SECURITY_CREDENTIALS, password);
            localCtx = new InitialDirContext(env);
            retval = (this.includeDescriptionLookup ? this.userExists(username, localCtx) : true);
        } catch (NamingException e) {
            LOGGER.error("Error encountered in getting LDAP details: ", e);
            retval = false;
        } finally {
            this.closeContextQuietly(localCtx);
        }

        return retval;
    }

    private void closeContextQuietly(DirContext ctx) {
        if (ctx != null) {
            try {
                ctx.close();
            } catch (NamingException e) {
                LOGGER.error("Error in closing the context is encountered.", e);
                // This is closed quietly hence exception is swallowed.
            }
        }
    }

    /**
     * Generate context string parameter
     *
     * @param username
     * @return
     */
    private String getCtxString(String username) {
        String ctxstring = "CN=" + username + ",OU=Staff,OU=User Accounts,DC=ph,DC=sunlife";
        return ctxstring;
    }

    /**
     * Check if an LDAP user exists
     *
     * @param username
     * @return
     * @throws Exception
     */
    private boolean userExists(String username, DirContext ctx) {
        boolean userExists;
        String[] attrIDs = {"DESCRIPTION"};
        try {
            ctx.getAttributes(getCtxString(username), attrIDs);
            userExists = true;
        } catch (NamingException nExc) {
            LOGGER.error("Error in checking LDAP: " + nExc);
            userExists = false;
        }
        return userExists;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setAuthentication(String authentication) {
        this.authentication = authentication;
    }

    public void setIncludeDescriptionLookup(boolean includeDescriptionLookup) {
        this.includeDescriptionLookup = includeDescriptionLookup;
    }
}
