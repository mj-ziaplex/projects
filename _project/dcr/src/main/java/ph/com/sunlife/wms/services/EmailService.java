package ph.com.sunlife.wms.services;

import java.util.List;

import ph.com.sunlife.wms.services.bo.ContactBO;
import ph.com.sunlife.wms.services.bo.WMSEmail;
import ph.com.sunlife.wms.services.exception.ServiceException;

/**
 * Service Interface for the email facility present in WMS DCR.
 * 
 * @author Zainal Limpao
 * @author Crisseljess Mendoza
 * 
 */
public interface EmailService {

	/**
	 * Sends out given {@link WMSEmail} based on an smtp mail senderi
	 * implementation.
	 * 
	 * @param email
	 * @return
	 * @throws ServiceException
	 */
	boolean sendEmail(WMSEmail email) throws ServiceException;

	/**
	 * Get list of {@link ContactBO}.
	 * 
	 * @return
	 * @throws ServiceException
	 */
	List<ContactBO> getAddressBook() throws ServiceException;
	
	boolean sendSimpleMessage(WMSEmail email) throws ServiceException;

}
