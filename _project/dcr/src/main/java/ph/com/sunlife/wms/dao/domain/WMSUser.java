/**
 * 
 */
package ph.com.sunlife.wms.dao.domain;

import java.io.Serializable;
import java.util.Date;

/**
 * @author i176
 * 
 */
public class WMSUser implements Serializable {

	private static final long serialVersionUID = 2254865098444790215L;

	private String wmsuId;
	private Boolean wmsuActive;
	private Date wmsuExpPwdDate;
	private String wmsuName;
	private String wmsuPwd;
	private String wmsuCreUser;
	private Date wmsuCreDate;
	private String wmsuUpdUser;
	private Date wmsuUpdDate;
	private String wmsuShortName;
	private String wmsuLongName;
	private Date wmsdcrLastLogin;
	private Date wmsdcrLastlogout;
	private Boolean wmsdcrActive;

	public String getWmsuId() {
		return wmsuId;
	}

	public void setWmsuId(String wmsuId) {
		this.wmsuId = wmsuId;
	}

	public Boolean getWmsuActive() {
		return wmsuActive;
	}

	public void setWmsuActive(Boolean wmsuActive) {
		this.wmsuActive = wmsuActive;
	}

	public Date getWmsuExpPwdDate() {
		return wmsuExpPwdDate;
	}

	public void setWmsuExpPwdDate(Date wmsuExpPwdDate) {
		this.wmsuExpPwdDate = wmsuExpPwdDate;
	}

	public String getWmsuName() {
		return wmsuName;
	}

	public void setWmsuName(String wmsuName) {
		this.wmsuName = wmsuName;
	}

	public String getWmsuPwd() {
		return wmsuPwd;
	}

	public void setWmsuPwd(String wmsuPwd) {
		this.wmsuPwd = wmsuPwd;
	}

	public String getWmsuCreUser() {
		return wmsuCreUser;
	}

	public void setWmsuCreUser(String wmsuCreUser) {
		this.wmsuCreUser = wmsuCreUser;
	}

	public Date getWmsuCreDate() {
		return wmsuCreDate;
	}

	public void setWmsuCreDate(Date wmsuCreDate) {
		this.wmsuCreDate = wmsuCreDate;
	}

	public String getWmsuUpdUser() {
		return wmsuUpdUser;
	}

	public void setWmsuUpdUser(String wmsuUpdUser) {
		this.wmsuUpdUser = wmsuUpdUser;
	}

	public Date getWmsuUpdDate() {
		return wmsuUpdDate;
	}

	public void setWmsuUpdDate(Date wmsuUpdDate) {
		this.wmsuUpdDate = wmsuUpdDate;
	}

	public String getWmsuShortName() {
		return wmsuShortName;
	}

	public void setWmsuShortName(String wmsuShortName) {
		this.wmsuShortName = wmsuShortName;
	}

	public String getWmsuLongName() {
		return wmsuLongName;
	}

	public void setWmsuLongName(String wmsuLongName) {
		this.wmsuLongName = wmsuLongName;
	}

	public Date getWmsdcrLastLogin() {
		return wmsdcrLastLogin;
	}

	public void setWmsdcrLastLogin(Date wmsdcrLastLogin) {
		this.wmsdcrLastLogin = wmsdcrLastLogin;
	}

	public Date getWmsdcrLastlogout() {
		return wmsdcrLastlogout;
	}

	public void setWmsdcrLastlogout(Date wmsdcrLastlogout) {
		this.wmsdcrLastlogout = wmsdcrLastlogout;
	}

	public Boolean getWmsdcrActive() {
		return wmsdcrActive;
	}

	public void setWmsdcrActive(Boolean wmsdcrActive) {
		this.wmsdcrActive = wmsdcrActive;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "WMSUser [wmsuId=" + wmsuId + ", wmsuActive=" + wmsuActive
				+ ", wmsuExpPwdDate=" + wmsuExpPwdDate + ", wmsuName="
				+ wmsuName + ", wmsuPwd=" + wmsuPwd + ", wmsuCreUser="
				+ wmsuCreUser + ", wmsuCreDate=" + wmsuCreDate
				+ ", wmsuUpdUser=" + wmsuUpdUser + ", wmsuUpdDate="
				+ wmsuUpdDate + ", wmsuShortName=" + wmsuShortName
				+ ", wmsuLongName=" + wmsuLongName + ", wmsdcrLastLogin="
				+ wmsdcrLastLogin + ", wmsdcrLastlogout=" + wmsdcrLastlogout
				+ ", wmsdcrActive=" + wmsdcrActive + "]";
	}

}
