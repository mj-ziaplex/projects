package ph.com.sunlife.wms.services.bo;

public class Denomination {

	private String name;
	private String value;
	
	public Denomination(String name, String value) {
		this.name = name;
		this.value = value;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	
	
	
}
