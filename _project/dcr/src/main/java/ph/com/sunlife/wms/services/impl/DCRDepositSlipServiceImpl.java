/*
 * ** Date: Feb 2016
 * ** Dev: Cshells Sayan
 * ** Desc: Added new product ADA(MF22) in conditions in filtering data and total collection accordingly 
 * 
 * ** Date: May 2017
 * ** Dev: Cshells Sayan
 * ** Desc: Modified methods for a more dynamic approach; SR-WF-16-00016 - SLAMC - New USD Fund (WMS-DCR)
 */
package ph.com.sunlife.wms.services.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import ph.com.sunlife.wms.dao.CashierDao;
import ph.com.sunlife.wms.dao.DCRBalancingToolDao;
import ph.com.sunlife.wms.dao.DCRCashDepositSlipDao;
import ph.com.sunlife.wms.dao.DCRChequeDepositSlipDao;
import ph.com.sunlife.wms.dao.IpacDao;
import ph.com.sunlife.wms.dao.domain.Bank;
import ph.com.sunlife.wms.dao.domain.Cashier;
import ph.com.sunlife.wms.dao.domain.CheckType;
import ph.com.sunlife.wms.dao.domain.Company;
import ph.com.sunlife.wms.dao.domain.Currency;
import ph.com.sunlife.wms.dao.domain.DCRCashDepositSlip;
import ph.com.sunlife.wms.dao.domain.DCRCashier;
import ph.com.sunlife.wms.dao.domain.DCRChequeDepositSlip;
import ph.com.sunlife.wms.dao.domain.ProductType;
import ph.com.sunlife.wms.dao.exceptions.WMSDaoException;
import ph.com.sunlife.wms.integration.DCRFilenetIntegrationService;
import ph.com.sunlife.wms.integration.exceptions.IntegrationException;
import ph.com.sunlife.wms.services.DCRDepositSlipService;
import ph.com.sunlife.wms.services.bo.BankBO;
import ph.com.sunlife.wms.services.bo.DCRBalancingToolProductBO;
import ph.com.sunlife.wms.services.bo.DCRCashDepositSlipBO;
import ph.com.sunlife.wms.services.bo.DCRCashierBO;
import ph.com.sunlife.wms.services.bo.DCRChequeDepositSlipBO;
import ph.com.sunlife.wms.services.exception.ServiceException;
import ph.com.sunlife.wms.util.IpacUtil;
import ph.com.sunlife.wms.util.ParameterMap;

public class DCRDepositSlipServiceImpl implements DCRDepositSlipService {

    private static final Logger LOGGER = Logger.getLogger(DCRDepositSlipServiceImpl.class);
    private static final String PDF_EXTENSION = ".pdf";
    private DCRCashDepositSlipDao dcrCashDepositSlipDao;
    private DCRChequeDepositSlipDao dcrChequeDepositSlipDao;
    private IpacDao ipacDao;
    private CashierDao cashierDao;
    private Map<String, String> bankFullNames;
    private DCRFilenetIntegrationService dcrFilenetIntegrationService;

    public void setBankFullNames(Map<String, String> bankFullNames) {
        this.bankFullNames = bankFullNames;
    }

    public String getCCNameById(String siteCode) throws ServiceException {
        try {
            return cashierDao.getCCNameById(siteCode);
        } catch (WMSDaoException e) {
            throw new ServiceException(e);
        }
    }

    public String getCashierNameByAcf2id(String acf2id) throws ServiceException {
        try {
            Cashier cashier = cashierDao.getCashier(acf2id);
            return cashier.getLastname() + ", " + cashier.getFirstname() + " "
                    + cashier.getMiddlename();
        } catch (WMSDaoException e) {
            throw new ServiceException(e);
        }
    }

    public DCRCashDepositSlipBO getSlocpiBankDetails(String centerCode,
            DCRCashDepositSlipBO bo) throws ServiceException {
        DCRCashDepositSlip entity = new DCRCashDepositSlip();
        entity.setCompany(Company.SLOCPI);
        entity.setCurrency(bo.getCurrency());
        try {
            entity = ipacDao.populateBankAccount(centerCode, entity);

            Bank depositoryBank = entity.getDepositoryBank();
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("Depository Bank ID " + depositoryBank.getId());
                LOGGER.debug("Depository Bank Name " + depositoryBank.getName());
            }

            BankBO bankBO = new BankBO();
            bankBO.setId(entity.getDepositoryBank().getId());
            bo.setAccountNumber(entity.getAccountNumber());
            
            String bankName = bankBO.getName();
            if (StringUtils.isNotEmpty(bankName)) {
                bo.setDepositoryBankId(bankName);
            } else {
                bo.setDepositoryBankId(bankFullNames.get(bankBO.getId()));
            }

            return bo;
        } catch (WMSDaoException e) {
            throw new ServiceException(e);
        }

    }

    public DCRChequeDepositSlipBO getSlocpiBankDetails(String centerCode,
            DCRChequeDepositSlipBO bo) throws ServiceException {
        DCRCashDepositSlip entity = new DCRCashDepositSlip();
        entity.setCompany(Company.SLOCPI);
        entity.setCurrency(bo.getCurrency());
        try {
            entity = ipacDao.populateBankAccount(centerCode, entity);

            Bank depositoryBank = entity.getDepositoryBank();
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("Depository Bank ID " + depositoryBank.getId());
                LOGGER.debug("Depository Bank Name " + depositoryBank.getName());
            }

            BankBO bankBO = new BankBO();
            bankBO.setId(entity.getDepositoryBank().getId());
            bo.setAccountNumber(entity.getAccountNumber());

            String bankName = bankBO.getName();
            if (StringUtils.isNotEmpty(bankName)) {
                bo.setDepositoryBankId(bankName);
            } else {
                bo.setDepositoryBankId(bankFullNames.get(bankBO.getId()));
            }

            return bo;
        } catch (WMSDaoException e) {
            throw new ServiceException(e);
        }

    }

    public DCRCashDepositSlipBO getIpacValuesForDisplay(String centerCode,
            DCRCashDepositSlipBO bo) throws ServiceException {

        try {
            DCRCashDepositSlip entity = new DCRCashDepositSlip();
            entity.setCompany(bo.getCompany());
            entity.setCurrency(bo.getCurrency());
            entity = ipacDao.populateBankAccount(centerCode, entity);

            Bank depositoryBank = entity.getDepositoryBank();

            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("Depository Bank ID " + depositoryBank.getId());
                LOGGER.debug("Depository Bank Name " + depositoryBank.getName());
            }

            BankBO bankBO = new BankBO();
            bankBO.setId(depositoryBank.getId());
            bo.setDepositoryBank(bankBO);

            String bankName = bankBO.getName();
            if (StringUtils.isNotEmpty(bankName)) {
                bo.setDepositoryBankId(bankName);
            } else {
                bo.setDepositoryBankId(bankFullNames.get(bankBO.getId()));
            }

            bo.setAccountNumber(entity.getAccountNumber());
        } catch (WMSDaoException e) {
            throw new ServiceException(e);
        }

        return bo;
    }

    public DCRChequeDepositSlipBO getIpacValuesForDisplay(String centerCode,
            DCRChequeDepositSlipBO bo) throws ServiceException {

        try {
            DCRChequeDepositSlip entity = new DCRChequeDepositSlip();
            entity.setCompany(bo.getCompany());
            entity.setCurrency(bo.getCurrency());
            entity.setCheckType(bo.getCheckType());
            entity = ipacDao.populateBankAccount(centerCode, entity);

            Bank depositoryBank = entity.getDepositoryBank();
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("Depository Bank ID " + depositoryBank.getId());
                LOGGER.debug("Depository Bank Name " + depositoryBank.getName());
            }

            BankBO bankBO = new BankBO();
            bankBO.setId(depositoryBank.getId());
            bo.setDepositoryBank(bankBO);

            String bankName = bankBO.getName();
            if (StringUtils.isNotEmpty(bankName)) {
                bo.setDepositoryBankId(bankName);
            } else {
                bo.setDepositoryBankId(bankFullNames.get(bankBO.getId()));
            }

            bo.setAccountNumber(entity.getAccountNumber());
        } catch (WMSDaoException e) {
            throw new ServiceException(e);
        }

        return bo;
    }

    public void saveCashDepositSlip(Map<String, Object> map)
            throws ServiceException {
        DCRCashDepositSlipBO businessObject = new DCRCashDepositSlipBO();
        try {
            BeanUtils.populate(businessObject, map);
        } catch (IllegalAccessException e) {
            throw new ServiceException(e);
        } catch (InvocationTargetException e) {
            throw new ServiceException(e);
        }
        saveCashDepositSlip(businessObject);
    }

    @Override
    public void saveCashDepositSlip(DCRCashDepositSlipBO businessObject)
            throws ServiceException {

        DCRCashDepositSlip entity = new DCRCashDepositSlip();

        entity.setAccountNumber(businessObject.getAccountNumber());
        entity.setTotalCashAmount(businessObject.getTotalCashAmount());
        entity.setRequiredTotalAmount(businessObject.getRequiredTotalAmount());
        entity.setDepositDate(businessObject.getDepositDate());
        entity.setDcrDate(businessObject.getDcrDate());

        Bank bank = new Bank();
        bank.setId(businessObject.getDepositoryBank().getId());
        entity.setDepositoryBank(bank);

        DCRCashier dcrCashier = new DCRCashier();
        dcrCashier.setId(businessObject.getDcrCashier().getId());
        entity.setDcrCashier(dcrCashier);

        entity.setCustomerCenter(businessObject.getCustomerCenter());
        entity.setCompanyId(businessObject.getCompanyId());
        entity.setCurrencyId(businessObject.getCurrencyId());
        entity.setDcrDepoSlipVersionSerialId(businessObject
                .getDcrDepoSlipVersionSerialId());
        entity.setProdCode(businessObject.getProdCode());

        entity.setDenom1000(businessObject.getDenom1000());
        entity.setDenom500(businessObject.getDenom500());
        entity.setDenom200(businessObject.getDenom200());
        entity.setDenom100(businessObject.getDenom100());
        entity.setDenom50(businessObject.getDenom50());
        entity.setDenom20(businessObject.getDenom20());
        entity.setDenom10(businessObject.getDenom10());
        entity.setDenom5(businessObject.getDenom5());
        entity.setDenom2(businessObject.getDenom2());
        entity.setDenom1(businessObject.getDenom1());
        entity.setDenom1Coin(businessObject.getDenom1Coin());
        entity.setDenom50c(businessObject.getDenom50c());
        entity.setDenom25c(businessObject.getDenom25c());
        entity.setDenom10c(businessObject.getDenom10c());
        entity.setDenom5c(businessObject.getDenom5c());
        entity.setDenom1c(businessObject.getDenom1c());

        // compute total amount
        double totalCashAmount = 0.0;
        if (entity.getCurrencyId() == Currency.PHP.getId()) {

            totalCashAmount = (1000.0 * businessObject.getDenom1000())
                    + (500.0 * businessObject.getDenom500())
                    + (200.0 * businessObject.getDenom200())
                    + (100.0 * businessObject.getDenom100())
                    + (50.0 * businessObject.getDenom50())
                    + (20.0 * businessObject.getDenom20())
                    + (10.0 * businessObject.getDenom10())
                    + (5.0 * businessObject.getDenom5())
                    + (1.0 * businessObject.getDenom1())
                    + (0.25 * businessObject.getDenom25c())
                    + (0.10 * businessObject.getDenom10c())
                    + (0.05 * businessObject.getDenom5c())
                    + (0.01 * businessObject.getDenom1c());

        } else if (entity.getCurrencyId() == Currency.USD.getId()) {

            totalCashAmount = (100.0 * businessObject.getDenom100())
                    + (50.0 * businessObject.getDenom50())
                    + (20.0 * businessObject.getDenom20())
                    + (10.0 * businessObject.getDenom10())
                    + (5.0 * businessObject.getDenom5())
                    + (2.0 * businessObject.getDenom2())
                    + (1.0 * businessObject.getDenom1());

        }
        entity.setTotalCashAmount(totalCashAmount);

        try {

            String verId = null;
            byte[] fileData = businessObject.getDeposlipFile();

            if (fileData != null) {
                Date dcrDate = businessObject.getDcrDate();
                String docCapsiteId = businessObject.getCustomerCenter();
                String userId = businessObject.getUserId();

                Company company = Company.getCompany(businessObject
                        .getCompanyId());

                Currency currency = Currency.getCurrency(businessObject
                        .getCurrencyId());

                String prodCode = "";
                if (Company.SLAMCID != company && Company.SLAMCIP != company) {
                    prodCode = "_" + businessObject.getProdCode();
                }

                String customFileName = company.getName() + "_"
                        + currency.getName() + prodCode + "_CashDepositSlip_"
                        + StringUtils.upperCase(userId) + PDF_EXTENSION;

                verId = dcrFilenetIntegrationService.attachFileToWorkItem(
                        dcrDate, docCapsiteId, customFileName, fileData);
            }

            if (verId != null) {
                entity.setDcrDepoSlipVersionSerialId(verId);
            }

            entity = dcrCashDepositSlipDao.save(entity);
            businessObject.setId(entity.getId());
        } catch (WMSDaoException e) {
            throw new ServiceException(e);
        } catch (IntegrationException e) {
            throw new ServiceException(e);
        }
    }

    public DCRCashDepositSlipBO getCashDepositSlip(Long dcrCashierId,
            Long currencyId, Long companyId, String prodCode)
            throws ServiceException {

        DCRCashDepositSlip example = new DCRCashDepositSlip();
        example.setDcrCashier(dcrCashierId);
        example.setCurrency(Currency.getCurrency(currencyId));
        example.setCompanyId(companyId);
        example.setProdCode(prodCode);

        DCRCashDepositSlipBO businessObject = null;
        try {
            DCRCashDepositSlip entity = dcrCashDepositSlipDao
                    .getCashDepositSlipByExample(example);

            if (entity != null) {
                businessObject = new DCRCashDepositSlipBO();
                businessObject.setId(entity.getId());
                businessObject.setAccountNumber(entity.getAccountNumber());
                businessObject.setTotalCashAmount(entity.getTotalCashAmount());
                businessObject.setRequiredTotalAmount(entity
                        .getRequiredTotalAmount());
                businessObject.setCustomerCenter(entity.getCustomerCenter());

                BankBO bankBO = new BankBO();
                bankBO.setId(entity.getDepositoryBank().getId());
                businessObject.setDepositoryBank(bankBO);

                DCRCashierBO dcrCashierBO = new DCRCashierBO();
                dcrCashierBO.setId(entity.getDcrCashier().getId());
                businessObject.setDcrCashier(dcrCashierBO);

                businessObject.setCompanyId(entity.getCompanyId());
                businessObject.setCompanyStr(IpacUtil
                        .toIpacFullCompanyName(entity.getCompany().getName()));
                businessObject.setCurrencyId(entity.getCurrencyId());
                businessObject.setDcrDepoSlipVersionSerialId(entity
                        .getDcrDepoSlipVersionSerialId());
                businessObject.setDepositDate(entity.getDepositDate());
                businessObject.setDcrDate(entity.getDcrDate());
                businessObject.setProdCode(entity.getProdCode());
                businessObject.setCustomerCenter(entity.getCustomerCenter());

                businessObject.setDenom1000(entity.getDenom1000());
                businessObject.setDenom500(entity.getDenom500());
                businessObject.setDenom200(entity.getDenom200());
                businessObject.setDenom100(entity.getDenom100());
                businessObject.setDenom50(entity.getDenom50());
                businessObject.setDenom20(entity.getDenom20());
                businessObject.setDenom10(entity.getDenom10());
                businessObject.setDenom5(entity.getDenom5());
                businessObject.setDenom2(entity.getDenom2());
                businessObject.setDenom1(entity.getDenom1());
                businessObject.setDenom1Coin(entity.getDenom1Coin());
                businessObject.setDenom50c(entity.getDenom50c());
                businessObject.setDenom25c(entity.getDenom25c());
                businessObject.setDenom10c(entity.getDenom10c());
                businessObject.setDenom5c(entity.getDenom5c());
                businessObject.setDenom1c(entity.getDenom1c());

            }

        } catch (WMSDaoException e) {
            throw new ServiceException(e);
        }

        return businessObject;
    }

    public void saveChequeDepositSlip(Map<String, Object> map)
            throws ServiceException {
        DCRChequeDepositSlipBO businessObject = new DCRChequeDepositSlipBO();
        try {
            BeanUtils.populate(businessObject, map);
        } catch (IllegalAccessException e) {
            throw new ServiceException(e);
        } catch (InvocationTargetException e) {
            throw new ServiceException(e);
        }
        saveChequeDepositSlip(businessObject);
    }

    public void saveChequeDepositSlip(DCRChequeDepositSlipBO businessObject)
            throws ServiceException {

        DCRChequeDepositSlip entity = new DCRChequeDepositSlip();
        entity.setAccountNumber(businessObject.getAccountNumber());
        entity.setTotalChequeAmount(businessObject.getTotalCheckAmount());
        entity.setRequiredTotalAmount(businessObject.getRequiredTotalAmount());
        entity.setDepositDate(businessObject.getDepositDate());
        entity.setDcrDate(businessObject.getDcrDate());
        entity.setCustomerCenter(businessObject.getCustomerCenter());

        Bank bank = new Bank();
        bank.setId(businessObject.getDepositoryBank().getId());
        entity.setDepositoryBank(bank);

        DCRCashier dcrCashier = new DCRCashier();
        dcrCashier.setId(businessObject.getDcrCashier().getId());
        entity.setDcrCashier(dcrCashier);

        entity.setCompanyId(businessObject.getCompanyId());
        entity.setCurrencyId(businessObject.getCurrencyId());
        entity.setDcrDepoSlipVersionSerialId(businessObject
                .getDcrDepoSlipVersionSerialId());

        entity.setCheckType(businessObject.getCheckType());
        entity.setProdCode(businessObject.getProdCode());

        try {
            String verId = null;
            byte[] fileData = businessObject.getDeposlipFile();

            if (fileData != null) {
                Date dcrDate = businessObject.getDcrDate();
                String docCapsiteId = businessObject.getCustomerCenter();
                String userId = businessObject.getUserId();

                Company company = Company.getCompany(businessObject
                        .getCompanyId());
                CheckType checkType = businessObject.getCheckType();

                String checkTypeName = StringUtils.replace(
                        StringUtils.trim(checkType.getCheckTypeName()), " ",
                        "_");
                String prodCode = "";
                if (Company.SLAMCID != company && Company.SLAMCIP != company) {
                    prodCode = "_" + businessObject.getProdCode();
                }
                String customFileName = company.getName() + "_" + checkTypeName
                        + prodCode
                        + "_ChequeDepositSlip_" + StringUtils.upperCase(userId)
                        + PDF_EXTENSION;

                verId = dcrFilenetIntegrationService.attachFileToWorkItem(
                        dcrDate, docCapsiteId, customFileName, fileData);
            }

            if (verId != null) {
                entity.setDcrDepoSlipVersionSerialId(verId);
            }

            entity = dcrChequeDepositSlipDao.save(entity);
            businessObject.setId(entity.getId());
        } catch (WMSDaoException e) {
            throw new ServiceException(e);
        } catch (IntegrationException e) {
            throw new ServiceException(e);
        }
    }

    public DCRChequeDepositSlipBO getChequeDepositSlip(Long dcrCashierId,
            Long currencyId, Long companyId, CheckType checkType,
            String prodCode) throws ServiceException {

        DCRChequeDepositSlip example = new DCRChequeDepositSlip();
        example.setDcrCashier(dcrCashierId);
        example.setCurrency(Currency.getCurrency(currencyId));
        example.setCompanyId(companyId);
        example.setCheckType(checkType);
        example.setProdCode(prodCode);
        DCRChequeDepositSlipBO businessObject = null;
        try {
            DCRChequeDepositSlip entity = dcrChequeDepositSlipDao
                    .getChequeDepositSlip(example);

            if (entity != null) {

                businessObject = new DCRChequeDepositSlipBO();
                businessObject.setId(entity.getId());
                businessObject.setAccountNumber(entity.getAccountNumber());
                businessObject.setTotalCheckAmount(entity
                        .getTotalChequeAmount());
                businessObject.setRequiredTotalAmount(entity
                        .getRequiredTotalAmount());

                BankBO bankBO = new BankBO();
                bankBO.setId(entity.getDepositoryBank().getId());
                businessObject.setDepositoryBank(bankBO);

                DCRCashierBO dcrCashierBO = new DCRCashierBO();
                dcrCashierBO.setId(entity.getDcrCashier().getId());
                businessObject.setDcrCashier(dcrCashierBO);
                businessObject.setCustomerCenter(entity.getCustomerCenter());

                businessObject.setCompanyId(entity.getCompanyId());
                businessObject.setCompanyStr(IpacUtil
                        .toIpacFullCompanyName(entity.getCompany().getName()));
                businessObject.setCurrencyId(entity.getCurrencyId());
                businessObject.setDcrDepoSlipVersionSerialId(entity
                        .getDcrDepoSlipVersionSerialId());
                businessObject.setDepositDate(entity.getDepositDate());
                businessObject.setDcrDate(entity.getDcrDate());

                businessObject.setCheckType(entity.getCheckType());
                businessObject.setProdCode(entity.getProdCode());

            }

        } catch (WMSDaoException e) {
            throw new ServiceException(e);
        }

        return businessObject;
    }

    public ParameterMap buildMapForFundBreakdownsSlamciPesoCash(
            List<DCRBalancingToolProductBO> products, ParameterMap map) {

        for (DCRBalancingToolProductBO product : products) {
            double amount;
            if (ProductType.SLAMCI_MF1.equals(product.getProductType())) {
                amount = product.getTotalCashCounter();
                map.put(ProductType.SLAMCI_MF1.getProductCode(), amount);
            } else if (ProductType.SLAMCI_MF2.equals(product.getProductType())) {
                amount = product.getTotalCashCounter();
                map.put(ProductType.SLAMCI_MF2.getProductCode(), amount);
            } else if (ProductType.SLAMCI_MF3.equals(product.getProductType())) {
                amount = product.getTotalCashCounter();
                map.put(ProductType.SLAMCI_MF3.getProductCode(), amount);
            } else if (ProductType.SLAMCI_MF5.equals(product.getProductType())) {
                amount = product.getTotalCashCounter();
                map.put(ProductType.SLAMCI_MF5.getProductCode(), amount);
            } else if (ProductType.SLAMCI_MF7.equals(product.getProductType())) {
                amount = product.getTotalCashCounter();
                map.put(ProductType.SLAMCI_MF7.getProductCode(), amount);
            } else if (ProductType.SLAMCI_MF8.equals(product.getProductType())) {
                amount = product.getTotalCashCounter();
                map.put(ProductType.SLAMCI_MF8.getProductCode(), amount);
            } else if (ProductType.SLAMCI_MF9.equals(product.getProductType())) {
                amount = product.getTotalCashCounter();
                map.put(ProductType.SLAMCI_MF9.getProductCode(), amount);
            } else if (ProductType.SLAMCI_MF20.equals(product.getProductType())) {
                amount = product.getTotalCashCounter();
                map.put(ProductType.SLAMCI_MF20.getProductCode(), amount);
            } else if (ProductType.SLAMCI_ADA.equals(product.getProductType())) { // Added for ADA
                amount = product.getTotalCashCounter();
                map.put(ProductType.SLAMCI_ADA.getProductCode(), amount);
            }
        }
        return map;
    }

    public ParameterMap buildMapForFundBreakdownsSlamciPesoCheque(
            List<DCRBalancingToolProductBO> products, ParameterMap map) {
        if (CheckType.ON_US.equals(map.get("checkType"))) {
            map = buildMapForFundBreakdownsSlamciPesoCheckOnUs(products, map);
        } else if (CheckType.LOCAL.equals(map.get("checkType"))) {
            map = buildMapForFundBreakdownsSlamciPesoCheckLocal(products, map);
        } else if (CheckType.REGIONAL.equals(map.get("checkType"))) {
            map = buildMapForFundBreakdownsSlamciPesoCheckRegional(products, map);
        } else if (CheckType.OT.equals(map.get("checkType"))) {
            map = buildMapForFundBreakdownsSlamciPesoCheckOT(products, map);
        }

        return map;

    }

    private ParameterMap buildMapForFundBreakdownsSlamciPesoCheckOnUs(
            List<DCRBalancingToolProductBO> products, ParameterMap map) {

        for (DCRBalancingToolProductBO product : products) {
            double amount;
            if (ProductType.SLAMCI_MF1.equals(product.getProductType())) {
                amount = product.getTotalCheckOnUs();
                map.put(ProductType.SLAMCI_MF1.getProductCode(), amount);
            } else if (ProductType.SLAMCI_MF2.equals(product.getProductType())) {
                amount = product.getTotalCheckOnUs();
                map.put(ProductType.SLAMCI_MF2.getProductCode(), amount);
            } else if (ProductType.SLAMCI_MF3.equals(product.getProductType())) {
                amount = product.getTotalCheckOnUs();
                map.put(ProductType.SLAMCI_MF3.getProductCode(), amount);
            } else if (ProductType.SLAMCI_MF5.equals(product.getProductType())) {
                amount = product.getTotalCheckOnUs();
                map.put(ProductType.SLAMCI_MF5.getProductCode(), amount);
            } else if (ProductType.SLAMCI_MF7.equals(product.getProductType())) {
                amount = product.getTotalCheckOnUs();
                map.put(ProductType.SLAMCI_MF7.getProductCode(), amount);
            } else if (ProductType.SLAMCI_MF8.equals(product.getProductType())) {
                amount = product.getTotalCheckOnUs();
                map.put(ProductType.SLAMCI_MF8.getProductCode(), amount);
            } else if (ProductType.SLAMCI_MF9.equals(product.getProductType())) {
                amount = product.getTotalCheckOnUs();
                map.put(ProductType.SLAMCI_MF9.getProductCode(), amount);
            } else if (ProductType.SLAMCI_MF20.equals(product.getProductType())) {
                amount = product.getTotalCheckOnUs();
                map.put(ProductType.SLAMCI_MF20.getProductCode(), amount);
            } else if (ProductType.SLAMCI_ADA.equals(product.getProductType())) { // Added for ADA
                amount = product.getTotalCheckOnUs();
                map.put(ProductType.SLAMCI_ADA.getProductCode(), amount);
            }
        }
        return map;
    }

    private ParameterMap buildMapForFundBreakdownsSlamciPesoCheckLocal(
            List<DCRBalancingToolProductBO> products, ParameterMap map) {

        for (DCRBalancingToolProductBO product : products) {
            double amount;
            if (ProductType.SLAMCI_MF1.equals(product.getProductType())) {
                amount = product.getTotalCheckLocal();
                map.put(ProductType.SLAMCI_MF1.getProductCode(), amount);
            } else if (ProductType.SLAMCI_MF2.equals(product.getProductType())) {
                amount = product.getTotalCheckLocal();
                map.put(ProductType.SLAMCI_MF2.getProductCode(), amount);
            } else if (ProductType.SLAMCI_MF3.equals(product.getProductType())) {
                amount = product.getTotalCheckLocal();
                map.put(ProductType.SLAMCI_MF3.getProductCode(), amount);
            } else if (ProductType.SLAMCI_MF5.equals(product.getProductType())) {
                amount = product.getTotalCheckLocal();
                map.put(ProductType.SLAMCI_MF5.getProductCode(), amount);
            } else if (ProductType.SLAMCI_MF7.equals(product.getProductType())) {
                amount = product.getTotalCheckLocal();
                map.put(ProductType.SLAMCI_MF7.getProductCode(), amount);
            } else if (ProductType.SLAMCI_MF8.equals(product.getProductType())) {
                amount = product.getTotalCheckLocal();
                map.put(ProductType.SLAMCI_MF8.getProductCode(), amount);
            } else if (ProductType.SLAMCI_MF9.equals(product.getProductType())) {
                amount = product.getTotalCheckLocal();
                map.put(ProductType.SLAMCI_MF9.getProductCode(), amount);
            } else if (ProductType.SLAMCI_MF20.equals(product.getProductType())) {
                amount = product.getTotalCheckLocal();
                map.put(ProductType.SLAMCI_MF20.getProductCode(), amount);
            } else if (ProductType.SLAMCI_ADA.equals(product.getProductType())) { // Added for ADA
                amount = product.getTotalCheckLocal();
                map.put(ProductType.SLAMCI_ADA.getProductCode(), amount);
            }
        }
        return map;
    }

    private ParameterMap buildMapForFundBreakdownsSlamciPesoCheckRegional(
            List<DCRBalancingToolProductBO> products, ParameterMap map) {

        for (DCRBalancingToolProductBO product : products) {
            double amount;
            if (ProductType.SLAMCI_MF1.equals(product.getProductType())) {
                amount = product.getTotalCheckRegional();
                map.put(ProductType.SLAMCI_MF1.getProductCode(), amount);
            } else if (ProductType.SLAMCI_MF2.equals(product.getProductType())) {
                amount = product.getTotalCheckRegional();
                map.put(ProductType.SLAMCI_MF2.getProductCode(), amount);
            } else if (ProductType.SLAMCI_MF3.equals(product.getProductType())) {
                amount = product.getTotalCheckRegional();
                map.put(ProductType.SLAMCI_MF3.getProductCode(), amount);
            } else if (ProductType.SLAMCI_MF5.equals(product.getProductType())) {
                amount = product.getTotalCheckRegional();
                map.put(ProductType.SLAMCI_MF5.getProductCode(), amount);
            } else if (ProductType.SLAMCI_MF7.equals(product.getProductType())) {
                amount = product.getTotalCheckRegional();
                map.put(ProductType.SLAMCI_MF7.getProductCode(), amount);
            } else if (ProductType.SLAMCI_MF8.equals(product.getProductType())) {
                amount = product.getTotalCheckRegional();
                map.put(ProductType.SLAMCI_MF8.getProductCode(), amount);
            } else if (ProductType.SLAMCI_MF9.equals(product.getProductType())) {
                amount = product.getTotalCheckRegional();
                map.put(ProductType.SLAMCI_MF9.getProductCode(), amount);
            } else if (ProductType.SLAMCI_MF20.equals(product.getProductType())) {
                amount = product.getTotalCheckRegional();
                map.put(ProductType.SLAMCI_MF20.getProductCode(), amount);
            } else if (ProductType.SLAMCI_ADA.equals(product.getProductType())) { // Added for ADA
                amount = product.getTotalCheckRegional();
                map.put(ProductType.SLAMCI_ADA.getProductCode(), amount);
            }
        }
        return map;
    }

    private ParameterMap buildMapForFundBreakdownsSlamciPesoCheckOT(
            List<DCRBalancingToolProductBO> products, ParameterMap map) {

        for (DCRBalancingToolProductBO product : products) {
            double amount;
            if (ProductType.SLAMCI_MF1.equals(product.getProductType())) {
                amount = product.getTotalCheckOT();
                map.put(ProductType.SLAMCI_MF1.getProductCode(), amount);
            } else if (ProductType.SLAMCI_MF2.equals(product.getProductType())) {
                amount = product.getTotalCheckOT();
                map.put(ProductType.SLAMCI_MF2.getProductCode(), amount);
            } else if (ProductType.SLAMCI_MF3.equals(product.getProductType())) {
                amount = product.getTotalCheckOT();
                map.put(ProductType.SLAMCI_MF3.getProductCode(), amount);
            } else if (ProductType.SLAMCI_MF5.equals(product.getProductType())) {
                amount = product.getTotalCheckOT();
                map.put(ProductType.SLAMCI_MF5.getProductCode(), amount);
            } else if (ProductType.SLAMCI_MF7.equals(product.getProductType())) {
                amount = product.getTotalCheckOT();
                map.put(ProductType.SLAMCI_MF7.getProductCode(), amount);
            } else if (ProductType.SLAMCI_MF8.equals(product.getProductType())) {
                amount = product.getTotalCheckOT();
                map.put(ProductType.SLAMCI_MF8.getProductCode(), amount);
            } else if (ProductType.SLAMCI_MF9.equals(product.getProductType())) {
                amount = product.getTotalCheckOT();
                map.put(ProductType.SLAMCI_MF9.getProductCode(), amount);
            } else if (ProductType.SLAMCI_MF20.equals(product.getProductType())) {
                amount = product.getTotalCheckOT();
                map.put(ProductType.SLAMCI_MF20.getProductCode(), amount);
            } else if (ProductType.SLAMCI_ADA.equals(product.getProductType())) { // Added for ADA
                amount = product.getTotalCheckOT();
                map.put(ProductType.SLAMCI_ADA.getProductCode(), amount);
            }
        }
        return map;
    }

    /*
     * Modified for SR-WF-16-00016 - SLAMC - New USD Fund (WMS-DCR)
     * - Modified if-else condition to a more dynamic approach
     */
    public ParameterMap buildMapForFundBreakdownsSlamciDollarCash(
            List<DCRBalancingToolProductBO> products, ParameterMap map) {
    	
    	for (DCRBalancingToolProductBO product : products) {
            String prodCode = ProductType.getById(product.getProductType().getId()).getProductCode();
            double amount = product.getTotalCashCounter();
            map.put(prodCode, amount);
        }
    	
        return map;
    }

    @Override
    public ParameterMap buildMapForFundBreakdownsSlamciDollarCheque(
            List<DCRBalancingToolProductBO> products, ParameterMap map) {

        if (CheckType.DOLLARCHECK.equals(map.get("checkType"))) {
            map = buildMapForFundBreakdownsSlamciDollarCheck(products, map);
        } else if (CheckType.USCD_MANILA.equals(map.get("checkType"))) {
            map = buildMapForFundBreakdownsSlamciDollarUSCheckInManila(
                    products, map);
        } else if (CheckType.USCD_OUTSIDEPH.equals(map.get("checkType"))) {
            map = buildMapForFundBreakdownsSlamciDollarUSCheckOutsidePH(
                    products, map);
        } else if (CheckType.BANK_OTC.equals(map.get("checkType"))) {
            map = buildMapForFundBreakdownsSlamciDollarBankOTC(products, map);
        }

        return map;
    }

    /*
     * Modified for SR-WF-16-00016 - SLAMC - New USD Fund (WMS-DCR)
     * - Modified if-else condition to a more dynamic approach
     */
    private ParameterMap buildMapForFundBreakdownsSlamciDollarCheck(
            List<DCRBalancingToolProductBO> products, ParameterMap map) {
    	
    	for (DCRBalancingToolProductBO product : products) {
            String prodCode = ProductType.getById(product.getProductType().getId()).getProductCode();
            double amount = product.getTotalDollarCheque();
            map.put(prodCode, amount);
        }
    	
        return map;
    }

    /*
     * Modified for SR-WF-16-00016 - SLAMC - New USD Fund (WMS-DCR)
     * - Modified if-else condition to a more dynamic approach
     */
    private ParameterMap buildMapForFundBreakdownsSlamciDollarUSCheckInManila(
            List<DCRBalancingToolProductBO> products, ParameterMap map) {
    	
    	for (DCRBalancingToolProductBO product : products) {
            String prodCode = ProductType.getById(product.getProductType().getId()).getProductCode();
            double amount = product.getTotalUsCheckInManila();
            map.put(prodCode, amount);
        }
    	
        return map;
    }

    /*
     * Modified for SR-WF-16-00016 - SLAMC - New USD Fund (WMS-DCR)
     * - Modified if-else condition to a more dynamic approach
     */
    private ParameterMap buildMapForFundBreakdownsSlamciDollarUSCheckOutsidePH(
            List<DCRBalancingToolProductBO> products, ParameterMap map) {
    	
    	for (DCRBalancingToolProductBO product : products) {
            String prodCode = ProductType.getById(product.getProductType().getId()).getProductCode();
            double amount = product.getTotalUsCheckOutPh();
            map.put(prodCode, amount);
        }
    	
        return map;
    }

    /*
     * Modified for SR-WF-16-00016 - SLAMC - New USD Fund (WMS-DCR)
     * - Modified if-else condition to a more dynamic approach
     */
    private ParameterMap buildMapForFundBreakdownsSlamciDollarBankOTC(
            List<DCRBalancingToolProductBO> products, ParameterMap map) {
    	
    	for (DCRBalancingToolProductBO product : products) {
            String prodCode = ProductType.getById(product.getProductType().getId()).getProductCode();
            double amount = product.getTotalBankOTCCheckPayment();
            map.put(prodCode, amount);
        }
    	
        return map;
    }

    public void setDcrCashDepositSlipDao(
            DCRCashDepositSlipDao dcrCashDepositSlipDao) {
        this.dcrCashDepositSlipDao = dcrCashDepositSlipDao;
    }

    public void setDcrChequeDepositSlipDao(
            DCRChequeDepositSlipDao dcrChequeDepositSlipDao) {
        this.dcrChequeDepositSlipDao = dcrChequeDepositSlipDao;
    }

    public void setIpacDao(IpacDao ipacDao) {
        this.ipacDao = ipacDao;
    }

    public void setDcrFilenetIntegrationService(
            DCRFilenetIntegrationService dcrFilenetIntegrationService) {
        this.dcrFilenetIntegrationService = dcrFilenetIntegrationService;
    }

    public void setCashierDao(CashierDao cashierDao) {
        this.cashierDao = cashierDao;
    }

    public void setDcrBalancingToolDao(DCRBalancingToolDao dcrBalancingToolDao) {
    }
}
