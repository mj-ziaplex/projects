package ph.com.sunlife.wms.dao.domain;

import java.util.Date;

public class DCRDollarCoinExchange extends Entity {
	
	private DCRVDIL dcrvdil = new DCRVDIL();
	
	private Date dateCreated;
	
	private int incomingDenom1;
	
	private int incomingDenom50c;
	
	private int incomingDenom25c;
	
	private int incomingDenom10c;
	
	private int incomingDenom5c;
	
	private int incomingDenom1c;
	
	private int outgoingDenom1;
	
	private int outgoingDenom50c;
	
	private int outgoingDenom25c;
	
	private int outgoingDenom10c;
	
	private int outgoingDenom5c;
	
	private int outgoingDenom1c;

	public DCRVDIL getDcrvdil() {
		return dcrvdil;
	}

	public void setDcrvdil(DCRVDIL dcrvdil) {
		this.dcrvdil = dcrvdil;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public int getIncomingDenom1() {
		return incomingDenom1;
	}

	public void setIncomingDenom1(int incomingDenom1) {
		this.incomingDenom1 = incomingDenom1;
	}

	public int getIncomingDenom50c() {
		return incomingDenom50c;
	}

	public void setIncomingDenom50c(int incomingDenom50c) {
		this.incomingDenom50c = incomingDenom50c;
	}

	public int getIncomingDenom25c() {
		return incomingDenom25c;
	}

	public void setIncomingDenom25c(int incomingDenom25c) {
		this.incomingDenom25c = incomingDenom25c;
	}

	public int getIncomingDenom10c() {
		return incomingDenom10c;
	}

	public void setIncomingDenom10c(int incomingDenom10c) {
		this.incomingDenom10c = incomingDenom10c;
	}

	public int getIncomingDenom5c() {
		return incomingDenom5c;
	}

	public void setIncomingDenom5c(int incomingDenom5c) {
		this.incomingDenom5c = incomingDenom5c;
	}

	public int getIncomingDenom1c() {
		return incomingDenom1c;
	}

	public void setIncomingDenom1c(int incomingDenom1c) {
		this.incomingDenom1c = incomingDenom1c;
	}

	public int getOutgoingDenom1() {
		return outgoingDenom1;
	}

	public void setOutgoingDenom1(int outgoingDenom1) {
		this.outgoingDenom1 = outgoingDenom1;
	}

	public int getOutgoingDenom50c() {
		return outgoingDenom50c;
	}

	public void setOutgoingDenom50c(int outgoingDenom50c) {
		this.outgoingDenom50c = outgoingDenom50c;
	}

	public int getOutgoingDenom25c() {
		return outgoingDenom25c;
	}

	public void setOutgoingDenom25c(int outgoingDenom25c) {
		this.outgoingDenom25c = outgoingDenom25c;
	}

	public int getOutgoingDenom10c() {
		return outgoingDenom10c;
	}

	public void setOutgoingDenom10c(int outgoingDenom10c) {
		this.outgoingDenom10c = outgoingDenom10c;
	}

	public int getOutgoingDenom5c() {
		return outgoingDenom5c;
	}

	public void setOutgoingDenom5c(int outgoingDenom5c) {
		this.outgoingDenom5c = outgoingDenom5c;
	}

	public int getOutgoingDenom1c() {
		return outgoingDenom1c;
	}

	public void setOutgoingDenom1c(int outgoingDenom1c) {
		this.outgoingDenom1c = outgoingDenom1c;
	}
	
}
