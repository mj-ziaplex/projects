package ph.com.sunlife.wms.services.bo;

import java.util.ArrayList;
import java.util.List;

import ph.com.sunlife.wms.security.Role;

/**
 * The holder object for various user-related information that will persist
 * through out the scope of the HttpSession.
 * 
 * @author Zainal Limpao
 */
public class UserSession {

	private String userId;
	
	private String password;

	private String siteCode;

	private String hubId;

	private List<Role> roles;

	private CashierBO cashierBO;

	private List<DCRIpacValueBO> ipacValues;
	
	private List<String> validHubs;
	
	private String adminUserId;
	
	public List<String> getValidHubs() {
		return validHubs;
	}

	public void setValidHubs(List<String> validHubs) {
		this.validHubs = validHubs;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getSiteCode() {
		return siteCode;
	}

	public void setSiteCode(String siteCode) {
		this.siteCode = siteCode;
	}

	public List<Role> getRoles() {
		return roles;
	}

	public void setRoles(List<Role> roles) {
		this.roles = roles;
	}

	public void addRole(Role role) {
		if (this.roles == null) {
			this.roles = new ArrayList<Role>();
		}

		this.roles.add(role);
	}

	public CashierBO getCashierBO() {
		return cashierBO;
	}

	public void setCashierBO(CashierBO cashierBO) {
		this.cashierBO = cashierBO;
	}

	public String getHubId() {
		return hubId;
	}

	public void setHubId(String hubId) {
		this.hubId = hubId;
	}

	public List<DCRIpacValueBO> getIpacValues() {
		return ipacValues;
	}

	public void setIpacValues(List<DCRIpacValueBO> ipacValues) {
		this.ipacValues = ipacValues;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getAdminUserId() {
		return adminUserId;
	}

	public void setAdminUserId(String adminUserId) {
		this.adminUserId = adminUserId;
	}
	
}
