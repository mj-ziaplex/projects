package ph.com.sunlife.wms.web.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.web.servlet.ModelAndView;

import ph.com.sunlife.wms.security.Role;
import ph.com.sunlife.wms.services.AttachmentService;
import ph.com.sunlife.wms.services.bo.AttachmentBO;
import ph.com.sunlife.wms.services.exception.InvalidFileTypeException;
import ph.com.sunlife.wms.services.exception.ServiceException;
import ph.com.sunlife.wms.web.controller.form.CashierWorkItemForm;
import ph.com.sunlife.wms.web.controller.form.UploadForm;
import ph.com.sunlife.wms.web.exception.ApplicationException;

/**
 * The Controller responsible for the "File Upload" page of the Cashier Work
 * Item.
 * 
 * @author Zainal Limpao
 * 
 */
public class UploadController extends AbstractCashierWorkItemController {

	private static final Logger LOGGER = Logger.getLogger(UploadController.class);
	
	private static final String TITLE_KEY = "title";

	private static final String ERROR_MESSAGE_KEY = "errorMessage";

	private static final String ATTACHMENTS_KEY = "attachments";

	private static final String VIEW_NAME = "uploadFilesView";

	private static final String PAGE_TITLE = "Upload Files";

	/**
	 * The displays the default Upload page.
	 * 
	 * @param request
	 * @param response
	 * @param form
	 * @return
	 * @throws ApplicationException
	 */
	public ModelAndView showUploadPage(HttpServletRequest request,
			HttpServletResponse response, CashierWorkItemForm form)
			throws ApplicationException {
		LOGGER.info("Loading the File Upload Page...");
		try {
			List<AttachmentBO> attachments = attachmentService
					.getAttachmentsByDcrCashierId(form.getDcrCashierId());

			ModelAndView modelAndView = new ModelAndView();
			modelAndView.setViewName(VIEW_NAME);
			modelAndView.addObject(TITLE_KEY, getPageTitle());
			modelAndView.addObject(ATTACHMENTS_KEY, attachments);
			modelAndView.addObject(ERROR_MESSAGE_KEY, "&nbsp;");
			return modelAndView;
		} catch (ServiceException e) {
			throw new ApplicationException(e);
		}

	}

    /**
     * The method that actually performs the uploading of the selected file.
     *
     * @param request
     * @param response
     * @param form
     * @return
     * @throws ApplicationException
     */
    public ModelAndView upload(
            HttpServletRequest request,
            HttpServletResponse response,
            UploadForm form) throws ApplicationException {
        
        LOGGER.info("File is being uploaded...");
        ModelAndView modelAndView = null;
        try {
            Long dcrCashierId = form.getDcrCashierId();
            String type = form.getType();
            String description = form.getDescription();
            byte[] file = form.getFile();

            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("DCR Cashier ID: " + dcrCashierId);
                LOGGER.debug("Type: " + type);
                LOGGER.debug("File Description: " + description);
                if (file != null) {
                    LOGGER.debug("File Size: " + file.length);
                }
            }

            attachmentService.uploadOtherFileAttachment(dcrCashierId, description, type, file);
            LOGGER.info("Upload Successful...");
            
            modelAndView = showUploadPage(request, response, form);
        } catch (InvalidFileTypeException e) {
            // Change according to Healthcheck
            // *Problem was identified as inconsistent logging
            // *Change was added logging with object exception
            LOGGER.error("Error in uploading File is Invalid: ", e);
            modelAndView = showUploadPage(request, response, form);
            modelAndView.addObject(ERROR_MESSAGE_KEY, e.getMessage());
        } catch (ServiceException e) {
            // Change according to Healthcheck
            // *Problem was identified as inconsistent logging
            // *Change was added logging with object exception
            LOGGER.error("Error in uploading File Service Exception: ", e);
            throw new ApplicationException(e);
        }

        return modelAndView;
    }

	@Override
	protected String getPageTitle() {
		return PAGE_TITLE;
	}

	@Override
	protected Role[] allowedRoles() {
		return new Role[] { Role.CASHIER };
	}

	public void setAttachmentService(AttachmentService attachmentService) {
		this.attachmentService = attachmentService;
	}

}
