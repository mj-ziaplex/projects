package ph.com.sunlife.wms.dao.impl;

import java.util.List;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import ph.com.sunlife.wms.dao.CollectionDiscrepancyReportDao;
import ph.com.sunlife.wms.dao.domain.DiscrepancyReportEntry;
import ph.com.sunlife.wms.dao.domain.StartEndMonth;
import ph.com.sunlife.wms.dao.exceptions.WMSDaoException;

public class CollectionDiscrepancyReportDaoImpl extends SqlMapClientDaoSupport
		implements CollectionDiscrepancyReportDao {

	private static final String NAMESPACE = "CollectionDiscrepancyReport";

	@SuppressWarnings("unchecked")
	@Override
	public List<DiscrepancyReportEntry> getTimelinessDiscrepancyReports(
			StartEndMonth param) throws WMSDaoException {
		List<DiscrepancyReportEntry> result = null;

		try {
			result = getSqlMapClientTemplate().queryForList(
					NAMESPACE + ".getTimelinessDiscrepancyReports", param);
			return result;
		} catch (Exception e) {
			throw new WMSDaoException(e);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<DiscrepancyReportEntry> getAccuracyDiscrepancyReports(
			StartEndMonth param) throws WMSDaoException {
		List<DiscrepancyReportEntry> result = null;

		try {
			result = getSqlMapClientTemplate().queryForList(
					NAMESPACE + ".getAccuracyDiscrepancyReports", param);
			return result;
		} catch (Exception e) {
			throw new WMSDaoException(e);
		}
	}
}
