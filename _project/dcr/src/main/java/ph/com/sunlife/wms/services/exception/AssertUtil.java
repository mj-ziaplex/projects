package ph.com.sunlife.wms.services.exception;

public class AssertUtil {

	public static void assertNotNullOnService(Object obj)
			throws ServiceException {
		if (obj == null) {
			throw new ServiceException("This value can't be null");
		}
	}
}
