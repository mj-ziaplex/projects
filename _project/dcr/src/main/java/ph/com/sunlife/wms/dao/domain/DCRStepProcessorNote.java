package ph.com.sunlife.wms.dao.domain;

public class DCRStepProcessorNote extends Entity {

	private Long dcrId;
	
	private int rowNum;
	
	private String noteType;
	
	private String noteContent;
	
	private String acf2id;

	public Long getDcrId() {
		return dcrId;
	}

	public void setDcrId(Long dcrId) {
		this.dcrId = dcrId;
	}

	public int getRowNum() {
		return rowNum;
	}

	public void setRowNum(int rowNum) {
		this.rowNum = rowNum;
	}

	public String getNoteType() {
		return noteType;
	}

	public void setNoteType(String noteType) {
		this.noteType = noteType;
	}

	public String getNoteContent() {
		return noteContent;
	}

	public void setNoteContent(String noteContent) {
		this.noteContent = noteContent;
	}

	public String getAcf2id() {
		return acf2id;
	}

	public void setAcf2id(String acf2id) {
		this.acf2id = acf2id;
	}
	
	
}
