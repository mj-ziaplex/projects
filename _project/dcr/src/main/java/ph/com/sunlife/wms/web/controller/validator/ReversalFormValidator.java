package ph.com.sunlife.wms.web.controller.validator;

import org.apache.commons.lang.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import ph.com.sunlife.wms.web.controller.form.ReversalForm;

/**
 * The {@link Validator} for Reversal Form Entries. F
 * 
 * @author Zainal Limpao
 * 
 */
public class ReversalFormValidator implements Validator {

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.validation.Validator#supports(java.lang.Class)
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public boolean supports(Class clazz) {
		return ReversalForm.class.isAssignableFrom(clazz);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.validation.Validator#validate(java.lang.Object,
	 * org.springframework.validation.Errors)
	 */
	@Override
	public void validate(Object target, Errors errors) {
		ReversalForm form = (ReversalForm) target;

		double amount = form.getAmount();
		String remarks = form.getRemarks();

		// Ensures that Remarks is only within 350 character length.
		if (StringUtils.length(remarks) > 350) {
			errors.rejectValue("remarks",
					"error.reversal.remarks.abovemaxlength");
		}

		// Ensures that amount is a non-zero.
//		if (amount == 0.0) {
//			errors.rejectValue("amount", "error.reversal.amount.iszero");
//		}
		
		if (amount > 99999999.99) {
			errors.rejectValue("amount", "error.reversal.amount.abovemaxlength");
		}
	}

}
