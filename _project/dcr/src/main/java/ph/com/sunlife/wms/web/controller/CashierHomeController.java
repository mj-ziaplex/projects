package ph.com.sunlife.wms.web.controller;

import static ph.com.sunlife.wms.util.WMSConstants.HAS_CASHIER_ADHOC_MENU;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.View;
import org.springframework.web.servlet.view.RedirectView;

import ph.com.sunlife.wms.security.Role;
import ph.com.sunlife.wms.services.DCRCreationService;
import ph.com.sunlife.wms.services.DCRSearchService;
import ph.com.sunlife.wms.services.bo.CustomerCenterBO;
import ph.com.sunlife.wms.services.bo.DCRBO;
import ph.com.sunlife.wms.services.bo.DCRCashierBO;
import ph.com.sunlife.wms.services.bo.HubBO;
import ph.com.sunlife.wms.services.bo.UserSession;
import ph.com.sunlife.wms.services.exception.ServiceException;
import ph.com.sunlife.wms.util.WMSConstants;
import ph.com.sunlife.wms.web.controller.form.CashierHomePageForm;
import ph.com.sunlife.wms.web.controller.form.ManagerPageForm;
import ph.com.sunlife.wms.web.exception.ApplicationException;

/**
 * The default controller for a Cashier who has successfully logged in.
 * 
 * @author Zainal Limpao
 * 
 */
public class CashierHomeController extends AbstractSecuredMultiActionController {

	private static final String TITLE = "title";

	private static final String ALL_MANAGER_DCR_KEY = "allManagerDcr";

	private static final String CUSTOMER_CENTER_KEY = "customerCenters";

	private static final String CASHIER_HOMEPAGE_VIEW = "homeViewPage";

	private static final String MANAGER_HOMEPAGE_VIEW = "managerPage";

	private static final String FILTER_KEY = "filter";

	private static final String DCR_WORK_ITEMS = "dcrBOList";

	private static final String CASHIER_WORK_ITEMS = "list";

	private static final String ALL_KEY = "ALL";

	private static final String DCR_KEY = "dcrObjects";

	private static final String SEARCH_ON_LOAD = "searchOnLoad";

	private static final String HUB_KEY = "hub";

	private static final String IS_PPA_KEY = "isPPA";

	private static final Logger LOGGER = Logger
			.getLogger(CashierHomeController.class);

	private DCRCreationService dcrCreationService;

	private DCRSearchService dcrSearchService;

	public void setDcrSearchService(DCRSearchService dcrSearchService) {
		this.dcrSearchService = dcrSearchService;
	}

	/**
	 * The handler method for the cashier home page.
	 * 
	 * @param request
	 * @param response
	 * @param form
	 * @return {@link ModelAndView} object of Spring MVC.
	 */
	public ModelAndView doShowHomePage(HttpServletRequest request,
			HttpServletResponse response, CashierHomePageForm form)
			throws Exception {

		LOGGER.debug("Initializing home page...");

		UserSession userSession = this.getUserSession(request);

		String acf2id = userSession.getUserId();
		String ccId = userSession.getSiteCode();
		String hubId = userSession.getHubId();
		List<Role> roles = userSession.getRoles();

		ModelAndView modelAndView = null;

		if (CollectionUtils.isNotEmpty(roles)) {
			if (roles.contains(Role.CASHIER)) {
				modelAndView = this.showCashierHomePage(acf2id, ccId);
			} else if (roles.contains(Role.MANAGER)) {
				modelAndView = this.showManagerHomePage(acf2id, ccId, hubId,
						request);
			} else if (roles.contains(Role.PPA)) {
				modelAndView = this.showPPAHomePage(acf2id, ccId, hubId,
						request);
			} else if (roles.contains(Role.OTHER)) {
				View view = new RedirectView("/search/index.html", true);
				modelAndView = new ModelAndView(view);
			}
		}

		return modelAndView;
	}

	private ModelAndView showManagerHomePage(String acf2id, String ccId,
			String hubId, HttpServletRequest request) throws Exception {
		ModelAndView modelAndView = new ModelAndView();
		List<CustomerCenterBO> customerCentersForHub = new ArrayList<CustomerCenterBO>();

		try {
			UserSession userSession = this.getUserSession(request);

			modelAndView.addObject(HUB_KEY, userSession.getHubId());
			customerCentersForHub = dcrSearchService
					.getCCByHubUserCC(userSession.getSiteCode());
			modelAndView.addObject(CUSTOMER_CENTER_KEY, customerCentersForHub);

		} catch (ServiceException e) {
			LOGGER.error(e);
			throw new ApplicationException(e);
		}
		modelAndView.addObject(SEARCH_ON_LOAD, true);
		modelAndView.addObject(WMSConstants.HAS_CASHIER_ADHOC_MENU,
				Boolean.FALSE);
		modelAndView.addObject(TITLE, "DCR Home Page");
		modelAndView.addObject(IS_PPA_KEY, false);
		modelAndView.setViewName(MANAGER_HOMEPAGE_VIEW);
		return modelAndView;
	}

	private ModelAndView showPPAHomePage(String acf2id, String ccId,
			String hubId, HttpServletRequest request) throws Exception {
		ModelAndView modelAndView = this.showManagerHomePage(acf2id, ccId,
				hubId, request);
		List<HubBO> hubList = dcrSearchService.getCCHubs();
		List<CustomerCenterBO> ccList = new ArrayList<CustomerCenterBO>();
		for (HubBO h : hubList) {
			ccList.addAll(dcrSearchService.getCCByHub(h.getHubId()));
		}

		Collections.sort(ccList);

		modelAndView.addObject(CUSTOMER_CENTER_KEY, ccList);
		modelAndView.addObject(IS_PPA_KEY, true);
		return modelAndView;
	}

	@SuppressWarnings("unchecked")
	public ModelAndView doShowManagerWI(
                HttpServletRequest request,
		HttpServletResponse response,
                ManagerPageForm form) throws Exception {

            LOGGER.info("Start getting values for manager view method ...");
            
		ModelAndView modelAndView = this.doShowHomePage(request, response, form);
		List<DCRBO> dcrWorkItems = new ArrayList<DCRBO>();
		try {
			DCRBO paramBO = new DCRBO();
			List<String> ccIdSet = new ArrayList<String>();
			if (ALL_KEY.equalsIgnoreCase(form.getCcId())) {
				List<CustomerCenterBO> customerCenterBO =
                                        (List<CustomerCenterBO>) modelAndView.getModel().get(CUSTOMER_CENTER_KEY);
				
                                for (CustomerCenterBO ccBO : customerCenterBO) {
					ccIdSet.add(ccBO.getCcId());
				}
			} else {
				ccIdSet.add(form.getCcId());
			}
			paramBO.setCcIdSet(ccIdSet);
			paramBO.setDcrEndDate(form.getDcrEndDate());
			paramBO.setDcrStartDate(form.getDcrStartDate());
                        
			// Added for MR-WF-16-00034 - Random sampling for QA
			boolean isPpa = Boolean.parseBoolean(request.getParameter("isPpa"));
			boolean isPpaQa = Boolean.parseBoolean(request.getParameter("isPpaQa"));
                        
			if (ALL_MANAGER_DCR_KEY.equals(form.getDcrDateStr())) {
				dcrWorkItems = dcrSearchService.getDCRManagerByCC(ccIdSet);
			}
			// Added for MR-WF-16-00034 - Random sampling for QA
			else if (isPpa && isPpaQa && ALL_MANAGER_DCR_KEY.equals(form.getDcrDateStr())) {
				dcrWorkItems = dcrSearchService.getDCRQa(paramBO);
			} else if (isPpa && isPpaQa && !ALL_MANAGER_DCR_KEY.equals(form.getDcrDateStr())) {
				dcrWorkItems = dcrSearchService.getDCRQaByCC(ccIdSet);
			} else 
				// Edited for MR-WF-16-00112
			{
				dcrWorkItems = dcrSearchService.getDCRManagerByCCAndDate(paramBO, isPpa);
			}
                        
			final String status = form.getStatus();
			if (StringUtils.isNotEmpty(status) && !StringUtils.equalsIgnoreCase("ALL", status)) {
				CollectionUtils.filter(dcrWorkItems, new Predicate() {
					@Override
					public boolean evaluate(Object object) {
						DCRBO thisObj = (DCRBO) object;
						return StringUtils.equalsIgnoreCase(status, thisObj.getStatus());
					}
				});
			}
                        
			modelAndView.addObject(DCR_KEY, dcrWorkItems);
			modelAndView.addObject(IS_PPA_KEY, false);
			modelAndView.addObject(FILTER_KEY, form.getFilterSelect());
		} catch (ServiceException e) {
			LOGGER.error(e);
		}

		List<String> statusList = new ArrayList<String>();
		statusList.add("For Review and Approval");
		statusList.add("Awaiting Feedback");
		Collections.sort(statusList);

                modelAndView.addObject(SEARCH_ON_LOAD, false);
		modelAndView.addObject("statusList", statusList);

                LOGGER.info("End getting values for manager view method ...");
		return modelAndView;
	}

	public ModelAndView doShowPPAWI(
                HttpServletRequest request,
		HttpServletResponse response,
                ManagerPageForm form) throws Exception {
            
            LOGGER.info("Start getting manager WI then set PPA status list method ...");

		ModelAndView modelAndView = this.doShowManagerWI(request, response, form);

		List<String> statusList = new ArrayList<String>();
		statusList.add("Approved for Recon");
		statusList.add("Awaiting Reqts");
		statusList.add("Reqts Submitted");
		statusList.add("Reqts Not Submitted");
		statusList.add("For Verification");
		statusList.add("Not Verified");
		statusList.add("Verified");

		Collections.sort(statusList);
                
		// Added for MR-WF-16-00034 - Random sampling for QA
		boolean isPpaQa = Boolean.parseBoolean(request.getParameter("isPpaQa"));

		// Added for MR-WF-16-00034 - Random sampling for QA
		modelAndView.addObject("isPpaQa", isPpaQa);
		modelAndView.addObject(IS_PPA_KEY, true);
		modelAndView.addObject("statusList", statusList);

		LOGGER.info("End getting manager WI then set PPA status list method ...");
		return modelAndView;
	}

	/**
	 * Creates a {@link ModelAndView} for Users with Role as Cashier.
	 * 
	 * @param acf2id
	 * @param centerCode
	 * @return {@link ModelAndView}
	 */
	private ModelAndView showCashierHomePage(String acf2id, String centerCode)
			throws Exception {
		LOGGER.info("Loading Home Page for Cashier...");

		List<DCRCashierBO> cashierWorkItems = null;
		List<DCRBO> dcrWorkItems = null;

		try {
			cashierWorkItems = dcrCreationService.createAndLoadDcrWorkItems(
					centerCode, acf2id);
			dcrWorkItems = dcrCreationService.getAllDcrWorkItems(centerCode);
		} catch (ServiceException e) {
			LOGGER.error("A problem has occured in loading work items", e);
			throw new ApplicationException(e);
		}

		this.refreshCashierWOrkItems(cashierWorkItems, dcrWorkItems);

		ModelAndView modelAndView = new ModelAndView();
		modelAndView.addObject(CASHIER_WORK_ITEMS, cashierWorkItems);
		modelAndView.addObject(DCR_WORK_ITEMS, dcrWorkItems);
		modelAndView.addObject(HAS_CASHIER_ADHOC_MENU, Boolean.FALSE);
		modelAndView.addObject(TITLE, "DCR Home Page");

		modelAndView.setViewName(CASHIER_HOMEPAGE_VIEW);
		return modelAndView;
	}

	@SuppressWarnings("unchecked")
	void refreshCashierWOrkItems(List<DCRCashierBO> cashierWorkItems,
			List<DCRBO> dcrWorkItems) {
		if (CollectionUtils.isNotEmpty(cashierWorkItems)) {
			List<DCRCashierBO> toBeRemoved = new ArrayList<DCRCashierBO>();
			for (DCRCashierBO cashierWorkItem : cashierWorkItems) {
				final Long dcrId = cashierWorkItem.getDcr().getId();

				DCRBO dcr = (DCRBO) CollectionUtils.find(dcrWorkItems,
						new Predicate() {
							@Override
							public boolean evaluate(Object object) {
								DCRBO thisDCR = (DCRBO) object;
								return (dcrId.equals(thisDCR.getId()));
							}
						});

				if (dcr == null) {
					// cashierWorkItems.remove(cashierWorkItem);
					toBeRemoved.add(cashierWorkItem);
				}
			}
			if (CollectionUtils.isNotEmpty(toBeRemoved)) {
				cashierWorkItems = (List<DCRCashierBO>) CollectionUtils
						.removeAll(cashierWorkItems, toBeRemoved);
			}
		}

	}

	/**
	 * Injection setter for {@link DCRCreationService}.
	 * 
	 * @param dcrCreationService
	 */
	public void setDcrCreationService(DCRCreationService dcrCreationService) {
		this.dcrCreationService = dcrCreationService;
	}

    /*
     * (non-Javadoc)
     * 
     * @see
     * ph.com.sunlife.wms.web.controller.AbstractSecuredMultiActionController
     * #allowedRoles()
     */
    @Override
    protected Role[] allowedRoles() {
        // Change according to Healthcheck
        // Problem was due to changes in AbstractSecuredMultiActionController
        // Change was use parent getter method instead of inherited values
        return super.getALL_ROLES();
    }

}
