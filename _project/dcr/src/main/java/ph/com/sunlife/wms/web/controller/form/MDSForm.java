package ph.com.sunlife.wms.web.controller.form;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.commons.collections.FactoryUtils;
import org.apache.commons.collections.ListUtils;
import org.springframework.validation.Errors;

import ph.com.sunlife.wms.services.bo.DCRPPAMDSBO;

// Change according to Healthcheck
// *Problem was declared as public/protected in BinderAware
// *Change BinderAware interface class to abstract class and extends it rather than implements
public class MDSForm extends PPAMDSForm {

	private HttpSession session;
	
	@SuppressWarnings("unchecked")
	private List<DCRPPAMDSBO> ppamdsRecons = ListUtils.lazyList(
			new ArrayList<DCRPPAMDSBO>(),
			FactoryUtils.instantiateFactory(DCRPPAMDSBO.class));
	
	@SuppressWarnings("unchecked")
	private List<DCRPPAMDSBO> ppamdsUnrecons = ListUtils.lazyList(
			new ArrayList<DCRPPAMDSBO>(),
			FactoryUtils.instantiateFactory(DCRPPAMDSBO.class));

	public List<DCRPPAMDSBO> getPpamdsRecons() {
		return ppamdsRecons;
	}

	public void setPpamdsRecons(List<DCRPPAMDSBO> ppamdsRecons) {
		this.ppamdsRecons = ppamdsRecons;
	}

	public List<DCRPPAMDSBO> getPpamdsUnrecons() {
		return ppamdsUnrecons;
	}

	public void setPpamdsUnrecons(List<DCRPPAMDSBO> ppamdsUnrecons) {
		this.ppamdsUnrecons = ppamdsUnrecons;
	}

    // Change according to Healthcheck
    // *Problem was declared as public/protected in BinderAware
    // *Change use BinderAware getter instead of direct access
    public String[] getAllowedFields() {
        return super.getAllowedFields();
    }

    // Change according to Healthcheck
    // *Problem was declared as public/protected in BinderAware
    // *Change use BinderAware getter instead of direct access
    public String[] getRequiredFields() {
        return super.getRequiredFields();
    }

    // Change according to Healthcheck
    // *Problem was declared as public/protected in BinderAware
    // *Change use BinderAware getter instead of direct access
    public String[] getUncheckedFields() {
        return super.getUncheckedFields();
    }

	@Override
	public void afterBind() {
	}

	@Override
	public boolean validate(Errors errors) {
		return false;
	}

	public HttpSession getSession() {
		return session;
	}

	public void setSession(HttpSession session) {
		this.session = session;
	}

	public String getSecretKey() {
		return null;
	}

}
