package ph.com.sunlife.wms.dao.impl;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.orm.ibatis.SqlMapClientCallback;

import ph.com.sunlife.wms.dao.AbstractWMSSqlMapClientDaoSupport;
import ph.com.sunlife.wms.dao.DCRDao;
import ph.com.sunlife.wms.dao.domain.CashierConfirmation;
import ph.com.sunlife.wms.dao.domain.Contact;
import ph.com.sunlife.wms.dao.domain.CustomerCenter;
import ph.com.sunlife.wms.dao.domain.DCR;
import ph.com.sunlife.wms.dao.domain.DCRAuditLog;
import ph.com.sunlife.wms.dao.domain.DCRNonCashierFile;
import ph.com.sunlife.wms.dao.domain.DCRStepProcessorNote;
import ph.com.sunlife.wms.dao.domain.DCRStepProcessorReconciled;
import ph.com.sunlife.wms.dao.exceptions.WMSDaoException;

import com.ibatis.sqlmap.client.SqlMapExecutor;
import java.util.HashMap;
import java.util.Map;

public class DCRDaoImpl extends AbstractWMSSqlMapClientDaoSupport<DCR>
		implements DCRDao {

	private static final int THIRD_ELEMENT = 3;

	private static final int FIFTH_ELEMENT = 5;

	private static final Logger LOGGER = Logger.getLogger(DCRDaoImpl.class);

	private static final String NAMESPACE = "DCR";

	@Override
	protected String getNamespace() {
		return NAMESPACE;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<String> getAllCenterIds() throws WMSDaoException {
		List<String> list = null;

		try {
			list = (List<String>) getSqlMapClientTemplate().execute(
					new SqlMapClientCallback() {

						@Override
						public Object doInSqlMapClient(SqlMapExecutor executor)
								throws SQLException {
							return executor.queryForList(getNamespace()
									+ ".getAllCenterCodes");
						}
					});
		} catch (Exception e) {
			throw new WMSDaoException(e);
		}
		return list;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<DCR> getDCRWorkItemsForCCCashier(final String ccId)
			throws WMSDaoException {
		List<DCR> list = null;

		try {
			list = (List<DCR>) getSqlMapClientTemplate().execute(
					new SqlMapClientCallback() {

						@Override
						public Object doInSqlMapClient(SqlMapExecutor executor)
								throws SQLException {
							return executor.queryForList(getNamespace()
									+ ".getDCRWorkItemsForCCCashier", ccId);
						}
					});
		} catch (Exception e) {
			throw new WMSDaoException(e);
		}
		return list;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<DCR> getAllDCRForTheDay(final Date dcrDate)
			throws WMSDaoException {
		List<DCR> list = null;

		try {
			list = (List<DCR>) getSqlMapClientTemplate().execute(
					new SqlMapClientCallback() {

						@Override
						public Object doInSqlMapClient(SqlMapExecutor executor)
								throws SQLException {
							return executor.queryForList(getNamespace()
									+ ".getAllDCRForTheDay", dcrDate);
						}
					});
		} catch (Exception e) {
			throw new WMSDaoException(e);
		}
		return list;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<DCR> getAllPendingDCRForCC(final String value)
			throws WMSDaoException {
		List<DCR> listofDCRs = null;

		try {
			listofDCRs = (List<DCR>) getSqlMapClientTemplate().execute(
					new SqlMapClientCallback() {

						@Override
						public Object doInSqlMapClient(SqlMapExecutor executor)
								throws SQLException {
							return executor.queryForList(getNamespace()
									+ ".getAllPendingDCRForCC", value);
						}
					});
		} catch (Exception e) {
			throw new WMSDaoException(e);
		}
		return listofDCRs;
	}

	@Override
	public String getCustomerCenterId(final String centerCode)
			throws WMSDaoException {
		String ccId = null;

		try {
			ccId = (String) getSqlMapClientTemplate().execute(
					new SqlMapClientCallback() {

						@Override
						public Object doInSqlMapClient(SqlMapExecutor executor)
								throws SQLException {
							return executor.queryForObject(getNamespace()
									+ ".getCustomerCenterId", centerCode);
						}
					});
		} catch (Exception e) {
			throw new WMSDaoException(e);
		}
		return ccId;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CustomerCenter> getCustomerCenters(final List<String> ccIds)
			throws WMSDaoException {
		List<CustomerCenter> customerCenters = null;

		try {
			customerCenters = (List<CustomerCenter>) getSqlMapClientTemplate()
					.execute(new SqlMapClientCallback() {

						@Override
						public Object doInSqlMapClient(SqlMapExecutor executor)
								throws SQLException {
							return executor.queryForList(getNamespace()
									+ ".getCustomerCenters", ccIds);
						}
					});
		} catch (Exception e) {
			throw new WMSDaoException(e);
		}
		return customerCenters;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<DCR> getDCRByCCAndDate(final DCR dcr) throws WMSDaoException {
		List<DCR> resultDCR = null;

		try {
			resultDCR = (List<DCR>) getSqlMapClientTemplate().execute(
					new SqlMapClientCallback() {

						@Override
						public Object doInSqlMapClient(SqlMapExecutor executor)
								throws SQLException {
							return executor.queryForList(getNamespace()
									+ ".getDCRByCCAndDate", dcr);
						}
					});
		} catch (Exception e) {
			throw new WMSDaoException(e);
		}
		return resultDCR;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<DCR> getDCRByHubAndDate(final DCR dcr) throws WMSDaoException {
		List<DCR> resultDCR = null;

		try {
			resultDCR = (List<DCR>) getSqlMapClientTemplate().execute(
					new SqlMapClientCallback() {

						@Override
						public Object doInSqlMapClient(SqlMapExecutor executor)
								throws SQLException {
							return executor.queryForList(getNamespace()
									+ ".getDCRByHubAndDate", dcr);
						}
					});
		} catch (Exception e) {
			throw new WMSDaoException(e);
		}
		return resultDCR;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<DCR> getAllDCRForManager(final String hubId)
			throws WMSDaoException {
		List<DCR> resultDCR = null;

		try {
			resultDCR = (List<DCR>) getSqlMapClientTemplate().execute(
					new SqlMapClientCallback() {

						@Override
						public Object doInSqlMapClient(SqlMapExecutor executor)
								throws SQLException {
							return executor.queryForList(getNamespace()
									+ ".getAllDCRForManager", hubId);
						}
					});
		} catch (Exception e) {
			throw new WMSDaoException(e);
		}
		return resultDCR;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<DCR> getDCRManagerByCCAndDate(final DCR dcr)
			throws WMSDaoException {
		List<DCR> resultDCR = null;

		try {
			resultDCR = (List<DCR>) getSqlMapClientTemplate().execute(
					new SqlMapClientCallback() {

						@Override
						public Object doInSqlMapClient(SqlMapExecutor executor)
								throws SQLException {
							return executor.queryForList(getNamespace()
									+ ".getDCRManagerByCCAndDate", dcr);
						}
					});
		} catch (Exception e) {
			throw new WMSDaoException(e);
		}
		return resultDCR;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<DCR> getDCRManagerByCC(final List<String> ccIds)
			throws WMSDaoException {
		List<DCR> resultDCR = null;

		try {
			resultDCR = (List<DCR>) getSqlMapClientTemplate().execute(
					new SqlMapClientCallback() {

						@Override
						public Object doInSqlMapClient(SqlMapExecutor executor)
								throws SQLException {
							return executor.queryForList(getNamespace()
									+ ".getDCRManagerByCC", ccIds);
						}
					});
		} catch (Exception e) {
			throw new WMSDaoException(e);
		}
		return resultDCR;
	}

	public Long saveNote(DCRStepProcessorNote note) throws WMSDaoException {
		Long noteId = null;

		try {
			noteId = (Long) getSqlMapClientTemplate().queryForObject(
					getNamespace() + ".insertNote", note);
		} catch (Exception ex) {
			throw new WMSDaoException(ex);
		}

		return noteId;
	}

	@SuppressWarnings("unchecked")
	public List<DCRStepProcessorNote> getAllNotesForDcr(Long dcrId)
			throws WMSDaoException {
		List<DCRStepProcessorNote> list = null;

		try {
			list = (List<DCRStepProcessorNote>) getSqlMapClientTemplate()
					.queryForList(getNamespace() + ".getAllNotesForDcr", dcrId);

			if (CollectionUtils.isNotEmpty(list)) {
				if (LOGGER.isDebugEnabled()) {
					LOGGER.debug("This DCR contains " + list.size()
							+ " notes/findings");
				}
				this.cleanLineSeparator(list);
			}
		} catch (Exception ex) {
			throw new WMSDaoException(ex);
		}

		return list;
	}

	private void cleanLineSeparator(List<DCRStepProcessorNote> list) {
		String separator = System.getProperty("line.separator");
		for (DCRStepProcessorNote note : list) {
			String content = note.getNoteContent();

			if (StringUtils.contains(content, separator)) {
				content = StringUtils.replace(content, "\n", "\\n");
				content = StringUtils.replace(content, "\r", "\\r");
			}
			note.setNoteContent(content);
		}
	}

	public DCRStepProcessorNote getNoteById(Long id) throws WMSDaoException {
		DCRStepProcessorNote note = null;

		try {
			note = (DCRStepProcessorNote) getSqlMapClientTemplate()
					.queryForObject(getNamespace() + ".getNoteById", id);
		} catch (Exception ex) {
			throw new WMSDaoException(ex);
		}

		return note;
	}

	@Override
	public boolean updateWorkObjectNumber(DCR dcr) throws WMSDaoException {
		int rowsAffected = 0;

		try {
			rowsAffected = getSqlMapClientTemplate().update(
					getNamespace() + ".updateWorkObjectNumber", dcr);
		} catch (Exception ex) {
			throw new WMSDaoException(ex);
		}
		return (rowsAffected != 0);
	}

	public DCRStepProcessorReconciled createDCRStepProcessorReconciled(
			DCRStepProcessorReconciled entity) throws WMSDaoException {

		try {
			Long id = (Long) getSqlMapClientTemplate().queryForObject(
					getNamespace() + ".createDCRStepProcessorReconciled",
					entity);
			entity.setId(id);
		} catch (Exception ex) {
			throw new WMSDaoException(ex);
		}

		return entity;
	}

	@SuppressWarnings("unchecked")
	public List<DCRStepProcessorReconciled> getDCRStepProcessorReconciled(
			Long dcrId) throws WMSDaoException {
		try {
			List<DCRStepProcessorReconciled> result = (List<DCRStepProcessorReconciled>) getSqlMapClientTemplate()
					.queryForList(
							getNamespace() + ".getDCRStepProcessorReconciled",
							dcrId);

			return result;
		} catch (Exception ex) {
			throw new WMSDaoException(ex);
		}
	}

	public boolean updateDCRStepProcessorReconciled(
			DCRStepProcessorReconciled entity) throws WMSDaoException {

		int rowsUpdated = 0;
		try {
			rowsUpdated = getSqlMapClientTemplate().update(
					getNamespace() + ".updateDCRStepProcessorReconciled",
					entity);
		} catch (Exception ex) {
			throw new WMSDaoException(ex);
		}

		return (rowsUpdated != 0);
	}

	@Override
	public boolean updateDcrStatus(DCR entity) throws WMSDaoException {
		int rowsUpdated = 0;
		try {
			rowsUpdated = getSqlMapClientTemplate().update(
					getNamespace() + ".updateDcrStatus", entity);
		} catch (Exception ex) {
			throw new WMSDaoException(ex);
		}

		return (rowsUpdated != 0);

	}
	
	@Override
	public boolean updateDcrStatusForCCQA(DCR entity) throws WMSDaoException {
		int rowsUpdated = 0;
		try {
			rowsUpdated = getSqlMapClientTemplate().update(
					getNamespace() + ".updateDcrStatusForCCQA", entity);
		} catch (Exception ex) {
			throw new WMSDaoException(ex);
		}

		return (rowsUpdated != 0);

	}

	public Long saveNonCashierFile(DCRNonCashierFile entity)
			throws WMSDaoException {
		Long id = null;
		try {
			id = (Long) getSqlMapClientTemplate().queryForObject(
					getNamespace() + ".saveNonCashierFile", entity);
			return id;
		} catch (Exception e) {
			throw new WMSDaoException(e);
		}
	}

	@SuppressWarnings("unchecked")
	public List<DCRNonCashierFile> getAllNonCashierFile(Long dcrId)
			throws WMSDaoException {
		try {
			List<DCRNonCashierFile> files = getSqlMapClientTemplate()
					.queryForList(getNamespace() + ".getAllNonCashierFile",
							dcrId);

			return files;
		} catch (Exception e) {
			throw new WMSDaoException(e);
		}
	}

	@Override
	public boolean deleteAllNotes(Long dcrId) throws WMSDaoException {
		int rowsDeleted = 0;

		try {
			rowsDeleted = getSqlMapClientTemplate().delete(
					getNamespace() + ".deleteAllNotes", dcrId);
		} catch (Exception e) {
			throw new WMSDaoException(e);
		}
		return (rowsDeleted != 0);
	}

	@Override
	public boolean checkIfHoliday(Date date) throws WMSDaoException {
		int count = 0;

		try {
			count = (Integer) getSqlMapClientTemplate().queryForObject(
					getNamespace() + ".checkIfHoliday", date);
		} catch (Exception e) {
			throw new WMSDaoException(e);
		}
		return (count != 0);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<CashierConfirmation> getCashierConfirmations(Long dcrId)
			throws WMSDaoException {
		try {
			List<CashierConfirmation> confirmations = getSqlMapClientTemplate()
					.queryForList(getNamespace() + ".getCashierConfirmations",
							dcrId);

			return confirmations;
		} catch (Exception e) {
			throw new WMSDaoException(e);
		}
	}

	@Override
	public Long createLog(DCRAuditLog dcrAuditLog) throws WMSDaoException {
		Long id = null;

		try {
			id = (Long) getSqlMapClientTemplate().queryForObject(
					getNamespace() + ".createLog", dcrAuditLog);
		} catch (Exception e) {
			LOGGER.error(e);
			throw new WMSDaoException(e);
		}

		return id;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<DCRAuditLog> getAuditLogsByDcrId(Long dcrId)
			throws WMSDaoException {
		List<DCRAuditLog> list = null;

		try {
			list = (List<DCRAuditLog>) getSqlMapClientTemplate().queryForList(
					getNamespace() + ".getAuditLogsByDcrId", dcrId);
		} catch (Exception e) {
			LOGGER.error(e);
			throw new WMSDaoException(e);
		}

		return list;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<DCR> getByIds(final List<Long> ids) throws WMSDaoException {

		try {
			List<DCR> list = (List<DCR>) getSqlMapClientTemplate().execute(
					new SqlMapClientCallback() {

						@Override
						public Object doInSqlMapClient(SqlMapExecutor executor)
								throws SQLException {
							return executor.queryForList(getNamespace()
									+ ".getByIds", ids);
						}
					});
			return list;
		} catch (Exception e) {
			throw new WMSDaoException(e);
		}
	}

	@SuppressWarnings("unchecked")
	public Date getDateFiveBusinessDaysAgo() throws WMSDaoException {
		try {
			List<Date> lastFiveDays = getSqlMapClientTemplate().queryForList(
					NAMESPACE + ".getLastFiveBusinessDays");
			return lastFiveDays.get(FIFTH_ELEMENT);
		} catch (Exception e) {
			throw new WMSDaoException(e);
		}
	}

	@SuppressWarnings("unchecked")
	public Date getDateThreeBusinessDaysAgo() throws WMSDaoException {
		try {
			List<Date> lastFiveDays = getSqlMapClientTemplate().queryForList(
					NAMESPACE + ".getLastThreeBusinessDays");
			return lastFiveDays.get(THIRD_ELEMENT);
		} catch (Exception e) {
			throw new WMSDaoException(e);
		}
	}

	@SuppressWarnings("unchecked")
	public Date getPreviousBusinessDate(int businessDaysAgo)
			throws WMSDaoException {
		if (businessDaysAgo < 0 || businessDaysAgo > 5) {
			return null;
		}

		try {
			List<Date> previousBusinessDate = getSqlMapClientTemplate()
					.queryForList(NAMESPACE + ".getLastFiveBusinessDays");
			return previousBusinessDate.get(businessDaysAgo);
		} catch (Exception e) {
			throw new WMSDaoException(e);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<DCR> getLapsedDCRSetForVerification(Date updateDate)
			throws WMSDaoException {
		try {
			List<DCR> lapsedDCR = getSqlMapClientTemplate().queryForList(
					NAMESPACE + ".getLapsedDCRSetForVerification", updateDate);
			return lapsedDCR;
		} catch (Exception e) {
			throw new WMSDaoException(e);
		}
	}

	@SuppressWarnings("unchecked")
	public List<DCR> getLapsedDCRSetForAwaitingReqts(Date updateDate)
			throws WMSDaoException {
		try {
			List<DCR> lapsedDCR = getSqlMapClientTemplate().queryForList(
					NAMESPACE + ".getLapsedDCRSetForAwaitingReqts", updateDate);
			return lapsedDCR;
		} catch (Exception e) {
			throw new WMSDaoException(e);
		}
	}

	public int countSubmittedNonCashierFileAfterGivenDate(DCRNonCashierFile obj)
			throws WMSDaoException {
		try {
			int count = (Integer) getSqlMapClientTemplate().queryForObject(
					NAMESPACE + ".countSubmittedNonCashierFileAfterGivenDate",
					obj);
			return count;
		} catch (Exception e) {
			throw new WMSDaoException(e);
		}
	}

	@Override
	public boolean updateDcrToPending(DCR dcr) throws WMSDaoException {
		try {
			int updatedRows = getSqlMapClientTemplate().update(
					NAMESPACE + ".updateDcrToPending", dcr);
			return updatedRows != 0 ? true : false;
		} catch (Exception ex) {
			throw new WMSDaoException(ex);
		}
	}

	@Override
	public int countHolidays(Date date) throws WMSDaoException {
		try {
			int count = (Integer) getSqlMapClientTemplate().queryForObject(
					NAMESPACE + ".countHolidays", date);
			return count;
		} catch (Exception ex) {
			throw new WMSDaoException(ex);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<DCR> getAllUnSubmittedToPPADcr() throws WMSDaoException {
		try {
			List<DCR> list = (List<DCR>) getSqlMapClientTemplate()
					.queryForList(NAMESPACE + ".getAllUnSubmittedToPPADcr");
			return list;
		} catch (Exception ex) {
			throw new WMSDaoException(ex);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Contact> getHubManagerContacts(String hubId)
			throws WMSDaoException {
		try {
			List<Contact> list = (List<Contact>) getSqlMapClientTemplate()
					.queryForList(NAMESPACE + ".getHubManagerContacts", hubId);
			return list;
		} catch (Exception ex) {
			throw new WMSDaoException(ex);
		}

	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<DCR> getDCRWorkItemsForIPACDownload() throws WMSDaoException {
		try {
			List<DCR> list = (List<DCR>) getSqlMapClientTemplate()
					.queryForList(NAMESPACE + ".getDCRWorkItemsForIPACDownload");
			return list;
		} catch (Exception ex) {
			throw new WMSDaoException(ex);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<DCR> getAllDcrForStatusUpdate() throws WMSDaoException {
		// TODO Auto-generated method stub
		List<DCR> list = null;

		try {
			list = (List<DCR>) getSqlMapClientTemplate().execute(
					new SqlMapClientCallback() {

						@Override
						public Object doInSqlMapClient(SqlMapExecutor executor)
								throws SQLException {
							return executor.queryForList(getNamespace()
									+ ".getAllDcrForStatusUpdate");
						}
					});
		} catch (Exception e) {
			throw new WMSDaoException(e);
		}
		return list;
	}
	
	@Override
	public boolean updateDCRDownloadFlag(DCR dcr) throws WMSDaoException {
		int rowsUpdated = 0;
		try {
			rowsUpdated = getSqlMapClientTemplate().update(
					getNamespace() + ".updateDCRDownloadFlag", dcr);
		} catch (Exception ex) {
			throw new WMSDaoException(ex);
		}

		return (rowsUpdated != 0);

	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<DCR> getAllDcrForStatusUpdateFromSearch(final DCR dcr) throws WMSDaoException {
		// TODO Auto-generated method stub
		List<DCR> list = null;

		try {
			list = (List<DCR>) getSqlMapClientTemplate().execute(
					new SqlMapClientCallback() {

						@Override
						public Object doInSqlMapClient(SqlMapExecutor executor)
								throws SQLException {
							return executor.queryForList(getNamespace()
									+ ".getAllDcrForStatusUpdateFromSearch", dcr);
						}
					});
		} catch (Exception e) {
			throw new WMSDaoException(e);
		}
		return list;
	}
        
	/*
     * Added for MR-WF-16-00034 - Random sampling for QA
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<DCR> getDCPpaQa(final DCR dcr) throws WMSDaoException {
        LOGGER.info("Start getting QA WI's with params Dao...");
        
        List<DCR> resultDCR = null;
        try {
            resultDCR = (List<DCR>) getSqlMapClientTemplate().execute(new SqlMapClientCallback() {
                @Override
                public Object doInSqlMapClient(SqlMapExecutor executor) throws SQLException {
                    return executor.queryForList(getNamespace() + ".getDCRPpaQa", dcr);
                }
            });
        } catch (Exception e) {
            LOGGER.error("Error in getDCPpaQa Dao: ", e);
            throw new WMSDaoException(e);
        }

        LOGGER.info("End getting QA WI's with params Dao...");
        return resultDCR;
    }

    /*
     * Added for MR-WF-16-00034 - Random sampling for QA
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<DCR> getDCRQaByCC(final List<String> ccIds) throws WMSDaoException {
        LOGGER.info("Start getting QA WI's by CC only Dao...");
        
        List<DCR> resultDCR = null;
        try {
            resultDCR = (List<DCR>) getSqlMapClientTemplate().execute(new SqlMapClientCallback() {
                @Override
                public Object doInSqlMapClient(SqlMapExecutor executor) throws SQLException {
                    return executor.queryForList(getNamespace() + ".getDCRQaByCC", ccIds);
                }
            });
        } catch (Exception e) {
            LOGGER.error("Error in getDCRQaByCC Dao: ", e);
            throw new WMSDaoException(e);
        }
        
        LOGGER.info("End getting QA WI's by CC only Dao...");
        return resultDCR;
    }
    
    /*
     * Added for MR-WF-16-00034 - Random sampling for QA
     */
    @Override
    public boolean updateQaFlag(Long dcrId, String newStatus) throws WMSDaoException {
        LOGGER.info("Start to update QA flag...");
        int qaUpdated = 0;
        
        try {
            Map<String, String> tempMap = new HashMap<String, String>();
            tempMap.put("dcrId", dcrId+"");
            tempMap.put("newStatus", newStatus);
            
            qaUpdated = getSqlMapClientTemplate().update(getNamespace() + ".updateQaFlag", tempMap);
        } catch (Exception e) {
            LOGGER.error("Error encountered when updating qa flag: ", e);
            throw new WMSDaoException(e);
        }
        
        LOGGER.info("End to update QA flag...");
        return (qaUpdated != 0);
    }
    
    //Added for MR-WF-15-00112
   	@Override
	public Integer requiredCompCount(){
		Integer count = (Integer) getSqlMapClientTemplate().queryForObject(
				NAMESPACE + ".requiredCompCount");
		return count;
	}
	
	@Override
	public boolean checkIfHolidayOrWeekend(int ccqaTAT) throws WMSDaoException {
		int count = 0;

		try {
			count = (Integer) getSqlMapClientTemplate().queryForObject(
					getNamespace() + ".checkIfHolidayOrWeekend", ccqaTAT);
		} catch (Exception e) {
			throw new WMSDaoException(e);
		}
		return (count != 0);
	}
	
   	@Override
	public Integer countHolidaysAndWeekend(int ccqaTAT){
		Integer count = (Integer) getSqlMapClientTemplate().queryForObject(
				NAMESPACE + ".countHolidaysAndWeekend", ccqaTAT);
		return count;
	}
}
