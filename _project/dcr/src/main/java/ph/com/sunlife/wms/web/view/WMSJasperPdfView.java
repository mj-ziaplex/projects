package ph.com.sunlife.wms.web.view;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperPrint;

import org.apache.log4j.Logger;
import org.springframework.web.servlet.view.AbstractView;

@SuppressWarnings("rawtypes")
public class WMSJasperPdfView extends AbstractView {

	private static final Logger LOGGER = Logger.getLogger(WMSJasperPdfView.class);
	
	public static final String JASPER_PRINT_KEY = "jasper_print";

	public WMSJasperPdfView() {
		setContentType("application/pdf");
	}

	protected void renderMergedOutputModel(Map model,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		final JasperPrint print = (JasperPrint) model.get(JASPER_PRINT_KEY);
		
		LOGGER.info("Jasper Print is being exported to Pdf Stream...");
		JasperExportManager.exportReportToPdfStream(print,
				response.getOutputStream());
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Properties of this PDF Stream");
			LOGGER.debug("Character Encoding: " + response.getCharacterEncoding());
			LOGGER.debug("Content Type: " + response.getContentType());
			LOGGER.debug("Buffer Size: " + response.getBufferSize());
			LOGGER.debug("Locale: " + response.getLocale());
		}
	}

}
