package ph.com.sunlife.wms.services.impl;

import org.apache.log4j.Logger;
import ph.com.sunlife.wms.dao.DCROtherDao;
import ph.com.sunlife.wms.dao.domain.DCROther;
import ph.com.sunlife.wms.dao.exceptions.WMSDaoException;
import ph.com.sunlife.wms.services.DCROtherService;
import ph.com.sunlife.wms.services.bo.DCROtherBO;
import ph.com.sunlife.wms.services.exception.ServiceException;

/**
 * The implementing class of {@link DCROtherService}.
 * 
 * @author Edgardo Cunanan
 * @author Zainal Limpao
 */
public class DCROtherServiceImpl implements DCROtherService {

	private static final Logger LOGGER = Logger
			.getLogger(DCROtherServiceImpl.class);

	private DCROtherDao dcrOtherDao;

	public DCROtherDao getDcrOtherDao() {
		return dcrOtherDao;
	}

	public void setDcrOtherDao(DCROtherDao dcrOtherDao) {
		this.dcrOtherDao = dcrOtherDao;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ph.com.sunlife.wms.services.DCROtherService#update(ph.com.sunlife.wms
	 * .services.bo.DCROtherBO)
	 */
	@Override
	public DCROtherBO update(DCROtherBO dcrOther) throws ServiceException {
		try {
			DCROther entity = new DCROther();
			this.convertToEntity(dcrOther, entity);
			dcrOtherDao.updateOther(entity);
			return dcrOther;
		} catch (WMSDaoException e) {
			LOGGER.error(e);
			throw new ServiceException(e);
		}

	}

	private void convertToEntity(DCROtherBO dcrOther, DCROther entity) {
		entity.setDcrBalancingToolProductId(dcrOther
				.getDcrBalancingToolProductId());
		entity.setCashCounterAmt(dcrOther.getCashCounterAmt());
		entity.setCashNonCounter(dcrOther.getCashNonCounter());
		entity.setChequeNonCounter(dcrOther.getChequeNonCounter());
		entity.setChequeOnUs(dcrOther.getChequeOnUs());
		entity.setChequeRegional(dcrOther.getChequeRegional());
		entity.setTotalDollarCheque(dcrOther.getTotalDollarCheque());
		entity.setTotalUsCheckInManila(dcrOther.getTotalUsCheckInManila());
		entity.setTotalUsCheckOutPh(dcrOther.getTotalUsCheckOutPh());
		entity.setId(dcrOther.getId());
		entity.setChequeLocal(dcrOther.getChequeLocal());
	}

}
