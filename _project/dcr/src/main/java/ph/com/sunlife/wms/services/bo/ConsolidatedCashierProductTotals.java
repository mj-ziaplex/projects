package ph.com.sunlife.wms.services.bo;

import ph.com.sunlife.wms.dao.domain.ProductType;

public class ConsolidatedCashierProductTotals {

    private ConsolidatedCashierTotals parent;
    private ProductType productType;
    private Long dcrBalancingToolProductId;
    private double totalCashCounter;
    private double totalCashNonCounter;
    private double totalCheckOnUs;
    private double totalCheckLocal;
    private double totalCheckRegional;
    private double totalCheckOT;
    private double totalCheckNonCounter;
    private double totalNonCash;
    private double totalPosBpi;
    private double totalPosCtb;
    private double totalPosHsbc;
    private double totalPosScb;
    private double totalPosRcbc;
    private double totalPosBdo;
//	private double totalNBIndividual;
//
//	private double totalNBVariable;
    // DTR
    private double totalVULRenewal;
    // DTR
    private double totalNonCashFromDTR;
    // DTR
    private double totalNonPolicy;
    // DTR
    private double totalWorksiteNonPosted;
    private double totalCreditMemo;
    // Exemptions
    private double exemptionsWorksite;
    // Exemptions
    private double totalReversalAmount;
    // DTR
    private double totalNewBusinessIndividual;
    // DTR
    private double totalNewBusinessVariable;
    // DTR
    private double totalIndividualRenewal;
    // DTR
    private double totalVariableRenewal;
    private double totalUsCheckInManila;
    private double totalUsCheckOutPh;
    private double totalDollarCheque;
    private double totalBankOTCCheckPayment;
    private double totalMdsBpi;
    private double totalMdsBdo;
    private double totalMdsRcbc;
    private double totalMdsSbc;
    private double totalMdsCtb;
    private double totalMdsHsbc;
    private double totalAutoCtb;
    private double totalAutoScb;
    private double totalAutoRcbc;
    private double totalSunlinkOnline;
    private double totalSunlinkHsbcDollar;
    private double totalSunlinkHsbcPeso;
    private double totalEpsBpi;
    // EPS-BPI (under Cash)
    private double totalCashEpsBpi;
    // Postal Money Order (under check)
    private double totalPmo;
    private double totalCheckGaf;
    private double totalCardGaf;
    private double sessionTotal;
    private double paymentFromInsurer;
    private double totalMf; // Added for PCO
    private double totalPcoInvest; // Added for PCO invest

    public double getPaymentFromInsurer() {
        return paymentFromInsurer;
    }

    public void setPaymentFromInsurer(double paymentFromInsurer) {
        this.paymentFromInsurer = paymentFromInsurer;
    }

    public double getSessionTotal() {
        return sessionTotal;
    }

    public void setSessionTotal(double sessionTotal) {
        this.sessionTotal = sessionTotal;
    }

    public double getTotalCheckGaf() {
        return totalCheckGaf;
    }

    public void setTotalCheckGaf(double totalCheckGaf) {
        this.totalCheckGaf = totalCheckGaf;
    }

    public double getTotalCardGaf() {
        return totalCardGaf;
    }

    public void setTotalCardGaf(double totalCardGaf) {
        this.totalCardGaf = totalCardGaf;
    }

    public double getTotalPmo() {
        return totalPmo;
    }

    public void setTotalPmo(double totalPmo) {
        this.totalPmo = totalPmo;
    }

    public double getTotalCashEpsBpi() {
        return totalCashEpsBpi;
    }

    public void setTotalCashEpsBpi(double totalCashEpsBpi) {
        this.totalCashEpsBpi = totalCashEpsBpi;
    }

    public double getTotalAutoCtb() {
        return totalAutoCtb;
    }

    public void setTotalAutoCtb(double totalAutoCtb) {
        this.totalAutoCtb = totalAutoCtb;
    }

    public double getTotalAutoScb() {
        return totalAutoScb;
    }

    public void setTotalAutoScb(double totalAutoScb) {
        this.totalAutoScb = totalAutoScb;
    }

    public double getTotalAutoRcbc() {
        return totalAutoRcbc;
    }

    public void setTotalAutoRcbc(double totalAutoRcbc) {
        this.totalAutoRcbc = totalAutoRcbc;
    }

    public double getTotalSunlinkOnline() {
        return totalSunlinkOnline;
    }

    public void setTotalSunlinkOnline(double totalSunlinkOnline) {
        this.totalSunlinkOnline = totalSunlinkOnline;
    }

    public double getTotalSunlinkHsbcDollar() {
        return totalSunlinkHsbcDollar;
    }

    public void setTotalSunlinkHsbcDollar(double totalSunlinkHsbcDollar) {
        this.totalSunlinkHsbcDollar = totalSunlinkHsbcDollar;
    }

    public double getTotalSunlinkHsbcPeso() {
        return totalSunlinkHsbcPeso;
    }

    public void setTotalSunlinkHsbcPeso(double totalSunlinkHsbcPeso) {
        this.totalSunlinkHsbcPeso = totalSunlinkHsbcPeso;
    }

    public double getTotalEpsBpi() {
        return totalEpsBpi;
    }

    public void setTotalEpsBpi(double totalEpsBpi) {
        this.totalEpsBpi = totalEpsBpi;
    }

    public double getTotalMdsBpi() {
        return totalMdsBpi;
    }

    public void setTotalMdsBpi(double totalMdsBpi) {
        this.totalMdsBpi = totalMdsBpi;
    }

    public double getTotalMdsBdo() {
        return totalMdsBdo;
    }

    public void setTotalMdsBdo(double totalMdsBdo) {
        this.totalMdsBdo = totalMdsBdo;
    }

    public double getTotalMdsRcbc() {
        return totalMdsRcbc;
    }

    public void setTotalMdsRcbc(double totalMdsRcbc) {
        this.totalMdsRcbc = totalMdsRcbc;
    }

    public double getTotalMdsSbc() {
        return totalMdsSbc;
    }

    public void setTotalMdsSbc(double totalMdsSbc) {
        this.totalMdsSbc = totalMdsSbc;
    }

    public double getTotalMdsCtb() {
        return totalMdsCtb;
    }

    public void setTotalMdsCtb(double totalMdsCtb) {
        this.totalMdsCtb = totalMdsCtb;
    }

    public double getTotalMdsHsbc() {
        return totalMdsHsbc;
    }

    public void setTotalMdsHsbc(double totalMdsHsbc) {
        this.totalMdsHsbc = totalMdsHsbc;
    }

    public ConsolidatedCashierTotals getParent() {
        return parent;
    }

    public void setParent(ConsolidatedCashierTotals parent) {
        this.parent = parent;
    }

    public ProductType getProductType() {
        return productType;
    }

    public void setProductType(ProductType productType) {
        this.productType = productType;
    }

    public double getTotalCashCounter() {
        return totalCashCounter;
    }

    public void setTotalCashCounter(double totalCashCounter) {
        this.totalCashCounter = totalCashCounter;
    }

    public double getTotalCashNonCounter() {
        return totalCashNonCounter;
    }

    public void setTotalCashNonCounter(double totalCashNonCounter) {
        this.totalCashNonCounter = totalCashNonCounter;
    }

    public double getTotalCheckOnUs() {
        return totalCheckOnUs;
    }

    public void setTotalCheckOnUs(double totalCheckOnUs) {
        this.totalCheckOnUs = totalCheckOnUs;
    }

    public double getTotalCheckLocal() {
        return totalCheckLocal;
    }

    public void setTotalCheckLocal(double totalCheckLocal) {
        this.totalCheckLocal = totalCheckLocal;
    }

    public double getTotalCheckRegional() {
        return totalCheckRegional;
    }

    public void setTotalCheckRegional(double totalCheckRegional) {
        this.totalCheckRegional = totalCheckRegional;
    }

    public double getTotalCheckOT() {
        return totalCheckOT;
    }

    public void setTotalCheckOT(double totalCheckOT) {
        this.totalCheckOT = totalCheckOT;
    }

    public double getTotalCheckNonCounter() {
        return totalCheckNonCounter;
    }

    public void setTotalCheckNonCounter(double totalCheckNonCounter) {
        this.totalCheckNonCounter = totalCheckNonCounter;
    }

    public double getTotalNonCash() {
        return totalNonCash;
    }

    public void setTotalNonCash(double totalNonCash) {
        this.totalNonCash = totalNonCash;
    }

    public double getTotalPosBpi() {
        return totalPosBpi;
    }

    public void setTotalPosBpi(double totalPosBpi) {
        this.totalPosBpi = totalPosBpi;
    }

    public double getTotalPosCtb() {
        return totalPosCtb;
    }

    public void setTotalPosCtb(double totalPosCtb) {
        this.totalPosCtb = totalPosCtb;
    }

    public double getTotalPosHsbc() {
        return totalPosHsbc;
    }

    public void setTotalPosHsbc(double totalPosHsbc) {
        this.totalPosHsbc = totalPosHsbc;
    }

    public double getTotalPosScb() {
        return totalPosScb;
    }

    public void setTotalPosScb(double totalPosScb) {
        this.totalPosScb = totalPosScb;
    }

    public double getTotalPosRcbc() {
        return totalPosRcbc;
    }

    public void setTotalPosRcbc(double totalPosRcbc) {
        this.totalPosRcbc = totalPosRcbc;
    }

    public double getTotalPosBdo() {
        return totalPosBdo;
    }

    public void setTotalPosBdo(double totalPosBdo) {
        this.totalPosBdo = totalPosBdo;
    }

//	public double getTotalNBIndividual() {
//		return totalNBIndividual;
//	}
//
//	public void setTotalNBIndividual(double totalNBIndividual) {
//		this.totalNBIndividual = totalNBIndividual;
//	}
//
//	public double getTotalNBVariable() {
//		return totalNBVariable;
//	}
//
//	public void setTotalNBVariable(double totalNBVariable) {
//		this.totalNBVariable = totalNBVariable;
//	}
    public double getTotalVULRenewal() {
        return totalVULRenewal;
    }

    public void setTotalVULRenewal(double totalVULRenewal) {
        this.totalVULRenewal = totalVULRenewal;
    }

    public double getTotalNonCashFromDTR() {
        return totalNonCashFromDTR;
    }

    public void setTotalNonCashFromDTR(double totalNonCashFromDTR) {
        this.totalNonCashFromDTR = totalNonCashFromDTR;
    }

    public double getTotalNonPolicy() {
        return totalNonPolicy;
    }

    public void setTotalNonPolicy(double totalNonPolicy) {
        this.totalNonPolicy = totalNonPolicy;
    }

    public double getTotalWorksiteNonPosted() {
        return totalWorksiteNonPosted;
    }

    public void setTotalWorksiteNonPosted(double totalWorksiteNonPosted) {
        this.totalWorksiteNonPosted = totalWorksiteNonPosted;
    }

    public double getTotalCreditMemo() {
        return totalCreditMemo;
    }

    public void setTotalCreditMemo(double totalCreditMemo) {
        this.totalCreditMemo = totalCreditMemo;
    }

    public double getExemptionsWorksite() {
        return exemptionsWorksite;
    }

    public void setExemptionsWorksite(double exemptionsWorksite) {
        this.exemptionsWorksite = exemptionsWorksite;
    }

    public double getTotalReversalAmount() {
        return totalReversalAmount;
    }

    public void setTotalReversalAmount(double totalReversalAmount) {
        this.totalReversalAmount = totalReversalAmount;
    }

    public double getTotalNewBusinessIndividual() {
        return totalNewBusinessIndividual;
    }

    public void setTotalNewBusinessIndividual(double totalNewBusinessIndividual) {
        this.totalNewBusinessIndividual = totalNewBusinessIndividual;
    }

    public double getTotalNewBusinessVariable() {
        return totalNewBusinessVariable;
    }

    public void setTotalNewBusinessVariable(double totalNewBusinessVariable) {
        this.totalNewBusinessVariable = totalNewBusinessVariable;
    }

    public double getTotalIndividualRenewal() {
        return totalIndividualRenewal;
    }

    public void setTotalIndividualRenewal(double totalIndividualRenewal) {
        this.totalIndividualRenewal = totalIndividualRenewal;
    }

    public double getTotalVariableRenewal() {
        return totalVariableRenewal;
    }

    public void setTotalVariableRenewal(double totalVariableRenewal) {
        this.totalVariableRenewal = totalVariableRenewal;
    }

    public double getTotalUsCheckInManila() {
        return totalUsCheckInManila;
    }

    public void setTotalUsCheckInManila(double totalUsCheckInManila) {
        this.totalUsCheckInManila = totalUsCheckInManila;
    }

    public double getTotalUsCheckOutPh() {
        return totalUsCheckOutPh;
    }

    public void setTotalUsCheckOutPh(double totalUsCheckOutPh) {
        this.totalUsCheckOutPh = totalUsCheckOutPh;
    }

    public double getTotalDollarCheque() {
        return totalDollarCheque;
    }

    public void setTotalDollarCheque(double totalDollarCheque) {
        this.totalDollarCheque = totalDollarCheque;
    }

    public double getTotalBankOTCCheckPayment() {
        return totalBankOTCCheckPayment;
    }

    public void setTotalBankOTCCheckPayment(double totalBankOTCCheckPayment) {
        this.totalBankOTCCheckPayment = totalBankOTCCheckPayment;
    }

    public Long getDcrBalancingToolProductId() {
        return dcrBalancingToolProductId;
    }

    public void setDcrBalancingToolProductId(Long dcrBalancingToolProductId) {
        this.dcrBalancingToolProductId = dcrBalancingToolProductId;
    }

    /**
     * @return the totalMf
     */
    public double getTotalMf() {
        return totalMf;
    }

    /**
     * @param totalMf the totalMf to set
     */
    public void setTotalMf(double totalMf) {
        this.totalMf = totalMf;
    }

    /**
     * @return the totalPcoInvest
     */
    public double getTotalPcoInvest() {
        return totalPcoInvest;
    }

    /**
     * @param totalPcoInvest the totalPcoInvest to set
     */
    public void setTotalPcoInvest(double totalPcoInvest) {
        this.totalPcoInvest = totalPcoInvest;
    }
}
