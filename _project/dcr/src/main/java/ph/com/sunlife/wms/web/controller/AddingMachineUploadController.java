package ph.com.sunlife.wms.web.controller;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.poi.util.IOUtils;
import org.springframework.web.servlet.ModelAndView;

import ph.com.sunlife.wms.security.Role;
import ph.com.sunlife.wms.services.DCRAddingMachineService;
import ph.com.sunlife.wms.services.bo.DCRAddingMachineBO;
import ph.com.sunlife.wms.services.exception.InvalidFileTypeException;
import ph.com.sunlife.wms.services.exception.ServiceException;
import ph.com.sunlife.wms.util.ParameterMap;
import ph.com.sunlife.wms.web.controller.form.AddingMachineForm;
import ph.com.sunlife.wms.web.controller.form.CashierWorkItemForm;
import ph.com.sunlife.wms.web.exception.ApplicationException;
import ph.com.sunlife.wms.web.view.WMSDownloadFileView;

/**
 * The Web Controller for the Adding Machine Page.
 * 
 * @author Zainal Limpao
 * 
 */
public class AddingMachineUploadController extends
		AbstractCashierWorkItemController {

	private static final Logger LOGGER = Logger
			.getLogger(AddingMachineUploadController.class);

	private static final String VIEW_NAME = "addingMachineView";

	private static final String PAGE_TITLE = "Adding Machine Upload";

	private DCRAddingMachineService dcrAddingMachineService;

	/*
	 * (non-Javadoc)
	 * 
	 * @see ph.com.sunlife.wms.web.controller.AbstractCashierWorkItemController#
	 * getPageTitle()
	 */
	@Override
	protected String getPageTitle() {
		return PAGE_TITLE;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ph.com.sunlife.wms.web.controller.AbstractSecuredMultiActionController
	 * #allowedRoles()
	 */
	@Override
	protected Role[] allowedRoles() {
		return new Role[] { Role.CASHIER };
	}

	/**
	 * The default controller method whenever the Adding Machine Page is being
	 * accessed.
	 * 
	 * @param request
	 * @param response
	 * @param form
	 * @return
	 * @throws Exception
	 */
	public ModelAndView showAddingMachineUploadPage(HttpServletRequest request,
			HttpServletResponse response, CashierWorkItemForm form)
			throws Exception {
		LOGGER.info("Loading Adding Machine Upload Page...");

		ModelAndView modelAndView = new ModelAndView(VIEW_NAME);

		Long dcrCashierId = form.getDcrCashierId();

		try {
			DCRAddingMachineBO businessObject = dcrAddingMachineService
					.getAddingMachine(dcrCashierId);

			boolean isHasUploaded = (businessObject != null);

			modelAndView.addObject("hasBeenUploaded", isHasUploaded);
			modelAndView.addObject("message", "&nbsp;");
		} catch (ServiceException e) {
			LOGGER.error(e);
			throw new ApplicationException(e);
		}

		return modelAndView;
	}

	/**
	 * The method in which returns a downloadable Excel Template for the Adding
	 * Machine.
	 * 
	 * @param request
	 * @param response
	 * @param form
	 * @return
	 * @throws ApplicationException
	 */
	public ModelAndView downloadAddingMachineTemplate(
			HttpServletRequest request, HttpServletResponse response,
			CashierWorkItemForm form) throws ApplicationException {
		LOGGER.info("Downloading Adding Machine Upload Template...");

		ServletContext context = request.getSession(false).getServletContext();

		ModelAndView modelAndView = new ModelAndView(new WMSDownloadFileView());
		try {
			// FIXME: make this configurable.
			InputStream is = context
					.getResourceAsStream("/WEB-INF/classes/CashBreakdown.xls");
			byte[] excelFile = IOUtils.toByteArray(is);

			modelAndView.addObject("fileData", excelFile);
			modelAndView.addObject("fileName", "template.xls");
		} catch (IOException e) {
			throw new ApplicationException(e);
		}

		return modelAndView;
	}

	/**
	 * The method called whenever user is done selecting a file to upload and
	 * clicks on the Upload button on the Adding Machine Page.
	 * 
	 * @param request
	 * @param response
	 * @param form
	 * @return
	 * @throws ApplicationException
	 */
	public ModelAndView uploadAddingMachine(HttpServletRequest request,
			HttpServletResponse response, AddingMachineForm form)
			throws ApplicationException {
		LOGGER.info("Uploading Adding Machine...");
		ModelAndView modelAndView = new ModelAndView(VIEW_NAME);

		try {
			Map<String, Object> map = new ParameterMap(form);

			DCRAddingMachineBO businessObject = dcrAddingMachineService
					.uploadDCRAddingMachine(map);

			if (businessObject.getId() != null) {
				modelAndView.addObject("message", "Upload Sucessful");
				modelAndView.addObject("hasBeenUploaded", true);
			}
		} catch (InvalidFileTypeException e) {
			LOGGER.error(e);
			modelAndView.addObject("message", "File is not valid");
			modelAndView.addObject("hasBeenUploaded", false);
		} catch (ServiceException e) {
			LOGGER.error(e);
			throw new ApplicationException(e);
		}

		return modelAndView;
	}

	public void setDcrAddingMachineService(
			DCRAddingMachineService dcrAddingMachineService) {
		this.dcrAddingMachineService = dcrAddingMachineService;
	}

}
