package ph.com.sunlife.wms.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.PreparedStatementCallback;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

import ph.com.sunlife.wms.dao.DCRCachedPropertyDao;
import ph.com.sunlife.wms.dao.domain.DCRCachedProperty;
import ph.com.sunlife.wms.dao.exceptions.WMSDaoException;
import ph.com.sunlife.wms.util.WMSDateUtil;

public class DCRCachedPropertyDaoImpl extends JdbcDaoSupport implements
		DCRCachedPropertyDao {

	private static final Logger LOGGER = Logger
			.getLogger(DCRCachedPropertyDaoImpl.class);
	
	private boolean useCurrentDate;
	
	public void setUseCurrentDate(boolean useCurrentDate) {
		this.useCurrentDate = useCurrentDate;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> getAllCachedProperties() throws WMSDaoException {

		String sql = "SELECT * FROM dbo.DCRCachedProperty";

		try {
			Map<String, Object> result = (Map<String, Object>) getJdbcTemplate()
					.execute(sql, new PreparedStatementCallback() {

						@Override
						public Object doInPreparedStatement(PreparedStatement ps)
								throws SQLException, DataAccessException {
							ResultSet rs = ps.executeQuery();

							Map<String, Object> result = new HashMap<String, Object>();
							while (rs.next()) {

								DCRCachedProperty prop = new DCRCachedProperty();
								String key = rs.getString("prop_key");

								prop.setId(rs.getLong("id"));
								prop.setPropKey(key);
								prop.setPropValue(rs.getString("prop_value"));
								prop.setDcrCreUser(rs.getString("dcr_cre_user"));
								prop.setDcrCreDate(rs.getDate("dcr_cre_date"));
								prop.setDcrUpdUser(rs.getString("dcr_upd_user"));
								prop.setDcrUpdDate(rs.getDate("dcr_upd_date"));
								result.put(key, prop);
							}

							System.out
									.println("DB has been queried for values...");
							return result;
						}
					});

			return result;
		} catch (Exception ex) {
			LOGGER.error(ex);
			throw new WMSDaoException(ex);
		}
	}

	public boolean incrementWmsDcrSystemDate(final String wmsDcrSystemDateKey,
			final Date wmsDcrSystemDate) throws WMSDaoException {
		// final Date updatedWmsDcrSystemDate = DateUtils.addDays(
		// wmsDcrSystemDate, 1);
		
		String dateStr = null;
		if (!useCurrentDate) {
			String getSql = "SELECT prop_value FROM DCRCachedProperty WHERE prop_key = ?";
			dateStr = (String) getJdbcTemplate().execute(getSql,
					new PreparedStatementCallback() {
	
						@Override
						public Object doInPreparedStatement(PreparedStatement ps)
								throws SQLException, DataAccessException {
							ps.setString(1, wmsDcrSystemDateKey);
	
							ResultSet rs = ps.executeQuery();
							String dateStr = "";
							while (rs.next()) {
								dateStr = rs.getString("prop_value");
								break;
							}
	
							return dateStr;
						}
					});
		} else {
			dateStr = WMSDateUtil.toFormattedDateStr(wmsDcrSystemDate);
		}
		
		int rowsUpdated = 0;
		if (StringUtils.isNotEmpty(dateStr)) {
			Date date = WMSDateUtil.toDate(dateStr);

			if (date != null) {
				
				if (!useCurrentDate) {
					date = DateUtils.addDays(date, 1);
				} 

				final Date incrementedDate = date;
				String updateSql = "UPDATE dbo.DCRCachedProperty SET prop_value = ? WHERE prop_key = ?";
				rowsUpdated = (Integer) getJdbcTemplate().execute(updateSql,
						new PreparedStatementCallback() {

							@Override
							public Object doInPreparedStatement(
									PreparedStatement ps) throws SQLException,
									DataAccessException {
								ps.setString(1, WMSDateUtil
										.toFormattedDateStr(incrementedDate));
//								ps.setString(1, WMSDateUtil
//										.toFormattedDateStr(wmsDcrSystemDate));
								ps.setString(2, wmsDcrSystemDateKey);

								return ps.executeUpdate();
							}
						});
			}
//
		}

		return (rowsUpdated != 0);
	}

	@Override
	public void reset() {
		LOGGER.info("Resetting cached properties...");
	}
	
	public boolean updateProperty(final String prop_key,final String prop_value, final String lastUpdatedUser, final Long lastUpdatedDate){
		int rowsUpdated = 0;
		String updateSql = "UPDATE dbo.DCRCachedProperty SET prop_value = ?, dcr_upd_user = ?, dcr_upd_date = ?  WHERE prop_key = ?";
		rowsUpdated = (Integer) getJdbcTemplate().execute(updateSql,
				new PreparedStatementCallback() {

					@Override
					public Object doInPreparedStatement(
							PreparedStatement ps) throws SQLException,
							DataAccessException {
						ps.setString(1, prop_value);
						ps.setString(2, lastUpdatedUser);
						ps.setDate(3, new java.sql.Date(lastUpdatedDate));
						ps.setString(4, prop_key);

						return ps.executeUpdate();
					}
				});
	
		return rowsUpdated > 0;
	}

}
