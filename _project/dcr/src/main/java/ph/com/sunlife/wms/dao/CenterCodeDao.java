package ph.com.sunlife.wms.dao;

import java.util.List;

import ph.com.sunlife.wms.dao.domain.CenterCode;
import ph.com.sunlife.wms.dao.exceptions.WMSDaoException;

public interface CenterCodeDao {
	
	boolean createCenterCode(CenterCode cc) throws WMSDaoException;

	List<CenterCode> getAllCenterCode() throws WMSDaoException;

	List<CenterCode> getCenterCodeById(String ccId) throws WMSDaoException;

	boolean updateCenterCode(CenterCode cc) throws WMSDaoException;

	boolean deleteCenterCode(String ccId) throws WMSDaoException;

}
