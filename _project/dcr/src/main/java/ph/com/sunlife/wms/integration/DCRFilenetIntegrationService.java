package ph.com.sunlife.wms.integration;

import java.util.Date;
import java.util.List;

import ph.com.sunlife.wms.integration.domain.DCRFile;
import ph.com.sunlife.wms.integration.exceptions.IntegrationException;

/**
 * The Interface responsible for Integrating Filenet and WMS DCR. All
 * Filenet-related calls are encapsulated within the implementing class of this
 * interface.
 * 
 * @author Zainal Limpao
 * @author Edgardo Cunanan
 * @author Crisseljess Mendoza
 * 
 */
public interface DCRFilenetIntegrationService {

	/**
	 * Creates DCR Workitem (and folder) within Filenet.
	 * 
	 * @param dcrDate
	 * @param dcrId
	 * @param docCapsiteId
	 * @return
	 * @throws IntegrationException
	 */
	String createDcrOnFilenet(Date dcrDate, Long dcrId, String docCapsiteId)
			throws IntegrationException;

	/**
	 * This is called when completing a step.
	 * 
	 * @param response
	 * @param wObName
	 * @param queueName
	 * @throws IntegrationException
	 */
	void approveStep(String response, String wObName, String queueName)
			throws IntegrationException;

	/**
	 * Returns {@link DCRFile} from filenet given the corresponding documentId.
	 * 
	 * @param documentId
	 * @return
	 * @throws IntegrationException
	 */
	DCRFile getFileFromFilenet(String documentId) throws IntegrationException;

	/**
	 * Attaches file to work item.
	 * 
	 * @param dcrDate
	 * @param docCapsiteId
	 * @param customFileName
	 * @param file
	 *            in byte array format
	 * @return document Id in String
	 * @throws IntegrationException
	 */
	String attachFileToWorkItem(Date dcrDate, String docCapsiteId,
			String customFileName, byte[] file) throws IntegrationException;

	/**
	 * Removes file from work item.
	 * 
	 * @param dcrDate
	 * @param docCapsiteId
	 * @param documentId
	 * @throws IntegrationException
	 */
	void removeFileFromWorkItem(Date dcrDate, String docCapsiteId,
			String documentId) throws IntegrationException;

	String copyFileToWorkItem(Date dcrDate, String docCapsiteId,
			String documentId) throws IntegrationException;

	List<DCRFile> getValidatedDepositSlipFiles(String dcrFolderPath)
			throws IntegrationException;
	
	DCRFile getFileFromFilenetInfo(String id) throws IntegrationException;

}
