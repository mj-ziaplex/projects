package ph.com.sunlife.wms.dao.domain;

public class DCRVDILPRSeriesNumber extends Entity {

	private DCRVDIL dcrvdil = new DCRVDIL();
	
	private Long companyId;
	
	private int from;
	
	private int to;
	
	private String reason;
	
	public Long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}

	public DCRVDIL getDcrvdil() {
		return dcrvdil;
	}

	public void setDcrvdil(DCRVDIL dcrvdil) {
		this.dcrvdil = dcrvdil;
	}
	
	public void setDcrvdil(Long dcrvdilId) {
		DCRVDIL dcrvdil = new DCRVDIL();
		dcrvdil.setId(dcrvdilId);
		this.setDcrvdil(dcrvdil);
	}

	public int getFrom() {
		return from;
	}

	public void setFrom(int from) {
		this.from = from;
	}

	public int getTo() {
		return to;
	}

	public void setTo(int to) {
		this.to = to;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}
	
}
