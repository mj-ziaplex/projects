package ph.com.sunlife.wms.dao.impl;

import java.sql.SQLException;
import java.util.Collections;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.orm.ibatis.SqlMapClientCallback;

import ph.com.sunlife.wms.dao.AbstractWMSSqlMapClientDaoSupport;
import ph.com.sunlife.wms.dao.DCRBalancingToolDao;
import ph.com.sunlife.wms.dao.domain.BalancingToolSnapshot;
import ph.com.sunlife.wms.dao.domain.CollectionOnBehalfData;
import ph.com.sunlife.wms.dao.domain.DCRBalancingTool;
import ph.com.sunlife.wms.dao.domain.DCRBalancingToolProduct;
import ph.com.sunlife.wms.dao.domain.DCRCashier;
import ph.com.sunlife.wms.dao.domain.DCRIpacValue;
import ph.com.sunlife.wms.dao.domain.DCROther;
import ph.com.sunlife.wms.dao.domain.Excemption;
import ph.com.sunlife.wms.dao.exceptions.WMSDaoException;

import com.ibatis.sqlmap.client.SqlMapExecutor;

/**
 * The Implementing Class of {@link DCRBalancingToolDao}.
 * 
 * @author Zainal Limpao
 * 
 */
public class DCRBalancingToolDaoImpl extends
		AbstractWMSSqlMapClientDaoSupport<DCRBalancingTool> implements
		DCRBalancingToolDao {

	private static final String NAMESPACE = "DCRBalancingTool";
	private static final Logger LOGGER = Logger.getLogger(DCRBalancingToolDaoImpl.class);

	/**
	 * Singleton Implementation of this lookup values of IPAC
	 */
	// private List<DCRIpacLookup> ipacLookupMapping;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ph.com.sunlife.wms.dao.AbstractWMSSqlMapClientDaoSupport#getNamespace()
	 */
	@Override
	protected String getNamespace() {
		return NAMESPACE;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ph.com.sunlife.wms.dao.DCRBalancingToolDao#getBalancingTools(java.lang
	 * .Long)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<DCRBalancingTool> getBalancingTools(final Long dcrCashierId) throws WMSDaoException {
		List<DCRBalancingTool> list = null;

		try {
			list = (List<DCRBalancingTool>) getSqlMapClientTemplate().execute(
					new SqlMapClientCallback() {

						@Override
						public Object doInSqlMapClient(SqlMapExecutor executor) throws SQLException {
							return executor.queryForList(getNamespace() + ".getBalancingTools", dcrCashierId);
						}
					});
		} catch (Exception e) {
			throw new WMSDaoException(e);
		}
		return list;
	}

    /*
     * (non-Javadoc)
     * 
     * @see
     * ph.com.sunlife.wms.dao.DCRBalancingToolDao#saveBalancingToolProduct(ph
     * .com.sunlife.wms.dao.domain.DCRBalancingToolProduct)
     */
    @Override
    public DCRBalancingToolProduct saveBalancingToolProduct(
            DCRBalancingToolProduct dcrBalancingToolProduct) {

        try {
            Long id = (Long) getSqlMapClientTemplate().queryForObject(
                    getNamespace() + ".insertDCRBalancingToolProduct",
                    dcrBalancingToolProduct);
            dcrBalancingToolProduct.setId(id);
        } catch (Exception e) {
            // Change according to Healthcheck
            // *Problem was identified as log4j was not used to log exception
            // *Change add log4j to log exception
            LOGGER.error("Error in saving balancingTool: ", e);
        }

        return dcrBalancingToolProduct;
    }

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ph.com.sunlife.wms.dao.DCRBalancingToolDao#getAllBalancingToolProducts
	 * (java.lang.Long)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<DCRBalancingToolProduct> getAllBalancingToolProducts(
			final Long dcrBalancingToolId) throws WMSDaoException {
		List<DCRBalancingToolProduct> list = null;

		try {
			list = (List<DCRBalancingToolProduct>) getSqlMapClientTemplate()
					.execute(new SqlMapClientCallback() {

						@Override
						public Object doInSqlMapClient(SqlMapExecutor executor)
								throws SQLException {
							return executor.queryForList(getNamespace()
									+ ".getDCRBalancingToolProducts",
									dcrBalancingToolId);
						}
					});
		} catch (Exception e) {
			throw new WMSDaoException(e);
		}
		return list;
	}

	@SuppressWarnings("unchecked")
	public List<DCRBalancingToolProduct> getAllBalancingToolProducts(
			final List<Long> dcrBalancingToolIds) throws WMSDaoException {

		try {
			return getSqlMapClientTemplate().executeWithListResult(
					new SqlMapClientCallback() {

						@Override
						public Object doInSqlMapClient(SqlMapExecutor executor)
								throws SQLException {
							return executor.queryForList(getNamespace()
									+ ".getAllDCRBalancingToolProducts",
									dcrBalancingToolIds);
						}
					});
		} catch (Exception e) {
			throw new WMSDaoException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ph.com.sunlife.wms.dao.DCRBalancingToolDao#getIpacValues(ph.com.sunlife
	 * .wms.dao.domain.DCRCashier)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<DCRIpacValue> getIpacValues(final DCRCashier dcrCashier)
			throws WMSDaoException {
		// TODO: the actual query will be through a linked server going to IPAC
		List<DCRIpacValue> list = null;

		try {
			list = (List<DCRIpacValue>) getSqlMapClientTemplate().execute(
					new SqlMapClientCallback() {

						@Override
						public Object doInSqlMapClient(SqlMapExecutor executor)
								throws SQLException {
							return executor.queryForList(getNamespace()
									+ ".getIpacValues", dcrCashier);
						}
					});

			for (DCRIpacValue value : list) {
				value.setAcf2id(dcrCashier.getCashier().getAcf2id());
				value.setDcrCashierId(dcrCashier.getId());
			}
		} catch (Exception e) {
			throw new WMSDaoException(e);
		}
		return list;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ph.com.sunlife.wms.dao.DCRBalancingToolDao#getIpacLookupMapping()
	 */
	// @SuppressWarnings("unchecked")
	// @Override
	// public List<DCRIpacLookup> getIpacLookupMapping() throws WMSDaoException
	// {
	//
	// // The IPAC LOOKUP MAPPING is singleton
	// if (CollectionUtils.isEmpty(ipacLookupMapping)) {
	// List<DCRIpacLookup> lookupList = null;
	//
	// try {
	// lookupList = (List<DCRIpacLookup>) getSqlMapClientTemplate()
	// .execute(new SqlMapClientCallback() {
	//
	// @Override
	// public Object doInSqlMapClient(
	// SqlMapExecutor executor)
	// throws SQLException {
	// return executor.queryForList(getNamespace()
	// + ".getIpacLookupMapping");
	// }
	// });
	//
	// } catch (Exception e) {
	// throw new WMSDaoException(e);
	// }
	// this.ipacLookupMapping = lookupList;
	// }
	// return this.ipacLookupMapping;
	// }

	// /*
	// * (non-Javadoc)
	// *
	// * @see org.springframework.dao.support.DaoSupport#initDao()
	// */
	// @Override
	// protected void initDao() throws Exception {
	// super.initDao();
	// this.getIpacLookupMapping();
	// }

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ph.com.sunlife.wms.dao.DCRBalancingToolDao#confirmBalancingTool(ph.com
	 * .sunlife.wms.dao.domain.DCRBalancingTool)
	 */
	@Override
	public boolean confirmBalancingTool(final DCRBalancingTool entity,
			final boolean isDayOne) throws WMSDaoException {
		Object result = null;

		try {
			result = getSqlMapClientTemplate().execute(
					new SqlMapClientCallback() {

						@Override
						public Object doInSqlMapClient(SqlMapExecutor executor)
								throws SQLException {

							int result = 0;

							if (isDayOne) {
								result = executor.update(getNamespace()
										+ ".confirmBalancingToolDay1", entity);
							} else {
								result = executor.update(getNamespace()
										+ ".confirmBalancingToolDay2", entity);
							}
							return result;
						}
					});

		} catch (Exception e) {
			throw new WMSDaoException(e);
		}

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Records updated: " + result);
		}

		return (result != null);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<DCROther> getDcrOthers(List<Long> productIds)
			throws WMSDaoException {
		if (CollectionUtils.isEmpty(productIds)) {
			return Collections.EMPTY_LIST;
		}

		List<DCROther> list = null;
		try {
			list = (List<DCROther>) getSqlMapClientTemplate().queryForList(
					getNamespace() + ".getDcrOthers", productIds);
		} catch (Exception e) {
			throw new WMSDaoException(e);
		}
		return list;
	}

	@SuppressWarnings("unchecked")
	public List<DCROther> getDcrOthersByDcrId(Long dcrId)
			throws WMSDaoException {
		List<DCROther> list = null;
		try {
			list = (List<DCROther>) getSqlMapClientTemplate().queryForList(
					getNamespace() + ".getDcrOthersByDcrId", dcrId);
		} catch (Exception e) {
			throw new WMSDaoException(e);
		}
		return list;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ph.com.sunlife.wms.dao.DCRBalancingToolDao#saveOther(ph.com.sunlife.wms
	 * .dao.domain.DCROther)
	 */
	public DCROther saveOther(final DCROther dcrOther) throws WMSDaoException {
		try {
			Long id = (Long) getSqlMapClientTemplate().queryForObject(
					getNamespace() + ".insertDcrOther", dcrOther);
			dcrOther.setId(id);
		} catch (Exception e) {
			throw new WMSDaoException(e);
		}
		return dcrOther;
	}

	@Override
	public boolean saveWorkSite(DCRBalancingToolProduct entity)
			throws WMSDaoException {
		try {
			int rowsUpdated = getSqlMapClientTemplate().update(
					getNamespace() + ".saveWorkSite", entity);

			return rowsUpdated != 0;
		} catch (Exception ex) {
			throw new WMSDaoException(ex);
		}

	}

	@SuppressWarnings("unchecked")
	public List<Excemption> getExcemptions(Long dcrId) throws WMSDaoException {
		List<Excemption> list = null;
		try {
			list = (List<Excemption>) getSqlMapClientTemplate().queryForList(
					getNamespace() + ".getReversalsByDcr", dcrId);

			return list;
		} catch (Exception e) {
			throw new WMSDaoException(e);
		}
	}

	@SuppressWarnings("unchecked")
	public List<CollectionOnBehalfData> getCollectionOnBehalfData(Long dcrId)
			throws WMSDaoException {
		List<CollectionOnBehalfData> list = null;
		try {
			list = (List<CollectionOnBehalfData>) getSqlMapClientTemplate()
					.queryForList(
							getNamespace() + ".getCollectionOnBehalfData",
							dcrId);

			return list;
		} catch (Exception e) {
			throw new WMSDaoException(e);
		}
	}

	@SuppressWarnings("unchecked")
	public List<BalancingToolSnapshot> getSnapshotsByDcrId(Long dcrId)
			throws WMSDaoException {
		List<BalancingToolSnapshot> list = null;
		try {
			list = (List<BalancingToolSnapshot>) getSqlMapClientTemplate()
					.queryForList(getNamespace() + ".getSnapshotsByDcrId",
							dcrId);

			return list;
		} catch (Exception e) {
			throw new WMSDaoException(e);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<DCRBalancingTool> getMultipleBalancingTools(
			final List<Long> cashierWorkItemIds) throws WMSDaoException {

		try {
			return getSqlMapClientTemplate().executeWithListResult(
					new SqlMapClientCallback() {

						@Override
						public Object doInSqlMapClient(SqlMapExecutor executor)
								throws SQLException {
							return executor.queryForList(getNamespace()
									+ ".getAllBalancingTools",
									cashierWorkItemIds);
						}
					});

		} catch (Exception e) {
			throw new WMSDaoException(e);
		}
	}

}
