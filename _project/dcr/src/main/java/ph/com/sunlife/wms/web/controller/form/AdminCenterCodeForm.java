package ph.com.sunlife.wms.web.controller.form;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.validation.Errors;

import ph.com.sunlife.wms.web.controller.binder.BinderAware;
import ph.com.sunlife.wms.web.controller.binder.SessionAware;

// Change according to Healthcheck
// *Problem was declared as public/protected in BinderAware
// *Change BinderAware interface class to abstract class and extends it rather than implements
public class AdminCenterCodeForm extends BinderAware implements SessionAware {
	
	private String ccId;
	private String ccName;
	private String ccDesc;
	private String ccAutoIdx;
	private String ccCanEncode;
	private String ccViewImage;
	private String ccZoomZone;
	private String ccActive;
	private String ccAbbrevName;
	private String ccIsIso;
	private HttpSession session;
	private String userId;
	private String userHub;
	private List<String> userGroups;
	private String userNBO;
	private String cId;
	
	public String getcId() {
		return cId;
	}

	public void setcId(String cId) {
		this.cId = cId;
	}

	public String getUserNBO() {
		return userNBO;
	}

	public void setUserNBO(String userNBO) {
		this.userNBO = userNBO;
	}

	public List<String> getUserGroups() {
		return userGroups;
	}

	public void setUserGroups(List<String> userGroups) {
		this.userGroups = userGroups;
	}

	public String getUserHub() {
		return userHub;
	}

	public void setUserHub(String userHub) {
		this.userHub = userHub;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getCcId() {
		return ccId;
	}

	public void setCcId(String ccId) {
		this.ccId = ccId;
	}

	public String getCcName() {
		return ccName;
	}

	public void setCcName(String ccName) {
		this.ccName = ccName;
	}

	public String getCcDesc() {
		return ccDesc;
	}

	public void setCcDesc(String ccDesc) {
		this.ccDesc = ccDesc;
	}

	public String getCcAutoIdx() {
		return ccAutoIdx;
	}

	public void setCcAutoIdx(String ccAutoIdx) {
		this.ccAutoIdx = ccAutoIdx;
	}

	public String getCcCanEncode() {
		return ccCanEncode;
	}

	public void setCcCanEncode(String ccCanEncode) {
		this.ccCanEncode = ccCanEncode;
	}

	public String getCcViewImage() {
		return ccViewImage;
	}

	public void setCcViewImage(String ccViewImage) {
		this.ccViewImage = ccViewImage;
	}

	public String getCcZoomZone() {
		return ccZoomZone;
	}

	public void setCcZoomZone(String ccZoomZone) {
		this.ccZoomZone = ccZoomZone;
	}

	public String getCcActive() {
		return ccActive;
	}

	public void setCcActive(String ccActive) {
		this.ccActive = ccActive;
	}

	public String getCcAbbrevName() {
		return ccAbbrevName;
	}

	public void setCcAbbrevName(String ccAbbrevName) {
		this.ccAbbrevName = ccAbbrevName;
	}

	public String getCcIsIso() {
		return ccIsIso;
	}

	public void setCcIsIso(String ccIsIso) {
		this.ccIsIso = ccIsIso;
	}

	@Override
	public HttpSession getSession() {
		// TODO Auto-generated method stub
		return session;
	}

	@Override
	public void setSession(HttpSession session) {
		// TODO Auto-generated method stub
		this.session = session;
	}

	@Override
	public String getSecretKey() {
		return null;
	}

    // Change according to Healthcheck
    // *Problem was declared as public/protected in BinderAware
    // *Change use BinderAware getter instead of direct access
    public String[] getAllowedFields() {
        return super.getAllowedFields();
    }

    // Change according to Healthcheck
    // *Problem was declared as public/protected in BinderAware
    // *Change use BinderAware getter instead of direct access
    public String[] getRequiredFields() {
        return super.getRequiredFields();
    }

    // Change according to Healthcheck
    // *Problem was declared as public/protected in BinderAware
    // *Change use BinderAware getter instead of direct access
    public String[] getUncheckedFields() {
        return super.getUncheckedFields();
    }

	@Override
	public void afterBind() {
	}

	@Override
	public boolean validate(Errors errors) {
		return true;
	}

}
