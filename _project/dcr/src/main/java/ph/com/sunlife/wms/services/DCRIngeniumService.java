/**
 * 
 */
package ph.com.sunlife.wms.services;

import java.util.Date;
import java.util.List;

import ph.com.sunlife.wms.dao.domain.DCRIpacValue;
import ph.com.sunlife.wms.dao.domain.SessionTotal;
import ph.com.sunlife.wms.services.exception.ServiceException;

import com.sunlife.ingeniumutil.container.UserCollectionsInfo;

/**
 * @author i176
 *
 */
public interface DCRIngeniumService {

	public boolean populateDCRSessionTotal(Long dcrId, Date dcrDate, String ccId, List<DCRIpacValue> ipacValues)  throws ServiceException;

	public SessionTotal retrieveDCRSessionTotal(UserCollectionsInfo info, String company)  throws ServiceException;

	public List<SessionTotal> retrieveDCRSessionTotal(UserCollectionsInfo info)  throws ServiceException;

	public void removeDCRIngeniumSessionTotal(String ccId, Date processDate) throws ServiceException;

}
