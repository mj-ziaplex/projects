package ph.com.sunlife.wms.services.bo;

import java.util.Date;
import java.util.List;

import ph.com.sunlife.wms.util.WMSDateUtil;


public class DCRErrorTagLogConsolidatedDisplayBO {
	
	private Long id;
	
	private Long dcrId;
	
	private Date datePosted;
	
	private Date dateUpdated;
	
	private String stepId;
	
	private String stepName;
	
	private String stepCompletorUserId;
	
	private String reason;
	
	private String postedUpdatedById;
	
	private String reasonDeleted;
	
	private List<DCRErrorBO> errors;

	public String getDatePostedStr() {
		return WMSDateUtil.toFormattedDateStrWithTime(datePosted);
	}

	public String getDateUpdatedStr() {
		return WMSDateUtil.toFormattedDateStrWithTime(dateUpdated);
	}

	public Long getDcrId() {
		return dcrId;
	}

	public void setDcrId(Long dcrId) {
		this.dcrId = dcrId;
	}

	public Date getDatePosted() {
		return datePosted;
	}

	public void setDatePosted(Date datePosted) {
		this.datePosted = datePosted;
	}

	public Date getDateUpdated() {
		return dateUpdated;
	}

	public void setDateUpdated(Date dateUpdated) {
		this.dateUpdated = dateUpdated;
	}

	public String getStepName() {
		return stepName;
	}

	public void setStepName(String stepName) {
		this.stepName = stepName;
	}

	public String getStepCompletorUserId() {
		return stepCompletorUserId;
	}

	public void setStepCompletorUserId(String stepCompletorUserId) {
		this.stepCompletorUserId = stepCompletorUserId;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getPostedUpdatedById() {
		return postedUpdatedById;
	}

	public void setPostedUpdatedById(String postedUpdatedById) {
		this.postedUpdatedById = postedUpdatedById;
	}

	public String getReasonDeleted() {
		return reasonDeleted;
	}

	public void setReasonDeleted(String reasonDeleted) {
		this.reasonDeleted = reasonDeleted;
	}

	public String getStepId() {
		return stepId;
	}

	public void setStepId(String stepId) {
		this.stepId = stepId;
	}

	public List<DCRErrorBO> getErrors() {
		return errors;
	}

	public void setErrors(List<DCRErrorBO> errors) {
		this.errors = errors;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((dcrId == null) ? 0 : dcrId.hashCode());
		result = prime
				* result
				+ ((postedUpdatedById == null) ? 0 : postedUpdatedById
						.hashCode());
		result = prime * result
				+ ((reasonDeleted == null) ? 0 : reasonDeleted.hashCode());
		result = prime
				* result
				+ ((stepCompletorUserId == null) ? 0 : stepCompletorUserId
						.hashCode());
		result = prime * result + ((stepId == null) ? 0 : stepId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DCRErrorTagLogConsolidatedDisplayBO other = (DCRErrorTagLogConsolidatedDisplayBO) obj;
		if (dcrId == null) {
			if (other.dcrId != null)
				return false;
		} else if (!dcrId.equals(other.dcrId))
			return false;
		if (postedUpdatedById == null) {
			if (other.postedUpdatedById != null)
				return false;
		} else if (!postedUpdatedById.equals(other.postedUpdatedById))
			return false;
		if (reasonDeleted == null) {
			if (other.reasonDeleted != null)
				return false;
		} else if (!reasonDeleted.equals(other.reasonDeleted))
			return false;
		if (stepCompletorUserId == null) {
			if (other.stepCompletorUserId != null)
				return false;
		} else if (!stepCompletorUserId.equals(other.stepCompletorUserId))
			return false;
		if (stepId == null) {
			if (other.stepId != null)
				return false;
		} else if (!stepId.equals(other.stepId))
			return false;
		return true;
	}
	
}
