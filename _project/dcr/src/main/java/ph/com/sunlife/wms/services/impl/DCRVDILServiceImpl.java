/**
 * @author i176
 * ****************************************************************************
 * @author: ia23 - sasay
 * @date: Mar2016
 * @desc: Removed methods and parameters that will not be used due to 
 * MR-WF-15-00089 - DCR Redesign
 */
package ph.com.sunlife.wms.services.impl;

import static ph.com.sunlife.wms.util.WMSConstants.LINE_SEPARATOR;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import ph.com.sunlife.wms.dao.CashierDao;
import ph.com.sunlife.wms.dao.DCRBalancingToolDao;
import ph.com.sunlife.wms.dao.DCRCashierDao;
import ph.com.sunlife.wms.dao.DCRDao;
import ph.com.sunlife.wms.dao.DCRIPACDao; // To be added for DCR redesign
import ph.com.sunlife.wms.dao.DCRVDILDao;
import ph.com.sunlife.wms.dao.IpacDao;
import ph.com.sunlife.wms.dao.domain.Cashier;
import ph.com.sunlife.wms.dao.domain.CollectionOnBehalfData;
import ph.com.sunlife.wms.dao.domain.Currency;
import ph.com.sunlife.wms.dao.domain.DCR;
import ph.com.sunlife.wms.dao.domain.DCRCashier;
import ph.com.sunlife.wms.dao.domain.DCRDollarCoinCollection;
import ph.com.sunlife.wms.dao.domain.DCRDollarCoinExchange;
import ph.com.sunlife.wms.dao.domain.DCRIpacValue;
import ph.com.sunlife.wms.dao.domain.DCRVDIL;
import ph.com.sunlife.wms.dao.domain.DCRVDILISOCollection;
import ph.com.sunlife.wms.dao.domain.DCRVDILPRSeriesNumber;
import ph.com.sunlife.wms.dao.domain.GAFValue;
import ph.com.sunlife.wms.dao.domain.ProductType;
import ph.com.sunlife.wms.dao.exceptions.WMSDaoException;
import ph.com.sunlife.wms.integration.DCRFilenetIntegrationService;
import ph.com.sunlife.wms.integration.exceptions.IntegrationException;
import ph.com.sunlife.wms.services.DCRVDILService;
import ph.com.sunlife.wms.services.bo.CashierBO;
import ph.com.sunlife.wms.services.bo.DCRCashierBO;
import ph.com.sunlife.wms.services.bo.DCRDollarCoinCollectionBO;
import ph.com.sunlife.wms.services.bo.DCRDollarCoinExchangeBO;
import ph.com.sunlife.wms.services.bo.DCRVDILBO;
import ph.com.sunlife.wms.services.bo.DCRVDILISOCollectionBO;
import ph.com.sunlife.wms.services.bo.DCRVDILPRSeriesNumberBO;
import ph.com.sunlife.wms.services.bo.VDILCollectionTotalsBO;
import ph.com.sunlife.wms.services.exception.ServiceException;

/**
 * The implementing class for the {@link DCRVDILService}.
 *
 * @author Zainal Limpao
 * @author Josephus Sardan
 *
 */
public class DCRVDILServiceImpl implements DCRVDILService {

    private static final Logger LOGGER = Logger.getLogger(DCRVDILServiceImpl.class);

    private static final int NUMBER_OF_SUB_ENTITIES = 4;

    private static final String MUTUAL_FUND_PROD_CODE_PREFIX = "MF";

    private static final String GROUPLIFE_PROD_CODE = "GL";

    private static final String PRENEED_PROD_CODE = "PN";

    private static final String CHECK_DOLLAR_DEPO_SLIP_SUB_CODE = "DDK";

    private static final String REGIONAL_CHECK_SUB_CODE = "R/C";

    private static final String ON_US_CHECK_SUB_CODE = "OUC";

    private static final String OUT_OF_TOWN_CHECK_SUB_CODE = "O/C";

    private static final String LOCAL_CHECK_SUB_CODE = "L/C";

    private static final String CHECK_PAY_CODE = "CHK";

    private static final String CURRENCY_USD = "USD";

    private static final String CURRENCY_PHP = "PHP";

    private static final String CPH_PAY_SUB_CODE = "CPH";

    private static final String CASH_PAY_CODE = "CSH";

    private static final String VL_PROD_CODE = "VL";

    private static final String IL_PROD_CODE = "IL";

    private static final String PDF_EXTENSION = ".pdf";

    private static final String[] CARD_POS_PAY_SUB_CODES = new String[]{
        "BMP", "BPP", "BPU", "CMM", "CMP", "CMU", "CPP", "CPU", "EMP",
        "EMU", "EPP", "EPP", "EPS", "HMP", "HMU", "HPP", "HPU", "HSP",
        "HSU", "ICT", "SMM", "SMP", "SMU", "SPP", "SPU"};
    
    private static final String[] SLGFI_PROD_CODE = new String[]{"RG", "RL", "RU", "RV"};

    private DCRVDILDao dcrVdilDao;

    private DCRCashierDao dcrCashierDao;

    private IpacDao ipacDao;

    private CashierDao cashierDao;

    private DCRFilenetIntegrationService dcrFilenetIntegrationService;

    private DCRBalancingToolDao dcrBalancingToolDao;

    private DCRDao dcrDao;

    private DCRIPACDao dcrIpacDao; // Added for DCR redesign

    /*
     * (non-Javadoc)
     * 
     * @see
     * ph.com.sunlife.wms.services.DCRVDILService#createNewVdil(java.lang.Long)
     */
    @Override
    public DCRVDILBO createNewVdil(Long dcrCashierId) throws ServiceException {
        LOGGER.debug("Creating VDIL...");

        DCRVDILBO businessObject = null;

        DCRVDIL vdil = new DCRVDIL();
        vdil.setDcrCashier(dcrCashierId);

        try {
            vdil = dcrVdilDao.save(vdil);
            DCRCashier dcrCashier = dcrCashierDao.getById(dcrCashierId);

            Long id = vdil.getId();
            if (id != null) {
                vdil.setDcrCashier(dcrCashier);
                int subEntityCount = NUMBER_OF_SUB_ENTITIES;

                List<DCRVDILPRSeriesNumber> prSeriesNumbers = new ArrayList<DCRVDILPRSeriesNumber>();
                List<DCRVDILISOCollection> isoCollections = new ArrayList<DCRVDILISOCollection>();
                for (int i = 0; i < subEntityCount; i++) {
                    DCRVDILPRSeriesNumber prSeriesNumber = new DCRVDILPRSeriesNumber();
                    prSeriesNumber.setDcrvdil(vdil);
                    prSeriesNumber = dcrVdilDao
                            .insertDcrVdilPrSeriesNumber(prSeriesNumber);

                    DCRVDILISOCollection isoCollection = new DCRVDILISOCollection();
                    isoCollection.setDcrvdil(vdil);
                    isoCollection = dcrVdilDao
                            .insertDcrVdilIsoCollection(isoCollection);

                    prSeriesNumbers.add(prSeriesNumber);
                    isoCollections.add(isoCollection);
                }

                DCRDollarCoinExchange dollarCoinExchange = new DCRDollarCoinExchange();
                dollarCoinExchange.setDcrvdil(vdil);
                dollarCoinExchange = dcrVdilDao
                        .createDCRDollarCoinExchange(dollarCoinExchange);

                DCRDollarCoinCollection previousCollection = getPreviousDollarCoinCollection(vdil);

                DCRDollarCoinCollection currentCollection = new DCRDollarCoinCollection();
                currentCollection.setDateCreated(new Date());
                currentCollection.setDcrvdil(vdil);
                currentCollection = dcrVdilDao
                        .saveDCRDollarCoinCollection(currentCollection);

                vdil.setDcrDollarCoinExchange(dollarCoinExchange);
                vdil.setCurrentCoinCollection(currentCollection);
                vdil.setPreviousCoinCollection(previousCollection);
                vdil.setPrSeriesNumbers(prSeriesNumbers);
                vdil.setIsoCollections(isoCollections);

            }

            businessObject = this.convertToBusinessObject(vdil, "");

            this.populatePdcCountAndDcrDateToBusinessObject(businessObject,
                    dcrCashier);
        } catch (Exception e) {
            LOGGER.error(e);
            throw new ServiceException(e);
        }

        return businessObject;
    }

    /*
     * (non-Javadoc)
     * 
     * @see ph.com.sunlife.wms.services.DCRVDILService#getVdil(java.lang.Long,
     * java.lang.String)
     */
    @Override
    public DCRVDILBO getVdil(Long dcrCashierId, String acf2id)
            throws ServiceException {

        DCRVDILBO dcrVdilBo = null;
        try {
            DCRVDIL vdil = dcrVdilDao.getByDCRCashierId(dcrCashierId);
            DCRCashier dcrCashier = dcrCashierDao.getById(dcrCashierId);

            if (vdil != null && dcrCashier != null) {
                Cashier cashier = cashierDao.getCashier(acf2id);
                dcrCashier.setCashier(cashier);
                vdil.setDcrCashier(dcrCashier);
                Long dcrVdilId = vdil.getId();

                List<DCRVDILISOCollection> isoCollections = dcrVdilDao
                        .getDcrVdilIsoCollections(dcrVdilId);

                List<DCRVDILPRSeriesNumber> prSeriesNumbers = dcrVdilDao
                        .getDcrVdilPrSeriesNumbers(dcrVdilId);

                DCRDollarCoinCollection currentCollection = dcrVdilDao
                        .getDollarCoinCollectionByDcrCashierId(dcrCashierId);

                DCRDollarCoinCollection previousCollection = getPreviousDollarCoinCollection(vdil);

                DCRDollarCoinExchange exchange = dcrVdilDao
                        .getDollarCoinExchangeByCashierId(dcrCashierId);

                vdil.setDcrDollarCoinExchange(exchange);
                vdil.setPreviousCoinCollection(previousCollection);
                vdil.setCurrentCoinCollection(currentCollection);
                vdil.setIsoCollections(isoCollections);
                vdil.setPrSeriesNumbers(prSeriesNumbers);

                vdil.setDcrCashier(dcrCashier);
                dcrVdilBo = convertToBusinessObject(vdil, acf2id);

                this.populatePdcCountAndDcrDateToBusinessObject(dcrVdilBo,
                        dcrCashier);
            }
        } catch (Exception e) {
            LOGGER.error(e);
            throw new ServiceException(e);
        }

        return dcrVdilBo;
    }

    private DCRDollarCoinCollection getPreviousDollarCoinCollection(DCRVDIL vdil)
            throws Exception, WMSDaoException {
        Long previousDayDcrId = null;
        DCR dcr = vdil.getDcrCashier().getDcr();
        DCRDollarCoinCollection previousCollection = null;

        while (previousCollection == null) {
            previousDayDcrId = dcrVdilDao.getPreviousDayDcrId(dcr);
            previousCollection = dcrVdilDao
                    .getDollarCoinCollectionByDcrId(previousDayDcrId);

            if (previousDayDcrId != null) {
                if (previousCollection == null) {
                    dcr = dcrDao.getById(previousDayDcrId);
                }
            } else {
                break;
            }
        }
        return previousCollection;
    }

    private void populatePdcCountAndDcrDateToBusinessObject(
            DCRVDILBO dcrVdilBo, DCRCashier dcrCashier) throws WMSDaoException {
        String customerCenterId = dcrCashier.getDcr().getCcId();
        Date dcrDate = dcrCashier.getDcr().getDcrDate();

        // int pdcCount = ipacDao.getCurrentPdcCount(customerCenterId);
        int currentPNPdcCount = ipacDao.getCurrentPNPdcCount(customerCenterId);
        dcrVdilBo.setPdcPnCount(currentPNPdcCount);

        int currentTradVulPdcCount = ipacDao
                .getCurrentTradVulPdcCount(customerCenterId);
        dcrVdilBo.setPdcTradVulCount(currentTradVulPdcCount);

        dcrVdilBo.setPdcReceivedForTheDayTotal(currentPNPdcCount
                + currentTradVulPdcCount);

        // dcrVdilBo.setPdcReceivedForTheDayTotal(pdcCount);
        dcrVdilBo.setDcrDate(dcrDate);
    }

    private DCRVDIL convertToEntity(DCRVDILBO businessObject) {
        DCRVDIL entity = new DCRVDIL();
        entity.setAfterCutoffCard(businessObject.getAfterCutoffCard());
        entity.setAfterCutoffCashDollar(businessObject
                .getAfterCutoffCashDollar());
        entity.setAfterCutoffCashPeso(businessObject.getAfterCutoffCashPeso());
        entity.setAfterCutoffCheck(businessObject.getAfterCutoffCheck());
        entity.setDcrCashier(businessObject.getDcrCashier().getId());
        entity.setForSafeKeeping(businessObject.getForSafeKeeping().replace(
                LINE_SEPARATOR, " "));
        entity.setId(businessObject.getId());
        entity.setOverages(businessObject.getOverages());
        entity.setPettyCash(businessObject.getPettyCash());
        entity.setPhotocopy(businessObject.getPhotocopy());
        entity.setPnPDCPendingWarehouse(businessObject
                .getPnPDCPendingWarehouse());
        entity.setTotalDollarCoins(businessObject.getTotalDollarCoins());
        entity.setUnclaimedChange(businessObject.getUnclaimedChange());
        entity.setVultradPDCRPendingWarehouse(businessObject
                .getVultradPDCRPendingWarehouse());
        entity.setWorkingFund(businessObject.getWorkingFund());

        List<DCRVDILPRSeriesNumber> prSeriesNumbers = new ArrayList<DCRVDILPRSeriesNumber>();

        List<DCRVDILPRSeriesNumberBO> prSeriesNumberBOs = businessObject
                .getPrSeriesNumbers();
        for (DCRVDILPRSeriesNumberBO subBusinessObject : prSeriesNumberBOs) {
            DCRVDILPRSeriesNumber subEntity = new DCRVDILPRSeriesNumber();
            subEntity.setCompanyId(subBusinessObject.getCompanyId());
            subEntity.setDcrvdil(entity);
            subEntity.setFrom(subBusinessObject.getFrom());
            subEntity.setTo(subBusinessObject.getTo());
            subEntity.setId(subBusinessObject.getId());
            subEntity.setReason(subBusinessObject.getReason());

            prSeriesNumbers.add(subEntity);
        }
        entity.setPrSeriesNumbers(prSeriesNumbers);

        List<DCRVDILISOCollection> isoCollections = new ArrayList<DCRVDILISOCollection>();

        List<DCRVDILISOCollectionBO> isoCollectionBOs = businessObject
                .getIsoCollections();
        for (DCRVDILISOCollectionBO subBusinessObject : isoCollectionBOs) {
            DCRVDILISOCollection subEntity = new DCRVDILISOCollection();
            subEntity.setAmount(subBusinessObject.getAmount());
            subEntity.setCurrencyId(subBusinessObject.getCurrencyId());
            subEntity.setDcrDate(subBusinessObject.getDcrDate());
            subEntity.setId(subBusinessObject.getId());
            subEntity.setDcrvdil(entity);
            subEntity.setPickupDate(subBusinessObject.getPickupDate());
            subEntity.setTypeId(subBusinessObject.getTypeId());

            isoCollections.add(subEntity);
        }
        entity.setIsoCollections(isoCollections);

        DCRDollarCoinExchangeBO exchangeBO = businessObject
                .getDollarCoinExchange();
        if (exchangeBO != null) {
            DCRDollarCoinExchange exchange = new DCRDollarCoinExchange();
            exchange.setDateCreated(exchangeBO.getDateCreated());
            exchange.setId(exchangeBO.getId());
            exchange.setIncomingDenom1(exchangeBO.getIncomingDenom1());
            exchange.setIncomingDenom50c(exchangeBO.getIncomingDenom50c());
            exchange.setIncomingDenom25c(exchangeBO.getIncomingDenom25c());
            exchange.setIncomingDenom10c(exchangeBO.getIncomingDenom10c());
            exchange.setIncomingDenom5c(exchangeBO.getIncomingDenom5c());
            exchange.setIncomingDenom1c(exchangeBO.getIncomingDenom1c());

            exchange.setOutgoingDenom1(exchangeBO.getOutgoingDenom1());
            exchange.setOutgoingDenom50c(exchangeBO.getOutgoingDenom50c());
            exchange.setOutgoingDenom25c(exchangeBO.getOutgoingDenom25c());
            exchange.setOutgoingDenom10c(exchangeBO.getOutgoingDenom10c());
            exchange.setOutgoingDenom5c(exchangeBO.getOutgoingDenom5c());
            exchange.setOutgoingDenom1c(exchangeBO.getOutgoingDenom1c());
            exchange.setDcrvdil(entity);
            entity.setDcrDollarCoinExchange(exchange);
        }

        DCRDollarCoinCollectionBO currentCollectionBO = businessObject
                .getCurrentCoinCollection();

        if (currentCollectionBO != null) {
            DCRDollarCoinCollection currentCollection = new DCRDollarCoinCollection();
            currentCollection.setDateCreated(currentCollectionBO
                    .getDateCreated());
            currentCollection.setId(currentCollectionBO.getId());
            currentCollection.setDcrvdil(entity);
            currentCollection.setDenom1(currentCollectionBO.getDenom1());
            currentCollection.setDenom50c(currentCollectionBO.getDenom50c());
            currentCollection.setDenom25c(currentCollectionBO.getDenom25c());
            currentCollection.setDenom10c(currentCollectionBO.getDenom10c());
            currentCollection.setDenom5c(currentCollectionBO.getDenom5c());
            currentCollection.setDenom1c(currentCollectionBO.getDenom1c());
            entity.setCurrentCoinCollection(currentCollection);
        }

        DCRDollarCoinCollectionBO previousCollectionBO = businessObject
                .getPreviousCoinCollection();
        if (previousCollectionBO != null) {
            DCRDollarCoinCollection previousCollection = new DCRDollarCoinCollection();
            previousCollection.setDateCreated(previousCollectionBO
                    .getDateCreated());
            previousCollection.setDcrvdil(entity);
            previousCollection.setId(previousCollectionBO.getId());
            previousCollection.setDenom1(previousCollectionBO.getDenom1());
            previousCollection.setDenom50c(previousCollectionBO.getDenom50c());
            previousCollection.setDenom25c(previousCollectionBO.getDenom25c());
            previousCollection.setDenom10c(previousCollectionBO.getDenom10c());
            previousCollection.setDenom5c(previousCollectionBO.getDenom5c());
            previousCollection.setDenom1c(previousCollectionBO.getDenom1c());
            entity.setPreviousCoinCollection(previousCollection);
        }
        entity.setDateSaved(businessObject.getDateSaved());
        return entity;
    }

    private DCRVDILBO convertToBusinessObject(DCRVDIL entity, String acf2id) {
        DCRVDILBO businessObject = new DCRVDILBO();
        businessObject.setAfterCutoffCard(entity.getAfterCutoffCard());
        businessObject.setAfterCutoffCashDollar(entity
                .getAfterCutoffCashDollar());
        businessObject.setAfterCutoffCashPeso(entity.getAfterCutoffCashPeso());
        businessObject.setAfterCutoffCheck(entity.getAfterCutoffCheck());

        DCRCashierBO dcrCashierBO = new DCRCashierBO();
        dcrCashierBO.setId(entity.getDcrCashier().getId());
        CashierBO cashierBO = new CashierBO(acf2id);
        cashierBO.setFirstname(entity.getDcrCashier().getCashier()
                .getFirstname());
        cashierBO
                .setLastname(entity.getDcrCashier().getCashier().getLastname());
        cashierBO.setMiddlename(entity.getDcrCashier().getCashier()
                .getMiddlename());
        dcrCashierBO.setCashier(cashierBO);

        businessObject.setDcrCashier(dcrCashierBO);
        String forSafeKeeping = entity.getForSafeKeeping();

        if (StringUtils.isNotEmpty(forSafeKeeping)) {
            forSafeKeeping = forSafeKeeping.replace(LINE_SEPARATOR, " ");
        }
        businessObject.setForSafeKeeping(forSafeKeeping);
        businessObject.setId(entity.getId());
        businessObject.setOverages(entity.getOverages());
        businessObject.setPettyCash(entity.getPettyCash());
        businessObject.setPhotocopy(entity.getPhotocopy());
        businessObject.setPnPDCPendingWarehouse(entity
                .getPnPDCPendingWarehouse());
        businessObject.setTotalDollarCoins(entity.getTotalDollarCoins());
        businessObject.setUnclaimedChange(entity.getUnclaimedChange());
        businessObject.setVultradPDCRPendingWarehouse(entity
                .getVultradPDCRPendingWarehouse());
        businessObject.setWorkingFund(entity.getWorkingFund());

        List<DCRVDILPRSeriesNumber> prSeriesNumbers = entity
                .getPrSeriesNumbers();

        List<DCRVDILPRSeriesNumberBO> prSeriesNumberBOs = new ArrayList<DCRVDILPRSeriesNumberBO>();
        for (DCRVDILPRSeriesNumber subEntity : prSeriesNumbers) {
            DCRVDILPRSeriesNumberBO subBusinessObject = new DCRVDILPRSeriesNumberBO();
            subBusinessObject.setCompanyId(subEntity.getCompanyId());
            subBusinessObject.setDcrvdil(businessObject);
            subBusinessObject.setFrom(subEntity.getFrom());
            subBusinessObject.setTo(subEntity.getTo());
            subBusinessObject.setId(subEntity.getId());
            subBusinessObject.setReason(subEntity.getReason());

            prSeriesNumberBOs.add(subBusinessObject);
        }
        businessObject.setPrSeriesNumbers(prSeriesNumberBOs);

        List<DCRVDILISOCollection> isoCollections = entity.getIsoCollections();

        List<DCRVDILISOCollectionBO> isoCollectionBOs = new ArrayList<DCRVDILISOCollectionBO>();
        for (DCRVDILISOCollection subEntity : isoCollections) {
            DCRVDILISOCollectionBO subBusinessObject = new DCRVDILISOCollectionBO();
            subBusinessObject.setAmount(subEntity.getAmount());
            subBusinessObject.setCurrencyId(subEntity.getCurrencyId());
            subBusinessObject.setDcrvdil(businessObject);
            subBusinessObject.setDcrDate(subEntity.getDcrDate());
            subBusinessObject.setId(subEntity.getId());
            subBusinessObject.setPickupDate(subEntity.getPickupDate());
            subBusinessObject.setTypeId(subEntity.getTypeId());

            isoCollectionBOs.add(subBusinessObject);
        }
        businessObject.setIsoCollections(isoCollectionBOs);

        DCRDollarCoinExchange exchange = entity.getDcrDollarCoinExchange();
        if (exchange != null) {
            DCRDollarCoinExchangeBO exchangeBO = new DCRDollarCoinExchangeBO();
            exchangeBO.setDateCreated(exchange.getDateCreated());
            exchangeBO.setId(exchange.getId());
            exchangeBO.setIncomingDenom1(exchange.getIncomingDenom1());
            exchangeBO.setIncomingDenom50c(exchange.getIncomingDenom50c());
            exchangeBO.setIncomingDenom25c(exchange.getIncomingDenom25c());
            exchangeBO.setIncomingDenom10c(exchange.getIncomingDenom10c());
            exchangeBO.setIncomingDenom5c(exchange.getIncomingDenom5c());
            exchangeBO.setIncomingDenom1c(exchange.getIncomingDenom1c());

            exchangeBO.setOutgoingDenom1(exchange.getOutgoingDenom1());
            exchangeBO.setOutgoingDenom50c(exchange.getOutgoingDenom50c());
            exchangeBO.setOutgoingDenom25c(exchange.getOutgoingDenom25c());
            exchangeBO.setOutgoingDenom10c(exchange.getOutgoingDenom10c());
            exchangeBO.setOutgoingDenom5c(exchange.getOutgoingDenom5c());
            exchangeBO.setOutgoingDenom1c(exchange.getOutgoingDenom1c());
            exchangeBO.setDcrvdil(businessObject);
            businessObject.setDollarCoinExchange(exchangeBO);
        }

        DCRDollarCoinCollection currentCollection = entity
                .getCurrentCoinCollection();

        if (currentCollection != null) {
            DCRDollarCoinCollectionBO currentCollectionBO = new DCRDollarCoinCollectionBO();
            currentCollectionBO.setDateCreated(currentCollection
                    .getDateCreated());
            currentCollectionBO.setId(currentCollection.getId());
            currentCollectionBO.setDcrvdil(businessObject);
            currentCollectionBO.setDenom1(currentCollection.getDenom1());
            currentCollectionBO.setDenom50c(currentCollection.getDenom50c());
            currentCollectionBO.setDenom25c(currentCollection.getDenom25c());
            currentCollectionBO.setDenom10c(currentCollection.getDenom10c());
            currentCollectionBO.setDenom5c(currentCollection.getDenom5c());
            currentCollectionBO.setDenom1c(currentCollection.getDenom1c());
            businessObject.setCurrentCoinCollection(currentCollectionBO);
        }

        DCRDollarCoinCollection previousCollection = entity
                .getPreviousCoinCollection();
        if (previousCollection != null) {
            DCRDollarCoinCollectionBO previousCollectionBO = new DCRDollarCoinCollectionBO();
            previousCollectionBO.setId(previousCollection.getId());
            previousCollectionBO.setDateCreated(previousCollection
                    .getDateCreated());
            previousCollectionBO.setDcrvdil(businessObject);
            previousCollectionBO.setDenom1(previousCollection.getDenom1());
            previousCollectionBO.setDenom50c(previousCollection.getDenom50c());
            previousCollectionBO.setDenom25c(previousCollection.getDenom25c());
            previousCollectionBO.setDenom10c(previousCollection.getDenom10c());
            previousCollectionBO.setDenom5c(previousCollection.getDenom5c());
            previousCollectionBO.setDenom1c(previousCollection.getDenom1c());
            businessObject.setPreviousCoinCollection(previousCollectionBO);
        }
        businessObject.setDateSaved(entity.getDateSaved());
        return businessObject;
    }

    public void setDcrVdilDao(DCRVDILDao dcrVdilDao) {
        this.dcrVdilDao = dcrVdilDao;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * ph.com.sunlife.wms.services.DCRVDILService#updateVdil(ph.com.sunlife.
     * wms.services.bo.DCRVDILBO)
     */
    @Override
    public DCRVDILBO updateVdil(DCRVDILBO businessObject)
            throws ServiceException {
        LOGGER.debug("Updating VDIL...");

        boolean isSuccessful = true;
        try {
            DCRVDIL entity = convertToEntity(businessObject);

            // start: for attaching vdil pdf file
            String verId = null;
            byte[] fileData = businessObject.getVdilFile();
            String docCapsiteId = businessObject.getCustomerCenter();
            Date dcrDate = businessObject.getDcrDate();
            String userId = businessObject.getUserId();
            String customFileName = "VDIL_" + StringUtils.upperCase(userId)
                    + PDF_EXTENSION;
            verId = dcrFilenetIntegrationService.attachFileToWorkItem(dcrDate,
                    docCapsiteId, customFileName, fileData);
            if (verId != null) {
                entity.setVdilVersionSerialId(verId);
            }
            // end: for attaching vdil pdf file

            // TODO: (JSARD) add column vdilVersionSerialId in DCRVDIL table and
            // affected dao methods (e.g. updateVdil)
            isSuccessful = isSuccessful && dcrVdilDao.updateVdil(entity);

            for (DCRVDILISOCollection subEntity : entity.getIsoCollections()) {
                isSuccessful = isSuccessful
                        && dcrVdilDao.updateDcrVdilIsoCollection(subEntity);
            }

            for (DCRVDILPRSeriesNumber subEntity : entity.getPrSeriesNumbers()) {
                isSuccessful = isSuccessful
                        && dcrVdilDao.updateDcrVdilPrSeriesNumber(subEntity);
            }

            DCRDollarCoinExchange exchange = entity.getDcrDollarCoinExchange();
            isSuccessful = isSuccessful
                    && dcrVdilDao.updateDCRDollarCoinExchange(exchange);

            DCRDollarCoinCollection currentCollection = entity
                    .getCurrentCoinCollection();
            isSuccessful = isSuccessful
                    && dcrVdilDao
                    .updateNetDCRDollarCoinCollection(currentCollection);

        } catch (WMSDaoException ex) {
            LOGGER.error(ex);
            throw new ServiceException(ex);
        } catch (IntegrationException ex) {
            LOGGER.error(ex);
            throw new ServiceException(ex);
        }
        return isSuccessful ? businessObject : null;
    }

    /*
     * (non-Javadoc)
     * 
     * @see ph.com.sunlife.wms.services.DCRVDILService#updateVdil(java.util.Map)
     */
    @Override
    public DCRVDILBO updateVdil(Map<String, Object> map)
            throws ServiceException {
        DCRVDILBO businessObject = new DCRVDILBO();
        try {
            BeanUtils.populate(businessObject, map);
        } catch (IllegalAccessException e) {
            LOGGER.error(e);
            throw new ServiceException(e);
        } catch (InvocationTargetException e) {
            LOGGER.error(e);
            throw new ServiceException(e);
        }
        return updateVdil(businessObject);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * ph.com.sunlife.wms.services.DCRVDILService#generateCollectionTotals(ph
     * .com.sunlife.wms.services.bo.DCRVDILBO)
     */
    public VDILCollectionTotalsBO generateCollectionTotals(DCRVDILBO vdil) throws ServiceException {
        LOGGER.info("Retrieving Data from Database for the VDIL...");

        VDILCollectionTotalsBO collectionTotals = null;
        DCRCashierBO dcb = vdil.getDcrCashier();
        if (dcb != null) {
            try {
                Long dcrCashierId = dcb.getId();
                DCRCashier dcrCashier = dcrCashierDao.getById(dcrCashierId);
                String userId = dcrCashier.getCashier().getAcf2id();

                DCR dcrEntity = dcrCashier.getDcr();
                String centerCode = dcrEntity.getCcId();
                Date processDate = dcrEntity.getDcrDate();

                // Added for DCR redesign
                List<DCRIpacValue> list =
                        getDcrIpacDao().getDCRIPACDailyCollectionSummary(centerCode, processDate, userId);

                collectionTotals = new VDILCollectionTotalsBO();
                this.mapIpacToCollectionTotals(list, collectionTotals, centerCode, processDate, userId);
                this.mapCobToCollectionTotals(dcrCashier, collectionTotals);
                this.mapGafToCollectionTotals(userId, dcrEntity, collectionTotals);
            } catch (WMSDaoException ex) {
                LOGGER.error(ex);
                throw new ServiceException(ex);
            }
        }

        LOGGER.info("Retrieval Successful");
        return collectionTotals;
    }

    private void mapGafToCollectionTotals(
            String thisUserId,
            DCR dcr,
            VDILCollectionTotalsBO collectionTotals) throws WMSDaoException {
        Date dcrDate = dcr.getDcrDate();
        String ccId = dcr.getCcId();

        List<String> userIds = new ArrayList<String>();
        userIds.add(thisUserId);

        List<GAFValue> gafValues = new ArrayList<GAFValue>();
        if (CollectionUtils.isNotEmpty(userIds)) {
            for (String userId : userIds) {
                // Added for DCR redesign
                List<GAFValue> gafValuesPerCashier = getDcrIpacDao().getDCRIPACGAF(ccId, dcrDate, userId); // Added for DCR redesign

                if (CollectionUtils.isNotEmpty(gafValuesPerCashier)) {
                    gafValues.addAll(gafValuesPerCashier);
                }
            }
        }

        if (CollectionUtils.isNotEmpty(gafValues)) {
            for (GAFValue value : gafValues) {
                String payType = value.getPayType();
                double amount = value.getTotalAmount();
                if (StringUtils.equals("1", payType)) {
                    collectionTotals.setCashPesoPreneedGroup(collectionTotals.getCashPesoPreneedGroup() + amount);
                } else if (StringUtils.equals("2", payType)) {
                    collectionTotals.setCheckLocalPesoPreneedGroup(collectionTotals.getCheckLocalPesoPreneedGroup() + amount);
                } else if (StringUtils.equals("3", payType)) {
                    collectionTotals.setCardPreneedGroup(collectionTotals.getCardPreneedGroup() + amount);
                }
            }
        }
    }

    private void mapCobToCollectionTotals(DCRCashier dcrCashier,
            VDILCollectionTotalsBO collectionTotals) throws WMSDaoException {
        DCR dcr = dcrCashier.getDcr();
        Long dcrId = dcr.getId();

        String acf2id = dcrCashier.getCashier().getAcf2id();

        List<CollectionOnBehalfData> cobList = dcrBalancingToolDao
                .getCollectionOnBehalfData(dcrId);

        if (CollectionUtils.isNotEmpty(cobList)) {
            for (CollectionOnBehalfData data : cobList) {
                if (StringUtils.equalsIgnoreCase(data.getAcf2id(), acf2id)) {
                    Long productTypeId = data.getProductTypeId();

                    double cashCounter = data.getCashCounterAmount();
                    if (ProductType.OTHER_DOLLAR.getId().equals(productTypeId)) {
                        collectionTotals.setCashDollarCob(collectionTotals
                                .getCashDollarCob() + cashCounter);
                    } else if (ProductType.OTHER_PESO.getId().equals(
                            productTypeId)) {
                        collectionTotals.setCashPesoCob(collectionTotals
                                .getCashPesoCob() + cashCounter);
                    }

                    double checkLocal = data.getChequeLocal();
                    collectionTotals.setCheckLocalPesoCob(collectionTotals
                            .getCheckLocalPesoCob() + checkLocal);

                    double checkRegional = data.getChequeRegional();
                    collectionTotals.setCheckRegionalPesoCob(collectionTotals
                            .getCheckRegionalPesoCob() + checkRegional);

                    double checkOnUs = data.getChequeOnUs();
                    collectionTotals.setCheckOnUsPesoCob(collectionTotals
                            .getCheckOnUsPesoCob() + checkOnUs);

                    double checkUsDrawnOut = data.getChequeUsDownOut();
                    double checkUsDrawnMla = data.getChequeUsDrawnMla();
                    double checkDollar = data.getChequeDollar();
                    double checkDollarCob = checkUsDrawnMla + checkUsDrawnOut
                            + checkDollar;
                    collectionTotals.setCheckDollarCob(checkDollarCob);
                }
            }
        }
    }

    private void mapIpacToCollectionTotals(List<DCRIpacValue> list,
            VDILCollectionTotalsBO collectionTotals, String centerCode,
            Date processDate, String userId) throws WMSDaoException {

        String userIdToUpperCase = StringUtils.upperCase(userId);

        boolean isHasCardCollectionTradVul = false;
        boolean isHasCardCollectionGroupLife = false;
        boolean isHasCardCollectionPreneed = false;
        boolean isHasCardCollectionSlgfi = false;
        boolean isHasCardCollectionDollar = false;

        if (CollectionUtils.isNotEmpty(list)) {
            for (DCRIpacValue value : list) {
                this.mapValueToCollectionTotals(value, collectionTotals);

                String payCode = value.getPayCode();
                String prodCode = value.getProdCode();
                String paySubCurr = value.getPaySubCurr();

                if (StringUtils.equalsIgnoreCase("CRD", payCode)) {

                    if (LOGGER.isDebugEnabled()) {
                        LOGGER.debug("Card Payment Found");
                        LOGGER.debug("Pay Code:" + payCode);
                        LOGGER.debug("Prod Code:" + prodCode);
                        LOGGER.debug("Currency:" + paySubCurr);
                    }

                    if (StringUtils.equalsIgnoreCase(CURRENCY_PHP, paySubCurr)) {
                        if (StringUtils.equalsIgnoreCase("IL", prodCode)
                                || StringUtils.equalsIgnoreCase("VL", prodCode)) {
                            isHasCardCollectionTradVul = true;
                        } else if (StringUtils.equalsIgnoreCase("PN", prodCode)) {
                            isHasCardCollectionPreneed = true;
                        } else if (StringUtils.equalsIgnoreCase("GL", prodCode)) {
                            isHasCardCollectionGroupLife = true;
                        } else if (ArrayUtils.contains(SLGFI_PROD_CODE,
                                prodCode)) {
                            isHasCardCollectionSlgfi = true;
                        }
                    } else {
                        isHasCardCollectionDollar = true;
                    }
                }
            }

            if (isHasCardCollectionTradVul) {
                int count = ipacDao.getSalesSlipCount(centerCode, "IL",
                        processDate, Currency.PHP, userIdToUpperCase)
                        + ipacDao.getSalesSlipCount(centerCode, "VL",
                        processDate, Currency.PHP, userIdToUpperCase);

                if (LOGGER.isDebugEnabled()) {
                    LOGGER.debug("Trad/Vul Sales Slip Count: " + count);
                }
                collectionTotals.setCardTradVulSalesSlipNum(count);
            }

            if (isHasCardCollectionGroupLife) {
                int count = ipacDao.getSalesSlipCount(centerCode, "GL",
                        processDate, Currency.PHP, userIdToUpperCase);

                if (LOGGER.isDebugEnabled()) {
                    LOGGER.debug("Group Life Sales Slip Count: " + count);
                }

                collectionTotals.setCardGroupLifeSalesSlipNum(count);
            }

            if (isHasCardCollectionSlgfi) {
                int count = 0;

                for (String slgfiProdCode : SLGFI_PROD_CODE) {
                    count += ipacDao.getSalesSlipCount(centerCode,
                            slgfiProdCode, processDate, Currency.PHP,
                            userIdToUpperCase);
                }

                if (LOGGER.isDebugEnabled()) {
                    LOGGER.debug("SLGFI Sales Slip Count: " + count);
                }

                collectionTotals.setCardSlgfiSalesSlipNum(count);
            }

            if (isHasCardCollectionPreneed) {
                int count = ipacDao.getSalesSlipCount(centerCode, "PN",
                        processDate, Currency.PHP, userIdToUpperCase);

                if (LOGGER.isDebugEnabled()) {
                    LOGGER.debug("Preneed Sales Slip Count: " + count);
                }

                collectionTotals.setCardPreneedSalesSlipNum(count);
            }

            if (isHasCardCollectionDollar) {
                int count = 0;

                count += ipacDao.getSalesSlipCount(centerCode, "IL",
                        processDate, Currency.USD, userIdToUpperCase);

                count += ipacDao.getSalesSlipCount(centerCode, "RG",
                        processDate, Currency.USD, userIdToUpperCase);

                count += ipacDao.getSalesSlipCount(centerCode, "RL",
                        processDate, Currency.USD, userIdToUpperCase);

                count += ipacDao.getSalesSlipCount(centerCode, "RU",
                        processDate, Currency.USD, userIdToUpperCase);

                if (LOGGER.isDebugEnabled()) {
                    LOGGER.debug("Dollar Payment Sales Slip Count: " + count);
                }

                collectionTotals.setCardDollarCollectionSalesSlipNum(count);
            }
        }

    }

    private void mapValueToCollectionTotals(DCRIpacValue value,
            VDILCollectionTotalsBO collectionTotals) {
        String prodCode = value.getProdCode();
        String payCode = value.getPayCode();
        String paySubCode = value.getPaySubCode();
        String paySubCurr = value.getPaySubCurr();
        // String paySubDesc = value.getPaySubDesc();
        double amount = value.getAmount();

        if (IL_PROD_CODE.equalsIgnoreCase(prodCode)
                || VL_PROD_CODE.equalsIgnoreCase(prodCode)) {
            if (CASH_PAY_CODE.equalsIgnoreCase(payCode)) {
                if (CPH_PAY_SUB_CODE.equalsIgnoreCase(paySubCode) /* || "EPS".equalsIgnoreCase(paySubCode) */) {
                    collectionTotals.setCashPesoTradVul(collectionTotals
                            .getCashPesoTradVul() + amount);
                } else if ("UCH".equalsIgnoreCase(paySubCode)) {
                    collectionTotals.setCashDollarTradVul(collectionTotals
                            .getCashDollarTradVul() + amount);
                } else if ("EPS".equalsIgnoreCase(paySubCode)) {
                    collectionTotals.setCashEpsBpi(collectionTotals
                            .getCashEpsBpi() + amount);
                }
            } else if (CHECK_PAY_CODE.equalsIgnoreCase(payCode)) {
                if (CURRENCY_PHP.equalsIgnoreCase(paySubCurr)) {
                    if (LOCAL_CHECK_SUB_CODE.equalsIgnoreCase(paySubCode)) {
                        collectionTotals
                                .setCheckLocalPesoTradVul(collectionTotals
                                .getCheckLocalPesoTradVul() + amount);
                    } else if (OUT_OF_TOWN_CHECK_SUB_CODE
                            .equalsIgnoreCase(paySubCode)) {
                        collectionTotals
                                .setCheckOutOfTownPesoTradVul(collectionTotals
                                .getCheckOutOfTownPesoTradVul()
                                + amount);
                    } else if (ON_US_CHECK_SUB_CODE
                            .equalsIgnoreCase(paySubCode)) {
                        collectionTotals
                                .setCheckOnUsPesoTradVul(collectionTotals
                                .getCheckOnUsPesoTradVul() + amount);
                    } else if (REGIONAL_CHECK_SUB_CODE
                            .equalsIgnoreCase(paySubCode)) {
                        collectionTotals
                                .setCheckRegionalPesoTradVul(collectionTotals
                                .getCheckRegionalPesoTradVul() + amount);
                    }
                } else if (CURRENCY_USD.equalsIgnoreCase(paySubCurr)) {
                    if (CHECK_DOLLAR_DEPO_SLIP_SUB_CODE
                            .equalsIgnoreCase(paySubCode)
                            || "CDM".equalsIgnoreCase(paySubCode)
                            || "COP".equalsIgnoreCase(paySubCode)) {
                        collectionTotals.setCheckDollarTradVul(collectionTotals
                                .getCheckDollarTradVul() + amount);
                    }
                }
            } else if ("CRD".equalsIgnoreCase(payCode)) {
                if (ArrayUtils.contains(CARD_POS_PAY_SUB_CODES, paySubCode)) {
                    if (CURRENCY_USD.equalsIgnoreCase(paySubCurr)) {
                        collectionTotals
                                .setCardDollarCollection(collectionTotals
                                .getCardDollarCollection() + amount);

                        // collectionTotals
                        // .setCardDollarCollectionSalesSlipNum(collectionTotals
                        // .getCardDollarCollectionSalesSlipNum() + 1);
                    } else {
                        collectionTotals.setCardTradVul(collectionTotals
                                .getCardTradVul() + amount);

                        // collectionTotals
                        // .setCardTradVulSalesSlipNum(collectionTotals
                        // .getCardTradVulSalesSlipNum() + 1);
                    }
                }
            }
        } else if (PRENEED_PROD_CODE.equalsIgnoreCase(prodCode)) {
            if (CPH_PAY_SUB_CODE.equalsIgnoreCase(paySubCode)) {
                collectionTotals.setCashPesoPreneed(collectionTotals
                        .getCashPesoPreneed() + amount);
            }

            if (CHECK_PAY_CODE.equalsIgnoreCase(payCode)) {
                if (LOCAL_CHECK_SUB_CODE.equalsIgnoreCase(paySubCode)) {
                    collectionTotals.setCheckLocalPesoPreneed(collectionTotals
                            .getCheckLocalPesoPreneed() + amount);
                } else if (OUT_OF_TOWN_CHECK_SUB_CODE
                        .equalsIgnoreCase(paySubCode)) {
                    collectionTotals
                            .setCheckOutOfTownPesoPreneed(collectionTotals
                            .getCheckOutOfTownPesoPreneed() + amount);
                } else if (ON_US_CHECK_SUB_CODE.equalsIgnoreCase(paySubCode)) {
                    collectionTotals.setCheckOnUsPesoPreneed(collectionTotals
                            .getCheckOnUsPesoPreneed() + amount);
                } else if (REGIONAL_CHECK_SUB_CODE.equalsIgnoreCase(paySubCode)) {
                    collectionTotals
                            .setCheckRegionalPesoPreneed(collectionTotals
                            .getCheckRegionalPesoPreneed() + amount);
                }
            } else if ("CRD".equalsIgnoreCase(payCode)) {
                if (ArrayUtils.contains(CARD_POS_PAY_SUB_CODES, paySubCode)) {
                    collectionTotals.setCardPreneed(collectionTotals
                            .getCardPreneed() + amount);

                    // collectionTotals
                    // .setCardPreneedSalesSlipNum(collectionTotals
                    // .getCardPreneedSalesSlipNum() + 1);
                }
            }
        } else if (GROUPLIFE_PROD_CODE.equalsIgnoreCase(prodCode)) {
            if (CASH_PAY_CODE.equalsIgnoreCase(payCode)) {
                if (CPH_PAY_SUB_CODE.equalsIgnoreCase(paySubCode)) {
                    collectionTotals.setCashPesoGroupLife(collectionTotals
                            .getCashPesoGroupLife() + amount);
                }
            } else if (CHECK_PAY_CODE.equalsIgnoreCase(payCode)) {
                if (LOCAL_CHECK_SUB_CODE.equalsIgnoreCase(paySubCode)) {
                    collectionTotals
                            .setCheckLocalPesoGroupLife(collectionTotals
                            .getCheckLocalPesoGroupLife() + amount);
                } else if (OUT_OF_TOWN_CHECK_SUB_CODE
                        .equalsIgnoreCase(paySubCode)) {
                    collectionTotals
                            .setCheckOutOfTownPesoGroupLife(collectionTotals
                            .getCheckOutOfTownPesoGroupLife() + amount);
                } else if (ON_US_CHECK_SUB_CODE.equalsIgnoreCase(paySubCode)) {
                    collectionTotals.setCheckOnUsPesoGroupLife(collectionTotals
                            .getCheckOnUsPesoGroupLife() + amount);
                } else if (REGIONAL_CHECK_SUB_CODE.equalsIgnoreCase(paySubCode)) {
                    collectionTotals
                            .setCheckRegionalPesoGroupLife(collectionTotals
                            .getCheckRegionalPesoGroupLife() + amount);
                }
            } else if ("CRD".equalsIgnoreCase(payCode)) {
                if (ArrayUtils.contains(CARD_POS_PAY_SUB_CODES, paySubCode)) {
                    collectionTotals.setCardGroupLife(collectionTotals
                            .getCardGroupLife() + amount);

                    // collectionTotals
                    // .setCardGroupLifeSalesSlipNum(collectionTotals
                    // .getCardGroupLifeSalesSlipNum() + 1);
                }
            }
        } else if (StringUtils.contains(prodCode, MUTUAL_FUND_PROD_CODE_PREFIX)) {
            if (CASH_PAY_CODE.equalsIgnoreCase(payCode)) {
                if (CPH_PAY_SUB_CODE.equalsIgnoreCase(paySubCode)) {
                    collectionTotals.setCashPesoSlamc(collectionTotals
                            .getCashPesoSlamc() + amount);
                } else if ("UCH".equalsIgnoreCase(paySubCode)) {
                    collectionTotals.setCashDollarSlamc(collectionTotals
                            .getCashDollarSlamc() + amount);
                }
            } else if (CHECK_PAY_CODE.equalsIgnoreCase(payCode)) {
                if (CURRENCY_PHP.equalsIgnoreCase(paySubCurr)) {
                    if (LOCAL_CHECK_SUB_CODE.equalsIgnoreCase(paySubCode)) {
                        collectionTotals
                                .setCheckLocalPesoSlamc(collectionTotals
                                .getCheckLocalPesoSlamc() + amount);
                    } else if (OUT_OF_TOWN_CHECK_SUB_CODE
                            .equalsIgnoreCase(paySubCode)) {
                        collectionTotals
                                .setCheckOutOfTownPesoSlamc(collectionTotals
                                .getCheckOutOfTownPesoSlamc() + amount);
                    } else if (ON_US_CHECK_SUB_CODE
                            .equalsIgnoreCase(paySubCode)) {
                        collectionTotals.setCheckOnUsPesoSlamc(collectionTotals
                                .getCheckOnUsPesoSlamc() + amount);
                    } else if (REGIONAL_CHECK_SUB_CODE
                            .equalsIgnoreCase(paySubCode)) {
                        collectionTotals
                                .setCheckRegionalPesoSlamc(collectionTotals
                                .getCheckRegionalPesoSlamc() + amount);
                    }
                } else if (CURRENCY_USD.equalsIgnoreCase(paySubCurr)) {
                    if ("DCK".equalsIgnoreCase(paySubCode)) {
                        collectionTotals.setCheckDollarSlamc(collectionTotals
                                .getCheckDollarSlamc() + amount);
                    }
                }
            }
        } else if (ArrayUtils.contains(SLGFI_PROD_CODE, prodCode)) {
            if (CURRENCY_PHP.equalsIgnoreCase(paySubCurr)) {
                if ("CPH".equalsIgnoreCase(paySubCode)) {
                    collectionTotals.setCashPesoSlgfi(collectionTotals
                            .getCashPesoSlgfi() + amount);
                } else if ("L/C".equalsIgnoreCase(paySubCode)) {
                    collectionTotals.setCheckLocalPesoSlgfi(collectionTotals
                            .getCheckLocalPesoSlgfi() + amount);
                } else if ("R/C".equalsIgnoreCase(paySubCode)) {
                    collectionTotals.setCheckRegionalPesoSlgfi(collectionTotals
                            .getCheckRegionalPesoSlgfi() + amount);
                } else if ("OUC".equalsIgnoreCase(paySubCode)) {
                    collectionTotals.setCheckOnUsPesoSlgfi(collectionTotals
                            .getCheckOnUsPesoSlgfi() + amount);
                } else if ("BCP".equalsIgnoreCase(paySubCode)) {
                    collectionTotals
                            .setCheckOutOfTownPesoSlgfi(collectionTotals
                            .getCheckOutOfTownPesoSlgfi() + amount);
                } else if (ArrayUtils.contains(new String[]{"RMM", "RMP",
                    "RPP"}, paySubCode)) {
                    collectionTotals.setCardSlgfi(collectionTotals
                            .getCardSlgfi() + amount);
                    // collectionTotals.setCardSlgfiSalesSlipNum(collectionTotals
                    // .getCardSlgfiSalesSlipNum() + 1);
                }
            } else if (CURRENCY_USD.equalsIgnoreCase(paySubCurr)) {
                if ("UCH".equalsIgnoreCase(paySubCode)) {
                    collectionTotals.setCashDollarSlgfi(collectionTotals
                            .getCashDollarSlgfi() + amount);
                } else if (ArrayUtils.contains(new String[]{"CDM", "COP",
                    "BCU"}, paySubCode)) {
                    collectionTotals.setCheckDollarSlgfi(collectionTotals
                            .getCheckDollarSlgfi() + amount);

                } else if (ArrayUtils.contains(new String[]{"RAU", "RMU",
                    "RPU"}, paySubCode)) {
                    collectionTotals.setCardDollarCollection(collectionTotals
                            .getCardDollarCollection() + amount);
                    // collectionTotals
                    // .setCardDollarCollectionSalesSlipNum(collectionTotals
                    // .getCardDollarCollectionSalesSlipNum() + 1);
                }
            }
        }

    }

    public void setDcrCashierDao(DCRCashierDao dcrCashierDao) {
        this.dcrCashierDao = dcrCashierDao;
    }

    public void setIpacDao(IpacDao ipacDao) {
        this.ipacDao = ipacDao;
    }

    public void setDcrFilenetIntegrationService(
            DCRFilenetIntegrationService dcrFilenetIntegrationService) {
        this.dcrFilenetIntegrationService = dcrFilenetIntegrationService;
    }

    public void setCashierDao(CashierDao cashierDao) {
        this.cashierDao = cashierDao;
    }

    public void setDcrBalancingToolDao(DCRBalancingToolDao dcrBalancingToolDao) {
        this.dcrBalancingToolDao = dcrBalancingToolDao;
    }

    public void setDcrDao(DCRDao dcrDao) {
        this.dcrDao = dcrDao;
    }

    /**
     * @return the dcrIpacDao
     * To be added for DCR redesign
     */
    public DCRIPACDao getDcrIpacDao() {
        return dcrIpacDao;
    }

    /**
     * @param dcrIpacDao the dcrIpacDao to set
     * To be added for DCR redesign
     */
    public void setDcrIpacDao(DCRIPACDao dcrIpacDao) {
        this.dcrIpacDao = dcrIpacDao;
    }
}
