package ph.com.sunlife.wms.web.controller;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import ph.com.sunlife.wms.security.Role;
import ph.com.sunlife.wms.services.CachingService;
import ph.com.sunlife.wms.services.DCRLogInService;
import ph.com.sunlife.wms.services.bo.UserSession;
import ph.com.sunlife.wms.services.exception.ServiceException;
import ph.com.sunlife.wms.util.WMSConstants;
import ph.com.sunlife.wms.web.controller.binder.SessionAware;
import ph.com.sunlife.wms.web.exception.IllegalWebAccessException;

/**
 * The implementing {@link AbstractWMSMultiActionController} which requires a
 * successful authentication through the login mechanism.
 * 
 * @author Zainal Limpao
 * 
 */
public abstract class AbstractSecuredMultiActionController extends
		AbstractWMSMultiActionController {

	private static final Logger LOGGER = Logger
			.getLogger(AbstractSecuredMultiActionController.class);

	private static final String USER_COMPLETE_NAME = "userCompleteName";

	private static final String HEADER_DATE = "headerDate";

	private static final String HEADER_DATE_FORMAT_STRING = "EEEEE MMMMM d, yyyy";

	private static final DateFormat HEADER_DATE_FORMAT = new SimpleDateFormat(
			HEADER_DATE_FORMAT_STRING);
    
    // Change according to Healthcheck
    // *Problem was declared as public/protected
    // *Change was declare as private and set getter method
    private static final Role[] ALL_ROLES = Role.values();

	protected DCRLogInService dcrLogInService;
	
	protected CachingService cachingService;

	/**
	 * The allowed user roles for this controller. Set this to
	 * {@link Role#values()} if you are allowing everyone to access this page.
	 * 
	 * @return
	 */
	abstract protected Role[] allowedRoles();

	/**
	 * Checks the following:
	 * <ul>
	 * <li>If command is an instance of {@link SessionAware}</li>
	 * <li>If {@link HttpSession} associated with the {@link SessionAware} is
	 * active and not null</li>
	 * <li>If {@link UserSession} associated with the {@link HttpSession} has
	 * enough roles for this controller.</li>
	 * </ul>
	 * 
	 * @param Object
	 *            the command object
	 */
	protected boolean preValidate(Object command) {
		LOGGER.debug("Validating if the current user is allowed access for this page...");

		IllegalWebAccessException toBeThrownEx = null;
		IllegalWebAccessException ex = new IllegalWebAccessException(
				"User must log in to the system");

		if (command instanceof SessionAware) {
			SessionAware sessionAwareCommand = (SessionAware) command;
			HttpSession session = sessionAwareCommand.getSession();

			if (session != null) {
				UserSession userSession = (UserSession) session
						.getAttribute(WMSConstants.USER_SESSION);

				if (userSession != null) {
					List<Role> roles = userSession.getRoles();
					String userId = userSession.getUserId();

					List<Role> allowedRolesList = new ArrayList<Role>();

					CollectionUtils.addAll(allowedRolesList,
							this.allowedRoles());

					if (!CollectionUtils.containsAny(allowedRolesList, roles)) {
						toBeThrownEx = new IllegalWebAccessException(userId
								+ " has no access rights for this page");
					}
				} else {
					toBeThrownEx = ex;
				}
			} else {
				toBeThrownEx = ex;
			}

		} else {
			toBeThrownEx = ex;
		}

		if (toBeThrownEx != null) {
			LOGGER.error("A problem occured in validating user credentials",
					toBeThrownEx);
			throw toBeThrownEx;
		}

		return Boolean.TRUE;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ph.com.sunlife.wms.web.controller.AbstractWMSMultiActionController#
	 * afterHandleRequest(org.springframework.web.servlet.ModelAndView,
	 * javax.servlet.http.HttpServletRequest,
	 * javax.servlet.http.HttpServletResponse)
	 */
	@Override
	protected void afterHandleRequest(ModelAndView modelAndView,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		// Completes the values on the Header JSP

		String userCompleteName = "";

		UserSession userSession = this.getUserSession(request);
		if (userSession != null) {
			if(null != userSession.getAdminUserId()){
				userCompleteName += StringUtils.upperCase(userSession.getAdminUserId());
			}else{
				userCompleteName += StringUtils.upperCase(userSession.getUserId());
			}
		}

		modelAndView.addObject(USER_COMPLETE_NAME, userCompleteName);

		Date today = cachingService.getWmsDcrSystemDate();
		String headerDateString = HEADER_DATE_FORMAT.format(today);
		modelAndView.addObject(HEADER_DATE, headerDateString);
	}

	/**
	 * Handler method for {@link IllegalWebAccessException}. This will basically
	 * show the log in page and an error message explaining the details of the
	 * illegal action.
	 * 
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @param cause
	 *            the cause
	 * @return {@link ModelAndView} {@link IllegalWebAccessException}
	 */
	public ModelAndView handleIllegalWebAccessException(
			HttpServletRequest request, HttpServletResponse response,
			IllegalWebAccessException cause) {
		AbstractSecuredMultiActionController.LOGGER.error(
				"Problem occured on the user's access rights", cause);

		HttpSession session = request.getSession(false);

		if (session != null) {
			session.invalidate();
		}

		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName(SecurityController.LOGIN_VIEW);
		modelAndView.addObject("errorMsg", cause.getMessage());

		this.pushExceptionToModelAndView(modelAndView, cause);
		this.pushCCNamesToModel(modelAndView);
		return modelAndView;
	}

	protected void pushCCNamesToModel(ModelAndView modelAndView) {
		List<String> ccNames = null;

		try {
			ccNames = dcrLogInService.displayCCNames();
		} catch (ServiceException e) {
			LOGGER.error(e);

			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("System is unable to display Customer Center Names "
						+ "hence we will force the controller to be redirected to the default log in page");
			}

			modelAndView.setView(new RedirectView("/login/index.html"));
		}

		modelAndView.addObject("ccNames", ccNames);
	}

	/**
	 * An Encapsulated getter in place for
	 * {@link HttpSession#getAttribute(String)}.
	 * 
	 * @param <T>
	 *            the generic type
	 * @param request
	 *            the request
	 * @param key
	 *            the key
	 * @param classType
	 *            the class type
	 * @return the object from session
	 */
	@SuppressWarnings("unchecked")
	protected <T> T getObjectFromSession(HttpServletRequest request,
			String key, Class<T> classType) {
		HttpSession session = request.getSession(false);

		T object = null;
		if (session != null) {
			object = (T) session.getAttribute(key);
		}

		return object;
	}

	/**
	 * Retrieves the {@link UserSession} object from the current session.
	 * 
	 * @param request
	 *            the request
	 * @return {@link UserSession}
	 */
	protected UserSession getUserSession(HttpServletRequest request) {
		UserSession userSession = this.getObjectFromSession(request,
				WMSConstants.USER_SESSION, UserSession.class);
		return userSession;
	}

	/**
	 * Replaces the current {@link UserSession} from the {@link HttpSession}.
	 * 
	 * @param request
	 * @param userSession
	 */
	protected void replaceUserSession(HttpServletRequest request,
			UserSession userSession) {
		this.removeObjectToSession(request, WMSConstants.USER_SESSION);

		HttpSession session = request.getSession(false);

		if (session != null) {
			session.setAttribute(WMSConstants.USER_SESSION, userSession);
		}
	}

	/**
	 * Encapsulates the manual removing of objects from {@link HttpSession}.
	 * 
	 * @param request
	 *            the request
	 * @param key
	 *            the key {@link HttpServletRequest} in adding more attributes
	 *            on to it.
	 */
	protected void removeObjectToSession(HttpServletRequest request, String key) {
		HttpSession session = request.getSession(false);

		if (session != null) {
			session.removeAttribute(key);
		}
	}

	public void setDcrLogInService(DCRLogInService dcrLogInService) {
		this.dcrLogInService = dcrLogInService;
	}

	public void setCachingService(CachingService cachingService) {
		this.cachingService = cachingService;
	}

    /**
     * Displays list of users valid in DCR
     * 
     * Change according to Healthcheck
     * Problem was declared as public/protected
     * Change was declare as private and set getter method
     * 
     * @return the ALL_ROLES
     */
    public Role[] getALL_ROLES() {
        return ALL_ROLES;
    }
}
