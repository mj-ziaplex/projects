package ph.com.sunlife.wms.services.impl;

import static ph.com.sunlife.wms.util.WMSConstants.LINE_SEPARATOR;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import ph.com.sunlife.wms.dao.DCRBalancingToolDao;
import ph.com.sunlife.wms.dao.DCRReversalDao;
import ph.com.sunlife.wms.dao.domain.Currency;
import ph.com.sunlife.wms.dao.domain.DCRBalancingTool;
import ph.com.sunlife.wms.dao.domain.DCRBalancingToolProduct;
import ph.com.sunlife.wms.dao.domain.DCRReversal;
import ph.com.sunlife.wms.dao.domain.ProductType;
import ph.com.sunlife.wms.dao.exceptions.WMSDaoException;
import ph.com.sunlife.wms.services.DCRReversalService;
import ph.com.sunlife.wms.services.bo.DCRReversalBO;
import ph.com.sunlife.wms.services.exception.ServiceException;

/**
 * The implementing class of {@link DCRReversalService}.
 * 
 * @author Zainal Limpao
 * 
 */
public class DCRReversalServiceImpl implements DCRReversalService {

	private static final Logger LOGGER = Logger
			.getLogger(DCRReversalServiceImpl.class);

	private DCRReversalDao dcrReversalDao;

	private DCRBalancingToolDao dcrBalancingToolDao;

    /*
     * (non-Javadoc)
     * 
     * @see
     * ph.com.sunlife.wms.services.DCRReversalService#getAllByBalancingToolProduct
     * (java.lang.Long)
     */
    @Override
    public List<DCRReversalBO> getAllByBalancingToolProduct(
            Long dcrBalancingToolProductId) throws ServiceException {
        
        LOGGER.debug("Retrieving all Reversals for this product...");
        List<DCRReversalBO> results = null;
        try {
            List<DCRReversal> entities = dcrReversalDao.getAllByBalancingToolProduct(dcrBalancingToolProductId);

            if (CollectionUtils.isNotEmpty(entities)) {
                results = new ArrayList<DCRReversalBO>();

                for (DCRReversal entity : entities) {
                    DCRReversalBO businessObject = convertToBusinessObject(entity);
                    results.add(businessObject);
                }

                if (LOGGER.isDebugEnabled()) {
                    LOGGER.debug("Found " + results.size() + " reversal entries");
                }
            }

            DCRBalancingToolProduct prod = dcrReversalDao.getBalancingToolProductById(dcrBalancingToolProductId);
            // Change according to Healthcheck
            // *Problem as == is inconsistent in checking floating points
            // *Change to compare to check value of two double amounts
            //   NOTE: doubleToRawIntBits was recommended but used compare
            //    as dev would easily understand method without looking up java notes
            //    and compare method also uses doubleToLongBits prod.getWorksiteAmount() != 0.0
            if (prod != null && (Double.compare(prod.getWorksiteAmount(), 0.0) == 0)) {
                ProductType type = prod.getProductType();
                Currency currency = type.getCurrency();

                Long dcrBalancingToolId = prod.getDcrBalancingTool().getId();
                if (dcrBalancingToolId != null) {
                    DCRBalancingTool balancingTool = dcrBalancingToolDao.getById(dcrBalancingToolId);

                    Date thisDate = null;
                    if (balancingTool.getDateReConfirmed() != null) {
                        thisDate = balancingTool.getDateReConfirmed();
                    } else if (balancingTool.getDateConfirmed() != null) {
                        thisDate = balancingTool.getDateConfirmed();
                    }

                    if (thisDate != null) {
                        DCRReversalBO worksiteEntry = new DCRReversalBO();
                        worksiteEntry.setAmount(prod.getWorksiteAmount());
                        worksiteEntry.setDateTime(thisDate);
                        worksiteEntry.setRemarks("Manually Inputted Worksite");
                        worksiteEntry.setCurrency(currency);

                        if (results != null) {
                            results.add(worksiteEntry);
                        } else {
                            results = new ArrayList<DCRReversalBO>();
                            results.add(worksiteEntry);
                        }
                    }
                }
            }

            return results;
        } catch (WMSDaoException e) {
            LOGGER.error(e);
            throw new ServiceException(e);
        }

    }

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ph.com.sunlife.wms.services.DCRReversalService#deleteById(java.lang.Long)
	 */
	@Override
	public boolean deleteById(Long id) throws ServiceException {
		try {
			boolean isDeleted = dcrReversalDao.deleteById(id);
			return isDeleted;
		} catch (WMSDaoException e) {
			LOGGER.error(e);
			throw new ServiceException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ph.com.sunlife.wms.services.DCRReversalService#updateReversal(ph.com.
	 * sunlife.wms.services.bo.DCRReversalBO)
	 */
	@Override
	public void updateReversal(DCRReversalBO dcrReversal)
			throws ServiceException {
		try {
			DCRReversal entity = convertToEntity(dcrReversal);
			dcrReversalDao.updateReversal(entity);
		} catch (WMSDaoException e) {
			LOGGER.error(e);
			throw new ServiceException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ph.com.sunlife.wms.services.DCRReversalService#save(ph.com.sunlife.wms
	 * .services.bo.DCRReversalBO)
	 */
	@Override
	public DCRReversalBO save(DCRReversalBO dcrReversal)
			throws ServiceException {
		DCRReversal entity = convertToEntity(dcrReversal);
		try {
			entity = dcrReversalDao.save(entity);
			dcrReversal = convertToBusinessObject(entity);
			return dcrReversal;
		} catch (WMSDaoException e) {
			LOGGER.error(e);
			throw new ServiceException(e);
		}
	}

	/**
	 * Converts to {@link DCRReversal}. Note that BeanUtils wasn't used for this
	 * because some properties are custom objects.
	 * 
	 * @param businessObject
	 * @return
	 */
	private DCRReversal convertToEntity(DCRReversalBO businessObject) {
		DCRReversal entity = new DCRReversal();
		entity.setAmount(businessObject.getAmount());
		entity.setCurrencyId(businessObject.getCurrencyId());
		entity.setDateTime(businessObject.getDateTime());

		String remarksWithoutLineBreaks = businessObject.getRemarks().replace(
				LINE_SEPARATOR, " ");

		entity.setRemarks(remarksWithoutLineBreaks);
		entity.setDcrBalancingToolProductId(businessObject
				.getDcrBalancingToolProductId());

		if (businessObject.getId() != null) {
			entity.setId(businessObject.getId());
		}

		return entity;
	}

	/**
	 * Converts to {@link DCRReversalBO}. Note that BeanUtils wasn't used for
	 * this because some properties are custom objects.
	 * 
	 * @param entity
	 * @return
	 */
	private DCRReversalBO convertToBusinessObject(DCRReversal entity) {
		DCRReversalBO businessObject = new DCRReversalBO();
		businessObject.setAmount(entity.getAmount());
		businessObject.setCurrencyId(entity.getCurrencyId());
		businessObject.setDateTime(entity.getDateTime());

		String remarks = entity.getRemarks();
		String remarksWithoutLineBreaks = remarks;

		remarksWithoutLineBreaks = StringUtils.replace(remarks, LINE_SEPARATOR,
				" ");
		remarksWithoutLineBreaks = StringUtils.replace(remarks, "& #39;", "'");

		businessObject.setRemarks(remarksWithoutLineBreaks);
		businessObject.setDcrBalancingToolProductId(entity
				.getDcrBalancingToolProductId());
		businessObject.setId(entity.getId());
		return businessObject;
	}

	public void setDcrReversalDao(DCRReversalDao dcrReversalDao) {
		this.dcrReversalDao = dcrReversalDao;
	}

	public void setDcrBalancingToolDao(DCRBalancingToolDao dcrBalancingToolDao) {
		this.dcrBalancingToolDao = dcrBalancingToolDao;
	}

}
