package ph.com.sunlife.wms.dao;

import ph.com.sunlife.wms.dao.exceptions.WMSDaoException;

public interface DCRCandidateMatchDao {
	
	public void deleteAttachment(Long recordId, String recordLocation) throws WMSDaoException;

}
