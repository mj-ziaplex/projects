package ph.com.sunlife.wms.dao.impl;

import java.sql.SQLException;
import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.orm.ibatis.SqlMapClientCallback;

import ph.com.sunlife.wms.dao.AbstractWMSSqlMapClientDaoSupport;
import ph.com.sunlife.wms.dao.DCRReversalDao;
import ph.com.sunlife.wms.dao.domain.DCRBalancingToolProduct;
import ph.com.sunlife.wms.dao.domain.DCRReversal;
import ph.com.sunlife.wms.dao.exceptions.WMSDaoException;

import com.ibatis.sqlmap.client.SqlMapExecutor;
import org.apache.log4j.Logger;

/**
 * The implementing class for {@link DCRReversalDao}.
 * 
 * @author Zainal Limpao
 * 
 */
public class DCRReversalDaoImpl extends
		AbstractWMSSqlMapClientDaoSupport<DCRReversal> implements
		DCRReversalDao {

	private static final String NAMESPACE = "DCRReversal";
        private static final Logger LOGGER = Logger.getLogger(DCRReversalDaoImpl.class);

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ph.com.sunlife.wms.dao.AbstractWMSSqlMapClientDaoSupport#getNamespace()
	 */
	@Override
	protected String getNamespace() {
		return NAMESPACE;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ph.com.sunlife.wms.dao.DCRReversalDao#getAllByBalancingToolProduct(java
	 * .lang.Long)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<DCRReversal> getAllByBalancingToolProduct(
			final Long dcrBalancingToolProductId) throws WMSDaoException {
		List<DCRReversal> list = null;

		try {
			list = (List<DCRReversal>) getSqlMapClientTemplate().execute(
					new SqlMapClientCallback() {

						@Override
						public Object doInSqlMapClient(SqlMapExecutor executor)
								throws SQLException {
							return executor.queryForList(getNamespace()
									+ ".getAllByBalancingToolProduct",
									dcrBalancingToolProductId);
						}
					});
		} catch (Exception e) {
			throw new WMSDaoException(e);
		}
		return list;
	}

    /*
     * (non-Javadoc)
     * 
     * @see ph.com.sunlife.wms.dao.DCRReversalDao#deleteById(java.lang.Long)
     */
    @Override
    public boolean deleteById(Long id) throws WMSDaoException {
        boolean success = false;
        try {
            // Change according to Healthcheck
            // *Problem was identified value being set to catch and no log4j logging
            // *Change was sql result store in int and return proper boolean based on result
            int x = getSqlMapClientTemplate().delete(getNamespace() + ".deleteById", id);

            if (x > 0) {
                success = true;
            }
        } catch (DataAccessException e) {
            // Change according to Healthcheck
            // *Problem was identified value being set to catch and no log4j logging
            // *Change was removed setting of variable value and log error through log4j
            LOGGER.error("Error in removing reversal: ", e);
        } catch (Exception e) {
            throw new WMSDaoException(e);
        }

        return success;
    }

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ph.com.sunlife.wms.dao.DCRReversalDao#updateReversal(ph.com.sunlife.wms
	 * .dao.domain.DCRReversal)
	 */
	@Override
	public void updateReversal(DCRReversal dcrReversal) throws WMSDaoException {
		try {
			getSqlMapClientTemplate().update(
					getNamespace() + ".updateReversal", dcrReversal);
		} catch (Exception e) {
			throw new WMSDaoException(e);
		}
	}

	@Override
	public DCRBalancingToolProduct getBalancingToolProductById(Long id)
			throws WMSDaoException {

		try {
			DCRBalancingToolProduct prod = (DCRBalancingToolProduct) getSqlMapClientTemplate()
					.queryForObject(
							getNamespace() + ".getBalancingToolProductById", id);

			return prod;
		} catch (Exception ex) {
			throw new WMSDaoException(ex);
		}
	}
}
