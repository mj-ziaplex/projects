package ph.com.sunlife.wms.services.bo;

import java.util.List;

public class ConsolidatedCashierTotals {

	private ConsolidatedData parent;
	
	private String acf2id;
	
	private String cashierFullName;
	
	private List<ConsolidatedCashierProductTotals> productTotals;

	public ConsolidatedData getParent() {
		return parent;
	}

	public void setParent(ConsolidatedData parent) {
		this.parent = parent;
	}

	public String getAcf2id() {
		return acf2id;
	}

	public void setAcf2id(String acf2id) {
		this.acf2id = acf2id;
	}

	public List<ConsolidatedCashierProductTotals> getProductTotals() {
		return productTotals;
	}

	public void setProductTotals(
			List<ConsolidatedCashierProductTotals> productTotals) {
		this.productTotals = productTotals;
	}

	public String getCashierFullName() {
		return cashierFullName;
	}

	public void setCashierFullName(String cashierFullName) {
		this.cashierFullName = cashierFullName;
	}
	
}
