package ph.com.sunlife.wms.web.controller;

import static ph.com.sunlife.wms.web.view.WMSJasperPdfView.JASPER_PRINT_KEY;

import java.beans.PropertyEditor;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import ph.com.sunlife.wms.security.Role;
import ph.com.sunlife.wms.services.DCRVDILService;
import ph.com.sunlife.wms.services.bo.DCRCashierBO;
import ph.com.sunlife.wms.services.bo.DCRDollarCoinCollectionBO;
import ph.com.sunlife.wms.services.bo.DCRDollarCoinExchangeBO;
import ph.com.sunlife.wms.services.bo.DCRVDILBO;
import ph.com.sunlife.wms.services.bo.DCRVDILISOCollectionBO;
import ph.com.sunlife.wms.services.bo.DCRVDILPRSeriesNumberBO;
import ph.com.sunlife.wms.services.bo.UserSession;
import ph.com.sunlife.wms.services.bo.VDILCollectionTotalsBO;
import ph.com.sunlife.wms.services.exception.ServiceException;
import ph.com.sunlife.wms.util.ParameterMap;
import ph.com.sunlife.wms.util.WMSDateUtil;
import ph.com.sunlife.wms.web.controller.form.CashierWorkItemForm;
import ph.com.sunlife.wms.web.controller.form.VDILForm;
import ph.com.sunlife.wms.web.exception.ApplicationException;
import ph.com.sunlife.wms.web.view.WMSJasperPdfView;

/**
 * The {@link Controller} responsible for Vault Daily Inventory List Creation.
 * 
 * @author
 * 
 */
public class CreateVDILController extends AbstractCashierWorkItemController {

	private String jrxmlPath;

	private static Long adhocMenuIndex = 69L;

	private static final String IS_SAVED_KEY = "isSaved";

	private static final Logger LOGGER = Logger
			.getLogger(CreateVDILController.class);

	private String pageTitle;

	private String displayViewName;

	private DCRVDILService dcrVDILService;

	public final ModelAndView doShowCreateVDILForm(HttpServletRequest request,
			HttpServletResponse response, CashierWorkItemForm form)
			throws ApplicationException {
		LOGGER.info("Loading Default Page for VDIL Creation...");
		
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName(this.getDisplayViewName());

		Long dcrCashierId = form.getDcrCashierId();
		String acf2id = getUserSession(request).getUserId();
		// System.out.println("dcrCashierId: " + dcrCashierId);
		DCRVDILBO businessObject = null;
		boolean isSaved = false;
		try {
			businessObject = dcrVDILService.getVdil(dcrCashierId, acf2id);

			if (businessObject == null) {
				// System.out.println("CALL CREATE NEW VDIL, BCOZ GET VDIL RETURNED NULL");
				// never called?
				businessObject = dcrVDILService.createNewVdil(dcrCashierId);
			}

			DCRDollarCoinCollectionBO previousCollection = businessObject
					.getPreviousCoinCollection();
			// System.out.println("doShowCreateVDILForm previousCollection: " +
			// previousCollection);
			if (previousCollection == null) {
				DCRDollarCoinCollectionBO emptyPreviousCoinCollection = new DCRDollarCoinCollectionBO();
				emptyPreviousCoinCollection.setDenom1(0);
				emptyPreviousCoinCollection.setDenom1c(0);
				emptyPreviousCoinCollection.setDenom5c(0);
				emptyPreviousCoinCollection.setDenom10c(0);
				emptyPreviousCoinCollection.setDenom25c(0);
				emptyPreviousCoinCollection.setDenom50c(0);
				emptyPreviousCoinCollection.setDcrvdil(businessObject.getId());
				emptyPreviousCoinCollection.setDateCreated(WMSDateUtil
						.getPreviousDate(businessObject.getDcrDate()));// set to
																		// previous
																		// 'DCR'
																		// Date
				businessObject
						.setPreviousCoinCollection(emptyPreviousCoinCollection);
			}

			modelAndView.addObject("vdil", businessObject);
			isSaved = (businessObject != null && businessObject.getDateSaved() != null);
			modelAndView.addObject(IS_SAVED_KEY, isSaved);
		} catch (ServiceException e) {
			LOGGER.error(e);
			throw new ApplicationException(e);
		}

		modelAndView.addObject(PAGE_SEQUENCE_KEY, adhocMenuIndex);

		return modelAndView;
	}

	@Override
	protected void registerAdditionalPropertyEditors(
			ServletRequestDataBinder binder) throws Exception {
		PropertyEditor moneyEditor = createMoneyPropertyEditor();

		binder.registerCustomEditor(Double.class, "afterCutoffCashPeso",
				moneyEditor);
		binder.registerCustomEditor(Double.class, "afterCutoffCashDollar",
				moneyEditor);

		binder.registerCustomEditor(Double.class, "rcvdPrevDay", moneyEditor);
		binder.registerCustomEditor(Double.class, "rcvdToday", moneyEditor);
		binder.registerCustomEditor(Double.class, "lessChangeForTheDay",
				moneyEditor);

		binder.registerCustomEditor(Double.class, "photocopy", moneyEditor);
		binder.registerCustomEditor(Double.class, "workingFund", moneyEditor);
		binder.registerCustomEditor(Double.class, "overages", moneyEditor);
		binder.registerCustomEditor(Double.class, "pettyCash", moneyEditor);
		binder.registerCustomEditor(Double.class, "unclaimedChange",
				moneyEditor);

		binder.registerCustomEditor(Double.class, "to0", moneyEditor);
		binder.registerCustomEditor(Double.class, "to1", moneyEditor);
		binder.registerCustomEditor(Double.class, "to2", moneyEditor);
		binder.registerCustomEditor(Double.class, "to3", moneyEditor);

		binder.registerCustomEditor(Double.class, "from0", moneyEditor);
		binder.registerCustomEditor(Double.class, "from1", moneyEditor);
		binder.registerCustomEditor(Double.class, "from2", moneyEditor);
		binder.registerCustomEditor(Double.class, "from3", moneyEditor);
		binder.registerCustomEditor(Double.class, "amount0", moneyEditor);
		binder.registerCustomEditor(Double.class, "amount1", moneyEditor);
		binder.registerCustomEditor(Double.class, "amount2", moneyEditor);
		binder.registerCustomEditor(Double.class, "amount3", moneyEditor);
	}

	public final ModelAndView saveVDIL(HttpServletRequest request,
			HttpServletResponse response, VDILForm form)
			throws ApplicationException {
		LOGGER.info("Attempting to Save VDIL...");
		
		ModelAndView mv = this.doShowCreateVDILForm(request, response, form);
		try {
			DCRVDILBO businessObject = (DCRVDILBO) mv.getModel().get("vdil");
			updateBusinessObjectFromForm(form, businessObject);

			// start: for attaching vdil pdf file
			byte[] data = null;
			ModelAndView mvPDF = doGenerateVDIL(request, response, form);
			JasperPrint jp = (JasperPrint) mvPDF.getModel().get(
					JASPER_PRINT_KEY);
			data = JasperExportManager.exportReportToPdf(jp);
			businessObject.setVdilFile(data);

			UserSession userSession = getUserSession(request);
			String centerCode = userSession.getSiteCode();
			businessObject.setCustomerCenter(centerCode);
			String userId = userSession.getUserId();
			businessObject.setUserId(userId);
			// end: for attaching vdil pdf file

			businessObject = dcrVDILService.updateVdil(businessObject);
			mv.addObject("vdil", businessObject);
			boolean isSaved = businessObject.getDateSaved() != null;
			// System.out.println("saveVDIL isSaved: "+isSaved);
			mv.addObject(IS_SAVED_KEY, isSaved);
		} catch (ServiceException e) {
			LOGGER.error(e);
			throw new ApplicationException(e);
		} catch (JRException e) {
			LOGGER.error(e);
			throw new ApplicationException(e);
		}

		LOGGER.info("Saving Successful");
		return mv;
	}

	private void updateBusinessObjectFromForm(VDILForm form,
			DCRVDILBO businessObject) {
		businessObject.setAfterCutoffCard(form.getAfterCutoffCard());
		businessObject
				.setAfterCutoffCashDollar(form.getAfterCutoffCashDollar());
		businessObject.setAfterCutoffCashPeso(form.getAfterCutoffCashPeso());
		businessObject.setAfterCutoffCheck(form.getAfterCutoffCheck());
		DCRCashierBO dcrCashierBO = new DCRCashierBO();
		dcrCashierBO.setId(form.getDcrCashierId());
		businessObject.setDcrCashier(dcrCashierBO);
		businessObject.setForSafeKeeping(form.getForSafeKeeping());
		businessObject.setId(form.getVdilId());
		businessObject.setOverages(form.getOverages());
		businessObject.setPettyCash(form.getPettyCash());
		businessObject.setPhotocopy(form.getPhotocopy());
		businessObject
				.setPnPDCPendingWarehouse(form.getPnPDCPendingWarehouse());
		businessObject.setTotalDollarCoins(form.getTotalDollarCoins());
		businessObject.setUnclaimedChange(form.getUnclaimedChange());
		businessObject.setVultradPDCRPendingWarehouse(form
				.getVultradPDCRPendingWarehouse());
		businessObject.setWorkingFund(form.getWorkingFund());

		List<DCRVDILPRSeriesNumberBO> prSeriesNumberBOs = businessObject
				.getPrSeriesNumbers();// new
										// ArrayList<DCRVDILPRSeriesNumberBO>();

		DCRVDILPRSeriesNumberBO subPrSeriesBO0 = prSeriesNumberBOs.get(0);// new
																			// DCRVDILPRSeriesNumberBO();
		subPrSeriesBO0.setCompanyId(form.getCompanyId0());
		subPrSeriesBO0.setDcrvdil(businessObject);
		subPrSeriesBO0.setFrom(form.getFrom0());
		subPrSeriesBO0.setTo(form.getTo0());
		subPrSeriesBO0.setId(form.getPrSeriesNumberId0());
		subPrSeriesBO0.setReason(form.getReason0());
		prSeriesNumberBOs.add(subPrSeriesBO0);

		DCRVDILPRSeriesNumberBO subPrSeriesBO1 = prSeriesNumberBOs.get(1);
		subPrSeriesBO1.setCompanyId(form.getCompanyId1());
		subPrSeriesBO1.setDcrvdil(businessObject);
		subPrSeriesBO1.setFrom(form.getFrom1());
		subPrSeriesBO1.setTo(form.getTo1());
		subPrSeriesBO1.setId(form.getPrSeriesNumberId1());
		subPrSeriesBO1.setReason(form.getReason1());
		prSeriesNumberBOs.add(subPrSeriesBO1);

		DCRVDILPRSeriesNumberBO subPrSeriesBO2 = prSeriesNumberBOs.get(2);
		subPrSeriesBO2.setCompanyId(form.getCompanyId2());
		subPrSeriesBO2.setDcrvdil(businessObject);
		subPrSeriesBO2.setFrom(form.getFrom2());
		subPrSeriesBO2.setTo(form.getTo2());
		subPrSeriesBO2.setId(form.getPrSeriesNumberId2());
		subPrSeriesBO2.setReason(form.getReason2());
		prSeriesNumberBOs.add(subPrSeriesBO2);

		DCRVDILPRSeriesNumberBO subPrSeriesBO3 = prSeriesNumberBOs.get(3);
		subPrSeriesBO3.setCompanyId(form.getCompanyId3());
		subPrSeriesBO3.setDcrvdil(businessObject);
		subPrSeriesBO3.setFrom(form.getFrom3());
		subPrSeriesBO3.setTo(form.getTo3());
		subPrSeriesBO3.setId(form.getPrSeriesNumberId3());
		subPrSeriesBO3.setReason(form.getReason3());
		prSeriesNumberBOs.add(subPrSeriesBO3);

		businessObject.setPrSeriesNumbers(prSeriesNumberBOs);

		List<DCRVDILISOCollectionBO> isoCollectionBOs = businessObject
				.getIsoCollections();// new ArrayList<DCRVDILISOCollectionBO>();

		DCRVDILISOCollectionBO subIsoCollectionBO0 = isoCollectionBOs.get(0);// new
																				// DCRVDILISOCollectionBO();
		subIsoCollectionBO0.setDcrvdil(businessObject);
		subIsoCollectionBO0.setAmount(form.getAmount0());
		subIsoCollectionBO0.setDcrDate(WMSDateUtil.toDate(form.getDcrDate0()));
		subIsoCollectionBO0.setId(form.getIsoCollectionId0());
		subIsoCollectionBO0.setPickupDate(WMSDateUtil.toDate(form
				.getPickupDate0()));
		isoCollectionBOs.add(subIsoCollectionBO0);

		DCRVDILISOCollectionBO subIsoCollectionBO1 = isoCollectionBOs.get(1);
		subIsoCollectionBO1.setDcrvdil(businessObject);
		subIsoCollectionBO1.setAmount(form.getAmount1());
		subIsoCollectionBO1.setDcrDate(WMSDateUtil.toDate(form.getDcrDate1()));
		subIsoCollectionBO1.setId(form.getIsoCollectionId1());
		subIsoCollectionBO1.setPickupDate(WMSDateUtil.toDate(form
				.getPickupDate1()));
		isoCollectionBOs.add(subIsoCollectionBO1);

		DCRVDILISOCollectionBO subIsoCollectionBO2 = isoCollectionBOs.get(2);
		subIsoCollectionBO2.setDcrvdil(businessObject);
		subIsoCollectionBO2.setAmount(form.getAmount2());
		subIsoCollectionBO2.setDcrDate(WMSDateUtil.toDate(form.getDcrDate2()));
		subIsoCollectionBO2.setId(form.getIsoCollectionId2());
		subIsoCollectionBO2.setPickupDate(WMSDateUtil.toDate(form
				.getPickupDate2()));
		isoCollectionBOs.add(subIsoCollectionBO2);

		DCRVDILISOCollectionBO subIsoCollectionBO3 = isoCollectionBOs.get(3);
		subIsoCollectionBO3.setDcrvdil(businessObject);
		subIsoCollectionBO3.setAmount(form.getAmount3());
		subIsoCollectionBO3.setDcrDate(WMSDateUtil.toDate(form.getDcrDate3()));
		subIsoCollectionBO3.setId(form.getIsoCollectionId3());
		subIsoCollectionBO3.setPickupDate(WMSDateUtil.toDate(form
				.getPickupDate3()));
		isoCollectionBOs.add(subIsoCollectionBO3);

		businessObject.setIsoCollections(isoCollectionBOs);

		DCRDollarCoinCollectionBO previousCollection = businessObject
				.getPreviousCoinCollection();

		DCRDollarCoinCollectionBO currentCollection = businessObject
				.getCurrentCoinCollection();

		DCRDollarCoinExchangeBO exchange = businessObject
				.getDollarCoinExchange();

		/*
		 * System.out.println("CHECK IF NOT NULL UNG TEST DATA GOTTEN FROM DB");
		 * System.out.println("previousCollection: " + previousCollection);
		 * System.out.println("currentCollection: " + currentCollection);
		 * System.out.println("exchange: " + exchange);
		 * 
		 * System.out
		 * .println("DATA FROM DCRDollarCoinCollection WITH yesterday's date");
		 * System.out.println("prev1 " + previousCollection.getDenom1());
		 * System.out.println("prev50c " + previousCollection.getDenom50c());
		 * System.out.println("prev25c " + previousCollection.getDenom25c());
		 * System.out.println("prev10c " + previousCollection.getDenom10c());
		 * System.out.println("prev5c " + previousCollection.getDenom5c());
		 * System.out.println("prev1c " + previousCollection.getDenom1c());
		 * 
		 * System.out.println("in1 " + form.getIn_denom1());
		 * System.out.println("in50c " + form.getIn_denom50c());
		 * System.out.println("in25c " + form.getIn_denom25c());
		 * System.out.println("in10c " + form.getIn_denom10c());
		 * System.out.println("in5c " + form.getIn_denom5c());
		 * System.out.println("in1c " + form.getIn_denom1c());
		 * 
		 * System.out.println("out1 " + form.getOut_denom1());
		 * System.out.println("out50c " + form.getOut_denom50c());
		 * System.out.println("out25c " + form.getOut_denom25c());
		 * System.out.println("out10c " + form.getOut_denom10c());
		 * System.out.println("out5c " + form.getOut_denom5c());
		 * System.out.println("out1c " + form.getOut_denom1c());
		 */

		exchange.setIncomingDenom1(form.getIn_denom1());
		exchange.setOutgoingDenom1(form.getOut_denom1());
		exchange.setIncomingDenom50c(form.getIn_denom50c());
		exchange.setOutgoingDenom50c(form.getOut_denom50c());
		exchange.setIncomingDenom25c(form.getIn_denom25c());
		exchange.setOutgoingDenom25c(form.getOut_denom25c());
		exchange.setIncomingDenom10c(form.getIn_denom10c());
		exchange.setOutgoingDenom10c(form.getOut_denom10c());
		exchange.setIncomingDenom5c(form.getIn_denom5c());
		exchange.setOutgoingDenom5c(form.getOut_denom5c());
		exchange.setIncomingDenom1c(form.getIn_denom1c());
		exchange.setOutgoingDenom1c(form.getOut_denom1c());
		// exchange.setDateCreated(new Date());
		// .setDcrvdil(businessObject);
		businessObject.setDollarCoinExchange(exchange);

		int denom1NetToday = 0;
		int denom50cNetToday = 0;
		int denom25cNetToday = 0;
		int denom10cNetToday = 0;
		int denom5cNetToday = 0;
		int denom1cNetToday = 0;

		if (previousCollection != null) {
			denom1NetToday = previousCollection.getDenom1()
					+ form.getIn_denom1().intValue()
					- form.getOut_denom1().intValue();
			denom50cNetToday = previousCollection.getDenom50c()
					+ form.getIn_denom50c() - form.getOut_denom50c();
			denom25cNetToday = previousCollection.getDenom25c()
					+ form.getIn_denom25c() - form.getOut_denom25c();
			denom10cNetToday = previousCollection.getDenom10c()
					+ form.getIn_denom10c() - form.getOut_denom10c();
			denom5cNetToday = previousCollection.getDenom5c()
					+ form.getIn_denom5c() - form.getOut_denom5c();
			denom1cNetToday = previousCollection.getDenom1c()
					+ form.getIn_denom1c() - form.getOut_denom1c();
		}
		/*
		 * int denom1NetToday = previousCollection.getDenom1() +
		 * form.getIn_denom1().intValue() - form.getOut_denom1().intValue(); int
		 * denom50cNetToday = previousCollection.getDenom50c() +
		 * form.getIn_denom50c() - form.getOut_denom50c(); int denom25cNetToday
		 * = previousCollection.getDenom25c() + form.getIn_denom25c() -
		 * form.getOut_denom25c(); int denom10cNetToday =
		 * previousCollection.getDenom10c() + form.getIn_denom10c() -
		 * form.getOut_denom10c(); int denom5cNetToday =
		 * previousCollection.getDenom5c() + form.getIn_denom5c() -
		 * form.getOut_denom5c(); int denom1cNetToday =
		 * previousCollection.getDenom1c() + form.getIn_denom1c() -
		 * form.getOut_denom1c();
		 */

		/*
		 * System.out .println(
		 * "AS SEEN IN THE COLUMN NET PIECES (BUT COMPUTED AGAIN HERE IN JAVA)"
		 * ); System.out.println("denom1NetToday: " + denom1NetToday);
		 * System.out.println("denom50cNetToday: " + denom50cNetToday);
		 * System.out.println("denom25cNetToday: " + denom25cNetToday);
		 * System.out.println("denom10cNetToday: " + denom10cNetToday);
		 * System.out.println("denom5cNetToday: " + denom5cNetToday);
		 * System.out.println("denom1cNetToday: " + denom1cNetToday);
		 */

		currentCollection.setDenom1(denom1NetToday);
		currentCollection.setDenom50c(denom50cNetToday);
		currentCollection.setDenom25c(denom25cNetToday);
		currentCollection.setDenom10c(denom10cNetToday);
		currentCollection.setDenom5c(denom5cNetToday);
		currentCollection.setDenom1c(denom1cNetToday);
		// currentCollection.setDateCreated(new Date());
		// .setDcrvdil(businessObject);
		businessObject.setCurrentCoinCollection(currentCollection);
		businessObject.setDateSaved(new Date());
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public ModelAndView doGenerateVDIL(HttpServletRequest request,
			HttpServletResponse response, VDILForm form)
			throws ApplicationException {
		LOGGER.info("Generating PDF file for VDIL...");

		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName(this.getDisplayViewName());

		try {
			Long dcrCashierId = form.getDcrCashierId();

			DCRCashierBO dcrCashierBO = getDCRCashierBO(dcrCashierId,
					getUserSession(request), modelAndView);
			Date dcrDate = dcrCashierBO.getDcr().getDcrDate();

			// 31OCT2012/A2/3/1/3
			// DCR Date: 31OCT2012
			// Site Code: A2
			// Company: 3 (SLOCPI)
			// Currency: 1 (Peso)
			// Payment Type: 3 (Cheque Local)

			// sobra na sa 15 chars, pano un?
			// 31OCT2012/A2/WC01/2
			// DCR Date: 31OCT2012
			// Site Code: A2
			// CashierId: WC01
			// Document Type: 2 (VDIL document)

			UserSession userSession = getUserSession(request);
			String acf2id = userSession.getUserId();
			String siteCode = userSession.getSiteCode();// aka centerCode aka
														// customerCenter
			String ccName = dcrCashierBO.getDcr().getCcName();
			DCRVDILBO businessObject = dcrVDILService.getVdil(dcrCashierId,
					acf2id);
			int pdcReceivedForTheDayTotal = businessObject
					.getPdcReceivedForTheDayTotal();

			String preparedByCompleteName = "";
			String preparedByLastName = businessObject.getDcrCashier()
					.getCashier().getLastname();
			String preparedByFirstName = businessObject.getDcrCashier()
					.getCashier().getFirstname();
			String preparedByMiddleName = businessObject.getDcrCashier()
					.getCashier().getMiddlename();
			preparedByCompleteName = preparedByLastName + ", "
					+ preparedByFirstName + " " + preparedByMiddleName;

			// System.out.println("preparedByCompleteName: "+preparedByCompleteName);

			VDILCollectionTotalsBO collectionTotals = dcrVDILService
					.generateCollectionTotals(businessObject);

			LOGGER.info("Attempting to load Jasper Design...");
			JasperDesign jasperDesign = JRXmlLoader.load(getServletContext()
					.getRealPath(jrxmlPath));

			if (jasperDesign != null) {
				LOGGER.info("Jasper Design has been loaded successfully");
			}

			LOGGER.info("Attempting to load Compiled Jasper Report...");
			JasperReport jasperReport = JasperCompileManager
					.compileReport(jasperDesign);
			
			if (jasperReport != null) {
				LOGGER.info("Jasper Report has been loaded successfully");
			}

			ParameterMap map = new ParameterMap(form);// form's variables must
														// equal jrxml's params

			ParameterMap collectionTotalsMap = new ParameterMap(collectionTotals);
			map.putAll(collectionTotalsMap);
			map.put("dcrDate", StringUtils.upperCase(WMSDateUtil.toFormattedDateStr(dcrDate)));// returned
																		// format
																		// is
																		// ddMMMyyyy
			map.put("pdcReceivedForTheDayTotal", pdcReceivedForTheDayTotal);
			map.put("preparedByCompleteName", preparedByCompleteName);
			map.put("siteCode", siteCode);
			map.put("ccName", ccName);
			map.put("acf2id", acf2id);

			Map model = new HashMap();

			LOGGER.info("Attempting to create Jasper Print...");
			JasperPrint jasperPrint = JasperFillManager.fillReport(
					jasperReport, map);
			
			if (jasperPrint != null) {
				LOGGER.info("Jasper Print has been generated successfully...");	
			}

			model.put(JASPER_PRINT_KEY, jasperPrint);

			LOGGER.info("Creating Model and View Object...");
			return new ModelAndView(new WMSJasperPdfView(), model);

		} catch (Exception e) {
			LOGGER.error(e);
			throw new ApplicationException(e);
		}

	}

	@Override
	protected Role[] allowedRoles() {
		return new Role[] { Role.CASHIER };
	}

	@Override
	protected String getPageTitle() {
		return pageTitle;
	}

	public void setPageTitle(String pageTitle) {
		this.pageTitle = pageTitle;
	}

	public String getDisplayViewName() {
		return displayViewName;
	}

	public void setDisplayViewName(String displayViewName) {
		this.displayViewName = displayViewName;
	}

	public DCRVDILService getDcrVDILService() {
		return dcrVDILService;
	}

	public void setDcrVDILService(DCRVDILService dcrVDILService) {
		this.dcrVDILService = dcrVDILService;
	}

	public String getJrxmlPath() {
		return jrxmlPath;
	}

	public void setJrxmlPath(String jrxmlPath) {
		this.jrxmlPath = jrxmlPath;
	}
}
