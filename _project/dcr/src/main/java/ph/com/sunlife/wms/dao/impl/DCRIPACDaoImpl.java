/**
 * @author i176
 * ****************************************************************************
 * @author: ia23 - sasay
 * @date: Mar2016
 * @desc: Removed methods that will not be used due to MR-WF-15-00089 - DCR
 * Redesign
 */
package ph.com.sunlife.wms.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.transaction.annotation.Transactional;

import ph.com.sunlife.wms.dao.DCRIPACDao;
import ph.com.sunlife.wms.dao.domain.DCR;
import ph.com.sunlife.wms.dao.domain.DCRIpacValue;
import ph.com.sunlife.wms.dao.domain.GAFValue;
import ph.com.sunlife.wms.dao.domain.NonPostedTransaction;
import ph.com.sunlife.wms.dao.domain.PPAMDS;
import ph.com.sunlife.wms.dao.exceptions.WMSDaoException;

/**
 * Implementing class for interface DCRIPACDao
 */
public class DCRIPACDaoImpl extends JdbcDaoSupport implements DCRIPACDao {

    private static final Logger LOGGER = Logger.getLogger(DCRIPACDaoImpl.class);

    /* (non-Javadoc)
     * Method that get main data for cashier collection
     * @param center_code - Customer center to be used as a sql filter
     * @param process_date - DCR date to be used as a sql filter
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<DCRIpacValue> getDCRIPACDailyCollectionSummary(
            String center_code,
            Date process_date) throws WMSDaoException {
        
        LOGGER.info("Retrieving from DCRIPACDailyCollectionSummary");
        // Modified for DCR - IPAC redesign with cancellation
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT * FROM DCRIPACDailyCollectionSummary")
                .append(" WHERE center_code='").append(center_code).append("'")
                .append("   AND process_date='").append((new java.sql.Date(process_date.getTime()))).append("'")
                .append("   AND SUBSTRING(transaction_key, 0, CHARINDEX('*', transaction_key)) NOT IN (SELECT document_id FROM DCRIPACCancelTransaction WHERE cancel_date = '").append((new java.sql.Date(process_date.getTime()))).append("')");
        LOGGER.info("SQL Daily Collection Transaction: " + sql);
        return getJdbcTemplate().query(sql.toString(), new DCRIpacValueRowMapper());
    }

    /* (non-Javadoc)
     * Method that get non cash/non confirmed cashier collection
     * @param center_code - Customer center to be used as a sql filter
     * @param process_date - DCR date to be used as a sql filter
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<NonPostedTransaction> getDCRIPACDTR(
            String center_code,
            Date process_date) throws WMSDaoException {
        
        LOGGER.info("Retrieving from DCRIPACDTR");
        // Modified for DCR - IPAC redesign with cancellation
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT * FROM DCRIPACDTR")
                .append(" WHERE center_code='").append(center_code).append("'")
                .append("   AND process_date='").append((new java.sql.Date(process_date.getTime()))).append("'")
                .append("   AND SUBSTRING(transaction_key, 0, CHARINDEX('*', transaction_key)) NOT IN (SELECT document_id FROM DCRIPACCancelTransaction WHERE cancel_date = '").append((new java.sql.Date(process_date.getTime()))).append("')");
        LOGGER.info("SQL DTR Transaction: " + sql);
        return getJdbcTemplate().query(sql.toString(), new NonPostedTransactionRowMapper());
    }

    /* (non-Javadoc)
     * Method that get gaf/preneed cashier collection
     * @param center_code - Customer center to be used as a sql filter
     * @param process_date - DCR date to be used as a sql filter
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<GAFValue> getDCRIPACGAF(
            String center_code,
            Date process_date) throws WMSDaoException {
        
        LOGGER.info("Retrieving from DCRIPACGAF");
        // Modified for DCR - IPAC redesign with cancellation
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT * FROM DCRIPACGAF")
                .append(" WHERE center_code='").append(center_code).append("'")
                .append("   AND process_date='").append((new java.sql.Date(process_date.getTime()))).append("'")
                .append("   AND SUBSTRING(transaction_key, 0, CHARINDEX('*', transaction_key)) NOT IN (SELECT document_id FROM DCRIPACCancelTransaction WHERE cancel_date = '").append((new java.sql.Date(process_date.getTime()))).append("')");
        LOGGER.info("SQL GAF Transaction: " + sql);
        return getJdbcTemplate().query(sql.toString(), new GAFValueRowMapper());
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<PPAMDS> getDCRIPACMDSTransaction(String company, String center_code,
            Date from_date, Date to_date) throws WMSDaoException {
        //String sql = "SELECT * FROM DCRIPACMDSTransaction where site_code='"+center_code+ "' and com_code='"+ company +"' and (date_range_from='"+(new java.sql.Date(from_date.getTime()))+"' and date_range_to='"+(new java.sql.Date(to_date.getTime()))+"') and downloaded_date='"+(new java.sql.Date(WMSDateUtil.startOfToday().getTime()))+"'";
        LOGGER.info("Retrieving from DCRIPACMDSTransaction");
        String sql = "SELECT * FROM DCRIPACMDSTransaction where site_code='" + center_code + "' and com_code='" + company + "' and (date_range_from='" + (new java.sql.Date(from_date.getTime())) + "' and date_range_to='" + (new java.sql.Date(to_date.getTime())) + "') ";
        LOGGER.info("SQL MDS Transaction: " + sql);
        return getJdbcTemplate().query(sql, new PPAMDSRowMapper());
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<PPAMDS> getDCRIPACMDSTransaction(String center_code,
            Date process_date) throws WMSDaoException {
        //String sql = "SELECT * FROM DCRIPACMDSTransaction where site_code='"+center_code+ "' and com_code='"+ company +"' and (date_range_from='"+(new java.sql.Date(from_date.getTime()))+"' and date_range_to='"+(new java.sql.Date(to_date.getTime()))+"') and downloaded_date='"+(new java.sql.Date(WMSDateUtil.startOfToday().getTime()))+"'";
        LOGGER.info("Retrieving from DCRIPACMDSTransaction");
        String sql = "SELECT * FROM DCRIPACMDSTransaction where site_code='" + center_code + "' and transaction_date='" + (new java.sql.Date(process_date.getTime())) + "' ";
        LOGGER.info("SQL MDS Transaction: " + sql);
        return getJdbcTemplate().query(sql, new PPAMDSRowMapper());
    }

    private static final class DCRIpacValueRowMapper implements RowMapper {

        @Override
        public DCRIpacValue mapRow(ResultSet resultSet, int rowNumber)
                throws SQLException {
            DCRIpacValue value = new DCRIpacValue();
            int idx = 1;
            value.setProdCode(resultSet.getString(++idx));
            value.setPayCode(resultSet.getString(++idx));
            value.setPaySubCode(resultSet.getString(++idx));
            value.setPaySubCurr(resultSet.getString(++idx));
            value.setAmount(resultSet.getDouble(++idx));
            value.setAcf2id(resultSet.getString(++idx));

            return value;
        }
    }

    private static final class NonPostedTransactionRowMapper implements RowMapper {

        @Override
        public NonPostedTransaction mapRow(ResultSet resultSet, int rowNumber)
                throws SQLException {
            NonPostedTransaction value = new NonPostedTransaction();
            int idx = 1;
            value.setCompanyCode(resultSet.getString(++idx));
            value.setProductCode(resultSet.getString(++idx));
            value.setTransactionType(resultSet.getString(++idx));
            value.setPaymentCurrencyStr(resultSet.getString(++idx));
            value.setPaymentPostStatus(resultSet.getInt(++idx));
            value.setAmount(resultSet.getDouble(++idx));
            value.setUserId(resultSet.getString(++idx));

            return value;
        }
    }

    private static final class GAFValueRowMapper implements RowMapper {

        @Override
        public GAFValue mapRow(ResultSet resultSet, int rowNumber)
                throws SQLException {
            GAFValue value = new GAFValue();
            int idx = 1;
            value.setCompanyName(resultSet.getString(++idx));
            value.setProdCode(resultSet.getString(++idx));
            value.setPaymentCurrency(resultSet.getString(++idx));
            value.setPaymentPostStatus(resultSet.getString(++idx));
            value.setPayType(resultSet.getString(++idx));
            value.setTotalAmount(resultSet.getDouble(++idx));
            value.setUserId(resultSet.getString(++idx));
            value.setCashAmount(resultSet.getDouble(++idx));
            value.setCardAmount(resultSet.getDouble(++idx));
            value.setCheckAmount(resultSet.getDouble(++idx));
            value.setNcashAmount(resultSet.getDouble(++idx));

            return value;
        }
    }

    private static final class PPAMDSRowMapper implements RowMapper {

        @Override
        public PPAMDS mapRow(ResultSet resultSet, int rowNumber)
                throws SQLException {
            PPAMDS value = new PPAMDS();
            int idx = 1;
            value.setPaySubDesc(resultSet.getString(++idx));
            value.setSalesSlipNumber(resultSet.getString(++idx));
            value.setDocumentId(resultSet.getString(++idx));
            value.setComCode(resultSet.getString(++idx));
            value.setCompany(resultSet.getString(++idx));
            value.setApplicationSerialNumber(resultSet.getString(++idx));
            value.setCardType(resultSet.getString(++idx));
            value.setCurrency(resultSet.getString(++idx));
            value.setCustomerCenter(resultSet.getString(++idx));
            value.setDateRangeFrom(resultSet.getDate(++idx));
            value.setDateRangeTo(resultSet.getDate(++idx));
            value.setPolicyPlanClientNumber(resultSet.getString(++idx));
            value.setProdCode(resultSet.getString(++idx));
            value.setPymtCurrency(resultSet.getString(++idx));
            value.setSiteCode(resultSet.getString(++idx));
            value.setTrxnDateProduct(resultSet.getDate(++idx));
            value.setUserId(resultSet.getString(++idx));
            value.setProduct(resultSet.getString(++idx));
            value.setAccountNumber(resultSet.getString(++idx));
            value.setApprovalDate(resultSet.getDate(++idx));
            value.setApprovalNumber(resultSet.getString(++idx));
            value.setCardAmount(resultSet.getString(++idx));

            return value;
        }
    }

    private static final class DCRDownloadDataRowMapper implements RowMapper {

        @Override
        public DCR mapRow(ResultSet resultSet, int rowNumber)
                throws SQLException {
            DCR value = new DCR();
            int idx = 0;
            value.setHasDownloaded(resultSet.getString(++idx));
            value.setDownloadedDate(resultSet.getDate(++idx));
            return value;
        }
    }

    @Override
    @Transactional
    public int updateDCRIPACDownloadFlag(String flag, Date date, Long dcrId) throws WMSDaoException {
        int rec = 0;
        String sql = "UPDATE [DCR] SET hasDownloaded = ?, downloaded_date = ? where id = ?";

        rec = getJdbcTemplate().update(
                sql,
                new Object[]{flag, date, dcrId});

        try {
            if (rec > 0) {
                getJdbcTemplate().getDataSource().getConnection().commit();
            }
        } catch (SQLException e) {
            LOGGER.error("Error in DCRIpacDaoImpl: ", e);
        }
        return rec;
    }

    @SuppressWarnings("unchecked")
    @Override
    public DCR getDCRDownloadData(Long dcrId) throws WMSDaoException {
        String sql = "SELECT hasDownloaded, downloaded_date FROM DCR WHERE ID = " + dcrId;

        List<DCR> dcr = getJdbcTemplate().query(sql, new DCRDownloadDataRowMapper());
        if (dcr.size() > 0) {
            return dcr.get(0);
        }
        return null;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<DCRIpacValue> getDCRIPACDailyCollectionSummary(
            String center_code,
            Date process_date,
            DCRIpacValue value) throws WMSDaoException {
        // Modified for DCR - IPAC redesign with cancellation
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT * FROM DCRIPACDailyCollectionSummary ")
                .append(" WHERE center_code='").append(center_code).append("'")
                .append("   AND process_date='").append((new java.sql.Date(process_date.getTime()))).append("'")
                .append("   AND product_code='").append(value.getProdCode()).append("'")
                .append("   AND pay_code='").append(value.getPayCode()).append("'")
                .append("   AND pay_sub_code='").append(value.getPaySubCode()).append("'")
                .append("   AND pay_sub_curr='").append(value.getPaySubCurr()).append("'")
                .append("   AND amount=").append(value.getAmount()).append("")
                .append("   AND acf2id='").append(value.getAcf2id()).append("'")
                .append("   AND SUBSTRING(transaction_key, 0, CHARINDEX('*', transaction_key)) NOT IN (SELECT document_id FROM DCRIPACCancelTransaction WHERE cancel_date = '").append((new java.sql.Date(process_date.getTime()))).append("')");
        LOGGER.info("SQL DAILY Transaction: " + sql);
        return getJdbcTemplate().query(sql.toString(), new DCRIpacValueRowMapper());
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<NonPostedTransaction> getDCRIPACDTR(
            String center_code,
            Date process_date,
            NonPostedTransaction value) throws WMSDaoException {
        // Modified for DCR - IPAC redesign with cancellation
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT * FROM DCRIPACDTR")
                .append(" WHERE center_code='").append(center_code).append("'")
                .append("   AND process_date='").append((new java.sql.Date(process_date.getTime()))).append("'")
                .append("   AND company_code='").append(value.getCompanyCode()).append("'")
                .append("   AND  product_code='").append(value.getProductCode()).append("'")
                .append("   AND  transaction_type='").append(value.getTransactionType()).append("'")
                .append("   AND  payment_currency='").append(value.getPaymentCurrencyStr()).append("'")
                .append("   AND  amount=").append(value.getAmount()).append("")
                .append("   AND  userid='").append(value.getUserId()).append("'")
                .append("   AND  payment_post_status='").append(value.getPaymentPostStatus()).append("'")
                .append("   AND SUBSTRING(transaction_key, 0, CHARINDEX('*', transaction_key)) NOT IN (SELECT document_id FROM DCRIPACCancelTransaction WHERE cancel_date = '").append((new java.sql.Date(process_date.getTime()))).append("')");
        LOGGER.info("SQL DTR Transaction: " + sql);
        return getJdbcTemplate().query(sql.toString(), new NonPostedTransactionRowMapper());
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<GAFValue> getDCRIPACGAF(
            String center_code,
            Date process_date,
            GAFValue value) throws WMSDaoException {
        // Modified for DCR - IPAC redesign with cancellation
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT * FROM DCRIPACGAF")
                .append(" WHERE center_code='").append(center_code).append("'")
                .append("   AND process_date='").append((new java.sql.Date(process_date.getTime()))).append("'")
                .append("   AND company_name='").append(value.getCompanyName()).append("'")
                .append("   AND product_code='").append(value.getProdCode()).append("'")
                .append("   AND payment_currency='").append(value.getPaymentCurrency()).append("'")
                .append("   AND payment_post_status='").append(value.getPaymentPostStatus()).append("'")
                .append("   AND pay_type='").append(value.getPayType()).append("'")
                .append("   AND total_amount=").append(value.getTotalAmount().doubleValue()).append("")
                .append("   AND userid='").append(value.getUserId()).append("'")
                .append("   AND cash_amount=").append(value.getCashAmount()).append("")
                .append("   AND card_amount=").append(value.getCardAmount()).append("")
                .append("   AND check_amount=").append(value.getCheckAmount()).append("")
                .append("   AND ncash_amount=").append(value.getNcashAmount()).append(" ")
                .append("   AND SUBSTRING(transaction_key, 0, CHARINDEX('*', transaction_key)) NOT IN (SELECT document_id FROM DCRIPACCancelTransaction WHERE cancel_date = '").append((new java.sql.Date(process_date.getTime()))).append("')");
        LOGGER.info("SQL GAF Transaction: " + sql);
        return getJdbcTemplate().query(sql.toString(), new GAFValueRowMapper());
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<PPAMDS> getDCRIPACMDSTransaction(String company,
            String center_code, Date from_date, Date to_date, PPAMDS value)
            throws WMSDaoException {
        //(pay_sub_desc,sales_slip_number,document_id,com_code,company,application_serial_number,card_type,currency,customer_center,policy_plan_client_number,prod_code,payment_currency,site_code,transaction_date,userid,product,account_number,approval_date,approval_number,card_amount)
        String sql = "SELECT * FROM DCRIPACMDSTransaction where site_code='" + center_code + "' and com_code='" + company + "' and (date_range_from='" + (new java.sql.Date(from_date.getTime())) + "' and date_range_to='" + (new java.sql.Date(to_date.getTime())) + "') "
                + " and pay_sub_desc='" + value.getPaySubDesc() + "' and sales_slip_number='" + value.getSalesSlipNumber() + "' and document_id='" + value.getDocumentId() + "' and com_code='" + company + "' and card_type='" + value.getCardType() + "' and currency='" + value.getCurrency() + "' and customer_center='" + value.getCustomerCenter() + "' and prod_code='" + value.getProdCode() + "' and payment_currency='" + value.getPymtCurrency() + "' and userid='" + value.getUserId() + "' and product='" + value.getProduct() + "' and account_number='" + value.getAccountNumber() + "' and approval_date='" + (new java.sql.Date(value.getApprovalDate().getTime())) + "' and approval_number='" + value.getApprovalNumber() + "' and card_amount=" + value.getCardAmount() + " ";
        LOGGER.info("SQL MDS Transaction: " + sql);
        return getJdbcTemplate().query(sql, new PPAMDSRowMapper());
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public List<DCRIpacValue> getDCRIPACDailyCollectionSummary(
            String centerCode,
            Date processDate,
            String acf2id) throws WMSDaoException {
        
        LOGGER.info("Retrieving from DCRIPACDailyCollectionSummary");
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT * FROM DCRIPACDailyCollectionSummary")
                .append(" WHERE center_code='").append(centerCode).append("'")
                .append("   AND process_date='").append((new java.sql.Date(processDate.getTime()))).append("'")
                .append("   AND acf2id='").append(acf2id).append("'")
                .append("   AND SUBSTRING(transaction_key, 0, CHARINDEX('*', transaction_key)) NOT IN (SELECT document_id FROM DCRIPACCancelTransaction WHERE cancel_date = '").append((new java.sql.Date(processDate.getTime()))).append("')");
        LOGGER.info("SQL Daily Collection Transaction: " + sql);
        
        return getJdbcTemplate().query(sql.toString(), new DCRIpacValueRowMapper());
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public List<NonPostedTransaction> getDCRIPACDTR(
            String centerCode,
            Date processDate,
            String acf2id) throws WMSDaoException {
        
        LOGGER.info("Retrieving from DCRIPACDTR");
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT * FROM DCRIPACDTR")
                .append(" WHERE center_code='").append(centerCode).append("'")
                .append("   AND process_date='").append((new java.sql.Date(processDate.getTime()))).append("'")
                .append("   AND userid = '").append(acf2id).append("'")
                .append("   AND SUBSTRING(transaction_key, 0, CHARINDEX('*', transaction_key)) NOT IN (SELECT document_id FROM DCRIPACCancelTransaction WHERE cancel_date = '").append((new java.sql.Date(processDate.getTime()))).append("')");
        LOGGER.info("SQL DTR Transaction: " + sql);
        
        return getJdbcTemplate().query(sql.toString(), new NonPostedTransactionRowMapper());
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public List<GAFValue> getDCRIPACGAF(
            String centerCode,
            Date processDate,
            String acf2id) throws WMSDaoException {
        
        LOGGER.info("Retrieving from DCRIPACGAF");
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT * FROM DCRIPACGAF")
                .append(" WHERE center_code='").append(centerCode).append("'")
                .append("   AND process_date='").append((new java.sql.Date(processDate.getTime()))).append("'")
                .append("   AND userid='").append(acf2id).append("'")
                .append("   AND SUBSTRING(transaction_key, 0, CHARINDEX('*', transaction_key)) NOT IN (SELECT document_id FROM DCRIPACCancelTransaction WHERE cancel_date = '").append((new java.sql.Date(processDate.getTime()))).append("')");
        LOGGER.info("SQL GAF Transaction: " + sql);
        
        return getJdbcTemplate().query(sql.toString(), new GAFValueRowMapper());
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<PPAMDS> getDCRIPACMDSTransaction(
            String company,
            String centerCode,
            Date fromDate,
            Date toDate,
            String currency) throws WMSDaoException {
        
        LOGGER.info("Retrieving from DCRIPACMDSTransaction");
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT * FROM DCRIPACMDSTransaction")
                .append(" WHERE site_code='").append(centerCode).append("'")
                .append("   AND com_code='").append(company).append("'")
                .append("   AND (date_range_from='").append((new java.sql.Date(fromDate.getTime()))).append("' AND date_range_to='").append((new java.sql.Date(toDate.getTime()))).append("') ")
                .append("   AND currency = '").append(currency).append("'");
        LOGGER.info("SQL MDS Transaction: " + sql);
        
        return getJdbcTemplate().query(sql.toString(), new PPAMDSRowMapper());
    }
}
