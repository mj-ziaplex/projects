/**
 * 
 */
package ph.com.sunlife.wms.services;

import ph.com.sunlife.wms.dao.domain.DCR;
import ph.com.sunlife.wms.services.exception.ServiceException;

/**
 * @author i176
 *
 */
public interface DCRIPACService {

	public boolean runIPACToDCRJob()  throws ServiceException;

	public boolean processIPACAndIngeniumData(DCR dcr)  throws ServiceException;

	public void removeIPACAndIngeniumData(DCR dcr)  throws ServiceException;

}
