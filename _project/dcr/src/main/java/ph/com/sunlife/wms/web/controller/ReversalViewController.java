package ph.com.sunlife.wms.web.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.web.servlet.ModelAndView;

import ph.com.sunlife.wms.security.Role;
import ph.com.sunlife.wms.services.DCRReversalService;
import ph.com.sunlife.wms.services.bo.DCRReversalBO;
import ph.com.sunlife.wms.services.exception.ServiceException;
import ph.com.sunlife.wms.web.controller.form.ReversalForm;
import ph.com.sunlife.wms.web.exception.ApplicationException;

public class ReversalViewController extends
		AbstractSecuredMultiActionController {

	private static final String REVERSAL_VIEW = "reversalView";

	private static final String REVERSALS_KEY = "reversals";

	private static final Logger LOGGER = Logger.getLogger(ReversalViewController.class);
	
	private static final String TITLE_KEY = "title";

	@Override
	protected Role[] allowedRoles() {
		return Role.values();
	}

	private DCRReversalService dcrReversalService;

	public void setDcrReversalService(DCRReversalService dcrReversalService) {
		this.dcrReversalService = dcrReversalService;
	}

	public ModelAndView showReversalDetails(HttpServletRequest request,
			HttpServletResponse response, ReversalForm form)
			throws ApplicationException {

		Long dcrBalancingToolProductId = form.getDcrBalancingToolProductId();

		ModelAndView modelAndView = new ModelAndView();
		modelAndView.addObject(TITLE_KEY, "Reversal/Unposted");

		try {
			List<DCRReversalBO> reversals = dcrReversalService
					.getAllByBalancingToolProduct(dcrBalancingToolProductId);
			modelAndView.addObject(REVERSALS_KEY, reversals);
		} catch (ServiceException ex) {
			LOGGER.error(ex);
			throw new ApplicationException(ex);
		}
		
		modelAndView.setViewName(REVERSAL_VIEW);
		return modelAndView;
	}

}
