package ph.com.sunlife.wms.services;

import java.util.List;

import ph.com.sunlife.wms.services.bo.UserSession;
import ph.com.sunlife.wms.services.exception.ServiceException;

/**
 * The Service Interface responsible for creating {@link UserSession} given a
 * set of user credentials. This is basically the Core Security Service for WMS
 * DCR OE Upload.
 * 
 * @author Zainal Limpao
 * 
 */
// TODO: change this to 'SecurityService'
public interface DCRLogInService {

	public static final String ROLE_PPA = "ppa";

	public static final String ROLE_MANAGER = "manager";

	public static final String ROLE_CASHIER = "cashier";

	public static final String ROLE_GUEST = "guest";

	/**
	 * Attempts to log in to the system using userId, password and site code.
	 * 
	 * @param userId
	 * @param password
	 * @param siteCode
	 * @return {@link UserSession} object. This is null if credentials are
	 *         invalid.
	 * @throws ServiceException
	 *             - if something is wrong such as LDAP fail.
	 */
	UserSession logInUser(String userId, String password, String siteCode,
			String selectedRole) throws ServiceException;

	/**
	 * Get all display Customer Center Names.
	 * 
	 * @return
	 * @throws ServiceException
	 */
	List<String> displayCCNames() throws ServiceException;

	UserSession logInUser(String adminUserId, String userId, String password, String siteCode,
			String selectedRole) throws ServiceException;

}
