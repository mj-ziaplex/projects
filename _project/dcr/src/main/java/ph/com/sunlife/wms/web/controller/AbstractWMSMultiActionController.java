package ph.com.sunlife.wms.web.controller;

import java.beans.PropertyEditor;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.ArrayUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.propertyeditors.CustomCollectionEditor;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.context.MessageSource;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.validation.FieldError;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.multipart.support.ByteArrayMultipartFileEditor;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;

import ph.com.sunlife.wms.web.controller.binder.BinderAware;
import ph.com.sunlife.wms.web.controller.binder.SessionAware;
import ph.com.sunlife.wms.web.exception.ApplicationException;

/**
 * Abstracted MultiActionController that handles {@link BinderAware} command
 * objects and performs certain life-cycle methods.
 * 
 * @author Zainal Limpao
 */
public abstract class AbstractWMSMultiActionController extends MultiActionController {

	/** The Log4J Logger. */
	private static final Logger LOGGER = Logger.getLogger(AbstractWMSMultiActionController.class);

	private static final String CURRENT_TIME = "tm";
	
	protected static final String EXCEPTION_KEY = "exception";

	/** The constant for the Error Message. */
	public static final String ERROR_MESSAGE = "errorMessage";

	/** Injectible dateFormat String with a default value. */
	private String dateFormat = "ddMMMyyyy";

	/** allowValidateAfterBindErrors boolean variable. */
	private boolean allowValidateAfterBindErrors = true;

	/** The default viewname on validation failures. */
	private String failureViewName;

	/** The MessageSource for this controller. */
	private MessageSource messageSource;

	/**
	 * Initialize the given binder instance, adding custom editors and
	 * propagating {@link BinderAware#getAllowedFields()} and
	 * {@link BinderAware#getRequiredFields()}. Called by
	 * <code>createBinder</code>.
	 * <p>
	 * Currently the following editors are defined:
	 * <ol>
	 * <li><code>CustomNumberEditor</code> for <code>Double</code> which allows
	 * null values.</li>
	 * <li><code>CustomNumberEditor</code> for <code>Long</code> which allows
	 * null values.</li>
	 * <li><code>BigDecimalEditor</code> for <code>BigDecimal</code> for better
	 * processing of fixed-point floats.</li>
	 * <li><code>CustomDateEditor</code> for <code>Date</code> which allows null
	 * values.</li>
	 * </ol>
	 * If you need additional binders, you could subclass this
	 * {@link org.springframework.web.servlet.mvc.Controller} or inject a
	 * {@link List} of {@link BinderConfig} via
	 * {@link #setAdditionalBinders(java.util.List)}.
	 * 
	 * @param request
	 *            current HTTP request
	 * @param binder
	 *            new binder instance
	 * @throws Exception
	 *             in case of invalid state or arguments
	 * @see #createBinder
	 * @see org.springframework.validation.DataBinder#registerCustomEditor
	 * @see org.springframework.beans.propertyeditors.CustomDateEditor
	 * @see org.springframework.validation.DataBinder#setAllowedFields(String[])
	 * @see org.springframework.validation.DataBinder#setRequiredFields(String[])
	 */
	@Override
	protected void initBinder(
			final HttpServletRequest request,
			final ServletRequestDataBinder binder) throws Exception {
		
		super.initBinder(request, binder);

		AbstractWMSMultiActionController.LOGGER.debug("Initializing Binders...");

		Object target = binder.getTarget();
		if (!(target instanceof BinderAware)) {
			throw new IllegalStateException("Command must implement the BinderAware interface");
		}

		BinderAware command = (BinderAware) target;
		binder.setAllowedFields(command.getAllowedFields());
		binder.setRequiredFields(command.getRequiredFields());
		binder.registerCustomEditor(Double.class, new CustomNumberEditor(Double.class, true));
		binder.registerCustomEditor(Long.class, new CustomNumberEditor(Long.class, true));
		binder.registerCustomEditor(byte[].class, new ByteArrayMultipartFileEditor());
		DateFormat format = new SimpleDateFormat(this.dateFormat);
		binder.registerCustomEditor(Date.class, new CustomDateEditor(format, true));
		binder.registerCustomEditor(Set.class, new CustomCollectionEditor(Set.class) {
			@Override
			protected Object convertElement(Object element) {
				String id = (String) element;
				return id;
			}
		});
		
		registerAdditionalPropertyEditors(binder);
	}
	
	/**
	 * Optional method when implementing class wants to 
	 * register more property editors.
	 * 
	 * @param binder
	 * @throws Exception
	 * @see PropertyEditor
	 * @see ServletRequestDataBinder
	 */
	protected void registerAdditionalPropertyEditors(ServletRequestDataBinder binder) throws Exception {
		// Optional method that can be overriden.
	}
	
	/**
	 * Creates a property editor for money-related fields.
	 * Note that you have to create a local variable for 
	 * this so you can reuse them in all your money-related 
	 * fields.
	 * 
	 * @return {@link PropertyEditor}
	 */
	protected PropertyEditor createMoneyPropertyEditor() {
		NumberFormat currencyFormat = NumberFormat.getCurrencyInstance();
		DecimalFormatSymbols decimalFormatSymbols = 
			((DecimalFormat) currencyFormat).getDecimalFormatSymbols();
		
		decimalFormatSymbols.setCurrencySymbol("");
		((DecimalFormat) currencyFormat).setDecimalFormatSymbols(decimalFormatSymbols);
		
		PropertyEditor propertyEditor = 
		    new CustomNumberEditor(Double.class, currencyFormat, true);
		
		return propertyEditor;
	}

	/**
	 * Bind request parameters onto the given form bean. This has been
	 * overridden to call
	 * {@link #onBindAndValidate(HttpServletRequest, Object, BindException)}
	 * 
	 * @param request
	 *            request from which parameters will be bound
	 * @param command
	 *            command object, that must be a JavaBean
	 * @throws Exception
	 *             in case of invalid state or arguments
	 * @see MultiActionController#bind(HttpServletRequest, Object)
	 */
	@Override
	protected void bind(
			final HttpServletRequest request,
			final Object command) throws Exception {
		
		ServletRequestDataBinder binder = this.createBinder(request, command);
		binder.bind(request);
		
		BindException errors = new BindException(binder.getBindingResult());
		this.onBindAndValidate(request, command, errors);
		
		binder.closeNoCatch();
	}

	/**
	 * Implements parts of lifecycle of {@link BinderAware} command. Processing
	 * events here:
	 * <ol>
	 * <li>Binding is performed <b>before</b> this method is invoked.</li>
	 * <li>Controller invokes {@link BinderAware#afterBind()} callback so that
	 * command can validate itself.</li>
	 * <li>Calls {@link #preValidate(Object)} callback method of this
	 * controller.</li>
	 * <li>Controller calls
	 * {@link BinderAware#validate(org.springframework.validation.Errors)}</li>
	 * <li>Invokes all {@link Validator} injected in this controller</li>
	 * <li>Calls {@link #postValidate(Object)} callback method of this
	 * controller</li>
	 * 
	 * @param request
	 *            current HTTP request
	 * @param command
	 *            the command object, still allowing for further binding
	 * @param errors
	 *            validation errors holder, allowing for additional custom
	 *            validation
	 * @throws Exception
	 *             in case of invalid state or arguments
	 *             {@link org.springframework.validation.Errors} using
	 *             {@link BinderAware#validate(org.springframework.validation.Errors)
	 *             . <li> If command is valid OR
	 *             {@link #allowValidateAfterBindErrors} allows to continue,
	 *             Controller performs validations for all validators that
	 *             support specific command class.</li> <li>If command is valid
	 *             at this moment, {@link #postValidate(BinderAware)} is called.
	 *             </li>
	 *             </ol>
	 * @see org.springframework.validation.Errors
	 */
	protected void onBindAndValidate(
			final HttpServletRequest request,
			final Object command,
			final BindException errors) throws Exception {
		
		if (AbstractWMSMultiActionController.LOGGER.isDebugEnabled()) {
			AbstractWMSMultiActionController.LOGGER.debug("Initializing validators after successful bind");
		}

		BinderAware bindedCommand = (BinderAware) command;
		
		this.bindSession(request, bindedCommand);
		
		if (this.allowValidateAfterBindErrors) {
			// Generally, command may perform additional self-validations here.
			bindedCommand.afterBind();

			// Security checks should be fired BEFORE validation, so we don't
			// expose and knowledge about validation errors, etc.
			boolean canContinue = this.preValidate(bindedCommand);

			// invoke validators if allowed by pre-processor.
			this.invokeValidators(bindedCommand, errors, canContinue);

			// form object does self validation, if necessary.
			bindedCommand.validate(errors);
		}

		if (!errors.hasErrors()) {
			this.postValidate(bindedCommand);
		}
	}

	private void bindSession(
			final HttpServletRequest request,
			final BinderAware bindedCommand) {
		
		if (bindedCommand instanceof SessionAware) {
			HttpSession session = request.getSession(false);
			if (session != null) {
				((SessionAware) bindedCommand).setSession(session);
			}
		}
	}

	/**
	 * Invoke validators if allowed by pre-processor.
	 * 
	 * @param command
	 *            the command
	 * @param errors
	 *            the errors
	 * @param canContinue
	 *            the can continue
	 */
	private void invokeValidators(
			final Object command,
			final BindException errors,
			final boolean canContinue) {
		
		if (canContinue) {
			if (this.getValidators() != null) {
				for (int i = 0; i < this.getValidators().length; i++) {
					Validator validator = this.getValidators()[i];
					if (validator.supports(command.getClass())) {
						ValidationUtils.invokeValidator(this.getValidators()[i], command, errors);
					}
				}
			}
		}
	}

	/**
	 * Create a reference data map for the given request and command,
	 * consisting of bean name/bean instance pairs as expected by ModelAndView.
	 * <p>The default implementation delegates to {@link #referenceData(HttpServletRequest)}.
	 * Subclasses can override this to set reference data used in the view.
	 * @param request current HTTP request
	 * @param command form object with request parameters bound onto it
	 * @param errors validation errors holder
	 * @return a Map with reference data entries, or <code>null</code> if none
	 * @throws Exception in case of invalid state or arguments
	 * @see ModelAndView
	 */
	@SuppressWarnings("rawtypes")
	protected Map referenceData(
			final HttpServletRequest request,
			final Object command,
			final Errors errors) throws Exception {
		return referenceData(request);
	}

	/**
	 * Create a reference data map for the given request.
	 * Called by the {@link #referenceData(HttpServletRequest, Object, Errors)}
	 * variant with all parameters.
	 * <p>The default implementation returns <code>null</code>.
	 * Subclasses can override this to set reference data used in the view.
	 * @param request current HTTP request
	 * @return a Map with reference data entries, or <code>null</code> if none
	 * @throws Exception in case of invalid state or arguments
	 * @see #referenceData(HttpServletRequest, Object, Errors)
	 * @see ModelAndView
	 */
	@SuppressWarnings("rawtypes")
	protected Map referenceData(final HttpServletRequest request) throws Exception {
		return null;
	}
	
	/**
	 * A custom exception handler that will manage BindingException and
	 * validation errors.
	 * 
	 * @param request
	 *            current HTTP request
	 * @param response
	 *            current HTTP response
	 * @param cause
	 *            which is a {@link ServletRequestBindingException}
	 * @return {@link ModelAndView} that will redirect you to the
	 *         {@link #failureViewName} view
	 * @see Errors
	 * @see BindException
	 */
	public ModelAndView handleBindingError(
			final HttpServletRequest request,
			final HttpServletResponse response,
			final ServletRequestBindingException cause) throws Exception {
		
		AbstractWMSMultiActionController.LOGGER.error("Problem on binding the input fields is encountered", cause);

		BindException errors = (BindException) cause.getCause();
		ModelAndView modelAndView = new ModelAndView(this.getFailureViewName());
		modelAndView.addAllObjects(errors.getModel());
		
		modelAndView.addObject("errorMsg", "There are problems on the input fields");
		pushExceptionToModelAndView(modelAndView, cause);

		return modelAndView;
	}
	
	/**
	 * Handler method for any other {@link ApplicationException}.
	 * @param request
	 * @param response
	 * @param cause
	 * @return
	 */
	public ModelAndView handleFatalError(
			final HttpServletRequest request, 
			final HttpServletResponse response,
			final ApplicationException cause) {
		
		AbstractWMSMultiActionController.LOGGER.error("An unexpected exception has been encountered.", cause);

		ModelAndView modelAndView = new ModelAndView("fatalError");
		// TODO: use message properties
		modelAndView.addObject("errorMsg", "An unexpected error has been encountered. Please contact helpdesk for assistance.");
		pushExceptionToModelAndView(modelAndView, cause);

		return modelAndView;
	}
	
	/**
	 * Handler for generic {@link Exception}.
	 * @param request
	 * @param response
	 * @param cause
	 * @return
	 */
	public ModelAndView handleFatalError(
			final HttpServletRequest request, 
			final HttpServletResponse response,
			final Exception cause) {
		
		AbstractWMSMultiActionController.LOGGER.error("An unexpected exception has been encountered.", cause);

		ModelAndView modelAndView = new ModelAndView("fatalError");
		// TODO: use message properties
		modelAndView.addObject("errorMsg", "An unexpected error has been encountered. Please contact helpdesk for assistance.");
		pushExceptionToModelAndView(modelAndView, cause);

		return modelAndView;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.springframework.web.servlet.mvc.multiaction.MultiActionController
	 * #handleRequestInternal(javax.servlet.http.HttpServletRequest,
	 * javax.servlet.http.HttpServletResponse)
	 */
	@Override
	protected final ModelAndView handleRequestInternal(
			final HttpServletRequest request,
			final HttpServletResponse response) throws Exception {
		this.beforeHandleRequest(request, response);
		
		ModelAndView modelAndView = super.handleRequestInternal(request, response);
		
		if (modelAndView != null) {
			modelAndView.addAllObjects(this.referenceData(request));
		}
		
		this.afterHandleRequest(modelAndView, request, response);
		return modelAndView;
	}
	
	/**
	 * Capture names of fields with errors
	 * @param errors
	 * @return
	 */
	protected Set<String> getFieldsWithErrors(final BindException errors) {
		
		Set<String> fieldsWithErrors = new HashSet<String>();
		for (Object error : errors.getFieldErrors()) {
			FieldError fieldError = (FieldError) error;
			fieldsWithErrors.add(fieldError.getField());
		}
		return fieldsWithErrors;
	}
	
	/**
	 * A callback method to be executed before invoking the 
	 * {@link MultiActionController#invokeNamedMethod}.
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	protected void beforeHandleRequest(
			final HttpServletRequest request, 
			final HttpServletResponse response)  throws Exception {
	}
	
	/**
	 * A callback method to be executed after invoking the 
	 * {@link MultiActionController#invokeNamedMethod}.
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	protected void afterHandleRequest(
			final ModelAndView modelAndView, 
			final HttpServletRequest request,
			final HttpServletResponse response) throws Exception {
		// this.addCurrentTime(modelAndView);
	}
	
	/**
	 * Adds current time into the {@link ModelAndView} object.
	 * 
	 * @param modelAndView
	 */
	@SuppressWarnings("unused")
	private void addCurrentTime(final ModelAndView modelAndView) {
		modelAndView.addObject(CURRENT_TIME, System.currentTimeMillis());
	}
	
	/**
	 * Pushes the {@link Exception} being caught into the {@link ModelAndView}.
	 * 
	 * @param modelAndView
	 * @param cause
	 */
	protected void pushExceptionToModelAndView(
			final ModelAndView modelAndView, 
			final Exception cause) {
		modelAndView.addObject(EXCEPTION_KEY, cause);
	}
	
	/**
	 * Pulls the {@link Exception} contained in the {@link ModelAndView}. 
	 * This will return <code>null</code> if there is no Exception.
	 * 
	 * @param modelAndView
	 * @return
	 */
	protected Exception pullExceptionFromModelAndView(final ModelAndView modelAndView) {
		return (Exception) modelAndView.getModel().get(EXCEPTION_KEY);
	}
	
	/**
	 * Callback invoked AFTER successful validation of command. This could run
	 * additional checks, e.g. security.
	 * 
	 * @param command
	 *            the command
	 */
	protected void postValidate(final Object command) {
	}

	/**
	 * Callback invoked BEFORE successful validation of command. It is only
	 * fired when command is valid after binding.
	 * <p>
	 * This could run additional checks, e.g. security.
	 * 
	 * @param command
	 *            <b>valid</b> binder-aware command
	 * @return true to continue with validation, false to stop.
	 */
	protected boolean preValidate(final Object command) {
		return Boolean.TRUE;
	}

	/**
	 * Override validators check and do nothing (because we allow validators
	 * which do not support our command class).
	 */
	@Override
	protected void initApplicationContext() {
		// suppress superclass checks for supports().
	}
	
	/**
	 * Wrapper method to get messages from {@link MessageSource}
	 * @param code
	 * @return
	 */
	protected String getMessage(final String code) {
		return this.getMessage(code, new Object[] {}, null);
	}
	
	/**
	 * Wrapper method to get messages from {@link MessageSource}
	 * @param code
	 * @param parameters
	 * @return
	 */
	protected String getMessage(final String code, final Object[] parameters) {
		return this.getMessage(code, parameters, null);
	}
	
	/**
	 * Wrapper method to get messages from {@link MessageSource}
	 * @param code
	 * @param parameters
	 * @param locale
	 * @return
	 */
	protected String getMessage(final String code, final Object[] parameters,  final Locale locale) {
		String message = null;
		if (this.messageSource != null) {
			message = messageSource.getMessage(code, parameters, locale);
		}
		return message;
	}

	/**
	 * Sets a {@link List} of {@link BinderConfig} for.
	 * 
	 * @param additionalValidators
	 *            the new additional validators {@link #additionalBinders}
	 */
	public void setAdditionalValidators(final Validator[] additionalValidators) {
		super.setValidators((Validator[]) ArrayUtils.addAll(this.getValidators(), additionalValidators));
	}

	/**
	 * Adds additional {@link Validator}s on top of the existing.
	 * 
	 * @param allowValidateAfterBindErrors
	 *            the new allow validate after bind errors {@link #validators}
	 */
	public void setAllowValidateAfterBindErrors(final boolean allowValidateAfterBindErrors) {
		this.allowValidateAfterBindErrors = allowValidateAfterBindErrors;
	}

	/**
	 * Getter for the {@link MessageSource}.
	 * 
	 * @return {@link MessageSource}
	 */
	public MessageSource getMessageSource() {
		return this.messageSource;
	}

	/**
	 * Setter for the {@link MessageSource}.
	 * 
	 * @param messageSource
	 *            the new message source
	 */
	public void setMessageSource(final MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	/**
	 * Getter for the failureViewName.
	 * 
	 * @return name of the failure view
	 */
	public String getFailureViewName() {
		return this.failureViewName;
	}

	/**
	 * Setter for the failureViewName.
	 * 
	 * @param failureViewName
	 *            the new failure view name
	 */
	public void setFailureViewName(final String failureViewName) {
		this.failureViewName = failureViewName;
	}

	/**
	 * Setter for the date String format.
	 * 
	 * @param dateFormat
	 *            the new date format
	 */
	public void setDateFormat(final String dateFormat) {
		this.dateFormat = dateFormat;
	}

}
