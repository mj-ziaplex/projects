package ph.com.sunlife.wms.dao.domain;

import static ph.com.sunlife.wms.util.ParameterMaskUtil.toMaskedString;

public class DCRPPAMDSNote extends Entity {
	
	private Long dcrId;

	private String salesSlipNumber;
	
	private String accountNumber;
	
	private String approvalNumber;
	
	private String ppaNotes;
	
	private String ppaFindings;

	public String getSalesSlipNumber() {
		return salesSlipNumber;
	}

	public void setSalesSlipNumber(String salesSlipNumber) {
		this.salesSlipNumber = salesSlipNumber;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = toMaskedString(accountNumber);
	}

	public String getApprovalNumber() {
		return approvalNumber;
	}

	public void setApprovalNumber(String approvalNumber) {
		this.approvalNumber = approvalNumber;
	}

	public String getPpaNotes() {
		return ppaNotes;
	}

	public void setPpaNotes(String ppaNotes) {
		this.ppaNotes = ppaNotes;
	}

	public String getPpaFindings() {
		return ppaFindings;
	}

	public void setPpaFindings(String ppaFindings) {
		this.ppaFindings = ppaFindings;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((accountNumber == null) ? 0 : accountNumber.hashCode());
		result = prime * result
				+ ((approvalNumber == null) ? 0 : approvalNumber.hashCode());
		result = prime * result
				+ ((salesSlipNumber == null) ? 0 : salesSlipNumber.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		DCRPPAMDSNote other = (DCRPPAMDSNote) obj;
		if (accountNumber == null) {
			if (other.accountNumber != null)
				return false;
		} else if (!accountNumber.equals(other.accountNumber))
			return false;
		if (approvalNumber == null) {
			if (other.approvalNumber != null)
				return false;
		} else if (!approvalNumber.equals(other.approvalNumber))
			return false;
		if (salesSlipNumber == null) {
			if (other.salesSlipNumber != null)
				return false;
		} else if (!salesSlipNumber.equals(other.salesSlipNumber))
			return false;
		return true;
	}

	public Long getDcrId() {
		return dcrId;
	}

	public void setDcrId(Long dcrId) {
		this.dcrId = dcrId;
	}

}
