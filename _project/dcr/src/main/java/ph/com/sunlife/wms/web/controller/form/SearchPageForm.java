package ph.com.sunlife.wms.web.controller.form;

import java.util.Calendar;
import java.util.Date;

import javax.servlet.http.HttpSession;

import org.springframework.validation.Errors;

import ph.com.sunlife.wms.util.WMSDateUtil;
import ph.com.sunlife.wms.web.controller.binder.BinderAware;
import ph.com.sunlife.wms.web.controller.binder.SessionAware;

// Change according to Healthcheck
// *Problem was declared as public/protected in BinderAware
// *Change BinderAware interface class to abstract class and extends it rather than implements
public class SearchPageForm extends BinderAware implements SessionAware {

	private static final String WITHIN_MONTH_KEY = "withinMonth";

	private HttpSession session;

	private String secretKey;

	private String ccId;

	private String hubId;

	private Date dcrStartDate;

	private Date dcrEndDate;

	private String dcrDateStr;

	private Boolean searchByHub;

	private Boolean searchByCC;

	public void setSearchByHub(Boolean searchByHub) {
		this.searchByHub = searchByHub;
	}

	public Boolean getSearchByHub() {
		return searchByHub;
	}

	public Boolean getSearchByCC() {
		return searchByCC;
	}

	public void setSearchByCC(Boolean searchByCC) {
		this.searchByCC = searchByCC;
	}

	public String getCcId() {
		return ccId;
	}

	public void setCcId(String ccId) {
		this.ccId = ccId;
	}

	public String getHubId() {
		return hubId;
	}

	public void setHubId(String hubId) {
		this.hubId = hubId;
	}

	public Date getDcrStartDate() {
		return dcrStartDate;
	}

	public void setDcrStartDate(Date dcrStartDate) {
		if (dcrStartDate != null) {
			this.dcrStartDate = dcrStartDate;
		} else {
			this.dcrStartDate = WMSDateUtil.startOfMonth(WMSDateUtil
					.toFormattedDateStr(new Date()));
		}
		// this.dcrDateStr = WMSDateUtil.toFormattedDateStr(dcrStartDate);

	}

	public Date getDcrEndDate() {
		return dcrEndDate;
	}

	public void setDcrEndDate(Date dcrEndDate) {
		if (dcrEndDate != null) {
			this.dcrEndDate = dcrEndDate;
		} else {
			this.dcrEndDate = WMSDateUtil.endOfMonth(WMSDateUtil
					.toFormattedDateStr(new Date()));
		}
	}

	public String getDcrDateStr() {
		return dcrDateStr;
	}

	public void setDcrDateStr(String dcrDateStr) {
		this.dcrDateStr = dcrDateStr;
//		Calendar c = Calendar.getInstance();
//		if (!dcrDateStr.equalsIgnoreCase(WITHIN_MONTH_KEY)) {
//			this.dcrStartDate = WMSDateUtil.startOfTheDay(WMSDateUtil
//					.toDate(dcrDateStr));
//			c.setTime(WMSDateUtil.toDate(dcrDateStr));
//			c.add(Calendar.DATE, 1);
//			this.dcrEndDate = WMSDateUtil.startOfTheDay(c.getTime());
//		} else {
//			this.dcrStartDate = WMSDateUtil.startOfMonth(WMSDateUtil
//					.toFormattedDateStr(new Date()));
//			c.setTime(WMSDateUtil.endOfMonth(WMSDateUtil
//					.toFormattedDateStr(new Date())));
//			c.add(Calendar.DATE, 1);
//			this.dcrEndDate = WMSDateUtil.startOfTheDay(c.getTime());
//		}
	}

    // Change according to Healthcheck
    // *Problem was declared as public/protected in BinderAware
    // *Change use BinderAware getter instead of direct access
    public String[] getAllowedFields() {
        return super.getAllowedFields();
    }

    // Change according to Healthcheck
    // *Problem was declared as public/protected in BinderAware
    // *Change use BinderAware getter instead of direct access
    public String[] getRequiredFields() {
        return super.getRequiredFields();
    }

    // Change according to Healthcheck
    // *Problem was declared as public/protected in BinderAware
    // *Change use BinderAware getter instead of direct access
    public String[] getUncheckedFields() {
        return super.getUncheckedFields();
    }

	@Override
	public void afterBind() {
	}

	@Override
	public boolean validate(Errors errors) {
		return true;
	}

	@Override
	public HttpSession getSession() {
		return session;
	}

	@Override
	public void setSession(HttpSession session) {
		this.session = session;
	}

	public String getSecretKey() {
		return secretKey;
	}

	public void setSecretKey(String secretKey) {
		this.secretKey = secretKey;
	}

}
