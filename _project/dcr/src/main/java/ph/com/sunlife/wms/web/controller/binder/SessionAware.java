package ph.com.sunlife.wms.web.controller.binder;

import javax.servlet.http.HttpSession;

import ph.com.sunlife.wms.web.controller.AbstractSecuredMultiActionController;

/**
 * A marker interface for {@link Object} Command that is bound to contain an
 * instance of {@link HttpSession}.
 * 
 * @author Zainal Limpao
 * 
 */
public interface SessionAware {

	/**
	 * Get {@link HttpSession}.
	 * 
	 * @return
	 */
	HttpSession getSession();

	/**
	 * Set (@link HttpSession).
	 * 
	 * @param session
	 */
	void setSession(HttpSession session);

	/**
	 * Get the secret key that can be used to bypass security checking of
	 * {@link AbstractSecuredMultiActionController}.
	 * 
	 * @return
	 */
	String getSecretKey();

}
