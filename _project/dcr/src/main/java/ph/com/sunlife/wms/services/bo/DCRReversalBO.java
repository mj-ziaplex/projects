package ph.com.sunlife.wms.services.bo;

import java.util.Date;

import ph.com.sunlife.wms.dao.domain.Currency;
import ph.com.sunlife.wms.util.WMSDateUtil;

public class DCRReversalBO {

	private Long id;

	private Long dcrBalancingToolProductId;

	private double amount;

	private String remarks;

	private Currency currency;

	private Long currencyId;

	private Date dateTime;
	
	private String dateTimeStr;

	public String getDateTimeStr() {
		if (dateTime != null) {
			return WMSDateUtil.toFormattedDateStrWithTime(dateTime);
		}

		return null;
	}

	public Long getDcrBalancingToolProductId() {
		return dcrBalancingToolProductId;
	}

	public void setDcrBalancingToolProductId(Long dcrBalancingToolProductId) {
		this.dcrBalancingToolProductId = dcrBalancingToolProductId;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public Currency getCurrency() {
		return currency;
	}

	public void setCurrency(Currency currency) {
		this.currency = currency;
		this.currencyId = currency.getId();
	}

	public Long getCurrencyId() {
		return currencyId;
	}

	public void setCurrencyId(Long currencyId) {
		this.currencyId = currencyId;
		this.currency = Currency.getCurrency(currencyId);
	}

	public Date getDateTime() {
		return dateTime;
	}

	public void setDateTime(Date dateTime) {
		this.dateTime = dateTime;
		this.dateTimeStr =  WMSDateUtil.toFormattedDateStrWithTime(dateTime);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DCRReversalBO other = (DCRReversalBO) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
