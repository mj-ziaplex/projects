package ph.com.sunlife.wms.services;

import java.util.Date;
import java.util.List;

import ph.com.sunlife.wms.dao.domain.PPAMDS;
import ph.com.sunlife.wms.services.bo.DCRPPAMDSBO;
import ph.com.sunlife.wms.services.bo.PPAMDSBO;
import ph.com.sunlife.wms.services.exception.ServiceException;

/**
 * The Service Interface for PPA MDS Reconciliation.
 * 
 * @author Josephus Sardan
 * @author Edgardo Cunanan
 * 
 */
public interface PPAMDSService {

	/**
	 * Get all {@link PPAMDSBO} given customer center, date range and DCR
	 * Workitem ID.
	 * 
	 * @param customerCenter
	 * @param from
	 * @param to
	 * @param dcrId
	 * @return
	 * @throws ServiceException
	 */
	List<PPAMDSBO> getMDSTransactions(String customerCenter, Date from,
			Date to, Long dcrId) throws ServiceException;

	// /**
	// * Checker if given Business Object has been reconciled.
	// *
	// * @param bo
	// * @return
	// * @throws ServiceException
	// */
	// boolean isReconciled(DCRPPAMDSBO bo) throws ServiceException;

	/**
	 * Set all objects within the list to Reconciled.
	 * 
	 * @param list
	 * @throws ServiceException
	 */
	void reconcilePPAMDSList(List<DCRPPAMDSBO> list, String reconciler)
			throws ServiceException;

	/**
	 * Set all obejcts within the list to Unreconciled.
	 * 
	 * @param list
	 * @throws ServiceException
	 */
	void unreconcilePPAMDSList(List<DCRPPAMDSBO> list) throws ServiceException;

	boolean isMdsReconciled(String customerCenter, Date from, Date to,
			List<PPAMDS> ppaMDS) throws ServiceException;

	void savePPAMDSNotes(Long dcrId, List<DCRPPAMDSBO> entries)
			throws ServiceException;
}
