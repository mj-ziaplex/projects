package ph.com.sunlife.wms.dao.domain;

public class Hub {

	private String hubId;
	private String hubName;
	
	public String getHubId() {
		return hubId;
	}
	public void setHubId(String hubId) {
		this.hubId = hubId;
	}
	public String getHubName() {
		return hubName;
	}
	public void setHubName(String hubName) {
		this.hubName = hubName;
	}
	
}
