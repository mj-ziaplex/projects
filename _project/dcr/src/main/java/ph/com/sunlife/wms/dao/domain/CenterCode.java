package ph.com.sunlife.wms.dao.domain;

import java.io.Serializable;
import java.util.Date;

public class CenterCode implements Serializable {

	private static final long serialVersionUID = -6927908750880523059L;

	private String ccId;
	private String ccName;
	private String ccDesc;
	private Boolean ccAutoIdx;
	private Boolean ccCanEncode;
	private Boolean ccViewImage;
	private Boolean ccZoomZone;
	private Boolean ccActive;
	private String ccCreUser;
	private Date ccCreDate;
	private String ccUpdUser;
	private Date ccUpdDate;
	private String ccAbbrevName;
	private Boolean ccIsIso;
	
	public String getCcId() {
		return ccId;
	}
	public void setCcId(String ccId) {
		this.ccId = ccId;
	}
	public String getCcName() {
		return ccName;
	}
	public void setCcName(String ccName) {
		this.ccName = ccName;
	}
	public String getCcDesc() {
		return ccDesc;
	}
	public void setCcDesc(String ccDesc) {
		this.ccDesc = ccDesc;
	}
	public Boolean getCcAutoIdx() {
		return ccAutoIdx;
	}
	public void setCcAutoIdx(Boolean ccAutoIdx) {
		this.ccAutoIdx = ccAutoIdx;
	}
	public Boolean getCcCanEncode() {
		return ccCanEncode;
	}
	public void setCcCanEncode(Boolean ccCanEncode) {
		this.ccCanEncode = ccCanEncode;
	}
	public Boolean getCcViewImage() {
		return ccViewImage;
	}
	public void setCcViewImage(Boolean ccViewImage) {
		this.ccViewImage = ccViewImage;
	}
	public Boolean getCcZoomZone() {
		return ccZoomZone;
	}
	public void setCcZoomZone(Boolean ccZoomZone) {
		this.ccZoomZone = ccZoomZone;
	}
	public Boolean getCcActive() {
		return ccActive;
	}
	public void setCcActive(Boolean ccActive) {
		this.ccActive = ccActive;
	}
	public String getCcCreUser() {
		return ccCreUser;
	}
	public void setCcCreUser(String ccCreUser) {
		this.ccCreUser = ccCreUser;
	}
	public Date getCcCreDate() {
		return ccCreDate;
	}
	public void setCcCreDate(Date ccCreDate) {
		this.ccCreDate = ccCreDate;
	}
	public String getCcUpdUser() {
		return ccUpdUser;
	}
	public void setCcUpdUser(String ccUpdUser) {
		this.ccUpdUser = ccUpdUser;
	}
	public Date getCcUpdDate() {
		return ccUpdDate;
	}
	public void setCcUpdDate(Date ccUpdDate) {
		this.ccUpdDate = ccUpdDate;
	}
	public String getCcAbbrevName() {
		return ccAbbrevName;
	}
	public void setCcAbbrevName(String ccAbbrevName) {
		this.ccAbbrevName = ccAbbrevName;
	}
	public Boolean getCcIsIso() {
		return ccIsIso;
	}
	public void setCcIsIso(Boolean ccIsIso) {
		this.ccIsIso = ccIsIso;
	}
	
	@Override
	public String toString() {
		return "CenterCode [ccId=" + ccId + ", ccName=" + ccName + ", ccDesc="
				+ ccDesc + ", ccAutoIdx=" + ccAutoIdx + ", ccCanEncode="
				+ ccCanEncode + ", ccViewImage=" + ccViewImage
				+ ", ccZoomZone=" + ccZoomZone + ", ccActive=" + ccActive
				+ ", ccCreUser=" + ccCreUser
				+ ", ccCreDate=" + ccCreDate + ", ccUpdUser=" + ccUpdUser
				+ ", ccUpdDate=" + ccUpdDate + ", ccAbbrevName=" + ccAbbrevName
				+ ", ccIsIso=" + ccIsIso + "]";
	}
}
