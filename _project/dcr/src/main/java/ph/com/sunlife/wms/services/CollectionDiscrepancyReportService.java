package ph.com.sunlife.wms.services;

import java.util.Date;

import ph.com.sunlife.wms.services.exception.ServiceException;

/**
 * The service responsible for generating Monthly Collection Discrepancy Report.
 * 
 * @author Zainal Limpao
 * 
 */
public interface CollectionDiscrepancyReportService {

	/**
	 * Generate Discrepancy Report in byte array format. Note that this is an
	 * .xlsx file.
	 * 
	 * @param template
	 * @param startOfMonth
	 * @param endOfMonth
	 * @return
	 * @throws ServiceException
	 */
	byte[] generateDiscrepancyReport(byte[] template, Date startOfMonth,
			Date endOfMonth) throws ServiceException;

	/**
	 * The method that generates discrepancy report and saves to a configured
	 * folder location.
	 * 
	 * @throws ServiceException
	 */
	void generateDiscrepancyReportNow() throws ServiceException;

}
