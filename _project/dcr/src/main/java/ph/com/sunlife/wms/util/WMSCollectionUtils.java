package ph.com.sunlife.wms.util;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.Predicate;

/**
 * Custom WMS Collection Util class.
 * 
 * @author Zainal Limpao
 * 
 */
public class WMSCollectionUtils {

	private WMSCollectionUtils() {
	}

	/**
	 * Filters the given {@link List} of qualifying objects based on the given
	 * {@link Predicate}.
	 * 
	 * @param <T>
	 * @param list
	 * @param predicate
	 * @return
	 */
	public static <T> List<T> findMatchedElements(List<T> list,
			Predicate predicate) {
		if (list == null) {
			return null;
		}

		List<T> matchedElements = new ArrayList<T>();

		for (T t : list) {
			if (predicate.evaluate(t)) {
				matchedElements.add(t);
			}
		}
		return matchedElements;
	}
}
