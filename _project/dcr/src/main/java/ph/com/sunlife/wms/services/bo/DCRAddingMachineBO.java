package ph.com.sunlife.wms.services.bo;

public class DCRAddingMachineBO {

	private Long id;

	private DCRCashierBO dcrCashier = new DCRCashierBO();

	private byte[] addingMachineFile;
	
	private String dcrAddingMachineVrsSerId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public DCRCashierBO getDcrCashier() {
		return dcrCashier;
	}

	public void setDcrCashier(DCRCashierBO dcrCashier) {
		this.dcrCashier = dcrCashier;
	}

	public void setDcrCashier(Long dcrCashierId) {
		DCRCashierBO dcrCashier = new DCRCashierBO();
		dcrCashier.setId(dcrCashierId);
		this.dcrCashier = dcrCashier;
	}

	public byte[] getAddingMachineFile() {
		return addingMachineFile;
	}

	public void setAddingMachineFile(byte[] addingMachineFile) {
		this.addingMachineFile = addingMachineFile;
	}

	public String getDcrAddingMachineVrsSerId() {
		return dcrAddingMachineVrsSerId;
	}

	public void setDcrAddingMachineVrsSerId(String dcrAddingMachineVrsSerId) {
		this.dcrAddingMachineVrsSerId = dcrAddingMachineVrsSerId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DCRAddingMachineBO other = (DCRAddingMachineBO) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
