package ph.com.sunlife.wms.dao;

import java.util.Date;
import java.util.Map;

import ph.com.sunlife.wms.dao.exceptions.WMSDaoException;

public interface DCRCachedPropertyDao {

	Map<String, Object> getAllCachedProperties() throws WMSDaoException;

	void reset();

	boolean incrementWmsDcrSystemDate(String wmsDcrSystemDateKey,
			Date wmsDcrSystemDate)
			throws WMSDaoException;
	
	boolean updateProperty(String prop_key,String prop_value, String lastUpdatedUser, Long lastUpdatedDate) throws WMSDaoException;
}
