package ph.com.sunlife.wms.web.controller;

import java.beans.PropertyEditor;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;

import ph.com.sunlife.wms.dao.domain.Company;
import ph.com.sunlife.wms.dao.domain.ProductType;
import ph.com.sunlife.wms.services.DCRCreationService;
import ph.com.sunlife.wms.services.bo.DCRBalancingToolProductBO;
import ph.com.sunlife.wms.web.controller.form.SlgfiWorksiteForm;

public class SlgfiBalancingToolController extends
		AbstractBalancingToolController {

	private String displayViewName = "slgfiBalancingToolView";

	private String pageTitle;

	public void setDcrCreationService(DCRCreationService dcrCreationService) {
		this.dcrCreationService = dcrCreationService;
	}

	public ModelAndView saveBalancingToolpage(HttpServletRequest request,
			HttpServletResponse response, SlgfiWorksiteForm form)
			throws Exception {
		List<DCRBalancingToolProductBO> products = new ArrayList<DCRBalancingToolProductBO>();
		
		DCRBalancingToolProductBO product0 = new DCRBalancingToolProductBO();
		product0.setId(form.getId0());
		product0.setWorksiteAmount(form.getWorksiteAmount0());
		products.add(product0);
		
		DCRBalancingToolProductBO product1 = new DCRBalancingToolProductBO();
		product1.setId(form.getId1());
		product1.setWorksiteAmount(form.getWorksiteAmount1());
		products.add(product1);
		
//		DCRBalancingToolProductBO product2 = new DCRBalancingToolProductBO();
//		product2.setId(form.getId2());
//		product2.setWorksiteAmount(form.getWorksiteAmount2());
//		products.add(product2);
//		
//		DCRBalancingToolProductBO product3 = new DCRBalancingToolProductBO();
//		product3.setId(form.getId3());
//		product3.setWorksiteAmount(form.getWorksiteAmount3());
//		products.add(product3);
		
		dcrCreationService.saveWorkSiteAmount(products);
		
		ModelAndView modelAndView = showBalancingToolPage(request, response, form);
		return modelAndView;
	}

	@Override
	protected void registerAdditionalPropertyEditors(
			ServletRequestDataBinder binder) throws Exception {
		PropertyEditor moneyEditor = createMoneyPropertyEditor();
		
		binder.registerCustomEditor(Double.class, "worksiteAmount0", moneyEditor);
		binder.registerCustomEditor(Double.class, "worksiteAmount1", moneyEditor);
		binder.registerCustomEditor(Double.class, "worksiteAmount2", moneyEditor);
		binder.registerCustomEditor(Double.class, "worksiteAmount3", moneyEditor);
	}
	
	@Override
	protected Company getCompany() {
		return Company.SLGFI;
	}

	@Override
	protected String getDisplayViewName() {
		return this.displayViewName;
	}

	public void setDisplayViewName(String displayViewName) {
		this.displayViewName = displayViewName;
	}

    @Override
    protected ProductType[] productTypesToDisplay() {
        // Change according to Healthcheck
        // Affected in due to change in AbstractBalancingToolController
        // Uses the parent method to display/use as getter
        return super.productTypesToDisplay();
    }

	@Override
	protected String getPageTitle() {
		return pageTitle;
	}

	public void setPageTitle(String pageTitle) {
		this.pageTitle = pageTitle;
	}
}
