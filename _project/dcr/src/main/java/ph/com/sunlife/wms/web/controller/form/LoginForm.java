package ph.com.sunlife.wms.web.controller.form;

import org.springframework.validation.Errors;

import ph.com.sunlife.wms.web.controller.SecurityController;
import ph.com.sunlife.wms.web.controller.binder.BinderAware;

/**
 * The command {@link Object} for the {@link SecurityController}.
 * 
 * @author Zainal Limpao
 * 
 */
// Change according to Healthcheck
// *Problem was declared as public/protected in BinderAware
// *Change BinderAware interface class to abstract class and extends it rather than implements
public class LoginForm extends BinderAware {

	private String userId;

	private String password;

	private String siteCode;

	private String selectedRole;
	
	private String adminUserId;

	public String getSelectedRole() {
		return selectedRole;
	}

	public void setSelectedRole(String selectedRole) {
		this.selectedRole = selectedRole;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getSiteCode() {
		return siteCode;
	}

	public void setSiteCode(String siteCode) {
		this.siteCode = siteCode;
	}

    // Change according to Healthcheck
    // *Problem was declared as public/protected in BinderAware
    // *Change use BinderAware getter instead of direct access
    public String[] getAllowedFields() {
        return super.getAllowedFields();
    }

	@Override
	public String[] getRequiredFields() {
		return new String[] { "userId", "password", "siteCode", "selectedRole" };
	}

    // Change according to Healthcheck
    // *Problem was declared as public/protected in BinderAware
    // *Change use BinderAware getter instead of direct access
    public String[] getUncheckedFields() {
        return super.getUncheckedFields();
    }

	@Override
	public void afterBind() {
	}

	@Override
	public boolean validate(Errors errors) {
		return true;
	}

	public String getAdminUserId() {
		return adminUserId;
	}

	public void setAdminUserId(String adminUserId) {
		this.adminUserId = adminUserId;
	}

}
