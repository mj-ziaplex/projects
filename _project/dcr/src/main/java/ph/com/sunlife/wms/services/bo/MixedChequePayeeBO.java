package ph.com.sunlife.wms.services.bo;

import java.util.Date;

import ph.com.sunlife.wms.dao.domain.CheckType;
import ph.com.sunlife.wms.dao.domain.Currency;

public class MixedChequePayeeBO {

	private Long id;

	private DCRCashierBO dcrCashier = new DCRCashierBO();

	private BankBO bank = new BankBO();

	private String bankId;
	
	private String checkNumber;

	private Date checkDate;
	
	private String checkDateStr;

	private double checkAmount;

	private CheckType checkType;

	private CompanyBO payee;

	private double slamci;

	private double slocpi;

	private double slgfi;

	private double slfpi;

	private double others;
	
	private Currency currency;

	public String getCheckDateStr() {
		return checkDateStr;
	}

	public void setCheckDateStr(String checkDateStr) {
		this.checkDateStr = checkDateStr;
	}

	public Currency getCurrency() {
		return currency;
	}

	public void setCurrency(Currency currency) {
		this.currency = currency;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public DCRCashierBO getDcrCashier() {
		return dcrCashier;
	}

	public void setDcrCashier(DCRCashierBO dcrCashier) {
		this.dcrCashier = dcrCashier;
	}
	
	public void setDcrCashier(Long dcrCashierId) {
		DCRCashierBO dcrCashier = new DCRCashierBO();
		dcrCashier.setId(dcrCashierId);
		this.setDcrCashier(dcrCashier);
	}	

	public BankBO getBank() {
		return bank;
	}

	public void setBank(BankBO bank) {
		this.bank = bank;
	}
	
	public void setBank(String id) {
		BankBO bank = new BankBO();
		bank.setId(id);
		this.setBank(bank);
		setBankId(id);
	}

	public String getCheckNumber() {
		return checkNumber;
	}

	public void setCheckNumber(String checkNumber) {
		this.checkNumber = checkNumber;
	}

	public Date getCheckDate() {
		return checkDate;
	}

	public void setCheckDate(Date checkDate) {
		this.checkDate = checkDate;
	}

	public double getCheckAmount() {
		return checkAmount;
	}

	public void setCheckAmount(double checkAmount) {
		this.checkAmount = checkAmount;
	}

	public CheckType getCheckType() {
		return checkType;
	}

	public void setCheckType(CheckType checkType) {
		this.checkType = checkType;
	}

	public CompanyBO getPayee() {
		return payee;
	}

	public void setPayee(CompanyBO payee) {
		this.payee = payee;
	}

	public double getSlamci() {
		return slamci;
	}

	public void setSlamci(double slamci) {
		this.slamci = slamci;
	}

	public double getSlocpi() {
		return slocpi;
	}

	public void setSlocpi(double slocpi) {
		this.slocpi = slocpi;
	}

	public double getSlgfi() {
		return slgfi;
	}

	public void setSlgfi(double slgfi) {
		this.slgfi = slgfi;
	}

	public double getSlfpi() {
		return slfpi;
	}

	public void setSlfpi(double slfpi) {
		this.slfpi = slfpi;
	}

	public double getOthers() {
		return others;
	}

	public void setOthers(double others) {
		this.others = others;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MixedChequePayeeBO other = (MixedChequePayeeBO) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public void setBankId(String bankId) {
		this.bankId = bankId;
	}

	public String getBankId() {
		return bankId;
	}


	
}
