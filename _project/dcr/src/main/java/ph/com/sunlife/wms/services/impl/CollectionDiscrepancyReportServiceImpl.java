package ph.com.sunlife.wms.services.impl;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import ph.com.sunlife.wms.dao.CollectionDiscrepancyReportDao;
import ph.com.sunlife.wms.dao.DCRDao;
import ph.com.sunlife.wms.dao.JobDataDao;
import ph.com.sunlife.wms.dao.domain.DCR;
import ph.com.sunlife.wms.dao.domain.DCRAuditLog;
import ph.com.sunlife.wms.dao.domain.DiscrepancyReportEntry;
import ph.com.sunlife.wms.dao.domain.JobData;
import ph.com.sunlife.wms.dao.domain.JobDataErrorLog;
import ph.com.sunlife.wms.dao.domain.StartEndMonth;
import ph.com.sunlife.wms.dao.exceptions.WMSDaoException;
import ph.com.sunlife.wms.services.CachingService;
import ph.com.sunlife.wms.services.CollectionDiscrepancyReportService;
import ph.com.sunlife.wms.services.bo.DiscrepancyReportEntryBO;
import ph.com.sunlife.wms.services.exception.ServiceException;
import ph.com.sunlife.wms.util.ExceptionUtil;
import ph.com.sunlife.wms.util.WMSDateUtil;

/**
 * The implementing class of {@link CollectionDiscrepancyReportService}.
 * 
 * @author Zainal Limpao
 * 
 */
public class CollectionDiscrepancyReportServiceImpl implements
		CollectionDiscrepancyReportService {

	private static final long COLLECTION_DISCREPANCY_JOB_ID = 2L;

	private static final Logger LOGGER = Logger
			.getLogger(CollectionDiscrepancyReportServiceImpl.class);

	private DCRDao dcrDao;

	private CollectionDiscrepancyReportDao collectionDiscrepancyReportDao;

	// FIXME: this should be configurable through Spring. (FIXED)
	private String destinationFolder = "\\\\phd001pv70\\DiscrepancyReports\\";

	private String templateFilename = "discrepancy_report_template.xlsx";

	private JobDataDao jobDataDao;

	private CachingService cachingService;

	public void setCachingService(CachingService cachingService) {
		this.cachingService = cachingService;
	}

	public void setJobDataDao(JobDataDao jobDataDao) {
		this.jobDataDao = jobDataDao;
	}

	public void setTemplateFilename(String templateFilename) {
		this.templateFilename = templateFilename;
	}

	public void setDestinationFolder(String destinationFolder) {
		this.destinationFolder = destinationFolder;
	}

	public void setDcrDao(DCRDao dcrDao) {
		this.dcrDao = dcrDao;
	}

	public void setCollectionDiscrepancyReportDao(
			CollectionDiscrepancyReportDao collectionDiscrepancyReportDao) {
		this.collectionDiscrepancyReportDao = collectionDiscrepancyReportDao;
	}

	/**
	 * Finds the {@link DCR} amongst the given {@link List}.
	 * 
	 * @param id
	 * @param dcrList
	 * @return
	 */
	private DCR findById(final Long id, List<DCR> dcrList) {
		DCR dcr = null;
		if (CollectionUtils.isNotEmpty(dcrList)) {
			dcr = (DCR) CollectionUtils.find(dcrList, new Predicate() {

				@Override
				public boolean evaluate(Object object) {
					DCR dcr = (DCR) object;
					return dcr.getId().equals(id);
				}
			});
		}

		return dcr;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ph.com.sunlife.wms.services.CollectionDiscrepancyReportService#
	 * generateDiscrepancyReport(byte[], java.util.Date, java.util.Date)
	 */
	@Override
	public byte[] generateDiscrepancyReport(byte template[], Date startOfMonth,
			Date endOfMonth) throws ServiceException {
		StartEndMonth param = new StartEndMonth();
		param.setEndOfMonth(endOfMonth);
		param.setStartOfMonth(startOfMonth);

		List<DiscrepancyReportEntryBO> reportEntries = new ArrayList<DiscrepancyReportEntryBO>();

		try {
			List<DiscrepancyReportEntry> timelinessEntries = collectionDiscrepancyReportDao
					.getTimelinessDiscrepancyReports(param);
			this.extractTimelinessDiscrepancyReports(timelinessEntries,
					reportEntries);

			List<DiscrepancyReportEntry> accuracyEntries = collectionDiscrepancyReportDao
					.getAccuracyDiscrepancyReports(param);
			this.extractAccuracyDiscrepancyReports(reportEntries,
					accuracyEntries);

			if (CollectionUtils.isNotEmpty(reportEntries)) {
				Collections.sort(reportEntries);
			}
		} catch (WMSDaoException e) {
			LOGGER.error(e);
			throw new ServiceException(e);
		}

		return this.buildExcelReport(template, reportEntries, startOfMonth);
	}

	/**
	 * Creates the Excel Report (XLSX) in byte array format.
	 * 
	 * @param template
	 * @param reportEntries
	 * @param startOfMonth
	 * @return
	 * @throws ServiceException
	 */
	private byte[] buildExcelReport(byte[] template,
			List<DiscrepancyReportEntryBO> reportEntries, Date startOfMonth)
			throws ServiceException {

		if (CollectionUtils.isNotEmpty(reportEntries) && template != null) {
			InputStream inputStream = null;
			ByteArrayOutputStream outputStream = null;

			try {
				inputStream = new ByteArrayInputStream(template);
				Workbook workbook = WorkbookFactory.create(inputStream);

				Sheet sheet = workbook.getSheet("Report");

				Row monthTitleRow = sheet.getRow(1);
				Cell monthTitleCell = monthTitleRow.getCell(0);
				String monthTitle = monthTitleCell.getStringCellValue();

				DateFormat df = new SimpleDateFormat("MMMMM-yyyy");
				String monthStr = df.format(startOfMonth);

				monthTitle = StringUtils.replace(monthTitle, "MMMMM-yyyy",
						monthStr);

				monthTitleCell.setCellValue(monthTitle);

				Row reportDateRow = sheet.getRow(2);
				Cell reportDateCell = reportDateRow.getCell(0);
				String reportDateTitle = reportDateCell.getStringCellValue();

				reportDateTitle = StringUtils
						.replace(reportDateTitle, "ddMMMyyyy",
								WMSDateUtil.toFormattedDateStr(new Date()));

				reportDateCell.setCellValue(reportDateTitle);

				int startingRow = 4;
				for (DiscrepancyReportEntryBO entry : reportEntries) {
					int rowNum = startingRow++;
					Row row = sheet.createRow(rowNum);

					Cell dcrDateCell = row.createCell(0);
					dcrDateCell.setCellType(Cell.CELL_TYPE_STRING);
					dcrDateCell.setCellValue(entry.getDcrDateStr());

					CellStyle cellStyle = workbook.createCellStyle();
					cellStyle.setBorderBottom(CellStyle.BORDER_THIN);
					cellStyle.setBorderTop(CellStyle.BORDER_THIN);
					cellStyle.setBorderRight(CellStyle.BORDER_THIN);
					cellStyle.setBorderLeft(CellStyle.BORDER_THIN);

					dcrDateCell.setCellStyle(cellStyle);

					Cell ccNameCell = row.createCell(1);
					ccNameCell.setCellType(Cell.CELL_TYPE_STRING);
					ccNameCell.setCellValue(entry.getCcName());
					ccNameCell.setCellStyle(cellStyle);

					Cell findingsCell = row.createCell(2);
					findingsCell.setCellType(Cell.CELL_TYPE_STRING);
					findingsCell.setCellValue(entry.getDiscrepancyType());
					findingsCell.setCellStyle(cellStyle);

					Cell remarksCell = row.createCell(3);
					remarksCell.setCellType(Cell.CELL_TYPE_STRING);
					remarksCell.setCellValue(entry.getRemarks());
					remarksCell.setCellStyle(cellStyle);
				}

				outputStream = new ByteArrayOutputStream();
				workbook.write(outputStream);

				return outputStream.toByteArray();
			} catch (InvalidFormatException e) {
				LOGGER.error(e);
				throw new ServiceException(e);
			} catch (IOException e) {
				LOGGER.error(e);
				throw new ServiceException(e);
			} finally {
				IOUtils.closeQuietly(inputStream);
				IOUtils.closeQuietly(outputStream);
			}
		}

		return null;

	}

	/**
	 * Retrieves Accuracy-related {@link DiscrepancyReportEntry} from the
	 * database. This is based on the number of PPA Findings.
	 * 
	 * @param reportEntries
	 * @param accuracyEntries
	 * @throws WMSDaoException
	 */
	private void extractAccuracyDiscrepancyReports(
			List<DiscrepancyReportEntryBO> reportEntries,
			List<DiscrepancyReportEntry> accuracyEntries)
			throws WMSDaoException {
		if (CollectionUtils.isNotEmpty(accuracyEntries)) {
			List<Long> dcrIds = new ArrayList<Long>();

			for (DiscrepancyReportEntry entry : accuracyEntries) {
				dcrIds.add(entry.getDcrId());
			}

			List<DCR> dcrList = dcrDao.getByIds(dcrIds);

			for (DiscrepancyReportEntry entry : accuracyEntries) {
				Long dcrId = entry.getDcrId();
				DCR dcr = findById(dcrId, dcrList);

				if (dcr != null) {
					DiscrepancyReportEntryBO reportEntry = new DiscrepancyReportEntryBO();

					Date dcrDate = dcr.getDcrDate();
					String formattedDateStr = WMSDateUtil
							.toFormattedDateStr(dcrDate);

					reportEntry.setDcrDate(dcrDate);
					reportEntry.setCcId(dcr.getCcId());
					reportEntry.setCcName(dcr.getCcName());
					reportEntry.setDcrDateStr(formattedDateStr);
					reportEntry.setDiscrepancyType("Accuracy");
					reportEntry.setRemarks("Findings: " + entry.getFindings());

					reportEntries.add(reportEntry);
				}

			}
		}
	}

	/**
	 * Retrieves Accuracy-related {@link DiscrepancyReportEntry} from the
	 * database. This is based on the number of days a DCR went passed the
	 * Required Completion Date before submission to PPA.
	 * 
	 * @param timelinessEntries
	 * @param reportEntries
	 */
	private void extractTimelinessDiscrepancyReports(
			List<DiscrepancyReportEntry> timelinessEntries,
			List<DiscrepancyReportEntryBO> reportEntries)
			throws ServiceException {
		if (CollectionUtils.isNotEmpty(timelinessEntries)) {
			for (DiscrepancyReportEntry entry : timelinessEntries) {
				DiscrepancyReportEntryBO reportEntry = new DiscrepancyReportEntryBO();

				boolean isSubmittedLate = false;

				Date requiredCompletionDate = entry.getRequiredCompletionDate();
				Date updatedDate = entry.getLastUpdateDate();
				Date dcrDate = entry.getDcrDate();
				String formattedDateStr = WMSDateUtil
						.toFormattedDateStr(dcrDate);
				Long dcrId = entry.getDcrId();
				String remarks = "";
				String status = entry.getStatus();

				if ("Approved for Recon".equalsIgnoreCase(status)) {
					if (updatedDate.after(requiredCompletionDate)) {
						int daysDifference = WMSDateUtil.daysBetween(
								requiredCompletionDate, updatedDate);
						isSubmittedLate = true;
						remarks = "Submission to PPA was " + daysDifference
								+ " day(s) overdue";
					}
				} else if ("Reqts Not Submitted".equalsIgnoreCase(status)
						|| "Awaiting Reqts".equalsIgnoreCase(status)
						|| "Reconciled".equalsIgnoreCase(status)
						|| "For Verification".equalsIgnoreCase(status)
						|| "Not Verified".equalsIgnoreCase(status)
						|| "Verified".equalsIgnoreCase(status)
						|| "Reqts Submitted".equalsIgnoreCase(status)
						|| "Reconciled".equalsIgnoreCase(status)) {
					List<DCRAuditLog> auditLogs = getAuditLogs(dcrId);

					Date lastUpdateDate = null;
					if (CollectionUtils.isNotEmpty(auditLogs)) {
						lastUpdateDate = getSubmissionToPPADate(auditLogs);
					}

					if (lastUpdateDate != null
							&& lastUpdateDate.after(requiredCompletionDate)) {
						isSubmittedLate = true;

						int daysDifference = WMSDateUtil.daysBetween(
								requiredCompletionDate, lastUpdateDate);
						remarks = "Submission to PPA was " + daysDifference
								+ " day(s) overdue";
					}
				} else {
					updatedDate = new Date();

					if (updatedDate.after(requiredCompletionDate)) {
						isSubmittedLate = true;
						remarks = "DCR has not been submitted to PPA.";
					}
				}

				reportEntry.setDcrDate(dcrDate);
				reportEntry.setCcId(entry.getCcId());
				reportEntry.setCcName(entry.getCcName());
				reportEntry.setDiscrepancyType("Timeliness");
				reportEntry.setDcrDateStr(formattedDateStr);
				reportEntry.setRemarks(remarks);

				if (isSubmittedLate) {
					reportEntries.add(reportEntry);
				}
			}

		}
	}

	private Date getSubmissionToPPADate(List<DCRAuditLog> auditLogs) {
		Date lastUpdateDate = null;
		for (DCRAuditLog log : auditLogs) {
			if ("WF Action: Submit to PPA".equalsIgnoreCase(log
					.getActionDetails())
					&& "Approved for Recon"
							.equalsIgnoreCase(log
									.getProcessStatus())) {
				lastUpdateDate = log.getLogDate();
				break;
			}
		}
		return lastUpdateDate;
	}

	private List<DCRAuditLog> getAuditLogs(Long dcrId) throws ServiceException {
		try {
			List<DCRAuditLog> auditLogs = dcrDao.getAuditLogsByDcrId(dcrId);
			return auditLogs;
		} catch (WMSDaoException ex) {
			throw new ServiceException(ex);
		}
	}

	/**
	 * An end call that logs the failure of Job Run.
	 * 
	 * @param today
	 * @param e
	 * @throws ServiceException
	 */
	private void failedJobRun(Date today, ServiceException e)
			throws ServiceException {
		try {
			String stackTrace = ExceptionUtil.getStackTrace(e, 1000);
			JobDataErrorLog log = new JobDataErrorLog();
			log.setDateTime(today);
			log.setErrorMessage(stackTrace);
			jobDataDao.createErrorLog(log);

			JobData jobData = new JobData();
			jobData.setId(COLLECTION_DISCREPANCY_JOB_ID);
			jobData.setLastRun(today);
			jobData.setSuccessful(false);
			jobDataDao.updateJobData(jobData);

		} catch (WMSDaoException ex1) {
			LOGGER.error(ex1);
			throw new ServiceException(ex1);
		}
	}

	/**
	 * An end call that logs the success of Job Run.
	 * 
	 * @param today
	 * @throws ServiceException
	 */
	private void successfulJobRun(Date today) throws ServiceException {
		try {
			JobData jobData = new JobData();
			jobData.setId(COLLECTION_DISCREPANCY_JOB_ID);
			jobData.setLastRun(today);
			jobData.setSuccessful(true);
			jobDataDao.updateJobData(jobData);
		} catch (WMSDaoException ex1) {
			LOGGER.error(ex1);
			throw new ServiceException(ex1);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ph.com.sunlife.wms.services.CollectionDiscrepancyReportService#
	 * generateDiscrepancyReportNow()
	 */
	@Override
	public void generateDiscrepancyReportNow() throws ServiceException {

		InputStream input = getClass().getClassLoader().getResourceAsStream(
				this.templateFilename);

		OutputStream output = null;
		Date today = cachingService.getWmsDcrSystemDate();

		try {
			byte[] template = IOUtils.toByteArray(input);

			Date aMonthAgo = DateUtils.addMonths(today, -1);
			Date start = WMSDateUtil.startOfMonth(WMSDateUtil
					.toFormattedDateStr(aMonthAgo));
			Date end = DateUtils.addMonths(start, 1);

			byte[] excelFile = this.generateDiscrepancyReport(template, start,
					end);

			DateFormat df = new SimpleDateFormat("MMMMM-yyyy");
			String filename = "DCR_Discrepancy_Report_" + df.format(start)
					+ ".xlsx";

			File file = new File(this.destinationFolder + filename);
			output = new FileOutputStream(file);
			IOUtils.write(excelFile, output);
			output.flush();

			successfulJobRun(today);
		} catch (IOException e) {
			LOGGER.error(e);
			throw new ServiceException(e);
		} catch (ServiceException e) {
			this.failedJobRun(today, e);
			throw new ServiceException(e);
		} finally {
			IOUtils.closeQuietly(input);
			IOUtils.closeQuietly(output);
		}
	}

	public String getDestinationFolder() {
		return destinationFolder;
	}

}
