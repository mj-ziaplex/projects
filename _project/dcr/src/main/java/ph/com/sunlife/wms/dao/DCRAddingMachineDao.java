/**
 * 
 */
package ph.com.sunlife.wms.dao;

import ph.com.sunlife.wms.dao.domain.DCRAddingMachine;
import ph.com.sunlife.wms.dao.exceptions.WMSDaoException;

/**
 * @author Zainal Limpao
 * 
 */
public interface DCRAddingMachineDao extends WMSDao<DCRAddingMachine> {

	DCRAddingMachine getByDcrCashierId(Long dcrCashierId)
			throws WMSDaoException;
}
