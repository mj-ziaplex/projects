package ph.com.sunlife.wms.dao.impl;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;

import ph.com.sunlife.wms.dao.AbstractWMSSqlMapClientDaoSupport;
import ph.com.sunlife.wms.dao.DCRChequeDepositSlipDao;
import ph.com.sunlife.wms.dao.domain.DCRChequeDepositSlip;
import ph.com.sunlife.wms.dao.exceptions.WMSDaoException;

public class DCRChequeDepositSlipDaoImpl extends
		AbstractWMSSqlMapClientDaoSupport<DCRChequeDepositSlip> implements
		DCRChequeDepositSlipDao {
	
	private static final String NAMESPACE = "DCRChequeDepositSlip";
	
	@Override
	protected String getNamespace() {
		return NAMESPACE;
	}

	@Override
	public DCRChequeDepositSlip getChequeDepositSlip(
			DCRChequeDepositSlip example) throws WMSDaoException {
		@SuppressWarnings("unchecked")
		List<DCRChequeDepositSlip> list = getSqlMapClientTemplate().queryForList(
				getNamespace() + ".getChequeDepositSlip", example);

		if (CollectionUtils.isNotEmpty(list)) {
			return list.get(0);
		}

		return null;
	}

	@SuppressWarnings("unchecked")
	public List<DCRChequeDepositSlip> getAllChequeDepositSlipsByDcrId(Long dcrId)
			throws WMSDaoException {
		List<DCRChequeDepositSlip> results = null;

		try {
			results = getSqlMapClientTemplate().queryForList(
					getNamespace() + ".getAllChequeDepositSlipByDcrId", dcrId);
		} catch (Exception e) {
			throw new WMSDaoException(e);
		}

		return results;
	}

	@SuppressWarnings("unchecked")
	public List<DCRChequeDepositSlip> getAllChequeDepositSlipsByDcrCashierId(
			Long dcrCashierId) throws WMSDaoException {
		List<DCRChequeDepositSlip> results = null;

		try {
			results = getSqlMapClientTemplate().queryForList(
					getNamespace() + ".getAllChequeDepositSlipByDcrCashierId",
					dcrCashierId);
		} catch (Exception e) {
			throw new WMSDaoException(e);
		}

		return results;
	}


}
