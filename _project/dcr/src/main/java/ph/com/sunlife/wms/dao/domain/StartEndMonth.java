package ph.com.sunlife.wms.dao.domain;

import java.util.Date;

public class StartEndMonth {

	private Date startOfMonth;
	
	private Date endOfMonth;

	public Date getStartOfMonth() {
		return startOfMonth;
	}

	public void setStartOfMonth(Date startOfMonth) {
		this.startOfMonth = startOfMonth;
	}

	public Date getEndOfMonth() {
		return endOfMonth;
	}

	public void setEndOfMonth(Date endOfMonth) {
		this.endOfMonth = endOfMonth;
	}
	
}
