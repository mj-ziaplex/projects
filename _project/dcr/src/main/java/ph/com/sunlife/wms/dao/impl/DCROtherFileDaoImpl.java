package ph.com.sunlife.wms.dao.impl;

import java.util.List;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import ph.com.sunlife.wms.dao.DCROtherFileDao;
import ph.com.sunlife.wms.dao.domain.DCROtherFile;
import ph.com.sunlife.wms.dao.exceptions.WMSDaoException;

public class DCROtherFileDaoImpl extends SqlMapClientDaoSupport implements
		DCROtherFileDao {

	private static final String NAMESPACE = "DCROtherFile";

	@Override
	public Long saveOtherFile(DCROtherFile entity) throws WMSDaoException {
		Long id = null;

		try {
			id = (Long) getSqlMapClientTemplate().queryForObject(
					NAMESPACE + ".saveOtherFile", entity);
		} catch (Exception ex) {
			throw new WMSDaoException(ex);
		}

		return id;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<DCROtherFile> getOtherFilesByDcrCashierId(Long dcrCashierId)
			throws WMSDaoException {
		List<DCROtherFile> files = null;
		try {
			files = getSqlMapClientTemplate().queryForList(
					NAMESPACE + ".getOtherFilesByDcrCashierId", dcrCashierId);
		} catch (Exception ex) {
			throw new WMSDaoException(ex);
		}

		return files;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<DCROtherFile> getOtherFilesByDcrId(Long dcrId)
			throws WMSDaoException {
		List<DCROtherFile> files = null;
		try {
			files = getSqlMapClientTemplate().queryForList(
					NAMESPACE + ".getOtherFilesByDcrId", dcrId);
		} catch (Exception ex) {
			throw new WMSDaoException(ex);
		}

		return files;
	}

}
