package ph.com.sunlife.wms.services.bo;

import java.util.Date;

public class DCRCommentBO {

	private Long id;

	private DCRCashierBO dcrCashier = new DCRCashierBO();

	private String acf2id;

	private String remarks;

	private Date datePosted;

	private String datePostedFormatted;
	
	private Long dcrId;

	public String getDatePostedFormatted() {
		return datePostedFormatted;
	}

	public void setDatePostedFormatted(String datePostedFormatted) {
		this.datePostedFormatted = datePostedFormatted;
	}

	public DCRCashierBO getDcrCashier() {
		return dcrCashier;
	}

	public void setDcrCashier(DCRCashierBO dcrCashier) {
		this.dcrCashier = dcrCashier;
	}

	public void setDcrCashier(Long dcrCashierId) {
		DCRCashierBO dcrCashier = new DCRCashierBO();
		dcrCashier.setId(dcrCashierId);
		this.dcrCashier = dcrCashier;
	}

	public String getAcf2id() {
		return acf2id;
	}

	public void setAcf2id(String acf2id) {
		this.acf2id = acf2id;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public Date getDatePosted() {
		return datePosted;
	}

	public void setDatePosted(Date datePosted) {
		this.datePosted = datePosted;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DCRCommentBO other = (DCRCommentBO) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public Long getDcrId() {
		return dcrId;
	}

	public void setDcrId(Long dcrId) {
		this.dcrId = dcrId;
	}

}
