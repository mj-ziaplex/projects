///*
// * $Id: SimpleSpyProfiler.java,v 1.2 2012/10/18 10:08:32 PV70 Exp $
// *
// * Copyright (c) 2010 HEB
// * All rights reserved.
// *
// * This software is the confidential and proprietary information
// * of HEB.
// * 
// * COMMENTED AS NOT BEING USED
// */
//package ph.com.sunlife.wms.logging.aop;
//
//import org.aspectj.lang.ProceedingJoinPoint;
//import org.springframework.util.StopWatch;
//
//public class SimpleSpyProfiler extends AbstractProfiler {
//
//	@Override
//	protected String createMessage(ProceedingJoinPoint call, Object retVal, StopWatch clock) {
//		return "message";
//	}
//
//	@Override
//	protected void log(String message) {
//	}
//
//	@Override
//	protected void log(String message, Throwable t) {
//	}
//
//
//	/**
//	 * Creates the spy instance.
//	 *
//	 * @return the simple spy profiler
//	 */
//	public static SimpleSpyProfiler createSpyInstance(){
//		return null;
//	}
//
//
//}
