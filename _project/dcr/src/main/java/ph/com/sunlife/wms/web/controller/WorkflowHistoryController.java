package ph.com.sunlife.wms.web.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.web.servlet.ModelAndView;

import ph.com.sunlife.wms.security.Role;
import ph.com.sunlife.wms.services.ConsolidatedDataService;
import ph.com.sunlife.wms.services.bo.AuditLog;
import ph.com.sunlife.wms.services.exception.ServiceException;
import ph.com.sunlife.wms.web.controller.form.CashierSPForm;
import ph.com.sunlife.wms.web.exception.ApplicationException;

/**
 * The Controller behind the Workflow History Popup Page.
 * 
 * @author Zainal Limpao
 * 
 */
public class WorkflowHistoryController extends
		AbstractSecuredMultiActionController {

	private static final Logger LOGGER = Logger
			.getLogger(WorkflowHistoryController.class);

	private static final String WF_HISTORY_VIEW_KEY = "wfHistoryView";

	private static final String AUDIT_LOG_KEY = "auditLog";

	private static final String TITLE_KEY = "title";

	private ConsolidatedDataService consolidatedDataService;

	/**
	 * Displays the list of {@link AuditLog} into the Data Table.
	 * 
	 * @param request
	 * @param response
	 * @param form
	 * @return
	 * @throws ApplicationException
	 */
	public ModelAndView doShowHomePage(HttpServletRequest request,
			HttpServletResponse response, CashierSPForm form)
			throws ApplicationException {
		LOGGER.info("Showing Workflow History Page...");

		ModelAndView modelAndView = new ModelAndView();
		modelAndView.addObject(TITLE_KEY, "Workflow History");
		try {
			Long dcrId = form.getDcrId();
			List<AuditLog> logs = consolidatedDataService.getAuditLog(dcrId);

			// Make sure that Process Status is always in upper case
			if (CollectionUtils.isNotEmpty(logs)) {
				for (AuditLog log : logs) {
					log.setProcessStatus(StringUtils.upperCase(log
							.getProcessStatus()));
				}
			}

			modelAndView.addObject(AUDIT_LOG_KEY, logs);
		} catch (ServiceException e) {
			LOGGER.error(e);
			throw new ApplicationException(e);
		}

		modelAndView.setViewName(WF_HISTORY_VIEW_KEY);

		return modelAndView;

	}

    @Override
    protected Role[] allowedRoles() {
        // Change according to Healthcheck
        // Problem was due to changes in AbstractSecuredMultiActionController
        // Change was use parent getter method instead of inherited values
        return super.getALL_ROLES();
    }

	public void setConsolidatedDataService(
			ConsolidatedDataService consolidatedDataService) {
		this.consolidatedDataService = consolidatedDataService;
	}

}
