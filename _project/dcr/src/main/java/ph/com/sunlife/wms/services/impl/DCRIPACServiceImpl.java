/**
 * @author i176
 * ****************************************************************************
 * @author: ia23 - sasay
 * @date: Mar2016
 * @desc: Removed methods and parameters that will not be used due to 
 * MR-WF-15-00089 - DCR Redesign
 */
package ph.com.sunlife.wms.services.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;

import ph.com.sunlife.wms.dao.DCRDao;
import ph.com.sunlife.wms.dao.DCRIPACDao;
import ph.com.sunlife.wms.dao.JobDataDao;
import ph.com.sunlife.wms.dao.domain.Company;
import ph.com.sunlife.wms.dao.domain.DCR;
import ph.com.sunlife.wms.dao.domain.DCRIpacValue;
import ph.com.sunlife.wms.dao.domain.GAFValue;
import ph.com.sunlife.wms.dao.domain.JobData;
import ph.com.sunlife.wms.dao.domain.JobDataErrorLog;
import ph.com.sunlife.wms.dao.domain.NonPostedTransaction;
import ph.com.sunlife.wms.dao.domain.PPAMDS;
import ph.com.sunlife.wms.dao.exceptions.WMSDaoException;
import ph.com.sunlife.wms.services.CachingService;
import ph.com.sunlife.wms.services.DCRIPACService;
import ph.com.sunlife.wms.services.DCRIngeniumService;
import ph.com.sunlife.wms.services.exception.ServiceException;
import ph.com.sunlife.wms.util.ExceptionUtil;
import ph.com.sunlife.wms.util.WMSCollectionUtils;
import ph.com.sunlife.wms.util.WMSConstants;
import ph.com.sunlife.wms.util.WMSDateUtil;

/**
 * @author i176
 *
 */
public class DCRIPACServiceImpl implements DCRIPACService {

    private static final Logger LOGGER = Logger.getLogger(DCRIPACServiceImpl.class);

    private static final long IPAC_DCR_CONVERSION_JOB_ID = 5L;

    private DCRDao dcrDao;
	
    private DCRIPACDao dcrIPACDao;
	
    private JobDataDao jobDataDao;
	
    private CachingService cachingService;
	
    private DCRIngeniumService dcrIngeniumService;
    // Change according to Healthcheck
    // *Problem as new values are being assign in static variable
    // *Change was to remove static as variable value is changing logic wise
    private boolean isRunning;

    public void setDcrIngeniumService(DCRIngeniumService dcrIngeniumService) {
        this.dcrIngeniumService = dcrIngeniumService;
    }

    public CachingService getCachingService() {
        return cachingService;
    }

    public void setCachingService(CachingService cachingService) {
        this.cachingService = cachingService;
    }


    public JobDataDao getJobDataDao() {
        return jobDataDao;
    }


    public void setJobDataDao(JobDataDao jobDataDao) {
        this.jobDataDao = jobDataDao;
    }

    public DCRDao getDcrDao() {
        return dcrDao;
    }


    public void setDcrDao(DCRDao dcrDao) {
        this.dcrDao = dcrDao;
    }


    public DCRIPACDao getDcrIPACDao() {
        return dcrIPACDao;
    }


    public void setDcrIPACDao(DCRIPACDao dcrIPACDao) {
        this.dcrIPACDao = dcrIPACDao;
    }


    /* (non-Javadoc)
     * @see ph.com.sunlife.wms.services.DCRIPACService#runIPACToDCRJob()
     */
    @Override
    public boolean runIPACToDCRJob() throws ServiceException {
        Date systemDate = cachingService.getWmsDcrSystemDate();
        boolean successRun = true;
        try {
            if (!isRunning) {
                List<DCR> dcrList = dcrDao.getDCRWorkItemsForIPACDownload();
                if (CollectionUtils.isNotEmpty(dcrList)) {
                    for (DCR dcr : dcrList) {
                        removeIPACAndIngeniumData(dcr);
                        isRunning = processIPACAndIngeniumData(dcr);
                    }
                } else {
                    LOGGER.info("No active DCRs to download from IPAC..");
                }
            } else {
                LOGGER.info("Download IPAC-DCR Values batch still running..");
                return false;
            }
        } catch (WMSDaoException ex) {
            LOGGER.error(ex);
            successRun = false;
            isRunning = false;
            ServiceException serviceException = new ServiceException(ex);
            this.failedJobRun(systemDate, serviceException);
            throw new ServiceException(ex);
        }
        if (successRun) {
            this.successfulJobRun(systemDate);
            isRunning = false;
        }
        return successRun;
    }

    private List<PPAMDS> assembleAllMDSTransactions(
            String customerCenter,
            Date transactionDate) throws WMSDaoException {
        
        List<PPAMDS> grandList = new ArrayList<PPAMDS>();
        Date from = WMSDateUtil.startOfTheDay(transactionDate);
        Date to = DateUtils.addDays(from, 1);

        Company[] mdsCompanies = new Company[]{Company.SLFPI, Company.SLGFI, Company.SLOCPI};
        for (Company company : mdsCompanies) {
            // Added for DCR redesign - Needed for DCR Redesign
            List<PPAMDS> allMdsTransactions = dcrIPACDao.getDCRIPACMDSTransaction(company.name(), customerCenter, from, to);

            if (CollectionUtils.isNotEmpty(allMdsTransactions)) {
                grandList.addAll(allMdsTransactions);
            }
        }
        return grandList;
    }

    private void failedJobRun(Date today, ServiceException e)
            throws ServiceException {
        try {
            String stackTrace = ExceptionUtil.getStackTrace(e, 1000);
            JobDataErrorLog log = new JobDataErrorLog();
            log.setDateTime(today);
            log.setErrorMessage(stackTrace);
            jobDataDao.createErrorLog(log);

            JobData jobData = new JobData();
            jobData.setId(IPAC_DCR_CONVERSION_JOB_ID);
            jobData.setLastRun(today);
            jobData.setSuccessful(false);
            jobDataDao.updateJobData(jobData);

        } catch (WMSDaoException ex1) {
            LOGGER.error(ex1);
            throw new ServiceException(ex1);
        }
    }

    private void successfulJobRun(Date today) throws ServiceException {
        try {
            JobData jobData = new JobData();
            jobData.setId(IPAC_DCR_CONVERSION_JOB_ID);
            jobData.setLastRun(today);
            jobData.setSuccessful(true);
            jobDataDao.updateJobData(jobData);
        } catch (WMSDaoException ex1) {
            LOGGER.error(ex1);
            throw new ServiceException(ex1);
        }
    }

    private void insertSessionTotalsToDCR(DCR dcr, List<DCRIpacValue> ipacValues) throws ServiceException {
        LOGGER.info("Begin inserting session totals for dcr id: " + dcr.getId());
        if (dcrIngeniumService.populateDCRSessionTotal(dcr.getId(), dcr.getDcrDate(), dcr.getCcId(), ipacValues)) {
            LOGGER.info("Done inserting session totals for dcr id: " + dcr.getId());
        } else {
            LOGGER.info("Problem upon inserting session totals for dcr id: " + dcr.getId());
        }
    }

    // Method is used for downloading IPAC values
    // Will have to test if needed for DCR Redesign
    @Override
    public boolean processIPACAndIngeniumData(DCR dcr) throws ServiceException {
        
        boolean isProcessing = true;
        try {
            int totRows = 0;
            LOGGER.info("Begin downloading IPAC values for dcr id : " + dcr.getId());
            String centerCode = dcr.getCcId();
            Date dcrDate = dcr.getDcrDate();
            Date processDate = dcrDate;
            int rowCnt = 0;

            // Added for DCR redesign
            List<DCRIpacValue> ipacValues = dcrIPACDao.getDCRIPACDailyCollectionSummary(centerCode, processDate);
            for (DCRIpacValue value : ipacValues) {
                if (CollectionUtils.isEmpty(dcrIPACDao.getDCRIPACDailyCollectionSummary(centerCode, processDate, value))) {
                    LOGGER.info("Something's wrong no value for DCRIpacValue. ");
                } else {
                    LOGGER.info("DCRIpacValue record already exist. " + value.toString());
                }
            }
            LOGGER.info("DCRIpacValue record downloaded successfully: " + rowCnt + "/" + ipacValues.size());
            totRows += rowCnt;
            rowCnt = 0;

            // Added for DCR redesign
            List<NonPostedTransaction> allNonPostedTransactions = dcrIPACDao.getDCRIPACDTR(centerCode, processDate);
            for (NonPostedTransaction value : allNonPostedTransactions) {
                if (CollectionUtils.isEmpty(dcrIPACDao.getDCRIPACDTR(centerCode, processDate, value))) {
                    LOGGER.info("Something's wrong no value for NonPostedTransaction. ");
                } else {
                    LOGGER.info("NonPostedTransaction record already exist. " + value.toString());
                }
            }
            LOGGER.info("NonPostedTransaction record downloaded successfully : " + rowCnt + "/" + allNonPostedTransactions.size());
            totRows += rowCnt;
            rowCnt = 0;

            // Added for DCR redesign
            List<GAFValue> allGafValues = dcrIPACDao.getDCRIPACGAF(centerCode, processDate);
            for (GAFValue value : allGafValues) {
                if (CollectionUtils.isEmpty(dcrIPACDao.getDCRIPACGAF(centerCode, processDate, value))) {
                    LOGGER.info("Something's wrong no value for GAFValue. ");
                } else {
                    LOGGER.info("GAFValue record already exist. " + value.toString());
                }
            }
            LOGGER.info("GAFValue record downloaded successfully: " + rowCnt + "/" + allGafValues.size());
            totRows += rowCnt;
            rowCnt = 0;

            List<PPAMDS> allMdsTransactions = assembleAllMDSTransactions(centerCode, processDate);
            for (PPAMDS value : allMdsTransactions) {
                if (CollectionUtils.isEmpty(dcrIPACDao.getDCRIPACMDSTransaction(value.getComCode(), centerCode, value.getDateRangeFrom(), value.getDateRangeTo(), value))) {
                    LOGGER.info("Something's wrong no value for PPAMDS. ");
                } else {
                    LOGGER.info("PPAMDS record already exist. " + value.toString());
                }
            }
            LOGGER.info("PPAMDS record downloaded successfully: " + rowCnt + "/" + allMdsTransactions.size());
            totRows += rowCnt;
            rowCnt = 0;
            LOGGER.info("Done downloading IPAC values.. Number of record/s : " + totRows + " for dcr id : " + dcr.getId());
            
            if ((totRows > 0)) {
                dcrIPACDao.updateDCRIPACDownloadFlag("Y", new Date(), dcr.getId());
                if (!WMSConstants.STATUS_PENDING.equalsIgnoreCase(dcr.getStatus())) {
                    insertSessionTotalsToDCR(dcr, ipacValues);
                }
            }
            totRows = 0;
        } catch (WMSDaoException e) {
            isProcessing = false;
            LOGGER.error("Error encountered in processIPACAndIngeniumData: ", e);
            throw new ServiceException(e);
        }
        return isProcessing;
    }

    @Override
    public void removeIPACAndIngeniumData(DCR dcr) throws ServiceException {
        try {
            String centerCode = dcr.getCcId();
            Date processDate = dcr.getDcrDate();

            List<DCRIpacValue> ipacValues = dcrIPACDao.getDCRIPACDailyCollectionSummary(centerCode, processDate);
            List<NonPostedTransaction> allNonPostedTransactions = dcrIPACDao.getDCRIPACDTR(centerCode, processDate);
            List<GAFValue> allGafValues = dcrIPACDao.getDCRIPACGAF(centerCode, processDate);
            List<PPAMDS> allMdsTransactions = dcrIPACDao.getDCRIPACMDSTransaction(centerCode, processDate);

            if ((ipacValues.size() + allNonPostedTransactions.size() + allGafValues.size() + allMdsTransactions.size()) > 0) {
                LOGGER.info("Clearing IPAC values and session totals for center code: " + centerCode + " and dcr date: " + processDate);
            }
        } catch (WMSDaoException e) {
            LOGGER.error("Error encountered in removeIPACAndIngeniumData: ", e);
            throw new ServiceException(e);
        }
    }

    private boolean checkIPACValue(List<DCRIpacValue> values, final DCRIpacValue valueToCheck) {
        List<DCRIpacValue> returnValues = WMSCollectionUtils
                .findMatchedElements(values, new Predicate() {

            @Override
            public boolean evaluate(Object object) {
                DCRIpacValue value = (DCRIpacValue) object;
                //(product_code,pay_code,pay_sub_code,pay_sub_curr,amount,acf2id,center_code,process_date)

                return StringUtils.equalsIgnoreCase(
                        valueToCheck.getProdCode(), value.getProdCode())
                        && StringUtils.equalsIgnoreCase(
                        valueToCheck.getPayCode(),
                        value.getPayCode())
                        && StringUtils.equalsIgnoreCase(
                        valueToCheck.getPaySubCode(),
                        value.getPaySubCode())
                        && StringUtils.equalsIgnoreCase(
                        valueToCheck.getPaySubCurr(),
                        value.getPaySubCurr())
                        && (valueToCheck.getAmount() == value
                        .getAmount())
                        && StringUtils.equalsIgnoreCase(
                        valueToCheck.getAcf2id(),
                        value.getAcf2id());
            }
        });

        return returnValues.size() > 0;
    }

    private boolean checkNonPostedTransactionValue(List<NonPostedTransaction> values, final NonPostedTransaction valueToCheck) {
        List<NonPostedTransaction> returnValues = WMSCollectionUtils
                .findMatchedElements(values, new Predicate() {

            @Override
            public boolean evaluate(Object object) {
                NonPostedTransaction value = (NonPostedTransaction) object;
                //(company_code,product_code,transaction_type,payment_currency,amount,userid,center_code,process_date)

                return StringUtils.equalsIgnoreCase(
                        valueToCheck.getCompanyCode(),
                        value.getCompanyCode())
                        && StringUtils.equalsIgnoreCase(
                        valueToCheck.getProductCode(),
                        value.getProductCode())
                        && StringUtils.equalsIgnoreCase(
                        valueToCheck.getTransactionType(),
                        value.getTransactionType())
                        && StringUtils.equalsIgnoreCase(
                        valueToCheck.getPaymentCurrencyStr(),
                        value.getPaymentCurrencyStr())
                        && (valueToCheck.getPaymentPostStatus() == value
                        .getPaymentPostStatus())
                        && (valueToCheck.getAmount() == value
                        .getAmount())
                        && StringUtils.equalsIgnoreCase(
                        valueToCheck.getUserId(),
                        value.getUserId());
            }
        });

        return returnValues.size() > 0;
    }

    private boolean checkGAFValue(List<GAFValue> values, final GAFValue valueToCheck) {
        List<GAFValue> returnValues = WMSCollectionUtils
                .findMatchedElements(values, new Predicate() {

            @Override
            public boolean evaluate(Object object) {
                GAFValue value = (GAFValue) object;
                //(company_name,product_code,payment_currency,payment_post_status,pay_type,total_amount,userid,cash_amount,card_amount,check_amount,ncash_amount,center_code,process_date)

                return StringUtils.equalsIgnoreCase(
                        valueToCheck.getCompanyName(),
                        value.getCompanyName())
                        && StringUtils.equalsIgnoreCase(
                        valueToCheck.getProdCode(),
                        value.getProdCode())
                        && StringUtils.equalsIgnoreCase(
                        valueToCheck.getPaymentCurrency(),
                        value.getPaymentCurrency())
                        && StringUtils.equalsIgnoreCase(
                        valueToCheck.getPaymentPostStatus(),
                        value.getPaymentPostStatus())
                        && StringUtils.equalsIgnoreCase(
                        valueToCheck.getPayType(),
                        value.getPayType())
                        && (valueToCheck.getTotalAmount().doubleValue() == value
                        .getTotalAmount().doubleValue())
                        && StringUtils.equalsIgnoreCase(
                        valueToCheck.getUserId(),
                        value.getUserId())
                        && (valueToCheck.getCashAmount() == value
                        .getCashAmount())
                        && (valueToCheck.getCardAmount() == value
                        .getCardAmount())
                        && (valueToCheck.getCheckAmount() == value
                        .getCheckAmount())
                        && (valueToCheck.getNcashAmount() == value
                        .getNcashAmount());
            }
        });

        return returnValues.size() > 0;
    }

    // Change according to Healthcheck
    // *Problem as == is inconsistent in checking floating points
    // *Change was to comment method as not being used
//    private boolean checkPPAMDSValue(List<PPAMDS> values, final PPAMDS valueToCheck) {
//        List<PPAMDS> returnValues = WMSCollectionUtils
//                .findMatchedElements(values, new Predicate() {
//
//            @Override
//            public boolean evaluate(Object object) {
//                PPAMDS value = (PPAMDS) object;
//                //(pay_sub_desc,sales_slip_number,document_id,com_code,company,application_serial_number,card_type,currency,customer_center,policy_plan_client_number,prod_code,payment_currency,site_code,transaction_date,userid,product,account_number,approval_date,approval_number,card_amount)
//
//                return StringUtils.equalsIgnoreCase(
//                        valueToCheck.getPaySubDesc(),
//                        value.getPaySubDesc())
//                        && StringUtils.equalsIgnoreCase(
//                        valueToCheck.getSalesSlipNumber(),
//                        value.getSalesSlipNumber())
//                        && StringUtils.equalsIgnoreCase(
//                        valueToCheck.getDocumentId(),
//                        value.getDocumentId())
//                        && StringUtils.equalsIgnoreCase(
//                        valueToCheck.getComCode(),
//                        value.getComCode())
//                        && StringUtils.equalsIgnoreCase(
//                        valueToCheck.getCompany(),
//                        value.getCompany())
//                        && StringUtils.equalsIgnoreCase(valueToCheck
//                        .getApplicationSerialNumber(), value
//                        .getApplicationSerialNumber())
//                        && StringUtils.equalsIgnoreCase(
//                        valueToCheck.getCardType(),
//                        value.getCardType())
//                        && StringUtils.equalsIgnoreCase(
//                        valueToCheck.getCurrency(),
//                        value.getCurrency())
//                        && StringUtils.equalsIgnoreCase(
//                        valueToCheck.getCustomerCenter(),
//                        value.getCustomerCenter())
//                        && StringUtils.equalsIgnoreCase(valueToCheck
//                        .getPolicyPlanClientNumber(), value
//                        .getPolicyPlanClientNumber())
//                        && StringUtils.equalsIgnoreCase(
//                        valueToCheck.getProdCode(),
//                        value.getProdCode())
//                        && StringUtils.equalsIgnoreCase(
//                        valueToCheck.getPymtCurrency(),
//                        value.getPymtCurrency())
//                        && StringUtils.equalsIgnoreCase(
//                        valueToCheck.getSiteCode(),
//                        value.getSiteCode())
//                        && DateUtils.isSameDay(
//                        valueToCheck.getTrxnDateProduct(),
//                        value.getTrxnDateProduct())
//                        && StringUtils.equalsIgnoreCase(
//                        valueToCheck.getUserId(),
//                        value.getUserId())
//                        && StringUtils.equalsIgnoreCase(
//                        valueToCheck.getProduct(),
//                        value.getProduct())
//                        && StringUtils.equalsIgnoreCase(
//                        valueToCheck.getAccountNumber(),
//                        value.getAccountNumber())
//                        && DateUtils.isSameDay(
//                        valueToCheck.getApprovalDate(),
//                        value.getApprovalDate())
//                        && StringUtils.equalsIgnoreCase(
//                        valueToCheck.getApprovalNumber(),
//                        value.getApprovalNumber())
//                        && new Double(valueToCheck
//                        .getCardAmount()).doubleValue() == new Double(value
//                        .getCardAmount()).doubleValue()
//                        && DateUtils.isSameDay(
//                        valueToCheck.getDateRangeFrom(),
//                        value.getDateRangeFrom())
//                        && DateUtils.isSameDay(
//                        valueToCheck.getDateRangeTo(),
//                        value.getDateRangeTo());
//            }
//        });
//
//        return returnValues.size() > 0;
//    }
	
}
