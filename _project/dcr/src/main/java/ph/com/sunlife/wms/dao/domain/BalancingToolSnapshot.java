package ph.com.sunlife.wms.dao.domain;

import java.util.Date;

public class BalancingToolSnapshot {

	private String acf2id;
	
	private Long dcrCashierId;
	
	private Date dateConfirmed;
	
	private Date dateReconfirmed;
	
	private String day1DocId;
	
	private String day2DocId;
	
	private Long companyId;
	
	public Long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}

	public String getAcf2id() {
		return acf2id;
	}

	public void setAcf2id(String acf2id) {
		this.acf2id = acf2id;
	}

	public Long getDcrCashierId() {
		return dcrCashierId;
	}

	public void setDcrCashierId(Long dcrCashierId) {
		this.dcrCashierId = dcrCashierId;
	}

	public Date getDateConfirmed() {
		return dateConfirmed;
	}

	public void setDateConfirmed(Date dateConfirmed) {
		this.dateConfirmed = dateConfirmed;
	}

	public Date getDateReconfirmed() {
		return dateReconfirmed;
	}

	public void setDateReconfirmed(Date dateReconfirmed) {
		this.dateReconfirmed = dateReconfirmed;
	}

	public String getDay1DocId() {
		return day1DocId;
	}

	public void setDay1DocId(String day1DocId) {
		this.day1DocId = day1DocId;
	}

	public String getDay2DocId() {
		return day2DocId;
	}

	public void setDay2DocId(String day2DocId) {
		this.day2DocId = day2DocId;
	}
	
}
