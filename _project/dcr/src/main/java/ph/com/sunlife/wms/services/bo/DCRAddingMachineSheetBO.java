package ph.com.sunlife.wms.services.bo;

public class DCRAddingMachineSheetBO {

	private Long id;

	private DCRAddingMachineBO dcrAddingMachine;

	private CompanyBO company;

	private double totalAmount;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public DCRAddingMachineBO getDcrAddingMachine() {
		return dcrAddingMachine;
	}

	public void setDcrAddingMachine(DCRAddingMachineBO dcrAddingMachine) {
		this.dcrAddingMachine = dcrAddingMachine;
	}
	
	public void setDcrAddingMachine(Long dcrAddingMachineId) {
		DCRAddingMachineBO dcrAddingMachine = new DCRAddingMachineBO();
		dcrAddingMachine.setId(dcrAddingMachineId);
		this.dcrAddingMachine = dcrAddingMachine;
	}

	public CompanyBO getCompany() {
		return company;
	}

	public void setCompany(CompanyBO company) {
		this.company = company;
	}
	
	public void setCompany(Long companyId) {
		this.setCompany(CompanyBO.getCompany(companyId));
	}

	public double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DCRAddingMachineSheetBO other = (DCRAddingMachineSheetBO) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
}
