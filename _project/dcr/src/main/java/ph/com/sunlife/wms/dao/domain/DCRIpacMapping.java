package ph.com.sunlife.wms.dao.domain;

public class DCRIpacMapping extends Entity {

	private String group;
	
	private String subGroup;
	
	private String subGroupDescription;

	public String getGroup() {
		return group;
	}

	public void setGroup(String group) {
		this.group = group;
	}

	public String getSubGroup() {
		return subGroup;
	}

	public void setSubGroup(String subGroup) {
		this.subGroup = subGroup;
	}

	public String getSubGroupDescription() {
		return subGroupDescription;
	}

	public void setSubGroupDescription(String subGroupDescription) {
		this.subGroupDescription = subGroupDescription;
	}
	
}
