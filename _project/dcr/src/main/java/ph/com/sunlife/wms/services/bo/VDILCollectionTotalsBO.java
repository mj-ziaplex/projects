package ph.com.sunlife.wms.services.bo;

public class VDILCollectionTotalsBO {

	private DCRVDILBO parent;

	private double cashPesoTradVul;

	private double cashPesoPreneed;

	private double cashPesoPreneedGroup;

	private double cashPesoGroupLife;

	private double cashPesoSlamc;

	private double cashPesoCob;
	
	private double cashPesoSlgfi;

	private double checkLocalPesoTradVul;

	private double checkLocalPesoPreneed;

	private double checkLocalPesoPreneedGroup;

	private double checkLocalPesoGroupLife;

	private double checkLocalPesoSlamc;

	private double checkLocalPesoCob;
	
	private double checkLocalPesoSlgfi;
	
	private double checkRegionalPesoTradVul;

	private double checkRegionalPesoPreneed;

	private double checkRegionalPesoPreneedGroup;

	private double checkRegionalPesoGroupLife;

	private double checkRegionalPesoSlamc;

	private double checkRegionalPesoCob;
	
	private double checkRegionalPesoSlgfi;
	
	private double checkOnUsPesoTradVul;

	private double checkOnUsPesoPreneed;

	private double checkOnUsPesoPreneedGroup;

	private double checkOnUsPesoGroupLife;

	private double checkOnUsPesoSlamc;

	private double checkOnUsPesoCob;
	
	private double checkOnUsPesoSlgfi;
	
	private double checkOutOfTownPesoTradVul;

	private double checkOutOfTownPesoPreneed;

	private double checkOutOfTownPesoPreneedGroup;

	private double checkOutOfTownPesoGroupLife;

	private double checkOutOfTownPesoSlamc;

	private double checkOutOfTownPesoCob;
	
	private double checkOutOfTownPesoSlgfi;
	
	private double cashDollarTradVul;
	
	private double cashDollarCob;
	
	private double cashDollarSlamc;
	
	private double cashDollarSlgfi;
	
	private double checkDollarTradVul;
	
	private double checkDollarCob;
	
	private double checkDollarSlamc;
	
	private double checkDollarSlgfi;
	
	private double cardTradVul;
	
	private int cardTradVulSalesSlipNum;
	
	private double cardGroupLife;
	
	private int cardGroupLifeSalesSlipNum;
	
	private double cardPreneed;
	
	private int cardPreneedSalesSlipNum;
	
	private double cardDollarCollection;
	
	private int cardDollarCollectionSalesSlipNum;
	
	// TODO: Josh, map this to the PDF
	private double cardPreneedGroup;
	
	private int cardPreneedGroupSalesSlipNum;
	
	private double cardSlgfi;
	
	private int cardSlgfiSalesSlipNum;
	
	private double cashEpsBpi;

	public DCRVDILBO getParent() {
		return parent;
	}

	public void setParent(DCRVDILBO parent) {
		this.parent = parent;
	}

	public double getCashPesoTradVul() {
		return cashPesoTradVul;
	}

	public void setCashPesoTradVul(double cashPesoTradVul) {
		this.cashPesoTradVul = cashPesoTradVul;
	}

	public double getCashPesoPreneed() {
		return cashPesoPreneed;
	}

	public void setCashPesoPreneed(double cashPesoPreneed) {
		this.cashPesoPreneed = cashPesoPreneed;
	}

	public double getCashPesoPreneedGroup() {
		return cashPesoPreneedGroup;
	}

	public void setCashPesoPreneedGroup(double cashPesoPreneedGroup) {
		this.cashPesoPreneedGroup = cashPesoPreneedGroup;
	}

	public double getCashPesoGroupLife() {
		return cashPesoGroupLife;
	}

	public void setCashPesoGroupLife(double cashPesoGroupLife) {
		this.cashPesoGroupLife = cashPesoGroupLife;
	}

	public double getCashPesoSlamc() {
		return cashPesoSlamc;
	}

	public void setCashPesoSlamc(double cashPesoSlamc) {
		this.cashPesoSlamc = cashPesoSlamc;
	}

	public double getCashPesoCob() {
		return cashPesoCob;
	}

	public void setCashPesoCob(double cashPesoCob) {
		this.cashPesoCob = cashPesoCob;
	}

	public double getCheckLocalPesoTradVul() {
		return checkLocalPesoTradVul;
	}

	public void setCheckLocalPesoTradVul(double checkLocalPesoTradVul) {
		this.checkLocalPesoTradVul = checkLocalPesoTradVul;
	}

	public double getCheckLocalPesoPreneed() {
		return checkLocalPesoPreneed;
	}

	public void setCheckLocalPesoPreneed(double checkLocalPesoPreneed) {
		this.checkLocalPesoPreneed = checkLocalPesoPreneed;
	}

	public double getCheckLocalPesoPreneedGroup() {
		return checkLocalPesoPreneedGroup;
	}

	public void setCheckLocalPesoPreneedGroup(double checkLocalPesoPreneedGroup) {
		this.checkLocalPesoPreneedGroup = checkLocalPesoPreneedGroup;
	}

	public double getCheckLocalPesoGroupLife() {
		return checkLocalPesoGroupLife;
	}

	public void setCheckLocalPesoGroupLife(double checkLocalPesoGroupLife) {
		this.checkLocalPesoGroupLife = checkLocalPesoGroupLife;
	}

	public double getCheckLocalPesoSlamc() {
		return checkLocalPesoSlamc;
	}

	public void setCheckLocalPesoSlamc(double checkLocalPesoSlamc) {
		this.checkLocalPesoSlamc = checkLocalPesoSlamc;
	}

	public double getCheckLocalPesoCob() {
		return checkLocalPesoCob;
	}

	public void setCheckLocalPesoCob(double checkLocalPesoCob) {
		this.checkLocalPesoCob = checkLocalPesoCob;
	}

	public double getCheckRegionalPesoTradVul() {
		return checkRegionalPesoTradVul;
	}

	public void setCheckRegionalPesoTradVul(double checkRegionalPesoTradVul) {
		this.checkRegionalPesoTradVul = checkRegionalPesoTradVul;
	}

	public double getCheckRegionalPesoPreneed() {
		return checkRegionalPesoPreneed;
	}

	public void setCheckRegionalPesoPreneed(double checkRegionalPesoPreneed) {
		this.checkRegionalPesoPreneed = checkRegionalPesoPreneed;
	}

	public double getCheckRegionalPesoPreneedGroup() {
		return checkRegionalPesoPreneedGroup;
	}

	public void setCheckRegionalPesoPreneedGroup(
			double checkRegionalPesoPreneedGroup) {
		this.checkRegionalPesoPreneedGroup = checkRegionalPesoPreneedGroup;
	}

	public double getCheckRegionalPesoGroupLife() {
		return checkRegionalPesoGroupLife;
	}

	public void setCheckRegionalPesoGroupLife(double checkRegionalPesoGroupLife) {
		this.checkRegionalPesoGroupLife = checkRegionalPesoGroupLife;
	}

	public double getCheckRegionalPesoSlamc() {
		return checkRegionalPesoSlamc;
	}

	public void setCheckRegionalPesoSlamc(double checkRegionalPesoSlamc) {
		this.checkRegionalPesoSlamc = checkRegionalPesoSlamc;
	}

	public double getCheckRegionalPesoCob() {
		return checkRegionalPesoCob;
	}

	public void setCheckRegionalPesoCob(double checkRegionalPesoCob) {
		this.checkRegionalPesoCob = checkRegionalPesoCob;
	}

	public double getCheckOnUsPesoTradVul() {
		return checkOnUsPesoTradVul;
	}

	public void setCheckOnUsPesoTradVul(double checkOnUsPesoTradVul) {
		this.checkOnUsPesoTradVul = checkOnUsPesoTradVul;
	}

	public double getCheckOnUsPesoPreneed() {
		return checkOnUsPesoPreneed;
	}

	public void setCheckOnUsPesoPreneed(double checkOnUsPesoPreneed) {
		this.checkOnUsPesoPreneed = checkOnUsPesoPreneed;
	}

	public double getCheckOnUsPesoPreneedGroup() {
		return checkOnUsPesoPreneedGroup;
	}

	public void setCheckOnUsPesoPreneedGroup(double checkOnUsPesoPreneedGroup) {
		this.checkOnUsPesoPreneedGroup = checkOnUsPesoPreneedGroup;
	}

	public double getCheckOnUsPesoGroupLife() {
		return checkOnUsPesoGroupLife;
	}

	public void setCheckOnUsPesoGroupLife(double checkOnUsPesoGroupLife) {
		this.checkOnUsPesoGroupLife = checkOnUsPesoGroupLife;
	}

	public double getCheckOnUsPesoSlamc() {
		return checkOnUsPesoSlamc;
	}

	public void setCheckOnUsPesoSlamc(double checkOnUsPesoSlamc) {
		this.checkOnUsPesoSlamc = checkOnUsPesoSlamc;
	}

	public double getCheckOnUsPesoCob() {
		return checkOnUsPesoCob;
	}

	public void setCheckOnUsPesoCob(double checkOnUsPesoCob) {
		this.checkOnUsPesoCob = checkOnUsPesoCob;
	}

	public double getCheckOutOfTownPesoTradVul() {
		return checkOutOfTownPesoTradVul;
	}

	public void setCheckOutOfTownPesoTradVul(double checkOutOfTownPesoTradVul) {
		this.checkOutOfTownPesoTradVul = checkOutOfTownPesoTradVul;
	}

	public double getCheckOutOfTownPesoPreneed() {
		return checkOutOfTownPesoPreneed;
	}

	public void setCheckOutOfTownPesoPreneed(double checkOutOfTownPesoPreneed) {
		this.checkOutOfTownPesoPreneed = checkOutOfTownPesoPreneed;
	}

	public double getCheckOutOfTownPesoPreneedGroup() {
		return checkOutOfTownPesoPreneedGroup;
	}

	public void setCheckOutOfTownPesoPreneedGroup(
			double checkOutOfTownPesoPreneedGroup) {
		this.checkOutOfTownPesoPreneedGroup = checkOutOfTownPesoPreneedGroup;
	}

	public double getCheckOutOfTownPesoGroupLife() {
		return checkOutOfTownPesoGroupLife;
	}

	public void setCheckOutOfTownPesoGroupLife(double checkOutOfTownPesoGroupLife) {
		this.checkOutOfTownPesoGroupLife = checkOutOfTownPesoGroupLife;
	}

	public double getCheckOutOfTownPesoSlamc() {
		return checkOutOfTownPesoSlamc;
	}

	public void setCheckOutOfTownPesoSlamc(double checkOutOfTownPesoSlamc) {
		this.checkOutOfTownPesoSlamc = checkOutOfTownPesoSlamc;
	}

	public double getCheckOutOfTownPesoCob() {
		return checkOutOfTownPesoCob;
	}

	public void setCheckOutOfTownPesoCob(double checkOutOfTownPesoCob) {
		this.checkOutOfTownPesoCob = checkOutOfTownPesoCob;
	}

	public double getCashDollarTradVul() {
		return cashDollarTradVul;
	}

	public void setCashDollarTradVul(double cashDollarTradVul) {
		this.cashDollarTradVul = cashDollarTradVul;
	}

	public double getCashDollarCob() {
		return cashDollarCob;
	}

	public void setCashDollarCob(double cashDollarCob) {
		this.cashDollarCob = cashDollarCob;
	}

	public double getCashDollarSlamc() {
		return cashDollarSlamc;
	}

	public void setCashDollarSlamc(double cashDollarSlamc) {
		this.cashDollarSlamc = cashDollarSlamc;
	}

	public double getCheckDollarTradVul() {
		return checkDollarTradVul;
	}

	public void setCheckDollarTradVul(double checkDollarTradVul) {
		this.checkDollarTradVul = checkDollarTradVul;
	}

	public double getCheckDollarCob() {
		return checkDollarCob;
	}

	public void setCheckDollarCob(double checkDollarCob) {
		this.checkDollarCob = checkDollarCob;
	}

	public double getCheckDollarSlamc() {
		return checkDollarSlamc;
	}

	public void setCheckDollarSlamc(double checkDollarSlamc) {
		this.checkDollarSlamc = checkDollarSlamc;
	}

	public double getCardTradVul() {
		return cardTradVul;
	}

	public void setCardTradVul(double cardTradVul) {
		this.cardTradVul = cardTradVul;
	}

	public int getCardTradVulSalesSlipNum() {
		return cardTradVulSalesSlipNum;
	}

	public void setCardTradVulSalesSlipNum(int cardTradVulSalesSlipNum) {
		this.cardTradVulSalesSlipNum = cardTradVulSalesSlipNum;
	}

	public double getCardGroupLife() {
		return cardGroupLife;
	}

	public void setCardGroupLife(double cardGroupLife) {
		this.cardGroupLife = cardGroupLife;
	}

	public int getCardGroupLifeSalesSlipNum() {
		return cardGroupLifeSalesSlipNum;
	}

	public void setCardGroupLifeSalesSlipNum(int cardGroupLifeSalesSlipNum) {
		this.cardGroupLifeSalesSlipNum = cardGroupLifeSalesSlipNum;
	}

	public double getCardPreneed() {
		return cardPreneed;
	}

	public void setCardPreneed(double cardPreneed) {
		this.cardPreneed = cardPreneed;
	}

	public int getCardPreneedSalesSlipNum() {
		return cardPreneedSalesSlipNum;
	}

	public void setCardPreneedSalesSlipNum(int cardPreneedSalesSlipNum) {
		this.cardPreneedSalesSlipNum = cardPreneedSalesSlipNum;
	}

	public double getCardDollarCollection() {
		return cardDollarCollection;
	}

	public void setCardDollarCollection(double cardDollarCollection) {
		this.cardDollarCollection = cardDollarCollection;
	}

	public int getCardDollarCollectionSalesSlipNum() {
		return cardDollarCollectionSalesSlipNum;
	}

	public void setCardDollarCollectionSalesSlipNum(
			int cardDollarCollectionSalesSlipNum) {
		this.cardDollarCollectionSalesSlipNum = cardDollarCollectionSalesSlipNum;
	}

	public double getCardPreneedGroup() {
		return cardPreneedGroup;
	}

	public void setCardPreneedGroup(double cardPreneedGroup) {
		this.cardPreneedGroup = cardPreneedGroup;
	}

	public int getCardPreneedGroupSalesSlipNum() {
		return cardPreneedGroupSalesSlipNum;
	}

	public void setCardPreneedGroupSalesSlipNum(int cardPreneedGroupSalesSlipNum) {
		this.cardPreneedGroupSalesSlipNum = cardPreneedGroupSalesSlipNum;
	}

	public double getCashPesoSlgfi() {
		return cashPesoSlgfi;
	}

	public void setCashPesoSlgfi(double cashPesoSlgfi) {
		this.cashPesoSlgfi = cashPesoSlgfi;
	}

	public double getCheckLocalPesoSlgfi() {
		return checkLocalPesoSlgfi;
	}

	public void setCheckLocalPesoSlgfi(double checkLocalPesoSlgfi) {
		this.checkLocalPesoSlgfi = checkLocalPesoSlgfi;
	}

	public double getCheckRegionalPesoSlgfi() {
		return checkRegionalPesoSlgfi;
	}

	public void setCheckRegionalPesoSlgfi(double checkRegionalPesoSlgfi) {
		this.checkRegionalPesoSlgfi = checkRegionalPesoSlgfi;
	}

	public double getCheckOnUsPesoSlgfi() {
		return checkOnUsPesoSlgfi;
	}

	public void setCheckOnUsPesoSlgfi(double checkOnUsPesoSlgfi) {
		this.checkOnUsPesoSlgfi = checkOnUsPesoSlgfi;
	}

	public double getCheckOutOfTownPesoSlgfi() {
		return checkOutOfTownPesoSlgfi;
	}

	public void setCheckOutOfTownPesoSlgfi(double checkOutOfTownPesoSlgfi) {
		this.checkOutOfTownPesoSlgfi = checkOutOfTownPesoSlgfi;
	}

	public double getCashDollarSlgfi() {
		return cashDollarSlgfi;
	}

	public void setCashDollarSlgfi(double cashDollarSlgfi) {
		this.cashDollarSlgfi = cashDollarSlgfi;
	}

	public double getCheckDollarSlgfi() {
		return checkDollarSlgfi;
	}

	public void setCheckDollarSlgfi(double checkDollarSlgfi) {
		this.checkDollarSlgfi = checkDollarSlgfi;
	}

	public double getCardSlgfi() {
		return cardSlgfi;
	}

	public void setCardSlgfi(double cardSlgfi) {
		this.cardSlgfi = cardSlgfi;
	}

	public int getCardSlgfiSalesSlipNum() {
		return cardSlgfiSalesSlipNum;
	}

	public void setCardSlgfiSalesSlipNum(int cardSlgfiSalesSlipNum) {
		this.cardSlgfiSalesSlipNum = cardSlgfiSalesSlipNum;
	}

	public double getCashEpsBpi() {
		return cashEpsBpi;
	}

	public void setCashEpsBpi(double cashEpsBpi) {
		this.cashEpsBpi = cashEpsBpi;
	}

}
