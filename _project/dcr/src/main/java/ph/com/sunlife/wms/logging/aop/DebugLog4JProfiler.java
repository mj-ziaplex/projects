///*
// * $Id: DebugLog4JProfiler.java,v 1.1 2012/10/16 08:17:59 PV70 Exp $
// *
// * Copyright (c) 2010 HEB
// * All rights reserved.
// *
// * This software is the confidential and proprietary information
// * of HEB.
// * 
// * COMMENTED AS NOT BEING USED
// */
//package ph.com.sunlife.wms.logging.aop;
//
//public class DebugLog4JProfiler extends AbstractLog4JProfiler {
//
//	@Override
//	protected void log(String message) {
//		logger.debug(message);
//	}
//
//	@Override
//	protected void log(String message, Throwable t) {
//		logger.debug(message, t);
//	}
//
//
//}
