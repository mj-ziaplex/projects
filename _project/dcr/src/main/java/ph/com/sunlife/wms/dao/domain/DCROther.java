package ph.com.sunlife.wms.dao.domain;

public class DCROther extends Entity {

	private Long dcrBalancingToolProductId;
	
	private double cashCounterAmt;

	private double cashNonCounter;
	
	private double chequeLocal;

	private double chequeOnUs;

	private double chequeRegional;

	private double chequeNonCounter;
	
	private double totalUsCheckInManila;

	private double totalUsCheckOutPh;

	private double totalDollarCheque;

	private double totalBankOTCCheckPayment;
	
	private String userId;
	
	public double getChequeLocal() {
		return chequeLocal;
	}

	public void setChequeLocal(double chequeLocal) {
		this.chequeLocal = chequeLocal;
	}

	public double getTotalUsCheckInManila() {
		return totalUsCheckInManila;
	}

	public void setTotalUsCheckInManila(double totalUsCheckInManila) {
		this.totalUsCheckInManila = totalUsCheckInManila;
	}

	public double getTotalUsCheckOutPh() {
		return totalUsCheckOutPh;
	}

	public void setTotalUsCheckOutPh(double totalUsCheckOutPh) {
		this.totalUsCheckOutPh = totalUsCheckOutPh;
	}

	public double getTotalDollarCheque() {
		return totalDollarCheque;
	}

	public void setTotalDollarCheque(double totalDollarCheque) {
		this.totalDollarCheque = totalDollarCheque;
	}

	public double getTotalBankOTCCheckPayment() {
		return totalBankOTCCheckPayment;
	}

	public void setTotalBankOTCCheckPayment(double totalBankOTCCheckPayment) {
		this.totalBankOTCCheckPayment = totalBankOTCCheckPayment;
	}

	public Long getDcrBalancingToolProductId() {
		return dcrBalancingToolProductId;
	}

	public void setDcrBalancingToolProductId(Long dcrBalancingToolProductId) {
		this.dcrBalancingToolProductId = dcrBalancingToolProductId;
	}

	public double getCashCounterAmt() {
		return cashCounterAmt;
	}

	public void setCashCounterAmt(double cashCounterAmt) {
		this.cashCounterAmt = cashCounterAmt;
	}

	public double getCashNonCounter() {
		return cashNonCounter;
	}

	public void setCashNonCounter(double cashNonCounter) {
		this.cashNonCounter = cashNonCounter;
	}

	public double getChequeOnUs() {
		return chequeOnUs;
	}

	public void setChequeOnUs(double chequeOnUs) {
		this.chequeOnUs = chequeOnUs;
	}

	public double getChequeRegional() {
		return chequeRegional;
	}

	public void setChequeRegional(double chequeRegional) {
		this.chequeRegional = chequeRegional;
	}

	public double getChequeNonCounter() {
		return chequeNonCounter;
	}

	public void setChequeNonCounter(double chequeNonCounter) {
		this.chequeNonCounter = chequeNonCounter;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

}
