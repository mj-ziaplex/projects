package ph.com.sunlife.wms.dao.domain;

import java.util.Date;

import static ph.com.sunlife.wms.util.ParameterMaskUtil.toMaskedString;

public class PPAMDS extends Entity {
	
	private String documentId;
	private String comCode;
	private String company;
	private String siteCode;
	private String customerCenter;
	private Date trxnDateProduct;
	private String pymtCurrency;
	private String currency;
	private String cardType;
	private String paySubDesc;
	private String policyPlanClientNumber;
	private String applicationSerialNumber;
	private String prodCode;
	private String userId;
	private Date dateRangeFrom;
	private Date dateRangeTo;
	private String product;
	private String salesSlipNumber;
	private String accountNumber;
	private String approvalNumber;
	private Date approvalDate;
	private String cardAmount;
		
	public String getSalesSlipNumber() {
		return salesSlipNumber;
	}
	public void setSalesSlipNumber(String salesSlipNumber) {
		this.salesSlipNumber = salesSlipNumber;
	}
	public String getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = toMaskedString(accountNumber);
	}
	public String getApprovalNumber() {
		return approvalNumber;
	}
	public void setApprovalNumber(String approvalNumber) {
		this.approvalNumber = approvalNumber;
	}
	public Date getApprovalDate() {
		return approvalDate;
	}
	public void setApprovalDate(Date approvalDate) {
		this.approvalDate = approvalDate;
	}
	public String getCardAmount() {
		return cardAmount;
	}
	public void setCardAmount(String cardAmount) {
		this.cardAmount = cardAmount;
	}
	public String getProduct() {
		return product;
	}
	public void setProduct(String product) {
		this.product = product;
	}
	public String getDocumentId() {
		return documentId;
	}
	public void setDocumentId(String documentId) {
		this.documentId = documentId;
	}
	public String getComCode() {
		return comCode;
	}
	public void setComCode(String comCode) {
		this.comCode = comCode;
	}
	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}
	public String getSiteCode() {
		return siteCode;
	}
	public void setSiteCode(String siteCode) {
		this.siteCode = siteCode;
	}
	public String getCustomerCenter() {
		return customerCenter;
	}
	public void setCustomerCenter(String customerCenter) {
		this.customerCenter = customerCenter;
	}
	public Date getTrxnDateProduct() {
		return trxnDateProduct;
	}
	public void setTrxnDateProduct(Date trxnDateProduct) {
		this.trxnDateProduct = trxnDateProduct;
	}
	public String getPymtCurrency() {
		return pymtCurrency;
	}
	public void setPymtCurrency(String pymtCurrency) {
		this.pymtCurrency = pymtCurrency;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public String getCardType() {
		return cardType;
	}
	public void setCardType(String cardType) {
		this.cardType = cardType;
	}
	public String getPaySubDesc() {
		return paySubDesc;
	}
	public void setPaySubDesc(String paySubDesc) {
		this.paySubDesc = paySubDesc;
	}
	public String getPolicyPlanClientNumber() {
		return policyPlanClientNumber;
	}
	public void setPolicyPlanClientNumber(String policyPlanClientNumber) {
		this.policyPlanClientNumber = policyPlanClientNumber;
	}
	public String getApplicationSerialNumber() {
		return applicationSerialNumber;
	}
	public void setApplicationSerialNumber(String applicationSerialNumber) {
		this.applicationSerialNumber = applicationSerialNumber;
	}
	public String getProdCode() {
		return prodCode;
	}
	public void setProdCode(String prodCode) {
		this.prodCode = prodCode;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public Date getDateRangeFrom() {
		return dateRangeFrom;
	}
	public void setDateRangeFrom(Date dateRangeFrom) {
		this.dateRangeFrom = dateRangeFrom;
	}
	public Date getDateRangeTo() {
		return dateRangeTo;
	}
	public void setDateRangeTo(Date dateRangeTo) {
		this.dateRangeTo = dateRangeTo;
	}
	@Override
	public String toString() {
		return "PPAMDS [documentId=" + documentId + ", comCode=" + comCode
				+ ", company=" + company + ", siteCode=" + siteCode
				+ ", customerCenter=" + customerCenter + ", trxnDateProduct="
				+ trxnDateProduct + ", pymtCurrency=" + pymtCurrency
				+ ", currency=" + currency + ", cardType=" + cardType
				+ ", paySubDesc=" + paySubDesc + ", policyPlanClientNumber="
				+ policyPlanClientNumber + ", applicationSerialNumber="
				+ applicationSerialNumber + ", prodCode=" + prodCode
				+ ", userId=" + userId + ", dateRangeFrom=" + dateRangeFrom
				+ ", dateRangeTo=" + dateRangeTo + ", product=" + product
				+ ", salesSlipNumber=" + salesSlipNumber + ", accountNumber="
				+ accountNumber + ", approvalNumber=" + approvalNumber
				+ ", approvalDate=" + approvalDate + ", cardAmount="
				+ cardAmount + "]";
	}

}
