package ph.com.sunlife.wms.web.controller.form;

import javax.servlet.http.HttpSession;

import org.springframework.validation.Errors;

import ph.com.sunlife.wms.web.controller.binder.BinderAware;
import ph.com.sunlife.wms.web.controller.binder.SessionAware;

// Change according to Healthcheck
// *Problem was declared as public/protected in BinderAware
// *Change BinderAware interface class to abstract class and extends it rather than implements
public class CashierSPForm extends BinderAware implements SessionAware {

    private Long dcrId;
    private String stepProcResponse;
    private HttpSession session;
    private String secretKey;
    private boolean fromSearch;

    public boolean isFromSearch() {
        return fromSearch;
    }

    public void setFromSearch(boolean fromSearch) {
        this.fromSearch = fromSearch;
    }

    public void setDcrId(Long dcrId) {
        this.dcrId = dcrId;
    }

    public Long getDcrId() {
        return dcrId;
    }

    // Change according to Healthcheck
    // *Problem was declared as public/protected in BinderAware
    // *Change use BinderAware getter instead of direct access
    public String[] getAllowedFields() {
        return super.getAllowedFields();
    }

    // Change according to Healthcheck
    // *Problem was declared as public/protected in BinderAware
    // *Change use BinderAware getter instead of direct access
    public String[] getRequiredFields() {
        return super.getRequiredFields();
    }

    // Change according to Healthcheck
    // *Problem was declared as public/protected in BinderAware
    // *Change use BinderAware getter instead of direct access
    public String[] getUncheckedFields() {
        return super.getUncheckedFields();
    }

    @Override
    public void afterBind() {
    }

    @Override
    public boolean validate(Errors errors) {
        return true;
    }

    @Override
    public HttpSession getSession() {
        return session;
    }

    @Override
    public void setSession(HttpSession session) {
        this.session = session;
    }

    public String getSecretKey() {
        return secretKey;
    }

    public void setSecretKey(String secretKey) {
        this.secretKey = secretKey;
    }

    public String getStepProcResponse() {
        return stepProcResponse;
    }

    public void setStepProcResponse(String stepProcResponse) {
        this.stepProcResponse = stepProcResponse;
    }
}
