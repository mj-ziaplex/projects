package ph.com.sunlife.wms.services.bo;

import java.util.Date;

import org.apache.commons.lang.StringUtils;

import ph.com.sunlife.wms.util.WMSDateUtil;
import ph.com.sunlife.wms.util.WMSMimeUtil;

public class AttachmentBO implements Comparable<AttachmentBO> {

	private String filenetId;

	private String fileType;

	private String filename;

	private Integer filesize;

	private String uploaderId;
	
	private String shortName;

	private String fileUrl;

	private String description;
	
	private Date uploadDate;
	
	private String timestamp; 
	
	private String formattedUploadDate;
	
	private String docId;
	
	private Long recordId;
	
	private String recordLocation;
	
	public Long getRecordId() {
		return recordId;
	}

	public void setRecordId(Long recordId) {
		this.recordId = recordId;
	}

	public String getRecordLocation() {
		return recordLocation;
	}

	public void setRecordLocation(String recordLocation) {
		this.recordLocation = recordLocation;
	}

	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	public String getFormattedUploadDate() {
		return formattedUploadDate;
	}

	public void setFormattedUploadDate(String formattedUploadDate) {
		this.formattedUploadDate = formattedUploadDate;
	}

	public Date getUploadDate() {
		return uploadDate;
	}

	public void setUploadDate(Date uploadDate) {
		this.uploadDate = uploadDate;
		this.formattedUploadDate = WMSDateUtil.toFormattedDateStr(uploadDate);
		if(uploadDate != null){
			this.timestamp = WMSDateUtil.toTimestamp(uploadDate);
		}
	}

	public String getFilesizeStr() {
		if (this.filesize == null) {
			return "&nbsp;";
		}

		return WMSMimeUtil.humanReadableByteCount(this.filesize);
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getFilenetId() {
		return filenetId;
	}

	public void setFilenetId(String filenetId) {
		this.filenetId = filenetId;
	}

	public String getFileType() {
		return fileType;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public Integer getFilesize() {
		return filesize;
	}

	public void setFilesize(Integer filesize) {
		this.filesize = filesize;
	}

	public String getUploaderId() {
		return uploaderId;
	}

	public void setUploaderId(String uploaderId) {
		this.uploaderId = uploaderId;
	}

	public String getFileUrl() {
		return fileUrl;
	}

	public void setFileUrl(String fileUrl) {
		this.fileUrl = fileUrl;
	}

    @Override
    public int compareTo(AttachmentBO other) {
        if (StringUtils.isNotEmpty(this.filename)
                && StringUtils.isNotEmpty(other.getFilename())) {
            // Change according to Healthcheck
            // *Problem as using compareTo without overriding equals
            // *Change to equals as per looking code seems to compare file name
            //   value, thus using equals should suffice
            if (this.filename.equals(other.getFilename())) {
                return 0;
            }
        }
        return 0;
    }

	public String getDocId() {
		return docId;
	}

	public void setDocId(String docId) {
		this.docId = docId;
	}

	public String getShortName() {
		return shortName;
	}

	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

}
