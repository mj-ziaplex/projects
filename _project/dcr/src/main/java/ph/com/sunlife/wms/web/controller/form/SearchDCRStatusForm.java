package ph.com.sunlife.wms.web.controller.form;

public class SearchDCRStatusForm extends SearchPageForm {

	private String status;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public SearchDCRStatusForm(){
		super();
	}
	
}
