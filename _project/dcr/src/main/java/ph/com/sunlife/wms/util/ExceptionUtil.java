package ph.com.sunlife.wms.util;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

public class ExceptionUtil {

    private static Logger LOGGER = Logger.getLogger(ExceptionUtil.class);

    public static String getStackTrace(Throwable throwable) {
        Writer writer = new StringWriter();
        PrintWriter printWriter = new PrintWriter(writer);

        // Change according to Healthcheck
        // *Problem was log4j was not used when an error/exception was thrown
        // *Change was add log4j and log error/exception, but retain implementation
        //  as it is being used by multiple classes to save error on DB
        throwable.printStackTrace(printWriter); // retained code
        LOGGER.error("Error was thrown to exception util: ", throwable);
        return writer.toString();
    }

    public static String getFirstNCharacters(String stackTrace, int n) {
        if (StringUtils.isNotBlank(stackTrace)) {
            return stackTrace.substring(0, Math.min(stackTrace.length(), 1000));
        }

        return null;
    }

    public static String getStackTrace(Throwable throwable, int n) {
        return getFirstNCharacters(getStackTrace(throwable), n);
    }
}
