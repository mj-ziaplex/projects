package ph.com.sunlife.wms.services.bo;

public class DCRErrorBO implements Comparable<DCRErrorBO>{
	private String errorId;
	private String errorName;
	private DCRCompletedStepBO completedStep;
	private DCRErrorTagLogConsolidatedDisplayBO consolidatedDisplay;
	
	public DCRErrorTagLogConsolidatedDisplayBO getConsolidatedDisplay() {
		return consolidatedDisplay;
	}

	public void setConsolidatedDisplay(
			DCRErrorTagLogConsolidatedDisplayBO consolidatedDisplay) {
		this.consolidatedDisplay = consolidatedDisplay;
	}

	public String getErrorId() {
		return errorId;
	}

	public void setErrorId(String errorId) {
		this.errorId = errorId;
	}

	public String getErrorName() {
		return errorName;
	}

	public void setErrorName(String errorName) {
		this.errorName = errorName;
	}

	public DCRCompletedStepBO getCompletedStep() {
		return completedStep;
	}

	public void setCompletedStep(DCRCompletedStepBO completedStep) {
		this.completedStep = completedStep;
	}

//	@Override
//	public int hashCode() {
//		final int prime = 31;
//		int result = 1;
//		result = prime * result + ((errorId == null) ? 0 : errorId.hashCode());
//		return result;
//	}
//
//	@Override
//	public boolean equals(Object obj) {
//		if (this == obj)
//			return true;
//		if (obj == null)
//			return false;
//		if (getClass() != obj.getClass())
//			return false;
//		DCRErrorBO other = (DCRErrorBO) obj;
//		if (errorId == null) {
//			if (other.errorId != null)
//				return false;
//		} else if (!errorId.equals(other.errorId))
//			return false;
//		return true;
//	}

	@Override
	public int compareTo(DCRErrorBO other) {
		int compareTo = 0;
		if(other.getErrorId() != null && this.getErrorId() != null){
			//compareTo = other.getErrorId().compareTo(this.getErrorId());
			compareTo = this.getErrorId().compareTo(other.getErrorId());
		}
		return compareTo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((consolidatedDisplay == null) ? 0 : consolidatedDisplay
						.hashCode());
		result = prime * result + ((errorId == null) ? 0 : errorId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DCRErrorBO other = (DCRErrorBO) obj;
		if (consolidatedDisplay == null) {
			if (other.consolidatedDisplay != null)
				return false;
		} else if (!consolidatedDisplay.equals(other.consolidatedDisplay))
			return false;
		if (errorId == null) {
			if (other.errorId != null)
				return false;
		} else if (!errorId.equals(other.errorId))
			return false;
		return true;
	}


}
