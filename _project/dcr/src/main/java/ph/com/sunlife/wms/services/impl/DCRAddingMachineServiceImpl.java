package ph.com.sunlife.wms.services.impl;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

//import net.sf.jmimemagic.Magic;
//import net.sf.jmimemagic.MagicException;
//import net.sf.jmimemagic.MagicMatch;
//import net.sf.jmimemagic.MagicMatchNotFoundException;
//import net.sf.jmimemagic.MagicParseException;
import ph.com.sunlife.wms.dao.DCRAddingMachineDao;
import ph.com.sunlife.wms.dao.DCRCashierDao;
import ph.com.sunlife.wms.dao.domain.DCRAddingMachine;
import ph.com.sunlife.wms.dao.domain.DCRCashier;
import ph.com.sunlife.wms.dao.exceptions.WMSDaoException;
import ph.com.sunlife.wms.integration.DCRFilenetIntegrationService;
import ph.com.sunlife.wms.integration.exceptions.IntegrationException;
import ph.com.sunlife.wms.services.DCRAddingMachineService;
import ph.com.sunlife.wms.services.bo.DCRAddingMachineBO;
import ph.com.sunlife.wms.services.exception.InvalidFileTypeException;
import ph.com.sunlife.wms.services.exception.ServiceException;

/**
 * The implementing class for {@link DCRAddingMachineService}.
 * 
 * @author Zainal Limpao
 * 
 */
public class DCRAddingMachineServiceImpl implements DCRAddingMachineService {

	private static final Logger LOGGER = Logger
			.getLogger(DCRAddingMachineServiceImpl.class);

	private DCRAddingMachineDao dcrAddingMachineDao;

	private DCRFilenetIntegrationService dcrFilenetIntegrationService;

	private DCRCashierDao dcrCashierDao;

	public void setDcrFilenetIntegrationService(
			DCRFilenetIntegrationService dcrFilenetIntegrationService) {
		this.dcrFilenetIntegrationService = dcrFilenetIntegrationService;
	}

	public void setDcrCashierDao(DCRCashierDao dcrCashierDao) {
		this.dcrCashierDao = dcrCashierDao;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ph.com.sunlife.wms.services.DCRAddingMachineService#uploadDCRAddingMachine
	 * (java.util.Map)
	 */
	public DCRAddingMachineBO uploadDCRAddingMachine(Map<String, Object> map)
			throws ServiceException, InvalidFileTypeException {
		Long dcrCashierId = (Long) map.get("dcrCashierId");
		byte[] file = (byte[]) map.get("data");

		return this.uploadDCRAddingMachine(dcrCashierId, file);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ph.com.sunlife.wms.services.DCRAddingMachineService#uploadDCRAddingMachine
	 * (java.lang.Long, byte[])
	 */
	@Override
	public DCRAddingMachineBO uploadDCRAddingMachine(Long dcrCashierId,
			byte[] file) throws ServiceException, InvalidFileTypeException {

		DCRAddingMachine entity = new DCRAddingMachine();
		try {
			String fileExtension = checkFileType(file);

			if (StringUtils.isEmpty(fileExtension)) {
				throw new InvalidFileTypeException("Not a valid excel file");
			}

			entity.setDcrCashier(dcrCashierId);

			DCRCashier dc = dcrCashierDao.getById(dcrCashierId);
			Date dcrDate = dc.getDcr().getDcrDate();
			String docCapsiteId = dc.getDcr().getCcId();

			String customFilename = "ADDINGMACHINE_"
					+ StringUtils.upperCase(dc.getCashier().getAcf2id()) + "."
					+ fileExtension;

			String docId = dcrFilenetIntegrationService.attachFileToWorkItem(
					dcrDate, docCapsiteId, customFilename, file);

			entity.setDcrAddingMachineVrsSerId(docId);
			entity = dcrAddingMachineDao.save(entity);

		} catch (WMSDaoException e) {
			LOGGER.error(e);
			throw new ServiceException(e);
		} catch (IntegrationException e) {
			LOGGER.error(e);
			throw new ServiceException(e);
		}

		DCRAddingMachineBO businessObject = new DCRAddingMachineBO();
		businessObject.setAddingMachineFile(file);
		businessObject.setDcrCashier(dcrCashierId);
		businessObject.setId(entity.getId());

		return businessObject;
	}

	private String checkFileType(byte[] data) {
		String fileType = null;

		InputStream input = new ByteArrayInputStream(data);
		try {
			Workbook workbook = WorkbookFactory.create(input);

			if (workbook instanceof HSSFWorkbook) {
				fileType = "xls";
			} else if (workbook instanceof XSSFWorkbook) {
				fileType = "xlsx";
			}

		} catch (InvalidFormatException e) {
			LOGGER.error(e);
		} catch (IOException e) {
			LOGGER.error(e);
		} catch (Exception e) {
			LOGGER.error(e);
		} finally {
			IOUtils.closeQuietly(input);
		}

		return fileType;
	}

	public void setDcrAddingMachineDao(DCRAddingMachineDao dcrAddingMachineDao) {
		this.dcrAddingMachineDao = dcrAddingMachineDao;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ph.com.sunlife.wms.services.DCRAddingMachineService#getAddingMachine(
	 * java.lang.Long)
	 */
	@Override
	public DCRAddingMachineBO getAddingMachine(Long dcrCashierId)
			throws ServiceException {
		DCRAddingMachineBO businessObject = null;
		try {
			DCRAddingMachine entity = dcrAddingMachineDao
					.getByDcrCashierId(dcrCashierId);
			if (entity != null) {
				businessObject = new DCRAddingMachineBO();
				businessObject.setId(entity.getId());
				businessObject.setDcrAddingMachineVrsSerId(entity
						.getDcrAddingMachineVrsSerId());
				businessObject.setAddingMachineFile(entity
						.getAddingMachineFile());
				businessObject.setDcrCashier(dcrCashierId);
			}
		} catch (WMSDaoException e) {
			LOGGER.error(e);
			throw new ServiceException(e);
		}

		return businessObject;
	}
}
