package ph.com.sunlife.wms.util;

/**
 * Constants Class across the entire WMS-DCR project.
 * 
 * @author Zainal Limpao
 * 
 */
public class WMSConstants {

	public static final String FILE_TYPE_PDF = "pdf";

	public static final String FILE_TYPE_EML = "eml";

	public static final String FILE_TYPE_EXCEL2007 = "excel2007";
	
	public static final String FILE_TYPE_DOC = "doc";
	
	public static final String FILE_TYPE_DOCX = "docx";

	public static final String FILE_TYPE_EXCEL = "excel";
	
	public static final String FILE_TYPE_JPEG = "jpeg";
	
	public static final String FILE_TYPE_GIF = "gif";
	
	public static final String FILE_TYPE_PNG = "png";
	
	public static final String FILE_TYPE_HTML = "html";
	
	public static final String FILE_TYPE_ZIP = "zip";
	
	public static final String FILE_TYPE_TIFF = "tiff";

	public static final String STATUS_NEW = "NEW";

	public static final String STATUS_PENDING = "PENDING";

	public static final String USER_SESSION = "userSession";

	public static final String HAS_CASHIER_ADHOC_MENU = "hasCashierAdhocMenu";

	public static final String LINE_SEPARATOR = System.getProperty("line.separator");

	public static final String STATUS_AWAITING_REQUIREMENTS = "Awaiting Reqts";

	public static final String STATUS_RECONCILED = "PPA Reconciled";

	public static final String STATUS_APPROVED_FOR_RECON = "Approved for Recon";

	public static final String STATUS_AWAITING_RESPONSE = "Awaiting Response";

	public static final String STATUS_FOR_REVIEW = "For Review";

	public static final String STATUS_FOR_VERIFICATION = "For Verification";
	
	public static final String STATUS_VERIFIED = "Verified";
	
	public static final String STATUS_NOT_VERIFIED = "Not Verified";
	
	public static final String STATUS_REQUIREMENTS_SUBMITTED = "Reqts Submitted";
	
	public static final String STATUS_REQUIREMENTS_NOT_SUBMITTED = "Reqts Not Submitted";
        
    // Added for MR-WF-16-00034 - Random sampling for QA
    public static final String STATUS_QA_REVIEWED = "QA Reviewed";
	
	public static final Long DCR_CREATION_JOB_ID = 1L;
	
	public static final Long COLLECTION_DISCREPANCY_JOB_ID = 2L;
	
	public static final Long PPA_ABEYANCE_JOB_ID = 3L;
	
	public static final Long CHECK_EXPIRING_DCR = 4L;

	public static final Long POPULATE_IPAC_TO_DCR = 5L;

}
