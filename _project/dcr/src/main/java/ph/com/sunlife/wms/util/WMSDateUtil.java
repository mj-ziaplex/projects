package ph.com.sunlife.wms.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.apache.commons.lang.time.DateUtils;
import org.joda.time.DateTime;
import org.joda.time.Days;

/**
 * The custom WMS Date Util class.
 * 
 * @author Zainal Limpao
 *
 */
public class WMSDateUtil {

	private static final int COMPLETION_DATE_ALLOWANCE = 7;

	private static final String FORMAT_DATE_STR = "ddMMMyyyy";

	private static final String FORMAT_DATE_STR_WITH_TIME = "ddMMMyyyy HH:mm";

	private static final String FORMAT_DATE_STR_WITH_COMPLETE_TIME = "ddMMMyyyy HH:mm:ss";

	private static final String FORMAT_TIMESTAMP_STR = "EEE MMM dd HH:mm:ss Z yyyy";
	
	private static final String FORMAT_DATE_MMMM_dd_yyyy = "MMMM dd, yyyy";
	
	private static final DateFormat DATE_MM_dd_yyyy_FORMAT = new SimpleDateFormat(
			FORMAT_DATE_MMMM_dd_yyyy);

	private static final DateFormat DATE_FORMAT = new SimpleDateFormat(
			FORMAT_DATE_STR);
	
	private static final DateFormat TIMESTAMP_FORMAT = new SimpleDateFormat(
		FORMAT_TIMESTAMP_STR
	);

	private static final DateFormat DATE_FORMAT_WITH_TIME = new SimpleDateFormat(
			FORMAT_DATE_STR_WITH_TIME);

	private static final DateFormat DATE_FORMAT_WITH_COMPLETE_TIME = new SimpleDateFormat(
			FORMAT_DATE_STR_WITH_COMPLETE_TIME);

	/**
	 * Set to false to avoid instantiation.
	 */
	private WMSDateUtil() {
	}

	/**
	 * It will set to the first millisecond of the current {@link Date} object.
	 * 
	 * @return
	 */
	public static Date startOfToday() {
		return startOfTheDay(new Date());
	}

	/**
	 * It will set to the first millisecond of the given {@link Date} object.
	 * 
	 * @param date
	 * @return
	 */
	public static Date startOfTheDay(Date date) {
		date = DateUtils.setHours(date, 0);
		date = DateUtils.setMinutes(date, 0);
		date = DateUtils.setSeconds(date, 0);
		date = DateUtils.setMilliseconds(date, 0);
		return date;
	}

	/**
	 * Get the Required Completion Date of the given {@link Date} object.
	 * 
	 * @param date
	 * @return
	 */
	public static Date getRCD(Date date) {
		if (date == null) {
			return null;
		}
		
		return DateUtils.addDays(date, COMPLETION_DATE_ALLOWANCE);
	}

	public static String toFormattedDateStr(Date date) {
		String formattedDateStr = null;
		if (date != null) {
			formattedDateStr = DATE_FORMAT.format(date);
		}
		return formattedDateStr;
	}

	public static String toFormattedDateStrWithTime(Date date) {
		String formattedDateStr = null;
		if (date != null) {
			formattedDateStr = DATE_FORMAT_WITH_TIME.format(date);
		}
		return formattedDateStr;
	}
	
	//Added for MR-WF-16-00112
	public static String toFormattedDateStrWithCompleteTime(Date date) {
		String formattedDateStr = null;
		if (date != null) {
			formattedDateStr = DATE_FORMAT_WITH_COMPLETE_TIME.format(date);
		}
		return formattedDateStr;
	}

	public static Date toDate(String formattedDateStr) {
		Date result = null;
		try {
			result = DATE_FORMAT.parse(formattedDateStr);
		} catch (ParseException e) {
			// Swallow this
		}

		return result;
	}

	public static Date toDateWithTime(String formattedDateStr) {
		Date result = null;
		try {
			result = DATE_FORMAT_WITH_TIME.parse(formattedDateStr);
		} catch (ParseException e) {
			// Swallow this
		}

		return result;
	}
	
	public static Date toDateWithCompleteTime(String formattedDateStr) {
		Date result = null;
		try {
			result = DATE_FORMAT_WITH_COMPLETE_TIME.parse(formattedDateStr);
		} catch (ParseException e) {
			// Swallow this
		}

		return result;
	}
	
	public static int daysBetween(Date start, Date end) {
		if (start == null || end == null) {
			throw new IllegalArgumentException("Input dates can't be null");
		}
		
		DateTime startDt = new DateTime(start);
		DateTime endDt = new DateTime(end);
		
		Days d = Days.daysBetween(startDt, endDt);
		return d.getDays();
	}

	public static Date startOfMonth(String formattedDateStr) {
		Date result = null;
		try {
			Date tempDate = DATE_FORMAT.parse(formattedDateStr);
			result = DateUtils.setDays(tempDate, 1);
			result = startOfTheDay(result);
		} catch (ParseException e) {
			// Swallow this
		}
		return result;
	}

	public static Date endOfMonth(String formattedDateStr) {
		Date result = null;
		try {
			Calendar calendar = Calendar.getInstance();
			Date tempDate = DATE_FORMAT.parse(formattedDateStr);
			calendar.setTime(tempDate);
			result = DateUtils.setDays(tempDate,
					calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
			result = endOfTheDay(result);
		} catch (ParseException e) {
			// Swallow this
		}
		return result;
	}

	public static Date endOfTheDay(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		date = DateUtils.setHours(date,
				calendar.getActualMaximum(Calendar.HOUR_OF_DAY));
		date = DateUtils.setMinutes(date,
				calendar.getActualMaximum(Calendar.MINUTE));
		date = DateUtils.setSeconds(date,
				calendar.getActualMaximum(Calendar.SECOND));
		date = DateUtils.setMilliseconds(date,
				calendar.getActualMaximum(Calendar.MILLISECOND));
		return date;
	}
	
	public static String toFormatMMMMddyyyy(Date date){
		
		String MMMMddyyyy = null;
		
		if (date != null) {
			MMMMddyyyy = DATE_MM_dd_yyyy_FORMAT.format(date);
		}
		
		return MMMMddyyyy;
	}

	public static Date getPreviousDate(Date date){//change to previous 'DCR' date (get from DB?)
		date = DateUtils.addDays(date, -1);
		return date;
	};
	
	public static String toTimestamp(Date date){
		return TIMESTAMP_FORMAT.format(date);
	}
}
