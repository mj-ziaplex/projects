/**
 * 
 */
package ph.com.sunlife.wms.dao;

import java.util.Date;
import java.util.List;

import ph.com.sunlife.wms.dao.domain.SessionTotal;
import ph.com.sunlife.wms.dao.exceptions.WMSDaoException;

/**
 * @author i176
 *
 */
public interface DCRIngeniumDao {
	
	int insertDCRIngeniumSessionTotal(SessionTotal sessTotal) throws WMSDaoException;

	int deleteDCRIngeniumSessionTotal(String ccId, Date processDate) throws WMSDaoException;

	List<SessionTotal> getDCRIngeniumSessionTotal(SessionTotal sessTotal) throws WMSDaoException;

	List<SessionTotal> getDCRIngeniumSessionTotal(String centerCode, Date processDate) throws WMSDaoException;

}
