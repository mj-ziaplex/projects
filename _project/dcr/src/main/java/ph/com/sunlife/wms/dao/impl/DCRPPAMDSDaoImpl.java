package ph.com.sunlife.wms.dao.impl;

import java.sql.SQLException;
import java.util.List;

import org.springframework.orm.ibatis.SqlMapClientCallback;

import ph.com.sunlife.wms.dao.AbstractWMSSqlMapClientDaoSupport;
import ph.com.sunlife.wms.dao.DCRPPAMDSDao;
import ph.com.sunlife.wms.dao.domain.DCRPPAMDS;
import ph.com.sunlife.wms.dao.domain.DCRPPAMDSNote;
import ph.com.sunlife.wms.dao.exceptions.WMSDaoException;

import com.ibatis.sqlmap.client.SqlMapExecutor;

public class DCRPPAMDSDaoImpl extends
		AbstractWMSSqlMapClientDaoSupport<DCRPPAMDS> implements DCRPPAMDSDao {

	private static final String NAMESPACE = "DCRPPAMDS";

	@Override
	public DCRPPAMDS insertDCRPPAMDS(DCRPPAMDS entity) throws WMSDaoException {
		try {
			Long id = (Long) getSqlMapClientTemplate().queryForObject(
					getNamespace() + ".insertDCRPPAMDS", entity);
			entity.setId(id);
		} catch (Exception ex) {
			throw new WMSDaoException(ex);
		}
		return entity;
	}

	@Override
	public boolean reconcilePPAMDS(DCRPPAMDS entity) throws WMSDaoException {
		boolean isUpdated = false;

		try {
			int rowsUpdated = getSqlMapClientTemplate().update(
					getNamespace() + ".reconcilePPAMDS", entity);

			isUpdated = rowsUpdated != 0;
		} catch (Exception ex) {
			throw new WMSDaoException(ex);
		}

		return isUpdated;
	}

	@Override
	public boolean unreconcilePPAMDS(Long id) throws WMSDaoException {
		boolean isUpdated = false;

		try {
			int rowsUpdated = getSqlMapClientTemplate().update(
					getNamespace() + ".unreconcilePPAMDS", id);

			isUpdated = rowsUpdated != 0;
		} catch (Exception ex) {
			throw new WMSDaoException(ex);
		}

		return isUpdated;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<DCRPPAMDS> getPPAMDSListByDcrId(final Long dcrId)
			throws WMSDaoException {
		List<DCRPPAMDS> results = null;

		try {
			results = (List<DCRPPAMDS>) getSqlMapClientTemplate().execute(
					new SqlMapClientCallback() {

						@Override
						public Object doInSqlMapClient(SqlMapExecutor executor)
								throws SQLException {
							return executor.queryForList(getNamespace()
									+ ".getAllPPAMDSByDcrId", dcrId);
						}
					});
		} catch (Exception e) {
			throw new WMSDaoException(e);
		}
		return results;
	}

	@Override
	public DCRPPAMDS getPPAMDSById(Long id) throws WMSDaoException {
		DCRPPAMDS entity = null;

		try {
			entity = (DCRPPAMDS) getSqlMapClientTemplate().queryForObject(
					getNamespace() + ".getPPAMDSById", id);
		} catch (Exception ex) {
			throw new WMSDaoException(ex);
		}

		return entity;
	}

	@Override
	protected String getNamespace() {
		return NAMESPACE;
	}

	@Override
	// add acct and approval ids to where clause in the xml?
	public DCRPPAMDS getIdByDcrIdAndSalesSlipNumber(final DCRPPAMDS entity)
			throws WMSDaoException {
		DCRPPAMDS fromDb = null;

		try {
			fromDb = (DCRPPAMDS) getSqlMapClientTemplate().queryForObject(
					getNamespace() + ".getIdByDcrIdAndSalesSlipNumber", entity);
		} catch (Exception e) {
			throw new WMSDaoException(e);
		}
		return fromDb;
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<DCRPPAMDS> getWmsMdsTransactions(List<String> salesSlipNumbers)
			throws WMSDaoException {
		try {
			List<DCRPPAMDS> result = getSqlMapClientTemplate()
					.queryForList(getNamespace() + ".getWmsMdsTransactions",
							salesSlipNumbers);
			return result;
		} catch (Exception ex) {
			throw new WMSDaoException(ex);
		}
	}

	@SuppressWarnings("unchecked")
	public List<DCRPPAMDSNote> getDCRPPAMDSNote(Long dcrId)
			throws WMSDaoException {
		try {
			List<DCRPPAMDSNote> result = getSqlMapClientTemplate()
					.queryForList(getNamespace() + ".getDCRPPAMDSNotes", dcrId);
			return result;
		} catch (Exception ex) {
			throw new WMSDaoException(ex);
		}
	}

	public DCRPPAMDSNote savePPAMDSNote(DCRPPAMDSNote note)
			throws WMSDaoException {
		try {
			Long id = (Long) getSqlMapClientTemplate().queryForObject(
					getNamespace() + ".insertDCRPPAMDSNote", note);
			note.setId(id);
			return note;
		} catch (Exception ex) {
			throw new WMSDaoException(ex);
		}
	}

	public boolean updatePPAMDSNote(DCRPPAMDSNote note)
			throws WMSDaoException {
		try {
			int update = getSqlMapClientTemplate().update(
					getNamespace() + ".updateDCRPPAMDSNote", note);
			return (update != 0);
		} catch (Exception ex) {
			throw new WMSDaoException(ex);
		}
	}

}
