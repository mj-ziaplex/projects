package ph.com.sunlife.wms.web.controller.form;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.validation.Errors;

import ph.com.sunlife.wms.web.controller.binder.BinderAware;
import ph.com.sunlife.wms.web.controller.binder.SessionAware;

// Change according to Healthcheck
// *Problem was declared as public/protected in BinderAware
// *Change BinderAware interface class to abstract class and extends it rather than implements
public class AdminWmsUserForm extends BinderAware implements SessionAware {

    private String wmsuId;
    private Boolean wmsuActive;
    private Date wmsuExpPwdDate;
    private String wmsuName;
    private String wmsuPwd;
    private String wmsuCreUser;
    private String wmsuCreDate;
    private String wmsuUpdUser;
    private String wmsuUpdDate;
    private String wmsuShortName;
    private String wmsuLongName;
    private Date wmsdcrLastLogin;
    private Date wmsdcrLastlogout;
    private Boolean wmsdcrActive;
    private List<String> userGroups;
    private String wmsuActiveOpt;
    private List<String> userHubs;
    private List<String> centerCodes;
    private String companyCode;
    private HttpSession session;

    public String getWmsuId() {
        return wmsuId;
    }

    public void setWmsuId(String wmsuId) {
        this.wmsuId = wmsuId;
    }

    public Boolean getWmsuActive() {
        return wmsuActive;
    }

    public void setWmsuActive(Boolean wmsuActive) {
        this.wmsuActive = wmsuActive;
    }

    public Date getWmsuExpPwdDate() {
        return wmsuExpPwdDate;
    }

    public void setWmsuExpPwdDate(Date wmsuExpPwdDate) {
        this.wmsuExpPwdDate = wmsuExpPwdDate;
    }

    public String getWmsuName() {
        return wmsuName;
    }

    public void setWmsuName(String wmsuName) {
        this.wmsuName = wmsuName;
    }

    public String getWmsuPwd() {
        return wmsuPwd;
    }

    public void setWmsuPwd(String wmsuPwd) {
        this.wmsuPwd = wmsuPwd;
    }

    public String getWmsuCreUser() {
        return wmsuCreUser;
    }

    public void setWmsuCreUser(String wmsuCreUser) {
        this.wmsuCreUser = wmsuCreUser;
    }

    public String getWmsuCreDate() {
        return wmsuCreDate;
    }

    public void setWmsuCreDate(String wmsuCreDate) {
        this.wmsuCreDate = wmsuCreDate;
    }

    public String getWmsuUpdUser() {
        return wmsuUpdUser;
    }

    public void setWmsuUpdUser(String wmsuUpdUser) {
        this.wmsuUpdUser = wmsuUpdUser;
    }

    public String getWmsuUpdDate() {
        return wmsuUpdDate;
    }

    public void setWmsuUpdDate(String wmsuUpdDate) {
        this.wmsuUpdDate = wmsuUpdDate;
    }

    public String getWmsuShortName() {
        return wmsuShortName;
    }

    public void setWmsuShortName(String wmsuShortName) {
        this.wmsuShortName = wmsuShortName;
    }

    public String getWmsuLongName() {
        return wmsuLongName;
    }

    public void setWmsuLongName(String wmsuLongName) {
        this.wmsuLongName = wmsuLongName;
    }

    public Date getWmsdcrLastLogin() {
        return wmsdcrLastLogin;
    }

    public void setWmsdcrLastLogin(Date wmsdcrLastLogin) {
        this.wmsdcrLastLogin = wmsdcrLastLogin;
    }

    public Date getWmsdcrLastlogout() {
        return wmsdcrLastlogout;
    }

    public void setWmsdcrLastlogout(Date wmsdcrLastlogout) {
        this.wmsdcrLastlogout = wmsdcrLastlogout;
    }

    public Boolean getWmsdcrActive() {
        return wmsdcrActive;
    }

    public void setWmsdcrActive(Boolean wmsdcrActive) {
        this.wmsdcrActive = wmsdcrActive;
    }

    public List<String> getUserGroups() {
        return userGroups;
    }

    public void setUserGroups(List<String> userGroups) {
        this.userGroups = userGroups;
    }

    public String getWmsuActiveOpt() {
        return wmsuActiveOpt;
    }

    public void setWmsuActiveOpt(String wmsuActiveOpt) {
        this.wmsuActiveOpt = wmsuActiveOpt;
    }

    public List<String> getUserHubs() {
        return userHubs;
    }

    public void setUserHubs(List<String> userHubs) {
        this.userHubs = userHubs;
    }

    public List<String> getCenterCodes() {
        return centerCodes;
    }

    public void setCenterCodes(List<String> centerCodes) {
        this.centerCodes = centerCodes;
    }

    public String getCompanyCode() {
        return companyCode;
    }

    public void setCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }

    @Override
    public HttpSession getSession() {
        // TODO Auto-generated method stub
        return session;
    }

    @Override
    public void setSession(HttpSession session) {
        // TODO Auto-generated method stub
        this.session = session;
    }

    @Override
    public String getSecretKey() {
        return null;
    }

    // Change according to Healthcheck
    // *Problem was declared as public/protected in BinderAware
    // *Change use BinderAware getter instead of direct access
    public String[] getAllowedFields() {
        return super.getAllowedFields();
    }

    // Change according to Healthcheck
    // *Problem was declared as public/protected in BinderAware
    // *Change use BinderAware getter instead of direct access
    public String[] getRequiredFields() {
        return super.getRequiredFields();
    }

    // Change according to Healthcheck
    // *Problem was declared as public/protected in BinderAware
    // *Change use BinderAware getter instead of direct access
    public String[] getUncheckedFields() {
        return super.getUncheckedFields();
    }

    @Override
    public void afterBind() {
    }

    @Override
    public boolean validate(Errors errors) {
        return true;
    }
}
