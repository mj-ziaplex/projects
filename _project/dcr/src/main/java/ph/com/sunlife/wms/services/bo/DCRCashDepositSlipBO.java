package ph.com.sunlife.wms.services.bo;

import java.util.Date;

import ph.com.sunlife.wms.dao.domain.Company;
import ph.com.sunlife.wms.dao.domain.Currency;
import ph.com.sunlife.wms.util.IpacUtil;
import ph.com.sunlife.wms.util.WMSDateUtil;

public class DCRCashDepositSlipBO {

	private Long id;
	
	private String userId;

	private DCRCashierBO dcrCashier = new DCRCashierBO();
	
	private long dcrCashierId;

	private BankBO depositoryBank = new BankBO();

	private String depositoryBankId;
	
	private String customerCenter;
	
	private String customerCenterName;

	private Long companyId;

	private Company company;
	
	private String companyStr;

	private String accountNumber;

	private Long currencyId;

	private Currency currency;
	
	private String prodCode;

	private double totalCashAmount;

	private double requiredTotalAmount;

	private Date depositDate;
	
	private String depositDateStr;
	
	private Date dcrDate;
	
	private String dcrDateStr;

	private int denom1000;

	private int denom500;

	private int denom200;

	private int denom100;

	private int denom50;

	private int denom20;

	private int denom10;

	private int denom5;

	private int denom2;

	private int denom1;

	private int denom1Coin;
	
	private int denom50c;
	
	private int denom25c;

	private int denom10c;

	private int denom5c;
	
	private int denom1c;

	private byte[] deposlipFile;

	private String dcrDepoSlipVersionSerialId;
	
	private String acf2id;
	
	private String cashierName;
	
	private String barcodeStr;
	
	private double tradVulCollectionTotal;
	
	private double grpLifeCollectionTotal;
	
	private double hiddenTradVulCollectionTotal;
	
	private double hiddenGrpLifeCollectionTotal;
        
        // Added for PCO purchase will have separate DS
        private double pcoRequiredTotal;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public DCRCashierBO getDcrCashier() {
		return dcrCashier;
	}

	public void setDcrCashier(DCRCashierBO dcrCashier) {
		this.dcrCashier = dcrCashier;
	}

	public void setDcrCashier(Long dcrCashierId) {
		if (dcrCashier == null) {
			dcrCashier = new DCRCashierBO();
		}
		dcrCashier.setId(dcrCashierId);
	}
	
	public void setDcrCashierId(long dcrCashierId) {
		this.dcrCashierId = dcrCashierId;
		if (dcrCashier == null) {
			dcrCashier = new DCRCashierBO();
		}
		dcrCashier.setId(dcrCashierId);
	}

	public long getDcrCashierId() {
		return dcrCashierId;
	}

	public BankBO getDepositoryBank() {
		return depositoryBank;
	}

	public void setDepositoryBank(BankBO depositoryBank) {
		this.depositoryBank = depositoryBank;
		this.depositoryBankId = depositoryBank.getId();
	}

	public String getDepositoryBankId() {
		return depositoryBankId;
	}

	public void setDepositoryBankId(String depositoryBankId) {
		this.depositoryBankId = depositoryBankId;
		BankBO bankBO = new BankBO();
		bankBO.setId(depositoryBankId);
		this.depositoryBank = bankBO;
	}

	public void setCustomerCenter(String customerCenter) {
		this.customerCenter = customerCenter;
	}

	public String getCustomerCenter() {
		return customerCenter;
	}

	public String getCustomerCenterName() {
		return customerCenterName;
	}

	public void setCustomerCenterName(String customerCenterName) {
		this.customerCenterName = customerCenterName;
	}

	public Long getCompanyId() {
		return companyId;
	}

	public void setCompany(Company company) {
		this.company = company;
		this.companyId = company.getId();
		this.companyStr = IpacUtil.toIpacComCode(company.getId());
	}

	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
		this.company = Company.getCompany(companyId);
		this.companyStr = IpacUtil.toIpacComCode(companyId);
	}

	public Company getCompany() {
		return company;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public Long getCurrencyId() {
		return currencyId;
	}

	public void setCurrencyId(Long currencyId) {
		this.currencyId = currencyId;
	}

	public Currency getCurrency() {
		return currency;
	}

	public void setCurrency(Currency currency) {
		this.currency = currency;
		this.currencyId = currency.getId();
	}

	public double getTotalCashAmount() {
		return totalCashAmount;
	}

	public void setTotalCashAmount(double totalCashAmount) {
		this.totalCashAmount = totalCashAmount;
	}

	public double getRequiredTotalAmount() {
		return requiredTotalAmount;
	}

	public void setRequiredTotalAmount(double requiredTotalAmount) {
		this.requiredTotalAmount = requiredTotalAmount;
	}

	public int getDenom1000() {
		return denom1000;
	}

	public void setDenom1000(int denom1000) {
		this.denom1000 = denom1000;
	}

	public int getDenom500() {
		return denom500;
	}

	public void setDenom500(int denom500) {
		this.denom500 = denom500;
	}

	public int getDenom200() {
		return denom200;
	}

	public void setDenom200(int denom200) {
		this.denom200 = denom200;
	}

	public int getDenom100() {
		return denom100;
	}

	public void setDenom100(int denom100) {
		this.denom100 = denom100;
	}

	public int getDenom50() {
		return denom50;
	}

	public void setDenom50(int denom50) {
		this.denom50 = denom50;
	}

	public int getDenom20() {
		return denom20;
	}

	public void setDenom20(int denom20) {
		this.denom20 = denom20;
	}

	public int getDenom10() {
		return denom10;
	}

	public void setDenom10(int denom10) {
		this.denom10 = denom10;
	}

	public int getDenom5() {
		return denom5;
	}

	public void setDenom5(int denom5) {
		this.denom5 = denom5;
	}

	public void setDenom2(int denom2) {
		this.denom2 = denom2;
	}

	public int getDenom2() {
		return denom2;
	}

	public int getDenom1() {
		return denom1;
	}

	public void setDenom1Coin(int denom1Coin) {
		this.denom1Coin = denom1Coin;
	}

	public int getDenom1Coin() {
		return denom1Coin;
	}

	public void setDenom50c(int denom50c) {
		this.denom50c = denom50c;
	}

	public int getDenom50c() {
		return denom50c;
	}

	public void setDenom1(int denom1) {
		this.denom1 = denom1;
	}

	public int getDenom25c() {
		return denom25c;
	}

	public void setDenom25c(int denom25c) {
		this.denom25c = denom25c;
	}

	public int getDenom10c() {
		return denom10c;
	}

	public void setDenom10c(int denom10c) {
		this.denom10c = denom10c;
	}

	public int getDenom5c() {
		return denom5c;
	}

	public void setDenom5c(int denom5c) {
		this.denom5c = denom5c;
	}

	public byte[] getDeposlipFile() {
		return deposlipFile;
	}

	public void setDeposlipFile(byte[] deposlipFile) {
		this.deposlipFile = deposlipFile;
	}

	public String getDcrDepoSlipVersionSerialId() {
		return dcrDepoSlipVersionSerialId;
	}

	public void setDcrDepoSlipVersionSerialId(String dcrDepoSlipVersionSerialId) {
		this.dcrDepoSlipVersionSerialId = dcrDepoSlipVersionSerialId;
	}

	public Date getDepositDate() {
		return depositDate;
	}

	public void setDepositDate(Date depositDate) {
		this.depositDate = depositDate;
		this.depositDateStr = WMSDateUtil.toFormattedDateStr(depositDate);
	}

	public String getDepositDateStr() {
		return depositDateStr;
	}

	public void setDepositDateStr(String depositDateStr) {
		this.depositDateStr = depositDateStr;
		this.depositDate = WMSDateUtil.toDate(depositDateStr);
	}
	
	public String getDcrDateStr() {
		return dcrDateStr;
	}

	public void setDcrDateStr(String dcrDateStr) {
		this.dcrDateStr = dcrDateStr;
		this.dcrDate = WMSDateUtil.toDate(dcrDateStr);
	}
	
	public Date getDcrDate() {
		return dcrDate;
	}

	public void setDcrDate(Date dcrDate) {
		this.dcrDate = dcrDate;
		this.dcrDateStr = WMSDateUtil.toFormattedDateStr(dcrDate);
	}
	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DCRCashDepositSlipBO other = (DCRCashDepositSlipBO) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public int getDenom1c() {
		return denom1c;
	}

	public void setDenom1c(int denom1c) {
		this.denom1c = denom1c;
	}

	public void setCompanyStr(String companyStr) {
		this.companyStr = companyStr;
	}

	public String getCompanyStr() {
		return companyStr;
	}

	public void setProdCode(String prodCode) {
		this.prodCode = prodCode;
	}

	public String getProdCode() {
		return prodCode;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getAcf2id() {
		return acf2id;
	}

	public void setAcf2id(String acf2id) {
		this.acf2id = acf2id;
	}

	public String getCashierName() {
		return cashierName;
	}

	public void setCashierName(String cashierName) {
		this.cashierName = cashierName;
	}

	public String getBarcodeStr() {
		return barcodeStr;
	}

	public void setBarcodeStr(String barcodeStr) {
		this.barcodeStr = barcodeStr;
	}

	public double getHiddenTradVulCollectionTotal() {
		return hiddenTradVulCollectionTotal;
	}

	public void setHiddenTradVulCollectionTotal(double hiddenTradVulCollectionTotal) {
		this.hiddenTradVulCollectionTotal = hiddenTradVulCollectionTotal;
	}

	public double getHiddenGrpLifeCollectionTotal() {
		return hiddenGrpLifeCollectionTotal;
	}

	public void setHiddenGrpLifeCollectionTotal(double hiddenGrpLifeCollectionTotal) {
		this.hiddenGrpLifeCollectionTotal = hiddenGrpLifeCollectionTotal;
	}

	public double getTradVulCollectionTotal() {
		return tradVulCollectionTotal;
	}

	public void setTradVulCollectionTotal(double tradVulCollectionTotal) {
		this.tradVulCollectionTotal = tradVulCollectionTotal;
	}

	public double getGrpLifeCollectionTotal() {
		return grpLifeCollectionTotal;
	}

	public void setGrpLifeCollectionTotal(double grpLifeCollectionTotal) {
		this.grpLifeCollectionTotal = grpLifeCollectionTotal;
	}

    /**
     * @return the pcoRequiredTotal
     */
    public double getPcoRequiredTotal() {
        return pcoRequiredTotal;
    }

    /**
     * @param pcoRequiredTotal the pcoRequiredTotal to set
     */
    public void setPcoRequiredTotal(double pcoRequiredTotal) {
        this.pcoRequiredTotal = pcoRequiredTotal;
    }

}
