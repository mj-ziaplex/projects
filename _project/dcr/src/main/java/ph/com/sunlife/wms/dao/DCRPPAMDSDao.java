package ph.com.sunlife.wms.dao;

import java.util.List;

import ph.com.sunlife.wms.dao.domain.DCRPPAMDS;
import ph.com.sunlife.wms.dao.domain.DCRPPAMDSNote;
import ph.com.sunlife.wms.dao.exceptions.WMSDaoException;

public interface DCRPPAMDSDao extends WMSDao<DCRPPAMDS> {

	DCRPPAMDS insertDCRPPAMDS(DCRPPAMDS entity) throws WMSDaoException;

	boolean reconcilePPAMDS(DCRPPAMDS entity) throws WMSDaoException;

	boolean unreconcilePPAMDS(Long id) throws WMSDaoException;

	List<DCRPPAMDS> getPPAMDSListByDcrId(final Long dcrId)
			throws WMSDaoException;

	List<DCRPPAMDS> getWmsMdsTransactions(List<String> salesSlipNumbers)
			throws WMSDaoException;

	DCRPPAMDS getPPAMDSById(final Long id) throws WMSDaoException;

	DCRPPAMDS getIdByDcrIdAndSalesSlipNumber(final DCRPPAMDS entity)
			throws WMSDaoException;

	List<DCRPPAMDSNote> getDCRPPAMDSNote(Long dcrId) throws WMSDaoException;

	DCRPPAMDSNote savePPAMDSNote(DCRPPAMDSNote note) throws WMSDaoException;

	boolean updatePPAMDSNote(DCRPPAMDSNote note) throws WMSDaoException;
}
