package ph.com.sunlife.wms.dao.impl;

import java.io.PrintStream;
import java.util.List;

import org.springframework.orm.ibatis.SqlMapClientTemplate;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import ph.com.sunlife.wms.dao.DatacapScannerDao;
import ph.com.sunlife.wms.dao.domain.DatacapScanner;
import ph.com.sunlife.wms.dao.domain.WMSUser;
import ph.com.sunlife.wms.dao.exceptions.WMSDaoException;

public class DatacapScannerDaoImpl
  extends SqlMapClientDaoSupport
  implements DatacapScannerDao
{
  private static final String NAMESPACE = "DatacapScanner";
  
  @SuppressWarnings("unchecked")
  @Override
  public List<DatacapScanner> getAllScanner()
    throws WMSDaoException
  {
    try
    {
      List<DatacapScanner> list = (List<DatacapScanner>) getSqlMapClientTemplate().queryForList(NAMESPACE + ".getAllScanner");
      
      return list;
    }
    catch (Exception ex)
    {
      throw new WMSDaoException("Error in " + DatacapScannerDaoImpl.class.getSimpleName() + ": " + ex);
    }
  }
  
  @SuppressWarnings("unchecked")
  @Override
  public List<DatacapScanner> getScannerById(String pcName)
    throws WMSDaoException
  {
    try
    {
    List<DatacapScanner> list = (List<DatacapScanner>) getSqlMapClientTemplate().queryForList(NAMESPACE + ".getScannerById", pcName);
       
    return list;
    }
    catch (Exception ex)
    {
      throw new WMSDaoException("Error in " + DatacapScannerDaoImpl.class.getSimpleName() + ": " + ex);
    }
  }
  
  @SuppressWarnings("unchecked")
  @Override
  public boolean deleteScanner(String pcName)
    throws WMSDaoException
  {
    try
    {
      int deletedRows = getSqlMapClientTemplate().delete(NAMESPACE + ".deleteScanner", pcName);
     
      return deletedRows > 0;
    }
    catch (Exception ex)
    {
      throw new WMSDaoException("Error in " + DatacapScannerDaoImpl.class.getSimpleName() + ": " + ex);
    }
  }
}
