package ph.com.sunlife.wms.dao;

import java.util.Date;
import java.util.List;

import ph.com.sunlife.wms.dao.domain.CashierConfirmation;
import ph.com.sunlife.wms.dao.domain.Contact;
import ph.com.sunlife.wms.dao.domain.CustomerCenter;
import ph.com.sunlife.wms.dao.domain.DCR;
import ph.com.sunlife.wms.dao.domain.DCRAuditLog;
import ph.com.sunlife.wms.dao.domain.DCRNonCashierFile;
import ph.com.sunlife.wms.dao.domain.DCRStepProcessorNote;
import ph.com.sunlife.wms.dao.domain.DCRStepProcessorReconciled;
import ph.com.sunlife.wms.dao.exceptions.WMSDaoException;

/**
 * The Data Access Object responsible for the {@link DCR}.
 * 
 * @author Zainal Limpao
 * 
 */
public interface DCRDao extends WMSDao<DCR> {

	/**
	 * Returns all center codes. This particular method is not directly related
	 * to {@link DCR} table but it's used to initiate creation of {@link DCR}
	 * objects and its children records.
	 * 
	 * @return
	 * @throws WMSDaoException
	 */
	List<String> getAllCenterIds() throws WMSDaoException;

	/**
	 * This will display all the {@link DCR} work items that can be
	 * viewed/access within the given customer center.
	 * 
	 * <p>
	 * <i>Note that this is a DB-only call, and the Filenet query will be
	 * located in a different project module.</i>
	 * </p>
	 * 
	 * @param ccId
	 * @return
	 * @throws WMSDaoException
	 */
	List<DCR> getDCRWorkItemsForCCCashier(String ccId) throws WMSDaoException;

	/**
	 * This retrieves all the created {@link DCR} work items within the entire
	 * Sun Life Philippines.
	 * 
	 * <p>
	 * <i>Note that this is a DB-only call, and the Filenet query will be
	 * located in a different project module.</i>
	 * </p>
	 * 
	 * @param dcrDate
	 * @return
	 * @throws WMSDaoException
	 */
	List<DCR> getAllDCRForTheDay(Date dcrDate) throws WMSDaoException;

	/**
	 * 
	 * @param value
	 * @return
	 * @throws WMSDaoException
	 */
	List<DCR> getAllPendingDCRForCC(String value) throws WMSDaoException;

	List<DCR> getByIds(List<Long> ids) throws WMSDaoException;

	String getCustomerCenterId(String centerCode) throws WMSDaoException;

	List<CustomerCenter> getCustomerCenters(List<String> ccIds)
			throws WMSDaoException;

	List<DCR> getDCRByCCAndDate(DCR dcr) throws WMSDaoException;

	List<DCR> getDCRByHubAndDate(DCR dcr) throws WMSDaoException;

	List<DCR> getAllDCRForManager(String hubId) throws WMSDaoException;

	List<DCR> getDCRManagerByCCAndDate(DCR dcr) throws WMSDaoException;

	List<DCR> getDCRManagerByCC(List<String> ccIds) throws WMSDaoException;

	Long saveNote(DCRStepProcessorNote note) throws WMSDaoException;

	List<DCRStepProcessorNote> getAllNotesForDcr(Long dcrId)
			throws WMSDaoException;

	DCRStepProcessorNote getNoteById(Long id) throws WMSDaoException;

	boolean updateWorkObjectNumber(DCR dcr) throws WMSDaoException;

	DCRStepProcessorReconciled createDCRStepProcessorReconciled(
			DCRStepProcessorReconciled entity) throws WMSDaoException;

	List<DCRStepProcessorReconciled> getDCRStepProcessorReconciled(Long dcrId)
			throws WMSDaoException;

	boolean updateDCRStepProcessorReconciled(DCRStepProcessorReconciled entity)
			throws WMSDaoException;

	boolean updateDcrStatus(DCR entity) throws WMSDaoException;
	
	boolean updateDcrStatusForCCQA(DCR entity) throws WMSDaoException;

	Long saveNonCashierFile(DCRNonCashierFile entity) throws WMSDaoException;

	List<DCRNonCashierFile> getAllNonCashierFile(Long dcrId)
			throws WMSDaoException;

	boolean deleteAllNotes(Long dcrId) throws WMSDaoException;

	boolean checkIfHoliday(Date date) throws WMSDaoException;
	
	List<CashierConfirmation> getCashierConfirmations(Long dcrId)
			throws WMSDaoException;

	Long createLog(DCRAuditLog dcrAuditLog) throws WMSDaoException;

	List<DCRAuditLog> getAuditLogsByDcrId(Long dcrId) throws WMSDaoException;

	Date getDateFiveBusinessDaysAgo() throws WMSDaoException;

	Date getDateThreeBusinessDaysAgo() throws WMSDaoException;

	List<DCR> getLapsedDCRSetForVerification(Date updateDate)
			throws WMSDaoException;

	List<DCR> getLapsedDCRSetForAwaitingReqts(Date updateDate)
			throws WMSDaoException;

	int countSubmittedNonCashierFileAfterGivenDate(DCRNonCashierFile obj)
			throws WMSDaoException;
	
	Date getPreviousBusinessDate(int businessDaysAgo) throws WMSDaoException;
	
	boolean updateDcrToPending(DCR dcr) throws WMSDaoException;

	int countHolidays(Date date) throws WMSDaoException;
	
	List<DCR> getAllUnSubmittedToPPADcr() throws WMSDaoException;
	
	List<Contact> getHubManagerContacts(String hubId) throws WMSDaoException;
	
	List<DCR> getDCRWorkItemsForIPACDownload() throws WMSDaoException;
	
	List<DCR> getAllDcrForStatusUpdate() throws WMSDaoException;
	
	boolean updateDCRDownloadFlag(DCR dcr) throws WMSDaoException;
	
	List<DCR> getAllDcrForStatusUpdateFromSearch(DCR dcr) throws WMSDaoException;
	
	//Added for and MR-WF-16-00112
	Integer requiredCompCount();
	
	//Added for and MR-WF-16-00112
	Integer countHolidaysAndWeekend(int ccqaTAT) throws WMSDaoException;
	
	//Added for and MR-WF-16-00112
	boolean checkIfHolidayOrWeekend(int ccqaTAT) throws WMSDaoException;
	
	
	/*
	 * Added for MR-WF-16-00034 - Random sampling for QA
	 */
	List<DCR> getDCPpaQa(DCR dcr) throws WMSDaoException;

	/*
	 * Added for MR-WF-16-00034 - Random sampling for QA
	 */
	List<DCR> getDCRQaByCC(List<String> ccIds) throws WMSDaoException;

	/*
	 * Added for MR-WF-16-00034 - Random sampling for QA
	 */
	boolean updateQaFlag(Long dcrId, String newStatus) throws WMSDaoException;

}
