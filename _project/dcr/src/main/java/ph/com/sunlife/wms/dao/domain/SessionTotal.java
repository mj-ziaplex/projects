/**
 * 
 */
package ph.com.sunlife.wms.dao.domain;

import java.util.Date;

/**
 * @author i176
 *
 */
public class SessionTotal {

	private String userId;
	
	private Date processDate;
	
	private String company;
	
	private String currency;
	
	private double collectionTotal;

	private String centerCode;

	public String getCenterCode() {
		return centerCode;
	}

	public void setCenterCode(String centerCode) {
		this.centerCode = centerCode;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Date getProcessDate() {
		return processDate;
	}

	public void setProcessDate(Date processDate) {
		this.processDate = processDate;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public double getCollectionTotal() {
		return collectionTotal;
	}

	public void setCollectionTotal(double collectionTotal) {
		this.collectionTotal = collectionTotal;
	}

	@Override
	public String toString() {
		return "SessionTotal [userId=" + userId + ", processDate="
				+ processDate + ", company=" + company + ", currency="
				+ currency + ", collectionTotal=" + collectionTotal
				+ ", centerCode=" + centerCode + "]";
	}
}
