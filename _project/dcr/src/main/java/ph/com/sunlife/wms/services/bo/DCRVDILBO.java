package ph.com.sunlife.wms.services.bo;

import java.util.Date;
import java.util.List;

public class DCRVDILBO {

	private Long id;

	private DCRCashierBO dcrCashier;
	
	private double pettyCash;
	
	private double photocopy;
	
	private double workingFund;
	
	private double unclaimedChange;
	
	private double overages;
	
	private int vultradPDCRPendingWarehouse;
	
	private int pnPDCPendingWarehouse;
	
	private int pdcReceivedForTheDayTotal;
	
	private int pdcTradVulCount;
	
	private int pdcPnCount;
	
	private double afterCutoffCashPeso;
	
	private double afterCutoffCashDollar;
	
	private int afterCutoffCheck;
	
	private int afterCutoffCard;
	
	private String forSafeKeeping;
	
	private double totalDollarCoins;
	
	private List<DCRVDILPRSeriesNumberBO> prSeriesNumbers;
	
	private List<DCRVDILISOCollectionBO> isoCollections;
	
	private DCRDollarCoinCollectionBO previousCoinCollection;
	
	private DCRDollarCoinCollectionBO currentCoinCollection;
	
	private DCRDollarCoinExchangeBO dollarCoinExchange;
	
	private Date dateSaved;
	
	private byte[] vdilFile;
	
	private String customerCenter;
	
	private String userId;
	
	private Date dcrDate;
	
	public int getPdcTradVulCount() {
		return pdcTradVulCount;
	}

	public void setPdcTradVulCount(int pdcTradVulCount) {
		this.pdcTradVulCount = pdcTradVulCount;
	}

	public int getPdcPnCount() {
		return pdcPnCount;
	}

	public void setPdcPnCount(int pdcPnCount) {
		this.pdcPnCount = pdcPnCount;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public DCRCashierBO getDcrCashier() {
		return dcrCashier;
	}

	public void setDcrCashier(DCRCashierBO dcrCashier) {
		this.dcrCashier = dcrCashier;
	}
	
	public void setDcrCashier(Long dcrCashierId) {
		DCRCashierBO dcrCashier = new DCRCashierBO();
		dcrCashier.setId(dcrCashierId);
		this.setDcrCashier(dcrCashier);
	}
	
	public double getPettyCash() {
		return pettyCash;
	}

	public void setPettyCash(double pettyCash) {
		this.pettyCash = pettyCash;
	}

	public double getPhotocopy() {
		return photocopy;
	}

	public void setPhotocopy(double photocopy) {
		this.photocopy = photocopy;
	}

	public double getWorkingFund() {
		return workingFund;
	}

	public void setWorkingFund(double workingFund) {
		this.workingFund = workingFund;
	}

	public double getUnclaimedChange() {
		return unclaimedChange;
	}

	public void setUnclaimedChange(double unclaimedChange) {
		this.unclaimedChange = unclaimedChange;
	}

	public double getOverages() {
		return overages;
	}

	public void setOverages(double overages) {
		this.overages = overages;
	}

	public int getVultradPDCRPendingWarehouse() {
		return vultradPDCRPendingWarehouse;
	}

	public void setVultradPDCRPendingWarehouse(int vultradPDCRPendingWarehouse) {
		this.vultradPDCRPendingWarehouse = vultradPDCRPendingWarehouse;
	}

	public int getPnPDCPendingWarehouse() {
		return pnPDCPendingWarehouse;
	}

	public void setPnPDCPendingWarehouse(int pnPDCPendingWarehouse) {
		this.pnPDCPendingWarehouse = pnPDCPendingWarehouse;
	}

	public double getAfterCutoffCashPeso() {
		return afterCutoffCashPeso;
	}

	public void setAfterCutoffCashPeso(double afterCutoffCashPeso) {
		this.afterCutoffCashPeso = afterCutoffCashPeso;
	}

	public double getAfterCutoffCashDollar() {
		return afterCutoffCashDollar;
	}

	public void setAfterCutoffCashDollar(double afterCutoffCashDollar) {
		this.afterCutoffCashDollar = afterCutoffCashDollar;
	}

	public int getAfterCutoffCheck() {
		return afterCutoffCheck;
	}

	public void setAfterCutoffCheck(int afterCutoffCheck) {
		this.afterCutoffCheck = afterCutoffCheck;
	}

	public int getAfterCutoffCard() {
		return afterCutoffCard;
	}

	public void setAfterCutoffCard(int afterCutoffCard) {
		this.afterCutoffCard = afterCutoffCard;
	}

	public String getForSafeKeeping() {
		return forSafeKeeping;
	}

	public void setForSafeKeeping(String forSafeKeeping) {
		this.forSafeKeeping = forSafeKeeping;
	}

	public double getTotalDollarCoins() {
		return totalDollarCoins;
	}

	public void setTotalDollarCoins(double totalDollarCoins) {
		this.totalDollarCoins = totalDollarCoins;
	}
	
	public List<DCRVDILPRSeriesNumberBO> getPrSeriesNumbers() {
		return prSeriesNumbers;
	}

	public void setPrSeriesNumbers(List<DCRVDILPRSeriesNumberBO> prSeriesNumbers) {
		this.prSeriesNumbers = prSeriesNumbers;
	}

	public List<DCRVDILISOCollectionBO> getIsoCollections() {
		return isoCollections;
	}

	public void setIsoCollections(List<DCRVDILISOCollectionBO> isoCollections) {
		this.isoCollections = isoCollections;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DCRVDILBO other = (DCRVDILBO) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public DCRDollarCoinCollectionBO getPreviousCoinCollection() {
		return previousCoinCollection;
	}

	public void setPreviousCoinCollection(
			DCRDollarCoinCollectionBO previousCoinCollection) {
		this.previousCoinCollection = previousCoinCollection;
	}

	public DCRDollarCoinCollectionBO getCurrentCoinCollection() {
		return currentCoinCollection;
	}

	public void setCurrentCoinCollection(
			DCRDollarCoinCollectionBO currentCoinCollection) {
		this.currentCoinCollection = currentCoinCollection;
	}

	public DCRDollarCoinExchangeBO getDollarCoinExchange() {
		return dollarCoinExchange;
	}

	public void setDollarCoinExchange(DCRDollarCoinExchangeBO dollarCoinExchange) {
		this.dollarCoinExchange = dollarCoinExchange;
	}

	public int getPdcReceivedForTheDayTotal() {
		return pdcReceivedForTheDayTotal;
	}

	public void setPdcReceivedForTheDayTotal(int pdcReceivedForTheDayTotal) {
		this.pdcReceivedForTheDayTotal = pdcReceivedForTheDayTotal;
	}

	public Date getDateSaved() {
		return dateSaved;
	}

	public void setDateSaved(Date dateSaved) {
		this.dateSaved = dateSaved;
	}
	
	public byte[] getVdilFile() {
		return vdilFile;
	}

	public void setVdilFile(byte[] vdilFile) {
		this.vdilFile = vdilFile;
	}
	
	public String getCustomerCenter() {
		return customerCenter;
	}

	public void setCustomerCenter(String customerCenter) {
		this.customerCenter = customerCenter;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Date getDcrDate() {
		return dcrDate;
	}

	public void setDcrDate(Date dcrDate) {
		this.dcrDate = dcrDate;
	}
}
