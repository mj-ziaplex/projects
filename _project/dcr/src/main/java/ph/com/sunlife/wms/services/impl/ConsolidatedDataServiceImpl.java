/**
 * @author i176
 * ****************************************************************************
 * @author: ia23 - sasay
 * @date: Mar2016
 * @desc: Removed methods and parameters that will not be used due to
 * MR-WF-15-00089 - DCR Redesign
 */
package ph.com.sunlife.wms.services.impl;

import static ph.com.sunlife.wms.util.WMSConstants.LINE_SEPARATOR;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.apache.commons.lang.ObjectUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.springframework.beans.factory.InitializingBean;

import ph.com.sunlife.wms.dao.CashierDao;
import ph.com.sunlife.wms.dao.DCRBalancingToolDao;
import ph.com.sunlife.wms.dao.DCRCashDepositSlipDao;
import ph.com.sunlife.wms.dao.DCRCashierDao;
import ph.com.sunlife.wms.dao.DCRChequeDepositSlipDao;
import ph.com.sunlife.wms.dao.DCRDao;
import ph.com.sunlife.wms.dao.DCRIPACDao;
import ph.com.sunlife.wms.dao.JobDataDao;
import ph.com.sunlife.wms.dao.domain.BalancingToolSnapshot;
import ph.com.sunlife.wms.dao.domain.Cashier;
import ph.com.sunlife.wms.dao.domain.CashierConfirmation;
import ph.com.sunlife.wms.dao.domain.CheckType;
import ph.com.sunlife.wms.dao.domain.CollectionOnBehalfData;
import ph.com.sunlife.wms.dao.domain.Company;
import ph.com.sunlife.wms.dao.domain.Contact;
import ph.com.sunlife.wms.dao.domain.Currency;
import ph.com.sunlife.wms.dao.domain.DCR;
import ph.com.sunlife.wms.dao.domain.DCRAuditLog;
import ph.com.sunlife.wms.dao.domain.DCRBalancingTool;
import ph.com.sunlife.wms.dao.domain.DCRBalancingToolProduct;
import ph.com.sunlife.wms.dao.domain.DCRCashDepositSlip;
import ph.com.sunlife.wms.dao.domain.DCRCashier;
import ph.com.sunlife.wms.dao.domain.DCRChequeDepositSlip;
import ph.com.sunlife.wms.dao.domain.DCRIpacValue;
import ph.com.sunlife.wms.dao.domain.DCRNonCashierFile;
import ph.com.sunlife.wms.dao.domain.DCROther;
import ph.com.sunlife.wms.dao.domain.DCRStepProcessorNote;
import ph.com.sunlife.wms.dao.domain.DCRStepProcessorReconciled;
import ph.com.sunlife.wms.dao.domain.Excemption;
import ph.com.sunlife.wms.dao.domain.GAFValue;
import ph.com.sunlife.wms.dao.domain.JobData;
import ph.com.sunlife.wms.dao.domain.JobDataErrorLog;
import ph.com.sunlife.wms.dao.domain.NonPostedTransaction;
import ph.com.sunlife.wms.dao.domain.PPAMDS;
import ph.com.sunlife.wms.dao.domain.ProductType;
import ph.com.sunlife.wms.dao.domain.SessionTotal;
import ph.com.sunlife.wms.dao.exceptions.WMSDaoException;
import ph.com.sunlife.wms.integration.DCRFilenetIntegrationService;
import ph.com.sunlife.wms.integration.client.UserCollectionsTotalUtil;
import ph.com.sunlife.wms.integration.domain.DCRFile;
import ph.com.sunlife.wms.integration.exceptions.IntegrationException;
import ph.com.sunlife.wms.services.AttachmentService;
import ph.com.sunlife.wms.services.CachingService;
import ph.com.sunlife.wms.services.ConsolidatedDataService;
import ph.com.sunlife.wms.services.DCRCommentService;
import ph.com.sunlife.wms.services.DCRIPACService;
import ph.com.sunlife.wms.services.DCRIngeniumService;
import ph.com.sunlife.wms.services.EmailService;
import ph.com.sunlife.wms.services.PPAMDSService;
import ph.com.sunlife.wms.services.bo.AttachmentBO;
import ph.com.sunlife.wms.services.bo.AuditLog;
import ph.com.sunlife.wms.services.bo.CashierBO;
import ph.com.sunlife.wms.services.bo.CashierConfirmationBO;
import ph.com.sunlife.wms.services.bo.CashierWorkItem;
import ph.com.sunlife.wms.services.bo.CompanyBO;
import ph.com.sunlife.wms.services.bo.ConsolidatedCashierProductTotals;
import ph.com.sunlife.wms.services.bo.ConsolidatedCashierTotals;
import ph.com.sunlife.wms.services.bo.ConsolidatedData;
import ph.com.sunlife.wms.services.bo.DCRBO;
import ph.com.sunlife.wms.services.bo.DCRCashDepositSlipBO;
import ph.com.sunlife.wms.services.bo.DCRChequeDepositSlipBO;
import ph.com.sunlife.wms.services.bo.DCRCommentBO;
import ph.com.sunlife.wms.services.bo.DCROtherBO;
import ph.com.sunlife.wms.services.bo.DCRStepProcessorNoteBO;
import ph.com.sunlife.wms.services.bo.ReconciledData;
import ph.com.sunlife.wms.services.bo.WMSEmail;
import ph.com.sunlife.wms.services.exception.AssertUtil;
import ph.com.sunlife.wms.services.exception.ServiceException;
import ph.com.sunlife.wms.util.ExceptionUtil;
import ph.com.sunlife.wms.util.WMSCollectionUtils;
import ph.com.sunlife.wms.util.WMSConstants;
import ph.com.sunlife.wms.util.WMSDateUtil;

import com.sunlife.ingeniumutil.container.UserCollectionsInfo;

/**
 * The implementing class of {@link ConsolidatedDataService} interface.
 *
 * @author Zainal Limpao
 * @author Edgardo Cunanan
 *
 */
public class ConsolidatedDataServiceImpl implements ConsolidatedDataService,
        InitializingBean {

    private static final Logger LOGGER = Logger
            .getLogger(ConsolidatedDataServiceImpl.class);
    private static final String INGENIUM_USERID_SUFFIX = "i";
    private static final long PPA_ABEYANCE_JOB_ID = 3L;
    private static final String QUEUE_DCR_CASHIER_RECON = "DCRCashierRecon";
    private static final String QUEUE_DCR_PPA_RECON = "DCRPPARecon";
    private static final String QUEUE_DCR_PPA_ABEYANCE = "DCRPPAAbeyance";
    private static final String STATUS_AWAITING_REQUIREMENTS = "Awaiting Reqts";
    private static final String STATUS_RECONCILED = "Reconciled";
    private static final String STATUS_APPROVED_FOR_RECON = "Approved for Recon";
    private static final String STATUS_AWAITING_FEEDBACK = "Awaiting Feedback";
    private static final String STATUS_FOR_REVIEW = "For Review and Approval";
    private static final String STATUS_FOR_VERIFICATION = "For Verification";
    private static final String STATUS_NOT_VERIFIED = "Not Verified";
    private static final String STATUS_VERIFIED = "Verified";
    private static final String STATUS_REQUIREMENTS_SUBMITTED = "Reqts Submitted";
    private static final String STATUS_REQUIREMENTS_NOT_SUBMITTED = "Reqts Not Submitted";
    private static final String STANDARD_DATE_FORMAT_STR = "ddMMMyyyy";
    private static final DateFormat STANDARD_DATE_FORMAT = new SimpleDateFormat(STANDARD_DATE_FORMAT_STR);
    private PPAMDSService ppaMDSService;
    private DCRDao dcrDao;
    private DCRBalancingToolDao dcrBalancingToolDao;
    private CashierDao cashierDao;
    private DCRFilenetIntegrationService dcrFilenetIntegrationService;
    private DCRCommentService dcrCommentService;
    private DCRCashierDao dcrCashierDao;
    private DCRCashDepositSlipDao dcrCashDepositSlipDao;
    private DCRChequeDepositSlipDao dcrChequeDepositSlipDao;
    private UserCollectionsTotalUtil slocpiUserCollectionsTotalUtil;
    private UserCollectionsTotalUtil slgfiUserCollectionsTotalUtil;
    private AttachmentService attachmentService;
    private JobDataDao jobDataDao;
    private Map<String, String> cashierReconQueues;
    private Map<String, String> managerReconQueues;
    private int awaitingRequirementsSLA;
    private int forVerificationSLA;
    private CachingService cachingService;
    private EmailService emailService;
    private boolean sendToActualManagers;
    private List<Contact> testManagerContacts;
    private DCRIPACDao dcrIPACDao;
    private DCRIngeniumService dcrIngeniumService;
    private DCRIPACService dcrIPACService;

    public void setDcrIPACService(DCRIPACService dcrIPACService) {
        this.dcrIPACService = dcrIPACService;
    }

    public void setDcrIngeniumService(DCRIngeniumService dcrIngeniumService) {
        this.dcrIngeniumService = dcrIngeniumService;
    }

    public void setDcrIPACDao(DCRIPACDao dcrIPACDao) {
        this.dcrIPACDao = dcrIPACDao;
    }

    public ConsolidatedData getConsolidatedData(Long dcrId)
            throws ServiceException {
        return getConsolidatedData(dcrId, null);
    }

    public ConsolidatedData getConsolidatedData(Long dcrId, String openedBy)
            throws ServiceException {
        return getConsolidatedData(dcrId, openedBy, false, false);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * ph.com.sunlife.wms.services.ConsolidatedDataService#getConsolidatedData
     * (java.lang.Long)
     */
    @Override
    public ConsolidatedData getConsolidatedData(
            Long dcrId,
            String openedBy,
            boolean isPPA,
            boolean isMgr) throws ServiceException {

        LOGGER.info("Retrieving all data for this Step Processor...");
        long startTime = System.currentTimeMillis();

        try {
            LOGGER.info("Start consolidatedData for dcr id: " + dcrId);

            // Get the DCR from DB
            DCR dcrEntity = dcrDao.getById(dcrId);
            AssertUtil.assertNotNullOnService(dcrEntity);

            ConsolidatedData consolidatedData = new ConsolidatedData();
            consolidatedData.setDcrId(dcrId);

            // Set if Day1 or Day2
            consolidatedData.setNonDay1(!DateUtils.isSameDay(dcrEntity.getDcrDate(), cachingService.getWmsDcrSystemDate()));

            Date dcrDate = dcrEntity.getDcrDate();
            Date processDate = dcrDate;
            String centerCode = dcrEntity.getCcId();
            String status = dcrEntity.getStatus();
            String ccName = dcrEntity.getCcName();

            // Get all IPAC Values.
            List<DCRIpacValue> ipacValues = new ArrayList<DCRIpacValue>();
            List<DCRIpacValue> ipacValuesTmp = dcrIPACDao.getDCRIPACDailyCollectionSummary(centerCode, processDate);
            if (CollectionUtils.isNotEmpty(ipacValuesTmp)) {
                LOGGER.info("Retrieving from dcr table calling getDCRIPACDailyCollectionSummary");
                ipacValues = ipacValuesTmp;
            } else {
                LOGGER.info("No data is retrieved in DCRIPACDailyCollectionSummary table...");
            }

            // Get list of Cashier User IDs.
            List<DCRCashier> cashierItems = dcrCashierDao.getDCRCashierListByDcrId(dcrId);
            List<String> cashierUserIds = getCashierUserIds(cashierItems);

            // Set retObject status and other main data
            consolidatedData.setStatus(status);
            consolidatedData.setCustomerCenterName(ccName);
            consolidatedData.setCcId(centerCode);
            consolidatedData.setDcrDate(processDate);

            // Populate data if cashier ID is not empty and with IpacValues
            if (CollectionUtils.isNotEmpty(cashierUserIds)
                    && CollectionUtils.isNotEmpty(ipacValues)) {

                List<Cashier> cashierList = cashierDao.getMultipleCashierInfo(cashierUserIds);

                Map<String, String> userIdShortNameMap = createUserIdShortNameMap(cashierItems);
                consolidatedData.setUserIdShortNameMap(userIdShortNameMap);

                // Get all Comments (combined for DCR) and set to retObject
                List<DCRCommentBO> commentBusinessObjects = dcrCommentService.getAllCommentsDcrId(dcrId);
                consolidatedData.setCommentBusinessObjects(commentBusinessObjects);

                String formattedDateStr = WMSDateUtil.toFormattedDateStr(dcrDate);

                StringBuilder sb = new StringBuilder();
                sb.append(StringUtils.upperCase(formattedDateStr)).append(centerCode);
                String dcrFolderPath = sb.toString();

                List<DCRCashDepositSlipBO> validatedCashDepositSlips = new ArrayList<DCRCashDepositSlipBO>();
                List<DCRChequeDepositSlipBO> validatedChequeDepositSlips = new ArrayList<DCRChequeDepositSlipBO>();

                DateTime startDateTime = DateTime.now();
                List<DCRFile> validatedFiles = dcrFilenetIntegrationService.getValidatedDepositSlipFiles(dcrFolderPath);
                LOGGER.info("getValidatedDepositSlipFiles:" + (DateTime.now().getMillis() - startDateTime.getMillis()));

                if (CollectionUtils.isNotEmpty(validatedFiles)) {
                    mapVdsFilesToConsolidatedDepositSlips(validatedCashDepositSlips, validatedChequeDepositSlips, validatedFiles);
                }

                consolidatedData.setValidatedCashDepositSlips(validatedCashDepositSlips);
                consolidatedData.setValidatedChequeDepositSlips(validatedChequeDepositSlips);

                // Get all attachments and associate to Consolidated Data
                startDateTime = DateTime.now();
                this.associateAttachmentsToConsolidatedData(validatedFiles, dcrId, consolidatedData);
                LOGGER.info("associateAttachmentsToConsolidatedData:" + (DateTime.now().getMillis() - startDateTime.getMillis()));

                List<DCRCashDepositSlipBO> cashDepositSlips = this
                        .getCashDepositSlipForSP(dcrCashDepositSlipDao.getAllCashDepositSlipsByDcrId(dcrId));

                List<DCRChequeDepositSlipBO> chequeDepositSlips = this
                        .getChequeDepositSlipForSP(dcrChequeDepositSlipDao.getAllChequeDepositSlipsByDcrId(dcrId));

                consolidatedData.setDcrCashDepositSlipBO(cashDepositSlips);
                consolidatedData.setDcrChequeDepositSlipBO(chequeDepositSlips);

                putInactiveCashiersToConsolidatedData(ipacValues, cashierItems, consolidatedData);

                // Get all confirmed balancing tool snapshots
                List<BalancingToolSnapshot> snapshots = dcrBalancingToolDao.getSnapshotsByDcrId(dcrId);
                this.associateSnapshotsToConsolidatedData(consolidatedData, snapshots);

                // Get MDS transactions
                List<PPAMDS> allMdsTransactions = assembleAllMDSTransactions(centerCode, processDate, isPPA, isMgr);
                // Get Exception transactions
                List<Excemption> allExcemptions = dcrBalancingToolDao.getExcemptions(dcrId);
                // Get cob transactions
                List<CollectionOnBehalfData> cobList = dcrBalancingToolDao.getCollectionOnBehalfData(dcrId);
                // Get cashier confirmations
                List<CashierConfirmation> confirmations = dcrDao.getCashierConfirmations(dcrId);

                this.associateCashierConfirmationToConsolidatedData(consolidatedData, confirmations);

                List<ConsolidatedCashierTotals> cashierTotals = new ArrayList<ConsolidatedCashierTotals>();

                Set<Long> cashierWorkItemIdsSet = new HashSet<Long>();
                for (DCRCashier cashierItem : cashierItems) {
                    cashierWorkItemIdsSet.add(cashierItem.getId());
                }

                List<Long> cashierWorkItemIds = new ArrayList<Long>(cashierWorkItemIdsSet);

                Set<Long> balancingToolIds = new HashSet<Long>();
                List<DCRBalancingTool> allBalancingTools = dcrBalancingToolDao.getMultipleBalancingTools(cashierWorkItemIds);

                if (CollectionUtils.isNotEmpty(allBalancingTools)) {
                    for (DCRBalancingTool bt : allBalancingTools) {
                        balancingToolIds.add(bt.getId());
                    }
                }

                List<DCRBalancingToolProduct> allBalancingToolProducts = new ArrayList<DCRBalancingToolProduct>();
                if (balancingToolIds.size() > 0) {
                    allBalancingToolProducts = dcrBalancingToolDao
                            .getAllBalancingToolProducts(new ArrayList<Long>(balancingToolIds));
                }

                // Get NonPosted data
                List<NonPostedTransaction> allNonPostedTransactions = new ArrayList<NonPostedTransaction>();
                List<NonPostedTransaction> allNonPostedTransactionsTmp = dcrIPACDao.getDCRIPACDTR(centerCode, processDate);
                if (CollectionUtils.isNotEmpty(allNonPostedTransactionsTmp)) {
                    LOGGER.info("Retrieving from dcr table calling getDCRIPACDTR");
                    allNonPostedTransactions = allNonPostedTransactionsTmp;
                } else {
                    LOGGER.info("No Data retrieve on DCRIPACDTR table...");
                }

                // Get GAF data
                List<GAFValue> allGafValues = new ArrayList<GAFValue>();
                List<GAFValue> allGafValuesTmp = dcrIPACDao.getDCRIPACGAF(centerCode, processDate);
                if (CollectionUtils.isNotEmpty(allGafValuesTmp)) {
                    LOGGER.info("retrieving from dcr table calling getDCRIPACGAF");
                    allGafValues = allGafValuesTmp;
                } else {
                    LOGGER.info("No Data retrieve on DCRIPACGAF table...");
                }

                startDateTime = DateTime.now();
                for (String userId : cashierUserIds) {
                    LOGGER.info("cashierTotal for " + userId);

                    DCRCashier cashierWorkItem = getCashierWorkItem(userId, cashierItems);

                    List<DCRBalancingTool> balancingTools = new ArrayList<DCRBalancingTool>();
                    for (DCRBalancingTool bt : allBalancingTools) {
                        Long thisCashierWorkItemId = bt.getDcrCashier().getId();
                        if (ObjectUtils.equals(cashierWorkItem.getId(), thisCashierWorkItemId)) {
                            balancingTools.add(bt);
                        }
                    }

                    ConsolidatedCashierTotals cashierTotal = new ConsolidatedCashierTotals();
                    cashierTotal.setAcf2id(userId);

                    Cashier cashier = getCashierInfo(userId, cashierList);

                    String cashierFullName = userId + ", " + userId + " " + userId;
                    if (cashier != null) {
                        String firstName = cashier.getFirstname();
                        String lastName = cashier.getLastname();
                        String middleName = cashier.getMiddlename();

                        cashierFullName = lastName + ", " + firstName + " " + middleName;
                    }

                    cashierTotal.setCashierFullName(cashierFullName);
                    cashierTotal.setParent(consolidatedData);

                    this.mapIpacValuesToCorrespondingCashier(cashierTotal, ipacValues);
                    // Set balancingTool values to Cashier
                    this.associateBalancingToolProductsToCashierTotals(
                            cashierTotal, balancingTools,
                            allBalancingToolProducts);

                    // Set NonPosted values to Cashier
                    final String thisCashierId = userId;
                    List<NonPostedTransaction> nonPostedTransactions = WMSCollectionUtils
                            .findMatchedElements(allNonPostedTransactions, new Predicate() {
                        @Override
                        public boolean evaluate(Object object) {
                            NonPostedTransaction thisObj = (NonPostedTransaction) object;
                            String thisUserId = thisObj.getUserId();
                            return StringUtils.equalsIgnoreCase(thisUserId, thisCashierId);
                        }
                    });

                    this.mapDTRToProductTotals(cashierTotal, nonPostedTransactions);
                    this.mapMDSToCashierTotals(allMdsTransactions, userId, cashierTotal);

                    // Set GAF values to Cashier
                    List<GAFValue> gafValues = WMSCollectionUtils
                            .findMatchedElements(allGafValues, new Predicate() {
                        @Override
                        public boolean evaluate(Object object) {
                            GAFValue thisValue = (GAFValue) object;
                            String thisUserId = thisValue.getUserId();
                            return StringUtils.equalsIgnoreCase(thisUserId, thisCashierId);
                        }
                    });
                    this.mapGAFValuesToProductTotals(cashierTotal, gafValues);

                    // Set Excemption values to Cashier
                    List<Excemption> excemptionForCashier = getExcemptionsPerCashier(allExcemptions, userId);
                    this.mapExcemptionsToProductTotals(cashierTotal, excemptionForCashier);
                    this.mapCollectionOnBehalfDataToProductTotals(cashierTotal, cobList);

                    cashierTotals.add(cashierTotal);
                }
                LOGGER.info("cashierTotal:" + (DateTime.now().getMillis() - startDateTime.getMillis()));

                putDcrOthersToConsolidatedData(dcrId, consolidatedData);
                consolidatedData.setCashierTotals(cashierTotals);

                this.associateNotesToConsolidatedData(dcrId, consolidatedData);

                // Check if logged in user is PPA
                if (isPPA) {
                    List<DCRStepProcessorReconciled> list = dcrDao.getDCRStepProcessorReconciled(dcrId);

                    if (CollectionUtils.isNotEmpty(list)) {
                        List<ReconciledData> reconciledData = new ArrayList<ReconciledData>();
                        for (DCRStepProcessorReconciled dspr : list) {
                            List<Integer> reconciledRowNums = this.getReconciledRowNums(dspr);

                            ReconciledData rd = new ReconciledData();
                            rd.setDcrId(dcrId);
                            rd.setUserId(dspr.getUserId());
                            rd.setDateTime(dspr.getDateTime());
                            rd.setReconciledRows(reconciledRowNums);
                            reconciledData.add(rd);
                        }
                        consolidatedData.setReconciledData(reconciledData);
                    }
                }

                this.populateWithCashierWorkItems(dcrId, consolidatedData);

                if (isPPA) {
                    boolean isMdsReconciled = ppaMDSService.isMdsReconciled(
                            dcrEntity.getCcId(), dcrDate, dcrDate, allMdsTransactions);
                    consolidatedData.setMdsReconciled(isMdsReconciled);
                }
            }
            // End of populate data if cashier ID is not empty

            long endTime = System.currentTimeMillis();
            long timeElapsed = endTime - startTime;

            if ("NEW".equalsIgnoreCase(dcrEntity.getStatus())
                    && StringUtils.isNotEmpty(openedBy)) {

                Date today = cachingService.getWmsDcrSystemDate();

                dcrEntity.setStatus("PENDING");
                dcrEntity.setUpdateDate(today);
                dcrEntity.setUpdateUserId(openedBy);
                dcrDao.updateDcrStatus(dcrEntity);

                DCRAuditLog log = new DCRAuditLog();
                log.setActionDetails("WF Action: Set to Pending");
                log.setDcrId(dcrEntity.getId());
                log.setLogDate(today);
                log.setProcessStatus("Pending");
                log.setUserId(openedBy);
                dcrDao.createLog(log);
            }

            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("It took " + timeElapsed + "ms to load all the necessary data for this step processor");
            }
            LOGGER.info("ConsolidatedData time elapsed: " + timeElapsed + " milliseconds");
            LOGGER.info("End consolidatedData for dcr id: " + dcrId);

            return consolidatedData;

        } catch (WMSDaoException ex) {
            LOGGER.error(ex);
            throw new ServiceException(ex);
        } catch (IllegalAccessException ex) {
            LOGGER.error(ex);
            throw new ServiceException(ex);
        } catch (InvocationTargetException ex) {
            LOGGER.error(ex);
            throw new ServiceException(ex);
        } catch (IntegrationException ex) {
            LOGGER.error(ex);
            throw new ServiceException(ex);
        }
    }

    private Map<String, String> createUserIdShortNameMap(
            List<DCRCashier> cashierItems) {
        Map<String, String> userIdShortNameMap = new HashMap<String, String>();
        if (CollectionUtils.isNotEmpty(cashierItems)) {
            for (DCRCashier cashierItem : cashierItems) {
                String acf2id = cashierItem.getCashier().getAcf2id();
                String shortName = cashierItem.getShortName();
                userIdShortNameMap
                        .put(StringUtils.lowerCase(acf2id), shortName);
            }
        }
        return userIdShortNameMap;
    }

    private void mapVdsFilesToConsolidatedDepositSlips(
            List<DCRCashDepositSlipBO> validatedCashDepositSlips,
            List<DCRChequeDepositSlipBO> validatedChequeDepositSlips,
            List<DCRFile> vdsFiles) {
        for (DCRFile file : vdsFiles) {

            String correspondenceType = file.getCorrespondenceType();

            if (StringUtils.containsIgnoreCase(correspondenceType, "Type Cash")) {
                DCRCashDepositSlipBO vds = new DCRCashDepositSlipBO();
                vds.setDcrDepoSlipVersionSerialId(file.getFileId());

                String sourceOfFunds = file.getSourceOfFunds();
                if (StringUtils.containsIgnoreCase(sourceOfFunds,
                        "Currency Peso")) {
                    vds.setCurrency(Currency.PHP);
                } else if (StringUtils.containsIgnoreCase(sourceOfFunds,
                        "Currency Dollar")) {
                    vds.setCurrency(Currency.USD);
                }

                boolean isSlamci = false;
                String reportTitle = file.getReportTitle();
                if (StringUtils.containsIgnoreCase(reportTitle, "SLAMCI")) {
                    Currency cur = vds.getCurrency();
                    if (cur != null) {
                        if (Currency.USD.equals(cur)) {
                            vds.setCompany(Company.SLAMCID);
                        } else {
                            vds.setCompany(Company.SLAMCIP);
                        }
                    }
                    vds.setProdCode("CASH");

                    isSlamci = true;
                } else if (StringUtils.containsIgnoreCase(reportTitle, "SLOCP")) {
                    vds.setCompany(Company.SLOCPI);
                } else if (StringUtils.containsIgnoreCase(reportTitle, "SLFPI")) {
                    vds.setCompany(Company.SLFPI);
                } else if (StringUtils.containsIgnoreCase(reportTitle, "SLGFI")) {
                    vds.setCompany(Company.SLGFI);
                } else if (StringUtils.containsIgnoreCase(reportTitle, "OTHER")) {
                    vds.setCompany(Company.OTHER);
                }

                if (!isSlamci) {
                    String templateID = file.getTemplateID();
                    if (StringUtils.containsIgnoreCase(templateID, "TradVul")
                            || StringUtils.containsIgnoreCase(templateID,
                            "Trad/Vul")) {
                        vds.setProdCode("TRAD/VUL");
                    } else if (StringUtils.containsIgnoreCase(templateID,
                            "Group")) {
                        if (Company.SLGFI.equals(vds.getCompany())) {
                            vds.setProdCode("GROUP");
                        } else {
                            vds.setProdCode("TRAD/VUL");
                        }
                    } else if (StringUtils.containsIgnoreCase(templateID,
                            "Unconverted")) {
                        vds.setProdCode("UNCOVERTED");
                    } else if (StringUtils.containsIgnoreCase(templateID,
                            "Preneed")) {
                        vds.setProdCode("PRENEED");
                    } else if (StringUtils
                            .containsIgnoreCase(templateID, "Gaf")) {
                        vds.setProdCode("GAF");
                    }
                }

                // vds.setAcf2id(file.getLastModifier());
                vds.setAcf2id(StringUtils.upperCase(file.getAssignedUser()));
                validatedCashDepositSlips.add(vds);

            } else if (StringUtils.containsIgnoreCase(correspondenceType,
                    "Cheque")) {

                DCRChequeDepositSlipBO vds = new DCRChequeDepositSlipBO();

                vds.setDcrDepoSlipVersionSerialId(file.getFileId());

                String sourceOfFunds = file.getSourceOfFunds();
                if (StringUtils.containsIgnoreCase(sourceOfFunds,
                        "Currency Peso")) {
                    vds.setCurrency(Currency.PHP);
                } else if (StringUtils.containsIgnoreCase(sourceOfFunds,
                        "Currency Dollar")) {
                    vds.setCurrency(Currency.USD);
                }

                boolean isSlamci = false;
                String reportTitle = file.getReportTitle();
                if (StringUtils.containsIgnoreCase(reportTitle, "SLAMCI")) {
                    Currency cur = vds.getCurrency();
                    if (cur != null) {
                        if (Currency.USD.equals(cur)) {
                            vds.setCompany(Company.SLAMCID);
                        } else {
                            vds.setCompany(Company.SLAMCIP);
                        }
                    }
                    vds.setProdCode("CHEQUE");

                    isSlamci = true;
                } else if (StringUtils.containsIgnoreCase(reportTitle, "SLOCP")) {
                    vds.setCompany(Company.SLOCPI);
                } else if (StringUtils.containsIgnoreCase(reportTitle, "SLFPI")) {
                    vds.setCompany(Company.SLFPI);
                } else if (StringUtils.containsIgnoreCase(reportTitle, "SLGFI")) {
                    vds.setCompany(Company.SLGFI);
                } else if (StringUtils.containsIgnoreCase(reportTitle, "OTHER")) {
                    vds.setCompany(Company.OTHER);
                }

                if (!isSlamci) {
                    String templateID = file.getTemplateID();
                    if (StringUtils.containsIgnoreCase(templateID, "TradVul")
                            || StringUtils.containsIgnoreCase(templateID,
                            "Trad/Vul")) {
                        vds.setProdCode("TRAD/VUL");
                    } else if (StringUtils.containsIgnoreCase(templateID,
                            "Group")) {
                        vds.setProdCode("GROUP");
                    } else if (StringUtils.containsIgnoreCase(templateID,
                            "Unconverted")) {
                        vds.setProdCode("UNCOVERTED");
                    } else if (StringUtils.containsIgnoreCase(templateID,
                            "Preneed")) {
                        vds.setProdCode("PRENEED");
                    } else if (StringUtils
                            .containsIgnoreCase(templateID, "Gaf")) {
                        vds.setProdCode("GAF");
                    }
                }

                vds.setAcf2id(StringUtils.upperCase(file.getAssignedUser()));

                if (StringUtils.containsIgnoreCase(correspondenceType, "On Us")) {
                    vds.setCheckType(CheckType.ON_US);
                } else if (StringUtils.containsIgnoreCase(correspondenceType,
                        "Local")) {
                    vds.setCheckType(CheckType.LOCAL);
                } else if (StringUtils.containsIgnoreCase(correspondenceType,
                        "Regional")) {
                    vds.setCheckType(CheckType.REGIONAL);
                } else if (StringUtils.containsIgnoreCase(correspondenceType,
                        "Dollar Cheque")) {
                    vds.setCheckType(CheckType.DOLLARCHECK);
                } else if (StringUtils.containsIgnoreCase(correspondenceType,
                        "Drawn in Manila")) {
                    vds.setCheckType(CheckType.USCD_MANILA);
                } else if (StringUtils.containsIgnoreCase(correspondenceType,
                        "Cheque Drawn outside PH")) {
                    vds.setCheckType(CheckType.USCD_OUTSIDEPH);
                }

                validatedChequeDepositSlips.add(vds);
            }
        }
    }

    private void associateBalancingToolProductsToCashierTotals(
            ConsolidatedCashierTotals cashierTotal,
            List<DCRBalancingTool> balancingTools,
            List<DCRBalancingToolProduct> allBalancingToolProducts)
            throws WMSDaoException {

        List<ConsolidatedCashierProductTotals> productTotals = cashierTotal
                .getProductTotals();

        List<DCRBalancingToolProduct> allBalancingToolProductsForThisCashier = new ArrayList<DCRBalancingToolProduct>();
        if (CollectionUtils.isNotEmpty(balancingTools)) {
            for (DCRBalancingTool bt : balancingTools) {
                final Long balancingToolId = bt.getId();
                List<DCRBalancingToolProduct> thisBalancingToolProducts = WMSCollectionUtils
                        .findMatchedElements(allBalancingToolProducts,
                        new Predicate() {
                    @Override
                    public boolean evaluate(Object object) {
                        DCRBalancingToolProduct thisBTP = (DCRBalancingToolProduct) object;
                        return ObjectUtils.equals(
                                balancingToolId, thisBTP
                                .getDcrBalancingTool()
                                .getId());
                    }
                });

                allBalancingToolProductsForThisCashier
                        .addAll(thisBalancingToolProducts);
            }
        }

        for (ConsolidatedCashierProductTotals pt : productTotals) {
            Long thisProductTypeId = pt.getProductType().getId();
            for (DCRBalancingToolProduct btpt : allBalancingToolProductsForThisCashier) {
                if (btpt.getProductTypeId().equals(thisProductTypeId)) {
                    pt.setDcrBalancingToolProductId(btpt.getId());
                }
            }
        }
    }

    private void associateSnapshotsToConsolidatedData(
            ConsolidatedData consolidatedData,
            List<BalancingToolSnapshot> snapshots) {
        if (CollectionUtils.isNotEmpty(snapshots)) {
            Map<String, List<AttachmentBO>> snapshotAttachments = new HashMap<String, List<AttachmentBO>>();

            for (BalancingToolSnapshot snapshot : snapshots) {
                CompanyBO company = CompanyBO.getCompany(snapshot
                        .getCompanyId());
                String acf2id = snapshot.getAcf2id();

                String docId1 = snapshot.getDay1DocId();
                if (StringUtils.isNotEmpty(docId1)) {
                    AttachmentBO attachment = new AttachmentBO();
                    attachment.setDocId(docId1);

                    String snapshotStr = "";
                    if (CompanyBO.SLGFI.equals(company)
                            || CompanyBO.SLOCPI.equals(company)) {
                        snapshotStr = "_DAY1_SNAPSHOT_";
                    } else {
                        snapshotStr = "_SNAPSHOT_";
                    }

                    String description = "Data Snapshot for " + company.name()
                            + " by " + StringUtils.upperCase(acf2id);
                    String customFileName = StringUtils.upperCase(company
                            .getName() + snapshotStr + acf2id)
                            + ".html";
                    attachment.setFilename(customFileName);
                    attachment.setDescription(description);

                    attachment.setUploaderId(acf2id);
                    attachment.setFileUrl("viewfile/view.html?fileId=" + docId1
                            + "&fileType=html");

                    attachment.setUploadDate(snapshot.getDateConfirmed());

                    this.putAttachmentToSnapshotsMap(snapshotAttachments,
                            company, attachment);
                }

                String docId2 = snapshot.getDay2DocId();
                if (StringUtils.isNotEmpty(docId2)) {
                    AttachmentBO attachment = new AttachmentBO();
                    attachment.setDocId(docId2);

                    String snapshotStr = "";
                    if (CompanyBO.SLGFI.equals(company)
                            || CompanyBO.SLOCPI.equals(company)) {
                        snapshotStr = "_DAY2_SNAPSHOT_";
                    } else {
                        snapshotStr = "_SNAPSHOT_";
                    }

                    String customFileName = StringUtils.upperCase(company
                            .getName() + snapshotStr + acf2id)
                            + ".html";
                    attachment.setFilename(customFileName);

                    attachment.setFileUrl("viewfile/view.html?fileId=" + docId2
                            + "&fileType=html");
                    attachment.setUploaderId(acf2id);

                    String description = "Data Snapshot for " + company.name()
                            + " by " + StringUtils.upperCase(acf2id);
                    attachment.setDescription(description);

                    attachment.setUploadDate(snapshot.getDateReconfirmed());

                    this.putAttachmentToSnapshotsMap(snapshotAttachments,
                            company, attachment);
                }

            }
            consolidatedData.setSnapshotAttachments(snapshotAttachments);
        }
    }

    private void putAttachmentToSnapshotsMap(
            Map<String, List<AttachmentBO>> snapshotAttachments,
            CompanyBO company, AttachmentBO attachment) {
        List<AttachmentBO> attachments = snapshotAttachments
                .get(company.name());
        if (attachments == null) {
            attachments = new ArrayList<AttachmentBO>();
        }
        attachments.add(attachment);

        snapshotAttachments.remove(company.name());
        snapshotAttachments.put(company.name(), attachments);
    }

    private void associateCashierConfirmationToConsolidatedData(
            ConsolidatedData consolidatedData,
            List<CashierConfirmation> confirmations)
            throws IllegalAccessException, InvocationTargetException {
        Map<String, List<CashierConfirmationBO>> cashierConfirmationsMap = new HashMap<String, List<CashierConfirmationBO>>();
        Date today = cachingService.getWmsDcrSystemDate();
        if (CollectionUtils.isNotEmpty(confirmations)) {
            for (CashierConfirmation c : confirmations) {
                CashierConfirmationBO cb = new CashierConfirmationBO();
                BeanUtils.copyProperties(cb, c);

                Date dateConfirmed = c.getDateConfirmed();
                Date dateReconfirmed = c.getDateReconfirmed();

                boolean isConfirmed = false;
                if (dateConfirmed != null) {
                    if (Company.SLOCPI.getId().equals(c.getCompanyId())
                            || Company.SLGFI.getId().equals(c.getCompanyId())) {
                        if (DateUtils.isSameDay(today, dateConfirmed)) {
                            isConfirmed = true;
                        } else if (dateReconfirmed != null) {
                            isConfirmed = true;
                        }
                    } else {
                        isConfirmed = true;
                    }
                } else {
                    if (Company.SLOCPI.getId().equals(c.getCompanyId())
                            || Company.SLGFI.getId().equals(c.getCompanyId())) {
                        if (dateReconfirmed != null) {
                            isConfirmed = true;
                        }
                    }
                }

                cb.setConfirmed(isConfirmed);

                String userId = c.getUserId();

                List<CashierConfirmationBO> cList = cashierConfirmationsMap
                        .get(userId);
                if (cList == null) {
                    cList = new ArrayList<CashierConfirmationBO>();
                }

                cList.add(cb);
                cashierConfirmationsMap.remove(userId);
                cashierConfirmationsMap.put(userId, cList);
            }
            consolidatedData
                    .setCashierConfirmationsMap(cashierConfirmationsMap);
        }
    }

    private void mapMDSToCashierTotals(List<PPAMDS> allMdsTransactions,
            String userId, ConsolidatedCashierTotals cashierTotal) {
        if (CollectionUtils.isNotEmpty(allMdsTransactions)) {
            for (ConsolidatedCashierProductTotals pt : cashierTotal
                    .getProductTotals()) {
                if (ProductType.SLOCPI_P_TRADVUL.equals(pt.getProductType())) {
                    for (PPAMDS mds : allMdsTransactions) {
                        if (StringUtils.equalsIgnoreCase(userId,
                                mds.getUserId())
                                && "PHP".equalsIgnoreCase(mds.getCurrency())) {
                            String desc = mds.getPaySubDesc();
                            if (StringUtils.containsIgnoreCase(desc, "bpi")) {
                                double amount = Double.valueOf(mds
                                        .getCardAmount());
                                double prevAmount = pt.getTotalMdsBpi();
                                pt.setTotalMdsBpi(prevAmount + amount);
                            } else if (StringUtils.containsIgnoreCase(desc,
                                    "bdo")) {
                                double amount = Double.valueOf(mds
                                        .getCardAmount());
                                double prevAmount = pt.getTotalMdsBdo();
                                pt.setTotalMdsBdo(prevAmount + amount);
                            } else if (StringUtils.containsIgnoreCase(desc,
                                    "ctb")) {
                                double amount = Double.valueOf(mds
                                        .getCardAmount());
                                double prevAmount = pt.getTotalMdsCtb();
                                pt.setTotalMdsCtb(prevAmount + amount);
                            } else if (StringUtils.containsIgnoreCase(desc,
                                    "rcbc")) {
                                double amount = Double.valueOf(mds
                                        .getCardAmount());
                                double prevAmount = pt.getTotalMdsRcbc();
                                pt.setTotalMdsRcbc(prevAmount + amount);
                            } else if (StringUtils.containsIgnoreCase(desc,
                                    "hsbc")) {
                                double amount = Double.valueOf(mds
                                        .getCardAmount());
                                double prevAmount = pt.getTotalMdsHsbc();
                                pt.setTotalMdsHsbc(prevAmount + amount);
                            } else if (StringUtils.containsIgnoreCase(desc,
                                    "sbc")) {
                                double amount = Double.valueOf(mds
                                        .getCardAmount());
                                double prevAmount = pt.getTotalMdsSbc();
                                pt.setTotalMdsSbc(prevAmount + amount);
                            }
                        }
                    }
                } else if (ProductType.SLOCPI_D_TRADVUL.equals(pt
                        .getProductType())) {
                    for (PPAMDS mds : allMdsTransactions) {
                        if (StringUtils.equalsIgnoreCase(userId,
                                mds.getUserId())
                                && "USD".equalsIgnoreCase(mds.getCurrency())) {
                            String desc = mds.getPaySubDesc();
                            if (StringUtils.containsIgnoreCase(desc, "bpi")) {
                                double amount = Double.valueOf(mds
                                        .getCardAmount());
                                double prevAmount = pt.getTotalMdsBpi();
                                pt.setTotalMdsBpi(prevAmount + amount);
                            } else if (StringUtils.containsIgnoreCase(desc,
                                    "bdo")) {
                                double amount = Double.valueOf(mds
                                        .getCardAmount());
                                double prevAmount = pt.getTotalMdsBdo();
                                pt.setTotalMdsBdo(prevAmount + amount);
                            } else if (StringUtils.containsIgnoreCase(desc,
                                    "ctb")) {
                                double amount = Double.valueOf(mds
                                        .getCardAmount());
                                double prevAmount = pt.getTotalMdsCtb();
                                pt.setTotalMdsCtb(prevAmount + amount);
                            } else if (StringUtils.containsIgnoreCase(desc,
                                    "rcbc")) {
                                double amount = Double.valueOf(mds
                                        .getCardAmount());
                                double prevAmount = pt.getTotalMdsRcbc();
                                pt.setTotalMdsRcbc(prevAmount + amount);
                            } else if (StringUtils.containsIgnoreCase(desc,
                                    "hsbc")) {
                                double amount = Double.valueOf(mds
                                        .getCardAmount());
                                double prevAmount = pt.getTotalMdsHsbc();
                                pt.setTotalMdsHsbc(prevAmount + amount);
                            } else if (StringUtils.containsIgnoreCase(desc,
                                    "sbc")) {
                                double amount = Double.valueOf(mds
                                        .getCardAmount());
                                double prevAmount = pt.getTotalMdsSbc();
                                pt.setTotalMdsSbc(prevAmount + amount);
                            }
                        }
                    }
                }
            }
        }
    }

    private List<PPAMDS> assembleAllMDSTransactions(
            String customerCenter,
            Date transactionDate,
            boolean isPPA,
            boolean isMgr) throws WMSDaoException {

        List<PPAMDS> grandList = new ArrayList<PPAMDS>();

        Date from = WMSDateUtil.startOfTheDay(transactionDate);
        Date to = DateUtils.addDays(from, 1);

        Company[] mdsCompanies = new Company[]{Company.SLFPI, Company.SLGFI, Company.SLOCPI};

        for (Company company : mdsCompanies) {
            List<PPAMDS> allMdsTransactions = new ArrayList<PPAMDS>();
            String comStr = company.getName().equalsIgnoreCase("SLOCPI") ? "SLOCP" : company.getName();
            List<PPAMDS> allMdsTransactionsTmp = dcrIPACDao.getDCRIPACMDSTransaction(comStr, customerCenter, from, to);
            if (CollectionUtils.isNotEmpty(allMdsTransactionsTmp)) {
                LOGGER.info("retrieving from dcr table calling getDCRIPACMDSTransaction");
                allMdsTransactions = allMdsTransactionsTmp;
            } else {
                LOGGER.info("retrieving from IPAC SP");
            }

            if (CollectionUtils.isNotEmpty(allMdsTransactions)) {
                grandList.addAll(allMdsTransactions);
            }
        }

        return grandList;
    }

    private void putInactiveCashiersToConsolidatedData(
            List<DCRIpacValue> ipacValues, List<DCRCashier> dcrCashierList,
            ConsolidatedData consolidatedData) {
        List<CashierBO> inactiveCashiers = new ArrayList<CashierBO>();

        Set<String> activeCashierNames = new HashSet<String>();
        Set<String> inactiveCashierNames = new HashSet<String>();

        if (CollectionUtils.isNotEmpty(dcrCashierList)) {
            for (DCRCashier dcrCashier : dcrCashierList) {
                Cashier cashier = dcrCashier.getCashier();
                if (cashier != null) {
                    String acf2id = StringUtils.upperCase(cashier.getAcf2id());
                    activeCashierNames.add(acf2id);
                }
            }
        }

        if (CollectionUtils.isNotEmpty(ipacValues)) {
            for (DCRIpacValue ipacValue : ipacValues) {
                String cashierIdFromIpac = StringUtils.upperCase(ipacValue
                        .getAcf2id());
                if (!StringUtils.containsIgnoreCase(cashierIdFromIpac, "BATCH")) {
                    if (!activeCashierNames.contains(cashierIdFromIpac)) {
                        inactiveCashierNames.add(cashierIdFromIpac);
                    }
                }
            }
        }

        if (CollectionUtils.isNotEmpty(inactiveCashierNames)) {
            for (String cashierName : inactiveCashierNames) {

                CashierBO tempCashierBO = new CashierBO(cashierName);
                try {
                    Cashier cashier = cashierDao.getCashier(cashierName);

                    if (cashier != null) {
                        tempCashierBO.setEmailaddress(cashier.getFirstname());
                        tempCashierBO.setFirstname(cashier.getFirstname());
                        tempCashierBO.setLastname(cashier.getLastname());
                        tempCashierBO.setMiddlename(cashier.getMiddlename());
                    }
                } catch (Exception e) {
                    System.out.println("Cashier info not available");
                    LOGGER.error(e);
                }

                inactiveCashiers.add(tempCashierBO);
            }
        }

        consolidatedData.setInactiveCashiers(inactiveCashiers);
    }

    private void mapGAFValuesToProductTotals(
            ConsolidatedCashierTotals cashierTotal, List<GAFValue> gafValues) {
        if (CollectionUtils.isNotEmpty(gafValues)) {
            List<ConsolidatedCashierProductTotals> productTotals = cashierTotal
                    .getProductTotals();

            for (ConsolidatedCashierProductTotals productTotal : productTotals) {
                if (ProductType.SLIFPI_GAF
                        .equals(productTotal.getProductType())) {
                    for (GAFValue val : gafValues) {
                        String payType = StringUtils.trim(val.getPayType());
                        // Double amount = val.getTotalAmount();

                        if (StringUtils.equals("1", payType)) {
                            productTotal.setTotalCashCounter(productTotal
                                    .getTotalCashCounter()
                                    + val.getCashAmount());
                        } else if (StringUtils.equals("2", payType)) {
                            productTotal.setTotalCheckGaf(productTotal
                                    .getTotalCheckGaf() + val.getCheckAmount());
                        } else if (StringUtils.equals("3", payType)) {
                            productTotal.setTotalCardGaf(productTotal
                                    .getTotalCardGaf() + val.getCardAmount());
                        } else if (StringUtils.equals("4", payType)) {
                            productTotal.setTotalNonCash(productTotal
                                    .getTotalNonCash() + val.getNcashAmount());
                        }
                    }
                }
            }
        }
    }

    private void putDcrOthersToConsolidatedData(Long dcrId,
            ConsolidatedData consolidatedData) throws ServiceException {
        try {
            List<DCROther> dcrOthers = dcrBalancingToolDao
                    .getDcrOthersByDcrId(dcrId);

            if (CollectionUtils.isNotEmpty(dcrOthers)) {
                List<DCROtherBO> dcrOtherBOs = new ArrayList<DCROtherBO>();
                for (DCROther dcrOther : dcrOthers) {
                    DCROtherBO bo = new DCROtherBO();
                    BeanUtils.copyProperties(bo, dcrOther);
                    dcrOtherBOs.add(bo);
                }
                consolidatedData.setDcrOthers(dcrOtherBOs);
            }
        } catch (IllegalAccessException e) {
            throw new ServiceException(e);
        } catch (InvocationTargetException e) {
            throw new ServiceException(e);
        } catch (WMSDaoException e) {
            throw new ServiceException(e);
        }
    }

    @SuppressWarnings("unchecked")
    private void putAndReplaceAttachments(AttachmentBO attachment,
            String userId, Map<String, Object> userToAttachmentsMap) {

        List<AttachmentBO> userAttachments = (List<AttachmentBO>) userToAttachmentsMap
                .get(userId);
        if (CollectionUtils.isNotEmpty(userAttachments)) {
            userAttachments.add(attachment);
        } else {
            userAttachments = new ArrayList<AttachmentBO>();
            userAttachments.add(attachment);
        }

        userToAttachmentsMap.remove(userId);

        Collections.sort(userAttachments);
        userToAttachmentsMap.put(userId, userAttachments);
    }

    private void associateAttachmentsToConsolidatedData(List<DCRFile> vdsFiles,
            Long dcrId, ConsolidatedData consolidatedData)
            throws ServiceException {

        // The expected keys are the userId of the uploader/cashier
        // The expected values are in the form of List
        Map<String, Object> userToAttachmentsMap = new HashMap<String, Object>();

        Map<String, String> userIdShortNameMap = consolidatedData
                .getUserIdShortNameMap();

        List<AttachmentBO> attachments = attachmentService
                .getAttachmentsByDcr(dcrId);

        if (CollectionUtils.isNotEmpty(attachments)) {
            for (AttachmentBO attachment : attachments) {
                String userId = StringUtils.lowerCase(attachment
                        .getUploaderId());
                putAndReplaceAttachments(attachment, userId,
                        userToAttachmentsMap);

                if (StringUtils.isEmpty(userIdShortNameMap.get(userId))) {
                    String shortName = StringUtils.upperCase(attachment
                            .getShortName());
                    userIdShortNameMap.put(userId, shortName);
                }
            }
        }
        if (CollectionUtils.isNotEmpty(vdsFiles)) {
            for (DCRFile dcrFile : vdsFiles) {
                String docTypeId = dcrFile.getDocTypeId();
                if (StringUtils.containsIgnoreCase(docTypeId, "Vault")
                        || StringUtils.containsIgnoreCase(docTypeId, "VDIL")) {
                    String userId = StringUtils.lowerCase(dcrFile
                            .getAssignedUser());

                    AttachmentBO attachment = new AttachmentBO();
                    attachment
                            .setDescription("Validated Vault Daily Inventory List (DAY2)");
                    attachment.setFilename("VALIDATED_VDIL_DAY2_"
                            + StringUtils.upperCase(userId) + "."
                            + WMSConstants.FILE_TYPE_TIFF);
                    attachment.setFileType("tiff");
                    attachment.setUploaderId(userId);
                    attachment.setFileUrl("viewfile/view.html?fileId="
                            + dcrFile.getFileId());
                    attachment.setUploadDate(dcrFile.getDateCreated());

                    // attachment.setRecordId(vdil.getId());
                    // attachment.setRecordLocation("DCRVDIL");

                    putAndReplaceAttachments(attachment, userId,
                            userToAttachmentsMap);
                }
            }
        }

        consolidatedData.setUserToAttachmentsMap(userToAttachmentsMap);
        consolidatedData.setUserIdShortNameMap(userIdShortNameMap);
    }

    /**
     * Associates the {@link ConsolidatedData} with list of {@link DCRCashierBO}
     * .
     *
     * @param dcrId
     * @param consolidatedData
     * @throws WMSDaoException
     */
    private void populateWithCashierWorkItems(Long dcrId,
            ConsolidatedData consolidatedData) throws WMSDaoException {
        List<DCRCashier> cWorkItems = dcrCashierDao
                .getDCRCashierListByDcrId(dcrId);

        if (CollectionUtils.isNotEmpty(cWorkItems)) {
            List<CashierWorkItem> cashierWorkItems = new ArrayList<CashierWorkItem>();
            for (DCRCashier cWorkItem : cWorkItems) {
                CashierWorkItem item = new CashierWorkItem();
                item.setAcf2id(cWorkItem.getCashier().getAcf2id());
                item.setDcrCashierId(cWorkItem.getId());
                item.setDcrId(dcrId);
                cashierWorkItems.add(item);
            }
            consolidatedData.setCashierWorkItems(cashierWorkItems);
        }
    }

    /**
     * Gets all "checked" row numbers representing "reconciled".
     *
     * @param dspr
     * @return
     */
    List<Integer> getReconciledRowNums(DCRStepProcessorReconciled dspr) {
        List<Integer> rowNumbers = null;
        String reconciledRowNums = dspr.getReconciledRowNums();
        if (StringUtils.isNotEmpty(reconciledRowNums)) {
            rowNumbers = new ArrayList<Integer>();
            String[] rowNums = StringUtils.split(reconciledRowNums, ",");
            for (String rowNum : rowNums) {
                rowNumbers.add(Integer.valueOf(StringUtils.trim(rowNum)));
            }
        }

        return rowNumbers;
    }

    /**
     * Associates {@link DCRStepProcessorNoteBO} objects to
     * {@link ConsolidatedData}.
     *
     * @param dcrId
     * @param consolidatedData
     * @throws WMSDaoException
     * @throws IllegalAccessException
     * @throws InvocationTargetException
     */
    private void associateNotesToConsolidatedData(Long dcrId,
            ConsolidatedData consolidatedData) throws WMSDaoException,
            IllegalAccessException, InvocationTargetException {
        List<DCRStepProcessorNote> notes = dcrDao.getAllNotesForDcr(dcrId);

        if (CollectionUtils.isNotEmpty(notes)) {
            List<DCRStepProcessorNoteBO> notesBO = new ArrayList<DCRStepProcessorNoteBO>();
            for (DCRStepProcessorNote note : notes) {
                DCRStepProcessorNoteBO noteBO = new DCRStepProcessorNoteBO();
                BeanUtils.copyProperties(noteBO, note);
                notesBO.add(noteBO);
            }
            consolidatedData.setNotes(notesBO);
        }
    }

    private void mapCollectionOnBehalfDataToProductTotals(
            ConsolidatedCashierTotals cashierTotal,
            List<CollectionOnBehalfData> cobList) {

        final String ac2id = cashierTotal.getAcf2id();

        List<CollectionOnBehalfData> cobForThisCashier = WMSCollectionUtils
                .findMatchedElements(cobList, new Predicate() {
            @Override
            public boolean evaluate(Object object) {
                CollectionOnBehalfData data = (CollectionOnBehalfData) object;
                return StringUtils.equalsIgnoreCase(ac2id,
                        data.getAcf2id());
            }
        });

        List<ConsolidatedCashierProductTotals> productTotals = cashierTotal
                .getProductTotals();

        List<ConsolidatedCashierProductTotals> cobProductTotals = WMSCollectionUtils
                .findMatchedElements(productTotals, new Predicate() {
            @Override
            public boolean evaluate(Object object) {
                ConsolidatedCashierProductTotals data = (ConsolidatedCashierProductTotals) object;
                ProductType productType = data.getProductType();
                return ((ProductType.OTHER_DOLLAR.equals(productType)) || (ProductType.OTHER_PESO
                        .equals(productType)));
            }
        });

        for (ConsolidatedCashierProductTotals productTotal : cobProductTotals) {
            for (CollectionOnBehalfData data : cobForThisCashier) {
                if (productTotal.getProductType().getId()
                        .equals(data.getProductTypeId())) {
                    productTotal.setTotalCashCounter(data
                            .getCashCounterAmount());
                    productTotal.setTotalCashNonCounter(data
                            .getCashNonCounterAmount());
                    productTotal.setTotalCheckLocal(data.getChequeLocal());
                    productTotal
                            .setTotalCheckRegional(data.getChequeRegional());
                    productTotal.setTotalCheckOnUs(data.getChequeOnUs());
                    productTotal.setTotalCheckNonCounter(data
                            .getChequeNonCounter());
                    productTotal
                            .setTotalUsCheckOutPh(data.getChequeUsDownOut());
                    productTotal.setTotalUsCheckInManila(data
                            .getChequeUsDrawnMla());
                    productTotal.setTotalDollarCheque(data.getChequeDollar());
                }
            }
        }
    }

    private void mapExcemptionsToProductTotals(
            ConsolidatedCashierTotals cashierTotal, List<Excemption> excemptions) {
        List<ConsolidatedCashierProductTotals> productTotals = cashierTotal
                .getProductTotals();

        for (ConsolidatedCashierProductTotals productTotal : productTotals) {
            double totalReversal = 0.0;
            double worksiteAmount = 0.0;
            for (Excemption xmp : excemptions) {
                if (xmp.getProductTypeId().equals(
                        productTotal.getProductType().getId())) {

                    Double reversalAmount = xmp.getReversalAmount();
                    if (reversalAmount != null) {
                        totalReversal += reversalAmount;
                    }

                    double xmpWorksiteAmount = xmp.getWorksiteAmount();
                    // Change according to Healthcheck
                    // *Problem as == is inconsistent in checking floating points
                    // *Change to compare to check value of two double amounts
                    //   NOTE: doubleToRawIntBits was recommended but used compare
                    //    as dev would easily understand method without looking up java notes
                    //    and compare method also uses doubleToLongBits
                    if (Double.compare(xmpWorksiteAmount, worksiteAmount) == 0) {
                        worksiteAmount = xmpWorksiteAmount;
                    }
                }
            }

            productTotal.setExemptionsWorksite(worksiteAmount);
            productTotal.setTotalReversalAmount(totalReversal + worksiteAmount);
        }
    }

    private List<Excemption> getExcemptionsPerCashier(
            List<Excemption> excemptions, final String userId) {
        return WMSCollectionUtils.findMatchedElements(excemptions,
                new Predicate() {
            @Override
            public boolean evaluate(Object object) {
                Excemption excemption = (Excemption) object;
                return userId.equalsIgnoreCase(excemption.getAcf2id());
            }
        });
    }

    private List<NonPostedTransaction> getDollarsOnlyDTR(
            List<NonPostedTransaction> nonPostedTransactions) {

        return WMSCollectionUtils.findMatchedElements(nonPostedTransactions,
                new Predicate() {
            @Override
            public boolean evaluate(Object object) {
                NonPostedTransaction trnx = (NonPostedTransaction) object;
                return "USD".equalsIgnoreCase(trnx
                        .getPaymentCurrencyStr());
            }
        });
    }

    private List<NonPostedTransaction> getPesosOnlyDTR(
            List<NonPostedTransaction> nonPostedTransactions) {

        return WMSCollectionUtils.findMatchedElements(nonPostedTransactions,
                new Predicate() {
            @Override
            public boolean evaluate(Object object) {
                NonPostedTransaction trnx = (NonPostedTransaction) object;
                return "PHP".equalsIgnoreCase(trnx
                        .getPaymentCurrencyStr());
            }
        });
    }

    private double getTotals(List<NonPostedTransaction> nonPostedTransactions) {
        double totals = 0.0;

        if (CollectionUtils.isNotEmpty(nonPostedTransactions)) {
            for (NonPostedTransaction npt : nonPostedTransactions) {
                totals += npt.getAmount();
            }
        }

        return totals;
    }

    private void mapDTRToProductTotals(ConsolidatedCashierTotals cashierTotal,
            List<NonPostedTransaction> nonPostedTransactions) {
        List<ConsolidatedCashierProductTotals> productTotals = cashierTotal
                .getProductTotals();

        ConsolidatedCashierProductTotals slocpiPesoTradVulProdTotals = (ConsolidatedCashierProductTotals) CollectionUtils
                .find(productTotals, new Predicate() {
            @Override
            public boolean evaluate(Object object) {
                ConsolidatedCashierProductTotals ccpt = (ConsolidatedCashierProductTotals) object;
                return ProductType.SLOCPI_P_TRADVUL.equals(ccpt
                        .getProductType());
            }
        });

        ConsolidatedCashierProductTotals slocpiDollarTradVulProdTotals = (ConsolidatedCashierProductTotals) CollectionUtils
                .find(productTotals, new Predicate() {
            @Override
            public boolean evaluate(Object object) {
                ConsolidatedCashierProductTotals ccpt = (ConsolidatedCashierProductTotals) object;
                return ProductType.SLOCPI_D_TRADVUL.equals(ccpt
                        .getProductType());
            }
        });

        ConsolidatedCashierProductTotals slgfiDollarTradVulProdTotals = (ConsolidatedCashierProductTotals) CollectionUtils
                .find(productTotals, new Predicate() {
            @Override
            public boolean evaluate(Object object) {
                ConsolidatedCashierProductTotals ccpt = (ConsolidatedCashierProductTotals) object;
                return ProductType.GREPA_D_TRADVUL.equals(ccpt
                        .getProductType());
            }
        });

        ConsolidatedCashierProductTotals slgfiPesoTradVulProdTotals = (ConsolidatedCashierProductTotals) CollectionUtils
                .find(productTotals, new Predicate() {
            @Override
            public boolean evaluate(Object object) {
                ConsolidatedCashierProductTotals ccpt = (ConsolidatedCashierProductTotals) object;
                return ProductType.GREPA_P_TRADVUL.equals(ccpt
                        .getProductType());
            }
        });

        List<NonPostedTransaction> newBusinessIndividualDTR = WMSCollectionUtils
                .findMatchedElements(nonPostedTransactions, new Predicate() {
            @Override
            public boolean evaluate(Object object) {
                NonPostedTransaction trnx = (NonPostedTransaction) object;

                boolean isMatch = "IL".equals(trnx.getProductCode());
                isMatch = isMatch
                        && ("I".equals(trnx.getTransactionType())
                        || "A".equals(trnx.getTransactionType()) || "C"
                        .equals(trnx.getTransactionType()));

                return isMatch;
            }
        });

        double newBusinessIndividualDollarTotal = getTotals(getDollarsOnlyDTR(newBusinessIndividualDTR));
        slocpiDollarTradVulProdTotals
                .setTotalNewBusinessIndividual(newBusinessIndividualDollarTotal);

        double newBusinessIndividualPesoTotal = getTotals(getPesosOnlyDTR(newBusinessIndividualDTR));
        slocpiPesoTradVulProdTotals
                .setTotalNewBusinessIndividual(newBusinessIndividualPesoTotal);

        List<NonPostedTransaction> renewalIndividualDTR = WMSCollectionUtils
                .findMatchedElements(nonPostedTransactions, new Predicate() {
            @Override
            public boolean evaluate(Object object) {
                NonPostedTransaction trnx = (NonPostedTransaction) object;

                boolean isMatch = "IL".equals(trnx.getProductCode())
                        && "R".equals(trnx.getTransactionType());

                return isMatch;
            }
        });

        double renewalIndividualDollarTotal = getTotals(getDollarsOnlyDTR(renewalIndividualDTR));
        slocpiDollarTradVulProdTotals
                .setTotalIndividualRenewal(renewalIndividualDollarTotal);

        double renewalIndividualPesoTotal = getTotals(getPesosOnlyDTR(renewalIndividualDTR));
        slocpiPesoTradVulProdTotals
                .setTotalIndividualRenewal(renewalIndividualPesoTotal);

        List<NonPostedTransaction> newBusinessVariableDTR = WMSCollectionUtils
                .findMatchedElements(nonPostedTransactions, new Predicate() {
            @Override
            public boolean evaluate(Object object) {
                NonPostedTransaction trnx = (NonPostedTransaction) object;

                boolean isMatch = "VL".equals(trnx.getProductCode())
                        && "VI".equals(trnx.getTransactionType());

                return isMatch;
            }
        });

        double newBusinessVariableDollarTotal = getTotals(getDollarsOnlyDTR(newBusinessVariableDTR));
        slocpiDollarTradVulProdTotals
                .setTotalNewBusinessVariable(newBusinessVariableDollarTotal);

        double newBusinessVariablePesoTotal = getTotals(getPesosOnlyDTR(newBusinessVariableDTR));
        slocpiPesoTradVulProdTotals
                .setTotalNewBusinessVariable(newBusinessVariablePesoTotal);

        List<NonPostedTransaction> renewalVariableDTR = WMSCollectionUtils
                .findMatchedElements(nonPostedTransactions, new Predicate() {
            @Override
            public boolean evaluate(Object object) {
                NonPostedTransaction trnx = (NonPostedTransaction) object;

                boolean isMatch = "VL".equals(trnx.getProductCode())
                        && "VR".equals(trnx.getTransactionType());

                return isMatch;
            }
        });

        double renewalVariableDollarTotal = getTotals(getDollarsOnlyDTR(renewalVariableDTR));
        slocpiDollarTradVulProdTotals
                .setTotalVariableRenewal(renewalVariableDollarTotal);

        double renewalVariablePesoTotal = getTotals(getPesosOnlyDTR(renewalVariableDTR));
        slocpiPesoTradVulProdTotals
                .setTotalVariableRenewal(renewalVariablePesoTotal);

        List<NonPostedTransaction> slgfiNewBusinessIndividualDTR = WMSCollectionUtils
                .findMatchedElements(nonPostedTransactions, new Predicate() {
            @Override
            public boolean evaluate(Object object) {
                NonPostedTransaction trnx = (NonPostedTransaction) object;

                boolean isMatch = "RL".equals(trnx.getProductCode())
                        && "RI".equals(trnx.getTransactionType());

                return isMatch;
            }
        });

        double slgfiNewBusinessIndividualDollarTotal = getTotals(getDollarsOnlyDTR(slgfiNewBusinessIndividualDTR));
        slgfiDollarTradVulProdTotals
                .setTotalNewBusinessIndividual(slgfiNewBusinessIndividualDollarTotal);

        double slgfiNewBusinessIndividualPesoTotal = getTotals(getPesosOnlyDTR(slgfiNewBusinessIndividualDTR));
        slgfiPesoTradVulProdTotals
                .setTotalNewBusinessIndividual(slgfiNewBusinessIndividualPesoTotal);

        List<NonPostedTransaction> slgfiRenewalIndividualDTR = WMSCollectionUtils
                .findMatchedElements(nonPostedTransactions, new Predicate() {
            @Override
            public boolean evaluate(Object object) {
                NonPostedTransaction trnx = (NonPostedTransaction) object;

                boolean isMatch = "RL".equals(trnx.getProductCode())
                        && "RR".equals(trnx.getTransactionType());

                return isMatch;
            }
        });

        double slgfiRenewalIndividualDollarTotal = getTotals(getDollarsOnlyDTR(slgfiRenewalIndividualDTR));
        slgfiDollarTradVulProdTotals
                .setTotalIndividualRenewal(slgfiRenewalIndividualDollarTotal);

        double slgfiRenewalIndividualPesoTotal = getTotals(getPesosOnlyDTR(slgfiRenewalIndividualDTR));
        slgfiPesoTradVulProdTotals
                .setTotalIndividualRenewal(slgfiRenewalIndividualPesoTotal);

        List<NonPostedTransaction> slgfiNewBusinessVariableDTR = WMSCollectionUtils
                .findMatchedElements(nonPostedTransactions, new Predicate() {
            @Override
            public boolean evaluate(Object object) {
                NonPostedTransaction trnx = (NonPostedTransaction) object;

                boolean isMatch = "RV".equals(trnx.getProductCode())
                        && "VB".equals(trnx.getTransactionType());

                return isMatch;
            }
        });

        double slgfiNewBusinessVariableDollarTotal = getTotals(getDollarsOnlyDTR(slgfiNewBusinessVariableDTR));
        slgfiDollarTradVulProdTotals
                .setTotalNewBusinessVariable(slgfiNewBusinessVariableDollarTotal);

        double slgfiNewBusinessVariablePesoTotal = getTotals(getPesosOnlyDTR(slgfiNewBusinessVariableDTR));
        slgfiPesoTradVulProdTotals
                .setTotalNewBusinessVariable(slgfiNewBusinessVariablePesoTotal);

        List<NonPostedTransaction> slgfiRenewalVariableDTR = WMSCollectionUtils
                .findMatchedElements(nonPostedTransactions, new Predicate() {
            @Override
            public boolean evaluate(Object object) {
                NonPostedTransaction trnx = (NonPostedTransaction) object;

                boolean isMatch = "RV".equals(trnx.getProductCode())
                        && "VV".equals(trnx.getTransactionType());

                return isMatch;
            }
        });

        double slgfiRenewalVariableDollarTotal = getTotals(getDollarsOnlyDTR(slgfiRenewalVariableDTR));
        slgfiDollarTradVulProdTotals
                .setTotalVariableRenewal(slgfiRenewalVariableDollarTotal);

        double slgfiRenewalVariablePesoTotal = getTotals(getPesosOnlyDTR(slgfiRenewalVariableDTR));
        slgfiPesoTradVulProdTotals
                .setTotalVariableRenewal(slgfiRenewalVariablePesoTotal);

        List<NonPostedTransaction> slocpiNonPolicyDTR = WMSCollectionUtils
                .findMatchedElements(nonPostedTransactions, new Predicate() {
            @Override
            public boolean evaluate(Object object) {
                NonPostedTransaction trnx = (NonPostedTransaction) object;
                return "N".equals(trnx.getTransactionType())
                        && StringUtils.equalsIgnoreCase("SLOCP",
                        trnx.getCompanyCode())
                        && (trnx.getPaymentPostStatus() != 1);

            }
        });

        double nonPolicyDollarTotal = getTotals(getDollarsOnlyDTR(slocpiNonPolicyDTR));
        slocpiDollarTradVulProdTotals.setTotalNonPolicy(nonPolicyDollarTotal);

        double nonPolicyPesoTotal = getTotals(getPesosOnlyDTR(slocpiNonPolicyDTR));
        slocpiPesoTradVulProdTotals.setTotalNonPolicy(nonPolicyPesoTotal);

        List<NonPostedTransaction> slocpiWorksiteNonPostedDTR = WMSCollectionUtils
                .findMatchedElements(nonPostedTransactions, new Predicate() {
            @Override
            public boolean evaluate(Object object) {
                NonPostedTransaction trnx = (NonPostedTransaction) object;
                boolean isMatch = "W".equals(trnx.getTransactionType())
                        && StringUtils.equalsIgnoreCase("SLOCP",
                        trnx.getCompanyCode());

                return isMatch;
            }
        });

        double worksiteNonPostedDollarTotal = getTotals(getDollarsOnlyDTR(slocpiWorksiteNonPostedDTR));
        slocpiDollarTradVulProdTotals
                .setTotalWorksiteNonPosted(worksiteNonPostedDollarTotal);

        double worksiteNonPostedPesoTotal = getTotals(getPesosOnlyDTR(slocpiWorksiteNonPostedDTR));
        slocpiPesoTradVulProdTotals
                .setTotalWorksiteNonPosted(worksiteNonPostedPesoTotal);

        List<NonPostedTransaction> slgfiNonPolicyDTR = WMSCollectionUtils
                .findMatchedElements(nonPostedTransactions, new Predicate() {
            @Override
            public boolean evaluate(Object object) {
                NonPostedTransaction trnx = (NonPostedTransaction) object;
                return "N".equals(trnx.getTransactionType())
                        && StringUtils.equalsIgnoreCase("SLGFI",
                        trnx.getCompanyCode())
                        && (trnx.getPaymentPostStatus() != 1);

            }
        });

        double slgfiNonPolicyDollarTotal = getTotals(getDollarsOnlyDTR(slgfiNonPolicyDTR));
        slgfiDollarTradVulProdTotals
                .setTotalNonPolicy(slgfiNonPolicyDollarTotal);

        double slgfiNonPolicyPesoTotal = getTotals(getPesosOnlyDTR(slgfiNonPolicyDTR));
        slgfiPesoTradVulProdTotals.setTotalNonPolicy(slgfiNonPolicyPesoTotal);

        List<NonPostedTransaction> slgfiWorksiteNonPostedDTR = WMSCollectionUtils
                .findMatchedElements(nonPostedTransactions, new Predicate() {
            @Override
            public boolean evaluate(Object object) {
                NonPostedTransaction trnx = (NonPostedTransaction) object;
                boolean isMatch = "W".equals(trnx.getTransactionType())
                        && StringUtils.equalsIgnoreCase("SLGFI",
                        trnx.getCompanyCode());

                return isMatch;
            }
        });

        double slgfiWorksiteNonPostedDollarTotal = getTotals(getDollarsOnlyDTR(slgfiWorksiteNonPostedDTR));
        slgfiDollarTradVulProdTotals
                .setTotalWorksiteNonPosted(slgfiWorksiteNonPostedDollarTotal);

        double slgfiWorksiteNonPostedPesoTotal = getTotals(getPesosOnlyDTR(slgfiWorksiteNonPostedDTR));
        slgfiPesoTradVulProdTotals
                .setTotalWorksiteNonPosted(slgfiWorksiteNonPostedPesoTotal);
    }

    private void mapIpacValuesToCorrespondingCashier(
            ConsolidatedCashierTotals cashierTotal,
            List<DCRIpacValue> ipacValues) {

        final String acf2id = cashierTotal.getAcf2id();

        List<ConsolidatedCashierProductTotals> productTotals = new ArrayList<ConsolidatedCashierProductTotals>();
        for (ProductType type : ProductType.values()) {
            ConsolidatedCashierProductTotals productTotal = new ConsolidatedCashierProductTotals();
            productTotal.setProductType(type);
            productTotals.add(productTotal);
        }

        List<DCRIpacValue> valuesForCashier = WMSCollectionUtils.findMatchedElements(ipacValues, new Predicate() {
            @Override
            public boolean evaluate(Object object) {
                DCRIpacValue value = (DCRIpacValue) object;
                return StringUtils.equalsIgnoreCase(acf2id, value.getAcf2id());
            }
        });
        this.mapIpacValuesToProductTotals(productTotals, valuesForCashier);

        try {
            this.mapSessionTotalsToProductTotals(cashierTotal, acf2id, productTotals);
        } catch (ServiceException e) {
            // Change according to Healthcheck
            // *Problem was identified as inconsistent logging
            // *Change add exceptionobject in logging
            LOGGER.error("Problem in mapping values to cshier: ", e);
        }

        cashierTotal.setProductTotals(productTotals);
    }

    private void mapSessionTotalsToProductTotals(
            ConsolidatedCashierTotals cashierTotal, final String userId,
            List<ConsolidatedCashierProductTotals> productTotals) throws ServiceException {

        List<ConsolidatedCashierProductTotals> slocpiProductTotals = WMSCollectionUtils
                .findMatchedElements(productTotals, new Predicate() {
            @Override
            public boolean evaluate(Object object) {
                ConsolidatedCashierProductTotals pt = (ConsolidatedCashierProductTotals) object;
                return (ProductType.SLOCPI_D_TRADVUL.equals(pt
                        .getProductType()) || ProductType.SLOCPI_P_TRADVUL
                        .equals(pt.getProductType()));
            }
        });



        for (ConsolidatedCashierProductTotals slocpiProductTotal : slocpiProductTotals) {
            double sessionTotal = 0.0;

            ProductType productType = slocpiProductTotal.getProductType();
            String userIdWithSuffix = StringUtils.upperCase(userId
                    + INGENIUM_USERID_SUFFIX);

            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("Querying Session Total in Ingenium for SLOCPI for user "
                        + userIdWithSuffix);
            }

            double sessionTotalWithSuffix = queryForSlocpiSessionTotal(
                    cashierTotal, userIdWithSuffix, productType.getCurrency());

            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("Querying Session Total in Ingenium for SLOCPI for user "
                        + StringUtils.upperCase(userId));
            }

            double sessionTotalWithoutSuffix = queryForSlocpiSessionTotal(
                    cashierTotal, StringUtils.upperCase(userId),
                    productType.getCurrency());

            if (sessionTotalWithSuffix > sessionTotalWithoutSuffix) {
                sessionTotal = sessionTotalWithSuffix;
            } else {
                sessionTotal = sessionTotalWithoutSuffix;
            }

            slocpiProductTotal.setSessionTotal(sessionTotal);
        }



        List<ConsolidatedCashierProductTotals> slgfiProductTotals = WMSCollectionUtils
                .findMatchedElements(productTotals, new Predicate() {
            @Override
            public boolean evaluate(Object object) {
                ConsolidatedCashierProductTotals pt = (ConsolidatedCashierProductTotals) object;
                return (ProductType.GREPA_D_TRADVUL.equals(pt
                        .getProductType()) || ProductType.GREPA_P_TRADVUL
                        .equals(pt.getProductType()));
            }
        });

        for (ConsolidatedCashierProductTotals slgfiProductTotal : slgfiProductTotals) {
            double sessionTotal = 0.0;

            ProductType productType = slgfiProductTotal.getProductType();
            String userIdWithSuffix = StringUtils.upperCase(userId
                    + INGENIUM_USERID_SUFFIX);

            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("Querying Session Total in Ingenium for SLGFI for user "
                        + userIdWithSuffix);
            }

            double sessionTotalWithSuffix = queryForSlgfiSessionTotal(
                    cashierTotal, userIdWithSuffix, productType.getCurrency());

            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("Querying Session Total in Ingenium for SLGFI for user "
                        + StringUtils.upperCase(userId));
            }

            double sessionTotalWithoutSuffix = queryForSlgfiSessionTotal(
                    cashierTotal, StringUtils.upperCase(userId),
                    productType.getCurrency());

            if (sessionTotalWithSuffix > sessionTotalWithoutSuffix) {
                sessionTotal = sessionTotalWithSuffix;
            } else {
                sessionTotal = sessionTotalWithoutSuffix;
            }

            slgfiProductTotal.setSessionTotal(sessionTotal);
        }

    }

    private double queryForSlgfiSessionTotal(
            ConsolidatedCashierTotals cashierTotal, final String userId,
            Currency currency) throws ServiceException {
        UserCollectionsInfo info = new UserCollectionsInfo();
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String processDateStr = df
                .format(cashierTotal.getParent().getDcrDate());

        info.setProcessDate(processDateStr);
        info.setUserId(userId);
        info.setCustomerCenterId(cashierTotal.getParent().getCcId());

        if (Currency.PHP.equals(currency)) {
            info.setCurrencyId("PS");
        } else if (Currency.USD.equals(currency)) {
            info.setCurrencyId("US");
        }

        List<String> outMessages = new ArrayList<String>();

        double sessionTotal = 0.0D;

        SessionTotal sessTotal = dcrIngeniumService.retrieveDCRSessionTotal(info, "SLGFI");
        if (null != sessTotal) {
            LOGGER.info("retrieveDCRSessionTotal: " + sessTotal.toString());
            sessionTotal = sessTotal.getCollectionTotal();
        } else {
            LOGGER.info("retrieving from Ingenium Webservice");
            info = slgfiUserCollectionsTotalUtil.userCollectionsInquiry(info,
                    outMessages);

            sessionTotal = info.getTotalCollectionAmount();
        }

        return sessionTotal;
    }

    private double queryForSlocpiSessionTotal(
            ConsolidatedCashierTotals cashierTotal, final String acf2id,
            Currency currency) throws ServiceException {
        UserCollectionsInfo info = new UserCollectionsInfo();
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String processDateStr = df
                .format(cashierTotal.getParent().getDcrDate());

        info.setProcessDate(processDateStr);
        info.setUserId(acf2id);
        info.setCustomerCenterId(cashierTotal.getParent().getCcId());

        if (Currency.PHP.equals(currency)) {
            info.setCurrencyId("PS");
        } else if (Currency.USD.equals(currency)) {
            info.setCurrencyId("US");
        }

        List<String> outMessages = new ArrayList<String>();

        double sessionTotal = 0.0D;

        SessionTotal sessTotal = dcrIngeniumService.retrieveDCRSessionTotal(info, "SLOCP");
        if (null != sessTotal) {
            LOGGER.info("retrieveDCRSessionTotal: " + sessTotal.toString());
            sessionTotal = sessTotal.getCollectionTotal();
        } else {
            LOGGER.info("retrieving from Ingenium Webservice");
            info = slocpiUserCollectionsTotalUtil.userCollectionsInquiry(info,
                    outMessages);

            sessionTotal = info.getTotalCollectionAmount();
        }

        return sessionTotal;
    }

    private void mapIpacValuesToProductTotals(
            List<ConsolidatedCashierProductTotals> productTotals,
            List<DCRIpacValue> valuesForCashier) {

        for (DCRIpacValue value : valuesForCashier) {
            String paySubCurr = value.getPaySubCurr();
            String prodCode = value.getProdCode();
            String payCode = value.getPayCode();
            // String paySubCode = value.getPaySubCode();
            double amount = value.getAmount();

            ProductType type = ProductType.prodCodeCurrencyToProductType(
                    prodCode, paySubCurr);

            ConsolidatedCashierProductTotals mappedPT = null;
            if (type != null) {
                for (ConsolidatedCashierProductTotals pt : productTotals) {
                    if (type.equals(pt.getProductType())) {
                        mappedPT = pt;
                    }
                }
            }

            if (mappedPT != null) {
                if ("NCSH".equalsIgnoreCase(payCode)) {
                    double totalNonCashAmount = mappedPT.getTotalNonCash()
                            + amount;
                    mappedPT.setTotalNonCash(totalNonCashAmount);

                    if (ProductType.GREPA_D_TRADVUL.equals(type)
                            || ProductType.GREPA_P_TRADVUL.equals(type)
                            || ProductType.SLOCPI_D_TRADVUL.equals(type)
                            || ProductType.SLOCPI_P_TRADVUL.equals(type)) {
                        mappedPT.setTotalNonCashFromDTR(totalNonCashAmount);
                    }

                } else if ("CSH".equalsIgnoreCase(payCode)) {
                    this.mapCashValueToTotals(mappedPT, value);
                } else if ("CHK".equalsIgnoreCase(payCode)) {
                    this.mapCheckValueToTotals(mappedPT, value);
                } else if ("CRD".equalsIgnoreCase(payCode)) {
                    this.mapCardValueToTotals(mappedPT, value);
                }
            }
        }
    }

    private void mapCheckValueToTotals(
            ConsolidatedCashierProductTotals mappedPT,
            DCRIpacValue value) {

        String paySubCode = value.getPaySubCode();
        double amount = value.getAmount();

        if ("R/C".equalsIgnoreCase(paySubCode)) {
            mappedPT.setTotalCheckRegional(mappedPT.getTotalCheckRegional() + amount);
        } else if ("L/C".equalsIgnoreCase(paySubCode)) {
            mappedPT.setTotalCheckLocal(mappedPT.getTotalCheckLocal() + amount);
        } else if ("O/C".equalsIgnoreCase(paySubCode)) {
            mappedPT.setTotalCheckOT(mappedPT.getTotalCheckOT() + amount);
        } else if ("PMO".equalsIgnoreCase(paySubCode)) {
            ProductType productType = mappedPT.getProductType();
            if (productType != null
                    && (Company.SLAMCID.equals(productType.getCompany())
                    || Company.SLAMCIP.equals(productType.getCompany()))) {
                mappedPT.setTotalCheckNonCounter(mappedPT.getTotalCheckNonCounter() + amount);
            } else {
                mappedPT.setTotalPmo(mappedPT.getTotalPmo() + amount);
            }
        } else if ("OUC".equalsIgnoreCase(paySubCode)) {
            mappedPT.setTotalCheckOnUs(mappedPT.getTotalCheckOnUs() + amount);
        } else if ("CDM".equalsIgnoreCase(paySubCode)) {
            mappedPT.setTotalUsCheckInManila(mappedPT.getTotalUsCheckInManila() + amount);
        } else if ("COP".equalsIgnoreCase(paySubCode)) {
            mappedPT.setTotalUsCheckOutPh(mappedPT.getTotalUsCheckOutPh() + amount);
        } else if ("DCK".equalsIgnoreCase(paySubCode)) {
            mappedPT.setTotalDollarCheque(mappedPT.getTotalDollarCheque() + amount);
        } else if ("BCP".equalsIgnoreCase(paySubCode)
                || "BCU".equalsIgnoreCase(paySubCode)) {
            mappedPT.setTotalCheckNonCounter(mappedPT.getTotalCheckNonCounter() + amount);
        } else if ("CGD".equalsIgnoreCase(paySubCode)
                || "CGL".equalsIgnoreCase(paySubCode)
                || "GYR".equalsIgnoreCase(paySubCode)) {
            mappedPT.setPaymentFromInsurer(mappedPT.getPaymentFromInsurer() + amount);
        } else if ("MF".equalsIgnoreCase(paySubCode)) { // Added for PCO
            mappedPT.setTotalMf(mappedPT.getTotalMf() + amount);
        } else if ("SPC".equalsIgnoreCase(paySubCode)) { // Added for PCO Invest
            mappedPT.setTotalPcoInvest(mappedPT.getTotalPcoInvest() + amount);
        } else {
            mappedPT.setTotalCheckNonCounter(mappedPT.getTotalCheckNonCounter() + amount);
        }
    }

    private void mapCashValueToTotals(
            ConsolidatedCashierProductTotals mappedPT,
            DCRIpacValue value) {

        String paySubCode = value.getPaySubCode();
        double amount = value.getAmount();

        if ("CPH".equalsIgnoreCase(paySubCode)
                || "UCH".equalsIgnoreCase(paySubCode)) {
            mappedPT.setTotalCashCounter(mappedPT.getTotalCashCounter() + amount);
        } else if ("EPS".equalsIgnoreCase(paySubCode)) {
            mappedPT.setTotalCashEpsBpi(mappedPT.getTotalCashEpsBpi() + amount);
        } else if ("MF".equalsIgnoreCase(paySubCode)) { // Added for PCO
            mappedPT.setTotalMf(mappedPT.getTotalMf() + amount);
        } else if ("SPC".equalsIgnoreCase(paySubCode)) { // Added for PCO Invest
            mappedPT.setTotalPcoInvest(mappedPT.getTotalPcoInvest() + amount);
        } else {
            mappedPT.setTotalCashNonCounter(mappedPT.getTotalCashNonCounter() + amount);
        }
    }

    private void mapCardValueToTotals(
            ConsolidatedCashierProductTotals mappedPT,
            DCRIpacValue value) {

        String paySubCode = value.getPaySubCode();
        double amount = value.getAmount();

        if ("BPP".equalsIgnoreCase(paySubCode)
                || "BPU".equalsIgnoreCase(paySubCode)) {
            mappedPT.setTotalPosBpi(mappedPT.getTotalPosBpi() + amount);
        } else if ("CPP".equalsIgnoreCase(paySubCode)
                || "CPU".equalsIgnoreCase(paySubCode)) {
            mappedPT.setTotalPosCtb(mappedPT.getTotalPosCtb() + amount);
        } else if ("EPP".equalsIgnoreCase(paySubCode)
                || "EPU".equalsIgnoreCase(paySubCode)) {
            mappedPT.setTotalPosBdo(mappedPT.getTotalPosBdo() + amount);
        } else if ("HPP".equalsIgnoreCase(paySubCode)
                || "HPU".equalsIgnoreCase(paySubCode)) {
            mappedPT.setTotalPosHsbc(mappedPT.getTotalPosHsbc() + amount);
        } else if ("SPP".equalsIgnoreCase(paySubCode)
                || "SPU".equalsIgnoreCase(paySubCode)) {
            mappedPT.setTotalPosScb(mappedPT.getTotalPosScb() + amount);
        } else if ("RPP".equalsIgnoreCase(paySubCode)
                || "RPU".equalsIgnoreCase(paySubCode)) {
            mappedPT.setTotalPosRcbc(mappedPT.getTotalPosRcbc() + amount);
        } else if ("BMP".equalsIgnoreCase(paySubCode)
                || "BMU".equalsIgnoreCase(paySubCode)) {
            mappedPT.setTotalMdsBpi(mappedPT.getTotalMdsBpi() + amount);
        } else if ("CMP".equalsIgnoreCase(paySubCode)
                || "CMU".equalsIgnoreCase(paySubCode)) {
            mappedPT.setTotalMdsCtb(mappedPT.getTotalMdsCtb() + amount);
        } else if ("EMP".equalsIgnoreCase(paySubCode)
                || "EMU".equalsIgnoreCase(paySubCode)) {
            mappedPT.setTotalMdsBdo(mappedPT.getTotalMdsBdo() + amount);
        } else if ("HMP".equalsIgnoreCase(paySubCode)
                || "HMU".equalsIgnoreCase(paySubCode)) {
            mappedPT.setTotalMdsHsbc(mappedPT.getTotalMdsHsbc() + amount);
        } else if ("SMP".equalsIgnoreCase(paySubCode)
                || "SMU".equalsIgnoreCase(paySubCode)) {
            mappedPT.setTotalMdsSbc(mappedPT.getTotalMdsSbc() + amount);
        } else if ("RMP".equalsIgnoreCase(paySubCode)
                || "RMU".equalsIgnoreCase(paySubCode)) {
            mappedPT.setTotalMdsRcbc(mappedPT.getTotalMdsRcbc() + amount);
        } else if ("CMM".equalsIgnoreCase(paySubCode)) {
            mappedPT.setTotalAutoCtb(mappedPT.getTotalAutoCtb() + amount);
        } else if ("SMM".equalsIgnoreCase(paySubCode)) {
            mappedPT.setTotalAutoScb(mappedPT.getTotalAutoScb() + amount);
        } else if ("RAU".equalsIgnoreCase(paySubCode)
                || "RMM".equalsIgnoreCase(paySubCode)) {
            mappedPT.setTotalAutoRcbc(mappedPT.getTotalAutoRcbc() + amount);
        } else if ("ICT".equalsIgnoreCase(paySubCode)) {
            mappedPT.setTotalSunlinkOnline(mappedPT.getTotalSunlinkOnline() + amount);
        } else if ("HSP".equalsIgnoreCase(paySubCode)) {
            mappedPT.setTotalSunlinkHsbcPeso(mappedPT.getTotalSunlinkHsbcPeso() + amount);
        } else if ("HSU".equalsIgnoreCase(paySubCode)) {
            mappedPT.setTotalSunlinkHsbcDollar(mappedPT.getTotalSunlinkHsbcDollar() + amount);
        } else if ("EPS".equalsIgnoreCase(paySubCode)) {
            mappedPT.setTotalEpsBpi(mappedPT.getTotalEpsBpi() + amount);
        } else if ("MF".equalsIgnoreCase(paySubCode)) { // Added for PCO
            mappedPT.setTotalMf(mappedPT.getTotalMf() + amount);
        } else if ("SPC".equalsIgnoreCase(paySubCode)) { // Added for PCO Invest
            mappedPT.setTotalPcoInvest(mappedPT.getTotalPcoInvest() + amount);
        }
    }

    private DCRCashier getCashierWorkItem(String userId,
            List<DCRCashier> cashierItems) {
        if (CollectionUtils.isNotEmpty(cashierItems)) {
            for (DCRCashier cashierItem : cashierItems) {
                String acf2id = cashierItem.getCashier().getAcf2id();
                if (StringUtils.equalsIgnoreCase(userId, acf2id)) {
                    return cashierItem;
                }
            }
        }
        return null;
    }

    private Cashier getCashierInfo(String userId, List<Cashier> cashierList) {
        if (CollectionUtils.isNotEmpty(cashierList)) {
            for (Cashier cashierInfo : cashierList) {
                String acf2id = cashierInfo.getAcf2id();
                if (StringUtils.equalsIgnoreCase(userId, acf2id)) {
                    return cashierInfo;
                }
            }
        }
        return null;
    }

    private List<String> getCashierUserIds(List<DCRCashier> cashierItems) {
        List<String> userIds = null;

        if (CollectionUtils.isNotEmpty(cashierItems)) {
            userIds = new ArrayList<String>();
            for (DCRCashier cashierItem : cashierItems) {
                String acf2id = cashierItem.getCashier().getAcf2id();
                userIds.add(acf2id);
            }
        }
        return userIds;
    }

    public List<String> formatConsolidatedData(ConsolidatedData con) throws ServiceException {

        Field[] fields = ConsolidatedCashierProductTotals.class.getDeclaredFields();
        List<String> list = new ArrayList();

        try {
            for (Field f : fields) {
                f.setAccessible(true);
                // Change according to Healthcheck
                // *Problem was identified as name used in condition
                // *Change was to compare by instance/class type
                if (f.getType() == Double.TYPE) {
                    if (CollectionUtils.isNotEmpty(con.getCashierTotals())) {
                        for (ConsolidatedCashierTotals c : con.getCashierTotals()) {
                            List<ConsolidatedCashierProductTotals> cpts = c.getProductTotals();
                            if (CollectionUtils.isNotEmpty(cpts)) {
                                for (ConsolidatedCashierProductTotals cpt : cpts) {
                                    if (cpt != null) {
                                        StringBuilder tmp = new StringBuilder();
                                        tmp.append(f.getName()).append("~").append(c.getAcf2id())
                                                .append("~").append(c.getCashierFullName())
                                                .append("~").append(cpt.getProductType().getProductCode())
                                                .append("~").append(cpt.getProductType().getProductName())
                                                .append("~").append(cpt.getProductType().getCurrency())
                                                .append("~").append(f.getDouble(cpt))
                                                .append("~").append(cpt.getDcrBalancingToolProductId());
                                        list.add(tmp.toString());
                                    }
                                }
                            } else {
                                System.out.println("No Cashier Totals");
                            }
                        }
                    }
                }
            }
        } catch (IllegalAccessException a) {
            throw new ServiceException(a);
        }
        return list;
    }

    private void moveToNextStep(String userId, Long dcrId, String response,
            String currentQueueName, String nextStatus) throws ServiceException {
        try {
            LOGGER.info("Commence process of DCR with ID " + dcrId + "...");
            DCR dcr = dcrDao.getById(dcrId);

            Date dcrDate = dcr.getDcrDate();
            String docCapSiteId = dcr.getCcId();
            String hubId = dcr.getHubId();

            String dcrDateStr = StringUtils.upperCase(WMSDateUtil
                    .toFormattedDateStr(dcrDate));

            final String wObName = dcrDateStr + docCapSiteId;

            String actionDetails = "";
            boolean checkIfNonWorkingDay = true;
            int ccqaTAT = 0;
            
            if (STATUS_APPROVED_FOR_RECON.equals(nextStatus)) {
                actionDetails = "WF Action: Submit to PPA";
                currentQueueName = managerReconQueues.get(hubId);
              //Added for and MR-WF-16-00112
                ccqaTAT = dcrDao.requiredCompCount();
                int holidaysAndWeekend = dcrDao.countHolidaysAndWeekend(ccqaTAT);
                holidaysAndWeekend = dcrDao.countHolidaysAndWeekend(ccqaTAT + holidaysAndWeekend);
                ccqaTAT = ccqaTAT + holidaysAndWeekend;
                while(checkIfNonWorkingDay) {
                	boolean holidayOrWeekend = dcrDao.checkIfHolidayOrWeekend(ccqaTAT);
                	if(holidayOrWeekend){
                		ccqaTAT = ccqaTAT + 1;
                	} else {
                		checkIfNonWorkingDay = false;
                	}
                }
                
                
            } else if (STATUS_AWAITING_FEEDBACK.equals(nextStatus)) {
                actionDetails = "WF Action: Set to Awaiting Feedback";
                currentQueueName = managerReconQueues.get(hubId);
            } else if (STATUS_FOR_REVIEW.equals(nextStatus)) {
                actionDetails = "WF Action: Submit to CCM";
                currentQueueName = cashierReconQueues.get(hubId);
            } else if (STATUS_FOR_VERIFICATION.equals(nextStatus)) {
                actionDetails = "WF Action: Set to For Verification";
            } else if (STATUS_AWAITING_REQUIREMENTS.equals(nextStatus)) {
                actionDetails = "WF Action: Set to Awaiting Requirements";
            } else if (STATUS_RECONCILED.equals(nextStatus)) {
                actionDetails = "WF Action: Reconciled";
            } else if (STATUS_NOT_VERIFIED.equals(nextStatus)) {
                actionDetails = "WF Action: Set to Not Verified";
            } else if (STATUS_VERIFIED.equals(nextStatus)) {
                actionDetails = "WF Action: Set to Verified";
            } else if (STATUS_REQUIREMENTS_NOT_SUBMITTED.equals(nextStatus)) {
                actionDetails = "WF Action: Set to Reqts Not Submitted";
            } else if (STATUS_REQUIREMENTS_SUBMITTED.equals(nextStatus)) {
                actionDetails = "WF Action: Set to Reqts Submitted";
            }

            if (StringUtils.isNotEmpty(response)
                    && StringUtils.isNotEmpty(currentQueueName)) {
                this.dcrFilenetIntegrationService.approveStep(response,
                        wObName, currentQueueName);
                LOGGER.info("Status of DCR was successfully set to: "
                        + response);
            }

            Date today = cachingService.getWmsDcrSystemDate();

            dcr.setStatus(nextStatus);
            dcr.setUpdateUserId(userId);
            dcr.setUpdateDate(today);
            
          //Added for and MR-WF-16-00112
            if (STATUS_APPROVED_FOR_RECON.equals(nextStatus)){ 
            	dcr.setCcqaTAT(ccqaTAT);
            	boolean checkCcqaSubmitReqDate = dcrDao.updateDcrStatusForCCQA(dcr);
            	//update normally if submit date is not null
            	if (!checkCcqaSubmitReqDate){
            		dcrDao.updateDcrStatus(dcr);
            	}
            }else{
            	dcrDao.updateDcrStatus(dcr);
            }
            
            LOGGER.info("DCR was successfully updated in WMS DB...");

            DCRAuditLog auditLog = new DCRAuditLog();
            auditLog.setActionDetails(actionDetails);

            auditLog.setDcrId(dcrId);
            auditLog.setLogDate(today);
            auditLog.setProcessStatus(nextStatus);
            auditLog.setUserId(userId);
            dcrDao.createLog(auditLog);
            LOGGER.info("WF History Log was created...");

        } catch (WMSDaoException ex) {
            LOGGER.error(ex);
            throw new ServiceException(ex);
        } catch (IntegrationException ex) {
            LOGGER.error(ex);
            throw new ServiceException(ex);
        }

    }
    
    @Override
    public void approveForReconcilation(String userId, Long dcrId,
            List<DCRStepProcessorNoteBO> notes) throws ServiceException {
        saveInlineNotes(userId, dcrId, notes);
        moveToNextStep(userId, dcrId, "Approved for PPA Recon",
                "DCRReviewAndApproval", STATUS_APPROVED_FOR_RECON);
    }

    @Override
    public void awaitingFeedback(String userId, Long dcrId,
            List<DCRStepProcessorNoteBO> notes) throws ServiceException {
        saveInlineNotes(userId, dcrId, notes);
        moveToNextStep(userId, dcrId, STATUS_AWAITING_FEEDBACK,
                "DCRReviewAndApproval", STATUS_AWAITING_FEEDBACK);
    }

    @Override
    public void ppaReconciled(String userId, Long dcrId,
            List<Integer> reconciledRowNums, List<DCRStepProcessorNoteBO> notes)
            throws ServiceException {

        savePPAStepProcessor(userId, dcrId, reconciledRowNums, notes);
        moveToNextStep(userId, dcrId, STATUS_RECONCILED, QUEUE_DCR_PPA_RECON,
                STATUS_RECONCILED);
    }

    @Override
    public void forReviewAndApproval(String userId, Long dcrId,
            List<DCRStepProcessorNoteBO> notes) throws ServiceException {

        saveInlineNotes(userId, dcrId, notes);
        moveToNextStep(userId, dcrId, STATUS_FOR_REVIEW,
                QUEUE_DCR_CASHIER_RECON, STATUS_FOR_REVIEW);
    }

    @Override
    public void forVerification(String userId, Long dcrId,
            List<Integer> reconciledRowNums, List<DCRStepProcessorNoteBO> notes)
            throws ServiceException {
        savePPAStepProcessor(userId, dcrId, reconciledRowNums, notes);
        moveToNextStep(userId, dcrId, STATUS_FOR_VERIFICATION,
                QUEUE_DCR_PPA_RECON, STATUS_FOR_VERIFICATION);
    }

    public void notVerifiedPPA(Long dcrId) throws ServiceException {
        moveToNextStep("auto", dcrId, STATUS_NOT_VERIFIED,
                QUEUE_DCR_PPA_ABEYANCE, STATUS_NOT_VERIFIED);
    }

    public void notVerifiedPPA(String userId, Long dcrId,
            List<Integer> reconciledRowNums, List<DCRStepProcessorNoteBO> notes)
            throws ServiceException {
        savePPAStepProcessor(userId, dcrId, reconciledRowNums, notes);
        moveToNextStep(userId, dcrId, STATUS_NOT_VERIFIED,
                QUEUE_DCR_PPA_ABEYANCE, STATUS_NOT_VERIFIED);
    }

    public void verifiedPPA(String userId, Long dcrId,
            List<Integer> reconciledRowNums, List<DCRStepProcessorNoteBO> notes)
            throws ServiceException {

        saveInlineNotes(userId, dcrId, notes);
        moveToNextStep(userId, dcrId, STATUS_VERIFIED, QUEUE_DCR_PPA_ABEYANCE,
                STATUS_VERIFIED);
    }

    public void reqtsSubmittedPPA(String userId, Long dcrId,
            List<Integer> reconciledRowNums, List<DCRStepProcessorNoteBO> notes)
            throws ServiceException {
        savePPAStepProcessor(userId, dcrId, reconciledRowNums, notes);
        moveToNextStep(userId, dcrId, STATUS_REQUIREMENTS_SUBMITTED,
                QUEUE_DCR_PPA_ABEYANCE, STATUS_REQUIREMENTS_SUBMITTED);
    }

    public void reqtsSubmittedPPA(String userId, Long dcrId)
            throws ServiceException {
        moveToNextStep(userId, dcrId, STATUS_REQUIREMENTS_SUBMITTED,
                QUEUE_DCR_PPA_ABEYANCE, STATUS_REQUIREMENTS_SUBMITTED);
    }

    public void reqtsSubmittedPPA(Long dcrId) throws ServiceException {
        moveToNextStep("auto", dcrId, STATUS_REQUIREMENTS_SUBMITTED,
                QUEUE_DCR_PPA_ABEYANCE, STATUS_REQUIREMENTS_SUBMITTED);
    }

    public void reqtsNotSubmittedPPA(Long dcrId) throws ServiceException {
        moveToNextStep("auto", dcrId, STATUS_REQUIREMENTS_NOT_SUBMITTED,
                QUEUE_DCR_PPA_ABEYANCE, STATUS_REQUIREMENTS_NOT_SUBMITTED);
    }

    public void reqtsNotSubmittedPPA(String userId, Long dcrId,
            List<Integer> reconciledRowNums, List<DCRStepProcessorNoteBO> notes)
            throws ServiceException {
        savePPAStepProcessor(userId, dcrId, reconciledRowNums, notes);
        moveToNextStep(userId, dcrId, STATUS_REQUIREMENTS_NOT_SUBMITTED,
                QUEUE_DCR_PPA_ABEYANCE, STATUS_REQUIREMENTS_NOT_SUBMITTED);
    }

    public void ppaAutoRun() throws ServiceException {
        Date systemDate = cachingService.getWmsDcrSystemDate();

        try {
            boolean isHolidayOrWeekend = dcrDao.checkIfHoliday(systemDate);

            if (!isHolidayOrWeekend) {
                this.checkForUnverifiedDCR();
                this.checkForNonSubmissionOfRequirements();
                this.successfulJobRun(systemDate);
            } else {
                LOGGER.debug("Today is a Weekend or a Holiday");
            }

        } catch (WMSDaoException ex) {
            LOGGER.error(ex);
            ServiceException serviceException = new ServiceException(ex);
            this.failedJobRun(systemDate, serviceException);
            throw serviceException;
        } catch (ServiceException ex) {
            LOGGER.error(ex);
            this.failedJobRun(systemDate, ex);
            throw ex;
        }
    }

    private void failedJobRun(Date today, ServiceException e)
            throws ServiceException {
        try {
            String stackTrace = ExceptionUtil.getStackTrace(e, 1000);
            JobDataErrorLog log = new JobDataErrorLog();
            log.setDateTime(today);
            log.setErrorMessage(stackTrace);
            jobDataDao.createErrorLog(log);

            JobData jobData = new JobData();
            jobData.setId(PPA_ABEYANCE_JOB_ID);
            jobData.setLastRun(today);
            jobData.setSuccessful(false);
            jobDataDao.updateJobData(jobData);

        } catch (WMSDaoException ex1) {
            LOGGER.error(ex1);
            throw new ServiceException(ex1);
        }
    }

    private void successfulJobRun(Date today) throws ServiceException {
        try {
            JobData jobData = new JobData();
            jobData.setId(PPA_ABEYANCE_JOB_ID);
            jobData.setLastRun(today);
            jobData.setSuccessful(true);
            jobDataDao.updateJobData(jobData);
        } catch (WMSDaoException ex1) {
            LOGGER.error(ex1);
            throw new ServiceException(ex1);
        }
    }

    public void checkForNonSubmissionOfRequirements() throws ServiceException {
        try {
            Date fiveDaysAgo = dcrDao
                    .getPreviousBusinessDate(awaitingRequirementsSLA);
            List<DCR> list = dcrDao
                    .getLapsedDCRSetForAwaitingReqts(fiveDaysAgo);

            boolean notEmpty = CollectionUtils.isNotEmpty(list);

            if (notEmpty && LOGGER.isDebugEnabled()) {
                LOGGER.debug(list.size()
                        + " DCR Workitems are currently set Awaiting Reqts five or more days ago");
            }

            if (notEmpty) {
                for (DCR dcr : list) {
                    Long dcrId = dcr.getId();

                    DCRNonCashierFile param = new DCRNonCashierFile();
                    param.setDcrId(dcrId);
                    param.setDcrFileUploadDate(dcr.getUpdateDate());

                    int count = dcrDao
                            .countSubmittedNonCashierFileAfterGivenDate(param);

                    if (count != 0) {
                        this.reqtsSubmittedPPA(dcrId);
                    } else {
                        this.reqtsNotSubmittedPPA(dcrId);
                    }
                }
            }

        } catch (WMSDaoException ex) {
            LOGGER.error(ex);
            throw new ServiceException(ex);
        }
    }

    public void checkForUnverifiedDCR() throws ServiceException {
        try {
            Date threeDaysAgo = dcrDao
                    .getPreviousBusinessDate(forVerificationSLA);
            List<DCR> list = dcrDao
                    .getLapsedDCRSetForVerification(threeDaysAgo);

            if (CollectionUtils.isNotEmpty(list)) {
                for (DCR dcr : list) {
                    Long dcrId = dcr.getId();
                    this.notVerifiedPPA(dcrId);
                }
            }

        } catch (WMSDaoException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public void forVerificationPPA(String userId, Long dcrId,
            List<Integer> reconciledRowNums, List<DCRStepProcessorNoteBO> notes)
            throws ServiceException {

        savePPAStepProcessor(userId, dcrId, reconciledRowNums, notes);
        moveToNextStep(userId, dcrId, STATUS_FOR_VERIFICATION,
                QUEUE_DCR_PPA_RECON, STATUS_FOR_VERIFICATION);
    }

    @Override
    public void awaitingRequirements(String userId, Long dcrId,
            List<Integer> reconciledRowNums, List<DCRStepProcessorNoteBO> notes)
            throws ServiceException {
        savePPAStepProcessor(userId, dcrId, reconciledRowNums, notes);
        moveToNextStep(userId, dcrId, STATUS_AWAITING_REQUIREMENTS,
                QUEUE_DCR_PPA_RECON, STATUS_AWAITING_REQUIREMENTS);
    }

    @Override
    public List<DCRStepProcessorNoteBO> saveInlineNotes(String userId,
            Long dcrId, List<DCRStepProcessorNoteBO> notes)
            throws ServiceException {

        try {
            if (CollectionUtils.isNotEmpty(notes)) {
                dcrDao.deleteAllNotes(dcrId);
                for (DCRStepProcessorNoteBO note : notes) {
                    try {
                        DCRStepProcessorNote entity = new DCRStepProcessorNote();
                        BeanUtils.copyProperties(entity, note);

                        if (StringUtils.equalsIgnoreCase(entity.getNoteType(),
                                "FINDINGS")) {
                            String findings = StringUtils.replace(
                                    entity.getNoteContent(), LINE_SEPARATOR,
                                    " ");
                            entity.setNoteContent(findings);
                        }

                        Long id = dcrDao.saveNote(entity);
                        note.setId(id);
                    } catch (IllegalAccessException e) {
                        throw new ServiceException(e);
                    } catch (InvocationTargetException e) {
                        throw new ServiceException(e);
                    } catch (WMSDaoException e) {
                        throw new ServiceException(e);
                    }
                }
            }

            DCR dcr = dcrDao.getById(dcrId);

            if (dcr != null) {
                DCRAuditLog auditLog = new DCRAuditLog();
                auditLog.setActionDetails("WF Action: Work Item Saved");

                auditLog.setDcrId(dcrId);
                auditLog.setLogDate(cachingService.getWmsDcrSystemDate());
                auditLog.setProcessStatus(dcr.getStatus());
                auditLog.setUserId(userId);
                dcrDao.createLog(auditLog);
                LOGGER.info("WF History Log was created...");
            }
            return notes;

        } catch (WMSDaoException e1) {
            throw new ServiceException(e1);
        }

    }

    @Override
    public void savePPAStepProcessor(String userId, Long dcrId,
            List<Integer> reconciledRowNums, List<DCRStepProcessorNoteBO> notes)
            throws ServiceException {
        LOGGER.info("Saving Step Processor Reconciled Checkboxes...");

        if (CollectionUtils.isNotEmpty(reconciledRowNums)) {
            try {
                List<DCRStepProcessorReconciled> prevReconciled = dcrDao
                        .getDCRStepProcessorReconciled(dcrId);

                List<Integer> previouslyReconciledRowNums = new ArrayList<Integer>();
                if (CollectionUtils.isNotEmpty(prevReconciled)) {
                    for (DCRStepProcessorReconciled prev : prevReconciled) {
                        String csvRowNums = prev.getReconciledRowNums();
                        String[] rowNumArr = StringUtils.split(csvRowNums, ",");
                        for (String rowNum : rowNumArr) {
                            previouslyReconciledRowNums.add(NumberUtils
                                    .toInt(rowNum));
                        }
                    }
                }

                for (Integer previouslyReconciledRowNum : previouslyReconciledRowNums) {
                    reconciledRowNums.remove(previouslyReconciledRowNum);
                }

                if (CollectionUtils.isNotEmpty(reconciledRowNums)) {
                    String rrn = "";
                    int index = 0;
                    for (Integer rowNum : reconciledRowNums) {
                        rrn += rowNum;
                        if (index < reconciledRowNums.size() - 1) {
                            rrn += ",";
                        }
                        index++;
                    }

                    DCRStepProcessorReconciled rc = new DCRStepProcessorReconciled();
                    rc.setDcrId(dcrId);
                    rc.setReconciledRowNums(rrn);
                    rc.setUserId(userId);
                    rc.setDateTime(cachingService.getWmsDcrSystemDate());

                    rc = dcrDao.createDCRStepProcessorReconciled(rc);
                }
            } catch (WMSDaoException e) {
                LOGGER.error(e);
                throw new ServiceException(e);
            }
        }

        this.saveInlineNotes(userId, dcrId, notes);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * ph.com.sunlife.wms.services.ConsolidatedDataService#getAuditLog(java.
     * lang.Long)
     */
    @Override
    public List<AuditLog> getAuditLog(Long dcrId) throws ServiceException {
        List<AuditLog> result = null;

        try {
            DCR dcr = dcrDao.getById(dcrId);
            List<DCRAuditLog> list = dcrDao.getAuditLogsByDcrId(dcrId);

            if (dcr != null && CollectionUtils.isNotEmpty(list)) {
                result = new ArrayList<AuditLog>();

                for (DCRAuditLog dLog : list) {
                    AuditLog log = new AuditLog();
                    BeanUtils.copyProperties(log, dLog);

                    String dcrDateStr = StringUtils.upperCase(WMSDateUtil
                            .toFormattedDateStr(dcr.getDcrDate()));
                    log.setDcrDateStr(dcrDateStr);
                    log.setDocCapsiteId(dcr.getCenterCode());
                    log.setDocCapsiteName(dcr.getCcName());

                    result.add(log);
                }
            }

        } catch (WMSDaoException e) {
            LOGGER.error(e);
            throw new ServiceException(e);
        } catch (IllegalAccessException e) {
            LOGGER.error(e);
            throw new ServiceException(e);
        } catch (InvocationTargetException e) {
            LOGGER.error(e);
            throw new ServiceException(e);
        }

        return result;
    }

    public void addDcrComment(DCRCommentBO dcrComment) throws ServiceException {
        this.dcrCommentService.save(dcrComment);
    }

    @Override
    public DCRBO getDcr(Long dcrId) throws ServiceException {
        try {
            DCR dcr = dcrDao.getById(dcrId);

            DCRBO dcrBO = null;
            if (dcr != null) {
                dcrBO = new DCRBO();
                dcrBO.setCcId(dcr.getCcId());
                dcrBO.setCcName(dcr.getCcName());
                dcrBO.setCenterCode(dcr.getCenterCode());
                dcrBO.setId(dcr.getId());
                dcrBO.setStatus(dcr.getStatus());

                String formatedDateStr = StringUtils
                        .upperCase(STANDARD_DATE_FORMAT.format(dcr.getDcrDate()));
                dcrBO.setFormatedDateStr(formatedDateStr);
                dcrBO.setFormatedRCDStr(StringUtils.upperCase(WMSDateUtil
                        .toFormattedDateStr(dcr.getRequiredCompletionDate())));
                dcrBO.setDcrDate(dcr.getDcrDate());
                dcrBO.setUpdateDate(dcr.getUpdateDate());
                dcrBO.setUpdateUserId(dcr.getUpdateUserId());
                dcrBO.setCreateDate(dcr.getCreateDate());
                dcrBO.setCreateUserId(dcr.getCreateUserId());
            }
            return dcrBO;
        } catch (WMSDaoException ex) {
            LOGGER.error(ex);
            throw new ServiceException(ex);
        }

    }

    private List<DCRCashDepositSlipBO> getCashDepositSlipForSP(
            List<DCRCashDepositSlip> dcrCashDepositSlip) {

        List<DCRCashDepositSlipBO> dcrCashDepositSlipBO = new ArrayList<DCRCashDepositSlipBO>();

        for (DCRCashDepositSlip d : dcrCashDepositSlip) {
            DCRCashDepositSlipBO tempBO = new DCRCashDepositSlipBO();
            tempBO.setCompany(d.getCompany());
            tempBO.setAcf2id(d.getAcf2id());
            tempBO.setCurrency(d.getCurrency());
            tempBO.setDcrDepoSlipVersionSerialId(d
                    .getDcrDepoSlipVersionSerialId());
            tempBO.setProdCode(d.getProdCode());
            dcrCashDepositSlipBO.add(tempBO);
        }

        return dcrCashDepositSlipBO;

    }

    private List<DCRChequeDepositSlipBO> getChequeDepositSlipForSP(
            List<DCRChequeDepositSlip> dcrChequeDepositSlip) {

        List<DCRChequeDepositSlipBO> dcrChequeDepositSlipBO = new ArrayList<DCRChequeDepositSlipBO>();

        for (DCRChequeDepositSlip d : dcrChequeDepositSlip) {
            DCRChequeDepositSlipBO tempBO = new DCRChequeDepositSlipBO();
            tempBO.setCheckType(d.getCheckType());
            tempBO.setCompany(d.getCompany());
            tempBO.setAcf2id(d.getAcf2id());
            tempBO.setCurrency(d.getCurrency());
            tempBO.setDcrDepoSlipVersionSerialId(d
                    .getDcrDepoSlipVersionSerialId());
            tempBO.setProdCode(d.getProdCode());
            dcrChequeDepositSlipBO.add(tempBO);
        }

        return dcrChequeDepositSlipBO;

    }

    @Override
    public void checkAgeingDcrWorkItems() throws ServiceException {
        try {
            List<DCR> unsubmittedDcrWorkItems = dcrDao
                    .getAllUnSubmittedToPPADcr();
            Date today = cachingService.getWmsDcrSystemDate();

            Map<String, List<Contact>> hubContactsMap = new HashMap<String, List<Contact>>();

            if (CollectionUtils.isNotEmpty(unsubmittedDcrWorkItems)) {
                for (DCR dcr : unsubmittedDcrWorkItems) {
                    if (DateUtils.isSameDay(dcr.getRequiredCompletionDate(),
                            today)) {
                        String hubId = dcr.getHubId();
                        if (StringUtils.isNotEmpty(hubId)) {

                            List<Contact> hubManagerContacts = hubContactsMap
                                    .get(hubId);

                            if (CollectionUtils.isEmpty(hubManagerContacts)) {

                                if (sendToActualManagers) {
                                    hubManagerContacts = dcrDao
                                            .getHubManagerContacts(hubId);
                                } else {
                                    hubManagerContacts = testManagerContacts;
                                }

                                hubContactsMap.put(hubId, hubManagerContacts);
                            }

                            Set<String> shortNames = new HashSet<String>();
                            if (CollectionUtils.isNotEmpty(hubManagerContacts)) {
                                for (Contact contact : hubManagerContacts) {
                                    shortNames.add(contact.getShortName());
                                }

                                List<String> shortNameList = new ArrayList<String>(
                                        shortNames);

                                String to = "";
                                int size = shortNameList.size();
                                for (int i = 0; i < size; i++) {
                                    String shortName = shortNameList.get(i);
                                    to += shortName + "@sunlife.com";
                                    if (i != size - 1) {
                                        to += ",";
                                    }
                                }

                                WMSEmail email = new WMSEmail();
                                email.setTo(to);
                                email.setFrom("autosys");
                                String centerCode = dcr.getCenterCode();
                                if (StringUtils.isNotEmpty(centerCode)) {
                                    String formattedDateStr = WMSDateUtil
                                            .toFormattedDateStr(dcr
                                            .getDcrDate());
                                    String message = StringUtils
                                            .upperCase(formattedDateStr)
                                            + " DCR for "
                                            + centerCode
                                            + " has not been submitted to PPA";

                                    if (!sendToActualManagers) {
                                        message += " [THIS IS JUST A TEST]";
                                    }
                                    email.setSubject(message);
                                    email.setText(message);

                                    if (emailService != null) {
                                        emailService.sendSimpleMessage(email);
                                    }
                                }
                            }

                        }

                    }
                }
            }

        } catch (WMSDaoException e) {
            LOGGER.error(e);
            throw new ServiceException(e);
        }
    }

    public void setSlgfiUserCollectionsTotalUtil(
            UserCollectionsTotalUtil slgfiUserCollectionsTotalUtil) {
        this.slgfiUserCollectionsTotalUtil = slgfiUserCollectionsTotalUtil;
    }

    public void setSlocpiUserCollectionsTotalUtil(
            UserCollectionsTotalUtil slocpiUserCollectionsTotalUtil) {
        this.slocpiUserCollectionsTotalUtil = slocpiUserCollectionsTotalUtil;
    }

    public void setDcrChequeDepositSlipDao(
            DCRChequeDepositSlipDao dcrChequeDepositSlipDao) {
        this.dcrChequeDepositSlipDao = dcrChequeDepositSlipDao;
    }

    public void setDcrCashDepositSlipDao(
            DCRCashDepositSlipDao dcrCashDepositSlipDao) {
        this.dcrCashDepositSlipDao = dcrCashDepositSlipDao;
    }

    public void setAttachmentService(AttachmentService attachmentService) {
        this.attachmentService = attachmentService;
    }

    public void setDcrDao(DCRDao dcrDao) {
        this.dcrDao = dcrDao;
    }

    public void setDcrBalancingToolDao(DCRBalancingToolDao dcrBalancingToolDao) {
        this.dcrBalancingToolDao = dcrBalancingToolDao;
    }

    public void setCashierDao(CashierDao cashierDao) {
        this.cashierDao = cashierDao;
    }

    public void setDcrFilenetIntegrationService(
            DCRFilenetIntegrationService dcrFilenetIntegrationService) {
        this.dcrFilenetIntegrationService = dcrFilenetIntegrationService;
    }

    public void setDcrCashierDao(DCRCashierDao dcrCashierDao) {
        this.dcrCashierDao = dcrCashierDao;
    }

    public void setDcrCommentService(DCRCommentService dcrCommentService) {
        this.dcrCommentService = dcrCommentService;
    }

    public void setCashierReconQueues(Map<String, String> cashierReconQueues) {
        this.cashierReconQueues = cashierReconQueues;
    }

    public void setManagerReconQueues(Map<String, String> managerReconQueues) {
        this.managerReconQueues = managerReconQueues;
    }

    public void setAwaitingRequirementsSLA(int awaitingRequirementsSLA) {
        this.awaitingRequirementsSLA = awaitingRequirementsSLA;
    }

    public void setForVerificationSLA(int forVerificationSLA) {
        this.forVerificationSLA = forVerificationSLA;
    }

    public void setJobDataDao(JobDataDao jobDataDao) {
        this.jobDataDao = jobDataDao;
    }

    public void setCachingService(CachingService cachingService) {
        this.cachingService = cachingService;
    }

    public void setPpaMDSService(PPAMDSService ppaMDSService) {
        this.ppaMDSService = ppaMDSService;
    }

    public void setEmailService(EmailService emailService) {
        this.emailService = emailService;
    }

    public void setSendToActualManagers(boolean sendToActualManagers) {
        this.sendToActualManagers = sendToActualManagers;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        testManagerContacts = new ArrayList<Contact>();

        /*		Contact c1 = new Contact();
         c1.setShortName("atuga");
         c1.setUserId("pp26");
         */
        Contact c2 = new Contact();
        c2.setShortName("lalap");
        c2.setUserId("i206");

        Contact c3 = new Contact();
        c3.setShortName("zmlim");
        c3.setUserId("pv70");

//		testManagerContacts.add(c1);
        testManagerContacts.add(c2);
        testManagerContacts.add(c3);
    }

    @Override
    public void processIPACAndIngeniumData(Long dcrId, boolean isMgr) throws ServiceException {
        LOGGER.info("start processIPACAndIngeniumData..");
        long startTime = System.currentTimeMillis();
        try {
            if (!isMgr) {
                DCR dcrEntity = dcrDao.getById(dcrId);
                boolean isDay1 = (DateUtils.isSameDay(
                        dcrEntity.getDcrDate(),
                        cachingService.getWmsDcrSystemDate()));

                boolean isDay2 = !isDay1;

                List<CashierConfirmation> confirmations = dcrDao
                        .getCashierConfirmations(dcrId);

                List<Date> ccDateList = new ArrayList<Date>();
                for (CashierConfirmation cc : confirmations) {
                    Date dateConfirmed = cc.getDateConfirmed();
                    if (null != dateConfirmed) {
                        ccDateList.add(dateConfirmed);
                    }
                }

                Date lastConfirmedDate = null;

                if (CollectionUtils.isNotEmpty(ccDateList)) {
                    Collections.sort(ccDateList);
                    lastConfirmedDate = ccDateList.get(ccDateList.size() - 1);
                }

                DCR dcr = dcrIPACDao.getDCRDownloadData(dcrId);
                String flag = dcr.getHasDownloaded();
                Date dDate = dcr.getDownloadedDate();

                if ((("N".equalsIgnoreCase(flag) || isDay1) || (isDay2 && (null == dDate || null == lastConfirmedDate)) || (isDay2 && (null != dDate && null != lastConfirmedDate) && dDate.after(lastConfirmedDate) == false)) && WMSConstants.STATUS_PENDING.equalsIgnoreCase(dcrEntity.getStatus())) {
                    if (null != dcrEntity) {
                        dcrIPACService.removeIPACAndIngeniumData(dcrEntity);
                        dcrIPACService.processIPACAndIngeniumData(dcrEntity);
                    }
                }

                if (isDay2) {
                    List<DCRIpacValue> ipacValues = dcrIPACDao.getDCRIPACDailyCollectionSummary(dcrEntity.getCcId(), dcrEntity.getDcrDate());
                    if (CollectionUtils.isNotEmpty(ipacValues)) {
                        dcrIngeniumService.removeDCRIngeniumSessionTotal(dcrEntity.getCcId(), dcrEntity.getDcrDate());
                        dcrIngeniumService.populateDCRSessionTotal(dcrId, dcrEntity.getDcrDate(), dcrEntity.getCcId(), ipacValues);
                    }
                }

            }
        } catch (WMSDaoException ex) {
            LOGGER.error(ex);
            throw new ServiceException(ex);
        }
        long endTime = System.currentTimeMillis();
        long timeElapsed = endTime - startTime;

        LOGGER.info("processIPACAndIngeniumData time elapsed: " + timeElapsed + " milliseconds");
    }

    @Override
    public ConsolidatedData getConsolidatedDataAttachment(Long dcrId)
            throws ServiceException {
        ConsolidatedData consolidatedData = new ConsolidatedData();
        try {
            consolidatedData.setDcrId(dcrId);

            DateTime startDateTime = null;

            // Get the DCR from DB
            DCR dcrEntity = dcrDao.getById(dcrId);

            String centerCode = dcrEntity.getCcId();
            Date dcrDate = dcrEntity.getDcrDate();
            Date processDate = dcrDate;
            String status = dcrEntity.getStatus();
            String ccName = dcrEntity.getCcName();

            consolidatedData.setCustomerCenterName(ccName);
            consolidatedData.setCcId(centerCode);
            consolidatedData.setDcrDate(processDate);
            consolidatedData.setStatus(status);

            String formattedDateStr = WMSDateUtil
                    .toFormattedDateStr(dcrDate);

            // Get list of Cashier User IDs.
            List<DCRCashier> cashierItems = dcrCashierDao
                    .getDCRCashierListByDcrId(dcrId);

            Map<String, String> userIdShortNameMap = createUserIdShortNameMap(cashierItems);
            consolidatedData.setUserIdShortNameMap(userIdShortNameMap);

            StringBuilder sb = new StringBuilder();
            sb.append(StringUtils.upperCase(formattedDateStr));
            sb.append(dcrEntity.getCcId());
            String dcrFolderPath = sb.toString();

            List<DCRCashDepositSlipBO> validatedCashDepositSlips = new ArrayList<DCRCashDepositSlipBO>();
            List<DCRChequeDepositSlipBO> validatedChequeDepositSlips = new ArrayList<DCRChequeDepositSlipBO>();

            startDateTime = DateTime.now();
            List<DCRFile> validatedFiles = dcrFilenetIntegrationService
                    .getValidatedDepositSlipFiles(dcrFolderPath);
            LOGGER.info("dcrFilenetIntegrationService.getValidatedDepositSlipFiles:" + (DateTime.now().getMillis() - startDateTime.getMillis()));

            if (CollectionUtils.isNotEmpty(validatedFiles)) {
                mapVdsFilesToConsolidatedDepositSlips(
                        validatedCashDepositSlips,
                        validatedChequeDepositSlips, validatedFiles);
            }

            consolidatedData
                    .setValidatedCashDepositSlips(validatedCashDepositSlips);
            consolidatedData
                    .setValidatedChequeDepositSlips(validatedChequeDepositSlips);

            startDateTime = DateTime.now();
            // Get all attachments and associate to Consolidated Data
            this.associateAttachmentsToConsolidatedData(validatedFiles,
                    dcrId, consolidatedData);
            LOGGER.info("associateAttachmentsToConsolidatedData:" + (DateTime.now().getMillis() - startDateTime.getMillis()));
        } catch (IntegrationException ex) {
            LOGGER.error(ex);
            throw new ServiceException(ex);
        } catch (WMSDaoException ex) {
            LOGGER.error(ex);
            throw new ServiceException(ex);
        }
        return consolidatedData;
    }

    @Override
    public List<DCR> getAllDcrForStatusUpdate() throws ServiceException {
        List<DCR> dcrList = null;
        try {
            dcrList = dcrDao.getAllDcrForStatusUpdate();
        } catch (WMSDaoException e) {
            LOGGER.error(e);
        }
        return dcrList;
    }

    public boolean updateDCRStatus(DCR dcr, DCRAuditLog dcrAuditLog) throws ServiceException {
        boolean isUpdated = true;
        try {
            DCR dcrEntity = dcrDao.getById(dcr.getId());
            if (!dcrEntity.getStatus().equalsIgnoreCase(dcr.getStatus()) && !"".equals(dcrAuditLog.getActionDetails().trim())) {
                dcrDao.updateDcrStatus(dcr);
                dcrDao.createLog(dcrAuditLog);
            }

            if ("N".equalsIgnoreCase(dcr.getHasDownloaded())) {
                dcrDao.updateDCRDownloadFlag(dcr);
                dcrIPACService.removeIPACAndIngeniumData(dcrEntity);
            }
            if ("Y".equalsIgnoreCase(dcr.getHasDownloaded())) {
                dcrIPACService.removeIPACAndIngeniumData(dcrEntity);
                dcrIPACService.processIPACAndIngeniumData(dcrEntity);
            }

        } catch (WMSDaoException e) {
            // TODO Auto-generated catch block
            LOGGER.error(e);
            isUpdated = false;
        }
        return isUpdated;
    }

    @Override
    public List<DCR> getAllDcrForStatusUpdateFromSearch(DCR dcr) throws ServiceException {
        List<DCR> dcrList = null;
        try {
            dcrList = dcrDao.getAllDcrForStatusUpdateFromSearch(dcr);
        } catch (WMSDaoException e) {
            LOGGER.error(e);
        }
        return dcrList;
    }
    
    /*
     * Added for MR-WF-16-00034 - Random sampling for QA
     */
    public void qaReviewed(
            Long dcrId,
            String userId,
            String stepProcResponse) throws ServiceException, WMSDaoException {
        
        LOGGER.info("Start to update QA flag and terminate QA WI...");
        
        // Update dcrqa table
        boolean updFlag = dcrDao.updateQaFlag(dcrId, stepProcResponse);
        
        // Update DCR and log WF History
        if (updFlag) {
            Date today = cachingService.getWmsDcrSystemDate();
            
            DCR dcr = dcrDao.getById(dcrId);
            dcr.setStatus(stepProcResponse);
            dcr.setUpdateUserId(userId);
            dcr.setUpdateDate(today);
            dcrDao.updateDcrStatus(dcr);
            LOGGER.info("DCR was successfully updated in WMS DB...");

            DCRAuditLog auditLog = new DCRAuditLog();
            auditLog.setActionDetails("WF Action: QA Reviewed");
            auditLog.setDcrId(dcrId);
            auditLog.setLogDate(today);
            auditLog.setProcessStatus(stepProcResponse);
            auditLog.setUserId(userId);
            dcrDao.createLog(auditLog);
            LOGGER.info("WF History Log was created...");
        }

        LOGGER.info("End to update QA flag and terminate QA WI...");
    }
}
