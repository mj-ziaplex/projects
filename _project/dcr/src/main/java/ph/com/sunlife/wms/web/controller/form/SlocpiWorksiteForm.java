package ph.com.sunlife.wms.web.controller.form;

public class SlocpiWorksiteForm extends CashierWorkItemForm {
	
	private Long id0;
	private Long id1;
	//private Long id2;
	
	private Double worksiteAmount0;
	private Double worksiteAmount1;
	//private Double worksiteAmount2;
	
	public Long getId0() {
		return id0;
	}
	public void setId0(Long id0) {
		this.id0 = id0;
	}
	public Long getId1() {
		return id1;
	}
	public void setId1(Long id1) {
		this.id1 = id1;
	}
//	public Long getId2() {
//		return id2;
//	}
//	public void setId2(Long id2) {
//		this.id2 = id2;
//	}
	public Double getWorksiteAmount0() {
		return worksiteAmount0;
	}
	public void setWorksiteAmount0(Double worksiteAmount0) {
		this.worksiteAmount0 = worksiteAmount0;
	}
	public Double getWorksiteAmount1() {
		return worksiteAmount1;
	}
	public void setWorksiteAmount1(Double worksiteAmount1) {
		this.worksiteAmount1 = worksiteAmount1;
	}
//	public Double getWorksiteAmount2() {
//		return worksiteAmount2;
//	}
//	public void setWorksiteAmount2(Double worksiteAmount2) {
//		this.worksiteAmount2 = worksiteAmount2;
//	}
	
}
