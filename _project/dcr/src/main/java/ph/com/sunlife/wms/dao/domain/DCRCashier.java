package ph.com.sunlife.wms.dao.domain;

import java.util.Date;

public class DCRCashier extends Entity {

	private Cashier cashier = new Cashier();

	private DCR dcr = new DCR();

	private String status;

	private Date lastUpdateDate;

	private byte[] dcrCashierFile;

	private String dcrCashierFileDescription;

	private DCRVDIL dcrVDIL;
	
	private String shortName;

	public Cashier getCashier() {
		return cashier;
	}

	public void setCashier(Cashier cashier) {
		this.cashier = cashier;
	}
	
	public void setCashier(String cashierId) {
		cashier.setAcf2id(cashierId);
		this.setCashier(cashier);
	}

	public DCR getDcr() {
		return dcr;
	}

	public void setDcr(DCR dcr) {
		this.dcr = dcr;
	}
	
	public void setDcr(Long dcrId) {
		dcr.setId(dcrId);
		this.setDcr(dcr);
	}	

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getLastUpdateDate() {
		return lastUpdateDate;
	}

	public void setLastUpdateDate(Date lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
	}

	public byte[] getDcrCashierFile() {
		return dcrCashierFile;
	}

	public void setDcrCashierFile(byte[] dcrCashierFile) {
		this.dcrCashierFile = dcrCashierFile;
	}

	public String getDcrCashierFileDescription() {
		return dcrCashierFileDescription;
	}

	public void setDcrCashierFileDescription(String dcrCashierFileDescription) {
		this.dcrCashierFileDescription = dcrCashierFileDescription;
	}

	public DCRVDIL getDcrVDIL() {
		return dcrVDIL;
	}

	public void setDcrVDIL(DCRVDIL dcrVDIL) {
		this.dcrVDIL = dcrVDIL;
	}

	public void setDcrVDIL(Long dcrVDILId) {
		DCRVDIL dcrVDIL = new DCRVDIL();
		dcrVDIL.setId(dcrVDILId);
		this.setDcrVDIL(dcrVDIL);
	}

	public String getShortName() {
		return shortName;
	}

	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

}
