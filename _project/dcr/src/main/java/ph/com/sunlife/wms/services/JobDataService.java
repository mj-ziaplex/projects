package ph.com.sunlife.wms.services;

import java.util.List;

import ph.com.sunlife.wms.services.bo.JobDataBO;
import ph.com.sunlife.wms.services.bo.JobDataErrorLogBO;
import ph.com.sunlife.wms.services.exception.ServiceException;

/**
 * This is responsible for creating LOG entries for all the 3 configured batch
 * programs supporting the WMS DCR OE Upload.
 * 
 * @author Zainal Limpao
 * 
 */
public interface JobDataService {

	/**
	 * Get all existing Job Data.
	 * 
	 * @return
	 * @throws ServiceException
	 */
	List<JobDataBO> getAllJobData() throws ServiceException;

	/**
	 * Get all stored Job Data Error Log Entries.
	 * 
	 * @param jobId
	 * @return
	 * @throws ServiceException
	 */
	List<JobDataErrorLogBO> getJobDataErrorLog(Long jobId)
			throws ServiceException;

	/**
	 * Updates the {@link JobDataBO}.
	 * 
	 * @param jobDataBO
	 * @param ex
	 * @return
	 * @throws ServiceException
	 */
	boolean updateJobData(JobDataBO jobDataBO, Exception ex)
			throws ServiceException;

}
