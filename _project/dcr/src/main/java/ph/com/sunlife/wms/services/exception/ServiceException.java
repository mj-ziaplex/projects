package ph.com.sunlife.wms.services.exception;

/**
 * The custom exception that is expected to be propagated by the Service Layer
 * should there be underlying exception encountered during the process.
 * 
 * @author Zainal Limpao
 * 
 */
public class ServiceException extends Exception {

	private static final long serialVersionUID = -1207220510713525777L;

	public ServiceException(String message) {
		super(message);
	}

	public ServiceException(String message, Throwable throwable) {
		super(message, throwable);
	}

	public ServiceException(Throwable throwable) {
		super(throwable);
	}
}
