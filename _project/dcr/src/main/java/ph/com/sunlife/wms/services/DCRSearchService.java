package ph.com.sunlife.wms.services;

import java.util.List;


import ph.com.sunlife.wms.services.bo.CustomerCenterBO;
import ph.com.sunlife.wms.services.bo.DCRBO;
import ph.com.sunlife.wms.services.bo.HubBO;
import ph.com.sunlife.wms.services.exception.ServiceException;

public interface DCRSearchService {
	
	List<CustomerCenterBO> getCCByHubUserCC(String ccId)throws ServiceException;
	
	List<CustomerCenterBO> getCCByHub(String hubId)throws ServiceException;
	
	List<HubBO> getCCHubs() throws ServiceException;	
	
	List<DCRBO> getDCRByCCAndDate(DCRBO dcrBO) throws ServiceException;
	
	List<DCRBO> getDCRByHubAndDate(DCRBO dcrBO) throws ServiceException;
	
	List<DCRBO> getAllDCRForManager(String hubId) throws ServiceException;
	
	// Edited for MR-WF-16-00112
	List<DCRBO> getDCRManagerByCCAndDate(DCRBO dcrBO, boolean isPpa) throws ServiceException;
	
	List<DCRBO> getDCRManagerByCC(List<String> ccIds) throws ServiceException;
        
	String getCCNameById(String ccId) throws ServiceException;
        
	/*
	 * Added for MR-WF-16-00034 - Random sampling for QA
	 */
	List<DCRBO> getDCRQa(DCRBO dcrBO) throws ServiceException;

	/*
	 * Added for MR-WF-16-00034 - Random sampling for QA
	 */
	List<DCRBO> getDCRQaByCC(List<String> ccIds) throws ServiceException;
	
}
