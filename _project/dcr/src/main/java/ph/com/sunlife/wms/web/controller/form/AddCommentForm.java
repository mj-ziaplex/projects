package ph.com.sunlife.wms.web.controller.form;

import org.apache.commons.lang.ArrayUtils;

/**
 * The Form Object when adding new Comment entries.
 * 
 * @author Josephus Sardan
 * 
 */
public class AddCommentForm extends CashierWorkItemForm {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ph.com.sunlife.wms.web.controller.form.CashierWorkItemForm#getRequiredFields
	 * ()
	 */
	@Override
	public String[] getRequiredFields() {
		// Including "remarks" as required field.
		return new String[] { "remarks" };
	}
	
	private Long dcrId;
	
	public Long getDcrId() {
		return dcrId;
	}

	public void setDcrId(Long dcrId) {
		this.dcrId = dcrId;
	}

	private String remarks;

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

}
