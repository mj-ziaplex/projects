package ph.com.sunlife.wms.dao;

import ph.com.sunlife.wms.dao.exceptions.WMSDaoException;

/**
 * The mother interface for all DAO objects that require some basic reusable
 * CRUD operations.
 * 
 * @author Zainal
 * 
 * @param <E>
 *            - domain object
 */
public interface WMSDao<E> {

	/**
	 * Saves the given entity into the database and returns the same value with
	 * populated auto-generated primary key ID.
	 * 
	 * @param entity
	 * @return
	 * @throws WMSDaoException
	 */
	E save(E entity) throws WMSDaoException;

	/**
	 * Loads entity given the ID.
	 * 
	 * @param id
	 * @return
	 * @throws WMSDaoException
	 */
	E getById(Long id) throws WMSDaoException;

	/**
	 * Populates the entity with auto-generated primary key ID.
	 * 
	 * @param entity
	 * @return
	 * @throws WMSDaoException
	 */
	E refresh(E entity) throws WMSDaoException;

}
