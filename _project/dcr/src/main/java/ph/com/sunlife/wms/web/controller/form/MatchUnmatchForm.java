package ph.com.sunlife.wms.web.controller.form;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.FactoryUtils;
import org.apache.commons.collections.ListUtils;

import ph.com.sunlife.wms.services.bo.MatchBO;
import ph.com.sunlife.wms.services.bo.UnmatchBO;

public class MatchUnmatchForm extends CandidateMatchForm {
	
	@SuppressWarnings("unchecked")
	private List<UnmatchBO> unMatchList = ListUtils.lazyList(
			new ArrayList<UnmatchBO>(),
			FactoryUtils.instantiateFactory(UnmatchBO.class));
	
	@SuppressWarnings("unchecked")
	private List<MatchBO> matchList = ListUtils.lazyList(
			new ArrayList<MatchBO>(),
			FactoryUtils.instantiateFactory(MatchBO.class));

	public List<UnmatchBO> getUnMatchList() {
		return unMatchList;
	}

	public void setUnMatchList(List<UnmatchBO> unMatchList) {
		this.unMatchList = unMatchList;
	}

	public List<MatchBO> getMatchList() {
		return matchList;
	}

	public void setMatchList(List<MatchBO> matchList) {
		this.matchList = matchList;
	}

}
