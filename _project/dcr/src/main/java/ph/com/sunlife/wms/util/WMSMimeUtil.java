package ph.com.sunlife.wms.util;

import java.util.Collection;

import org.apache.commons.lang.ArrayUtils;

import eu.medsea.mimeutil.MimeType;
import eu.medsea.mimeutil.MimeUtil;

/**
 * The custom WMS Mime Util class.
 *
 * @author Zainal Limpao
 * @see eu.medsea.mimeutil.MimeUtil
 */
public class WMSMimeUtil {

    public static final String HTML_CONTENT_TYPE = "text/html";
    public static final String PNG_CONTENT_TYPE = "image/png";
    public static final String GIF_CONTENT_TYPE = "image/gif";
    public static final String JPEG_CONTENT_TYPE = "image/jpeg";
    public static final String PDF_CONTENT_TYPE = "application/pdf";
    public static final String TIFF_CONTENT_TYPE = "image/tiff";
    public static final String MSEXCEL2007_CONTENT_TYPE = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
    public static final String MSEXCEL_CONTENT_TYPE = "application/vnd.ms-excel";
    public static final String MSWORD2007_CONTENT_TYPE = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
    public static final String MSWORD_CONTENT_TYPE = "application/msword";
    public static final String EML_CONTENT_TYPE = "message/rfc822";
    public static final String DOWNLOAD_CONTENT_TYPE = "application/x-download";
    // Change according to Healthcheck
    // *Problem was identified as security risk; and a public array
    // *Change was set as private and generated getter method to be able to fetch content
    private static final String[] ZIP_CONTENT_TYPE = new String[]{
        "application/x-zip-compressed", "application/zip",
        "application/x-zip", "application/x-compress",
        "application/x-compressed", "multipart/x-zip"};

    static {
        MimeUtil.registerMimeDetector("eu.medsea.mimeutil.detector.OpendesktopMimeDetector");
        MimeUtil.registerMimeDetector("eu.medsea.mimeutil.detector.MagicMimeMimeDetector");
        MimeUtil.registerMimeDetector("eu.medsea.mimeutil.detector.ExtensionMimeDetector");
    }

    /**
     * Identifies the most specific Mime Type for the given byte array.
     *
     * @param byteArray
     * @return
     */
    @SuppressWarnings("rawtypes")
    public static String getMimeType(byte[] byteArray) {
        if (byteArray != null) {
            Collection mimeTypes = MimeUtil.getMimeTypes(byteArray);
            MimeType mostSpecificMimeType = MimeUtil.getMostSpecificMimeType(mimeTypes);

            return mostSpecificMimeType.toString();
        }

        return null;
    }

    public static String getFileExtension(String contentType) {
        String extension = null;

        if (EML_CONTENT_TYPE.equals(contentType)) {
            extension = ".eml";
        } else if (MSWORD_CONTENT_TYPE.equals(contentType)) {
            extension = ".doc";
        } else if (MSWORD2007_CONTENT_TYPE.equals(contentType)) {
            extension = ".docx";
        } else if (MSEXCEL_CONTENT_TYPE.equals(contentType)) {
            extension = ".xls";
        } else if (MSEXCEL2007_CONTENT_TYPE.equals(contentType)) {
            extension = ".xlsx";
        } else if (PDF_CONTENT_TYPE.equals(contentType)) {
            extension = ".pdf";
        } else if (JPEG_CONTENT_TYPE.equals(contentType)) {
            extension = ".jpeg";
        } else if (GIF_CONTENT_TYPE.equals(contentType)) {
            extension = ".gif";
        } else if (PNG_CONTENT_TYPE.equals(contentType)) {
            extension = ".png";
        } else if (HTML_CONTENT_TYPE.equals(contentType)) {
            extension = ".html";
        } else if (TIFF_CONTENT_TYPE.equals(contentType)) {
            extension = ".tiff";
        } else if (ArrayUtils.contains(getZIP_CONTENT_TYPE(), contentType)) {
            extension = ".zip";
        }

        return extension;
    }

    public static String getFileType(String contentType) {
        String extension = null;

        if (EML_CONTENT_TYPE.equals(contentType)) {
            extension = "eml";
        } else if (MSWORD_CONTENT_TYPE.equals(contentType)) {
            extension = "doc";
        } else if (MSWORD2007_CONTENT_TYPE.equals(contentType)) {
            extension = "docx";
        } else if (MSEXCEL_CONTENT_TYPE.equals(contentType)) {
            extension = "xls";
        } else if (MSEXCEL2007_CONTENT_TYPE.equals(contentType)) {
            extension = "xlsx";
        } else if (PDF_CONTENT_TYPE.equals(contentType)) {
            extension = "pdf";
        } else if (JPEG_CONTENT_TYPE.equals(contentType)) {
            extension = "jpeg";
        } else if (GIF_CONTENT_TYPE.equals(contentType)) {
            extension = "gif";
        } else if (PNG_CONTENT_TYPE.equals(contentType)) {
            extension = "png";
        } else if (HTML_CONTENT_TYPE.equals(contentType)) {
            extension = "html";
        } else if (TIFF_CONTENT_TYPE.equals(contentType)) {
            extension = "tiff";
        } else if (ArrayUtils.contains(getZIP_CONTENT_TYPE(), contentType)) {
            extension = "zip";
        }

        return extension;
    }

    public static String humanReadableByteCount(long bytes, boolean si) {
        int unit = si ? 1000 : 1024;
        if (bytes < unit) {
            return bytes + " B";
        }
        
        int exp = (int) (Math.log(bytes) / Math.log(unit));
        String pre = (si ? "kMGTPE" : "KMGTPE").charAt(exp - 1) + (si ? "" : "i");
        return String.format("%.1f %sB", bytes / Math.pow(unit, exp), pre);
    }

    public static String humanReadableByteCount(long bytes) {
        return humanReadableByteCount(bytes, true);
    }

    /**
     * Change according to Healthcheck
     * *Problem was identified as security risk; and a public array
     * *getter method for other classes to fetch data
     * @return the ZIP_CONTENT_TYPE
     */
    public static String[] getZIP_CONTENT_TYPE() {
        return ZIP_CONTENT_TYPE;
    }
}
