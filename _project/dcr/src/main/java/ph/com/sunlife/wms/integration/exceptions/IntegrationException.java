package ph.com.sunlife.wms.integration.exceptions;

public class IntegrationException extends Exception {

	private static final long serialVersionUID = -7663715918099383450L;

	public IntegrationException(Throwable throwable) {
		super(throwable);
	}

	public IntegrationException(String message) {
		super(message);
	}

	public IntegrationException(String message, Throwable throwable) {
		super(message, throwable);
	}

}
