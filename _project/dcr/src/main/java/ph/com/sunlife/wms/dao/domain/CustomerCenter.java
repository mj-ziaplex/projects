package ph.com.sunlife.wms.dao.domain;

/**
 * The entity class representing the <b>CenterCodes</b> Table.
 * 
 * @author Zainal Limpao
 * 
 */
public class CustomerCenter {

	private String ccId;

	private String ccName;

	private String ccCode;

	/**
	 * The <b>CC_ID</b> column with varchar(5) type.
	 * 
	 * @return
	 */
	public String getCcId() {
		return ccId;
	}

	public void setCcId(String ccId) {
		this.ccId = ccId;
	}

	/**
	 * The <b>CC_Name</b> column with varchar(50) type.
	 * 
	 * @return
	 */
	public String getCcName() {
		return ccName;
	}

	public void setCcName(String ccName) {
		this.ccName = ccName;
	}

	/**
	 * The <b>CC_AbbrevName</b> column with varchar(5) type.
	 * 
	 * @return
	 */
	public String getCcCode() {
		return ccCode;
	}

	public void setCcCode(String ccCode) {
		this.ccCode = ccCode;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((ccId == null) ? 0 : ccId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CustomerCenter other = (CustomerCenter) obj;
		if (ccId == null) {
			if (other.ccId != null)
				return false;
		} else if (!ccId.equals(other.ccId))
			return false;
		return true;
	}

}
