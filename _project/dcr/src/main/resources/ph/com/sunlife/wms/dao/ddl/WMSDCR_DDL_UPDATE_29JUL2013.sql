IF OBJECT_ID('dbo.DCRPPAMDSNote', 'U') IS NOT NULL DROP TABLE dbo.DCRPPAMDSNote
GO
CREATE TABLE dbo.DCRPPAMDSNote
( 
	id bigint PRIMARY KEY IDENTITY,
	dcr_id bigint,
	sales_slip_number varchar(40),
	account_number varchar(40),
	approval_number varchar(40),
	ppa_notes varchar(100),
	ppa_findings varchar(100)
)
GO