ALTER TABLE dbo.DCRPPAMDS
  ADD reconby varchar(6);

ALTER TABLE dbo.DCRPPAMDS
  ADD ppa_notes varchar(100);

ALTER TABLE dbo.DCRPPAMDS
  ADD ppa_findings varchar(100);
  
ALTER TABLE dbo.DCRPPAMDS
  ADD recon_date datetime;
