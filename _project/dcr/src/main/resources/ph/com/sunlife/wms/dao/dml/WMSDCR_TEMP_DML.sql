--insert new 'VDIL' Document Type and Form Version in existing WMS table, to be used in Document Capture Scanning
--NOTE: Some insert values below are not final, only the ID and description are correct so far
insert into DocumentType(DT_ID,DT_ApplForm,DT_FNDCName,DT_FolderGroup,DT_Name,
						DT_SLFForm,DT_SubstituteForm,DT_SpecialForm,DT_ReqtClassification,DT_Category,DT_ReqtLevel,
						DT_CRE_User,DT_CRE_Date,DT_UPD_User,DT_UPD_Date,
						DT_NB_DOC_CLASS,DT_MF_DOC_CLASS,DT_PS_DOC_CLASS)
values('VDIL',0,'NBNonMedicalDocs','05_Other_Forms_and_Questionnaires','VAULT''S DAILY INVENTORY LIST',
						1,0,0,'OTHR','','P',
						'pw12',getdate(),NULL,NULL,
						'NBNonMedicalDocs','MFDocuments','PSRequirements');
insert into FormTypeVersions(FTV_ID,FTV_DT_ID,FTV_Desc,
							FTV_CRE_User,FTV_CRE_Date,FTV_UPD_User,FTV_UPD_Date,
							FTV_Pages,FTV_Company_Code)
values ('VDIL.00.01','VDIL','VAULT''S DAILY INVENTORY LIST',
							'pw12',getdate(),NULL,NULL,
							NULL,'CP');
GO

