IF OBJECT_ID('dbo.DCR', 'U') IS NOT NULL DROP TABLE dbo.DCR
GO
CREATE TABLE dbo.DCR
(
	id bigint PRIMARY KEY IDENTITY,
	cc_id varchar(5) NOT NULL,
	dcr_dt datetime NOT NULL,
	required_cmp_dt datetime,
	status varchar(60),
	F_WobNum varchar(40),
	dcr_cre_user varchar(6),
	dcr_cre_date datetime,
	dcr_upd_user varchar(6),
	dcr_upd_date datetime,
	UNIQUE(cc_id, dcr_dt)
)
GO

IF OBJECT_ID('dbo.DCRCashier', 'U') IS NOT NULL DROP TABLE dbo.DCRCashier
GO
CREATE TABLE dbo.DCRCashier
(
	id bigint PRIMARY KEY IDENTITY,
	dcr_id bigint NOT NULL,
	acf2id varchar(6) NOT NULL,
	status varchar(20),
	last_updt_dt datetime,
	dcr_cashier_fl_vrs_ser_id varchar(40),
	dcr_cashier_fl_desc varchar(120),
	UNIQUE(dcr_id, acf2id)
)
GO

IF OBJECT_ID('dbo.DCRComment', 'U') IS NOT NULL DROP TABLE dbo.DCRComment
GO
CREATE TABLE dbo.DCRComment
(
	id bigint PRIMARY KEY IDENTITY,
	dcr_cashier_id bigint,
	acf2id varchar(6) NOT NULL,
	remarks varchar(350),
	dt_posted datetime,
	dcr_id bigint
)
GO

IF OBJECT_ID('dbo.DCRAddingMachine', 'U') IS NOT NULL DROP TABLE dbo.DCRAddingMachine
GO
CREATE TABLE dbo.DCRAddingMachine
( 
	id bigint PRIMARY KEY IDENTITY,
	dcr_cashier_id bigint NOT NULL,
	dcr_adding_machine_vrs_ser_id varchar(40),
	file_data varBinary(MAX)
)
GO

IF OBJECT_ID('dbo.DCRAddingMachineSheet', 'U') IS NOT NULL DROP TABLE dbo.DCRAddingMachineSheet
GO
CREATE TABLE dbo.DCRAddingMachineSheet
(
	id bigint PRIMARY KEY IDENTITY,
	dcr_adding_machine_id bigint NOT NULL,
	company_id bigint NOT NULL,
	total_amount decimal(10,2)
)
GO

IF OBJECT_ID('dbo.DCROther', 'U') IS NOT NULL DROP TABLE dbo.DCROther
GO
CREATE TABLE dbo.DCROther
( 
	id bigint PRIMARY KEY IDENTITY,
	dcr_balancing_tool_product_id bigint NOT NULL,
	cash_counter_amt decimal(13,2) default 0.0,
	cash_non_counter_amt decimal(13,2) default 0.0,
	cheque_on_us decimal(13,2) default 0.0,
	cheque_regional decimal(13,2) default 0.0,
	cheque_local decimal(13,2) default 0.0,
	cheque_dollar decimal(13,2) default 0.0,
	cheque_us_drawn_mla decimal(13,2) default 0.0,
	cheque_us_drawn_out decimal(13,2) default 0.0,
	cheque_non_counter decimal(13,2) default 0.0
)
GO

IF OBJECT_ID('dbo.DCRBalancingTool', 'U') IS NOT NULL DROP TABLE dbo.DCRBalancingTool
GO
CREATE TABLE dbo.DCRBalancingTool
( 
	id bigint PRIMARY KEY IDENTITY,
	dcr_cashier_id bigint NOT NULL,
	date_confirmed datetime,
	date_reconfirmed datetime,
	dcr_snapshot_day1_vrs_ser_id varchar(40),
	dcr_snapshot_day2_vrs_ser_id varchar(40),
	last_retrieved_sess_total datetime,
	company_id bigint NOT NULL,
	UNIQUE(dcr_cashier_id, company_id)
)
GO

IF OBJECT_ID('dbo.DCRBalancingToolProduct', 'U') IS NOT NULL DROP TABLE dbo.DCRBalancingToolProduct
GO
CREATE TABLE dbo.DCRBalancingToolProduct
( 
	id bigint PRIMARY KEY IDENTITY,
	dcr_balancing_tool_id bigint NOT NULL,
	product_type_id bigint NOT NULL,
	worksite_amt decimal(13,2)
)
GO

IF OBJECT_ID('dbo.Reversal', 'U') IS NOT NULL DROP TABLE dbo.Reversal
GO
IF OBJECT_ID('dbo.DCRReversal', 'U') IS NOT NULL DROP TABLE dbo.DCRReversal
GO
CREATE TABLE dbo.DCRReversal
( 
	id bigint PRIMARY KEY IDENTITY,
	dcr_balancing_tool_prod_id bigint NOT NULL,
	amount decimal(13,2),
	remarks varchar(120),
	currency_id bigint,
	date_time datetime
)
GO

IF OBJECT_ID('dbo.DCROtherFile', 'U') IS NOT NULL DROP TABLE dbo.DCROtherFile
GO
CREATE TABLE dbo.DCROtherFile
( 
	id bigint PRIMARY KEY IDENTITY,
	dcr_file_size bigint,
	dcr_other_file_vrs_ser_id varchar(40),
	dcr_file_upload_date datetime,
	dcr_file_upload_desc varchar(100),
	dcr_file_type varchar(10),
	dcr_cashier_id bigint NOT NULL
)
GO

IF OBJECT_ID('dbo.DCRDepositSlip', 'U') IS NOT NULL DROP TABLE dbo.DCRDepositSlip
GO
CREATE TABLE dbo.DCRDepositSlip
( 
	id bigint PRIMARY KEY IDENTITY,
	dcr_cashier_id bigint NOT NULL,
	company_id bigint,
	date_time_created datetime
)
GO

IF OBJECT_ID('dbo.DCRCashDepositSlip', 'U') IS NOT NULL DROP TABLE dbo.DCRCashDepositSlip
GO
CREATE TABLE dbo.DCRCashDepositSlip
( 
	id bigint PRIMARY KEY IDENTITY,
	dcr_cashier_id bigint NOT NULL,
	company_id bigint,
	depository_bank_id varchar(30),
	account_number varchar(30),
	customer_center varchar(30),
	currency_id bigint,
	total_cash_amt decimal(13,2),
	required_total_amt decimal(13,2),
	deposit_date datetime,
	dcr_date datetime,
	denom_1000 bigint default 0,
	denom_500 bigint default 0,
	denom_200 bigint default 0,
	denom_100 bigint default 0,
	denom_50 bigint default 0,
	denom_20 bigint default 0,
	denom_10 bigint default 0,
	denom_5 bigint default 0,
	denom_2 bigint default 0,
	denom_1 bigint default 0,
	denom_1_coin bigint default 0,
	denom_50c bigint default 0,
	denom_25c bigint default 0,
	denom_10c bigint default 0,
	denom_5c bigint default 0,
	denom_1c bigint default 0,
	dcr_depo_slip_vrs_ser_id varchar(40),
	prod_code varchar(10)
)
GO

IF OBJECT_ID('dbo.DCRCheckDepositSlip', 'U') IS NOT NULL DROP TABLE dbo.DCRCheckDepositSlip
GO
CREATE TABLE dbo.DCRCheckDepositSlip
( 
	id bigint PRIMARY KEY IDENTITY,
	dcr_cashier_id bigint NOT NULL,
	check_type_id bigint,
	company_id bigint,
	depository_bank_id varchar(30),
	account_number varchar(30),
	customer_center varchar(30),
	currency_id bigint,
	total_check_amt decimal(13,2),
	required_total_amt decimal(13,2),
	deposit_date datetime,
	dcr_date datetime,
	dcr_depo_slip_vrs_ser_id varchar(40),
	prod_code varchar(10)
)
GO

IF OBJECT_ID('dbo.MixedChequePayee', 'U') IS NOT NULL DROP TABLE dbo.MixedChequePayee
GO
IF OBJECT_ID('dbo.DCRMixedChequePayee', 'U') IS NOT NULL DROP TABLE dbo.DCRMixedChequePayee
GO
CREATE TABLE dbo.DCRMixedChequePayee
( 
	id bigint PRIMARY KEY IDENTITY,
	dcr_cashier_id bigint NOT NULL,
	bank_id varchar(20),
	check_number varchar(20),
	check_date datetime,
	check_amt decimal(13,2),
	check_type_id bigint,
	payee_company_id bigint,
	slamci_amt decimal(13,2),
	slocpi_amt decimal(13,2),
	slgfi_amt decimal(13,2),
	slfpi_amt decimal(13,2),
	others_amt decimal(13,2),
	currency_id bigint
)
GO

IF OBJECT_ID('dbo.DCRVDIL', 'U') IS NOT NULL DROP TABLE dbo.DCRVDIL
GO
CREATE TABLE dbo.DCRVDIL
( 
	id bigint PRIMARY KEY IDENTITY,
	dcr_cashier_id bigint NOT NULL,
	petty_cash decimal(10,2),
	photo_copy decimal(10,2),
	working_fund decimal(10,2),
	unclaimed_change decimal(10,2),
	overages decimal(10,2),
	vul_trad_pdc_pending_warehouse bigint,
	pn_pdcr_pending_warehouse bigint,
	after_cutoff_cash_peso decimal(10,2),
	after_cutoff_cash_dollar decimal(10,2),
	after_cutoff_check bigint,
	after_cutoff_card bigint,
	for_safe_keeping varchar(320),
	total_dollar_cobigints_amt decimal(10,2),
	date_saved datetime,
	vdil_version_serial_id varchar(40)
)
GO

IF OBJECT_ID('dbo.DCRVDILPRSeriesNumber', 'U') IS NOT NULL DROP TABLE dbo.DCRVDILPRSeriesNumber
GO
CREATE TABLE dbo.DCRVDILPRSeriesNumber
( 
	id bigint PRIMARY KEY IDENTITY,
	dcr_vdil_id bigint NOT NULL,
	company_id bigint,
	from_amt bigint,
	to_amt bigint,
	reason varchar(80)
)
GO

IF OBJECT_ID('dbo.DCRVDILISOCollection', 'U') IS NOT NULL DROP TABLE dbo.DCRVDILISOCollection
GO
CREATE TABLE dbo.DCRVDILISOCollection
( 
	id bigint PRIMARY KEY IDENTITY,
	dcr_vdil_id bigint NOT NULL,
	dcr_date datetime,
	type_id bigint,
	currency_id bigint,
	pickup_date datetime,
	amount decimal(10,2)
)
GO


IF OBJECT_ID('dbo.DCRDollarCoinCollection', 'U') IS NOT NULL DROP TABLE dbo.DCRDollarCoinCollection
GO
CREATE TABLE dbo.DCRDollarCoinCollection
( 
	id bigint PRIMARY KEY IDENTITY,
	dcr_vdil_id bigint NOT NULL,
	date_created datetime,
	denom1 bigint default 0,
	denom50c bigint default 0,
	denom25c bigint default 0,
	denom10c bigint default 0,
	denom5c bigint default 0,
	denom1c bigint default 0
)
GO

IF OBJECT_ID('dbo.DCRDollarCoinExchange', 'U') IS NOT NULL DROP TABLE dbo.DCRDollarCoinExchange
GO
CREATE TABLE dbo.DCRDollarCoinExchange
( 
	id bigint PRIMARY KEY IDENTITY,
	dcr_vdil_id bigint NOT NULL,
	date_created datetime,
	in_denom1 bigint default 0,
	in_denom50c bigint default 0,
	in_denom25c bigint default 0,
	in_denom10c bigint default 0,
	in_denom5c bigint default 0,
	in_denom1c bigint default 0,
	out_denom1 bigint default 0,
	out_denom50c bigint default 0,
	out_denom25c bigint default 0,
	out_denom10c bigint default 0,
	out_denom5c bigint default 0,
	out_denom1c bigint default 0
)
GO

IF OBJECT_ID('dbo.DCRProductType', 'U') IS NOT NULL DROP TABLE dbo.DCRProductType
GO
CREATE TABLE dbo.DCRProductType
( 
	id bigint PRIMARY KEY IDENTITY,
	company_id bigint,
	currency_id bigint,
	productname varchar(20),
	productcode varchar(4)
)
GO

IF OBJECT_ID('dbo.DCRStepProcessorNote', 'U') IS NOT NULL DROP TABLE dbo.DCRStepProcessorNote
GO
CREATE TABLE dbo.DCRStepProcessorNote
( 
	id bigint PRIMARY KEY IDENTITY,
	dcr_id bigint,
	row_num bigint,
	note_type varchar(8),
	note_content varchar(100),
	acf2id varchar(6) 
)
GO

IF OBJECT_ID('dbo.DCRStepProcessorReconciled', 'U') IS NOT NULL DROP TABLE dbo.DCRStepProcessorReconciled
GO
CREATE TABLE dbo.DCRStepProcessorReconciled
( 
	id bigint PRIMARY KEY IDENTITY,
	dcr_id bigint,
	reconciled_row_nums varchar(MAX),
	user_id VARCHAR(6),
    date_time datetime
)
GO

IF OBJECT_ID('dbo.DCRNonCashierFile', 'U') IS NOT NULL DROP TABLE dbo.DCRNonCashierFile
GO
CREATE TABLE dbo.DCRNonCashierFile
( 
	id bigint PRIMARY KEY IDENTITY,
	dcr_id bigint,
	dcr_file_size bigint,
	dcr_other_file_vrs_ser_id varchar(40),
	dcr_file_upload_date datetime,
	dcr_file_upload_desc varchar(100),
	dcr_file_type varchar(10),
	uploader_id varchar(6)
)
GO

IF OBJECT_ID('dbo.DCRAuditLog', 'U') IS NOT NULL DROP TABLE dbo.DCRAuditLog
GO
CREATE TABLE dbo.DCRAuditLog
( 
	id bigint PRIMARY KEY IDENTITY,
	dcr_id bigint,
	log_date datetime,
	process_status varchar (60),
	 action_details VARCHAR(60),
	user_id varchar(6)
)
GO

IF OBJECT_ID('dbo.DCRPPAMDS', 'U') IS NOT NULL DROP TABLE dbo.DCRPPAMDS
GO
CREATE TABLE dbo.DCRPPAMDS
( 
	id bigint PRIMARY KEY IDENTITY,
	dcr_id bigint,
	sales_slip_number varchar(40),--assuming this is unique in PR
	account_number varchar(40),
	approval_number varchar(40),
	reconciled varchar(2)--Y or N
)
GO

IF OBJECT_ID('dbo.DCRError', 'U') IS NOT NULL DROP TABLE dbo.DCRError
GO
CREATE TABLE dbo.DCRError
( 
	id bigint PRIMARY KEY IDENTITY,
	error_id varchar(10),
	error_name varchar(100)
)
GO

IF OBJECT_ID('dbo.DCRStep', 'U') IS NOT NULL DROP TABLE dbo.DCRStep
GO
CREATE TABLE dbo.DCRStep
( 
	id bigint PRIMARY KEY IDENTITY,
	step_id varchar(10),
	step_name varchar(60)
)
GO

IF OBJECT_ID('dbo.DCRStepErrorLookup', 'U') IS NOT NULL DROP TABLE dbo.DCRStepErrorLookup
GO
CREATE TABLE dbo.DCRStepErrorLookup
( 
	id bigint PRIMARY KEY IDENTITY,
	step_id varchar(10),
	error_id varchar(10)
)
GO

IF OBJECT_ID('dbo.DCRErrorTagLog', 'U') IS NOT NULL DROP TABLE dbo.DCRErrorTagLog
GO
CREATE TABLE dbo.DCRErrorTagLog
( 
	id bigint PRIMARY KEY IDENTITY,
	dcr_id bigint,
	date_posted datetime,
	date_updated datetime,
	reason varchar(100),
	posted_updated_by_id varchar(6),
	reason_deleted varchar(100),	
	step_id varchar(10),
	step_completor_user_id varchar(6)		
)
GO

IF OBJECT_ID('dbo.DCRErrorTagLogLookup', 'U') IS NOT NULL DROP TABLE dbo.DCRErrorTagLogLookup
GO
CREATE TABLE dbo.DCRErrorTagLogLookup
( 
	id bigint PRIMARY KEY IDENTITY,
	dcr_id bigint,
	step_id varchar(10),
	error_id varchar(10),
	log_id bigint
)
GO

IF OBJECT_ID('dbo.DCRJobData', 'U') IS NOT NULL DROP TABLE dbo.DCRJobData
GO
CREATE TABLE dbo.DCRJobData
( 
	id bigint PRIMARY KEY IDENTITY,
	job_name varchar(60),
	last_run datetime,
	successful bit
)
GO

IF OBJECT_ID('dbo.DCRJobDataErrorLog', 'U') IS NOT NULL DROP TABLE dbo.DCRJobDataErrorLog
GO
CREATE TABLE dbo.DCRJobDataErrorLog
( 
	id bigint PRIMARY KEY IDENTITY,
	job_data_id bigint,
	date_time datetime,
	error_message varchar(1000)
)
GO

IF OBJECT_ID('dbo.DCRCachedProperty', 'U') IS NOT NULL DROP TABLE dbo.DCRCachedProperty
GO
CREATE TABLE dbo.DCRCachedProperty
( 
	id bigint PRIMARY KEY IDENTITY,
	prop_key varchar(60),
	prop_value varchar(160),
	dcr_cre_user varchar(6),
	dcr_cre_date datetime,
	dcr_upd_user varchar(6),
	dcr_upd_date datetime,
	UNIQUE(prop_key)	
)
GO

IF OBJECT_ID('dbo.DCRCenterCoinCollection', 'U') IS NOT NULL DROP TABLE dbo.DCRCenterCoinCollection
GO
CREATE TABLE dbo.DCRCenterCoinCollection
( 
	id bigint PRIMARY KEY IDENTITY,
	cc_id varchar(5) NOT NULL,
	date_updated datetime,
	denom1 bigint default 0,
	denom50c bigint default 0,
	denom25c bigint default 0,
	denom10c bigint default 0,
	denom5c bigint default 0,
	denom1c bigint default 0
)
GO

IF OBJECT_ID('dbo.DCRCashierNoCollection', 'U') IS NOT NULL DROP TABLE dbo.DCRCashierNoCollection
GO
CREATE TABLE dbo.DCRCashierNoCollection
( 
	dcr_id bigint,
	acf2id varchar(6),
	primary key (dcr_id, acf2id)
)
GO