ALTER TABLE DCRStepProcessorReconciled ALTER COLUMN reconciled_row_nums varchar(MAX);

ALTER TABLE DCRErrorTagLog ALTER COLUMN posted_updated_by_id varchar(6);