IF OBJECT_ID('dbo.DCRCachedProperty', 'U') IS NOT NULL DROP TABLE dbo.DCRCachedProperty
GO
CREATE TABLE dbo.DCRCachedProperty
( 
	id bigint PRIMARY KEY IDENTITY,
	prop_key varchar(60),
	prop_value varchar(160),
	dcr_cre_user varchar(6),
	dcr_cre_date datetime,
	dcr_upd_user varchar(6),
	dcr_upd_date datetime,
	UNIQUE(prop_key)	
)
GO