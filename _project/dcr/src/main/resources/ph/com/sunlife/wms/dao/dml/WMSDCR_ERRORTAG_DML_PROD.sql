delete from DCRStep;
insert into DCRStep(step_id,step_name) values('STPCCM01','For Review and Approval');
insert into DCRStep(step_id,step_name) values('STPPPA01','Approved for Recon');

delete from DCRError;
insert into DCRError(error_id,error_name) values('ERRCCM01','No Attachment(s)');
insert into DCRError(error_id,error_name) values('ERRCCM02','Incorrect Card type');
insert into DCRError(error_id,error_name) values('ERRCCM03','Incomplete attachment');
insert into DCRError(error_id,error_name) values('ERRCCM04','Remarks missing');
insert into DCRError(error_id,error_name) values('ERRCCM05','PR series not indicated');
insert into DCRError(error_id,error_name) values('ERRCCM06','Undeposited collections');
insert into DCRError(error_id,error_name) values('ERRCCM07','Pickup date not indicated in VDIL');
insert into DCRError(error_id,error_name) values('ERRCCM08','Incorrect file uploaded');
insert into DCRError(error_id,error_name) values('ERRCCM09','Incomplete VDIL');
insert into DCRError(error_id,error_name) values('ERRCCM10','Comment not available');
insert into DCRError(error_id,error_name) values('ERRCCM11','Delayed collection recon');
insert into DCRError(error_id,error_name) values('ERRCCM12','Late submission of DCR report');
insert into DCRError(error_id,error_name) values('ERRCCM99','Other');

insert into DCRError(error_id,error_name) values('ERRPPA01','Total Cash Amount not tally with VDS');
insert into DCRError(error_id,error_name) values('ERRPPA02','Total Check Amount not tally with VDS');
insert into DCRError(error_id,error_name) values('ERRPPA03','Total Card Amount not tally with settlement slip');
insert into DCRError(error_id,error_name) values('ERRPPA04','Incorrect Cash classification');
insert into DCRError(error_id,error_name) values('ERRPPA05','Incorrect Check classification');
insert into DCRError(error_id,error_name) values('ERRPPA06','Incorrect Card classification');
insert into DCRError(error_id,error_name) values('ERRPPA07','Incomplete sale slip submitted');
insert into DCRError(error_id,error_name) values('ERRPPA08','Incomplete attachment re: 2nd endorsed check');
insert into DCRError(error_id,error_name) values('ERRPPA09','No email advice re: approved/processed OR cancellation attached');
insert into DCRError(error_id,error_name) values('ERRPPA10','POS sales slip unsigned');
insert into DCRError(error_id,error_name) values('ERRPPA11','MDS sales slip unsigned');
insert into DCRError(error_id,error_name) values('ERRPPA12','MDS with alterations not countersigned');
insert into DCRError(error_id,error_name) values('ERRPPA13','MDS without approval number');
insert into DCRError(error_id,error_name) values('ERRPPA14','MDS card details overwritten');
insert into DCRError(error_id,error_name) values('ERRPPA15','MDS card details unreadable');
insert into DCRError(error_id,error_name) values('ERRPPA16','Incorrect Site code used');
insert into DCRError(error_id,error_name) values('ERRPPA17','Collection deposited to incorrect LOB');
insert into DCRError(error_id,error_name) values('ERRPPA18','POS not settled within the day');
insert into DCRError(error_id,error_name) values('ERRPPA19','Card collection swiped to incorrect LOB');
insert into DCRError(error_id,error_name) values('ERRPPA99','Other');

delete from DCRStepErrorLookup;
insert into DCRStepErrorLookup(step_id,error_id) values('STPCCM01','ERRCCM01');
insert into DCRStepErrorLookup(step_id,error_id) values('STPCCM01','ERRCCM02');
insert into DCRStepErrorLookup(step_id,error_id) values('STPCCM01','ERRCCM03');
insert into DCRStepErrorLookup(step_id,error_id) values('STPCCM01','ERRCCM04');
insert into DCRStepErrorLookup(step_id,error_id) values('STPCCM01','ERRCCM05');
insert into DCRStepErrorLookup(step_id,error_id) values('STPCCM01','ERRCCM06');
insert into DCRStepErrorLookup(step_id,error_id) values('STPCCM01','ERRCCM07');
insert into DCRStepErrorLookup(step_id,error_id) values('STPCCM01','ERRCCM08');
insert into DCRStepErrorLookup(step_id,error_id) values('STPCCM01','ERRCCM09');
insert into DCRStepErrorLookup(step_id,error_id) values('STPCCM01','ERRCCM10');
insert into DCRStepErrorLookup(step_id,error_id) values('STPCCM01','ERRCCM11');
insert into DCRStepErrorLookup(step_id,error_id) values('STPCCM01','ERRCCM12');
insert into DCRStepErrorLookup(step_id,error_id) values('STPCCM01','ERRCCM99');

insert into DCRStepErrorLookup(step_id,error_id) values('STPPPA01','ERRPPA01');
insert into DCRStepErrorLookup(step_id,error_id) values('STPPPA01','ERRPPA02');
insert into DCRStepErrorLookup(step_id,error_id) values('STPPPA01','ERRPPA03');
insert into DCRStepErrorLookup(step_id,error_id) values('STPPPA01','ERRPPA04');
insert into DCRStepErrorLookup(step_id,error_id) values('STPPPA01','ERRPPA05');
insert into DCRStepErrorLookup(step_id,error_id) values('STPPPA01','ERRPPA06');
insert into DCRStepErrorLookup(step_id,error_id) values('STPPPA01','ERRPPA07');
insert into DCRStepErrorLookup(step_id,error_id) values('STPPPA01','ERRPPA08');
insert into DCRStepErrorLookup(step_id,error_id) values('STPPPA01','ERRPPA09');
insert into DCRStepErrorLookup(step_id,error_id) values('STPPPA01','ERRPPA10');
insert into DCRStepErrorLookup(step_id,error_id) values('STPPPA01','ERRPPA11');
insert into DCRStepErrorLookup(step_id,error_id) values('STPPPA01','ERRPPA12');
insert into DCRStepErrorLookup(step_id,error_id) values('STPPPA01','ERRPPA13');
insert into DCRStepErrorLookup(step_id,error_id) values('STPPPA01','ERRPPA14');
insert into DCRStepErrorLookup(step_id,error_id) values('STPPPA01','ERRPPA15');
insert into DCRStepErrorLookup(step_id,error_id) values('STPPPA01','ERRPPA16');
insert into DCRStepErrorLookup(step_id,error_id) values('STPPPA01','ERRPPA17');
insert into DCRStepErrorLookup(step_id,error_id) values('STPPPA01','ERRPPA18');
insert into DCRStepErrorLookup(step_id,error_id) values('STPPPA01','ERRPPA19');
insert into DCRStepErrorLookup(step_id,error_id) values('STPPPA01','ERRPPA99');

--select count(*) from DCRError;--33
--select count(*) from DCRStep;--2
--select count(*) from DCRStepErrorLookup;--33