IF OBJECT_ID('dbo.DCRCashierNoCollection', 'U') IS NOT NULL DROP TABLE dbo.DCRCashierNoCollection
GO
CREATE TABLE dbo.DCRCashierNoCollection
( 
	dcr_id bigint,
	acf2id varchar(6),
	primary key (dcr_id, acf2id)
)
GO