<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<link rel="stylesheet" href="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/css/search.css" media="screen">
<style>
div.fileinputs {
	position: relative;
}

div.fakefile {
	position: absolute;
	top: 0px;
	left: 0px;
	z-index: 1;
}

input.file {
	position: relative;
	text-align: right;
	-moz-opacity:0 ;
	filter:alpha(opacity: 0);
	opacity: 0;
	z-index: 2;
}
</style>
<div class="overflow" id="searchCriteriaDiv" >
	<div id="searchCriteriaTitle" class="tableTitle">
		Upload Adding Machine
	</div>
	<form action="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/addingmachine/upload.html?dcrCashierId=${dcrCashierBO.id}" method="post" enctype="multipart/form-data">
	<table id="searchCriteriaTable">
		<tr>
			<td  class="searchCriteriaTableLabel">
				<label for="searchDatePicker">Upload File</label> 
		 	</td>
		 	<td colspan="4">		 		
				<div>
					<c:if test="${not hasBeenUploaded}">
					<input type="file" id="browseButton" value="Browse..." name="data">
					</c:if>
				</div>		 		
		 	</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
		 	<td colspan="4">		 		
				<input <c:if test="${hasBeenUploaded}">disabled='disabled'</c:if> type="submit" name="findNow" value="Upload Now"/>
		 	</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
		 	<td colspan="4">		 		
				<div style="color: red;">${message}</div>
		 	</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
		 	<td colspan="4">		 		
				&nbsp;
		 	</td>
		</tr>
		<tr>
			<td  class="searchCriteriaTableLabel">
				<label for="searchDatePicker">Template</label> 
		 	</td>
		 	<td colspan="4">		 		
				<a target="_blank" href="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/addingmachine/download.html?dcrCashierId=${dcrCashierBO.id}">template.xlsx</a>
		 	</td>
		</tr>				
	</table>
	</form>
