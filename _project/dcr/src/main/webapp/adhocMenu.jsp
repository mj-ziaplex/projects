<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<!--link rel="stylesheet" href="/WMS-Web/css/common.css" media="screen"-->
<link rel="stylesheet" href="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/css/adhocMenu.css" media="screen">
<script src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/js/adhocMenu.js"></script>
		<div id="menudiv" class="claro">
			<ul class="menuListStyle">
				<li class='menuLvl_1'><a href='${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/home/index.html'>HOME</a></li>
				<li class='menuLvl_1'><a href='${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/search/index.html'>SEARCH</a></li>
				<c:if test="${isAdmin}">
				<li class='menuLvl_1'><a href='${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/admin/index.html'>MAINTENANCE</a></li>
				</c:if>
				<c:if test="${hasCashierAdhocMenu}">				
				<li class='menuLvl_1' id='collectionsNode'><a href='#' onclick="toggleNodeVis('collectionsNode','collectionsSubNodes');">Collections</a>
					<ul class="menuListStyle" id='collectionsSubNodes'>
						<li class='menuLvl_2'><a id='SLAMCIP' class="${isSlamcipConfirmed}" href='${pageContext.request.contextPath}/slamcip/index.html?dcrCashierId=${dcrCashierBO.id}'>SLAMCI - PESO</a></li>
						<li class='menuLvl_2'><a id='SLAMCID' class="${isSlamcidConfirmed}" href='${pageContext.request.contextPath}/slamcid/index.html?dcrCashierId=${dcrCashierBO.id}'>SLAMCI - DOLLAR</a></li>
						<li class='menuLvl_2'><a id='SLOCPI' class="${isSlocpiConfirmed}" href='${pageContext.request.contextPath}/slocpi/index.html?dcrCashierId=${dcrCashierBO.id}'>SLOCPI</a></li>
						<li class='menuLvl_2'><a id='SLGFI' class="${isSlgfiConfirmed}" href='${pageContext.request.contextPath}/slgfi/index.html?dcrCashierId=${dcrCashierBO.id}'>SLGFI</a></li>
						<li class='menuLvl_2'><a id='SLFPI' class="${isSlfpiConfirmed}" href='${pageContext.request.contextPath}/slfpi/report.html?dcrCashierId=${dcrCashierBO.id}'>SLFPI</a></li>
						<li class='menuLvl_2'><a id='OTHER' class="${isOthersConfirmed}" href='${pageContext.request.contextPath}/other/report.html?dcrCashierId=${dcrCashierBO.id}'>OTHERS</a></li>
					</ul>
				</li>
				<li class='menuLvl_1'><a href='${pageContext.request.contextPath}/addingmachine/index.html?dcrCashierId=${dcrCashierBO.id}'>Adding Machine</a></li>
				<li class='menuLvl_1'><a href='${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/mixedChequePayee/report.html?dcrCashierId=${dcrCashierBO.id}'>Mixed Cheque Payee</a></li>
				<li class='menuLvl_1' id='deposlipNode'><a href='#' onclick="toggleNodeVis('deposlipNode','deposlipSubNodes');">Create Deposit Slip</a>
					<ul class="menuListStyle" id='deposlipSubNodes'>
						<li class='menuLvl_2' id='slamcipesoNode'><a href='#' onclick="toggleNodeVis('slamcipesoNode','slamcipesoSubNodes');">SLAMCI PESO</a>
							<ul class="menuListStyle" id='slamcipesoSubNodes'>
								<li class='menuLvl_3'><a class="${depositSlip01}" id="cdsSlamcipCash" href='${pageContext.request.contextPath}/cds/slamcipCash.html?dcrCashierId=${dcrCashierBO.id}'>Cash</a></li>
								<li class='menuLvl_3'><a href='#'>Check</a>
									<ul class="menuListStyle">
										<li class='menuLvl_4'><a class="${depositSlip02}" id="cdsSlamcipChequeOnUs" href='${pageContext.request.contextPath}/cds/slamcipChequeOnUs.html?dcrCashierId=${dcrCashierBO.id}'>On-US</a></li>
										<li class='menuLvl_4'><a class="${depositSlip03}" id="cdsSlamcipLocalCheque" href='${pageContext.request.contextPath}/cds/slamcipLocalCheque.html?dcrCashierId=${dcrCashierBO.id}'>Local</a></li>
										<li class='menuLvl_4'><a class="${depositSlip04}" id="cdsSlamcipRegionalCheque" href='${pageContext.request.contextPath}/cds/slamcipRegionalCheque.html?dcrCashierId=${dcrCashierBO.id}'>Regional</a></li>
									<!--<li class='menuLvl_4'><a  id="cdsSlamcipChequeOT" href='${pageContext.request.contextPath}/cds/slamcipChequeOT.html?dcrCashierId=${dcrCashierBO.id}'>Out of Town</a></li>
									 --></ul>
								</li>
							</ul>
						</li>
						<li class='menuLvl_2' id='slamcidollarNode'><a href='#' onclick="toggleNodeVis('slamcidollarNode','slamcidollarSubNodes');">SLAMCI DOLLAR</a>
							<ul class="menuListStyle" id='slamcidollarSubNodes'>
								<li class='menuLvl_3'><a class="${depositSlip05}" id="cdsSlamcidCash" href='${pageContext.request.contextPath}/cds/slamcidCash.html?dcrCashierId=${dcrCashierBO.id}'>Cash</a></li>
								<li class='menuLvl_3'><a href='#'>Check</a>
									<ul class="menuListStyle">
										<li class='menuLvl_4'><a  class="${depositSlip06}" id="cdsSlamcidDollarCheck" href='${pageContext.request.contextPath}/cds/slamcidDollarCheck.html?dcrCashierId=${dcrCashierBO.id}'>Dollar Cheque</a></li>
										<!-- <li class='menuLvl_4'><a id="cdsSlamcidUCIM" href='${pageContext.request.contextPath}/cds/slamcidUSCheckInManila.html?dcrCashierId=${dcrCashierBO.id}'>US Check Drawn in Manila</a></li>
										<li class='menuLvl_4'><a id="cdsSlamcidUCOP" href='${pageContext.request.contextPath}/cds/slamcidUSCheckOutsidePH.html?dcrCashierId=${dcrCashierBO.id}'>US Check Drawn outside PH</a></li>
									<!--<li class='menuLvl_4'><a id="cdsSlamcidBankOTC" href='${pageContext.request.contextPath}/cds/slamcidBankOTC.html?dcrCashierId=${dcrCashierBO.id}'>Bank OTC Check Payment</a></li>
								--></ul>
								</li>
							</ul>
						</li>
						<li class='menuLvl_2' id='slocpipesoNode'><a href='#' onclick="toggleNodeVis('slocpipesoNode','slocpipesoSubNodes');">SLOCPI PESO</a>
							<ul class="menuListStyle" id='slocpipesoSubNodes'>
								<li class='menuLvl_3'><a class="${depositSlip07}" id="cdsSlocpipCash" href='${pageContext.request.contextPath}/cds/slocpipCash.html?dcrCashierId=${dcrCashierBO.id}'>Cash</a></li>
								<li class='menuLvl_3'><a href='#'>Check</a>
									<ul class="menuListStyle">
										<li class='menuLvl_4'><a class="${depositSlip08}" id="cdsSlocpipChequeOnUs" href='${pageContext.request.contextPath}/cds/slocpipChequeOnUs.html?dcrCashierId=${dcrCashierBO.id}'>On-US</a></li>
										<li class='menuLvl_4'><a class="${depositSlip09}" id="cdsSlocpipLocalCheque" href='${pageContext.request.contextPath}/cds/slocpipLocalCheque.html?dcrCashierId=${dcrCashierBO.id}'>Local</a></li>
										<li class='menuLvl_4'><a class="${depositSlip10}" id="cdsSlocpipRegionalCheque" href='${pageContext.request.contextPath}/cds/slocpipRegionalCheque.html?dcrCashierId=${dcrCashierBO.id}'>Regional</a></li>
									<!--<li class='menuLvl_4'><a id="cdsSlocpipChequeOT" href='${pageContext.request.contextPath}/cds/slocpipChequeOT.html?dcrCashierId=${dcrCashierBO.id}'>Out of Town</a></li>
								  --> </ul>
								</li>
								<!-- <li class='menuLvl_3'><a id="cdsSlocpipGrpCash" href='${pageContext.request.contextPath}/cds/slocpipGrpCash.html?dcrCashierId=${dcrCashierBO.id}'>Grp Life Cash</a></li>
								<li class='menuLvl_3'><a href='#'>Grp Life Check</a>
									<ul class="menuListStyle">
										<li class='menuLvl_4'><a id="cdsSlocpipGrpChequeOnUs" href='${pageContext.request.contextPath}/cds/slocpipGrpChequeOnUs.html?dcrCashierId=${dcrCashierBO.id}'>On-US</a></li>
										<li class='menuLvl_4'><a id="cdsSlocpipGrpLocalCheque" href='${pageContext.request.contextPath}/cds/slocpipGrpLocalCheque.html?dcrCashierId=${dcrCashierBO.id}'>Local</a></li>
										<li class='menuLvl_4'><a id="cdsSlocpipGrpRegionalCheque" href='${pageContext.request.contextPath}/cds/slocpipGrpRegionalCheque.html?dcrCashierId=${dcrCashierBO.id}'>Regional</a></li>
									<li class='menuLvl_4'><a id="cdsSlocpipGrpChequeOT" href='${pageContext.request.contextPath}/cds/slocpipGrpChequeOT.html?dcrCashierId=${dcrCashierBO.id}'>Out of Town</a></li>
								   </ul>
								</li> -->								
							</ul>
						</li>
						<li class='menuLvl_2' id='slocpidollarNode'><a href='#' onclick="toggleNodeVis('slocpidollarNode','slocpidollarSubNodes');">SLOCPI DOLLAR</a>
							<ul class="menuListStyle" id='slocpidollarSubNodes'>
								<li class='menuLvl_3'><a class="${depositSlip11}" id="cdsSlocpidCash" href='${pageContext.request.contextPath}/cds/slocpidCash.html?dcrCashierId=${dcrCashierBO.id}'>TRAD/VUL Cash</a></li>
								<li class='menuLvl_3'><a href='#'>TRAD/VUL Check</a>
									<ul class="menuListStyle">
										<li class='menuLvl_4'><a class="${depositSlip12}" id="cdsSlocpidDollarCheck" href='${pageContext.request.contextPath}/cds/slocpidDollarCheck.html?dcrCashierId=${dcrCashierBO.id}'>Dollar Cheque</a></li>
										<li class='menuLvl_4'><a class="${depositSlip13}" id="cdsSlocpidUCIM" href='${pageContext.request.contextPath}/cds/slocpidUSCheckInManila.html?dcrCashierId=${dcrCashierBO.id}'>US Check Drawn in Manila</a></li>
										<li class='menuLvl_4'><a class="${depositSlip14}" id="cdsSlocpidUCOP" href='${pageContext.request.contextPath}/cds/slocpidUSCheckOutsidePH.html?dcrCashierId=${dcrCashierBO.id}'>US Check Drawn outside PH</a></li>
									<!--<li class='menuLvl_4'><a id="cdsSlocpidBankOTC" href='${pageContext.request.contextPath}/cds/slocpidBankOTC.html?dcrCashierId=${dcrCashierBO.id}'>Bank OTC Check Payment</a></li>
									 --></ul>
								</li>
							</ul>
						</li>
						<li class='menuLvl_2' id='slgfipesoNode'><a href='#' onclick="toggleNodeVis('slgfipesoNode','slgfipesoSubNodes');">SLGFI PESO</a>
							<ul class="menuListStyle" id='slgfipesoSubNodes'>
								<li class='menuLvl_3'><a class="${depositSlip15}" id="cdsSlgfipCash" href='${pageContext.request.contextPath}/cds/slgfipCash.html?dcrCashierId=${dcrCashierBO.id}'>TRAD/VUL Cash</a></li>
								<li class='menuLvl_3'><a href='#'>TRAD/VUL Check</a>
									<ul class="menuListStyle">
										<li class='menuLvl_4'><a class="${depositSlip16}" id="cdsSlgfipChequeOnUs" href='${pageContext.request.contextPath}/cds/slgfipChequeOnUs.html?dcrCashierId=${dcrCashierBO.id}'>On-US</a></li>
										<li class='menuLvl_4'><a class="${depositSlip17}" id="cdsSlgfipLocalCheque" href='${pageContext.request.contextPath}/cds/slgfipLocalCheque.html?dcrCashierId=${dcrCashierBO.id}'>Local</a></li>
										<li class='menuLvl_4'><a class="${depositSlip18}" id="cdsSlgfipRegionalCheque" href='${pageContext.request.contextPath}/cds/slgfipRegionalCheque.html?dcrCashierId=${dcrCashierBO.id}'>Regional</a></li>
									<!--<li class='menuLvl_4'><a id="cdsSlgfipChequeOT" href='${pageContext.request.contextPath}/cds/slgfipChequeOT.html?dcrCashierId=${dcrCashierBO.id}'>Out of Town</a></li>
								 --></ul>
								</li>
								<li class='menuLvl_3'><a class="${depositSlip19}" id="cdsSlgfipUncCash" href='${pageContext.request.contextPath}/cds/slgfipUncCash.html?dcrCashierId=${dcrCashierBO.id}'>Unconverted Cash</a></li>
								<li class='menuLvl_3'><a href='#'>Unconverted Check</a>
									<ul class="menuListStyle">
										<li class='menuLvl_4'><a class="${depositSlip20}" id="cdsSlgfipUncChequeOnUs" href='${pageContext.request.contextPath}/cds/slgfipUncChequeOnUs.html?dcrCashierId=${dcrCashierBO.id}'>On-US</a></li>
										<li class='menuLvl_4'><a class="${depositSlip21}" id="cdsSlgfipUncLocalCheque" href='${pageContext.request.contextPath}/cds/slgfipUncLocalCheque.html?dcrCashierId=${dcrCashierBO.id}'>Local</a></li>
										<li class='menuLvl_4'><a class="${depositSlip22}" id="cdsSlgfipUncRegionalCheque" href='${pageContext.request.contextPath}/cds/slgfipUncRegionalCheque.html?dcrCashierId=${dcrCashierBO.id}'>Regional</a></li>
									<!--<li class='menuLvl_4'><a id="cdsSlgfipUncChequeOT" href='${pageContext.request.contextPath}/cds/slgfipUncChequeOT.html?dcrCashierId=${dcrCashierBO.id}'>Out of Town</a></li>
									  --></ul>
								</li>	
								<li class='menuLvl_3'><a class="${depositSlip23}" id="cdsSlgfipGrpCash" href='${pageContext.request.contextPath}/cds/slgfipGrpCash.html?dcrCashierId=${dcrCashierBO.id}'>Grp Life Cash</a></li>
								<li class='menuLvl_3'><a href='#'>Grp Life Check</a>
									<ul class="menuListStyle">
										<li class='menuLvl_4'><a class="${depositSlip24}" id="cdsSlgfipGrpChequeOnUs" href='${pageContext.request.contextPath}/cds/slgfipGrpChequeOnUs.html?dcrCashierId=${dcrCashierBO.id}'>On-US</a></li>
										<li class='menuLvl_4'><a class="${depositSlip25}" id="cdsSlgfipGrpLocalCheque" href='${pageContext.request.contextPath}/cds/slgfipGrpLocalCheque.html?dcrCashierId=${dcrCashierBO.id}'>Local</a></li>
										<li class='menuLvl_4'><a class="${depositSlip26}" id="cdsSlgfipGrpRegionalCheque" href='${pageContext.request.contextPath}/cds/slgfipGrpRegionalCheque.html?dcrCashierId=${dcrCashierBO.id}'>Regional</a></li>
									<!--<li class='menuLvl_4'><a id="cdsSlgfipGrpChequeOT" href='${pageContext.request.contextPath}/cds/slgfipGrpChequeOT.html?dcrCashierId=${dcrCashierBO.id}'>Out of Town</a></li>
									--></ul>
								</li>								
							</ul>
						</li>
						<li class='menuLvl_2' id='slgfidollarNode'><a href='#' onclick="toggleNodeVis('slgfidollarNode','slgfidollarSubNodes');">SLGFI DOLLAR</a>
							<ul class="menuListStyle" id='slgfidollarSubNodes'>
								<li class='menuLvl_3'><a class="${depositSlip27}" id="cdsSlgfidCash" href='${pageContext.request.contextPath}/cds/slgfidCash.html?dcrCashierId=${dcrCashierBO.id}'>TRAD/VUL Cash</a></li>
								<li class='menuLvl_3'><a href='#'>TRAD/VUL Check</a>
									<ul class="menuListStyle">
										<li class='menuLvl_4'><a class="${depositSlip28}" id="cdsSlgfidDollarCheck" href='${pageContext.request.contextPath}/cds/slgfidDollarCheck.html?dcrCashierId=${dcrCashierBO.id}'>Dollar Cheque</a></li>
										<li class='menuLvl_4'><a class="${depositSlip29}" id="cdsSlgfidUCIM" href='${pageContext.request.contextPath}/cds/slgfidUSCheckInManila.html?dcrCashierId=${dcrCashierBO.id}'>US Check Drawn in Manila</a></li>
										<li class='menuLvl_4'><a class="${depositSlip30}" id="cdsSlgfidUCOP" href='${pageContext.request.contextPath}/cds/slgfidUSCheckOutsidePH.html?dcrCashierId=${dcrCashierBO.id}'>US Check Drawn outside PH</a></li>
										<!--<li class='menuLvl_4'><a id="cdsSlgfidBankOTC" href='${pageContext.request.contextPath}/cds/slgfidBankOTC.html?dcrCashierId=${dcrCashierBO.id}'>Bank OTC Check Payment</a></li>
									 --></ul>
								</li>
								<li class='menuLvl_3'><a class="${depositSlip31}" id="cdsSlgfidUncCash" href='${pageContext.request.contextPath}/cds/slgfidUncCash.html?dcrCashierId=${dcrCashierBO.id}'>Unconverted Cash</a></li>
								<li class='menuLvl_3'><a href='#'>Unconverted Check</a>
									<ul class="menuListStyle">
										<li class='menuLvl_4'><a class="${depositSlip32}" id="cdsSlgfidUncDollarCheck" href='${pageContext.request.contextPath}/cds/slgfidUncDollarCheck.html?dcrCashierId=${dcrCashierBO.id}'>Dollar Cheque</a></li>
										<li class='menuLvl_4'><a class="${depositSlip33}" id="cdsSlgfidUncUCIM" href='${pageContext.request.contextPath}/cds/slgfidUncUSCheckInManila.html?dcrCashierId=${dcrCashierBO.id}'>US Check Drawn in Manila</a></li>
										<li class='menuLvl_4'><a class="${depositSlip34}" id="cdsSlgfidUncUCOP" href='${pageContext.request.contextPath}/cds/slgfidUncUSCheckOutsidePH.html?dcrCashierId=${dcrCashierBO.id}'>US Check Drawn outside PH</a></li>
									<!--<li class='menuLvl_4'><a id="cdsSlgfidUncBankOTC" href='${pageContext.request.contextPath}/cds/slgfidUncBankOTC.html?dcrCashierId=${dcrCashierBO.id}'>Bank OTC Check Payment</a></li>
									 --></ul>
								</li>
								<li class='menuLvl_3'><a class="${depositSlip35}" id="cdsSlgfidGrpCash" href='${pageContext.request.contextPath}/cds/slgfidGrpCash.html?dcrCashierId=${dcrCashierBO.id}'>Grp Life Cash</a></li>
								<li class='menuLvl_3'><a href='#'>Grp Life Check</a>
									<ul class="menuListStyle">
										<li class='menuLvl_4'><a class="${depositSlip36}" id="cdsSlgfidGrpDollarCheck" href='${pageContext.request.contextPath}/cds/slgfidGrpDollarCheck.html?dcrCashierId=${dcrCashierBO.id}'>Dollar Cheque</a></li>
										<li class='menuLvl_4'><a class="${depositSlip37}" id="cdsSlgfidGrpUCIM" href='${pageContext.request.contextPath}/cds/slgfidGrpUSCheckInManila.html?dcrCashierId=${dcrCashierBO.id}'>US Check Drawn in Manila</a></li>
										<li class='menuLvl_4'><a class="${depositSlip38}" id="cdsSlgfidGrpUCOP" href='${pageContext.request.contextPath}/cds/slgfidGrpUSCheckOutsidePH.html?dcrCashierId=${dcrCashierBO.id}'>US Check Drawn outside PH</a></li>
									<!--<li class='menuLvl_4'><a id="cdsSlgfidGrpBankOTC" href='${pageContext.request.contextPath}/cds/slgfidGrpBankOTC.html?dcrCashierId=${dcrCashierBO.id}'>Bank OTC Check Payment</a></li>
									 --></ul>
								</li>								
							</ul>
						</li>
						<li class='menuLvl_2' id='slfpipesoNode'><a href='#' onclick="toggleNodeVis('slfpipesoNode','slfpipesoSubNodes');">SLFPI PESO</a>
							<ul class="menuListStyle" id='slfpipesoSubNodes'>
								<li class='menuLvl_3'><a class="${depositSlip39}" id="cdsSlfpipCash" href='${pageContext.request.contextPath}/cds/slfpipCash.html?dcrCashierId=${dcrCashierBO.id}'>Preneed Cash</a></li>
								<li class='menuLvl_3'><a href='#'>Preneed Check</a>
									<ul class="menuListStyle">
										<li class='menuLvl_4'><a class="${depositSlip40}" id="cdsSlfpipChequeOnUs" href='${pageContext.request.contextPath}/cds/slfpipChequeOnUs.html?dcrCashierId=${dcrCashierBO.id}'>On-US</a></li>
										<li class='menuLvl_4'><a class="${depositSlip41}" id="cdsSlfpipLocalCheque" href='${pageContext.request.contextPath}/cds/slfpipLocalCheque.html?dcrCashierId=${dcrCashierBO.id}'>Local</a></li>
										<li class='menuLvl_4'><a class="${depositSlip42}" id="cdsSlfpipRegionalCheque" href='${pageContext.request.contextPath}/cds/slfpipRegionalCheque.html?dcrCashierId=${dcrCashierBO.id}'>Regional</a></li>
										<!--<li class='menuLvl_4'><a id="cdsSlfpipChequeOT" href='${pageContext.request.contextPath}/cds/slfpipChequeOT.html?dcrCashierId=${dcrCashierBO.id}'>Out of Town</a></li>
									   --></ul>
								</li>
								<li class='menuLvl_3'><a class="${depositSlip43}" id="cdsSlfpipGafCash" href='${pageContext.request.contextPath}/cds/slfpipGafCash.html?dcrCashierId=${dcrCashierBO.id}'>GAF Cash</a></li>
								<li class='menuLvl_3'><a class="${depositSlip44}" id="cdsSlfpipGafLocalCheque" href='${pageContext.request.contextPath}/cds/slfpipGafLocalCheque.html?dcrCashierId=${dcrCashierBO.id}'>GAF Check</a>
									<ul class="menuListStyle">
										<!-- <li class='menuLvl_4'><a id="cdsSlfpipGafChequeOnUs" href='${pageContext.request.contextPath}/cds/slfpipGafChequeOnUs.html?dcrCashierId=${dcrCashierBO.id}'>On-US</a></li> -->
										<!-- <li class='menuLvl_4'><a id="cdsSlfpipGafLocalCheque" href='${pageContext.request.contextPath}/cds/slfpipGafLocalCheque.html?dcrCashierId=${dcrCashierBO.id}'>GAF Check</a></li> -->
										<!-- <li class='menuLvl_4'><a id="cdsSlfpipGafRegionalCheque" href='${pageContext.request.contextPath}/cds/slfpipGafRegionalCheque.html?dcrCashierId=${dcrCashierBO.id}'>Regional</a></li>  -->
										<!--<li class='menuLvl_4'><a id="cdsSlfpipGafChequeOT" href='${pageContext.request.contextPath}/cds/slfpipGafChequeOT.html?dcrCashierId=${dcrCashierBO.id}'>Out of Town</a></li>
									   --></ul>
								</li>								
							</ul>
						</li>
						<!-- No Other Node -->
						<li class='menuLvl_2' id='otherpesoNode'><a href='#' onclick="toggleNodeVis('otherpesoNode','otherpesoSubNodes');">OTHER PESO</a>
							<ul class="menuListStyle" id='otherpesoSubNodes'>
								<li class='menuLvl_3'><a class="${depositSlip45}" id="cdsOtherpCash" href='${pageContext.request.contextPath}/cds/otherpCash.html?dcrCashierId=${dcrCashierBO.id}'>COB Cash</a></li>
								<li class='menuLvl_3'><a href='#'>COB Check</a>
									<ul class="menuListStyle">
										<li class='menuLvl_4'><a class="${depositSlip46}" id="cdsOtherpChequeOnUs" href='${pageContext.request.contextPath}/cds/otherpChequeOnUs.html?dcrCashierId=${dcrCashierBO.id}'>On-US</a></li>
										<li class='menuLvl_4'><a class="${depositSlip47}" id="cdsOtherpLocalCheque" href='${pageContext.request.contextPath}/cds/otherpLocalCheque.html?dcrCashierId=${dcrCashierBO.id}'>Local</a></li>
										<li class='menuLvl_4'><a class="${depositSlip48}" id="cdsOtherpRegionalCheque" href='${pageContext.request.contextPath}/cds/otherpRegionalCheque.html?dcrCashierId=${dcrCashierBO.id}'>Regional</a></li>
										<!--<li class='menuLvl_4'><a id="cdsOtherpChequeOT" href='${pageContext.request.contextPath}/cds/otherpChequeOT.html?dcrCashierId=${dcrCashierBO.id}'>Out of Town</a></li>
									  --></ul>
								</li>
							</ul>
						</li>
						<li class='menuLvl_2' id='otherdollarNode'><a href='#' onclick="toggleNodeVis('otherdollarNode','otherdollarSubNodes');">OTHER DOLLAR</a>
							<ul class="menuListStyle" id='otherdollarSubNodes'>
								<li class='menuLvl_3'><a  class="${depositSlip49}" id="cdsOtherdCash" href='${pageContext.request.contextPath}/cds/otherdCash.html?dcrCashierId=${dcrCashierBO.id}'>COB Cash</a></li>
								<li class='menuLvl_3'><a href='#'>COB Check</a>
									<ul class="menuListStyle">
										<li class='menuLvl_4'><a class="${depositSlip50}" id="cdsOtherdDollarCheck" href='${pageContext.request.contextPath}/cds/otherdDollarCheck.html?dcrCashierId=${dcrCashierBO.id}'>Dollar Cheque</a></li>
										<li class='menuLvl_4'><a class="${depositSlip51}" id="cdsOtherdUCIM" href='${pageContext.request.contextPath}/cds/otherdUSCheckInManila.html?dcrCashierId=${dcrCashierBO.id}'>US Check Drawn in Manila</a></li>
										<li class='menuLvl_4'><a class="${depositSlip52}" id="cdsOtherdUCOP" href='${pageContext.request.contextPath}/cds/otherdUSCheckOutsidePH.html?dcrCashierId=${dcrCashierBO.id}'>US Check Drawn outside PH</a></li>
									<!--<li class='menuLvl_4'><a id="cdsOtherdBankOTC" href='${pageContext.request.contextPath}/cds/otherdBankOTC.html?dcrCashierId=${dcrCashierBO.id}'>Bank OTC Check Payment</a></li>
									--></ul>
								</li>
							</ul>
						</li>
					</ul>
				</li> 
				<li class='menuLvl_1'><a id="createVDIL" href='${pageContext.request.contextPath}/vdil/vdilCreate.html?dcrCashierId=${dcrCashierBO.id}'>Create VDIL</a></li>
				<li class='menuLvl_1'><a id="uploadFiles" href='${pageContext.request.contextPath}/upload/upload.html?dcrCashierId=${dcrCashierBO.id}'>Upload Files</a></li>
				<!-- 
				<li class='menuLvl_1'><a id="uploadFiles" target="_blank" href='${pageContext.request.contextPath}/email/create.html?dcrId=62'>Email</a></li>
				<li class='menuLvl_1'><a id="attachFiles" target="_blank" href='${pageContext.request.contextPath}/attachSP/attach.html?dcrCashierId=${dcrCashierBO.id}'>Attach Files SP</a></li>
				<li class='menuLvl_1'><a id="addNote" target="_blank" href='${pageContext.request.contextPath}/addNoteSP/create.html?dcrCashierId=59'>Add Note SP</a></li>
				 -->
				</c:if>
			</ul>
		</div>	
		<c:if test="${not empty pageSequence}">
		<script>
			activateLink('${pageSequence}');
		</script>
		</c:if>