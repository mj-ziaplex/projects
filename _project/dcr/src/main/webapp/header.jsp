<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<link rel="stylesheet" href="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/css/header.css" media="screen">

<div id="headerdiv">
<a href=''><img src='${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/images/web/common/Spacer.gif' width='0' height='0' border='0' alt='Skip to content'></a>
<table class='banner' width='100%' cellspacing='0' cellpadding='0' border='0'>
<tr>
	<td width='99%' rowspan='2' valign='bottom'>
		<br/>
		<div id="headerText">
		<a href=''>
		<img class='bannerImage' src='${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/images/Banner.gif' border='0' alt='Home' title='Home' width='264' height='35'></a><br/>
		${userCompleteName} | ${headerDate}
		</div>
	</td>
	<td nowrap align='right' valign='top'>
	</td>
</tr>
<tr>
<td nowrap align='right' valign='bottom'>
	<a title="Help" class="white" href="#">Help</a> | 
	<a title="Home" class="white" href="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/home/index.html">Home</a> | 
	<a title="Sign Out" class="white" href="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/logout/logout.html">Sign Out</a>
</td>
</tr>
</table>

</div>
<!-- 
<div id="headerdiv">
	<table id="headerTable">
		<tr>
			<td id="logoSection">
				<div id="logo"></div>
			</td>
			<td id="bannerSection">
				<div id="banner"></div>
			</td>
		</tr>
	</table>
	
</div>
-->