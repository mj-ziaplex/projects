<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<link rel="stylesheet" href="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/css/centerCode.css" media="screen">
<link rel="stylesheet" href="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/css/pagination.css" media="screen">
<link rel="stylesheet" href="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/jquery/jquery-ui-1.9.1/themes/base/jquery-ui.css" />
<script src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/jquery/jquery-1.8.2.js"></script>
<script src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/jquery/jquery-ui-1.9.1/ui/jquery-ui.js"></script>
<script src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/js/custom-table.js"></script>
<script src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/js/sorttable.js"></script>
<script src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/js/common.js"></script>
<script src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/js/pagination.js"></script>
<script src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/js/centerCode.js"></script>
<c:if test="${action == null}">
<div class="overflow" id="centerCodeDiv" >

	<div id="adminCenterCodeOperationDiv">
		<form method="POST" id="adminCCOperation">
			<input type="hidden" id="ccIds" name="ccIds" />
			<table id="adminCenterCodeOperationTable">
				<tr>
					<td><input type="button" value="Remove Center Code"
						id="removeCenterCodeBtn" onclick="doDeleteCC('${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/admin/centerCodeDoRemove.html');" /></td>
					<td><input type="button" value="Add New Center Code" id="addCenterCodeBtn" onclick="window.open('${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/admin/centerCodeForm.html','_self');"/></td>
				</tr>
			</table>
		</form>
	</div>
	<div id="centerCodeTableNode">
		<div id="centerCodeLabelNode"></div>
		<div id="centerCodeNode"></div>
	</div>
	<div id="addCenterCodeNode"></div>	
	<div id="navigationNode"></div>

</div>

<script>
sorttable.init();
var centerCodes=[
			<c:forEach items="${ccList}" var="centerCode" varStatus="rowCnt">
		    	{ccIdChkbox: "<input type='checkbox' id='ccIdChkbox_${centerCode.ccId}' rowInd='${centerCode.ccId}' onchange='handleRemoveToggle(this);' />",
		    		ccId: "${centerCode.ccId}",
		    		ccAbbrevName: "${centerCode.ccAbbrevName}",
		    		ccName: "${centerCode.ccName}",
		    		ccDesc: "${centerCode.ccDesc}",
			         <c:if test="${centerCode.ccAutoIdx == true}">
			         ccAutoIdx: "Y",
			         </c:if>
			         <c:if test="${centerCode.ccAutoIdx == false}">
			         ccAutoIdx: "N",
			         </c:if>
			         <c:if test="${centerCode.ccCanEncode == true}">
			         ccCanEncode: "Y",
			         </c:if>
			         <c:if test="${centerCode.ccCanEncode == false}">
			         ccCanEncode: "N",
			         </c:if>
			         <c:if test="${centerCode.ccViewImage == true}">
			         ccViewImage: "Y",
			         </c:if>
			         <c:if test="${centerCode.ccViewImage == false}">
			         ccViewImage: "N",
			         </c:if>
			         <c:if test="${centerCode.ccZoomZone == true}">
			         ccZoomZone: "Y",
			         </c:if>
			         <c:if test="${centerCode.ccZoomZone == false}">
			         ccZoomZone: "N",
			         </c:if>
			         <c:if test="${centerCode.ccIsIso == true}">
			         ccIsIso: "Y",
			         </c:if>
			         <c:if test="${centerCode.ccIsIso == false}">
			         ccIsIso: "N",
			         </c:if>
		    		ccCreUser: "${centerCode.ccCreUser}",
		    		ccCreDate: "<fmt:formatDate type='both' value='${centerCode.ccCreDate}'/>",
		    		ccUpdUser: "${centerCode.ccUpdUser}",
		    		ccUpdDate: "<fmt:formatDate type='both' value='${centerCode.ccUpdDate}'/>",
			         <c:if test="${centerCode.ccActive == true}">
			         ccActive: "Y",
			         </c:if>
			         <c:if test="${centerCode.ccActive == false}">
			         ccActive: "N",
			         </c:if>
		       	 action: "<a href='centerCodeForm.html?ccId=${centerCode.ccId}'>Edit</a>"} 
				<c:if test="${not rowCnt.last}">
	        	,
				</c:if>
			</c:forEach>
];
              
setCenterCodes(centerCodes);          
</script>
</c:if>

<c:if test="${action == 'update' || action == 'new'}">
<div id="centerCodeDiv">
	<form:form method="POST" name="adminCenterCodeForm" id="adminCenterCodeForm" modelAttribute="adminCenterCodeForm">
		<input type="hidden" id="cId" name="cId" value="${param.ccId}">
			<table id="adminCenterCodeFormTable">
				<tr>
					<th align="left">Center Code</th>
				</tr>
				<tr class="evenRow">
					<td>CC ID</td>
					<td><form:input id="ccId" path="ccId" size="25" /></td>
				</tr>
				<tr class="oddRow">
					<td>CC Name</td>
					<td><form:input id="ccName" path="ccName" size="25" /></td>
				</tr>
				<tr class="evenRow">
					<td>CC Abbrevation Name</td>
					<td><form:input id="ccAbbrevName" path="ccAbbrevName" size="25" /></td>
				</tr>
				<tr class="oddRow">
					<td>CC Description</td>
					<td><form:input id="ccDesc" path="ccDesc" size="25" /></td>
				</tr>
				<tr class="evenRow">
					<td>User ID</td>
					<td><form:select path="userId" id="userId">
						<c:forEach items="${userList}" var="user">
					    <form:option value="${user.wmsuId}">${user.wmsuName}</form:option>
  						</c:forEach>
						</form:select>
					</td>
				</tr>
				<tr class="oddRow">
					<td>User Hub</td>
					<td><form:select path="userHub" id="userHub">
							<form:options items="${userHubList}"/>
						</form:select></td>
				</tr>
				<tr class="evenRow">
					<td>User NBO</td>
					<td><form:input id="userNBO" path="userNBO" size="25" /></td>
				</tr>
				<tr class="oddRow">
					<td>User Group</td>
					<td><form:select path="userGroups" id="userGroups" multiple="true" size="5">
					  	<c:forEach items="${userGroupList}" var="userGroup">
					    <form:option value="${userGroup.groupId}">${userGroup.groupId} - ${userGroup.groupName}</form:option>
  						</c:forEach>
						</form:select></td>
				</tr>
				<tr>
					<td class="evenRow">Auto Idx</td>
					<td><form:radiobuttons path="ccAutoIdx"
							items="${yesNoOption}" /></td>
				</tr>
				<tr>
					<td class="oddRow">Can Encode</td>
					<td><form:radiobuttons path="ccCanEncode"
							items="${yesNoOption}" /></td>
				</tr>
				<tr>
					<td class="evenRow">View Image</td>
					<td><form:radiobuttons path="ccViewImage"
							items="${yesNoOption}" /></td>
				</tr>
				<tr>
					<td class="oddRow">Zoom Zone</td>
					<td><form:radiobuttons path="ccZoomZone"
							items="${yesNoOption}" /></td>
				</tr>
				<tr>
					<td class="evenRow">Is ISO</td>
					<td><form:radiobuttons path="ccIsIso"
							items="${yesNoOption}" /></td>
				</tr>
				<tr>
					<td class="oddRow">Active</td>
					<td><form:radiobuttons path="ccActive"
							items="${yesNoOption}" /></td>
				</tr>
				<tr>
					<td colspan="2"><br></td>
				</tr>
				<tr>
					<td colspan="2" align="center">
					<c:if test="${action == 'new'}">
					<input id="addCenterCodeBtn"
						type="button" value="Create" onclick="onDoSubmit('${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/admin/centerCodeDoCreate.html');" />
					</c:if>	
					<c:if test="${action == 'update'}">
					<input id="updateCenterCodeBtn"
						type="button" value="Update" onclick="onDoSubmit('${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/admin/centerCodeDoUpdate.html');" />
					</c:if>	
					<input id="cancelCenterCodeBtn"
						type="button" value="Cancel" onclick="window.open('${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/admin/centerCode.html','_self');" />
					</td>
				</tr>
			</table>
		</form:form>
</div>
</c:if>
