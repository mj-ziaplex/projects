<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<link rel="stylesheet" href="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/css/slfpiGrid.css" media="screen">
<link rel="stylesheet" href="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/jquery/jquery-ui-1.9.1/themes/base/jquery-ui.css" />
<script src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/jquery/jquery-1.8.2.js"></script>
<script src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/jquery/jquery-ui-1.9.1/ui/jquery-ui.js"></script>
<script src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/js/custom-table.js"></script>
<script src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/js/slfpiGrid.js"></script>
<script src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/js/common.js"></script>
<c:forEach items="${productsToDisplay}" var="column" varStatus="rowCnt">
	<script>	
 				var headerName= "${column.productType.productName}" + "(" + "${column.productType.productCode}" + ")";
 				var headerField = "${column.productType.productCode}";
 				var tempHeader = [{name: headerName, field: headerField, width: "15%"}];
 				var tempTotal = 0;
   				appendJsons([slfpiTableLayout,tempHeader]);
   				totalCashCounter.${column.productType.productCode} = numberWithCommas(parseFloat("${column.totalCashCounter}").toFixed(2));
   				totalCashNonCounter.${column.productType.productCode} = numberWithCommas(parseFloat("${column.totalCashNonCounter}").toFixed(2));
   				totalCheckGaf.${column.productType.productCode} = numberWithCommas(parseFloat("${column.totalCheckGaf}").toFixed(2));
   				totalCheckOnUs.${column.productType.productCode} = numberWithCommas(parseFloat("${column.totalCheckOnUs}").toFixed(2));
   				totalCheckLocal.${column.productType.productCode} = numberWithCommas(parseFloat("${column.totalCheckLocal}").toFixed(2));
   				totalCheckRegional.${column.productType.productCode} = numberWithCommas(parseFloat("${column.totalCheckRegional}").toFixed(2));
   				totalPmo.${column.productType.productCode} = numberWithCommas(parseFloat("${column.totalPmo}").toFixed(2));
   				totalCheckNonCounter.${column.productType.productCode} = numberWithCommas(parseFloat("${column.totalCheckNonCounter}").toFixed(2));
   				totalCheckOT.${column.productType.productCode} = numberWithCommas(parseFloat("${column.totalCheckOT}").toFixed(2));
   				//totalCreditMemo.${column.productType.productCode} = numberWithCommas(parseFloat("${column.totalCreditMemo}").toFixed(2));
   				totalUsCheckInManila.${column.productType.productCode} = numberWithCommas(parseFloat("${column.totalUsCheckInManila}").toFixed(2));
   				totalUsCheckOutPh.${column.productType.productCode} = numberWithCommas(parseFloat("${column.totalUsCheckOutPh}").toFixed(2));
   				totalDollarCheque.${column.productType.productCode} = numberWithCommas(parseFloat("${column.totalDollarCheque}").toFixed(2));
   				//totalBankOTCCheckPayment.${column.productType.productCode} = numberWithCommas(parseFloat("${column.totalBankOTCCheckPayment}").toFixed(2));
   				cardTypes.${column.productType.productCode} = " ";
   				totalNonCash.${column.productType.productCode} = numberWithCommas(parseFloat("${column.totalNonCash}").toFixed(2));
   				totalCardGaf.${column.productType.productCode} = numberWithCommas(parseFloat("${column.totalCardGaf}").toFixed(2));
   				totalPosBpi.${column.productType.productCode} = numberWithCommas(parseFloat("${column.totalPosBpi}").toFixed(2));
   				totalPosCtb.${column.productType.productCode} = numberWithCommas(parseFloat("${column.totalPosCtb}").toFixed(2));
   				totalPosHsbc.${column.productType.productCode} = numberWithCommas(parseFloat("${column.totalPosHsbc}").toFixed(2));
   				totalPosScb.${column.productType.productCode} = numberWithCommas(parseFloat("${column.totalPosScb}").toFixed(2));
   				totalPosRcbc.${column.productType.productCode} = numberWithCommas(parseFloat("${column.totalPosRcbc}").toFixed(2));
   				totalPosBdo.${column.productType.productCode} = numberWithCommas(parseFloat("${column.totalPosBdo}").toFixed(2));
   				totalMdsBpi.${column.productType.productCode} = numberWithCommas(parseFloat("${column.totalMdsBpi}").toFixed(2));
   				totalMdsCtb.${column.productType.productCode} = numberWithCommas(parseFloat("${column.totalMdsCtb}").toFixed(2));
   				totalMdsHsbc.${column.productType.productCode} = numberWithCommas(parseFloat("${column.totalMdsHsbc}").toFixed(2));
   				totalMdsSbc.${column.productType.productCode} = numberWithCommas(parseFloat("${column.totalMdsSbc}").toFixed(2));
   				totalMdsRcbc.${column.productType.productCode} = numberWithCommas(parseFloat("${column.totalMdsRcbc}").toFixed(2));
   				totalMdsBdo.${column.productType.productCode} = numberWithCommas(parseFloat("${column.totalMdsBdo}").toFixed(2));
   				totalAutoCtb.${column.productType.productCode} = numberWithCommas(parseFloat("${column.totalAutoCtb}").toFixed(2));
   				totalAutoScb.${column.productType.productCode} = numberWithCommas(parseFloat("${column.totalAutoScb}").toFixed(2));
   				totalAutoRcbc.${column.productType.productCode} = numberWithCommas(parseFloat("${column.totalAutoRcbc}").toFixed(2));
   				totalSunlinkOnline.${column.productType.productCode} = numberWithCommas(parseFloat("${column.totalSunlinkOnline}").toFixed(2));
   				totalEpsBpi.${column.productType.productCode} = numberWithCommas(parseFloat("${column.totalEpsBpi}").toFixed(2));
   				totalSunlinkHsbcPeso.${column.productType.productCode} = numberWithCommas(parseFloat("${column.totalSunlinkHsbcPeso}").toFixed(2));
   				paymentFromInsurer.${column.productType.productCode} = numberWithCommas(parseFloat("${column.paymentFromInsurer}").toFixed(2));
   				tempTotal = parseFloat("${column.totalCashCounter}") +
   							parseFloat("${column.totalCashNonCounter}") +
   							parseFloat("${column.totalCheckGaf}") +
   							parseFloat("${column.paymentFromInsurer}") +
   							parseFloat("${column.totalCheckOnUs}") +
   							parseFloat("${column.totalCheckLocal}") +
   							parseFloat("${column.totalCheckRegional}") +
   							parseFloat("${column.totalPmo}") +
   							parseFloat("${column.totalCheckNonCounter}") +
   							parseFloat("${column.totalCheckOT}") +
   							parseFloat("${column.totalNonCash}") +
   							parseFloat("${column.totalCardGaf}") +
   							parseFloat("${column.totalPosBpi}") +
   							parseFloat("${column.totalPosCtb}") +
   							parseFloat("${column.totalPosHsbc}") +
   							parseFloat("${column.totalPosScb}") +
   							parseFloat("${column.totalPosRcbc}") +
   							parseFloat("${column.totalPosBdo}") +
   							parseFloat("${column.totalMdsBpi}") +
   							parseFloat("${column.totalMdsCtb}") +
   							parseFloat("${column.totalMdsHsbc}") +
   							parseFloat("${column.totalMdsSbc}") +
   							parseFloat("${column.totalMdsRcbc}") +
   							parseFloat("${column.totalMdsBdo}") +
   							parseFloat("${column.totalAutoCtb}") +
   							parseFloat("${column.totalAutoScb}") +
   							parseFloat("${column.totalAutoRcbc}") +
   							parseFloat("${column.totalSunlinkOnline}") +
   							parseFloat("${column.totalEpsBpi}") +
   							parseFloat("${column.totalSunlinkHsbcPeso}") +
   						//	parseFloat("${column.totalCreditMemo}") +
   							parseFloat("${column.totalUsCheckInManila}") +
   							parseFloat("${column.totalUsCheckOutPh}") +
   							parseFloat("${column.totalDollarCheque}");
   						//	parseFloat("${column.totalBankOTCCheckPayment}");
   				total.${column.productType.productCode} = numberWithCommas(tempTotal.toFixed(2));
   				blankRow.${column.productType.productCode} = " ";
   				exceptionsHeader.${column.productType.productCode} = " ";
   				exceptionsReversal.${column.productType.productCode} = 
					<c:choose>
						<c:when test="${isConfirmed}">
							alignRightCell(numberWithCommas(parseFloat("${column.totalReversalAmt}").toFixed(2)));
						</c:when>
						<c:otherwise>   						
		   					<c:choose>
								<c:when test="${column.totalReversalAmt == 0.0}">
									centerCell("<a class=\"blue\" href=\"javascript:showAddEditReversalForm('${column.id}','${column.productType.currency.id}','${column.productType.productName}','add','${column.productType.company.name}')\"><img src=\"${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/images/UndoButton.png\"></a>");	
								</c:when>
								<c:otherwise>
									alignRightCell("<a href=\"javascript:showReversalsTable('${column.productType.productCode}','${column.productType.company.name}')\">" + numberWithCommas(parseFloat("${column.totalReversalAmt}").toFixed(2)) + "</a>");
								</c:otherwise>
							</c:choose>
						</c:otherwise>
					</c:choose>
   	</script>
</c:forEach>
<script>
	exceptionsReversal.gafPeso = centerCell("<img src=\"${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/images/UndoButton.png\">");
	slfpiTableBlankRow.push(blankRow);
	var gafPesoField = [{name: "GAF PESO", field: "gafPeso", width:"15%"}];
	var fillerField = [{name: " ", field: "filler2", width:"75%"}];
	appendJsons([slfpiTableLayout,fillerField]);	
/*	totalCashCounter.gafPeso = "0.00";
	totalCashNonCounter.gafPeso = "0.00";
	totalCheckOnUs.gafPeso = "0.00";
	totalCheckLocal.gafPeso = "0.00";
	totalCheckRegional.gafPeso = "0.00";
	totalCheckNonCounter.gafPeso = "0.00";
	totalCheckOT.gafPeso = "0.00";
	totalNonCash.gafPeso = "0.00";
	cardTypes.gafPeso = " ";
	totalPosBpi.gafPeso = "0.00";
	totalPosCtb.gafPeso = "0.00";
	totalPosHsbc.gafPeso = "0.00";
	totalPosScb.gafPeso = "0.00";
	totalPosRcbc.gafPeso = "0.00";
	totalPosBdo.gafPeso = "0.00";
	total.gafPeso = "0.00";*/
	totalCashCounter.filler2 = " ";
	totalCashNonCounter.filler2 = " ";
	totalCheckGaf.filler2 = " ";
	totalCheckOnUs.filler2 = " ";
	totalCheckLocal.filler2 = " ";
	totalCheckRegional.filler2 = " ";
	totalCheckNonCounter.filler2 = " ";
	totalCheckOT.filler2 = " ";
	cardTypes.filler2 = " ";
	totalNonCash.filler2 = " ";
	totalCardGaf.filler2 = " ";
	totalPosBpi.filler2 = " ";
	totalPosCtb.filler2 = " ";
	totalPosHsbc.filler2 = " ";
	totalPosScb.filler2 = " ";
	totalPosRcbc.filler2 = " ";
	totalPosBdo.filler2 = " ";
	total.filler2 = " ";
	var slfpiData =[];
	/*slfpiData.push(totalCashCounter);
	slfpiData.push(totalCashNonCounter);
	slfpiData.push(totalCheckOnUs);
	slfpiData.push(totalCheckLocal);
	slfpiData.push(totalCheckRegional);
	slfpiData.push(totalCheckOT);
	slfpiData.push(totalCheckNonCounter);
	slfpiData.push(totalNonCash);
	slfpiData.push(cardTypes);
	slfpiData.push(totalPosBpi);
	slfpiData.push(totalPosCtb);
	slfpiData.push(totalPosHsbc);
	slfpiData.push(totalPosScb);
	slfpiData.push(totalPosRcbc);
	slfpiData.push(totalPosBdo);*/
	if(checkForValue(totalCashCounter)){slfpiData.push(totalCashCounter);dataFlag=true;};
	if(checkForValue(totalCashNonCounter)){slfpiData.push(totalCashNonCounter);dataFlag=true;};
	if(checkForValue(totalCheckGaf)){slfpiData.push(totalCheckGaf);dataFlag=true;};
	if(checkForValue(paymentFromInsurer)){slfpiData.push(paymentFromInsurer);dataFlag=true;};
	if(checkForValue(totalCheckOnUs)){slfpiData.push(totalCheckOnUs);dataFlag=true;};
	if(checkForValue(totalCheckLocal)){slfpiData.push(totalCheckLocal);dataFlag=true;};
	if(checkForValue(totalCheckRegional)){slfpiData.push(totalCheckRegional);dataFlag=true;};
	if(checkForValue(totalPmo)){slfpiData.push(totalPmo);dataFlag=true;};
	if(checkForValue(totalCheckNonCounter)){slfpiData.push(totalCheckNonCounter);dataFlag=true;};
	if(checkForValue(totalCheckOT)){slfpiData.push(totalCheckOT);dataFlag=true;};
	if(checkForValue(totalNonCash)){slfpiData.push(totalNonCash);dataFlag=true;};
//	if(checkForValue(totalCreditMemo)){slfpiData.push(totalCreditMemo);dataFlag=true;};
	if(checkForValue(totalUsCheckInManila)){slfpiData.push(totalUsCheckInManila);dataFlag=true;};
	if(checkForValue(totalUsCheckOutPh)){slfpiData.push(totalUsCheckOutPh);dataFlag=true;};
	if(checkForValue(totalDollarCheque)){slfpiData.push(totalDollarCheque);dataFlag=true;};
	//if(checkForValue(totalBankOTCCheckPayment)){slfpiData.push(totalBankOTCCheckPayment);dataFlag=true;};
	slfpiData.push(cardTypes);
	var cardTypeFlag = false;
	if(checkForValue(totalCardGaf)){slfpiData.push(totalCardGaf); cardTypeFlag=true;};
	if(checkForValue(totalPosBpi)){slfpiData.push(totalPosBpi); cardTypeFlag=true;};
	if(checkForValue(totalPosCtb)){slfpiData.push(totalPosCtb); cardTypeFlag=true;};
	if(checkForValue(totalPosHsbc)){slfpiData.push(totalPosHsbc); cardTypeFlag=true;};
	if(checkForValue(totalPosScb)){slfpiData.push(totalPosScb); cardTypeFlag=true;};
	if(checkForValue(totalPosRcbc)){slfpiData.push(totalPosRcbc); cardTypeFlag=true;};
	if(checkForValue(totalPosBdo)){slfpiData.push(totalPosBdo); cardTypeFlag=true;};
	if(checkForValue(totalMdsBpi)){slfpiData.push(totalMdsBpi); cardTypeFlag=true;};
	if(checkForValue(totalMdsCtb)){slfpiData.push(totalMdsCtb); cardTypeFlag=true;};
	if(checkForValue(totalMdsHsbc)){slfpiData.push(totalMdsHsbc); cardTypeFlag=true;};
	if(checkForValue(totalMdsSbc)){slfpiData.push(totalMdsSbc); cardTypeFlag=true;};
	if(checkForValue(totalMdsRcbc)){slfpiData.push(totalMdsRcbc); cardTypeFlag=true;};
	if(checkForValue(totalMdsBdo)){slfpiData.push(totalMdsBdo); cardTypeFlag=true;};
	if(checkForValue(totalAutoCtb)){slfpiData.push(totalAutoCtb); cardTypeFlag=true;};
	if(checkForValue(totalAutoScb)){slfpiData.push(totalAutoScb); cardTypeFlag=true;};
	if(checkForValue(totalAutoRcbc)){slfpiData.push(totalAutoRcbc); cardTypeFlag=true;};
	if(checkForValue(totalSunlinkOnline)){slfpiData.push(totalSunlinkOnline); cardTypeFlag=true;};
	if(checkForValue(totalSunlinkHsbcPeso)){slfpiData.push(totalSunlinkHsbcPeso); cardTypeFlag=true;};
	if(checkForValue(totalEpsBpi)){slfpiData.push(totalEpsBpi); cardTypeFlag=true;};
	if(!cardTypeFlag){
		for(a in slfpiData){
			if(slfpiData[a] == cardTypes){
				delete slfpiData[a];
			}
		}
	}
	
	if(dataFlag || cardTypeFlag){
		slfpiData.push(total);
		slfpiExceptions.push(exceptionsHeader);
		slfpiExceptions.push(exceptionsReversal);
	}
</script>
<div class="overflow" id="slfpiTableHeader" >
	<div id="slfpiTableHeaderNode">		
	</div>
	<form id="slfpiForm" name="slfpiForm" 
		action="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/slfpi/confirm.wms" 
		method="post">
	<div id="confirmButtonDiv">
	</div>	
	<input type="hidden" name="dcrCashierId" value="${dcrCashierBO.id}"/>
	<input type="hidden" id="htmlCode" name="htmlCode" value=""/>
	</form>
</div>
<script>
setLoadData(slfpiData);
if(${isConfirmed}){
	$(document).ready(function(){
	var confirmButton = document.getElementById("confirmButton");
	confirmButton.disabled = true;
	}
	);
}
</script>