<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<link rel="stylesheet" href="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/css/jobData.css" media="screen">
<link rel="stylesheet" href="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/css/pagination.css" media="screen">
<link rel="stylesheet" href="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/jquery/jquery-ui-1.9.1/themes/base/jquery-ui.css" />
<script src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/jquery/jquery-1.8.2.js"></script>
<script src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/jquery/jquery-ui-1.9.1/ui/jquery-ui.js"></script>
<script src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/js/custom-table.js"></script>
<script src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/js/sorttable.js"></script>
<script src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/js/common.js"></script>
<script src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/js/pagination.js"></script>
<script src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/js/jobData.js"></script>
<c:forEach items="${jobs}" var="job" varStatus="rowCnt">
	<script>
	$(document).ready(function(){
		var tempStat;
		var tempHref;
		if(${job.successful}){
			tempStat = "<div class='green'>Successful</div>";
		} else{
			tempStat = "<div class='red'>Failed</div>";
		}
		if(${job.id} == 1){
			tempHref = "<a href='#' onclick='openDatePickerWindow()'>Execute Manually</a>";
		} else{
			tempHref = "<a href='${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/jobData/execute.html?jobDataId=${job.id}'>Execute Manually</a>";
		}
		var tempJob = {id:"${job.id}",jobName: "${job.jobName}",status: tempStat,lastRun: "${job.lastRun}",action:tempHref};
		
		jobData.push(tempJob);
	});	
	</script>
</c:forEach>
<div id="jobDataDiv" hidden="true">
	<div id="jobTabletTitle" class="tableTitle">
		DCR JOBS
	</div>
	<div id="jobDataNode">		
	</div>
</div>
<script>
showResultsTable(jobData);
</script>