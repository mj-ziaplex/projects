<link rel="stylesheet" href="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/jquery/jquery-ui-1.9.1/themes/base/jquery-ui.css" />
<script src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/jquery/jquery-1.8.2.js"></script>
<script src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/jquery/jquery-ui-1.9.1/ui/jquery-ui.js"></script>
 <script>
 	if(${isDcrCreationExecuted}){
 		window.close();
 		window.opener.location.reload();
 	}	
 	function setDcrDateStr(val){
		var dcrDateStr = document.getElementById("dcrDate");
		dcrDateStr.value = val;
	}
 	function clearDate(){
 		var searchDatePicker = document.getElementById("searchDatePicker");
 		var dcrDate = document.getElementById("dcrDate");
 		dcrDate.value = "";
 		searchDatePicker.value = "";
 	}
    $(function() {
        $( "#searchDatePicker" ).datepicker({ dateFormat: 'ddMyy' });
    }); 
    function submitForm(){
    	var dcrCreationForm = document.getElementById("dcrCreationForm");
    	var executeButton = document.getElementById("execute");
    	executeButton.disabled = true;
    	executeButton.value = "Loading...";
    	dcrCreationForm.submit();
    }
 </script>
 <form id="dcrCreationForm" name="dcrCreationForm" action="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/jobData/execute.html" method="post">
 		<input type="text" id="searchDatePicker" name="searchDatePicker" readonly="true"" onchange="setDcrDateStr(this.value);"/>
		 <input type="hidden" id="dcrDate" name="dcrDate"/>
		 <input type="hidden" id="jobDataId" name="jobDataId" value ="1"/>
		 <a href="#" onclick="clearDate()">Clear</a> (dd/MMM/yyyy)<br>		 
		 <input type="button" id="execute" onclick="submitForm()" value="Execute"/>
 </form>
 