<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<!-- Added for MR-WF-16-00221 - Change For safekeeping in VDIL -->
<jsp:useBean id="dateDoNotUse" class="java.util.Date" />
<c:set var="url" value="${pageContext.request.contextPath}" scope="request"/>
<c:set var="jsNoCache" value="?jsNoCache=${dateDoNotUse.time}" scope="request"/>

<link href="${url}/css/common.css" media="screen" rel="stylesheet" type="text/css" >
<link href="${url}/css/createVDIL.css" media="screen" rel="stylesheet" type="text/css">
<link href="${url}/jquery/jquery-ui-1.9.1/themes/base/jquery-ui.css" rel="stylesheet"/>
<script src="${url}/jquery/jquery-1.8.2.js"></script>
<script src="${url}/jquery/jquery-ui-1.9.1/ui/jquery-ui.js"></script>
<script src="${url}/js/extraFunctions.js${jsNoCache}"></script>
<script src="${url}/js/common.js${jsNoCache}"></script>
<script src="${url}/js/createVDIL.js${jsNoCache}"></script>

<script>
$(function() {
    $( ".dcrDate" ).datepicker({ dateFormat: 'ddMyy' });
    //returns and displays as ddMMMyyyy
    $( ".pickupDate" ).datepicker({ dateFormat: 'ddMyy' });
        
    var onEditCallback = function(remaining){
            $(this).siblings('.charsRemain').text("Characters remaining: " + remaining);
            if(remaining > 0){
                    $(this).css('background-color', 'white');
            }
    }
        
    var onLimitCallback = function(){
            $(this).css('background-color', 'red');
    }
        
    $('textarea[maxlength]').limitMaxlength({
            onEdit: onEditCallback,
            onLimit: onLimitCallback
    });
});
</script>

<div id="rcvdPrevDayGridDiv">
	<table id="rcvdPrevDayGrid">
			<tr>
				<th>Denomination</th>
				<th>Pieces Received</th>
				<th>Total Amount</th>
			</tr>
			<tr>
				<td>1 coin</td>
				<td>${vdil.previousCoinCollection.denom1}</td><c:set var="rcvdPrevDay1coinTotal" value="${vdil.previousCoinCollection.denom1 * 1}"/>
				<td><fmt:formatNumber type="number" value="${rcvdPrevDay1coinTotal}" maxFractionDigits="2" minFractionDigits="2" /></td>
			</tr>
			<tr>
				<td>0.50</td>
				<td>${vdil.previousCoinCollection.denom50c}</td><c:set var="rcvdPrevDay50cTotal" value="${vdil.previousCoinCollection.denom50c * 0.50}"/>
				<td><fmt:formatNumber type="number" value="${rcvdPrevDay50cTotal}" maxFractionDigits="2" minFractionDigits="2" /></td>
			</tr>
			<tr>
				<td>0.25</td>
				<td>${vdil.previousCoinCollection.denom25c}</td><c:set var="rcvdPrevDay25cTotal" value="${vdil.previousCoinCollection.denom25c * 0.25}"/>
				<td><fmt:formatNumber type="number" value="${rcvdPrevDay25cTotal}" maxFractionDigits="2" minFractionDigits="2" /></td>
			</tr>
			<tr>
				<td>0.10</td>
				<td>${vdil.previousCoinCollection.denom10c}</td><c:set var="rcvdPrevDay10cTotal" value="${vdil.previousCoinCollection.denom10c * 0.10}"/>
				<td><fmt:formatNumber type="number" value="${rcvdPrevDay10cTotal}" maxFractionDigits="2" minFractionDigits="2" /></td>
			</tr>
			<tr>
				<td>0.05</td>
				<td>${vdil.previousCoinCollection.denom5c}</td><c:set var="rcvdPrevDay5cTotal" value="${vdil.previousCoinCollection.denom5c * 0.05}"/>
				<td><fmt:formatNumber type="number" value="${rcvdPrevDay5cTotal}" maxFractionDigits="2" minFractionDigits="2" /></td>
			</tr>
			<tr>
				<td>0.01</td>
				<td>${vdil.previousCoinCollection.denom1c}</td><c:set var="rcvdPrevDay1cTotal" value="${vdil.previousCoinCollection.denom1c * 0.01}"/>
				<td><fmt:formatNumber type="number" value="${rcvdPrevDay1cTotal}" maxFractionDigits="2" minFractionDigits="2" /></td>
			</tr>
			<tr>
				<td class="grandtotalCell">Grand Total:</td>
					<c:set var="rcvdPrevDayPcsTotal" value="${vdil.previousCoinCollection.denom1 + vdil.previousCoinCollection.denom50c + vdil.previousCoinCollection.denom25c + vdil.previousCoinCollection.denom10c + vdil.previousCoinCollection.denom5c + vdil.previousCoinCollection.denom1c}"/>
					<c:set var="rcvdPrevDayAmtTotal" value="${rcvdPrevDay1coinTotal + rcvdPrevDay50cTotal + rcvdPrevDay25cTotal + rcvdPrevDay10cTotal + rcvdPrevDay5cTotal + rcvdPrevDay1cTotal}"/>
				<td class="grandtotalCell"><fmt:formatNumber type="number" groupingUsed="false" value="${rcvdPrevDayPcsTotal}"/></td>
				<td class="grandtotalCell"><fmt:formatNumber type="number" value="${rcvdPrevDayAmtTotal}" maxFractionDigits="2" minFractionDigits="2" /></td>
			</tr>
	</table>
</div>

<div id="rcvdTodayFormDiv">
	<table id="rcvdTodayTable">
		<tr>
			<th>Denomination</th>
			<th>Pcs PrevDay</th>
			<th>Pcs Received</th>
			<th>Amt Received</th>
			<th>Pcs Changed</th>
			<th>Amt Changed</th>
			<th>Net Pieces</th>
			<th>Net Amount</th>
		</tr>
		<!-- ibalik ung mga value sa zero pieces? -->
		<tr>
			<td>1</td>
			<td>${vdil.previousCoinCollection.denom1}</td>
			<td><input class="decimalAlign inputPcs" type="text" id="temp_in_denom1" name="temp_in_denom1" value="${vdil.dollarCoinExchange.incomingDenom1}" onchange="saveAndFormatNumber(this);calculateBalance(1);" /></td>
			<td>0.00</td>
			<td><input class="decimalAlign inputPcs" type="text" id="temp_out_denom1" name="temp_out_denom1" value="${vdil.dollarCoinExchange.outgoingDenom1}" onchange="saveAndFormatNumber(this);calculateBalance(1);" /></td>
			<td>0.00</td>
			<td>${vdil.previousCoinCollection.denom1}</td>
			<td><fmt:formatNumber type="number" value="${rcvdPrevDay1coinTotal}" maxFractionDigits="2" minFractionDigits="2" /></td>
		</tr>
		<tr>
			<td>0.50</td>
			<td>${vdil.previousCoinCollection.denom50c}</td>
			<td><input class="decimalAlign inputPcs" type="text" id="temp_in_denom50c" name="temp_in_denom50c" value="${vdil.dollarCoinExchange.incomingDenom50c}" onchange="saveAndFormatNumber(this);calculateBalance(2);" /></td>
			<td>0.00</td>
			<td><input class="decimalAlign inputPcs" type="text" id="temp_out_denom50c" name="temp_out_denom50c" value="${vdil.dollarCoinExchange.outgoingDenom50c}" onchange="saveAndFormatNumber(this);calculateBalance(2);" /></td>
			<td>0.00</td>
			<td>${vdil.previousCoinCollection.denom50c}</td>
			<td><fmt:formatNumber type="number" value="${rcvdPrevDay50cTotal}" maxFractionDigits="2" minFractionDigits="2" /></td>
		</tr>
		<tr>
			<td>0.25</td>
			<td>${vdil.previousCoinCollection.denom25c}</td>
			<td><input class="decimalAlign inputPcs" type="text" id="temp_in_denom25c" name="temp_in_denom25c" value="${vdil.dollarCoinExchange.incomingDenom25c}" onchange="saveAndFormatNumber(this);calculateBalance(3);" /></td>
			<td>0.00</td>
			<td><input class="decimalAlign inputPcs" type="text" id="temp_out_denom25c" name="temp_out_denom25c" value="${vdil.dollarCoinExchange.outgoingDenom25c}" onchange="saveAndFormatNumber(this);calculateBalance(3);" /></td>
			<td>0.00</td>
			<td>${vdil.previousCoinCollection.denom25c}</td>
			<td><fmt:formatNumber type="number" value="${rcvdPrevDay25cTotal}" maxFractionDigits="2" minFractionDigits="2" /></td>
		</tr>
		<tr>
			<td>0.10</td>
			<td>${vdil.previousCoinCollection.denom10c}</td>
			<td><input class="decimalAlign inputPcs" type="text" id="temp_in_denom10c" name="temp_in_denom10c" value="${vdil.dollarCoinExchange.incomingDenom10c}" onchange="saveAndFormatNumber(this);calculateBalance(4);" /></td>
			<td>0.00</td>
			<td><input class="decimalAlign inputPcs" type="text" id="temp_out_denom10c" name="temp_out_denom10c" value="${vdil.dollarCoinExchange.outgoingDenom10c}" onchange="saveAndFormatNumber(this);calculateBalance(4);" /></td>
			<td>0.00</td>
			<td>${vdil.previousCoinCollection.denom10c}</td>
			<td><fmt:formatNumber type="number" value="${rcvdPrevDay10cTotal}" maxFractionDigits="2" minFractionDigits="2" /></td>
		</tr>
		<tr>
			<td>0.05</td>
			<td>${vdil.previousCoinCollection.denom5c}</td>
			<td><input class="decimalAlign inputPcs" type="text" id="temp_in_denom5c" name="temp_in_denom5c" value="${vdil.dollarCoinExchange.incomingDenom5c}" onchange="saveAndFormatNumber(this);calculateBalance(5);" /></td>
			<td>0.00</td>
			<td><input class="decimalAlign inputPcs" type="text" id="temp_out_denom5c" name="temp_out_denom5c" value="${vdil.dollarCoinExchange.outgoingDenom5c}" onchange="saveAndFormatNumber(this);calculateBalance(5);" /></td>
			<td>0.00</td>
			<td>${vdil.previousCoinCollection.denom5c}</td>
			<td><fmt:formatNumber type="number" value="${rcvdPrevDay5cTotal}" maxFractionDigits="2" minFractionDigits="2" /></td>
		</tr>
		<tr>
			<td>0.01</td>
			<td>${vdil.previousCoinCollection.denom1c}</td>
			<td><input class="decimalAlign inputPcs" type="text" id="temp_in_denom1c" name="temp_in_denom1c" value="${vdil.dollarCoinExchange.incomingDenom1c}" onchange="saveAndFormatNumber(this);calculateBalance(6);" /></td>
			<td>0.00</td>
			<td><input class="decimalAlign inputPcs" type="text" id="temp_out_denom1c" name="temp_out_denom1c" value="${vdil.dollarCoinExchange.outgoingDenom1c}" onchange="saveAndFormatNumber(this);calculateBalance(6);" /></td>
			<td>0.00</td>
			<td>${vdil.previousCoinCollection.denom1c}</td>
			<td><fmt:formatNumber type="number" value="${rcvdPrevDay1cTotal}" maxFractionDigits="2" minFractionDigits="2" /></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td class="grandtotalCell">Total Amt Received:</td>
			<td>0.00</td>
			<td class="grandtotalCell">Total Amt Changed:</td>
			<td>0.00</td>
			<td class="grandtotalCell">Net Collection:</td>
			<td class="grandtotalCell"><fmt:formatNumber type="number" value="${rcvdPrevDayAmtTotal}" maxFractionDigits="2" minFractionDigits="2" /></td>
		</tr>
	</table>
</div>

<form id="vdilForm" action="${pageContext.request.contextPath}/vdil/vdilSave.html?dcrCashierId=${dcrCashierBO.id}" method="post">
    <fieldset class="customFieldset">
    	<input type="hidden" id="vdilId" name="vdilId" value="${vdil.id}"/>
        <h2 class="header">MISCELLANEOUS CASH ON HAND</h2>
        <ul>                                                
            <li><!-- format is 00,000.00 -->
                <label for="pettyCash">PETTY CASH</label>
                <input id="pettyCash" name="pettyCash" type="text" class="decimalAlign"
                	value="<fmt:formatNumber type="number" value="${vdil.pettyCash}" maxFractionDigits="2" minFractionDigits="2"/>" onchange="saveAndFormatDecimal(this)" />
            </li>
            
            <li>
                <label for="photocopy">PHOTOCOPY</label>
                <input id="photocopy" name="photocopy" type="text" class="decimalAlign"
                	value="<fmt:formatNumber type="number" value="${vdil.photocopy}" maxFractionDigits="2" minFractionDigits="2"/>" onchange="saveAndFormatDecimal(this)" />
            </li>
            
            <li>
                <label for="workingFund">WORKING FUND</label>
                <input id="workingFund" name="workingFund" type="text" class="decimalAlign"
                	value="<fmt:formatNumber type="number" value="${vdil.workingFund}" maxFractionDigits="2" minFractionDigits="2"/>" onchange="saveAndFormatDecimal(this)" />
            </li>
            
            <li>
                <label for="unclaimedChange">UNCLAIMED CHANGE</label>
                <input id="unclaimedChange" name="unclaimedChange" type="text" class="decimalAlign"
                	value="<fmt:formatNumber type="number" value="${vdil.unclaimedChange}" maxFractionDigits="2" minFractionDigits="2"/>" onchange="saveAndFormatDecimal(this)" />
            </li>
            
            <li>
                <label for="overages">OVERAGES</label>
                <input id="overages" name="overages" type="text" class="decimalAlign"
                	value="<fmt:formatNumber type="number" value="${vdil.overages}" maxFractionDigits="2" minFractionDigits="2"/>" onchange="saveAndFormatDecimal(this)" />
            </li>
        </ul>
        
        <h2 class="header">US DOLLAR COIN COLLECTION</h2>
                
        <ul>
            <li><!-- +Hyperlink- Pop out window of dollar coin denomination from Depo Slip from current date -->
                <label class="popUpLink" for="rcvdPrevDay" onclick="javascript:showPrevDayRcvdGrid();">RCVD FROM PREV DAY</label>
                <input class="decimalAlign" id="rcvdPrevDay" name="rcvdPrevDay" readonly="readonly"  type="text" value="0.00" onchange="saveAndFormatDecimal(this);computeDollarCoinTotal();" />
                <script>
                		var rcvdPrevDayAmtTotal = "${rcvdPrevDayAmtTotal}";
                		document.getElementById("rcvdPrevDay").value = parseFloat(rcvdPrevDayAmtTotal).toFixed(2);
                </script>
                <!-- ^Single hyperlink for all dollar coin collection -->
            </li>
            <li><!-- like above -->
                <label class="popUpLink" for="rcvdToday" onclick="javascript:showTodayRcvdForm();">RCVD TODAY</label>
                <input class="decimalAlign" id="rcvdToday" name="rcvdToday" readonly="readonly"  type="text" value="0.00" onchange="saveAndFormatDecimal(this);computeDollarCoinTotal();" />
            </li>
            <li><!-- format is 00.00 -->
                <label for="lessChangeForTheDay">LESS CHANGE FOR THE DAY</label>
                <input class="decimalAlign" id="lessChangeForTheDay" name="lessChangeForTheDay" readonly="readonly" type="text" value="0.00" onchange="saveAndFormatDecimal(this);computeDollarCoinTotal();" />
            </li>
            <li><!-- A+B-C, 00.00 -->
                <label for="totalDollarCoins">TOTAL</label>
                <input class="decimalAlign" id="totalDollarCoins" name="totalDollarCoins" type="text" readonly="readonly" value="0.00"/>
            </li>
        </ul>
        
        <script>
			//call calculateBalance para kung may hindi hardcoded zero ung mga value
            // ay tatama ung mga AMT <td>0.00</td> cols
			calculateBalance(1);
			calculateBalance(2);
			calculateBalance(3);
			calculateBalance(4);
			calculateBalance(5);
			calculateBalance(6);
			computeDollarCoinTotal();
		</script>
		
        <h2 class="header">PDC-PENDING FOR WAREHOUSE</h2>
        <!-- non Decimal Input and a+b=c -->
		  <label for="vultradPDCRPendingWarehouse" class="columnStyle">
		      <span>VUL/TRAD</span>
		    <input type="text" class="decimalAlign" id="vultradPDCRPendingWarehouse" name="vultradPDCRPendingWarehouse" onchange="saveAndFormatNumber(this);computeWarehouseTotal();"
		    value="<fmt:formatNumber type="number" groupingUsed="false" value="${vdil.vultradPDCRPendingWarehouse}"/>" />
		  </label>
		  
		  <label for="pnPDCPendingWarehouse" class="columnStyle">
		    <span>PN</span>
		    <input type="text" class="decimalAlign" id="pnPDCPendingWarehouse" name="pnPDCPendingWarehouse" onchange="saveAndFormatNumber(this);computeWarehouseTotal();"
		    value="<fmt:formatNumber type="number" groupingUsed="false" value="${vdil.pnPDCPendingWarehouse}"/>" />
		  </label>
		  
		  <label for="warehouseTotal" class="columnStyle">
		    <span>TOTAL</span><!-- OUTPUT <99> Total no of pieces  -->
		    <input readonly="readonly" type="text" class="decimalAlign" id="warehouseTotal" name="warehouseTotal" 
		    value="<c:out value="${vdil.vultradPDCRPendingWarehouse + vdil.pnPDCPendingWarehouse}"/>" />
		  </label>
		  
        <h2 class="header">PR SERIES NUMBER USED</h2>
        <table id="prSeriesGrid">
	        <tr>
	        	<th class="headcell">Company</th>
	        	<th class="headcell">From</th>
	        	<th class="headcell">To</th>
	        	<th class="headcell">Reason</th>
	        </tr>
	        
	        <c:forEach items="${vdil.prSeriesNumbers}" var="prSeriesNumber" varStatus="rowCounter" begin="0" end="3">
	        <input type="hidden" id="prSeriesNumberId${rowCounter.index}" name="prSeriesNumberId${rowCounter.index}" value="${prSeriesNumber.id}"/>
	        <tr>
				<td class="cell">
				<select id="companyId${rowCounter.index}" name="companyId${rowCounter.index}">
					<option
					<c:if test="${prSeriesNumber.companyId eq null}">selected="selected"</c:if> 
					value="0"></option>
					<option
					<c:if test="${prSeriesNumber.companyId eq 1}">selected="selected"</c:if> 
					value="1">SLAMCI Peso</option>
					<option
					<c:if test="${prSeriesNumber.companyId eq 2}">selected="selected"</c:if> 
					value="2">SLAMCI Dollar</option>
					<option
					<c:if test="${prSeriesNumber.companyId eq 3}">selected="selected"</c:if> 
					value="3">SLOCPI</option>
					<option
					<c:if test="${prSeriesNumber.companyId eq 4}">selected="selected"</c:if> 
					value="4">SLGFI</option>
					<option
					<c:if test="${prSeriesNumber.companyId eq 5}">selected="selected"</c:if> 
					value="5">SLFPI</option>
					<option
					<c:if test="${prSeriesNumber.companyId eq 6}">selected="selected"</c:if> 
					value="6">OTHERS</option>
				</select>
				</td>
	        	<td class="cell"><input class="decimalAlign" id="from${rowCounter.index}" name="from${rowCounter.index}" type="text" onchange="saveAndFormatNumber(this);"
	        	value="<fmt:formatNumber type="number" groupingUsed="false" value="${prSeriesNumber.from}"/>" /></td>
	        	<td class="cell"><input class="decimalAlign" id="to${rowCounter.index}" name="to${rowCounter.index}" type="text" onchange="saveAndFormatNumber(this);"
	        	value="<fmt:formatNumber type="number" groupingUsed="false" value="${prSeriesNumber.to}"/>" /></td>
	        	<td class="cell"><input id="reason${rowCounter.index}" name="reason${rowCounter.index}" type="text" value="${prSeriesNumber.reason}" /></td>
	        </tr>	        
	        </c:forEach>
        </table>
        
		<h2 class="header">AFTER CUTOFF COLLECTION</h2>
		<ul>                                                
            <li>
                <label class="oneThird" for="afterCutoffCashPeso">CASH</label>
                <input class="oneThird decimalAlign PhpOverlay" id="afterCutoffCashPeso" name="afterCutoffCashPeso" type="text" onchange="saveAndFormatDecimal(this);"
                value="<fmt:formatNumber type="number" value="${vdil.afterCutoffCashPeso}" maxFractionDigits="2" minFractionDigits="2" />" />
                <input class="oneThird decimalAlign USDOverlay" id="afterCutoffCashDollar" name="afterCutoffCashDollar" type="text" onchange="saveAndFormatDecimal(this);"
                value="<fmt:formatNumber type="number" value="${vdil.afterCutoffCashDollar}" maxFractionDigits="2" minFractionDigits="2" />" />
            </li>
            
            <li><!-- pang-Dollar pala tong dalawa:, align right? -->
                <label class="oneThird" for="afterCutoffCheck">CHEQUE</label><!-- Input no of pcs -->
                <input class="oneThird decimalAlign PcsOverlay" id="afterCutoffCheck" name="afterCutoffCheck" type="text" onchange="saveAndFormatNumber(this);"
                value="<fmt:formatNumber type="number" groupingUsed="false" value="${vdil.afterCutoffCheck}"/>" />
            </li>
            
            <li>
                <label class="oneThird" for="afterCutoffCard">CARD</label><!-- Input no of sale slip -->
                <input class="oneThird decimalAlign PcsOverlay" id="afterCutoffCard" name="afterCutoffCard" type="text" onchange="saveAndFormatNumber(this);"
                value="<fmt:formatNumber type="number" groupingUsed="false" value="${vdil.afterCutoffCard}"/>" />
            </li>
        </ul>
		
		<h2 class="header">ISO COLLECTION</h2>
        <table id="isoCollectionGrid">
	        <tr>
	        	<th class="headcell">DCR Date</th>
	        	<th class="headcell">PickUp Date</th>
	        	<th class="headcell">Amount</th><!-- free text daw eh, but decimal for now-->
	        </tr>
	        
	        <c:forEach items="${vdil.isoCollections}" var="isoCollection" varStatus="rowCounter" begin="0" end="3">
	        <input type="hidden" id="isoCollectionId${rowCounter.index}" name="isoCollectionId${rowCounter.index}" value="${isoCollection.id}"/>
	        <tr>
	        	<td class="cell"><input class="dcrDate" readonly="readonly" id="dcrDate${rowCounter.index}" name="dcrDate${rowCounter.index}" type="text" value="${isoCollection.dcrDateStr}" /></td>
				<td class="cell"><input class="pickupDate" readonly="readonly" id="pickupDate${rowCounter.index}" name="pickupDate${rowCounter.index}" type="text" value="${isoCollection.pickupDateStr}" /></td>
	        	<td class="cell"><input class="decimalAlign" id="amount${rowCounter.index}" name="amount${rowCounter.index}" type="text" onchange="saveAndFormatDecimal(this)"
	        	value="<fmt:formatNumber type="number" value="${isoCollection.amount}" maxFractionDigits="2" minFractionDigits="2" />" /></td>
	        </tr>
	        </c:forEach>

        </table>
        
        <!-- Modified for MR-WF-16-00221 - Change For safekeeping in VDIL -->
        <h2 class="header">REMARKS</h2>
        <span class="charsRemain"></span>
        <!-- Modified for MR-WF-16-00221 - Change For safekeeping in VDIL -->
		<textarea rows="5" cols="45" class="safekeepingTextArea" id="forSafeKeeping" name="forSafeKeeping" maxlength="400">${vdil.forSafeKeeping}</textarea>
		<!-- The system will have a SAVE and Confirm button to indicate the completion of the prep screen and to 
        	associate the VDIL to the DCR date. When confirmed, 
			the system will create a VDIL output forms that will be printable. -->
        <input type="button" class="submitBtns" id="dcrSave" name="dcrSave" value="SAVE" onclick="saveVDIL();"/>
		<input type="button" class="submitBtns" id="dcrPreview" name="dcrPreview" value="PREVIEW" onclick="previewVDILPDF();"/>
    </fieldset>
	
	<div id="realFormInputsHidden">
		<!-- this form inputs must be separated from rcvdTodayFormDiv-->
		<input type="hidden" id="in_denom1" name="in_denom1" value="0"/>
		<input type="hidden" id="out_denom1" name="out_denom1" value="0"/>
		
		<input type="hidden" id="in_denom50c" name="in_denom50c" value="0"/>
		<input type="hidden" id="out_denom50c" name="out_denom50c" value="0"/>
		
		<input type="hidden" id="in_denom25c" name="in_denom25c" value="0"/>
		<input type="hidden" id="out_denom25c" name="out_denom25c" value="0"/>
		
		<input type="hidden" id="in_denom10c" name="in_denom10c" value="0"/>
		<input type="hidden" id="out_denom10c" name="out_denom10c" value="0"/>
		
		<input type="hidden" id="in_denom5c" name="in_denom5c" value="0"/>
		<input type="hidden" id="out_denom5c" name="out_denom5c" value="0"/>
		
		<input type="hidden" id="in_denom1c" name="in_denom1c" value="0"/>
		<input type="hidden" id="out_denom1c" name="out_denom1c" value="0"/>
	</div>
	
</form>

<script>
if(${isSaved}){
	$(document).ready(function(){
		var saveButton = document.getElementById("dcrSave");
		saveButton.disabled = true;
	});
}
</script>