<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<!-- Added for MR-WF-16-00034 - Random sampling for QA -->
<jsp:useBean id="dateDoNotUse" class="java.util.Date" />
<c:set var="url" value="${pageContext.request.contextPath}" scope="request"/>
<c:set var="jsNoCache" value="?jsNoCache=${dateDoNotUse.time}" scope="request"/>

<link rel="stylesheet" href="${url}/css/manager.css" media="screen">
<link rel="stylesheet" href="${url}/css/pagination.css" media="screen">
<link rel="stylesheet" href="${url}/jquery/jquery-ui-1.9.1/themes/base/jquery-ui.css" />
<script src="${url}/jquery/jquery-1.8.2.js"></script>
<script src="${url}/jquery/jquery-ui-1.9.1/ui/jquery-ui.js"></script>
<script src="${url}/js/custom-table.js${jsNoCache}"></script>
<script src="${url}/js/manager.js${jsNoCache}"></script>
<script src="${url}/js/sorttable.js${jsNoCache}"></script>
<script src="${url}/js/dateFormatter.js${jsNoCache}"></script>
<script src="${url}/js/common.js${jsNoCache}"></script>
<script src="${url}/js/pagination.js${jsNoCache}"></script>
<script>
 $(function() {
     $( "#searchDatePickerStart" ).datepicker({ dateFormat: 'ddMyy' });
 });   
 
 $(function() {
     $( "#searchDatePickerEnd" ).datepicker({ dateFormat: 'ddMyy' });
 });   
</script>
	
<form id="searchForm" name="searchForm" action="${url}/home/filterManagerWI.html" method="post">
 &nbsp;
<select id="filterSelect" name="filterSelect" onchange="submitForm();">
</select>
 
 <script>
	if(!${isPPA}){
		getQueueSelect("${hub}");	
	} else{
		var sf = document.getElementById("searchForm");
		sf.action = "../home/filterPPAWI.html";
		getPPAQueueSelect();
	}
</script>
 
<div class="overflow" id="searchCriteriaDiv" >
	<div id="searchCriteriaTitle" class="tableTitle">
		Filter
	</div>	
	<table id="searchCriteriaTable">
		<tr>
			<td  class="searchCriteriaTableLabel">
				<label for="searchDatePicker">DCR Date From: </label> 
		 	</td>
		 	<td colspan="2">		 		
		 		<input type="text" id="searchDatePickerStart" name="searchDatePickerStart" readonly="true"" onchange="setDcrStartDate(this.value);"/>
		 		<input type="hidden" id="dcrStartDate" name="dcrStartDate">
		 		<a href="#" onclick="clearStartDate()">Clear</a> (dd/MMM/yyyy)
		 	</td>
		</tr>
		<tr>
			<td  class="searchCriteriaTableLabel">
				<label for="searchDatePicker">DCR Date To: </label> 
		 	</td>
		 	<td colspan="2">		 		
		 		<input type="text" id="searchDatePickerEnd" name="searchDatePickerEnd" readonly="true"" onchange="setDcrEndDate(this.value);"/>
		 		<input type="hidden" id="dcrEndDate" name="dcrEndDate">
		 		<a href="#" onclick="clearEndDate()">Clear</a> (dd/MMM/yyyy)
		 	</td>
		</tr>
		<tr>
			<td>				
				Status<br>
		 	</td>	
		 	<td id="searchCriteriaTableDDSiteCode" colspan="4">	
		 		<select id="status" name="status">
		 			<option value="ALL">ALL</option>
		 		<c:choose>
		 			<c:when test="${not empty statusList}">
						<c:forEach items="${statusList}" var="status" varStatus="rowCnt">
							<option value="${status}">${status}</option>	
						</c:forEach>		 			
		 			</c:when>
		 			<c:otherwise>
		 			</c:otherwise>
		 		</c:choose>
				</select>
		 	</td>
		</tr>
		<tr>
			<td>				
				Site Code<br>
		 	</td>	
		 	<td id="searchCriteriaTableDDSiteCode">	
		 		<select id="ccId" name="ccId">
		 			<option value="ALL">ALL</option>
				</select>
		 	</td>
		 	<td>
		 		<script>
		 			document.write("<input type='button' onclick='submitForm()' value='Filter'/>");
		 			
		 		</script>
		 	</td>
		 	<td>	
		 	</td>
		 	<td>		 		
		 	</td>
		</tr>
	</table>
    <!-- Added for MR-WF-16-00034 - Random sampling for QA -->
    <input type="hidden" id="isPpa" name="isPpa" value="${isPPA}"/>
    <input type="hidden" id="isPpaQa" name="isPpaQa" value="${isPpaQa}"/>
</div>
</form>

<c:forEach items="${customerCenters}" var="cc" varStatus="rowCnt">
<script>
$(document).ready(function(){
	var ccId = document.getElementById("ccId");
	var tempSelect = document.createElement("option");
	tempSelect.innerHTML = "${cc.ccName}";
	tempSelect.value = "${cc.ccId}";
	ccId.appendChild(tempSelect);
});	
</script>
</c:forEach>
<c:forEach items="${dcrObjects}" var="dcr" varStatus="rowCnt">
	<script>
	if(!${isPPA}){
		if("${filter}" == "cashierRecon"){
			if(jQuery.inArray("${dcr.status}",cashierRecon) != -1){
				itemsFound = itemsFound + 1;
				searchTableData.push({ccName: "${dcr.ccName}", dcrDate: "<a class='blue' href='#' splink='${url}/cashierSP/index.html?dcrId=${dcr.id}' onclick='openCashierSP(this);'>${dcr.formatedDateStr}</a>", submittedBy: "${dcr.updateUserId}", dateSubmitted: "${dcr.formattedUpdateDate}", processStatus: "${dcr.status}", requiredCompletionDate: "${dcr.formatedRCDStr}"});
			}
		} else if("${filter}" == "reviewAndApproval"){
			if(jQuery.inArray("${dcr.status}",reviewAndApproval) != -1){
				itemsFound = itemsFound + 1;
				searchTableData.push({ccName: "${dcr.ccName}", dcrDate: "<a class='blue' href='#' splink='${url}/cashierSP/index.html?dcrId=${dcr.id}' onclick='openCashierSP(this);'>${dcr.formatedDateStr}</a>", submittedBy: "${dcr.updateUserId}", dateSubmitted: "${dcr.formattedUpdateDate}", processStatus: "${dcr.status}", requiredCompletionDate: "${dcr.formatedRCDStr}"});
			}
		}
	} else {
		if("${filter}" == "ppaRecon"){
			if(jQuery.inArray("${dcr.status}",ppaRecon) != -1){
				itemsFound = itemsFound + 1;
				searchTableData.push({ccName: "${dcr.ccName}", dcrDate: "<a class='blue' href='#' splink='${url}/ppaSP/index.html?dcrId=${dcr.id}&dcrDt=${dcr.formatedDateStr}' onclick='openCashierSP(this);'>${dcr.formatedDateStr}</a>", submittedBy: "${dcr.updateUserId}", dateSubmitted: "${dcr.formattedUpdateDate}", processStatus: "${dcr.status}", requiredCompletionDate: "${dcr.formatedRCDStr}"});
			}
		} else if("${filter}" == "ppaAbeyance"){
			if(jQuery.inArray("${dcr.status}",ppaAbeyance) != -1){
				itemsFound = itemsFound + 1;
				searchTableData.push({ccName: "${dcr.ccName}", dcrDate: "<a class='blue' href='#' splink='${url}/ppaSP/index.html?dcrId=${dcr.id}&dcrDt=${dcr.formatedDateStr}' onclick='openCashierSP(this);'>${dcr.formatedDateStr}</a>", submittedBy: "${dcr.updateUserId}", dateSubmitted: "${dcr.formattedUpdateDate}", processStatus: "${dcr.status}", requiredCompletionDate: "${dcr.formatedRCDStr}"});
			}
		} // Added for MR-WF-16-00034 - Random sampling for QA
		else if("${filter}" == "ppaQa"){
			if(jQuery.inArray("${dcr.status}",ppaQa) != -1){
				itemsFound = itemsFound + 1;
				searchTableData.push({ccName: "${dcr.ccName}", dcrDate: "<a class='blue' href='#' splink='${url}/ppaSP/index.html?dcrId=${dcr.id}&dcrDt=${dcr.formatedDateStr}&fromSearch=true&isPpaQa=${isPpaQa}' onclick='openCashierSP(this);'>${dcr.formatedDateStr}</a>", submittedBy: "${dcr.updateUserId}", dateSubmitted: "${dcr.formattedUpdateDate}", processStatus: "${dcr.status}", requiredCompletionDate: "${dcr.formatedRCDStr}"});
			}
		}
	}
	</script>
</c:forEach>
<div id="searchResultDiv">
	<div id="itemsFoundDiv" class="searchResultHeader">		
		<script>document.write("Items Found: " + itemsFound);</script>
	</div>
	<div id="showItemsDiv" class="searchResultHeader">
		<a href='#' onclick="clearResults();"/>Clear Results</a>
	</div>
	<div id="searhResultTitle" class="tableTitle">
		<script>
			var tempFil = document.getElementById("filterSelect");
			if(!${isPPA}){
				if("${filter}" == "cashierRecon"){
					tempFil.selectedIndex = 1;
				} else if("${filter}" == "reviewAndApproval"){
					tempFil.selectedIndex = 0;
				}
				document.write(tempFil.options[tempFil.selectedIndex].text + " Inbox");
			} else{
				if("${filter}" == "ppaAbeyance"){
					tempFil.selectedIndex = 1;
				} else if("${filter}" == "ppaRecon"){
					tempFil.selectedIndex = 0;
				} else if ("${filter}" == "ppaQa") {
                                    tempFil.selectedIndex = 2;
                                    
                                }
				document.write(tempFil.options[tempFil.selectedIndex].text + " Inbox");
			}
		</script>
	</div>
	<div id="searchResultTable">		
	</div>
</div>
<script>
	if(${searchOnLoad}){
		submitForm();
	}
	sorttable.init();
	showSearchResultDiv();
	showResultsTable(searchTableData);
	autoSort();
</script>

<div class="loader" style="display: none"></div>
