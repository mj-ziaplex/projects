<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<html>
<head>
<META HTTP-EQUIV="Pragma" CONTENT="no-cache">
<META HTTP-EQUIV="Expires" CONTENT="-1">
<link rel="stylesheet" href="/WMS-Web/css/common.css" media="screen">

<script src="/WMS-Web/js/common.js"></script>
<title>${title}</title>
</head>
<body id="mainTemplateBodyId" style="width:600px;height:400px">
<table border="0" cellspacing="0" cellpadding="0" style="width:600px;height:400px;resize:none;">
	<tr>
		<td id="bodyCell" colspan="2">
                    <tiles:insertAttribute name="body" /></td>
	</tr>
</table>
</body>
</html>
