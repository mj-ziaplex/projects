<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<html>
<head>
<META HTTP-EQUIV="Pragma" CONTENT="no-cache">
<META HTTP-EQUIV="Expires" CONTENT="-1">
<link rel="stylesheet" href="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/css/common.css" media="screen">

<script src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/js/common.js"></script>
<title>${title}</title>
</head>
<body id="mainTemplateBodyId" style="width:100%;height:100%">
<table border="1" cellspacing="0" cellpadding="0" style="width:1400px;height:100%;resize:none;">
	<tr>
		<td id="headerCell" colspan="2">
                    <tiles:insertAttribute name="header" /></td>
	</tr>
	<tr>
		<td id="menuCell">
                    <tiles:insertAttribute name="menu" /></td>
		<td id="bodyCell">
                    <tiles:insertAttribute name="body" /></td>
	</tr>
</table>
</body>
</html>
