<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<link rel="stylesheet" href="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/css/dcrStatus.css" media="screen">
<link rel="stylesheet" href="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/css/pagination.css" media="screen">
<link rel="stylesheet" href="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/jquery/jquery-ui-1.9.1/themes/base/jquery-ui.css" />
<script src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/jquery/jquery-1.8.2.js"></script>
<script src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/jquery/jquery-ui-1.9.1/ui/jquery-ui.js"></script>
<script src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/js/custom-table.js"></script>
<script src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/js/sorttable.js"></script>
<script src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/js/common.js"></script>
<script src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/js/pagination.js"></script>
<script src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/js/dcrStatus.js"></script>

<c:if test="${action == 'view'}">
    <script>
                $(function() {
        $("#searchDatePickerStart").datepicker({dateFormat: 'ddMyy'});
        });
                $(function() {
        $("#searchDatePickerEnd").datepicker({dateFormat: 'ddMyy'});
        });</script>

    <div class="overflow" id="searchCriteriaDiv">
        <form:form id="searchDCRStatusForm" name="searchDCRStatusForm"
                   modelAttribute="searchDCRStatusForm"
                   action="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/admin/findDCRStatus.html"
                   method="post">
            <fmt:formatDate pattern="ddMMMyyyy" value="${searchDCRStatusForm.dcrStartDate}" var="dcrStartDateFmt" />
            <fmt:formatDate pattern="ddMMMyyyy" value="${searchDCRStatusForm.dcrEndDate}" var="dcrEndDateFmt" />
            <table id="searchCriteriaTable">
                <tr>
                    <td class="searchCriteriaTableLabel"><label for="searchDatePicker">DCR Date From: </label></td>
                    <td colspan="5">
                        <input type="text" id="searchDatePickerStart" name="searchDatePickerStart" readonly="true" onchange="setDcrStartDate(this.value);" />
                        <input type="hidden" id="dcrStartDate" name="dcrStartDate" />
                        <a href="#" onclick="clearStartDate()">Clear</a> (dd/MMM/yyyy)
                    </td>
                </tr>
                <tr>
                    <td class="searchCriteriaTableLabel"><label for="searchDatePicker">DCR Date To: </label></td>
                    <td colspan="5">
                        <input type="text" id="searchDatePickerEnd" name="searchDatePickerEnd" readonly="true" onchange="setDcrEndDate(this.value);" />
                        <input type="hidden" id="dcrEndDate" name="dcrEndDate" />
                        <a href="#" onclick="clearEndDate()">Clear</a> (dd/MMM/yyyy)
                    </td>
                </tr>
                <tr>
                    <td id="searchCriteriaTableLabel">Site Name</td>
                    <td>
                        <form:select path="ccId">
                            <form:options items="${ccList}" itemValue="ccId" itemLabel="ccName" />
                        </form:select>
                    </td>
                </tr>
                <tr>
                    <td id="searchCriteriaTableLabel">Status</td>
                    <td>
                        <form:select path="status">
                            <form:option value="ALL" label="ALL" />
                            <form:options items="${statusList}" />
                        </form:select>
                    </td>
                </tr>
                <tr>
                    <td><input type="button" name="findNow" value="Search" onclick="submitForm();" /></td>
                </tr>
            </table>
        </form:form>
    </div>

    <div class="overflow" id="dcrDiv">
        <div id="dcrTableNode">
            <div id="dcrLabelNode"></div>
            <div id="dcrNode"></div>
        </div>
        <div id="adddcrNode"></div>
        <div id="navigationNode"></div>
    </div>

    <script>
                sorttable.init();
                var dcrItems = [
        <c:forEach items="${dcrList}" var="dcr" varStatus="rowCnt">
        {
        customerCenter:"${dcr.ccName}",
                date: "<fmt:formatDate pattern='ddMMMyyyy' value='${dcr.dcrDate}'/>",
                status: "${dcr.status}",
                hasDownload: "${dcr.hasDownloaded}",
                updateUserId: "${dcr.updateUserId}",
                updateUserTime: "<fmt:formatDate type='both' value='${dcr.updateDate}'/>",
                action: "<a href='dcrStatusUpdate.html?dcrId=${dcr.id}'>Edit</a> | <a href='#' dcr='<fmt:formatDate pattern='ddMMMyyyy' value='${dcr.dcrDate}'/>${dcr.ccId}' onclick='showViewSQLPopUp(this);'>View SQL</a>"
        }
            <c:if test="${not rowCnt.last}">,</c:if></c:forEach>
        ];
                setDCRWorkItems(dcrItems);</script>

            <div id="viewSQLPopUpTableNode" style="display: none;">
                <div id="viewSQLPopUpNode"></div>
            </div>
</c:if>

<c:if test="${action == 'update'}">
    <div id="dcrUpdateDiv">
        <form:form
            action="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/admin/dcrStatusDoUpdate.html"
            method="POST" name="adminDCRStatusForm" id="adminDCRStatusForm"
            modelAttribute="adminDCRStatusForm"
            onsubmit="return onUpdateSubmit()">
            <input type="hidden" id="dcrId" name="dcrId" value="${param.dcrId}"/>
            <table id="adminDCRStatusFormTable">
                <tr>
                    <th align="left">DCR STATUS</th>
                </tr>
                <tr class="evenRow">
                    <td>Center Code</td>
                    <td>${adminDCRStatusForm.centerCode}</td>
                </tr>
                <tr class="oddRow">
                    <td>DCR Date</td>
                    <td><fmt:formatDate pattern='ddMMMyyyy' value='${adminDCRStatusForm.dcrDate}' /></td>
                </tr>
                <tr class="evenRow">
                    <td>Status</td>
                    <td>
                        <form:select path="status" id="status">
                            <form:option value="-" label="--" />
                            <form:options items="${statusList}" />
                        </form:select>
                    </td>
                </tr>
                <tr>
                    <td class="oddRow">Log Date</td>
                    <td class="oddRow">
                        <form:input id="logDate" path="logDate" size="25" /> format: <i>ddMmmyyyy hh:mm:ss</i>
                    </td>
                </tr>
                <tr class="evenRow">
                    <td>Users</td>
                    <td>
                        <form:input path="updateUserId" id="updateUserId" size="25" />
                        <%--<form:select path="updateUserId" id="updateUserId">
                              <form:option value="-" label="--" />
                               <form:options items="${userList}" />
                           </form:select>--%>
                    </td>
                </tr>
                <tr>
                    <td class="oddRow">IPAC-DCR Download</td>
                    <td
                        <form:radiobuttons path="hasDownload" items="${downloadOption}" />
                </td>
            </tr>
            <tr>
                <td valign="top" class="evenRow">Remarks</td>
                <td>
                    <form:textarea path="remark" rows="7" cols="60" id="remark" htmlEscape="true" />

                </td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <input id="updateDCRStatusBtn" type="button" value="Update" onclick="onUpdateSubmit();"/>
                    <input id="cancelDCRStatusBtn" type="button" value="Cancel" onclick="window.open('${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/admin/dcrStatus.html', '_self');"/>
                </td>
            </tr>
        </table>
    </form:form>
</div>
</c:if>