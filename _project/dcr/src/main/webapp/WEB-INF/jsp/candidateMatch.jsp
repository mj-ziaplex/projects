<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<link rel="stylesheet" href="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/jquery/jquery-ui-1.9.1/themes/base/jquery-ui.css" />
<script src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/jquery/jquery-1.8.2.js"></script>
<script src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/jquery/jquery-ui-1.9.1/ui/jquery-ui.js"></script>
<script src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/js/custom-table.js"></script>
<script src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/js/candidateMatch.js"></script>
<script src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/js/sorttable.js"></script>
<link rel="stylesheet" href="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/css/pagination.css" media="screen">
<link rel="stylesheet" href="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/css/candidateMatch.css" media="screen">
<script src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/js/common.js"></script>
<script src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/js/pagination.js"></script>
<script>
    $(function() {
        $( "#cmSearchDatePicker" ).datepicker({ dateFormat: 'ddMyy' });
    });    
	sorttable.init();
	if(${reloadSP}){
		window.opener.location.reload();
	}
	var matchCount = "${matchCount}";
	var unmatchCount = "${unmatchCount}";
	if(matchCount.length > 0){
		alert(matchCount + " documents matched.");
	} else if(unmatchCount.length > 0){
		alert(unmatchCount + " documents unmatched.");
	}
 </script>
 <div>
 	<div class="cmInfoDiv">
 		DCR Date: ${dcrDateStr}
 	</div>
 	<div class="cmInfoDiv">
 		Customer Center: ${ccName}
 	</div>
 </div>
<div class="overflow" id="cmSearchCriteriaDiv" >
	<div id="cmSearchCriteriaTitle" class="tableTitle">
		Candidate Match
	</div>
	<form id="cmSearchForm" name="cmSearchForm" action="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/candidateMatchSP/find.html?dcrId=${dcrId}&dcrDate=${dcrDate}&ccId=${ccId}&ccName=${ccName}&dcrDateStr=${dcrDateStr}" method="post">
	<table id="cmSearchCriteriaTable">
		<tr>
			<td  class="cmSearchCriteriaTableLabel">
				<label for="cmSearchDatePicker">DCR Date: </label> 
		 	</td>
		 	<td colspan="5">		 		
		 		<input type="text" id="cmSearchDatePicker" name="cmSearchDatePicker" readonly="true"" onchange="setDcrDateStr(this.value);" value="${form.searchDcrDateStr}"/>
		 		<input type="hidden" id="searchDcrDateStr" name="searchDcrDateStr" value="${form.searchDcrDateStr}">		 		
		 		<a href="#" onclick="clearDate()">Clear</a> (dd/MMM/yyyy)
		 	</td>
		</tr>
		<tr>
			<td id="cmSearchCriteriaTableRBSiteCode">
				<input id="rbSiteCode" type="radio" name="crit2" value="rbSiteCode" onclick="disableHubId()" checked="true" value="cc">Site Name<br>
		 	</td>
		 	<td id="cmSearchCriteriaTableDDSiteCode">		 		
		 		<select id="searchCcId" name="searchCcId">
				</select>
		 	</td>
		 	<td>
		 		<input type="button" name="findNow" value="Find Now" onclick="findMatch();"/>
		 	</td>
		</tr>
	</table>	
	</form>
</div>
<!-- <div id="cmSearchResultDiv" hidden="true">
	<div id="itemsFoundDiv" class="cmSearchResultHeader">
		<script>document.write("Items Found: " + itemsFound);</script>
	</div>
	<div id="showItemsDiv" class="cmSearchResultHeader">
	</div>
	<div id="searchResultTitle" class="tableTitle">
		Search Results
	</div>
	<div id="cmSearchResultTable">		
	</div>
</div>-->
<table id="tabTable" border="1px">
	<tr>	
		<td id="attached" class="tabCell" onclick="changeTab(this,'attachedRequirementsDiv','searchRequirementsDiv')">
			Attached Requirements
		</td>
		<td id="search" class="tabCell" onclick="changeTab(this,'searchRequirementsDiv','attachedRequirementsDiv')">
			DCR Search
		</td>
	</tr>
</table>
<c:forEach items="${attachments}" var="attachments" varStatus="rowCnt">
	<script>
		var tempAttachment = {};
	//	if("${attachments.fileType}" != "eml"){
			tempAttachment.filename = "<a target='_blank' href='../${attachments.fileUrl}'>${attachments.filename}</a>";
			tempAttachment.uploadDate = "<div class='alignCenter'>${attachments.timestamp}</div>";
			tempAttachment.unmatch = "<div class='alignCenter'><input type='checkbox' recordId='${attachments.recordId}' recordLocation='${attachments.recordLocation}' filenetId='${attachments.filenetId}' ccId='${ccId}' dcrDate='${dcrDate}' onclick='consolidateUnmatchList(this)'/></div>";	
			attachments.push(tempAttachment);
	//	}
	</script>
</c:forEach>
<form id="unmatchForm" action="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/candidateMatchSP/unmatch.html?dcrId=${dcrId}&dcrDate=${dcrDate}&ccId=${ccId}&ccName=${ccName}&dcrDateStr=${dcrDateStr}" method="post">	
	<div id="attachedRequirementsDiv">
		<div class="cmInfoDiv">Items Found: <script>document.write(attachments.length);</script></div>
		<div id="attachedRequirementsNode">		
		</div>		
		<input type="button" onclick="submitUnmatchForm();" value="Unmatch"/>
	</div>
</form>
<form id="matchForm" action="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/candidateMatchSP/match.html?dcrId=${dcrId}&dcrDate=${dcrDate}&ccId=${ccId}&ccName=${ccName}&dcrDateStr=${dcrDateStr}" method="post">

<c:forEach items="${searchResults}" var="searchResults" varStatus="rowCnt">
	<script>
		var tempResult = {};
	//	if("${searchResults.fileType}" != "eml"){
			tempResult.ccName = "${searchCC}";
			tempResult.dcrDate = "${searchDate}";
			tempResult.filename = "<a target='_blank' href='../${searchResults.fileUrl}'>${searchResults.filename}</a>"
			tempResult.uploadDate = "<div class='alignCenter'>${searchResults.timestamp}</div>";
			tempResult.match = "<div class='alignCenter'><input type='checkbox' filesize='${searchResults.filesize}' fileType='${searchResults.fileType}' description ='${searchResults.description}' recordId='${searchResults.recordId}' recordLocation='${searchResults.recordLocation}' filenetId='${searchResults.filenetId}' ccId='${ccId}' dcrDate='${dcrDate}' onclick='consolidateMatchList(this)'/></div>";
			searchResultList.push(tempResult);
	//	}
	</script>
</c:forEach>
<div id="searchRequirementsDiv">
	<div class="cmInfoDiv">Items Found: <script>document.write(searchResultList.length);</script></div>
	<div id="searchRequirementsNode">		
	</div>
	<input type="button" onclick="submitMatchForm();" value="Match"/>
</div>
</form>
<c:forEach items="${ccList}" var="ccList" varStatus="rowCnt">
	<script>
	$(document).ready( function(){
		var ccId = document.getElementById("searchCcId");
		var tempSelect = document.createElement("option");
		tempSelect.innerHTML = "${ccList.ccName}";
		tempSelect.value = "${ccList.ccId}";
		ccId.appendChild(tempSelect);
		ccId.value = "${form.searchCcId}";
	});	
	</script>
</c:forEach>
<script>
	generateRequiredAttachmentTable();
	generateSearchResultsTable();
	if(${fromSearch}){
		var searchTab = document.getElementById("search");
		$(document).ready( function(){changeTab(searchTab,"searchRequirementsDiv","attachedRequirementsDiv");});
	} else {
		var attachedReqTab = document.getElementById("attached");
		$(document).ready( function(){changeTab(attachedReqTab,"attachedRequirementsDiv","searchRequirementsDiv");});
	}
</script>
