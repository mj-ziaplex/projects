<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<link rel="stylesheet" href="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/css/mixedChequePayee.css" media="screen">
<link rel="stylesheet" href="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/jquery/jquery-ui-1.9.1/themes/base/jquery-ui.css" />
<script src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/jquery/jquery-1.8.2.js"></script>
<script src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/jquery/jquery-ui-1.9.1/ui/jquery-ui.js"></script>
<script src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/js/custom-table.js"></script>
<script src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/js/mixedChequePayee.js"></script>
<script src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/js/common.js"></script>
<script>
    $(function() {
        $( "#searchDatePicker" ).datepicker({ dateFormat: 'ddMyy' });
    });    
 </script>
<div class="overflow" id="mixedChequePayeeTableDiv" >
	<div id="mixedChequePayeeTitle" class="tableTitle">
		&nbsp;&nbsp;&nbsp;Mixed Cheque Payee ${customerCenter.ccId} - ${customerCenter.ccCode} ${dcrCashierBO.dcr.formatedDateStr}
	</div>
	<div id="mixedChequePayeeTableNode">		
	</div>
	<div id="createNewCheckDiv">
		<input type="button" name="createNewCheck" id="createNewCheck" value="Create New Check" onclick="showCreateNewCheckPopUp(1)"/>
	</div>
	<div id="navigationDiv">
		<input type="button" name="goBack" onclick="location.href='${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/addingmachine/index.html?dcrCashierId=${dcrCashierBO.id}'" id="goBack" value="Go Back"/>
		<input type="button" name="continue" onclick="location.href='${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/cds/slamcipCash.html?dcrCashierId=${dcrCashierBO.id}'" id="continue" value="Continue"/>
	</div>
</div>

<div id="createNewCheckDialogDiv">
	<div id="createNewCheckFormDiv">
		<form action="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/mixedChequePayee/createNewCheck.html?dcrCashierId=${dcrCashierBO.id}" id="checkForm" name="checkForm" method="POST">
		<div id="createNewCheckFormNode">	
		</div>		
		</form>
	</div>
</div>

<script>
loadCreateNewCheckForm();
</script>

<c:forEach items="${banks}" var="bank" varStatus="rowCnt">
	<script>
	$(document).ready(function(){
		var bankDD = document.getElementById("bankDD");
		var tempSelect = document.createElement("option");
		tempSelect.innerHTML = "${bank.name}";
		tempSelect.value = "${bank.id}";
		bankDD.appendChild(tempSelect);
	});	
	</script>
</c:forEach>

<c:forEach items="${mixedChequePayeeList}" var="mcp" varStatus="rowCnt">
	<script>
	$(document).ready(function(){		
		var tempObj = [{id: "${mcp.id}", bankId: "${mcp.bank.id}", checkNumber: "${mcp.checkNumber}", checkDate: "${mcp.checkDateStr}", checkAmount: "${mcp.checkAmount}",
				currency: "${mcp.currency.id}", typeOfCheck: "${mcp.checkType.id}",payee: "${mcp.payee.id}",
				slamci:"${mcp.slamci}",slocpi:"${mcp.slocpi}",slgfi:"${mcp.slgfi}",slfpi:"${mcp.slfpi}",others:"${mcp.others}"}];
		var tempMCP = [{bank: getBankName("${mcp.bank.id}"),checkNumber: "${mcp.checkNumber}", checkDate: "<a href='#' rowId='${mcp.id}' class='blue' onclick='showCreateNewCheckPopUp(2);setForm(this.rowId);'>${mcp.checkDateStr}<a>",
			            checkAmount: "${mcp.currency.name} "+ numberWithCommas(parseFloat("${mcp.checkAmount}").toFixed(2)), typeOfCheck: "${mcp.checkType.checkTypeName}", payee: "${mcp.payee}"}];
		appendJsons([mcpStore,tempObj]);
		appendJsons([mcpData,tempMCP]);
	});	
	</script>
</c:forEach>

<script>
setLoadData(mcpData);
</script>

<c:forEach items="${payees}" var="payee" varStatus="rowCnt">
	<script>
	$(document).ready(function(){
		var payeeDD = document.getElementById("payeeDD");
		var tempSelect = document.createElement("option");
		tempSelect.innerHTML = "${payee.name}";
		tempSelect.value = "${payee.id}";
		payeeDD.appendChild(tempSelect);
	});	
	</script>
</c:forEach>
<c:forEach items="${currencies}" var="currency" varStatus="rowCnt">
	<script>
	$(document).ready(function(){
		var currencyDD = document.getElementById("currencyDD");
		var tempSelect = document.createElement("option");
		tempSelect.innerHTML = "${currency.name}";
		tempSelect.value = "${currency.id}";
		currencyDD.appendChild(tempSelect);
	});	
	</script>
</c:forEach>
<c:forEach items="${checkTypes}" var="checkType" varStatus="rowCnt">
	<script>
	$(document).ready(function(){
		if(${checkType.id} != 7 && ${checkType.id} != 8){
			var checkTypeDD = document.getElementById("typeOfCheckDD");
			var tempSelect = document.createElement("option");
			tempSelect.innerHTML = "${checkType.checkTypeName}";
			tempSelect.value = "${checkType.id}";
			checkTypeDD.appendChild(tempSelect);
		}
	});	
	</script>
</c:forEach>