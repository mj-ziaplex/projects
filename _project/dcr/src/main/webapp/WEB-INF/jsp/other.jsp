<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<link rel="stylesheet" href="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/css/otherGrid.css" media="screen">
<link rel="stylesheet" href="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/jquery/jquery-ui-1.9.1/themes/base/jquery-ui.css" />
<script src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/jquery/jquery-1.8.2.js"></script>
<script src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/jquery/jquery-ui-1.9.1/ui/jquery-ui.js"></script>
<script src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/js/custom-table.js"></script>
<script src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/js/otherGrid.js"></script>
<c:forEach items="${productsToDisplay}" var="column" varStatus="rowCnt">
	<script>	
 				var headerName= "${column.productType.productName}" + "(" + "${column.productType.productCode}" + ")";
 				var headerField = "${column.productType.productCode}";
 				var tempHeader = [{name: headerName, field: headerField, width: "25%"}]; 	
 				var tempTotal = 0;
   				appendJsons([otherTableLayout,tempHeader]);
   				if("${column.productType.productCode}" == "COBP"){
   					totalCashCounter.${column.productType.productCode} = returnTextbox("${column.productType.productCode}","totalCashCounter",numberWithCommas(parseFloat("${column.dcrOther.cashCounterAmt}").toFixed(2)),false);
   					totalCashNonCounter.${column.productType.productCode} = returnTextbox("${column.productType.productCode}","totalCashNonCounter",numberWithCommas(parseFloat("${column.dcrOther.cashNonCounter}").toFixed(2)),false);
   					totalCheckOnUs.${column.productType.productCode} = returnTextbox("${column.productType.productCode}","totalCheckOnUs",numberWithCommas(parseFloat("${column.dcrOther.chequeOnUs}").toFixed(2)),false);
   					totalChequeLocal.${column.productType.productCode} = returnTextbox("${column.productType.productCode}","totalChequeLocal",numberWithCommas(parseFloat("${column.dcrOther.chequeLocal}").toFixed(2)),false);
   					totalCheckRegional.${column.productType.productCode} = returnTextbox("${column.productType.productCode}","totalCheckRegional",numberWithCommas(parseFloat("${column.dcrOther.chequeRegional}").toFixed(2)),false);
   					totalCheckNonCounter.${column.productType.productCode} = returnTextbox("${column.productType.productCode}","totalCheckNonCounter",numberWithCommas(parseFloat("${column.dcrOther.chequeNonCounter}").toFixed(2)),false);
   					tempTotal = parseFloat("${column.dcrOther.cashCounterAmt}") +
					parseFloat("${column.dcrOther.cashNonCounter}") +
					parseFloat("${column.dcrOther.chequeOnUs}") +
					parseFloat("${column.dcrOther.chequeLocal}") +
					parseFloat("${column.dcrOther.chequeRegional}") +
					parseFloat("${column.dcrOther.chequeNonCounter}");
   				}else if("${column.productType.productCode}" == "COBD"){
   					totalCashCounter.${column.productType.productCode} = returnTextbox("${column.productType.productCode}","totalCashCounter",numberWithCommas(parseFloat("${column.dcrOther.cashCounterAmt}").toFixed(2)),false);
   					totalCashNonCounter.${column.productType.productCode} = returnTextbox("${column.productType.productCode}","totalCashNonCounter",numberWithCommas(parseFloat("${column.dcrOther.cashNonCounter}").toFixed(2)),false);
   					totalUsCheckInManila.${column.productType.productCode} = returnTextbox("${column.productType.productCode}","totalUsCheckInManila",numberWithCommas(parseFloat("${column.dcrOther.totalUsCheckInManila}").toFixed(2)),false);
   					totalUsCheckOutPh.${column.productType.productCode} = returnTextbox("${column.productType.productCode}","totalUsCheckOutPh",numberWithCommas(parseFloat("${column.dcrOther.totalUsCheckOutPh}").toFixed(2)),false);
   					// totalChequeLocal.${column.productType.productCode} = returnTextbox("${column.productType.productCode}","totalChequeLocal",parseFloat("${column.totalCheckLocal}").toFixed(2),false);
   					totalDollarCheque.${column.productType.productCode} = returnTextbox("${column.productType.productCode}","totalDollarCheque",numberWithCommas(parseFloat("${column.dcrOther.totalDollarCheque}").toFixed(2)),false);
   					totalCheckNonCounter.${column.productType.productCode} = returnTextbox("${column.productType.productCode}","totalCheckNonCounter",numberWithCommas(parseFloat("${column.dcrOther.chequeNonCounter}").toFixed(2)),false);
   					tempTotal = parseFloat("${column.dcrOther.cashCounterAmt}") +
					parseFloat("${column.dcrOther.cashNonCounter}") + 
   					parseFloat("${column.dcrOther.totalDollarCheque}") +
					parseFloat("${column.dcrOther.totalUsCheckInManila}") +
					parseFloat("${column.dcrOther.totalUsCheckOutPh}") +
					parseFloat("${column.dcrOther.chequeNonCounter}");
   				}
   				
   				//totalCheckOT.${column.productType.productCode} = returnTextbox("${column.productType.productCode}","totalCheckOT",parseFloat("${column.totalCheckOT}").toFixed(2),false);
   				//totalNonCash.${column.productType.productCode} = returnTextbox("${column.productType.productCode}","totalNonCash",parseFloat("${column.totalNonCash}").toFixed(2),false);
   				//cardTypes.${column.productType.productCode} = " ";
   				//totalPosBpi.${column.productType.productCode} = returnTextbox("${column.productType.productCode}","totalPosBpi",parseFloat("${column.totalPosBpi}").toFixed(2),false);
   				//totalPosCtb.${column.productType.productCode} = returnTextbox("${column.productType.productCode}","totalPosCtb",parseFloat("${column.totalPosCtb}").toFixed(2),false);
   				//totalPosHsbc.${column.productType.productCode} = returnTextbox("${column.productType.productCode}","totalPosHsbc",parseFloat("${column.totalPosHsbc}").toFixed(2),false);
   				//totalPosScb.${column.productType.productCode} =returnTextbox("${column.productType.productCode}","totalPosScb",parseFloat("${column.totalPosScb}").toFixed(2),false);
   				//totalPosRcbc.${column.productType.productCode} = returnTextbox("${column.productType.productCode}","totalPosRcbc",parseFloat("${column.totalPosRcbc}").toFixed(2),false);
   				//totalPosBdo.${column.productType.productCode} = returnTextbox("${column.productType.productCode}","totalPosBdo",parseFloat("${column.totalPosBdo}").toFixed(2),false);
   				
			//		parseFloat("${column.totalCheckOT}") +
			//		parseFloat("${column.totalNonCash}");
			//		parseFloat("${column.totalPosBpi}") +
			//		parseFloat("${column.totalPosCtb}") +
			//		parseFloat("${column.totalPosHsbc}") +
			//		parseFloat("${column.totalPosScb}") +
			//		parseFloat("${column.totalPosRcbc}") +
			//		parseFloat("${column.totalPosBdo}");
				total.${column.productType.productCode} = returnTextbox("${column.productType.productCode}","total",numberWithCommas(parseFloat(tempTotal).toFixed(2)),true);
   	</script>
</c:forEach>
<div class="overflow" id="otherTableHeader" >
	<form id="otherForm" name="otherForm" 
		action="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/other/confirm.wms?dcrCashierId=${dcrCashierBO.id}" 
		method="post">
	<div id="otherTableHeaderNode">		
	</div>
	<input id="tempStore" type="hidden"/>
	<input id="confirm" type="hidden" name="confirm" value=true/>
	<input type="hidden" id="htmlCode" name="htmlCode" value=""/>
	<div id="confirmButtonDiv">
	</div>	
	</form>
</div>
<script>

<c:if test="${alertMessage ne ''}">
	alert('${alertMessage}');
</c:if>

var fillerField = [{name: " ", field: "filler2", width:"75%"}];
appendJsons([otherTableLayout,fillerField]);
totalCashCounter.filler2 = " ";
totalCashNonCounter.filler2 = " ";
totalCheckOnUs.filler2 = " ";
totalChequeLocal.filler2 = " ";
totalCheckRegional.filler2 = " ";
totalCheckNonCounter.filler2 = " ";
//totalCheckOT.filler2 = " ";
//cardTypes.filler2 = " ";
//totalNonCash.filler2 = " ";
//totalPosBpi.filler2 = " ";
//totalPosCtb.filler2 = " ";
//totalPosHsbc.filler2 = " ";
//totalPosScb.filler2 = " ";
//totalPosRcbc.filler2 = " ";
//totalPosBdo.filler2 = " ";
total.filler2 = " ";
var otherData =[];
otherData.push(totalCashCounter);
otherData.push(totalCashNonCounter);
otherData.push(totalCheckOnUs);
otherData.push(totalChequeLocal);
otherData.push(totalCheckRegional);
//otherData.push(totalCheckOT);
otherData.push(totalCheckNonCounter);
otherData.push(totalUsCheckInManila);
otherData.push(totalUsCheckOutPh);
otherData.push(totalDollarCheque);
//otherData.push(totalNonCash);
//otherData.push(cardTypes);
//otherData.push(totalPosBpi);
//otherData.push(totalPosCtb);
//otherData.push(totalPosHsbc);
//otherData.push(totalPosScb);
//otherData.push(totalPosRcbc);
//otherData.push(totalPosBdo);
otherData.push(total);
setLoadData(otherData);
if(${isConfirmed}){
	$(document).ready(function(){
	var confirmButton = document.getElementById("confirmButton");
	confirmButton.disabled = true;
	var saveButton = document.getElementById("saveButton");
	saveButton.disabled = true;
	var inputs = document.getElementsByTagName("input");
	for(x in inputs){
		if(inputs[x].id != "addCommentBtn"){
			inputs[x].disabled = true;
		}
	}
	}
	);
}

</script>