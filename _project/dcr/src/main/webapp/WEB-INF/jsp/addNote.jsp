<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<link rel="stylesheet"
	href="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/css/common.css"
	media="screen">
<link rel="stylesheet"
	href="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/jquery/jquery-ui-1.9.1/themes/base/jquery-ui.css" />
<script
	src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/jquery/jquery-1.8.2.js"></script>
<script
	src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/jquery/jquery-ui-1.9.1/ui/jquery-ui.js"></script>
<link rel="stylesheet"
	href="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/css/addNote.css"
	media="screen">

<script type="text/javascript"
	src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/tiny_mce/tiny_mce.js"></script>
<script
	src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/js/addNote.js"></script>
<script
	src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/js/custom-table.js"></script>
<script
	src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/js/common.js"></script>
<script>
	var reloadSP = "${reloadSP}";
	if(reloadSP == "true"){
		window.close();
		window.opener.location.reload();
	}
</script>

<div id="addNoteDiv">

	<form id="addNoteForm" method="post" enctype="multipart/form-data">
	<input type="hidden" id="dcrId" name="dcrId" value="${dcrId}"> 
		<table id="addNoteTable">
			<tr>
				<th>Input Note</th>
			</tr>
			<tr>
				<td><label for="documentTitle"> Note Title :</label><input
					type="text" id="documentTitle" name="title">
				</td>
			</tr>
			<tr>
				<td>Notes :</td>
			</tr>
			<tr>
				<td><textarea id="notesTextArea" name="content" width="100%"> </textarea>
				</td>
			</tr>
			<tr>
				<td><div id="controls">
						<input type="button" id="saveButton" value="Save" onclick="submitForm()">
						<input type="button" id="resetButton" value="Reset" onclick="reload()">
						<input type="button" id="cancelButton" value="Cancel"
							onclick="window.close()">
					</div></td>
			</tr>
		</table>

	</form>
</div>
<script>
function submitForm() {
	$(document).ready(function(){
				
				document.getElementById("addNoteForm").action = "${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/addNoteSP/save.html";
				document.getElementById("addNoteForm").target = "_self";
				document.getElementById("addNoteForm").submit();
				return true;
	});
}

function reload() {
	$(document).ready(function(){
				
				document.getElementById("addNoteForm").action = "${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/addNoteSP/create.html";
				document.getElementById("addNoteForm").target = "_self";
				document.getElementById("addNoteForm").submit();
				return true;
	});
}
</script>
