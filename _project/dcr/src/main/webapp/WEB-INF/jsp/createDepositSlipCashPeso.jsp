<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/strict.dtd">
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<link rel="stylesheet" href="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/jquery/jquery-ui-1.9.1/themes/base/jquery-ui.css" />
<script src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/jquery/jquery-1.8.2.js"></script>
<script src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/jquery/jquery-ui-1.9.1/ui/jquery-ui.js"></script>



<link href="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/css/common.css" media="screen" rel="stylesheet" type="text/css" >
<link href="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/css/createDepositSlip.css" media="screen" rel="stylesheet" type="text/css">


<script src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/js/custom-table.js"></script>
<script src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/js/createDepositSlip.js"></script>		
<script src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/js/common.js"></script>




<script>
    $(function() {
        $("#datepicker").datepicker({dateFormat: 'ddMyy'});
    });
</script>	

<form id="cashDepositSlipForm" class="depositSlipForm"  method="post">

    <div class="group">

        <!-- removed based on FRS 1.0
        <label for="customerCenter">Customer Center</label> 	 -->
        <input id="customerCenter" type="hidden" name="customerCenterName" readonly="readonly" tabindex="-1" tabindex="-1" class="readonly"  value="${bo.customerCenterName }"/>
        <input id="customerCenter" type="hidden" name="customerCenter" readonly="readonly" tabindex="-1" tabindex="-1" class="readonly" value="${bo.customerCenter}" />
        <input id="cashierName" type="hidden" name="cashierName" readonly="readonly" tabindex="-1" tabindex="-1" class="readonly"  value="${bo.cashierName }"/>

        <div class="content"> 
            <label for="date">Date</label> 
            <input type="text" id="datepicker" name="depositDateStr" readonly="readonly" value="${bo.depositDateStr}"  />
        </div>

        <div class="contentRight" > 
            <label for="accountNumber">Account Number</label> 
            <input id="accountNumber" type="text" name="accountNumber" readonly="readonly" class="readonly" tabindex="-1" value="${bo.accountNumber}"/>
        </div>

        <div class="contentRight" >
            <label for="currency">Currency</label> 
            <input id="currency" type="text" name="currencyDisplay"   readonly="readonly" tabindex="-1"  class="readonly"  value="PESO"/>
        </div>

        <div class="contentRight"> 
            <label for="dcrDate">DCR Date</label> 
            <input type="text" id="dcrDate"  readonly="readonly" tabindex="-1" class="readonly" name="dcrDateStr" value="${dcrCashierBO.dcr.formatedDateStr}" />
        </div>
    </div>


    <div class="group">
        <div class="content" > 
            <label for="depositedWith">Deposited with</label> 
            <input id="depositedWith" type="text" name="depositoryBankId"  readonly="readonly" tabindex="-1" class="readonly" value="${bo.depositoryBankId}"/>
        </div>
    </div>	

    <div class="group">
        <div class="content" >
            <label for="toTheCreditOf">To the credit of</label> 
            <input id="toTheCreditOf" type="text" name="companyStr" readonly="readonly" tabindex="-1" class="readonly"  value="${bo.companyStr}"/>
        </div>
    </div>	

    <c:if test="${bo.companyId eq 3 && bo.currencyId eq 1 }" >
        <div id ="SLOCPIBreakdown">
            <table id="slocpiBreakdownTable">
                <tr>
                    <td width="92">	
                        <div id="TradVulLabel" ><p>TRAD / VUL</p></div>
                    </td>

                    <td width="172">
                        <input id="tradVulCollectionTotal" type="text" name="tradVulCollectionTotal" readonly="readonly" tabindex="-1" class="readonly" value=""/>
                        <input id="hiddenTradVulCollectionTotal" type="hidden" name="hiddenTradVulCollectionTotal" value="${bo.tradVulCollectionTotal}" /><!-- ${bo.tradVulCollectionTotal} -->
                    </td>
                </tr>

                <tr>
                    <td>
                        <div id="GroupLifeLabel" >GRP LIFE</div>
                    </td>
                    <td>
                        <input id="grpLifeCollectionTotal" type="text" name="grpLifeCollectionTotal" readonly="readonly" tabindex="-1" class="readonly" value=""/>
                        <input id="hiddenGrpLifeCollectionTotal" type="hidden" name="hiddenGrpLifeCollectionTotal" value="${bo.grpLifeCollectionTotal}" /><!-- ${bo.grpLifeCollectionTotal} -->
                    </td>
                </tr>
            </table>
        </div>
        <script>
            var tradVul = document.getElementById("tradVulCollectionTotal");
            tradVul.value = numberWithCommas(parseFloat(${bo.tradVulCollectionTotal}).toFixed(2));
            var grpLife = document.getElementById("grpLifeCollectionTotal");
            grpLife.value = numberWithCommas(parseFloat(${bo.grpLifeCollectionTotal}).toFixed(2));
        </script>

    </c:if>


    <div id="sessionGroupCash" >

        <div class="content" >
            <input id="depositSlipType" type="text" name="depositSlipType"   readonly="readonly" tabindex="-1" class="readonly"  value="CASH"/>
        </div>

        <div class="contentRight" >
            <input id="collectionTotal" type="text" name="collectionTotal" readonly="readonly" tabindex="-1" class="readonly" value=""/>
            <input id="hiddenCollectionTotal" type="hidden" name="requiredTotalAmount" value="${bo.requiredTotalAmount - bo.pcoRequiredTotal}" />
        </div>

        <div class="contentRight" >
            <div  id="collectionTotalLabel" >Collection Total</div>
        </div>

    </div>	
    <div class="sessionGroupRight" >

        <div id="labels">
            <div class ="label" id="denominationLabel">Denomination</div>
            <div class ="label" id="noOfPiecesLabel">No of Pieces</div>
            <div class ="label" id="cashAmountLabel">Cash Amount</div>
        </div>



        <div class="groupRight" >

            <div class="contentRight" >
                <input type="text" name="cashAmount1000" id="cashAmount1000" readonly="readonly" tabindex="-1" class="readonlyCash" value="0.00"  />
                <input type="hidden" name="hiddenCashAmount1000" id="hiddenCashAmount1000" />
            </div>

            <div class="contentMiddle" >
                <input type="text" maxlength="8" name="denom1000" id="noOfPieces1000" class= "noOfPieces" value="${bo.denom1000}"  onkeyup="validateInput(this);
                calculateAmount('1000', '1000');
                calculateTotalAmount('PESO');
                calculateDiscrepancy('${isSavedDisabled}', '${isConfirmed}');" />
            </div>

            <div class="contentMiddle" id="denomination">
                <input type="text" name="denomination1000"  readonly="readonly" tabindex="-1" class="readonlyDenomination" value="1000"/>
            </div>

        </div>

        <div class="groupRight" >

            <div class="contentRight" >
                <input type="text" name="cashAmount500" id="cashAmount500" readonly="readonly" tabindex="-1" class="readonlyCash" value="0.00"  />
                <input type="hidden" name="hiddenCashAmount500" id="hiddenCashAmount500" />
            </div>

            <div class="contentMiddle" >
                <input type="text" maxlength="8" name="denom500" id="noOfPieces500" class= "noOfPieces" value="${bo.denom500}" onkeyup="validateInput(this);
                calculateAmount('500', '500');
                calculateTotalAmount('PESO');
                calculateDiscrepancy('${isSavedDisabled}', '${isConfirmed}');"/>
            </div>

            <div class="contentMiddle" id="denomination">
                <input type="text" maxlength="8" name="denomination500"  readonly="readonly" tabindex="-1" class="readonlyDenomination" value="500"/>
            </div>

        </div>

        <div class="groupRight" >

            <div class="contentRight" >
                <input type="text" name="cashAmount200" id="cashAmount200" readonly="readonly" tabindex="-1" class="readonlyCash" value="0.00"  />
                <input type="hidden" name="hiddenCashAmount200" id="hiddenCashAmount200" />
            </div>

            <div class="contentMiddle" >
                <input type="text" maxlength="8" name="denom200" id="noOfPieces200" class= "noOfPieces" value="${bo.denom200}" onkeyup="validateInput(this);
                calculateAmount('200', '200');
                calculateTotalAmount('PESO');
                calculateDiscrepancy('${isSavedDisabled}', '${isConfirmed}');"/>
            </div>

            <div class="contentMiddle" id="denomination">
                <input type="text" name="denomination200"  readonly="readonly" tabindex="-1" class="readonlyDenomination" value="200"/>
            </div>

        </div>

        <div class="groupRight" >

            <div class="contentRight" >
                <input type="text" name="cashAmount100" id="cashAmount100" readonly="readonly" tabindex="-1" class="readonlyCash" value="0.00"  />
                <input type="hidden" name="hiddenCashAmount100" id="hiddenCashAmount100" />
            </div>

            <div class="contentMiddle" >
                <input type="text" maxlength="8" name="denom100" id="noOfPieces100" class= "noOfPieces" value="${bo.denom100}" onkeyup="validateInput(this);
                calculateAmount('100', '100');
                calculateTotalAmount('PESO');
                calculateDiscrepancy('${isSavedDisabled}', '${isConfirmed}');"/>
            </div>

            <div class="contentMiddle" id="denomination">
                <input type="text" name="denomination100"  readonly="readonly" tabindex="-1" class="readonlyDenomination" value="100"/>
            </div>

        </div>

        <div class="groupRight" >

            <div class="contentRight" >
                <input type="text" name="cashAmount50" id="cashAmount50" readonly="readonly" tabindex="-1" class="readonlyCash" value="0.00"  />
                <input type="hidden" name="hiddenCashAmount50" id="hiddenCashAmount50" />
            </div>

            <div class="contentMiddle" >
                <input type="text" maxlength="8" name="denom50" id="noOfPieces50" class= "noOfPieces" value="${bo.denom50}" onkeyup="validateInput(this);
                calculateAmount('50', '50');
                calculateTotalAmount('PESO');
                calculateDiscrepancy('${isSavedDisabled}', '${isConfirmed}');"/>
            </div>

            <div class="contentMiddle" id="denomination">
                <input type="text" name="denomination50"  readonly="readonly" tabindex="-1" class="readonlyDenomination" value="50"/>
            </div>

        </div>

        <div class="groupRight" >

            <div class="contentRight" >
                <input type="text" name="cashAmount20" id="cashAmount20" readonly="readonly" tabindex="-1" class="readonlyCash" value="0.00"  />
                <input type="hidden" name="hiddenCashAmount20" id="hiddenCashAmount20" />
            </div>

            <div class="contentMiddle" >
                <input type="text" maxlength="8" name="denom20" id="noOfPieces20" class= "noOfPieces" value="${bo.denom20}" onkeyup="validateInput(this);
                calculateAmount('20', '20');
                calculateTotalAmount('PESO');
                calculateDiscrepancy('${isSavedDisabled}', '${isConfirmed}');"/>
            </div>

            <div class="contentMiddle" id="denomination">
                <input type="text" name="denomination20"  readonly="readonly" tabindex="-1" class="readonlyDenomination" value="20"/>
            </div>

        </div>

        <div class="groupRight" >

            <div class="contentRight" >
                <input type="text" name="cashAmount10" id="cashAmount10" readonly="readonly" tabindex="-1" class="readonlyCash" value="0.00"  />
                <input type="hidden" name="hiddenCashAmount10" id="hiddenCashAmount10" />
            </div>

            <div class="contentMiddle" >
                <input type="text" maxlength="8" name="denom10" id="noOfPieces10" class= "noOfPieces" value="${bo.denom10}" onkeyup="validateInput(this);
                calculateAmount('10', '10');
                calculateTotalAmount('PESO');
                calculateDiscrepancy('${isSavedDisabled}', '${isConfirmed}');"/>
            </div>

            <div class="contentMiddle" id="denomination">
                <input type="text" name="denomination10"  readonly="readonly" tabindex="-1" class="readonlyDenomination" value="10"/>
            </div>

        </div>

        <div class="groupRight" >

            <div class="contentRight" >
                <input type="text" name="cashAmount5" id="cashAmount5" readonly="readonly" tabindex="-1" class="readonlyCash" value="0.00"  />
                <input type="hidden" name="hiddenCashAmount5" id="hiddenCashAmount5" />
            </div>

            <div class="contentMiddle" >
                <input type="text" maxlength="8" name="denom5" id="noOfPieces5" class= "noOfPieces" value="${bo.denom5}" onkeyup="validateInput(this);
                calculateAmount('5', '5');
                calculateTotalAmount('PESO');
                calculateDiscrepancy('${isSavedDisabled}', '${isConfirmed}');"/>
            </div>

            <div class="contentMiddle" id="denomination">
                <input type="text" name="denomination5"  readonly="readonly" tabindex="-1" class="readonlyDenomination" value="5"/>
            </div>

        </div>

        <div class="groupRight" >

            <div class="contentRight" >
                <input type="text" name="cashAmount1" id="cashAmount1" readonly="readonly" tabindex="-1" class="readonlyCash" value="0.00"  />
                <input type="hidden" name="hiddenCashAmount1" id="hiddenCashAmount1" />
            </div>

            <div class="contentMiddle" >
                <input type="text" maxlength="8" name="denom1" id="noOfPieces1" class= "noOfPieces"  value="${bo.denom1}" onkeyup="validateInput(this);
                calculateAmount('1', '1');
                calculateTotalAmount('PESO');
                calculateDiscrepancy('${isSavedDisabled}', '${isConfirmed}');"/>
            </div>

            <div class="contentMiddle" id="denomination">
                <input type="text" name="denomination1"  readonly="readonly" tabindex="-1" class="readonlyDenomination" value="1"/>
            </div>

        </div>

        <div class="groupRight" >

            <div class="contentRight" >
                <input type="text" name="cashAmount25c" id="cashAmount25c" readonly="readonly" tabindex="-1" class="readonlyCash" value="0.00"  />
                <input type="hidden" name="hiddenCashAmount25c" id="hiddenCashAmount25c" />
            </div>

            <div class="contentMiddle" >
                <input type="text" maxlength="8" name="denom25c" id="noOfPieces25c" class= "noOfPieces" value="${bo.denom25c}" onkeyup="validateInput(this);
                calculateAmount('25c', '0.25');
                calculateTotalAmount('PESO');
                calculateDiscrepancy('${isSavedDisabled}', '${isConfirmed}');"/>
            </div>

            <div class="contentMiddle" id="denomination">
                <input type="text" name="denomination25c"  readonly="readonly" tabindex="-1" class="readonlyDenomination" value="0.25"/>
            </div>

        </div>

        <div class="groupRight" >

            <div class="contentRight" >
                <input type="text" name="cashAmount10c" id="cashAmount10c" readonly="readonly" tabindex="-1" class="readonlyCash" value="0.00"  />
                <input type="hidden" name="hiddenCashAmount10c" id="hiddenCashAmount10c" />
            </div>

            <div class="contentMiddle" >
                <input type="text" maxlength="8" name="denom10c" id="noOfPieces10c" class= "noOfPieces" value="${bo.denom10c}" onkeyup="validateInput(this);
                calculateAmount('10c', '0.10');
                calculateTotalAmount('PESO');
                calculateDiscrepancy('${isSavedDisabled}', '${isConfirmed}');"/>
            </div>

            <div class="contentMiddle" id="denomination">
                <input type="text" name="denomination10c"  readonly="readonly" tabindex="-1" class="readonlyDenomination" value="0.10"/>
            </div>

        </div>

        <div class="groupRight" >

            <div class="contentRight" >
                <input type="text" name="cashAmount5c" id="cashAmount5c" readonly="readonly" tabindex="-1" class="readonlyCash" value="0.00"  />
                <input type="hidden" name="hiddenCashAmount5c" id="hiddenCashAmount5c" />
            </div>

            <div class="contentMiddle" >
                <input type="text" maxlength="8" name="denom5c" id="noOfPieces5c" class= "noOfPieces" value="${bo.denom5c}" onkeyup="validateInput(this);
                calculateAmount('5c', '0.05');
                calculateTotalAmount('PESO');
                calculateDiscrepancy('${isSavedDisabled}', '${isConfirmed}');"/>
            </div>

            <div class="contentMiddle" id="denomination">
                <input type="text" name="denomination5c"  readonly="readonly" tabindex="-1" class="readonlyDenomination" value="0.05"/>
            </div>

        </div>

        <div class="groupRight" >

            <div class="contentRight" >
                <input type="text" name="cashAmount1c" id="cashAmount1c" readonly="readonly" tabindex="-1" class="readonlyCash" value="0.00"  />
                <input type="hidden" name="hiddenCashAmount1c" id="hiddenCashAmount1c" />
            </div>

            <div class="contentMiddle" >
                <input type="text" maxlength="8" name="denom1c" id="noOfPieces1c" class= "noOfPieces" value="${bo.denom1c}" onkeyup="validateInput(this);
                calculateAmount('1c', '0.01');
                calculateTotalAmount('PESO');
                calculateDiscrepancy('${isSavedDisabled}', '${isConfirmed}');"/>
            </div>

            <div class="contentMiddle" id="denomination">
                <input type="text" name="denomination1c"  readonly="readonly" tabindex="-1" class="readonlyDenomination" value="0.01"/>
            </div>

        </div>

        <!-- totals -->

        <div class="contentRight" >
            <input id="totalAmount" type="text" name="totalCashAmount" readonly="readonly" tabindex="-1" class="readonlyCash" value="0.00"/>
            <input id="hiddenTotalAmount" type="hidden" name="hiddenTotalAmount"/>
        </div>

        <div class="contentMiddle">
            <div id="totalCashAmountLabel">Total Amount</div>
        </div>

        <div class="contentRight" >
            <input id="discrepancy" type="text" name="discrepancy" readonly="readonly" tabindex="-1" class="readonlyDiscrepancy" value="0.00"/>
        </div>

        <div class="contentMiddle">
            <div id="discrepancyLabel">Discrepancy</div>
        </div>

    </div>




    <br/><br/>
    <input type="hidden" name="prodCode" value ="${bo.prodCode }"/>
    <input type="hidden" name="companyId" value="${bo.companyId}"/>
    <input type="hidden" name="dcrCashierId" value="${dcrCashierBO.id}"/>
    <input type="hidden" name="currencyId" value="${bo.currencyId}" /> 
    <input type="hidden" name="acf2id" value="${bo.acf2id}" />
    <div id="controls">
        <input type="button" value="PREVIEW" id="previewButton" onclick="previewPDF()"/>
        <input type="button" value="SAVE" id="saveButton"  onclick="submitForm()"/>
    </div>
</form>
<script>
    function previewPDF() {
        $(document).ready(function() {

            var date = document.getElementById("datepicker");
            if (!date.value) {
                alert('Date cannot be blank');

                return false;

                    }

            document.getElementById("cashDepositSlipForm").action = "${pageContext.request.contextPath}${previewMapping}";
            document.getElementById("cashDepositSlipForm").target = "_blank";
            document.getElementById("cashDepositSlipForm").submit();
            return true;

        });
    }

    function submitForm() {
        $(document).ready(function() {

            var date = document.getElementById("datepicker");
            if (!date.value) {
                alert('Date cannot be blank');

                return false;

                    }

                    if (!${isSavedDisabled}) {
                document.getElementById("cashDepositSlipForm").action = "${pageContext.request.contextPath}${saveMapping}";
                document.getElementById("cashDepositSlipForm").target = "_self";
                document.getElementById("cashDepositSlipForm").submit();
            }
            var saveButton = document.getElementById('saveButton');
            var previewButton = document.getElementById('previewButton');
            previewButton.disabled = true;
            saveButton.disabled = true;
            return true;
        });
    }


    initializePageCash('PESO', '${isSavedDisabled}', '${bo.requiredTotalAmount - bo.pcoRequiredTotal}',${isConfirmed});

</script>

