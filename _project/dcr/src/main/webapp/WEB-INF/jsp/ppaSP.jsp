<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:if test="${not empty consolidatedData}">
    
<!-- Added for MR-WF-16-00034 - Random sampling for QA -->
<jsp:useBean id="dateDoNotUse" class="java.util.Date" />
<c:set var="url" value="${pageContext.request.contextPath}" scope="request"/>
<c:set var="jsNoCache" value="?jsNoCache=${dateDoNotUse.time}" scope="request"/>
    
<link rel="stylesheet" href="${url}/css/ppaSP.css${jsNoCache}" media="screen">
<link rel="stylesheet" href="${url}/jquery/jquery-ui-1.9.1/themes/base/jquery-ui.css" />
<script src="${url}/jquery/jquery-1.8.2.js"></script>
<script src="${url}/jquery/jquery-ui-1.9.1/ui/jquery-ui.js"></script>
<script src="${url}/js/custom-table.js${jsNoCache}"></script>
<script src="${url}/js/common.js${jsNoCache}"></script>
<script src="${url}/js/stepProcResponses.js${jsNoCache}"></script>
<script src="${url}/js/ppaspSLFPI.js${jsNoCache}"></script>
<script src="${url}/js/ppaspOtherPeso.js${jsNoCache}"></script>
<script src="${url}/js/ppaspOtherDollar.js${jsNoCache}"></script>
<!-- still to create/edit and import two files -->
<!--script src="${url}/js/ppaspSLAMCIDollar.js${jsNoCache}"></script>
<script src="${url}/js/ppaspSLAMCIPeso.js${jsNoCache}"></script-->
<script src="${url}/js/ppaspSLOCPIPeso.js${jsNoCache}"></script>
<script src="${url}/js/ppaspSLOCPIDollar.js${jsNoCache}"></script>
<script src="${url}/js/ppaspSLGFIPeso.js${jsNoCache}"></script>
<script src="${url}/js/ppaspSLGFIDollar.js${jsNoCache}"></script>
<script src="${url}/js/ppaSP.js${jsNoCache}"></script>
<script src="${url}/js/extraFunctions.js${jsNoCache}"></script>
<script>
$(document).ready(function(){
	if(${closeWindow}){
		window.close();
		window.opener.location.reload();
	}
});

	dcrId = ${consolidatedData.dcrId};
	dcrStatus = "${consolidatedData.status}";
	ccName = "${consolidatedData.customerCenterName}";
	collectionsDate = "${consolidatedData.formattedDcrDate}";
	ccId = "${consolidatedData.ccId}";
	dcrDateStr = "${dcrDateStr}";
	formattedDcrDateStr = "${consolidatedData.dcrDateStr}";
	mdsReconciled = ${consolidatedData.mdsReconciled};
</script>
<div id="spHeaderDiv">
	<div id="spHeaderNode"></div>
</div>
<div id='inactiveCashierDiv' onclick='exCoArrow(this);showData(this);'><img src='../images/collapseArrow.JPG'/>CASHIER(S) WITHOUT WORKITEM</div><div id='icList'><div id='icListNode'></div></div>
<div class="centeredHeader">COLLECTIONS SUMMARY</div>
<!--div id="slamciPesoDiv">
	<div id="slamciPesoHeaderDiv" class="alignLeftHeader" onclick="exCoArrow(this);showData(this);"><img src="${url}/images/collapseArrow.JPG"/>SLAMCI - PESO</div>
	<div id="slamciPeso" class="overflow">
		<div id="slamciPesoSummary" class="slamciPesoTable"></div>
		<div id="slamciPesoBTSummary" class="slamciPesoTable"></div>
	</div>
</div>
<div id="slamciDollarDiv">
	<div id="slamciDollarHeaderDiv" class="alignLeftHeader" onclick="exCoArrow(this);showData(this);"><img src="${url}/images/collapseArrow.JPG"/>SLAMCI - DOLLAR</div>
	<div id="slamciDollar" class="overflow">
		<div id="slamciDollarSummary" class="slamciDollarTable"></div>
		<div id="slamciDollarBTSummary" class="slamciDollarTable"></div>
	</div>
</div-->
<div id="slocpiPesoDiv">
	<div id="slocpiPesoHeaderDiv" class="alignLeftHeader" onclick="exCoArrow(this);showData(this);"><img src="${url}/images/collapseArrow.JPG"/>SLOCPI - PESO</div>
	<div id="slocpiPeso" class="overflow" style="padding-bottom: 20px">
		<div id="slocpiPesoSummary" ></div>
		<div id="slocpiPesoBTSummary"></div>
	</div>
</div>
<div id="slocpiDollarDiv">
	<div id="slocpiDollarHeaderDiv" class="alignLeftHeader" onclick="exCoArrow(this);showData(this);"><img src="${url}/images/collapseArrow.JPG"/>SLOCPI - DOLLAR</div>
	<div id="slocpiDollar" class="overflow" style="padding-bottom: 20px">
		<div id="slocpiDollarSummary" ></div>
		<div id="slocpiDollarBTSummary"></div>
	</div>
</div>
<div id="slgfiPesoDiv">
	<div id="slgfiPesoHeaderDiv" class="alignLeftHeader" onclick="exCoArrow(this);showData(this);"><img src="${url}/images/collapseArrow.JPG"/>SLGFI - PESO</div>
	<div id="slgfiPeso" class="overflow" style="padding-bottom: 20px">
		<div id="slgfiPesoSummary" ></div>
		<div id="slgfiPesoBTSummary"></div>
	</div>
</div>
<div id="slgfiDollarDiv">
	<div id="slgfiDollarHeaderDiv" class="alignLeftHeader" onclick="exCoArrow(this);showData(this);"><img src="${url}/images/collapseArrow.JPG"/>SLGFI - DOLLAR</div>
	<div id="slgfiDollar" class="overflow" style="padding-bottom: 20px">
		<div id="slgfiDollarSummary" ></div>
		<div id="slgfiDollarBTSummary"></div>
	</div>
</div>
<div id="slfpiDiv">
	<div id="slfpiHeaderDiv" class="alignLeftHeader" onclick="exCoArrow(this);showData(this);"><img src="${url}/images/collapseArrow.JPG"/>SLFPI</div>
	<div id="slfpi">
		<div id="slfpiSummary" ></div>
		<div id="slfpiBTSummary"></div>
	</div>
</div>
<div id="otherPesoDiv">
	<div id="otherPesoHeaderDiv" class="alignLeftHeader" onclick="exCoArrow(this);showData(this);"><img src="${url}/images/collapseArrow.JPG"/>OTHER - PESO</div>
	<div id="otherPeso">
		<div id="otherPesoSummary" ></div>
		<div id="otherPesoBTSummary"></div>
	</div>
</div>
<div id="otherDollarDiv">
	<div id="otherDollarHeaderDiv" class="alignLeftHeader" onclick="exCoArrow(this);showData(this);"><img src="${url}/images/collapseArrow.JPG"/>OTHER - DOLLAR</div>
	<div id="otherDollar">
		<div id="otherDollarSummary" ></div>
		<div id="otherDollarBTSummary"></div>
	</div>
</div>
<c:forEach items="${consolidatedData.userIdShortNameMap}" var="entry" varStatus="rowCnt">
		<script>
			var tempUserIdShortNameMapping = {};
			tempUserIdShortNameMapping.key = "${entry.key}";
			tempUserIdShortNameMapping.shortName = "${entry.value}";
			userIdShortname.push(tempUserIdShortNameMapping);
		</script>
</c:forEach>
<c:forEach items="${consolidatedData.snapshotAttachments}" var="snapshots" varStatus="rowCnt">
		<c:forEach items="${snapshots.value}" var="snapshot" varStatus="rowCnt">
			<script>
				var tempSnapshot = {};
				tempSnapshot.cashierId = "${snapshot.uploaderId}";
				tempSnapshot.filename = "${snapshot.filename}";
				tempSnapshot.fileUrl = "${snapshot.fileUrl}";
				tempSnapshot.uploaderId = "${snapshot.uploaderId}";
				tempSnapshot.desc = "${snapshot.description}";
				tempSnapshot.filenetId = "${snapshot.filenetId}";
				tempSnapshot.uploadDate = "${snapshot.formattedUploadDate}";
				tempSnapshot.timestamp = "${snapshot.timestamp}";
				tempSnapshot.name = "<a target='_blank' href='../${snapshot.fileUrl}'>${snapshot.filename}</a>";
				if("${snapshots.key}" == "SLAMCIP"){
					slamcipSnapshot.push(tempSnapshot);
				}if("${snapshots.key}" == "SLAMCID"){
					slamcidSnapshot.push(tempSnapshot);
				}if("${snapshots.key}" == "SLOCPI"){
					slocpiSnapshot.push(tempSnapshot);
				}if("${snapshots.key}" == "SLGFI"){
					slgfiSnapshot.push(tempSnapshot);
				}if("${snapshots.key}" == "SLFPI"){
					slfpiSnapshot.push(tempSnapshot);
				}if("${snapshots.key}" == "OTHER"){
					otherSnapshot.push(tempSnapshot);
				}
			</script>
		</c:forEach>
</c:forEach>
<div id="snapshotsDiv">
	<div id="snapShotsNode" ></div>
</div>
<div id="cashierDivs">
</div>
<div id="actionDivs">
	<div id="actionNode"></div>
	<form id="hiddenForm" name="hiddenForm" action="${pageContext.request.contextPath}${savePPAStepProcessorMapping}" method="post">
		<input type="hidden" name="dcrId" value="${consolidatedData.dcrId}"/>
	</form>
</div>
<div id="addCommentDiv">
	<form action="${url}/ppaSP/saveComment.html" method="POST" name="addCommentForm" id="addCommentForm" target="iframe_a">
		Remarks:
		<br/><textarea rows="7" cols="50" name="remarks" id="commentRemarks" maxlength="350"></textarea>
		<span class="charsRemaining"></span>		
		<br/><label id="sendLabel">Send To:</label><select id="dcrCashierId" name="dcrCashierId"></select>
		<br/><input type="button" value="Add" onclick="submitComment()">
		<input type="hidden" id="dcrId" name="dcrId" value="${consolidatedData.dcrId}"/>
	</form>
</div>
<c:forEach items="${consolidatedData.cashierConfirmationsMap}" var="confirmations" varStatus="rowCnt">
		<c:forEach items="${confirmations.value}" var="confirmation" varStatus="rowCnt">
			<script>
				var tempConfirmation = {};
				tempConfirmation.cashierId = "${confirmations.key}";
				tempConfirmation.company = ${confirmation.companyId};
				tempConfirmation.confirmed = ${confirmation.confirmed};
				switch(tempConfirmation.company){
					case 1: slamcipConfirm.push(tempConfirmation); break;
					case 2: slamcidConfirm.push(tempConfirmation); break;
					case 3: slocpiConfirm.push(tempConfirmation); break;
					case 4: slgfiConfirm.push(tempConfirmation); break;
					case 5: slfpiConfirm.push(tempConfirmation); break;
					case 6: otherConfirm.push(tempConfirmation); break;
				}
				if(!${confirmation.confirmed}){
					confirmed = false;
				}
			</script>
		</c:forEach>
</c:forEach>
<c:forEach items="${attachments}" var="attachment" varStatus="rowCnt">
		<c:forEach items="${attachment.value}" var="attachmentObj" varStatus="rowCnt">
			<script>
				var tempAttachment = {};
				tempAttachment.cashierId = "${attachment.key}";
				tempAttachment.filename = "${attachmentObj.filename}";
				tempAttachment.fileUrl = "${attachmentObj.fileUrl}";
				tempAttachment.uploaderId = "${attachmentObj.uploaderId}";
				tempAttachment.desc = "${attachmentObj.description}";
				tempAttachment.filenetId = "${attachmentObj.filenetId}";
				tempAttachment.uploadDate = "${attachmentObj.formattedUploadDate}";
				tempAttachment.name = "<a target='_blank' href='${url}/${attachmentObj.fileUrl}'>${attachmentObj.filename}</a>";
				attachments.push(tempAttachment);
			</script>
		</c:forEach>
</c:forEach>
<c:forEach items="${consolidatedData.dcrCashDepositSlipBO}" var="dcrCashDepositSlipBO" varStatus="rowCnt">
	<script>
		var tempCDS = {};
		tempCDS.company = "${dcrCashDepositSlipBO.company.name}";
		tempCDS.acf2id = "${dcrCashDepositSlipBO.acf2id}";
		tempCDS.dcrDepoSlipVersionSerialId = "${dcrCashDepositSlipBO.dcrDepoSlipVersionSerialId}";
		tempCDS.prodCode = "${dcrCashDepositSlipBO.prodCode}";
		tempCDS.currency = "${dcrCashDepositSlipBO.currency.name}";
		cashDepositSlips.push(tempCDS);
	</script>
</c:forEach>
<c:forEach items="${consolidatedData.dcrChequeDepositSlipBO}" var="dcrChequeDepositSlipBO" varStatus="rowCnt">
	<script>
		var tempCHDS = {};
		tempCHDS.company = "${dcrChequeDepositSlipBO.company.name}";
		tempCHDS.acf2id = "${dcrChequeDepositSlipBO.acf2id}";
		tempCHDS.dcrDepoSlipVersionSerialId = "${dcrChequeDepositSlipBO.dcrDepoSlipVersionSerialId}";
		tempCHDS.checkType = "${dcrChequeDepositSlipBO.checkType.name}";
		tempCHDS.prodCode = "${dcrChequeDepositSlipBO.prodCode}";
		tempCHDS.currency = "${dcrChequeDepositSlipBO.currency.name}";
		chequeDepositSlips.push(tempCHDS);
	</script>
</c:forEach>
<c:forEach items="${consolidatedData.validatedCashDepositSlips}" var="dcrValidatedCashDepositSlipBO" varStatus="rowCnt">
	<script>
		var tempVCDS = {};
		tempVCDS.company = "${dcrValidatedCashDepositSlipBO.company.name}";
		tempVCDS.acf2id = "${dcrValidatedCashDepositSlipBO.acf2id}";
		tempVCDS.dcrDepoSlipVersionSerialId = "${dcrValidatedCashDepositSlipBO.dcrDepoSlipVersionSerialId}";
		tempVCDS.prodCode = "${dcrValidatedCashDepositSlipBO.prodCode}";
		tempVCDS.currency = "${dcrValidatedCashDepositSlipBO.currency.name}";
		validatedCashDepositSlips.push(tempVCDS);
	</script>
</c:forEach>
<c:forEach items="${consolidatedData.validatedChequeDepositSlips}" var="dcrValidatedChequeDepositSlipBO" varStatus="rowCnt">
	<script>
		var tempVCHDS = {};
		tempVCHDS.company = "${dcrValidatedChequeDepositSlipBO.company.name}";
		tempVCHDS.acf2id = "${dcrValidatedChequeDepositSlipBO.acf2id}";
		tempVCHDS.dcrDepoSlipVersionSerialId = "${dcrValidatedChequeDepositSlipBO.dcrDepoSlipVersionSerialId}";
		tempVCHDS.checkType = "${dcrValidatedChequeDepositSlipBO.checkType.name}";
		tempVCHDS.prodCode = "${dcrValidatedChequeDepositSlipBO.prodCode}";
		tempVCHDS.currency = "${dcrValidatedChequeDepositSlipBO.currency.name}";
		validatedChequeDepositSlips.push(tempVCHDS);
	</script>
</c:forEach>
<c:forEach items="${userRoles}" var="userRoles" varStatus="rowCnt">
	<script>
		userRoles.push("${userRoles}");
	</script>
</c:forEach>
<c:forEach items="${consolidatedData.cashierWorkItems}" var="workItems" varStatus="rowCnt">
	<script>
		var cashierCommentDropDown = document.getElementById("dcrCashierId");
		var cashierOption = document.createElement("option");
		cashierOption.value = "${workItems.dcrCashierId}";
		cashierOption.innerHTML = "${workItems.acf2id}";
		cashierCommentDropDown.appendChild(cashierOption);
		cashierCtr++;
	</script>
</c:forEach>
<c:forEach items="${consolidatedData.reconciledData}" var="reconciledData" varStatus="rowCnt1">
	<script>
		var tempReconData = {};
		var reconList = [];
		tempReconData.userId = "${reconciledData.userId}";
		tempReconData.dateTime = "${reconciledData.dateTime}";
	</script>
	<c:forEach items="${reconciledData.reconciledRows}" var="reconRowNums" varStatus="rowCnt2">
	<!-- reconciledData.reconciledRowNums before, and no loop above-->
		<script>
			reconList.push("${reconRowNums}");
		</script>
	</c:forEach>
	<script>
		tempReconData.reconList = reconList;
		reconDataList.push(tempReconData);
	</script>
</c:forEach>

<c:forEach items="${consolidatedData.notes}" var="notes" varStatus="rowCnt">
	<script>
		var tempNote = {};
		tempNote.rowNum = "${notes.rowNum}";
		tempNote.content = "${notes.noteContent}";
		tempNote.type = "${notes.noteType}";
		noteList.push(tempNote);
	</script>
</c:forEach>
<c:forEach items="${consolidatedData.commentBusinessObjects}" var="comments" varStatus="rowCnt">
	<script>
		var tempComment = {};
		tempComment.acf2id = "${comments.acf2id}";
		tempComment.remarks = "${comments.remarks}";
		tempComment.datePostedFormatted = "${comments.datePostedFormatted}";
		commentList.push(tempComment);
		if(commentCtr < 3){
			coupleCommentList.push(tempComment);
		}

		commentCtr++;
	</script>
</c:forEach>
<c:forEach items="${consolidatedData.inactiveCashiers}" var="inactiveCashier" varStatus="rowCnt">
	<script>
		if(jQuery.inArray("${inactiveCashier.acf2id}",inactiveId) == -1){
			 var tempInactiveCashier = {};
			 tempInactiveCashier.id = "${inactiveCashier.acf2id}";
			 tempInactiveCashier.name = "${inactiveCashier.lastname}, ${inactiveCashier.firstname} ${inactiveCashier.middlename}";
			 inactiveId.push(tempInactiveCashier.id);
			 inactiveCashierList.push(tempInactiveCashier);
		}
	</script>
</c:forEach>
<c:forEach items="${formattedConsolidatedData}" var="fcd" varStatus="rowCnt">
	<script>
		var tempObj = "${fcd}";
		var tempObjArr = tempObj.split("~");
		var newObj = {product: tempObjArr[0], cashierId: tempObjArr[1], cashierName: tempObjArr[2], productCode: tempObjArr[3], productName: tempObjArr[4], currency: tempObjArr[5], total: tempObjArr[6], balancingToolProductId: tempObjArr[7]};
		if(newObj.product == "totalReversalAmount" || newObj.product == "exemptionsWorksite"){
			newObj.product = "reversalProduct";
		}
		var prodCode = newObj.productCode;
		if(jQuery.inArray(newObj.cashierId,uniqueCashier) == -1){
			var dummyCashier = {};
			dummyCashier.name = newObj.cashierName;
			dummyCashier.id = newObj.cashierId;
			uniqueCashier.push(newObj.cashierId);
			cashierList.push(dummyCashier);			
		}
		switch(prodCode){
			case "PN": slfpiData.push(newObj);break;
			case "GAF": slfpiData.push(newObj);break;
			case "COBP": otherPesoData.push(newObj);break;
			case "COBD": otherDollarData.push(newObj);break;
			case "MF1": slamciPesoData.push(newObj);break;
			case "MF2": slamciPesoData.push(newObj);break;
			case "MF3": slamciPesoData.push(newObj);break;
			case "MF5": slamciPesoData.push(newObj);break;
			case "MF7": slamciPesoData.push(newObj);break;
			case "MF20": slamciPesoData.push(newObj);break;
			case "MF4": slamciDollarData.push(newObj);break;
			case "MF6": slamciDollarData.push(newObj);break;
			case "MF21": slamciDollarData.push(newObj);break;
			case "RL_RV_PESO": slgfiPesoData.push(newObj);break;
			case "RU_PESO": slgfiPesoData.push(newObj);break;
			case "RL_RV_DOLLAR": slgfiDollarData.push(newObj);break;
			case "RU_DOLLAR": slgfiDollarData.push(newObj);break;
			case "IL_VL_PESO": slocpiPesoData.push(newObj);break;
			case "GL": slocpiPesoData.push(newObj);break;
			case "IL_VL_DOLLAR": slocpiDollarData.push(newObj);break;
			case "RG_PESO": slgfiPesoData.push(newObj);break;
			case "RG_DOLLAR": slgfiDollarData.push(newObj);break;
			case "MF8": slamciPesoData.push(newObj);break;
			case "MF9": slamciPesoData.push(newObj);break;
            case "MF10": slamciDollarData.push(newObj);break;
            case "MF11": slamciDollarData.push(newObj);break;
            case "MF": slamciPesoData.push(newObj);break; // Added for PCO
            case "MF22": slamciPesoData.push(newObj);break; // Added for ADA
            case "MF12": slamciDollarData.push(newObj);break; // Added for SR-WF-16-00016
			default: alert("Unknown Product Code: " + prodCode); break;
		}	
	</script>
</c:forEach>
<script>
$(document).ready(function(){

	var spHeaderLayout = [ {
		name : ccName.toUpperCase(),
		field : "cc",
		width : "50%"
	},{
		name : "COLLECTIONS FOR " + collectionsDate.toUpperCase(),
		field : "dcrDate",
		width : "50%"
	}];
	
	new TableContainer({
		divName : "spHeaderDiv",
		nodeName : "spHeaderNode",
		width : "100%",
		id : "spHeader",
		layout : spHeaderLayout,
		tableClass: "spHeaderClass"
	}).startUp();


	var cashierDiv = document.getElementById("cashierDivs");

	var cashiers = [];
	
	for(cshr in attachments){	
		if(jQuery.inArray(attachments[cshr].cashierId,cashiers) == -1){			
			cashiers.push(attachments[cshr].cashierId);
		}
	}	
	
	var cashierLayout = [{
		name : "",
		field : "name",
		width : "33%"
	},{
		name : "",
		field : "desc",
		width : "33%"
	},{
		name : "",
		field : "uploadDate",
		width : "33%"
	}];
	
	for(var a = 0; a < cashiers.length; a++){
		var collapseImg = document.createElement("img");
		collapseImg.src = "${url}/images/collapseArrow.JPG";
		var headerDiv = document.createElement("div");
		headerDiv.className="alignLeftHeader";
		headerDiv.onclick=function(){exCoArrow(this);showData(this);};
		headerDiv.appendChild(collapseImg);		
		headerDiv.appendChild(document.createTextNode(getShortName(cashiers[a]) + " - ATTACHMENTS"));
		cashierDiv.appendChild(headerDiv);
		var attachmentDiv = document.createElement("div");
		attachmentDiv.id = "cashier" + a + "Div";
		cashierDiv.appendChild(attachmentDiv);
		
		var tempAttachments = [];
		
		for(b in attachments){
			if(cashiers[a] == attachments[b].cashierId){
				var tempAttachment = {};
				tempAttachment.name = attachments[b].name;
				tempAttachment.desc = attachments[b].desc;
				tempAttachment.uploadDate = attachments[b].uploadDate;
				tempAttachments.push(tempAttachment);
			}
			
		}		
		
		new TableContainer({
			divName : "cashierDivs",
			nodeName : attachmentDiv.id,
			width : "1000px",
			id : "cashier" + a + "Table",
			layout : cashierLayout,
			tableClass: "balancingToolSummary",
			data: tempAttachments
		}).startUp();
		
		attachmentDiv.style.display = "none";
	}
});	
</script>
<script>
	//var slamciPesoDivDetails = {divName: "slamciPeso", nodeName: "slamciPesoBTSummary", tableId: "slamciPesoTable"};

	//var slamciPesoSummaryDivDetails = {divName: "slamciPeso", nodeName: "slamciPesoSummary", tableId: "slamciPesoSum"};

	//var slamciDollarDivDetails = {divName: "slamciDollar", nodeName: "slamciDollarBTSummary", tableId: "slamciDollarTable"};

	//var slamciDollarSummaryDivDetails = {divName: "slamciDollar", nodeName: "slamciDollarSummary", tableId: "slamciDollarSum"};
	rowIndex = cashierCtr*slamciPaymentSubTypeCount;
        
	// Added for MR-WF-16-00034 - Random sampling for QA
    isPpaQa = '${isPpaQa}';

	var slocpiPesoDivDetails = {divName: "slocpiPeso", nodeName: "slocpiPesoBTSummary", tableId: "slocpiPesoTable"};
	
	var slocpiPesoSummaryDivDetails = {divName: "slocpiPeso", nodeName: "slocpiPesoSummary", tableId: "slocpiPesoSum"};
	
	var slocpiDollarDivDetails = {divName: "slocpiDollar", nodeName: "slocpiDollarBTSummary", tableId: "slocpiDollarTable"};
	
	var slocpiDollarSummaryDivDetails = {divName: "slocpiDollar", nodeName: "slocpiDollarSummary", tableId: "slocpiDollarSum"};
	
	var slgfiPesoDivDetails = {divName: "slgfiPeso", nodeName: "slgfiPesoBTSummary", tableId: "slgfiPesoTable"};
	
	var slgfiPesoSummaryDivDetails = {divName: "slgfiPeso", nodeName: "slgfiPesoSummary", tableId: "slgfiPesoSum"};
	
	var slgfiDollarDivDetails = {divName: "slgfiDollar", nodeName: "slgfiDollarBTSummary", tableId: "slgfiDollarTable"};
	
	var slgfiDollarSummaryDivDetails = {divName: "slgfiDollar", nodeName: "slgfiDollarSummary", tableId: "slgfiDollarSum"};
	
	var slfpiDivDetails = {divName: "slfpi", nodeName: "slfpiBTSummary", tableId: "slfpiTable"};
	
	var slfpiSummaryDivDetails = {divName: "slfpi", nodeName: "slfpiSummary", tableId: "slfpiSum"};
	
	var otherPesoDivDetails = {divName: "otherPeso", nodeName: "otherPesoBTSummary", tableId: "otherPesoTable"};
	
	var otherPesoSummaryDivDetails = {divName: "otherPeso", nodeName: "otherPesoSummary", tableId: "otherPesoSum"};
	
	var otherDollarDivDetails = {divName: "otherDollar", nodeName: "otherDollarBTSummary", tableId: "otherDollarTable"};
	
	var otherDollarSummaryDivDetails = {divName: "otherDollar", nodeName: "otherDollarSummary", tableId: "otherDollarSum"};

	//generateSLAMCIPesoSection("slamciPeso",slamciPesoDivDetails,slamciPesoSummaryDivDetails);
	//generateSLAMCIDollarSection("slamciDollar",slamciDollarDivDetails,slamciDollarSummaryDivDetails);
	generateSLOCPIPesoSection("slocpiPeso",slocpiPesoDivDetails,slocpiPesoSummaryDivDetails);
	generateSLOCPIDollarSection("slocpiDollar",slocpiDollarDivDetails,slocpiDollarSummaryDivDetails);
	generateSLGFIPesoSection("slgfiPeso",slgfiPesoDivDetails,slgfiPesoSummaryDivDetails);
	generateSLGFIDollarSection("slgfiDollar",slgfiDollarDivDetails,slgfiDollarSummaryDivDetails);
	generateSLFPISection("slfpi",slfpiDivDetails,slfpiSummaryDivDetails);
	generateOtherPesoSection("otherPeso",otherPesoDivDetails,otherPesoSummaryDivDetails);
	generateOtherDollarSection("otherDollar",otherDollarDivDetails,otherDollarSummaryDivDetails);
	generateInactiveCashierSection();

	$(document).ready(function(){
	var compBut = document.getElementById("completeButton");
	var saveBut = document.getElementById("saveButton");
	var errorTagBut = document.getElementById("errorTaggingButton");
	var candidateMatchBut = document.getElementById("candidateMatchButton");
	if(!confirmed || inactiveCashierList.length > 0){
		compBut.disabled = true;
	}
	if(${fromSearch}){
		compBut.disabled = true;
		saveBut.disabled = true;
		if(role == "MANAGER" || role == "CASHIER"){
			errorTagBut.disabled =true;
			candidateMatchBut.disabled = true;
		} else if(role == "OTHER"){
			errorTagBut.disabled =true;
			candidateMatchBut.disabled = true;
		}
	}
	if(role=="CASHIER"){
		errorTagBut.disabled = true;
	} else if(role == "MANAGER"){
		if(jQuery.inArray(dcrStatus,reviewAndApproval) == -1){
			//errorTagBut.disabled =true;
			candidateMatchBut.disabled = true;
		}
	}
	resolveCompleteButtonEnablingViaReconCheckboxes();
	});
</script>
<form action="${url}/ppaSP/complete.html" method="POST" name="spCompleteForm" id="spCompleteForm">
	<input type="hidden" id="dcrIdComplete" name="dcrId"/>
	<input type="hidden" id="formFromSearch" name="formFromSearch"/>
	<input type="hidden" id="stepProcResponse" name="stepProcResponse"/>
	<input type="hidden" id="rowNums" name="rowNums" />
    <!-- Added for IM00331115 (21APR2017 Release) -->
    <input type="hidden" id="reconRownums" name="reconRownums" value=""/>
</form>


<div id="spNoteDiv">
	<form name="spNoteForm" id="spNoteForm" method="post">
		Remarks:
		<br/><textarea rows="7" cols="50" name="spNoteContent" id="spNoteContent" maxlength="1000"></textarea>
		<span class="charRemaining"></span>
	</form>
</div>

<input type="hidden" id="secPass" name="secPass" value="${userSession.password}"/>
</c:if>
