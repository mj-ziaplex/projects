<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<!-- Added for Audit finding for CCQA notes -->
<jsp:useBean id="dateDoNotUse" class="java.util.Date" />
<c:set var="url" value="${pageContext.request.contextPath}" scope="request"/>
<c:set var="jsNoCache" value="?jsNoCache=${dateDoNotUse.time}" scope="request"/>

<link rel="stylesheet" href="${url}/css/search.css" media="screen">
<link rel="stylesheet" href="${url}/css/pagination.css" media="screen">
<link rel="stylesheet" href="${url}/jquery/jquery-ui-1.9.1/themes/base/jquery-ui.css" />
<script src="${url}/jquery/jquery-1.8.2.js"></script>
<script src="${url}/jquery/jquery-ui-1.9.1/ui/jquery-ui.js"></script>
<script src="${url}/js/custom-table.js${jsNoCache}"></script>
<script src="${url}/js/search.js${jsNoCache}"></script>
<script src="${url}/js/sorttable.js${jsNoCache}"></script>
<script src="${url}/js/dateFormatter.js${jsNoCache}"></script>
<script src="${url}/js/common.js${jsNoCache}"></script>
<script src="${url}/js/pagination.js${jsNoCache}"></script>
 <script>
    $(function() {
        $( "#searchDatePickerStart" ).datepicker({ dateFormat: 'ddMyy' });
    });   
    
    $(function() {
        $( "#searchDatePickerEnd" ).datepicker({ dateFormat: 'ddMyy' });
    });  
 </script>
<div class="overflow" id="searchCriteriaDiv" >
	<div id="searchCriteriaTitle" class="tableTitle">
		Collections Workitem Viewer
	</div>
	<form id="searchForm" name="searchForm" action="${url}/search/find.html" method="post">
	<table id="searchCriteriaTable">
		<tr>
			<td  class="searchCriteriaTableLabel">
				<label for="searchDatePicker">DCR Date From: </label> 
		 	</td>
		 	<td colspan="5">		 		
		 		<input type="text" id="searchDatePickerStart" name="searchDatePickerStart" readonly="true"" onchange="setDcrStartDate(this.value);"/>
		 		<input type="hidden" id="dcrStartDate" name="dcrStartDate">
		 		<a href="#" onclick="clearStartDate()">Clear</a> (dd/MMM/yyyy)
		 	</td>
		</tr>
		<tr>
			<td  class="searchCriteriaTableLabel">
				<label for="searchDatePicker">DCR Date To: </label> 
		 	</td>
		 	<td colspan="5">		 		
		 		<input type="text" id="searchDatePickerEnd" name="searchDatePickerEnd" readonly="true"" onchange="setDcrEndDate(this.value);"/>
		 		<input type="hidden" id="dcrEndDate" name="dcrEndDate">
		 		<a href="#" onclick="clearEndDate()">Clear</a> (dd/MMM/yyyy)
		 	</td>
		</tr>
		<tr>
			<td id="searchCriteriaTableRBSiteCode">
				<input id="rbSiteCode" type="radio" name="crit2" value="rbSiteCode" onclick="disableHubId()" checked="true" value="cc">Site Name<br>
		 	</td>
		 	<td id="searchCriteriaTableDDSiteCode">		 		
		 		<select id="ccId" name="ccId">
		 			<option value="ALL">ALL</option>
				</select>
		 	</td>
		 	<td id="searchCriteriaTableRBHub">
		 		<input id="rbHub" type="radio" name="crit2" value="rbHub" onclick="disableCcId()" value="hub"/>Hub<br>		 		
		 	</td>
		 	<td>		 		
		 		<select id="hubId" name="hubId" disabled="true">
				</select>
		 	</td>
		 	<td>
		 		<input type="button" name="findNow" value="Find Now" onclick="submitForm();"/>
		 	</td>
		</tr>
	</table>
	<input type="hidden" id="searchByCC" name="searchByCC" value="true"/>
	<input type="hidden" id="searchByHub" name="searchByHub" value="false"/>
	</form>
</div>
<c:forEach items="${customerCenters}" var="cc" varStatus="rowCnt">
	<script>
	$(document).ready(function(){
		var ccId = document.getElementById("ccId");
		var tempSelect = document.createElement("option");
		tempSelect.innerHTML = "${cc.ccName}";
		tempSelect.value = "${cc.ccId}";
		ccId.appendChild(tempSelect);
		ccId.selectedIndex = -1;
	});	
	</script>
</c:forEach>
<c:forEach items="${hubs}" var="hub" varStatus="rowCnt">
	<script>
	$(document).ready(function(){
		var hubId = document.getElementById("hubId");
		var tempSelect = document.createElement("option");
		tempSelect.innerHTML = "${hub.hubName}";
		tempSelect.value = "${hub.hubId}";
		hubId.appendChild(tempSelect);
		hubId.selectedIndex = -1;
	});	
	</script>
</c:forEach>
<c:forEach items="${dcrObjects}" var="dcr" varStatus="rowCnt">
	<script>
		itemsFound = itemsFound + 1;
		showTable = true;
		// Modified for Audit finding for CCQA notes
		searchTableData.push({ccName: "${dcr.ccName}", dcrDate: "<a class='blue' href='#' aplink='${url}/cashierSP/processData.html?dcrId=${dcr.id}' splink='${url}/cashierSP/index.html?dcrId=${dcr.id}&fromSearch=true' ppaLink='${url}/ppaSP/index.html?dcrId=${dcr.id}&dcrDt=${dcr.formatedDateStr}&fromSearch=true&isPpaQa=true' isPpa='${isPpa}' stat='${dcr.status}' onclick='openAjaxCashierSP(this);'>${dcr.formatedDateStr}</a>", submittedBy: "", dateSubmitted: "", processStatus: "${dcr.status}"});
	</script>
</c:forEach>

<div class="loader" style="display: none"></div>

<div id="searchResultDiv" hidden="true">
	<div id="itemsFoundDiv" class="searchResultHeader">
		<script>document.write("Items Found: " + itemsFound);</script>
	</div>
	<div id="showItemsDiv" class="searchResultHeader">
		<a href='#' onclick="clearResults();"/>Clear Results</a>
	</div>
	<div id="searhResultTitle" class="tableTitle">
		Search Results
	</div>
	<div id="searchResultTable">		
	</div>
</div>
<script>
	if(showTable){
		sorttable.init();
		showSearchResultDiv();
		showResultsTable(searchTableData);
		autoSort();
	}
</script>
