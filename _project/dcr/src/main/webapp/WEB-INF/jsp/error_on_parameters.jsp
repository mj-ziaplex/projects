<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
	  
<link rel="stylesheet" href="/WMS-Web/css/home.css" media="screen">
<link rel="stylesheet" href="/WMS-Web/css/home_dcr.css" media="screen">
<script src="/WMS-Web/js/home.js"></script>
<script src="/WMS-Web/js/custom-table.js"></script>
<LINK REL=stylesheet HREF="/WMS-Web/css/errors.css">
<link rel='stylesheet' href="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/css/Wcm_CP.css" type='text/css'>

<p/><p/><p/><p/><p/>
<table class="wcmErrorTable" align="center" border="0" cellpadding="0" cellspacing="0">
<tbody>
<tr>
<td>&nbsp;</td>
</tr>
<tr>
<td class="wcmErrorHeader" align="center">
<b>Error Message</b>
</td>
</tr>
<tr>
<td class="wcmErrorMessage" align="left">Your request to URL &quot;${thisURL}&quot; has invalid input(s) on the following field(s):
          <ul>
          <c:forEach items="${errors}" var="error" varStatus="rowCnt">
          	<li>${error}</li>
		  </c:forEach>
          </ul>
          <P/>
          <P/>
          You may press the "Back" button of your browser or go to <a href="/WMS-Web/home/index.html">HOME</a></td>
</tr>
<tr>
<td class="wcmErrorHeader" align="center">&nbsp;
</td>
</tr>
</tbody></table>

<!-- 		
<table border="0" cellpadding="10" cellspacing="0" width="600">

      <tr>      
        <td align="left" valign="top"><img src="/WMS-Web/images/wwfile.gif"

 alt="Webwasher" width="215" height="60"></td><td><p class="notification">Notification</p></td>
        
      </tr>
      <tr>
        <td colspan="2" align="left">
          

            <p class="title">Request Blocked by WMS</td>
      </tr>
 
      <tr>
        <td colspan="2" align="left" valign="top">
          

          Your request to URL &quot;${thisURL}&quot; has invalid input(s) on the following field(s):
          <ul>
          <c:forEach items="${errors}" var="error" varStatus="rowCnt">
          	<li>${error}</li>
		  </c:forEach>
          </ul>
          <P/>
          <P/>
          You may press the "Back" button of your browser or go to <a href="/WMS-Web/home/index.html">HOME</a>
	</td>
      </tr>

      <tr>
        <td colspan="2" align="right" valign="top">
          <hr size="1">
          
   
	      <small><i>generated at ${thisDate}</i></small>
	  

	</td>
      </tr>
</table> 	
-->
