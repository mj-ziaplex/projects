<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
	  
<link rel="stylesheet" href="/WMS-Web/css/home.css" media="screen">
<link rel="stylesheet" href="/WMS-Web/css/home_dcr.css" media="screen">
<script src="/WMS-Web/js/home.js"></script>
<script src="/WMS-Web/js/custom-table.js"></script>
<LINK REL=stylesheet HREF="/WMS-Web/css/errors.css">
<link rel='stylesheet' href="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/css/Wcm_CP.css" type='text/css'>

<p/><p/><p/><p/><p/>
<table class="wcmErrorTable" align="center" border="0" cellpadding="0" cellspacing="0">
<tbody>
<tr>
<td>&nbsp;</td>
</tr>
<tr>
<td class="wcmErrorHeader" align="center">
<b>Error Message</b>
</td>
</tr>
<tr>
<td class="wcmErrorMessage" align="center">An unexpected error has been encountered. Please contact helpdesk for assistance.</td>
</tr>
<tr>
<td class="wcmErrorHeader" align="center">&nbsp;
</td>
</tr>
</tbody></table>
