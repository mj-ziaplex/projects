<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<div class="overflow" id="searchCriteriaDiv" >
    <div id="searchCriteriaTitle" class="tableTitle">
        Merchant Deposit Slip Reconciliation
    </div>
    <form id="searchForm" name="searchForm" action="${url}/search/find.html" method="post">
        <table id="searchCriteriaTable">
            <tr>
                <td class="searchCriteriaTableRBApprovalCode">
                    <input id="rbSalesSlipNumber" type="radio" name="crit2" value="rbSalesSlipNumber" onclick="enableSalesSlipNumber();">Sales Slip Number: <br>
                </td>
                <td colspan="5">		 		
                    <input type="text" id="salesSlipNumber" name="salesSlipNumber" disabled="true" />		 		
                </td>
            </tr>
            <tr>
                <td class="searchCriteriaTableRBApprovalCode">
                    <input id="rbApprovalCode" type="radio" name="crit2" value="rbApprovalCode" checked="true" onclick="enableApprovalCode();">Approval Code:<br>
                </td>
                <td colspan="5">		 		
                    <input type="text" id="approvalCode" name="approvalCode"/>		 		
                </td>		 	
            </tr>
            <tr>
                <td class="searchCriteriaTableRBApprovalCode">
                    <input id="rbCardNumber" type="radio" name="crit2" value="rbCardNumber" onclick="enableCardNumber();">Card Number: <br>
                </td>
                <td colspan="5">		 		
                    <input type="text" id="cardNumber" name="cardNumber" disabled="true"/>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <input type="button" name="findNow" value="Find Now" onclick="filterNow();"/>
                    <input type="button" name="clear" value="Clear" onclick="clearSearch();"/>		 		
                </td>
            </tr>
            <tr>
                <td colspan="4">&nbsp;</td>
                <td style="width: 550px; text-align: right">MDS Count:</td>
                <td style="text-align: left">		 		
                    <div style="margin-left: 5px" id="mdsCountDisp"></div>	 		
                </td>
            </tr>
        </table>
    </form>
    <form id="hiddenForm" name="hiddenForm" action="${pageContext.request.contextPath}${savePPAMDSMapping}" method="post">
        <input type="hidden" name="dcrId" value="${dcrId}"/>
        <input type="hidden" name="ccId" value="${ccId}"/>
        <input type="hidden" name="dcrDateStr" value="${dcrDateStr}"/>
        <input type="hidden" name="mdsActiveCB" id="mdsActiveCB"/>
        <input type="hidden" name="mdsTotalCB" id="mdsTotalCB"/>
        <!-- Added for MR-WF-16-00031 - PPA CheckBox -->
        <input type="hidden" id="isPpaQa" name="isPpaQa" value="${isPpaQa}"/>
    </form>
</div>
<div id="searchResultDiv">
    <div id="itemsFoundDiv" class="searchResultHeader">		
        <span id="itemsFound"></span>
    </div>
    <div id="showItemsDiv" class="searchResultHeader">
    </div>
    <div id="searhResultTitle" class="tableTitle">
        Search Results
    </div>
    <div id="searchResultTable">		
    </div>
    <br/>
    <div id="saveEditDiv">
        <input type="button" name="save" value="Save" onclick="saveMDSRecon();" />
    </div>
</div>

<!-- Added for MR-WF-16-00034 - Random sampling for QA -->
<jsp:useBean id="dateDoNotUse" class="java.util.Date" />
<c:set var="url" value="${pageContext.request.contextPath}" scope="request"/>
<c:set var="jsNoCache" value="?jsNoCache=${dateDoNotUse.time}" scope="request"/>

<link rel="stylesheet" href="${url}/css/ppamds.css" media="screen">
<link rel="stylesheet" href="${url}/css/pagination.css" media="screen">
<link rel="stylesheet" href="${url}/jquery/jquery-ui-1.9.1/themes/base/jquery-ui.css" />
<script src="${url}/jquery/jquery-1.8.2.js"></script>
<script src="${url}/jquery/jquery-ui-1.9.1/ui/jquery-ui.js"></script>
<script src="${url}/js/custom-table.js${jsNoCache}"></script>
<script src="${url}/js/ppamds.js${jsNoCache}"></script>
<script src="${url}/js/sorttable.js${jsNoCache}"></script>
<script src="${url}/js/dateFormatter.js${jsNoCache}"></script>
<script src="${url}/js/common.js${jsNoCache}"></script>
<script src="${url}/js/pagination.js${jsNoCache}"></script>
<script src="${url}/js/extraFunctions.js${jsNoCache}"></script>

<script>
    cleanLists();
    // Added for MR-WF-16-00034 - Random sampling for QA
    isPpaQa = ${isPpaQa};
    disableFieldFlag();
</script>

<c:forEach items="${ppaMDSObjects}" var="ppaMDSObject" varStatus="rowCnt">
    <script>
        var isReconciled = "${ppaMDSObject.isReconciled}";
        commentList.push(new Comment(${dcrId} + "${ppaMDSObject.salesSlipNumber}", "${ppaMDSObject.ppaNotes}", "${ppaMDSObject.ppaFindings}"));
        if ("${ppaMDSObject.ppaNotes}" !== null || "${ppaMDSObject.ppaNotes}" !== '' || "${ppaMDSObject.ppaFindings}" !== null || "${ppaMDSObject.ppaFindings}" !== '') {
            existingCommentList.push(new Comment(${dcrId} + "${ppaMDSObject.salesSlipNumber}", "${ppaMDSObject.ppaNotes}", "${ppaMDSObject.ppaFindings}"));
        }

        var tempData = [{salesSlipNumber: "${ppaMDSObject.salesSlipNumber}", approvalCode: "${ppaMDSObject.approvalNumber}", cardNumber: "${ppaMDSObject.accountNumber}", currency: "${ppaMDSObject.currency}", amount: numberWithCommas(parseFloat("${ppaMDSObject.cardAmount}").toFixed(2)), userId: "${ppaMDSObject.userId}", productCode: "${ppaMDSObject.prodCode}", company: "${ppaMDSObject.company}", paymentType: "${ppaMDSObject.paySubDesc}", reconciled: getCheckbox(isReconciled, "${dcrId}", "${ppaMDSObject.salesSlipNumber}", "${ppaMDSObject.approvalNumber}", "${ppaMDSObject.accountNumber}"), reconBy: "${ppaMDSObject.reconBy}", reconDate: "${ppaMDSObject.reconDate}", notes: getNotes(${dcrId} + "${ppaMDSObject.salesSlipNumber}", "notes"), findings: getNotes(${dcrId} + "${ppaMDSObject.salesSlipNumber}", "findings")}];
        appendJsons([searchTableData, tempData]);

        var tempMDS = {};
        tempMDS.dcrId = ${dcrId};
        tempMDS.salesSlipNumber = "${ppaMDSObject.salesSlipNumber}";
        tempMDS.approvalCode = "${ppaMDSObject.approvalNumber}";
        tempMDS.cardNumber = "${ppaMDSObject.accountNumber}";
        tempMDS.reconBy = "${ppaMDSObject.reconBy}";
        tempMDS.reconDate = "${ppaMDSObject.reconDate}";
        if (isReconciled === 'Y') {
            tempMDS.reconciled = 'Y';
            mdsReconciledList.push(tempMDS);
        } else {
            tempMDS.reconciled = 'N';
            mdsUnreconciledList.push(tempMDS);
        }
    </script>
</c:forEach>

<script>
    sorttable.init();
    showSearchResultDiv();
    showResultsTable(searchTableData);
</script>

<div id="mdsNoteDiv">
    <form name="mdsNoteForm" id="mdsNoteForm" method="post">
        Remarks:
        <br/><textarea rows="7" cols="50" name="mdsNoteContent" id="mdsNoteContent" maxlength="100"></textarea>
        <span class="charRemaining"></span>
    </form>
</div>
