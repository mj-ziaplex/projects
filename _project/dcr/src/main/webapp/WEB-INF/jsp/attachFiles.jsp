<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<link rel="stylesheet"
	href="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/css/common.css"
	media="screen">
<link rel="stylesheet"
	href="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/css/attachFiles.css"
	media="screen">


<script
	src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/js/custom-table.js"></script>
<script
	src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/js/common.js"></script>
<link rel="stylesheet"
	href="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/jquery/jquery-ui-1.9.1/themes/base/jquery-ui.css" />
<script
	src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/jquery/jquery-1.8.2.js"></script>
<script
	src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/jquery/jquery-ui-1.9.1/ui/jquery-ui.js"></script>

<script>
	var reloadSP = "${reloadSP}";
	if(reloadSP == "true"){
		window.close();
		window.opener.location.reload();
	}
	var reloadMainWindow = "${reloadMainWindow}";
	if(reloadMainWindow == "true"){
		window.close();
		window.opener.reloadMainWindow();
	}
</script>

<div id="addMoreFilesDiv">

	<table class="attachmentsTable">
		<form id="attachForm"
			action="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/attachSP/doAttach.html?dcrId=${dcrId}"
			method="post" enctype="multipart/form-data">
			<tr>
				<th>Add More Files</th>
			</tr>
			<tr>
				<td><label for="browseButton">File:</label><input type="file"
					id="browseButton" value="Browse..." name="file"></td>
			</tr>
			<tr>
				<td><label for="descTextArea">Description:</label><br />
				<textarea id="descTextArea" name="description"></textarea></td>
			</tr>
			<tr>
				<td><div id="controls">
						<input type="button" id="uploadButton" value="Upload" onclick="attachFile()"> <input
							type="button" id="cancelButton" value="Cancel" onclick="window.close()">
					</div></td>
			</tr>
			<tr>
				<td>
					<div style="color: red;">${errorMessage}</div></td>
			</tr>
		</form>
	</table>
</div>
<script>
function attachFile(){
	var attachForm = document.getElementById("attachForm");
	
	var description = document.getElementById("descTextArea");
	
	if(!description.value){
		alert('Description cannot be blank!');
		
		return false;
	
	}
	
	attachForm.submit();
}
</script>