<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<html>
<head>
</head>
<body style="width:100%;height:100%">
<table border="0" cellspacing="0" cellpadding="0" style="width:100%;height:100%">
	<tr>
		<td id="contentCell">
                    <tiles:insertAttribute name="content" /></td>
	</tr>
	<tr>
		<td id="commentCell">
                    <tiles:insertAttribute name="comments" /></td>
	</tr>
	<tr>
		<td id="reversalCell">
                    <tiles:insertAttribute name="reversals" /></td>
	</tr>
</table>
</body>
</html>
