<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<link rel="stylesheet" href="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/css/wmsUser.css" media="screen">
<link rel="stylesheet" href="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/css/pagination.css" media="screen">
<link rel="stylesheet" href="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/jquery/jquery-ui-1.9.1/themes/base/jquery-ui.css" />
<script src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/jquery/jquery-1.8.2.js"></script>
<script src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/jquery/jquery-ui-1.9.1/ui/jquery-ui.js"></script>
<script src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/js/custom-table.js"></script>
<script src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/js/sorttable.js"></script>
<script src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/js/common.js"></script>
<script src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/js/pagination.js"></script>
<script src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/js/wmsUser.js"></script>

<c:if test="${action == null}">
    <div class="overflow" id="searchCriteriaDiv">
        <form id="searchUserForm" name="searchUserForm"
              action="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/admin/wmsUser.html"
              method="post">
            <table id="searchCriteriaTable">
                <tr>
                    <td class="searchCriteriaTableLabel">
                        <label for="searchDatePicker">User ID: </label>
                    </td>
                    <td colspan="5">
                        <input type="text" id="searchUserID" name="searchUserID" />&nbsp;
                        <input type="button" name="findNow" value="Search" onclick="submitSearchForm();" />
                    </td>
                </tr>
            </table>
        </form>
    </div>

    <div class="overflow" id="wmsUserDiv" >
        <div id="adminWmsUserStatusOperationDiv">
            <form method="POST" id="adminUserOperation">
                <input type="hidden" id="userIds" name="userIds" />
                <table id="adminWmsUserOperationTable">
                    <tr>
                        <td><input type="button" value="Remove User" id="removeUserBtn" onclick="doDeleteUser('${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/admin/wmsUserDoRemove.html');" /></td>
                        <td><input type="button" value="Add New User" id="addUserBtn" onclick="window.open('${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/admin/wmsUserForm.html', '_self');" /></td>
                    </tr>
                </table>
            </form>
        </div>

        <div id="wmsUserTableNode">
            <div id="wmsUserLabelNode"></div>
            <div id="wmsUserNode"></div>
        </div>
        <div id="addWmsUserNode"></div>	
        <div id="navigationNode"></div>
    </div>

    <script>
        sorttable.init();
        var users = [
            <c:forEach items="${userList}" var="user" varStatus="rowCnt">
                {userIdChkbox: "<input type='checkbox' id='userIdChkbox_${user.wmsuId}' rowInd='${user.wmsuId}' onchange='handleRemoveToggle(this);' />",
                    userId: "${user.wmsuId}",
                    userName: "${user.wmsuName}",
                    createdUserId: "${user.wmsuCreUser}",
                    createdUserTime: "<fmt:formatDate type='both' value='${user.wmsuCreDate}'/>",
                    updatedUserId: "${user.wmsuUpdUser}",
                    updatedUserTime: "<fmt:formatDate type='both' value='${user.wmsuUpdDate}'/>",
                    <c:if test="${user.wmsuActive == true}">
                        active: "Y",
                    </c:if>
                    <c:if test="${user.wmsuActive == false}">
                        active: "N",
                    </c:if>
                    action: "<a href='wmsUserForm.html?userId=${user.wmsuId}'>Edit</a>"
                }
                <c:if test="${not rowCnt.last}">,</c:if>
            </c:forEach>
        ];
        // Set WMS Users
        setWMSUsers(users);
    </script>
</c:if>

<!-- ***************************** -->
<!-- FORM FOR UPDATING USER ACCESS -->
<!-- ***************************** -->
<c:if test="${action == 'update' || action == 'new'}">
    <div id="wmsUserDiv">
        <form:form method="POST" name="adminWmsUserForm" id="adminWmsUserForm" modelAttribute="adminWmsUserForm">
            <table id="adminWmsUserFormTable">
                <tr>
                    <th align="left" colspan="2">WMS User</th>
                </tr>
                <tr class="evenRow">
                    <td>User ID</td>
                    <td><form:input id="wmsuId" path="wmsuId" size="25" /></td>
                </tr>
                <tr class="oddRow">
                    <td>User Name</td>
                    <td><form:input id="wmsuName" path="wmsuName" size="25" /></td>
                </tr>
                <tr class="evenRow">
                    <td>Company Code</td>
                    <td>
                        <form:select path="companyCode">
                            <form:option value="-" label="--"/>
                            <form:options items="${comCodes}"/>
                        </form:select>
                    </td>
                </tr>
                <tr class="evenRow">
                    <td>Center Code</td>
                    <td>
                        <form:select path="centerCodes" multiple="true" size="5">
                            <form:options items="${ccList}" itemValue="ccId" itemLabel="ccName" />
                        </form:select>
                    </td>
                </tr>
                <tr class="oddRow">
                    <td>User Hub</td>
                    <td>
                        <form:select path="userHubs" id="userHubs" multiple="true" size="5">
                            <form:options items="${userHubList}"/>
                        </form:select>
                    </td>
                </tr>
                <tr class="evenRow">
                    <td>User Group</td>
                    <td>
                        <form:select path="userGroups" id="userGroups" multiple="true" size="5">
                            <form:options items="${userGroupList}" itemValue="groupId" itemLabel="groupName"/>
                        </form:select>
                    </td>
                </tr>
                <tr>
                    <td class="oddRow">Active</td>
                    <td>
                        <form:radiobuttons path="wmsuActiveOpt" items="${activeOption}" />
                    </td>
                </tr>
                <tr>
                    <td class="evenRow">PS Lookup Cashier</td>
                    <td>
                        <form:radiobuttons path="psActiveOpt" items="${psActive}" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2"><br></td>
                </tr>
                <tr>
                    <td colspan="2" align="center">
                        <c:if test="${action == 'new'}">
                            <input id="addWmsUserBtn" type="button" value="Create" onclick="onDoSubmit('${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/admin/wmsUserDoCreate.html');" />
                        </c:if>	
                        <c:if test="${action == 'update'}">
                            <input id="updateWmsUserBtn" type="button" value="Update" onclick="onDoSubmit('${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/admin/wmsUserDoUpdate.html');" />
                        </c:if>	
                        <input id="cancelWmsUserBtn" type="button" value="Cancel" onclick="window.open('${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/admin/wmsUser.html', '_self');" />
                    </td>
                </tr>
            </table>
            <input type="hidden" id="userId" name="userId" value="${param.userId}">
        </form:form>
    </div>
</c:if>