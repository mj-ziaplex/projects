<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<link rel="stylesheet" href="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/css/wfHistory.css" media="screen">
<script src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/js/custom-table.js"></script>
<script src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/js/common.js"></script>
<script src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/js/wfHistory.js"></script>
<link rel="stylesheet" href="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/jquery/jquery-ui-1.9.1/themes/base/jquery-ui.css" />
<script src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/jquery/jquery-1.8.2.js"></script>
<script src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/jquery/jquery-ui-1.9.1/ui/jquery-ui.js"></script>
<div id="wfHistoryDiv">
	<div id="wfHistoryNode">
	</div>
</div>
<c:forEach items="${auditLog}" var="auditLog" varStatus="rowCnt">
	<script>
		var tempAudit = {};
		tempAudit.dcrDate = "${auditLog.dcrDateStr}";
		tempAudit.customerCenter = "${auditLog.docCapsiteName}";
		tempAudit.logDate = "${auditLog.logDate}";
		tempAudit.processStatus = "${auditLog.processStatus}";
		tempAudit.actionDetails = "${auditLog.actionDetails}";
		tempAudit.userId = "${auditLog.userId}";
		wfData.push(tempAudit);
	</script>
</c:forEach>
<script>
	generateWFHistoryTable();
</script>