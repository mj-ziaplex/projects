<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<link rel="stylesheet" href="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/css/errorTag.css" media="screen">
<script src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/js/custom-table.js"></script>
<script src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/js/common.js"></script>
<script src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/js/errorTag.js"></script>
<link rel="stylesheet" href="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/jquery/jquery-ui-1.9.1/themes/base/jquery-ui.css" />
<script src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/jquery/jquery-1.8.2.js"></script>
<script src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/jquery/jquery-ui-1.9.1/ui/jquery-ui.js"></script>
<script src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/js/sorttable.js"></script>

<div id="errorLookupDiv">
	<div id="errorLookupDivTitle" class="tableTitle">
		Error Tagging Utility
	</div>
	
	<input type="hidden" id="dcrId" value="${errorTagDcrId}"/>
	<input type="hidden" id="dcrStatus" value="${dcrStatus}"/>
	<input type="hidden" id="dcrDate" value="${dcrDate}"/>
	<input type="hidden" id="ccName" value="${ccName}"/>
	<input type="hidden" id="concatCashierNameListFromSP" value="${concatCashierNameListFromSP}"/>
	<input type="hidden" id="stepIdByStatus" value="${stepIdByStatus}"/>
	<input type="hidden" id="stepNameAsStatus" value="${stepNameAsStatus}"/>
	<input type="hidden" id="loggedInUser" value="${loggedInUser}"/>
	
	
	<form id="hiddenAddForm" name="hiddenAddForm" action="${pageContext.request.contextPath}${saveErrorTagMapping}" method="post">
		<!-- String sa form, but Long ung pinasa, add new Long var? -->
		<input type="hidden" id="fromPPA" value="${fromPPA}"/>
	</form>

	<form id="hiddenEditForm" name="hiddenEditForm" action="${pageContext.request.contextPath}${editErrorTagMapping}" method="post">
		<!-- not all will be used -->
		<input type="hidden" id="logIdentity" name="logIdentity" value=""/>
	</form>
	
	<form id="hiddenDeleteForm" name="hiddenDeleteForm" action="${pageContext.request.contextPath}${deleteErrorTagMapping}" method="post">
		<!-- not all will be used -->
		<input type="hidden" id="reasonDeleted" name="reasonDeleted" value=""/>
		<input type="hidden" id="stepId" name="stepId" value=""/>
		<input type="hidden" id="userId" name="userId" value=""/>
		<input type="hidden" id="tagger" name="tagger" value=""/>
		
	</form>
	
	<c:forEach items="${errorList}" var="error" varStatus="errCnt">
		<script>
			var tempErr = {};
			tempErr.errorId = "${error.errorId}";
			tempErr.errorName = "${error.errorName}";
			errorListByStatus.push(tempErr);
		</script>
	</c:forEach>
		
	<!-- selectUser -->User Name: &nbsp;
	<select id="userNameSelect" name="userNameSelect" class="ETselect">
		<option selected="selected" value="none">-------------------------------</option>
		<c:forEach items="${cashierIDs}" var="cashierID" varStatus="userCnt"><!-- see ppaSP.js-openErrorTag function -->
			<option value="${cashierID}">${cashierID}</option>
		</c:forEach>
	<!--<c:if test="${not empty managerId}">
			<option value="${managerId}">${managerId}</option>
		</c:if>-->
	</select>
	<br/>
	
	<!-- chkboxesErr -->
	<div id="errorListDiv"></div>
	<br/>
	
	<!-- txtfldReason -->Reason: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	<input id="reasonField" name="reasonField" type="text" value="" />
	<br/>
	${loggedInUser}
	<!-- buttonsAddExit -->
	<div id="lookupButtonsDiv">
	</div>
</div>
<script>
	dcrId = ${errorTagDcrId};
	dcrDate = "${dcrDate}";
	ccName = "${ccName}";
</script>
<script>
	generateErrorListCheckboxes();
	generateLookupButtons();
</script>

<div id="errorLogDiv">
	<div id="errorLogNode">
	</div>
</div>
<c:forEach items="${errLog}" var="errLog" varStatus="rowCnt">
	<script>
		var tempErrLog = {};
		
		tempErrLog.identity = "${errLog.id}";
		tempErrLog.id = "${errLog.dcrId}_${errLog.stepId}_${errLog.stepCompletorUserId}_${errLog.postedUpdatedById}";
		tempErrLog.radio = "<input type='radio' id='${errLog.dcrId}_${errLog.stepId}_${errLog.stepCompletorUserId}_${errLog.postedUpdatedById}' name='radioLog' value='${errLog.dcrId}' stepId='${errLog.stepId}' onclick='enableLogButtons(this)' />";
		tempErrLog.datePosted = "${errLog.datePostedStr}";
		tempErrLog.dateUpdated = "${errLog.dateUpdatedStr}";
		tempErrLog.stepId = "${errLog.stepId}";
		tempErrLog.stepName = "${errLog.stepName}";
		tempErrLog.userName = "${errLog.stepCompletorUserId}";
		tempErrLog.userNameX = "${errLog.stepCompletorUserId}";
		
		var errorsPerRow = "";
		var errorList = [];
		<c:forEach items="${errLog.errors}" var="err" varStatus="errCnt">
			errorsPerRow = errorsPerRow + "- " + "${err.errorName}" + "<br>";
			var tempErr = {};
			tempErr.errorId = "${err.errorId}";
			tempErr.errorName = "${err.errorName}";
			errorList.push(tempErr);
		</c:forEach>
		tempErrLog.errors = errorsPerRow;//1 cell, but all errors here in this column
		tempErrLog.errorList = errorList;//2d array to store them
		//alert("reason sa push: "+"${errLog.reason}");
		tempErrLog.reason = "${errLog.reason}";
		tempErrLog.reportedUpdatedBy = "${errLog.postedUpdatedById}";
		tempErrLog.reportedUpdatedByX = "${errLog.postedUpdatedById}";
		tempErrLog.reasonDeleted = "${errLog.reasonDeleted}";//if may laman, disable radio/row and i-red
		
		wrapDivToRow(tempErrLog,"${errLog.reasonDeleted}","${errLog.dcrId}_${errLog.stepId}_${errLog.stepCompletorUserId}");
		
		errLogData.push(tempErrLog);
	</script>
</c:forEach>

<script>
	sorttable.init();
var fromPPA = ${fromPPA};
	errLogLayout = fromPPA?PPALayout:nonPPALayout;
	generateErrorLogTable();
	autoSort();
</script>
<div id="logButtonsDiv">
</div>
<script>
	generateLogButtons();
</script>