<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/strict.dtd">
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<link rel="stylesheet"
	href="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/jquery/jquery-ui-1.9.1/themes/base/jquery-ui.css" />
<script
	src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/jquery/jquery-1.8.2.js"></script>
<script
	src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/jquery/jquery-ui-1.9.1/ui/jquery-ui.js"></script>

<link
	href="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/css/common.css"
	media="screen" rel="stylesheet" type="text/css">
<link
	href="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/css/createEmail.css"
	media="screen" rel="stylesheet" type="text/css">

<script
	src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/js/common.js"></script>
<script>
	var reloadSP = "${reloadSP}";
	if(reloadSP == "true"){
		window.close();
		window.opener.location.reload();
	}
</script>
<form id="emailForm" class="emailForm" method="post"
	action="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/email/send.html">
<input type="hidden" name="dcrId" value="${dcrId}" />
<table class="emailTable">
	<tr>
		<th>
		<div id="emailTableHeader">Email</div>
		</th>
	</tr>

	<tr>
		<td><input type="text" id="emailTableTo" name="to" value="${toFromErrorTag}"> <label
			class="emailTableLabel" for="emailTableTo">To</label>
	</tr>
	<tr>
		<td><input type="text" id="emailTableCc" name="cc" value="${ccFromErrorTag}"> <label
			class="emailTableLabel" for="emailTableCc">cc</label>
	</tr>
	<tr>
		<td><div class="controls"><input class="controls" type="button" id="addRemoveButton" value="Add / Remove"
			onclick="showAddressBook()"></div>
	</tr>

	<tr>
		<td><input type="text" id="emailTableSubject" name="subject" value="${subjectFromErrorTag}">
		<label class="emailTableLabel" for="emailTableSubject">Subject</label>
	</tr>

	<tr>
		<td><label class="emailTableLabel" for="emailTableContent">Message</label>
		<textarea id="emailTableContent" name="content"></textarea>
	</tr>

	<tr>
		<td><div class="controls" ><input  type="button" id="sendButton"value="Send" onclick="submitForm()"> 
			<input type="button" id="cancelButton" value="Cancel"	onclick="window.close()">
			</div>
			</td>
	</tr>
</table>
</form>

<div id="addRemoveDiv">
<table id="addRemoveTable">
	<tr>
		<td><select multiple id="addressBookSelect">
			<optgroup label="" id="addressBookOptGrp"></optgroup>
		</select></td>
		<td><b><input type="button" value=">" onclick="addToSelected()"/><br />
		<input type="button" value="<" onclick="removeFromSelected()"/></b>
		</td>
		<td><select multiple id="selectedContacts">
		</select></td>
	</tr>
</table>

</div>

<script>
	var errorsPerRow = "";
	<c:if test="${fromErrorTag}">
		errorsPerRow = "Errors:\n";
	</c:if>
	<c:forEach items="${contentFromErrorTag}" var="error" varStatus="errCnt">
		errorsPerRow = errorsPerRow + "- " + "${error}" + "\n";
	</c:forEach>

	<c:forEach items="${addressBook}" var="contact" varStatus="rowCnt">
		addOption('${contact.fullName}', '${contact.emailAddress}')
	</c:forEach>

	function addOption(optionText, optionValue) {
		$(document).ready(function() {
			var selectList = document.getElementById("addressBookSelect");
			var optionObject = new Option(optionText, optionValue);
			var optionRank = selectList.options.length;
			selectList.options[optionRank] = optionObject;
		});
	}

	function addToSelected() {
		$(document).ready(function() {
			var select = document.getElementById("addressBookSelect");
			var selectedContacts = document.getElementById("selectedContacts");
			for (i = 0; i < select.options.length; i++) {
				if (select[i].selected == true) {
					if (select[i].length != 0) {
						strOption = select[i].value;
						addOptionList(selectedContacts,  strOption);
						strOption = "";
					}
				}
			}
		});
	}
	
	function removeFromSelected() {
		$(document).ready(function() {
		var selectedContacts = document.getElementById("selectedContacts");
	    var i;
	    for(i=selectedContacts.options.length-1;i>=0;i--)
	    {
	       if(selectedContacts[i].selected==true){
	    	   selectedContacts.remove(i);
	       }
	    } 
		});
	}

	function addOptionList(selectObject, optionValue) {
		var optionObject = new Option(optionValue, optionValue)
		var optionRank = selectObject.options.length
		selectObject.options[optionRank] = optionObject
	}

	$(function() {
		$('optgroup').each(
				function() {
					var optgroup = $(this), options = optgroup.children()
							.toArray().sort(function(a, b) {
								return $(a).text() < $(b).text() ? 1 : -1
							})
					$.each(options, function(i, v) {
						optgroup.prepend(v)
					})
				})
	})
	
	$(function(){
		document.getElementById("emailTableContent").value = errorsPerRow;
	})
	
	function submitForm() {
		validateForm();
		var emailForm = document.getElementById("emailForm");
		emailForm.submit();
		return true;
	}

	function showAddressBook() {
		$(function() {
			$("#addRemoveDiv")
					.dialog(
							{
								autoOpen : true,
								height : "auto",
								minHeight : 580,
								width : 500,
								modal : true,
								closeOnEscape : true,
								draggable : false,
								resizable : false,
								dialogClass : "",
								title : "Address Book",
								buttons : [
										{
											id : '',
											text : 'Add to',
											click : function() {
												addToSendingList('to');
												$(this).dialog('close');
											}
										}, {
											id : '',
											text : 'Add cc',
											click : function() {
												addToSendingList('cc');
												$(this).dialog('close');
											}
										}, {
											id : '',
											text : 'Remove All',
											click : function() {
												removeAll();
												$(this).dialog('close');
											}
										} ]
							}).dialog('widget').position({
						my : 'center',
						at : 'center',
						of : $(document.getElementById("mainTemplateBodyId"))
					});
		});
	}
	
	function addToSendingList(type){
			var selectedContacts = document.getElementById("selectedContacts");
			var tempString="";
			var selected= "";
			for(i=0; i< selectedContacts.length;i++){
				tempString = tempString + selectedContacts[i].value;
				 if(i < selectedContacts.length-1)
					 tempString = tempString + ", ";
			}
			if (type=='to'){
				var emailTo = document.getElementById("emailTableTo");
				if(emailTo.value.length==0){
					emailTo.value = tempString;
				}else{
					emailTo.value = emailTo.value + ", " +tempString;
				}
			}else if (type =='cc'){
				var emailCc = document.getElementById("emailTableCc");
				if(emailCc.value.length==0){
					emailTo.value = tempString;
				}else{
					emailCc.value = emailCc.value + ", " +tempString;
				}
			}
			
			
	}
	
	function removeAll(){
		var emailTo = document.getElementById("emailTableTo");
		var emailCc = document.getElementById("emailTableCc");
		emailTo.value='';
		emailCc.value='';
		var selectedContacts = document.getElementById("selectedContacts");
		   var i;
		    for(i=selectedContacts.options.length-1;i>=0;i--)
		    {
		    	   selectedContacts.remove(i);
		    } 
	}
	
	function validateForm(){
		 validateTo();
		 validateCc();
	}
	
	function validateTo(){
	var emailTo= document.getElementById("emailTableTo");
	var emails= emailTo.value;
		if(emails.length>0){
		emails = emails.replace(/;/g,",");
		emails = emails.replace(/:/g,",");
		var toArr = emails.split(",");
		
		emailTo.value =validateEmailAdds(toArr);
		}else{
			alert("\"To\" cannot be blank");
			return false;
		}
	}
	
	
	function validateCc(){
		var emailCc=document.getElementById("emailTableCc");
		var emails= emailCc.value;
		emails = emails.replace(/;/g,",");
		emails = emails.replace(/:/g,",");
		var ccArr = emails.split(",");

		emailCc.value =validateEmailAdds(ccArr);
		
	}
	
	function validateEmailAdds(emailArr){
		var emails="";
		for(var i=0; i < emailArr.length; i++){
			if(emailArr[i].length!=0){
				var str = trim(emailArr[i]);
				var atpos=str.indexOf("@");
				var dotpos=str.lastIndexOf(".");
				var emailAdd="";
					if (atpos<1 || dotpos<atpos+2 || dotpos+2>=str.length)  {
						emailAdd = str+"@Sunlife.com";
					 if(emails.length==0){
						emails = emailAdd;
					 }else{
						emails = emails +","+emailAdd;
					 }
					  //return false;
				  }else{
						if(emails.length==0){
							 emails = str;
						}else{
							 emails = emails +","+str;
						}
				  }
			}				
		}
		return emails;
	}
</script>

