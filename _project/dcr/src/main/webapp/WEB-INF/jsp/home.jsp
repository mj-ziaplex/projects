<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<link rel="stylesheet" href="/WMS-Web/css/home.css" media="screen">
<link rel="stylesheet" href="/WMS-Web/css/home_dcr.css" media="screen">
<script src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/js/sorttable.js"></script>
<link rel="stylesheet" href="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/jquery/jquery-ui-1.9.1/themes/base/jquery-ui.css" />
<script src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/jquery/jquery-1.8.2.js"></script>
<script src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/jquery/jquery-ui-1.9.1/ui/jquery-ui.js"></script>
<script src="/WMS-Web/js/home.js"></script>
<script src="/WMS-Web/js/custom-table.js"></script>
<script src="/WMS-Web/js/common.js"></script>

<div class="loader" style="display: none"></div>

<div class="overflow" id="homeDiv" >
    <div id="homeTableNode">
        <div id="homeLabelNode"></div>
        <div id="homeNode"></div>
    </div>
    <div id="addhomeNode"></div>	
    <div id="navigationNode"></div>
</div>

<p/><p/><p/>

<div class="overflow" id="homeDCRDiv" >
    <div id="homeDCRTableNode">
        <div id="homeDCRLabelNode"></div>
        <div id="homeDCRNode"></div>
    </div>
    <div id="addhomeDCRNode"></div>	
    <div id="navigationNode"></div>
</div>

<script>
    sorttable.init();
    var cashierWorkItems = [
    <c:choose>
        <c:when test="${not empty list}">
            <c:forEach items="${list}" var="bo" varStatus="rowCnt">
                    {customerCenter: "${bo.dcr.ccName} (${bo.dcr.centerCode})",
                            date: "<a class='blue' href='${pageContext.request.contextPath}/slamcip/index.html?dcrCashierId=${bo.id}' onClick='showLoader()'>${bo.dcr.formatedDateStr}</a>",
                            status: "${bo.status}"}
            <c:if test="${not rowCnt.last}">,</c:if>                        </c:forEach>
        </c:when>
        <c:otherwise>
                                {customerCenter: "&nbsp;", date: "No Cashier Work Item", status: "&nbsp;"}
        </c:otherwise>
    </c:choose>
                    ];
                            setCashierWorkItems(cashierWorkItems);
                    var dcrWorkItems = [
    <c:choose>
        <c:when test="${not empty dcrBOList}">
            <c:forEach items="${dcrBOList}" var="bo" varStatus="rowCnt">
                    {customerCenter: "${bo.ccName} (${bo.centerCode})",
                            date: "<a class='blue' href='#' aplink='${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/cashierSP/processData.html?dcrId=${bo.id}' splink='${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/cashierSP/index.html?dcrId=${bo.id}&fromSearch=false' stat='${bo.status}' onclick='openAjaxCashierSP(this);'>${bo.formatedDateStr}</a>",
                            status: "${bo.status}",
                            requiredCompletionDate: "${bo.formatedRCDStr}"}
            <c:if test="${not rowCnt.last}">,</c:if>                        </c:forEach>
        </c:when>
        <c:otherwise>
                                {customerCenter: "&nbsp;", date: "No DCR Work Item", status: "&nbsp;", requiredCompletionDate: "&nbsp"}
        </c:otherwise>
    </c:choose>
                    ];
                            setDCRWorkItems(dcrWorkItems);
</script>
