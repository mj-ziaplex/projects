<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<link rel="stylesheet" href="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/css/adminPage.css" media="screen">
<link rel="stylesheet" href="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/jquery/jquery-ui-1.9.1/themes/base/jquery-ui.css" />
<script src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/jquery/jquery-1.8.2.js"></script>
<script src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/jquery/jquery-ui-1.9.1/ui/jquery-ui.js"></script>

<div id="adminDiv" hidden="true">
	<div id="adminTableTitle" class="tableTitle">
		DCR MAINTENANCE
	</div>
	<div id="adminNode">
		<table class="adminTable">
			<tr>
				<td class="evenRow"><a
					href="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/admin/centerCode.html">Center Codes</a></td>
			</tr>
			<tr>
				<td class="oddRow"><a
					href="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/admin/dcrStatus.html">DCR Status</a></td>
			</tr>
			<tr>
				<td class="evenRow"><a
					href="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/jobData/index.html">Batch Job
						</a></td>
			</tr>
			<tr>
				<td class="oddRow"><a
					href="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/admin/wmsUser.html">User Account</a></td>
			</tr>
			<tr>
				<td class="evenRow"><a
					href="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/admin/datacapScanner.html">Datacap Scanner
						</a></td>
			</tr>
		</table>
	</div>
</div>
