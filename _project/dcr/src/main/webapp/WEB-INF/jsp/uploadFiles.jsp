<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<link rel="stylesheet"
	href="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/css/common.css"
	media="screen">
<link rel="stylesheet"
	href="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/css/uploadFiles.css"
	media="screen">

<script
	src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/js/custom-table.js"></script>
<script
	src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/js/common.js"></script>
<script
	src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/js/uploadFiles.js"></script>
<link rel="stylesheet"
	href="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/jquery/jquery-ui-1.9.1/themes/base/jquery-ui.css" />
<script
	src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/jquery/jquery-1.8.2.js"></script>
<script
	src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/jquery/jquery-ui-1.9.1/ui/jquery-ui.js"></script>

<script>
	$(function() {
		$("#progressbar").progressbar({
			value : 70
		});
	});
</script>

<div id="uploadHeader">Upload Files</div>

<div class="overflow" id="uploadTableDiv">
<div id="uploadTableNode"></div>

</div>

<div id="addMoreFilesDiv">
 

<!-- add actual building of data from controller here.
<c:forEach items="${uploadedFileList}" var="column" varStatus="rowCnt">


</c:forEach>
 -->

<table>
	<form id="uploadForm" action="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/upload/doupload.html?dcrCashierId=${dcrCashierBO.id}" method="post" enctype="multipart/form-data">
	<tr>
		<th>Add More Files</th>
	</tr>
	<tr>
		<td>File</td>
		<td><input type="file" id="browseButton" value="Browse..." name="file">
		</td>
	</tr>
	<!--  
	<tr>
		<td>Type</td>
		<td><select name="type">
			<option value="pdf">PDF</option>
			<option value="excel">MS Excel</option>
			<option value="excel2007">MS Excel 2007</option>
			<option value="doc">MS Word Doc</option>
			<option value="docx">MS Word Doc 2007</option>
		</select></td>
	</tr>
	-->
	<tr>
		<td>Description</td>
		<td><textarea id="descTextArea" name="description"></textarea></td>
		</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td><input type="button" id="browseButton" value="Upload" onclick="uploadFile()"><br/>
			<div style="color: red;">${errorMessage}</div>
		</td>
	</tr>
	</form>
</table>
</div>

<script>
	var data = [
		<c:choose>
	    	<c:when test="${not empty attachments}">
				<c:forEach items="${attachments}" var="bo" varStatus="rowCnt">
			    	{ 
			    			"fileId":"<a href='${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/${bo.fileUrl}' target='_blank'>${bo.filename}</a>" ,	
			    			"fileType":"${bo.fileType}",
			    			"fileDescription":"${bo.description}"
			    	}
					<c:if test="${not rowCnt.last}">
			        	,
					</c:if>
				</c:forEach>
			</c:when>
    		<c:otherwise>
		    	{
	    			"fileId":"&nbsp;" ,	
	    			"fileType":"&nbsp;",
	    			"fileDescription":"&nbsp;"
				}
			</c:otherwise>
		</c:choose>
			];
			
	loadTable(data);
	
	
			
</script>
