<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<link rel="stylesheet" href="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/css/datacapScanner.css" media="screen">
<link rel="stylesheet" href="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/css/pagination.css" media="screen">
<link rel="stylesheet" href="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/jquery/jquery-ui-1.9.1/themes/base/jquery-ui.css" />
<script src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/jquery/jquery-1.8.2.js"></script>
<script src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/jquery/jquery-ui-1.9.1/ui/jquery-ui.js"></script>
<script src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/js/custom-table.js"></script>
<script src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/js/sorttable.js"></script>
<script src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/js/common.js"></script>
<script src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/js/pagination.js"></script>

<script src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/js/datacapScanner.js"></script>

<c:if test="${action == null}">
    <div class="overflow" id="searchCriteriaDiv">
        <form id="searchScannerForm" name="searchScannerForm"
              action="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/admin/datacapScanner.html"
              method="post">
            <table id="searchCriteriaTable">
                <tr>
                    <td class="searchCriteriaTableLabel">
                        <label for="searchDatePicker">PC ID: </label>
                    </td>
                    <td colspan="5">
                        <input type="text" id="searchScannerID" name="searchScannerID" />&nbsp;
                        <input type="button" name="findNow" value="Search" onclick="submitSearchForm();" />
                    </td>
                </tr>
            </table>
        </form>
    </div>

    <div class="overflow" id="datacapScannerDiv" >
        <div id="adminDatacapScannerStatusOperationDiv">
            <form method="POST" id="adminScannerOperation">
                <input type="hidden" id="pcNames" name="pcNames" />
                <table id="adminDatacapScannerOperationTable">
                    <tr>
                        <td><input type="button" value="Remove User" id="removeUserBtn" onclick="doDeleteUser('${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/admin/datacapScannerDoRemove.html');" /></td>                        
                    </tr>
                </table>
            </form>
        </div>

        <div id="datacapScannerTableNode">
            <div id="datacapScannerLabelNode"></div>
            <div id="datacapScannerNode"></div>
        </div>
        <div id="addWmsUserNode"></div>	
        <div id="navigationNode"></div>
    </div>

    <script>
        sorttable.init();
        var scanners = [
            <c:forEach items="${scannerList}" var="scanner" varStatus="rowCnt">
                {scannerIdChkbox: "<input type='checkbox' id='scannerIdChkbox_${scanner.wsuName}' rowInd='${scanner.wsuName}' onchange='handleRemoveToggle(this);' />",
                	scannerId: "${scanner.wsuName}",
                	datacapUserName: "${scanner.wsuContactperson}",
                	branchName: "${scanner.wsuDeptbrnchname}",
                	createdDate: "<fmt:formatDate type='both' value='${scanner.wsuCredate}'/>"
                }
                <c:if test="${not rowCnt.last}">,</c:if>
            </c:forEach>
        ];
        // Set Datacap scanners
        setDatacapScanners(scanners);
               
    </script>
</c:if>


<!-- ***************************** -->
<!-- FORM FOR UPDATING USER ACCESS -->
<!-- ***************************** -->
