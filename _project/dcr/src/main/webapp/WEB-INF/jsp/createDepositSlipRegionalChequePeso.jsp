<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/strict.dtd">
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<link rel="stylesheet"
	href="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/jquery/jquery-ui-1.9.1/themes/base/jquery-ui.css" />
<script
	src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/jquery/jquery-1.8.2.js"></script>
<script
	src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/jquery/jquery-ui-1.9.1/ui/jquery-ui.js"></script>
	

<link
	href="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/css/common.css"
	media="screen" rel="stylesheet" type="text/css">
<link
	href="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/css/createDepositSlip.css"
	media="screen" rel="stylesheet" type="text/css">


<script
	src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/js/custom-table.js"></script>
<script
	src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/js/createDepositSlip.js"></script>
<script
	src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/js/common.js"></script>



<script>
    $(function() {
        $( "#datepicker" ).datepicker({ dateFormat: 'ddMyy' });
    });
    </script>

<form id="chequeDepositSlipForm" class="chequeDepositSlipForm"
	method="post">

<div class="group"><!-- removed based on FRS 1.0
		<label for="customerCenter">Customer Center</label>  --> <input
	id="customerCenter" type="hidden" name="customerCenterName"
	readonly="readonly" tabindex="-1" tabindex="-1" class="readonly"
	value="${bo.customerCenterName}" /> <input id="customerCenter"
	type="hidden" name="customerCenter" readonly="readonly" tabindex="-1"
	tabindex="-1" class="readonly" value="${bo.customerCenter}" /> <input
	id="cashierName" type="hidden" name="cashierName" readonly="readonly"
	tabindex="-1" tabindex="-1" class="readonly" value="${bo.cashierName }" />

<div class="content"><label for="date">Date</label> <input
	type="text" id="datepicker" name="depositDateStr" readonly="readonly"
	value="${bo.depositDateStr}" /></div>

<div class="contentRight"><label for="accountNumber">Account
Number</label> <input id="accountNumber" type="text" name="accountNumber"
	readonly="readonly" class="readonly" tabindex="-1"
	value="${bo.accountNumber}" /></div>

<div class="contentRight"><label for="currency">Currency</label> <input
	id="currency" type="text" name="currencyDisplay" readonly="readonly"
	tabindex="-1" class="readonly" value="PESO" /></div>

<div class="contentRight"><label for="dcrDate">DCR Date</label> <input
	type="text" id="dcrDate" readonly="readonly" tabindex="-1"
	class="readonly" name="dcrDateStr"
	value="${dcrCashierBO.dcr.formatedDateStr}" /></div>
</div>


<div class="group">
<div class="content"><label for="depositedWith">Deposited with</label>
<input id="depositedWith" type="text" name="depositoryBankId" readonly="readonly" tabindex="-1" class="readonly" value="${bo.depositoryBankId}" /></div>
</div>

<div class="group">
<div class="content"><label for="toTheCreditOf">To the
credit of</label> <input id="toTheCreditOf" type="text" name="companyStr"
	readonly="readonly" tabindex="-1" class="readonly"
	value="${bo.companyStr}" /></div>
</div>


<div id="sessionGroupCheque">

<div class="content"><input id="depositSlipType" type="text"
	name="depositSlipType" readonly="readonly" tabindex="-1"
	class="readonly" value="CHEQUE" /></div>


<!-- 
<div class="contentRight"></div>

<div class="contentRight">

</div>

</div>


<div class="sessionGroupRightCheque">


<div class="groupRight"></div>

</div>


<div class="contentMiddleCheque"></div>
 -->


<table id="chequeTable">

	<tr>
		<td>
		<div id="chequeTypeLabel">Cheque Type</div>
		</td>
		<td><input id="chequeType" type="text"
			name="chequeType" readonly="readonly" tabindex="-1"
			class="readonlyChequeType" value="REGIONAL CHEQUE" /></td>
	</tr>
	<tr>
		<td>
		<div id="chequeAmountLabel">Cheque Amount</div>
		</td>
		<td><input id="amountCheque" type="text"
			name="amountCheque" readonly="readonly" class="readonlyCash"
			value="${bo.totalCheckAmount}" /> <input id="hiddenAmountCheque"
			type="hidden" name="hiddenAmountCheque" readonly="readonly"
			class="readonlyCash" value="${bo.totalCheckAmount}" /></td>
		<td width="116">
		<div id="collectionTotalLabel">Collection Total</div>
		</td>
		<td width="188"><input id="collectionTotal" type="text"
			name="collectionTotal" readonly="readonly" tabindex="-1"
			class="readonly" value="" /> <input id="hiddenCollectionTotal"
			type="hidden" name="hiddenCollectionTotal"
			value="${bo.requiredTotalAmount}" /></td>
	</tr>
	
	<c:if test="${bo.companyId eq 3 && bo.currencyId eq 1 }" >
	<tr>
		<td></td>
		<td></td>
		<td >	
		<div id="TradVulLabel" ><p>TRAD / VUL</p></div>
		</td>

		<td >
		<input id="tradVulCollectionTotal" type="text" name="tradVulCollectionTotal" readonly="readonly" tabindex="-1" class="readonly" value=""/>
		<input id="hiddenTradVulCollectionTotal" type="hidden" name="hiddenTradVulCollectionTotal" value="${bo.tradVulCollectionTotal}" /><!-- ${bo.tradVulCollectionTotal} -->
		</td>
	</tr>
	
	<tr>
		<td></td>
		<td></td>
		<td>
		<div id="GroupLifeLabel" >GRP LIFE</div>
		</td>
		<td>
		<input id="grpLifeCollectionTotal" type="text" name="grpLifeCollectionTotal" readonly="readonly" tabindex="-1" class="readonly" value=""/>
		<input id="hiddenGrpLifeCollectionTotal" type="hidden" name="hiddenGrpLifeCollectionTotal" value="${bo.grpLifeCollectionTotal}" /><!-- ${bo.grpLifeCollectionTotal} -->
		</td>
	</tr>
	<script>
	var tradVul = document.getElementById("tradVulCollectionTotal");
	tradVul.value = numberWithCommas(parseFloat(${bo.tradVulCollectionTotal}).toFixed(2));
	var grpLife = document.getElementById("grpLifeCollectionTotal");
	grpLife.value = numberWithCommas(parseFloat(${bo.grpLifeCollectionTotal}).toFixed(2));
	</script>
	</c:if>


	<c:if test="${hasMixCheque eq true }" >	
	<tr><td></td></tr>
	<tr><td></td></tr>
	<tr><td></td></tr>
	
	<tr>
	<td></td>
	</tr>
	<tr>
	<td></td>
	</tr>
	
	<tr>
		<td></td>
		<td></td>
		<td>
		<div id="mixedChequeLabel">Total Mixed Cheque</div>
		</td>
		<td><input id="mixedChequeTotal" type="text"
			name="mixedChequeTotal" readonly="readonly" tabindex="-1"
			class="readonly" value="" /> <input id="hiddenMixedChequeTotal"
			type="hidden" name="hiddenMixedChequeTotal" value="" /></td>

	</tr>

	<tr>
		<td></td>
		<td></td>
		<td>
		<div id="SLOCPI">SLOCPI</div>
		</td>
		<td><input id="slocpiTotal" type="text" name="slocpiTotal"
			readonly="readonly" tabindex="-1" class="readonly" value="${bo.slocpiTotal }" /> 
			<input id="hiddenSlocpiTotal" type="hidden" name="hiddenSlocpiTotal" value="${bo.slocpiTotal }" /></td>

	</tr>
	<tr>
		<td></td>
		<td></td>
		<td>
		<div id="SLFPI">SLFPI</div>
		</td>
		<td><input id="slfpiTotal" type="text" name="slfpiTotal"
			readonly="readonly" tabindex="-1" class="readonly" value="${bo.slfpiTotal }" /> <input
			id="hiddenSlfpiTotal" type="hidden" name="hiddenSlfpiTotal" value="${bo.slfpiTotal }" /></td>

	</tr>
	</c:if>
</table>
	


<!-- 
			<div class="contentRight" >
			<input id="totalAmount" type="text" name="totalAmount" readonly="readonly" tabindex="-1" class="readonlyCash" value="0.00"/>
			<input id="hiddenTotalAmount" type="hidden" name="totalCheckAmount" value="${bo.totalCheckAmount}"/>
			</div>
			
			<div class="contentMiddleCheque">
			<div id="totalChequeAmountLabel">Total Amount</div>
			</div>
			
		
			<div class="contentRight" >
			<input id="discrepancy" type="text" name="discrepancy" readonly="readonly" tabindex="-1" class="readonlyCash" value="0.00"/>
			</div> 
			
			<div class="contentMiddleCheque">
			<div id="chequediscrepancyLabel">Discrepancy</div>
			</div>--></div>


<br />
<br />
<input id="hiddenTotalAmount" type="hidden" name="totalCheckAmount" value="${bo.totalCheckAmount}"/>
<input id="hiddenRequiredTotalAmount" type="hidden" name="requiredTotalAmount" value="${bo.requiredTotalAmount}"/>
<input type="hidden" name="prodCode" value="${bo.prodCode }" /> 
<input type="hidden" name="companyId" value="${bo.companyId}" /> 
<input type="hidden" name="currencyId" value="${bo.currencyId}" /> 
<input type="hidden" name="checkType" value="${bo.checkType}" /> 
<input type="hidden" name="dcrCashierId" value="${dcrCashierBO.id}" />
<input type="hidden" name="acf2id" value="${bo.acf2id}" />
<div id="controls"><input type="button" value="PREVIEW"
	id="previewButton" onclick="previewPDF()" /> <input type="button"
	value="SAVE" id="saveButton" onclick="submitForm()" /></div>
</form>
<script>
var saveDisabled =false;
	function previewPDF(){
		$(document).ready( function(){
			
			var date = document.getElementById("datepicker");
			if(!date.value){
				alert('Date cannot be blank');
				
				return false;
			
			}
			
			document.getElementById("chequeDepositSlipForm").action="${pageContext.request.contextPath}${previewMapping}" ;
			document.getElementById("chequeDepositSlipForm").target="_blank";
			document.getElementById("chequeDepositSlipForm").submit();
		 return true;
		 
		});
	}
	

	function submitForm(){
		$(document).ready( function(){
			
			var date = document.getElementById("datepicker");
			if(!date.value){
				alert('Date cannot be blank');
				
				return false;
			
			}
			if(!${isSavedDisabled}){
				document.getElementById("chequeDepositSlipForm").action="${pageContext.request.contextPath}${saveMapping}" ;
				document.getElementById("chequeDepositSlipForm").target="_self";
				document.getElementById("chequeDepositSlipForm").submit();
			}
			var saveButton = document.getElementById('saveButton');
			var previewButton = document.getElementById('previewButton');
			previewButton.disabled = true;
			saveButton.disabled = true;
		 return true;
		});
	}

	 initializePageCheque('PESO','${isSavedDisabled}','ChequeRegional','${bo.requiredTotalAmount}','${bo.company.name}','${hasMixCheque}',${isConfirmed});
	</script>

