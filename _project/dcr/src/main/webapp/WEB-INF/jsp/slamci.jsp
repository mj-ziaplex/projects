<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<jsp:useBean id="dateDoNotUse" class="java.util.Date" />
<c:set var="url" value="${pageContext.request.contextPath}" scope="request"/>
<c:set var="jsNoCache" value="?jsNoCache=${dateDoNotUse.time}" scope="request"/>

<link rel="stylesheet" href="${url}/css/common.css" media="screen">
<link rel="stylesheet" href="${url}/css/slamci.css" media="screen">
<link rel="stylesheet" href="${url}/jquery/jquery-ui-1.9.1/themes/base/jquery-ui.css" />
<script src="${url}/jquery/jquery-1.8.2.js"></script>
<script src="${url}/jquery/jquery-ui-1.9.1/ui/jquery-ui.js"></script>

<script src="${url}/js/custom-table.js${jsNoCache}"></script>
<script src="${url}/js/common.js${jsNoCache}"></script>
<script src="${url}/js/slamci.js${jsNoCache}"></script>

<div class="overflow" id="slamciTableDiv" >
    <div id="slamciTableNode">
    </div>

    <c:choose>
        <c:when test="${company == 'SLAMCIP'}">
            <form id="slamcipForm" name="slamcipForm" action="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/slamcip/confirm.wms" method="post">
                <div id="confirmButtonDiv">
                </div>	
                <input type="hidden" name="dcrCashierId" value="${dcrCashierBO.id}"/>
                <input type="hidden" id="htmlCode" name="htmlCode" value=""/>
            </form>
        </c:when>
        <c:otherwise>
            <form id="slamcidForm" name="slamcidForm" action="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/slamcid/confirm.wms" method="post">
                <div id="confirmButtonDiv">
                </div>	
                <input type="hidden" name="dcrCashierId" value="${dcrCashierBO.id}"/>
                <input type="hidden" id="htmlCode" name="htmlCode" value=""/>
            </form>
        </c:otherwise>
    </c:choose>
</div>




<c:forEach items="${productsToDisplay}" var="column" varStatus="rowCnt">
    <script>
        var currency = '${column.productType.currency}';

        cashCounter.${column.productType.productCode } = numberWithCommas(parseFloat("${column.totalCashCounter}").toFixed(2));
        cashNonCounter.${column.productType.productCode } = numberWithCommas(parseFloat("${column.totalCashNonCounter}").toFixed(2));
        creditMemo.${column.productType.productCode } = numberWithCommas(parseFloat("${column.totalCreditMemo}").toFixed(2));
        checkOnUs.${column.productType.productCode } = numberWithCommas(parseFloat("${column.totalCheckOnUs}").toFixed(2));
        checkLocal.${column.productType.productCode } = numberWithCommas(parseFloat("${column.totalCheckLocal}").toFixed(2));
        checkRegional.${column.productType.productCode } = numberWithCommas(parseFloat("${column.totalCheckRegional}").toFixed(2));
        checkNonCounter.${column.productType.productCode } = numberWithCommas(parseFloat("${column.totalCheckNonCounter}").toFixed(2));
        checkOT.${column.productType.productCode } = numberWithCommas(parseFloat("${column.totalCheckOT}").toFixed(2));
        checkInManila.${column.productType.productCode } = numberWithCommas(parseFloat("${column.totalUsCheckInManila}").toFixed(2));
        usCheckOutPh.${column.productType.productCode } = numberWithCommas(parseFloat("${column.totalUsCheckOutPh}").toFixed(2));
        dollarCheque.${column.productType.productCode } = numberWithCommas(parseFloat("${column.totalDollarCheque}").toFixed(2));
        bankOTCCheckPayment.${column.productType.productCode } = numberWithCommas(parseFloat("${column.totalBankOTCCheckPayment}").toFixed(2));
        postalMoneyOrder.${column.productType.productCode } = numberWithCommas(parseFloat("${column.totalPmo}").toFixed(2));

        nonCash.${column.productType.productCode } = numberWithCommas(parseFloat("${column.totalNonCash}").toFixed(2));
        posBpi.${column.productType.productCode } = numberWithCommas(parseFloat("${column.totalPosBpi}").toFixed(2));
        posCtb.${column.productType.productCode } = numberWithCommas(parseFloat("${column.totalPosCtb}").toFixed(2));
        posHsbc.${column.productType.productCode } = numberWithCommas(parseFloat("${column.totalPosHsbc}").toFixed(2));
        posScb.${column.productType.productCode } = numberWithCommas(parseFloat("${column.totalPosScb}").toFixed(2));
        posRcbc.${column.productType.productCode } = numberWithCommas(parseFloat("${column.totalPosRcbc}").toFixed(2));
        posBdo.${column.productType.productCode } = numberWithCommas(parseFloat("${column.totalPosBdo}").toFixed(2));

        // Added for PCO
        mf.${column.productType.productCode } = numberWithCommas(parseFloat("${column.totalMf}").toFixed(2));
        mfInvest.${column.productType.productCode } = numberWithCommas(parseFloat("${column.totalPcoInvest}").toFixed(2));

        var tempTotal = parseFloat("${column.totalCashCounter}") +
                parseFloat("${column.totalCashNonCounter}") +
                parseFloat("${column.totalCreditMemo}") +
                parseFloat("${column.totalCheckOnUs}") +
                parseFloat("${column.totalCheckLocal}") +
                parseFloat("${column.totalCheckRegional}") +
                parseFloat("${column.totalCheckNonCounter}") +
                parseFloat("${column.totalCheckOT}") +
                parseFloat("${column.totalUsCheckInManila}") +
                parseFloat("${column.totalUsCheckOutPh}") +
                parseFloat("${column.totalDollarCheque}") +
                parseFloat("${column.totalBankOTCCheckPayment}") +
                parseFloat("${column.totalPmo}") +
                parseFloat("${column.totalNonCash}") +
                parseFloat("${column.totalPosBpi}") +
                parseFloat("${column.totalPosCtb}") +
                parseFloat("${column.totalPosHsbc}") +
                parseFloat("${column.totalPosScb}") +
                parseFloat("${column.totalPosRcbc}") +
                parseFloat("${column.totalPosBdo}") +
                parseFloat("${column.totalMf}") + // Added for PCO
                parseFloat("${column.totalPcoInvest}"); // Added for PCO

        productTotal.${column.productType.productCode } = numberWithCommas(parseFloat(tempTotal).toFixed(2));

        totalCashCounter = parseFloat(totalCashCounter) + parseFloat("${column.totalCashCounter}");
        totalCashNonCounter = parseFloat(totalCashNonCounter) + parseFloat("${column.totalCashNonCounter}");
        totalCheckOnUs = parseFloat(totalCheckOnUs) + parseFloat("${column.totalCheckOnUs}");
        totalCheckLocal = parseFloat(totalCheckLocal) + parseFloat("${column.totalCheckLocal}");
        totalCheckRegional = parseFloat(totalCheckRegional) + parseFloat("${column.totalCheckRegional}");
        totalCheckNonCounter = parseFloat(totalCheckNonCounter) + parseFloat("${column.totalCheckNonCounter}");
        totalCheckOT = parseFloat(totalCheckOT) + parseFloat("${column.totalCheckOT}");

        totalUsCheckInManila = parseFloat(totalUsCheckInManila) + parseFloat("${column.totalUsCheckInManila}");
        totalUsCheckOutPh = parseFloat(totalUsCheckOutPh) + parseFloat("${column.totalUsCheckOutPh}");
        totalDollarCheque = parseFloat(totalDollarCheque) + parseFloat("${column.totalDollarCheque}");
        totalBankOTCCheckPayment = parseFloat(totalBankOTCCheckPayment) + parseFloat("${column.totalBankOTCCheckPayment}");
        totalPostalMoneyOrder = parseFloat(totalPostalMoneyOrder) + parseFloat("${column.totalPmo}");

        totalNonCash = parseFloat(totalNonCash) + parseFloat("${column.totalNonCash}");
        totalPosBpi = parseFloat(totalPosBpi) + parseFloat("${column.totalPosBpi}");
        totalPosCtb = parseFloat(totalPosCtb) + parseFloat("${column.totalPosCtb}");
        totalPosHsbc = parseFloat(totalPosHsbc) + parseFloat("${column.totalPosHsbc}");
        totalPosScb = parseFloat(totalPosScb) + parseFloat("${column.totalPosScb}");
        totalPosRcbc = parseFloat(totalPosScb) + parseFloat("${column.totalPosRcbc}");
        totalPosBdo = parseFloat(totalPosBdo) + parseFloat("${column.totalPosBdo}");

        // Added for PCO
        totalMf = parseFloat(totalMf) + parseFloat("${column.totalMf}");
        totalPcoInvest = parseFloat(totalPcoInvest) + parseFloat("${column.totalPcoInvest}");

        exemptionsLabel.${column.productType.productCode} = " ";
        exemptions.${column.productType.productCode} =<c:choose>
            <c:when test="${isConfirmed}">
                numberWithCommas(parseFloat("${column.totalReversalAmt}").toFixed(2))
            </c:when>
            <c:otherwise>
                <c:choose>
                    <c:when test="${column.totalReversalAmt == 0.0}">
                    centerCell("<a class=\"blue\" href=\"javascript:showAddEditReversalForm('${column.id}','${column.productType.currency.id}','${column.productType.productName}','add','${column.productType.company.name}')\"><img src=\"${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/images/UndoButton.png\"></a>");
                    </c:when>
                    <c:otherwise>
                        alignRightCell("<a href=\"javascript:showReversalsTable('${column.productType.productCode}','${column.productType.company.name}')\">" + numberWithCommas(parseFloat("${column.totalReversalAmt}").toFixed(2)) + "</a>");
                    </c:otherwise>
                </c:choose>
            </c:otherwise>
        </c:choose>
    </script>
</c:forEach>

<script>
    cashCounter.total = numberWithCommas(parseFloat(totalCashCounter).toFixed(2));
    cashNonCounter.total = numberWithCommas(parseFloat(totalCashNonCounter).toFixed(2));
    checkOnUs.total = numberWithCommas(parseFloat(totalCheckOnUs).toFixed(2));
    checkLocal.total = numberWithCommas(parseFloat(totalCheckLocal).toFixed(2));
    checkRegional.total = numberWithCommas(parseFloat(totalCheckRegional).toFixed(2));
    checkNonCounter.total = numberWithCommas(parseFloat(totalCheckNonCounter).toFixed(2));
    checkOT.total = numberWithCommas(parseFloat(totalCheckOT).toFixed(2));

    checkInManila.total = numberWithCommas(parseFloat(totalUsCheckInManila).toFixed(2));
    usCheckOutPh.total = numberWithCommas(parseFloat(totalUsCheckOutPh).toFixed(2));
    dollarCheque.total = numberWithCommas(parseFloat(totalDollarCheque).toFixed(2));
    bankOTCCheckPayment.total = numberWithCommas(parseFloat(totalBankOTCCheckPayment).toFixed(2));
    postalMoneyOrder.total = numberWithCommas(parseFloat(totalPostalMoneyOrder).toFixed(2));

    nonCash.total = numberWithCommas(parseFloat(totalNonCash).toFixed(2));
    posBpi.total = numberWithCommas(parseFloat(totalPosBpi).toFixed(2));
    posCtb.total = numberWithCommas(parseFloat(totalPosCtb).toFixed(2));
    posHsbc.total = numberWithCommas(parseFloat(totalPosHsbc).toFixed(2));
    posScb.total = numberWithCommas(parseFloat(totalPosScb).toFixed(2));
    posRcbc.total = numberWithCommas(parseFloat(totalPosRcbc).toFixed(2));
    posBdo.total = numberWithCommas(parseFloat(totalPosBdo).toFixed(2));

    // Added for PCO
    mf.total = numberWithCommas(parseFloat(totalMf).toFixed(2));
    mfInvest.total = numberWithCommas(parseFloat(totalPcoInvest).toFixed(2));

    grandTotal = parseFloat(totalCashCounter) +
            parseFloat(totalCashNonCounter) +
            parseFloat(totalCheckOnUs) +
            parseFloat(totalCheckLocal) +
            parseFloat(totalCheckRegional) +
            parseFloat(totalCheckNonCounter) +
            parseFloat(totalUsCheckInManila) +
            parseFloat(totalUsCheckOutPh) +
            parseFloat(totalDollarCheque) +
            parseFloat(totalBankOTCCheckPayment) +
            parseFloat(totalPostalMoneyOrder) +
            parseFloat(totalNonCash) +
            parseFloat(totalPosBpi) +
            parseFloat(totalPosCtb) +
            parseFloat(totalPosHsbc) +
            parseFloat(totalPosScb) +
            parseFloat(totalPosRcbc) +
            parseFloat(totalPosBdo) +
            parseFloat(totalMf) + // Added for PCO
            parseFloat(totalPcoInvest); // Added for PCO

    productTotal.total = numberWithCommas(grandTotal.toFixed(2));

    data.push(blankRow);
    if (checkForValue(cashCounter)) {
        data.push(cashCounter);
    };
    if (checkForValue(cashNonCounter)) {
        data.push(cashNonCounter);
    };
    if (checkForValue(checkOnUs)) {
        data.push(totalCreditMemo);
    };
    if (checkForValue(checkOnUs)) {
        data.push(checkOnUs);
    };
    if (checkForValue(checkLocal)) {
        data.push(checkLocal);
    };
    if (checkForValue(checkRegional)) {
        data.push(checkRegional);
    };
    if (checkForValue(checkNonCounter)) {
        data.push(checkNonCounter);
    };
    if (checkForValue(checkOT)) {
        data.push(checkOT);
    };
    if (checkForValue(checkInManila)) {
        data.push(checkInManila);
    };
    if (checkForValue(usCheckOutPh)) {
        data.push(usCheckOutPh);
    };
    if (checkForValue(dollarCheque)) {
        data.push(dollarCheque);
    };
    if (checkForValue(bankOTCCheckPayment)) {
        data.push(bankOTCCheckPayment);
    };
    if (checkForValue(postalMoneyOrder)) {
        data.push(postalMoneyOrder);
    };
    if (checkForValue(nonCash)) {
        data.push(nonCash);
    };
    if (checkForValue(posBpi)) {
        data.push(posBpi);
    };
    if (checkForValue(posCtb)) {
        data.push(posCtb);
    };
    if (checkForValue(posHsbc)) {
        data.push(posHsbc);
    };
    if (checkForValue(posScb)) {
        data.push(posScb);
    };
    if (checkForValue(posRcbc)) {
        data.push(posRcbc);
    };
    if (checkForValue(posBdo)) {
        data.push(posBdo);
    };
    if (checkForValue(mf)) {
        data.push(mf);
    };
    if (checkForValue(mfInvest)) {
        data.push(mfInvest);
    };
    data.push(blankRow);
    if (checkForValue(productTotal)) {
        data.push(productTotal);
        data.push(blankRow);
        data.push(exemptionsLabel);
        data.push(exemptions);
    }


    layout = currency === "PHP" ? layoutPeso : layoutDollar;

    loadTable(data, '${company}');

    if (${isConfirmed}) {
        $(document).ready(function() {
            var confirmButton = document.getElementById("confirmButton");
            confirmButton.disabled = true;
        }
        );
    }
</script>
