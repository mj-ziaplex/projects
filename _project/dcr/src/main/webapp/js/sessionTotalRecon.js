var labelTradVulTotalCollection = alignLeftCell("&nbsp;&nbsp;&nbsp;Trad/VUL Total Collection(From DCR)");
var labelExemptions = alignLeftCell("&nbsp;&nbsp;&nbsp;Exemptions");
var labelSubTotalA = alignLeftCell("&nbsp;&nbsp;&nbsp;<b>A. Sub Total</b>");
var labelNonPostedTotal = alignLeftCell("&nbsp;&nbsp;&nbsp;Non-Posted Total(From DTR)");
var labelManualPosting = alignLeftCell("&nbsp;&nbsp;&nbsp;Manual Posting");
var labelSubTotalB = alignLeftCell("&nbsp;&nbsp;&nbsp;<b>B. Sub Total</b>");
var labelPostedSuccessfully = alignLeftCell("&nbsp;&nbsp;&nbsp;Posted Successfully(A - B)");
var labelSessionTotalAtEOD = alignLeftCell("&nbsp;&nbsp;&nbsp;<b>Session Total at EOD</b>");
var labelSessionTotalAtEOD2 = alignLeftCell("&nbsp;&nbsp;&nbsp;<b>Session Total</b>");
var labelSessionTotalReconciled = alignLeftCell("&nbsp;&nbsp;&nbsp;<b>SESSION TOTAL RECONCILED</b>");
var labelPesoTotalPostedNonPolicy = alignLeftCell("&nbsp;&nbsp;&nbsp;<b>Total Posted Non Policy</b>");
var labelDollarTotalPostedNonPolicy = alignLeftCell("&nbsp;&nbsp;&nbsp;<b>Total Posted Non Policy</b>");

var sessionTotalReconTableLayout = [ {
	name : "SESSION TOTAL RECON",
	field : "product",//1st column, the leftmost
	width : "75%"
}];

var blankRowSTR = {product: "&nbsp;"};
var sessionTotalReconTableBlankRow = [];

var sessionTotalReconPesoArea = [];
var sessionTotalReconDollarArea = [];

var pesoHeader = {product: alignLeftCell("<b>PESO</b>")};
var pesoTradVulTotalCollection = {product: labelTradVulTotalCollection};
var pesoExemptions = {product: labelExemptions};
var pesoSubTotalA = {product: labelSubTotalA};
var pesoNonPostedTotal = {product: labelNonPostedTotal};
var pesoManualPosting = {product: labelManualPosting};
var pesoSubTotalB = {product: labelSubTotalB};
var pesoPostedSuccessfully = {product: labelPostedSuccessfully};
var pesoSessionTotalAtEOD = {product: labelSessionTotalAtEOD};
var pesoSessionTotalAtEOD2 = {product: labelSessionTotalAtEOD2};
var pesoSessionTotalReconciled = {product: labelSessionTotalReconciled};
var pesoTotalPostedNonPolicy = {product: labelPesoTotalPostedNonPolicy};

var dollarHeader = {product: alignLeftCell("<b>DOLLAR</b>")};
var dollarTradVulTotalCollection = {product: labelTradVulTotalCollection};
var dollarExemptions = {product: labelExemptions};
var dollarSubTotalA = {product: labelSubTotalA};
var dollarNonPostedTotal = {product: labelNonPostedTotal};
var dollarManualPosting = {product: labelManualPosting};
var dollarSubTotalB = {product: labelSubTotalB};
var dollarPostedSuccessfully = {product: labelPostedSuccessfully};
var dollarSessionTotalAtEOD = {product: labelSessionTotalAtEOD};
var dollarSessionTotalAtEOD2 = {product: labelSessionTotalAtEOD2};
var dollarSessionTotalReconciled = {product: labelSessionTotalReconciled};
var dollarTotalPostedNonPolicy = {product: labelDollarTotalPostedNonPolicy}; 


function loadSessionTotalReconTableNodeData(){
	$(document).ready(function(){
	
		new TableContainer({
			divName : "sessionTotalReconTableDiv",
			nodeName : "sessionTotalReconTableNode",
			width : "100%",
			id : "sessionTotalReconTable",
			layout : sessionTotalReconTableLayout,
			tableClass : "borderedTable"
		}).startUp();
		
		var sessionTotalReconTableData = [];
		
		var sessionTotalReconTable = _globalTableContainer["sessionTotalReconTable"];
		
		appendJsons([sessionTotalReconTableData,
		             sessionTotalReconPesoArea,
		             sessionTotalReconTableBlankRow,
		             sessionTotalReconDollarArea]);
		
		sessionTotalReconTable.setData(sessionTotalReconTableData);
		sessionTotalReconTable.refreshTable();
		
		/*var confirmButtonDiv = document.getElementById("confirmButtonDiv");
		
		var confirmButton = document.createElement("input");
		confirmButton.type="submit";
		confirmButton.style.right="0px";
		confirmButton.id="confirmButton";
		confirmButton.value="Confirm";
		confirmButtonDiv.appendChild(confirmButton);*/
	
	});
}