function setCenterCodes(centerCodeData) {
	$(document).ready(function(){
		
		destroyViewTable();
		
		var centerCodeLabelLayout = [{
			name : "Center Codes",
			field : "centerCodeLabel",
			width : "100%"
		}];
		
		new TableContainer({
			divName : "centerCodeDiv",
			nodeName : "centerCodeLabelNode",
			width : "100%",
			id : "centerCodeLabel",
			layout : centerCodeLabelLayout,
			tableClass : "centerCodeLabelBorderedTable"
		}).startUp();
		
		var centerCodeTableLayout = [{
			name : "",
			field : "ccIdChkbox",
			width : "2%"
		},{
			name : "CC ID",
			field : "ccId",
			width : "7%"
		},{
			name : "CC Abbrev Name",
			field : "ccAbbrevName",
			width : "5%"
		},{
			name : "CC Name",
			field : "ccName",
			width : "15%"
		}/*,{
			name : "CC Description",
			field : "ccDesc",
			width : "10%"
		}*/,{
			name : "AutoIdx",
			field : "ccAutoIdx",
			width : "5%"
		},{
			name : "CanEncode",
			field : "ccCanEncode",
			width : "5%"
		},{
			name : "ViewImage",
			field : "ccViewImage",
			width : "5%"
		},{
			name : "ZoomZone",
			field : "ccZoomZone",
			width : "5%"
		},{
			name : "IsIso",
			field : "ccIsIso",
			width : "3%"
		},{
			name : "Created by User",
			field : "ccCreUser",
			width : "10%"
		},{
			name : "Created Time",
			field : "ccCreDate",
			width : "13%"
		},{
			name : "Updated by User",
			field : "ccUpdUser",
			width : "10%"
		},{
			name : "Update Time",
			field : "ccUpdDate",
			width : "13%"
		},{
			name : "Active",
			field : "ccActive",
			width : "5%"
		},{
			name : "Action",
			field : "action",
			width : "4%"
		}];
		
		new TableContainer({
			divName : "centerCodeTableNode",
			nodeName : "centerCodeNode",
			width : "100%",
			id : "centerCodeTable",
			layout : centerCodeTableLayout,
			tableClass : "centerCodeBorderedTable"
		}).startUp();
		
		var centerCodeTable = _globalTableContainer["centerCodeTable"];
		centerCodeTable.setData(centerCodeData);
		centerCodeTable.refreshTable();

		var centerCode = document.getElementById("centerCodeTable");
		
		var options = {
	              optionsForRows : [5,10,20,30,50],
	              rowsPerPage : 20,
	              firstArrow : (new Image()).src="../images/firstBlue.gif",
	              prevArrow : (new Image()).src="../images/prevBlue.gif",
	              lastArrow : (new Image()).src="../images/lastBlue.gif",
	              nextArrow : (new Image()).src="../images/nextBlue.gif",
	              topNav : false
	            }
		$('#centerCodeTable').tablePagination(options);
		sorttable.makeSortable(centerCode);
		autoSort(centerCode);
	});
}

function autoSort(centerCodeTable){
	$(document).ready(function(){
	var headers = centerCodeTable.getElementsByTagName("th");
	for(var h = 0; h < headers.length; h++){
		if(headers[h].innerHTML == "CC ID"){
			headers[h].click();
		}
	}
	resetOddEvenRows(centerCodeTable);
	});
}

function onDoSubmit(action){
	$("#adminCenterCodeForm").attr("action", action);
	$("#adminCenterCodeForm").submit();
}

function destroyViewTable(){
	$("#centerCodeLabelNode").empty();
}

var ccRemoveList = [];

function handleRemoveToggle(cb) {
	if(cb.checked){
		ccRemoveList.push(cb.rowInd);
	}else{
		var ctr = 0;		
		for(cc in ccRemoveList){
				if(ccRemoveList[cc] == cb.rowInd){
					ccRemoveList.splice(ctr, 1);
				}
			ctr++;
		}
		
	}
}

function doDeleteCC(action){
	if(ccRemoveList.length > 0){
		var conf = confirm('Do you want to continue?');
		if(conf){
			document.getElementById("ccIds").value = ccRemoveList;
			$("#adminCCOperation").attr("action", action);
			$("#adminCCOperation").submit();
		}
	}else{
		alert('No center code selected.')
	}
}