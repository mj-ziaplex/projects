/*
**Feb 2016; Cshells Sayan
  - Added new product ADA(MF22) in layout peso
**May 2017; Cshells Sayan
  - Added new product Dollar Starter(MF12) in layout dollar
*/
/*initialize variables */
var blankRow = {product: "&nbsp;"};
var cashCounter = {product: alignLeftCell("Cash - Counter")};
var cashNonCounter = {product: alignLeftCell("Cash - Non Counter")};
var creditMemo = {product: alignLeftCell("Credit Memo")};
var checkOnUs = {product: alignLeftCell("Cheque - On us")};
var checkLocal = {product: alignLeftCell("Cheque - Local")};
var checkRegional = {product: alignLeftCell("Cheque - Regional")};
var checkNonCounter = {product: alignLeftCell("Cheque - Non Counter")};
var checkOT = {product: alignLeftCell("Cheque - OT")};
var checkInManila = {product: alignLeftCell("US Cheque drawn in Manila")};
var usCheckOutPh = {product: alignLeftCell("US Cheque drawn outside PH")};
var dollarCheque = {product: alignLeftCell("Dollar Cheque")};
var bankOTCCheckPayment = {product: alignLeftCell("Bank OTC Check Payment (Dollar)")};
var postalMoneyOrder = {product: alignLeftCell("Postal Money Order")};
var nonCash = {product: alignLeftCell("Noncash")};
var cardTypes = {product: alignLeftCell("Card Types")};
var posBpi = {product: alignLeftCell("&nbsp;&nbsp;&nbsp;POS - BPI")};
var posCtb = {product: alignLeftCell("&nbsp;&nbsp;&nbsp;POS - CTIB")};
var posHsbc = {product: alignLeftCell("&nbsp;&nbsp;&nbsp;POS - HSBC")};
var posScb = {product: alignLeftCell("&nbsp;&nbsp;&nbsp;POS - SCB")};
var posRcbc = {product: alignLeftCell("&nbsp;&nbsp;&nbsp;POS - RCBC")};
var posBdo = {product: alignLeftCell("&nbsp;&nbsp;&nbsp;POS - BDO")};
var productTotal = {product: alignLeftCell("TOTAL")};
var exemptionsLabel = {"product": alignLeftCell("<b>EXEMPTIONS (Manually Inputted)</b>")};
var exemptions = {"product": alignLeftCell("SRF Cancellation")};
// Added for PCO
var mf = {product: alignLeftCell("Prosperity Card Purchase")};
var mfInvest = {product: alignLeftCell("Prosperity Card Invest")};

var totalCashCounter = 0;
var totalCashNonCounter = 0;
var totalCreditMemo = 0;
var totalCheckOnUs = 0;
var totalCheckLocal = 0;
var totalCheckRegional = 0;
var totalCheckNonCounter = 0;
var totalCheckOT = 0;
var totalUsCheckInManila = 0;
var totalUsCheckOutPh = 0;
var totalDollarCheque = 0;
var totalBankOTCCheckPayment = 0;
var totalPostalMoneyOrder = 0;
var totalNonCash = 0;
var totalCardTypes = 0;
var totalPosBpi = 0;
var totalPosCtb = 0;
var totalPosHsbc = 0;
var totalPosScb = 0;
var totalPosRcbc = 0;
var totalPosBdo = 0;
var grandTotal = 0;
// Added for PCO
var totalMf = 0;
var totalPcoInvest = 0;

var data = [];
var layoutPeso = [{name: "SLAMCI - Peso", field: "product", width: "10%"},
    {name: "BOND FUND (MF1)", field: "MF1", width: "10%"},
    {name: "BALANCED FUND (MF2)", field: "MF2", width: "10%"},
    {name: "EQUITY FUND (MF3)", field: "MF3", width: "10%"},
    {name: "MONEY MARKET FUND (MF5)", field: "MF5", width: "10%"},
    {name: "GS FUND (MF7)", field: "MF7", width: "10%"},
    {name: "DYNAMIC FUND (MF8)", field: "MF8", width: "10%"},
    {name: "INDEX FUND (MF9)", field: "MF9", width: "10%"},
    {name: "GROUP FUND (MF20)", field: "MF20", width: "10%"},
    {name: "PROSPERITY CARD (MF)", field: "MF", width: "10%"}, // Added for PCO
    {name: "ADA (MF22)", field: "MF22", width: "10%"}, // Added for ADA
    {name: "TOTAL", field: "total", width: "10%"}];

var layoutDollar = [{name: "SLAMCI -Dollar", field: "product", width: "18%"},
    {name: "DOLLAR ADVANTAGE FUND (MF4)", field: "MF4", width: "12%"},
    {name: "DOLLAR ABUNDANCE FUND (MF6)", field: "MF6", width: "12%"},
    {name: "GROUP FUND (MF21)", field: "MF21", width: "12%"},
    {name: "WELLSPRING FUND (MF10)", field: "MF10", width: "12%"},
    {name: "VOYAGER FUND (MF11)", field: "MF11", width: "12%"},
    {name: "DOLLAR STARTER FUND (MF12)", field: "MF12", width: "12%"}, // Added for SR-WF-16-00016
    {name: "TOTAL", field: "total", width: "14%"}
];

var layout;
function loadTable(data, company) {
    $(document).ready(function() {
        var slamciTableData = [];

        new TableContainer({
            divName: "slamciTableDiv",
            nodeName: "slamciTableNode",
            width: "100%",
            id: "slamciTable",
            layout: layout,
            tableClass: "slamciTable",
            data: []
        }).startUp();

        var table = _globalTableContainer["slamciTable"];

        appendJsons([slamciTableData, data]);

        table.setData(slamciTableData);
        table.refreshTable();

        var confirmButtonDiv = document.getElementById("confirmButtonDiv");

        var confirmButton = document.createElement("input");
        confirmButton.type = "button";
        confirmButton.onclick = function() {
            callConfirmForm(company);
        };
        confirmButton.style.right = "0px";
        confirmButton.id = "confirmButton";
        confirmButton.value = "Confirm";
        confirmButtonDiv.appendChild(confirmButton);
    });
}

var callConfirmForm = function confirmForm(company) {
    document.getElementById("confirmButton").disabled = true;
    var form = document.getElementById("slamcidForm");
    if (company === 'SLAMCIP') {
        form = document.getElementById("slamcipForm");
    }

    var markup = document.documentElement.innerHTML;
    document.getElementById("htmlCode").value = markup;
    form.submit();
};
