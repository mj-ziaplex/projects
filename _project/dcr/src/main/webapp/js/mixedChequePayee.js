

var mixedChequePayeeTableLayout = [ {
	name : "Bank",
	field : "bank",
	width : "16%"
},{
	name : "Check Number",
	field : "checkNumber",
	width : "16%"
},{
	name : "Check Date",
	field : "checkDate",
	width : "16%"
},{
	name : "Check Amount",
	field : "checkAmount",
	width : "16%"
},{
	name : "Type of Check",
	field : "typeOfCheck",
	width : "16%"
},{
	name : "Payee",
	field : "payee",
	width : "15%"
}];

var mcpData = [];

var mcpStore = [];

function setLoadData(mcpData){
	$(document).ready(function(){



new TableContainer({
	divName : "mixedChequePayeeTableDiv",
	nodeName : "mixedChequePayeeTableNode",
	width : "100%",
	id : "mixedChequePayeeTable",
	layout : mixedChequePayeeTableLayout,
	tableClass : "mixedChequePayeeTable",
	data: mcpData
}).startUp();

//var mixedChequePayeeTableData = [];

//var mixedChequePayeeTable = _globalTableContainer["mixedChequePayeeTable"];

//appendJsons([mixedChequePayeeTableData,mixedChequePayeeTableBlankRow,mixedChequePayeeData,mixedChequePayeeTableBlankRow,mixedChequePayeeExceptions]);

//mixedChequePayeeTable.setData(mixedChequePayeeTableData);
//mixedChequePayeeTable.refreshTable();

});
}

function loadCreateNewCheckForm(){
	var  createNewCheckFormLayout =[{
		name: "",
		field: "col1",
		width: "30%"
	},{
		name: "",
		field: "col2",
		width: "70%"
	}];
	
	var createNewCheckFormTableData = [];
	
	var bank = [{ col1: "<label for=\"bankDD\">Bank </label> ",
				  col2: "<select id=\"bankDD\" name=\"bankDD\">" +
				  		"</select>"
	}];
	
	var checkNumber = [{ col1: "<label for=\"checkNumberTB\">Check Number </label> ",
		  col2: "<input type=\"text\" id=\"checkNumberTB\" name=\"checkNumberTB\"/>"
	}];
	
	var checkDate = [{ col1: "<label for=\"searchDatePicker\">Check Date </label> ",
		  col2: "<input type=\"text\" id=\"searchDatePicker\" name=\"searchDatePicker\" readonly=\"true\"/>"
	}];
	
	var checkAmount = [{ col1: "<label for=\"checkAmountTB\">Check Amount </label> ",
		  col2: "<input type=\"text\" id=\"checkAmountTB\" onchange=\"checkForNum(this)\" class=\"numTB\" name=\"checkAmountTB\" value=\"0.00\"/>"
	}];
	
	var payee = [{ col1: "<label for=\"payeeDD\">Payee </label> ",
		  col2: "<select id=\"payeeDD\" onchange=\"setCurrency(this)\" name=\"payeeDD\">" +
		  		"</select>"
	}];
	
	var typeOfCheck = [{ col1: "<label for=\"typeOfCheckDD\">Type Of Check </label> ",
		  col2: "<select id=\"typeOfCheckDD\" name=\"typeOfCheckDD\">" +
		  		"</select>"
	}];
	
	var currency = [{ col1: "<label for=\"currencyDD\">Currency </label> ",
		  col2: "<select id=\"currencyDD\" name=\"currencyDD\">" +
	  		"</select>"
	}];
	
	var breakdown = [{ col1: "<label>Breakdown</label> ",
		  col2: ""
	}];
	
	var slamci = [{ col1: "<label for=\"slamciTB\">&nbsp;&nbsp;&nbsp;SLAMCI</label> ",
		  col2: "<input type=\"text\" onchange=\"checkForNum(this)\" class=\"numTB\" id=\"slamciTB\" name=\"slamciTB\" value=\"0.00\"/>"
	}];
	
	var slocpi = [{ col1: "<label for=\"slocpiTB\">&nbsp;&nbsp;&nbsp;SLOCPI</label> ",
		  col2: "<input type=\"text\" onchange=\"checkForNum(this)\" class=\"numTB\" id=\"slocpiTB\" name=\"slocpiTB\" value=\"0.00\"/>"
	}];
	
	var slgfi = [{ col1: "<label for=\"slgfiTB\">&nbsp;&nbsp;&nbsp;SLGFI</label> ",
		  col2: "<input type=\"text\" onchange=\"checkForNum(this)\" class=\"numTB\" id=\"slgfiTB\" name=\"slgfiTB\" value=\"0.00\"/>"
	}];
	
	var slfpi = [{ col1: "<label for=\"slfpiTB\">&nbsp;&nbsp;&nbsp;SLFPI</label> ",
		  col2: "<input type=\"text\" onchange=\"checkForNum(this)\" class=\"numTB\" id=\"slfpiTB\" name=\"slfpiTB\" value=\"0.00\"/>"
	}];
	
	var other = [{ col1: "<label for=\"otherTB\">&nbsp;&nbsp;&nbsp;OTHER</label> ",
		  col2: "<input type=\"text\" onchange=\"checkForNum(this)\" class=\"numTB\" id=\"otherTB\" name=\"otherTB\" value=\"0.00\"/>"
	}];
	
	var actionButtons =  [{ col1: "<input type=\"hidden\" name=\"mcpId\" id=\"mcpId\" value=\" \"/><input type=\"button\" value=\"Save\" class=\"actionButtons\" name=\"saveButton\" id=\"saveButton\" onclick=\"submitForm()\"/> ",
		  col2: "<input type=\"button\" value=\"Delete\" class=\"actionButtons\" name=\"deleteButton\" id=\"deleteButton\" onclick=\"deleteCheck()\"/> "
	}];
	
	appendJsons([createNewCheckFormTableData,bank,checkNumber,checkDate,checkAmount,typeOfCheck,payee,currency,breakdown,slocpi,slfpi,actionButtons]);
	
	
	new TableContainer({
		divName : "createNewCheckFormDiv",
		nodeName : "createNewCheckFormNode",
		width : "100%",
		id : "createNewCheckTable",
		layout : createNewCheckFormLayout,
		tableClass : "createNewCheckFormTable",
		data: createNewCheckFormTableData
	}).startUp();
	
	var createNewCheckTable = document.getElementById("createNewCheckTable");
	
	
}

var showCreateNewCheckPopUp = function(val) {
	document.getElementById("checkForm").reset();
	var saveButton = document.getElementById("saveButton");
	var deleteButton = document.getElementById("deleteButton");
	if(val == 1){
		saveButton.value = "Add";
		deleteButton.style.visibility = "hidden";
	} else if (val == 2){
		saveButton.value = "Update";
		deleteButton.style.visibility = "visible";
	}
	$(function() {
		$( "#createNewCheckDialogDiv" ).dialog({
			autoOpen: true,
			height: 950,
			width: 400,
			modal: true,
			closeOnEscape: true,
			draggable: false,
			resizable: false,
			dialogClass: "createNewCheckFormClass",
			title: "Mixed Cheque Payee Creator"
		}).dialog('widget').position({ my: 'center', at: 'center', of: $(document.getElementById("mainTemplateBodyId")) });
	});
	
};

function setCurrency(val){
	var currencyDD = document.getElementById("currencyDD");
	if(val.value == "1"){
		currencyDD.value = 1;
		currencyDD.disabled = true;
	} else if(val.value == "2"){
		currencyDD.value = 2;
		currencyDD.disabled = true;
	} else {
		currencyDD.disabled = false;
	}
		
}

function validateForm(){
	var checkNumberTB = document.getElementById("checkNumberTB");
	var searchDatePicker = document.getElementById("searchDatePicker");
	var checkAmountTB = document.getElementById("checkAmountTB");
	//var slamciTB = document.getElementById("slamciTB");
	var slocpiTB = document.getElementById("slocpiTB");
	//var slgfiTB = document.getElementById("slgfiTB");
	var slfpiTB = document.getElementById("slfpiTB");
	//var otherTB = document.getElementById("otherTB");
	
//	if(!validateIsANumber(slamciTB) || slamciTB.value.length < 1 || slamciTB.value.indexOf(" ") != -1){
//		return false;
//	}
	if(!validateIsANumber(slocpiTB) || slocpiTB.value.length < 1 || slocpiTB.value.indexOf(" ") != -1){
		return false;
	}
//	if(!validateIsANumber(slgfiTB) || slgfiTB.value.length < 1 || slgfiTB.value.indexOf(" ") != -1){
//		return false;
//	}
	if(!validateIsANumber(slfpiTB) || slfpiTB.value.length < 1 || slfpiTB.value.indexOf(" ") != -1){
		return false;
	}
//	if(!validateIsANumber(otherTB) || otherTB.value.length < 1 || otherTB.value.indexOf(" ") != -1){
//		return false;
//	}
	if(!validateIsANumber(checkAmountTB) || checkAmountTB.value.length < 1 || checkAmountTB.value.indexOf(" ") != -1){
		return false;
	}
	if(searchDatePicker.value.length < 1 || searchDatePicker.value.indexOf(" ") != -1){
		return false;
	}
	if(checkNumberTB.value.length < 1 || checkNumberTB.value.indexOf(" ") != -1){
		return false;
	}
	
	var breakdownSum = //parseFloat(slamciTB.value.replace(/,/g,"")) + 
					   parseFloat(slocpiTB.value.replace(/,/g,"")) +
					 //  parseFloat(slgfiTB.value.replace(/,/g,"")) + 
					   parseFloat(slfpiTB.value.replace(/,/g,"")) ;
					 //  parseFloat(otherTB.value.replace(/,/g,""));
	
	if(breakdownSum != parseFloat(checkAmountTB.value.replace(/,/g,""))){
		alert("Breakdown does not match Check Amount");
		return false;
	}
	return true;
}

function checkForNum(val){
	if(!validateIsANumber(val) || val.value.length < 1 || val.value.indexOf(" ") != -1){
		alert("Not a number");
		val.value = "0.00";
		return false;
	} else{
		val.value = numberWithCommas(parseFloat(val.value).toFixed(2));
	}
}

function submitForm(){
	var checkForm = document.getElementById("checkForm");
	if(validateForm()){
		var currencyDD = document.getElementById("currencyDD");
		currencyDD.disabled = false;
		var saveButton = document.getElementById("saveButton");
		var checkForm = document.getElementById("checkForm");
		var formActionVal = checkForm.action;
		var methodName = formActionVal.substring(formActionVal.lastIndexOf("/")+1, formActionVal.search(".html"));
		if(saveButton.value.localeCompare("Add") == 0){
			checkForm.action = formActionVal.replace(methodName,"createNewCheck");
		} else if(saveButton.value.localeCompare("Update") == 0){
			checkForm.action = formActionVal.replace(methodName,"updateCheck");
		}
		checkForm.submit();
	} else{
		alert("Input error");
	}
}

function getBankName(val){
	
	var bankDD = document.getElementById("bankDD");
	var bankList = bankDD.getElementsByTagName("option");
	for(var a = 0; a < bankList.length; a++){
		if(bankList[a].value == val){
			return bankList[a].innerHTML;
		}
	}
	
	return val;
	
}

function clearFormFields(){
	var checkNumberTB = document.getElementById("checkNumberTB");
	var searchDatePicker = document.getElementById("searchDatePicker");
	var checkAmountTB = document.getElementById("checkAmountTB");
	//var slamciTB = document.getElementById("slamciTB");
	var slocpiTB = document.getElementById("slocpiTB");
	//var slgfiTB = document.getElementById("slgfiTB");
	var slfpiTB = document.getElementById("slfpiTB");
//	var otherTB = document.getElementById("otherTB");
	
	checkNumberTB.value = "";
	searchDatePicker.value = "";
	checkAmountTB.value = "";
	//slamciTB.value = "";
	slocpiTB.value = "";
//	slgfiTB.value = "";
	slfpiTB.value = "";
//	otherTB.value = "";
}

function setForm(val){
	for(var a=0; a < mcpStore.length; a++){
		if(val == mcpStore[a].id){
			var checkNumberTB = document.getElementById("checkNumberTB");
			var searchDatePicker = document.getElementById("searchDatePicker");
			var checkAmountTB = document.getElementById("checkAmountTB");
			//var slamciTB = document.getElementById("slamciTB");
			var slocpiTB = document.getElementById("slocpiTB");
			//var slgfiTB = document.getElementById("slgfiTB");
			var slfpiTB = document.getElementById("slfpiTB");
			//var otherTB = document.getElementById("otherTB");
			var bankDD = document.getElementById("bankDD");
			var typeOfCheckDD = document.getElementById("typeOfCheckDD");
			var payeeDD = document.getElementById("payeeDD");
			var currencyDD = document.getElementById("currencyDD");
			var mcpID = document.getElementById("mcpId");			
			
			mcpID.value = mcpStore[a].id;
			checkNumberTB.value = mcpStore[a].checkNumber;
			searchDatePicker.value = mcpStore[a].checkDate;
			checkAmountTB.value = numberWithCommas(parseFloat(mcpStore[a].checkAmount).toFixed(2));
		//	slamciTB.value = numberWithCommas(parseFloat(mcpStore[a].slamci).toFixed(2));
			slocpiTB.value = numberWithCommas(parseFloat(mcpStore[a].slocpi).toFixed(2));
		//	slgfiTB.value = numberWithCommas(parseFloat(mcpStore[a].slgfi).toFixed(2));
			slfpiTB.value = numberWithCommas(parseFloat(mcpStore[a].slfpi).toFixed(2));
		//	otherTB.value = numberWithCommas(parseFloat(mcpStore[a].others).toFixed(2));
			bankDD.value = mcpStore[a].bankId;
			payeeDD.value = mcpStore[a].payee;
			currencyDD.value = mcpStore[a].currency;
			typeOfCheckDD.value = mcpStore[a].typeOfCheck;
			if(payeeDD.value == "1" || payeeDD.value == "2"){
				currencyDD.disabled = true;
			} else {
				currencyDD.disabled = false;
			}
		}
	}
}

function deleteCheck(){
	var checkForm = document.getElementById("checkForm");
	var formActionVal = checkForm.action;
	var methodName = formActionVal.substring(formActionVal.lastIndexOf("/")+1, formActionVal.search(".html"));
	checkForm.action = formActionVal.replace(methodName,"deleteCheck");
	checkForm.submit();
}
