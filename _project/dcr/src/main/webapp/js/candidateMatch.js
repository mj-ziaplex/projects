function tab(tab) {
document.getElementById('tab1').style.display = 'none';
document.getElementById('tab2').style.display = 'none';
document.getElementById('li_tab1').setAttribute("class", "");
document.getElementById('li_tab2').setAttribute("class", "");
document.getElementById(tab).style.display = 'block';
document.getElementById('li_'+tab).setAttribute("class", "active");
}

function clearDate(){
	var searchDatePicker = document.getElementById("cmSearchDatePicker");
	var dcrDate = document.getElementById("searchDcrDateStr");
	dcrDate.value = "";
	searchDatePicker.value = "";
}

function changeTab(val,showDivName,hideDivName){	
	var tabTable = document.getElementById("tabTable");
	var tabs = tabTable.getElementsByTagName("td");
	for(var t=0; t < tabs.length; t++){
		if(tabs[t].id != val.id){
			tabs[t].style.color = "white";
			tabs[t].style.background = "grey";
		} else{
			tabs[t].style.color = "black";
			tabs[t].style.background = "#F8CF00";
		}
	}
	var showDiv = document.getElementById(showDivName);
	showDiv.style.display = "inline";
	var hideDiv = document.getElementById(hideDivName);
	hideDiv.style.display = "none";
}

var searchRequirementsLayout = [{
	name: "Customer Center Name",
	field: "ccName",
	width: "20%"
},{
	name: "DCR Date",
	field: "dcrDate",
	width: "20%"
},{
	name: "File Name",
	field: "filename",
	width: "20%"
},{
	name: "Upload Date",
	field: "uploadDate",
	width: "20%"
},{
	name: "<div class='alignCenter'>Match</div>",
	field: "match",
	width: "20%"
}];

function generateSearchResultsTable(){
	$(document).ready( function(){

		new TableContainer({
			divName : "searchRequirementsDiv",
			nodeName : "searchRequirementsNode",
			width : "100%",
			id : "searchRequirementsTable",
			layout : searchRequirementsLayout,
			tableClass: "cmTableClass",
			data: searchResultList
		}).startUp();
		
		var searchRequirementsTable = document.getElementById("searchRequirementsTable");
		
		var options = {
	              optionsForRows : [5,10,20,30,50],
	              rowsPerPage : 10,
	              firstArrow : (new Image()).src="../images/firstBlue.gif",
	              prevArrow : (new Image()).src="../images/prevBlue.gif",
	              lastArrow : (new Image()).src="../images/lastBlue.gif",
	              nextArrow : (new Image()).src="../images/nextBlue.gif",
	              topNav : false
	            }
		sorttable.makeSortable(searchRequirementsTable);
		$('#searchRequirementsTable').tablePagination(options);
	});
		
}

var attachedRequirementsLayout = [{
	name: "Attachment",
	field: "filename",
	width: "50%"
},{
	name: "Upload Date",
	field: "uploadDate",
	width: "30%"
},{
	name: "<div class='alignCenter'>Unmatch</div>",
	field: "unmatch",
	width: "20%"
}];

function generateRequiredAttachmentTable(){
	$(document).ready( function(){

		new TableContainer({
			divName : "attachedRequirementsDiv",
			nodeName : "attachedRequirementsNode",
			width : "100%",
			id : "attachedRequirementsTable",
			layout : attachedRequirementsLayout,
			tableClass: "cmTableClass",
			data: attachments
		}).startUp();
		
		var attachedRequirementsTable = document.getElementById("attachedRequirementsTable");
		
		var options = {
	              optionsForRows : [5,10,20,30,50],
	              rowsPerPage : 10,
	              firstArrow : (new Image()).src="../images/firstBlue.gif",
	              prevArrow : (new Image()).src="../images/prevBlue.gif",
	              lastArrow : (new Image()).src="../images/lastBlue.gif",
	              nextArrow : (new Image()).src="../images/nextBlue.gif",
	              topNav : false
	            }
		sorttable.makeSortable(attachedRequirementsTable);
		$('#attachedRequirementsTable').tablePagination(options);
	});
		
}

function submitForm(){
	var cmForm = document.getElementById("cmSearchForm");
	if(validateForm()){
		cmForm.submit();
	}
}

function consolidateUnmatchList(val){
	
	if(val.checked){
		var tempUnmatch = {};
		tempUnmatch.recordId = val.recordId;
		tempUnmatch.recordLocation = val.recordLocation;
		tempUnmatch.filenetId = val.filenetId;
		tempUnmatch.ccId = val.ccId;
		tempUnmatch.dcrDate = val.dcrDate;
	
		unMatchList.push(tempUnmatch);
	} else{
		for(u in unMatchList){
			if(unMatchList[u].recordId == val.recordId && unMatchList[u].recordLocation == val[u].recordLocation && unMatchList[u].filenetId == val.filenetId){
				delete unMatchList[u];
			}
		}
	}
}

function consolidateMatchList(val){
	
	if(val.checked){
		var tempMatch = {};
		tempMatch.recordId = val.recordId;
		tempMatch.recordLocation = val.recordLocation;
		tempMatch.filenetId = val.filenetId;
		tempMatch.ccId = val.ccId;
		tempMatch.dcrDate = val.dcrDate;
		tempMatch.description = val.description;
		tempMatch.fileType = val.fileType;
		tempMatch.filesize = val.filesize;
	
		matchList.push(tempMatch);
	} else{
		for(u in matchList){
			if(matchList[u].recordId == val.recordId && matchList[u].recordLocation == val[u].recordLocation && matchList[u].filenetId == val.filenetId){
				delete unMatchList[u];
			}
		}
	}
}

function submitUnmatchForm(){
	var unmatchForm = document.getElementById("unmatchForm");
	for(um in unMatchList){
		var hiddenRecordId = document.createElement("input");
		hiddenRecordId.type = 'hidden';
		hiddenRecordId.name = 'unMatchList['+um+'].recordId';
		hiddenRecordId.value = unMatchList[um].recordId;
		
		var hiddenRecordLocation = document.createElement("input");
		hiddenRecordLocation.type = 'hidden';
		hiddenRecordLocation.name = 'unMatchList['+um+'].recordLocation';
		hiddenRecordLocation.value = unMatchList[um].recordLocation;
		
		var hiddenFilenetId = document.createElement("input");
		hiddenFilenetId.type = 'hidden';
		hiddenFilenetId.name = 'unMatchList['+um+'].filenetId';
		hiddenFilenetId.value = unMatchList[um].filenetId;
		
		var hiddenCCId = document.createElement("input");
		hiddenCCId.type = 'hidden';
		hiddenCCId.name = 'unMatchList['+um+'].ccId';
		hiddenCCId.value = unMatchList[um].ccId;
		
		var hiddenDcrDate = document.createElement("input");
		hiddenDcrDate.type = 'hidden';
		hiddenDcrDate.name = 'unMatchList['+um+'].dcrDate';
		hiddenDcrDate.value = unMatchList[um].dcrDate;
		
		unmatchForm.appendChild(hiddenRecordId);
		unmatchForm.appendChild(hiddenRecordLocation);
		unmatchForm.appendChild(hiddenFilenetId);
		unmatchForm.appendChild(hiddenCCId);
		unmatchForm.appendChild(hiddenDcrDate);
	}
	unmatchForm.submit();
}

function submitMatchForm(){
	var matchForm = document.getElementById("matchForm");
	for(um in matchList){
		var hiddenRecordId = document.createElement("input");
		hiddenRecordId.type = 'hidden';
		hiddenRecordId.name = 'matchList['+um+'].recordId';
		hiddenRecordId.value = matchList[um].recordId;
		
		var hiddenRecordLocation = document.createElement("input");
		hiddenRecordLocation.type = 'hidden';
		hiddenRecordLocation.name = 'matchList['+um+'].recordLocation';
		hiddenRecordLocation.value = matchList[um].recordLocation;
		
		var hiddenFilenetId = document.createElement("input");
		hiddenFilenetId.type = 'hidden';
		hiddenFilenetId.name = 'matchList['+um+'].filenetId';
		hiddenFilenetId.value = matchList[um].filenetId;
		
		var hiddenCCId = document.createElement("input");
		hiddenCCId.type = 'hidden';
		hiddenCCId.name = 'matchList['+um+'].ccId';
		hiddenCCId.value = matchList[um].ccId;
		
		var hiddenDcrDate = document.createElement("input");
		hiddenDcrDate.type = 'hidden';
		hiddenDcrDate.name = 'matchList['+um+'].dcrDate';
		hiddenDcrDate.value = matchList[um].dcrDate;
		
		var hiddenDescription = document.createElement("input");
		hiddenDescription.type = 'hidden';
		hiddenDescription.name = 'matchList['+um+'].description';
		hiddenDescription.value = matchList[um].description;
		
		var hiddenFileType = document.createElement("input");
		hiddenFileType.type = 'hidden';
		hiddenFileType.name = 'matchList['+um+'].fileType';
		hiddenFileType.value = matchList[um].fileType;
		
		var hiddenFileSize = document.createElement("input");
		hiddenFileSize.type = 'hidden';
		hiddenFileSize.name = 'matchList['+um+'].filesize';
		hiddenFileSize.value = matchList[um].filesize;
		
		matchForm.appendChild(hiddenRecordId);
		matchForm.appendChild(hiddenRecordLocation);
		matchForm.appendChild(hiddenFilenetId);
		matchForm.appendChild(hiddenCCId);
		matchForm.appendChild(hiddenDcrDate);
		matchForm.appendChild(hiddenDescription);
		matchForm.appendChild(hiddenFileType);
		matchForm.appendChild(hiddenFileSize);
	}
	matchForm.submit();
}
	
function findMatch(){
	if(validateSearchForm()){
		var cmSearchForm = document.getElementById("cmSearchForm");
		cmSearchForm.submit();
	}		
}

function setDcrDateStr(val){
	var dcrDateStr = document.getElementById("searchDcrDateStr");
	dcrDateStr.value = val;
}

function validateSearchForm(){
	var dcrDateStr = document.getElementById("searchDcrDateStr");
	var ccId = document.getElementById("searchCcId");
	if(dcrDateStr.value.length == 0){
		alert("Please enter a date");
		return false;
	} else if(ccId.selectedIndex == -1){
		alert("Please select a customer center");
		return false;
	}
	
	return true;
}

function getCCName(ccId){
	$(document).ready( function(){
		var searchCcId = document.getElementById("searchCcId");
		var ccList = searchCcId.getElementsByTagName("option");
		if(ccList.value == ccId){
			return ccList.innerHTML;
		}

		return "";
	});
}

var unMatchList = [];
var matchList = [];
var searchResultList = [];
var attachments = [];