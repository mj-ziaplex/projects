// (c) Copyright IBM Corp. 2002, 2008.  All Rights Reserved.
//
// $Workfile:   Wcm.js  $
// $Revision: 1.1 $
// $Date: 2012/10/24 12:18:36 $
//
//  Standard client-side utility JavaScript functions for Workplace...

// this variable will be true if the browser is MSIE
var isIE = document.all && document.getElementById;
var helpWindow = null;

// Hides all <select> elements in IE.  This is so they don't show above the context menu
function hideSelectElements()
{
    if (isIE)
    {
        var selectElements = document.getElementsByTagName("select");
        for (var i = 0; i < selectElements.length; i++)
            selectElements[i].style.visibility = "hidden";
    }
}

// Show all <select> elements (hidden by hideSelectElements() function)
function showSelectElements()
{
    if (isIE)
    {
        var selectElements = document.getElementsByTagName("select");
        for (var i = 0; i < selectElements.length; i++)
            selectElements[i].style.visibility = "visible";
    }
}

// Function:
//    noBoxesChecked
//
// Description:
//    Use this function to put up an alert of no boxes are checked.
//    Else go the url.
//
// Parameters:
//    errorMessage - The error message to be put onto the alert box
//    url - the url to redirect to
//    formName    - The name of the form
//    checkboxName -  The name of the checkboxes
//
function noBoxesChecked( errorMessage, url, formName, checkboxName )
{
    var anychecked = false;
    o = eval("document.forms." + formName);
    var checkBoxes = document.getElementsByName( checkboxName );
    for (var i = 0; i < checkBoxes.length; i++)
    {
        var checkbox = checkBoxes[i];
        if (checkbox.checked)
        {
            anychecked=true;
        }
    }
    if ( anychecked )
    {
        o.action=url;
        o.submit();
    }
    else
    {
        alert( errorMessage );
    }
}

// Function:
//    checkAll
//
// Description:
//    Use this function check/uncheck all checkboxes.
//
// Parameters:
//
function checkAll()
{
   var allInputs = document.getElementsByTagName('input');
   var allBox, input, i = allInputs.length;

   while (i--)
   {
       input = allInputs[i];
       if ('checkbox' == input.type && input.name == 'allBox')
       {
           allBox = input;
           break;
       }
   }

   i = allInputs.length;

   while (i--)
   {
       input = allInputs[i];
       if ('checkbox' == input.type && input.name != 'allBox')
       {
           input.checked = allBox.checked;
       }
   }
}

// Function:
//    checkAllInForm
//
// Description:
//    Use this function to check/uncheck all checkboxes.
//
// Parameters:
// Parameters:
//    formName    - The name of the form (required).
//    checkboxName - Checkbox to change values all on/off
//    checkboxValue - Checkbox input to test value of
//
function checkAllInForm(formName, checkboxName, checkboxValue)
{
    var frm = document.getElementById(formName);
    var allbox = document.getElementById(checkboxValue);
    for (var i = 0; i < frm.elements.length; i++)
    {
        var e = frm.elements[i];
        if (e.name == checkboxName && e.type == 'checkbox')
            e.checked = allbox.checked;
    }
}

// Function:
//    submitForm
//
// Description:
//    Use this function to submit a client-side framework form.
//
// Parameters:
//    formName    - The name of the form (required).
//    eventTarget - Specify the target module.  If you want to use the
//                  default eventTarget, specify null (optional).
//    eventName   - Specify the event name.  If you want to use the
//                  default eventName, specify null (optional).
function submitForm(formName, eventTarget, eventName, param1, param2, param3)
{
   o = eval("document.forms." + formName);

   if ( o != null )
   {
      if ( eventTarget != null )
         o.eventTarget.value = eventTarget;

      if ( eventName != null )
         o.eventName.value = eventName;

      if ( param1 != null )
         o.eventParam1.value = param1;

      if ( param2 != null )
         o.eventParam2.value = param2;

      if ( param3 != null )
         o.eventParam3.value = param3;

      o.submit();
   }
}

// Function:
//    createWindow
//
// Description:
//    Low-level function for creating a popup window.
//
// Parameters:
//    pageURL    - the URL to open
//    title      - the unique page window name
//    toolbar    - yes/no whether or not to show the toolbar
//    addressbar - yes/no whether or not to show the URL address bar
//    resizable  - yes/no whether or not to make the window resizable
//    scrollbars - yes/no whether or not to display scollbars
//    width      - window width
//    height     - window height
//    align      - left/center/right, default is center
//    valign     - top/middle/bottom, default is middle
//    focus      - yes/no whether or not to attempt to pop the window on top
//
function createWindow(pageURL, title, toolbar, addressbar, resizable, scrollbars, width, height, align, valign, focus)
{
    return createWindowEx(pageURL, title, toolbar, addressbar, resizable, scrollbars, "no", width, height, align, valign, focus);
}

// Description:
//    Low-level function for creating a popup window.
//
// Parameters:
//    pageURL    - the URL to open
//    title      - the unique page window name
//    toolbar    - yes/no whether or not to show the toolbar
//    addressbar - yes/no whether or not to show the URL address bar
//    resizable  - yes/no whether or not to make the window resizable
//    scrollbars - yes/no whether or not to display scollbars
//    status     - yes/no whether or not to display the status bar
//    width      - window width
//    height     - window height
//    align      - left/center/right, default is center
//    valign     - top/middle/bottom, default is middle
//    focus      - yes/no whether or not to attempt to pop the window on top
//
function createWindowEx(pageURL, title, toolbar, addressbar, resizable, scrollbars, status, width, height, align, valign, focus)
{
    var decoration = 30;
    var x = (screen.width - width) / 2;
    var y = (screen.height - decoration - height) / 2;

    if ( align == 'left' )
        x = 0;
    else if ( align == 'right' )
        x = screen.width - (decoration/2) - width;

    if ( valign == 'top' )
        y = 0;
    else if ( valign == 'bottom' )
        y = screen.height - decoration - height;

    var popupFeatures = "dependent=no,toolbar=" + toolbar + ",directories=no,location=" + addressbar + ",menubar=no,resizable=" + resizable + ",scrollbars=" + scrollbars + ",status=" + status + ",width=" + width + ",height=" + height + ",top=" + y + ",left=" + x;

    var w = window.open(pageURL, title, popupFeatures);

    if ( focus == 'yes' ) w.focus();

    return(w);
}

// Function:
//    createNewWindow
//
// Description:
//    Low-level function for creating a popup window.  The new window refererence won't be returned.
//
// Parameters:
//    pageURL    - the URL to open
//    title      - the unique page window name
//    toolbar    - yes/no whether or not to show the toolbar
//    addressbar - yes/no whether or not to show the URL address bar
//    resizable  - yes/no whether or not to make the window resizable
//    scrollbars - yes/no whether or not to display scollbars
//    width      - window width
//    height     - window height
//    align      - left/center/right, default is center
//    valign     - top/middle/bottom, default is middle
//    focus      - yes/no whether or not to attempt to pop the window on top
//
function createNewWindow(pageURL, title, toolbar, addressbar, resizable, scrollbars, width, height, align, valign, focus)
{
    var w = createWindowEx(pageURL, title, toolbar, addressbar, resizable, scrollbars, "yes", width, height, align, valign, focus);
}

// Function:
//    createHelpWindow
//
// Description:
//    Function for creating a popup window for on-line help.
//
// Parameters:
//    pageURL    - the URL to open
//    title      - the unique page window name
//    toolbar    - yes/no whether or not to show the toolbar
//    addressbar - yes/no whether or not to show the URL address bar
//    resizable  - yes/no whether or not to make the window resizable
//    width      - window width
//    height     - window height
//    align      - left/center/right, default is center
//    valign     - top/middle/bottom, default is middle
//    focus      - yes/no whether or not to attempt to pop the window on top
function createHelpWindow(pageURL, title, toolbar, addressbar, resizable, scrollbars, width, height, align, valign, focus)
{
    var decoration = 30;
    var x = (screen.width - width) / 2;
    var y = (screen.height - decoration - height) / 2;

    if ( align == 'left' )
        x = 0;
    else if ( align == 'right' )
        x = screen.width - (decoration/2) - width;

    if ( valign == 'top' )
        y = 0;
    else if ( valign == 'bottom' )
        y = screen.height - decoration - height;

    var popupFeatures = "dependent=no,toolbar=" + toolbar + ",directories=no,location=" + addressbar + ",menubar=yes,resizable=" + resizable + ",scrollbars=" + scrollbars + ",status=no,width=" + width + ",height=" + height + ",top=" + y + ",left=" + x;

    helpWindow = window.open(pageURL, title, popupFeatures);
    if ( focus == 'yes' ) 
        helpWindow.focus();

    return(helpWindow);
}

// Function:
//    onBodyLoadInitializeFileTracking
//
// Description:
//    handles the <body> onLoad Event for initializing
//    Workplace File Tracking (using the Active-X control)
//    conditionally invokes the intialization logic only if
//    the javascript function and the "ControlNotLoaded" Url
//    have BOTH been defined
//
// Parameters:
function onBodyLoadInitializeFileTracking()
{
    if ( ( typeof initializePage != "undefined" ) && ( typeof gControlNotLoadedUrl != "undefined" ) )
    {
        // Only Invoke this method if BOTH the javascript funciton and the ControlNotLoaded URL have
        // been defined, otherwise it is a no-op...
        initializePage();
    }
}


// Function:
//    openHelpWindow
//
// Description:
//    Open the help popup
//
// Parameters:
//    pageURL   - the URL to open
function openHelpWindow(pageURL)
{
    var width  = (screen.width * 3) / 4;
    var height = (screen.height * 3) / 4;
    createHelpWindow(pageURL, 'WcmHelp', 'yes', 'yes', 'yes', 'yes', width, height, 'right', 'top', 'yes');
}

function hideAllMenus()
{
    if (typeof hidemenu != "undefined")
        hidemenu();
    if (typeof hidetreemodule != "undefined")
        hidetreemodule();
}
function write(tag)
 {
    document.write(tag);

 }
document.onclick = hideAllMenus;
