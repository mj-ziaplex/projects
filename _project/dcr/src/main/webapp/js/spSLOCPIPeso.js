var slocpiPesoColumns = [{
	name : "",
	field : "product",
	width : "14%"
},{
	name : "",
	field : "cashier",
	width : "21%"
},{
	name : "TRAD/VUL (PESO)",
	field : "tradVul",
	width : "8%"
},{
	name : "DS",
	field : "dsTV",
	width : "7%"
},{
	name : "VDS",
	field : "vdsTV",
	width : "7%"
},{
	name : "GROUP LIFE PESO",
	field : "groupLife",
	width : "7%"
},{
	name : "DS",
	field : "dsGL",
	width : "7%"
},{
	name : "VDS",
	field : "vdsGL",
	width : "7%"
},{
	name : "CCM NOTES",
	field : "ccmNotes",
	width : "7%"
},{
	// Modified for MR-WF-16-00036 - Change PPA notes to CCQA notes
	name : "CCQA NOTES",
	field : "ppaNotes",
	width : "7%"
},{
	name : "Findings",
	field : "findings",
	width : "8%"
}	
];

var slocpiPesoColumnsNH = [{
	name : "",
	field : "product",
	width : "14%"
},{
	name : "",
	field : "cashier",
	width : "21%"
},{
	name : "",
	field : "tradVul",
	width : "8%"
},{
	name : "",
	field : "dsTV",
	width : "7%"
},{
	name : "",
	field : "vdsTV",
	width : "7%"
},{
	name : "",
	field : "groupLife",
	width : "7%"
},{
	name : "",
	field : "dsGL",
	width : "7%"
},{
	name : "",
	field : "vdsGL",
	width : "7%"
},{
	name : "",
	field : "ccmNotes",
	width : "7%"
},{
	name : "",
	field : "ppaNotes",
	width : "7%"
},{
	name : "",
	field : "findings",
	width : "8%"
}	
];

function generateSLOCPIPesoSection(slocpiPesoTableDivId,slocpiPesoDivDetails,slocpiPesoSummaryDivDetails){
	$(document).ready( function(){
			
	mergeSLOCPIPesoCashierProductTypes();
	
	var pesoCashHeader = [{product: "<div class='alignLeft' cdTarget='slocpiPesoCash' onclick='collapseDivision(this)'>PESO CASH</div>",cashier:"", tradVul: "",dsTV: "",vdsTV:"",groupLife:"",dsGL:"",vdsGL:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var cashNonCounterData = generateSLOCPIPesoSectionData("totalCashNonCounter","slocpiPesoCash");
	
	var cncTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > Non Counter</div>", tradVul: "<div class='numFormat'>"+cashNonCounterData.cashierTotals.tv+"</div>",dsTV: "",vdsTV:"",groupLife:"<div class='numFormat'>"+cashNonCounterData.cashierTotals.gl+"</div>",dsGL:"",vdsGL:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var cashCounterData = generateSLOCPIPesoSectionData("totalCashCounter","slocpiPesoCash");
	
	var ccTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > Counter</div>", tradVul: "<div class='numFormat'>"+cashCounterData.cashierTotals.tv+"</div>",dsTV: "",vdsTV:"",groupLife:"<div class='numFormat'>"+cashCounterData.cashierTotals.gl+"</div>",dsGL:"",vdsGL:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	
	var cashCounterEpsBpiData = generateSLOCPIPesoSectionData("totalCashEpsBpi","slocpiPesoCash");
	
	var ccTotalEpsBpi = [{product: "",cashier:"<div class='alignRight'>Sub Total > Counter EPS - BPI</div>", tradVul: "<div class='numFormat'>"+cashCounterEpsBpiData.cashierTotals.tv+"</div>",dsTV: "",vdsTV:"",groupLife:"<div class='numFormat'>"+cashCounterEpsBpiData.cashierTotals.gl+"</div>",dsGL:"",vdsGL:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	
	var pesoCashTotals = getSLOCPIPesoAreaTotal([cashNonCounterData,cashCounterData,cashCounterEpsBpiData]);
	
	var areaCashTotal = [{product: "<div class='totalRow'>&nbsp;</div>",cashier:"<div class='totalRow'><div class='alignRight'>TOTAL CASH</div></div>", tradVul: "<div class='totalRow'><div class='numFormat'>"+pesoCashTotals.tv+"</div></div>",dsTV: "<div class='totalRow'>&nbsp;</div>",vdsTV:"<div class='totalRow'>&nbsp;</div>",groupLife:"<div class='totalRow'><div class='numFormat'>"+pesoCashTotals.groupLife+"</div></div>",dsGL:"<div class='totalRow'>&nbsp;</div>",vdsGL:"<div class='totalRow'>&nbsp;</div>",ccmNotes:"<div class='totalRow'>&nbsp;</div>",ppaNotes:"<div class='totalRow'>&nbsp;</div>",findings:"<div class='totalRow'>&nbsp;</div>"}];
	
	var pesoCheckHeader = [{product: "<div class='alignLeft' cdTarget='slocpiPesoCheck' onclick='collapseDivision(this)'>PESO CHECK</div>",cashier:"", tradVul: "",dsTV: "",vdsTV:"",groupLife:"",dsGL:"",vdsGL:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var checkOnUsData = generateSLOCPIPesoSectionData("totalCheckOnUs","slocpiPesoCheck");
	
	var chouTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > On Us</div>", tradVul: "<div class='numFormat'>"+checkOnUsData.cashierTotals.tv+"</div>",dsTV: "",vdsTV:"",groupLife:"<div class='numFormat'>"+checkOnUsData.cashierTotals.gl+"</div>",dsGL:"",vdsGL:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var checkLocalData = generateSLOCPIPesoSectionData("totalCheckLocal","slocpiPesoCheck");
	
	var chlTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > Local</div>", tradVul: "<div class='numFormat'>"+checkLocalData.cashierTotals.tv+"</div>",dsTV: "",vdsTV:"",groupLife:"<div class='numFormat'>"+checkLocalData.cashierTotals.gl+"</div>",dsGL:"",vdsGL:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var checkRegionalData = generateSLOCPIPesoSectionData("totalCheckRegional","slocpiPesoCheck");
	
	var chrTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > Regional</div>", tradVul: "<div class='numFormat'>"+checkRegionalData.cashierTotals.tv+"</div>",dsTV: "",vdsTV:"",groupLife:"<div class='numFormat'>"+checkRegionalData.cashierTotals.gl+"</div>",dsGL:"",vdsGL:"",ccmNotes:"",ppaNotes:"",findings:""}];

	var checkNonCounterData = generateSLOCPIPesoSectionData("totalCheckNonCounter","slocpiPesoCheck");
	
	var chncTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > Non Counter</div>", tradVul: "<div class='numFormat'>"+checkNonCounterData.cashierTotals.tv+"</div>",dsTV: "",vdsTV:"",groupLife:"<div class='numFormat'>"+checkNonCounterData.cashierTotals.gl+"</div>",dsGL:"",vdsGL:"",ccmNotes:"",ppaNotes:"",findings:""}];

	var checkOTData = generateSLOCPIPesoSectionData("totalCheckOT","slocpiPesoCheck");
	
	var chotTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > OT</div>", tradVul: "<div class='numFormat'>"+checkOTData.cashierTotals.tv+"</div>",dsTV: "",vdsTV:"",groupLife:"<div class='numFormat'>"+checkOTData.cashierTotals.gl+"</div>",dsGL:"",vdsGL:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	
	var postalMoneyOrderData = generateSLOCPIPesoSectionData("totalPmo","slocpiPesoCheck");
	
	var postalMoneyOrderTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > Postal Money Order</div>", tradVul: "<div class='numFormat'>"+postalMoneyOrderData.cashierTotals.tv+"</div>",dsTV: "",vdsTV:"",groupLife:"<div class='numFormat'>"+postalMoneyOrderData.cashierTotals.gl+"</div>",dsGL:"",vdsGL:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	
	//var creditMemoData = generateSLOCPIPesoSectionData("totalCreditMemo");
	
	//var cmTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > Local</div>", tradVul: "<div class='numFormat'>"+creditMemoData.cashierTotals.tv+"</div>",dsTV: "",vdsTV:"",groupLife:"<div class='numFormat'>"+creditMemoData.cashierTotals.gl+"</div>",dsGL:"",vdsGL:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	//var bankOTCCheckData = generateSLOCPIPesoSectionData("totalBankOTCCheckPayment");
	
	//var bocTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > Bank OTC Check Payment </div>", tradVul: "<div class='numFormat'>"+bankOTCCheckData.cashierTotals.tv+"</div>",dsTV: "",vdsTV:"",groupLife:"<div class='numFormat'>"+bankOTCCheckData.cashierTotals.gl+"</div>",dsGL:"",vdsGL:"",ccmNotes:"",ppaNotes:"",findings:""}];

	var pesoCheckTotals = getSLOCPIPesoAreaTotal([checkOnUsData,checkLocalData,checkRegionalData,checkNonCounterData,postalMoneyOrderData,checkOTData]);
	
	var areaCheckTotal = [{product: "<div class='totalRow'>&nbsp;</div>",cashier:"<div class='totalRow'><div class='alignRight'>TOTAL CHECK</div></div>", tradVul: "<div class='totalRow'><div class='numFormat'>"+pesoCheckTotals.tv+"</div></div>",dsTV: "<div class='totalRow'>&nbsp;</div>",vdsTV:"<div class='totalRow'>&nbsp;</div>",groupLife:"<div class='totalRow'><div class='numFormat'>"+pesoCheckTotals.groupLife+"</div></div>",dsGL:"<div class='totalRow'>&nbsp;</div>",vdsGL:"<div class='totalRow'>&nbsp;</div>",ccmNotes:"<div class='totalRow'>&nbsp;</div>",ppaNotes:"<div class='totalRow'>&nbsp;</div>",findings:"<div class='totalRow'>&nbsp;</div>"}];
	
	var nonCashHeader = [{product: "<div class='alignLeft' cdTarget='slocpiPesoNonCash' onclick='collapseDivision(this)'>NON CASH</div>",cashier:"", tradVul: "",dsTV: "",vdsTV:"",groupLife:"",dsGL:"",vdsGL:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var nonCashData = generateSLOCPIPesoSectionData("totalNonCash","slocpiPesoNonCash");
	
	var nonCashTotal = [{product: "<div class='totalRow'>&nbsp;</div>",cashier:"<div class='totalRow'><div class='alignRight'>TOTAL NON CASH</div></div>", tradVul: "<div class='totalRow'><div class='numFormat'>"+nonCashData.cashierTotals.tv+"</div></div>",dsTV: "<div class='totalRow'>&nbsp;</div>",vdsTV:"<div class='totalRow'>&nbsp;</div>",groupLife:"<div class='totalRow'><div class='numFormat'>"+nonCashData.cashierTotals.gl+"</div></div>",dsGL:"<div class='totalRow'>&nbsp;</div>",vdsGL:"<div class='totalRow'>&nbsp;</div>",ccmNotes:"<div class='totalRow'>&nbsp;</div>",ppaNotes:"<div class='totalRow'>&nbsp;</div>",findings:"<div class='totalRow'>&nbsp;</div>"}];
	
	var pesoCardHeader = [{product: "<div class='alignLeft' cdTarget='slocpiPesoCard' onclick='collapseDivision(this)'>PESO CARD</div>",cashier:"", tradVul: "",dsTV: "",vdsTV:"",groupLife:"",dsGL:"",vdsGL:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var posBPIData = generateSLOCPIPesoSectionData("totalPosBpi","slocpiPesoCard");
	
	var posBPITotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > POS - BPI</div>", tradVul: "<div class='numFormat'>"+posBPIData.cashierTotals.tv+"</div>",dsTV: "",vdsTV:"",groupLife:"<div class='numFormat'>"+posBPIData.cashierTotals.gl+"</div>",dsGL:"",vdsGL:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var posCTIBData = generateSLOCPIPesoSectionData("totalPosCtb","slocpiPesoCard");
	
	var posCTIBTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > POS - CITIBANK</div>", tradVul: "<div class='numFormat'>"+posCTIBData.cashierTotals.tv+"</div>",dsTV: "",vdsTV:"",groupLife:"<div class='numFormat'>"+posCTIBData.cashierTotals.gl+"</div>",dsGL:"",vdsGL:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var posHSBCData = generateSLOCPIPesoSectionData("totalPosHsbc","slocpiPesoCard");
	
	var posHSBCTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > POS - HSBC</div>", tradVul: "<div class='numFormat'>"+posHSBCData.cashierTotals.tv+"</div>",dsTV: "",vdsTV:"",groupLife:"<div class='numFormat'>"+posHSBCData.cashierTotals.gl+"</div>",dsGL:"",vdsGL:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var posSCBData = generateSLOCPIPesoSectionData("totalPosScb","slocpiPesoCard");
	
	var posSCBTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > POS - SCB</div>", tradVul: "<div class='numFormat'>"+posSCBData.cashierTotals.tv+"</div>",dsTV: "",vdsTV:"",groupLife:"<div class='numFormat'>"+posSCBData.cashierTotals.gl+"</div>",dsGL:"",vdsGL:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
//	var posRCBCData = generateSLOCPIPesoSectionData("totalPosRcbc");
//	
//	var posRCBCTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > POS - RCBC</div>", tradVul: "<div class='numFormat'>"+posRCBCData.cashierTotals.tv+"</div>",dsTV: "",vdsTV:"",groupLife:"<div class='numFormat'>"+posRCBCData.cashierTotals.gl+"</div>",dsGL:"",vdsGL:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var posBDOData = generateSLOCPIPesoSectionData("totalPosBdo","slocpiPesoCard");
	
	var posBDOTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > POS - BDO</div>", tradVul: "<div class='numFormat'>"+posBDOData.cashierTotals.tv+"</div>",dsTV: "",vdsTV:"",groupLife:"<div class='numFormat'>"+posBDOData.cashierTotals.gl+"</div>",dsGL:"",vdsGL:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var mdsBPIData = generateSLOCPIPesoSectionData("totalMdsBpi","slocpiPesoCard");
	
	var mdsBPITotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > MDS - BPI</div>", tradVul: "<div class='numFormat'>"+mdsBPIData.cashierTotals.tv+"</div>",dsTV: "",vdsTV:"",groupLife:"<div class='numFormat'>"+mdsBPIData.cashierTotals.gl+"</div>",dsGL:"",vdsGL:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var mdsBDOData = generateSLOCPIPesoSectionData("totalMdsBdo","slocpiPesoCard");
	
	var mdsBDOTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > MDS - BDO</div>", tradVul: "<div class='numFormat'>"+mdsBDOData.cashierTotals.tv+"</div>",dsTV: "",vdsTV:"",groupLife:"<div class='numFormat'>"+mdsBDOData.cashierTotals.gl+"</div>",dsGL:"",vdsGL:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
//	var mdsRCBCData = generateSLOCPIPesoSectionData("totalMdsRcbc");
//	
//	var mdsRCBCTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > MDS - RCBC</div>", tradVul: "<div class='numFormat'>"+mdsRCBCData.cashierTotals.tv+"</div>",dsTV: "",vdsTV:"",groupLife:"<div class='numFormat'>"+mdsRCBCData.cashierTotals.gl+"</div>",dsGL:"",vdsGL:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var mdsSBCData = generateSLOCPIPesoSectionData("totalMdsSbc","slocpiPesoCard");
	
	var mdsSBCTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > MDS - SCB</div>", tradVul: "<div class='numFormat'>"+mdsSBCData.cashierTotals.tv+"</div>",dsTV: "",vdsTV:"",groupLife:"<div class='numFormat'>"+mdsSBCData.cashierTotals.gl+"</div>",dsGL:"",vdsGL:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var mdsCTBData = generateSLOCPIPesoSectionData("totalMdsCtb","slocpiPesoCard");
	
	var mdsCTBTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > MDS - CITIBANK</div>", tradVul: "<div class='numFormat'>"+mdsCTBData.cashierTotals.tv+"</div>",dsTV: "",vdsTV:"",groupLife:"<div class='numFormat'>"+mdsCTBData.cashierTotals.gl+"</div>",dsGL:"",vdsGL:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var mdsHSBCData = generateSLOCPIPesoSectionData("totalMdsHsbc","slocpiPesoCard");
	
	var mdsHSBCTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > MDS - HSBC</div>", tradVul: "<div class='numFormat'>"+mdsHSBCData.cashierTotals.tv+"</div>",dsTV: "",vdsTV:"",groupLife:"<div class='numFormat'>"+mdsHSBCData.cashierTotals.gl+"</div>",dsGL:"",vdsGL:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var autoCTBData = generateSLOCPIPesoSectionData("totalAutoCtb","slocpiPesoCard");
	
	var autoCTBTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > CITIBANK (AUTOCHARGE)</div>", tradVul: "<div class='numFormat'>"+autoCTBData.cashierTotals.tv+"</div>",dsTV: "",vdsTV:"",groupLife:"<div class='numFormat'>"+autoCTBData.cashierTotals.gl+"</div>",dsGL:"",vdsGL:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var autoSCBData = generateSLOCPIPesoSectionData("totalAutoScb","slocpiPesoCard");
	
	var autoSCBTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > SCB (AUTOCHARGE)</div>", tradVul: "<div class='numFormat'>"+autoSCBData.cashierTotals.tv+"</div>",dsTV: "",vdsTV:"",groupLife:"<div class='numFormat'>"+autoSCBData.cashierTotals.gl+"</div>",dsGL:"",vdsGL:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
//	var autoRCBCData = generateSLOCPIPesoSectionData("totalAutoRcbc");
//	
//	var autoRCBCTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > RCBC - AUTOCHARGE</div>", tradVul: "<div class='numFormat'>"+autoRCBCData.cashierTotals.tv+"</div>",dsTV: "",vdsTV:"",groupLife:"<div class='numFormat'>"+autoRCBCData.cashierTotals.gl+"</div>",dsGL:"",vdsGL:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var sunlinkOLData = generateSLOCPIPesoSectionData("totalSunlinkOnline","slocpiPesoCard");
	
	var sunlinkOLTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > SUNLINK ONLINE</div>", tradVul: "<div class='numFormat'>"+sunlinkOLData.cashierTotals.tv+"</div>",dsTV: "",vdsTV:"",groupLife:"<div class='numFormat'>"+sunlinkOLData.cashierTotals.gl+"</div>",dsGL:"",vdsGL:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var sunlinkHSBCPData = generateSLOCPIPesoSectionData("totalSunlinkHsbcPeso","slocpiPesoCard");
	
	var sunlinkHSBCPTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > SUNLINK HSBC PESO</div>", tradVul: "<div class='numFormat'>"+sunlinkHSBCPData.cashierTotals.tv+"</div>",dsTV: "",vdsTV:"",groupLife:"<div class='numFormat'>"+sunlinkHSBCPData.cashierTotals.gl+"</div>",dsGL:"",vdsGL:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var epsBPIData = generateSLOCPIPesoSectionData("totalEpsBpi","slocpiPesoCard");
	
	var epsBPITotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > EPS - BPI</div>", tradVul: "<div class='numFormat'>"+epsBPIData.cashierTotals.tv+"</div>",dsTV: "",vdsTV:"",groupLife:"<div class='numFormat'>"+epsBPIData.cashierTotals.gl+"</div>",dsGL:"",vdsGL:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var areaCardTotals = getSLOCPIPesoAreaTotal([posBPIData,posCTIBData,posHSBCData,posSCBData,posBDOData,mdsBPIData,mdsBDOData,mdsSBCData,mdsCTBData,mdsHSBCData,autoCTBData,autoSCBData,sunlinkOLData,sunlinkHSBCPData,epsBPIData]);	
	
	var pesoCardTotal = [{product: "<div class='totalRow'>&nbsp;</div>",cashier:"<div class='totalRow'><div class='alignRight'>TOTAL PESO CARD</div></div>", tradVul: "<div class='totalRow'><div class='numFormat'>"+areaCardTotals.tv+"</div></div>",dsTV: "<div class='totalRow'>&nbsp;</div>",vdsTV:"<div class='totalRow'>&nbsp;</div>",groupLife:"<div class='totalRow'><div class='numFormat'>"+areaCardTotals.groupLife+"</div></div>",dsGL:"<div class='totalRow'>&nbsp;</div>",vdsGL:"<div class='totalRow'>&nbsp;</div>",ccmNotes:"<div class='totalRow'>&nbsp;</div>",ppaNotes:"<div class='totalRow'>&nbsp;</div>",findings:"<div class='totalRow'>&nbsp;</div>"}];
	
	var nonPostedFromDTRHeader = [{product: "<div class='alignLeft' cdTarget='slocpiPesoNonPosted' onclick='collapseDivision(this)'>NON POSTED (from DTR)</div>",cashier:"", tradVul: "",dsTV: "",vdsTV:"",groupLife:"",dsGL:"",vdsGL:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var nbIndividualData = generateSLOCPIPesoSectionData("totalNewBusinessIndividual","slocpiPesoNonPosted");
	
	var nbIndividualTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > NB Individual</div>", tradVul: "<div class='numFormat'>"+nbIndividualData.cashierTotals.tv+"</div>",dsTV: "",vdsTV:"",groupLife:"<div class='numFormat'>"+nbIndividualData.cashierTotals.gl+"</div>",dsGL:"",vdsGL:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var nbVULData = generateSLOCPIPesoSectionData("totalNewBusinessVariable","slocpiPesoNonPosted");
	
	var nbVULTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > NB VUL</div>", tradVul: "<div class='numFormat'>"+nbVULData.cashierTotals.tv+"</div>",dsTV: "",vdsTV:"",groupLife:"<div class='numFormat'>"+nbVULData.cashierTotals.gl+"</div>",dsGL:"",vdsGL:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var individualRenewalData = generateSLOCPIPesoSectionData("totalIndividualRenewal","slocpiPesoNonPosted");
	
	var individualRenewalTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > Individual Renewal</div>", tradVul: "<div class='numFormat'>"+individualRenewalData.cashierTotals.tv+"</div>",dsTV: "",vdsTV:"",groupLife:"<div class='numFormat'>"+individualRenewalData.cashierTotals.gl+"</div>",dsGL:"",vdsGL:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var vulRenewalData = generateSLOCPIPesoSectionData("totalVariableRenewal","slocpiPesoNonPosted");
	
	var vulRenewalTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > VUL Renewal</div>", tradVul: "<div class='numFormat'>"+vulRenewalData.cashierTotals.tv+"</div>",dsTV: "",vdsTV:"",groupLife:"<div class='numFormat'>"+vulRenewalData.cashierTotals.gl+"</div>",dsGL:"",vdsGL:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var nonCashDTRData = generateSLOCPIPesoSectionData("totalNonCashFromDTR","slocpiPesoFMP");
	
	var nonCashDTRTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > Non Cash</div>", tradVul: "<div class='numFormat'>"+nonCashDTRData.cashierTotals.tv+"</div>",dsTV: "",vdsTV:"",groupLife:"<div class='numFormat'>"+nonCashDTRData.cashierTotals.gl+"</div>",dsGL:"",vdsGL:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var areaNonPostedTotals = getSLOCPIPesoAreaTotal([nbIndividualData,nbVULData,individualRenewalData,vulRenewalData,nonCashDTRData]);
	
	var nonPostedTotal = [{product: "<div class='totalRow'>&nbsp;</div>",cashier:"<div class='totalRow'><div class='alignRight'>TOTAL NON-POSTED</div></div>", tradVul: "<div class='totalRow'><div class='numFormat'>"+areaNonPostedTotals.tv+"</div></div>",dsTV: "<div class='totalRow'>&nbsp;</div>",vdsTV:"<div class='totalRow'>&nbsp;</div>",groupLife:"<div class='totalRow'><div class='numFormat'>"+areaNonPostedTotals.groupLife+"</div></div>",dsGL:"<div class='totalRow'>&nbsp;</div>",vdsGL:"<div class='totalRow'>&nbsp;</div>",ccmNotes:"<div class='totalRow'>&nbsp;</div>",ppaNotes:"<div class='totalRow'>&nbsp;</div>",findings:"<div class='totalRow'>&nbsp;</div>"}];
	
	var forManualPostingHeader = [{product: "<div class='alignLeft' cdTarget='slocpiPesoFMP' onclick='collapseDivision(this)'>FOR MANUAL POSTING</div>",cashier:"", tradVul: "",dsTV: "",vdsTV:"",groupLife:"",dsGL:"",vdsGL:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var nonPolicyFMPData = generateSLOCPIPesoSectionData("totalNonPolicy","slocpiPesoFMP");
	
	var nonPolicyFMPTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > Non Policy</div>", tradVul: "<div class='numFormat'>"+nonPolicyFMPData.cashierTotals.tv+"</div>",dsTV: "",vdsTV:"",groupLife:"<div class='numFormat'>"+nonPolicyFMPData.cashierTotals.gl+"</div>",dsGL:"",vdsGL:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var worksiteNotPostedFMPData = generateSLOCPIPesoSectionData("totalWorksiteNonPosted","slocpiPesoFMP");
	
	var worksiteNotPostedFMPTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > Worksite</div>", tradVul: "<div class='numFormat'>"+worksiteNotPostedFMPData.cashierTotals.tv+"</div>",dsTV: "",vdsTV:"",groupLife:"<div class='numFormat'>"+worksiteNotPostedFMPData.cashierTotals.gl+"</div>",dsGL:"",vdsGL:"",ccmNotes:"",ppaNotes:"",findings:""}];	
	
	var areaForManualPostingTotals = getSLOCPIPesoAreaTotal([nonPolicyFMPData,worksiteNotPostedFMPData,nonCashDTRData]);
	
	var forManualPostingotal = [{product: "<div class='totalRow'>&nbsp;</div>",cashier:"<div class='totalRow'><div class='alignRight'>TOTAL FOR MANUAL POSTING</div></div>", tradVul: "<div class='totalRow'><div class='numFormat'>"+areaForManualPostingTotals.tv+"</div></div>",dsTV: "<div class='totalRow'>&nbsp;</div>",vdsTV:"<div class='totalRow'>&nbsp;</div>",groupLife:"<div class='totalRow'><div class='numFormat'>"+areaForManualPostingTotals.groupLife+"</div></div>",dsGL:"<div class='totalRow'>&nbsp;</div>",vdsGL:"<div class='totalRow'>&nbsp;</div>",ccmNotes:"<div class='totalRow'>&nbsp;</div>",ppaNotes:"<div class='totalRow'>&nbsp;</div>",findings:"<div class='totalRow'>&nbsp;</div>"}];
	
	var exemptionHeader = [{product: "<div class='alignLeft' cdTarget='slocpiPesoExemptions' onclick='collapseDivision(this)'>EXEMPTIONS</div>",cashier:"", tradVul: "",dsTV: "",vdsTV:"",groupLife:"",dsGL:"",vdsGL:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var exemptionData = generateSLOCPIPesoSectionData("reversalProduct","slocpiPesoExemptions");
	
	var exemptionTotal = [{product: "<div class='totalRow'>&nbsp;</div>",cashier:"<div class='totalRow'><div class='alignRight'>TOTAL EXEMPTIONS</div></div>", tradVul: "<div class='totalRow'><div class='numFormat'>"+exemptionData.cashierTotals.tv+"</div></div>",dsTV: "<div class='totalRow'>&nbsp;</div>",vdsTV:"<div class='totalRow'>&nbsp;</div>",groupLife:"<div class='totalRow'><div class='numFormat'>"+exemptionData.cashierTotals.gl+"</div></div>",dsGL:"<div class='totalRow'>&nbsp;</div>",vdsGL:"<div class='totalRow'>&nbsp;</div>",ccmNotes:"<div class='totalRow'>&nbsp;</div>",ppaNotes:"<div class='totalRow'>&nbsp;</div>",findings:"<div class='totalRow'>&nbsp;</div>"}];
	
	var sessionTotalHeader = [{product: "<div class='alignLeft' cdTarget='slocpiPesoST' onclick='collapseDivision(this)'>SESSION TOTALS</div>",cashier:"", tradVul: "",dsTV: "",vdsTV:"",groupLife:"",dsGL:"",vdsGL:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var sessionTotalData = generateSLOCPIPesoSectionData("sessionTotal","slocpiPesoST");
	
	var sessionTotalTotal = [{product: "<div class='totalRow'>&nbsp;</div>",cashier:"<div class='totalRow'><div class='alignRight'>SESSION TOTAL</div></div>", tradVul: "<div class='totalRow'><div class='numFormat'>"+sessionTotalData.cashierTotals.tv+"</div></div>",dsTV: "<div class='totalRow'>&nbsp;</div>",vdsTV:"<div class='totalRow'>&nbsp;</div>",groupLife:"<div class='totalRow'><div class='numFormat'>"+sessionTotalData.cashierTotals.gl+"</div></div>",dsGL:"<div class='totalRow'>&nbsp;</div>",vdsGL:"<div class='totalRow'>&nbsp;</div>",ccmNotes:"<div class='totalRow'>&nbsp;</div>",ppaNotes:"<div class='totalRow'>&nbsp;</div>",findings:"<div class='totalRow'>&nbsp;</div>"}];
	
	var totalSLOCPIPCashiersCollection = getSLOCPIPesoAreaTotal([nonCashData,cashNonCounterData,cashCounterData,checkOnUsData,checkLocalData,checkRegionalData,checkNonCounterData,posBPIData,posCTIBData,posHSBCData,posSCBData,posBDOData,mdsBPIData,mdsBDOData,mdsSBCData,mdsCTBData,mdsHSBCData,autoCTBData,autoSCBData,sunlinkOLData,sunlinkHSBCPData,epsBPIData,cashCounterEpsBpiData,checkOTData,postalMoneyOrderData]);
	
	var cashierTotalHeader = [{product: "<div class='alignLeft' cdTarget='slamciPesoGTPC' onclick='collapseDivision(this)' >Grand Total Per Cashier</div>",cashier:"", tradVul: "",dsTV: "",vdsTV:"",groupLife:"",dsGL:"",vdsGL:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var cashierTotal = getSLOCPIPGrandTotalPerCashier("slamciPesoGTPC");
	
	var cashierTotalRow = [{product: "<div class='totalRow'>&nbsp;</div>",cashier:"<div class='totalRow'><div class='alignRight'>TOTAL COLLECTION</div></div>", tradVul: "<div class='totalRow'><div class='numFormat'>"+numberWithCommas(totalSLOCPIPCashiersCollection.tv)+"</div></div>",dsTV: "<div class='totalRow'>&nbsp;</div>",vdsTV:"<div class='totalRow'>&nbsp;</div>",groupLife:"<div class='totalRow'><div class='numFormat'>"+numberWithCommas(totalSLOCPIPCashiersCollection.groupLife)+"</div></div>",dsGL:"<div class='totalRow'>&nbsp;</div>",vdsGL:"<div class='totalRow'>&nbsp;</div>",ccmNotes:"<div class='totalRow'>&nbsp;</div>",ppaNotes:"<div class='totalRow'>&nbsp;</div>",findings:"<div class='totalRow'>&nbsp;</div>"}];
	
	var slocpiPesoSummaryData = [{product: "",cashier:"CONSOLIDATED GRAND TOTAL", tradVul: "<div class='numFormat'>"+numberWithCommas(slocpiPesoConsolidatedTotals.tv)+"</div>",dsTV: "",vdsTV:"",groupLife:"<div class='numFormat'>"+numberWithCommas(slocpiPesoConsolidatedTotals.gl)+"</div>",dsGL:"",vdsGL:"",ccmNotes:"",ppaNotes:"",findings:""},
	                        {product: "",cashier:"TOTAL CASHIERS COLLECTION", tradVul:"<div class='numFormat'>"+numberWithCommas(totalSLOCPIPCashiersCollection.tv)+"</div>",dsTV: "",vdsTV:"",groupLife:"<div class='numFormat'>"+numberWithCommas(totalSLOCPIPCashiersCollection.groupLife)+"</div>",dsGL:"",vdsGL:"",ccmNotes:"",ppaNotes:"",findings:""},
	                        {product: "",cashier:"", tradVul: compareTotalsImg(totalSLOCPIPCashiersCollection.tv,numberWithCommas(slocpiPesoConsolidatedTotals.tv)),dsTV: "",vdsTV:"",groupLife:compareTotalsImg(totalSLOCPIPCashiersCollection.groupLife,numberWithCommas(slocpiPesoConsolidatedTotals.gl)),dsGL:"",vdsGL:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	new TableContainer({
		divName : slocpiPesoSummaryDivDetails.divName,
		nodeName : slocpiPesoSummaryDivDetails.nodeName,
		width : "1000px",
		id : slocpiPesoSummaryDivDetails.tableId,
		layout : slocpiPesoColumns,
		tableClass: "balancingToolSummary",
		data: slocpiPesoSummaryData
	}).startUp();
	
	var slocpiPesoSectionData = [];
	
	appendJsons([slocpiPesoSectionData,pesoCashHeader,cashNonCounterData.prodTotals,cncTotal,cashCounterData.prodTotals,ccTotal,cashCounterEpsBpiData.prodTotals,ccTotalEpsBpi,areaCashTotal,pesoCheckHeader,checkOnUsData.prodTotals,chouTotal,checkLocalData.prodTotals,chlTotal,checkRegionalData.prodTotals,chrTotal,checkOTData.prodTotals,chotTotal,postalMoneyOrderData.prodTotals,postalMoneyOrderTotal,checkNonCounterData.prodTotals,chncTotal,areaCheckTotal,nonCashHeader,nonCashData.prodTotals,nonCashTotal,pesoCardHeader,posBPIData.prodTotals,posBPITotal,posCTIBData.prodTotals,posCTIBTotal,posHSBCData.prodTotals,posHSBCTotal,posSCBData.prodTotals,posSCBTotal,posBDOData.prodTotals,posBDOTotal,mdsBPIData.prodTotals,mdsBPITotal,mdsBDOData.prodTotals,mdsBDOTotal,mdsSBCData.prodTotals,mdsSBCTotal,mdsCTBData.prodTotals,mdsCTBTotal,mdsHSBCData.prodTotals,mdsHSBCTotal,autoCTBData.prodTotals,autoCTBTotal,autoSCBData.prodTotals,autoSCBTotal,sunlinkOLData.prodTotals,sunlinkOLTotal,sunlinkHSBCPData.prodTotals,sunlinkHSBCPTotal,epsBPIData.prodTotals,epsBPITotal,pesoCardTotal,cashierTotalHeader,cashierTotal,cashierTotalRow,nonPostedFromDTRHeader,nbIndividualData.prodTotals,nbIndividualTotal,nbVULData.prodTotals,nbVULTotal,individualRenewalData.prodTotals,individualRenewalTotal,vulRenewalData.prodTotals,vulRenewalTotal,nonPostedTotal,forManualPostingHeader,nonCashDTRData.prodTotals,nonCashDTRTotal,nonPolicyFMPData.prodTotals,nonPolicyFMPTotal,worksiteNotPostedFMPData.prodTotals,worksiteNotPostedFMPTotal,forManualPostingotal,exemptionHeader,exemptionData.prodTotals,exemptionTotal,sessionTotalHeader,sessionTotalData.prodTotals,sessionTotalTotal,generateSnapshotSection(slocpiSnapshot)]);
	
	new TableContainer({
		divName : slocpiPesoDivDetails.divName,
		nodeName : slocpiPesoDivDetails.nodeName,
		width : "1000px",
		id : slocpiPesoDivDetails.tableId,
		layout : slocpiPesoColumnsNH,
		tableClass: "balancingToolSummary",
		data: slocpiPesoSectionData
	}).startUp();
	
	var slocpiPesoTableDiv = document.getElementById(slocpiPesoTableDivId);

	slocpiPesoTableDiv.style.display = "none";
	
	});
}


var slocpiPesoSecData = [];

function mergeSLOCPIPesoCashierProductTypes(){
	var cashierRep = [];
	for(c in slocpiPesoData){
		//using jQuery.inArray instead of Array.indexOf because it is not supported in IE8 and below. thanks IE.
		if(jQuery.inArray(slocpiPesoData[c].cashierId+"~"+slocpiPesoData[c].product,cashierRep) == -1){
			var pushedData = "";
			pushedData = pushedData.concat(slocpiPesoData[c].cashierId);
			pushedData = pushedData.concat("~");
			pushedData = pushedData.concat(slocpiPesoData[c].product);
			cashierRep.push(pushedData);
		}
	}
	
	for(var a = 0; a < cashierRep.length; a++){
		slocpiPesoSecData.push(mergeSLOCPIPesoCashierData(cashierRep[a]));
	}
		
}

function getSLOCPIPesoAreaTotal(area){
	var tots = {};
	var tv = 0;
	var groupLife = 0;
	for(a in area){
		tv = tv + parseFloat(area[a].cashierTotals.tv.replace(/,/g,""));
		groupLife = groupLife + parseFloat(area[a].cashierTotals.gl.replace(/,/g,""));
	}
	tots.tv = numberWithCommas(parseFloat(tv).toFixed(2));
	tots.groupLife = numberWithCommas(parseFloat(groupLife).toFixed(2));
	return tots;
}

function generateSLOCPIPesoSectionData(section,cdTarget){
	var sectionObj = {};
	var sectionCashierTotals = {};
	var tv = 0;
	var gl = 0;
	var  returnCNC = [];
	var firstRowFlag = true;
	for(a in slocpiPesoSecData){
		var tempCNC = {};
		if(slocpiPesoSecData[a].product == section){
			var isCashDepo=false;
			var isCheckDepo=false;
			if(firstRowFlag){
				switch(section){
					case "totalCashNonCounter": tempCNC.product = "<div class='alignLeft'>Non Counter</div>";break;
					case "totalCashCounter": tempCNC.product = "<div class='alignLeft'>Counter</div>";isCashDepo=true;break;
					case "totalCheckOnUs": tempCNC.product = "<div class='alignLeft'>On Us</div>";isCheckDepo=true;checkType="ON_US";break;
					case "totalCheckRegional": tempCNC.product = "<div class='alignLeft'>Regional</div>";isCheckDepo=true;checkType="REGIONAL";break;
					case "totalCheckNonCounter": tempCNC.product = "<div class='alignLeft'>Non Counter</div>";break;
					case "totalCheckOT": tempCNC.product = "<div class='alignLeft'>OT</div>";isCheckDepo=true;checkType="OT";break;
					case "totalCheckLocal": tempCNC.product = "<div class='alignLeft'>Local</div>";isCheckDepo=true;checkType="LOCAL";break;
					//case "totalCreditMemo" : tempCNC.product = "<div class='alignLeft'>Credit Memo</div>";break;
					case "totalUsCheckInManila" : tempCNC.product = "<div class='alignLeft'>US Cheque drawn in Manila</div>";break;
					case "totalUsCheckOutPh" : tempCNC.product = "<div class='alignLeft'>US Cheque drawn outside PH</div>";break;
					case "totalPesoCheque" : tempCNC.product = "<div class='alignLeft'>Peso Cheque</div>";break;
					//case "totalBankOTCCheckPayment" : tempCNC.product = "<div class='alignLeft'>Bank OTC Check Payment</div>";break;
					case "totalNonCash": tempCNC.product = "<div class='alignLeft'>Non Cash</div>";break;
					case "totalPosBpi": tempCNC.product = "<div class='alignLeft'>POS - BPI</div>";break;
					case "totalPosCtb": tempCNC.product = "<div class='alignLeft'>POS - CITIBANK</div>";break;
					case "totalPosHsbc": tempCNC.product = "<div class='alignLeft'>POS - HSBC</div>";break;
					case "totalPosScb": tempCNC.product = "<div class='alignLeft'>POS - SCB</div>";break;
					case "totalPosRcbc": tempCNC.product = "<div class='alignLeft'>POS - RCBC</div>";break;
					case "totalPosBdo": tempCNC.product = "<div class='alignLeft'>POS - BDO</div>";break;
					case "totalMdsBpi": tempCNC.product = "<div class='alignLeft'>MDS - BPI</div>";break;
					case "totalMdsBdo": tempCNC.product = "<div class='alignLeft'>MDS - BDO</div>";break;
					case "totalMdsRcbc": tempCNC.product = "<div class='alignLeft'>MDS - RCBC</div>";break;
					case "totalMdsSbc": tempCNC.product = "<div class='alignLeft'>MDS - SCB</div>";break;
					case "totalMdsCtb": tempCNC.product = "<div class='alignLeft'>MDS - CITIBANK</div>";break;
					case "totalMdsHsbc": tempCNC.product = "<div class='alignLeft'>MDS - HSBC</div>";break;
					case "totalAutoCtb": tempCNC.product = "<div class='alignLeft'>CITIBANK (AUTOCHARGE)</div>";break;
					case "totalAutoScb": tempCNC.product = "<div class='alignLeft'>SCB (AUTOCHARGE)</div>";break;
					case "totalAutoRcbc": tempCNC.product = "<div class='alignLeft'>RCBC - AUTOCHARGE</div>";break;
					case "totalSunlinkOnline": tempCNC.product = "<div class='alignLeft'>SUNLINK ONLINE</div>";break;
					case "totalSunlinkHsbcPeso": tempCNC.product = "<div class='alignLeft'>SUNLINK HSBC PESO</div>";break;
					case "totalEpsBpi": tempCNC.product = "<div class='alignLeft'>EPS - BPI</div>";break;
					case "totalNewBusinessIndividual": tempCNC.product = "<div class='alignLeft'>New Business Individual</div>";break;
					case "totalNewBusinessVariable": tempCNC.product = "<div class='alignLeft'>NB VUL</div>";break;
					case "totalIndividualRenewal": tempCNC.product = "<div class='alignLeft'>Individual Renewal</div>";break;
					case "totalVariableRenewal": tempCNC.product = "<div class='alignLeft'>VUL Renewal</div>";break;
					case "totalNonCashFromDTR": tempCNC.product = "<div class='alignLeft'>Non Cash</div>";break;
					case "totalNonPolicy": tempCNC.product = "<div class='alignLeft'>Non Policy</div>";break;
					case "totalWorksiteNonPosted": tempCNC.product = "<div class='alignLeft'>Worksite (not posted)</div>";break;
					case "sessionTotal": tempCNC.product = "";break;
					case "reversalProduct" : tempCNC.product = "";break;
					case "totalCashEpsBpi":  tempCNC.product = "<div class='alignLeft'>EPS - BPI</div>";break;
					case "totalPmo":  tempCNC.product = "<div class='alignLeft'>Postal Money Order</div>";break;
					default: alert("Unknown Product Total: " + section); break;
				}
			} else{
				tempCNC.product = "";	
				switch(section){
					case "totalCashCounter": isCashDepo=true;break;
					case "totalCheckOnUs": isCheckDepo=true;checkType="ON_US";break;
					case "totalCheckRegional": isCheckDepo=true;checkType="REGIONAL";break;
					case "totalCheckOT": isCheckDepo=true;checkType="OT";break;
					case "totalCheckLocal": isCheckDepo=true;checkType="LOCAL";break;
				}
			}
			tempCNC.cashier = "<div class='alignLeft'>"+slocpiPesoSecData[a].cashierName+"</div>";
			if(isSLOCPIConfirmed(slocpiPesoSecData[a].cashierId))	{		
				if(section == "reversalProduct"){
					if(parseFloat(slocpiPesoSecData[a].tradVul) != 0){
						tempCNC.tradVul = "<div class='alignRight'><a style='cursor:pointer;text-decoration:underline;color:blue;'onclick='openSPReversal("+getBalancingToolProductId(slocpiPesoSecData[a].cashierId,"IL_VL_PESO",section,slocpiPesoData)+");'>"+numberWithCommas(parseFloat(slocpiPesoSecData[a].tradVul).toFixed(2))+"</a></div>";
					} else{
						tempCNC.tradVul = "<div class='alignRight'>"+numberWithCommas(parseFloat(slocpiPesoSecData[a].tradVul).toFixed(2))+"</div>";
					}
					if(parseFloat(slocpiPesoSecData[a].groupLife) != 0){
						tempCNC.groupLife = "<div class='alignRight'><a style='cursor:pointer;text-decoration:underline;color:blue;'onclick='openSPReversal("+getBalancingToolProductId(slocpiPesoSecData[a].cashierId,"GL",section,slocpiPesoData)+");'>"+parseFloat(slocpiPesoSecData[a].groupLife).toFixed(2)+"</a></div>";
					}else{
						tempCNC.groupLife = "<div class='alignRight'>"+numberWithCommas(parseFloat(slocpiPesoSecData[a].groupLife).toFixed(2))+"</div>";
					}
				} else{
					tempCNC.tradVul = "<div class='alignRight'>"+numberWithCommas(parseFloat(slocpiPesoSecData[a].tradVul).toFixed(2))+"</div>";
					tempCNC.groupLife = "<div class='alignRight'>"+numberWithCommas(parseFloat(slocpiPesoSecData[a].groupLife).toFixed(2))+"</div>";
					if(isCashDepo){
						if(parseFloat(slocpiPesoSecData[a].tradVul) > 0){
							tempCNC.dsTV  = getCashDepoURL("SLOCPI","TRAD/VUL","PHP",slocpiPesoSecData[a].cashierId);
							tempCNC.vdsTV = getValidatedCashDepoURL("SLOCPI","TRAD/VUL","PHP",slocpiPesoSecData[a].cashierId);
						}
						if(parseFloat(slocpiPesoSecData[a].groupLife) > 0){
							tempCNC.dsGL = getCashDepoURL("SLOCPI","TRAD/VUL","PHP",slocpiPesoSecData[a].cashierId);
							tempCNC.vdsGL = getValidatedCashDepoURL("SLOCPI","TRAD/VUL","PHP",slocpiPesoSecData[a].cashierId);
						}
				
					}else if(isCheckDepo){
						if(parseFloat(slocpiPesoSecData[a].tradVul) > 0){
							tempCNC.dsTV  = getChequeDepoURL("SLOCPI","TRAD/VUL","PHP",slocpiPesoSecData[a].cashierId,checkType);
							tempCNC.vdsTV  = getValidatedChequeDepoURL("SLOCPI","TRAD/VUL","PHP",slocpiPesoSecData[a].cashierId,checkType);
							
						}
						if(parseFloat(slocpiPesoSecData[a].groupLife) > 0){
							tempCNC.dsGL = getChequeDepoURL("SLOCPI","TRAD/VUL","PHP",slocpiPesoSecData[a].cashierId,checkType);
							tempCNC.vdsGL  = getValidatedChequeDepoURL("SLOCPI","TRAD/VUL","PHP",slocpiPesoSecData[a].cashierId,checkType);
						}
				
					}
				}
				tv = tv + parseFloat(slocpiPesoSecData[a].tradVul);
				gl = gl + parseFloat(slocpiPesoSecData[a].groupLife);
			} else{
				tempCNC.tradVul = "<div class='alignCenter'>-</div>";
				tempCNC.groupLife = "<div class='alignCenter'>-</div>";
			}
			if(jQuery.inArray(section,excludeFromCashierTotal) == -1){
				slocpiPesoConsolidatedTotals.tv = (parseFloat(slocpiPesoConsolidatedTotals.tv) + parseFloat(slocpiPesoSecData[a].tradVul)).toFixed(2);
				slocpiPesoConsolidatedTotals.gl = (parseFloat(slocpiPesoConsolidatedTotals.gl) + parseFloat(slocpiPesoSecData[a].groupLife)).toFixed(2);
			}			
			tempCNC.ccmNotes = "<div class='alignCenter'>"+getNote(rowIndex,"CCM")+"</div>";
			tempCNC.ppaNotes = "<div class='alignCenter'>"+getNote(rowIndex,"PPA")+"</div>";
			tempCNC.findings = "<div class='alignCenter' collapseTarget='"+cdTarget+"'>"+getNote(rowIndex,"FINDINGS")+"</div>";			
			rowIndex++;
			firstRowFlag = false;		
			returnCNC.push(tempCNC);
		}
	}
	sectionCashierTotals.tv = numberWithCommas(parseFloat(tv).toFixed(2));
	sectionCashierTotals.gl = numberWithCommas(parseFloat(gl).toFixed(2));
	sectionObj.prodTotals = returnCNC;
	sectionObj.cashierTotals = sectionCashierTotals;
	return sectionObj;
}

function mergeSLOCPIPesoCashierData(cashierProduct){
	var cpArr = cashierProduct.split("~");
	var cashierId = cpArr[0];
	var product = cpArr[1];
	var tempCashier = {};
	for(a in slocpiPesoData){
		if(slocpiPesoData[a].cashierId == cashierId && slocpiPesoData[a].product == product){
			if(slocpiPesoData[a].productCode == "IL_VL_PESO"){
				tempCashier.tradVul = slocpiPesoData[a].total;
			} else if(slocpiPesoData[a].productCode == "GL"){
				tempCashier.groupLife = slocpiPesoData[a].total;
			}
			tempCashier.cashierId = slocpiPesoData[a].cashierId;
			tempCashier.cashierName = slocpiPesoData[a].cashierName;
			tempCashier.product = product;
		}
	}
	return tempCashier;
}

function getSLOCPIPGrandTotalPerCashier(cdTarget){
	var returnSection = [];
	var c = 0;
	for(c in cashierList){
		var tv = 0;
		var gl = 0;
		var mf21 = 0;
		var d=0;
		for(d in slocpiPesoData){
			if(jQuery.inArray(slocpiPesoData[d].product,excludeFromCashierTotal) == -1 && slocpiPesoData[d].cashierId.toUpperCase() == cashierList[c].id.toUpperCase()){
				switch(slocpiPesoData[d].productCode){
					case "IL_VL_PESO": tv = tv + parseFloat(slocpiPesoData[d].total);break;
					case "GL": gl = gl  + parseFloat(slocpiPesoData[d].total);break;
				}
			}
		}
		var tempCashier = {};
		tempCashier.cashier = "<div class='alignLeft'  collapseTarget='"+cdTarget+"'>"+cashierList[c].name+"</div>";
		if(isSLOCPIConfirmed(cashierList[c].id)){
			tempCashier.tradVul = "<div class='alignRight'>"+numberWithCommas(tv.toFixed(2))+"</div>";
			tempCashier.groupLife = "<div class='alignRight'>"+numberWithCommas(gl.toFixed(2))+"</div>";
		} else {
			tempCashier.tradVul = "<div class='alignCenter'>-</div>";
			tempCashier.groupLife = "<div class='alignCenter'>-</div>";
		}
		returnSection.push(tempCashier);
	}
	return returnSection;
}



var slocpiPesoConsolidatedTotals = {tv: "0.00", gl: "0.00"};
