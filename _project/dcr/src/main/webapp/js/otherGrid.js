
var otherTableLayout = [ {
	name : " ",
	field : "product",
	width : "25%"
}];

var totalCashCounter = {product: "Cash - Counter"};
var totalCashNonCounter = {product: "Cash - Non Counter"};
var totalCheckOnUs = {product: "Cheque - On us"};
var totalChequeLocal = {product: "Cheque - Local"};
var totalCheckRegional = {product: "Cheque - Regional"};
var totalCheckNonCounter = {product: "Cheque - Non Counter"};
var totalUsCheckInManila = {product: "US Cheque drawn in Manila"};
var totalUsCheckOutPh = {product: "US Cheque drawn outside PH"};
var totalDollarCheque = {product: "Dollar Cheque"};
var totalCheckOT = {product: "Cheque - OT"};
var totalNonCash = {product: "Noncash"};
var cardTypes = {product: "Card Types"};
var totalPosBpi = {product: "&nbsp;&nbsp;&nbsp;POS - BPI"};
var totalPosCtb = {product: "&nbsp;&nbsp;&nbsp;POS - CTIB"};
var totalPosHsbc = {product: "&nbsp;&nbsp;&nbsp;POS - HSBC"};
var totalPosScb = {product: "&nbsp;&nbsp;&nbsp;POS - SCB"};
var totalPosRcbc = {product: "&nbsp;&nbsp;&nbsp;POS - RCBC"};
var totalPosBdo = {product: "&nbsp;&nbsp;&nbsp;POS - BDO"};
var total = {product: "TOTAL"};

function setLoadData(otherData){

$(document).ready(function(){


new TableContainer({
	divName : "otherTableHeader",
	nodeName : "otherTableHeaderNode",
	width : "100%",
	id : "otherTable",
	layout : otherTableLayout,
	tableClass : "borderedTable"
}).startUp();

var otherTableData = [];

var otherTableBlankRow = [{product: "&nbsp;", cobPeso: " ", cobDollar: " ", filler2: " "}];

var otherTable = _globalTableContainer["otherTable"];
appendJsons([otherTableData,otherData]);

otherTable.setData(otherTableData);
otherTable.refreshTable();

var confirmButtonDiv = document.getElementById("confirmButtonDiv");

var confirmButton = document.createElement("input");
confirmButton.type="button";
confirmButton.style.right="0px";
confirmButton.id="confirmButton";
confirmButton.value="Confirm";
confirmButton.onclick = confirmOther;
confirmButtonDiv.appendChild(confirmButton);


var saveButton = document.createElement("input");
saveButton.type="button";
saveButton.style.right="0px";
saveButton.id="saveButton";
saveButton.value="Save";
saveButton.onclick = saveOther;
confirmButtonDiv.appendChild(saveButton);

});

}

function saveOther(){
	document.getElementById("saveButton").disabled = true;
	var confirm = document.getElementById("confirm");
	var otherForm = document.getElementById("otherForm");
	
	confirm.value = false;
	otherForm.submit();
	
}

function confirmOther(){
	document.getElementById("confirmButton").disabled = true;
	var confirm = document.getElementById("confirm");
	var otherForm = document.getElementById("otherForm");
	
	var markup = document.documentElement.innerHTML;
	document.getElementById("htmlCode").value = markup;
	
	confirm.value = true;
	otherForm.submit();
	
}

var onLoadTag=true;

function computeTotal(val){
	var prefix = val.id;
	var total = document.getElementById(prefix.substring(0,1)+"total");
	var temp = document.getElementById("tempStore");
	val.value = val.value.replace(/,/g,"");
	if(validateIsANumber(val) && val.value.indexOf(" ") == -1 && val.value.length > 0){
		var num = parseFloat(val.value).toFixed(2);
		if(parseFloat(num) > 99999999999.99){
			alert("Please keep input below 100 Billion.");
			if(parseFloat(val.value) != parseFloat(temp.value)){
				total.value = (parseFloat(total.value.replace(/,/g,"")) - parseFloat(temp.value)).toFixed(2);
			}
			val.value = "0.00";
			val.focus();
		} else {
			total.value = (parseFloat(total.value.replace(/,/g,"")) - parseFloat(temp.value)).toFixed(2);
			total.value = numberWithCommas((parseFloat(val.value) + parseFloat(total.value)).toFixed(2));
			val.value = numberWithCommas(parseFloat(val.value).toFixed(2));
		}
	} else {
		alert("Not a number");
		if(parseFloat(val.value) != parseFloat(temp.value)){
			total.value = (parseFloat(total.value.replace(/,/g,"")) - parseFloat(temp.value)).toFixed(2);
		}
		val.value = "0.00";
		val.focus();
	}
	onLoadTag = false;
}

function copyToTempStore(val){
	var temp = document.getElementById("tempStore");
	temp.value = val.value.replace(/,/g,"");
}

function returnTextbox(productCode, row, val, readOnly){
	var prefix="";
	switch(productCode){
		case "COBP": prefix="p"; break;
		case "COBD": prefix="d"; break;	
	}
	if(!readOnly){
		return centerCell("<input class=\"inputTextbox\" id=\"" + prefix + row + "\" name=\"" + prefix + row + "\" type=\"number\" class=\"input\" value=\"" + val + "\" onchange=\"computeTotal(this)\"  onfocus=\"copyToTempStore(this)\">");
	} else{
		return centerCell("<input class=\"inputTextbox\" id=\"" + prefix + row + "\" name=\"" + prefix + row + "\" readonly=\"true\" type=\"number\" class=\"input\" value=\"" + val + "\" onchange=\"computeTotal(this)\" onfocus=\"copyToTempStore(this)\">");
		
	}
	
}