var searchTableData = [];

var hubs = [{id:"CCH001", name:"Hub01"},{id:"CCH002", name:"Hub02"},{id:"CCH002", name:"Hub02"},{id:"CCH002", name:"Hub02"}];

var showTable = false;

var itemsFound = 0;

function clearResults(){
	window.location.href = "../home/index.html";
}

function clearStartDate(){
	var searchDatePicker = document.getElementById("searchDatePickerStart");
	var dcrDate = document.getElementById("dcrStartDate");
	dcrDate.value = "";
	searchDatePicker.value = "";
}

function clearEndDate(){
	var searchDatePicker = document.getElementById("searchDatePickerEnd");
	var dcrDate = document.getElementById("dcrEndDate");
	dcrDate.value = "";
	searchDatePicker.value = "";
}

function disableHubId(){
	var hubId = document.getElementById("hubId");
	var searchByHub = document.getElementById("searchByHub");
	searchByHub.value = "false";
	hubId.disabled = true;
	var ccId = document.getElementById("ccId");
	var searchByCC = document.getElementById("searchByCC");
	searchByCC.value = "true";
	ccId.disabled = false;
}

function disableCcId(){
	var ccId = document.getElementById("ccId");
	var searchByCC = document.getElementById("searchByCC");
	searchByCC.value = "false";
	ccId.disabled = true;
	var hubId = document.getElementById("hubId");
	var searchByHub = document.getElementById("searchByHub");
	searchByHub.value = "true";
	hubId.disabled = false;
}

function showResultsTable(searchData){
	
	$(document).ready(function(){
		
		destroySearchTable();
		
		var searchTableLayout = [{
			name : "Customer Center Name",
			field : "ccName",
			width : "25%"
		},{
			name : "DCR Date",
			field : "dcrDate",
			width : "15%"
		},{
			name : "Submitted By",
			field : "submittedBy",
			width : "15%"
		},{
			name : "Date Submitted",
			field : "dateSubmitted",
			width : "15%"
		},{
			name : "Process Status",
			field : "processStatus",
			width : "15%"
		},{
			name : "Required Completion Date",
			field : "requiredCompletionDate",
			width : "15%"
		}];
		
		new TableContainer({
			divName : "searchResultDiv",
			nodeName : "searchResultTable",
			width : "100%",
			id : "searchResults",
			layout : searchTableLayout,
			tableClass : "searchResultsTable",
			data: searchData
		}).startUp();
		
		var searchTable = document.getElementById("searchResults");
		
		var options = {
	              optionsForRows : [5,10,20,30,50],
	              rowsPerPage : 10,
	              firstArrow : (new Image()).src="../images/firstBlue.gif",
	              prevArrow : (new Image()).src="../images/prevBlue.gif",
	              lastArrow : (new Image()).src="../images/lastBlue.gif",
	              nextArrow : (new Image()).src="../images/nextBlue.gif",
	              topNav : false
	            }
		sorttable.makeSortable(searchTable);
		$('#searchResults').tablePagination(options);
		
	});
	
}

function destroySearchTable(){
	$("#searchResultTable").empty();
}

function showSearchResultDiv(){
	var searchResultDiv = document.getElementById("searchResultDiv");
	searchResultDiv.style.visibility = 'visible';
}

function submitForm(){
	var searchForm = document.getElementById("searchForm");
        // Added for MR-WF-16-00034 - Random sampling for QA
        var filter = document.getElementById("filterSelect").value;
        if(filter === "ppaQa") {
            document.getElementById("isPpaQa").value = "true";
        } else {
            document.getElementById("isPpaQa").value = "false";
        }
        
	if(validateForm()){		
		searchForm.submit();
	}
}

function validateForm(){
	var validated = true;
	var ccId = document.getElementById("ccId");
	var dcrDate = document.getElementById("dcrDateStr");
	if(ccId.selectedIndex < 0){
		alert("Please choose a site code");
		return false;
	}
	return validated;
	
}

function setDcrStartDate(val){
	var dcrStartDate = document.getElementById("dcrStartDate");
	dcrStartDate.value = val;
}

function setDcrEndDate(val){
	var dcrEndDate = document.getElementById("dcrEndDate");
	dcrEndDate.value = val;
}

function getQueueSelect(hub){
	var queueSelect = document.getElementById("filterSelect");
	var tempOption1 = document.createElement("option");
	tempOption1.value = "reviewAndApproval";
	tempOption1.innerHTML = "DCRHub0" + hub.substring(hub.length-1) + "ReviewAndApproval";
	var tempOption2 = document.createElement("option");
	tempOption2.value = "cashierRecon";
	tempOption2.innerHTML = "DCRHub0" + hub.substring(hub.length-1) + "CashierRecon";
        queueSelect.appendChild(tempOption1);
	queueSelect.appendChild(tempOption2);
}

function getPPAQueueSelect() {
	var queueSelect = document.getElementById("filterSelect");
	var tempOption1 = document.createElement("option");
	tempOption1.value = "ppaRecon";
	tempOption1.innerHTML = "DCRPPARecon";
	var tempOption2 = document.createElement("option");
	tempOption2.value = "ppaAbeyance";
	tempOption2.innerHTML = "DCRPPAAbeyance";
	
	// Added for MR-WF-16-00034 - Random sampling for QA
	var tempOption3 = document.createElement("option");
	tempOption3.value = "ppaQa";
	tempOption3.innerHTML = "DCRQA";

	queueSelect.appendChild(tempOption1);
	queueSelect.appendChild(tempOption2);
	// Added for MR-WF-16-00034 - Random sampling for QA
	queueSelect.appendChild(tempOption3);
}

var cashierRecon = ["NEW","PENDING"];
var reviewAndApproval = ["For Review and Approval", "Awaiting Feedback"];
var ppaRecon = ["Verified", "Not Verified", "Approved for Recon","Reqts Submitted","Reqts Not Submitted"];
var ppaAbeyance = ["For Verification","Awaiting Reqts"];
// Added for MR-WF-16-00034 - Random sampling for QA
var ppaQa = ["For QA Review", "QA Reviewed"];

function autoSort(){
	$(document).ready(function(){
	var resultTable = document.getElementById("searchResults");
	var headers = resultTable.getElementsByTagName("th");
	for(var h = 0; h < headers.length; h++){
		if(headers[h].innerHTML == "DCR Date"){
			headers[h].click();
		}
	}
	resetOddEvenRows(resultTable);
	});
}