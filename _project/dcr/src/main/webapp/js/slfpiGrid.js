var slfpiTableLayout = [ {
	name : " ",
	field : "product",
	width : "25%"
}];


var dataFlag = false;


var slfpiTableBlankRow = [];

var slfpiExceptions = [];

var blankRow = {product: "&nbsp;"};
var exceptionsHeader = {product: "<p align=\"left\"><b>EXCEPTIONS(Manual Input)</b></p>"};
var exceptionsReversal = {product: "Reversals"};
var totalCashCounter = {product: "<p align=\"left\">Cash - Counter</p>"};
var totalCashNonCounter = {product: "<p align=\"left\">Cash - Non Counter</p>"};
var totalCheckGaf = {product: "<p align=\"left\">Check - GAF</p>"};
var totalCheckOnUs = {product: "<p align=\"left\">Cheque - On us</p>"};
var totalCheckLocal = {product: "<p align=\"left\">Cheque - Local</p>"};
var totalCheckRegional = {product: "<p align=\"left\">Cheque - Regional</p>"};
var totalPmo = {product: "<p align=\"left\">Postal Money Order</p>"};
var totalCheckNonCounter = {product: "<p align=\"left\">Cheque - Non Counter</p>"};
var totalCheckOT = {product: "<p align=\"left\">Cheque - OT</p>"};
var totalNonCash = {product: "<p align=\"left\">Noncash</p>"};
var totalCreditMemo = {product: "<p align=\"left\">Total Credit Memo</p>"};
var totalUsCheckInManila = {product: "<p align=\"left\">On Us Check In Manila</p>"};
var totalUsCheckOutPh = {product: "<p align=\"left\">On Us Check Out Ph</p>"};
var totalDollarCheque = {product: "<p align=\"left\">Dollar Cheque</p>"};
var totalBankOTCCheckPayment = {product: "<p align=\"left\">Bankk OTC Check Payment</p>"};
var cardTypes = {product: "<p align=\"left\">Card Types</p>"};
var totalCardGaf = {product: "<p align=\"left\">&nbsp;&nbsp;&nbsp;Card - GAF</p>"};
var totalPosBpi = {product: "<p align=\"left\">&nbsp;&nbsp;&nbsp;POS - BPI</p>"};
var totalPosCtb = {product: "<p align=\"left\">&nbsp;&nbsp;&nbsp;POS - CTIB</p>"};
var totalPosHsbc = {product: "<p align=\"left\">&nbsp;&nbsp;&nbsp;POS - HSBC</p>"};
var totalPosScb = {product: "<p align=\"left\">&nbsp;&nbsp;&nbsp;POS - SCB</p>"};
var totalPosRcbc = {product: "<p align=\"left\">&nbsp;&nbsp;&nbsp;POS - RCBC</p>"};
var totalPosBdo = {product: "<p align=\"left\">&nbsp;&nbsp;&nbsp;POS - BDO</p>"};
var totalMdsBpi = {product: "<p align=\"left\">&nbsp;&nbsp;&nbsp;MDS - BPI</p>"};
var totalMdsCtb = {product: "<p align=\"left\">&nbsp;&nbsp;&nbsp;MDS - CTIB</p>"};
var totalMdsHsbc = {product: "<p align=\"left\">&nbsp;&nbsp;&nbsp;MDS - HSBC</p>"};
var totalMdsSbc = {product: "<p align=\"left\">&nbsp;&nbsp;&nbsp;MDS - SCB</p>"};
var totalMdsRcbc = {product: "<p align=\"left\">&nbsp;&nbsp;&nbsp;MDS - RCBC</p>"};
var totalMdsBdo = {product: "<p align=\"left\">&nbsp;&nbsp;&nbsp;MDS - BDO</p>"};
var totalAutoCtb = {product: "<p align=\"left\">&nbsp;&nbsp;&nbsp;CITIBANK (AUTOCHARGE)</p>"};
var totalAutoScb = {product: "<p align=\"left\">&nbsp;&nbsp;&nbsp;SCB (AUTOCHARGE</p>"};
var totalAutoRcbc = {product: "<p align=\"left\">&nbsp;&nbsp;&nbsp;RCBC - AUTOCHARGE</p>"};
var totalSunlinkOnline = {product: "<p align=\"left\">&nbsp;&nbsp;&nbsp;SUNLINK ONLINE</p>"};
var totalSunlinkHsbcPeso = {product: "<p align=\"left\">&nbsp;&nbsp;&nbsp;SUNLINK HSBC PESO</p>"};
var totalEpsBpi = {product: "<p align=\"left\">&nbsp;&nbsp;&nbsp;EPS - BPI</p>"};
var paymentFromInsurer = {product: "<p align=\"left\">Payment from Insurer</p>"};
var total = {product: "<p align=\"left\">TOTAL</p>"};

function setLoadData(slfpiData){
$(document).ready(function(){



new TableContainer({
	divName : "slfpiTableHeader",
	nodeName : "slfpiTableHeaderNode",
	width : "100%",
	id : "slfpiTable",
	layout : slfpiTableLayout,
	tableClass : "borderedTable"
}).startUp();

var slfpiTableData = [];

var slfpiTable = _globalTableContainer["slfpiTable"];

appendJsons([slfpiTableData,slfpiTableBlankRow,slfpiData,slfpiTableBlankRow,slfpiExceptions]);

slfpiTable.setData(slfpiTableData);
slfpiTable.refreshTable();

var confirmButtonDiv = document.getElementById("confirmButtonDiv");

var confirmButton = document.createElement("input");
confirmButton.type="button";
confirmButton.onclick=function() { callConfirmForm(); };
confirmButton.style.right="0px";
confirmButton.id="confirmButton";
confirmButton.value="Confirm";
confirmButtonDiv.appendChild(confirmButton);



});
}

var callConfirmForm = function () {
	document.getElementById("confirmButton").disabled = true;
	var form = document.getElementById("slfpiForm");
	var markup = document.documentElement.innerHTML;
	document.getElementById("htmlCode").value = markup;
	form.submit();
}
