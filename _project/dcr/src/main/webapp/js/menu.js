function generateMenu(){
	var mainMenuItems = new Array("HOME","SEARCH","Adding Machine","Collections","Mixed Cheque Payee","Create Deposit Slip","Create VDIL","Upload Files");
	var subMenuItems_Collections = new Array("SLAMCI - PESO","SLAMCI - DOLLAR","SLOCPI","SLGFI","SLFPI","OTHERS");
	var subMenuItems_Cr8DepoSlip = new Array("SLAMCI Peso Cash","SLAMCI Peso Check per type of check",
			"SLAMCI Dollar cash","SLAMCI Dollar Check per type of check","SLOCPI Peso Cash VUL/TRAD",
			"SLOCPI Peso Check per typ of check VUL/TRAD","SLOCPI Peso CASH Grp Life","SLOCPI Peso Check per type of check Grp Life",
			"SLOCPI Dollar Cash VUL/TRAD","SLOCPI Dollar Check per typ of check VUL/TRAD",
			"SLGFI Peso Cash VUL/TRAD","SLGFI Peso Check per typ of check VUL/TRAD","SLGFI Dollar Cash VUL/TRAD",
			"SLGFI Dollar Check per typ of check VUL/TRAD","SLFPI Peso Cash Preneed","SLFPI Peso Check per typ of check Preneed",
			"SLFPI Peso Cash Group Preneed","SLFPI Peso Check per typ of check Group Preneed","SLOCPI Peso Cash COB",
			"SLOCPI Peso Check per typ of check COB","SLOCPI Dollar Cash COB","SLOCPI Dollar Check per typ of check COB");
	
    var htmlStr = "<ul id='mainmenu'>";

    for (var i = 0; i < mainMenuItems.length; i++) {
    	//console.log(mainMenuItems[i]);
    	if(mainMenuItems[i]=="Collections"){
    		htmlStr += "<li class='mainmenuitem' id='collections'><a href='#' onclick=toggleMenu('collections','collectionsSubMenu')>"  + mainMenuItems[i] + "</a>";
    		var root = "<ul id='collectionsSubMenu'>";
    		for (var x = 0; x < subMenuItems_Collections.length; x++){
    			if(subMenuItems_Collections[x]=="SLFPI"){
    				root += "<li class='submenuitem'><a href='#' onclick=switchGrid('slfpi')>" + subMenuItems_Collections[x] + "</a></li>";
    			}else if(subMenuItems_Collections[x]=="SLOCPI"){
    				root += "<li class='submenuitem'><a href='#' onclick=switchGrid('slocpi')>" + subMenuItems_Collections[x] + "</a></li>";
    			}else{
    				root += "<li class='submenuitem'><a href='#'>" + subMenuItems_Collections[x] + "</a></li>";
    			}
    		}
    		htmlStr += root;
    		htmlStr += "</ul></li>";
    	}else if(mainMenuItems[i]=="Create Deposit Slip"){
    		htmlStr += "<li class='mainmenuitem' id='createDepoSlip'><a href='#' onclick=toggleMenu('createDepoSlip','createDepoSlipSubMenu')>"  + mainMenuItems[i] + "</a>";
    		var root = "<ul id='createDepoSlipSubMenu'>";
    		for (var j = 0; j < subMenuItems_Cr8DepoSlip.length; j++){
    			root += "<li class='submenuitem'><a href='#'>" + subMenuItems_Cr8DepoSlip[j] + "</a></li>";
    		}
    		htmlStr += root;
    		htmlStr += "</ul></li>";
    	}else{
    		htmlStr += "<li class='mainmenuitem'><a href='#'>" + mainMenuItems[i] + "</a></li>";
    	}
    }
    
    htmlStr += "</ul>";
    var menuDiv = document.getElementById("menudiv");
    //console.log(htmlStr);
    menuDiv.innerHTML = htmlStr;
}
