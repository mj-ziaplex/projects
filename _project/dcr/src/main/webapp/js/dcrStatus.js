function setDCRWorkItems(dcrData) {
	$(document).ready(function(){
		var dcrLabelLayout = [{
			name : "DCR STATUS",
			field : "dcrLabel",
			width : "100%"
		}];
		
		new TableContainer({
			divName : "dcrDiv",
			nodeName : "dcrLabelNode",
			width : "100%",
			id : "dcrLabel",
			layout : dcrLabelLayout,
			tableClass : "dcrLabelBorderedTable"
		}).startUp();
		
		var dcrTableLayout = [{
			name : "Customer Center",
			field : "customerCenter",
			width : "25%"
		},{
			name : "Date",
			field : "date",
			width : "15%"
		},{
			name : "Status",
			field : "status",
			width : "20%"
		},{
			name : "IPAC-DCR",
			field : "hasDownload",
			width : "5%"
		},{
			name : "Last Update User",
			field : "updateUserId",
			width : "10%"
		},{
			name : "Update Time",
			field : "updateUserTime",
			width : "15%"
		},{
			name : "Action",
			field : "action",
			width : "10%"
		}];
		
		new TableContainer({
			divName : "dcrTableNode",
			nodeName : "dcrNode",
			width : "100%",
			id : "dcrTable",
			layout : dcrTableLayout,
			tableClass : "dcrBorderedTable"
		}).startUp();
		
		var dcrTable = _globalTableContainer["dcrTable"];
		dcrTable.setData(dcrData);
		dcrTable.refreshTable();
		
		var dcr = document.getElementById("dcrTable");
		
		var options = {
	              optionsForRows : [5,10,20,30,50],
	              rowsPerPage : 20,
	              firstArrow : (new Image()).src="../images/firstBlue.gif",
	              prevArrow : (new Image()).src="../images/prevBlue.gif",
	              lastArrow : (new Image()).src="../images/lastBlue.gif",
	              nextArrow : (new Image()).src="../images/nextBlue.gif",
	              topNav : false
	            }
		$('#dcrTable').tablePagination(options);
		sorttable.makeSortable(dcr);
		autoSort(dcr);
	});
}

function autoSort(dcrTable){
	$(document).ready(function(){
	var headers = dcrTable.getElementsByTagName("th");
	for(var h = 0; h < headers.length; h++){
		if(headers[h].innerHTML == "Date"){
			headers[h].click();
		}
	}
	resetOddEvenRows(dcrTable);
	});
}

var showViewSQLPopUp = function(val) {
	$('#viewSQLPopUpNode').html('Select * FROM [WDFNPE01].[f_sw].[VWVQ1_DCRPPAAbeyance] where F_Subject = \''+val.dcr+'\';');
	$(function() {
		$( "#viewSQLPopUpTableNode" ).dialog({
			autoOpen: true,
			height: 50,
			width: 600,
			modal: true,
			closeOnEscape: true,
			draggable: false,
			resizable: false,
			dialogClass: "viewSQLTableClass",
			title: "View SQL"
		}).dialog('widget').position({ my: 'center', at: 'center', of: $(document.getElementById("mainTemplateBodyId")) });
	});
};

function setDcrStartDate(val){
	var dcrStartDate = document.getElementById("dcrStartDate");
	dcrStartDate.value = val;
}

function setDcrEndDate(val){
	var dcrEndDate = document.getElementById("dcrEndDate");
	dcrEndDate.value = val;
}

function clearStartDate(){
	var searchDatePicker = document.getElementById("searchDatePickerStart");
	var dcrDate = document.getElementById("dcrStartDate");
	dcrDate.value = "";
	searchDatePicker.value = "";
}

function clearEndDate(){
	var searchDatePicker = document.getElementById("searchDatePickerEnd");
	var dcrDate = document.getElementById("dcrEndDate");
	dcrDate.value = "";
	searchDatePicker.value = "";
}

function submitForm(){
	var searchForm = document.getElementById("searchDCRStatusForm");
		searchForm.submit();
}

function isValidDate(sText) { 
    var isValid = true;
    var matches = sText.value.match(/^(\d{2})(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)(\d{4}) (\d{2}):(\d{2}):(\d{2})$/);
  
    if(sText.value != "" && null == matches){
        isValid = false;
        sText.focus();
    }
    return isValid;
}

function isValidRemark(sText) { 
    var isValid = true;
    if(sText.value == ""){
        isValid = false;
    }
    return isValid;
}

function isValidOption(sText) { 
    var isValid = true;
    if(sText.value == "-"){
        isValid = false;
    }
    return isValid;
}

function onUpdateSubmit(){
	var isValid = true;
	var errMsg = "";
	if(!isValidDate(document.getElementById("logDate"))){
		errMsg+= "*Invalid log date format\n";
		if(!isValidOption(document.getElementById("updateUserId"))){
			errMsg+= "*No user selected\n";
		}
		if(!isValidOption(document.getElementById("status"))){
			errMsg+= "*No status selected\n";
		}
		if(!isValidRemark(document.getElementById("remark"))){
			errMsg+= "*Remark is blank\n";
		}
		isValid = false;
	}else if(document.getElementById("logDate").value != ""){
		if(!isValidOption(document.getElementById("updateUserId"))){
			errMsg+= "*No user selected\n";
			isValid = false;
		}
		if(!isValidOption(document.getElementById("status"))){
			errMsg+= "*No status selected\n";
			isValid = false;
		}
		if(!isValidRemark(document.getElementById("remark"))){
			errMsg+= "*Remark is blank\n";
			isValid = false;
		}
	}
	
	if(isValid){
		document.getElementById("adminDCRStatusForm").submit();
	}else{
		alert(errMsg);
		return false;
	}
}
