var slamciDollarColumns = [
    {name: "", field: "product", width: "6%"},
    {name: "", field: "cashier", width: "12%"},
    {name: "Dollar Advantage Fund(MF4)", field: "mf4", width: "6%"},
    {name: "DS", field: "mf4DS", width: "3%"},
    {name: "VDS", field: "mf4VDS", width: "3%"},
    {name: "Dollar Abundance Fund(MF6)", field: "mf6", width: "6%"},
    {name: "DS", field: "mf6DS", width: "3%"},
    {name: "VDS", field: "mf6VDS", width: "3%"},
    {name: "Group Fund(MF21)", field: "mf21", width: "6%"},
    {name: "DS", field: "mf21DS", width: "3%"},
    {name: "VDS", field: "mf21VDS", width: "3%"},
    {name: "Wellspring Fund(MF10)", field: "mf10", width: "6%"},
    {name: "DS", field: "mf10DS", width: "3%"},
    {name: "VDS", field: "mf10VDS", width: "3%"},
    {name: "Voyager Fund(MF11)", field: "mf11", width: "6%"},
    {name: "DS", field: "mf11DS", width: "3%"},
    {name: "VDS", field: "mf11VDS", width: "3%"},
    {name: "Dollar Starter Fund(MF12)", field: "mf12", width: "6%"},
    {name: "DS", field: "mf12DS", width: "3%"},
    {name: "VDS", field: "mf12VDS", width: "3%"},
    {name: "CCM NOTES", field: "ccmNotes", width: "3%"},
    // Modified for MR-WF-16-00036 - Change PPA notes to CCQA notes
    {name: "CCQA NOTES", field: "ppaNotes", width: "3%"},
    {name: "Findings", field: "findings", width: "4%"}
];

var slamciDollarColumnsNH = [
    {name: "", field: "product", width: "6%"},
    {name: "", field: "cashier", width: "12%"},
    {name: "", field: "mf4", width: "6%"},
    {name: "", field: "mf4DS", width: "3%"},
    {name: "", field: "mf4VDS", width: "3%"},
    {name: "", field: "mf6", width: "6%"},
    {name: "", field: "mf6DS", width: "3%"},
    {name: "", field: "mf6VDS", width: "3%"},
    {name: "", field: "mf21", width: "6%"},
    {name: "", field: "mf21DS", width: "3%"},
    {name: "", field: "mf21VDS", width: "3%"},
    {name: "", field: "mf10", width: "6%"},
    {name: "", field: "mf10DS", width: "3%"},
    {name: "", field: "mf10VDS", width: "3%"},
    {name: "", field: "mf11", width: "6%"},
    {name: "", field: "mf11DS", width: "3%"},
    {name: "", field: "mf11VDS", width: "3%"},
    {name: "", field: "mf12", width: "6%"},
    {name: "", field: "mf12DS", width: "3%"},
    {name: "", field: "mf12VDS", width: "3%"},
    {name: "", field: "ccmNotes", width: "3%"},
    {name: "", field: "ppaNotes", width: "3%"},
    {name: "", field: "findings", width: "4%"}
];

function generateSLAMCIDollarSection(slamciDollarTableDivId, slamciDollarDivDetails, slamciDollarSummaryDivDetails) {
    $(document).ready(function() {

        mergeSLAMCIDollarCashierProductTypes();

        var dollarCashHeader = [{
                product: "<div class='alignLeft' cdTarget='slamciDollarCash' onclick='collapseDivision(this)'>DOLLAR CASH</div>",
                cashier: "",
                mf4: "",
                mf4DS: "",
                mf4VDS: "",
                mf6: "",
                mf6DS: "",
                mf6VDS: "",
                mf21: "",
                mf21DS: "",
                mf21VDS: "",
                mf10: "",
                mf10DS: "",
                mf10VDS: "",
                mf11: "",
                mf11DS: "",
                mf11VDS: "",
                mf12: "",
                mf12DS: "",
                mf12VDS: "",
                ccmNotes: "",
                ppaNotes: "",
                findings: ""
            }];

        var cashNonCounterData = generateSLAMCIDollarSectionData("totalCashNonCounter", "slamciDollarCash");

        var cncTotal = [{
                product: "",
                cashier: "<div class='alignRight'>Sub Total > Non Counter</div>",
                mf4: "<div class='numFormat'>" + cashNonCounterData.cashierTotals.mf4Total + "</div>",
                mf4DS: "",
                mf4VDS: "",
                mf6: "<div class='numFormat'>" + cashNonCounterData.cashierTotals.mf6Total + "</div>",
                mf6DS: "",
                mf21: "<div class='numFormat'>" + cashNonCounterData.cashierTotals.mf21Total + "</div>",
                mf21DS: "",
                mf21VDS: "",
                mf10: "<div class='numFormat'>" + cashNonCounterData.cashierTotals.mf10Total + "</div>",
                mf10DS: "",
                mf10VDS: "",
                mf11: "<div class='numFormat'>" + cashNonCounterData.cashierTotals.mf11Total + "</div>",
                mf11DS: "",
                mf11VDS: "",
                mf12: "<div class='numFormat'>" + cashNonCounterData.cashierTotals.mf12Total + "</div>",
                mf12DS: "",
                mf12VDS: "",
                ccmNotes: "",
                ppaNotes: "",
                findings: ""
            }];

        var cashCounterData = generateSLAMCIDollarSectionData("totalCashCounter", "slamciDollarCash");

        var ccTotal = [{
                product: "",
                cashier: "<div class='alignRight'>Sub Total > Counter</div>",
                mf4: "<div class='numFormat'>" + cashCounterData.cashierTotals.mf4Total + "</div>",
                mf4DS: "",
                mf4VDS: "",
                mf6: "<div class='numFormat'>" + cashCounterData.cashierTotals.mf6Total + "</div>",
                mf6DS: "",
                mf21: "<div class='numFormat'>" + cashCounterData.cashierTotals.mf21Total + "</div>",
                mf21DS: "",
                mf21VDS: "",
                mf10: "<div class='numFormat'>" + cashCounterData.cashierTotals.mf10Total + "</div>",
                mf10DS: "",
                mf10VDS: "",
                mf11: "<div class='numFormat'>" + cashCounterData.cashierTotals.mf11Total + "</div>",
                mf11DS: "",
                mf11VDS: "",
                mf12: "<div class='numFormat'>" + cashCounterData.cashierTotals.mf12Total + "</div>",
                mf12DS: "",
                mf12VDS: "",
                ccmNotes: "",
                ppaNotes: "",
                findings: ""
            }];

        var dollarCashTotals = getSLAMCIDollarAreaTotal([cashNonCounterData, cashCounterData]);

        var areaCashTotal = [{
                product: "<div class='totalRow'>&nbsp;</div>",
                cashier: "<div class='totalRow'><div class='alignRight'>TOTAL CASH</div></div>",
                mf4: "<div class='totalRow'><div class='numFormat'>" + dollarCashTotals.mf4 + "</div></div>",
                mf4DS: "<div class='totalRow'>&nbsp;</div>",
                mf4VDS: "<div class='totalRow'>&nbsp;</div>",
                mf6: "<div class='totalRow'><div class='numFormat'>" + dollarCashTotals.mf6 + "</div></div>",
                mf6DS: "<div class='totalRow'>&nbsp;</div>",
                mf6VDS: "<div class='totalRow'>&nbsp;</div>",
                mf21: "<div class='totalRow'><div class='numFormat'>" + dollarCashTotals.mf21 + "</div></div>",
                mf21DS: "<div class='totalRow'>&nbsp;</div>",
                mf21VDS: "<div class='totalRow'>&nbsp;</div>",
                mf10: "<div class='totalRow'><div class='numFormat'>" + dollarCashTotals.mf10 + "</div></div>",
                mf10DS: "<div class='totalRow'>&nbsp;</div>",
                mf10VDS: "<div class='totalRow'>&nbsp;</div>",
                mf11: "<div class='totalRow'><div class='numFormat'>" + dollarCashTotals.mf11 + "</div></div>",
                mf11DS: "<div class='totalRow'>&nbsp;</div>",
                mf11VDS: "<div class='totalRow'>&nbsp;</div>",
                mf12: "<div class='totalRow'><div class='numFormat'>" + dollarCashTotals.mf12 + "</div></div>",
                mf12DS: "<div class='totalRow'>&nbsp;</div>",
                mf12VDS: "<div class='totalRow'>&nbsp;</div>",
                ccmNotes: "<div class='totalRow'>&nbsp;</div>",
                ppaNotes: "<div class='totalRow'>&nbsp;</div>",
                findings: "<div class='totalRow'>&nbsp;</div>"
            }];

        var dollarCheckHeader = [{
                product: "<div class='alignLeft' cdTarget='slamciDollarCheck' onclick='collapseDivision(this)'>DOLLAR CHECK</div>",
                cashier: "",
                mf4: "",
                mf4DS: "",
                mf4VDS: "",
                mf6: "",
                mf6DS: "",
                mf6VDS: "",
                mf21: "",
                mf21DS: "",
                mf21VDS: "",
                mf10: "",
                mf10DS: "",
                mf10VDS: "",
                mf11: "",
                mf11DS: "",
                mf11VDS: "",
                mf12: "",
                mf12DS: "",
                mf12VDS: "",
                ccmNotes: "",
                ppaNotes: "",
                findings: ""
            }];

        var dollarChequeData = generateSLAMCIDollarSectionData("totalDollarCheque", "slamciDollarCheck");

        var dcTotal = [{
                product: "",
                cashier: "<div class='alignRight'>Sub Total > Dollar Cheque</div>",
                mf4: "<div class='numFormat'>" + dollarChequeData.cashierTotals.mf4Total + "</div>",
                mf4DS: "",
                mf4VDS: "",
                mf6: "<div class='numFormat'>" + dollarChequeData.cashierTotals.mf6Total + "</div>",
                mf6DS: "",
                mf21: "<div class='numFormat'>" + dollarChequeData.cashierTotals.mf21Total + "</div>",
                mf21DS: "",
                mf21VDS: "",
                mf10: "<div class='numFormat'>" + dollarChequeData.cashierTotals.mf10Total + "</div>",
                mf10DS: "",
                mf10VDS: "",
                mf11: "<div class='numFormat'>" + dollarChequeData.cashierTotals.mf11Total + "</div>",
                mf11DS: "",
                mf11VDS: "",
                mf12: "<div class='numFormat'>" + dollarChequeData.cashierTotals.mf12Total + "</div>",
                mf12DS: "",
                mf12VDS: "",
                ccmNotes: "",
                ppaNotes: "",
                findings: ""
            }];

        var checkNCData = generateSLAMCIDollarSectionData("totalCheckNonCounter", "slamciDollarCheck");

        var cNCTotal = [{
                product: "",
                cashier: "<div class='alignRight'>Sub Total > Cheque Non Counter</div>",
                mf4: "<div class='numFormat'>" + checkNCData.cashierTotals.mf4Total + "</div>",
                mf4DS: "",
                mf4VDS: "",
                mf6: "<div class='numFormat'>" + checkNCData.cashierTotals.mf6Total + "</div>",
                mf6DS: "",
                mf21: "<div class='numFormat'>" + checkNCData.cashierTotals.mf21Total + "</div>",
                mf21DS: "",
                mf21VDS: "",
                mf10: "<div class='numFormat'>" + checkNCData.cashierTotals.mf10Total + "</div>",
                mf10DS: "",
                mf10VDS: "",
                mf11: "<div class='numFormat'>" + checkNCData.cashierTotals.mf11Total + "</div>",
                mf11DS: "",
                mf11VDS: "",
                mf12: "<div class='numFormat'>" + checkNCData.cashierTotals.mf12Total + "</div>",
                mf12DS: "",
                mf12VDS: "",
                ccmNotes: "",
                ppaNotes: "",
                findings: ""
            }];

        var dollarCheckTotals = getSLAMCIDollarAreaTotal([dollarChequeData, checkNCData]);

        var areaCheckTotal = [{
                product: "<div class='totalRow'>&nbsp;</div>",
                cashier: "<div class='totalRow'><div class='alignRight'>TOTAL CHECK</div></div>",
                mf4: "<div class='totalRow'><div class='numFormat'>" + dollarCheckTotals.mf4 + "</div></div>",
                mf4DS: "<div class='totalRow'>&nbsp;</div>",
                mf4VDS: "<div class='totalRow'>&nbsp;</div>",
                mf6: "<div class='totalRow'><div class='numFormat'>" + dollarCheckTotals.mf6 + "</div></div>",
                mf6DS: "<div class='totalRow'>&nbsp;</div>",
                mf6VDS: "<div class='totalRow'>&nbsp;</div>",
                mf21: "<div class='totalRow'><div class='numFormat'>" + dollarCheckTotals.mf21 + "</div></div>",
                mf21DS: "<div class='totalRow'>&nbsp;</div>",
                mf21VDS: "<div class='totalRow'>&nbsp;</div>",
                mf10: "<div class='totalRow'><div class='numFormat'>" + dollarCheckTotals.mf10 + "</div></div>",
                mf10DS: "<div class='totalRow'>&nbsp;</div>",
                mf10VDS: "<div class='totalRow'>&nbsp;</div>",
                mf11: "<div class='totalRow'><div class='numFormat'>" + dollarCheckTotals.mf11 + "</div></div>",
                mf11DS: "<div class='totalRow'>&nbsp;</div>",
                mf11VDS: "<div class='totalRow'>&nbsp;</div>",
                mf12: "<div class='totalRow'><div class='numFormat'>" + dollarCheckTotals.mf12 + "</div></div>",
                mf12DS: "<div class='totalRow'>&nbsp;</div>",
                mf12VDS: "<div class='totalRow'>&nbsp;</div>",
                ccmNotes: "<div class='totalRow'>&nbsp;</div>",
                ppaNotes: "<div class='totalRow'>&nbsp;</div>",
                findings: "<div class='totalRow'>&nbsp;</div>"
            }];

        var nonCashHeader = [{
                product: "<div class='alignLeft' cdTarget='slamciDollarNonCash' onclick='collapseDivision(this)'>NON CASH</div>",
                cashier: "",
                mf4: "",
                mf4DS: "",
                mf4VDS: "",
                mf6: "",
                mf6DS: "",
                mf6VDS: "",
                mf21: "",
                mf21DS: "",
                mf21VDS: "",
                mf10: "",
                mf10DS: "",
                mf10VDS: "",
                mf11: "",
                mf11DS: "",
                mf11VDS: "",
                mf12: "",
                mf12DS: "",
                mf12VDS: "",
                ccmNotes: "",
                ppaNotes: "",
                findings: ""
            }];

        var nonCashData = generateSLAMCIDollarSectionData("totalNonCash", "slamciDollarNonCash");

        var ncTotal = [{
                product: "<div class='totalRow'>&nbsp;</div>",
                cashier: "<div class='totalRow'><div class='alignRight'>TOTAL NON CASH</div></div>",
                mf4: "<div class='totalRow'><div class='numFormat'>" + nonCashData.cashierTotals.mf4Total + "</div></div>",
                mf4DS: "<div class='totalRow'>&nbsp;</div>",
                mf4VDS: "<div class='totalRow'>&nbsp;</div>",
                mf6: "<div class='totalRow'><div class='numFormat'>" + nonCashData.cashierTotals.mf6Total + "</div></div>",
                mf6DS: "<div class='totalRow'>&nbsp;</div>",
                mf6VDS: "<div class='totalRow'>&nbsp;</div>",
                mf21: "<div class='totalRow'><div class='numFormat'>" + nonCashData.cashierTotals.mf21Total + "</div></div>",
                mf21DS: "<div class='totalRow'>&nbsp;</div>",
                mf21VDS: "<div class='totalRow'>&nbsp;</div>",
                mf10: "<div class='totalRow'><div class='numFormat'>" + nonCashData.cashierTotals.mf10Total + "</div></div>",
                mf10DS: "<div class='totalRow'>&nbsp;</div>",
                mf10VDS: "<div class='totalRow'>&nbsp;</div>",
                mf11: "<div class='totalRow'><div class='numFormat'>" + nonCashData.cashierTotals.mf11Total + "</div></div>",
                mf11DS: "<div class='totalRow'>&nbsp;</div>",
                mf11VDS: "<div class='totalRow'>&nbsp;</div>",
                mf12: "<div class='totalRow'><div class='numFormat'>" + nonCashData.cashierTotals.mf12Total + "</div></div>",
                mf12DS: "<div class='totalRow'>&nbsp;</div>",
                mf12VDS: "<div class='totalRow'>&nbsp;</div>",
                ccmNotes: "<div class='totalRow'>&nbsp;</div>",
                ppaNotes: "<div class='totalRow'>&nbsp;</div>",
                findings: "<div class='totalRow'>&nbsp;</div>"
            }];

        var exemptionHeader = [{
                product: "<div class='alignLeft' cdTarget='slamciDollarExemption' onclick='collapseDivision(this)'>EXEMPTIONS</div>",
                cashier: "",
                mf4: "",
                mf4DSN: "",
                mf6: "",
                mf6DS: "",
                mf21: "",
                mf21DS: "",
                mf21VDS: "",
                mf10: "",
                mf10DS: "",
                mf10VDS: "",
                mf11: "",
                mf11DS: "",
                mf11VDS: "",
                mf12: "",
                mf12DS: "",
                mf12VDS: "",
                ccmNotes: "",
                ppaNotes: "",
                findings: ""
            }];

        var exemptionData = generateSLAMCIDollarSectionData("reversalProduct", "slamciDollarExemption");

        var exemptionTotal = [{
                product: "<div class='totalRow'>&nbsp;</div>",
                cashier: "<div class='totalRow'><div class='alignRight'>TOTAL EXEMPTIONS</div></div>",
                mf4: "<div class='totalRow'><div class='numFormat'>" + exemptionData.cashierTotals.mf4Total + "</div></div>",
                mf4DS: "<div class='totalRow'>&nbsp;</div>",
                mf4VDS: "<div class='totalRow'>&nbsp;</div>",
                mf6: "<div class='totalRow'><div class='numFormat'>" + exemptionData.cashierTotals.mf6Total + "</div></div>",
                mf6DS: "<div class='totalRow'>&nbsp;</div>",
                mf6VDS: "<div class='totalRow'>&nbsp;</div>",
                mf21: "<div class='totalRow'><div class='numFormat'>" + exemptionData.cashierTotals.mf21Total + "</div></div>",
                mf21DS: "<div class='totalRow'>&nbsp;</div>",
                mf21VDS: "<div class='totalRow'>&nbsp;</div>",
                mf10: "<div class='totalRow'><div class='numFormat'>" + exemptionData.cashierTotals.mf10Total + "</div></div>",
                mf10DS: "<div class='totalRow'>&nbsp;</div>",
                mf10VDS: "<div class='totalRow'>&nbsp;</div>",
                mf11: "<div class='totalRow'><div class='numFormat'>" + exemptionData.cashierTotals.mf11Total + "</div></div>",
                mf11DS: "<div class='totalRow'>&nbsp;</div>",
                mf11VDS: "<div class='totalRow'>&nbsp;</div>",
                mf12: "<div class='totalRow'><div class='numFormat'>" + exemptionData.cashierTotals.mf12Total + "</div></div>",
                mf12DS: "<div class='totalRow'>&nbsp;</div>",
                mf12VDS: "<div class='totalRow'>&nbsp;</div>",
                ccmNotes: "<div class='totalRow'>&nbsp;</div>",
                ppaNotes: "<div class='totalRow'>&nbsp;</div>",
                findings: "<div class='totalRow'>&nbsp;</div>"
            }];

        var totalSLAMCIDCashiersCollection = getSLAMCIDollarAreaTotal([cashNonCounterData, cashCounterData, dollarChequeData, nonCashData, checkNCData]);

        var cashierTotalHeader = [{
                product: "<div class='alignLeft' cdTarget='slamciDollarGTPC' onclick='collapseDivision(this)'>Grand Total Per Cashier</div>",
                cashier: "",
                mf4: "",
                mf4DSN: "",
                mf6: "",
                mf6DS: "",
                mf21: "",
                mf21DS: "",
                mf21VDS: "",
                mf10: "",
                mf10DS: "",
                mf10VDS: "",
                mf11: "",
                mf11DS: "",
                mf11VDS: "",
                mf12: "",
                mf12DS: "",
                mf12VDS: "",
                ccmNotes: "",
                ppaNotes: "",
                findings: ""
            }];

        var cashierTotal = getSLAMCIDGrandTotalPerCashier("slamciDollarGTPC");

        var cashierTotalRow = [{
                product: "<div class='totalRow'>&nbsp;</div>",
                cashier: "<div class='totalRow'><div class='alignRight'>TOTAL COLLECTION</div></div>",
                mf4: "<div class='totalRow'><div class='numFormat'>" + numberWithCommas(totalSLAMCIDCashiersCollection.mf4) + "</div></div>",
                mf4DS: "<div class='totalRow'>&nbsp;</div>",
                mf4VDS: "<div class='totalRow'>&nbsp;</div>",
                mf6: "<div class='totalRow'><div class='numFormat'>" + numberWithCommas(totalSLAMCIDCashiersCollection.mf6) + "</div></div>",
                mf6DS: "<div class='totalRow'>&nbsp;</div>",
                mf6VDS: "<div class='totalRow'>&nbsp;</div>",
                mf21: "<div class='totalRow'><div class='numFormat'>" + numberWithCommas(totalSLAMCIDCashiersCollection.mf21) + "</div></div>",
                mf21DS: "<div class='totalRow'>&nbsp;</div>",
                mf21VDS: "<div class='totalRow'>&nbsp;</div>",
                mf10: "<div class='totalRow'><div class='numFormat'>" + numberWithCommas(totalSLAMCIDCashiersCollection.mf10) + "</div></div>",
                mf10DS: "<div class='totalRow'>&nbsp;</div>",
                mf10VDS: "<div class='totalRow'>&nbsp;</div>",
                mf11: "<div class='totalRow'><div class='numFormat'>" + numberWithCommas(totalSLAMCIDCashiersCollection.mf11) + "</div></div>",
                mf11DS: "<div class='totalRow'>&nbsp;</div>",
                mf11VDS: "<div class='totalRow'>&nbsp;</div>",
                mf12: "<div class='totalRow'><div class='numFormat'>" + numberWithCommas(totalSLAMCIDCashiersCollection.mf12) + "</div></div>",
                mf12DS: "<div class='totalRow'>&nbsp;</div>",
                mf12VDS: "<div class='totalRow'>&nbsp;</div>",
                ccmNotes: "<div class='totalRow'>&nbsp;</div>",
                ppaNotes: "<div class='totalRow'>&nbsp;</div>",
                findings: "<div class='totalRow'>&nbsp;</div>"
            }];

        var slamciDollarSummaryData = [{
                product: "",
                cashier: "CONSOLIDATED GRAND TOTAL",
                mf4: "<div class='numFormat'>" + numberWithCommas(slamciDollarConsolidatedTotals.mf4) + "</div>",
                mf4DS: "<div class='totalRow'></div>",
                mf4VDS: "<div class='totalRow'></div>",
                mf6: "<div class='numFormat'>" + numberWithCommas(slamciDollarConsolidatedTotals.mf6) + "</div>",
                mf6DS: "<div class='totalRow'></div>",
                mf6VDS: "<div class='totalRow'></div>",
                mf21: "<div class='numFormat'>" + numberWithCommas(slamciDollarConsolidatedTotals.mf21) + "</div>",
                mf21DS: "<div class='totalRow'></div>",
                mf21VDS: "<div class='totalRow'></div>",
                mf10: "<div class='numFormat'>" + numberWithCommas(slamciDollarConsolidatedTotals.mf10) + "</div>",
                mf10DS: "<div class='totalRow'></div>",
                mf10VDS: "<div class='totalRow'></div>",
                mf11: "<div class='numFormat'>" + numberWithCommas(slamciDollarConsolidatedTotals.mf11) + "</div>",
                mf11DS: "<div class='totalRow'></div>",
                mf11VDS: "<div class='totalRow'></div>",
                mf12: "<div class='numFormat'>" + numberWithCommas(slamciDollarConsolidatedTotals.mf12) + "</div>",
                mf12DS: "<div class='totalRow'></div>",
                mf12VDS: "<div class='totalRow'></div>",
                ccmNotes: "",
                ppaNotes: "",
                findings: ""
            }, {
                product: "",
                cashier: "TOTAL CASHIERS COLLECTION",
                mf4: "<div class='numFormat'>" + numberWithCommas(totalSLAMCIDCashiersCollection.mf4) + "</div>",
                mf4DS: "",
                mf4VDS: "",
                mf6: "<div class='numFormat'>" + numberWithCommas(totalSLAMCIDCashiersCollection.mf6) + "</div>",
                mf6DS: "",
                mf6VDS: "<div class='totalRow'></div>",
                mf21: "<div class='numFormat'>" + numberWithCommas(totalSLAMCIDCashiersCollection.mf21) + "</div>",
                mf21DS: "",
                mf21VDS: "",
                mf10: "<div class='numFormat'>" + numberWithCommas(totalSLAMCIDCashiersCollection.mf10) + "</div>",
                mf10DS: "",
                mf10VDS: "",
                mf11: "<div class='numFormat'>" + numberWithCommas(totalSLAMCIDCashiersCollection.mf11) + "</div>",
                mf11DS: "",
                mf11VDS: "",
                mf12: "<div class='numFormat'>" + numberWithCommas(totalSLAMCIDCashiersCollection.mf12) + "</div>",
                mf12DS: "",
                mf12VDS: "",
                ccmNotes: "",
                ppaNotes: "",
                findings: ""
            }, {
                product: "",
                cashier: "",
                mf4: compareTotalsImg(totalSLAMCIDCashiersCollection.mf4, numberWithCommas(slamciDollarConsolidatedTotals.mf4)),
                mf4DS: "",
                mf4VDS: "",
                mf6: compareTotalsImg(totalSLAMCIDCashiersCollection.mf6, numberWithCommas(slamciDollarConsolidatedTotals.mf6)),
                mf6DS: "",
                mf6VDS: "<div class='totalRow'></div>",
                mf21: compareTotalsImg(totalSLAMCIDCashiersCollection.mf21, numberWithCommas(slamciDollarConsolidatedTotals.mf21)),
                mf21DS: "",
                mf21VDS: "",
                mf10: compareTotalsImg(totalSLAMCIDCashiersCollection.mf10, numberWithCommas(slamciDollarConsolidatedTotals.mf10)),
                mf10DS: "",
                mf10VDS: "",
                mf11: compareTotalsImg(totalSLAMCIDCashiersCollection.mf11, numberWithCommas(slamciDollarConsolidatedTotals.mf11)),
                mf11DS: "",
                mf11VDS: "",
                mf12: compareTotalsImg(totalSLAMCIDCashiersCollection.mf12, numberWithCommas(slamciDollarConsolidatedTotals.mf12)),
                mf12DS: "",
                mf12VDS: "",
                ccmNotes: "",
                ppaNotes: "",
                findings: ""
            }];

        new TableContainer({
            divName: slamciDollarSummaryDivDetails.divName,
            nodeName: slamciDollarSummaryDivDetails.nodeName,
            width: "1500px",
            id: slamciDollarSummaryDivDetails.tableId,
            layout: slamciDollarColumns,
            tableClass: "balancingToolSummary",
            data: slamciDollarSummaryData
        }).startUp();

        var slamciDollarSectionData = [];

        appendJsons([slamciDollarSectionData, dollarCashHeader, cashNonCounterData.prodTotals, cncTotal, cashCounterData.prodTotals, ccTotal, areaCashTotal, dollarCheckHeader, dollarChequeData.prodTotals, dcTotal, checkNCData.prodTotals, cNCTotal, areaCheckTotal, nonCashHeader, nonCashData.prodTotals, ncTotal, cashierTotalHeader, cashierTotal, cashierTotalRow, exemptionHeader, exemptionData.prodTotals, exemptionTotal, generateSnapshotSection(slamcidSnapshot)]);

        new TableContainer({
            divName: slamciDollarDivDetails.divName,
            nodeName: slamciDollarDivDetails.nodeName,
            width: "1500px",
            id: slamciDollarDivDetails.tableId,
            layout: slamciDollarColumnsNH,
            tableClass: "balancingToolSummary",
            data: slamciDollarSectionData
        }).startUp();

        var slamciDollarTableDiv = document.getElementById(slamciDollarTableDivId);

        slamciDollarTableDiv.style.display = "none";

    });
}


var slamciDollarSecData = [];

function mergeSLAMCIDollarCashierProductTypes() {
    var cashierRep = [];
    for (c in slamciDollarData) {
        //using jQuery.inArray instead of Array.indexOf because it is not supported in IE8 and below. thanks IE.
        if (jQuery.inArray(slamciDollarData[c].cashierId + "~" + slamciDollarData[c].product, cashierRep) == -1) {
            var pushedData = "";
            pushedData = pushedData.concat(slamciDollarData[c].cashierId);
            pushedData = pushedData.concat("~");
            pushedData = pushedData.concat(slamciDollarData[c].product);
            cashierRep.push(pushedData);
        }
    }

    for (var a = 0; a < cashierRep.length; a++) {
        slamciDollarSecData.push(mergeSLAMCIDollarCashierData(cashierRep[a]));
    }
}

function getSLAMCIDollarAreaTotal(area) {
    var tots = {};
    var mf4 = 0;
    var mf6 = 0;
    var mf21 = 0;
    var mf10 = 0;
    var mf11 = 0;
    var mf12 = 0;
    for (a in area) {
        mf4 = mf4 + parseFloat(area[a].cashierTotals.mf4Total.replace(/,/g, ""));
        mf6 = mf6 + parseFloat(area[a].cashierTotals.mf6Total.replace(/,/g, ""));
        mf21 = mf21 + parseFloat(area[a].cashierTotals.mf21Total.replace(/,/g, ""));
        mf10 = mf10 + parseFloat(area[a].cashierTotals.mf10Total.replace(/,/g, ""));
        mf11 = mf11 + parseFloat(area[a].cashierTotals.mf11Total.replace(/,/g, ""));
        mf12 = mf12 + parseFloat(area[a].cashierTotals.mf12Total.replace(/,/g, ""));
    }
    tots.mf4 = numberWithCommas(parseFloat(mf4).toFixed(2));
    tots.mf6 = numberWithCommas(parseFloat(mf6).toFixed(2));
    tots.mf21 = numberWithCommas(parseFloat(mf21).toFixed(2));
    tots.mf10 = numberWithCommas(parseFloat(mf10).toFixed(2));
    tots.mf11 = numberWithCommas(parseFloat(mf11).toFixed(2));
    tots.mf12 = numberWithCommas(parseFloat(mf12).toFixed(2));
    return tots;
}

function generateSLAMCIDollarSectionData(section, cdTarget) {
    var sectionObj = {};
    var sectionCashierTotals = {};
    var mf4 = 0;
    var mf6 = 0;
    var mf21 = 0;
    var mf10 = 0;
    var mf11 = 0;
    var mf12 = 0;
    var returnCNC = [];
    var firstRowFlag = true;
    for (a in slamciDollarSecData) {
        var tempCNC = {};
        if (slamciDollarSecData[a].product == section) {
            var isCashDepo = false;
            var isCheckDepo = false;
            if (firstRowFlag) {
                switch (section) {
                    case "totalCashNonCounter":
                        tempCNC.product = "<div class='alignLeft'>Non Counter</div>";
                        break;
                    case "totalCashCounter":
                        tempCNC.product = "<div class='alignLeft'>Counter</div>";
                        isCashDepo = true;
                        break;
                    case "totalUsCheckInManila":
                        tempCNC.product = "<div class='alignLeft'>US Cheque drawn in Manila</div>";
                        isCheckDepo = true;
                        checkType = "USCD_MANILA";
                        break;
                    case "totalUsCheckOutPh":
                        tempCNC.product = "<div class='alignLeft'>US Cheque drawn outside PH</div>";
                        isCheckDepo = true;
                        checkType = "USCD_OUTSIDEPH";
                        break;
                    case "totalDollarCheque":
                        tempCNC.product = "<div class='alignLeft'>Dollar Cheque</div>";
                        isCheckDepo = true;
                        checkType = "DOLLARCHECK";
                        break;
                    case "totalCheckNonCounter":
                        tempCNC.product = "<div class='alignLeft'>Cheque - Non Counter</div>";
                        break;
                    case "totalNonCash":
                        tempCNC.product = "";
                        break;
                    case "reversalProduct":
                        tempCNC.product = "";
                        break;
                    default:
                        alert("Unknown Product Total: " + section);
                        break;
                }
            } else {
                tempCNC.product = "";
                switch (section) {
                    case "totalCashCounter":
                        isCashDepo = true;
                        break;
                    case "totalUsCheckInManila":
                        isCheckDepo = true;
                        checkType = "USCD_MANILA";
                        break;
                    case "totalUsCheckOutPh":
                        isCheckDepo = true;
                        checkType = "USCD_OUTSIDEPH";
                        break;
                    case "totalDollarCheque":
                        isCheckDepo = true;
                        checkType = "DOLLARCHECK";
                        break;
                }
            }

            tempCNC.cashier = "<div class='alignLeft'>" + slamciDollarSecData[a].cashierName + "</div>";
            if (isSLAMCIDConfirmed(slamciDollarSecData[a].cashierId)) {
                if (section == "reversalProduct") {
                    if (parseFloat(slamciDollarSecData[a].mf4) != 0) {
                        tempCNC.mf4 = "<div class='alignRight'><a style='cursor:pointer;text-decoration:underline;color:blue;'onclick='openSPReversal(" + getBalancingToolProductId(slamciDollarSecData[a].cashierId, "MF4", section, slamciDollarData) + ");'>" + numberWithCommas(parseFloat(slamciDollarSecData[a].mf4).toFixed(2)) + "</a></div>";
                    } else {
                        tempCNC.mf4 = "<div class='alignRight'>" + numberWithCommas(parseFloat(slamciDollarSecData[a].mf4).toFixed(2)) + "</div>";
                    }

                    if (parseFloat(slamciDollarSecData[a].mf6) != 0) {
                        tempCNC.mf6 = "<div class='alignRight'><a style='cursor:pointer;text-decoration:underline;color:blue;'onclick='openSPReversal(" + getBalancingToolProductId(slamciDollarSecData[a].cashierId, "MF6", section, slamciDollarData) + ");'>" + numberWithCommas(parseFloat(slamciDollarSecData[a].mf6).toFixed(2)) + "</a></div>";
                    } else {
                        tempCNC.mf6 = "<div class='alignRight'>" + numberWithCommas(parseFloat(slamciDollarSecData[a].mf6).toFixed(2)) + "</div>";
                    }

                    if (parseFloat(slamciDollarSecData[a].mf21) != 0) {
                        tempCNC.mf21 = "<div class='alignRight'><a style='cursor:pointer;text-decoration:underline;color:blue;'onclick='openSPReversal(" + getBalancingToolProductId(slamciDollarSecData[a].cashierId, "MF21", section, slamciDollarData) + ");'>" + numberWithCommas(parseFloat(slamciDollarSecData[a].mf21).toFixed(2)) + "</a></div>";
                    } else {
                        tempCNC.mf21 = "<div class='alignRight'>" + numberWithCommas(parseFloat(slamciDollarSecData[a].mf21).toFixed(2)) + "</div>";
                    }

                    if (parseFloat(slamciDollarSecData[a].mf10) != 0) {
                        tempCNC.mf10 = "<div class='alignRight'><a style='cursor:pointer;text-decoration:underline;color:blue;'onclick='openSPReversal(" + getBalancingToolProductId(slamciDollarSecData[a].cashierId, "MF10", section, slamciDollarData) + ");'>" + numberWithCommas(parseFloat(slamciDollarSecData[a].mf10).toFixed(2)) + "</a></div>";
                    } else {
                        tempCNC.mf10 = "<div class='alignRight'>" + numberWithCommas(parseFloat(slamciDollarSecData[a].mf10).toFixed(2)) + "</div>";
                    }

                    if (parseFloat(slamciDollarSecData[a].mf11) != 0) {
                        tempCNC.mf11 = "<div class='alignRight'><a style='cursor:pointer;text-decoration:underline;color:blue;'onclick='openSPReversal(" + getBalancingToolProductId(slamciDollarSecData[a].cashierId, "MF11", section, slamciDollarData) + ");'>" + numberWithCommas(parseFloat(slamciDollarSecData[a].mf11).toFixed(2)) + "</a></div>";
                    } else {
                        tempCNC.mf11 = "<div class='alignRight'>" + numberWithCommas(parseFloat(slamciDollarSecData[a].mf11).toFixed(2)) + "</div>";
                    }
                    
                    if (parseFloat(slamciDollarSecData[a].mf12) != 0) {
                        tempCNC.mf12 = "<div class='alignRight'><a style='cursor:pointer;text-decoration:underline;color:blue;'onclick='openSPReversal(" + getBalancingToolProductId(slamciDollarSecData[a].cashierId, "MF12", section, slamciDollarData) + ");'>" + numberWithCommas(parseFloat(slamciDollarSecData[a].mf12).toFixed(2)) + "</a></div>";
                    } else {
                        tempCNC.mf12 = "<div class='alignRight'>" + numberWithCommas(parseFloat(slamciDollarSecData[a].mf12).toFixed(2)) + "</div>";
                    }
                } else {
                    tempCNC.mf4 = "<div class='alignRight'>" + numberWithCommas(parseFloat(slamciDollarSecData[a].mf4).toFixed(2)) + "</div>";
                    tempCNC.mf6 = "<div class='alignRight'>" + numberWithCommas(parseFloat(slamciDollarSecData[a].mf6).toFixed(2)) + "</div>";
                    tempCNC.mf21 = "<div class='alignRight'>" + numberWithCommas(parseFloat(slamciDollarSecData[a].mf21).toFixed(2)) + "</div>";
                    tempCNC.mf10 = "<div class='alignRight'>" + numberWithCommas(parseFloat(slamciDollarSecData[a].mf10).toFixed(2)) + "</div>";
                    tempCNC.mf11 = "<div class='alignRight'>" + numberWithCommas(parseFloat(slamciDollarSecData[a].mf11).toFixed(2)) + "</div>";
                    tempCNC.mf12 = "<div class='alignRight'>" + numberWithCommas(parseFloat(slamciDollarSecData[a].mf12).toFixed(2)) + "</div>";
                    var tempVal = "";
                    var tempVal2 = "";
                    if (isCashDepo) {
                        tempVal = getCashDepoURL("SLAMCID", "CASH", "USD", slamciDollarSecData[a].cashierId);
                        tempVal2 = getValidatedCashDepoURL("SLAMCID", "CASH", "USD", slamciDollarSecData[a].cashierId);
                    } else if (isCheckDepo) {
                        tempVal = getChequeDepoURL("SLAMCID", "CHEQUE", "USD", slamciDollarSecData[a].cashierId, checkType);
                        tempVal2 = getValidatedChequeDepoURL("SLAMCID", "CHEQUE", "USD", slamciDollarSecData[a].cashierId, checkType);
                    }
                    if (parseFloat(slamciDollarSecData[a].mf4) > 0) {
                        tempCNC.mf4DS = tempVal;
                        tempCNC.mf4VDS = tempVal2;
                    }
                    if (parseFloat(slamciDollarSecData[a].mf6) > 0) {
                        tempCNC.mf6DS = tempVal;
                        tempCNC.mf6VDS = tempVal2;
                    }
                    if (parseFloat(slamciDollarSecData[a].mf21) > 0) {
                        tempCNC.mf21DS = tempVal;
                        tempCNC.mf21VDS = tempVal2;
                    }
                    if (parseFloat(slamciDollarSecData[a].mf10) > 0) {
                        tempCNC.mf10DS = tempVal;
                        tempCNC.mf10VDS = tempVal2;
                    }
                    if (parseFloat(slamciDollarSecData[a].mf11) > 0) {
                        tempCNC.mf11DS = tempVal;
                        tempCNC.mf11VDS = tempVal2;
                    }
                    if (parseFloat(slamciDollarSecData[a].mf12) > 0) {
                        tempCNC.mf12DS = tempVal;
                        tempCNC.mf12VDS = tempVal2;
                    }
                }
                mf4 = mf4 + parseFloat(slamciDollarSecData[a].mf4);
                mf6 = mf6 + parseFloat(slamciDollarSecData[a].mf6);
                mf21 = mf21 + parseFloat(slamciDollarSecData[a].mf21);
                mf10 = mf10 + parseFloat(slamciDollarSecData[a].mf10);
                mf11 = mf11 + parseFloat(slamciDollarSecData[a].mf11);
                mf12 = mf12 + parseFloat(slamciDollarSecData[a].mf12);
            } else {
                tempCNC.mf4 = "<div class='alignRight'>-</div>";
                tempCNC.mf6 = "<div class='alignRight'>-</div>";
                tempCNC.mf21 = "<div class='alignRight'>-</div>";
                tempCNC.mf10 = "<div class='alignRight'>-</div>";
                tempCNC.mf11 = "<div class='alignRight'>-</div>";
                tempCNC.mf12 = "<div class='alignRight'>-</div>";
            }
            if (jQuery.inArray(section, excludeFromCashierTotal) == -1) {
                slamciDollarConsolidatedTotals.mf4 = (parseFloat(slamciDollarConsolidatedTotals.mf4) + parseFloat(slamciDollarSecData[a].mf4)).toFixed(2);
                slamciDollarConsolidatedTotals.mf6 = (parseFloat(slamciDollarConsolidatedTotals.mf6) + parseFloat(slamciDollarSecData[a].mf6)).toFixed(2);
                slamciDollarConsolidatedTotals.mf21 = (parseFloat(slamciDollarConsolidatedTotals.mf21) + parseFloat(slamciDollarSecData[a].mf21)).toFixed(2);
                slamciDollarConsolidatedTotals.mf10 = (parseFloat(slamciDollarConsolidatedTotals.mf10) + parseFloat(slamciDollarSecData[a].mf10)).toFixed(2);
                slamciDollarConsolidatedTotals.mf11 = (parseFloat(slamciDollarConsolidatedTotals.mf11) + parseFloat(slamciDollarSecData[a].mf11)).toFixed(2);
                slamciDollarConsolidatedTotals.mf12 = (parseFloat(slamciDollarConsolidatedTotals.mf12) + parseFloat(slamciDollarSecData[a].mf12)).toFixed(2);
            }
            tempCNC.ccmNotes = "<div class='alignCenter'>" + getNote(rowIndex, "CCM") + "</div>";
            tempCNC.ppaNotes = "<div class='alignCenter'>" + getNote(rowIndex, "PPA") + "</div>";
            tempCNC.findings = "<div class='alignCenter' collapseTarget='" + cdTarget + "'>" + getNote(rowIndex, "FINDINGS") + "</div>";
            rowIndex++;
            firstRowFlag = false;
            returnCNC.push(tempCNC);
        }
    }
    sectionCashierTotals.mf4Total = numberWithCommas(parseFloat(mf4).toFixed(2));
    sectionCashierTotals.mf6Total = numberWithCommas(parseFloat(mf6).toFixed(2));
    sectionCashierTotals.mf21Total = numberWithCommas(parseFloat(mf21).toFixed(2));
    sectionCashierTotals.mf10Total = numberWithCommas(parseFloat(mf10).toFixed(2));
    sectionCashierTotals.mf11Total = numberWithCommas(parseFloat(mf11).toFixed(2));
    sectionCashierTotals.mf12Total = numberWithCommas(parseFloat(mf12).toFixed(2));
    sectionObj.prodTotals = returnCNC;
    sectionObj.cashierTotals = sectionCashierTotals;
    return sectionObj;
}

function mergeSLAMCIDollarCashierData(cashierProduct) {
    var cpArr = cashierProduct.split("~");
    var cashierId = cpArr[0];
    var product = cpArr[1];
    var tempCashier = {};
    for (a in slamciDollarData) {
        if (slamciDollarData[a].cashierId == cashierId && slamciDollarData[a].product == product) {
            if (slamciDollarData[a].productCode == "MF4") {
                tempCashier.mf4 = slamciDollarData[a].total;
            } else if (slamciDollarData[a].productCode == "MF6") {
                tempCashier.mf6 = slamciDollarData[a].total;
            } else if (slamciDollarData[a].productCode == "MF21") {
                tempCashier.mf21 = slamciDollarData[a].total;
            } else if (slamciDollarData[a].productCode == "MF10") {
                tempCashier.mf10 = slamciDollarData[a].total;
            } else if (slamciDollarData[a].productCode == "MF11") {
                tempCashier.mf11 = slamciDollarData[a].total;
            } else if (slamciDollarData[a].productCode == "MF12") {
                tempCashier.mf12 = slamciDollarData[a].total;
            }
            tempCashier.cashierId = slamciDollarData[a].cashierId;
            tempCashier.cashierName = slamciDollarData[a].cashierName;
            tempCashier.product = product;
        }
    }
    return tempCashier;
}

function getSLAMCIDGrandTotalPerCashier(cdTarget) {
    var returnSection = [];
    var c = 0;
    for (c in cashierList) {
        var mf4 = 0;
        var mf6 = 0;
        var mf21 = 0;
        var mf10 = 0;
        var mf11 = 0;
        var mf12 = 0;
        var d = 0;
        for (d in slamciDollarData) {
            if (jQuery.inArray(slamciDollarData[d].product, excludeFromCashierTotal) == -1 && slamciDollarData[d].cashierId.toUpperCase() == cashierList[c].id.toUpperCase()) {
                switch (slamciDollarData[d].productCode) {
                    case "MF4":
                        mf4 = mf4 + parseFloat(slamciDollarData[d].total);
                        break;
                    case "MF6":
                        mf6 = mf6 + parseFloat(slamciDollarData[d].total);
                        break;
                    case "MF21":
                        mf21 = mf21 + parseFloat(slamciDollarData[d].total);
                        break;
                    case "MF10":
                        mf10 = mf10 + parseFloat(slamciDollarData[d].total);
                        break;
                    case "MF11":
                        mf11 = mf11 + parseFloat(slamciDollarData[d].total);
                        break;
                    case "MF12":
                        mf12 = mf12 + parseFloat(slamciDollarData[d].total);
                        break;
                }
            }
        }

        var tempCashier = {};
        tempCashier.cashier = "<div class='alignLeft'  collapseTarget='" + cdTarget + "'>" + cashierList[c].name + "</div>";
        if (isSLAMCIDConfirmed(cashierList[c].id)) {
            tempCashier.mf4 = "<div class='alignRight'>" + numberWithCommas(mf4.toFixed(2)) + "</div>";
            tempCashier.mf6 = "<div class='alignRight'>" + numberWithCommas(mf6.toFixed(2)) + "</div>";
            tempCashier.mf21 = "<div class='alignRight'>" + numberWithCommas(mf21.toFixed(2)) + "</div>";
            tempCashier.mf10 = "<div class='alignRight'>" + numberWithCommas(mf10.toFixed(2)) + "</div>";
            tempCashier.mf11 = "<div class='alignRight'>" + numberWithCommas(mf11.toFixed(2)) + "</div>";
            tempCashier.mf12 = "<div class='alignRight'>" + numberWithCommas(mf12.toFixed(2)) + "</div>";
        } else {
            tempCashier.mf4 = "<div class='alignCenter'>-</div>";
            tempCashier.mf6 = "<div class='alignCenter'>-</div>";
            tempCashier.mf21 = "<div class='alignCenter'>-</div>";
            tempCashier.mf10 = "<div class='alignCenter'>-</div>";
            tempCashier.mf11 = "<div class='alignCenter'>-</div>";
            tempCashier.mf12 = "<div class='alignCenter'>-</div>";
        }
        returnSection.push(tempCashier);
    }
    return returnSection;
}

var slamciDollarConsolidatedTotals = {
    mf4: "0.00",
    mf6: "0.00",
    mf21: "0.00",
    mf10: "0.00",
    mf11: "0.00",
    mf12: "0.00"
};
