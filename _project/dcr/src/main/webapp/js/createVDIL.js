function saveAndFormatDecimal(obj){
	obj.value = obj.value.replace(/,/g,"");//remove commas
	
	if(!isNaN(obj.value) && obj.value.indexOf(" ") == -1 && obj.value.length > 0){
		obj.value = numberWithCommas(parseFloat(obj.value).toFixed(2));
	} else {
		alert("Not a number");
		obj.value = "0.00";
		obj.focus();
	}
}

function saveAndFormatNumber(obj){
	if(!isNaN(obj.value) && obj.value.indexOf(" ") == -1 && obj.value.length > 0){
		obj.value = parseInt(parseFloat(obj.value));//remove decimal .00, etc
	} else {
		alert("Not a number");
		obj.value = "0";
		obj.focus();
	}
}

function computeDollarCoinTotal(){
	//rcvdPrevDay
	//rcvdToday
	//lessChangeForTheDay
	//--------------------
	//totalDollarCoins
	var prev = document.getElementById('rcvdPrevDay');
	var today = document.getElementById('rcvdToday');
	var change = document.getElementById('lessChangeForTheDay');
	var totalValue = parseFloat(prev.value) + parseFloat(today.value) - parseFloat(change.value);
	//alert('totalValue: '+totalValue);
	var totalLabel = document.getElementById('totalDollarCoins');
	//var totalHidden = document.getElementById('totalDollarCoinsHidden');
	
	//TODO: replace tag type of id="totalDollarCoins"
	//totalLabel.innerHTML = totalValue;//NOT WORKING, but reflected after onSave
	totalLabel.value = numberWithCommas(parseFloat(totalValue).toFixed(2));//WORKING, onchange, now totalLabel is a readonly textfield
	
	//totalHidden.value = totalValue;
	//alert('totalHidden.value: '+totalHidden.value);
}

function computeWarehouseTotal(){
	var vultrad = document.getElementById('vultradPDCRPendingWarehouse');
	var pn = document.getElementById('pnPDCPendingWarehouse');
	var warehouseTotal = document.getElementById('warehouseTotal');
	warehouseTotal.value = parseInt(vultrad.value) + parseInt(pn.value);
}

function previewVDILPDF(){
	var form = document.getElementById("vdilForm");
	var formActionVal = form.action;
	var methodName = formActionVal.substring(formActionVal.lastIndexOf("/")+1, formActionVal.search(".html"));
	form.action = formActionVal.replace(methodName,"vdilPreview");
	form.target="_blank";
	form.submit();
}

function saveVDIL(){
	//pass temp values to hidden values
	//why hidden and not direct input type text with name="sample"?
	//because the div for jquery dialog separates itself from its mother container, which is the <form> tag...
	//so the form cant get the name="sample"
	//alert('pass temp to hidden');
	document.getElementById('in_denom1').value = document.getElementById('temp_in_denom1').value;
	document.getElementById('out_denom1').value = document.getElementById('temp_out_denom1').value;
	
	document.getElementById('in_denom50c').value = document.getElementById('temp_in_denom50c').value;
	document.getElementById('out_denom50c').value = document.getElementById('temp_out_denom50c').value;
	
	document.getElementById('in_denom25c').value = document.getElementById('temp_in_denom25c').value;
	document.getElementById('out_denom25c').value = document.getElementById('temp_out_denom25c').value;
	
	document.getElementById('in_denom10c').value = document.getElementById('temp_in_denom10c').value;
	document.getElementById('out_denom10c').value = document.getElementById('temp_out_denom10c').value;
	
	document.getElementById('in_denom5c').value = document.getElementById('temp_in_denom5c').value;
	document.getElementById('out_denom5c').value = document.getElementById('temp_out_denom5c').value;
	
	document.getElementById('in_denom1c').value = document.getElementById('temp_in_denom1c').value;
	document.getElementById('out_denom1c').value = document.getElementById('temp_out_denom1c').value;
	
	var form = document.getElementById("vdilForm");
	var formActionVal = form.action;
	var methodName = formActionVal.substring(formActionVal.lastIndexOf("/")+1, formActionVal.search(".html"));
	form.action = formActionVal.replace(methodName,"vdilSave");
	form.target="";
	form.submit();
}

function showPrevDayRcvdGrid(){
	//alert('showPrevDayRcvdGrid');
		$(function() {
			$( "#rcvdPrevDayGridDiv" ).dialog({
				autoOpen: true,
				height: 440,
				width: 400,
				modal: false,
				closeOnEscape: true,
				draggable: false,
				resizable: false,
				dialogClass: "rcvdPrevDayGridClass",
				title: "Dollar Coin Collection From Previous Day"
			}).dialog('widget').position({ my: 'right+40 center-100', at: 'center', of: $(document.getElementById("mainTemplateBodyId")) });
		});
}
function showTodayRcvdForm(){
	//alert('showTodayRcvdForm');
		$(function() {
			$( "#rcvdTodayFormDiv" ).dialog({
				autoOpen: true,
				height: 455,
				width: 705,
				modal: false,
				closeOnEscape: true,
				draggable: false,
				resizable: false,
				dialogClass: "rcvdTodayFormClass",
				title: "Dollar Coin Collection Exchange Today"
			}).dialog('widget').position({ my: 'right+40 center+150', at: 'center', of: $(document.getElementById("mainTemplateBodyId")) });
			//^set position under the first one
		});
}
function calculateBalance(rowNum){
	var rcvdTodayTable = document.getElementById("rcvdTodayTable");
	
	var denominationFactor = parseFloat(rcvdTodayTable.rows[rowNum].cells[0].innerHTML);
	
	var pcsNetPrev = parseInt(rcvdTodayTable.rows[rowNum].cells[1].innerHTML);
	var pcsRcvdToday = parseInt(rcvdTodayTable.rows[rowNum].cells[2].children[0].value);
	var pcsChangedToday = parseInt(rcvdTodayTable.rows[rowNum].cells[4].children[0].value);//orig is 3
	
	var netPcs = parseFloat(pcsNetPrev + pcsRcvdToday - pcsChangedToday);
	var amtRcvdToday = denominationFactor * pcsRcvdToday;
	var amtChangedToday = denominationFactor * pcsChangedToday;
	var amount = denominationFactor * netPcs;
	
	rcvdTodayTable.rows[rowNum].cells[3].innerHTML = numberWithCommas(parseFloat(amtRcvdToday).toFixed(2));//new
	rcvdTodayTable.rows[rowNum].cells[5].innerHTML = numberWithCommas(parseFloat(amtChangedToday).toFixed(2));//new
	rcvdTodayTable.rows[rowNum].cells[6].innerHTML = netPcs;//orig is 4
	rcvdTodayTable.rows[rowNum].cells[7].innerHTML = numberWithCommas(parseFloat(amount).toFixed(2));//orig is 5
	
	var totalAmtRcvd = 0.00;
	var totalAmtChanged = 0.00;
	//var netPcs = 0;
	var netAmt = 0.00;
	for (var rowCtr=1;rowCtr<7;rowCtr++){
		totalAmtRcvd = totalAmtRcvd + parseFloat(rcvdTodayTable.rows[rowCtr].cells[3].innerHTML.replace(/,/g,""));
		totalAmtChanged = totalAmtChanged + parseFloat(rcvdTodayTable.rows[rowCtr].cells[5].innerHTML.replace(/,/g,""));
		//netPcs = netPcs + parseInt(rcvdTodayTable.rows[rowCtr].cells[6].innerHTML);
		netAmt = netAmt + parseFloat(rcvdTodayTable.rows[rowCtr].cells[7].innerHTML.replace(/,/g,""));
	}
	//last rows
	rcvdTodayTable.rows[7].cells[3].innerHTML = numberWithCommas(parseFloat(totalAmtRcvd).toFixed(2));
	rcvdTodayTable.rows[7].cells[5].innerHTML = numberWithCommas(parseFloat(totalAmtChanged).toFixed(2));
	//rcvdTodayTable.rows[7].cells[6].innerHTML = netPcs;//orig is 4
	rcvdTodayTable.rows[7].cells[7].innerHTML = numberWithCommas(parseFloat(netAmt).toFixed(2));//orig is 5
	
	document.getElementById("rcvdToday").value = numberWithCommas(parseFloat(totalAmtRcvd).toFixed(2));
	document.getElementById("lessChangeForTheDay").value = numberWithCommas(parseFloat(totalAmtChanged).toFixed(2));
	
	document.getElementById("totalDollarCoins").value = numberWithCommas(parseFloat(netAmt).toFixed(2));
}