// FOR PPA MDS
var searchTableData = [];
var mdsReconciledList = [];
var mdsUnreconciledList = [];
var commentList = [];
var showTable = false;
var itemsFound = 0;
var existingCommentList = [];
// Added for MR-WF-16-00031 - PPA CheckBox
var mdsTotalCB = 0;
var mdsActiveCB = 0;
// Added for MR-WF-16-00034 - Random sampling for QA
var isPpaQa;

function Comment(id, notes, findings) {
    this.comId = id;
    this.comNotes = notes;
    this.comFindings = findings;
}

function enableCardNumber(){
	
	var approvalCode = document.getElementById("approvalCode");
	var cardNumber = document.getElementById("cardNumber");
	var salesSlipNumber = document.getElementById("salesSlipNumber");
	
	cardNumber.disabled = false;
	approvalCode.value = "";
	approvalCode.disabled = true;
	salesSlipNumber.value = "";
	salesSlipNumber.disabled = true;
	
}

function enableSalesSlipNumber(){
	
	var approvalCode = document.getElementById("approvalCode");
	var cardNumber = document.getElementById("cardNumber");
	var salesSlipNumber = document.getElementById("salesSlipNumber");
	
	salesSlipNumber.disabled = false;
	cardNumber.value = "";
	cardNumber.disabled = true;
	approvalCode.value = "";
	approvalCode.disabled = true;
	
}

function enableApprovalCode(){
	
	var approvalCode = document.getElementById("approvalCode");
	var cardNumber = document.getElementById("cardNumber");
	var salesSlipNumber = document.getElementById("salesSlipNumber");
	
	approvalCode.disabled = false;
	cardNumber.value = "";
	cardNumber.disabled = true;
	salesSlipNumber.value = "";
	salesSlipNumber.disabled = true;
	
}

function clearDate(){
	var searchDatePicker = document.getElementById("searchDatePicker");
	var dcrDate = document.getElementById("dcrDateStr");
	dcrDate.value = "";
	searchDatePicker.value = "";
}


function disableHubId(){
	var hubId = document.getElementById("hubId");
	var searchByHub = document.getElementById("searchByHub");
	searchByHub.value = "false";
	hubId.disabled = true;
	var ccId = document.getElementById("ccId");
	var searchByCC = document.getElementById("searchByCC");
	searchByCC.value = "true";
	ccId.disabled = false;
}

function disableCcId(){
	var ccId = document.getElementById("ccId");
	var searchByCC = document.getElementById("searchByCC");
	searchByCC.value = "false";
	ccId.disabled = true;
	var hubId = document.getElementById("hubId");
	var searchByHub = document.getElementById("searchByHub");
	searchByHub.value = "true";
	hubId.disabled = false;
}

// Modified for MR-WF-16-00031 - PPA CheckBox
function showResultsTable(searchData) {
    $(document).ready(function() {
        // Ensure that table is clear before populating data
        destroySearchTable();
        var searchTableLayout = [{
                name: "Sales Slip Number",
                field: "salesSlipNumber",
                width: "14%"
            }, {
                name: "Approval Code",
                field: "approvalCode",
                width: "11%"
            }, {
                name: "Card Number",
                field: "cardNumber",
                width: "11%"
            }, {
                name: "Currency",
                field: "currency",
                width: "5%"
            }, {
                name: "Amount",
                field: "amount",
                width: "9%"
            }, {
                name: "User Id",
                field: "userId",
                width: "6%"
            }, {
                name: "Product Code",
                field: "productCode",
                width: "5%"
            }, {
                name: "Company",
                field: "company",
                width: "5%"
            }, {
                name: "Payment Type",
                field: "paymentType",
                width: "5%"
            }, {
                name: "Reconciled",
                field: "reconciled",
                width: "5%"
            }, {
                name: "Recon By",
                field: "reconBy",
                width: "8%"
            }, {
                name: "Recon Date",
                field: "reconDate",
                width: "9%"
            }, {
                name: "Notes",
                field: "notes",
                width: "7%"
            }, {
                name: "Findings",
                field: "findings",
                width: "5%"
            }];

        new TableContainer({
            divName: "searchResultDiv",
            nodeName: "searchResultTable",
            width: "100%",
            id: "searchResults",
            layout: searchTableLayout,
            tableClass: "searchResultsTable",
            data: searchData
        }).startUp();

        var searchTable = document.getElementById("searchResults");

        var options = {
            optionsForRows: [5, 10, 20, 30, 50],
            rowsPerPage: 10,
            firstArrow: (new Image()).src = "../images/firstBlue.gif",
            prevArrow: (new Image()).src = "../images/prevBlue.gif",
            lastArrow: (new Image()).src = "../images/lastBlue.gif",
            nextArrow: (new Image()).src = "../images/nextBlue.gif",
            topNav: false
        };
        sorttable.makeSortable(searchTable);
        $('#searchResults').tablePagination(options);
        $("#itemsFound").text('Items Found: ' + searchData.length);
        // Set checkBox count
        $("#mdsActiveCB").val(mdsActiveCB);
        $("#mdsTotalCB").val(mdsTotalCB);
        $("#mdsCountDisp").text(mdsActiveCB + "/" + mdsTotalCB);
        
        // Added for MR-WF-16-00034 - Random sampling for QA
        $(".ppaMdsChkBox").attr("disabled", isPpaQa);
        $("input[name=save]").attr("disabled", isPpaQa);
    });

}

function destroySearchTable(){
	$("#searchResultTable").empty();
}

function showSearchResultDiv(){
	var searchResultDiv = document.getElementById("searchResultDiv");
	searchResultDiv.style.visibility = 'visible';
}

function submitForm(){
	var searchForm = document.getElementById("searchForm");
	if(validateForm()){		
		searchForm.submit();
	}
}

function validateForm(){
	var validated = true;
	var ccId = document.getElementById("ccId");
	var dcrDate = document.getElementById("dcrDateStr");
	if(ccId.selectedIndex < 0){
		alert("Please choose a site code");
		return false;
	}
	if(dcrDate.value == null || dcrDate.value.length == 0){
		dcrDate.value = "allManagerDcr";
	}
	return validated;
	
}

function setDcrDateStr(val){
	var dcrDateStr = document.getElementById("dcrDateStr");
	dcrDateStr.value = val;
}

function filterNow(){
	var rbApprovalCode = document.getElementById("rbApprovalCode");
	var rbCardNumber = document.getElementById("rbCardNumber");
	var rbSalesSlipNumber = document.getElementById("rbSalesSlipNumber");
	
	var approvalCode = document.getElementById("approvalCode");
	var cardNumber = document.getElementById("cardNumber");
	var salesSlipNumber = document.getElementById("salesSlipNumber");
	
	var approvalCodeKey = "approvalCode";
	var cardNumberKey = "cardNumber";
	var salesSlipNumberKey = "salesSlipNumber";
	
	if(rbApprovalCode.checked){
		filterTable(approvalCodeKey,approvalCode.value);
	} else if(rbCardNumber.checked){
		filterTable(cardNumberKey,cardNumber.value);
	} else if(rbSalesSlipNumber.checked){
		filterTable(salesSlipNumberKey,salesSlipNumber.value);
	}
}

function cleanLists(){
	for(mdsUnreconCnt in mdsUnreconciledList){
		delete mdsUnreconciledList[mdsUnreconCnt];
	}
	for(mdsReconCnt in mdsReconciledList){
		delete mdsReconciledList[mdsReconCnt];
	}
}

function clearSearch(){
	document.getElementById("approvalCode").value = '';
	document.getElementById("cardNumber").value = '';
	document.getElementById("salesSlipNumber").value = '';
	showResultsTable(searchTableData);
}

// Modified for MR-WF-16-00031 - PPA CheckBox
function getCheckbox(isReconciled, dcrId, salesSlipNumber, approvalNumber, accountNumber) {
    mdsTotalCB = addCount(mdsTotalCB);
    if (isReconciled === 'Y') {
        // Set initial value of checkedCB
        mdsActiveCB = addCount(mdsActiveCB);
        $("#mdsActiveCB").val(mdsActiveCB);
        return "<center><input class='ppaMdsChkBox' DISABLED type='checkbox' checked='checked' dcrId='" + dcrId + "' ssNo='" + salesSlipNumber + "' appNo='" + approvalNumber + "' accNo='" + accountNumber + "' onclick='handleReconCheck(this)'/></center>";
    } else if (isReconciled === 'N') {
        return "<center><div class='invisible'>n</div><input class='ppaMdsChkBox' type='checkbox' dcrId='" + dcrId + "' ssNo='" + salesSlipNumber + "' appNo='" + approvalNumber + "' accNo='" + accountNumber + "' onclick='handleReconCheck(this)'/></center>";
    }
}

// Modified for MR-WF-16-00031 - PPA CheckBox
function handleReconCheck(cb) {
    var mdsCurCount = 0;
    if (cb.checked) {
        // Will refesh MDS CB count and display
        mdsCurCount = addCount($("#mdsActiveCB").val());
        $("#mdsActiveCB").val(mdsCurCount);
        $("#mdsCountDisp").text(mdsCurCount + "/" + mdsTotalCB);
        
        var tempMDS = {};
        tempMDS.dcrId = cb.dcrId;
        tempMDS.salesSlipNumber = cb.ssNo;
        tempMDS.approvalCode = cb.appNo;
        tempMDS.cardNumber = cb.accNo;
        tempMDS.reconciled = 'Y';
        mdsReconciledList.push(tempMDS);

        for (mdsUnreconCnt in mdsUnreconciledList) {
            if (mdsUnreconciledList[mdsUnreconCnt].dcrId === tempMDS.dcrId && mdsUnreconciledList[mdsUnreconCnt].salesSlipNumber === tempMDS.salesSlipNumber) {
                delete mdsUnreconciledList[mdsUnreconCnt];
            }
        }
    } else {//TODO gawa rin ng unreconlist for unchecked
        // Will refesh MDS CB count and display
        mdsCurCount = subtractCount($("#mdsActiveCB").val());
        $("#mdsActiveCB").val(mdsCurCount);
        $("#mdsCountDisp").text(mdsCurCount + "/" + mdsTotalCB);
        
        for (mdsReconCnt in mdsReconciledList) {//assuming dcrId plus salesSlipNumber is unique
            if (mdsReconciledList[mdsReconCnt].dcrId === cb.dcrId && mdsReconciledList[mdsReconCnt].salesSlipNumber === cb.ssNo) {
                mdsUnreconciledList.push(mdsReconciledList[mdsReconCnt]);
                delete mdsReconciledList[mdsReconCnt];
            }
        }
    }
}

function saveMDSRecon(){
	//alert('saveMDSRecon');
	
	var hiddenForm = document.getElementById("hiddenForm");
	for(mdsReconCnt in mdsReconciledList){
			//alert('meron sa reconciled');
			var hiddenDcrId = document.createElement("input");
			hiddenDcrId.type = 'hidden';
			hiddenDcrId.name = 'ppamdsRecons['+mdsReconCnt+'].dcrId';
			hiddenDcrId.value = mdsReconciledList[mdsReconCnt].dcrId;
			
			var hiddenSalesSlipNumber = document.createElement("input");
			hiddenSalesSlipNumber.type = 'hidden';
			hiddenSalesSlipNumber.name = 'ppamdsRecons['+mdsReconCnt+'].salesSlipNumber';
			hiddenSalesSlipNumber.value = mdsReconciledList[mdsReconCnt].salesSlipNumber;
			
			var hiddenAccountNumber = document.createElement("input");
			hiddenAccountNumber.type = 'hidden';
			hiddenAccountNumber.name = 'ppamdsRecons['+mdsReconCnt+'].accountNumber';
			hiddenAccountNumber.value = mdsReconciledList[mdsReconCnt].cardNumber;
			
			var hiddenApprovalNumber = document.createElement("input");
			hiddenApprovalNumber.type = 'hidden';
			hiddenApprovalNumber.name = 'ppamdsRecons['+mdsReconCnt+'].approvalNumber';
			hiddenApprovalNumber.value = mdsReconciledList[mdsReconCnt].approvalCode;
			
			var hiddenReconciled = document.createElement("input");
			hiddenReconciled.type = 'hidden';
			hiddenReconciled.name = 'ppamdsRecons['+mdsReconCnt+'].reconciled';
			hiddenReconciled.value = mdsReconciledList[mdsReconCnt].reconciled;
			
			var hiddenNotes = document.createElement("input");
			hiddenNotes.type = 'hidden';
			hiddenNotes.name = 'ppamdsRecons['+mdsReconCnt+'].ppaNotes';
			hiddenNotes.value = getNoteValue(mdsReconciledList[mdsReconCnt].dcrId+mdsReconciledList[mdsReconCnt].salesSlipNumber,"notes");
			
			var hiddenFindings = document.createElement("input");
			hiddenFindings.type = 'hidden';
			hiddenFindings.name = 'ppamdsRecons['+mdsReconCnt+'].ppaFindings';
			hiddenFindings.value = getNoteValue(mdsReconciledList[mdsReconCnt].dcrId+mdsReconciledList[mdsReconCnt].salesSlipNumber,"findings");
			
			hiddenForm.appendChild(hiddenDcrId);
			hiddenForm.appendChild(hiddenSalesSlipNumber);
			hiddenForm.appendChild(hiddenAccountNumber);
			hiddenForm.appendChild(hiddenApprovalNumber);
			hiddenForm.appendChild(hiddenReconciled);
			hiddenForm.appendChild(hiddenNotes);
			hiddenForm.appendChild(hiddenFindings);
	}
	
	for(mdsUnreconCnt in mdsUnreconciledList){
			//alert('MERON SA UNRECONCILED');
			var hiddenDcrId = document.createElement("input");
			hiddenDcrId.type = 'hidden';
			hiddenDcrId.name = 'ppamdsUnrecons['+mdsUnreconCnt+'].dcrId';
			hiddenDcrId.value = mdsUnreconciledList[mdsUnreconCnt].dcrId;
			
			var hiddenSalesSlipNumber = document.createElement("input");
			hiddenSalesSlipNumber.type = 'hidden';
			hiddenSalesSlipNumber.name = 'ppamdsUnrecons['+mdsUnreconCnt+'].salesSlipNumber';
			hiddenSalesSlipNumber.value = mdsUnreconciledList[mdsUnreconCnt].salesSlipNumber;
			
			var hiddenAccountNumber = document.createElement("input");
			hiddenAccountNumber.type = 'hidden';
			hiddenAccountNumber.name = 'ppamdsUnrecons['+mdsUnreconCnt+'].accountNumber';
			hiddenAccountNumber.value = mdsUnreconciledList[mdsUnreconCnt].cardNumber;
			
			var hiddenApprovalNumber = document.createElement("input");
			hiddenApprovalNumber.type = 'hidden';
			hiddenApprovalNumber.name = 'ppamdsUnrecons['+mdsUnreconCnt+'].approvalNumber';
			hiddenApprovalNumber.value = mdsUnreconciledList[mdsUnreconCnt].approvalCode;
			
			var hiddenReconciled = document.createElement("input");
			hiddenReconciled.type = 'hidden';
			hiddenReconciled.name = 'ppamdsUnrecons['+mdsUnreconCnt+'].reconciled';
			hiddenReconciled.value = mdsUnreconciledList[mdsUnreconCnt].reconciled;
			
			var hiddenNotes = document.createElement("input");
			hiddenNotes.type = 'hidden';
			hiddenNotes.name = 'ppamdsUnrecons['+mdsUnreconCnt+'].ppaNotes';
			hiddenNotes.value = getNoteValue(mdsUnreconciledList[mdsUnreconCnt].dcrId+mdsUnreconciledList[mdsUnreconCnt].salesSlipNumber,"notes");
			
			var hiddenFindings = document.createElement("input");
			hiddenFindings.type = 'hidden';
			hiddenFindings.name = 'ppamdsUnrecons['+mdsUnreconCnt+'].ppaFindings';
			hiddenFindings.value = getNoteValue(mdsUnreconciledList[mdsUnreconCnt].dcrId+mdsUnreconciledList[mdsUnreconCnt].salesSlipNumber,"findings");
			
			hiddenForm.appendChild(hiddenDcrId);
			hiddenForm.appendChild(hiddenSalesSlipNumber);
			hiddenForm.appendChild(hiddenAccountNumber);
			hiddenForm.appendChild(hiddenApprovalNumber);
			hiddenForm.appendChild(hiddenReconciled);
			hiddenForm.appendChild(hiddenNotes);
			hiddenForm.appendChild(hiddenFindings);
	}
	
	hiddenForm.submit();
	
}

function filterTable(field,val){
	var bufferData=[];
	var rowFind = false;
	//var patt1 = new RegExp(val, "g");
	var patt1 = new RegExp(val);
	switch(field){
	case "approvalCode": 
		for(b in searchTableData){
			if(patt1.test(searchTableData[b].approvalCode)){
				bufferData.push(searchTableData[b]);
				rowFind = true;
			}
		}
		break;
	case "cardNumber": 
		for(b in searchTableData){
			if(patt1.test(searchTableData[b].cardNumber)){
				bufferData.push(searchTableData[b]);	
				rowFind = true;
			}
		}
		break;
	case "salesSlipNumber": 
		for(b in searchTableData){
			if(patt1.test(searchTableData[b].salesSlipNumber)){
				bufferData.push(searchTableData[b]);	
				rowFind = true;
			}
		}
		break;
	}
	if(!rowFind){
		alert("No Data Found");
	} else{
		showResultsTable(bufferData);
	}
}

function getNotes(val, type) {
    for (c in commentList) {
        if (commentList[c].comId == val) {
            if (type == "notes") {
                // Modified for MR-WF-16-00034 - Random sampling for QA
                if ("true" !== $("#isPpaQa").val()) {
                    if (commentList[c].comNotes != null && commentList[c].comNotes != '') {
                        return '<center><a href="#" id="notesId' + val + type + '" rowInd="' + val + '" noteType="notes" onclick="showNoteForm(this)">View</a></center>';
                    } else {
                        return '<center><a href="#" id="notesId' + val + type + '" rowInd="' + val + '" noteType="notes" onclick="showNoteForm(this)">Add</a></center>';
                    }
                }
            } else if (type == "findings") {
                // Modified for MR-WF-16-00034 - Random sampling for QA
                if ("true" !== $("#isPpaQa").val()) {
                    if (commentList[c].comFindings != null && commentList[c].comFindings != '') {
                        return '<center><a href="#" id="notesId' + val + type + '" rowInd="' + val + '" noteType="findings" onclick="showNoteForm(this)">View</a></center>';
                    } else {
                        return '<center><a href="#" id="notesId' + val + type + '" rowInd="' + val + '" noteType="findings" onclick="showNoteForm(this)">Add</a></center>';
                    }
                }
            }
        }
    }

    return "";
}

function showNoteForm(val){
	document.getElementById("mdsNoteForm").reset();
	var isContentToBeEdited = false;
	var noteCntToDeleteIfEdited;
	for(commentCnt in commentList){
		if(commentList[commentCnt].comId == val.rowInd){
			if(val.noteType == "notes"){
				document.getElementById("mdsNoteContent").value = commentList[commentCnt].comNotes;				
			} else if(val.noteType == "findings"){
				document.getElementById("mdsNoteContent").value = commentList[commentCnt].comFindings;
			}
			noteCntToDeleteIfEdited = commentCnt;
			break;
		}
	}
	
	$(function() {
	$( "#mdsNoteDiv" ).dialog({
		autoOpen: true,
		height: "auto",
		minHeight: 580,
		width: 540,
		modal: true,
		closeOnEscape: true,
		draggable: false,
		resizable: false,
		dialogClass: "noteFormClass",
		title: "Add/View Note",
	    buttons: [
	              {
	                  id: 'addMdsNoteBtn',
	                  text: 'Add',
	                  click: function() {
	                	  	var contentEntered = document.getElementById("mdsNoteContent").value;
	                	    var currentLink = document.getElementById(val.id);
	                	    //alert('content entered: '+contentEntered);
	                	    //alert('current link label: '+currentLink.innerHTML);
	                	    if (contentEntered==null || $.trim(contentEntered)==''){
	                	    	 alert('Remarks area needs to be filled');
	                	    }else{
		                	  	if(val.noteType == "notes"){
		                	  		commentList[noteCntToDeleteIfEdited].comNotes = contentEntered;
		                	  		currentLink.innerHTML = 'View';
		                	  	} else if(val.noteType == "findings"){
		                	  		commentList[noteCntToDeleteIfEdited].comFindings = contentEntered;
		                	  		currentLink.innerHTML = 'View';
		                	  	}
		                	  $( this ).dialog('close');
	                	   }
	                }
	              },
	              {
	                  id: 'cancelMdsNoteBtn',
	                  text: 'Cancel',
	                  click: function() { 
	                	  	$( this ).dialog('close');
	                	  }
	              }
	            ]
	}).dialog('widget').position({ my: 'center', at: 'center', of: $(document.getElementById("mainTemplateBodyId")) });
	});

	$('textarea[maxlength]').limitMaxlength({
	onEdit: onEditCallback,
	onLimit: onLimitCallback
	});
	
	document.getElementById("addMdsNoteBtn").value = 'Add';
	document.getElementById("addMdsNoteBtn").style.visibility = "visible";
	$('#mdsNoteContent').prop('disabled',false);
	if(checkIfSavedComment(val.rowInd,val.noteType)){
		if(val.noteType == "notes"){
	  		if(commentList[noteCntToDeleteIfEdited].comNotes != null && commentList[noteCntToDeleteIfEdited].comNotes != ''){
	  			document.getElementById("addMdsNoteBtn").style.visibility = "hidden";
	  			$('#mdsNoteContent').prop('disabled',true);
	  		}
		} else if(val.noteType == "findings"){
			if(commentList[noteCntToDeleteIfEdited].comFindings != null && commentList[noteCntToDeleteIfEdited].comFindings != ''){
	  			document.getElementById("addMdsNoteBtn").style.visibility = "hidden";
	  			$('#mdsNoteContent').prop('disabled',true);
	  		}
		}
  	} 
	document.getElementById("cancelMdsNoteBtn").value = 'Cancel';
}

var onEditCallback = function(remaining){
	$(this).siblings('.charRemaining').text("Characters remaining: " + remaining);

	if(remaining > 0){
		$(this).css('background-color', 'white');
	}
}

var onLimitCallback = function(){
	$(this).css('background-color', 'red');
}

function getNoteValue(id,type){
	for(x in commentList){
		if(id == commentList[x].comId){
			if(type == "notes"){
				return commentList[x].comNotes;
			}else if(type == "findings"){
				return commentList[x].comFindings;
			}
		}
	}
	return "";
}

function checkIfSavedComment(id,type){
	for(y in existingCommentList){
		if(existingCommentList[y].comId == id){
			if(type == "notes"){
				if(existingCommentList[y].comNotes != null && existingCommentList[y].comNotes != ''){
					return true;
				}
			}else if(type == "findings"){
				if(existingCommentList[y].comFindings != null && existingCommentList[y].comFindings != ''){
					return true;
				}
			}
		}
	}
	return false;
}

// Added for MR-WF-16-00034 - Random sampling for QA
function disableFieldFlag() {
    $(function() {
        if ("true" === $("#isPpaQa").val()) {
            $("#salesSlipNumber").attr("disabled", isPpaQa);
            $("#approvalCode").attr("disabled", isPpaQa);
            $("input[name=crit2]").attr("disabled", isPpaQa);
            $("input[name=findNow]").attr("disabled", isPpaQa);
            $("input[name=clear]").attr("disabled", isPpaQa);
        }
    });
}
