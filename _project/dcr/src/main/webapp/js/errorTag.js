var PPALayout = [{
	name: "..",
	field: "radio",
	width: "10%"
},{
	name: "Date Posted",
	field: "datePosted",
	width: "10%"
},{
	name: "Date Updated",
	field: "dateUpdated",
	width: "10%"
},{ 
	name: "User Name",
	field: "userName",
	width: "10%"
},{
	name: "Errors",
	field: "errors",
	width: "10%"	
},{
	name: "Reported/Updated by",
	field: "reportedUpdatedBy",
	width: "10%"	
},{
	name: "Reason for Deletion",
	field: "reasonDeleted",
	width: "10%"	
}];

var nonPPALayout = [{
	name: "..",
	field: "radio",
	width: "10%"
},{
	name: "Date Posted",
	field: "datePosted",
	width: "10%"
},{
	name: "Date Updated",
	field: "dateUpdated",
	width: "10%"
},{ 
	name: "Step Name",
	field: "stepName",
	width: "10%"	
},{
	name: "User Name",
	field: "userName",
	width: "10%"
},{
	name: "Errors",
	field: "errors",
	width: "10%"	
},{
	name: "Reason",
	field: "reason",
	width: "10%"	
},{
	name: "Reported/Updated by",
	field: "reportedUpdatedBy",
	width: "10%"	
},{
	name: "Reason for Deletion",
	field: "reasonDeleted",
	width: "10%"	
}];

var errLogLayout;

var errLogData = [];
var taggedErrList = [];
var errListPerStep = [];
var errorListByStatus = [];
var dcrId;
var dcrDate;
var ccName;
var datePattern = /^(\d\d?)(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)((\d\d)?\d\d)/i;

function generateErrorLogTable(){
	
	$(document).ready(function(){
		
		new TableContainer({
			divName : "errorLogDiv",
			nodeName : "errorLogNode",
			width : "100%",
			id : "errorLogTable",
			layout : errLogLayout,
			tableClass: "errorLogTableClass",
			data: errLogData
		}).startUp();
		

		var errorLogTable = document.getElementById("errorLogTable");
		var columns = errorLogTable.getElementsByTagName("td");
		for(col in columns){
			var result = datePattern.exec(columns[col].innerHTML);
			if(result != null){
				columns[col].sorttable_customkey = result[0];
			}
		}
		sorttable.makeSortable(errorLogTable);
		
	}); 
	
	
}

function wrapDivToRow(tempErrLog, reasonDeleted, radioId){
	//alert('welcome');
	//alert('reasonDeleted: '+reasonDeleted);
	//alert('radioId [compound]: '+radioId);
	if(reasonDeleted != null && reasonDeleted != ''){//means deleted na, show with red font (and disabled radio button)
		for(property in tempErrLog){
			//alert(tempErrLog[property]);
			if(property=='radio'){
				//alert('entered property==radio');
				var radioMarkup = tempErrLog[property];
				//must disable this, so append disabled='true' to the input radio html markup inside
				tempErrLog[property] = radioMarkup.replace("/>","disabled='true'/>");
			}
			//if property is not included in the column layout, dont wrap to red div
			if(property!='identity' && property!='id' && property!='stepId' && property!='errorList' && property!='userNameX' && property!='reportedUpdatedByX'){
				tempErrLog[property] = "<div class='redFont'>" + tempErrLog[property] + "</div>";
				//alert('JSARD tempErrLog[property]: '+tempErrLog[property]);
			}
		}
	}else{
		//do nothing
	}

}

function generateErrorListCheckboxes(){
	var errorListDiv = document.getElementById("errorListDiv");
	//clear div, before appending the checkboxes
	while (errorListDiv.hasChildNodes()) {
		errorListDiv.removeChild(errorListDiv.lastChild);
	}
	for(err in errorListByStatus){
		var errorId = errorListByStatus[err].errorId;
		var errorName = errorListByStatus[err].errorName;
		
		var br = document.createElement("br");
		var chkbox = document.createElement("input");
		chkbox.type = "checkbox";
		//chkbox.value =
		chkbox.id = errorId;
		chkbox.name = "chkboxErr";
		chkbox.errname = errorName;
		chkbox.onclick = function() {callAddRemoveTaggedErrorList(this)};
		
		var chkboxLabel = document.createElement("label");
		chkboxLabel.htmlFor = errorId;
		chkboxLabel.appendChild(document.createTextNode(errorName));
		
		errorListDiv.appendChild(chkbox);
		errorListDiv.appendChild(chkboxLabel);
		errorListDiv.appendChild(br);
	}
}

function generateLookupButtons(){
	var btnsDiv = document.getElementById("lookupButtonsDiv");
	
	var addButton = document.createElement("button");
	addButton.id = "addButton";
	addButton.value = "Add";
	addButton.disabled = false;
	addButton.onclick = function() {callTagError("Add")};
	
//	var saveButton = document.createElement("button");
//	saveButton.id = "saveButton";
//	saveButton.value = "Save";
//	saveButton.disabled = true;
//	saveButton.onclick = function() {callTagError("Edit")};
	
	var clearButton = document.createElement("button");
	clearButton.id = "clearButton";
	clearButton.value = "Clear";
	clearButton.disabled = false;
	clearButton.onclick = clearUserNameSelectAndErrorCheckboxes;
	
	var exitButton = document.createElement("button");
	exitButton.id = "exitButton";
	exitButton.value = "Exit";
	exitButton.disabled = false;
	exitButton.onclick = function() {window.close()};
	
	btnsDiv.appendChild(addButton);
	btnsDiv.appendChild(clearButton);
	//btnsDiv.appendChild(saveButton);
	btnsDiv.appendChild(exitButton);
}

function clearUserNameSelectAndErrorCheckboxes(){
	document.getElementById("reasonField").value = '';
	var userNameSelectBox = document.getElementById("userNameSelect");
	for(var i = 0; i < userNameSelectBox.options.length; i++){
		if(userNameSelectBox.options[i].value == "none"){
			userNameSelectBox.options[i].selected = true;
		}
	}
	
	var inputs = document.getElementById("errorListDiv").getElementsByTagName('input');
	for(var i=0; i < inputs.length; i++){
		inputs[i].checked = false;
	}
	
	for(e in taggedErrList){
		delete taggedErrList[e];
	}
}

var callTagError = function tagError(addOrEdit){
	//alert('logIdentity: '+document.getElementById("logIdentity").value);
	var hiddenForm = document.getElementById("hiddenAddForm");
	if(addOrEdit=="Edit"){
		hiddenForm = document.getElementById("hiddenEditForm");
	}
	
	var dcrId = document.getElementById("dcrId").value;
	var dcrStatus = document.getElementById("dcrStatus").value;
	var dcrDate = document.getElementById("dcrDate").value;
	var ccName = document.getElementById("ccName").value;
	//2 dates (from java new Date) || setDatePosted || setDateUpdated
	var reason = document.getElementById("reasonField").value;
	//1 logged in user id (from session) || setPostedUpdatedById
	//reasonDeleted
	//var obj = document.getElementById("stepNameSelect");
	//var stepId = obj.options[obj.selectedIndex].value;
	var stepId = document.getElementById("stepIdByStatus").value;
	//var stepName = obj.options[obj.selectedIndex].text;
	var stepName = document.getElementById("stepNameAsStatus").value;
	var userName = document.getElementById("userNameSelect").value;
	var concatCashierNameListFromSP = document.getElementById("concatCashierNameListFromSP").value;
	
	if(userName=="none"){
		alert('Please specify user name');
		return;
	}
	
	var hiddenDcrId = document.createElement("input");
	hiddenDcrId.type = 'hidden';
	hiddenDcrId.name = 'dcrId';
	hiddenDcrId.value = dcrId;
	
	var hiddenDcrStatus = document.createElement("input");
	hiddenDcrStatus.type = 'hidden';
	hiddenDcrStatus.name = 'dcrStatus';
	hiddenDcrStatus.value = dcrStatus;
	
	var hiddenDcrDate = document.createElement("input");
	hiddenDcrDate.type = 'hidden';
	hiddenDcrDate.name = 'dcrDate';
	hiddenDcrDate.value = dcrDate;
	
	var hiddenCCName = document.createElement("input");
	hiddenCCName.type = 'hidden';
	hiddenCCName.name = 'ccName';
	hiddenCCName.value = ccName;
	
	var hiddenConcatCashiers = document.createElement("input");
	hiddenConcatCashiers.type = 'hidden';
	hiddenConcatCashiers.name = 'concatCashierNameList';
	hiddenConcatCashiers.value = concatCashierNameListFromSP;
	
	var hiddenReason = document.createElement("input");
	hiddenReason.type = 'hidden';
	hiddenReason.name = 'reason';
	hiddenReason.value = reason;
	
	var hiddenStepId = document.createElement("input");
	hiddenStepId.type = 'hidden';
	hiddenStepId.name = 'stepId';
	hiddenStepId.value = stepId;
	
	var hiddenStepName = document.createElement("input");
	hiddenStepName.type = 'hidden';
	hiddenStepName.name = 'stepName';
	hiddenStepName.value = stepName;
	
	var hiddenUserName = document.createElement("input");
	hiddenUserName.type = 'hidden';
	hiddenUserName.name = 'userName';
	hiddenUserName.value = userName;
	
	//if taggedErrList array is empty(no checkboxes ticked), prompt user
	//NOTE: when using delete in javascript arrays, it only assigns the element to undefined, it will decrease the length of the array!
	//so using .length does not yield expected results like zero
	//So, in summary use delete on object literals. Use splice on arrays.
	
	var toSkipSubmit = true;
	//alert user when it clicks add but no checkboxes were checked
	var inputs = document.getElementById("errorListDiv").getElementsByTagName('input');
	for(var i=0; i < inputs.length; i++){
		//alert('is checked? '+inputs[i].checked);
		if(inputs[i].checked){
			toSkipSubmit = false;
		}
	}
	
	if(toSkipSubmit){
		alert('Please check at least one checkbox');
		return;
	}
	
	for(errCnt in taggedErrList){
		//alert(taggedErrList[errCnt].errorId);
		//alert(taggedErrList[errCnt].errorName);
		
		var hiddenErrId = document.createElement("input");
		hiddenErrId.type = 'hidden';
		hiddenErrId.name = 'taggedErrors['+errCnt+'].errorId';
		hiddenErrId.value = taggedErrList[errCnt].errorId;
		
		var hiddenErrName = document.createElement("input");
		hiddenErrName.type = 'hidden';
		hiddenErrName.name = 'taggedErrors['+errCnt+'].errorName';
		hiddenErrName.value = taggedErrList[errCnt].errorName;
		if(hiddenErrName.value=='Other'){
			if(reason==null || $.trim(reason)==''){
				alert('Other is selected, please specify error in the Reason field');
				return;//exit function
			}else{
				hiddenErrName.value = reason;
			}
		}
		
		if(addOrEdit=="Add"){
			//alert('onAdd only: trigger validation if error is already tagged');
			for(existing in errLogData){
				//alert(errLogData[existing].stepId);
				if(errLogData[existing].stepId == stepId){
					//alert('userName: '+errLogData[existing].userNameX);
					if(errLogData[existing].userNameX == userName){
						var erList = errLogData[existing].errorList;
						//var reasonDeleted = errLogData[existing].reasonDeleted;
						//alert('reasonDeleted: '+reasonDeleted);
						for(er in erList){
							//alert('errorId: '+erList[er].errorId);
							//alert('errorName: '+erList[er].errorName);
							if(erList[er].errorId == taggedErrList[errCnt].errorId){
								//if (reasonDeleted==null || reasonDeleted==""){
									alert('Error is already tagged');
									return;//exit function
								//}
							}
						}						
					}
				}
			}
		}
		
		hiddenForm.appendChild(hiddenErrId);
		hiddenForm.appendChild(hiddenErrName);
	}
	
	hiddenForm.appendChild(hiddenDcrId);
	hiddenForm.appendChild(hiddenDcrStatus);
	hiddenForm.appendChild(hiddenDcrDate);
	hiddenForm.appendChild(hiddenCCName);
	hiddenForm.appendChild(hiddenConcatCashiers);
	hiddenForm.appendChild(hiddenReason);
	hiddenForm.appendChild(hiddenStepId);
	hiddenForm.appendChild(hiddenStepName);
	hiddenForm.appendChild(hiddenUserName);
	
	//complete
	hiddenForm.submit();
}

function generateLogButtons(){
	var btnsDiv = document.getElementById("logButtonsDiv");
	
	//var editButton = document.createElement("button");
	//editButton.id = "editButton";
	//editButton.value = "Edit";
	//editButton.disabled = true;
	//editButton.onclick = loadEditValuesIntoLookup;//NEEDS ID
	
	var deleteButton = document.createElement("button");
	deleteButton.id = "deleteButton";
	deleteButton.value = "Delete";
	deleteButton.disabled = true;
	deleteButton.onclick = tagErrorAsDeleted;//NEEDS ID
	
	var emailButton = document.createElement("button");
	emailButton.id = "emailButton";
	emailButton.value = "Email";
	emailButton.onclick = openEmail;//NEEDS ID
	emailButton.disabled = true;
	
	//btnsDiv.appendChild(editButton);
	btnsDiv.appendChild(deleteButton);
	btnsDiv.appendChild(emailButton);
}

var emailWindow = null;
var tempEmailWindow = null;

function closeEmailWindow(){
	if(emailWindow != null || emailWindow != tempEmailWindow){
		emailWindow.close();
	}
}
function openEmail(){
	closeEmailWindow();
	
	//build and pass comma-separated values of error names from the row of the selected radio button in the error log table
	var concatErrorNameList = '';//content
	var to = '';//userName
	var cc = '';//reportedUpdatedBy
	var subject = 'Error Tag - DCR findings '+ccName+' '+dcrDate;
	
	var radioLogGroup = document.getElementsByName('radioLog');
    for (var i=0; i<radioLogGroup.length; i++)  {
        if(radioLogGroup[i].type=='radio' && radioLogGroup[i].checked){
        	for(existing in errLogData){
        		if(errLogData[existing].id == radioLogGroup[i].id){
        			to =  errLogData[existing].userNameX;
        			cc = errLogData[existing].reportedUpdatedByX;
        			
        			var errListOfSelectedRow = errLogData[existing].errorList;
        			for(error in errListOfSelectedRow){
        				concatErrorNameList = concatErrorNameList + errListOfSelectedRow[error].errorName + '~';
        			}
        		}
        	}
        }
     }

	if(concatErrorNameList != ''){
		concatErrorNameList = concatErrorNameList.substring(0, concatErrorNameList.length - 1);//clip last '~' char
	}
	
	emailWindow = window.open("../email/create.html?dcrId="+dcrId+"&concatErrorNameList="+concatErrorNameList+"&to="+to+"&cc="+cc+"&subject="+subject+"&fromErrorTag=true","_blank", "width=600,height=500,resizable=yes,status=yes,scrollbars=yes");
	tempEmailWindow = emailWindow;
}

function tagErrorAsDeleted(){
	var hiddenForm = document.getElementById("hiddenDeleteForm");
	
	//pop up input for reasonDeleted
	var reasonDeleted = prompt("Please enter reason for deletion","");
	//alert('reasonDeleted: '+reasonDeleted);
	if (reasonDeleted!=null && reasonDeleted!=""){
		document.getElementById("reasonDeleted").value = reasonDeleted;
	}else{
		alert("Not a valid input");
		return;
	}

	var radioLogGroup = document.getElementsByName('radioLog');
    for (var i=0; i<radioLogGroup.length; i++)  {
        if(radioLogGroup[i].type=='radio' && radioLogGroup[i].checked){
        	for(existing in errLogData){
        	
        		if(errLogData[existing].id == radioLogGroup[i].id && radioLogGroup[i].checked){
        			
        			//NEW
        			//alert('JSARD username passed when deleting: '+errLogData[existing].userNameX);
        			document.getElementById("stepId").value = errLogData[existing].stepId;
        			document.getElementById("userId").value = errLogData[existing].userNameX;
        			document.getElementById("tagger").value = errLogData[existing].reportedUpdatedByX;
        			var tagger = errLogData[existing].reportedUpdatedByX.toUpperCase();
        			var loggedInUser = document.getElementById("loggedInUser").value.toUpperCase();
        			if(tagger!=loggedInUser){
        				alert("Cannot delete Error tagged by a different user.");
        				return false;
        			}
        			
        			
        		}
        	}
        }
     }
    var dcrId = document.getElementById("dcrId").value;
	var hiddenDcrId = document.createElement("input");
	hiddenDcrId.type = 'hidden';
	hiddenDcrId.name = 'dcrId';
	hiddenDcrId.value = dcrId;
	
	var dcrStatus = document.getElementById("dcrStatus").value;
	var hiddenDcrStatus = document.createElement("input");
	hiddenDcrStatus.type = 'hidden';
	hiddenDcrStatus.name = 'dcrStatus';
	hiddenDcrStatus.value = dcrStatus;
	
	var dcrDate = document.getElementById("dcrDate").value;
	var hiddenDcrDate = document.createElement("input");
	hiddenDcrDate.type = 'hidden';
	hiddenDcrDate.name = 'dcrDate';
	hiddenDcrDate.value = dcrDate;
	
	var ccName = document.getElementById("ccName").value;
	var hiddenCCName = document.createElement("input");
	hiddenCCName.type = 'hidden';
	hiddenCCName.name = 'ccName';
	hiddenCCName.value = ccName;
	
	var concatCashierNameListFromSP = document.getElementById("concatCashierNameListFromSP").value;
	var hiddenConcatCashiers = document.createElement("input");
	hiddenConcatCashiers.type = 'hidden';
	hiddenConcatCashiers.name = 'concatCashierNameList';
	hiddenConcatCashiers.value = concatCashierNameListFromSP;
	
	hiddenForm.appendChild(hiddenDcrId);
	hiddenForm.appendChild(hiddenDcrStatus);
	hiddenForm.appendChild(hiddenDcrDate);
	hiddenForm.appendChild(hiddenCCName);
	hiddenForm.appendChild(hiddenConcatCashiers);
	
    hiddenForm.submit();
}

var callAddRemoveTaggedErrorList = function addRemoveTaggedErrorList(obj){
	//alert('is checked? '+obj.checked);
	//alert(obj.id);
	//alert(obj.name);
	if(obj.checked){
		var e = {};
		e.errorId = obj.id;
		e.errorName = obj.errname;
		taggedErrList.push(e);
	}else{
		for(e in taggedErrList){
			if(taggedErrList[e].errorId == obj.id){
				delete taggedErrList[e];
			}
		}
	}
}
function enableLogButtons(radioBtnSelected){
	//alert('radioBtnSelected id: '+radioBtnSelected.id);
	//alert('radioBtnSelected value: '+radioBtnSelected.value);
	//alert('radioBtnSelected stepId: '+radioBtnSelected.stepId);
	
	//document.getElementById("editButton").disabled = false;
	document.getElementById("deleteButton").disabled = false;
	document.getElementById("emailButton").disabled = false;
}

function autoSort(){
	$(document).ready(function(){
	var resultTable = document.getElementById("errorLogTable");
	var headers = resultTable.getElementsByTagName("th");
	for(var h = 0; h < headers.length; h++){
		if(headers[h].innerHTML == "Date Updated"){
			headers[h].click();
		}
	}
	resetOddEvenRows(resultTable);
	});
}