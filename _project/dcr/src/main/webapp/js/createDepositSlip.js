var denominationsJSON ={};


var chequesJSON = {"securityBank":"SECURITY BANK CHEQUES",
				   "localClearing":"LOCAL CLEARING CHEQUES",
				   "regionalClearing":"REGIONAL CLEARING CHEQUES",
				   "chequeforCollection":"CHEQUE FOR COLLECTION",
				   "otherCheque":"OTHER CHEQUES"};
					



function calculateAmount(denominationName,denominationValue){
	$(document).ready(function(){
	
	var hiddenCAId = "hiddenCashAmount"+denominationName;
	var cashAmountId = "cashAmount"+denominationName;
	
	var sourceId ="noOfPieces"+denominationName;
	
	hiddenCashAmountTextBox =  document.getElementById(hiddenCAId);
	cashAmountTextbox= document.getElementById(cashAmountId);
	noOfpieces = document.getElementById(sourceId);
	
	resetNaNtoZero(noOfpieces);
	var total = parseFloat(noOfpieces.value) * parseFloat(denominationValue) ;
	
	hiddenCashAmountTextBox.value=total;
	cashAmountTextbox.value = numberWithCommas(parseFloat(total).toFixed(2));
	
	});
}

function calculateBillsCoinsDiscrepancy(isSavedDisabled,isConfirmed){
	$(document).ready(function(){
	calculateTotalAmountBills();
	calculateTotalAmountCoins();
	calculateDiscrepancy(isSavedDisabled,isConfirmed);
	var saveButton = document.getElementById('saveButton');
	if(!isConfirmed && isSavedDisabled=="true"){
		saveButton.disabled=true;
	}
	});
}


function calculateTotalAmount(currency){
	$(document).ready(function(){
	var totalAmount=0;
	
	if(currency=="PESO"){
	var denominations = {"1000":"1000", "500":"500", "200":"200", "100":"100", "50":"50", "20":"20", "10":"10", "5":"5", "1":"1", "25c":"0.25", "10c":"0.10", "5c":"0.05", "1c":"0.01"};
	}else if (currency=="DOLLAR"){
	var denominations = {"100":"100", "50":"50", "20":"20", "10":"10", "5":"5","2":"2", "1":"1", "1Coin":"1", "50c":"0.50", "25c":"0.25", "10c":"0.10", "5c":"0.05", "1c":"0.01" };
	}
	for(denom in denominations){	
	var id = "hiddenCashAmount"+denom;
	amount = document.getElementById(id);
	resetNaNtoZero(amount);
	totalAmount = parseFloat(totalAmount) + parseFloat(amount.value);
		
	}
	var totalAmountBox = document.getElementById("totalAmount");
	var hiddenTotalCashAmount = document.getElementById("hiddenTotalAmount");
	hiddenTotalCashAmount.value = totalAmount;
	totalAmountBox.value = numberWithCommas(parseFloat(totalAmount).toFixed(2));
	});
	
}

function calculateTotalAmountBills(){
	$(document).ready(function(){
	var totalAmount=0;
	var denominations = {"100":"100", "50":"50", "20":"20", "10":"10", "5":"5","2":"2", "1":"1"};
	for(denom in denominations){	
	var id = "hiddenCashAmount"+denom;
	amount = document.getElementById(id);
	resetNaNtoZero(amount);
	totalAmount = parseFloat(totalAmount) + parseFloat(amount.value);
		
	}
	var totalAmountBox = document.getElementById("totalAmountBills");
	var hiddenTotalCashAmount = document.getElementById("hiddenTotalAmountBills");
	hiddenTotalCashAmount.value = totalAmount;
	totalAmountBox.value = numberWithCommas(parseFloat(totalAmount).toFixed(2));
	});
	
}
function calculateTotalAmountCoins(){
	$(document).ready(function(){
	var totalAmount=0;
	var denominations = {"1Coin":"1","50c":"0.50","25c":"0.25","10c":"0.10","5c":"0.05","1c":"0.01"};
	for(denom in denominations){	
	var id = "hiddenCashAmount"+denom;
	amount = document.getElementById(id);
	resetNaNtoZero(amount);
	totalAmount = parseFloat(totalAmount) + parseFloat(amount.value);
		
	}
	var totalAmountBox = document.getElementById("totalAmountCoins");
	var hiddenTotalCashAmount = document.getElementById("hiddenTotalAmountCoins");
	hiddenTotalCashAmount.value = totalAmount;
	totalAmountBox.value = numberWithCommas(parseFloat(totalAmount).toFixed(2));
	});
	
}


function calculateTotalAmountCheque(currency,type){
	$(document).ready(function(){
	var totalAmount=0;
	
	var id = "amountCheque";
	var idHidden = "hiddenAmountCheque";
	
	var chequeAmount= document.getElementById(id);
	var hiddenChequeAmount = document.getElementById(idHidden);
	//resetNaNtoZero(chequeAmount);
	var collection = "hiddenCollectionTotal";
	collectionAmount = document.getElementById(collection);
//	totalAmount = parseFloat(totalAmount) + parseFloat(chequeAmount.value);
		
//	var totalCashAmount = document.getElementById("totalAmount");
//	var hiddenTotalCashAmount = document.getElementById("hiddenTotalAmount");
//	hiddenTotalCashAmount.value = totalAmount;
//	totalCashAmount.value = numberWithCommas(parseFloat(totalAmount).toFixed(2));
	var prefixcurrency="";
	if(currency=='PESO'){
		prefixcurrency="PHP";
	}else{
		prefixcurrency="USD";
	}
	chequeAmount.value=prefixcurrency+" "+numberWithCommas(parseFloat(hiddenChequeAmount.value).toFixed(2));
	hiddenChequeAmount.value = hiddenChequeAmount.value;
	});
	
}


function calculateDiscrepancy(isSavedDisabled,isConfirmed){
	$(document).ready(function(){
	var discrepancyAmt;
	var hiddenTotalAmount = document.getElementById("hiddenTotalAmount");
	var sessionTotal = document.getElementById("hiddenCollectionTotal");
	resetNaNtoZero(hiddenTotalAmount);
	discrepancyAmt = parseFloat(sessionTotal.value).toFixed(2) - parseFloat(hiddenTotalAmount.value).toFixed(2);
	var discrepancy = document.getElementById("discrepancy");
	discrepancy.value = numberWithCommas(parseFloat(discrepancyAmt).toFixed(2));
	var saveButton = document.getElementById('saveButton');
	var previewButton = document.getElementById('previewButton');
	if(discrepancyAmt!=0){
		saveButton.disabled=true;
		previewButton.disabled=true;
	}else if (discrepancyAmt==0 ){
		saveButton.disabled=false;
		previewButton.disabled=false;
	}
	if(isConfirmed =="false"){
		saveButton.disabled=true;
	}else if(isConfirmed =="true" && isSavedDisabled=="true"){
		saveButton.disabled=true;
		previewButton.disabled=true;
	}else if(isConfirmed && isSavedDisabled=="true"){
		saveButton.disabled=true;
		previewButton.disabled=true;
	}else if(isConfirmed =="true" && isSavedDisabled=="false" && discrepancyAmt==0){
		saveButton.disabled=false;
		previewButton.disabled=false;
	}
	
	if(sessionTotal.value==0){
		saveButton.disabled=true;
		previewButton.disabled=true;
	}
		
	});
}



function validateInput(obj){
	if(!isNaN(obj.value) && obj.value.indexOf(" ") == -1 && obj.value.length > 0 && obj.value.indexOf(".") == -1){
		return true;
	}else {
		obj.value = "0";
		obj.select();
		return false;
	}
	
	
}

function resetNaNtoZero(obj){
	$(document).ready(function(){
	if(isNaN(obj.value)||obj.value.length==0){
		obj.value ="0";
		obj.select();
	}
	
	});
}


function getFormattedValue(currency,amount){
	$(document).ready(function(){
	var collectionTotal = document.getElementById("collectionTotal");
	if(currency=='PESO'){
		prefixcurrency="PHP";
	}else{
		prefixcurrency="USD";
	}
	collectionTotal.value = prefixcurrency+" " +numberWithCommas(parseFloat(amount).toFixed(2));
	
	});
}

function initializePageCash(currency,isSavedDisabled, reqAmt,isConfirmed){
		$(document).ready( function(){
			
	if(currency=='PESO'){
		var denominations = {"1000":"1000", "500":"500", "200":"200", "100":"100", "50":"50", "20":"20", "10":"10", "5":"5", "1":"1", "25c":"0.25", "10c":"0.10", "5c":"0.05", "1c":"0.01"};
	}else if(currency=='DOLLAR'){
		var denominations = {"100":"100", "50":"50", "20":"20", "10":"10", "5":"5","2":"2", "1":"1", "1Coin":"1", "50c":"0.50", "25c":"0.25", "10c":"0.10", "5c":"0.05", "1c":"0.01"};
		
	}
	for (denom in denominations) {
		calculateAmount(denom,denominations[denom]);
	}

	calculateTotalAmount(currency);
	getFormattedValue(currency,reqAmt);
	calculateDiscrepancy(isSavedDisabled,isConfirmed);
	if(currency=='DOLLAR'){
		calculateBillsCoinsDiscrepancy(isSavedDisabled,isConfirmed);
	}
	var discrepancyAmt;
	var hiddenTotalAmount =document.getElementById("hiddenTotalAmount");
	var sessionTotal = document.getElementById("hiddenCollectionTotal");
	resetNaNtoZero(hiddenTotalAmount);
	discrepancyAmt = parseFloat(sessionTotal.value).toFixed(2) - parseFloat(hiddenTotalAmount.value).toFixed(2);
	var saveButton = document.getElementById('saveButton');
	var previewButton = document.getElementById('previewButton');
	if(!isConfirmed){
		saveButton.disabled=true;
		for(denom in denominations){	
		var id ="noOfPieces"+denom;
		var	noOfPieces = document.getElementById(id);
		noOfPieces.readOnly =true;
		}
	}else if(isConfirmed && isSavedDisabled=="true"){
		previewButton.disabled=true;
		saveButton.disabled=true;
		for(denom in denominations){	
		var id ="noOfPieces"+denom;
		var	noOfPieces = document.getElementById(id);
		noOfPieces.readOnly =true;
		}
	}else if(isConfirmed && isSavedDisabled=="false" && discrepancyAmt ==0){
		saveButton.disabled=false;
	}
	
	if(reqAmt==0){
		saveButton.disabled=true;
		previewButton.disabled=true;
	}
	
	});
}



function calculateTotalMixCheque(){
	$(document).ready(function(){
	var slocpiTotal = document.getElementById("slocpiTotal");
	var slfpiTotal = document.getElementById("slfpiTotal");
	var mixedChequeTotal = document.getElementById("mixedChequeTotal");
	
	var hiddenSlocpiTotal = document.getElementById("hiddenSlocpiTotal");
	var hiddenSlfpiTotal = document.getElementById("hiddenSlfpiTotal");
	var hiddenMixedChequeTotal = document.getElementById("hiddenMixedChequeTotal");
	var amount =0;
	var slocpi =0;
	var slfpi =0;
	
	resetNaNtoZero(hiddenSlfpiTotal);
	resetNaNtoZero(hiddenSlocpiTotal);
	resetNaNtoZero(hiddenMixedChequeTotal);
	amount = parseFloat(hiddenSlocpiTotal.value) + parseFloat(hiddenSlfpiTotal.value);
	slocpi = parseFloat(hiddenSlocpiTotal.value);
	slfpi = parseFloat(hiddenSlfpiTotal.value);
	
	hiddenMixedChequeTotal.value = amount;
	
	mixedChequeTotal.value = numberWithCommas(parseFloat(amount).toFixed(2));
	slocpiTotal.value = numberWithCommas(parseFloat(slocpi).toFixed(2));
	slfpiTotal.value = numberWithCommas(parseFloat(slfpi).toFixed(2));
	});
}

function calculateChequeAmountWithMixCheque(currency,company,type,isConfirmed){
	$(document).ready(function(){
	var amount = 0;
	var slocpiTotal = document.getElementById("hiddenSlocpiTotal");
	var slfpiTotal = document.getElementById("hiddenSlfpiTotal");
	var collectionTotal = document.getElementById("hiddenCollectionTotal");
	var id = "amountCheque";
	var hiddenId = "hiddenAmountCheque";
	var totalCheckAmount = document.getElementById("hiddenTotalAmount");
	
	var amountCheque= document.getElementById(id);
	var hiddenamountCheque= document.getElementById(hiddenId);
	
	resetNaNtoZero(slfpiTotal);
	resetNaNtoZero(slocpiTotal);
	var prefixcurrency="";
	if(currency=='PESO'){
		prefixcurrency="PHP";
	}else{
		prefixcurrency="USD";
	}
	
	if('SLOCPI'==company){
		amount = parseFloat(collectionTotal.value) + parseFloat( slfpiTotal.value);
	}else if('SLFPI'==company){
		amount = parseFloat(collectionTotal.value) - parseFloat( slfpiTotal.value) 
	}
	hiddenamountCheque.value = amount;
	totalCheckAmount.value = amount;
	amountCheque.value =prefixcurrency +" "+numberWithCommas(parseFloat(amount).toFixed(2));
	
	var ct = document.getElementById('hiddenCollectionTotal').value;
	var saveButton = document.getElementById('saveButton');
	var previewButton = document.getElementById('previewButton');
	if( isConfirmed && collectionTotal.value >0 && amount ==0){
		saveButton.disabled=false;
		previewButton.disabled=true;
	}
	
	});
}

function initializePageCheque(currency,isSavedDisabled, type,reqAmt,company,hasMixCheque,isConfirmed){
	$(document).ready( function(){
		getFormattedValue(currency,reqAmt);
		var amount = 0.0;
		if(hasMixCheque == 'true'){
		calculateTotalMixCheque();
	//	calculateChequeAmountWithMixCheque(currency,company,type,isConfirmed);
		calculateTotalAmountCheque(currency,type);
		}else{
			calculateTotalAmountCheque(currency,type);
		}
		var saveButton = document.getElementById('saveButton');
		var previewButton = document.getElementById('previewButton');
		if(!isConfirmed){
			saveButton.disabled=true;
		}else if(isConfirmed && isSavedDisabled=="true"){
			saveButton.disabled=true;
			previewButton.disabled=true;
		}else if(isConfirmed =="true" && isSavedDisabled=="true"){
			saveButton.disabled=true;
			previewButton.disabled=true;
		
		}else if(isConfirmed && isSavedDisabled=="false"){
			saveButton.disabled=false;
		}
		if(reqAmt==0){
			saveButton.disabled=true;
			previewButton.disabled=true;
		}
		if(hasMixCheque == 'true'){
		var ct = document.getElementById('hiddenCollectionTotal').value;
		amount =  document.getElementById('hiddenAmountCheque').value;
		var saveButton = document.getElementById('saveButton');
		var previewButton = document.getElementById('previewButton');
		if( isConfirmed && ct.value >0 && amount ==0){
			saveButton.disabled=false;
			previewButton.disabled=true;
		}
		}
		
		
	});
}
