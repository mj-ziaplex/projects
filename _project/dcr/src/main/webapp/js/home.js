function setCashierWorkItems(realHomeData) {
	$(document).ready(function(){
		var homeLabelLayout = [{
			name : "Cashier's Inbox",
			field : "homeLabel",
			width : "100%"
		}];
		
		new TableContainer({
			divName : "homeDiv",
			nodeName : "homeLabelNode",
			width : "100%",
			id : "homeLabel",
			layout : homeLabelLayout,
			tableClass : "homeLabelBorderedTable"
		}).startUp();
		
		var homeTableLayout = [{
			name : "Customer Center",
			field : "customerCenter",
			width : "40%"
		},{
			name : "Date",
			field : "date",
			width : "30%"
		},{
			name : "Status",
			field : "status",
			width : "30%"
		}];
		
		new TableContainer({
			divName : "homeTableNode",
			nodeName : "homeNode",
			width : "100%",
			id : "homeTable",
			layout : homeTableLayout,
			tableClass : "homeBorderedTable"
		}).startUp();
		
		var homeTable = _globalTableContainer["homeTable"];
		homeTable.setData(realHomeData);
		homeTable.refreshTable();
		
		var home = document.getElementById("homeTable");
		
		sorttable.makeSortable(home);
		autoSort(home);
	});
}

function setDCRWorkItems(realHomeData) {
	$(document).ready(function(){
		var homeDCRLabelLayout = [{
			name : "Customer Center Inbox",
			field : "homeDCRLabel",
			width : "100%"
		}];
		
		new TableContainer({
			divName : "homeDCRDiv",
			nodeName : "homeDCRLabelNode",
			width : "100%",
			id : "homeDCRLabel",
			layout : homeDCRLabelLayout,
			tableClass : "homeDCRLabelBorderedTable"
		}).startUp();
		
		var homeDCRTableLayout = [{
			name : "Customer Center",
			field : "customerCenter",
			width : "40%"
		},{
			name : "Date",
			field : "date",
			width : "20%"
		},{
			name : "Status",
			field : "status",
			width : "20%"
		},{
			name : "Required Completion Date",
			field : "requiredCompletionDate",
			width : "20%"
		}];
		
		new TableContainer({
			divName : "homeDCRTableNode",
			nodeName : "homeDCRNode",
			width : "100%",
			id : "homeDCRTable",
			layout : homeDCRTableLayout,
			tableClass : "homeDCRBorderedTable"
		}).startUp();
		
		var homeDCRTable = _globalTableContainer["homeDCRTable"];
		homeDCRTable.setData(realHomeData);
		homeDCRTable.refreshTable();
		
		var homeDCR = document.getElementById("homeDCRTable");
		
		sorttable.makeSortable(homeDCR);
		autoSort(homeDCR);
	});
}

function autoSort(resultTable){
	$(document).ready(function(){
	var headers = resultTable.getElementsByTagName("th");
	for(var h = 0; h < headers.length; h++){
		if(headers[h].innerHTML == "Date"){
			headers[h].click();
		}
	}
	resetOddEvenRows(resultTable);
	});
}
