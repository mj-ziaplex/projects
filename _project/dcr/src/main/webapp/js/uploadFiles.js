var layout = [ { name : "File ID", field : "fileId", width : "60%" }, 
               { name : "Type", field : "fileType", width : "10%" }, 
               { name : "Description", field : "fileDescription", width : "30%" }];

function loadTable(data){
	$(document).ready(function(){
		
		var uploadTableData = [];

		new TableContainer({
			divName : "uploadTableDiv",
			nodeName : "uploadTableNode",
			width : "100%",
			id : "uploadTable",
			layout : layout,
			tableClass : "uploadTable",
			data : []
		}).startUp();



		var table = _globalTableContainer["uploadTable"];
		
		appendJsons([uploadTableData,data]);
		
		table.setData(uploadTableData);
		table.refreshTable();
		
		
		
	});
	
}

function uploadFile(){
	var uploadForm = document.getElementById("uploadForm");
	var description  = document.getElementById("descTextArea");
	if(!description.value){
		alert('Description cannot be blank!');
		
		return false;
	
	}
	uploadForm.submit();
}

