				var onEditCallback = function(remaining){
					$(this).siblings('.charsRemaining').text("Characters remaining: " + remaining);

					if(remaining > 0){
						$(this).css('background-color', 'white');
					}
				}

				var onLimitCallback = function(){
					$(this).css('background-color', 'red');
				}

		var showAddCommentPopUp = function() {
			document.getElementById("addCommentForm").reset();
			$(function() {
				$( "#addCommentDiv" ).dialog({
					autoOpen: true,
					height: "auto",
					minHeight: 580,
					width: 540,
					modal: true,
					closeOnEscape: true,
					draggable: false,
					resizable: false,
					dialogClass: "commentFormClass",
					title: "Add Comment"
				}).dialog('widget').position({ my: 'center', at: 'center', of: $(document.getElementById("mainTemplateBodyId")) });
			});

			$('textarea[maxlength]').limitMaxlength({
				onEdit: onEditCallback,
				onLimit: onLimitCallback
			});
			
		};

		var showViewCommentsPopUp = function() {
			$(function() {
				$( "#allCommentsPopUpTableNode" ).dialog({
					autoOpen: true,
					height: 600,
					width: 828,
					modal: true,
					closeOnEscape: true,
					draggable: false,
					resizable: false,
					dialogClass: "commentTableClass",
					title: "View Comments"
				}).dialog('widget').position({ my: 'center', at: 'center', of: $(document.getElementById("mainTemplateBodyId")) });
			});
		};
		
		var commentColumnStyle = "text-align:center;";
		
		var commentLabelLayout = [{
			name : "Comments",
			field : "commentLabel",
			width : "100%"
		}];

		var commentTableLayout = [{
			name : "User",
			field : "user",
			width : "10%",
			styles: commentColumnStyle
		},{
			name : "Date Posted",
			field : "datePosted",
			width : "30%",
			styles: commentColumnStyle
		},{
			name : "Remarks",
			field : "remark",
			width : "60%",
			styles: commentColumnStyle
		}];
		
function setDCRComments(commentData,url) {
	$(document).ready(function(){
		
		new TableContainer({
			divName : "commentDiv",
			nodeName : "commentLabelNode",
			width : "100%",
			id : "commentLabel",
			layout : commentLabelLayout,
			tableClass : "commentLabelBorderedTable"
		}).startUp();
		
		new TableContainer({
			divName : "commentTableNode",
			nodeName : "commentNode",
			width : "100%",
			id : "commentTable",
			layout : commentTableLayout,
			tableClass : "commentBorderedTable"
		}).startUp();
		
		var commentTable = _globalTableContainer["commentTable"];
		
		commentTable.setData(commentData);
		commentTable.refreshTable();
		
		var addViewCommentButtonDiv = document.getElementById("addCommentNode");
		
		var viewCommentButton = document.createElement("button");
		viewCommentButton.innerHTML = "View Comments";
		viewCommentButton.style.left="0px";
		viewCommentButton.id="viewCommentsButton";
		viewCommentButton.onclick = showViewCommentsPopUp;
		addViewCommentButtonDiv.appendChild(viewCommentButton);
		
		var addCommentButton = document.createElement("button");
		addCommentButton.innerHTML = "Add Comment";
		//addCommentButton.style.left="0px";
		addCommentButton.id="addCommentButton";
		addCommentButton.onclick = showAddCommentPopUp;
		addViewCommentButtonDiv.appendChild(addCommentButton);
		
		var goBackButtonDiv = document.getElementById("navigationNode");
		var goBackButton = document.createElement("button");
		goBackButton.innerHTML = "Go Back";
		goBackButton.id="goBackButton";
		goBackButton.onclick = function() { goBack(url); };
		if(getCompanyContextFromUrl(url)=='slamcip'){
			goBackButton.disabled = true;
		}
		goBackButtonDiv.appendChild(goBackButton);
		
		var continueButton = document.createElement("button");	
		continueButton.innerHTML = "Continue";
		continueButton.id="continueButton";
		continueButton.onclick = function() { moveOn(url); };
		if(getCompanyContextFromUrl(url)=='other'){
			continueButton.disabled = true;
		}
		goBackButtonDiv.appendChild(continueButton);
	});
}

function getCompanyContextFromUrl(url){
	var urlStr = String(url);
	var urlStrPart = urlStr.substring(0,urlStr.lastIndexOf("/"));
	var companyContext = urlStrPart.substring(urlStrPart.lastIndexOf("/")+1);
	return companyContext;
}

var goBack = function(url){
	var companyContext = getCompanyContextFromUrl(url);
	//alert(companyContext);
	
	var companyTarget = companyContext;
	if(companyContext=='other'){
		companyTarget = 'slfpi';
	}else if(companyContext=='slfpi'){
		companyTarget = 'slgfi';
	}else if(companyContext=='slgfi'){
		companyTarget = 'slocpi';
	}else if(companyContext=='slocpi'){
		companyTarget = 'slamcid';
	}else if(companyContext=='slamcid'){
		companyTarget = 'slamcip';
	}
	var urlStr = String(url);
	var href = urlStr;
	if(companyContext=='slfpi'){
		//alert('slfpi clicked, replace report with index');
		href = href.replace('report','index');
	}
	href = href.replace(companyContext,companyTarget);
	
	//alert(href);
	location.href = href;
}

var moveOn = function(url){
	var companyContext = getCompanyContextFromUrl(url);
	//alert(companyContext);
	
	var companyTarget = companyContext;
	if(companyContext=='slamcip'){
		companyTarget = 'slamcid';
	}else if(companyContext=='slamcid'){
		companyTarget = 'slocpi';
	}else if(companyContext=='slocpi'){
		companyTarget = 'slgfi';
	}else if(companyContext=='slgfi'){
		companyTarget = 'slfpi';
	}else if(companyContext=='slfpi'){
		companyTarget = 'other';
	}
	var urlStr = String(url);
	var href = urlStr;
	if(companyContext=='slgfi'){
		//alert('slgfi clicked, replace index with report');
		href = href.replace('index','report');
	}
	href = href.replace(companyContext,companyTarget);
	
	//alert(href);
	location.href = href;
}

function returnViewEditButton(){
	return "<button class=\"viewEditButton\" onClick=alert(\"View/Edit\")>...</button>";
}

function setDataForPopUp(commentData){
	$(document).ready(function(){
		
		new TableContainer({
			divName : "allCommentsPopUpTableNode",
			nodeName : "allCommentsPopUpNode",
			width : "100%",
			id : "popupTable",
			layout : commentTableLayout,
			tableClass : "commentBorderedTable"
		}).startUp();
		
		var popupTable = _globalTableContainer["popupTable"];
		
		popupTable.setData(commentData);
		popupTable.refreshTable();

	});
}
