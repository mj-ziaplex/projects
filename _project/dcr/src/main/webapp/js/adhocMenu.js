function activateLink(companyKeyLink){
	var mainNodeId = '';
	var subNodeId = '';
	var expandedHeight = '';
	
	if(companyKeyLink > 0 && companyKeyLink < 7){
		//alert('its a balancing tool page');
		mainNodeId = 'collectionsNode';
		subNodeId = 'collectionsSubNodes';
		expandedHeight = '122px';
		expandList(mainNodeId,subNodeId,expandedHeight);
	}else if(companyKeyLink > 8 && companyKeyLink < 90){
		//alert('its a slamci cr8deposlip page');
		//expand main node of cr8deposlip muna
		mainNodeId = 'deposlipNode';
		subNodeId = 'deposlipSubNodes';
		expandedHeight = '188px';
		expandList(mainNodeId,subNodeId,expandedHeight);
		
		if(companyKeyLink > 8 && companyKeyLink < 14){
			mainNodeId = 'slamcipesoNode';
			subNodeId = 'slamcipesoSubNodes';
			expandedHeight = '106px';
		}else if(companyKeyLink > 13 && companyKeyLink < 19){
			mainNodeId = 'slamcidollarNode';
			subNodeId = 'slamcidollarSubNodes';
			expandedHeight = '46px';
		}else if(companyKeyLink > 18 && companyKeyLink < 29){
			mainNodeId = 'slocpipesoNode';
			subNodeId = 'slocpipesoSubNodes';
			expandedHeight = '106px';
		}else if(companyKeyLink > 28 && companyKeyLink < 39){
			mainNodeId = 'slocpidollarNode';
			subNodeId = 'slocpidollarSubNodes';
			expandedHeight = '106px';
		}else if(companyKeyLink > 38 && companyKeyLink < 49){
			mainNodeId = 'slgfipesoNode';
			subNodeId = 'slgfipesoSubNodes';
			expandedHeight = '159px';
		}else if(companyKeyLink > 48 && companyKeyLink < 59){
			mainNodeId = 'slgfidollarNode';
			subNodeId = 'slgfidollarSubNodes';
			expandedHeight = '106px';
		}else if(companyKeyLink > 38 && companyKeyLink < 69){
			mainNodeId = 'slfpipesoNode';
			subNodeId = 'slfpipesoSubNodes';
			expandedHeight = '106px';
		}else if(companyKeyLink > 69 && companyKeyLink < 75){
			mainNodeId = 'otherpesoNode';
			subNodeId = 'otherpesoSubNodes';
			expandedHeight = '106px';
		}else if(companyKeyLink > 74 && companyKeyLink < 80){
			mainNodeId = 'otherdollarNode';
			subNodeId = 'otherdollarSubNodes';
			expandedHeight = '106px';
		}else if(companyKeyLink > 79 && companyKeyLink < 85){
			mainNodeId = 'slgfipesoNode';
			subNodeId = 'slgfipesoSubNodes';
			expandedHeight = '159px';
		}else if(companyKeyLink > 84 && companyKeyLink < 90){
			mainNodeId = 'slgfidollarNode';
			subNodeId = 'slgfidollarSubNodes';
			expandedHeight = '159px';
		}
		expandList(mainNodeId,subNodeId,expandedHeight);
	}
	highlightLink(companyKeyLink);
}

function toggleNodeVis(mainNodeId, subNodeId){
	var collapsedHeight = '16px';
	var expandedHeight = '124px';
	switch (mainNodeId)
	{
	  case 'collectionsNode':
		expandedHeight = '122px';
	    break;
	  case 'deposlipNode':
		expandedHeight = '188px';
		break;
	  case 'slamcipesoNode':
		expandedHeight = '106px';
		break;
	  case 'slamcidollarNode':
		expandedHeight = '46px';
		break;
	  case 'slocpipesoNode':
		expandedHeight = '106px';
		break;
	  case 'slocpidollarNode':
		expandedHeight = '106px';
		break;
	  case 'slgfipesoNode':
		expandedHeight = '196px';
	    break;
	  case 'slgfidollarNode':
		expandedHeight = '196px';
		break;
	  case 'slfpipesoNode':
		expandedHeight = '106px';
		break;
	  case 'otherpesoNode':
		expandedHeight = '106px';
		break;
	  case 'otherdollarNode':
		expandedHeight = '106px';
		break;
	  default:
		expandedHeight = '124px';
	}
	
	if(document.getElementById(subNodeId).style.display=='block'){
		collapseList(mainNodeId,subNodeId,collapsedHeight);
	}else{
		expandList(mainNodeId,subNodeId,expandedHeight);
	}	
}

function collapseList(mainNodeId,subNodeId,collapsedHeight){//to hide: hide list then cut height
	document.getElementById(subNodeId).style.display='none';
	document.getElementById(mainNodeId).style.height=collapsedHeight;
}
function expandList(mainNodeId,subNodeId,expandedHeight){//to show, add height then show list
	document.getElementById(mainNodeId).style.height=expandedHeight;
	document.getElementById(subNodeId).style.display='block';
}

function highlightLink(companyKeyLink){
	var linkId = '';
	if(companyKeyLink == 1){
		linkId = 'SLAMCIP';
	}else if(companyKeyLink == 2){
		linkId = 'SLAMCID';
	}else if(companyKeyLink == 3){
		linkId = 'SLOCPI';
	}else if(companyKeyLink == 4){
		linkId = 'SLGFI';
	}else if(companyKeyLink == 5){
		linkId = 'SLFPI';
	}else if(companyKeyLink == 6){
		linkId = 'OTHER';
	}else if(companyKeyLink == 9){
		linkId = 'cdsSlamcipCash';
	}else if(companyKeyLink == 10){
		linkId = 'cdsSlamcipChequeOnUs';
	}else if(companyKeyLink == 11){
		linkId = 'cdsSlamcipLocalCheque';
	}else if(companyKeyLink == 12){
		linkId = 'cdsSlamcipRegionalCheque';
	}else if(companyKeyLink == 13){
		linkId = 'cdsSlamcipChequeOT'
	}else if(companyKeyLink == 14){
		linkId = 'cdsSlamcidCash';
	}else if(companyKeyLink == 15){
		linkId = 'cdsSlamcidDollarCheck';
	}else if(companyKeyLink == 16){
		linkId = 'cdsSlamcidUCIM';
	}else if(companyKeyLink == 17){
		linkId = 'cdsSlamcidUCOP';
	}else if(companyKeyLink == 18){
		linkId = 'cdsSlamcidBankOTC';
	}else if(companyKeyLink == 19){
		linkId = 'cdsSlocpipCash';
	}else if(companyKeyLink == 20){
		linkId = 'cdsSlocpipChequeOnUs';
	}else if(companyKeyLink == 21){
		linkId = 'cdsSlocpipLocalCheque';
	}else if(companyKeyLink == 22){
		linkId = 'cdsSlocpipRegionalCheque';
	}else if(companyKeyLink == 23){
		linkId = 'cdsSlocpipChequeOT';
	}else if(companyKeyLink == 24){
		linkId = 'cdsSlocpipGrpCash';
	}else if(companyKeyLink == 25){
		linkId = 'cdsSlocpipGrpChequeOnUs';
	}else if(companyKeyLink == 26){
		linkId = 'cdsSlocpipGrpLocalCheque';
	}else if(companyKeyLink == 27){
		linkId = 'cdsSlocpipGrpRegionalCheque';
	}else if(companyKeyLink == 28){
		linkId = 'cdsSlocpipGrpChequeOT';
	}else if(companyKeyLink == 29){
		linkId = 'cdsSlocpidCash';
	}else if(companyKeyLink == 30){
		linkId = 'cdsSlocpidDollarCheck';
	}else if(companyKeyLink == 31){
		linkId = 'cdsSlocpidUCIM';
	}else if(companyKeyLink == 32){
		linkId = 'cdsSlocpidUCOP';
	}else if(companyKeyLink == 33){
		linkId = 'cdsSlocpidBankOTC';
	}else if(companyKeyLink == 34){
		linkId ;
	}else if(companyKeyLink == 35){
		linkId ;
	}else if(companyKeyLink == 36){
		linkId ;
	}else if(companyKeyLink == 37){
		linkId ;
	}else if(companyKeyLink == 38){
		linkId ;
	}else if(companyKeyLink == 39){
		linkId = 'cdsSlgfipCash';
	}else if(companyKeyLink == 40){
		linkId = 'cdsSlgfipChequeOnUs';
	}else if(companyKeyLink == 41){
		linkId = 'cdsSlgfipLocalCheque';
	}else if(companyKeyLink == 42){
		linkId = 'cdsSlgfipRegionalCheque';
	}else if(companyKeyLink == 43){
		linkId = 'cdsSlgfipChequeOT';
	}else if(companyKeyLink == 44){
		linkId = 'cdsSlgfipUncCash';
	}else if(companyKeyLink == 45){
		linkId = 'cdsSlgfipUncChequeOnUs';
	}else if(companyKeyLink == 46){
		linkId = 'cdsSlgfipUncLocalCheque';
	}else if(companyKeyLink == 47){
		linkId = 'cdsSlgfipUncRegionalCheque';
	}else if(companyKeyLink == 48){
		linkId = 'cdsSlgfipUncChequeOT';
	}else if(companyKeyLink == 49){
		linkId = 'cdsSlgfidCash';
	}else if(companyKeyLink == 50){
		linkId = 'cdsSlgfidDollarCheck';
	}else if(companyKeyLink == 51){
		linkId = 'cdsSlgfidUCIM';
	}else if(companyKeyLink == 52){
		linkId = 'cdsSlgfidUCOP';
	}else if(companyKeyLink == 53){
		linkId = 'cdsSlgfidBankOTC';
	}else if(companyKeyLink == 54){
		linkId = 'cdsSlgfidUncCash';
	}else if(companyKeyLink == 55){
		linkId = 'cdsSlgfidUncDollarCheck';
	}else if(companyKeyLink == 56){
		linkId = 'cdsSlgfidUncUCIM';
	}else if(companyKeyLink == 57){
		linkId = 'cdsSlgfidUncUCOP';
	}else if(companyKeyLink == 58){
		linkId = 'cdsSlgfidUncBankOTC';
	}else if(companyKeyLink == 59){
		linkId = 'cdsSlfpipCash';
	}else if(companyKeyLink == 60){
		linkId = 'cdsSlfpipChequeOnUs';
	}else if(companyKeyLink == 61){
		linkId = 'cdsSlfpipLocalCheque';
	}else if(companyKeyLink == 62){
		linkId = 'cdsSlfpipRegionalCheque';
	}else if(companyKeyLink == 63){
		linkId = 'cdsSlfpipChequeOT';
	}else if(companyKeyLink == 64){
		linkId = 'cdsSlfpipGafCash';
	}else if(companyKeyLink == 65){
		linkId = 'cdsSlfpipGafChequeOnUs';
	}else if(companyKeyLink == 66){
		linkId = 'cdsSlfpipGafLocalCheque';
	}else if(companyKeyLink == 67){
		linkId = 'cdsSlfpipGafRegionalCheque';
	}else if(companyKeyLink == 68){
		linkId = 'cdsSlfpipGafChequeOT';
	}else if(companyKeyLink == 70){
		linkId = 'cdsOtherpCash';
	}else if(companyKeyLink == 71){
		linkId = 'cdsOtherpChequeOnUs';
	}else if(companyKeyLink == 72){
		linkId = 'cdsOtherpLocalCheque';
	}else if(companyKeyLink == 73){
		linkId = 'cdsOtherpRegionalCheque';
	}else if(companyKeyLink == 74){
		linkId = 'cdsOtherpChequeOT';
	}else if(companyKeyLink == 75){
		linkId = 'cdsOtherdCash';
	}else if(companyKeyLink == 76){
		linkId = 'cdsOtherdDollarCheck';
	}else if(companyKeyLink == 77){
		linkId = 'cdsOtherdUCIM';
	}else if(companyKeyLink == 78){
		linkId = 'cdsOtherdUCOP';
	}else if(companyKeyLink == 79){
		linkId = 'cdsOtherdBankOTC';
	}else if(companyKeyLink == 80){
		linkId = 'cdsSlgfipGrpCash';
	}else if(companyKeyLink == 81){
		linkId = 'cdsSlgfipGrpChequeOnUs';
	}else if(companyKeyLink == 82){
		linkId = 'cdsSlgfipGrpLocalCheque';
	}else if(companyKeyLink == 83){
		linkId = 'cdsSlgfipGrpRegionalCheque';
	}else if(companyKeyLink == 84){
		linkId = 'cdsSlgfipGrpChequeOT';
	}else if(companyKeyLink == 85){
		linkId = 'cdsSlgfidGrpCash';
	}else if(companyKeyLink == 86){
		linkId = 'cdsSlgfidGrpDollarCheck';
	}else if(companyKeyLink == 87){
		linkId = 'cdsSlgfidGrpUCIM';
	}else if(companyKeyLink == 88){
		linkId = 'cdsSlgfidGrpUCOP';
	}else if(companyKeyLink == 89){
		linkId = 'cdsSlgfidGrpBankOTC';
	}
	
	
	
	
	
	
	else if(companyKeyLink == 69){
		linkId = 'createVDIL';
	}else{
		linkId = companyKeyLink;
	}
	document.getElementById(linkId).style.backgroundColor='white';
}

