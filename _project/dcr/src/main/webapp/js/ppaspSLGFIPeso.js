var slgfiPesoColumns = [{
	name : "",
	field : "product",
	width : "12%"
},{
	name : "",
	field : "cashier",
	width : "16%"
},{
	name : "TRAD/VUL (PESO)",
	field : "tradVul",
	width : "6%"
},{
	name : "DS",
	field : "dsTV",
	width : "6%"
},{
	name : "VDS",
	field : "vdsTV",
	width : "6%"
},{
	name : "Unconv Peso",
	field : "up",
	width : "7%"
},{
	name : "DS",
	field : "dsUP",
	width : "5%"
},{
	name : "VDS",
	field : "vdsUP",
	width : "5%"
},{
	name : "Group Life",
	field : "gl",
	width : "5%"
},{
	name : "DS",
	field : "dsGL",
	width : "4%"
},{
	name : "VDS",
	field : "vdsGL",
	width : "4%"
},{
	name : "Recon?",
	field : "reconciled",
	width : "4%"
},{
	name : "Recon By",
	field : "reconciledBy",
	width : "4%"
},{
	name : "Recon Date",
	field : "reconciledDate",
	width : "4%"
},{
	name : "CCM NOTES",
	field : "ccmNotes",
	width : "4%"
},{
	name : "CCQA NOTES",
	field : "ppaNotes",
	width : "4%"
},{
	name : "Findings",
	field : "findings",
	width : "4%"
}	
];

var slgfiPesoColumnsNH = [{
	name : "",
	field : "product",
	width : "12%"
},{
	name : "",
	field : "cashier",
	width : "16%"
},{
	name : "",
	field : "tradVul",
	width : "6%"
},{
	name : "",
	field : "dsTV",
	width : "6%"
},{
	name : "",
	field : "vdsTV",
	width : "6%"
},{
	name : "",
	field : "up",
	width : "7%"
},{
	name : "",
	field : "dsUP",
	width : "5%"
},{
	name : "",
	field : "vdsUP",
	width : "5%"
},{
	name : "",
	field : "gl",
	width : "5%"
},{
	name : "",
	field : "dsGL",
	width : "4%"
},{
	name : "",
	field : "vdsGL",
	width : "4%"
},{
	name : "",
	field : "reconciled",
	width : "4%"
},{
	name : "",
	field : "reconciledBy",
	width : "4%"
},{
	name : "",
	field : "reconciledDate",
	width : "4%"
},{
	name : "",
	field : "ccmNotes",
	width : "4%"
},{
	name : "",
	field : "ppaNotes",
	width : "4%"
},{
	name : "",
	field : "findings",
	width : "4%"
}	
];

function generateSLGFIPesoSection(slgfiPesoTableDivId,slgfiPesoDivDetails,slgfiPesoSummaryDivDetails){
	$(document).ready(function(){
			
	mergeSLGFIPesoCashierProductTypes();
	
	var pesoCashHeader = [{product: "<div class='alignLeft' cdTarget='slgfiPesoCash' onclick='collapseDivision(this)'>PESO CASH</div>",cashier:"", tradVul: "",dsTV: "",vdsTV:"",up:"",dsUP:"",vdsUP:"",reconciled:"",reconciledBy:"",reconciledDate:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var cashNonCounterData = generateSLGFIPesoSectionData("totalCashNonCounter","slgfiPesoCash");
	
	var cncTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > Non Counter</div>", tradVul: "<div class='numFormat'>"+cashNonCounterData.cashierTotals.tv+"</div>",gl: "<div class='numFormat'>"+cashNonCounterData.cashierTotals.gl+"</div>", dsTV: "",vdsTV:"",up:"<div class='numFormat'>"+cashNonCounterData.cashierTotals.up+"</div>",dsUP:"",vdsUP:"",reconciled:"",reconciledBy:"",reconciledDate:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var cashCounterData = generateSLGFIPesoSectionData("totalCashCounter","slgfiPesoCash");
	
	var ccTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > Counter</div>", tradVul: "<div class='numFormat'>"+cashCounterData.cashierTotals.tv+"</div>",dsTV: "",vdsTV:"",up:"<div class='numFormat'>"+cashCounterData.cashierTotals.up+"</div>",dsUP:"",vdsUP:"",gl: "<div class='numFormat'>"+cashCounterData.cashierTotals.gl+"</div>",reconciled:"",reconciledBy:"",reconciledDate:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var pesoCashTotals = getSLGFIPesoAreaTotal([cashNonCounterData,cashCounterData]);
	
	var areaCashTotal = [{product: "<div class='totalRow'>&nbsp;</div>",cashier:"<div class='totalRow'><div class='alignRight'>TOTAL CASH</div></div>", tradVul: "<div class='totalRow'><div class='numFormat'>"+pesoCashTotals.tv+"</div></div>",dsTV: "<div class='totalRow'>&nbsp;</div>",vdsTV:"<div class='totalRow'>&nbsp;</div>",gl: "<div class='totalRow'><div class='numFormat'>"+pesoCashTotals.gl+"</div></div>",up:"<div class='totalRow'><div class='numFormat'>"+pesoCashTotals.up+"</div></div>",dsGL:"<div class='totalRow'>&nbsp;</div>",vdsGL:"<div class='totalRow'>&nbsp;</div>",dsUP:"<div class='totalRow'>&nbsp;</div>",vdsUP:"<div class='totalRow'>&nbsp;</div>",reconciled:"<div class='totalRow'>&nbsp;</div>",reconciledBy:"<div class='totalRow'>&nbsp;</div>",reconciledDate:"<div class='totalRow'>&nbsp;</div>",ccmNotes:"<div class='totalRow'>&nbsp;</div>",ppaNotes:"<div class='totalRow'>&nbsp;</div>",findings:"<div class='totalRow'>&nbsp;</div>"}];
	
	var pesoCheckHeader = [{product: "<div class='alignLeft' cdTarget='slgfiPesoCheck' onclick='collapseDivision(this)'>PESO CHECK</div>",cashier:"", tradVul: "",dsTV: "",vdsTV:"",up:"",dsUP:"",vdsUP:"",reconciled:"",reconciledBy:"",reconciledDate:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var checkOnUsData = generateSLGFIPesoSectionData("totalCheckOnUs","slgfiPesoCheck");
	
	var chouTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > On Us</div>", gl: "<div class='numFormat'>"+checkOnUsData.cashierTotals.gl+"</div>",tradVul: "<div class='numFormat'>"+checkOnUsData.cashierTotals.tv+"</div>",dsTV: "",vdsTV:"",up:"<div class='numFormat'>"+checkOnUsData.cashierTotals.up+"</div>",dsUP:"",vdsUP:"",reconciled:"",reconciledBy:"",reconciledDate:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var checkLocalData = generateSLGFIPesoSectionData("totalCheckLocal","slgfiPesoCheck");
	
	var chlTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > Local</div>",  gl: "<div class='numFormat'>"+checkLocalData.cashierTotals.gl+"</div>",tradVul: "<div class='numFormat'>"+checkLocalData.cashierTotals.tv+"</div>",dsTV: "",vdsTV:"",up:"<div class='numFormat'>"+checkLocalData.cashierTotals.up+"</div>",dsUP:"",vdsUP:"",reconciled:"",reconciledBy:"",reconciledDate:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var checkRegionalData = generateSLGFIPesoSectionData("totalCheckRegional","slgfiPesoCheck");
	
	var chrTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > Regional</div>",  gl: "<div class='numFormat'>"+checkRegionalData.cashierTotals.gl+"</div>",tradVul: "<div class='numFormat'>"+checkRegionalData.cashierTotals.tv+"</div>",dsTV: "",vdsTV:"",up:"<div class='numFormat'>"+checkRegionalData.cashierTotals.up+"</div>",dsUP:"",vdsUP:"",reconciled:"",reconciledBy:"",reconciledDate:"",ccmNotes:"",ppaNotes:"",findings:""}];

	var checkNonCounterData = generateSLGFIPesoSectionData("totalCheckNonCounter","slgfiPesoCheck");
	
	var chncTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > Non Counter</div>",  gl: "<div class='numFormat'>"+checkNonCounterData.cashierTotals.gl+"</div>",tradVul: "<div class='numFormat'>"+checkNonCounterData.cashierTotals.tv+"</div>",dsTV: "",vdsTV:"",up:"<div class='numFormat'>"+checkNonCounterData.cashierTotals.up+"</div>",dsUP:"",vdsUP:"",reconciled:"",reconciledBy:"",reconciledDate:"",ccmNotes:"",ppaNotes:"",findings:""}];

//	var checkOTData = generateSLGFIPesoSectionData("totalCheckOT");
	
//	var chotTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > OT</div>",  gl: "<div class='numFormat'>"+checkOTData.cashierTotals.gl+"</div>", tradVul: "<div class='numFormat'>"+checkOTData.cashierTotals.tv+"</div>",dsTV: "",vdsTV:"",up:"<div class='numFormat'>"+checkOTData.cashierTotals.up+"</div>",dsUP:"",vdsUP:"",reconciled:"",reconciledBy:"",reconciledDate:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var postalMoneyOrderData = generateSLGFIPesoSectionData("totalPmo","slgfiPesoCheck");
	
	var postalMoneyOrderTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > Postal Money Order</div>",  gl: "<div class='numFormat'>"+postalMoneyOrderData.cashierTotals.gl+"</div>", tradVul: "<div class='numFormat'>"+postalMoneyOrderData.cashierTotals.tv+"</div>",dsTV: "",vdsTV:"",up:"<div class='numFormat'>"+postalMoneyOrderData.cashierTotals.up+"</div>",dsUP:"",vdsUP:"",reconciled:"",reconciledBy:"",reconciledDate:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	//var creditMemoData = generateSLGFIPesoSectionData("totalCreditMemo");
	
	//var cmTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > Local</div>", tradVul: "<div class='numFormat'>"+creditMemoData.cashierTotals.tv+"</div>",dsTV: "",vdsTV:"",up:"<div class='numFormat'>"+creditMemoData.cashierTotals.up+"</div>",dsUP:"",vdsUP:"",reconciled:"",reconciledBy:"",reconciledDate:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	//var bankOTCCheckData = generateSLGFIPesoSectionData("totalBankOTCCheckPayment");
	
	//var bocTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > Bank OTC Check Payment </div>", tradVul: "<div class='numFormat'>"+bankOTCCheckData.cashierTotals.tv+"</div>",dsTV: "",vdsTV:"",up:"<div class='numFormat'>"+bankOTCCheckData.cashierTotals.up+"</div>",dsUP:"",vdsUP:"",ccmNotes:"",ppaNotes:"",findings:""}];

	var pesoCheckTotals = getSLGFIPesoAreaTotal([checkOnUsData,checkLocalData,checkRegionalData,checkNonCounterData,postalMoneyOrderData]);
	
	var areaCheckTotal = [{product: "<div class='totalRow'>&nbsp;</div>",cashier:"<div class='totalRow'><div class='alignRight'>TOTAL CHECK</div></div>",gl: "<div class='totalRow'><div class='numFormat'>"+pesoCheckTotals.gl+"</div></div>", tradVul: "<div class='totalRow'><div class='numFormat'>"+pesoCheckTotals.tv+"</div></div>",dsGL:"<div class='totalRow'>&nbsp;</div>",vdsGL:"<div class='totalRow'>&nbsp;</div>",dsTV: "<div class='totalRow'>&nbsp;</div>",vdsTV:"<div class='totalRow'>&nbsp;</div>",up:"<div class='totalRow'><div class='numFormat'>"+pesoCheckTotals.up+"</div></div>",dsUP:"<div class='totalRow'>&nbsp;</div>",vdsUP:"<div class='totalRow'>&nbsp;</div>",reconciled:"<div class='totalRow'>&nbsp;</div>",reconciledBy:"<div class='totalRow'>&nbsp;</div>",reconciledDate:"<div class='totalRow'>&nbsp;</div>",ccmNotes:"<div class='totalRow'>&nbsp;</div>",ppaNotes:"<div class='totalRow'>&nbsp;</div>",findings:"<div class='totalRow'>&nbsp;</div>"}];
	
	var nonCashHeader = [{product: "<div class='alignLeft' cdTarget='slgfiPesoNonCash' onclick='collapseDivision(this)'>NON CASH</div>",cashier:"", tradVul: "",dsTV: "",vdsTV:"",up:"",dsUP:"",vdsUP:"",reconciled:"",reconciledBy:"",reconciledDate:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var nonCashData = generateSLGFIPesoSectionData("totalNonCash","slgfiPesoNonCash");
	
	var nonCashTotal = [{product: "<div class='totalRow'>&nbsp;</div>",cashier:"<div class='totalRow'><div class='alignRight'>TOTAL NON CASH</div></div>", gl: "<div class='totalRow'><div class='numFormat'>"+nonCashData.cashierTotals.gl+"</div></div>",dsGL:"<div class='totalRow'>&nbsp;</div>",vdsGL:"<div class='totalRow'>&nbsp;</div>", tradVul: "<div class='totalRow'><div class='numFormat'>"+nonCashData.cashierTotals.tv+"</div></div>",dsTV: "<div class='totalRow'>&nbsp;</div>",vdsTV:"<div class='totalRow'>&nbsp;</div>",up:"<div class='totalRow'><div class='numFormat'>"+nonCashData.cashierTotals.up+"</div></div>",dsUP:"<div class='totalRow'>&nbsp;</div>",vdsUP:"<div class='totalRow'>&nbsp;</div>",reconciled:"<div class='totalRow'>&nbsp;</div>",reconciledBy:"<div class='totalRow'>&nbsp;</div>",reconciledDate:"<div class='totalRow'>&nbsp;</div>",ccmNotes:"<div class='totalRow'>&nbsp;</div>",ppaNotes:"<div class='totalRow'>&nbsp;</div>",findings:"<div class='totalRow'>&nbsp;</div>"}];
	
	var pesoCardHeader = [{product: "<div class='alignLeft' cdTarget='slgfiPesoCard' onclick='collapseDivision(this)'>PESO CARD</div>",cashier:"", tradVul: "",dsTV: "",vdsTV:"",up:"",dsUP:"",vdsUP:"",reconciled:"",reconciledBy:"",reconciledDate:"",ccmNotes:"",ppaNotes:"",findings:""}];
//	
//	var posBPIData = generateSLGFIPesoSectionData("totalPosBpi");
//	
//	var posBPITotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > POS - BPI</div>",  gl: "<div class='numFormat'>"+posBPIData.cashierTotals.gl+"</div>",tradVul: "<div class='numFormat'>"+posBPIData.cashierTotals.tv+"</div>",dsTV: "",vdsTV:"",up:"<div class='numFormat'>"+posBPIData.cashierTotals.up+"</div>",dsUP:"",vdsUP:"",reconciled:"",reconciledBy:"",reconciledDate:"",ccmNotes:"",ppaNotes:"",findings:""}];
//	
//	var posCTIBData = generateSLGFIPesoSectionData("totalPosCtb");
//	
//	var posCTIBTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > POS - CITIBANK</div>", gl: "<div class='numFormat'>"+posCTIBData.cashierTotals.gl+"</div>", tradVul: "<div class='numFormat'>"+posCTIBData.cashierTotals.tv+"</div>",dsTV: "",vdsTV:"",up:"<div class='numFormat'>"+posCTIBData.cashierTotals.up+"</div>",dsUP:"",vdsUP:"",reconciled:"",reconciledBy:"",reconciledDate:"",ccmNotes:"",ppaNotes:"",findings:""}];
//	
//	var posHSBCData = generateSLGFIPesoSectionData("totalPosHsbc");
//	
//	var posHSBCTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > POS - HSBC</div>",  gl: "<div class='numFormat'>"+posHSBCData.cashierTotals.gl+"</div>",tradVul: "<div class='numFormat'>"+posHSBCData.cashierTotals.tv+"</div>",dsTV: "",vdsTV:"",up:"<div class='numFormat'>"+posHSBCData.cashierTotals.up+"</div>",dsUP:"",vdsUP:"",reconciled:"",reconciledBy:"",reconciledDate:"",ccmNotes:"",ppaNotes:"",findings:""}];
//	
//	var posSCBData = generateSLGFIPesoSectionData("totalPosScb");
//	
//	var posSCBTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > POS - SCB</div>", gl: "<div class='numFormat'>"+posSCBData.cashierTotals.gl+"</div>", tradVul: "<div class='numFormat'>"+posSCBData.cashierTotals.tv+"</div>",dsTV: "",vdsTV:"",up:"<div class='numFormat'>"+posSCBData.cashierTotals.up+"</div>",dsUP:"",vdsUP:"",reconciled:"",reconciledBy:"",reconciledDate:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var posRCBCData = generateSLGFIPesoSectionData("totalPosRcbc","slgfiPesoCard");
	
	var posRCBCTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > POS - RCBC</div>",  gl: "<div class='numFormat'>"+posRCBCData.cashierTotals.gl+"</div>",tradVul: "<div class='numFormat'>"+posRCBCData.cashierTotals.tv+"</div>",dsTV: "",vdsTV:"",up:"<div class='numFormat'>"+posRCBCData.cashierTotals.up+"</div>",dsUP:"",vdsUP:"",reconciled:"",reconciledBy:"",reconciledDate:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
//	var posBDOData = generateSLGFIPesoSectionData("totalPosBdo");
//	
//	var posBDOTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > POS - BDO</div>",  gl: "<div class='numFormat'>"+posBDOData.cashierTotals.gl+"</div>",tradVul: "<div class='numFormat'>"+posBDOData.cashierTotals.tv+"</div>",dsTV: "",vdsTV:"",up:"<div class='numFormat'>"+posBDOData.cashierTotals.up+"</div>",dsUP:"",vdsUP:"",reconciled:"",reconciledBy:"",reconciledDate:"",ccmNotes:"",ppaNotes:"",findings:""}];
//	
//	var mdsBPIData = generateSLGFIPesoSectionData("totalMdsBpi");
//	
//	var mdsBPITotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > MDS - BPI</div>", gl: "<div class='numFormat'>"+mdsBPIData.cashierTotals.gl+"</div>", tradVul: "<div class='numFormat'>"+mdsBPIData.cashierTotals.tv+"</div>",dsTV: "",vdsTV:"",up:"<div class='numFormat'>"+mdsBPIData.cashierTotals.up+"</div>",dsUP:"",vdsUP:"",reconciled:"",reconciledBy:"",reconciledDate:"",ccmNotes:"",ppaNotes:"",findings:""}];
//	
//	var mdsBDOData = generateSLGFIPesoSectionData("totalMdsBdo");
//	
//	var mdsBDOTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > MDS - BDO</div>", gl: "<div class='numFormat'>"+mdsBDOData.cashierTotals.gl+"</div>", tradVul: "<div class='numFormat'>"+mdsBDOData.cashierTotals.tv+"</div>",dsTV: "",vdsTV:"",up:"<div class='numFormat'>"+mdsBDOData.cashierTotals.up+"</div>",dsUP:"",vdsUP:"",reconciled:"",reconciledBy:"",reconciledDate:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var mdsRCBCData = generateSLGFIPesoSectionData("totalMdsRcbc","slgfiPesoCard");
	
	var mdsRCBCTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > MDS - RCBC</div>", gl: "<div class='numFormat'>"+mdsRCBCData.cashierTotals.gl+"</div>", tradVul: "<div class='numFormat'>"+mdsRCBCData.cashierTotals.tv+"</div>",dsTV: "",vdsTV:"",up:"<div class='numFormat'>"+mdsRCBCData.cashierTotals.up+"</div>",dsUP:"",vdsUP:"",reconciled:"",reconciledBy:"",reconciledDate:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
//	var mdsSBCData = generateSLGFIPesoSectionData("totalMdsSbc");
//	
//	var mdsSBCTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > MDS - SCB</div>", gl: "<div class='numFormat'>"+mdsSBCData.cashierTotals.gl+"</div>", tradVul: "<div class='numFormat'>"+mdsSBCData.cashierTotals.tv+"</div>",dsTV: "",vdsTV:"",up:"<div class='numFormat'>"+mdsSBCData.cashierTotals.up+"</div>",dsUP:"",vdsUP:"",reconciled:"",reconciledBy:"",reconciledDate:"",ccmNotes:"",ppaNotes:"",findings:""}];
//	
//	var mdsCTBData = generateSLGFIPesoSectionData("totalMdsCtb");
//	
//	var mdsCTBTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > MDS - CITIBANK</div>",  gl: "<div class='numFormat'>"+mdsCTBData.cashierTotals.gl+"</div>",tradVul: "<div class='numFormat'>"+mdsCTBData.cashierTotals.tv+"</div>",dsTV: "",vdsTV:"",up:"<div class='numFormat'>"+mdsCTBData.cashierTotals.up+"</div>",dsUP:"",vdsUP:"",reconciled:"",reconciledBy:"",reconciledDate:"",ccmNotes:"",ppaNotes:"",findings:""}];
//	
//	var mdsHSBCData = generateSLGFIPesoSectionData("totalMdsHsbc");
//	
//	var mdsHSBCTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > MDS - HSBC</div>", gl: "<div class='numFormat'>"+mdsHSBCData.cashierTotals.gl+"</div>", tradVul: "<div class='numFormat'>"+mdsHSBCData.cashierTotals.tv+"</div>",dsTV: "",vdsTV:"",up:"<div class='numFormat'>"+mdsHSBCData.cashierTotals.up+"</div>",dsUP:"",vdsUP:"",reconciled:"",reconciledBy:"",reconciledDate:"",ccmNotes:"",ppaNotes:"",findings:""}];
//	
//	var autoCTBData = generateSLGFIPesoSectionData("totalAutoCtb");
//	
//	var autoCTBTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > CITIBANK (AUTOCHARGE)</div>",  gl: "<div class='numFormat'>"+autoCTBData.cashierTotals.gl+"</div>",tradVul: "<div class='numFormat'>"+autoCTBData.cashierTotals.tv+"</div>",dsTV: "",vdsTV:"",up:"<div class='numFormat'>"+autoCTBData.cashierTotals.up+"</div>",dsUP:"",vdsUP:"",reconciled:"",reconciledBy:"",reconciledDate:"",ccmNotes:"",ppaNotes:"",findings:""}];
//	
//	var autoSCBData = generateSLGFIPesoSectionData("totalAutoScb");
//	
//	var autoSCBTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > SCB (AUTOCHARGE)</div>",  gl: "<div class='numFormat'>"+autoSCBData.cashierTotals.gl+"</div>",tradVul: "<div class='numFormat'>"+autoSCBData.cashierTotals.tv+"</div>",dsTV: "",vdsTV:"",up:"<div class='numFormat'>"+autoSCBData.cashierTotals.up+"</div>",dsUP:"",vdsUP:"",reconciled:"",reconciledBy:"",reconciledDate:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var autoRCBCData = generateSLGFIPesoSectionData("totalAutoRcbc","slgfiPesoCard");
	
	var autoRCBCTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > RCBC - AUTOCHARGE</div>",  gl: "<div class='numFormat'>"+autoRCBCData.cashierTotals.gl+"</div>",tradVul: "<div class='numFormat'>"+autoRCBCData.cashierTotals.tv+"</div>",dsTV: "",vdsTV:"",up:"<div class='numFormat'>"+autoRCBCData.cashierTotals.up+"</div>",dsUP:"",vdsUP:"",reconciled:"",reconciledBy:"",reconciledDate:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
//	var sunlinkOLData = generateSLGFIPesoSectionData("totalSunlinkOnline");
//	
//	var sunlinkOLTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > SUNLINK ONLINE</div>",  gl: "<div class='numFormat'>"+sunlinkOLData.cashierTotals.gl+"</div>",tradVul: "<div class='numFormat'>"+sunlinkOLData.cashierTotals.tv+"</div>",dsTV: "",vdsTV:"",up:"<div class='numFormat'>"+sunlinkOLData.cashierTotals.up+"</div>",dsUP:"",vdsUP:"",reconciled:"",reconciledBy:"",reconciledDate:"",ccmNotes:"",ppaNotes:"",findings:""}];
//	
//	var sunlinkHSBCPData = generateSLGFIPesoSectionData("totalSunlinkHsbcPeso");
//	
//	var sunlinkHSBCPTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > SUNLINK HSBC PESO</div>",  gl: "<div class='numFormat'>"+sunlinkHSBCPData.cashierTotals.gl+"</div>",tradVul: "<div class='numFormat'>"+sunlinkHSBCPData.cashierTotals.tv+"</div>",dsTV: "",vdsTV:"",up:"<div class='numFormat'>"+sunlinkHSBCPData.cashierTotals.up+"</div>",dsUP:"",vdsUP:"",reconciled:"",reconciledBy:"",reconciledDate:"",ccmNotes:"",ppaNotes:"",findings:""}];
//	
//	var epsBPIData = generateSLGFIPesoSectionData("totalEpsBpi");
//	
//	var epsBPITotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > EPS - BPI</div>",  gl: "<div class='numFormat'>"+epsBPIData.cashierTotals.gl+"</div>",tradVul: "<div class='numFormat'>"+epsBPIData.cashierTotals.tv+"</div>",dsTV: "",vdsTV:"",up:"<div class='numFormat'>"+epsBPIData.cashierTotals.up+"</div>",dsUP:"",vdsUP:"",reconciled:"",reconciledBy:"",reconciledDate:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var areaCardTotals = getSLGFIPesoAreaTotal([posRCBCData,mdsRCBCData,autoRCBCData]);			
	
	var pesoCardTotal = [{product: "<div class='totalRow'>&nbsp;</div>",cashier:"<div class='totalRow'><div class='alignRight'>TOTAL PESO CARD</div></div>",gl: "<div class='totalRow'><div class='numFormat'>"+areaCardTotals.gl+"</div></div>",dsGL:"<div class='totalRow'>&nbsp;</div>",vdsGL:"<div class='totalRow'>&nbsp;</div>", tradVul: "<div class='totalRow'><div class='numFormat'>"+areaCardTotals.tv+"</div></div>",dsTV: "<div class='totalRow'>&nbsp;</div>",vdsTV:"<div class='totalRow'>&nbsp;</div>",up:"<div class='totalRow'><div class='numFormat'>"+areaCardTotals.up+"</div></div>",dsUP:"<div class='totalRow'>&nbsp;</div>",vdsUP:"<div class='totalRow'>&nbsp;</div>",reconciled:"<div class='totalRow'>&nbsp;</div>",reconciledBy:"<div class='totalRow'>&nbsp;</div>",reconciledDate:"<div class='totalRow'>&nbsp;</div>",ccmNotes:"<div class='totalRow'>&nbsp;</div>",ppaNotes:"<div class='totalRow'>&nbsp;</div>",findings:"<div class='totalRow'>&nbsp;</div>"}];
	
	var nonPostedFromDTRHeader = [{product: "<div class='alignLeft' cdTarget='slgfiPesoNonPosted' onclick='collapseDivision(this)'>NON POSTED (from DTR)</div>",cashier:"", tradVul: "",dsTV: "",vdsTV:"",up:"",dsUP:"",vdsUP:"",reconciled:"",reconciledBy:"",reconciledDate:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var nbIndividualData = generateSLGFIPesoSectionData("totalNewBusinessIndividual","slgfiPesoNonPosted");
	
	var nbIndividualTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > NB Individual</div>",  gl: "<div class='numFormat'>"+nbIndividualData.cashierTotals.gl+"</div>",tradVul: "<div class='numFormat'>"+nbIndividualData.cashierTotals.tv+"</div>",dsTV: "",vdsTV:"",up:"<div class='numFormat'>"+nbIndividualData.cashierTotals.up+"</div>",dsUP:"",vdsUP:"",reconciled:"",reconciledBy:"",reconciledDate:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var nbVULData = generateSLGFIPesoSectionData("totalNewBusinessVariable","slgfiPesoNonPosted");
	
	var nbVULTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > NB VUL</div>",  gl: "<div class='numFormat'>"+nbVULData.cashierTotals.gl+"</div>",tradVul: "<div class='numFormat'>"+nbVULData.cashierTotals.tv+"</div>",dsTV: "",vdsTV:"",up:"<div class='numFormat'>"+nbVULData.cashierTotals.up+"</div>",dsUP:"",vdsUP:"",reconciled:"",reconciledBy:"",reconciledDate:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var individualRenewalData = generateSLGFIPesoSectionData("totalIndividualRenewal","slgfiPesoNonPosted");
	
	var individualRenewalTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > Individual Renewal</div>",  gl: "<div class='numFormat'>"+individualRenewalData.cashierTotals.gl+"</div>",tradVul: "<div class='numFormat'>"+individualRenewalData.cashierTotals.tv+"</div>",dsTV: "",vdsTV:"",up:"<div class='numFormat'>"+individualRenewalData.cashierTotals.up+"</div>",dsUP:"",vdsUP:"",reconciled:"",reconciledBy:"",reconciledDate:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var vulRenewalData = generateSLGFIPesoSectionData("totalVariableRenewal","slgfiPesoNonPosted");
	
	var vulRenewalTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > VUL Renewal</div>",  gl: "<div class='numFormat'>"+vulRenewalData.cashierTotals.gl+"</div>",tradVul: "<div class='numFormat'>"+vulRenewalData.cashierTotals.tv+"</div>",dsTV: "",vdsTV:"",up:"<div class='numFormat'>"+vulRenewalData.cashierTotals.up+"</div>",dsUP:"",vdsUP:"",reconciled:"",reconciledBy:"",reconciledDate:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var nonCashDTRData = generateSLGFIPesoSectionData("totalNonCashFromDTR","slgfiPesoFMP");
	
	var nonCashDTRTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > Non Cash</div>",  gl: "<div class='numFormat'>"+nonCashDTRData.cashierTotals.gl+"</div>",tradVul: "<div class='numFormat'>"+nonCashDTRData.cashierTotals.tv+"</div>",dsTV: "",vdsTV:"",up:"<div class='numFormat'>"+nonCashDTRData.cashierTotals.up+"</div>",dsUP:"",vdsUP:"",reconciled:"",reconciledBy:"",reconciledDate:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var areaNonPostedTotals = getSLGFIPesoAreaTotal([nbIndividualData,nbVULData,individualRenewalData,vulRenewalData,nonCashDTRData]);
	
	var nonPostedTotal = [{product: "<div class='totalRow'>&nbsp;</div>",cashier:"<div class='totalRow'><div class='alignRight'>TOTAL NON-POSTED</div></div>", gl: "<div class='totalRow'><div class='numFormat'>"+areaNonPostedTotals.gl+"</div></div>",dsGL:"<div class='totalRow'>&nbsp;</div>",vdsGL:"<div class='totalRow'>&nbsp;</div>",tradVul: "<div class='totalRow'><div class='numFormat'>"+areaNonPostedTotals.tv+"</div></div>",dsTV: "<div class='totalRow'>&nbsp;</div>",vdsTV:"<div class='totalRow'>&nbsp;</div>",up:"<div class='totalRow'><div class='numFormat'>"+areaNonPostedTotals.up+"</div></div>",dsUP:"<div class='totalRow'>&nbsp;</div>",vdsUP:"<div class='totalRow'>&nbsp;</div>",reconciled:"<div class='totalRow'>&nbsp;</div>",reconciledBy:"<div class='totalRow'>&nbsp;</div>",reconciledDate:"<div class='totalRow'>&nbsp;</div>",ccmNotes:"<div class='totalRow'>&nbsp;</div>",ppaNotes:"<div class='totalRow'>&nbsp;</div>",findings:"<div class='totalRow'>&nbsp;</div>"}];
	
	var forManualPostingHeader = [{product: "<div class='alignLeft' cdTarget='slgfiPesoFMP' onclick='collapseDivision(this)'>FOR MANUAL POSTING</div>",cashier:"", tradVul: "",dsTV: "",vdsTV:"",up:"",dsUP:"",vdsUP:"",reconciled:"",reconciledBy:"",reconciledDate:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var nonPolicyFMPData = generateSLGFIPesoSectionData("totalNonPolicy","slgfiPesoFMP");
	
	var nonPolicyFMPTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > Non Policy</div>",  gl: "<div class='numFormat'>"+nonPolicyFMPData.cashierTotals.gl+"</div>",tradVul: "<div class='numFormat'>"+nonPolicyFMPData.cashierTotals.tv+"</div>",dsTV: "",vdsTV:"",up:"<div class='numFormat'>"+nonPolicyFMPData.cashierTotals.up+"</div>",dsUP:"",vdsUP:"",reconciled:"",reconciledBy:"",reconciledDate:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var worksiteNotPostedFMPData = generateSLGFIPesoSectionData("totalWorksiteNonPosted","slgfiPesoFMP");
	
	var worksiteNotPostedFMPTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > Worksite</div>",  gl: "<div class='numFormat'>"+worksiteNotPostedFMPData.cashierTotals.gl+"</div>",tradVul: "<div class='numFormat'>"+worksiteNotPostedFMPData.cashierTotals.tv+"</div>",dsTV: "",vdsTV:"",up:"<div class='numFormat'>"+worksiteNotPostedFMPData.cashierTotals.up+"</div>",dsUP:"",vdsUP:"",reconciled:"",reconciledBy:"",reconciledDate:"",ccmNotes:"",ppaNotes:"",findings:""}];		
	
	var areaForManualPostingTotals = getSLGFIPesoAreaTotal([nonPolicyFMPData,worksiteNotPostedFMPData]);
	
	var forManualPostingotal = [{product: "<div class='totalRow'>&nbsp;</div>",cashier:"<div class='totalRow'><div class='alignRight'>TOTAL FOR MANUAL POSTING</div></div>",gl: "<div class='totalRow'><div class='numFormat'>"+areaForManualPostingTotals.gl+"</div></div>",dsGL:"<div class='totalRow'>&nbsp;</div>",vdsGL:"<div class='totalRow'>&nbsp;</div>", tradVul: "<div class='totalRow'><div class='numFormat'>"+areaForManualPostingTotals.tv+"</div></div>",dsTV: "<div class='totalRow'>&nbsp;</div>",vdsTV:"<div class='totalRow'>&nbsp;</div>",up:"<div class='totalRow'><div class='numFormat'>"+areaForManualPostingTotals.up+"</div></div>",dsUP:"<div class='totalRow'>&nbsp;</div>",vdsUP:"<div class='totalRow'>&nbsp;</div>",reconciled:"<div class='totalRow'>&nbsp;</div>",reconciledBy:"<div class='totalRow'>&nbsp;</div>",reconciledDate:"<div class='totalRow'>&nbsp;</div>",ccmNotes:"<div class='totalRow'>&nbsp;</div>",ppaNotes:"<div class='totalRow'>&nbsp;</div>",findings:"<div class='totalRow'>&nbsp;</div>"}];
	
	var exemptionHeader = [{product: "<div class='alignLeft' cdTarget='slgfiPesoExemptions' onclick='collapseDivision(this)'>EXEMPTIONS</div>",cashier:"", tradVul: "",dsTV: "",vdsTV:"",up:"",dsUP:"",vdsUP:"",reconciled:"",reconciledBy:"",reconciledDate:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var exemptionData = generateSLGFIPesoSectionData("reversalProduct","slgfiPesoExemptions");
	
	var exemptionTotal = [{product: "<div class='totalRow'>&nbsp;</div>",cashier:"<div class='totalRow'><div class='alignRight'>TOTAL EXEMPTIONS</div></div>",gl: "<div class='totalRow'><div class='numFormat'>"+exemptionData.cashierTotals.gl+"</div></div>",dsGL:"<div class='totalRow'>&nbsp;</div>",vdsGL:"<div class='totalRow'>&nbsp;</div>", tradVul: "<div class='totalRow'><div class='numFormat'>"+exemptionData.cashierTotals.tv+"</div></div>",dsTV: "<div class='totalRow'>&nbsp;</div>",vdsTV:"<div class='totalRow'>&nbsp;</div>",up:"<div class='totalRow'><div class='numFormat'>"+exemptionData.cashierTotals.up+"</div></div>",dsUP:"<div class='totalRow'>&nbsp;</div>",vdsUP:"<div class='totalRow'>&nbsp;</div>",reconciled:"<div class='totalRow'>&nbsp;</div>",reconciledBy:"<div class='totalRow'>&nbsp;</div>",reconciledDate:"<div class='totalRow'>&nbsp;</div>",ccmNotes:"<div class='totalRow'>&nbsp;</div>",ppaNotes:"<div class='totalRow'>&nbsp;</div>",findings:"<div class='totalRow'>&nbsp;</div>"}];
	
	var sessionTotalHeader = [{product: "<div class='alignLeft' cdTarget='slgfiPesoST' onclick='collapseDivision(this)'>SESSION TOTALS</div>",cashier:"", tradVul: "",dsTV: "",vdsTV:"",up:"",dsUP:"",vdsUP:"",reconciled:"",reconciledBy:"",reconciledDate:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var sessionTotalData = generateSLGFIPesoSectionData("sessionTotal","slgfiPesoST");
	
	var sessionTotalTotal = [{product: "<div class='totalRow'>&nbsp;</div>",cashier:"<div class='totalRow'><div class='alignRight'>SESSION TOTAL</div></div>",gl: "<div class='totalRow'><div class='numFormat'>"+sessionTotalData.cashierTotals.gl+"</div></div>",dsGL:"<div class='totalRow'>&nbsp;</div>",vdsGL:"<div class='totalRow'>&nbsp;</div>", tradVul: "<div class='totalRow'><div class='numFormat'>"+sessionTotalData.cashierTotals.tv+"</div></div>",dsTV: "<div class='totalRow'>&nbsp;</div>",vdsTV:"<div class='totalRow'>&nbsp;</div>",up:"<div class='totalRow'><div class='numFormat'>"+sessionTotalData.cashierTotals.up+"</div></div>",dsUP:"<div class='totalRow'>&nbsp;</div>",vdsUP:"<div class='totalRow'>&nbsp;</div>",reconciled:"<div class='totalRow'><input type='hidden' id='gfPhpActiveCB' name='gfPhpActiveCB' value='"+ppaGfPesoActive+"'/><div>"+ppaGfPesoActive+"/"+ppaGfPesoTotal+"</div></div>",reconciledBy:"<div class='totalRow'>&nbsp;</div>",reconciledDate:"<div class='totalRow'>&nbsp;</div>",ccmNotes:"<div class='totalRow'>&nbsp;</div>",ppaNotes:"<div class='totalRow'>&nbsp;</div>",findings:"<div class='totalRow'>&nbsp;</div>"}];
	
	var totalSLGFIPCashiersCollection = getSLGFIPesoAreaTotal([nonCashData,cashNonCounterData,cashCounterData,checkOnUsData,checkLocalData,checkRegionalData,checkNonCounterData,postalMoneyOrderData,posRCBCData,mdsRCBCData,autoRCBCData]);
	
	var cashierTotalHeader = [{product: "<div class='alignLeft' cdTarget='slgfiPesoGTPC' onclick='collapseDivision(this)'>Grand Total Per Cashier</div>",cashier:"", tradVul: "",dsTV: "",vdsTV:"",up:"",dsUP:"",vdsUP:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var cashierTotal = getSLGFIPGrandTotalPerCashier("slgfiPesoGTPC");
	
	var cashierTotalRow = [{product: "<div class='totalRow'>&nbsp;</div>",cashier:"<div class='totalRow'><div class='alignRight'>TOTAL COLLECTION</div></div>",gl: "<div class='totalRow'><div class='numFormat'>"+numberWithCommas(totalSLGFIPCashiersCollection.gl)+"</div></div>",dsGL:"<div class='totalRow'>&nbsp;</div>",vdsGL:"<div class='totalRow'>&nbsp;</div>", tradVul: "<div class='totalRow'><div class='numFormat'>"+numberWithCommas(totalSLGFIPCashiersCollection.tv)+"</div></div>",dsTV: "<div class='totalRow'>&nbsp;</div>",vdsTV:"<div class='totalRow'>&nbsp;</div>",up:"<div class='totalRow'><div class='numFormat'>"+numberWithCommas(totalSLGFIPCashiersCollection.up)+"</div></div>",dsUP:"<div class='totalRow'>&nbsp;</div>",vdsUP:"<div class='totalRow'>&nbsp;</div>",reconciled:"<div class='totalRow'>&nbsp;</div>",reconciledBy:"<div class='totalRow'>&nbsp;</div>",reconciledDate:"<div class='totalRow'>&nbsp;</div>",ccmNotes:"<div class='totalRow'>&nbsp;</div>",ppaNotes:"<div class='totalRow'>&nbsp;</div>",findings:"<div class='totalRow'>&nbsp;</div>"}];
	
	var slgfiPesoSummaryData = [{product: "",cashier:"CONSOLIDATED GRAND TOTAL", tradVul: "<div class='numFormat'>"+numberWithCommas(slgfiPesoConsolidatedTotals.tv)+"</div>",gl:"<div class='numFormat'>"+numberWithCommas(slgfiPesoConsolidatedTotals.gl)+"</div>",dsTV: "",vdsTV:"",up:"<div class='numFormat'>"+numberWithCommas(slgfiPesoConsolidatedTotals.up)+"</div>",dsUP:"",vdsUP:"",reconciled:"",reconciledBy:"",reconciledDate:"",ccmNotes:"",ppaNotes:"",findings:""},
		                        {product: "",cashier:"TOTAL CASHIERS COLLECTION", tradVul:"<div class='numFormat'>"+numberWithCommas(totalSLGFIPCashiersCollection.tv)+"</div>",gl:"<div class='numFormat'>"+numberWithCommas(totalSLGFIPCashiersCollection.gl)+"</div>",dsTV: "",vdsTV:"",up:"<div class='numFormat'>"+numberWithCommas(totalSLGFIPCashiersCollection.up)+"</div>",dsUP:"",vdsUP:"",reconciled:"",reconciledBy:"",reconciledDate:"",ccmNotes:"",ppaNotes:"",findings:""},
		                        {product: "",cashier:"", tradVul: compareTotalsImg(numberWithCommas(slgfiPesoConsolidatedTotals.tv),totalSLGFIPCashiersCollection.tv),dsTV: "",gl:"<div class='numFormat'>"+compareTotalsImg(numberWithCommas(slgfiPesoConsolidatedTotals.gl),totalSLGFIPCashiersCollection.gl)+"</div>",vdsTV:"",up:compareTotalsImg(numberWithCommas(slgfiPesoConsolidatedTotals.up),totalSLGFIPCashiersCollection.up),dsUP:"",vdsUP:"",reconciled:"",reconciledBy:"",reconciledDate:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	new TableContainer({
		divName : slgfiPesoSummaryDivDetails.divName,
		nodeName : slgfiPesoSummaryDivDetails.nodeName,
		width : "1210px",
		id : slgfiPesoSummaryDivDetails.tableId,
		layout : slgfiPesoColumns,
		tableClass: "balancingToolSummary",
		data: slgfiPesoSummaryData
	}).startUp();
	
	var slgfiPesoSectionData = [];
	
	appendJsons([slgfiPesoSectionData,pesoCashHeader,cashNonCounterData.prodTotals,cncTotal,cashCounterData.prodTotals,ccTotal,areaCashTotal,pesoCheckHeader,checkOnUsData.prodTotals,chouTotal,checkLocalData.prodTotals,chlTotal,checkRegionalData.prodTotals,chrTotal,checkNonCounterData.prodTotals,chncTotal,postalMoneyOrderData.prodTotals,postalMoneyOrderTotal,areaCheckTotal,nonCashHeader,nonCashData.prodTotals,nonCashTotal,pesoCardHeader,posRCBCData.prodTotals,posRCBCTotal,mdsRCBCData.prodTotals,mdsRCBCTotal,autoRCBCData.prodTotals,autoRCBCTotal,pesoCardTotal,cashierTotalHeader,cashierTotal,cashierTotalRow,nonPostedFromDTRHeader,nbIndividualData.prodTotals,nbIndividualTotal,nbVULData.prodTotals,nbVULTotal,individualRenewalData.prodTotals,individualRenewalTotal,vulRenewalData.prodTotals,vulRenewalTotal,nonPostedTotal,forManualPostingHeader,nonCashDTRData.prodTotals,nonCashDTRTotal,nonPolicyFMPData.prodTotals,nonPolicyFMPTotal,worksiteNotPostedFMPData.prodTotals,worksiteNotPostedFMPTotal,forManualPostingotal,exemptionHeader,exemptionData.prodTotals,exemptionTotal,sessionTotalHeader,sessionTotalData.prodTotals,sessionTotalTotal,generateSnapshotSection(slgfiSnapshot)]);
	
	new TableContainer({
		divName : slgfiPesoDivDetails.divName,
		nodeName : slgfiPesoDivDetails.nodeName,
		width : "1210px",
		id : slgfiPesoDivDetails.tableId,
		layout : slgfiPesoColumnsNH,
		tableClass: "balancingToolSummary",
		data: slgfiPesoSectionData
	}).startUp();
	
	var slgfiPesoTableDiv = document.getElementById(slgfiPesoTableDivId);

	slgfiPesoTableDiv.style.display = "none";
	
	});
}


var slgfiPesoSecData = [];

function mergeSLGFIPesoCashierProductTypes(){
	var cashierRep = [];
	for(c in slgfiPesoData){
		//using jQuery.inArray instead of Array.indexOf because it is not supported in IE8 and below. thanks IE.
		if(jQuery.inArray(slgfiPesoData[c].cashierId+"~"+slgfiPesoData[c].product,cashierRep) == -1){
			var pushedData = "";
			pushedData = pushedData.concat(slgfiPesoData[c].cashierId);
			pushedData = pushedData.concat("~");
			pushedData = pushedData.concat(slgfiPesoData[c].product);
			cashierRep.push(pushedData);
		}
	}
	
	for(var a = 0; a < cashierRep.length; a++){
		slgfiPesoSecData.push(mergeSLGFIPesoCashierData(cashierRep[a]));
	}
		
}

function getSLGFIPesoAreaTotal(area){
	var tots = {};
	var tv = 0;
	var up = 0;
	var gl = 0;
	for(a in area){
		tv = tv + parseFloat(area[a].cashierTotals.tv.replace(/,/g,""));
		up = up + parseFloat(area[a].cashierTotals.up.replace(/,/g,""));
		gl = gl + parseFloat(area[a].cashierTotals.gl.replace(/,/g,""));
	}
	tots.tv = numberWithCommas(parseFloat(tv).toFixed(2));
	tots.up = numberWithCommas(parseFloat(up).toFixed(2));
	tots.gl = numberWithCommas(parseFloat(gl).toFixed(2));
	return tots;
}

function generateSLGFIPesoSectionData(section,cdTarget){
	var sectionObj = {};
	var sectionCashierTotals = {};
	var tv = 0;
	var up = 0;
	var gl = 0;
	var  returnCNC = [];
	var firstRowFlag = true;
	for(a in slgfiPesoSecData){
		var tempCNC = {};
		if(slgfiPesoSecData[a].product == section){
			var isCashDepo=false;
			var isCheckDepo=false;
			if(firstRowFlag){
				switch(section){
					case "totalCashNonCounter": tempCNC.product = "<div class='alignLeft'>Non Counter</div>";break;
					case "totalCashCounter": tempCNC.product = "<div class='alignLeft'>Counter</div>";isCashDepo=true;break;
					case "totalCheckOnUs": tempCNC.product = "<div class='alignLeft'>On Us</div>";isCheckDepo=true;checkType="ON_US";break;
					case "totalCheckRegional": tempCNC.product = "<div class='alignLeft'>Regional</div>";isCheckDepo=true;checkType="REGIONAL";break;
					case "totalCheckNonCounter": tempCNC.product = "<div class='alignLeft'>Non Counter</div>";break;
					case "totalCheckOT": tempCNC.product = "<div class='alignLeft'>OT</div>";isCheckDepo=true;checkType="OT";break;
					case "totalCheckLocal": tempCNC.product = "<div class='alignLeft'>Local</div>";isCheckDepo=true;checkType="LOCAL";break;
					case "totalPmo": tempCNC.product = "<div class='alignLeft'>Postal Money Order</div>";break;
					//case "totalCreditMemo" : tempCNC.product = "<div class='alignLeft'>Credit Memo</div>";break;
					case "totalUsCheckInManila" : tempCNC.product = "<div class='alignLeft'>US Cheque drawn in Manila</div>";break;
					case "totalUsCheckOutPh" : tempCNC.product = "<div class='alignLeft'>US Cheque drawn outside PH</div>";break;
					case "totalPesoCheque" : tempCNC.product = "<div class='alignLeft'>Peso Cheque</div>";break;
					//case "totalBankOTCCheckPayment" : tempCNC.product = "<div class='alignLeft'>Bank OTC Check Payment</div>";break;
					case "totalNonCash": tempCNC.product = "<div class='alignLeft'>Non Cash</div>";break;
					case "totalPosBpi": tempCNC.product = "<div class='alignLeft'>POS - BPI</div>";break;
					case "totalPosCtb": tempCNC.product = "<div class='alignLeft'>POS - CITIBANK</div>";break;
					case "totalPosHsbc": tempCNC.product = "<div class='alignLeft'>POS - HSBC</div>";break;
					case "totalPosScb": tempCNC.product = "<div class='alignLeft'>POS - SCB</div>";break;
					case "totalPosRcbc": tempCNC.product = "<div class='alignLeft'>POS - RCBC</div>";break;
					case "totalPosBdo": tempCNC.product = "<div class='alignLeft'>POS - BDO</div>";break;
					case "totalMdsBpi": tempCNC.product = "<div class='alignLeft'>MDS - BPI</div>";break;
					case "totalMdsBdo": tempCNC.product = "<div class='alignLeft'>MDS - BDO</div>";break;
					case "totalMdsRcbc": tempCNC.product = "<div class='alignLeft'>MDS - RCBC</div>";break;
					case "totalMdsSbc": tempCNC.product = "<div class='alignLeft'>MDS - SCB</div>";break;
					case "totalMdsCtb": tempCNC.product = "<div class='alignLeft'>MDS - CITIBANK</div>";break;
					case "totalMdsHsbc": tempCNC.product = "<div class='alignLeft'>MDS - HSBC</div>";break;
					case "totalAutoCtb": tempCNC.product = "<div class='alignLeft'>CITIBANK (AUTOCHARGE)</div>";break;
					case "totalAutoScb": tempCNC.product = "<div class='alignLeft'>SCB (AUTOCHARGE)</div>";break;
					case "totalAutoRcbc": tempCNC.product = "<div class='alignLeft'>RCBC - AUTOCHARGE</div>";break;
					case "totalSunlinkOnline": tempCNC.product = "<div class='alignLeft'>SUNLINK ONLINE</div>";break;
					case "totalSunlinkHsbcPeso": tempCNC.product = "<div class='alignLeft'>SUNLINK HSBC PESO</div>";break;
					case "totalEpsBpi": tempCNC.product = "<div class='alignLeft'>EPS - BPI</div>";break;
					case "totalNewBusinessIndividual": tempCNC.product = "<div class='alignLeft'>New Business Individual</div>";break;
					case "totalNewBusinessVariable": tempCNC.product = "<div class='alignLeft'>NB VUL</div>";break;
					case "totalIndividualRenewal": tempCNC.product = "<div class='alignLeft'>Individual Renewal</div>";break;
					case "totalVariableRenewal": tempCNC.product = "<div class='alignLeft'>VUL Renewal</div>";break;
					case "totalNonCashFromDTR": tempCNC.product = "<div class='alignLeft'>Non Cash</div>";break;
					case "totalNonPolicy": tempCNC.product = "<div class='alignLeft'>Non Policy</div>";break;
					case "totalWorksiteNonPosted": tempCNC.product = "<div class='alignLeft'>Worksite (not posted)</div>";break;
					case "reversalProduct" : tempCNC.product = "";break;
					case "sessionTotal" : tempCNC.product = "";break;
					default: alert("Unknown Product Total: " + section); break;
				}
			} else{
				tempCNC.product = "";		
				switch(section){
					case "totalCashCounter": isCashDepo=true;break;
					case "totalCheckOnUs": isCheckDepo=true;checkType="ON_US";break;
					case "totalCheckRegional": isCheckDepo=true;checkType="REGIONAL";break;
					case "totalCheckOT": isCheckDepo=true;checkType="OT";break;
					case "totalCheckLocal": isCheckDepo=true;checkType="LOCAL";break;
				}				
			}
			tempCNC.cashier = "<div class='alignLeft'>"+slgfiPesoSecData[a].cashierName+"</div>";
			if(isSLGFIConfirmed(slgfiPesoSecData[a].cashierId)){
				if(section == "reversalProduct"){
					if(parseFloat(slgfiPesoSecData[a].tradVul) != 0){
						tempCNC.tradVul = "<div class='alignRight'><a style='cursor:pointer;text-decoration:underline;color:blue;'onclick='openSPReversal("+getBalancingToolProductId(slgfiPesoSecData[a].cashierId,"RL_RV_PESO",section,slgfiPesoData)+");'>"+numberWithCommas(parseFloat(slgfiPesoSecData[a].tradVul).toFixed(2))+"</a></div>";
					} else{
						tempCNC.tradVul = "<div class='alignRight'>"+numberWithCommas(parseFloat(slgfiPesoSecData[a].tradVul).toFixed(2))+"</div>";
					}					
					if(parseFloat(slgfiPesoSecData[a].up) != 0){
						tempCNC.up = "<div class='alignRight'><a style='cursor:pointer;text-decoration:underline;color:blue;'onclick='openSPReversal("+getBalancingToolProductId(slgfiPesoSecData[a].cashierId,"RU_PESO",section,slgfiPesoData)+");'>"+numberWithCommas(parseFloat(slgfiPesoSecData[a].up).toFixed(2))+"</a></div>";
					} else{
						tempCNC.up = "<div class='alignRight'>"+numberWithCommas(parseFloat(slgfiPesoSecData[a].up).toFixed(2))+"</div>";
					}					
					if(parseFloat(slgfiPesoSecData[a].gl) != 0){
						tempCNC.gl = "<div class='alignRight'><a style='cursor:pointer;text-decoration:underline;color:blue;'onclick='openSPReversal("+getBalancingToolProductId(slgfiPesoSecData[a].cashierId,"RG_PESO",section,slgfiPesoData)+");'>"+numberWithCommas(parseFloat(slgfiPesoSecData[a].gl).toFixed(2))+"</a></div>";
					} else{
						tempCNC.gl = "<div class='alignRight'>"+numberWithCommas(parseFloat(slgfiPesoSecData[a].gl).toFixed(2))+"</div>";
					}				
				} else{
					tempCNC.tradVul = "<div class='alignRight'>"+numberWithCommas(parseFloat(slgfiPesoSecData[a].tradVul).toFixed(2))+"</div>";
					tempCNC.up = "<div class='alignRight'>"+numberWithCommas(parseFloat(slgfiPesoSecData[a].up).toFixed(2))+"</div>";
					tempCNC.gl = "<div class='alignRight'>"+numberWithCommas(parseFloat(slgfiPesoSecData[a].gl).toFixed(2))+"</div>";
					if(isCashDepo){
						if(parseFloat(slgfiPesoSecData[a].tradVul) > 0){
							tempCNC.dsTV  = getCashDepoURL("SLGFI","TRAD/VUL","PHP",slgfiPesoSecData[a].cashierId);
							tempCNC.vdsTV  = getValidatedCashDepoURL("SLGFI","TRAD/VUL","PHP",slgfiPesoSecData[a].cashierId);
						}
						if(parseFloat(slgfiPesoSecData[a].up) > 0){
							tempCNC.dsUP = getCashDepoURL("SLGFI","UNCOVERTED","PHP",slgfiPesoSecData[a].cashierId);
							tempCNC.vdsUP = getValidatedCashDepoURL("SLGFI","UNCOVERTED","PHP",slgfiPesoSecData[a].cashierId);
						}
						if(parseFloat(slgfiPesoSecData[a].gl) > 0){
							tempCNC.dsGL = getCashDepoURL("SLGFI","GROUP","PHP",slgfiPesoSecData[a].cashierId);
							tempCNC.vdsGL = getValidatedCashDepoURL("SLGFI","GROUP","PHP",slgfiPesoSecData[a].cashierId);
						}
					}else if(isCheckDepo){
						if(parseFloat(slgfiPesoSecData[a].tradVul) > 0){
							tempCNC.dsTV  = getChequeDepoURL("SLGFI","TRAD/VUL","PHP",slgfiPesoSecData[a].cashierId,checkType);
							tempCNC.vdsTV  = getValidatedChequeDepoURL("SLGFI","TRAD/VUL","PHP",slgfiPesoSecData[a].cashierId,checkType);
						}
						if(parseFloat(slgfiPesoSecData[a].up) > 0){
							tempCNC.dsUP = getChequeDepoURL("SLGFI","UNCOVERTED","PHP",slgfiPesoSecData[a].cashierId,checkType);
							tempCNC.vdsUP = getValidatedChequeDepoURL("SLGFI","UNCOVERTED","PHP",slgfiPesoSecData[a].cashierId,checkType);
						}
						if(parseFloat(slgfiPesoSecData[a].gl) > 0){
							tempCNC.dsGL = getChequeDepoURL("SLGFI","GROUP","PHP",slgfiPesoSecData[a].cashierId,checkType);
							tempCNC.vdsGL = getValidatedChequeDepoURL("SLGFI","GROUP","PHP",slgfiPesoSecData[a].cashierId,checkType)
						}
					}
				}
				tv = tv + parseFloat(slgfiPesoSecData[a].tradVul);
				up = up + parseFloat(slgfiPesoSecData[a].up);
				gl = gl + parseFloat(slgfiPesoSecData[a].gl);
			} else{
				tempCNC.tradVul = "<div class='alignCenter'>-</div>";
				tempCNC.up = "<div class='alignCenter'>-</div>";
				tempCNC.gl = "<div class='alignCenter'>-</div>";
			}
			if(jQuery.inArray(section,excludeFromCashierTotal) == -1){
				slgfiPesoConsolidatedTotals.tv = (parseFloat(slgfiPesoConsolidatedTotals.tv) + parseFloat(slgfiPesoSecData[a].tradVul)).toFixed(2);
				slgfiPesoConsolidatedTotals.gl = (parseFloat(slgfiPesoConsolidatedTotals.gl) + parseFloat(slgfiPesoSecData[a].gl)).toFixed(2);
				slgfiPesoConsolidatedTotals.up = (parseFloat(slgfiPesoConsolidatedTotals.up) + parseFloat(slgfiPesoSecData[a].up)).toFixed(2);
			}
			if(jQuery.inArray(section,excludeFromReconCheckbox) == -1){
				var isWholeRowZero = false;
				var isMDS = false;
				if(parseFloat(slgfiPesoSecData[a].tradVul) == 0 && parseFloat(slgfiPesoSecData[a].up) == 0 && parseFloat(slgfiPesoSecData[a].gl) == 0){
					isWholeRowZero = true;
				}
				if(jQuery.inArray(section,mdsProdTotals) != -1){
					isMDS = true;
				}
				tempCNC.reconciled = "<div class='alignCenter'>"+getReconCheckbox(rowIndex, isWholeRowZero, isMDS, "GFPHP")+"</div>";
				tempCNC.reconciledBy = "<div class='alignCenter'>"+getReconcilerName(rowIndex, isWholeRowZero, isMDS)+"</div>";
				tempCNC.reconciledDate = "<div class='alignCenter'>"+getReconciledDate(rowIndex)+"</div>";
			}else{
				tempCNC.reconciled = "<div class='alignCenter'>-</div>";
				tempCNC.reconciledBy = "<div class='alignCenter'>-</div>";
				tempCNC.reconciledDate = "<div class='alignCenter'>-</div>";
			}
			tempCNC.ccmNotes = "<div class='alignCenter'>"+getNote(rowIndex,"CCM")+"</div>";
			tempCNC.ppaNotes = "<div class='alignCenter'>"+getNote(rowIndex,"PPA")+"</div>";
			tempCNC.findings = "<div class='alignCenter'  collapseTarget='"+cdTarget+"'>"+getNote(rowIndex,"FINDINGS")+"</div>";			
			rowIndex++;
			firstRowFlag = false;		
			returnCNC.push(tempCNC);
		}
	}
	sectionCashierTotals.tv = numberWithCommas(parseFloat(tv).toFixed(2));
	sectionCashierTotals.up = numberWithCommas(parseFloat(up).toFixed(2));
	sectionCashierTotals.gl = numberWithCommas(parseFloat(gl).toFixed(2));
	sectionObj.prodTotals = returnCNC;
	sectionObj.cashierTotals = sectionCashierTotals;
	return sectionObj;
}

function mergeSLGFIPesoCashierData(cashierProduct){
	var cpArr = cashierProduct.split("~");
	var cashierId = cpArr[0];
	var product = cpArr[1];
	var tempCashier = {};
	for(a in slgfiPesoData){
		if(slgfiPesoData[a].cashierId == cashierId && slgfiPesoData[a].product == product){
			if(slgfiPesoData[a].productCode == "RL_RV_PESO"){
				tempCashier.tradVul = slgfiPesoData[a].total;
			} else if(slgfiPesoData[a].productCode == "RU_PESO"){
				tempCashier.up = slgfiPesoData[a].total;
			} else if(slgfiPesoData[a].productCode == "RG_PESO"){
				tempCashier.gl = slgfiPesoData[a].total;
			} 
			tempCashier.cashierId = slgfiPesoData[a].cashierId;
			tempCashier.cashierName = slgfiPesoData[a].cashierName;
			tempCashier.product = product;
		}
	}
	return tempCashier;
}

function getSLGFIPGrandTotalPerCashier(cdTarget){
	var returnSection = [];
	var c = 0;
	for(c in cashierList){
		var tradVul = 0;
		var up = 0;
		var gl = 0;
		var d=0;
		for(d in slgfiPesoData){
			if(jQuery.inArray(slgfiPesoData[d].product,excludeFromCashierTotal) == -1 && slgfiPesoData[d].cashierId.toUpperCase() == cashierList[c].id.toUpperCase()){
				switch(slgfiPesoData[d].productCode){
					case "RL_RV_PESO": tradVul = tradVul + parseFloat(slgfiPesoData[d].total);break;
					case "RU_PESO": up = up  + parseFloat(slgfiPesoData[d].total);break;
					case "RG_PESO": gl = gl + parseFloat(slgfiPesoData[d].total);break;
				}
			}
		}
		var tempCashier = {};
		tempCashier.cashier = "<div class='alignLeft'  collapseTarget='"+cdTarget+"'>"+cashierList[c].name+"</div>";
		if(isSLGFIConfirmed(cashierList[c].id)){
			tempCashier.tradVul = "<div class='alignRight'>"+numberWithCommas(tradVul.toFixed(2))+"</div>";
			tempCashier.up = "<div class='alignRight'>"+numberWithCommas(up.toFixed(2))+"</div>";
			tempCashier.gl = "<div class='alignRight'>"+numberWithCommas(gl.toFixed(2))+"</div>";
		} else {
			tempCashier.tradVul = "<div class='alignCenter'>-</div>";
			tempCashier.up = "<div class='alignCenter'>-</div>";
			tempCashier.gl = "<div class='alignCenter'>-</div>";
		}
		returnSection.push(tempCashier);
	}
	return returnSection;
}


var slgfiPesoConsolidatedTotals = {tv: "0.00", gl: "0.00", up: "0.00"};