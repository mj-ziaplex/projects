function generatePNGrid_NEW(){
	require(['dojox/grid/EnhancedGrid'],
	function(EnhancedGrid){
		var store = loadData('slfpi');
		var structure = loadLayout('slfpi');
		var grid = new EnhancedGrid({
	        id: 'grid',
	        rowHeight: 24,
	        selectionMode: 'none'
	    });
		grid.setStore(store);
		grid.set('structure',structure);
    	grid.canSort = function(){return false}; 
        grid.placeAt("gridDiv");
        grid.startup();
	});
}

function generatePNGrid(){
	require(['dojo/_base/lang', 'dojox/grid/EnhancedGrid', 'dojo/data/ItemFileWriteStore', 'dojo/dom', 'dijit/form/Button', 'dojo/parser',  'dojo/domReady!'],
				function(lang, DataGrid, ItemFileWriteStore, dom, Button, parser){
			
					/*set up data store*/
					var data = {
					items: []
					};
					var data_list = [
					                 { col1: " ", preNeedPeso: " ", gafPeso: " ", col4: 0},
					                 { col1: "Cash - Counter", preNeedPeso: "99,999.99", gafPeso: "99,999.99", col4: 0},
					                 { col1: "Cash - Non Counter", preNeedPeso: "99,999.99", gafPeso: "99,999.99", col4: 0},
					                 { col1: "Cheque - On us", preNeedPeso: "99,999.99", gafPeso: "99,999.99", col4: 0},
					                 { col1: "Cheque - Local", preNeedPeso: "99,999.99", gafPeso: "99,999.99", col4: 0},
					                 { col1: "Cheque - Regional", preNeedPeso: "99,999.99", gafPeso: "99,999.99", col4: 0},
					                 { col1: "Cheque - Non Counter", preNeedPeso: "99,999.99", gafPeso: "99,999.99", col4: 0},
					                 { col1: "Card Types", preNeedPeso: "99,999.99", gafPeso: "99,999.99", col4: 0},
					                 { col1: "- POS-BPI", preNeedPeso: "99,999.99", gafPeso: "99,999.99", col4: 0},
					                 { col1: "- POS-CTIB", preNeedPeso: "99,999.99", gafPeso: "99,999.99", col4: 0},
					                 { col1: "- POS-HSBC", preNeedPeso: "99,999.99", gafPeso: "99,999.99", col4: 0},
					                 { col1: "- POS-SCB", preNeedPeso: "99,999.99", gafPeso: "99,999.99", col4: 0},
					                 { col1: " ", preNeedPeso: " ", gafPeso: " ", col4: 0},
					                 { col1: "TOTAL", preNeedPeso: "99,999.99", gafPeso: "99,999.99", col4: 0},
					                 { col1: " ", preNeedPeso: " ", gafPeso: " ", col4: 0},
					                 { col1: "EXCEPTIONS(Manually Inputted)", preNeedPeso: "99,999.99", gafPeso: "99,999.99", col4: 0},
					                 { col1: "Reversals", preNeedPeso: 1, gafPeso: 1, col4: 0},
					                 { col1: " ", preNeedPeso: " ", gafPeso: " ", col4: 1}
					                 ];
					for(var i = 0; i < data_list.length;i++){
						data.items.push(lang.mixin(data_list[i]));
					}
					var store = new ItemFileWriteStore({data: data});

					/*set up layout*/
					var layout = [[
					               {'name': ' ', 'field': 'col1', 'width': '30%', 'noresize': true},
					               {'name': 'PRENEED PESO', 'field': 'preNeedPeso', 'width': '12%','noresize': true, 'formatter': addReversalImage},
					               {'name': 'GAF PESO', 'field': 'gafPeso', 'width': '12%','noresize': true, 'formatter': addReversalImage},
					               {'name': ' ', 'field': 'col4', 'width': '46%','noresize': true, 'formatter': function addConfirmButton (val) {
					            	   		var mess = "CONFIRM";
											if ( val == 0 ) {
											        return " ";
											} else if ( val == 1 ) {
											        return "<button onClick=alert('" +mess+"')>Confirm</button>";
											}
											   }}
					               ]];

					/*create a new grid*/
					var grid = new DataGrid({
						id: 'grid',
						store: store,
						structure: layout,
						selectionMode: 'none',
						rowHeight: 22});
					
					grid.canSort =function(){return false}; 
					
						

					/*append the new grid to the div*/
					grid.placeAt("gridDiv");

					/*Call startup() to render the grid*/
					grid.startup();
    			});
	

}

function addReversalImage (val) {				            	   		
		if ( val == 1 ) {
		return "<img src=\"css/UndoButton.png\" />"; 
	} else {
		return val;
	}
}
