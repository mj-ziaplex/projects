var otherPesoColumns = [{
	name : "",
	field : "product",
	width : "15%"
},{
	name : "",
	field : "cashier",
	width : "22%"
},{
	name : "COB Peso (COBP)",
	field : "cobPeso",
	width : "7%"
},{
	name : "DS",
	field : "dsCobp",
	width : "6%"
},{
	name : "VDS",
	field : "vdsCobp",
	width : "6%"
},{
	name : "Recon?",
	field : "reconciled",
	width : "6%"
},{
	name : "Recon By",
	field : "reconciledBy",
	width : "6%"
},{
	name : "Recon Date",
	field : "reconciledDate",
	width : "6%"
},{
	name : "CCM NOTES",
	field : "ccmNotes",
	width : "7%"
},{
	name : "CCQA NOTES",
	field : "ppaNotes",
	width : "7%"
},{
	name : "Findings",
	field : "findings",
	width : "8%"
}	
];

var otherPesoColumnsNH = [{
	name : "",
	field : "product",
	width : "15%"
},{
	name : "",
	field : "cashier",
	width : "22%"
},{
	name : "",
	field : "cobPeso",
	width : "7%"
},{
	name : "",
	field : "dsCobp",
	width : "6%"
},{
	name : "",
	field : "vdsCobp",
	width : "6%"
},{
	name : "",
	field : "reconciled",
	width : "6%"
},{
	name : "",
	field : "reconciledBy",
	width : "6%"
},{
	name : "",
	field : "reconciledDate",
	width : "6%"
},{
	name : "",
	field : "ccmNotes",
	width : "7%"
},{
	name : "",
	field : "ppaNotes",
	width : "7%"
},{
	name : "",
	field : "findings",
	width : "8%"
}	
];

function generateOtherPesoSection(otherPesoTableDivId,otherPesoDivDetails,otherPesoSummaryDivDetails){
	$(document).ready(function(){
			
	mergeOtherPesoCashierProductTypes();
	
	var pesoCashHeader = [{product: "<div class='alignLeft' cdTarget='otherPesoCash' onclick='collapseDivision(this)'>PESO CASH</div>",cashier:"", cobPeso: "",dsCobp: "",vdsCobp:"",reconciled:"",reconciledBy:"",reconciledDate:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var cashNonCounterData = generateOtherPesoSectionData("totalCashNonCounter","otherPesoCash");
	
	var cncTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > Non Counter</div>", cobPeso: "<div class='numFormat'>"+cashNonCounterData.cashierTotals.otherPesoTotal+"</div>",dsCobp: "",vdsCobp:"",reconciled:"",reconciledBy:"",reconciledDate:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var cashCounterData = generateOtherPesoSectionData("totalCashCounter","otherPesoCash");
	
	var ccTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > Counter</div>", cobPeso: "<div class='numFormat'>"+cashCounterData.cashierTotals.otherPesoTotal+"</div>",dsCobp: "",vdsCobp:"",reconciled:"",reconciledBy:"",reconciledDate:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var pesoCashTotals = getOtherPesoAreaTotal([cashNonCounterData,cashCounterData]);
	
	var areaCashTotal = [{product: "<div class='totalRow'>&nbsp;</div>",cashier:"<div class='totalRow'><div class='alignRight'>TOTAL CASH</div></div>", cobPeso: "<div class='totalRow'><div class='numFormat'>"+pesoCashTotals.cobPeso+"</div></div>",dsCobp: "<div class='totalRow'>&nbsp;</div>",vdsCobp:"<div class='totalRow'>&nbsp;</div>",reconciled:"<div class='totalRow'>&nbsp;</div>",reconciledBy:"<div class='totalRow'>&nbsp;</div>",reconciledDate:"<div class='totalRow'>&nbsp;</div>",ccmNotes:"<div class='totalRow'>&nbsp;</div>",ppaNotes:"<div class='totalRow'>&nbsp;</div>",findings:"<div class='totalRow'>&nbsp;</div>"}];
	
	var pesoCheckHeader = [{product: "<div class='alignLeft' cdTarget='otherPesoCheck' onclick='collapseDivision(this)'>PESO CHECK</div>",cashier:"", cobPeso: "",dsCobp: "",vdsCobp:"",reconciled:"",reconciledBy:"",reconciledDate:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var checkOnUsData = generateOtherPesoSectionData("totalCheckOnUs","otherPesoCheck");
	
	var chouTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > On Us</div>", cobPeso: "<div class='numFormat'>"+checkOnUsData.cashierTotals.otherPesoTotal+"</div>",dsCobp: "",vdsCobp:"",reconciled:"",reconciledBy:"",reconciledDate:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var checkLocalData = generateOtherPesoSectionData("totalCheckLocal","otherPesoCheck");
	
	var chLocTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > Local</div>", cobPeso: "<div class='numFormat'>"+checkLocalData.cashierTotals.otherPesoTotal+"</div>",dsCobp: "",vdsCobp:"",reconciled:"",reconciledBy:"",reconciledDate:"",ccmNotes:"",ppaNotes:"",findings:""}];
		
	var checkRegionalData = generateOtherPesoSectionData("totalCheckRegional","otherPesoCheck");
	
	var chrTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > Regional</div>", cobPeso: "<div class='numFormat'>"+checkRegionalData.cashierTotals.otherPesoTotal+"</div>",dsCobp: "",vdsCobp:"",reconciled:"",reconciledBy:"",reconciledDate:"",ccmNotes:"",ppaNotes:"",findings:""}];

	var checkNonCounterData = generateOtherPesoSectionData("totalCheckNonCounter","otherPesoCheck");
	
	var chncTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > Non Counter</div>", cobPeso: "<div class='numFormat'>"+checkNonCounterData.cashierTotals.otherPesoTotal+"</div>",dsCobp: "",vdsCobp:"",reconciled:"",reconciledBy:"",reconciledDate:"",ccmNotes:"",ppaNotes:"",findings:""}];

	var pesoCheckTotals = getOtherPesoAreaTotal([checkOnUsData,checkRegionalData,checkNonCounterData,checkLocalData]);
	
	var areaCheckTotal = [{product: "<div class='totalRow'>&nbsp;</div>",cashier:"<div class='totalRow'><div class='alignRight'>TOTAL CHECK</div></div>", cobPeso: "<div class='totalRow'><div class='numFormat'>"+pesoCheckTotals.cobPeso+"</div></div>",dsCobp: "<div class='totalRow'>&nbsp;</div>",vdsCobp:"<div class='totalRow'>&nbsp;</div>",reconciled:"<div class='totalRow'>&nbsp;</div>",reconciledBy:"<div class='totalRow'>&nbsp;</div>",reconciledDate:"<div class='totalRow'>&nbsp;</div>",ccmNotes:"<div class='totalRow'>&nbsp;</div>",ppaNotes:"<div class='totalRow'>&nbsp;</div>",findings:"<div class='totalRow'>&nbsp;</div>"}];
		
	var totalOTHERPCashiersCollection = getOtherPesoAreaTotal([cashNonCounterData,cashCounterData,checkOnUsData,checkRegionalData,checkNonCounterData,checkLocalData]);
	
	var cashierTotalHeader = [{product: "<div class='alignLeft' cdTarget='otherPesoGTPC' onclick='collapseDivision(this)'>Grand Total Per Cashier</div>",cashier:"", cobPeso: "",dsCobp: "",vdsCobp:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var cashierTotal = getOTHERPGrandTotalPerCashier("otherPesoGTPC");
	
	var cashierTotalRow = [{product: "<div class='totalRow'>&nbsp;</div>",cashier:"<div class='totalRow'><div class='alignRight'>TOTAL COLLECTION</div></div>", cobPeso: "<div class='totalRow'><div class='numFormat'>"+numberWithCommas(totalOTHERPCashiersCollection.cobPeso)+"</div></div>",dsCobp: "<div class='totalRow'>&nbsp;</div>",vdsCobp:"<div class='totalRow'>&nbsp;</div>",reconciled:"<div class='totalRow'><input type='hidden' id='othActiveCB' name='othActiveCB' value='"+ppaOthPesoActive+"'/><div>"+ppaOthPesoActive+"/"+ppaOthPesoTotal+"</div></div>",reconciledBy:"<div class='totalRow'>&nbsp;</div>",reconciledDate:"<div class='totalRow'>&nbsp;</div>",ccmNotes:"<div class='totalRow'>&nbsp;</div>",ppaNotes:"<div class='totalRow'>&nbsp;</div>",findings:"<div class='totalRow'>&nbsp;</div>"}];
	
	var otherPesoSummaryData = [{product: "",cashier:"CONSOLIDATED GRAND TOTAL", cobPeso: "<div class='numFormat'>"+numberWithCommas(otherPesoConsolidatedTotals.cobPeso)+"</div>",dsCobp: "",vdsCobp:"",reconciled:"",reconciledBy:"",reconciledDate:"",ccmNotes:"",ppaNotes:"",findings:""},
	                        {product: "",cashier:"TOTAL CASHIERS COLLECTION", cobPeso:"<div class='numFormat'>"+numberWithCommas(totalOTHERPCashiersCollection.cobPeso)+"</div>",dsCobp: "",vdsCobp:"",reconciled:"",reconciledBy:"",reconciledDate:"",ccmNotes:"",ppaNotes:"",findings:""},
	                        {product: "",cashier:"", cobPeso: compareTotalsImg(numberWithCommas(otherPesoConsolidatedTotals.cobPeso),totalOTHERPCashiersCollection.cobPeso),dsCobp: "",vdsCobp:"",reconciled:"",reconciledBy:"",reconciledDate:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	new TableContainer({
		divName : otherPesoSummaryDivDetails.divName,
		nodeName : otherPesoSummaryDivDetails.nodeName,
		width : "900px",
		id : otherPesoSummaryDivDetails.tableId,
		layout : otherPesoColumns,
		tableClass: "balancingToolSummary",
		data: otherPesoSummaryData
	}).startUp();
	
	var otherPesoSectionData = [];
	
	appendJsons([otherPesoSectionData,pesoCashHeader,cashNonCounterData.prodTotals,cncTotal,cashCounterData.prodTotals,ccTotal,areaCashTotal,pesoCheckHeader,checkOnUsData.prodTotals,chouTotal,checkLocalData.prodTotals,chLocTotal,checkRegionalData.prodTotals,chrTotal,checkNonCounterData.prodTotals,chncTotal,areaCheckTotal,cashierTotalHeader,cashierTotal,cashierTotalRow,generateSnapshotSection(otherSnapshot)]);
	
	new TableContainer({
		divName : otherPesoDivDetails.divName,
		nodeName : otherPesoDivDetails.nodeName,
		width : "900px",
		id : otherPesoDivDetails.tableId,
		layout : otherPesoColumnsNH,
		tableClass: "balancingToolSummary",
		data: otherPesoSectionData
	}).startUp();
	
	var otherPesoTableDiv = document.getElementById(otherPesoTableDivId);

	otherPesoTableDiv.style.display = "none";
	
	});
}


var otherPesoSecData = [];

function mergeOtherPesoCashierProductTypes(){
	var cashierRep = [];
	for(c in otherPesoData){
		//using jQuery.inArray instead of Array.indexOf because it is not supported in IE8 and below. thanks IE.
		if(jQuery.inArray(otherPesoData[c].cashierId+"~"+otherPesoData[c].product,cashierRep) == -1){
			var pushedData = "";
			pushedData = pushedData.concat(otherPesoData[c].cashierId);
			pushedData = pushedData.concat("~");
			pushedData = pushedData.concat(otherPesoData[c].product);
			cashierRep.push(pushedData);
		}
	}
	
	for(var a = 0; a < cashierRep.length; a++){
		otherPesoSecData.push(mergeOtherPesoCashierData(cashierRep[a]));
	}
		
}

function getOtherPesoAreaTotal(area){
	var tots = {};
	var cobPeso = 0;
	for(a in area){
		cobPeso = cobPeso + parseFloat(area[a].cashierTotals.otherPesoTotal.replace(/,/g,""));
	}
	tots.cobPeso = numberWithCommas(parseFloat(cobPeso).toFixed(2));
	return tots;
}

function generateOtherPesoSectionData(section,cdTarget){
	var sectionObj = {};
	var sectionCashierTotals = {};
	var otherPesoTotal = 0;
	var  returnCNC = [];
	var firstRowFlag = true;
	for(a in otherPesoSecData){
		var tempCNC = {};
		if(otherPesoSecData[a].product == section){
			var isCashDepo=false;
			var isCheckDepo=false;
			if(firstRowFlag){
				switch(section){
					case "totalCashNonCounter": tempCNC.product = "<div class='alignLeft'>Non Counter</div>";break;
					case "totalCashCounter": tempCNC.product = "<div class='alignLeft'>Counter</div>";isCashDepo=true;break;
					case "totalCheckOnUs": tempCNC.product = "<div class='alignLeft'>On Us</div>";isCheckDepo=true;checkType="ON_US";break;
					case "totalCheckRegional": tempCNC.product = "<div class='alignLeft'>Regional</div>";isCheckDepo=true;checkType="REGIONAL";break;
					case "totalCheckNonCounter": tempCNC.product = "<div class='alignLeft'>Non Counter</div>";break;
					case "totalCheckLocal": tempCNC.product = "<div class='alignLeft'>Local</div>";break;
					default: alert("Unknow Product Total: " + section); break;
				}
			} else{
				tempCNC.product = "";		
				switch(section){
					case "totalCashCounter": isCashDepo=true;break;
					case "totalCheckOnUs": isCheckDepo=true;checkType="ON_US";break;
					case "totalCheckRegional": isCheckDepo=true;checkType="REGIONAL";break;
				}
			}
			
			tempCNC.cashier = "<div class='alignLeft'>"+otherPesoSecData[a].cashierName+"</div>";
			if(isOTHERConfirmed(otherPesoSecData[a].cashierId)){
				tempCNC.cobPeso = "<div class='alignRight'>"+numberWithCommas(parseFloat(otherPesoSecData[a].cobPeso).toFixed(2))+"</div>";
				if(isCashDepo){
					if(parseFloat(otherPesoSecData[a].cobPeso) > 0){
						tempCNC.dsCobp  = getCashDepoURL("OTHER","CASH","PHP",otherPesoSecData[a].cashierId);
						tempCNC.vdsCobp  = getValidatedCashDepoURL("OTHER","CASH","PHP",otherPesoSecData[a].cashierId);
					}
				}else if(isCheckDepo){
					if(parseFloat(otherPesoSecData[a].cobPeso) > 0){
						tempCNC.dsCobp  = getChequeDepoURL("OTHER","CHEQUE","PHP",otherPesoSecData[a].cashierId,checkType);
						tempCNC.vdsCobp  = getValidatedChequeDepoURL("OTHER","CHEQUE","PHP",otherPesoSecData[a].cashierId,checkType);
					}
				}
				otherPesoTotal = otherPesoTotal + parseFloat(otherPesoSecData[a].cobPeso);
			} else{
				tempCNC.cobPeso = "<div class='alignRight'>-</div>";
			}
			otherPesoConsolidatedTotals.cobPeso = (parseFloat(otherPesoConsolidatedTotals.cobPeso) + parseFloat(otherPesoSecData[a].cobPeso)).toFixed(2);
			var isWholeRowZero = false;
			if(parseFloat(otherPesoSecData[a].cobPeso) == 0){
				isWholeRowZero = true;
			}
			tempCNC.reconciled = "<div class='alignCenter'>"+getReconCheckbox(rowIndex, isWholeRowZero,"" , "OTH")+"</div>";
			tempCNC.reconciledBy = "<div class='alignCenter'>"+getReconcilerName(rowIndex,isWholeRowZero,false)+"</div>";
			tempCNC.reconciledDate = "<div class='alignCenter'>"+getReconciledDate(rowIndex)+"</div>";
			tempCNC.ccmNotes = "<div class='alignCenter'>"+getNote(rowIndex,"CCM")+"</div>";
			tempCNC.ppaNotes = "<div class='alignCenter'>"+getNote(rowIndex,"PPA")+"</div>";
			tempCNC.findings = "<div class='alignCenter'  collapseTarget='"+cdTarget+"'>"+getNote(rowIndex,"FINDINGS")+"</div>";			
			rowIndex++;
			firstRowFlag = false;		
			returnCNC.push(tempCNC);
		}
	}
	sectionCashierTotals.otherPesoTotal = numberWithCommas(parseFloat(otherPesoTotal).toFixed(2));
	sectionObj.prodTotals = returnCNC;
	sectionObj.cashierTotals = sectionCashierTotals;
	return sectionObj;
}

function mergeOtherPesoCashierData(cashierProduct){
	var cpArr = cashierProduct.split("~");
	var cashierId = cpArr[0];
	var product = cpArr[1];
	var tempCashier = {};
	for(a in otherPesoData){
		if(otherPesoData[a].cashierId == cashierId && otherPesoData[a].product == product){
			if(otherPesoData[a].productCode == "COBP"){
				tempCashier.cobPeso = otherPesoData[a].total;
			}
			tempCashier.cashierId = otherPesoData[a].cashierId;
			tempCashier.cashierName = otherPesoData[a].cashierName;
			tempCashier.product = product;
		}
	}
	return tempCashier;
}

function getOTHERPGrandTotalPerCashier(cdTarget){
	var returnSection = [];
	var c = 0;
	for(c in cashierList){
		var cobPeso = 0;
		var d=0;
		for(d in otherPesoData){
			if(jQuery.inArray(otherPesoData[d].product,excludeFromCashierTotal) == -1 && otherPesoData[d].cashierId.toUpperCase() == cashierList[c].id.toUpperCase()){
				switch(otherPesoData[d].productCode){
					case "COBP": cobPeso = cobPeso + parseFloat(otherPesoData[d].total);break;
				}
			}
		}
		var tempCashier = {};
		tempCashier.cashier = "<div class='alignLeft'  collapseTarget='"+cdTarget+"'>"+cashierList[c].name+"</div>";
		if(isOTHERConfirmed(cashierList[c].id)){
			tempCashier.cobPeso = "<div class='alignRight'>"+numberWithCommas(cobPeso.toFixed(2))+"</div>";
		} else {
			tempCashier.cobPeso = "<div class='alignCenter'>-</div>";
		}
		returnSection.push(tempCashier);
	}
	return returnSection;
}

var otherPesoConsolidatedTotals = {cobPeso: "0.00"};