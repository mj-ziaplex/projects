/*
**Feb 2016; Cshells Sayan
  - Added comment for new product PCO
  - Added new variables and new product ADA(MF22)
*/
var slamciPesoColumns = [
    {name: "", field: "product", width: "4%"},
    {name: "", field: "cashier", width: "10%"},
    {name: "Bond Fund(MF1)", field: "mf1", width: "3%"},
    {name: "DS", field: "mf1DS", width: "3%"},
    {name: "VDS", field: "mf1VDS", width: "3%"},
    {name: "Balanced Fund(MF2)", field: "mf2", width: "3%"},
    {name: "DS", field: "mf2DS", width: "3%"},
    {name: "VDS", field: "mf2VDS", width: "3%"},
    {name: "Equity Fund(MF3)", field: "mf3", width: "3%"},
    {name: "DS", field: "mf3DS", width: "3%"},
    {name: "VDS", field: "mf3VDS", width: "3%"},
    {name: "Money Market Fund(MF5)", field: "mf5", width: "3%"},
    {name: "DS", field: "mf5DS", width: "3%"},
    {name: "VDS", field: "mf5VDS", width: "3%"},
    {name: "GS Fund(MF7)", field: "mf7", width: "3%"},
    {name: "DS", field: "mf7DS", width: "3%"},
    {name: "VDS", field: "mf7VDS", width: "3%"},
    {name: "Group Fund(MF20)", field: "mf20", width: "3%"},
    {name: "DS", field: "mf20DS", width: "3%"},
    {name: "VDS", field: "mf20VDS", width: "3%"},
    {name: "Dynamic Fund(MF8)", field: "mf8", width: "3%"},
    {name: "DS", field: "mf8DS", width: "3%"},
    {name: "VDS", field: "mf8VDS", width: "3%"},
    {name: "Index Fund(MF9)", field: "mf9", width: "3%"},
    {name: "DS", field: "mf9DS", width: "3%"},
    {name: "VDS", field: "mf9VDS", width: "3%"},
    {name: "Prosperity Card(MF)", field: "mf", width: "3%"}, // Added for PCO
    {name: "DS", field: "mfDS", width: "3%"},
    {name: "VDS", field: "mfVDS", width: "3%"},
    {name: "ADA(MF22)", field: "mf22", width: "3%"}, // Added for ADA
    {name: "DS", field: "mf22DS", width: "3%"},
    {name: "VDS", field: "mf22VDS", width: "3%"},
    {name: "CCM NOTES", field: "ccmNotes", width: "3%"},
    // Modified for MR-WF-16-00036 - Change PPA notes to CCQA notes
    {name: "CCQA NOTES", field: "ppaNotes", width: "3%"},
    {name: "Findings", field: "findings", width: "3%"}
];

var slamciPesoColumnsNH = [
    {name: "", field: "product", width: "3%"},
    {name: "", field: "cashier", width: "10%"},
    {name: "", field: "mf1", width: "3%"},
    {name: "", field: "mf1DS", width: "3%"},
    {name: "", field: "mf1VDS", width: "3%"},
    {name: "", field: "mf2", width: "3%"},
    {name: "", field: "mf2DS", width: "3%"},
    {name: "", field: "mf2VDS", width: "3%"},
    {name: "", field: "mf3", width: "3%"},
    {name: "", field: "mf3DS", width: "3%"},
    {name: "", field: "mf3VDS", width: "3%"},
    {name: "", field: "mf5", width: "3%"},
    {name: "", field: "mf5DS", width: "3%"},
    {name: "", field: "mf5VDS", width: "3%"},
    {name: "", field: "mf7", width: "3%"},
    {name: "", field: "mf7DS", width: "3%"},
    {name: "", field: "mf7VDS", width: "3%"},
    {name: "", field: "mf20", width: "3%"},
    {name: "", field: "mf20DS", width: "3%"},
    {name: "", field: "mf20VDS", width: "3%"},
    {name: "", field: "mf8", width: "3%"},
    {name: "", field: "mf8DS", width: "3%"},
    {name: "", field: "mf8VDS", width: "3%"},
    {name: "", field: "mf9", width: "3%"},
    {name: "", field: "mf9DS", width: "3%"},
    {name: "", field: "mf9VDS", width: "3%"},
    {name: "", field: "mf", width: "3%"}, // Added for PCO
    {name: "", field: "mfDS", width: "3%"},
    {name: "", field: "mfVDS", width: "3%"},
    {name: "", field: "mf22", width: "3%"}, // Added for ADA
    {name: "", field: "mf22DS", width: "3%"},
    {name: "", field: "mf22VDS", width: "3%"},
    {name: "", field: "ccmNotes", width: "3%"},
    {name: "", field: "ppaNotes", width: "3%"},
    {name: "", field: "findings", width: "3%"}
];

function generateSLAMCIPesoSection(slamciPesoTableDivId, slamciPesoDivDetails, slamciPesoSummaryDivDetails) {
    $(document).ready(function() {

        mergeSLAMCIPesoCashierProductTypes();

        var pesoCashHeader = [
            {product: "<div class='alignLeft' cdTarget='slamciPesoCash' onclick='collapseDivision(this)'>PESO CASH</div>",
                cashier: "",
                mf1: "", mf1DS: "", mf1VDS: "",
                mf2: "", mf2DS: "", mf2VDS: "",
                mf3: "", mf3DS: "", mf3VDS: "",
                mf5: "", mf5DS: "", mf5VDS: "",
                mf7: "", mf7DS: "", mf7VDS: "",
                mf20: "", mf20DS: "", mf20VDS: "",
                mf8: "", mf8DS: "", mf8VDS: "",
                mf9: "", mf9DS: "", mf9VDS: "",
                mf: "", mfDS: "", mfVDS: "",
                ccmNotes: "",
                ppaNotes: "",
                findings: ""}
        ];

        var cashNonCounterData = generateSLAMCIPesoSectionData("totalCashNonCounter", "slamciPesoCash");

        var cncTotal = [
            {product: "",
                cashier: "<div class='alignRight'>Sub Total > Non Counter</div>",
                mf1: "<div class='numFormat'>" + cashNonCounterData.cashierTotals.mf1Total + "</div>", mf1DS: "", mf1VDS: "",
                mf2: "<div class='numFormat'>" + cashNonCounterData.cashierTotals.mf2Total + "</div>", mf2DS: "", mf2VDS: "",
                mf3: "<div class='numFormat'>" + cashNonCounterData.cashierTotals.mf3Total + "</div>", mf3DS: "", mf3VDS: "",
                mf5: "<div class='numFormat'>" + cashNonCounterData.cashierTotals.mf5Total + "</div>", mf5DS: "", mf5VDS: "",
                mf7: "<div class='numFormat'>" + cashNonCounterData.cashierTotals.mf7Total + "</div>", mf7DS: "", mf7VDS: "",
                mf20: "<div class='numFormat'>" + cashNonCounterData.cashierTotals.mf20Total + "</div>", mf20DS: "", mf20VDS: "",
                mf8: "<div class='numFormat'>" + cashNonCounterData.cashierTotals.mf8Total + "</div>", mf8DS: "", mf8VDS: "",
                mf9: "<div class='numFormat'>" + cashNonCounterData.cashierTotals.mf9Total + "</div>", mf9DS: "", mf9VDS: "",
                mf: "<div class='numFormat'>" + cashNonCounterData.cashierTotals.mfTotal + "</div>", mfDS: "", mfVDS: "", // Added for PCO
                mf22: "<div class='numFormat'>" + cashNonCounterData.cashierTotals.mf22Total + "</div>", mf22DS: "", mf22VDS: "", // Added for ADA
                ccmNotes: "",
                ppaNotes: "",
                findings: ""}
        ];

        var cashCounterData = generateSLAMCIPesoSectionData("totalCashCounter", "slamciPesoCash");

        var ccTotal = [
            {product: "",
                cashier: "<div class='alignRight'>Sub Total > Counter</div>",
                mf1: "<div class='numFormat'>" + cashCounterData.cashierTotals.mf1Total + "</div>", mf1DS: "", mf1VDS: "",
                mf2: "<div class='numFormat'>" + cashCounterData.cashierTotals.mf2Total + "</div>", mf2DS: "", mf2VDS: "",
                mf3: "<div class='numFormat'>" + cashCounterData.cashierTotals.mf3Total + "</div>", mf3DS: "", mf3VDS: "",
                mf5: "<div class='numFormat'>" + cashCounterData.cashierTotals.mf5Total + "</div>", mf5DS: "", mf5VDS: "",
                mf7: "<div class='numFormat'>" + cashCounterData.cashierTotals.mf7Total + "</div>", mf7DS: "", mf7VDS: "",
                mf20: "<div class='numFormat'>" + cashCounterData.cashierTotals.mf20Total + "</div>", mf20DS: "", mf20VDS: "",
                mf8: "<div class='numFormat'>" + cashCounterData.cashierTotals.mf8Total + "</div>", mf8DS: "", mf8VDS: "",
                mf9: "<div class='numFormat'>" + cashCounterData.cashierTotals.mf9Total + "</div>", mf9DS: "", mf9VDS: "",
                mf: "<div class='numFormat'>" + cashCounterData.cashierTotals.mfTotal + "</div>", mfDS: "", mfVDS: "", // Added for PCO
                mf22: "<div class='numFormat'>" + cashCounterData.cashierTotals.mf22Total + "</div>", mf22DS: "", mf22VDS: "", // Added for ADA
                ccmNotes: "",
                ppaNotes: "",
                findings: ""}
        ];

        var pesoCashTotals = getSLAMCIPesoAreaTotal([cashNonCounterData, cashCounterData]);

        var areaCashTotal = [
            {product: "<div class='totalRow'>&nbsp;</div>",
                cashier: "<div class='totalRow'><div class='alignRight'>TOTAL CASH</div></div>",
                mf1: "<div class='totalRow'><div class='numFormat'>" + pesoCashTotals.mf1 + "</div></div>", mf1DS: "<div class='totalRow'>&nbsp;</div>", mf1VDS: "<div class='totalRow'>&nbsp;</div>",
                mf2: "<div class='totalRow'><div class='numFormat'>" + pesoCashTotals.mf2 + "</div></div>", mf2DS: "<div class='totalRow'>&nbsp;</div>", mf2VDS: "<div class='totalRow'>&nbsp;</div>",
                mf3: "<div class='totalRow'><div class='numFormat'>" + pesoCashTotals.mf3 + "</div></div>", mf3DS: "<div class='totalRow'>&nbsp;</div>", mf3VDS: "<div class='totalRow'>&nbsp;</div>",
                mf5: "<div class='totalRow'><div class='numFormat'>" + pesoCashTotals.mf5 + "</div></div>", mf5DS: "<div class='totalRow'>&nbsp;</div>", mf5VDS: "<div class='totalRow'>&nbsp;</div>",
                mf7: "<div class='totalRow'><div class='numFormat'>" + pesoCashTotals.mf7 + "</div></div>", mf7DS: "<div class='totalRow'>&nbsp;</div>", mf7VDS: "<div class='totalRow'>&nbsp;</div>",
                mf20: "<div class='totalRow'><div class='numFormat'>" + pesoCashTotals.mf20 + "</div></div>", mf20DS: "<div class='totalRow'>&nbsp;</div>", mf20VDS: "<div class='totalRow'>&nbsp;</div>",
                mf8: "<div class='totalRow'><div class='numFormat'>" + pesoCashTotals.mf8 + "</div></div>", mf8DS: "<div class='totalRow'>&nbsp;</div>", mf8VDS: "<div class='totalRow'>&nbsp;</div>",
                mf9: "<div class='totalRow'><div class='numFormat'>" + pesoCashTotals.mf9 + "</div></div>", mf9DS: "<div class='totalRow'>&nbsp;</div>", mf9VDS: "<div class='totalRow'>&nbsp;</div>",
                mf: "<div class='totalRow'><div class='numFormat'>" + pesoCashTotals.mf + "</div></div>", mfDS: "<div class='totalRow'>&nbsp;</div>", mfVDS: "<div class='totalRow'>&nbsp;</div>", // Added for PCO
                mf22: "<div class='totalRow'><div class='numFormat'>" + pesoCashTotals.mf22 + "</div></div>", mf22DS: "<div class='totalRow'>&nbsp;</div>", mf22VDS: "<div class='totalRow'>&nbsp;</div>", // Added for ADA
                ccmNotes: "<div class='totalRow'>&nbsp;</div>",
                ppaNotes: "<div class='totalRow'>&nbsp;</div>",
                findings: "<div class='totalRow'>&nbsp;</div>"}
        ];

        var pesoCheckHeader = [
            {product: "<div class='alignLeft' cdTarget='slamciPesoCheck' onclick='collapseDivision(this)'>PESO CHECK</div>",
                cashier: "",
                mf1: "", mf1DS: "", mf1VDS: "",
                mf2: "", mf2DS: "", mf2VDS: "",
                mf3: "", mf3DS: "", mf3VDS: "",
                mf5: "", mf5DS: "", mf5VDS: "",
                mf7: "", mf7DS: "", mf7VDS: "",
                mf20: "", mf20DS: "", mf20VDS: "",
                mf8: "", mf8DS: "", mf8VDS: "",
                mf9: "", mf9DS: "", mf9VDS: "",
                mf: "", mfDS: "", mfVDS: "", // Added for PCO
                mf22: "", mf22DS: "", mf22VDS: "", // Added for ADA
                ccmNotes: "",
                ppaNotes: "",
                findings: ""}
        ];
        
        var checkLocalData = generateSLAMCIPesoSectionData("totalCheckLocal", "slamciPesoCheck");

        var clTotal = [
            {product: "",
                cashier: "<div class='alignRight'>Sub Total > Check Local</div>",
                mf1: "<div class='numFormat'>" + checkLocalData.cashierTotals.mf1Total + "</div>", mf1DS: "", mf1VDS: "",
                mf2: "<div class='numFormat'>" + checkLocalData.cashierTotals.mf2Total + "</div>", mf2DS: "",
                mf3: "<div class='numFormat'>" + checkLocalData.cashierTotals.mf3Total + "</div>", mf3DS: "", mf3VDS: "",
                mf5: "<div class='numFormat'>" + checkLocalData.cashierTotals.mf5Total + "</div>", mf5DS: "", mf5VDS: "",
                mf7: "<div class='numFormat'>" + checkLocalData.cashierTotals.mf7Total + "</div>", mf7DS: "", mf7VDS: "",
                mf20: "<div class='numFormat'>" + checkLocalData.cashierTotals.mf20Total + "</div>", mf20DS: "", mf20VDS: "",
                mf8: "<div class='numFormat'>" + checkLocalData.cashierTotals.mf8Total + "</div>",
                mf9: "<div class='numFormat'>" + checkLocalData.cashierTotals.mf9Total + "</div>",
                mf: "<div class='numFormat'>" + checkLocalData.cashierTotals.mfTotal + "</div>", // Added for PCO
                mf22: "<div class='numFormat'>" + checkLocalData.cashierTotals.mf22Total + "</div>", // Added for ADA
                ccmNotes: "",
                ppaNotes: "",
                findings: ""}
        ];

        var checkRegionalData = generateSLAMCIPesoSectionData("totalCheckRegional", "slamciPesoCheck");

        var crTotal = [
            {product: "",
                cashier: "<div class='alignRight'>Sub Total > Check Regional</div>",
                mf1: "<div class='numFormat'>" + checkRegionalData.cashierTotals.mf1Total + "</div>", mf1DS: "", mf1VDS: "",
                mf2: "<div class='numFormat'>" + checkRegionalData.cashierTotals.mf2Total + "</div>", mf2DS: "",
                mf3: "<div class='numFormat'>" + checkRegionalData.cashierTotals.mf3Total + "</div>", mf3DS: "", mf3VDS: "",
                mf5: "<div class='numFormat'>" + checkRegionalData.cashierTotals.mf5Total + "</div>", mf5DS: "", mf5VDS: "",
                mf7: "<div class='numFormat'>" + checkRegionalData.cashierTotals.mf7Total + "</div>", mf7DS: "", mf7VDS: "",
                mf20: "<div class='numFormat'>" + checkRegionalData.cashierTotals.mf20Total + "</div>", mf20DS: "", mf20VDS: "",
                mf8: "<div class='numFormat'>" + checkRegionalData.cashierTotals.mf8Total + "</div>",
                mf9: "<div class='numFormat'>" + checkRegionalData.cashierTotals.mf9Total + "</div>",
                mf: "<div class='numFormat'>" + checkRegionalData.cashierTotals.mfTotal + "</div>", // Added for PCO
                mf22: "<div class='numFormat'>" + checkRegionalData.cashierTotals.mf22Total + "</div>", // Added for ADA
                ccmNotes: "",
                ppaNotes: "",
                findings: ""}
        ];

        var checkOnUsData = generateSLAMCIPesoSectionData("totalCheckOnUs", "slamciPesoCheck");

        var couTotal = [
            {product: "",
                cashier: "<div class='alignRight'>Sub Total > Check On Us</div>",
                mf1: "<div class='numFormat'>" + checkOnUsData.cashierTotals.mf1Total + "</div>", mf1DS: "", mf1VDS: "",
                mf2: "<div class='numFormat'>" + checkOnUsData.cashierTotals.mf2Total + "</div>", mf2DS: "",
                mf3: "<div class='numFormat'>" + checkOnUsData.cashierTotals.mf3Total + "</div>", mf3DS: "", mf3VDS: "",
                mf5: "<div class='numFormat'>" + checkOnUsData.cashierTotals.mf5Total + "</div>", mf5DS: "", mf5VDS: "",
                mf7: "<div class='numFormat'>" + checkOnUsData.cashierTotals.mf7Total + "</div>", mf7DS: "", mf7VDS: "",
                mf20: "<div class='numFormat'>" + checkOnUsData.cashierTotals.mf20Total + "</div>", mf20DS: "", mf20VDS: "",
                mf8: "<div class='numFormat'>" + checkOnUsData.cashierTotals.mf8Total + "</div>",
                mf9: "<div class='numFormat'>" + checkOnUsData.cashierTotals.mf9Total + "</div>",
                mf: "<div class='numFormat'>" + checkOnUsData.cashierTotals.mfTotal + "</div>", // Added for PCO
                mf22: "<div class='numFormat'>" + checkOnUsData.cashierTotals.mf22Total + "</div>", // Added for ADA
                ccmNotes: "",
                ppaNotes: "",
                findings: ""}
        ];

        var checkOTData = generateSLAMCIPesoSectionData("totalCheckOT", "slamciPesoCheck");

        var cotTotal = [
            {product: "",
                cashier: "<div class='alignRight'>Sub Total > Check OT</div>",
                mf1: "<div class='numFormat'>" + checkOTData.cashierTotals.mf1Total + "</div>", mf1DS: "", mf1VDS: "",
                mf2: "<div class='numFormat'>" + checkOTData.cashierTotals.mf2Total + "</div>", mf2DS: "",
                mf3: "<div class='numFormat'>" + checkOTData.cashierTotals.mf3Total + "</div>", mf3DS: "", mf3VDS: "",
                mf5: "<div class='numFormat'>" + checkOTData.cashierTotals.mf5Total + "</div>", mf5DS: "", mf5VDS: "",
                mf7: "<div class='numFormat'>" + checkOTData.cashierTotals.mf7Total + "</div>", mf7DS: "", mf7VDS: "",
                mf20: "<div class='numFormat'>" + checkOTData.cashierTotals.mf20Total + "</div>", mf20DS: "", mf20VDS: "",
                mf8: "<div class='numFormat'>" + checkOTData.cashierTotals.mf8Total + "</div>",
                mf9: "<div class='numFormat'>" + checkOTData.cashierTotals.mf9Total + "</div>",
                mf: "<div class='numFormat'>" + checkOTData.cashierTotals.mfTotal + "</div>", // Added for PCO
                mf22: "<div class='numFormat'>" + checkOTData.cashierTotals.mf22Total + "</div>", // Added for ADA
                ccmNotes: "",
                ppaNotes: "",
                findings: ""}
        ];

        var postalMoneyOrderData = generateSLAMCIPesoSectionData("totalPmo", "slamciPesoCheck");

        var postalMoneyOrderTotal = [
            {product: "",
                cashier: "<div class='alignRight'>Sub Total > Postal Money Order</div>",
                mf1: "<div class='numFormat'>" + postalMoneyOrderData.cashierTotals.mf1Total + "</div>", mf1DS: "", mf1VDS: "",
                mf2: "<div class='numFormat'>" + postalMoneyOrderData.cashierTotals.mf2Total + "</div>", mf2DS: "",
                mf3: "<div class='numFormat'>" + postalMoneyOrderData.cashierTotals.mf3Total + "</div>", mf3DS: "", mf3VDS: "",
                mf5: "<div class='numFormat'>" + postalMoneyOrderData.cashierTotals.mf5Total + "</div>", mf5DS: "", mf5VDS: "",
                mf7: "<div class='numFormat'>" + postalMoneyOrderData.cashierTotals.mf7Total + "</div>", mf7DS: "", mf7VDS: "",
                mf20: "<div class='numFormat'>" + postalMoneyOrderData.cashierTotals.mf20Total + "</div>", mf20DS: "", mf20VDS: "",
                mf8: "<div class='numFormat'>" + postalMoneyOrderData.cashierTotals.mf8Total + "</div>",
                mf9: "<div class='numFormat'>" + postalMoneyOrderData.cashierTotals.mf9Total + "</div>",
                mf: "<div class='numFormat'>" + postalMoneyOrderData.cashierTotals.mfTotal + "</div>", // Added for PCO
                mf22: "<div class='numFormat'>" + postalMoneyOrderData.cashierTotals.mf22Total + "</div>", // Added for ADA
                ccmNotes: "",
                ppaNotes: "",
                findings: ""}
        ];

        var checkNCData = generateSLAMCIPesoSectionData("totalCheckNonCounter", "slamciPesoCheck");

        var cNCTotal = [
            {product: "",
                cashier: "<div class='alignRight'>Sub Total > Check Non Counter</div>",
                mf1: "<div class='numFormat'>" + checkNCData.cashierTotals.mf1Total + "</div>", mf1DS: "", mf1VDS: "",
                mf2: "<div class='numFormat'>" + checkNCData.cashierTotals.mf2Total + "</div>", mf2DS: "",
                mf3: "<div class='numFormat'>" + checkNCData.cashierTotals.mf3Total + "</div>", mf3DS: "", mf3VDS: "",
                mf5: "<div class='numFormat'>" + checkNCData.cashierTotals.mf5Total + "</div>", mf5DS: "", mf5VDS: "",
                mf7: "<div class='numFormat'>" + checkNCData.cashierTotals.mf7Total + "</div>", mf7DS: "", mf7VDS: "",
                mf20: "<div class='numFormat'>" + checkNCData.cashierTotals.mf20Total + "</div>", mf20DS: "", mf20VDS: "",
                mf8: "<div class='numFormat'>" + checkNCData.cashierTotals.mf8Total + "</div>",
                mf9: "<div class='numFormat'>" + checkNCData.cashierTotals.mf9Total + "</div>",
                mf: "<div class='numFormat'>" + checkNCData.cashierTotals.mfTotal + "</div>", // Added for PCO
                mf22: "<div class='numFormat'>" + checkNCData.cashierTotals.mf22Total + "</div>", // Added for ADA
                ccmNotes: "",
                ppaNotes: "",
                findings: ""}
        ];

        var pesoCheckTotals = getSLAMCIPesoAreaTotal([checkLocalData, checkRegionalData, checkOnUsData, checkNCData, postalMoneyOrderData, checkOTData]);

        var areaCheckTotal = [
            {product: "<div class='totalRow'>&nbsp;</div>",
                cashier: "<div class='totalRow'><div class='alignRight'>TOTAL CHECK</div></div>",
                mf1: "<div class='totalRow'><div class='numFormat'>" + pesoCheckTotals.mf1 + "</div></div>", mf1DS: "<div class='totalRow'>&nbsp;</div>", mf1VDS: "<div class='totalRow'>&nbsp;</div>",
                mf2: "<div class='totalRow'><div class='numFormat'>" + pesoCheckTotals.mf2 + "</div></div>", mf2DS: "<div class='totalRow'>&nbsp;</div>", mf2VDS: "<div class='totalRow'>&nbsp;</div>",
                mf3: "<div class='totalRow'><div class='numFormat'>" + pesoCheckTotals.mf3 + "</div></div>", mf3DS: "<div class='totalRow'>&nbsp;</div>", mf3VDS: "<div class='totalRow'>&nbsp;</div>",
                mf5: "<div class='totalRow'><div class='numFormat'>" + pesoCheckTotals.mf5 + "</div></div>", mf5DS: "<div class='totalRow'>&nbsp;</div>", mf5VDS: "<div class='totalRow'>&nbsp;</div>",
                mf7: "<div class='totalRow'><div class='numFormat'>" + pesoCheckTotals.mf7 + "</div></div>", mf7DS: "<div class='totalRow'>&nbsp;</div>", mf7VDS: "<div class='totalRow'>&nbsp;</div>",
                mf20: "<div class='totalRow'><div class='numFormat'>" + pesoCheckTotals.mf20 + "</div></div>", mf20DS: "<div class='totalRow'>&nbsp;</div>", mf20VDS: "<div class='totalRow'>&nbsp;</div>",
                mf8: "<div class='totalRow'><div class='numFormat'>" + pesoCheckTotals.mf8 + "</div></div>", mf8DS: "<div class='totalRow'>&nbsp;</div>", mf8VDS: "<div class='totalRow'>&nbsp;</div>",
                mf9: "<div class='totalRow'><div class='numFormat'>" + pesoCheckTotals.mf9 + "</div></div>", mf9DS: "<div class='totalRow'>&nbsp;</div>", mf9VDS: "<div class='totalRow'>&nbsp;</div>",
                mf: "<div class='totalRow'><div class='numFormat'>" + pesoCheckTotals.mf + "</div></div>", mfDS: "<div class='totalRow'>&nbsp;</div>", mfVDS: "<div class='totalRow'>&nbsp;</div>", // Added for PCO
                mf22: "<div class='totalRow'><div class='numFormat'>" + pesoCheckTotals.mf22 + "</div></div>", mf22DS: "<div class='totalRow'>&nbsp;</div>", mf22VDS: "<div class='totalRow'>&nbsp;</div>", // Added for ADA
                ccmNotes: "<div class='totalRow'>&nbsp;</div>",
                ppaNotes: "<div class='totalRow'>&nbsp;</div>",
                findings: "<div class='totalRow'>&nbsp;</div>"}
        ];

        var nonCashHeader = [
            {product: "<div class='alignLeft' cdTarget='slamciPesoNonCash' onclick='collapseDivision(this)'>NON CASH</div>",
                cashier: "",
                mf1: "", mf1DS: "", mf1VDS: "",
                mf2: "", mf2DS: "", mf2VDS: "",
                mf3: "", mf3DS: "", mf3VDS: "",
                mf5: "", mf5DS: "", mf5VDS: "",
                mf7: "", mf7DS: "", mf7VDS: "",
                mf20: "", mf20DS: "", mf20VDS: "",
                mf8: "", mf8DS: "", mf8VDS: "",
                mf9: "", mf9DS: "", mf9VDS: "",
                mf: "", mfDS: "", mfVDS: "", // Added for PCO
                mf22: "", mf22DS: "", mf22VDS: "", // Added for ADA
                ccmNotes: "",
                ppaNotes: "",
                findings: ""}
        ];

        var nonCashData = generateSLAMCIPesoSectionData("totalNonCash", "slamciPesoNonCash");

        var ncTotal = [
            {product: "<div class='totalRow'>&nbsp;</div>",
                cashier: "<div class='totalRow'><div class='alignRight'>TOTAL NON CASH</div></div>",
                mf1: "<div class='totalRow'><div class='numFormat'>" + nonCashData.cashierTotals.mf1Total + "</div></div>", mf1DS: "<div class='totalRow'>&nbsp;</div>", mf1VDS: "<div class='totalRow'>&nbsp;</div>",
                mf2: "<div class='totalRow'><div class='numFormat'>" + nonCashData.cashierTotals.mf2Total + "</div></div>", mf2DS: "<div class='totalRow'>&nbsp;</div>", mf2VDS: "<div class='totalRow'>&nbsp;</div>",
                mf3: "<div class='totalRow'><div class='numFormat'>" + nonCashData.cashierTotals.mf3Total + "</div></div>", mf3DS: "<div class='totalRow'>&nbsp;</div>", mf3VDS: "<div class='totalRow'>&nbsp;</div>",
//                mf3:"<div class='totalRow'><div class='numFormat'>" + nonCashData.cashierTotals.mf3Total + "</div></div>", mf3DS:"<div class='totalRow'>&nbsp;</div>", mf3VDS:"<div class='totalRow'>&nbsp;</div>",
                mf5: "<div class='totalRow'><div class='numFormat'>" + nonCashData.cashierTotals.mf5Total + "</div></div>", mf5DS: "<div class='totalRow'>&nbsp;</div>", mf5VDS: "<div class='totalRow'>&nbsp;</div>",
                mf7: "<div class='totalRow'><div class='numFormat'>" + nonCashData.cashierTotals.mf7Total + "</div></div>", mf7DS: "<div class='totalRow'>&nbsp;</div>", mf7VDS: "<div class='totalRow'>&nbsp;</div>",
                mf20: "<div class='totalRow'><div class='numFormat'>" + nonCashData.cashierTotals.mf20Total + "</div></div>", mf20DS: "<div class='totalRow'>&nbsp;</div>", mf20VDS: "<div class='totalRow'>&nbsp;</div>",
                mf8: "<div class='totalRow'><div class='numFormat'>" + nonCashData.cashierTotals.mf8Total + "</div></div>", mf8DS: "<div class='totalRow'>&nbsp;</div>", mf8VDS: "<div class='totalRow'>&nbsp;</div>",
                mf9: "<div class='totalRow'><div class='numFormat'>" + nonCashData.cashierTotals.mf9Total + "</div></div>", mf9DS: "<div class='totalRow'>&nbsp;</div>", mf9VDS: "<div class='totalRow'>&nbsp;</div>",
                mf: "<div class='totalRow'><div class='numFormat'>" + nonCashData.cashierTotals.mfTotal + "</div></div>", mfDS: "<div class='totalRow'>&nbsp;</div>", mfVDS: "<div class='totalRow'>&nbsp;</div>", // Added for PCO
                mf22: "<div class='totalRow'><div class='numFormat'>" + nonCashData.cashierTotals.mf22Total + "</div></div>", mf22DS: "<div class='totalRow'>&nbsp;</div>", mf22VDS: "<div class='totalRow'>&nbsp;</div>", // Added for ADA
                ccmNotes: "<div class='totalRow'>&nbsp;</div>",
                ppaNotes: "<div class='totalRow'>&nbsp;</div>",
                findings: "<div class='totalRow'>&nbsp;</div>"}
        ];

        var exemptionHeader = [
            {product: "<div class='alignLeft' cdTarget='slamciPesoExemption' onclick='collapseDivision(this)'>EXEMPTIONS</div>",
                cashier: "",
                mf1: "", mf1DSN: "", mf1VSN:"",
                mf2: "", mf2DS: "",
                mf3: "", mf3DS: "", mf3VDS: "",
                mf5: "", mf5DS: "", mf5VDS: "",
                mf7: "", mf7DS: "", mf7VDS: "",
                mf20: "", mf20DS: "", mf20VDS: "",
                mf8: "", mf8DS: "", mf8VDS: "",
                mf9: "", mf9DS: "", mf9VDS: "",
                mf: "", mfDS: "", mfVDS: "", // Added for PCO
                mf22: "", mf22DS: "", mf22VDS: "", // Added for ADA
                ccmNotes: "",
                ppaNotes: "",
                findings: ""}
        ];

        var exemptionData = generateSLAMCIPesoSectionData("reversalProduct", "slamciPesoExemption");

        var exemptionTotal = [
            {product: "<div class='totalRow'>&nbsp;</div>",
                cashier: "<div class='totalRow'><div class='alignRight'>TOTAL EXEMPTIONS</div></div>",
                mf1: "<div class='totalRow'><div class='numFormat'>" + exemptionData.cashierTotals.mf1Total + "</div></div>", mf1DS: "<div class='totalRow'>&nbsp;</div>", mf1VDS: "<div class='totalRow'>&nbsp;</div>",
                mf2: "<div class='totalRow'><div class='numFormat'>" + exemptionData.cashierTotals.mf2Total + "</div></div>", mf2DS: "<div class='totalRow'>&nbsp;</div>", mf2VDS: "<div class='totalRow'>&nbsp;</div>",
                mf3: "<div class='totalRow'><div class='numFormat'>" + exemptionData.cashierTotals.mf3Total + "</div></div>", mf3DS: "<div class='totalRow'>&nbsp;</div>", mf3VDS: "<div class='totalRow'>&nbsp;</div>",
                mf5: "<div class='totalRow'><div class='numFormat'>" + exemptionData.cashierTotals.mf5Total + "</div></div>", mf5DS: "<div class='totalRow'>&nbsp;</div>", mf5VDS: "<div class='totalRow'>&nbsp;</div>",
                mf7: "<div class='totalRow'><div class='numFormat'>" + exemptionData.cashierTotals.mf7Total + "</div></div>", mf7DS: "<div class='totalRow'>&nbsp;</div>", mf7VDS: "<div class='totalRow'>&nbsp;</div>",
                mf20: "<div class='totalRow'><div class='numFormat'>" + exemptionData.cashierTotals.mf20Total + "</div></div>", mf20DS: "<div class='totalRow'>&nbsp;</div>", mf20VDS: "<div class='totalRow'>&nbsp;</div>",
                mf8: "<div class='totalRow'><div class='numFormat'>" + exemptionData.cashierTotals.mf8Total + "</div></div>", mf8DS: "<div class='totalRow'>&nbsp;</div>", mf8VDS: "<div class='totalRow'>&nbsp;</div>",
                mf9: "<div class='totalRow'><div class='numFormat'>" + exemptionData.cashierTotals.mf9Total + "</div></div>", mf9DS: "<div class='totalRow'>&nbsp;</div>", mf9VDS: "<div class='totalRow'>&nbsp;</div>",
                mf: "<div class='totalRow'><div class='numFormat'>" + exemptionData.cashierTotals.mfTotal + "</div></div>", mfDS: "<div class='totalRow'>&nbsp;</div>", mfVDS: "<div class='totalRow'>&nbsp;</div>", // Added for PCO
                mf22: "<div class='totalRow'><div class='numFormat'>" + exemptionData.cashierTotals.mf22Total + "</div></div>", mf22DS: "<div class='totalRow'>&nbsp;</div>", mf22VDS: "<div class='totalRow'>&nbsp;</div>", // Added for ADA
                ccmNotes: "<div class='totalRow'>&nbsp;</div>",
                ppaNotes: "<div class='totalRow'>&nbsp;</div>",
                findings: "<div class='totalRow'>&nbsp;</div>"}
        ];
        
        // Start Added for PCO
        var pcoHeader = [
            {product: "<div class='alignLeft' cdTarget='slamciPesoPco' onclick='collapseDivision(this)'>PROSPERITY CARD</div>",
                cashier: "",
                mf1: "", mf1DS: "", mf1VDS: "",
                mf2: "", mf2DS: "", mf2VDS: "",
                mf3: "", mf3DS: "", mf3VDS: "",
                mf5: "", mf5DS: "", mf5VDS: "",
                mf7: "", mf7DS: "", mf7VDS: "",
                mf20: "", mf20DS: "", mf20VDS: "",
                mf8: "", mf8DS: "", mf8VDS: "",
                mf9: "", mf9DS: "", mf9VDS: "",
                mf: "", mfDS: "", mfVDS: "", // Added for PCO
                mf22: "", mf22DS: "", mf22VDS: "", // Added for ADA
                ccmNotes: "",
                ppaNotes: "",
                findings: ""}
        ];
        
        var pcoInvestData = generateSLAMCIPesoSectionData("totalPcoInvest", "slamciPesoPco");
        
        var pcoTotal = [
            {product: "<div class='totalRow'>&nbsp;</div>",
                cashier: "<div class='totalRow'><div class='alignRight'>TOTAL PCO CASH</div></div>",
                mf1: "<div class='totalRow'><div class='numFormat'>" + pcoInvestData.cashierTotals.mf1Total + "</div></div>", mf1DS: "<div class='totalRow'>&nbsp;</div>", mf1VDS: "<div class='totalRow'>&nbsp;</div>",
                mf2: "<div class='totalRow'><div class='numFormat'>" + pcoInvestData.cashierTotals.mf2Total + "</div></div>", mf2DS: "<div class='totalRow'>&nbsp;</div>", mf2VDS: "<div class='totalRow'>&nbsp;</div>",
                mf3: "<div class='totalRow'><div class='numFormat'>" + pcoInvestData.cashierTotals.mf3Total + "</div></div>", mf3DS: "<div class='totalRow'>&nbsp;</div>", mf3VDS: "<div class='totalRow'>&nbsp;</div>",
                mf5: "<div class='totalRow'><div class='numFormat'>" + pcoInvestData.cashierTotals.mf5Total + "</div></div>", mf5DS: "<div class='totalRow'>&nbsp;</div>", mf5VDS: "<div class='totalRow'>&nbsp;</div>",
                mf7: "<div class='totalRow'><div class='numFormat'>" + pcoInvestData.cashierTotals.mf7Total + "</div></div>", mf7DS: "<div class='totalRow'>&nbsp;</div>", mf7VDS: "<div class='totalRow'>&nbsp;</div>",
                mf20: "<div class='totalRow'><div class='numFormat'>" + pcoInvestData.cashierTotals.mf20Total + "</div></div>", mf20DS: "<div class='totalRow'>&nbsp;</div>", mf20VDS: "<div class='totalRow'>&nbsp;</div>",
                mf8: "<div class='totalRow'><div class='numFormat'>" + pcoInvestData.cashierTotals.mf8Total + "</div></div>", mf8DS: "<div class='totalRow'>&nbsp;</div>", mf8VDS: "<div class='totalRow'>&nbsp;</div>",
                mf9: "<div class='totalRow'><div class='numFormat'>" + pcoInvestData.cashierTotals.mf9Total + "</div></div>", mf9DS: "<div class='totalRow'>&nbsp;</div>", mf9VDS: "<div class='totalRow'>&nbsp;</div>",
                mf: "<div class='totalRow'><div class='numFormat'>" + pcoInvestData.cashierTotals.mfTotal + "</div></div>", mfDS: "<div class='totalRow'>&nbsp;</div>", mfVDS: "<div class='totalRow'>&nbsp;</div>", // Added for PCO
                mf22: "<div class='totalRow'><div class='numFormat'>" + pcoInvestData.cashierTotals.mf22Total + "</div></div>", mf22DS: "<div class='totalRow'>&nbsp;</div>", mf22VDS: "<div class='totalRow'>&nbsp;</div>", // Added for ADA
                ccmNotes: "<div class='totalRow'>&nbsp;</div>",
                ppaNotes: "<div class='totalRow'>&nbsp;</div>",
                findings: "<div class='totalRow'>&nbsp;</div>"}
        ];
        // End Added for PCO

        var totalSLAMCIPCashiersCollection = getSLAMCIPesoAreaTotal([cashNonCounterData, cashCounterData, checkLocalData, checkRegionalData, checkOnUsData, checkNCData, nonCashData, pcoInvestData, checkOTData, postalMoneyOrderData]);

        var cashierTotalHeader = [
            {product: "<div class='alignLeft' cdTarget='slamciPesoGTPC' onclick='collapseDivision(this)'>Grand Total Per Cashier</div>",
                cashier: "",
                mf1: "", mf1DS: "", mf1VDS:"",
                mf2: "", mf2DS: "",
                mf3: "", mf3DS: "", mf3VDS: "",
                mf5: "", mf5DS: "", mf5VDS: "",
                mf7: "", mf7DS: "", mf7VDS: "",
                mf20: "", mf20DS: "", mf20VDS: "",
                mf8: "", mf8DS: "", mf8VDS: "",
                mf9: "", mf9DS: "", mf9VDS: "",
                mf: "", mfDS: "", mfVDS: "", // Added for PCO
                mf22: "", mf22DS: "", mf22VDS: "", // Added for ADA
                ccmNotes: "",
                ppaNotes: "",
                findings: ""}
        ];

        var cashierTotal = getSLAMCIPGrandTotalPerCashier("slamciPesoGTPC");

        var cashierTotalRow = [
            {product: "<div class='totalRow'>&nbsp;</div>",
                cashier: "<div class='totalRow'><div class='alignRight'>TOTAL COLLECTION</div></div>",
                mf1: "<div class='totalRow'><div class='numFormat'>" + numberWithCommas(totalSLAMCIPCashiersCollection.mf1) + "</div></div>", mf1DS: "<div class='totalRow'>&nbsp;</div>", mf1VDS: "<div class='totalRow'>&nbsp;</div>",
                mf2: "<div class='totalRow'><div class='numFormat'>" + numberWithCommas(totalSLAMCIPCashiersCollection.mf2) + "</div></div>", mf2DS: "<div class='totalRow'>&nbsp;</div>", mf2VDS: "<div class='totalRow'>&nbsp;</div>",
                mf3: "<div class='totalRow'><div class='numFormat'>" + numberWithCommas(totalSLAMCIPCashiersCollection.mf3) + "</div></div>", mf3DS: "<div class='totalRow'>&nbsp;</div>", mf3VDS: "<div class='totalRow'>&nbsp;</div>",
                mf5: "<div class='totalRow'><div class='numFormat'>" + numberWithCommas(totalSLAMCIPCashiersCollection.mf5) + "</div></div>", mf5DS: "<div class='totalRow'>&nbsp;</div>", mf5VDS: "<div class='totalRow'>&nbsp;</div>",
                mf7: "<div class='totalRow'><div class='numFormat'>" + numberWithCommas(totalSLAMCIPCashiersCollection.mf7) + "</div></div>", mf7DS: "<div class='totalRow'>&nbsp;</div>", mf7VDS: "<div class='totalRow'>&nbsp;</div>",
                mf20: "<div class='totalRow'><div class='numFormat'>" + numberWithCommas(totalSLAMCIPCashiersCollection.mf20) + "</div></div>", mf20DS: "<div class='totalRow'>&nbsp;</div>", mf20VDS: "<div class='totalRow'>&nbsp;</div>",
                mf8: "<div class='totalRow'><div class='numFormat'>" + numberWithCommas(totalSLAMCIPCashiersCollection.mf8) + "</div></div>", mf8DS: "<div class='totalRow'>&nbsp;</div>", mf8VDS: "<div class='totalRow'>&nbsp;</div>",
                mf9: "<div class='totalRow'><div class='numFormat'>" + numberWithCommas(totalSLAMCIPCashiersCollection.mf9) + "</div></div>", mf9DS: "<div class='totalRow'>&nbsp;</div>", mf9VDS: "<div class='totalRow'>&nbsp;</div>",
                mf: "<div class='totalRow'><div class='numFormat'>" + numberWithCommas(totalSLAMCIPCashiersCollection.mf) + "</div></div>", mfDS: "<div class='totalRow'>&nbsp;</div>", mfVDS: "<div class='totalRow'>&nbsp;</div>", // Added for PCO
                mf22: "<div class='totalRow'><div class='numFormat'>" + numberWithCommas(totalSLAMCIPCashiersCollection.mf22) + "</div></div>", mf22DS: "<div class='totalRow'>&nbsp;</div>", mf22VDS: "<div class='totalRow'>&nbsp;</div>", // Added for ADA
                ccmNotes: "<div class='totalRow'>&nbsp;</div>",
                ppaNotes: "<div class='totalRow'>&nbsp;</div>",
                findings: "<div class='totalRow'>&nbsp;</div>"}
        ];

        var slamciPesoSummaryData = [
            {
                product: "",
                cashier: "CONSOLIDATED GRAND TOTAL",
                mf1: "<div class='numFormat'>" + numberWithCommas(slamciPesoConsolidatedTotals.mf1) + "</div>", mf1DS: "<div class='totalRow'></div>", mf1VDS: "<div class='totalRow'></div>",
                mf2: "<div class='numFormat'>" + numberWithCommas(slamciPesoConsolidatedTotals.mf2) + "</div>", mf2DS: "<div class='totalRow'></div>", mf2VDS: "<div class='totalRow'></div>",
                mf3: "<div class='numFormat'>" + numberWithCommas(slamciPesoConsolidatedTotals.mf3) + "</div>", mf3DS: "<div class='totalRow'></div>", mf3VDS: "<div class='totalRow'></div>",
                mf5: "<div class='numFormat'>" + numberWithCommas(slamciPesoConsolidatedTotals.mf5) + "</div>", mf5DS: "<div class='totalRow'></div>", mf5VDS: "<div class='totalRow'></div>",
                mf7: "<div class='numFormat'>" + numberWithCommas(slamciPesoConsolidatedTotals.mf7) + "</div>", mf7DS: "<div class='totalRow'></div>", mf7VDS: "<div class='totalRow'></div>",
                mf20: "<div class='numFormat'>" + numberWithCommas(slamciPesoConsolidatedTotals.mf20) + "</div>", mf20DS: "<div class='totalRow'></div>", mf20VDS: "<div class='totalRow'></div>",
                mf8: "<div class='numFormat'>" + numberWithCommas(slamciPesoConsolidatedTotals.mf8) + "</div>",
                mf9: "<div class='numFormat'>" + numberWithCommas(slamciPesoConsolidatedTotals.mf9) + "</div>",
                mf: "<div class='numFormat'>" + numberWithCommas(slamciPesoConsolidatedTotals.mf) + "</div>", // Added for PCO
                mf22: "<div class='numFormat'>" + numberWithCommas(slamciPesoConsolidatedTotals.mf22) + "</div>", // Added for ADA
                ccmNotes: "",
                ppaNotes: "",
                findings: ""
            },
            {
                product: "",
                cashier: "TOTAL CASHIERS COLLECTION",
                mf1: "<div class='numFormat'>" + numberWithCommas(totalSLAMCIPCashiersCollection.mf1) + "</div>", mf1DS: "<div class='totalRow'></div>", mf1VDS: "<div class='totalRow'></div>",
                mf2: "<div class='numFormat'>" + numberWithCommas(totalSLAMCIPCashiersCollection.mf2) + "</div>", mf2DS: "<div class='totalRow'></div>", mf2VDS: "<div class='totalRow'></div>",
                mf3: "<div class='numFormat'>" + numberWithCommas(totalSLAMCIPCashiersCollection.mf3) + "</div>", mf3DS: "<div class='totalRow'></div>", mf3VDS: "<div class='totalRow'></div>",
                mf5: "<div class='numFormat'>" + numberWithCommas(totalSLAMCIPCashiersCollection.mf5) + "</div>", mf5DS: "<div class='totalRow'></div>", mf5VDS: "<div class='totalRow'></div>",
                mf7: "<div class='numFormat'>" + numberWithCommas(totalSLAMCIPCashiersCollection.mf7) + "</div>", mf7DS: "<div class='totalRow'></div>", mf7VDS: "<div class='totalRow'></div>",
                mf20: "<div class='numFormat'>" + numberWithCommas(totalSLAMCIPCashiersCollection.mf20) + "</div>", mf20DS: "<div class='totalRow'></div>", mf20VDS: "<div class='totalRow'></div>",
                mf8: "<div class='numFormat'>" + numberWithCommas(totalSLAMCIPCashiersCollection.mf8) + "</div>",
                mf9: "<div class='numFormat'>" + numberWithCommas(totalSLAMCIPCashiersCollection.mf9) + "</div>",
                mf: "<div class='numFormat'>" + numberWithCommas(totalSLAMCIPCashiersCollection.mf) + "</div>", // Added for PCO
                mf22: "<div class='numFormat'>" + numberWithCommas(totalSLAMCIPCashiersCollection.mf22) + "</div>", // Added for ADA
                ccmNotes: "",
                ppaNotes: "",
                findings: ""
            },
            {
                product: "",
                cashier: "",
                mf1: compareTotalsImg(totalSLAMCIPCashiersCollection.mf1, numberWithCommas(slamciPesoConsolidatedTotals.mf1)), mf1DS: "<div class='totalRow'></div>", mf1VDS: "<div class='totalRow'></div>",
                mf2: "<div class='numFormat'>" + compareTotalsImg(totalSLAMCIPCashiersCollection.mf2, numberWithCommas(slamciPesoConsolidatedTotals.mf2)) + "</div>", mf2DS: "<div class='totalRow'></div>", mf2VDS: "<div class='totalRow'></div>",
                mf3: "<div class='numFormat'>" + compareTotalsImg(totalSLAMCIPCashiersCollection.mf3, numberWithCommas(slamciPesoConsolidatedTotals.mf3)) + "</div>", mf3DS: "<div class='totalRow'></div>", mf3VDS: "<div class='totalRow'></div>",
                mf5: "<div class='numFormat'>" + compareTotalsImg(totalSLAMCIPCashiersCollection.mf5, numberWithCommas(slamciPesoConsolidatedTotals.mf5)) + "</div>", mf5DS: "<div class='totalRow'></div>", mf5VDS: "<div class='totalRow'></div>",
                mf7: "<div class='numFormat'>" + compareTotalsImg(totalSLAMCIPCashiersCollection.mf7, numberWithCommas(slamciPesoConsolidatedTotals.mf7)) + "</div>", mf7DS: "<div class='totalRow'></div>", mf7VDS: "<div class='totalRow'></div>",
                mf20: "<div class='numFormat'>" + compareTotalsImg(totalSLAMCIPCashiersCollection.mf20, numberWithCommas(slamciPesoConsolidatedTotals.mf20)) + "</div>", mf20DS: "<div class='totalRow'></div>", mf20VDS: "<div class='totalRow'></div>",
                mf8: "<div class='numFormat'>" + compareTotalsImg(totalSLAMCIPCashiersCollection.mf8, numberWithCommas(slamciPesoConsolidatedTotals.mf8)) + "</div>",
                mf9: "<div class='numFormat'>" + compareTotalsImg(totalSLAMCIPCashiersCollection.mf9, numberWithCommas(slamciPesoConsolidatedTotals.mf9)) + "</div>",
                mf: "<div class='numFormat'>" + compareTotalsImg(totalSLAMCIPCashiersCollection.mf, numberWithCommas(slamciPesoConsolidatedTotals.mf)) + "</div>", // Added for PCO
                mf22: "<div class='numFormat'>" + compareTotalsImg(totalSLAMCIPCashiersCollection.mf22, numberWithCommas(slamciPesoConsolidatedTotals.mf22)) + "</div>", // Added for ADA
                ccmNotes: "",
                ppaNotes: "",
                findings: ""
            }
        ];

        new TableContainer({
            divName: slamciPesoSummaryDivDetails.divName,
            nodeName: slamciPesoSummaryDivDetails.nodeName,
//            width: "1750px",
            width: "1873px",
            id: slamciPesoSummaryDivDetails.tableId,
            layout: slamciPesoColumns,
            tableClass: "balancingToolSummary",
            data: slamciPesoSummaryData
        }).startUp();

        var slamciPesoSectionData = [];

        appendJsons([slamciPesoSectionData, pesoCashHeader, cashNonCounterData.prodTotals, cncTotal, cashCounterData.prodTotals, ccTotal, areaCashTotal, pesoCheckHeader, checkLocalData.prodTotals, clTotal, checkRegionalData.prodTotals, crTotal, checkOnUsData.prodTotals, couTotal, checkNCData.prodTotals, cNCTotal, checkOTData.prodTotals, cotTotal, postalMoneyOrderData.prodTotals, postalMoneyOrderTotal, areaCheckTotal, nonCashHeader, nonCashData.prodTotals, ncTotal, pcoHeader, pcoInvestData.prodTotals, pcoTotal, cashierTotalHeader, cashierTotal, cashierTotalRow, exemptionHeader, exemptionData.prodTotals, exemptionTotal, generateSnapshotSection(slamcipSnapshot)]);

        new TableContainer({
            divName: slamciPesoDivDetails.divName,
            nodeName: slamciPesoDivDetails.nodeName,
//            width: "1750px",
            width: "1873px",
            id: slamciPesoDivDetails.tableId,
            layout: slamciPesoColumnsNH,
            tableClass: "balancingToolSummary",
            data: slamciPesoSectionData
        }).startUp();

        var slamciPesoTableDiv = document.getElementById(slamciPesoTableDivId);

        slamciPesoTableDiv.style.display = "none";
    });
}


var slamciPesoSecData = [];

function mergeSLAMCIPesoCashierProductTypes() {
    var cashierRep = [];
    for (c in slamciPesoData) {
        //using jQuery.inArray instead of Array.indexOf because it is not supported in IE8 and below. thanks IE.
        if (jQuery.inArray(slamciPesoData[c].cashierId + "~" + slamciPesoData[c].product, cashierRep) === -1) {
            var pushedData = "";
            pushedData = pushedData.concat(slamciPesoData[c].cashierId);
            pushedData = pushedData.concat("~");
            pushedData = pushedData.concat(slamciPesoData[c].product);
            cashierRep.push(pushedData);
        }
    }

    for (var a = 0; a < cashierRep.length; a++) {
        slamciPesoSecData.push(mergeSLAMCIPesoCashierData(cashierRep[a]));
    }
}

function getSLAMCIPesoAreaTotal(area) {
    var tots = {};
    var mf1 = 0;
    var mf2 = 0;
    var mf3 = 0;
    var mf5 = 0;
    var mf7 = 0;
    var mf20 = 0;
    var mf8 = 0;
    var mf9 = 0;
    var mf = 0; // Added for PCO
    var mf22 = 0; // Added for ADA
    for (a in area) {
        mf1 = mf1 + parseFloat(area[a].cashierTotals.mf1Total.replace(/,/g, ""));
        mf2 = mf2 + parseFloat(area[a].cashierTotals.mf2Total.replace(/,/g, ""));
        mf3 = mf3 + parseFloat(area[a].cashierTotals.mf3Total.replace(/,/g, ""));
        mf5 = mf5 + parseFloat(area[a].cashierTotals.mf5Total.replace(/,/g, ""));
        mf7 = mf7 + parseFloat(area[a].cashierTotals.mf7Total.replace(/,/g, ""));
        mf20 = mf20 + parseFloat(area[a].cashierTotals.mf20Total.replace(/,/g, ""));
        mf8 = mf8 + parseFloat(area[a].cashierTotals.mf8Total.replace(/,/g, ""));
        mf9 = mf9 + parseFloat(area[a].cashierTotals.mf9Total.replace(/,/g, ""));
        mf = mf + parseFloat(area[a].cashierTotals.mfTotal.replace(/,/g, "")); // Added for PCO
        mf22 = mf22 + parseFloat(area[a].cashierTotals.mf22Total.replace(/,/g, "")); // Added for ADA
    }
    tots.mf1 = numberWithCommas(parseFloat(mf1).toFixed(2));
    tots.mf2 = numberWithCommas(parseFloat(mf2).toFixed(2));
    tots.mf3 = numberWithCommas(parseFloat(mf3).toFixed(2));
    tots.mf5 = numberWithCommas(parseFloat(mf5).toFixed(2));
    tots.mf7 = numberWithCommas(parseFloat(mf7).toFixed(2));
    tots.mf8 = numberWithCommas(parseFloat(mf8).toFixed(2));
    tots.mf9 = numberWithCommas(parseFloat(mf9).toFixed(2));
    tots.mf20 = numberWithCommas(parseFloat(mf20).toFixed(2));
    tots.mf = numberWithCommas(parseFloat(mf).toFixed(2)); // Added for PCO
    tots.mf22 = numberWithCommas(parseFloat(mf22).toFixed(2)); // Added for ADA
    return tots;
}

function generateSLAMCIPesoSectionData(section, cdTarget) {
    var sectionObj = {};
    var sectionCashierTotals = {};
    var mf1 = 0;
    var mf2 = 0;
    var mf3 = 0;
    var mf5 = 0;
    var mf7 = 0;
    var mf20 = 0;
    var mf8 = 0;
    var mf9 = 0;
    var mf = 0; // Added for PCO
    var mf22 = 0; // Added for ADA
    var returnCNC = [];
    var firstRowFlag = true;
    for (a in slamciPesoSecData) {
        var tempCNC = {};
        if (slamciPesoSecData[a].product === section) {
            var isCashDepo = false;
            var isCheckDepo = false;
            var checkType;
            if (firstRowFlag) {
                switch (section) {
                    case "totalCashNonCounter":
                        tempCNC.product = "<div class='alignLeft'>Non Counter</div>";
                        break;
                    case "totalCashCounter":
                        tempCNC.product = "<div class='alignLeft'>Counter</div>";
                        isCashDepo = true;
                        break;
                    case "totalCheckLocal":
                        tempCNC.product = "<div class='alignLeft'>Cheque - Local</div>";
                        isCheckDepo = true;
                        checkType = "LOCAL";
                        break;
                    case "totalCheckRegional":
                        tempCNC.product = "<div class='alignLeft'>Cheque - Regional</div>";
                        isCheckDepo = true;
                        checkType = "REGIONAL";
                        break;
                    case "totalCheckOnUs":
                        tempCNC.product = "<div class='alignLeft'>Cheque - On Us</div>";
                        isCheckDepo = true;
                        checkType = "ON_US";
                        break;
                    case "totalCheckOT":
                        tempCNC.product = "<div class='alignLeft'>Cheque - OT</div>";
                        isCheckDepo = true;
                        checkType = "OT";
                        break;
                    case "totalCheckNonCounter":
                        tempCNC.product = "<div class='alignLeft'>Cheque - Non Counter</div>";
                        break;
                    case "totalPmo":
                        tempCNC.product = "<div class='alignLeft'>Postal Money Order</div>";
                        break;
                        //case "totalBankOTCCheckPayment": tempCNC.product = "<div class='alignLeft'>Bank OTC Check Payment </div>";isCheckDepo=true;checkType="BANK_OTC";break;
                    case "totalNonCash":
                        tempCNC.product = "";
                        break;
                    case "totalMf": // Added for PCO
                        tempCNC.product = "<div class='alignLeft'>PCO Purchase</div>";
                        break;
                    case "totalPcoInvest": // Added for PCO
                        tempCNC.product = "<div class='alignLeft'>PCO Investment</div>";
                        break;
                    case "reversalProduct" :
                        tempCNC.product = "";
                        break;
                    default:
                        alert("Unknown Product Total: " + section);
                        break;
                }
            } else {
                tempCNC.product = "";
                switch (section) {
                    case "totalCashCounter":
                        isCashDepo = true;
                        break;
                    case "totalCheckLocal":
                        isCheckDepo = true;
                        checkType = "LOCAL";
                        break;
                    case "totalCheckRegional":
                        isCheckDepo = true;
                        checkType = "REGIONAL";
                        break;
                    case "totalCheckOnUs":
                        isCheckDepo = true;
                        checkType = "ON_US";
                        break;
                    case "totalCheckOT":
                        isCheckDepo = true;
                        checkType = "OT";
                        break;
                }
            }

            tempCNC.cashier = "<div class='alignLeft'>" + slamciPesoSecData[a].cashierName + "</div>";
            if (isSLAMCIPConfirmed(slamciPesoSecData[a].cashierId)) {
                if (section === "reversalProduct") {
                    if (parseFloat(slamciPesoSecData[a].mf1) !== 0) {
                        tempCNC.mf1 = "<div class='alignRight'><a style='cursor:pointer;text-decoration:underline;color:blue;'onclick='openSPReversal(" + getBalancingToolProductId(slamciPesoSecData[a].cashierId, "MF1", section, slamciPesoData) + ");'>" + numberWithCommas(parseFloat(slamciPesoSecData[a].mf1).toFixed(2)) + "</a></div>";
                    } else {
                        tempCNC.mf1 = "<div class='alignRight'>" + numberWithCommas(parseFloat(slamciPesoSecData[a].mf1).toFixed(2)) + "</div>";
                    }
                    if (parseFloat(slamciPesoSecData[a].mf2) !== 0) {
                        tempCNC.mf2 = "<div class='alignRight'><a style='cursor:pointer;text-decoration:underline;color:blue;'onclick='openSPReversal(" + getBalancingToolProductId(slamciPesoSecData[a].cashierId, "MF2", section, slamciPesoData) + ");'>" + numberWithCommas(parseFloat(slamciPesoSecData[a].mf2).toFixed(2)) + "</a></div>";
                    } else {
                        tempCNC.mf2 = "<div class='alignRight'>" + numberWithCommas(parseFloat(slamciPesoSecData[a].mf2).toFixed(2)) + "</div>";
                    }
                    if (parseFloat(slamciPesoSecData[a].mf3) !== 0) {
                        tempCNC.mf3 = "<div class='alignRight'><a style='cursor:pointer;text-decoration:underline;color:blue;'onclick='openSPReversal(" + getBalancingToolProductId(slamciPesoSecData[a].cashierId, "MF3", section, slamciPesoData) + ");'>" + numberWithCommas(parseFloat(slamciPesoSecData[a].mf3).toFixed(2)) + "</a></div>";
                    } else {
                        tempCNC.mf3 = "<div class='alignRight'>" + numberWithCommas(parseFloat(slamciPesoSecData[a].mf3).toFixed(2)) + "</div>";
                    }
                    if (parseFloat(slamciPesoSecData[a].mf5) !== 0) {
                        tempCNC.mf5 = "<div class='alignRight'><a style='cursor:pointer;text-decoration:underline;color:blue;'onclick='openSPReversal(" + getBalancingToolProductId(slamciPesoSecData[a].cashierId, "MF5", section, slamciPesoData) + ");'>" + numberWithCommas(parseFloat(slamciPesoSecData[a].mf5).toFixed(2)) + "</a></div>";
                    } else {
                        tempCNC.mf5 = "<div class='alignRight'>" + numberWithCommas(parseFloat(slamciPesoSecData[a].mf5).toFixed(2)) + "</div>";
                    }
                    if (parseFloat(slamciPesoSecData[a].mf7) !== 0) {
                        tempCNC.mf7 = "<div class='alignRight'><a style='cursor:pointer;text-decoration:underline;color:blue;'onclick='openSPReversal(" + getBalancingToolProductId(slamciPesoSecData[a].cashierId, "MF7", section, slamciPesoData) + ");'>" + numberWithCommas(parseFloat(slamciPesoSecData[a].mf7).toFixed(2)) + "</a></div>";
                    } else {
                        tempCNC.mf7 = "<div class='alignRight'>" + numberWithCommas(parseFloat(slamciPesoSecData[a].mf7).toFixed(2)) + "</div>";
                    }
                    if (parseFloat(slamciPesoSecData[a].mf20) !== 0) {
                        tempCNC.mf20 = "<div class='alignRight'><a style='cursor:pointer;text-decoration:underline;color:blue;'onclick='openSPReversal(" + getBalancingToolProductId(slamciPesoSecData[a].cashierId, "MF20", section, slamciPesoData) + ");'>" + numberWithCommas(parseFloat(slamciPesoSecData[a].mf20).toFixed(2)) + "</a></div>";
                    } else {
                        tempCNC.mf20 = "<div class='alignRight'>" + numberWithCommas(parseFloat(slamciPesoSecData[a].mf20).toFixed(2)) + "</div>";
                    }
                    if (parseFloat(slamciPesoSecData[a].mf8) !== 0) {
                        tempCNC.mf8 = "<div class='alignRight'><a style='cursor:pointer;text-decoration:underline;color:blue;'onclick='openSPReversal(" + getBalancingToolProductId(slamciPesoSecData[a].cashierId, "MF8", section, slamciPesoData) + ");'>" + numberWithCommas(parseFloat(slamciPesoSecData[a].mf8).toFixed(2)) + "</a></div>";
                    } else {
                        tempCNC.mf8 = "<div class='alignRight'>" + numberWithCommas(parseFloat(slamciPesoSecData[a].mf8).toFixed(2)) + "</div>";
                    }
                    if (parseFloat(slamciPesoSecData[a].mf9) !== 0) {
                        tempCNC.mf9 = "<div class='alignRight'><a style='cursor:pointer;text-decoration:underline;color:blue;'onclick='openSPReversal(" + getBalancingToolProductId(slamciPesoSecData[a].cashierId, "MF9", section, slamciPesoData) + ");'>" + numberWithCommas(parseFloat(slamciPesoSecData[a].mf9).toFixed(2)) + "</a></div>";
                    } else {
                        tempCNC.mf9 = "<div class='alignRight'>" + numberWithCommas(parseFloat(slamciPesoSecData[a].mf9).toFixed(2)) + "</div>";
                    }
                    if (parseFloat(slamciPesoSecData[a].mf) !== 0) { // Added for PCO
                        tempCNC.mf = "<div class='alignRight'><a style='cursor:pointer;text-decoration:underline;color:blue;'onclick='openSPReversal(" + getBalancingToolProductId(slamciPesoSecData[a].cashierId, "MF", section, slamciPesoData) + ");'>" + numberWithCommas(parseFloat(slamciPesoSecData[a].mf).toFixed(2)) + "</a></div>";
                    } else { // Added for PCO
                        tempCNC.mf = "<div class='alignRight'>" + numberWithCommas(parseFloat(slamciPesoSecData[a].mf).toFixed(2)) + "</div>";
                    }
                    if (parseFloat(slamciPesoSecData[a].mf22) !== 0) { // Added for ADA
                        tempCNC.mf = "<div class='alignRight'><a style='cursor:pointer;text-decoration:underline;color:blue;'onclick='openSPReversal(" + getBalancingToolProductId(slamciPesoSecData[a].cashierId, "MF22", section, slamciPesoData) + ");'>" + numberWithCommas(parseFloat(slamciPesoSecData[a].mf22).toFixed(2)) + "</a></div>";
                    } else { // Added for ADA
                        tempCNC.mf = "<div class='alignRight'>" + numberWithCommas(parseFloat(slamciPesoSecData[a].mf22).toFixed(2)) + "</div>";
                    }
                } else {
                    tempCNC.mf1 = "<div class='alignRight'>" + numberWithCommas(parseFloat(slamciPesoSecData[a].mf1).toFixed(2)) + "</div>";
                    tempCNC.mf2 = "<div class='alignRight'>" + numberWithCommas(parseFloat(slamciPesoSecData[a].mf2).toFixed(2)) + "</div>";
                    tempCNC.mf3 = "<div class='alignRight'>" + numberWithCommas(parseFloat(slamciPesoSecData[a].mf3).toFixed(2)) + "</div>";
                    tempCNC.mf5 = "<div class='alignRight'>" + numberWithCommas(parseFloat(slamciPesoSecData[a].mf5).toFixed(2)) + "</div>";
                    tempCNC.mf7 = "<div class='alignRight'>" + numberWithCommas(parseFloat(slamciPesoSecData[a].mf7).toFixed(2)) + "</div>";
                    tempCNC.mf8 = "<div class='alignRight'>" + numberWithCommas(parseFloat(slamciPesoSecData[a].mf8).toFixed(2)) + "</div>";
                    tempCNC.mf9 = "<div class='alignRight'>" + numberWithCommas(parseFloat(slamciPesoSecData[a].mf9).toFixed(2)) + "</div>";
                    tempCNC.mf20 = "<div class='alignRight'>" + numberWithCommas(parseFloat(slamciPesoSecData[a].mf20).toFixed(2)) + "</div>";
                    tempCNC.mf = "<div class='alignRight'>" + numberWithCommas(parseFloat(slamciPesoSecData[a].mf).toFixed(2)) + "</div>"; // Added for PCO
                    tempCNC.mf22 = "<div class='alignRight'>" + numberWithCommas(parseFloat(slamciPesoSecData[a].mf22).toFixed(2)) + "</div>"; // Added for PCO
                    var tempVal = "";
                    var tempVal2 = "";
                    if (isCashDepo) {
                        tempVal = getCashDepoURL("SLAMCIP", "CASH", "PHP", slamciPesoSecData[a].cashierId);
                        tempVal2 = getValidatedCashDepoURL("SLAMCIP", "CASH", "PHP", slamciPesoSecData[a].cashierId);
                    } else if (isCheckDepo) {
                        tempVal = getChequeDepoURL("SLAMCIP", "CHEQUE", "PHP", slamciPesoSecData[a].cashierId, checkType);
                        tempVal2 = getValidatedChequeDepoURL("SLAMCIP", "CHEQUE", "PHP", slamciPesoSecData[a].cashierId, checkType);
                    }
                    if (parseFloat(slamciPesoSecData[a].mf1) > 0) {
                        tempCNC.mf1DS = tempVal;
                        tempCNC.mf1VDS = tempVal2;
                    }
                    if (parseFloat(slamciPesoSecData[a].mf2) > 0) {
                        tempCNC.mf2DS = tempVal;
                        tempCNC.mf2VDS = tempVal2;
                    }
                    if (parseFloat(slamciPesoSecData[a].mf3) > 0) {
                        tempCNC.mf3DS = tempVal;
                        tempCNC.mf3VDS = tempVal2;
                    }
                    if (parseFloat(slamciPesoSecData[a].mf5) > 0) {
                        tempCNC.mf5DS = tempVal;
                        tempCNC.mf5VDS = tempVal2;
                    }
                    if (parseFloat(slamciPesoSecData[a].mf7) > 0) {
                        tempCNC.mf7DS = tempVal;
                        tempCNC.mf7VDS = tempVal2;
                    }
                    if (parseFloat(slamciPesoSecData[a].mf8) > 0) {
                        tempCNC.mf8DS = tempVal;
                        tempCNC.mf8VDS = tempVal2;
                    }
                    if (parseFloat(slamciPesoSecData[a].mf9) > 0) {
                        tempCNC.mf9DS = tempVal;
                        tempCNC.mf9VDS = tempVal2;
                    }
                    if (parseFloat(slamciPesoSecData[a].mf20) > 0) {
                        tempCNC.mf20DS = tempVal;
                        tempCNC.mf20VDS = tempVal2;
                    }
                    if (parseFloat(slamciPesoSecData[a].mf) > 0) { // Added for PCO
                        tempCNC.mfDS = tempVal;
                        tempCNC.mfVDS = tempVal2;
                    }
                    if (parseFloat(slamciPesoSecData[a].mf22) > 0) { // Added for ADA
                        tempCNC.mf22DS = tempVal;
                        tempCNC.mf22VDS = tempVal2;
                    }
                }
                mf1 = mf1 + parseFloat(slamciPesoSecData[a].mf1);
                mf2 = mf2 + parseFloat(slamciPesoSecData[a].mf2);
                mf3 = mf3 + parseFloat(slamciPesoSecData[a].mf3);
                mf5 = mf5 + parseFloat(slamciPesoSecData[a].mf5);
                mf7 = mf7 + parseFloat(slamciPesoSecData[a].mf7);
                mf8 = mf8 + parseFloat(slamciPesoSecData[a].mf8);
                mf9 = mf9 + parseFloat(slamciPesoSecData[a].mf9);
                mf20 = mf20 + parseFloat(slamciPesoSecData[a].mf20);
                mf = mf + parseFloat(slamciPesoSecData[a].mf); // Added for PCO
                mf22 = mf22 + parseFloat(slamciPesoSecData[a].mf22); // Added for ADA
            } else {
                tempCNC.mf1 = "<div class='alignCenter'>-</div>";
                tempCNC.mf2 = "<div class='alignCenter'>-</div>";
                tempCNC.mf3 = "<div class='alignCenter'>-</div>";
                tempCNC.mf5 = "<div class='alignCenter'>-</div>";
                tempCNC.mf7 = "<div class='alignCenter'>-</div>";
                tempCNC.mf8 = "<div class='alignCenter'>-</div>";
                tempCNC.mf9 = "<div class='alignCenter'>-</div>";
                tempCNC.mf20 = "<div class='alignCenter'>-</div>";
                tempCNC.mf = "<div class='alignCenter'>-</div>"; // Added for PCO
                tempCNC.mf22 = "<div class='alignCenter'>-</div>"; // Added for PCO
            }
            if (jQuery.inArray(section, excludeFromCashierTotal) === -1) {
                slamciPesoConsolidatedTotals.mf1 = (parseFloat(slamciPesoConsolidatedTotals.mf1) + parseFloat(slamciPesoSecData[a].mf1)).toFixed(2);
                slamciPesoConsolidatedTotals.mf2 = (parseFloat(slamciPesoConsolidatedTotals.mf2) + parseFloat(slamciPesoSecData[a].mf2)).toFixed(2);
                slamciPesoConsolidatedTotals.mf3 = (parseFloat(slamciPesoConsolidatedTotals.mf3) + parseFloat(slamciPesoSecData[a].mf3)).toFixed(2);
                slamciPesoConsolidatedTotals.mf5 = (parseFloat(slamciPesoConsolidatedTotals.mf5) + parseFloat(slamciPesoSecData[a].mf5)).toFixed(2);
                slamciPesoConsolidatedTotals.mf7 = (parseFloat(slamciPesoConsolidatedTotals.mf7) + parseFloat(slamciPesoSecData[a].mf7)).toFixed(2);
                slamciPesoConsolidatedTotals.mf8 = (parseFloat(slamciPesoConsolidatedTotals.mf8) + parseFloat(slamciPesoSecData[a].mf8)).toFixed(2);
                slamciPesoConsolidatedTotals.mf9 = (parseFloat(slamciPesoConsolidatedTotals.mf9) + parseFloat(slamciPesoSecData[a].mf9)).toFixed(2);
                slamciPesoConsolidatedTotals.mf20 = (parseFloat(slamciPesoConsolidatedTotals.mf20) + parseFloat(slamciPesoSecData[a].mf20)).toFixed(2);
                slamciPesoConsolidatedTotals.mf = (parseFloat(slamciPesoConsolidatedTotals.mf) + parseFloat(slamciPesoSecData[a].mf)).toFixed(2); // Added for PCO
                slamciPesoConsolidatedTotals.mf22 = (parseFloat(slamciPesoConsolidatedTotals.mf22) + parseFloat(slamciPesoSecData[a].mf22)).toFixed(2); // Added for ADA
            }

            tempCNC.ccmNotes = "<div class='alignCenter'>" + getNote(rowIndex, "CCM") + "</div>";
            tempCNC.ppaNotes = "<div class='alignCenter'>" + getNote(rowIndex, "PPA") + "</div>";
            tempCNC.findings = "<div class='alignCenter' collapseTarget='" + cdTarget + "'>" + getNote(rowIndex, "FINDINGS") + "</div>";
            rowIndex++;
            firstRowFlag = false;
            returnCNC.push(tempCNC);
        }
    }
    sectionCashierTotals.mf1Total = numberWithCommas(parseFloat(mf1).toFixed(2));
    sectionCashierTotals.mf2Total = numberWithCommas(parseFloat(mf2).toFixed(2));
    sectionCashierTotals.mf3Total = numberWithCommas(parseFloat(mf3).toFixed(2));
    sectionCashierTotals.mf5Total = numberWithCommas(parseFloat(mf5).toFixed(2));
    sectionCashierTotals.mf7Total = numberWithCommas(parseFloat(mf7).toFixed(2));
    sectionCashierTotals.mf8Total = numberWithCommas(parseFloat(mf8).toFixed(2));
    sectionCashierTotals.mf9Total = numberWithCommas(parseFloat(mf9).toFixed(2));
    sectionCashierTotals.mf20Total = numberWithCommas(parseFloat(mf20).toFixed(2));
    sectionCashierTotals.mfTotal = numberWithCommas(parseFloat(mf).toFixed(2)); // Added for PCO
    sectionCashierTotals.mf22Total = numberWithCommas(parseFloat(mf22).toFixed(2)); // Added for ADA
    sectionObj.prodTotals = returnCNC;
    sectionObj.cashierTotals = sectionCashierTotals;
    return sectionObj;
}

function mergeSLAMCIPesoCashierData(cashierProduct) {
    var cpArr = cashierProduct.split("~");
    var cashierId = cpArr[0];
    var product = cpArr[1];
    var tempCashier = {};
    for (a in slamciPesoData) {
        if (slamciPesoData[a].cashierId === cashierId && slamciPesoData[a].product === product) {
            if (slamciPesoData[a].productCode === "MF1") {
                tempCashier.mf1 = slamciPesoData[a].total;
            } else if (slamciPesoData[a].productCode === "MF2") {
                tempCashier.mf2 = slamciPesoData[a].total;
            } else if (slamciPesoData[a].productCode === "MF3") {
                tempCashier.mf3 = slamciPesoData[a].total;
            } else if (slamciPesoData[a].productCode === "MF5") {
                tempCashier.mf5 = slamciPesoData[a].total;
            } else if (slamciPesoData[a].productCode === "MF7") {
                tempCashier.mf7 = slamciPesoData[a].total;
            } else if (slamciPesoData[a].productCode === "MF20") {
                tempCashier.mf20 = slamciPesoData[a].total;
            } else if (slamciPesoData[a].productCode === "MF8") {
                tempCashier.mf8 = slamciPesoData[a].total;
            } else if (slamciPesoData[a].productCode === "MF9") {
                tempCashier.mf9 = slamciPesoData[a].total;
            } else if (slamciPesoData[a].productCode === "MF") { // Added for PCO
                tempCashier.mf = slamciPesoData[a].total;
            } else if (slamciPesoData[a].productCode === "MF22") { // Added for ADA
                tempCashier.mf22 = slamciPesoData[a].total;
            }
            tempCashier.cashierId = slamciPesoData[a].cashierId;
            tempCashier.cashierName = slamciPesoData[a].cashierName;
            tempCashier.product = product;
        }
    }
    return tempCashier;
}

function getSLAMCIPGrandTotalPerCashier(cdTarget) {
    var returnSection = [];
    var c = 0;
    for (c in cashierList) {
        var mf1 = 0;
        var mf2 = 0;
        var mf3 = 0;
        var mf5 = 0;
        var mf7 = 0;
        var mf8 = 0;
        var mf9 = 0;
        var mf20 = 0;
        var mf = 0; // Added for PCO
        var mf22 = 0; // Added for ADA
        var d = 0;
        for (d in slamciPesoData) {
            if (jQuery.inArray(slamciPesoData[d].product, excludeFromCashierTotal) === -1 && slamciPesoData[d].cashierId.toUpperCase() === cashierList[c].id.toUpperCase()) {
                switch (slamciPesoData[d].productCode) {
                    case "MF1":
                        mf1 = mf1 + parseFloat(slamciPesoData[d].total);
                        break;
                    case "MF2":
                        mf2 = mf2 + parseFloat(slamciPesoData[d].total);
                        break;
                    case "MF3":
                        mf3 = mf3 + parseFloat(slamciPesoData[d].total);
                        break;
                    case "MF5":
                        mf5 = mf5 + parseFloat(slamciPesoData[d].total);
                        break;
                    case "MF7":
                        mf7 = mf7 + parseFloat(slamciPesoData[d].total);
                        break;
                    case "MF8":
                        mf8 = mf8 + parseFloat(slamciPesoData[d].total);
                        break;
                    case "MF9":
                        mf9 = mf9 + parseFloat(slamciPesoData[d].total);
                        break;
                    case "MF20":
                        mf20 = mf20 + parseFloat(slamciPesoData[d].total);
                        break;
                    case "MF": // Added for PCO
                        mf = mf + parseFloat(slamciPesoData[d].total);
                        break;
                    case "MF22": // Added for ADA
                        mf22 = mf22 + parseFloat(slamciPesoData[d].total);
                        break;
                }
            }
        }
        var tempCashier = {};
        tempCashier.cashier = "<div class='alignLeft'  collapseTarget='" + cdTarget + "'>" + cashierList[c].name + "</div>";
        if (isSLAMCIPConfirmed(cashierList[c].id)) {
            tempCashier.mf1 = "<div class='alignRight'>" + numberWithCommas(mf1.toFixed(2)) + "</div>";
            tempCashier.mf2 = "<div class='alignRight'>" + numberWithCommas(mf2.toFixed(2)) + "</div>";
            tempCashier.mf3 = "<div class='alignRight'>" + numberWithCommas(mf3.toFixed(2)) + "</div>";
            tempCashier.mf5 = "<div class='alignRight'>" + numberWithCommas(mf5.toFixed(2)) + "</div>";
            tempCashier.mf7 = "<div class='alignRight'>" + numberWithCommas(mf7.toFixed(2)) + "</div>";
            tempCashier.mf8 = "<div class='alignRight'>" + numberWithCommas(mf8.toFixed(2)) + "</div>";
            tempCashier.mf9 = "<div class='alignRight'>" + numberWithCommas(mf9.toFixed(2)) + "</div>";
            tempCashier.mf20 = "<div class='alignRight'>" + numberWithCommas(mf20.toFixed(2)) + "</div>";
            tempCashier.mf = "<div class='alignRight'>" + numberWithCommas(mf.toFixed(2)) + "</div>"; // Added for PCO
            tempCashier.mf22 = "<div class='alignRight'>" + numberWithCommas(mf22.toFixed(2)) + "</div>"; // Added for ADA
        } else {
            tempCashier.mf1 = "<div class='alignCenter'>-</div>";
            tempCashier.mf2 = "<div class='alignCenter'>-</div>";
            tempCashier.mf3 = "<div class='alignCenter'>-</div>";
            tempCashier.mf5 = "<div class='alignCenter'>-</div>";
            tempCashier.mf7 = "<div class='alignCenter'>-</div>";
            tempCashier.mf8 = "<div class='alignCenter'>-</div>";
            tempCashier.mf9 = "<div class='alignCenter'>-</div>";
            tempCashier.mf20 = "<div class='alignCenter'>-</div>";
            tempCashier.mf = "<div class='alignCenter'>-</div>"; // Added for PCO
            tempCashier.mf22 = "<div class='alignCenter'>-</div>"; // Added for ADA
        }
        returnSection.push(tempCashier);
    }
    return returnSection;
}

// Added "mf" for PCO
// Added "MF22" for ADA
var slamciPesoConsolidatedTotals = {mf1: "0.00", mf2: "0.00", mf3: "0.00", mf5: "0.00", mf7: "0.00", mf8: "0.00", mf9: "0.00", mf20: "0.00", mf: "0.00", mf22: "0.00"};
