///this is to fix problem with horizontal overflow scrollbar
window.onload = function() {
    // only apply to IE
    if (!/*@cc_on!@*/0)
        return;

    // find every element to test
    var all = document.getElementsByTagName('*'), i = all.length;

    // fast reverse loop
    while (i--) {
        // if the scrollWidth (the real width) is greater than
        // the visible width, then apply style changes
        if (all[i].scrollWidth > all[i].offsetWidth) {
            all[i].style['paddingBottom'] = '20px';
            all[i].style['overflowY'] = 'auto';
        }
    }
};
///

var cashierRecon = ["NEW", "PENDING"];
var reviewAndApproval = ["For Review and Approval", "Awaiting Feedback"];
var ppaRecon = ["Verified", "Not Verified", "Approved for Reconciliation", "Reqts Submitted", "Reqts Not Submitted"];
var ppaAbeyance = ["For Verification", "Awaiting Reqts"];

var dcrCashierId;
var ccName;
var ccId;
var dcrDateStr;
var formattedDcrDateStr;
var dcrId;
var dcrStatus;
var noteList = [];
var rowIndex = 0;
var commentCtr = 1;
var userRoles = [];
var cashierList = [];
var inactiveCashierList = [];
var inactiveId = [];
var commentList = [];
var coupleCommentList = [];
var consolidatedDataJSON = [];
var slfpiData = [];
var slgfiPesoData = [];
var slgfiDollarData = [];
var slamciPesoData = [];
var slamciDollarData = [];
var slocpiPesoData = [];
var slocpiDollarData = [];
var otherPesoData = [];
var otherDollarData = [];
var attachments = [];
var confirmations = [];
var uniqueCashier = [];
var slamcipConfirm = [];
var slamcidConfirm = [];
var slocpiConfirm = [];
var slgfiConfirm = [];
var slfpiConfirm = [];
var otherConfirm = [];
var role;
var confirmed = true;
var nonDay1;
var cashDepositSlips = [];
var chequeDepositSlips = [];
var validatedCashDepositSlips = [];
var validatedChequeDepositSlips = [];
var slocpiSnapshot = [];
var slamcipSnapshot = [];
var slamcidSnapshot = [];
var slfpiSnapshot = [];
var slgfiSnapshot = [];
var otherSnapshot = [];
var userIdShortname = [];
var slamciPaymentSubTypeCount = 16; //this should be equal to the number of payment types for slamci peso and dollar combined
var cashierCtr = 0;

var isPpaQa = true; //added for MR-WF-16-00037

var onEditCallback = function(remaining) {
    $(this).siblings('.charsRemaining').text("Characters remaining: " + remaining);

    if (remaining > 0) {
        $(this).css('background-color', 'white');
    }
}

var onLimitCallback = function() {
    $(this).css('background-color', 'red');
}

function exCoArrow(val) {
    var imageChild = val.getElementsByTagName("img");
    var image = imageChild[0];
    var imgSrc = image.src;
    var fileName = imgSrc.substring(imgSrc.lastIndexOf("/") + 1, imgSrc.search(".JPG"));

    if (fileName == "collapseArrow") {
        image.src = imgSrc.replace(fileName, "expandArrow");
    } else {
        image.src = imgSrc.replace(fileName, "collapseArrow");
    }

}

var balancingToolHeader = [{
        name: "",
        field: "product",
        width: "15%"
    }, {
        name: "",
        field: "cashier",
        width: "25%"
    }, {
        name: "VUL/TRAD",
        field: "vulTrad",
        width: "7%"
    }, {
        name: "DS",
        field: "dsVT",
        width: "6%"
    }, {
        name: "VDS",
        field: "vdsVT",
        width: "6%"
    }, {
        name: "GROUP LIFE",
        field: "groupLife",
        width: "6%"
    }, {
        name: "DS",
        field: "dsGL",
        width: "6%"
    }, {
        name: "VDS",
        field: "vdsGL",
        width: "7%"
    }, {
        name: "CCM NOTES",
        field: "ccmNotes",
        width: "7%"
    }, {
    	// Modified for MR-WF-16-00036 - Change PPA notes to CCQA notes
        name: "CCQA NOTES",
        field: "ppaNotes",
        width: "7%"
    }, {
        name: "Findings",
        field: "findings",
        width: "8%"
    }];

var balancingToolHeaderless = [{
        name: "",
        field: "product",
        width: "15%"
    }, {
        name: "",
        field: "cashier",
        width: "25%"
    }, {
        name: "",
        field: "vulTrad",
        width: "7%"
    }, {
        name: "",
        field: "dsVT",
        width: "6%"
    }, {
        name: "",
        field: "vdsVT",
        width: "6%"
    }, {
        name: "",
        field: "groupLife",
        width: "6%"
    }, {
        name: "",
        field: "dsGL",
        width: "6%"
    }, {
        name: "",
        field: "vdsGL",
        width: "7%"
    }, {
        name: "",
        field: "ccmNotes",
        width: "7%"
    }, {
        name: "",
        field: "ppaNotes",
        width: "7%"
    }, {
        name: "",
        field: "findings",
        width: "8%"
    }];

var counterPaymentHeaderRow = [{
        product: "<div class='alignLeftRed'>Counter Payment</div>",
        cashier: "",
        vulTrad: "",
        dsVT: "",
        vdsVT: "",
        groupLife: "",
        dsGL: "",
        vdsGL: "",
        ccmNotes: "",
        ppaNotes: "",
        findings: ""
    }];
var nonCounterPaymentHeaderRow = [{
        product: "<div class='alignLeftRed'>NonCounter Payment</div>",
        cashier: "",
        vulTrad: "",
        dsVT: "",
        vdsVT: "",
        groupLife: "",
        dsGL: "",
        vdsGL: "",
        ccmNotes: "",
        ppaNotes: "",
        findings: ""
    }];
var pesoPCData = [{
        product: "<div class='alignLeft'>PESO CASH</div>",
        cashier: "",
        vulTrad: "",
        dsVT: "",
        vdsVT: "",
        groupLife: "",
        dsGL: "",
        vdsGL: "",
        ccmNotes: "",
        ppaNotes: "",
        findings: ""
    }];
var pesoCHKData = [{
        product: "<div class='alignLeft'>PESO CHECK</div>",
        cashier: "",
        vulTrad: "",
        dsVT: "",
        vdsVT: "",
        groupLife: "",
        dsGL: "",
        vdsGL: "",
        ccmNotes: "",
        ppaNotes: "",
        findings: ""
    }];
var pesoCardData = [{
        product: "<div class='alignLeft'>PESO CARD</div>",
        cashier: "",
        vulTrad: "",
        dsVT: "",
        vdsVT: "",
        groupLife: "",
        dsGL: "",
        vdsGL: "",
        ccmNotes: "",
        ppaNotes: "",
        findings: ""
    }];
var nonPostedData = [{
        product: "<div class='alignLeft'>NON-POSTED(from DTR)</div>",
        cashier: "",
        vulTrad: "",
        dsVT: "",
        vdsVT: "",
        groupLife: "",
        dsGL: "",
        vdsGL: "",
        ccmNotes: "",
        ppaNotes: "",
        findings: ""
    }];
var forManualPostingData = [{
        product: "<div class='alignLeft'>FOR MANUAL POSTING</div>",
        cashier: "",
        vulTrad: "",
        dsVT: "",
        vdsVT: "",
        groupLife: "",
        dsGL: "",
        vdsGL: "",
        ccmNotes: "",
        ppaNotes: "",
        findings: ""
    }];
var exemptionsData = [{
        product: "<div class='alignLeft'>EXEMPTIONS</div>",
        cashier: "",
        vulTrad: "",
        dsVT: "",
        vdsVT: "",
        groupLife: "",
        dsGL: "",
        vdsGL: "",
        ccmNotes: "",
        ppaNotes: "",
        findings: ""
    }];
var sessionTotalData = [{
        product: "<div class='alignLeft'>SESSION TOTAL</div>",
        cashier: "",
        vulTrad: "",
        dsVT: "",
        vdsVT: "",
        groupLife: "",
        dsGL: "",
        vdsGL: "",
        ccmNotes: "",
        ppaNotes: "",
        findings: ""
    }];
var collectionDetailsData = [{
        product: "<div class='alignLeft'>Collection Details</div>",
        cashier: "",
        vulTrad: "",
        dsVT: "",
        vdsVT: "",
        groupLife: "",
        dsGL: "",
        vdsGL: "",
        ccmNotes: "",
        ppaNotes: "",
        findings: ""
    }];

function generateBalancingToolSummary(tableDivId, summaryDivDetails, divDetails, summary, pesoCash, checkNC, checkC, pesoCard, nonPosted, forManualPosting, exemp, session, collection, tabLayout, tabLayoutNH) {

    $(document).ready(function() {

        new TableContainer({
            divName: summaryDivDetails.divName,
            nodeName: summaryDivDetails.nodeName,
            width: "100%",
            id: summaryDivDetails.tableId,
            layout: tabLayout,
            tableClass: "balancingToolSummary",
            data: summary
        }).startUp();

        var summaryData = [];

        appendJsons([summaryData, pesoPCData, nonCounterPaymentHeaderRow, pesoCash.pesoCashNCPDS, pesoCash.pesoCashNCPTT, counterPaymentHeaderRow, pesoCash.pesoCashCPC, pesoCash.pcTotal, pesoCHKData, nonCounterPaymentHeaderRow, checkNC.checkLocal, checkNC.checkRegional, checkNC.checkTelegraphicTransfer, checkNC.checkOnUs, checkNC.checkTotal, counterPaymentHeaderRow, checkC.checkLocal, checkC.checkRegional, checkC.checkTelegraphicTransfer, checkC.checkOnUs, checkC.checkTotal, pesoCardData, pesoCard.pesoCardPOSBPI, pesoCard.pesoCardPOSCTB, pesoCard.pesoCardTotal, nonPostedData, nonPosted.npNBI, nonPosted.npNBVUL, nonPosted.npVULR, nonPosted.npNC, nonPosted.npTotal, forManualPostingData, forManualPosting.fmpNP, forManualPosting.fmpW, forManualPosting.fmpTot, exemptionsData, exemp.exmptns, exemp.exmptnsTot, sessionTotalData, session.sessionTot, session.sessionGT, collectionDetailsData, collection.collectionDet]);

        new TableContainer({
            divName: divDetails.divName,
            nodeName: divDetails.nodeName,
            width: "100%",
            id: divDetails.tableId,
            layout: tabLayoutNH,
            tableClass: "balancingToolSummary",
            data: summaryData
        }).startUp();

        var tableDiv = document.getElementById(tableDivId);

        tableDiv.style.display = "none";
    });

}

function showData(val) {
    var nextDiv = val.nextSibling;
    if (nextDiv.style.display == "none") {
        nextDiv.style.display = "inline";
    } else {
        nextDiv.style.display = "none";
    }
}

$(document).ready(function() {
    var actionDivsLayout = [{
            name: "",
            field: "launch",
            width: "50%"
        }, {
            name: "",
            field: "response",
            width: "50%"
        }];

    var actionDivsData = [{
            launch: "<iframe id='iframe_a' scrolling='no' frameborder='0' name='iframe_a' src='../cashierSP/comments.html?dcrId=" + dcrId + "'></iframe></div><div id='allCommentsPopUpDiv'><div id='allCommentsPopUpTableNode'><div id='allCommentsPopUpNode'></div></div></div>",
            response: "<div id='responseDiv'><div id='responseHeader' class='responseHeader'>Response</div><div id='responseNode'></div></div><div class='responseHeader'>Common Tools</div><div id='launchDiv'><div id='launchNode'></div>"
        }];

    new TableContainer({
        divName: "actionDivs",
        nodeName: "actionNode",
        width: "100%",
        id: "actionTable",
        layout: actionDivsLayout,
        tableClass: "spHeaderClass",
        data: actionDivsData
    }).startUp();

    var invalidStatus = false;
    var response = document.createElement("select");
    response.id = "response";
    response.style.width = "100%";

    var responseLabel = document.createElement("div");
    responseLabel.id = "responseLabel";

    if (jQuery.inArray("PPA", userRoles) != -1) {
        role = "PPA";
        // Modified for MR-WF-16-00220 - Disable response list
        if (dcrStatus == 'Approved for Recon') {
            dcrStatus = 'PPA Reconciled';
        }
        if (jQuery.inArray(dcrStatus, PPA) != -1) {
            for (p in PPA) {
                var tempOption = document.createElement("option");
                tempOption.innerHTML = PPA[p];
                tempOption.value = PPA[p];
                response.appendChild(tempOption);
            }
        } else {
            var tempOption = document.createElement("span");
            tempOption.innerHTML = dcrStatus;
            responseLabel.appendChild(tempOption);
            invalidStatus = true;
        }
    } else if (jQuery.inArray("MANAGER", userRoles) != -1) {
        role = "MANAGER";
        // Modified for MR-WF-16-00220 - Disable response list
        if (dcrStatus == 'For Review and Approval') {
            dcrStatus = 'Approve For Reconciliation';
        }
        
        if (jQuery.inArray(dcrStatus, MANAGER) != -1) {
            for (mn in MANAGER) {
                var tempOption = document.createElement("option");
                tempOption.innerHTML = MANAGER[mn];
                tempOption.value = MANAGER[mn];
                response.appendChild(tempOption);
            }
            response.onchange = function(response) {
                return isResponseBlank();
            };
        } else {
            var tempOption = document.createElement("span");
            tempOption.innerHTML = dcrStatus;
            responseLabel.appendChild(tempOption);
            invalidStatus = true;
        }
    } else if (jQuery.inArray("CASHIER", userRoles) != -1) {
        role = "CASHIER";
        if (jQuery.inArray(dcrStatus, cashierRecon) != -1) {
            dcrStatus = CASHIER[1];
        }
        
        if (jQuery.inArray(dcrStatus, CASHIER) != -1) {
            for (to in CASHIER) {
                var tempOption = document.createElement("option");
                tempOption.innerHTML = CASHIER[to];
                tempOption.value = CASHIER[to];
                response.appendChild(tempOption);
            }
            response.onchange = function(response) {
                return isResponseBlank()
            };
        } else {
            var tempOption = document.createElement("span");
            tempOption.innerHTML = dcrStatus;
            responseLabel.appendChild(tempOption);
            invalidStatus = true;
        }
    } else if (jQuery.inArray("OTHER", userRoles) != -1) {
        role = "OTHER";
        // Added for MR-WF-16-00220 - Disable response list
        var tempOption = document.createElement("span");
        tempOption.innerHTML = dcrStatus;
        responseLabel.appendChild(tempOption);
        invalidStatus = true;
    }
    var responseDiv = document.getElementById("responseDiv");

    if (invalidStatus) {
        $('#responseHeader').html('Status');
        responseDiv.appendChild(responseLabel);
    } else {
        responseDiv.appendChild(response);
    }

    var spaDiv = document.createElement("div");
    spaDiv.innerHTML = "Step Processor Actions";
    spaDiv.id = "spaDiv";
    responseDiv.appendChild(spaDiv);

    var actionButtonDiv = document.createElement("div");
    responseDiv.appendChild(actionButtonDiv);

    var completeButton = document.createElement("button");
    completeButton.id = "completeButton";
    completeButton.value = "COMPLETE";
    completeButton.onclick = completeWorkItem;
    completeButton.style.width = "120px";
    if (role == "PPA") {
        if (jQuery.inArray(dcrStatus, ppaRecon) == -1 && jQuery.inArray(dcrStatus, ppaAbeyance) == -1) {
            completeButton.disabled = true;
        }
    } else if (role == "MANAGER") {
        if (dcrStatus != "For Review and Approval" && dcrStatus != "Awaiting Feedback") {
            completeButton.disabled = true;
        }
    } else if (role == "CASHIER") {
        if (dcrStatus != "NEW" && dcrStatus != "PENDING") {
            completeButton.disabled = true;
        }
    }
    
    var saveButton = document.createElement("button");
    saveButton.id = "saveButton";
    saveButton.value = "SAVE";
    saveButton.style.width = "120px";
    saveButton.onclick = function() {
        callSaveStepProcData();
    };
    
    var exitButton = document.createElement("button");
    exitButton.onclick = closeWindow;
    exitButton.value = "EXIT";
    exitButton.style.width = "120px";

    actionButtonDiv.appendChild(completeButton);
    actionButtonDiv.appendChild(saveButton);
    actionButtonDiv.appendChild(exitButton);

    var launchNode = document.getElementById("launchNode");
    var emailButton = document.createElement("button");
    emailButton.value = "EMAIL";
    emailButton.onclick = openEmail;
    
    var addNoteButton = document.createElement("button");
    addNoteButton.value = "ADD NOTE";
    addNoteButton.onclick = openAddNote;
    
    var attachFileButton = document.createElement("button");
    attachFileButton.value = "ATTACH FILE";
    attachFileButton.onclick = openAttachFile;
    
    var candidateMatchButton = document.createElement("button");
    candidateMatchButton.value = "CANDIDATE MATCH";
    candidateMatchButton.id = "candidateMatchButton";
    candidateMatchButton.onclick = openCandidateMatch;
    
    var errorTaggingButton = document.createElement("button");
    errorTaggingButton.id = "errorTaggingButton";
    errorTaggingButton.value = "ERROR TAGGING";
    errorTaggingButton.onclick = openErrorTag;
    
    //added for MR-WF-16-00037
	var viewMDSButton = document.createElement("button");
	viewMDSButton.value = "VIEW MDS";
	viewMDSButton.onclick = openMDS;
    
    var workflowHistoryButton = document.createElement("button");
    workflowHistoryButton.value = "WF HISTORY";
    workflowHistoryButton.onclick = openWFHistory;
    
    var tempBr1 = document.createElement("br");
    var viewCommentButton = document.createElement("button");
    viewCommentButton.value = "VIEW COMMENTS";
    viewCommentButton.id = "viewCommentButton";
    viewCommentButton.onclick = showViewCommentsPopUp;
    
    var addCommentButton = document.createElement("button");
    addCommentButton.value = "ADD COMMENT";
    addCommentButton.id = "addCommentButton";
    addCommentButton.onclick = showAddCommentPopUp;
    
    // Append creted button on launch node
    launchNode.appendChild(workflowHistoryButton);
    launchNode.appendChild(emailButton);
    launchNode.appendChild(addNoteButton);
    launchNode.appendChild(attachFileButton);
    launchNode.appendChild(candidateMatchButton);
    launchNode.appendChild(tempBr1);
	launchNode.appendChild(viewMDSButton); //added for MR-WF-16-00037
    launchNode.appendChild(errorTaggingButton);
    launchNode.appendChild(viewCommentButton);
    launchNode.appendChild(addCommentButton);
});

function closeWindow() {
    window.close();
}

function compareTotalsImg(cashier, consolidated) {
    if (consolidated != cashier) {
        return "<img src='../images/redx.JPG'/>";
    } else if (parseFloat(consolidated) == 0 && parseFloat(cashier) == 0) {
        return "";
    }
    return "<img src='../images/greencheck.jpg'/>";
}

function getNote(ind, noteType) {

    var returnNote = "";
    for (noteCnt in noteList) {
        if (noteList[noteCnt].rowNum == ind && noteList[noteCnt].type == noteType) {
            returnNote = "<a href='#' id='noteId" + ind + noteType + "' rowInd='" + ind + "' noteType='" + noteType + "' onclick='showNoteForm(this);' >View</a>";
            return returnNote;
        }
    }

    if (noteType == "CCM") {
        // Modified for MR-WF-16-00230 - Finance view on CCQA/PPA notes
        if (jQuery.inArray("MANAGER", userRoles) != -1 && "Reconciled" !== dcrStatus) {
            returnNote = "<a href='#' id='noteId" + ind + noteType + "' rowInd='" + ind + "' noteType='" + noteType + "' onclick='showNoteForm(this);' >Add</a>";
        } else {
            returnNote = "&nbsp;-&nbsp;";
        }
    } else if (noteType == "PPA" || noteType == "FINDINGS") {
        // Modified for MR-WF-16-00230 - Finance view on CCQA/PPA notes
        if (jQuery.inArray("PPA", userRoles) != -1 && "Reconciled" !== dcrStatus) {
            returnNote = "<a href='#' id='noteId" + ind + noteType + "' rowInd='" + ind + "' noteType='" + noteType + "' onclick='showNoteForm(this);' >Add</a>";
        } else {
            returnNote = "&nbsp;-&nbsp;";
        }
    }

    return returnNote;
}

var showViewCommentsPopUp = function() {
    $(function() {
        var popupTable = _globalTableContainer["popupTable"];
        if (popupTable != null) {
            popupTable.destroyTable();
        }
        $("#allCommentsPopUpTableNode").dialog({
            autoOpen: true,
            height: 600,
            width: 828,
            modal: true,
            closeOnEscape: true,
            draggable: false,
            resizable: false,
            dialogClass: "commentTableClass",
            title: "View Comments"
        }).dialog('widget').position({
            my: 'center',
            at: 'center',
            of: $(document.getElementById("mainTemplateBodyId"))
        });

        setDataForPopUp(commentList);
    });
};

function setDataForPopUp(commentData) {
    $(document).ready(function() {

        new TableContainer({
            divName: "allCommentsPopUpTableNode",
            nodeName: "allCommentsPopUpNode",
            width: "100%",
            id: "popupTable",
            layout: commentLayout,
            tableClass: "commentBorderedTable"
        }).startUp();

        var popupTable = _globalTableContainer["popupTable"];

        popupTable.setData(commentData);
        popupTable.refreshTable();

    });
}

var commentLayout = [{
        name: "User",
        field: "acf2id",
        width: "10%"
    }, {
        name: "Date and Time",
        field: "datePostedFormatted",
        width: "20%"
    }, {
        name: "Remark",
        field: "remarks",
        width: "70%"
    }];

var showAddCommentPopUp = function() {
    document.getElementById("addCommentForm").reset();
    $(function() {
        $("#addCommentDiv").dialog({
            autoOpen: true,
            height: "auto",
            minHeight: 650,
            width: 540,
            modal: true,
            closeOnEscape: true,
            draggable: false,
            resizable: false,
            dialogClass: "commentFormClass",
            title: "Add Comment"

        }).dialog('widget').position({
            my: 'center',
            at: 'center',
            of: $(document.getElementById("mainTemplateBodyId"))
        });
    });

    $('textarea[maxlength]').limitMaxlength({
        onEdit: onEditCallback,
        onLimit: onLimitCallback
    });
    if (jQuery.inArray(dcrStatus, cashierRecon) == -1) {
        var dcrCashierId = document.getElementById("dcrCashierId");
        var sendLable = document.getElementById("sendLabel");
        dcrCashierId.style.visibility = "hidden";
        sendLable.style.visibility = "hidden";
    }
};

function closeAddComment() {
    $("#addCommentDiv").dialog('close');
}

function submitComment() {

    var commentText = document.getElementById("commentRemarks");
    if (commentText.value.length == 0) {
        alert("No remarks");
        return false;
    }

    var commentForm = document.getElementById("addCommentForm");
    commentForm.submit();
    closeAddComment();
}

function isResponseBlank() {
    var val = document.getElementById("response");
    var cb = document.getElementById("completeButton");
    if (val.value.length == 0) {
        cb.disabled = true;
    } else if (confirmed && nonDay1 && inactiveCashierList.length == 0) {
        cb.disabled = false;
    }
}

function completeWorkItem() {
    var formDcrId = document.getElementById("dcrIdComplete");
    var formStepProcResponse = document.getElementById("stepProcResponse");
    var formFromSearch = document.getElementById("formFromSearch");
    var responseDD = document.getElementById("response");
    var completeForm = document.getElementById("spCompleteForm");

    for (noteCnt in noteList) {
        if (noteList[noteCnt].content != null || noteList[noteCnt].content != '') {
            var hiddenRowNum = document.createElement("input");
            hiddenRowNum.type = 'hidden';
            hiddenRowNum.name = 'noteList[' + noteCnt + '].rowNum';
            hiddenRowNum.value = noteList[noteCnt].rowNum;

            var hiddenNoteType = document.createElement("input");
            hiddenNoteType.type = 'hidden';
            hiddenNoteType.name = 'noteList[' + noteCnt + '].noteType';
            hiddenNoteType.value = noteList[noteCnt].type;

            var hiddenNoteContent = document.createElement("input");
            hiddenNoteContent.type = 'hidden';
            hiddenNoteContent.name = 'noteList[' + noteCnt + '].noteContent';
            hiddenNoteContent.value = noteList[noteCnt].content;

            completeForm.appendChild(hiddenRowNum);
            completeForm.appendChild(hiddenNoteType);
            completeForm.appendChild(hiddenNoteContent);
        }
    }

    formDcrId.value = dcrId;
    formStepProcResponse.value = responseDD.value;
    formFromSearch.value = false;

    completeForm.submit();
}

function showNoteForm(val) {
    document.getElementById("spNoteForm").reset();
    var isContentToBeEdited = false;
    var noteCntToDeleteIfEdited;
    for (noteCnt in noteList) {
        if (noteList[noteCnt].rowNum == val.rowInd && noteList[noteCnt].type == val.noteType) {
            if (val.content != null || val.content != '') {
                isContentToBeEdited = true;
                document.getElementById("spNoteContent").value = noteList[noteCnt].content;
                noteCntToDeleteIfEdited = noteCnt;
            } else {
                document.getElementById("spNoteContent").value = '';
            }
        }
    }

    $(function() {
        $("#spNoteDiv").dialog({
            autoOpen: true,
            height: "auto",
            minHeight: 580,
            width: 540,
            modal: true,
            closeOnEscape: true,
            draggable: false,
            resizable: false,
            dialogClass: "noteFormClass",
            title: "Add/View Note",
            buttons: [
                {
                    id: 'addSPNoteBtn',
                    text: 'Add',
                    click: function() {
                        var contentEntered = document.getElementById("spNoteContent").value;
                        var currentLink = document.getElementById("noteId" + val.rowInd + val.noteType);

                        if (contentEntered == null || $.trim(contentEntered) == '') {
                            alert('Remarks area needs to be filled');
                        } else {
                            if (isContentToBeEdited) {
                                delete noteList[noteCntToDeleteIfEdited];
                            }
                            var inputNote = {};
                            inputNote.rowNum = val.rowInd;
                            inputNote.type = val.noteType;
                            inputNote.content = contentEntered;
                            noteList.push(inputNote);
                            currentLink.innerHTML = 'View';
                            $(this).dialog('close');
                        }
                    }
                },
                {
                    id: 'cancelSPNoteBtn',
                    text: 'Cancel',
                    click: function() {
                        $(this).dialog('close');
                    }
                }
            ]
        }).dialog('widget').position({
            my: 'center',
            at: 'center',
            of: $(document.getElementById("mainTemplateBodyId"))
        });
    });

    $('textarea[maxlength]').limitMaxlength({
        onEdit: onEditCallback,
        onLimit: onLimitCallback
    });

    document.getElementById("addSPNoteBtn").value = 'Add';
    if (isContentToBeEdited) {
        document.getElementById("addSPNoteBtn").value = 'Edit';
    }
    document.getElementById("cancelSPNoteBtn").value = 'Cancel';
    
    // Added for MR-WF-16-00230 - Finance view on CCQA/PPA notes
    if ("Reconciled" === dcrStatus) {
        $("#addSPNoteBtn").hide();
    }
}

var callSaveStepProcData = function saveStepProcData() {
    var hiddenForm = document.getElementById("hiddenForm");
    for (noteCnt in noteList) {
        if (noteList[noteCnt].content != null || noteList[noteCnt].content != '') {

            var hiddenRowNum = document.createElement("input");
            hiddenRowNum.type = 'hidden';
            hiddenRowNum.name = 'noteList[' + noteCnt + '].rowNum';
            hiddenRowNum.value = noteList[noteCnt].rowNum;

            var hiddenNoteType = document.createElement("input");
            hiddenNoteType.type = 'hidden';
            hiddenNoteType.name = 'noteList[' + noteCnt + '].noteType';
            hiddenNoteType.value = noteList[noteCnt].type;

            var hiddenNoteContent = document.createElement("input");
            hiddenNoteContent.type = 'hidden';
            hiddenNoteContent.name = 'noteList[' + noteCnt + '].noteContent';
            hiddenNoteContent.value = noteList[noteCnt].content;

            hiddenForm.appendChild(hiddenRowNum);
            hiddenForm.appendChild(hiddenNoteType);
            hiddenForm.appendChild(hiddenNoteContent);
        }
    }
    hiddenForm.submit();
}

function getAttachments(cashierId) {
    var returnAttachments = [];
    for (a in attachments) {
        if (attachments[a].cashierId == cashierId) {
            var dummyAttachment = {};
            dummyAttachment.name = "<a target='_blank' href='../" + attachments[a].fileUrl + "'>" + attachments[a].filename + "</a>";
            dummyAttachment.desc = attachments[a].description;
            dummyAttachment.uploadDate = attachments[a].uploadDate;
            returnAttachments.push(dummyAttachment);
        }
    }
    return returnAttachments;
}

var emailWindow = null;

var tempEmailWindow = null;

function closeEmailWindow() {
    if (emailWindow != null || emailWindow != tempEmailWindow) {
        emailWindow.close();
    }
}

function openEmail() {
    closeEmailWindow();
    emailWindow = window.open("../email/create.html?dcrId=" + dcrId + "&fromErrorTag=false", "_blank", "width=600,height=500,resizable=yes,status=yes,scrollbars=yes");
    tempEmailWindow = emailWindow;
}

var attachFileWindow = null;

var tempAttachFileWindow = null;

function closeAttachFileWindow() {
    if (attachFileWindow != null || attachFileWindow != tempAttachFileWindow) {
        attachFileWindow.close();
    }
}

function openAttachFile() {
    closeAttachFileWindow();
    attachFileWindow = window.open("../attachSP/attach.html?reloadSP=false&dcrId=" + dcrId, "_blank", "width=600,height=500,resizable=yes,status=yes,scrollbars=yes");
    tempAttachFileWindow = attachFileWindow;
}

function generateInactiveCashierSection() {

    $(document).ready(function() {
        var icListLayout = [
            {name: "Cashier ID", field: "id", width: "50%"},
            {name: "Cashier Name", field: "name", width: "50%"}
        ];

        new TableContainer({
            divName: "icList",
            nodeName: "icListNode",
            width: "1000px",
            id: "icTable",
            layout: icListLayout,
            tableClass: "icHeaderClass",
            data: inactiveCashierList
        }).startUp();

        var icListDiv = document.getElementById("icList");

        icListDiv.style.display = "none";
    });

}

function isSLOCPIConfirmed(cashierId) {
    var sl = 0;
    for (sl in slocpiConfirm) {
        if (slocpiConfirm[sl].cashierId == cashierId) {
            return slocpiConfirm[sl].confirmed;
        }
    }
}

function isSLGFIConfirmed(cashierId) {
    var sl = 0;
    for (sl in slgfiConfirm) {
        if (slgfiConfirm[sl].cashierId == cashierId) {
            return slgfiConfirm[sl].confirmed;
        }
    }
}

function isSLAMCIPConfirmed(cashierId) {
    var sl = 0;
    for (sl in slamcipConfirm) {
        if (slamcipConfirm[sl].cashierId == cashierId) {
            return slamcipConfirm[sl].confirmed;
        }
    }
}

function isSLAMCIDConfirmed(cashierId) {
    var sl = 0;
    for (sl in slamcidConfirm) {
        if (slamcidConfirm[sl].cashierId == cashierId) {
            return slamcidConfirm[sl].confirmed;
        }
    }
}

function isSLFPIConfirmed(cashierId) {
    var sl = 0;
    for (sl in slfpiConfirm) {
        if (slfpiConfirm[sl].cashierId == cashierId) {
            return slfpiConfirm[sl].confirmed;
        }
    }
}

function isOTHERConfirmed(cashierId) {
    var sl = 0;
    for (sl in otherConfirm) {
        if (otherConfirm[sl].cashierId == cashierId) {
            return otherConfirm[sl].confirmed;
        }
    }
}

var wfHistoryWindow = null;

var tempWFHistoryWindow = null;

function closeWFHistoryWindow() {
    if (wfHistoryWindow != null || wfHistoryWindow != tempWFHistoryWindow) {
        wfHistoryWindow.close();
    }
}

function openWFHistory() {
    closeAttachFileWindow();
    wfHistoryWindow = window.open("../wfHistory/index.html?dcrId=" + dcrId, "_blank", "width=1000,height=500,resizable=yes,status=yes,scrollbars=yes");
    tempWFHistoryWindow = wfHistoryWindow;
}

var addNoteWindow = null;

var tempAddNoteWindow = null;

function closeAddNoteWindow() {
    if (addNoteWindow != null || addNoteWindow != tempAddNoteWindow) {
        addNoteWindow.close();
    }
}

function openAddNote() {
    closeAddNoteWindow();
    addNoteWindow = window.open("../addNoteSP/create.html?reloadSP=false&dcrId=" + dcrId, "_blank", "width=650,height=550,resizable=yes,status=yes,scrollbars=yes");
    tempAddNoteWindow = addNoteWindow;
}

var candidateMatchWindow = null;

var tempCandidateMatchWindow = null;

function closeCandidateMatchWindow() {
    if (candidateMatchWindow != null || candidateMatchWindow != tempCandidateMatchWindow) {
        candidateMatchWindow.close();
    }
}

function openCandidateMatch() {
    closeCandidateMatchWindow();
    candidateMatchWindow = window.open("../candidateMatchSP/index.html?dcrDate=" + formattedDcrDateStr + "&ccId=" + ccId + "&ccName=" + ccName + "&dcrDateStr=" + collectionsDate + "&dcrId=" + dcrId, "_blank", "width=650,height=550,resizable=yes,status=yes,scrollbars=yes");
    tempCandidateMatchWindow = candidateMatchWindow;
}

//added for MR-WF-16-00037
var mdsWindow = null;
var tempMDSWindow = null;

function closeMDSWindow(){
	if(mdsWindow != null || mdsWindow != tempMDSWindow){
		mdsWindow.close();
	}
}

function openMDS(){
	closeMDSWindow();
	mdsWindow = window.open("../ppamds/index.html?ccId="+ccId+"&dcrDateStr="+dcrDateStr+"&dcrId="+dcrId+"&isPpaQa="+isPpaQa, "_blank", "width=1080,height=550,top=100,left=100,resizable=yes,status=yes,scrollbars=yes");
	tempMDSWindow = mdsWindow;
}

var errorTagWindow = null;
var tempErrorTagWindow = null;

function closeErrorTag() {
    if (errorTagWindow != null || errorTagWindow != tempErrorTagWindow) {
        errorTagWindow.close();
    }
}
function openErrorTag() {
    closeErrorTag();

    //build and pass comma-separated values of cashier ids from cashierList
    var concatCashierNameList = '';
    for (cashier in cashierList) {
        concatCashierNameList = concatCashierNameList + cashierList[cashier].id + '~';
    }
    if (concatCashierNameList != '') {
        concatCashierNameList = concatCashierNameList.substring(0, concatCashierNameList.length - 1);//clip last '~' char
    }
    errorTagWindow = window.open("../errorTag/index.html?dcrId=" + dcrId + "&dcrStatus=" + dcrStatus + "&concatCashierNameList=" + concatCashierNameList + "&dcrDate=" + formattedDcrDateStr + "&ccName=" + ccName, "_blank", "width=820,height=550,resizable=yes,status=yes,scrollbars=yes");
    tempErrorTagWindow = errorTagWindow;
}

function generateSnapshotSection(snapshotList) {
    var returnSection = [];
    if (snapshotList.length > 0) {
        returnSection.push({
            product: "<div class='alignLeft'>Collection Details</div>"
        });
    }
    for (x in snapshotList) {
        var tempRow = {};
        tempRow.cashier = "<a target='_blank' href='../" + snapshotList[x].fileUrl + "'>" + snapshotList[x].filename + "</a>";
        tempRow.product = snapshotList[x].timestamp;
        returnSection.push(tempRow);
    }
    return returnSection;
}

function reloadMainWindow() {
    window.close();
    window.opener.location.reload();
}

function getShortName(key) {
    for (x in userIdShortname) {
        if (key == userIdShortname[x].key) {
            return userIdShortname[x].shortName;
        }
    }
    return "";
}

var excludeFromCashierTotal = ["totalNewBusinessIndividual", "totalNewBusinessVariable", "totalIndividualRenewal", "totalVariableRenewal", "totalNonCashFromDTR", "totalNonPolicy", "totalWorksiteNonPosted", "sessionTotal", "reversalProduct"];
