function loadEditValuesIntoLookup(){
	//FOR TESTING, sa onEdit/onDelete ito
	alert('You are about to edit the error log');
	var radioLogGroup = document.getElementsByName('radioLog');//chkboxErr
    for (var i=0; i<radioLogGroup.length; i++)  {
        if(radioLogGroup[i].type=='radio' && radioLogGroup[i].checked){
        	//dapat ISA lang makuha
        	//get id, add to form, pass to edit/delete
        	//alert('from loop: one time pass lang dapat');
        	//alert();//dcrId_stepId
        	//alert(radioLogGroup[i].value);//dcrId
        	//alert(radioLogGroup[i].stepId);
        	//alert('load this obj above');
        	//radioLogGroup[i]
        	for(existing in errLogData){
        		if(errLogData[existing].id == radioLogGroup[i].id){
        			//NEW
        			document.getElementById("logIdentity").value = errLogData[existing].identity;
        			
        			//alert('from loop: one time pass lang dapat ULET');
        			//use this object's props to fill lookup div
        			var stepIdOfSelectedLog = errLogData[existing].stepId;
        			var stepNameOfSelectedLog = errLogData[existing].stepName;
        			var userNameOfSelectedLog = errLogData[existing].userName;
        			var reason = errLogData[existing].reason;
        			//alert('reason sa load edit2lookup: '+reason);
        			//create and set option, for the selected value and text inside the select
        			//NOTE: TO TEST
        			/*var stepNameSelectBox = document.getElementById("stepNameSelect");
        			var option = document.createElement("option");
        			option.value = stepIdOfSelectedLog;
        			option.text = stepNameOfSelectedLog;
        			option.user = userNameOfSelectedLog;
        			option.selected = "selected";//dapat gumana to
        			stepNameSelectBox.options.add(option);
        			stepNameSelectBox.disabled = true;*/
        			
        			//code for 2 select box
        			//var userNameSelectedOption = document.getElementById("userNameSelected");
        			//userNameSelectedOption.value = stepIdOfSelectedLog;
        			//userNameSelectedOption.text = userNameOfSelectedLog;
        			
        			//code for 1 select box
        			var userNameSelectBox = document.getElementById("userNameSelect");
        			//userNameSelectBox.disabled = true;
        			for(var i = 0; i < userNameSelectBox.options.length; i++){
        				if(userNameSelectBox.options[i].value == userNameOfSelectedLog){
        					userNameSelectBox.options[i].selected = true;             
        				}
        			}
        			
        			var reasonTextField = document.getElementById("reasonField");
        			reasonTextField.value = reason;
        			//reasonTextField.disabled = true;
        			
        			generateCheckboxesFromEdit(stepIdOfSelectedLog, errLogData[existing].errorList);
        			//note: the whole obj is passed
        			//note: obj.errorList is another array object
        			//for(er in erList){
        			//erList[er].errorId
        			//erList[er].errorName
        		}
        	}
        }
     }
    document.getElementById("addButton").disabled = true;
	document.getElementById("saveButton").disabled = false;
	document.getElementById("deleteButton").disabled = true;
}

function generateCheckboxesFromEdit(stepId, taggedErrorsOfSelectedLog){
	var errorListDiv = document.getElementById("errorListDiv");
	//reload div
	while (errorListDiv.hasChildNodes()) {
		errorListDiv.removeChild(errorListDiv.lastChild);
	}
	//available errors for checking
	for(err in errListPerStep){
		if(errListPerStep[err].stepId==stepId){//only display errors associated with selected step
			var errorId = errListPerStep[err].errorId;
			var errorName = errListPerStep[err].errorName;
			
			var br = document.createElement("br");
			var chkbox = document.createElement("input");
			chkbox.type = "checkbox";
			//chkbox.value =
			chkbox.id = errorId;
			chkbox.name = "chkboxErr";
			chkbox.errname = errorName;
			chkbox.onclick = function() {callAddRemoveTaggedErrorList(this)};

			var chkboxLabel = document.createElement("label");
			chkboxLabel.htmlFor = errorId;
			chkboxLabel.appendChild(document.createTextNode(errorName));
			
			errorListDiv.appendChild(chkbox);
			errorListDiv.appendChild(chkboxLabel);
			errorListDiv.appendChild(br);
			
			//automatically check those errs on the selected log
			for(er in taggedErrorsOfSelectedLog){
				//alert(taggedErrorsOfSelectedLog[er].errorId);
				if(taggedErrorsOfSelectedLog[er].errorId==errorId){
					document.getElementById(errorId).checked = true;
					//chkbox.checked=true;
					//$('#'+errorId).trigger('click');//trigger onclick event jquery
					var e = {};//OR, emulate adding
					e.errorId = errorId;
					e.errorName = errorName;
					taggedErrList.push(e);
				}
			}
		}
	}
}

	function displayNameAndLookup(obj){
		//for display
		var labelText = obj.options[obj.selectedIndex].text;
		
		var stepId = obj.options[obj.selectedIndex].value;
		var userName = obj.options[obj.selectedIndex].user;
		if(labelText=="-------------------------------"){
			stepId = "";
			userName = "-------------------------------";
		}
		var userNameSelectedOption = document.getElementById("userNameSelected");
		userNameSelectedOption.value = stepId;
		userNameSelectedOption.text = userName;
		
		//var userNameSelect = document.getElementById("userNameSelect");
		//userNameSelect.options[userNameSelect.selectedIndex].value = obj.user;
		//userNameSelect.options[userNameSelect.selectedIndex].text = obj.user;
		//or
		//userNameSelect.options[userNameSelect.options.length] = new Option(obj.user, obj.user);
		//or
		//var option = document.createElement("option")
		//option.innerText = obj.user;
		//option.Value = obj.user;
		//userNameSelect.options.add(anOption);
		
		generateCheckboxes(stepId);
		document.getElementById("addButton").disabled = false;
		document.getElementById("saveButton").disabled = true;
		//FOR PASSING, sa onAdd ito, but first get TheSelect.options[TheSelect.selectedIndex].attributeName
		//var stepId = obj.value;
		//if((obj.selectedIndex != -1){
		//var stepName = obj.options[obj.selectedIndex].text;
		//}else{
			//alert('Please select a step name');
		//}
		//var userName = obj.user;
		//for(e in taggedErrList){//cr8 hidden}
		//document.getElementById("reasonField").value()
	}
	
	function generateCheckboxes(stepId){
		var errorListDiv = document.getElementById("errorListDiv");
		//reload div
		while (errorListDiv.hasChildNodes()) {
			errorListDiv.removeChild(errorListDiv.lastChild);
		}
		for(err in errListPerStep){
			if(errListPerStep[err].stepId==stepId){//only display errors associated with selected step
				var errorId = errListPerStep[err].errorId;
				var errorName = errListPerStep[err].errorName;
				
				var br = document.createElement("br");
				var chkbox = document.createElement("input");
				chkbox.type = "checkbox";
				//chkbox.value =
				chkbox.id = errorId;
				chkbox.name = "chkboxErr";
				chkbox.errname = errorName;
				chkbox.onclick = function() {callAddRemoveTaggedErrorList(this)};
				
				var chkboxLabel = document.createElement("label");
				chkboxLabel.htmlFor = errorId;
				chkboxLabel.appendChild(document.createTextNode(errorName));
				
				errorListDiv.appendChild(chkbox);
				errorListDiv.appendChild(chkboxLabel);
				errorListDiv.appendChild(br);
			}
		}
	}