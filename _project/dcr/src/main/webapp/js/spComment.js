var spCommentList = [];
var spCoupleCommentList = [];
var commentCtr = 0;
$(document).ready(function(){

var commentLayout = [ {
	name : "User",
	field : "acf2id",
	width : "10%"
},{
	name : "Date and Time",
	field : "datePostedFormatted",
	width : "20%"
},{
	name : "Remarks",
	field : "remarks",
	width : "70%"
}];

new TableContainer({
	divName : "commentDiv",
	nodeName : "commentNode",
	width : "100%",
	id : "commentTable",
	layout : commentLayout,
	tableClass: "commentTableClass",
	data: spCoupleCommentList
}).startUp();
});