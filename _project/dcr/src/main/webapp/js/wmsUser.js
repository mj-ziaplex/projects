function setWMSUsers(wmsUserData) {
    $(document).ready(function() {
        destroyViewTable();

        var wmsUserLabelLayout = [{name: "WMS Users", field: "wmsUserLabel", width: "100%"}];

        new TableContainer({
            divName: "wmsUserDiv",
            nodeName: "wmsUserLabelNode",
            width: "100%",
            id: "wmsUserLabel",
            layout: wmsUserLabelLayout,
            tableClass: "wmsUserLabelBorderedTable"
        }).startUp();

        var wmsUserTableLayout = [
            {name: "", field: "userIdChkbox", width: "2%"},
            {name: "User ID", field: "userId", width: "10%"},
            {name: "User Name", field: "userName", width: "20%"},
            {name: "Created by User", field: "createdUserId", width: "15%"},
            {name: "Created Time", field: "createdUserTime", width: "15%"},
            {name: "Updated by User", field: "updatedUserId", width: "15%"},
            {name: "Update Time", field: "updatedUserTime", width: "15%"},
            {name: "Active", field: "active", width: "5%"},
            {name: "Action", field: "action", width: "4%"}];

        new TableContainer({
            divName: "wmsUserTableNode",
            nodeName: "wmsUserNode",
            width: "100%",
            id: "wmsUserTable",
            layout: wmsUserTableLayout,
            tableClass: "wmsUserBorderedTable"
        }).startUp();

        // Set table content - list of users to obj
        var wmsUserTable = _globalTableContainer["wmsUserTable"];
        wmsUserTable.setData(wmsUserData);
        wmsUserTable.refreshTable();

        // Set footer and users to table
        var wmsUser = document.getElementById("wmsUserTable");
        var options = {
            optionsForRows: [5, 10, 20, 30, 50],
            rowsPerPage: 20,
            firstArrow: (new Image()).src = "../images/firstBlue.gif",
            prevArrow: (new Image()).src = "../images/prevBlue.gif",
            lastArrow: (new Image()).src = "../images/lastBlue.gif",
            nextArrow: (new Image()).src = "../images/nextBlue.gif",
            topNav: false
        };
        $('#wmsUserTable').tablePagination(options);
        sorttable.makeSortable(wmsUser);
        autoSort(wmsUser);
    });
}

function autoSort(wmsUserTable) {
    $(document).ready(function() {
        var headers = wmsUserTable.getElementsByTagName("th");
        for (var h = 0; h < headers.length; h++) {
            if (headers[h].innerHTML === "User ID") {
                headers[h].click();
            }
        }
        resetOddEvenRows(wmsUserTable);
    });
}

function onDoSubmit(action) {
    $("#adminWmsUserForm").attr("action", action);
    $("#adminWmsUserForm").submit();
}

function destroyViewTable() {
    $("#wmsUserLabelNode").empty();
}

var userRemoveList = [];

function handleRemoveToggle(cb) {
    alert("REMOVE TOGGLE");
    if (cb.checked) {
        userRemoveList.push(cb.rowInd);
    } else {
        var ctr = 0;
        for (user in userRemoveList) {
            if (userRemoveList[user] == cb.rowInd) {
                userRemoveList.splice(ctr, 1);
            }
            ctr++;
        }

    }
}

function doDeleteUser(action) {
    if (userRemoveList.length > 0) {
        var conf = confirm('Do you want to continue?');
        if (conf) {
            document.getElementById("userIds").value = userRemoveList;
            $("#adminUserOperation").attr("action", action);
            $("#adminUserOperation").submit();
        }
    } else {
        alert('No user selected.');
    }
}

function submitSearchForm() {
    $("#searchUserForm").submit();
}

function ajaxCall(hrefObj) {
    $(document).ready(function() {
        $.ajax({
            url: $(hrefObj).attr('editAction'),
            type: "POST",
            data: {
                userId: $(hrefObj).attr('acf2id')
            },
            //dataType : "json",
            success: function(data) {
                $("#addWmsUserNode").html(data);
                //                $( "<h1>" ).text( json.title ).appendTo( "body" );
                //                $( "<div class=\"content\">").html( json.html ).appendTo( "body" );
            },
            error: function(xhr, status, errorThrown) {
                alert("Sorry, there was a problem!");
                alert("Error: " + errorThrown);
                alert("Status: " + status);
                alert(xhr);
            },
            complete: function(xhr, status) {
                alert("The request is complete!");
            }
        });
    }); // End ready function   
}
