var searchTableData = [];

var showTable = false;

var itemsFound = 0;

function clearResults(){
	window.location.href = "../search/index.html";
}

function clearStartDate(){
	var searchDatePicker = document.getElementById("searchDatePickerStart");
	var dcrDate = document.getElementById("dcrStartDate");
	dcrDate.value = "";
	searchDatePicker.value = "";
}

function clearEndDate(){
	var searchDatePicker = document.getElementById("searchDatePickerEnd");
	var dcrDate = document.getElementById("dcrEndDate");
	dcrDate.value = "";
	searchDatePicker.value = "";
}


function disableHubId(){
	var hubId = document.getElementById("hubId");
	var searchByHub = document.getElementById("searchByHub");
	searchByHub.value = "false";
	hubId.disabled = true;
	var ccId = document.getElementById("ccId");
	var searchByCC = document.getElementById("searchByCC");
	searchByCC.value = "true";
	ccId.disabled = false;
}

function disableCcId(){
	var ccId = document.getElementById("ccId");
	var searchByCC = document.getElementById("searchByCC");
	searchByCC.value = "false";
	ccId.disabled = true;
	var hubId = document.getElementById("hubId");
	var searchByHub = document.getElementById("searchByHub");
	searchByHub.value = "true";
	hubId.disabled = false;
}

function showResultsTable(searchData){
	
	$(document).ready(function(){
		
		destroySearchTable();
		
		var searchTableLayout = [{
			name : "Customer Center Name",
			field : "ccName",
			width : "25%"
		},{
			name : "DCR Date",
			field : "dcrDate",
			width : "19%"
		},{
			name : "Submitted By",
			field : "submittedBy",
			width : "19%"
		},{
			name : "Date Submitted",
			field : "dateSubmitted",
			width : "19%"
		},{
			name : "Process Status",
			field : "processStatus",
			width : "18%"
		}];
		
		new TableContainer({
			divName : "searchResultDiv",
			nodeName : "searchResultTable",
			width : "100%",
			id : "searchResults",
			layout : searchTableLayout,
			tableClass : "searchResultsTable",
			data: searchData
		}).startUp();
		
		var searchTable = document.getElementById("searchResults");
		
		var options = {
	              optionsForRows : [5,10,20,30,50],
	              rowsPerPage : 10,
	              firstArrow : (new Image()).src="../images/firstBlue.gif",
	              prevArrow : (new Image()).src="../images/prevBlue.gif",
	              lastArrow : (new Image()).src="../images/lastBlue.gif",
	              nextArrow : (new Image()).src="../images/nextBlue.gif",
	              topNav : false
	            }
		sorttable.makeSortable(searchTable);
		$('#searchResults').tablePagination(options);
		
	});
	
}

function destroySearchTable(){
	$("#searchResultTable").empty();
}

function showSearchResultDiv(){
	var searchResultDiv = document.getElementById("searchResultDiv");
	searchResultDiv.style.visibility = 'visible';
}

function submitForm(){
	var searchForm = document.getElementById("searchForm");
	if(validateForm()){		
		searchForm.submit();
	}
}

function validateForm(){
	var validated = true;
	var ccId = document.getElementById("ccId");
	var hubId = document.getElementById("hubId");
	var rbSiteCode = document.getElementById("rbSiteCode");
	var rbHub = document.getElementById("rbHub");
	var dcrDate = document.getElementById("dcrDateStr");
	if(rbSiteCode.checked == true){
		if(ccId.selectedIndex < 0){
			alert("Please choose a site code");
			return false;
		}
	} else if(rbHub.checked == true){
		if(hubId.selectedIndex < 0){
			alert("Please choose a hub");
			return false;
		}
	}
//	if(dcrDate.value == null || dcrDate.value.length == 0){
//		dcrDate.value = "withinMonth";
//	}
	return validated;
	
}

function setDcrStartDate(val){
	var dcrStartDate = document.getElementById("dcrStartDate");
	dcrStartDate.value = val;
}

function setDcrEndDate(val){
	var dcrEndDate = document.getElementById("dcrEndDate");
	dcrEndDate.value = val;
}



function autoSort(){
	$(document).ready(function(){
	var resultTable = document.getElementById("searchResults");
	var headers = resultTable.getElementsByTagName("th");
	for(var h = 0; h < headers.length; h++){
		if(headers[h].innerHTML == "DCR Date"){
			headers[h].click();
		}
	}
	resetOddEvenRows(resultTable);
	});
}