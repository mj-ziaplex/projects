var slocpiTableLayout = [ {
	name : " ",
	field : "product",
	width : "25%"
}];

var slgfiTableLayout = [ {
	name : " ",
	field : "product",
	width : "25%"
}];

var slocpiTableBlankRow = [];

var slocpiCollectionSummaryArea = [];
var slocpiNonPostedFromDTRArea = [];
var slocpiForManualPostingArea = [];
var slocpiExemptionsManualInputArea = [];

var blankRow = {product: "&nbsp;"};

var collectionSummaryHeader = {product: alignLeftCell("<b>COLLECTION SUMMARY</b>")};
var totalCashCounter = {product: alignLeftCell("&nbsp;&nbsp;&nbsp;Cash - Counter")};
var totalCashEpsBpi = {product: alignLeftCell("&nbsp;&nbsp;&nbsp;EPS - BPI")};
var totalCashNonCounter = {product: alignLeftCell("&nbsp;&nbsp;&nbsp;Cash - Non Counter")};
var totalCheckOnUs = {product: alignLeftCell("&nbsp;&nbsp;&nbsp;Cheque - On us")};
var totalCheckLocal = {product: alignLeftCell("&nbsp;&nbsp;&nbsp;Cheque - Local")};
var totalCheckRegional = {product: alignLeftCell("&nbsp;&nbsp;&nbsp;Cheque - Regional")};
var totalCheckNonCounter = {product: alignLeftCell("&nbsp;&nbsp;&nbsp;Cheque - Non Counter")};
var totalCheckOT = {product: alignLeftCell("&nbsp;&nbsp;&nbsp;Cheque - OT")};
var totalPmo = {product: alignLeftCell("&nbsp;&nbsp;&nbsp;Postal Money Order")};
var totalCreditMemo = {product: alignLeftCell("&nbsp;&nbsp;&nbsp;Total Credit Memo")};
var totalUsCheckInManila = {product: alignLeftCell("&nbsp;&nbsp;&nbsp;US Check Drawn In Manila")};
var totalUsCheckOutPh = {product: alignLeftCell("&nbsp;&nbsp;&nbsp;US Check Drawn Outside PH")};
var totalDollarCheque = {product: alignLeftCell("&nbsp;&nbsp;&nbsp;Dollar Cheque")};


var cardTypes = {product: alignLeftCell("&nbsp;&nbsp;&nbsp;Card Types")};
var totalPosBpi = {product: alignLeftCell("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;POS - BPI")};
var totalPosCtb = {product: alignLeftCell("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;POS - CITIBANK")};
var totalPosHsbc = {product: alignLeftCell("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;POS - HSBC")};
var totalPosScb = {product: alignLeftCell("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;POS - SCB")};
var totalPosRcbc = {product: alignLeftCell("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;POS - RCBC")};
var totalPosBdo = {product: alignLeftCell("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;POS - BDO")};
var totalMdsBpi = {product: alignLeftCell("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MDS - BPI")};
var totalMdsCtb = {product: alignLeftCell("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MDS - CITIBANK")};
var totalMdsHsbc = {product: alignLeftCell("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MDS - HSBC")};
var totalMdsSbc = {product: alignLeftCell("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MDS - SCB")};
var totalMdsRcbc = {product: alignLeftCell("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MDS - RCBC")};
var totalMdsBdo = {product: alignLeftCell("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MDS - BDO")};

var totalAutoCtb = {product: alignLeftCell("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CITIBANK(AUTOCHARGE)")};
var totalAutoScb = {product: alignLeftCell("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SCB(AUTOCHARGE)")};
var totalAutoRcbc = {product: alignLeftCell("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;RCBC(AUTOCHARGE)")};
var totalSunlinkOnline = {product: alignLeftCell("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Sunlink Online")};
var totalSunlinkHsbcDollar = {product: alignLeftCell("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Sunlink HSBC Dollar")};
var totalSunlinkHsbcPeso = {product: alignLeftCell("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Sunlink HSBC Peso")};
var totalEpsBpi = {product: alignLeftCell("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EPS BPI")};

var totalNonCash = {product: alignLeftCell("&nbsp;&nbsp;&nbsp;Non - Cash")};
var totalCollectionSummary = {product: alignLeftCell("&nbsp;&nbsp;&nbsp;<b>Total Collection</b>")};

var nonPostedFromDTRHeader = {product: alignLeftCell("<b>NON-POSTED (from DTR)</b>")};
var totalNewBusinessIndividual = {product: alignLeftCell("&nbsp;&nbsp;&nbsp;New Business Individual")};
var totalNewBusinessVariable = {product: alignLeftCell("&nbsp;&nbsp;&nbsp;New Business Variable")};
var totalVariableRenewal = {product: alignLeftCell("&nbsp;&nbsp;&nbsp;Variable Renewal")};
var totalIndividualRenewal = {product: alignLeftCell("&nbsp;&nbsp;&nbsp;Individual Renewal")};
var totalNonCashFromDTR = {product: alignLeftCell("&nbsp;&nbsp;&nbsp;Non - Cash")};
var totalNonPostedFromDTR = {product: alignLeftCell("&nbsp;&nbsp;&nbsp;<b>Total</b>")};

var forManualPostingHeader = {product: alignLeftCell("<b>FOR MANUAL POSTING</b>")};
var totalNonPolicy = {product: alignLeftCell("&nbsp;&nbsp;&nbsp;Non-Policy")};
var totalWorksiteNonPosted = {product: alignLeftCell("&nbsp;&nbsp;&nbsp;Worksite (not posted)")};
var totalForManualPosting = {product: alignLeftCell("&nbsp;&nbsp;&nbsp;<b>Total</b>")};

var exemptionsHeader = {product: alignLeftCell("<b>EXEMPTIONS(Manually Inputted)</b>")};
var exemptionsReversal = {product: alignLeftCell("&nbsp;&nbsp;&nbsp;Reversals/Unposted")};
var exemptionsWorksite = {product: alignLeftCell("&nbsp;&nbsp;&nbsp;Worksite")};
var totalExemptions = {product: alignLeftCell("&nbsp;&nbsp;&nbsp;<b>Total</b>")};

function loadSlocpiTableNodeData(company,showSaveButton,isConfirmed){
	$(document).ready(function(){
	
		var tableLayout = slocpiTableLayout;
		if(company=='SLGFI'){
			tableLayout = slgfiTableLayout;
		} 
		
		new TableContainer({
			divName : "slocpiTableDiv",
			nodeName : "slocpiTableNode",
			width : "100%",
			id : "slocpiTable",
			layout : tableLayout,
			tableClass : "borderedTable"
		}).startUp();
		
		var slocpiTableData = [];
		
		var slocpiTable = _globalTableContainer["slocpiTable"];
		
		//appendJsons([slocpiTableData,slocpiTableBlankRow,slocpiData,slocpiTableBlankRow,slocpiExceptions]);
		appendJsons([slocpiTableData,
		             slocpiCollectionSummaryArea,
		             slocpiTableBlankRow,
		             slocpiNonPostedFromDTRArea,
		             slocpiTableBlankRow,
		             slocpiForManualPostingArea,
		             slocpiTableBlankRow,
		             slocpiExemptionsManualInputArea,
		             slocpiTableBlankRow]);
		
		slocpiTable.setData(slocpiTableData);
		slocpiTable.refreshTable();
		
		var confirmButtonDiv = document.getElementById("confirmButtonDiv");
		
		if(showSaveButton){
			var saveButton = document.createElement("input");
			saveButton.type="button";
			saveButton.style.right="0px";
			saveButton.id="saveButton";
			saveButton.value="Save";
			saveButton.onclick = function() { callSubmitForm(company); };
			if(isConfirmed=='true'){
				saveButton.disabled = true;
			}
			confirmButtonDiv.appendChild(saveButton);
		}
		
		var confirmButton = document.createElement("input");
		confirmButton.type="button";
		confirmButton.style.right="0px";
		confirmButton.id="confirmButton";
		confirmButton.value="Confirm";
		confirmButton.onclick = function() { callConfirmForm(company); };
		confirmButtonDiv.appendChild(confirmButton);
	
	});
}

var callConfirmForm = function confirmForm(company) {
	document.getElementById("confirmButton").disabled = true;
	var form = document.getElementById("slocpiForm");
	if(company=='SLGFI'){
		form = document.getElementById("slgfiForm");
	}
	var markup = document.documentElement.innerHTML;
	document.getElementById("htmlCode").value = markup;
	form.submit();	
}

var callSubmitForm = function submitForm(company){
	document.getElementById("saveButton").disabled = true;
	var form = document.getElementById("slocpiForm");
	if(company=='SLGFI'){
		form = document.getElementById("slgfiForm");
	}
	var formActionVal = form.action;
	//alert('old formActionVal per company: ' + formActionVal);
	var methodName = formActionVal.substring(formActionVal.lastIndexOf("/")+1); 
	form.action = formActionVal.replace(methodName,"save.html");
	//alert('new formActionVal: ' + formActionVal);
	
	document.getElementById("id0").value = document.getElementById("tempId0").value;
	document.getElementById("worksiteAmount0").value = document.getElementById("tempWorksiteAmount0").value;
	document.getElementById("id1").value = document.getElementById("tempId1").value;
	document.getElementById("worksiteAmount1").value = document.getElementById("tempWorksiteAmount1").value;
//	document.getElementById("id2").value = document.getElementById("tempId2").value;
//	document.getElementById("worksiteAmount2").value = document.getElementById("tempWorksiteAmount2").value;
	
//	if(company=='SLGFI'){
//		document.getElementById("id3").value = document.getElementById("tempId3").value;
//		document.getElementById("worksiteAmount3").value = document.getElementById("tempWorksiteAmount3").value;
//	}
	
	form.submit();
}

function formatTxtFldValDecimal(obj){
	obj.value = obj.value.replace(/,/g,"");//remove commas
	
	if(!isNaN(obj.value) && obj.value.indexOf(" ") == -1 && obj.value.length > 0){
		obj.value = numberWithCommas(parseFloat(obj.value).toFixed(2));
	} else {
		alert("Not a number");
		obj.value = "0.00";
		obj.focus();
	}
}