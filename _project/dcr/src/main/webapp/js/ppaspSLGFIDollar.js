var slgfiDollarColumns = [{
	name : "",
	field : "product",
	width : "12%"
},{
	name : "",
	field : "cashier",
	width : "17%"
},{
	name : "TRAD/VUL ",
	field : "tradVul",
	width : "6%"
},{
	name : "DS",
	field : "dsTV",
	width : "6%"
},{
	name : "VDS",
	field : "vdsTV",
	width : "6%"
},{
	name : "Unconv Dollar",
	field : "ud",
	width : "6%"
},{
	name : "DS",
	field : "dsUD",
	width : "6%"
},{
	name : "VDS",
	field : "vdsUD",
	width : "6%"
},{
	name : "Group Life",
	field : "gl",
	width : "5%"
},{
	name : "DS",
	field : "dsGL",
	width : "5%"
},{
	name : "VDS",
	field : "vdsGL",
	width : "3%"
},{
	name : "Recon?",
	field : "reconciled",
	width : "3%"
},{
	name : "Recon By",
	field : "reconciledBy",
	width : "3%"
},{
	name : "Recon Date",
	field : "reconciledDate",
	width : "4%"
},{
	name : "CCM NOTES",
	field : "ccmNotes",
	width : "4%"
},{
	name : "CCQA NOTES",
	field : "ppaNotes",
	width : "4%"
},{
	name : "Findings",
	field : "findings",
	width : "4%"
}	
];

var slgfiDollarColumnsNH = [{
	name : "",
	field : "product",
	width : "12%"
},{
	name : "",
	field : "cashier",
	width : "17%"
},{
	name : "",
	field : "tradVul",
	width : "6%"
},{
	name : "",
	field : "dsTV",
	width : "6%"
},{
	name : "",
	field : "vdsTV",
	width : "6%"
},{
	name : "",
	field : "ud",
	width : "6%"
},{
	name : "",
	field : "dsUD",
	width : "6%"
},{
	name : "",
	field : "vdsUD",
	width : "6%"
},{
	name : "",
	field : "gl",
	width : "5%"
},{
	name : "",
	field : "dsGL",
	width : "5%"
},{
	name : "",
	field : "vdsGL",
	width : "3%"
},{
	name : "",
	field : "reconciled",
	width : "3%"
},{
	name : "",
	field : "reconciledBy",
	width : "3%"
},{
	name : "",
	field : "reconciledDate",
	width : "4%"
},{
	name : "",
	field : "ccmNotes",
	width : "4%"
},{
	name : "",
	field : "ppaNotes",
	width : "4%"
},{
	name : "",
	field : "findings",
	width : "4%"
}	
];

function generateSLGFIDollarSection(slgfiDollarTableDivId,slgfiDollarDivDetails,slgfiDollarSummaryDivDetails){
	$(document).ready(function(){
			
	mergeSLGFIDollarCashierProductTypes();
	
	var dollarCashHeader = [{product: "<div class='alignLeft' cdTarget='slgfiDollarCash' onclick='collapseDivision(this)'>DOLLAR CASH</div>",cashier:"", tradVul: "",dsTV: "",vdsTV:"",ud:"",dsUD:"",vdsUD:"",reconciled:"",reconciledBy:"",reconciledDate:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var cashNonCounterData = generateSLGFIDollarSectionData("totalCashNonCounter","slgfiDollarCash");
	
	var cncTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > Non Counter</div>",gl: "<div class='numFormat'>"+cashNonCounterData.cashierTotals.gl+"</div>", tradVul: "<div class='numFormat'>"+cashNonCounterData.cashierTotals.tv+"</div>",dsTV: "",vdsTV:"",ud:"<div class='numFormat'>"+cashNonCounterData.cashierTotals.ud+"</div>",dsUD:"",vdsUD:"",reconciled:"",reconciledBy:"",reconciledDate:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var cashCounterData = generateSLGFIDollarSectionData("totalCashCounter","slgfiDollarCash");
	
	var ccTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > Counter</div>",gl: "<div class='numFormat'>"+cashCounterData.cashierTotals.gl+"</div>", tradVul: "<div class='numFormat'>"+cashCounterData.cashierTotals.tv+"</div>",dsTV: "",vdsTV:"",ud:"<div class='numFormat'>"+cashCounterData.cashierTotals.ud+"</div>",dsUD:"",vdsUD:"",reconciled:"",reconciledBy:"",reconciledDate:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var dollarCashTotals = getSLGFIDollarAreaTotal([cashNonCounterData,cashCounterData]);
	
	var areaCashTotal = [{product: "<div class='totalRow'>&nbsp;</div>",gl: "<div class='totalRow'><div class='numFormat'>"+dollarCashTotals.gl+"</div></div>",dsGL:"<div class='totalRow'>&nbsp;</div>",vdsGL:"<div class='totalRow'>&nbsp;</div>",cashier:"<div class='totalRow'><div class='alignRight'>TOTAL CASH</div></div>", tradVul: "<div class='totalRow'><div class='numFormat'>"+dollarCashTotals.tv+"</div></div>",dsTV: "<div class='totalRow'>&nbsp;</div>",vdsTV:"<div class='totalRow'>&nbsp;</div>",ud:"<div class='totalRow'><div class='numFormat'>"+dollarCashTotals.ud+"</div></div>",dsUD:"<div class='totalRow'>&nbsp;</div>",vdsUD:"<div class='totalRow'>&nbsp;</div>",reconciled:"<div class='totalRow'>&nbsp;</div>",reconciledBy:"<div class='totalRow'>&nbsp;</div>",reconciledDate:"<div class='totalRow'>&nbsp;</div>",ccmNotes:"<div class='totalRow'>&nbsp;</div>",ppaNotes:"<div class='totalRow'>&nbsp;</div>",findings:"<div class='totalRow'>&nbsp;</div>"}];
	
	var dollarCheckHeader = [{product: "<div class='alignLeft' cdTarget='slgfiDollarCheck' onclick='collapseDivision(this)'>DOLLAR CHECK</div>",cashier:"", tradVul: "",dsTV: "",vdsTV:"",ud:"",dsUD:"",vdsUD:"",reconciled:"",reconciledBy:"",reconciledDate:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var usCheckInManilaData = generateSLGFIDollarSectionData("totalUsCheckInManila","slgfiDollarCheck");
	
	var ucimTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > US Cheque drawn in Manila</div>",gl: "<div class='numFormat'>"+usCheckInManilaData.cashierTotals.gl+"</div>", tradVul: "<div class='numFormat'>"+usCheckInManilaData.cashierTotals.tv+"</div>",dsTV: "",vdsTV:"",ud:"<div class='numFormat'>"+usCheckInManilaData.cashierTotals.ud+"</div>",dsUD:"",vdsUD:"",reconciled:"",reconciledBy:"",reconciledDate:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var usCheckOutPhData = generateSLGFIDollarSectionData("totalUsCheckOutPh","slgfiDollarCheck");
	
	var ucopTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > US Cheque drawn outside PH</div>",gl: "<div class='numFormat'>"+usCheckOutPhData.cashierTotals.gl+"</div>", tradVul: "<div class='numFormat'>"+usCheckOutPhData.cashierTotals.tv+"</div>",dsTV: "",vdsTV:"",ud:"<div class='numFormat'>"+usCheckOutPhData.cashierTotals.ud+"</div>",dsUD:"",vdsUD:"",reconciled:"",reconciledBy:"",reconciledDate:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
//	var bankOTCCheckData = generateSLGFIDollarSectionData("totalBankOTCCheckPayment");
	
//	var bocTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > Bank OTC Check Payment </div>", tradVul: "<div class='numFormat'>"+bankOTCCheckData.cashierTotals.tv+"</div>",dsTV: "",vdsTV:"",ud:"<div class='numFormat'>"+bankOTCCheckData.cashierTotals.ud+"</div>",dsUD:"",vdsUD:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var checkNCData = generateSLGFIDollarSectionData("totalCheckNonCounter","slgfiDollarCheck");
	
	var cNCTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > Cheque Non Counter</div>",gl: "<div class='numFormat'>"+checkNCData.cashierTotals.gl+"</div>", tradVul: "<div class='numFormat'>"+checkNCData.cashierTotals.tv+"</div>",dsTV: "",vdsTV:"",ud:"<div class='numFormat'>"+checkNCData.cashierTotals.ud+"</div>",dsUD:"",vdsUD:"",reconciled:"",reconciledBy:"",reconciledDate:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var dollarCheckTotals = getSLGFIDollarAreaTotal([usCheckInManilaData,usCheckOutPhData,checkNCData]);
	
	var areaCheckTotal = [{product: "<div class='totalRow'>&nbsp;</div>",gl: "<div class='totalRow'><div class='numFormat'>"+dollarCheckTotals.gl+"</div></div>",dsGL:"<div class='totalRow'>&nbsp;</div>",vdsGL:"<div class='totalRow'>&nbsp;</div>",cashier:"<div class='totalRow'><div class='alignRight'>TOTAL CHECK</div></div>", tradVul: "<div class='totalRow'><div class='numFormat'>"+dollarCheckTotals.tv+"</div></div>",dsTV: "<div class='totalRow'>&nbsp;</div>",vdsTV:"<div class='totalRow'>&nbsp;</div>",ud:"<div class='totalRow'><div class='numFormat'>"+dollarCheckTotals.ud+"</div></div>",dsUD:"<div class='totalRow'>&nbsp;</div>",vdsUD:"<div class='totalRow'>&nbsp;</div>",reconciled:"<div class='totalRow'>&nbsp;</div>",reconciledBy:"<div class='totalRow'>&nbsp;</div>",reconciledDate:"<div class='totalRow'>&nbsp;</div>",ccmNotes:"<div class='totalRow'>&nbsp;</div>",ppaNotes:"<div class='totalRow'>&nbsp;</div>",findings:"<div class='totalRow'>&nbsp;</div>"}];
	
	var nonCashHeader = [{product: "<div class='alignLeft' cdTarget='slgfiDollarNonCash' onclick='collapseDivision(this)'>NON CASH</div>",cashier:"", tradVul: "",dsTV: "",vdsTV:"",ud:"",dsUD:"",vdsUD:"",reconciled:"",reconciledBy:"",reconciledDate:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var nonCashData = generateSLGFIDollarSectionData("totalNonCash","slgfiDollarNonCash");
	
	var nonCashTotal = [{product: "<div class='totalRow'>&nbsp;</div>",gl: "<div class='totalRow'><div class='numFormat'>"+nonCashData.cashierTotals.gl+"</div></div>",dsGL:"<div class='totalRow'>&nbsp;</div>",vdsGL:"<div class='totalRow'>&nbsp;</div>",cashier:"<div class='totalRow'><div class='alignRight'>TOTAL NON CASH</div></div>", tradVul: "<div class='totalRow'><div class='numFormat'>"+nonCashData.cashierTotals.tv+"</div></div>",dsTV: "<div class='totalRow'>&nbsp;</div>",vdsTV:"<div class='totalRow'>&nbsp;</div>",ud:"<div class='totalRow'><div class='numFormat'>"+nonCashData.cashierTotals.ud+"</div></div>",dsUD:"<div class='totalRow'>&nbsp;</div>",vdsUD:"<div class='totalRow'>&nbsp;</div>",reconciled:"<div class='totalRow'>&nbsp;</div>",reconciledBy:"<div class='totalRow'>&nbsp;</div>",reconciledDate:"<div class='totalRow'>&nbsp;</div>",ccmNotes:"<div class='totalRow'>&nbsp;</div>",ppaNotes:"<div class='totalRow'>&nbsp;</div>",findings:"<div class='totalRow'>&nbsp;</div>"}];
	
	var dollarCardHeader = [{product: "<div class='alignLeft' cdTarget='slgfiDollarCard' onclick='collapseDivision(this)'>DOLLAR CARD</div>",cashier:"", tradVul: "",dsTV: "",vdsTV:"",ud:"",dsUD:"",vdsUD:"",reconciled:"",reconciledBy:"",reconciledDate:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
//	var mdsBPIData = generateSLGFIDollarSectionData("totalMdsBpi","slgfiDollarCard");
	
//	var mdsBPITotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > MDS - BPI</div>", gl: "<div class='numFormat'>"+mdsBPIData.cashierTotals.gl+"</div>",tradVul: "<div class='numFormat'>"+mdsBPIData.cashierTotals.tv+"</div>",dsTV: "",vdsTV:"",ud:"<div class='numFormat'>"+mdsBPIData.cashierTotals.ud+"</div>",dsUD:"",vdsUD:"",reconciled:"",reconciledBy:"",reconciledDate:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
//	var mdsBDOData = generateSLGFIDollarSectionData("totalMdsBdo","slgfiDollarCard");
	
//	var mdsBDOTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > MDS - BDO</div>", gl: "<div class='numFormat'>"+mdsBDOData.cashierTotals.gl+"</div>",tradVul: "<div class='numFormat'>"+mdsBDOData.cashierTotals.tv+"</div>",dsTV: "",vdsTV:"",ud:"<div class='numFormat'>"+mdsBDOData.cashierTotals.ud+"</div>",dsUD:"",vdsUD:"",reconciled:"",reconciledBy:"",reconciledDate:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var mdsRCBCData = generateSLGFIDollarSectionData("totalMdsRcbc","slgfiDollarCard");
	
	var mdsRCBCTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > MDS - RCBC</div>", gl: "<div class='numFormat'>"+mdsRCBCData.cashierTotals.gl+"</div>",tradVul: "<div class='numFormat'>"+mdsRCBCData.cashierTotals.tv+"</div>",dsTV: "",vdsTV:"",ud:"<div class='numFormat'>"+mdsRCBCData.cashierTotals.ud+"</div>",dsUD:"",vdsUD:"",reconciled:"",reconciledBy:"",reconciledDate:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
//	var mdsSBCData = generateSLGFIDollarSectionData("totalMdsSbc","slgfiDollarCard");
	
//	var mdsSBCTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > MDS - SCB</div>",gl: "<div class='numFormat'>"+mdsSBCData.cashierTotals.gl+"</div>", tradVul: "<div class='numFormat'>"+mdsSBCData.cashierTotals.tv+"</div>",dsTV: "",vdsTV:"",ud:"<div class='numFormat'>"+mdsSBCData.cashierTotals.ud+"</div>",dsUD:"",vdsUD:"",reconciled:"",reconciledBy:"",reconciledDate:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
//	var mdsCTBData = generateSLGFIDollarSectionData("totalMdsCtb","slgfiDollarCard");
	
//	var mdsCTBTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > MDS - CITIBANK</div>",gl: "<div class='numFormat'>"+mdsCTBData.cashierTotals.gl+"</div>", tradVul: "<div class='numFormat'>"+mdsCTBData.cashierTotals.tv+"</div>",dsTV: "",vdsTV:"",ud:"<div class='numFormat'>"+mdsCTBData.cashierTotals.ud+"</div>",dsUD:"",vdsUD:"",reconciled:"",reconciledBy:"",reconciledDate:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
//	var mdsHSBCData = generateSLGFIDollarSectionData("totalMdsHsbc","slgfiDollarCard");
	
//	var mdsHSBCTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > MDS - HSBC</div>",gl: "<div class='numFormat'>"+mdsHSBCData.cashierTotals.gl+"</div>", tradVul: "<div class='numFormat'>"+mdsHSBCData.cashierTotals.tv+"</div>",dsTV: "",vdsTV:"",ud:"<div class='numFormat'>"+mdsHSBCData.cashierTotals.ud+"</div>",dsUD:"",vdsUD:"",reconciled:"",reconciledBy:"",reconciledDate:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var posRCBCData = generateSLGFIDollarSectionData("totalPosRcbc","slgfiDollarCard");
	
	var posRCBCTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > POS - RCBC</div>",gl: "<div class='numFormat'>"+posRCBCData.cashierTotals.gl+"</div>", tradVul: "<div class='numFormat'>"+posRCBCData.cashierTotals.tv+"</div>",dsTV: "",vdsTV:"",ud:"<div class='numFormat'>"+posRCBCData.cashierTotals.ud+"</div>",dsUD:"",vdsUD:"",reconciled:"",reconciledBy:"",reconciledDate:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
//	var autoCTBData = generateSLGFIDollarSectionData("totalAutoCtb","slgfiDollarCard");
	
//	var autoCTBTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > CITIBANK (AUTOCHARGE)</div>",gl: "<div class='numFormat'>"+autoCTBData.cashierTotals.gl+"</div>", tradVul: "<div class='numFormat'>"+autoCTBData.cashierTotals.tv+"</div>",dsTV: "",vdsTV:"",ud:"<div class='numFormat'>"+autoCTBData.cashierTotals.ud+"</div>",dsUD:"",vdsUD:"",reconciled:"",reconciledBy:"",reconciledDate:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
//	var autoSCBData = generateSLGFIDollarSectionData("totalAutoScb","slgfiDollarCard");
	
//	var autoSCBTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > SCB (AUTOCHARGE)</div>", gl: "<div class='numFormat'>"+autoSCBData.cashierTotals.gl+"</div>",tradVul: "<div class='numFormat'>"+autoSCBData.cashierTotals.tv+"</div>",dsTV: "",vdsTV:"",ud:"<div class='numFormat'>"+autoSCBData.cashierTotals.ud+"</div>",dsUD:"",vdsUD:"",reconciled:"",reconciledBy:"",reconciledDate:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var autoRCBCData = generateSLGFIDollarSectionData("totalAutoRcbc","slgfiDollarCard");
	
	var autoRCBCTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > RCBC - AUTOCHARGE</div>",gl: "<div class='numFormat'>"+autoRCBCData.cashierTotals.gl+"</div>", tradVul: "<div class='numFormat'>"+autoRCBCData.cashierTotals.tv+"</div>",dsTV: "",vdsTV:"",ud:"<div class='numFormat'>"+autoRCBCData.cashierTotals.ud+"</div>",dsUD:"",vdsUD:"",reconciled:"",reconciledBy:"",reconciledDate:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
//	var sunlinkOLData = generateSLGFIDollarSectionData("totalSunlinkOnline","slgfiDollarCard");
	
//	var sunlinkOLTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > SUNLINK ONLINE</div>", gl: "<div class='numFormat'>"+sunlinkOLData.cashierTotals.gl+"</div>",tradVul: "<div class='numFormat'>"+sunlinkOLData.cashierTotals.tv+"</div>",dsTV: "",vdsTV:"",ud:"<div class='numFormat'>"+sunlinkOLData.cashierTotals.ud+"</div>",dsUD:"",vdsUD:"",reconciled:"",reconciledBy:"",reconciledDate:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
//	var sunlinkHSBCDData = generateSLGFIDollarSectionData("totalSunlinkHsbcDollar","slgfiDollarCard");
	
//	var sunlinkHSBCDTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > SUNLINK HSBC DOLLAR</div>",gl: "<div class='numFormat'>"+sunlinkHSBCDData.cashierTotals.gl+"</div>", tradVul: "<div class='numFormat'>"+sunlinkHSBCDData.cashierTotals.tv+"</div>",dsTV: "",vdsTV:"",ud:"<div class='numFormat'>"+sunlinkHSBCDData.cashierTotals.ud+"</div>",dsUD:"",vdsUD:"",reconciled:"",reconciledBy:"",reconciledDate:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
//	var epsBPIData = generateSLGFIDollarSectionData("totalEpsBpi","slgfiDollarCard");
	
//	var epsBPITotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > EPS - BPI</div>", gl: "<div class='numFormat'>"+epsBPIData.cashierTotals.gl+"</div>",tradVul: "<div class='numFormat'>"+epsBPIData.cashierTotals.tv+"</div>",dsTV: "",vdsTV:"",ud:"<div class='numFormat'>"+epsBPIData.cashierTotals.ud+"</div>",dsUD:"",vdsUD:"",reconciled:"",reconciledBy:"",reconciledDate:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var areaCardTotals = getSLGFIDollarAreaTotal([mdsRCBCData,posRCBCData,autoRCBCData]);			
	
	var dollarCardTotal = [{product: "<div class='totalRow'>&nbsp;</div>",gl: "<div class='totalRow'><div class='numFormat'>"+areaCardTotals.gl+"</div></div>",dsGL:"<div class='totalRow'>&nbsp;</div>",vdsGL:"<div class='totalRow'>&nbsp;</div>",cashier:"<div class='totalRow'><div class='alignRight'>TOTAL CARD</div></div>", tradVul: "<div class='totalRow'><div class='numFormat'>"+areaCardTotals.tv+"</div></div>",dsTV: "<div class='totalRow'>&nbsp;</div>",vdsTV:"<div class='totalRow'>&nbsp;</div>",ud:"<div class='totalRow'><div class='numFormat'>"+areaCardTotals.ud+"</div></div>",dsUD:"<div class='totalRow'>&nbsp;</div>",vdsUD:"<div class='totalRow'>&nbsp;</div>",reconciled:"<div class='totalRow'>&nbsp;</div>",reconciledBy:"<div class='totalRow'>&nbsp;</div>",reconciledDate:"<div class='totalRow'>&nbsp;</div>",ccmNotes:"<div class='totalRow'>&nbsp;</div>",ppaNotes:"<div class='totalRow'>&nbsp;</div>",findings:"<div class='totalRow'>&nbsp;</div>"}];
	
	var nonPostedFromDTRHeader = [{product: "<div class='alignLeft' cdTarget='slgfiDollarNonPosted' onclick='collapseDivision(this)'>NON POSTED (from DTR)</div>",cashier:"", tradVul: "",dsTV: "",vdsTV:"",ud:"",dsUD:"",vdsUD:"",reconciled:"",reconciledBy:"",reconciledDate:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var nbIndividualData = generateSLGFIDollarSectionData("totalNewBusinessIndividual","slgfiDollarNonPosted");
	
	var nbIndividualTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > NB Individual</div>", gl: "<div class='numFormat'>"+nbIndividualData.cashierTotals.gl+"</div>",tradVul: "<div class='numFormat'>"+nbIndividualData.cashierTotals.tv+"</div>",dsTV: "",vdsTV:"",ud:"<div class='numFormat'>"+nbIndividualData.cashierTotals.ud+"</div>",dsUD:"",vdsUD:"",reconciled:"",reconciledBy:"",reconciledDate:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var nbVULData = generateSLGFIDollarSectionData("totalNewBusinessVariable","slgfiDollarNonPosted");
	
	var nbVULTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > NB VUL</div>", gl: "<div class='numFormat'>"+nbVULData.cashierTotals.gl+"</div>",tradVul: "<div class='numFormat'>"+nbVULData.cashierTotals.tv+"</div>",dsTV: "",vdsTV:"",ud:"<div class='numFormat'>"+nbVULData.cashierTotals.ud+"</div>",dsUD:"",vdsUD:"",reconciled:"",reconciledBy:"",reconciledDate:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var individualRenewalData = generateSLGFIDollarSectionData("totalIndividualRenewal","slgfiDollarNonPosted");
	
	var individualRenewalTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > Individual Renewal</div>", gl: "<div class='numFormat'>"+individualRenewalData.cashierTotals.gl+"</div>",tradVul: "<div class='numFormat'>"+individualRenewalData.cashierTotals.tv+"</div>",dsTV: "",vdsTV:"",ud:"<div class='numFormat'>"+individualRenewalData.cashierTotals.ud+"</div>",dsUD:"",vdsUD:"",reconciled:"",reconciledBy:"",reconciledDate:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var vulRenewalData = generateSLGFIDollarSectionData("totalVariableRenewal","slgfiDollarNonPosted");
	
	var vulRenewalTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > VUL Renewal</div>",gl: "<div class='numFormat'>"+vulRenewalData.cashierTotals.gl+"</div>", tradVul: "<div class='numFormat'>"+vulRenewalData.cashierTotals.tv+"</div>",dsTV: "",vdsTV:"",ud:"<div class='numFormat'>"+vulRenewalData.cashierTotals.ud+"</div>",dsUD:"",vdsUD:"",reconciled:"",reconciledBy:"",reconciledDate:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var nonCashDTRData = generateSLGFIDollarSectionData("totalNonCashFromDTR","slgfiDollarFMP");
	
	var nonCashDTRTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > Non Cash</div>", gl: "<div class='numFormat'>"+nonCashDTRData.cashierTotals.gl+"</div>",tradVul: "<div class='numFormat'>"+nonCashDTRData.cashierTotals.tv+"</div>",dsTV: "",vdsTV:"",ud:"<div class='numFormat'>"+nonCashDTRData.cashierTotals.ud+"</div>",dsUD:"",vdsUD:"",reconciled:"",reconciledBy:"",reconciledDate:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var areaNonPostedTotals = getSLGFIDollarAreaTotal([nbIndividualData,nbVULData,individualRenewalData,vulRenewalData,nonCashDTRData]);
	
	var nonPostedTotal = [{product: "<div class='totalRow'>&nbsp;</div>",gl: "<div class='totalRow'><div class='numFormat'>"+areaNonPostedTotals.gl+"</div></div>",dsGL:"<div class='totalRow'>&nbsp;</div>",vdsGL:"<div class='totalRow'>&nbsp;</div>",cashier:"<div class='totalRow'><div class='alignRight'>TOTAL NON-POSTED</div></div>", tradVul: "<div class='totalRow'><div class='numFormat'>"+areaNonPostedTotals.tv+"</div></div>",dsTV: "<div class='totalRow'>&nbsp;</div>",vdsTV:"<div class='totalRow'>&nbsp;</div>",ud:"<div class='totalRow'><div class='numFormat'>"+areaNonPostedTotals.ud+"</div></div>",dsUD:"<div class='totalRow'>&nbsp;</div>",vdsUD:"<div class='totalRow'>&nbsp;</div>",reconciled:"<div class='totalRow'>&nbsp;</div>",reconciledBy:"<div class='totalRow'>&nbsp;</div>",reconciledDate:"<div class='totalRow'>&nbsp;</div>",ccmNotes:"<div class='totalRow'>&nbsp;</div>",ppaNotes:"<div class='totalRow'>&nbsp;</div>",findings:"<div class='totalRow'>&nbsp;</div>"}];
	
	var forManualPostingHeader = [{product: "<div class='alignLeft' cdTarget='slgfiDollarFMP' onclick='collapseDivision(this)'>FOR MANUAL POSTING</div>",cashier:"", tradVul: "",dsTV: "",vdsTV:"",ud:"",dsUD:"",vdsUD:"",reconciled:"",reconciledBy:"",reconciledDate:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var nonPolicyFMPData = generateSLGFIDollarSectionData("totalNonPolicy","slgfiDollarFMP");
	
	var nonPolicyFMPTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > Non Policy</div>", gl: "<div class='numFormat'>"+nonPolicyFMPData.cashierTotals.gl+"</div>",tradVul: "<div class='numFormat'>"+nonPolicyFMPData.cashierTotals.tv+"</div>",dsTV: "",vdsTV:"",ud:"<div class='numFormat'>"+nonPolicyFMPData.cashierTotals.ud+"</div>",dsUD:"",vdsUD:"",reconciled:"",reconciledBy:"",reconciledDate:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var worksiteNotPostedFMPData = generateSLGFIDollarSectionData("totalWorksiteNonPosted","slgfiDollarFMP");
	
	var worksiteNotPostedFMPTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > Worksite</div>", gl: "<div class='numFormat'>"+worksiteNotPostedFMPData.cashierTotals.gl+"</div>",tradVul: "<div class='numFormat'>"+worksiteNotPostedFMPData.cashierTotals.tv+"</div>",dsTV: "",vdsTV:"",ud:"<div class='numFormat'>"+worksiteNotPostedFMPData.cashierTotals.ud+"</div>",dsUD:"",vdsUD:"",reconciled:"",reconciledBy:"",reconciledDate:"",ccmNotes:"",ppaNotes:"",findings:""}];		
	
	var areaForManualPostingTotals = getSLGFIDollarAreaTotal([nonPolicyFMPData,worksiteNotPostedFMPData]);
	
	var forManualPostingotal = [{product: "<div class='totalRow'>&nbsp;</div>",gl: "<div class='totalRow'><div class='numFormat'>"+areaForManualPostingTotals.gl+"</div></div>",dsGL:"<div class='totalRow'>&nbsp;</div>",vdsGL:"<div class='totalRow'>&nbsp;</div>",cashier:"<div class='totalRow'><div class='alignRight'>TOTAL FOR MANUAL POSTING</div></div>", tradVul: "<div class='totalRow'><div class='numFormat'>"+areaForManualPostingTotals.tv+"</div></div>",dsTV: "<div class='totalRow'>&nbsp;</div>",vdsTV:"<div class='totalRow'>&nbsp;</div>",ud:"<div class='totalRow'><div class='numFormat'>"+areaForManualPostingTotals.ud+"</div></div>",dsUD:"<div class='totalRow'>&nbsp;</div>",vdsUD:"<div class='totalRow'>&nbsp;</div>",reconciled:"<div class='totalRow'>&nbsp;</div>",reconciledBy:"<div class='totalRow'>&nbsp;</div>",reconciledDate:"<div class='totalRow'>&nbsp;</div>",ccmNotes:"<div class='totalRow'>&nbsp;</div>",ppaNotes:"<div class='totalRow'>&nbsp;</div>",findings:"<div class='totalRow'>&nbsp;</div>"}];
	
	var exemptionHeader = [{product: "<div class='alignLeft' cdTarget='slgfiDollarExemptions' onclick='collapseDivision(this)'>EXEMPTIONS</div>",cashier:"", tradVul: "",dsTV: "",vdsTV:"",ud:"",dsUD:"",vdsUD:"",reconciled:"",reconciledBy:"",reconciledDate:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var exemptionData = generateSLGFIDollarSectionData("reversalProduct","slgfiDollarExemptions");
	
	var exemptionTotal = [{product: "<div class='totalRow'>&nbsp;</div>",gl: "<div class='totalRow'><div class='numFormat'>"+exemptionData.cashierTotals.gl+"</div></div>",dsGL:"<div class='totalRow'>&nbsp;</div>",vdsGL:"<div class='totalRow'>&nbsp;</div>",cashier:"<div class='totalRow'><div class='alignRight'>TOTAL EXEMPTIONS</div></div>", tradVul: "<div class='totalRow'><div class='numFormat'>"+exemptionData.cashierTotals.tv+"</div></div>",dsTV: "<div class='totalRow'>&nbsp;</div>",vdsTV:"<div class='totalRow'>&nbsp;</div>",ud:"<div class='totalRow'><div class='numFormat'>"+exemptionData.cashierTotals.ud+"</div></div>",dsUD:"<div class='totalRow'>&nbsp;</div>",vdsUD:"<div class='totalRow'>&nbsp;</div>",reconciled:"<div class='totalRow'>&nbsp;</div>",reconciledBy:"<div class='totalRow'>&nbsp;</div>",reconciledDate:"<div class='totalRow'>&nbsp;</div>",ccmNotes:"<div class='totalRow'>&nbsp;</div>",ppaNotes:"<div class='totalRow'>&nbsp;</div>",findings:"<div class='totalRow'>&nbsp;</div>"}];
	
	var sessionTotalHeader = [{product: "<div class='alignLeft' cdTarget='slgfiDollarST' onclick='collapseDivision(this)'>SESSION TOTALS</div>",cashier:"", tradVul: "",dsTV: "",vdsTV:"",ud:"",dsUD:"",vdsUD:"",reconciled:"",reconciledBy:"",reconciledDate:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var sessionTotalData = generateSLGFIDollarSectionData("sessionTotal","slgfiDollarST");
	
	var sessionTotalTotal = [{product: "<div class='totalRow'>&nbsp;</div>",gl: "<div class='totalRow'><div class='numFormat'>"+sessionTotalData.cashierTotals.gl+"</div></div>",dsGL:"<div class='totalRow'>&nbsp;</div>",vdsGL:"<div class='totalRow'>&nbsp;</div>",cashier:"<div class='totalRow'><div class='alignRight'>SESSION TOTAL</div></div>", tradVul: "<div class='totalRow'><div class='numFormat'>"+sessionTotalData.cashierTotals.tv+"</div></div>",dsTV: "<div class='totalRow'>&nbsp;</div>",vdsTV:"<div class='totalRow'>&nbsp;</div>",ud:"<div class='totalRow'><div class='numFormat'>"+sessionTotalData.cashierTotals.ud+"</div></div>",dsUD:"<div class='totalRow'>&nbsp;</div>",vdsUD:"<div class='totalRow'>&nbsp;</div>",reconciled:"<div class='totalRow'><input type='hidden' id='gfUsdActiveCB' name='gfUsdActiveCB' value='"+ppaGfUsdActive+"'/><div>"+ppaGfUsdActive+"/"+ppaGfUsdTotal+"</div></div>",reconciledBy:"<div class='totalRow'>&nbsp;</div>",reconciledDate:"<div class='totalRow'>&nbsp;</div>",ccmNotes:"<div class='totalRow'>&nbsp;</div>",ppaNotes:"<div class='totalRow'>&nbsp;</div>",findings:"<div class='totalRow'>&nbsp;</div>"}];
	
	var totalSLGFIDCashiersCollection = getSLGFIDollarAreaTotal([nonCashData,cashNonCounterData,cashCounterData,usCheckInManilaData,usCheckOutPhData,checkNCData,mdsRCBCData,posRCBCData,autoRCBCData]);
	
	var cashierTotalHeader = [{product: "<div class='alignLeft' cdTarget='slgfiDollarGTPC' onclick='collapseDivision(this)'>Grand Total Per Cashier</div>",cashier:"", tradVul: "",dsTV: "",vdsTV:"",ud:"",dsUD:"",vdsUD:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var cashierTotal = getSLGFIDGrandTotalPerCashier("slgfiDollarGTPC");
	
	var cashierTotalRow = [{product: "<div class='totalRow'>&nbsp;</div>",cashier:"<div class='totalRow'><div class='alignRight'>TOTAL COLLECTION</div></div>",gl: "<div class='totalRow'><div class='numFormat'>"+numberWithCommas(totalSLGFIDCashiersCollection.gl)+"</div></div>",dsGL:"<div class='totalRow'>&nbsp;</div>",vdsGL:"<div class='totalRow'>&nbsp;</div>", tradVul: "<div class='totalRow'><div class='numFormat'>"+numberWithCommas(totalSLGFIDCashiersCollection.tv)+"</div></div>",dsTV: "<div class='totalRow'>&nbsp;</div>",vdsTV:"<div class='totalRow'>&nbsp;</div>",ud:"<div class='totalRow'><div class='numFormat'>"+numberWithCommas(totalSLGFIDCashiersCollection.ud)+"</div></div>",dsUD:"<div class='totalRow'>&nbsp;</div>",vdsUD:"<div class='totalRow'>&nbsp;</div>",reconciled:"<div class='totalRow'>&nbsp;</div>",reconciledBy:"<div class='totalRow'>&nbsp;</div>",reconciledDate:"<div class='totalRow'>&nbsp;</div>",ccmNotes:"<div class='totalRow'>&nbsp;</div>",ppaNotes:"<div class='totalRow'>&nbsp;</div>",findings:"<div class='totalRow'>&nbsp;</div>"}];
	
	var slgfiDollarSummaryData = [{product: "",cashier:"CONSOLIDATED GRAND TOTAL",gl:"<div class='numFormat'>"+numberWithCommas(slgfiDollarConsolidatedTotals.gl)+"</div>", tradVul: "<div class='numFormat'>"+numberWithCommas(slgfiDollarConsolidatedTotals.tv)+"</div>",dsTV: "",vdsTV:"",ud:"<div class='numFormat'>"+numberWithCommas(slgfiDollarConsolidatedTotals.ud)+"</div>",dsUD:"",vdsUD:"",reconciled:"",reconciledBy:"",reconciledDate:"",ccmNotes:"",ppaNotes:"",findings:""},
	  	                        {product: "",cashier:"TOTAL CASHIERS COLLECTION",gl:"<div class='numFormat'>"+numberWithCommas(totalSLGFIDCashiersCollection.gl)+"</div>", tradVul:"<div class='numFormat'>"+numberWithCommas(totalSLGFIDCashiersCollection.tv)+"</div>",dsTV: "",vdsTV:"",ud:"<div class='numFormat'>"+numberWithCommas(totalSLGFIDCashiersCollection.ud)+"</div>",dsUD:"",vdsUD:"",reconciled:"",reconciledBy:"",reconciledDate:"",ccmNotes:"",ppaNotes:"",findings:""},
	  	                        {product: "",cashier:"",gl:"<div class='numFormat'>"+compareTotalsImg(numberWithCommas(slgfiDollarConsolidatedTotals.gl),totalSLGFIDCashiersCollection.gl)+"</div>",tradVul: compareTotalsImg(numberWithCommas(slgfiDollarConsolidatedTotals.tv),totalSLGFIDCashiersCollection.tv),dsTV: "",vdsTV:"",ud:compareTotalsImg(numberWithCommas(slgfiDollarConsolidatedTotals.ud),totalSLGFIDCashiersCollection.ud),dsUD:"",vdsUD:"",reconciled:"",reconciledBy:"",reconciledDate:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	new TableContainer({
		divName : slgfiDollarSummaryDivDetails.divName,
		nodeName : slgfiDollarSummaryDivDetails.nodeName,
		width : "1210px",
		id : slgfiDollarSummaryDivDetails.tableId,
		layout : slgfiDollarColumns,
		tableClass: "balancingToolSummary",
		data: slgfiDollarSummaryData
	}).startUp();
	
	var slgfiDollarSectionData = [];
	
	appendJsons([slgfiDollarSectionData,dollarCashHeader,cashNonCounterData.prodTotals,cncTotal,cashCounterData.prodTotals,ccTotal,areaCashTotal,dollarCheckHeader,usCheckInManilaData.prodTotals,ucimTotal,usCheckOutPhData.prodTotals,ucopTotal,checkNCData.prodTotals,cNCTotal,areaCheckTotal,dollarCardHeader,mdsRCBCData.prodTotals,mdsRCBCTotal,posRCBCData.prodTotals,posRCBCTotal,autoRCBCData.prodTotals,autoRCBCTotal,dollarCardTotal,nonCashHeader,nonCashData.prodTotals,nonCashTotal,cashierTotalHeader,cashierTotal,cashierTotalRow,nonPostedFromDTRHeader,nbIndividualData.prodTotals,nbIndividualTotal,nbVULData.prodTotals,nbVULTotal,individualRenewalData.prodTotals,individualRenewalTotal,vulRenewalData.prodTotals,vulRenewalTotal,nonPostedTotal,forManualPostingHeader,nonCashDTRData.prodTotals,nonCashDTRTotal,nonPolicyFMPData.prodTotals,nonPolicyFMPTotal,worksiteNotPostedFMPData.prodTotals,worksiteNotPostedFMPTotal,forManualPostingotal,exemptionHeader,exemptionData.prodTotals,exemptionTotal,sessionTotalHeader,sessionTotalData.prodTotals,sessionTotalTotal,generateSnapshotSection(slgfiSnapshot)]);
	
	new TableContainer({
		divName : slgfiDollarDivDetails.divName,
		nodeName : slgfiDollarDivDetails.nodeName,
		width : "1210px",
		id : slgfiDollarDivDetails.tableId,
		layout : slgfiDollarColumnsNH,
		tableClass: "balancingToolSummary",
		data: slgfiDollarSectionData
	}).startUp();
	
	var slgfiDollarTableDiv = document.getElementById(slgfiDollarTableDivId);

	slgfiDollarTableDiv.style.display = "none";
	
	});
}


var slgfiDollarSecData = [];

function mergeSLGFIDollarCashierProductTypes(){
	var cashierRep = [];
	for(c in slgfiDollarData){
		//using jQuery.inArray instead of Array.indexOf because it is not sudported in IE8 and below. thanks IE.
		if(jQuery.inArray(slgfiDollarData[c].cashierId+"~"+slgfiDollarData[c].product,cashierRep) == -1){
			var pushedData = "";
			pushedData = pushedData.concat(slgfiDollarData[c].cashierId);
			pushedData = pushedData.concat("~");
			pushedData = pushedData.concat(slgfiDollarData[c].product);
			cashierRep.push(pushedData);
		}
	}
	
	for(var a = 0; a < cashierRep.length; a++){
		slgfiDollarSecData.push(mergeSLGFIDollarCashierData(cashierRep[a]));
	}
		
}

function getSLGFIDollarAreaTotal(area){
	var tots = {};
	var tv = 0;
	var ud = 0;
	var gl = 0;
	for(a in area){
		tv = tv + parseFloat(area[a].cashierTotals.tv.replace(/,/g,""));
		ud = ud + parseFloat(area[a].cashierTotals.ud.replace(/,/g,""));
		gl = gl + parseFloat(area[a].cashierTotals.gl.replace(/,/g,""));
	}
	tots.tv = numberWithCommas(parseFloat(tv).toFixed(2));
	tots.ud = numberWithCommas(parseFloat(ud).toFixed(2));
	tots.gl = numberWithCommas(parseFloat(gl).toFixed(2));
	return tots;
}

function generateSLGFIDollarSectionData(section, cdTarget) {
    var sectionObj = {};
    var sectionCashierTotals = {};
    var tv = 0;
    var ud = 0;
    var gl = 0;
    var returnCNC = [];
    var firstRowFlag = true;
    for (a in slgfiDollarSecData) {
        var tempCNC = {};
	if(slgfiDollarSecData[a].product === section){
            var isCashDepo=false;
            var isCheckDepo=false;
            if(firstRowFlag){
                switch(section){
                    case "totalCashNonCounter": tempCNC.product = "<div class='alignLeft'>Non Counter</div>";break;
                    case "totalCashCounter": tempCNC.product = "<div class='alignLeft'>Counter</div>";isCashDepo=true;break;
                    case "totalCheckOnUs": tempCNC.product = "<div class='alignLeft'>On Us</div>";break;
                    case "totalCheckRegional": tempCNC.product = "<div class='alignLeft'>Regional</div>";break;
                    case "totalCheckNonCounter": tempCNC.product = "<div class='alignLeft'>Non Counter</div>";break;
                    case "totalCheckLocal": tempCNC.product = "<div class='alignLeft'>Local</div>";break;
                    case "totalUsCheckInManila" : tempCNC.product = "<div class='alignLeft'>US Cheque drawn in Manila</div>";isCheckDepo=true;checkType="USCD_MANILA";break;
                    case "totalUsCheckOutPh" : tempCNC.product = "<div class='alignLeft'>US Cheque drawn outside PH</div>";isCheckDepo=true;checkType="USCD_OUTSIDEPH";break;
                    case "totalDollarCheque" : tempCNC.product = "<div class='alignLeft'>Dollar Cheque</div>";isCheckDepo=true;checkType="DOLLARCHECK";break;
                    case "totalMdsBpi": tempCNC.product = "<div class='alignLeft'>MDS - BPI</div>";break;
                    case "totalMdsBdo": tempCNC.product = "<div class='alignLeft'>MDS - BDO</div>";break;
                    case "totalMdsRcbc": tempCNC.product = "<div class='alignLeft'>MDS - RCBC</div>";break;
                    case "totalMdsSbc": tempCNC.product = "<div class='alignLeft'>MDS - SCB</div>";break;
                    case "totalMdsCtb": tempCNC.product = "<div class='alignLeft'>MDS - CITIBANK</div>";break;
                    case "totalMdsHsbc": tempCNC.product = "<div class='alignLeft'>MDS - HSBC</div>";break;
                    case "totalAutoCtb": tempCNC.product = "<div class='alignLeft'>CITIBANK (AUTOCHARGE)</div>";break;
                    case "totalAutoScb": tempCNC.product = "<div class='alignLeft'>SCB (AUTOCHARGE)</div>";break;
                    case "totalAutoRcbc": tempCNC.product = "<div class='alignLeft'>RCBC - AUTOCHARGE</div>";break;
                    case "totalPosRcbc": tempCNC.product = "<div class='alignLeft'>POS - RCBC</div>";break;					
                    case "totalSunlinkOnline": tempCNC.product = "<div class='alignLeft'>SUNLINK ONLINE</div>";break;
                    case "totalSunlinkHsbcDollar": tempCNC.product = "<div class='alignLeft'>SUNLINK HSBC DOLLAR</div>";break;
                    case "totalEpsBpi": tempCNC.product = "<div class='alignLeft'>EPS - BPI</div>";break;
                    case "totalNonCash": tempCNC.product = "<div class='alignLeft'>Non Cash</div>";break;
                    case "totalNewBusinessIndividual": tempCNC.product = "<div class='alignLeft'>New Business Individual</div>";break;
                    case "totalNewBusinessVariable": tempCNC.product = "<div class='alignLeft'>NB VUL</div>";break;
                    case "totalIndividualRenewal": tempCNC.product = "<div class='alignLeft'>Individual Renewal</div>";break;
                    case "totalVariableRenewal": tempCNC.product = "<div class='alignLeft'>VUL Renewal</div>";break;
                    case "totalNonCashFromDTR": tempCNC.product = "<div class='alignLeft'>Non Cash</div>";break;
                    case "totalNonPolicy": tempCNC.product = "<div class='alignLeft'>Non Policy</div>";break;
                    case "totalWorksiteNonPosted": tempCNC.product = "<div class='alignLeft'>Worksite (not posted)</div>";break;
                    case "reversalProduct" : tempCNC.product = "";break;
                    case "sessionTotal" : tempCNC.product = "";break;
                    default: alert("Unknown Product Total"); break;
                }
            } else {
                tempCNC.product = "";
                switch(section){
                    case "totalCashCounter": isCashDepo=true;break;
                    case "totalUsCheckInManila" : isCheckDepo=true;checkType="USCD_MANILA";break;
                    case "totalUsCheckOutPh" : isCheckDepo=true;checkType="USCD_OUTSIDEPH";break;
                    case "totalDollarCheque" : isCheckDepo=true;checkType="DOLLARCHECK";break;
                }
            }

            tempCNC.cashier = "<div class='alignLeft'>" + slgfiDollarSecData[a].cashierName + "</div>";
            if (isSLGFIConfirmed(slgfiDollarSecData[a].cashierId)) {
                if (section === "reversalProduct") {
                    if (parseFloat(slgfiDollarSecData[a].tradVul) !== 0) {
                        tempCNC.tradVul = "<div class='alignRight'><a style='cursor:pointer;text-decoration:underline;color:blue;'onclick='openSPReversal(" + getBalancingToolProductId(slgfiDollarSecData[a].cashierId, "RL_RV_DOLLAR", section, slgfiDollarData) + ");'>" + numberWithCommas(parseFloat(slgfiDollarSecData[a].tradVul).toFixed(2)) + "</a></div>";
                    } else {
                        tempCNC.tradVul = "<div class='alignRight'>" + numberWithCommas(parseFloat(slgfiDollarSecData[a].tradVul).toFixed(2)) + "</div>";
                    }
                    if (parseFloat(slgfiDollarSecData[a].ud) !== 0) {
                        tempCNC.ud = "<div class='alignRight'><a style='cursor:pointer;text-decoration:underline;color:blue;'onclick='openSPReversal(" + getBalancingToolProductId(slgfiDollarSecData[a].cashierId, "RU_DOLLAR", section, slgfiDollarData) + ");'>" + numberWithCommas(parseFloat(slgfiDollarSecData[a].ud).toFixed(2)) + "</a></div>";
                    } else {
                        tempCNC.ud = "<div class='alignRight'>" + numberWithCommas(parseFloat(slgfiDollarSecData[a].ud).toFixed(2)) + "</div>";
                    }
                    if (parseFloat(slgfiDollarSecData[a].gl) !== 0) {
                        tempCNC.gl = "<div class='alignRight'><a style='cursor:pointer;text-decoration:underline;color:blue;'onclick='openSPReversal(" + getBalancingToolProductId(slgfiDollarSecData[a].cashierId, "RG_DOLLAR", section, slgfiDollarData) + ");'>" + numberWithCommas(parseFloat(slgfiDollarSecData[a].gl).toFixed(2)) + "</a></div>";
                    } else {
                        tempCNC.gl = "<div class='alignRight'>" + numberWithCommas(parseFloat(slgfiDollarSecData[a].gl).toFixed(2)) + "</div>";
                    }
                } else {
                    tempCNC.tradVul = "<div class='alignRight'>" + numberWithCommas(parseFloat(slgfiDollarSecData[a].tradVul).toFixed(2)) + "</div>";
                    tempCNC.ud = "<div class='alignRight'>" + numberWithCommas(parseFloat(slgfiDollarSecData[a].ud).toFixed(2)) + "</div>";
                    tempCNC.gl = "<div class='alignRight'>" + numberWithCommas(parseFloat(slgfiDollarSecData[a].gl).toFixed(2)) + "</div>";
                    if (isCashDepo) {
                        if (parseFloat(slgfiDollarSecData[a].tradVul) > 0) {
                            tempCNC.dsTV = getCashDepoURL("SLGFI", "TRAD/VUL", "USD", slgfiDollarSecData[a].cashierId);
                            tempCNC.vdsTV = getValidatedCashDepoURL("SLGFI", "TRAD/VUL", "USD", slgfiDollarSecData[a].cashierId);
                        }
                        if (parseFloat(slgfiDollarSecData[a].ud) > 0) {
                            tempCNC.dsUD = getCashDepoURL("SLGFI", "UNCOVERTED", "USD", slgfiDollarSecData[a].cashierId);
                            tempCNC.vdsUD = getValidatedCashDepoURL("SLGFI", "UNCOVERTED", "USD", slgfiDollarSecData[a].cashierId);
                        }
                    } else if (isCheckDepo) {
                        if (parseFloat(slgfiDollarSecData[a].tradVul) > 0) {
                            tempCNC.dsTV = getChequeDepoURL("SLGFI", "TRAD/VUL", "USD", slgfiDollarSecData[a].cashierId, checkType);
                            tempCNC.vdsTV = getValidatedChequeDepoURL("SLGFI", "TRAD/VUL", "USD", slgfiDollarSecData[a].cashierId, checkType);
                        }
                        if (parseFloat(slgfiDollarSecData[a].ud) > 0) {
                            tempCNC.dsUD = getChequeDepoURL("SLGFI", "UNCOVERTED", "USD", slgfiDollarSecData[a].cashierId, checkType);
                            tempCNC.vdsUD = getValidatedChequeDepoURL("SLGFI", "UNCOVERTED", "USD", slgfiDollarSecData[a].cashierId, checkType);
                        }
                    }
                }
                tv = tv + parseFloat(slgfiDollarSecData[a].tradVul);
                ud = ud + parseFloat(slgfiDollarSecData[a].ud);
                gl = gl + parseFloat(slgfiDollarSecData[a].gl);
            } else {
                tempCNC.tradVul = "<div class='alignCenter'>-</div>";
                tempCNC.ud = "<div class='alignCenter'>-</div>";
                tempCNC.gl = "<div class='alignCenter'>-</div>";
            }
            if (jQuery.inArray(section, excludeFromCashierTotal) === -1) {
                slgfiDollarConsolidatedTotals.tv = (parseFloat(slgfiDollarConsolidatedTotals.tv) + parseFloat(slgfiDollarSecData[a].tradVul)).toFixed(2);
                slgfiDollarConsolidatedTotals.gl = (parseFloat(slgfiDollarConsolidatedTotals.gl) + parseFloat(slgfiDollarSecData[a].gl)).toFixed(2);
                slgfiDollarConsolidatedTotals.ud = (parseFloat(slgfiDollarConsolidatedTotals.ud) + parseFloat(slgfiDollarSecData[a].ud)).toFixed(2);
            }
            if (jQuery.inArray(section, excludeFromReconCheckbox) === -1) {
                var isWholeRowZero = false;
                var isMDS = false;
                if (parseFloat(slgfiDollarSecData[a].tradVul) === 0 && parseFloat(slgfiDollarSecData[a].ud) === 0 && parseFloat(slgfiDollarSecData[a].gl) === 0) {
                    isWholeRowZero = true;
                }
                if (jQuery.inArray(section, mdsProdTotals) !== -1) {
                    isMDS = true;
                }
                tempCNC.reconciled = "<div class='alignCenter'>" + getReconCheckbox(rowIndex, isWholeRowZero, isMDS, "GFUSD") + "</div>";
                tempCNC.reconciledBy = "<div class='alignCenter'>" + getReconcilerName(rowIndex, isWholeRowZero, isMDS) + "</div>";
                tempCNC.reconciledDate = "<div class='alignCenter'>" + getReconciledDate(rowIndex) + "</div>";
            } else {
                tempCNC.reconciled = "<div class='alignCenter'>-</div>";
                tempCNC.reconciledBy = "<div class='alignCenter'>-</div>";
                tempCNC.reconciledDate = "<div class='alignCenter'>-</div>";
            }
            tempCNC.ccmNotes = "<div class='alignCenter'>" + getNote(rowIndex, "CCM") + "</div>";
            tempCNC.ppaNotes = "<div class='alignCenter'>" + getNote(rowIndex, "PPA") + "</div>";
            tempCNC.findings = "<div class='alignCenter'  collapseTarget='" + cdTarget + "'>" + getNote(rowIndex, "FINDINGS") + "</div>";
            rowIndex++;
            firstRowFlag = false;
            returnCNC.push(tempCNC);
        }
    }
    sectionCashierTotals.tv = numberWithCommas(parseFloat(tv).toFixed(2));
    sectionCashierTotals.ud = numberWithCommas(parseFloat(ud).toFixed(2));
    sectionCashierTotals.gl = numberWithCommas(parseFloat(gl).toFixed(2));
    sectionObj.prodTotals = returnCNC;
    sectionObj.cashierTotals = sectionCashierTotals;
    return sectionObj;
}

function mergeSLGFIDollarCashierData(cashierProduct){
	var cpArr = cashierProduct.split("~");
	var cashierId = cpArr[0];
	var product = cpArr[1];
	var tempCashier = {};
	for(a in slgfiDollarData){
		if(slgfiDollarData[a].cashierId == cashierId && slgfiDollarData[a].product == product){
			if(slgfiDollarData[a].productCode == "RL_RV_DOLLAR"){
				tempCashier.tradVul = slgfiDollarData[a].total;
			} else if(slgfiDollarData[a].productCode == "RU_DOLLAR"){
				tempCashier.ud = slgfiDollarData[a].total;
			} else if(slgfiDollarData[a].productCode == "RG_DOLLAR"){
				tempCashier.gl = slgfiDollarData[a].total;				
			}
			tempCashier.cashierId = slgfiDollarData[a].cashierId;
			tempCashier.cashierName = slgfiDollarData[a].cashierName;
			tempCashier.product = product;
		}
	}
	return tempCashier;
}

function getSLGFIDGrandTotalPerCashier(cdTarget){
	var returnSection = [];
	var c = 0;
	for(c in cashierList){
		var tradVul = 0;
		var ud = 0;
		var gl = 0;
		var d=0;
		for(d in slgfiDollarData){
			if(jQuery.inArray(slgfiDollarData[d].product,excludeFromCashierTotal) == -1 && slgfiDollarData[d].cashierId.toUpperCase() == cashierList[c].id.toUpperCase()){
				switch(slgfiDollarData[d].productCode){
					case "RL_RV_DOLLAR": tradVul = tradVul + parseFloat(slgfiDollarData[d].total);break;
					case "RU_DOLLAR": ud = ud  + parseFloat(slgfiDollarData[d].total);break;
					case "RG_DOLLAR": gl = gl + parseFloat(slgfiDollarData[d].total);break;
				}
			}
		}
		var tempCashier = {};
		tempCashier.cashier = "<div class='alignLeft'  collapseTarget='"+cdTarget+"'>"+cashierList[c].name+"</div>";
		if(isSLGFIConfirmed(cashierList[c].id)){
			tempCashier.tradVul = "<div class='alignRight'>"+numberWithCommas(tradVul.toFixed(2))+"</div>";
			tempCashier.ud = "<div class='alignRight'>"+numberWithCommas(ud.toFixed(2))+"</div>";
			tempCashier.gl = "<div class='alignRight'>"+numberWithCommas(gl.toFixed(2))+"</div>";
		} else {
			tempCashier.tradVul = "<div class='alignCenter'>-</div>";
			tempCashier.ud = "<div class='alignCenter'>-</div>";
			tempCashier.gl = "<div class='alignCenter'>-</div>";
		}
		returnSection.push(tempCashier);
	}
	return returnSection;
}

var slgfiDollarConsolidatedTotals = {tv: "0.00", gl: "0.00", ud: "0.00"};