var wfLayout = [{
	name: "Log Date",
	field: "logDate",
	width: "14%"
},{
	name: "DCR Date",
	field: "dcrDate",
	width: "14%"
},{
	name: "Customer Center",
	field: "customerCenter",
	width: "14%"
},{ name: "Process Status",
	field: "processStatus",
	width: "14%"	
},{
	name: "Action Details",
	field: "actionDetails",
	width: "14%"
},{
	name: "User Id",
	field: "userId",
	width: "14%"	
}];

var wfData = [];

function generateWFHistoryTable(){
	
	$(document).ready( function(){
		
		new TableContainer({
			divName : "wfHistoryDiv",
			nodeName : "wfHistoryNode",
			width : "100%",
			id : "wfHistoryTable",
			layout : wfLayout,
			tableClass: "balancingToolSummary",
			data: wfData
		}).startUp();
		
	});
	
}