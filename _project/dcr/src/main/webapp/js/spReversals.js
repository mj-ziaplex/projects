var spReversalLayout = [{
	name: "Date & Time",
	field: "date",
	width: "20%"
},{
	name: "Reason",
	field: "remarks",
	width: "40%"
},{
	name: "Currency",
	field: "currency",
	width: "10%"
},{
	name: "Amount",
	field: "amount",
	width: "30%"
}];

var spReversalData = [];

function generateSPReversalTable(){
	
	$(document).ready( function(){
		
		new TableContainer({
			divName : "spReversalsDiv",
			nodeName : "spReversalsNode",
			width : "800px",
			id : "spReversalsTable",
			layout : spReversalLayout,
			tableClass: "balancingToolSummary",
			data: spReversalData
		}).startUp();
		
	});
	
}