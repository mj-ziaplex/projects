tinyMCE.init({
        // General options
        mode : "textareas",
        theme : "advanced",
        
        theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,fontselect,fontsizeselect,forecolor",
        theme_advanced_buttons2 : "undo,redo,|,bullist,numlist",
        theme_advanced_toolbar_location : "bottom",
        theme_advanced_toolbar_align : "left"

});


function close_window() {
	if (confirm("Close Window?")) {
		close();
	}
}