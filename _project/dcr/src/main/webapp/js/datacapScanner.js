function setDatacapScanners(datacapScannerData) {
    $(document).ready(function() {
        destroyViewTable();

        var datacapScannerLabelLayout = [{name: "Datacap Scanners", field: "datacapScannerLabel", width: "100%"}];

        new TableContainer({
            divName: "datacapScannerDiv",
            nodeName: "datacapScannerLabelNode",
            width: "100%",
            id: "datacapScannerLabel",
            layout: datacapScannerLabelLayout,
            tableClass: "datacapScannerLabelBorderedTable"
        }).startUp();

        var datacapScannerTableLayout = [
            {name: "", field: "scannerIdChkbox", width: "2%"},
            {name: "PC ID", field: "scannerId", width: "10%"},
            {name: "Contact Name", field: "datacapUserName", width: "20%"},
            {name: "Branch Name", field: "branchName", width: "15%"},
            {name: "Created Date", field: "createdDate", width: "15%"}];

        new TableContainer({
            divName: "datacapScannerTableNode",
            nodeName: "datacapScannerNode",
            width: "100%",
            id: "datacapScannerTable",
            layout: datacapScannerTableLayout,
            tableClass: "datacapScannerBorderedTable"
        }).startUp();

        var datacapScannerTable = _globalTableContainer["datacapScannerTable"];
        datacapScannerTable.setData(datacapScannerData);
        datacapScannerTable.refreshTable();

        var datacapScanner = document.getElementById("datacapScannerTable");
        var options = {
            optionsForRows: [5, 10, 20, 30, 50],
            rowsPerPage: 20,
            firstArrow: (new Image()).src = "../images/firstBlue.gif",
            prevArrow: (new Image()).src = "../images/prevBlue.gif",
            lastArrow: (new Image()).src = "../images/lastBlue.gif",
            nextArrow: (new Image()).src = "../images/nextBlue.gif",
            topNav: false
        };
        $('#datacapScannerTable').tablePagination(options);
        sorttable.makeSortable(datacapScanner);
        autoSort(datacapScanner);
    });
}

function autoSort(datacapScannerTable) {
    $(document).ready(function() {
        var headers = datacapScannerTable.getElementsByTagName("th");
        for (var h = 0; h < headers.length; h++) {
            if (headers[h].innerHTML === "User ID") {
                headers[h].click();
            }
        }
        resetOddEvenRows(datacapScannerTable);
    });
}


function destroyViewTable() {
    $("#datacapScannerLabelNode").empty();
}

var userRemoveList = [];

function handleRemoveToggle(cb) {
    if (cb.checked) {
        userRemoveList.push(cb.rowInd);
    } else {
        var ctr = 0;
        for (user in userRemoveList) {
            if (userRemoveList[user] == cb.rowInd) {
                userRemoveList.splice(ctr, 1);
            }
            ctr++;
        }

    }
}

function doDeleteUser(action) {
    if (userRemoveList.length > 0) {
        var conf = confirm('Do you want to continue?');
        if (conf) {
            document.getElementById("pcNames").value = userRemoveList;
            $("#adminScannerOperation").attr("action", action);
            $("#adminScannerOperation").submit();
        }
    } else {
        alert('No user selected.');
    }
}

function submitSearchForm() {
    $("#searchScannerForm").submit();
}

function ajaxCall(hrefObj) {
    $(document).ready(function() {
        $.ajax({
            url: $(hrefObj).attr('editAction'),
            type: "POST",
            data: {
                userId: $(hrefObj).attr('acf2id')
            },
            success: function(data) {
                $("#addWmsUserNode").html(data);
            },
            error: function(xhr, status, errorThrown) {
                alert("Sorry, there was a problem!");
                alert("Error: " + errorThrown);
                alert("Status: " + status);
                alert(xhr);
            },
            complete: function(xhr, status) {
                alert("The request is complete!");
            }
        });
    }); // End ready function   
}
