///this is to fix problem with horizontal overflow scrollbar
window.onload = function () {
  // only apply to IE
  if (!/*@cc_on!@*/0) return;

  // find every element to test
  var all = document.getElementsByTagName('*'), i = all.length;

  // fast reverse loop
  while (i--) {
    // if the scrollWidth (the real width) is greater than
    // the visible width, then apply style changes
    if (all[i].scrollWidth > all[i].offsetWidth) {
      all[i].style['paddingBottom'] = '20px';
      all[i].style['overflowY'] = 'auto';
    }
  }
};
///

var cashierRecon = ["NEW","PENDING"];
var reviewAndApproval = ["For Review and Approval", "Awaiting Feedback"];
var ppaRecon = ["Verified", "Not Verified", "Approved for Reconciliation","Reqts Submitted","Reqts Not Submitted"];
var ppaAbeyance = ["For Verification","Awaiting Reqts"];
// Added for MR-WF-16-00034 - Random sampling for QA
var ppaQa = ["", "QA Reviewed"];

var dcrCashierId;
var ccName;
var ccId;
var dcrDateStr;
var formattedDcrDateStr;
var dcrId;
var dcrStatus;
var ccId;
var dcrDateStr;
var noteList = [];
var reconDataList = [];//changed from var reconList = []; 
var rowIndex = 0;
var commentCtr = 1;
var userRoles = [];
var cashierList = [];
var inactiveCashierList = [];
var inactiveId = [];
var commentList = [];
var coupleCommentList = [];
var consolidatedDataJSON = [];
var slfpiData = [];
var slgfiPesoData = [];
var slgfiDollarData = [];
var slamciPesoData = [];
var slamciDollarData = [];
var slocpiPesoData = [];
var slocpiDollarData = [];
var otherPesoData = [];
var otherDollarData = [];
var attachments = [];
var confirmations = [];
var uniqueCashier = [];
var slamcipConfirm = [];
var slamcidConfirm = [];
var slocpiConfirm = [];
var slgfiConfirm = [];
var slfpiConfirm = [];
var otherConfirm = [];
var role;
var confirmed = true;
var cashDepositSlips = [];
var chequeDepositSlips = [];
var validatedCashDepositSlips = [];
var validatedChequeDepositSlips = [];
var slocpiSnapshot = [];
var slamcipSnapshot = [];
var slamcidSnapshot = [];
var slfpiSnapshot = [];
var slgfiSnapshot = [];
var otherSnapshot = [];
var mdsReconciled = false;
var mdsProdTotals = ["totalMdsBpi","totalMdsBdo","totalMdsRcbc","totalMdsSbc","totalMdsCtb","totalMdsHsbc"];
var slamciPaymentSubTypeCount = 16; //this should be equal to the number of payment types for slamci peso and dollar combined
var cashierCtr = 0;
var userIdShortname = [];
// Added for MR-WF-16-00031 - PPA CheckBox
var ppaCpPesoActive = 0;
var ppaCpPesoTotal = 0;
var ppaCpUsdActive = 0;
var ppaCpUsdTotal = 0;
var ppaGfPesoActive = 0;
var ppaGfPesoTotal = 0;
var ppaGfUsdActive = 0;
var ppaGfUsdTotal = 0;
var ppaPnGafActive = 0;
var ppaPnGafTotal = 0;
var ppaOthPesoActive = 0;
var ppaOthPesoTotal = 0;

// Added for MR-WF-16-00034 - Random sampling for QA
var isPpaQa;

function exCoArrow(val){
	var imageChild = val.getElementsByTagName("img");
	var image = imageChild[0];
	var imgSrc = image.src;
	var fileName = imgSrc.substring(imgSrc.lastIndexOf("/")+1, imgSrc.search(".JPG"));
	
	if(fileName == "collapseArrow"){
		image.src = imgSrc.replace(fileName,"expandArrow");
	} else{
		image.src = imgSrc.replace(fileName,"collapseArrow");
	}
	
}

var balancingToolHeader = [ {
	name : "",
	field : "product",
	width : "15%"
},{
	name : "",
	field : "cashier",
	width : "25%"
},{
	name : "VUL/TRAD",
	field : "vulTrad",
	width : "7%"
},{
	name : "DS",
	field : "dsVT",
	width : "5%"
},{
	name : "VDS",
	field : "vdsVT",
	width : "5%"
},{
	name : "GROUP LIFE",
	field : "groupLife",
	width : "5%"
},{
	name : "DS",
	field : "dsGL",
	width : "5%"
},{
	name : "VDS",
	field : "vdsGL",
	width : "7%"
},{
	name : "Reconciled",
	field : "reconciled",
	width : "6%"
},{
	name : "CCM NOTES",
	field : "ccmNotes",
	width : "7%"
},{
	name : "CCQA NOTES",
	field : "ppaNotes",
	width : "7%"
},{
	name : "Findings",
	field : "findings",
	width : "6%"
}];

var balancingToolHeaderless = [ {
	name : "",
	field : "product",
	width : "15%"
},{
	name : "",
	field : "cashier",
	width : "25%"
},{
	name : "",
	field : "vulTrad",
	width : "7%"
},{
	name : "",
	field : "dsVT",
	width : "5%"
},{
	name : "",
	field : "vdsVT",
	width : "5%"
},{
	name : "",
	field : "groupLife",
	width : "5%"
},{
	name : "",
	field : "dsGL",
	width : "5%"
},{
	name : "",
	field : "vdsGL",
	width : "7%"
},{
	name : "",
	field : "reconciled",
	width : "6%"
},{
	name : "",
	field : "ccmNotes",
	width : "7%"
},{
	name : "",
	field : "ppaNotes",
	width : "7%"
},{
	name : "",
	field : "findings",
	width : "6%"
}];

var counterPaymentHeaderRow = [{product: "<div class='alignLeftRed'>Counter Payment</div>",cashier:"", vulTrad: "",dsVT: "",vdsVT:"",groupLife:"",dsGL:"",vdsGL:"",reconciled:"",ccmNotes:"",ppaNotes:"",findings:""}];
var nonCounterPaymentHeaderRow = [{product: "<div class='alignLeftRed'>NonCounter Payment</div>",cashier:"", vulTrad: "",dsVT: "",vdsVT:"",groupLife:"",dsGL:"",vdsGL:"",reconciled:"",ccmNotes:"",ppaNotes:"",findings:""}];
var pesoPCData = [{product: "<div class='alignLeft'>PESO CASH</div>",cashier:"", vulTrad: "",dsVT: "",vdsVT:"",groupLife:"",dsGL:"",vdsGL:"",reconciled:"",ccmNotes:"",ppaNotes:"",findings:""}];
var pesoCHKData = [{product: "<div class='alignLeft'>PESO CHECK</div>",cashier:"", vulTrad: "",dsVT: "",vdsVT:"",groupLife:"",dsGL:"",vdsGL:"",reconciled:"",ccmNotes:"",ppaNotes:"",findings:""}];
var pesoCardData = [{product: "<div class='alignLeft'>PESO CARD</div>",cashier:"", vulTrad: "",dsVT: "",vdsVT:"",groupLife:"",dsGL:"",vdsGL:"",reconciled:"",ccmNotes:"",ppaNotes:"",findings:""}];
var nonPostedData = [{product: "<div class='alignLeft'>NON-POSTED(from DTR)</div>",cashier:"", vulTrad: "",dsVT: "",vdsVT:"",groupLife:"",dsGL:"",vdsGL:"",reconciled:"",ccmNotes:"",ppaNotes:"",findings:""}];
var forManualPostingData = [{product: "<div class='alignLeft'>FOR MANUAL POSTING</div>",cashier:"", vulTrad: "",dsVT: "",vdsVT:"",groupLife:"",dsGL:"",vdsGL:"",reconciled:"",ccmNotes:"",ppaNotes:"",findings:""}];
var exemptionsData = [{product: "<div class='alignLeft'>EXEMPTIONS</div>",cashier:"", vulTrad: "",dsVT: "",vdsVT:"",groupLife:"",dsGL:"",vdsGL:"",reconciled:"",ccmNotes:"",ppaNotes:"",findings:""}];
var sessionTotalData = [{product: "<div class='alignLeft'>SESSION TOTAL</div>",cashier:"", vulTrad: "",dsVT: "",vdsVT:"",groupLife:"",dsGL:"",vdsGL:"",reconciled:"",ccmNotes:"",ppaNotes:"",findings:""}];
var collectionDetailsData = [{product: "<div class='alignLeft'>Collection Details</div>",cashier:"", vulTrad: "",dsVT: "",vdsVT:"",groupLife:"",dsGL:"",vdsGL:"",reconciled:"",ccmNotes:"",ppaNotes:"",findings:""}];

function generateBalancingToolSummary(tableDivId,summaryDivDetails,divDetails,summary,pesoCash,checkNC,checkC,pesoCard,nonPosted,forManualPosting,exemp,session,collection,tabLayout,tabLayoutNH){

$(document).ready(function(){
	
new TableContainer({
	divName : summaryDivDetails.divName,
	nodeName : summaryDivDetails.nodeName,
	width : "100%",
	id : summaryDivDetails.tableId,
	layout : tabLayout,
	tableClass: "balancingToolSummary",
	data: summary
}).startUp();

var summaryData = [];

appendJsons([summaryData,pesoPCData,nonCounterPaymentHeaderRow,pesoCash.pesoCashNCPDS,pesoCash.pesoCashNCPTT,counterPaymentHeaderRow,pesoCash.pesoCashCPC,pesoCash.pcTotal,pesoCHKData,nonCounterPaymentHeaderRow,checkNC.checkLocal,checkNC.checkRegional,checkNC.checkTelegraphicTransfer,checkNC.checkOnUs,checkNC.checkTotal,counterPaymentHeaderRow,checkC.checkLocal,checkC.checkRegional,checkC.checkTelegraphicTransfer,checkC.checkOnUs,checkC.checkTotal,pesoCardData,pesoCard.pesoCardPOSBPI,pesoCard.pesoCardPOSCTB,pesoCard.pesoCardTotal,nonPostedData,nonPosted.npNBI,nonPosted.npNBVUL,nonPosted.npVULR,nonPosted.npNC,nonPosted.npTotal,forManualPostingData,forManualPosting.fmpNP,forManualPosting.fmpW,forManualPosting.fmpTot,exemptionsData,exemp.exmptns,exemp.exmptnsTot,sessionTotalData,session.sessionTot,session.sessionGT,collectionDetailsData,collection.collectionDet]);

new TableContainer({
	divName : divDetails.divName,
	nodeName : divDetails.nodeName,
	width : "100%",
	id : divDetails.tableId,
	layout : tabLayoutNH,
	tableClass: "balancingToolSummary",
	data: summaryData
}).startUp();

var tableDiv = document.getElementById(tableDivId);

tableDiv.style.display = "none";

});

}

function showData(val){
	var nextDiv = val.nextSibling;
	if(nextDiv.style.display == "none"){
		nextDiv.style.display = "inline";
	} else{
		nextDiv.style.display = "none";
	}
}

$(document).ready(function(){
	var actionDivsLayout = [ {
		name : "",
		field : "launch",
		width : "50%"
	},{
		name : "",
		field : "response",
		width : "50%"
	}];
	
	var actionDivsData = [{launch:"<iframe id='iframe_a' scrolling='no' frameborder='0' name='iframe_a' src='../cashierSP/comments.html?dcrId="+dcrId+"'></iframe></div><div id='allCommentsPopUpDiv'><div id='allCommentsPopUpTableNode'><div id='allCommentsPopUpNode'></div></div></div>",response:"<div id='responseDiv'><div class='responseHeader'>Response</div><div id='responseNode'></div></div><div class='responseHeader'>Common Tools</div><div id='launchDiv'><div id='launchNode'></div>"}];
	
	new TableContainer({
		divName : "actionDivs",
		nodeName : "actionNode",
		width : "100%",
		id : "actionTable",
		layout : actionDivsLayout,
		tableClass: "spHeaderClass",
		data: actionDivsData
	}).startUp();
	
	var response = document.createElement("select");
	response.id = "response";
	response.style.width="100%";
	response.onchange = function() {callResolveCompleteButtonEnablingViaResponse(this)};
	
	if(jQuery.inArray("PPA",userRoles) != -1){
		role = "PPA";
		if(dcrStatus=="For Verification"){
				var tempOption = document.createElement("option");
				tempOption.innerHTML = "Verified";
				tempOption.value = "Verified";
				response.appendChild(tempOption);
				
				tempOption = document.createElement("option");
				tempOption.innerHTML = "Not Verified";
				tempOption.value = "Not Verified";
				response.appendChild(tempOption);
		} else if(dcrStatus=="Awaiting Reqts"){
				var tempOption = document.createElement("option");
				tempOption.innerHTML = "Reqts Submitted";
				tempOption.value = "Reqts Submitted";
				response.appendChild(tempOption);
				
				tempOption = document.createElement("option");
				tempOption.innerHTML = "Reqts Not Submitted";
				tempOption.value = "Reqts Not Submitted";
				response.appendChild(tempOption);
		} // Added for MR-WF-16-00034 - Random sampling for QA
        else if ("true" === isPpaQa) {
        	for(p in ppaQa){
        		var tempOption = document.createElement("option");
        		tempOption.innerHTML = ppaQa[p];
        		tempOption.value = ppaQa[p];
        		response.appendChild(tempOption);
        	}
        } else {
			for(p in PPA){
				var tempOption = document.createElement("option");
				tempOption.innerHTML = PPA[p];
				tempOption.value = PPA[p];
				response.appendChild(tempOption);
			}
		}
	} else if(jQuery.inArray("MANAGER",userRoles) != -1){
		role = "MANAGER";
		for(mn in MANAGER){
			var tempOption = document.createElement("option");
			tempOption.innerHTML = MANAGER[mn];
			tempOption.value = MANAGER[mn];
			response.appendChild(tempOption);
		} 
	} else if(jQuery.inArray("CASHIER",userRoles) != -1){
		role = "CASHIER";
		for(to in CASHIER){
			var tempOption = document.createElement("option");
			tempOption.innerHTML = CASHIER[to];
			tempOption.value = CASHIER[to];
			response.appendChild(tempOption);
		}
	} else if(jQuery.inArray("OTHER",userRoles) != -1){
		role = "OTHER";
	}
	
	var responseDiv = document.getElementById("responseDiv");
	responseDiv.appendChild(response);
	
	// Added for Audit finding for CCQA notes
    if (("Reconciled" === dcrStatus || "For QA Review" === dcrStatus || "QA Reviewed" === dcrStatus )
    		&& "true" !== isPpaQa) {
    	response.style.display = "none";
    	
    	var responseLabel = document.createElement("div");
        responseLabel.id = "responseLabel";
        responseLabel.style.textAlign = "left";
        responseLabel.style.fontSize = "12px";
        responseLabel.style.fontWeight = "bold";

        var tempOption = document.createElement("span");
        tempOption.innerHTML = dcrStatus;
        responseLabel.appendChild(tempOption);
        
        responseDiv.appendChild(responseLabel);
        
        $(".responseHeader").html("Status");
    }
	
	var spaDiv = document.createElement("div");
	spaDiv.innerHTML = "Step Processor Actions";
	spaDiv.id = "spaDiv";
	
	responseDiv.appendChild(spaDiv);
	
	var actionButtonDiv = document.createElement("div");
	responseDiv.appendChild(actionButtonDiv);
	
	var completeButton = document.createElement("button");
	completeButton.id = "completeButton";
	completeButton.value = "COMPLETE";
	completeButton.onclick = completeWorkItem;
	completeButton.style.width="120px";
	if(role == "PPA"){
		if(dcrStatus == "For Verification" || dcrStatus == "Awaiting Reqts"){
			completeButton.disabled = false;
		}else if(jQuery.inArray(dcrStatus,ppaRecon) == -1){
			completeButton.disabled = true;
		}
	} else if(role == "MANAGER"){
		if(dcrStatus!="For Review and Approval" && dcrStatus != "Awaiting Feedback"){
			completeButton.disabled = true;
		}
	}  else if(role == "CASHIER"){
		if(dcrStatus!="NEW" && dcrStatus!="PENDING"){
			completeButton.disabled = true;
		}
	}
	
	var saveButton = document.createElement("button");
	saveButton.id = "saveButton";
	saveButton.value = "SAVE";
	saveButton.style.width="120px";
	//pagka-save, call controller method to write in db
	saveButton.onclick = function() { callSaveStepProcData(); };
	
	var exitButton = document.createElement("button");
	exitButton.onclick = closeWindow;
	exitButton.value = "EXIT";
	exitButton.style.width="120px";
	
	actionButtonDiv.appendChild(completeButton);
	actionButtonDiv.appendChild(saveButton);
	actionButtonDiv.appendChild(exitButton);

	var launchNode = document.getElementById("launchNode");
	
	var breakLine = document.createElement("br");
	var emailButton = document.createElement("button");
	emailButton.value = "EMAIL";
	emailButton.onclick = openEmail;
	
	var addNoteButton = document.createElement("button");
	addNoteButton.value = "ADD NOTE";
	addNoteButton.onclick = openAddNote;
	
	var attachFileButton = document.createElement("button");
	attachFileButton.value = "ATTACH FILE";
	attachFileButton.onclick = openAttachFile;
	
	var candidateMatchButton = document.createElement("button");
	candidateMatchButton.value = "CANDIDATE MATCH";
	candidateMatchButton.id="candidateMatchButton";
	candidateMatchButton.onclick=openCandidateMatch;
	
	var errorTaggingButton = document.createElement("button");
	errorTaggingButton.id = "errorTaggingButton";
	errorTaggingButton.value = "ERROR TAGGING";
	errorTaggingButton.onclick = openErrorTag;
	
	var viewMDSButton = document.createElement("button");
	viewMDSButton.value = "VIEW MDS";
	viewMDSButton.onclick = openMDS;
	
	var workflowHistoryButton = document.createElement("button");
	workflowHistoryButton.value = "WF HISTORY";
	workflowHistoryButton.onclick = openWFHistory;
	
	var tempBr1 = document.createElement("br");
	var viewCommentButton = document.createElement("button");
	viewCommentButton.value = "VIEW COMMENTS";
	viewCommentButton.id="viewCommentButton";
	viewCommentButton.onclick=showViewCommentsPopUp;
	
	var addCommentButton = document.createElement("button");
	addCommentButton.value = "ADD COMMENT";
	addCommentButton.id="addCommentButton";
	addCommentButton.onclick=showAddCommentPopUp;
	
	launchNode.appendChild(workflowHistoryButton);
	launchNode.appendChild(emailButton);
	launchNode.appendChild(addNoteButton);
	launchNode.appendChild(attachFileButton);
	launchNode.appendChild(candidateMatchButton);
	launchNode.appendChild(tempBr1);
	launchNode.appendChild(viewMDSButton);
	launchNode.appendChild(errorTaggingButton);
	launchNode.appendChild(viewCommentButton);
	launchNode.appendChild(addCommentButton);
});

var callResolveCompleteButtonEnablingViaResponse = function resolveCompleteButtonEnablingViaResponse(obj){
	var responseText = obj.options[obj.selectedIndex].text;
	//alert('response selected: '+responseText);
	if(responseText!='PPA Reconciled'){
		document.getElementById('completeButton').disabled = false;
	}else{
		document.getElementById('completeButton').disabled = true;
		resolveCompleteButtonEnablingViaReconCheckboxes();
	}
}

function closeWindow(){
	
	window.close();
	
}

function compareTotalsImg(cashier,consolidated){
	if(consolidated != cashier){
		return "<img src='../images/redx.JPG'/>";
	} else if(parseFloat(consolidated) == 0 && parseFloat(cashier) == 0){
		return "";
	}
	return "<img src='../images/greencheck.jpg'/>";
}

function getReconcilerName(ind, isWholeZero, isMDS){
	
	if(isWholeZero){
		return "-";
	}
	
	for(reconData in reconDataList){
		var reconList = reconDataList[reconData].reconList;
		for(reconCnt in reconList){
			if(reconList[reconCnt] == ind){
				return reconDataList[reconData].userId;
			}
		}
	}
	
	if(isMDS && mdsReconciled){
		return "-";
	} else if (isMDS && !mdsReconciled){
		return "X";
	}
	
	return "X";
}

function getReconciledDate(ind){
	for(reconData in reconDataList){
		var reconList = reconDataList[reconData].reconList;
		for(reconCnt in reconList){
			if(reconList[reconCnt] == ind){
				return reconDataList[reconData].dateTime; 
			}
		}
	}
	return "-";
}

// Modified for MR-WF-16-00031 - PPA CheckBox
// - Code clean-up and add new code which is a concatenation of company code and currency
// - Added variables as a counter for PPA checkbox count
function getReconCheckbox(ind, isWholeRowZero, isMDS, compCodeCur) {
    // Get Total Count of checkbox regardless of condition below
    countCheckBox(compCodeCur, "Total");
    
    if (isWholeRowZero) {
        // Addition of initial value/ticked PPA CB
        countCheckBox(compCodeCur, "Active");
        return '<input type="checkbox" class="reconChkbox" rowInd=' + ind + ' checked="checked" disabled="true" />';
    }
    if (isMDS && mdsReconciled) {
        // Addition of initial value/ticked PPA CB
        countCheckBox(compCodeCur, "Active");
        return "<input type='checkbox' class='reconChkbox' rowInd='" + ind + "' checked='checked' disabled='true' />";
    } else if (isMDS && !mdsReconciled) {
        return "<input type='checkbox' class='reconChkbox' rowInd='" + ind + "' disabled='true' />";
    } else {
        for (reconData in reconDataList) {
            var reconList = reconDataList[reconData].reconList;
            for (reconCnt in reconList) {
                if (reconList[reconCnt] == ind) {
                    // Addition of initial value/ticked PPA CB
                    countCheckBox(compCodeCur, "Active");
                    
                    // Added for MR-WF-16-00034 - Random sampling for QA
                    var tmpChkBox = "<input type='checkbox' class='reconChkbox' rowInd='" + ind + "' checked='checked' onclick='handleReconToggle(this)' checkBoxCat='" + compCodeCur + "' />";
                    if("true" === isPpaQa) {
                        tmpChkBox = "<input type='checkbox' class='reconChkbox' rowInd='" + ind + "' checked='checked' disabled='true' />";
                    }
                    
                    return tmpChkBox;
                }
            }
        }
    }
    
    // Added for MR-WF-16-00034 - Random sampling for QA
    var tmpChkBox = "<input type='checkbox' class='reconChkbox' rowInd='" + ind + "' onclick='handleReconToggle(this)' checkBoxCat='" + compCodeCur + "' />";
    if("true" === isPpaQa) {
        tmpChkBox = "<input type='checkbox' class='reconChkbox' rowInd='" + ind + "' disabled='true' />";
    }
    
    return tmpChkBox;
}

// Modified for MR-WF-16-00031 - PPA CheckBox
// -Code clean-up and handle handling of ActiveCB count
// -Addition of parameter to check which activeCount to update
function handleReconToggle(cb) {
    var cbCat = cb.checkBoxCat;
    if (cb.checked) {
        var tempReconData = {};
        var reconList = [];
        reconList.push(cb.rowInd);//orig line
        tempReconData.reconList = reconList;
        reconDataList.push(tempReconData);
        // Will refesh PPA CB count and display
        updateActiveCount(cbCat, "add");
    } else {
        for (reconData in reconDataList) {
            var reconList = reconDataList[reconData].reconList;
            for (reconCnt in reconList) {
                if (reconList[reconCnt] === cb.rowInd) {
                    delete reconList[reconCnt];
                }
            }
        }
        // Will refesh PPA CB count and display
        updateActiveCount(cbCat, "sub");
    }
    resolveCompleteButtonEnablingViaReconCheckboxes();
}

// Function to update active count of PPA checkbox upon ticking
function updateActiveCount(CBCat, refreshCnt) {
    var curCount = 0;
    var totalCount = 0;
    var cbCatId = "";
    // Set ID based on passed parameter
    if ("CPPHP" === CBCat) {
        cbCatId = $("input[id*=cpPhpActive]").attr("id");
        totalCount = ppaCpPesoTotal;
    } else if ("CPUSD" === CBCat) {
        cbCatId = $("input[id*=cpUsdActive]").attr("id");
        totalCount = ppaCpUsdTotal;
    } else if ("GFPHP" === CBCat) {
        cbCatId = $("input[id*=gfPhpActive]").attr("id");
        totalCount = ppaGfPesoTotal;
    } else if ("GFUSD" === CBCat) {
        cbCatId = $("input[id*=gfUsdActive]").attr("id");
        totalCount = ppaGfUsdTotal;
    } else if ("PNGAF" === CBCat) {
        cbCatId = $("input[id*=pnGafActive]").attr("id");
        totalCount = ppaPnGafTotal;
    } else if ("OTH" === CBCat) {
        cbCatId = $("input[id*=othActive]").attr("id");
        totalCount = ppaOthPesoTotal;
    }
    
    // Add a value of 1 based on fectched ID and update Active Count
    if ("add" === refreshCnt){
        curCount = addCount($("#" + cbCatId).val());
    } else {
        curCount = subtractCount($("#" + cbCatId).val());
    }
    $("#" + cbCatId).val(curCount);
    $("#" + cbCatId).parent().find("div").text(curCount + "/" + totalCount);
}

// Function to be used to compute checkbox count upon initia load of page
function countCheckBox(compCodeCur, activeOrTotal) {
    if ("CPPHP" === compCodeCur && "Total" === activeOrTotal) {
        ppaCpPesoTotal = addCount(ppaCpPesoTotal);
    } else if ("CPPHP" === compCodeCur && "Active" === activeOrTotal) {
        ppaCpPesoActive = addCount(ppaCpPesoActive);
    } else if ("CPUSD" === compCodeCur && "Total" === activeOrTotal) {
        ppaCpUsdTotal = addCount(ppaCpUsdTotal);
    } else if ("CPUSD" === compCodeCur && "Active" === activeOrTotal) {
        ppaCpUsdActive = addCount(ppaCpUsdActive);
    } else if ("GFPHP" === compCodeCur && "Total" === activeOrTotal) {
        ppaGfPesoTotal = addCount(ppaGfPesoTotal);
    } else if ("GFPHP" === compCodeCur && "Active" === activeOrTotal) {
        ppaGfPesoActive = addCount(ppaGfPesoActive);
    } else if ("GFUSD" === compCodeCur && "Total" === activeOrTotal) {
        ppaGfUsdTotal = addCount(ppaGfUsdTotal);
    } else if ("GFUSD" === compCodeCur && "Active" === activeOrTotal) {
        ppaGfUsdActive = addCount(ppaGfUsdActive);
    } else if ("PNGAF" === compCodeCur && "Total" === activeOrTotal) {
        ppaPnGafTotal = addCount(ppaPnGafTotal);
    } else if ("PNGAF" === compCodeCur && "Active" === activeOrTotal) {
        ppaPnGafActive = addCount(ppaPnGafActive);
    } else if ("OTH" === compCodeCur && "Total" === activeOrTotal) {
        ppaOthPesoTotal = addCount(ppaOthPesoTotal);
    } else if ("OTH" === compCodeCur && "Active" === activeOrTotal) {
        ppaOthPesoActive = addCount(ppaOthPesoActive);
    }
}

function resolveCompleteButtonEnablingViaReconCheckboxes(){
	
	var responseValue = document.getElementById("response").value;
	//alert('responseValue: '+responseValue);

	//only check status of recon checkboxes if response selected is PPA Reconciled
	//if(responseDD.value=='PPA Reconciled'){
	if(responseValue==='PPA Reconciled'){
		var isAllChecked = false;
	    if ($('.reconChkbox:checked').length === $('.reconChkbox').length) {
	    	//alert('all checked');
	    	isAllChecked = true;
	    }else{
	    	//alert('not all checked');
	    }
		
		if(isAllChecked){
			document.getElementById('completeButton').disabled = false;
		}else{
			document.getElementById('completeButton').disabled = true;
		}
	}
}

function getNote(ind, noteType){
	var returnNote = "";
	for(noteCnt in noteList){
		//alert('noteCnt check: '+noteCnt);//andame
		if(noteList[noteCnt].rowNum == ind && noteList[noteCnt].type == noteType){
			returnNote = "<a href='#' id='noteId"+ind+noteType+"' rowInd='"+ind+"' noteType='"+noteType+"' onclick='showNoteForm(this);' >View</a>";
			return returnNote;
		}
	}
	if(noteType == "CCM"){
		if(jQuery.inArray("MANAGER",userRoles) != -1){
			returnNote = "<a href='#' id='noteId"+ind+noteType+"' rowInd='"+ind+"' noteType='"+noteType+"' onclick='showNoteForm(this);' >Add</a>";
		} else {
			returnNote = "-";
		}
	}else if(noteType == "PPA" || noteType == "FINDINGS"){
        // Modified for MR-WF-16-00034 - Random sampling for QA
		if(jQuery.inArray("PPA",userRoles) != -1 && "true" != isPpaQa){
			returnNote = "<a href='#' id='noteId"+ind+noteType+"' rowInd='"+ind+"' noteType='"+noteType+"' onclick='showNoteForm(this);' >Add</a>";
		} else {
			returnNote = "-";
		}
	}
	return returnNote;
}

var onEditCallback = function(remaining) {
    $(this).siblings('.charRemaining').text("Characters remaining: " + remaining);
    if (remaining > 0) {
        $(this).css('background-color', 'white');
    }
};

var onLimitCallback = function() {
    $(this).css('background-color', 'red');
};

function showNoteForm(val) {
    document.getElementById("spNoteForm").reset();
    var isContentToBeEdited = false;
    var noteCntToDeleteIfEdited;
    for (noteCnt in noteList) {
        if (noteList[noteCnt].rowNum === val.rowInd && noteList[noteCnt].type === val.noteType) {
            if (val.content !== null || val.content !== '') {
                isContentToBeEdited = true;
                document.getElementById("spNoteContent").value = noteList[noteCnt].content;
                noteCntToDeleteIfEdited = noteCnt;
            } else {
                document.getElementById("spNoteContent").value = '';
            }
        }
    }

    //pagka-add, dont call controller method to write in db, add only to noteList array
    $(function() {
        $("#spNoteDiv").dialog({
            autoOpen: true,
            height: "auto",
            minHeight: 580,
            width: 540,
            modal: true,
            closeOnEscape: true,
            draggable: false,
            resizable: false,
            dialogClass: "noteFormClass",
            title: "Add/View Note",
            buttons: [{
                    id: 'addSPNoteBtn',
                    text: 'Add',
                    click: function() {
                        var contentEntered = document.getElementById("spNoteContent").value;
                        var currentLink = document.getElementById("noteId" + val.rowInd + val.noteType);
                        if (contentEntered === null || $.trim(contentEntered) === '') {
                            alert('Remarks area needs to be filled');
                        } else {
                            if (isContentToBeEdited) {
                                delete noteList[noteCntToDeleteIfEdited];
                            }
                            var inputNote = {};
                            inputNote.rowNum = val.rowInd;
                            inputNote.type = val.noteType;
                            inputNote.content = contentEntered;
                            noteList.push(inputNote);
                            currentLink.innerHTML = 'View';
                            $(this).dialog('close');
                        }
                    }
                },
                {
                    id: 'cancelSPNoteBtn',
                    text: 'Cancel',
                    click: function() {$(this).dialog('close'); }
                }
            ]
        }).dialog('widget').position({my: 'center', at: 'center', of: $(document.getElementById("mainTemplateBodyId"))});
    });

    $('textarea[maxlength]').limitMaxlength({
        onEdit: onEditCallback,
        onLimit: onLimitCallback
    });

    // Added for Audit finding for CCQA notes
    if ("Reconciled" === dcrStatus) {
        $("#addSPNoteBtn").hide();
    } else {
    	document.getElementById("addSPNoteBtn").value = 'Add';
        if (isContentToBeEdited) {
            document.getElementById("addSPNoteBtn").value = 'Edit';
        }
    }
    document.getElementById("cancelSPNoteBtn").value = 'Cancel';
}


var callSaveStepProcData = function saveStepProcData(){
	//create this dynamically, before passing to form when calling controller 
	//for(noteCnt in noteList){ //loop sample
	//<input type="hidden" name="noteForms[noteCnt].rowNum" value="noteList[noteCnt].rowNum"/>
	//<input type="hidden" name="noteForms[noteCnt].noteType" value="noteList[noteCnt].type"/>
	//<input type="hidden" name="noteForms[noteCnt].noteContent" value="noteList[noteCnt].content"/>
	//}
	
	var hiddenForm = document.getElementById("hiddenForm");
	for(noteCnt in noteList){
		if(noteList[noteCnt].content!=null || noteList[noteCnt].content!=''){
			//alert(noteCnt);
			var hiddenRowNum = document.createElement("input");
			hiddenRowNum.type = 'hidden';
			hiddenRowNum.name = 'noteForms['+noteCnt+'].rowNum';
			hiddenRowNum.value = noteList[noteCnt].rowNum;
			
			var hiddenNoteType = document.createElement("input");
			hiddenNoteType.type = 'hidden';
			hiddenNoteType.name = 'noteForms['+noteCnt+'].noteType';
			hiddenNoteType.value = noteList[noteCnt].type;
			
			var hiddenNoteContent = document.createElement("input");
			hiddenNoteContent.type = 'hidden';
			hiddenNoteContent.name = 'noteForms['+noteCnt+'].noteContent';
			hiddenNoteContent.value = noteList[noteCnt].content;
			
			hiddenForm.appendChild(hiddenRowNum);
			hiddenForm.appendChild(hiddenNoteType);
			hiddenForm.appendChild(hiddenNoteContent);
		}
	}
	
	var hiddenReconRownums = document.createElement("input");
	hiddenReconRownums.type = 'hidden';
	hiddenReconRownums.name = 'reconRownums';
	var reconRownumsCSV = '';
	for(reconData in reconDataList){
		var reconList = reconDataList[reconData].reconList;
		for(reconCnt in reconList){
			reconRownumsCSV = reconRownumsCSV + reconList[reconCnt] + ',';
		}
	}
	if(reconRownumsCSV != ''){
		reconRownumsCSV = reconRownumsCSV.substring(0, reconRownumsCSV.length - 1);//clip last ',' char
	}
	hiddenReconRownums.value = reconRownumsCSV;
	hiddenForm.appendChild(hiddenReconRownums);
	
	hiddenForm.submit();
};


var showViewCommentsPopUp = function() {
	$(function() {
		var popupTable = _globalTableContainer["popupTable"];
		if(popupTable != null){
			popupTable.destroyTable();
		}
		$( "#allCommentsPopUpTableNode" ).dialog({
			autoOpen: true,
			height: 600,
			width: 828,
			modal: true,
			closeOnEscape: true,
			draggable: false,
			resizable: false,
			dialogClass: "commentTableClass",
			title: "View Comments"
		}).dialog('widget').position({ my: 'center', at: 'center', of: $(document.getElementById("mainTemplateBodyId")) });
		
		setDataForPopUp(commentList);
	});
};

function setDataForPopUp(commentData){
	$(document).ready(function(){
		
		new TableContainer({
			divName : "allCommentsPopUpTableNode",
			nodeName : "allCommentsPopUpNode",
			width : "100%",
			id : "popupTable",
			layout : commentLayout,
			tableClass : "commentBorderedTable"
		}).startUp();
		
		var popupTable = _globalTableContainer["popupTable"];
		
		popupTable.setData(commentData);
		popupTable.refreshTable();

	});
}

var commentLayout = [ {
	name : "User",
	field : "acf2id",
	width : "10%"
},{
	name : "Date and Time",
	field : "datePostedFormatted",
	width : "20%"
},{
	name : "Remark",
	field : "remarks",
	width : "70%"
}];

var showAddCommentPopUp = function() {
	document.getElementById("addCommentForm").reset();
	$(function() {
		$( "#addCommentDiv" ).dialog({
			autoOpen: true,
			height: "auto",
			minHeight: 650,
			width: 540,
			modal: true,
			closeOnEscape: true,
			draggable: false,
			resizable: false,
			dialogClass: "commentFormClass",
			title: "Add Comment"

		}).dialog('widget').position({ my: 'center', at: 'center', of: $(document.getElementById("mainTemplateBodyId")) });
	});

	$('textarea[maxlength]').limitMaxlength({
		onEdit: onEditCallback,
		onLimit: onLimitCallback
	});

	if(jQuery.inArray(dcrStatus,cashierRecon) == -1){
		var dcrCashierId = document.getElementById("dcrCashierId");
		var sendLable = document.getElementById("sendLabel");
		dcrCashierId.style.visibility = "hidden";
		sendLable.style.visibility = "hidden";
	}
	
};

function closeAddComment(){
	$("#addCommentDiv").dialog('close');
}

function submitComment(){

	var commentText = document.getElementById("commentRemarks");
	if(commentText.value.length == 0){
		alert("No remarks");
		return false;
	}
	var commentDropDown = document.getElementById("dcrCashierId");
	var commentForm = document.getElementById("addCommentForm");
	commentForm.submit();
	closeAddComment();
}


function completeWorkItem(){
	var formDcrId = document.getElementById("dcrIdComplete");
	var formStepProcResponse = document.getElementById("stepProcResponse");
	var formFromSearch = document.getElementById("formFromSearch");
	var responseDD = document.getElementById("response");
	var completeForm = document.getElementById("spCompleteForm");
	
	
	
	for(noteCnt in noteList){
		if(noteList[noteCnt].content!=null || noteList[noteCnt].content!=''){
			var hiddenRowNum = document.createElement("input");
			hiddenRowNum.type = 'hidden';
			hiddenRowNum.name = 'noteList['+noteCnt+'].rowNum';
			hiddenRowNum.value = noteList[noteCnt].rowNum;
			
			var hiddenNoteType = document.createElement("input");
			hiddenNoteType.type = 'hidden';
			hiddenNoteType.name = 'noteList['+noteCnt+'].noteType';
			hiddenNoteType.value = noteList[noteCnt].type;
			
			var hiddenNoteContent = document.createElement("input");
			hiddenNoteContent.type = 'hidden';
			hiddenNoteContent.name = 'noteList['+noteCnt+'].noteContent';
			hiddenNoteContent.value = noteList[noteCnt].content;
			
			completeForm.appendChild(hiddenRowNum);
			completeForm.appendChild(hiddenNoteType);
			completeForm.appendChild(hiddenNoteContent);
		}
	}
	
	// Added for IM00331115 (21APR2017 Release)
	var reconRownumsCSV = '';
	for(reconData in reconDataList){
		var reconList = reconDataList[reconData].reconList;
		for(reconCnt in reconList){
			reconRownumsCSV = reconRownumsCSV + reconList[reconCnt] + ',';
		}
	}
	if(reconRownumsCSV != ''){
		reconRownumsCSV = reconRownumsCSV.substring(0, reconRownumsCSV.length - 1);//clip last ',' char
	}
	$("#reconRownums").val(reconRownumsCSV);
	
	formDcrId.value = dcrId;
	formStepProcResponse.value = responseDD.value;
	formFromSearch.value = false;

	completeForm.submit();	
}

function getAttachments(cashierId){
	var returnAttachments = [];
	for(a in attachments){
		if(attachments[a].cashierId == cashierId){
			var dummyAttachment = {};
			dummyAttachment.name = "<a target='_blank' href='../"+attachments[a].fileUrl+"'>"+attachments[a].filename+"</a>";
			dummyAttachment.desc = attachments[a].description;
			dummyAttachment.uploadDate = attachments[a].uploadDate;
			returnAttachments.push(dummyAttachment);
		}
	}
	return returnAttachments;
}

var emailWindow = null;
var tempEmailWindow = null;

function closeEmailWindow(){
	if(emailWindow != null || emailWindow != tempEmailWindow){
		emailWindow.close();
	}
}
function openEmail(){
	closeEmailWindow();
	var subject = ccName;
	emailWindow = window.open("../email/create.html?dcrId="+dcrId+"&fromErrorTag=false&subject="+subject, "_blank", "width=600,height=500,resizable=yes,status=yes,scrollbars=yes");
	tempEmailWindow = emailWindow;
}

var mdsWindow = null;
var tempMDSWindow = null;

function closeMDSWindow(){
	if(mdsWindow != null || mdsWindow != tempMDSWindow){
		mdsWindow.close();
	}
}

function openMDS(){
	closeMDSWindow();
    // Modified for MR-WF-16-00034 - Random sampling for QA
	mdsWindow = window.open("../ppamds/index.html?ccId="+ccId+"&dcrDateStr="+dcrDateStr+"&dcrId="+dcrId+"&isPpaQa="+isPpaQa, "_blank", "width=1080,height=550,top=100,left=100,resizable=yes,status=yes,scrollbars=yes");
	tempMDSWindow = mdsWindow;
}

var errorTagWindow = null;
var tempErrorTagWindow = null;

function closeErrorTag(){
	if(errorTagWindow != null || errorTagWindow != tempErrorTagWindow){
		errorTagWindow.close();
	}
}
function openErrorTag(){
	closeErrorTag();
	
	//build and pass comma-separated values of cashier ids from cashierList
	var concatCashierNameList = '';
	for(cashier in cashierList){
		//alert('cashierList[cashier].id: '+cashierList[cashier].id);
		//alert('cashierList[cashier].name: '+cashierList[cashier].name);
		//concatCashierNameList = concatCashierNameList + cashierList[cashier].name + '~';
		concatCashierNameList = concatCashierNameList + cashierList[cashier].id + '~';
	}
	if(concatCashierNameList != ''){
		concatCashierNameList = concatCashierNameList.substring(0, concatCashierNameList.length - 1);//clip last '~' char
	}
	//alert('final concatenated cashierIdList: '+concatCashierNameList);
	errorTagWindow = window.open("../errorTag/index.html?dcrId="+dcrId+"&dcrStatus="+dcrStatus+"&concatCashierNameList="+concatCashierNameList+"&dcrDate="+formattedDcrDateStr+"&ccName="+ccName, "_blank", "width=820,height=550,resizable=yes,status=yes,scrollbars=yes");
	//errorTagWindow = window.open("../errorTag/index.html?dcrId="+dcrId, "_blank", "width=820,height=550,resizable=yes,status=yes,scrollbars=yes");
	tempErrorTagWindow = errorTagWindow;
}

var attachFileWindow = null;
var tempAttachFileWindow = null;

function closeAttachFileWindow(){
	if(attachFileWindow != null || attachFileWindow != tempAttachFileWindow){
		attachFileWindow.close();
	}
}
function openAttachFile(){
	closeAttachFileWindow();
	attachFileWindow = window.open("../attachSP/attach.html?reloadSP=false&dcrId="+dcrId, "_blank", "width=600,height=500,resizable=yes,status=yes,scrollbars=yes");
	tempAttachFileWindow = attachFileWindow;
}

var wfHistoryWindow = null;
var tempWFHistoryWindow = null;

function closeWFHistoryWindow(){
	if(wfHistoryWindow != null || wfHistoryWindow != tempWFHistoryWindow){
		wfHistoryWindow.close();
	}
}
function openWFHistory(){
	closeWFHistoryWindow();
	wfHistoryWindow = window.open("../wfHistory/index.html?dcrId="+dcrId, "_blank", "width=1000,height=500,resizable=yes,status=yes,scrollbars=yes");
	tempWFHistoryWindow = wfHistoryWindow;
}

var addNoteWindow = null;
var tempAddNoteWindow = null;

function closeAddNoteWindow(){
	if(addNoteWindow != null || addNoteWindow != tempAddNoteWindow){
		addNoteWindow.close();
	}
}
function openAddNote(){
	closeAddNoteWindow();
	addNoteWindow = window.open("../addNoteSP/create.html?reloadSP=false&dcrId="+dcrId, "_blank", "width=650,height=550,resizable=yes,status=yes,scrollbars=yes");
	tempAddNoteWindow = addNoteWindow;
}

var candidateMatchWindow = null;
var tempCandidateMatchWindow = null;

function closeCandidateMatchWindow(){
	if(candidateMatchWindow != null || candidateMatchWindow != tempCandidateMatchWindow){
		candidateMatchWindow.close();
	}
}
function openCandidateMatch(){
	closeCandidateMatchWindow();
	candidateMatchWindow = window.open("../candidateMatchSP/index.html?dcrDate="+formattedDcrDateStr+"&ccId="+ccId+"&ccName="+ccName+"&dcrDateStr="+collectionsDate+"&dcrId="+dcrId, "_blank", "width=650,height=550,resizable=yes,status=yes,scrollbars=yes");
	tempCandidateMatchWindow = candidateMatchWindow;
}

function generateSnapshotSection(snapshotList){
	var returnSection = [];
	if(snapshotList.length > 0){
		returnSection.push({product: "<div class='alignLeft'>Collection Details</div>"});
	}
	for(x in snapshotList){
		var tempRow = {};
		tempRow.cashier = "<a target='_blank' href='../" + snapshotList[x].fileUrl + "'>"+snapshotList[x].filename +"</a>";
		tempRow.product = snapshotList[x].timestamp;
		returnSection.push(tempRow);
	}
	return returnSection;
}

function generateInactiveCashierSection(){
	
	$(document).ready(function(){
	var icListLayout = [{
		name: "Cashier ID",
		field: "id",
		width: "50%"
	},{
		name: "Cashier Name",
		field: "name",
		width: "50%"
	}];

	new TableContainer({
		divName : "icList",
		nodeName : "icListNode",
		width : "100%",
		id : "icTable",
		layout : icListLayout,
		tableClass: "icHeaderClass",
		data: inactiveCashierList
	}).startUp();
	
	var icListDiv = document.getElementById("icList");

	icListDiv.style.display = "none";
	});
	
}

function isSLOCPIConfirmed(cashierId){
	var sl = 0;
	for(sl in slocpiConfirm){
		if(slocpiConfirm[sl].cashierId == cashierId){
			return slocpiConfirm[sl].confirmed;
		}
	}
}

function isSLGFIConfirmed(cashierId){
	var sl = 0;
	for(sl in slgfiConfirm){
		if(slgfiConfirm[sl].cashierId == cashierId){
			return slgfiConfirm[sl].confirmed;
		}
	}
}

function isSLAMCIPConfirmed(cashierId){
	var sl = 0;
	for(sl in slamcipConfirm){
		if(slamcipConfirm[sl].cashierId == cashierId){
			return slamcipConfirm[sl].confirmed;
		}
	}
}

function isSLAMCIDConfirmed(cashierId){
	var sl = 0;
	for(sl in slamcidConfirm){
		if(slamcidConfirm[sl].cashierId == cashierId){
			return slamcidConfirm[sl].confirmed;
		}
	}
}

function isSLFPIConfirmed(cashierId){
	var sl = 0;
	for(sl in slfpiConfirm){
		if(slfpiConfirm[sl].cashierId == cashierId){
			return slfpiConfirm[sl].confirmed;
		}
	}
}

function isOTHERConfirmed(cashierId){
	var sl = 0;
	for(sl in otherConfirm){
		if(otherConfirm[sl].cashierId == cashierId){
			return otherConfirm[sl].confirmed;
		}
	}
}

function getShortName(key){
	for(x in userIdShortname){
		if(key == userIdShortname[x].key){
			return userIdShortname[x].shortName;
		}
	}
	return "";
}


function reloadMainWindow(){
	window.close();
	window.opener.location.reload();
}

var excludeFromCashierTotal = ["totalNewBusinessIndividual","totalNewBusinessVariable","totalIndividualRenewal","totalVariableRenewal","totalNonCashFromDTR","totalNonPolicy","totalWorksiteNonPosted","sessionTotal","reversalProduct"];
var excludeFromReconCheckbox = ["totalNewBusinessIndividual","totalNewBusinessVariable","totalIndividualRenewal","totalVariableRenewal","totalNonCashFromDTR","totalNonPolicy","totalWorksiteNonPosted","reversalProduct","sessionTotal","totalNonCash"];