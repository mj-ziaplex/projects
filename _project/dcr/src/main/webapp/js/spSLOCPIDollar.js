var slocpiDollarColumns = [{
	name : "",
	field : "product",
	width : "15%"
},{
	name : "",
	field : "cashier",
	width : "22%"
},{
	name : "TRAD/VUL ",
	field : "tradVul",
	width : "10%"
},{
	name : "DS",
	field : "dsTV",
	width : "6%"
},{
	name : "VDS",
	field : "vdsTV",
	width : "7%"
},{
	name : "CCM NOTES",
	field : "ccmNotes",
	width : "8%"
},{
	// Modified for MR-WF-16-00036 - Change PPA notes to CCQA notes
	name : "CCQA NOTES",
	field : "ppaNotes",
	width : "8%"
},{
	name : "Findings",
	field : "findings",
	width : "8%"
}	
];

var slocpiDollarColumnsNH = [{
	name : "",
	field : "product",
	width : "15%"
},{
	name : "",
	field : "cashier",
	width : "22%"
},{
	name : "",
	field : "tradVul",
	width : "10%"
},{
	name : "",
	field : "dsTV",
	width : "6%"
},{
	name : "",
	field : "vdsTV",
	width : "7%"
},{
	name : "",
	field : "ccmNotes",
	width : "8%"
},{
	name : "",
	field : "ppaNotes",
	width : "8%"
},{
	name : "",
	field : "findings",
	width : "8%"
}	
];

function generateSLOCPIDollarSection(slocpiDollarTableDivId,slocpiDollarDivDetails,slocpiDollarSummaryDivDetails){
	$(document).ready( function(){
			
	mergeSLOCPIDollarCashierProductTypes();
	
	var dollarCashHeader = [{product: "<div class='alignLeft' cdTarget='slocpiDollarCash' onclick='collapseDivision(this)'>DOLLAR CASH</div>",cashier:"", tradVul: "",dsTV: "",vdsTV:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var cashNonCounterData = generateSLOCPIDollarSectionData("totalCashNonCounter","slocpiDollarCash");
	
	var cncTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > Non Counter</div>", tradVul: "<div class='numFormat'>"+cashNonCounterData.cashierTotals.tv+"</div>",dsTV: "",vdsTV:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var cashCounterData = generateSLOCPIDollarSectionData("totalCashCounter","slocpiDollarCash");
	
	var ccTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > Counter</div>", tradVul: "<div class='numFormat'>"+cashCounterData.cashierTotals.tv+"</div>",dsTV: "",vdsTV:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var dollarCashTotals = getSLOCPIDollarAreaTotal([cashNonCounterData,cashCounterData]);
	
	var areaCashTotal = [{product: "<div class='totalRow'>&nbsp;</div>",cashier:"<div class='totalRow'><div class='alignRight'>TOTAL CASH</div></div>", tradVul: "<div class='totalRow'><div class='numFormat'>"+dollarCashTotals.tv+"</div></div>",dsTV: "<div class='totalRow'>&nbsp;</div>",vdsTV:"<div class='totalRow'>&nbsp;</div>",ccmNotes:"<div class='totalRow'>&nbsp;</div>",ppaNotes:"<div class='totalRow'>&nbsp;</div>",findings:"<div class='totalRow'>&nbsp;</div>"}];
	
	var dollarCheckHeader = [{product: "<div class='alignLeft' cdTarget='slocpiDollarCheck' onclick='collapseDivision(this)'>DOLLAR CHECK</div>",cashier:"", tradVul: "",dsTV: "",vdsTV:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var usCheckInManilaData = generateSLOCPIDollarSectionData("totalUsCheckInManila","slocpiDollarCheck");
	
	var ucimTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > US Cheque drawn in Manila</div>", tradVul: "<div class='numFormat'>"+usCheckInManilaData.cashierTotals.tv+"</div>",dsTV: "",vdsTV:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var usCheckOutPhData = generateSLOCPIDollarSectionData("totalUsCheckOutPh","slocpiDollarCheck");
	
	var ucopTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > US Cheque drawn outside PH</div>", tradVul: "<div class='numFormat'>"+usCheckOutPhData.cashierTotals.tv+"</div>",dsTV: "",vdsTV:"",ccmNotes:"",ppaNotes:"",findings:""}];
		
//	var bankOTCCheckData = generateSLOCPIDollarSectionData("totalBankOTCCheckPayment");
	
//	var bocTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > Bank OTC Check Payment </div>", tradVul: "<div class='numFormat'>"+bankOTCCheckData.cashierTotals.tv+"</div>",dsTV: "",vdsTV:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var checkNCData = generateSLOCPIDollarSectionData("totalCheckNonCounter","slocpiDollarCheck");
	
	var cNCTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > Cheque Non Counter</div>", tradVul: "<div class='numFormat'>"+checkNCData.cashierTotals.tv+"</div>",dsTV: "",vdsTV:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var dollarCheckTotals = getSLOCPIDollarAreaTotal([usCheckInManilaData,usCheckOutPhData,checkNCData]);
	
	var areaCheckTotal = [{product: "<div class='totalRow'>&nbsp;</div>",cashier:"<div class='totalRow'><div class='alignRight'>TOTAL CHECK</div></div>", tradVul: "<div class='totalRow'><div class='numFormat'>"+dollarCheckTotals.tv+"</div></div>",dsTV: "<div class='totalRow'>&nbsp;</div>",vdsTV:"<div class='totalRow'>&nbsp;</div>",ccmNotes:"<div class='totalRow'>&nbsp;</div>",ppaNotes:"<div class='totalRow'>&nbsp;</div>",findings:"<div class='totalRow'>&nbsp;</div>"}];
	
	var dollarCardHeader = [{product: "<div class='alignLeft' cdTarget='slocpiDollarCard' onclick='collapseDivision(this)'>DOLLAR CARD</div>",cashier:"", tradVul: "",dsTV: "",vdsTV:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var posBPIData = generateSLOCPIDollarSectionData("totalPosBpi","slocpiDollarCard");
	
	var posBPITotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > POS - BPI</div>", tradVul: "<div class='numFormat'>"+posBPIData.cashierTotals.tv+"</div>",dsTV: "",vdsTV:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var posCTIBData = generateSLOCPIDollarSectionData("totalPosCtb","slocpiDollarCard");
	
	var posCTIBTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > POS - CITIBANK</div>", tradVul: "<div class='numFormat'>"+posCTIBData.cashierTotals.tv+"</div>",dsTV: "",vdsTV:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var posHSBCData = generateSLOCPIDollarSectionData("totalPosHsbc","slocpiDollarCard");
	
	var posHSBCTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > POS - HSBC</div>", tradVul: "<div class='numFormat'>"+posHSBCData.cashierTotals.tv+"</div>",dsTV: "",vdsTV:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var posSBCData = generateSLOCPIDollarSectionData("totalPosScb","slocpiDollarCard");
	
	var posSBCTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > POS - SCB</div>", tradVul: "<div class='numFormat'>"+posSBCData.cashierTotals.tv+"</div>",dsTV: "",vdsTV:"",ccmNotes:"",ppaNotes:"",findings:""}];

//	var mdsBPIData = generateSLOCPIDollarSectionData("totalMdsBpi");
//	
//	var mdsBPITotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > MDS - BPI</div>", tradVul: "<div class='numFormat'>"+mdsBPIData.cashierTotals.tv+"</div>",dsTV: "",vdsTV:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var mdsBDOData = generateSLOCPIDollarSectionData("totalMdsBdo","slocpiDollarCard");
	
	var mdsBDOTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > MDS - BDO</div>", tradVul: "<div class='numFormat'>"+mdsBDOData.cashierTotals.tv+"</div>",dsTV: "",vdsTV:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
//	var mdsRCBCData = generateSLOCPIDollarSectionData("totalMdsRcbc");
//	
//	var mdsRCBCTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > MDS - RCBC</div>", tradVul: "<div class='numFormat'>"+mdsRCBCData.cashierTotals.tv+"</div>",dsTV: "",vdsTV:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var mdsSBCData = generateSLOCPIDollarSectionData("totalMdsSbc","slocpiDollarCard");
	
	var mdsSBCTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > MDS - SCB</div>", tradVul: "<div class='numFormat'>"+mdsSBCData.cashierTotals.tv+"</div>",dsTV: "",vdsTV:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var mdsCTBData = generateSLOCPIDollarSectionData("totalMdsCtb","slocpiDollarCard");
	
	var mdsCTBTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > MDS - CITIBANK</div>", tradVul: "<div class='numFormat'>"+mdsCTBData.cashierTotals.tv+"</div>",dsTV: "",vdsTV:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var mdsHSBCData = generateSLOCPIDollarSectionData("totalMdsHsbc","slocpiDollarCard");
	
	var mdsHSBCTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > MDS - HSBC</div>", tradVul: "<div class='numFormat'>"+mdsHSBCData.cashierTotals.tv+"</div>",dsTV: "",vdsTV:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
//	var autoCTBData = generateSLOCPIDollarSectionData("totalAutoCtb");
//	
//	var autoCTBTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > CITIBANK (AUTOCHARGE)</div>", tradVul: "<div class='numFormat'>"+autoCTBData.cashierTotals.tv+"</div>",dsTV: "",vdsTV:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
//	var autoSCBData = generateSLOCPIDollarSectionData("totalAutoScb");
//	
//	var autoSCBTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > SCB (AUTOCHARGE)</div>", tradVul: "<div class='numFormat'>"+autoSCBData.cashierTotals.tv+"</div>",dsTV: "",vdsTV:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
//	var autoRCBCData = generateSLOCPIDollarSectionData("totalAutoRcbc");
//	
//	var autoRCBCTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > RCBC - AUTOCHARGE</div>", tradVul: "<div class='numFormat'>"+autoRCBCData.cashierTotals.tv+"</div>",dsTV: "",vdsTV:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
//	var sunlinkOLData = generateSLOCPIDollarSectionData("totalSunlinkOnline");
//	
//	var sunlinkOLTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > SUNLINK ONLINE</div>", tradVul: "<div class='numFormat'>"+sunlinkOLData.cashierTotals.tv+"</div>",dsTV: "",vdsTV:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var sunlinkHSBCDData = generateSLOCPIDollarSectionData("totalSunlinkHsbcDollar","slocpiDollarCard");
	
	var sunlinkHSBCDTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > SUNLINK HSBC DOLLAR</div>", tradVul: "<div class='numFormat'>"+sunlinkHSBCDData.cashierTotals.tv+"</div>",dsTV: "",vdsTV:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
//	var epsBPIData = generateSLOCPIDollarSectionData("totalEpsBpi");
//	
//	var epsBPITotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > EPS - BPI</div>", tradVul: "<div class='numFormat'>"+epsBPIData.cashierTotals.tv+"</div>",dsTV: "",vdsTV:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var areaCardTotals = getSLOCPIDollarAreaTotal([posBPIData,posCTIBData,posHSBCData,posSBCData,mdsBDOData,mdsSBCData,mdsCTBData,mdsHSBCData,sunlinkHSBCDData]);
	
	var dollarCardTotals = [{product: "<div class='totalRow'>&nbsp;</div>",cashier:"<div class='totalRow'><div class='alignRight'>TOTAL CARD</div></div>", tradVul: "<div class='totalRow'><div class='numFormat'>"+areaCardTotals.tv+"</div></div>",dsTV: "<div class='totalRow'>&nbsp;</div>",vdsTV:"<div class='totalRow'>&nbsp;</div>",ccmNotes:"<div class='totalRow'>&nbsp;</div>",ppaNotes:"<div class='totalRow'>&nbsp;</div>",findings:"<div class='totalRow'>&nbsp;</div>"}];
	
	var nonCashHeader = [{product: "<div class='alignLeft' cdTarget='slocpiDollarNonCash' onclick='collapseDivision(this)'>NON CASH</div>",cashier:"", tradVul: "",dsTV: "",vdsTV:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var nonCashData = generateSLOCPIDollarSectionData("totalNonCash","slocpiDollarNonCash");
	
	var nonCashTotal = [{product: "<div class='totalRow'>&nbsp;</div>",cashier:"<div class='totalRow'><div class='alignRight'>TOTAL NON CASH</div></div>", tradVul: "<div class='totalRow'><div class='numFormat'>"+nonCashData.cashierTotals.tv+"</div></div>",dsTV: "<div class='totalRow'>&nbsp;</div>",vdsTV:"<div class='totalRow'>&nbsp;</div>",ccmNotes:"<div class='totalRow'>&nbsp;</div>",ppaNotes:"<div class='totalRow'>&nbsp;</div>",findings:"<div class='totalRow'>&nbsp;</div>"}];
	
	var nonPostedFromDTRHeader = [{product: "<div class='alignLeft' cdTarget='slocpiDollarshNonPosted' onclick='collapseDivision(this)'>NON POSTED (from DTR)</div>",cashier:"", tradVul: "",dsTV: "",vdsTV:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var nbIndividualData = generateSLOCPIDollarSectionData("totalNewBusinessIndividual","slocpiDollarshNonPosted");
	
	var nbIndividualTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > NB Individual</div>", tradVul: "<div class='numFormat'>"+nbIndividualData.cashierTotals.tv+"</div>",dsTV: "",vdsTV:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var nbVULData = generateSLOCPIDollarSectionData("totalNewBusinessVariable","slocpiDollarshNonPosted");
	
	var nbVULTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > NB VUL</div>", tradVul: "<div class='numFormat'>"+nbVULData.cashierTotals.tv+"</div>",dsTV: "",vdsTV:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var individualRenewalData = generateSLOCPIDollarSectionData("totalIndividualRenewal","slocpiDollarshNonPosted");
	
	var individualRenewalTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > Individual Renewal</div>", tradVul: "<div class='numFormat'>"+individualRenewalData.cashierTotals.tv+"</div>",dsTV: "",vdsTV:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var vulRenewalData = generateSLOCPIDollarSectionData("totalVariableRenewal","slocpiDollarshNonPosted");
	
	var vulRenewalTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > VUL Renewal</div>", tradVul: "<div class='numFormat'>"+vulRenewalData.cashierTotals.tv+"</div>",dsTV: "",vdsTV:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var nonCashDTRData = generateSLOCPIDollarSectionData("totalNonCashFromDTR","slocpiDollarFMP");
	
	var nonCashDTRTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > Non Cash</div>", tradVul: "<div class='numFormat'>"+nonCashDTRData.cashierTotals.tv+"</div>",dsTV: "",vdsTV:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var areaNonPostedTotals = getSLOCPIDollarAreaTotal([nbIndividualData,nbVULData,individualRenewalData,vulRenewalData,nonCashDTRData]);
	
	var nonPostedTotal = [{product: "<div class='totalRow'>&nbsp;</div>",cashier:"<div class='totalRow'><div class='alignRight'>TOTAL NON-POSTED</div></div>", tradVul: "<div class='totalRow'><div class='numFormat'>"+areaNonPostedTotals.tv+"</div></div>",dsTV: "<div class='totalRow'>&nbsp;</div>",vdsTV:"<div class='totalRow'>&nbsp;</div>",ccmNotes:"<div class='totalRow'>&nbsp;</div>",ppaNotes:"<div class='totalRow'>&nbsp;</div>",findings:"<div class='totalRow'>&nbsp;</div>"}];
	
	var forManualPostingHeader = [{product: "<div class='alignLeft' cdTarget='slocpiDollarFMP' onclick='collapseDivision(this)'>FOR MANUAL POSTING</div>",cashier:"", tradVul: "",dsTV: "",vdsTV:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var nonPolicyFMPData = generateSLOCPIDollarSectionData("totalNonPolicy","slocpiDollarFMP");
	
	var nonPolicyFMPTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > Non Policy</div>", tradVul: "<div class='numFormat'>"+nonPolicyFMPData.cashierTotals.tv+"</div>",dsTV: "",vdsTV:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var worksiteNotPostedFMPData = generateSLOCPIDollarSectionData("totalWorksiteNonPosted","slocpiDollarFMP");
	
	var worksiteNotPostedFMPTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > Worksite</div>", tradVul: "<div class='numFormat'>"+worksiteNotPostedFMPData.cashierTotals.tv+"</div>",dsTV: "",vdsTV:"",ccmNotes:"",ppaNotes:"",findings:""}];	
	
	var areaForManualPostingTotals = getSLOCPIDollarAreaTotal([nonPolicyFMPData,worksiteNotPostedFMPData,nonCashDTRData]);
	
	var forManualPostingotal = [{product: "<div class='totalRow'>&nbsp;</div>",cashier:"<div class='totalRow'><div class='alignRight'>TOTAL FOR MANUAL POSTING</div></div>", tradVul: "<div class='totalRow'><div class='numFormat'>"+areaForManualPostingTotals.tv+"</div></div>",dsTV: "<div class='totalRow'>&nbsp;</div>",vdsTV:"<div class='totalRow'>&nbsp;</div>",ccmNotes:"<div class='totalRow'>&nbsp;</div>",ppaNotes:"<div class='totalRow'>&nbsp;</div>",findings:"<div class='totalRow'>&nbsp;</div>"}];
	
	var exemptionHeader = [{product: "<div class='alignLeft' cdTarget='slocpiDollarExemptions' onclick='collapseDivision(this)'>EXEMPTIONS</div>",cashier:"", tradVul: "",dsTV: "",vdsTV:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var exemptionData = generateSLOCPIDollarSectionData("reversalProduct","slocpiDollarExemptions");
	
	var exemptionTotal = [{product: "<div class='totalRow'>&nbsp;</div>",cashier:"<div class='totalRow'><div class='alignRight'>TOTAL EXEMPTIONS</div></div>", tradVul: "<div class='totalRow'><div class='numFormat'>"+exemptionData.cashierTotals.tv+"</div></div>",dsTV: "<div class='totalRow'>&nbsp;</div>",vdsTV:"<div class='totalRow'>&nbsp;</div>",ccmNotes:"<div class='totalRow'>&nbsp;</div>",ppaNotes:"<div class='totalRow'>&nbsp;</div>",findings:"<div class='totalRow'>&nbsp;</div>"}];
	
	var sessionTotalHeader = [{product: "<div class='alignLeft' cdTarget='slocpiDollarST' onclick='collapseDivision(this)'>SESSION TOTALS</div>",cashier:"", tradVul: "",dsTV: "",vdsTV:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var sessionTotalData = generateSLOCPIDollarSectionData("sessionTotal","slocpiDollarST");
	
	var sessionTotalTotal = [{product: "<div class='totalRow'>&nbsp;</div>",cashier:"<div class='totalRow'><div class='alignRight'>SESSION TOTAL</div></div>", tradVul: "<div class='totalRow'><div class='numFormat'>"+sessionTotalData.cashierTotals.tv+"</div></div>",dsTV: "<div class='totalRow'>&nbsp;</div>",vdsTV:"<div class='totalRow'>&nbsp;</div>",ccmNotes:"<div class='totalRow'>&nbsp;</div>",ppaNotes:"<div class='totalRow'>&nbsp;</div>",findings:"<div class='totalRow'>&nbsp;</div>"}];
	
	var totalSLOCPIDCashiersCollection = getSLOCPIDollarAreaTotal([nonCashData,cashNonCounterData,cashCounterData,usCheckInManilaData,usCheckOutPhData,checkNCData,posBPIData,posCTIBData,posHSBCData,posSBCData,mdsBDOData,mdsCTBData,mdsHSBCData,sunlinkHSBCDData]);
	
	var cashierTotalHeader = [{product: "<div class='alignLeft' cdTarget='slocpiDollarGTPC' onclick='collapseDivision(this)'>Grand Total Per Cashier</div>",cashier:"", tradVul: "",dsTV: "",vdsTV:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var cashierTotal = getSLOCPIDGrandTotalPerCashier("slocpiDollarGTPC");
	
	var cashierTotalRow = [{product: "<div class='totalRow'>&nbsp;</div>",cashier:"<div class='totalRow'><div class='alignRight'>TOTAL COLLECTION</div></div>", tradVul: "<div class='totalRow'><div class='numFormat'>"+numberWithCommas(totalSLOCPIDCashiersCollection.tv)+"</div></div>",dsTV: "<div class='totalRow'>&nbsp;</div>",vdsTV:"<div class='totalRow'>&nbsp;</div>",ccmNotes:"<div class='totalRow'>&nbsp;</div>",ppaNotes:"<div class='totalRow'>&nbsp;</div>",findings:"<div class='totalRow'>&nbsp;</div>"}];
	
	var slocpiDollarSummaryData = [{product: "",cashier:"CONSOLIDATED GRAND TOTAL", tradVul: "<div class='numFormat'>"+numberWithCommas(slocpiDollarConsolidatedTotals.tv)+"</div>",dsTV: "",vdsTV:"",ccmNotes:"",ppaNotes:"",findings:""},
	                        {product: "",cashier:"TOTAL CASHIERS COLLECTION", tradVul:"<div class='numFormat'>"+numberWithCommas(totalSLOCPIDCashiersCollection.tv)+"</div>",dsTV: "",vdsTV:"",ccmNotes:"",ppaNotes:"",findings:""},
	                        {product: "",cashier:"", tradVul: compareTotalsImg(totalSLOCPIDCashiersCollection.tv,numberWithCommas(slocpiDollarConsolidatedTotals.tv)),dsTV: "",vdsTV:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	new TableContainer({
		divName : slocpiDollarSummaryDivDetails.divName,
		nodeName : slocpiDollarSummaryDivDetails.nodeName,
		width : "800px",
		id : slocpiDollarSummaryDivDetails.tableId,
		layout : slocpiDollarColumns,
		tableClass: "balancingToolSummary",
		data: slocpiDollarSummaryData
	}).startUp();
	
	var slocpiDollarSectionData = [];
	
	appendJsons([slocpiDollarSectionData,dollarCashHeader,cashNonCounterData.prodTotals,cncTotal,cashCounterData.prodTotals,ccTotal,areaCashTotal,dollarCheckHeader,usCheckInManilaData.prodTotals,ucimTotal,usCheckOutPhData.prodTotals,ucopTotal,checkNCData.prodTotals,cNCTotal,areaCheckTotal,dollarCardHeader,posBPIData.prodTotals,posBPITotal,posCTIBData.prodTotals,posCTIBTotal,posHSBCData.prodTotals,posHSBCTotal,posSBCData.prodTotals,posSBCTotal,mdsBDOData.prodTotals,mdsBDOTotal,mdsSBCData.prodTotals,mdsSBCTotal,mdsCTBData.prodTotals,mdsCTBTotal,mdsHSBCData.prodTotals,mdsHSBCTotal,sunlinkHSBCDData.prodTotals,sunlinkHSBCDTotal,dollarCardTotals,nonCashHeader,nonCashData.prodTotals,nonCashTotal,cashierTotalHeader,cashierTotal,cashierTotalRow,nonPostedFromDTRHeader,nbIndividualData.prodTotals,nbIndividualTotal,nbVULData.prodTotals,nbVULTotal,individualRenewalData.prodTotals,individualRenewalTotal,vulRenewalData.prodTotals,vulRenewalTotal,nonPostedTotal,forManualPostingHeader,nonCashDTRData.prodTotals,nonCashDTRTotal,nonPolicyFMPData.prodTotals,nonPolicyFMPTotal,worksiteNotPostedFMPData.prodTotals,worksiteNotPostedFMPTotal,forManualPostingotal,exemptionHeader,exemptionData.prodTotals,exemptionTotal,sessionTotalHeader,sessionTotalData.prodTotals,sessionTotalTotal,generateSnapshotSection(slocpiSnapshot)]);
	
	new TableContainer({
		divName : slocpiDollarDivDetails.divName,
		nodeName : slocpiDollarDivDetails.nodeName,
		width : "800px",
		id : slocpiDollarDivDetails.tableId,
		layout : slocpiDollarColumnsNH,
		tableClass: "balancingToolSummary",
		data: slocpiDollarSectionData
	}).startUp();
	
	var slocpiDollarTableDiv = document.getElementById(slocpiDollarTableDivId);

	slocpiDollarTableDiv.style.display = "none";
	
	});
}


var slocpiDollarSecData = [];

function mergeSLOCPIDollarCashierProductTypes(){
	var cashierRep = [];
	for(c in slocpiDollarData){
		//using jQuery.inArray instead of Array.indexOf because it is not supported in IE8 and below. thanks IE.
		if(jQuery.inArray(slocpiDollarData[c].cashierId+"~"+slocpiDollarData[c].product,cashierRep) == -1){
			var pushedData = "";
			pushedData = pushedData.concat(slocpiDollarData[c].cashierId);
			pushedData = pushedData.concat("~");
			pushedData = pushedData.concat(slocpiDollarData[c].product);
			cashierRep.push(pushedData);
		}
	}
	
	for(var a = 0; a < cashierRep.length; a++){
		slocpiDollarSecData.push(mergeSLOCPIDollarCashierData(cashierRep[a]));
	}
		
}

function getSLOCPIDollarAreaTotal(area){
	var tots = {};
	var tv = 0;
	var groupLife = 0;
	for(a in area){
		tv = tv + parseFloat(area[a].cashierTotals.tv.replace(/,/g,""));
		groupLife = groupLife + parseFloat(area[a].cashierTotals.gl.replace(/,/g,""));
	}
	tots.tv = numberWithCommas(parseFloat(tv).toFixed(2));
	tots.groupLife = numberWithCommas(parseFloat(groupLife).toFixed(2));
	return tots;
}

function generateSLOCPIDollarSectionData(section,cdTarget){
	var sectionObj = {};
	var sectionCashierTotals = {};
	var tv = 0;
	var gl = 0;
	var  returnCNC = [];
	var firstRowFlag = true;
	for(a in slocpiDollarSecData){
		var tempCNC = {};
		if(slocpiDollarSecData[a].product == section){
			var isCashDepo=false;
			var isCheckDepo=false;
			if(firstRowFlag){
				switch(section){
					case "totalCashNonCounter": tempCNC.product = "<div class='alignLeft'>Non Counter</div>";break;
					case "totalCashCounter": tempCNC.product = "<div class='alignLeft'>Counter</div>";isCashDepo=true;break;
					case "totalCheckOnUs": tempCNC.product = "<div class='alignLeft'>On Us</div>";break;
					case "totalCheckRegional": tempCNC.product = "<div class='alignLeft'>Regional</div>";break;
					case "totalCheckNonCounter": tempCNC.product = "<div class='alignLeft'>Non Counter</div>";break;
					case "totalCheckLocal": tempCNC.product = "<div class='alignLeft'>Local</div>";break;
					case "totalUsCheckInManila" : tempCNC.product = "<div class='alignLeft'>US Cheque drawn in Manila</div>";isCheckDepo=true;checkType="USCD_MANILA";break;
					case "totalUsCheckOutPh" : tempCNC.product = "<div class='alignLeft'>US Cheque drawn outside PH</div>";isCheckDepo=true;checkType="USCD_OUTSIDEPH";break;
					//case "totalBankOTCCheckPayment" : tempCNC.product = "<div class='alignLeft'>Bank OTC Check Payment </div>";isCheckDepo=true;checkType="BANK_OTC";break;
					case "totalPosBpi": tempCNC.product = "<div class='alignLeft'>POS - BPI</div>";break;
					case "totalPosCtb": tempCNC.product = "<div class='alignLeft'>POS - CITIBANK</div>";break;
					case "totalPosHsbc": tempCNC.product = "<div class='alignLeft'>POS - HSBC</div>";break;
					case "totalPosScb": tempCNC.product = "<div class='alignLeft'>POS - SCB</div>";break;
					case "totalMdsBpi": tempCNC.product = "<div class='alignLeft'>MDS - BPI</div>";break;
					case "totalMdsBdo": tempCNC.product = "<div class='alignLeft'>MDS - BDO</div>";break;
					case "totalMdsRcbc": tempCNC.product = "<div class='alignLeft'>MDS - RCBC</div>";break;
					case "totalMdsSbc": tempCNC.product = "<div class='alignLeft'>MDS - SCB</div>";break;
					case "totalMdsCtb": tempCNC.product = "<div class='alignLeft'>MDS - CITIBANK</div>";break;
					case "totalMdsHsbc": tempCNC.product = "<div class='alignLeft'>MDS - HSBC</div>";break;
					case "totalAutoCtb": tempCNC.product = "<div class='alignLeft'>CITIBANK (AUTOCHARGE)</div>";break;
					case "totalAutoScb": tempCNC.product = "<div class='alignLeft'>SCB (AUTOCHARGE)</div>";break;
					case "totalAutoRcbc": tempCNC.product = "<div class='alignLeft'>RCBC - AUTOCHARGE</div>";break;
					case "totalSunlinkOnline": tempCNC.product = "<div class='alignLeft'>SUNLINK ONLINE</div>";break;
					case "totalSunlinkHsbcDollar": tempCNC.product = "<div class='alignLeft'>SUNLINK HSBC DOLLAR</div>";break;
					case "totalEpsBpi": tempCNC.product = "<div class='alignLeft'>EPS - BPI</div>";break;
					case "totalNonCash": tempCNC.product = "<div class='alignLeft'>Non Cash</div>";break;
					case "totalNewBusinessIndividual": tempCNC.product = "<div class='alignLeft'>New Business Individual</div>";break;
					case "totalNewBusinessVariable": tempCNC.product = "<div class='alignLeft'>NB VUL</div>";break;
					case "totalIndividualRenewal": tempCNC.product = "<div class='alignLeft'>Individual Renewal</div>";break;
					case "totalVariableRenewal": tempCNC.product = "<div class='alignLeft'>VUL Renewal</div>";break;
					case "totalNonCashFromDTR": tempCNC.product = "<div class='alignLeft'>Non Cash</div>";break;
					case "totalNonPolicy": tempCNC.product = "<div class='alignLeft'>Non Policy</div>";break;
					case "totalWorksiteNonPosted": tempCNC.product = "<div class='alignLeft'>Worksite (not posted)</div>";break;
					case "reversalProduct" : tempCNC.product = "";break;
					case "sessionTotal" : tempCNC.product = "";break;
					default: alert("Unknown Product Total: " + section); break;
				}
			} else{
				tempCNC.product = "";		
				switch(section){
					case "totalCashCounter": isCashDepo=true;break;
					case "totalUsCheckInManila" : isCheckDepo=true;checkType="USCD_MANILA";break;
					case "totalUsCheckOutPh" : isCheckDepo=true;checkType="USCD_OUTSIDEPH";break;
				}
			}
			
			tempCNC.cashier = "<div class='alignLeft'>"+slocpiDollarSecData[a].cashierName+"</div>";
			if(isSLOCPIConfirmed(slocpiDollarSecData[a].cashierId))	{
				if(section == "reversalProduct"){
					if(parseFloat(slocpiDollarSecData[a].tradVul) != 0){
						tempCNC.tradVul = "<div class='alignRight'><a style='cursor:pointer;text-decoration:underline;color:blue;'onclick='openSPReversal("+getBalancingToolProductId(slocpiDollarSecData[a].cashierId,"IL_VL_DOLLAR",section,slocpiDollarData)+");'>"+numberWithCommas(parseFloat(slocpiDollarSecData[a].tradVul).toFixed(2))+"</a></div>";
					} else{
						tempCNC.tradVul = "<div class='alignRight'>"+numberWithCommas(parseFloat(slocpiDollarSecData[a].tradVul).toFixed(2))+"</div>";
					}
				} else {
					tempCNC.tradVul = "<div class='alignRight'>"+numberWithCommas(parseFloat(slocpiDollarSecData[a].tradVul).toFixed(2))+"</div>";
					tempCNC.groupLife = "<div class='alignRight'>"+numberWithCommas(parseFloat(slocpiDollarSecData[a].groupLife).toFixed(2))+"</div>";
					if(isCashDepo){
						if(parseFloat(slocpiDollarSecData[a].tradVul) > 0){
							tempCNC.dsTV  = getCashDepoURL("SLOCPI","TRAD/VUL","USD",slocpiDollarSecData[a].cashierId);
							tempCNC.vdsTV  = getValidatedCashDepoURL("SLOCPI","TRAD/VUL","USD",slocpiDollarSecData[a].cashierId);
						}
						if(parseFloat(slocpiDollarSecData[a].groupLife) > 0){
							tempCNC.dsGL = getCashDepoURL("SLOCPI","TRAD/VUL","USD",slocpiDollarSecData[a].cashierId);
							tempCNC.vdsGL = getValidatedCashDepoURL("SLOCPI","TRAD/VUL","USD",slocpiDollarSecData[a].cashierId);
						}
				
					}else if(isCheckDepo){
						if(parseFloat(slocpiDollarSecData[a].tradVul) > 0){
							tempCNC.dsTV  = getChequeDepoURL("SLOCPI","TRAD/VUL","USD",slocpiDollarSecData[a].cashierId,checkType);
							tempCNC.vdsTV = getValidatedChequeDepoURL("SLOCPI","TRAD/VUL","USD",slocpiDollarSecData[a].cashierId,checkType);
						}
						if(parseFloat(slocpiDollarSecData[a].groupLife) > 0){
							tempCNC.dsGL = getChequeDepoURL("SLOCPI","TRAD/VUL","USD",slocpiDollarSecData[a].cashierId,checkType);
							tempCNC.vdsGL = getValidatedChequeDepoURL("SLOCPI","TRAD/VUL","USD",slocpiDollarSecData[a].cashierId,checkType);
						}
					}
				}
				tv = tv + parseFloat(slocpiDollarSecData[a].tradVul);
				gl = gl + parseFloat(slocpiDollarSecData[a].groupLife);
			} else{
				tempCNC.tradVul = "<div class='alignCenter'>-</div>";
				tempCNC.groupLife = "<div class='alignCenter'>-</div>";
			}
			if(jQuery.inArray(section,excludeFromCashierTotal) == -1){
				slocpiDollarConsolidatedTotals.tv = (parseFloat(slocpiDollarConsolidatedTotals.tv) + parseFloat(slocpiDollarSecData[a].tradVul)).toFixed(2);
				slocpiDollarConsolidatedTotals.gl = (parseFloat(slocpiDollarConsolidatedTotals.gl) + parseFloat(slocpiDollarSecData[a].groupLife)).toFixed(2);
			}
			tempCNC.ccmNotes = "<div class='alignCenter'>"+getNote(rowIndex,"CCM")+"</div>";
			tempCNC.ppaNotes = "<div class='alignCenter'>"+getNote(rowIndex,"PPA")+"</div>";
			tempCNC.findings = "<div class='alignCenter'  collapseTarget='"+cdTarget+"'>"+getNote(rowIndex,"FINDINGS")+"</div>";			
			rowIndex++;
			firstRowFlag = false;		
			returnCNC.push(tempCNC);
		}
	}
	sectionCashierTotals.tv = numberWithCommas(parseFloat(tv).toFixed(2));
	sectionCashierTotals.gl = numberWithCommas(parseFloat(gl).toFixed(2));
	sectionObj.prodTotals = returnCNC;
	sectionObj.cashierTotals = sectionCashierTotals;
	return sectionObj;
}

function mergeSLOCPIDollarCashierData(cashierProduct){
	var cpArr = cashierProduct.split("~");
	var cashierId = cpArr[0];
	var product = cpArr[1];
	var tempCashier = {};
	for(a in slocpiDollarData){
		if(slocpiDollarData[a].cashierId == cashierId && slocpiDollarData[a].product == product){
			if(slocpiDollarData[a].productCode == "IL_VL_DOLLAR"){
				tempCashier.tradVul = slocpiDollarData[a].total;
			} 
			tempCashier.cashierId = slocpiDollarData[a].cashierId;
			tempCashier.cashierName = slocpiDollarData[a].cashierName;
			tempCashier.product = product;
		}
	}
	return tempCashier;
}

function getSLOCPIDGrandTotalPerCashier(cdTarget){
	var returnSection = [];
	var c = 0;
	for(c in cashierList){
		var tv = 0;
		var d=0;
		for(d in slocpiDollarData){
			if(jQuery.inArray(slocpiDollarData[d].product,excludeFromCashierTotal) == -1 && slocpiDollarData[d].cashierId.toUpperCase() == cashierList[c].id.toUpperCase()){
				switch(slocpiDollarData[d].productCode){
					case "IL_VL_DOLLAR": tv = tv + parseFloat(slocpiDollarData[d].total);break;
				}
			}
		}
		var tempCashier = {};
		tempCashier.cashier = "<div class='alignLeft' collapseTarget='"+cdTarget+"'>"+cashierList[c].name+"</div>";
		if(isSLOCPIConfirmed(cashierList[c].id)){
			tempCashier.tradVul = "<div class='alignRight'>"+numberWithCommas(tv.toFixed(2))+"</div>";
		} else {
			tempCashier.tradVul = "<div class='alignCenter'>-</div>";
		}
		returnSection.push(tempCashier);
	}
	return returnSection;
}

var slocpiDollarConsolidatedTotals = {tv: "0.00", gl: "0.00"};