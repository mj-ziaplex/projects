var slfpiColumns = [{
	name : "",
	field : "product",
	width : "14%"
},{
	name : "",
	field : "cashier",
	width : "21%"
},{
	name : "Preneed(PN)",
	field : "preneed",
	width : "8%"
},{
	name : "DS",
	field : "dsPN",
	width : "7%"
},{
	name : "VDS",
	field : "vdsPN",
	width : "7%"
},{
	name : "GAF PESO",
	field : "gafPeso",
	width : "7%"
},{
	name : "DS",
	field : "dsGaf",
	width : "7%"
},{
	name : "VDS",
	field : "vdsGaf",
	width : "7%"
},{
	name : "CCM NOTES",
	field : "ccmNotes",
	width : "7%"
},{
	// Modified for MR-WF-16-00036 - Change PPA notes to CCQA notes
	name : "CCQA NOTES",
	field : "ppaNotes",
	width : "7%"
},{
	name : "Findings",
	field : "findings",
	width : "8%"
}	
];

var slfpiColumnsNH = [{
	name : "",
	field : "product",
	width : "14%"
},{
	name : "",
	field : "cashier",
	width : "21%"
},{
	name : "",
	field : "preneed",
	width : "8%"
},{
	name : "",
	field : "dsPN",
	width : "7%"
},{
	name : "",
	field : "vdsPN",
	width : "7%"
},{
	name : "",
	field : "gafPeso",
	width : "7%"
},{
	name : "",
	field : "dsGaf",
	width : "7%"
},{
	name : "",
	field : "vdsGaf",
	width : "7%"
},{
	name : "",
	field : "ccmNotes",
	width : "7%"
},{
	name : "",
	field : "ppaNotes",
	width : "7%"
},{
	name : "",
	field : "findings",
	width : "8%"
}	
];

function generateSLFPISection(slfpiTableDivId,slfpiDivDetails,slfpiSummaryDivDetails){
	$(document).ready( function(){
			
	mergeSLFPICashierProductTypes();
	
	var pesoCashHeader = [{product: "<div class='alignLeft' cdTarget='slfpiCash' onclick='collapseDivision(this)'>PESO CASH</div>",cashier:"", preneed: "",dsPN: "",vdsPN:"",gafPeso:"",dsGaf:"",vdsGaf:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var cashNonCounterData = generateSLFPISectionData("totalCashNonCounter","slfpiCash");
	
	var cncTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > Non Counter</div>", preneed: "<div class='numFormat'>"+cashNonCounterData.cashierTotals.pnTotal+"</div>",dsPN: "",vdsPN:"",gafPeso:"<div class='numFormat'>"+cashNonCounterData.cashierTotals.gafTotal+"</div>",dsGaf:"",vdsGaf:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var cashCounterData = generateSLFPISectionData("totalCashCounter","slfpiCash");
	
	var ccTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > Counter</div>", preneed: "<div class='numFormat'>"+cashCounterData.cashierTotals.pnTotal+"</div>",dsPN: "",vdsPN:"",gafPeso:"<div class='numFormat'>"+cashCounterData.cashierTotals.gafTotal+"</div>",dsGaf:"",vdsGaf:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var pesoCashTotals = getSLFPIAreaTotal([cashNonCounterData,cashCounterData]);
	
	var areaCashTotal = [{product: "<div class='totalRow'>&nbsp;</div>",cashier:"<div class='totalRow'><div class='alignRight'>TOTAL CASH</div></div>", preneed: "<div class='totalRow'><div class='numFormat'>"+pesoCashTotals.pn+"</div></div>",dsPN: "<div class='totalRow'>&nbsp;</div>",vdsPN:"<div class='totalRow'>&nbsp;</div>",gafPeso:"<div class='totalRow'><div class='numFormat'>"+pesoCashTotals.gafPeso+"</div></div>",dsGaf:"<div class='totalRow'>&nbsp;</div>",vdsGaf:"<div class='totalRow'>&nbsp;</div>",ccmNotes:"<div class='totalRow'>&nbsp;</div>",ppaNotes:"<div class='totalRow'>&nbsp;</div>",findings:"<div class='totalRow'>&nbsp;</div>"}];
	
	var pesoCheckHeader = [{product: "<div class='alignLeft' cdTarget='slfpiCheck' onclick='collapseDivision(this)'>PESO CHECK</div>",cashier:"", preneed: "",dsPN: "",vdsPN:"",gafPeso:"",dsGaf:"",vdsGaf:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var checkOnUsData = generateSLFPISectionData("totalCheckOnUs","slfpiCheck");
	
	var chouTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > On Us</div>", preneed: "<div class='numFormat'>"+checkOnUsData.cashierTotals.pnTotal+"</div>",dsPN: "",vdsPN:"",gafPeso:"<div class='numFormat'>"+checkOnUsData.cashierTotals.gafTotal+"</div>",dsGaf:"",vdsGaf:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var checkLocalData = generateSLFPISectionData("totalCheckLocal","slfpiCheck");
	
	var chlTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > Local</div>", preneed: "<div class='numFormat'>"+checkLocalData.cashierTotals.pnTotal+"</div>",dsPN: "",vdsPN:"",gafPeso:"<div class='numFormat'>"+checkLocalData.cashierTotals.gafTotal+"</div>",dsGaf:"",vdsGaf:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var checkRegionalData = generateSLFPISectionData("totalCheckRegional","slfpiCheck");
	
	var chrTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > Regional</div>", preneed: "<div class='numFormat'>"+checkRegionalData.cashierTotals.pnTotal+"</div>",dsPN: "",vdsPN:"",gafPeso:"<div class='numFormat'>"+checkRegionalData.cashierTotals.gafTotal+"</div>",dsGaf:"",vdsGaf:"",ccmNotes:"",ppaNotes:"",findings:""}];

	var checkNonCounterData = generateSLFPISectionData("totalCheckNonCounter","slfpiCheck");
	
	var chncTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > Non Counter</div>", preneed: "<div class='numFormat'>"+checkNonCounterData.cashierTotals.pnTotal+"</div>",dsPN: "",vdsPN:"",gafPeso:"<div class='numFormat'>"+checkNonCounterData.cashierTotals.gafTotal+"</div>",dsGaf:"",vdsGaf:"",ccmNotes:"",ppaNotes:"",findings:""}];

	var checkOTData = generateSLFPISectionData("totalCheckOT","slfpiCheck");
	
	var chotTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > OT</div>", preneed: "<div class='numFormat'>"+checkOTData.cashierTotals.pnTotal+"</div>",dsPN: "",vdsPN:"",gafPeso:"<div class='numFormat'>"+checkOTData.cashierTotals.gafTotal+"</div>",dsGaf:"",vdsGaf:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var checkGafData = generateSLFPISectionData("totalCheckGaf","slfpiCheck");
	
	var checkGafTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > GAF</div>", preneed: "<div class='numFormat'>"+checkGafData.cashierTotals.pnTotal+"</div>",dsPN: "",vdsPN:"",gafPeso:"<div class='numFormat'>"+checkGafData.cashierTotals.gafTotal+"</div>",dsGaf:"",vdsGaf:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var paymFrInsData = generateSLFPISectionData("paymentFromInsurer","slfpiCheck");
	
	var paymFrInsTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > Payment From Insurer</div>", preneed: "<div class='numFormat'>"+paymFrInsData.cashierTotals.pnTotal+"</div>",dsPN: "",vdsPN:"",gafPeso:"<div class='numFormat'>"+paymFrInsData.cashierTotals.gafTotal+"</div>",dsGaf:"",vdsGaf:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var postalMoneyOrderData = generateSLFPISectionData("totalPmo","slfpiCheck");
	
	var postalMoneyOrderTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > Postal Money Order</div>", preneed: "<div class='numFormat'>"+postalMoneyOrderData.cashierTotals.pnTotal+"</div>",dsPN: "",vdsPN:"",gafPeso:"<div class='numFormat'>"+postalMoneyOrderData.cashierTotals.gafTotal+"</div>",dsGaf:"",vdsGaf:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	
	//var bankOTCCheckData = generateSLFPISectionData("totalBankOTCCheckPayment");
	
	//var bocTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > Bank OTC Check Payment </div>", preneed: "<div class='numFormat'>"+bankOTCCheckData.cashierTotals.pnTotal+"</div>",dsPN: "",vdsPN:"",gafPeso:"<div class='numFormat'>"+bankOTCCheckData.cashierTotals.gafTotal+"</div>",dsGaf:"",vdsGaf:"",ccmNotes:"",ppaNotes:"",findings:""}];

	var pesoCheckTotals = getSLFPIAreaTotal([checkOnUsData,checkLocalData,checkRegionalData,checkNonCounterData,checkOTData,postalMoneyOrderData,checkGafData,paymFrInsData]);
	
	var areaCheckTotal = [{product: "<div class='totalRow'>&nbsp;</div>",cashier:"<div class='totalRow'><div class='alignRight'>TOTAL CHECK</div></div>", preneed: "<div class='totalRow'><div class='numFormat'>"+pesoCheckTotals.pn+"</div></div>",dsPN: "<div class='totalRow'>&nbsp;</div>",vdsPN:"<div class='totalRow'>&nbsp;</div>",gafPeso:"<div class='totalRow'><div class='numFormat'>"+pesoCheckTotals.gafPeso+"</div></div>",dsGaf:"<div class='totalRow'>&nbsp;</div>",vdsGaf:"<div class='totalRow'>&nbsp;</div>",ccmNotes:"<div class='totalRow'>&nbsp;</div>",ppaNotes:"<div class='totalRow'>&nbsp;</div>",findings:"<div class='totalRow'>&nbsp;</div>"}];
	
	var nonCashHeader = [{product: "<div class='alignLeft' cdTarget='slfpiNonCash' onclick='collapseDivision(this)'>NON CASH</div>",cashier:"", preneed: "",dsPN: "",vdsPN:"",gafPeso:"",dsGaf:"",vdsGaf:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var nonCashData = generateSLFPISectionData("totalNonCash","slfpiNonCash");
	
	var nonCashTotal = [{product: "<div class='totalRow'>&nbsp;</div>",cashier:"<div class='totalRow'><div class='alignRight'>TOTAL NON CASH</div></div>", preneed: "<div class='totalRow'><div class='numFormat'>"+nonCashData.cashierTotals.pnTotal+"</div></div>",dsPN: "<div class='totalRow'>&nbsp;</div>",vdsPN:"<div class='totalRow'>&nbsp;</div>",gafPeso:"<div class='totalRow'><div class='numFormat'>"+nonCashData.cashierTotals.gafTotal+"</div></div>",dsGaf:"<div class='totalRow'>&nbsp;</div>",vdsGaf:"<div class='totalRow'>&nbsp;</div>",ccmNotes:"<div class='totalRow'>&nbsp;</div>",ppaNotes:"<div class='totalRow'>&nbsp;</div>",findings:"<div class='totalRow'>&nbsp;</div>"}];
	
	var pesoCardHeader = [{product: "<div class='alignLeft' cdTarget='slfpiCard' onclick='collapseDivision(this)'>PESO CARD</div>",cashier:"", preneed: "",dsPN: "",vdsPN:"",gafPeso:"",dsGaf:"",vdsGaf:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var posBPIData = generateSLFPISectionData("totalPosBpi","slfpiCard");
	
	var posBPITotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > POS - BPI</div>", preneed: "<div class='numFormat'>"+posBPIData.cashierTotals.pnTotal+"</div>",dsPN: "",vdsPN:"",gafPeso:"<div class='numFormat'>"+posBPIData.cashierTotals.gafTotal+"</div>",dsGaf:"",vdsGaf:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var posCTIBData = generateSLFPISectionData("totalPosCtb","slfpiCard");
	
	var posCTIBTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > POS - CITIBANK</div>", preneed: "<div class='numFormat'>"+posCTIBData.cashierTotals.pnTotal+"</div>",dsPN: "",vdsPN:"",gafPeso:"<div class='numFormat'>"+posCTIBData.cashierTotals.gafTotal+"</div>",dsGaf:"",vdsGaf:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var posHSBCData = generateSLFPISectionData("totalPosHsbc","slfpiCard");
	
	var posHSBCTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > POS - HSBC</div>", preneed: "<div class='numFormat'>"+posHSBCData.cashierTotals.pnTotal+"</div>",dsPN: "",vdsPN:"",gafPeso:"<div class='numFormat'>"+posHSBCData.cashierTotals.gafTotal+"</div>",dsGaf:"",vdsGaf:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var posSCBData = generateSLFPISectionData("totalPosScb","slfpiCard");
	
	var posSCBTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > POS - SCB</div>", preneed: "<div class='numFormat'>"+posSCBData.cashierTotals.pnTotal+"</div>",dsPN: "",vdsPN:"",gafPeso:"<div class='numFormat'>"+posSCBData.cashierTotals.gafTotal+"</div>",dsGaf:"",vdsGaf:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var posRCBCData = generateSLFPISectionData("totalPosRcbc","slfpiCard");
	
	var posRCBCTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > POS - RCBC</div>", preneed: "<div class='numFormat'>"+posRCBCData.cashierTotals.pnTotal+"</div>",dsPN: "",vdsPN:"",gafPeso:"<div class='numFormat'>"+posRCBCData.cashierTotals.gafTotal+"</div>",dsGaf:"",vdsGaf:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var posBDOData = generateSLFPISectionData("totalPosBdo","slfpiCard");
	
	var posBDOTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > POS - BDO</div>", preneed: "<div class='numFormat'>"+posBDOData.cashierTotals.pnTotal+"</div>",dsPN: "",vdsPN:"",gafPeso:"<div class='numFormat'>"+posBDOData.cashierTotals.gafTotal+"</div>",dsGaf:"",vdsGaf:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var mdsBPIData = generateSLFPISectionData("totalMdsBpi","slfpiCard");
	
	var mdsBPITotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > MDS - BPI</div>", preneed: "<div class='numFormat'>"+mdsBPIData.cashierTotals.pnTotal+"</div>",dsPN: "",vdsPN:"",gafPeso:"<div class='numFormat'>"+mdsBPIData.cashierTotals.gafTotal+"</div>",dsGaf:"",vdsGaf:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var mdsCTIBData = generateSLFPISectionData("totalMdsCtb","slfpiCard");
	
	var mdsCTIBTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > MDS - CITIBANK</div>", preneed: "<div class='numFormat'>"+mdsCTIBData.cashierTotals.pnTotal+"</div>",dsPN: "",vdsPN:"",gafPeso:"<div class='numFormat'>"+mdsCTIBData.cashierTotals.gafTotal+"</div>",dsGaf:"",vdsGaf:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var mdsHSBCData = generateSLFPISectionData("totalMdsHsbc","slfpiCard");
	
	var mdsHSBCTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > MDS - HSBC</div>", preneed: "<div class='numFormat'>"+mdsHSBCData.cashierTotals.pnTotal+"</div>",dsPN: "",vdsPN:"",gafPeso:"<div class='numFormat'>"+mdsHSBCData.cashierTotals.gafTotal+"</div>",dsGaf:"",vdsGaf:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var mdsSBCData = generateSLFPISectionData("totalMdsSbc","slfpiCard");
	
	var mdsSBCTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > MDS - SCB</div>", preneed: "<div class='numFormat'>"+mdsSBCData.cashierTotals.pnTotal+"</div>",dsPN: "",vdsPN:"",gafPeso:"<div class='numFormat'>"+mdsSBCData.cashierTotals.gafTotal+"</div>",dsGaf:"",vdsGaf:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var mdsRCBCData = generateSLFPISectionData("totalMdsRcbc","slfpiCard");
	
	var mdsRCBCTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > MDS - RCBC</div>", preneed: "<div class='numFormat'>"+mdsRCBCData.cashierTotals.pnTotal+"</div>",dsPN: "",vdsPN:"",gafPeso:"<div class='numFormat'>"+mdsRCBCData.cashierTotals.gafTotal+"</div>",dsGaf:"",vdsGaf:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var mdsBDOData = generateSLFPISectionData("totalMdsBdo","slfpiCard");
	
	var mdsBDOTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > MDS - BDO</div>", preneed: "<div class='numFormat'>"+mdsBDOData.cashierTotals.pnTotal+"</div>",dsPN: "",vdsPN:"",gafPeso:"<div class='numFormat'>"+mdsBDOData.cashierTotals.gafTotal+"</div>",dsGaf:"",vdsGaf:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var autoCTBData = generateSLFPISectionData("totalAutoCtb","slfpiCard");
	
	var autoCTBTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > CITIBANK (AUTOCHARGE)</div>", preneed: "<div class='numFormat'>"+autoCTBData.cashierTotals.pnTotal+"</div>",dsPN: "",vdsPN:"",gafPeso:"<div class='numFormat'>"+autoCTBData.cashierTotals.gafTotal+"</div>",dsGaf:"",vdsGaf:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var autoSCBData = generateSLFPISectionData("totalAutoScb","slfpiCard");
	
	var autoSCBTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > SCB (AUTOCHARGE)</div>", preneed: "<div class='numFormat'>"+autoSCBData.cashierTotals.pnTotal+"</div>",dsPN: "",vdsPN:"",gafPeso:"<div class='numFormat'>"+autoSCBData.cashierTotals.gafTotal+"</div>",dsGaf:"",vdsGaf:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var autoRCBCData = generateSLFPISectionData("totalAutoRcbc","slfpiCard");
	
	var autoRCBCTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > RCBC - AUTOCHARGE</div>", preneed: "<div class='numFormat'>"+autoRCBCData.cashierTotals.pnTotal+"</div>",dsPN: "",vdsPN:"",gafPeso:"<div class='numFormat'>"+autoRCBCData.cashierTotals.gafTotal+"</div>",dsGaf:"",vdsGaf:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var sunlinkOLData = generateSLFPISectionData("totalSunlinkOnline","slfpiCard");
	
	var sunlinkOLTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > SUNLINK ONLINE</div>", preneed: "<div class='numFormat'>"+sunlinkOLData.cashierTotals.pnTotal+"</div>",dsPN: "",vdsPN:"",gafPeso:"<div class='numFormat'>"+sunlinkOLData.cashierTotals.gafTotal+"</div>",dsGaf:"",vdsGaf:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var sunlinkHSBCPData = generateSLFPISectionData("totalSunlinkHsbcPeso","slfpiCard");
	
	var sunlinkHSBCPTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > SUNLINK HSBC PESO</div>", preneed: "<div class='numFormat'>"+sunlinkHSBCPData.cashierTotals.pnTotal+"</div>",dsPN: "",vdsPN:"",gafPeso:"<div class='numFormat'>"+sunlinkHSBCPData.cashierTotals.gafTotal+"</div>",dsGaf:"",vdsGaf:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var epsBPIData = generateSLFPISectionData("totalEpsBpi","slfpiCard");
	
	var epsBPITotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > EPS - BPI</div>", preneed: "<div class='numFormat'>"+epsBPIData.cashierTotals.pnTotal+"</div>",dsPN: "",vdsPN:"",gafPeso:"<div class='numFormat'>"+epsBPIData.cashierTotals.gafTotal+"</div>",dsGaf:"",vdsGaf:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var cardGafData = generateSLFPISectionData("totalCardGaf","slfpiCard");
	
	var cardGafTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > GAF</div>", preneed: "<div class='numFormat'>"+cardGafData.cashierTotals.pnTotal+"</div>",dsPN: "",vdsPN:"",gafPeso:"<div class='numFormat'>"+cardGafData.cashierTotals.gafTotal+"</div>",dsGaf:"",vdsGaf:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var areaCardTotals = getSLFPIAreaTotal([posBPIData,posCTIBData,posHSBCData,posSCBData,posRCBCData,posBDOData,mdsBPIData,mdsCTIBData,mdsHSBCData,mdsSBCData,mdsRCBCData,mdsBDOData,autoCTBData,autoSCBData,autoRCBCData,sunlinkOLData,sunlinkHSBCPData,epsBPIData,cardGafData]);	
	
	var pesoCardTotal = [{product: "<div class='totalRow'>&nbsp;</div>",cashier:"<div class='totalRow'><div class='alignRight'>TOTAL PESO CARD</div></div>", preneed: "<div class='totalRow'><div class='numFormat'>"+areaCardTotals.pn+"</div></div>",dsPN: "<div class='totalRow'>&nbsp;</div>",vdsPN:"<div class='totalRow'>&nbsp;</div>",gafPeso:"<div class='totalRow'><div class='numFormat'>"+areaCardTotals.gafPeso+"</div></div>",dsGaf:"<div class='totalRow'>&nbsp;</div>",vdsGaf:"<div class='totalRow'>&nbsp;</div>",ccmNotes:"<div class='totalRow'>&nbsp;</div>",ppaNotes:"<div class='totalRow'>&nbsp;</div>",findings:"<div class='totalRow'>&nbsp;</div>"}];
	
	var exemptionHeader = [{product: "<div class='alignLeft' cdTarget='slfpiExemptions' onclick='collapseDivision(this)'>EXEMPTIONS</div>",cashier:"", preneed: "",dsPN: "",vdsPN:"",gafPeso:"",dsGaf:"",vdsGaf:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var exemptionData = generateSLFPISectionData("reversalProduct","slfpiExemptions");
	
	var exemptionTotal = [{product: "<div class='totalRow'>&nbsp;</div>",cashier:"<div class='totalRow'><div class='alignRight'>TOTAL EXEMPTIONS</div></div>", preneed: "<div class='totalRow'><div class='numFormat'>"+exemptionData.cashierTotals.pnTotal+"</div></div>",dsPN: "<div class='totalRow'>&nbsp;</div>",vdsPN:"<div class='totalRow'>&nbsp;</div>",gafPeso:"<div class='totalRow'><div class='numFormat'>"+exemptionData.cashierTotals.gafTotal+"</div></div>",dsGaf:"<div class='totalRow'>&nbsp;</div>",vdsGaf:"<div class='totalRow'>&nbsp;</div>",ccmNotes:"<div class='totalRow'>&nbsp;</div>",ppaNotes:"<div class='totalRow'>&nbsp;</div>",findings:"<div class='totalRow'>&nbsp;</div>"}];
	
	var totalSLFPICashiersCollection = getSLFPIAreaTotal([cashNonCounterData,cashCounterData,checkOnUsData,checkLocalData,checkRegionalData,checkNonCounterData,checkOTData,postalMoneyOrderData,checkGafData,paymFrInsData,posBPIData,posCTIBData,posHSBCData,posSCBData,posRCBCData,posBDOData,mdsBPIData,mdsCTIBData,mdsHSBCData,mdsSBCData,mdsRCBCData,mdsBDOData,autoCTBData,autoSCBData,autoRCBCData,sunlinkOLData,sunlinkHSBCPData,epsBPIData,cardGafData]);
	
	var cashierTotalHeader = [{product: "<div class='alignLeft' cdTarget='slfpiGTPC' onclick='collapseDivision(this)'>Grand Total Per Cashier</div>",cashier:"", preneed: "",dsPN: "",vdsPN:"",gafPeso:"",dsGaf:"",vdsGaf:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var cashierTotal = getSLFPIGrandTotalPerCashier("slfpiGTPC");
	
	var cashierTotalRow = [{product: "<div class='totalRow'>&nbsp;</div>",cashier:"<div class='totalRow'><div class='alignRight'>TOTAL EXEMPTIONS</div></div>", preneed: "<div class='totalRow'><div class='numFormat'>"+numberWithCommas(totalSLFPICashiersCollection.pn)+"</div></div>",dsPN: "<div class='totalRow'>&nbsp;</div>",vdsPN:"<div class='totalRow'>&nbsp;</div>",gafPeso:"<div class='totalRow'><div class='numFormat'>"+numberWithCommas(totalSLFPICashiersCollection.gafPeso)+"</div></div>",dsGaf:"<div class='totalRow'>&nbsp;</div>",vdsGaf:"<div class='totalRow'>&nbsp;</div>",ccmNotes:"<div class='totalRow'>&nbsp;</div>",ppaNotes:"<div class='totalRow'>&nbsp;</div>",findings:"<div class='totalRow'>&nbsp;</div>"}];
	
	var slfpiSummaryData = [{product: "",cashier:"CONSOLIDATED GRAND TOTAL", preneed: "<div class='numFormat'>"+numberWithCommas(slfpiConsolidatedTotals.pn)+"</div>",dsPN: "",vdsPN:"",gafPeso:"<div class='numFormat'>"+numberWithCommas(slfpiConsolidatedTotals.gafPeso)+"</div>",dsGaf:"",vdsGaf:"",ccmNotes:"",ppaNotes:"",findings:""},
	                        {product: "",cashier:"TOTAL CASHIERS COLLECTION", preneed:"<div class='numFormat'>"+numberWithCommas(totalSLFPICashiersCollection.pn)+"</div>",dsPN: "",vdsPN:"",gafPeso:"<div class='numFormat'>"+numberWithCommas(totalSLFPICashiersCollection.gafPeso)+"</div>",dsGaf:"",vdsGaf:"",ccmNotes:"",ppaNotes:"",findings:""},
	                        {product: "",cashier:"", preneed: compareTotalsImg(totalSLFPICashiersCollection.pn,numberWithCommas(slfpiConsolidatedTotals.pn)),dsPN: "",vdsPN:"",gafPeso:compareTotalsImg(totalSLFPICashiersCollection.gafPeso,numberWithCommas(slfpiConsolidatedTotals.gafPeso)),dsGaf:"",vdsGaf:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	new TableContainer({
		divName : slfpiSummaryDivDetails.divName,
		nodeName : slfpiSummaryDivDetails.nodeName,
		width : "1000px",
		id : slfpiSummaryDivDetails.tableId,
		layout : slfpiColumns,
		tableClass: "balancingToolSummary",
		data: slfpiSummaryData
	}).startUp();
	
	var slfpiSectionData = [];
	
	appendJsons([slfpiSectionData,pesoCashHeader,cashNonCounterData.prodTotals,cncTotal,cashCounterData.prodTotals,ccTotal,areaCashTotal,pesoCheckHeader,checkOnUsData.prodTotals,chouTotal,checkLocalData.prodTotals,chlTotal,checkRegionalData.prodTotals,chrTotal,checkNonCounterData.prodTotals,chncTotal,checkOTData.prodTotals,chotTotal,postalMoneyOrderData.prodTotals,postalMoneyOrderTotal,checkGafData.prodTotals,checkGafTotal,paymFrInsData.prodTotals,paymFrInsTotal,areaCheckTotal,nonCashHeader,nonCashData.prodTotals,nonCashTotal,pesoCardHeader,posBPIData.prodTotals,posBPITotal,posCTIBData.prodTotals,posCTIBTotal,posHSBCData.prodTotals,posHSBCTotal,posSCBData.prodTotals,posSCBTotal,posRCBCData.prodTotals,posRCBCTotal,posBDOData.prodTotals,posBDOTotal,mdsBPIData.prodTotals,mdsBPITotal,mdsCTIBData.prodTotals,mdsCTIBTotal,mdsHSBCData.prodTotals,mdsHSBCTotal,mdsSBCData.prodTotals,mdsSBCTotal,mdsRCBCData.prodTotals,mdsRCBCTotal,mdsBDOData.prodTotals,mdsBDOTotal,autoCTBData.prodTotals,autoCTBTotal,autoSCBData.prodTotals,autoSCBTotal,autoRCBCData.prodTotals,autoRCBCTotal,sunlinkOLData.prodTotals,sunlinkOLTotal,sunlinkHSBCPData.prodTotals,sunlinkHSBCPTotal,epsBPIData.prodTotals,epsBPITotal,cardGafData.prodTotals,cardGafTotal,pesoCardTotal,cashierTotalHeader,cashierTotal,cashierTotalRow,exemptionHeader,exemptionData.prodTotals,exemptionTotal,generateSnapshotSection(slfpiSnapshot)]);
	
	new TableContainer({
		divName : slfpiDivDetails.divName,
		nodeName : slfpiDivDetails.nodeName,
		width : "1000px",
		id : slfpiDivDetails.tableId,
		layout : slfpiColumnsNH,
		tableClass: "balancingToolSummary",
		data: slfpiSectionData
	}).startUp();
	
	var slfpiTableDiv = document.getElementById(slfpiTableDivId);

	slfpiTableDiv.style.display = "none";
	
	});
}


var slfpiSecData = [];

function mergeSLFPICashierProductTypes(){
	var cashierRep = [];
	for(c in slfpiData){
		//using jQuery.inArray instead of Array.indexOf because it is not supported in IE8 and below. thanks IE.
		if(jQuery.inArray(slfpiData[c].cashierId+"~"+slfpiData[c].product,cashierRep) == -1){
			var pushedData = "";
			pushedData = pushedData.concat(slfpiData[c].cashierId);
			pushedData = pushedData.concat("~");
			pushedData = pushedData.concat(slfpiData[c].product);
			cashierRep.push(pushedData);
		}
	}
	
	for(var a = 0; a < cashierRep.length; a++){
		slfpiSecData.push(mergeSLFPICashierData(cashierRep[a]));
	}
		
}

function getSLFPIAreaTotal(area){
	var tots = {};
	var pn = 0;
	var gafPeso = 0;
	for(a in area){
		pn = pn + parseFloat(area[a].cashierTotals.pnTotal.replace(/,/g,""));
		gafPeso = gafPeso + parseFloat(area[a].cashierTotals.gafTotal.replace(/,/g,""));
	}
	tots.pn = numberWithCommas(parseFloat(pn).toFixed(2));
	tots.gafPeso = numberWithCommas(parseFloat(gafPeso).toFixed(2));
	return tots;
}

function generateSLFPISectionData(section,cdTarget){
	var sectionObj = {};
	var sectionCashierTotals = {};
	var pnTotal = 0;
	var gafTotal = 0;
	var  returnCNC = [];
	var firstRowFlag = true;
	for(a in slfpiSecData){
		var tempCNC = {};
		if(slfpiSecData[a].product == section){
			var isCashDepo=false;
			var isCheckDepo=false;
			if(firstRowFlag){
				switch(section){
					case "totalCashNonCounter": tempCNC.product = "<div class='alignLeft'>Non Counter</div>";break;
					case "totalCashCounter": tempCNC.product = "<div class='alignLeft'>Counter</div>";isCashDepo=true;break;
					case "totalCheckOnUs": tempCNC.product = "<div class='alignLeft'>On Us</div>";isCheckDepo=true;checkType="ON_US";break;
					case "totalCheckRegional": tempCNC.product = "<div class='alignLeft'>Regional</div>";isCheckDepo=true;checkType="REGIONAL";break;
					case "totalCheckNonCounter": tempCNC.product = "<div class='alignLeft'>Non Counter</div>";break;
					case "totalCheckOT": tempCNC.product = "<div class='alignLeft'>OT</div>";isCheckDepo=true;checkType="OT";break;
					case "totalCheckLocal": tempCNC.product = "<div class='alignLeft'>Local</div>";isCheckDepo=true;checkType="LOCAL";break;
					case "totalPmo": tempCNC.product = "<div class='alignLeft'>Postal Money Order</div>";break;
					//case "totalBankOTCCheckPayment" : tempCNC.product = "<div class='alignLeft'>Bank OTC Check Payment</div>";break;
					case "totalNonCash": tempCNC.product = "<div class='alignLeft'></div>";break;
					case "totalPosBpi": tempCNC.product = "<div class='alignLeft'>POS - BPI</div>";break;
					case "totalPosCtb": tempCNC.product = "<div class='alignLeft'>POS - CITIBANK</div>";break;
					case "totalPosHsbc": tempCNC.product = "<div class='alignLeft'>POS - HSBC</div>";break;
					case "totalPosScb": tempCNC.product = "<div class='alignLeft'>POS - SCB</div>";break;
					case "totalPosRcbc": tempCNC.product = "<div class='alignLeft'>POS - RCBC</div>";break;
					case "totalPosBdo": tempCNC.product = "<div class='alignLeft'>POS - BDO</div>";break;
					case "totalMdsBpi": tempCNC.product = "<div class='alignLeft'>MDS - BPI</div>";break;
					case "totalMdsCtb": tempCNC.product = "<div class='alignLeft'>MDS - CITIBANK</div>";break;
					case "totalMdsHsbc": tempCNC.product = "<div class='alignLeft'>MDS - HSBC</div>";break;
					case "totalMdsSbc": tempCNC.product = "<div class='alignLeft'>MDS - SCB</div>";break;
					case "totalMdsRcbc": tempCNC.product = "<div class='alignLeft'>MDS - RCBC</div>";break;
					case "totalMdsBdo": tempCNC.product = "<div class='alignLeft'>MDS - BDO</div>";break;
					case "totalAutoCtb": tempCNC.product = "<div class='alignLeft'>CITIBANK (AUTOCHARGE)</div>";break;
					case "totalAutoScb": tempCNC.product = "<div class='alignLeft'>SCB (AUTOCHARGE)</div>";break;
					case "totalAutoRcbc": tempCNC.product = "<div class='alignLeft'>RCBC - AUTOCHARGE</div>";break;
					case "totalSunlinkOnline": tempCNC.product = "<div class='alignLeft'>SUNLINK ONLINE</div>";break;
					case "totalSunlinkHsbcPeso": tempCNC.product = "<div class='alignLeft'>SUNLINK HSBC PESO</div>";break;
					case "totalEpsBpi": tempCNC.product = "<div class='alignLeft'>EPS - BPI</div>";break;
					case "totalCheckGaf": tempCNC.product = "<div class='alignLeft'>GAF</div>";isCheckDepo=true;checkType="LOCAL";break;
					case "paymentFromInsurer": tempCNC.product = "<div class='alignLeft'>Payment from Insurer</div>";break;
					case "totalCardGaf": tempCNC.product = "<div class='alignLeft'>GAF</div>";isCheckDepo=true;checkType="LOCAL";break;
					case "reversalProduct" : tempCNC.product = "";break;
					default: alert("Unknown Product Total: " + section); break;
				}
			} else{
				tempCNC.product = "";		
				switch(section){
					case "totalCashCounter": isCashDepo=true;break;
					case "totalCheckOnUs": isCheckDepo=true;checkType="ON_US";break;
					case "totalCheckRegional": isCheckDepo=true;checkType="REGIONAL";break;
					case "totalCheckOT": isCheckDepo=true;checkType="OT";break;
					case "totalCheckLocal": isCheckDepo=true;checkType="LOCAL";break;
					case "totalCheckGaf": isCheckDepo=true;checkType="LOCAL";break;
				}
			}
			
			tempCNC.cashier = "<div class='alignLeft'>"+slfpiSecData[a].cashierName+"</div>";
			if(isSLFPIConfirmed(slfpiSecData[a].cashierId)){
				if(section == "reversalProduct"){					
					if(parseFloat(slfpiSecData[a].preneed) != 0){
						tempCNC.preneed = "<div class='alignRight'><a style='cursor:pointer;text-decoration:underline;color:blue;'onclick='openSPReversal("+getBalancingToolProductId(slfpiSecData[a].cashierId,"PN",section,slfpiData)+");'>"+numberWithCommas(parseFloat(slfpiSecData[a].preneed).toFixed(2))+"</a></div>";
					} else{
						tempCNC.preneed = "<div class='alignRight'>"+numberWithCommas(parseFloat(slfpiSecData[a].preneed).toFixed(2))+"</div>";
					}					
					if(parseFloat(slfpiSecData[a].gafPeso) != 0){
						tempCNC.gafPeso = "<div class='alignRight'><a style='cursor:pointer;text-decoration:underline;color:blue;'onclick='openSPReversal("+getBalancingToolProductId(slfpiSecData[a].cashierId,"GAF",section,slfpiData)+");'>"+numberWithCommas(parseFloat(slfpiSecData[a].gafPeso).toFixed(2))+"</a></div>";
					} else{
						tempCNC.gafPeso = "<div class='alignRight'>"+numberWithCommas(parseFloat(slfpiSecData[a].gafPeso).toFixed(2))+"</div>";
					}	
				} else{
					tempCNC.preneed = "<div class='alignRight'>"+numberWithCommas(parseFloat(slfpiSecData[a].preneed).toFixed(2))+"</div>";
					tempCNC.gafPeso = "<div class='alignRight'>"+numberWithCommas(parseFloat(slfpiSecData[a].gafPeso).toFixed(2))+"</div>";
					if(isCashDepo){
						if(parseFloat(slfpiSecData[a].preneed) > 0){
							tempCNC.dsPN  = getCashDepoURL("SLFPI","PRENEED","PHP",slfpiSecData[a].cashierId);
							tempCNC.vdsPN  = getValidatedCashDepoURL("SLFPI","PRENEED","PHP",slfpiSecData[a].cashierId);
						}
						if(parseFloat(slfpiSecData[a].gafPeso) > 0){
							tempCNC.dsGaf = getCashDepoURL("SLFPI","GAF","PHP",slfpiSecData[a].cashierId);
							tempCNC.vdsGaf = getValidatedCashDepoURL("SLFPI","GAF","PHP",slfpiSecData[a].cashierId);
						}
					}else if(isCheckDepo){
						if(parseFloat(slfpiSecData[a].preneed) > 0){
							tempCNC.dsPN  = getChequeDepoURL("SLFPI","PRENEED","PHP",slfpiSecData[a].cashierId,checkType);
							tempCNC.vdsPN  = getValidatedChequeDepoURL("SLFPI","PRENEED","PHP",slfpiSecData[a].cashierId,checkType);
						}
						if(parseFloat(slfpiSecData[a].gafPeso) > 0){
							tempCNC.dsGaf = getChequeDepoURL("SLFPI","GAF","PHP",slfpiSecData[a].cashierId,checkType);
							tempCNC.vdsGaf = getValidatedChequeDepoURL("SLFPI","GAF","PHP",slfpiSecData[a].cashierId,checkType);
						}
					}
				}
				pnTotal = pnTotal + parseFloat(slfpiSecData[a].preneed);
				gafTotal = gafTotal + parseFloat(slfpiSecData[a].gafPeso);
			} else {
				tempCNC.preneed = "<div class='alignCenter'>-</div>";
				tempCNC.gafPeso = "<div class='alignCenter'>-</div>";
			}
			if(jQuery.inArray(section,excludeFromCashierTotal) == -1){
				slfpiConsolidatedTotals.pn = (parseFloat(slfpiConsolidatedTotals.pn) + parseFloat(slfpiSecData[a].preneed)).toFixed(2);
				slfpiConsolidatedTotals.gafPeso = (parseFloat(slfpiConsolidatedTotals.gafPeso) + parseFloat(slfpiSecData[a].gafPeso)).toFixed(2);
			}
			tempCNC.ccmNotes = "<div class='alignCenter'>"+getNote(rowIndex,"CCM")+"</div>";
			tempCNC.ppaNotes = "<div class='alignCenter'>"+getNote(rowIndex,"PPA")+"</div>";
			tempCNC.findings = "<div class='alignCenter'  collapseTarget='"+cdTarget+"'>"+getNote(rowIndex,"FINDINGS")+"</div>";			
			rowIndex++;
			firstRowFlag = false;		
			returnCNC.push(tempCNC);
		}
	}
	sectionCashierTotals.pnTotal = numberWithCommas(parseFloat(pnTotal).toFixed(2));
	sectionCashierTotals.gafTotal = numberWithCommas(parseFloat(gafTotal).toFixed(2));
	sectionObj.prodTotals = returnCNC;
	sectionObj.cashierTotals = sectionCashierTotals;
	return sectionObj;
}

function mergeSLFPICashierData(cashierProduct){
	var cpArr = cashierProduct.split("~");
	var cashierId = cpArr[0];
	var product = cpArr[1];
	var tempCashier = {};
	for(a in slfpiData){
		if(slfpiData[a].cashierId == cashierId && slfpiData[a].product == product){
			if(slfpiData[a].productCode == "PN"){
				tempCashier.preneed = slfpiData[a].total;
			} else if(slfpiData[a].productCode == "GAF"){
				tempCashier.gafPeso = slfpiData[a].total;
			}
			tempCashier.cashierId = slfpiData[a].cashierId;
			tempCashier.cashierName = slfpiData[a].cashierName;
			tempCashier.product = product;
		}
	}
	return tempCashier;
}

function getSLFPIGrandTotalPerCashier(cdTarget){
	var returnSection = [];
	var c = 0;
	for(c in cashierList){
		var preneed = 0;
		var gafPeso = 0;
		var d=0;
		for(d in slfpiData){
			if(jQuery.inArray(slfpiData[d].product,excludeFromCashierTotal) == -1 && slfpiData[d].cashierId.toUpperCase() == cashierList[c].id.toUpperCase()){
				switch(slfpiData[d].productCode){
					case "PN": preneed = preneed + parseFloat(slfpiData[d].total);break;
					case "GAF": gafPeso = gafPeso  + parseFloat(slfpiData[d].total);break;
				}
			}
		}
		var tempCashier = {};
		tempCashier.cashier = "<div class='alignLeft'  collapseTarget='"+cdTarget+"'>"+cashierList[c].name+"</div>";
		if(isSLFPIConfirmed(cashierList[c].id)){
			tempCashier.preneed = "<div class='alignRight'>"+numberWithCommas(preneed.toFixed(2))+"</div>";
			tempCashier.gafPeso = "<div class='alignRight'>"+numberWithCommas(gafPeso.toFixed(2))+"</div>";
		} else {
			tempCashier.preneed = "<div class='alignCenter'>-</div>";
			tempCashier.gafPeso = "<div class='alignCenter'>-</div>";
		}
		returnSection.push(tempCashier);
	}
	return returnSection;
}

var slfpiConsolidatedTotals = {pn: "0.00", gafPeso: "0.00"};