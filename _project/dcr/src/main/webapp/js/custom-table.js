/*
 * Created on January 2012
 * John Lesty A. Tolentino
 * 
 * Notes: This class requires jquery
 * Modified: April 26, 2012 added jquery classes
 */
var _globalTableContainer = {};
var TableContainer = (function() {
	
	var cls = function(args) {
		var data;
		var layout;
		var id;
		var width;
		var divName;
		var nodeName;
//		var totalField; /* in progress */
		var cellPadding;
		var cellSpacing;
		var tableClass;
		var disableAlternateColor;
		
		this.setProperty = function(propertyName,value) {
			this[propertyName] = value;
		};
		
		for (variable in args) {
			this.setProperty(variable,args[variable]);
		}
		// for data null problems
		if (!this.data) { this.data = []; }
		
		this._getContainerDiv = function() { return this.containerDiv; };
		this._getContainerNode = function() { return this.containerNode; };
		this._getTable = function() { return document.getElementById(this.id); };
		this._getTableBody = function() { return document.getElementById(this.id).childNodes[1]; };
		this.registerTable = function() {
			//add this to globalTableContainers
			// will overwrite previous entries if the same id
			_globalTableContainer[this.id] = this;
			
		};
		this.startUp = function() {
			var _containerDiv = document.getElementById(this["divName"]);//changed from this.divName
			var _containerNode = document.getElementById(this["nodeName"]);//changed from this.divName
			
			
			//create table container and set id
			var table = document.createElement("table");
			table.id = this.id;
			if (this.cellSpacing == null) {
				table.setAttribute("cellspacing","0px");
			} else {
				table.setAttribute("cellspacing",this.cellSpacing);
			}
			if (this.cellPadding == null) {
				table.setAttribute("cellpadding","0px");
			} else {
				table.setAttribute("cellpadding",this.cellPadding);
			}
			table.className = "customTable ";
			if (this.tableClass) table.className += this.tableClass;
//			$(table).addClass("customTable");
			
			//create main header dom
			var theader = document.createElement("thead");
			var headerRow = document.createElement("tr");
			
			//create header elements
			var layoutLength = this.layout.length;
			var totalWidth = 0;
			for (var i = 0; i < layoutLength; i++) {
				var currentField = this.layout[i];
				var header = document.createElement("th");
				header.innerHTML = currentField["name"];
				header.style.width = currentField["width"];
				//append this new header to headerRow
				headerRow.appendChild(header);
				totalWidth += parseInt(header.style.width);
			}
			if (!this.width || (totalWidth < parseInt(this.width) - 20)) {
				_containerDiv.style.cssText += "overflow-x:auto;";
			}
			
			//append header row
			theader.appendChild(headerRow);
			//append header to table
			table.appendChild(theader);
			
			
			//create body
			var tbody = document.createElement("tbody");
			//data
			var dataSize = this.data.length;
			for (var i = 0; i < dataSize; i++) {
				//current data
				var currentData = this.data[i];
				//create new data row
				var row = document.createElement("tr");
				
				if (this.disableAlternateColor != true && i%2 == 1) {
					//change this to class later on so that it can be changed in css
//					row.style.backgroundColor = "#CCFFFF";
					row.className = "oddRow";
				} else {
					row.className = "evenRow";
				}
				
				//loop through layout to retrieve all tds, add this to the row
				var originalData = currentData;
				for (var l = 0; l < layoutLength; l++) {
					//reset the currentData
					currentData = originalData;
					//create td
					var _td = document.createElement("td");
					//field name
					var _field = this.layout[l].field;
					//field names
					var _fields = this.layout[l].fields;
					
					//styles
					var _styles = this.layout[l].styles;
					
					
					var _fieldValue;
					if (_field) {
						//retrieve bottom field value
						var _splitFieldName = _field.split(".");
						if (_splitFieldName.length > 1) {
							for (var idx = 0; idx < _splitFieldName.length - 1; idx++) {
								var tempData = currentData[_splitFieldName[idx]];
								if (tempData) {
									currentData = tempData;
								}
							}
							_field = _splitFieldName[_splitFieldName.length - 1];
						}
					//check for null values on currentData for _field
						_fieldValue = (null == currentData[_field] ? " " : currentData[_field]);
					} else {
						if (_fields) {
							_fieldValue = [];
							for (var fIdx = 0; fIdx < _fields.length; fIdx++) {
								_fieldValue.push(currentData[_fields[fIdx]]);
							}
						} else {
							_fieldValue = " ";
						}
					}
					
					//formatter
					var _formatter = this.layout[l].formatter;
					//check if current field has formatter
					if (null != _formatter) {
						//call formatter if not null, pass the currentData[field value]
						var returnedValue = _formatter(_fieldValue,i,_field, this);
						if (returnedValue.nodeType && returnedValue.nodeType == 1) {
							_td.appendChild(returnedValue);
						} else {
							_td.innerHTML = returnedValue;
						}
					} else {
						_td.innerHTML = _fieldValue;
					}
					
					if (null != _styles) {
						_td.style.cssText = _styles;
//						_td.setAttribute("style",_styles); // apparently this doesn't work on IE
					}
					row.appendChild(_td);
				}
				//append the created row to the body
				tbody.appendChild(row);
			}
			
			//append tbody to table
			table.appendChild(tbody);
			//append table to the container node
			_containerNode.appendChild(table);
			_containerNode.style.width = this.width;
			_containerDiv.style.display = "block";
			this.registerTable();
		};
		this.destroyTable = function() {
			var containerNode = document.getElementById(this.nodeName);
			while (containerNode.hasChildNodes()) {
				containerNode.removeChild(containerNode.lastChild);
			}
			document.getElementById(this.divName).style.display = "none";
		};
		this.refreshTable = function(data) {
			if (data) {
				this.data = data;
			}
			this.destroyTable();
			this.startUp();
		};
		this.setData = function(newData) { this.data = newData; };
		this.getData = function() { return this.data; };
	};
	return cls;
})();





function resetOddEvenRows(tableObject){
	var rows = tableObject.getElementsByTagName("tr");
	var resetArray=[];
	
	for(var a=0; a< rows.length; a++){
		if(rows[a].style.display!="none"){
			resetArray.push(rows[a]);
		}
	}
	
	for(var b=0; b<resetArray.length; b++){
		if(b%2 == 0){
			resetArray[b].className = "oddRow";
		} else{
			resetArray[b].className = "evenRow";
		}
	}
	
}
