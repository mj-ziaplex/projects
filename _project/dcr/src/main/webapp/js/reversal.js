var centeredStyle = "text-align:center;";
var reversalTableLayout = [{
			name : "Date & Time",
			field : "dateTime",
			width : "20%",
			styles: centeredStyle
		},{
			name : "Reason",
			field : "remarks",
			width : "50%",
			styles: centeredStyle
		},{
			name : "Amount (PHP)",
			field : "amount",
			width : "20%",
			styles: centeredStyle
		},{
			name : "Actions",
			field : "actions",
			width : "10%",
			styles: centeredStyle
		}];

function initReversaTablePopUp(reversalData, productCode,
		dcrBalancingToolProductId, currencyId, productName, companyName){//generate divs dynamically,diff IDs.jQuery,but same class.CSS
		
		$(document).ready(function(){
			var outerNodeId = "reversalTableOuterNode" + productCode;
			var innerNodeId = "reversalTableInnerNode" + productCode;
			var contentNodeId = "allReversalTable" + productCode;
			
			//alert('Build Reversal Table, with unique DIV IDs, as follows: ');
			//alert('Outer DIV ID: ' + outerNodeId);
			//alert('Inner DIV ID: ' + innerNodeId);
			
			//MUST BE DIFFERENT DIV and NODE NAMES, FOR JQUERY
			var reversalDivs = document.getElementById("reversalDivsByProductId");
			
			var reversalTableOuterNode = document.createElement("div");
			reversalTableOuterNode.id = outerNodeId;				  //to be used in jquery below
			reversalTableOuterNode.className = "allReversalTableNode";//to be used in reversal.css
			var reversalTableInnerNode = document.createElement("div");
			reversalTableInnerNode.id = innerNodeId;
			reversalTableInnerNode.className = "allReversalNode";
			
			var addNewReversalEntryBtn = document.createElement("button");
			addNewReversalEntryBtn.className = "addNewReversalEntryBtn";
			addNewReversalEntryBtn.style.fontSize = "12px";
			//set Add New Reversal Entry column
			if(companyName=='SLAMCIP' || companyName=='SLAMCID'){
				addNewReversalEntryBtn.innerHTML = "Add New SRF Cancel Entry";
			}else{
				addNewReversalEntryBtn.innerHTML = "Add New Reversal Entry";
			}
			addNewReversalEntryBtn.onclick = function() { callReversalForm(dcrBalancingToolProductId, currencyId, productName, 'add', companyName); };
			
			var spanAddNewReversalEntryBtn = document.createElement("span");
			spanAddNewReversalEntryBtn.className = "spanAddNewReversalEntryBtn";
			spanAddNewReversalEntryBtn.appendChild(addNewReversalEntryBtn);
			
			reversalTableOuterNode.appendChild(reversalTableInnerNode);
			reversalTableOuterNode.appendChild(spanAddNewReversalEntryBtn);
			reversalDivs.appendChild(reversalTableOuterNode);
			
			new TableContainer({
				divName : outerNodeId,
				nodeName : innerNodeId,
				width : "100%",
				id : contentNodeId,
				layout : reversalTableLayout,
				tableClass : "reversalBorderedTable"
			}).startUp();
			
			var allReversalTable = _globalTableContainer[contentNodeId];
			
			allReversalTable.setData(reversalData);
			allReversalTable.refreshTable();
			
			var reversalTable = document.getElementById(contentNodeId);
			//set Amount(PHP) column
			if(currencyId==1){
				reversalTable.rows[0].cells[2].innerHTML = "Amount (PHP)";
			}else if(currencyId==2){
				reversalTable.rows[0].cells[2].innerHTML = "Amount (USD)";
			}
			//set Reason For Reversal column
			if(companyName=='SLAMCIP' || companyName=='SLAMCID'){
				reversalTable.rows[0].cells[1].innerHTML = "Reason For SRF Cancellation";
			}else{
				reversalTable.rows[0].cells[1].innerHTML = "Reason";
			}
		});
}

function resetAddEditReversalForm(){
	document.getElementById("addEditReversalForm").reset();
}

function showReversalsTable(productCode, companyName){
	//alert('Show the reversals of Product Code: '+ productCode);
	//alert('So pop up the dialog with div id: reversalTableOuterNode'+productCode);
	
	//set dialog window title
	var title = "View Reversals/Unposted";
	if(companyName=='SLAMCIP' || companyName=='SLAMCID'){
		title = "View SRF Cancellations";
	}
	
	var divToPopUp = "#reversalTableOuterNode" + productCode;
	
	$(function() {
		$( divToPopUp ).dialog({
			autoOpen: true,
			height: "auto",
			minHeight: 600,
			width: 800,
			modal: true,
			closeOnEscape: true,
			draggable: false,
			resizable: false,
			dialogClass: "reversalTableClass",
			title: title
		}).dialog('widget').position({ my: 'center', at: 'center', of: $(document.getElementById("mainTemplateBodyId")) });
	});
}

var callReversalForm = function showAddEditReversalForm(dcrBalancingToolProductId, currencyId, productName, actionType, companyName){
	//alert('dcrBalancingToolProductId: '+dcrBalancingToolProductId);
	//alert('currencyId: '+currencyId);
	
	//set dialog window title
	var title = "Reversals/Unposted Option";
	if(companyName=='SLAMCIP' || companyName=='SLAMCID'){
		title = "SRF Cancellation Option";
	}
	
	var form = document.getElementById("addEditReversalForm");
	//clear form after showing
	form.reset();
	
	//needed for adding a reversal
	document.getElementById("dcrBalancingToolProductId").value = dcrBalancingToolProductId;
	document.getElementById("currencyId").value = currencyId;
	
	//set which action is in the form
	var formActionVal = form.action;
	//alert('orig form action value: '+formActionVal);
	var methodName = formActionVal.substring(formActionVal.lastIndexOf("/")+1, formActionVal.search(".html"));
	//alert('methodName to change: '+methodName);
	if(actionType=='add'){
		form.action = formActionVal.replace(methodName,"addreversal");
		//alert('new form action value: '+form.action);
	}else if(actionType=='edit'){
		form.action = formActionVal.replace(methodName,"updatereversal");
		//alert('new form action value: '+form.action);
	}
	
	document.getElementById("productName").innerHTML = productName;//addt'l label
	document.getElementById("submitBtnId").style.fontSize = "12px";//can't style the ids in css
	document.getElementById("clearBtnId").style.fontSize = "12px";
	
	//alert('pop the form');
	$(function() {
		$( "#addEditReversalDiv" ).dialog({
			autoOpen: true,
			height: "auto",
			minHeight: 590,
			width: 490,
			modal: true,
			closeOnEscape: true,
			draggable: false,
			resizable: false,
			dialogClass: "reversalFormClass",
			title: title
		}).dialog('widget').position({ my: 'center', at: 'center', of: $(document.getElementById("mainTemplateBodyId")) });
	});
	
	var amountLabel = "Amount";
	var reasonLabel = "Reason";
	if(companyName=='SLAMCIP' || companyName=='SLAMCID'){
		amountLabel = "SRF Cancellation Amount";
		reasonLabel = "Reason For SRF Cancellation";
	}
	document.getElementById("amountLabel").innerHTML = amountLabel;
	document.getElementById("reasonLabel").innerHTML = reasonLabel;
}

function showEditReversalFormWithData(dcrBalancingToolProductId, currencyId, productName, 
		amount, remarks, dcrReversalId, actionType, companyName){
	showAddEditReversalForm(dcrBalancingToolProductId, currencyId, productName, actionType, companyName);
	document.getElementById("amount").value = amount;
	document.getElementById("reversalRemarks").value = remarks;
	
	var form = document.getElementById("addEditReversalForm");
	
	//needed for editing a reversal
	document.getElementById("dcrReversalId").value = dcrReversalId;
}