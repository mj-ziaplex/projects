var jobData = [];

function showResultsTable(data){
	
	$(document).ready(function(){
		
		destroyJobTable();
		
		var jobTableLayout = [{
			name: "Job Name",
			field: "jobName",
			width: "36%"
		},{
			name: "Status",
			field: "status",
			width: "10%"
		},{
			name: "Last Run",
			field: "lastRun",
			width: "34%"
		},{
			name: "Action",
			field: "action",
			width: "20%"
		}];
		
		new TableContainer({
			divName : "jobDataDiv",
			nodeName : "jobDataNode",
			width : "100%",
			id : "jobTable",
			layout : jobTableLayout,
			tableClass : "jobTable",
			data: data
		}).startUp();
		
	});

}

function destroyJobTable(){
	$("#jobTable").empty();
}

var datePickerWindow = null;

var tempDatePickerWindow = null;

function closeDatePickerWindow(){
	if(datePickerWindow != null || datePickerWindow != tempDatePickerWindow){
		datePickerWindow.close();
	}
}

function openDatePickerWindow(){
	closeDatePickerWindow();
	datePickerWindow = window.open("../jobData/dcrcreation.html", "_blank", "width=300,height=400,resizable=no,status=yes,scrollbars=no");
	tempDatePickerWindow = datePickerWindow;
}

function clearDate(){
	var searchDatePicker = document.getElementById("searchDatePicker");
	var dcrDate = document.getElementById("dcrDate");
	dcrDate.value = "";
	searchDatePicker.value = "";
}
function setDcrDateStr(val){
	var dcrDateStr = document.getElementById("dcrDate");
	dcrDateStr.value = val;
}