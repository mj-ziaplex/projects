var otherDollarColumns = [{
	name : "",
	field : "product",
	width : "15%"
},{
	name : "",
	field : "cashier",
	width : "22%"
},{
	name : "COB Dollar (COBD)",
	field : "cobDollar",
	width : "7%"
},{
	name : "DS",
	field : "dsCobd",
	width : "6%"
},{
	name : "VDS",
	field : "vdsCobd",
	width : "6%"
},{
	name : "Recon?",
	field : "reconciled",
	width : "6%"
},{
	name : "Recon By",
	field : "reconciledBy",
	width : "6%"
},{
	name : "Recon Date",
	field : "reconciledDate",
	width : "6%"
},{
	name : "CCM NOTES",
	field : "ccmNotes",
	width : "7%"
},{
	name : "CCQA NOTES",
	field : "ppaNotes",
	width : "7%"
},{
	name : "Findings",
	field : "findings",
	width : "8%"
}	
];

var otherDollarColumnsNH = [{
	name : "",
	field : "product",
	width : "15%"
},{
	name : "",
	field : "cashier",
	width : "22%"
},{
	name : "",
	field : "cobDollar",
	width : "7%"
},{
	name : "",
	field : "dsCobd",
	width : "6%"
},{
	name : "",
	field : "vdsCobd",
	width : "6%"
},{
	name : "",
	field : "reconciled",
	width : "6%"
},{
	name : "",
	field : "reconciledBy",
	width : "6%"
},{
	name : "",
	field : "reconciledDate",
	width : "6%"
},{
	name : "",
	field : "ccmNotes",
	width : "7%"
},{
	name : "",
	field : "ppaNotes",
	width : "7%"
},{
	name : "",
	field : "findings",
	width : "8%"
}	
];

function generateOtherDollarSection(otherDollarTableDivId,otherDollarDivDetails,otherDollarSummaryDivDetails){
	$(document).ready(function(){
			
	mergeOtherDollarCashierProductTypes();
	
	var dollarCashHeader = [{product: "<div class='alignLeft' cdTarget='otherDollarCash' onclick='collapseDivision(this)'>DOLLAR CASH</div>",cashier:"", cobDollar: "",dsCobd: "",vdsCobd:"",reconciled:"",reconciledBy:"",reconciledDate:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var cashNonCounterData = generateOtherDollarSectionData("totalCashNonCounter","otherDollarCash");
	
	var cncTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > Non Counter</div>", cobDollar: "<div class='numFormat'>"+cashNonCounterData.cashierTotals.otherDollarTotal+"</div>",dsCobd: "",vdsCobd:"",reconciled:"",reconciledBy:"",reconciledDate:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var cashCounterData = generateOtherDollarSectionData("totalCashCounter","otherDollarCash");
	
	var ccTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > Counter</div>", cobDollar: "<div class='numFormat'>"+cashCounterData.cashierTotals.otherDollarTotal+"</div>",dsCobd: "",vdsCobd:"",reconciled:"",reconciledBy:"",reconciledDate:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var dollarCashTotals = getOtherDollarAreaTotal([cashNonCounterData,cashCounterData]);
	
	var areaCashTotal = [{product: "<div class='totalRow'>&nbsp;</div>",cashier:"<div class='totalRow'><div class='alignRight'>TOTAL CASH</div></div>", cobDollar: "<div class='totalRow'><div class='numFormat'>"+dollarCashTotals.cobDollar+"</div></div>",dsCobd: "<div class='totalRow'>&nbsp;</div>",vdsCobd:"<div class='totalRow'>&nbsp;</div>",reconciled:"<div class='totalRow'>&nbsp;</div>",reconciledBy:"<div class='totalRow'>&nbsp;</div>",reconciledDate:"<div class='totalRow'>&nbsp;</div>",ccmNotes:"<div class='totalRow'>&nbsp;</div>",ppaNotes:"<div class='totalRow'>&nbsp;</div>",findings:"<div class='totalRow'>&nbsp;</div>"}];
	
	var dollarCheckHeader = [{product: "<div class='alignLeft' cdTarget='otherDollarCheck' onclick='collapseDivision(this)'>DOLLAR CHECK</div>",cashier:"", cobDollar: "",dsCobd: "",vdsCobd:"",reconciled:"",reconciledBy:"",reconciledDate:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var dollarChequeData = generateOtherDollarSectionData("totalDollarCheque","otherDollarCheck");
	
	var dolChTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > Dollar Cheque</div>", cobDollar: "<div class='numFormat'>"+dollarChequeData.cashierTotals.otherDollarTotal+"</div>",dsCobd: "",vdsCobd:"",reconciled:"",reconciledBy:"",reconciledDate:"",ccmNotes:"",ppaNotes:"",findings:""}];	
		
	var totDrawnMlaData = generateOtherDollarSectionData("totalUsCheckInManila","otherDollarCheck");
	
	var totDrawnMlaTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > US Cheque drawn in Manila</div>", cobDollar: "<div class='numFormat'>"+totDrawnMlaData.cashierTotals.otherDollarTotal+"</div>",dsCobd: "",vdsCobd:"",reconciled:"",reconciledBy:"",reconciledDate:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var totDrawnOutPHData = generateOtherDollarSectionData("totalUsCheckOutPh","otherDollarCheck");
	
	var totDrawnOutPHTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > US Cheque drawn in Manila</div>", cobDollar: "<div class='numFormat'>"+totDrawnOutPHData.cashierTotals.otherDollarTotal+"</div>",dsCobd: "",vdsCobd:"",reconciled:"",reconciledBy:"",reconciledDate:"",ccmNotes:"",ppaNotes:"",findings:""}];

	var checkNonCounterData = generateOtherDollarSectionData("totalCheckNonCounter","otherDollarCheck");
	
	var chncTotal = [{product: "",cashier:"<div class='alignRight'>Sub Total > Non Counter</div>", cobDollar: "<div class='numFormat'>"+checkNonCounterData.cashierTotals.otherDollarTotal+"</div>",dsCobd: "",vdsCobd:"",reconciled:"",reconciledBy:"",reconciledDate:"",ccmNotes:"",ppaNotes:"",findings:""}];

	var dollarCheckTotals = getOtherDollarAreaTotal([dollarChequeData,totDrawnMlaData,totDrawnOutPHData,checkNonCounterData]);
	
	var areaCheckTotal = [{product: "<div class='totalRow'>&nbsp;</div>",cashier:"<div class='totalRow'><div class='alignRight'>TOTAL CHECK</div></div>", cobDollar: "<div class='totalRow'><div class='numFormat'>"+dollarCheckTotals.cobDollar+"</div></div>",dsCobd: "<div class='totalRow'>&nbsp;</div>",vdsCobd:"<div class='totalRow'>&nbsp;</div>",reconciled:"<div class='totalRow'>&nbsp;</div>",reconciledBy:"<div class='totalRow'>&nbsp;</div>",reconciledDate:"<div class='totalRow'>&nbsp;</div>",ccmNotes:"<div class='totalRow'>&nbsp;</div>",ppaNotes:"<div class='totalRow'>&nbsp;</div>",findings:"<div class='totalRow'>&nbsp;</div>"}];
		
	var totalOTHERDCashiersCollection = getOtherDollarAreaTotal([cashNonCounterData,cashCounterData,dollarChequeData,totDrawnMlaData,totDrawnOutPHData,checkNonCounterData]);
	
	var cashierTotalHeader = [{product: "<div class='alignLeft' cdTarget='otherDollarGTPC' onclick='collapseDivision(this)'>Grand Total Per Cashier</div>",cashier:"", cobDollar: "",dsCobd: "",vdsCobd:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	var cashierTotal = getOTHERDGrandTotalPerCashier("otherDollarGTPC");
	
	var cashierTotalRow = [{product: "<div class='totalRow'>&nbsp;</div>",cashier:"<div class='totalRow'><div class='alignRight'>TOTAL COLLECTION</div></div>", cobDollar: "<div class='totalRow'><div class='numFormat'>"+numberWithCommas(totalOTHERDCashiersCollection.cobDollar)+"</div></div>",dsCobd: "<div class='totalRow'>&nbsp;</div>",vdsCobd:"<div class='totalRow'>&nbsp;</div>",reconciled:"<div class='totalRow'>&nbsp;</div>",reconciledBy:"<div class='totalRow'>&nbsp;</div>",reconciledDate:"<div class='totalRow'>&nbsp;</div>",ccmNotes:"<div class='totalRow'>&nbsp;</div>",ppaNotes:"<div class='totalRow'>&nbsp;</div>",findings:"<div class='totalRow'>&nbsp;</div>"}];
	
	var otherDollarSummaryData = [{product: "",cashier:"CONSOLIDATED GRAND TOTAL", cobDollar: "<div class='numFormat'>"+numberWithCommas(otherDollarConsolidatedTotals.cobDollar)+"</div>",dsCobd: "",vdsCobd:"",reconciled:"",reconciledBy:"",reconciledDate:"",ccmNotes:"",ppaNotes:"",findings:""},
	                        {product: "",cashier:"TOTAL CASHIERS COLLECTION", cobDollar:"<div class='numFormat'>"+numberWithCommas(totalOTHERDCashiersCollection.cobDollar)+"</div>",dsCobd: "",vdsCobd:"",reconciled:"",reconciledBy:"",reconciledDate:"",ccmNotes:"",ppaNotes:"",findings:""},
	                        {product: "",cashier:"", cobDollar: compareTotalsImg(numberWithCommas(otherDollarConsolidatedTotals.cobDollar),totalOTHERDCashiersCollection.cobDollar),dsCobd: "",vdsCobd:"",reconciled:"",reconciledBy:"",reconciledDate:"",ccmNotes:"",ppaNotes:"",findings:""}];
	
	new TableContainer({
		divName : otherDollarSummaryDivDetails.divName,
		nodeName : otherDollarSummaryDivDetails.nodeName,
		width : "900px",
		id : otherDollarSummaryDivDetails.tableId,
		layout : otherDollarColumns,
		tableClass: "balancingToolSummary",
		data: otherDollarSummaryData
	}).startUp();
	
	var otherDollarSectionData = [];
	
	appendJsons([otherDollarSectionData,dollarCashHeader,cashNonCounterData.prodTotals,cncTotal,cashCounterData.prodTotals,ccTotal,areaCashTotal,dollarCheckHeader,dollarChequeData.prodTotals,dolChTotal,totDrawnMlaData.prodTotals,totDrawnMlaTotal,totDrawnOutPHData.prodTotals,totDrawnOutPHTotal,checkNonCounterData.prodTotals,chncTotal,areaCheckTotal,cashierTotalHeader,cashierTotal,cashierTotalRow,generateSnapshotSection(otherSnapshot)]);
	
	new TableContainer({
		divName : otherDollarDivDetails.divName,
		nodeName : otherDollarDivDetails.nodeName,
		width : "900px",
		id : otherDollarDivDetails.tableId,
		layout : otherDollarColumnsNH,
		tableClass: "balancingToolSummary",
		data: otherDollarSectionData
	}).startUp();
	
	var otherDollarTableDiv = document.getElementById(otherDollarTableDivId);

	otherDollarTableDiv.style.display = "none";
	
	});
}


var otherDollarSecData = [];

function mergeOtherDollarCashierProductTypes(){
	var cashierRep = [];
	for(c in otherDollarData){
		//using jQuery.inArray instead of Array.indexOf because it is not supported in IE8 and below. thanks IE.
		if(jQuery.inArray(otherDollarData[c].cashierId+"~"+otherDollarData[c].product,cashierRep) == -1){
			var pushedData = "";
			pushedData = pushedData.concat(otherDollarData[c].cashierId);
			pushedData = pushedData.concat("~");
			pushedData = pushedData.concat(otherDollarData[c].product);
			cashierRep.push(pushedData);
		}
	}
	
	for(var a = 0; a < cashierRep.length; a++){
		otherDollarSecData.push(mergeOtherDollarCashierData(cashierRep[a]));
	}
		
}

function getOtherDollarAreaTotal(area){
	var tots = {};
	var cobDollar = 0;
	for(a in area){
		cobDollar = cobDollar + parseFloat(area[a].cashierTotals.otherDollarTotal.replace(/,/g,""));
	}
	tots.cobDollar = numberWithCommas(parseFloat(cobDollar).toFixed(2));
	return tots;
}

function generateOtherDollarSectionData(section,cdTarget){
	var sectionObj = {};
	var sectionCashierTotals = {};
	var otherDollarTotal = 0;
	var  returnCNC = [];
	var firstRowFlag = true;
	for(a in otherDollarSecData){
		var tempCNC = {};
		if(otherDollarSecData[a].product == section){
			var isCashDepo=false;
			var isCheckDepo=false;
			if(firstRowFlag){
				switch(section){
					case "totalCashNonCounter": tempCNC.product = "<div class='alignLeft'>Non Counter</div>";break;
					case "totalCashCounter": tempCNC.product = "<div class='alignLeft'>Counter</div>";isCashDepo=true;break;
					case "totalCheckOnUs": tempCNC.product = "<div class='alignLeft'>On Us</div>";isCheckDepo=true;checkType="ON_US";break;
					case "totalCheckRegional": tempCNC.product = "<div class='alignLeft'>Regional</div>";isCheckDepo=true;checkType="REGIONAL";break;
					case "totalCheckNonCounter": tempCNC.product = "<div class='alignLeft'>Non Counter</div>";break;
					case "totalDollarCheque": tempCNC.product = "<div class='alignLeft'>Dollar Cheque</div>";isCheckDepo=true;checkType="DOLLARCHECK";break;
					case "totalUsCheckOutPh": tempCNC.product = "<div class='alignLeft'>US Cheque drawn outside PH</div>";isCheckDepo=true;checkType="USCD_OUTSIDEPH";break;
					case "totalUsCheckInManila": tempCNC.product = "<div class='alignLeft'>US Cheque drawn in Manila</div>";isCheckDepo=true;checkType="USCD_MANILA";break;
					default: alert("Unknow Product Total: " + section); break;
				}
			} else{
				tempCNC.product = "";
				switch(section){
					case "totalCashCounter": isCashDepo=true;break;
					case "totalCheckOnUs": isCheckDepo=true;checkType="ON_US";break;
					case "totalCheckRegional": isCheckDepo=true;checkType="REGIONAL";break;
					case "totalDollarCheque": isCheckDepo=true;checkType="DOLLARCHECK";break;
					case "totalUsCheckOutPh": isCheckDepo=true;checkType="USCD_OUTSIDEPH";break;
					case "totalUsCheckInManila": isCheckDepo=true;checkType="USCD_MANILA";break;
				}
			}
			if(isOTHERConfirmed(otherDollarSecData[a].cashierId)){
				tempCNC.cobDollar = "<div class='alignRight'>"+numberWithCommas(parseFloat(otherDollarSecData[a].cobDollar).toFixed(2))+"</div>";
				if(isCashDepo){
					if(parseFloat(otherDollarSecData[a].cobDollar) > 0){
						tempCNC.dsCobd  = getCashDepoURL("OTHER","CASH","USD",otherDollarSecData[a].cashierId);
						tempCNC.vdsCobd  = getValidatedCashDepoURL("OTHER","CASH","USD",otherDollarSecData[a].cashierId);	
					}
				}else if(isCheckDepo){
					if(parseFloat(otherDollarSecData[a].cobDollar) > 0){
						tempCNC.dsCobd  = getChequeDepoURL("OTHER","CHEQUE","USD",otherDollarSecData[a].cashierId,checkType);
						tempCNC.vdsCobd  = getValidatedChequeDepoURL("OTHER","CHEQUE","USD",otherDollarSecData[a].cashierId,checkType);	
					}
				}
				otherDollarTotal = otherDollarTotal + parseFloat(otherDollarSecData[a].cobDollar);
			} else{
				tempCNC.cobDollar = "<div class='alignRight'>-</div>";
			}
			otherDollarConsolidatedTotals.cobDollar = (parseFloat(otherDollarConsolidatedTotals.cobDollar) + parseFloat(otherDollarSecData[a].cobDollar)).toFixed(2);
			var isWholeRowZero = false;
			if(parseFloat(otherDollarSecData[a].cobDollar) == 0){
				isWholeRowZero = true;
			}
			tempCNC.reconciled = "<div class='alignCenter'>"+getReconCheckbox(rowIndex,isWholeRowZero)+"</div>";
			tempCNC.reconciledBy = "<div class='alignCenter'>"+getReconcilerName(rowIndex,isWholeRowZero,false)+"</div>";
			tempCNC.reconciledDate = "<div class='alignCenter'>"+getReconciledDate(rowIndex)+"</div>";
			tempCNC.ccmNotes = "<div class='alignCenter'>"+getNote(rowIndex,"CCM")+"</div>";
			tempCNC.ppaNotes = "<div class='alignCenter'>"+getNote(rowIndex,"PPA")+"</div>";
			tempCNC.findings = "<div class='alignCenter'  collapseTarget='"+cdTarget+"'>"+getNote(rowIndex,"FINDINGS")+"</div>";			
			rowIndex++;
			tempCNC.cashier = "<div class='alignLeft'>"+otherDollarSecData[a].cashierName+"</div>";
			firstRowFlag = false;
			//JSARD for testing
			//if(isReconciled(rowIndex)){
				//alert('highlight the row, change bgcolor');
				//highlightReconciledRow(tempCNC);
			//}
			returnCNC.push(tempCNC);
		}
	}
	sectionCashierTotals.otherDollarTotal = numberWithCommas(parseFloat(otherDollarTotal).toFixed(2));
	sectionObj.prodTotals = returnCNC;
	sectionObj.cashierTotals = sectionCashierTotals;
	return sectionObj;
}

function mergeOtherDollarCashierData(cashierProduct){
	var cpArr = cashierProduct.split("~");
	var cashierId = cpArr[0];
	var product = cpArr[1];
	var tempCashier = {};
	for(a in otherDollarData){
		if(otherDollarData[a].cashierId == cashierId && otherDollarData[a].product == product){
			if(otherDollarData[a].productCode == "COBD"){
				tempCashier.cobDollar = otherDollarData[a].total;
			}
			tempCashier.cashierId = otherDollarData[a].cashierId;
			tempCashier.cashierName = otherDollarData[a].cashierName;
			tempCashier.product = product;
		}
	}
	return tempCashier;
}

function getOTHERDGrandTotalPerCashier(cdTarget){
	var returnSection = [];
	var c = 0;
	for(c in cashierList){ 
		var cobDollar = 0;
		var d=0;
		for(d in otherDollarData){
			if(jQuery.inArray(otherDollarData[d].product,excludeFromCashierTotal) == -1 && otherDollarData[d].cashierId.toUpperCase() == cashierList[c].id.toUpperCase()){
				switch(otherDollarData[d].productCode){
					case "COBD": cobDollar = cobDollar + parseFloat(otherDollarData[d].total);break;
				}
			}
		}
		var tempCashier = {};
		tempCashier.cashier = "<div class='alignLeft'  collapseTarget='"+cdTarget+"'>"+cashierList[c].name+"</div>";
		if(isOTHERConfirmed(cashierList[c].id)){
			tempCashier.cobDollar = "<div class='alignRight'>"+numberWithCommas(cobDollar.toFixed(2))+"</div>";
		} else {
			tempCashier.cobDollar = "<div class='alignCenter'>-</div>";
		}
		returnSection.push(tempCashier);
	}
	return returnSection;
}

var otherDollarConsolidatedTotals = {cobDollar: "0.00"};