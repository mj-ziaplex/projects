function centerCell(val){
	return "<center>" + val + "</center>";
}

function appendJsons(jsons){
	for(var x = 0; x < jsons.length ; x++){
		var json2 = jsons[x+1];
		for(y in json2){
			jsons[0].push(json2[y]);
		}
	}
}

function boldCell(val){
	return "<b>" + val + "</b>";
}

function validateIsANumber(obj){
	if(isNaN(obj.value.replace(/,/g,""))){
		obj.value = "0.00";
		obj.focus();
	} else {
		return true;
	}
}

function alignRightCell(val){
	return "<p align=\"right\">" + val + "</p>";
}
function alignLeftCell(val){
	return "<p align=\"left\">" + val + "</p>";
}

function checkForValue(val){
	//if all values are 0.00 return false; if has valid value return true;
	for (prop in val) {
	    if (!val.hasOwnProperty(prop)) {
	        //The current property is not a direct property of p
	        continue;
	    }
	    	var value = val[prop];
	    	if(prop=="product"){
	    		continue;
	    	}
	    	if(parseFloat(value) > 0 && value != " " && value.length > 0){
	    		return true;
	    	}
	    	
	}
		
	return false;
}

function numberWithCommas(x) {
    var parts = x.toString().split(".");
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    return parts.join(".");
}

var spwindow = null;

var tempWindow = null;

var stime = null;
var starttime = null;

function openCashierSP(val){
	closeCashierSP();
	stime = new Date(); 
	starttime = stime.getTime();
	$(".loader").show();
	self.focus();
        
	// Added for Audit finding for CCQA notes
    var isPpa = val.isPpa;
    var url = val.splink;
    if("true" === isPpa) {
    	url = val.ppaLink;
    }
        
	spwindow = window.open(url, "_blank", "width=1000,height=700,resizable=yes,status=yes,scrollbars=yes");
	tempWindow = spwindow;
	
	tempWindow.blur();
	$(tempWindow).load(function() {
		$(".loader").hide();
		tempWindow.focus();
	});
}

function openAjaxCashierSP(val){
	closeCashierSP();
	stime = new Date(); 
	starttime = stime.getTime();
	$(".loader").show();
	var stat = val.stat;
	var timeoutDef = 10; // in minutes
	var timeoutVal = timeoutDef * 60000;
        
	// Added for Audit finding for CCQA notes
    var isPpa = val.isPpa;
    var url = val.splink;
    if ("true" === isPpa) {
    	url = val.ppaLink;
    }
        
	if(null != stat) {
		self.focus(); 
		if(stat != "Awaiting Feedback" && stat != "For Review and Approval" && stat != "Approved for Recon") {
			jQuery.support.cors = true;
			$.ajax({
				url : val.aplink,
                type : 'POST',
                cache: false,
                timeout: timeoutVal,// in milliseconds
                success : function(res) {
                	// Modified for Audit finding for CCQA notes
                	spwindow = window.open(url, "_blank", "width=1000,height=700,resizable=yes,status=yes,scrollbars=yes");
                    tempWindow = spwindow;
			    	
			    	tempWindow.blur();
			    	$(tempWindow).load(function() {
			    		$(".loader").hide();
			    		tempWindow.focus();
			    	});
			    	
			    },   
			    error : function(xhr, status, error) {
			    	if(status == "timeout") {
			    		alert('IPAC/Ingenium response takes too long...');
			    		var conf = window.confirm('Do you want to continue?');
			    		if(conf){
					    	$(".loader").hide();
					    	openCashierSP(val);
			    		}else{
			    			$(".loader").hide();
			    		}
			    	} else {
			    		alert("An error occurred: " + status + "\nError: " + error);
			    	}
			    }   
			});
		} else {
			$(".loader").hide();
            openCashierSP(val);
		}
	}
}

function pageLoaded(win){
	var etime = new Date();
	var endtime = etime.getTime();
	var totaltime = (endtime - starttime) / 1000;
	var result = Math.round(totaltime * 100) / 100;
	var title = win.document.title;
	win.document.title = title +" loaded in " + result + " sec";
}

function closeCashierSP(){
	if(spwindow != null || spwindow != tempWindow){
		spwindow.close();
	}
}

function getCashDepoURL(company,prodCode,currency,cashierId){
	var c = 0;
	for(c in cashDepositSlips){
		if(cashDepositSlips[c].company == company && cashDepositSlips[c].prodCode == prodCode && cashDepositSlips[c].currency == currency && cashDepositSlips[c].acf2id.toUpperCase() == cashierId.toUpperCase()){
			return "<a target='_blank' href='../viewfile/view.html?fileId="+cashDepositSlips[c].dcrDepoSlipVersionSerialId+"'>view</a>";
		} 
	}
	return "";
}

function getValidatedCashDepoURL(company,prodCode,currency,cashierId){
	var c = 0;
	for(c in validatedCashDepositSlips){
		if(validatedCashDepositSlips[c].company == company && validatedCashDepositSlips[c].prodCode == prodCode && validatedCashDepositSlips[c].currency == currency && validatedCashDepositSlips[c].acf2id.toUpperCase() == cashierId.toUpperCase()){
			return "<a target='_blank' href='../viewfile/view.html?fileId="+validatedCashDepositSlips[c].dcrDepoSlipVersionSerialId+"'>view</a>";
		} 
	}
	return "";
}

var reversalWindow = null;
var tempReversalWindow = null;

function closeReversalWindow(){
	if(reversalWindow != null || reversalWindow != tempReversalWindow){
		reversalWindow.close();
	}
}

function openSPReversal(balancingToolProductId){
	closeReversalWindow();
	reversalWindow = window.open("../reversal/view.html?dcrBalancingToolProductId="+balancingToolProductId, "_blank", "width=810,height=500,resizable=yes,status=yes,scrollbars=yes");
	tempReversalWindow = reversalWindow;
}

function getBalancingToolProductId(cashierId,column,product,dataArray){
	var z = 0;
	for(z in dataArray){
		if(cashierId == dataArray[z].cashierId && product == dataArray[z].product && column == dataArray[z].productCode){
			return dataArray[z].balancingToolProductId;
		}
	}
	
}

function getChequeDepoURL(company,prodCode,currency,cashierId,checkType){
	var c = 0;
	for(c in chequeDepositSlips){
		if(chequeDepositSlips[c].company == company && chequeDepositSlips[c].prodCode == prodCode && chequeDepositSlips[c].currency == currency && chequeDepositSlips[c].acf2id.toUpperCase() == cashierId.toUpperCase() && chequeDepositSlips[c].checkType == checkType){
			return "<a target='_blank' href='../viewfile/view.html?fileId="+chequeDepositSlips[c].dcrDepoSlipVersionSerialId+"'>view</a>";
		} 
	}
	return "";
}

function getValidatedChequeDepoURL(company,prodCode,currency,cashierId,checkType){
	var c = 0;
	for(c in validatedChequeDepositSlips){
		if(validatedChequeDepositSlips[c].company == company && validatedChequeDepositSlips[c].prodCode == prodCode && validatedChequeDepositSlips[c].currency == currency && validatedChequeDepositSlips[c].acf2id.toUpperCase() == cashierId.toUpperCase() && validatedChequeDepositSlips[c].checkType == checkType){
			return "<a target='_blank' href='../viewfile/view.html?fileId="+validatedChequeDepositSlips[c].dcrDepoSlipVersionSerialId+"'>view</a>";
		} 
	}
	return "";
}

function trim(str){
	
	var trimmed = str.replace(/(^\s+)|(\s+$)/g,'');
	
	return trimmed;
}

function collapseDivision(val){
	var divs = document.getElementsByTagName("div");
	var d = 0;
	for(d in divs){
		if(divs[d].collapseTarget == val.cdTarget){
			var parentColumn = divs[d].parentNode;
			var parentRow = parentColumn.parentNode;
			if(parentRow.style.display != "none"){
				parentRow.style.display = "none";
			} else{
				parentRow.style.display = "block";
			}
		}
	}
}

var ctrClk = 0;

function showAttached(frm,src){
	if(ctrClk == 0){
		$(frm).attr('src', src);
		++ctrClk;
	}
}

function showLoader() {
    $(".loader").show();
}

function window.confirm(str) {
	execScript('n = msgbox("' + str + '","4132")', "vbscript");
    return (n == 6);
}